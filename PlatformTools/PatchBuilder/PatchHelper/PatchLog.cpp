﻿#include "StdAfx.h"
#include "PatchLog.h"
#include "PatchDef.h"

CPatchLog::CPatchLog(void)
: m_iFileCount(0)
{

}

CPatchLog::~CPatchLog(void)
{
	m_mapLogInfos.clear();
}

CPatchLog* CPatchLog::GetInstance()
{
	static CPatchLog* m_pkThis = new CPatchLog();

	return m_pkThis;
}

bool	CPatchLog::AddLog(PATCH_LOG_TYPE nType,string strLog)
{
	if (strLog.empty())
	{
		return false;
	}

	strLog += "\t";
	strLog += _GetTimeStamp();

	m_mapLogInfos[nType].push_back(strLog);

	printf(strLog.c_str());
	printf("\n");

	return true;
}

bool	CPatchLog::CreateLogFile(string strLogFileDir)
{
	if (strLogFileDir.empty())
	{
		return false;
	}

	// check whether there is such a directory
	if (_access_s(strLogFileDir.c_str(),2) != 0)
	{
		printf("Could not open dir (error %d)\n", GetLastError());
		return false;
	}

	string strLogFilePath = strLogFileDir + "\\" + PATCH_LOG_FILE_NAME;

	// delete orginal file if already exist [3/18/2010 hemeng]
	if (_access_s(strLogFilePath.c_str(),0) == 0)
	{
		if (!DeleteFile(strLogFilePath.c_str()))
		{
			printf("Fail to delete exist log file: %s \n", strLogFilePath.c_str());
			return false;
		}
	}

	FILE* pFile;
	if ( fopen_s( &pFile,strLogFilePath.c_str(), "wb" ) )
	{
		printf("Could not open file(error %d)\n", GetLastError());
		return false;
	}

	bool bSuccess = _WriteLogFile(pFile);

	fclose(pFile);	

	return bSuccess;	
}

bool	CPatchLog::_WriteLogFile(FILE* pFile)
{
	if (NULL == pFile)
	{
		return false;
	}

	map<PATCH_LOG_TYPE,vector<string>>::iterator it = m_mapLogInfos.begin();


#ifdef _DEBUG
	for ( ; it != m_mapLogInfos.end(); it++ )
	{
		vector<string> vLogs = it->second;
		char* szSplit = "-------------------------------------------------------\r\n";
		if ( fwrite( szSplit, strlen(szSplit),1, pFile ) != 1 )
		{
			printf("Could not write file(error %d)\n", GetLastError());
			return false;
		}

		for ( vector<string>::size_type uiIndex = 0; uiIndex < vLogs.size(); uiIndex++ )
		{
			string strTmp = vLogs[uiIndex] + "\n";
			const char* szLog = strTmp.c_str();
			size_t nBufferSize = strlen(szLog);

			if ( fwrite( szLog, nBufferSize,1, pFile ) != 1 )
			{
				printf("Could not write file(error %d)\n", GetLastError());
				return false;
			}
		}
	}
#else
	it = m_mapLogInfos.find(PLT_ERROR);
	if (it != m_mapLogInfos.end())
	{
		vector<string>::iterator findIt = it->second.begin();
		for (; findIt != it->second.end(); findIt ++)
		{
			string strTmp = (*findIt) + "\n";
			const char* szLog = strTmp.c_str();
			size_t nBufferSize = strlen(szLog);

			if ( fwrite( szLog, nBufferSize,1, pFile ) != 1 )
			{
				printf("Could not write file(error %d)\n", GetLastError());
				return false;
			}
		}
	}

#endif

	// 写入文件计数器 [12/16/2009 hemeng]
	char szTmp[200];
	sprintf_s(szTmp,200,"Total process file:%d\n", m_iFileCount);
	size_t nStrLen = strlen(szTmp);
	if (fwrite(szTmp, nStrLen, 1, pFile) != 1)
	{
		printf("Could not write file(error %d)\n", GetLastError());
		return false;
	}

	return true;
}

string CPatchLog::_GetTimeStamp()
{
	string strTime = "";

	SYSTEMTIME systime;
	GetLocalTime(&systime);

	char szTime[50];
	sprintf_s(szTime, 50, "%u/%u/%u  %u:%u:%u:%u", 
		systime.wYear, systime.wMonth, systime.wDay,
		systime.wHour, systime.wMinute, systime.wSecond,
		systime.wMilliseconds);
	strTime += szTime;

	return strTime;

}

void CPatchLog::IncFileCount(int iCount)
{
	m_iFileCount += iCount;
}

