﻿#if	!defined _PATCHRESOURCEHELPER_H_
#define _PATCHRESOURCEHELPER_H_

#pragma once

#include <string>
#include <windows.h>
#include <io.h>
#include "PatchHelper.h"

using namespace std;

class CResourceHelper
{
public:
	CResourceHelper(unsigned int uiResID, bool bInner);
	~CResourceHelper(void);

	unsigned int	GetResourceID() const;
	unsigned int	GetResourceSize() const;
	unsigned int	GetResourcePosOffset() const;

	// 由于数据是写在exe尾部的，如果先写数据在update内部RES则会造成写入数据被删除（PE更新造成） [1/8/2010 hemeng]
	// 如果对同一个exe更新多个外部资源，则应先对所有资源初始化，然后再更新 [1/8/2010 hemeng]
	// 更新数据首先要init 然后再更新 [1/8/2010 hemeng]
	//bool			Init(string strExeFileName);
	// 设置文件位置 [1/11/2010 hemeng]
	//void			SetResourcePosOffset(unsigned int uiOffset);
	// 更新数据首先要init 然后再更新 [1/8/2010 hemeng]
	bool			DoUpdateResouce(string strExeFileName,string strResFileName);

protected:
	CResourceHelper(void);
	// 是否包含m_uiResID资源 [1/4/2010 hemeng]
	//bool			_ResourceValidate(string strExeFileName);
	bool			_UpdateResourceInfo(string strExeFileName);
	bool			_WriteResouce(string strExeFileName,string strResFileName);
	bool			_UpdateInnerResource(string strExeFileName, string strResFileName);

protected:
	unsigned int	m_uiResID;
	unsigned int	m_uiResSize;
	unsigned int	m_uiResPosOffset;
	// 资源是作为EXE内部资源更新，还是作为外部资源 [1/8/2010 hemeng]
	// m_bInner = true 资源更新采用UpdateResource方式，反之则写在exe尾部  [1/8/2010 hemeng]
	bool			m_bInner;			
};

#endif