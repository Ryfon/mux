﻿#if	!defined _PATCHHELPER_H_
#define _PATCHHELPER_H_

#pragma once

#include <string>
#include <windows.h>
#include <map>
#include <vector>
#include <sstream>
#include <io.h>
#include "tinyxml.h"

using namespace std;

// msg 报错注释，filePath 报错文件名
//#ifdef CODE_IN_PATCH
//#define PATCH_MEG_REPROT(msg,fileName) do {   char szInfo[MAX_PATH * 3];\
//	sprintf_s( szInfo, "%s %s\n", msg,fileName );\
//	MessageBox(NULL,szInfo,"Error",MB_OK); } while(0)
//#else
#define PATCH_MEG_REPROT(msg,filePath)do {   char szInfo[MAX_PATH * 3];\
	sprintf_s( szInfo, "%s %s\n", msg,filePath );\
	OutputDebugString(szInfo); } while(0)
//#endif //#ifndef 

// 严重错误报告 [8/25/2009 hemeng]
#define PATCH_ERROR_REPROT(msg,fileName) do {   char szInfo[MAX_PATH * 3];\
	sprintf_s( szInfo, "%s %s\n", msg,fileName );\
	MessageBox(NULL,szInfo,"Error",MB_OK); } while(0)

struct SPatchVersionRule 
{
	string strVerPre;
	string strCmpDate;
	map<string,string> mapBranch;
};

class CPatchHelper
{
public:
	CPatchHelper(void);
	~CPatchHelper(void);

	// 获取当前工作目录 [8/14/2009 hemeng]
	// 参数		
	// 返回值	工作目录以“\\”结尾
	static string	GetCurrWorkDir();

	// 将string转换为byte* [8/14/2009 hemeng]
	// 参数		str 转换源	pBuffer 转换目标 nBufferSize pBuffer的大小
	// 返回值	成功 true 失败 false 
	static bool		StringToBYTE(string str, BYTE* pBuffer, UINT nBufferSize);

	// 删除vector中的数据 [8/14/2009 hemeng]
	// 参数		vector 源	strInfo 查找目标
	// 返回值	成功 true 失败 false
	static bool		DeleteVectorElem(vector<string>& vec,string strInfo);

	// string 与DWORD互转
	static DWORD MyStringToDWORD(string str);
	static string MyIntToString(int num);

	// 版本号比较 [8/13/2009 hemeng]
	// 比较错误 return -2
	// strSrcVer > strDesVer return 1	strSrcVer < strDesVer return -1		strSrcVer == strDesVer return 0
	static int		CompareVersion(string strSrcVer,string strDesVer);

	// 从指定文件读取版本信息 [8/6/2009 hemeng]
	static string	GetVersion(string strFilePath,SPatchVersionRule sRule,string strPatchClientPath);

	static bool		LoadVersionRule(TiXmlElement* pEmlt, SPatchVersionRule& sRule);

	static string	LoadVersionFromPatchClient(string strPathClientPath);

	static void		StringLower(string& str );

	static bool		IsRoot(string szPath );

	// 获取版本号 [1/11/2010 hemeng]
	// 返回为MAX_PATH长的数组 [1/11/2010 hemeng]
	static char*	GetFileVersion(string strFileName);

	// 验证文件是否存在，是否可改写 [9/10/2009 hemeng]
	static bool	VerifyFile(string strFileName);

	
private:
	// return strSrc - strDes [9/17/2009 hemeng]
	static int _SubDate(string strSrcDate,string strDesDate);
};

#endif