﻿#include "StdAfx.h"
#include "PatchReportSender.h"

//CPatchReportSender* CPatchReportSender::m_spThis = NULL;
CPatchReportSender::CPatchReportSender(void)
{
	m_dwThreadID = 0;
	m_hdWind = NULL;

	m_bInit = false;	
}

CPatchReportSender::~CPatchReportSender(void)
{
	if (m_hdWind)
	{
		m_hdWind = NULL;
	}
}

bool	CPatchReportSender::Initial(DWORD dwThreadID)
{
	if (dwThreadID == 0)
	{
		m_bInit = false;
		return false;
	}

	m_dwThreadID = dwThreadID;

	m_bInit = true;
	return true;
}

bool	CPatchReportSender::Initial(HWND hdWin)
{
	if (hdWin == NULL)
	{
		return false;
	}

	m_hdWind = hdWin;
	m_bInit = true;

	return true;
}

CPatchReportSender* CPatchReportSender::GetInstance()
{
	static CPatchReportSender sender;
	return &sender;
}

BOOL	CPatchReportSender::SendUpdateMessage(WPARAM nState,LPARAM nProgress)
{
	if (!m_bInit)
	{
		return FALSE;
	}


	BOOL bSuccess = true;

	if (m_dwThreadID != 0)
	{
		bSuccess = ::PostThreadMessage(m_dwThreadID,(UINT)nState,nState,nProgress);
		if (!bSuccess)
		{
			DWORD dwError = ::GetLastError();
		}

	}
	else if (m_hdWind != NULL)
	{
		bSuccess = ::PostMessage(m_hdWind,(UINT)nState,nState,nProgress);
		if (!bSuccess)
		{
			DWORD dwError = ::GetLastError();
		}
	}
	else
	{
		return false;
	}
	
	return bSuccess;
}