﻿#if	!defined _PATCHLOG_H_
#define _PATCHLOG_H_

/** @file PatchLog.h 
@brief patch信息文件
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2009-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#pragma once
#include "PatchHelper.h"
#include <string>
#include <map>
#include <vector>
#include <io.h>

// PATCH报告类型 [8/14/2009 hemeng]
enum	PATCH_LOG_TYPE
{
	PLT_ERROR = 0,
	PLT_WARNNING,
	PLT_MSG,
};


class CPatchLog
{
public:
	static CPatchLog*	GetInstance();

	// 插入log信息 [8/14/2009 hemeng]
	// 参数		nType log类型 strLog log内容
	// 返回值	成功 true 失败 false 
	bool	AddLog(PATCH_LOG_TYPE nType,string strLog);
	
	// 创建log文件 [8/14/2009 hemeng]
	// 参数		strLogFileDir log文件存放目录
	// 返回值	成功 true 失败 false 
	bool	CreateLogFile(string strLogFileDir);	

	// 文件计数器递增 [12/16/2009 hemeng]
	void	IncFileCount(int iCount = 1);

protected:
	CPatchLog(void);
	~CPatchLog(void);

	bool	_WriteLogFile(FILE* pFile);

	// 插入time log [12/16/2009 hemeng]
	string	_GetTimeStamp();	
	
protected:
	// variables
	map<PATCH_LOG_TYPE,vector<string>>	m_mapLogInfos;		// log信息
	int	m_iFileCount;		// 文件计数器 [12/16/2009 hemeng]
};

#endif