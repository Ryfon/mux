﻿/** @file PatchDef.h 
@brief 文件打包：patch工具一些定义 第一版测试
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-08-06 
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#if	!defined _PATCHDEF_H_ 
#define 	_PATCHDEF_H_

// 说明 [8/6/2009 hemeng]
// 预定义更新资源的ID [8/20/2009 hemeng]
#define RESOURCE_ID	132
#define PACK_CONFIG_RES_ID	133

// 必须将打包配置文件至于可执行文件同目录下
#define CLIENT_VERSION_LOG				"MUX"						// 版本号前标志 [8/6/2009 hemeng]
#define CLIENT_VERSION_SUB_SGIN			 "_"						// 版本号出现前的标志 [8/24/2009 hemeng]
#define CLIENT_VERSIN_LENGTH			10							// 版本号长度 [8/6/2009 hemeng]
#define PATCH_CONFIGXML_FILE_NAME		"PatchClient.xml"			// 客户端版本号文件 [1/11/2010 hemeng]
#define CLIENT_VERSION_FILE				"version.txt"				// 版本号文件名 [8/6/2009 hemeng]
#define PACKAGE_EXTENTION				".pkg"	

#define PATCH_DATAPACK_NAME				".mux"						// patch文件的类型 [8/12/2009 hemeng]
#define PATCH_CLIENT_ROOT_FILE			"PatchBuilder_config.xml"	// 配置patch包根目录的文件
#define PATCH_RESULT_FILE_NAME			"patch.xml"				// 更新文件列表 [1/11/2010 hemeng]	
#define PATCH_LOG_FILE_NAME				"Patch_log.log"
#define PATCH_DOWNLOAD_DIR				"download"					// 更新文件下载目录 [1/11/2010 hemeng]
#define PATCH_RESOURCE_TMP_FILE_NAME	"patch_tmp.tmp"				// 释放包创建的临时文件 [8/21/2009 hemeng]
#define PATCH_MUTEX_FILE_NAME			"MuxLauncher.exe"			// 如果更新文件中有muxlauncher.exe，则要先关闭该线程，待更新完毕重新启动线程 [8/25/2009 hemeng]
#define PATCH_MUTEX_DLL_NAME			"patchclient.dll"			// muxlauncher使用的dll [9/7/2009 hemeng]
#define PATCHCLIENT_RESULT_FILE_NAME	"patchclient_log.xml"		// launcher相关更新文件列表 [1/11/2010 hemeng]
#define PATCHCLIENT_SERVER_FILE_NAME	"LauncherVersion.txt"		// 服务器需要读取的launcher相关更新列表 [1/11/2010 hemeng]
#define PATCHCLIENT_UPDATER_FILE_NAME	"LauncherUpdate.exe"

// 消息处理标志宏 [8/25/2009 hemeng]
#define WM_MUXPATCHUPDATE				WM_APP + 1						// 安装进度消息 [8/24/2009 hemeng]
#define MUXPATCH_ERROR_INIT				WM_MUXPATCHUPDATE + 1000		// 初始化失败 [8/24/2009 hemeng]
#define MUXPATCH_ERROR_CONTENTVERSION	WM_MUXPATCHUPDATE + 1001		// 错误的内容版本（与当前更新包版本不符）  [8/24/2009 hemeng]
#define MUXPATCH_ERROR_UPDATEFAIL		WM_MUXPATCHUPDATE + 1002		// 更新失败 [8/24/2009 hemeng]
#define MUXPATCH_ERROR_FINISHING		WM_MUXPATCHUPDATE + 1003		// 完成更新是出错 [8/24/2009 hemeng]
#define	MUXPATCH_ERROR_ALREADYEXIST		WM_MUXPATCHUPDATE + 1004		// 已有启动的更新线程
#define	MUXPATCH_UPDATE_INIT			WM_MUXPATCHUPDATE + 1			// Patch初始化成功 [8/24/2009 hemeng]
#define	MUXPATCH_UPDATE_PATCHING		WM_MUXPATCHUPDATE + 2			// Patch成功 [8/24/2009 hemeng]
#define MUXPATCH_UPDATE_FINISHING		WM_MUXPATCHUPDATE + 3			// 完成patch,更新配置文件 [8/24/2009 hemeng]
#define MUXPATCH_FINISH					WM_MUXPATCHUPDATE + 4			// 完成 [8/24/2009 hemeng]

#define MUXPATCH_PROGRESS_BEGIN			0
#define MUXPATCH_PROGRESS_END			100

#define	MUXPATCH_PERCENT_OFFSET			25
#define	MUXPATCH_PRECENT_INIT			25
#define MUXPATCH_PRECENT_PATCHING		50
#define MUXPATCH_PRECENT_FINISHING		75
#define MUXPATCH_PRECENT_FINISHN		100

#ifdef _DEBUG
#define PATCH_RELEASER_FILE_NAME		"ReleasePatchTool_D.exe"
#define PATCH_CLIENT_UPDATER_FILE_NAME	"PatchClientUpdater_D.exe"
#else
#define PATCH_RELEASER_FILE_NAME	"ReleasePatchTool_S.exe"
#define PATCH_CLIENT_UPDATER_FILE_NAME	"PatchClientUpdater_S.exe"
#endif

#endif