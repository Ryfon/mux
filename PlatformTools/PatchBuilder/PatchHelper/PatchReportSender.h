﻿/** @file PatchDef.h 
@brief 文件打包：patch进度消息处理类 第一版测试
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-08-24
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#if	!defined _PATCHREPORTSENDER_H_ 
#define 	_PATCHREPORTSENDER_H_
#pragma once

#include "PatchDef.h"
#include <Windows.h>

class CPatchReportSender
{
public:
	CPatchReportSender(void);
	~CPatchReportSender(void);

	bool	Initial(DWORD dwThreadID);
	bool	Initial(HWND hdWin);
	static	CPatchReportSender* GetInstance();
	BOOL	SendUpdateMessage(WPARAM nState,LPARAM nProgress);

protected:
	

protected:
	bool	m_bInit;
	DWORD	m_dwThreadID;				// 发送目标的线程ID [8/24/2009 hemeng]
	HWND	m_hdWind;					// 对话框模式下窗口句柄 [8/24/2009 hemeng]					
};

#endif