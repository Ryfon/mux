﻿// PatchClientBuilder.cpp : 定义控制台应用程序的入口点。
//

/************************************************************************/
/* patchclient脱离出日常打包，独立生成 
*	//  [5/5/2010 hemeng]
/************************************************************************/

#include "stdafx.h"
#include <iostream>
#include "PatchClientUpdaterBuilder.h"
#include "PatchDef.h"

using namespace std;

bool	CreatePatchReleaser(string strOldName, string strNewName)
{

	if (_access_s(strOldName.c_str(),0) != 0)
	{
		string strMsg = "未找到目标可执行文件，更新资源失败 " + strOldName;
		cerr << strMsg << endl;
		return false;
	}

	if (_access_s(strNewName.c_str(), 0) == 0)
	{
		if (DeleteFile(strNewName.c_str()))
		{
			string strMsg = "删除已存在可执行文件失败，更新可执行文件失败 " + strNewName;
			cerr << strMsg << endl;
			return false;
		}
	}

	if (!CopyFile(strOldName.c_str(),strNewName.c_str(),FALSE))
	{
		string strMsg = "拷贝可执行文件失败，更新可执行文件失败 " + strNewName;
		cerr << strMsg << endl;
		return false;
	}

	return true;
}

bool ProcessAssamble(string strDirPath, string strDesPath)
{
	// validate [5/5/2010 hemeng]
	if (strDirPath.empty() || _access_s(strDirPath.c_str(), 0) != 0 )
	{
		return false;
	}

	if (strDesPath.empty() || _access_s(strDesPath.c_str(), 0) != 0)
	{
		return false;
	}

	string strExeFileName = CPatchHelper::GetCurrWorkDir() + PATCH_CLIENT_UPDATER_FILE_NAME;
	string strNewExeFileName = strDesPath + "\\" + PATCHCLIENT_UPDATER_FILE_NAME;

	if (!CreatePatchReleaser(strExeFileName, strNewExeFileName))
	{
		cerr << "创建更新EXE出错" << endl;
		return false;
	}


	CPatchClientUpdaterBuilder* pkPatchBuilder = new CPatchClientUpdaterBuilder();
	if (!pkPatchBuilder->Init(strDirPath))
	{
		return false;
	}

	bool bSuccess = pkPatchBuilder->BuildResource(strNewExeFileName);

	if (!bSuccess)
	{
		cerr << "更新资源出错"  << endl;
	}

	delete pkPatchBuilder;
	pkPatchBuilder = NULL;

	return bSuccess;
}

int _tmain(int argc, _TCHAR* argv[])
{
	if ( NULL == argv[1] )
	{
		return 1;
	}
	// commands 第一项是应用程序路径，第二项起为参数 [8/10/2009 hemeng]
	if (argv[1] != "" && argv[2] != "")
	{
		if (ProcessAssamble(argv[1],argv[2]) == false)
		{
			getchar();
			return 1;
		}
	}

	return 0;
}

