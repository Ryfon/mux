﻿#ifndef _PATCHCLIENTUPDATERBUILDER_H_
#define _PATCHCLIENTUPDATERBUILDER_H_

#pragma once
#include <vector>
#include <map>
#include <string>
#include "tinyxml.h"
#include "ResourceHelper.h"

using namespace std;

// 添加资源到exe的尾部 [1/8/2010 hemeng]

// 与PatchClientUpdater中用户自定义资源第一个ID对应 [1/8/2010 hemeng]
#define USER_RESOURCE_DATAID                     130

class CPatchClientUpdaterBuilder
{
public:
	CPatchClientUpdaterBuilder(void);
	~CPatchClientUpdaterBuilder(void);

	bool	Init(string strResPath);
	bool	BuildResource(string strExeFileName);

protected:
	bool	_LoadConfigFile(string strFileName);
	// 创建patchclinet读取的配置文件 [1/8/2010 hemeng]
	bool	_CreateVersionFile(string strVersionFileName);
	// 读取patchclient文件列表 [1/8/2010 hemeng]
	bool	_LoadPatchClientFileList(TiXmlElement* pElmt, map<string, CResourceHelper>& mapFileList);
	// 读取有版本号的文件列表 [1/8/2010 hemeng]
	bool	_LoadCompareVersionFileList(TiXmlElement* pElmt, vector<string>& vFileList);
	// 创建并写入更新文件信息 [1/8/2010 hemeng]
	bool	_UpdateResourceInfo(unsigned int uiWriteID, string strExeFileName);
	// 创建更新信息表 [1/11/2010 hemeng]
	bool	_ExportUpdateInfoFile(string strExprotPath);
	// 创建服务器使用的配置文件 [1/11/2010 hemeng]
	bool	_ExportServerFile(string strExportPath);


protected:
	bool	m_bInit;
	//vector<string>	m_vUpdateFileName;
	map<string, CResourceHelper>	m_mapUpdateFileList;
	string	m_strResPath;
	vector<string>	m_vCompareVersionList;
};

#endif