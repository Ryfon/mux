﻿#include "StdAfx.h"
#include "PatchClientUpdaterBuilder.h"
#include "PatchLog.h"
#include "PatchDef.h"
#include "ResourceHelper.h"

CPatchClientUpdaterBuilder::CPatchClientUpdaterBuilder(void)
{
	m_bInit = false;
}

CPatchClientUpdaterBuilder::~CPatchClientUpdaterBuilder(void)
{
	m_mapUpdateFileList.clear();
	m_vCompareVersionList.clear();
}

bool CPatchClientUpdaterBuilder::Init(std::string strResPath)
{
	m_strResPath = strResPath;

	string strWorkDir = CPatchHelper::GetCurrWorkDir();
	if (strWorkDir == "")
	{
		CPatchLog::GetInstance()->AddLog(PLT_ERROR,"PATCHCLIENT:Fail to get current work directory");
		return false;
	}	
	string strConfigFilePath = strWorkDir + PATCH_CLIENT_ROOT_FILE;

	CPatchLog::GetInstance()->AddLog(PLT_MSG,"PATCHCLIENT:Start load config file");
	if (_LoadConfigFile(strConfigFilePath) == false)
	{
		CPatchLog::GetInstance()->AddLog(PLT_ERROR,"PATCHCLIENT:Fail to get load config file");
		return false;
	}

	m_bInit = true;
	return true;
}

bool CPatchClientUpdaterBuilder::BuildResource(std::string strExeFileName)
{
	if (!m_bInit)
	{
		return false;
	}

	CPatchLog::GetInstance()->AddLog(PLT_MSG,"PATCHCLIENT:Start build launcher update resource......");
	if (!_UpdateResourceInfo(USER_RESOURCE_DATAID, strExeFileName))
	{
		string strMsg = "PATCHCLIENT:Fail to update resource information:" + strExeFileName;
		CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
		return false;
	}
	

	bool bSuccess = true;
	// 开始更新 [1/11/2010 hemeng]
	map<string, CResourceHelper>::iterator mapIt = m_mapUpdateFileList.begin();
	for (; mapIt != m_mapUpdateFileList.end(); mapIt++)
	{
		string strResFileName = m_strResPath + "\\" + mapIt->first;

		string strMsg = "PATCHCLIENT:Update resource:" + strResFileName;
		CPatchLog::GetInstance()->AddLog(PLT_MSG,strMsg);
		CPatchLog::GetInstance()->IncFileCount();

		if (!mapIt->second.DoUpdateResouce(strExeFileName,strResFileName))
		{
			strMsg = "PATCHCLIENT:Fail to initial resource updater:" + strResFileName;
			CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
			
			bSuccess = false;
			continue;
		}
	}

	string strServerFileName = strExeFileName.substr(0,strExeFileName.find_last_of("\\") + 1) + PATCHCLIENT_SERVER_FILE_NAME;
	
	string strMsg = "PATCHCLIENT:Export server file:" + strServerFileName;
	CPatchLog::GetInstance()->AddLog(PLT_MSG,strMsg);

	if (!_ExportServerFile(strServerFileName))
	{
		string strMsg = "PATCHCLIENT:Fail to create server file:" + strServerFileName;
		CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
		bSuccess = false;
	}

	return bSuccess;
}

bool CPatchClientUpdaterBuilder::_UpdateResourceInfo(unsigned int uiWriteID, string strExeFileName)
{
	if (!m_bInit)
	{
		return false;
	}

	string strExprotFileName = strExeFileName.substr(0, strExeFileName.find_last_of("\\") + 1) + PATCHCLIENT_RESULT_FILE_NAME;

	string strMsg = "PATCHCLIENT:Export launcher update file list:" + strExprotFileName;
	CPatchLog::GetInstance()->AddLog(PLT_MSG, strMsg);

	if (!_ExportUpdateInfoFile(strExprotFileName))
	{
		strMsg = "PATCHCLIENT:Fail to create file:" + strExprotFileName;
		CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
		return false;
	}

	CResourceHelper nInnerRes(uiWriteID,true);
	if (!nInnerRes.DoUpdateResouce(strExeFileName, strExprotFileName))
	{
		strMsg = "PATCHCLIENT:Fail to update file:" + strExprotFileName;
		CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
		return false;
	}

	return true;
}

bool CPatchClientUpdaterBuilder::_LoadConfigFile(string strFileName)
{
	if (strFileName == "")
	{
		CPatchLog::GetInstance()->AddLog(PLT_ERROR,"Fail to get current work directory");
		return false;
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->LoadFile(strFileName.c_str());

	TiXmlElement* pElmtRoot = pXmlDoc->RootElement();
	TiXmlElement* pElmt = pElmtRoot->FirstChildElement();

	while (pElmt != NULL)
	{
		const char* pszName = pElmt->Value();

		// 获取包根目录命名 [8/18/2009 hemeng]
		if (strcmp(pszName, "PatchClientFile") == 0)
		{
			if (_LoadPatchClientFileList(pElmt,m_mapUpdateFileList) == false)
			{
				return false;
			}
		}
		else if (strcmp(pszName, "CompareVersion") == 0)
		{
			if (_LoadCompareVersionFileList(pElmt, m_vCompareVersionList) == false)
			{
				return false;
			}
		}

		pElmt = pElmt->NextSiblingElement();
	}

	return true;
}

bool	CPatchClientUpdaterBuilder::_LoadPatchClientFileList(TiXmlElement* pElmt, map<string, CResourceHelper>& mapFileList)
{
	if (NULL == pElmt)
	{
		return false;
	}

	TiXmlElement* pChildElmt = pElmt->FirstChildElement();
	if (NULL == pChildElmt)
	{
		return false;
	}

	// 资源文件ID [1/11/2010 hemeng]
	static unsigned int uiResID = USER_RESOURCE_DATAID + 1;
	while (pChildElmt  != NULL)
	{
		string strFileName = pChildElmt->Attribute("FileName");
		CPatchHelper::StringLower(strFileName);
		
		CResourceHelper nResHelper(uiResID,true);
		mapFileList.insert(pair<string, CResourceHelper>(strFileName, nResHelper));
		
		pChildElmt = pChildElmt->NextSiblingElement();
		uiResID ++;
	}

	return true;
}

bool	CPatchClientUpdaterBuilder::_LoadCompareVersionFileList(TiXmlElement* pElmt, vector<string>& vFileList)
{
	if (NULL == pElmt)
	{
		return false;
	}

	TiXmlElement* pChildElmt = pElmt->FirstChildElement();
	if (NULL == pChildElmt)
	{
		return false;
	}

	while (pChildElmt  != NULL)
	{
		string strFileName = pChildElmt->Attribute("FileName");
		CPatchHelper::StringLower(strFileName);
		vFileList.push_back(strFileName);

		pChildElmt = pChildElmt->NextSiblingElement();
	}

	return true;
}


bool	CPatchClientUpdaterBuilder::_ExportUpdateInfoFile(string strExprotPath)
{
	if (!m_bInit)
	{
		return false;
	}

	if (strExprotPath == "")
	{
		return false;
	}

	//插入Xml声明
	TiXmlDocument* pXmlDoc = new TiXmlDocument(strExprotPath.c_str());
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	pXmlDoc->InsertEndChild(*pXmlDec);

	// 写入校验文件名称 [9/2/2009 hemeng]
	TiXmlElement* pXmlCheckElement = new TiXmlElement("UpdateFile");

	map<string, CResourceHelper>::iterator mapIt = m_mapUpdateFileList.begin();
	for (; mapIt != m_mapUpdateFileList.end(); mapIt++)
	{
		string strFileName = mapIt->first;
		TiXmlElement* pXmlCheckChildElm = new TiXmlElement("File");

		pXmlCheckChildElm->SetAttribute("FileName",strFileName.c_str());
		
		unsigned int uiResID = mapIt->second.GetResourceID();
		pXmlCheckChildElm->SetAttribute("ResourceID", uiResID);

		if (NULL == pXmlCheckChildElm)
		{
			delete pXmlCheckChildElm;
			pXmlCheckChildElm = NULL;

			delete pXmlDoc;
			pXmlDoc = NULL;

			return false;
		}

		pXmlCheckElement->LinkEndChild(pXmlCheckChildElm);
	}
	
	pXmlDoc->LinkEndChild(pXmlCheckElement);	

	return pXmlDoc->SaveFile(strExprotPath.c_str());
}

bool CPatchClientUpdaterBuilder::_ExportServerFile(string strExportPath)
{
	if (strExportPath == "")
	{
		return false;
	}

	vector<string> vServerFileList;
	for (unsigned int uiIndex = 0; uiIndex < m_vCompareVersionList.size(); uiIndex++)
	{
		string strFileName = m_vCompareVersionList[uiIndex];
		map<string, CResourceHelper>::iterator mapIt = m_mapUpdateFileList.find(strFileName);
		if (mapIt != m_mapUpdateFileList.end())
		{
			vServerFileList.push_back(strFileName);
		}
	}

	FILE* pServerFile;
	if (fopen_s(&pServerFile, strExportPath.c_str(),"wb") != 0)
	{
		string strMsg = "PATCHCLIENT:Fail to create server file:" + strExportPath;
		CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
		return false;
	}

	// 写入数据 [1/11/2010 hemeng]
	for (unsigned int uiIndex = 0; uiIndex < vServerFileList.size(); uiIndex++)
	{
		string strFileName = vServerFileList[uiIndex] + "\n";
		if(fwrite(strFileName.c_str(), sizeof(char) * strFileName.size(), 1, pServerFile) != 1)
		{
			string strMsg = "PATCHCLIENT:Fail to write server file:" + strExportPath;
			CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
			return false;
		}

		strFileName = m_strResPath + "\\" + vServerFileList[uiIndex];
		char* pFileVersion = CPatchHelper::GetFileVersion(strFileName);

		if (NULL == pFileVersion)
		{
			string strMsg = "PATCHCLIENT:Fail to get file version:" + strFileName;
			CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
			return false;
		}
		string strFileVersion = string(pFileVersion);
		delete [] pFileVersion;
		pFileVersion = NULL;

		if (uiIndex != vServerFileList.size() - 1)
		{
			strFileVersion += "\n";
		}
		if(fwrite(strFileVersion.c_str(), sizeof(char) * strFileVersion.size(), 1, pServerFile) != 1)
		{
			string strMsg = "PATCHCLIENT:Fail to write server file:" + strExportPath;
			CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
			return false;
		}
	}

	return true;
}
