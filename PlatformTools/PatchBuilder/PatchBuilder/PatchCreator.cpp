﻿#include "StdAfx.h"
#include "PatchCreator.h"
#include <direct.h>


CPatchCreator::CPatchCreator(void)
:	m_bInit(false),m_pPatchLog(CPatchLog::GetInstance())
{
}

CPatchCreator::~CPatchCreator(void)
{
	Destory();
}

CPatchCreator::CPatchCreator(string strSrcDir,string strDesDir, string strPatchFileDir)
:	m_bInit(false),m_pPatchLog(CPatchLog::GetInstance())
{
	if (strSrcDir.empty() || strDesDir.empty() || strPatchFileDir.empty()) 
	{
		return;
	}

	m_bInit = _Init(strSrcDir,strDesDir,strPatchFileDir);
}

bool CPatchCreator::_Init(string strSrcDir, string strDesDir,string strPatchFileDir)
{
	m_strPatchFileDir = strPatchFileDir;

	m_pPatchComparer = new CPatchComparer(strSrcDir,strDesDir,m_pPatchLog);

	if (NULL == m_pPatchComparer)
	{
		return false;
	}

	// load patch creator config file
	if (!_LoadPatchCreatorConfigFile())
	{
		m_pPatchLog->AddLog(PLT_ERROR,"Fail to load patch creator config file");
		return false;
	}

	// create patch releaser config file
	if ( _access_s(strPatchFileDir.c_str(),2) != 0)
	{
		m_pPatchLog->AddLog(PLT_ERROR,"Cannot write on destination directory");
		return false;
	}

	return true;
}

void CPatchCreator::Destory()
{
	if (m_pPatchLog)
	{
		m_pPatchLog = NULL;
	}

	if (m_pPatchComparer)
	{
		delete m_pPatchComparer;
		m_pPatchComparer = NULL;
	}

}

bool CPatchCreator::Create()
{
	if (!m_bInit)
	{
		return false;
	}

	printf("Process compare,please wait......\r\n");
	if(!m_pPatchComparer->CompareDir())
	{
		m_pPatchLog->CreateLogFile(m_strPatchFileDir);
		return false;
	}

	// 创建普通更新包 [1/6/2010 hemeng]
	// 更新数据包 [1/6/2010 hemeng]
	printf("Create patch package,please wait......\r\n");
	string strPackPath = m_strPatchFileDir + "\\" + "Patch" + m_pPatchComparer->GetNewVersion();
	strPackPath += PATCH_DATAPACK_NAME;
	if(!_CreatePatchPack(strPackPath))
	{
		m_pPatchLog->AddLog(PLT_ERROR, "Fail to create update data package");
		m_pPatchLog->CreateLogFile(m_strPatchFileDir);
		return false;
	}

	m_pPatchLog->AddLog(PLT_MSG,"Create update data success");

	string strExeFileName = CPatchHelper::GetCurrWorkDir() + PATCH_RELEASER_FILE_NAME;
	string strNewExeFileName = strPackPath.substr(0,strPackPath.find_last_of(".")) + ".exe";

	if (!_CreatePatchReleaser(strExeFileName,strNewExeFileName))
	{
		m_pPatchLog->CreateLogFile(m_strPatchFileDir);
		return false;
	}

	if (!_UpdateResource(strNewExeFileName,strPackPath))
	{
		m_pPatchLog->AddLog(PLT_ERROR,"Create patch exe fail");		

		m_pPatchLog->CreateLogFile(m_strPatchFileDir);
		return false;
	}
	else
	{
		m_pPatchLog->AddLog(PLT_MSG,"Create patch exe success");
	}

#ifdef _DEBUG
	printf("Create log file,please wait......\r\n");
#endif
	m_pPatchLog->CreateLogFile(m_strPatchFileDir);

	return true;
}

//-------------------------------------------------------------------------------------------------------
bool CPatchCreator::_CreatePatchPack(string strPackPath)
{
	if (!m_bInit || strPackPath.empty())
	{
		return false;
	}

	// 已存在该包 [9/3/2009 hemeng]
	if (_access_s(strPackPath.c_str(),0) == 0)
	{
		if (!DeleteFile(strPackPath.c_str()))
		{
			return false;
		}
	}

	CPackage* pkPatchDataPack = new CPackage();
	if (pkPatchDataPack->OpenPackage(strPackPath,CPackage::EO_WRITE) == false)
	{
		m_pPatchLog->AddLog(PLT_ERROR,"Fail to create patch package");
		return false;
	}

	// 首先将数据包的根目录设置为资源所在目录，从而可以插入数据 [8/12/2009 hemeng]
	string strPackRootDir = m_pPatchComparer->GetDesDirPath();
	strPackRootDir = strPackRootDir.substr(strPackRootDir.find_last_of("\\") + 1);

	pkPatchDataPack->SetPackageRootDir(strPackRootDir);

	string strResultLogPath = strPackPath.substr(0,strPackPath.find_last_of("\\")); 
	bool bSuccess = _CreateAndAddPatchConfig(pkPatchDataPack, strResultLogPath);
	if (!bSuccess)
	{
		pkPatchDataPack->ClosePackage();
		delete pkPatchDataPack;
		pkPatchDataPack = NULL;

		return false;
	}

	// 将数据包的根目录设置为PATCH_ROOT_DIR [8/12/2009 hemeng]
	pkPatchDataPack->SetPackageRootDir(m_strPatchClientRoot);
	
	// Add new files to package [8/18/2009 hemeng]
	vector<string> vAddFileList = m_pPatchComparer->GetResult(PATCH_COMPARE_RESULT_TYPE::PCRT_ADD_FILE);
	for (vector<string>::size_type uiIndex = 0; uiIndex < vAddFileList.size(); uiIndex++)
	{
		string strAddFileName = m_pPatchComparer->GetDesDirPath() + "\\" + vAddFileList[uiIndex];

		if (!strAddFileName.empty() )
		{
			//string strMsg = "添加文件 " + strAddFileName + "\r\n";
			//printf(strMsg.c_str());

			if (!pkPatchDataPack->AddFile(strAddFileName,m_pPatchComparer->GetDesDirPath()))
			{
				string strError = "添加更新文件至更新包失败！ " + strAddFileName; 
				m_pPatchLog->AddLog(PLT_WARNNING,strError);
				continue;
			}			
		}
	}

	// Add update files to package [8/18/2009 hemeng]
	vector<string> vUpdateFileList = m_pPatchComparer->GetResult(PATCH_COMPARE_RESULT_TYPE::PCRT_UPDATE_FILE);
	for (vector<string>::size_type uiIndex = 0; uiIndex < vUpdateFileList.size(); uiIndex++)
	{
		string strUpdateFileName = m_pPatchComparer->GetDesDirPath() + "\\" + vUpdateFileList[uiIndex];

		if (!strUpdateFileName.empty())
		{
			/*string strMsg = "更新文件 " + strUpdateFileName + "\r\n";
			printf(strMsg.c_str());*/

			if (!pkPatchDataPack->AddFile(strUpdateFileName,m_pPatchComparer->GetDesDirPath()))
			{
				string strError = "添加更新文件至更新包失败！ " + strUpdateFileName; 
				m_pPatchLog->AddLog(PLT_WARNNING,strError);
				continue;
			}

		}
	}

	pkPatchDataPack->ClosePackage();
	delete pkPatchDataPack;
	pkPatchDataPack = NULL;

	return true;
}

// 创建配置文件并插入包中 [8/12/2009 hemeng]
bool CPatchCreator::_CreateAndAddPatchConfig(CPackage* pkPackage, string strResLogDir)
{
	if (!m_bInit)
	{
		return false;
	}

	string strFileName = strResLogDir + "\\" + PATCH_RESULT_FILE_NAME;
	if (!m_pPatchComparer->CreateResultLog(strFileName))
	{
		return false;
	}
	

	UINT nBuffSize = GetBufferSize(strFileName);
	BYTE* pBuffer = new BYTE[nBuffSize];

	if (!ReadFileToBuffer(pBuffer,nBuffSize,strFileName.c_str()))
	{
		m_pPatchLog->AddLog(PLT_ERROR,"读取文件比较结果文件失败");
		return false;
	}

	SPackageFileInfo nFileInfo;
	nFileInfo.nFileOrgSize = nBuffSize;
	nFileInfo.nFileSize = nBuffSize;
	nFileInfo.szFilename = PATCH_RESULT_FILE_NAME;
	nFileInfo.szFullPathName = PATCH_RESULT_FILE_NAME;
	nFileInfo.eFileType = FT_XML;
	nFileInfo.eEncryptType = ET_ZIP_HEADER;

	if (!pkPackage->AddFile(nFileInfo,pBuffer,false))
	{
		m_pPatchLog->AddLog(PLT_ERROR,"Fail to add patch config file to package");

		delete [] pBuffer;
		pBuffer = NULL;
		return false;
	}	

	delete [] pBuffer;
	pBuffer = NULL;

	return true;

}

bool CPatchCreator::_LoadPatchCreatorConfigFile()
{
	string strWorkDir = CPatchHelper::GetCurrWorkDir();

	if (strWorkDir.empty())
	{
		m_pPatchLog->AddLog(PLT_ERROR,"Fail to get current work directory");
		return false;
	}

	string strConfigFilePath = strWorkDir + PATCH_CLIENT_ROOT_FILE;

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->LoadFile(strConfigFilePath.c_str());

	TiXmlElement* pElmtRoot = pXmlDoc->RootElement();
	TiXmlElement* pElmt = pElmtRoot->FirstChildElement();

	while (pElmt != NULL)
	{
		const char* pszName = pElmt->Value();

		// 获取包根目录命名 [8/18/2009 hemeng]
		if (strcmp(pszName, "ClientRootDir") == 0)
		{
			m_strPatchClientRoot = pElmt->Attribute("DirName");
		}
				
		pElmt = pElmt->NextSiblingElement();
	}

	return true;
}

bool	CPatchCreator::_CreatePatchReleaser(string strOldName, string strNewName)
{
	
	if (_access_s(strOldName.c_str(),0) != 0)
	{
		string strMsg = "未找到目标可执行文件，更新资源失败 " + strOldName;
		m_pPatchLog->AddLog(PLT_ERROR,strMsg);
		return false;
	}

	if (_access_s(strNewName.c_str(), 0) == 0)
	{
		if (DeleteFile(strNewName.c_str()))
		{
			string strMsg = "删除已存在可执行文件失败，更新可执行文件失败 " + strNewName;
			m_pPatchLog->AddLog(PLT_ERROR,strMsg);
			return false;
		}
	}

	if (!CopyFile(strOldName.c_str(),strNewName.c_str(),FALSE))
	{
		string strMsg = "拷贝可执行文件失败，更新可执行文件失败 " + strNewName;
		m_pPatchLog->AddLog(PLT_ERROR,strMsg);
		return false;
	}

	return true;
}

bool	CPatchCreator::_UpdateResource(string strExeFileName, string strResourceFileName)
{
	if (!m_bInit)
	{
		return false;
	}

	if (strResourceFileName.empty())
	{
		return false;
	}

	// 初始化需要加入exe的所有文件 [8/22/2009 hemeng]

	string strCurrWorkDir = CPatchHelper::GetCurrWorkDir();
	string strPackConfigFileName = strCurrWorkDir + CONFIG_FILE_NAME;
	
	// 更新配置文件 - exe内部资源 [1/4/2010 hemeng]
	CResourceHelper nConfigResHelper(PACK_CONFIG_RES_ID, true);	
	bool bSuccess = nConfigResHelper.DoUpdateResouce(strExeFileName, strPackConfigFileName);

	if (!bSuccess)
	{
		m_pPatchLog->AddLog(PLT_ERROR,"更新配置文件到可执行文件失败");
		return false;
	}

	CResourceHelper nUpdateResHelper(RESOURCE_ID, false);	
	bSuccess = nUpdateResHelper.DoUpdateResouce(strExeFileName, strResourceFileName);
	if (!bSuccess)
	{
		m_pPatchLog->AddLog(PLT_ERROR,"更新资源到可执行文件失败");
		return false;
	}

	return true;
}
