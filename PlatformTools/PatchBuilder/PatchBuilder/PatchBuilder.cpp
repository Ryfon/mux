﻿// PatchBuilder.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "PatchCreator.h"
#include <direct.h>

bool ProcessPatching(string strSrcPath,string strDesPath, string strPatchPackPath)
{
	if (strSrcPath == "" || strDesPath == "" || strPatchPackPath == "")
	{
		return false;
	}

	// 检查文件是否存在 [8/10/2009 hemeng]
	if (_access_s(strSrcPath.c_str(),0) != 0)
	{
		ERROR_PRINT( "更新文件不存在！",strSrcPath.c_str() );
		return false;
	}

	if (_access_s(strDesPath.c_str(),0) != 0)
	{
		ERROR_PRINT( "更新文件不存在！",strDesPath.c_str() );
		return false;
	}

	// 检查是否目录是否存在，不存在则创建 [8/10/2009 hemeng]
	if (_access_s(strPatchPackPath.c_str(),0) !=0)
	{
		if (_mkdir(strPatchPackPath.c_str()) != 0)
		{
			ERROR_PRINT( "更新目录不存在或不可写！",strPatchPackPath.c_str() );
			return false;
		}		
	}

	CPatchCreator* pkCreator = new CPatchCreator(strSrcPath,strDesPath,strPatchPackPath);

	bool bSuccess = pkCreator->Create();

	if (bSuccess == false)
	{
		ERROR_PRINT( "创建更新数据包失败！",strPatchPackPath.c_str() );		
	}

	pkCreator->Destory();

	pkCreator = NULL;

	return bSuccess;
}

bool	ProcessUpdateResource(map<string,UINT> mapResourceList, string strExcutiveFileName)
{
	if (mapResourceList.size() < 1 || strExcutiveFileName == "")
	{
		return false;
	}

	map<string,UINT>::iterator it = mapResourceList.begin();
	for ( ; it != mapResourceList.end(); it++ )
	{
		UINT uiResID = it->second;
		string strResName = it->first;
		CResourceHelper kResHelper(uiResID, false);
		if ( kResHelper.DoUpdateResouce(strExcutiveFileName,strResName) == false)
		{
			ERROR_PRINT( "更新数据失败！",strExcutiveFileName.c_str() );
			return false;
		}

	}

	return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	if ( NULL == argv[1] )
	{
		return 1;
	}
	// commands 第一项是应用程序路径，第二项起为参数 [8/10/2009 hemeng]
	if (argv[1] != "" && argv[2] != "")
	{
		if (strcmp(argv[1],"+") == 0)
		{
			map<string,UINT> mapResourceList;
			string strResourceFileName = argv[2];
			string strExcutiveFileName = argv[3];

			mapResourceList.insert(pair<string,UINT>(strResourceFileName,RESOURCE_ID));

			if (ProcessUpdateResource(mapResourceList,strExcutiveFileName) == false)
			{
				ERROR_PRINT("Update package resource failed!", argv[3]);
				return 0;
			}
		}
		else 
		{
			if (ProcessPatching(argv[1],argv[2],argv[3]) == false)
			{
				ERROR_PRINT("Package failed!",argv[1]);
				return 0;
			}
			
		}

	}


	return 0;
}

