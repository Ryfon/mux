﻿/** @file PatchCreator.h 
@brief 文件打包：patch工具 第一版测试
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-08-06 
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#if	!defined _PATCHCREATOR_H_
#define  _PATCHCREATOR_H_

#pragma once

#include "Package.h"
#include "PatchDef.h"
#include "tinyxml.h"
#include "PatchHelper.h"
#include "PatchLog.h"
#include "PatchComparer.h"
#include "ResourceHelper.h"


class CPatchCreator
{
public:
	CPatchCreator(string strSrcDir,string strDesDir, string strPatchFileDir);

	bool Create();
	
	void Destory();

protected:
	CPatchCreator(void);
	~CPatchCreator(void);

	// 创建数据包 [8/10/2009 hemeng]
	// 记录需要更新或添加的文件数量  [1/7/2010 hemeng]
	bool _CreatePatchPack(string strPackPath);

	// 创建patch client包 [8/14/2009 hemeng]
	bool _CreatePatchClientPack(string strPackPath);

	// 拷贝patchreleaser到指定目录 [1/7/2010 hemeng]
	bool _CreatePatchReleaser(string strOldName, string strNewName);

	// 创建版本号文件 [8/10/2009 hemeng]
	bool _CreateAndAddPatchConfig(CPackage* pkPackage, string strResLogDir);

	bool _Init(string strSrcDir, string strDesDir,string strPatchFileDir);

	// 载入patch创建工具配置文件 [8/14/2009 hemeng]
	bool _LoadPatchCreatorConfigFile();
	
	// 更新资源 [8/21/2009 hemeng]
	bool _UpdateResource(string strExeFileName, string strResourceFileName);	

protected:

	string				m_strPatchFileDir;			// patch包的存放文件夹 [8/14/2009 hemeng]
	string				m_strPatchClientRoot;		// 用于Patch包的根目录 [8/14/2009 hemeng]
	CPatchLog*			m_pPatchLog;				// patch log
	bool				m_bInit;					// patch creator是否已创建完全
	CPatchComparer*		m_pPatchComparer;			// 文件比较器 [8/17/2009 hemeng]
};

#endif