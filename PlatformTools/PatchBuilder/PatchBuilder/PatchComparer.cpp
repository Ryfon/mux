﻿#include "StdAfx.h"
#include "PatchComparer.h"
#include <Winver.h>

CPatchComparer::CPatchComparer(void)
: m_bInitialed(false)
{
	m_pkLog = CPatchLog::GetInstance();
}

CPatchComparer::~CPatchComparer(void)
{
	Destroy();
}

CPatchComparer::CPatchComparer(std::string strSrcDir, std::string strDesDir,CPatchLog* pkLog)
:	m_bInitialed(false)
{
	if (strDesDir.empty() || strSrcDir.empty())
	{
		return;
	}

	CPatchHelper::StringLower(strSrcDir);
	CPatchHelper::StringLower(strDesDir);

	if (NULL == pkLog)
	{
		return;
	}
	m_pkLog = pkLog; 

	m_sSourceDirectory.strThisDir = strSrcDir;
	m_sDestDirectory.strThisDir = strDesDir;

	printf("Initial comparer,please wait......\r\n");

	if (!_LoadPatchCreatorConfigFile())
	{
		return;
	}

	printf("Validate version,please wait......\t\n");
	// 初始化版本号 [8/18/2009 hemeng]
	string strVersionFileName = strSrcDir + "\\" +  CLIENT_VERSION_FILE;
	if (_access_s(strVersionFileName.c_str(),0) != 0)
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:获取源版本号失败，版本文件不存在");
		return;
	}

	string strPathClientFileName = strSrcDir + "\\" + PATCH_CONFIGXML_FILE_NAME;
	m_strSrcVersion = CPatchHelper::GetVersion(strVersionFileName,m_sVersionRule,strPathClientFileName);
	if (m_strSrcVersion.empty())
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:获取源版本号失败");
		return;
	}

	strVersionFileName = strDesDir + "\\" + CLIENT_VERSION_FILE;
	strPathClientFileName = strDesDir + "\\" + PATCH_CONFIGXML_FILE_NAME;
	if (_access_s(strVersionFileName.c_str(),0) != 0)
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:获取目标版本号失败，版本文件不存在");
		return;
	}

	m_strDesVersion = CPatchHelper::GetVersion(strVersionFileName,m_sVersionRule,strPathClientFileName);
	if (m_strDesVersion.empty())
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:获取目标版本号失败");
		return ;
	}

	if (CPatchHelper::CompareVersion(m_strSrcVersion,m_strDesVersion) >= 0 )
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:源版本与目标版本相同或更高，不可创建更新");
		return;
	}

	m_bInitialed = true;	
}

void	CPatchComparer::Destroy()
{
	if (m_pkLog)
	{
		m_pkLog = NULL;
	}

	m_mapCompareResult.clear();

	m_sSourceDirectory.mapDirList.clear();
	m_sSourceDirectory.vFileList.clear();

	m_sDestDirectory.mapDirList.clear();
	m_sDestDirectory.vFileList.clear();

	m_vCheckFiles.clear();
	m_vCompareVersionFile.clear();
}

void	CPatchComparer::ClearResult()
{
	m_mapCompareResult.clear();
}


bool	CPatchComparer::CompareDir()
{
	if (!m_bInitialed)
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER::uninitialed comparer!");
		return false;
	}

	printf("Initial source directory,please wait......\t\n");

	if(!_InitDir(m_sSourceDirectory))
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:初始化源目录失败");
		return false;
	}

	printf("Initial destination directory,please wait......\t\n");
	if(!_InitDir(m_sDestDirectory))
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:初始化目标目录失败");
		return false;
	}


	if (!_CompareDir(m_sSourceDirectory,m_sDestDirectory))
	{
		return false;
	}

	if (!_CompareDir(m_sSourceDirectory,m_sDestDirectory,true))
	{
		return false;
	}

	return true;
}

bool	CPatchComparer::_InitDir(SPatchDirInfo& sDirInfo)
{
	if (sDirInfo.strThisDir == "")
	{
		return false;
	}

	bool bSuccess = true;

	string strFind;
	strFind = sDirInfo.strThisDir;
	CPatchHelper::StringLower(strFind);

	if ( !CPatchHelper::IsRoot(strFind) )
	{
		strFind +="\\";
	}

	strFind += "*.*";

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = FindFirstFile( strFind.c_str(), &FindFileData );

	if ( hFind == INVALID_HANDLE_VALUE )
	{
		return false;
	}

	do 
	{
		//过滤本机目录和父级目录
		if ( FindFileData.cFileName[0] == '.' )
		{
			continue;
		}

		//找到的是目录，则进入目录进行递归
		if ( FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			string szFile;
			if (CPatchHelper::IsRoot(sDirInfo.strThisDir) )
			{
				szFile = sDirInfo.strThisDir + FindFileData.cFileName;
			}
			else
			{
				szFile = sDirInfo.strThisDir + "\\" + FindFileData.cFileName;
			}
			
			SPatchDirInfo newDir;
			CPatchHelper::StringLower(szFile);
			newDir.strThisDir = szFile;
			if (!_InitDir(newDir))
			{
				return false;
			}
			
			sDirInfo.mapDirList.insert(pair<string,SPatchDirInfo>(szFile,newDir));
		}

		//找到的是文件
		else
		{
			string szFile;
			if ( CPatchHelper::IsRoot(sDirInfo.strThisDir) )
			{
				szFile = sDirInfo.strThisDir + FindFileData.cFileName;
			}
			else
			{
				szFile = sDirInfo.strThisDir + "\\" + FindFileData.cFileName;
			}
			CPatchHelper::StringLower(szFile);
			sDirInfo.vFileList.push_back(szFile);

		}

	} while(FindNextFile(hFind, &FindFileData));

	return FindClose(hFind) == TRUE;
}

bool	CPatchComparer::_CompareDirFiles(SPatchDirInfo sSrcDirInfo, SPatchDirInfo sDesDirInfo)
{
	if (!m_bInitialed)
	{
		return false;
	}
	
	// 检查新文件夹中的所有文件旧文件夹是否都有 [8/17/2009 hemeng]
	for (vector<string>::size_type uiIndex = 0; uiIndex < sDesDirInfo.vFileList.size(); uiIndex ++)
	{
		string strDesFileName = sDesDirInfo.vFileList[uiIndex];

		string strSrcFileName = _FindFile(sSrcDirInfo,strDesFileName);

		// 如果没找到则为新添加的文件 [8/17/2009 hemeng]
		if (strSrcFileName.empty())
		{
			string strFileName = _GetRelPath(strDesFileName,false);
			if (strFileName.empty())
			{
				string strError = "CPATCHCOMPARER:获取相对路径出错" + strDesFileName;
				m_pkLog->AddLog(PLT_ERROR,strError);

				return false;
			}

			// 是否为不处理文件 [3/24/2010 hemeng]
			if (_IsExcludeFile(strDesFileName))
			{
				continue;
			}		
			else if (!_AddFileToResource(strFileName,PCRT_ADD_FILE))
			{
				string strError = "CPATCHCOMPARER:添加新增文件出错" + strDesFileName;
				m_pkLog->AddLog(PLT_ERROR,strError);

				return false;
			}
		}
		// 找到相同文件 [8/17/2009 hemeng]
		else
		{
			PATCH_COMPARE_RESULT_TYPE nType = PCRT_NONE;
			
			// 仅当不需要验证版本号，或版本号验证结果为需要更新时比较文件 [1/7/2010 hemeng]
			int iCompareResult = _CompareFile(strSrcFileName,strDesFileName);
			if (iCompareResult == 0)
			{
				nType = PCRT_NONE;
			}
			else if (iCompareResult == 1)
			{
				nType = PCRT_UPDATE_FILE;
			}
			else	// iCompareResult == 2 [8/18/2009 hemeng]
			{
				nType = PCRT_ERROR;
			}

			if (nType != PCRT_ERROR && nType != PCRT_NONE)
			{
				string strFileName = _GetRelPath(strDesFileName,false);
				if (strFileName.empty())
				{
					string strError = "CPATCHCOMPARER:获取相对路径出错" + strDesFileName;
					m_pkLog->AddLog(PLT_ERROR,strError);

					return false;
				}

				if (!_AddFileToResource(strFileName,nType))
				{
					string strError = "CPATCHCOMPARER:添加更新文件出错" + strDesFileName;
					m_pkLog->AddLog(PLT_ERROR,strError);

					return false;
				}
			}
			else if (nType == PCRT_ERROR)
			{
				string strError = "CPATCHCOMPARER:比较文件出错" + strDesFileName;
				m_pkLog->AddLog(PLT_ERROR,strError);

				return false;
			}


		}
	}
	
	// 检查删除的文件 [8/17/2009 hemeng]
	for (vector<string>::size_type uiIndex = 0; uiIndex < sSrcDirInfo.vFileList.size(); uiIndex++)
	{
		string strSrcFileName = sSrcDirInfo.vFileList[uiIndex];
		string strDesFileName = _FindFile(sDesDirInfo,strSrcFileName);

		// 在目标目录中没有源目录中的文件，则为删除文件 [8/17/2009 hemeng]
		if (strDesFileName.empty())
		{
			string strFileName = _GetRelPath(strSrcFileName,true);
			if (strFileName.empty())
			{
				string strError = "CPATCHCOMPARER:添加删除文件出错" + strSrcFileName;
				m_pkLog->AddLog(PLT_ERROR,strError);

				return false;
			}
			
			// 是否为不处理文件 [3/24/2010 hemeng]
			if (_IsExcludeFile(strDesFileName))
			{
				continue;
			}	
			else if (!_AddFileToResource(strFileName,PCRT_DELETE_FILE))
			{
				string strError = "CPATCHCOMPARER:添加删除文件出错" + strDesFileName;
				m_pkLog->AddLog(PLT_ERROR,strError);

				return false;
			}
		}
	}
	
	return true;
}

bool	CPatchComparer::_CompareDir(SPatchDirInfo sSrcDirInfo, SPatchDirInfo sDesDirInfo,bool bOpposite)
{
	if (!m_bInitialed)
	{
		return false;
	}

	// 检查文件是否存在 [8/17/2009 hemeng]
	if(_access_s(sSrcDirInfo.strThisDir.c_str(),0) != 0)
	{		
		string strError = "CPATCHCOMPARER:查找文件夹失败" + sSrcDirInfo.strThisDir;
		m_pkLog->AddLog(PLT_ERROR,strError);
		return false;
	}

	if (_access_s(sDesDirInfo.strThisDir.c_str(),0) != 0)
	{
		string strError = "CPATCHCOMPARER:查找文件夹失败" + sDesDirInfo.strThisDir;
		m_pkLog->AddLog(PLT_ERROR,strError);
		return false;
	}

	//string strMsg = "Compare dir: " + sSrcDirInfo.strThisDir + " and " + sDesDirInfo.strThisDir + " please wait.....\r\n";
	//printf(strMsg.c_str());

	// 正向检查 [8/18/2009 hemeng]
	if (bOpposite == false)
	{
		// 比较目录下所有文件 [8/17/2009 hemeng]
		if (!_CompareDirFiles(sSrcDirInfo,sDesDirInfo))
		{
			return false;
		}

		// 比较目录下的所有目录 [8/17/2009 hemeng]
		for (map<string,SPatchDirInfo>::iterator itDes = sDesDirInfo.mapDirList.begin(); 
			itDes != sDesDirInfo.mapDirList.end(); itDes++)
		{
			map<string, SPatchDirInfo>::iterator itSrc = sSrcDirInfo.mapDirList.begin();
			string strLastDirName = itDes->first;
			strLastDirName = strLastDirName.substr(strLastDirName.find_last_of("\\") + 1);

			for (; itSrc != sSrcDirInfo.mapDirList.end(); itSrc++)
			{
				string strTmp = itSrc->first;
				strTmp = strTmp.substr(strTmp.find_last_of("\\") + 1);

				if (strTmp == strLastDirName)
				{
					break;
				}
			}

			// 没有找到目录，则为新添加的目录 [8/17/2009 hemeng]
			if (itSrc == sSrcDirInfo.mapDirList.end())
			{
				if (!_AddDirFilesToResouce(itDes->second,PCRT_ADD_DIR))
				{
					string strError = "CPATCHCOMPARER:添加新增文件夹出错" + itDes->first;
					m_pkLog->AddLog(PLT_ERROR,strError);

					return false;
				}
			}

			// 找到则递归比较 [8/17/2009 hemeng]
			else
			{
				if (!_CompareDir(itSrc->second,itDes->second))
				{
					string strError = "CPATCHCOMPARER:比较文件夹出错" + itDes->first;
					m_pkLog->AddLog(PLT_ERROR,strError);

					return false;
				}
			}
		}
	}
	
	// 逆向检查需要删除的目录 [8/18/2009 hemeng]	
	else
	{
		// 查找所有需要删除的目录 [8/17/2009 hemeng]
		for (map<string, SPatchDirInfo>::iterator itSrc = sSrcDirInfo.mapDirList.begin();
			itSrc != sSrcDirInfo.mapDirList.end(); itSrc++		 
			)
		{
			map<string, SPatchDirInfo>::iterator itDes = sDesDirInfo.mapDirList.begin();
			string strLastDirName = itSrc->first;
			strLastDirName = strLastDirName.substr(strLastDirName.find_last_of("\\") + 1);

			for (; itDes != sDesDirInfo.mapDirList.end(); itDes++)
			{
				string strTmp = itDes->first;
				strTmp = strTmp.substr(strTmp.find_last_of("\\") + 1);

				if (strTmp == strLastDirName)
				{
					break;
				}
			}

			// 没有找到则为应删除的文件夹 [8/17/2009 hemeng]
			if (itDes == sDesDirInfo.mapDirList.end())
			{
				if (!_AddDirFilesToResouce(itSrc->second,PCRT_DELETE_DIR))
				{
					string strError = "CPATCHCOMPARER:添加删除文件夹出错" + itSrc->first;
					m_pkLog->AddLog(PLT_ERROR,strError);

					return false;
				}
			}

			// 找到则递归 [8/18/2009 hemeng]
			else
			{
				if(!_CompareDir(itSrc->second,itDes->second,true))
				{
					return false;
				}
			}
		}
	
	}
	return true;
}

int	CPatchComparer::_CompareFile(std::string strSrcFilePath, std::string strDesFilePath)
{
	if (strSrcFilePath.empty() || strDesFilePath.empty())
	{
		return 2;
	}

	CPatchHelper::StringLower(strSrcFilePath);
	CPatchHelper::StringLower(strDesFilePath);

	string strMsg = "Compare file: " + strSrcFilePath + " and " + strDesFilePath + " please wait......\r\n";
	string strTimeLog = "Enter Compare file: " + strSrcFilePath + " and " + strDesFilePath;
	m_pkLog->AddLog(PLT_MSG,strTimeLog);
	m_pkLog->IncFileCount(2);

	//printf(strMsg.c_str());

	// 是否为不处理文件 [1/7/2010 hemeng]
	if (_IsExcludeFile(strSrcFilePath))
	{
		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);

		return 0;
	}

	// 是否验证版本号文件 [1/7/2010 hemeng]
	if (_IsCompareVersionFile(strSrcFilePath))
	{
		return _CompareFileVersionInfo(strSrcFilePath,strDesFilePath);
	}


	HANDLE hdSrcFile = CreateFile(strSrcFilePath.c_str(),
									GENERIC_READ , 0, NULL, 
									OPEN_EXISTING, 
									FILE_ATTRIBUTE_NORMAL, NULL);

	if (hdSrcFile == INVALID_HANDLE_VALUE)
	{
		string strError = "CPATCHCOMPARER:打开文件" + strSrcFilePath + "失败";
		m_pkLog->AddLog(PLT_ERROR,strError);

		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);
		return 2;
	}
	
	DWORD dwSrcFileSize = GetFileSize(hdSrcFile, NULL);

	HANDLE hdDesFile = CreateFile(strDesFilePath.c_str(),
									GENERIC_READ , 0, NULL, 
									OPEN_EXISTING, 
									FILE_ATTRIBUTE_NORMAL, NULL);
	if (hdDesFile == INVALID_HANDLE_VALUE)
	{
		string strError = "CPATCHCOMPARER:打开文件" + strDesFilePath + "失败";
		m_pkLog->AddLog(PLT_ERROR,strError);

		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);

		CloseHandle(hdSrcFile);

		return 2;
	}

	DWORD dwDesFileSize = GetFileSize(hdDesFile, NULL);

	// 比较长度先	
	if (dwSrcFileSize != dwDesFileSize)
	{
		CloseHandle(hdSrcFile);
		CloseHandle(hdDesFile);

		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);
		return 1;
	}
	// 大小为0的文件不打入包中，同样不记录到比较中 [8/19/2009 hemeng]
	else if (dwSrcFileSize == 0 && dwSrcFileSize == dwDesFileSize)
	{
		CloseHandle(hdSrcFile);
		CloseHandle(hdDesFile);

		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);
		return 0;		
	}

	// 创建文件映射 [1/1/2010 hemeng]
	HANDLE hdSrcFileMap = CreateFileMapping(hdSrcFile, NULL, PAGE_READONLY, 0, 0, NULL);
	if (hdSrcFileMap == NULL)
	{
		CloseHandle(hdSrcFile);
		CloseHandle(hdDesFile);

		string strError = "CPATCHCOMPARER:创建" + strSrcFilePath + "文件映射失败";
		m_pkLog->AddLog(PLT_ERROR,strError);
		
		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);
		return 2;
	}

	CloseHandle(hdSrcFile);

	PVOID pvSrcFile = MapViewOfFile(hdSrcFileMap, FILE_MAP_READ, 0, 0, 0);
	if (NULL == pvSrcFile)
	{
		CloseHandle(hdSrcFileMap);
		CloseHandle(hdDesFile);

		string strError = "CPATCHCOMPARER:获取" + strSrcFilePath + "文件映射地址失败";
		m_pkLog->AddLog(PLT_ERROR,strError);

		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);
		return 2;
	}

	HANDLE hdDesFileMap = CreateFileMapping(hdDesFile, NULL, PAGE_READONLY, 0, 0, NULL);
	if (hdDesFileMap == NULL)
	{
		UnmapViewOfFile(pvSrcFile);
		CloseHandle(hdSrcFileMap);
		CloseHandle(hdDesFile);

		string strError = "CPATCHCOMPARER:创建" + strDesFilePath + "文件映射失败";
		m_pkLog->AddLog(PLT_ERROR,strError);

		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);
		return 2;
	}
	CloseHandle(hdDesFile);

	PVOID pvDesFile = MapViewOfFile(hdDesFileMap, FILE_MAP_READ, 0, 0, 0);
	if (NULL == pvDesFile)
	{
		UnmapViewOfFile(pvSrcFile);
		CloseHandle(hdSrcFileMap);
		CloseHandle(hdDesFileMap);

		string strError = "CPATCHCOMPARER:获取" + strDesFilePath + "文件映射地址失败";
		m_pkLog->AddLog(PLT_ERROR,strError);

		strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
		m_pkLog->AddLog(PLT_MSG,strTimeLog);
		return 2;
	}


	// 比较内容 [8/18/2009 hemeng]
	bool bSame = true;
	
	bSame = (memcmp(pvSrcFile,pvDesFile,dwSrcFileSize) == 0);

	UnmapViewOfFile(pvSrcFile);
	UnmapViewOfFile(pvDesFile);
	CloseHandle(hdSrcFileMap);
	CloseHandle(hdDesFileMap);

	strTimeLog = "Quit Compare file: " + strSrcFilePath + " and " + strDesFilePath;
	m_pkLog->AddLog(PLT_MSG,strTimeLog);

	if (bSame)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int		CPatchComparer::_CompareFileVersionInfo(string strSrcFilePath, string strDesFilePath)
{
	if (strSrcFilePath.empty() || strDesFilePath.empty() )
	{
		return 2;
	}

	CPatchHelper::StringLower(strSrcFilePath);
	CPatchHelper::StringLower(strDesFilePath);

	/*string strMsg = "Compare file: " + strSrcFilePath + " and " + strDesFilePath + " please wait......\r\n";
	printf(strMsg.c_str());*/

	string strCurWorkDir = CPatchHelper::GetCurrWorkDir();
	if (strCurWorkDir.empty())
	{
		m_pkLog->AddLog(PLT_ERROR,"获取工作目录失败");
		return 2;
	}
	char* pSrcVersionData = CPatchHelper::GetFileVersion(strSrcFilePath);
	if (NULL == pSrcVersionData)
	{	
		m_pkLog->AddLog(PLT_ERROR,"获取版本号失败！");
		//return 2;
	}
	char* pDesVersionData = CPatchHelper::GetFileVersion(strDesFilePath);
	if (NULL == pDesVersionData)
	{
		m_pkLog->AddLog(PLT_ERROR,"获取版本号失败！");
		delete [] pSrcVersionData;
		pSrcVersionData = NULL;

		//return 2;
	}

	// 两个版本号都不存在 [1/12/2010 hemeng]
	if (NULL == pSrcVersionData && NULL == pDesVersionData)
	{
		return 2;
	}
	// 两者有一个存在 [1/12/2010 hemeng]
	else if (NULL == pSrcVersionData && pDesVersionData != NULL)
	{
		delete [] pDesVersionData;
		pDesVersionData = NULL;
		return 1;
	}
	else if (NULL == pDesVersionData && pSrcVersionData != NULL)
	{
		delete [] pSrcVersionData; 
		pSrcVersionData = NULL;
		return 1;
	}

	// 两者都存在 [1/12/2010 hemeng]
	bool bSame = (memcmp(pSrcVersionData,pDesVersionData,MAX_PATH) == 0);

	delete [] pSrcVersionData;
	pSrcVersionData = NULL;
	delete pDesVersionData;
	pDesVersionData = NULL;

	if (bSame)
	{
		return 0;
	}
	else
	{
		return 1;
	}	
}


bool	CPatchComparer::_AddDirFilesToResouce(SPatchDirInfo sDirInfo, PATCH_COMPARE_RESULT_TYPE nCompareType)
{
	if (sDirInfo.strThisDir == "")
	{
		return false;
	}

	map<PATCH_COMPARE_RESULT_TYPE,vector<string>>::iterator itThis = m_mapCompareResult.find(nCompareType);

	if (itThis == m_mapCompareResult.end())
	{
		vector<string> vTmp;
		m_mapCompareResult.insert(pair<PATCH_COMPARE_RESULT_TYPE,vector<string>>(nCompareType,vTmp));

		itThis = m_mapCompareResult.find(nCompareType);

		if (itThis == m_mapCompareResult.end())
		{
			m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:创建类型列表失败");
			return false;
		}
	}

	if (nCompareType == PCRT_DELETE_DIR)
	{
		string strRelDirPath = _GetRelPath(sDirInfo.strThisDir,true);
		if (strRelDirPath.empty())
		{
			return false;
		}
		itThis->second.push_back(strRelDirPath);
	}
	else if (nCompareType == PCRT_ADD_DIR)
	{
		// 添加所有目录中的文件 [8/17/2009 hemeng]
		// 创建vector [8/17/2009 hemeng]
		vector<string>	vFiles;

		for (unsigned int uiIndex = 0; uiIndex < sDirInfo.vFileList.size(); uiIndex++)
		{
			string strFullFilePath = sDirInfo.vFileList[uiIndex];
			// 添加文件，相对路径与des [8/17/2009 hemeng]
			string strFileName = _GetRelPath(strFullFilePath,false);
			if (strFileName.empty())
			{
				string strError = "CPATCHCOMPARER:获取文件相对路径出错" + strFullFilePath;
				m_pkLog->AddLog(PLT_ERROR,strError);

				return false;
			}
			if (!_AddFileToResource(strFileName,PCRT_ADD_FILE))
			{
				string strError = "CPATCHCOMPARER:添加新增文件出错" + strFullFilePath;
				m_pkLog->AddLog(PLT_ERROR,strError);

				return false;
			}
					
		}

		// 递归处理目录中的所有目录 [8/17/2009 hemeng]
		for(map<string,SPatchDirInfo>::iterator itSub = sDirInfo.mapDirList.begin(); itSub != sDirInfo.mapDirList.end(); itSub++)
		{
			SPatchDirInfo sSubDirInfo = itSub->second;
			if(!_AddDirFilesToResouce(sSubDirInfo,nCompareType))
			{
				string strError = "CPATCHCOMPARER:添加新增文件夹出错" + sSubDirInfo.strThisDir;
				m_pkLog->AddLog(PLT_ERROR,strError);

				return false;
			}
		}
	}

	

	return true;
}

bool	CPatchComparer::_AddFileToResource(string strFilePath, PATCH_COMPARE_RESULT_TYPE nCompareType)
{
	if(strFilePath.empty())
	{
		return false;
	}

	CPatchHelper::StringLower(strFilePath);

	/*if (_IsExcludeFile(strFilePath))
	{
		return true;
	}*/

	string strTmp = strFilePath.substr(strFilePath.find_last_of("\\") + 1);
	string strLog = "Add fill to resource:" + strTmp;
	m_pkLog->AddLog(PLT_MSG,strLog);
	

	// 对于等于0K的不操作 [8/19/2009 hemeng]
	string strAbsFilePath; 

	if (nCompareType == PCRT_DELETE_FILE || nCompareType == PCRT_DELETE_DIR)
	{
		strAbsFilePath = m_sSourceDirectory.strThisDir + "\\" + strFilePath;
	}
	else
	{
		strAbsFilePath = m_sDestDirectory.strThisDir + "\\" + strFilePath;
	}

	HANDLE hdFile = CreateFile(strAbsFilePath.c_str(),GENERIC_READ , 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hdFile == INVALID_HANDLE_VALUE)
	{
		string strMsg = "打开文件" + strAbsFilePath + "失败";
		m_pkLog->AddLog(PLT_ERROR,strMsg);
		return true;
	}

	DWORD dwFileSize = GetFileSize(hdFile, NULL);
	CloseHandle(hdFile);

	if (dwFileSize == 0)
	{
		return true;
	}

	if (nCompareType == PCRT_NONE || nCompareType == PCRT_ERROR)
	{
		return true;
	}

	map<PATCH_COMPARE_RESULT_TYPE,vector<string>>::iterator itThis = m_mapCompareResult.find(nCompareType);

	if (itThis == m_mapCompareResult.end())
	{
		vector<string> vTmp;

		m_mapCompareResult.insert(pair<PATCH_COMPARE_RESULT_TYPE,vector<string>>(nCompareType,vTmp));

		itThis = m_mapCompareResult.find(nCompareType);

		if (itThis == m_mapCompareResult.end())
		{
			m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:创建类型列表失败");
			return false;
		}
	}

	itThis->second.push_back(strFilePath);

	return true;
}

string	CPatchComparer::_FindFile(SPatchDirInfo sDirInfo,string strFileName)
{
	if (sDirInfo.strThisDir.empty() || strFileName.empty())
	{
		return "";
	}

	CPatchHelper::StringLower(strFileName);

	string strOnlyFileName = strFileName.substr(strFileName.find_last_of("\\") + 1);

	// 首先在根目录找 [8/17/2009 hemeng]
	for (vector<string>::size_type uiIndex = 0; uiIndex < sDirInfo.vFileList.size(); uiIndex++)
	{
		// 截取文件名 [8/17/2009 hemeng]
		string strTmp = sDirInfo.vFileList[uiIndex];
		strTmp = strTmp.substr(strTmp.find_last_of("\\") + 1);

		if (strTmp == strOnlyFileName)
		{
			return sDirInfo.vFileList[uiIndex];
		}
	}

	return "";
}

string CPatchComparer::_GetRelPath(std::string strFilePath, bool bSource)
{
	if (strFilePath .empty())
	{
		return "";
	}

	CPatchHelper::StringLower(strFilePath);

	string strResult = "";
	string strTmp = "";
	size_t nLength = 0;

	// 相对于源目录 [8/17/2009 hemeng]
	if (bSource)
	{
		strTmp = m_sSourceDirectory.strThisDir;
		nLength = strTmp.size();		
	}
	else
	{
		strTmp = m_sDestDirectory.strThisDir;
		nLength = strTmp.size();		
	}

	strResult = strFilePath.substr(strFilePath.find(strTmp) + nLength + 1);

	return strResult;
}

bool CPatchComparer::CreateResultLog(string strFileName)
{
	if (!m_bInitialed)
	{
		return false;
	}

	if (strFileName.empty())
	{
		return false;
	}

	//插入Xml声明
	TiXmlDocument* pXmlDoc = new TiXmlDocument(strFileName.c_str());
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	pXmlDoc->InsertEndChild(*pXmlDec);

	// 插入版本号信息 [8/18/2009 hemeng]
	TiXmlElement* pXmlChildElement = new TiXmlElement("Version");
	pXmlChildElement->SetAttribute("OldVersion",m_strSrcVersion.c_str());
	pXmlChildElement->SetAttribute("NewVersion",m_strDesVersion.c_str());
	pXmlDoc->LinkEndChild(pXmlChildElement);

	// 写入校验文件名称 [9/2/2009 hemeng]
	TiXmlElement* pXmlCheckElement = new TiXmlElement("CheckFile");
	for (vector<string>::size_type uiIndex = 0; uiIndex < m_vCheckFiles.size(); uiIndex++)
	{
		TiXmlElement* pXmlCheckChildElm = new TiXmlElement("File");

		pXmlCheckChildElm->SetAttribute("FileName",m_vCheckFiles[uiIndex].c_str());

		if (NULL == pXmlCheckChildElm)
		{
			return NULL;
		}
		pXmlCheckElement->LinkEndChild(pXmlCheckChildElm);
	}
	pXmlDoc->LinkEndChild(pXmlCheckElement);

	// 将更新信息写入Log文件 [9/2/2009 hemeng]
	for (map<PATCH_COMPARE_RESULT_TYPE,vector<string>>::iterator it = m_mapCompareResult.begin(); it != m_mapCompareResult.end(); it++)
	{
		// 类型包括 [8/18/2009 hemeng]
		//	PCRT_ADD_FILE		记录
		//	PCRT_DELETE_FILE	记录
		//	PCRT_UPDATE_FILE	记录
		//	PCRT_ADD_DIR		不记录，以记录添加文件方式
		//	PCRT_DELETE_DIR		记录
		//	PCRT_NONE			不记录
		//	PCRT_ERROR			不记录
		if (it->first == PCRT_ADD_DIR || it->first == PCRT_NONE || it->first == PCRT_ERROR)
		{
			continue;
		}
		TiXmlElement* pXmlEle = _GetAsXmlElement(it->first);

		if(pXmlEle == NULL)
		{
			m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:写入patch创建信息失败");
			return false;
		}
		//插入根节点
		pXmlDoc->LinkEndChild(pXmlEle);
	}

	return pXmlDoc->SaveFile(strFileName.c_str());

}

TiXmlElement*	CPatchComparer::_GetAsXmlElement(PATCH_COMPARE_RESULT_TYPE nType)
{
	if (nType == PCRT_ERROR)
	{
		return NULL;
	}

	map<PATCH_COMPARE_RESULT_TYPE,vector<string>>::iterator it = m_mapCompareResult.find(nType);

	if (it == m_mapCompareResult.end())
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:未找到指定类型");
		return NULL;
	}

	string strType = "";

	// 类型包括 [8/18/2009 hemeng]
	//	PCRT_ADD_FILE		记录
	//	PCRT_DELETE_FILE	记录
	//	PCRT_UPDATE_FILE	记录
	//	PCRT_ADD_DIR		不记录，以记录添加文件方式
	//	PCRT_DELETE_DIR		记录
	//	PCRT_NONE			不记录

	if (nType == PCRT_ADD_FILE)
	{
		strType = "AddFileList";
	}
	else if (nType == PCRT_DELETE_FILE)
	{
		strType = "DeleteFileList";
	}
	else if (nType == PCRT_UPDATE_FILE)
	{
		strType = "UpdateFileList";
	}
	else if (nType == PCRT_DELETE_DIR)
	{
		strType = "DeleteDirList";
	}

	TiXmlElement* pXmlRootElement = new TiXmlElement(strType.c_str());

	for (unsigned int uiIndex = 0; uiIndex < it->second.size(); uiIndex++)
	{
		string strFileName = it->second[uiIndex];
		TiXmlElement* pXmlChildElement = new TiXmlElement("File");

		pXmlChildElement->SetAttribute("FileName",strFileName.c_str());

		if (NULL == pXmlChildElement)
		{
			return NULL;
		}
		pXmlRootElement->LinkEndChild(pXmlChildElement);
	}

	return pXmlRootElement;
}

vector<string> CPatchComparer::GetResult(PATCH_COMPARE_RESULT_TYPE nType)
{
	vector<string> vResult;
	if (!m_bInitialed)
	{
		return vResult;
	}

	map<PATCH_COMPARE_RESULT_TYPE,vector<string>>::iterator it = m_mapCompareResult.find(nType);

	if (it == m_mapCompareResult.end())
	{
		return vResult;
	}
	else
	{
		return it->second;
	}

	return vResult;
}

bool	CPatchComparer::_LoadPatchCreatorConfigFile()
{
	string strWorkDir = CPatchHelper::GetCurrWorkDir();

	if (strWorkDir.empty())
	{
		m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:Fail to get current work directory");
		return false;
	}

	string strConfigFilePath = strWorkDir + PATCH_CLIENT_ROOT_FILE;

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->LoadFile(strConfigFilePath.c_str());

	TiXmlElement* pElmtRoot = pXmlDoc->RootElement();
	TiXmlElement* pElmt = pElmtRoot->FirstChildElement();

	while (pElmt != NULL)
	{
		const char* pszName = pElmt->Value();

		if (strcmp(pszName, "ExcludeFile") == 0)
		{
			if (!_LoadExcludeFile(pElmt,m_mapExcludeFile))
			{
				m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:Fail to load exclude file list");
				return false;
			}
		}
		else if (strcmp(pszName,"VersionRule") == 0)
		{
			// 处理版本号 [9/2/2009 hemeng]
			if (!CPatchHelper::LoadVersionRule(pElmt,m_sVersionRule))
			{
				m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:初始化版本号规则列表出错");
				return false;
			}
		}
		else if (strcmp(pszName,"CheckFile") == 0)
		{
			if (!_LoadCheckFile(pElmt,m_vCheckFiles))
			{
				m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:初始化目录校验列表出错");
				return false;
			}
		}
		else if (strcmp(pszName, "CompareVersion") == 0)
		{
			if (!_LoadCompareVersionFile(pElmt, m_vCompareVersionFile))
			{
				m_pkLog->AddLog(PLT_ERROR,"CPATCHCOMPARER:初始化版本号校验列表出错");
				return false;
			}
		}
	
		pElmt = pElmt->NextSiblingElement();
	}

	return true;
}

bool	CPatchComparer::_LoadExcludeFile(TiXmlElement* pElmt,map<string,vector<string>>& mapExcludeFiles)
{
	if (NULL == pElmt)
	{
		return false;
	}

	TiXmlElement* pChildElmt = pElmt->FirstChildElement();
	if (NULL == pChildElmt)
	{
		return false;
	}

	while (pChildElmt  != NULL)
	{
		string strDirName = pChildElmt->Attribute("FileDir");
		CPatchHelper::StringLower(strDirName);
		string strFileName = pChildElmt->Attribute("FileName");
		CPatchHelper::StringLower(strFileName);
		
		map<string,vector<string>>::iterator it = mapExcludeFiles.find(strDirName);

		if (it == mapExcludeFiles.end())
		{
			vector<string> vTmp;
			vTmp.push_back(strFileName);
			mapExcludeFiles.insert(pair<string,vector<string>>(strDirName,vTmp));
		}
		else
		{
			it->second.push_back(strFileName);
		}

		pChildElmt = pChildElmt->NextSiblingElement();
	}
	
	return true;
}

bool CPatchComparer::_LoadCheckFile(TiXmlElement* pElmt,vector<string>& vCheckFiles)
{
	if (NULL == pElmt)
	{
		return false;
	}

	TiXmlElement* pChildElmt = pElmt->FirstChildElement();
	if (NULL == pChildElmt)
	{
		return false;
	}

	while (pChildElmt  != NULL)
	{
		string strFileName = pChildElmt->Attribute("FileName");
		CPatchHelper::StringLower(strFileName);
		vCheckFiles.push_back(strFileName);

		pChildElmt = pChildElmt->NextSiblingElement();
	}

	return true;

}

bool CPatchComparer::_LoadCompareVersionFile(TiXmlElement* pElmt,vector<string>& vFilesList)
{
	if (NULL == pElmt)
	{
		return false;
	}

	TiXmlElement* pChildElmt = pElmt->FirstChildElement();
	if (NULL == pChildElmt)
	{
		return false;
	}

	while (pChildElmt  != NULL)
	{
		string strFileName = pChildElmt->Attribute("FileName");
		CPatchHelper::StringLower(strFileName);
		vFilesList.push_back(strFileName);

		pChildElmt = pChildElmt->NextSiblingElement();
	}

	return true;
}

bool	CPatchComparer::_IsExcludeFile(string strFileAbsPath)
{
	if (!m_bInitialed)
	{
		return true;
	}

	if (strFileAbsPath == "")
	{
		return true;
	}

	CPatchHelper::StringLower(strFileAbsPath);

	// 检查路径中是否含有指定目录 [8/19/2009 hemeng]

	map<string,vector<string>>::iterator it = m_mapExcludeFile.begin();

	for (;it != m_mapExcludeFile.end(); it++)
	{
		string strDirName = it->first;
		
		vector<string> vExcludeFileName = it->second;

		for (vector<string>::size_type uiIndex = 0; uiIndex < vExcludeFileName.size(); uiIndex++)
		{
			string strExcludeFileNanme = vExcludeFileName[uiIndex];
			// 特定类型文件 [9/2/2009 hemeng]
			if (strExcludeFileNanme.find("*") != string::npos)
			{
				strExcludeFileNanme = strExcludeFileNanme.substr(strExcludeFileNanme.find("*") + 1);				
			}

			string strFileName = strFileAbsPath.substr(strFileAbsPath.find_last_of("\\") + 1);
			if (strDirName == "muxall")
			{
				if (strFileName.find(strExcludeFileNanme) != string::npos)
				{
					return true;
				}
			}
			else
			{
				string strDirPath = strFileAbsPath.substr(0,strFileAbsPath.find_last_of("\\"));
				if (strDirPath.find(strDirName) != string::npos)
				{
					if(strFileName.find(strExcludeFileNanme) != string::npos)
					{
						return true;
					}
				}				
			}
			
		}		
	}

	return false;
}


bool CPatchComparer::_IsCompareVersionFile(string strFileAbsPath)
{
	for (unsigned int uiIndex = 0; uiIndex < m_vCompareVersionFile.size(); uiIndex++)
	{
		if (strFileAbsPath.find(m_vCompareVersionFile[uiIndex]) != string::npos)
		{
			return true;
		}
	}

	return false;
}