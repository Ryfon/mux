﻿#if	!defined _PATCHCOMPARER_H_
#define _PATCHCOMPARER_H_

/** @file PatchCompare.h 
@brief 差异文件查找类
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2009-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#pragma once
#include "PatchDef.h"
#include "PatchHelper.h"
#include "PatchLog.h"
#include "tinyxml.h"

// 比较类型 [8/17/2009 hemeng]
enum PATCH_COMPARE_RESULT_TYPE
{
	PCRT_ADD_FILE = 0,
	PCRT_DELETE_FILE,
	PCRT_UPDATE_FILE,
	PCRT_ADD_DIR,
	PCRT_DELETE_DIR,
	PCRT_NONE,
	PCRT_ERROR,
};

struct SPatchDirInfo 
{
	string						strThisDir;		// 本目录的名称[8/17/2009 hemeng]
	map<string,SPatchDirInfo>	mapDirList;		// 本目录下的所有目录 [8/17/2009 hemeng]
	vector<string>				vFileList;		// 本目录下的文件 [8/17/2009 hemeng]	
};

class CPatchComparer
{
public:
	CPatchComparer(string strSrcDir, string strDesDir,CPatchLog* pkLog);
	~CPatchComparer(void);

	// 比较目录下的所有文件 [8/17/2009 hemeng]
	bool	CompareDir();

	void	ClearResult();

	// 获取指定结果文件列表 [8/17/2009 hemeng]
	vector<string>	GetResult(PATCH_COMPARE_RESULT_TYPE nType);

	// 创建指定类型的列表文件到指定目录 [8/17/2009 hemeng]
	bool	CreateResultLog(string strFileName);

	// 获取版本号 [8/18/2009 hemeng]
	string	GetOldVersion()	{return m_strSrcVersion;};
	string	GetNewVersion()	{return m_strDesVersion;};

	// 获取目标目录路径 [8/18/2009 hemeng]
	string	GetDesDirPath()	{return m_sDestDirectory.strThisDir;};
	string	GetSrcDirPath()	{return m_sSourceDirectory.strThisDir;};

protected:
	CPatchComparer(void);
	
	// 清理 [8/17/2009 hemeng]
	void	Destroy();

	// 读取配置文件 [8/19/2009 hemeng]
	bool	_LoadPatchCreatorConfigFile();
	bool	_LoadExcludeFile(TiXmlElement* pElmt,map<string,vector<string>>& mapExcludeFiles);
	bool	_LoadCheckFile(TiXmlElement* pElmt,vector<string>& vCheckFiles);
	bool	_LoadCompareVersionFile(TiXmlElement* pElmt,vector<string>& vFilesList);

	bool	_InitDir(SPatchDirInfo& sDirInfo);

	// 比较目录	递归调用 [8/17/2009 hemeng]
	// bOpposite 是否反向检查（检查删除的文件）
	bool	_CompareDir(SPatchDirInfo sSrcDirInfo, SPatchDirInfo sDesDirInfo,bool bOpposite = false);

	// 比较目录下所有文件 [8/17/2009 hemeng]
	bool	_CompareDirFiles(SPatchDirInfo sSrcDirInfo, SPatchDirInfo sDesDirInfo);

	// 比较文件 [8/17/2009 hemeng]
	// 比较文件内容,相同返回0，不同1，严重错误2
	int		_CompareFile(string strSrcFilePath, string strDesFilePath);

	// 将目录下的所有文件添加至指定结果中 [8/17/2009 hemeng]
	bool	_AddDirFilesToResouce(SPatchDirInfo sDirInfo, PATCH_COMPARE_RESULT_TYPE nCompareType);

	// 添加文件到指定结果中 [8/17/2009 hemeng]
	// 文件路径必须为相对路径 [8/17/2009 hemeng]
	bool	_AddFileToResource(string strFilePath, PATCH_COMPARE_RESULT_TYPE nCompareType);

	// 在目录中查找指定文件，没有则返回"" [8/17/2009 hemeng]
	string	_FindFile(SPatchDirInfo strDirInfo,string strFileName);	

	// 获取相对路径 [8/17/2009 hemeng]
	// 对文件或目录 [8/17/2009 hemeng]
	// bSource标志是相对于源还是目标目录 [8/17/2009 hemeng]
	string _GetRelPath(string strFilePath,bool bSource);

	// 创建指定类型xml节点 [8/18/2009 hemeng]
	TiXmlElement*	_GetAsXmlElement(PATCH_COMPARE_RESULT_TYPE nType);

	// 是否为例外文件 [8/19/2009 hemeng]
	bool	_IsExcludeFile(string strFileAbsPath);
	
	// 是否为比较版本号的文件 [1/7/2010 hemeng]
	bool	_IsCompareVersionFile(string strFileAbsPath);

	// 比较文件版本号 [12/15/2009 hemeng]
	// 相同返回0，不同1，严重错误2
	int		_CompareFileVersionInfo(string strSrcFilePath, string strDesFilePath);

protected:
	bool						m_bInitialed;				// 是否初始化完成 [8/17/2009 hemeng]
	SPatchDirInfo				m_sSourceDirectory;			// 比较源目录 [8/17/2009 hemeng]
	SPatchDirInfo				m_sDestDirectory;			// 比较目标目录 [8/17/2009 hemeng]

	// 比较结果 以结果类为key [8/17/2009 hemeng]
	// 存放的全部为相对路径 [8/17/2009 hemeng]
	map<PATCH_COMPARE_RESULT_TYPE,vector<string>>	m_mapCompareResult;		

	CPatchLog*					m_pkLog;					// log文件 [8/17/2009 hemeng]
	string						m_strSrcVersion;			// 旧版本号 [8/18/2009 hemeng]
	string						m_strDesVersion;			// 新版本号
	map<string,vector<string>>	m_mapExcludeFile;			// 不进行比较的指定文件夹下的文件类型 [8/19/2009 hemeng]
	vector<string>				m_vCompareVersionFile;		// 验证版本号的文件 [1/7/2010 hemeng]
	SPatchVersionRule			m_sVersionRule;				// 版本号规则 [9/17/2009 hemeng]
	vector<string>				m_vCheckFiles;				// 更新时需要验证是否是客户端目录 [9/2/2009 hemeng]
};

#endif
