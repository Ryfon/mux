﻿#pragma once
#include "tinyxml.h"
#include <string>
#include <map>
#include "PatchLog.h"

using namespace std;

#define USER_RESOURCE_DATAID                     130

class CPatchClientReleaser
{
public:
	CPatchClientReleaser(string strReleasePath, unsigned int uiLauncherID);
	~CPatchClientReleaser(void);

	bool	ReleaseResource();

protected:
	CPatchClientReleaser();
	bool	_Init();
	bool	_LoadResourceList();
	void*	_LoadInnerResource(unsigned int uiResID, DWORD& dwResSize);

protected:
	bool	m_bInit;
	string	m_strReleasePath;
	map<string, unsigned int>	m_mapRes;		// 资源列表 [1/11/2010 hemeng]
	bool	m_bStartupLauncher;					// 是否启动launcher [1/11/2010 hemeng]
	unsigned int	m_uiLauncherID;				// launcher的线程ID [1/11/2010 hemeng]
	string			m_strLauncherName;
};
