﻿#include "StdAfx.h"
#include "PatchClientReleaser.h"
#include "PatchDef.h"
#include <direct.h>
#include <Psapi.h>

CPatchClientReleaser::CPatchClientReleaser(void)
:	m_bInit(false),
	m_bStartupLauncher(false),
	m_uiLauncherID(0)
{
}

CPatchClientReleaser::CPatchClientReleaser(string strReleasePath, unsigned int uiLauncherID)
:	m_bInit(false),
	m_bStartupLauncher(false),
	m_uiLauncherID(0)
{
	m_strReleasePath = strReleasePath;
	m_uiLauncherID = uiLauncherID;	

	m_bInit = _Init();
}

CPatchClientReleaser::~CPatchClientReleaser(void)
{
	m_mapRes.clear();

	// 重启Lanucher [1/11/2010 hemeng]
	if (m_bStartupLauncher && !m_strLauncherName.empty())
	{
		if (_access_s(m_strLauncherName.c_str(),0) == 0)
		{
			string strMsg = "Release Destroy - Start launcher.exe "; 
			CPatchLog::GetInstance()->AddLog(PLT_MSG,strMsg);
			STARTUPINFO si;
			PROCESS_INFORMATION pi;
			memset(&si, 0, sizeof(si));
			si.cb = sizeof(si);
			memset(&pi, 0, sizeof(pi));
			if (CreateProcess(m_strLauncherName.c_str(), "", NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi))
			{
				CloseHandle(pi.hThread);
				CloseHandle(pi.hProcess);
			}
		}
	}

	// 输出更新日志 [1/11/2010 hemeng]
	string strLogFileName = m_strReleasePath + "\\" +  PATCH_DOWNLOAD_DIR;

	if (_access_s(strLogFileName.c_str(), 0) != 0 )
	{
		if(_mkdir(strLogFileName.c_str()) != 0)
		{
			return;
		}
	}
	if(!CPatchLog::GetInstance()->CreateLogFile(strLogFileName))
	{
		PATCH_MEG_REPROT("Fail to create log",strLogFileName.c_str());
	}
}

bool CPatchClientReleaser::_Init()
{
	HANDLE hdLauncher = CreateMutex(
		NULL,
		TRUE,
		"MUX_LAUNCHER"
		);

	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		#ifdef DEBUG_PATCHCLIENTUPDATER
		PATCH_ERROR_REPROT("更新失败！请关闭客户端再更新！","muxlauncher.exe");
		CloseHandle(hdLauncher);
		return false;

		#else
		// 正式代码 [1/11/2010 hemeng]
		if (m_uiLauncherID == 0)
		{
			PATCH_ERROR_REPROT("更新失败！获取窗口句柄失败！","muxlauncher.exe");
			CloseHandle(hdLauncher);
			return false;
		}
		HANDLE hd = OpenProcess(PROCESS_ALL_ACCESS,FALSE,m_uiLauncherID);
		char szBuf[MAX_PATH];
		GetModuleFileNameEx(hd, NULL, szBuf, MAX_PATH);
		m_strLauncherName = szBuf;

		if (hd)
		{
			TerminateProcess(hd,-1);
			WaitForSingleObject(hd,INFINITE);
			CloseHandle(hd);
			// 停留1秒 [9/7/2009 hemeng]
			Sleep(3000);
			m_bStartupLauncher = true;						
		}
		#endif	// end define DEBUG_PATCHCLIENTUPDATER  [1/12/2010 hemeng]
	}

	CloseHandle(hdLauncher);
	

	if (!_LoadResourceList())
	{
		string strMsg = "Fail to load resource list";
		CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
		return false;
	}
	
	return true;
}

void*	CPatchClientReleaser::_LoadInnerResource(unsigned int uiResID,DWORD& dwResSize)
{
	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC	hRC = FindResource(hModule,MAKEINTRESOURCE(uiResID),RT_RCDATA);

	if (NULL == hRC)
	{
		string strMsg = "Fail to find inner resource";
		CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
		return NULL;
	}

	dwResSize = SizeofResource(hModule,hRC);

	HGLOBAL hGlb = LoadResource(hModule, hRC);
	if (NULL == hGlb)
	{
		string strMsg = "Fail to find inner resource";
		CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
		return NULL;
	}

	return LockResource(hGlb);
}

bool	CPatchClientReleaser::_LoadResourceList()
{
	DWORD dwResSize = 0;
	void* pListBuffer = _LoadInnerResource(USER_RESOURCE_DATAID, dwResSize);
	if (NULL == pListBuffer)
	{
		return false;
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	if (NULL == pXmlDoc)
	{
		FreeResource((HGLOBAL)pListBuffer);
		pListBuffer = NULL;
		return false;
	}

	pXmlDoc->Parse( (char*)pListBuffer );
	TiXmlElement* pRootElement = pXmlDoc->RootElement();
	if (NULL == pRootElement)
	{
		FreeResource((HGLOBAL)pListBuffer);
		pListBuffer = NULL;

		return false;
	}

	TiXmlElement* pChildElement = pRootElement->FirstChildElement();
	if (NULL == pChildElement)
	{
		FreeResource((HGLOBAL)pListBuffer);
		pListBuffer = NULL;

		return false;
	}

	while (pChildElement != NULL)
	{
		const char* pszName = pChildElement->Value();
		if (strcmp(pszName, "File") == 0)
		{
			string strFileName = pChildElement->Attribute("FileName");
			unsigned int uiResID = pChildElement->IntAttribute("ResourceID");

			m_mapRes.insert(pair<string, unsigned int>(strFileName, uiResID));
		}
		pChildElement = pChildElement->NextSiblingElement();
	}

	// 释放资源 [1/11/2010 hemeng]
	FreeResource((HGLOBAL)pListBuffer);
	pListBuffer = NULL;
	return true;
}

bool	CPatchClientReleaser::ReleaseResource()
{
	if (!m_bInit)
	{
		return false;
	}

	string strMsg = "Start patch launcher resource......";
	CPatchLog::GetInstance()->AddLog(PLT_MSG, strMsg);

	map<string, unsigned int>::iterator mapIt = m_mapRes.begin();
	for (; mapIt != m_mapRes.end(); mapIt ++)
	{
		string strFileName = mapIt->first;
		strFileName = m_strReleasePath + "\\" + strFileName;
		strMsg = "Update launcher file:" + strFileName;
		CPatchLog::GetInstance()->AddLog(PLT_MSG,strMsg);
		
		DWORD dwResSize = 0;
		void* pvResData = _LoadInnerResource(mapIt->second, dwResSize);
		if (NULL == pvResData)
		{
			strMsg = "Fail to load resource:" + strFileName;
			CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);

			return false;
		}

		// 如果文件已存在则删除文件 [1/12/2010 hemeng]
		if (_access_s(strFileName.c_str(),0) == 0)
		{
			if(!DeleteFile(strFileName.c_str()))
			{
				strMsg = "Fail to delete old resource:" + strFileName;
				CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);

				return false;
			}
		}


		HANDLE hdFile = CreateFile(strFileName.c_str(), FILE_ALL_ACCESS, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (INVALID_HANDLE_VALUE == hdFile)
		{
			strMsg = "Fail to create resource:" + strFileName;
			CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);
	
			FreeResource(pvResData);
			pvResData = NULL;

			return false;
		}

		DWORD dwTmp = 0;
		if (WriteFile(hdFile, pvResData, dwResSize, &dwTmp, NULL) == false)
		{
			strMsg = "Fail to write resource:" + strFileName;
			CPatchLog::GetInstance()->AddLog(PLT_ERROR, strMsg);

			FreeResource(pvResData);
			pvResData = NULL;

			return false;
		}

		CloseHandle(hdFile);

		FreeResource(pvResData);
		pvResData = NULL;

		CPatchLog::GetInstance()->IncFileCount();
	}
	
	return true;
}

//void*	CPatchClientReleaser::_LoadResource(CResourceHelper* pkResHelper)
//{
//	if (NULL == pkResHelper)
//	{
//		return NULL;
//	}
//	
//	char pThisFileName[MAX_PATH];
//	GetModuleFileName(NULL,pThisFileName,MAX_PATH);
//	HANDLE hdThisFile = CreateFile(pThisFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
//	if (hdThisFile == INVALID_HANDLE_VALUE)
//	{
//		CPatchLog::GetInstance()->AddLog(PLT_ERROR,"Fail to open file during mapping exe");
//		return NULL;
//	}
//
//	HANDLE hThisFileMapping = CreateFileMapping(hdThisFile, NULL, PAGE_READONLY, 0, 0, NULL);
//	if ( NULL == hThisFileMapping )
//	{
//		CloseHandle(hdThisFile);
//
//		CPatchLog::GetInstance()->AddLog(PLT_ERROR,"Fail to create exe map");
//		return NULL;
//	}
//	CloseHandle(hdThisFile);
//
//	PVOID pvThisFile = MapViewOfFile(hThisFileMapping, FILE_MAP_READ, 0,0,0);
//	if ( NULL == pvThisFile )
//	{
//		//CloseHandle(hdThisFile);
//		CloseHandle(hThisFileMapping);
//
//		CPatchLog::GetInstance()->AddLog(PLT_ERROR,"Fail during mapping exe");
//
//		return NULL;
//	}	
//
//	pvThisFile = (PVOID)( (char*)pvThisFile + pkResHelper->GetResourcePosOffset());
//
//	return pvThisFile;
//}