﻿
// PatchClientUpdater.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "PatchClientReleaser.h"

bool	ProcessPatching(string strReleasePath,unsigned int uiLauncherID)
{
	CPatchClientReleaser nReleaser(strReleasePath, uiLauncherID);

	return nReleaser.ReleaseResource();
}

void SplitString(_TCHAR* szStr, vector< string >& vStrs, char chToken )
{
	if (NULL == szStr)
	{
		return;
	}

	string strCur;
	for ( UINT nIndex = 0; nIndex < strlen( szStr ); nIndex ++ )
	{
		if ( chToken == szStr[nIndex] )
		{
			vStrs.push_back( strCur );
			strCur = "";
		}
		else
		{
			strCur.push_back( szStr[nIndex] );
		}
	}

	if ( strCur.length() >= 1 )
		vStrs.push_back( strCur );
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (argv[1] != NULL)
	{
		vector<string> vCommands;
		SplitString(argv[1],vCommands,'//');

		DWORD dwLauncherID = 0;
		if (vCommands.size() > 1 && vCommands[1] != "")
		{
			dwLauncherID = CPatchHelper::MyStringToDWORD(vCommands[1]);
		}
		ProcessPatching(vCommands[0], dwLauncherID);
	}
	

	return 0;
}

