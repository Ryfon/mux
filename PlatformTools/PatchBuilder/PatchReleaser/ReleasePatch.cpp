﻿#include "StdAfx.h"
#include "ReleasePatch.h"
#include <direct.h>
#include <Psapi.h>

CReleasePatch::~CReleasePatch(void)
{
	if (m_pkPackage)
	{
		Destroy();
	}

	if (m_pLog)
	{
		m_pLog = NULL;
	}
}

void CReleasePatch::SetLaunchID(UINT nID)
{
	m_nLaunchID = nID;	
}

CReleasePatch::CReleasePatch(void)
{
	m_pkPackage = NULL;
	FreeResource((void*)m_pConfigFileBuffer);
	m_pConfigFileBuffer = NULL;
	m_pkReportSender = NULL;

	m_pLog = CPatchLog::GetInstance();
	
}

void CReleasePatch::Destroy()
{
	string strPackFileName = m_pkPackage->GetPackFileName();
	
	if (m_pkPackage)
	{
		m_pkPackage->ClosePackage();
		delete m_pkPackage;
		m_pkPackage = NULL;

		if (!DeleteFile(strPackFileName.c_str()))
		{
			string strMsg = "Fail to delete temp file " + strPackFileName;
			m_pLog->AddLog(PLT_ERROR, strMsg);
		}
	}

	if (m_pConfigFileBuffer)
	{
		//delete [] m_pConfigFileBuffer;
		FreeResource((HGLOBAL)m_pConfigFileBuffer);
		m_pConfigFileBuffer = NULL;
	}

	m_mapPackages.clear();
	m_vCheckFiles.clear();
	m_mapUpdateFiles.clear();

	if (m_hdSigleObject)
	{
		CloseHandle(m_hdSigleObject);
		m_hdSigleObject = NULL;
	}

	// 重新启动launcher [8/25/2009 hemeng]
	if (m_bCreateLauncherThread && !m_strLauncherName.empty())
	{
		if (_access_s(m_strLauncherName.c_str(),0) == 0)
		{
			string strMsg = "Release Destroy - Start launcher.exe "; 
			m_pLog->AddLog(PLT_MSG,strMsg);
			STARTUPINFO si;
			PROCESS_INFORMATION pi;
			memset(&si, 0, sizeof(si));
			si.cb = sizeof(si);
			memset(&pi, 0, sizeof(pi));
			if (CreateProcess(m_strLauncherName.c_str(), "", NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi))
			{
				CloseHandle(pi.hThread);
				CloseHandle(pi.hProcess);
			}
		}
	}

	if (m_pLog)
	{
		string strLogFileName = m_strReleasePath + "\\" +  PATCH_DOWNLOAD_DIR;
		if(!m_pLog->CreateLogFile(strLogFileName))
		{
			PATCH_MEG_REPROT("Fail to create log",strLogFileName.c_str());
		}
	}
	
	m_pLog = NULL;

	m_pkReportSender = NULL;
}

CReleasePatch::CReleasePatch(const char* pszReleasePath)
{
	m_bInit = false;
	m_pConfigFileBuffer = NULL;
	m_hdSigleObject = NULL;
	m_bCreateLauncherThread = false;
	m_nLaunchID = 0;
	m_pLog = CPatchLog::GetInstance();

	if (NULL == m_pLog)
	{
		return;
	}

	m_pLog->AddLog(PLT_MSG,"Start create patch releaser ");
	if (NULL == pszReleasePath)
	{
		return;
	}
	m_strReleasePath = pszReleasePath;
	StringLower(m_strReleasePath);

	m_pkReportSender = CPatchReportSender::GetInstance();
	if (NULL == m_pkReportSender)
	{
		m_pLog->AddLog(PLT_ERROR, "Fail to get reporter");
		return;
	}

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN);
	// 创建实例唯一标志 [8/25/2009 hemeng]
	m_hdSigleObject = CreateMutex(
		NULL,
		TRUE,
		"MUX_PATCH_RELEASER"
		);

	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_ALREADYEXIST,MUXPATCH_PROGRESS_END);
		return;
	}

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 5);
	m_pLog->AddLog(PLT_MSG,"Start load package config file");

	if (!_LoadPackConfigFile())
	{
		m_pLog->AddLog(PLT_ERROR, "Fail to load package config file");
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		return;
	}
	
	m_pLog->AddLog(PLT_MSG,"Start initial packages");
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 10);

	if (!_InitPackages())
	{
		m_pLog->AddLog(PLT_ERROR,"Fail to load update resource");
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		return;
	}
	
	m_pLog->AddLog(PLT_MSG,"Create update resource package");
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 20);
	string strTmpFilePath = "";
	bool bSuccess = _CreateUpdateResourcePackage(strTmpFilePath);
	if (bSuccess)
	{
		
		m_pkPackage = new CPackage();

		if (!m_pkPackage->OpenPackage(strTmpFilePath))
		{
			string strMsg = "Fail to open update data file " + strTmpFilePath;
			m_pLog->AddLog(PLT_MSG,strMsg);
			bSuccess = false;
			delete m_pkPackage;
			m_pkPackage = NULL;
		}
	
	}

	if (!bSuccess)
	{
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
	}
	else
	{
		m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 30);
	}

	m_bInit = bSuccess;
}

bool	CReleasePatch::_CreateUpdateResourcePackage(string& strResFileName)
{
	m_pLog->AddLog(PLT_MSG,"Start load update resource");
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 20);

	DWORD dwResSize = 0;
	CResourceHelper* pkResHelper = (CResourceHelper*)_LoadResourceInfo(dwResSize);
	if (NULL == pkResHelper || dwResSize != sizeof(CResourceHelper))
	{
		m_pLog->AddLog(PLT_ERROR,"Fail to load update resource information");
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		return false;
	}

	unsigned int uiFileSize = pkResHelper->GetResourceSize();
	unsigned int uiFileOffset = pkResHelper->GetResourcePosOffset();

	FreeResource((void*)pkResHelper);
	pkResHelper = NULL;

	strResFileName = m_strReleasePath + "\\" + PATCH_DOWNLOAD_DIR;
	if (_access_s(strResFileName.c_str(),2) != 0)
	{
		if (_mkdir(strResFileName.c_str()) != 0)
		{
			string strMsg = "Fail to create temp dir " + strResFileName;
			m_pLog->AddLog(PLT_ERROR,strMsg);
			
			m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
			return false;			
		}
		if (_access_s(strResFileName.c_str(),2) != 0)
		{
			string strMsg = "Fail to create temp dir " + strResFileName;
			m_pLog->AddLog(PLT_ERROR,strMsg);
				
			m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
			return false;
		}
	}

	strResFileName = strResFileName + "\\" + PATCH_RESOURCE_TMP_FILE_NAME;

	char pThisFileName[MAX_PATH];
	GetModuleFileName(NULL,pThisFileName,MAX_PATH);
	HANDLE hdThisFile = CreateFile(pThisFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hdThisFile == INVALID_HANDLE_VALUE)
	{
		m_pLog->AddLog(PLT_ERROR,"Fail to open file during mapping exe");
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		return false;
	}

	HANDLE hThisFileMapping = CreateFileMapping(hdThisFile, NULL, PAGE_READONLY, 0, 0, NULL);
	if ( NULL == hThisFileMapping )
	{
		CloseHandle(hdThisFile);

		m_pLog->AddLog(PLT_ERROR,"Fail to create exe map");
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		return false;
	}
	CloseHandle(hdThisFile);
	
	PVOID pvThisFile = MapViewOfFile(hThisFileMapping, FILE_MAP_READ, 0,0,0);
	if ( NULL == pvThisFile )
	{
		//CloseHandle(hdThisFile);
		CloseHandle(hThisFileMapping);

		m_pLog->AddLog(PLT_ERROR,"Fail during mapping exe");
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);

		return false;
	}	

	pvThisFile = (PVOID)( (char*)pvThisFile + uiFileOffset);

	FILE* pFile = NULL;

	if (fopen_s(&pFile,strResFileName.c_str(),"wb") != 0)
	{
		string strMsg = "Fail to create temp file " + strResFileName;
		m_pLog->AddLog(PLT_ERROR,strMsg);

		UnmapViewOfFile((PVOID)pvThisFile);
		CloseHandle(hThisFileMapping);
		
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		return false;
	}

	fwrite(pvThisFile,uiFileSize,1,pFile);
	if (ferror(pFile) != 0)
	{
		string strMsg = "Fail to create temp file " + strResFileName;
		m_pLog->AddLog(PLT_ERROR,strMsg);
		
		fclose(pFile);
		UnmapViewOfFile((PVOID)pvThisFile);
		CloseHandle(hThisFileMapping);
		
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		return false;
	}

	fclose(pFile);
	UnmapViewOfFile((PVOID)pvThisFile);
	CloseHandle(hThisFileMapping);

	return true;
}

void*	CReleasePatch::_LoadResourceInfo(DWORD& dwResSize)
{
	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC	hRC = FindResource(hModule,MAKEINTRESOURCE(RESOURCE_ID),RT_RCDATA);

	if (NULL == hRC)
	{
		return NULL;
	}

	HGLOBAL hGlb = LoadResource(hModule,hRC);

	if (NULL == hGlb)
	{
		return NULL;
	}

	void* pvData = LockResource(hGlb);
	if (NULL == pvData)
	{
		return NULL;
	}

	dwResSize = SizeofResource(hModule,hRC);
	

	return pvData;

}

bool	CReleasePatch::_LoadPackConfigFile()
{
	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC	hRC = FindResource(hModule,MAKEINTRESOURCE(PACK_CONFIG_RES_ID),RT_RCDATA);

	if (NULL == hRC)
	{
		return false;
	}

	HGLOBAL hGlb = LoadResource(hModule,hRC);

	if (NULL == hGlb)
	{
		return false;
	}

	m_pConfigFileBuffer = (BYTE*)LockResource(hGlb);

	if (NULL == m_pConfigFileBuffer)
	{
		return false;
	}

	return true;
}

bool CReleasePatch::ReleasePatchPackage()
{
	if (NULL == m_pkPackage)
	{
		return false;
	}

	if (!m_bInit)
	{
		m_pLog->AddLog(PLT_ERROR, "Un initial patch package");
		return false;
	}

	// 如果是手动更新，且launcher已启动 [12/15/2009 hemeng]
	if ( m_nLaunchID == 0 && _IsMuxLauncherEnabled())
	{
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_ALREADYEXIST,MUXPATCH_PROGRESS_END);
		return false;
	}

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 35);
	
	StringLower(m_strReleasePath);

	string strConfigXMLPath = m_strReleasePath + "\\" + PATCH_CONFIGXML_FILE_NAME;

	m_pLog->AddLog(PLT_MSG,"Load current version");
	if (!_LoadCurrentVersion(strConfigXMLPath))
	{
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		string strMsg = "未找到配置文件，或配置文件已损坏" + strConfigXMLPath;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		m_bInit = false;
		return false;
	}
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 40);

	m_pLog->AddLog(PLT_MSG,"Initial patching");
	if(!_InitUpdating())
	{
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,MUXPATCH_PROGRESS_END);
		m_pLog->AddLog(PLT_ERROR, "初始化更新失败！");

		m_bInit = false;
		return false;
	}	

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_END);
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_PATCHING,MUXPATCH_PROGRESS_BEGIN);
	m_pLog->AddLog(PLT_MSG,"Start patching");
	if (!_Patch())
	{
		m_pLog->AddLog(PLT_ERROR,"Fail during patching");
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_UPDATEFAIL,MUXPATCH_PROGRESS_END);
		return false;
	}
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_PATCHING,MUXPATCH_PROGRESS_END);
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_FINISHING,MUXPATCH_PROGRESS_BEGIN);
	
	m_pLog->AddLog(PLT_MSG,"Update version");
	if (!_UpdatePatchClientXML(strConfigXMLPath))
	{
		m_pLog->AddLog(PLT_ERROR,"Fail during update local version");
		m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_FINISHING,MUXPATCH_PROGRESS_END);
		return false;
	}
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_FINISHING,MUXPATCH_PROGRESS_END);
	
	return true;
}

bool CReleasePatch::_InitUpdating()
{
	if (m_strReleasePath.empty() || m_bInit == false)
	{
		return false;
	}

	if (!_LoadLogFile())
	{
		m_pLog->AddLog(PLT_ERROR,"Fail during load log file");
		return false;
	}
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 40);

	if (!_IsClientDir())
	{
		string strMsg = "更新目录不是mux客户端目录，请查证后再更新" + m_strReleasePath;
		m_pLog->AddLog(PLT_ERROR,strMsg);
		return false;
	}
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 50);

	if (_NeedUpdate() == false)
	{
		return false;
	}

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_INIT,MUXPATCH_PROGRESS_BEGIN + 80);
	return true;
}

bool CReleasePatch::_NeedUpdate()
{
	if (NULL == m_pkPackage || m_strReleasePath == "")
	{
		return false;
	}

	if (CPatchHelper::CompareVersion(m_CurrVersion,m_OldVersion) != 0)
	{
		string strMsg = "当前版本与本更新包适用版本不符，不可更新" + m_CurrVersion;
		m_pLog->AddLog(PLT_ERROR, strMsg);
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_CONTENTVERSION,MUXPATCH_PROGRESS_END);		
		return false;
	}
	if (CPatchHelper::CompareVersion(m_CurrVersion,m_NewVersion) != -1)
	{
		string strMsg = "当前版本与本更新包最高版本相同或更高，不可更新" + m_CurrVersion;
		m_pLog->AddLog(PLT_ERROR, strMsg);
		m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_CONTENTVERSION,MUXPATCH_PROGRESS_END);	
		return false;
	}

	return true;
}

// 文件名必须为相对路径 [8/7/2009 hemeng]
bool CReleasePatch::_IsPackageFile(std::string strFileName)
{
	if (!m_bInit)
	{
		return false;
	}

	StringLower(strFileName);
	vector<string> vFilePathSeg;

	_GetPathSegment(strFileName.c_str(),vFilePathSeg);

	for (unsigned int uiIndex = 0; uiIndex < vFilePathSeg.size(); uiIndex++)
	{
		string strSeg = vFilePathSeg[uiIndex];

		map<string,string>::iterator it = m_mapPackages.begin();

		for (it; it != m_mapPackages.end(); it++)
		{
			string strPackName = it->first;

			if (strPackName == strSeg)
			{
				return true;
			}
		}

	}
	return false;
}

string CReleasePatch::_GetPackNameByFileName(string strFileName)
{
	if (!m_bInit)
	{
		return "";
	}
	
	StringLower(strFileName);

	vector<string> vFilePathSeg;

	_GetPathSegment(strFileName.c_str(),vFilePathSeg);

	for (unsigned int uiIndex = 0; uiIndex < vFilePathSeg.size(); uiIndex++)
	{
		string strSeg = vFilePathSeg[uiIndex];

		map<string,string>::iterator it = m_mapPackages.begin();

		for (it; it != m_mapPackages.end(); it++)
		{
			string strPackName = it->first;

			if (strPackName == strSeg)
			{
				return it->second;
			}
		}

	}


	return "";
}

bool CReleasePatch::_DeleteUnuseFiles()
{
	map<PATCH_COMPARE_RESULT_TYPE, vector<string>>::iterator it = m_mapUpdateFiles.find(PCRT_DELETE_FILE);
	m_pLog->AddLog(PLT_MSG,"Delete files");

	// 需要删除的文件 [8/10/2009 hemeng]
	if (it != m_mapUpdateFiles.end())
	{
		vector<string> vDeleteFileList = it->second;
		map<string,vector<string>> mapPackFiles;
		vector<string> vFiles;

		_PartFiles(vDeleteFileList,mapPackFiles,vFiles);

		// 处理包文件 [8/31/2009 hemeng]
		for (map<string,vector<string>>::iterator packIt = mapPackFiles.begin();
			packIt != mapPackFiles.end(); packIt ++)
		{
			string strPackName = packIt->first;
			strPackName = m_strReleasePath + "\\" + strPackName;
			string strMsg = "Delete files:" + strPackName;
			m_pLog->AddLog(PLT_MSG,strMsg);

			if (!CPatchHelper::VerifyFile(strPackName))
			{
				string strMsg = "Can not find file or file can not over write " + strPackName;
				m_pLog->AddLog(PLT_ERROR, strMsg);
				continue;
			}

			if (!_DeleteFilesFromPackage(strPackName,packIt->second))
			{
				string strMsg = "Fail to delete file from package " + strPackName;
				m_pLog->AddLog(PLT_ERROR, strMsg);
				continue;
			}
		}

		// 处理文件 [8/31/2009 hemeng]		
		for (vector<string>::size_type szIndex = 0; szIndex < vFiles.size(); szIndex++)
		{
			string strAbsFilePath = _ConvertMediaFileName(vFiles[szIndex]);
			if (strAbsFilePath.empty())
			{
				continue;
			}
			// 是否存在且可读 [8/10/2009 hemeng]
			if (!CPatchHelper::VerifyFile(strAbsFilePath.c_str()))
			{
				string strMsg = "Can not find file or file can not over write " + strAbsFilePath;
				m_pLog->AddLog(PLT_ERROR, strMsg);
			}
			else
			{
				// 删除文件 [8/10/2009 hemeng]
				if(!DeleteFile(strAbsFilePath.c_str()))
				{
					string strMsg = "Fail to delete file:" + strAbsFilePath;
					m_pLog->AddLog(PLT_ERROR, strMsg);
				}
				else
				{
					string strMsg = "Delete file:" + strAbsFilePath;
					m_pLog->AddLog(PLT_MSG, strMsg);
				}
			}
		}	
	}

	return true;
}

bool CReleasePatch::_Patch()
{
	if (!m_bInit)
	{
		return false;
	}

	// 删除文件失败仍继续 [8/26/2009 hemeng]
	_DeleteUnuseFiles();
			
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_PATCHING,MUXPATCH_PROGRESS_BEGIN + 10);
	// 删除文件失败仍继续 [8/26/2009 hemeng]
	_DeleteUnuseDirs();		

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_PATCHING,MUXPATCH_PROGRESS_BEGIN + 30);
	if(!_UpdateFiles())
	{
		return false;
	}	

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_PATCHING,MUXPATCH_PROGRESS_BEGIN + 50);
	if(!_AddFiles())
	{
		return false;
	}

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_PATCHING,MUXPATCH_PROGRESS_BEGIN + 90);

	return true;
}

bool CReleasePatch::_LoadLogFile()
{
	CPackageFileReader* pkReader = m_pkPackage->GetPackageFileReader(PATCH_RESULT_FILE_NAME);

	if (NULL == pkReader)
	{
		return false;
	}

	SPackageFileInfo* pkFileInfo = pkReader->GetPackageFileInfo();
	if (NULL == pkFileInfo)
	{
		delete pkReader;
		pkReader = NULL;

		return false;
	}

	UINT nBufferSize = pkFileInfo->nFileOrgSize;

	char* pBuffer = new char[nBufferSize];

	if (pkReader->ReadBuffer((unsigned char*)pBuffer) == 0)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		pkFileInfo = NULL;

		delete pkReader;
		pkReader = NULL;		
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->Parse(pBuffer);

	bool bSuccess = true;
	TiXmlElement* pElmt = pXmlDoc->FirstChildElement();
	
	if (NULL == pElmt)
	{
		bSuccess = false;
	}

	while (pElmt != NULL && bSuccess != false)
	{
		const char* pszName = pElmt->Value();
		if (strcmp(pszName, "Version") == 0)
		{
			_LoadVersion(pElmt);
		}
		else if (strcmp(pszName,"CheckFile") == 0)
		{
			_LoadCheckFiles(pElmt);
		}
		else if (strcmp(pszName, "AddFileList") == 0)
		{
			if(!_LoadFileList(pElmt,PCRT_ADD_FILE))
			{
				bSuccess = false;
			}
		}
		else if(strcmp(pszName, "DeleteFileList") == 0)
		{
			if(!_LoadFileList(pElmt,PCRT_DELETE_FILE))
			{
				bSuccess = false;
			}
		}
		else if(strcmp(pszName,"UpdateFileList") == 0)
		{
			if(!_LoadFileList(pElmt,PCRT_UPDATE_FILE))
			{
				bSuccess = false;
			}
		}
		else if(strcmp(pszName,"DeleteDirList") == 0)
		{
			if(!_LoadFileList(pElmt,PCRT_DELETE_DIR))
			{
				bSuccess = false;
			}
		}

		pElmt = pElmt->NextSiblingElement();
	}


	pkFileInfo = NULL;

	delete pkReader;
	pkReader = NULL;

	delete [] pBuffer;
	pBuffer = NULL;

	delete pXmlDoc;
	pXmlDoc = NULL;

	return bSuccess;
}

void CReleasePatch::_LoadVersion(TiXmlElement* pElmt)
{
	if (pElmt == NULL)
	{
		return;
	}

	m_NewVersion = pElmt->Attribute("NewVersion");
	m_OldVersion = pElmt->Attribute("OldVersion");
}

void CReleasePatch::_LoadCheckFiles(TiXmlElement* pElmt)
{
	if (NULL == pElmt)
	{
		return;
	}

	TiXmlElement* pElmtSub = pElmt->FirstChildElement();

	while (pElmtSub != NULL)
	{
		string strTmp = pElmtSub->Attribute("FileName");
		StringLower(strTmp);

		if (strTmp != "")
		{
			m_vCheckFiles.push_back(strTmp);
		}

		pElmtSub = pElmtSub->NextSiblingElement();
	}
}

bool CReleasePatch::_LoadFileList(TiXmlElement* pElmt,PATCH_COMPARE_RESULT_TYPE nType)
{
	if (pElmt == NULL)
	{
		return false;
	}

	map<PATCH_COMPARE_RESULT_TYPE,vector<string>>::iterator it = m_mapUpdateFiles.find(nType);

	if (it == m_mapUpdateFiles.end())
	{
		vector<string> vTmp;
		m_mapUpdateFiles.insert(pair<PATCH_COMPARE_RESULT_TYPE,vector<string>>(nType,vTmp));

		it = m_mapUpdateFiles.find(nType);
		if (it == m_mapUpdateFiles.end())
		{
			return false;
		}
	}

	TiXmlElement* pElmtSub = pElmt->FirstChildElement();

	while (pElmtSub != NULL)
	{
		string strTmp = pElmtSub->Attribute("FileName");
		StringLower(strTmp);

		if (strTmp != "")
		{
			it->second.push_back(strTmp);
		}
			
		pElmtSub = pElmtSub->NextSiblingElement();
	}

	return true;
}

bool CReleasePatch::_UpdatePatchClientXML(string strXmlFilePath)
{
	if (!m_bInit)
	{
		return false;
	}

	if (!CPatchHelper::VerifyFile(strXmlFilePath))
	{
		return false;
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->LoadFile(strXmlFilePath.c_str());
	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_FINISHING,MUXPATCH_PROGRESS_BEGIN + 10);

	TiXmlElement* pElmtRoot = pXmlDoc->RootElement();
	TiXmlElement* pElmt = pElmtRoot->FirstChildElement();

	if (NULL == pElmt)
	{
		return false;
	}

	const char* pszName = pElmt->Value();

	if (strcmp(pszName, "ClientVersion") == 0)
	{
		pElmt->SetAttribute("Version",m_NewVersion.c_str());
	}

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_FINISHING,MUXPATCH_PROGRESS_BEGIN + 50);

	bool bSuccess = pXmlDoc->SaveFile();

	m_pkReportSender->SendUpdateMessage(MUXPATCH_UPDATE_FINISHING,MUXPATCH_PROGRESS_BEGIN + 90);

	delete pXmlDoc;
	pXmlDoc = NULL;

	return bSuccess;
}

string CReleasePatch::_ConvertMediaFileName(string strFilePath)
{
	if (strFilePath.empty())
	{
		return "";
	}

	StringLower(strFilePath);
	
	string strMediaFilePath = "";
	// 属于包文件 [8/6/2009 hemeng]
	if (_IsPackageFile(strFilePath))
	{
		string strPackName = _GetPackNameByFileName(strFilePath);
		if (strPackName.empty())
		{
			return "";
		}
		strMediaFilePath = m_strReleasePath + "\\" + strPackName;
		
	}
	// 属于文件 [8/6/2009 hemeng]
	else
	{
		strMediaFilePath = m_strReleasePath + "\\" + strFilePath;
	}

	
	return strMediaFilePath;
}



CPackage* CReleasePatch::_LoadPackage(string strPackFilePath)
{
	if (strPackFilePath.empty())
	{
		return false;
	}

	if (NULL == m_pConfigFileBuffer)
	{
		return false;
	}

	if (_access_s(strPackFilePath.c_str(),2) != 0)
	{
		string strMsg = "Fail to access package:" + strPackFilePath;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		return NULL;
	}

	StringLower(strPackFilePath);

	CPackage* pkPackage = new CPackage();

	if (NULL == pkPackage)
	{
		return NULL;
	}

	if (!pkPackage->SpecialOpen(strPackFilePath,(char*)m_pConfigFileBuffer))
	{
		string strMsg = "Fail to load package:" + strPackFilePath;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		delete pkPackage;
		pkPackage = NULL;

		return NULL;
	}

	return pkPackage;
}

string CReleasePatch::_ConvertRelPath(const string strFilePath)
{
	if (strFilePath == "")
	{
		return "";
	}

	string strTmp = strFilePath;
	StringLower(strTmp);

	// 找不到相对路径 [8/6/2009 hemeng]
	size_t nPos = strTmp.find(m_strReleasePath);
	if (nPos == string::npos)
	{
		return "";
	}

	nPos += m_strReleasePath.length();
	string strRelPath = strTmp.substr(nPos);
	strRelPath = strRelPath.substr(strRelPath.find("\\") + 1);

	return strRelPath;
}

bool CReleasePatch::_InitPackages()
{
	if (NULL == m_pConfigFileBuffer)
	{
		return false;
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();

	if (NULL == pXmlDoc)
	{
		return false;
	}

	pXmlDoc->Parse((char*)m_pConfigFileBuffer);

	TiXmlElement* pRootElement = pXmlDoc->RootElement();

	if (NULL == pRootElement)
	{
		return false;
	}

	TiXmlElement* pChildElement = pRootElement->FirstChildElement();

	while (pChildElement != NULL)
	{
		const char* pszName = pChildElement->Value();
		if (strcmp(pszName, "PackDir") == 0)
		{
			const char* pDirPath = pChildElement->Attribute("DirName");
			if (NULL == pDirPath)
			{
				continue;				
			}
			string strDirPath = (string)pDirPath;
			StringLower(strDirPath);
			string strDirName = strDirPath.substr(strDirPath.find_last_of("\\") + 1);
			strDirPath += PACKAGE_EXTENTION;
			m_mapPackages.insert(pair<string,string>(strDirName,strDirPath));
		}	

		pChildElement = pChildElement->NextSiblingElement();
	}

	delete pXmlDoc;
	pXmlDoc = NULL;

	return true;	
}

void CReleasePatch::_PartFiles(std::vector<string> vFiles, std::map<string,vector<string> >& mapPackFiles, std::vector<string>& vPathFiles)
{
	for (vector<string>::size_type uiIndex = 0; uiIndex < vFiles.size(); uiIndex++)
	{
		string strFileName = vFiles[uiIndex];

		// 如果是包文件 [8/31/2009 hemeng]
		if (_IsPackageFile(strFileName))
		{
			string strPackName = _GetPackNameByFileName(strFileName);
			if (strFileName != "")
			{
				map<string,vector<string>>::iterator packIt = mapPackFiles.find(strPackName);
				if (packIt == mapPackFiles.end())
				{
					vector<string> vFiles;
					vFiles.push_back(strFileName);
					mapPackFiles.insert(pair<string,vector<string>>(strPackName,vFiles));
				}
				else
				{
					packIt->second.push_back(strFileName);
				}
			}				
		}
		// 如果不是包文件 [8/31/2009 hemeng]
		else
		{
			vPathFiles.push_back(strFileName);
		}
	}
}

bool CReleasePatch::_AddFiles()
{
	map<PATCH_COMPARE_RESULT_TYPE, vector<string>>::iterator it = m_mapUpdateFiles.find(PCRT_ADD_FILE);
	m_pLog->AddLog(PLT_MSG,"Add files");

	// 有需要添加的文件 [8/7/2009 hemeng]
	if (it != m_mapUpdateFiles.end())
	{
		vector<string> vFilesToAdd = it->second;
		// 对需要添加的文件进行分类 [8/31/2009 hemeng]
		// 包名称  文件名称 [8/31/2009 hemeng]
		map<string,vector<string>> mapPackFiles;
		vector<string> vFiles;

		_PartFiles(vFilesToAdd,mapPackFiles,vFiles);

		// 处理包文件 [8/31/2009 hemeng]
		for (map<string,vector<string>>::iterator packIt = mapPackFiles.begin();
			packIt != mapPackFiles.end(); packIt ++)
		{
			string strPackName = packIt->first;
			strPackName = m_strReleasePath + "\\" + strPackName;
			string strMsg = "Add files:" + strPackName;
			m_pLog->AddLog(PLT_MSG,strMsg);
			if (!CPatchHelper::VerifyFile(strPackName))
			{
				string strMsg = "Can not find file or file can not over write " + strPackName;
				m_pLog->AddLog(PLT_ERROR, strMsg);
				
				return false;
			}

			if (!_AddFilesToPackage(strPackName,packIt->second))
			{
				string strMsg = "Fail to add files to package:" + strPackName;
				m_pLog->AddLog(PLT_ERROR, strMsg);
				return false;
			}
		}
		
		// 处理文件 [8/31/2009 hemeng]
		
		for (unsigned int uiIndex = 0; uiIndex < vFiles.size(); uiIndex++)
		{
			string strFileName = vFiles[uiIndex];
			if (!_AddFile(strFileName))
			{
				string strMsg = "Fail to add file:" + strFileName;
				m_pLog->AddLog(PLT_ERROR, strMsg);

				return false;
			}
		}

	}


	return true;
}

bool CReleasePatch::_UpdateFiles()
{
	map<PATCH_COMPARE_RESULT_TYPE, vector<string>>::iterator it = m_mapUpdateFiles.find(PCRT_UPDATE_FILE);
	m_pLog->AddLog(PLT_MSG,"Update files");

	// 有需要更新的文件 [8/7/2009 hemeng]
	if (it != m_mapUpdateFiles.end())
	{
		vector<string> vFilesToUpdate = it->second;
		map<string,vector<string>> mapPackFiles;
		vector<string> vFiles;

		_PartFiles(vFilesToUpdate,mapPackFiles,vFiles);

		// 处理包文件 [8/31/2009 hemeng]
		for (map<string,vector<string>>::iterator packIt = mapPackFiles.begin();
			packIt != mapPackFiles.end(); packIt ++)
		{
			string strPackName = packIt->first;
			strPackName = m_strReleasePath + "\\" + strPackName;
			if (!CPatchHelper::VerifyFile(strPackName))
			{
				string strMsg = "Can not find file or file can not over write " + strPackName;
				m_pLog->AddLog(PLT_ERROR, strMsg);
				return false;
			}
			string strMsg = "Update file:" + strPackName;
			m_pLog->AddLog(PLT_MSG,strMsg);
			if (!_AddFilesToPackage(strPackName,packIt->second))
			{
				string strMsg = "Fail to add files to package " + strPackName;
				m_pLog->AddLog(PLT_ERROR, strMsg);
				return false;
			}
		}

		// 处理文件 [8/31/2009 hemeng]

		for (unsigned int uiIndex = 0; uiIndex < vFiles.size(); uiIndex++)
		{
			string strFileName = vFiles[uiIndex];
			string strLauncherName = PATCH_MUTEX_FILE_NAME;
			string strLauncherDllName = PATCH_MUTEX_DLL_NAME;
			StringLower(strLauncherName);
			if (strFileName == strLauncherName || strFileName == strLauncherDllName)
			{
				if(m_nLaunchID != 0 && m_bCreateLauncherThread == false)
				{
					HANDLE hd = OpenProcess(PROCESS_ALL_ACCESS,FALSE,m_nLaunchID);
					char szBuf[MAX_PATH];
					GetModuleFileNameEx(hd, NULL, szBuf, MAX_PATH);
					m_strLauncherName = szBuf;

					if (hd)
					{
						//PATCH_ERROR_REPROT("更新完成！正在重启！","Mux");
						TerminateProcess(hd,-1);
						WaitForSingleObject(hd,INFINITE);
						CloseHandle(hd);
						// 停留1秒 [9/7/2009 hemeng]
						Sleep(3000);
						m_bCreateLauncherThread = true;						
					}
				}		
			}
			
			if (!_AddFile(strFileName))
			{
				string strMsg = "Fail to add file " + strFileName;
				m_pLog->AddLog(PLT_ERROR, strMsg);
				return false;
			}
		}		
	}


	return true;
}

bool CReleasePatch::_AddFileToPackage(CPackage* pkPackage,string strFileName)
{
	if (NULL == pkPackage)
	{
		return false;
	}

	string strPackName = pkPackage->GetPackageRootDir();
	if (strPackName == "")
	{
		string strMsg = "Fail to get pack root dir " + strFileName;
		m_pLog->AddLog(PLT_ERROR, strMsg);
		return false;
	}

	string strMsg = "Adding file:" + strFileName + " to package" + pkPackage->GetPackFileName();
	m_pLog->AddLog(PLT_MSG,strMsg);
	m_pLog->IncFileCount();

	string strTmp = strFileName.substr(strFileName.find(strPackName) + strPackName.size() + 1);

	// 将文件名转换为相对于包的名称 [8/10/2009 hemeng]
	bool bSuccess = true;

	CPackageFileReader* pkReader = m_pkPackage->GetPackageFileReader(strFileName);
	if (NULL == pkReader)
	{
		strMsg = "Fail to read file data" + strFileName;
		m_pLog->AddLog(PLT_ERROR, strMsg);
		bSuccess = false;
	}

	if(bSuccess)
	{
		SPackageFileInfo* pkFileInfo = pkReader->GetPackageFileInfo();

		if (NULL == pkFileInfo)
		{
			bSuccess = false;
		}

		if (bSuccess)
		{
			UINT nUnZipBuffer = pkFileInfo->nFileSize;
			string strFileNameInPKG = strTmp;
			if (strTmp.find_last_of("\\") != string::npos)
			{
				strFileNameInPKG = strTmp.substr(strTmp.find_last_of("\\") + 1);
			}

			pkFileInfo->szFilename = strFileNameInPKG;
			pkFileInfo->szFullPathName = strTmp;
			BYTE* pBuffer = new BYTE[nUnZipBuffer];

			bSuccess = pkReader->ReadUnZipBuffer(pBuffer);

			if (bSuccess)
			{
				bSuccess = pkPackage->AddFile(*pkFileInfo,pBuffer,true);
			}

			if (!bSuccess)
			{
				strMsg = "Fail to add file " + pkFileInfo->szFullPathName;
				m_pLog->AddLog(PLT_ERROR, strMsg);
			}

			delete [] pBuffer;
			pBuffer = NULL;
		}
	}

	delete pkReader;
	pkReader = NULL;

	return bSuccess;
}

bool CReleasePatch::_AddFilesToPackage(string strPackFileName,vector<string> vFileList)
{
	if (strPackFileName.empty())
	{
		return false;
	}
	StringLower(strPackFileName);

	// 查找包是否存在 [9/2/2009 hemeng]
	if (_access_s(strPackFileName.c_str(),0) != 0)
	{
		string strMsg = "Fail to find package " + strPackFileName;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		return false;
	}

	string strBackUpFileName = _SecurityPack(strPackFileName);

	if (strBackUpFileName.empty())
	{
		return false;
	}

	CPackage* pkPackage = _LoadPackage(strPackFileName);
	if (NULL == pkPackage)
	{
		return false;
	}

	bool bSuccess = true;

	for (unsigned int uiIndex = 0; uiIndex < vFileList.size(); uiIndex++)
	{
		string strFileName = vFileList[uiIndex];
		bSuccess = _AddFileToPackage(pkPackage,strFileName);
		if (!bSuccess)
		{
			break;
		}
	}

	if (pkPackage)
	{
		delete pkPackage;
		pkPackage = NULL;
	}

	if (bSuccess)
	{
		// 更新成功则删除备用包 [9/1/2009 hemeng]
		bSuccess = (bool)DeleteFile(strBackUpFileName.c_str());
	}

	return bSuccess;
}

bool CReleasePatch::_DeleteFilesFromPackage(string strPackFileName,vector<string> vFileList)
{
	if (strPackFileName.empty())
	{
		return false;
	}
	StringLower(strPackFileName);

	// 查找包是否存在 [9/2/2009 hemeng]
	if (_access_s(strPackFileName.c_str(),0) != 0)
	{
		string strMsg = "Fail to find package" + strPackFileName;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		return false;
	}

	string strBackUpFileName = _SecurityPack(strPackFileName);

	if (strBackUpFileName.empty())
	{
		return false;
	}

	CPackage* pkPackage = _LoadPackage(strPackFileName);
	if (NULL == pkPackage)
	{
		return false;
	}

	bool bSuccess = true;

	for (unsigned int uiIndex = 0; uiIndex < vFileList.size(); uiIndex++)
	{
		string strFileName = vFileList[uiIndex];
		if(!_DeleteFileFromPackage(pkPackage,strFileName))
		{
			string strMsg = "Fail to delete package file " + strFileName;
			m_pLog->AddLog(PLT_ERROR, strMsg);

			bSuccess = false;
		}
	}

	if (pkPackage)
	{
		delete pkPackage;
		pkPackage = NULL;
	}

	if (bSuccess)
	{
		bSuccess = (bool)DeleteFile(strBackUpFileName.c_str());
	}

	return bSuccess;
}

bool CReleasePatch::_DeleteFileFromPackage(CPackage* pkPackage,string strFileName)
{
	if (NULL == pkPackage)
	{
		return false;
	}

	if (strFileName.empty())
	{
		return false;
	}

	string strPackName = pkPackage->GetPackageRootDir();
	if (strPackName.empty())
	{
		string strMsg = "Fail to get pack root dir " + strFileName;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		return false;
	}

	string strMsg = "Deleting file:" + strFileName + " from package:" + strPackName;
	m_pLog->AddLog(PLT_MSG, strMsg);

	StringLower(strFileName);
	string strRelPath = strFileName.substr(strFileName.find(strPackName) + strPackName.length() + 1);	

	if (strRelPath.empty())
	{
		return false;
	}

	m_pLog->IncFileCount();

	return pkPackage->DeletePackFile(strRelPath);

}

bool CReleasePatch::_AddFile(string strFileName)
{
	if (strFileName.empty())
	{
		return false;
	}

	StringLower(strFileName);
	string strAbsPath = _ConvertMediaFileName(strFileName);

	if (strAbsPath.empty())
	{
		return false;
	}

	string strMsg = "Adding file:" + strFileName;
	m_pLog->AddLog(PLT_MSG,strMsg);
	m_pLog->IncFileCount();

	if (!_ValidateDirs(strAbsPath))
	{
		strMsg = "Fail to update file,invalidate directory " + strAbsPath;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		return false;
	}

	// 如果文件已存在则删除重建 [8/10/2009 hemeng]
	if (CPatchHelper::VerifyFile(strAbsPath))
	{
		if (DeleteFile(strAbsPath.c_str()) == 0)
		{
			strMsg = "Fail to update file,delete old file fail " + strAbsPath;
			m_pLog->AddLog(PLT_ERROR, strMsg);

			return false;
		}
		/*else
		{
			PATCH_MEG_REPROT("文件不可写",strAbsPath.c_str());
			return false;
		}*/
	}	

	return m_pkPackage->CreateNewFileFromPackage(strFileName,strAbsPath);

}

bool CReleasePatch::_DeleteUnuseDirs()
{
	map<PATCH_COMPARE_RESULT_TYPE, vector<string>>::iterator it = m_mapUpdateFiles.find(PCRT_DELETE_DIR);

	if (it != m_mapUpdateFiles.end())
	{
		vector<string> vDeleteDirs = it->second;
		map<string,vector<string>> mapPackDir;
		vector<string> vDirs;

		_PartFiles(vDeleteDirs,mapPackDir,vDirs);

		// 处理包文件 [8/31/2009 hemeng]
		for (map<string,vector<string>>::iterator packIt = mapPackDir.begin();
			packIt != mapPackDir.end(); packIt ++)
		{
			string strPackName = packIt->first;
			strPackName = m_strReleasePath + "\\" + strPackName;
			string strMsg = "Delete directories " + strPackName;
			m_pLog->AddLog(PLT_MSG,strMsg);
			if (!_DeleteDirsFromPackage(strPackName,packIt->second))
			{
				string strMsg = "Fail to delete file from package " + strPackName;
				m_pLog->AddLog(PLT_ERROR, strMsg);

				continue;
			}
		}

		// 处理文件 [8/31/2009 hemeng]
		m_pLog->AddLog(PLT_MSG,"Delete directories");
		for (unsigned int uiIndex = 0; uiIndex < vDirs.size(); uiIndex++)
		{
			string strDirPath = _ConvertMediaFileName(vDirs[uiIndex]);
			if (strDirPath == "")
			{
				return false;
			}

			// 是否存在 [8/10/2009 hemeng]
			if ( _access_s(strDirPath.c_str(),6) != 0 )
			{
				string strMsg = "Fail to delete directoy, bad file access " + strDirPath;
				m_pLog->AddLog(PLT_ERROR, strMsg);

				return false;
			}

			if (_rmdir(strDirPath.c_str()))
			{
				string strMsg = "Fail to delete directory " + strDirPath;
				m_pLog->AddLog(PLT_ERROR, strMsg);

				return false;
			}
		}	
	}

	return true;
}

bool	CReleasePatch::_LoadCurrentVersion(string strPatchClientFileName)
{
	if (strPatchClientFileName.empty() || m_bInit == false)
	{
		return false;
	}

	StringLower(strPatchClientFileName);
	if (_access_s(strPatchClientFileName.c_str(),0) != 0)
	{
		string strMsg = "Fail to find current version file " + strPatchClientFileName;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		return false;
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->LoadFile(strPatchClientFileName.c_str());

	bool bSuccess = true;

	TiXmlElement* pElmtRoot = pXmlDoc->RootElement();
	TiXmlElement* pElmt = pElmtRoot->FirstChildElement();

	if (NULL == pElmt)
	{
		bSuccess = false;
	}

	const char* pszName = pElmt->Value();
	if (strcmp(pszName, "ClientVersion") == 0)
	{
		m_CurrVersion = pElmt->Attribute("Version");
	}

	delete pXmlDoc;
	pXmlDoc = NULL;

	return bSuccess;
}

bool	CReleasePatch::_DeleteDirsFromPackage(string strPackFileName,vector<string> vDirList)
{
	if (strPackFileName.empty())
	{
		return false;
	}

	StringLower(strPackFileName);

	// 查找包是否存在 [9/2/2009 hemeng]
	if (_access_s(strPackFileName.c_str(),0) != 0)
	{
		string strMsg = "Fail to find package " + strPackFileName;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		return false;
	}

	string strBackUpFileName = _SecurityPack(strPackFileName);
	
	if (strBackUpFileName.empty())
	{
		return false;
	}

	CPackage* pkPackge = _LoadPackage(strPackFileName);
	if (NULL == pkPackge)
	{
		return false;
	}	

	CPackageFileSystem* pkFileSys = pkPackge->GetPackageFileSystem();
	if (NULL == pkPackge)
	{
		pkPackge = NULL;
		return false;
	}

	string strPackName = pkPackge->GetPackageRootDir();
	if (strPackName.empty())
	{
		string strMsg = "Fail to get pack root dir " + strPackFileName;
		m_pLog->AddLog(PLT_ERROR, strMsg);

		return false;
	}

	bool bSuccess = true;
	for (unsigned int uiIndex = 0; uiIndex < vDirList.size(); uiIndex++)
	{
		string strDirName = vDirList[uiIndex];
		
		string strRelDirName = strDirName.substr(strDirName.find(strPackName) + strPackName.size() + 1);

		SPackageDirInfo* pkDelDirInfo = pkFileSys->GetPackDirInfo(strRelDirName);
		if (NULL == pkDelDirInfo)
		{
			bSuccess = false;
		}
		else
		{
			bSuccess = true;
		}

		if (bSuccess)
		{
			if(!_DeleteDirFromPackage(pkPackge,pkDelDirInfo))
			{
				bSuccess = false;
			}
		}
		
	}	

	if (pkPackge)
	{
		delete pkPackge;
		pkPackge = NULL;
	}
	
	pkFileSys = NULL;

	if (bSuccess)
	{
		bSuccess = (bool)DeleteFile(strBackUpFileName.c_str());
	}

	return bSuccess;
}

bool	CReleasePatch::_DeleteDirFromPackage(CPackage* pkPackge,SPackageDirInfo* pkDirInfo)
{
	if (NULL == pkPackge || NULL == pkDirInfo)
	{
		return false;
	}
	
	string strMsg = "Deleting directory:" + pkDirInfo->szDirName + " from package:" + pkPackge->GetPackFileName();
	m_pLog->AddLog(PLT_MSG, strMsg);

	// 首先删除文件 [8/20/2009 hemeng]
	map<string,SPackageFileInfo> vDirFileInfos = pkDirInfo->vPackageFileInfos;
	map<string,SPackageFileInfo>::iterator itFiles = vDirFileInfos.begin();	
	for (; itFiles != vDirFileInfos.end(); itFiles++)
	{
		SPackageFileInfo nFileInfo = itFiles->second;
		m_pLog->IncFileCount();
		if(!pkPackge->DeletePackFile(nFileInfo.szFullPathName))
		{
			return false;
		}
	}

	// 删除子目录文件 [8/20/2009 hemeng]
	map<string,SPackageDirInfo>::iterator itDirs = pkDirInfo->vPackageDirInfos.begin();
	for (; itDirs != pkDirInfo->vPackageDirInfos.end(); itDirs++)
	{
		SPackageDirInfo nFileInfo = itDirs->second;
		if (!_DeleteDirFromPackage(pkPackge,&nFileInfo))
		{
			return false;
		}
	}

	return true;
}

string CReleasePatch::_SecurityPack(string strPackFileName)
{
	// 拷贝备用文件 [9/1/2009 hemeng]
	string strBackUpFileName = strPackFileName;
	strBackUpFileName = strPackFileName.substr(0,strPackFileName.find_last_of("."));
	strBackUpFileName += ".tmp";

	CPackage* pkPackage = new CPackage();
	bool bOK = pkPackage->OpenPackage(strPackFileName);
	bool bSuccess = true;
	delete pkPackage;
	pkPackage = NULL;

	if (bOK)
	{
		bSuccess = _MakeBackUpFile(strPackFileName,strBackUpFileName);
	}
	else
	{
		// 如果包不存在或已损坏则检查是否存在备用包 [9/1/2009 hemeng]
		if (_access_s(strBackUpFileName.c_str(),2) != 0)
		{
			bSuccess = false;
		}

		if (bSuccess)
		{
			bSuccess = _BackUpPack(strPackFileName,strBackUpFileName);	
		}		
	}	

	if (bSuccess)
	{
		return strBackUpFileName;
	}

	return "";
	
}

bool CReleasePatch::_IsClientDir()
{
	for (unsigned int uiIndex = 0; uiIndex < m_vCheckFiles.size(); uiIndex++)
	{
		string strFilePath = m_strReleasePath + "\\" + m_vCheckFiles[uiIndex];

		if (_access_s(strFilePath.c_str(),0) != 0)
		{
			return false;
		}
	}

	return true;
}

bool	CReleasePatch::_MakeBackUpFile(string strPackName,string strBackUpFileName)
{
	if (strPackName.empty() || strBackUpFileName.empty())
	{
		return false;
	}

	CPackage* pPackage = new CPackage;
	if ( NULL == pPackage)
	{
		return false;
	}

	if ( !pPackage->OpenPackage(strPackName,pPackage->EO_READ) )
	{
		return false;
	}

	CPackageFileSystem* pPackSys = pPackage->GetPackageFileSystem();
	if (NULL == pPackSys)
	{
		delete pPackage;
		pPackage = NULL;
	}

	UINT nDataEndPos = pPackSys->GetPackageDataEndPos();

	pPackSys = NULL;
	delete pPackage;
	pPackage = NULL;

	FILE* pPackFile;
	if (fopen_s(&pPackFile,strPackName.c_str(),"rb") != 0)
	{
		pPackFile = NULL;
		return false;
	}

	SPackageFileHeader sHeader;
	fread(&sHeader,SIZE_PACK_HEAD,1,pPackFile);
	if (feof(pPackFile))
	{
		fclose(pPackFile);
		pPackFile = NULL;
		return false;
	}

	fseek(pPackFile,0,SEEK_END);
	int iPackFileEnd = ftell(pPackFile);

	fseek(pPackFile,nDataEndPos,SEEK_SET);
	int nBufferSize = iPackFileEnd - nDataEndPos;
	BYTE* pBuffer = new BYTE[nBufferSize];

	fread(pBuffer,nBufferSize,1,pPackFile);
	if (feof(pPackFile))
	{
		delete [] pBuffer;
		pBuffer = NULL;
		fclose(pPackFile);
		pPackFile = NULL;
		return false;
	}
	fclose(pPackFile);
	pPackFile = NULL;

	BYTE* pEnctyptBuffer = new BYTE[nBufferSize];
	if (!EncryptBuffer(pBuffer,nBufferSize,pEnctyptBuffer,nBufferSize))
	{
		delete [] pBuffer;
		pBuffer = NULL;

		delete [] pEnctyptBuffer;
		pEnctyptBuffer = NULL;
		return false;
	}
	delete [] pBuffer;
	pBuffer = NULL;

	FILE* pExportFile;
	if (fopen_s(&pExportFile,strBackUpFileName.c_str(),"wb+") != 0)
	{
		delete [] pEnctyptBuffer;
		pEnctyptBuffer = NULL;
		pExportFile = NULL;
		return false;
	}

	// 首先写入dataend位置 [9/7/2009 hemeng]
	fwrite(&nDataEndPos,sizeof(UINT),1,pExportFile);
	// 写入文件信息系统存储位置 [9/7/2009 hemeng]
	fwrite(&sHeader,SIZE_PACK_HEAD,1,pExportFile);

	fwrite(pEnctyptBuffer,nBufferSize,1,pExportFile);

	delete [] pEnctyptBuffer;
	pEnctyptBuffer = NULL;

	fclose(pExportFile);
	pExportFile = NULL;

	return true;
}

bool	CReleasePatch::_BackUpPack(string strPackName,string strBackUpData)
{
	if (strPackName.empty() || strBackUpData.empty())
	{
		return false;
	}

	// 减去dataend位置存储的大小和文件信息存储位置数据的大小 [9/7/2009 hemeng]
	int nBufferSize = GetBufferSize(strBackUpData) - sizeof(UINT) - SIZE_PACK_HEAD;

	if (nBufferSize <= 0)
	{
		return false;
	}

	BYTE* pBuffer = new BYTE[nBufferSize];

	FILE* pFile;
	if (fopen_s(&pFile,strBackUpData.c_str(),"rb") != 0)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		pFile = NULL;

		return false;
	}


	UINT nDataEndPos = 0;
	SPackageFileHeader sHeader;
	fread(&nDataEndPos,sizeof(UINT),1,pFile);	
	fread(&sHeader,SIZE_PACK_HEAD,1,pFile);
	fread(pBuffer,nBufferSize,1,pFile);
	if (feof(pFile))
	{
		delete [] pBuffer;
		pBuffer = NULL;
		fclose(pFile);
		pFile = NULL;
		return false;
	}

	fclose(pFile);
	pFile = NULL;

	BYTE* pDecryptBuffer = new BYTE[nBufferSize];
	if (!DecryptBuffer(pBuffer,nBufferSize,pDecryptBuffer,nBufferSize))
	{
		delete [] pBuffer;
		pBuffer = NULL;
		delete [] pDecryptBuffer;
		pDecryptBuffer = NULL;
		return false;
	}
	delete [] pBuffer;
	pBuffer = NULL;


	FILE* pPackFile;
	if (fopen_s(&pPackFile,strPackName.c_str(),"rb+") != 0)
	{
		delete [] pDecryptBuffer;
		pDecryptBuffer = NULL;
		pPackFile = NULL;
		return false;
	}

	fseek(pPackFile,nDataEndPos,SEEK_SET);
	if (feof(pPackFile))
	{
		delete [] pDecryptBuffer;
		pDecryptBuffer = NULL;
		fclose(pPackFile);
		pPackFile = NULL;
		return false;
	}

	fwrite(pDecryptBuffer,nBufferSize,1,pPackFile);
	// 重写包头 [9/7/2009 hemeng]
	fseek(pPackFile,0,SEEK_SET);
	fwrite(&sHeader,SIZE_PACK_HEAD,1,pPackFile);

	fclose(pPackFile);
	pPackFile = NULL;
	delete [] pDecryptBuffer;
	pDecryptBuffer = NULL;

	return true;
}

bool CReleasePatch::_IsMuxLauncherEnabled()
{
	HANDLE hdLauncher = CreateMutex(
		NULL,
		TRUE,
		"MUX_LAUNCHER"
		);

	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		PATCH_ERROR_REPROT("更新失败！请关闭客户端再更新！","muxlauncher.exe");
		return true;
	}
	else
	{
		return false;
	}
}