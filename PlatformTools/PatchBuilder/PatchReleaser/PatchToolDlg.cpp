﻿// PatchToolDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "PatchTool.h"
#include "PatchToolDlg.h"
#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	
END_MESSAGE_MAP()


// CPatchToolDlg 对话框




CPatchToolDlg::CPatchToolDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPatchToolDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

}

void CPatchToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PROGRESS, m_UpdateProgress);
	DDX_Control(pDX, IDC_PROGRESS1, m_ImageProgress);
}

BEGIN_MESSAGE_MAP(CPatchToolDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_MESSAGE(MUXPATCH_ERROR_INIT, OnInitError)
	ON_MESSAGE(MUXPATCH_ERROR_CONTENTVERSION, OnVersionError)
	ON_MESSAGE(MUXPATCH_ERROR_UPDATEFAIL, OnUpdateError)
	ON_MESSAGE(MUXPATCH_ERROR_FINISHING, OnFinishError)
	ON_MESSAGE(MUXPATCH_UPDATE_INIT, OnInitUpdate)
	ON_MESSAGE(MUXPATCH_UPDATE_PATCHING, OnPatching)
	ON_MESSAGE(MUXPATCH_UPDATE_FINISHING, OnFinishing)
	ON_MESSAGE(MUXPATCH_FINISH, OnFinished)
	ON_MESSAGE(MUXPATCH_ERROR_ALREADYEXIST,OnAlreadyExist)

	ON_BN_CLICKED(IDC_BTN_RELEASEPATH, &CPatchToolDlg::OnBnClickedBtnReleasepath)
	ON_BN_CLICKED(IDC_BTN_UPDATE, &CPatchToolDlg::OnBnClickedBtnUpdate)


	ON_BN_CLICKED(IDCANCEL, &CPatchToolDlg::OnBnClickedCancel)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CPatchToolDlg 消息处理程序

BOOL CPatchToolDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	m_ImageProgress.SetRange(0,100);
	m_ImageProgress.SetPos(0);

	if(m_ImageProgress.InitImages(AfxGetInstanceHandle(),IDR_PNG_GREEN,IDR_PNG_GRAD,"PNG"))
	{
		m_ImageProgress.SetShowImage(TRUE);
	}

	if (!LoadBackgroundPicture())
	{
		return false;
	}
	// TODO: 在此添加额外的初始化代码
	m_pkReportSender = CPatchReportSender::GetInstance();
	if (!m_pkReportSender->Initial(this->m_hWnd))
	{
		return false;
	}
	
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

bool	CPatchToolDlg::LoadBackgroundPicture()
{
	HINSTANCE hInstace = AfxGetInstanceHandle();

	UINT nID = IDB_MUXLOG;

	m_pBkImage = new CBitmap();
	m_pBkImage->LoadBitmap(nID);

	return true;

}

bool	CPatchToolDlg::DrawPicture(CWnd* pCtrl,CBitmap* pBitmap)
{
	if (NULL == pCtrl || NULL == pBitmap)
	{
		return false;
	}

	if (NULL == pCtrl->GetSafeHwnd())
	{
		return false;
	}

	CRect rect;
	CDC* pDC = pCtrl->GetDC();
	pCtrl->GetClientRect(&rect);

	CDC memDC;
	CBitmap* pOld;
	BITMAP bm;

	pBitmap->GetBitmap(&bm);	
	memDC.CreateCompatibleDC(pDC);
	pOld = memDC.SelectObject(pBitmap);

	pDC->StretchBlt(0,0,rect.Width(),rect.Height(),&memDC,0,0,bm.bmWidth,bm.bmHeight,SRCCOPY);

	memDC.SelectObject(pOld);
	pCtrl->ReleaseDC(pDC);

	return true;
}

void CPatchToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CPatchToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
	
	if (m_pBkImage)
	{
		DrawPicture(GetDlgItem(IDC_BKPIC),m_pBkImage);
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CPatchToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

string CPatchToolDlg::GetDir()
{
	//查找文件夹
	string strPath;
	BROWSEINFO	bi;

	LPITEMIDLIST pidl;
	LPMALLOC	pMalloc;

	TCHAR szDirPath[MAX_PATH];

	if ( SUCCEEDED( SHGetMalloc(&pMalloc) ) )
	{
		ZeroMemory( &bi,sizeof(bi) );

		bi.hwndOwner = this->m_hWnd;
		bi.pszDisplayName = 0;
		bi.pidlRoot = 0;
		bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_VALIDATE;

		pidl = SHBrowseForFolder( &bi );

		if ( pidl )
		{
			if ( !SHGetPathFromIDList( pidl, szDirPath ) )
			{
				//ShowMessage("读取文件夹路径出错！");
				return "";
			}
			else
			{
				strPath = string( szDirPath );
			}
		}

		pMalloc->Free( pidl );
		pMalloc->Release();
	}

	return strPath;
}


void CPatchToolDlg::OnBnClickedBtnReleasepath()
{
	// TODO: 在此添加控件通知处理程序代码
	string strReleasePath = GetDir();

	if (strReleasePath == "")
	{
		return;
	}

	strncpy_s( m_pReleasePath, (LPCTSTR)strReleasePath.c_str(),sizeof(m_pReleasePath) );		

	GetDlgItem( IDC_EDIT2 )->SetWindowText( m_pReleasePath );
}

unsigned __stdcall  CPatchToolDlg::PatchThreadProc(void* pParam)
{
	CPatchToolDlg* pkDlg = (CPatchToolDlg*)pParam;

	CReleasePatch* pkPatch = new CReleasePatch(pkDlg->m_pReleasePath);

	if (NULL == pkPatch)
	{
		return 1;
	}

	bool bSuccse = pkPatch->ReleasePatchPackage();

	delete pkPatch;
	pkPatch = NULL;

	if (bSuccse)
	{
		pkDlg->m_pkReportSender->SendUpdateMessage(MUXPATCH_FINISH,MUXPATCH_PROGRESS_END);
	}
	

	return 0;
}

void CPatchToolDlg::OnBnClickedBtnUpdate()
{
	// TODO: 在此添加控件通知处理程序代码

	// 按钮失效 [8/25/2009 hemeng]
	GetDlgItem(IDC_BTN_UPDATE)->EnableWindow(FALSE);
	GetDlgItem(IDCANCEL)->EnableWindow(FALSE);

	std::locale::global(std::locale(""));

	GetDlgItem( IDC_EDIT2 )->GetWindowText(m_pReleasePath,MAX_PATH);
	

	if (m_pReleasePath == "")
	{
		return;
	}

	// 检查是否目录是否存在，不存在则创建 [8/10/2009 hemeng]
	if (_access_s(m_pReleasePath,0) !=0)
	{
		if (_mkdir(m_pReleasePath) != 0)
		{
			//PATCH_MEG_REPROT( "更新目录不存在或不可写！",m_pReleasePath);
			m_pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,(LPARAM)MUXPATCH_PROGRESS_BEGIN);
			return;
		}		
	}

	//MessageBox("更新开始");
	HANDLE hThread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)PatchThreadProc,this,TRUE,NULL);
	//MessageBox("更新结束");

	//GetDlgItem(IDCANCEL)->EnableWindow(TRUE);	
}

LRESULT	CPatchToolDlg::OnInitError(WPARAM wParam, LPARAM lParam)
{
	m_UpdateProgress.Clear();
	m_UpdateProgress.SetSel(0,-1);
	m_UpdateProgress.ReplaceSel("初始化失败，更新失败",false);

	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	return 0;
}

LRESULT CPatchToolDlg::OnVersionError(WPARAM wParam, LPARAM lParam)
{
	m_UpdateProgress.Clear();
	m_UpdateProgress.SetSel(0,-1);
	m_UpdateProgress.ReplaceSel("当前版本与更新包版本不符，不可更新",false);


	MessageBox("更新包不适用于当前版本");
	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	return 0;
}

LRESULT CPatchToolDlg::OnUpdateError(WPARAM wParam, LPARAM lParam)
{
	m_UpdateProgress.Clear();
	m_UpdateProgress.SetSel(0,-1);
	m_UpdateProgress.ReplaceSel("更新过程中出错，更新失败！",false);
	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	return 0;
}

LRESULT	CPatchToolDlg::OnFinishError(WPARAM wParam, LPARAM lParam)
{
	m_UpdateProgress.Clear();
	m_UpdateProgress.SetSel(0,-1);
	m_UpdateProgress.ReplaceSel("结束更新时出错，更新失败",false);

	MessageBox("更新失败！");	
	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	return 0;
}

LRESULT CPatchToolDlg::OnInitUpdate(WPARAM wParam, LPARAM lParam)
{
	int iProgress = (int)lParam;
	string strMsg = "初始化更新：" + CPatchHelper::MyIntToString(iProgress) + "%";

	m_UpdateProgress.Clear();
	m_UpdateProgress.SetSel(0,-1);
	m_UpdateProgress.ReplaceSel(strMsg.c_str(),false);

	int iPrecent = MUXPATCH_PRECENT_INIT + (iProgress / MUXPATCH_PERCENT_OFFSET);
	m_ImageProgress.SetPos(iPrecent);

	return 0;
}

LRESULT CPatchToolDlg::OnPatching(WPARAM wParam, LPARAM lParam)
{
	int iProgress = (int)lParam;
	string strMsg = "正在更新：" + CPatchHelper::MyIntToString(iProgress) + "%";

	m_UpdateProgress.Clear();
	m_UpdateProgress.SetSel(0,-1);
	m_UpdateProgress.ReplaceSel(strMsg.c_str(),false);

	int iPrecent = MUXPATCH_PRECENT_PATCHING + (iProgress / MUXPATCH_PERCENT_OFFSET);;
	m_ImageProgress.SetPos(iPrecent);

	return 0;
}

LRESULT CPatchToolDlg::OnFinishing(WPARAM wParam, LPARAM lParam)
{
	int iProgress = (int)lParam;
	string strMsg = "正在结束更新：" + CPatchHelper::MyIntToString(iProgress) + "%";

	m_UpdateProgress.Clear();
	m_UpdateProgress.SetSel(0,-1);
	m_UpdateProgress.ReplaceSel(strMsg.c_str(),false);

	int iPrecent = MUXPATCH_PRECENT_FINISHING + (iProgress / MUXPATCH_PERCENT_OFFSET);;
	m_ImageProgress.SetPos(iPrecent);

	return 0;
}

LRESULT CPatchToolDlg::OnFinished(WPARAM wParam, LPARAM lParam)
{
	int iProgress = (int)lParam;
	string strMsg = "更新完成：" + CPatchHelper::MyIntToString(iProgress) + "%";

	m_UpdateProgress.Clear();
	m_UpdateProgress.SetSel(0,-1);
	m_UpdateProgress.ReplaceSel(strMsg.c_str(),false);

	m_ImageProgress.SetPos(MUXPATCH_PRECENT_FINISHN);

	MessageBox("更新完毕！");	
	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	return 0;
}

void CPatchToolDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码

	if (m_pkReportSender)
	{
		//delete m_pkReportSender;
		m_pkReportSender = NULL;
	}

	OnCancel();
}

void CPatchToolDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	//if (m_pkReportSender)
	//{
	//	//delete m_pkReportSender;
	//	m_pkReportSender = NULL;
	//}

	//CDialog::OnClose();

	OnCancel();
}

LRESULT CPatchToolDlg::OnAlreadyExist(WPARAM wParam, LPARAM lParam)
{
	MessageBox("更新程序已经运行","错误");
	OnClose();

	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	return 0;
}
void CPatchToolDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
	if (m_pBkImage)
	{
		delete m_pBkImage;
		m_pBkImage = NULL;
	}
}
