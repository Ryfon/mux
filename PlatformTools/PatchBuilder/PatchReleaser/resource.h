﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PatchTool.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PATCHTOOL_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDR_RCDATA1                     132
#define IDR_RCDATA2                     133
#define IDR_PNG_GREEN                   135
#define IDR_PNG_GRAD                    136
#define IDB_BITMAP1                     140
#define IDB_MUXLOG                      140
#define IDC_EDIT1                       1000
#define IDC_EDIT_PROGRESS               1000
#define IDC_EDIT2                       1001
#define IDC_BTN_PACKPATH                1002
#define IDC_BTN_RELEASEPATH             1003
#define IDC_BUTTON3                     1004
#define IDC_BTN_UPDATE                  1004
#define IDC_PROGRESS1                   1005
#define IDC_BKPIC                       1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
