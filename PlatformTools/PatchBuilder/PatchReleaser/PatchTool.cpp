﻿// PatchTool.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "PatchTool.h"
#include "PatchToolDlg.h"

#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPatchToolApp

BEGIN_MESSAGE_MAP(CPatchToolApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CPatchToolApp 构造

CPatchToolApp::CPatchToolApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CPatchToolApp 对象

CPatchToolApp theApp;


// CPatchToolApp 初始化

BOOL CPatchToolApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	// 添加控制台调用方式 [8/10/2009 hemeng]
	LPSTR strCommand = m_lpCmdLine;
	vector<string> vCommands;
	// 删除保护参数的前双引号 [3/15/2010 hemeng]
	strCommand = strCommand ++;
	_SplitString(strCommand,vCommands,'//');

	// 参数2为要释放的路径，参数0为回调线程ID，参数1为launcher线程ID [8/21/2009 hemeng]
	if (vCommands.size() > 2)
	{
		if (vCommands[0] != "" && vCommands[1] != "" && vCommands[2] != "")
		{
			DWORD dwThreadID = CPatchHelper::MyStringToDWORD(vCommands[vCommands.size() - 2]);
			UINT nLauncherID = 0;
			nLauncherID = (UINT)CPatchHelper::MyStringToDWORD(vCommands[vCommands.size() - 1]);			
			
			string strReleasePatch = vCommands[0];
			if (vCommands.size() > 3)
			{
				for (unsigned int uiIndex = 1; uiIndex < (vCommands.size() - 2); uiIndex++)
				{
					strReleasePatch += " ";
					strReleasePatch += vCommands[uiIndex];
				}
			}
			if(_ProcessReleasePatching(strReleasePatch,dwThreadID,nLauncherID) == false)
			{
				ERROR_PRINT("Package failed!",vCommands[0].c_str());
			}
			
		}
	}

	else
	{
		CPatchToolDlg dlg;
		m_pMainWnd = &dlg;
		INT_PTR nResponse = dlg.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: 在此处放置处理何时用“确定”来关闭
			//  对话框的代码
		}
		else if (nResponse == IDCANCEL)
		{
			// TODO: 在此放置处理何时用“取消”来关闭
			//  对话框的代码
		}
	}
	
	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

void CPatchToolApp::_SplitString(const char * szStr, vector< string >& vStrs, char chToken )
{
	if (NULL == szStr)
	{
		return;
	}

	string strCur;
	for ( UINT nIndex = 0; nIndex < strlen( szStr ); nIndex ++ )
	{
		if ( chToken == szStr[nIndex] )
		{
			vStrs.push_back( strCur );
			strCur = "";
		}
		else
		{
			strCur.push_back( szStr[nIndex] );
		}
	}

	if ( strCur.length() >= 1 )
		vStrs.push_back( strCur );
}

bool CPatchToolApp::_ProcessReleasePatching(string strReleasePath,DWORD dwThreadID,UINT nLaunchID)
{
	if (strReleasePath == "" || dwThreadID == 0)
	{
		return false;
	}

	CPatchReportSender* pkReportSender = CPatchReportSender::GetInstance();
	if (!pkReportSender->Initial(dwThreadID))
	{
		return false;
	}

	if (NULL == pkReportSender)
	{
		return false;
	}

	// 检查是否目录是否存在，不存在则创建 [8/10/2009 hemeng]
	if (_access_s(strReleasePath.c_str(),0) !=0)
	{
		if (_mkdir(strReleasePath.c_str()) != 0)
		{
			PATCH_MEG_REPROT( "更新目录不存在或不可写！",strReleasePath.c_str() );
			pkReportSender->SendUpdateMessage(MUXPATCH_ERROR_INIT,(LPARAM)MUXPATCH_PROGRESS_BEGIN);
			return false;
		}		
	}

	CReleasePatch* pkPatch = new CReleasePatch(strReleasePath.c_str());

	if (NULL == pkPatch)
	{
		return false;
	}

	pkPatch->SetLaunchID(nLaunchID);
	pkPatch->ReleasePatchPackage();

	delete pkPatch;
	pkPatch = NULL;

	pkReportSender->SendUpdateMessage(MUXPATCH_FINISH,MUXPATCH_PROGRESS_END);

	if (pkReportSender)
	{
		//delete pkReportSender;
		pkReportSender = NULL;
	}

	return true;
}
