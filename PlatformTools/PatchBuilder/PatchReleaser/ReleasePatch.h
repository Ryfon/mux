﻿/** @file ReleasePatch.h 
@brief 文件打包：patch工具 第一版测试
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-08-06 
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

// 说明 [8/6/2009 hemeng]
// 必须将打包配置文件至于可执行文件同目录下

#pragma once

#include "Package.h"
#include "PatchDef.h"	
#include "tinyxml.h"
#include "PatchHelper.h"
#include "resource.h"
#include "PatchReportSender.h"
#include "PatchLog.h"
#include "ResourceHelper.h"

class CReleasePatch
{
public:
	CReleasePatch(const char* pszReleasePath);
	~CReleasePatch(void);

	bool	ReleasePatchPackage();
	string	GetPatchLastError()	{return m_strLastError;};
	void	Destroy();
	void	SetLaunchID(UINT nID);

protected:
	CReleasePatch(void);

	// 比较类型 [8/17/2009 hemeng]
	enum PATCH_COMPARE_RESULT_TYPE
	{
		PCRT_ADD_FILE = 0,
		PCRT_DELETE_FILE,
		PCRT_UPDATE_FILE,
		PCRT_DELETE_DIR,
	};

	// 初始化 [8/6/2009 hemeng]	
	bool		_InitUpdating();

	bool		_IsPackageFile(string strFileName);

	// 检查release目录下的version是否需要更新
	bool		_NeedUpdate();

	// 获得包文件的相对路径 [8/31/2009 hemeng]
	string		_GetPackNameByFileName(string strFileName);

	bool		_Patch();

	bool		_AddFiles();

	bool		_UpdateFiles();

	bool		_DeleteUnuseFiles();

	bool		_DeleteUnuseDirs();

	// 检查是否目录下的所有PKG文件 [8/6/2009 hemeng]
	CPackage*	_LoadPackage(string strPackFilePath);

	// 从文件读取log数据 [8/6/2009 hemeng]
	bool		_LoadLogFile();

	void		_LoadCheckFiles(TiXmlElement* pElmt);

	void		_LoadVersion(TiXmlElement* pElmt);

	bool		_LoadFileList(TiXmlElement* pElmt,PATCH_COMPARE_RESULT_TYPE nType);

	// 获取当前版本 [8/19/2009 hemeng]
	bool		_LoadCurrentVersion(string strPatchClientFileName);

	// 转换文件路径为绝对路径 [8/6/2009 hemeng]
	string		_ConvertMediaFileName(const string strFilePath);

	// 获取相对路径 [8/6/2009 hemeng]
	string		_ConvertRelPath(const string strFilePath);

	// 装载安装位置下的所有包文件 [8/10/2009 hemeng]
	bool		_InitPackages();

	// 添加包数据 [8/10/2009 hemeng]
	bool		_AddFilesToPackage(string strPackFileName,vector<string> vFileList);

	// strFileName为更新包中的文件名称 [8/31/2009 hemeng]
	bool		_AddFileToPackage(CPackage* pkPackage,string strFileName);

	// 删除包数据 [8/10/2009 hemeng]
	bool		_DeleteFilesFromPackage(string strPackFileName,vector<string> vFileList);

	bool		_DeleteFileFromPackage(CPackage* pkPackage,string strFileName);

	bool		_DeleteDirsFromPackage(string strPackFileName,vector<string> vDirList);

	bool		_DeleteDirFromPackage(CPackage* pkPackge,SPackageDirInfo* pkDirInfo);

	// 添加文件数据 [8/10/2009 hemeng]
	bool		_AddFile(string strFileName);

	bool		_UpdatePatchClientXML(string strXmlFilePath);

	// Load local resource [8/21/2009 hemeng]
	bool		_CreateUpdateResourcePackage(string& strResFileName);
	void*		_LoadResourceInfo(DWORD& dwResSize);

	bool		_LoadPackConfigFile();

	// 划分包文件及文件 [8/31/2009 hemeng]
	void		_PartFiles(vector<string> vFiles,map<string,vector<string>>& mapPackFiles,vector<string>& vPathFiles);

	// 备份包，以防更新中出错 [9/1/2009 hemeng]
	// bOK = true - 原包正常，仅做备份 [9/1/2009 hemeng]
	// bOK = false - 原包错误，尝试恢复
	// 操作成功则返回备份包路径，反之返回空字符串 [9/1/2009 hemeng]
	string		_SecurityPack(string strPackName);
	// 创建备份文件系统 [9/7/2009 hemeng]
	bool		_MakeBackUpFile(string strPackName,string strBackUpFileName);
	// 由备份文件恢复文件系统 [9/7/2009 hemeng]
	bool		_BackUpPack(string strPackName,string strBackUpData);

	// 检查是否为客户端目录 [9/2/2009 hemeng]
	bool		_IsClientDir();

	// 检查muxlauncher是否启动 [12/15/2009 hemeng]
	bool		_IsMuxLauncherEnabled();

protected:
	CPackage*				m_pkPackage;			// patch包文件 [8/6/2009 hemeng]
	string					m_strReleasePath;		// 释放目录 [8/6/2009 hemeng]
	CPatchLog*				m_pLog;					// 更新日志 [9/4/2009 hemeng]

	// 所有需要删除、更新、添加的文件及文件夹 [8/6/2009 hemeng]
	// key为类型（删除、添加etc） 值为文件 key都为相对路径
	map<PATCH_COMPARE_RESULT_TYPE,vector<string>> m_mapUpdateFiles;
	string					m_NewVersion;
	string					m_OldVersion;
	string					m_CurrVersion;
	bool					m_bInit;
	BYTE*					m_pConfigFileBuffer;			// package_config.xml数据
	CPatchReportSender*		m_pkReportSender;
	HANDLE					m_hdSigleObject;				// 标志patch releaser唯一实例 [8/25/2009 hemeng]
	bool					m_bCreateLauncherThread;		// 如果需要更新luncher，则需在退出更新时启动luncher [8/25/2009 hemeng]
	string					m_strLastError;					// 最后一个错误 [8/25/2009 hemeng]
	UINT					m_nLaunchID;					// lanuch的线程ID [8/31/2009 hemeng]
	string					m_strLauncherName;
	map<string,string>		m_mapPackages;					// 由配置文件获得的客户端目录中应为包文件夹 [8/31/2009 hemeng]
	vector<string>			m_vCheckFiles;					// 用于校验是否为客户端目录的文件 [9/2/2009 hemeng]
};
