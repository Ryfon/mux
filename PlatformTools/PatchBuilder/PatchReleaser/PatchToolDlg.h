﻿// PatchToolDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "ImageProgressCtrl.h"


// CPatchToolDlg 对话框
class CPatchToolDlg : public CDialog
{
// 构造
public:
	CPatchToolDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_PATCHTOOL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()
	bool	LoadBackgroundPicture();
	bool	DrawPicture(CWnd* pCtrl,CBitmap* pImage);
public:
	afx_msg void OnBnClickedBtnPackpath();
	string GetDir();

protected:
	afx_msg HCURSOR OnQueryDragIcon();

	afx_msg LRESULT OnInitError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnVersionError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFinishError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnInitUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPatching(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFinishing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFinished(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAlreadyExist(WPARAM wParam, LPARAM lParam);

	// patch releaser线程函数 [8/25/2009 hemeng]
	static unsigned __stdcall PatchThreadProc(void* pParam);
	char m_pReleasePath[MAX_PATH];
public:
	afx_msg void OnBnClickedBtnReleasepath();
	afx_msg void OnBnClickedBtnUpdate();

	CEdit m_UpdateProgress;
	CPatchReportSender* m_pkReportSender;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnClose();
	CImageProgressCtrl m_ImageProgress;
	CBitmap* m_pBkImage;
	afx_msg void OnDestroy();
};
