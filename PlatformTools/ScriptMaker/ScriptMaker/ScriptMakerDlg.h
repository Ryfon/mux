﻿// ScriptMakerDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "FileExporter/FileExporter.h"
#include "lxEdit.h"

// CScriptMakerDlg 对话框
class CScriptMakerDlg : public CDialog
{
// 构造
public:
	CScriptMakerDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_SCRIPTMAKER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton1();
	void UpdateFileList(vector<string> fileList);
	ClxEdit m_TemplateFilePath;
	ClxEdit m_RuleFilePath;
	CListCtrl m_OutFileListCtrl;
	afx_msg void OnBnClickedCheck1();
	BOOL m_bCover;
	CFileExporter * m_Exporter;
	afx_msg void OnBnClickedOk();
	CString m_strAppDir;	// 保存可执行程序的路径
	afx_msg void OnEnChangeXmlpath();
};
