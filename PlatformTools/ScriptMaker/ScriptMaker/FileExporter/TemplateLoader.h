﻿#pragma once
#define LINE_MAX 1024
#include <vector>
using namespace std;

typedef struct stArea
{
	TiXmlElement* pXmlStart;
	TiXmlElement* pXmlEnd;
	int iType;
	string strKeyWord;
	string strPriCol;
}stArea;

class CTemplateLoader
{
public:
	CTemplateLoader(void);
	~CTemplateLoader(void);
	bool LoadFromLua(const char * fileName);
	bool LoadFromXml(const char * fileName);
	bool SaveFile(const char * fileName);
	TiXmlElement * GetLineByID(int id);
private:
	char * isArea(char * Line);
	int isVariable(char * Line);
	char * isSpecialArea(char * Line, int & iType);//通过@的数量，判断区域的类型，并返回该区域的关键字
	TiXmlElement * FindEnd(TiXmlElement * pXmlStart);
	int GetVariable(char * LineBuffer, int StartIndex, int EndIndex, char * VariableBuffer);
	void _deleteXmlDoc();
	bool DivideArea();
	char * isElseTag(char * czKeyWord);	// 判断关键字是否被“#”标记，带“#”标记的为else符号，返回去掉“#”后的结果
	char * hasPrimaryColum(char * czKeyWord);	// 判断关键字是否带“$”符号，返回“$”符号后面的字符串
	bool AddJump();// 对@@进行if...else...end的处理，添加jump属性
public:
	TiXmlDocument * m_XmlDoc;
	vector<stArea*> m_AreaList;
private:
	int m_VariableArr[32];//公用数组，用于存储一行中变量的分割点。'@'
};
