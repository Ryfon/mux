﻿#include "StdAfx.h"
#include "RuleLoader.h"

CRuleLoader::CRuleLoader(void)
{
	m_XmlDoc = NULL;
}

CRuleLoader::~CRuleLoader(void)
{
	if (m_XmlDoc != NULL)
	{
		delete m_XmlDoc;
		m_XmlDoc = NULL;
	}
}

bool CRuleLoader::LoadRule(const char * fileName)
{
	if (m_XmlDoc != NULL)
	{
		delete m_XmlDoc;
		m_XmlDoc = NULL;
	}

	m_XmlDoc = new TiXmlDocument();

	if (m_XmlDoc->LoadFile(fileName))
	{
		FixRule();
		SaveRule("RuleBak.xml");	//导出文件，调试用
		return true;
	}
	else
	{
		delete m_XmlDoc;
		m_XmlDoc = NULL;
		return false;
	}
}

bool CRuleLoader::SaveRule(const char * fileName)
{
	if (m_XmlDoc == NULL)
	{
		return false;
	}
	else
	{
		return m_XmlDoc->SaveFile(fileName);
	}
}

bool CRuleLoader::FixRule()
{
	if (m_XmlDoc == NULL)
	{
		return false;
	}

	TiXmlElement * pXmlRuleRoot = m_XmlDoc->RootElement();
	const char * czLastFileName = NULL;
	const char * czCurrentFileName = NULL;

	// 补充缺省的文件名
	for (TiXmlElement * pXmlRule = pXmlRuleRoot->FirstChildElement();pXmlRule;pXmlRule = pXmlRule->NextSiblingElement())
	{
		czCurrentFileName = pXmlRule->Attribute("FileName");	
		if (czCurrentFileName != NULL)
		{
			if (strcmp(czCurrentFileName, "") == 0)
			{
				pXmlRule->SetAttribute("FileName", czLastFileName);
			}
			else
			{
				czLastFileName = czCurrentFileName;
			}
		}
	}

	// 生成新的文件
	TiXmlDocument * NewDoc = new TiXmlDocument();
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	NewDoc->InsertEndChild(*pXmlDec);
	TiXmlElement * pNewRoot = new TiXmlElement("root");
	NewDoc->LinkEndChild(pNewRoot);
	TiXmlNode * pNewRule = NULL;
	TiXmlElement* pNewElementRule = NULL;

	// 加入多文件名的支持，文件名用分号分割
	for (TiXmlElement * pXmlRule = pXmlRuleRoot->FirstChildElement();pXmlRule;pXmlRule = pXmlRule->NextSiblingElement())
	{
		czCurrentFileName = pXmlRule->Attribute("FileName");
		
		if (czCurrentFileName != NULL)
		{
			string strMulFileName(czCurrentFileName);
			string strSingleFileName;
			size_t index = strMulFileName.find(";");
			while (index != string::npos)
			{
				// 获得文件名
				strSingleFileName = strMulFileName.substr(0, index);
				pNewRule = pXmlRule->Clone();
				pNewElementRule = pNewRule->ToElement();
				pNewElementRule->SetAttribute("FileName", strSingleFileName.c_str());
				pNewRoot->LinkEndChild(pNewElementRule);

				// 将获得的文件名去除，进行下一次的遍历
				strMulFileName = strMulFileName.substr(index + 1, strMulFileName.length() - index - 1);
				index = strMulFileName.find(";");
			}

			// 最后的文件名
			pNewRule = pXmlRule->Clone();
			pNewElementRule = pNewRule->ToElement();
			pNewElementRule->SetAttribute("FileName", strMulFileName.c_str());
			pNewRoot->LinkEndChild(pNewElementRule);

		}
		else
		{
			// 没有filename,直接复制节点
			pNewRule = pXmlRule->Clone();
			pNewRoot->LinkEndChild(pNewRule);
		}
	}

	// 用新的文件替换老的
	delete m_XmlDoc;
	m_XmlDoc = NewDoc;

	return true;
}

bool CRuleLoader::GetRules(std::string strFileNum, int iType, std::string strKeyWord, std::string strPriCol, std::vector<TiXmlElement*>& RuleList)
{
	if (m_XmlDoc == NULL)
	{
		return false;
	}

	TiXmlElement* pXmlRoot = m_XmlDoc->RootElement();
	TiXmlElement* pXmlRule = pXmlRoot->FirstChildElement();

	for (;pXmlRule;pXmlRule = pXmlRule->NextSiblingElement())
	{
		if (iType == 0)
		{
			if (strcmp(pXmlRule->Value(), "Normal") == 0)
			{
				if (strcmp(pXmlRule->Attribute("FileName"), strFileNum.c_str()) == 0)
				{
					RuleList.push_back(pXmlRule);
				}
			}
		}

		if (iType == 3)
		{
			if (strcmp(pXmlRule->Value(), strKeyWord.c_str()) == 0)
			{
				if (strcmp(pXmlRule->Attribute("FileName"), strFileNum.c_str()) == 0)
				{
					RuleList.push_back(pXmlRule);
				}
			}
		}

		if (iType == 4)
		{
			if (strcmp(pXmlRule->Value(), strKeyWord.c_str()) == 0)
			{	
				// 指定PriCol，根据filename取rule
				if (strPriCol.length() != 0)
				{
					if (pXmlRule->Attribute("FileName") != NULL)
					{
						if (strcmp(pXmlRule->Attribute("FileName"), strFileNum.c_str()) == 0)
						{
							RuleList.push_back(pXmlRule);
						}
					}
				}
			}
		}
	}

	// 将type为4的，带*的rule加入列表
	if (iType == 4)
	{
		pXmlRule = pXmlRoot->FirstChildElement();
		for (;pXmlRule;pXmlRule = pXmlRule->NextSiblingElement())
		{
			if (strcmp(pXmlRule->Value(), strKeyWord.c_str()) == 0)
			{	
				bool bIsRule = false;

				if (pXmlRule->Attribute("FileName") == NULL)
				{
					bIsRule = true;	
				}
				else if (strcmp(pXmlRule->Attribute("FileName"), "*") == 0)
				{
					bIsRule = true;
				}

				if (bIsRule)
				{
					bool bExist = false;

					const char * PriValue = pXmlRule->Attribute(strPriCol.c_str());
					int iSize = (int)RuleList.size();
					for (int index = 0;index < iSize;index++)
					{
						const char * tempValue = RuleList[index]->Attribute(strPriCol.c_str());
						if (PriValue != NULL && tempValue != NULL)
						{
							if (strcmp(PriValue, tempValue) == 0)
							{
								bExist = true;
								break;
							}
						}
					}

					if (!bExist)
					{
						RuleList.push_back(pXmlRule);
					}
				}
			}
		}
	}

	return true;
}

TiXmlElement* CRuleLoader::GetGlobalRule(string strFileNum)
{
	if (m_XmlDoc == NULL)
	{
		return NULL;
	}

	TiXmlElement* pXmlRoot = m_XmlDoc->RootElement();
	TiXmlElement* pXmlRule = pXmlRoot->FirstChildElement();

	for (;pXmlRule;pXmlRule = pXmlRule->NextSiblingElement())
	{
			if (strcmp(pXmlRule->Value(), "Normal") == 0)
			{
				if (strcmp(pXmlRule->Attribute("FileName"), strFileNum.c_str()) == 0)
				{
					return pXmlRule;
				}
			}
	}

	return NULL;
}