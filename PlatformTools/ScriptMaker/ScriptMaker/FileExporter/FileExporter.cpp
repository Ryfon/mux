﻿#include "StdAfx.h"
#include "FileExporter.h"
#include <fstream>
#include <vector>
using namespace std;

CFileExporter::CFileExporter(void)
{
	m_Template = new CTemplateLoader();
	m_Rule = new CRuleLoader();
}

CFileExporter::~CFileExporter(void)
{
	if (m_Template != NULL)
	{
		delete m_Template;
		m_Template = NULL;
	}

	if (m_Rule != NULL)
	{
		delete m_Rule;
		m_Rule = NULL;
	}
}

bool CFileExporter::LoadTemplateFile(const char * fileName, bool type)
{
	if (type)
	{
		return m_Template->LoadFromXml(fileName);
	} 
	else
	{
		return m_Template->LoadFromLua(fileName);
	}	
}

bool CFileExporter::LoadRuleFile(const char * fileName)
{
	// 设置输出目录
	string strTemp = fileName;
	m_strExportPath = strTemp.substr(0, strTemp.find_last_of("\\")+1);

	return m_Rule->LoadRule(fileName);
}

bool CFileExporter::ExportLuaFile(TiXmlElement * pXmlRule, BOOL bCover)
{
	const char * fileName = NULL;

	fileName = pXmlRule->Attribute("FileName");
	if (fileName == NULL)
	{
		return false;
	}

	string TotalFileName = _getFileName(fileName, bCover);
	ofstream outFile(TotalFileName.c_str());
	
	TiXmlElement * pXmlTemplateRoot = m_Template->m_XmlDoc->RootElement();

	int iCurrentRowNo = 1;
	bool bShow = true;
	const char * AreaStart = NULL;
	for (const TiXmlElement * pXmlLine = pXmlTemplateRoot->FirstChildElement();pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
	{
		int iRow = atoi(pXmlLine->Attribute("RowNo"));
		if (iRow != iCurrentRowNo)
		{
			outFile << "\n";
			iCurrentRowNo = iRow;
		}
		
		
		int iType = atoi(pXmlLine->Attribute("Type"));

		if (iType == 0)
		{
			if (_isEmptyVariable(pXmlRule, pXmlLine->Attribute("KeyWord")))
			{
				if (AreaStart == NULL)
				{
					AreaStart = pXmlLine->Attribute("KeyWord");
					bShow = false;
				}
				else
				{
					if (strcmp(AreaStart, pXmlLine->Attribute("KeyWord")) == 0)
					{
						AreaStart = NULL;
						bShow = true;
					}
					else
					{
						bShow = false;
					}
				}
			}
		}

		if (bShow)
		{
			if (iType == 1)
			{
				outFile << pXmlLine->GetText();
			}

			if (iType == 2)
			{
				outFile << pXmlRule->Attribute(pXmlLine->GetText());
			}
		}	
	}
	return true;
}

bool CFileExporter::ExportArea(stArea* pArea, string strFileName, ofstream& outFile)
{
	string strFileNum = _getFileNum(strFileName.c_str());
	
	if (m_Rule->m_XmlDoc == NULL)
	{
		return false;
	}

	vector<TiXmlElement*> RuleList;
	m_Rule->GetRules(strFileNum, pArea->iType,pArea->strKeyWord, pArea->strPriCol, RuleList);
	vector<TiXmlElement*>::iterator it = RuleList.begin();
	for (;it != RuleList.end();++it)
	{
		ExportAreaByRule(pArea,strFileNum, *it, outFile);
	}

	return true;
}

bool CFileExporter::ExportAreaByRule(stArea* pArea, string strFileNum, TiXmlElement * pXmlRule, ofstream& outFile)
{
	int iCurrentRowNo = 1;
	bool bShow = true;
	const char * AreaStart = NULL;
	TiXmlElement* pXmlGlobalRule = m_Rule->GetGlobalRule(strFileNum);

	for (const TiXmlElement * pXmlLine = pArea->pXmlStart;;pXmlLine = pXmlLine->NextSiblingElement())
	{
		int iRow = atoi(pXmlLine->Attribute("RowNo"));
		int iType = atoi(pXmlLine->Attribute("Type"));
		if (iType == 3 || iType == 4)
		{
			if (pXmlLine == pArea->pXmlStart)
			{
				continue;
			}

			if (pXmlLine == pArea->pXmlEnd)
			{
				break;
			}
		}

		
		if (iRow != iCurrentRowNo && iType < 2)
		{
			outFile << "\n";
			iCurrentRowNo = iRow;
		}

		if (iType == 2)
		{
			int iNextStep = 0;
			if (_isEmptyVariable(pXmlRule, pXmlLine->Attribute("KeyWord")) && _isEmptyVariable(pXmlGlobalRule, pXmlLine->Attribute("KeyWord")))
			{
				/*if (AreaStart == NULL)
				{
					AreaStart = pXmlLine->Attribute("KeyWord");
					bShow = false;
				}
				else
				{
					if (strcmp(AreaStart, pXmlLine->Attribute("KeyWord")) == 0)
					{
						AreaStart = NULL;
						bShow = true;
					}
					else
					{
						bShow = false;
					}
				}*/
				iNextStep = atoi(pXmlLine->Attribute("NoJump"));
			}
			else
			{
				iNextStep = atoi(pXmlLine->Attribute("YesJump"));
			}

			pXmlLine = m_Template->GetLineByID(iNextStep);

		}

		if (bShow)
		{
			if (iType == 0)
			{
				outFile << pXmlLine->GetText();
			}

			if (iType == 1)
			{
				// 获取局部变量的值
				const char* czOutPut = pXmlRule->Attribute(pXmlLine->GetText());

				// 局部变量不存在，寻找全局变量
				if (czOutPut == NULL)
				{
					czOutPut = pXmlGlobalRule->Attribute(pXmlLine->GetText());
				}

				// 输出变量
				outFile << czOutPut;
			}
		}	

		if (pXmlLine == pArea->pXmlEnd)
		{
			break;
		}
	}

	return true;
}

bool CFileExporter::ExportLuaFile(string strFileName)
{
	// 加入输出路径
	string strFullName;
	strFullName += m_strExportPath;
	strFullName += strFileName;
	ofstream outFile(strFullName.c_str());

	vector<stArea*>::iterator it = m_Template->m_AreaList.begin();
	for (;it != m_Template->m_AreaList.end();++it)
	{
		ExportArea(*it, strFileName, outFile);
	}

	return true;
}

bool CFileExporter::ExportALL(BOOL bCover)
{
	if (m_Rule->m_XmlDoc == NULL || m_Template->m_XmlDoc == NULL)
	{
		return false;
	}

	/*TiXmlElement * pXmlRuleRoot = m_Rule->m_XmlDoc->RootElement();

	for (TiXmlElement * pXmlRule = pXmlRuleRoot->FirstChildElement();pXmlRule;pXmlRule = pXmlRule->NextSiblingElement())
	{
		ExportLuaFile(pXmlRule, bCover);
	}*/
	vector<string>::iterator it = m_fileList.begin();
	for (;it != m_fileList.end();++it)
	{
		ExportLuaFile(*it);
	}

	return true;
}

bool CFileExporter::_FormatArea(TiXmlElement * pXmlRule)
{
	TiXmlElement * pXmlTemplateRoot = m_Template->m_XmlDoc->RootElement();
	vector<const char *> Stack;

	for (const TiXmlElement * pXmlLine = pXmlTemplateRoot->FirstChildElement();pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
	{
		int iRow = atoi(pXmlLine->Attribute("RowNo"));
		int iType = atoi(pXmlLine->Attribute("Type"));

		if (iType == 0)
		{
			Stack.push_back(pXmlLine->Attribute("KeyWord"));
		}
		
	}
	return true;
}

bool CFileExporter::_isEmptyVariable(TiXmlElement * pXmlRle, const char * variableName)
{
	const char * variableValue = pXmlRle->Attribute(variableName);
	
	if (variableValue == NULL)
	{
		return true;
	}

	if (strcmp(variableValue, "") == 0)
	{
		return true;
	}
	return false;
}

bool CFileExporter::GetFileList(BOOL bCover)
{
	if (m_Rule == NULL)
	{
		return false;
	} 
	else
	{
		if (m_Rule->m_XmlDoc == NULL)
		{
			return false;
		}

		TiXmlElement * pXmlRuleRoot = m_Rule->m_XmlDoc->RootElement();
		m_fileList.clear();
		for (TiXmlElement * pXmlRule = pXmlRuleRoot->FirstChildElement();pXmlRule;pXmlRule = pXmlRule->NextSiblingElement())
		{
			string strFileName = _getFileName(pXmlRule->Attribute("FileName"), bCover);
			
			if (strFileName.length() != 0 && strcmp(strFileName.c_str(), "*.lua") != 0)
			{
				m_fileList.push_back(strFileName);
			}
		}

		FixFileList();

		return true;
	}
}

string CFileExporter::_getFileName(const char * fileName, BOOL bCover)
{
	if (fileName == NULL)
	{
		string strNull;
		return strNull;
	}

	string strTempName(fileName);
	string strFileName;
	string strSuffixName = ".lua";

	strFileName += strTempName;
	strFileName += strSuffixName;

	if (bCover)
	{
		return strFileName;
	}

	// 加入输出路径
	string strFullName1;
	strFullName1 += m_strExportPath;
	strFullName1 += strFileName;
	ifstream infile;
	infile.open(strFullName1.c_str());

	int index = 1;
	char indexBuffer[8];

	while (infile)
	{
		strSuffixName = "";
		strSuffixName += "(";
		_itoa_s(index, indexBuffer, 10);
		strSuffixName += indexBuffer;
		strSuffixName += ").lua";

		strFileName = "";
		strFileName += strTempName;
		strFileName += strSuffixName;
		infile.close();

		string strFullName2;
		strFullName2 += m_strExportPath;
		strFullName2 += strFileName;
		infile.open(strFullName2.c_str());
	
		index++;
	}

	infile.close();
	return strFileName;
}

string CFileExporter::_getFileNum(const char* fileName)
{
	string strResult;
	
	for (int i = 0;;i++)
	{
		if (fileName[i] == '\0')
		{
			break;
		}
		if (fileName[i] == '.')
		{
			break;
		}
		if (fileName[i] == '(')
		{
			break;
		}
		
		strResult += fileName[i];
	}
	return strResult;
}

bool CFileExporter::FixFileList()
{
	// 将文件名放入vector，则可以去除重复
	vector<string> TempFileList;
	TempFileList.clear();
	vector<string>::iterator it = m_fileList.begin();
	for (;it != m_fileList.end();++it)
	{
		if ((*it).length() != 0)
		{
			bool bExist = false;
			vector<string>::iterator itTemp = TempFileList.begin();
			for (;itTemp != TempFileList.end();itTemp++)
			{
				if ( (*it).compare(*itTemp) == 0)
				{
					bExist = true;
				}
			}

			if (!bExist)
			{
				string strTemp(*it);
				TempFileList.push_back(strTemp);
			}
		}
	}

	// 重新更新文件列表
	m_fileList.clear();
	vector<string>::iterator itTemp = TempFileList.begin();
	for (;itTemp != TempFileList.end();itTemp++)
	{
		string strTemp(*itTemp);
		m_fileList.push_back(strTemp);
	}

	return true;
}

bool CFileExporter::SetExportPath(string strPath)
{
	m_strExportPath = strPath;
	return true;
}