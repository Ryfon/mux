﻿#include "StdAfx.h"
#include "TemplateLoader.h"
#include <fstream>
#include <vector>
using namespace std;

CTemplateLoader::CTemplateLoader(void)
{
	m_XmlDoc = NULL;
}

CTemplateLoader::~CTemplateLoader(void)
{
	_deleteXmlDoc();
}

bool CTemplateLoader::LoadFromLua(const char * fileName)
{
	std::locale::global(std::locale(""));

	ifstream inFile;
	char czBuffer[LINE_MAX];
	char czVariableBuffer[LINE_MAX];
	char * czKeyWord;
	char * index;

	_deleteXmlDoc();
	m_XmlDoc = new TiXmlDocument();
	TiXmlElement * pXmlLine;

	inFile.open(fileName);
	if (!inFile)
	{
		inFile.close();
		_deleteXmlDoc();
		return false;
	}

	//m_XmlDoc->LoadFile("TempT.xml");
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	m_XmlDoc->InsertEndChild(*pXmlDec);
	TiXmlElement * pXmlRoot = new TiXmlElement("Template");
	m_XmlDoc->LinkEndChild(pXmlRoot);



	int RowNo = 1;
	int RowID = 1;
	while (inFile.getline(czBuffer, LINE_MAX))
	{
		index = czBuffer;
		pXmlLine = new TiXmlElement("Module");

		pXmlLine->SetAttribute("RowNo", RowNo);
		//czKeyWord = isArea(czBuffer);
		int iAreaType = 0;
		czKeyWord = isSpecialArea(czBuffer, iAreaType);

		if (czKeyWord == NULL)
		{
			int VariableCount = isVariable(czBuffer);

			if (VariableCount == 0)
			{
				pXmlLine->SetAttribute("Type", 0);
				TiXmlText* pXmlText = new TiXmlText(czBuffer);
				pXmlLine->LinkEndChild(pXmlText);
				pXmlRoot->LinkEndChild(pXmlLine);
			}
			else
			{
				int iStartIndex = 0;
				int iEndIndex = 0;
				int iType = 0;
				for (int i = 0;i < VariableCount; i++)
				{
					iEndIndex = m_VariableArr[i];

					GetVariable(czBuffer, iStartIndex, iEndIndex,czVariableBuffer);

					pXmlLine = new TiXmlElement("Module");
					pXmlLine->SetAttribute("RowNo", RowNo);
					pXmlLine->SetAttribute("Type", iType);
					TiXmlText* pXmlText = new TiXmlText(czVariableBuffer);
					pXmlLine->LinkEndChild(pXmlText);

					pXmlLine->SetAttribute("ID", RowID);
					RowID++;
					pXmlRoot->LinkEndChild(pXmlLine);
					
					iStartIndex = iEndIndex + 1;
					if (iType == 0)
					{
						iType = 1;
					} 
					else
					{
						iType = 0;
					}
				}	
			}
		}
		else
		{
			pXmlLine->SetAttribute("Type", iAreaType);

			if (iAreaType == 2)
			{
				// 对于@@#...#@@的类型，进行特别的检测和处理
				char * czNewKeyWord = isElseTag(czKeyWord);

				// 不存在#标记
				if (czNewKeyWord == NULL)
				{
					pXmlLine->SetAttribute("KeyWord", czKeyWord);
					pXmlLine->SetAttribute("Tag", "");
				}
				else//存在#标记
				{
					pXmlLine->SetAttribute("KeyWord", czNewKeyWord);
					pXmlLine->SetAttribute("Tag", "else");
				}
			}

			if (iAreaType == 3)
			{
				pXmlLine->SetAttribute("KeyWord", czKeyWord);
			}
			
			if (iAreaType == 4)
			{
				char * czPriCol = hasPrimaryColum(czKeyWord);
		
				pXmlLine->SetAttribute("KeyWord", czKeyWord);

				if (czPriCol == NULL)
				{
					pXmlLine->SetAttribute("PriCol", "");
				} 
				else
				{
					pXmlLine->SetAttribute("PriCol", czPriCol);
				}
			}

			pXmlLine->SetAttribute("ID", RowID);
			RowID++;
			pXmlRoot->LinkEndChild(pXmlLine);
		}
		




		
		RowNo++;
	}

	// 处理if...else...end语句
	if (!AddJump())
	{
		return false;
	}

	// 划分区域
	DivideArea();

	// 导出文件，调试用
	//SaveFile("TemplateBak.xml");
	return true;
}

bool CTemplateLoader::LoadFromXml(const char * fileName)
{
	_deleteXmlDoc();
	m_XmlDoc = new TiXmlDocument();
	if (m_XmlDoc->LoadFile(fileName))
	{
		return true;
	}
	else
	{
		_deleteXmlDoc();
		return false;
	}
	
}

char * CTemplateLoader::isArea(char * Line)
{
	char * result = NULL;
	char * tail = NULL;
	result = strstr(Line, "@@");

	if ( result== NULL)
	{
		return NULL;
	}
	else
	{
		tail = strstr(&result[2], "@@");
		tail[0] = '\0';
		return &result[2];
	}

	return result;
}

int CTemplateLoader::isVariable(char * Line)
{
	int count = 0;
	int index = 0;
	for (;Line[index] != '\0';index++)
	{
		if (Line[index] == '@')
		{
			m_VariableArr[count] = index;
			count++;
		}
	}
	m_VariableArr[count] = index;
	count++;

	return count;
}

char * CTemplateLoader::isSpecialArea(char * Line, int & iType)
{
	char * result = NULL;
	iType = 0;

	int index = 0;
	for (;Line[index] != '\0';index++)
	{
		if (Line[index] == '@')
		{
			iType++;
		}
		else
		{
			if (iType != 0)
			{
				result = &Line[index];
				break;
			}
		}
	}

	if (iType < 2)
	{
		return NULL;
	}

	for (;Line[index] != '\0';index++)
	{
		if (Line[index] == '@')
		{
			Line[index] = '\0';
			break;
		}
	}

	return result;
}

TiXmlElement * CTemplateLoader::FindEnd(TiXmlElement * pXmlStart)
{
	TiXmlElement * pXmlEnd = NULL;
	TiXmlElement * pXmlLine = pXmlStart->NextSiblingElement();
	const char * StartWord;
	const char * EndWord;

	int iType = atoi(pXmlStart->Attribute("Type"));
	if (iType == 0)
	{
		pXmlEnd = pXmlStart;
	}
	else
	{
		StartWord = pXmlStart->Attribute("KeyWord");

		while (pXmlLine)
		{
			EndWord = pXmlLine->Attribute("KeyWord");
			if (EndWord != NULL)
			{

				if (strcmp(StartWord, EndWord) == 0)
				{
					pXmlEnd = pXmlLine;
					break;
				}
			}
			pXmlLine = pXmlLine->NextSiblingElement();
		}

	}

	return pXmlEnd;
}

int CTemplateLoader::GetVariable(char * LineBuffer, int StartIndex, int EndIndex, char * VariableBuffer)
{
	int iVariableLength = 0;

	for (int index = StartIndex; index < EndIndex;index++)
	{
		VariableBuffer[iVariableLength] = LineBuffer[index];
		iVariableLength++;
	}
	
	VariableBuffer[iVariableLength] = '\0';

	return iVariableLength;
}

void CTemplateLoader::_deleteXmlDoc()
{
	if (m_XmlDoc != NULL)
	{
		delete m_XmlDoc;
		m_XmlDoc = NULL;
	}
}

bool CTemplateLoader::SaveFile(const char * fileName)
{
	if (m_XmlDoc == NULL)
	{
		return false;
	} 
	else
	{
		return m_XmlDoc->SaveFile(fileName);
	}
}

TiXmlElement * CTemplateLoader::GetLineByID(int id)
{
	if (m_XmlDoc == NULL)
	{
		return NULL;
	}

	TiXmlElement* pXmlRoot = m_XmlDoc->RootElement();

	TiXmlElement* pXmlLine = pXmlRoot->FirstChildElement();
	for (;pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
	{
		int iID = atoi(pXmlLine->Attribute("ID"));

		if (id == iID)
		{
			return pXmlLine;
		}
	}

	return NULL;
}

bool CTemplateLoader::DivideArea()
{
	m_AreaList.clear();

	if (m_XmlDoc == NULL)
	{
		return false;
	}

	TiXmlElement* pXmlRoot = m_XmlDoc->RootElement();

	TiXmlElement* pXmlLine = pXmlRoot->FirstChildElement();
	// 初始化区域结构体
	stArea* pArea = new stArea();
	pArea->pXmlStart = NULL;
	pArea->pXmlEnd =NULL;
	//
	for (;pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
	{
		int iType = atoi(pXmlLine->Attribute("Type"));
		int iRowNo = atoi(pXmlLine->Attribute("RowNo"));
		if (iType <= 2)
		{
			if (pArea->pXmlStart == NULL)
			{
				pArea->pXmlStart = pXmlLine;
				pArea->pXmlEnd = pXmlLine;
				pArea->iType = 0;
			}
			else
			{
				pArea->pXmlEnd = pXmlLine;
				if (pXmlLine->NextSiblingElement() == NULL)
				{
					m_AreaList.push_back(pArea);
				}
			}
		}
		else
		{
			// 保存上一个区域
			m_AreaList.push_back(pArea);

			// 保存当前区域
			pArea = new stArea();
			pArea->pXmlStart = pXmlLine;
			pArea->pXmlEnd = FindEnd(pXmlLine);
			pArea->iType = iType;
			pArea->strKeyWord += pXmlLine->Attribute("KeyWord");
			if (pXmlLine->Attribute("PriCol") != NULL)
			{
				pArea->strPriCol += pXmlLine->Attribute("PriCol");
			}
			else
			{
				pArea->strPriCol = "";
			}
			
			m_AreaList.push_back(pArea);

			// 开始下一个区域
			pXmlLine = pArea->pXmlEnd;
			pArea = new stArea();
			pArea->pXmlStart = NULL;
			pArea->pXmlEnd =NULL;			
		}
				 	
	}

	return true;
}

char * CTemplateLoader::isElseTag(char * czKeyWord)
{
	char * result = NULL;

	if (czKeyWord == NULL)
	{
		return NULL;
	}

	if (strlen(czKeyWord) <= 2)
	{
		return NULL;
	}

	if (czKeyWord[0] != '#')
	{
		return NULL;
	}
	else
	{
		result = &czKeyWord[1];

		int index = 1;
		for (;czKeyWord[index] != '\0';index++)
		{
			if (czKeyWord[index] == '#')
			{
				czKeyWord[index] = '\0';
				return result;
			}
		}
	}
	
	return NULL;
}

char * CTemplateLoader::hasPrimaryColum(char * czKeyWord)
{
	if (czKeyWord == NULL)
	{
		return NULL;
	}

	if (strlen(czKeyWord) < 3)
	{
		return NULL;
	}

	int index = 1;
	for (;czKeyWord[index] != '\0';index++)
	{
		if (czKeyWord[index] == '$')
		{
			czKeyWord[index] = '\0';
			return &czKeyWord[index+1];
		}
	}
	return NULL;
}

bool CTemplateLoader::AddJump()
{
	if (m_XmlDoc == NULL)
	{
		return false;
	}

	TiXmlElement* pXmlRoot = m_XmlDoc->RootElement();

	TiXmlElement* pXmlLine = pXmlRoot->FirstChildElement();
	for (;pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
	{
		int iType = atoi(pXmlLine->Attribute("Type"));

		if (iType == 2)
		{	
			const char * czTag = pXmlLine->Attribute("Tag");
			const char * czKeyWord = pXmlLine->Attribute("KeyWord");
			int iID = atoi(pXmlLine->Attribute("ID"));

			// 找到if
			if (strcmp(czTag, "") == 0)
			{
				// 设置if标签
				pXmlLine->SetAttribute("Tag", "if");

				// 寻找匹配的标签
				TiXmlElement * pXmlNext = FindEnd(pXmlLine);
				// 没有找到匹配的标签，说明模板文件格式有误，直接返回false
				if (pXmlNext == NULL)
				{
					return false;
				}
				else
				{
					int iNextID = atoi(pXmlNext->Attribute("ID"));

					// 找到else标签
					if (strcmp(pXmlNext->Attribute("Tag"), "else") == 0)
					{
						// 找到了else，继续寻找end
						TiXmlElement * pXmlEnd = FindEnd(pXmlNext);

						// 没有找到匹配的标签，说明模板文件格式有误，直接返回false
						if (pXmlEnd == NULL)
						{
							return false;
						}
						else
						{
							// 找到了end，获取end的索引ID
							int iEnd = atoi(pXmlEnd->Attribute("ID"));

							// 设置end标签
							pXmlEnd->SetAttribute("Tag", "end");

							// 设置if的jump
							pXmlLine->SetAttribute("YesJump", iID);		// 正确的话，指向自己，不做跳转
							pXmlLine->SetAttribute("NoJump", iNextID);	// 错误的话，指向end，跳转到else，中间的语句被跳过

							// 设置else的jump
							pXmlNext->SetAttribute("YesJump", iEnd);	// 指向end语句
							pXmlNext->SetAttribute("NoJump", iNextID);	// 指向自己

							// 设置end的jump
							pXmlEnd->SetAttribute("YesJump", iEnd);	// end语句，不具备跳转功能
							pXmlEnd->SetAttribute("NoJump", iEnd);
						}
					}
					else	// 没有else，直接是end 
					{
						// 设置end标签
						pXmlNext->SetAttribute("Tag", "end");

						// 设置if的jump
						pXmlLine->SetAttribute("YesJump", iID);		// 正确的话，指向自己，不做跳转
						pXmlLine->SetAttribute("NoJump", iNextID);	// 错误的话，指向end，跳转到end，中间的语句被跳过

						// 设置end的jump
						pXmlNext->SetAttribute("YesJump", iNextID);	// end语句，不具备跳转功能
						pXmlNext->SetAttribute("NoJump", iNextID);
						
					}
				}
			}
			
		}
	}

	return true;
}