﻿#pragma once
#include <vector>
using namespace std;

class CRuleLoader
{
public:
	CRuleLoader(void);
	~CRuleLoader(void);
	bool LoadRule(const char * fileName);
	bool SaveRule(const char * fileName);
	bool FixRule();//自动填充缺失的文件名
	bool GetRules(string strFileNum, int iType, string strKeyWord, string strPriCol, vector<TiXmlElement*>& RuleList);
	TiXmlElement* GetGlobalRule(string strFileNum);
public:
	TiXmlDocument * m_XmlDoc;
};
