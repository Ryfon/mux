﻿#pragma once
#include "TemplateLoader.h"
#include "RuleLoader.h"
#include <vector>
using namespace std;

class CFileExporter
{
public:
	CFileExporter(void);
	~CFileExporter(void);
	bool LoadTemplateFile(const char * fileName, bool type);
	bool LoadRuleFile(const char * fileName);
	bool ExportArea(stArea* pArea, string strFileName, ofstream& outFile);
	bool ExportAreaByRule(stArea* pArea, string strFileNum, TiXmlElement * pXmlRule, ofstream& outFile);
	bool ExportLuaFile(TiXmlElement * pXmlRule, BOOL bCover);
	bool ExportLuaFile(string strFileName);
	bool ExportALL(BOOL bCover);
	bool GetFileList(BOOL bCover);
	bool FixFileList();//使文件名列表规范化
	bool SetExportPath(string strPath);	// 设置输出目录
private:
	bool _FormatArea(TiXmlElement * pXmlRule);
	bool _isEmptyVariable(TiXmlElement * pXmlRle, const char * variableName);
	string _getFileName(const char * fileName, BOOL bCover);
	string _getFileNum(const char* fileName);//得到文件的前缀名
public:
	vector<string> m_fileList;
private:
	CTemplateLoader * m_Template;
	CRuleLoader * m_Rule;
	string m_strExportPath;	// 输出目录
};
