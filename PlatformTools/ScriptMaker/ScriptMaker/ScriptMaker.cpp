﻿// ScriptMaker.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "ScriptMaker.h"
#include "ScriptMakerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CScriptMakerApp

BEGIN_MESSAGE_MAP(CScriptMakerApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CScriptMakerApp 构造

CScriptMakerApp::CScriptMakerApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CScriptMakerApp 对象

CScriptMakerApp theApp;


// CScriptMakerApp 初始化

BOOL CScriptMakerApp::InitInstance()
{
	// 获取命令行参数
	CString strCmd(AfxGetApp()->m_lpCmdLine);
	
	// 是否以console模式运行
	if (ConsoleMode(strCmd))
	{
		return TRUE;
	}
	

	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	CScriptMakerDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此处放置处理何时用“确定”来关闭
		//  对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用“取消”来关闭
		//  对话框的代码
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

BOOL CScriptMakerApp::ConsoleMode(CString strCmd)
{
	USES_CONVERSION;

	// 判断有无命令参数
	int iCmdLength = strCmd.GetLength();
	if (iCmdLength == 0)
	{
		return FALSE;
	}

	// 参数分解
	CString strRulePath;
	CString strTemplatePath;
	CString strExportPath;

	// 解析第一个空格
	int index1 = strCmd.Find(L" ");
	if (index1 == -1)
	{
		return FALSE;
	}
	else
	{
		strRulePath = strCmd.Mid(0, index1);
	}

	// 创建FileExportet实例
	CFileExporter* pFileExporter = new CFileExporter();
	pFileExporter->LoadRuleFile(T2A(strRulePath));

	// 解析第二个空格
	int index2 = strCmd.Find(L" ", index1 + 1);
	if (index2 == -1)
	{
		strTemplatePath = strCmd.Mid(index1 + 1, iCmdLength - index1 - 1);
		GetCurrentDirectory(MAX_PATH, strExportPath.GetBuffer(MAX_PATH));
	} 
	else
	{
		strTemplatePath = strCmd.Mid(index1 + 1, index2 - index1 - 1);
		strExportPath = strCmd.Mid(index2 + 1, iCmdLength - index2 - 1);
	}

	// 输出文件
	pFileExporter->LoadTemplateFile(T2A(strTemplatePath), false);
	
		string temp(T2A(strExportPath));
		temp += "\\";
		pFileExporter->SetExportPath(temp);
	
	pFileExporter->GetFileList(TRUE);
	pFileExporter->ExportALL(TRUE);

	// 销毁FileExportet实例
	delete pFileExporter;

	return TRUE;
}