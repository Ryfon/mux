﻿// ScriptMakerDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ScriptMaker.h"
#include "ScriptMakerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CScriptMakerDlg 对话框




CScriptMakerDlg::CScriptMakerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CScriptMakerDlg::IDD, pParent)
	/*, m_TemplateFilePath(_T(""))
	, m_RuleFilePath(_T(""))*/
	, m_strAppDir(_T(""))
	, m_bCover(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	// 获得可执行程序所在的路径
	GetCurrentDirectory(MAX_PATH, m_strAppDir.GetBuffer(MAX_PATH));
}

void CScriptMakerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LUAPATH, m_TemplateFilePath);
	DDX_Control(pDX, IDC_XMLPATH, m_RuleFilePath);
	DDX_Control(pDX, IDC_OUTFILELIST, m_OutFileListCtrl);
	DDX_Check(pDX, IDC_CHECK1, m_bCover);
}

BEGIN_MESSAGE_MAP(CScriptMakerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON2, &CScriptMakerDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, &CScriptMakerDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_CHECK1, &CScriptMakerDlg::OnBnClickedCheck1)
	ON_BN_CLICKED(IDOK, &CScriptMakerDlg::OnBnClickedOk)
	ON_EN_CHANGE(IDC_XMLPATH, &CScriptMakerDlg::OnEnChangeXmlpath)
END_MESSAGE_MAP()


// CScriptMakerDlg 消息处理程序

BOOL CScriptMakerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	m_Exporter = new CFileExporter();
	m_bCover = TRUE;
	UpdateData(false);

	m_OutFileListCtrl.InsertColumn(0, L"文件名");
	m_OutFileListCtrl.SetColumnWidth(0, 256);
	
	// 设置默认的文件后缀名
	m_TemplateFilePath.SetSuffixName(L".lua");
	m_RuleFilePath.SetSuffixName(L".xml");

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CScriptMakerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CScriptMakerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CScriptMakerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CScriptMakerDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog TemplateFileDlg(true, L"", L"", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, L"TemplateScriptFile (*.lua)|*.lua||");
	TemplateFileDlg.m_ofn.lpstrInitialDir = m_strAppDir;
	if (TemplateFileDlg.DoModal() == IDOK)
	{
		this->m_TemplateFilePath.SetWindowText(TemplateFileDlg.GetPathName());
		UpdateData(false);
	}
}

void CScriptMakerDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog RuleFileDlg(true, L"", L"", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, L"RuleFile (*.xml)|*.xml||");
	RuleFileDlg.m_ofn.lpstrInitialDir = m_strAppDir;
	if (RuleFileDlg.DoModal() == IDOK)
	{
		this->m_RuleFilePath.SetWindowText(RuleFileDlg.GetPathName());
		UpdateData(false);
	}
}

void CScriptMakerDlg::OnBnClickedCheck1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(true);

	if (this->m_Exporter->GetFileList(this->m_bCover))
	{
		this->UpdateFileList(this->m_Exporter->m_fileList);
	}
	else
	{
		this->m_Exporter->m_fileList.clear();
	}

}

void CScriptMakerDlg::UpdateFileList(vector<string> fileList)
{
	m_OutFileListCtrl.DeleteAllItems();

	int iSize = static_cast<int> (fileList.size());
	for (int i = 0;i < iSize;i++)
	{
		m_OutFileListCtrl.InsertItem(i, L"");
		CString csFilename(fileList[i].c_str());
		m_OutFileListCtrl.SetItemText(i, 0, csFilename);
	}

}
void CScriptMakerDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码

	USES_CONVERSION;
	CString strTemp;

	// 加载模板文件
	m_TemplateFilePath.GetWindowText(strTemp);
	m_Exporter->LoadTemplateFile(T2A(strTemp), false);

	// 开始导出
	if (m_Exporter->ExportALL(this->m_bCover))
	{
		MessageBox(L"生成文件成功！");

		// 及时更新文件列表的状态
		UpdateData(true);

		if (this->m_Exporter->GetFileList(this->m_bCover))
		{
			this->UpdateFileList(this->m_Exporter->m_fileList);
		}
	}
	
}

void CScriptMakerDlg::OnEnChangeXmlpath()
{
	// TODO:  如果该控件是 RICHEDIT 控件，则它将不会
	// 发送该通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码

	// 加载xml配置文件
	USES_CONVERSION;
	CString strTemp;
	m_RuleFilePath.GetWindowText(strTemp);
	if(m_Exporter->LoadRuleFile(T2A(strTemp)))
	{
		this->m_Exporter->GetFileList(this->m_bCover);
	}
	else
	{
		this->m_Exporter->m_fileList.clear();
	}

	this->UpdateFileList(this->m_Exporter->m_fileList);
}
