﻿// lxEdit.cpp : 实现文件
//

#include "stdafx.h"
#include "lxEdit.h"


// ClxEdit

IMPLEMENT_DYNAMIC(ClxEdit, CEdit)

ClxEdit::ClxEdit()
{

}

ClxEdit::~ClxEdit()
{
}


BEGIN_MESSAGE_MAP(ClxEdit, CEdit)
	ON_WM_DROPFILES()
END_MESSAGE_MAP()



// ClxEdit 消息处理程序

void ClxEdit::OnDropFiles(HDROP hDropInfo)
{
	// 被拖拽的文件的文件名
	WCHAR szFileName[MAX_PATH];

	// 得到被拖拽的文件名
	DragQueryFile(hDropInfo, 0, szFileName, MAX_PATH);

	// 转化为CString,方便进行后缀名的检测
	CString strFileName(szFileName);

	// 后缀名检测
	int result = strFileName.Find(m_strSuffixName);
	if (result == -1)
	{
		CString strError = L"只接受";
		strError += m_strSuffixName;
		strError += L"格式的文件";
		MessageBox(strError);
		return;
	}

	// 把文件名显示出来
	this->SetWindowText(strFileName);

	CEdit::OnDropFiles(hDropInfo);
}

// 获取默认的后缀名
CString ClxEdit::GetSuffixName()
{
	return m_strSuffixName;
}

// 设置默认的后缀名
void ClxEdit::SetSuffixName(CString strSuffixName)
{
	m_strSuffixName = strSuffixName;
}