﻿#pragma once


// ClxEdit

class ClxEdit : public CEdit
{
	DECLARE_DYNAMIC(ClxEdit)

public:
	ClxEdit();
	virtual ~ClxEdit();

	afx_msg void OnDropFiles(HDROP hDropInfo);

protected:
	DECLARE_MESSAGE_MAP()

private:
	CString m_strSuffixName;	// 默认的文件后缀名

public:
	CString GetSuffixName();
	void SetSuffixName(CString strSuffixName);
};


