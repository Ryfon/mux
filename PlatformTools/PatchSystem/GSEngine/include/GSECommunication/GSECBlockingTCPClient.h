﻿/**
* @file	    GSECBlockingTCPClient.h 
* @brief    Blocking TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECBlockingTCPClient.h
* 摘    要: Blocking TCP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.10
* 修改记录: 
*/

#ifndef __GSECBlockingTCPClient__H__
#define __GSECBlockingTCPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECBlockingTCPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECBlockingTCPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECBlockingTCPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};
#endif
