﻿/**
* @file	    GSECommunicationApplication.h 
* @brief    Application Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationApplication.h
* 摘    要: 应用程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.16
* 修改记录: 
*/

#ifndef __GSECommunicationApplication__H__
#define __GSECommunicationApplication__H__
#include "GSECommunication.h"

#define	DEFAULTLISTENBACKLOG 60

class GSECommunicationApplicationCallback;
class GSESYSTEMENTRY_CLASS GSECommunicationApplication : public GSEMemoryObject
{
friend class GSECommunicationFactory;
protected:
    GSECommunicationApplication(GSECommunicationApplicationCallback * pkApplicationCallback) throw(GSEException);
    virtual ~GSECommunicationApplication(void) throw(GSEException);

protected:
    virtual void Create() throw(GSEException) = 0;
    virtual void DoProcess() throw(GSEException) = 0;

protected:
    void  DoCreateSocket(void) throw(GSEException);
    bool  DoBind(const char* pszIP, int nPort);
    void  DoSetSocket(GSECommunication* pkCommunication, Socket hSocket);

protected:
    GSECommunication*                    m_pkGSECommunication;
    GSECommunicationApplicationCallback* m_pkApplicationCallback;
};

#endif
