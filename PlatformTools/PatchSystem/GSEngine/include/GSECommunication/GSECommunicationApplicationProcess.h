﻿/**
* @file	    GSECommunicationApplicationProcess.h 
* @brief    Application Process Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationApplicationProcess.h
* 摘    要: 应用处理程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.18
* 修改记录: 
*/

#ifndef __GSECommunicationApplicationProcess__H__
#define __GSECommunicationApplicationProcess__H__
#include "GSECommunication.h"

class GSECommunicationApplication;
class GSECommunicationApplicationCallback;
class GSESYSTEMENTRY_CLASS GSECommunicationApplicationProcess : public GSEMemoryObject
{
friend class GSECommunicationApplicationClient;
public:
    GSECommunicationApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECommunicationApplicationProcess(void) throw(GSEException);

protected:
    virtual void Process(void) throw(GSEException) = 0;

protected:
    GSECommunication*                    m_pkCommunication;
    GSECommunicationApplication*         m_pkApplication;
    GSECommunicationApplicationCallback* m_pkApplicationCallback;
};

#endif
