﻿/**
* @file	    GSECOverlappedIOCPUDPClient.h 
* @brief    Overlapped IOCP UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedIOCPUDPClient.h
* 摘    要: Overlapped IOCP UDP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.01
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedIOCPUDPClient__H__
#define __GSECOverlappedIOCPUDPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedIOCPUDPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedIOCPUDPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedIOCPUDPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
