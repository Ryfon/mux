﻿/**
* @file	    GSECOverlappedRoutineTCPServer.h 
* @brief    Overlapped Routine TCP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedRoutineTCPServer.h
* 摘    要: Overlapped Routine TCP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.25
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedRoutineTCPServer__H__
#define __GSECOverlappedRoutineTCPServer__H__
#include "GSECommunicationApplicationServer.h"
#include "GSEPool.h"

class GSECOverlappedEx;
class GSECOverlappedAcceptEx;
class GSESYSTEMENTRY_CLASS GSECOverlappedRoutineTCPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedRoutineTCPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedRoutineTCPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException);
    virtual void DeAccept(GSECommunication* pkCommunication, LPVOID pkParam = NULL) throw(GSEException);
    virtual void DoProcess() throw(GSEException);

protected:
    GSECOverlappedEx*                              m_pkOverlappedEx;
    GSEPool<GSECommunication*, GSECOverlappedEx*>* m_pkOverlappedExPool;
    GSECOverlappedAcceptEx*                        m_pkAcceptEx;
};

#endif
#endif
