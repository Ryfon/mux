﻿/**
* @file	    GSECOverlappedRoutineCommunicationTCP.h 
* @brief    Overlapper Routine TCP Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedRoutineCommunicationTCP.h
* 摘    要: Overlapped Routine TCP 通讯接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.24
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedRoutineCommunicationTCP__H__
#define __GSECOverlappedRoutineCommunicationTCP__H__
#include "GSECommunicationTCP.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedRoutineCommunicationTCP : public GSECommunicationTCP
{
public:
    GSECOverlappedRoutineCommunicationTCP(void){}
    virtual ~GSECOverlappedRoutineCommunicationTCP(void){}

public:
    virtual int  Send(char* pszData, INT nLen, LPVOID pkExtraData = NULL) throw();
    virtual int  Recv(char* pszBuf,  INT nLen, LPVOID pkExtraData = NULL) throw();
};

#endif
#endif
