﻿/**
* @file	    GSECommunicationApplicationProcess.h 
* @brief    Blocking Application Process Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationApplicationProcess.h
* 摘    要: Blocking 应用处理程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.17
* 修改记录: 
*/

#ifndef __GSECBlockingApplicationProcess__H__
#define __GSECBlockingApplicationProcess__H__
#include "GSECommunicationApplicationProcess.h"

class GSESYSTEMENTRY_CLASS GSECBlockingApplicationProcess : public GSECommunicationApplicationProcess
{
public:
    GSECBlockingApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECBlockingApplicationProcess(void) throw(GSEException){}

protected:
    virtual void Process(void) throw(GSEException);
};
#endif
