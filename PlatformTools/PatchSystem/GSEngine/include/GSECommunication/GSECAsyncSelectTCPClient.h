﻿/**
* @file	    GSECAsyncSelectTCPClient.h 
* @brief    Asysnc Select TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECAsyncSelectTCPClient.h
* 摘    要: Asysnc Select TCP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.16
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECAsyncSelectTCPClient__H__
#define __GSECAsyncSelectTCPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECAsyncSelectTCPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECAsyncSelectTCPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECAsyncSelectTCPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
