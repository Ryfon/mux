﻿/**
* @file	    GSECOverlappedRoutineTCPClient.h 
* @brief    Overlapped Routine TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedRoutineTCPClient.h
* 摘    要: Overlapped Routine TCP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.25
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedRoutineTCPClient__H__
#define __GSECOverlappedRoutineTCPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedRoutineTCPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedRoutineTCPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedRoutineTCPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
