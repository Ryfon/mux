﻿/**
* @file	    GSECBlockingUDPClient.h 
* @brief    Blocking UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECBlockingUDPClient.h
* 摘    要: Blocking UDP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.11
* 修改记录: 
*/

#ifndef __GSECBlockingUDPClient__H__
#define __GSECBlockingUDPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECBlockingUDPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECBlockingUDPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECBlockingUDPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};
#endif
