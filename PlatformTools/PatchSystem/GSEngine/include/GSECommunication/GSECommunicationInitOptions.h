﻿/**
* @file	    GSECommunicationInitOptions.h 
* @brief    Communication Init Options Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationInitOptions.h
* 摘    要: 通讯初始化接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.20
* 修改记录: 
*/

#ifndef __GSECommunicationInitOptions__H__
#define __GSECommunicationInitOptions__H__
#include "GSECommunication.h"
#include "GSECommunicationFactory.h"

class GSESYSTEMENTRY_CLASS GSECommunicationInitOptions
{
public:
    GSECommunicationInitOptions(void) throw(GSEException);
    ~GSECommunicationInitOptions(void) throw(GSEException);
};

#endif
