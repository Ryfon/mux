﻿/**
* @file	    GSECommunicationApplicationClient.h 
* @brief    Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationApplicationClient.h
* 摘    要: Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.5.25
* 修改记录: 
*/

#ifndef __GSECommunicationApplicationClient__H__
#define __GSECommunicationApplicationClient__H__
#include "GSECommunicationApplication.h"

class GSECommunicationApplicationProcess;
class GSESYSTEMENTRY_CLASS GSECommunicationApplicationClient : public GSECommunicationApplication
{
friend class GSECommunicationFactory;
protected:
    GSECommunicationApplicationClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECommunicationApplicationClient(void) throw(GSEException);

public:
    virtual void Connect(const char* pszIP, short nPort) throw(GSEException);
    virtual void Disconnect() throw(GSEException);

protected:
    virtual void DoProcess() throw(GSEException);

protected:
    bool DoConnect(const char* pszIP, short nPort);

protected:
    GSECommunicationApplicationProcess*  m_pkApplicationProcess;
};
#endif
