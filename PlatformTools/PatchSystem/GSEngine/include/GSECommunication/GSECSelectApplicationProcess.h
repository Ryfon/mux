﻿/**
* @file	    GSECSelectApplicationProcess.h 
* @brief    Select Application Process Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECSelectApplicationProcess.h
* 摘    要: Select 应用处理程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.16
* 修改记录: 
*/

#ifndef __GSECSelectApplicationProcess__H__
#define __GSECSelectApplicationProcess__H__
#include "GSECommunicationApplicationProcess.h"

class GSESYSTEMENTRY_CLASS GSECSelectApplicationProcess : public GSECommunicationApplicationProcess
{
public:
    GSECSelectApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECSelectApplicationProcess(void) throw(GSEException){}

protected:
    virtual void Process(void) throw(GSEException);
};
#endif
