﻿/**
* @file	    GSECAsyncSelectUDPClient.h 
* @brief    Asysnc Select UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECAsyncSelectUDPClient.h
* 摘    要: Asysnc Select UDP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.16
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECAsyncSelectUDPClient__H__
#define __GSECAsyncSelectUDPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECAsyncSelectUDPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECAsyncSelectUDPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECAsyncSelectUDPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
