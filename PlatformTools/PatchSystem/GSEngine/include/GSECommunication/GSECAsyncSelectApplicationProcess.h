﻿/**
* @file	    GSECAsyncSelectApplicationProcess.h 
* @brief    Asysnc Select Application Process Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECAsyncSelectApplicationProcess.h
* 摘    要: Asysnc Select 应用处理程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.16
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECAsyncSelectApplicationProcess__H__
#define __GSECAsyncSelectApplicationProcess__H__
#include "GSECommunicationApplicationProcess.h"

class GSESYSTEMENTRY_CLASS GSECAsyncSelectApplicationProcess : public GSECommunicationApplicationProcess
{
public:
    GSECAsyncSelectApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECAsyncSelectApplicationProcess(void) throw(GSEException){}

protected:
    virtual void Process(void) throw(GSEException);

protected:
    HWND  m_hWnd;
};

#endif
#endif
