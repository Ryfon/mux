﻿/**
* @file	    GSECommunicationFactory.h 
* @brief    Communication Factory Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationFactory.h
* 摘    要: 通讯工厂接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.19
* 修改记录: 
*/

#ifndef __GSECommunicationFactory__H__
#define __GSECommunicationFactory__H__
#include "GSECommunication.h"
#include "GSESystem.h"
#include "GSEMemoryObject.h"
#include "GSECommunicationApplication.h"
#include "GSECommunicationApplicationClient.h"
#include "GSECommunicationApplicationServer.h"
#include "GSECBlockingApplicationProcess.h"
#include "GSECBlockingTCPClient.h"
#include "GSECBlockingTCPServer.h"
#include "GSECSelectApplicationProcess.h"
#include "GSECSelectTCPClient.h"
#include "GSECSelectTCPServer.h"
#include "GSECAsyncSelectApplicationProcess.h"
#include "GSECAsyncSelectTCPClient.h"
#include "GSECAsyncSelectTCPServer.h"
#include "GSECEventSelectApplicationProcess.h"
#include "GSECEventSelectTCPClient.h"
#include "GSECEventSelectTCPServer.h"
#include "GSECOverlappedEventApplicationProcess.h"
#include "GSECOverlappedEventTCPClient.h"
#include "GSECOverlappedEventTCPServer.h"
#include "GSECOverlappedRoutineApplicationProcess.h"
#include "GSECOverlappedRoutineTCPClient.h"
#include "GSECOverlappedRoutineTCPServer.h"
#include "GSECOverlappedIOCPApplicationProcess.h"
#include "GSECOverlappedIOCPTCPClient.h"
#include "GSECOverlappedIOCPTCPServer.h"
#include "GSECEPOLLApplicationProcess.h"
#include "GSECEPOLLTCPClient.h"
#include "GSECEPOLLTCPServer.h"

typedef enum
{
    GSECOMMUNICATION_BLOCKING           =  0,   //阻塞模式
    GSECOMMUNICATION_SELECT,                    //Select模式
    GSECOMMUNICATION_ASYNCSELECT,               //AsyncSelect模式(Windows only)
    GSECOMMUNICATION_EVENTSELECT,               //EventSelect模式(Windows only)
    GSECOMMUNICATION_OVERLAPPEDEVENT,           //OverlappedEvent模式(Windows only)
    GSECOMMUNICATION_OVERLAPPEDROUTINE,         //OverlappedRoutine模式(Windows only)
    GSECOMMUNICATION_OVERLAPPEDIOCP,            //OverlappedIOCP模式(Windows only)
    GSECOMMUNICATION_EPOLL,                     //EPOLL模式(CentOs only)
}GSECommunicationApplicationType;

class GSECommunicationApplicationCallback;
class GSESYSTEMENTRY_CLASS GSECommunicationFactory : public GSEMemoryObject
{
template<typename> friend class GSESingleton;
public:    
    virtual ~GSECommunicationFactory(void) {}

protected:
    static GSECommunicationFactory* Instance() throw(GSEException);

public:
    virtual GSECommunicationApplicationClient* CreateApplicationClient(GSECommunicationApplicationCallback* pkApplicationCallback, GSECommunicationApplicationType eApplicationType) throw(GSEException);
    virtual GSECommunicationApplicationServer* CreateApplicationServer(GSECommunicationApplicationCallback* pkApplicationCallback, GSECommunicationApplicationType eApplicationType) throw(GSEException);
    virtual void DestroyApplication(GSECommunicationApplication* pkApplication) throw(GSEException);

protected:
    GSECommunicationFactory(void) {}
    GSECommunicationFactory(const GSECommunicationFactory&) {}
    GSECommunicationFactory& operator=(const GSECommunicationFactory&) { return *this; }

protected:
    static GSECommunicationFactory* ms_pkGSECommunicationFactory;
};

#endif
