﻿/**
* @file	    GSECEPOLLTCPServer.h 
* @brief    EPOLL TCP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEPOLLTCPServer.h
* 摘    要: EPOLL TCP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.02
* 修改记录: 
*/

#ifndef WIN32
#ifndef __GSECEPOLLTCPServer__H__
#define __GSECEPOLLTCPServer__H__
#include "GSECommunicationApplicationServer.h"
#include "GSEPool.h"
#include "GSELock.h"

#define EPOLLVOLUME     10000
#define MAXSOCKVOLUME   50000
#define THREADCOUNT     MAXSOCKVOLUME / EPOLLVOLUME

class GSESYSTEMENTRY_CLASS GSECEPOLLTCPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECEPOLLTCPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEPOLLTCPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException);
    virtual void DeAccept(GSECommunication* pkPeer, LPVOID pkParam = NULL) throw(GSEException);
    virtual void DoProcess() throw(GSEException);

protected:
    static void* WorkerThread(LPVOID pkParam) throw(GSEException);

protected:
    int                                 m_nEpollDescriptor;
    GSELock                             m_kLock;
    GSEPool<Socket, GSECommunication*>* m_pkTempCommunicationPool;
    pthread_t                           m_hThreadHandles[THREADCOUNT];
};

#endif
#endif
