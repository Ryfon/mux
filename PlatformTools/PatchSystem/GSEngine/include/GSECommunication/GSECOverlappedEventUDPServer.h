﻿/**
* @file	    GSECOverlappedEventUDPServer.h 
* @brief    Overlapped Event UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedEventUDPServer.h
* 摘    要: Overlapped Event UDP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.24
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedEventUDPServer__H__
#define __GSECOverlappedEventUDPServer__H__
#include "GSECommunicationApplicationServer.h"

class GSECommunicationApplicationProcess;
class GSESYSTEMENTRY_CLASS GSECOverlappedEventUDPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedEventUDPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedEventUDPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);

protected:
    GSECommunicationApplicationProcess* m_pkApplicationProcess;
};

#endif
#endif
