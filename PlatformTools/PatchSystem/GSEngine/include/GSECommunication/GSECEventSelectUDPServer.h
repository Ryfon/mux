﻿/**
* @file	    GSECEventSelectUDPServer.h 
* @brief    Event Select UDP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEventSelectUDPServer.h
* 摘    要: Event Select UDP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.17
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECEventSelectUDPServer__H__
#define __GSECEventSelectUDPServer__H__
#include "GSECommunicationApplicationServer.h"

class GSECommunicationApplicationProcess;
class GSESYSTEMENTRY_CLASS GSECEventSelectUDPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECEventSelectUDPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEventSelectUDPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);

protected:
    GSECommunicationApplicationProcess* m_pkApplicationProcess;
};

#endif
#endif
