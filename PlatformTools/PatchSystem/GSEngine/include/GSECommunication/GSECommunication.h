﻿/**
* @file	    GSECommunication.h 
* @brief    Communication Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunication.h
* 摘    要: 通讯接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.3.18
* 修改记录: 
*/

#ifndef  __GSEComm__H__
#define  __GSEComm__H__
#ifdef	 WIN32
#define  FD_SETSIZE                                         1024
#include <winsock2.h>
typedef  int                                                SOCKLEN_T;
typedef	 SOCKET                                             Socket;
#define  ASYSNCSELECT_MSG                                   WM_USER + 200
#else
#include <sys/socket.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
typedef  socklen_t                                          SOCKLEN_T;
typedef  size_t                                             INT;
typedef	 int                                                Socket;
typedef  struct sockaddr                                    SOCKADDR;
typedef  struct sockaddr_in                                 SOCKADDR_IN;
#define	 INVALID_SOCKET	                                    -1
#define	 SOCKET_ERROR                                       -1
#define  SD_RECEIVE                                         0x00
#define  SD_SEND                                            0x01
#define  SD_BOTH                                            0x02
#endif

#include "GSESystem.h"
#include "GSEMemoryObject.h"
#include "GSEException.h"
#include "GSEFlags.h"

#define MAXSOCKS                                            FD_SETSIZE

#define GSEINIT_COMMUNICATION()                             GSECommunicationInitOptions g_GSECommunicationOptions;

#define GSECCreateApplicationClient(pkCallback, appType)    GSESingleton<GSECommunicationFactory>::Instance()->CreateApplicationClient(pkCallback, appType);
#define GSECCreateApplicationServer(pkCallback, appType)    GSESingleton<GSECommunicationFactory>::Instance()->CreateApplicationServer(pkCallback, appType);
#define GSECDestroyApplication(pkApp)                       GSESingleton<GSECommunicationFactory>::Instance()->DestroyApplication(pkApp);

GSESYSTEMENTRY_FUNCTION int                                 GetSockErrorCode() throw();
GSESYSTEMENTRY_FUNCTION bool                                WouldBlocked(int nSockErrorCode);
GSESYSTEMENTRY_FUNCTION void                                SetNonBlocking(Socket hSocket) throw(GSEException);

class GSESYSTEMENTRY_CLASS GSECommunication : public GSEMemoryObject
{
friend class GSECommunicationApplication;
friend class GSECommunicationApplicationClient;
friend class GSECommunicationApplicationServer;
friend class GSECommunicationApplicationProcess;
public:
    GSECommunication(void);
    virtual ~GSECommunication(void);

public:
    static void InitSockSubsys(int nHighVer = 2, int nLowVer = 2) throw(GSEException);
    static void FnitSockSubsys() throw(GSEException);

public:
    Socket GetSocket()
    {
        return m_hSocket;
    }

public:
    virtual int    Send(char* pszData, INT nLen, LPVOID pkExtraData = NULL) throw() = 0;
    virtual int    Recv(char* pszBuf,  INT nLen, LPVOID pkExtraData = NULL) throw() = 0;
    virtual bool   Shutdown(int nMode = SD_SEND);
    virtual bool   CloseSocket(void);

protected:
    virtual void   CreateSocket(void) throw(GSEException) = 0;
    virtual bool   Connect(const char* pszIP, short nPort);
    virtual bool   Bind(const char* pszIP, short nPort);
    virtual bool   Listen(int nBackLog);
    virtual Socket Accept(SOCKADDR_IN& kSockAddr, SOCKLEN_T& nSockAddrLen);

protected:
    void SetLinger(int nSeconds) throw(GSEException);
    void SetAddress(SOCKADDR_IN& kSockAddr, const char* pszIP, int nPort);
    void SetSocket(Socket hSocket);
 
protected:
    Socket    m_hSocket;
};
#endif
