﻿/**
* @file	    GSECommunicationTCP.h 
* @brief    GSECommunicationTCP Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationTCP.h
* 摘    要: TCP通讯接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.5.25
* 修改记录: 
*/

#ifndef __GSECommunicationTCP__H__
#define __GSECommunicationTCP__H__
#include "GSECommunication.h"

class GSESYSTEMENTRY_CLASS GSECommunicationTCP : public GSECommunication
{
public:
    GSECommunicationTCP(void){}
    virtual ~GSECommunicationTCP(void){}

public:
    virtual int  Send(char* pszData, INT nLen, LPVOID pkExtraData = NULL) throw();
    virtual int  Recv(char* pszBuf,  INT nLen, LPVOID pkExtraData = NULL) throw();

protected:
    virtual void CreateSocket(void) throw(GSEException);
};
#endif
