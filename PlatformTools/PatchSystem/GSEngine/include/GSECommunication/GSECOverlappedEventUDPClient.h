﻿/**
* @file	    GSECOverlappedEventUDPClient.h 
* @brief    Overlapped Event UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedEventUDPClient.h
* 摘    要: Overlapped Event UDP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.24
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedEventUDPClient__H__
#define __GSECOverlappedEventUDPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedEventUDPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedEventUDPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedEventUDPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
