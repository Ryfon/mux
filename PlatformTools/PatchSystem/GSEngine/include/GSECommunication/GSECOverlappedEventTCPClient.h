﻿/**
* @file	    GSECOverlappedEventTCPClient.h 
* @brief    Overlapped Event TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedEventTCPClient.h
* 摘    要: Overlapped Event TCP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.19
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedEventTCPClient__H__
#define __GSECOverlappedEventTCPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedEventTCPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedEventTCPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedEventTCPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
