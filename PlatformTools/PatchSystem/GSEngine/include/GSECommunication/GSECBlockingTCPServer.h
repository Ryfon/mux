﻿/**
* @file	    GSECBlockingTCPServer.h 
* @brief    Blocking TCP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECBlockingTCPServer.h
* 摘    要: Blocking TCP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.10
* 修改记录: 
*/

#ifndef __GSECBlockingTCPServer__H__
#define __GSECBlockingTCPServer__H__
#include "GSECommunicationApplicationServer.h"
#include "GSEPool.h"
#include "GSEThread.h"
#include "GSELock.h"

class GSESYSTEMENTRY_CLASS GSECBlockingTCPServer : public GSECommunicationApplicationServer
{
friend class GSECBlockingTCPServerThreadProc;
friend class GSECommunicationFactory;
protected:
    GSECBlockingTCPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECBlockingTCPServer(void) throw(GSEException);

protected:
    virtual void Create() throw(GSEException);
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException);
    virtual void DeAccept(GSECommunication* pkCommunication, LPVOID pkParam = NULL) throw(GSEException);
    virtual void DoProcess() throw(GSEException);

protected:
    GSELock                                        m_kLock;
    GSEPool<GSECommunication*, GSEThread*>*        m_pkCommunicationThreadPool;
    GSEPool<GSECommunication*, GSECommunication*>* m_pkDeprecatedCommunicationPool;
};
#endif
