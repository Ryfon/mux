﻿/**
* @file	    GSECommunicationApplicationCallback.h 
* @brief    Communicatiion Call back Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationApplicationCallback.h
* 摘    要: 通讯回调接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.5.26
* 修改记录: 
*/

#ifndef __GSECommunicationApplicationCallback__H__
#define __GSECommunicationApplicationCallback__H__
#include "GSECommunication.h"
#include "GSEMemoryObject.h"

class GSECommunicationApplication;
class GSECommunicationApplicationServer;
class GSESYSTEMENTRY_CLASS GSECommunicationApplicationCallback : public GSEMemoryObject
{
public:
    GSECommunicationApplicationCallback(void){}
    virtual ~GSECommunicationApplicationCallback(void){}

public:
    virtual bool Continue() = 0;
    virtual void Terminate() = 0;
    virtual void OnCreated(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication) {}
	virtual void OnDestroy(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication) {}
    virtual void OnRecv(GSECommunication* pkCommunication, char* pszData, int nLen) {}
    virtual void OnSend(GSECommunication* pkCommunication, char* pszData, int nLen) {}
    virtual void OnTimedout(GSECommunicationApplication* pkApplication) {}
    virtual void OnError(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication, int nErrCode){}
    virtual void OnClose(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication) {}

    virtual void GetWaitTime(struct timeval& kTimeValue);
    virtual int  GetWaitTime(){ return 0; }
    virtual void GetRecvBuf(GSECommunication* pkCommunication, char*& pszBuf, int& nLen);
    virtual void GetSendBuf(GSECommunication* pkCommunication, char*& pszBuf, int& nLen); 
	
public:
    virtual int  GetPostAcceptCounts() { return 1; }
    virtual bool GetRecvMask(GSECommunication* pkCommunication) { return true; }
    virtual bool GetSendMask(GSECommunication* pkCommunication) { return true; }
    virtual void OnCreatedPeer(GSECommunicationApplicationServer* pkServer, GSECommunication* pkCommunication) throw(GSEException){}
    virtual void OnDestroyPeer(GSECommunicationApplicationServer* pkServer, GSECommunication* pkCommunication) throw(GSEException) {}
    virtual bool IsErasedPeer(GSECommunicationApplicationServer* pkServer, GSECommunication* pkGSECommunication){ return false; }
  
public:
#ifdef WIN32
    virtual HWND CreateHWND(WNDPROC pfnProc) throw(GSEException){return NULL;}
#endif
};
#endif
