﻿/**
* @file	    GSECSelectTCPClient.h 
* @brief    Select TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECSelectTCPClient.h
* 摘    要: Select TCP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.12
* 修改记录: 
*/

#ifndef __GSECSelectTCPClient__H__
#define __GSECSelectTCPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECSelectTCPClient : public GSECommunicationApplicationClient
{
public:
    GSECSelectTCPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    ~GSECSelectTCPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};
#endif
