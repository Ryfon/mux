﻿/**
* @file	    GSECOverlappedEx.h 
* @brief    Overlapped Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedEx.h
* 摘    要: 重叠接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.19
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedEx__H__
#define __GSECOverlappedEx__H__
#include "GSECommunication.h"

typedef enum
{
    GSECOMMU_OPER_NOOPER,
    GSECOMMU_OPER_CONNET,
    GSECOMMU_OPER_ACCEPT,
    GSECOMMU_OPER_READ,
    GSECOMMU_OPER_WRITE
}GSECommOperation;

class GSECommunicationApplication;
class GSECommunicationApplicationCallback;
class GSESYSTEMENTRY_CLASS GSECOverlappedEx : public GSEMemoryObject
{
public:
    GSECOverlappedEx(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback, bool bCreateEvent = true) throw(GSEException);
    virtual ~GSECOverlappedEx(void) throw(GSEException);

public:
    WSAOVERLAPPED*    GetOverlapped();
    WSAOVERLAPPED*    operator()(void);
    WSAEVENT          GetEvent();
    GSECommunication* GetCommunication();
    GSECommOperation  GetCurrOperation();
    Socket            GetSocket();
    WSABUF*           GetBuf();
    void              RegisterAccept(Socket hSocket, char* pszBuf, int nLen) throw(GSEException);
    void              RegisterRead(char* pszBuf,  int nLen) throw(GSEException);
    void              RegisterWrite(char* pszData, int nLen) throw(GSEException);

public:
    static void CALLBACK CompletionRoutine(DWORD dwError, DWORD dwTransferred, LPWSAOVERLAPPED pkOverlapped, DWORD dwFlags);

protected:
    void ResetEvent();	

protected:
    WSAOVERLAPPED                        m_kOverlapped;
    GSECommunication*                    m_pkCommunication;
    GSECommunicationApplication*         m_pkApplication;
    GSECommunicationApplicationCallback* m_pkApplicationCallback;
    GSECommOperation                     m_eCommOperation;
    Socket                               m_hSocket;
    WSABUF                               m_kBuf;
};

#endif
#endif
