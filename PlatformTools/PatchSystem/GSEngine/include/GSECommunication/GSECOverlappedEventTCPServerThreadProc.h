﻿/**
* @file	    GSECOverlappedEventTCPServerThreadProc.h 
* @brief    Overlapped Event TCP Server Thread Proc Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedEventTCPServerThreadProc.h
* 摘    要: Overlapped Event TCP服务器线程接口程序声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.30
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedEventTCPServerThreadProc__H__
#define __GSECOverlappedEventTCPServerThreadProc__H__
#include "GSECOverlappedEx.h"
#include "GSECommunicationThreadProc.h"
#include "GSEPool.h"
#include "GSELock.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedEventTCPServerThreadProc : public GSECommunicationThreadProc
{
public:
    GSECOverlappedEventTCPServerThreadProc(GSECommunicationApplicationServer* pkApplicationServer, GSECommunicationApplicationCallback* pkApplicationCallback, GSEPool<WSAEVENT, GSECOverlappedEx*>* pkOverlappedExPool) throw(GSEException);
    virtual ~GSECOverlappedEventTCPServerThreadProc(void) throw(GSEException);

protected:
    virtual void ThreadProc(void) throw(GSEException);
    virtual void Terminate() throw(GSEException);
    virtual void Clear() throw(GSEException);

protected:
    bool Erase(WSAEVENT hEvent, GSECOverlappedEx* pkOverlappedEx, bool bEraseCurrent = false);

protected:
    GSELock                               m_kLock;
    GSEPool<WSAEVENT, GSECOverlappedEx*>* m_pkOverlappedExPool;
};

#endif
#endif
