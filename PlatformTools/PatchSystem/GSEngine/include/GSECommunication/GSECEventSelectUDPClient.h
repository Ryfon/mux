﻿/**
* @file	    GSECEventSelectUDPClient.h 
* @brief    Event Select UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEventSelectUDPClient.h
* 摘    要: Event Select UDP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.17
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECEventSelectUDPClient__H__
#define __GSECEventSelectUDPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECEventSelectUDPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECEventSelectUDPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEventSelectUDPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);   
};

#endif
#endif
