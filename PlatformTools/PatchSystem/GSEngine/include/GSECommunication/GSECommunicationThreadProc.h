﻿/**
* @file	    GSECommunicationThreadProc.h 
* @brief    Communication Thread Proc Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationThreadProc.h
* 摘    要: 通讯线程接口程序声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.17
* 修改记录: 
*/

#ifndef __GSECommunicationThreadProc__H__
#define __GSECommunicationThreadProc__H__
#include "GSECommunication.h"
#include "GSEThreadProc.h"

class GSECommunicationApplicationServer;
class GSECommunicationApplicationCallback;
class GSESYSTEMENTRY_CLASS GSECommunicationThreadProc : public GSEThreadProc
{
public:
    GSECommunicationThreadProc(GSECommunicationApplicationServer* pkApplicationServer, GSECommunicationApplicationCallback* pkApplicationCallback);
    virtual ~GSECommunicationThreadProc(void);

protected:
    GSECommunicationApplicationServer*   m_pkApplicationServer;
    GSECommunicationApplicationCallback* m_pkApplicationCallback;
};

#endif
