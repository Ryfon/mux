﻿/**
* @file	    GSECSelectUDPClient.h 
* @brief    Select UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECSelectUDPClient.h
* 摘    要: Select UDP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.13
* 修改记录: 
*/

#ifndef __GSECSelectUDPClient__H__
#define __GSECSelectUDPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECSelectUDPClient : public GSECommunicationApplicationClient
{
public:
    GSECSelectUDPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    ~GSECSelectUDPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};
#endif
