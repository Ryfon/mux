﻿/**
* @file	    GSECEventSelectTCPServerThreadProc.h 
* @brief    Event Select TCP Server Thread Proc Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEventSelectTCPServerThreadProc.h
* 摘    要: Event Select TCP服务器线程接口程序声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.28
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECEventSelectTCPServerThreadProc__H__
#define __GSECEventSelectTCPServerThreadProc__H__
#include "GSECommunication.h"
#include "GSECommunicationThreadProc.h"
#include "GSEPool.h"
#include "GSELock.h"

class GSESYSTEMENTRY_CLASS GSECEventSelectTCPServerThreadProc : public GSECommunicationThreadProc
{
public:
    GSECEventSelectTCPServerThreadProc(GSECommunicationApplicationServer* pkApplicationServer, GSECommunicationApplicationCallback* pkApplicationCallback, GSEPool<WSAEVENT, GSECommunication*>* pkCommunicationPool) throw(GSEException);
    virtual ~GSECEventSelectTCPServerThreadProc(void) throw(GSEException);

protected:
    virtual void ThreadProc(void) throw(GSEException);
    virtual void Terminate() throw(GSEException);
    virtual void Clear() throw(GSEException);

protected:
    bool Erase(WSAEVENT hEvent, GSECommunication* pkCommunication);

protected:
    GSELock                               m_kLock;
    GSEPool<WSAEVENT, GSECommunication*>* m_pkCommunicationPool;
};

#endif
#endif
