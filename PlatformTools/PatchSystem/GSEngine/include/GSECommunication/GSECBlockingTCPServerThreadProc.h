﻿/**
* @file	    GSECBlockingTCPServerThreadProc.h 
* @brief    Blocking TCP Server Thread Proc Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECBlockingTCPServerThreadProc.h
* 摘    要: 阻塞模式TCP服务器线程接口程序声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.22
* 修改记录: 
*/

#ifndef __GSECBlockingTCPServerThreadProc__H__
#define __GSECBlockingTCPServerThreadProc__H__
#include "GSECommunication.h"
#include "GSECommunicationThreadProc.h"

class GSECommunicationApplicationServer;
class GSECommunicationApplicationCallback;
class GSESYSTEMENTRY_CLASS GSECBlockingTCPServerThreadProc : public GSECommunicationThreadProc
{
public:
    GSECBlockingTCPServerThreadProc(GSECommunication* pkCommunication, GSECommunicationApplicationServer* pkApplicationServer, GSECommunicationApplicationCallback* pkApplicationCallback);
    virtual ~GSECBlockingTCPServerThreadProc(void);

protected:
    virtual void ThreadProc(void) throw(GSEException);
    virtual void Terminate() throw(GSEException);
    virtual void Clear() throw(GSEException);

protected:
    GSECommunication* m_pkCommunication;
};

#endif
