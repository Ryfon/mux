﻿/**
* @file	    GSECommunicationUDP.h 
* @brief    GSECommunicationUDP Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationUDP.h
* 摘    要: UDP通讯接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.5.26
* 修改记录: 
*/

#ifndef __GSECommunicationUDP__H__
#define __GSECommunicationUDP__H__
#include "GSECommunication.h"

class GSESYSTEMENTRY_CLASS GSECommunicationUDP : public GSECommunication
{
public:
    GSECommunicationUDP(void){}
    virtual ~GSECommunicationUDP(void){}

public:
    virtual int  Send(char* pszData, INT nLen, LPVOID pkExtraData = NULL) throw();
    virtual int  Recv(char* pszBuf,  INT nLen, LPVOID pkExtraData = NULL) throw();

protected:
    virtual void CreateSocket(void) throw(GSEException);
    virtual bool Connect(const char* pszIP, short nPort);
    virtual bool Bind(const char* pszIP, short nPort);
    virtual bool Listen(int nBackLog) { return true; }

protected:
    SOCKADDR_IN m_kLocalSockAddr;
    SOCKADDR_IN m_kRemoteSockAddr;
    SOCKLEN_T   m_nRemoteSockAddrLen;
};
#endif
