﻿/**
* @file	    GSECOverlappedRoutineUDPClient.h 
* @brief    Overlapped Routine UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedRoutineUDPClient.h
* 摘    要: Overlapped Routine UDP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.25
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedRoutineUDPClient__H__
#define __GSECOverlappedRoutineUDPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedRoutineUDPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedRoutineUDPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedRoutineUDPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
