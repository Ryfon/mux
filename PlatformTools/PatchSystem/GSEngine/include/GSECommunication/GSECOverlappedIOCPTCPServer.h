﻿/**
* @file	    GSECOverlappedIOCPTCPServer.h 
* @brief    Overlapped IOCP TCP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedIOCPTCPServer.h
* 摘    要: Overlapped IOCP TCP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.01
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedIOCPTCPServer__H__
#define __GSECOverlappedIOCPTCPServer__H__
#include "GSECommunicationApplicationServer.h"
#include "GSEPool.h"

#define MAXWORKERTHREAD 10

class GSECOverlappedEx;
class GSECOverlappedAcceptEx;
class GSESYSTEMENTRY_CLASS GSECOverlappedIOCPTCPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedIOCPTCPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedIOCPTCPServer(void) throw(GSEException);

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException);
    virtual void DeAccept(GSECommunication* pkCommunication, LPVOID pkParam = NULL) throw(GSEException);
    virtual void DoProcess() throw(GSEException);

protected:
    static DWORD  WINAPI WorkerThread(LPVOID pParam) throw(GSEException);

protected:
    HANDLE                                         m_hCompletionPort;
    GSEPool<GSECommunication*, GSECOverlappedEx*>* m_pkOverlappedExPool;
    GSECOverlappedEx*                              m_pkOverlappedEx;
    GSECOverlappedAcceptEx*                        m_pkAcceptEx;
};

#endif
#endif
