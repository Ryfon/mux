﻿/**
* @file	    GSECSelectTCPServer.h 
* @brief    Select TCP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECSelectTCPServer.h
* 摘    要: Select TCP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.6.2
* 修改记录: 
*/

#ifndef __GSECSelectTCPServer__H__
#define __GSECSelectTCPServer__H__
#include "GSECommunicationApplicationServer.h"
#include "GSEPool.h"

class GSESYSTEMENTRY_CLASS GSECSelectTCPServer : public GSECommunicationApplicationServer
{
public:
    GSECSelectTCPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual  ~GSECSelectTCPServer(void) throw(GSEException);

protected:
    virtual void Create() throw(GSEException);
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException);
    virtual void DeAccept(GSECommunication* pkCommunication, LPVOID pkParam = NULL) throw(GSEException);
    virtual void DoProcess() throw(GSEException);

protected:
    GSEPool<Socket, GSECommunication*>* m_pkCommunicationPool;
};
#endif
