﻿/**
* @file	    GSECEPOLLApplicationProcess.h 
* @brief    EPOLL Application Process Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEPOLLApplicationProcess.h
* 摘    要: EPOLL 应用处理程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.02
* 修改记录: 
*/

#ifndef WIN32
#ifndef __GSECEPOLLApplicationProcess__H__
#define __GSECEPOLLApplicationProcess__H__
#include "GSECommunicationApplicationProcess.h"

class GSESYSTEMENTRY_CLASS GSECEPOLLApplicationProcess : public GSECommunicationApplicationProcess
{
public:
    GSECEPOLLApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEPOLLApplicationProcess(void) throw(GSEException);

protected:
    virtual void Process(void) throw(GSEException);

protected:
    int   m_nEpollDescriptor;
};

#endif
#endif
