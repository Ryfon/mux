﻿/**
* @file	    GSECOverlappedEventApplicationProcess.h 
* @brief    Overlapped Event Application Process Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedEventApplicationProcess.h
* 摘    要: Overlapped Event 应用处理程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.18
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedEventApplicationProcess__H__
#define __GSECOverlappedEventApplicationProcess__H__
#include "GSECommunicationApplicationProcess.h"
#include "GSECOverlappedEx.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedEventApplicationProcess : public GSECommunicationApplicationProcess
{
public:
    GSECOverlappedEventApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedEventApplicationProcess(void) throw(GSEException){}

protected:
    virtual void Process(void) throw(GSEException);

protected:
    GSECOverlappedEx  m_kOverlappedEx;
};

#endif
#endif
