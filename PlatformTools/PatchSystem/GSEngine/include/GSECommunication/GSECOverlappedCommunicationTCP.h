﻿/**
* @file	    GSECOverlappedCommunicationTCP.h 
* @brief    Overlapper Event TCP Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedCommunicationTCP.h
* 摘    要: Overlapped Event TCP 通讯接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.18
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedCommunicationTCP__H__
#define __GSECOverlappedCommunicationTCP__H__
#include "GSECommunicationTCP.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedCommunicationTCP : public GSECommunicationTCP
{
public:
    GSECOverlappedCommunicationTCP(void){}
    virtual ~GSECOverlappedCommunicationTCP(void){}

public:
    virtual int  Send(char* pszData, INT nLen, LPVOID pkExtraData = NULL) throw();
    virtual int  Recv(char* pszBuf,  INT nLen, LPVOID pkExtraData = NULL) throw();
};

#endif
#endif
