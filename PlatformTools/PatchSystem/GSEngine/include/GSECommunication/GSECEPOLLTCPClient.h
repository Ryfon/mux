﻿/**
* @file	    GSECEPOLLTCPClient.h 
* @brief    EPOLL TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEPOLLTCPClient.h
* 摘    要: EPOLL TCP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.02
* 修改记录: 
*/

#ifndef WIN32
#ifndef __GSECEPOLLTCPClient__H__
#define __GSECEPOLLTCPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECEPOLLTCPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECEPOLLTCPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEPOLLTCPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
