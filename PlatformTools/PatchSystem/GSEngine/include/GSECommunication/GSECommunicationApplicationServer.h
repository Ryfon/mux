﻿/**
* @file	    GSECommunicationApplicationServer.h 
* @brief    Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECommunicationApplicationServer.h
* 摘    要: Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.5.26
* 修改记录: 
*/

#ifndef __GSECommunicationApplicationServer__H__
#define __GSECommunicationApplicationServer__H__
#include "GSECommunicationApplication.h"

class GSECommunicationApplicationCallback;
class GSESYSTEMENTRY_CLASS GSECommunicationApplicationServer : public GSECommunicationApplication
{
friend class GSECommunicationFactory;
protected:
    GSECommunicationApplicationServer(GSECommunicationApplicationCallback* pkApplicationCallback)  throw(GSEException);
    virtual ~GSECommunicationApplicationServer(void) throw(GSEException) {}

public:
    virtual void Start(const char* pszIP, int nPort, int nListenBacklog = 0) throw(GSEException);
    virtual void Stop() throw(GSEException);

protected:
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException) = 0;
    virtual void DeAccept(GSECommunication* pkCommunication, LPVOID pkParam = NULL) throw(GSEException) = 0;

protected:
    bool   DoListen(int nBacklog = DEFAULTLISTENBACKLOG);
    Socket DoAccept(SOCKADDR_IN& kSockAddr, SOCKLEN_T& nSockAddrLen);
};
#endif
