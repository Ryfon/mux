﻿/**
* @file	    GSECOverlappedIOCPTCPClient.h 
* @brief    Overlapped IOCP TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedIOCPTCPClient.h
* 摘    要: Overlapped IOCP TCP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.01
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedIOCPTCPClient__H__
#define __GSECOverlappedIOCPTCPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedIOCPTCPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedIOCPTCPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedIOCPTCPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif
