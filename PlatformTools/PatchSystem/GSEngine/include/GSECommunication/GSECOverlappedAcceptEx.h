﻿/**
* @file	    GSECOverlappedAcceptEx.h 
* @brief    Overlapped AcceptEx Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedAcceptEx.h
* 摘    要: Overlapped AcceptEx接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.01
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedAcceptEx__H__
#define __GSECOverlappedAcceptEx__H__
#include "GSECommunication.h"
#include "MSWSock.h"

class GSECOverlappedEx;
class GSECommunicationApplicationServer;
class GSECommunicationApplicationCallback;
class GSESYSTEMENTRY_CLASS GSECOverlappedAcceptEx : public GSEMemoryObject
{
public:
    GSECOverlappedAcceptEx(GSECommunication* pkCommunication) throw(GSEException);
    ~GSECOverlappedAcceptEx(void) throw(GSEException);

public:
    virtual bool PostAccept(GSECOverlappedEx* pkOverlappedEx, GSECommunicationApplicationServer* pkServer, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual void GetAddress(sockaddr_in& kLocalAddr, sockaddr_in& kRemoteAddr, GSECOverlappedEx* pkOverlappedEx);

protected:
    GSECommunication*	      m_pkCommunication;
    LPFN_ACCEPTEX             m_pfnAcceptEx;
    LPFN_GETACCEPTEXSOCKADDRS m_pfnGetAcceptExSockaddrs;
    char                      m_szAcceptBuf[2 * (sizeof(struct sockaddr_in) + 16)];
};

#endif
#endif
