﻿/**
* @file	    GSECOverlappedIOCPApplicationProcess.h 
* @brief    Overlapped IOCP Application Process Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedIOCPApplicationProcess.h
* 摘    要: Overlapped IOCP 应用处理程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.30
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedIOCPApplicationProcess__H__
#define __GSECOverlappedIOCPApplicationProcess__H__
#include "GSECommunicationApplicationProcess.h"
#include "GSECOverlappedEx.h"

class GSECommunicationApplication;
class GSESYSTEMENTRY_CLASS GSECOverlappedIOCPApplicationProcess : public GSECommunicationApplicationProcess
{
public:
    GSECOverlappedIOCPApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedIOCPApplicationProcess(void) throw(GSEException);

protected:
    virtual void Process(void) throw(GSEException);

protected:
    HANDLE           m_hCompletionPort;
    GSECOverlappedEx m_kOverlappedEx;
};

#endif
#endif
