﻿/**
* @file	    GSECAsyncSelectUDPServer.h 
* @brief    Asysnc Select UDP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECAsyncSelectUDPServer.h
* 摘    要: Asysnc Select UDP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.16
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECAsyncSelectUDPServer__H__
#define __GSECAsyncSelectUDPServer__H__
#include "GSECommunicationApplicationServer.h"

class GSECommunicationApplicationProcess;
class GSESYSTEMENTRY_CLASS GSECAsyncSelectUDPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECAsyncSelectUDPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECAsyncSelectUDPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);

protected:
    GSECommunicationApplicationProcess* m_pkApplicationProcess;
};

#endif
#endif
