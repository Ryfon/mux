﻿/**
* @file	    GSECBlockingTCPServer.h 
* @brief    Server UDP Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECBlockingTCPServer.h
* 摘    要: Blocking UDP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.10
* 修改记录: 
*/

#ifndef __GSECBlockingUDPServer__H__
#define __GSECBlockingUDPServer__H__
#include "GSECommunicationApplicationServer.h"

class GSECommunicationApplicationProcess;
class GSESYSTEMENTRY_CLASS GSECBlockingUDPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECBlockingUDPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECBlockingUDPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);

protected:
    GSECommunicationApplicationProcess* m_pkApplicationProcess;
};
#endif
