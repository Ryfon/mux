﻿/**
* @file	    GSECEPOLLUDPClient.h 
* @brief    EPOLL UDP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEPOLLUDPClient.h
* 摘    要: EPOLL UDP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.02
* 修改记录: 
*/

#ifndef WIN32
#ifndef __GSECEPOLLUDPClient__H__
#define __GSECEPOLLUDPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECEPOLLUDPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECEPOLLUDPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEPOLLUDPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};

#endif
#endif

