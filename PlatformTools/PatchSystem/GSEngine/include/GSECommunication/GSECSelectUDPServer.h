﻿/**
* @file	    GSECSelectUDPServer.h 
* @brief    Select UDP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECSelectUDPServer.h
* 摘    要: Select UDP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.13
* 修改记录: 
*/

#ifndef __GSECSelectUDPServer__H__
#define __GSECSelectUDPServer__H__
#include "GSECommunicationApplicationServer.h"

class GSECommunicationApplicationProcess;
class GSESYSTEMENTRY_CLASS GSECSelectUDPServer : public GSECommunicationApplicationServer
{
public:
    GSECSelectUDPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECSelectUDPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);

protected:
    GSECommunicationApplicationProcess* m_pkApplicationProcess;
};
#endif
