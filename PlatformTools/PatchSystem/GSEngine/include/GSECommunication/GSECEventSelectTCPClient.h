﻿/**
* @file	    GSECEventSelectTCPClient.h 
* @brief    Event Select TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEventSelectTCPClient.h
* 摘    要: Event Select TCP Client接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.17
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECEventSelectTCPClient__H__
#define __GSECEventSelectTCPClient__H__
#include "GSECommunicationApplicationClient.h"

class GSESYSTEMENTRY_CLASS GSECEventSelectTCPClient : public GSECommunicationApplicationClient
{
friend class GSECommunicationFactory;
protected:
    GSECEventSelectTCPClient(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEventSelectTCPClient(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
};


#endif
#endif
