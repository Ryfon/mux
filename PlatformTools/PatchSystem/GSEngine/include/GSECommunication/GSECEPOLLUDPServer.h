﻿/**
* @file	    GSECEPOLLUDPServer.h 
* @brief    EPOLL UDP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEPOLLUDPServer.h
* 摘    要: EPOLL UDP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.02
* 修改记录: 
*/

#ifndef WIN32
#ifndef __GSECEPOLLUDPServer__H__
#define __GSECEPOLLUDPServer__H__
#include "GSECommunicationApplicationServer.h"

class GSECommunicationApplicationProcess;
class GSESYSTEMENTRY_CLASS GSECEPOLLUDPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECEPOLLUDPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEPOLLUDPServer(void) throw(GSEException) {}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);

protected:
    GSECommunicationApplicationProcess* m_pkApplicationProcess;
};

#endif
#endif
