﻿/**
* @file	    GSECOverlappedCommunicationUDP.h 
* @brief    Overlapper Event UDP Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedCommunicationUDP.h
* 摘    要: Overlapped Event UDP 通讯接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.24
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedCommunicationUDP__H__
#define __GSECOverlappedCommunicationUDP__H__
#include "GSECommunicationUDP.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedCommunicationUDP : public GSECommunicationUDP
{
public:
    GSECOverlappedCommunicationUDP(void){}
    virtual ~GSECOverlappedCommunicationUDP(void){}

public:
    virtual int  Send(char* pszData, INT nLen, LPVOID pkExtraData = NULL) throw();
    virtual int  Recv(char* pszBuf,  INT nLen, LPVOID pkExtraData = NULL) throw();
};

#endif
#endif
