﻿/**
* @file	    GSECOverlappedRoutineUDPServer.h 
* @brief    Overlapped Routine UDP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedRoutineUDPServer.h
* 摘    要: Overlapped Routine UDP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.25
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedRoutineUDPServer__H__
#define __GSECOverlappedRoutineUDPServer__H__
#include "GSECommunicationApplicationServer.h"

class GSECommunicationApplicationProcess;
class GSESYSTEMENTRY_CLASS GSECOverlappedRoutineUDPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedRoutineUDPServer(GSECommunicationApplicationCallback* pkApplicaationCallback) throw(GSEException);
    virtual ~GSECOverlappedRoutineUDPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);

protected:
    GSECommunicationApplicationProcess*  m_pkApplicationProcess;
};

#endif
#endif
