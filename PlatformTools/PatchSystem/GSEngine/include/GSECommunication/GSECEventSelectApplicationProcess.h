﻿/**
* @file	    GSECEventSelectApplicationProcess.h 
* @brief    Event Select Application Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEventSelectApplicationProcess.h
* 摘    要: Event Select 应用程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.17
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECEventSelectApplicationProcess__H__
#define __GSECEventSelectApplicationProcess__H__
#include "GSECommunicationApplicationProcess.h"

class GSESYSTEMENTRY_CLASS GSECEventSelectApplicationProcess : public GSECommunicationApplicationProcess
{
public:
    GSECEventSelectApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECEventSelectApplicationProcess(void) throw(GSEException);

protected:
    virtual void Process(void) throw(GSEException);

protected:
    WSAEVENT  m_hEvent;
};

#endif
#endif
