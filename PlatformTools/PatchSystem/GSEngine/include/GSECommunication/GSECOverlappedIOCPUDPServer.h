﻿/**
* @file	    GSECOverlappedIOCPUDPServer.h 
* @brief    Overlapped IOCP UDP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedIOCPUDPServer.h
* 摘    要: Overlapped IOCP UDP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.01
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedIOCPUDPServer__H__
#define __GSECOverlappedIOCPUDPServer__H__
#include "GSECommunicationApplicationServer.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedIOCPUDPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECOverlappedIOCPUDPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedIOCPUDPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);

protected:
    GSECommunicationApplicationProcess* m_pkApplicationProcess;
};

#endif
#endif
