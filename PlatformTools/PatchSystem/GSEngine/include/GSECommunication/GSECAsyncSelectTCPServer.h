﻿/**
* @file	    GSECAsyncSelectTCPServer.h 
* @brief    Asysnc Select TCP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECAsyncSelectTCPServer.h
* 摘    要: Asysnc Select TCP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.15
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECAsyncSelectTCPServer__H__
#define __GSECAsyncSelectTCPServer__H__
#include "GSECommunicationApplicationServer.h"
#include "GSEPool.h"

class GSESYSTEMENTRY_CLASS GSECAsyncSelectTCPServer : public GSECommunicationApplicationServer
{
friend class GSECommunicationFactory;
protected:
    GSECAsyncSelectTCPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECAsyncSelectTCPServer(void) throw(GSEException){}

public:
    HWND GetHWND();
    void ProxyAccept() throw(GSEException);
    void ProxyDeAccept(GSECommunication* pkCommunication);
    GSECommunication* GetCommunication(Socket hSocket);
    GSECommunication* GetServerCommunication();
    GSECommunicationApplicationCallback* GetApplicationCallback();

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException);
    virtual void DeAccept(GSECommunication* pkCommunication, LPVOID pkParam = NULL) throw(GSEException);
    virtual void DoProcess() throw(GSEException);

protected:
    HWND                                m_hWnd;
    GSEPool<Socket, GSECommunication*>* m_pkCommunicationPool;
};

#endif
#endif
