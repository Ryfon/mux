﻿/**
* @file	    GSECOverlappedRoutineApplicationProcess.h 
* @brief    Overlapped Routine Application Process Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedRoutineApplicationProcess.h
* 摘    要: Overlapped Routine应用处理程序接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.24
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedRoutineApplicationProcess__H__
#define __GSECOverlappedRoutineApplicationProcess__H__
#include "GSECommunicationApplicationProcess.h"
#include "GSECOverlappedEx.h"

class GSECommunicationApplication;
class GSESYSTEMENTRY_CLASS GSECOverlappedRoutineApplicationProcess : public GSECommunicationApplicationProcess
{
public:
    GSECOverlappedRoutineApplicationProcess(GSECommunication* pkCommunication, GSECommunicationApplication* pkApplication, GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedRoutineApplicationProcess(void) throw(GSEException){}

protected:
    virtual void Process(void) throw(GSEException);

protected:
    GSECOverlappedEx  m_kOverlappedEx;
};

#endif
#endif
