﻿/**
* @file	    GSECOverlappedRoutineCommunicationUDP.h 
* @brief    Overlapper Routine UDP Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedRoutineCommunicationUDP.h
* 摘    要: Overlapped Routine UDP 通讯接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.25
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedRoutineCommunicationUDP__H__
#define __GSECOverlappedRoutineCommunicationUDP__H__
#include "GSECommunicationUDP.h"

class GSESYSTEMENTRY_CLASS GSECOverlappedRoutineCommunicationUDP : public GSECommunicationUDP
{
public:
    GSECOverlappedRoutineCommunicationUDP(void){}
    virtual ~GSECOverlappedRoutineCommunicationUDP(void){}

public:
    virtual int  Send(char* pszData, INT nLen, LPVOID pkExtraData = NULL) throw();
    virtual int  Recv(char* pszBuf,  INT nLen, LPVOID pkExtraData = NULL) throw();
};

#endif
#endif
