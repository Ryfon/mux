﻿/**
* @file	    GSECEventSelectTCPServer.h 
* @brief    Event Select TCP Server Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECEventSelectTCPServer.h
* 摘    要: Event Select TCP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.17
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECEventSelectTCPServer__H__
#define __GSECEventSelectTCPServer__H__
#include "GSECommunicationApplicationServer.h"
#include "GSELock.h"
#include "GSEThread.h"
#include "GSEPool.h"
#include "GSECOverlappedEx.h"

class GSESYSTEMENTRY_CLASS GSECEventSelectTCPServer : public GSECommunicationApplicationServer
{
friend class GSECEventSelectTCPServerThreadProc;
friend class GSECommunicationFactory;
protected:
    GSECEventSelectTCPServer(GSECommunicationApplicationCallback* pkApplicationCallback = NULL) throw(GSEException);
    virtual ~GSECEventSelectTCPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException);
    virtual void DeAccept(GSECommunication* pkCommmunication, LPVOID pkParam = NULL) throw(GSEException);
    virtual void DoProcess() throw(GSEException);

protected:
    void CreateThread() throw(GSEException);
    void DestroyThread(GSEThread* pkThread);

protected:
    GSELock                               m_kLock;
    WSAEVENT                              m_hEvent;
    GSEPool<WSAEVENT, GSECommunication*>* m_pkCommunicationPool;
    GSEPool<GSEThread*, GSEThread*>*      m_pkThreadPool;
};

#endif
#endif
