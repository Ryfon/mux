﻿/**
* @file	    GSECOverlappedEventTCPServer.h 
* @brief    Overlapped Event TCP Client Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECOverlappedEventTCPServer.h
* 摘    要: Overlapped Event TCP Server接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.06.22
* 修改记录: 
*/

#ifdef  WIN32
#ifndef __GSECOverlappedEventTCPServer__H__
#define __GSECOverlappedEventTCPServer__H__
#include "GSECommunication.h"
#include "GSECommunicationApplicationServer.h"
#include "GSEPool.h"
#include "GSELock.h"

class GSEThread;
class GSECOverlappedEx;
class GSECOverlappedAcceptEx;
class GSESYSTEMENTRY_CLASS GSECOverlappedEventTCPServer : public GSECommunicationApplicationServer
{
friend class GSECOverlappedEventTCPServerThreadProc;
friend class GSECommunicationFactory;
protected:
    GSECOverlappedEventTCPServer(GSECommunicationApplicationCallback* pkApplicationCallback) throw(GSEException);
    virtual ~GSECOverlappedEventTCPServer(void) throw(GSEException){}

protected:
    virtual void Create() throw(GSEException);
    virtual void Destroy() throw(GSEException);
    virtual void PostAccept(Socket hSocket = INVALID_SOCKET) throw(GSEException);
    virtual void DeAccept(GSECommunication* pkCommunication, LPVOID pkParam = NULL) throw(GSEException);
    virtual void DoProcess() throw(GSEException);

protected:
    void  CreateThread() throw(GSEException);
    void  DestroyThread(GSEThread* pkThread);

protected:
    GSELock                                m_kLock;
    GSECOverlappedEx*                      m_pkOverlappedEx;
    GSECOverlappedAcceptEx*                m_pkAcceptEx;
    GSEPool<WSAEVENT, GSECOverlappedEx*>*  m_pkOverlappedExPool;
    GSEPool<GSEThread*, GSEThread*>*       m_pkThreadPool;
};

#endif
#endif
