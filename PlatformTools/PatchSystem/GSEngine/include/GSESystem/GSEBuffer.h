﻿/**
* @file	    GSEBuffer.h 
* @brief    Buffer Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEBuffer.h
* 摘    要: 缓冲池接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.6.9
* 修改记录: 
*/

#ifndef __GSEBuffer__H__
#define __GSEBuffer__H__
#include "GSESystem.h"

class GSESYSTEMENTRY_CLASS GSEBuffer
{
public:
    GSEBuffer(char* pszBuffer, int nLen);
    ~GSEBuffer(void){}

    char* GetBuffer()
    {
        return m_pszBuffer;
    }
    int GetLen()
    {
        return m_nLen;
    }

protected:
    char* m_pszBuffer;
    int   m_nLen;
};
#endif
