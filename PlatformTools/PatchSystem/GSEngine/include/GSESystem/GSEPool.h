﻿/**
* @file	    GSEPool.h 
* @brief    Object Pool Abstract Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEPool.h
* 摘    要: 对象池抽象接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.13
* 修改记录: 
*/

#ifndef __GSEPool__H__
#define __GSEPool__H__
#include "GSESystem.h"
#include "GSEMemoryObject.h"
#include "GSEException.h"

#define	DEFAULTPOOLSIZE	37
#define	DEFAULTINCSIZE	13

template<typename Key, typename Value> class GSESYSTEMENTRY_CLASS GSEPool : public GSEMemoryObject
{
public:
    GSEPool(size_t nMaxSize = DEFAULTPOOLSIZE, size_t nIncSize = DEFAULTINCSIZE) throw(GSEException);
    virtual ~GSEPool(void) throw(GSEException);

public:
    size_t GetIncSize();
    void   SetIncSize(size_t nIncSize);
    size_t GetCurCount();
    size_t GetCapacity();

public:
	virtual void  First() = 0;
    virtual void  Next() = 0;
    virtual bool  IsDone() const = 0;
    virtual Value GetCurrent(Key& kKey) throw(GSEException) = 0;
	virtual Value EraseCurrent(Key& kKey) throw(GSEException) = 0;

public:
    virtual Value Find(Key& kKey) = 0;
	virtual void  Append(Key kKey, Value kValue = 0) throw(GSEException) = 0;
	virtual Value Erase(Key pkKey) = 0;
	virtual void  EraseAll() = 0;

private:
    GSEPool(const GSEPool&);
    GSEPool& operator=(const GSEPool&);

protected:
    size_t  m_nSlotCapacity;
    size_t  m_nCurEntityCount;
    size_t  m_nSlotIncSize;
};

#endif
