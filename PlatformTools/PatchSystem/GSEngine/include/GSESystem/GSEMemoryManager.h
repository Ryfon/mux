﻿/**
* @file	    GSEMemoryManager.h 
* @brief    Memory Manager Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEMemoryManager.h
* 摘    要: 内存管理器接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.29
* 修改记录: 
*/

#ifndef __GSEMemoryManager__H__
#define __GSEMemoryManager__H__

class GSEMemoryAllocator;
class GSESYSTEMENTRY_CLASS GSEMemoryManager
{
public:
    ~GSEMemoryManager();
    static GSEMemoryManager* Instance() throw(GSEException);

    void* Allocate(size_t nSizeInBytes, const char *pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);
    void* Reallocate(void* pvMem, size_t nSizeInBytes, const char *pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);
    void  Deallocate(void* pvMem, size_t nSizeinBytes = -1);


protected:
    GSEMemoryManager() throw(GSEException);
    GSEMemoryManager(const GSEMemoryManager&) {};
    GSEMemoryManager& operator=(const GSEMemoryManager&)
    {
        return *this;
    }

protected:
    static GSEMemoryManager* ms_pkMemManager;
    GSEMemoryAllocator*      m_pkAllocator;
};
#endif
