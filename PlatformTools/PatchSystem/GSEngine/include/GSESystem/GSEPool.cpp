﻿/**
* @file	    GSEPool.cpp 
* @brief    Object Pool Abstract Interface Definition
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEPool.cpp
* 摘    要: 对象池抽象接口定义
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.13
* 修改记录: 
*/

#ifndef __GSEPool__CPP__
#define __GSEPool__CPP__
#include "GSEPool.h"

template<typename Key, typename Value> GSEPool<Key, Value>::GSEPool(size_t nMaxSize, size_t nIncSize) throw(GSEException)
{
    assert((nMaxSize > 0) && (nIncSize > 0));
    m_nSlotCapacity   = nMaxSize;
    m_nSlotIncSize	  = nIncSize;
    m_nCurEntityCount = 0;
}

template<typename Key, typename Value> GSEPool<Key, Value>::~GSEPool(void) throw(GSEException)
{
    m_nCurEntityCount = 0;
}

template<typename Key, typename Value> size_t GSEPool<Key, Value>::GetIncSize()
{
    return m_nSlotIncSize;
}

template<typename Key, typename Value> void GSEPool<Key, Value>::SetIncSize(size_t nIncSize)
{
    assert(nIncSize > 0);
    m_nSlotIncSize = nIncSize;
}

template<typename Key, typename Value> size_t GSEPool<Key, Value>::GetCurCount()
{
    return m_nCurEntityCount;
}

template<typename Key, typename Value> size_t GSEPool<Key, Value>::GetCapacity()
{
    return m_nSlotCapacity;
}
#endif
