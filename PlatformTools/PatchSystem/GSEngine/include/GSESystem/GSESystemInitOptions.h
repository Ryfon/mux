﻿/**
* @file	    GSESystemInitOptions.h 
* @brief    System Initoptions Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSESystemInitOptions.h
* 摘    要: 系统初始化接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.20
* 修改记录: 
*/

#ifndef __GSESystemInitOptions__H__
#define __GSESystemInitOptions__H__
#include "GSESystem.h"
#include "GSEException.h"
#include "GSEMemoryManager.h"
#include "GSEErrorHandling.h"
#include "GSEThreadFactory.h"

class GSESYSTEMENTRY_CLASS GSESystemInitOptions
{
public:
    GSESystemInitOptions(short nErrorHandlingFalgs = GSEERROR_SCREEN_MASK) throw(GSEException);
    ~GSESystemInitOptions(void) throw(GSEException);
};

#endif
