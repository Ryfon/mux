﻿/**
* @file	    GSEException.h 
* @brief    Exception Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEException.h
* 摘    要: 异常接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.5.13
* 修改记录:
*/

#ifndef __GSEException__H__
#define __GSEException__H__
#include "GSESystem.h"

class GSESYSTEMENTRY_CLASS GSEException
{
public:
    GSEException(const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction, const char* pszMsg, ...) throw();
    ~GSEException(void) throw(){}

public:
    size_t Getm_nRethrowCount() throw()
    {
        return m_nRethrowCount;
    }
    const char* GetSourceFile(size_t nIndex) throw()
    {
        assert(nIndex < m_nRethrowCount);
        return m_szSourceFile[nIndex];
    }
    size_t GetSourceLine(size_t nIndex) throw()
    {
        assert(nIndex < m_nRethrowCount);
        return m_nSourceLine[m_nRethrowCount];
    }
    const char* GetSourceFunction(size_t nIndex) throw()
    {
        assert(nIndex < m_nRethrowCount);
        return m_szSourceFunction[m_nRethrowCount];
    }
    const char* GetMessage(size_t nIndex) throw()
    {
        assert(nIndex < m_nRethrowCount);
        return m_szMessage[m_nRethrowCount];
    }

public:
    void Append(const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction, const char* pszMsg, ...);

protected:
    size_t  m_nRethrowCount;
    char    m_szSourceFile[MAXFILENAMELEN][MAXRETHROW];
    size_t  m_nSourceLine[MAXRETHROW];
    char    m_szSourceFunction[MAXFUNCTIONNAMELEN][MAXRETHROW];
    char    m_szMessage[MAXMSGLEN][MAXRETHROW];
};

#endif
