﻿/**
* @file	    GSEMGuard.h 
* @brief    Guard Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEGuard.h
* 摘    要: 守卫接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.3.21
* 修改记录: 
*/

#ifndef __GSEGuard__H__
#define __GSEGuard__H__
#include "GSEGuardObj.h"
#include "GSEException.h"

class GSESYSTEMENTRY_CLASS GSEGuard
{
public:
    GSEGuard(GSEGuardObj* pkGuardObj) throw(GSEException);
    ~GSEGuard(void) throw(GSEException);

protected:
    GSEGuardObj* m_pkGuardObj;
};
#endif
