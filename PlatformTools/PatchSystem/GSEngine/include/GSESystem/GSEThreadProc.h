﻿/**
* @file	    GSEThreadProc.h 
* @brief    Thread Proc Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEThreadProc.h
* 摘    要: 线程接口程序声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.3.16
* 修改记录: 
*/

#ifndef __GSEThreadProc__H__
#define __GSEThreadProc__H__
#include "GSESystem.h"
#include "GSEException.h"
#include "GSEMemoryObject.h"

class GSEThread;
class GSESYSTEMENTRY_CLASS GSEThreadProc : public GSEMemoryObject
{
friend class GSEThread;
public:
    GSEThreadProc(void)
    {
        m_pkThread = NULL;
    }
    virtual ~GSEThreadProc(void)
    {
        m_pkThread = NULL;
    }

protected:
    virtual void ThreadProc(void) throw(GSEException) = 0;	
    virtual void Terminate() throw(GSEException) = 0;
    virtual void Clear() throw(GSEException){}

protected:
    void SetThread(GSEThread* pkThread)
    {
        assert(pkThread);
        m_pkThread = pkThread;
    }

protected:
    GSEThread* m_pkThread;
};
#endif
