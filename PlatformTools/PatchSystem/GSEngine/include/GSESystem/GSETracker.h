﻿/**
* @file	    GSETracker.h
* @brief    Tracker Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSETracker.h
* 摘    要: 追踪接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.28
* 修改记录: 
*/

#ifndef __GSETracker__H__
#define __GSETracker__H__
#include "GSESystem.h"
#include "GSEMemory.h"

class GSESYSTEMENTRY_CLASS GSETracker
{
friend class GSEErrorHandling;
public:
    ~GSETracker(void);

public:
    void Append(const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);
    void Depend();
    const char* GetTrackerContent()
    {
        return m_pszTracker;
    }

protected:
    GSETracker(void) throw(GSEException);
    GSETracker(const GSETracker&){}
    GSETracker& operator=(const GSETracker&){return *this;}

protected:
    char*       m_pszTracker;
    size_t      m_nTrackerCapacity;
    size_t      m_nTrackerOffset;
    static char ms_cSeperatorOfRecord;
    static char ms_cSeperatorOfSegment;
};
#endif

