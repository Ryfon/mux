﻿/**
* @file	    GSECondition.h 
* @brief    Conditional Variable Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECondition.h
* 摘    要: 条件变量接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.5.7
* 修改记录: 
*/

#ifndef __GSECondition__H__
#define __GSECondition__H__
#include "GSESystem.h"
#include "GSELock.h"

class GSESYSTEMENTRY_CLASS GSECondition : public GSEGuardObj
{
public:
    GSECondition(unsigned long nMaxCount = 1024) throw(GSEException);
    ~GSECondition(void) throw(GSEException);

public:
    GSELock* GetLock()
    {
        return &m_kLock;
    }

public:
    bool Wait(DWORD dwTime = INFINITE) throw(GSEException);
    void Signal() throw(GSEException);
    void Broadcast() throw(GSEException);

protected:
    virtual void DoSomething() throw()
    {
        ++m_nWaitCount;
    }
    virtual void UndoSomething() throw()
    {
        --m_nWaitCount;
    }

protected:
    unsigned long  m_nWaitCount;
    unsigned long  m_nMaxCount;
    GSELock        m_kLock;
#ifdef WIN32
    HANDLE         m_kSemaphore;
#else
    pthread_cond_t m_kCond;
#endif
};
#endif
