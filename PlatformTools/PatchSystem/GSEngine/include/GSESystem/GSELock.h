﻿/**
* @file	    GSELock.h 
* @brief    Lock Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSELock.h
* 摘    要: 锁接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.3.21
* 修改记录: 
*/

#ifndef __GSELock__H__
#define __GSELock__H__
#include "GSELockInterface.h"

class GSESYSTEMENTRY_CLASS GSELock : public GSELockInterface
{
public:
    GSELock(void) throw(GSEException);
    virtual ~GSELock(void) throw(GSEException);

public:
    virtual void Lock() throw(GSEException);
    virtual void Unlock() throw(GSEException);

public:
    CRITICAL_SECTION* GetLock()
    {
        return &m_kLock;
    }

protected:
    CRITICAL_SECTION     m_kLock;
#ifndef WIN32
    pthread_mutexattr_t  m_kLockAttr;
#endif
};
#endif
