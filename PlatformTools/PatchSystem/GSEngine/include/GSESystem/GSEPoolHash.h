﻿/**
* @file	    GSEPoolHash.h 
* @brief    Hash Object Pool Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEPoolHash.h
* 摘    要: 哈希对象池接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.07.13
* 修改记录: 
*/

#ifndef __GSEPoolHash__H__
#define __GSEPoolHash__H__
#include "GSEPool.h"

//Value必需具有Value& operator =(const Value&)
template<typename Key, typename Value> class  GSEHashComponent : public GSEMemoryObject
{
public:
    Key               kKey;
    Value             kValue;
    GSEHashComponent* pkNext;
	GSEHashComponent* pkPrev;
};

template<typename Key, typename Value> class GSESYSTEMENTRY_CLASS GSEPoolHash : public GSEPool<Key, Value>
{
public:
    GSEPoolHash(size_t nMaxSize = DEFAULTPOOLSIZE, size_t nIncSize = DEFAULTINCSIZE) throw(GSEException);
    virtual ~GSEPoolHash(void) throw(GSEException);

public:
    virtual void  First();
    virtual void  Next();
    virtual bool  IsDone() const;
    virtual Value GetCurrent(Key& kKey) throw(GSEException);
	virtual Value EraseCurrent(Key& kKey) throw(GSEException);

public:
    virtual Value Find(Key& kKey);
    virtual void  Append(Key kKey, Value kValue = 0) throw(GSEException);
    virtual Value Erase(Key kKey);
	virtual void  EraseAll();

protected:
    void ClearAllComponents() throw(GSEException);

private:
    GSEPoolHash(const GSEPoolHash&);
    GSEPoolHash& operator=(const GSEPoolHash&);

protected:
	GSEHashComponent<Key, Value>*  m_pkHashIterator;;
    GSEHashComponent<Key, Value>** m_ppkHashComponents;
};

#endif
