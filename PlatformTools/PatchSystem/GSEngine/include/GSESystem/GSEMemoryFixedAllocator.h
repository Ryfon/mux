﻿/**
* @file	    GSEMemoryFixedAllocator.h 
* @brief    Fixed Memory Object Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEMemorySmallObjectAllocator.h
* 摘    要: 固定对象内存管理接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.28
* 修改记录: 
*/

#ifndef __GSEMemoryFixedAllocator__H__
#define __GSEMemoryFixedAllocator__H__
#include "GSEMemoryChunk.h"
#include "GSELock.h"

#ifndef DEFAULT_CHUNK_SIZE
#define DEFAULT_CHUNK_SIZE 25600
#endif

#ifndef MAX_SMALL_OBJECT_SIZE
#define MAX_SMALL_OBJECT_SIZE 256
#endif

class GSEMemoryFixedAllocator
{
public:
    ~GSEMemoryFixedAllocator(void);

friend class GSEMemorySmallObjectAllocator;
protected:
    GSEMemoryFixedAllocator(void);

protected:
    size_t BlockSize() const
    { 
        return m_nBlockSize; 
    }
    bool operator<(size_t rhs) const
    { 
        return BlockSize() < rhs; 
    }
    size_t GetNumChunks() const
    { 
        return m_nNumChunks;
    }

private:
    void  Init(size_t nBlockSize);
    void* Allocate() throw(GSEException);
    void  Deallocate(void* p);

protected:
    void             Push_Back(GSEMemoryChunk& kMemoryChunk);
    void             Pop_Back();
    void             Reserve(size_t stNewSize) throw(GSEException);
    void             DoDeallocate(void* p);
    GSEMemoryChunk*  VicinityFind(void* p);
    
protected:
    size_t           m_nBlockSize;
    unsigned char    m_ucNumBlocks;
    
    GSEMemoryChunk*  m_pkChunks;
    size_t           m_nNumChunks;
    size_t           m_nMaxNumChunks;
    GSEMemoryChunk*  m_pkAllocChunk;
    GSEMemoryChunk*  m_pkDeallocChunk;
    GSEMemoryChunk*  m_pkEmptyChunk;
    GSELock          m_kLock;    
};

#endif
