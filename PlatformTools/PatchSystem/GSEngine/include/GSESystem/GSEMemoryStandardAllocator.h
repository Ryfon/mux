﻿/**
* @file	    GSEMemoryStandardAllocator.h 
* @brief    Standard Memory Manager Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEMemoryAllocator.h
* 摘    要: 标准内存管理接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.29
* 修改记录: 
*/

#ifndef __GSEMemoryStandardAllocator__H__
#define __GSEMemoryStandardAllocator__H__
#include "GSEMemoryAllocator.h"
#include "GSEMemorySmallObjectAllocator.h"

class GSESYSTEMENTRY_CLASS GSEMemoryStandardAllocator : public GSEMemoryAllocator
{
public:
    GSEMemoryStandardAllocator() throw(GSEException);
    virtual ~GSEMemoryStandardAllocator();

public:
    virtual void* Allocate(size_t nSizeInBytes, const char* pszSoruceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);
	virtual void* Reallocate(void* pvMemory, size_t nSizeInBytes, const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);
    virtual void  Deallocate(void* pvMemory, size_t nSizeInBytes);

protected:
    void   SetAddressSize(void* pvMemory, size_t nSizeInBytes);
    size_t GetAddrssSize(void* pvMemory);

protected:
    GSEMemorySmallObjectAllocator* m_pkSmallAlloc;     
};
#endif
