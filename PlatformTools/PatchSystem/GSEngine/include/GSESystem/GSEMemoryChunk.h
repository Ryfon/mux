﻿/**
* @file	    GSEMemoryChunk.h 
* @brief    Chunk Memory Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEMemoryChunk.h
* 摘    要: 块内存内存管理接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.30
* 修改记录: 
*/

#ifndef __GSEMemoryChunk__H__
#define __GSEMemoryChunk__H__
#include "GSEMemory.h"
#include "GSEException.h"

class GSEMemoryChunk
{
public:
    ~GSEMemoryChunk(void){}

friend class GSEMemoryFixedAllocator;
protected:
    GSEMemoryChunk(void){}

protected:
    void  Init(size_t nBlockSize, unsigned char ucBlocks) throw(GSEException);
    void  Reset(size_t nBlockSize, unsigned char ucBlocks);
    void  Release();
    void* Allocate(size_t nBlockSize);
    void  Deallocate(void* p, size_t stBlockSize);
    bool  HasAvailable(unsigned char ucNumBlocks) const;
    bool  HasBlock(unsigned char * p, size_t nChunkLength) const;
    bool  IsFilled() const;

protected:
    unsigned char* m_pucData;
    unsigned char  m_ucFirstAvailableBlock;
    unsigned char  m_ucBlocksAvailable;
};
#endif
