﻿/**
* @file	    GSEMemorySmallObjectAllocator.h 
* @brief    Memory Manager For Small Object Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEMemorySmallObjectAllocator.h
* 摘    要: 小对象内存管理接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.28
* 修改记录: 
*/

#ifndef __GSEMemorySmallObjectAllocator__H__
#define __GSEMemorySmallObjectAllocator__H__
#include "GSEMemoryFixedAllocator.h"

//////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design 
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any 
//     purpose is hereby granted without fee, provided that the above 
//     copyright notice appear in all copies and that both that copyright 
//     notice and this permission notice appear in supporting documentation.
// The author or Addison-Welsey Longman make no representations about the 
//     suitability of this software for any purpose. It is provided "as is" 
//     without express or implied warranty.
//////////////////////////////////////////////////////////////////////////////

// Last update: June 20, 2001

class GSEMemorySmallObjectAllocator
{
public:
    GSEMemorySmallObjectAllocator();

    void* Allocate(size_t nNumBytes) throw (GSEException);
    void  Deallocate(void* p, size_t nSize);

    GSEMemoryFixedAllocator* GetGSEMemFixedAllocatorForSize(size_t nNumBytes);

private:
    GSEMemorySmallObjectAllocator(const GSEMemorySmallObjectAllocator&){};
    GSEMemorySmallObjectAllocator& operator=(const GSEMemorySmallObjectAllocator&)
    {
        return *(this);
    }
    
protected:
    GSEMemoryFixedAllocator m_kPool[MAX_SMALL_OBJECT_SIZE];
};
#endif
