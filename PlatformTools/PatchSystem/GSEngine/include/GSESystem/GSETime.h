﻿/**
* @file	    GSETime.h 
* @brief    Time Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSETime.h
* 摘    要: 时间接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.28
* 修改记录: 
*/

#ifndef __GSETime__H__
#define __GSETime__H__
#include "GSESystem.h"

class GSESYSTEMENTRY_CLASS GSETime
{
friend class GSEErrorHandling;
public:
    ~GSETime(void){}

public:
    const char* GetTime();

protected:
    GSETime(void){}
    GSETime(const GSETime&){}
    GSETime& operator=(const GSETime&){return *this;}

protected:
    char    m_szTime[20];
};
#endif
