﻿/**
* @file	    GSEErrorHandling.h 
* @brief    Error Handling Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEErrorHandling.h
* 摘    要: 错误处理接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.3.19
* 修改记录: 
*/

#ifndef __GSEErrorHandling__H__
#define __GSEErrorHandling__H__
#include "GSESystem.h"
#include "GSEException.h"
#include "GSEMemoryObject.h"
#include "GSEFlags.h"
#include "GSETime.h"
#include "GSETracker.h"

//错误处理标记
enum
{
    GSEERROR_SCREEN_MASK = 0X0001,
    GSEERROR_SCREEN_POS  = 0,
    GSEERROR_FILE_MASK   = 0X0002,
    GSEERROR_FILE_POS    = 1,
    GSEERROR_THROW_MASK  = 0X0004,
    GSEERROR_THROW_POS   = 2,
    GSEERROR_TRACK_MASK  = 0X0008,
    GSEERROR_TRACK_POS   = 3,
    GSEERROR_ALL_MASK    = 0X000F,
};

class GSESYSTEMENTRY_CLASS GSEErrorHandling : public GSEMemoryObject
{
template<typename> friend class GSESingleton;
public:
    ~GSEErrorHandling(void){}

protected:
    static GSEErrorHandling* Instance() throw(GSEException);

public:
    void  SetErrorHandlingFlags(short nFlags);
    float GetVersion()
    {
        return m_fVersion;
    }
    void  ErrorHandling(const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction, const char* pszFormat, ...) throw(GSEException);
    void  ErrorHandlingFlush();
    void  Tracker(const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction);
    void  DeTracker();
    const char* GetTime();

protected:
    GSEErrorHandling(void);
    GSEErrorHandling(const GSEErrorHandling&) {}
    GSEErrorHandling& operator=(const GSEErrorHandling&) { return *this; }

protected:
    float                    m_fVersion;
    GSEFlags<short>          m_kFlags;
    GSETracker               m_kStackTracker;
    GSETime                  m_kTime;
    static GSEErrorHandling* ms_pkGSEErrorHandling;
};

#endif
