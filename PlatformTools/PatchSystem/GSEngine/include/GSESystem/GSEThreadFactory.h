﻿/**
* @file	    GSEThreadFactory.h 
* @brief    Thread Factory Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEThreadFactory.h
* 摘    要: 线程工厂接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.14
* 修改记录: 
*/

#ifndef __GSEThreadFactory__H__
#define __GSEThreadFactory__H__
#include "GSESystem.h"
#include "GSEMemoryObject.h"

class GSEThreadProc;
class GSEThread;
class GSESYSTEMENTRY_CLASS GSEThreadFactory : public GSEMemoryObject
{
template<typename> friend class GSESingleton;
public:
    virtual ~GSEThreadFactory(void) throw(GSEException) {}

protected:
    static GSEThreadFactory* Instance() throw(GSEException);

public:
    virtual GSEThread* CreateThread(GSEThreadProc* pkThreadProc, bool bDellocatedInterlly) throw(GSEException);
    virtual void DestroyThread(GSEThread* pkThread) throw(GSEException);

protected:
    GSEThreadFactory(void) {}
    GSEThreadFactory(const GSEThreadFactory&)  {}
    GSEThreadFactory& operator=(const GSEThreadFactory&) { return *this; }

protected:
    static GSEThreadFactory*  ms_pkGSEThreadFactory;
};

#endif
