﻿/**
* @file	    GSESemaphore.h 
* @brief    Semaphore Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSESemaphore.h
* 摘    要: 信号量接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.14
* 修改记录: 
*/

#ifndef __GSESemaphore__H__
#define __GSESemaphore__H__
#include "GSESystem.h"
#include "GSEMemoryObject.h"

class GSESYSTEMENTRY_CLASS GSESemaphore : public GSEMemoryObject
{
public:
    GSESemaphore(void) throw(GSEException);
    virtual ~GSESemaphore(void) throw(GSEException);

public:
    virtual bool Wait(DWORD dwTime = INFINITE) throw(GSEException);
    virtual void Release() throw(GSEException);

protected:
#ifdef WIN32
    HANDLE m_hSemaphore;
#else
    sem_t  m_hSemaphore;
#endif
};

#endif
