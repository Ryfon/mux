﻿/**
* @file	    GSESpinLock.h 
* @brief    Spin Lock Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSESpinLock.h
* 摘    要: 自旋锁接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.13
* 修改记录: 
*/

#ifndef __GSESYSTEMENTRY_CLASS__H__
#define __GSESYSTEMENTRY_CLASS__H__
#include "GSELockInterface.h"

class GSESYSTEMENTRY_CLASS GSESpinLock : public GSELockInterface
{
public:
    GSESpinLock(void) throw(GSEException);
    virtual ~GSESpinLock(void) throw(GSEException);

public:
    virtual void Lock() throw(GSEException);
    virtual void Unlock() throw(GSEException);

protected:
#ifdef WIN32
    LONG               m_nCurrentThreadID;
#else
    pthread_spinlock_t m_kSpinlock;
#endif
};

#endif
