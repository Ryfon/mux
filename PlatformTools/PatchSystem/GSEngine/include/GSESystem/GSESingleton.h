﻿/**
* @file	    GSESingleton.h 
* @brief    Singleton Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSESingleton.h
* 摘    要: 单体接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.14
* 修改记录: 
*/

#ifndef __GSESingleton__H__
#define __GSESingleton__H__
#include "GSESystem.h"
#include "GSEMemoryObject.h"

template<typename Type> class GSESYSTEMENTRY_CLASS GSESingleton : public GSEMemoryObject
{
public:
    ~GSESingleton(void){}
     static Type* Instance() throw(GSEException);

protected:
    GSESingleton(void) {}
    GSESingleton(const GSESingleton<Type>& ) {}
    GSESingleton<Type>& operator=(const GSESingleton<Type>& ) { return *this; }
};

#endif
