﻿/**
* @file	    GSEFlags.h 
* @brief    Flags Bit Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEFlags.h
* 摘    要: 标志位接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.3.19
* 修改记录:
*/

#ifndef __GSEFlags__H__
#define __GSEFlags__H__
#include "GSESystem.h"

template <class type> class GSESYSTEMENTRY_CLASS GSEFlags
{
public:
    GSEFlags(void)
    {
        m_nFlag = 0;
    }
    ~GSEFlags(void){}

public:
    void SetField(type nValue, type nMask, type nPos)
    {
        m_nFlag = (m_nFlag & ~nMask) | (nValue << nPos);
    }

    type GetField(type nMask, type nPos)
    {
        return (m_nFlag & nMask) >> nPos;
    }

    void SetBit(bool bVal, type nMask)
    {
        if(bVal)
        {
            m_nFlag |= nMask;
        }
        else
        {
            m_nFlag &= ~nMask;
        }
    }

    bool GetBit(type nMask)
    {
        return (m_nFlag & nMask) != 0;
    }

protected:
    type    m_nFlag;
};
#endif
