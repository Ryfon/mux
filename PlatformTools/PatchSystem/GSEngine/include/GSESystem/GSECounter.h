﻿/**
* @file	    GSECounter.h 
* @brief    Atomic Counting Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSECounter.h
* 摘    要: 原子计数接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.08.13
* 修改记录: 
*/

#ifndef __GSECounter__H__
#define __GSECounter__H__
#include "GSESpinLock.h"

class GSESYSTEMENTRY_CLASS GSECounter
{
public:
    GSECounter(int nInitCount = 0) throw(GSEException);
    virtual ~GSECounter(void) throw(GSEException) {}

public:
    virtual LONG Get() throw(GSEException);
    virtual bool Test(int nTest) throw(GSEException);
    virtual void Set(int nValue) throw(GSEException);
    virtual bool TestAndSet(int nTest, int nSet) throw(GSEException);
    virtual void Increment(int nInc = 1) throw(GSEException);
    virtual void Decrement(int nDec = 1) throw(GSEException);

protected:
    LONG        m_nCount;
    GSESpinLock m_kSpinlock;
};

#endif
