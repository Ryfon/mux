﻿/**
* @file	    GSESystem.h 
* @brief    System Common Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSESystem.h
* 摘    要: 通用接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.30
* 修改记录:
*/

#ifndef __GSESystem__H__
#define __GSESystem__H__
#include <memory>
#include <ctime>
#include <cassert>
#include <exception>
#include <cstdarg>

#define MAXFILENAMELEN                 64
#define MAXFUNCTIONNAMELEN             64
#define MAXMSGLEN                      256
#define MAXRETHROW                     6

#ifndef WIN32
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
typedef void*                          LPVOID;
typedef unsigned long                  DWORD;
typedef	pthread_t                      HANDLE;
typedef pthread_mutex_t                CRITICAL_SECTION;
typedef long                           LONG;
#define INVALID_HANDLE	               0
#define INFINITE                       0XFFFFFFFF
#define	GSESYSTEMENTRY_CLASS
#define	GSESYSTEMENTRY_FUNCTION
#else
#include <windows.h>
#pragma warning(disable : 4290) 
#define INVALID_HANDLE                 NULL
#define SEM_VALUE_MAX                  (2147483647)
#define	GSESYSTEMENTRY_CLASS           _declspec(dllexport) 
#define	GSESYSTEMENTRY_FUNCTION        extern "C" _declspec(dllexport) 
#endif

#define GSEINIT_SYSTEM(ErrorFlags)     GSESystemInitOptions g_GSESystemInitOptions;

#define	GSEErrHandling(...)            GSEErrorHandling::Instance()->ErrorHandling(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__);
#define	GSEFlushErrHandling()          GSEErrorHandling::Instance()->ErrorHandlingFlush();
#define	GSEStackTracker()              GSEErrorHandling::Instance()->Tracker(__FILE__, __LINE__, __FUNCTION__);
#define	GSEStackDeTracker()            GSEErrorHandling::Instance()->DeTracker();
#define	GSEGetTime()                   GSEErrorHandling::Instance()->GetTime();

#define GSEEXCEPTION(...)              GSEException(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__);
#define GSEAPPENDEX(exp, ...)          exp.Append(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__);

#define GSECreateThread(proc, bDel)    GSESingleton<GSEThreadFactory>::Instance()->CreateThread(proc, bDel);
#define GSEDestroyThread(thread)       GSESingleton<GSEThreadFactory>::Instance()->DestroyThread(thread);  

GSESYSTEMENTRY_FUNCTION const char*    GSEGetFileName(const char* pszFullName);
GSESYSTEMENTRY_FUNCTION void           GSEMemcpy(void* pDesBuf, size_t nDesLen, const void* pSrcBuf, size_t nSrcLen, bool bAppedZero = false);
GSESYSTEMENTRY_FUNCTION void           GSEStrcpy(char* pszDes, size_t nDesLen, const char* pszSrc);
GSESYSTEMENTRY_FUNCTION void           GSESleep(int nMicroSeconds);
#endif
