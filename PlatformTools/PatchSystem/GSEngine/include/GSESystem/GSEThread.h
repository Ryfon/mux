﻿/**
* @file	    GSEThread.h 
* @brief    Thread Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEThread.h
* 摘    要: 线程接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.3.16
* 修改记录: 
*/

#ifndef __GSEThread__H__
#define __GSEThread__H__
#include "GSESystem.h"
#include "GSEException.h"
#include "GSEGuardObj.h"
#include "GSECounter.h"

class GSEThreadProc;
class GSESYSTEMENTRY_CLASS GSEThread : public GSEGuardObj
{
friend class GSEThreadFactory;
protected:
    GSEThread(void);
    virtual ~GSEThread(void);

public:
    virtual HANDLE GetHandle();

protected:
    virtual void Create(GSEThreadProc* pkThreadProc, bool bDellocatedInterlly) throw(GSEException);
    virtual void Destroy(void) throw(GSEException);

protected:
    virtual void DoSomething() throw(GSEException);
    virtual void UndoSomething() throw(GSEException);
    virtual void KillThread() throw(GSEException);

protected:
#ifdef WIN32
    static DWORD  WINAPI ThreadProc(LPVOID pParam) throw(GSEException);
#else
    static LPVOID ThreadProc(LPVOID pParam) throw(GSEException);
#endif

protected:
    HANDLE         m_hThreadHandle;
    GSEThreadProc* m_pkThreadProc;
    bool           m_bDellocatedInterlly;
    GSECounter     m_kCounter;
};
#endif
