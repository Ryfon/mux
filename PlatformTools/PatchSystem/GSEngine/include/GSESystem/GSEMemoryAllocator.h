﻿/**
* @file	    GSEMemoryAllocator.h 
* @brief    Memory Manager Abstract Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEMemoryAllocator.h
* 摘    要: 内存管理抽象接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.28
* 修改记录: 
*/

#ifndef __GSEMemoryAllocator__H__
#define __GSEMemoryAllocator__H__
#include "GSEMemory.h"

class GSESYSTEMENTRY_CLASS GSEMemoryAllocator
{
public:
    GSEMemoryAllocator() throw(GSEException) {}
    virtual ~GSEMemoryAllocator(){};

public:
    virtual void* Allocate(size_t nSizeInBytes, const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException) = 0;
    virtual void* Reallocate(void* pvMemory, size_t nSizeInBytes, const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException) = 0;
    virtual void  Deallocate(void* pvMemory, size_t nSizeInBytes) = 0;
};

#endif
