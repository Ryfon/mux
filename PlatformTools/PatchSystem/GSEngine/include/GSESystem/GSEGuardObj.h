﻿/**
* @file	    GSEGuradObj.h 
* @brief    Guard Object Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEGuradObj.h
* 摘    要: 守卫对象接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.5.19
* 修改记录:
*/

#ifndef __GSEGuradObj__H__
#define __GSEGuradObj__H__
#include "GSESystem.h"
#include "GSEException.h"
#include "GSEMemoryObject.h"

class GSESYSTEMENTRY_CLASS GSEGuardObj : public GSEMemoryObject
{
friend class GSEGuard;
public:
	GSEGuardObj(void){}
    virtual ~GSEGuardObj(void){}

protected:
    virtual void DoSomething() throw(GSEException) = 0;
    virtual void UndoSomething() throw(GSEException) = 0;
};
#endif
