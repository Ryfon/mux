﻿/**
* @file	    GSEMemoryObject.h 
* @brief    Overload new&dlete Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEMemoryObject.h
* 摘    要: 重载new&Delete接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.29
* 修改记录: 
*/

#ifndef __GSEMemoryObject__H__
#define __GSEMemoryObject__H__
#include "GSEMemory.h"

class GSESYSTEMENTRY_CLASS GSEMemoryObject
{
private:
    static void* operator new(size_t nSizeInBytes);
    static void* operator new[](size_t nSizeInBytes);

public:
    static void *operator new(size_t nSize, const char *pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);
    static void *operator new[](size_t nSize, const char *pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);

public:
    static void operator delete(void *pvMem, const char *pszSourceFile, size_t nSourceLine, const char* pszSourceFunction){}
    static void operator delete[](void *pvMem, const char *pszSourceFile, size_t nSourceLine, const char* pszSourceFunction){}

public:
    static void operator delete(void *pvMem, size_t nSizeInBytes);
    static void operator delete[](void *pvMem, size_t nSizeInBytes);

public:
    static void* operator new(size_t nSize, void* p)
    { 
        return p; 
    }
    static void* operator new[](size_t nSize, void* p)
    {
        return p; 
    }
    static void operator delete(void *, void*){}
    static void operator delete[](void *, void*){}
};
#endif
