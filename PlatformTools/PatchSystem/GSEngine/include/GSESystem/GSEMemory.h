﻿/**
* @file	    GSEMemory.h 
* @brief    Memory Marco Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEMemory.h
* 摘    要: 内存宏接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.4.28
* 修改记录: 
*/

#ifndef __GSEMemory__H__
#define __GSEMemory__H__
#include "GSESystem.h"
#include "GSEException.h"

#define    GSEMEMPREFIX_SIZE          sizeof(size_t)

//---------------------------------------------------------------------------
//分配内存的函数和宏
//GSEMemNew和GSEMemDelete仅被用于GSEMemObject的子类分配和释放.调用
//构造和析构函数,并且在其中缓存了编译时的文件、代码行、函数等.
//GSEMemMalloc、GSEMemRealloc、GSEMemAlloc、GSEMemFree等被用于
//非GSEMemObject子类、内建类型的分配和释放等. 不调用构造和析构函数
//---------------------------------------------------------------------------
#define GSEMemNew                     new(__FILE__, __LINE__, __FUNCTION__)
#define GSEMemDelete                  delete
#define GSEMemMalloc(size)            _GSEMemMalloc(size, __FILE__, __LINE__, __FUNCTION__) 
#define GSEMemRealloc(memblock, size) _GSEMemRealloc(memblock, size, __FILE__, __LINE__, __FUNCTION__) 
#define GSEMemAlloc(T, count)         (T*)_GSEMemMalloc(sizeof(T)*(count), __FILE__, __LINE__, __FUNCTION__)
#define GSEMemFree(p)                 _GSEMemFree(p)
#define GSEMemExternalNew             new
#define GSEMemExternalDelete          delete
#define GSEMemExternalMalloc          malloc
#define GSEMemExternalRealloc         realloc
#define GSEMemExternalAlloc(T, count) ((T*)malloc(sizeof(T)*(count)))
#define GSEMemExternalFree            free

GSESYSTEMENTRY_FUNCTION void*         _GSEMemMalloc(size_t nSizeInBytes, const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);
GSESYSTEMENTRY_FUNCTION void*         _GSEMemRealloc(void *memblock, size_t nSizeInBytes, const char* pszSourceFile, size_t nSourceLine, const char* pszSourceFunction) throw(GSEException);
GSESYSTEMENTRY_FUNCTION void          _GSEMemFree(void* pvMemory);
#endif
