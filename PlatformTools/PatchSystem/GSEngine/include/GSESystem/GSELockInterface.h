﻿/**
* @file	    GSEMLockInterface.h 
* @brief    Abstract Lock Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSELockInterface.h
* 摘    要: 抽象锁锁接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.3.21
* 修改记录: 
*/

#ifndef __GSELockInterface__H__
#define __GSELockInterface__H__
#include "GSESystem.h"
#include "GSEException.h"
#include "GSEGuardObj.h"

class GSESYSTEMENTRY_CLASS GSELockInterface : public GSEGuardObj
{
public:
    GSELockInterface(void){}
    virtual ~GSELockInterface(void){}

public:
    virtual void Lock() throw(GSEException) = 0;
    virtual void Unlock() throw(GSEException) = 0;

protected:
    virtual void DoSomething() throw(GSEException)
    {
        Lock();
    }
    virtual void UndoSomething() throw(GSEException)
    {
        Unlock();
    }
};
#endif
