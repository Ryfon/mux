﻿/**
* @file	    GSEPoolArray.h 
* @brief    Array Object Pool Interface Declaration
* Copyright(c) 2009, Xuchengyong Private
* All rights reserved
* 文件名称: GSEPoolArray.h
* 摘    要: 数组对象池接口声明
* 作	者: 徐成勇
* 版	本: Ver 0.1
* 完成时间: 2009.6.2
* 修改记录: 
*/

#ifndef __GSEPoolArray__H__
#define __GSEPoolArray__H__
#include "GSEPool.h"

template<typename Value> class GSESYSTEMENTRY_CLASS GSEPoolArray : public GSEPool<Value, Value>
{
public:
    GSEPoolArray(size_t nMaxSize = DEFAULTPOOLSIZE, size_t nIncSize = DEFAULTINCSIZE) throw(GSEException);
    virtual ~GSEPoolArray(void) throw(GSEException);

public:
    virtual void  First();
    virtual void  Next();
    virtual bool  IsDone() const;
    virtual Value GetCurrent(Value& kValue) throw(GSEException);
	virtual Value EraseCurrent(Value& kValue) throw(GSEException);

public:
    virtual Value Find(Value& kValue);
    virtual void  Append(Value kKey, Value kValue = 0) throw(GSEException);
    virtual Value Erase(Value kValue);
	virtual void  EraseAll();

private:
    GSEPoolArray(const GSEPoolArray&);
    GSEPoolArray& operator=(const GSEPoolArray&);

protected:
    Value*  m_pkValues;
	size_t  m_nSlotIndex;
};

#endif
