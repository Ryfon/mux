﻿#ifndef PATCHSESSION_H
#define PATCHSESSION_H
#include "..\Common\Common.h"
#include "GSECommunicationApplicationCallback.h"
#include "GSECommunication.h"

class CPatchClient;
class CPatchSession: public GSECommunicationApplicationCallback
{
public:
	CPatchSession(void);
	virtual ~CPatchSession(void);
public:
	virtual void Terminate();
	virtual bool Continue();
	virtual void OnCreated(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication);
	virtual void OnDestroy(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication);
	virtual void OnRecv(GSECommunication* pkCommunication, char* pszData, int nLen);
	virtual void OnSend(GSECommunication* pkCommunication, char* pszData, int nLen);
	virtual void OnError(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication, int nErrCode);    
	virtual void OnClose(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication);
	virtual void OnTimedout(GSECommunicationApplication* pkApplication);
	virtual void GetWaitTime(struct timeval& kTimeValue);
	virtual int  GetWaitTime(){ return 0; }

	virtual void GetRecvBuf(GSECommunication* pkCommunication, char*& pszBuf, int& nLen);
	virtual void GetSendBuf(GSECommunication* pkCommunication, char*& pszBuf, int& nLen);

	virtual bool GetRecvMask(GSECommunication* pkCommunication);
	virtual bool GetSendMask(GSECommunication* pkCommunication);

public:
	void SetOwner(CPatchClient* Owner);
	void Quit();
	void SetFlag()
	{
		m_Quit = false;
	}
private:
	CPatchClient* m_Owner;
	bool volatile m_Quit;
};

#endif











