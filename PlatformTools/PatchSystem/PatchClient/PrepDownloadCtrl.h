﻿#ifndef PREPDOWNLOADCTRL_H
#define PREPDOWNLOADCTRL_H
#include "../Common/Common.h"
#include <Pdh.h>
#include "PatchCliIntf.h"
#include "GSEThreadProc.h"
#include "GSEThread.h"
#include "GSESingleton.h"
#include "GSEThreadFactory.h"
#include "GSELock.h"

typedef pair<CONTROL_MASK, int> CONTROL_PAIR;
typedef pair<CONTROL_MASK, PDH_HCOUNTER> CONTROL_COUNTER;
class CPatchClient;
class CPrepDownloadCtrl: public GSEThreadProc
{
public:
	CPrepDownloadCtrl(CPatchClient& PatchClient);
	virtual ~CPrepDownloadCtrl(void);
public:
	int Start();
	int Stop();
public:
	int AddCtrl(CONTROL_MASK mask, int nMaskValue);
	int RemoveCtrl(CONTROL_MASK mask);
public:
	virtual void ThreadProc(void) throw(GSEException);	
	virtual void Terminate() throw(GSEException);
private:
	bool ProcMask(int nIndex);
	PDH_HCOUNTER AddControlCounter(CONTROL_MASK mask);
	string GetNetworkIntf();
	bool IsInCtrl();
private:
	CPatchClient& m_PatchClient;
	vector<CONTROL_PAIR> m_Controls;
	vector<CONTROL_COUNTER> m_Counters;
	GSEThread* m_Thread;
	bool m_Quit;
	GSELock m_Lock;
	uint32 m_IdleTime;
	string m_NetworkIntf; //网卡描述
	uint32 m_CurBandWidth;
private:
	PDH_HQUERY m_hQuery; //pdh 查询句柄
};

#endif







