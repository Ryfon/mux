﻿#include "Pinger.h"

CPinger::CPinger(void)
{
	//winsock库已在底层加载，此处不要再加载winsock库
	m_hIcmpModule = LoadLibrary("ICMP.DLL");
	if (m_hIcmpModule == NULL)
	{
		return;
	}
	m_IcmpCreateFile = (PICMPCREATEFILE)GetProcAddress((HMODULE)m_hIcmpModule, "IcmpCreateFile");
	m_IcmpSendEcho = (PICMPSENDECHO)GetProcAddress((HMODULE)m_hIcmpModule, "IcmpSendEcho");
	m_IcmpCloseHandle = (PICMPCLOSEHANDLE)GetProcAddress((HMODULE)m_hIcmpModule, "IcmpCloseHandle");
	if (m_IcmpCreateFile == NULL || m_IcmpCloseHandle == NULL || m_IcmpSendEcho == NULL)
	{
		return;
	}
	m_hImcpFile = (m_IcmpCreateFile)();
}

CPinger::~CPinger(void)
{
	if (m_hImcpFile != INVALID_HANDLE_VALUE)
	{
		(m_IcmpCloseHandle)(m_hImcpFile);
	}
	if (m_hIcmpModule)
	{
		FreeLibrary((HMODULE)m_hIcmpModule);
	}
}

PingStatus CPinger::Ping(const char *szIp, uint32 ttl)
{
	PingStatus ps;
	memset(&ps, 0, sizeof(ps));

	IN_ADDR DestAddr;
	DestAddr.s_addr = inet_addr(szIp);
	IPINFO ipInfo;
	ipInfo.Ttl = ttl;
	ipInfo.Tos = 0;
	ipInfo.Flags = 0;
	ipInfo.OptionsSize = 0;
	ipInfo.OptionsData = NULL;

	char szBuf[sizeof(ICMPECHO) + 8192];
	LARGE_INTEGER PerfFreq;
	QueryPerformanceFrequency(&PerfFreq);
	LARGE_INTEGER liSTime;
	QueryPerformanceCounter(&liSTime);
	DWORD dwReplyCount = (m_IcmpSendEcho)(m_hImcpFile, DestAddr.s_addr, NULL, 0, &ipInfo, szBuf, sizeof(szBuf), 1000);
	LARGE_INTEGER liETime;
	QueryPerformanceCounter(&liETime);
	if (dwReplyCount != 0)
	{
		long PingTime = *(u_long *) &(szBuf[8]);
		ps.destinationAddress = *(u_long *)szBuf;
		ps.success = true;
		ps.status = *(DWORD *) &(szBuf[4]);
		ps.delay = (float)(liETime.QuadPart - liSTime.QuadPart) * 1000i64 / (float)PerfFreq.QuadPart;
		ps.ttl = (ps.status != 0) ? ttl : ((*(char *)&(szBuf[20])) &0x00FF);
	}
	else
	{
		ps.success = false;
		ps.error = GetLastError();
	}
	return ps;
}

