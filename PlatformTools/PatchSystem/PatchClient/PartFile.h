﻿#ifndef PARTFILE_H
#define PARTFILE_H
#include "..\Common\Common.h"
#include "..\Common\PatchFile.h"
#include "GSEThreadProc.h"
#include "GSEThread.h"
#include "GSESingleton.h"
#include "GSELock.h"
#include "GSESemaphore.h"

#define MAX_HASHVALUE_LEN 16
#pragma pack(push, 1)
typedef struct _tagFileHeader 
{
	uint8 MajorVersion;
	uint8 MinVersion;
	uint16 AddVersion;
	uint32 PartCount;
	uint32 nFileSize;
	uint8 FileHash[MAX_HASHVALUE_LEN];
} FILEHEADER, *PFILEHEADER;

typedef struct _tagGap 
{
	uint32 StartPos;
	uint32 EndPos;
} GAP, *PGAP;

#pragma pack(pop)

typedef struct _tagIOData
{
	uint32 nStartPos;
	uint32 nDataLen;
	char* pData;

}IODATA, *PIODATA;

class CPatchClient;
class CPartFile: public GSEThreadProc
{
public:
	CPartFile(CPatchVersion patchVersion, CPatchClient& patchClient);
	virtual ~CPartFile(void);
public:
	int InitDownload();
	int SetPatchInfo(CPatchVersion patchVersion, uint32 nFileSize, uint8* FileHash);
	int AddPartHash(uint8* PartHash);
	int OnPartComplete();
	int OnComplete();
	uint32 GetFileSize() const
	{
		return m_FileSize;
	}
	uint32 GetTransferSize() const
	{
		return m_TranferSize;
	}

	void GetDownloadPos(uint32& nStartPos, uint32& nEndPos);
	
	CPatchVersion GetDownloadVerson() const
	{
		return m_DownloadVersion;
	}

	int UpdateDownload(uint32 nStartPos, uint32 nDataLen, const char* pData);
	bool IsSaved() const
	{
		return m_IsSaved;
	}

private:
	int SavePartFile(uint32 nStartPos, uint32 nDataLen, const char* pData);
	int WriteBufToDisk(uint32 nPartIndex);
	int LoadFromFile();
	int CheckFile();
private:
	virtual void ThreadProc() throw(GSEException);
	virtual void Terminate() throw(GSEException);
private:
	CPatchVersion m_DownloadVersion; //版本号
	CPatchClient& m_PatchClient;
	uint8 m_FileHash[MAX_HASHVALUE_LEN]; //文件的MD5值，因为MD5的长度固定，因此不用再定义长度了
	uint32 m_FileSize; //文件大小
	uint32 m_TranferSize; //已下载文件大小
	string m_PatchFileName; //文件名
	string m_PartFileName; //文件名
	vector<string> m_PartHashs; //每一块的Hash值
	vector<GAP> m_DownloadGaps;
	uint32 m_LastTime;
	uint32 m_DownloadSpeed; //下载速度
	uint32 m_UploadSpeed; //上传速度
	bool m_IsSaved;
	vector<IODATA> m_DowndloadData;
	GSELock m_DataLock;
	GSEThread* m_Thread;
	char m_PartData[PARTSIZE];
	//坏块列表
	vector<GAP> m_BadPartList;
};

#endif
