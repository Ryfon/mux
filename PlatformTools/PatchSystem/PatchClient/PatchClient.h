﻿#ifndef PATCHCLIENT_H 
#define PATCHCLIENT_H
#include "PatchCliIntf.h"
#include "..\Common\Common.h"
#include "..\Common\PatchFile.h"
#include "..\Common\PatchFileKeys.h"
#include "..\PatchProtocol\PatchProtocol.h"
#include "PartFile.h"
#include "PatchSession.h"
#include "GSECBlockingTCPClient.h"
#include "GSEThreadProc.h"
#include "GSEThread.h"
#include "GSESingleton.h"
#include "GSECommunicationFactory.h"
#include "GSEThreadFactory.h"
#include "GSESystemInitOptions.h"
#include "GSECommunicationInitOptions.h"
#include "..\PatchServer\CircularBuf.h"
#include "Pinger.h"
#include "PrepDownloadCtrl.h"

#define MAX_NET_BUF 40960

class CPatchClient;

struct MSGHANDLER 
{
	typedef int (CPatchClient::*HANDLER)(char* pData, unsigned short usDataLen);
	MSGHANDLER(): handler(0){}
	MSGHANDLER(HANDLER _handler): handler(_handler){}
	~MSGHANDLER(){}
	HANDLER handler;
};

#define DealPlayerPkgPre( OUTTYPE, outPtr, inputPkg, inputLen, ProtolTool ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	memset( outPtr, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)ProtolTool.DeCode(OUTTYPE::wCmd, &tmpStruct, outBufLen, (char*)inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
{\
	return false;\
}

typedef PatchServer::MsgHeader MSGHEADER;
typedef PatchServer::MsgHeader* PMSGHEADER;



enum PATCHCLIENT_STATE {INVALID_STATE, CONNECTED_STATE, REQLUANCHERVERSION_STATE, DOWNLOADLUANCHERUPDATE_STATE, NORMAL_STATE};

class CBtClient;

class CPatchClient: public CPatchCliIntf, public GSEThreadProc, public CDirectDownloadIntf
{
	typedef std::map<unsigned short, MSGHANDLER> HANDLERMAP;
public:
	explicit CPatchClient(CPatchCliEvent* pPatchCliEvent);
	explicit CPatchClient(CDownloadEvent* pDownloadEvent);
	virtual ~CPatchClient();
public:
	virtual int AddConstrlMask(CONTROL_MASK mask, int nValue);
	virtual int RemoveControlMask(CONTROL_MASK mask);
	virtual int GetServerInfo();
	virtual PCATINFO GetCatInfo(unsigned char& ucNum);
	virtual PAREAINFO GetAreaInfo(unsigned int nCatID, unsigned char& ucNum);
	virtual PSERVERINFO GetRegionInfo(unsigned int nAreaID, unsigned char& ucNum);
	virtual void GetRecommendServer(unsigned int& nCatID, unsigned int& nAreaID, unsigned int& nServerID);
	virtual int StartUp(const char* szIp, unsigned short usPort, bool bPrep = false);
	virtual int CleanUp();
	virtual int ControlPreDownload(bool bPause);
	virtual int CheckVersion();
	CPatchCliEvent* GetClientEvent()
	{
		return m_PatchCliEvent;
	}
public:
	virtual void ThreadProc(void) throw(GSEException);	
	virtual void Terminate() throw(GSEException);
public:
	virtual int Init(const char* szIp, unsigned short usPort);
	virtual int Finit();
	virtual int QueryCliHashFileInfo();
	virtual int QueryFileSizeByName(const char* szFileName);
	virtual int DownloadFile(const char* szFileName, unsigned int nOffset, unsigned int nFileSize);
public:
	int OnPatchConnected();
	int OnPatchClose();
	int OnTimeOut();
	int OnError(int nErrCode);
	int OnRecv(char* pszData, int nLen);
	int OnSend(char* pszData, int nLen);
	int OnInitSend(char*& pszBuf, int& nLen);
	int OnInitRecv(char*& pszBuf, int& nLen);
	bool OnReadMask();
	bool OnWriteMask();
public:
	void OnControl(bool bInControl);
public:
	//版本验证请求
	int ReqServerVersion();
	//
	int ReqPatchFileInfo(CPatchVersion patchVersion);

	int RegisterHandler();

	bool IsPatchExist(const char* szFileName);
	//调用安装进行安装
	int UpdateVersion();
	int StartDownload();
	int Download();
	int DownloadFile();

	int ReqLauncherVersion(int nType);

	int HeatBeat();

	int ReqBtServerInfo();

	int ReqTorrentInfo(CPatchVersion patchVersion);
private:
	int ProcData(char* pData);
	int ProcServerVersion(char* pData, unsigned short usDataLen);
	int ProcPatchFileInfoResp(char* pData, unsigned short usDataLen);
	int ProcDownloadData(char* pData, unsigned short usDataLen);
	int ProcError(char* pData, unsigned short usDataLen);
	int ProcDownloadFile(char* pData, unsigned short usDataLen);
	int ProcLauncherVersionInfo(char* pData, unsigned short usDataLen);
	int ProcBtServerInfo(char* pData, unsigned short usDataLen);
	int ProcTorrentInfo(char* pData, unsigned short usDataLen);
	int ProcCliVerifyInfo(char* pData, unsigned short usDataLen);
	int ProcFileInfoResp(char* pData, unsigned short usDataLen);
	int VerifyHash(int nIndex);
	int ParseData();
	int ProcUpdateMsg(int nIndex, CPatchVersion patchVersion, const char* szPatchFileName);
	int VerifyLuancherVersion(const char* szFileName);
	void SetCurPorcessID();
private:
	CDownloadEvent* m_DownloadEvent;
	HANDLERMAP m_HandlerMap;
	PatchServer::PatchSrvProtocol m_Protocol;
	PATCHVERSION m_CliVersion;
	PATCHVERSION m_SvrVersion;
	vector<CPatchVersion> m_PatchList;
	vector<string> m_PatchHash;
	vector<CPatchVersion> m_DownloadQue;
	vector<CPatchVersion> m_UpdateQue;
	CPartFile* m_CurPartFile;
	CPatchSession m_PatchSession;
	GSECommunicationApplicationClient* m_TcpClient;
	GSEThread* m_Thread;
	bool m_PatchConnected;
	CCircularBuf* m_InBuf;
	CCircularBuf* m_OutBuf;
	list<char*> m_SendQue;
	DWORD m_LauncherProcessID;
	bool m_bPrep;
	CPinger m_Pinger;
	string m_PatchIp;
	unsigned short m_PatchPort;
	CPrepDownloadCtrl* m_PrepDownloadCtrl;
	bool m_CanDoPrepDownload;
	bool m_IsInit;
	PATCHCLIENT_STATE m_ClientState;
	time_t m_LastSendTime;
	bool m_InitiativeBroken;
	bool m_NeedUpateLauncher;
private:
	CPatchCliEvent* m_PatchCliEvent;
	struct _tagDownloadFile 
	{
		string strFileName;
		uint32 nOffset1;
		uint32 nOffset2;
		uint32 nFileSize;
		char* pFileData;
	};
	_tagDownloadFile m_CurDownloadFile;

private:	
	CBtClient* m_pBtClient;
	int m_TaskID;
	DWORD m_LastBtTime;
	int64 m_BtFileSize;
};


#endif






