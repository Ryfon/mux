﻿#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include "..\Common\Common.h"
#include "PatchCliIntf.h"

#define PATCHDIR "Download"
class CConfiguration
{
public:
	CConfiguration(void);
	virtual ~CConfiguration(void);
public:
	int LoadServerConfig();
private:
	int LoadConfig();
public:
	string GetPatchDir();
public:
	PATCHVERSION m_CliVersion;
	string m_HttpAddr;
	vector<CATINFO> m_CatInfos;
	vector<AREAINFO> m_AreasInfo;
	vector<SERVERINFO> m_ServerInfos;
	uint32 m_RecommendCatID;
	uint32 m_RecommendAreaID;
	uint32 m_RecommendServerID;
};

extern CConfiguration g_Configuration;
#endif