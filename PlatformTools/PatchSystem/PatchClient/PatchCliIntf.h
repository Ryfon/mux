﻿#ifndef PATCHCLIINTF_H
#define PATCHCLIINTF_H

#ifdef PATCHCLIENT_EXPORTS
#define PATCH_DLL_API __declspec(dllexport)
#else
#define PATCH_DLL_API __declspec(dllimport)
#endif

//安装常量定义
#define WM_MUXPATCHUPDATE	WM_APP + 1									// 安装进度消息 [8/24/2009 hemeng]

#define	MUXPATCH_UPDATE_INIT			WM_MUXPATCHUPDATE + 1			// Patch初始化成功 [8/24/2009 hemeng]
#define	MUXPATCH_UPDATE_PATCHING		WM_MUXPATCHUPDATE + 2			// Patch成功 [8/24/2009 hemeng]
#define MUXPATCH_UPDATE_FINISHING		WM_MUXPATCHUPDATE + 3			// 完成patch,更新配置文件 [8/24/2009 hemeng]
#define MUXPATCH_FINISH					WM_MUXPATCHUPDATE + 4			// 完成 [8/24/2009 hemeng]

#define MUXPATCH_ERROR_INIT				WM_MUXPATCHUPDATE + 1000		// 初始化失败 [8/24/2009 hemeng]
#define MUXPATCH_ERROR_CONTENTVERSION	WM_MUXPATCHUPDATE + 1001		// 错误的内容版本（与当前更新包版本不符）  [8/24/2009 hemeng]
#define MUXPATCH_ERROR_UPDATEFAIL		WM_MUXPATCHUPDATE + 1002		// 更新失败 [8/24/2009 hemeng]
#define MUXPATCH_ERROR_FINISHING		WM_MUXPATCHUPDATE + 1003		// 完成更新是出错 [8/24/2009 hemeng]
#define	MUXPATCH_ERROR_ALREADYEXIST		WM_MUXPATCHUPDATE + 1004		// 已有启动的更新线程]
#define MUXPREPPATCH_HAVEPATCH WM_MUXPATCHUPDATE + 1005 //有预下载需要下载


#define MUXPATCH_PROGRESS_BEGIN			0
#define MUXPATCH_PROGRESS_END			100

//下载常量定义
#define MUXPATCH_DOWNLOAD_SERVERNOTFOUND WM_MUXPATCHUPDATE + 2000 //Patch服务器不可用
#define MUXPATCH_DOWNLOAD_NETBREAKDOWN WM_MUXPATCHUPDATE + 2001  //网络连接断开
#define MUXPATCH_PATCHCHECK_HASHERROR WM_MUXPATCHUPDATE + 2002   //文件已损坏，需要重新下载
#define MUXPATCH_DOWNLOAD_PATCHNOTFOUND WM_MUXPATCHUPDATE + 2003 //下载的补丁不存在
#define MUXPATCH_GETHTTPADDR_ERROR WM_MUXPATCHUPDATE + 2004 //获得服务器列表失败
#define MUXPATCH_DISK_NOTENOUGH_ERROR WM_MUXPATCHUPDATE + 2005 //磁盘空间不足 
#define MAX_NAME_SIZE 128
//Cat信息
typedef struct _CatInfo 
{
	unsigned int nCatID;
	char szCatName[MAX_NAME_SIZE];
} CATINFO, *PCATINFO;

//Area信息
typedef struct _AreaInfo
{
	unsigned int nCatID;
	unsigned int nAreaID;
	char szAreaName[MAX_NAME_SIZE];
} AREAINFO, *PAREAINFO;

//Server信息
typedef struct _ServerInfo
{
	unsigned int nAreaID;
	unsigned int nServerID;
	char szServerName[MAX_NAME_SIZE];
	char szServerIp[MAX_NAME_SIZE];
	unsigned short ServerPort;
	char szPatchIp[MAX_NAME_SIZE];
	unsigned short PatchPort;
	unsigned int PintValue;
} SERVERINFO, *PSERVERINFO;
//版本号
typedef struct _Version 
{
	unsigned char MajorVersion;
	unsigned char MinVersion;
	unsigned short AdditionVersion;
}PATCHVERSION, *PPATCHVERSION;

enum CONTROL_MASK {CPU_MASK = 0, NETWORK_MASK, IDLE_MASK, BANDWIDTH_MASK};
//接口定义
class PATCH_DLL_API CPatchCliIntf
{
public:
	virtual ~CPatchCliIntf(){};
public:
	//自动更新配置
	virtual int AddConstrlMask(CONTROL_MASK mask, int nValue) = 0;
	virtual int RemoveControlMask(CONTROL_MASK mask) = 0;
	//获取服务器配置
	virtual int GetServerInfo() = 0;
	//获取Cat信息
	virtual PCATINFO GetCatInfo(unsigned char& ucNum) = 0;

	virtual PAREAINFO GetAreaInfo(unsigned int nCatID, unsigned char& ucNum) = 0;

	virtual PSERVERINFO GetRegionInfo(unsigned int nAreaID, unsigned char& ucNum) = 0;
	
	virtual void GetRecommendServer(unsigned int& nCatID, unsigned int& nAreaID, unsigned int& nServerID) = 0;

	//启动Patch
	virtual int StartUp(const char* szIp, unsigned short usPort, bool bPrep = false) = 0;
	//停止Patch
	virtual int CleanUp() = 0;
	//控制预下载的暂停与继续
	virtual int ControlPreDownload(bool bPause) = 0;
	/*
	检查版本差异 在发现版本不一致时调用 
	主要作用时检查download下已下载的文件，检查结束后会通知客户端是下载还是安装，如果是下载
	，还需要下载那些文件，优先下载那些还没有下载完成的
	在预下载时，需要判断返回值来确定是否有预下载的Patch，以提醒用户是否下载 MUXPREPPATCH_HAVEPATCH
	*/
	virtual int CheckVersion() = 0;
};



//回调接口定义
class PATCH_DLL_API CPatchCliEvent
{
public:
	//客户端版本
	virtual int OnCliVersion(PATCHVERSION& version) = 0;
	//服务器版本
	virtual int OnServerVersion(PATCHVERSION& version, unsigned char ucPatchNum) = 0;
	//下载进度
	virtual int OnDownloadProgress(PATCHVERSION& version, int nIndex, int nPos, int nDownloadSpeed, int nUploadSpeed, float fFileSize) = 0;
	//安装进度
	virtual int OnInstallProgress(PATCHVERSION& version, int nStatus, int nPos) = 0;
	//错误接口
	virtual int OnError(int nErrorCode) = 0;
};

extern "C" PATCH_DLL_API CPatchCliIntf* CreatePatchIntf(CPatchCliEvent* pPatchCliEvent);
extern "C" PATCH_DLL_API void DeletePatchIntf(CPatchCliIntf* pIntf);

class PATCH_DLL_API CDirectDownloadIntf
{
public:
	virtual ~CDirectDownloadIntf() {};
public:
	virtual int Init(const char* szIp, unsigned short usPort) = 0;
	virtual int Finit() = 0;
	virtual int QueryCliHashFileInfo() = 0;
	virtual int QueryFileSizeByName(const char* szFileName) = 0;
	virtual int DownloadFile(const char* szFileName, unsigned int nOffset, unsigned int nFileSize) = 0;
};

class PATCH_DLL_API CDownloadEvent
{
public:
	virtual ~CDownloadEvent(){};
public:
	virtual int OnCliHashFileInfo(const char* szHashFileName, unsigned int nFileSize, const char* szRelativePath) = 0;
	virtual int OnDownloadFinished(const char* szFileName, unsigned int nOffset, char* pFileData) = 0;
	virtual int OnFileInfo(const char* szFileName, unsigned int nFileSize) = 0;
};

extern "C" PATCH_DLL_API CDirectDownloadIntf* CreateDownloadIntf(CDownloadEvent* pEvent);
extern "C" PATCH_DLL_API void DeleteDownloadIntf(CDirectDownloadIntf* pIntf);

#endif