﻿#include "BtClient.h"


CBtClient::CBtClient(const char* szUdpXServer, const char* szUdpEServer, const char* szConfigFile, const char* szStatisticIp, unsigned short nPort, const char* szTrackerServer)
: m_UdpXServer(szUdpXServer), m_UdpEServer(szUdpEServer), m_ConfigFileName(szConfigFile), m_Running(false), m_StatisticServerIp(szStatisticIp), m_StatisticServerPort(nPort), m_TrackerServer(szTrackerServer)
{
	m_LastDoEventTime = GetTickCount();
}

CBtClient::~CBtClient(void)
{
	if (m_Running)
	{
		Stop();
	}
}

int CBtClient::Start()
{
	char szBuf[MAX_FILE_NAME];
	getcwd(szBuf, MAX_FILE_NAME);
	string strCurDir = szBuf;
	BOOL bRet = TEST_APP_LoadP2PMod((strCurDir + "\\").c_str(), m_UdpXServer.c_str(), m_UdpEServer.c_str(), TRUE, (strCurDir + "\\" + m_ConfigFileName).c_str());
	if (!bRet)
	{
		return -1;
	}
	
	m_Running = true;
	return 0;
}

int CBtClient::Stop()
{
	if (m_Running)
	{
		TEST_APP_ReleaseP2PMod();
		m_Running = false;
	}
	return 0;
}

int CBtClient::GetNatInfo(P2PNatInfo &NatInfo)
{
	TEST_APP_GetNatInfo(&NatInfo);
	return 0;
}

void CBtClient::DoEvents()
{
	if (GetTickCount() - m_LastDoEventTime > 200)
	{
		TEST_APP_DoEvents();
		m_LastDoEventTime = GetTickCount();
	}
}

int CBtClient::BeginTask(const char* szTorrentUrl, const char* szFileUrl, const char* szLocalPath)
{
	TEST_APP_SetAnnounces(m_TrackerServer.c_str());

	TEST_APP_SetStatServerInfo(m_StatisticServerIp.c_str(), m_StatisticServerPort);

	//全局预先设置Http超时, Data超时, Http重试次数(单位毫秒)
	TEST_APP_SetHttpTimeOut( 5 * 1000, 30 * 1000, 0 ); //推荐值

	//全局设置WebSeed模式下的数据断流超时(和SetHttpTimeOut不同)
	TEST_APP_SetWebSeedDataTimeOut( 150 * 1000 );

	int nTaskID = -1;
	nTaskID = TEST_APP_StartTask(szTorrentUrl, szFileUrl, szLocalPath, "PatchClient");
	return nTaskID;
}

void CBtClient::EndTask(int nTaskID)
{
	TEST_APP_StopTask(nTaskID);
}

void CBtClient::GetTaskInfo(int nTaskID, P2PTaskInfo& p2pTaskInfo)
{
	TEST_APP_GetTaskInfo(nTaskID, &p2pTaskInfo);
}

void CBtClient::SetDownloadRate(int nTaskID, int nLimit)
{
	TEST_APP_SetTaskDownRate(nTaskID, nLimit);
}

void CBtClient::SetUploadRate(int nTaskID, int nLimit)
{
	TEST_APP_SetTaskUpRate(nTaskID, nLimit);
}




