﻿#include "Configuration.h"
#include "tinyxml.h"

#define PATCH_CONFIG_FILE "PatchClient.xml"

CConfiguration g_Configuration;
CConfiguration::CConfiguration(void)
{
	LoadConfig();
}

CConfiguration::~CConfiguration(void)
{

}

int CConfiguration::LoadConfig()
{
	TiXmlDocument xmlDoc;
	if (!xmlDoc.LoadFile(PATCH_CONFIG_FILE))
	{
		return -1;
	}
	TiXmlElement* pXmlRoot = xmlDoc.RootElement();
	TiXmlElement* pXmlElement = pXmlRoot->FirstChildElement();
	if (pXmlElement)
	{
		string strVersion = pXmlElement->Attribute("Version");
		string::size_type stPos = strVersion.find('.');
		vector<uint32> vectInt;
		while (stPos != string::npos)
		{
			string strTmp;
			strTmp = strVersion.substr(0, stPos);
			vectInt.push_back(atoi(strTmp.c_str()));
			strVersion = strVersion.substr(stPos + 1, strVersion.length());
			stPos = strVersion.find('.');
		}
		if (!strVersion.empty())
		{
			vectInt.push_back(atoi(strVersion.c_str()));
		}
		if (vectInt.size() != 3)
		{
			//显示版本号错误
			return -1;
		}
		m_CliVersion.MajorVersion = vectInt[0];
		m_CliVersion.MinVersion = vectInt[1];
		m_CliVersion.AdditionVersion = vectInt[2];
	}

	pXmlElement = pXmlElement->NextSiblingElement();
	if (pXmlElement)
	{
		m_HttpAddr = pXmlElement->Attribute("HttpAddr");
	}
	
	return 0;
}

string CConfiguration::GetPatchDir()
{
	char szBuf[MAX_FILE_NAME];
	getcwd(szBuf, MAX_FILE_NAME);
	//设置Patch下载目录
	string strDir(szBuf);
	strDir.append(1, PATH_DELIMITER);
	strDir.append(PATCHDIR);
	return strDir;
}

int CConfiguration::LoadServerConfig()
{
	TiXmlDocument xmlDoc;
	if (!xmlDoc.LoadFile(".\\ServerConfig.xml"))
	{
		return -1;
	}
	
	TiXmlElement* pXmlRoot = xmlDoc.RootElement();
	TiXmlElement* pXmlRecommendServer = pXmlRoot->FirstChildElement();
	pXmlRecommendServer->Attribute("CatID", (int*)&(m_RecommendCatID));
	pXmlRecommendServer->Attribute("AreaID", (int*)&(m_RecommendAreaID));
	pXmlRecommendServer->Attribute("ServerID", (int*)&(m_RecommendServerID));

	for (TiXmlElement* pCatElement = pXmlRecommendServer->NextSiblingElement(); pCatElement; pCatElement = pCatElement->NextSiblingElement())
	{
		CATINFO catInfo;
		memset(&catInfo, 0, sizeof(CATINFO));
		pCatElement->Attribute("CatID", (int*)&(catInfo.nCatID));
		strcpy(catInfo.szCatName, pCatElement->Attribute("CatName"));
		m_CatInfos.push_back(catInfo);
		for (TiXmlElement* pAreaElement = pCatElement->FirstChildElement(); pAreaElement; pAreaElement = pAreaElement->NextSiblingElement())
		{
			AREAINFO areaInfo;
			memset(&areaInfo, 0, sizeof(areaInfo));
			pAreaElement->Attribute("AreaID", (int*)&(areaInfo.nAreaID));
			areaInfo.nCatID = catInfo.nCatID;
			strcpy(areaInfo.szAreaName, pAreaElement->Attribute("AreaName"));
			m_AreasInfo.push_back(areaInfo);
			for (TiXmlElement* pServerElement = pAreaElement->FirstChildElement(); pServerElement; pServerElement = pServerElement->NextSiblingElement())
			{
				SERVERINFO serverInfo;
				memset(&serverInfo, 0, sizeof(SERVERINFO));
				serverInfo.nAreaID = areaInfo.nAreaID;
				pServerElement->Attribute("ServerID", (int*)&(serverInfo.nServerID));
				strcpy(serverInfo.szServerName, pServerElement->Attribute("ServerName"));
				strcpy(serverInfo.szServerIp, pServerElement->Attribute("ASServerIp"));
				pServerElement->Attribute("ASServerPort", (int*)&(serverInfo.ServerPort));
				strcpy(serverInfo.szPatchIp, pServerElement->Attribute("PatchServerIp"));
				pServerElement->Attribute("PatchServerPort", (int*)&(serverInfo.PatchPort));
				m_ServerInfos.push_back(serverInfo);
			}
		}
	}
	return 0;
}






