﻿//////////////////////////////////////////////////////////////////////////
// TestAppAPI.h : header file
//////////////////////////////////////////////////////////////////////////

#ifndef __TESTAPPAPI_H__
#define __TESTAPPAPI_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef BTAPI
#define BTAPI __stdcall
#endif

#ifndef __P2PTASKINFO__
#define __P2PTASKINFO__

typedef struct 
{
	int nTaskID;
	UINT nState;
	unsigned __int64 n64TotalFileSize;
	unsigned __int64 n64TotalDownSize;
	unsigned __int64 n64TotalWebDownSize;
	float fPercent;
	float fDownRate;
	float fUpRate;
	float fAvgDownRate;
	float fAvgUpRate;
	int nErrCode;
	int nRetryTimes;

} P2PTaskInfo;

#endif//__P2PTASKINFO__

#ifndef __P2PNATINFO__
#define __P2PNATINFO__

typedef struct 
{
	int nLoginStatus;			//0: 失败, 1: 成功
	int nNatType;				//网络类型, 0:未知, 1:公网, 2:内网U, 3:内网S, 4:内网C, 5:内网M
	char szLocalIP[ 16 ];		//本地IP地址
	int nUsersCount;			//穿透登陆服务器当前在线人数

} P2PNatInfo;

#endif//__P2PNATINFO__

extern "C"
{
	//////////////////////////////////////////////////////////////////////////
	
	//库初始化
	BOOL BTAPI TEST_APP_LoadP2PMod( const char *lpszDllPath, const char *lpszUDPIPPorts, const char *lpszTunnelIPPorts, BOOL bEnableLog = TRUE, const char *lpszConfigPath = NULL );

	//库释放
	void BTAPI TEST_APP_ReleaseP2PMod();

	//设置本地的Tracker服务器地址
	void BTAPI TEST_APP_SetAnnounces( const char * lpszAnnounce );

	//设置要通知的统计服务器的域名(IP)和端口
	void BTAPI TEST_APP_SetStatServerInfo( const char *lpszHost, int nPort );

	//全局预先设置Http超时, Data超时, Http重试次数(单位毫秒)
	void BTAPI TEST_APP_SetHttpTimeOut( int nHttpTimeOut, int DataTimeOut, int nRetryTimes );

	//全局设置WebSeed模式下的数据断流超时(和SetHttpTimeOut不同)
	void BTAPI TEST_APP_SetWebSeedDataTimeOut( int nDataTimeOut );

	//开始一个新任务，返回一个任务ID, nBackUpdate=0 表示对比更新, nBackUpdate=1表示后台上传
	UINT BTAPI TEST_APP_StartTask( const char *lpszTorrentURL, const char *lpszWebURLs, const char *lpszDestPath, const char *lpszTaskName, BOOL bOnlyCheck = FALSE );

	//结束一个任务，传入要结束的任务ID
	void BTAPI TEST_APP_StopTask( UINT nTaskID );

	//限制任务的下载速度(单位:字节)
	void BTAPI TEST_APP_SetTaskDownRate( int nTaskID, int nLimit );

	//限制任务的上传速度(单位:字节)
	void BTAPI TEST_APP_SetTaskUpRate( int nTaskID, int nLimit );

	//驱动环境运行
	void BTAPI TEST_APP_DoEvents();

	//获取内网穿透的网络信息
	void BTAPI TEST_APP_GetNatInfo( P2PNatInfo *pInfo );

	//获取任务下载运行信息
	void BTAPI TEST_APP_GetTaskInfo( int nTaskID, P2PTaskInfo *pInfo );

	//////////////////////////////////////////////////////////////////////////
}

#endif//__TESTAPPAPI_H__