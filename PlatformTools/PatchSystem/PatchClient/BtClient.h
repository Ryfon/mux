﻿#ifndef BTCLIENT_H
#define	BTCLIENT_H	
#include "..\Common\Common.h"
#include "TestAppAPI.h"

class CBtClient
{
public:
	CBtClient(const char* szUdpXServer, const char* szUdpEServer, const char* szConfigFile, const char* szStatisticIp, unsigned short nPort, const char* szTrackerServer);
	virtual ~CBtClient(void);
public:
	int Start();
	int Stop();
	int GetNatInfo(P2PNatInfo& NatInfo);
	void GetTaskInfo(int nTaskID, P2PTaskInfo& p2pTaskInfo);
	void DoEvents();
	int BeginTask(const char* szTorrentUrl, const char* szFileUrl, const char* szLocalPath);
	void EndTask(int nTaskID);

	void SetDownloadRate(int nTaskID, int nLimit);
	void SetUploadRate(int nTaskID, int nLimit);


	bool IsRunning() const
	{
		return m_Running;
	}

private:
	string m_UdpXServer;
	string m_UdpEServer;
	string m_ConfigFileName;
	string m_StatisticServerIp;
	unsigned short m_StatisticServerPort;
	string m_TrackerServer;
	bool m_Running;
	DWORD m_LastDoEventTime;
};




#endif






