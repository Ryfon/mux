﻿#ifndef PINGER_H
#define PINGER_H

#include "../Common/Common.h"
#include <WinSock2.h>

typedef struct ip_option_information {
	u_char Ttl;		/* Time To Live (used for traceroute) */
	u_char Tos; 	/* Type Of Service (usually 0) */
	u_char Flags; 	/* IP header flags (usually 0) */
	u_char OptionsSize; /* Size of options data (usually 0, max 40) */
	u_char *OptionsData;   /* Options data buffer */
} IPINFO, *PIPINFO, FAR *LPIPINFO;

/* Note 1: The Reply Buffer will have an array of ICMP_ECHO_REPLY
* structures, followed by options and the data in ICMP echo reply
* datagram received. You must have room for at least one ICMP
* echo reply structure, plus 8 bytes for an ICMP header. 
*/
typedef struct icmp_echo_reply {
	u_long Address; 	/* source address */
	u_long Status;	/* IP status value (see below) */
	u_long RTTime;	/* Round Trip Time in milliseconds */
	u_short DataSize; 	/* reply data size */
	u_short Reserved; 	/* */
	void FAR *Data; 	/* reply data buffer */
	struct ip_option_information Options; /* reply options */
} ICMPECHO, *PICMPECHO, FAR *LPICMPECHO;

struct PingStatus {
	bool success;
	DWORD status;
	float delay;
	uint32 destinationAddress;
	uint32 ttl;

	DWORD error;
};

//ICMP函数指针定义
typedef HANDLE (WINAPI* PICMPCREATEFILE)(VOID);
typedef BOOL (WINAPI* PICMPCLOSEHANDLE)(HANDLE);
typedef DWORD (WINAPI* PICMPSENDECHO)(HANDLE, DWORD, LPVOID, WORD, PIPINFO, LPVOID, DWORD, DWORD);

class CPinger
{
public:
	CPinger(void);
	virtual ~CPinger(void);
public:
	PingStatus Ping(const char* szIp, uint32 ttl = 64);
private:
	PICMPCREATEFILE m_IcmpCreateFile;
	PICMPCLOSEHANDLE m_IcmpCloseHandle;
	PICMPSENDECHO m_IcmpSendEcho;
	HANDLE m_hIcmpModule;
	HANDLE m_hImcpFile;
};

#endif




