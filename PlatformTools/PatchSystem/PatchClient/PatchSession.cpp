﻿#include "PatchSession.h"
#include "PatchClient.h"

CPatchSession::CPatchSession(void)
: m_Owner(NULL), m_Quit(false)
{

}

CPatchSession::~CPatchSession(void)
{

}

void CPatchSession::Terminate()
{
	m_Quit = true;
}

bool CPatchSession::Continue()
{
	return !m_Quit;
}

void CPatchSession::Quit()
{
	m_Quit = true;
}

void CPatchSession::OnCreated(GSECommunicationApplication *pkApplication, GSECommunication *pkCommunication)
{
	m_Owner->OnPatchConnected();
}

void CPatchSession::OnDestroy(GSECommunicationApplication *pkApplication, GSECommunication *pkCommunication)
{
	m_Owner->OnPatchClose();
}

void CPatchSession::OnRecv(GSECommunication *pkCommunication, char *pszData, int nLen)
{
	m_Owner->OnRecv(pszData, nLen);
}

void CPatchSession::OnSend(GSECommunication *pkCommunication, char *pszData, int nLen)
{
	m_Owner->OnSend(pszData, nLen);
}

void CPatchSession::OnError(GSECommunicationApplication *pkApplication, GSECommunication *pkCommunication, int nErrCode)
{
	m_Owner->OnError(nErrCode);
}

void CPatchSession::OnClose(GSECommunicationApplication *pkApplication, GSECommunication *pkCommunication)
{
	m_Owner->OnPatchClose();
}

void CPatchSession::OnTimedout(GSECommunicationApplication* pkApplication)
{
	m_Owner->OnTimeOut();
}

void CPatchSession::GetRecvBuf(GSECommunication *pkCommunication, char *&pszBuf, int &nLen)
{
	m_Owner->OnInitRecv(pszBuf, nLen);
}

void CPatchSession::GetSendBuf(GSECommunication *pkCommunication, char *&pszBuf, int &nLen)
{
	m_Owner->OnInitSend(pszBuf, nLen);
}

void CPatchSession::SetOwner(CPatchClient *Owner)
{
	m_Owner = Owner;
}

void CPatchSession::GetWaitTime(timeval &kTimeValue)
{
	kTimeValue.tv_sec = 0;
	kTimeValue.tv_usec = 1;
}

bool CPatchSession::GetRecvMask(GSECommunication* pkCommunication)
{

	return m_Owner->OnReadMask();
}

bool CPatchSession::GetSendMask(GSECommunication *pkCommunication)
{
	return m_Owner->OnWriteMask();
}





