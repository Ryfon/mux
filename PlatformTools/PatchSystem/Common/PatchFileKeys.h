﻿#ifndef PATCHFILEKEYS_H
#define PATCHFILEKEYS_H

#include <stddef.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

#include "Common.h"

int CreateMD5(const unsigned char* szBuf, int nBufLen, unsigned char* szMd5, int nMd5Len);
int CreateSHA1(const unsigned char* szBuf, int nBufLen, unsigned char* szSHA1, int nSHA1Len);

//目前只支持MD5的Key
class CPatchKey
{
	friend class CPatchFileKeys;
public:
	CPatchKey();
	CPatchKey(uint8* Key);
	virtual ~CPatchKey();
public:
	void SetKey(uint8* Key)
	{
		memcpy(m_Key, Key, MD5_DIGEST_LENGTH);
	}
	uint8* GetKey()
	{
		return m_Key;
	}
	void CreateKey(const unsigned char* szBuf, int nBufLen);
	bool operator == (const CPatchKey& other);
protected:

private:
	uint8 m_Key[MD5_DIGEST_LENGTH];
};

class CPatchFileKeys
{
public:
	CPatchFileKeys(void);
	virtual ~CPatchFileKeys(void);
public:
	CPatchKey* GetFileKey()
	{
		return &m_FileKey;
	}

	CPatchKey* GetKeyByPart(uint16 nPartIndex);
	void AddKeys(CPatchKey Key);
	uint16 GetHashCount() const
	{
		return m_PartsKey.size();
	}
	void CreateFileKey();
private:
	CPatchKey m_FileKey;
	vector<CPatchKey> m_PartsKey;
};

#endif

