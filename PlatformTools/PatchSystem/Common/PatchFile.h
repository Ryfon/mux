﻿#ifndef PATCHFILE_H 
#define PATCHFILE_H
#include "Common.h"
#include "PatchFileKeys.h"

#define PATCHFILE_PRE "Patch"
#define PATCHFILE_POST ".exe"
#define DOWNLOAD_POST ".dow"

class CPatchVersion
{
	friend bool operator <(const CPatchVersion& first, const CPatchVersion& second);
	friend bool operator ==(const CPatchVersion& first, const CPatchVersion& second);
	friend bool operator !=(const CPatchVersion& first, const CPatchVersion& second);
public:
	CPatchVersion();
	CPatchVersion(uint8 nMajor, uint8 nMin, uint16 nAdd);
	virtual ~CPatchVersion();
public:
	void SetVersion(uint8 nMajor, uint8 nMin, uint16 nAdd);
public:
	int ParseVersion(const char* szVersionString); //patch2.20090727.2.mux
	string VersionToFileName(); //由版本号生成Patch文件名
	void GetVersionInfo(uint8& nMajor, uint8& nMin, uint16& nAdd);
public:
	CPatchVersion& operator =(const CPatchVersion& patchVersion);
private:
	uint8 m_MajorVersion; //主版本号
	uint8 m_MinorVersion; //小版本号 在DailyBuild中表示日期
	uint16  m_AdditionVersion; //附加版本号
};

bool operator < (const CPatchVersion& first, const CPatchVersion& second);

bool operator == (const CPatchVersion& first, const CPatchVersion& second);
bool operator != (const CPatchVersion& first, const CPatchVersion& second);

struct VersionComp 
{
	bool operator()(const CPatchVersion& Src, const CPatchVersion& Dst) const
	{
		return Src < Dst;
	}
};

class CFileSource;

class CPatchFile
{
public:
	CPatchFile(const char* PatchFileName);
	virtual ~CPatchFile(void);
public:
	int CheckPatchFileName(const char* szFileName);
	CPatchVersion GetPatchVersion()
	{
		return m_PatchVersion;
	}
	int LoadPatchFile(const char* szFileName);
	void AddSource(CFileSource* pFileSource);
	void DeleteSource(CFileSource* pFileSource);
	uint32 GetFileSize() const
	{
		return m_FileSize;
	}
	//从给定的位置读取数据
	uint32 ReadData(uint32 nPos, char* pData, uint32 nLen);
	uint16 GetPartCount() const
	{
		return m_PartCount;
	}

	CPatchFileKeys* GetFileKeys()
	{
		return &m_FileKeys;
	}
private:
	void CreatePartCount();
private:
	uint32 m_FileSize; //Patch文件大小 最大为4G
	uint16 m_PartCount;
	vector<string> m_HashList;
	CPatchVersion m_PatchVersion;
	CPatchFileKeys m_FileKeys;
	vector<CFileSource*> m_FileSources;
	string m_FileName;
};

#endif
