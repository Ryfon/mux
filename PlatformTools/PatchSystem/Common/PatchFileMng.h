﻿#ifndef PATCHFILEMNG_H
#define PATCHFILEMNG_H
#include "Common.h"
#include "GSELock.h"
#include "PatchFile.h"

class CPatchFile;
class CPatchVersion;

class CPatchFileMng
{
	friend class CPatchMonitor;
public:
	CPatchFileMng(void);
	virtual ~CPatchFileMng(void);
public:
	int LoadPatchFiles(const char* szFilePath);
public:
	CPatchFile* GetPatchFileByVersion(uint8 nMajor, uint32 nMin, uint16 nAdd);
	CPatchVersion GetCurVersion();
	void GetLatestVersion(CPatchVersion SrcVersion, vector<CPatchVersion>& vectVersion);
public:
	int CreatePatchFile(const char* szFileName);
	int DeletePatchFile(const char* szFileName);
private:
	void ClearPatchFileList();
private:
	map<CPatchVersion, CPatchFile*, VersionComp> m_PatchFileMap;
	GSELock m_MapLock;
};

#endif

