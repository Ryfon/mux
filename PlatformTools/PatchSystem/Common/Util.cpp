﻿#include "Util.h"

CUtil::CUtil(void)
{

}

CUtil::~CUtil(void)
{

}

void CUtil::Msg(const char* szMsg, ...)
{
	va_list va;
	va_start(va, szMsg);
	vprintf(szMsg, va);
	va_end(va);
	fflush(stdout);
}





