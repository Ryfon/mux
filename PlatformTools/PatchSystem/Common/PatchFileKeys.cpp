﻿#include "PatchFileKeys.h"

int CreateMD5(const unsigned char* szBuf, int nBufLen, unsigned char* szMd5, int nMd5Len)
{
	if (nMd5Len < MD5_DIGEST_LENGTH)
	{
		return -1;
	}

	MD5(szBuf, nBufLen, szMd5);

	return 0;
}

int CreateSHA1(const unsigned char* szBuf, int nBufLen, unsigned char* szSHA1, int nSHA1Len)
{
	if (nSHA1Len < SHA_DIGEST_LENGTH)
	{
		return -1;
	}
	SHA1(szBuf, nBufLen, szSHA1);
	return 0;
}

CPatchKey::CPatchKey()
{
	memset(m_Key, 0, MD5_DIGEST_LENGTH);
}

CPatchKey::CPatchKey(uint8* Key)
{
	memset(m_Key, 0, MD5_DIGEST_LENGTH);
	memcpy(m_Key, Key, MD5_DIGEST_LENGTH);
}

CPatchKey::~CPatchKey()
{
	
}

void CPatchKey::CreateKey(const unsigned char *szBuf, int nBufLen)
{
	CreateMD5(szBuf, nBufLen, m_Key, MD5_DIGEST_LENGTH);
}

bool CPatchKey::operator ==(const CPatchKey &other)
{
	return memcmp(m_Key, other.m_Key, MD5_DIGEST_LENGTH);
}

CPatchFileKeys::CPatchFileKeys(void)
{
	
}

CPatchFileKeys::~CPatchFileKeys(void)
{
	m_PartsKey.clear();
}

CPatchKey* CPatchFileKeys::GetKeyByPart(uint16 nPartIndex)
{
	assert(nPartIndex < m_PartsKey.size());
	if (!(nPartIndex < m_PartsKey.size()))
	{
		return 0;
	}
	return &(m_PartsKey[nPartIndex]);
}

void CPatchFileKeys::AddKeys(CPatchKey Key)
{
	m_PartsKey.push_back(Key);
}

void CPatchFileKeys::CreateFileKey()
{
	if (m_PartsKey.size() == 0)
	{
		return;
	}
	string strKeys;
	for (size_t nIndex = 0; nIndex < m_PartsKey.size(); nIndex++)
	{
		strKeys.append((char*)(m_PartsKey[nIndex].GetKey()), 0, MD5_DIGEST_LENGTH);
	}
	m_FileKey.CreateKey((uint8*)strKeys.c_str(), strKeys.length());
}



