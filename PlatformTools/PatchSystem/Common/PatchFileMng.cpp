﻿#include "PatchFileMng.h"
#include "PatchFile.h"
#include "GSEGuard.h"
#include "Util.h"

CPatchFileMng::CPatchFileMng(void)
{
	
}

CPatchFileMng::~CPatchFileMng(void)
{
	ClearPatchFileList();
}

int CPatchFileMng::LoadPatchFiles(const char *szFilePath)
{
	char szBuf[MAX_FILE_NAME];
	getcwd(szBuf, MAX_FILE_NAME);
	string strWorkdir(szBuf);
	strWorkdir.append(1, PATH_DELIMITER);
	strWorkdir.append(szFilePath);
	int nRet = chdir(strWorkdir.c_str());
#ifdef WIN32
	if (nRet == -1 && errno == ENOENT)
	{
		mkdir(strWorkdir.c_str());
		nRet = chdir(strWorkdir.c_str());
	}
	_finddata_t c_filedata;
	string strFilter = "*";
	strFilter += PATCHFILE_POST;
	long hFile =_findfirst(strFilter.c_str(), &c_filedata);
	while (hFile != -1)
	{
		//如果不是目录
		if (!(c_filedata.attrib & _A_SUBDIR))
		{
			//获得文件名
			char szFileName[MAX_FILE_NAME];
			sprintf(szFileName, "%s%c%s", strWorkdir.c_str(), PATH_DELIMITER, c_filedata.name);
			CreatePatchFile(szFileName);
		}
		if (_findnext(hFile, &c_filedata))
		{
			break;
		}
	}
	_findclose(hFile);
#else
	if (nRet == -1 && errno == ENOENT)
	{
		mkdir(strWorkdir.c_str(), 0777);
		nRet = chdir(strWorkdir.c_str());
	}

	DIR* pDir = 0;
	dirent* pEnt = 0;
	pDir = opendir(strWorkdir.c_str());
	if (0 == pDir)
	{
		CUtil::Msg("Patch directory not found\n");
		chdir(szBuf);
		return -1;
	}
	
	while (0 != (pEnt = readdir(pDir)))
	{
		//判断是否..或.
		if (pEnt->d_name[0] == '.')
		{
			continue;
		}
		//判断后缀名，只有.exe才认为是补丁
		const char* nPos = strstr(pEnt->d_name, PATCHFILE_POST);
		if (!nPos || strlen(pEnt->d_name) - (nPos - pEnt->d_name) != 4)
		{
			CUtil::Msg("%s\n", pEnt->d_name);
			continue;
		}
		//获得文件名
		char szFileName[MAX_FILE_NAME];
		sprintf(szFileName, "%s%c%s", strWorkdir.c_str(), PATH_DELIMITER, pEnt->d_name);
		CreatePatchFile(szFileName);
	}
	closedir(pDir);
	pDir = 0;
#endif
	chdir(szBuf);
	return 0;
}

int CPatchFileMng::CreatePatchFile(const char *szFileName)
{
	//根据文件名获得Patch版本号
	CPatchFile* pPatchFile = new CPatchFile(szFileName);
	pPatchFile->LoadPatchFile(szFileName);

	GSEGuard gseGuard(&m_MapLock);
	map<CPatchVersion, CPatchFile*, VersionComp>::iterator Ite = m_PatchFileMap.find(pPatchFile->GetPatchVersion());
	if (Ite != m_PatchFileMap.end())
	{
		delete Ite->second;
		m_PatchFileMap.erase(Ite);
	}
	m_PatchFileMap[pPatchFile->GetPatchVersion()] = pPatchFile;
	return 0;
}

int CPatchFileMng::DeletePatchFile(const char* szFileName)
{
	//根据文件名获得Patch版本号
	CPatchFile* pPatchFile = new CPatchFile(szFileName);
	pPatchFile->LoadPatchFile(szFileName);

	GSEGuard gseGuard(&m_MapLock);
	map<CPatchVersion, CPatchFile*, VersionComp>::iterator Ite = m_PatchFileMap.find(pPatchFile->GetPatchVersion());
	if (Ite != m_PatchFileMap.end())
	{
		delete Ite->second;
		m_PatchFileMap.erase(Ite);
	}
	delete pPatchFile;	
	return 0;
}

void CPatchFileMng::ClearPatchFileList()
{

	GSEGuard gseGaurd(&m_MapLock);
	for (map<CPatchVersion, CPatchFile*, VersionComp>::iterator Ite = m_PatchFileMap.begin(); Ite != m_PatchFileMap.end(); Ite++)
	{
		delete Ite->second;
	}
	m_PatchFileMap.clear();
}

CPatchFile* CPatchFileMng::GetPatchFileByVersion(uint8 nMajor, uint32 nMin, uint16 nAdd)
{
	CPatchVersion PatchVersion(nMajor, nMin, nAdd);

	GSEGuard gseGuard(&m_MapLock);
	map<CPatchVersion, CPatchFile*, VersionComp>::iterator Ite = m_PatchFileMap.find(PatchVersion);
	return Ite != m_PatchFileMap.end() ? Ite->second : NULL;
}

void CPatchFileMng::GetLatestVersion(CPatchVersion SrcVersion, vector<CPatchVersion>& vectVersion)
{
	vectVersion.clear();

	GSEGuard gseGaurd(&m_MapLock);
	for (map<CPatchVersion, CPatchFile*, VersionComp>::iterator Ite = m_PatchFileMap.begin(); Ite != m_PatchFileMap.end(); Ite++)
	{
		CPatchFile* pPatchFile = Ite->second;
		if (operator<(SrcVersion, pPatchFile->GetPatchVersion()))
		{
			CPatchVersion PatchVersion = pPatchFile->GetPatchVersion();
			vectVersion.push_back(PatchVersion);
		}
	}
}

CPatchVersion CPatchFileMng::GetCurVersion()
{
	GSEGuard gseGuard(&m_MapLock);
	if (!m_PatchFileMap.empty())
	{
		map<CPatchVersion, CPatchFile*, VersionComp>::iterator Ite = --(m_PatchFileMap.end());
		return Ite->first;
	}
	return CPatchVersion(0, 0, 0);
}

