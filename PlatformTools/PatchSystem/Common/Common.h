﻿#ifndef COMMON_H
#define COMMON_H

#include <sys/types.h>

//去掉某些警告
#ifdef WIN32
#pragma warning(disable:4244)

#pragma warning(disable:4267)

#pragma warning(disable:4800)

#pragma warning(disable:4018)

#pragma warning(disable:4311)

#pragma warning(disable:4305)

#pragma warning(disable:4005)

#pragma warning(disable:4996)
#endif

//类型定义
#ifdef WIN32
	typedef __int64  int64;
	typedef long int32;
	typedef short int16;
	typedef char int8;
	typedef unsigned __int64 uint64;
	typedef unsigned long uint32;
	typedef unsigned short uint16;
	typedef unsigned char uint8;
#else
	typedef __int64_t int64;
	typedef __int32_t int32;
	typedef __int16_t int16;
	typedef __int8_t int8;
	typedef __uint64_t uint64;
	typedef __uint32_t uint32;
	typedef __uint16_t uint16;
	typedef __uint8_t uint8;
	typedef uint16 WORD;
	//typedef uint32 DWORD;
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <assert.h>
#include <stdarg.h>

#include <set>
#include <list>
#include <string>
#include <map>
#include <queue>
#include <sstream>
#include <algorithm>
#include <fstream>
using namespace std;

#ifdef WIN32
#define STRCASECMP stricmp
#define STRNCASECMP strnicmp
#define SIGQUIT 3
#define MAX_FILE_NAME 260
#define PATH_DELIMITER '\\'
#include <io.h>
#include <direct.h>

//为了支持稀疏文件增加的头文件
#define _WIN32_WINNT 0x0501
#define _WINSOCKAPI_
#include <Windows.h>
#include <WinIoCtl.h>

#else
#define STRCASECMP strcasecmp
#define STRNCASECMP strncasecmp
#define MAX_FILE_NAME 256
#define PATH_DELIMITER '/'
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/inotify.h>
#endif

#define PARTSIZE 9728000

#endif
