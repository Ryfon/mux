﻿#include "Configuration.h"
#include "tinyxml.h"
#define PATCH_CONFIG_FILE "PatchServer.xml"
CConfiguration::CConfiguration(void)
{
	LoadData(PATCH_CONFIG_FILE);
}

CConfiguration::~CConfiguration(void)
{

}

int CConfiguration::LoadData(const char *szXmlFile)
{
	TiXmlDocument xmlDoc;
	if (!xmlDoc.LoadFile(szXmlFile))
	{
		return -1;
	}
	TiXmlElement* pXmlRoot = xmlDoc.RootElement();
	TiXmlElement* pElement = pXmlRoot->FirstChildElement();
	m_PatchServerAddr = pElement->Attribute("IpAddr");
	pElement->Attribute("Port", (int*)&m_PatchServerPort);
	
	pElement = pElement->NextSiblingElement();
	m_PatchFilePath = pElement->Attribute("FilePath");
	m_PatchTorrentUrl = pElement->Attribute("TorrentUrl");
	m_PatchFileUrl = pElement->Attribute("FileUrl");

	pElement = pElement->NextSiblingElement();
	m_PrepPatchPath = pElement->Attribute("FilePath");
	m_PrepPatchTorrentUrl = pElement->Attribute("TorrentUrl");
	m_PrepPatchFileUrl = pElement->Attribute("FileUrl");

	pElement = pElement->NextSiblingElement();
	m_CheckPatchPatch = pElement->Attribute("FilePath");
	m_CheckPatchTorrentUrl = pElement->Attribute("TorrentUrl");
	m_CheckPatchFileUrl = pElement->Attribute("FileUrl");

	for (TiXmlElement* pIpFilter = pElement->FirstChildElement(); pIpFilter; pIpFilter = pIpFilter->NextSiblingElement())
	{
		string strIpFilter = pIpFilter->Attribute("IpMask");
		IpStruct ips;
		memset(&ips, 0, sizeof(IpStruct));
		GetIpStruct(strIpFilter, ips);
		m_IpFilterVector.push_back(ips);
	}

	pElement = pElement->NextSiblingElement();
	m_UdpXServer = pElement->Attribute("UdpXServer");
	m_UdpEServer = pElement->Attribute("UdpEServer");
	m_StatisticServerIp = pElement->Attribute("StatisticServer");
	m_StatisticServerPort = atoi(pElement->Attribute("StatisticServerPort"));
	m_TrackServer = pElement->Attribute("TrackServer");

	pElement = pElement->NextSiblingElement();
	m_CliHashFileName = pElement->Attribute("CliHashFileName");
	m_ClientPath = pElement->Attribute("ClientPath");

	return 0;		 
}

void CConfiguration::GetIpAddr(std::string &strIpAddr, uint16& nPort)
{
	strIpAddr = m_PatchServerAddr;
	nPort = m_PatchServerPort;
}

bool CConfiguration::IsAllowIp(const char *szIp)
{
	bool bAllow = false;
	IpStruct ips;
	memset(&ips, 0, sizeof(IpStruct));
	GetIpStruct(szIp, ips);
	for (size_t nIndex = 0; nIndex < m_IpFilterVector.size(); nIndex++)
	{
		if (m_IpFilterVector[nIndex].Ip[0] == ips.Ip[0] && m_IpFilterVector[nIndex].Ip[1] == ips.Ip[1] && m_IpFilterVector[nIndex].Ip[2] == ips.Ip[2] && 
			(m_IpFilterVector[nIndex].Ip[3] == 0 || m_IpFilterVector[nIndex].Ip[3] == ips.Ip[3]))
		{
			bAllow = true;
			break;
		}
	}
	return bAllow;
}

void CConfiguration::GetIpStruct(const string& strIpFilter, IpStruct &ips)
{
	int nIpIndex = 0;
	int nPos = 0;
	for (size_t nIndex = 0; nIndex < strIpFilter.length(); nIndex++)
	{
		if (strIpFilter[nIndex] == '.')
		{
			ips.Ip[nIpIndex] = atoi(strIpFilter.substr(nPos, nIndex - nPos).c_str());
			nIpIndex++;
			nPos = nIndex + 1;
		}
		else if (nIndex == strIpFilter.length() - 1)
		{
			ips.Ip[nIpIndex] = atoi(strIpFilter.substr(nPos, strIpFilter.length() - nPos).c_str());
		}

	}

}
CConfiguration g_Configuration;




