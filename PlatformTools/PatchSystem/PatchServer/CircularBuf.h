﻿#ifndef CIRCULARBUF_H
#define CIRCULARBUF_H
#include "../Common/Common.h"
#include "GSEMemoryObject.h"
//循环缓存区
class CCircularBuf: public GSEMemoryObject
{
public:
	CCircularBuf(uint32 nSize);
	virtual ~CCircularBuf(void);
	int ReadData(char* pDest, uint32 nDataLen); //把数据拷贝出去
	int SoftReadData(char* pDest, uint32 nDataLen);
	int WriteData(const char* pSrc, uint32 nDataLen); //把数据拷贝进来
	int UpdateWrite(uint32 nLen);
	int UpdateRead(uint32 nLen);
	uint32 GetUsedSize(); //获取数据大小
	uint32 GetFreeSize(); //获得自由空间大小
	char* GetReadPtr(); //获得读指针
	char* GetWritePtr(); //获得写指针
	uint32 GetContiguousFreeSize(int nType); //获取地址连续的空间大小
private:
	uint32 m_DataSize; //缓冲区大小
	char* m_Data; //数据指针
	uint32 m_ReadPtr; //读位置
	uint32 m_WritePtr; //写位置
	uint32 m_Count; //数据长度
};

#endif


