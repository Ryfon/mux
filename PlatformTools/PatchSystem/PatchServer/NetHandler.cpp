﻿#include "NetHandler.h"
#include "SessionMng.h"

CNetHandler::CNetHandler(CSessionMng& SessionMng) throw (GSEException)
: m_SessionMng(SessionMng), m_bContinue(true)
{

}

CNetHandler::~CNetHandler(void)
{

}

void CNetHandler::Terminate()
{
	m_bContinue = false;
}

bool CNetHandler::Continue()
{
	return m_bContinue;
}

void CNetHandler::OnCreated(GSECommunicationApplication *pkApplication, GSECommunication *pkCommunication)
{
	m_SessionMng.OnStart(pkCommunication);
}

void CNetHandler::OnDestroy(GSECommunicationApplication *pkApplication, GSECommunication *pkCommunication)
{
	m_SessionMng.OnStop(pkCommunication);
}

void CNetHandler::OnRecv(GSECommunication *pkCommunication, char *pszData, int nLen)
{
	m_SessionMng.OnRecv(pkCommunication, pszData, nLen);
}

void CNetHandler::OnSend(GSECommunication *pkCommunication, char *pszData, int nLen)
{
	m_SessionMng.OnSend(pkCommunication, pszData, nLen);
}

void CNetHandler::OnError(GSECommunicationApplication *pkApplication, GSECommunication *pkCommunication, int nErrCode)
{
	m_SessionMng.OnError(pkCommunication, nErrCode);
}

void CNetHandler::OnClose(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication)
{
	m_SessionMng.OnClose(pkCommunication);
}

void CNetHandler::GetRecvBuf(GSECommunication* pkCommunication, char*& pszBuf, int& nLen)
{
	m_SessionMng.OnInitRecv(pkCommunication, pszBuf, nLen);
}

void CNetHandler::GetSendBuf(GSECommunication *pkCommunication, char *&pszBuf, int &nLen)
{
	m_SessionMng.OnInitSend(pkCommunication, pszBuf, nLen);
}

void CNetHandler::OnCreatedPeer(GSECommunicationApplicationServer *pkServer, GSECommunication *pkCommunication) throw(GSEException)
{
	m_SessionMng.OnConnection(pkCommunication);
}

void CNetHandler::OnDestroyPeer(GSECommunicationApplicationServer *pkServer, GSECommunication *pkCommunication) throw(GSEException)
{
	m_SessionMng.OnDestroyConnection(pkCommunication);
}

bool CNetHandler::IsErasedPeer(GSECommunicationApplicationServer *pkServer, GSECommunication *pkCommunication)
{
	return m_SessionMng.OnErase(pkCommunication);
}

void CNetHandler::OnTimedout(GSECommunicationApplication *pkApplication)
{
	
}

void CNetHandler::GetWaitTime(timeval &kTimeValue)
{
	kTimeValue.tv_sec = 0;
	kTimeValue.tv_usec = 1;
}

bool CNetHandler::GetRecvMask(GSECommunication *pkCommunication)
{
	return m_SessionMng.OnReadMask(pkCommunication);
}

bool CNetHandler::GetSendMask(GSECommunication* pkCommunication)
{
	return m_SessionMng.OnWriteMask(pkCommunication);
}




