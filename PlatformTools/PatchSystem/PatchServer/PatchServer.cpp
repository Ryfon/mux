﻿#include "PatchServer.h"
#include "../Common/Util.h"

bool CPatchServer::m_bQuit = false;

CPatchServer::CPatchServer(void)
: m_PatchMonitor(0)
{

}

CPatchServer::~CPatchServer(void)
{
	if (m_PatchMonitor)
	{
		GSEMemDelete m_PatchMonitor;
		m_PatchMonitor = 0;
	}
}

void CPatchServer::HookSignal()
{
	signal(SIGINT, &CPatchServer::OnSignal);
	signal(SIGQUIT, &CPatchServer::OnSignal);
	signal(SIGTERM, &CPatchServer::OnSignal);
	signal(SIGABRT, &CPatchServer::OnSignal);
#ifdef WIN32
	signal(SIGBREAK, &CPatchServer::OnSignal);
#endif
}

void CPatchServer::UnHookSignal()
{
	signal(SIGINT, 0);
	signal(SIGQUIT, 0);
	signal(SIGTERM, 0);
	signal(SIGABRT, 0);
#ifdef WIN32
	signal(SIGBREAK, 0);
#endif
}

void CPatchServer::OnSignal(int nsignal)
{
	switch(nsignal)
	{
	case SIGINT:
	case SIGQUIT:
	case SIGTERM:
	case SIGABRT:
#ifdef WIN32
	case SIGBREAK:
#endif
		{
			m_bQuit = true;
		}
		break;
	default:
	    break;
	}
	signal(nsignal, &CPatchServer::OnSignal);
}

int CPatchServer::Init()
{
	HookSignal();
	m_SessionMng.SetDataProc(&m_DataProc);
	//加载PatchFile
	CUtil::Msg("%s", "loading Patch file \n");
	m_PatchFileMng.LoadPatchFiles(g_Configuration.GetPatchFilePath());
	m_PrepPatchFileMng.LoadPatchFiles(g_Configuration.GetPrepPatchFilePath());
	m_CheckPatchFileMng.LoadPatchFiles(g_Configuration.GetCheckPatchPatch());
	//创建Patch文件监视器
	m_PatchMonitor = GSEMemNew CPatchMonitor;
	m_PatchMonitor->SetPatchPath(g_Configuration.GetPatchFilePath(), &m_PatchFileMng);
	m_PatchMonitor->SetPatchPath(g_Configuration.GetPrepPatchFilePath(), &m_PrepPatchFileMng);
	m_PatchMonitor->SetPatchPath(g_Configuration.GetCheckPatchPatch(), &m_CheckPatchFileMng);
	m_PatchMonitor->Start();
	CUtil::Msg("%s", "process module running \n");
	m_DataProc.SetPatchFileMng(&m_PatchFileMng);
	m_DataProc.SetPrepPatchFileMng(&m_PrepPatchFileMng);
	m_DataProc.SetCheckPatchFileMng(&m_CheckPatchFileMng);
	m_DataProc.Init();
	//创建网络模块
	CUtil::Msg("%s", "net module running \n");
	m_SessionMng.Init();
	//创建业务处理模块
	return 0;
}

int CPatchServer::Finit()
{
	if (m_PatchMonitor)
	{
		m_PatchMonitor->Stop();
	}
	//停止业务处理模块
	CUtil::Msg("%s", "stopping process module \n");
	m_DataProc.Finit();
	//停止网络模块
	CUtil::Msg("%s", "stopping net module \n");
	m_SessionMng.Finit();
	UnHookSignal();
	return 0;
}

int CPatchServer::Run()
{
	char szBuf[128];
	while (!m_bQuit)
	{
#ifdef WIN32
		Sleep(1);
#else
		sleep(1);
#endif

		if (!fgets(szBuf, sizeof(szBuf), stdin))
		{
			continue;
		}
		if (STRNCASECMP(szBuf, "quit", strlen("quit")) == 0)
		{
			break;
		}
	}

	return 0;
}




