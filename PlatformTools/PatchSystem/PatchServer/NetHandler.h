﻿#ifndef NETHANDLER_H
#define NETHANDLER_H
#include "GSECommunicationApplicationCallback.h"
#include "GSELock.h"
#include "GSEPool.h"

class CSessionMng;
class CNetHandler: public GSECommunicationApplicationCallback
{
public:
	CNetHandler(CSessionMng& SessionMng) throw (GSEException);
	virtual ~CNetHandler(void);
public:
	
public:
	virtual void Terminate();
	virtual bool Continue();
	virtual void OnCreated(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication);
	virtual void OnDestroy(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication);
	virtual void OnRecv(GSECommunication* pkCommunication, char* pszData, int nLen);
	virtual void OnSend(GSECommunication* pkCommunication, char* pszData, int nLen);
	virtual void OnError(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication, int nErrCode);    
	virtual void OnClose(GSECommunicationApplication* pkApplication, GSECommunication* pkCommunication);
	virtual void OnTimedout(GSECommunicationApplication* pkApplication);
		 
	virtual bool GetRecvMask(GSECommunication* pkCommunication);
	virtual bool GetSendMask(GSECommunication* pkCommunication);

	virtual void GetWaitTime(struct timeval& kTimeValue);
	virtual int  GetWaitTime(){ return 0; }
	virtual void GetRecvBuf(GSECommunication* pkCommunication, char*& pszBuf, int& nLen);
	virtual void GetSendBuf(GSECommunication* pkCommunication, char*& pszBuf, int& nLen);
public:
	virtual void OnCreatedPeer(GSECommunicationApplicationServer* pkServer, GSECommunication* pkCommunication) throw(GSEException);
	virtual void OnDestroyPeer(GSECommunicationApplicationServer* pkServer, GSECommunication* pkCommunication) throw(GSEException);
	virtual bool IsErasedPeer(GSECommunicationApplicationServer* pkServer, GSECommunication* pkCommunication);
private:
	CSessionMng& m_SessionMng;
	bool m_bContinue;
};

#endif
