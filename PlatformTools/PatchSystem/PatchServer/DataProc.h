﻿#ifndef DATAPROC_H
#define DATAPROC_H
#include "../Common/Common.h"
#include "../PatchProtocol/PatchProtocol.h"
#include "GSELock.h"
#include "GSEThread.h"
#include "GSEThreadProc.h"
#include "GSECondition.h"
#include "GSESemaphore.h"

class CPatchFileMng;
class CCliSession;
class CDataProc;

struct MSGHANDLER 
{
	typedef int (CDataProc::*HANDLER)(CCliSession* pSession, char* pData, unsigned short usDataLen);
	MSGHANDLER(): handler(0){}
	MSGHANDLER(HANDLER _handler): handler(_handler){}
	~MSGHANDLER(){}
	HANDLER handler;
};

#define DealPlayerPkgPre( OUTTYPE, outPtr, inputPkg, inputLen, ProtolTool ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	memset( outPtr, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)ProtolTool.DeCode(OUTTYPE::wCmd, &tmpStruct, outBufLen, (char*)inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
{\
	return false;\
}

typedef PatchServer::MsgHeader MSGHEADER;
typedef PatchServer::MsgHeader* PMSGHEADER;

//自定义消息
#pragma pack(push, 1)

//连接建立
struct CONNMSG
{
	static const PatchServer::USHORT	wCmd = 0xFFFF;
};

struct CLOSEMSG
{
	static const PatchServer::USHORT wCmd = 0xFFFE;
};

#pragma pack(pop)

//处理Patch客户端相关请求并且回应Patch客户端
class CDataProc: public GSEThreadProc
{
	typedef std::map<unsigned short, MSGHANDLER> HANDLERMAP;
public:
	CDataProc(void);
	virtual ~CDataProc(void);
public:
	int Init();
	void SetPatchFileMng(CPatchFileMng* pPatchFileMng);
	void SetPrepPatchFileMng(CPatchFileMng* pMng);
	void SetCheckPatchFileMng(CPatchFileMng* pMng);
	int Finit();
	void AddData(char* pData);
	/*
	有一个线程，专门负责处理消息
	有一个队列，负责存储客户端传过来的消息
	线程就处理这个队列
	在有消息需要发送时，放进clisession的队列中
	网络线程会从中处理消息
	
	*/
private:
	int RegisterHandler();
	int ProcConn(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcClose(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcServerVersionReq(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcPatchFileInfoReq(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcDownload(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcError(CCliSession* pSession, unsigned short usErrorID);
	int ProcDownloadFile(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcLuancherVersionReq(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcHeatBeat(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcBtServerInfoReq(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcTorrentInfoReq(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcCliVerifyReq(CCliSession* pSession, char* pData, unsigned short usDataLen);
	int ProcReqFileInfo(CCliSession* pSession, char* pData, unsigned short usDataLen);

	virtual void ThreadProc(void) throw(GSEException);
	virtual void Terminate() throw(GSEException);
	int ProcData(char* pData); //因为把pData可以转化成协议，因此不用再传数据长度参数
private:
	CPatchFileMng* m_PatchFileMng;
	CPatchFileMng* m_PrePatchMng;
	CPatchFileMng* m_CheckPatchMng;
	HANDLERMAP m_ProcHandler;
	PatchServer::PatchSrvProtocol m_Protocol;
	list<char*> m_NetDatas;
	GSELock m_InputLock;
	GSEThread* m_DataProcThread;
#ifdef WIN32
	GSESemaphore m_QueSem;
#else
	pthread_mutex_t m_Mutex;
	pthread_cond_t m_Cond;
#endif
	bool m_bQuit;
};

#endif
