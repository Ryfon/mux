﻿#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include "../Common/Common.h"

struct IpStruct 
{
	unsigned char Ip[4];
};
class CConfiguration
{
public:
	CConfiguration(void);
	virtual ~CConfiguration(void);
public:
	void GetIpAddr(string& strIpAddr, uint16& nPort);
	const char* GetPatchFilePath()
	{
		return m_PatchFilePath.c_str();
	}
	
	const char* GetPrepPatchFilePath()
	{
		return m_PrepPatchPath.c_str();
	}

	const char* GetCheckPatchPatch()
	{
		return m_CheckPatchPatch.c_str();
	}
	bool IsAllowIp(const char* szIp);
private:
	int LoadData(const char* szXmlFile);
	void GetIpStruct(const string& strIpFilter, IpStruct& ips);
private:
	string m_PatchServerAddr;
	uint16 m_PatchServerPort;
	string m_PatchFilePath;
	string m_PrepPatchPath;
	string m_CheckPatchPatch;
	vector<IpStruct> m_IpFilterVector;
public:
	string m_UdpXServer;
	string m_UdpEServer;
	string m_StatisticServerIp;
	uint16 m_StatisticServerPort;

	string m_TrackServer;

	string m_PatchTorrentUrl;
	string m_PatchFileUrl;

	string m_PrepPatchTorrentUrl;
	string m_PrepPatchFileUrl;

	string m_CheckPatchTorrentUrl;
	string m_CheckPatchFileUrl;
	
	string m_CliHashFileName;
	string m_ClientPath;
};

extern CConfiguration g_Configuration;

#endif

