﻿#include "DataProc.h"
#include "../Common/PatchFileMng.h"
#include "CliSession.h"
#include "../Common/PatchFile.h"
#include "../Common/Common.h"
#include "GSEGuard.h"
#include "GSESingleton.h"
#include "GSEThreadFactory.h"
#include "../Common/Util.h"
#include "zlib.h"
#include "Configuration.h"

CDataProc::CDataProc(void)
: m_PatchFileMng(0), m_DataProcThread(0), m_bQuit(false)
{

}

CDataProc::~CDataProc(void)
{
	m_ProcHandler.clear();
}

int CDataProc::Init()
{
	RegisterHandler();
#ifndef WIN32
	pthread_mutex_init(&m_Mutex, NULL);
	pthread_cond_init(&m_Cond, NULL);
#endif
	m_DataProcThread = GSECreateThread(this, false);
	return 0;
}

int CDataProc::Finit()
{
	m_bQuit = true;
#ifdef WIN32
	m_QueSem.Release();
#else
	pthread_cond_signal(&m_Cond);
#endif
	Terminate();
	GSEDestroyThread(m_DataProcThread);
	m_DataProcThread = 0;
#ifndef WIN32
	pthread_mutex_destroy(&m_Mutex);
	pthread_cond_destroy(&m_Cond);
#endif
	return 0;
}

void CDataProc::SetPatchFileMng(CPatchFileMng *pPatchFileMng)
{
	assert(pPatchFileMng != 0);
	m_PatchFileMng = pPatchFileMng;
}

void CDataProc::SetPrepPatchFileMng(CPatchFileMng* pMng)
{
	assert(pMng != 0);
	m_PrePatchMng = pMng;
}

void CDataProc::SetCheckPatchFileMng(CPatchFileMng* pMng)
{
	assert(pMng != 0);
	m_CheckPatchMng = pMng;
}


int CDataProc::RegisterHandler()
{
	m_ProcHandler[PatchServer::C_P_REQSERVERVERSION] = MSGHANDLER(&CDataProc::ProcServerVersionReq);
	m_ProcHandler[PatchServer::C_P_REQPATCHFILEINFO] = MSGHANDLER(&CDataProc::ProcPatchFileInfoReq);
	m_ProcHandler[PatchServer::C_P_REQDOWNLOAD] = MSGHANDLER(&CDataProc::ProcDownload);
	m_ProcHandler[0xFFFF] = MSGHANDLER(&CDataProc::ProcConn);
	m_ProcHandler[0xFFFE] = MSGHANDLER(&CDataProc::ProcClose);
	m_ProcHandler[PatchServer::C_P_DOWNLOADFILE] = MSGHANDLER(&CDataProc::ProcDownloadFile);
	m_ProcHandler[PatchServer::C_P_VERIFYLUANCHERVERSION] = MSGHANDLER(&CDataProc::ProcLuancherVersionReq);
	m_ProcHandler[PatchServer::C_P_HEARTBEAT] = MSGHANDLER(&CDataProc::ProcHeatBeat);
	m_ProcHandler[PatchServer::C_P_REQBTSERVERINFO] = MSGHANDLER(&CDataProc::ProcBtServerInfoReq);
	m_ProcHandler[PatchServer::C_P_REQTORRENTINFO] = MSGHANDLER(&CDataProc::ProcTorrentInfoReq);
	m_ProcHandler[PatchServer::C_P_CLIVERIFYREQ] = MSGHANDLER(&CDataProc::ProcCliVerifyReq);
	m_ProcHandler[PatchServer::C_P_QUERYFILEINFO] = MSGHANDLER(&CDataProc::ProcReqFileInfo);

	return 0;
}

int CDataProc::ProcServerVersionReq(CCliSession* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPServerVersionReq, pCPServerVersionReq, pData, usDataLen, m_Protocol);
	bool bPrep = pCPServerVersionReq->Version.VersionMajor == 0 && pCPServerVersionReq->Version.VersionMin == 0 && pCPServerVersionReq->Version.VersionAddition == 0;
	CPatchFileMng* pPatchFileMng = bPrep ? m_PrePatchMng : m_PatchFileMng;
	//如果客户端版本号都是0，则表示是预下载的
	//获得当前版本
	PatchServer::PCServerVersionResp pcServerVersionResp;
	memset(&pcServerVersionResp, 0, sizeof(PatchServer::PCServerVersionResp));
	
	CPatchVersion SvrVersion = pPatchFileMng->GetCurVersion();
	CPatchVersion checkVersion(0, 0, 0);
	bool bHaveCheckVersion = false;
	if (!bPrep && pSession->CanVerify())
	{
		checkVersion = m_CheckPatchMng->GetCurVersion();
		if (SvrVersion < checkVersion)
		{
			SvrVersion = checkVersion;
			bHaveCheckVersion = true;
		}
	}

	SvrVersion.GetVersionInfo(pcServerVersionResp.Version.VersionMajor, (uint8&)pcServerVersionResp.Version.VersionMin, pcServerVersionResp.Version.VersionAddition);
	pcServerVersionResp.PatchNum = 0;
	CPatchVersion CliPatchVersion(pCPServerVersionReq->Version.VersionMajor, pCPServerVersionReq->Version.VersionMin, pCPServerVersionReq->Version.VersionAddition);
	if (CliPatchVersion != SvrVersion)
	{
		vector<CPatchVersion> vectPatchs;
		pPatchFileMng->GetLatestVersion(CliPatchVersion, vectPatchs);
		//如果不是预下载，根据ip判断是否允许下载验证版本，如果允许，把验证版本号放在最后
		if (bHaveCheckVersion)
		{
			vectPatchs.push_back(SvrVersion);
		}

		pcServerVersionResp.PatchNum = vectPatchs.size();
		for (size_t nIndex = 0; nIndex < vectPatchs.size(); nIndex++)
		{
			vectPatchs[nIndex].GetVersionInfo(pcServerVersionResp.PatchList[nIndex].VersionMajor, pcServerVersionResp.PatchList[nIndex].VersionMin, 
				pcServerVersionResp.PatchList[nIndex].VersionAddition);
			CPatchFile* pPatchFile = pPatchFileMng->GetPatchFileByVersion(pcServerVersionResp.PatchList[nIndex].VersionMajor, pcServerVersionResp.PatchList[nIndex].VersionMin
				, pcServerVersionResp.PatchList[nIndex].VersionAddition);
			if (!pPatchFile && bHaveCheckVersion)
			{
				pPatchFile = m_CheckPatchMng->GetPatchFileByVersion(pcServerVersionResp.PatchList[nIndex].VersionMajor, pcServerVersionResp.PatchList[nIndex].VersionMin
					, pcServerVersionResp.PatchList[nIndex].VersionAddition);
			}
			if (pPatchFile)
			{
				pcServerVersionResp.PatchList[nIndex].FileHash.HashLen = MAX_HASHVALUE_LEN;
				memcpy(pcServerVersionResp.PatchList[nIndex].FileHash.HashValue, pPatchFile->GetFileKeys()->GetFileKey()->GetKey(), MAX_HASHVALUE_LEN);
			}
		}
	}

	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCServerVersionResp) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCServerVersionResp* pPCServerVersionResp  = reinterpret_cast<PatchServer::PCServerVersionResp*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(pPCServerVersionResp->wCmd, &pcServerVersionResp, (char*)pPCServerVersionResp, sizeof(pcServerVersionResp));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCServerVersionResp EnCode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCServerVersionResp::wCmd;
	pmsgHeader->PackType = 0;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);
	return 0;
}

int CDataProc::ProcPatchFileInfoReq(CCliSession *pSession, char *pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPPatchFileInfoReq, pCPPatchFileInfoReq, pData, usDataLen, m_Protocol);

	CPatchFile* pPatchFile = m_PatchFileMng->GetPatchFileByVersion(pCPPatchFileInfoReq->Version.VersionMajor, pCPPatchFileInfoReq->Version.VersionMin, pCPPatchFileInfoReq->Version.VersionAddition);
	if (pPatchFile == 0)
	{
		pPatchFile = m_PrePatchMng->GetPatchFileByVersion(pCPPatchFileInfoReq->Version.VersionMajor, pCPPatchFileInfoReq->Version.VersionMin, pCPPatchFileInfoReq->Version.VersionAddition);
		if (pPatchFile == 0 && pSession->CanVerify())
		{
			//验证Ip，如果允许，才让访问验证目录
			pPatchFile = m_CheckPatchMng->GetPatchFileByVersion(pCPPatchFileInfoReq->Version.VersionMajor, pCPPatchFileInfoReq->Version.VersionMin, pCPPatchFileInfoReq->Version.VersionAddition);
		}
		if (pPatchFile == 0)
		{
			//发送错误消息
			ProcError(pSession, PatchServer::PATCH_NOTEXIST);
			return -1;
		}
	}
	PatchServer::PCPatchFileInfoResp pcPatchFileInfoResp;
	memset(&pcPatchFileInfoResp, 0 ,sizeof(PatchServer::PCPatchFileInfoResp));
	pcPatchFileInfoResp.Version.VersionMajor = pCPPatchFileInfoReq->Version.VersionMajor;
	pcPatchFileInfoResp.Version.VersionMin = pCPPatchFileInfoReq->Version.VersionMin;
	pcPatchFileInfoResp.Version.VersionAddition = pCPPatchFileInfoReq->Version.VersionAddition;
	pcPatchFileInfoResp.FileSize = pPatchFile->GetFileSize();
	pcPatchFileInfoResp.FileHash.HashLen = MAX_HASHVALUE_LEN;
	memcpy(pcPatchFileInfoResp.FileHash.HashValue, pPatchFile->GetFileKeys()->GetFileKey()->GetKey(), MAX_HASHVALUE_LEN);
	pcPatchFileInfoResp.HashCount = pPatchFile->GetPartCount();
	for (uint16 nIndex = 0; nIndex < pPatchFile->GetPartCount(); nIndex++)
	{
		pcPatchFileInfoResp.PartHash[nIndex].HashLen = MAX_HASHVALUE_LEN;
		memcpy(pcPatchFileInfoResp.PartHash[nIndex].HashValue, pPatchFile->GetFileKeys()->GetKeyByPart(nIndex)->GetKey(), MAX_HASHVALUE_LEN);
	}

	//向客户端发送回应
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCPatchFileInfoResp) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCPatchFileInfoResp* pPCPatchFileInfoResp  = reinterpret_cast<PatchServer::PCPatchFileInfoResp*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(pPCPatchFileInfoResp->wCmd, &pcPatchFileInfoResp, (char*)pPCPatchFileInfoResp, sizeof(pcPatchFileInfoResp));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCPatchFileInfoResp EnCode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCPatchFileInfoResp::wCmd;
	pmsgHeader->PackType = 0;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);

	return 0;
}

int CDataProc::ProcDownload(CCliSession *pSession, char *pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPDownloadReq, pCPDownloadReq, pData, usDataLen, m_Protocol);
	CPatchFile* pPatchFile = m_PatchFileMng->GetPatchFileByVersion(pCPDownloadReq->Version.VersionMajor, pCPDownloadReq->Version.VersionMin, pCPDownloadReq->Version.VersionAddition);

	if (pPatchFile == 0)
	{
		pPatchFile = m_PrePatchMng->GetPatchFileByVersion(pCPDownloadReq->Version.VersionMajor, pCPDownloadReq->Version.VersionMin, pCPDownloadReq->Version.VersionAddition);
		if (pPatchFile == 0 && pSession->CanVerify())
		{
			//验证Ip，如果允许，才让访问验证目录
			pPatchFile = m_CheckPatchMng->GetPatchFileByVersion(pCPDownloadReq->Version.VersionMajor, pCPDownloadReq->Version.VersionMin, pCPDownloadReq->Version.VersionAddition);
		}
		if (pPatchFile == 0)
		{
			//发送错误消息
			ProcError(pSession, PatchServer::PATCH_NOTEXIST);
			return -1;
		}
	}
	
	if (pCPDownloadReq->EndPos < pCPDownloadReq->StartPos)
	{
		//发送错误消息
		CUtil::Msg("%s\n", "ProcDownload end pos < start pos.");
		return -1;
	}

	if (pCPDownloadReq->EndPos - pCPDownloadReq->StartPos > pPatchFile->GetFileSize())
	{
		//发送错误消息
		CUtil::Msg("%s\n", "ProcDownload end pos - start pos > file size.");
		return -1;
	}
	
	//考虑用一个单独的线程读取数据来进行优化
	uint32 nWantRead = pCPDownloadReq->EndPos - pCPDownloadReq->StartPos + 1;
	if (nWantRead > MAX_PATCHDATALEN)
	{
		nWantRead = MAX_PATCHDATALEN;
	}
	int nCompressFlag = 1;
	PatchServer::PCDownloadResp pcDownloadResp;
	memset(&pcDownloadResp, 0, sizeof(pcDownloadResp));
	pcDownloadResp.Version = pCPDownloadReq->Version;
	pcDownloadResp.StartPos = pCPDownloadReq->StartPos;

	//对下载的数据进行压缩
	char szBuf[MAX_PATCHDATALEN];
	uint32 nDataLen = pPatchFile->ReadData(pCPDownloadReq->StartPos, szBuf, nWantRead);
	pcDownloadResp.DataLen = nDataLen;
	int nCompressRet = compress((Bytef*)(pcDownloadResp.PatchData), (uLongf*)&(pcDownloadResp.DataLen), (Bytef*)szBuf, nDataLen);
	if (nCompressRet != Z_OK)
	{
		memcpy(pcDownloadResp.PatchData, szBuf, nDataLen);
		pcDownloadResp.DataLen = nDataLen;
		nCompressFlag = 0;
	}
	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCDownloadResp) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCDownloadResp* pPCDownloadResp  = reinterpret_cast<PatchServer::PCDownloadResp*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(PatchServer::PCDownloadResp::wCmd, &pcDownloadResp, (char*)pPCDownloadResp, sizeof(pcDownloadResp));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCDownloadResp encode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCDownloadResp::wCmd;
	pmsgHeader->PackType = nCompressFlag;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);

	return 0;
}

void CDataProc::AddData(char* pData)
{
	GSEGuard gseGuard(&m_InputLock);
	m_NetDatas.push_back(pData);
#ifdef WIN32 
	m_QueSem.Release();
#else
	pthread_cond_signal(&m_Cond);
#endif
}

void CDataProc::ThreadProc() throw(GSEException)
{
	while (!m_bQuit)
	{
#ifdef WIN32
		m_QueSem.Wait(INFINITE);
#else
		pthread_mutex_lock(&m_Mutex);
		if(m_NetDatas.size() == 0)
		{
			pthread_cond_wait(&m_Cond, &m_Mutex);
			pthread_mutex_unlock(&m_Mutex);
		}
		else
		{
			pthread_mutex_unlock(&m_Mutex);			
		}
#endif
		GSEGuard gseGuard(&m_InputLock);
		while (m_NetDatas.size())
		{
			char* pData = m_NetDatas.front();
			m_NetDatas.pop_front();
			ProcData(pData);
			GSEMemFree(pData);
		}
	}
	return;
}

void CDataProc::Terminate() throw(GSEException)
{
	m_bQuit = true;
}

int CDataProc::ProcData(char *pData)
{
	int nRet = 0;
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pData);
	HANDLERMAP::iterator Ite = m_ProcHandler.find(pmsgHeader->PackCmd);
	if (Ite != m_ProcHandler.end())
	{
		unsigned long ulPtr = *((unsigned long*)(pData + pmsgHeader->PackLen));
		CCliSession* pCliSession = reinterpret_cast<CCliSession*>(ulPtr);
		nRet = (this->*Ite->second.handler)(pCliSession, pData + sizeof(MSGHEADER), pmsgHeader->PackLen - sizeof(MSGHEADER));
	}
	return nRet;
}

int CDataProc::ProcConn(CCliSession *pSession, char *pData, unsigned short usDataLen)
{
	CUtil::Msg("%s connection arrived.\n", pSession->CliIp().c_str());
	return 0;
}

int CDataProc::ProcClose(CCliSession *pSession, char *pData, unsigned short usDataLen)
{
	CUtil::Msg("%s connection close.\n", pSession->CliIp().c_str());
	GSEMemDelete pSession;
	return 0;
}

int CDataProc::ProcError(CCliSession* pSession, unsigned short usErrorID)
{
	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCError) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	pmsgHeader->PackCmd = PatchServer::PCError::wCmd;
	pmsgHeader->PackType = 0;
	pmsgHeader->PackLen = sizeof(MSGHEADER);
	PatchServer::PCError* pPCError = reinterpret_cast<PatchServer::PCError*>(pmsgHeader + sizeof(MSGHEADER));
	pPCError->ErrID = usErrorID;
	pSession->AddData(pPack);
	return 0;
}

int CDataProc::ProcDownloadFile(CCliSession* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPDownloadFile, pCPDownloadFile, pData, usDataLen, m_Protocol);
	FILE* pFile = NULL;
	pFile = fopen(pCPDownloadFile->FileName, "rb");
	if (pFile == NULL)
	{
		//发送错误消息
		ProcError(pSession, PatchServer::PATCH_NOTEXIST);
	}
	uint32 nWantRead = pCPDownloadFile->DataLen > MAX_PATCHDATALEN ? MAX_PATCHDATALEN : pCPDownloadFile->DataLen;
	PatchServer::PCDownloadFile pcDownloadFile;
	memset(&pcDownloadFile, 0, sizeof(PatchServer::PCDownloadFile));
	pcDownloadFile.FileNameLen = pCPDownloadFile->FileNameLen;
	memcpy(pcDownloadFile.FileName, pCPDownloadFile->FileName, pCPDownloadFile->FileNameLen);
	pcDownloadFile.Offset1 = pCPDownloadFile->Offset1;
	pcDownloadFile.Offset2 = pCPDownloadFile->Offset2;
	//
	//对下载的数据进行压缩
	char szBuf[MAX_PATCHDATALEN];
	fseek(pFile, pCPDownloadFile->Offset2, SEEK_SET);
	nWantRead = fread(szBuf, 1, nWantRead, pFile);

	fclose(pFile);
	pcDownloadFile.DataLen = nWantRead;
	int nCompressFlag = 1;
	int nCompressRet = compress((Bytef*)(pcDownloadFile.FileData), (uLongf*)&(pcDownloadFile.DataLen), (Bytef*)szBuf, nWantRead);
	if (nCompressRet != Z_OK)
	{
		memcpy(pcDownloadFile.FileData, szBuf, nWantRead);
		pcDownloadFile.DataLen = nWantRead;
		nCompressFlag = 0;
	}
	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCDownloadFile) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCDownloadFile* pPCDownloadFile = reinterpret_cast<PatchServer::PCDownloadFile*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(PatchServer::PCDownloadFile::wCmd, &pcDownloadFile, (char*)pPCDownloadFile, sizeof(pcDownloadFile));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCDownloadResp encode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCDownloadFile::wCmd;
	pmsgHeader->PackType = nCompressFlag;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);

	return 0;
}

#define LAUNCHERVERSION_FILENAME "LauncherVersion.txt"
#define LUANCHERUPDATE_FILENAME "LauncherUpdate.exe"

int CDataProc::ProcLuancherVersionReq(CCliSession *pSession, char *pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPVerifyLauncherVersion, pCPVerifyLauncherVersion, pData, usDataLen, m_Protocol);
	
	PatchServer::PCLauncherVersionInfo pcLauncherVersionInfo;
	memset(&pcLauncherVersionInfo, 0, sizeof(PatchServer::PCLauncherVersionInfo));
	string strFileName;
	if (pCPVerifyLauncherVersion->ReqType == 0)
	{
		strFileName = LAUNCHERVERSION_FILENAME;
	}
	else
	{
		strFileName = LUANCHERUPDATE_FILENAME;
	}
	pcLauncherVersionInfo.FileNameLen = strFileName.length() + 1;
	strcpy(pcLauncherVersionInfo.FileName, strFileName.c_str());
	FILE* pFile = 0;
	pFile = fopen(strFileName.c_str(), "rb");
	if (pFile == 0)
	{
		CUtil::Msg("launcher version not found.");
		return 0;
	}
	fseek(pFile, 0, SEEK_SET);
	int nPos = ftell(pFile);
	fseek(pFile, 0, SEEK_END);
	pcLauncherVersionInfo.FileSize = ftell(pFile) - nPos;
	fclose(pFile);
	pFile = NULL;
	
	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCLauncherVersionInfo) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCLauncherVersionInfo* pPCLauncherVersionInfo = reinterpret_cast<PatchServer::PCLauncherVersionInfo*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(PatchServer::PCLauncherVersionInfo::wCmd, &pcLauncherVersionInfo, (char*)pPCLauncherVersionInfo, sizeof(pcLauncherVersionInfo));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCDownloadResp encode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCLauncherVersionInfo::wCmd;
	pmsgHeader->PackType = 0;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);

	return 0;
}

int CDataProc::ProcHeatBeat(CCliSession* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPHeartBeat, pCPHeartBeat, pData, usDataLen, m_Protocol);

	return 0;
}

int CDataProc::ProcBtServerInfoReq(CCliSession *pSession, char *pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPReqBtServerInfo, pCPReqBtServerInfo, pData, usDataLen, m_Protocol);
	PatchServer::PCBtServerInfo pcBtServerInfo;
	memset(&pcBtServerInfo, 0, sizeof(PatchServer::PCBtServerInfo));
	pcBtServerInfo.UdpXServerLen = g_Configuration.m_UdpXServer.length();
	strncpy(pcBtServerInfo.UdpXServer, g_Configuration.m_UdpXServer.c_str(), g_Configuration.m_UdpXServer.length());
	pcBtServerInfo.UdpEServerLen = g_Configuration.m_UdpEServer.length();
	strncpy(pcBtServerInfo.UdpEServer, g_Configuration.m_UdpEServer.c_str(), g_Configuration.m_UdpEServer.length());
	pcBtServerInfo.StatisticServerLen = g_Configuration.m_StatisticServerIp.length();
	strncpy(pcBtServerInfo.StatisticServer, g_Configuration.m_StatisticServerIp.c_str(), g_Configuration.m_StatisticServerIp.length());
	pcBtServerInfo.StatisticServerPort = g_Configuration.m_StatisticServerPort;
	pcBtServerInfo.TrackerServerLen = g_Configuration.m_TrackServer.length();
	strncpy(pcBtServerInfo.TrackerServer, g_Configuration.m_TrackServer.c_str(), g_Configuration.m_TrackServer.length());

	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCBtServerInfo) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCBtServerInfo* pPCBtServerInfo = reinterpret_cast<PatchServer::PCBtServerInfo*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(PatchServer::PCBtServerInfo::wCmd, &pcBtServerInfo, (char*)pPCBtServerInfo, sizeof(pcBtServerInfo));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCBtServerInfo encode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCBtServerInfo::wCmd;
	pmsgHeader->PackType = 0;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);

	return 0;
}

int CDataProc::ProcTorrentInfoReq(CCliSession *pSession, char *pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPReqTorrentInfo, pCPReqTorrentInfo, pData, usDataLen, m_Protocol);

	string strTorrentUrl;
	string strFileUrl;
	CPatchFile* pPatchFile = NULL;
	pPatchFile = m_PatchFileMng->GetPatchFileByVersion(pCPReqTorrentInfo->Version.VersionMajor, pCPReqTorrentInfo->Version.VersionMin, pCPReqTorrentInfo->Version.VersionAddition);
	if (pPatchFile == NULL)
	{
		pPatchFile = m_PrePatchMng->GetPatchFileByVersion(pCPReqTorrentInfo->Version.VersionMajor, pCPReqTorrentInfo->Version.VersionMin, pCPReqTorrentInfo->Version.VersionAddition);
		if (pPatchFile == NULL && pSession->CanVerify())
		{
			//验证Ip，如果允许，才让访问验证目录
			pPatchFile = m_CheckPatchMng->GetPatchFileByVersion(pCPReqTorrentInfo->Version.VersionMajor, pCPReqTorrentInfo->Version.VersionMin, pCPReqTorrentInfo->Version.VersionAddition);
			if (pPatchFile == NULL)
			{
				//发送错误消息
				ProcError(pSession, PatchServer::PATCH_NOTEXIST);
				return -1;
			}
			else
			{
				strTorrentUrl = g_Configuration.m_CheckPatchTorrentUrl;
				strFileUrl = g_Configuration.m_CheckPatchFileUrl;

			}
		}
		else
		{
			strTorrentUrl = g_Configuration.m_PrepPatchTorrentUrl;
			strFileUrl = g_Configuration.m_PrepPatchFileUrl;
		}
	}
	else
	{
		strTorrentUrl = g_Configuration.m_PatchTorrentUrl;
		strFileUrl = g_Configuration.m_PatchFileUrl;
	}

	PatchServer::PCTorrentInfoResp pcTorrentInfoResp;
	memset(&pcTorrentInfoResp, 0, sizeof(pcTorrentInfoResp));

	pcTorrentInfoResp.Version = pCPReqTorrentInfo->Version;
	pcTorrentInfoResp.FileSize = pPatchFile->GetFileSize();
	pcTorrentInfoResp.TorrentUrlLen = strTorrentUrl.length();
	strncpy(pcTorrentInfoResp.TorrentUrl, strTorrentUrl.c_str(), strTorrentUrl.length());

	pcTorrentInfoResp.PatchUrlLen = strFileUrl.length();
	strncpy(pcTorrentInfoResp.PatchUrl, strFileUrl.c_str(), strFileUrl.length());

	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCTorrentInfoResp) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCTorrentInfoResp* pPCTorrentInfoResp = reinterpret_cast<PatchServer::PCTorrentInfoResp*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(PatchServer::PCTorrentInfoResp::wCmd, &pcTorrentInfoResp, (char*)pPCTorrentInfoResp, sizeof(pcTorrentInfoResp));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCBtServerInfo encode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCTorrentInfoResp::wCmd;
	pmsgHeader->PackType = 0;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);
	
	return 0;
}

int CDataProc::ProcCliVerifyReq(CCliSession* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPCliVerifyReq, pCPCliVerifyReq, pData, usDataLen, m_Protocol);
	
	PatchServer::PCCliVerifyResp pcCliVerifyResp;
	memset(&pcCliVerifyResp, 0, sizeof(pcCliVerifyResp));
	pcCliVerifyResp.FileNameLen = g_Configuration.m_CliHashFileName.length();
	strncpy(pcCliVerifyResp.FileName, g_Configuration.m_CliHashFileName.c_str(), pcCliVerifyResp.FileNameLen);
	pcCliVerifyResp.CliPathLen = g_Configuration.m_ClientPath.length();
	strncpy(pcCliVerifyResp.CliPath, g_Configuration.m_ClientPath.c_str(), pcCliVerifyResp.CliPathLen);
	
	uint64 nFileSize = 0;

	//获得文件大小
	FILE* pFile = NULL;
	pFile = fopen(pcCliVerifyResp.FileName, "rb");
	if (!pFile)
	{
		//报告错误
		CUtil::Msg("%s %d.\n", "open file error", errno);
		return -1;
	}
	try
	{
		fseek(pFile, 0, SEEK_SET);
		uint32 nPos = ftell(pFile);
		fseek(pFile, 0, SEEK_END);
		nFileSize = ftell(pFile) - nPos;
	}
	catch (...)
	{

	}
	fclose(pFile);
	pFile = NULL;

	pcCliVerifyResp.FileSize = nFileSize;

	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCCliVerifyResp) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCCliVerifyResp* pPCCliVerifyResp = reinterpret_cast<PatchServer::PCCliVerifyResp*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(PatchServer::PCCliVerifyResp::wCmd, &pcCliVerifyResp, (char*)pPCCliVerifyResp, sizeof(pcCliVerifyResp));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCBtServerInfo encode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCCliVerifyResp::wCmd;
	pmsgHeader->PackType = 0;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);

	return 0;
}

int CDataProc::ProcReqFileInfo(CCliSession *pSession, char *pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(PatchServer::CPQueryFileInfo, pCPQueryFileInfo, pData, usDataLen, m_Protocol);

	PatchServer::PCFileInfoResp pcFileInfoResp;
	memset(&pcFileInfoResp, 0, sizeof(pcFileInfoResp));
	pcFileInfoResp.FileNameLen = pCPQueryFileInfo->FileNameLen;
	strncpy(pcFileInfoResp.FileName, pCPQueryFileInfo->FileName, pCPQueryFileInfo->FileNameLen);

	int64 nFileSize = 0;
	//获得文件大小
	FILE* pFile = NULL;
	pFile = fopen(pCPQueryFileInfo->FileName, "rb");
	if (!pFile)
	{
		//报告错误
		CUtil::Msg("%s %d.\n", "open file error", errno);
		return -1;
	}
	try
	{
		fseek(pFile, 0, SEEK_SET);
		uint32 nPos = ftell(pFile);
		fseek(pFile, 0, SEEK_END);
		nFileSize = ftell(pFile) - nPos;
	}
	catch (...)
	{

	}
	fclose(pFile);
	pFile = NULL;

	pcFileInfoResp.FileSize = nFileSize;

	//回应客户端
	char* pPack = GSEMemAlloc(char, sizeof(PatchServer::PCFileInfoResp) + sizeof(MSGHEADER));
	PMSGHEADER pmsgHeader = reinterpret_cast<PMSGHEADER>(pPack);
	PatchServer::PCFileInfoResp* pPCFileInfoResp = reinterpret_cast<PatchServer::PCFileInfoResp*>(pPack + sizeof(MSGHEADER));
	size_t nEnCodeLen = m_Protocol.EnCode(PatchServer::PCFileInfoResp::wCmd, &pcFileInfoResp, (char*)pPCFileInfoResp, sizeof(pcFileInfoResp));
	if (nEnCodeLen == (size_t)-1)
	{
		GSEMemFree(pPack);
		CUtil::Msg("%s\n", "PCBtServerInfo encode failed.");
		return 0;
	}
	pmsgHeader->PackCmd = PatchServer::PCFileInfoResp::wCmd;
	pmsgHeader->PackType = 0;
	pmsgHeader->PackLen = nEnCodeLen + sizeof(MSGHEADER);
	pSession->AddData(pPack);

	return 0;
}















