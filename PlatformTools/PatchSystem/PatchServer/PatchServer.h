﻿#ifndef PATCHSERVER_H 
#define PATCHSERVER_H
#include "../Common/Common.h"
#include "Configuration.h"
#include "../Common/PatchFileMng.h"
#include "SessionMng.h"
#include "PatchMonitor.h"
#include "DataProc.h"
#include "GSECommunication.h"
#include "GSECBlockingTCPServer.h"
#include "GSEErrorHandling.h"
#include "GSESystem.h"

class CPatchServer: public GSEMemoryObject
{
public:
	CPatchServer(void);
	virtual ~CPatchServer(void);
public:
	int Init();
	int Finit();
	int Run();
public:
	static void OnSignal(int nsignal);
private:
	void HookSignal();
	void UnHookSignal();
private:
	CPatchFileMng m_PatchFileMng;
	CPatchFileMng m_PrepPatchFileMng;
	CPatchFileMng m_CheckPatchFileMng;
	CDataProc m_DataProc;
	CSessionMng m_SessionMng;
	static bool m_bQuit;
	CPatchMonitor* m_PatchMonitor;
};

#endif
