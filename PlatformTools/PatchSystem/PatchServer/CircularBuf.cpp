﻿#include "CircularBuf.h"

CCircularBuf::CCircularBuf(uint32 nSize)
: m_DataSize(nSize), m_ReadPtr(0), m_WritePtr(0), m_Count(0)
{
	m_Data = new char[nSize];
}

CCircularBuf::~CCircularBuf(void)
{
	delete [] m_Data;
}

int CCircularBuf::ReadData(char* pDest, uint32 nDataLen)
{
	//判断溢出
	if (nDataLen > m_Count)
	{
		return -1;
	}
	
	if (nDataLen + m_ReadPtr > m_DataSize)
	{
		uint32 nPart = m_DataSize - m_ReadPtr;
		memcpy(pDest, m_Data + m_ReadPtr, nPart);
		memcpy(pDest + nPart, m_Data, nDataLen - nPart);
		m_ReadPtr = nDataLen - nPart;
	}
	else
	{
		memcpy(pDest, m_Data + m_ReadPtr, nDataLen);
		m_ReadPtr += nDataLen;
	}

	m_Count -= nDataLen;
	if (m_Count == 0)
	{
		m_ReadPtr = 0;
		m_WritePtr = 0;
	}
	return 0;
}

int CCircularBuf::WriteData(const char *pSrc, uint32 nDataLen)
{
	//判读溢出
	if (nDataLen + m_Count > m_DataSize)
	{
		return -1;
	}
	if (nDataLen + m_WritePtr > m_DataSize)
	{
		uint32 nPart = m_DataSize - m_WritePtr;
		memcpy(m_Data + m_WritePtr, pSrc, nPart);
		memcpy(m_Data, pSrc + nPart, nDataLen - nPart);
		m_WritePtr = nDataLen - nPart;
	}
	else
	{
		memcpy(m_Data + m_WritePtr, pSrc, nDataLen);
		m_WritePtr += nDataLen;
	}
	m_Count += nDataLen;
	return 0;
}

uint32 CCircularBuf::GetFreeSize()
{
	return m_DataSize - m_Count;
}

uint32 CCircularBuf::GetUsedSize()
{
	return m_Count;
}

char* CCircularBuf::GetReadPtr()
{
	return m_Data + m_ReadPtr;
}

char* CCircularBuf::GetWritePtr()
{
	return m_Data + m_WritePtr;
}

int CCircularBuf::UpdateWrite(uint32 nLen)
{
	if (nLen + m_Count > m_DataSize)
	{
		return -1;
	}

	if (nLen + m_WritePtr > m_DataSize)
	{
		uint32 nPart = m_DataSize - m_WritePtr;
		m_WritePtr = nLen - nPart;
	}
	else
	{
		m_WritePtr += nLen;
	}
	m_Count += nLen;
	return 0;
}

int CCircularBuf::UpdateRead(uint32 nLen)
{
	if (nLen > m_Count)
	{
		return -1;
	}

	if (nLen + m_ReadPtr > m_DataSize)
	{
		uint32 nPart = m_DataSize - m_ReadPtr;
		m_ReadPtr = nLen - nPart;
	}
	else
	{
		m_ReadPtr += nLen;
	}
	m_Count -= nLen;
	if (m_Count == 0)
	{
		m_ReadPtr = m_WritePtr = 0;
	}
	return 0;
}

uint32 CCircularBuf::GetContiguousFreeSize(int nType)
{
	uint32 nLen = 0;
	//读
	if (nType == 0)
	{
		if (m_ReadPtr < m_WritePtr)
		{
			nLen = m_WritePtr - m_ReadPtr;
			assert(nLen == m_Count);
		}
		else if (m_ReadPtr == m_WritePtr)
		{
			if (m_Count != 0)
			{
				nLen = m_DataSize - m_ReadPtr;
			}
		}
		else
		{
			nLen = m_DataSize - m_ReadPtr;
		}
	}
	//写
	else
	{
		if (m_WritePtr > m_ReadPtr)
		{
			nLen = m_DataSize - m_WritePtr;
		}
		else if (m_WritePtr == m_ReadPtr)
		{
			if (m_Count != 0)
			{
				nLen = m_DataSize - m_WritePtr;
			}
			else
			{
				nLen = m_DataSize;
			}
		}
		else
		{
			nLen = m_ReadPtr - m_WritePtr;
		}
	}
	return nLen;
}

int CCircularBuf::SoftReadData(char *pDest, uint32 nDataLen)
{
	//判断溢出
	if (nDataLen > m_Count)
	{
		return -1;
	}

	if (nDataLen + m_ReadPtr > m_DataSize)
	{
		uint32 nPart = m_DataSize - m_ReadPtr;
		memcpy(pDest, m_Data + m_ReadPtr, nPart);
		memcpy(pDest + nPart, m_Data, nDataLen - nPart);
	}
	else
	{
		memcpy(pDest, m_Data + m_ReadPtr, nDataLen);
	}
	return 0;
}




