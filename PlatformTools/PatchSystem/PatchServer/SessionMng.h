﻿#ifndef SESSIONMNG_H
#define SESSIONMNG_H
#include "GSECommunication.h"
#include "GSECBlockingTCPServer.h"
#include "GSEErrorHandling.h"
#include "GSESystem.h"
#include "Configuration.h"
#include "CliSession.h"
#include "GSEPool.h"
#include "GSEThreadProc.h"

//负责创建CCliSession，维护CCliSession的生命周期
class CNetHandler;
class CDataProc;

class CSessionMng: public GSEThreadProc
{
public:
	CSessionMng();
	virtual ~CSessionMng(void);
public:
	int Init();
	int Finit();
	void SetDataProc(CDataProc* pDataProc);
public:
	virtual void ThreadProc(void) throw(GSEException);
	virtual void Terminate() throw(GSEException);
	virtual void Clear() throw(GSEException);
public:
	int OnStart(GSECommunication* pSvrCommunication);
	int OnStop(GSECommunication* pSvrCommunication);
	int OnConnection(GSECommunication* pPeerConnection);
	int OnDestroyConnection(GSECommunication* pPeerConnection);
	int OnClose(GSECommunication* pPeerConnection);
	int OnError(GSECommunication* pPeerConnection, int nErrCode);
	int OnRecv(GSECommunication *pkCommunication, char *pszData, int nLen);
	int OnSend(GSECommunication *pkCommunication, char *pszData, int nLen);
	bool OnReadMask(GSECommunication* pkCommunication);
	bool OnWriteMask(GSECommunication* pkCommunication);
	bool OnErase(GSECommunication *pkCommunication);
	int OnInitRecv(GSECommunication* pkCommunication, char*& pszBuf, int& nLen);
	int OnInitSend(GSECommunication* pkCommunication, char*& pszBuf, int& nLen);
private:
	CNetHandler* m_NetHandler;
	GSECommunicationApplicationServer* m_PatchServer;
    GSEPool<GSECommunication*, CCliSession*>* m_SessionMap;	
	GSECommunication* m_SvrCommunication;
	GSELock m_ConnectionLock;
	CDataProc* m_DataProc;
	GSEThread* m_NetThread;
};

#endif
