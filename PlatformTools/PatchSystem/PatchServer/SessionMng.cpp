﻿#include "SessionMng.h"
#include "NetHandler.h"
#include "Configuration.h"
#include "GSECommunicationInitOptions.h"
#include "GSESystemInitOptions.h"
#include "GSEGuard.h"
#include "GSEPoolHash.h"
#include "GSEPoolHash.cpp"
#include "GSESingleton.h"

#include "DataProc.h"

//错误输出
GSEINIT_SYSTEM(GSEERROR_SCREEN_MASK);
//socket初始化
GSEINIT_COMMUNICATION();

CSessionMng::CSessionMng(void)
: m_NetHandler(0), m_PatchServer(0), m_SvrCommunication(0), m_NetThread(0)
{
	
}

CSessionMng::~CSessionMng(void)
{
	
}

int CSessionMng::Init()
{

	m_NetHandler = GSEMemNew CNetHandler(*this);
	m_SessionMap = GSEMemNew GSEPoolHash<GSECommunication*, CCliSession*>();
	m_PatchServer = GSECCreateApplicationServer(m_NetHandler, GSECOMMUNICATION_SELECT);
	m_NetThread = GSECreateThread(this, false);
	return 0;
}

int CSessionMng::Finit()
{
	m_NetHandler->Terminate();
	Terminate();
	GSEDestroyThread(m_NetThread);
	m_NetThread = 0;
	GSECDestroyApplication(m_PatchServer);
	m_PatchServer = 0;
	GSEMemDelete m_NetHandler;
	m_NetHandler = 0;
	GSEMemDelete m_SessionMap;
	return 0;
}

int CSessionMng::OnStart(GSECommunication* pSvrCommunication)
{
	if (m_SvrCommunication == 0)
	{
		m_SvrCommunication = pSvrCommunication;
	}
	else
	{

	}
	return 0;
}

int CSessionMng::OnStop(GSECommunication *pSvrCommunication)
{
	if (m_SvrCommunication)
	{
		m_SvrCommunication = 0;
	}
	return 0;
}

int CSessionMng::OnConnection(GSECommunication* pPeerConnection)
{
	sockaddr_in peeraddr;
	memset(&peeraddr, 0, sizeof(peeraddr));
	int nAddrLen = sizeof(sockaddr_in);
#ifndef WIN32
	getpeername(pPeerConnection->GetSocket(), (sockaddr*)&peeraddr, (socklen_t*)&nAddrLen);
#else
	getpeername(pPeerConnection->GetSocket(), (sockaddr*)&peeraddr, &nAddrLen);
#endif
	CCliSession* pCliSession = GSEMemNew CCliSession(m_DataProc, pPeerConnection);
	pCliSession->CliIp(inet_ntoa(peeraddr.sin_addr));
	pCliSession->CanVerify(g_Configuration.IsAllowIp(pCliSession->CliIp().c_str()));
	GSEGuard gseGuard(&m_ConnectionLock);
	m_SessionMap->Append(pPeerConnection, pCliSession);
	pCliSession->OnConnect();
	return 0;
}

int CSessionMng::OnDestroyConnection(GSECommunication *pPeer)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Erase(pPeer);
	return 0;
}

int CSessionMng::OnClose(GSECommunication* pPeerConnection)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pPeerConnection);
	if (pCliSession)
	{
		pCliSession->OnClose();
	}
	return 0;
}

int CSessionMng::OnError(GSECommunication *pPeerConnection, int nErrCode)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pPeerConnection);
	if (pCliSession)
	{
		pCliSession->OnError(nErrCode);
	}
	return 0;
}

int CSessionMng::OnRecv(GSECommunication *pkCommunication, char *pszData, int nLen)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pkCommunication);
	if (pCliSession)
	{
		pCliSession->OnRecv(pszData, nLen);
	}

	return 0;
}

int CSessionMng::OnSend(GSECommunication *pkCommunication, char *pszData, int nLen)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pkCommunication);
	if (pCliSession)
	{
		pCliSession->OnSend(pszData, nLen);
	}

	return 0;
}

bool CSessionMng::OnErase(GSECommunication *pkCommunication)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pkCommunication);
	if (pCliSession)
	{
		return pCliSession->OnErase();
	}

	return false;
}

int CSessionMng::OnInitRecv(GSECommunication* pkCommunication, char*& pszBuf, int& nLen)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pkCommunication);
	if (pCliSession)
	{
		pCliSession->OnInitRecv(pszBuf, nLen);
	}

	return 0;
}

int CSessionMng::OnInitSend(GSECommunication* pkCommunication, char*& pszBuf, int& nLen)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pkCommunication);
	if (pCliSession)
	{
		pCliSession->OnInitSend(pszBuf, nLen);
	}
	
	return 0;
}

void CSessionMng::SetDataProc(CDataProc* pDataProc)
{
	m_DataProc = pDataProc;
}

void CSessionMng::ThreadProc() throw(GSEException)
{
	//获得Ip地址
	string strIp;
	uint16 nPort;
	g_Configuration.GetIpAddr(strIp, nPort);
	m_PatchServer->Start(strIp.c_str(), nPort);
}

void CSessionMng::Terminate() throw(GSEException)
{
	m_PatchServer->Stop();
}

void CSessionMng::Clear() throw(GSEException)
{

}

bool CSessionMng::OnReadMask(GSECommunication* pkCommunication)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pkCommunication);
	if (pCliSession)
	{
		return pCliSession->OnReadMask();
	}
	else
	{
		return false;
	}
}

bool CSessionMng::OnWriteMask(GSECommunication* pkCommunication)
{
	CCliSession* pCliSession = 0;
	GSEGuard gseGuard(&m_ConnectionLock);
	pCliSession = m_SessionMap->Find(pkCommunication);
	if (pCliSession)
	{
		return pCliSession->OnWriteMask();
	}
	else
	{
		return false;
	}
}





