﻿#ifndef CLISESSION_H
#define CLISESSION_H
#include "../Common/Common.h"
#include "GSEMemoryObject.h"
#include "GSELock.h"

//前置声明
class CDataProc;
class CCircularBuf;
class GSECommunication;
//负责简要的封装底层网络模块接口
class CCliSession: public GSEMemoryObject
{
public:
	CCliSession(CDataProc* pDataProc, GSECommunication* pCommucation);
	virtual ~CCliSession(void);

public:
	void UnUsed();
public:
	int OnClose();
	int OnConnect();
	int OnError(int nErrorCode);
	int OnRecv(char *pszData, int nLen);
	int OnSend(char *pszData, int nLen);
	int OnInitSend(char*& pszBuf, int& nLen);
	int OnInitRecv(char*& pszBuf, int& nLen);
	bool OnReadMask();
	bool OnWriteMask();
	bool OnErase();
	void AddData(char* pData);
public:
	void CanVerify(bool bVerify);
	bool CanVerify();
	void CliIp(string strIp);
	string CliIp();
private:
	int ParseData();
private:
	bool volatile m_CommunicationUsed;
	CDataProc* m_DataProc;
	GSECommunication* m_Communication;
	CCircularBuf* m_InBuf; //接收数据
	CCircularBuf* m_OutBuf; //发送数据
	list<char*> m_SendQue;
	GSELock m_SendQueLock;
	bool m_bCanVerify; //能不能获取待验证版本
	string m_CliIp;
	time_t m_LastRecvTime;
};

#endif
