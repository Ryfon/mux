﻿/*
监控Patch目录变化，如果有新Patch生成，自动加入到列表中
*/
#ifndef PATCHMONITOR_H
#define PATCHMONITOR_H
#include "../Common/Common.h"
#include "../Common/PatchFileMng.h"
#include "GSEThreadProc.h"
#include "GSEThread.h"
#include "GSEThreadFactory.h"

struct MONITOR_OBJ 
{
	string strFilePath;
#ifdef WIN32
	HANDLE nObj;
#else
	int nObj;
#endif	
	CPatchFileMng* pMng;
};

class CPatchMonitor: public GSEThreadProc
{
public:
	CPatchMonitor();
	virtual ~CPatchMonitor(void);
public:
	int Start();
	int Stop();
public:
	void SetPatchPath(string strPath, CPatchFileMng* pMng);
public:
	virtual void ThreadProc() throw(GSEException);
	virtual void Terminate() throw(GSEException);
private:
	int AddPatchFile(const char* szFileName, int nIndex);
	int DelPatchFile(const char* szFileName, int nIndex);
#ifdef WIN32
	int FindMonitorIndex(HANDLE nObj);
#else
	int FindMonitorIndex(int nObj);
#endif

private:
	vector<MONITOR_OBJ> m_MonitorObjs;
	GSEThread* m_MonitorThread;
#ifndef WIN32 
	int m_hPatchDir; 
#endif
	bool m_bQuit;
};

#endif






