﻿#ifndef			__PatchSrvProtocol__
#define			__PatchSrvProtocol__
#include "ParserTool.h"
#include "map"

namespace PatchServer
{
	//Enums
	typedef	enum
	{
		PATCH_TOOMANY 	=	1001,         	//需要下载的补丁太多
		PATCH_NOTEXIST	=	1002,         	//补丁不存在
	}PatchOpCode;


	//Defines
	#define	MAX_PATCH_SIZE   	 	100  	//最大补丁个数
	#define	MAX_HASHVALUE_LEN	 	16   	//最大hash字符个数 MD5长度
	#define	MAX_PATCHDATALEN 	 	10240	//下载的最大数据长度
	#define	MAX_PART_COUNT   	 	1024 	//最大块数
	#define	MAX_FILE_NAME    	 	260  	//文件名最大长度
	#define	MAX_URL_LEN      	 	50   	//url最大长度

	//Typedefs

	//Types
	template<bool> struct TypeMaxBytes;
	template<> struct TypeMaxBytes<true> {};
	#define	PATCHPROTOCOL_MAXBYTES_CHECK(typename) \
	struct type##typename \
	{\
		TypeMaxBytes< sizeof(typename) < 40960 > struct_larger_##typename;\
	}

	struct MsgHeader
	{
		USHORT      	PackLen;                   	//消息包长度（包含消息头在内）
		BYTE        	PackType;                  	//包类型
		USHORT      	PackCmd;                   	//消息命令字
	};

	struct Hash
	{
		BYTE        	HashLen;                   	//Hash长度
		BYTE        	HashValue[MAX_HASHVALUE_LEN];	//hash值
	};

	struct PatchVersion
	{
		BYTE        	VersionMajor;              	//主版本号
		BYTE        	VersionMin;                	//小版本号
		USHORT      	VersionAddition;           	//附加版本号
		Hash        	FileHash;                  	//文件Hash
	};

	struct CPServerVersionReq
	{
		static const USHORT	wCmd = 0x1000;

		PatchVersion	Version;                   	//版本
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPServerVersionReq);

	struct PCServerVersionResp
	{
		static const USHORT	wCmd = 0x9000;

		PatchVersion	Version;                   	//版本
		BYTE        	PatchNum;                  	//版本差异的补丁数目
		PatchVersion	PatchList[MAX_PATCH_SIZE];   	//需要下载的Patch列表
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCServerVersionResp);

	struct CPPatchFileInfoReq
	{
		static const USHORT	wCmd = 0x1001;

		PatchVersion	Version;                   	//版本
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPPatchFileInfoReq);

	struct PCPatchFileInfoResp
	{
		static const USHORT	wCmd = 0x9001;

		PatchVersion	Version;                   	//版本
		UINT        	FileSize;                  	//文件大小
		Hash        	FileHash;                  	//文件Hash
		USHORT      	HashCount;                 	//hash数目
		Hash        	PartHash[MAX_PART_COUNT];    	//hash列表
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCPatchFileInfoResp);

	struct CPDownloadReq
	{
		static const USHORT	wCmd = 0x1002;

		PatchVersion	Version;                   	//版本
		UINT        	StartPos;                  	//开始位置
		UINT        	EndPos;                    	//结束位置
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPDownloadReq);

	struct PCDownloadResp
	{
		static const USHORT	wCmd = 0x9002;

		PatchVersion	Version;                   	//版本
		UINT        	StartPos;                  	//开始位置
		UINT        	DataLen;                   	//数据长度
		CHAR        	PatchData[MAX_PATCHDATALEN]; 	//下载的数据
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCDownloadResp);

	struct PCHeartBeat
	{
		static const USHORT	wCmd = 0x9003;

		UINT        	HeartID;                   	//包序号
		UINT        	HeartTime;                 	//发包时间戳
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCHeartBeat);

	struct CPHeartBeat
	{
		static const USHORT	wCmd = 0x1003;

		UINT        	HeartID;                   	//包序号
		UINT        	HeartTime;                 	//发包时间戳
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPHeartBeat);

	struct PCError
	{
		static const USHORT	wCmd = 0x9999;

		UINT        	ErrID;                     	//错误码
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCError);

	struct CPCliVerifyReq
	{
		static const USHORT	wCmd = 0x1006;
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPCliVerifyReq);

	struct PCCliVerifyResp
	{
		static const USHORT	wCmd = 0x9006;

		UINT        	FileNameLen;               	//文件名长度
		CHAR        	FileName[MAX_FILE_NAME];     	//请求下载的文件名 相对路径
		UINT        	FileSize;                  	//文件大小
		BYTE        	CliPathLen;                	//客户端相对目录长度
		CHAR        	CliPath[MAX_FILE_NAME];      	//客户端相对目录
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCCliVerifyResp);

	struct CPDownloadFile
	{
		static const USHORT	wCmd = 0x1004;

		UINT        	FileNameLen;               	//文件名长度
		CHAR        	FileName[MAX_FILE_NAME];     	//请求下载的文件名 相对路径
		UINT        	Offset1;                   	//偏移量 pkg文件有用，其他文件都是0
		UINT        	Offset2;                   	//偏移量
		UINT        	DataLen;                   	//请求的数据大小
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPDownloadFile);

	struct PCDownloadFile
	{
		static const USHORT	wCmd = 0x9004;

		UINT        	FileNameLen;               	//文件名长度
		CHAR        	FileName[MAX_FILE_NAME];     	//请求下载的文件名 相对路径
		UINT        	Offset1;                   	//偏移量 pkg文件有用，其他文件都是0
		UINT        	Offset2;                   	//偏移量
		UINT        	DataLen;                   	//请求的数据大小
		CHAR        	FileData[MAX_PATCHDATALEN];  	//下载的数据
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCDownloadFile);

	struct CPVerifyLauncherVersion
	{
		static const USHORT	wCmd = 0x1005;

		UINT        	ReqType;                   	//类型 0 Luancher版本文件 1 Launcher更新包
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPVerifyLauncherVersion);

	struct PCLauncherVersionInfo
	{
		static const USHORT	wCmd = 0x9005;

		UINT        	ReqType;                   	//类型 0 Luancher版本文件 1 Launcher更新包
		UINT        	FileNameLen;               	//文件名长度
		CHAR        	FileName[MAX_FILE_NAME];     	//请求下载的文件名 相对路径
		UINT        	FileSize;                  	//文件大小
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCLauncherVersionInfo);

	struct CPQueryFileInfo
	{
		static const USHORT	wCmd = 0x1007;

		UINT        	FileNameLen;               	//文件名长度
		CHAR        	FileName[MAX_FILE_NAME];     	//请求下载的文件名 相对路径
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPQueryFileInfo);

	struct PCFileInfoResp
	{
		static const USHORT	wCmd = 0x9007;

		UINT        	FileNameLen;               	//文件名长度
		CHAR        	FileName[MAX_FILE_NAME];     	//请求下载的文件名 相对路径
		UINT        	FileSize;                  	//文件大小
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCFileInfoResp);

	struct CPReqBtServerInfo
	{
		static const USHORT	wCmd = 0x1008;
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPReqBtServerInfo);

	struct PCBtServerInfo
	{
		static const USHORT	wCmd = 0x9008;

		BYTE        	UdpXServerLen;             	//UdpXServer长度
		CHAR        	UdpXServer[MAX_URL_LEN];     	//注册服务器url
		BYTE        	UdpEServerLen;             	//UdpEServer长度
		CHAR        	UdpEServer[MAX_URL_LEN];     	//打洞服务器url
		BYTE        	StatisticServerLen;        	//统计服务器长度
		CHAR        	StatisticServer[MAX_URL_LEN];	//统计服务器Url
		USHORT      	StatisticServerPort;       	//统计服务器端口号
		BYTE        	TrackerServerLen;          	//TrackerServer长度
		CHAR        	TrackerServer[MAX_URL_LEN];  	//TrackerServer服务器
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCBtServerInfo);

	struct CPReqTorrentInfo
	{
		static const USHORT	wCmd = 0x1009;

		PatchVersion	Version;                   	//版本
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(CPReqTorrentInfo);

	struct PCTorrentInfoResp
	{
		static const USHORT	wCmd = 0x9009;

		PatchVersion	Version;                   	//版本
		UINT        	FileSize;                  	//补丁大小
		BYTE        	TorrentUrlLen;             	//TorrentUrl长度
		CHAR        	TorrentUrl[MAX_URL_LEN];     	//种子文件的目录
		BYTE        	PatchUrlLen;               	//PatchUrl长度
		CHAR        	PatchUrl[MAX_URL_LEN];       	//patch地址
	};
	PATCHPROTOCOL_MAXBYTES_CHECK(PCTorrentInfoResp);


	//Messages
	typedef enum Message_id_type
	{
		C_P_REQSERVERVERSION     	=	0x1000,	//CPServerVersionReq
		P_C_RESPSERVERVERSION    	=	0x9000,	//PCServerVersionResp
		C_P_REQPATCHFILEINFO     	=	0x1001,	//CPPatchFileInfoReq
		P_C_RESPPATCHFILEINFO    	=	0x9001,	//PCPatchFileInfoResp
		C_P_REQDOWNLOAD          	=	0x1002,	//CPDownloadReq
		P_C_RESPDOWNLOAD         	=	0x9002,	//PCDownloadResp
		P_C_HEARTBEAT            	=	0x9003,	//PCHeartBeat
		C_P_HEARTBEAT            	=	0x1003,	//CPHeartBeat
		P_C_ERROR                	=	0x9999,	//PCError
		C_P_DOWNLOADFILE         	=	0x1004,	//CPDownloadFile
		P_C_DOWNLOADFILE         	=	0x9004,	//PCDownloadFile
		C_P_VERIFYLUANCHERVERSION	=	0x1005,	//CPVerifyLauncherVersion
		P_C_LUANCHERVERSIONINFO  	=	0x9005,	//PCLauncherVersionInfo
		C_P_CLIVERIFYREQ         	=	0x1006,	//CPCliVerifyReq
		P_C_CLIVERIFYRESP        	=	0x9006,	//PCCliVerifyResp
		C_P_QUERYFILEINFO        	=	0x1007,	//CPQueryFileInfo
		P_C_FILEINFORESP         	=	0x9007,	//PCFileInfoResp
		C_P_REQBTSERVERINFO      	=	0x1008,	//CPReqBtServerInfo
		P_C_BTSERVERINFO         	=	0x9008,	//PCBtServerInfo
		C_P_REQTORRENTINFO       	=	0x1009,	//CPReqTorrentInfo
		P_C_TORRENTINFORESP      	=	0x9009,	//PCTorrentInfoResp
	};

	//Class Data
	class PatchSrvProtocol;
	typedef size_t (PatchSrvProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (PatchSrvProtocol::*DeCodeFunc)(void* pData);

	class 
	#ifdef WIN32
	_declspec(dllexport) 
	#endif
	PatchSrvProtocol
	{
	public:
		PatchSrvProtocol();
		~PatchSrvProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__MsgHeader(void* pData);
		size_t	DeCode__MsgHeader(void* pData);

		size_t	EnCode__Hash(void* pData);
		size_t	DeCode__Hash(void* pData);

		size_t	EnCode__PatchVersion(void* pData);
		size_t	DeCode__PatchVersion(void* pData);

		size_t	EnCode__CPServerVersionReq(void* pData);
		size_t	DeCode__CPServerVersionReq(void* pData);

		size_t	EnCode__PCServerVersionResp(void* pData);
		size_t	DeCode__PCServerVersionResp(void* pData);

		size_t	EnCode__CPPatchFileInfoReq(void* pData);
		size_t	DeCode__CPPatchFileInfoReq(void* pData);

		size_t	EnCode__PCPatchFileInfoResp(void* pData);
		size_t	DeCode__PCPatchFileInfoResp(void* pData);

		size_t	EnCode__CPDownloadReq(void* pData);
		size_t	DeCode__CPDownloadReq(void* pData);

		size_t	EnCode__PCDownloadResp(void* pData);
		size_t	DeCode__PCDownloadResp(void* pData);

		size_t	EnCode__PCHeartBeat(void* pData);
		size_t	DeCode__PCHeartBeat(void* pData);

		size_t	EnCode__CPHeartBeat(void* pData);
		size_t	DeCode__CPHeartBeat(void* pData);

		size_t	EnCode__PCError(void* pData);
		size_t	DeCode__PCError(void* pData);

		size_t	EnCode__CPCliVerifyReq(void* pData);
		size_t	DeCode__CPCliVerifyReq(void* pData);

		size_t	EnCode__PCCliVerifyResp(void* pData);
		size_t	DeCode__PCCliVerifyResp(void* pData);

		size_t	EnCode__CPDownloadFile(void* pData);
		size_t	DeCode__CPDownloadFile(void* pData);

		size_t	EnCode__PCDownloadFile(void* pData);
		size_t	DeCode__PCDownloadFile(void* pData);

		size_t	EnCode__CPVerifyLauncherVersion(void* pData);
		size_t	DeCode__CPVerifyLauncherVersion(void* pData);

		size_t	EnCode__PCLauncherVersionInfo(void* pData);
		size_t	DeCode__PCLauncherVersionInfo(void* pData);

		size_t	EnCode__CPQueryFileInfo(void* pData);
		size_t	DeCode__CPQueryFileInfo(void* pData);

		size_t	EnCode__PCFileInfoResp(void* pData);
		size_t	DeCode__PCFileInfoResp(void* pData);

		size_t	EnCode__CPReqBtServerInfo(void* pData);
		size_t	DeCode__CPReqBtServerInfo(void* pData);

		size_t	EnCode__PCBtServerInfo(void* pData);
		size_t	DeCode__PCBtServerInfo(void* pData);

		size_t	EnCode__CPReqTorrentInfo(void* pData);
		size_t	DeCode__CPReqTorrentInfo(void* pData);

		size_t	EnCode__PCTorrentInfoResp(void* pData);
		size_t	DeCode__PCTorrentInfoResp(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool                    m_kPackage;
	};
}
#endif			//__PatchSrvProtocol__
