﻿#include "PatchProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

PatchServer::PatchSrvProtocol::PatchSrvProtocol()
{
	m_mapEnCodeFunc[0x1000]	=	&PatchSrvProtocol::EnCode__CPServerVersionReq;
	m_mapDeCodeFunc[0x1000]	=	&PatchSrvProtocol::DeCode__CPServerVersionReq;

	m_mapEnCodeFunc[0x9000]	=	&PatchSrvProtocol::EnCode__PCServerVersionResp;
	m_mapDeCodeFunc[0x9000]	=	&PatchSrvProtocol::DeCode__PCServerVersionResp;

	m_mapEnCodeFunc[0x1001]	=	&PatchSrvProtocol::EnCode__CPPatchFileInfoReq;
	m_mapDeCodeFunc[0x1001]	=	&PatchSrvProtocol::DeCode__CPPatchFileInfoReq;

	m_mapEnCodeFunc[0x9001]	=	&PatchSrvProtocol::EnCode__PCPatchFileInfoResp;
	m_mapDeCodeFunc[0x9001]	=	&PatchSrvProtocol::DeCode__PCPatchFileInfoResp;

	m_mapEnCodeFunc[0x1002]	=	&PatchSrvProtocol::EnCode__CPDownloadReq;
	m_mapDeCodeFunc[0x1002]	=	&PatchSrvProtocol::DeCode__CPDownloadReq;

	m_mapEnCodeFunc[0x9002]	=	&PatchSrvProtocol::EnCode__PCDownloadResp;
	m_mapDeCodeFunc[0x9002]	=	&PatchSrvProtocol::DeCode__PCDownloadResp;

	m_mapEnCodeFunc[0x9003]	=	&PatchSrvProtocol::EnCode__PCHeartBeat;
	m_mapDeCodeFunc[0x9003]	=	&PatchSrvProtocol::DeCode__PCHeartBeat;

	m_mapEnCodeFunc[0x1003]	=	&PatchSrvProtocol::EnCode__CPHeartBeat;
	m_mapDeCodeFunc[0x1003]	=	&PatchSrvProtocol::DeCode__CPHeartBeat;

	m_mapEnCodeFunc[0x9999]	=	&PatchSrvProtocol::EnCode__PCError;
	m_mapDeCodeFunc[0x9999]	=	&PatchSrvProtocol::DeCode__PCError;

	m_mapEnCodeFunc[0x1004]	=	&PatchSrvProtocol::EnCode__CPDownloadFile;
	m_mapDeCodeFunc[0x1004]	=	&PatchSrvProtocol::DeCode__CPDownloadFile;

	m_mapEnCodeFunc[0x9004]	=	&PatchSrvProtocol::EnCode__PCDownloadFile;
	m_mapDeCodeFunc[0x9004]	=	&PatchSrvProtocol::DeCode__PCDownloadFile;

	m_mapEnCodeFunc[0x1005]	=	&PatchSrvProtocol::EnCode__CPVerifyLauncherVersion;
	m_mapDeCodeFunc[0x1005]	=	&PatchSrvProtocol::DeCode__CPVerifyLauncherVersion;

	m_mapEnCodeFunc[0x9005]	=	&PatchSrvProtocol::EnCode__PCLauncherVersionInfo;
	m_mapDeCodeFunc[0x9005]	=	&PatchSrvProtocol::DeCode__PCLauncherVersionInfo;

	m_mapEnCodeFunc[0x1006]	=	&PatchSrvProtocol::EnCode__CPCliVerifyReq;
	m_mapDeCodeFunc[0x1006]	=	&PatchSrvProtocol::DeCode__CPCliVerifyReq;

	m_mapEnCodeFunc[0x9006]	=	&PatchSrvProtocol::EnCode__PCCliVerifyResp;
	m_mapDeCodeFunc[0x9006]	=	&PatchSrvProtocol::DeCode__PCCliVerifyResp;

	m_mapEnCodeFunc[0x1007]	=	&PatchSrvProtocol::EnCode__CPQueryFileInfo;
	m_mapDeCodeFunc[0x1007]	=	&PatchSrvProtocol::DeCode__CPQueryFileInfo;

	m_mapEnCodeFunc[0x9007]	=	&PatchSrvProtocol::EnCode__PCFileInfoResp;
	m_mapDeCodeFunc[0x9007]	=	&PatchSrvProtocol::DeCode__PCFileInfoResp;

	m_mapEnCodeFunc[0x1008]	=	&PatchSrvProtocol::EnCode__CPReqBtServerInfo;
	m_mapDeCodeFunc[0x1008]	=	&PatchSrvProtocol::DeCode__CPReqBtServerInfo;

	m_mapEnCodeFunc[0x9008]	=	&PatchSrvProtocol::EnCode__PCBtServerInfo;
	m_mapDeCodeFunc[0x9008]	=	&PatchSrvProtocol::DeCode__PCBtServerInfo;

	m_mapEnCodeFunc[0x1009]	=	&PatchSrvProtocol::EnCode__CPReqTorrentInfo;
	m_mapDeCodeFunc[0x1009]	=	&PatchSrvProtocol::DeCode__CPReqTorrentInfo;

	m_mapEnCodeFunc[0x9009]	=	&PatchSrvProtocol::EnCode__PCTorrentInfoResp;
	m_mapDeCodeFunc[0x9009]	=	&PatchSrvProtocol::DeCode__PCTorrentInfoResp;

}

PatchServer::PatchSrvProtocol::~PatchSrvProtocol()
{
}

size_t	PatchServer::PatchSrvProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	PatchServer::PatchSrvProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

PatchServer::EnCodeFunc	PatchServer::PatchSrvProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

PatchServer::DeCodeFunc	PatchServer::PatchSrvProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	PatchServer::PatchSrvProtocol::EnCode__MsgHeader(void* pData)
{
	MsgHeader* pkMsgHeader = (MsgHeader*)(pData);

	//EnCode PackLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMsgHeader->PackLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PackType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkMsgHeader->PackType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PackCmd
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMsgHeader->PackCmd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__MsgHeader(void* pData)
{
	MsgHeader* pkMsgHeader = (MsgHeader*)(pData);

	//DeCode PackLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMsgHeader->PackLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PackType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkMsgHeader->PackType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PackCmd
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMsgHeader->PackCmd), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(MsgHeader);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__Hash(void* pData)
{
	Hash* pkHash = (Hash*)(pData);

	//EnCode HashLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkHash->HashLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode HashValue
	if((int)MAX_HASHVALUE_LEN < (int)pkHash->HashLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkHash->HashLen;
	if(!m_kPackage.Pack("BYTE", &(pkHash->HashValue), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__Hash(void* pData)
{
	Hash* pkHash = (Hash*)(pData);

	//DeCode HashLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkHash->HashLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode HashValue
	if((int)MAX_HASHVALUE_LEN < (int)pkHash->HashLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkHash->HashLen;
	if(!m_kPackage.UnPack("BYTE", &(pkHash->HashValue), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(Hash);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PatchVersion(void* pData)
{
	PatchVersion* pkPatchVersion = (PatchVersion*)(pData);

	//EnCode VersionMajor
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPatchVersion->VersionMajor), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VersionMin
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPatchVersion->VersionMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VersionAddition
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPatchVersion->VersionAddition), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileHash
	if(EnCode__Hash(&(pkPatchVersion->FileHash)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PatchVersion(void* pData)
{
	PatchVersion* pkPatchVersion = (PatchVersion*)(pData);

	//DeCode VersionMajor
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPatchVersion->VersionMajor), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VersionMin
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPatchVersion->VersionMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VersionAddition
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPatchVersion->VersionAddition), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileHash
	if(DeCode__Hash(&(pkPatchVersion->FileHash)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(PatchVersion);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPServerVersionReq(void* pData)
{
	CPServerVersionReq* pkCPServerVersionReq = (CPServerVersionReq*)(pData);

	//EnCode Version
	if(EnCode__PatchVersion(&(pkCPServerVersionReq->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPServerVersionReq(void* pData)
{
	CPServerVersionReq* pkCPServerVersionReq = (CPServerVersionReq*)(pData);

	//DeCode Version
	if(DeCode__PatchVersion(&(pkCPServerVersionReq->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CPServerVersionReq);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCServerVersionResp(void* pData)
{
	PCServerVersionResp* pkPCServerVersionResp = (PCServerVersionResp*)(pData);

	//EnCode Version
	if(EnCode__PatchVersion(&(pkPCServerVersionResp->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode PatchNum
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPCServerVersionResp->PatchNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PatchList
	if((int)MAX_PATCH_SIZE < (int)pkPCServerVersionResp->PatchNum)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkPCServerVersionResp->PatchNum; ++i)
	{
		if(EnCode__PatchVersion(&(pkPCServerVersionResp->PatchList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCServerVersionResp(void* pData)
{
	PCServerVersionResp* pkPCServerVersionResp = (PCServerVersionResp*)(pData);

	//DeCode Version
	if(DeCode__PatchVersion(&(pkPCServerVersionResp->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode PatchNum
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPCServerVersionResp->PatchNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PatchList
	if((int)MAX_PATCH_SIZE < (int)pkPCServerVersionResp->PatchNum)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkPCServerVersionResp->PatchNum; ++i)
	{
		if(DeCode__PatchVersion(&(pkPCServerVersionResp->PatchList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(PCServerVersionResp);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPPatchFileInfoReq(void* pData)
{
	CPPatchFileInfoReq* pkCPPatchFileInfoReq = (CPPatchFileInfoReq*)(pData);

	//EnCode Version
	if(EnCode__PatchVersion(&(pkCPPatchFileInfoReq->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPPatchFileInfoReq(void* pData)
{
	CPPatchFileInfoReq* pkCPPatchFileInfoReq = (CPPatchFileInfoReq*)(pData);

	//DeCode Version
	if(DeCode__PatchVersion(&(pkCPPatchFileInfoReq->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CPPatchFileInfoReq);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCPatchFileInfoResp(void* pData)
{
	PCPatchFileInfoResp* pkPCPatchFileInfoResp = (PCPatchFileInfoResp*)(pData);

	//EnCode Version
	if(EnCode__PatchVersion(&(pkPCPatchFileInfoResp->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode FileSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCPatchFileInfoResp->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileHash
	if(EnCode__Hash(&(pkPCPatchFileInfoResp->FileHash)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode HashCount
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPCPatchFileInfoResp->HashCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PartHash
	if((int)MAX_PART_COUNT < (int)pkPCPatchFileInfoResp->HashCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkPCPatchFileInfoResp->HashCount; ++i)
	{
		if(EnCode__Hash(&(pkPCPatchFileInfoResp->PartHash[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCPatchFileInfoResp(void* pData)
{
	PCPatchFileInfoResp* pkPCPatchFileInfoResp = (PCPatchFileInfoResp*)(pData);

	//DeCode Version
	if(DeCode__PatchVersion(&(pkPCPatchFileInfoResp->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode FileSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCPatchFileInfoResp->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileHash
	if(DeCode__Hash(&(pkPCPatchFileInfoResp->FileHash)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode HashCount
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPCPatchFileInfoResp->HashCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PartHash
	if((int)MAX_PART_COUNT < (int)pkPCPatchFileInfoResp->HashCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkPCPatchFileInfoResp->HashCount; ++i)
	{
		if(DeCode__Hash(&(pkPCPatchFileInfoResp->PartHash[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(PCPatchFileInfoResp);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPDownloadReq(void* pData)
{
	CPDownloadReq* pkCPDownloadReq = (CPDownloadReq*)(pData);

	//EnCode Version
	if(EnCode__PatchVersion(&(pkCPDownloadReq->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode StartPos
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPDownloadReq->StartPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode EndPos
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPDownloadReq->EndPos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPDownloadReq(void* pData)
{
	CPDownloadReq* pkCPDownloadReq = (CPDownloadReq*)(pData);

	//DeCode Version
	if(DeCode__PatchVersion(&(pkCPDownloadReq->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode StartPos
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPDownloadReq->StartPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode EndPos
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPDownloadReq->EndPos), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CPDownloadReq);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCDownloadResp(void* pData)
{
	PCDownloadResp* pkPCDownloadResp = (PCDownloadResp*)(pData);

	//EnCode Version
	if(EnCode__PatchVersion(&(pkPCDownloadResp->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode StartPos
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCDownloadResp->StartPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DataLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCDownloadResp->DataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PatchData
	if((int)MAX_PATCHDATALEN < (int)pkPCDownloadResp->DataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCDownloadResp->DataLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCDownloadResp->PatchData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCDownloadResp(void* pData)
{
	PCDownloadResp* pkPCDownloadResp = (PCDownloadResp*)(pData);

	//DeCode Version
	if(DeCode__PatchVersion(&(pkPCDownloadResp->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode StartPos
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCDownloadResp->StartPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DataLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCDownloadResp->DataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PatchData
	if((int)MAX_PATCHDATALEN < (int)pkPCDownloadResp->DataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCDownloadResp->DataLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCDownloadResp->PatchData), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCDownloadResp);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCHeartBeat(void* pData)
{
	PCHeartBeat* pkPCHeartBeat = (PCHeartBeat*)(pData);

	//EnCode HeartID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCHeartBeat->HeartID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode HeartTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCHeartBeat->HeartTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCHeartBeat(void* pData)
{
	PCHeartBeat* pkPCHeartBeat = (PCHeartBeat*)(pData);

	//DeCode HeartID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCHeartBeat->HeartID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode HeartTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCHeartBeat->HeartTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCHeartBeat);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPHeartBeat(void* pData)
{
	CPHeartBeat* pkCPHeartBeat = (CPHeartBeat*)(pData);

	//EnCode HeartID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPHeartBeat->HeartID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode HeartTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPHeartBeat->HeartTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPHeartBeat(void* pData)
{
	CPHeartBeat* pkCPHeartBeat = (CPHeartBeat*)(pData);

	//DeCode HeartID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPHeartBeat->HeartID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode HeartTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPHeartBeat->HeartTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CPHeartBeat);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCError(void* pData)
{
	PCError* pkPCError = (PCError*)(pData);

	//EnCode ErrID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCError->ErrID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCError(void* pData)
{
	PCError* pkPCError = (PCError*)(pData);

	//DeCode ErrID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCError->ErrID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCError);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPCliVerifyReq(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPCliVerifyReq(void* pData)
{
	return sizeof(CPCliVerifyReq);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCCliVerifyResp(void* pData)
{
	PCCliVerifyResp* pkPCCliVerifyResp = (PCCliVerifyResp*)(pData);

	//EnCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCCliVerifyResp->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileName
	if((int)MAX_FILE_NAME < (int)pkPCCliVerifyResp->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCCliVerifyResp->FileNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCCliVerifyResp->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCCliVerifyResp->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CliPathLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPCCliVerifyResp->CliPathLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CliPath
	if((int)MAX_FILE_NAME < (int)pkPCCliVerifyResp->CliPathLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCCliVerifyResp->CliPathLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCCliVerifyResp->CliPath), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCCliVerifyResp(void* pData)
{
	PCCliVerifyResp* pkPCCliVerifyResp = (PCCliVerifyResp*)(pData);

	//DeCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCCliVerifyResp->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileName
	if((int)MAX_FILE_NAME < (int)pkPCCliVerifyResp->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCCliVerifyResp->FileNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCCliVerifyResp->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCCliVerifyResp->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CliPathLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPCCliVerifyResp->CliPathLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CliPath
	if((int)MAX_FILE_NAME < (int)pkPCCliVerifyResp->CliPathLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCCliVerifyResp->CliPathLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCCliVerifyResp->CliPath), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCCliVerifyResp);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPDownloadFile(void* pData)
{
	CPDownloadFile* pkCPDownloadFile = (CPDownloadFile*)(pData);

	//EnCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPDownloadFile->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileName
	if((int)MAX_FILE_NAME < (int)pkCPDownloadFile->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCPDownloadFile->FileNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCPDownloadFile->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Offset1
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPDownloadFile->Offset1), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Offset2
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPDownloadFile->Offset2), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DataLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPDownloadFile->DataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPDownloadFile(void* pData)
{
	CPDownloadFile* pkCPDownloadFile = (CPDownloadFile*)(pData);

	//DeCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPDownloadFile->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileName
	if((int)MAX_FILE_NAME < (int)pkCPDownloadFile->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCPDownloadFile->FileNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCPDownloadFile->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Offset1
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPDownloadFile->Offset1), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Offset2
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPDownloadFile->Offset2), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DataLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPDownloadFile->DataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CPDownloadFile);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCDownloadFile(void* pData)
{
	PCDownloadFile* pkPCDownloadFile = (PCDownloadFile*)(pData);

	//EnCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCDownloadFile->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileName
	if((int)MAX_FILE_NAME < (int)pkPCDownloadFile->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCDownloadFile->FileNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCDownloadFile->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Offset1
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCDownloadFile->Offset1), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Offset2
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCDownloadFile->Offset2), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DataLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCDownloadFile->DataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileData
	if((int)MAX_PATCHDATALEN < (int)pkPCDownloadFile->DataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCDownloadFile->DataLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCDownloadFile->FileData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCDownloadFile(void* pData)
{
	PCDownloadFile* pkPCDownloadFile = (PCDownloadFile*)(pData);

	//DeCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCDownloadFile->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileName
	if((int)MAX_FILE_NAME < (int)pkPCDownloadFile->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCDownloadFile->FileNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCDownloadFile->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Offset1
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCDownloadFile->Offset1), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Offset2
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCDownloadFile->Offset2), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DataLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCDownloadFile->DataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileData
	if((int)MAX_PATCHDATALEN < (int)pkPCDownloadFile->DataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCDownloadFile->DataLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCDownloadFile->FileData), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCDownloadFile);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPVerifyLauncherVersion(void* pData)
{
	CPVerifyLauncherVersion* pkCPVerifyLauncherVersion = (CPVerifyLauncherVersion*)(pData);

	//EnCode ReqType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPVerifyLauncherVersion->ReqType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPVerifyLauncherVersion(void* pData)
{
	CPVerifyLauncherVersion* pkCPVerifyLauncherVersion = (CPVerifyLauncherVersion*)(pData);

	//DeCode ReqType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPVerifyLauncherVersion->ReqType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CPVerifyLauncherVersion);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCLauncherVersionInfo(void* pData)
{
	PCLauncherVersionInfo* pkPCLauncherVersionInfo = (PCLauncherVersionInfo*)(pData);

	//EnCode ReqType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCLauncherVersionInfo->ReqType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCLauncherVersionInfo->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileName
	if((int)MAX_FILE_NAME < (int)pkPCLauncherVersionInfo->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCLauncherVersionInfo->FileNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCLauncherVersionInfo->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCLauncherVersionInfo->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCLauncherVersionInfo(void* pData)
{
	PCLauncherVersionInfo* pkPCLauncherVersionInfo = (PCLauncherVersionInfo*)(pData);

	//DeCode ReqType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCLauncherVersionInfo->ReqType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCLauncherVersionInfo->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileName
	if((int)MAX_FILE_NAME < (int)pkPCLauncherVersionInfo->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCLauncherVersionInfo->FileNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCLauncherVersionInfo->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCLauncherVersionInfo->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCLauncherVersionInfo);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPQueryFileInfo(void* pData)
{
	CPQueryFileInfo* pkCPQueryFileInfo = (CPQueryFileInfo*)(pData);

	//EnCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCPQueryFileInfo->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileName
	if((int)MAX_FILE_NAME < (int)pkCPQueryFileInfo->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCPQueryFileInfo->FileNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCPQueryFileInfo->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPQueryFileInfo(void* pData)
{
	CPQueryFileInfo* pkCPQueryFileInfo = (CPQueryFileInfo*)(pData);

	//DeCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCPQueryFileInfo->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileName
	if((int)MAX_FILE_NAME < (int)pkCPQueryFileInfo->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCPQueryFileInfo->FileNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCPQueryFileInfo->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CPQueryFileInfo);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCFileInfoResp(void* pData)
{
	PCFileInfoResp* pkPCFileInfoResp = (PCFileInfoResp*)(pData);

	//EnCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCFileInfoResp->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileName
	if((int)MAX_FILE_NAME < (int)pkPCFileInfoResp->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCFileInfoResp->FileNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCFileInfoResp->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FileSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCFileInfoResp->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCFileInfoResp(void* pData)
{
	PCFileInfoResp* pkPCFileInfoResp = (PCFileInfoResp*)(pData);

	//DeCode FileNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCFileInfoResp->FileNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileName
	if((int)MAX_FILE_NAME < (int)pkPCFileInfoResp->FileNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCFileInfoResp->FileNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCFileInfoResp->FileName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FileSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCFileInfoResp->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCFileInfoResp);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPReqBtServerInfo(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPReqBtServerInfo(void* pData)
{
	return sizeof(CPReqBtServerInfo);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCBtServerInfo(void* pData)
{
	PCBtServerInfo* pkPCBtServerInfo = (PCBtServerInfo*)(pData);

	//EnCode UdpXServerLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPCBtServerInfo->UdpXServerLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UdpXServer
	if((int)MAX_URL_LEN < (int)pkPCBtServerInfo->UdpXServerLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCBtServerInfo->UdpXServerLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCBtServerInfo->UdpXServer), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UdpEServerLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPCBtServerInfo->UdpEServerLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UdpEServer
	if((int)MAX_URL_LEN < (int)pkPCBtServerInfo->UdpEServerLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCBtServerInfo->UdpEServerLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCBtServerInfo->UdpEServer), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode StatisticServerLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPCBtServerInfo->StatisticServerLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode StatisticServer
	if((int)MAX_URL_LEN < (int)pkPCBtServerInfo->StatisticServerLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCBtServerInfo->StatisticServerLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCBtServerInfo->StatisticServer), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode StatisticServerPort
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPCBtServerInfo->StatisticServerPort), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TrackerServerLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPCBtServerInfo->TrackerServerLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TrackerServer
	if((int)MAX_URL_LEN < (int)pkPCBtServerInfo->TrackerServerLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCBtServerInfo->TrackerServerLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCBtServerInfo->TrackerServer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCBtServerInfo(void* pData)
{
	PCBtServerInfo* pkPCBtServerInfo = (PCBtServerInfo*)(pData);

	//DeCode UdpXServerLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPCBtServerInfo->UdpXServerLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UdpXServer
	if((int)MAX_URL_LEN < (int)pkPCBtServerInfo->UdpXServerLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCBtServerInfo->UdpXServerLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCBtServerInfo->UdpXServer), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UdpEServerLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPCBtServerInfo->UdpEServerLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UdpEServer
	if((int)MAX_URL_LEN < (int)pkPCBtServerInfo->UdpEServerLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCBtServerInfo->UdpEServerLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCBtServerInfo->UdpEServer), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode StatisticServerLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPCBtServerInfo->StatisticServerLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode StatisticServer
	if((int)MAX_URL_LEN < (int)pkPCBtServerInfo->StatisticServerLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCBtServerInfo->StatisticServerLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCBtServerInfo->StatisticServer), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode StatisticServerPort
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPCBtServerInfo->StatisticServerPort), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TrackerServerLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPCBtServerInfo->TrackerServerLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TrackerServer
	if((int)MAX_URL_LEN < (int)pkPCBtServerInfo->TrackerServerLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCBtServerInfo->TrackerServerLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCBtServerInfo->TrackerServer), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCBtServerInfo);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__CPReqTorrentInfo(void* pData)
{
	CPReqTorrentInfo* pkCPReqTorrentInfo = (CPReqTorrentInfo*)(pData);

	//EnCode Version
	if(EnCode__PatchVersion(&(pkCPReqTorrentInfo->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__CPReqTorrentInfo(void* pData)
{
	CPReqTorrentInfo* pkCPReqTorrentInfo = (CPReqTorrentInfo*)(pData);

	//DeCode Version
	if(DeCode__PatchVersion(&(pkCPReqTorrentInfo->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CPReqTorrentInfo);
}

size_t	PatchServer::PatchSrvProtocol::EnCode__PCTorrentInfoResp(void* pData)
{
	PCTorrentInfoResp* pkPCTorrentInfoResp = (PCTorrentInfoResp*)(pData);

	//EnCode Version
	if(EnCode__PatchVersion(&(pkPCTorrentInfoResp->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode FileSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPCTorrentInfoResp->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TorrentUrlLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPCTorrentInfoResp->TorrentUrlLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TorrentUrl
	if((int)MAX_URL_LEN < (int)pkPCTorrentInfoResp->TorrentUrlLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCTorrentInfoResp->TorrentUrlLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCTorrentInfoResp->TorrentUrl), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PatchUrlLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPCTorrentInfoResp->PatchUrlLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PatchUrl
	if((int)MAX_URL_LEN < (int)pkPCTorrentInfoResp->PatchUrlLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCTorrentInfoResp->PatchUrlLen;
	if(!m_kPackage.Pack("CHAR", &(pkPCTorrentInfoResp->PatchUrl), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	PatchServer::PatchSrvProtocol::DeCode__PCTorrentInfoResp(void* pData)
{
	PCTorrentInfoResp* pkPCTorrentInfoResp = (PCTorrentInfoResp*)(pData);

	//DeCode Version
	if(DeCode__PatchVersion(&(pkPCTorrentInfoResp->Version)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode FileSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPCTorrentInfoResp->FileSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TorrentUrlLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPCTorrentInfoResp->TorrentUrlLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TorrentUrl
	if((int)MAX_URL_LEN < (int)pkPCTorrentInfoResp->TorrentUrlLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCTorrentInfoResp->TorrentUrlLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCTorrentInfoResp->TorrentUrl), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PatchUrlLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPCTorrentInfoResp->PatchUrlLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PatchUrl
	if((int)MAX_URL_LEN < (int)pkPCTorrentInfoResp->PatchUrlLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPCTorrentInfoResp->PatchUrlLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPCTorrentInfoResp->PatchUrl), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PCTorrentInfoResp);
}

