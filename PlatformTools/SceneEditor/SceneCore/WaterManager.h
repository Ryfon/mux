﻿/** @file WaterManager.h 
@brief 水体管理器,负责反射纹理的渲染
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-03-03
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
#include "WaterRenderView.h"
#include "NiEntity.h"
#include "WaterBody.h"

namespace SceneCore
{

class  MAIN_ENTRY CWaterManager: public NiMemObject
{
public:
	~CWaterManager(void);

	// 获取唯一实例
	static CWaterManager* GetInstance(); 

	/**
	*	<BR>功能说明：水体管理器初始化
	*	<BR>可访问性：对象访问
	*	<BR>注    释：初始化后水体管理器才具有渲染反射纹理功能
	*	@param	pkWaterRenderView[in]	水体 render view.包含摄像机属性和反射纹理需要渲染得物件的最小集
	*	@return 无
	*/
	bool Init(	CWaterRenderViewPtr	pkWaterRenderView);

	/// 销毁管理器
	void Destory();

	/// 是否初始化过
	bool Initialized();

	void Enable();	// 打开实时反射
	void Disable();	// 关闭实时反射

	/// 添加/移除/清空 水体实例
	void AddWaterInstance(CWaterBodyPtr pkWater);
	void RemoveWaterInstance(CWaterBodyPtr pkWater);
	void ClearWaterInstance();

	/// 获取主 render view
	CWaterRenderViewPtr GetMainWaterRenderView();

	/// 判断一个entity 是否具有 水体 component
	static bool EnityIsWaterBody(const NiEntityInterface* pEntity);

	/// 渲染所有水体的反射纹理
	void RenderReflectTexture(NiEntityRenderingContextPtr pkRenderingContext);
private:
	CWaterManager(void);
	//list<CWaterBody*>::iterator _FindByObject(CWaterBodyPtr pkRenderObject);

	//static CWaterManager*	ms_pWaterMgrInstance;	// 水体管理器唯一实例

	/// 创建渲染到全屏的 render view
	void _CreateScreenFillingRenderViews();

	/// 场景主 render view
	CWaterRenderViewPtr				m_psMainSceneRenderView;


	//list<CWaterBody*>		m_WaterList;				// 水体列表
	NiTPointerList<CWaterBodyPtr> m_WaterList; 			// 水体列表

};
};
