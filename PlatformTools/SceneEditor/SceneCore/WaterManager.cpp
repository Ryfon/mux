﻿#include "StdAfx.h"
#include "WaterManager.h"
#include "WaterEntityComponent.h"

using namespace SceneCore;

CWaterManager::CWaterManager(void)
:	m_psMainSceneRenderView(NULL)
{}
//-------------------------------------------------------------------------------------------------
CWaterManager::~CWaterManager(void)
{}
//-------------------------------------------------------------------------------------------------
CWaterManager* CWaterManager::GetInstance()
{
	static CWaterManager sWaterMgr;
	return &sWaterMgr;
}
//-------------------------------------------------------------------------------------------------
bool CWaterManager::Init(CWaterRenderViewPtr	pkWaterRenderView)
{
	if (m_psMainSceneRenderView == NULL)
	{
		m_psMainSceneRenderView = pkWaterRenderView;
		return true;
	}
	else
	{
		return false;
	}
}
//-------------------------------------------------------------------------------------------------
void CWaterManager::Destory()
{
	m_WaterList.RemoveAll();
	m_psMainSceneRenderView = NULL;
}
//-------------------------------------------------------------------------------------------------
bool CWaterManager::Initialized()
{
	return (m_psMainSceneRenderView != NULL);
}
//-------------------------------------------------------------------------------------------------
void CWaterManager::Enable()	// 打开实时反射
{
	NiTListIterator iter = m_WaterList.GetHeadPos();

	while (iter)
	{
		(m_WaterList.GetNext(iter))->Enable();
	}	
}
//-------------------------------------------------------------------------------------------------
void CWaterManager::Disable()	// 关闭实时反射
{
	NiTListIterator iter = m_WaterList.GetHeadPos();

	while (iter)
	{
		(m_WaterList.GetNext(iter))->Disable();
	}	
}
//-------------------------------------------------------------------------------------------------
CWaterRenderViewPtr CWaterManager::GetMainWaterRenderView()
{
	return m_psMainSceneRenderView;
}
//-------------------------------------------------------------------------------------------------
bool CWaterManager::EnityIsWaterBody(const NiEntityInterface* pEntity)
{
	unsigned int compCount = pEntity->GetComponentCount();
	for (unsigned int i=0; i<compCount; i++)
	{
		NiEntityComponentInterface* pComp = pEntity->GetComponentAt(i);
		if (pComp->GetClassName() == CWaterEntityComponent::ClassName())
		{
			return true;
		}
	}
	return false;

}
//-------------------------------------------------------------------------------------------------
void CWaterManager::AddWaterInstance(CWaterBodyPtr pkWater)
{
	//list<CWaterBody*>::iterator iter = _FindByObject(pkWater);
	if (m_WaterList.FindPos(pkWater) == NULL)	// pkWater 不在列表中
	{
		m_WaterList.AddTail( pkWater );
	}
}
//-------------------------------------------------------------------------------------------------
void CWaterManager::RemoveWaterInstance(CWaterBodyPtr pkWater)
{
	NiTListIterator iter = m_WaterList.FindPos(pkWater);
	if (iter != NULL)
	{
		m_WaterList.RemovePos(iter);
	}
}
//-------------------------------------------------------------------------------------------------
void CWaterManager::ClearWaterInstance()
{
	m_WaterList.RemoveAll();
}
//-------------------------------------------------------------------------------------------------
void CWaterManager::RenderReflectTexture(NiEntityRenderingContextPtr pkRenderingContext)
{
	NiTListIterator iter = m_WaterList.GetHeadPos();

	while (iter)
	{
		// NiSpeedTreeComponent 中使用 uiTreesAdded 控制树只能渲染一次。所以要把 uiTreesAdded 恢复
//		unsigned int uiTreesAdded = NiSpeedTreeComponent::GetNumTreesAdded();
		(m_WaterList.GetNext(iter))->RenderReflectTexture(pkRenderingContext);
//		NiSpeedTreeComponent::SetNumTreesAdded(uiTreesAdded);	
	}
}
//-------------------------------------------------------------------------------------------------