﻿/** @file Terrain.h 
@brief 编辑地形顶点高度 command,用于 undo/redo
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：1.0
*	作    者：Shi Yazheng
*	完成日期：2008-02-18
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME

#ifndef	TERRAINCHANGEVERTEXCOMMAND_H 
#define	TERRAINCHANGEVERTEXCOMMAND_H

#pragma once

class CTerrain;

struct MAIN_ENTRY tVertexChange
{
	int iIndex;			// 被改变高度的的顶点索引
	float fOldHeight;	// 改变之前的高度
	float fNewHeight;	// 改变之后的高度
};

class MAIN_ENTRY CTerrainChangeVertexCommand
{
public:
	CTerrainChangeVertexCommand(CTerrain* pTerrain);
	~CTerrainChangeVertexCommand(void);

	/// 执行该命令
	void DoCommand(void);

	/// 撤销该命令
	void UndoCommand(void);

	/// 增加一个改变的顶点 
	void AddVertexChange(const tVertexChange& vertChange);

	/// 增加有顶点改变的 chunk
	void AddChangedChunk(int iChunk);

protected:
	/// 计算顶点法线并且将顶点变化通知 chunk
	void _CalculateNormalAndNotifyChunk();

	std::map<int, tVertexChange>	m_VertexChanges;	// 顶点索引-该索引顶点发生的改变 映射
	set<int>					m_chunkset;
	CTerrain*					m_pTerrain;
};

#pragma make_public(CTerrainChangeVertexCommand)

#endif

#endif