﻿#include "StdAfx.h"
#include "PostEffectManager.h"

#ifndef CODE_INGAME
using namespace SceneCore;

CPostEffectManager::CPostEffectManager(void)
{

}
//---------------------------------------------------------------------------------------------------------
CPostEffectManager::~CPostEffectManager(void)
{
	RemoveAllEffects();
}
//---------------------------------------------------------------------------------------------------------
bool CPostEffectManager::AddEffect(CPostEffect* pkEffect)
{
	if ( pkEffect == NULL )
	{
		return false;
	}
	unsigned int uiIndex = m_kEffects.GetSize();
	const char* strName = pkEffect->GetName();

	pkEffect->SetEffectID(uiIndex);

	m_kEffects.Add( pkEffect );

	return true;
}

//--------------------------------------------------------------------------------------------------------
unsigned int CPostEffectManager::GetNumEffects() const
{
	return m_kEffects.GetSize();
}

//--------------------------------------------------------------------------------------------------------
CPostEffect* CPostEffectManager::GetEffectAt(unsigned int uiIndex) const
{
	if ( uiIndex < m_kEffects.GetSize() )
	{
		return m_kEffects.GetAt(uiIndex);
	}
	return NULL;
}

//--------------------------------------------------------------------------------------------------------
void CPostEffectManager::SetActive(unsigned int uiIndex,bool bActive)
{
	if ( uiIndex < m_kEffects.GetSize() )
	{
		 m_kEffects.GetAt(uiIndex)->SetActive(bActive);
	}
}

//--------------------------------------------------------------------------------------------------------
bool CPostEffectManager::GetActive(unsigned int uiIndex)
{
	if ( uiIndex < m_kEffects.GetSize() )
	{
		return m_kEffects.GetAt(uiIndex)->GetActive();
	}
	return false;
}
//--------------------------------------------------------------------------------------------------------
void CPostEffectManager::RemoveEffectAt(unsigned int uiIndex)
{
	if ( uiIndex < m_kEffects.GetSize() )
	{
		CPostEffect* pkEffect = m_kEffects.GetAt(uiIndex);
		m_kEffects.OrderedRemoveAt(uiIndex);
		pkEffect->Destory();
		NiDelete pkEffect;
	}
}

//--------------------------------------------------------------------------------------------------------
void CPostEffectManager::RemoveAllEffects()
{
	for (unsigned int uiIndex = 0; uiIndex < m_kEffects.GetSize(); uiIndex++)
	{
		CPostEffect* pkEffect = m_kEffects.GetAt(uiIndex);
		if ( pkEffect->GetRenderStep() )
		{
			pkEffect->Destory();
			NiDelete pkEffect;
		}
	}
	m_kEffects.RemoveAll();
}

////--------------------------------------------------------------------------------------------------------
bool CPostEffectManager::Init(NiRenderedTexturePtr pkSourceTexture)
{
	bool bSuccess = false;
	for ( unsigned int uiIndex = 0; uiIndex < m_kEffects.GetSize(); uiIndex++)
	{
		bool bActive = false;
		CPostEffect* pkEffect = m_kEffects.GetAt(uiIndex);
		bSuccess = pkEffect->Init( pkSourceTexture );
		if ( !bSuccess )
		{
			return bSuccess;
		}
	}

	return bSuccess;
}
//---------------------------------------------------------------------------------------------------------
void CPostEffectManager::PerformRendering()
{
	NiRenderer* pkRenderer = NiRenderer::GetRenderer();

	for (unsigned int ui = 0; ui < m_kEffects.GetSize(); ui++)
	{
		CPostEffect* pkCurEffect = m_kEffects.GetAt(ui);
		if (pkCurEffect->GetActive())
		{
			if (pkRenderer->IsRenderTargetGroupActive())
				pkRenderer->EndUsingRenderTargetGroup();

			if ( pkCurEffect->GetInitialized() )
			{
				pkCurEffect->PerformRendering();
			}
						
		}
	}

}

//---------------------------------------------------------------------------------------------------------
#endif