﻿#pragma once
/** @file Terrain.h 
@brief 地形优化器 用于地形数据导出时的优化
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：1.0
*	作    者：Shi Yazheng
*	完成日期：2008-01-27
*
*	取代版本：
*	作    者：和萌
*	完成日期：
</pre>*/

#ifndef CODE_INGAME

#include "Terrain.h"

class MAIN_ENTRY CTerrainOptimizer
{
public:
	CTerrainOptimizer(CTerrain* pTerrain, int iSensitivity = 5);

	~CTerrainOptimizer(void);

	/**
	*	<BR>功能说明：优化地形
	*	<BR>可访问性：
	*	<BR>注    释：
	*	@param	bFinalOptimize[in]	是否最终优化。最终优化过程不可逆，只用作地形导出
	*	@return 
	*/ 
	void Optimize(bool bFinalOptimize = false);


	/// 获取 quadMatrix 中 iX, iY 坐标元素 对应的索引值
	int GetQuadMatrixIndex(int iX, int iY)
	{
		return iY*(m_pTerrain->GetChunkNumX()*GRIDINCHUNK+1)+iX;
	}

	unsigned char GetQuadMatrixValue(int iX, int iY)
	{
		return m_ucpQuadMatrix[GetQuadMatrixIndex(iX, iY)];
	}

private:
	// 优化单个 chunk
	void _OptimizeChunk(int iX, int iY);

	// quad mat 标准化
	void _StandardQuadMat(int iChunkX, int iChunkY, int iX, int iY, int iEdgeLength);

	/**
	*	<BR>功能说明：生成一个 quad tree 节点的三角形
	*	<BR>可访问性：
	*	<BR>注    释：
	*	@param	iChunkX[in]			当前节点所在 chunk 的坐标（行列下标）
	*	@param	iChunkY[in]		
	*	@param	iX[in]				节点中心顶点在本chunk顶点中坐标（行列下标）
	*	@param	iY[in]		
	*	@param	iEdegLength[in]	节点边长
	*	@param	pVI[out]			顶点索引
	*	@param	iNumTri[out]		生成的三角形总数
	*	@return 
	*/ 
	void _GenerateTriangles(int iChunkX, int iChunkY, int iX, int iY, int iEdgeLength, WORD* pVI, int &iNumTri);

	// 清除所有未使用顶点。用于最终优化
	void _ClearUnuseVertices();

	// 修正chunk边界的顶点normal值 [6/22/2009 hemeng]
	// 逐chunk计算normal容易造成chunk接缝处的公用顶点计算的normal不相同：因为两边的三角形并不相同
	bool _ModifyNormals();
	
	// 记录chunk normal错误的结构 [6/22/2009 hemeng]
	struct stChunkNormal 
	{
		unsigned int uiChunkID;
		unsigned int uiVertIndex;
		NiPoint3 kNormal;
	};

	void _CheckChunkBorder(int iChunkID, vector<stChunkNormal>& vErrorVer, bool bRow);

	CTerrain*		m_pTerrain;
	unsigned char*	m_ucpQuadMatrix;	// 四叉树矩阵, 用来标记所有四叉树节点的细分情况
	int				m_iSensitivity;		// 敏感度
};

#endif
