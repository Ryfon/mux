﻿#include "StdAfx.h"
#ifndef CODE_INGAME

#include "FogSurfaceMaker.h"
#include "NiOptimize.h"

using namespace SceneCore;
//-----------------------------------------------------------------------------
CFogSurfaceMaker::CFogSurfaceMaker(CTerrain* pkTerrain)
:	m_pkTerrain(pkTerrain),
	m_spFogGeometry(NULL),
	m_pGridList(NULL)
{
}
//-----------------------------------------------------------------------------
CFogSurfaceMaker::~CFogSurfaceMaker(void)
{
	m_spFogGeometry = NULL;
	SAFE_DELETE_ARRAY(m_pGridList);
}
//-----------------------------------------------------------------------------
bool CFogSurfaceMaker::Make(const stSimpleFogPrarm param)
{
	if (m_pkTerrain == NULL)
	{
		return false;
	}

	SAFE_DELETE_ARRAY(m_pGridList);

	int iNumVertTotalX = m_pkTerrain->GetTotalSize().x+1;
	int iNumVertTotalY = m_pkTerrain->GetTotalSize().y+1;

	// 1. 获取目标区域内按照 terrain grid 尺寸的 grid, 并且初始化
	int iLeft = max(0, (int)param.kLBPoint.x);
	int iRight = min(iNumVertTotalX-1, (int)param.kRUPoint.x);
	int iBottom = max(0, (int)param.kLBPoint.y);
	int iTop = min(iNumVertTotalY-1, (int)param.kRUPoint.y);

	int iGridInRow = iRight-iLeft;
	int iGridInColum = iTop-iBottom;
	
	m_iNumGridX = iGridInRow;
	m_iNumGridY = iGridInColum;

	// Fog Geometry 第一个顶点（左下）坐标
	int iOffsetX = iLeft;
	int iOffsetY = iBottom;

	m_pGridList = new stFogGrid[iGridInColum*iGridInRow];
	int iIdx = 0;
	for (int i=0; i<iGridInColum; i++)	// 遍历列， 向 y 正方向
	{
		for (int j=0; j<iGridInRow; j++, iIdx++)	// 遍历行， 向 x 正方向
		{
			m_pGridList[iIdx].locationState = GS_UNKNOW;
			// 初始化四个顶点索引
			m_pGridList[iIdx].i0 = (iOffsetY+i)*iNumVertTotalX + iOffsetX+j;
			m_pGridList[iIdx].i1 = m_pGridList[iIdx].i0 + iNumVertTotalX;
			m_pGridList[iIdx].i2 = m_pGridList[iIdx].i1 + 1;
			m_pGridList[iIdx].i3 = m_pGridList[iIdx].i0 + 1;
		}
	}

	// 2. 确定每个 grid 的位置。 
	// 2.1	如果 grid 完全在地形之上，geometry 边缘的grid 认为是 GS_OUTSIDE, 其他的都认为是 GS_INSIDE
	//		如果 grid 与地形相交，如果边缘点在地上，认为是GS_HALFOUTSIDE, 否则认为是 GS_HALFINSIDE
	//		如果 grid 在地形之下，认为是 GS_UNDERGROUND

	const NiPoint3* pVertices = m_pkTerrain->GetVertices();
	iIdx = 0;
	for (int i=0; i<iGridInColum; i++)	// 遍历列， 向 y 正方向
	{
		for (int j=0; j<iGridInRow; j++, iIdx++)	// 遍历行， 向 x 正方向
		{
			// 确定四个顶点的位置
			bool bOver0 = param.fStartZ > pVertices[m_pGridList[iIdx].i0].z; 
			bool bOver1 = param.fStartZ > pVertices[m_pGridList[iIdx].i1].z; 
			bool bOver2 = param.fStartZ > pVertices[m_pGridList[iIdx].i2].z; 
			bool bOver3 = param.fStartZ > pVertices[m_pGridList[iIdx].i3].z; 

			if (bOver0 && bOver1 && bOver2 && bOver3)
			{
				// 完全在地形之上
				if (i==0 || i==iGridInColum-1
						|| j==0 || j==iGridInRow-1)
				{
					// 边缘的 grid
					m_pGridList[iIdx].locationState = GS_OUTSIDE;
					m_pGridList[iIdx].combineState = GS_DELETED;
				}
				else
				{
					m_pGridList[iIdx].locationState = GS_INSIDE;
					m_pGridList[iIdx].combineState = GS_UNDESIDED;
				}
			}
			else if(!bOver0 && !bOver1 && !bOver2 && !bOver3)
			{
				// 完全地形之下
				m_pGridList[iIdx].locationState = GS_UNDERGROUND;
				m_pGridList[iIdx].combineState = GS_DELETED;
			}
			else
			{
				if (i==0 || j==0 || i==iGridInColum-1 || j==iGridInRow-1)
				{
					m_pGridList[iIdx].locationState = GS_HALFOUTSIDE;
					m_pGridList[iIdx].combineState = GS_DELETED;
				}
				else
				{
					m_pGridList[iIdx].locationState = GS_HALFINSIDE;
					m_pGridList[iIdx].combineState = GS_UNDESIDED;
				}

			}
		}
	}

	// 2.2	从边缘 grid 遍历所有邻居 grid. 
	//		
	//		如果是 GS_INSIDE 改为 GS_OUTSIDE.并从该节点开始遍历邻居
	//		如果是 GS_HALFINSIDE 改为 GS_HALFOUTSIDE.并从该节点开始遍历邻居
	//		如果是 GS_UNDERGROUND, GS_HALFOUTSIDE, GS_OUTSIDE 不遍历该节点
	
	
	// 遍历四条外边
	// 左边， 右边
	for (int iY=0; iY<iGridInColum; iY++)
	{
		m_OpenList.push_back(stGridCoord(0, iY));
		m_OpenList.push_back(stGridCoord(iGridInRow-1, iY));
	}

	// 顶边， 底边
	for (int iX=0; iX<iGridInRow; iX++)
	{
		m_OpenList.push_back(stGridCoord(iX, 0));
		m_OpenList.push_back(stGridCoord(iX, iGridInColum-1));
	}

	list<stGridCoord>::iterator iter = m_OpenList.begin();
	while (iter != m_OpenList.end())
	{
		iter = _DecideLocation(iter);
		//iter++;
	}

	//Debug_OutputGrid("e:\\before_combine.txt");

	// 3 合并 grid
	_CombineAllGrids();
	//Debug_OutputGrid("e:\\after_combine.txt");
	//Debug_OutputGrid("e:\\gridCombine.txt");
	// 4 根据合并的 grid 生成 geometry
	vector<int> gridIdList;	
	//iIdx = 0;
	for (int i=0; i<m_iNumGridX*m_iNumGridY; i++)
	{
		if (m_pGridList[i].combineState == GS_COMBINED)
		{
			gridIdList.push_back(i);
		}
	}

	const NiPoint3* pTerrainVertices = m_pkTerrain->GetVertices();

	int iVertNum = gridIdList.size()*4;	// 顶点数量
	int iTriNum = gridIdList.size()*2;	// 三角形数量
	NiPoint3* pVerts		= NiNew NiPoint3[iVertNum];
	NiPoint3* pNormals		= NiNew NiPoint3[iVertNum];
	NiPoint2* pTexCoords	= NiNew NiPoint2[iVertNum];
	NiColorA* pColors		= NiNew NiColorA[iVertNum];
	WORD	*pConnect = NiAlloc(WORD, iTriNum*3);

	int dX = param.kRUPoint.x-param.kLBPoint.x;
	int dY = param.kRUPoint.y-param.kLBPoint.y;

	for (unsigned int i=0; i<gridIdList.size(); i++)
	{
		pVerts[i*4] = pTerrainVertices[m_pGridList[gridIdList[i]].i0];
		pVerts[i*4+1] = pTerrainVertices[m_pGridList[gridIdList[i]].i1];
		pVerts[i*4+2] = pTerrainVertices[m_pGridList[gridIdList[i]].i2];
		pVerts[i*4+3] = pTerrainVertices[m_pGridList[gridIdList[i]].i3];

		pVerts[i*4].z = pVerts[i*4+1].z = pVerts[i*4+2].z = pVerts[i*4+3].z = param.fStartZ;

		pNormals[i*4] = pNormals[i*4+1] = pNormals[i*4+2] = pNormals[i*4+3] = NiPoint3(0, 0, 1);
		
		pTexCoords[i*4] = NiPoint2((pVerts[i*4].x-param.kLBPoint.x)/dX, (pVerts[i*4].y-param.kLBPoint.y)/dY);
		pTexCoords[i*4].y = 1.0f - pTexCoords[i*4].y;
		pTexCoords[i*4+1] = NiPoint2((pVerts[i*4+1].x-param.kLBPoint.x)/dX, (pVerts[i*4+1].y-param.kLBPoint.y)/dY);
		pTexCoords[i*4+1].y = 1.0f - pTexCoords[i*4+1].y;
		pTexCoords[i*4+2] = NiPoint2((pVerts[i*4+2].x-param.kLBPoint.x)/dX, (pVerts[i*4+2].y-param.kLBPoint.y)/dY);
		pTexCoords[i*4+2].y = 1.0f - pTexCoords[i*4+2].y;
		pTexCoords[i*4+3] = NiPoint2((pVerts[i*4+3].x-param.kLBPoint.x)/dX, (pVerts[i*4+3].y-param.kLBPoint.y)/dY);
		pTexCoords[i*4+3].y = 1.0f - pTexCoords[i*4+3].y;

		pColors[i*4] = pColors[i*4+1] = pColors[i*4+2] = pColors[i*4+3] = NiColorA(1,1,1,1);

		pConnect[i*6] = i*4;
		pConnect[i*6+1] = i*4+2;
		pConnect[i*6+2] = i*4+1;
		pConnect[i*6+3] = i*4;
		pConnect[i*6+4] = i*4+3;
		pConnect[i*6+5] = i*4+2;
	}

	NiTriShapeDynamicData *pData = NiNew NiTriShapeDynamicData(
		iVertNum, pVerts, pNormals,
		pColors, pTexCoords, 1, NiGeometryData::NBT_METHOD_NONE,
		iTriNum, pConnect, iVertNum, iTriNum );

	m_spFogGeometry = NiNew NiTriShape(pData);
	m_spFogGeometry->Update(0.0f);

	//m_spFogGeometry->PrependController()
	//NiOptimize::OptimizeTriShape(m_spFogGeometry);
	//m_spFogGeometry->Update(0.0f);

	// 优化 
	return true;
}
//-----------------------------------------------------------------------------
NiTriShape* CFogSurfaceMaker::GetFogGeometry()
{
	return m_spFogGeometry;
}
//-----------------------------------------------------------------------------
list<CFogSurfaceMaker::stGridCoord>::iterator CFogSurfaceMaker::_DecideLocation(list<stGridCoord>::iterator iter)
{
	int iCurrX = (*iter)._iX;
	int iCurrY = (*iter)._iY;

	// 遍历周围8个grid
	for (int i=-1; i<2; i++)
	{
		for (int j=-1; j<2; j++)
		{
			if (i==0 && j==0)	// 忽略当前节点
			{
				continue;
			}

			static int iX;
			iX = iCurrX+i;
			static int iY;
			iY = iCurrY+j;
			// 判断当前 grid 坐标是否合法
			if (iX<0 || iY<0 || iX>=m_iNumGridX || iY>=m_iNumGridY)
			{
				// 不合法
				continue;
			}

			static int iIdx;
			iIdx = iY*m_iNumGridX+iX;
			if (m_pGridList[iIdx].locationState == GS_INSIDE)
			{
				m_pGridList[iIdx].locationState = GS_OUTSIDE;
				m_pGridList[iIdx].combineState = GS_DELETED;
				//_RecursiveDecideLocation(iX, iY);
				m_OpenList.push_back(stGridCoord(iX, iY));
			}
			else if(m_pGridList[iIdx].locationState == GS_HALFINSIDE)
			{
				m_pGridList[iIdx].locationState = GS_HALFOUTSIDE;
				m_pGridList[iIdx].combineState = GS_DELETED;
				m_OpenList.push_back(stGridCoord(iX, iY));
			}
			else if(m_pGridList[iIdx].locationState == GS_UNDERGROUND)
			{
				//m_pGridList[iIdx].locationState = GS_HALFOUTSIDE;
				m_pGridList[iIdx].combineState = GS_DELETED;
				//_RecursiveDecideLocation(iX, iY, iNumGridsX, iNumGridsY);
			}

		}
	}
	return m_OpenList.erase(iter);
}
//-----------------------------------------------------------------------------
void CFogSurfaceMaker::_CombineAllGrids()
{
	// 从下向上，从左向右遍历所有 grid. 
	// 对于每个 grid
	// 如果 combineState 为 GS_DELETED 忽略该节点
	// 如果 combineState 为 GS_UNDESIDED， 改为GS_COMBINED。将该节点作为当前节点。
	//	以矩形向右上合并 GS_UNDESIDED 节点，改变本节点的顶点索引。
	//	被合并的节点变为 GS_DELETE。

	int iGridID = 0;
	for (int i=0; i<m_iNumGridY; i++)	// 遍历列，向 Y 正方向
	{
		for (int j=0; j<m_iNumGridX; j++, iGridID++)	// 遍历行，向 X 正方向
		{
			if (m_pGridList[iGridID].combineState == GS_DELETED)
			{
				continue;
			}
			else if(m_pGridList[iGridID].combineState == GS_COMBINED)
			{
				// 不应该发生
				//Log("CFogSurfaceMaker::_RecusiveCombineGrid(), 合并 grid 发现不合理属性 GS_COMBINED");
				continue;
			}
			else
			{
				// GS_UNDESIDED
				_CombineGrid(j, i);
			}

		}
	}
}
//-----------------------------------------------------------------------------
void CFogSurfaceMaker::_CombineGrid(const int iCurrX, const int iCurrY)
{
	int iCurrGridIdx = iCurrY*m_iNumGridX+iCurrX;
	m_pGridList[iCurrGridIdx].combineState = GS_COMBINED;

	vector<int> gridIDList;
	//bool bCombine = true;
	// 向右方扩展
	for (int i=1; i<m_iNumGridX-iCurrX; i++)
	{
		int iX = iCurrX+i;
		int iY = iCurrY;

		int iGridIdx = iY*m_iNumGridX+iX;
		if (m_pGridList[iGridIdx].combineState == GS_DELETED)
		{
			break;
		}
		else if(m_pGridList[iGridIdx].combineState == GS_COMBINED)
		{
			break;
		}
		gridIDList.push_back(iGridIdx);
	}
	for (unsigned int j=0; j<gridIDList.size(); j++)
	{
		m_pGridList[gridIDList[j]].combineState = GS_DELETED;
		m_pGridList[iCurrGridIdx].i2++;
		m_pGridList[iCurrGridIdx].i3++;
	}

}
//-----------------------------------------------------------------------------
void CFogSurfaceMaker::Debug_OutputGrid(const char* pszFileName)
{
	FILE* pFile = fopen(pszFileName, "w");

	int idx = 0;
	for (int i=0; i<m_iNumGridX; i++)
	{
		for (int j=0; j<m_iNumGridY; j++, idx++)
		{
			//if (m_pGridList[idx].locationState==GS_OUTSIDE || m_pGridList[idx].locationState==GS_HALFOUTSIDE)
			//{
			//	fprintf(pFile, "O ");
			//}
			//else if(m_pGridList[idx].locationState==GS_INSIDE || m_pGridList[idx].locationState==GS_HALFINSIDE)
			//{
			//	fprintf(pFile, "I ");
			//}
			//else
			//{
			//	fprintf(pFile, "U ");
			//}
			if (m_pGridList[idx].combineState==GS_COMBINED )
			{
				fprintf(pFile, "C");
			}
			else if(m_pGridList[idx].combineState==GS_UNDESIDED)
			{
				fprintf(pFile, "U");
			}
			else if(m_pGridList[idx].combineState==GS_DELETED)
			{
				fprintf(pFile, "D");
			}
		}
		fprintf(pFile, "\n");
	}
	fclose(pFile);
}
//-----------------------------------------------------------------------------
#endif 