﻿/** @file 3DSExportor.h 
@brief 使用 lib3ds 将场景导出为 3ds 格式
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-06-10
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：本类只在场景编辑器中使用
</pre>*/
#pragma once
#ifndef CODE_INGAME

struct Lib3dsFile;
struct Lib3dsNode;

namespace SceneCore
{

// 3ds 格式导出类
class MAIN_ENTRY C3DSExportor
{
public:
	C3DSExportor(void);
	~C3DSExportor(void);

	/// 添加一个结点到导出列表 
	void AddSceneNode(NiAVObject* pkAVObj, int& iBeginNodeID, int& iEndNodeID);

	/// 将场景保存到 3ds 文件
	bool SaveToFile(const char* pszFilePath);

	/// 清除 Lib3dsFile 里所有结点
	void Clear();

	// 测试导出一个 node
	void Debug_Export3DSNode();

private:
	Lib3dsFile* m_p3dsFile;

};	// End of class

}; // End of namespace
#endif