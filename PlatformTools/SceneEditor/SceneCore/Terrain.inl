/// 获取顶点位置
inline const NiPoint3&	CTerrain::GetVertex(int x, int y)
{
	return m_pVertices[y*(m_iChunkNumX*GRIDINCHUNK+1) + x];
}
//-----------------------------------------------------------------
inline const NiPoint3&	CTerrain::GetVertex(int iIndex)
{
	return m_pVertices[iIndex];
}
//-----------------------------------------------------------------
inline float CTerrain::GetVertexHeight(int x, int y)
{
	return m_pVertices[y*(m_iChunkNumX*GRIDINCHUNK+1) + x].z;
}
//-----------------------------------------------------------------
inline float CTerrain::GetVertexAlpha(int x, int y)
{
	return m_pVertexClr[y*(m_iChunkNumX*GRIDINCHUNK+1) + x].a;
}
//-----------------------------------------------------------------
/// 获取第 y 行 x 列 顶点的 R
inline float CTerrain::GetVertexR(int x, int y)
{
	return m_pVertexClr[y*(m_iChunkNumX*GRIDINCHUNK+1) + x].r;
}
//-----------------------------------------------------------------
/// 获取第 y 行 x 列 顶点的 G
inline float CTerrain::GetVertexG(int x, int y)
{
	return m_pVertexClr[y*(m_iChunkNumX*GRIDINCHUNK+1) + x].g;
}
//-----------------------------------------------------------------
//-----------------------------------------------------------------
#ifndef CODE_INGAME
//-----------------------------------------------------------------
inline NiPoint3* CTerrain::GetNormals()
{
	return m_pNormals;
}

//-----------------------------------------------------------------
#endif