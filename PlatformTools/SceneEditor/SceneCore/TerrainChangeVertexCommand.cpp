﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include "TerrainChangeVertexCommand.h"
#include "Terrain.h"
#include "TerrainModifier.h"

CTerrainChangeVertexCommand::CTerrainChangeVertexCommand(CTerrain* pTerrain)
:	m_pTerrain(pTerrain)
{
}

CTerrainChangeVertexCommand::~CTerrainChangeVertexCommand(void)
{
}

// 执行该命令
void CTerrainChangeVertexCommand::DoCommand(void)
{
	if (this == NULL)
	{
		return;
	}

	NiPoint3* pVertices = m_pTerrain->m_pVertices;
	std::map<int, tVertexChange>::iterator it = m_VertexChanges.begin();
	for (; it!=m_VertexChanges.end(); it++)
	{
		pVertices[it->second.iIndex].z = it->second.fNewHeight;
	}
	_CalculateNormalAndNotifyChunk();
}

// 撤销该命令
void CTerrainChangeVertexCommand::UndoCommand(void)
{
	NiPoint3* pVertices = m_pTerrain->m_pVertices;
	std::map<int, tVertexChange>::iterator it = m_VertexChanges.begin();
	for (; it!=m_VertexChanges.end(); it++)
	{
		pVertices[it->second.iIndex].z = it->second.fOldHeight;
	}
	_CalculateNormalAndNotifyChunk();
}

void CTerrainChangeVertexCommand::_CalculateNormalAndNotifyChunk()
{
	// 依次改变顶点的法线, 必须在所有顶点位置改变后完成, 否则可能导致计算无效
	std::map<int, tVertexChange>::iterator it = m_VertexChanges.begin();
	for (; it!=m_VertexChanges.end(); it++)
	{
		NiPoint3& n  = m_pTerrain->m_pNormals[it->second.iIndex];
		n = CTerrainModifier::CalculateNormal( m_pTerrain, it->second.iIndex );
	}

	// 通知Chunks顶点改变
	for (set<int>::iterator iter = m_chunkset.begin(); iter != m_chunkset.end(); ++iter )
	{
		CTerrainModifier::NotifyChunkVertexChanged( m_pTerrain, *iter );
	}
}

// 增加一个改变的顶点 
void CTerrainChangeVertexCommand::AddVertexChange(const tVertexChange& vertChange)
{
	if (vertChange.fNewHeight == vertChange.fOldHeight)
	{
		return;
	}

	std::map<int, tVertexChange>::iterator it = m_VertexChanges.find(vertChange.iIndex);
	if (m_VertexChanges.end() != it)
	{
		it->second.fNewHeight = vertChange.fNewHeight;
		return;
	}
	else
	{
		m_VertexChanges[vertChange.iIndex] = vertChange;
	}
}

void CTerrainChangeVertexCommand::AddChangedChunk(int iChunk)
{
	m_chunkset.insert(iChunk);
}
#endif