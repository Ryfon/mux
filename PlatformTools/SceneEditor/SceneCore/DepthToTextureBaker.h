﻿/** @file DepthToTextureBaker.h 
@brief 使用
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：1.0
*	作    者：Shi Yazheng
*	完成日期：2008-05-19
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#ifndef CODE_INGAME
#include "Terrain.h"
#include "MRT_ColorDepthMaterial.h"

namespace SceneCore
{

/// 创建 SimpleFog 用的参数
struct MAIN_ENTRY stSimpleFogPrarm
{
	unsigned int	uiTextureSize;	// 目标纹理尺寸
	NiPoint2		kLBPoint;		// 烘焙目标场景左下点
	NiPoint2		kRUPoint;		// 烘焙目标场景右上点
	float			fStartZ;		// 世界坐标中 Z 轴上雾开始的坐标
	float			fEndZ;			// 世界坐标中 Z 轴上雾达到最浓的坐标。 在fStartZ 到 fEndZ 中间部分雾的浓度线性增加
	NiColor			kColor;			// 雾的颜色
	float			fSpeedU;		// UV 动画速度
	float			fSpeedV;
	char			szUVAnimTexture[256];	// UV 动画纹理文件
};

/// 将场景某区域深度烘焙到纹理 的烘焙器
class MAIN_ENTRY CDepthToTextureBaker
{
public:
	CDepthToTextureBaker(CTerrain* pkTerrain);
	~CDepthToTextureBaker(void);

	/// 增加场景物件
	void AddSceneObject(NiAVObject* pkObj);

	/// 烘焙场景深度到纹理
	bool Bake(const stSimpleFogPrarm param);

	NiSourceTexture* GetDepthTexture();

private:
	NiSourceTexturePtr	m_spFogTexture;		// 作为 Fog 贴图的 texture. 
	vector<NiAVObject*>	m_spSceneObjects;	// 所有需要写入深度纹理的场景物件
	float				m_fFogStart;		// 雾的起始高度
	float				m_fFogEnd;			// 雾的结束高度	从起始高度到结束高度雾的浓度线性增加
	NiColor				m_kColor;			// 雾的颜色
	CTerrain*			m_pkTerrain;		// 目标地形

};	// End of class

}	// End of namespace

#endif	// end of namespace