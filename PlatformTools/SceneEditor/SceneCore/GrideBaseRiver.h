﻿/** @file CGrideBaseRiver.h 
// 基于Grid 区域工具的水体工具
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2009-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME

#include "Terrain.h"
#include <vector>

#ifndef _GRIDEBASERIVER_H
#define _GRIDEBASERIVER_H

#define			GRIDINCHUNK			64
// 河流高度调整值，用于判定不需要的点
#define			ADJUST_HEIGHT		0.0

class MAIN_ENTRY CGrideBaseRiver
{
public:
	CGrideBaseRiver(void);
	~CGrideBaseRiver(void);

	// 初始化
	struct MAIN_ENTRY  stTriList 
	{
		unsigned int	uiP0,uiP1,uiP2;
	};

	struct MAIN_ENTRY stRiverPoint
	{
		NiPoint3 kVertex;				//河流顶点坐标
		NiColorA kVertexClr;			//河流的顶点颜色

	};

	//静态函数，获得水域静态指针	
	static	CGrideBaseRiver*	GetInstance();	

	bool Init(CTerrain* pTerrain, float fDesAlpha, float fHeight, NiColor kColor,std::set<int>& gridSet);
	
	void DoDispose();

	NiTriShapePtr GetRiverGeom()	{ return m_spRiverMesh;};

	unsigned int GetTrianglesCount()	{ return m_stTri.size();};

private:
	// 由TerrainIndex生成几何形
	bool _GenerateRiverGeom();

	// 创建半径多边形
	unsigned int _BuildTriangles();

	float _GetAlphaValue(NiPoint3 kPoint);

	bool _ExistPoint(int iTerrainIndex);

	// 获取指定iTerrainIndex在m_vGrideSet中的下标
	int _GetRiverVerticeIndex(int iTerrainIndex);

	// 根据传入的grid id，计算该grid中的第一点是否保存
	// 如果可以插入，则返回由iGridID换算出的iTerrainIndex
	// 否则 返回-1
	int _InsertPointToRiver(int iGridID);

private:
	CTerrain*					m_pTerrain;			// 目标地形
	float						m_fRiverHeight;		// 水面高度
	float						m_fAlphaDet;			// 每米alpha下降值
	NiColor						m_kColor;			// color of river
	vector<int>					m_vGridSet;			// all vertices of gird
	map<int,stRiverPoint>		m_mapRiverPointInfo;// 一个顶点的信息，包括坐标和颜色
	vector<stTriList>			m_stTri;			// 顶点索引	存储的是terrain的顶点索引
	NiTriShapePtr				m_spRiverMesh;		// 水体面片

	static CGrideBaseRiver*		m_spThis;
};

#pragma  make_public(CGrideBaseRiver)



#endif
#endif