﻿/** @file ShadowMapProcess.h 
 @brief Shadow Map 渲染DarkMap解决方案
 <pre>
 *	Copyright (c) 2008，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2008-03-17
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 </pre>*/

#ifndef SHADOWMAP_PROCESS_H
#define SHADOWMAP_PROCESS_H

class CTerrain;
class NiEntityRenderingContext;
class MAIN_ENTRY	CShadowMapProcess
{
public:
	/// 构造
	CShadowMapProcess( void ){}
	/// 析构
	virtual ~CShadowMapProcess( void ){}

	bool						Init( CTerrain *pTerrain, NiRenderer *pRenderer, NiCamera *pCamera, NiRenderView *pMainRenderView, NiPoint3 vLitDir );
	void						UnInit();
	void						Do( NiEntityRenderingContext* pkRenderingContext );
	NiRenderedTexturePtr		GetShadowTexture();

protected:

	static bool					_PreDepthCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );
	static bool					_PostDepthCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );
	static bool					_PreShadowMapCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );
	static bool					_PostShadowMapCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );

	CTerrain					*m_pTerrain;
	
	// light direction
	NiPoint3					m_vLightDir;	

	NiMaterialPtr				m_spDepthMtl;
	NiMaterialPtr	 			m_spBlurUMapMtl;
	NiMaterialPtr	 			m_spBlurVMapMtl;
	NiMaterialPtr				m_spShadowMapMtl;
	// shadow map
	NiRenderedTexturePtr		m_spDepthMap;
	// blur map/UV
	NiRenderedTexturePtr		m_spBlurUMap;
	NiRenderedTexturePtr		m_spBlurVMap;	
	NiRenderedTexturePtr		m_spShadowMap;

	// render target for shadow map
	NiRenderTargetGroupPtr		m_spRTGroupDepth;
	// render target for blur map
	NiRenderTargetGroupPtr		m_spRTGroupBlurU;
	NiRenderTargetGroupPtr		m_spRTGroupBlurV;
	NiRenderTargetGroupPtr		m_spRTGroupShadowMap;

	// help light, in light space / ortho view
	NiCameraPtr					m_spHelpCamera;
	NiRenderViewPtr				m_spMainRenderView;

	// main render step
	NiDefaultClickRenderStepPtr m_spRenderStep;

private:

	void						_setCameraInLightSpace();
	void						_setCameraInOrthoSpace();

};

#endif