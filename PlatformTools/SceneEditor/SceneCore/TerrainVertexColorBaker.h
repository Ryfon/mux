﻿#pragma once
/** @file TerrainVertexColorBaker.h 
@brief 灯光烘焙到地形顶点色
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：1.0
*	作    者：Shi Yazheng
*	完成日期：2008-07-14
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef CODE_INGAME

#include "Terrain.h"

class MAIN_ENTRY CTerrainVertexColorBaker
{
public:
	CTerrainVertexColorBaker(CTerrain* pTerrain);
	~CTerrainVertexColorBaker(void);

	// 用颜色填充地形所有顶点色
	// 去除修改alpha的功能，以保证地形隐藏 [5/18/2009 hemeng]
	void FloodTerrainVertexColor(NiColor kColor);

	/**
	*	<BR>功能说明：烘焙灯光列表到地形顶点色
	*	<BR>可访问性：public
	*	<BR>注    释：
	*	@param	lightList[in]	灯光列表
	*	@param	bMultiply[in]	与原顶点色的计算方式　true - 乘　　　false - 加
	*	@param  fTerrainAmbient[in] 地形的 ambient 系数
	*	@param	command[out]	该操作生成的　command
	*	@return 无
	*/
	void BakeLightsToTerrainVertex(std::vector<NiLight*>* lightList, bool bMultiply, float fTerrainAmbient, CTerrainChangeVertexColorCommand& command);

	/**
	*	<BR>功能说明：光照一个顶点
	*	<BR>可访问性：public
	*	<BR>注    释：
	*	@param	kPos[in]		
	*	@param	kNormal[in]		
	*	@param	kLight[in]		
	*	@param	kDiffuseAccum[in]		
	*	@param	kAmbientAccumY[in]		
	*	@return 无
	*/
	void Light(NiPoint3 kPos, NiPoint3 kNormal, NiLight* pkLight, NiColor& kDiffuseAccum, NiColor& kAmbientAccum);

private:
	// 目标地形
	CTerrain*	 m_pTerrain;
};

#include "TerrainVertexColorBaker.inl"
#endif
