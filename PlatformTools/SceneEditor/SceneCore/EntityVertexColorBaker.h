﻿#pragma once

/** @file EntityVertexColorBaker.h 
@brief 灯光烘焙到物件顶点色
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：1.0
*	作    者：hemeng
*	完成日期：2010-04-12
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef _ENTITYVERTEXCOLORBAKER_H_
#define _ENTITYVERTEXCOLORBAKER_H_

#ifndef CODE_INGAME

class MAIN_ENTRY CEntityVertexColorBaker
{
public:
	CEntityVertexColorBaker(void);
	~CEntityVertexColorBaker(void);

	// 烘焙单个灯光 [4/12/2010 hemeng]
	static void BakeLight(NiAVObject* pkObj,NiLight* pkLight);
	static void FloodEntityVertexColor(NiAVObject* pkObj,NiColorA kColor);

protected:
	static void _BackPosLight(NiPoint3 kPos, NiPoint3 kNormal, NiLight* pkLight, NiColor& kDiffuseAccum, NiColor& kAmbientAccum);
	static void _BakeLight(NiAVObject* pkObj,NiLight* pkLight);

private:

};

#endif // end of define .h [4/12/2010 hemeng]

#endif // end of CODE_INGAME [4/12/2010 hemeng]