﻿/** @file IDepthAlphaEffect.h 
@brief alpha 值与 深度 相关特效接口
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.4
*	作    者：Shi Yazheng
*	完成日期：2008-04-02
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
#include <NiEntityRenderingContext.h>

namespace SceneCore
{

NiSmartPointer(IDepthAlphaEffect);

/// alpha 与 depth value 相关特效接口
class  MAIN_ENTRY IDepthAlphaEffect : public NiAVObject
{
		NiDeclareRTTI;
public:
	IDepthAlphaEffect(void);
	virtual ~IDepthAlphaEffect(void);

	/// 设置深度纹理
	virtual void SetDepthTexture(NiTexturePtr pkDepthTexture);

	/// 销毁 释放占用资源，智能指针置 NULL
	virtual void Destory();

	/// 更新
	virtual void Update(float fTime);

	///构造可视集
	virtual void BuildVisibleSet(NiEntityRenderingContextPtr	pkRenderingCondtex);
};

};