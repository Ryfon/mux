﻿#include "StdAfx.h"
#include "Vegetation.h"

CVegetation::CVegetation(void)
:  m_spNode(0), m_pExtraData(0)
{
}

CVegetation::~CVegetation(void)
{
	Destroy();
}


bool	CVegetation::Create( const char *pszNifFile )
{	
	m_pExtraData = NiNew NiStringExtraData( NiFixedString(pszNifFile ) );
	m_pExtraData->SetValue( pszNifFile );
	return true;
}

void	CVegetation::Destroy()
{
	m_spNode = 0;
	SAFE_DELETE( m_pExtraData );
}