﻿/** @file ObjectsManager.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-10-04
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 </pre>*/

#ifndef CODE_INGAME

#ifndef OBJECTS_MGR_H 
#define OBJECTS_MGR_H



#define TRANSLATION "Translation"
#define ROTATION "Rotation"
#define SCALE "Scale"
#define NPCCREATORFILENAME ".\\NPCCreatorInfo.xml"
#define NPCCREATORNOMNGFILENAME ".\\NPCCreatorNOMng.xml"

/// 前置声明
class CTerrain;
class CEditableBaseObj;
class NiEntityInterface;
/**
 @brief 物件管理器
 *
 *	负责场景中物件的加载,移除,点选,,, 分担terrain的物件管理工作
 */
class MAIN_ENTRY CObjectsManager
{
public:
	/// 构造
	CObjectsManager( CTerrain* );
	/// 析构
	virtual ~CObjectsManager(void);

	/// 添加单个物件, 添加名字索引
	CEditableBaseObj*			AddObject(NiEntityInterface*, NiPoint3& vPos, const char *pszNifFile );
	
	CEditableBaseObj* InitObject(NiEntityInterface* pEntityInf, const char* pszNifFile);

	// 添加单个物件 函数作废
	//CEditableBaseObj*			AddObject( NiPoint3& vPos, CEditableBaseObj* pObject );

	/// 移除物件
	bool						RemoveObject( NiEntityInterface* );

	/// 移除所有物件
	void						RemoveAllObjects();

	/// 更新物件
	void						UpdateObject( NiEntityInterface* , NiFixedString& pkPropertyName);

	/// 判断是否有该物件
	bool						IsExited(NiEntityInterface*);	

	CEditableBaseObj			*GetEditalbeBaseObject( NiEntityInterface* );

	/// 遍历,用于调试
	void PrintAll();
	//从文件中加载Object
	int Load(const char	*pszFile);
	int Save(const char* pszFile);

	int LoadNPCCreator();
	int SaveNPCCreator();

protected:

	/// 托管terrain
	CTerrain*					m_pTerrain;

	map< NiEntityInterface*, CEditableBaseObj* > m_ObjectMap;


};

#endif//OBJECTS_MGR_H 
#endif // end #ifndef CODE_INGAME
