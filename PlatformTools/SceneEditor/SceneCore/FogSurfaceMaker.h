﻿/** @file FogSurfaceMaker.h 
@brief 雾表面生成器
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-05-19
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#ifndef CODE_INGAME
#include "DepthToTextureBaker.h"

namespace SceneCore
{

/// 程序生成雾表面
class MAIN_ENTRY CFogSurfaceMaker
{
public:
	CFogSurfaceMaker(CTerrain* pkTerrain);
	~CFogSurfaceMaker(void);

	/// 创建雾表面几何体
	bool Make(const stSimpleFogPrarm param);

	/// 获取创建好的雾表面
	NiTriShape* GetFogGeometry();

	enum eGridState
	{
		GS_UNKNOW = -1,// position state
		GS_OUTSIDE = 0,	
		GS_HALFOUTSIDE,
		GS_UNDERGROUND,
		GS_HALFINSIDE,
		GS_INSIDE,
		GS_UNDESIDED,	// combine state
		GS_COMBINED,	// 被合并，由父节点绘制
		GS_DELETED,		// 不合并,不单独绘制
	};
	struct stFogGrid
	{
		int i0, i1, i2, i3;	// 四个顶点索引 按 左 上 右 下 顺序
		eGridState	locationState;	// 位置状态
		eGridState	combineState;	// 合并状态
	};

	/// 测试函数
	void Debug_OutputGrid(const char* pszFileName);
	struct stGridCoord
	{
		stGridCoord(int x, int y)
		{
			_iX = x;
			_iY = y;
		}

		int _iX;
		int _iY;
	};
private:


	//决定每个 grid 的位置
	list<stGridCoord>::iterator _DecideLocation(list<stGridCoord>::iterator iter);

	/// 合并所有可能合并的 Grid
	void _CombineAllGrids();

	/// 以 iX, iY 为起点向右上以矩形合并 grid
	void _CombineGrid(const int iCurrX, const int iCurrY);

	CTerrain*			m_pkTerrain;		// 目标地形
	NiTriShapePtr		m_spFogGeometry;	// 雾表面面片

	stFogGrid*			m_pGridList;		// 雾表面 Grid 列表 
	int					m_iNumGridX;		// x 方向 Fog surface 的 grid 数
	int					m_iNumGridY;		// y 方向 Fog surface 的 grid 数

	list<stGridCoord>	m_OpenList;			// 待确定的 grid 坐标列表.
};

} // end of namespace

#endif // CODE_INGAME
