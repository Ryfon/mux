﻿/** @file SimpleFog.h 
@brief 使用
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.5
*	作    者：Shi Yazheng
*	完成日期：2008-05-19
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#ifndef CODE_INGAME

#include "Terrain.h"
#include "DepthToTextureBaker.h"
#include "FogSurfaceMaker.h"

namespace SceneCore
{
/**
@brief 使用预先烘焙到纹理 alpha 通道来模拟层雾效果
* 
*/
class MAIN_ENTRY CSimpleFog
{

public:
	CSimpleFog(CTerrain* pkTerrain);
	~CSimpleFog(void);

	/// 初始化
	bool Init(const stSimpleFogPrarm param);

	/// 获取雾表面
	NiTriShape* GetFogSurface();

	/// 获取雾 alpha 贴图
	NiTexture* GetFogTexture();

	/// 保存 simple fog
	bool SaveSimpleFog(const char* pszFogTexFileName, const char* pszFogSurfaceFileName);

private:
	CTerrain*				m_pkTerrain;		// 目标地形
	CDepthToTextureBaker*	m_pkDepthTexBaker;	// Fog Texture 生成器
	CFogSurfaceMaker*		m_pkSurfaceMaker;	// Fog surface geometry maker

	// 雾本身的属性
	unsigned int			m_uiFogTextureSize;	// Fog Texture尺寸
	NiPoint2				m_kLBPoint;			// 烘焙目标场景左下点
	NiPoint2				m_kRUPoint;			// 烘焙目标场景右上点
	float					m_fStartZ;			// 世界坐标中 Z 轴上雾开始的坐标
	float					m_fEndZ;			// 世界坐标中 Z 轴上雾达到最浓的坐标。 在fStartZ 到 fEndZ 中间部分雾的浓度线性增加
	NiColor					m_kColor;			// 雾的颜色
	float					m_fSpeedU;
	float					m_fSpeedV;

};	// end of class

}	// end of namespace

#endif // end of CODE_INGAME
