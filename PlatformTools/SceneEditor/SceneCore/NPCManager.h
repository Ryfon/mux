﻿/** @file NPCManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangshaodong
*	完成日期：2007-12-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME

#ifndef _NPCMANAGER_H
#define _NPCMANAGER_H

class CNPC;

class CNPCManager
{
	typedef pair<DWORD, int> NO_PAIR;
public:
	CNPCManager(void);
public:
	virtual ~CNPCManager(void);
public:
	int LoadNPC(const string& strXmlFileName);
	int CleanUp();

	int SaveNPC(const string& strXmlFileName);

	size_t GetNPCCount() const {return m_NPCList.size();};
	CNPC* GetNPC(DWORD dwNPCNO);
	
	CNPC* AddNPC();
	int DelNPC(DWORD dwNPCNO);

	const map<DWORD, CNPC*>& GetNPCList() const {return m_NPCList;}
	int LoadNPCNOList(const string& strXmlFileName);
	int SaveNPCNOList(const string& strXmlFileName);
public:	
	map<DWORD, CNPC*> m_NPCList;
	
private:
	string m_FileVersion;
	vector<NO_PAIR> m_NOList;
	DWORD GetNPCNewNO();
	int SetNPCNewNO(DWORD dwNO);
};

#endif

#endif