﻿#include "stdafx.h"

#ifndef CODE_INGAME

#include "tinyxml.h"
#include "NPCCreator.h"

CNPCCreator::CNPCCreator(void)
{
	m_NPCCreatorNO = 1;
	m_CreatorName = "NPC生成器";
	m_MapNO = 1;
	m_MapName = "";
	m_XPos = 0;
	m_YPos = 0;
	m_Row = 0;
	m_Col = 0;
	m_Radius = 0;
	m_NPCNo = 0;
	m_RefreshNum = 1;
	m_TimeInterval = 0;
	m_RefreshType = 2;
	m_CreateType = 1;
	m_Direction = 0;	//朝向 0：北 1： 东北 2：东 3：东南 4：南 5：西南  6：西 7：西北

	//M1新增属性的初始化
	m_RadiusX = 0;
	m_RadiusY = 0;
	m_AreaMethod = 1;
	m_TimeMethod = 1;
	m_SupplyMethod = 1;
	m_TimeFormat = 4;
	m_StartTime = "00";
	m_EndTime = "01";
}

CNPCCreator::~CNPCCreator(void)
{

}

int CNPCCreator::LoadData(const TiXmlElement* pXmlNPCCreator)
{
	m_NPCCreatorNO = atol(pXmlNPCCreator->Attribute("NPCCreatorNO"));
	m_CreatorName = pXmlNPCCreator->Attribute("NPCCreatorName");
	m_MapNO = atol(pXmlNPCCreator->Attribute("MapNo"));
	m_MapName = pXmlNPCCreator->Attribute("MapName");
	m_XPos = atol(pXmlNPCCreator->Attribute("XPos"));
	m_YPos = atol(pXmlNPCCreator->Attribute("YPos"));
	m_Row = atol(pXmlNPCCreator->Attribute("Row"));
	m_Col = atol(pXmlNPCCreator->Attribute("Col"));
	m_Radius = atol(pXmlNPCCreator->Attribute("Radius"));
	m_NPCNo = atol(pXmlNPCCreator->Attribute("NPCNo"));
	m_RefreshNum = atol(pXmlNPCCreator->Attribute("RefreshNum"));
	m_TimeInterval = atol(pXmlNPCCreator->Attribute("TimeInterval"));
	m_RefreshType = atol(pXmlNPCCreator->Attribute("RefreshType"));
	m_CreateType = atol(pXmlNPCCreator->Attribute("CreateType"));
	m_Direction = atol(pXmlNPCCreator->Attribute("Direction"));
	return 0;
}

int CNPCCreator::SaveData(TiXmlElement *pXmlNPCCreator)
{
	pXmlNPCCreator->SetAttribute("NPCCreatorNO", m_NPCCreatorNO);
	pXmlNPCCreator->SetAttribute("NPCCreatorName", m_CreatorName.c_str());
	pXmlNPCCreator->SetAttribute("MapNo", m_MapNO);
	pXmlNPCCreator->SetAttribute("MapName", m_MapName.c_str());
	pXmlNPCCreator->SetAttribute("XPos", m_XPos);
	pXmlNPCCreator->SetAttribute("YPos", m_YPos);
	pXmlNPCCreator->SetAttribute("Row", m_Row);
	pXmlNPCCreator->SetAttribute("Col", m_Col);
	pXmlNPCCreator->SetAttribute("Radius", m_Radius);
	pXmlNPCCreator->SetAttribute("NPCNo", m_NPCNo);
	pXmlNPCCreator->SetAttribute("RefreshNum", m_RefreshNum);
	pXmlNPCCreator->SetAttribute("TimeInterval", m_TimeInterval);
	pXmlNPCCreator->SetAttribute("RefreshType", m_RefreshType);
	pXmlNPCCreator->SetAttribute("CreateType", m_CreateType);
	pXmlNPCCreator->SetAttribute("Direction", m_Direction);
	return 0;
}

int CNPCCreator::LoadDataM1(const TiXmlElement* pXmlNPCCreator)
{
	m_NPCCreatorNO = atol(pXmlNPCCreator->Attribute("NPCCreatorNO"));
	m_CreatorName = pXmlNPCCreator->Attribute("NPCCreatorName");
	m_AreaMethod = atol(pXmlNPCCreator->Attribute("AreaMethod"));
	m_CreateType = atol(pXmlNPCCreator->Attribute("TimeMethod"));
	m_RefreshType = atol(pXmlNPCCreator->Attribute("SupplyMethod"));

	unsigned int uiLeft = atol(pXmlNPCCreator->Attribute("Left"));
	unsigned int uiTop = atol(pXmlNPCCreator->Attribute("Top"));
	unsigned int uiRight = atol(pXmlNPCCreator->Attribute("Right"));
	unsigned int uiBottom = atol(pXmlNPCCreator->Attribute("Bottom"));
	m_XPos = (uiLeft + uiRight) / 2;
	m_YPos = (uiTop + uiBottom) / 2;	
	m_RadiusX = (uiRight - uiLeft) / 2;
	m_RadiusY = (uiBottom - uiTop) / 2;

	m_TimeFormat = atol(pXmlNPCCreator->Attribute("TimeFormat"));
	m_StartTime = pXmlNPCCreator->Attribute("StartTime");
	m_EndTime = pXmlNPCCreator->Attribute("EndTime");

	for(const TiXmlElement* pXmlNPCTargetList = pXmlNPCCreator->FirstChildElement();pXmlNPCTargetList;pXmlNPCTargetList = pXmlNPCTargetList->NextSiblingElement())
	{
		NPCTarget* pTarget = new NPCTarget;
		pTarget->NPCNo = atol(pXmlNPCTargetList->Attribute("NPCNo"));
		pTarget->Num = atoi(pXmlNPCTargetList->Attribute("Num"));
		pTarget->Direction = atoi(pXmlNPCTargetList->Attribute("Direction"));
		m_NPCTargetList.push_back(pTarget);
	}
	return 0;
}

int CNPCCreator::SaveDataM1(TiXmlElement *pXmlNPCCreator)
{
	pXmlNPCCreator->SetAttribute("NPCCreatorNO", m_NPCCreatorNO);
	pXmlNPCCreator->SetAttribute("NPCCreatorName", m_CreatorName.c_str());
	pXmlNPCCreator->SetAttribute("AreaMethod", m_AreaMethod);
	pXmlNPCCreator->SetAttribute("TimeMethod", m_CreateType);
	pXmlNPCCreator->SetAttribute("SupplyMethod", m_RefreshType);

	pXmlNPCCreator->SetAttribute("Left", m_XPos - m_RadiusX);
	pXmlNPCCreator->SetAttribute("Top", m_YPos - m_RadiusY);
	pXmlNPCCreator->SetAttribute("Right", m_XPos + m_RadiusX);
	pXmlNPCCreator->SetAttribute("Bottom", m_YPos + m_RadiusY);

	pXmlNPCCreator->SetAttribute("TimeFormat", m_TimeFormat);
	pXmlNPCCreator->SetAttribute("StartTime", m_StartTime.c_str());
	pXmlNPCCreator->SetAttribute("EndTime", m_EndTime.c_str());

	int TargetNum = 0;
	for (int index = 0;index < static_cast<int> (m_NPCTargetList.size());++index)
	{
		if (m_NPCTargetList[index]->NPCNo == 0)
		{
			continue;
		}
		TiXmlElement* pXmlNPCTarget = new TiXmlElement("NPC");
		pXmlNPCTarget->SetAttribute("NPCNo", m_NPCTargetList[index]->NPCNo);
		pXmlNPCTarget->SetAttribute("Num", m_NPCTargetList[index]->Num);
		pXmlNPCTarget->SetAttribute("Direction", m_NPCTargetList[index]->Direction);
		pXmlNPCCreator->LinkEndChild(pXmlNPCTarget);
		TargetNum++;
	}

	pXmlNPCCreator->SetAttribute("SortNum", TargetNum);

	return 0;
}

int CNPCCreator::SetDirection(float fAngle)
{
	float fDegree = (fAngle * 180.0f) / 3.1415926535897932f;

	if (fDegree < 0.0f)
	{
		fDegree += 360.0f;
	}

	int iDirection = 0;
	iDirection = static_cast<int>( fDegree / 22.5f);

	switch(iDirection)
	{
		case 15 :this->m_Direction = 0;break;
		case 0 :this->m_Direction = 0;break;
		case 1 :this->m_Direction = 1;break;
		case 2 :this->m_Direction = 1;break;
		case 3 :this->m_Direction = 2;break;
		case 4 :this->m_Direction = 2;break;
		case 5 :this->m_Direction = 3;break;
		case 6 :this->m_Direction = 3;break;
		case 7 :this->m_Direction = 4;break;
		case 8 :this->m_Direction = 4;break;
		case 9 :this->m_Direction = 5;break;
		case 10 :this->m_Direction = 5;break;
		case 11 :this->m_Direction = 6;break;
		case 12 :this->m_Direction = 6;break;
		case 13 :this->m_Direction = 7;break;
		case 14 :this->m_Direction = 7;break;
		default : this->m_Direction = 0;break;
	}
	
	if (this->m_AreaMethod == 1)
	{
		this->m_NPCTargetList[0]->Direction = this->m_Direction;
	}
	return this->m_Direction;
}

int CNPCCreator::SetPosition(int iXPos, int iYPos)
{
	this->m_XPos = iXPos;
	this->m_YPos = iYPos;
	this->m_Col = iXPos;
	this->m_Row = iYPos;

	return 0;
}

int CNPCCreator::AddNPCTarget(unsigned int NPCNo, unsigned short Num, unsigned short Direction)
{
	NPCTarget* newTarget = new NPCTarget;
	newTarget->NPCNo = NPCNo;
	newTarget->Num = Num;
	newTarget->Direction = Direction;
	this->m_NPCTargetList.push_back(newTarget);
	return 0;
}

int CNPCCreator::UpdateNPCTarget(int index, unsigned int NPCNo, unsigned short Num)
{
	while(index >= static_cast<int>(m_NPCTargetList.size()) )
	{
		this->AddNPCTarget(0, 0, 0);
	}

	this->m_NPCTargetList[index]->NPCNo = NPCNo;
	this->m_NPCTargetList[index]->Num = Num;

	return 0;
}

#endif