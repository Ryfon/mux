﻿#include "StdAfx.h"
#include "TerrainGridBaseRegionManager.h"

#ifndef CODE_INGAME

using namespace SceneCore;

// 静态成员声明
CTerrainGridBaseRegionManager*			CTerrainGridBaseRegionManager::ms_pInstance = NULL;
//---------------------------------------------------------------------------------------
CTerrainGridBaseRegionManager::CTerrainGridBaseRegionManager(void)
:	m_pOnEditRegion(NULL)
{
}
//---------------------------------------------------------------------------------------
CTerrainGridBaseRegionManager::~CTerrainGridBaseRegionManager(void)
{
}
//---------------------------------------------------------------------------------------
CTerrainGridBaseRegionManager* CTerrainGridBaseRegionManager::GetInstance()
{
	if (ms_pInstance == NULL)
	{
		ms_pInstance = new CTerrainGridBaseRegionManager;
	}

	return ms_pInstance;
}
//---------------------------------------------------------------------------------------
// 添加一个区域
bool CTerrainGridBaseRegionManager::AddGridBaseRegion(CTerrainGridBaseRegion* pRegion)
{
	if (pRegion == NULL)
	{
		return false;
	}

	// 判断 region list 中是否已有该名称的 region
	if (GetGridBaseRegion(pRegion->GetName()) != NULL)
	{
		return false;
	}

	m_RegionList[pRegion] = NULL;

	return true;
}
//---------------------------------------------------------------------------------------
bool CTerrainGridBaseRegionManager::AddGridBaseRegion(const char* pszName)
{
	if (GetGridBaseRegion(pszName) != NULL)
	{
		return false;
	}

	CTerrainGridBaseRegion* pRegion = new CTerrainGridBaseRegion(pszName);
	m_RegionList[pRegion] = NULL;
	return true;
}
//---------------------------------------------------------------------------------------
// 根据区域名称获取
CTerrainGridBaseRegion* CTerrainGridBaseRegionManager::GetGridBaseRegion(const char* pszName)
{
	std::map<CTerrainGridBaseRegion*, NiGeometry*>::iterator iter = m_RegionList.begin();
	while (iter != m_RegionList.end())
	{
		if (strcmp(iter->first->GetName(), pszName) == 0)
		{
			return iter->first;
		}

		iter++;
	}

	return NULL;
}
//---------------------------------------------------------------------------------------
// 获取/设置 当前正被编辑的区域
CTerrainGridBaseRegion* CTerrainGridBaseRegionManager::GetOnEditRegion()
{
	return m_pOnEditRegion;
}
//---------------------------------------------------------------------------------------
void CTerrainGridBaseRegionManager::SetOnEditRegion(CTerrainGridBaseRegion* pRegion)
{
	m_pOnEditRegion = pRegion;
}
//---------------------------------------------------------------------------------------
void  CTerrainGridBaseRegionManager::SetOnEditRegion(const char* pszName)
{
	CTerrainGridBaseRegion* pRegion = GetGridBaseRegion(pszName);
	if (pRegion != NULL)
	{
		m_pOnEditRegion = pRegion;
	}
}
//---------------------------------------------------------------------------------------
void CTerrainGridBaseRegionManager::RemoveRegion(const char* pszName)
{
	if (NULL == pszName)
	{
		return;
	}

	// 删除一个区域
	if (m_pOnEditRegion!=NULL && strcmp(m_pOnEditRegion->GetName(), pszName)==0)
	{
		m_pOnEditRegion = NULL;
	}

	map<CTerrainGridBaseRegion*, NiGeometry*>::iterator iter = m_RegionList.begin();
	while (iter != m_RegionList.end())
	{
		if (strcmp(pszName, iter->first->GetName()) == 0)
		{
			// 在 m_RegionList 中找到该区域和其对应的几何体
			if (iter->second!=NULL && iter->second->GetRefCount()!=0)
			{
				NiDelete iter->second;
			}

			delete iter->first;
			m_RegionList.erase(iter);
			return;
		}
		iter++;
	}
}
//---------------------------------------------------------------------------------------
NiGeometry* CTerrainGridBaseRegionManager::GetOnEditRegionGeometry(CTerrain* pTerrain)
{
	if (m_pOnEditRegion == NULL)
	{
		return NULL;
	}
	else if (m_pOnEditRegion->IsChanged())
	{
		NiGeometry* pOldGeom = m_RegionList[m_pOnEditRegion];
		if (NULL != pOldGeom)
		{
			NiDelete pOldGeom;
		}
		m_RegionList[m_pOnEditRegion] = GenerateParticlesFromGridBaseRegion(pTerrain, m_pOnEditRegion);;
		m_pOnEditRegion->SetChanged(false);
	}

	return m_RegionList[m_pOnEditRegion];
}
//---------------------------------------------------------------------------------------
const std::vector<string>* CTerrainGridBaseRegionManager::GetRegionNameList()
{
	static vector<string> nameList;
	nameList.clear();

	std::map<CTerrainGridBaseRegion*, NiGeometry*>::iterator iter = m_RegionList.begin();
	while (iter != m_RegionList.end())
	{
		nameList.push_back(iter->first->GetName());

		iter++;
	}

	return &nameList;
}
//---------------------------------------------------------------------------------------
void CTerrainGridBaseRegionManager::Dispose()
{
	map<CTerrainGridBaseRegion*, NiGeometry*>::iterator iter = m_RegionList.begin();
	while (iter != m_RegionList.end())
	{
		if (iter->second!=NULL && iter->second->GetRefCount()!=0)
		{
			NiDelete iter->second;
		}

		if (iter->first != NULL)
		{
			delete iter->first;
		}
		iter++;
	}

	ms_pInstance = NULL;
}
//---------------------------------------------------------------------------------------
#endif