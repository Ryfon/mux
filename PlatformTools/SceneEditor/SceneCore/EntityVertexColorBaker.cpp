﻿#include "StdAfx.h"
#include "EntityVertexColorBaker.h"

#ifndef CODE_INGAME

CEntityVertexColorBaker::CEntityVertexColorBaker(void)
{
}

CEntityVertexColorBaker::~CEntityVertexColorBaker(void)
{
}

void CEntityVertexColorBaker::FloodEntityVertexColor(NiAVObject *pkAVObj, NiColorA kColor)
{
	if (NULL == pkAVObj)
	{
		return;
	}

	if ( pkAVObj->GetAppCulled() || (!NiIsKindOf(NiNode, pkAVObj) && !NiIsKindOf(NiTriBasedGeom, pkAVObj)) )
	{
		//不是合法节点
		return;
	}

	//如果是NiNode则递归调用
	if ( NiIsKindOf( NiNode,pkAVObj ) )
	{
		NiNode* pkNode = (NiNode*)pkAVObj;
		//获得子节点个数
		int num_pkNode = pkNode->GetChildCount();
		for (int i = 0; i < num_pkNode; i ++ )
		{
			FloodEntityVertexColor( pkNode->GetAt(i),kColor );

		}
	}

	else	if( NiIsKindOf( NiTriBasedGeom,pkAVObj ) )
	{

		//如果是NiTriBasedGeom类型则替换顶点
		{	
			NiTriBasedGeom* pkGeom = (NiTriBasedGeom*)pkAVObj;

			//获得pkGeom的NiGeometryData
			NiGeometryData* pkGeomData = pkGeom->GetModelData();
			pkGeomData->SetConsistency(pkGeomData->MUTABLE);

			//顶点颜色列表
			NiColorA * pkColorList	= pkGeomData->GetColors();
			if (NULL == pkColorList)
			{
				pkGeomData->CreateColors(true);

				// 设置顶点颜色模式 [4/15/2010 hemeng]
				NiVertexColorProperty* pkVertexColorPro = 
					NiDynamicCast(NiVertexColorProperty, pkAVObj->GetProperty(NiProperty::VERTEX_COLOR));

				if (NULL == pkVertexColorPro)
				{
					pkVertexColorPro = NiNew NiVertexColorProperty();
					pkAVObj->AttachProperty(pkVertexColorPro);
				}

				pkColorList = pkGeomData->GetColors();
			}	

			// 设置顶点颜色模式 [4/15/2010 hemeng]
			NiVertexColorProperty* pkVertexColorPro = 
				NiDynamicCast(NiVertexColorProperty, pkAVObj->GetProperty(NiProperty::VERTEX_COLOR));
			pkVertexColorPro->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE);	
			pkVertexColorPro->SetLightingMode( NiVertexColorProperty::LIGHTING_E_A_D );
			
			int iNumVertTotal = pkGeomData->GetVertexCount();

			for (int iVertIdx=0; iVertIdx< iNumVertTotal; iVertIdx++)
			{
				pkColorList[iVertIdx] = kColor;
			}
			pkGeomData->SetActiveVertexCount( iNumVertTotal );
			pkGeomData->MarkAsChanged( pkGeomData->COLOR_MASK );

			pkAVObj->Update( 0.0f );
			pkAVObj->UpdateProperties();
			pkAVObj->UpdateEffects();

		}

	}
}

void CEntityVertexColorBaker::BakeLight(NiAVObject* pkObj,NiLight* pkLight)
{
	if (NULL == pkObj || NULL == pkLight)
	{
		return;
	}

	if ( pkObj->GetAppCulled() || (!NiIsKindOf(NiNode, pkObj) && !NiIsKindOf(NiTriBasedGeom, pkObj)) )
	{
		//不是合法节点
		return;
	}

	if (NiIsKindOf(NiDirectionalLight, pkLight))
	{
		/*NiDirectionalLight* pDirLight = NiDynamicCast(NiDirectionalLight, pkLight);
		kLightVector = -(pDirLight->GetWorldDirection());
		kLightVector.Unitize();*/

		// 不处理方向光，场景物件都受动态方向光影响 [4/28/2010 hemeng]
		return;
	}

	//如果是NiNode则递归调用
	if ( NiIsKindOf( NiNode,pkObj ) )
	{
		NiNode* pkNode = (NiNode*)pkObj;
		//获得子节点个数
		int num_pkNode = pkNode->GetChildCount();
		for (int i = 0; i < num_pkNode; i ++ )
		{
			BakeLight( pkNode->GetAt(i), pkLight );

		}
	}

	else	if( NiIsKindOf( NiTriBasedGeom,pkObj ) )
	{

		//如果是NiTriBasedGeom类型则替换顶点
		{	
			NiTriBasedGeom* pkGeom = (NiTriBasedGeom*)pkObj;

			//获得pkGeom的NiGeometryData
			NiGeometryData* pkGeomData = pkGeom->GetModelData();
			pkGeomData->SetConsistency(pkGeomData->MUTABLE);

			//顶点颜色列表
			NiColorA * pkColorList	= pkGeomData->GetColors();
			if (NULL == pkColorList)
			{
				pkGeomData->CreateColors(true);				

				// 设置顶点颜色模式 [4/15/2010 hemeng]
				NiVertexColorProperty* pkVertexColorPro = 
					NiDynamicCast(NiVertexColorProperty, pkObj->GetProperty(NiProperty::VERTEX_COLOR));

				if (NULL == pkVertexColorPro)
				{
					pkVertexColorPro = NiNew NiVertexColorProperty();
					pkObj->AttachProperty(pkVertexColorPro);
				}			

				pkColorList = pkGeomData->GetColors();
			}

			NiVertexColorProperty* pkVertexColorPro = 
				NiDynamicCast(NiVertexColorProperty, pkObj->GetProperty(NiProperty::VERTEX_COLOR));
			pkVertexColorPro->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE);	
			pkVertexColorPro->SetLightingMode( NiVertexColorProperty::LIGHTING_E_A_D );
			
			_BakeLight(pkObj,pkLight);

		}

	}
}

void CEntityVertexColorBaker::_BakeLight(NiAVObject *pkObj,NiLight *pkLight)
{
	if (NULL == pkObj || NULL == pkLight)
	{
		return;
	}

	if ( !NiIsKindOf(NiTriBasedGeom, pkObj)) 
	{
		//不是合法节点
		return;
	}

	NiTriBasedGeom* pkGeom = (NiTriBasedGeom*)pkObj;
	NiTransform pkWorldTransform = pkGeom->GetWorldTransform();

	//获得pkGeom的NiGeometryData
	NiGeometryData* pkGeomData = pkGeom->GetModelData();
	pkGeomData->SetConsistency(pkGeomData->MUTABLE);

	int iNumVertTotal = pkGeomData->GetVertexCount();
	NiColorA * pkColorList	= pkGeomData->GetColors();	
	const NiPoint3* pkPositions = pkGeomData->GetVertices();
	NiPoint3* pkNormals = pkGeomData->GetNormals();
	NiPoint3* pkWorldNormals = (NiPoint3*)NiMalloc(sizeof(NiPoint3) * iNumVertTotal);
	NiMatrix3::TransformNormals(pkWorldTransform.m_Rotate,iNumVertTotal,pkNormals,pkWorldNormals);
		

	for (int iVertIdx=0; iVertIdx< iNumVertTotal; iVertIdx++)
	{
		NiColor kDiffuseAccum;
		NiColor kAmbientAccum;
		NiPoint3 kWorldPos = pkWorldTransform * pkPositions[iVertIdx];
		_BackPosLight(kWorldPos, pkWorldNormals[iVertIdx], pkLight, kDiffuseAccum, kAmbientAccum);

		NiColor kColor = kDiffuseAccum + kAmbientAccum;

		NiColorA kNewColor = NiColorA(kColor.r,kColor.g,kColor.b,0.0) + pkColorList[iVertIdx];

		//if (kNewColor != pkColorList[iVertIdx])
		//{
		//	// 提亮光照后的顶点 [5/6/2010 hemeng]
		//	/*CColorHSL kHSL(kNewColor);
		//	kHSL.m_fLum += 5;
		//	kNewColor = kHSL.GetNiColor();*/
		//	kNewColor.r += 0.2;
		//	kNewColor.g += 0.2;
		//	kNewColor.b += 0.2;
		//}
	
		kNewColor.Clamp();		

		pkColorList[iVertIdx] = kNewColor;

	}
	pkGeomData->MarkAsChanged( pkGeomData->COLOR_MASK );

	pkObj->Update( 0.0f );
	pkObj->UpdateProperties();
	pkObj->UpdateEffects();
	
}

void CEntityVertexColorBaker::_BackPosLight(NiPoint3 kPos, NiPoint3 kNormal, NiLight* pkLight, NiColor& kDiffuseAccum, NiColor& kAmbientAccum)
{

	// light计算 from GameBryo light shader [4/16/2010 hemeng]

	NiPoint3 kLightVector(0, 0, 0);	// 被光照顶点的光照方向
	float fAttenuation = 1.0f;	// 衰减
	float fDimmer = pkLight->GetDimmer();
	// 确定灯光类型
	if (NiIsKindOf(NiDirectionalLight, pkLight))
	{
		/*NiDirectionalLight* pDirLight = NiDynamicCast(NiDirectionalLight, pkLight);
		kLightVector = -(pDirLight->GetWorldDirection());
		kLightVector.Unitize();*/

		// 不处理方向光，场景物件都受动态方向光影响 [4/28/2010 hemeng]
		return;
	}
	else if(NiIsKindOf(NiPointLight, pkLight))
	{
		NiPointLight* pPointLight = NiDynamicCast(NiPointLight, pkLight);
		kLightVector = pPointLight->GetWorldLocation() - kPos;
		float fDistance = kLightVector.Unitize();	// 顶点到光源的距离

		// 计算衰减
		fAttenuation =	pPointLight->GetConstantAttenuation() + 
			pPointLight->GetLinearAttenuation()*fDistance +
			pPointLight->GetQuadraticAttenuation()*fDistance*fDistance;

		fAttenuation = max(1.0f, fAttenuation);
		fAttenuation = 1.0f / fAttenuation;
	}
	else if(NiIsKindOf(NiAmbientLight, pkLight))
	{
		NiAmbientLight* pAmbientLight = NiDynamicCast(NiAmbientLight, pkLight);
		NiColor kAmbColor = pAmbientLight->GetAmbientColor()*pAmbientLight->GetDimmer();
		kAmbientAccum += kAmbColor;
		return;
	}
	else
	{
		// 不支持聚光灯
		return;
	}

	// Take N dot L as intensity.
	float fLightNDotL = kLightVector.Dot(kNormal);
	float fLightIntensity = max(0, fLightNDotL);

	kDiffuseAccum += pkLight->GetDiffuseColor()*fAttenuation*fLightIntensity*fDimmer;
	kAmbientAccum += pkLight->GetAmbientColor()*fAttenuation*fDimmer;
}


#endif