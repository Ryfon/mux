﻿/** @file SprayGenerator.h 
@brief 高级水体 排浪生成器
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-10-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once


#include "Terrain.h"
#include <vector>

#ifndef CODE_INGAME

namespace SceneCore
{
// 排浪生成
// 根据面片和地形的交叉处生成排浪面片和UV
class MAIN_ENTRY CSprayGenerator
{
public:
	// Constructor
	CSprayGenerator();

	// Destructor
	~CSprayGenerator(void);

	// 初始化
	bool Init(CTerrain* pTerrain, float fSeaLevel, float fWidth, float fAlpha, 
				float fT0, float fV0, float fT1, float fV1, float fT2, float fV2, float fT3, float fV3, const char* strTexFilename);

	// 生成排浪几何体
	NiGeometryPtr GenerateSprayGeometry();
	NiGeometryPtr GenerateSprayGeometry(std::set<int>& gridSet);

	// 获取已生成的排浪几何体
	NiGeometryPtr GetSprayGeometry();
protected:

	// 提取海岸线
	void _ExtractCoastEdge();
	void _ExtractCoastEdge(std::set<int>& gridSet);

	// 连接海岸线线段
	void _ConnectCoastEdge();

	// 挤压海岸线生成排浪面片
	void _ExtrudeCoastEdge();

	// 生成排浪面片
	void _CreateMesh();

	// 为排浪面片设置纹理动画
	void _SetTextureAnimation();

	// 按照 z 对两点线性插值
	NiPoint3 _Lerp(const NiPoint3& kStart, const NiPoint3& kEnd, float fInterValue);

	// 判断两个 NiPoint3 的位置是否重合
	bool _SimpleEqule(const NiPoint3& p0, const NiPoint3& p1);

	// 检查从 kBase 向 kDirection 方向挤压是否合法.返回合法的方向.
	// 如果挤压一个很小的距离(比如0.1)后在地形之下,认为不合理,反之合理
	bool _CheckExtrudeDirection(const NiPoint3& kBase, const NiPoint3& kDirection);

private:
	CTerrain*					m_pTerrain;			// 目标地形
	float						m_fSeaLevel;		// 海平面高度
	float						m_fWidth;			// 排浪宽度
	float						m_fAlpha;			// 动画速度
	float						m_fT0, m_fT1, m_fT2, m_fT3;	// 关键帧时间
	float						m_fV0, m_fV1, m_fV2, m_fV3;	// V 偏移
	NiTexturePtr				m_spSprayTexture;	// 排浪纹理
	list<NiPoint3>				m_EdgeSegmentList;	// 海岸线线段列表
	vector<list<NiPoint3>*>		m_CoastEdgeList;	// 连续海岸线列表
	vector<list<NiPoint3>*>		m_ExtrudeEdgeList;	// 连续海岸线挤压顶点列表

	NiGeometryPtr				m_spSprayMesh;		// 排浪几何体

		
};	// End of Class

#include "SprayGenerator.inl"

};	// End of namespace

#endif