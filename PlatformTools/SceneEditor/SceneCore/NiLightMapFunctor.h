﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2008 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Calabasas, CA 91302
// http://www.emergent.net

#ifndef NILIGHTMAPFUNCTOR_H
#define NILIGHTMAPFUNCTOR_H

#ifndef CODE_INGAME

//#include <NiMesh.h>
#include <NiGeometry.h>
#include <NiEntity.h>

struct NiLightMapLightProperties
{
    NiLightMapLightProperties(
        bool bGenerateRuntimeLight = false,
        float fAreaLightRadius = 0.0f,
        int iAreaLightSamples = 0,
        NiLightComponent* pkLightComponent = NULL,
        const char* pcEntityID = NULL)
    {
        m_bGenerateRuntimeLight = bGenerateRuntimeLight;
        m_fAreaLightRadius = fAreaLightRadius;
        m_iAreaLightSamples = iAreaLightSamples;
        m_pkLightComponent = pkLightComponent;
        m_pcEntityID = pcEntityID;
    }

    bool m_bGenerateRuntimeLight;
    float m_fAreaLightRadius;
    int m_iAreaLightSamples;
    NiLightComponent* m_pkLightComponent;
    const char* m_pcEntityID;
};

struct NiLightMapMeshProperties
{
    NiLightMapMeshProperties(
        const char* pcEntityDirectory = "",
        const char* pcLightMapFilename = "",
        NiEntityInterface* pkEntity = NULL)
    {
        m_pcEntityDirectory = pcEntityDirectory;
        m_pcLightMapFilename = pcLightMapFilename;
        m_pkEntity = pkEntity;
    }

    const char* m_pcEntityDirectory;
    const char* m_pcLightMapFilename;
    NiEntityInterface* m_pkEntity;
};

class NiVisitLightMapMeshFunctor
{
public:
    virtual bool operator() (
        NiGeometry* pkGeom, 
        NiLightMapMeshProperties kProps = NiLightMapMeshProperties()) = 0;
    virtual ~NiVisitLightMapMeshFunctor(){}
};

class NiVisitLightMapLightFunctor
{
public:
    virtual bool operator() (
        NiLight* pkLight, 
        NiLightMapLightProperties kProps = NiLightMapLightProperties()) = 0;
    virtual ~NiVisitLightMapLightFunctor(){}
};

#endif // CODE_INGAME
#endif // NILIGHTMAPFUNCTOR_H