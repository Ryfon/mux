inline void CWaterBody::SetColor(const NiColor& kColor)
{
	m_kColor = kColor;
	// 通知 material
	if (NULL != m_pkWaterSurface)
	{
		NiFloatsExtraData* pExtraData = (NiFloatsExtraData*)m_pkWaterSurface->GetExtraData("WaterColor");
		pExtraData->SetArray(3, (float *)&(m_kColor.r));
	}

}
//---------------------------------------------------------------------------
inline NiColor CWaterBody::GetColor() const
{
	return m_kColor;
}
//---------------------------------------------------------------------------
inline void CWaterBody::SetAlpha(float fAlpha)
{
	m_fAlpha = fAlpha;
	// 通知 material
	if (NULL != m_pkWaterSurface)
	{
		NiFloatExtraData* pExtraData = (NiFloatExtraData*)m_pkWaterSurface->GetExtraData("WaterAlpha");
		pExtraData->SetValue(fAlpha);
	}
}
//---------------------------------------------------------------------------
inline float CWaterBody::GetAlpha() const
{
	return m_fAlpha;
}
//---------------------------------------------------------------------------
// 设置/获取 bump map 重复次数
inline void CWaterBody::SetBumpMapRepeat(const NiPoint2 kRepeat)
{
	m_kBumpMapRepeat = kRepeat;
	// 通知 material
	if (NULL != m_pkWaterSurface)
	{
		NiFloatsExtraData* pExtraData = (NiFloatsExtraData*)m_pkWaterSurface->GetExtraData("BumpMapRepeat");
		pExtraData->SetArray(2, (float *)&(m_kBumpMapRepeat.x));
	}
}
//---------------------------------------------------------------------------
inline NiPoint2  CWaterBody::GetBumpMapRepeat() const
{
	return m_kBumpMapRepeat;
}
//---------------------------------------------------------------------------
// 设置/获取 bump map UV 动画速度
inline void  CWaterBody::SetBumpMapSpeed(const NiPoint2 kSpeed)
{
	m_kUVSpeed = kSpeed;
}
//---------------------------------------------------------------------------
inline NiPoint2  CWaterBody::GetBumpMapSpeed() const
{
	return m_kUVSpeed;
}
//---------------------------------------------------------------------------
// 设置反射纹理的背景天空纹理
inline void CWaterBody::SetReflectTextureBG(NiTexturePtr pkTexture)
{
	//NIASSERT(pkTexture);
	m_spTextureSky = pkTexture;
	if (NULL != m_pkWaterSurface)
	{
		m_pkRTViewTexProp->SetBaseTexture(m_spTextureSky);

		//m_spReflectBGRenderClick->RemoveAllRenderViews();
		//NiTexturingProperty* pTexProp = (NiTexturingProperty*)(m_pkWaterSurface->GetProperty(NiProperty::TEXTURING));
		//NiTexturingProperty::ShaderMap* pShaderMap = NiNew NiTexturingProperty::ShaderMap(
		//	m_spTexture0, 0 ); 
		//pTexProp->SetShaderMap(0, pShaderMap);	// reflect texture

		m_spRTView->GetScreenFillingQuad().Update(0.0f);
		m_spRTView->GetScreenFillingQuad().UpdateProperties();
		m_spRTView->GetScreenFillingQuad().UpdateEffects();
	}

}
//---------------------------------------------------------------------------
// 设置Bump map
inline void CWaterBody::SetBumpMap(NiTexturePtr pkTexture)
{
	//NIASSERT(pkTexture);
	m_spBumpMap = pkTexture;
	if (NULL != m_pkWaterSurface)
	{
		NiTexturingProperty* pTexProp = (NiTexturingProperty*)(m_pkWaterSurface->GetProperty(NiProperty::TEXTURING));
		NiTexturingProperty::ShaderMap* pShaderMap = NiNew NiTexturingProperty::ShaderMap(
			m_spBumpMap, 1 ); 
		pTexProp->SetShaderMap(1, pShaderMap);
		m_pkWaterSurface->Update(0.0f);
		m_pkWaterSurface->UpdateProperties();
		m_pkWaterSurface->UpdateEffects();
	}
}
//---------------------------------------------------------------------------
inline void CWaterBody::SetReflectTextureSize(const unsigned short usSize)
{
	if (usSize==128 || usSize==256 || usSize==512 || usSize==1024 && usSize != m_uiReflectTexSize)
	{
		NiRenderer* pkRenderer = NiRenderer::GetRenderer();
		m_uiReflectTexSize = usSize;
		m_spTexture0 = NiRenderedTexture::Create(m_uiReflectTexSize, m_uiReflectTexSize, pkRenderer);
		if ( !m_spTexture0)
		{
			return;
		}

		m_spRTGroup0 = NiRenderTargetGroup::Create(	m_spTexture0->GetBuffer(), pkRenderer, true, true );
		m_spReflectBGRenderClick->SetRenderTargetGroup(m_spRTGroup0);
		m_spReflectTexRenderClick->SetRenderTargetGroup(m_spRTGroup0);

		NiTexturingProperty* pTexProp = (NiTexturingProperty*)m_pkWaterSurface->GetProperty(NiProperty::TEXTURING);
		NiTexturingProperty::ShaderMap* pShaderMap = NiNew NiTexturingProperty::ShaderMap(
			m_spTexture0, 0 ); 
		pTexProp->SetShaderMap(0, pShaderMap);	// reflect texture
		m_pkWaterSurface->Update(0.0f);
		m_pkWaterSurface->UpdateProperties();
		m_pkWaterSurface->UpdateEffects();
		//m_pkWaterSurface->Update(0.0f);
		//m_pkWaterSurface->UpdateProperties();
		//m_pkWaterSurface->UpdateEffects();
	}
}
//---------------------------------------------------------------------------
inline unsigned short CWaterBody::GetReflectTextureSize() const
{
	return m_uiReflectTexSize;
}
//---------------------------------------------------------------------------