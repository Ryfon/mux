﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include "EditableBaseObj.h"

CEditableBaseObj::CEditableBaseObj()
: m_spNode(0), m_pExtraData(0)
{
	m_ObjectType = OT_COMMON;
}

CEditableBaseObj::~CEditableBaseObj(void)
{

}

bool	CEditableBaseObj::Create( const char *pszNifFile )
{	
	m_pExtraData = NiNew NiStringExtraData( NiFixedString(pszNifFile ) );
	m_pExtraData->SetValue( pszNifFile );

	return true;
}

void	CEditableBaseObj::Destroy()
{}

CEditableBaseObj*	CEditableBaseObj::Clone()
{
	return 0;
}

CEditableBaseObj*	CEditableBaseObj::DeepClone()
{
	return 0;
}

void	CEditableBaseObj::SetTranslate( float fX, float fY, float fZ )
{
	m_vPosition =  NiPoint3( fX, fY, fZ ) ;
}

void	CEditableBaseObj::SetTranslate( NiPoint3& vPos)
{
	m_vPosition =  vPos ;
}

void	CEditableBaseObj::SetRotation( float fX, float fY, float fZ )
{
	//m_vRotation = NiPoint3( fX, fY, fZ );
}

void	CEditableBaseObj::SetRotation( NiMatrix3& vRot )
{
	m_vRotation = vRot;
}

void	CEditableBaseObj::SetScale(  float fX, float fY, float fZ )
{
	m_vScale = NiPoint3( fX, fY, fZ );
}

void	CEditableBaseObj::SetScale( NiPoint3& vScale )
{
	m_vScale = vScale;
}

//NPC生成器物件
CNpcCreatorObj::CNpcCreatorObj()
{
	m_ObjectType = OT_NPCCREATOR;
}

CNpcCreatorObj::~CNpcCreatorObj()
{

}

#endif