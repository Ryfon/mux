﻿/** @file RiverExportor.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2008-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
#ifndef CODE_INGAME

struct Lib3dsFile;
struct Lib3dsNode;

namespace SceneCore
{
	class MAIN_ENTRY CRiverExportor
	{
	public:
		CRiverExportor(void);
		~CRiverExportor(void);

		/// 添加一个结点到导出列表 
		void AddRiverNode(NiTriShapePtr pTriGeom);

		/// 将场景保存到 3ds 文件
		bool SaveToFile(const char* pszFilePath);

		/// 清除 Lib3dsFile 里所有结点
		void Clear();

	private:
	CRiverExportor(CRiverExportor&);
	Lib3dsFile* m_p3dsFile;

	};
	//end of class CRiverExportor

}
//end of namespace

#endif
