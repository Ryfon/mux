﻿/** @file NPC.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangshaodong
*	完成日期：2007-12-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef CODE_INGAME
#ifndef _NPC_H
#define _NPC_H



//NPC类，封装NCP信息
class CNPC
{
public:
	CNPC(void);
public:
	virtual ~CNPC(void);
	int LoadData(const TiXmlElement* pXmlNPC);
	int SaveData(TiXmlElement* pXmlNPC);

	int LoadDataM1(const TiXmlElement* pXmlNPC);	//供M1使用，仅读取ID，名字，外观模型号

	unsigned long GetSkillNo(size_t nIndex) const
	{
		if (nIndex >= GetSkillCount() || nIndex < 0)
		{
			return -1;
		}

		return m_SkillInfo[nIndex];
	}
	size_t GetSkillCount() const
	{
		return m_SkillInfo.size();
	}

	int AddSkillNo(unsigned long ulSkillNo);
	int DelSkillNo(unsigned long ulSkillNo);

public:
/*NPC属性开始定义*/
	//基本属性
	//NPC编号
	int m_NPCNO;
	//NPC名字
	string	m_Name;
	//中文描述
	string m_Desc;
	//NPC等级
	short m_Level;
	//NPC类型
	short m_Type;
	//NPC种族
	int m_RaceType;
	//NPC大小 0：巨 1：大 2：中 3：小
	short m_Size;
	//NPC经验值
	int m_Exp;
	//经验值取得的条件
	short m_ExpLimitLevel;
	//生命值上限
	short m_MaxHp;
	//生命值恢复速度
	int m_HpUp;
	//移动速度等级 0：不动 1：缓步 2：步行 3：加速 4：跑步 5：极速
	int m_SpeedLevel;
	//视野范围
	float m_ViewSpan;
	//巡逻范围
	float m_PatrolSpan;
	//追击范围
	float m_ChaseSpan;
	//追击时间
	int m_ChaseTime;

	//攻击属性
	//攻击类型 0:魔法 1:普通
	short m_AttType;
	//最小攻击
	int m_AttMin;
	//最大攻击
	int m_AttMax;
	//攻击动作
	string m_ActionName;
	//伤害类型 0：点 1：长方形 2：圆形
	short m_HeartType;
	//最小攻击距离
	int m_AttDistMin;
	//最大攻击距离
	int m_AttDistMax;
	//攻击半径
	float m_AttRadius;
	//攻击频率
	float m_AttFreq;
	//扣血间隔
	int m_SpaceTime;
	//攻击浮动率
	float m_AttProb;
	//必杀值
	int m_FinalKill;
	//命中
	int m_Hit;
	//最小防御
	int m_DefMin;
	//最大防御
	int m_DefMax;
	//攻击速度
	int m_AttSpeed;
	//移动速度
	int m_MoveSpeed;
	//爆击率
	float m_Critical;


	//AI属性
	//攻击类型 0：忍受 1：被动 2：主动
	short m_AttProp;
	//攻击目标选择 0：随机 1：伤害 2：等级 3：任务标志 4：血量
	short m_AttObjSel;
	//目标选择数据
	string m_AttObjSelData;
	//追击次数
	int m_ChaseTimes;
	//触发条件
	int m_AIFireCond;
	//触发数据
	int m_AIFireData;
	//逃跑几率
	float m_EscapeRatio;
	//逃跑距离
	float m_EscapeDist;
	//逃跑时间
	int m_EscapeTime;
	//范围
	float m_Dist;
	//接受请求
	int m_AcceptAID;
	//加血几率
	float m_AddBloodRatio;
	//请求加血率
	float m_AidBloodRatio;
	//请求支援率
	float m_AidBoostRatio;
	//请求解除状态率
	float m_UnchainStateRatio;
	//技能释放率
	float m_SkillRatio;
	//召唤几率
	float m_SummonRatio;
	//召唤数量
	int m_SummonNum;
	//召唤的NPC
	int m_SummonNPCNO;
	//传送几率
	float m_CarryRatio;
	//变形几率
	float m_TransRatio;
	//返回重生点 0:传送 1：跑回
	int m_ReturnBorn;
	//永久状态
	string m_ForeverState;
	//加血技能
	string m_AddBloodId;
	//能力暴涨
	int m_PowerRise;
	//自身增益技能
	string m_PlusID;
	//对方伤害技能
	string m_DamnifyID;

	//外观属性
	//NPC模型编号;
	int m_ModalNO;
	//NPC头像;
	int m_HeadImg;
	//图形号
	short m_Logo;
	//贴图
	short m_Text;
	//骨骼
	short m_Cadre;
	//动画
	short m_Movie;
	//长度，从头到尾的尺寸为长
	short m_Length;
	//宽度，肩膀的宽度为宽
	short m_Width;
	//是否按实际尺寸判断怪物所占的格子
	bool m_TrimSize;

	//动作编号,用于获取模型号
	string m_ActionNO;
	/*NPC属性定义结束*/


	//********************************************M1阶段NPC属性字段改变，导出方式改变，需要用到的字段重新定义************************************
	string m_NPCAppearance;
private:
	vector<unsigned long> m_SkillInfo;
};

#endif
#endif