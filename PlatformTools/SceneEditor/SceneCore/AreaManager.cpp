﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include "AreaManager.h"
#include "Terrain.h"
#include "SceneDesignerConfig.h"

CAreaManager::CAreaManager(void)
:m_uiAreaPointNum(0),m_uiAreaNum(0)
{
}

CAreaManager::CAreaManager(CTerrain* pTerrain)
:m_pTerrain(pTerrain),m_uiAreaPointNum(0),m_uiAreaNum(0)
{

}

CAreaManager::~CAreaManager(void)
{
}

bool CAreaManager::LoadData(string strFileName)
{
	TiXmlDocument XmlDoc(strFileName.c_str());
	if (!XmlDoc.LoadFile())
	{
		return false;
	}
	TiXmlElement* pRootElement = XmlDoc.RootElement();

	for (const TiXmlElement* pXmlRegionProp = pRootElement->FirstChildElement();pXmlRegionProp;pXmlRegionProp = pXmlRegionProp->NextSiblingElement())
	{
		for (const TiXmlElement* pXmlRegion = pXmlRegionProp->FirstChildElement();pXmlRegion;pXmlRegion = pXmlRegion->NextSiblingElement())
		{
			CArea* pNewArea = new CArea();
			unsigned int uiTempAreaPointID = pNewArea->LoadData(pXmlRegion);
			m_AreaList.push_back(pNewArea);

			unsigned int uiTempAreaID = atol(pXmlRegion->Attribute("RegionID"));
			if (uiTempAreaID > m_uiAreaNum)
			{
				m_uiAreaNum = uiTempAreaID;
			}
			if (uiTempAreaPointID > m_uiAreaPointNum)
			{
				m_uiAreaPointNum = uiTempAreaPointID;
			}
		}
	}

	return true;
}

bool CAreaManager::SaveData(string strFileName)
{
	//插入Xml声明
	TiXmlDocument* pXmlDoc = new TiXmlDocument(strFileName.c_str());
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	pXmlDoc->InsertEndChild(*pXmlDec);
	//插入根节点
	TiXmlElement* pXmlRootElement = new TiXmlElement("Regions");
	pXmlRootElement->SetAttribute("FileVersion", "0.9");
	pXmlDoc->LinkEndChild(pXmlRootElement);

	int nRegionPropIndex = 0;
	stRegionProperty* pTempRegionProp = CSceneDesignerConfig::GetInstance()->GetRegionPropByIndex(nRegionPropIndex);
	while(pTempRegionProp != NULL)
	{
		TiXmlElement* pXmlRegionPropElement = new TiXmlElement((pTempRegionProp->strName).c_str());
		pXmlRegionPropElement->SetAttribute("Type", pTempRegionProp->nType);
		pXmlRegionPropElement->SetAttribute("Name", (pTempRegionProp->strViewName).c_str());
		pXmlRegionPropElement->SetAttribute("Shape", pTempRegionProp->nShape);
		
		for (vector<CArea*>::iterator it = m_AreaList.begin();it != m_AreaList.end();it++)
		{
			unsigned short usAreaType = (*it)->m_usAreaType;

			if (usAreaType == pTempRegionProp->nType)
			{
				TiXmlElement* pXmlRegionElement = new TiXmlElement("Region");
				(*it)->SaveData(pXmlRegionElement, pTempRegionProp->nShape);
				pXmlRegionPropElement->LinkEndChild(pXmlRegionElement);
			}
		}

		pXmlRootElement->LinkEndChild(pXmlRegionPropElement);
		nRegionPropIndex++;
		pTempRegionProp = CSceneDesignerConfig::GetInstance()->GetRegionPropByIndex(nRegionPropIndex);
	}

	pXmlDoc->SaveFile();
	delete pXmlDoc;
	return true;
}

bool CAreaManager::EntityIsAreaPoint(NiEntityInterface* pkEntityInterface)
{
	unsigned int uiAreaPointID;

	NiBool bPropExist = pkEntityInterface->GetPropertyData(REGIONPOINTID, uiAreaPointID);

	if (bPropExist)
	{
		return true;
	}

	return false;
}

vector<CArea*>::iterator CAreaManager::LocateArea(unsigned int uiRegionPointID)
{
	vector<CArea*>::iterator it = m_AreaList.begin();

	for (;it != m_AreaList.end();it++)
	{
		int nIndex = (*it)->IsExist(uiRegionPointID);
		if (nIndex != -1)
		{
			return it;
		}
	}

	return m_AreaList.end();
}

vector<CArea*>::iterator CAreaManager::GetAreaByID(unsigned int uiAreaID)
{
	vector<CArea*>::iterator it = m_AreaList.begin();

	for (;it != m_AreaList.end();it++)
	{
		unsigned int uiTempID = (*it)->m_uiAreaID;
		if (uiTempID == uiAreaID)
		{
			return it;
		}
	}

	return m_AreaList.end();
}

bool CAreaManager::UpdateAreaPoint(NiEntityInterface *pkEntityInterface, NiFixedString &pkPropertyName)
{
	unsigned int nRegionPointID;
	pkEntityInterface->GetPropertyData(REGIONPOINTID, nRegionPointID);

	vector<CArea*>::iterator it = LocateArea(nRegionPointID);

	if (it == m_AreaList.end())
	{
		return false;
	}

	if (pkPropertyName == TRANSLATION)
	{
		NiPoint3 vPos;
		pkEntityInterface->GetPropertyData(pkPropertyName, vPos);
		int iPosX;
		int iPosY;
		this->m_pTerrain->GetGridPosInServer(vPos, iPosX, iPosY);
		(*it)->UpdatePoint(nRegionPointID, iPosX, iPosY);
	}

	return true;
}

bool CAreaManager::AddAreaPoint(unsigned int uiAreaID,NiEntityInterface* pkEntityInterface)
{
	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return false;
	}

	//获取ID
	m_uiAreaPointNum++;
	unsigned int uiAreaPointID = m_uiAreaPointNum;
	//获取位置信息
	NiPoint3 vPos;
	pkEntityInterface->GetPropertyData(TRANSLATION, vPos);
	int iPosX;
	int iPosY;
	this->m_pTerrain->GetGridPosInServer(vPos, iPosX, iPosY);
	//添加新的点
	(*it)->InsertPoint(uiAreaPointID, iPosX, iPosY);
	//更新数据
	pkEntityInterface->SetPropertyData(REGIONPOINTID, uiAreaPointID);
	pkEntityInterface->SetPropertyData(REGIONID, uiAreaID);
	return true;
}

bool CAreaManager::InitAreaPoint(NiEntityInterface* pkEntityInterface)
{
	unsigned int uiAreaPointID = 0;
	pkEntityInterface->GetPropertyData(REGIONPOINTID, uiAreaPointID);
	vector<CArea*>::iterator it = LocateArea(uiAreaPointID);
	if (it != m_AreaList.end())
	{
		pkEntityInterface->SetPropertyData(REGIONID, (*it)->m_uiAreaID);
		NiFixedString niStr(TRANSLATION);
		UpdateAreaPoint(pkEntityInterface, niStr);
	}
	else
	{
		unsigned int uiAreaID = 0;
		pkEntityInterface->GetPropertyData(REGIONID, uiAreaID);
		this->AddAreaPoint(uiAreaID,pkEntityInterface);
	}
	return true;
}

bool CAreaManager::DelAreaPoint(NiEntityInterface *pkEntityInterface)
{
	unsigned int nRegionPointID;
	pkEntityInterface->GetPropertyData(REGIONPOINTID, nRegionPointID);

	vector<CArea*>::iterator it = LocateArea(nRegionPointID);

	if (it == m_AreaList.end())
	{
		return false;
	}

	(*it)->DeletePoint(nRegionPointID);

	//删除区域
	unsigned int uiAreaID = (*it)->m_uiAreaID;
	DelArea(uiAreaID);

	return true;
}

unsigned short CAreaManager::GetAreaTypeByID(unsigned int uiAreaID)
{
	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return -1;
	}

	return (*it)->m_usAreaType;
}

bool CAreaManager::SetAreaTypeByID(unsigned int uiAreaID, unsigned short usAreaType)
{
	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return false;
	}

	(*it)->m_usAreaType = usAreaType;

	return true;
}

string CAreaManager::GetAreaNameByID(unsigned int uiAreaID)
{
	string strResult;

	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return strResult;
	}

	return (*it)->m_strAreaName;
}

bool CAreaManager::SetAreaNameByID(unsigned int uiAreaID, string strAreaName)
{
	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return false;
	}

	(*it)->m_strAreaName = strAreaName;

	return true;
}

string CAreaManager::GetAreaMusicInfoByID(unsigned int uiAreaID, string strPropName)
{
	string strResult;

	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return strResult;
	}

	if (strcmp(strPropName.c_str(), "MusicFile") == 0)
	{
		return (*it)->m_strMusicFile;
	}
	else if (strcmp(strPropName.c_str(), "Volume") == 0)
	{
		stringstream strTemp;
		strTemp << (*it)->m_usVolume; 
		strTemp >> strResult;
		return strResult;
	} 
	else if (strcmp(strPropName.c_str(), "MusicLoop") == 0)
	{
		stringstream strTemp;
		strTemp << (*it)->m_usMusicLoop;
		strTemp >> strResult;
		return strResult;
	}

	return strResult;
}

bool CAreaManager::SetAreaMusicInfoByID(unsigned int uiAreaID, string strPropName, string strPropValue)
{
	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return false;
	}

	if (strcmp(strPropName.c_str(), "MusicFile") == 0)
	{
		(*it)->m_strMusicFile = strPropValue;
	}
	else if (strcmp(strPropName.c_str(), "Volume") == 0)
	{
		(*it)->m_usVolume = atoi(strPropValue.c_str());
	} 
	else if (strcmp(strPropName.c_str(), "MusicLoop") == 0)
	{
		(*it)->m_usMusicLoop = atoi(strPropValue.c_str());
	}

	return true;
}

unsigned int CAreaManager::GetTopAreaID()
{
	return m_uiAreaNum;
}

unsigned int CAreaManager::CreateArea(unsigned short usAreaType)
{
	m_uiAreaNum++;

	CArea* pNewArea = new CArea();

	pNewArea->m_uiAreaID = m_uiAreaNum;
	pNewArea->m_usAreaType = 0;//区域的类型，统一为0，表示没有分配的区域
	pNewArea->m_usAreaShape = usAreaType;//原先的参数，作为区域的形状，0代表矩形区域，1代表多边形区域
	string strAreaName("未分配的的区域");
	pNewArea->m_strAreaName = strAreaName;//名称统一为，未分配的区域
	//////////////////////////////////////////////////////////////////////////
	string strMusicFileName("");
	pNewArea->m_strMusicFile = strMusicFileName;
	pNewArea->m_usVolume = 50;//50代表音量为50%
	pNewArea->m_usMusicLoop = 1;//默认为循环播放
	//////////////////////////////////////////////////////////////////////////

	m_AreaList.push_back(pNewArea);

	return m_uiAreaNum;
}

bool CAreaManager::DelArea(unsigned int uiAreaID)
{
	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return false;
	}

	int nAreaNum = (*it)->GetAreaPointNum();
	if (nAreaNum != 0)
	{
		return false;
	}

	delete (*it);
	m_AreaList.erase(it);

	return true;
}

bool CAreaManager::UpdateArea(unsigned int uiAreaID, unsigned short usAreaType, std::string strAreaName)
{
	vector<CArea*>::iterator it = GetAreaByID(uiAreaID);

	if (it == m_AreaList.end())
	{
		return false;
	}

	(*it)->m_usAreaType = usAreaType;
	(*it)->m_strAreaName = strAreaName;

	return true;
}
#endif