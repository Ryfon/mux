﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include "TerrainChangeVertexColorCommand.h"
#include "Terrain.h"
#include "TerrainModifier.h"

CTerrainChangeVertexColorCommand::CTerrainChangeVertexColorCommand(CTerrain *pTerrain)
: m_pTerrain(pTerrain)
{
}

CTerrainChangeVertexColorCommand::~CTerrainChangeVertexColorCommand(void)
{
}



// 执行该命令
void CTerrainChangeVertexColorCommand::DoCommand(void)
{
	if (this == NULL)
	{
		return;
	}

	// 改变地形的顶点色

	NiColorA* pVertColor = m_pTerrain->GetVertexColor();

	std::map<int, tVertexColorChange>::iterator it = m_VerticesChange.begin();
	for (; it!=m_VerticesChange.end(); it++)
	{
		pVertColor[it->second.iIndex] = it->second.kNewColor;
	}
	_NotifyChunk();
}

// 撤销该命令
void CTerrainChangeVertexColorCommand::UndoCommand(void)
{
	NiColorA* pVertColor = m_pTerrain->GetVertexColor();

	std::map<int, tVertexColorChange>::iterator it = m_VerticesChange.begin();
	for (; it!=m_VerticesChange.end(); it++)
	{
		pVertColor[it->second.iIndex] = it->second.kOldColor;
	}
	_NotifyChunk();
}

void CTerrainChangeVertexColorCommand::_NotifyChunk()
{
	// 依次改变顶点的法线, 必须在所有顶点位置改变后完成, 否则可能导致计算无效
	//std::map<int, tVertexColorChange>::iterator it = m_VerticesChange.begin();
	//for (; it!=m_VerticesChange.end(); it++)
	//{
	//	NiPoint3& n  = m_pTerrain->m_pNormals[it->second.iIndex];
	//	n = CTerrainModifier::CalculateNormal( m_pTerrain, it->second.iIndex );
	//}

	// 通知Chunks顶点改变
	int iNumChunks = m_pTerrain->GetChunkNumX()*m_pTerrain->GetChunkNumY();

	for (int i=0; i<iNumChunks; i++)
	{
		CTerrainModifier::NotifyChunkVertexChanged( m_pTerrain, i);
	}
}

// 增加一个改变的顶点 
void CTerrainChangeVertexColorCommand::AddVertexChange(const tVertexColorChange& vertChange)
{
	if (vertChange.kNewColor == vertChange.kOldColor)
	{
		return;
	}

	std::map<int, tVertexColorChange>::iterator it = m_VerticesChange.find(vertChange.iIndex);
	if (m_VerticesChange.end() != it)
	{
		//it->second.fNewHeight = vertChange.fNewHeight;
		return;
	}
	else
	{
		m_VerticesChange[vertChange.iIndex] = vertChange;
	}
}

void CTerrainChangeVertexColorCommand::AddChangedChunk(int iChunk)
{
	m_chunkset.insert(iChunk);
}

#endif