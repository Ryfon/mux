﻿/** @file WaterBody.h 
@brief 水体
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-03-03
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef WATER_BODY_H 
#define WATER_BODY_H

#pragma once

#include <NiMemObject.h>
#include <NiEntity.h>
#include "WaterRenderView.h"
//#include "NiSpeedTreeComponent.h"

namespace SceneCore
{


/// 用来初始化水体的参数
struct stWaterInitParam
{
	//NiRenderer*			pkRenderer;			// 场景唯一的 renderer
	CWaterRenderViewPtr	pWaterRenderView;	// 水体 render view.包含摄像机属性和反射纹理需要渲染得物件的最小集
	NiPoint3			kPosition;			// 水体左下定点坐标
	float				fSize;				// 尺寸
	float				fAlpha;				// 透明度
	NiColor				kColor;				// 水面颜色
	NiTexturePtr		pkBGSkyTexture;		// 用来做反射纹理背景的天空纹理
	unsigned int		uiReflectTexSize;	// 反射纹理尺寸
	NiTexturePtr		pkBumpTexture;		// bump 纹理
	NiPoint2			kBumpMapRepeat;		// u/v 方向上bump map 重复数量
	NiPoint2			kUVSpeed;			// u/v 方向上UV 动画运动速度
};

NiSmartPointer(CWaterBody);

/// 水体
class MAIN_ENTRY CWaterBody : public NiAVObject
{
	NiDeclareRTTI;
public:
	CWaterBody(void);
	virtual ~CWaterBody(void);

	/// 
	/**
	*	<BR>功能说明：	   创建水体
	*	<BR>可访问性：
	*	<BR>注    释：	   
	*	@param	stWaterInitParam[in]	水体初始化参数集
	*	@return							创建是否成功
	*/
	bool		Init(stWaterInitParam& waterInitParam);

	/// 获取材质
	NiMaterialPtr		GetMaterial();

	/// 设置水体透明度
	void SetAlpha(float fAlpha);
	float GetAlpha() const;

	/// 设置/获取 水体颜色
	void SetColor(const NiColor& kColor);
	NiColor GetColor() const;

	/// 设置/获取 反射纹理尺寸
	void SetReflectTextureSize(const unsigned short usSize);
	unsigned short GetReflectTextureSize() const;

	/// 设置/获取主活动相机
	NiCamera			*GetMainCamera();
	void				SetMainCamera(NiCamera* pCamera); 

	/// 设置 mainRenderView
	void SetMainRenderView(CWaterRenderViewPtr pMainRenderView);

	/// 设置位置
	void SetPosition(const NiPoint3& kPosition) {m_kPosition = kPosition;}
	const NiPoint3& GetPosition() {return m_kPosition;}

	/// 设置反射纹理的背景天空纹理
	void SetReflectTextureBG(NiTexturePtr pkTexture);

	/// 设置Bump map
	void SetBumpMap(NiTexturePtr pkTexture);

	/// 设置/获取 bump map 重复次数
	void SetBumpMapRepeat(const NiPoint2 kRepeat);
	NiPoint2 GetBumpMapRepeat() const;

	/// 设置/获取 bump map UV 动画速度
	void SetBumpMapSpeed(const NiPoint2 kSpeed);
	NiPoint2 GetBumpMapSpeed() const;

	/// 保存/获取摄像机参数
	void SaveCamParam(const NiPoint3& kPos, const NiPoint3& kDir);
	void RetrieveCamParam(NiPoint3& kPos, NiPoint3& kDir);

	/// 设置 clip plan
	void SetWaterClipPlane(bool bSet);

	/// 添加/删除/获取 可能被渲染到反射纹理中的潜在静态物件
	void AppendPVStaticObject(NiAVObject* pkPVObject);
	void RemovePVStaticObject(NiAVObject* pkPVObject);
	list<NiAVObject*>& GetPVStaticObjects();

	/// 创建水体表面可视集 (针对水体表面的剔除,不针对 CWaterRenderView 中的渲染集)
	virtual void BuildVisibleSet( NiCamera * pCamera, NiCullingProcess* pCullingProcess );

	/// 渲染反射纹理 由于各水面高低可能有差别，所以反射纹理只能单独渲染
	void RenderReflectTexture(NiEntityRenderingContext* pkRenderingContext);

	/// 将speed tree 物件渲染到当前 render target
	//void RenderSpeedTree();

	/// 终结
	void	ShutDown();

	/// 更新
	void	Update(float fTotalTime );

	/// 打开该效果
	void	Enable();
	/// 关闭该效果
	void	Disable();

	/// 获取场景根
	NiNode*	GetSceneRoot() { return m_spRootScene; }

	/// 获取RenderStep
	NiDefaultClickRenderStepPtr	GetRenderStep(){ return m_spRenderStep; }

protected:
	// 创建水体表面几何体
	void _CreateWaterBodyGeom();

	// 销毁
	void _Destroy();

	/// Water Texture 回调
	static bool			_PreWaterTextureCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData ); 
	static bool			_PostWaterTextureCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData ); 

	/// 创建渲染到全屏的 render view
	void _CreateScreenFillingRenderViews();



	/// 创建 bump map 函数作废
	// bool _CreateBumpMap(NiPoint2 kNumWaves);

	/// 场景根节点
	NiNodePtr					m_spRootScene;
	/// 主视 包含摄像机和 culling processor
	CWaterRenderViewPtr			m_spWaterRenderView;
	/// 渲染器
	//NiRenderer*				m_spRenderer;
	/// 水体周围,可能被渲染到反射纹理中的静态物件列表.
	list<NiAVObject*>			m_PVStaticObjects;
	/// 初始化结束标志
	bool						m_bInitialized;

	/// 默认RenderStep
	NiDefaultClickRenderStepPtr	m_spRenderStep;
	/// 渲染到纹理
	NiRenderedTexturePtr		m_spTexture0;
	/// 渲染目标0
	NiRenderTargetGroupPtr		m_spRTGroup0;
	/// 作为倒影纹理的背景天空纹理
	NiTexturePtr				m_spTextureSky;
	/// Bump map
	NiTexturePtr			m_spBumpMap;
	/// Render-to-texture全屏View
	NiScreenFillingRenderViewPtr	m_spRTView;
	/// Texture Property
	NiTexturingPropertyPtr		m_pkRTViewTexProp;	
	/// 渲染背景天空纹理到反射纹理的 render click
	NiViewRenderClickPtr		m_spReflectBGRenderClick;
	/// 渲染水面倒影纹理的 render click
	NiViewRenderClickPtr		m_spReflectTexRenderClick;
	/// 水面几何体
	NiTriShapePtr				m_pkWaterSurface;	
	/// 材质
	NiMaterialPtr				m_spWaterBodyMtl;	

	// 水体本身属性
	NiPoint3			m_kPosition;	// 位置
	float				m_fSize;		// 尺寸
	float				m_fAlpha;		// 透明度
	NiColor				m_kColor;		// 颜色
	NiPoint2			m_kBumpMapRepeat;// bump map 重复次数
	NiPoint2			m_kUVSpeed;		// u/v 方向波纹运动速度
	NiPoint3			m_kSaveCameraPos;	// 保存的摄像机位置，用于水下拍照后渲染水面时恢复该位置
	NiPoint3			m_kSaveCameraDir;	// 保存摄像机方向
	NiQuaternion		m_kBumpMat;			// bump matrix
	unsigned int		m_uiReflectTexSize; // 反射纹理尺寸
	bool				m_bEnabled;		// 水体效果是否开启
	bool				m_bInVisableArray;	// 水体是否在可视集中
};

#include "WaterBody.inl"

}; // end of namespace 

#endif