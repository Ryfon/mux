﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include <NiTNodeTraversal.h>
#include <NiTriStripsData.h>
#include <NiTriShapeData.h>
#include "AODiskGenerator.h"
#include "Terrain.h"

using namespace SceneCore;

CAODiskGenerator::CAODiskGenerator(CTerrain* pkTerrain)
:	m_pkTerrain(pkTerrain)
{
	assert(m_pkTerrain);
	m_iNumChunkX = m_pkTerrain->GetChunkNumX();
	m_iNumChunkY = m_pkTerrain->GetChunkNumY();
	int iNumChunks = m_iNumChunkX*m_iNumChunkY;

	m_aSceneDiskList = new list<tAODisk>[iNumChunks];	
	m_aTerrainDiskList = new list<tAODisk>[iNumChunks];

	_GenerateDisk(m_pkTerrain);

}
//--------------------------------------------------------------------------
CAODiskGenerator::~CAODiskGenerator(void)
{
	int iNumChunks = m_pkTerrain->GetChunkNumX()*m_pkTerrain->GetChunkNumY();
	for (int i=0; i<iNumChunks; i++)
	{
		m_aSceneDiskList[i].clear();
		m_aTerrainDiskList[i].clear();
	}
	SAFE_DELETE_ARRAY(m_aSceneDiskList);
	SAFE_DELETE_ARRAY(m_aTerrainDiskList);
}
//--------------------------------------------------------------------------
class MAIN_ENTRY RecursiveGenerateDisk
{
public:
	RecursiveGenerateDisk(CAODiskGenerator* pkSP) :
	  m_spDiskGenerator(pkSP) { }

	  bool operator() (NiAVObject* pkAVObject)
	  {
		  if (NULL == pkAVObject)
		  {
			  return false;
		  }

		  // 根据 Geometry 产生 disk
		  if (NiIsKindOf(NiGeometry, pkAVObject))
		  {
			  m_spDiskGenerator->GenerateDisk((NiGeometry*)pkAVObject);
		  }
		  return true;
	  }
protected:
	CAODiskGenerator* m_spDiskGenerator;
};
//--------------------------------------------------------------------------
/// 产生 disk
void CAODiskGenerator::RecursiveFindGenerateDisk(NiAVObjectPtr pkSceneRoot)
{
	// 遍历 pkSceneRoot 将所有 NiGeometry 的 顶点构造成 disk
	RecursiveGenerateDisk kFunctor(this);
	NiTNodeTraversal::DepthFirst_AllObjects(pkSceneRoot, kFunctor);
}
//--------------------------------------------------------------------------
void CAODiskGenerator::GenerateDisk(NiGeometryPtr pkGeom)
{
	pkGeom->Update(0.0f);
	NiTransform kTransform = pkGeom->GetWorldTransform();
	NiPoint3 kTranslate = pkGeom->GetTranslate();
	NiMatrix3 kRotate = pkGeom->GetRotate();
	float fScale = pkGeom->GetScale();

	NiGeometryData* pkGeomData = pkGeom->GetModelData();

	if (NiIsKindOf(NiTriBasedGeomData, pkGeomData))
	{
		NiTriBasedGeomData* pkTriGeomData = (NiTriBasedGeomData*)pkGeomData;
		unsigned short usNumVertices = pkTriGeomData->GetVertexCount();	// 顶点数量
		NiPoint3* pkVertices = pkTriGeomData->GetVertices();			// 顶点列表
		NiPoint3* pkNormals = pkTriGeomData->GetNormals();				// 法线列表
		if (pkNormals == NULL)	//  不支持没有法线列表的模型生成 disk
		{
			//pkNormals = (NiPoint3*)NiMalloc(sizeof(NiPoint3)*usNumVertices);
			return;
			//pkNormals = pkTriGeomData->GetNormals();
		}
		unsigned short usNumTringles = pkTriGeomData->GetTriangleCount();

		vector<unsigned short>* vert2TriList = 
			new vector<unsigned short>[usNumVertices];	// 顶点-三角形 对应列表
		float*	pTriAreaList = new float[usNumTringles];	// 三角形面积列表
		// 遍历所有三角形,找出顶点和三角形的对应关系
		for (unsigned short i=0; i<usNumTringles; i++)
		{
			unsigned short i0, i1, i2;
			pkTriGeomData->GetTriangleIndices(i, i0, i1, i2);
			pTriAreaList[i] = TriangleArea(pkVertices[i0], pkVertices[i1], pkVertices[i2]);
			vert2TriList[i0].push_back(i);
			vert2TriList[i1].push_back(i);
			vert2TriList[i2].push_back(i);
		}
		
		NiTransform kWorldTransform = pkGeom->GetWorldTransform();
		NiTransform kLocalTransform = pkGeom->GetLocalTransform();
		NiTransform kTransform = kWorldTransform*kLocalTransform;//

		//kTransform.m_Rotate = kRotate;
		//kTransform.m_Translate = kTranslate;
		//kTransform.m_fScale = fScale;

		NiPoint3* pkWorldNromals = (NiPoint3*)NiMalloc(sizeof(NiPoint3)*usNumVertices);
		NiMatrix3::TransformNormals(kTransform.m_Rotate, usNumVertices, pkNormals, pkWorldNromals);

		// 遍历所有顶点,计算每个顶点世界坐标的位置,世界坐标中的法线,disk 的面积.将所有 disk 添加到 disk 列表中
		for (unsigned short i=0; i<usNumVertices; i++)
		{
			tAODisk disk;
			disk.kPosition = kTransform*pkVertices[i];
			disk.kNormal = pkWorldNromals[i];
			disk.fArea = 0.0f;
			// 计算每个 disk 的面积, 所有相邻三角形面积平均值
			for (unsigned int j=0; j<vert2TriList[i].size(); j++)
			{
				int iTriIdx = vert2TriList[i][j];
				disk.fArea += pTriAreaList[iTriIdx];
			}
			disk.fArea /= vert2TriList[i].size();
			disk.fArea *= kTransform.m_fScale;

			if (disk.fArea>0.01f)
			{
				// 判断 disk 所在 chunk id, 将其放到相应 list 中
				int iChunkID = m_pkTerrain->GetChunkIndex(disk.kPosition);
				if (iChunkID>=0 && iChunkID<m_iNumChunkY*m_iNumChunkX)
				{
					m_aSceneDiskList[iChunkID].push_back(disk);
				}
			}
		}

		delete[] vert2TriList;
		delete[] pTriAreaList;
		//NiDelete pkWorldNromals
	}
	else
	{
		// 不支持其他的 NiGeometryData. 不过应该也没有了
	}
}
//--------------------------------------------------------------------------
void CAODiskGenerator::_GenerateDisk(CTerrain* pkTerrain)
{
	NiPoint2 kTotalSize = pkTerrain->GetTotalSize();

	const NiPoint3* pkVertices = pkTerrain->GetVertices();
	const NiPoint3* pkNormals = pkTerrain->GetNormals();
	const NiColorA* pkColors = pkTerrain->GetVertexColor();

	float fArea = 0.5f;

	int iNumVertexPerRow = m_iNumChunkX*GRIDINCHUNK+1;
	// 遍历所有 chunk ,逐个生成 AODisks
	int iChunkIdx = 0;
	for (int i=0; i<m_iNumChunkY; i++)
	{
		for (int j=0; j<m_iNumChunkX; j++, iChunkIdx++)
		{
			// 该 chunk 第一个顶点(左下)坐标
			int iOffsetX = j*GRIDINCHUNK;
			int iOffsetY = i*GRIDINCHUNK;
			
			// 遍历一个 chunk 中所有顶点
			for (int iVY=0; iVY<GRIDINCHUNK+1; iVY++)
			{
				for (int iVX=0; iVX<GRIDINCHUNK+1; iVX++)
				{
					int iVertIndex = (iOffsetY+iVY)*iNumVertexPerRow+iOffsetX+iVX;


					tAODisk disk;
					disk.kPosition = pkVertices[iVertIndex];
					disk.kNormal = pkNormals[iVertIndex];
					disk.fArea = fArea;
					if (pkColors[iVertIndex].a < 0.5f)
					{
						disk.fArea = 0.0f;
					}
					m_aTerrainDiskList[iChunkIdx].push_back(disk);
				}
			}
			
		}
	}

	//for (int i=0; i<(kTotalSize.x+1)*(kTotalSize.y+1); i++)
	//{
	//	tAODisk disk;
	//	disk.kPosition = pkVertices[i];
	//	disk.kNormal = pkNormals[i];
	//	disk.fArea = fArea;
	//	// 判断 disk 所在 chunk id, 将其放到相应 list 中
	//	int iChunkID = m_pkTerrain->GetChunkIndex(disk.kPosition);
	//	m_aTerrainDiskList[iChunkID].push_back(disk);
	//}
}
//--------------------------------------------------------------------------

#endif