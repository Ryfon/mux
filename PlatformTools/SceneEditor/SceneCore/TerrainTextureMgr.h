﻿/** @file TerrainTextureMgr.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-10-03
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 </pre>*/


#ifndef TERRAINE_TEXMGR_H
#define TERRAINE_TEXMGR_H

/**
 @brief 地表材质纹理管理器
 *
 *	负责地表纹理的一次性加载
 */
class MAIN_ENTRY CTerrainTextureMgr : public TSingleton< CTerrainTextureMgr >
{
	friend class TSingleton< CTerrainTextureMgr >;

public:
	/// 构造
	CTerrainTextureMgr(void);

	/// 析构
	virtual ~CTerrainTextureMgr(void);

	/// 纹理容器定义，健值为文件名string
	typedef map< string, NiSourceTexturePtr >	TextureMap;
	/// 纹理容器
	TextureMap		m_TexMap;

	/// 纹理库路径
	string			m_strPath;

public:

	/// 初始化
	bool							Initialize( const string& strPath );

	/// 结束
	void							UnInitialize();

	/// 创建纹理
	NiSourceTexturePtr				GetTexture( const char *pszFile );

	/// 移除纹理
	bool							RemoveTexture( const char *pszFile );

	/// 移除纹理
	bool							RemoveTexture( NiSourceTexture *pTexture );

	/// 清空纹理容器
	void							ClearAll();	
};

#endif

