#ifndef CODE_INGAME

inline float CAOPerVertexShadowGenerator::ElementShadow(NiPoint3 v, float r, 
							NiPoint3 kReceiveNromal, NiPoint3 kEmitterNormal, float fEmitterArea)
{
	return (1 - sqrt(fEmitterArea/(r*r)+1)) *
		NiClamp((kEmitterNormal.Dot(v)), 0.0f, 1.0f) * NiClamp((kReceiveNromal.Dot(-v)*4), 0.0f, 1.0f);
}

//	1_2
//  | |
//	0-3
inline float CAOPerVertexShadowGenerator::QuadColorInterpolation(float fColor0, float fColor1, float fColor2, float fColor3, NiPoint2 uv)
{
	float fColorLeft = fColor1*(1.0f-uv.y) + fColor0*(uv.y);
	float fColorRight = fColor2*(1.0f-uv.y) + fColor3*(uv.y);

	return fColorLeft*(1.0f-uv.x) + fColorRight*(uv.x);
}

#endif