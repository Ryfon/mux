﻿#include "StdAfx.h"
#include "PostEffect.h"

#ifndef CODE_INGAME
using namespace SceneCore;

//---------------------------------------------------------------------------------------------
CPostEffect::CPostEffect(void)
:	m_spRenderStep(NULL),
	m_bInitialized(false),
	m_uiEffectID(0)
{
}
//---------------------------------------------------------------------------------------------
CPostEffect::~CPostEffect(void)
{

}
//---------------------------------------------------------------------------------------------
bool CPostEffect::Init(NiRenderedTexturePtr pkSourceTexture)
{
	if (pkSourceTexture == NULL)
	{
		return false;
	}
	// 创建 render step
	m_spRenderStep = NiNew NiDefaultClickRenderStep;
	m_spRenderStep->SetActive(false);
	return true;
}
//---------------------------------------------------------------------------------------------
void CPostEffect::Destory()
{
	m_spRenderStep = NULL;
	m_bInitialized = false;
	m_uiEffectID = 0;
}
//---------------------------------------------------------------------------------------------
void CPostEffect::SetActive(bool bActive)
{
	if ( m_spRenderStep )
	{
		m_spRenderStep->SetActive(bActive);
	}
	
}
//---------------------------------------------------------------------------------------------
void CPostEffect::PerformRendering()
{
	if (m_spRenderStep!=NULL)
	{
		m_spRenderStep->Render();
	}

}
//---------------------------------------------------------------------------------------------
void CPostEffect::SetEffectID(unsigned int uiID)
{
	m_uiEffectID = uiID;
}
//---------------------------------------------------------------------------------------------
#endif
