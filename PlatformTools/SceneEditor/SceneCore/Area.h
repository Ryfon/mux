﻿/** @file Area.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：徐磊
*	完成日期：2007-12-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME
#ifndef _AREA_H
#define _AREA_H

//封装区域的顶点
typedef struct MAIN_ENTRY AreaPoint
{
	unsigned int uiPointID;	//每一个点的唯一标识，这样便于点的定位，也便于和场景中的物件联系起来
	int nXPos;				//点的X轴坐标
	int nYPos;				//点的Y轴坐标
}AreaPoint;

//封装一个区域
class MAIN_ENTRY CArea
{
public:
	CArea(void);
	~CArea(void);
	unsigned int LoadData(const TiXmlElement* pXmlArea);
	int SaveData(TiXmlElement* pXmlArea, int nAreaShape);
	int IsExist(unsigned int uiPointID);//判断点属于哪一个区域，返回区域的索引，不存在则返回-1
	int InsertPoint(unsigned int uiPointID, int nXPos, int nYPos);	//插入一个点
	int DeletePoint(unsigned int uiPointID);//删除一个顶点
	int UpdatePoint(unsigned int uiPointID, int nXPos, int nYPos);//更新顶点
	int GetAreaPointNum();//获取区域所包含的顶点数量
private:
	vector<AreaPoint*>::iterator _GetInsertPos(int nXPos, int nYPos);//判断插入的新的点的位置
	float _GetNearDistance(AreaPoint &PointM, AreaPoint &PointA, AreaPoint &PointB);	//计算点m到线段ab的距离
public:
	unsigned int m_uiAreaID;			//区域的ID---不可更改
	unsigned short m_usAreaType;		//区域的类型
	string m_strAreaName;				//区域的名称
	unsigned short m_usAreaShape;		//区域的形状---不可更改
	//////////////////////////////////////////////////////////////////////////
	/// 新添加的音乐音效属性
	string m_strMusicFile;				//区域上绑定的音乐文件
	unsigned short m_usVolume;			//区域的音量
	unsigned short m_usMusicLoop;		//是否循环播放音乐音效
	//////////////////////////////////////////////////////////////////////////
public:
	vector<AreaPoint*> m_AreaPointList;	//区域所包含的点
};

#endif
#endif