﻿/** @file TextureSearchPath.h 
@brief 纹理搜索路径
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-04-08
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：
</pre>*/
#pragma once
#include <NiSearchPath.h>

class MAIN_ENTRY CTextureSearchPath :
	public NiSearchPath
{
public:
	CTextureSearchPath(void);
	~CTextureSearchPath(void);

	/// 增加一个搜索路径
	void AddSearchPath(string strPath, unsigned int uiStringLen);

	virtual void Reset();

	// 这个函数原形不对,少了一个参数.不过暂时用不到了.以后要用的话注意修改
	virtual bool GetNextSearchPath(char* pcPath);	

private:
	int m_iInSearchingPath;	// 当前正被搜索路径索引

	vector<string>	m_SearchPaths;	// 搜索路径列表
};
