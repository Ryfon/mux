﻿/** @file IPostEffect.h 
@brief 后期效果接口
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-04-18
*
*	取代版本：
*	作    者：和萌
*	完成日期：
*
*  备    注：
</pre>*/

#ifndef _POSTEFFECT_H
#define _POSTEFFECT_H

#pragma once

#ifndef CODE_INGAME

namespace SceneCore
{

	class MAIN_ENTRY CPostEffect
	{
	public:
		CPostEffect(void);
		virtual ~CPostEffect(void);

		/// 初始化 
		//需要源textur
		virtual bool Init(NiRenderedTexturePtr pkSourceTexture);

		/// 销毁
		virtual void Destory();

		//Render
		virtual void PerformRendering();

		/// 打开 or 关闭
		virtual		void	SetActive(bool bActive);

		virtual	const char* GetName() = 0;

		bool			GetInitialized() const	{return m_bInitialized;};
		bool			GetActive() const	{ return m_spRenderStep->GetActive(); };
		unsigned int	GetEffectID()		{ return m_uiEffectID; };
		void			SetEffectID(unsigned int uiID );		

		NiRenderStep*	GetRenderStep()	const { return m_spRenderStep; };

	private:	
		CPostEffect(CPostEffect&);

	protected:
		NiDefaultClickRenderStepPtr		m_spRenderStep;					// 默认RenderStep
		bool							m_bInitialized;					// 效果是否开启
		unsigned int					m_uiEffectID;
	};
};

#endif 
#endif
