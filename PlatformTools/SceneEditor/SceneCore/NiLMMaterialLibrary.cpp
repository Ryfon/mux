﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2008 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Calabasas, CA 91302
// http://www.emergent.net

#include "stdafx.h"
#include "NiLightMapMaterial.h"
#ifndef CODE_INGAME

NiLightMapMaterialLibrary* NiLightMapMaterialLibrary::ms_pThis = 0;

//-----------------------------------------------------------------------------------------------
NiMaterialLibrary* NiLightMapMaterialLibrary::Create()
{
    if (!ms_pThis)
        ms_pThis = NiNew NiLightMapMaterialLibrary;
    return ms_pThis;
}
//-----------------------------------------------------------------------------------------------
void NiLightMapMaterialLibrary::Shutdown()
{
    if (!ms_pThis)
        return;
    NiDelete ms_pThis;
    ms_pThis = 0;
}
//-----------------------------------------------------------------------------------------------
NiLightMapMaterialLibrary::NiLightMapMaterialLibrary()
: NiMaterialLibrary("LightMapMaterial")
{
    m_pShaderDescArray[0] = NiNew NiShaderDesc;
    m_pShaderDescArray[0]->SetName("LightMapMaterial");
    m_pShaderDescArray[0]->SetConfigurableMaterial(true);
    m_pShaderDescArray[0]->SetDescription(
        "NiStandardMaterial subclass with lightmapping support.");

    NiShaderAttributeDesc* pAttribDesc = NiNew NiShaderAttributeDesc;
    pAttribDesc->SetName("LightMap");
    pAttribDesc->SetType(NiShaderAttributeDesc::ATTRIB_TYPE_TEXTURE);
    pAttribDesc->SetHidden(false);
    pAttribDesc->SetValue_Texture(0, "DefaultLightMap.tga");
    m_pShaderDescArray[0]->AddAttribute(pAttribDesc);
}
//-----------------------------------------------------------------------------------------------
NiLightMapMaterialLibrary::~NiLightMapMaterialLibrary()
{
    if (m_pShaderDescArray[0])
        NiDelete m_pShaderDescArray[0];
}
//-----------------------------------------------------------------------------------------------
NiShaderDesc* NiLightMapMaterialLibrary::GetFirstMaterialDesc()
{
    m_shaderDescIndex = 0;
    return m_pShaderDescArray[0];
}
//-----------------------------------------------------------------------------------------------
NiShaderDesc* NiLightMapMaterialLibrary::GetNextMaterialDesc()
{
    if (++m_shaderDescIndex < (unsigned int)NUM_MATERIALS)
        return m_pShaderDescArray[m_shaderDescIndex];
    return 0;
}
//-----------------------------------------------------------------------------------------------
NiMaterial* NiLightMapMaterialLibrary::GetMaterial(const NiFixedString& name)
{
    if (name.Equals("LightMapMaterial"))
        return NiNew NiLightMapMaterial;
    
    return 0;
}
//-----------------------------------------------------------------------------------------------
const char* NiLightMapMaterialLibrary::GetMaterialName(unsigned int ui)
{
    switch (ui)
    {
    case 0: return "LightMapMaterial";
    default: return 0;
    }
}
//-----------------------------------------------------------------------------------------------
unsigned int NiLightMapMaterialLibrary::GetMaterialCount() const
{
    return NUM_MATERIALS;
}
//-----------------------------------------------------------------------------------------------
#endif	// CODE_INGAME