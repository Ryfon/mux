﻿#include "stdafx.h"

#ifndef CODE_INGAME

#include "NPC.h"

CNPC::CNPC(void)
{
	m_NPCNO = 0;
	m_Name = "";
	m_Desc = "";
	m_Level = 1;
	m_Type = 1;
	m_RaceType = 1;
	m_Size = 1;
	m_Exp = 0;
	m_ExpLimitLevel = 0;
	m_MaxHp = 0;
	m_HpUp = 0;
	m_SpeedLevel = 0;
	m_ViewSpan = 0.0;
	m_PatrolSpan = 0.0;
	m_ChaseSpan = 0.0;
	m_ChaseTime = 0;
	m_AttType = 1;
	m_AttMin = 0;
	m_AttMax = 0;
	m_ActionName = "";
	m_HeartType = 1;
	m_AttDistMin = 0;
	m_AttDistMax = 0;
	m_AttRadius = 0.0;
	m_AttFreq = 0.0;
	m_SpaceTime = 0;
	m_AttProb = 0.0;
	m_FinalKill = 0;
	m_Hit = 0;
	m_DefMin = 0;
	m_DefMax = 0;
	m_AttSpeed = 0;
	m_MoveSpeed = 0;
	m_Critical = 0.0;
	m_AttProp = 0;
	m_AttObjSel = 0;
	m_AttObjSelData = "";
	m_ChaseTimes = 0;
	m_AIFireCond = 0;
	m_AIFireData = 0;
	m_EscapeRatio = 0;
	m_EscapeDist = 0;
	m_EscapeTime = 0;
	m_Dist = 0;
	m_AcceptAID = 0;
	m_AddBloodRatio = 0;
	m_AidBloodRatio = 0;
	m_AidBoostRatio = 0;
	m_UnchainStateRatio = 0;
	m_SkillRatio = 0;
	m_SummonRatio = 0;
	m_SummonNum = 0;
	m_SummonNPCNO = 0;
	m_CarryRatio = 0;
	m_TransRatio = 0;
	m_ReturnBorn = 0;
	m_ForeverState = "";
	m_AddBloodId = "";
	m_PowerRise= 0;
	m_PlusID = "";
	m_DamnifyID = "";
	m_ModalNO = 0;
	m_HeadImg = 0;
	m_Logo = 0;
	m_Text = 0;
	m_Cadre = 0;
	m_Movie = 0;
	m_Length = 1;
	m_Width = 1;
	m_TrimSize = false;
	m_ActionNO = "";

	m_NPCAppearance = "";	//M1新添加的数据
}

CNPC::~CNPC(void)
{
	
}

int CNPC::LoadData(const TiXmlElement* pXmlNPC)
{
	m_NPCNO = atoi(pXmlNPC->Attribute("NO"));
	m_Name = pXmlNPC->Attribute("Name");
	m_Desc = pXmlNPC->Attribute("Desc");
	m_Level = atoi(pXmlNPC->Attribute("Level"));
	m_Type = atoi(pXmlNPC->Attribute("Type"));
	m_RaceType = atoi(pXmlNPC->Attribute("RaceType"));
	m_Size = atoi(pXmlNPC->Attribute("Size"));
	m_Exp = atoi(pXmlNPC->Attribute("Exp"));
	m_ExpLimitLevel = atoi(pXmlNPC->Attribute("ExpLimitLevel"));
	m_MaxHp = atoi((pXmlNPC->Attribute("MaxHp")));
	m_HpUp = atoi(pXmlNPC->Attribute("HpUp"));
	m_SpeedLevel = atoi(pXmlNPC->Attribute("SpeedLevel"));
	m_ViewSpan = static_cast<float>(atof(pXmlNPC->Attribute("ViewSpan")));
	m_PatrolSpan = static_cast<float>(atof(pXmlNPC->Attribute("PatrolSpan")));
	m_ChaseSpan = static_cast<float>(atof(pXmlNPC->Attribute("ChaseSpan")));
	m_ChaseTime = atoi(pXmlNPC->Attribute("ChaseTime"));
	m_AttType = atoi(pXmlNPC->Attribute("AttType"));
	m_AttMin = atoi(pXmlNPC->Attribute("AttMin"));
	m_AttMax = atoi(pXmlNPC->Attribute("AttMax"));
	m_ActionName = pXmlNPC->Attribute("ActionName");
	m_HeartType = atoi(pXmlNPC->Attribute("HeartType"));
	m_AttDistMin = atoi(pXmlNPC->Attribute("AttDistMin"));
	m_AttDistMax = atoi(pXmlNPC->Attribute("AttDistMax"));
	m_AttRadius = static_cast<float>(atof(pXmlNPC->Attribute("AttRadius")));
	m_AttFreq = static_cast<float>(atof(pXmlNPC->Attribute("AttFreq")));
	m_SpaceTime = atoi(pXmlNPC->Attribute("SpaceTime"));
	m_AttProb = static_cast<float>(atof(pXmlNPC->Attribute("AttProb")));
	m_FinalKill = atoi(pXmlNPC->Attribute("FinalKill"));
	m_Hit = atoi(pXmlNPC->Attribute("Hit"));
	m_DefMin = atoi(pXmlNPC->Attribute("DefMin"));
	m_DefMax = atoi(pXmlNPC->Attribute("DefMax"));
	m_AttSpeed = atoi(pXmlNPC->Attribute("AttSpeed"));
	m_MoveSpeed = atoi(pXmlNPC->Attribute("MoveSpeed"));
	m_Critical = static_cast<float>(atof(pXmlNPC->Attribute("Critical")));
	m_AttProp = atoi(pXmlNPC->Attribute("AttProp"));
	m_AttObjSel = atoi(pXmlNPC->Attribute("AttObjSel"));
	m_AttObjSelData = pXmlNPC->Attribute("AttObjSelData");
	m_ChaseTimes = atoi(pXmlNPC->Attribute("ChaseTimes"));
	m_AIFireCond = atoi(pXmlNPC->Attribute("AIFireCond"));
	m_AIFireData = atoi(pXmlNPC->Attribute("AIFireData"));
	m_EscapeRatio = static_cast<float>(atof(pXmlNPC->Attribute("EscapeRatio")));
	m_EscapeDist = static_cast<float>(atof(pXmlNPC->Attribute("EscapeDist")));
	m_EscapeTime = atoi(pXmlNPC->Attribute("EscapeTime"));
	m_Dist = static_cast<float>(atof(pXmlNPC->Attribute("Dist")));
	m_AcceptAID = atoi(pXmlNPC->Attribute("AcceptAID"));
	m_AddBloodRatio = static_cast<float>(atof(pXmlNPC->Attribute("AddBloodRatio")));
	m_AidBloodRatio = static_cast<float>(atof(pXmlNPC->Attribute("AidBloodRatio")));
	m_AidBoostRatio = static_cast<float>(atof(pXmlNPC->Attribute("AidBoostRatio")));
	m_UnchainStateRatio = static_cast<float>(atof(pXmlNPC->Attribute("UnchainStateRatio")));
	m_SkillRatio = static_cast<float>(atof(pXmlNPC->Attribute("SkillRatio")));
	m_SummonRatio = static_cast<float>(atof(pXmlNPC->Attribute("SummonRatio")));
	m_SummonNum = atoi(pXmlNPC->Attribute("SummonNum"));
	m_SummonNPCNO = atoi(pXmlNPC->Attribute("SummonNPCNO"));
	m_CarryRatio = static_cast<float>(atof(pXmlNPC->Attribute("CarryRatio")));
	m_TransRatio = static_cast<float>(atof(pXmlNPC->Attribute("TransRatio")));
	m_ReturnBorn = atoi(pXmlNPC->Attribute("ReturnBorn"));
	m_ForeverState = pXmlNPC->Attribute("ForeverState");
	m_AddBloodId = pXmlNPC->Attribute("AddBloodId");
	m_PowerRise= atoi(pXmlNPC->Attribute("PowerRise"));
	m_PlusID = pXmlNPC->Attribute("PlusID");
	m_DamnifyID = pXmlNPC->Attribute("DamnifyID");
	m_ModalNO = atoi(pXmlNPC->Attribute("ModalNO"));
	m_HeadImg = atoi(pXmlNPC->Attribute("HeadImg"));
	m_Logo = atoi(pXmlNPC->Attribute("Logo"));
	m_Text = atoi(pXmlNPC->Attribute("Text"));
	m_Cadre = atoi(pXmlNPC->Attribute("Cadre"));
	m_Movie = atoi(pXmlNPC->Attribute("Movie"));
	m_Length = atoi(pXmlNPC->Attribute("Length"));
	m_Width = atoi(pXmlNPC->Attribute("Width"));
	m_TrimSize = atoi(pXmlNPC->Attribute("TrimSize")) != 0;

	m_ActionNO = pXmlNPC->Attribute("ActionNO");

	for (const TiXmlElement* pXmlSkill = pXmlNPC->FirstChildElement(); pXmlSkill; pXmlSkill = pXmlSkill->NextSiblingElement())
	{
		unsigned long ulSkillNo = atol(pXmlSkill->GetText());
		AddSkillNo(ulSkillNo);
	}
	return 0;
}

int CNPC::LoadDataM1(const TiXmlElement* pXmlNPC)
{
	m_NPCNO = atoi(pXmlNPC->Attribute("nId"));
	m_Name = pXmlNPC->Attribute("nNPCName");
	m_NPCAppearance = pXmlNPC->Attribute("nNPCAppearance");
	return 0;
}

int CNPC::SaveData(TiXmlElement* pXmlNPC)
{
	pXmlNPC->SetAttribute("NO", m_NPCNO);
	pXmlNPC->SetAttribute("Name", m_Name.c_str());
	pXmlNPC->SetAttribute("Desc", m_Desc.c_str());
	pXmlNPC->SetAttribute("Level", m_Level);
	pXmlNPC->SetAttribute("Type", m_Type);
	pXmlNPC->SetAttribute("RaceType", m_RaceType);
	pXmlNPC->SetAttribute("Size", m_Size);
	pXmlNPC->SetAttribute("Exp", m_Exp);
	pXmlNPC->SetAttribute("ExpLimitLevel", m_ExpLimitLevel);
	pXmlNPC->SetAttribute("MaxHp", m_MaxHp);
	pXmlNPC->SetAttribute("HpUp", m_HpUp);
	pXmlNPC->SetAttribute("SpeedLevel", m_SpeedLevel);
	pXmlNPC->SetDoubleAttribute("ViewSpan", m_ViewSpan);
	pXmlNPC->SetDoubleAttribute("ChaseSpan", m_ChaseSpan);
	pXmlNPC->SetAttribute("ChaseTime", m_ChaseTime);
	pXmlNPC->SetDoubleAttribute("PatrolSpan", m_PatrolSpan);

	pXmlNPC->SetAttribute("AttType", m_AttType);
	pXmlNPC->SetAttribute("AttMin", m_AttMin);
	pXmlNPC->SetAttribute("AttMax", m_AttMax);
	pXmlNPC->SetAttribute("ActionName", m_ActionName.c_str());
	pXmlNPC->SetAttribute("HeartType", m_HeartType);
	pXmlNPC->SetAttribute("AttDistMin", m_AttDistMin);
	pXmlNPC->SetAttribute("AttDistMax", m_AttDistMax);
	pXmlNPC->SetDoubleAttribute("AttRadius", m_AttRadius);
	pXmlNPC->SetDoubleAttribute("AttFreq", m_AttFreq);
	pXmlNPC->SetAttribute("SpaceTime", m_SpaceTime);
	pXmlNPC->SetDoubleAttribute("AttProb", m_AttProb);
	pXmlNPC->SetAttribute("FinalKill", m_FinalKill);
	pXmlNPC->SetAttribute("Hit", m_Hit);
	pXmlNPC->SetAttribute("DefMin", m_DefMin);
	pXmlNPC->SetAttribute("DefMax", m_DefMax);
	pXmlNPC->SetAttribute("AttSpeed", m_AttSpeed);
	pXmlNPC->SetAttribute("MoveSpeed", m_MoveSpeed);
	pXmlNPC->SetDoubleAttribute("Critical", m_Critical);
	pXmlNPC->SetAttribute("AttProp", m_AttProp);
	pXmlNPC->SetAttribute("AttObjSel", m_AttObjSel);
	pXmlNPC->SetAttribute("AttObjSelData", m_AttObjSelData.c_str());
	pXmlNPC->SetAttribute("ChaseTimes", m_ChaseTimes);
	pXmlNPC->SetAttribute("AIFireCond", m_AIFireCond);
	pXmlNPC->SetAttribute("AIFireData", m_AIFireData);
	pXmlNPC->SetDoubleAttribute("EscapeRatio", m_EscapeRatio);
	pXmlNPC->SetDoubleAttribute("EscapeDist", m_EscapeDist);
	pXmlNPC->SetAttribute("EscapeTime", m_EscapeTime);
	pXmlNPC->SetDoubleAttribute("Dist", m_Dist);
	pXmlNPC->SetAttribute("AcceptAID", m_AcceptAID);
	pXmlNPC->SetDoubleAttribute("AddBloodRatio", m_AddBloodRatio);
	pXmlNPC->SetDoubleAttribute("AidBloodRatio", m_AidBloodRatio);
	pXmlNPC->SetDoubleAttribute("AidBoostRatio", m_AidBoostRatio);
	pXmlNPC->SetDoubleAttribute("UnchainStateRatio", m_UnchainStateRatio);
	pXmlNPC->SetDoubleAttribute("SkillRatio", m_SkillRatio);
	pXmlNPC->SetDoubleAttribute("SummonRatio", m_SummonRatio);
	pXmlNPC->SetAttribute("SummonNum", m_SummonNum);
	pXmlNPC->SetAttribute("SummonNPCNO", m_SummonNPCNO);
	pXmlNPC->SetDoubleAttribute("CarryRatio", m_CarryRatio);
	pXmlNPC->SetDoubleAttribute("TransRatio", m_TransRatio);
	pXmlNPC->SetAttribute("ReturnBorn", m_ReturnBorn);

	pXmlNPC->SetAttribute("ForeverState", m_ForeverState.c_str());
	pXmlNPC->SetAttribute("AddBloodId", m_AddBloodId.c_str());
	pXmlNPC->SetAttribute("PowerRise", m_PowerRise);
	pXmlNPC->SetAttribute("PlusID", m_PlusID.c_str());
	pXmlNPC->SetAttribute("DamnifyID", m_DamnifyID.c_str());
	pXmlNPC->SetAttribute("ModalNO", m_ModalNO);
	pXmlNPC->SetAttribute("HeadImg", m_HeadImg);
	pXmlNPC->SetAttribute("Logo", m_Logo);
	pXmlNPC->SetAttribute("Text", m_Text);
	pXmlNPC->SetAttribute("Cadre", m_Cadre);
	pXmlNPC->SetAttribute("Movie", m_Movie);
	pXmlNPC->SetAttribute("Length", m_Length);
	pXmlNPC->SetAttribute("Width", m_Width);
	pXmlNPC->SetAttribute("TrimSize", m_TrimSize);

	pXmlNPC->SetAttribute("ActionNO", m_ActionNO.c_str() );

	for (size_t nIndex = 0; nIndex < m_SkillInfo.size(); nIndex++)
	{
		TiXmlElement* pXmlSkill = new TiXmlElement("Skill");
		string strTemp;
		stringstream ssTemp;
		ssTemp << m_SkillInfo[nIndex];
		ssTemp >> strTemp;
		TiXmlText* pXmlText = new TiXmlText(strTemp.c_str());
		pXmlSkill->InsertEndChild(*pXmlText);
		pXmlNPC->LinkEndChild(pXmlSkill);
	}
	return 0;
}

int CNPC::AddSkillNo(unsigned long ulSkillNo)
{
	for (size_t nIndex = 0; nIndex < m_SkillInfo.size(); nIndex++)
	{
		if (ulSkillNo == m_SkillInfo[nIndex])
		{
			return -1;
		}
	}

	m_SkillInfo.push_back(ulSkillNo);
	return 0;
}

int CNPC::DelSkillNo(unsigned long ulSkillNo)
{
	for (vector<unsigned long>::iterator Ite = m_SkillInfo.begin(); Ite != m_SkillInfo.end(); Ite++)
	{
		if (ulSkillNo == *Ite)
		{
			m_SkillInfo.erase(Ite);
			return 0;
		}
	}

	return 0;
}


#endif