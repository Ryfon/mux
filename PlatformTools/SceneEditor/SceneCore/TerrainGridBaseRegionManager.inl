
inline void CTerrainGridBaseRegionManager::PaintGridBaseRegion(CTerrain* pTerrain, const NiPoint3& kCenter, const float fRadius, bool bAdd)
{
	CTerrainGridBaseRegion* pOnEditRegion = CTerrainGridBaseRegionManager::GetInstance()->GetOnEditRegion();
	if (pTerrain==NULL || pOnEditRegion==NULL)
	{
		return;
	}

	static std::set<int> gridSet;
	gridSet.clear();

	int iCenterGridID = pTerrain->GetGridIndex(kCenter);
	pTerrain->GetGridArray(iCenterGridID, (WORD)fRadius, gridSet);

	if (bAdd)
	{
		std::set<int>::iterator iter = gridSet.begin();
		while (iter != gridSet.end())
		{
			pOnEditRegion->AddGridID(*iter);
			iter++;
		}
	}
	else
	{
		std::set<int>::iterator iter = gridSet.begin();
		while (iter != gridSet.end())
		{
			pOnEditRegion->RemoveGridID(*iter);
			iter++;
		}
	}
}
//------------------------------------------------------------------------------
inline NiGeometry* CTerrainGridBaseRegionManager::GenerateParticlesFromGridBaseRegion(CTerrain* pTerrain, CTerrainGridBaseRegion* pRegion)
{
	if (pTerrain == NULL || pRegion == NULL) return NULL;

	std::set<int>& gridSet = pRegion->GetGridSet();
	unsigned int uiGridNum = gridSet.size();	// grid 数量

	int iGridInRow = pTerrain->GetChunkNumX()*GRIDINCHUNK;	// 每行 grid 数量
	int iVertInRow = iGridInRow + 1;	// 每行顶点数量

	const NiPoint3* pTerrVert = pTerrain->GetVertices();

	NiPoint3* pVert = NiNew NiPoint3[uiGridNum];	
	NiColorA* pColor = NiNew NiColorA[uiGridNum];

	// 填充顶点列表
	int i = 0;
	std::set<int>::iterator iter = gridSet.begin();
	while (iter != gridSet.end())
	{
		int iGridID = *iter;
		// 将 Grid ID 转换为 Vertex ID
		int iX = iGridID % iGridInRow;
		int iY = iGridID / iGridInRow;
		int iVertID = iY * iVertInRow + iX;
		pVert[i++] = pTerrVert[iVertID];
		
		iter++;
	}

	NiParticles* pkParticles = NiNew NiParticles(uiGridNum, pVert, NULL, pColor);
	pkParticles->IncRefCount();
	float fRadii = 0.2f;
	float* pfSizes = ((NiParticlesData*)pkParticles->GetModelData())->GetSizes();
	for (unsigned short usCount=0; usCount<pkParticles->GetVertexCount(); usCount++)
	{
		pfSizes[usCount] = 0.2f;
	}

	pkParticles->Update(0.0f);
	pkParticles->UpdateProperties();
	//pLine->UpdateEffects();

	return pkParticles;

}
//------------------------------------------------------------------------------
inline NiGeometry* CTerrainGridBaseRegionManager::GenerateTriGeomFromGridBaseRegion(
								CTerrain* pTerrain, CTerrainGridBaseRegion* pRegion)
{
	// 1. 遍历所有被选择的 grid, 按chunk放到不同的容器中
	// 2. 对于每个 chunk 中的

	if (pTerrain == NULL || pRegion == NULL) return NULL;

	// 取得被选择的 grid id 列表
	std::set<int>& gridSet =  pRegion->GetGridSet();
	unsigned int uiGridNum = gridSet.size();	// grid 数量

	int iGridInRow = pTerrain->GetChunkNumX()*GRIDINCHUNK;	// 每行 grid 数量

	return NULL;
}