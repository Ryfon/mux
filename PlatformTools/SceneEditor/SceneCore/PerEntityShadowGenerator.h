﻿/** @file PerEntityShadowGenerator.h 
@brief 使用 MOUT Demo 中 Shadow Geometry 为每个物件生成 Projection Shadow
<pre>
*	Copyright (c) 2008，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2008-05-08
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
#include <NiEntityInterface.h>
#include "terrain.h"

#ifndef CODE_INGAME
#include "ShadowGeometry.h"

namespace SceneCore
{

class MAIN_ENTRY CPerEntityShadowGenerator
{
public:
	CPerEntityShadowGenerator(CTerrain* pkTerrain);
	~CPerEntityShadowGenerator(void);

	/// 添加阴影投射物体
	void AddShadowCaster(NiEntityInterface* pkEntity);

	/// 
	/**
	*	<BR>功能说明：创建 iChunkID 的投影阴影,并且写入 chunk 的 blend texture的 alpha 通道
	*	<BR>可访问性：public
	*	<BR>注    释：
	*	@param	kLitDir[in]				光照方向
	*	@param	fMaxShadowValue[in]		最大阴影深度 [0~1]
	*	@param	fBlur					模糊程度
	*	@return 无
	*/
	void CastProjectionShadow(CTerrain* pkTerrain, NiPoint3 kLitDir, float fMaxShadowValue, float fBlur);

	CTerrain* GetTargetTerrain() {return m_pkTerrain;}

protected:

	// 对 pkTexture 进行 U/V 方向的模糊
	void _BlurTexture(NiRenderedTexture* pkTexture, float fBlur);

	list<NiEntityInterface*>	*m_aEntityList;		// 场景所有物件列表,按照 chunk 分别存放
	CTerrain*					m_pkTerrain;		// 目标地形
	
};
};
#endif