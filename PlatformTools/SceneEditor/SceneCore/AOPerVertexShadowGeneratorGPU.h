﻿/** @file AOPerVertexShadowGeneratorGPU.h 
@brief 利用 GPU 多官线并行处理,逐顶点地形 AO 阴影产生.利用场景物件和地形产生 AODisk, 逐地形顶点计算阴影,将结果插值写入 blend texture 的 alpha 通道.
<pre>
*	Copyright (c) 2008，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2008-04-23
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#ifndef CODE_INGAME

#include "AODiskGenerator.h"

// 在 2X2 chunk 下会显卡过热导致死机。所以放弃使用。
// 速度比 cpu 的的确快很多。
// 以后如果有更好硬件设备可以尝试大一些的地形。

namespace SceneCore
{
	class MAIN_ENTRY CAOPerVertexShadowGeneratorGPU
	{
	public:
		CAOPerVertexShadowGeneratorGPU(CTerrain* pkTerrain);
		~CAOPerVertexShadowGeneratorGPU(void);

		/// 添加投射阴影的物件,利用该物件产生 AODisk
		void AddShadowEmitter(NiAVObjectPtr pkAVObj);

		/// 产生地形阴影
		void GenerateTerrainShadow();

	protected:
		CTerrain*			m_pkTerrain;		// 产生阴影的目标地形
		CAODiskGenerator	m_kDiskGenerator;	// AODisk 产生器
		const static int	TEXTURE_SIZE = 2048;// 存放 AODisk 信息 的纹理 
	};

};

#endif
