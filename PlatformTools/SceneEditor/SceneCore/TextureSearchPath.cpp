﻿#include "StdAfx.h"
#include "TextureSearchPath.h"

CTextureSearchPath::CTextureSearchPath(void)
:	m_iInSearchingPath(0)
{
}

CTextureSearchPath::~CTextureSearchPath(void)
{
}

void CTextureSearchPath::Reset()
{
	m_iInSearchingPath = 0;
	m_uiNextPath = m_SearchPaths.size()+1;
}

bool CTextureSearchPath::GetNextSearchPath(char* pcPath)
{
	if (NULL == pcPath || m_iInSearchingPath == m_SearchPaths.size())
	{
		return false;
	}

	strcpy_s(pcPath, strlen(m_SearchPaths[m_iInSearchingPath].c_str()), m_SearchPaths[m_iInSearchingPath].c_str());
	m_iInSearchingPath++;

	return true;

}

void CTextureSearchPath::AddSearchPath(string strPath, unsigned int uiStringLen)
{
	// 在已存在路径中搜索 strPath.如果已存在则不添加
	vector<string>::iterator iter = m_SearchPaths.begin();
	while(iter != m_SearchPaths.end())
	{
		if ((*iter) == strPath)
		{
			return;
		}
		iter++;
	}

	m_SearchPaths.push_back(strPath);
}
