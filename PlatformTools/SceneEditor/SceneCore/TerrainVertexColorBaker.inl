//#ifndef CODE_INGAME
//#include "TerrainVertexColorBaker.h"
#include "float.h"

inline void CTerrainVertexColorBaker::Light(NiPoint3 kPos, NiPoint3 kNormal, NiLight* pkLight, NiColor& kDiffuseAccum, NiColor& kAmbientAccum)
{
	NiPoint3 kLightVector(0, 0, 0);	// 被光照顶点的光照方向
	float fAttenuation = 1.0f;	// 衰减
	float fDimmer = pkLight->GetDimmer();
	// 确定灯光类型
	if (NiIsKindOf(NiDirectionalLight, pkLight))
	{
		NiDirectionalLight* pDirLight = NiDynamicCast(NiDirectionalLight, pkLight);
		kLightVector = -(pDirLight->GetWorldDirection());
		kLightVector.Unitize();
	}
	else if(NiIsKindOf(NiPointLight, pkLight))
	{
		NiPointLight* pPointLight = NiDynamicCast(NiPointLight, pkLight);
		kLightVector = pPointLight->GetWorldLocation() - kPos;
		float fDistance = kLightVector.Unitize();	// 顶点到光源的距离

		// 计算衰减
		fAttenuation =	pPointLight->GetConstantAttenuation() + 
						pPointLight->GetLinearAttenuation()*fDistance +
						pPointLight->GetQuadraticAttenuation()*fDistance*fDistance;

		fAttenuation = max(1.0f, fAttenuation);
		fAttenuation = 1.0f / fAttenuation;
	}
	else if(NiIsKindOf(NiAmbientLight, pkLight))
	{
		NiAmbientLight* pAmbientLight = NiDynamicCast(NiAmbientLight, pkLight);
		NiColor kAmbColor = pAmbientLight->GetAmbientColor()*pAmbientLight->GetDimmer();
		kAmbientAccum += kAmbColor;
		return;
	}
	else
	{
		// 不支持聚光灯
		return;
	}

	// Take N dot L as intensity.
	float fLightNDotL = kLightVector.Dot(kNormal);
	float fLightIntensity = max(0, fLightNDotL);

	kDiffuseAccum += pkLight->GetDiffuseColor()*fAttenuation*fLightIntensity*fDimmer;
	kAmbientAccum += pkLight->GetAmbientColor()*fAttenuation*fDimmer;
}

//#endif