﻿#pragma once

/** @file ColorAdjustEffect.h 
@brief 颜色调节shader
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：
</pre>*/

#pragma once

#ifndef CODE_INGAME

#include "PostEffect.h"

struct MAIN_ENTRY  stMyPostEffectData 
{
	float		fBrightAdjustValue;
	float		fContrastAdjustValue;
	float		fHSLAdjustHValue;
	float		fHSLAdjustSValue;
};

namespace SceneCore
{
	NiSmartPointer(CColorAdjustEffect);
	class MAIN_ENTRY CColorAdjustEffect:public CPostEffect, public NiAVObject 
	{
	public:
		CColorAdjustEffect(void);
		~CColorAdjustEffect(void);

		//初始化
		virtual bool Init( NiRenderedTexturePtr pkSourceTexture );

		//销毁函数
		virtual void Destroy();

		virtual	const char* GetName();

		//初始化需要外部带入的变量
		void UpdateData(float fBright,float fContrast,float fHSLAdjustH,float fHSLAdjustS);
		virtual void PerformRendering();

	private:
		/// 创建渲染到全屏 render view
		void		_CreateScreenFillingRenderViews();
		//回调函数，调用shader 
		static bool	_PrePostEffectCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );
		//绘制到最终output view
		static bool _PreDrawSceneCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );
		//

		//长、宽
		unsigned int m_uiWidth;
		unsigned int m_uiHeight;

		//亮度调整值
		stMyPostEffectData m_stData;

		//Render-to-texture全屏View
		NiScreenFillingRenderViewPtr	m_spRTView;
		//纹理属性
		NiTexturingProperty*			m_pkRTViewTexProp;

		//Render-to-Output RTG全屏View
		NiScreenFillingRenderViewPtr	m_spDrawScreenRTView;

		//输出target，绑定源texture
		NiRenderTargetGroupPtr			m_spOutputRenderTarget;
		NiRenderedTexturePtr			m_spSourceTexture;

		//过程像素矩阵处理目标组
		NiRenderTargetGroupPtr			m_spRTGroup;
		//目标组纹理
		NiRenderedTexturePtr			m_spTexture;

		//调用shader的material
		//亮度
		NiMaterialPtr					m_spMtrl;

	};//end of class
};	// end of namespace
#endif