﻿#include "StdAfx.h"
#include "WaterRenderView.h"

using namespace SceneCore;

NiImplementRTTI(CWaterRenderView, NiRenderView);

CWaterRenderView::CWaterRenderView(NiCamera* pkCamera, NiCullingProcess* pkCullingProcess,
								   bool bAlwaysUseCameraViewport)
								   : m_spCamera( pkCamera )
								   , m_spCullingProcess( pkCullingProcess )
								   , m_bAlwaysUseCameraViewport( bAlwaysUseCameraViewport )
								   , m_pkFrameVisibleArray( NULL )
{
}
//-------------------------------------------------------------------------------------------------
CWaterRenderView::~CWaterRenderView()
{
	if ( m_pkFrameVisibleArray )
	{
		m_pkFrameVisibleArray->RemoveAll();
	}
}
//-------------------------------------------------------------------------------------------------
void CWaterRenderView::SetCamera( NiCamera* pCamera )
{
	m_spCamera = pCamera;
}
//-------------------------------------------------------------------------------------------------
NiCamera* CWaterRenderView::GetCamera() const
{
	return m_spCamera;
}
//-------------------------------------------------------------------------------------------------
void CWaterRenderView::SetCullingProcess( NiCullingProcess* pCullingProcess )
{
	m_spCullingProcess = pCullingProcess;
}
//-------------------------------------------------------------------------------------------------
NiCullingProcess* CWaterRenderView::GetCullingProcess() const
{
	return m_spCullingProcess;
}
//-------------------------------------------------------------------------------------------------
void CWaterRenderView::SetAlwaysUseCameraViewport( bool bAlwaysUseCameraViewport )
{
	m_bAlwaysUseCameraViewport = bAlwaysUseCameraViewport;
}
//-------------------------------------------------------------------------------------------------
bool CWaterRenderView::GetAlwaysUseCameraViewport() const
{
	return m_bAlwaysUseCameraViewport;
}
//-------------------------------------------------------------------------------------------------
void CWaterRenderView::SetCameraData(const NiRect<float>& kViewport)
{
	NIASSERT( m_spCamera );

	const NiRect<float>* pViewportToUse = NULL;
	if (m_bAlwaysUseCameraViewport)
	{
		pViewportToUse = &m_spCamera->GetViewPort();
	}
	else
	{
		pViewportToUse = &kViewport;
	}

	NiRenderer::GetRenderer()->SetCameraData( m_spCamera->GetWorldLocation(),
		m_spCamera->GetWorldDirection(), m_spCamera->GetWorldUpVector(),
		m_spCamera->GetWorldRightVector(), m_spCamera->GetViewFrustum(),
		*pViewportToUse );
}
//-------------------------------------------------------------------------------------------------
void CWaterRenderView::CalculatePVGeometry()
{
	if ( !m_pkFrameVisibleArray )
		return;

	for (unsigned int i=0; i<m_pkFrameVisibleArray->GetCount(); i++)
	{
		if ( !IsWaterObject( &m_pkFrameVisibleArray->GetAt(i) ) )
			m_kCachedPVGeometry.Add(m_pkFrameVisibleArray->GetAt(i));
	}
}
//-------------------------------------------------------------------------------------------------