﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net
#include "stdafx.h"

#include <NiStandardMaterialDescriptor.h>
#include <NiStandardMaterialNodeLibrary.h>
#include <NiStandardPixelProgramDescriptor.h>
#include <NiStandardVertexProgramDescriptor.h>
#include <NiRenderer.h>
#include <NiStandardMaterialNodeLibrary.h>
#include <NiMaterialFragmentNode.h>
#include <NiMaterialNodeLibrary.h>
#include <NiMaterialResource.h>
#include <NiCodeBlock.h>
#include "MRT_ColorDepthMaterial.h"
// version 1: first version
// version 2: packing depth into argb for PS3
#define NICOLORDEPTHMATERIAL_VERTEX_VERSION 2
#define NICOLORDEPTHMATERIAL_GEOMETRY_VERSION 2
#define NICOLORDEPTHMATERIAL_PIXEL_VERSION 2

NiImplementRTTI(MRT_ColorDepthMaterial, NiStandardMaterial);
//---------------------------------------------------------------------------
MRT_ColorDepthMaterial::MRT_ColorDepthMaterial(bool bAutoCreateCaches) :
NiStandardMaterial("ColorDepthMaterial", NULL, 
				   NICOLORDEPTHMATERIAL_VERTEX_VERSION, NICOLORDEPTHMATERIAL_GEOMETRY_VERSION,
				   NICOLORDEPTHMATERIAL_PIXEL_VERSION, bAutoCreateCaches)
{
	m_kLibraries.Add(
		NiStandardMaterialNodeLibrary::CreateMaterialNodeLibrary());
	m_kLibraries.Add(CreateCustomNodeLibrary());
}
//---------------------------------------------------------------------------
bool MRT_ColorDepthMaterial::HandleCalculateFog(Context& kContext, 
												NiMaterialResource* pkViewPos, Fog eFogType)
{
	if (!NiStandardMaterial::HandleCalculateFog(kContext, pkViewPos, eFogType))
	{
		return false;
	}

	// NOTE: the reason that we add the texture coordinate and do this
	// binding here is because we know that fog is the last step in the
	// vertex shader generating pipeline.  This way, we can guarantee
	// that this texture coordinate is appended (rather than inserted
	// arbitrarily) into the list of texture coordinates so that we can
	// match outputs and inputs between the vertex and pixel shaders.

	// add projected position as a texture coordinate 
	NiMaterialResource* pkVertOutViewTexCoord = 
		kContext.m_spOutputs->AddInputResource("float4", "TexCoord", "World", 
		"PosViewPassThrough");

	// Split vertex out projected position into the new texture coordinate
	// as well as output it from this fragment.  This is necessary because
	// (for now) you can't bind two named outputs to a single input.
	NiMaterialNode* pkSplitterNode = GetAttachableNodeFromLibrary(
		"PositionToDepthNormal");
	kContext.m_spConfigurator->AddNode(pkSplitterNode);
	kContext.m_spConfigurator->AddBinding(m_pkVertOutViewPos,
		pkSplitterNode->GetInputResourceByVariableName("Input"));
	kContext.m_spConfigurator->AddBinding(
		pkSplitterNode->GetOutputResourceByVariableName("Output"),
		pkVertOutViewTexCoord);

	return true;
}
//---------------------------------------------------------------------------
bool MRT_ColorDepthMaterial::HandleViewProjectionFragment(Context& kContext, 
														  bool bForceViewPos, NiMaterialResource* pkVertWorldPos,
														  NiMaterialResource*& pkVertOutProjectedPos,
														  NiMaterialResource*& pkVertOutViewPos)
{
	const bool bForceViewPosOverride = true;
	if (!NiStandardMaterial::HandleViewProjectionFragment(kContext,
		bForceViewPosOverride, pkVertWorldPos, pkVertOutProjectedPos,
		pkVertOutViewPos))
	{
		return false;
	}

	// Store the view position for use later in HandleCalculateFog
	NIASSERT(pkVertOutViewPos);
	m_pkVertOutViewPos = pkVertOutViewPos;
	return true;
}
//---------------------------------------------------------------------------
bool MRT_ColorDepthMaterial::HandlePostLightTextureApplication(
	Context& kContext,
	NiStandardPixelProgramDescriptor* pkPixelDesc,
	NiMaterialResource*& pkWorldNormal,
	NiMaterialResource* pkWorldView, 
	NiMaterialResource*& pkOpacityAccum,
	NiMaterialResource*& pkDiffuseTexAccum,
	NiMaterialResource*& pkSpecularTexAccum,
	NiMaterialResource* pkGlossiness,
	unsigned int& uiTexturesApplied,
	NiMaterialResource** apkUVSets,
	unsigned int uiNumStandardUVs,
	unsigned int uiNumTexEffectUVs)
{
	if (!NiStandardMaterial::HandlePostLightTextureApplication(
		kContext, pkPixelDesc, pkWorldNormal, pkWorldView, pkOpacityAccum,
		pkDiffuseTexAccum, pkSpecularTexAccum, pkGlossiness, uiTexturesApplied,
		apkUVSets, uiNumStandardUVs, uiNumTexEffectUVs))
	{
		return false;
	}

	// Create the output (i.e., input MRT data from interpolator / VP)
	NiMaterialNode * pkInputResource;
	pkInputResource = kContext.m_spConfigurator->GetNodeByName("VertexOut");

	NiMaterialResource* pkDepthFromVP = pkInputResource->AddOutputResource(
		"float4", "TexCoord", "World",  "WorldPosProjected");
	// Create an input (to the hardware, i.e., an output for us) for color1
	NiMaterialNode * pkOutputResource;
	pkOutputResource = kContext.m_spConfigurator->GetNodeByName("PixelOut");
	NiMaterialResource* pkPixelOutColor = pkOutputResource->
		AddInputResource("float4", "Color", "", "DepthOutInR");

#if defined(_PS3)
	// Pack the constant into the other render target
	NiMaterialNode* pkSplitterNode = GetAttachableNodeFromLibrary(
		"PackFloatToFixed4");
	kContext.m_spConfigurator->AddNode(pkSplitterNode);
	kContext.m_spConfigurator->AddBinding(pkDepthFromVP,
		pkSplitterNode->GetInputResourceByVariableName("Input"));
	kContext.m_spConfigurator->AddBinding(
		pkSplitterNode->GetOutputResourceByVariableName("Output"),
		pkPixelOutColor);
#else
	// Bind the constant output to the hardware input
	kContext.m_spConfigurator->AddBinding(pkDepthFromVP, pkPixelOutColor);
#endif

	return true;
}
//---------------------------------------------------------------------------
NiStandardMaterial::ReturnCode MRT_ColorDepthMaterial::GenerateShaderDescArray(
	NiMaterialDescriptor* pkMaterialDescriptor,
	RenderPassDescriptor* pkRenderPasses, unsigned int uiMaxCount,
	unsigned int& uiCountAdded)
{
	return NiStandardMaterial::GenerateShaderDescArray(pkMaterialDescriptor,
		pkRenderPasses, uiMaxCount, uiCountAdded);
}
//---------------------------------------------------------------------------
bool MRT_ColorDepthMaterial::GenerateDescriptor(const NiGeometry* pkGeometry, 
												const NiSkinInstance* pkSkin, const NiPropertyState* pkState, 
												const NiDynamicEffectState* pkEffects,
												NiMaterialDescriptor& kMaterialDesc)
{
	return NiStandardMaterial::GenerateDescriptor(pkGeometry, pkSkin,
		pkState, pkEffects, kMaterialDesc);
}
//---------------------------------------------------------------------------
NiMaterialNodeLibrary* MRT_ColorDepthMaterial::CreateCustomNodeLibrary()
{
	// Create a new NiMaterialNodeLibrary
	NiMaterialNodeLibrary* pkLib = NiNew NiMaterialNodeLibrary(2);

	// Create a new NiMaterialNode based on a fragment
	{
		NiMaterialFragmentNode* pkFrag = NiNew NiMaterialFragmentNode();

		// Set the fragment type
		pkFrag->SetType("Vertex/Pixel");

		// Set the fragment name
		pkFrag->SetName("PositionToDepthNormal");

		// Set the fragment description
		pkFrag->SetDescription("\n"
			"    This fragment takes in a position and outputs "
			"the z from the original position.\n");

		// Insert an input resource
		{
			NiMaterialResource* pkRes = NiNew NiMaterialResource();

			// Set resource type
			pkRes->SetType("float4");

			// Set resource variable name
			pkRes->SetVariable("Input");

			// Insert resource
			pkFrag->AddInputResource(pkRes);
		}

		// Insert an output resource
		{
			NiMaterialResource* pkRes = NiNew NiMaterialResource();

			// Set resource type
			pkRes->SetType("float4");

			// Set resource variable name
			pkRes->SetVariable("Output");

			// Insert resource
			pkFrag->AddOutputResource(pkRes);
		}

		// Insert a code block
		{
			NiCodeBlock* pkBlock = NiNew NiCodeBlock();

			// Set code block language
			pkBlock->SetLanguage("hlsl/Cg");

			// Set code block platform
			pkBlock->SetPlatform("D3D10/DX9/Xenon/PS3");

			// Set code block compile target
			pkBlock->SetTarget("vs_2_0/ps_2_0/vs_4_0/ps_4_0");

			// Set code block text
			pkBlock->SetText(
				"\n    Output = float4(Input.z, Input.z, Input.z, 1.0);\n");

			// Insert code block
			pkFrag->AddCodeBlock(pkBlock);
		}

		pkLib->AddNode(pkFrag);
	}

#if defined(_PS3)
	{
		NiMaterialFragmentNode* pkFrag = NiNew NiMaterialFragmentNode();

		// Set the fragment type
		pkFrag->SetType("Vertex/Pixel");

		// Set the fragment name
		pkFrag->SetName("PackFloatToFixed4");

		// Set the fragment description
		pkFrag->SetDescription("\n"
			"    This fragment packs a single float into a 4 byte RT\n");

		// Insert an input resource
		{
			NiMaterialResource* pkRes = NiNew NiMaterialResource();
			pkRes->SetType("float4");
			pkRes->SetVariable("Input");
			pkFrag->AddInputResource(pkRes);
		}

		// Insert an output resource
		{
			NiMaterialResource* pkRes = NiNew NiMaterialResource();
			pkRes->SetType("float4");
			pkRes->SetVariable("Output");
			pkFrag->AddOutputResource(pkRes);
		}

		// Insert a code block
		{
			NiCodeBlock* pkBlock = NiNew NiCodeBlock();

			// Set code block language
			pkBlock->SetLanguage("Cg");

			// Set code block platform
			pkBlock->SetPlatform("PS3");

			// Set code block compile target
			pkBlock->SetTarget("vs_2_0/ps_2_0/vs_4_0/ps_4_0");

			// Set code block text
			pkBlock->SetText(
				"\n  Output = unpack_4ubyte(Input.z);\n");

			// Insert code block
			pkFrag->AddCodeBlock(pkBlock);
		}

		pkLib->AddNode(pkFrag);
	}
#endif

	return pkLib;
}
//---------------------------------------------------------------------------


