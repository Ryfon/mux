﻿/** @file LightsManager.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-12-13
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 </pre>*/

#ifndef CODE_INGAME

#ifndef	LIGHTS_MANAGER_H	
#define	LIGHTS_MANAGER_H

#define TRANSLATION "Translation"
#define ROTATION "Rotation"
#define SCALE "Scale"
#define LIGHTTYPE "Light Type"
#define DIMMER "Dimmer"
#define COLOR_AMBIENT "Color (Ambient)"
#define COLOR_DIFFUSE "Color (Diffuse)"
#define COLOR_SPECULAR "Color (Specular)"
#define ATTENUATION_CONSTANT "Attenuation (Constant)"
#define ATTENUATION_LINEAR "Attenuation (Linear)"
#define ATTENUATION_QUADRATIC "Attenuation (Quadratic)"
#define SPOTANGLE_OUTER "Spot Angle (Outer)"
#define SPOTANGLE_INNER "Sport Angle (Inner)"
#define SPOT_EXPONENT "Spot Exponent"
#define DIRECTION "Direction"

#include <NiTSet.h>
#include <NiFixedString.h>
#include <NiEntityInterface.h>

class CTerrain;
class MAIN_ENTRY CLightsManager
{
public:

	/// 构造
	CLightsManager( CTerrain * );

	/// 析构
	virtual ~CLightsManager( void );	

protected:

	/// 地形
	CTerrain	*m_pTerrain;

	/// 灯光集合
	typedef map< NiEntityInterface*, NiLight* >	LightsMap;
	LightsMap	m_LightsMap;

	NiLight* m_pkSun;		// 唯一影响地形的方向光
	NiLight* m_pkFillLight;	// 补光

	std::set<NiGeometry*>	m_UnaffectCasterList;	// 不投射阴影物件列表
	std::set<NiGeometry*>	m_UnaffectReceiverList;	// 不接受阴影物件列表

public:

	NiLight*		GetLight(  NiEntityInterface* );
	void			UpdateLight( NiEntityInterface*, NiFixedString& );
	// 更新所有灯光，让列表中所有灯光都影响地形
	void			RefreshAllLights();
	// 更新 sun light
	void			RefreshSunLight();

	bool			AddLight( NiEntityInterface* );
	bool			RemoveLight( NiEntityInterface* );
	void			RemoveAllLights();
	bool			IsExited(NiEntityInterface*);

	// [11/17/2009 shiyazheng] 重新 cache 不投射物件/不接受物件列表 
	void BuildUnaffectCasterList(NiDirectionalLight* pkSun);
	void BuildUnaffectReceiverList(NiDirectionalLight* pkSun);

	// 是否在不投射阴影列表中
	bool IsUnaffectCaster(NiGeometry* pkGeom)
	{
		bool bInList = 
			(m_UnaffectCasterList.find(pkGeom) != m_UnaffectCasterList.end());
		return bInList;
	}

	// 是否在不接受阴影列表中
	bool IsUnaffectReceiver(NiGeometry* pkGeom)
	{
		bool bInList = 
			(m_UnaffectReceiverList.find(pkGeom) != m_UnaffectReceiverList.end());
		return bInList;
	}
};

#endif
#endif