﻿/** @file TerrainGridBaseRegion.h 
@brief 地形基于Grid的区域
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-07-28
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#ifndef CODE_INGAME

#include "Terrain.h"
#include <set>

namespace SceneCore
{
	class MAIN_ENTRY CTerrainGridBaseRegion
	{
	public:
		CTerrainGridBaseRegion(const char* pszName = NULL);

		~CTerrainGridBaseRegion(void);

		// 添加一个 Grid. 如果 Grid 数量超界，返回 false
		void AddGridID(int iID);

		// 移除一个 Grid
		void RemoveGridID(int iID);

		// 清除所有 Grid
		void Clear();

		// 设置/获取 名称
		void SetName(const char* pszName);
		const char* GetName();
		
		// 获取当前区域所包含的所有 Grid ID
		std::set<int>& GetGridSet();

		// 获取/设置 区域是否发生过改变。如果改变了需要重新计算 Geometry
		bool IsChanged();
		void SetChanged(bool bChanged);

	private:
		char				m_szName[256];	// 区域名称
		std::set<int>		m_GridSet;		// 当前区域包含的 Grid ID
		bool				m_bChanged;		// 区域是否发生改变
	};

// 内联函数实现
#include "TerrainGridBaseRegion.inl"

};	// End of namespace

#endif
