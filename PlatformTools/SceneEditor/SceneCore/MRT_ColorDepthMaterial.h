﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef MRT_COLORDEPTHMATERIAL
#define MRT_COLORDEPTHMATERIAL
#pragma once

#include "NiStandardMaterial.h"

// This material is identical to NiStandardMaterial except that it writes
// the camera-space depth value of the scene out to the r component of the
// secondary render target.

class MRT_ColorDepthMaterial : public NiStandardMaterial
{
public:
	NiDeclareRTTI;
	MRT_ColorDepthMaterial(bool bAutoCreateCaches = true);
protected:
	// overridden virtuals
	virtual bool GenerateDescriptor(const NiGeometry* pkGeometry, 
		const NiSkinInstance* pkSkin, const NiPropertyState* pkState, 
		const NiDynamicEffectState* pkEffects,
		NiMaterialDescriptor& kMaterialDesc);

	virtual bool HandleCalculateFog(Context& kContext,
		NiMaterialResource* pkViewPos, Fog eFogType);

	virtual bool HandleViewProjectionFragment(Context& kContext, 
		bool bForceViewPos, NiMaterialResource* pkVertWorldPos,
		NiMaterialResource*& pkVertOutProjectedPos,
		NiMaterialResource*& pkVertOutViewPos);

	virtual bool HandlePostLightTextureApplication(Context& kContext,
		NiStandardPixelProgramDescriptor* pkPixelDesc,
		NiMaterialResource*& pkWorldNormal,
		NiMaterialResource* pkWorldView, 
		NiMaterialResource*& pkOpacityAccum,
		NiMaterialResource*& pkDiffuseTexAccum,
		NiMaterialResource*& pkSpecularTexAccum,
		NiMaterialResource* pkGlossiness,
		unsigned int& uiTexturesApplied,
		NiMaterialResource** apkUVSets,
		unsigned int uiNumStandardUVs,
		unsigned int uiNumTexEffectUVs);

	virtual ReturnCode GenerateShaderDescArray(
		NiMaterialDescriptor* pkMaterialDescriptor,
		RenderPassDescriptor* pkRenderPasses, unsigned int uiMaxCount,
		unsigned int& uiCountAdded);

	NiMaterialNodeLibrary* CreateCustomNodeLibrary();

	// Temporary used to grab the view position during vertex
	// shader construction
	NiMaterialResource* m_pkVertOutViewPos;
};

NiSmartPointer(MRT_ColorDepthMaterial);

#endif
