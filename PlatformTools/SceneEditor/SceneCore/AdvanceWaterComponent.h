﻿/** @file AdvanceWaterComponent.h 
@brief Advance Water component 现在使用 max 给面片覆 AdvanceWater Shader, 本类作废，只作参考
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-09-22
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#include <NiRefObject.h>
#include <NiSmartPointer.h>
#include <NiEntityComponentInterface.h>
#include <NiFlags.h>
#include <NiNode.h>
//#include <NiGeometry.h>
#include <NiTriShape.h>
#include <NiFactories.h>
#include <NiEntityInterface.h>
#include <NiEntityErrorInterface.h>
#include "AdvanceWater.h"

#ifndef CODE_INGAME

namespace SceneCore
{

NiSmartPointer(CAdvanceWaterComponent);

class MAIN_ENTRY CAdvanceWaterComponent : public NiRefObject,
	public NiEntityComponentInterface
{
public:
	CAdvanceWaterComponent(NiAVObjectPtr pkSceneRoot = NULL);
	~CAdvanceWaterComponent(void);

	static NiFixedString ClassName();

	// *** begin Emergent internal use only ***
	static void _SDMInit();
	static void _SDMShutdown();
	// *** end Emergent internal use only ***

	/// 获取 AdvanceWater 实例
	CAdvanceWaterComponent GetAdvanceWaterInstance();

	// 设置/获取波长
	void SetWaveLength(float afWaveLength[4]);
	const float* GetWaveLength();

	// 设置/获取振幅
	void SeAmplitude(float afAmplitude[4]);
	const float* GetAmplitude();

	// 设置/获取波传播速度
	void SetSpeed(float afSpeed[4]);
	const float* GetSpeed();

	// 设置/获取方向
	void SetWaveDirection(NiPoint2 avWaveDirection[4]);
	const NiPoint2* GetWaveDirection();

	// 设置/获取 Fresnel
	void SetFresnel(float fFresnel);
	const float GetFresnel();

	// 设置/获取 Alpha
	void SetAlpha(float fAlpha);
	const float GetAlpha();

	// 设置/获取反射色
	void SetReflectColor(NiColor kColor);
	const NiColor GetReflectColor();

	// 设置/获取高光色
	void SetSpecularColor(NiColor kColor);
	const NiColor GetSpecularColor();

	// 设置/获取光方向
	void SetLightDir(NiPoint3 kDir);
	const NiPoint3 GetLightDir();

	// 设置/获取环境纹理
	void SetEnvMap(NiTexturePtr spEnvMap);
	const NiTexturePtr GetEnvMap();

	// 设置/获取折射纹理
	void SetRefractMap(NiTexturePtr spRefractMap);
	const NiTexturePtr GetRefractMap();

	// 设置世界变换
	void SetTranslate(const NiPoint3& kTranslation);
	const NiPoint3& GetTranslate() const;

	void SetRotate(const NiMatrix3& kRotation);
	const NiMatrix3& GetRotate() const;

	void SetScale(const float fScale);
	float GetScale() const;

	// 设置水表面几何体
	void SetSceneRoot(NiAVObjectPtr pkSceneRoot);
	NiAVObjectPtr GetSceneRoot();

	/// 判断一个 entity 是否 Advance Water
	static bool EntityIsAdvanceWater(NiEntityInterface* pkEntity);

protected:

	// 报错字符串
	static NiFixedString ERR_TRANSLATION_NOT_FOUND;
	static NiFixedString ERR_ROTATION_NOT_FOUND;
	static NiFixedString ERR_SCALE_NOT_FOUND;
	static NiFixedString ERR_FILE_LOAD_FAILED;

	// Class name.
	static NiFixedString ms_kClassName;

	// Component name.
	static NiFixedString ms_kComponentName;

	// Property names.
	static NiFixedString ms_kWaveLengthName;
	static NiFixedString ms_kAmplitudeName;
	static NiFixedString ms_kSpeedName;
	static NiFixedString ms_kDirectionName;
	static NiFixedString ms_kFresnelName;
	static NiFixedString ms_kAlphaName;
	static NiFixedString ms_kReflectColorName;
	static NiFixedString ms_kSpecularColorName;
	static NiFixedString ms_kLightDirName;
	static NiFixedString ms_kEnvMapName;
	static NiFixedString ms_kRefractMapName;
	static NiFixedString ms_kNifFilePathName;
	static NiFixedString ms_kSceneRootPointerName;

	// Property descriptions.
	static NiFixedString ms_kWaveLengthDescription;
	static NiFixedString ms_kAmplitudeDescription;
	static NiFixedString ms_kSpeedDescription;
	static NiFixedString ms_kDirectionDescription;
	static NiFixedString ms_kFresnelDescription;
	static NiFixedString ms_kAlphaDescription;
	static NiFixedString ms_kReflectColorDescription;
	static NiFixedString ms_kSpecularColorDescription;
	static NiFixedString ms_kLightDirDescription;
	static NiFixedString ms_kEnvMapDescription;
	static NiFixedString ms_kRefractMapDescription;
	static NiFixedString ms_kNifFilePathDescription;
	static NiFixedString ms_kSceneRootPointerDescription;

	// Dependent property names.
	static NiFixedString ms_kTranslationName;
	static NiFixedString ms_kRotationName;
	static NiFixedString ms_kScaleName;

	// properties

	CAdvanceWaterPtr			m_spWaterInstance;		// Advance Water 实例
	CAdvanceWaterComponentPtr	m_spMasterComponent;
	NiFixedString				m_spNifFilePath;		// nif 文件路径
	NiString					m_spEnvMapFile;			// 环境纹理名称				
	bool						m_bNifFileChanged;		// nif 文件路径是否改变，需要重新加载 nif

public:

	// NiEntityComponentInterface overrides.
	virtual NiEntityComponentInterface* Clone(bool bInheritProperties);
	virtual NiEntityComponentInterface* GetMasterComponent();
	virtual void SetMasterComponent(
		NiEntityComponentInterface* pkMasterComponent);
	virtual void GetDependentPropertyNames(
		NiTObjectSet<NiFixedString>& kDependentPropertyNames);

	// NiEntityPropertyInterface overrides.
	virtual NiBool SetTemplateID(const NiUniqueID& kID);
	virtual NiUniqueID GetTemplateID();
	virtual void AddReference();
	virtual void RemoveReference();
	virtual NiFixedString GetClassName() const;
	virtual NiFixedString GetName() const;
	virtual NiBool SetName(const NiFixedString& kName);
	virtual NiBool IsAnimated() const;
	virtual void Update(NiEntityPropertyInterface* pkParentEntity,
		float fTime, NiEntityErrorInterface* pkErrors,
		NiExternalAssetManager* pkAssetManager);
	virtual void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext,
		NiEntityErrorInterface* pkErrors);
	virtual void GetPropertyNames(
		NiTObjectSet<NiFixedString>& kPropertyNames) const;
	virtual NiBool CanResetProperty(const NiFixedString& kPropertyName,
		bool& bCanReset) const;
	virtual NiBool ResetProperty(const NiFixedString& kPropertyName);
	virtual NiBool MakePropertyUnique(const NiFixedString& kPropertyName,
		bool& bMadeUnique);
	virtual NiBool GetDisplayName(const NiFixedString& kPropertyName,
		NiFixedString& kDisplayName) const;
	virtual NiBool SetDisplayName(const NiFixedString& kPropertyName,
		const NiFixedString& kDisplayName);
	virtual NiBool GetPrimitiveType(const NiFixedString& kPropertyName,
		NiFixedString& kPrimitiveType) const;	// property 是什么类型的
	virtual NiBool SetPrimitiveType(const NiFixedString& kPropertyName,
		const NiFixedString& kPrimitiveType);
	virtual NiBool GetSemanticType(const NiFixedString& kPropertyName,
		NiFixedString& kSemanticType) const;
	virtual NiBool SetSemanticType(const NiFixedString& kPropertyName,
		const NiFixedString& kSemanticType);
	virtual NiBool GetDescription(const NiFixedString& kPropertyName,
		NiFixedString& kDescription) const;
	virtual NiBool SetDescription(const NiFixedString& kPropertyName,
		const NiFixedString& kDescription);
	virtual NiBool GetCategory(const NiFixedString& kPropertyName,
		NiFixedString& kCategory) const;
	virtual NiBool IsPropertyReadOnly(const NiFixedString& kPropertyName,
		bool& bIsReadOnly);
	virtual NiBool IsPropertyUnique(const NiFixedString& kPropertyName,
		bool& bIsUnique);
	virtual NiBool IsPropertySerializable(const NiFixedString& kPropertyName,
		bool& bIsSerializable);
	virtual NiBool IsPropertyInheritable(const NiFixedString& kPropertyName,
		bool& bIsInheritable);
	virtual NiBool IsExternalAssetPath(const NiFixedString& kPropertyName,
		unsigned int uiIndex, bool& bIsExternalAssetPath) const;
	virtual NiBool GetElementCount(const NiFixedString& kPropertyName,
		unsigned int& uiCount) const;
	virtual NiBool SetElementCount(const NiFixedString& kPropertyName,
		unsigned int uiCount, bool& bCountSet);
	virtual NiBool IsCollection(const NiFixedString& kPropertyName,
		bool& bIsCollection) const;
	// 设置/获取 
	//virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiQuaternion& kData, unsigned int uiIndex = 0);
	//virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiQuaternion& kData, unsigned int uiIndex = 0) const;
	// 设置/获取颜色
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiColor& kData, unsigned int uiIndex = 0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiColor& kData, unsigned int uiIndex = 0) const;
	// 设置/获取 alpha 波长，振幅，速度
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex = 0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex = 0) const;
	// 设置/获取 Wave Direction
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiPoint2& kData, unsigned int uiIndex = 0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiPoint2& kData, unsigned int uiIndex = 0) const;

	// 设置/获取 Light Direction
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiPoint3& kData, unsigned int uiIndex = 0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiPoint3& kData, unsigned int uiIndex = 0) const;

	// 设置/获取 nif 文件路径
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName,const NiFixedString& kData, unsigned int uiIndex=0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName,NiFixedString& kData, unsigned int uiIndex=0) const;

	// 设置/获取 场景根
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiObject*& pkData, unsigned int uiIndex=0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiObject*& pkData, unsigned int uiIndex=0) const;

};	// end of class
};	// end of namespace

#endif