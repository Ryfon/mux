﻿/** @file EditableBaseObj.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-09-28
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 </pre>*/


#ifndef CODE_INGAME

#ifndef EDITABLE_BASEOBJ_H
#define EDITABLE_BASEOBJ_H

#define TRANSLATION "Translation"
#define ROTATION "Rotation"
#define SCALE "Scale"
#define CREATORNAME "CreatorName"
#define RADIUS "Radius"
#define REFRESHNUM "RefreshNum"
#define TIMEINTERVAL "TimeInterval"
#define REFRESHTYPE "RefreshType"
#define CREATETYPE "CreateType"
#define NPCNO "NPCNO"
#define NPCCREATORNO "NPCCreatorNO"

#define RADIUSX "RadiusX"
#define RADIUSY "RadiusY"
#define TIMEFORMAT "TimeFormat"
#define STARTTIME "StartTime"
#define ENDTIME "EndTime"
#define NPC1	"NPCNO1"
#define NPCNUM1 "NPCNum1"
#define NPC2	"NPCNO2"
#define NPCNUM2 "NPCNum2"
#define NPC3	"NPCNO3"
#define NPCNUM3 "NPCNum3"
#define NPC4	"NPCNO4"
#define NPCNUM4 "NPCNum4"
#define NPC5	"NPCNO5"
#define NPCNUM5 "NPCNum5"

#include <NiStringExtraData.h>
#include "NPCCreator.h"


/**
 @brief 可编辑对象实体类
 *
 *	负责可编辑对象的方位修改, 同类对象的复制etc
 */
class MAIN_ENTRY CEditableBaseObj
{
public:
	enum OBEJCT_TYPE{OT_NPCCREATOR, OT_COMMON};
	/// 构造
	CEditableBaseObj();

	/// 析构
	virtual ~CEditableBaseObj(void);

	/// 获取名字
	const string					GetExtraData() const 
	{	return static_cast< string >( m_pExtraData->GetValue() );	}

	/// 获取对象
	NiNode							*GetNode() { return m_spNode; }

	/// 从文件中创建
	virtual bool					Create( const char *pszNifFile );

	/// 销毁
	virtual void					Destroy();

	/// 克隆,数据共享
	virtual	CEditableBaseObj*		Clone();

	/// 深度拷贝,数据不共享
	virtual	CEditableBaseObj*		DeepClone();

	/// 平移
	virtual	void					SetTranslate( float fX, float fY, float fZ );
	virtual	void					SetTranslate( NiPoint3& vPos);

	/// 旋转
	virtual	void					SetRotation( float fX, float fY, float fZ );
	virtual	void					SetRotation( NiMatrix3& );

	/// 放缩
	virtual	void					SetScale(  float fX, float fY, float fZ );
	virtual	void					SetScale(  NiPoint3& );

	/// 获取位置
	const NiPoint3&					GetPosition() const	{ return m_vPosition; }

	/// 获取旋转方位
	const NiMatrix3&				GetRotation() const { return m_vRotation; }

	/// 获取放缩
	const NiPoint3&					GetScale() const{ return m_vScale; }

	//设置对象类型
	OBEJCT_TYPE GetObjectType() const
	{
		return m_ObjectType;
	}
	//获得对象类型
	void SetObjectType(OBEJCT_TYPE objType)
	{
		m_ObjectType = objType;
	}
	
protected:
	/// Extra Data, 用来标识同材质的物件
	NiStringExtraData				*m_pExtraData;
	
	/// 物件实体对象
	NiNodePtr						m_spNode;

	/// 位置
	NiPoint3						m_vPosition;
	
	/// 旋转
	NiMatrix3						m_vRotation;

	/// 放缩
	NiPoint3						m_vScale;

	/// NPC生成器标识
	OBEJCT_TYPE							m_ObjectType;
};

/**
@brief 物件类比较符,这里以ExtraData作比较
*/
class MAIN_ENTRY CEditableObjectNameComp
{
public:

	/// 重载()
	bool operator()( const CEditableBaseObj	*pObj1, const CEditableBaseObj *pObj2 )
	{
		return pObj1->GetExtraData() < pObj2->GetExtraData();
	}
};

/// 物件set定义
typedef set< CEditableBaseObj*, CEditableObjectNameComp >	EditableObjectSet;

//NPC物件定义
class CNpcCreatorObj: public CNPCCreator, public CEditableBaseObj
{
public:
	CNpcCreatorObj();
	virtual ~CNpcCreatorObj();
/*
	/// 平移
	virtual	void					SetTranslate( float fX, float fY, float fZ );
	virtual	void					SetTranslate( NiPoint3& vPos);

	/// 旋转
	virtual	void					SetRotation( float fX, float fY, float fZ );
	virtual	void					SetRotation( NiMatrix3& );

	/// 放缩
	virtual	void					SetScale(  float fX, float fY, float fZ );
	virtual	void					SetScale(  NiPoint3& );
*/

protected:

private:

};

#endif
#endif