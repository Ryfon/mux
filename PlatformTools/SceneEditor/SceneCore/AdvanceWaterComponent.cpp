﻿#include "StdAfx.h"
#include "AdvanceWaterComponent.h"
#include "Utility.h"

#ifndef CODE_INGAME

using namespace SceneCore;

NiFixedString CAdvanceWaterComponent::ERR_TRANSLATION_NOT_FOUND;
NiFixedString CAdvanceWaterComponent::ERR_ROTATION_NOT_FOUND;
NiFixedString CAdvanceWaterComponent::ERR_SCALE_NOT_FOUND;
NiFixedString CAdvanceWaterComponent::ERR_FILE_LOAD_FAILED;

NiFixedString CAdvanceWaterComponent::ms_kClassName;

// Component name.
NiFixedString CAdvanceWaterComponent::ms_kComponentName;

// Property names.
NiFixedString CAdvanceWaterComponent::ms_kWaveLengthName;
NiFixedString CAdvanceWaterComponent::ms_kAmplitudeName;
NiFixedString CAdvanceWaterComponent::ms_kSpeedName;
NiFixedString CAdvanceWaterComponent::ms_kDirectionName;
NiFixedString CAdvanceWaterComponent::ms_kFresnelName;
NiFixedString CAdvanceWaterComponent::ms_kAlphaName;
NiFixedString CAdvanceWaterComponent::ms_kReflectColorName;
NiFixedString CAdvanceWaterComponent::ms_kSpecularColorName;
NiFixedString CAdvanceWaterComponent::ms_kLightDirName;
NiFixedString CAdvanceWaterComponent::ms_kEnvMapName;
NiFixedString CAdvanceWaterComponent::ms_kRefractMapName;
NiFixedString CAdvanceWaterComponent::ms_kNifFilePathName;
NiFixedString CAdvanceWaterComponent::ms_kSceneRootPointerName;

// Property descriptions.
NiFixedString CAdvanceWaterComponent::ms_kWaveLengthDescription;
NiFixedString CAdvanceWaterComponent::ms_kAmplitudeDescription;
NiFixedString CAdvanceWaterComponent::ms_kSpeedDescription;
NiFixedString CAdvanceWaterComponent::ms_kDirectionDescription;
NiFixedString CAdvanceWaterComponent::ms_kFresnelDescription;
NiFixedString CAdvanceWaterComponent::ms_kAlphaDescription;
NiFixedString CAdvanceWaterComponent::ms_kReflectColorDescription;
NiFixedString CAdvanceWaterComponent::ms_kSpecularColorDescription;
NiFixedString CAdvanceWaterComponent::ms_kLightDirDescription;
NiFixedString CAdvanceWaterComponent::ms_kEnvMapDescription;
NiFixedString CAdvanceWaterComponent::ms_kRefractMapDescription;
NiFixedString CAdvanceWaterComponent::ms_kNifFilePathDescription;
NiFixedString CAdvanceWaterComponent::ms_kSceneRootPointerDescription;

// Dependent property names.
NiFixedString CAdvanceWaterComponent::ms_kTranslationName;
NiFixedString CAdvanceWaterComponent::ms_kRotationName;
NiFixedString CAdvanceWaterComponent::ms_kScaleName;


// Declare classes
NiFactoryDeclarePropIntf(CAdvanceWaterComponent);

void CAdvanceWaterComponent::_SDMInit()
{
	ERR_TRANSLATION_NOT_FOUND = "Translation property not found.";
	ERR_ROTATION_NOT_FOUND = "Rotation property not found.";
	ERR_SCALE_NOT_FOUND = "Scale property not found.";
	ERR_FILE_LOAD_FAILED = "SPT file load failed.";

	CAdvanceWaterComponent::ms_kClassName = "CAdvanceWaterComponent";

	// Component name.
	CAdvanceWaterComponent::ms_kComponentName = "Advance Water Component";

	// Property names.
	ms_kWaveLengthName = "Wave Length";
	ms_kAmplitudeName = "Wave Amplitude";
	ms_kSpeedName = "Wave Speed";
	ms_kDirectionName = "Wave Direction";
	ms_kFresnelName = "Adv Water Fresnel";
	ms_kAlphaName = "Adv Water Alpha";
	ms_kReflectColorName = "Adv Water Reflect Color";
	ms_kSpecularColorName = "Adv Water Specular Color";
	ms_kLightDirName = "Adv Water Light Direction";
	ms_kEnvMapName = "Adv Water Environment Map";
	ms_kRefractMapName = "Adv Water Refract Map";
	ms_kNifFilePathName = "NIF File Path";
	ms_kSceneRootPointerName = "Scene Root Pointer";

	// Property descriptions.
	ms_kWaveLengthDescription = "波长";
	ms_kAmplitudeDescription = "振幅";
	ms_kSpeedDescription = "传播速度";
	ms_kDirectionDescription = "传播方向";
	ms_kFresnelDescription = "菲涅耳系数";
	ms_kAlphaDescription = "透明度";
	ms_kReflectColorDescription = "反射色";
	ms_kSpecularColorDescription = "高光色";
	ms_kLightDirDescription = "光照方向";
	ms_kEnvMapDescription = "环境纹理 Cube map";
	ms_kRefractMapDescription = "折射纹理";
	ms_kNifFilePathDescription = "水表面模型对应 nif 文件";
	ms_kSceneRootPointerDescription = "水表面几何体结点";

	// Dependent property names.
	CAdvanceWaterComponent::ms_kTranslationName = "Translation";
	CAdvanceWaterComponent::ms_kRotationName = "Rotation";
	CAdvanceWaterComponent::ms_kScaleName = "Scale";

	NiFactoryRegisterPropIntf(CAdvanceWaterComponent);	// registe creation function
}
//-------------------------------------------------------------------------------------------------
void CAdvanceWaterComponent::_SDMShutdown()
{
	ERR_TRANSLATION_NOT_FOUND = NULL;
	ERR_ROTATION_NOT_FOUND = NULL;
	ERR_SCALE_NOT_FOUND = NULL;
	ERR_FILE_LOAD_FAILED = NULL; 

	CAdvanceWaterComponent::ms_kClassName = NULL;

	// Component name.
	CAdvanceWaterComponent::ms_kComponentName = NULL;

	// Property names.
	ms_kWaveLengthName = NULL;
	ms_kAmplitudeName = NULL;
	ms_kSpeedName = NULL;
	ms_kDirectionName = NULL;
	ms_kFresnelName = NULL;
	ms_kAlphaName = NULL;
	ms_kReflectColorName = NULL;
	ms_kSpecularColorName = NULL;
	ms_kLightDirName = NULL;
	ms_kEnvMapName = NULL;
	ms_kRefractMapName = NULL;
	ms_kNifFilePathName = NULL;
	ms_kSceneRootPointerName = NULL;

	// Property descriptions.
	ms_kWaveLengthDescription = NULL;
	ms_kAmplitudeDescription = NULL;
	ms_kSpeedDescription = NULL;
	ms_kDirectionDescription = NULL;
	ms_kFresnelDescription = NULL;
	ms_kAlphaDescription = NULL;
	ms_kReflectColorDescription = NULL;
	ms_kSpecularColorDescription = NULL;
	ms_kLightDirDescription = NULL;
	ms_kEnvMapDescription = NULL;
	ms_kRefractMapDescription = NULL;
	ms_kNifFilePathDescription = NULL;
	ms_kSceneRootPointerDescription = NULL;

	// Dependent property names.
	CAdvanceWaterComponent::ms_kTranslationName = NULL;
	CAdvanceWaterComponent::ms_kRotationName = NULL;
	CAdvanceWaterComponent::ms_kScaleName = NULL;

}
//-------------------------------------------------------------------------------------------------

CAdvanceWaterComponent::CAdvanceWaterComponent(NiAVObjectPtr pkSceneRoot)
:	m_spMasterComponent(NULL),
	m_spNifFilePath(""),
	m_bNifFileChanged(false),
	m_spWaterInstance(NULL)
{
	//if (pkSceneRoot != NULL)
	{
		// 填充初始化参数表
		stAdvWaterInitParam defParam;

		// 设置4个叠加波的参数
		defParam.afAmplitude[0] = 0.012f;
		defParam.afAmplitude[1] = 0.006f;
		defParam.afAmplitude[2] = 0.005f;
		defParam.afAmplitude[3] = 0.01f;
		defParam.afWaveLength[0] = 1.2f;
		defParam.afWaveLength[1] = 0.6f;
		defParam.afWaveLength[2] = 0.5f;
		defParam.afWaveLength[3] = 1.0f;
		defParam.afSpeed[0] = 0.3f;
		defParam.afSpeed[1] = 0.6f;
		defParam.afSpeed[2] = 0.8f;
		defParam.afSpeed[3] = 0.26f;
		defParam.avDirection[0] = NiPoint2(1.0f, 0.2f);
		defParam.avDirection[1] = NiPoint2(1.0f, -0.1f);
		defParam.avDirection[2] = NiPoint2(1.0f, 0.12f);
		defParam.avDirection[3] = NiPoint2(1.0f, -0.25f);

		// 水体参数
		//defParam.fFresnel = 0.02037f;
		defParam.fAlpha = 1.0f;
		defParam.kReflectColor = NiColor(0.7f, 0.7f, 0.8f);	// 反射色偏蓝
		defParam.kSpecularColor = NiColor(1.0f, 1.0f, 1.0f);//

		// 环境参数
		defParam.kLightDir = NiPoint3(0.0f, 1.0f, 1.0f);
		defParam.spEnvMap = NULL;
//		defParam.spRefractMap = NULL;

		m_spWaterInstance = NiNew CAdvanceWater();		// Advance Water 实例
		m_spWaterInstance->Init(pkSceneRoot, defParam);
	}

}

CAdvanceWaterComponent::~CAdvanceWaterComponent(void)
{
	m_spWaterInstance = NULL;
	m_spMasterComponent = NULL;
	m_spNifFilePath = NULL;
}
//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CAdvanceWaterComponent::Clone(bool bInheritProperties)
{
	CAdvanceWaterComponent* pkLayerFogComp = NiNew CAdvanceWaterComponent;

	pkLayerFogComp->SetPropertyData(ms_kNifFilePathName, m_spNifFilePath);

	return pkLayerFogComp;
}
//-------------------------------------------------------------------------------------------------

NiEntityComponentInterface* CAdvanceWaterComponent::GetMasterComponent()
{
	return m_spMasterComponent;
}
//-------------------------------------------------------------------------------------------------

void CAdvanceWaterComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
	m_spMasterComponent = (CAdvanceWaterComponent*)pkMasterComponent;
}
//-------------------------------------------------------------------------------------------------

void CAdvanceWaterComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
	kDependentPropertyNames.Add(ms_kTranslationName);
	kDependentPropertyNames.Add(ms_kRotationName);
	kDependentPropertyNames.Add(ms_kScaleName);
	//kDependentPropertyNames.Add(ms_kNifFilePathName);
	//kDependentPropertyNames.Add(ms_kSceneRootPointerName);
}
//-------------------------------------------------------------------------------------------------

// NiEntityPropertyInterface overrides.
NiBool CAdvanceWaterComponent::SetTemplateID(const NiUniqueID& kID)
{
	// 不支持
	return false;
}
//-------------------------------------------------------------------------------------------------

NiUniqueID CAdvanceWaterComponent::GetTemplateID()
{
	static const NiUniqueID kUniqueID = 
		NiUniqueID(0xC2, 0x39, 0x02, 0x43, 0xEE, 0x89, 0x48, 0xab, 0xA8, 0x2E, 0xA8, 0xE9, 0x0E, 0xA9, 0xCF, 0xFC);
	return kUniqueID;
}
//-------------------------------------------------------------------------------------------------

void CAdvanceWaterComponent::AddReference()
{
	this->IncRefCount();
}
//-------------------------------------------------------------------------------------------------

void CAdvanceWaterComponent::RemoveReference()
{
	this->DecRefCount();
}
//-------------------------------------------------------------------------------------------------

NiFixedString CAdvanceWaterComponent::GetClassName() const
{
	return ms_kClassName;
}
//-------------------------------------------------------------------------------------------------
NiFixedString CAdvanceWaterComponent::ClassName()
{
	return ms_kClassName;
}
//-------------------------------------------------------------------------------------------------
NiFixedString CAdvanceWaterComponent::GetName() const
{
	return ms_kComponentName;
}
//-------------------------------------------------------------------------------------------------

NiBool CAdvanceWaterComponent::SetName(const NiFixedString& kName)
{
	// 不支持
	return false;
}
//-------------------------------------------------------------------------------------------------

NiBool CAdvanceWaterComponent::IsAnimated() const
{
	return true;
}
//-------------------------------------------------------------------------------------------------
bool CAdvanceWaterComponent::EntityIsAdvanceWater(NiEntityInterface* pkEntity)
{
	static const NiUniqueID kUniqueID = 
		NiUniqueID(0xC2, 0x39, 0x02, 0x43, 0xEE, 0x89, 0x48, 0xab, 0xA8, 0x2E, 0xA8, 0xE9, 0x0E, 0xA9, 0xCF, 0xFC);

	if (NULL != pkEntity->GetComponentByTemplateID(kUniqueID))
	{
		return true;
	}
	else
	{
		return false;
	}
}
//-------------------------------------------------------------------------------------------------
void CAdvanceWaterComponent::Update(NiEntityPropertyInterface* pkParentEntity,
								float fTime, NiEntityErrorInterface* pkErrors,
								NiExternalAssetManager* pkAssetManager)
{
	if (!m_spWaterInstance)
	{
		return;
	}

	// 基本上用的 NiSceneGraphComponent 的 update
	if (m_bNifFileChanged)
	{
		m_bNifFileChanged = false;
		// If the scene root does not exist, create it using the property
		// data.
		if (m_spNifFilePath.Exists() && pkAssetManager &&
			pkAssetManager->GetAssetFactory())
		{
			NIASSERT(pkAssetManager);
			NiParamsNIF kNIFParams;
			if (!kNIFParams.SetAssetPath(m_spNifFilePath) ||
				!pkAssetManager->RegisterAndResolve(&kNIFParams))
			{
				// Failing to resolve amounts to failed to load file.
				pkErrors->ReportError(ERR_FILE_LOAD_FAILED,
					m_spNifFilePath, pkParentEntity->GetName(),
					ms_kNifFilePathName);
				return;
			}

			NiBool bSuccess = pkAssetManager->Retrieve(&kNIFParams);
			NIASSERT(bSuccess);

			NiAVObject* pkAVObject; 
			bSuccess = kNIFParams.GetSceneRoot(pkAVObject);

			if (bSuccess && pkAVObject != NULL)
			{
				m_spWaterInstance->SetSceneRoot(pkAVObject);

				// Perform initial update.
				pkAVObject->Update(0.0f);
				pkAVObject->UpdateNodeBound();
				pkAVObject->UpdateProperties();
				pkAVObject->UpdateEffects();
			}
			else
			{
				NIASSERT(!"Failed to Retrieve file!");
				// Failing to retrieve amounts to failed to load file.
				pkErrors->ReportError(ERR_FILE_LOAD_FAILED,
					m_spNifFilePath, pkParentEntity->GetName(),
					ms_kNifFilePathName);
				return;
			}
		}
	}

	if (m_spWaterInstance != NULL)
	{
		NiBool bDependentPropertiesFound = true;

		// Find dependent properties.
		NiPoint3 kTranslation;
		if (!pkParentEntity->GetPropertyData(ms_kTranslationName,
			kTranslation))
		{
			bDependentPropertiesFound = false;
			pkErrors->ReportError(ERR_TRANSLATION_NOT_FOUND, NULL,
				pkParentEntity->GetName(), ms_kTranslationName);
		}
		NiMatrix3 kRotation;
		if (!pkParentEntity->GetPropertyData(ms_kRotationName, kRotation))
		{
			bDependentPropertiesFound = false;
			pkErrors->ReportError(ERR_ROTATION_NOT_FOUND, NULL,
				pkParentEntity->GetName(), ms_kRotationName);
		}
		float fScale;
		if (!pkParentEntity->GetPropertyData(ms_kScaleName, fScale))
		{
			bDependentPropertiesFound = false;
			pkErrors->ReportError(ERR_SCALE_NOT_FOUND, NULL,
				pkParentEntity->GetName(), ms_kScaleName);
		}

		// Use dependent properties to update transform of scene root.
		bool bUpdatedTransforms = false;
		if (bDependentPropertiesFound)
		{
			if (m_spWaterInstance->GetTranslate() != kTranslation)
			{
				m_spWaterInstance->SetTranslate(kTranslation);
				bUpdatedTransforms = true;
			}
			if (m_spWaterInstance->GetRotate() != kRotation)
			{
				m_spWaterInstance->SetRotate(kRotation);
				bUpdatedTransforms = true;
			}
			if (m_spWaterInstance->GetScale() != fScale)
			{
				m_spWaterInstance->SetScale(fScale);
				bUpdatedTransforms = true;
			}
		}

		m_spWaterInstance->Update(fTime);
	}

}
//-------------------------------------------------------------------------------------------------

void CAdvanceWaterComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext,
										 NiEntityErrorInterface* pkErrors)
{
	if (m_spWaterInstance != NULL)
	{
		m_spWaterInstance->BuildVisibleSet(pkRenderingContext);
	}
}
//-------------------------------------------------------------------------------------------------

void CAdvanceWaterComponent::GetPropertyNames(
	NiTObjectSet<NiFixedString>& kPropertyNames) const
{

	kPropertyNames.Add(ms_kWaveLengthName);
	kPropertyNames.Add(ms_kAmplitudeName);
	kPropertyNames.Add(ms_kSpeedName);
	kPropertyNames.Add(ms_kDirectionName);
	kPropertyNames.Add(ms_kFresnelName);
	kPropertyNames.Add(ms_kAlphaName);
	kPropertyNames.Add(ms_kReflectColorName);
	kPropertyNames.Add(ms_kSpecularColorName);
	kPropertyNames.Add(ms_kLightDirName);
	kPropertyNames.Add(ms_kEnvMapName);
	kPropertyNames.Add(ms_kRefractMapName);
	kPropertyNames.Add(ms_kNifFilePathName);
	kPropertyNames.Add(ms_kSceneRootPointerName);
}
//-------------------------------------------------------------------------------------------------

NiBool CAdvanceWaterComponent::CanResetProperty(const NiFixedString& kPropertyName,
											bool& bCanReset) const
{
	if (kPropertyName == ms_kWaveLengthName || kPropertyName == ms_kAmplitudeName ||
		kPropertyName == ms_kSpeedName || kPropertyName == ms_kDirectionName ||
		kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName ||
		kPropertyName == ms_kRefractMapName || kPropertyName == ms_kNifFilePathName ||
		kPropertyName == ms_kSceneRootPointerName 
		)		
	{
		bCanReset = false;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::ResetProperty(const NiFixedString& kPropertyName)
{
	if (kPropertyName == ms_kWaveLengthName || kPropertyName == ms_kAmplitudeName ||
		kPropertyName == ms_kSpeedName || kPropertyName == ms_kDirectionName ||
		kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName ||
		kPropertyName == ms_kRefractMapName || kPropertyName == ms_kNifFilePathName ||
		kPropertyName == ms_kSceneRootPointerName )
	{
	}
	else
	{
		return false;
	}
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::MakePropertyUnique(const NiFixedString& kPropertyName,
											  bool& bMadeUnique)
{
	bool bCanReset;
	if (!CanResetProperty(kPropertyName, bCanReset))
	{
		return false;
	}
	bMadeUnique = true;
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetDisplayName(const NiFixedString& kPropertyName,
										  NiFixedString& kDisplayName) const
{
	//NiFixedString CAdvanceWaterComponent::ms_kWaveLengthName;
	//NiFixedString CAdvanceWaterComponent::ms_kAmplitudeName;
	//NiFixedString CAdvanceWaterComponent::ms_kSpeedName;
	//NiFixedString CAdvanceWaterComponent::ms_kDirectionName;
	//NiFixedString CAdvanceWaterComponent::ms_kFresnelName;
	//NiFixedString CAdvanceWaterComponent::ms_kAlphaName;
	//NiFixedString CAdvanceWaterComponent::ms_kReflectColorName;
	//NiFixedString CAdvanceWaterComponent::ms_kRefractColorName;
	//NiFixedString CAdvanceWaterComponent::ms_kLightDirName;
	//NiFixedString CAdvanceWaterComponent::ms_kEnvMapName;
	//NiFixedString CAdvanceWaterComponent::ms_kRefractMapName;
	//NiFixedString CAdvanceWaterComponent::ms_kNifFilePathName;
	//NiFixedString CAdvanceWaterComponent::ms_kSceneRootPointerName;


	if (kPropertyName == ms_kWaveLengthName)
	{
		kDisplayName = ms_kWaveLengthName;
	}
	else if (kPropertyName == ms_kAmplitudeName)
	{
		kDisplayName = ms_kAmplitudeName;
	}
	else if( kPropertyName == ms_kSpeedName)
	{
		kDisplayName = ms_kSpeedName;
	}
	else if (kPropertyName == ms_kDirectionName)
	{
		kDisplayName = ms_kDirectionName;
	}
	else if( kPropertyName == ms_kFresnelName)
	{
		kDisplayName = ms_kFresnelName;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kDisplayName = ms_kAlphaName;
	}
	else if( kPropertyName == ms_kReflectColorName)
	{
		kDisplayName = ms_kReflectColorName;
	}
	else if (kPropertyName == ms_kSpecularColorName)
	{
		kDisplayName = ms_kSpecularColorName;
	}
	else if( kPropertyName == ms_kLightDirName)
	{
		kDisplayName = ms_kLightDirName;
	}
	else if (kPropertyName == ms_kEnvMapName)
	{
		kDisplayName = ms_kEnvMapName;
	}
	else if( kPropertyName == ms_kRefractMapName)
	{
		kDisplayName = ms_kRefractMapName;
	}
	else if( kPropertyName == ms_kNifFilePathName)
	{
		kDisplayName = ms_kNifFilePathName;
	}
	else if( kPropertyName == ms_kSceneRootPointerName)
	{
		kDisplayName = NULL;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::SetDisplayName(const NiFixedString& kPropertyName,
										  const NiFixedString& kDisplayName)
{
	// This component does not allow the display name to be set.
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetPrimitiveType(const NiFixedString& kPropertyName,
											NiFixedString& kPrimitiveType) const// property 是什么类型的
{

	if (kPropertyName == ms_kWaveLengthName)
	{
		kPrimitiveType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kAmplitudeName)
	{
		kPrimitiveType = PT_FLOAT;
	}
	else if( kPropertyName == ms_kSpeedName)
	{
		kPrimitiveType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kDirectionName)
	{
		kPrimitiveType = PT_POINT2;
	}
	else if( kPropertyName == ms_kFresnelName)
	{
		kPrimitiveType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kPrimitiveType = PT_FLOAT;
	}
	else if( kPropertyName == ms_kReflectColorName)
	{
		kPrimitiveType = PT_COLOR;
	}
	else if (kPropertyName == ms_kSpecularColorName)
	{
		kPrimitiveType = PT_COLOR;
	}
	else if( kPropertyName == ms_kLightDirName)
	{
		kPrimitiveType = PT_POINT3;
	}
	else if (kPropertyName == ms_kEnvMapName)
	{
		kPrimitiveType = PT_STRING;
	}
	else if( kPropertyName == ms_kRefractMapName)
	{
		// Scene Root Pointer property should not be displayed.
		kPrimitiveType = PT_STRING;
	}
	else if( kPropertyName == ms_kNifFilePathName)
	{
		kPrimitiveType = PT_STRING;
	}
	else if( kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kPrimitiveType = PT_NIOBJECTPOINTER;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::SetPrimitiveType(const NiFixedString& kPropertyName,
											const NiFixedString& kPrimitiveType)
{
	// This component does not allow the primitive type to be set.
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetSemanticType(const NiFixedString& kPropertyName,
										   NiFixedString& kSemanticType) const
{

	if (kPropertyName == ms_kWaveLengthName)
	{
		kSemanticType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kAmplitudeName)
	{
		kSemanticType = PT_FLOAT;
	}
	else if( kPropertyName == ms_kSpeedName)
	{
		kSemanticType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kDirectionName)
	{
		kSemanticType = PT_POINT2;
	}
	else if( kPropertyName == ms_kFresnelName)
	{
		kSemanticType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kSemanticType = PT_FLOAT;
	}
	else if( kPropertyName == ms_kReflectColorName)
	{
		kSemanticType = PT_COLOR;
	}
	else if (kPropertyName == ms_kSpecularColorName)
	{
		kSemanticType = PT_COLOR;
	}
	else if( kPropertyName == ms_kLightDirName)
	{
		kSemanticType = PT_POINT3;
	}
	else if (kPropertyName == ms_kEnvMapName)
	{
		kSemanticType = "Filename";
	}
	else if( kPropertyName == ms_kRefractMapName)
	{
		// Scene Root Pointer property should not be displayed.
		kSemanticType = "Filename";
	}
	else if( kPropertyName == ms_kNifFilePathName)
	{
		kSemanticType = "NIF Filename";
	}
	else if( kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kSemanticType = PT_NIOBJECTPOINTER;
	}
	else
	{
		return false;
	}


	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::SetSemanticType(const NiFixedString& kPropertyName,

										   const NiFixedString& kSemanticType)
{
	// This component does not allow the semantic type to be set.
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetDescription(const NiFixedString& kPropertyName,
										  NiFixedString& kDescription) const
{

	if (kPropertyName == ms_kWaveLengthName)
	{
		kDescription = ms_kWaveLengthDescription;
	}
	else if (kPropertyName == ms_kAmplitudeName)
	{
		kDescription = ms_kAmplitudeDescription;
	}
	else if( kPropertyName == ms_kSpeedName)
	{
		kDescription = ms_kSpeedDescription;
	}
	else if (kPropertyName == ms_kDirectionName)
	{
		kDescription = ms_kDirectionDescription;
	}
	else if( kPropertyName == ms_kFresnelName)
	{
		kDescription = ms_kFresnelDescription;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kDescription = ms_kAlphaDescription;
	}
	else if( kPropertyName == ms_kReflectColorName)
	{
		kDescription = ms_kReflectColorDescription;
	}
	else if (kPropertyName == ms_kSpecularColorName)
	{
		kDescription = ms_kSpecularColorDescription;
	}
	else if( kPropertyName == ms_kLightDirName)
	{
		kDescription = ms_kLightDirDescription;
	}
	else if (kPropertyName == ms_kEnvMapName)
	{
		kDescription = ms_kEnvMapDescription;
	}
	else if( kPropertyName == ms_kRefractMapName)
	{
		// Scene Root Pointer property should not be displayed.
		kDescription = ms_kRefractMapDescription;
	}
	else if( kPropertyName == ms_kNifFilePathName)
	{
		kDescription = ms_kNifFilePathDescription;
	}
	else if( kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kDescription = ms_kSceneRootPointerDescription;
	}
	else
	{
		return false;
	}


	return true;

}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::SetDescription(const NiFixedString& kPropertyName,
										  const NiFixedString& kDescription)
{
	// This component does not allow the semantic type to be set.
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetCategory(const NiFixedString& kPropertyName,
									   NiFixedString& kCategory) const
{
	if (kPropertyName == ms_kWaveLengthName || kPropertyName == ms_kAmplitudeName ||
		kPropertyName == ms_kSpeedName || kPropertyName == ms_kDirectionName ||
		kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName ||
		kPropertyName == ms_kRefractMapName || kPropertyName == ms_kNifFilePathName ||
		kPropertyName == ms_kSceneRootPointerName )
	{
		kCategory = ms_kComponentName;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName,
											  bool& bIsReadOnly)
{
	if (kPropertyName == ms_kWaveLengthName || kPropertyName == ms_kAmplitudeName ||
		kPropertyName == ms_kSpeedName || kPropertyName == ms_kDirectionName ||
		kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName || kPropertyName == ms_kNifFilePathName)
	{
		bIsReadOnly = false;
		return true;
	}
	else if (kPropertyName == ms_kSceneRootPointerName||
						kPropertyName == ms_kRefractMapName )
	{
		bIsReadOnly = true;
		return true;
	}
	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::IsPropertyUnique(const NiFixedString& kPropertyName,
											bool& bIsUnique)
{
	if (kPropertyName == ms_kWaveLengthName || kPropertyName == ms_kAmplitudeName ||
		kPropertyName == ms_kSpeedName || kPropertyName == ms_kDirectionName ||
		kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName ||
		kPropertyName == ms_kRefractMapName || kPropertyName == ms_kNifFilePathName ||
		kPropertyName == ms_kSceneRootPointerName )
	{
		bIsUnique = true;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::IsPropertySerializable(const NiFixedString& kPropertyName,
												  bool& bIsSerializable)
{
	if (kPropertyName == ms_kWaveLengthName || kPropertyName == ms_kAmplitudeName ||
		kPropertyName == ms_kSpeedName || kPropertyName == ms_kDirectionName ||
		kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName || kPropertyName == ms_kNifFilePathName )
	{
		bIsSerializable = true;
	}
	else if (kPropertyName==ms_kSceneRootPointerName ||
						kPropertyName == ms_kRefractMapName )
	{
		bIsSerializable = false;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::IsPropertyInheritable(const NiFixedString& kPropertyName,
												 bool& bIsInheritable)
{
	if (kPropertyName == ms_kWaveLengthName || kPropertyName == ms_kAmplitudeName ||
		kPropertyName == ms_kSpeedName || kPropertyName == ms_kDirectionName ||
		kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName ||
		kPropertyName == ms_kRefractMapName || kPropertyName == ms_kNifFilePathName ||
		kPropertyName == ms_kSceneRootPointerName )
	{
		bIsInheritable = false;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::IsExternalAssetPath(const NiFixedString& kPropertyName,
											   unsigned int uiIndex, bool& bIsExternalAssetPath) const
{
	if (kPropertyName == ms_kWaveLengthName || kPropertyName == ms_kAmplitudeName ||
		kPropertyName == ms_kSpeedName || kPropertyName == ms_kDirectionName ||
		kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName  || kPropertyName == ms_kRefractMapName || 
		kPropertyName == ms_kSceneRootPointerName)
	{
		bIsExternalAssetPath = false;
	}
	else if(kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kEnvMapName)
	{
		bIsExternalAssetPath = false;
	}
	else
	{
		return false;
	}
	
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetElementCount(const NiFixedString& kPropertyName,
										   unsigned int& uiCount) const
{
	if (kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName ||
		kPropertyName == ms_kRefractMapName || kPropertyName == ms_kNifFilePathName ||
		kPropertyName == ms_kSceneRootPointerName )
	{
		uiCount = 1;
		return true;
	}
	else if (	kPropertyName == ms_kDirectionName || 
				kPropertyName == ms_kWaveLengthName || 
				kPropertyName == ms_kAmplitudeName ||
				kPropertyName == ms_kSpeedName )
	{
		uiCount = 4;
		return true;
	}
	else
	{
		return false;
	}

}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::SetElementCount(const NiFixedString& kPropertyName,
										   unsigned int uiCount, bool& bCountSet)
{
	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::IsCollection(const NiFixedString& kPropertyName,
										bool& bIsCollection) const
{
	if (kPropertyName == ms_kFresnelName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kReflectColorName || kPropertyName == ms_kSpecularColorName ||
		kPropertyName == ms_kLightDirName || kPropertyName == ms_kEnvMapName ||
		kPropertyName == ms_kRefractMapName || kPropertyName == ms_kNifFilePathName ||
		kPropertyName == ms_kSceneRootPointerName )
	{
		bIsCollection = false;
		return true;
	}
	else if (	kPropertyName == ms_kDirectionName || 
				kPropertyName == ms_kWaveLengthName || 
				kPropertyName == ms_kAmplitudeName ||
				kPropertyName == ms_kSpeedName )
	{
		bIsCollection = true;
		return true;
	}
	return false;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取 波长，振幅，速度
//NiBool CAdvanceWaterComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiQuaternion& kData, unsigned int uiIndex)
//{
//	if (uiIndex != 0)
//	{
//		return false;
//	}
//
//
//	if (kPropertyName==ms_kWaveLengthName && m_spWaterInstance!=NULL)
//	{
//		m_spWaterInstance->SetWaveLength((float*)(&kData));
//	}
//	else if (kPropertyName==ms_kAmplitudeName && m_spWaterInstance!=NULL)
//	{
//		m_spWaterInstance->SetAmplitude((float*)(&kData));
//	}
//	else if (kPropertyName==ms_kSpeedName && m_spWaterInstance!=NULL)
//	{
//		m_spWaterInstance->SetSpeed((float*)(&kData));
//	}
//	else if (m_spWaterInstance == NULL)
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//
//	return true;
//}
////-------------------------------------------------------------------------------------------------
//NiBool CAdvanceWaterComponent::GetPropertyData(const NiFixedString& kPropertyName, NiQuaternion& kData, unsigned int uiIndex) const
//{
//	if (uiIndex != 0)
//	{
//		return false;
//	}
//
//
//	if (m_spWaterInstance != NULL)
//	{
//		if (kPropertyName==ms_kWaveLengthName && m_spWaterInstance!=NULL)
//		{
//			memcpy(&kData, m_spWaterInstance->GetWaveLength(), sizeof(NiQuaternion));
//		}
//		else if (kPropertyName==ms_kAmplitudeName && m_spWaterInstance!=NULL)
//		{
//			memcpy(&kData, m_spWaterInstance->GetAmplitude(), sizeof(NiQuaternion));
//		}
//		else if (kPropertyName==ms_kSpeedName && m_spWaterInstance!=NULL)
//		{
//			memcpy(&kData, m_spWaterInstance->GetSpeed(), sizeof(NiQuaternion));
//		}
//		else
//		{
//			return false;
//		}
//	}
//	else
//	{
//		kData = NiQuaternion(0.0f, 0.0f, 0.0f, 0.0f); 
//	}
//
//
//	return true;
//}
//-------------------------------------------------------------------------------------------------

// 设置/获取反射/折射颜色
NiBool CAdvanceWaterComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiColor& kData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}

	if (kPropertyName==ms_kReflectColorName && m_spWaterInstance!=NULL)
	{
		m_spWaterInstance->SetReflectColor(kData);
	}
	else if (kPropertyName==ms_kSpecularColorName && m_spWaterInstance!=NULL)
	{
		m_spWaterInstance->SetSpecularColor(kData);
	}
	else if(m_spWaterInstance == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetPropertyData(const NiFixedString& kPropertyName, NiColor& kData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}

	if (m_spWaterInstance!=NULL)
	{
		if (kPropertyName == ms_kReflectColorName)
		{
			kData = m_spWaterInstance->GetReflectColor();
		}
		else if(kPropertyName == ms_kSpecularColorName)
		{
			kData = m_spWaterInstance->GetSpecularColor();
		}
		else
		{
			return false;
		}
	}
	else
	{
		kData = NiColor(0,0,0);
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取 fresnel alpha
NiBool CAdvanceWaterComponent::SetPropertyData(const NiFixedString& kPropertyName, const float fData, unsigned int uiIndex)
{
	if (uiIndex>=4)
	{
		return false;
	}
	
	if (kPropertyName==ms_kAlphaName && m_spWaterInstance!=NULL)
	{
		m_spWaterInstance->SetAlpha(fData);
	}
	else if (kPropertyName==ms_kFresnelName && m_spWaterInstance!=NULL)
	{
		m_spWaterInstance->SetFresnel(fData);
	}
	else if (kPropertyName==ms_kWaveLengthName && m_spWaterInstance!=NULL)
	{
		m_spWaterInstance->SetWaveLength(uiIndex, fData);
	}
	else if (kPropertyName==ms_kAmplitudeName && m_spWaterInstance!=NULL)
	{
		m_spWaterInstance->SetAmplitude(uiIndex, fData);
	}
	else if (kPropertyName==ms_kSpeedName && m_spWaterInstance!=NULL)
	{
		m_spWaterInstance->SetSpeed(uiIndex, fData);
	}
	else if(m_spWaterInstance == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex) const
{
	if (uiIndex>=4)
	{
		return false;
	}

	if (m_spWaterInstance!=NULL)
	{
		if (kPropertyName == ms_kAlphaName)
		{
			fData = m_spWaterInstance->GetAlpha();
		}
		else if(kPropertyName == ms_kFresnelName)
		{
			fData = m_spWaterInstance->GetFresnel();
		}
		else if (kPropertyName==ms_kWaveLengthName)
		{
			fData = m_spWaterInstance->GetWaveLength()[uiIndex];
		}
		else if (kPropertyName==ms_kAmplitudeName)
		{
			fData = m_spWaterInstance->GetAmplitude()[uiIndex];
		}
		else if (kPropertyName==ms_kSpeedName)
		{
			fData = m_spWaterInstance->GetSpeed()[uiIndex];
		}
		else
		{
			return false;
		}
	}
	else
	{
		fData = 0.0f;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取 Wave Direction
NiBool CAdvanceWaterComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiPoint2& kData, unsigned int uiIndex)
{
	if (uiIndex<0 || uiIndex>3)
	{
		return true;
	}

	if (kPropertyName==ms_kDirectionName )
	{
		if (m_spWaterInstance!=NULL)
		{
			m_spWaterInstance->SetWaveDirection(uiIndex, kData);
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetPropertyData(const NiFixedString& kPropertyName, NiPoint2& kData, unsigned int uiIndex) const
{
	if (uiIndex<0 || uiIndex>3)
	{
		return true;
	}

	if (kPropertyName == ms_kDirectionName)
	{
		if (m_spWaterInstance!=NULL)
		{
			NiPoint2* pkDirection = m_spWaterInstance->GetWaveDirection();
			kData = pkDirection[uiIndex];
		}
		else
		{
			kData = NiPoint2(0.0f, 0.0f);
		}
		return true;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetPropertyData(const NiFixedString& kPropertyName, NiPoint3& kData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kLightDirName)
	{
		if (m_spWaterInstance == NULL)
		{
			kData = NiPoint3(0.0f, 0.0f, 0.0f);
		}
		else
		{
			kData = m_spWaterInstance->GetLightDir();
		}
		return true;

	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiPoint3& kData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kLightDirName)
	{
		if (m_spWaterInstance == NULL)
		{
		}
		else
		{
			 m_spWaterInstance->SetLightDir(kData);
		}
		return true;

	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取 nif 文件路径
NiBool CAdvanceWaterComponent::GetPropertyData(const NiFixedString& kPropertyName,
										   NiFixedString& kData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kNifFilePathName)
	{
		kData = m_spNifFilePath;
	}
	else if (kPropertyName==ms_kEnvMapName)
	{
		kData = m_spEnvMapFile;
	}
	else if (kPropertyName==ms_kRefractMapName)
	{
		kData = "";
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::SetPropertyData(const NiFixedString& kPropertyName,
										   const NiFixedString& kData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kNifFilePathName)
	{
		if (kData != m_spNifFilePath)
		{

			m_spNifFilePath = kData;
			m_bNifFileChanged = true;
		}
	}
	else if (kPropertyName==ms_kEnvMapName)
	{
		if (kData != m_spEnvMapFile)
		{
			m_spEnvMapFile = kData;
			if (m_spWaterInstance != NULL)
			{
				NiTexturePtr pEnvMap = NiSourceTexture::Create(kData);
				NiSourceTexture* pkSrcTex = NiDynamicCast(NiSourceTexture, pEnvMap);
				char szTmp[MAX_PATH];
				ExtractFileNameFromPath(kData, szTmp);
				pkSrcTex->SetFilename(szTmp);
				m_spWaterInstance->SetEnvMap(pEnvMap);
			}
		}
	}
	else if (kPropertyName==ms_kRefractMapName)
	{

	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取 场景根
NiBool CAdvanceWaterComponent::SetPropertyData(const NiFixedString& kPropertyName,
										   const NiObject*& pkData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kSceneRootPointerName)
	{
		// 不支持
	}
	else
	{
		return false;
	}
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CAdvanceWaterComponent::GetPropertyData(const NiFixedString& kPropertyName,
										   NiObject*& pkData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kSceneRootPointerName)
	{
		if (m_spWaterInstance!=NULL)
		{
			pkData = m_spWaterInstance->GetSceneRoot();
		}
		else
		{
			pkData = NULL;
		}
	}
	else
	{
		return false;
	}
	return true;
}
//----------------------------------------------

#endif