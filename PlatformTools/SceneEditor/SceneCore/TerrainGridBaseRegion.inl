// 添加一个 Grid. 如果 Grid 数量超界，返回 false
inline void CTerrainGridBaseRegion::AddGridID(int iID)
{
	m_GridSet.insert(iID);
	m_bChanged = true;
}

// 移除一个 Grid
inline void CTerrainGridBaseRegion::RemoveGridID(int iID)
{
	m_GridSet.erase(iID);
	m_bChanged = true;
}

// 获取/设置 区域是否发生过改变。如果改变了需要重新计算 Geometry
inline bool CTerrainGridBaseRegion::IsChanged()
{
	return m_bChanged;
}

inline void CTerrainGridBaseRegion::SetChanged(bool bChanged)
{
	m_bChanged = bChanged;
}
