﻿/** @file AreaManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：徐磊
*	完成日期：2007-12-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME
#ifndef _AREAMANAGER_H
#define _AREAMANAGER_H

#include <NiTSet.h>
#include <NiFixedString.h>
#include <NiEntityInterface.h>

#include "Area.h"

#define REGIONPOINTID	"PointID"
#define REGIONID		"RegionID"
class CTerrain;

class MAIN_ENTRY CAreaManager
{
public:
	CAreaManager(void);
	CAreaManager(CTerrain* pTerrain);
public:
	~CAreaManager(void);
	bool LoadData(string strFileName);
	bool SaveData(string strFileName);
	static bool EntityIsAreaPoint(NiEntityInterface* pkEntityInterface);
public:
	bool UpdateAreaPoint(NiEntityInterface* pkEntityInterface, NiFixedString& pkPropertyName);
	bool AddAreaPoint(unsigned int uiAreaID,NiEntityInterface* pkEntityInterface);
	bool InitAreaPoint(NiEntityInterface* pkEntityInterface);
	bool DelAreaPoint(NiEntityInterface* pkEntityInterface);
public:
	unsigned short GetAreaTypeByID(unsigned int uiAreaID);//通过ID获取一个区域的类型
	bool SetAreaTypeByID(unsigned int uiAreaID, unsigned short usAreaType);//通过ID设置一个区域的类型
	string GetAreaNameByID(unsigned int uiAreaID);//通过ID获取一个区域的名称
	bool SetAreaNameByID(unsigned int uiAreaID, string strAreaName);//通过ID获取一个区域的名称
	string GetAreaMusicInfoByID(unsigned int uiAreaID, string strPropName);//获取特定的音乐属性
	bool SetAreaMusicInfoByID(unsigned int uiAreaID, string strPropName, string strPropValue);//设置特定的音乐属性

	unsigned int GetTopAreaID();//获取区域ID的最大值
public:
	unsigned int CreateArea(unsigned short usAreaType);//创建一个新的区域，返回区域的ID
	bool DelArea(unsigned int uiAreaID);//删除一个区域
	bool UpdateArea(unsigned int uiAreaID, unsigned short usAreaType, string strAreaName);//更新一个区域的信息
private:
	vector<CArea*>::iterator LocateArea(unsigned int uiRegionPointID);
	vector<CArea*>::iterator GetAreaByID(unsigned int uiAreaID);
public:
	vector<CArea*> m_AreaList;
	unsigned int m_uiAreaNum;		//记录区域的数量，用来生成
	unsigned int m_uiAreaPointNum;	//记录总共的点的数量，用来生成AreaPointID
protected:
	CTerrain*	m_pTerrain;//地形
};

#endif
#endif