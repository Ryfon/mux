﻿#include "StdAfx.h"
#ifndef CODE_INGAME

#include "GameScene.h"

#include "Terrain.h"
#include "SkyDome.h"
#include "EditableBaseObj.h"
#include "SceneFile.h"

CGameScene::CGameScene(void)
: m_pSkyDome(0)
, m_pTerrain(0)
{}

CGameScene::~CGameScene(void)
{}

void	CGameScene::Update( double dTime, float fElapsedTime )
{
	m_pSkyDome->Update( dTime, fElapsedTime );
	m_pTerrain->Update( dTime, fElapsedTime );
}

void	CGameScene::Render( NiCamera *pCamera, NiCullingProcess *pCuller, NiVisibleArray *pVisible )
{}


/// 设置活动相机
void	CGameScene::SetActiveCamera( NiCamera* pCamera )
{
	m_pTerrain->SetActiveCamera( pCamera );
}

bool	CGameScene::SaveToFile( const char *pszFile )
{
	CSceneFile sceneFile( pszFile );
	return sceneFile.Save();
}

bool	CGameScene::LoadFromFile( const char* pszFile )
{
	CSceneFile sceneFile( pszFile );
	return sceneFile.Load( this );
}

#endif