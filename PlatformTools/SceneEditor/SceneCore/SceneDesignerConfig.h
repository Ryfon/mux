﻿/** @file SceneDesignerConfig.h 
@brief 场景编辑器中用户配置数据
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-03-14
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：本类只在场景编辑器中使用
</pre>*/

#pragma once
#ifndef CODE_INGAME

#include "TextureSearchPath.h"
// 配置文件名
#define CONFIG_FILE_XML "scene_designer_config.xml"	

// 地表属性
struct MAIN_ENTRY stTerrainSurfaceProperty
{
	stTerrainSurfaceProperty(){}

	int		iID;		
	char	szName[64];
	NiColor	kColor;
};
#pragma  make_public(stTerrainSurfaceProperty)

// 区域属性
struct MAIN_ENTRY stRegionProperty
{
	stRegionProperty(){}

	int		nType;
	string	strName;
	string	strViewName;
	int		nShape;
	int		nColorR;
	int		nColorG;
	int		nColorB;
	int		nColorA;
};
#pragma make_public(stRegionProperty)

//081007 add by 和萌
//纹理区域属性
struct MAIN_ENTRY stMaterialRegPro
{
	//纹理名及其对应区域属性
	map<string,int> mapMtlRegPro;

};

/// 场景编辑器配置数据
class MAIN_ENTRY CSceneDesignerConfig
{
public:
	/// 析构
	~CSceneDesignerConfig(void);

	/// 获取唯一实例
	static CSceneDesignerConfig* GetInstance();

	/// 添加/删除/获取 地表属性
	bool AddTerrainSurfaceProperty(const stTerrainSurfaceProperty& terrProperty);
	bool RemoveTerrainSurfaceProperty(const char* pszPropertyName);
	bool SetTerrainSurfacePropertyColor(const char* pszPropertyName, const NiColor& kColor);
	vector<stTerrainSurfaceProperty>& GetTerrainSurfacePropertyList();

	/// 获取地表属性数量
	int GetNumTerrainSurfaceProerties();

	/// 按顺序获取地表属性
	stTerrainSurfaceProperty& GetTerrainSurfaceProperty(unsigned int iNum);

	/// 获取 iPropertyID 对应的颜色
	const NiColor&	GetTerrainSurfacePropertyColor(int iPropertyID)
	{
		// 暂时放在 .h 中，记得移到 .cpp
		for (unsigned int i=0; i<m_TerrainSurfacePropertyList.size(); i++)
		{
			if (m_TerrainSurfacePropertyList[i].iID == iPropertyID)
			{
				return m_TerrainSurfacePropertyList[i].kColor;
			}
		}

		return m_TerrainSurfacePropertyList[0].kColor;
	}

	/// 获取摄象机参数
	void GetCameraParam(float& fCamFOV, float& fPlayerToCamera);

	float GetCameraFOV() {return m_fCameraFOV;}
	float GetCameraToPlayer() {return m_fPlayerToCamera;}

	/// 获取entity category 列表
	const vector<string>& GetEntityCategoryList() const;

	/// 通过9位类型名获取 template 的 显示名称 
	const char* GetTemplateViewName(string strTypeName);

	/// 保存配置文件
	bool SaveToXml();

	/// 获取纹理搜索路径
	NiSearchPath*	GetTextureSearchPath();
	vector<string>& GetTexturePathList() {return m_TexturePathList;}

	//获取区域属性信息
	string GetRegionPropViewName(int nIndex);
	stRegionProperty* GetRegionPropByIndex(int nIndex);
	int GetRegionShapeByType(int nType);
	int GetRegionColorByType(int nType, int nColorType);
	int GetRegionShapyeByMaterial(const string strMtlName);

private:
	/// 构造
	CSceneDesignerConfig(void);

	/// 保存/载入 配置文件
	bool _LoadFromXml();
	/// 根据名称搜索 property
	vector<stTerrainSurfaceProperty>::iterator _FindTerrainSurfaceProperty(const char* pszPropertyName, int iID);
	
	/// 属性
	vector<stTerrainSurfaceProperty>		m_TerrainSurfacePropertyList;	// 地表属性列表
	float								m_fCameraFOV;					// game camera 的 fov
	float								m_fPlayerToCamera;				// game camera 到 角色的距离
	vector<string>						m_EntityCategoryList;			// entity 类型列表. 目前包含地上不可行走 地上可行走物体 地下物体 标尺
	map<string, string>					m_mapTypeName2TemplateName;		// 9位的类型名称(文件名前9位)到模板名的影射
	CTextureSearchPath					m_TextureSearchPath;			// 纹理的搜索路径 
	vector<string>						m_TexturePathList;				
	vector<stRegionProperty>			m_RegionPropertyList;			//	区域属性列表
	stMaterialRegPro					m_MtlRegionProperty;			//  纹理区域属性列表
};
#pragma  make_public(CSceneDesignerConfig)
#endif