utility My_Rollout "导入3ds及顶点信息" width:162 height:300
(
	button btn1 "导入3ds文件" pos:[38,42] width:96 height:19
	on btn1 pressed do
	(
		--导入文件 3ds
		actionMan.executeAction 0 "40010"  -- File: Import File
		modPanel.addModToSelection (Uvwmap ()) ui:on
		--从文件读取alpha
		vert_alpha = #()
		in_name = GetOpenFileName()
		in_file = openFile in_name
	
		if in_file != undefined then
		(
			num_vert = readValue in_file
			vert_alpha.count = num_vert
			for i = 1 to num_vert do
			(
				vert_alpha[i] = readValue in_file
			)--end of num_vert do	
			close in_file
		)--end of if
		
		
		for cObject in Geometry do
		(
			tmesh = getNodeByName(cObject.name)
			--设置显示顶点色
			tmesh.showVertexColors = true
			--设置顶点色模式为alpha
			tmesh.vertexColorType = #alpha
			mesh_verts = tmesh.numverts
			
			if mesh_verts != num_vert then
			(
				messagebox ("顶点数不匹配")
				messagebox("mesh_verts=" + (tmesh.numverts as string) +"num_vert=" + ( vert_alpha.count as string ))
			)--end of then
			
			--设置alpha 从顶点1到mesh_verts 数组#
			for i = 1 to mesh_verts do
			(
				meshop.setVertAlpha tmesh -2 i (vert_alpha[i] / 255)
				update tmesh
			)--end of mesh_verts do
			
			
		)-- end of cObject in Gerometry
	)--end of btn1 do
)