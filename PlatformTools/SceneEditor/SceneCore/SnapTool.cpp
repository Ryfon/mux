﻿#include "StdAfx.h"
#include "SnapTool.h"

#ifndef CODE_INGAME
//---------------------------------------------------------------------------------------------------------------
CSnapTool* CSnapTool::ms_pkThis = NULL;

//////////////////////////////////////////////////////////////////////////
CSnapTool::CSnapTool(void)
:m_pkSrcEntity(NULL),
 m_pkDesEntity(NULL),
 m_pkSnapSign(NULL),
 m_pkSrcGeom(NULL),
 m_pkDesGeom(NULL)
{
	
}

//////////////////////////////////////////////////////////////////////////
CSnapTool::~CSnapTool(void)
{
	Do_Dispos();
}

//////////////////////////////////////////////////////////////////////////
CSnapTool* CSnapTool::Instance()
{
	if (ms_pkThis == NULL)
	{
		ms_pkThis = new CSnapTool();
	}
	return ms_pkThis;
}

//////////////////////////////////////////////////////////////////////////
bool CSnapTool::SetSrcEntity(NiAVObject* pkEntity)
{
	if (pkEntity == NULL)
	{
		return false;
	}

	if (_IsValidaEntity(pkEntity) == false)
	{
		return false;
	}

	m_pkSrcEntity = pkEntity;

	//if (_CreateGeom(EP_SRC) == false)
	//{
	//	return false;
	//}	

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool CSnapTool::SetDesEntity(NiAVObject* pkEntity)
{
	if (pkEntity == NULL)
	{
		return false;
	}

	if (_IsValidaEntity(pkEntity) == false)
	{
		return false;
	}

	m_pkDesEntity = pkEntity;

	/*if (_CreateGeom(EP_DES) == false)
	{
		return false;
	}	*/

	return true;
}

//////////////////////////////////////////////////////////////////////////
void CSnapTool::Do_Dispos()
{
	if (m_pkSnapSign)
	{
		NiDelete m_pkSnapSign;
		m_pkSnapSign = NULL;
	}
	
	ClearSnapEntity();
}

//////////////////////////////////////////////////////////////////////////
bool CSnapTool::GetDesPos(NiPoint3& kCurrPos)
{
	//没有初始化目标和源物件
	if (NULL == m_pkSrcEntity || NULL == m_pkDesEntity)
	{
		return false;
	}

	//目标和源物件距离过远则不进行吸附
	NiPoint3 kPos = m_pkDesEntity->GetWorldTranslate();	
	float fDistance = _ComputeDistance(kPos,kCurrPos);
	/*if ( fDistance >= MAX_DISTANCE)
	{
		return false;
	}*/

	vector<NiPoint3*> kSrcVertices;
	vector<NiPoint3*> kDesVertices;
	RecursiveFindGeometries kFunctor;

	//获得几何形位置
	NiTNodeTraversal::DepthFirst_AllObjects(m_pkSrcEntity, kFunctor);	
	kSrcVertices = kFunctor.m_pkPos;
	kFunctor.m_pkPos.clear();

	NiTNodeTraversal::DepthFirst_AllObjects(m_pkDesEntity, kFunctor);	
	kDesVertices = kFunctor.m_pkPos;

	fDistance = _ComputeDistance(*kSrcVertices[0],*kDesVertices[0]);

	for (unsigned int uiSrcIndex = 0; uiSrcIndex < kSrcVertices.size(); uiSrcIndex++)
	{
		NiPoint3 kSrcPos = *kSrcVertices[uiSrcIndex];
		for (unsigned int uiDesIndex = 0; uiDesIndex < kDesVertices.size(); uiDesIndex++)
		{
			NiPoint3 kDesPos = *kDesVertices[uiDesIndex];
			float fCurrDis = _ComputeDistance(kSrcPos, kDesPos);
			if (abs(fCurrDis) <= abs(fDistance))
			{
				fDistance = fCurrDis;
				kPos = kSrcPos - kDesPos;				
			}
		}		
	}
	//目标和源物件距离过远则不进行吸附
	if ( fDistance >= MAX_DISTANCE)
	{
		for (UINT uiIndex = 0; uiIndex < kSrcVertices.size(); uiIndex++)
		{
			NiDelete kSrcVertices[uiIndex];
			kSrcVertices[uiIndex] = NULL;
		}
		kSrcVertices.clear();

		for (UINT uiIndex = 0; uiIndex < kDesVertices.size(); uiIndex++)
		{
			NiDelete kDesVertices[uiIndex];
			kDesVertices[uiIndex] = NULL;
		}
		kDesVertices.clear();
		return false;
	}
	
	kCurrPos -= kPos;

	// clear
	for (UINT uiIndex = 0; uiIndex < kSrcVertices.size(); uiIndex++)
	{
		NiDelete kSrcVertices[uiIndex];
		kSrcVertices[uiIndex] = NULL;
	}
	kSrcVertices.clear();

	for (UINT uiIndex = 0; uiIndex < kDesVertices.size(); uiIndex++)
	{
		NiDelete kDesVertices[uiIndex];
		kDesVertices[uiIndex] = NULL;
	}
	kDesVertices.clear();

	return true;
}

//////////////////////////////////////////////////////////////////////////
//bool CSnapTool::RenderSnapEntityVertice(NiRenderer* pkRenderer)
//{
//	if (NULL == pkRenderer)
//	{
//		return false;
//	}
//	if (NULL == m_pkSrcGeom || NULL == m_pkDesGeom)
//	{
//		return false;
//	}
//
//	m_pkSrcGeom->RenderImmediate(pkRenderer);
//	m_pkDesGeom->RenderImmediate(pkRenderer);
//
//	return true;
//}

//////////////////////////////////////////////////////////////////////////
int CSnapTool::IsSnapEntity(NiAVObject* pkObject)
{
	if (NULL == pkObject)
	{
		return -1;
	}

	if (NULL == m_pkSrcEntity || NULL == m_pkDesEntity)
	{
		return -1;
	}

	if (pkObject == m_pkSrcEntity)
	{
		return 1;
	}
	
	if (pkObject == m_pkDesEntity)
	{
		return 2;
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////
//bool CSnapTool::_CreateGeom(EEntityType epType)
//{
//	if (epType == EP_NONE)
//	{
//		return false;
//	}
//	vector<NiPoint3*> kVertices;
//	RecursiveFindGeometries kFunctor;
//
//	//获得几何形位置
//	if (epType == EP_SRC)
//	{
//		if (NULL == m_pkSrcEntity)
//		{
//			return false;
//		}
//		NiTNodeTraversal::DepthFirst_AllObjects(m_pkSrcEntity, kFunctor);		
//	}
//	else if (epType == CSnapTool::EP_DES)
//	{
//		if (NULL == m_pkDesEntity)
//		{
//			return false;
//		}
//		NiTNodeTraversal::DepthFirst_AllObjects(m_pkDesEntity, kFunctor);	
//	}
//	kVertices = kFunctor.m_pkPos;
//
//	for (unsigned int uiIndex = 0; uiIndex < kVertices.size(); uiIndex++)
//	{
//		unsigned int nVertexNum = 4;
//		NiPoint3 *pVerts  = NiNew NiPoint3[nVertexNum];
//		NiColorA *pColors = NiNew NiColorA[nVertexNum];
//		NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* nVertexNum );
//
//		NiPoint3 tmpPointTop(*kVertices[uiIndex]);
//		NiPoint3 tmpPointLeft(*kVertices[uiIndex]);
//		NiPoint3 tmpPointRight(*kVertices[uiIndex]);
//		NiPoint3 tmpPointBtm(*kVertices[uiIndex]);
//		NiPoint3 kOffset = NiPoint3(0.5f,0.5f,0.0f);
//
//		tmpPointTop.z += 0.5f;
//		tmpPointBtm.z -= 0.5f;
//		tmpPointLeft -= kOffset;
//		tmpPointRight += kOffset;
//
//		//位置
//		pVerts[0] = tmpPointTop;
//		pVerts[1] = tmpPointBtm;
//		pVerts[2] = tmpPointLeft;		
//		pVerts[3] = tmpPointRight;
//
//		//颜色
//		for (unsigned int uiCount = 0; uiCount < nVertexNum; uiCount++)
//		{
//			if (epType == CSnapTool::EP_SRC)
//			{
//				pColors[uiCount] = NiColorA(1.0f,0.0f,0.0f,1.0f);
//			}
//			else
//			{
//				pColors[uiCount] = NiColorA(0.0f,1.0f,0.0f,1.0f);
//			}
//
//		}
//		//连接
//		bConnect[0] = true;
//		bConnect[1] = false;
//		bConnect[2] = true;
//		bConnect[3] = false;
//
//		NiLinesPtr pLine1 = NiNew NiLines( nVertexNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );		
//		NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
//		pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
//		pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
//		pLine1->AttachProperty( pVCProp );
//
//		pLine1->Update(0.0f);
//		pLine1->UpdateProperties();
//		pLine1->UpdateEffects();
//
//		if (epType == EP_SRC)
//		{
//			m_pkSrcGeom = pLine1;
//		}
//		else if (epType == EP_DES)
//		{
//			m_pkDesGeom = pLine1;
//		}
//		//pLine1->RenderImmediate(NiRenderer::GetRenderer());
//	}
//
//	for (UINT uiIndex = 0; uiIndex < kVertices.size(); uiIndex++)
//	{
//		NiDelete kVertices[uiIndex];
//		kVertices[uiIndex] = NULL;
//	}
//	kVertices.clear();
//
//	return true;
//}

//////////////////////////////////////////////////////////////////////////
float CSnapTool::_ComputeDistance(NiPoint3 kSrcPos, NiPoint3 kDesPos)
{
	float fResult = (kSrcPos.x - kDesPos.x) * (kSrcPos.x - kDesPos.x) +
					(kSrcPos.y - kDesPos.y) * (kSrcPos.y - kDesPos.y) + 
					(kSrcPos.z - kDesPos.z) * (kSrcPos.z - kDesPos.z);

	return fResult;
}

//////////////////////////////////////////////////////////////////////////
bool CSnapTool::ClearSnapEntity()
{
	if (m_pkSrcEntity)
	{
		m_pkSrcEntity = NULL;
	}

	if (m_pkDesEntity)
	{
		m_pkDesEntity = NULL;
	}

	if (m_pkSrcGeom)
	{
		NiDelete m_pkSrcGeom;
		m_pkSrcGeom = NULL;
	}

	if (m_pkDesGeom)
	{
		NiDelete m_pkDesGeom;
		m_pkDesGeom = NULL;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////
bool CSnapTool::_IsValidaEntity(NiAVObject* pkEntity)
{
	if (NULL == pkEntity)
	{
		NiMessageBox("没有选择物体","Error",0);
		return false;
	}

	RecursiveFindGeometries kFunctor;

	//获得几何形位置
	NiTNodeTraversal::DepthFirst_AllObjects(pkEntity, kFunctor);	
	
	if (kFunctor.m_pkPos.size() == 0)
	{
		NiMessageBox("没有名为Snap的标志点","Error",0);

		for (UINT uiIndex = 0; uiIndex < kFunctor.m_pkPos.size(); uiIndex++)
		{
			NiDelete kFunctor.m_pkPos[uiIndex];
			kFunctor.m_pkPos[uiIndex] = NULL;
		}
		kFunctor.m_pkPos.clear();

		return false;
	}

	for (UINT uiIndex = 0; uiIndex < kFunctor.m_pkPos.size(); uiIndex++)
	{
		NiDelete kFunctor.m_pkPos[uiIndex];
		kFunctor.m_pkPos[uiIndex] = NULL;
	}
	kFunctor.m_pkPos.clear();

	return true;
}

//--------------------------------------------------------------------------------------------
#endif
