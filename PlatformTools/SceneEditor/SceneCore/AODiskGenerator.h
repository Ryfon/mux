﻿/** @file AODiskGenerator.h 
@brief 将场景中所有顶点产生对应的 Disk,来进行地形 AO 计算。
<pre>
*	Copyright (c) 2008，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2008-04-23
*	由于场景模型精度低,产生的 disk 不够理想.目前没有考虑场景模型对地面的 AO
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#ifndef CODE_INGAME

class CTerrain;

namespace SceneCore
{

/// 顶点对应的 disk
struct  MAIN_ENTRY tAODisk
{
	NiPoint3	kPosition;	// 位置
	NiPoint3	kNormal;	// 法线
	float		fArea;		// 面积
};

/// Disk 生成器
class  MAIN_ENTRY CAODiskGenerator
{
public:
	CAODiskGenerator(CTerrain* pkTerrain);
	~CAODiskGenerator(void);

	// 产生 disk
	void RecursiveFindGenerateDisk(NiAVObjectPtr pkSceneRoot);

	// 根据 NiGeometry 产生 disk, 
	void GenerateDisk(NiGeometryPtr pkGeom);

	/// 地形产生 disk
	void _GenerateDisk(CTerrain* pkTerrain);

	/// 计算三角形面积
	float TriangleArea(const NiPoint3& v0, const NiPoint3& v1, const NiPoint3& v2);

	list<tAODisk>	*m_aSceneDiskList;		// 所有场景物件 disk 列表数组
	list<tAODisk>	*m_aTerrainDiskList;	// 所有地形 disk 列表.同地形顶点一一对应

	CTerrain*		m_pkTerrain;			// 目标地形
	int				m_iNumChunkX;			// 地形在 x 方向 chunk 数
	int				m_iNumChunkY;			//		  y 方向 chunk 数
};

#include "AODiskGenerator.inl"
};

#endif