﻿/** @file SceneFile.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-09-29
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 *
 *  备    注：暂时没有用到 
 </pre>*/

#ifndef CODE_INGAME

#ifndef	SCENE_H 
#define SCENE_H

/// 前置声明
class CGameScene;

/**
 @brief 场景文件
 *	用于保存工具端用户生成的.scn场景文件，供用户在game中调用
 */
class MAIN_ENTRY ISceneFile
{
public:

	/// 构造
	ISceneFile( const char *pszFile ) : m_strFileName( pszFile){}

	/// 析构
	virtual ~ISceneFile( void ){}

	/// 保存
	virtual bool		Save() = 0;

	/// 加载
	virtual bool		Load( CGameScene* ) = 0;

protected:

	/// 文件名
	string				m_strFileName;
};

/**
 @brief 场景文件实现类
 */
class CSceneFile :	public ISceneFile
{
public:
	/// 构造
	CSceneFile( const char *pszFile ) : ISceneFile( pszFile ){}

	/// 析构
	virtual ~CSceneFile(void) {}

	/// 保存
	virtual bool		Save();

	/// 加载
	virtual bool		Load( CGameScene* );
};

#endif
#endif