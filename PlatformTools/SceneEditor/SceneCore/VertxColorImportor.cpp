﻿#include "StdAfx.h"
#include "VertxColorImportor.h"
#include <stdlib.h>

#ifndef CODE_INGAME

using namespace SceneCore;

CVertxColorImportor::CVertxColorImportor(void)
{
	m_numEntity = 0;
					
}

CVertxColorImportor::~CVertxColorImportor(void)
{
	int iLength = m_geoMap.size();

	if ( iLength > 0 )
	{
		return;
	}

	for ( int i = 0; i < iLength; i ++ )
	{
		delete m_geoMap[i];
	}
	
}

//////////////////////////////////////////////////////////////////////////
//初始化函数，由nodeCount个Geometry的importor
bool	CVertxColorImportor::InitialScene(const char* strSceneFile)
{
	
	//////////////////////////////////////////////////////////////////////////
	//打开顶点颜色数据文件
	ifstream fInputFile(strSceneFile);

	float p1,p2,p3;
	int num_vertex,iIndexNode;
	string temp;

	for ( int i = 0; i < m_numEntity; i ++ )
	{
		if ( fInputFile.good() )
		{
			for (int t = 0; t < entityInfo[i].end - entityInfo[i].begin; t ++ )
			{
				//////////////////////////////////////////////////////////////////////////
				//将节点名取到temp中，该节点的顶点数取到num_vertex中
				fInputFile>>temp>>num_vertex;

				//////////////////////////////////////////////////////////////////////////
				//初始化map指针，用于给m_geoVector赋值
				map<int,NiColorA> *pGeoMap = new map<int,NiColorA>;
								
				//从temp中提取node编号
				string strNode = "node_";
				int iIndex = temp.find_last_of( strNode );
				strNode = temp.substr( iIndex + 1, temp.size() );
				iIndexNode = atoi( strNode.c_str() );

				//////////////////////////////////////////////////////////////////////////
				//检查atoi是否出错
				if (  (errno == ERANGE) )
				{
					NiMessageBox("atoi 溢出","error");
				}
				else
					if ( iIndexNode == 0 )
					{
						NiMessageBox("atoi 转换错误","error");
					}

				//////////////////////////////////////////////////////////////////////////
				//下标从0开始
				iIndexNode --; 

				//////////////////////////////////////////////////////////////////////////
				//填入数据
				for ( int j = 0; j < num_vertex; j ++ )
				{
					//将节点序号取到index中 
					int index;
					fInputFile>>index;

					//.dat文件中的index是从1开始
					index --;
					//取3个顶点的颜色
					fInputFile>>p1>>p2>>p3;
					//NiColorA是从0.0f-1.0f
					p1 = p1 / 255.0f;
					p2 = p2 / 255.0f;
					p3 = p3 / 255.0f;

					(*pGeoMap)[index] = NiColorA(p1,p2,p3,1.0f );
				}
			
				m_geoMap[iIndexNode] = pGeoMap;
				
				//delete pGeoMap;
			}
		}
	}
	fInputFile.close();


	return true;
	
}

bool	CVertxColorImportor::InitialEntity(const char* strEntityFile)
{

	//////////////////////////////////////////////////////////////////////////
	//打开顶点数据文件
	ifstream fEntityFile(strEntityFile);

	//将entity个数取到 num_Node,num_Node不包含地形，所以m_numEntity + 1
	fEntityFile >> m_numEntity;
	m_numEntity ++;
	
	for ( int i = 0; i < m_numEntity; i++ )
	{
		if ( fEntityFile.good() )
		{
			struct	entityStru arr;
			fEntityFile >> arr.begin >> arr.end;
			entityInfo.push_back(arr);
		
		}
	}
	
	fEntityFile.close();


	return true;

}


//////////////////////////////////////////////////////////////////////////
//替换pkObject中的顶点颜色
//参数：NiAVObject *pkAVObj			要着色的几何形
//		int			entityCount		entity的序号
void CVertxColorImportor::SetVertexColor(NiAVObject* pkAVObj,int &iBeginNode,int iEndNode )
{
	//如果该entity的起始点和终结点相同，则忽略
	if ( iBeginNode == iEndNode )
	{
		return;
	}
	else
	{
		
		if ( pkAVObj->GetAppCulled() || (!NiIsKindOf(NiNode, pkAVObj) && !NiIsKindOf(NiTriBasedGeom, pkAVObj)) )
		{
			//不是合法节点
			return;
		}

		//如果是NiNode则递归调用
		if ( NiIsKindOf( NiNode,pkAVObj ) )
		{
			NiNode* pkNode = (NiNode*)pkAVObj;
			//获得子节点个数
			int num_pkNode = pkNode->GetChildCount();
			for (int i = 0; i < num_pkNode; i ++ )
			{
				SetVertexColor( pkNode->GetAt(i),iBeginNode,iEndNode );
				
			}
		}

		else	if( NiIsKindOf( NiTriBasedGeom,pkAVObj ) )
		{

			//如果是NiTriBasedGeom类型则替换顶点
			{	
				NiTriBasedGeom* pkGeom = (NiTriBasedGeom*)pkAVObj;
				//顶点数和面片数
				int numVertex = pkGeom->GetVertexCount();

				//获得pkGeom的NiGeometryData
				NiGeometryData* pkGeomData = pkGeom->GetModelData();
				pkGeomData->SetConsistency(pkGeomData->MUTABLE);

				//顶点颜色列表
				pkGeom->CreateColors(true);
				NiColorA * pkColorList	= pkGeomData->GetColors();
					
				map< int,NiColorA > *iNodeList = m_geoMap[iBeginNode];
				for (map<int,NiColorA>::iterator it = iNodeList->begin(); it != iNodeList->end(); it++)
				{
					if (it->first < numVertex )
					{
						pkColorList[it->first] = it->second ;

					}
					else
						NiMessageBox("out of range","error");
				}
					
				iBeginNode ++;
				pkGeomData->MarkAsChanged( pkGeomData->COLOR_MASK );

			}
				
		}
	}
	
}

//////////////////////////////////////////////////////////////////////////
//为地形保存顶点数据
void CVertxColorImportor::SetTerrainVertexColor(CTerrain * pkTerrain,int &iBegin )
{
	int xNum = pkTerrain->GetChunkNumX();
	int yNum = pkTerrain->GetChunkNumY();

	NiColorA * pkColorList = pkTerrain->GetVertexColor();
	map<int,NiColorA>::iterator it;

	//设置chunk偏移量
	DWORD	scanLine = xNum * ( NUMCHUNKVERTEX - 1) + 1;

	//////////////////////////////////////////////////////////////////////////
	//每行chunk的顶点数是 xNum * 64 + 1
	for ( int iChunkY = 0; iChunkY < yNum; iChunkY ++ )
	{
		for ( int iChunkX = 0; iChunkX < xNum; iChunkX ++ )
		{
			map<int, NiColorA> *iNodeList = m_geoMap[iBegin ++ ];
			it = iNodeList->begin();
			int x = iChunkX * ( NUMCHUNKVERTEX - 1);
			int y = iChunkY * ( NUMCHUNKVERTEX - 1);
			for ( int i = 0; i < NUMCHUNKVERTEX ; i ++ )
			{
				for ( int j = 0; j < NUMCHUNKVERTEX; j ++ )
				{

					int iIndex = x + j +  scanLine *  ( y + i );
					if ( iIndex >= NUMVERTEX *  xNum * yNum )
						NiMessageBox("","");

					pkColorList[ iIndex ] = it->second;

					it++;
				}
			}

			//相邻两个chunk公用顶点
			
		}
	}
	
	
}

//End of Class


#endif