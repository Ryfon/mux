﻿/** @file SnapTool.h 
@brief 包围盒绘制
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-09-10
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
#ifndef	_BOUNDGEOMETROY_H
#define _BOUNDGEOMETROY_H

#define INVALID_POINT NiPoint3( -1000.0f, -1000.0f, -1000.0f )

///最大线数
#define MAX_LINE_COUNT (12)

#ifndef CODE_INGAME
class MAIN_ENTRY CBoundGeometroy
{
public:
	CBoundGeometroy(void);
	~CBoundGeometroy(void);

	static CBoundGeometroy* GetInstance();
		
	// 设置盒子的位置 [9/11/2009 hemeng]
	NiLinesPtr	GetBox();
	void	SetBoundPosCenter( const NiPoint3& vBoundCenter );
	void	SetBoundRadius( float fBoundRaidus );
	void	SetBox(NiPoint3 kCenter, NiPoint3 kExtents, NiMatrix3 matRotate,const char* szEntityName);
	void	SetBoxScale(const char* strEntityName, float fScale);
	float	GetAABBScale(const char* strEntityName);
	map<string,float>	GetScaleInfo()		{return m_mapAABBScale;}
;
protected:
	NiLinesPtr	_CreateLine();
	void		_SetLinePoint(int nLineIndex, const NiPoint3& vStartPos, const NiPoint3& vEndPos );
	void		_Update();

private:
	bool				m_bInit;
	NiLinesPtr			m_pkBoxLine;
	float				m_fBoundRaidus;
	NiPoint3			m_kBoundCenter;
	bool				m_bChanged;
	map<string, float>	m_mapAABBScale;
};

#endif // end code_ingame define [9/10/2009 hemeng]
#endif // end h file define [9/10/2009 hemeng]