﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include "Area.h"

CArea::CArea(void)
{
	m_usVolume = 0;
	m_usMusicLoop = 1;
}

CArea::~CArea(void)
{
}

int CArea::IsExist(unsigned int uiPointID)
{
	int nPointNum = m_AreaPointList.size();

	for (int index = 0;index < nPointNum; ++index)
	{
		if (m_AreaPointList[index]->uiPointID == uiPointID)
		{
			return index;
		}
	}

	return -1;
}

float CArea::_GetNearDistance(AreaPoint &PointM, AreaPoint &PointA, AreaPoint &PointB)
{
	return 0.0f;
}

vector<AreaPoint*>::iterator CArea::_GetInsertPos(int nXPos, int nYPos)
{
	return m_AreaPointList.end();
}

int CArea::InsertPoint(unsigned int uiPointID, int nXPos, int nYPos)
{
	vector<AreaPoint*>::iterator it = _GetInsertPos(nXPos, nYPos);
	AreaPoint* pTempPoint = new AreaPoint();
	pTempPoint->uiPointID = uiPointID;
	pTempPoint->nXPos = nXPos;
	pTempPoint->nYPos = nYPos;
	m_AreaPointList.insert(it, pTempPoint);

	return 0;
}

int CArea::UpdatePoint(unsigned int uiPointID, int nXPos, int nYPos)
{
	int nIndex = IsExist(uiPointID);
	if ( nIndex == -1)
	{
		return -1;
	}

	m_AreaPointList[nIndex]->nXPos = nXPos;
	m_AreaPointList[nIndex]->nYPos = nYPos;

	return 0;
}

int CArea::DeletePoint(unsigned int uiPointID)
{
	int nIndex = IsExist(uiPointID);
	if ( nIndex == -1)
	{
		return -1;
	}

	vector<AreaPoint*>::iterator it = m_AreaPointList.begin()+nIndex;
	delete (*it);
	m_AreaPointList.erase(it);

	return 0;
}

unsigned int CArea::LoadData(const TiXmlElement* pXmlArea)
{
	m_uiAreaID = atol(pXmlArea->Attribute("RegionID"));
	m_usAreaType = atol(pXmlArea->Attribute("RegionType"));
	m_strAreaName = pXmlArea->Attribute("RegionName");
	m_usAreaShape = atol(pXmlArea->Attribute("RegionShape"));

	// 音乐区域，增加属性
	if (m_usAreaType == 4 || m_usAreaType == 5)
	{
		m_strMusicFile = pXmlArea->Attribute("MusicFile");
		m_usVolume = atol(pXmlArea->Attribute("Volume"));
		m_usMusicLoop = atol(pXmlArea->Attribute("MusicLoop"));
	}

	unsigned int nTempAreaPointID = 0;
	for (const TiXmlElement* pXmlAreaPoint = pXmlArea->FirstChildElement();pXmlAreaPoint;pXmlAreaPoint = pXmlAreaPoint->NextSiblingElement())
	{
		AreaPoint* pTempAreaPoint = new AreaPoint();
		pTempAreaPoint->uiPointID = atol(pXmlAreaPoint->Attribute("PointID"));
		if (pTempAreaPoint->uiPointID > nTempAreaPointID)
		{
			nTempAreaPointID = pTempAreaPoint->uiPointID;
		}
		pTempAreaPoint->nXPos = atol(pXmlAreaPoint->Attribute("XPos"));
		pTempAreaPoint->nYPos = atol(pXmlAreaPoint->Attribute("YPos"));
		m_AreaPointList.push_back(pTempAreaPoint);
	}

	return nTempAreaPointID;
}

int CArea::SaveData(TiXmlElement* pXmlArea, int nAreaShape)
{
	pXmlArea->SetAttribute("RegionID", m_uiAreaID);
	pXmlArea->SetAttribute("RegionType", m_usAreaType);
	pXmlArea->SetAttribute("RegionName", m_strAreaName.c_str());
	pXmlArea->SetAttribute("RegionShape", m_usAreaShape);

	// 音乐区域，增加属性
	if (m_usAreaType == 4 || m_usAreaType == 5)
	{
		pXmlArea->SetAttribute("MusicFile", m_strMusicFile.c_str());
		pXmlArea->SetAttribute("Volume", m_usVolume);
		pXmlArea->SetAttribute("MusicLoop", m_usMusicLoop);
	}

	/// 如果为矩形区域，则要添加属性
	if (nAreaShape == 0)
	{
		AreaPoint* pFirstPoint = m_AreaPointList[0];
		AreaPoint* pSecondPoint = m_AreaPointList[0];
		if (GetAreaPointNum() > 1)
		{
			pSecondPoint = m_AreaPointList[1];
		}

		int nXPos1 = pFirstPoint->nXPos;
		int nYPos1 = pFirstPoint->nYPos;
		int nXPos2 = pSecondPoint->nXPos;
		int nYPos2 = pSecondPoint->nYPos;

		int nLeft = (nXPos1 < nXPos2 ? nXPos1 : nXPos2);
		int nTop = (nYPos1 < nYPos2 ? nYPos1 : nYPos2);
		int nRight = (nXPos1 > nXPos2 ? nXPos1 : nXPos2);
		int nBottom = (nYPos1 > nYPos2 ? nYPos1 : nYPos2);

		pXmlArea->SetAttribute("Left", nLeft);
		pXmlArea->SetAttribute("Top", nTop);
		pXmlArea->SetAttribute("Right", nRight);
		pXmlArea->SetAttribute("Bottom", nBottom);
	}
	for (int index = 0;index < static_cast<int> (m_AreaPointList.size());++index)
	{
		TiXmlElement* pXmlAreaPoint = new TiXmlElement("RegionPoint");
		pXmlAreaPoint->SetAttribute("PointID", m_AreaPointList[index]->uiPointID);
		pXmlAreaPoint->SetAttribute("XPos", m_AreaPointList[index]->nXPos);
		pXmlAreaPoint->SetAttribute("YPos", m_AreaPointList[index]->nYPos);
		pXmlArea->LinkEndChild(pXmlAreaPoint);
	}

	return 0;
}

int CArea::GetAreaPointNum()
{
	return m_AreaPointList.size();
}
#endif