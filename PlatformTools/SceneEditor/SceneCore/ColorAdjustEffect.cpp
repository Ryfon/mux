﻿#include "StdAfx.h"
#include "ColorAdjustEffect.h"

#ifndef CODE_INGAME
using namespace SceneCore;

CColorAdjustEffect::CColorAdjustEffect(void)
:	m_spRTView(NULL),
m_pkRTViewTexProp(NULL),
m_spDrawScreenRTView(NULL),
m_spRTGroup(NULL),
m_spOutputRenderTarget(NULL),
m_spTexture(NULL),
m_spMtrl(NULL)
{
	m_stData.fBrightAdjustValue = 0.0f;
	m_stData.fContrastAdjustValue = 0.0f;
	m_stData.fHSLAdjustHValue = 0.0f;
	m_stData.fHSLAdjustSValue = 0.0f;
}
//---------------------------------------------------------------------------------------------
CColorAdjustEffect::~CColorAdjustEffect(void)
{
}
//---------------------------------------------------------------------------------------------
void	CColorAdjustEffect::_CreateScreenFillingRenderViews()
{
	//初始化 render to texture view
	m_spRTView = NiNew NiScreenFillingRenderView;

	m_spRTView->GetScreenFillingQuad().ApplyAndSetActiveMaterial( 0 );
	//初始化纹理属性
	m_pkRTViewTexProp = NiNew NiTexturingProperty;
	//设置纹理属性
	m_pkRTViewTexProp->SetBaseTexture( m_spSourceTexture );
	m_pkRTViewTexProp->SetBaseFilterMode( NiTexturingProperty::FILTER_BILERP );
	m_pkRTViewTexProp->SetApplyMode( NiTexturingProperty::APPLY_REPLACE );
	//添加属性到render to texture view
	m_spRTView->AttachProperty( m_pkRTViewTexProp );

	// Create the render-to-texture screen camera.  The render target must
	// be set before the creation of the camera since the camera grabs the
	// current renderer's back buffer width and height.

	// Create the render-to-backbuffer object.
	m_spDrawScreenRTView = NiNew NiScreenFillingRenderView;
	//设置纹理属性
	NiTexturingPropertyPtr m_pkTexProp = NiNew NiTexturingProperty;
	m_pkTexProp->SetBaseTexture( m_spTexture );
	m_pkTexProp->SetBaseFilterMode( NiTexturingProperty::FILTER_BILERP );
	m_pkTexProp->SetApplyMode( NiTexturingProperty::APPLY_REPLACE );
	m_spDrawScreenRTView->AttachProperty( m_pkTexProp );

	//设置alpha属性
	//NiAlphaPropertyPtr m_pkAlphaProp = NiNew NiAlphaProperty;
	//m_pkAlphaProp->SetAlphaBlending( true);
	//m_pkAlphaProp->SetSrcBlendMode( NiAlphaProperty::ALPHA_ONE );
	//m_pkAlphaProp->SetDestBlendMode( NiAlphaProperty::ALPHA_ONE );
	//m_spDrawScreenRTView->AttachProperty( m_pkAlphaProp );
}
//-----------------------------------------------------------------------------------------------------------
bool CColorAdjustEffect::_PrePostEffectCallback(NiRenderClick* pkCurrentRenderClick, void* pvCallbackData)
{
	CColorAdjustEffect* pkThis = (CColorAdjustEffect*)pvCallbackData;
	NIASSERT(pkThis);

	//设置调用shader
	pkThis->m_spRTView->GetScreenFillingQuad().ApplyAndSetActiveMaterial( pkThis->m_spMtrl );
	//传入源纹理 
	pkThis->m_pkRTViewTexProp->SetBaseTexture( pkThis->m_spSourceTexture );

	return true;
}
//------------------------------------------------------------------------------------------------------------
bool CColorAdjustEffect::Init(NiRenderedTexturePtr pkSourceTexture)
{
	if (!__super::Init( pkSourceTexture ))
	{
		return false;
	}

	//获得renderer
	NiRenderer* pkRender = NiRenderer::GetRenderer();

	m_uiWidth = pkSourceTexture->GetWidth();
	m_uiHeight = pkSourceTexture->GetHeight();


	//初始化目标纹理
	m_spSourceTexture = pkSourceTexture;
	m_spTexture = NiRenderedTexture::Create( m_uiWidth, m_uiHeight, pkRender );

	if ( m_spTexture == NULL || m_spSourceTexture == NULL )
	{
		return false;
	}

	//初始化render target group
	m_spOutputRenderTarget = NiRenderTargetGroup::Create( m_spSourceTexture->GetBuffer(), pkRender,true, true );
	m_spRTGroup= NiRenderTargetGroup::Create( m_spTexture->GetBuffer(), pkRender, true, true );
	NIASSERT(m_spOutputRenderTarget);
	NIASSERT(m_spRTGroup);


	//创建material 即shader
	m_spMtrl = NiSingleShaderMaterial::Create("HSL");

	NIASSERT(m_spMtrl);

	//初始化view
	_CreateScreenFillingRenderViews();
	NIASSERT(m_spRTView);

	//设置shader step
	//1.亮度调节
	NiViewRenderClick*	pkRenderClick = NiNew NiViewRenderClick;
	//使用临时view渲染，用于着色处理
	pkRenderClick->AppendRenderView(m_spRTView);
	//清除缓存
	pkRenderClick->SetClearAllBuffers(true);
	//设置RTG
	pkRenderClick->SetRenderTargetGroup(m_spRTGroup);
	//设置shader事件
	pkRenderClick->SetPreProcessingCallbackFunc(_PrePostEffectCallback, this);
	//添加step到renderstep
	m_spRenderStep->AppendRenderClick(pkRenderClick);


	//2.渲染到最终output view
	pkRenderClick = NiNew NiViewRenderClick;
	pkRenderClick->AppendRenderView(m_spDrawScreenRTView);
	pkRenderClick->SetClearAllBuffers(false);
	pkRenderClick->SetRenderTargetGroup( m_spOutputRenderTarget );
	pkRenderClick->SetPostProcessingCallbackFunc(_PreDrawSceneCallback, this);
	m_spRenderStep->AppendRenderClick(pkRenderClick);

	m_bInitialized = true;
	return true;
}
//--------------------------------------------------------------------------------------------------------------
void CColorAdjustEffect::UpdateData(float fBrightAdjust,float fContrast,float fHSLAdjustH,float fHSLAdjustS)
{
	m_stData.fBrightAdjustValue = fBrightAdjust;
	m_stData.fContrastAdjustValue = fContrast;
	m_stData.fHSLAdjustHValue = fHSLAdjustH;
	m_stData.fHSLAdjustSValue = fHSLAdjustS;
}
//--------------------------------------------------------------------------------------------------------------
bool CColorAdjustEffect::_PreDrawSceneCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData )
{
	CColorAdjustEffect* pkThis = (CColorAdjustEffect*)pvCallbackData;
	NIASSERT(pkThis);

	bool bSuccess = NiShaderFactory::UpdateGlobalShaderConstant( "gBrightness", sizeof(float), &pkThis->m_stData.fBrightAdjustValue);

	bSuccess = NiShaderFactory::UpdateGlobalShaderConstant( "gSaturation", sizeof(float), &pkThis->m_stData.fHSLAdjustSValue);

	bSuccess = NiShaderFactory::UpdateGlobalShaderConstant( "gContrast", sizeof(float), &pkThis->m_stData.fContrastAdjustValue);


	return true;
}
//--------------------------------------------------------------------------------------------------------------
void	CColorAdjustEffect::Destroy()
{
	m_spRTView = NULL;
	m_pkRTViewTexProp = NULL;
	m_spDrawScreenRTView = NULL;
	m_spRTGroup = NULL;
	m_spTexture = NULL;
	m_spMtrl = NULL;

	__super::Destory();
}
//--------------------------------------------------------------------------------------------------------------
const char* CColorAdjustEffect::GetName()
{
	return "ColorAdjust";
}
//---------------------------------------------------------------------------------------------------------

void CColorAdjustEffect::PerformRendering()
{
	NiShaderFactory::UpdateGlobalShaderConstant( "gBrightness", sizeof(float), &m_stData.fBrightAdjustValue);
	NiShaderFactory::UpdateGlobalShaderConstant( "gSaturation", sizeof(float), &m_stData.fHSLAdjustSValue);

	NiShaderFactory::UpdateGlobalShaderConstant( "gContrast", sizeof(float), &m_stData.fContrastAdjustValue);

	__super::PerformRendering();
}

#endif