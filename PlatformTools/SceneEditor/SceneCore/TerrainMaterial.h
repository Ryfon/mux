﻿/** @file TerrainMaterial.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-09-28
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 </pre>*/

#ifndef	TERRAIN_MTL_H 
#define TERRAIN_MTL_H

#define		BLENDTEX_SIZE		256
#define		MAX_LAYER			4

/// 材质类型定义
//enum MAIN_ENTRY EMaterialType 
//{	 
//	MT_GRASS,
//	MT_MORASS,
//	MT_SAND,
//	MT_UNKNOWN
//};
// 从流导入地形需要将所有所使用的纹理初始化，并为每个chunk填充此结构并传入导入函数
struct stChunkTextures
{
	int					iChunkID;			// chunk 索引
	NiSourceTexturePtr	pkBlendTexture;		// 混合纹理
	int					iUsedTextureLayer;	// 使用的纹理层数
	char				szFileNames[4][MAX_PATH];	// 四层纹理名称 不一定需要
	NiSourceTexturePtr	pTextures[4];		// 四层纹理
};

/**
 @brief 地表材质类，限制四层纹
 *
 * 限制四层纹理，通过一张带有RGB通道的贴图来混合 
 */
class MAIN_ENTRY CTerrainMaterial 
{
public:
	/// 构造
	CTerrainMaterial( const char *pszL0=0, const char *pszL1=0, const char *pszL2=0, const char *pszL3=0 );
	
	/// 析构
	virtual ~CTerrainMaterial(void);

	/// Blend Texture通道枚举
	enum EChannel
	{
		CHANNEL_R,
		CHANNEL_G,
		CHANNEL_B,
		CHANNEL_MAX
	};

	/// 获取Shader
	NiShader					*GetShader()
	{	return m_spShader;	}

	/// 获取材质
	NiSingleShaderMaterial		*GetMaterial()
	{	return m_spSSMaterial; }

public:

	/// 创建
	bool						Create(const char* pszBlendTexFile = NULL, const char* pszPropertyTexFile = NULL);

	/// 创建
	bool						Create(const stChunkTextures& pChunkTextueres);

	/// 销毁
	void						Destroy();

	/// 创建blend纹理
	bool						CreateBlendTexture( const char *pszTexFile = NULL);

	/// 设置层纹理 返回当前使用的纹理层数
	int							SetTexture( const string& strTexFile, bool& bAddLayer);

	/// 已使用纹理层数统计
	const int					GetUsedLayerCount() const {	return m_iUsedTexLayer;	}

	/// 当前正使用的纹理层
	const int					GetInUseLayer() const {return m_iTexIndex;}

	/// 由层索引获得纹理
	NiSourceTexturePtr			GetTexture( int iLayer )
	{
		if ( iLayer >=0 && iLayer < MAX_LAYER )
			return m_pLayerTexture[iLayer];

		return 0;
	}

	const string&				GetTextureFile( int iLayer )
	{
		assert( iLayer >= 0 && iLayer < MAX_LAYER );
		return m_strTexFiles[iLayer]; 
	}

	/// 获取Blend纹理
	NiSourceTexturePtr				GetBlendTexture()
	{	return m_spBlendTexture;	}

	/// 获取Blend纹理大小
	const int					GetBlendTexSize() const
	{ return BLENDTEX_SIZE; }

	friend class CTerrain;
/************************************************
*	这里是只在编辑器中使用，游戏中不使用的函数
*************************************************/
#ifndef CODE_INGAME

public:	
	/// 替换第iLayer层纹理
	bool						ReplaceTexture(int iLayer, const string& strTexFile);

	// 删除iLayer层纹理 [5/4/2009 hemeng]
	bool						DeleteTexture(int iLayer);

	/**
	*	<BR>功能说明：			设置Blend数据,R通道
	*	<BR>可访问性：
	*	<BR>注    释：
	*	@param	vPos[in]		2D纹理坐标位置
	*	@param	fAlpha[in]		调整到的值
	*	@param	fRadius0[in]	影响半径(小)
	*	@param	fRadius1[in]	影响半径(大)
	*	@return 无
	*/
	void						SetBlendDataR( const NiPoint2& vPos, float fAlpha, float fRadius0, float fRadius1, 
														bool bAddTo = false, NiSourceTexture* pkBrushTexture = NULL);
	/// 设置blend纹理数据,G通道
	void						SetBlendDataG( const NiPoint2& vPos, float fAlpha, float fRadius0, float fRadius1, 
														bool bAddTo = false, NiSourceTexture* pkBrushTexture = NULL);
	/// 设置blend纹理数据,B通道
	void						SetBlendDataB( const NiPoint2& vPos, float fAlpha, float fRadius0, float fRadius1, 
														bool bAddTo = false, NiSourceTexture* pkBrushTexture = NULL);

	void						SetTerrainPropertyA( const NiRect<float> &rect, int iProperty );

	///	获取4层纹理的最终混合结果纹理 函数作废。保留供参考
	NiSourceTexturePtr			GetTextureBlendResult();

	/// 保存blend纹理数据,仅供测试
	bool						SaveBlendTexture( const char *pszFile );

	/// 清除所有碰撞信息
	void						ClearCollisionDataA();

	/// 获取属性纹理
	NiSourceTexturePtr				GetPropertyTexture()
	{	return m_spPropertyTexture;	}

private:
	void						_doSetBlendData( int iChannel, const NiPoint2& vPos, float fAlpha, 
											float fr, float fR, bool bAddTo, NiSourceTexture* pkBrushTexture = NULL);

	/// 创建 property texture
	bool						_CreatePropertyTexture( const char *pszTexFile = NULL);

	/// Property 纹理 只在编辑器中使用
	NiSourceTexturePtr			m_spPropertyTexture;
#endif

private:

	/// Shader
	NiShaderPtr					m_spShader;

	/// 材质
	NiSingleShaderMaterial		*m_spSSMaterial;

	/// Blend 纹理
	NiSourceTexturePtr			m_spBlendTexture;


	/// 材质类型
	//EMaterialType				m_eMtlType;

	/// 纹理文件名
	string						m_strTexFiles[MAX_LAYER];

	/// 纹理数组
	NiSourceTexturePtr			m_pLayerTexture[MAX_LAYER];

	/// 当前编辑纹理层索引
	int							m_iTexIndex;

	/// 当前使用的纹理层数
	int							m_iUsedTexLayer;
};

#pragma make_public(CTerrainMaterial)

#endif