﻿#include "StdAfx.h"
#ifndef CODE_INGAME
#include "SceneDesignerConfig.h"
#include <tinyxml.h>

CSceneDesignerConfig::CSceneDesignerConfig(void)
:	m_fCameraFOV(45),
	m_fPlayerToCamera(22.5)
{
	_LoadFromXml();
	//SaveToXml();
}

CSceneDesignerConfig::~CSceneDesignerConfig(void)
{

}

CSceneDesignerConfig* CSceneDesignerConfig::GetInstance()
{
	static CSceneDesignerConfig sInstance;
	//if (NULL == ms_pInstance)
	//{
	//	ms_pInstance = new CSceneDesignerConfig;
	//	ms_pInstance->_LoadFromXml();
	//}

	return &sInstance;
}

bool CSceneDesignerConfig::AddTerrainSurfaceProperty(const stTerrainSurfaceProperty& terrProperty)
{
	// 名称和ID均无重复，才可以添加
	if (_FindTerrainSurfaceProperty(terrProperty.szName, terrProperty.iID) == m_TerrainSurfacePropertyList.end())
	{
		m_TerrainSurfacePropertyList.push_back(terrProperty);
		SaveToXml();
		return true;
	}
	else
	{
		return false;
	}
}

bool CSceneDesignerConfig::RemoveTerrainSurfaceProperty(const char* pszPropertyName)
{
	vector<stTerrainSurfaceProperty>::iterator iter = _FindTerrainSurfaceProperty(pszPropertyName, -1);
	if (iter != m_TerrainSurfacePropertyList.end())
	{
		m_TerrainSurfacePropertyList.erase(iter);
		SaveToXml();
		return true;
	}
	else
	{
		return false;
	}

}

bool CSceneDesignerConfig::SetTerrainSurfacePropertyColor(const char* pszPropertyName, const NiColor& kColor)
{
	vector<stTerrainSurfaceProperty>::iterator iter = _FindTerrainSurfaceProperty(pszPropertyName, -1);
	if (iter != m_TerrainSurfacePropertyList.end())
	{
		(*iter).kColor = kColor;
		SaveToXml();
		return true;
	}
	else
	{
		return false;
	}
}

vector<stTerrainSurfaceProperty>& CSceneDesignerConfig::GetTerrainSurfacePropertyList()
{
	return m_TerrainSurfacePropertyList;
}

/// 获取地表属性数量
int CSceneDesignerConfig::GetNumTerrainSurfaceProerties()
{
	return m_TerrainSurfacePropertyList.size();
}

/// 按顺序获取地表属性
stTerrainSurfaceProperty& CSceneDesignerConfig::GetTerrainSurfaceProperty(unsigned int iNum)
{
	NIASSERT(iNum>-1 && iNum<m_TerrainSurfacePropertyList.size());
	vector<stTerrainSurfaceProperty>::iterator iter = m_TerrainSurfacePropertyList.begin();
	for (unsigned int i=0; i<iNum; i++)
	{
		iter++;
	}
	return *iter;
}

vector<stTerrainSurfaceProperty>::iterator CSceneDesignerConfig::_FindTerrainSurfaceProperty(const char* pszPropertyName, int iID)
{
	vector<stTerrainSurfaceProperty>::iterator iter = m_TerrainSurfacePropertyList.begin();
	while (iter != m_TerrainSurfacePropertyList.end())
	{
		// 名称相同 或 ID 相同
		if (strcmp((*iter).szName, pszPropertyName)==0 || (*iter).iID==iID)
		{
			return iter;
		}
		iter++;
	}
	return iter;
}

void CSceneDesignerConfig::GetCameraParam(float& fCamFOV, float& fPlayerToCamera)
{
	fCamFOV = m_fCameraFOV;
	fPlayerToCamera = m_fPlayerToCamera;
}

const vector<string>& CSceneDesignerConfig::GetEntityCategoryList() const
{
	return m_EntityCategoryList;
}

const char* CSceneDesignerConfig::GetTemplateViewName(string strFileName)
{
	map<string, string>::iterator iter = m_mapTypeName2TemplateName.find(strFileName);
	if (iter != m_mapTypeName2TemplateName.end())
	{
		return iter->second.c_str();
	}
	else
	{
		return NULL;
	}
}

NiSearchPath*	CSceneDesignerConfig::GetTextureSearchPath()
{
	return &m_TextureSearchPath;
}

bool CSceneDesignerConfig::SaveToXml()
{
	TiXmlDocument* pDoc = new TiXmlDocument(CONFIG_FILE_XML);
	TiXmlElement* pElmRoot = new TiXmlElement("Root");
	{
		// 写摄像机信息
		TiXmlElement* pElmtCameraInfo = new TiXmlElement("CameraInfo");
		pElmtCameraInfo->SetDoubleAttribute("FOV", m_fCameraFOV);
		pElmtCameraInfo->SetDoubleAttribute("PlayerToCam", m_fPlayerToCamera);
		pElmRoot->LinkEndChild(pElmtCameraInfo);
		
		// 写地表属性种类信息
		TiXmlElement* pElmtTerrSurPropRoot = new TiXmlElement("TerrainSurfaceProperty");
		pElmtTerrSurPropRoot->SetAttribute("NumProperties", m_TerrainSurfacePropertyList.size());
		{	
			// all property
			vector<stTerrainSurfaceProperty>::iterator iter = m_TerrainSurfacePropertyList.begin();
			while (iter != m_TerrainSurfacePropertyList.end())
			{
				stTerrainSurfaceProperty& terrProp = (*iter);
				TiXmlElement* pElmtTerrProp = new TiXmlElement("TerrainProperty");
				pElmtTerrProp->SetAttribute("ID", terrProp.iID);
				pElmtTerrProp->SetAttribute("NAME", terrProp.szName);
				pElmtTerrProp->SetDoubleAttribute("R", terrProp.kColor.r);
				pElmtTerrProp->SetDoubleAttribute("G", terrProp.kColor.g);
				pElmtTerrProp->SetDoubleAttribute("B", terrProp.kColor.b);
				pElmtTerrSurPropRoot->LinkEndChild(pElmtTerrProp );
				iter++;
			}
		}
		pElmRoot->LinkEndChild(pElmtTerrSurPropRoot);

		// 写 entity category 信息
		TiXmlElement* pElmtEntityCategoryRoot = new TiXmlElement("EntityCategoryList");
		pElmtEntityCategoryRoot->SetAttribute("NumCategories", m_EntityCategoryList.size());
		{
			for (unsigned int i=0; i<m_EntityCategoryList.size(); i++)
			{
				TiXmlElement* pElmtTerrProp = new TiXmlElement("EntityCategory");
				pElmtTerrProp->SetAttribute("NAME", m_EntityCategoryList[i].c_str());
				pElmtEntityCategoryRoot->LinkEndChild(pElmtTerrProp);
			}
		}
		pElmRoot->LinkEndChild(pElmtEntityCategoryRoot);
	}

	pDoc->LinkEndChild(pElmRoot);
	pDoc->SaveFile();
	delete pDoc;

	return true;
}

bool CSceneDesignerConfig:: _LoadFromXml()
{
	m_TerrainSurfacePropertyList.clear();
	m_EntityCategoryList.clear();

	// 从文件载入 xml doc
	TiXmlDocument* pDoc = new TiXmlDocument;
	pDoc->LoadFile(CONFIG_FILE_XML);

	TiXmlElement* pElmtRoot = pDoc->FirstChildElement();

	TiXmlElement* pElmtSecondRoot = pElmtRoot->FirstChildElement();
	while (pElmtSecondRoot != NULL)
	{
		//const char* value = pElmtSecondRoot->Value();
		if (strcmp(pElmtSecondRoot->Value(), "CameraInfo") == 0)
		{
			// 读取摄象机参数
			TiXmlElement* pElmtCamParam = pElmtSecondRoot;
			pElmtCamParam->QueryFloatAttribute("FOV", &m_fCameraFOV);
			pElmtCamParam->QueryFloatAttribute("PlayerToCam", &m_fPlayerToCamera);
		}
		else if(strcmp(pElmtSecondRoot->Value(), "TerrainSurfaceProperty") == 0)
		{
			// 读取地形表面属性
			TiXmlElement* pElmtTerrPropRoot = pElmtSecondRoot;
			TiXmlElement* pElmtTerrProp = pElmtTerrPropRoot->FirstChildElement();
			while (pElmtTerrProp != NULL)
			{
				stTerrainSurfaceProperty terrProp;
				pElmtTerrProp->Attribute("ID", &(terrProp.iID));
				const char* pszName = pElmtTerrProp->Attribute("NAME");
				strcpy_s(terrProp.szName, pszName);
				pElmtTerrProp->QueryFloatAttribute("R", &(terrProp.kColor.r));
				pElmtTerrProp->QueryFloatAttribute("G", &(terrProp.kColor.g));
				pElmtTerrProp->QueryFloatAttribute("B", &(terrProp.kColor.b));
				m_TerrainSurfacePropertyList.push_back(terrProp);

				pElmtTerrProp = pElmtTerrProp->NextSiblingElement();
			}
		}
		//081007 add by 和萌
		//纹理区域属性
		else if (strcmp(pElmtSecondRoot->Value(), "PropertyTextures") == 0 )
		{
			TiXmlElement* pElmtMtlRegProRoot = pElmtSecondRoot;
			while (pElmtMtlRegProRoot != NULL)
			{
				TiXmlElement* pElmtMtlProID = pElmtMtlRegProRoot->FirstChildElement();
				while (pElmtMtlProID != NULL)
				{
					int iProID= 0;				
					pElmtMtlProID->Attribute("id",&iProID);
					TiXmlElement* pElmtMtlName = pElmtMtlProID->FirstChildElement();
					while (pElmtMtlName != NULL)
					{
						const char* pszName = pElmtMtlName->Value();
						if ( strcmp(pszName,"Texture") == 0)
						{
							string strMtlName = pElmtMtlName->Attribute("FILE");
							m_MtlRegionProperty.mapMtlRegPro.insert(map<string,int>::value_type(strMtlName,iProID));
							pElmtMtlName = pElmtMtlName->NextSiblingElement();
						}
						else if (strcmp(pszName,"ModleName") == 0)
						{
							string strMtlName = pElmtMtlName->Attribute("ENTITYNAME");
							m_MtlRegionProperty.mapMtlRegPro.insert(map<string,int>::value_type(strMtlName,iProID));
							pElmtMtlName = pElmtMtlName->NextSiblingElement();
						}
						
					}
					pElmtMtlProID = pElmtMtlProID->NextSiblingElement();
				}
				pElmtMtlRegProRoot = pElmtMtlRegProRoot->NextSiblingElement();
			} 
		}

		else if(strcmp(pElmtSecondRoot->Value(), "EntityCategoryList") == 0)
		{
			// 读取 Entity Category
			TiXmlElement* pElmtEntityCategoryRoot = pElmtSecondRoot;
			TiXmlElement* pElmtEntityCategory = pElmtEntityCategoryRoot->FirstChildElement();
			while (pElmtEntityCategory != NULL)
			{
				string strName = pElmtEntityCategory->Attribute("NAME");
				m_EntityCategoryList.push_back(strName);
				pElmtEntityCategory = pElmtEntityCategory->NextSiblingElement();
			}
		}
		else if(strcmp(pElmtSecondRoot->Value(), "TemplateNameList") == 0)
		{
			// 读取 file name/template name 对应列表
			TiXmlElement* pElmtTemplateNameRoot = pElmtSecondRoot;
			TiXmlElement* pElmtTemplateName = pElmtTemplateNameRoot->FirstChildElement();
			while (pElmtTemplateName != NULL)
			{
				string strFileName = pElmtTemplateName->Attribute("TypeName");
				string strViewName = pElmtTemplateName->Attribute("ViewName");
				m_mapTypeName2TemplateName[strFileName] = strViewName;
				pElmtTemplateName = pElmtTemplateName->NextSiblingElement();
			}
		}
		else if(strcmp(pElmtSecondRoot->Value(), "TexturePathList") == 0)
		{
			// 读取 搜索路径列表
			TiXmlElement* pElmtTexturePathRoot = pElmtSecondRoot;
			TiXmlElement* pElmtTextuerPath = pElmtTexturePathRoot->FirstChildElement();
			while (pElmtTextuerPath != NULL)
			{
				string strTexturePath = pElmtTextuerPath->Attribute("Path");
				m_TextureSearchPath.AddSearchPath(strTexturePath, strTexturePath.size());
				pElmtTextuerPath = pElmtTextuerPath->NextSiblingElement();
				m_TexturePathList.push_back(strTexturePath);
			}
			m_TextureSearchPath.SetDefaultPath(m_TexturePathList[0].c_str());
		}
		else if(strcmp(pElmtSecondRoot->Value(), "RegionPropertyList") == 0)
		{
			// 读取 区域属性
			TiXmlElement* pElmtRegionPropertyRoot = pElmtSecondRoot;
			TiXmlElement* pElmtRegionProperty = pElmtRegionPropertyRoot->FirstChildElement();
			while (pElmtRegionProperty != NULL)
			{
				stRegionProperty regionProp;
				pElmtRegionProperty->Attribute("TYPE", &(regionProp.nType));

				string strTempName = pElmtRegionProperty->Attribute("NAME");
				regionProp.strName = strTempName;

				string strTempViewName = pElmtRegionProperty->Attribute("VIEWNAME");
				regionProp.strViewName = strTempViewName;

				pElmtRegionProperty->Attribute("SHAPE", &(regionProp.nShape));

				pElmtRegionProperty->Attribute("R", &(regionProp.nColorR));
				pElmtRegionProperty->Attribute("G", &(regionProp.nColorG));
				pElmtRegionProperty->Attribute("B", &(regionProp.nColorB));
				pElmtRegionProperty->Attribute("A", &(regionProp.nColorA));

				pElmtRegionProperty = pElmtRegionProperty->NextSiblingElement();
				m_RegionPropertyList.push_back(regionProp);
			}
		}
		pElmtSecondRoot = pElmtSecondRoot->NextSiblingElement();
	}
	pDoc->Clear();
	

	return true;
}

string CSceneDesignerConfig::GetRegionPropViewName(int nIndex)
{
	string strResult;
	int nSize = static_cast<int>(m_RegionPropertyList.size());
	if (nIndex < 0 || nIndex >= nSize)
	{
		return strResult;
	}
	return m_RegionPropertyList[nIndex].strViewName;
}

stRegionProperty* CSceneDesignerConfig::GetRegionPropByIndex(int nIndex)
{
	int nSize = static_cast<int>(m_RegionPropertyList.size());

	if (nIndex < 0 || nIndex >= nSize)
	{
		return NULL;
	}
	return &m_RegionPropertyList[nIndex];
}

int CSceneDesignerConfig::GetRegionShapeByType(int nType)
{
	vector<stRegionProperty>::iterator it = m_RegionPropertyList.begin();

	for (;it != m_RegionPropertyList.end();it++)
	{
		if ((*it).nType == nType)
		{
			return (*it).nShape;
		}
	}

	return -1;
}
int CSceneDesignerConfig::GetRegionShapyeByMaterial(const string strMtlName)
{
	map<string,int>::iterator it = m_MtlRegionProperty.mapMtlRegPro.begin();

	for(; it != m_MtlRegionProperty.mapMtlRegPro.end(); it++)
	{
		if (it->first == strMtlName)
		{
			return it->second;
		}
	}
	return -1;
}

int CSceneDesignerConfig::GetRegionColorByType(int nType, int nColorType)
{
	vector<stRegionProperty>::iterator it = m_RegionPropertyList.begin();

	for (;it != m_RegionPropertyList.end();it++)
	{
		if ((*it).nType == nType)
		{
			switch(nColorType)
			{
			case 1:
				return (*it).nColorR;
				break;
			case 2:
				return (*it).nColorG;
				break;
			case 3:
				return (*it).nColorB;
			    break;
			case 4:
				return (*it).nColorA;
			    break;
			default:
				return 0;
			    break;
			}
		}
	}

	return 0;
}

#endif