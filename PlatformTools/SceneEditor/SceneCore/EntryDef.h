﻿#pragma once

#ifdef SCENE_CORE_EXPORT
    // DLL library project uses this
    #define MAIN_ENTRY __declspec(dllexport)
#else
#ifdef SCENE_CORE_IMPORT
    // client of DLL uses this
    #define MAIN_ENTRY __declspec(dllimport)
#else
    // static library project uses this
    #define MAIN_ENTRY
#endif
#endif
