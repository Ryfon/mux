﻿/** @file NiLightMapMaterialHelper.h 
@brief 给 nif 文件赋以 NiLightMapMaterial 的辅助函数集合, 
		包括展 uv, 合并 geometry, 合并 light map 等功能
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2009-09-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef NILIGHTMAPMATERIALHELPER_H
#define NILIGHTMAPMATERIALHELPER_H

#ifndef CODE_INGAME
// 这个类只在编辑器中时候,客户端中不使用

#include <NiMain.h>
#include <d3dx9.h>


// NiStandardMaterial 名称
#define STANDARD_MATERIAL_NAME "NiStandardMaterial"
#define LIGHT_MAP_MATERIAL_NAME "LightMapMaterial"


/// 辅助类, 静态函数集合
class MAIN_ENTRY NiLightMapMaterialHelper
{
	// 顶点结构
	struct sMeshVert
	{
		D3DXVECTOR3 vPos;
		D3DXVECTOR2 vUV;
	};

public:
	/// 将 NiNode 下所有 NiGeometry 转换为 light map material
	static NiNode* ConvertToLightMapMaterial(NiNode* pkOrigNode, int& iNumGeomConv, int& iNumGeomFail, std::vector<string>& arrInfo);

	/// 判断一个 geometry 是否可以转换为 light map material
	static bool VerifyToLightMapMaterial(NiGeometry* pkGeomchar, char szInfo[256]);

	/// 为 pkGeom 添加一套新 uv, 返回新 uv id
	static int AddUVSet(NiGeometry* pkGeom);

	/// 展平 pkGeom 的第 uiID 套 UV (用 dx 的函数)
	static NiTriShape* FlatUV(NiGeometry* pkGeom, unsigned int uiID);

	/// 将 NiGeometry 转换到 D3D Mesh, 只拷贝顶点.
	static LPD3DXMESH ConvertToD3DMesh(NiGeometry* pkGeom);

	/// 将 D3DMesh 的 uv 复制给 pkGeom
	static NiTriShape* ApplyUVToGeometry(LPD3DXMESH pD3DMesh, NiGeometry* pkGeom, DWORD* pRemapArray, unsigned int uiID);

	// 添加 shader map 0 到 geometry 的 texturing property
	static void AddShaderMap(NiGeometry* pkGeom, int iTexSetID);

protected:

};

#endif	// CODE_INGAME
#endif	// NILIGHTMAPMATERIALHELPER_H