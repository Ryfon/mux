﻿/** @file LayerFogComponent.h 
@brief Layer fog entity component
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-04-19
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#include <NiRefObject.h>
#include <NiSmartPointer.h>
#include <NiEntityComponentInterface.h>
#include <NiFlags.h>
#include <NiNode.h>
//#include <NiGeometry.h>
#include <NiTriShape.h>
#include <NiFactories.h>
#include <NiEntityInterface.h>
#include <NiEntityErrorInterface.h>
#include "LayerFog.h"

namespace SceneCore
{

NiSmartPointer(CLayerFogComponent);

class MAIN_ENTRY CLayerFogComponent : public NiRefObject,
	public NiEntityComponentInterface
{
	NiDeclareFlags(unsigned char);

public:
	CLayerFogComponent(NiAVObjectPtr pkSceneRoot = NULL);
	virtual ~CLayerFogComponent(void);

	static NiFixedString ClassName();

	// *** begin Emergent internal use only ***
	static void _SDMInit();
	static void _SDMShutdown();
	// *** end Emergent internal use only ***

	/// 获取层雾实例
	CLayerFogPtr GetLayerFogInstance() {return m_spFogInstance;}

	/// 设置雾的透明度
	float GetAlpha();

	/// 设置雾的颜色
	NiColor GetColor();

	/// 判断一个 entity 是否 Layer Fog
	static bool EntityIsLayerFog(NiEntityInterface* pkEntity);

protected:

	// 报错字符串
	static NiFixedString ERR_TRANSLATION_NOT_FOUND;
	static NiFixedString ERR_ROTATION_NOT_FOUND;
	static NiFixedString ERR_SCALE_NOT_FOUND;
	static NiFixedString ERR_FILE_LOAD_FAILED;

	// Class name.
	static NiFixedString ms_kClassName;

	// Component name.
	static NiFixedString ms_kComponentName;

	// Property names.
	static NiFixedString ms_kColorName;
	static NiFixedString ms_kAlphaName;
	static NiFixedString ms_kNifFilePathName;
	static NiFixedString ms_kSceneRootPointerName;

	// Property descriptions.
	static NiFixedString ms_kColorDescription;
	static NiFixedString ms_kAlphaDescription;
	static NiFixedString ms_kNifFilePathDescription;
	static NiFixedString ms_kSceneRootPointerDescription;

	// Dependent property names.
	static NiFixedString ms_kTranslationName;
	static NiFixedString ms_kRotationName;
	static NiFixedString ms_kScaleName;

	// properties

	CLayerFogPtr			m_spFogInstance;		// 层雾实例
	CLayerFogComponentPtr	m_spMasterComponent;
	NiFixedString			m_spNifFilePath;		// nif 文件路径
	bool					m_bNifFileChanged;		// nif 文件路径是否改变，需要重新加载 nif

public:
	// NiEntityComponentInterface overrides.
	virtual NiEntityComponentInterface* Clone(bool bInheritProperties);
	virtual NiEntityComponentInterface* GetMasterComponent();
	virtual void SetMasterComponent(
		NiEntityComponentInterface* pkMasterComponent);
	virtual void GetDependentPropertyNames(
		NiTObjectSet<NiFixedString>& kDependentPropertyNames);

	// NiEntityPropertyInterface overrides.
	virtual NiBool SetTemplateID(const NiUniqueID& kID);
	virtual NiUniqueID GetTemplateID();
	virtual void AddReference();
	virtual void RemoveReference();
	virtual NiFixedString GetClassName() const;
	virtual NiFixedString GetName() const;
	virtual NiBool SetName(const NiFixedString& kName);
	virtual NiBool IsAnimated() const;
	virtual void Update(NiEntityPropertyInterface* pkParentEntity,
		float fTime, NiEntityErrorInterface* pkErrors,
		NiExternalAssetManager* pkAssetManager);
	virtual void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext,
		NiEntityErrorInterface* pkErrors);
	virtual void GetPropertyNames(
		NiTObjectSet<NiFixedString>& kPropertyNames) const;
	virtual NiBool CanResetProperty(const NiFixedString& kPropertyName,
		bool& bCanReset) const;
	virtual NiBool ResetProperty(const NiFixedString& kPropertyName);
	virtual NiBool MakePropertyUnique(const NiFixedString& kPropertyName,
		bool& bMadeUnique);
	virtual NiBool GetDisplayName(const NiFixedString& kPropertyName,
		NiFixedString& kDisplayName) const;
	virtual NiBool SetDisplayName(const NiFixedString& kPropertyName,
		const NiFixedString& kDisplayName);
	virtual NiBool GetPrimitiveType(const NiFixedString& kPropertyName,
		NiFixedString& kPrimitiveType) const;	// property 是什么类型的
	virtual NiBool SetPrimitiveType(const NiFixedString& kPropertyName,
		const NiFixedString& kPrimitiveType);
	virtual NiBool GetSemanticType(const NiFixedString& kPropertyName,
		NiFixedString& kSemanticType) const;
	virtual NiBool SetSemanticType(const NiFixedString& kPropertyName,
		const NiFixedString& kSemanticType);
	virtual NiBool GetDescription(const NiFixedString& kPropertyName,
		NiFixedString& kDescription) const;
	virtual NiBool SetDescription(const NiFixedString& kPropertyName,
		const NiFixedString& kDescription);
	virtual NiBool GetCategory(const NiFixedString& kPropertyName,
		NiFixedString& kCategory) const;
	virtual NiBool IsPropertyReadOnly(const NiFixedString& kPropertyName,
		bool& bIsReadOnly);
	virtual NiBool IsPropertyUnique(const NiFixedString& kPropertyName,
		bool& bIsUnique);
	virtual NiBool IsPropertySerializable(const NiFixedString& kPropertyName,
		bool& bIsSerializable);
	virtual NiBool IsPropertyInheritable(const NiFixedString& kPropertyName,
		bool& bIsInheritable);
	virtual NiBool IsExternalAssetPath(const NiFixedString& kPropertyName,
		unsigned int uiIndex, bool& bIsExternalAssetPath) const;
	virtual NiBool GetElementCount(const NiFixedString& kPropertyName,
		unsigned int& uiCount) const;
	virtual NiBool SetElementCount(const NiFixedString& kPropertyName,
		unsigned int uiCount, bool& bCountSet);
	virtual NiBool IsCollection(const NiFixedString& kPropertyName,
		bool& bIsCollection) const;
	// 设置/获取颜色
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, const NiColor& kData, unsigned int uiIndex = 0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, NiColor& kData, unsigned int uiIndex = 0) const;
	// 设置/获取 alpha 
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex = 0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex = 0) const;
	// 设置/获取 nif 文件路径
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName,
		NiFixedString& kData, unsigned int uiIndex=0) const;
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName,
		const NiFixedString& kData, unsigned int uiIndex=0);
	// 设置/获取 场景根
	virtual NiBool SetPropertyData(const NiFixedString& kPropertyName,
		NiObject*& pkData, unsigned int uiIndex=0);
	virtual NiBool GetPropertyData(const NiFixedString& kPropertyName,
		NiObject*& pkData, unsigned int uiIndex=0) const;
};

};

#pragma make_public(SceneCore::CLayerFogComponent)