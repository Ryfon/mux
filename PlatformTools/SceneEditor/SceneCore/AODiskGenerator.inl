#ifndef CODE_INGAME
//-----------------------------------------------------------------------------------------------
// 计算三角形面积
inline float CAODiskGenerator::TriangleArea(const NiPoint3& v0, const NiPoint3& v1, const NiPoint3& v2)
{
	// 秦九韶公式
	//float fA = (v1-v2).SqrLength();
	//float fB = (v0-v1).SqrLength();
	//float fC = (v0-v2).SqrLength();
	//float fD = (fA*fA+fC*fC-fB*fB)*(fA*fA+fC*fC-fB*fB)*0.25f;

	//float fArea = sqrt(0.25f*(fA*fA*fC*fC - fD));
	//return fArea;

	float a = (v0-v1).Length();
	float b = (v1-v2).Length();
	float c = (v2-v0).Length();
	float p = (a+b+c)*0.5f;
	return sqrt(p*(p-a)*(p-b)*(p-c));
}
//-----------------------------------------------------------------------------------------------

#endif