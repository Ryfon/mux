﻿#include "StdAfx.h"
#include "IDepthAlphaEffect.h"

using namespace SceneCore;

NiImplementRTTI(IDepthAlphaEffect, NiAVObject);

IDepthAlphaEffect::IDepthAlphaEffect(void)
{}
//------------------------------------------------------------------------
IDepthAlphaEffect::~IDepthAlphaEffect(void)
{}
//------------------------------------------------------------------------
/// 设置深度纹理
void IDepthAlphaEffect::SetDepthTexture(NiTexturePtr pkDepthTexture)
{
	//m_pkDepthTexture = pkDepthTexture;
}
//------------------------------------------------------------------------
/// 销毁 释放占用资源，智能指针置 NULL
void IDepthAlphaEffect::Destory()
{
	//m_pkDepthTexture = NULL;
}
//------------------------------------------------------------------------
/// 更新
void IDepthAlphaEffect::Update(float fTime)
{}
//------------------------------------------------------------------------
///构造可视集
void IDepthAlphaEffect::BuildVisibleSet(NiEntityRenderingContextPtr	pkRenderingCondtex)
{}
//------------------------------------------------------------------------
