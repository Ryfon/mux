﻿/** @file VertxColorImportor.h 
@brief 导出由max导出的着色顶点
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2008-06-16
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：本类只在场景编辑器中使用
</pre>*/
#pragma once

#include <fstream>
#include "Terrain.h"

#ifndef CODE_INGAME
#define NUMVERTEX		4225
#define NUMCHUNKVERTEX	65

//class CTerrain;

namespace SceneCore
{
	
class MAIN_ENTRY CVertxColorImportor
{
public:
	CVertxColorImportor(void);
	~CVertxColorImportor(void);

	struct	entityStru
	{
		int begin;
		int end;
	};

	//定义一个容器数组，用于存放scene.dat中的各顶点颜色
	map< int, map<int,NiColorA>* >	m_geoMap;

	int m_numEntity;


	//初始化
	bool InitialScene(const char* strSceneFile);
	bool InitialEntity(const char *strEntityFile);
	
	//定义一个容器，用于存放EntityVertRange.txt中的entity和地形信息 iBegin和iEnd
	vector<struct entityStru> entityInfo;

	//替换场景中的顶点颜色
	void SetVertexColor(NiAVObject* pkAVObj,int &ibegin,int iEnd);
	void SetTerrainVertexColor( CTerrain * pkTerrain,int &iBegin);



};//End of Class

}; // End of namespace
#endif