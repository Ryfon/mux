﻿#include "StdAfx.h"
#ifndef CODE_INGAME
#include "SceneFile.h"

/// 保存
bool	CSceneFile::Save()
{ 
	TRY_BEGIN
		FILE *fp;	errno_t err;
	if( (err = fopen_s( &fp,m_strFileName.c_str(), "w" )) != 0 )
		return false;

	fclose( fp );
	TRY_END_HANDLE( ERROR_R("保存场景文件失败!", false ) );

	return true;
}

/// 加载
bool	CSceneFile::Load( CGameScene* )
{
	TRY_BEGIN
		FILE *fp;	errno_t err;
	if( (err = fopen_s( &fp,m_strFileName.c_str(), "w" )) != 0 )
		return false;

	fclose( fp );
	TRY_END_HANDLE( ERROR_R("保存场景文件失败!", false ) );

	return true;
}

#endif