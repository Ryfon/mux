﻿#include "StdAfx.h"
#include "WaterEntityComponent.h"
#include "WaterManager.h"
#include "TerrainTextureMgr.h"

using namespace SceneCore;

NiFixedString CWaterEntityComponent::ERR_TRANSLATION_NOT_FOUND;
NiFixedString CWaterEntityComponent::ERR_ROTATION_NOT_FOUND;
NiFixedString CWaterEntityComponent::ERR_SCALE_NOT_FOUND;
NiFixedString CWaterEntityComponent::ERR_FILE_LOAD_FAILED;

NiFixedString CWaterEntityComponent::ms_kClassName;

// Component name.
NiFixedString CWaterEntityComponent::ms_kComponentName;

// Property names.
NiFixedString CWaterEntityComponent::ms_kColorName;
NiFixedString CWaterEntityComponent::ms_kAlphaName;
NiFixedString CWaterEntityComponent::ms_kBGSkyTextureName;
NiFixedString CWaterEntityComponent::ms_kReflectTexSizeName;
NiFixedString CWaterEntityComponent::ms_kBumpTextureName;
NiFixedString CWaterEntityComponent::ms_kBumpRepeatName;
NiFixedString CWaterEntityComponent::ms_kBumpSpeedName;
NiFixedString CWaterEntityComponent::ms_kSceneRootPointerName;

// Property descriptions.
NiFixedString CWaterEntityComponent::ms_kColorDescription;
NiFixedString CWaterEntityComponent::ms_kAlphaDescription;
NiFixedString CWaterEntityComponent::ms_kBGSkyTextureDescription;
NiFixedString CWaterEntityComponent::ms_kReflectTexSizeDescription;
NiFixedString CWaterEntityComponent::ms_kBumpTextureDescription;
NiFixedString CWaterEntityComponent::ms_kBumpRepeatDescription;
NiFixedString CWaterEntityComponent::ms_kBumpSpeedDescription;
NiFixedString CWaterEntityComponent::ms_kSceneRootPointerDescription;

// Dependent property names.
NiFixedString CWaterEntityComponent::ms_kTranslationName;
NiFixedString CWaterEntityComponent::ms_kRotationName;
NiFixedString CWaterEntityComponent::ms_kScaleName;

// Declare classes
NiFactoryDeclarePropIntf(CWaterEntityComponent);

void CWaterEntityComponent::_SDMInit()
{
	ERR_TRANSLATION_NOT_FOUND = "Translation property not found.";
	ERR_ROTATION_NOT_FOUND = "Rotation property not found.";
	ERR_SCALE_NOT_FOUND = "Scale property not found.";
	ERR_FILE_LOAD_FAILED = "SPT file load failed.";

	CWaterEntityComponent::ms_kClassName = "CWaterEntityComponent";

	// Component name.
	CWaterEntityComponent::ms_kComponentName = "Water Component";

	// Property names.
	CWaterEntityComponent::ms_kColorName = "Color";
	CWaterEntityComponent::ms_kAlphaName = "Alpha";
	CWaterEntityComponent::ms_kBGSkyTextureName = "Background Sky Texture File Path";
	CWaterEntityComponent::ms_kReflectTexSizeName = "Reflect Texture Size";
	CWaterEntityComponent::ms_kBumpTextureName = "Bump Map File Path";
	CWaterEntityComponent::ms_kBumpRepeatName = "Bump Map Repeat";
	CWaterEntityComponent::ms_kBumpSpeedName = "Bump Map Speed";
	CWaterEntityComponent::ms_kSceneRootPointerName = "Scene Root Pointer";

	// Property descriptions.
	CWaterEntityComponent::ms_kColorDescription = "The color of the water body.";
	CWaterEntityComponent::ms_kAlphaDescription = "The transparence of the water surface.";
	CWaterEntityComponent::ms_kBGSkyTextureDescription = "The path of the background sky texture of the reflect texture";
	CWaterEntityComponent::ms_kReflectTexSizeDescription = "The size of the reflect texture. Can be 128, 256, 512 or 1024";
	CWaterEntityComponent::ms_kBumpTextureDescription = "The path of the bump texture.";
	CWaterEntityComponent::ms_kBumpRepeatDescription = "The repeating times of the bump texture.";
	CWaterEntityComponent::ms_kBumpSpeedDescription = "The moving direction&speed of the bump texture.";
	CWaterEntityComponent::ms_kSceneRootPointerDescription = "Scene Root Pointer";

	// Dependent property names.
	CWaterEntityComponent::ms_kTranslationName = "Translation";
	CWaterEntityComponent::ms_kRotationName = "Rotation";
	CWaterEntityComponent::ms_kScaleName = "Scale";

	NiFactoryRegisterPropIntf(CWaterEntityComponent);	// registe creation function
}
//-------------------------------------------------------------------------------------------------
void CWaterEntityComponent::_SDMShutdown()
{
	ERR_TRANSLATION_NOT_FOUND = NULL;
	ERR_ROTATION_NOT_FOUND = NULL;
	ERR_SCALE_NOT_FOUND = NULL;
	ERR_FILE_LOAD_FAILED = NULL; 

	CWaterEntityComponent::ms_kClassName = NULL;

	// Component name.
	CWaterEntityComponent::ms_kComponentName = NULL;

	// Property names.
	CWaterEntityComponent::ms_kColorName = NULL;
	CWaterEntityComponent::ms_kAlphaName = NULL;
	CWaterEntityComponent::ms_kBGSkyTextureName = NULL;
	CWaterEntityComponent::ms_kReflectTexSizeName = NULL;
	CWaterEntityComponent::ms_kBumpTextureName = NULL;
	CWaterEntityComponent::ms_kBumpRepeatName = NULL;
	CWaterEntityComponent::ms_kBumpSpeedName = NULL;
	CWaterEntityComponent::ms_kSceneRootPointerName = NULL;

	// Property descriptions.
	CWaterEntityComponent::ms_kColorDescription = NULL;
	CWaterEntityComponent::ms_kAlphaDescription = NULL;
	CWaterEntityComponent::ms_kBGSkyTextureDescription = NULL;
	CWaterEntityComponent::ms_kReflectTexSizeDescription = NULL;
	CWaterEntityComponent::ms_kBumpTextureDescription = NULL;
	CWaterEntityComponent::ms_kBumpRepeatDescription = NULL;
	CWaterEntityComponent::ms_kBumpSpeedDescription = NULL;
	CWaterEntityComponent::ms_kSceneRootPointerDescription = NULL;

	// Dependent property names.
	CWaterEntityComponent::ms_kTranslationName = NULL;
	CWaterEntityComponent::ms_kRotationName = NULL;
	CWaterEntityComponent::ms_kScaleName = NULL;
}
//-------------------------------------------------------------------------------------------------
// 构造/析构
CWaterEntityComponent::CWaterEntityComponent(void)
:	m_spMasterComponent(NULL)
{
	m_kBGSkyTextureFile  = ".\\Data\\Texture\\Terrain\\defaultSky.bmp";	
	m_kBumpTextureFile = ".\\Data\\Texture\\Terrain\\defaultWaterBump.bmp";

	m_spWaterInstance = NiNew CWaterBody;
	CWaterRenderViewPtr pkWaterRenderView = CWaterManager::GetInstance()->GetMainWaterRenderView();
	if (pkWaterRenderView != NULL)
	{
		stWaterInitParam waterInitParam;
		//waterInitParam.pkRenderer = NiRenderer::GetRenderer();
		waterInitParam.pWaterRenderView = pkWaterRenderView;
		waterInitParam.kPosition = NiPoint3(0, 0, 0.5);
		waterInitParam.fSize = 10.0f;
		waterInitParam.fAlpha = 0.75f;
		waterInitParam.kColor = NiColor(0.7f, 0.9f, 0.8f);
		waterInitParam.uiReflectTexSize = 512;
		waterInitParam.kBumpMapRepeat = NiPoint2(10, 10);
		waterInitParam.kUVSpeed = NiPoint2(1, 1);
#ifndef CODE_INGAME
		waterInitParam.pkBGSkyTexture = CTerrainTextureMgr::GetInstance()->GetTexture(m_kBGSkyTextureFile);
		waterInitParam.pkBumpTexture = CTerrainTextureMgr::GetInstance()->GetTexture(m_kBumpTextureFile);
#endif
		m_spWaterInstance->Init(waterInitParam);
	}
}
//-------------------------------------------------------------------------------------------------
CWaterEntityComponent::~CWaterEntityComponent(void)
{
}
//-------------------------------------------------------------------------------------------------
CWaterEntityComponent::CWaterEntityComponent(CWaterEntityComponent* pkMasterComponent)
:	m_spMasterComponent(pkMasterComponent)
{
	if (pkMasterComponent != NULL)
	{
		m_spWaterInstance = NiNew CWaterBody;
		CWaterRenderViewPtr pkWaterRenderView = CWaterManager::GetInstance()->GetMainWaterRenderView();
		float fAlpha;
		NiColor kColor;
		//NiFixedString kBGSkyTexture, kBumpTexture;
		unsigned short usReflectTexSize;
		NiPoint2 kBumpMapRepeat, kUVSpeed;

		pkMasterComponent->GetPropertyData(ms_kAlphaName, fAlpha);
		pkMasterComponent->GetPropertyData(ms_kColorName, kColor);
		pkMasterComponent->GetPropertyData(ms_kBGSkyTextureName, m_kBGSkyTextureFile, 0);
		pkMasterComponent->GetPropertyData(ms_kBumpTextureName, m_kBumpTextureFile, 0);
		pkMasterComponent->GetPropertyData(ms_kReflectTexSizeName, usReflectTexSize);
		pkMasterComponent->GetPropertyData(ms_kBumpRepeatName, kBumpMapRepeat);
		pkMasterComponent->GetPropertyData(ms_kBumpSpeedName, kUVSpeed);

		if (pkWaterRenderView != NULL)
		{
			stWaterInitParam waterInitParam;
			//waterInitParam.pkRenderer = NiRenderer::GetRenderer();
			waterInitParam.pWaterRenderView = pkWaterRenderView;
			waterInitParam.kPosition = NiPoint3(0, 0, 0);
			waterInitParam.fSize = 10;
			waterInitParam.fAlpha = fAlpha;
			waterInitParam.kColor = kColor;
			waterInitParam.pkBGSkyTexture = CTerrainTextureMgr::GetInstance()->GetTexture(m_kBGSkyTextureFile);
			waterInitParam.uiReflectTexSize = usReflectTexSize;
			waterInitParam.pkBumpTexture = CTerrainTextureMgr::GetInstance()->GetTexture(m_kBumpTextureFile);
			waterInitParam.kBumpMapRepeat = kBumpMapRepeat;
			waterInitParam.kUVSpeed = kUVSpeed;
			m_spWaterInstance->Init(waterInitParam);
		}
	}
	else
	{
		CWaterEntityComponent();
	}

}
//-------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CWaterEntityComponent::Clone(
	bool bInheritProperties)
{
	return NiNew CWaterEntityComponent(this);
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* CWaterEntityComponent::GetMasterComponent()
{
	return m_spMasterComponent;
}
//---------------------------------------------------------------------------
void CWaterEntityComponent::SetMasterComponent(
	NiEntityComponentInterface* pkMasterComponent)
{
	if (pkMasterComponent)
	{
		NIASSERT(pkMasterComponent->GetClassName() == GetClassName());
		m_spMasterComponent = (CWaterEntityComponent*) pkMasterComponent;
	}
	else
	{
		NiTObjectSet<NiFixedString> kPropertyNames(10);
		GetPropertyNames(kPropertyNames);
		for (unsigned int ui = 0; ui < kPropertyNames.GetSize(); ui++)
		{
			bool bMadeUnique;
			NiBool bSuccess = MakePropertyUnique(kPropertyNames.GetAt(ui),
				bMadeUnique);
			NIASSERT(bSuccess);
		}
		m_spMasterComponent = NULL;
	}
}
//---------------------------------------------------------------------------
void CWaterEntityComponent::GetDependentPropertyNames(
	NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
	kDependentPropertyNames.Add(ms_kTranslationName);
	kDependentPropertyNames.Add(ms_kRotationName);
	kDependentPropertyNames.Add(ms_kScaleName);
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetTemplateID(const NiUniqueID& kID)
{
	//Not supported for custom components
	return false;
}
//---------------------------------------------------------------------------
NiUniqueID  CWaterEntityComponent::GetTemplateID()
{
	static const NiUniqueID kUniqueID = 
		NiUniqueID(0xb8, 0xbf, 0x2d, 0x6f, 0xeb, 0x83, 0x4e, 0x9c, 0xaf, 0xe0, 0x20, 0xe0, 0xf9, 0x5e, 0x8, 0xd8);
	return kUniqueID;
}
//---------------------------------------------------------------------------
void CWaterEntityComponent::AddReference()
{
	this->IncRefCount();
}
//---------------------------------------------------------------------------
void CWaterEntityComponent::RemoveReference()
{
	this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString CWaterEntityComponent::GetClassName() const
{
	return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CWaterEntityComponent::ClassName()
{
	return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString CWaterEntityComponent::GetName() const
{
	return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetName(const NiFixedString& kName)
{
	// This component does not allow its name to be set.
	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::IsAnimated() const
{
	return true;
}
//---------------------------------------------------------------------------
void CWaterEntityComponent::Update(NiEntityPropertyInterface* pkParentEntity,
								  float fTime, NiEntityErrorInterface* pkErrors,
								  NiExternalAssetManager* pkAssetManager)
{
	m_fLastUpdateTime = fTime;
	// 水体实例更新

	// 从 pkParentEntity 中取得世界变换信息，赋给 water body 的 scene root
	// Find dependent properties.
	bool bDependentPropertiesFound = true;

	NiPoint3 kTranslation;
	if (!pkParentEntity->GetPropertyData(ms_kTranslationName,
		kTranslation))
	{
		bDependentPropertiesFound = false;
		pkErrors->ReportError(ERR_TRANSLATION_NOT_FOUND, NULL,
			pkParentEntity->GetName(), ms_kTranslationName);
	}
	//NiMatrix3 kRotation;
	//if (!pkParentEntity->GetPropertyData(ms_kRotationName, kRotation))
	//{
	//	bDependentPropertiesFound = false;
	//	pkErrors->ReportError(ERR_ROTATION_NOT_FOUND, NULL,
	//		pkParentEntity->GetName(), ms_kRotationName);
	//}
	float fScale;
	if (!pkParentEntity->GetPropertyData(ms_kScaleName, fScale))
	{
		bDependentPropertiesFound = false;
		pkErrors->ReportError(ERR_SCALE_NOT_FOUND, NULL,
			pkParentEntity->GetName(), ms_kScaleName);
	}

	// Use dependent properties to update transform of scene root.
	bool bUpdatedTransforms = false;
	if (bDependentPropertiesFound)
	{
		if (m_spWaterInstance->GetSceneRoot()->GetTranslate() != kTranslation)
		{
			m_spWaterInstance->GetSceneRoot()->SetTranslate(kTranslation);
			bUpdatedTransforms = true;
		}
		//if (m_spWaterInstance->GetSceneRoot()->GetRotate() != kRotation)
		//{
		//	//m_spWaterInstance->GetSceneRoot()->SetRotate(kRotation);
		//	//bUpdatedTransforms = true;
		//}
		if (m_spWaterInstance->GetSceneRoot()->GetScale() != fScale)
		{
			m_spWaterInstance->GetSceneRoot()->SetScale(fScale);
			bUpdatedTransforms = true;
		}
	}

	// Update scene root with the provided time.
	if (bUpdatedTransforms)
	{
		m_spWaterInstance->GetSceneRoot()->Update(fTime);

		m_spWaterInstance->SetPosition(kTranslation);
		m_spWaterInstance->SetScale(fScale);
		m_spWaterInstance->GetSceneRoot()->SetLocalTransform(
			m_spWaterInstance->GetSceneRoot()->GetWorldTransform());
		m_spWaterInstance->GetSceneRoot()->Update(fTime);

	}
	m_spWaterInstance->Update(fTime);

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void CWaterEntityComponent::BuildVisibleSet(
	NiEntityRenderingContext* pkRenderingContext,
	NiEntityErrorInterface* pkErrors)
{
	if (m_spWaterInstance)
	{
		m_spWaterInstance->BuildVisibleSet(pkRenderingContext->m_pkCamera, pkRenderingContext->m_pkCullingProcess);
	}
}
//---------------------------------------------------------------------------
void CWaterEntityComponent::GetPropertyNames(
	NiTObjectSet<NiFixedString>& kPropertyNames) const
{
	kPropertyNames.Add(ms_kColorName);
	kPropertyNames.Add(ms_kAlphaName);
	kPropertyNames.Add(ms_kBGSkyTextureName);
	kPropertyNames.Add(ms_kReflectTexSizeName);
	kPropertyNames.Add(ms_kBumpTextureName);
	kPropertyNames.Add(ms_kBumpRepeatName);
	kPropertyNames.Add(ms_kBumpSpeedName);
	kPropertyNames.Add(ms_kSceneRootPointerName);
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::CanResetProperty(
	const NiFixedString& kPropertyName, bool& bCanReset) const
{
	if (kPropertyName == ms_kColorName			|| 
		kPropertyName == ms_kAlphaName			|| 
		kPropertyName == ms_kBGSkyTextureName	|| 
		kPropertyName == ms_kReflectTexSizeName || 
		kPropertyName == ms_kBumpTextureName	|| 
		kPropertyName == ms_kBumpRepeatName		|| 
		kPropertyName == ms_kBumpSpeedName)
	{
		bCanReset = true;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		bCanReset = false;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::ResetProperty(const NiFixedString& kPropertyName)
{
	bool bCanReset;
	if (!CanResetProperty(kPropertyName, bCanReset) || !bCanReset)
	{
		return false;
	}

	NIASSERT(m_spMasterComponent);

	if (kPropertyName == ms_kColorName)
	{
		
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		
	}
	else if (kPropertyName == ms_kBGSkyTextureName)
	{

	}
	else if (kPropertyName == ms_kReflectTexSizeName)
	{

	}
	else if (kPropertyName == ms_kBumpTextureName)
	{

	}
	else if (kPropertyName == ms_kBumpRepeatName)
	{

	}
	else if (kPropertyName == ms_kBumpSpeedName)
	{

	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		return false;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::MakePropertyUnique(
	const NiFixedString& kPropertyName, bool& bMadeUnique)
{
	bool bCanReset;
	if (!CanResetProperty(kPropertyName, bCanReset))
	{
		return false;
	}
	bMadeUnique = true;

	//if (!bCanReset && m_spMasterComponent)
	//{
	//	if (kPropertyName == ms_kSptFilePathName)
	//	{
	//		SetSptFilePathChanged(true);
	//		bMadeUnique = true;
	//	}
	//	else if (kPropertyName == ms_kWindEntityName)
	//	{
	//		SetWindEntityChanged(true);
	//		bMadeUnique = true;
	//	}
	//	else
	//	{
	//		bMadeUnique = false;
	//	}
	//}
	//else
	//{
	//	bMadeUnique = false;
	//}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetDisplayName(
	const NiFixedString& kPropertyName, NiFixedString& kDisplayName) const
{
	if (kPropertyName == ms_kColorName)
	{
		kDisplayName = ms_kColorName;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kDisplayName = ms_kAlphaName;
	}
	else if (kPropertyName == ms_kBGSkyTextureName)
	{
		kDisplayName = ms_kBGSkyTextureName;
	}
	else if (kPropertyName == ms_kReflectTexSizeName)
	{
		kDisplayName = ms_kReflectTexSizeName;
	}
	else if (kPropertyName == ms_kBumpTextureName)
	{
		kDisplayName = ms_kBumpTextureName;
	}
	else if (kPropertyName == ms_kBumpRepeatName)
	{
		kDisplayName = ms_kBumpRepeatName;
	}
	else if (kPropertyName == ms_kBumpSpeedName)
	{
		kDisplayName = ms_kBumpSpeedName;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kDisplayName = NULL;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetDisplayName(
	const NiFixedString& kPropertyName, const NiFixedString& kDisplayName)
{
	// This component does not allow the display name to be set.
	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetPrimitiveType(
	const NiFixedString& kPropertyName, NiFixedString& kPrimitiveType) const
{
	if (kPropertyName == ms_kColorName)
	{
		kPrimitiveType = PT_COLOR;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kPrimitiveType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kBGSkyTextureName)
	{
		kPrimitiveType = PT_STRING;
	}
	else if (kPropertyName == ms_kReflectTexSizeName)
	{
		kPrimitiveType = PT_USHORT;
	}
	else if (kPropertyName == ms_kBumpTextureName)
	{
		kPrimitiveType = PT_STRING;
	}
	else if (kPropertyName == ms_kBumpRepeatName)
	{
		kPrimitiveType = PT_POINT2;
	}
	else if (kPropertyName == ms_kBumpSpeedName)
	{
		kPrimitiveType = PT_POINT2;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kPrimitiveType = PT_NIOBJECTPOINTER;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetPrimitiveType(
	const NiFixedString& kPropertyName, const NiFixedString& kPrimitiveType)
{
	// This component does not allow the primitive type to be set.
	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetSemanticType(
	const NiFixedString& kPropertyName, NiFixedString& kSemanticType) const
{
	if (kPropertyName == ms_kColorName)
	{
		kSemanticType = PT_COLOR;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kSemanticType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kBGSkyTextureName)
	{
		kSemanticType = "Filename";
	}
	else if (kPropertyName == ms_kReflectTexSizeName)
	{
		kSemanticType = PT_USHORT;
	}
	else if (kPropertyName == ms_kBumpTextureName)
	{
		kSemanticType = "Filename";
	}
	else if (kPropertyName == ms_kBumpRepeatName)
	{
		kSemanticType = PT_POINT2;
	}
	else if (kPropertyName == ms_kBumpSpeedName)
	{
		kSemanticType = PT_POINT2;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kSemanticType = PT_NIOBJECTPOINTER;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetSemanticType(
	const NiFixedString& kPropertyName, const NiFixedString& kSemanticType)
{
	// This component does not allow the semantic type to be set.
	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetDescription(
	const NiFixedString& kPropertyName, NiFixedString& kDescription) const
{
	if (kPropertyName == ms_kColorName)
	{
		kDescription = ms_kColorDescription;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kDescription = ms_kAlphaDescription;
	}
	else if (kPropertyName == ms_kBGSkyTextureName)
	{
		kDescription = ms_kBGSkyTextureDescription;
	}
	else if (kPropertyName == ms_kReflectTexSizeName)
	{
		kDescription = ms_kReflectTexSizeDescription;
	}
	else if (kPropertyName == ms_kBumpTextureName)
	{
		kDescription = ms_kBumpTextureDescription;
	}
	else if (kPropertyName == ms_kBumpRepeatName)
	{
		kDescription = ms_kBumpRepeatDescription;
	}
	else if (kPropertyName == ms_kBumpSpeedName)
	{
		kDescription = ms_kBumpSpeedDescription;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kDescription = ms_kSceneRootPointerDescription;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetDescription(
	const NiFixedString& kPropertyName, const NiFixedString& kDescription)
{
	// This component does not allow the description to be set.
	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetCategory(const NiFixedString& kPropertyName,
										 NiFixedString& kCategory) const
{
	if (kPropertyName == ms_kColorName			|| 
		kPropertyName == ms_kAlphaName			|| 
		kPropertyName == ms_kBGSkyTextureName	|| 
		kPropertyName == ms_kReflectTexSizeName || 
		kPropertyName == ms_kBumpTextureName	|| 
		kPropertyName == ms_kBumpRepeatName		|| 
		kPropertyName == ms_kBumpSpeedName		|| 
		kPropertyName == ms_kSceneRootPointerName)
	{
		kCategory = ms_kComponentName;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::IsPropertyReadOnly(
	const NiFixedString& kPropertyName, bool& bIsReadOnly)
{
	if (kPropertyName == ms_kColorName			|| 
		kPropertyName == ms_kAlphaName			|| 
		kPropertyName == ms_kBGSkyTextureName	|| 
		kPropertyName == ms_kReflectTexSizeName || 
		kPropertyName == ms_kBumpTextureName	|| 
		kPropertyName == ms_kBumpRepeatName		|| 
		kPropertyName == ms_kBumpSpeedName)
	{
		bIsReadOnly = false;
		return true;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		bIsReadOnly = true;
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::IsPropertyUnique(
	const NiFixedString& kPropertyName, bool& bIsUnique)
{
	bIsUnique = true;
	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::IsPropertySerializable(
	const NiFixedString& kPropertyName, bool& bIsSerializable)
{
	if (kPropertyName == ms_kColorName			|| 
		kPropertyName == ms_kAlphaName			|| 
		kPropertyName == ms_kBGSkyTextureName	|| 
		kPropertyName == ms_kReflectTexSizeName || 
		kPropertyName == ms_kBumpTextureName	|| 
		kPropertyName == ms_kBumpRepeatName		|| 
		kPropertyName == ms_kBumpSpeedName)
	{
		bIsSerializable = true;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		bIsSerializable = false;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::IsPropertyInheritable(
	const NiFixedString& kPropertyName, bool& bIsInheritable)
{
	if (kPropertyName == ms_kColorName			|| 
		kPropertyName == ms_kAlphaName			|| 
		kPropertyName == ms_kBGSkyTextureName	|| 
		kPropertyName == ms_kReflectTexSizeName || 
		kPropertyName == ms_kBumpTextureName	|| 
		kPropertyName == ms_kBumpRepeatName		|| 
		kPropertyName == ms_kBumpSpeedName)
	{
		bIsInheritable = true;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		bIsInheritable = false;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::IsExternalAssetPath(
	const NiFixedString& kPropertyName, unsigned int uiIndex,
	bool& bIsExternalAssetPath) const
{
	if (kPropertyName == ms_kColorName			|| 
			kPropertyName == ms_kAlphaName			|| 
			kPropertyName == ms_kReflectTexSizeName || 
			kPropertyName == ms_kBumpRepeatName		|| 
			kPropertyName == ms_kBumpSpeedName		||
			kPropertyName == ms_kSceneRootPointerName ||
			kPropertyName == ms_kBGSkyTextureName	|| 
			kPropertyName == ms_kBumpTextureName)
	{
		bIsExternalAssetPath = true;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetElementCount(
	const NiFixedString& kPropertyName, unsigned int& uiCount) const
{
	if (kPropertyName == ms_kColorName			|| 
		kPropertyName == ms_kAlphaName			|| 
		kPropertyName == ms_kBGSkyTextureName	|| 
		kPropertyName == ms_kReflectTexSizeName || 
		kPropertyName == ms_kBumpTextureName	|| 
		kPropertyName == ms_kBumpRepeatName		|| 
		kPropertyName == ms_kBumpSpeedName		|| 
		kPropertyName == ms_kSceneRootPointerName)
	{
		uiCount = 1;
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetElementCount(
	const NiFixedString& kPropertyName, unsigned int uiCount, bool& bCountSet)
{
	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::IsCollection(const NiFixedString& kPropertyName,
										  bool& bIsCollection) const
{
	if (kPropertyName == ms_kColorName			|| 
		kPropertyName == ms_kAlphaName			|| 
		kPropertyName == ms_kBGSkyTextureName	|| 
		kPropertyName == ms_kReflectTexSizeName || 
		kPropertyName == ms_kBumpTextureName	|| 
		kPropertyName == ms_kBumpRepeatName		|| 
		kPropertyName == ms_kBumpSpeedName		|| 
		kPropertyName == ms_kSceneRootPointerName)
	{
		bIsCollection = false;
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetPropertyData(
	const NiFixedString& kPropertyName, NiFixedString& kData,
	unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kBGSkyTextureName)
	{
		kData = m_kBGSkyTextureFile;
	}
	else if (kPropertyName == ms_kBumpTextureName)
	{
		kData = m_kBumpTextureFile;
	}
	else
	{
		return false;
	}

	return true;
}

NiFixedString CWaterEntityComponent::GetBGSkyTexFile()
{
	return m_kBGSkyTextureFile;
}

NiFixedString CWaterEntityComponent::GetBumpTexFile()
{
	return m_kBumpTextureFile;
}

//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetPropertyData(
	const NiFixedString& kPropertyName, const NiFixedString& kData,
	unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kBGSkyTextureName)
	{
		m_kBGSkyTextureFile = kData;
		// 载入纹理，设置给水体实例
		NiTexturePtr pkBGTexture = NULL;
#ifndef CODE_INGAME
		// 编辑器中的纹理载入
		pkBGTexture = CTerrainTextureMgr::GetInstance()->GetTexture(m_kBGSkyTextureFile);
#else
		// 客户端的纹理载入，请老黄将代码写在这里
		// brabra



#endif
		m_spWaterInstance->SetReflectTextureBG(pkBGTexture);
	}
	else if (kPropertyName == ms_kBumpTextureName)
	{
		m_kBumpTextureFile = kData ;
		NiTexturePtr pkBumpTexture = NULL;
#ifndef CODE_INGAME
		// 编辑器中的纹理载入
		pkBumpTexture = CTerrainTextureMgr::GetInstance()->GetTexture(m_kBumpTextureFile);
#else
		// 客户端的纹理载入，请老黄将代码写在这里
		// brabra



#endif
		m_spWaterInstance->SetBumpMap(pkBumpTexture);

	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetPropertyData(const NiFixedString& kPropertyName,
							   NiObject*& pkData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetPropertyData(
	const NiFixedString& kPropertyName, NiObject*& pkData,
	unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		NiAVObject* pkAVObj = m_spWaterInstance->GetSceneRoot();
		pkData = pkAVObj;
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetPropertyData(const NiFixedString& kPropertyName, 
							   const NiPoint2& kData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kBumpRepeatName)
	{
		m_spWaterInstance->SetBumpMapRepeat(kData);
	}
	else if (kPropertyName == ms_kBumpSpeedName)
	{
		m_spWaterInstance->SetBumpMapSpeed(kData);
	}
	else
	{
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetPropertyData(const NiFixedString& kPropertyName, 
							   NiPoint2& kData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kBumpRepeatName)
	{
		kData = m_spWaterInstance->GetBumpMapRepeat();
	}
	else if (kPropertyName == ms_kBumpSpeedName)
	{
		kData = m_spWaterInstance->GetBumpMapSpeed();
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
// 设置/获取水面颜色
NiBool CWaterEntityComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiColor& kData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kColorName)
	{
		m_spWaterInstance->SetColor(kData);
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetPropertyData(const NiFixedString& kPropertyName, NiColor& kData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kColorName)
	{
		kData = m_spWaterInstance->GetColor();
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
//// 设置/获取水面位置，旋转
//NiBool CWaterEntityComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiPoint3& kData, unsigned int uiIndex = 0)
//{
//
//}
////---------------------------------------------------------------------------
//NiBool CWaterEntityComponent::GetPropertyData(const NiFixedString& kPropertyName, NiPoint3& kData, unsigned int uiIndex = 0) const
//{
//
//}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		m_spWaterInstance->SetAlpha(fData);
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		fData = m_spWaterInstance->GetAlpha();
	}
	else
	{
		return false;
	}

	return true;
}
//---------------------------------------------------------------------------
NiBool CWaterEntityComponent::SetPropertyData(const NiFixedString& kPropertyName, unsigned short usData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kReflectTexSizeName)
	{
		m_spWaterInstance->SetReflectTextureSize(usData);
	}
	else
	{
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------
NiBool CWaterEntityComponent::GetPropertyData(const NiFixedString& kPropertyName, unsigned short& usData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kReflectTexSizeName)
	{
		usData = m_spWaterInstance->GetReflectTextureSize();
	}
	else
	{
		return false;
	}

	return true;
}
//------------------------------------------------------------------------------