﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include "SimpleFog.h"
#include "NiTextureTransformController.h"
#include "NiLinFloatKey.h"
#include "NiFloatData.h"
#include "NiFloatInterpolator.h"
#include "Utility.h"
using namespace SceneCore;

CSimpleFog::CSimpleFog(CTerrain* pkTerrain)
:	m_pkTerrain(pkTerrain),
	m_pkDepthTexBaker(NULL),
	m_pkSurfaceMaker(NULL)
{

}
//-----------------------------------------------------------------------------
CSimpleFog::~CSimpleFog(void)
{
	SAFE_DELETE(m_pkDepthTexBaker);
	SAFE_DELETE(m_pkSurfaceMaker);
}
//-----------------------------------------------------------------------------
bool CSimpleFog::Init(const stSimpleFogPrarm param)
{
	if (m_pkDepthTexBaker!=NULL || m_pkTerrain==NULL)
	{
		// 已经初始化过了
		return false;
	}

	// 拷贝雾的属性
	m_uiFogTextureSize = param.uiTextureSize;
	m_kLBPoint = param.kLBPoint;
	m_kRUPoint = param.kRUPoint;
	m_fStartZ = param.fStartZ;
	m_fEndZ = param.fEndZ;
	m_kColor = param.kColor;

	// 创建 FogTexture
	m_pkDepthTexBaker = new CDepthToTextureBaker(m_pkTerrain);
	m_pkDepthTexBaker->Bake(param);

	// 创建雾表面几何体
	m_pkSurfaceMaker = new CFogSurfaceMaker(m_pkTerrain);
	m_pkSurfaceMaker->Make(param);

	NiTriShape* pkFogGeom = m_pkSurfaceMaker->GetFogGeometry();

	NiTexturingProperty* pkTexProp = NiNew NiTexturingProperty();
	pkTexProp->SetBaseTexture(m_pkDepthTexBaker->GetDepthTexture());
	pkTexProp->SetApplyMode(NiTexturingProperty::APPLY_REPLACE);

	pkFogGeom->AttachProperty(pkTexProp);

	NiVertexColorProperty* pkVertColorProp = NiNew NiVertexColorProperty();
	pkVertColorProp->SetLightingMode(NiVertexColorProperty::LIGHTING_E);
	pkFogGeom->AttachProperty(pkVertColorProp);

	NiAlphaProperty* pkAlphaProp = NiNew NiAlphaProperty();
	pkAlphaProp->SetAlphaBlending(true);
	pkAlphaProp->SetSrcBlendMode(NiAlphaProperty::ALPHA_SRCALPHA);
	pkAlphaProp->SetDestBlendMode(NiAlphaProperty::ALPHA_INVSRCALPHA);
	pkFogGeom->AttachProperty(pkAlphaProp);

	NiZBufferProperty* pkZBuffProp = NiNew NiZBufferProperty();
	pkZBuffProp->SetZBufferTest(true);
	pkZBuffProp->SetZBufferWrite(false);
	pkFogGeom->AttachProperty(pkZBuffProp);

	pkFogGeom->UpdateProperties();

	// 设置 纹理 UV 动画
	NiSourceTexturePtr pkAnimTexture = NiSourceTexture::Create(param.szUVAnimTexture);
	char szFileName[MAX_PATH];
	ExtractFileNameFromPath(param.szUVAnimTexture, szFileName);
	pkAnimTexture->SetFilename(szFileName);
	NiTextureTransform* pkTexTrans = NiNew NiTextureTransform();
	NiTexturingProperty::Map* pkMap = NiNew NiTexturingProperty::Map(pkAnimTexture, 0);
	//pkMap->SetTextureTransform(pkTexTrans);
	pkTexProp->SetGlowMap(pkMap);

	NiTextureTransformController* pkTexController = NiNew NiTextureTransformController(pkTexProp, pkMap);

	pkTexController->SetBeginKeyTime( 0.0 );
	pkTexController->SetEndKeyTime( 10.0/param.fSpeedU );
	pkTexController->SetCycleType( NiTimeController::LOOP );

	NiLinFloatKey * pFloatKey = NiNew NiLinFloatKey[2];
	pFloatKey[0].SetTime( 0.0 );
	pFloatKey[0].SetValue( 0.0 );
	pFloatKey[1].SetTime( 10.0/param.fSpeedU  );
	pFloatKey[1].SetValue( 1.0 );

	NiFloatData * pFloatData = NiNew NiFloatData();
	pFloatData->SetAnim( pFloatKey, 2, NiAnimationKey::LINKEY );

	NiFloatInterpolator * pFloatInter = NiNew NiFloatInterpolator( pFloatData );
	pkTexController->SetInterpolator( pFloatInter );

	pkTexController->SetActive(true);
	pkTexController->Start();
	pkFogGeom->SetSelectiveUpdate(true);
	pkFogGeom->SetSelectiveUpdatePropertyControllers(true);

	pkFogGeom->Update(0.0f);
	pkFogGeom->UpdateProperties();
	pkFogGeom->UpdateControllers(0.0f);

	//pkFogGeom->SetSelectiveUpdatePropertyControllers(true);
	//pkTexProp->SetControllers(pkTexController);
	//pkTexController->set
	return true;
}
//-----------------------------------------------------------------------------
/// 获取雾表面
NiTriShape* CSimpleFog::GetFogSurface()
{
	if (m_pkSurfaceMaker != NULL)
	{
		return m_pkSurfaceMaker->GetFogGeometry();
	}
	else
	{
		return NULL;
	}
}
//-----------------------------------------------------------------------------
/// 获取雾 alpha 贴图
NiTexture* CSimpleFog::GetFogTexture()
{
	if (m_pkDepthTexBaker != NULL)
	{
		return m_pkDepthTexBaker->GetDepthTexture();
	}
	else
	{
		return NULL;
	}
}
//-----------------------------------------------------------------------------
bool CSimpleFog::SaveSimpleFog(const char* pszFogTexFileName, const char* pszFogSurfaceFileName)
{
	if (m_pkDepthTexBaker==NULL || m_pkSurfaceMaker==NULL)
	{
		return false;
	}

	NiTriShape* pkFogSurface = m_pkSurfaceMaker->GetFogGeometry();
	NiSourceTexture* pkFogTexture = m_pkDepthTexBaker->GetDepthTexture();

	if (pkFogSurface==NULL || pkFogTexture==NULL)
	{
		return false;
	}

	SaveTextureToDDS(pkFogTexture, pszFogTexFileName);

	NiSourceTexturePtr pkTexture = NiSourceTexture::Create(pszFogTexFileName);

	char pszFileName[MAX_PATH];
	ExtractFileNameFromPath(pszFogTexFileName, pszFileName);
	pkTexture->SetFilename(pszFileName);

	NiTexturingProperty* pkTexProp = (NiTexturingProperty*)pkFogSurface->GetProperty(NiTexturingProperty::GetType());
	if (pkTexProp==NULL)
	{
		pkTexProp = NiNew NiTexturingProperty();
	}

	pkTexProp->SetBaseTexture(pkTexture);
	
	pkFogSurface->UpdateProperties();

	NiNodePtr pkNode = NiNew NiNode;
	pkNode->AttachChild(pkFogSurface);
	pkNode->Update(0.0f);

	NiStream kStream;
	kStream.InsertObject(pkNode);
	kStream.Save(pszFogSurfaceFileName);

	return true;

}
//-----------------------------------------------------------------------------
#endif