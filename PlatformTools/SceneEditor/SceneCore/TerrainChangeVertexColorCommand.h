﻿/** @file Terrain.h 
@brief  烘焙顶点颜色 command,用于 undo/redo
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：1.0
*	作    者：Shi Yazheng
*	完成日期：2008-02-18
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME
#pragma once

#include <NiColor.h>

class CTerrain;

struct MAIN_ENTRY tVertexColorChange
{
	int iIndex;			// 被改变颜色的顶点索引
	NiColorA kOldColor;	// 改变之前的颜色
	NiColorA kNewColor;	// 改变之后的颜色
};

class  MAIN_ENTRY CTerrainChangeVertexColorCommand
{
public:
	CTerrainChangeVertexColorCommand(CTerrain* pTerrain);
	~CTerrainChangeVertexColorCommand(void);

	/// 执行该命令
	void DoCommand(void);

	/// 撤销该命令
	void UndoCommand(void);

	/// 增加一个改变的顶点 
	void AddVertexChange(const tVertexColorChange& vertChange);

	/// 增加有顶点改变的 chunk
	void AddChangedChunk(int iChunk);

protected:
	/// 计算顶点法线并且将顶点变化通知 chunk
	void _NotifyChunk();

	std::map<int, tVertexColorChange>	m_VerticesChange;	// 顶点索引-该索引顶点发生的改变 映射
	set<int>					m_chunkset;
	CTerrain*					m_pTerrain;
};

#endif