﻿#pragma once

/** @file PostEffectManager.h 
@brief 灰度化，测试用代码
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2008-09-01
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：
</pre>*/

#pragma once

#ifndef CODE_INGAME

#include "PostEffect.h"
#include <NiTSet.h>

namespace SceneCore
{
	NiSmartPointer(CPostEffectManager);
	class MAIN_ENTRY CPostEffectManager:public NiAVObject
	{
	public:
		CPostEffectManager(void);
		~CPostEffectManager(void);

		//add new effect 
		bool			AddEffect(CPostEffect* pkEffect);
		unsigned int	GetNumEffects() const;
		CPostEffect*	GetEffectAt(unsigned int uiIndex) const;

		//set and get active
		void			SetActive(unsigned int uiIndex,bool bActive);
		bool			GetActive(unsigned int uiIndex);

		//remove effect and remove all effects
		void			RemoveEffectAt(unsigned int uiIndex);
		void			RemoveAllEffects();

		//Initial
		bool			Init(NiRenderedTexturePtr pkSourceTexture);

		//Render
		void			PerformRendering();

	private:
		CPostEffectManager(CPostEffectManager&);

		NiTPrimitiveSet<CPostEffect*> m_kEffects;
	};//end of class
};	// end of namespace
#endif