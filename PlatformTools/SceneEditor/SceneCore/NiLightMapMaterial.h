﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2008 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Calabasas, CA 91302
// http://www.emergent.net

#ifndef NILIGHTMAPMATERIAL_H
#define NILIGHTMAPMATERIAL_H

#ifndef CODE_INGAME

#include <NiSystem.h>
#include <NiMain.h>
#include <NiStaticDataManager.h>

#ifdef _USRDLL
#include <NiMaterialLibraryInterface.h>
#endif


#ifdef NILIGHTMAPMATERIAL_EXPORT
    // DLL library project uses this
    #define NILIGHTMAPMATERIAL_ENTRY __declspec(dllexport)
#else
#ifdef NILIGHTMAPMATERIAL_IMPORT
    // client of DLL uses this
    #define NILIGHTMAPMATERIAL_ENTRY __declspec(dllimport)
#else
    // static library project uses this
    #define NILIGHTMAPMATERIAL_ENTRY
#endif
#endif

class MAIN_ENTRY NiLightMapMaterialLibrary: public NiMaterialLibrary
{
public:
    NiLightMapMaterialLibrary();
    virtual ~NiLightMapMaterialLibrary();

    // From NiMaterialLibrary
    virtual NiMaterial* GetMaterial(const NiFixedString& kName);
    virtual const char* GetMaterialName(unsigned int ui);
    virtual unsigned int GetMaterialCount() const;
    virtual NiShaderDesc* GetFirstMaterialDesc();
    virtual NiShaderDesc* GetNextMaterialDesc();

    // *** begin Emergent internal use only ***
    static NiMaterialLibrary* Create();
    static void Shutdown();
    // *** end Emergent internal use only ***

    static const int NUM_MATERIALS = 1;

protected:
    static NiLightMapMaterialLibrary* ms_pThis;

    NiShaderDesc* m_pShaderDescArray[NUM_MATERIALS];
    unsigned m_shaderDescIndex;
};

#if !defined(_WII)
class MAIN_ENTRY NiLightMapMaterial: public NiStandardMaterial
{
public:
    NiDeclareRTTI;

    NiLightMapMaterial();
    virtual ~NiLightMapMaterial();

protected:
    virtual bool HandleCustomMaps(Context& kContext, 
		NiStandardPixelProgramDescriptor* pkPixDesc, 
        unsigned int& uiWhichTexture, NiMaterialResource** apkUVSets, 
        unsigned int uiNumStandardUVs, NiMaterialResource*& pkMatDiffuseColor, 
        NiMaterialResource*& pkMatSpecularColor, 
		NiMaterialResource*& pkMatSpecularPower, 
        NiMaterialResource*& pkMatAmbientColor, 
        NiMaterialResource*& pkMatEmissiveColor, 
		NiMaterialResource*& pkDiffuseLightAccum, 
        NiMaterialResource*& pkSpecularLightAccum);
//
//virtual bool HandleCustomMaps(Context& kContext, 
//		NiStandardPixelProgramDescriptor* pkPixDesc, 
//		unsigned int& uiWhichTexture, NiMaterialResource** apkUVSets, 
//		unsigned int uiNumStandardUVs, NiMaterialResource*& pkDiffuseColorRes, 
//		NiMaterialResource*& pkSpecularColorRes, 
//		NiMaterialResource*& pkSpecularPowerRes, 
//		NiMaterialResource*& pkAmbientColorRes,
//		NiMaterialResource*& pkEmissiveColorRes, 
//		NiMaterialResource*& pkDiffuseAccum, 
//		NiMaterialResource*& pkSpecularAccum)

};
#else
class NILIGHTMAPMATERIAL_ENTRY NiLightMapMaterial: public NiSingleShaderMaterial
{
public:
    NiDeclareRTTI;

    NiLightMapMaterial();
    virtual ~NiLightMapMaterial();
};
#endif

NiDeclareSDM(NiLightMapMaterial, MAIN_ENTRY);

static NiLightMapMaterialSDM NiLightMapMaterialSDMObject;

#endif // CODE_INGAME
#endif