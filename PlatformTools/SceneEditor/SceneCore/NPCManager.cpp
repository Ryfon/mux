﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include "NPC.h"
#include "NPCManager.h"


CNPCManager::CNPCManager(void)
{
	
}

CNPCManager::~CNPCManager(void)
{
	CleanUp();
}

int CNPCManager::LoadNPC(const string& strXmlFileName)
{
	TiXmlDocument XmlDoc(strXmlFileName.c_str());
	if (!XmlDoc.LoadFile())
	{
		return 0;
	}
	TiXmlElement* pRootElement = XmlDoc.RootElement();
	m_FileVersion = pRootElement->Attribute("FileVersion");
	
	for (const TiXmlElement* pNPCElement = pRootElement->FirstChildElement(); pNPCElement; 
		pNPCElement = pNPCElement->NextSiblingElement())
	{
		//创建NPC
		CNPC* pNPC = new CNPC;
		//pNPC->LoadData(pNPCElement);
		pNPC->LoadDataM1(pNPCElement);//M1阶段新的读取函数
		DWORD dwNPCNo = pNPC->m_NPCNO;
		m_NPCList[dwNPCNo] = pNPC;
	}

	return 0;
}

int CNPCManager::SaveNPC(const string& strXmlFileName)
{
	//插入Xml声明
	TiXmlDocument* pXmlDoc = new TiXmlDocument(strXmlFileName.c_str());
	
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	pXmlDoc->InsertEndChild(*pXmlDec);
	TiXmlElement* pXmlRootElement = new TiXmlElement("NPCInfoRoot");
	pXmlRootElement->SetAttribute("FileVersion", "0.9");
	pXmlDoc->LinkEndChild(pXmlRootElement);
	for (map<DWORD, CNPC*>::iterator Ite = m_NPCList.begin(); Ite != m_NPCList.end(); Ite++)
	{
		TiXmlElement* pXmlNpc = new TiXmlElement("NPC");
		CNPC* pNPC = Ite->second;
		pNPC->SaveData(pXmlNpc);
		pXmlRootElement->LinkEndChild(pXmlNpc);
	}

	pXmlDoc->SaveFile();
	
	delete pXmlDoc;
	
	return 0;
}

CNPC* CNPCManager::GetNPC(DWORD dwNPCNO)
{
	map<DWORD, CNPC*>::iterator Ite = m_NPCList.find(dwNPCNO);
	if (Ite == m_NPCList.end())
	{
		return NULL;
	}
	return Ite->second;
}

int CNPCManager::CleanUp()
{
	for (map<DWORD, CNPC*>::iterator Ite = m_NPCList.begin(); Ite != m_NPCList.end(); Ite++)
	{
		delete Ite->second;
	}

	m_NPCList.clear();
	return 0;
}

CNPC* CNPCManager::AddNPC()
{
	CNPC* pNPC = new CNPC;
	pNPC->m_NPCNO = GetNPCNewNO();
	//如果NPCNO不唯一，会导致内存泄露
	m_NPCList[pNPC->m_NPCNO] = pNPC;
	stringstream ssTemp;
	ssTemp << pNPC->m_NPCNO;
	ssTemp >> pNPC->m_Name;
	return pNPC;
}

/*
NO长度：4字节
从高位到低位依次的含义：
第一个字节：NO类型，总的分类 1：功能NPC 2：怪物 3：道具 4：地图
第二个字节：预留，每个分类可以自己使用
后两个字节：表示NO
*/
DWORD CNPCManager::GetNPCNewNO()
{
	for (size_t nIndex = 0; nIndex < m_NOList.size(); nIndex++)
	{
		if (m_NOList[nIndex].second == 0)
		{
			m_NOList[nIndex].second = 1;
			return m_NOList[nIndex].first;
		}
	}
	
	DWORD dwNo = 0;
	if (m_NOList.size() != 0)
	{
		dwNo = m_NOList[m_NOList.size() - 1].first + 1;
	}
	else
	{
		dwNo = 1;
		//最高字节置1
		dwNo = dwNo << 25;
		dwNo++;
	}

	NO_PAIR ANoPair = make_pair(dwNo, 1);
	m_NOList.push_back(ANoPair);
	return dwNo;
}

int CNPCManager::DelNPC(DWORD dwNPCNO)
{
	map<DWORD, CNPC*>::iterator Ite;
	Ite = m_NPCList.find(dwNPCNO);
	if (Ite == m_NPCList.end())
	{
		return 0;
	}
	CNPC* pNpc = Ite->second;
	SetNPCNewNO(pNpc->m_NPCNO);
	delete pNpc;
	
	m_NPCList.erase(Ite);
	return 0;
}

int CNPCManager::LoadNPCNOList(const string& strXmlFileName)
{
	m_NOList.clear();
	TiXmlDocument XmlDoc(strXmlFileName.c_str());
	if (!XmlDoc.LoadFile())
	{
		return 0;
	}

	TiXmlElement* pRootElement = XmlDoc.RootElement();

	for (const TiXmlElement* pNPCElement = pRootElement->FirstChildElement(); pNPCElement; 
		pNPCElement = pNPCElement->NextSiblingElement())
	{
		NO_PAIR ANoPair = make_pair(atol(pNPCElement->GetText()), atoi(pNPCElement->Attribute("Used")));
		m_NOList.push_back(ANoPair);
	}
	
	return 0;
}

int CNPCManager::SetNPCNewNO(DWORD dwNO)
{
	for (size_t nIndex = 0; nIndex < m_NOList.size(); nIndex++)
	{
		if (dwNO == m_NOList[nIndex].first)
		{
			m_NOList[nIndex].second = 0;
		}
	}
	return 0;
}

int CNPCManager::SaveNPCNOList(const string& strXmlFileName)
{
	//插入Xml声明
	TiXmlDocument* pXmlDoc = new TiXmlDocument(strXmlFileName.c_str());

	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	pXmlDoc->InsertEndChild(*pXmlDec);
	TiXmlElement* pXmlRootElement = new TiXmlElement("NPCNO");
	pXmlDoc->LinkEndChild(pXmlRootElement);

	for (vector<NO_PAIR>::iterator Ite = m_NOList.begin(); Ite != m_NOList.end(); Ite++)
	{
		TiXmlElement* pXmlNo = new TiXmlElement("NO");
		pXmlNo->SetAttribute("Used", Ite->second);

		stringstream strTemp;
		strTemp << Ite->first;
		string str;
		strTemp >> str;
		TiXmlText* pXmlText = new TiXmlText(str.c_str());
		pXmlNo->InsertEndChild(*pXmlText);
		pXmlRootElement->LinkEndChild(pXmlNo);
	}

	pXmlDoc->SaveFile();
	delete pXmlDoc;

	return 0;
}

#endif