﻿/** @file NPCCreator.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangshaodong
*	完成日期：2007-12-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME

#ifndef _NPCCREATOR_H
#define _NPCCREATOR_H

class TiXmlElement;

typedef struct NPCTarget
{
	unsigned int NPCNo;
	unsigned short Num;
	unsigned short Direction;
}NPCTarget;

class CNPCCreator
{
public:
	CNPCCreator(void);

	int LoadData(const TiXmlElement* pXmlNPCCreator);
	int SaveData(TiXmlElement* pXmlNPCCreator);
	int SetDirection(float fAngle);
	int SetPosition(int iXPos, int iYPos);
	
	int LoadDataM1(const TiXmlElement* pXmlNPCCreator);
	int SaveDataM1(TiXmlElement* pXmlNPCCreator);
	int AddNPCTarget(unsigned int NPCNo, unsigned short Num, unsigned short Direction);
	int UpdateNPCTarget(int index, unsigned int NPCNo, unsigned short Num);
public:
	virtual ~CNPCCreator(void);
public:
	unsigned int m_NPCCreatorNO; //生成器编号
	string m_CreatorName; //生成器名字
	unsigned int m_MapNO; //地图号
	string m_MapName; //地图名字
	int m_XPos;	//X坐标
	int m_YPos; //Y坐标
	int m_Row;  //格子行号
	int m_Col;  //格子列号
	int m_Radius; //生成半径
	int m_NPCNo; //NPC编号
	int m_RefreshNum; //生成数目
	int m_TimeInterval; //刷新时间
	int m_RefreshType;  //刷新方式 1：固定时间刷新 2：死亡后立即刷新
	int m_CreateType;	//生成方式 1：单独生成 2：群体生成
	int m_Direction;	//朝向 0：北 1： 东北 2：东 3：东南 4：南 5：西南  6：西 7：西北

	/*M1阶段，NPC生成方式改变，强调区域的概念。
	原先的属性保留m_NPCCreatorNO，m_CreatorName，m_MapNO,m_XPos,m_YPos
	m_MapName, m_Row和m_Col为冗余属性，剔除。
	m_Radius扩展为长和宽，m_RadiusX和m_RadiusY
	新填属性m_AreaMethod, m_TimeMethod, m_SupplyMethod
	时间方面的新增属性m_TimeFormat, m_StartTime, m_EndTime
	*/
	int m_RadiusX;
	int m_RadiusY;
	int m_AreaMethod;
	int m_TimeMethod;
	int m_SupplyMethod;
	int m_TimeFormat;
	string m_StartTime;
	string m_EndTime;
	vector<NPCTarget*> m_NPCTargetList;
};

#endif
#endif