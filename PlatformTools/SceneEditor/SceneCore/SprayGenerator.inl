
inline NiPoint3 CSprayGenerator::_Lerp(const NiPoint3& kStart, const NiPoint3& kEnd, float fInterValue)
{
	NiPoint3 kValue = kStart + (kEnd-kStart)*fInterValue;
	return kValue;
}
//---------------------------------------------------------------------
// 判断两个 NiPoint3 的位置是否重合
inline bool CSprayGenerator::_SimpleEqule(const NiPoint3& p0, const NiPoint3& p1)
{
	if (fabs(p0.x-p1.x)+fabs(p0.y-p1.y)+fabs(p0.z-p1.z) < EPSILON)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//---------------------------------------------------------------------
inline bool CSprayGenerator::_CheckExtrudeDirection(const NiPoint3& kBase, const NiPoint3& kDirection)
{
	// 从效率考虑不检查地形合法性了.
	static NiPoint3 kExtruded;
	kExtruded = kBase + kDirection * 0.1f;
	float fHeight = m_pTerrain->GetHeight(NiPoint2(kExtruded.x, kExtruded.y));
	if (fHeight < kExtruded.z)
	{
		// 顶点在地形之上
		return true;
	}
	else
	{
		return false;
	}
}
//---------------------------------------------------------------------