﻿/** @file SceneRenderView.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-03-05
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
//#include "NiSpeedTreeComponent.h"

namespace SceneCore
{

/**
@brief CWaterRenderView 水体渲染
* 
*/
class  MAIN_ENTRY CWaterRenderView: public NiRenderView
{
	NiDeclareRTTI;
public:
	///构造
	CWaterRenderView(NiCamera* pkCamera, NiCullingProcess* pkCullingProcess,
		bool bAlwaysUseCameraViewport = true);
	///析构
	virtual ~CWaterRenderView();

	///
	void SetCamera(NiCamera* pCamera);
	NiCamera* GetCamera() const;

	///
	void SetCullingProcess(NiCullingProcess* pCullingProcess);
	NiCullingProcess* GetCullingProcess() const;

	/// 
	void SetAlwaysUseCameraViewport(bool bAlwaysUseCameraViewport);
	bool GetAlwaysUseCameraViewport() const;

	/// 清理用于渲染反射纹理的 visible array
	void ClearVisibleArray();

	///获取渲染对象列表
	list<NiAVObject*>& GetRenderObjects();

	///实现虚方法
	virtual void SetCameraData(const NiRect<float>& kViewport);

	//NiVisibleArray& GetVisibleArray();
	
//#ifdef CODE_INGAME
	/// 设置渲染集
	void SetVisibleArray(NiVisibleArray* kFrameVisibleArray);
//#endif

protected:
	/// 实现虚方法
	virtual void CalculatePVGeometry();

	NiCameraPtr				m_spCamera;
	NiCullingProcessPtr		m_spCullingProcess;
	bool					m_bAlwaysUseCameraViewport;
	NiVisibleArray*			m_pkFrameVisibleArray;	// 本帧的渲染集，在 CalculatePVGeometry 时将拷贝到 m_kCachedPVGeometry 中
};

#include "WaterRenderView.inl"

NiSmartPointer(CWaterRenderView);

}; // end of namespace 
