﻿/** @file BloomEffect.h 
@brief 光晕效果，从客户端移植
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-04-18
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：
</pre>*/

#pragma once

#ifndef CODE_INGAME

#include "PostEffect.h"

namespace SceneCore
{

NiSmartPointer(CBloomEffect);

class MAIN_ENTRY CBloomEffect : public CPostEffect, public NiAVObject 
{
public:
	CBloomEffect(void);
	~CBloomEffect(void);

	/// 初始化 
	virtual bool Init(NiRenderedTexturePtr pkSourceTexture);

	/// 销毁
	virtual void Destory();

	//0811171 add by 和萌 for post effect manager
	virtual	const char* GetName();

	// add [5/27/2009 hemeng]
	// 更新参数
	void UpDateLum(float fLum);
	void UpDateScale(float fScale);

	virtual void PerformRendering();

private:
	CBloomEffect(CBloomEffect&);

	// add [5/27/2009 hemeng]
	// 添加BLOOM调节参数
	float							m_fBloom_Luminance;		
	float							m_fBloom_Scale;


protected:
	/// 创建渲染到全屏 render view
	void _CreateScreenFillingRenderViews();

	/// scale down 回调
	static bool			_PreScaleDownCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );
	// bloom combine回调函数 [6/15/2009 hemeng]
	static bool			_PreBloomCombineCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData);
	/// BlurU回调
	static bool			_PreBlurUCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );
	/// BlurV回调
	static bool			_PreBlurVCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );
	/// DrawScene/ScaleUp回调
	static bool			_PreDrawSceneCallback( NiRenderClick* pkCurrentRenderClick, void* pvCallbackData );

	/// Render-to-texture全屏View
	NiScreenFillingRenderViewPtr	m_spRTView;
	/// Texture Property
	NiTexturingProperty*			m_pkRTViewTexProp;	

	/// Render-to-Output RTG全屏View
	NiScreenFillingRenderViewPtr	m_spDrawSceneRTView;

	/// 原纹理宽度
	unsigned int					m_uiWidthSource;
	/// 原纹理高度
	unsigned int					m_uiHeightSource;
	/// 原纹理Texel尺寸
	NiPoint2						m_TexelSizeSource;
	/// 半尺寸纹理Texel尺寸
	NiPoint2						m_TexelSizeQtr;

	/// 四分之一尺寸纹理Texel宽
	float							m_fPerTexelWidthQtr;
	/// 四分之一尺寸纹理Texel高
	float							m_fPerTexelHeightQtr;

	//081117 add by 和萌 modified for post effect manager
	//输出target，绑定源texture
	NiRenderTargetGroupPtr			m_spOutputRenderTarget;
	NiRenderedTexturePtr			m_spSourceTexture;

	/// down pass 到 一半尺寸的 RTG
	NiRenderTargetGroupPtr			m_spRTGroupDownPassHalf;
	NiRenderedTexturePtr			m_spTextureDownPassHalf;

	/// down pass 到 1/4 尺寸的 RTG
	NiRenderTargetGroupPtr			m_spRTGroupDownPassQtr0;
	NiRenderedTexturePtr			m_spTextureDownPassQtr0;
	NiRenderTargetGroupPtr			m_spRTGroupDownPassQtr1;
	NiRenderedTexturePtr			m_spTextureDownPassQtr1;

	/// 缩小
	NiMaterialPtr					m_spScaleDownMtl;
	/// BlurU材质
	NiMaterialPtr					m_spBlurUMtl;
	/// BlurV材质
	NiMaterialPtr					m_spBlurVMtl;
	/// 放大材质
	NiMaterialPtr					m_spBloomCombineMtl;
	
};	// end of class
};	// end of namespace
#endif