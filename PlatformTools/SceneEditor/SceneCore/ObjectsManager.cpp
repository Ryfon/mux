﻿#include "StdAfx.h"

#ifndef CODE_INGAME

#include "ObjectsManager.h"

#include "Terrain.h"
#include "EditableBaseObj.h"

#define MAP_NO 1

CObjectsManager::CObjectsManager( CTerrain* pTerrain )
: m_pTerrain( pTerrain )
{}

CObjectsManager::~CObjectsManager(void)
{}

/// 添加单个物件
CEditableBaseObj*	CObjectsManager::AddObject(NiEntityInterface* pkEntityInterface, NiPoint3& vPos, const char *pszNifFile)
{
	//判断是不是新增的Object是不是NPC生成器
	/*int nCreateType;
	NiBool bPropExist = pkEntityInterface->GetPropertyData(CREATETYPE, nCreateType);*/

	int nCreateType;
	NiFixedString strTemp;	
	NiBool bPropExist = pkEntityInterface->GetPropertyData(CREATETYPE, strTemp);
	string strCreateType = strTemp;
	nCreateType = atol((strCreateType.substr(0,strCreateType.find(" "))).c_str());

	CEditableBaseObj* pObject = NULL;
	if (bPropExist)
	{
		CNpcCreatorObj* pNpcCreatorOb = new CNpcCreatorObj();
		pNpcCreatorOb->m_MapNO = MAP_NO;
		//设置NPC生成器属性
		CTerrain::m_NpcCreatorManager.AddNPCCreator(pNpcCreatorOb);
		pkEntityInterface->GetPropertyData(NPCNO, pNpcCreatorOb->m_NPCNo);
		pkEntityInterface->SetPropertyData(NPCCREATORNO, (int)pNpcCreatorOb->m_NPCCreatorNO);
		NiPoint3 NpcCreatorPos;
		pkEntityInterface->GetPropertyData(TRANSLATION, NpcCreatorPos);
		int nGridIndex = m_pTerrain->GetGridIndex(NpcCreatorPos);
		//pNpcCreatorOb->m_Col = nGridIndex / 
		//pNpcCreatorOb->m_Row = 
//		NiFixedString strCreatorName;
//		pkEntityInterface->GetPropertyData(CREATORNAME, strCreatorName);
//		pNpcCreatorOb->m_CreatorName = strCreatorName;
		int iPosX;
		int iPosY;
		this->m_pTerrain->GetGridPosInServer(NpcCreatorPos, iPosX, iPosY);
		pNpcCreatorOb->SetPosition(iPosX, iPosY);


		pkEntityInterface->GetPropertyData(REFRESHNUM, pNpcCreatorOb->m_RefreshNum);
		pkEntityInterface->GetPropertyData(TIMEINTERVAL, pNpcCreatorOb->m_TimeInterval);

		pkEntityInterface->GetPropertyData(REFRESHTYPE, strTemp);
		string strRefreshType = strTemp;
		pNpcCreatorOb->m_RefreshType = atol((strRefreshType.substr(0,strRefreshType.find(" "))).c_str());

		pkEntityInterface->GetPropertyData(RADIUS, pNpcCreatorOb->m_Radius);
		NiMatrix3 NpcCreatorMatrix;
		pkEntityInterface->GetPropertyData(ROTATION, NpcCreatorMatrix);
		//pNpcCreatorOb->m_Direction = 
		pNpcCreatorOb->m_CreateType = nCreateType;
		pObject = pNpcCreatorOb;
	}
	else
	{
		pObject = new CEditableBaseObj();
	}
	
	/*if ( pObject && pObject->Create( pszNifFile ) )
	{
		int iChunkIndx = m_pTerrain->GetChunkIndex( vPos );
		if ( iChunkIndx != INVALID_CHUNK )
		{
			m_pTerrain->m_pChunks[iChunkIndx].AddObject( pObject );
			return pObject;
		}
	}*/
	pObject->Create(pszNifFile);
	pObject->SetTranslate(vPos);
	
	m_ObjectMap[pkEntityInterface] = pObject;

	return pObject;
}

/// 添加单个物件
//CEditableBaseObj*	CObjectsManager::AddObject( NiPoint3& vPos, CEditableBaseObj* pObject )
//{
//	int iChunkIndx = m_pTerrain->GetChunkIndex( vPos );
//	if ( iChunkIndx != INVALID_CHUNK )
//	{
//		m_pTerrain->m_pChunks[iChunkIndx].AddObject( pObject );
//
//		// to do
//		// 设定网格blocked标志
//
//		return pObject;
//	}
//
//	return NULL; 
//}

/// 移除物件
bool	CObjectsManager::RemoveObject( NiEntityInterface* pkEntityInterface)
{
	map<NiEntityInterface*, CEditableBaseObj*>::iterator iter;
	iter = m_ObjectMap.find(pkEntityInterface);

	if ( iter != m_ObjectMap.end() )
	{
		CEditableBaseObj* pObj = static_cast<CEditableBaseObj*>(iter->second);
		//如果是NPCCreator，需要在NPCManager中进行删除操作
		if (pObj->GetObjectType() == CEditableBaseObj::OT_NPCCREATOR)
		{
			CNpcCreatorObj* pNpcCreatorObj = static_cast<CNpcCreatorObj*>(iter->second);
			//删除生成范围框
			stringstream strTemp;
			string strBuffer;
			strTemp << pNpcCreatorObj->m_NPCCreatorNO;
			strTemp >> strBuffer;
			NiFixedString strNPCCreatorNO(strBuffer.c_str());			

			CTerrain::m_NpcCreatorManager.DelNPCCreator(MAP_NO, pNpcCreatorObj->m_NPCCreatorNO);
			m_ObjectMap.erase(pkEntityInterface);
		}
		else
		{
			delete iter->second;
			m_ObjectMap.erase(pkEntityInterface);
		}
		return true;
	}
	else
	{
		return false;
	}
}

/// 移除所有物件
void	CObjectsManager::RemoveAllObjects()
{
	map<NiEntityInterface*, CEditableBaseObj*>::iterator iter;
	for(iter = m_ObjectMap.begin(); iter!= m_ObjectMap.end(); iter++)
	{
		CEditableBaseObj* pObj = static_cast<CEditableBaseObj*>(iter->second);
		//如果是NPCCreator，需要在NPCManager中进行删除操作
		if (pObj->GetObjectType() == CEditableBaseObj::OT_NPCCREATOR)
		{
			CNpcCreatorObj* pNpcCreatorObj = static_cast<CNpcCreatorObj*>(pObj);
			CTerrain::m_NpcCreatorManager.DelNPCCreator(pNpcCreatorObj->m_MapNO, pNpcCreatorObj->m_NPCCreatorNO);
		}
		else
		{
			delete iter->second;
		}
	}
	m_ObjectMap.clear();

}

/// 更新物件
void	CObjectsManager::UpdateObject(NiEntityInterface* pkEntityInterface, NiFixedString& pkPropertyName)
{
	map< NiEntityInterface*, CEditableBaseObj* >::iterator Ite = m_ObjectMap.find(pkEntityInterface);
	if (Ite == m_ObjectMap.end())
	{
		return;
	}
	CEditableBaseObj* pObj = Ite->second;
	if (pObj == NULL)
	{
		return;
	}
	
	if (pObj->GetObjectType() == CEditableBaseObj::OT_NPCCREATOR)
	{
		CNpcCreatorObj* pNpcOjb = static_cast<CNpcCreatorObj*>(pObj);
		if (pkPropertyName == CREATORNAME)
		{
			NiFixedString strCreateName;
			pkEntityInterface->GetPropertyData(CREATORNAME, strCreateName);
			pNpcOjb->m_CreatorName = strCreateName;
		}
		if (pkPropertyName == RADIUS)
		{
			pkEntityInterface->GetPropertyData(RADIUS, pNpcOjb->m_Radius);
		}
		if (pkPropertyName == REFRESHNUM)
		{
			pkEntityInterface->GetPropertyData(REFRESHNUM, pNpcOjb->m_RefreshNum);
		}
		if (pkPropertyName == TIMEINTERVAL)
		{
			pkEntityInterface->GetPropertyData(TIMEINTERVAL, pNpcOjb->m_TimeInterval);
		}
		if (pkPropertyName == REFRESHTYPE)
		{
			NiFixedString strTemp;
			pkEntityInterface->GetPropertyData(REFRESHTYPE, strTemp);
			string strRefreshType = strTemp;
			pNpcOjb->m_RefreshType = atol((strRefreshType.substr(0,strRefreshType.find(" "))).c_str());
		}
		if (pkPropertyName == CREATETYPE)
		{
			NiFixedString strTemp;
			pkEntityInterface->GetPropertyData(CREATETYPE, strTemp);
			string strCreateType = strTemp;
			pNpcOjb->m_CreateType = atol((strCreateType.substr(0,strCreateType.find(" "))).c_str());
		}
	}
	if (pkPropertyName == TRANSLATION)
	{
		NiPoint3 vPos;
		pkEntityInterface->GetPropertyData(pkPropertyName, vPos);
		m_ObjectMap[pkEntityInterface]->SetTranslate(vPos);
	}
	else if (pkPropertyName == ROTATION)
	{
		NiMatrix3 vRot;
		pkEntityInterface->GetPropertyData(pkPropertyName, vRot);
		m_ObjectMap[pkEntityInterface]->SetRotation(vRot);
	}
	else if (pkPropertyName == SCALE)
	{
		float fScale;
		pkEntityInterface->GetPropertyData(pkPropertyName, fScale);
		m_ObjectMap[pkEntityInterface]->SetScale(fScale, fScale, fScale);
	}

	if (pObj->GetObjectType() == CEditableBaseObj::OT_NPCCREATOR)
	{
		CNpcCreatorObj* pNpcOjb = static_cast<CNpcCreatorObj*>(pObj);
		//设置朝向信息
		NiMatrix3 vRot;
		pkEntityInterface->GetPropertyData(ROTATION, vRot);
		float fRotX = 0.0f;
		float fRotY = 0.0f;
		float fRotZ = 0.0f;
		vRot.ToEulerAnglesXYZ(fRotX,fRotY,fRotZ);		
		pNpcOjb->SetDirection(fRotZ);

		//计算格子信息
		int iPosX;
		int iPosY;
		this->m_pTerrain->GetGridPosInServer(pNpcOjb->GetPosition(), iPosX, iPosY);
		pNpcOjb->SetPosition(iPosX, iPosY);

		//渲染生成范围框
		//stringstream strTemp;
		//string strBuffer;
		//strTemp << pNpcOjb->m_NPCCreatorNO;
		//strTemp >> strBuffer;

		//NiFixedString strNPCCreatorNO(strBuffer.c_str());
		//if (pNpcOjb->m_CreateType != 2)
		//{
		//	this->m_pTerrain->CreateNPCRange(pNpcOjb->GetPosition(), -1, strNPCCreatorNO);
		//}
		//else
		//{
		//	this->m_pTerrain->CreateNPCRange(pNpcOjb->GetPosition(), pNpcOjb->m_Radius, strNPCCreatorNO);
		//}
	}
}

/// 点选物件
CEditableBaseObj*	CObjectsManager::GetEditalbeBaseObject( NiEntityInterface* pkEntityInterface)
{
	return m_ObjectMap[pkEntityInterface];
}

void CObjectsManager::PrintAll()
{
	map< NiEntityInterface*, CEditableBaseObj* >::iterator it = m_ObjectMap.begin();

	for (; it!= m_ObjectMap.end(); it++)
	{
		LOG2("ObjectMap:", it->first, it->second);
	}
}

bool CObjectsManager::IsExited(NiEntityInterface* pkEntityInterface)
{
	map<NiEntityInterface*, CEditableBaseObj*>::iterator it = m_ObjectMap.find(pkEntityInterface);

	if (it == m_ObjectMap.end())
	{
		return false;
	}
	else
	{
		return true;
	}
}

int CObjectsManager::Load(const char *pszFile)
{

	return 0;
}

int CObjectsManager::Save(const char* pszFile)
{
	return 0;
}

int CObjectsManager::SaveNPCCreator()
{
	CTerrain::m_NpcCreatorManager.SaveNPCCreator(NPCCREATORFILENAME);
	CTerrain::m_NpcCreatorManager.SaveNPCCreatorNo(NPCCREATORNOMNGFILENAME);
	return 0;
}

CEditableBaseObj* CObjectsManager::InitObject(NiEntityInterface* pEntityInf, const char* pszNifFile)
{
	//判断是不是新增的Object是不是NPC生成器
	int nCreateType;
	NiFixedString strTemp;	
	NiBool bPropExist = pEntityInf->GetPropertyData(CREATETYPE, strTemp);
	string strCreateType = strTemp;
	nCreateType = atol((strCreateType.substr(0,strCreateType.find(" "))).c_str());

	CEditableBaseObj* pObject = NULL;
	if (bPropExist)
	{
		int nCreatorNo;
		pEntityInf->GetPropertyData(NPCCREATORNO, nCreatorNo);
		CNpcCreatorObj* pNpcCreatorOb = static_cast<CNpcCreatorObj*>(CTerrain::m_NpcCreatorManager.GetNPCCreator(MAP_NO, nCreatorNo));

		//NPC已经存在，只需更新位置，朝向，和缩放
		if (pNpcCreatorOb != NULL)
		{
			//初始化位置
			NiPoint3 NpcCreatorPos;
			pEntityInf->GetPropertyData(TRANSLATION, NpcCreatorPos);
			pNpcCreatorOb->SetTranslate(NpcCreatorPos);

			int iPosX;
			int iPosY;
			this->m_pTerrain->GetGridPosInServer(NpcCreatorPos, iPosX, iPosY);
			pNpcCreatorOb->SetPosition(iPosX, iPosY);

			//初始化朝向
			NiMatrix3 vNpcCreatorRot;
			pEntityInf->GetPropertyData(ROTATION, vNpcCreatorRot);
			pNpcCreatorOb->SetRotation(vNpcCreatorRot);
			float fRotX = 0.0f;
			float fRotY = 0.0f;
			float fRotZ = 0.0f;
			vNpcCreatorRot.ToEulerAnglesXYZ(fRotX,fRotY,fRotZ);
			pNpcCreatorOb->SetDirection(fRotZ);
			//初始化缩放
			float fNpcCreatorScale;
			pEntityInf->GetPropertyData(SCALE, fNpcCreatorScale);
			pNpcCreatorOb->SetScale(fNpcCreatorScale,fNpcCreatorScale,fNpcCreatorScale);

			//更新生成范围框
			//stringstream strTemp;
			//string strBuffer;
			//strTemp << pNpcCreatorOb->m_NPCCreatorNO;
			//strTemp >> strBuffer;

			//NiFixedString strNPCCreatorNO(strBuffer.c_str());
			//if (pNpcCreatorOb->m_CreateType != 2)
			//{
			//	this->m_pTerrain->CreateNPCRange(pNpcCreatorOb->GetPosition(), -1, strNPCCreatorNO);
			//}
			//else
			//{
			//	this->m_pTerrain->CreateNPCRange(pNpcCreatorOb->GetPosition(), pNpcCreatorOb->m_Radius, strNPCCreatorNO);
			//}
		} 
		else
		{
			pNpcCreatorOb = new CNpcCreatorObj();
			pNpcCreatorOb->m_MapNO = MAP_NO;
			//设置NPC生成器属性
			CTerrain::m_NpcCreatorManager.AddNPCCreator(pNpcCreatorOb);
			pEntityInf->SetPropertyData(NPCCREATORNO, (int)pNpcCreatorOb->m_NPCCreatorNO);
			pEntityInf->GetPropertyData(NPCNO, pNpcCreatorOb->m_NPCNo);
			
			//pNpcCreatorOb->m_Col = nGridIndex / 
			//pNpcCreatorOb->m_Row = 
			NiFixedString strCreatorName;
			pEntityInf->GetPropertyData(CREATORNAME, strCreatorName);
			if (strCreatorName.Exists())
			{
				pNpcCreatorOb->m_CreatorName = strCreatorName;
			}

			pEntityInf->GetPropertyData(REFRESHNUM, pNpcCreatorOb->m_RefreshNum);
			pEntityInf->GetPropertyData(TIMEINTERVAL, pNpcCreatorOb->m_TimeInterval);

			pEntityInf->GetPropertyData(REFRESHTYPE, strTemp);
			string strRefreshType = strTemp;
			pNpcCreatorOb->m_RefreshType = atol((strRefreshType.substr(0,strRefreshType.find(" "))).c_str());

			pEntityInf->GetPropertyData(RADIUS, pNpcCreatorOb->m_Radius);

			NiMatrix3 NpcCreatorMatrix;
			pEntityInf->GetPropertyData(ROTATION, NpcCreatorMatrix);
			pNpcCreatorOb->SetRotation(NpcCreatorMatrix);
			pNpcCreatorOb->m_CreateType = nCreateType;

			//初始化位置
			NiPoint3 NpcCreatorPos;
			pEntityInf->GetPropertyData(TRANSLATION, NpcCreatorPos);
			pNpcCreatorOb->SetTranslate(NpcCreatorPos);

			int iPosX;
			int iPosY;
			this->m_pTerrain->GetGridPosInServer(NpcCreatorPos, iPosX, iPosY);
			pNpcCreatorOb->SetPosition(iPosX, iPosY);

			//初始化朝向
			NiMatrix3 vNpcCreatorRot;
			pEntityInf->GetPropertyData(ROTATION, vNpcCreatorRot);
			pNpcCreatorOb->SetRotation(vNpcCreatorRot);
			float fRotX = 0.0f;
			float fRotY = 0.0f;
			float fRotZ = 0.0f;
			vNpcCreatorRot.ToEulerAnglesXYZ(fRotX,fRotY,fRotZ);
			pNpcCreatorOb->SetDirection(fRotZ);
			//初始化缩放
			float fNpcCreatorScale;
			pEntityInf->GetPropertyData(SCALE, fNpcCreatorScale);
			pNpcCreatorOb->SetScale(fNpcCreatorScale,fNpcCreatorScale,fNpcCreatorScale);

			//更新生成范围框
			//stringstream strTemp;
			//string strBuffer;
			//strTemp << pNpcCreatorOb->m_NPCCreatorNO;
			//strTemp >> strBuffer;

			//NiFixedString strNPCCreatorNO(strBuffer.c_str());
			//if (pNpcCreatorOb->m_CreateType != 2)
			//{
			//	this->m_pTerrain->CreateNPCRange(pNpcCreatorOb->GetPosition(), -1, strNPCCreatorNO);
			//}
			//else
			//{
			//	this->m_pTerrain->CreateNPCRange(pNpcCreatorOb->GetPosition(), pNpcCreatorOb->m_Radius, strNPCCreatorNO);
			//}
		}

		pObject = pNpcCreatorOb;
	}
	else
	{
		pObject = new CEditableBaseObj();
	}

	m_ObjectMap[pEntityInf] = pObject;

	return pObject;

}

#endif