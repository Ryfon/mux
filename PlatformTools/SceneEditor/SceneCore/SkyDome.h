﻿/** @file SkyDome.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-09-28
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 *  备	  注：暂时没有用到
 </pre>*/

#ifndef SKYDOME_H 
#define SKYDOME_H

/**
 @brief 天空盒类
 *
 *	负责天空的亮度变化,,, 注: 考虑到影子静态处理, 暂不加入日夜变化
 */
class MAIN_ENTRY CSkyDome
{
public: 

	/// 构造
	CSkyDome(void);

	/// 析构
	virtual ~CSkyDome(void);

	/**
	*	<BR>功能说明：更新
	*	<BR>可访问性：
	*	<BR>注    释：
	*	@param	dTime[in]			总时间
	*	@param	fElapsedTime[in]	从上一帧的消逝时间
	*	@return 无
	*/
	void					Update( double dTime, float fElapsedTime );

	/// 渲染
	void					Render( NiCamera *pCamera, NiCullingProcess *pCuller, NiVisibleArray *pVisible );
};

#endif