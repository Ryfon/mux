﻿/** @file GameScene.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-09-28
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 *  
 *	备	  注：暂时没有用到
 </pre>*/

#ifndef CODE_INGAME

#ifndef GAME_SCENE_H 
#define GAME_SCENE_H

/// 前置声明
class CTerrain;
class CSkyDome;
class CEditableBaseObj;

/**
 @brief	游戏静态场景类
 *
 * 用于生成用户在工具端生成的场景数据
 */
class MAIN_ENTRY CGameScene
{
public:
	/// 构造
	CGameScene(void);

	/// 析构
	virtual ~CGameScene(void);

	/**
	 *	<BR>功能说明：更新
	 *	<BR>可访问性：
	 *	<BR>注    释：
	 *	@param	dTime[in]			总时间
	 *	@param	fElapsedTime[in]	从上一帧的消逝时间
	 *	@return 无
	 */
	void					Update( double dTime, float fElapsedTime );

	/// 渲染
	void					Render( NiCamera *pCamera, NiCullingProcess *pCuller, NiVisibleArray *pVisible );

	/// 设置活动相机
	void					SetActiveCamera( NiCamera* pCamera );

	/// 导出
	bool					SaveToFile( const char *pszFile );

	/// 导入
	bool					LoadFromFile( const char* pszFile );

protected:
////////////////////////天与地////////////////////////////////////////////////////

	/// 天空盒
	CSkyDome			*m_pSkyDome;

	/// 地形
	CTerrain			*m_pTerrain;
};

#endif
#endif