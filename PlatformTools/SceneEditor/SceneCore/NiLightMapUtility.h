﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2008 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Calabasas, CA 91302
// http://www.emergent.net

#ifndef NILIGHTMAPUTILITY_H
#define NILIGHTMAPUTILITY_H

#ifndef CODE_INGAME

#include "NiLightMapFunctor.h"
#include <NiGeometry.h>
#include <NiEntity.h>


#define MAX_LIGHTMAP_SIZE 2048;

class MAIN_ENTRY NiLightMapUtility
{
public:
    static bool LoadLightMaps(
        NiScene* pkScene, 
        NiString kLightMapDirectory, 
        NiString kExtension = NiString("tga"));

    static bool VisitLightMapMeshes(
        const char* pcGSAFilename, 
        NiVisitLightMapMeshFunctor &kFunctor);

    static bool VisitLightMapMeshes(
        NiScene* pkScene, 
        NiVisitLightMapMeshFunctor &kFunctor);

    static bool VisitLightMapMeshes(
        NiEntityInterface* pkEntity, 
        NiVisitLightMapMeshFunctor &kFunctor);

    static bool VisitLightMapMeshes(
        NiNode* pkNode, 
        NiVisitLightMapMeshFunctor &kFunctor,
        NiEntityInterface* pkEntity);

    static bool VisitLightMapLights(
        const char* pcGSAFilename, 
        NiVisitLightMapLightFunctor &kFunctor);

    static bool VisitLightMapLights(
        NiScene* pkScene, 
        NiVisitLightMapLightFunctor &kFunctor);
    
    static bool VisitLightMapLights(
        NiEntityInterface* pkEntity, 
        NiVisitLightMapLightFunctor &kFunctor);

    static int GetLightMapUVSetIndex(NiGeometry* pkGeom);
    //static void GetLightMapResolution(
    //    int& iU,
    //    int& iV,
    //    NiGeometry* pkGeom, 
    //    float fTexelsPerWorldUnit = 1.0, 
    //    int iMinSize = 32,
    //    int iMaxSize = 1024);
    static NiNode* GetSceneGraphNode(NiEntityInterface* pkEntity);

    static bool IsLightMapMesh(NiGeometry* pkGeom);

    // *** begin Emergent internal use only ***
    static NiString GenerateLightMapName(
        NiAVObject* pkObject, 
        unsigned int uiTraversalID);

    static void RemoveLightMapNodes(NiLight* pkLight);
    static void RemoveLightMapEntities(NiLightComponent* pkComponent);
    static bool LoadLightMap(NiGeometry* pkGeom, const char* pcFilename);   
    static int FloatCompare(const void* pv0, const void* pv1);
    // *** end Emergent internal use only ***
};

#include "NiLightMapUtility.inl"

#endif // CODE_INGAME
#endif // NILIGHTMAPUTILITY_H