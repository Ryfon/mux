﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2008 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Calabasas, CA 91302
// http://www.emergent.net
#include "stdafx.h"
#include "NiLightMapMaterial.h"

#ifndef CODE_INGAME

#include <NiStandardMaterialNodeLibrary.h>
#if defined(_WII)
#include "NiWiiLightMapDefaultShader.h"
#endif

#if !defined(_WII)
NiImplementRTTI(NiLightMapMaterial, NiStandardMaterial);

NiLightMapMaterial::NiLightMapMaterial(): NiStandardMaterial("LightMapMaterial", 0, true)
{
    m_bForcePerPixelLighting = true;
    m_kLibraries.Add(NiStandardMaterialNodeLibrary::CreateMaterialNodeLibrary());
    AddDefaultFallbacks();
	
}
//-----------------------------------------------------------------------------------------------
NiLightMapMaterial::~NiLightMapMaterial()
{
    
}
//-----------------------------------------------------------------------------------------------
bool NiLightMapMaterial::HandleCustomMaps(Context& kContext, 
										  NiStandardPixelProgramDescriptor* pkPixDesc, 
										  unsigned int& uiWhichTexture, NiMaterialResource** apkUVSets, 
										  unsigned int uiNumStandardUVs, NiMaterialResource*& pkMatDiffuseColor, 
										  NiMaterialResource*& pkMatSpecularColor, NiMaterialResource*& pkMatSpecularPower, 
										  NiMaterialResource*& pkMatAmbientColor, 
										  NiMaterialResource*& pkMatEmissiveColor, 
										  NiMaterialResource*& pkDiffuseLightAccum, 
										  NiMaterialResource*& pkSpecularLightAccum)
{
    NiMaterialResource* pkOpacityRes = NULL;

    NiMaterialResource*& inOut = pkDiffuseLightAccum;
    //NIASSERT(inOut);

	//bool InsertTexture(
	//	Context& kContext, 
	//	TextureMap eMap,
	//	unsigned int uiOccurance, 
	//	TextureMapApplyType eApplyType, 
	//	TextureMapSampleType eSample, 
	//	NiMaterialResource* pkUV, 
	//	NiMaterialResource* pkInputColor, 
	//	NiMaterialResource* pkSampledRGBScalar, 
	//	NiMaterialResource*& pkOutputColor, 
	//	NiMaterialResource*& pkOutputOpacity)

    // Add lightmap to diffuse lighting
    InsertTexture(
        kContext, 
        MAP_CUSTOM00, 
        0, 
        TEXTURE_RGB_APPLY_ADD, //TEXTURE_RGB_APPLY_MULTIPLY, TEXTURE_RGB_APPLY_NOOP
        TEXTURE_SAMPLE_RGB, 
        FindUVSetIndexForTextureEnum(
            MAP_CUSTOM00, 
            pkPixDesc, 
            apkUVSets), 
        inOut,//
        NULL, 
        inOut, 
        pkOpacityRes);


    return true;
}
#else
NiImplementRTTI(NiLightMapMaterial, NiSingleShaderMaterial);

//-----------------------------------------------------------------------------------------------
NiLightMapMaterial::NiLightMapMaterial() : 
    NiSingleShaderMaterial("LightMapMaterial", NiNew NiWiiLightMapDefaultShader, false)
{
}
//-----------------------------------------------------------------------------------------------
NiLightMapMaterial::~NiLightMapMaterial()
{
}
#endif

//---------------------------------------------------------------------------
NiImplementSDMConstructor(NiLightMapMaterial);// , "NiMain"
//---------------------------------------------------------------------------
void NiLightMapMaterialSDM::Init()
{
    NiImplementSDMInitCheck();
    NiLightMapMaterialLibrary::Create();
}
//---------------------------------------------------------------------------
void NiLightMapMaterialSDM::Shutdown()
{
    NiImplementSDMShutdownCheck();
    NiLightMapMaterialLibrary::Shutdown();
}
//---------------------------------------------------------------------------

#endif // CODE_INGAME
