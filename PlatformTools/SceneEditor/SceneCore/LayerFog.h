﻿/** @file LayerFog.h 
@brief 层雾
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-02-02
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#include "IDepthAlphaEffect.h"

namespace SceneCore
{

NiSmartPointer(CLayerFog);

/**
@brief 层雾效果
* 用带alpha通道的纹理[动画]实现体积雾
*/
class MAIN_ENTRY CLayerFog : public IDepthAlphaEffect
{
	NiDeclareRTTI;
public:
	CLayerFog(void);
	virtual ~CLayerFog(void);

	/// 
	/**
	*	<BR>功能说明： 创建层雾
	*	<BR>可访问性：		
	*	<BR>注    释：	   
	*	@param	kPosition[in]			雾体所在位置
	*	@param	kRotation[in]			雾体的旋转
	*	@param	fSize[in]				雾表面尺寸
	*	@param	pkFogShapeSceneRoot[in]	描述体雾形状的几何体场景
	*	@param	fDensity[in]			浓度,雾颜色的每米增加度
		@param	kColor[in]				颜色
	*	@return							创建是否成功
	*/
	bool		Init(NiAVObjectPtr pkFogShapeSceneRoot, const NiPoint3& kPosition, 
							const NiMatrix3& kRotation, const float& fScale, const float fDensity=0.1f, const NiColor& kColor=NiColor(1,1,1));

	/// 获取材质
	NiMaterialPtr		GetMaterial();

	/// 设置世界变换
	void SetTranslate(const NiPoint3& kTranslation);
	const NiPoint3& GetTranslate() const;

	void SetRotate(const NiMatrix3& kRotation);
	const NiMatrix3& GetRotate() const;

	void SetScale(const float fScale);
	float GetScale() const;

	/// 设置雾的浓度
	void SetAlpha(const float fAlpha);
	float GetAlpha() const;

	/// 设置颜色
	void SetColor(const NiColor kColor);
	const NiColor& GetColor() const;

	/// 设置描述体雾形状的场景
	void SetSceneRoot(NiAVObjectPtr pkSceneRoot);
	NiAVObjectPtr GetSceneRoot();
	/// 
	void AddGeometry(NiGeometry* pkGeo);

	// 重写父类虚函数部分
	/// 设置深度纹理
	virtual void SetDepthTexture(NiTexturePtr pkDepthTexture);

	/// 销毁 释放占用资源，智能指针置 NULL
	virtual void Destory();

	/// 更新
	virtual void Update(float fTime);

	///构造可视集
	virtual void BuildVisibleSet(NiEntityRenderingContextPtr	pkRenderingCondtex);

protected:
	/// 遍历一个 NiAVObject 所有子节点，搜索 Geometry 并且设置　LayerFog material
	void _RecursiveFindAndSetLayerFogMaterial(NiAVObject*	pkSceneRoot);

	NiAVObjectPtr		m_pkSceneRoot;	// 雾表面几何体
	NiMaterialPtr		m_spLayerFogMtl;// 材质
	NiPoint3			m_kTranslation;	// 位置
	NiMatrix3			m_kRotation;	// 旋转
	float				m_fScale;		// 缩放
	float				m_fDensity;		// 浓度 每米增加
	NiColor				m_kColor;		// 雾的颜色
	bool				m_bInitialized;	// 是否初始化过
	vector<NiGeometry*> m_GeometryList;	// SceneRoot 中所有 NiGeometry 列表
};

};
