﻿#include "StdAfx.h"
#ifndef CODE_INGAME
#include "NPCCreator.h"
#include "NPCCreatorManager.h"
#include "EditableBaseObj.h"
#include "Terrain.h"

CNPCCreatorManager::CNPCCreatorManager(void)
{

}
CNPCCreatorManager::CNPCCreatorManager(CTerrain *pTerrain)
:m_pTerrain(pTerrain)
{

}

CNPCCreatorManager::~CNPCCreatorManager(void)
{
	CleanUp();
}

int CNPCCreatorManager::LoadNPCCreator(const string& strXmlFileName)
{
	TiXmlDocument XmlDoc(strXmlFileName.c_str());
	if (!XmlDoc.LoadFile())
	{
		return 0;
	}
	TiXmlElement* pRootElement = XmlDoc.RootElement();
	m_FileVersion = pRootElement->Attribute("FileVersion");
	for (const TiXmlElement* pXmlMap = pRootElement->FirstChildElement(); pXmlMap; pXmlMap = pXmlMap->NextSiblingElement())
	{
		DWORD dwMapNO = atol(pXmlMap->Attribute("MapNo"));
		PERMAP_NPCCREATOR* pPerMapNPCCreator = new PERMAP_NPCCREATOR;
		for (const TiXmlElement* pXmlNPCCreator = pXmlMap->FirstChildElement(); pXmlNPCCreator; pXmlNPCCreator = pXmlNPCCreator->NextSiblingElement() )
		{
			//创建NPCCreator
			CNPCCreator* pNPCCreator = new CNpcCreatorObj;
			//pNPCCreator->LoadData(pXmlNPCCreator);
			pNPCCreator->LoadDataM1(pXmlNPCCreator);
			DWORD dwNPCCreatorNo = pNPCCreator->m_NPCCreatorNO;
			(*pPerMapNPCCreator)[dwNPCCreatorNo] = pNPCCreator;
		}
		m_NPCCreatorList[dwMapNO] = pPerMapNPCCreator;
	}

	return 0;
}

int CNPCCreatorManager::SaveNPCCreator(const string& strXmlFileName)
{
	//插入Xml声明
	TiXmlDocument* pXmlDoc = new TiXmlDocument(strXmlFileName.c_str());
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	pXmlDoc->InsertEndChild(*pXmlDec);
	TiXmlElement* pXmlRootElement = new TiXmlElement("NPCCreatorInfo");
	pXmlRootElement->SetAttribute("FileVersion", "0.9");
	pXmlDoc->LinkEndChild(pXmlRootElement);

	for (MAP_NPCCREATOR::iterator Ite = m_NPCCreatorList.begin(); Ite != m_NPCCreatorList.end(); Ite++)
	{
		TiXmlElement* pXmlMap = new TiXmlElement("Map");
		pXmlMap->SetAttribute("MapNo", Ite->first);
		PERMAP_NPCCREATOR* pPerMapNPCCreator = Ite->second;
		for (PERMAP_NPCCREATOR::iterator IteNpcCreator = (*pPerMapNPCCreator).begin(); IteNpcCreator != (*pPerMapNPCCreator).end(); IteNpcCreator++)
		{
			CNPCCreator* pNPCCreator = IteNpcCreator->second;
			TiXmlElement* pXmlNpcCreator = new TiXmlElement("NPCCreator");
			//pNPCCreator->SaveData(pXmlNpcCreator);
			pNPCCreator->SaveDataM1(pXmlNpcCreator);
			pXmlMap->LinkEndChild(pXmlNpcCreator);
		}
		pXmlRootElement->LinkEndChild(pXmlMap);
	}

	pXmlDoc->SaveFile();

	delete pXmlDoc;

	return 0;
}

CNPCCreator* CNPCCreatorManager::AddNPCCreator(DWORD dwMapNo)
{
	CNPCCreator* pNpcCreator = new CNpcCreatorObj;
	pNpcCreator->m_NPCCreatorNO = GetNPCCreatorNO();
	pNpcCreator->m_MapNO = dwMapNo;

	//查找地图
	MAP_NPCCREATOR::iterator Ite = m_NPCCreatorList.find(dwMapNo);
	//该地图不在NPC列表中
	PERMAP_NPCCREATOR* pPerMapNpcCreator;
	if (Ite == m_NPCCreatorList.end())
	{
		pPerMapNpcCreator = new PERMAP_NPCCREATOR;
		(*pPerMapNpcCreator)[pNpcCreator->m_NPCCreatorNO] = pNpcCreator;
		m_NPCCreatorList[dwMapNo] = pPerMapNpcCreator;
		stringstream strTemp;
		strTemp << pNpcCreator->m_NPCCreatorNO;
		strTemp >> pNpcCreator->m_CreatorName;
		
	}
	else
	{
		pPerMapNpcCreator = Ite->second;
		(*pPerMapNpcCreator)[pNpcCreator->m_NPCCreatorNO] = pNpcCreator;
	}
	return pNpcCreator;
}

int CNPCCreatorManager::CleanUp()
{
	for (MAP_NPCCREATOR::iterator Ite = m_NPCCreatorList.begin(); Ite != m_NPCCreatorList.end(); Ite++)
	{
		PERMAP_NPCCREATOR* pPerMapNPCCreator = Ite->second;
		for (PERMAP_NPCCREATOR::iterator IteNpcCreator = (*pPerMapNPCCreator).begin(); IteNpcCreator != (*pPerMapNPCCreator).end(); IteNpcCreator++)
		{
			delete IteNpcCreator->second;
		}
		pPerMapNPCCreator->clear();
		delete pPerMapNPCCreator;
	}
	m_NPCCreatorList.clear();
	return 0;
}

CNPCCreator* CNPCCreatorManager::GetNPCCreator(DWORD dwMapNo, DWORD dwNPCCreatorNO)
{
	//查找地图
	MAP_NPCCREATOR::iterator Ite = m_NPCCreatorList.find(dwMapNo);
	if (Ite == m_NPCCreatorList.end())
	{
		return NULL;
	}

	//查找该地图对应的NPC生成器
	PERMAP_NPCCREATOR* pPerMapNpcCreator = Ite->second;
	PERMAP_NPCCREATOR::iterator ItePerMap = pPerMapNpcCreator->find(dwNPCCreatorNO);
	if (ItePerMap == pPerMapNpcCreator->end())
	{
		return NULL;
	}
	return ItePerMap->second;
}

int CNPCCreatorManager::DelNPCCreator(DWORD dwMapNo, DWORD dwNPCCreatorNO)
{
	MAP_NPCCREATOR::iterator Ite = m_NPCCreatorList.find(dwMapNo);
	if (Ite != m_NPCCreatorList.end())
	{
		PERMAP_NPCCREATOR* pPerMap = Ite->second;
		PERMAP_NPCCREATOR::iterator ItePerMap = pPerMap->find(dwNPCCreatorNO);
		if (ItePerMap != pPerMap->end())
		{
			CNPCCreator* pNpcCreator = ItePerMap->second;
			SetNPCCreatorNO(pNpcCreator->m_NPCCreatorNO);
			delete ItePerMap->second;
			pPerMap->erase(ItePerMap);
		}
	}
	return 0;
}

/*
NO长度：4字节
从高位到低位依次的含义：
第一个字节：NO类型，总的分类 1：功能NPC 2：怪物 3：道具 4：地图
第二个字节：预留，每个分类可以自己使用
后两个字节：表示NO
*/
DWORD CNPCCreatorManager::GetNPCCreatorNO()
{
	for (size_t nIndex = 0; nIndex < m_NOList.size(); nIndex++)
	{
		if (m_NOList[nIndex].second == 0)
		{
			m_NOList[nIndex].second = 1;
			return m_NOList[nIndex].first;
		}
	}

	DWORD dwNo = 0;
	if (m_NOList.size() != 0)
	{
		dwNo = m_NOList[m_NOList.size() - 1].first + 1;
	}
	else
	{
		dwNo = 1;
		//最高字节置1
		dwNo = dwNo << 25;
		dwNo++;
	}

	NO_PAIR ANoPair = make_pair(dwNo, 1);
	m_NOList.push_back(ANoPair);
	return dwNo;
	return 1;
}

int CNPCCreatorManager::LoadNPCCreatorNo(const string strXmlFileName)
{
	m_NOList.clear();
	TiXmlDocument XmlDoc(strXmlFileName.c_str());
	if (!XmlDoc.LoadFile())
	{
		return 0;
	}

	TiXmlElement* pRootElement = XmlDoc.RootElement();

	for (const TiXmlElement* pNPCElement = pRootElement->FirstChildElement(); pNPCElement; 
		pNPCElement = pNPCElement->NextSiblingElement())
	{
		NO_PAIR ANoPair = make_pair(atol(pNPCElement->GetText()), atoi(pNPCElement->Attribute("Used")));
		m_NOList.push_back(ANoPair);
	}

	return 0;
}

int CNPCCreatorManager::SetNPCCreatorNO(DWORD dwNO)
{
	for (size_t nIndex = 0; nIndex < m_NOList.size(); nIndex++)
	{
		if (dwNO == m_NOList[nIndex].first)
		{
			m_NOList[nIndex].second = 0;
		}
	}
	return 0;
}

int CNPCCreatorManager::SaveNPCCreatorNo(const string strXmlFileName)
{
	//插入Xml声明
	TiXmlDocument* pXmlDoc = new TiXmlDocument(strXmlFileName.c_str());

	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	pXmlDoc->InsertEndChild(*pXmlDec);
	TiXmlElement* pXmlRootElement = new TiXmlElement("NPCCreatorNO");
	pXmlDoc->LinkEndChild(pXmlRootElement);

	for (vector<NO_PAIR>::iterator Ite = m_NOList.begin(); Ite != m_NOList.end(); Ite++)
	{
		TiXmlElement* pXmlNo = new TiXmlElement("NO");
		pXmlNo->SetAttribute("Used", Ite->second);
		string strTem;
		stringstream ssTemp;
		ssTemp << Ite->first;
		ssTemp >> strTem;
		TiXmlText* pXmlText = new TiXmlText(strTem.c_str());
		pXmlNo->InsertEndChild(*pXmlText);
		pXmlRootElement->LinkEndChild(pXmlNo);
	}

	pXmlDoc->SaveFile();
	delete pXmlDoc;
	return 0;
}

int CNPCCreatorManager::AddNPCCreator(CNPCCreator* pNpcCreator)
{
	pNpcCreator->m_NPCCreatorNO = GetNPCCreatorNO();
	//查找地图
	MAP_NPCCREATOR::iterator Ite = m_NPCCreatorList.find(pNpcCreator->m_MapNO);
	//该地图不在NPC列表中
	PERMAP_NPCCREATOR* pPerMapNpcCreator;
	if (Ite == m_NPCCreatorList.end())
	{
		pPerMapNpcCreator = new PERMAP_NPCCREATOR;
		(*pPerMapNpcCreator)[pNpcCreator->m_NPCCreatorNO] = pNpcCreator;
		m_NPCCreatorList[pNpcCreator->m_MapNO] = pPerMapNpcCreator;
	}
	else
	{
		pPerMapNpcCreator = Ite->second;
		(*pPerMapNpcCreator)[pNpcCreator->m_NPCCreatorNO] = pNpcCreator;
	}

	return 0;
}

bool CNPCCreatorManager::EntityIsNPC(NiEntityInterface* pkEntityInterface)
{	
	NiFixedString strTemp;	
	NiBool bPropExist = pkEntityInterface->GetPropertyData(CREATORNAME, strTemp);
	
	if (bPropExist)
	{
		return true;
	}

	return false;
}

bool CNPCCreatorManager::IsExited(DWORD dwMapNo, NiEntityInterface* pkEntityInterface)
{
	 int dwNPCCreatorNO;
	pkEntityInterface->GetPropertyData(NPCCREATORNO, dwNPCCreatorNO);

	return IsExited(dwMapNo,dwNPCCreatorNO);
}

bool CNPCCreatorManager::IsExited(DWORD dwMapNo, DWORD dwNPCCreatorNO)
{
	MAP_NPCCREATOR::iterator IteMap = m_NPCCreatorList.find(dwMapNo);
	if (IteMap != m_NPCCreatorList.end())
	{
		PERMAP_NPCCREATOR* pNPCCreatorMap = IteMap->second;
		PERMAP_NPCCREATOR::iterator IteNPCMap = pNPCCreatorMap->find(dwNPCCreatorNO);
		if (IteNPCMap != pNPCCreatorMap->end())
		{
			return true;
		}
	}

	return false;
}

bool CNPCCreatorManager::AddNPCCreator(DWORD dwMapNo, NiEntityInterface* pkEntityInterface)
{
	CNpcCreatorObj* pNpcCreatorOb = new CNpcCreatorObj();
	pNpcCreatorOb->m_MapNO = dwMapNo;
	this->AddNPCCreator(pNpcCreatorOb);
	//设置NPC生成器属性
	int nCreateType;
	NiFixedString strTemp;	
	pkEntityInterface->GetPropertyData(CREATETYPE, strTemp);
	string strCreateType = strTemp;
	nCreateType = atol((strCreateType.substr(0,strCreateType.find(" "))).c_str());
	pNpcCreatorOb->m_CreateType = nCreateType;

	//M1新增
	NiBool pIsSingle = pkEntityInterface->GetPropertyData(NPCNO, pNpcCreatorOb->m_NPCNo);
	if (pIsSingle)
	{
		pNpcCreatorOb->AddNPCTarget(pNpcCreatorOb->m_NPCNo, 1 ,0);
		pNpcCreatorOb->m_AreaMethod = 1;
	}
	else
	{
		pNpcCreatorOb->m_AreaMethod = 2;
		pkEntityInterface->GetPropertyData(RADIUSX, pNpcCreatorOb->m_RadiusX);//M1新增
		pkEntityInterface->GetPropertyData(RADIUSY, pNpcCreatorOb->m_RadiusY);//M1新增
		pkEntityInterface->GetPropertyData(TIMEFORMAT, pNpcCreatorOb->m_TimeFormat);//M1新增
		NiFixedString strStartTime;
		pkEntityInterface->GetPropertyData(STARTTIME, strStartTime);//M1新增
		pNpcCreatorOb->m_StartTime = strStartTime;
		NiFixedString strEndTime;
		pkEntityInterface->GetPropertyData(ENDTIME, strEndTime);//M1新增
		pNpcCreatorOb->m_EndTime = strEndTime;
	}
	//M1新增

	pkEntityInterface->SetPropertyData(NPCCREATORNO, (int)pNpcCreatorOb->m_NPCCreatorNO);

	NiPoint3 NpcCreatorPos;
	pkEntityInterface->GetPropertyData(TRANSLATION, NpcCreatorPos);
	int nGridIndex = m_pTerrain->GetGridIndex(NpcCreatorPos);
	
	int iPosX;
	int iPosY;
	this->m_pTerrain->GetGridPosInServer(NpcCreatorPos, iPosX, iPosY);
	pNpcCreatorOb->SetPosition(iPosX, iPosY);


	//pkEntityInterface->GetPropertyData(REFRESHNUM, pNpcCreatorOb->m_RefreshNum);//M1废除

	//pkEntityInterface->GetPropertyData(TIMEINTERVAL, pNpcCreatorOb->m_TimeInterval);//M1废除

	pkEntityInterface->GetPropertyData(REFRESHTYPE, strTemp);
	string strRefreshType = strTemp;
	pNpcCreatorOb->m_RefreshType = atol((strRefreshType.substr(0,strRefreshType.find(" "))).c_str());

	//pkEntityInterface->GetPropertyData(RADIUS, pNpcCreatorOb->m_Radius);//M1废除
	

	NiMatrix3 NpcCreatorMatrix;
	pkEntityInterface->GetPropertyData(ROTATION, NpcCreatorMatrix);
	pNpcCreatorOb->SetRotation(NpcCreatorMatrix);

	return true;
}
bool CNPCCreatorManager::DelNPCCreator(DWORD dwMapNo, NiEntityInterface* pkEntityInterface)
{
	 int dwNPCCreatorNO;
	pkEntityInterface->GetPropertyData(NPCCREATORNO, dwNPCCreatorNO);
	
	this->DelNPCCreator(dwMapNo, dwNPCCreatorNO);
	return true;
}

bool CNPCCreatorManager::UpdateNPCCreator(DWORD dwMapNo, NiEntityInterface* pkEntityInterface, NiFixedString& pkPropertyName)
{
	if (this->IsExited(dwMapNo, pkEntityInterface))
	{
		 int dwNPCCreatorNO;
		int dwNPCNo;
		int iNPCNum;

		pkEntityInterface->GetPropertyData(NPCCREATORNO, dwNPCCreatorNO);
		CNPCCreator* pNpcOjb = this->GetNPCCreator(dwMapNo,dwNPCCreatorNO);

		if (pkPropertyName == CREATORNAME)
		{
			NiFixedString strCreateName;
			pkEntityInterface->GetPropertyData(CREATORNAME, strCreateName);
			pNpcOjb->m_CreatorName = strCreateName;
		}
		if (pkPropertyName == RADIUS)
		{
			pkEntityInterface->GetPropertyData(RADIUS, pNpcOjb->m_Radius);
		}
		//M1新增
		if (pkPropertyName == RADIUSX)
		{
			pkEntityInterface->GetPropertyData(RADIUSX, pNpcOjb->m_RadiusX);
		}
		if (pkPropertyName == RADIUSY)
		{
			pkEntityInterface->GetPropertyData(RADIUSY, pNpcOjb->m_RadiusY);
		}
		if (pkPropertyName == TIMEFORMAT)
		{
			pkEntityInterface->GetPropertyData(TIMEFORMAT, pNpcOjb->m_TimeFormat);
		}
		if (pkPropertyName == STARTTIME)
		{
			NiFixedString strStartTime;
			pkEntityInterface->GetPropertyData(STARTTIME, strStartTime);
			pNpcOjb->m_StartTime = strStartTime;
		}
		if (pkPropertyName == ENDTIME)
		{
			NiFixedString strEndTime;
			pkEntityInterface->GetPropertyData(ENDTIME, strEndTime);
			pNpcOjb->m_EndTime = strEndTime;
		}
		if (pkPropertyName == NPC1 || pkPropertyName == NPCNUM1)
		{
			pkEntityInterface->GetPropertyData(NPC1, dwNPCNo);
			pkEntityInterface->GetPropertyData(NPCNUM1, iNPCNum);
			pNpcOjb->UpdateNPCTarget(0, dwNPCNo, iNPCNum);
		}
		if (pkPropertyName == NPC2 || pkPropertyName == NPCNUM2)
		{
			pkEntityInterface->GetPropertyData(NPC2, dwNPCNo);
			pkEntityInterface->GetPropertyData(NPCNUM2, iNPCNum);
			pNpcOjb->UpdateNPCTarget(1, dwNPCNo, iNPCNum);
		}
		if (pkPropertyName == NPC3 || pkPropertyName == NPCNUM3)
		{
			pkEntityInterface->GetPropertyData(NPC3, dwNPCNo);
			pkEntityInterface->GetPropertyData(NPCNUM3, iNPCNum);
			pNpcOjb->UpdateNPCTarget(2, dwNPCNo, iNPCNum);
		}
		if (pkPropertyName == NPC4 || pkPropertyName == NPCNUM4)
		{
			pkEntityInterface->GetPropertyData(NPC4, dwNPCNo);
			pkEntityInterface->GetPropertyData(NPCNUM4, iNPCNum);
			pNpcOjb->UpdateNPCTarget(3, dwNPCNo, iNPCNum);
		}
		if (pkPropertyName == NPC5 || pkPropertyName == NPCNUM5)
		{
			pkEntityInterface->GetPropertyData(NPC5, dwNPCNo);
			pkEntityInterface->GetPropertyData(NPCNUM5, iNPCNum);
			pNpcOjb->UpdateNPCTarget(4, dwNPCNo, iNPCNum);
		}
		//M1新增
		if (pkPropertyName == REFRESHNUM)
		{
			pkEntityInterface->GetPropertyData(REFRESHNUM, pNpcOjb->m_RefreshNum);
		}
		if (pkPropertyName == TIMEINTERVAL)
		{
			pkEntityInterface->GetPropertyData(TIMEINTERVAL, pNpcOjb->m_TimeInterval);
		}
		if (pkPropertyName == REFRESHTYPE)
		{
			NiFixedString strTemp;
			pkEntityInterface->GetPropertyData(REFRESHTYPE, strTemp);
			string strRefreshType = strTemp;
			pNpcOjb->m_RefreshType = atol((strRefreshType.substr(0,strRefreshType.find(" "))).c_str());
		}
		if (pkPropertyName == CREATETYPE)
		{
			NiFixedString strTemp;
			pkEntityInterface->GetPropertyData(CREATETYPE, strTemp);
			string strCreateType = strTemp;
			pNpcOjb->m_CreateType = atol((strCreateType.substr(0,strCreateType.find(" "))).c_str());
		}

		if (pkPropertyName == TRANSLATION)
		{
			NiPoint3 vPos;
			pkEntityInterface->GetPropertyData(pkPropertyName, vPos);
			int iPosX;
			int iPosY;
			this->m_pTerrain->GetGridPosInServer(vPos, iPosX, iPosY);
			pNpcOjb->SetPosition(iPosX, iPosY);

		}
		else if (pkPropertyName == ROTATION)
		{
			NiMatrix3 vRot;
			pkEntityInterface->GetPropertyData(pkPropertyName, vRot);
			float fRotX = 0.0f;
			float fRotY = 0.0f;
			float fRotZ = 0.0f;
			vRot.ToEulerAnglesXYZ(fRotX,fRotY,fRotZ);		
			pNpcOjb->SetDirection(fRotZ);
		}
		
	}
	else
	{
		this->AddNPCCreator(dwMapNo, pkEntityInterface);
	}
	return true;	
}

bool CNPCCreatorManager::InitNPCCreator(DWORD dwMapNo, NiEntityInterface* pEntityInf)
{
		CNPCCreator* pNpcCreatorOb = NULL;
	
		 int nCreatorNo;
		pEntityInf->GetPropertyData(NPCCREATORNO, nCreatorNo);
		

		//NPC已经存在，只需更新位置，朝向，和缩放
		if (this->IsExited(dwMapNo,nCreatorNo))
		{
			pNpcCreatorOb = this->GetNPCCreator(dwMapNo, nCreatorNo);

			//初始化位置
			NiPoint3 NpcCreatorPos;
			pEntityInf->GetPropertyData(TRANSLATION, NpcCreatorPos);

			int iPosX;
			int iPosY;
			this->m_pTerrain->GetGridPosInServer(NpcCreatorPos, iPosX, iPosY);
			pNpcCreatorOb->SetPosition(iPosX, iPosY);

			//初始化朝向
			NiMatrix3 vNpcCreatorRot;
			pEntityInf->GetPropertyData(ROTATION, vNpcCreatorRot);
			float fRotX = 0.0f;
			float fRotY = 0.0f;
			float fRotZ = 0.0f;
			vNpcCreatorRot.ToEulerAnglesXYZ(fRotX,fRotY,fRotZ);
			pNpcCreatorOb->SetDirection(fRotZ);

			//设置NPC生成器属性
			//int nCreateType;
			NiFixedString strTemp;	
			pEntityInf->GetPropertyData(CREATETYPE, strTemp);
			string strCreateType = strTemp;
			pNpcCreatorOb->m_CreateType = atol((strCreateType.substr(0,strCreateType.find(" "))).c_str());

			//M1新增
			NiBool pIsSingle = pEntityInf->GetPropertyData(NPCNO, pNpcCreatorOb->m_NPCNo);
			if (pIsSingle)
			{
				pNpcCreatorOb->UpdateNPCTarget(0, pNpcCreatorOb->m_NPCNo, 1);
				pNpcCreatorOb->m_AreaMethod = 1;
			}
			else
			{
				int dwNPCNo;
				int iNPCNum;
				pEntityInf->GetPropertyData(NPC1, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM1, iNPCNum);
				pNpcCreatorOb->UpdateNPCTarget(0, dwNPCNo, iNPCNum);
				pEntityInf->GetPropertyData(NPC2, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM2, iNPCNum);
				pNpcCreatorOb->UpdateNPCTarget(1, dwNPCNo, iNPCNum);
				pEntityInf->GetPropertyData(NPC3, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM3, iNPCNum);
				pNpcCreatorOb->UpdateNPCTarget(2, dwNPCNo, iNPCNum);
				pEntityInf->GetPropertyData(NPC4, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM4, iNPCNum);
				pNpcCreatorOb->UpdateNPCTarget(3, dwNPCNo, iNPCNum);
				pEntityInf->GetPropertyData(NPC5, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM5, iNPCNum);
				pNpcCreatorOb->UpdateNPCTarget(4, dwNPCNo, iNPCNum);
				pNpcCreatorOb->m_AreaMethod = 2;

				pEntityInf->GetPropertyData(RADIUSX, pNpcCreatorOb->m_RadiusX);//M1新增
				pEntityInf->GetPropertyData(RADIUSY, pNpcCreatorOb->m_RadiusY);//M1新增
				pEntityInf->GetPropertyData(TIMEFORMAT, pNpcCreatorOb->m_TimeFormat);//M1新增

				NiFixedString strStartTime;
				pEntityInf->GetPropertyData(STARTTIME, strStartTime);//M1新增
				pNpcCreatorOb->m_StartTime = strStartTime;

				NiFixedString strEndTime;
				pEntityInf->GetPropertyData(ENDTIME, strEndTime);//M1新增
				pNpcCreatorOb->m_EndTime = strEndTime;
			}
			//M1新增

			//pNpcCreatorOb->m_Col = nGridIndex / 
			//pNpcCreatorOb->m_Row = 
			NiFixedString strCreatorName;
			pEntityInf->GetPropertyData(CREATORNAME, strCreatorName);
			if (strCreatorName.Exists())
			{
				pNpcCreatorOb->m_CreatorName = strCreatorName;
			}

			//pEntityInf->GetPropertyData(REFRESHNUM, pNpcCreatorOb->m_RefreshNum);//M1废除
			//pEntityInf->GetPropertyData(TIMEINTERVAL, pNpcCreatorOb->m_TimeInterval);//M1废除

			pEntityInf->GetPropertyData(REFRESHTYPE, strTemp);
			string strRefreshType = strTemp;
			pNpcCreatorOb->m_RefreshType = atol((strRefreshType.substr(0,strRefreshType.find(" "))).c_str());
		} 
		else
		{
			pNpcCreatorOb = new CNpcCreatorObj();
			pNpcCreatorOb->m_MapNO = dwMapNo;
			//设置NPC生成器属性
			//int nCreateType;
			NiFixedString strTemp;	
			pEntityInf->GetPropertyData(CREATETYPE, strTemp);
			string strCreateType = strTemp;
			pNpcCreatorOb->m_CreateType = atol((strCreateType.substr(0,strCreateType.find(" "))).c_str());

			CTerrain::m_NpcCreatorManager.AddNPCCreator(pNpcCreatorOb);
			pEntityInf->SetPropertyData(NPCCREATORNO, (int)pNpcCreatorOb->m_NPCCreatorNO);
			//M1新增
			NiBool pIsSingle = pEntityInf->GetPropertyData(NPCNO, pNpcCreatorOb->m_NPCNo);
			if (pIsSingle)
			{
				pNpcCreatorOb->AddNPCTarget(pNpcCreatorOb->m_NPCNo, 1 ,0);
				pNpcCreatorOb->m_AreaMethod = 1;
			}
			else
			{
				int dwNPCNo;
				int iNPCNum;
				pEntityInf->GetPropertyData(NPC1, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM1, iNPCNum);
				pNpcCreatorOb->AddNPCTarget(dwNPCNo, iNPCNum, 0);
				pEntityInf->GetPropertyData(NPC2, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM2, iNPCNum);
				pNpcCreatorOb->AddNPCTarget(dwNPCNo, iNPCNum, 0);
				pEntityInf->GetPropertyData(NPC3, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM3, iNPCNum);
				pNpcCreatorOb->AddNPCTarget(dwNPCNo, iNPCNum, 0);
				pEntityInf->GetPropertyData(NPC4, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM4, iNPCNum);
				pNpcCreatorOb->AddNPCTarget(dwNPCNo, iNPCNum, 0);
				pEntityInf->GetPropertyData(NPC5, dwNPCNo);
				pEntityInf->GetPropertyData(NPCNUM5, iNPCNum);
				pNpcCreatorOb->AddNPCTarget(dwNPCNo, iNPCNum, 0);
				pNpcCreatorOb->m_AreaMethod = 2;

				pEntityInf->GetPropertyData(RADIUSX, pNpcCreatorOb->m_RadiusX);//M1新增
				pEntityInf->GetPropertyData(RADIUSY, pNpcCreatorOb->m_RadiusY);//M1新增
				pEntityInf->GetPropertyData(TIMEFORMAT, pNpcCreatorOb->m_TimeFormat);//M1新增
				
				NiFixedString strStartTime;
				pEntityInf->GetPropertyData(STARTTIME, strStartTime);//M1新增
				pNpcCreatorOb->m_StartTime = strStartTime;

				NiFixedString strEndTime;
				pEntityInf->GetPropertyData(ENDTIME, strEndTime);//M1新增
				pNpcCreatorOb->m_EndTime = strEndTime;
			}
			//M1新增

			//pNpcCreatorOb->m_Col = nGridIndex / 
			//pNpcCreatorOb->m_Row = 
			NiFixedString strCreatorName;
			pEntityInf->GetPropertyData(CREATORNAME, strCreatorName);
			if (strCreatorName.Exists())
			{
				pNpcCreatorOb->m_CreatorName = strCreatorName;
			}

			//pEntityInf->GetPropertyData(REFRESHNUM, pNpcCreatorOb->m_RefreshNum);//M1废除
			//pEntityInf->GetPropertyData(TIMEINTERVAL, pNpcCreatorOb->m_TimeInterval);//M1废除

			pEntityInf->GetPropertyData(REFRESHTYPE, strTemp);
			string strRefreshType = strTemp;
			pNpcCreatorOb->m_RefreshType = atol((strRefreshType.substr(0,strRefreshType.find(" "))).c_str());

			//pEntityInf->GetPropertyData(RADIUS, pNpcCreatorOb->m_Radius);
			
			/*NiFixedString strStartTime;
			if (pEntityInf->GetPropertyData(STARTTIME, strStartTime))
			{
				pNpcCreatorOb->m_StartTime = strStartTime;
			}
			else
			{
				strStartTime = "0";
				int k = pEntityInf->GetComponentCount();
				for (unsigned int i=0; i<k; i++)
				{
					NiEntityComponentInterface* pkComponent = pEntityInf->GetComponentAt(i);

					if (pkComponent->GetName() == "NPCCreator")
					{
						NiTObjectSet<NiFixedString> nameList;
						pkComponent->GetPropertyNames(nameList);

						for (int j=0; j<nameList.GetSize(); j++)
						{
							NiFixedString str = nameList.GetAt(j);
						}
					}


				}

			}*/
				//M1新增
			
			

			//初始化位置
			NiPoint3 NpcCreatorPos;
			pEntityInf->GetPropertyData(TRANSLATION, NpcCreatorPos);
			int iPosX;
			int iPosY;
			this->m_pTerrain->GetGridPosInServer(NpcCreatorPos, iPosX, iPosY);
			pNpcCreatorOb->SetPosition(iPosX, iPosY);

			//初始化朝向
			NiMatrix3 vNpcCreatorRot;
			pEntityInf->GetPropertyData(ROTATION, vNpcCreatorRot);
			float fRotX = 0.0f;
			float fRotY = 0.0f;
			float fRotZ = 0.0f;
			vNpcCreatorRot.ToEulerAnglesXYZ(fRotX,fRotY,fRotZ);
			pNpcCreatorOb->SetDirection(fRotZ);
		}

	return true;
}

void CNPCCreatorManager::SetTerrain(CTerrain *pTerrain)
{
	this->m_pTerrain = pTerrain;
}
#endif