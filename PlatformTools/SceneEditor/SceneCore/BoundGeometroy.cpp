﻿#include "StdAfx.h"
#include "BoundGeometroy.h"

#ifndef CODE_INGAME

CBoundGeometroy::CBoundGeometroy(void)
{
	m_pkBoxLine = _CreateLine();

	if (NULL == m_pkBoxLine)
	{
		return;
	}
	m_bChanged = false;
	m_kBoundCenter = NiPoint3::ZERO;
	m_fBoundRaidus = 10.0f;

	m_bInit = true;
}

CBoundGeometroy::~CBoundGeometroy(void)
{
	m_bInit = false;
	m_pkBoxLine = NULL;

	m_mapAABBScale.clear();
}

CBoundGeometroy* CBoundGeometroy::GetInstance()
{
	static CBoundGeometroy kThis;
	return &kThis;
}

NiLinesPtr CBoundGeometroy::_CreateLine()
{
	NiPoint3 * pLineVertexData = NiNew NiPoint3[MAX_LINE_COUNT * 2];
	NiColorA * pLineVertexColor = NiNew NiColorA[MAX_LINE_COUNT * 2];

	NiColorA kColor = NiColorA( 1.0f, 1.0f, 0.0f, 1.0f );
	
	for ( UINT nIndex = 0; nIndex < MAX_LINE_COUNT * 2; nIndex ++ )
	{
		pLineVertexData[nIndex] = INVALID_POINT;
		pLineVertexColor[nIndex] = kColor;
	}

	NiBool * pConnectFlag = NiAlloc( NiBool, MAX_LINE_COUNT * 2 );

	for ( UINT nIndex = 0; nIndex < MAX_LINE_COUNT * 2; nIndex ++ )
	{
		pConnectFlag[nIndex] = nIndex % 2 == 0;		
	}

	NiLinesPtr spLine = NiNew NiLines( MAX_LINE_COUNT * 2, pLineVertexData,
		pLineVertexColor, NULL, 0, NiGeometryData::NBT_METHOD_NDL, pConnectFlag );

	NiVertexColorPropertyPtr spVertexColorProp = NiNew NiVertexColorProperty;
	spVertexColorProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
	spVertexColorProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
	spLine->AttachProperty( spVertexColorProp );

	spLine->UpdateProperties();
	spLine->Update( 0.0f );
	return spLine;
}

void	CBoundGeometroy::_SetLinePoint(int nLineIndex, const NiPoint3& vStartPos, const NiPoint3& vEndPos )
{
	if ( !m_bInit)
		return;
	NiPoint3 * pVertices = m_pkBoxLine->GetVertices();
	pVertices[nLineIndex * 2 + 0] = vStartPos;
	pVertices[nLineIndex * 2 + 1] = vEndPos;
	m_bChanged = true;
}

void CBoundGeometroy::_Update()
{
	if ( !m_bInit)
		return;

	for (unsigned int uiIndex = 0; uiIndex < MAX_LINE_COUNT; uiIndex++)
	{
		NiPoint3 * pVertices = m_pkBoxLine->GetVertices();

		pVertices[uiIndex * 2 + 0] += m_kBoundCenter;
		pVertices[uiIndex * 2 + 1] += m_kBoundCenter;
		
	}	

	m_pkBoxLine->GetModelData()->MarkAsChanged( NiGeometryData::VERTEX_MASK );
	m_pkBoxLine->Update( 0.0f );

	
	m_bChanged = false;
}

void CBoundGeometroy::SetBoundPosCenter(const NiPoint3& vBoundCenter )
{
	m_kBoundCenter = vBoundCenter;
}

void CBoundGeometroy::SetBoundRadius(float fBoundRaidus )
{
	m_fBoundRaidus = fBoundRaidus;
}

void CBoundGeometroy::SetBox(NiPoint3 kCenter, NiPoint3 kExtents, NiMatrix3 matRotate,const char* szEntityName)
{
	if ( !m_bInit )
		return;
	NiPoint3 vLines[12][2];
	NiPoint3 vUpPlanePoints[4];
	NiPoint3 vDownPlanePoints[4];

	map<string, float>::iterator it = m_mapAABBScale.find(string(string(szEntityName)));
	if (it != m_mapAABBScale.end())
	{
		float fScale = it->second;
		kExtents = fScale * kExtents;
	}

	// 0  2
	// 1  3
	vUpPlanePoints[0] = kCenter + matRotate * NiPoint3( kExtents.x, kExtents.y, kExtents.z );
	vUpPlanePoints[1] = kCenter + matRotate * NiPoint3( kExtents.x, -kExtents.y, kExtents.z );
	vUpPlanePoints[2] = kCenter + matRotate * NiPoint3( -kExtents.x, kExtents.y, kExtents.z );
	vUpPlanePoints[3] = kCenter + matRotate * NiPoint3( -kExtents.x, -kExtents.y, kExtents.z );

	vDownPlanePoints[0] = kCenter + matRotate * NiPoint3( kExtents.x, kExtents.y, -kExtents.z );
	vDownPlanePoints[1] = kCenter + matRotate * NiPoint3( kExtents.x, -kExtents.y, -kExtents.z );
	vDownPlanePoints[2] = kCenter + matRotate * NiPoint3( -kExtents.x, kExtents.y, -kExtents.z );
	vDownPlanePoints[3] = kCenter + matRotate * NiPoint3( -kExtents.x, -kExtents.y, -kExtents.z );

	vLines[0][0] = vUpPlanePoints[0];
	vLines[0][1] = vUpPlanePoints[1];

	vLines[1][0] = vUpPlanePoints[0];
	vLines[1][1] = vUpPlanePoints[2];

	vLines[2][0] = vUpPlanePoints[3];
	vLines[2][1] = vUpPlanePoints[1];

	vLines[3][0] = vUpPlanePoints[3];
	vLines[3][1] = vUpPlanePoints[2];

	vLines[4][0] = vDownPlanePoints[0];
	vLines[4][1] = vDownPlanePoints[1];

	vLines[5][0] = vDownPlanePoints[0];
	vLines[5][1] = vDownPlanePoints[2];

	vLines[6][0] = vDownPlanePoints[3];
	vLines[6][1] = vDownPlanePoints[1];

	vLines[7][0] = vDownPlanePoints[3];
	vLines[7][1] = vDownPlanePoints[2];

	vLines[8][0] = vDownPlanePoints[0];
	vLines[8][1] = vUpPlanePoints[0];

	vLines[9][0] = vDownPlanePoints[1];
	vLines[9][1] = vUpPlanePoints[1];

	vLines[10][0] = vDownPlanePoints[2];
	vLines[10][1] = vUpPlanePoints[2];

	vLines[11][0] = vDownPlanePoints[3];
	vLines[11][1] = vUpPlanePoints[3];

	for ( UINT nIndex = 0; nIndex < 12; nIndex ++ )
	{
		_SetLinePoint(nIndex, vLines[nIndex][0], vLines[nIndex][1] );
	}
}

NiLinesPtr	CBoundGeometroy::GetBox()
{
	if (!m_bInit)
	{
		return NULL;
	}

	if (!m_bChanged)
	{
		return m_pkBoxLine;
	}

	_Update();

	return m_pkBoxLine;
}

void CBoundGeometroy::SetBoxScale(const char* strEntityName, float fScale)
{
	map<string,float>::iterator it = m_mapAABBScale.find(string(strEntityName));

	if (it == m_mapAABBScale.end())
	{
		m_mapAABBScale.insert(pair<string,float>(string(strEntityName),fScale));
	}
	else
	{
		it->second = fScale;
	}

	m_bChanged = true;
}

float CBoundGeometroy::GetAABBScale(const char* strEntityName)
{
	map<string,float>::iterator it = m_mapAABBScale.find(string(strEntityName));

	if (it == m_mapAABBScale.end())
	{
		return -1.0f;
	}
	else
	{
		return it->second;
	}
}

#endif // end code_ingame define [9/10/2009 hemeng]