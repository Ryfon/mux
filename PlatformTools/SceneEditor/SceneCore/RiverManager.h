﻿/** @file RiverManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2008-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME

#include "Terrain.h"

#ifndef _RIVERMANAGER_H
#define _RIVERMANAGER_H

#define			GRIDINCHUNK			64

struct MAIN_ENTRY  stTriList 
{
	unsigned int	uiP0,uiP1,uiP2;
};

struct MAIN_ENTRY stRiverPoint
{
	NiPoint3 kVertex;				//河流顶点坐标
	NiColorA kVertexClr;			//河流的顶点颜色

};

struct MAIN_ENTRY stRiverRegion		
{
	unsigned int			uiVertexCount;				//河流顶点数目
	unsigned int			uiTriCount;					//多边形个数
	float					fRiverHeight;				//河流的高度
	NiColorA				kColor;						//河流的颜色
	vector<DWORD>			dwTerrainIndex;				//河流中存在点的TerrainID
	map<DWORD,stRiverPoint> mVertex;					//河流顶点数据
	vector<stTriList>		stTri;						//顶点索引	存储的是terrain的顶点索引
	NiTriShapePtr			pkRiverGeom;				//河流几何，主要用于在编辑器中绘制河流形状
};


class MAIN_ENTRY CRiverManager
{
	//////////////////////////////////////////////////////////////////////////
public:	
	//判断当前terrain点是否属于水域内的点 
	bool					IsExistPos(DWORD terrainIndex);
	//返回水域顶点数
	unsigned int			GetRiverVerticCount();
	//销毁函数
	static	void			Do_Dispos();
	//静态函数，获得水域管理静态指针	
	static	CRiverManager*	GetInstance();			
	//添加水域
	void					AddRiverRegion( float fZPos,NiColorA pColor,float fAlphaDet);		
	//添加或删除水域点
	void					EditRiverPoint(CTerrain *pTerrain,const NiPoint3& vPos, float fRadius, bool bAddTo);		
	//由河流ID获得河流中三角形的个数
	unsigned int			GetTrianglesCount();	
	//获得水域数据
	stRiverRegion*			GetRiverData();																					
	//获得水域几何
	NiTriShapePtr			GetRiverGeom();
	//设定指定位置alpha值
	void					AdjustAlphaByHeight(CTerrain *pTerrain);
	//创建几何型
	bool					BuildRiverRegionGem(CTerrain *pTerrain );
	//前优化几何
	void					OptimizerRiverGeom(CTerrain *pTerrain);			

	//////////////////////////////////////////////////////////////////////////
private:
	CRiverManager();
	~CRiverManager(void);
	CRiverManager(CRiverManager&);

	//创建半径为iRadiu的多边形
	unsigned int	_BuildTriangles(CTerrain *pTerrain,int	iRadiu);
	//删除alpha为0的多余顶点
	//fAdjust为Alpha值可接受范围
	void			_OptimizeByAlpha(CTerrain *pTerrain,float fAdjust);
	//根据给定terrainIndex获得在dwTerrainIndex的下标
	unsigned int	_GetIdFromTerrainIndex( DWORD terrainIndex );				
	//插入水域点
	bool			_InsertRiverPoint(DWORD terrainIndex,stRiverPoint stVertex);
	//删除水域点
	bool			_DeleteRiverPoint(DWORD terrainIndex);

	//////////////////////////////////////////////////////////////////////////
private:
	stRiverRegion*			m_RiverRegion;				//水域数据
	static CRiverManager*	ms_pRiverMgr;				//水域管理指针
	float					m_fAlphaDet;				//Alpha增量
};
//end of Class RiverManager.h

#pragma  make_public(CRiverManager)



#endif
#endif