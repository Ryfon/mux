﻿#include "StdAfx.h"
#include "TerrainTextureMgr.h"

CTerrainTextureMgr::CTerrainTextureMgr(void)
{
}

CTerrainTextureMgr::~CTerrainTextureMgr(void)
{}

bool	CTerrainTextureMgr::Initialize( const string& strPath )
{
	m_strPath = strPath;
	return true;
}

void	CTerrainTextureMgr::UnInitialize()
{
	ClearAll();
}

NiSourceTexturePtr	CTerrainTextureMgr::GetTexture( const char *pszFile )
{
	if ( !pszFile )	return 0;

	TextureMap::iterator it = m_TexMap.find( string(pszFile) );
	if ( it != m_TexMap.end() )
	{
		return it->second;
	}
	else
	{
		NiTexture::FormatPrefs kPrefs;
		kPrefs.m_ePixelLayout	= NiTexture::FormatPrefs::TRUE_COLOR_32;
		kPrefs.m_eMipMapped		= NiTexture::FormatPrefs::NO;

		NiSourceTexture::SetDestroyAppDataFlag( false );
		NiSourceTexture* pSrcTex = NiSourceTexture::Create( pszFile, kPrefs);

		pSrcTex->SetStatic(false);
		NiSourceTexture::SetDestroyAppDataFlag( true );

		if ( pSrcTex)
		{
			LOG1( "加入纹理 %s", pszFile );
			m_TexMap.insert( TextureMap::value_type(string(pszFile), pSrcTex ) );

			// 测试载入的纹理
			////pSrcTex->SetStatic(false);
			//pSrcTex->LoadPixelDataFromFile();	// 从文件载入的纹理，必须执行这个才能取得 pixel data
			//NiPixelData* pSourcePixelData = pSrcTex->GetSourcePixelData();
			//pSourcePixelData->MarkAsChanged();

			////pSourcePixelData->set
			//for ( int y = 0; y < 255; ++y )
			//{
			//	for ( int x = 0; x < 255; ++x )
			//	{
			//		unsigned char *pPtrColor = ( *pSourcePixelData )(x,y);	// 目标纹理象素
			//		NiColor color(pPtrColor[2]/255.0f, pPtrColor[1]/255.0f, pPtrColor[0]/255.0f);
 			//		int i = 0;
			//	}
			//}
			return pSrcTex;
		}
		return 0;
	}	
}

bool		CTerrainTextureMgr::RemoveTexture( const char *pszFile )
{
	TextureMap::iterator it = m_TexMap.find( string(pszFile) );
	if ( it != m_TexMap.end() )
	{
		it->second = 0;	m_TexMap.erase( it ); 
		return true;
	}
	
	return false;
}

bool		CTerrainTextureMgr::RemoveTexture( NiSourceTexture *pTexture )
{
	TextureMap::iterator it = m_TexMap.begin();
	while ( it != m_TexMap.end() )
	{
		if ( it->second == pTexture )
		{
			it->second = 0; m_TexMap.erase( it );
			return true;
		}
		++it;
	}

	return false;
}

/// 清空纹理容器
void		CTerrainTextureMgr::ClearAll()
{
	TextureMap::iterator it = m_TexMap.begin();
	while ( it != m_TexMap.end() )
	{
		it->second = 0;
		it++;
	}

	m_TexMap.clear();	
}