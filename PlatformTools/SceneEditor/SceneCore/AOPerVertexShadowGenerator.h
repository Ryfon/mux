﻿/** @file AOPerVertexShadowGenerator.h 
@brief 逐顶点地形 AO 阴影产生.利用场景物件和地形产生 AODisk, 逐地形顶点计算阴影,将结果插值写入 blend texture 的 alpha 通道.
<pre>
*	Copyright (c) 2008，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2008-04-23
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef CODE_INGAME

#pragma once

#include "AODiskGenerator.h"
#include <NiEntityInterface.h>
namespace SceneCore
{
class MAIN_ENTRY CAOPerVertexShadowGenerator
{
public:
	CAOPerVertexShadowGenerator(CTerrain* pkTerrain);
	~CAOPerVertexShadowGenerator(void);

	/// 添加投射阴影的物件,利用该物件产生 AODisk
	//void AddShadowEmitter(NiAVObject* pkAVObj);

	void AddShadowEmitter(NiEntityInterface* pkEntity);
	/// 产生地形阴影
	void GenerateTerrainShadow(int iChunkID, float fTerrainAffectFactor=20.0f,
							float fSceneAffectFactor=12.0f, float fMaxShadowValue = 0.5f);

	/// 产生假的物件环境阴影
	// fMaxShadowValue - 最大阴影深度
	// fBlur - 模糊程度
	// fBlurTexSize -  scale down 到原纹理的多少尺寸 
	void GenerateSceneObjectShadow(float fMaxShadowValue, float fBlur, float fBlurTexSize);


	//	1_2
	//  | |
	//	0-3
	/**
	*	<BR>功能说明：	为一个4个顶点构成的正方形插值计算任一 uv 点的值
	*	<BR>可访问性：	public
	*	<BR>注    释：	用来插值阴影,所以只是一个通道.
	*	@param	fColor[0,3] 四个顶点的颜色
	*	@param	uv		目标点 uv
	*	@return			插值结果
	*/ 
	float QuadColorInterpolation(float fColor0, float fColor1, float fColor2, float fColor3, NiPoint2 uv);

	float ElementShadow(NiPoint3 v, float rSquared, NiPoint3 kReceiveNromal,
								NiPoint3 vEmitterNormal, float fEmitterArea);

	// 获取 disk 数量
	void GetNumDisks(int& iNumTerrainDisks, int &iNumSceneDisks);

	// 保存场景物件 disk info
	bool SaveSceneDiskInfo(const char* pszFileName);

	// 保存地形 disk info
	bool LoadSceneDiskInfo(const char* pszFileName);

	CTerrain* GetTargetTerrain() {return m_pkTerrain;}
protected:
	// 对纹理进行模糊
	void _BlurTexture(NiTexture* pkTexture, float fBlur, float fBlurTexSize);

	CTerrain*			m_pkTerrain;		// 产生阴影的目标地形
	CAODiskGenerator	m_kDiskGenerator;	// AODisk 产生器
	list<NiEntityInterface*>	*m_aEntityList;		// 场景所有物件列表,按照 chunk 分别存放
};

#include "AOPerVertexShadowGenerator.inl"

};

#endif
