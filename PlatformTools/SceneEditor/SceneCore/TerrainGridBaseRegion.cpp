﻿#include "StdAfx.h"
#include "TerrainGridBaseRegion.h"

#ifndef CODE_INGAME

using namespace SceneCore;
//---------------------------------------------------------------------------------------
CTerrainGridBaseRegion::CTerrainGridBaseRegion(const char* pszName)
:	m_bChanged(false)
{
	if (pszName != NULL)
	{
		SetName(pszName);
	}
}
//---------------------------------------------------------------------------------------
CTerrainGridBaseRegion::~CTerrainGridBaseRegion(void)
{
}
//---------------------------------------------------------------------------------------
// 清除所有 Grid
void CTerrainGridBaseRegion::Clear()
{
	m_GridSet.clear();
}
//---------------------------------------------------------------------------------------
void CTerrainGridBaseRegion::SetName(const char* pszName)
{
	if (pszName == NULL)
	{
		return;
	}

	strcpy_s(m_szName, pszName);
}
//---------------------------------------------------------------------------------------
const char* CTerrainGridBaseRegion::GetName()
{
	return m_szName;
}
//---------------------------------------------------------------------------------------
std::set<int>& CTerrainGridBaseRegion::GetGridSet()
{
	return m_GridSet;
}
//---------------------------------------------------------------------------------------
#endif
