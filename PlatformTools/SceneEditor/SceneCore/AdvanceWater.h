﻿/** @file AdvanceWater.h 
@brief 高级水体 现在使用 max 给面片覆 AdvanceWater Shader, 本类作废，只作参考
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-09-22
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef ADVANCE_WATER_H 
#define ADVANCE_WATER_H

#pragma once

#include <NiMemObject.h>
#include <NiEntity.h>

#ifndef CODE_INGAME

namespace SceneCore
{

/// 用来初始化水体的参数
struct stAdvWaterInitParam
{
	// 4 个用做叠加的波的参数
	float				afWaveLength[4];	// 波长
	float				afAmplitude[4];		// 振幅
	float				afSpeed[4];			// 波峰移动速度
	NiPoint2			avDirection[4];		// 方向

	// 水面的参数
	//float				fFresnel;			// Fresnel
	float				fAlpha;				// 透明度,用于修正 fresnel, 调整水体透明度
	NiColor				kReflectColor;		// 反射色修正
	NiColor				kSpecularColor;		// 高光色

	// 环境参数
	NiPoint3			kLightDir;			// 光线方向
	//NiTexturePtr		spRefractMap;		// 折射纹理
	NiTexturePtr		spEnvMap;			// 环境纹理, cube map
};

NiSmartPointer(CAdvanceWater);

class MAIN_ENTRY CAdvanceWater : public NiNode
{
	NiDeclareRTTI;

public:
	CAdvanceWater(void);
	~CAdvanceWater(void);

	// 初始化
	void Init(NiAVObjectPtr spSceneRoot, stAdvWaterInitParam& initParam);

	// 设置/获取波长
	void SetWaveLength(UINT uiIndex, float fData);
	const float* GetWaveLength();

	// 设置/获取振幅
	void SetAmplitude(UINT uiIndex, float fData);
	const float* GetAmplitude();

	// 设置/获取波传播速度
	void SetSpeed(UINT uiIndex, float fData);
	const float* GetSpeed();

	// 设置/获取方向
	void SetWaveDirection(UINT uiIndex, NiPoint2 vWaveDirection);
	NiPoint2* GetWaveDirection();

	// 设置/获取 Fresnel
	void SetFresnel(float fFresnel);
	const float GetFresnel();

	// 设置/获取 Alpha
	void SetAlpha(float fAlpha);
	const float GetAlpha();

	// 设置/获取反射色
	void SetReflectColor(NiColor kColor);
	const NiColor GetReflectColor();

	// 设置/获取折射色
	void SetSpecularColor(NiColor kColor);
	const NiColor GetSpecularColor();

	// 设置/获取光方向
	void SetLightDir(NiPoint3 kDir);
	const NiPoint3 GetLightDir();

	// 设置/获取环境纹理
	void SetEnvMap(NiTexturePtr spEnvMap);
	const NiTexturePtr GetEnvMap();

	// 设置/获取折射纹理
	void SetRefractMap(NiTexturePtr spRefractMap);
	const NiTexturePtr GetRefractMap();

	// 设置世界变换
	void SetTranslate(const NiPoint3& kTranslation);
	const NiPoint3& GetTranslate() const;

	void SetRotate(const NiMatrix3& kRotation);
	const NiMatrix3& GetRotate() const;

	void SetScale(const float fScale);
	float GetScale() const;

	// 设置水表面几何体
	void SetSceneRoot(NiAVObjectPtr pkSceneRoot);
	NiAVObjectPtr GetSceneRoot();

	// 添加 Geometry 到 多边形列表
	void AddGeometry(NiGeometry* pkGeom);

	// 更新
	void Update(float fTime, bool bUpdateControllers = true);

	// 创建可视集
	void BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext);

	void RenderImmediate(NiRenderer* pkRenderer);

private:
	// 刷新水波参数
	void _RefreshWaveParam();
	void _RecursiveFindAndSetAdvanceWaterMaterial();

	float			m_afWaveLength[4];		// 波长
	float			m_afAmplitude[4];		// 振幅
	float			m_afSpeed[4];			// 波峰移动速度
	NiPoint2		m_avDirection[4];		// 方向
	float			m_afW[4];				// 频率, 每个 2*Pai 距离波的重复次数. 2*Pai/waveLength
	float			m_afPhi[4];				// phase-constant 相位常数 speed*W

	float			m_fFresnel;				// Fresnel
	float			m_fAlpha;				// Alpha
	NiColor			m_kReflectColor;		// 反射色
	NiColor			m_kSpecularColor;		// 折射色

	NiPoint3		m_kLightDir;			// 光方向
	NiTexturePtr	m_spEnvMap;				// 环境纹理, cube map
	NiTexturePtr	m_spRefractMap;			// 用做折射采样的纹理

	bool			m_bEnableRefract;		// 是否开启折射
	bool			m_bMoveVertex;			// 是否改变顶点高度

	NiAVObjectPtr	m_spSceneRoot;			// 水表面几何体
	NiMaterialPtr	m_spAdvWaterMtl;		// 材质
	NiPoint3		m_kTranslation;			// 位置
	NiMatrix3		m_kRotation;			// 旋转
	float			m_fScale;				// 缩放
	vector<NiGeometry*> m_GeometryList;		// SceneRoot 中所有 NiGeometry 列表
};
};

#endif

#endif