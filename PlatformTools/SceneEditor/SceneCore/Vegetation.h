﻿/** @file Vegetation.h 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhaixufeng
 *	完成日期：2007-09-28
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 *
 *  备    注：暂未使用
 </pre>*/

#ifndef VEGETATION_H 
#define VEGETATION_H

#include <NiStringExtraData.h>

/**
 @brief 植被类
 *	
 */
class MAIN_ENTRY CVegetation
{
public:

	/// 构造
	CVegetation(void);

	/// 析构
	virtual ~CVegetation(void);

	/// 从文件中创建
	bool				Create( const char *pszFile );

	// 销毁
	void				Destroy();

	/// 获得名字
	const string		GetExtraData() const
	{	return static_cast< string >( m_pExtraData->GetValue() );	}

	/// 获得对象
	NiNode							*GetNode() { return m_spNode; }

private:
	
	/// Extra Data, 用来标识同材质的物件
	NiStringExtraData		*m_pExtraData;

	/// 节点
	NiNodePtr				m_spNode;
};

/**
 @brief 植被类比较符,这里以ExtraData作比较
 * 
 */
class	MAIN_ENTRY CVegetationNameComp
{
public:

	/// 重载()
	bool operator()( const CVegetation	*pVeg1, const CVegetation *pVeg2 )
	{
		return 	pVeg1->GetExtraData() < pVeg2->GetExtraData();
	}
};

/// 植被set定义
typedef set< CVegetation*, CVegetationNameComp >	VegetationSet;

#endif //VEGETATION_H