﻿/** @file RiverExportor.cpp
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2008-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#include "StdAfx.h"
#include "RiverExportor.h"
#ifndef CODE_INGAME

#include <lib3ds/io.h>
#include <lib3ds/node.h>
#include <lib3ds/file.h>
#include <lib3ds/mesh.h>
#include <lib3ds/vector.h>

using namespace SceneCore;
//-----------------------------------------------------------------------------------------------
CRiverExportor::CRiverExportor(void)
{
	m_p3dsFile = lib3ds_file_new();
}

CRiverExportor::~CRiverExportor(void)
{
	lib3ds_file_free(m_p3dsFile);
}

bool	CRiverExportor::SaveToFile(const char* pszFilePath)
{
	return (lib3ds_file_save(m_p3dsFile, pszFilePath) == 1);
}
void	CRiverExportor::Clear()
{
	lib3ds_file_free(m_p3dsFile);
	m_p3dsFile = lib3ds_file_new();
}

void	CRiverExportor::AddRiverNode(NiTriShapePtr pTriGeom)
{
	static int iNodeID = 0;
	if ( !pTriGeom )
	{
		return;
	}
	// 获取原 NiTriBasedGeom 信息
	int iNumVert = pTriGeom->GetVertexCount();
	int iNumTri = pTriGeom->GetTriangleCount();
	const NiTransform& kTransform = pTriGeom->GetWorldTransform();

	// 为本结点创建 3dsNode
	Lib3dsNode* p3dsNode = lib3ds_node_new_object();
	p3dsNode->node_id = ++iNodeID;
	sprintf_s(p3dsNode->name, "node_%d", iNodeID); 
	p3dsNode->type = LIB3DS_OBJECT_NODE;
	p3dsNode->parent_id = LIB3DS_NO_PARENT;


	lib3ds_file_insert_node(m_p3dsFile, p3dsNode);	// 将 node 插入 file 

	// 创建 mesh
	Lib3dsMesh* p3dsMesh = lib3ds_mesh_new(p3dsNode->name);

	// 创建顶点列表
	//顶点列表
	lib3ds_mesh_new_point_list(p3dsMesh, iNumVert);
	NiPoint3* pkVertices = pTriGeom->GetVertices();
	for (int i=0; i<iNumVert; i++)
	{
		NiPoint3 kPos = pkVertices[i];
		kPos = kTransform * kPos;
		p3dsMesh->pointL[i].pos[0] = kPos.x;
		p3dsMesh->pointL[i].pos[1] = kPos.y;
		p3dsMesh->pointL[i].pos[2] = kPos.z;
	}

	// 创建三角形列表
	lib3ds_mesh_new_face_list(p3dsMesh, iNumTri);
	for (int i=0; i<iNumTri; i++)
	{
		unsigned short i0, i1, i2;
		pTriGeom->GetTriangleIndices(i, i0, i1, i2);
		p3dsMesh->faceL[i].points[0] = i0;
		p3dsMesh->faceL[i].points[1] = i1;
		p3dsMesh->faceL[i].points[2] = i2;
	}
	p3dsNode->type = LIB3DS_OBJECT_NODE;
	lib3ds_file_insert_mesh(m_p3dsFile, p3dsMesh);	// 将 mesh 插入 file 
}

//end of class CRiverExportor
#endif
