﻿#include "StdAfx.h"
#include "C3DSExportor.h"

#ifndef CODE_INGAME

#include <lib3ds/io.h>
#include <lib3ds/node.h>
#include <lib3ds/file.h>
#include <lib3ds/mesh.h>
#include <lib3ds/vector.h>

using namespace SceneCore;
//--------------------------------------------------------------------------------
C3DSExportor::C3DSExportor(void)
{
	m_p3dsFile = lib3ds_file_new();
}
//--------------------------------------------------------------------------------
C3DSExportor::~C3DSExportor(void)
{
	lib3ds_file_free(m_p3dsFile);
}
//--------------------------------------------------------------------------------
/// 添加一个结点到导出列表 
void C3DSExportor::AddSceneNode(NiAVObject* pkAVObj, int& iBeginNodeID, int& iEndNodeID)
{
	static int iNodeID = 0;
	// 起始 node id
	iBeginNodeID = iNodeID;	

	if( NULL == pkAVObj )
	{
		return;
	}

	if (pkAVObj->GetAppCulled() || (!NiIsKindOf(NiNode, pkAVObj) && !NiIsKindOf(NiTriBasedGeom, pkAVObj)))
	{
		// 终止 node id
		iEndNodeID = iNodeID;
		return;
	}

	if (NiIsKindOf(NiNode, pkAVObj))
	{
		NiNode* pkNode = (NiNode*)pkAVObj;
		// 遍历 NiNode 下所有子结点.递归调用
		unsigned int uiNumChildren = pkNode->GetChildCount();
		for (unsigned int i=0; i<uiNumChildren; i++)
		{
			int iA, iB;	// 无意义的返回值
			AddSceneNode(pkNode->GetAt(i), iA, iB);
		}
	}
	else if (NiIsKindOf(NiTriBasedGeom, pkAVObj))
	{
		// 获取原 NiTriBasedGeom 信息
		NiTriBasedGeom* pkTriGeom = (NiTriBasedGeom*)pkAVObj;
		int iNumVert = pkTriGeom->GetVertexCount();
		int iNumTri = pkTriGeom->GetTriangleCount();
		const NiTransform& kTransform = pkTriGeom->GetWorldTransform();

		// 为本结点创建 3dsNode
		Lib3dsNode* p3dsNode = lib3ds_node_new_object();
		p3dsNode->node_id = ++iNodeID;
		sprintf(p3dsNode->name, "node_%d", iNodeID); 
		p3dsNode->type = LIB3DS_OBJECT_NODE;
		p3dsNode->parent_id = LIB3DS_NO_PARENT;
		

		lib3ds_file_insert_node(m_p3dsFile, p3dsNode);	// 将 node 插入 file 

		// 创建 mesh
		Lib3dsMesh* p3dsMesh = lib3ds_mesh_new(p3dsNode->name);

		// 创建顶点列表
		lib3ds_mesh_new_point_list(p3dsMesh, iNumVert);
		NiPoint3* pkVertices = pkTriGeom->GetVertices();
		for (int i=0; i<iNumVert; i++)
		{
			NiPoint3 kPos = pkVertices[i];
			kPos = kTransform * kPos;
			p3dsMesh->pointL[i].pos[0] = kPos.x;
			p3dsMesh->pointL[i].pos[1] = kPos.y;
			p3dsMesh->pointL[i].pos[2] = kPos.z;
		}

		// 创建三角形列表
		lib3ds_mesh_new_face_list(p3dsMesh, iNumTri);
		for (int i=0; i<iNumTri; i++)
		{
			unsigned short i0, i1, i2;
			pkTriGeom->GetTriangleIndices(i, i0, i1, i2);
			p3dsMesh->faceL[i].points[0] = i0;
			p3dsMesh->faceL[i].points[1] = i1;
			p3dsMesh->faceL[i].points[2] = i2;
		}
		p3dsNode->type = LIB3DS_OBJECT_NODE;
		lib3ds_file_insert_mesh(m_p3dsFile, p3dsMesh);	// 将 mesh 插入 file 
	}

	// 终止 node id
	iEndNodeID = iNodeID;

	return;
}
//--------------------------------------------------------------------------------
/// 将场景保存到 3ds 文件
bool C3DSExportor::SaveToFile(const char* pszFilePath)
{
	return lib3ds_file_save(m_p3dsFile, pszFilePath);
}
//--------------------------------------------------------------------------------
/// 清除 Lib3dsFile 里所有结点
void C3DSExportor::Clear()
{
	lib3ds_file_free(m_p3dsFile);
	m_p3dsFile = lib3ds_file_new();
}
//--------------------------------------------------------------------------------
void C3DSExportor::Debug_Export3DSNode()
{
	// 创建 node
	Lib3dsNode* p3dsNode = lib3ds_node_new_object();
	p3dsNode->node_id = 1;
	p3dsNode->parent_id = LIB3DS_NO_PARENT;

	// 创建 file
	Lib3dsFile* p3dsFile = lib3ds_file_new();

	// 创建 mesh
	Lib3dsMesh* p3dsMesh = lib3ds_mesh_new("mesh 01");
	strcpy(p3dsNode->name, p3dsMesh->name);

	// 写顶点数据
	lib3ds_mesh_new_point_list(p3dsMesh, 3);
	p3dsMesh->pointL[0].pos[0] = -10.0f;
	p3dsMesh->pointL[0].pos[1] = 0.0f;
	p3dsMesh->pointL[0].pos[2] = 0.0f;
	p3dsMesh->pointL[1].pos[0] = 0.0f;
	p3dsMesh->pointL[1].pos[1] = 10.0f;
	p3dsMesh->pointL[1].pos[2] = 0.0f;
	p3dsMesh->pointL[2].pos[0] = 10.0f;
	p3dsMesh->pointL[2].pos[1] = 0.0f;
	p3dsMesh->pointL[2].pos[2] = 0.0f;

	// 写 face 数据
	lib3ds_mesh_new_face_list(p3dsMesh, 1);

	//LIB3DS_FACE_FLAG_VIS_AC = 0x1,       /*!< Bit 0: Edge visibility AC */
	//LIB3DS_FACE_FLAG_VIS_BC = 0x2,       /*!< Bit 1: Edge visibility BC */
	//LIB3DS_FACE_FLAG_VIS_AB = 0x4,       /*!< Bit 2: Edge visibility AB */
	//p3dsMesh->faceL[0].flags = 0x1|0x2;
	//p3dsMesh->faceL[0].material
	//p3dsMesh->faceL[0].normal[0] = 0.0f;
	//p3dsMesh->faceL[0].normal[1] = 0.0f;
	//p3dsMesh->faceL[0].normal[2] = 1.0f;
	p3dsMesh->faceL[0].points[2] = 0;
	p3dsMesh->faceL[0].points[1] = 1;
	p3dsMesh->faceL[0].points[0] = 2;
	//p3dsMesh->faceL[0].smoothing 

	//p3DSNode->user.mesh = p3dsMesh;
	lib3ds_file_insert_node(p3dsFile, p3dsNode);	// 将 node 插入 file 
	lib3ds_file_insert_mesh(p3dsFile, p3dsMesh);

	lib3ds_file_save(p3dsFile, "e:\\test.3ds");

	// 释放资源
	//lib3ds_mesh_free_face_list(p3dsMesh);
	//lib3ds_mesh_free_point_list(p3dsMesh);
	//lib3ds_mesh_free(p3dsMesh);
	//lib3ds_node_free(p3dsNode);
	lib3ds_file_free(p3dsFile);
}
#endif
