﻿/** @file RiverOptimizer.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2008-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CODE_INGAME

#include "RiverManager.h"

//定义水域CHUNK为4*4，相邻CHUNK共用8顶点
#define VERTEXINCHUNK 3
#define VERTEICEINCHUNK (VERTEXINCHUNK+1)*(VERTEXINCHUNK+1)

//8的倍数
#define  WIDTHEBITY(bit)	( ( (bit<<3) + 31) / 32 * 4 )

#pragma once

class MAIN_ENTRY CRiverOptimizer
{
public:
	CRiverOptimizer(CRiverManager* pRiverManager, CTerrain *pTerrain);
	~CRiverOptimizer(void);
	
	//初始化优化器，创建chunk，生成LOD标志数组
	bool			Initial();
	//获得Chunk数 
	unsigned int	GetRiverChunkNumX();
	unsigned int	GetRiverChunkNumY();
	bool			Optimize();

	//////////////////////////////////////////////////////////////////////////
	//测试用临时变量
	NiPoint3	m_kMaxPoint,m_kMinPoint;

private:
	CRiverOptimizer();
	CRiverOptimizer(CRiverOptimizer&);
	//创建水域外包矩形
	bool			_CreateRiverChunks();
	//获得当前点在terrain上的索引
	DWORD			_GetTerrainIndex(float fX,float fY);
	//获得扫描线（地形）长
	DWORD			_GetScanline();
	//判断当前Chunk是否包含水域信息
	bool			_IsUsedInRiver(unsigned int uiChunkX, unsigned int uiChunkY);
//////////////////////////////////////////////////////////////////////////
private:
	CRiverManager	*m_pRiverManager;
	CTerrain		*m_pTerrain;
	//标志每一个顶点的LOD
	unsigned char	*m_ucpQuadMatrix;
	//RiverChunk的长宽
	unsigned int	m_iRiverChunkX;
	unsigned int	m_iRiverChunkY;
	//标志RiverChunk属性：该RiverChunk是否包含水域信息
	bool			*m_bIsRiverChunk;
};

#endif