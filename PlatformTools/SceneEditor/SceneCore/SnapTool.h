﻿/** @file SnapTool.h 
@brief 吸点工具
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-02-04
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#include <NiTNodeTraversal.h>

#ifndef	_SNAPTOOL_H
#define _SNAPTOOL_H

#ifndef CODE_INGAME

//源物件与目标物件之间的最大距离，只有小于最大距离时才进行吸附
#define	MAX_DISTANCE	100.0f

struct RecursiveFindGeometries
{
	bool operator() (NiAVObject* pkAVObject)
	{
		if (NiIsKindOf(NiNode, pkAVObject))
		{
			NiNode* pkNode = NiDynamicCast(NiNode, pkAVObject);
			if ( pkNode->GetName().ContainsNoCase("Snap") )
			{
				NiPoint3* pkPos = NiNew NiPoint3;
				pkPos->x = pkNode->GetWorldTranslate().x;
				pkPos->y = pkNode->GetWorldTranslate().y;
				pkPos->z = pkNode->GetWorldTranslate().z;
				m_pkPos.push_back(pkPos);
			}
		}
		return true;
	}
	vector<NiPoint3*> m_pkPos;
};

//----------------------------------------------------------------------------------------------------
class MAIN_ENTRY CSnapTool
{
public:
	CSnapTool(void);
	~CSnapTool(void);

	static CSnapTool* Instance();

	bool	SetSrcEntity(NiAVObject* pkEntity);
	bool	SetDesEntity(NiAVObject* pkEntity);

	// 判断是否是可吸附物件
	// 返回 0 否
	//		1 源物件
	//		2 目标物件
	//		-1 错误
	int 	IsSnapEntity(NiAVObject* pkObject);
	NiAVObject* GetSrcEntity()	{return m_pkSrcEntity;}
	NiAVObject* GetDesEntity()	{return m_pkDesEntity;}

	/*NiAVObject* GetSanpSign()	
	{
		return m_pkSnapSign;
	}*/

	bool GetDesPos(NiPoint3 &kCurrPos);
	bool ClearSnapEntity();
	/*bool RenderSnapEntityVertice(NiRenderer* pkRenderer);*/

	void	Do_Dispos();
	enum	EEntityType
	{
		EP_NONE,
		EP_SRC,
		EP_DES
	};

private:
	

	CSnapTool(CSnapTool&);
	float	_ComputeDistance(NiPoint3 kSrcPos, NiPoint3 kDesPos);
	bool	_IsValidaEntity(NiAVObject* pkEntity);
	/*bool	_CreateGeom(EEntityType epType);*/

	// 源物件，即需要移动的物件
	NiAVObject* m_pkSrcEntity;
	// 目标物件，即吸附的物件
	NiAVObject* m_pkDesEntity;

	NiAVObject* m_pkSnapSign;

	// 源物件snap点标志
	NiLinesPtr m_pkSrcGeom;

	// 目标物件snap点标志
	NiLinesPtr m_pkDesGeom;

	static CSnapTool*	ms_pkThis;

};
#endif
#endif
