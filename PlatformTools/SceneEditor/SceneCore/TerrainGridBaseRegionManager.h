﻿/** @file TerrainGridBaseRegionManager.h 
@brief 基于Grid的区域管理器
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-07-28
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#ifndef CODE_INGAME

#include <map>
#include <set>
#include "TerrainGridBaseRegion.h"
#include <NiTriShape.h>

namespace SceneCore
{
	class MAIN_ENTRY CTerrainGridBaseRegionManager
	{
	public:
		~CTerrainGridBaseRegionManager(void);

		// 添加一个区域
		bool AddGridBaseRegion(CTerrainGridBaseRegion*);
		bool AddGridBaseRegion(const char*);

		// 根据区域名称获取
		CTerrainGridBaseRegion* GetGridBaseRegion(const char* pszName);

		// 获取/设置 当前正被编辑的区域
		CTerrainGridBaseRegion* GetOnEditRegion();

		// 设置当前正编辑的区域
		void SetOnEditRegion(const char* pszName);
		void SetOnEditRegion(CTerrainGridBaseRegion* pRegion);

		// 删除一个区域
		void RemoveRegion(const char* pszName);

		// 获取唯一实例
		static CTerrainGridBaseRegionManager* GetInstance();

		// 销毁
		void Dispose();

		// 获取所有区域名称列表
		const std::vector<string>* GetRegionNameList();

		/**
		*	<BR>功能说明：为当前被编辑 Grid 区域绘制 Grid
		*	<BR>可访问性：static public
		*	<BR>注    释：
		*	@param	pTerrain[in]	目标地形
		*	@param	kCenter[in]		笔刷中心点
		*	@param	fRadius[in]		笔刷外半径
		*	@param	bAdd[in]		true-增加Grid false-删除Grid
		*	@return 无
		*/
		static void PaintGridBaseRegion(CTerrain* pTerrain, const NiPoint3& kCenter, const float fRadius, bool bAdd);

		// 根据区域创建用于标识该区域的粒子
		static NiGeometry* GenerateParticlesFromGridBaseRegion(CTerrain* pTerrain, CTerrainGridBaseRegion* pRegion);

		// 根据选择的区域从 terrain 中挖出对应的 geometry. 如果地形是优化过的，那么生成的 geom 也是优化过的
		static NiGeometry* GenerateTriGeomFromGridBaseRegion(CTerrain* pTerrain, CTerrainGridBaseRegion* pRegion);

		// 获取正被编辑的区域的对应几何体。如果被编辑区域发生改变，则重新计算几何体。
		NiGeometry* GetOnEditRegionGeometry(CTerrain* pTerrain);

	private:
		CTerrainGridBaseRegionManager(void);

		std::map<CTerrainGridBaseRegion*, NiGeometry*>	m_RegionList;		// 区域 - 对应几何体 列表
		CTerrainGridBaseRegion*							m_pOnEditRegion;	// 当前正被编辑的 区域
		static CTerrainGridBaseRegionManager*			ms_pInstance;		// 唯一实例
	}; // end of class

#include "TerrainGridBaseRegionManager.inl"

};	// end of namespace


#endif
