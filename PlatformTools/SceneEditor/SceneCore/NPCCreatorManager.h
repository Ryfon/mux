﻿/** @file NPCCreatorManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangshaodong
*	完成日期：2007-12-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef CODE_INGAME

#ifndef _NPCCREATORMANAGER_H
#define _NPCCREATORMANAGER_H

#include <NiTSet.h>
#include <NiFixedString.h>
#include <NiEntityInterface.h>

class CNPCCreator;
class CTerrain;

typedef map<DWORD, CNPCCreator*> PERMAP_NPCCREATOR; 
typedef map<DWORD, PERMAP_NPCCREATOR*> MAP_NPCCREATOR;
class MAIN_ENTRY CNPCCreatorManager
{
	typedef pair<DWORD, int> NO_PAIR;
public:
	CNPCCreatorManager(void);
	CNPCCreatorManager(CTerrain *pTerrain);
public:
	virtual ~CNPCCreatorManager(void);
public:
	int LoadNPCCreator(const string& strXmlFileName);
	int LoadNPCCreatorNo(const string strXmlFileName);

	int CleanUp();

	int SaveNPCCreator(const string& strXmlFileName);
	int SaveNPCCreatorNo(const string strXmlFileName);

	size_t GetNPCCreatorCount() const {return m_NPCCreatorList.size();};
	CNPCCreator* GetNPCCreator(DWORD dwMapNo, DWORD dwNPCCreatorNO);

	CNPCCreator* AddNPCCreator(DWORD dwMapNo);
	int AddNPCCreator(CNPCCreator* pNpcCreator);

	int DelNPCCreator(DWORD dwMapNo, DWORD dwNPCCreatorNO);

	static bool EntityIsNPC(NiEntityInterface* pkEntityInterface);
	bool AddNPCCreator(DWORD dwMapNo, NiEntityInterface* pkEntityInterface);
	bool DelNPCCreator(DWORD dwMapNo, NiEntityInterface* pkEntityInterface);
	bool UpdateNPCCreator(DWORD dwMapNo, NiEntityInterface* pkEntityInterface, NiFixedString& pkPropertyName);
	bool IsExited(DWORD dwMapNo, NiEntityInterface* pkEntityInterface);
	bool IsExited(DWORD dwMapNo, DWORD dwNPCCreatorNO);
	bool InitNPCCreator(DWORD dwMapNo, NiEntityInterface* pEntityInf);
	void SetTerrain(CTerrain *pTerrain);
public:
	MAP_NPCCREATOR m_NPCCreatorList;
protected:
	/// 地形
	CTerrain	*m_pTerrain;

private:
	string m_FileVersion;
	vector<NO_PAIR> m_NOList;

	DWORD GetNPCCreatorNO();
	int SetNPCCreatorNO(DWORD dwNO);	 
};
#endif
#endif