﻿#pragma once

/** @file DepthAlphaEffectManager.h 
@brief 管理和渲染 所有 根据场景深度变化 alpha 混合因子的特效 管理器，比如层雾，softparticles.
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：0.1
*	作    者：Shi Yazheng
*	完成日期：2008-02-02
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#include "MRT_ColorDepthMaterial.h"

#include "IDepthAlphaEffect.h"

#define MAIN_RENDER_STEP_NAME "NiApplication Main Scene Render Step"

namespace SceneCore
{

NiSmartPointer(CDepthAlphaEffectManager);

/// DepthAlpha Effect 指 alpha混合因子 与 深度值 有关的特效
class  MAIN_ENTRY CDepthAlphaEffectManager : public NiViewRenderClick
{
public:
	/// 获取唯一实例
	static CDepthAlphaEffectManager* GetInstance();

	/// 初始化 DepthAlpha effect,主要工作是在 stand material 中增加一个 render target 保存 depth buffer
	bool Initialize(NiRendererPtr spRenderer);

	bool CreateFrame(NiRenderFramePtr spFrame);
	
	/// 获取/设置深度纹理
	NiTexturePtr GetDepthTexture();
	void SetDepthTexture(NiRenderedTexturePtr pkDepthTexture);

	// 客户端使用 CreateFrame 的方法。场景编辑器中没有 Frame, 使用 
	// 1. 通过RenderTargetGroup 增加 depth buffer 的 RenderTarget.只在初始化时调用一次
	NiRenderTargetGroup* CreateRenderTargetGroup(NiRenderedTexture* pkTexture);

#ifndef CODE_INGAME
	// 2. 场景其他物件渲染完后再渲染 体雾本身, 
	void PerformRendering(NiEntityRenderingContextPtr	pkRenderingCondtex);
#endif

	// 设置当前使用的摄像机
	void SetCamera(NiCameraPtr pkCamera);
	// 客户端使用的渲染，老黄可以自由修改
    virtual void PerformRendering(unsigned int uiFrameID);

	/// 销毁
	static void Destory();

	/// 添加新的 depth alpha effect
	void RegisteEffect(IDepthAlphaEffect* pEffect);

	/// 删除 depth alpha effect
	void UnregisteEffect(IDepthAlphaEffect* pEffect);

	/// 注销所有 depth alpha effect
	void UnregisteAllEffect();

	/// 遍历一个 NiAVObject 所有子节点，搜索 DepthAlphaEffect 添加到 manager 中
	void RecursiveFindAndAddDepthAlphaEffect(NiAVObjectPtr	pkSceneRoot);

	/// 创建可视集合
	virtual void BuildVisibleSet(NiEntityRenderingContextPtr	pkRenderingCondtex);

	/// 更新
	void Update(float fElapsedTime, float fTotalTime );

protected:
	CDepthAlphaEffectManager(void);
	~CDepthAlphaEffectManager(void);

	/// 从 DAEffect 列表中 搜索
	list<IDepthAlphaEffect*>::iterator _Find(IDepthAlphaEffect* pkDAEffect);

	static CDepthAlphaEffectManagerPtr		ms_pkInstance;		// 唯一实例
	list<IDepthAlphaEffect*>		m_EffectList;	// 场景中所有 Depth alpha effect
	NiRenderedTexturePtr			m_spDepthMap;	// depth buffer
	NiRendererPtr					m_spRenderer;	// 
	NiRenderTargetGroupPtr			m_spSceneRTG;	// 渲染目标。
	NiCameraPtr						m_spCamera;		// 当前摄像机
};


};