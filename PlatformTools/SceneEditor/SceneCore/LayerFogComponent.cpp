﻿#include "StdAfx.h"
#include "LayerFogComponent.h"
#include <NiParamsNIF.h>

using namespace SceneCore;

NiFixedString CLayerFogComponent::ERR_TRANSLATION_NOT_FOUND;
NiFixedString CLayerFogComponent::ERR_ROTATION_NOT_FOUND;
NiFixedString CLayerFogComponent::ERR_SCALE_NOT_FOUND;
NiFixedString CLayerFogComponent::ERR_FILE_LOAD_FAILED;

NiFixedString CLayerFogComponent::ms_kClassName;

// Component name.
NiFixedString CLayerFogComponent::ms_kComponentName;

// Property names.
NiFixedString CLayerFogComponent::ms_kColorName;
NiFixedString CLayerFogComponent::ms_kAlphaName;
NiFixedString CLayerFogComponent::ms_kNifFilePathName;
NiFixedString CLayerFogComponent::ms_kSceneRootPointerName;

// Property descriptions.
NiFixedString CLayerFogComponent::ms_kColorDescription;
NiFixedString CLayerFogComponent::ms_kAlphaDescription;
NiFixedString CLayerFogComponent::ms_kNifFilePathDescription;
NiFixedString CLayerFogComponent::ms_kSceneRootPointerDescription;

// Dependent property names.
NiFixedString CLayerFogComponent::ms_kTranslationName;
NiFixedString CLayerFogComponent::ms_kRotationName;
NiFixedString CLayerFogComponent::ms_kScaleName;


// Declare classes
NiFactoryDeclarePropIntf(CLayerFogComponent);

void CLayerFogComponent::_SDMInit()
{
	ERR_TRANSLATION_NOT_FOUND = "Translation property not found.";
	ERR_ROTATION_NOT_FOUND = "Rotation property not found.";
	ERR_SCALE_NOT_FOUND = "Scale property not found.";
	ERR_FILE_LOAD_FAILED = "SPT file load failed.";

	CLayerFogComponent::ms_kClassName = "CLayerFogComponent";

	// Component name.
	CLayerFogComponent::ms_kComponentName = "Layer Fog Component";

	// Property names.
	CLayerFogComponent::ms_kColorName = "Color";
	CLayerFogComponent::ms_kAlphaName = "Alpha";
	CLayerFogComponent::ms_kNifFilePathName = "NIF File Path";
	CLayerFogComponent::ms_kSceneRootPointerName = "Scene Root Pointer";

	// Property descriptions.
	CLayerFogComponent::ms_kColorDescription = "雾的颜色。";
	CLayerFogComponent::ms_kAlphaDescription = "雾的透明度。每米的浓度增加值。";
	CLayerFogComponent::ms_kNifFilePathDescription = "nif 文件路径。";
	CLayerFogComponent::ms_kSceneRootPointerDescription = "场景根节点。";

	// Dependent property names.
	CLayerFogComponent::ms_kTranslationName = "Translation";
	CLayerFogComponent::ms_kRotationName = "Rotation";
	CLayerFogComponent::ms_kScaleName = "Scale";

	NiFactoryRegisterPropIntf(CLayerFogComponent);	// registe creation function
}
//-------------------------------------------------------------------------------------------------
void CLayerFogComponent::_SDMShutdown()
{
	ERR_TRANSLATION_NOT_FOUND = NULL;
	ERR_ROTATION_NOT_FOUND = NULL;
	ERR_SCALE_NOT_FOUND = NULL;
	ERR_FILE_LOAD_FAILED = NULL; 

	CLayerFogComponent::ms_kClassName = NULL;

	// Component name.
	CLayerFogComponent::ms_kComponentName = NULL;

	// Property names.
	CLayerFogComponent::ms_kColorName = NULL;
	CLayerFogComponent::ms_kAlphaName = NULL;
	CLayerFogComponent::ms_kNifFilePathName = NULL;
	CLayerFogComponent::ms_kSceneRootPointerName = NULL;

	// Property descriptions.
	CLayerFogComponent::ms_kColorDescription = NULL;
	CLayerFogComponent::ms_kAlphaDescription = NULL;
	CLayerFogComponent::ms_kNifFilePathDescription = NULL;
	CLayerFogComponent::ms_kSceneRootPointerDescription = NULL;

	// Dependent property names.
	CLayerFogComponent::ms_kTranslationName = NULL;
	CLayerFogComponent::ms_kRotationName = NULL;
	CLayerFogComponent::ms_kScaleName = NULL;

}
//-------------------------------------------------------------------------------------------------
CLayerFogComponent::CLayerFogComponent(NiAVObjectPtr pkSceneRoot)
: m_spNifFilePath("")
{
	m_spFogInstance = NiNew CLayerFog();
	NiPoint3 kPosition(0,0,0);
	NiMatrix3 kRotation;
	kRotation.MakeIdentity();
	m_spFogInstance->Init(pkSceneRoot, kPosition, kRotation, 1.0f);
}
//-------------------------------------------------------------------------------------------------
CLayerFogComponent::~CLayerFogComponent(void)
{
	m_spFogInstance = NULL;
	m_spMasterComponent = NULL;
}

//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* CLayerFogComponent::Clone(bool bInheritProperties)
{
	CLayerFogComponent* pkLayerFogComp = NiNew CLayerFogComponent;
	pkLayerFogComp->GetLayerFogInstance()->SetAlpha(m_spFogInstance->GetAlpha());
	pkLayerFogComp->GetLayerFogInstance()->SetColor(m_spFogInstance->GetColor());
	pkLayerFogComp->SetPropertyData(ms_kNifFilePathName, m_spNifFilePath);

	return pkLayerFogComp;
}
//-------------------------------------------------------------------------------------------------

NiEntityComponentInterface* CLayerFogComponent::GetMasterComponent()
{
	return m_spMasterComponent;
}
//-------------------------------------------------------------------------------------------------

void CLayerFogComponent::SetMasterComponent(NiEntityComponentInterface* pkMasterComponent)
{
	m_spMasterComponent = (CLayerFogComponent*)pkMasterComponent;
}
//-------------------------------------------------------------------------------------------------

void CLayerFogComponent::GetDependentPropertyNames(NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
	kDependentPropertyNames.Add(ms_kTranslationName);
	kDependentPropertyNames.Add(ms_kRotationName);
	kDependentPropertyNames.Add(ms_kScaleName);
	//kDependentPropertyNames.Add(ms_kNifFilePathName);
	//kDependentPropertyNames.Add(ms_kSceneRootPointerName);
}
//-------------------------------------------------------------------------------------------------

// NiEntityPropertyInterface overrides.
NiBool CLayerFogComponent::SetTemplateID(const NiUniqueID& kID)
{
	// 不支持
	return false;
}
//-------------------------------------------------------------------------------------------------

NiUniqueID CLayerFogComponent::GetTemplateID()
{
	static const NiUniqueID kUniqueID = 
		NiUniqueID(0xC2, 0x39, 0x02, 0x43, 0xEE, 0x89, 0x48, 0xab, 0xA8, 0x2E, 0xA3, 0xE9, 0x0E, 0x09, 0xCE, 0xFC);
	return kUniqueID;
}
//-------------------------------------------------------------------------------------------------

void CLayerFogComponent::AddReference()
{
	this->IncRefCount();
}
//-------------------------------------------------------------------------------------------------

void CLayerFogComponent::RemoveReference()
{
	this->DecRefCount();
}
//-------------------------------------------------------------------------------------------------

NiFixedString CLayerFogComponent::GetClassName() const
{
	return ms_kClassName;
}
//-------------------------------------------------------------------------------------------------
NiFixedString CLayerFogComponent::ClassName()
{
	return ms_kClassName;
}
//-------------------------------------------------------------------------------------------------
NiFixedString CLayerFogComponent::GetName() const
{
	return ms_kComponentName;
}
//-------------------------------------------------------------------------------------------------

NiBool CLayerFogComponent::SetName(const NiFixedString& kName)
{
	// 不支持
	return false;
}
//-------------------------------------------------------------------------------------------------

NiBool CLayerFogComponent::IsAnimated() const
{
	return false;
}
//-------------------------------------------------------------------------------------------------
bool CLayerFogComponent::EntityIsLayerFog(NiEntityInterface* pkEntity)
{
	static const NiUniqueID kUniqueID = 
		NiUniqueID(0xC2, 0x39, 0x02, 0x43, 0xEE, 0x89, 0x48, 0xab, 0xA8, 0x2E, 0xA3, 0xE9, 0x0E, 0x09, 0xCE, 0xFC);

	if (NULL != pkEntity->GetComponentByTemplateID(kUniqueID))
	{
		return true;
	}
	else
	{
		return false;
	}
}
//-------------------------------------------------------------------------------------------------
void CLayerFogComponent::Update(NiEntityPropertyInterface* pkParentEntity,
					float fTime, NiEntityErrorInterface* pkErrors,
					NiExternalAssetManager* pkAssetManager)
{
	if (!m_spFogInstance)
	{
		return;
	}

	// 基本上用的 NiSceneGraphComponent 的 update
	if (m_bNifFileChanged)
	{
		m_bNifFileChanged = false;
		// If the scene root does not exist, create it using the property
		// data.
		if (m_spNifFilePath.Exists() && pkAssetManager &&
			pkAssetManager->GetAssetFactory())
		{
			NIASSERT(pkAssetManager);
			NiParamsNIF kNIFParams;
			if (!kNIFParams.SetAssetPath(m_spNifFilePath) ||
				!pkAssetManager->RegisterAndResolve(&kNIFParams))
			{
				// Failing to resolve amounts to failed to load file.
				pkErrors->ReportError(ERR_FILE_LOAD_FAILED,
					m_spNifFilePath, pkParentEntity->GetName(),
					ms_kNifFilePathName);
				return;
			}

			NiBool bSuccess = pkAssetManager->Retrieve(&kNIFParams);
			NIASSERT(bSuccess);

			NiAVObject* pkAVObject; 
			bSuccess = kNIFParams.GetSceneRoot(pkAVObject);

			if (bSuccess && pkAVObject != NULL)
			{
				m_spFogInstance->SetSceneRoot(pkAVObject);

				// Perform initial update.
				pkAVObject->Update(0.0f);
				pkAVObject->UpdateNodeBound();
				pkAVObject->UpdateProperties();
				pkAVObject->UpdateEffects();
			}
			else
			{
				NIASSERT(!"Failed to Retrieve file!");
				// Failing to retrieve amounts to failed to load file.
				pkErrors->ReportError(ERR_FILE_LOAD_FAILED,
					m_spNifFilePath, pkParentEntity->GetName(),
					ms_kNifFilePathName);
				return;
			}
		}
	}

	if (m_spFogInstance)
	{
		NiBool bDependentPropertiesFound = true;

		// Find dependent properties.
		NiPoint3 kTranslation;
		if (!pkParentEntity->GetPropertyData(ms_kTranslationName,
			kTranslation))
		{
			bDependentPropertiesFound = false;
			pkErrors->ReportError(ERR_TRANSLATION_NOT_FOUND, NULL,
				pkParentEntity->GetName(), ms_kTranslationName);
		}
		NiMatrix3 kRotation;
		if (!pkParentEntity->GetPropertyData(ms_kRotationName, kRotation))
		{
			bDependentPropertiesFound = false;
			pkErrors->ReportError(ERR_ROTATION_NOT_FOUND, NULL,
				pkParentEntity->GetName(), ms_kRotationName);
		}
		float fScale;
		if (!pkParentEntity->GetPropertyData(ms_kScaleName, fScale))
		{
			bDependentPropertiesFound = false;
			pkErrors->ReportError(ERR_SCALE_NOT_FOUND, NULL,
				pkParentEntity->GetName(), ms_kScaleName);
		}

		// Use dependent properties to update transform of scene root.
		bool bUpdatedTransforms = false;
		if (bDependentPropertiesFound)
		{
			if (m_spFogInstance->GetTranslate() != kTranslation)
			{
				m_spFogInstance->SetTranslate(kTranslation);
				bUpdatedTransforms = true;
			}
			if (m_spFogInstance->GetRotate() != kRotation)
			{
				m_spFogInstance->SetRotate(kRotation);
				bUpdatedTransforms = true;
			}
			if (m_spFogInstance->GetScale() != fScale)
			{
				m_spFogInstance->SetScale(fScale);
				bUpdatedTransforms = true;
			}
		}

		m_spFogInstance->Update(fTime);
		// Update scene root with the provided time.
		//if (bUpdatedTransforms)
		//{
		//	m_spFogInstance->GetSceneRoot()
		//	//m_spSceneRoot->Update(fTime);
		//}
		//else if (GetShouldUpdateSceneRoot())
		//{
		//	m_spSceneRoot->UpdateSelected(fTime);
		//}
	}

	//if (m_spFogInstance != NULL)
	//{

	//	bool bDependentPropertiesFound = true;
	//	NiAVObjectPtr pkSceneRoot = NULL;
	//	if (!pkParentEntity->GetPropertyData(ms_kSceneRootPointerName, pkSceneRoot))
	//	{
	//		bDependentPropertiesFound = false;
	//		pkErrors->ReportError(ERR_SCALE_NOT_FOUND, NULL,
	//			pkParentEntity->GetName(), ms_kScaleName);
	//	}		

	//	if (m_spFogInstance->GetSceneRoot() != pkSceneRoot)
	//	{
	//		m_spFogInstance->SetSceneRoot(pkSceneRoot);
	//	}
	//}
}
//-------------------------------------------------------------------------------------------------

void CLayerFogComponent::BuildVisibleSet(NiEntityRenderingContext* pkRenderingContext,
										 NiEntityErrorInterface* pkErrors)
{
	// Layerfog 不与其他物件一同在渲染列表中渲染，需要在所有物件渲染完后，最后单独渲染
	//if (m_spFogInstance != NULL)
	//{
	//	m_spFogInstance->BuildVisibleSet(pkRenderingContext);
	//}
}
//-------------------------------------------------------------------------------------------------

void CLayerFogComponent::GetPropertyNames(
	NiTObjectSet<NiFixedString>& kPropertyNames) const
{
	kPropertyNames.Add(ms_kColorName);
	kPropertyNames.Add(ms_kAlphaName);
	kPropertyNames.Add(ms_kNifFilePathName);
	kPropertyNames.Add(ms_kSceneRootPointerName);
}
//-------------------------------------------------------------------------------------------------

NiBool CLayerFogComponent::CanResetProperty(const NiFixedString& kPropertyName,
											bool& bCanReset) const
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName ||
		kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kSceneRootPointerName)		
	{
		bCanReset = false;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::ResetProperty(const NiFixedString& kPropertyName)
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName||
		kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kSceneRootPointerName)
	{
	}
	else
	{
		return false;
	}
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::MakePropertyUnique(const NiFixedString& kPropertyName,
											  bool& bMadeUnique)
{
	bool bCanReset;
	if (!CanResetProperty(kPropertyName, bCanReset))
	{
		return false;
	}
	bMadeUnique = true;
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetDisplayName(const NiFixedString& kPropertyName,
										  NiFixedString& kDisplayName) const
{
	if (kPropertyName == ms_kColorName)
	{
		kDisplayName = ms_kColorName;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kDisplayName = ms_kAlphaName;
	}
	else if( kPropertyName == ms_kNifFilePathName)
	{
		kDisplayName = ms_kNifFilePathName;
	}
	else if( kPropertyName == ms_kSceneRootPointerName)
	{
		kDisplayName = NULL;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::SetDisplayName(const NiFixedString& kPropertyName,
										  const NiFixedString& kDisplayName)
{
	// This component does not allow the display name to be set.
	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetPrimitiveType(const NiFixedString& kPropertyName,
											NiFixedString& kPrimitiveType) const// property 是什么类型的
{
	if (kPropertyName == ms_kColorName)
	{
		kPrimitiveType = PT_COLOR;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kPrimitiveType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kNifFilePathName)
	{
		// Scene Root Pointer property should not be displayed.
		kPrimitiveType = PT_STRING;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kPrimitiveType = PT_NIOBJECTPOINTER;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::SetPrimitiveType(const NiFixedString& kPropertyName,
											const NiFixedString& kPrimitiveType)
{
	// This component does not allow the primitive type to be set.
	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetSemanticType(const NiFixedString& kPropertyName,
										   NiFixedString& kSemanticType) const
{
	if (kPropertyName == ms_kColorName)
	{
		kSemanticType = PT_COLOR;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kSemanticType = PT_FLOAT;
	}
	else if (kPropertyName == ms_kNifFilePathName)
	{
		// Scene Root Pointer property should not be displayed.
		kSemanticType = "Filename";
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kSemanticType = PT_NIOBJECTPOINTER;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::SetSemanticType(const NiFixedString& kPropertyName,

										   const NiFixedString& kSemanticType)
{
	// This component does not allow the semantic type to be set.
	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetDescription(const NiFixedString& kPropertyName,
										  NiFixedString& kDescription) const
{
	if (kPropertyName == ms_kColorName)
	{
		kDescription = ms_kColorDescription;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		kDescription = ms_kAlphaDescription;
	}
	else if (kPropertyName == ms_kNifFilePathName)
	{
		// Scene Root Pointer property should not be displayed.
		kDescription = ms_kNifFilePathDescription;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		// Scene Root Pointer property should not be displayed.
		kDescription = ms_kSceneRootPointerDescription;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::SetDescription(const NiFixedString& kPropertyName,
										  const NiFixedString& kDescription)
{
	// This component does not allow the semantic type to be set.
	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetCategory(const NiFixedString& kPropertyName,
									   NiFixedString& kCategory) const
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName
		|| kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kSceneRootPointerName)
	{
		kCategory = ms_kComponentName;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName,
											  bool& bIsReadOnly)
{
	if (kPropertyName == ms_kColorName ||	
		kPropertyName == ms_kAlphaName ||	
		kPropertyName == ms_kNifFilePathName)
	{
		bIsReadOnly = false;
		return true;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		bIsReadOnly = true;
		return true;
	}
	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::IsPropertyUnique(const NiFixedString& kPropertyName,
											bool& bIsUnique)
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName|| 
		kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kSceneRootPointerName)
	{
		bIsUnique = true;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::IsPropertySerializable(const NiFixedString& kPropertyName,
												  bool& bIsSerializable)
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName||	
										kPropertyName == ms_kNifFilePathName)
	{
		bIsSerializable = true;
	}
	else if (kPropertyName == ms_kSceneRootPointerName)
	{
		bIsSerializable = false;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::IsPropertyInheritable(const NiFixedString& kPropertyName,
												 bool& bIsInheritable)
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName|| 
		kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kSceneRootPointerName)
	{
		bIsInheritable = false;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::IsExternalAssetPath(const NiFixedString& kPropertyName,
											   unsigned int uiIndex, bool& bIsExternalAssetPath) const
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName|| 
		kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kSceneRootPointerName)
	{
		bIsExternalAssetPath = true;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetElementCount(const NiFixedString& kPropertyName,
										   unsigned int& uiCount) const
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName|| 
		kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kSceneRootPointerName)
	{
		uiCount = 1;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::SetElementCount(const NiFixedString& kPropertyName,
										   unsigned int uiCount, bool& bCountSet)
{
	return false;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::IsCollection(const NiFixedString& kPropertyName,
										bool& bIsCollection) const
{
	if (kPropertyName == ms_kColorName || kPropertyName == ms_kAlphaName|| 
		kPropertyName == ms_kNifFilePathName || kPropertyName == ms_kSceneRootPointerName)
	{
		bIsCollection = false;
		return true;
	}

	return false;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取颜色
NiBool CLayerFogComponent::SetPropertyData(const NiFixedString& kPropertyName, const NiColor& kData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kColorName && m_spFogInstance!=NULL)
	{
		m_spFogInstance->SetColor(kData);
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetPropertyData(const NiFixedString& kPropertyName, NiColor& kData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kColorName)
	{
		if (m_spFogInstance!=NULL)
		{
			kData = m_spFogInstance->GetColor();
		}
		else
		{
			kData = NiColor(0,0,0);
		}
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取 alpha 
NiBool CLayerFogComponent::SetPropertyData(const NiFixedString& kPropertyName, float fData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kAlphaName && m_spFogInstance!=NULL)
	{
		m_spFogInstance->SetAlpha(fData);
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetPropertyData(const NiFixedString& kPropertyName, float& fData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName == ms_kAlphaName)
	{
		if (m_spFogInstance!=NULL)
		{
			fData = m_spFogInstance->GetAlpha();
		}
		else
		{
			fData = 0.0f;
		}
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取 nif 文件路径
NiBool CLayerFogComponent::GetPropertyData(const NiFixedString& kPropertyName,
										   NiFixedString& kData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kNifFilePathName)
	{
		kData = m_spNifFilePath;
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::SetPropertyData(const NiFixedString& kPropertyName,
										   const NiFixedString& kData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kNifFilePathName)
	{
		if (kData != m_spNifFilePath)
		{

			m_spNifFilePath = kData;
			m_bNifFileChanged = true;
		}
	}
	else
	{
		return false;
	}

	return true;
}
//-------------------------------------------------------------------------------------------------
// 设置/获取 场景根
NiBool CLayerFogComponent::SetPropertyData(const NiFixedString& kPropertyName,
										   NiObject*& pkData, unsigned int uiIndex)
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kSceneRootPointerName)
	{
		// 不支持
	}
	else
	{
		return false;
	}
	return true;
}
//-------------------------------------------------------------------------------------------------
NiBool CLayerFogComponent::GetPropertyData(const NiFixedString& kPropertyName,
										   NiObject*& pkData, unsigned int uiIndex) const
{
	if (uiIndex != 0)
	{
		return false;
	}
	else if (kPropertyName==ms_kSceneRootPointerName)
	{
		if (m_spFogInstance!=NULL)
		{
			pkData = m_spFogInstance->GetSceneRoot();
		}
		else
		{
			pkData = NULL;
		}
	}
	else
	{
		return false;
	}
	return true;
}
//-------------------------------------------------------------------------------------------------