﻿#include "StdAfx.h"
#include "TerrainVertexColorBaker.h"
#include "TerrainModifier.h"
#include "TerrainChangeVertexColorCommand.h"
#ifndef CODE_INGAME

CTerrainVertexColorBaker::CTerrainVertexColorBaker(CTerrain* pTerrain)
:	m_pTerrain(pTerrain)
{
}

CTerrainVertexColorBaker::~CTerrainVertexColorBaker(void)
{
}

void CTerrainVertexColorBaker::FloodTerrainVertexColor(NiColor kColor)
{
	// 遍历所有顶点
	int iNumVertX = m_pTerrain->GetChunkNumX() * GRIDINCHUNK + 1;
	int iNumVertY = m_pTerrain->GetChunkNumY() * GRIDINCHUNK + 1;
	int iNumVertTotal = iNumVertX * iNumVertY;	// 地形顶点总数

	NiColorA* pkColors = m_pTerrain->GetVertexColor();

	for (int iVertIdx=0; iVertIdx<iNumVertTotal; iVertIdx++)
	{
		pkColors[iVertIdx].r = kColor.r;
		pkColors[iVertIdx].g = kColor.g;
		pkColors[iVertIdx].b = kColor.b;
	}

	for (int i=0; i<m_pTerrain->GetChunkNumX()*m_pTerrain->GetChunkNumY(); i++)
	{
		CTerrainModifier::NotifyChunkVertexChanged(m_pTerrain, i);
	}
}

// 烘焙灯光到定点色
void CTerrainVertexColorBaker::BakeLightsToTerrainVertex(
	std::vector<NiLight*>* lightList, bool bMultiply, float fTerrainAmbient, CTerrainChangeVertexColorCommand& command)
{
	// 遍历所有顶点
	int iNumVertX = m_pTerrain->GetChunkNumX() * GRIDINCHUNK + 1;
	int iNumVertY = m_pTerrain->GetChunkNumY() * GRIDINCHUNK + 1;
	int iNumVertTotal = iNumVertX * iNumVertY;	// 地形顶点总数

	NiColorA* pkColors = m_pTerrain->GetVertexColor();
	const NiPoint3* pkPositions = m_pTerrain->GetVertices();
	NiPoint3* pkNormals = m_pTerrain->GetNormals();

	NiColor* diffuseAccumArray = NiNew NiColor[iNumVertTotal];	//　漫反射光积累
	NiColor* ambientAccumArray = NiNew NiColor[iNumVertTotal];	//	环境光累积

	for (int iVertIdx=0; iVertIdx<iNumVertTotal; iVertIdx++)
	{
		// 遍历所有灯光
		for (unsigned int iLightIdx=0; iLightIdx<lightList->size(); iLightIdx++)
		{
			Light(pkPositions[iVertIdx], pkNormals[iVertIdx], (*lightList)[iLightIdx], diffuseAccumArray[iVertIdx], ambientAccumArray[iVertIdx]);
		}

		NiColor kTotalColor = diffuseAccumArray[iVertIdx] + (ambientAccumArray[iVertIdx] * fTerrainAmbient);

		NiColorA kNewColor;
		if (bMultiply)
		{
			kNewColor = pkColors[iVertIdx] * NiColorA(kTotalColor.r, kTotalColor.g, kTotalColor.b, 1.0);
		}
		else
		{
			kNewColor = pkColors[iVertIdx] + NiColorA(kTotalColor.r, kTotalColor.g, kTotalColor.b, 0.0);
		}

		tVertexColorChange change;
		change.iIndex = iVertIdx;
		change.kNewColor = kNewColor;
		change.kOldColor = pkColors[iVertIdx];
		command.AddVertexChange(change);
	}
}

#endif