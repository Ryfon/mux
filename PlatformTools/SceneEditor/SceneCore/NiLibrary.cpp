﻿#include "stdafx.h"
#include "NiLightMapMaterial.h"
#ifdef _USRDLL
BOOL WINAPI DllMain(HINSTANCE /*hInstance*/, DWORD reason, LPVOID)
{
    switch (reason)
    {
    case DLL_PROCESS_ATTACH:
        OutputDebugStringA("NiLightMapMaterialLib> DLLMain CALL - PROCESS ATTACH\n");
        //NiStaticDataManager::ProcessAccumulatedLibraries();
        break;

    case DLL_PROCESS_DETACH:
        OutputDebugStringA("NiLightMapMaterialLib> DLLMain CALL - PROCESS DETACH\n");
        //NiStaticDataManager::RemoveLibrary("NiLightMapMaterial");
        NiLightMapMaterialLibrary::Shutdown();
        break;
    }
    return TRUE;
}

//-----------------------------------------------------------------------------------------------
NILIGHTMAPMATERIAL_ENTRY unsigned int GetCompilerVersion()
{
    return _MSC_VER;
}
//-----------------------------------------------------------------------------------------------
NILIGHTMAPMATERIAL_ENTRY unsigned int GetMaterialLibraryCount()
{
    return 1;
}
//-----------------------------------------------------------------------------------------------
NILIGHTMAPMATERIAL_ENTRY bool LoadMaterialLibrary(
    NiRenderer* /*pRenderer*/,
    NiMaterialLibrary*& pLibrary,
    unsigned int whichLibrary)
{
    if (whichLibrary == 0)
    {
        pLibrary = NiLightMapMaterialLibrary::Create();
        return (pLibrary != 0);
    }
    else
    {
        pLibrary = 0;
        return false;
    }
}
#endif