﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;

namespace TextureCombinationTool
{
    public partial class TextureCombinationPanel : Form
    {
        public TextureCombinationPanel()
        {
            InitializeComponent();
            m_comboBoxSizeFilter.Items.Add("全部");
            m_comboBoxSizeFilter.Items.Add("32");
            m_comboBoxSizeFilter.Items.Add("64");
            m_comboBoxSizeFilter.Items.Add("128");
            m_comboBoxSizeFilter.Items.Add("256");
            m_comboBoxSizeFilter.Items.Add("512");
            m_comboBoxSizeFilter.Items.Add("1024");
            m_comboBoxSizeFilter.Items.Add("2048");
            m_comboBoxSizeFilter.SelectedIndex = 0;

            m_comboBoxCompressMode.Items.Add("DXT1");
            m_comboBoxCompressMode.Items.Add("DXT3");
            m_comboBoxCompressMode.Items.Add("DXT5");
            m_comboBoxCompressMode.SelectedIndex = 0;

            m_comboBoxTargetTexSize.Items.Add("64");
            m_comboBoxTargetTexSize.Items.Add("128");
            m_comboBoxTargetTexSize.Items.Add("256");
            m_comboBoxTargetTexSize.Items.Add("512");
            m_comboBoxTargetTexSize.Items.Add("1024");
            m_comboBoxTargetTexSize.Items.Add("2048");
            m_comboBoxTargetTexSize.SelectedIndex = 0;

            MFramework.Init();
            MTextureCombination.Init(this , m_pictureBoxRenderBox);
        }

        private int GetTextureSizeFilter()
        {
            if ((m_comboBoxSizeFilter.SelectedItem as String) == "全部")
            {
                return 0;
            }
            else 
            {
                try
                {
                    int iSize = Int32.Parse(m_comboBoxSizeFilter.SelectedItem as String);
                    return iSize;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return 0;
                }
            }
        }

        // 刷新当前目录下所有纹理列表
        private void RefershAllTextureList()
        {
            try
            {
                m_listBoxAllTexture.Items.Clear();
                int iFilterSize = GetTextureSizeFilter();
                String[] fileList = new String[4096];
                if (MTextureCombination.Instance != null)
                {
                    MTextureCombination.Instance.GetTextureFileNamesBySize(iFilterSize, fileList);
                }
 
                foreach (String strFile in fileList)
                {
                    if (strFile != null)
                        m_listBoxAllTexture.Items.Add(strFile);
                }
                //String[] fileList = System.IO.Directory.GetFileSystemEntries(m_strTextureFolder);
                //foreach (String file in fileList)
                //{
                //    if (System.IO.Directory.Exists(file))
                //    {
                //        String fileName = System.IO.Path.GetFileName(file);
                //        m_listBoxAllTexture.Items.Add(fileName);
                //    }
                //}
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void m_btnSelectTexturePath_Click(object sender, EventArgs e)
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            // 选择纹理所在路径
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory+"Data\\Texture\\SceneObj\\1101\\";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                if (m_strTextureFolder != fbd.SelectedPath)
                {
                    m_textBoxTexturePath.Text = fbd.SelectedPath;
                    m_strTextureFolder = fbd.SelectedPath;
                    MTextureCombination.Instance.SetTextureFolder(m_strTextureFolder+"\\");
                    RefershAllTextureList();
                    m_textBoxTargetTexture.Clear();
                }

            }
        }

        private void m_btnSelectModelPath_Click(object sender, EventArgs e)
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            // 选择纹理所在路径
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory + "Data\\Model\\SceneObj\\1101\\";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                if (m_strModelFolder != fbd.SelectedPath)
                {
                    m_textBoxModelPath.Text = fbd.SelectedPath;
                    m_strModelFolder = fbd.SelectedPath;
                    // RefershAllTextureList();
                    MTextureCombination.Instance.SetModelFolder(m_strModelFolder + "\\");
                }
            }
        }

        private void m_btnRefersh_Click(object sender, EventArgs e)
        {
            m_textBoxMessage.Text += "开始解析纹理/模型数据,请稍候....";

            m_textBoxMessage.Update();
            MTextureCombination.Instance.RelodeTextureModelInfo();

            m_textBoxMessage.Text += "数据解析完成.\r\n";
        }

        private void TextureCombinationPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            // 关闭窗口
            MFramework.Shutdown();
        }

        private void m_comboBoxSizeFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefershAllTextureList();
        }

        private void m_listBoxAllTexture_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 选择所有纹理列表中的纹理项
            m_listBoxSelectAffectModel.Items.Clear();
            string textureName = m_listBoxAllTexture.SelectedItem as string;

            String[] fileList = new String[4096];

            MTextureCombination.Instance.GetModelByTexture(textureName, fileList);
            foreach (String strFile in fileList)
            {
                if (strFile != null)
                {
                    m_listBoxSelectAffectModel.Items.Add(strFile);
                }
            }
        }

        private void m_btnAddToCombineList_Click(object sender, EventArgs e)
        {
            // 从所有纹理列表中添加到待合并列表
            String strTexture = m_listBoxAllTexture.SelectedItem as String;
            if (strTexture!=null && !m_listBoxCombineTexture.Items.Contains(strTexture))
            {
                m_listBoxCombineTexture.Items.Add(strTexture);
            }
        }

        private void m_btnRemoveFromCombineList_Click(object sender, EventArgs e)
        {
            m_listBoxCombineTexture.Items.Remove(m_listBoxCombineTexture.SelectedItem);
        }

        private void m_btnClearAll_Click(object sender, EventArgs e)
        {
            m_listBoxCombineTexture.Items.Clear();
        }

        private void m_listBoxAllTexture_DoubleClick(object sender, EventArgs e)
        {

        }

        private void m_listBoxAllTexture_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // 鼠标双击,预览纹理

        }

        private void m_btnSelectTargetTexture_Click(object sender, EventArgs e)
        {
            // 选择目标文件
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory + "Data\\Texture\\SceneObj\\1101\\");
            SaveFileDialog saveFiledlg = new SaveFileDialog();
            saveFiledlg.Filter = "dds files (*.dds)|*.dds|All files (*.*)|*.*";
            saveFiledlg.FilterIndex = 1;
            saveFiledlg.RestoreDirectory = true;

            if (saveFiledlg.ShowDialog() == DialogResult.OK)
            {
                m_textBoxTargetTexture.Text = saveFiledlg.FileName;
            }
        }

        private void m_btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                // 预浏生成的纹理
               // MTextureCombination::Instance.CombineTextures()
                String strCompressType = m_comboBoxCompressMode.SelectedItem as String;
                String strTargetSize = m_comboBoxTargetTexSize.SelectedItem as String;
                int iTargetSize = Int32.Parse(strTargetSize);
                int iNumTexture = m_listBoxCombineTexture.Items.Count;
                String[] textureList = new String[iNumTexture];
                int iIdx = 0;
                foreach(Object strTexFile in m_listBoxCombineTexture.Items)
                {
                    textureList[iIdx++] = strTexFile as String;
                }
                MTextureCombination.Instance.PreviewCombineTextures(textureList, iTargetSize, strCompressType);
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void m_btnCombine_Click(object sender, EventArgs e)
        {
            try
            {
                // 预浏生成的纹理
                // MTextureCombination::Instance.CombineTextures()
                String strCompressType = m_comboBoxCompressMode.SelectedItem as String;
                String strTargetSize = m_comboBoxTargetTexSize.SelectedItem as String;
                int iTargetSize = Int32.Parse(strTargetSize);
                int iNumTexture = m_listBoxCombineTexture.Items.Count;
                String[] textureList = new String[iNumTexture];
                int iIdx = 0;
                foreach (Object strTexFile in m_listBoxCombineTexture.Items)
                {
                    textureList[iIdx++] = strTexFile as String;
                }
                string strTargetTexture = m_textBoxTargetTexture.Text;
                if (strTargetTexture == null)
                {
                    MessageBox.Show("请选择输出纹理名称.");
                    return;
                }

                if (!MTextureCombination.Instance.CombineTexturesAndChangeModelUV(textureList, iTargetSize, 
                                        strCompressType, strTargetTexture, m_checkBoxBackUpOldModel.Checked))
                {
                    MessageBox.Show("纹理合并失败.");
                }
                else
                {
                    MessageBox.Show("纹理合并完成.");
                }
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }
    }
}