﻿namespace TextureCombinationTool
{
    partial class TextureCombinationPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnPreview = new System.Windows.Forms.Button();
            this.m_checkBoxBackUpOldModel = new System.Windows.Forms.CheckBox();
            this.m_textBoxTargetTexture = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_btnSelectTargetTexture = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxModelPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_btnSelectModelPath = new System.Windows.Forms.Button();
            this.m_listBoxSelectAffectModel = new System.Windows.Forms.ListBox();
            this.m_btnCombine = new System.Windows.Forms.Button();
            this.m_btnClearAll = new System.Windows.Forms.Button();
            this.m_btnRemoveFromCombineList = new System.Windows.Forms.Button();
            this.m_btnAddToCombineList = new System.Windows.Forms.Button();
            this.m_listBoxCombineTexture = new System.Windows.Forms.ListBox();
            this.m_listBoxAllTexture = new System.Windows.Forms.ListBox();
            this.m_comboBoxSizeFilter = new System.Windows.Forms.ComboBox();
            this.m_textBoxTexturePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_btnSelectTexturePath = new System.Windows.Forms.Button();
            this.m_btnRefersh = new System.Windows.Forms.Button();
            this.m_textBoxMessage = new System.Windows.Forms.TextBox();
            this.m_comboBoxCompressMode = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.m_comboBoxTargetTexSize = new System.Windows.Forms.ComboBox();
            this.m_pictureBoxRenderBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxRenderBox)).BeginInit();
            this.SuspendLayout();
            // 
            // m_btnPreview
            // 
            this.m_btnPreview.Location = new System.Drawing.Point(65, 423);
            this.m_btnPreview.Name = "m_btnPreview";
            this.m_btnPreview.Size = new System.Drawing.Size(78, 24);
            this.m_btnPreview.TabIndex = 41;
            this.m_btnPreview.Text = "预览";
            this.m_btnPreview.UseVisualStyleBackColor = true;
            this.m_btnPreview.Click += new System.EventHandler(this.m_btnPreview_Click);
            // 
            // m_checkBoxBackUpOldModel
            // 
            this.m_checkBoxBackUpOldModel.AutoSize = true;
            this.m_checkBoxBackUpOldModel.Checked = true;
            this.m_checkBoxBackUpOldModel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxBackUpOldModel.Location = new System.Drawing.Point(249, 374);
            this.m_checkBoxBackUpOldModel.Name = "m_checkBoxBackUpOldModel";
            this.m_checkBoxBackUpOldModel.Size = new System.Drawing.Size(108, 16);
            this.m_checkBoxBackUpOldModel.TabIndex = 40;
            this.m_checkBoxBackUpOldModel.Text = "备份原模型文件";
            this.m_checkBoxBackUpOldModel.UseVisualStyleBackColor = true;
            // 
            // m_textBoxTargetTexture
            // 
            this.m_textBoxTargetTexture.Location = new System.Drawing.Point(124, 396);
            this.m_textBoxTargetTexture.Name = "m_textBoxTargetTexture";
            this.m_textBoxTargetTexture.Size = new System.Drawing.Size(204, 21);
            this.m_textBoxTargetTexture.TabIndex = 39;
            this.m_textBoxTargetTexture.Text = "E:\\mux\\Developer\\PlatformTools\\Bin\\scenedesigner\\test\\combinedTex.dds";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 400);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 38;
            this.label4.Text = "目标纹理";
            // 
            // m_btnSelectTargetTexture
            // 
            this.m_btnSelectTargetTexture.Location = new System.Drawing.Point(85, 396);
            this.m_btnSelectTargetTexture.Name = "m_btnSelectTargetTexture";
            this.m_btnSelectTargetTexture.Size = new System.Drawing.Size(33, 20);
            this.m_btnSelectTargetTexture.TabIndex = 37;
            this.m_btnSelectTargetTexture.Text = "...";
            this.m_btnSelectTargetTexture.UseVisualStyleBackColor = true;
            this.m_btnSelectTargetTexture.Click += new System.EventHandler(this.m_btnSelectTargetTexture_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 257);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 35;
            this.label3.Text = "相关模型";
            // 
            // m_textBoxModelPath
            // 
            this.m_textBoxModelPath.Location = new System.Drawing.Point(124, 28);
            this.m_textBoxModelPath.Name = "m_textBoxModelPath";
            this.m_textBoxModelPath.Size = new System.Drawing.Size(204, 21);
            this.m_textBoxModelPath.TabIndex = 34;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 33;
            this.label2.Text = "模型目录";
            // 
            // m_btnSelectModelPath
            // 
            this.m_btnSelectModelPath.Location = new System.Drawing.Point(85, 27);
            this.m_btnSelectModelPath.Name = "m_btnSelectModelPath";
            this.m_btnSelectModelPath.Size = new System.Drawing.Size(33, 20);
            this.m_btnSelectModelPath.TabIndex = 32;
            this.m_btnSelectModelPath.Text = "...";
            this.m_btnSelectModelPath.UseVisualStyleBackColor = true;
            this.m_btnSelectModelPath.Click += new System.EventHandler(this.m_btnSelectModelPath_Click);
            // 
            // m_listBoxSelectAffectModel
            // 
            this.m_listBoxSelectAffectModel.FormattingEnabled = true;
            this.m_listBoxSelectAffectModel.ItemHeight = 12;
            this.m_listBoxSelectAffectModel.Location = new System.Drawing.Point(26, 273);
            this.m_listBoxSelectAffectModel.Name = "m_listBoxSelectAffectModel";
            this.m_listBoxSelectAffectModel.Size = new System.Drawing.Size(146, 64);
            this.m_listBoxSelectAffectModel.TabIndex = 31;
            // 
            // m_btnCombine
            // 
            this.m_btnCombine.Location = new System.Drawing.Point(262, 423);
            this.m_btnCombine.Name = "m_btnCombine";
            this.m_btnCombine.Size = new System.Drawing.Size(78, 24);
            this.m_btnCombine.TabIndex = 30;
            this.m_btnCombine.Text = "合并纹理";
            this.m_btnCombine.UseVisualStyleBackColor = true;
            this.m_btnCombine.Click += new System.EventHandler(this.m_btnCombine_Click);
            // 
            // m_btnClearAll
            // 
            this.m_btnClearAll.Location = new System.Drawing.Point(384, 224);
            this.m_btnClearAll.Name = "m_btnClearAll";
            this.m_btnClearAll.Size = new System.Drawing.Size(47, 23);
            this.m_btnClearAll.TabIndex = 29;
            this.m_btnClearAll.Text = "清除";
            this.m_btnClearAll.UseVisualStyleBackColor = true;
            this.m_btnClearAll.Click += new System.EventHandler(this.m_btnClearAll_Click);
            // 
            // m_btnRemoveFromCombineList
            // 
            this.m_btnRemoveFromCombineList.Location = new System.Drawing.Point(178, 165);
            this.m_btnRemoveFromCombineList.Name = "m_btnRemoveFromCombineList";
            this.m_btnRemoveFromCombineList.Size = new System.Drawing.Size(47, 22);
            this.m_btnRemoveFromCombineList.TabIndex = 28;
            this.m_btnRemoveFromCombineList.Text = "<-";
            this.m_btnRemoveFromCombineList.UseVisualStyleBackColor = true;
            this.m_btnRemoveFromCombineList.Click += new System.EventHandler(this.m_btnRemoveFromCombineList_Click);
            // 
            // m_btnAddToCombineList
            // 
            this.m_btnAddToCombineList.Location = new System.Drawing.Point(178, 137);
            this.m_btnAddToCombineList.Name = "m_btnAddToCombineList";
            this.m_btnAddToCombineList.Size = new System.Drawing.Size(47, 22);
            this.m_btnAddToCombineList.TabIndex = 27;
            this.m_btnAddToCombineList.Text = "->";
            this.m_btnAddToCombineList.UseVisualStyleBackColor = true;
            this.m_btnAddToCombineList.Click += new System.EventHandler(this.m_btnAddToCombineList_Click);
            // 
            // m_listBoxCombineTexture
            // 
            this.m_listBoxCombineTexture.FormattingEnabled = true;
            this.m_listBoxCombineTexture.ItemHeight = 12;
            this.m_listBoxCombineTexture.Location = new System.Drawing.Point(231, 87);
            this.m_listBoxCombineTexture.Name = "m_listBoxCombineTexture";
            this.m_listBoxCombineTexture.Size = new System.Drawing.Size(147, 160);
            this.m_listBoxCombineTexture.TabIndex = 26;
            // 
            // m_listBoxAllTexture
            // 
            this.m_listBoxAllTexture.FormattingEnabled = true;
            this.m_listBoxAllTexture.ItemHeight = 12;
            this.m_listBoxAllTexture.Location = new System.Drawing.Point(25, 111);
            this.m_listBoxAllTexture.Name = "m_listBoxAllTexture";
            this.m_listBoxAllTexture.Size = new System.Drawing.Size(147, 136);
            this.m_listBoxAllTexture.TabIndex = 25;
            this.m_listBoxAllTexture.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.m_listBoxAllTexture_MouseDoubleClick);
            this.m_listBoxAllTexture.DoubleClick += new System.EventHandler(this.m_listBoxAllTexture_DoubleClick);
            this.m_listBoxAllTexture.SelectedIndexChanged += new System.EventHandler(this.m_listBoxAllTexture_SelectedIndexChanged);
            // 
            // m_comboBoxSizeFilter
            // 
            this.m_comboBoxSizeFilter.FormattingEnabled = true;
            this.m_comboBoxSizeFilter.Location = new System.Drawing.Point(25, 76);
            this.m_comboBoxSizeFilter.Name = "m_comboBoxSizeFilter";
            this.m_comboBoxSizeFilter.Size = new System.Drawing.Size(147, 20);
            this.m_comboBoxSizeFilter.TabIndex = 24;
            this.m_comboBoxSizeFilter.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxSizeFilter_SelectedIndexChanged);
            // 
            // m_textBoxTexturePath
            // 
            this.m_textBoxTexturePath.Location = new System.Drawing.Point(124, 7);
            this.m_textBoxTexturePath.Name = "m_textBoxTexturePath";
            this.m_textBoxTexturePath.Size = new System.Drawing.Size(204, 21);
            this.m_textBoxTexturePath.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 22;
            this.label1.Text = "纹理目录";
            // 
            // m_btnSelectTexturePath
            // 
            this.m_btnSelectTexturePath.Location = new System.Drawing.Point(85, 6);
            this.m_btnSelectTexturePath.Name = "m_btnSelectTexturePath";
            this.m_btnSelectTexturePath.Size = new System.Drawing.Size(33, 20);
            this.m_btnSelectTexturePath.TabIndex = 21;
            this.m_btnSelectTexturePath.Text = "...";
            this.m_btnSelectTexturePath.UseVisualStyleBackColor = true;
            this.m_btnSelectTexturePath.Click += new System.EventHandler(this.m_btnSelectTexturePath_Click);
            // 
            // m_btnRefersh
            // 
            this.m_btnRefersh.Location = new System.Drawing.Point(178, 55);
            this.m_btnRefersh.Name = "m_btnRefersh";
            this.m_btnRefersh.Size = new System.Drawing.Size(53, 23);
            this.m_btnRefersh.TabIndex = 42;
            this.m_btnRefersh.Text = "刷新";
            this.m_btnRefersh.UseVisualStyleBackColor = true;
            this.m_btnRefersh.Click += new System.EventHandler(this.m_btnRefersh_Click);
            // 
            // m_textBoxMessage
            // 
            this.m_textBoxMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_textBoxMessage.Location = new System.Drawing.Point(0, 453);
            this.m_textBoxMessage.Multiline = true;
            this.m_textBoxMessage.Name = "m_textBoxMessage";
            this.m_textBoxMessage.Size = new System.Drawing.Size(437, 61);
            this.m_textBoxMessage.TabIndex = 43;
            // 
            // m_comboBoxCompressMode
            // 
            this.m_comboBoxCompressMode.FormattingEnabled = true;
            this.m_comboBoxCompressMode.Location = new System.Drawing.Point(24, 372);
            this.m_comboBoxCompressMode.Name = "m_comboBoxCompressMode";
            this.m_comboBoxCompressMode.Size = new System.Drawing.Size(119, 20);
            this.m_comboBoxCompressMode.TabIndex = 44;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 354);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 12);
            this.label5.TabIndex = 45;
            this.label5.Text = "目标纹理压缩格式";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(158, 353);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 46;
            this.label6.Text = "目标纹理尺寸";
            // 
            // m_comboBoxTargetTexSize
            // 
            this.m_comboBoxTargetTexSize.FormattingEnabled = true;
            this.m_comboBoxTargetTexSize.Location = new System.Drawing.Point(155, 372);
            this.m_comboBoxTargetTexSize.Name = "m_comboBoxTargetTexSize";
            this.m_comboBoxTargetTexSize.Size = new System.Drawing.Size(76, 20);
            this.m_comboBoxTargetTexSize.TabIndex = 47;
            // 
            // m_pictureBoxRenderBox
            // 
            this.m_pictureBoxRenderBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxRenderBox.Location = new System.Drawing.Point(260, 257);
            this.m_pictureBoxRenderBox.Name = "m_pictureBoxRenderBox";
            this.m_pictureBoxRenderBox.Size = new System.Drawing.Size(97, 97);
            this.m_pictureBoxRenderBox.TabIndex = 48;
            this.m_pictureBoxRenderBox.TabStop = false;
            // 
            // TextureCombinationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 514);
            this.Controls.Add(this.m_pictureBoxRenderBox);
            this.Controls.Add(this.m_comboBoxTargetTexSize);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_comboBoxCompressMode);
            this.Controls.Add(this.m_textBoxMessage);
            this.Controls.Add(this.m_btnRefersh);
            this.Controls.Add(this.m_btnPreview);
            this.Controls.Add(this.m_checkBoxBackUpOldModel);
            this.Controls.Add(this.m_textBoxTargetTexture);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_btnSelectTargetTexture);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_textBoxModelPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_btnSelectModelPath);
            this.Controls.Add(this.m_listBoxSelectAffectModel);
            this.Controls.Add(this.m_btnCombine);
            this.Controls.Add(this.m_btnClearAll);
            this.Controls.Add(this.m_btnRemoveFromCombineList);
            this.Controls.Add(this.m_btnAddToCombineList);
            this.Controls.Add(this.m_listBoxCombineTexture);
            this.Controls.Add(this.m_listBoxAllTexture);
            this.Controls.Add(this.m_comboBoxSizeFilter);
            this.Controls.Add(this.m_textBoxTexturePath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_btnSelectTexturePath);
            this.Name = "TextureCombinationPanel";
            this.Text = "纹理合并工具";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TextureCombinationPanel_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxRenderBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnPreview;
        private System.Windows.Forms.CheckBox m_checkBoxBackUpOldModel;
        private System.Windows.Forms.TextBox m_textBoxTargetTexture;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button m_btnSelectTargetTexture;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxModelPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button m_btnSelectModelPath;
        private System.Windows.Forms.ListBox m_listBoxSelectAffectModel;
        private System.Windows.Forms.Button m_btnCombine;
        private System.Windows.Forms.Button m_btnClearAll;
        private System.Windows.Forms.Button m_btnRemoveFromCombineList;
        private System.Windows.Forms.Button m_btnAddToCombineList;
        private System.Windows.Forms.ListBox m_listBoxCombineTexture;
        private System.Windows.Forms.ListBox m_listBoxAllTexture;
        private System.Windows.Forms.ComboBox m_comboBoxSizeFilter;
        private System.Windows.Forms.TextBox m_textBoxTexturePath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button m_btnSelectTexturePath;
        private System.String m_strTextureFolder;   // 纹理所在目录
        private System.String m_strModelFolder;
        private System.Windows.Forms.Button m_btnRefersh;
        private System.Windows.Forms.TextBox m_textBoxMessage;
        private System.Windows.Forms.ComboBox m_comboBoxCompressMode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox m_comboBoxTargetTexSize;
        private System.Windows.Forms.PictureBox m_pictureBoxRenderBox;     // 模型所在目录
    }
}

