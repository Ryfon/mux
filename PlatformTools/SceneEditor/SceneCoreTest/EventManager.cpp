﻿#include "stdafx.h"
#include "EventManager.h"

CEvent::CEvent( int iId ) : m_iId( iId )
{}

CEvent::~CEvent()
{}

bool CEventManager::SendEvent( CEventPtr ptrEvent ) 
{
	bool bRt = true;
	EventListenerSetType::iterator iterListener = m_setListeners.begin();
	while( iterListener != m_setListeners.end() ) 
	{
		bRt = bRt && (*iterListener)->ProcessEvent( ptrEvent );
		++iterListener;
	}

	return bRt;
}

void CEventManager::PostEvent( CEventPtr ptrEvent ) 
{
	m_queEvents.push( ptrEvent );
}

void CEventManager::AddEventListener( CEventListenerBase* pListener ) 
{
	m_setListeners.insert( pListener );
}

void CEventManager::RemoveEventListener( CEventListenerBase* pListener ) 
{
	m_setListeners.erase( pListener );
}

CEventManager::CEventManager()
{}

CEventManager::~CEventManager()
{}

bool CEventManager::ProcessEvent() 
{
	bool bRt = true;

	EventQueueType tempDeque = m_queEvents;
	while( !tempDeque.empty() ) 
	{
		CEventPtr ptrEvent = tempDeque.front();
		EventListenerSetType::iterator iterListener = m_setListeners.begin();
		while( iterListener != m_setListeners.end() ) 
		{
			bRt = bRt && (*iterListener)->ProcessEvent( ptrEvent );
			++iterListener;
		}
		tempDeque.pop();
	}

	while ( !m_queEvents.empty() )
		m_queEvents.pop();

	return bRt;
}