﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#ifndef RENDERERS_H
#define RENDERERS_H


#include <NiSample.h>
class CTerrain;
class CLayerFog;

class Renderers : public NiSample
{
public:
    Renderers();

	virtual bool CreateShaderSystem();
    virtual bool CreateRenderer();

	virtual bool CreateUIElements();

	// 用 sce 场景导出文件创建场景
	virtual bool CreateScene();

	virtual void OnIdle(void);

	virtual bool CreateCamera();

	virtual bool CreateFrame();

	virtual void UpdateFrame();
	
	virtual void RenderFrame();

	virtual void Terminate();

	void EventTest();

	virtual bool CreateUISystem();

    virtual bool CreateNavigationControllers();

	virtual bool CompleteUISystem();

protected:

    NiColor			m_kBackgroundColor;
	CTerrain		*m_pTerrain;
	CLayerFog		*m_pLayerFog;

	bool RunShaderParsers(const char* pcShaderDir);
	bool RunShaderLibraries(const char* pcShaderDir);
	static bool ShaderCallback_LibraryClassCreate(const char* pcLibFile, 
		NiRenderer* pkRenderer, int iDirectoryCount, char* apcDirectories[], 
		bool bRecurseSubFolders, NiShaderLibrary** ppkLibrary);
	static unsigned int ShaderCallback_RunParser(const char* pcLibFile, 
		NiRenderer* pkRenderer, const char* pcDirectory, 
		bool bRecurseSubFolders);
	static unsigned int ShaderCallback_Errors(const char* pcError, 
		NiShaderError eError, bool bRecoverable);


};

#include "EventManager.h"

#define MYEVENTID		0x4000
#define YOUREVENTID		0x4001


class CMyEvent: public CEvent
{
public:
	CMyEvent(): CEvent(MYEVENTID ){}
	virtual ~CMyEvent(){}	
};


class CYourEvent : public CEvent
{
public:
	CYourEvent(): CEvent( YOUREVENTID ){}
	virtual ~CYourEvent(){}	
};

class CMyEventDisposal : public TEventListener<CMyEventDisposal>
{
public:
	CMyEventDisposal(){};
	~CMyEventDisposal(){};

	bool OnMyEvent( CEventPtr pEvent )
	{
		LOG( "disposal My Event!!!\n\n" );
		return true;
	}
};

class CYourEventDisposal : public TEventListener<CYourEventDisposal>
{
public:
	CYourEventDisposal(){};
	~CYourEventDisposal(){}; 

	bool OnYourEvent( CEventPtr pEvent )
	{
		LOG( "disposal Your Event!\n\n" );
		return true;
	}
};


#endif
