﻿
#pragma once

#include "Utility/TSingleton.h"
#include "Utility/GlobalHeader.h"
//#include "Utility/GlobalError.h"
#include "Utility/globalmacro.h"
#pragma warning(disable : 4267 )	// 关闭警告
#define NI_RELEASE_MODE_LOGGING 1

#include "NiMain.h"

#pragma comment( lib, "NiDX9Renderer.lib" )
#pragma comment( lib, "dxguid.lib" )
#pragma comment( lib, "NiSystem.lib" )
#pragma comment( lib, "NiMain.lib" )
#pragma comment( lib, "NiAnimation.lib" )
#pragma comment( lib, "NiInput.lib" )
#pragma comment( lib, "NiCollision.lib" )
#pragma comment( lib, "NiParticle.lib" )
#pragma comment( lib, "NiApplication.lib")
#pragma comment( lib, "NiSample.lib" )
#pragma comment( lib, "NiUserInterface.lib")
#pragma comment( lib, "NiCursor.lib")
#pragma comment( lib, "NiFont.lib" )

#pragma comment( lib, "Winmm.lib")


#ifdef _DEBUG

#pragma comment( lib, "Scenecore_d.lib" )
#pragma comment( lib, "Utility_d.lib" )
#pragma comment( lib, "TinyXML_D.lib")

#else

#pragma comment( lib, "Scenecore.lib" )
#pragma comment( lib, "Utility.lib" )
#pragma comment( lib, "TinyXML.lib")

#endif // _DEBUG
