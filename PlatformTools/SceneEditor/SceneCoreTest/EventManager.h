﻿#ifndef __EVENT_MANAGER_H__
#define __EVENT_MANAGER_H__

// 事件类
class CEvent : public NiRefObject
{
public:
	inline int GetId() const { return m_iId; }

	CEvent( int iId );
	virtual ~CEvent();

protected:
	
	// 事件Id
	int	m_iId;
};

NiSmartPointer( CEvent );

// 事件帧听器父类
class CEventListenerBase
{
	friend class CEventManager;
public:
	CEventListenerBase() {}
	virtual ~CEventListenerBase(){}

	// 处理事件, 返回处理的结果
	virtual bool ProcessEvent( CEventPtr pEvent ) = 0;
};

// 事件帧听器模板类
template < class _Derive >
class TEventListener : public CEventListenerBase
{
protected: 

 	typedef bool (_Derive::*EventFunctionType)( CEventPtr ); 
	typedef std::map< int, EventFunctionType >	EventProcessorMapType;

public:
	TEventListener()
	{
		CEventManager::GetInstance()->AddEventListener( this );
	}

	virtual ~TEventListener()
	{
		CEventManager::GetInstance()->RemoveEventListener( this );
	}

	void RegisterEventProcessor( int iEventId, EventFunctionType pEventProcessor ) 
	{
		m_mapEventProcessors[iEventId] = pEventProcessor;		
	}

	void UnregisterEventProcessor( int iEventId ) 
	{
		m_mapEventProcessors.erase( iEventId );
	}
	
	virtual void RegisterAllEventProcessors() {}
	
	virtual void UnregisterAllEventProcessors() {}

	virtual bool ProcessEvent( CEventPtr pEvent ) 
	{
		EventProcessorMapType::iterator iter = m_mapEventProcessors.find( pEvent->GetId() );
		if( iter != m_mapEventProcessors.end() ) 
		{
			return ((*(_Derive*)this).*(iter->second))( pEvent );
		}
		return true;
	}

protected:
	EventProcessorMapType	m_mapEventProcessors;
};

// 事件管理器
class CEventManager 
{
protected:
	CEventManager();
	virtual ~CEventManager();

public:
	static CEventManager*		GetInstance()
	{
		static CEventManager g_EventMgr;
		return &g_EventMgr;
	}

	bool ProcessEvent();

	bool SendEvent( CEventPtr ptrEvent );

	void PostEvent( CEventPtr ptrEvent );

	//! Add a event listener.
	/*!
	\param[in]	pListener	The event listener.
	*/
	void AddEventListener( CEventListenerBase* pListener );

	//! Remove a event listener.
	/*!
	\param[in]	pListener	The event listener.
	*/
	void RemoveEventListener( CEventListenerBase* pListener );

	// 事件容器
	typedef std::queue< CEventPtr >			EventQueueType;

	// 帧听器容器
	typedef std::set< CEventListenerBase* >	EventListenerSetType;

protected:

	EventQueueType			m_queEvents;
	EventListenerSetType	m_setListeners;
};

#endif	// __EVENT_MANAGER_H__
