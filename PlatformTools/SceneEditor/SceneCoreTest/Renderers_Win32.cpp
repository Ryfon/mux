﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#include "stdafx.h"
#include "Renderers.h"
#if defined(WIN32)
#include <NiDX9Renderer.h>
#include <NiD3D10Renderer.h>
#endif
//---------------------------------------------------------------------------
bool Renderers::CreateRenderer()
{
    // The NiApplication framework provides renderer creation, however, we  
    // can override the default CreateRenderer as we have done here. In this
    // instance, we create a basic renderer for our platform and then  
    // change the background color red to boot.
    //
    // In subsequent tutorials, we won't bother overriding the renderer,
    // as the one provided by the framework is adequate for our purposes and
    // we can always change the background color at a later time.
    
    unsigned int uiWidth = m_pkAppWindow->GetWidth();
    unsigned int uiHeight = m_pkAppWindow->GetHeight();

    if (m_bD3D10Renderer)
    {
        NiD3D10Renderer::CreationParameters kParameters(
            GetRenderWindowReference());
        NiD3D10RendererPtr spD3D10Renderer;

        bool bResult = NiD3D10Renderer::Create(kParameters, spD3D10Renderer);

        if (bResult)
            m_spRenderer = spD3D10Renderer;
    }
    
    // Fall back to DX9 if D3D10 not present
    if (m_spRenderer == NULL)
    {
        m_spRenderer = NiDX9Renderer::Create(uiWidth, uiHeight,
            NiDX9Renderer::USE_NOFLAGS, GetWindowReference(), 
            GetRenderWindowReference());
    }

    if (m_spRenderer == NULL)
    {
        NiMessageBox("Unable to create a renderer!", "Renderer Failure!");
        QuitApplication();
        return false;
    }
    else
    {
        m_spRenderer->SetBackgroundColor(m_kBackgroundColor);
    }
    
    return true;
}
//---------------------------------------------------------------------------
