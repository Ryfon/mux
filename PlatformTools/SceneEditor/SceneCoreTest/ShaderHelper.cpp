﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "ShaderHelper.h"
#include <NiApplication.h>

// This sample only has static library configurations
#define _LIB
    #include <NSBShaderLib.h>
    #include <NSFParserLib.h>
#undef _LIB

#if defined(WIN32)
    #pragma comment(lib, "NiBinaryShaderLibDX9.lib")
    #pragma comment(lib, "NiD3D10BinaryShaderLibD3D10.lib")
    #pragma comment(lib, "NSBShaderLibDX9.lib")
    #pragma comment(lib, "NSFParserLibDX9.lib")
#endif

//---------------------------------------------------------------------------
bool ShaderHelper::SetupShaderSystem()
{
    char* pcShaderDir = getenv("EGB_SHADER_LIBRARY_PATH");
    NiShaderFactory::AddShaderProgramFileDirectory( pcShaderDir );

    if (!RunShaderParsers(pcShaderDir))
    {
        NiMessageBox("Failed to run shader parsers!", "ERROR");
        return false;
    }

    if (!RegisterShaderLibraries(pcShaderDir))
    {
        NiMessageBox("Failed to register shader libraries!", "ERROR");
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
bool ShaderHelper::RunShaderParsers(const char* pcShaderDir)
{
    NiShaderFactory::RegisterRunParserCallback(RunParser);

    unsigned int uiCount = NiShaderFactory::LoadAndRunParserLibrary(
        NULL, NiApplication::ConvertMediaFilename(pcShaderDir), true);

    char acTemp[256];
    NiSprintf(acTemp, 256, "Parsed %d shaders from NSFShaderParser!\n", 
        uiCount);
    NiOutputDebugString(acTemp);

    return true;
}
//---------------------------------------------------------------------------
bool ShaderHelper::RegisterShaderLibraries(const char* pcShaderDir)
{
    NiShaderFactory::RegisterClassCreationCallback(LibraryClassCreate);

    unsigned int uiCount = 1;
    char* apcDirectories[1];
    apcDirectories[0] = 
        (char*)NiApplication::ConvertMediaFilename(pcShaderDir);

    if (!NiShaderFactory::LoadAndRegisterShaderLibrary(NULL,
        uiCount, apcDirectories, true))
    {
        NiMessageBox("Failed to load shader library!", "ERROR");
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
bool ShaderHelper::CleanupShaderLibraries()
{
    // Note that we don't have to do anything here. The scene will clean up
    // the shader instances, and the renderer shutdown code will take care
    // of the factory!
    return true;
}
//---------------------------------------------------------------------------
bool ShaderHelper::LibraryClassCreate(const char* pcLibFile, 
    NiRenderer* pkRenderer, int iDirectoryCount, char* apcDirectories[], 
    bool bRecurseSubFolders, NiShaderLibrary** ppkLibrary)
{
    *ppkLibrary = NULL;

    // Create the NSB Shader Library.
    return NSBShaderLib_LoadShaderLibrary(pkRenderer, iDirectoryCount, 
        apcDirectories, bRecurseSubFolders, ppkLibrary);
}
//---------------------------------------------------------------------------
unsigned int ShaderHelper::RunParser(const char* pcLibFile, 
    NiRenderer* pkRenderer, const char* pcDirectory, 
    bool bRecurseSubFolders)
{
    // Run the NSF Parser.
    return NSFParserLib_RunShaderParser(pcDirectory, bRecurseSubFolders);
}
//---------------------------------------------------------------------------
