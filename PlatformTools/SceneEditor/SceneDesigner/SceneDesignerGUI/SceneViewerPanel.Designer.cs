﻿
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using PluginAPI = Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;

namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    partial class SceneViewerPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SceneViewerPanel));
            this.m_tvScene = new System.Windows.Forms.TreeView();
            this.m_menuTreeView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_miAddGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.m_miActive = new System.Windows.Forms.ToolStripMenuItem();
            this.m_miDeactive = new System.Windows.Forms.ToolStripMenuItem();
            this.m_miRename = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ilTreeView = new System.Windows.Forms.ImageList(this.components);
            this.m_tbToolbar = new System.Windows.Forms.ToolBar();
            this.m_tbbAddGroup = new System.Windows.Forms.ToolBarButton();
            this.m_tbbDeleteNode = new System.Windows.Forms.ToolBarButton();
            this.m_tbbSyncToScene = new System.Windows.Forms.ToolBarButton();
            this.m_ilToolbar = new System.Windows.Forms.ImageList(this.components);
            this.m_menuTreeView.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_tvScene
            // 
            this.m_tvScene.AllowDrop = true;
            this.m_tvScene.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tvScene.ContextMenuStrip = this.m_menuTreeView;
            this.m_tvScene.ImageIndex = 0;
            this.m_tvScene.ImageList = this.m_ilTreeView;
            this.m_tvScene.Location = new System.Drawing.Point(10, 30);
            this.m_tvScene.Name = "m_tvScene";
            this.m_tvScene.SelectedImageIndex = 0;
            this.m_tvScene.Size = new System.Drawing.Size(270, 340);
            this.m_tvScene.TabIndex = 0;
            this.m_tvScene.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.m_tvScene_NodeMouseDoubleClick);
            this.m_tvScene.DragDrop += new System.Windows.Forms.DragEventHandler(this.m_tvScene_DragDrop);
            this.m_tvScene.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.m_tvScene_AfterSelect);
            this.m_tvScene.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.m_tvScene_NodeMouseClick);
            this.m_tvScene.DragEnter += new System.Windows.Forms.DragEventHandler(this.m_tvScene_DragEnter);
            this.m_tvScene.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.m_tvScene_ItemDrag);
            // 
            // m_menuTreeView
            // 
            this.m_menuTreeView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_miAddGroup,
            this.m_miActive,
            this.m_miDeactive,
            this.m_miRename});
            this.m_menuTreeView.Name = "m_menuTreeView";
            this.m_menuTreeView.Size = new System.Drawing.Size(135, 92);
            this.m_menuTreeView.Opening += new System.ComponentModel.CancelEventHandler(this.m_menuTreeView_Opening);
            // 
            // m_miAddGroup
            // 
            this.m_miAddGroup.Name = "m_miAddGroup";
            this.m_miAddGroup.Size = new System.Drawing.Size(134, 22);
            this.m_miAddGroup.Text = "添加组";
            this.m_miAddGroup.Click += new System.EventHandler(this.m_miAddGroup_Click);
            // 
            // m_miActive
            // 
            this.m_miActive.Name = "m_miActive";
            this.m_miActive.Size = new System.Drawing.Size(134, 22);
            this.m_miActive.Text = "设为活动组";
            this.m_miActive.Click += new System.EventHandler(this.m_miActive_Click);
            // 
            // m_miDeactive
            // 
            this.m_miDeactive.Name = "m_miDeactive";
            this.m_miDeactive.Size = new System.Drawing.Size(134, 22);
            this.m_miDeactive.Text = "取消活动组";
            this.m_miDeactive.Click += new System.EventHandler(this.m_miDeactive_Click);
            // 
            // m_miRename
            // 
            this.m_miRename.Name = "m_miRename";
            this.m_miRename.Size = new System.Drawing.Size(134, 22);
            this.m_miRename.Text = "重命名";
            this.m_miRename.Click += new System.EventHandler(this.m_miRename_Click);
            // 
            // m_ilTreeView
            // 
            this.m_ilTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("m_ilTreeView.ImageStream")));
            this.m_ilTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.m_ilTreeView.Images.SetKeyName(0, "01_group.png");
            this.m_ilTreeView.Images.SetKeyName(1, "02_geometry.png");
            this.m_ilTreeView.Images.SetKeyName(2, "03_light.png");
            this.m_ilTreeView.Images.SetKeyName(3, "04_active_group.png");
            this.m_ilTreeView.Images.SetKeyName(4, "05_npc.png");
            // 
            // m_tbToolbar
            // 
            this.m_tbToolbar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.m_tbToolbar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.m_tbbAddGroup,
            this.m_tbbDeleteNode,
            this.m_tbbSyncToScene});
            this.m_tbToolbar.ButtonSize = new System.Drawing.Size(23, 22);
            this.m_tbToolbar.Divider = false;
            this.m_tbToolbar.DropDownArrows = true;
            this.m_tbToolbar.ImageList = this.m_ilToolbar;
            this.m_tbToolbar.Location = new System.Drawing.Point(0, 0);
            this.m_tbToolbar.Name = "m_tbToolbar";
            this.m_tbToolbar.ShowToolTips = true;
            this.m_tbToolbar.Size = new System.Drawing.Size(292, 26);
            this.m_tbToolbar.TabIndex = 0;
            this.m_tbToolbar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.m_tbToolbar_ButtonClick);
            // 
            // m_tbbAddGroup
            // 
            this.m_tbbAddGroup.ImageIndex = 0;
            this.m_tbbAddGroup.Name = "m_tbbAddGroup";
            this.m_tbbAddGroup.ToolTipText = "添加组";
            // 
            // m_tbbDeleteNode
            // 
            this.m_tbbDeleteNode.ImageIndex = 1;
            this.m_tbbDeleteNode.Name = "m_tbbDeleteNode";
            this.m_tbbDeleteNode.ToolTipText = "删除节点";
            // 
            // m_tbbSyncToScene
            // 
            this.m_tbbSyncToScene.ImageIndex = 2;
            this.m_tbbSyncToScene.Name = "m_tbbSyncToScene";
            this.m_tbbSyncToScene.ToolTipText = "节点与场景同步";
            // 
            // m_ilToolbar
            // 
            this.m_ilToolbar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("m_ilToolbar.ImageStream")));
            this.m_ilToolbar.TransparentColor = System.Drawing.Color.Transparent;
            this.m_ilToolbar.Images.SetKeyName(0, "01_group.png");
            this.m_ilToolbar.Images.SetKeyName(1, "02_delete_node.png");
            this.m_ilToolbar.Images.SetKeyName(2, "03_sync_to_scene.png");
            // 
            // SceneViewerPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(292, 379);
            this.Controls.Add(this.m_tvScene);
            this.Controls.Add(this.m_tbToolbar);
            this.KeyPreview = true;
            this.Name = "SceneViewerPanel";
            this.Text = "Tree View";
            this.Load += new System.EventHandler(this.SceneViewerPanel_Load);
            this.m_menuTreeView.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        #region EventManager Handlers

        private bool m_bBuildAndSync = false;
        private bool m_bSyncListBoxToSelectionService = false;
        private bool m_bSyncHideFreezeButtons = false;
        private bool m_bSyncSelectionSetComboBox = false;

        private void EventManager_NewSceneLoaded(MScene pmScene)
        {
            if (pmScene == MFramework.Instance.Scene)
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //BuildAndSync();
                    //SyncSelectionSetComboBox();
                    // 清空

                }
                else
                {
                    m_bBuildAndSync = true;
                    m_bSyncSelectionSetComboBox = true;
                }
            }
            m_tvScene.Update();
        }

        private void EventManager_SelectedEntitiesChanged()
        {
            if (!MFramework.Instance.PerformingLongOperation)
            {
                //SyncListBoxToSelectionService();
                //SyncHideFreezeButtons();
            }
            else
            {
                m_bSyncListBoxToSelectionService = true;
                m_bSyncHideFreezeButtons = true;
            }
            //MEntity[] amEntitiesSelected = SelectionService.GetSelectedEntities();
            //if (amEntitiesSelected.Length > 0)
            //{
            //    m_tvScene.SelectedNode =
            //        m_htTreeNode[amEntitiesSelected[amEntitiesSelected.Length - 1].Name] as TreeNode;
            //}
            //else
            //{
            //    m_tvScene.SelectedNode = null;
            //}
        }

        private void EventManager_EntityAddedToScene(MScene pmScene,
            MEntity pmEntity)
        {
            if (pmScene == MFramework.Instance.Scene)
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //BuildAndSync();
                }
                else
                {
                    m_bBuildAndSync = true;
                } 

                TreeNode treeNode = new TreeNode(pmEntity.Name);
                treeNode.Name = pmEntity.Name;
                treeNode.ImageIndex = treeNode.SelectedImageIndex = MScene.GetEntityType(pmEntity);

                if (m_tnActiveGroup != null)
                {
                    m_tnActiveGroup.Nodes.Add(treeNode);
                }
                else
                {
                    m_tvScene.Nodes.Add(treeNode);
                }

                if (!m_htTreeNode.ContainsKey(pmEntity.Name))
                {
                    m_htTreeNode.Add(pmEntity.Name, treeNode);
                }
  


                m_tvScene.SelectedNode = treeNode;
                m_tvScene.Update();
            }
        }

        private void EventManager_EntityRemovedFromScene(MScene pmScene,
            MEntity pmEntity, bool bResolveDependencies)
        {
            if (pmScene == MFramework.Instance.Scene)
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //BuildAndSync();
                }
                else
                {
                    m_bBuildAndSync = true;
                }

                if (!m_htTreeNode.ContainsKey(pmEntity.Name))
                {
                    return;
                }

                TreeNode treeNode = m_htTreeNode[pmEntity.Name] as TreeNode;
                if (treeNode == null)
                {
                    return;
                }

                m_htTreeNode.Remove(pmEntity.Name);
                treeNode.Remove();
                m_tvScene.Update();
            }
        }

        private void EventManager_EntityNameChanged(MEntity pmEntity,
            string strOldName, bool bInBatch)
        {
            if (MFramework.Instance.Scene.IsEntityInScene(pmEntity))
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //BuildAndSync();
                }
                else
                {
                    m_bBuildAndSync = true;
                }

                if (!m_htTreeNode.ContainsKey(pmEntity.Name))
                {
                    return;
                }

                TreeNode treeNode = m_htTreeNode[strOldName] as TreeNode;
                m_htTreeNode.Remove(strOldName);
                treeNode.Name = pmEntity.Name;
                treeNode.Text = pmEntity.Name;
                m_htTreeNode[pmEntity.Name] = treeNode;
            }

        }

        private void EventManager_EntityHiddenStateChanged(MEntity pmEntity,
            bool bHidden)
        {
            if (MFramework.Instance.Scene.IsEntityInScene(pmEntity))
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //BuildAndSync();
                }
                else
                {
                    m_bBuildAndSync = true;
                }
            }
        }

        private void EventManager_EntityFrozenStateChanged(MEntity pmEntity,
            bool bFrozen)
        {
            if (MFramework.Instance.Scene.IsEntityInScene(pmEntity))
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //BuildAndSync();
                }
                else
                {
                    m_bBuildAndSync = true;
                }
            }
        }

        private void EventManager_SelectionSetAddedToScene(MScene pmScene,
            MSelectionSet pmSelectionSet)
        {
            if (MFramework.Instance.Scene == pmScene)
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //SyncSelectionSetComboBox();
                }
                else
                {
                    m_bSyncSelectionSetComboBox = true;
                }
            }
        }

        private void EventManager_SelectionSetRemovedFromScene(MScene pmScene,
            MSelectionSet pmSelectionSet)
        {
            if (MFramework.Instance.Scene == pmScene)
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //SyncSelectionSetComboBox();
                }
                else
                {
                    m_bSyncSelectionSetComboBox = true;
                }
            }
        }

        private void EventManager_SelectionSetNameChanged(
            MSelectionSet pmSelectionSet, string strOldName, bool bInBatch)
        {
            if (MFramework.Instance.Scene.IsSelectionSetInScene(
                pmSelectionSet))
            {
                if (!MFramework.Instance.PerformingLongOperation)
                {
                    //SyncSelectionSetComboBox();
                }
                else
                {
                    m_bSyncSelectionSetComboBox = true;
                }
            }
        }

        private void EventManager_LongOperationCompleted()
        {
            if (m_bBuildAndSync)
            {
                //BuildAndSync();
            }
            else if (m_bSyncListBoxToSelectionService)
            {
                //SyncListBoxToSelectionService();
            }

            if (m_bSyncHideFreezeButtons)
            {
                //SyncHideFreezeButtons();
            }

            if (m_bSyncSelectionSetComboBox)
            {
                //SyncSelectionSetComboBox();
            }

            m_bBuildAndSync = false;
            m_bSyncListBoxToSelectionService = false;
            m_bSyncHideFreezeButtons = false;
            m_bSyncSelectionSetComboBox = false;
        }
        #endregion


        #region Private Helpers
        private void AttachToEventManager()
        {
            MEventManager pmEventMgr = MFramework.Instance.EventManager;
            pmEventMgr.SelectedEntitiesChanged +=
                new MEventManager.__Delegate_SelectedEntitiesChanged(
                EventManager_SelectedEntitiesChanged);
            pmEventMgr.EntityAddedToScene +=
                new MEventManager.__Delegate_EntityAddedToScene(
                EventManager_EntityAddedToScene);
            pmEventMgr.EntityRemovedFromScene +=
                new MEventManager.__Delegate_EntityRemovedFromScene(
                EventManager_EntityRemovedFromScene);
            pmEventMgr.EntityNameChanged +=
                new MEventManager.__Delegate_EntityNameChanged(
                EventManager_EntityNameChanged);
            pmEventMgr.EntityHiddenStateChanged += new MEventManager
                .__Delegate_EntityHiddenStateChanged(
                EventManager_EntityHiddenStateChanged);
            pmEventMgr.EntityFrozenStateChanged += new MEventManager
                .__Delegate_EntityFrozenStateChanged(
                EventManager_EntityFrozenStateChanged);
            pmEventMgr.SelectionSetAddedToScene += new MEventManager
                .__Delegate_SelectionSetAddedToScene(
                EventManager_SelectionSetAddedToScene);
            pmEventMgr.SelectionSetRemovedFromScene += new MEventManager
                .__Delegate_SelectionSetRemovedFromScene(
                EventManager_SelectionSetRemovedFromScene);
            pmEventMgr.SelectionSetNameChanged += new MEventManager
                .__Delegate_SelectionSetNameChanged(
                EventManager_SelectionSetNameChanged);
            pmEventMgr.NewSceneLoaded += new MEventManager
                .__Delegate_NewSceneLoaded(EventManager_NewSceneLoaded);
            pmEventMgr.LongOperationCompleted += new MEventManager
                .__Delegate_LongOperationCompleted(
                EventManager_LongOperationCompleted);
        }
        #endregion


        #region Service Accessors
        private static ISelectionService ms_pmSelectionService = null;
        private static ISelectionService SelectionService
        {
            get
            {
                if (ms_pmSelectionService == null)
                {
                    ms_pmSelectionService = ServiceProvider.Instance
                        .GetService(typeof(ISelectionService)) as
                        ISelectionService;
                    Debug.Assert(ms_pmSelectionService != null,
                        "Selection service not found!");
                }
                return ms_pmSelectionService;
            }
        }

        private static ICommandService ms_pmCommandService = null;
        private static ICommandService CommandService
        {
            get
            {
                if (ms_pmCommandService == null)
                {
                    ms_pmCommandService = ServiceProvider.Instance
                        .GetService(typeof(ICommandService)) as
                        ICommandService;
                    Debug.Assert(ms_pmCommandService != null,
                        "Command service not found!");
                }
                return ms_pmCommandService;
            }
        }

        private static IMessageService ms_pmMessageService = null;
        private static IMessageService MessageService
        {
            get
            {
                if (ms_pmMessageService == null)
                {
                    ms_pmMessageService = ServiceProvider.Instance
                        .GetService(typeof(IMessageService)) as
                        IMessageService;
                    Debug.Assert(ms_pmMessageService != null,
                        "Message service not found!");
                }
                return ms_pmMessageService;
            }
        }

        private static ISelectionSetService ms_pmSelectionSetService = null;
        private static ISelectionSetService SelectionSetService
        {
            get
            {
                if (ms_pmSelectionSetService == null)
                {
                    ms_pmSelectionSetService = ServiceProvider.Instance
                        .GetService(typeof(ISelectionSetService)) as
                        ISelectionSetService;
                    Debug.Assert(ms_pmSelectionSetService != null,
                        "Selection set service not found!");
                }
                return ms_pmSelectionSetService;
            }
        }

        private static ISettingsService ms_pmSettingsService = null;
        private static ISettingsService SettingsService
        {
            get
            {
                if (ms_pmSettingsService == null)
                {
                    ms_pmSettingsService = ServiceProvider.Instance
                        .GetService(typeof(ISettingsService)) as
                        ISettingsService;
                    Debug.Assert(ms_pmSettingsService != null,
                        "Settings service not found!");
                }
                return ms_pmSettingsService;
            }
        }
        #endregion

        private System.Windows.Forms.TreeView m_tvScene;
        private System.Windows.Forms.ToolBar m_tbToolbar;
        private System.Windows.Forms.ToolBarButton m_tbbAddGroup;
        private System.Windows.Forms.ToolBarButton m_tbbDeleteNode;
        //private System.Windows.Forms.ToolBarButton m_tbbHideUnhide;
        //private System.Windows.Forms.ToolBarButton m_tbbFreezeUnfreeze;
        //private System.Windows.Forms.ToolBarButton m_tbbSeparator1;
        //private System.Windows.Forms.ToolBarButton m_tbbSelectAll;
        //private System.Windows.Forms.ToolBarButton m_tbbSelectNone;
        //private System.Windows.Forms.ToolBarButton m_tbbInvertSelection;
        //private System.Windows.Forms.ToolBarButton m_tbbSeparator2;
        //private System.Windows.Forms.ToolBarButton m_tbbFilter;
        //private System.Windows.Forms.ImageList m_ilToobarImages;
        private ImageList m_ilToolbar;      
        private ImageList m_ilTreeView;
        private ContextMenuStrip m_menuTreeView;
        private ToolStripMenuItem m_miAddGroup;
        private ToolStripMenuItem m_miActive;
        private ToolStripMenuItem m_miRename;
        private ToolStripMenuItem m_miDeactive;    
        private TreeNode m_tnActiveGroup;       // 当前被激活组
        private Hashtable m_htTreeNode;
        private ToolBarButton m_tbbSyncToScene;        // 场景Entity Name 到 树节点映射

    }
}