﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    public partial class GlobalConfigForm : Form
    {
        public GlobalConfigForm()
        {
            InitializeComponent();
            m_pCustomColors = new int[1];
            //m_checkBoxPostEffectEnable.Checked = MGlobalSetting.Instance.GetPostEffectEnabled(); 
            // 初始化次要物品列表 [7/9/2009 hemeng]
            //if (m_MinorEntityPanel == null)
            //{
            //    //  [7/9/2009 hemeng]
            //    m_MinorEntityPanel = new MinorEntityListForm();

            //}
           // panel_MinorEntity.Controls.Add(m_MinorEntityPanel);
           // m_MinorEntityPanel.Dock = DockStyle.Fill;
        }

        private void GlobalConfigForm_Load(object sender, EventArgs e)
        {}

        private void GlobalConfigForm_Shown(object sender, EventArgs e)
        {}

        private void m_pictureBoxFogColor_Click(object sender, EventArgs e)
        {
           // 首先刷新，防止转换场景时出错
            m_btnRefresh_Click(sender,e);
            int iColorR = MGlobalSetting.Instance.R;
            int iColorG = MGlobalSetting.Instance.G;
            iColorG = iColorG << 8;
            int iColorB = MGlobalSetting.Instance.B;
            iColorB = iColorB << 16;

            m_pCustomColors[0] = iColorR + iColorG + iColorB;
            // 选择雾的颜色
            ColorDialog colorDlg = new ColorDialog();
            colorDlg.CustomColors =  m_pCustomColors;            
                    
            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                m_pictureBoxFogColor.BackColor = colorDlg.Color;

                // add by 和萌
                // 保留自定义颜色直到关闭场景编辑器
                m_pCustomColors = colorDlg.CustomColors;
                m_btnOK_Click(sender, e);
            }
            else
            {
                m_pCustomColors = colorDlg.CustomColors;
            }
           
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                // 点确定按钮
                bool bEnabled = m_checkBoxFogEnable.Checked;
                System.Drawing.Color color = m_pictureBoxFogColor.BackColor;
                float fDepth = float.Parse(m_textBoxFogDepth.Text);
                MGlobalSetting.Instance.SetFogProperties(ref color, fDepth, bEnabled);

                float fNear = float.Parse(m_TextBoxNear.Text);
                float fFar = float.Parse(m_TextBoxFar.Text);

                MGlobalSetting.Instance.SetNearFarPlane(fNear, fFar);
                MGlobalSetting.Instance.ApplyFogToScene();
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }

        }

        private void m_btnRefresh_Click(object sender, EventArgs e)
        {
            m_checkBoxFogEnable.Checked = MGlobalSetting.Instance.FogEnabled;
            m_pictureBoxFogColor.BackColor = Color.FromArgb(MGlobalSetting.Instance.R, MGlobalSetting.Instance.G, MGlobalSetting.Instance.B);
            m_textBoxFogDepth.Text = MGlobalSetting.Instance.Depth.ToString();
            m_TextBoxNear.Text = MGlobalSetting.Instance.NearPlane.ToString();
            m_TextBoxFar.Text = MGlobalSetting.Instance.FarPlane.ToString();
        }

        private void m_tbTerrainAmbient_TextChanged(object sender, EventArgs e)
        {
            // ambient 系数被修改
            float fAmbien = MTerrain.Instance.TerrainAmbient;

            try
            {
                fAmbien = float.Parse(m_tbTerrainAmbient.Text);
                MTerrain.Instance.TerrainAmbient = fAmbien;

            }
            catch(Exception)
            {
                m_tbTerrainAmbient.Text = fAmbien.ToString();
            }


        }

        private void m_btnRefreshTerrainParam_Click(object sender, EventArgs e)
        {
            m_tbTerrainAmbient.Text = MTerrain.Instance.TerrainAmbient.ToString();
        }

        private void m_btnResetRandomSize_Click(object sender, EventArgs e)
        {
            float fSizeDown = 0.5f;
            float fSizeUpper = 2.0f;

            try
            {
                fSizeDown = (float)Convert.ToDouble(m_textBoxRandomSizeDown.Text);
                if(fSizeDown <= 0)
                {
                    fSizeDown = 0.1f;
                    m_textBoxRandomSizeDown.Text = Convert.ToString(fSizeDown);
                }
            }
            catch(Exception err)
            {
                MessageBox.Show(err.ToString());
                return;
            }

            try
            {
                fSizeUpper = (float)Convert.ToDouble(m_textBoxRandomSizeUpper.Text);
                if(fSizeUpper < fSizeDown)
                {
                    fSizeUpper = fSizeDown + 0.1f;
                    m_textBoxRandomSizeUpper.Text = Convert.ToString(fSizeUpper);
                }
            }
            catch(Exception err)
            {
                MessageBox.Show(err.ToString());
                return;
            }

            MFramework.Instance.Scene.SetEntityRandomSize(fSizeDown, fSizeUpper);
        }

        private void btn_SetRectSelPrec_Click(object sender, EventArgs e)
        {
            try
            {
                int iRectSelPrec = Convert.ToInt32(m_textBoxRectSelPrec.Text);
                if(iRectSelPrec < 0)
                {
                    iRectSelPrec = 0;
                    m_textBoxRectSelPrec.Text = Convert.ToString(iRectSelPrec);
                }

                MFramework.Instance.Scene.SetRectSelectPrec(iRectSelPrec);
            }
            catch(Exception err)
            {
                MessageBox.Show(err.ToString());
                return;
            }
        }

        private void btn_MinorEntityList_Click(object sender, EventArgs e)
        {
            if (m_MinorEntityPanel == null || m_MinorEntityPanel.IsDisposed)
            {
                m_MinorEntityPanel = new MinorEntityListForm();
                m_MinorEntityPanel.Show();
            }
            else
            {
                m_MinorEntityPanel.Activate();
            }
        }




        //private void m_checkBoxPostEffectEnable_CheckedChanged(object sender, EventArgs e)
        //{
        //    MGlobalSetting.Instance.SetPostEffectEnabled(m_checkBoxPostEffectEnable.Checked);
        //}

        
    }
}