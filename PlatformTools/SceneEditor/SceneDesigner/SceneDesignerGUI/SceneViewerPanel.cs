﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using PluginAPI = Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using System.Xml;

namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    public partial class SceneViewerPanel : Form
    {
        //public SceneViewer()
        //{
        //    InitializeComponent();
        //}

        public SceneViewerPanel(IUICommandService commandService)
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
            commandService.BindCommands(this);  // 与 command service 绑定
            m_htTreeNode = new Hashtable();        // 场景Entity Name 到 树节点映射
		}

        private void SceneViewerPanel_Load(object sender, EventArgs e)
        {
            this.AttachToEventManager();
        }

        private void RemoveInvalidNode(System.String strName)
        {
            TreeNode node = m_htTreeNode[strName] as TreeNode;
            if (node != null)
            {
                m_htTreeNode.Remove(strName);
                node.Remove();
            }
        }

        private void m_tvScene_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // 节点选择发生改变
            MEntity[] amEntitiesToRemove = SelectionService.GetSelectedEntities(); // 之前的选择列表
            MEntity[] amEntitiesToAdd ; // 要构建的选择列表

            if(m_tvScene.SelectedNode == null)  // 当前没有选择节点
            {
                amEntitiesToAdd = new MEntity[0];
            }
            else if (IsGroup(m_tvScene.SelectedNode)) // 当前选择了一个 group
            {
                ArrayList tnChildList = new ArrayList();
                GetAllChildren(m_tvScene.SelectedNode, tnChildList);
                amEntitiesToAdd = new MEntity[tnChildList.Count];
                int idx = 0;
                foreach(TreeNode tnChild in tnChildList)
                {
                    MEntity pkEntity = MFramework.Instance.Scene.GetEntityByName(tnChild.Text);
                    if (pkEntity == null)
                    {
                        RemoveInvalidNode(tnChild.Text);
                        continue;
                    }
                    amEntitiesToAdd.SetValue(pkEntity, idx);
                    ++idx;
                }
            }
            else // 当前只选择了一个物件
            {
                  // 根据名称找到 entity
                MEntity pkEntity = MFramework.Instance.Scene.GetEntityByName(m_tvScene.SelectedNode.Text);
                if (pkEntity == null)
                {
                    RemoveInvalidNode(m_tvScene.SelectedNode.Text);
                    return;
                }
                amEntitiesToAdd = new MEntity[1];
                amEntitiesToAdd.SetValue(pkEntity, 0);
            }

            if (amEntitiesToRemove.Length > 0 ||
                amEntitiesToAdd.Length > 0)
            {
                
                CommandService.BeginUndoFrame("Replace main selection " +
                    "with " + amEntitiesToAdd.Length + (amEntitiesToAdd.Length == 1 ? " entity" :
                        " entities"));

                //m_bSynchingSelectionService = true;

                SelectionService.RemoveEntitiesFromSelection(
                    amEntitiesToRemove);

                SelectionService.AddEntitiesToSelection(amEntitiesToAdd);
                // m_bSynchingSelectionService = false;
                CommandService.EndUndoFrame(SelectionService
                    .CommandsAreUndoable);
            }
                
        }

        private bool IsGroup(TreeNode treeNode)
        {
            return treeNode.ImageIndex == 0 || treeNode.ImageIndex == 3;
        }

        private bool IsGeometry(TreeNode treeNode)
        {
            return treeNode.ImageIndex == 1;
        }

        // 获取treeNode的所有子节点
        private void GetAllChildren(TreeNode tnParent, ArrayList tnList)
        {
            foreach(TreeNode tnChild in tnParent.Nodes)
            {
                if (IsGroup(tnChild))
                {
                    GetAllChildren(tnChild, tnList);
                }
                else
                {
                    tnList.Add(tnChild);
                }
            }
        }

        private void AddGroup()
        {
            TreeNode newGroup = new TreeNode(); ;
            newGroup.Text = "Group";
            newGroup.SelectedImageIndex = 0;
            newGroup.ImageIndex = 0;
            if (m_tnActiveGroup != null)
            {
                m_tnActiveGroup.Nodes.Add(newGroup);
            }
            else
            {
                m_tvScene.Nodes.Add(newGroup);
            }
        }

        private void DeleteTreeNode()
        {
            if (null != m_tvScene.SelectedNode)
            {
                ArrayList children = new ArrayList();
                GetAllChildren(m_tvScene.SelectedNode, children);
                if (children.Count > 0)
                {
                    // 弹出对话框，不可删除
                    MessageBox.Show("不可删除非空组。","提示", MessageBoxButtons.OK);
                    return;
                }
 
                if (m_tvScene.SelectedNode.ImageIndex == 3) // 被删除的是active group
                {
                    m_tnActiveGroup = null;
                }
                if (IsGroup(m_tvScene.SelectedNode))
                {
                    m_tvScene.Nodes.Remove(m_tvScene.SelectedNode);
                    m_tvScene.SelectedNode = null;
                }
            }
        }

        // 检查 TreeView 中节点与场景物件是否对应并根据场景物件进行同步
        private void SyncToScene()
        {
            MScene scene =  MFramework.Instance.Scene;
            MEntity[] amEntities =scene.GetEntities();
            foreach (MEntity pmEntity in amEntities)
            {
             
                // 找到 tree view 中没有的 entity
                if (!m_htTreeNode.ContainsKey(pmEntity.Name))
                {
                    TreeNode treeNode = new TreeNode(pmEntity.Name);
                    treeNode.Name = pmEntity.Name;
                    treeNode.ImageIndex = treeNode.SelectedImageIndex = MScene.GetEntityType(pmEntity);
                    m_tvScene.Nodes.Add(treeNode);
                    m_htTreeNode.Add(pmEntity.Name, treeNode);
                    m_tvScene.Update();
                }
            }
        }

        // 清理所有节点
        public void ClearAllNodes()
        {
            m_htTreeNode.Clear();
            m_tvScene.Nodes.Clear();
        }

        private void m_tbToolbar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if (e.Button == m_tbbAddGroup)  
            {
                // 增加组,弹出请输入组名对话框
                AddGroup();
            }
            else if (e.Button == m_tbbDeleteNode)
            {
                DeleteTreeNode();
            }           
            else if (e.Button == m_tbbSyncToScene)
            {
                SyncToScene();
            }
        }

        private void m_menuTreeView_Opening(object sender, CancelEventArgs e)
        {
            // 右键菜单弹出
            if (null == m_tvScene.SelectedNode)
            {
                // 没有选择节点
                //m_miAddGroup;
                m_miActive.Enabled = false;
                m_miRename.Enabled = false;
            }
            else
            {
                m_miActive.Enabled = true;
                m_miRename.Enabled = true;
            }

            if (m_tnActiveGroup == null)
            {
                m_miDeactive.Enabled = false;
            }
            else
            {
                m_miDeactive.Enabled = true;
            }
        }

        private void m_miAddGroup_Click(object sender, EventArgs e)
        {
            // 右键菜单 添加组
            AddGroup();
        }

        private void m_miActive_Click(object sender, EventArgs e)
        {
            // 右键菜单 设为活动组
            if (m_tvScene.SelectedNode.ImageIndex == 0)
            {
                if (m_tnActiveGroup != null)
                {
                    m_tnActiveGroup.ImageIndex = 0;
                    m_tnActiveGroup.SelectedImageIndex = 0;
                }

                m_tnActiveGroup = m_tvScene.SelectedNode;
                m_tnActiveGroup.ImageIndex = 3;
                m_tnActiveGroup.SelectedImageIndex = 3;
            }
        }

        private void m_miDeactive_Click(object sender, EventArgs e)
        {
            // 右键菜单 取消活动组
            if (m_tnActiveGroup != null)
            {
                m_tnActiveGroup.ImageIndex = 0;
                m_tnActiveGroup.SelectedImageIndex = 0;
                m_tnActiveGroup = null;
            }
        }

        private void m_tvScene_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (IsGroup(e.Node))
            {
                ChangeTreeNodeNameDlg changeNameDlg = new ChangeTreeNodeNameDlg(e.Node.Text);
                changeNameDlg.ShowDialog();
                e.Node.Text = changeNameDlg.GetName();
            }
        }

        private void m_tvScene_ItemDrag(object sender, ItemDragEventArgs e)
        {
            //if (e.Button == MouseButtons.Left)
            //{
            //    TreeNode node = e.Item as TreeNode;
            //    m_tnOnDragNode = node;
            //   // m_tvScene.SelectedNode = node;
            //}
            DoDragDrop(e.Item, DragDropEffects.Move );

        }

        private void m_tvScene_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void m_tvScene_DragDrop(object sender, DragEventArgs e)
        {
            TreeNode newNode;
            if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
            {
                Point pt = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
                TreeNode destNode = ((TreeView)sender).GetNodeAt(pt);

                newNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

                if ((destNode != null && !IsGroup(destNode)) || destNode == newNode)
                {
                    return;
                }


                TreeNode parent = newNode.Parent;
                if (null == parent)
                {
                    m_tvScene.Nodes.Remove(newNode);
                }
                else
                {
                    parent.Nodes.Remove(newNode);
                }

                if (null == destNode)
                {
                    m_tvScene.Nodes.Add(newNode);
                }
                else
                {
                    destNode.Expand();
                    destNode.Nodes.Add(newNode);

                }

            }
        }

        private void m_tvScene_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            
        }

        private void m_miRename_Click(object sender, EventArgs e)
        {
            if (m_tvScene.SelectedNode != null && IsGroup(m_tvScene.SelectedNode))
            {
                ChangeTreeNodeNameDlg changeNameDlg = new ChangeTreeNodeNameDlg(m_tvScene.SelectedNode.Text);
                changeNameDlg.ShowDialog();
                m_tvScene.SelectedNode.Text = changeNameDlg.GetName();
            }

        }

        public void SaveTreeViewToXML(String strFileName)
        {
            XmlTextWriter writer = new XmlTextWriter(strFileName, null);
            writer.Formatting = Formatting.Indented;    // 按照级别缩进
            writer.WriteStartDocument();
            writer.WriteStartElement("Node");
            writer.WriteAttributeString("Name", "Root");    // 先写入Root节点.读取的时候不解析
            writer.WriteAttributeString("Type", "group");
            foreach (TreeNode node in m_tvScene.Nodes)
            {
                WriteSubNode(node, writer);
            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }

        private void WriteSubNode(TreeNode tNode, System.Xml.XmlTextWriter writer)
        {
            writer.WriteStartElement("Node");
            writer.WriteAttributeString("Name", tNode.Text);
            if (tNode.ImageIndex == 0 || tNode.ImageIndex == 3)
            {
                writer.WriteAttributeString("Type", "group");
            }
            else if (tNode.ImageIndex == 1)
            {
                writer.WriteAttributeString("Type", "geometry");
            }
            else if (tNode.ImageIndex == 2)
            {
                writer.WriteAttributeString("Type", "light");
            }
            else if (tNode.ImageIndex == 4)
            {
                writer.WriteAttributeString("Type", "npc");
            }

            foreach (TreeNode subNode in tNode.Nodes)
            {
                WriteSubNode(subNode, writer);
            }
            writer.WriteEndElement();
        }

        public void LoadTreeViewFromXML(String strFileName)
        {
            try
            {
                XmlDocument dom = new XmlDocument();
                dom.Load(strFileName);
                m_tvScene.SelectedNode = null;
                m_tvScene.Nodes.Clear();
                m_htTreeNode.Clear();
                m_tnActiveGroup = null;

                AddNode(dom.DocumentElement, m_tvScene);
                m_tvScene.Update();
            }
            catch(XmlException xmlEx)
            {
                MessageBox.Show(xmlEx.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddNode(XmlElement inXmlElement, TreeView tvParent)
        {
            TreeNode tRootNode = new TreeNode(inXmlElement.GetAttribute("Name"));

            if (inXmlElement.HasChildNodes)
            {
                for (int i=0; i<inXmlElement.ChildNodes.Count; i++)
                {
                    XmlElement xElem = inXmlElement.ChildNodes[i] as XmlElement;
                    AddNode(xElem, tRootNode);
                }
            }

            foreach (TreeNode tNode in tRootNode.Nodes)
            {
                tvParent.Nodes.Add(tNode);
            }
        }

        private void AddNode(XmlElement inXmlElement, TreeNode tParentNode)
        {
            TreeNode tNewNode = new TreeNode(inXmlElement.GetAttribute("Name"));
            tNewNode.Name = tNewNode.Text;
            String type = inXmlElement.GetAttribute("Type");
            if (type == "group")
            {
                tNewNode.ImageIndex = tNewNode.SelectedImageIndex = 0;
            }
            else if (type == "geometry")
            {
                tNewNode.ImageIndex = tNewNode.SelectedImageIndex = 1;
            }
            else if (type == "light")
            {
                tNewNode.ImageIndex = tNewNode.SelectedImageIndex = 2;
            }
            else if (type == "npc")
            {
                tNewNode.ImageIndex = tNewNode.SelectedImageIndex = 4;
            }

            tParentNode.Nodes.Add(tNewNode);
            if (type != "group")
            {
                m_htTreeNode.Add(tNewNode.Text, tNewNode);
            }

            if (inXmlElement.HasChildNodes)
            {
                for (int i = 0; i < inXmlElement.ChildNodes.Count; i++)
                {
                    XmlElement xElem = inXmlElement.ChildNodes[i] as XmlElement;
                    AddNode(xElem, tNewNode);
                }
            }
        }
    }
}