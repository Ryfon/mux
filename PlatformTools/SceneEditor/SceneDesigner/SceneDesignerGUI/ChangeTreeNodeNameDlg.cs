﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    public partial class ChangeTreeNodeNameDlg : Form
    {
        public ChangeTreeNodeNameDlg(string strOldName)
        {
            InitializeComponent();
            m_tbName.Text = strOldName;
            m_strNewName = strOldName;
            m_tbName.Focus();
            m_tbName.SelectAll();
        }

        private void m_btOK_Click(object sender, EventArgs e)
        {
            m_strNewName = m_tbName.Text;
            Close();
        }

        private void m_btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void m_tbName_KeyDown(object sender, KeyEventArgs e)
        {
            //e.KeyCode 
        }
    }
}