﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using PluginAPI = Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using System.Xml;

namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    public partial class TemaplatePreviewForm : Form
    {
        public TemaplatePreviewForm() //IUICommandService commandService)
        {
            InitializeComponent();
            //commandService.BindCommands(this);  // 与 command service 绑定
        }

        private void TemaplatePreviewForm_Load(object sender, EventArgs e)
        {
            MTemplatePreviewer.Instance.SetTargetWnd(this.Handle);
            m_pictureBoxPreview.Image = MTemplatePreviewer.Instance.PreviewBitmap;
        }

        private void TemaplatePreviewForm_Paint(object sender, PaintEventArgs e)
        {
            //MTemplatePreviewer.Instance.SetTargetWnd(this.Handle);
           // m_pictureBoxPreview.Image = 
            m_pictureBoxPreview.BackgroundImage = MTemplatePreviewer.Instance.PreviewBitmap;
        }

        private void TemaplatePreviewForm_MouseDown(object sender, MouseEventArgs e)
        {
            MTemplatePreviewer.Instance.SetTargetWnd(this.Handle);
            m_pictureBoxPreview.Image = MTemplatePreviewer.Instance.PreviewBitmap;
        }
    }
}