﻿namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    partial class ChangeTreeNodeNameDlg
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_tbName = new System.Windows.Forms.TextBox();
            this.m_btOK = new System.Windows.Forms.Button();
            this.m_btCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_tbName
            // 
            this.m_tbName.Location = new System.Drawing.Point(24, 25);
            this.m_tbName.Name = "m_tbName";
            this.m_tbName.Size = new System.Drawing.Size(244, 21);
            this.m_tbName.TabIndex = 0;
            this.m_tbName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_tbName_KeyDown);
            // 
            // m_btOK
            // 
            this.m_btOK.Location = new System.Drawing.Point(30, 61);
            this.m_btOK.Name = "m_btOK";
            this.m_btOK.Size = new System.Drawing.Size(73, 21);
            this.m_btOK.TabIndex = 1;
            this.m_btOK.Text = "确定";
            this.m_btOK.UseVisualStyleBackColor = true;
            this.m_btOK.Click += new System.EventHandler(this.m_btOK_Click);
            // 
            // m_btCancel
            // 
            this.m_btCancel.Location = new System.Drawing.Point(177, 61);
            this.m_btCancel.Name = "m_btCancel";
            this.m_btCancel.Size = new System.Drawing.Size(73, 21);
            this.m_btCancel.TabIndex = 2;
            this.m_btCancel.Text = "取消";
            this.m_btCancel.UseVisualStyleBackColor = true;
            this.m_btCancel.Click += new System.EventHandler(this.m_btCancel_Click);
            // 
            // ChangeTreeNodeNameDlg
            // 
            this.AcceptButton = this.m_btOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.m_btCancel;
            this.ClientSize = new System.Drawing.Size(292, 87);
            this.Controls.Add(this.m_btCancel);
            this.Controls.Add(this.m_btOK);
            this.Controls.Add(this.m_tbName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangeTreeNodeNameDlg";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "重命名";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public string GetName()
        {
            return m_tbName.Text;
        }

        private System.Windows.Forms.TextBox m_tbName;
        private System.Windows.Forms.Button m_btOK;
        private System.Windows.Forms.Button m_btCancel;
        private System.String m_strNewName;
    }
}