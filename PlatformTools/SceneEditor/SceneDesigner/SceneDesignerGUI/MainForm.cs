﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.GUI.PluginCore;
using Emergent.Gamebryo.SceneDesigner.GUI.PluginCore.StandardServices;
using Emergent.Gamebryo.SceneDesigner.GUI.Utility;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;

namespace Emergent.Gamebryo.SceneDesigner.GUI
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class z : System.Windows.Forms.Form
	{
        private ShortcutServiceImpl m_shortcutService;
        private System.ComponentModel.IContainer components;
        private TimedInvoker m_timer;
	    IUICommandService m_uiCommandService;
	    UICommand m_idleCommand;
        private static readonly string dockConfigSettingName = 
            "DockConfig";
        private System.Windows.Forms.Timer m_timerComponent;
        private static readonly string m_paletteFileExtension = "pal";
        private bool m_lastDirtyStatus;
        private DateTime m_timeOfLastAutoSave;
        private bool m_bClearWindowLayout = false;
        private bool m_bProcessDelayedLoading;
        static private Mutex m_restartLock = null;
        private bool m_bWindowPositionSet = false;

        // begin [7/22/2009]
        private bool m_bClearLightAffectLists = false;
        // end [7/22/2009]

        /// <summary>
        /// Only one instace of Scene Designer is allowed.
        /// </summary>
        static private Mutex m_instanceLock;

        // 笔刷大小递进值 [5/4/2009]
        static float m_fBrushUnit = 0.3f;

        private class PluginLoadException : ApplicationException
        {
            public PluginLoadException(string msg) : base (msg) {}
        }



        public z()
        {


            //
            // Required for Windows Form Designer support
            //
            m_bProcessDelayedLoading = true;
            InitializeComponent();
            Debug.WriteLine("End of InitializeComponent()\n");

            WinFormsUtility.MainForm = this;
            WinFormsUtility.DockPanel = this.m_ctlDockPanel;
            LoadPlugins();
            Debug.WriteLine("End of LoadPlugins()\n");

            InitializeGUI();
            Debug.WriteLine("End of InitializeGUI()\n");

            ServiceProvider sp = ServiceProvider.Instance;
            m_uiCommandService = sp.GetService(typeof(IUICommandService))
                as IUICommandService;

            MFramework.Instance.Startup();
            Debug.WriteLine("End of MFramework.Instance.Startup()\n");
        }

        private ShortcutServiceImpl ShortcutService
        {
            get
            {
                if (m_shortcutService == null)
                {
                    ServiceProvider sp = ServiceProvider.Instance;
                    m_shortcutService = 
                        sp.GetService(typeof(IShortcutService)) 
                        as ShortcutServiceImpl;                    
                }
                return m_shortcutService;
            }
        }

        private void TimerCallBack(object sender, EventArgs e)
        {
            try
            {
                //It may be possible to get this callback in the middle of
                //Shutting things down, so guard against that
                if (!this.Disposing && !this.IsDisposed)
                {
                    m_idleCommand.DoClick(this, null);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception thrown in timer code:");
                Debug.WriteLine(ex);
            }
        }

        private void LoadPlugins()
        {
            try 
            {
                string pluginPath =
                    MFramework.Instance.AppStartupPath + @"plugins";

                PluginManager.LoadPlugins(pluginPath);
            }
            catch (Exception e)
            {
                //This amounts to a catastrophic error. The application
                //cannot recover from this, so we will print this dialog
                //and throw a PlinLoadException, which will cause the app 
                //to exit
                Console.WriteLine("Uncought exception loading plugins:");
                Console.WriteLine(e);
                MessageBox.Show("Application could not load plugins" + 
                    Environment.NewLine +
                    e.ToString(),
                                "Critical Error");
                throw new PluginLoadException(
                       "Plugins could not load properly");
            }
        }

        private static string ms_strDisplayNewSceneFormSettingName =
            "DisplayNewSceneForm";
        private static string ms_strDisplayNewSceneFormOptionName =
            "Palettes.Prompt to Select Palette Folder for New Scenes";

        private void InitializeGUI()
        {
            ServiceProvider sp = ServiceProvider.Instance;
            IUICommandService commandService =
                sp.GetService(typeof(IUICommandService))
                as IUICommandService;
    
            commandService.BindCommands(this);
    
    
            ISettingsService settingsService = 
                sp.GetService(typeof(ISettingsService))
                as ISettingsService;

            settingsService.RegisterSettingsObject(
                "AutoSaveIntervalInMinutes", 1, SettingsCategory.PerUser);

            settingsService.RegisterSettingsObject(
                ms_strDisplayNewSceneFormSettingName, true,
                SettingsCategory.PerUser);

            IOptionsService pmOptionsService = sp.GetService(
                typeof(IOptionsService)) as IOptionsService;
            Debug.Assert(pmOptionsService != null,
                "Options service not found!");

            pmOptionsService.AddOption(ms_strDisplayNewSceneFormOptionName,
                SettingsCategory.PerUser,
                ms_strDisplayNewSceneFormSettingName);
            pmOptionsService.SetHelpDescription(
                ms_strDisplayNewSceneFormOptionName, "Indicates whether or " +
                "not a dialog box is displayed when a new scene is created " +
                "that prompts the user to select a palette folder " +
                "location. If this option is set to false, the most " +
                "recently used palette folder location will be used for " +
                "the new scene.");
    
            MenuServiceImpl menuService =
                sp.GetService(typeof(IMenuService)) as MenuServiceImpl;

            // This function must be first called here to load the saved size
            // for the Scene Designer window. It will be called again later to
            // ensure that the initialization of the third party docking code
            // does not resize the window from its stored size. The Boolean
            // passed in here allows the function to detect where it is being
            // called from to process special logic.
            LoadMainWindowPosition(false);

            menuService.AnnotateMenus();
            ComponentManagementUtilities.BuildComponentListFromPalettes(
                MFramework.Instance.PaletteManager.GetPalettes());
            MRUManager.Init();

            FileHandlerServiceImpl fileHandlerService =
                sp.GetService(typeof (IFileHandlerService)) 
                as FileHandlerServiceImpl;

            fileHandlerService.RegisterFileLoader(".gsa",
              new FileLoadHandler(SceneFileLoaderCallBack));


        }

        private void LoadMainWindowPosition(bool bFromOnIdle)
        {
            // This function is called twice: once during window creation and
            // once on the first OnIdle call. This is necessary to ensure that
            // the final window size actually matches the size stored in the
            // settings. bFromOnIdle indicates whether or not this function
            // is being called from the OnIdle function. A value of true
            // indicates that this is the second call to this function.

            // Get settings service.
            ServiceProvider sp = ServiceProvider.Instance;
            ISettingsService settingsService = 
                sp.GetService(typeof(ISettingsService))
                as ISettingsService;

            // Load window settings.
            object objWindowState = settingsService.GetSettingsObject(
                "MainWindowState", SettingsCategory.PerUser);
            object objWindowSize = settingsService.GetSettingsObject(
                "MainWindowSize", SettingsCategory.PerUser);
            object objWindowLocation = settingsService.GetSettingsObject(
                "MainWindowLocation", SettingsCategory.PerUser);

            if (objWindowState != null)
            {
                if (objWindowSize != null && objWindowLocation != null)
                {
                    this.ClientSize = (Size)objWindowSize;
                    this.Location = (Point)objWindowLocation;
                }

                FormWindowState ws = (FormWindowState)objWindowState;
                if (ws != FormWindowState.Minimized)
                {
                    this.WindowState = ws;
                }
            }
            else
            {
                // If we are in this block, it indicates that no window
                // settings exist yet. This happens the first time Scene
                // Designer is run for a user. In this case, the window is
                // intially set to be maximized. If this is the second call
                // to this function, then the window state is set to Normal,
                // but the size of the window is set to be slightly smaller
                // than its maximized state. This is to avoid a graphical
                // problem that can exhibit itself when the Scene Designer
                // window is maximized with particular configurations.

                this.WindowState = FormWindowState.Maximized;
                if (bFromOnIdle)
                {
                    Size clientSize = this.ClientSize;
                    clientSize.Height -= 10;
                    clientSize.Width -= 10;
                    this.WindowState = FormWindowState.Normal;
                    this.ClientSize = clientSize;
                }
            }

            // This block of code ensures that the launched Scene Designer
            // window is initially displayed on top of all other windows on the
            // desktop. If this is removed, the initialization of the thrid-
            // party docking code will cause the Scene Designer window to
            // appear behind other open windows.
            if (bFromOnIdle)
            {
                this.TopMost = true;
                this.TopMost = false;
            }
        }

        private void SaveMainWindowPosition()
        {
            if (this.WindowState != FormWindowState.Minimized)
            {
                ServiceProvider sp = ServiceProvider.Instance;
                ISettingsService settingsService =
                    sp.GetService(typeof(ISettingsService))
                    as ISettingsService;

                settingsService.SetSettingsObject("MainWindowState",
                    this.WindowState, SettingsCategory.PerUser);

                if (this.WindowState != FormWindowState.Maximized)
                {
                    settingsService.SetSettingsObject("MainWindowSize",
                        this.ClientSize, SettingsCategory.PerUser);

                    settingsService.SetSettingsObject("MainWindowLocation",
                        this.Location, SettingsCategory.PerUser);
                }
            }
        }

        [UICommandHandler("ReCheckSceneAfterPaletteImport")]
        private void OnReCheckScene(object sender, EventArgs args)
        {
            MPalette[] palettes = 
                MFramework.Instance.PaletteManager.GetPalettes();
            MScene scene =MFramework.Instance.Scene;

            ConflictManagementUtilities.CheckForConflicts(scene,
                palettes);
	        MPalette orphans = 
                EntityManagementUtilities.ResolveMasterEntities(
                scene, palettes);
            //We should never get orphans as the result of importing a palette
            Debug.Assert(orphans.Scene.GetEntities().Length == 0);
            orphans.Dispose();
        }

        [UICommandHandler("SavePalettes")]
        private void OnSavePalettes(object sender, EventArgs args)
        {
            SavePalettes();
        }

        private void SavePalettes()
        {
            MPalette[] dirtyPalettes = GetDirtyPalettes();

            // add [6/8/2009 hemeng]
            // copy current palette to secen dir
            // 等待验收[6/9/2009 hemeng]
            string strSceneName = MFramework.Instance.CurrentFilename;
            string strPaletteName = System.IO.Path.GetFileNameWithoutExtension(strSceneName);

            MPaletteManager paletteManager =
               MFramework.Instance.PaletteManager;

            foreach (MPalette palette in paletteManager.GetPalettes())
            {
                if (strPaletteName.Equals(palette.Name) == true)
                {
                    CopyCurrentPaletteToDirectory(strSceneName, strPaletteName);
                }
            }
          

            if (dirtyPalettes.Length == 0)
            {
                return;
            }

            if (!CheckPaletteFolderExists())
            {
                return;
            }

            if (!CheckForReadOnlyPaletteFiles(dirtyPalettes))
            {
                return;
            }

            string[] streamingExtensions = 
                MUtility.GetStreamingFormatExtensions();                       

            string paletteFolder = paletteManager.PaletteFolder;

            foreach (MPalette palette in paletteManager.GetPalettes())
            {
                if (palette.DontSave)
                {
                    if (palette.Scene.Dirty)
                    {
                        DirtyBitUtilities.MakeSceneClean(palette.Scene);
                    }
                    continue;
                }

                if (!palette.Scene.Dirty)
                    continue;

                SceneManagementUtilities.SetNameOnTemplates(palette);

                string paletteFileName = paletteFolder + palette.Name 
                    + "." + m_paletteFileExtension;

                FileInfo fi = new FileInfo(paletteFileName);
                bool bReadonly = (fi.Attributes & FileAttributes.ReadOnly)
                    == FileAttributes.ReadOnly;

                if (fi.Exists && bReadonly)
                {
                    fi.Attributes = fi.Attributes & 
                        ~FileAttributes.ReadOnly;
                }
                
                paletteManager.SavePalette(palette, 
                    paletteFolder + palette.Name + "." + 
                    m_paletteFileExtension, streamingExtensions[0]);
                
                DirtyBitUtilities.MakeSceneClean(palette.Scene);              
                
            }
            
        }

        /// <summary>
        /// Checks to make sure the palette files are writeable
        /// </summary>
        /// <param name="palettes"></param>
        /// <returns>true</returns>
	    private bool CheckForReadOnlyPaletteFiles(MPalette[] palettes)
	    {
            MPaletteManager paletteManager =
                MFramework.Instance.PaletteManager;
            string folder = paletteManager.PaletteFolder;

            ArrayList readonlyPaletteFiles = new ArrayList();

	        foreach (MPalette palette in palettes)
	        {
	            string palettePath = string.Format("{0}{1}.{2}",
                    folder, palette.Name, m_paletteFileExtension);
                FileInfo fi = new FileInfo(palettePath);
                if (fi.Exists)
                {
                    if ((fi.Attributes & FileAttributes.ReadOnly) == 
                        FileAttributes.ReadOnly)
                    {
                        readonlyPaletteFiles.Add(fi.FullName);
                    }
                }
	        }
            if (readonlyPaletteFiles.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("The following palette file(s) are read-only.\n");
                foreach (string paletteFile in readonlyPaletteFiles)
                {
                    sb.AppendFormat("{0}\n", paletteFile);
                }
                sb.Append("Would you like to over write them?\n");
                sb.Append(
                    "Note: If you click 'No', none " + 
                    "of your palettes will be saved");
                string messageText = sb.ToString();
                DialogResult result = MessageBox.Show(messageText,
                    "Palette Files Are Read Only.", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                return (result == DialogResult.Yes);
            }
            return true;
	    }


	    private MPalette[] GetDirtyPalettes()
	    {
            MPaletteManager paletteManager = 
                MFramework.Instance.PaletteManager;
	        ArrayList dirtyPalettes = new ArrayList();

            foreach (MPalette palette in paletteManager.GetPalettes())
            {
                if (palette.Scene.Dirty)
                {
                    dirtyPalettes.Add(palette);
                }
            }
            return dirtyPalettes.ToArray(typeof(MPalette)) as MPalette[];
	    }

	    private bool CheckPaletteFolderExists()
	    {
	        MPaletteManager paletteManager = 
                MFramework.Instance.PaletteManager;
            DirectoryInfo di = new DirectoryInfo(paletteManager.PaletteFolder);
            if (!di.Exists)
            {
                DialogResult result = 
                    MessageBox.Show("The Palette Folder:\n" + 
                    paletteManager.PaletteFolder + "\n"+
                    "does not exist. Would you like to create it?\n"
                    +"Note: if you chose not to create the folder, \n" +
                    "your palettes will not be saved.",
                    "Palette Folder Not Found", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    di.Create();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
	    }

	    /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if (components != null) 
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(z));
            this.m_ctlDockPanel = new WeifenLuo.WinFormsUI.DockPanel();
            this.m_menuMain = new System.Windows.Forms.MainMenu(this.components);
            this.m_timerComponent = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // m_ctlDockPanel
            // 
            this.m_ctlDockPanel.ActiveAutoHideContent = null;
            this.m_ctlDockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ctlDockPanel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.m_ctlDockPanel.Location = new System.Drawing.Point(0, 0);
            this.m_ctlDockPanel.Name = "m_ctlDockPanel";
            this.m_ctlDockPanel.Size = new System.Drawing.Size(1028, 725);
            this.m_ctlDockPanel.TabIndex = 0;
            // 
            // m_timerComponent
            // 
            this.m_timerComponent.Enabled = true;
            this.m_timerComponent.Interval = 1000;
            this.m_timerComponent.Tick += new System.EventHandler(this.m_timerComponent_Tick);
            // 
            // z
            // 
            this.AllowDrop = true;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.ClientSize = new System.Drawing.Size(1028, 725);
            this.Controls.Add(this.m_ctlDockPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MaximumSize = new System.Drawing.Size(2458, 2206);
            this.Menu = this.m_menuMain;
            this.Name = "z";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Gamebryo Scene Designer";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDragDrop);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) 
        {
            Debug.WriteLine("主入口\n");
            System.AppDomain.CurrentDomain.AppendPrivatePath("plugins");
            m_restartLock = new Mutex(false, "SceneDesignerRestart");
            DateTime before = DateTime.Now;
            m_restartLock.WaitOne();
            DateTime after = DateTime.Now;
            Debug.WriteLine(
            "Waited " + (after - before).TotalMilliseconds.ToString()
                + "ms for startup mutext");
            m_restartLock.ReleaseMutex();           
            m_restartLock = null;

            //Now, allow only one instance.
            string mutexGuid = "{A8DE0E56-4130-4f92-A3D4-391D7A799F96}";
            // {A8DE0E56-4130-4f92-A3D4-391D7A799F96}

            Debug.WriteLine( "线程对象创建\n" );
            m_instanceLock = new Mutex(false, mutexGuid);

            bool aquiredLock = m_instanceLock.WaitOne(new TimeSpan(0, 0, 1),
                true);

            if (!aquiredLock)
            {
                string message = string.Format(
                    "{0} is already running.\n" 
                    + "Only one instance is supported.",
                    Application.ProductName);
                    MessageBox.Show(message, 
                        string.Format("Cannot Launch {0}",
                        Application.ProductName), 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Debug.WriteLine( "MFrameWork 初始化\n" );
            MFramework.Init();

            Debug.WriteLine( "Invalidator 初始化\n" );
            Invalidator.Init();

            Debug.WriteLine("Handle D3D10 \n");
            // Handle D3D10 command-line parameter.
            foreach (string strArg in args)
            {
                string strUpperArg = strArg.ToUpper();
                if (strUpperArg.IndexOf("-D3D10") > -1)
                {
                    MFramework.Instance.Renderer.UseD3D10();
                    break;
                }
            }

            try
            {
                using (new EnableThemingInScope(true))
                {
                    Debug.WriteLine("MainForm 开始\n");
                    z mainForm = new z();
                    Debug.WriteLine("MainForm 结束\n");

                    // Register idle handler.
                    Application.Idle += new EventHandler(mainForm.OnIdle);

                    Application.Run(mainForm);
                }
            }
            catch (PluginLoadException)
            {}
            //finally
            {
                ServiceProvider.Instance.Shutdown();
                PluginManager.Shutdown();
                Invalidator.Shutdown();
                MFramework.Shutdown();
                ShutdownAssemblies();
            }

            m_instanceLock.ReleaseMutex();

            if (m_restartLock != null)
            {
                m_restartLock.ReleaseMutex();
            }
        }

        private static void ShutdownAssemblies()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in assemblies)
            {
                AssemblyUtilities.FindAndCallShutdownMethod(assembly);
            }
        }

        private void OnIdle(object sender, EventArgs e)
        {
            if (!m_bWindowPositionSet)
            {
                // This function must be called again here to ensure that the
                // window size matches the size stored in the settings. The
                // initialization of the third party docking code resizes the
                // window from the size set by the initial call to
                // LoadMainWindowPosition. The Boolean passed in here allows
                // the function to detect where it is being called from to
                // process special logic.
                LoadMainWindowPosition(true);
                m_bWindowPositionSet = true;
            }

            if (m_bProcessDelayedLoading)
            {
                m_bProcessDelayedLoading = false;
                DoDelayedStartupLoading();
            }
            if (this.Visible &&
                this.WindowState !=  FormWindowState.Minimized &&
                Form.ActiveForm == this)
            {
                Invalidator.Instance.Update();
            }
        }

        private string BuildTitleBarText(string fileName)
        {
            string title = string.Format("{0} - {1}", fileName, 
                MFramework.Instance.ApplicationName);
            if (MFramework.Instance.Scene.Dirty)
            {
                title = title + "*";
            }
            return title;
        }

        private WeifenLuo.WinFormsUI.DockPanel m_ctlDockPanel;
        private System.Windows.Forms.MainMenu m_menuMain;

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            ServiceProvider sp = ServiceProvider.Instance;

            IMessageService messageService = 
                sp.GetService(typeof(IMessageService)) as IMessageService;
            
            ISettingsService settingsService = 
                sp.GetService(typeof(ISettingsService))
                as ISettingsService;

            CommandPanelServiceImpl panelService =
                sp.GetService(typeof(ICommandPanelService))
                as CommandPanelServiceImpl;


            string dockConfig = settingsService.GetSettingsObject(
                dockConfigSettingName, SettingsCategory.PerUser) as string;

            bool panelLayoutLoaded = false;
            try
            {
                if (dockConfig != null)
                {
                    using (Stream s = new MemoryStream(
                               ASCIIEncoding.UTF8.GetBytes(dockConfig)))
                    {                        
                        panelService.LoadConfig(s);
                        panelLayoutLoaded = true;
                    }
                }
            }
            catch(Exception)
            {
                messageService.AddMessage(MessageChannelType.General, 
                    "Panel Layout not found or couldn't load.\n" +
                    "Default layout will be loaded");
                
            }
            if (!panelLayoutLoaded)
            {
                try
                {
                    using (Stream resourceStream = 
                               Assembly.GetExecutingAssembly()
                               .GetManifestResourceStream(
                               "Emergent.Gamebryo.SceneDesigner.GUI" +
                               ".DefaultDocking.config"))
                    {
                        panelService.LoadConfig(resourceStream);
                    }
                }    
                catch (InvalidOperationException)
                {
                    //Something was not right about the format...
                    messageService.AddMessage(MessageChannelType.General, 
                        "Panel Layout Failed to load.");

                }
            }

            AssociateFileTypes.Associate(".gsa", 
                Application.ExecutablePath, "GSA.Document",
                "Gamebryo Scene Ascii File", Application.StartupPath +
                @"\..\..\Data\GSADoc.ico");


            RenderForm renderForm = 
                panelService.GetPanel("Viewports") as RenderForm;

            Invalidator.Instance.RegisterControl(renderForm);
            MakeNewFile(false);
            m_idleCommand = m_uiCommandService.GetCommand("Idle");
            m_timer = new TimedInvoker();
            m_timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            m_timer.SychronizedObject = this;
            m_timer.CallBack = new EventHandler(TimerCallBack);
            m_timer.Start();

            string strURL = Environment.GetEnvironmentVariable("EGB_PATH");
            if (strURL != null)
                strURL = strURL + @"\Documentation\HTML\Gamebryo.chm";

            string strBaseSceneDesignerHelpPath = 
                @"/Tool_Manuals/Scene_Designer/";
            HelpManager.Instance.Init(strURL, strBaseSceneDesignerHelpPath);

            MFramework.Instance.EventManager.LongOperationStarted +=
                new MEventManager.__Delegate_LongOperationStarted(
                this.OnLongOperationStarted);
            MFramework.Instance.EventManager.LongOperationCompleted +=
                new MEventManager.__Delegate_LongOperationCompleted(
                this.OnLongOperationCompleted);

            //Note jwolfe 3/13/06
            //This seems to be necessary to make the main form's window
            //Show up in the task bar.
            //With out this call, the user must alt-tab back to the window
            //to get it to show up...

            NativeWin32.SetActiveWindow((IntPtr) null);
            NativeWin32.SetActiveWindow(this.Handle);

            IUICommandService commandService =
                sp.GetService(typeof(IUICommandService))
                as IUICommandService;
            //m_shortcutService.RegisterShortcut(Keys.Alt | Keys.W, commandService.GetCommand("ToggleViewports"));
        }

	    private void DoDelayedStartupLoading()
	    {
	        if (!CheckForAutosaveFiles())
	        {
	            string[] args = Environment.GetCommandLineArgs();
                for (int i = 1; i < args.Length; i++)
                {
                    string strArg = args[i];
                    if (!strArg.StartsWith("-"))
                    {
                        FileHandlerServiceImpl fileService = ServiceProvider
                            .Instance.GetService(typeof(IFileHandlerService))
                            as FileHandlerServiceImpl;
                        fileService.LoadFile(strArg);
                        break;
                    }
	            }
	        }
	    }
	    
	    private void SceneFileLoaderCallBack(object sender, string strFilename)
	    {
	                StringBuilder longPath = new StringBuilder(
	                    NativeWin32.MAX_PATH);
                    NativeWin32.GetLongPathName(strFilename, longPath,
                        longPath.Capacity);
	                string longFileName = longPath.ToString();
                    if (longFileName == null ||
                        longFileName.Equals(string.Empty))
                    {
                        return;
                    }

	                FileInfo fi = new FileInfo(longFileName);
                    if (fi.Extension != null &&
                        !fi.Extension.Equals(string.Empty))
                    {
                        string extension = fi.Extension.Substring(1).ToUpper();
                        LoadMainScene(longFileName, extension, false);
                    }
	        
	    }

	    private void OnLongOperationStarted()
        {
            this.Cursor = Cursors.WaitCursor;
        }

        private void OnLongOperationCompleted()
        {
            this.Cursor = Cursors.Default;
        }

        private void OnDragEnter(object sender, 
            System.Windows.Forms.DragEventArgs e)
        {
            Debug.WriteLine(new StackTrace().GetFrame(0).ToString());
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void OnDragDrop(object sender, 
            System.Windows.Forms.DragEventArgs e)
        {
            string[] filenames = e.Data.GetData(DataFormats.FileDrop) 
                as string[];
            if (filenames.Length > 1)
            {
                MessageBox.Show("Only one Scene file may be opened at a time."
                    ,"Can not open multiple files", MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }
            else
            {
                FileInfo fi = new FileInfo(filenames[0]);
                string extension = fi.Extension.Substring(1).ToUpper();
                if (extension.ToUpper().CompareTo("GSA") == 0 &&
                    !CheckBeforeLosingChanges())
                {                
                    return;
                }

                FileHandlerServiceImpl fileService =
                    ServiceProvider.Instance.GetService(
                    typeof(IFileHandlerService))
                    as FileHandlerServiceImpl;

                fileService.LoadFile(filenames[0]);

                LoadMainScene(filenames[0], extension, false);
            }
        }

        [UICommandHandler("Exit")]
        private void m_miExit_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Closing(object sender, 
            System.ComponentModel.CancelEventArgs e)
        {
            if (!CheckBeforeLosingChanges())
            {
                e.Cancel = true;
                return;
            }

            m_timer.Stop();

            ServiceProvider sp = ServiceProvider.Instance;
            ISettingsService settingsService = 
                sp.GetService(typeof(ISettingsService))
                as ISettingsService;
            CommandPanelServiceImpl panelService =
                sp.GetService(typeof(ICommandPanelService))
                as CommandPanelServiceImpl;

            if (!m_bClearWindowLayout)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    panelService.SaveConfig(ms);
                    Encoding.UTF8.GetString(ms.GetBuffer());
                    string dockConfig = 
                        Encoding.UTF8.GetString(ms.GetBuffer());
                    settingsService.SetSettingsObject(dockConfigSettingName, 
                        dockConfig, SettingsCategory.PerUser);
                }
            }
            else
            {
                settingsService.RemoveSettingsObject(dockConfigSettingName,
                    SettingsCategory.PerUser);
            }

            SaveMainWindowPosition();

            settingsService.SaveSettings(SettingsCategory.Global);
            settingsService.SaveSettings(SettingsCategory.PerUser);
           
            SavePalettes();
            DeleteAutoSave();

            // syz [11/24/2009] 保存 time stamp
            MTimeStamp.Instance.SaveToFile();
            Process.GetCurrentProcess().Kill();
        }


	    /// <summary>
        /// Makes sure the user has saved the scene if it has changed
        /// </summary>
        /// <returns>return value of false means the caller should cancle
        /// whatever it was about to do.</returns>
	    private bool CheckBeforeLosingChanges()
	    {
            if (!MFramework.Instance.Scene.Dirty)
            {
                return true;
            }
            DialogResult result = MessageBox.Show(
                "The current file has been modified.\nWould you like to " +
                "save before continuing?", "Save Changed File?",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            switch (result)
            {
                case System.Windows.Forms.DialogResult.Yes:
                    {
                        OnSave(this, null);
                        break;
                    }
                case System.Windows.Forms.DialogResult.No:
                    {
                        return true;
                    }
                case System.Windows.Forms.DialogResult.Cancel:
                    {
                        return false;
                    }
            }
            return true;
	    }

	    #region UICommand Hanlders
        [UICommandHandler("SaveFile")]
        private void OnSave(object sender, EventArgs args)
        {
            if (MFramework.Instance.CurrentFilename.Equals(string.Empty))
            {
                OnSaveFileAs(sender, args);
            }
            else
            {
                FileInfo fi = new FileInfo(
                    MFramework.Instance.CurrentFilename);
                SaveMainScene(MFramework.Instance.CurrentFilename, 
                    fi.Extension.Replace(".", "").ToUpper(), false);
            }
        }

        [UICommandHandler("SaveFileAs")]
        private void OnSaveFileAs(object sender, EventArgs args)
        {
            SaveFileDialog dlg = new SaveFileDialog();

            string[] streamingDescriptions = 
                MUtility.GetStreamingFormatDescriptions();
            string[] streamingExtensions =
                MUtility.GetStreamingFormatExtensions();

            int iFormats = streamingDescriptions.Length;
            Debug.Assert(iFormats > 0);
            dlg.Filter = streamingDescriptions[0];
            for (int i=1; i<iFormats; i++)
            {
                dlg.Filter = dlg.Filter + "|" + streamingDescriptions[i];
            }
            dlg.FileName = MFramework.Instance.CurrentFilename;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string fileName = dlg.FileName;
                string extension = streamingExtensions[dlg.FilterIndex-1];
                SaveMainScene(fileName, extension, false);
            }
        }

        [UICommandHandler("ImportTerrain")]
        private void OnImportTerrain(object sender, EventArgs args)
        {
            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);

            OpenFileDialog openFiledlg = new OpenFileDialog();
            openFiledlg.Filter = "trr files (*.etr)|*.etr|All files (*.*)|*.*";
            openFiledlg.FilterIndex = 1;
            openFiledlg.RestoreDirectory = true;

            if (openFiledlg.ShowDialog() == DialogResult.OK)
            {
                MFramework fw = MFramework.Instance;
                fw.Scene.ImportTerrain(openFiledlg.FileName);
                // 测试流载入地形
                //fw.Scene.ImportTerrainFromStream(openFiledlg.FileName);
/*
                // 附带读取场景数据
                string sceneName = openFiledlg.FileName;
                sceneName = sceneName.Remove( sceneName.Length - 4 );
                LoadMainScene( sceneName, "GSA", true );
*/
            }   
        }

        [UICommandHandler("ExportTerrain")]
        private void OnExportTerrain(object sender, EventArgs args)
        {
            SaveFileDialog saveFiledlg = new SaveFileDialog();
            saveFiledlg.Filter = "etr files (*.etr)|*.etr|All files (*.*)|*.*";
            saveFiledlg.FilterIndex = 1;
            saveFiledlg.RestoreDirectory = true;

            if (saveFiledlg.ShowDialog() == DialogResult.OK)
            {
                MFramework fw = MFramework.Instance;
                fw.Scene.ExportTerrain(saveFiledlg.FileName);
/*
                //附带保存场景数据
                string sceneName = saveFiledlg.FileName;
                sceneName = sceneName.Remove(sceneName.Length - 4);
                SaveMainScene(sceneName, "GSA", false);
 */ 
            }
        }

        [UICommandHandler("ExportNPCCreator")]
        private void OnExportNPCCreator(object sender, EventArgs args)
        {
            MFramework fw = MFramework.Instance;
            fw.Scene.ExportNPCCreator();

        }

        // 将场景导出为nif文件
        [UICommandHandler("ExportScene")]
        private void OnExportScene(object sender, EventArgs args)
        {
            MFramework fw = MFramework.Instance;

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = Application.ExecutablePath;
            dlg.Filter = "NIF 文件 (*.nif)|*.nif|All files (*.*)|*.*";
            dlg.FilterIndex = 1;
            dlg.RestoreDirectory = true;
            dlg.ShowDialog();

            if (dlg.FileName != "")
            {
                fw.Scene.ExportScene(dlg.FileName);
            }
        }

        // 导出场景碰撞信息
        [UICommandHandler("ExportCollisionData")]
        private void OnExportCollisionData(object sender, EventArgs args)
        {
            MFramework fw = MFramework.Instance;
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = Application.ExecutablePath;
            dlg.Filter = "碰撞信息 文件 (*.map)|*.map|All files (*.*)|*.*";
            dlg.FilterIndex = 1;
            dlg.RestoreDirectory = true;
            dlg.ShowDialog();

            if (dlg.FileName != "")
            {
                fw.Scene.ExportCollisionData(dlg.FileName,null);
            }
        }

        // 导出场景信息，场景草图给任务编辑器
        [UICommandHandler("ExportSceneToMissionEditor")]
        private void OnExportSceneToMissionEditor(object sender, EventArgs args)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.ShowDialog();
            MFramework.Instance.Scene.ExportTerrainInfoToMissionEditor(dlg.SelectedPath, true);
        }

        // 将场景完全导出。可导出到一个目录再对该目录打包
        [UICommandHandler("ExportAll")]
        private void OnExportAll(object sender, EventArgs args)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = Application.ExecutablePath;
            dlg.Filter = "场景文件 (*.sce) |*.sce|All files (*.*)|*.*";
            dlg.FilterIndex = 1;
            dlg.RestoreDirectory = true;
            dlg.ShowDialog();

            if (dlg.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs;
                try
                {
                    fs = (System.IO.FileStream)dlg.OpenFile();
                }
                catch (System.UnauthorizedAccessException e2)
                {
                    MessageBox.Show("被覆盖文件可能是只读的.导出失败.");
                    return;
                }
                System.IO.StreamWriter sw = new StreamWriter(fs);
                // 在这里写入场景文件内容
                sw.WriteLine("这是总场景文件，可以在这里写入一些场景描述信息。");
                sw.WriteLine("一下是场景物件简表：");
                sw.Close();
                fs.Close();

                // [5/21/2009 hemeng]
                // 将场景物件简表写入sce文件
                string strSceFilePath = dlg.FileName;
                
                // 创建目录
                int iLastDot = dlg.FileName.LastIndexOf('.');
                string strDirectory = dlg.FileName.Substring(0, iLastDot);
                string strMapID = strDirectory.Substring(strDirectory.LastIndexOf("\\") + 1);
                Directory.CreateDirectory(strDirectory);
                Directory.CreateDirectory(strDirectory+"/Terrain");
                string strSceneFile = strDirectory+"/scene.nif";
                string strTerrainFile = strDirectory + "/Terrain/terrain.etr";
                string strCollisionFile = strDirectory + "/terrainProperty.map";
                //string strNPCFile = strDirectory + "/NPC.xml";
                //string strGASFile = strDirectory + "/scene.gas";
                string strTerrainInfo = strDirectory + "/SceneInfo.xml";
                string strEntityInfo = strDirectory + "/EntityInfo.xml";
                String strCurrFilename = MFramework.Instance.CurrentFilename;
                iLastDot = strCurrFilename.LastIndexOf(".");
                string strSrcLightMapDir = strCurrFilename.Substring(0, iLastDot) + "\\LightMaps\\";
                String strDestLightMapDir = strDirectory + "/LightMaps/";

                if (!Directory.Exists(strDestLightMapDir))
                {
                    Directory.CreateDirectory(strDestLightMapDir);
                }

                MFramework fw = MFramework.Instance;
                //fw.Scene.ExportScene(strSceneFile);

                bool bResult = true; 
                // 导出时不用导出 GSA 文件
                //fw.SaveScene(strGASFile, "GSA");
                //if (!bResult)
                //{
                //    MessageBox.Show("场景GSA文件保存失败.");
                //}
                // [5/21/2009 hemeng]
                // 将场景物件简表写入sce文件
                bResult = fw.Scene.ExportSceneEntityList(strSceFilePath);
                if(!bResult)
                {
                    MessageBox.Show("场景物件简表导出失败!");
                }

                bResult = fw.Scene.ExportSceneEntity(strEntityInfo, strSrcLightMapDir);
                if (!bResult)
                {
                    MessageBox.Show("Entity 导出失败.");
                }
                bResult = fw.Scene.ExportTerrainInfo(strTerrainInfo, strMapID);
                if (!bResult)
                {
                    MessageBox.Show("地形信息导出失败.");
                }
                bResult = fw.Scene.ExportTerrain(strTerrainFile);
                if (!bResult)
                {
                    MessageBox.Show("地形导出失败.");
                }

                bResult = fw.Scene.ExportCollisionData(strCollisionFile, strMapID);
                if (!bResult)
                {
                    MessageBox.Show("地表属性(to server)导出失败.");
                }

                // 拷贝 LightMaps
                if (Directory.Exists(strSrcLightMapDir))
                {
                    String[] arrFiles = Directory.GetFiles(strSrcLightMapDir);
                    foreach (String strDDSFile in arrFiles)
                    {
                        if (Path.GetExtension(strDDSFile).ToLower().Equals(".dds")
                            && !strDDSFile.Contains("chunk_"))
                        {
                            String strPureFileName = Path.GetFileName(strDDSFile);
                            String strDestFileName = strDestLightMapDir + strPureFileName;
                            File.Copy(strDDSFile, strDestFileName);
                        }
                    }
                }

                MessageBox.Show("场景导出完成.地形已被优化过不能再被编辑. 请重新载入项目.");
               // fw.Scene.ExportTerrainTextureToDDS(strDirectory + "/TerrainTexture");
                //fw.Scene.ExportNPCCreator();
                MakeNewFile(true);
            }
        }
        [UICommandHandler("FastPreview")]
        private void OnFastPreview(object sender, EventArgs args)
        {
            // 创建目录
           // int iLastDot = dlg.FileName.LastIndexOf('.');
            string strDirectory = Application.StartupPath + "/Data/Model/TerrainScene/_Preview";//dlg.FileName.Substring(0, iLastDot);
            Directory.CreateDirectory(strDirectory);
            Directory.CreateDirectory(strDirectory + "/Terrain");
            string strSceneFile = strDirectory + "/scene.nif";
            string strTerrainFile = strDirectory + "/Terrain/terrain.etr";
            string strCollisionFile = strDirectory + "/terrainProperty.map";
            //string strNPCFile = strDirectory + "/NPC.xml";
            string strGASFile = strDirectory + "/scene.gas";
            string strTerrainInfo = strDirectory + "/SceneInfo.xml";
            MFramework fw = MFramework.Instance;
            //fw.Scene.ExportScene(strSceneFile);

            bool bResult = fw.SaveScene(strGASFile, "GSA");
            if (!bResult)
            {
                MessageBox.Show("场景GSA文件保存失败.");
            }
            bResult = fw.Scene.ExportTerrainInfo(strTerrainInfo,null);
            if (!bResult)
            {
                MessageBox.Show("地形信息导出失败.");
            }
            bResult = fw.Scene.ExportTerrain(strTerrainFile);
            if (!bResult)
            {
                MessageBox.Show("地形导出失败.");
            }
            bResult = fw.Scene.ExportCollisionData(strCollisionFile,null);
            if (!bResult)
            {
                MessageBox.Show("地表属性(to server)导出失败.");
            }

            MessageBox.Show("场景导出完成.地形已被优化过不能再被编辑. 请重新载入项目.");
            // fw.Scene.ExportTerrainTextureToDDS(strDirectory + "/TerrainTexture");
            //fw.Scene.ExportNPCCreator();
            MakeNewFile(true);

            System.Environment.CurrentDirectory = Application.StartupPath;

            // 将 _Preview 目录拷贝到 client 目录.
            Process p = new Process();
            p.StartInfo.FileName = "cmd.exe";
            //p.StartInfo.Arguments = "/c" command;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.Start();
            string strOutput = null;
            string strCommand = "xcopy  /D /F /I /R /Y /E .\\Data\\Model\\TerrainScene\\_Preview .\\..\\client\\Data\\Model\\TerrainScene\\_Preview";
            p.StandardInput.WriteLine(strCommand);
            p.StandardInput.WriteLine("exit");
            strOutput = p.StandardOutput.ReadToEnd();
            Console.WriteLine(strOutput);
            p.Close();

            p.Start();
            int iInitX = (int)MFramework.Instance.GetCurrentX();
            int iInitY = (int)MFramework.Instance.GetCurrentY();

            strCommand = "..\\client\\MuxS.exe -NoUseUI -InitX " + iInitX + " -InitY " + iInitY;
            p.StandardInput.WriteLine(strCommand);
            p.StandardInput.WriteLine("exit");
            strOutput = p.StandardOutput.ReadToEnd();
            Console.WriteLine(strOutput);

            p.Close();
        }
        // 最大化当前
        //[UICommandHandler("ToggleViewports")]
        //private void OnToggleViewports(object sender, EventArgs args)
        //{
        //   // ToggleViewports
        //    MFramework.Instance.ViewportManager.ToggleViewports(null, null);
        //}

        private bool SaveUndoCommands(string filePath,string extension)
        {
            // save redo commands
            if(filePath == null || extension == null)
            {
                return false;
            }

            string strTemp = "UndoCommands" + DateTime.Now.ToLongTimeString();
            strTemp = strTemp.Replace(':', '_');
            string strFileName = filePath + strTemp + extension;
            FileInfo Undofi = new FileInfo(strFileName);

            // check if file is exist
            if (Undofi.Exists)
            {
                bool bReadonly = (Undofi.Attributes & FileAttributes.ReadOnly)
                    == FileAttributes.ReadOnly;
                // if is read only return false
                if (bReadonly)
                {
                    bool bOverwrite =
                        MessageBox.Show(Undofi.FullName + Environment.NewLine +
                        "    is read only. Would you like to over write it?",
                        "Can not save file", MessageBoxButtons.YesNo)
                        == System.Windows.Forms.DialogResult.Yes;
                    if (!bOverwrite)
                        return false;                   
                }
                // else delete old file
                File.Delete(strFileName);
            }
          
            // create new file
            FileStream fs = File.Create(strFileName);

            ICommandService iServer = ServiceProvider.Instance
                        .GetService(typeof(ICommandService)) as
                        ICommandService;
            Debug.Assert(iServer != null,
                "Command service not found!");
            ICommand[] pArrRedoCommands = iServer.GetUndoCommands();
            foreach (ICommand command in pArrRedoCommands)
            {
                string commandName = command.Name + "\n";
                Byte[] info = new UTF8Encoding(true).GetBytes(commandName);
                fs.Write(info, 0, info.Length);
            }

            fs.Close();

            // clear undo and redo commands stack
            iServer.ClearUndoAndRedoStacks();

            return true;
        }

	    private void SaveMainScene(string fileName, string extension,
            bool bAutosave)
	    {
            MFramework fw = MFramework.Instance;
	        FileInfo fi = new FileInfo(fileName);
            if (fi.Exists)
            {
                bool bReadonly = (fi.Attributes & FileAttributes.ReadOnly)
                    == FileAttributes.ReadOnly;
                if (bReadonly)
                {
                    bool bOverwrite = 
                        MessageBox.Show(fi.FullName + Environment.NewLine +
                        "    is read only. Would you like to over write it?",
                        "Can not save file", MessageBoxButtons.YesNo)
                        == System.Windows.Forms.DialogResult.Yes;
                    if (!bOverwrite)
                        return;
                    fi.Attributes = fi.Attributes & (~FileAttributes.ReadOnly);
                }
            }
            SceneManagementUtilities.PutSceneItemsIntoSettings(fw.Scene);
	        bool fileSaved = fw.SaveScene(fileName, extension);
	        if (!fileSaved)
	        {
	            MessageBox.Show("Failed to save scene");
	        }
	        else
	        {
                // 创建场景文件同名目录,将 tree view 及 地形保存在目录中
                int iLastDot = fileName.LastIndexOf('.');
                string strDirectory = fileName.Substring(0, iLastDot);
                Directory.CreateDirectory(strDirectory);
                fw.Scene.SaveTerrain(strDirectory + "\\terrain.trr"); // 导出地形

                // 保存 SceneViewerPanel 中的 tree view
                CommandPanelServiceImpl panelService =
                    ServiceProvider.Instance.GetService(typeof(ICommandPanelService)) as CommandPanelServiceImpl;
                SceneViewerPanel sceneViewer =
                    panelService.GetPanel("Tree View") as SceneViewerPanel;
                sceneViewer.SaveTreeViewToXML(strDirectory + "\\TreeView.xml");

                // 保存 Fog 参数
                MGlobalSetting.Instance.SaveToXml(strDirectory + "\\GlobalSetting.xml");

                // 保存Region信息
                fw.Scene.SaveRegionToXml(strDirectory + "\\RegionInfo.xml");

                // 保存 Entity Export Settings
                MEntityExportManager.Instance.SaveToXml(strDirectory + "\\");

                // add [6/10/2009 hemeng]
                // 保存Undo命令，用于查找BUG
                string strUndoFilePath = fileName.Substring(0, fileName.LastIndexOf(".")) + "\\";
                if (!SaveUndoCommands(strUndoFilePath, ".dat"))
                {
                    MessageBox.Show("Fail to save UndoCommands to file", "Error", 0);
                }
                // end add [6/10/2009 hemeng]

	            ServiceProvider sp = ServiceProvider.Instance;
	            ISettingsService settingsService = 
	                sp.GetService(typeof(ISettingsService))
	                    as ISettingsService;
	            settingsService.ScenePath = fi.DirectoryName;
	            settingsService.SceneFileName = fi.Name 
	                + ".scene.settings";
                SceneManagementUtilities.ConvertScenePalettePathToRelative(
                    fi.DirectoryName);
	            settingsService.SaveSettings(SettingsCategory.PerScene); 
                SceneManagementUtilities.ConvertScenePalettePathToAbsolute(
                    fi.DirectoryName);
                
                if (!bAutosave)
                {
                    MFramework.Instance.CurrentFilename = fileName;
                    DirtyBitUtilities.MakeSceneClean(
                        MFramework.Instance.Scene);
                    SavePalettes();
	                this.Text = BuildTitleBarText(fi.Name);
                    MRUManager.AddFile(fileName);
                }
	                                      
	        }
	    }

        [UICommandHandler("MRULoad")]
        [RequiresParameter("Filename")]
        private void OnMRULoad(object sender, EventArgs args)
        {
            UICommandEventArgs commandArgs = args as UICommandEventArgs;
            if (!CheckBeforeLosingChanges())
            {                
                return;
            }
            UICommand mruSender = sender as UICommand;
            string fileName = commandArgs.GetValue("Filename");
            FileInfo fi = new FileInfo(fileName);
            string extension = fi.Extension.Substring(1).ToUpper();
            LoadMainScene(fileName, extension, false);
        }

        // begin [7/22/2009]
        [UICommandHandler("OpenAndClearLightList")]
        private void OnOpenAndClearLightList(object sender, EventArgs args)
        {
            m_bClearLightAffectLists = true;
            OnOpenFile(sender, args);
        }
        // end [7/22/2009]

	    [UICommandHandler("OpenFile")]
        private void OnOpenFile(object sender, EventArgs args)
        {
            if (!CheckBeforeLosingChanges())
            {                
                return;
            }
            OpenFileDialog dlg = new OpenFileDialog();

            string[] streamingDescriptions = 
                MUtility.GetStreamingFormatDescriptions();
            string[] streamingExtensions =
                MUtility.GetStreamingFormatExtensions();

            int iFormats = streamingDescriptions.Length;
            Debug.Assert(iFormats > 0);
            dlg.Filter = streamingDescriptions[0];
            for (int i=1; i<iFormats; i++)
            {
                dlg.Filter = dlg.Filter + "|" + streamingDescriptions[i];
            }
            dlg.InitialDirectory = MFramework.Instance.CurrentFilename;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                IMessageService messageService = 
                    ServiceProvider.Instance.GetService(
                    typeof(IMessageService)) as IMessageService;
                messageService.ClearMessages(MessageChannelType.Conflicts);
                string sceneFilename = dlg.FileName;
                string extension = streamingExtensions[dlg.FilterIndex-1];
                LoadMainScene(sceneFilename, extension, false);
            }
            
        }

	    private void LoadMainScene(string sceneFilename, string extension,
            bool bRecoveryFile)
	    {
            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);

            // 添加检查场景目录位置功能 [12/2/2009 hemeng]
            if(!sceneFilename.ToLower().Contains(Application.StartupPath.ToLower()))
            {
                MessageBox.Show("打开场景不是当前工作目录下资源，可能会引起不确定错误!");
            }

	        MFramework fw = MFramework.Instance;
	        MScene pmScene = fw.LoadScene(sceneFilename, extension);
	        if (pmScene == null)
	        {
	            MessageBox.Show("Failed to load the following file:\n\n" +
                    sceneFilename + "\n\nCheck the Messages Panel for " +
                    "more information.", "Error Loading Scene Designer File",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
	        }
	        else
	        {


	            ServiceProvider sp = ServiceProvider.Instance;
	            ISettingsService settingsService = 
	                sp.GetService(typeof(ISettingsService))
	                    as ISettingsService;
	            FileInfo fi = new FileInfo(sceneFilename);
	            settingsService.ScenePath = fi.DirectoryName;
	            settingsService.SceneFileName = fi.Name + 
	                ".scene.settings";
                SceneManagementUtilities.ClearSceneSettings();
	            settingsService.LoadSettings(SettingsCategory.PerScene);

                // begin [7/22/2009 hemeng]
                fw.InitNewScene(pmScene,m_bClearLightAffectLists);
                m_bClearLightAffectLists = false;
                // end [7/22/2009 hemeng]

                SceneManagementUtilities.PutSettingsIntoSceneItems(
	                fw.Scene);

                SceneManagementUtilities.ConvertScenePalettePathToAbsolute(
                    fi.DirectoryName);
                SceneManagementUtilities.CopySceneSettingsToUserSettings(
                    );

                SceneManagementUtilities.ResetPalettes();

               // fw.Scene.Debug_CheckEntitys();  // debug

	            MPalette oldOrphan = fw.PaletteManager.GetPaletteByName(
                    "Unsaved Scene Templates");
	            if (oldOrphan != null)
	            {
	                fw.PaletteManager.RemovePalette(oldOrphan);
	            }
                bool bConflictsFound = false;

               // bConflictsFound = SceneManagementUtilities.CheckMainSceneForConflicts();  

               // fw.Scene.Debug_CheckEntitys();  // debug
	            ComponentManagementUtilities.BuildComponentListFromScene(
	                fw.Scene);
	            MPalette orphans = 
	                EntityManagementUtilities.ResolveMasterEntities(
	                    fw.Scene, fw.PaletteManager.GetPalettes());

	            orphans.Name = "Unsaved Scene Templates";
	            orphans.DontSave = true;
                bool bAddedToManager = SceneManagementUtilities
                    .AddOrphansToPalettes(orphans);

                DirtyBitUtilities.MakeSceneClean(orphans.Scene);
                if (!bRecoveryFile)
                {
                    if (!bConflictsFound)
                        DirtyBitUtilities.MakeSceneClean(
                        MFramework.Instance.Scene); 
                    this.Text = BuildTitleBarText(fi.Name);
                    MFramework.Instance.CurrentFilename = sceneFilename;
                    MRUManager.AddFile(MFramework.Instance.CurrentFilename);
                }
                if (!bAddedToManager)
                {
                    orphans.Dispose();
                }

                // 导入地形
                int iLastDot = sceneFilename.LastIndexOf('.');
                string strDirectory = sceneFilename.Substring(0, iLastDot);
                fw.Scene.LoadTerrain(strDirectory + "\\terrain.trr"); // 导入地形

                MCityLevelManager.Instance.LoadFromXml(strDirectory + "\\" + MCityLevelManager.CITY_LEVEL_SAVING_FILE);

                // 导入 treeview
                CommandPanelServiceImpl panelService =
                    ServiceProvider.Instance.GetService(typeof(ICommandPanelService)) as CommandPanelServiceImpl;
                SceneViewerPanel sceneViewer =
                    panelService.GetPanel("Tree View") as SceneViewerPanel;
                sceneViewer.LoadTreeViewFromXML(strDirectory + "\\TreeView.xml");

                // 导入 fog
                MGlobalSetting.Instance.LoadFromXml(strDirectory + "\\GlobalSetting.xml");
               // fw.Scene.Debug_CheckEntitys();

                // 加载 Entity Export Settings
                MEntityExportManager.Instance.LoadFromXml(strDirectory + "\\");                
                
                // 刷新地形 ambient 系数
                MTerrain.Instance.TerrainAmbient = MGlobalSetting.Instance.TerrainAmbient;

	        }
	    }


	    [UICommandHandler("NewFile")]
        private void OnNewFile(object sender, EventArgs args)
        {
            if (!CheckBeforeLosingChanges())
            {                
                return;
            }
            IMessageService messageService = 
                ServiceProvider.Instance.GetService(
                typeof(IMessageService)) as IMessageService;
            messageService.ClearMessages(MessageChannelType.Conflicts);
            MakeNewFile(true);
        }

        // add [6/8/2009 hemeng]
        // copy palettes
        private void CopyScenePalettetsToCurrentPath()
        {
            // add [6/8/2009 hemeng]
            // copy palettes
            string strRootPath = System.IO.Directory.GetCurrentDirectory();
            string palettesPath = strRootPath + "\\AppData\\Palettes\\";
            if(System.IO.Directory.Exists(palettesPath))
            {
                string strSceneDir = strRootPath + "\\Scene";
                if(System.IO.Directory.Exists(strSceneDir))
                {
                    string[] strSceneFiles = System.IO.Directory.GetDirectories(strSceneDir);
                    foreach(string sceneFileDir in strSceneFiles)
                    {
                        string strPaletteName = sceneFileDir.Substring(sceneFileDir.LastIndexOf('\\') + 1) + ".pal";
                        string strPalettePath = sceneFileDir + "\\" + strPaletteName;
                        if(System.IO.File.Exists(strPalettePath))
                        {
                            string strCopyPath = palettesPath + strPaletteName;
                            System.IO.File.Copy(strPalettePath, strCopyPath,true);
                        }

                    }
                }
                
            }
        }

        // copy scene palette to palette directory
        // strSceneName - 场景文件gsa路径，strPaletteName 要拷贝的palette名称
        private void CopyCurrentPaletteToDirectory(string strSceneName,string strPaletteName)
        {
            strPaletteName += ".pal";
            string palettesDir = System.IO.Directory.GetCurrentDirectory() + "\\AppData\\Palettes\\";
            if(System.IO.Directory.Exists(palettesDir))
            {
                if(System.IO.File.Exists(palettesDir + "\\" + strPaletteName))
                {
                    string strScenePath = strSceneName.Substring(0,strSceneName.LastIndexOf('\\'));
                    string strSceneFileName = System.IO.Path.GetFileNameWithoutExtension(strSceneName);
                    string strCopyPath = strScenePath + "\\" + strSceneFileName + "\\" + strPaletteName;
                    if(System.IO.File.Exists(strCopyPath))
                    {
                        System.IO.File.Delete(strCopyPath);
                    }
                    System.IO.File.Copy(palettesDir + "\\" + strPaletteName, strCopyPath,true);
                }
            }
        }

        // end add [6/8/2009 hemeng]

        private void MakeNewFile(bool bAskForPalettePath)
        {
            MScene pmScene = MFramework.Instance.NewScene(0);
            MFramework.Instance.InitNewScene(pmScene,false);
            string newFilePalettePath = null;
            if (bAskForPalettePath)
            {
                newFilePalettePath = AskUserForPalettePath();
            }
            ISettingsService settingService =
                ServiceProvider.Instance.GetService(typeof(ISettingsService))
                as ISettingsService;
            settingService.ScenePath = string.Empty;
            if (newFilePalettePath != null)
            {
                settingService.SetSettingsObject("PaletteFolder", 
                    new MFolderLocation(newFilePalettePath),
                    SettingsCategory.PerUser);
            }
            SceneManagementUtilities.ClearSceneSettings();

            // 等待功能验收 [6/9/2009 hemeng]
            //CopyScenePalettetsToCurrentPath();

            SceneManagementUtilities.ResetPalettes();
            ISelectionService pmSelectionService =
                ServiceProvider.Instance.GetService(
                typeof(ISelectionService)) as ISelectionService;
            pmSelectionService.ClearSelectedEntities();

            DirtyBitUtilities.MakeSceneClean(MFramework.Instance.Scene);
            this.Text = BuildTitleBarText("Untitled");
            MFramework.Instance.CurrentFilename = string.Empty;

            // 清理 tree view panel
            CommandPanelServiceImpl panelService =
            ServiceProvider.Instance.GetService(typeof(ICommandPanelService)) as CommandPanelServiceImpl;
            SceneViewerPanel sceneViewer =
                panelService.GetPanel("Tree View") as SceneViewerPanel;
            sceneViewer.ClearAllNodes();

            // 清理 fog
            MGlobalSetting.Instance.Clear();
        }

	    private string AskUserForPalettePath()
	    {
            // Retrieve setting for whether or not to prompt the user for
            // a palette path.
            ISettingsService pmSettingsService = ServiceProvider.Instance
                .GetService(typeof(ISettingsService)) as ISettingsService;
            Debug.Assert(pmSettingsService != null,
                "Settings service not found!");
            object pmDisplayNewSceneFormObject = pmSettingsService
                .GetSettingsObject(ms_strDisplayNewSceneFormSettingName,
                SettingsCategory.PerUser);
            Debug.Assert(pmDisplayNewSceneFormObject != null, "\"" +
                ms_strDisplayNewSceneFormSettingName + "\" setting not " +
                "found!");
            bool bDisplayNewSceneForm = (bool) pmDisplayNewSceneFormObject;

            string strPalettePath = null;
            if (bDisplayNewSceneForm)
            {
                NewSceneForm pmNewSceneForm = new NewSceneForm();
                pmNewSceneForm.ShowDialog();
                strPalettePath = pmNewSceneForm.PalettePath;
            }

            return strPalettePath;
	    }

	    [UICommandHandler("SaveDefaultConfig")]
        private void SaveConfigToFile(object sender, EventArgs args)
        {
            using (FileStream fs = new FileStream("DefaultDocking.config",
                       FileMode.Create))
            {
                CommandPanelServiceImpl panelService =
                    ServiceProvider.Instance.
                    GetService(typeof(ICommandPanelService))
                    as CommandPanelServiceImpl;
                panelService.SaveConfig(fs);
            }
        }

        [UICommandHandler("ResetWindowLayout")]
        private void OnResetWindowLayout(object sender, EventArgs args)
        {
            DialogResult result = MessageBox.Show(
                "Would you like to reset the window layout?\n" +
                string.Format(
                "The default layout will be restored next time {0} "+
                "is launched.", MFramework.Instance.ApplicationName),
                "Reset Window Layout?", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            m_bClearWindowLayout = (result == DialogResult.Yes);
        }

        [UICommandHandler("Options")]
        private void OnOptions(object sender, EventArgs args)
        {
            
            OptionsDlg dlg = new OptionsDlg();
            dlg.SetCategories( new SettingsCategory[]
                {
                        SettingsCategory.Global, SettingsCategory.PerUser, 
                    SettingsCategory.PerScene, SettingsCategory.Temp
                });
            dlg.Text = "Options";
            dlg.ShowDialog();
        }

        [UICommandHandler("ClearMRUList")]
        private void OnClearMRUList(object sender, EventArgs args)
        {
            MRUManager.Clear();
        }

        [UICommandHandler("Restart")]
        private void OnRestart(object sender, EventArgs args)
        {
            string path = Assembly.GetEntryAssembly().Location;
            string command = path;
            Mutex mutex = new Mutex(true, "SceneDesignerRestart");
            mutex.WaitOne();
            ProcessStartInfo psi = new ProcessStartInfo(command);
            psi.WorkingDirectory = Path.GetDirectoryName(command);
            System.Diagnostics.Process.Start(psi);
            m_restartLock = mutex;
            this.Close();
        }

        [UICommandHandler("RenderModeStandardPlusWireframe")]
        private void OnRenderModeStandardPlusWireframe(object sender, EventArgs args)
        {
            ServiceProvider sp = ServiceProvider.Instance;

            CommandPanelServiceImpl panelService =
                    sp.GetService(typeof(ICommandPanelService))
                    as CommandPanelServiceImpl;

            RenderForm renderForm =
                panelService.GetPanel("Viewports") as RenderForm;
            renderForm.SetRenderMode("Standard + Wireframe (Unshaded)");
            //renderForm.
        }

        [UICommandHandler("RenderModeWireframe")]
        private void OnRenderModeWireframe(object sender, EventArgs args)
        {
            ServiceProvider sp = ServiceProvider.Instance;

            CommandPanelServiceImpl panelService =
                    sp.GetService(typeof(ICommandPanelService))
                    as CommandPanelServiceImpl;

            RenderForm renderForm =
                panelService.GetPanel("Viewports") as RenderForm;
            renderForm.SetRenderMode("Wireframe (Unshaded)");
            //renderForm.
        }

        [UICommandHandler("RenderModeStand")]
        private void OnRenderModeStand(object sender, EventArgs args)
        {
            ServiceProvider sp = ServiceProvider.Instance;

            CommandPanelServiceImpl panelService =
                    sp.GetService(typeof(ICommandPanelService))
                    as CommandPanelServiceImpl;

            RenderForm renderForm =
                panelService.GetPanel("Viewports") as RenderForm;
            renderForm.SetRenderMode("Standard");
        }

        [UICommandHandler("RenderSceneObject")]
        private void OnRenderSceneObject(object sender, EventArgs args)
        {
            MFramework.Instance.Scene.SetRenderSceneObject(!MFramework.Instance.Scene.GetRenderSceneObject());
        }      

        [UICommandHandler("RenderNPCCreator")]
        private void OnRenderNPCCreator(object sender, EventArgs args)
        {
            MFramework.Instance.Scene.SetRenderNPCCreator(!MFramework.Instance.Scene.GetRenderNPCCreator());

        }

        [UICommandHandler("ShowSceneMetric")]
        private void OnShowSceneMetric(object sender, EventArgs args)
        {
            //bool bShow = MFramework.Instance.Renderer.ShowVisualTracker;
            //MFramework.Instance.Renderer.ShowVisualTracker = !bShow;
            MFramework.Instance.Renderer.ChangeVisualTracker();

        }

        [UICommandHandler("BakeCollisionBlock")]
        private void OnBakeCollisionBlock(object sender, EventArgs args)
        {
            MFramework.Instance.Scene.BakeObjectsToCollisionBlock();
        }
        [UICommandHandler("TerrainOperationFlat")]
        private void TerrainOperationFlat(object sender, EventArgs args)
        {
            MFramework.Instance.Scene.SetTerrainOperation(0);
        }

        [UICommandHandler("TerrainOperationUp")]
        private void TerrainOperationUp(object sender, EventArgs args)
        {
            MFramework.Instance.Scene.SetTerrainOperation(1);
        }

        [UICommandHandler("TerrainOperationFall")]
        private void TerrainOperationFall(object sender, EventArgs args)
        {
            MFramework.Instance.Scene.SetTerrainOperation(2);
        }

        [UICommandHandler("TerrainOperationSmooth")]
        private void TerrainOperationSmooth(object sender, EventArgs args)
        {
            MFramework.Instance.Scene.SetTerrainOperation(3);
        }

        //[UICommandHandler("ZoomExtendsSelection")]
        //private void ZoomExtendsSelection(object sender, EventArgs args)
        //{
        //    ServiceProvider sp = ServiceProvider.Instance;
        //    IUICommandService uiCommandService =
        //        sp.GetService(typeof(IUICommandService)) as IUICommandService;

        //    UICommand zoomCommand =
        //        uiCommandService.GetCommand("MoveToSelection");
        //    UIState state = new UIState();
        //    zoomCommand.ValidateCommand(state);
        //    if (state.Enabled)
        //    {
        //        zoomCommand.DoClick(this, null);
        //    }
        //}

        //[UICommandHandler("ZoomExtendsAll")]
        //private void ZoomExtendsAll(object sender, EventArgs args)
        //{
           
        //}
        #endregion

    
        protected override bool ProcessCmdKey(
            ref System.Windows.Forms.Message msg, Keys keyData)
        {
            if (!base.ProcessCmdKey (ref msg, keyData))
            {
                return ShortcutService.HandleKey(keyData);
            }
            else
            {
                return true;
            }
        }

        private void m_timerComponent_Tick(object sender, System.EventArgs e)
        {
            //Very lazy execution, since we dont really need this to update
            //we lightning speed, a 1000 ms timer should suffice
            bool bMainSceneDirty = MFramework.Instance.Scene.Dirty;
            CheckAutosaveInterval(bMainSceneDirty);
            if (bMainSceneDirty != m_lastDirtyStatus)
            {                                
                if (bMainSceneDirty)
                {
                    if (this.Text.IndexOf("*") == -1)
                    {
                        this.Text = this.Text + "*";
                    }
                }
                else
                {
                    this.Text = this.Text.Replace("*", "");
                }
                m_lastDirtyStatus = bMainSceneDirty;
            }
        }

        #region Autosaving Methods

	    private void CheckAutosaveInterval(bool dirty)
	    {
            if (dirty)
            {
                ISettingsService settingsSvc = 
                    ServiceProvider.Instance.GetService(
                    typeof(ISettingsService)) as ISettingsService;
                int intervalMinutes = (int) 
                    settingsSvc.GetSettingsObject("AutoSaveIntervalInMinutes",
                    SettingsCategory.PerUser);
                if (intervalMinutes <= 0)
                    return;
                TimeSpan interval = new TimeSpan(0, intervalMinutes, 0);

                if ((DateTime.Now - m_timeOfLastAutoSave) > interval)
                {
                    DoAutosave();
                    m_timeOfLastAutoSave = DateTime.Now;
                }
            }
            else
            {
                m_timeOfLastAutoSave = DateTime.Now;
            }
	    }

	    private void DoAutosave()
	    {
            string autosaveFilename = GetAutosavePath();
            SaveMainScene(autosaveFilename, "GSA", true);
	    }

        private void DeleteAutoSave()
        {
            string autosaveFilename = GetAutosavePath();
            FileInfo fi = new FileInfo(autosaveFilename);
            if (fi.Exists)
            {
                fi.Delete();
            }
            //Now, delete the settings file
            fi = new FileInfo(autosaveFilename + ".scene.settings");
            if (fi.Exists)
            {
                fi.Delete();
            }
        }

        private bool CheckForAutosaveFiles()
        {
            MFramework fw = MFramework.Instance;
            string autosaveFilename = GetAutosavePath();
            FileInfo fi = new FileInfo(autosaveFilename);
            if (fi.Exists)
            {
                string message = fw.ApplicationName +  
                    " was not shut down properly.\nWould you like to " +
                    "recover the unsaved file?";
                DialogResult result = MessageBox.Show(message, 
                    "Recover File?", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    LoadMainScene(autosaveFilename, "GSA", true);
                    return true;
                }
            }
            return false;
            
        }

        private string GetAutosavePath()
        {
            //string folderPath = Environment.GetFolderPath(
            //    Environment.SpecialFolder.LocalApplicationData) +
            //    @"\Emergent Game Technologies\" + 
            //    MFramework.Instance.ApplicationName;
            string folderPath = Application.StartupPath + @"\_tmp\";
            DirectoryInfo di = new DirectoryInfo(folderPath);
            if (!di.Exists)
            {
                di.Create();
            }
            return folderPath + MFramework.Instance.ApplicationName + "_" + 
                System.DateTime.Parse(Convert.ToString(System.DateTime.Now)).ToString("hh_mm_ss") + ".gsa";

        }


        #endregion


    }
}
