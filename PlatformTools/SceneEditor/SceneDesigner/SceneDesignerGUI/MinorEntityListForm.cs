﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using PluginAPI = Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;

namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    public partial class MinorEntityListForm : Form
    {
        public MinorEntityListForm()
        {
            InitializeComponent();
            MonorEntityListForm_Load();
        }

        private void MonorEntityListForm_Load()
        {
            m_listBoxAllEntities.Items.Clear();
            m_listBoxMidEntities.Items.Clear();

            // 填充物件列表
            MEntity[] aEntities = MFramework.Instance.Scene.GetEntities();
            foreach (MEntity entity in aEntities)
            {
                if(MLightManager.EntityIsLight(entity))
                {
                    continue;
                }
                m_listBoxAllEntities.Items.Add(entity.Name);
            }

            ArrayList midList = MGlobalSetting.Instance.MinorEntityList[0];
            foreach (String strEntity in midList)
            {
                m_listBoxMidEntities.Items.Add(strEntity);
                // 从0级中剔除 [7/9/2009 hemeng]
                m_listBoxAllEntities.Items.Remove(strEntity);
            }

            ArrayList lowList = MGlobalSetting.Instance.MinorEntityList[1];
            foreach (String strEntity in lowList)
            {
                m_listBoxLowEntities.Items.Add(strEntity);

                // 从0级中剔除 [7/9/2009 hemeng]
                m_listBoxAllEntities.Items.Remove(strEntity);
            }
        }

        private void RefreshMinorList()
        {
            MGlobalSetting.Instance.MinorEntityList[0].Clear();
            MGlobalSetting.Instance.MinorEntityList[1].Clear();

            foreach (String strEntity in m_listBoxMidEntities.Items)
            {
                MGlobalSetting.Instance.MinorEntityList[0].Add(strEntity);
            }
            foreach (String strEntity in m_listBoxLowEntities.Items)
            {
                MGlobalSetting.Instance.MinorEntityList[1].Add(strEntity);
            }

        }

         private void MoveMinorEntity(ListBox srcList, ListBox desList)
        {
            ArrayList removeList = new ArrayList();

            foreach (String strEntity in srcList.SelectedItems)
            {
                removeList.Add(strEntity);
            }
                      
            foreach (String strEntity in removeList)
            {
                if(!desList.Items.Contains(strEntity))
                {
                    desList.Items.Add(strEntity);
                }
                if(srcList.Items.Contains(strEntity))
                {
                    srcList.Items.Remove(strEntity);
                }
            }
             
            RefreshMinorList();
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
           contextMenuStrip1.Show(this, m_btnAdd.Left, m_btnAdd.Top);
        }

        // level 1 to level 0 [7/9/2009 hemeng]
        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            MoveMinorEntity(m_listBoxMidEntities, m_listBoxAllEntities);
        }

        // level 0 to level 1 [7/9/2009 hemeng]
        private void level1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoveMinorEntity(m_listBoxAllEntities, m_listBoxMidEntities);            
        }

        // level 0 to level 2
        private void level2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoveMinorEntity(m_listBoxAllEntities, m_listBoxLowEntities);
        }

        private void m_btnRemoveLowEntity_Click(object sender, EventArgs e)
        {
           contextMenuStrip2.Show(this, m_btnRemoveLowEntity.Left, m_btnRemoveLowEntity.Top);
        }

        // level 2 to level 0
        private void levelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoveMinorEntity(m_listBoxLowEntities, m_listBoxAllEntities);
        }

        // level 2 to level 1 [7/9/2009 hemeng]
        private void level1ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MoveMinorEntity(m_listBoxLowEntities, m_listBoxMidEntities);
        }

        // level 1 to level 2 [7/9/2009 hemeng]
        private void m_btnLower_Click(object sender, EventArgs e)
        {
            MoveMinorEntity(m_listBoxMidEntities, m_listBoxLowEntities);
        }

        private void checkBox_ShowTerrainProperty_CheckedChanged(object sender, EventArgs e)
        {
            bool bShow = checkBox_ShowTerrainProperty.Checked;

            MFramework.Instance.Scene.ShowCollisionData(bShow);
           
        }

        #region Service Accessors
        private static ISelectionService ms_pmSelectionService = null;
        private static ISelectionService SelectionService
        {
            get
            {
                if (ms_pmSelectionService == null)
                {
                    ms_pmSelectionService = MFramework.SelectionService;
                    if (ms_pmSelectionService == null)
                    {
                        MessageBox.Show("Selection service not found!");
                    }
                }
                return ms_pmSelectionService;
            }
        }

        #endregion

        private void ZoomExtents()
        {
            ServiceProvider sp = ServiceProvider.Instance;
            IUICommandService uiCommandService =
                sp.GetService(typeof(IUICommandService)) as IUICommandService;

            UICommand zoomCommand =
                uiCommandService.GetCommand("MoveToSelection");
            UIState state = new UIState();
            zoomCommand.ValidateCommand(state);
            if (state.Enabled)
            {
                zoomCommand.DoClick(this, null);
            }
        }

        private void SelectEntityInScene(ListBox list)
        {
            SelectionService.ClearSelectedEntities();

            foreach (String strEntity in list.SelectedItems)
            {
                MEntity pmEntity = MFramework.Instance.Scene.GetEntityByName(strEntity);
                if (pmEntity != null)
                {
                    SelectionService.AddEntityToSelection(pmEntity);
                }
            }           

            ZoomExtents();
        }

        private void m_listBoxAllEntities_DoubleClick(object sender, EventArgs e)
        {

            SelectEntityInScene(m_listBoxAllEntities);
            
        }

        // 拣选物件 [7/9/2009 hemeng]
        private void btn_GetSelectEntity_Click(object sender, EventArgs e)
        {
            m_listBoxAllEntities.SelectedItems.Clear();
            m_listBoxMidEntities.SelectedItems.Clear();
            m_listBoxLowEntities.SelectedItems.Clear();

            MEntity[] aEntities = SelectionService.GetSelectedEntities();
            if (aEntities.Length == 0)
            {
                return;
            }

            ArrayList selEntityNames = new ArrayList();
            foreach(MEntity pmEntity in aEntities)
            {
                selEntityNames.Add(pmEntity.Name);
            }

            foreach(String strEntity in selEntityNames)
            {
                if(m_listBoxAllEntities.Items.Contains(strEntity))
                {
                    m_listBoxAllEntities.SelectedItems.Add(strEntity);
                }
                else if(m_listBoxMidEntities.Items.Contains(strEntity))
                {
                    m_listBoxMidEntities.SelectedItems.Add(strEntity);
                }
                else if(m_listBoxLowEntities.Items.Contains(strEntity))
                {
                    m_listBoxLowEntities.SelectedItems.Add(strEntity);
                }
            }
        }

        private void m_listBoxMidEntities_DoubleClick(object sender, EventArgs e)
        {
            SelectEntityInScene(m_listBoxMidEntities);
        }

        private void m_listBoxLowEntities_DoubleClick(object sender, EventArgs e)
        {
            SelectEntityInScene(m_listBoxLowEntities);
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            MonorEntityListForm_Load();
        }
    }
}