﻿namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    partial class TemaplatePreviewForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_pictureBoxPreview = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // m_pictureBoxPreview
            // 
            this.m_pictureBoxPreview.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.m_pictureBoxPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_pictureBoxPreview.Location = new System.Drawing.Point(0, 0);
            this.m_pictureBoxPreview.Name = "m_pictureBoxPreview";
            this.m_pictureBoxPreview.Size = new System.Drawing.Size(275, 264);
            this.m_pictureBoxPreview.TabIndex = 0;
            this.m_pictureBoxPreview.TabStop = false;
            // 
            // TemaplatePreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 264);
            this.Controls.Add(this.m_pictureBoxPreview);
            this.Name = "TemaplatePreviewForm";
            this.Text = "预览窗口";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.TemaplatePreviewForm_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TemaplatePreviewForm_MouseDown);
            this.Load += new System.EventHandler(this.TemaplatePreviewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxPreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox m_pictureBoxPreview;
    }
}