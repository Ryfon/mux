﻿namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    partial class GlobalConfigForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_pictureBoxFogColor = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxFogDepth = new System.Windows.Forms.TextBox();
            this.m_checkBoxFogEnable = new System.Windows.Forms.CheckBox();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.m_TextBoxNear = new System.Windows.Forms.TextBox();
            this.m_TextBoxFar = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_btnRefresh = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_btnRefreshTerrainParam = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_tbTerrainAmbient = new System.Windows.Forms.TextBox();
            this.m_groupBoxRandomControl = new System.Windows.Forms.GroupBox();
            this.m_btnResetRandomSize = new System.Windows.Forms.Button();
            this.m_textBoxRandomSizeUpper = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_textBoxRandomSizeDown = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox_RectSelParams = new System.Windows.Forms.GroupBox();
            this.btn_SetRectSelPrec = new System.Windows.Forms.Button();
            this.m_textBoxRectSelPrec = new System.Windows.Forms.TextBox();
            this.label_RectSelPrec = new System.Windows.Forms.Label();
            this.btn_MinorEntityList = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxFogColor)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.m_groupBoxRandomControl.SuspendLayout();
            this.groupBox_RectSelParams.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_pictureBoxFogColor
            // 
            this.m_pictureBoxFogColor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.m_pictureBoxFogColor.Location = new System.Drawing.Point(53, 41);
            this.m_pictureBoxFogColor.Name = "m_pictureBoxFogColor";
            this.m_pictureBoxFogColor.Size = new System.Drawing.Size(52, 21);
            this.m_pictureBoxFogColor.TabIndex = 1;
            this.m_pictureBoxFogColor.TabStop = false;
            this.m_pictureBoxFogColor.Click += new System.EventHandler(this.m_pictureBoxFogColor_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "颜色";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(142, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "深度";
            // 
            // m_textBoxFogDepth
            // 
            this.m_textBoxFogDepth.Location = new System.Drawing.Point(189, 41);
            this.m_textBoxFogDepth.Name = "m_textBoxFogDepth";
            this.m_textBoxFogDepth.Size = new System.Drawing.Size(54, 21);
            this.m_textBoxFogDepth.TabIndex = 4;
            this.m_textBoxFogDepth.Text = "1.0";
            // 
            // m_checkBoxFogEnable
            // 
            this.m_checkBoxFogEnable.AutoSize = true;
            this.m_checkBoxFogEnable.Checked = true;
            this.m_checkBoxFogEnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxFogEnable.Location = new System.Drawing.Point(12, 17);
            this.m_checkBoxFogEnable.Name = "m_checkBoxFogEnable";
            this.m_checkBoxFogEnable.Size = new System.Drawing.Size(48, 16);
            this.m_checkBoxFogEnable.TabIndex = 5;
            this.m_checkBoxFogEnable.Text = "开启";
            this.m_checkBoxFogEnable.UseVisualStyleBackColor = true;
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(199, 106);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 24);
            this.m_btnOK.TabIndex = 6;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "近平面";
            // 
            // m_TextBoxNear
            // 
            this.m_TextBoxNear.Location = new System.Drawing.Point(53, 78);
            this.m_TextBoxNear.Name = "m_TextBoxNear";
            this.m_TextBoxNear.Size = new System.Drawing.Size(54, 21);
            this.m_TextBoxNear.TabIndex = 8;
            this.m_TextBoxNear.Text = "1";
            // 
            // m_TextBoxFar
            // 
            this.m_TextBoxFar.Location = new System.Drawing.Point(189, 78);
            this.m_TextBoxFar.Name = "m_TextBoxFar";
            this.m_TextBoxFar.Size = new System.Drawing.Size(54, 21);
            this.m_TextBoxFar.TabIndex = 10;
            this.m_TextBoxFar.Text = "128";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(142, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "远平面";
            // 
            // m_btnRefresh
            // 
            this.m_btnRefresh.Location = new System.Drawing.Point(55, 106);
            this.m_btnRefresh.Name = "m_btnRefresh";
            this.m_btnRefresh.Size = new System.Drawing.Size(75, 24);
            this.m_btnRefresh.TabIndex = 11;
            this.m_btnRefresh.Text = "刷新";
            this.m_btnRefresh.UseVisualStyleBackColor = true;
            this.m_btnRefresh.Click += new System.EventHandler(this.m_btnRefresh_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.m_pictureBoxFogColor);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.m_btnRefresh);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.m_TextBoxFar);
            this.groupBox1.Controls.Add(this.m_textBoxFogDepth);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.m_checkBoxFogEnable);
            this.groupBox1.Controls.Add(this.m_TextBoxNear);
            this.groupBox1.Controls.Add(this.m_btnOK);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(8, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 136);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "雾效设置";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.m_btnRefreshTerrainParam);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.m_tbTerrainAmbient);
            this.groupBox2.Location = new System.Drawing.Point(8, 149);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(352, 44);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "地形系数";
            // 
            // m_btnRefreshTerrainParam
            // 
            this.m_btnRefreshTerrainParam.Location = new System.Drawing.Point(189, 14);
            this.m_btnRefreshTerrainParam.Name = "m_btnRefreshTerrainParam";
            this.m_btnRefreshTerrainParam.Size = new System.Drawing.Size(75, 24);
            this.m_btnRefreshTerrainParam.TabIndex = 12;
            this.m_btnRefreshTerrainParam.Text = "刷新";
            this.m_btnRefreshTerrainParam.UseVisualStyleBackColor = true;
            this.m_btnRefreshTerrainParam.Click += new System.EventHandler(this.m_btnRefreshTerrainParam_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "Ambient 系数";
            // 
            // m_tbTerrainAmbient
            // 
            this.m_tbTerrainAmbient.Location = new System.Drawing.Point(103, 17);
            this.m_tbTerrainAmbient.Name = "m_tbTerrainAmbient";
            this.m_tbTerrainAmbient.Size = new System.Drawing.Size(54, 21);
            this.m_tbTerrainAmbient.TabIndex = 6;
            this.m_tbTerrainAmbient.Text = "0.3";
            this.m_tbTerrainAmbient.TextChanged += new System.EventHandler(this.m_tbTerrainAmbient_TextChanged);
            // 
            // m_groupBoxRandomControl
            // 
            this.m_groupBoxRandomControl.Controls.Add(this.m_btnResetRandomSize);
            this.m_groupBoxRandomControl.Controls.Add(this.m_textBoxRandomSizeUpper);
            this.m_groupBoxRandomControl.Controls.Add(this.label8);
            this.m_groupBoxRandomControl.Controls.Add(this.m_textBoxRandomSizeDown);
            this.m_groupBoxRandomControl.Controls.Add(this.label7);
            this.m_groupBoxRandomControl.Location = new System.Drawing.Point(10, 202);
            this.m_groupBoxRandomControl.Name = "m_groupBoxRandomControl";
            this.m_groupBoxRandomControl.Size = new System.Drawing.Size(322, 50);
            this.m_groupBoxRandomControl.TabIndex = 17;
            this.m_groupBoxRandomControl.TabStop = false;
            this.m_groupBoxRandomControl.Text = "随机参数";
            // 
            // m_btnResetRandomSize
            // 
            this.m_btnResetRandomSize.Location = new System.Drawing.Point(229, 19);
            this.m_btnResetRandomSize.Name = "m_btnResetRandomSize";
            this.m_btnResetRandomSize.Size = new System.Drawing.Size(75, 23);
            this.m_btnResetRandomSize.TabIndex = 4;
            this.m_btnResetRandomSize.Text = "重设";
            this.m_btnResetRandomSize.UseVisualStyleBackColor = true;
            this.m_btnResetRandomSize.Click += new System.EventHandler(this.m_btnResetRandomSize_Click);
            // 
            // m_textBoxRandomSizeUpper
            // 
            this.m_textBoxRandomSizeUpper.Location = new System.Drawing.Point(161, 21);
            this.m_textBoxRandomSizeUpper.MaxLength = 10;
            this.m_textBoxRandomSizeUpper.Name = "m_textBoxRandomSizeUpper";
            this.m_textBoxRandomSizeUpper.Size = new System.Drawing.Size(46, 21);
            this.m_textBoxRandomSizeUpper.TabIndex = 3;
            this.m_textBoxRandomSizeUpper.Text = "2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(114, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 2;
            this.label8.Text = "最大值";
            // 
            // m_textBoxRandomSizeDown
            // 
            this.m_textBoxRandomSizeDown.Location = new System.Drawing.Point(58, 21);
            this.m_textBoxRandomSizeDown.MaxLength = 10;
            this.m_textBoxRandomSizeDown.Name = "m_textBoxRandomSizeDown";
            this.m_textBoxRandomSizeDown.Size = new System.Drawing.Size(45, 21);
            this.m_textBoxRandomSizeDown.TabIndex = 1;
            this.m_textBoxRandomSizeDown.Text = "0.5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "最小值";
            // 
            // groupBox_RectSelParams
            // 
            this.groupBox_RectSelParams.Controls.Add(this.btn_SetRectSelPrec);
            this.groupBox_RectSelParams.Controls.Add(this.m_textBoxRectSelPrec);
            this.groupBox_RectSelParams.Controls.Add(this.label_RectSelPrec);
            this.groupBox_RectSelParams.Location = new System.Drawing.Point(11, 259);
            this.groupBox_RectSelParams.Name = "groupBox_RectSelParams";
            this.groupBox_RectSelParams.Size = new System.Drawing.Size(321, 54);
            this.groupBox_RectSelParams.TabIndex = 18;
            this.groupBox_RectSelParams.TabStop = false;
            this.groupBox_RectSelParams.Text = "框选参数";
            // 
            // btn_SetRectSelPrec
            // 
            this.btn_SetRectSelPrec.Location = new System.Drawing.Point(115, 23);
            this.btn_SetRectSelPrec.Name = "btn_SetRectSelPrec";
            this.btn_SetRectSelPrec.Size = new System.Drawing.Size(75, 23);
            this.btn_SetRectSelPrec.TabIndex = 2;
            this.btn_SetRectSelPrec.Text = "重设";
            this.btn_SetRectSelPrec.UseVisualStyleBackColor = true;
            this.btn_SetRectSelPrec.Click += new System.EventHandler(this.btn_SetRectSelPrec_Click);
            // 
            // m_textBoxRectSelPrec
            // 
            this.m_textBoxRectSelPrec.Location = new System.Drawing.Point(52, 25);
            this.m_textBoxRectSelPrec.MaxLength = 5;
            this.m_textBoxRectSelPrec.Name = "m_textBoxRectSelPrec";
            this.m_textBoxRectSelPrec.Size = new System.Drawing.Size(50, 21);
            this.m_textBoxRectSelPrec.TabIndex = 1;
            this.m_textBoxRectSelPrec.Text = "10";
            // 
            // label_RectSelPrec
            // 
            this.label_RectSelPrec.AutoSize = true;
            this.label_RectSelPrec.Location = new System.Drawing.Point(10, 28);
            this.label_RectSelPrec.Name = "label_RectSelPrec";
            this.label_RectSelPrec.Size = new System.Drawing.Size(29, 12);
            this.label_RectSelPrec.TabIndex = 0;
            this.label_RectSelPrec.Text = "精度";
            // 
            // btn_MinorEntityList
            // 
            this.btn_MinorEntityList.Location = new System.Drawing.Point(16, 338);
            this.btn_MinorEntityList.Name = "btn_MinorEntityList";
            this.btn_MinorEntityList.Size = new System.Drawing.Size(122, 28);
            this.btn_MinorEntityList.TabIndex = 19;
            this.btn_MinorEntityList.Text = "次要物品列表";
            this.btn_MinorEntityList.UseVisualStyleBackColor = true;
            this.btn_MinorEntityList.Click += new System.EventHandler(this.btn_MinorEntityList_Click);
            // 
            // GlobalConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 656);
            this.Controls.Add(this.btn_MinorEntityList);
            this.Controls.Add(this.groupBox_RectSelParams);
            this.Controls.Add(this.m_groupBoxRandomControl);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "GlobalConfigForm";
            this.Text = "全局属性配置";
            this.Load += new System.EventHandler(this.GlobalConfigForm_Load);
            this.Shown += new System.EventHandler(this.GlobalConfigForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxFogColor)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.m_groupBoxRandomControl.ResumeLayout(false);
            this.m_groupBoxRandomControl.PerformLayout();
            this.groupBox_RectSelParams.ResumeLayout(false);
            this.groupBox_RectSelParams.PerformLayout();
            this.ResumeLayout(false);

       }

        #endregion

       private System.Windows.Forms.PictureBox m_pictureBoxFogColor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxFogDepth;
        private System.Windows.Forms.CheckBox m_checkBoxFogEnable;
        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox m_TextBoxNear;
        private System.Windows.Forms.TextBox m_TextBoxFar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button m_btnRefresh;
        // add by hemeng
        private int[] m_pCustomColors;
        private MinorEntityListForm m_MinorEntityPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_tbTerrainAmbient;
        private System.Windows.Forms.Button m_btnRefreshTerrainParam;
        private System.Windows.Forms.GroupBox m_groupBoxRandomControl;
        private System.Windows.Forms.Button m_btnResetRandomSize;
        private System.Windows.Forms.TextBox m_textBoxRandomSizeUpper;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox m_textBoxRandomSizeDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox_RectSelParams;
        private System.Windows.Forms.Label label_RectSelPrec;
        private System.Windows.Forms.Button btn_SetRectSelPrec;
        private System.Windows.Forms.TextBox m_textBoxRectSelPrec;
        private System.Windows.Forms.Button btn_MinorEntityList; 
    }
}