﻿namespace Emergent.Gamebryo.SceneDesigner.GUI
{
    partial class MinorEntityListForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_listBoxAllEntities = new System.Windows.Forms.ListBox();
            this.m_listBoxMidEntities = new System.Windows.Forms.ListBox();
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_btnRemoveMidEntity = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_listBoxLowEntities = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.level1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.level2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_btnRemoveLowEntity = new System.Windows.Forms.Button();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.level1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.levelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_btnLower = new System.Windows.Forms.Button();
            this.checkBox_ShowTerrainProperty = new System.Windows.Forms.CheckBox();
            this.btn_GetSelectEntity = new System.Windows.Forms.Button();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_listBoxAllEntities
            // 
            this.m_listBoxAllEntities.FormattingEnabled = true;
            this.m_listBoxAllEntities.ItemHeight = 12;
            this.m_listBoxAllEntities.Location = new System.Drawing.Point(14, 77);
            this.m_listBoxAllEntities.Name = "m_listBoxAllEntities";
            this.m_listBoxAllEntities.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxAllEntities.Size = new System.Drawing.Size(157, 340);
            this.m_listBoxAllEntities.TabIndex = 0;
            this.m_listBoxAllEntities.DoubleClick += new System.EventHandler(this.m_listBoxAllEntities_DoubleClick);
            // 
            // m_listBoxMidEntities
            // 
            this.m_listBoxMidEntities.FormattingEnabled = true;
            this.m_listBoxMidEntities.ItemHeight = 12;
            this.m_listBoxMidEntities.Location = new System.Drawing.Point(208, 77);
            this.m_listBoxMidEntities.Name = "m_listBoxMidEntities";
            this.m_listBoxMidEntities.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxMidEntities.Size = new System.Drawing.Size(150, 340);
            this.m_listBoxMidEntities.TabIndex = 1;
            this.m_listBoxMidEntities.DoubleClick += new System.EventHandler(this.m_listBoxMidEntities_DoubleClick);
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Location = new System.Drawing.Point(177, 97);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(25, 23);
            this.m_btnAdd.TabIndex = 2;
            this.m_btnAdd.Text = "->";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_btnRemoveMidEntity
            // 
            this.m_btnRemoveMidEntity.Location = new System.Drawing.Point(177, 166);
            this.m_btnRemoveMidEntity.Name = "m_btnRemoveMidEntity";
            this.m_btnRemoveMidEntity.Size = new System.Drawing.Size(25, 23);
            this.m_btnRemoveMidEntity.TabIndex = 3;
            this.m_btnRemoveMidEntity.Text = "<-";
            this.m_btnRemoveMidEntity.UseVisualStyleBackColor = true;
            this.m_btnRemoveMidEntity.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "所有物件(level 0)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(206, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "中等物件(level 1)";
            // 
            // m_listBoxLowEntities
            // 
            this.m_listBoxLowEntities.FormattingEnabled = true;
            this.m_listBoxLowEntities.ItemHeight = 12;
            this.m_listBoxLowEntities.Location = new System.Drawing.Point(398, 77);
            this.m_listBoxLowEntities.Name = "m_listBoxLowEntities";
            this.m_listBoxLowEntities.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxLowEntities.Size = new System.Drawing.Size(150, 340);
            this.m_listBoxLowEntities.TabIndex = 7;
            this.m_listBoxLowEntities.DoubleClick += new System.EventHandler(this.m_listBoxLowEntities_DoubleClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(396, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "低等物件(level 2)";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.level1ToolStripMenuItem,
            this.level2ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(107, 48);
            // 
            // level1ToolStripMenuItem
            // 
            this.level1ToolStripMenuItem.Name = "level1ToolStripMenuItem";
            this.level1ToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.level1ToolStripMenuItem.Text = "Level1";
            this.level1ToolStripMenuItem.Click += new System.EventHandler(this.level1ToolStripMenuItem_Click);
            // 
            // level2ToolStripMenuItem
            // 
            this.level2ToolStripMenuItem.Name = "level2ToolStripMenuItem";
            this.level2ToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.level2ToolStripMenuItem.Text = "Level2";
            this.level2ToolStripMenuItem.Click += new System.EventHandler(this.level2ToolStripMenuItem_Click);
            // 
            // m_btnRemoveLowEntity
            // 
            this.m_btnRemoveLowEntity.Location = new System.Drawing.Point(364, 166);
            this.m_btnRemoveLowEntity.Name = "m_btnRemoveLowEntity";
            this.m_btnRemoveLowEntity.Size = new System.Drawing.Size(25, 23);
            this.m_btnRemoveLowEntity.TabIndex = 10;
            this.m_btnRemoveLowEntity.Text = "<-";
            this.m_btnRemoveLowEntity.UseVisualStyleBackColor = true;
            this.m_btnRemoveLowEntity.Click += new System.EventHandler(this.m_btnRemoveLowEntity_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.level1ToolStripMenuItem1,
            this.levelToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(113, 48);
            // 
            // level1ToolStripMenuItem1
            // 
            this.level1ToolStripMenuItem1.Name = "level1ToolStripMenuItem1";
            this.level1ToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.level1ToolStripMenuItem1.Text = "Level 1";
            this.level1ToolStripMenuItem1.Click += new System.EventHandler(this.level1ToolStripMenuItem1_Click);
            // 
            // levelToolStripMenuItem
            // 
            this.levelToolStripMenuItem.Name = "levelToolStripMenuItem";
            this.levelToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.levelToolStripMenuItem.Text = "Level 0";
            this.levelToolStripMenuItem.Click += new System.EventHandler(this.levelToolStripMenuItem_Click);
            // 
            // m_btnLower
            // 
            this.m_btnLower.Location = new System.Drawing.Point(364, 97);
            this.m_btnLower.Name = "m_btnLower";
            this.m_btnLower.Size = new System.Drawing.Size(25, 23);
            this.m_btnLower.TabIndex = 12;
            this.m_btnLower.Text = "->";
            this.m_btnLower.UseVisualStyleBackColor = true;
            this.m_btnLower.Click += new System.EventHandler(this.m_btnLower_Click);
            // 
            // checkBox_ShowTerrainProperty
            // 
            this.checkBox_ShowTerrainProperty.AutoSize = true;
            this.checkBox_ShowTerrainProperty.Location = new System.Drawing.Point(208, 18);
            this.checkBox_ShowTerrainProperty.Name = "checkBox_ShowTerrainProperty";
            this.checkBox_ShowTerrainProperty.Size = new System.Drawing.Size(96, 16);
            this.checkBox_ShowTerrainProperty.TabIndex = 13;
            this.checkBox_ShowTerrainProperty.Text = "显示地形属性";
            this.checkBox_ShowTerrainProperty.UseVisualStyleBackColor = true;
            this.checkBox_ShowTerrainProperty.CheckedChanged += new System.EventHandler(this.checkBox_ShowTerrainProperty_CheckedChanged);
            // 
            // btn_GetSelectEntity
            // 
            this.btn_GetSelectEntity.Location = new System.Drawing.Point(14, 18);
            this.btn_GetSelectEntity.Name = "btn_GetSelectEntity";
            this.btn_GetSelectEntity.Size = new System.Drawing.Size(63, 20);
            this.btn_GetSelectEntity.TabIndex = 14;
            this.btn_GetSelectEntity.Text = "获取物件";
            this.btn_GetSelectEntity.UseVisualStyleBackColor = true;
            this.btn_GetSelectEntity.Click += new System.EventHandler(this.btn_GetSelectEntity_Click);
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Location = new System.Drawing.Point(98, 18);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(90, 20);
            this.btn_Refresh.TabIndex = 15;
            this.btn_Refresh.Text = "刷新物件列表";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // MinorEntityListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(575, 463);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_btnLower);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.m_btnRemoveLowEntity);
            this.Controls.Add(this.m_listBoxLowEntities);
            this.Controls.Add(this.m_listBoxAllEntities);
            this.Controls.Add(this.btn_GetSelectEntity);
            this.Controls.Add(this.checkBox_ShowTerrainProperty);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.m_btnRemoveMidEntity);
            this.Controls.Add(this.m_listBoxMidEntities);
            this.Name = "MinorEntityListForm";
            this.Text = "次要物品列表";
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox m_listBoxAllEntities;
        private System.Windows.Forms.ListBox m_listBoxMidEntities;
        private System.Windows.Forms.Button m_btnAdd;
        private System.Windows.Forms.Button m_btnRemoveMidEntity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox m_listBoxLowEntities;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem level1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem level2ToolStripMenuItem;
        private System.Windows.Forms.Button m_btnRemoveLowEntity;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem level1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem levelToolStripMenuItem;
        private System.Windows.Forms.Button m_btnLower;
        private System.Windows.Forms.CheckBox checkBox_ShowTerrainProperty;
        private System.Windows.Forms.Button btn_GetSelectEntity;
        private System.Windows.Forms.Button btn_Refresh;
    }
}