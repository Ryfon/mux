﻿/** @file MDesignerConfig.h 
@brief 将 CSceneDesignerConfig 中的结构封装为 Object
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-03-14
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：本类只在场景编辑器中使用
</pre>*/

#pragma once

//int		iID;		
//char	szName[64];
//NiColor	kColor;

#include "SceneDesignerConfig.h"
#include "MDisposable.h"
namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		public __gc class MTerrainSurfaceProperty : public MDisposable
		{
		public:
			MTerrainSurfaceProperty(stTerrainSurfaceProperty*	pProp);
			~MTerrainSurfaceProperty(void);

			// properties
			__property int get_ID();
			__property System::String __gc * get_Name();
			__property float get_R();
			__property float get_G();
			__property float get_B();

		protected:
			virtual void Do_Dispose(bool bDisposing);

		private:
			int m_iID;
			System::String*	m_strName;
			float m_fR;
			float m_fG;
			float m_fB;
		};
}}}}
