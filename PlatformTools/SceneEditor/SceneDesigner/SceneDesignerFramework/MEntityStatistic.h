﻿#pragma once
#include "MBound.h"
#include "MEntity.h"
#include <NiTSet.h>

using namespace System;
using namespace System::Collections;
using namespace System::Windows;
using namespace System::Windows::Forms;
using namespace System::IO;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{

// 场景中一个物件实例的统计信息
public __gc class MEntityStatInst
{
public:
	MEntityStatInst(MEntity* pmEntity, const NiBound& kBound, int iLights);

	MEntity*	m_pmTagEntity;	// 目标 entity
	MBound*		m_pmBound;	// 该实例包围盒
	int			m_iLights;	// 影响该实例得灯光数量
};

// 纹理的统计信息
public __gc class MTextureStatInfo
{
public:
	MTextureStatInfo(String* strName, int iInstCount, __int64 i64Size, int iWidth, int iHeight)
		: m_strFilename(strName), m_iInstCount(iInstCount), m_i64Size(i64Size),
		m_iWidth(iWidth), m_iHeight(iHeight), m_iSeqTextureNum(1)
	{}
	String*		m_strFilename;	// 文件名称
	int			m_iInstCount;	// 被使用的次数
	__int64		m_i64Size;		// 纹理大小
	int			m_iWidth;
	int			m_iHeight;
	int			m_iSeqTextureNum;	// 序列纹理数量

};

// 场景中相同实例物件信息统计
public __gc class MEntityStatInfo
{
public:
	MEntityStatInfo(String* strNifFilename);
	
	~MEntityStatInfo();

	// 获取SceneGraph 中的几何体统计信息，递归调用
	void GetObjectGeomInfo(NiAVObject* pkAVObj);

	// 获取 texturing property 中纹理数量
	static int GetTextureCount(NiTexturingProperty* pTexProp);

	// 统计纹理使用
	void CollectTextureStatInfo(NiTexturingProperty::Map* pkMap);

	// 获取纹理尺寸
	__int64 GetSrcTextureSize(NiSourceTexture* pkSrcTex);

	// 获取 map 对应的 ifl 控制器
	NiFlipController* GetMapFlipController(NiTexturingProperty::Map* pMap);

	// 成员变量，为了方便操作将其放在 public 中 
	String*			m_strNifFilename;	// 对应的 nif 文件
	__int64			m_i64NifSize;		// nif 文件尺寸
	ArrayList*		m_arrInstStats;		// 该 nif 每个实例的 统计
	int				m_iGeomCount;		// 几何体数量
	int				m_iVertCount;		// 顶点数量
	int				m_iTriCount;		// 三角形数量
	int				m_iTexInstCount;	// 纹理实例数量
	int				m_iMaxLights;		// 所有实例中最大灯光影响数

	int				m_iParticleCount;	// 粒子数量
	int				m_iMaxParticleCount;// 最大粒子数量

	NiTPrimitiveSet<NiFlipController*>*		m_FlipControllers;// IFL / AVI 格式纹理控制器列表
	Hashtable*		m_htTextures;		// 对应的所有纹理  名称 - MTextureStatInfo
	__int64			m_i64TotalTextureLength;	// 总纹理大小
};

// 场景中所有物件统计信息
public __gc class MSceneEntityStatistic
{
public:
	MSceneEntityStatistic();

	// 从当前场景生成物件统计信息
	MSceneEntityStatistic* GenerateFromCurrentScene(bool bCollectLightInfo);

	// 获取统计结果
	Hashtable* GetStatisticResult();

protected:

	Hashtable*		m_htEntityStat;		// nif filename - MEntityStatInfo
};

}}}}

