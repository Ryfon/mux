﻿/** @file MSceneGraphView.h 
@brief 生成一个场景的可视场景图
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2009-12-07
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

#include "MEntity.h"
#include "MAlphaProperty.h"
#include "MMaterialProperty.h"
#include "MVertexColorProperty.h"
#include "MSpecularProperty.h"

using namespace System::Windows::Forms;
using namespace System::Collections::Generic;
using namespace System::Collections;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{

		// 场景图的一个结点
		public __gc class MSceneGraphNode
		{
		public:
			MSceneGraphNode(NiAVObject* pkAVObj);

			// properties
			__property String* get_Name() {return m_strName;}
			__property String* get_Type() {return m_strType;}

			// 根据场景图生成 TreeNode
			TreeNode* ToTreeNode();

			// 添加子结点
			void AddChild(MSceneGraphNode* pmChild);

			// 获取结点的属性
			MMaterialProperty* GetMaterialProperty();
			MAlphaProperty* GetAlphaProperty();
			MVertexColorProperty* GetVertexColorProperty();
			MSpecularProperty* GetSpecularProperty();

			// 添加属性
			void AddMaterialProperty();
			void AddAlphaProperty();
			void AddSpecularProperty();

			// 显示/隐藏
			void Hide();
			void Show();

		protected:
			NiAVObject*		m_pkTagAVObject;	// 结点对应的 NiAVObject
			String*			m_strName;			// 结点名称
			String*			m_strType;			// 结点类型 (NiNode, NiTriStrips)
			ArrayList*		m_aChildren;		// 子结点列表 (MSceneGraphNode)
		};

		// 整个场景图
		public __gc class MSceneGraphView
		{
		public:

			// 生成一个 MEntity 的场景图。
			static MSceneGraphView* GenSceneGraphView(MEntity* pmEntity);
		
			// 
			TreeNode* ToTreeNode();

			// 清除目标对象的 dynamic effects
			static bool ClearNifFogPropertyAndDynamicEffect(String* strFilename);


			// 保存到 nif 文件
			bool SaveToNif();

			// 检查这个 nif 中是否有 fog property 或者 DynamicEffect
			// return  true-有  false-没有
			static bool CheckFogPropertyAndDynamicEffect(String* strFilename);


			// 测试用函数，读取nif
			static void TestLoadNif(String* strFilename);

		protected:
			static void ClearForPropertyAndDynamicEffect(NiAVObject* pkAVObj);
			static bool CheckFogPropertyAndDynamicEffect(NiAVObject* pkAVObj);

			// 将构造函数隐藏起来，用户只能通过 GenSceneGraphView 静态函数来创建该对象
			MSceneGraphView(MEntity* pmEntity);

			// 通过 NiAVObject 生成 MSceneGraphNode, 递归调用
			static MSceneGraphNode* _GenSceneGraph(MSceneGraphNode* pmParent, NiAVObject* pkAVObj);

			MSceneGraphNode* m_RootSceneGraphNode;	// 根结点
			MEntity* m_pmTagEntity;

		};


	}}}}
