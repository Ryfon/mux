﻿/** @file MTerrainParameter.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhuxichi
*	完成日期：2007-11-14
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		[SerializableAttribute]
		public __gc class MTerrainParameter
		{
		public:
			MTerrainParameter();
			MTerrainParameter(const int iChunkX, const int iChunkY, String* pcMaterialName, const int iMode, int iMapNo);
			MTerrainParameter(const MTerrainParameter& kTerrainParameter);

			~MTerrainParameter();
			void SetData(const MTerrainParameter& kTerrainParameter);

			void SetChunkX(const int iChunkX);
			void SetChunkY(const int iChunkY);
			void SetMaterialName( String* pcMaterialName);
			void SetMode(const int iMode);
			void SetMapNo(const int iMapNo);

			__property int get_ChunkX();
			__property int get_ChunkY();
			__property String* get_MaterialName();
			__property int get_Mode();
			__property int get_MapNo();

			//System::Object overrides
			virtual bool Equals(Object* pmObj);

		private:

			int m_iChunkX;
			int m_iChunkY;
			String* m_pcMaterialName;
			
			/*
			*	模式选择变量
			*   0: 创建地形 
			*   1: 保存地形
			*   2: 读取地形
			*/
			int m_iMode;

			int m_iMapNo;	// 地图编号
		};
	}}}}
