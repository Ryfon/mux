﻿
#include "SceneDesignerFrameworkPCH.h"
#include "MChangeTerrainVertexCommand.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

MChangeTerrainVertexCommand::MChangeTerrainVertexCommand(CTerrainChangeVertexCommand* pkCommand)
: m_pkCommand(pkCommand)
{
}

//---------------------------------------------------------------------------
// ICommand members.
//---------------------------------------------------------------------------
String* MChangeTerrainVertexCommand::get_Name()
{
	MVerifyValidInstance;

	return new String("Change terrain vertex");
}
//---------------------------------------------------------------------------
void MChangeTerrainVertexCommand::Do_Dispose(bool bDisposing)
{}
//---------------------------------------------------------------------------
NiEntityCommandInterface*
MChangeTerrainVertexCommand::GetNiEntityCommandInterface()
{
	MVerifyValidInstance;

	return NULL;
}
//---------------------------------------------------------------------------
void MChangeTerrainVertexCommand::DoCommand(bool bInBatch, bool bUndoable)
{
	MVerifyValidInstance;

	// Execute command.
	m_pkCommand->DoCommand();
}
//---------------------------------------------------------------------------
void MChangeTerrainVertexCommand::UndoCommand(bool bInBatch)
{
	MVerifyValidInstance;
	m_pkCommand->UndoCommand();

}
//---------------------------------------------------------------------------
