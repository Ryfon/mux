﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#pragma once

#include "MDisposable.h"

#include "MRenderer.h"
#include "MEventManager.h"
#include "MTimeManager.h"
#include "MCameraManager.h"
#include "MViewportManager.h"
#include "MPaletteManager.h"
#include "MProxyManager.h"
#include "MLightManager.h"
#include "MBoundManager.h"

#include "MSceneFactory.h"
#include "MEntityFactory.h"
#include "MSelectionSetFactory.h"
#include "MComponentFactory.h"

#include "MScene.h"
#include "MPickUtility.h"
#include "IInteractionModeService.h"
#include "IRenderingModeService.h"
#include "ISelectionService.h"
#include "IUICommandService.h"
#include "ISettingsService.h"
#include "IOptionsService.h"
#include "MTextureCombination.h"

#pragma unmanaged
//#include "WaterRenderView.h"
#include "TerrainTextureMgr.h"
#include <NiVisualTracker.h>
#pragma managed

using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;
using namespace SceneCore;

class NiVisualTracker;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace Framework
{
    public __gc class MFramework : public MDisposable
    {
    public:
        void Startup();

        // Managers.
        __property MRenderer* get_Renderer();
        __property MEventManager* get_EventManager();
        __property MTimeManager* get_TimeManager();
        __property MCameraManager* get_CameraManager();
        __property MViewportManager* get_ViewportManager();
        __property MPaletteManager* get_PaletteManager();
        __property MProxyManager* get_ProxyManager();
        __property MLightManager* get_LightManager();
        __property MBoundManager* get_BoundManager();

        // Factories.
        __property MSceneFactory* get_SceneFactory();
        __property MEntityFactory* get_EntityFactory();
        __property MSelectionSetFactory* get_SelectionSetFactory();
        __property MComponentFactory* get_ComponentFactory();

        __property String* get_AppStartupPath();
        __property MScene* get_Scene();
        __property MPickUtility* get_PickUtility();
        __property NiExternalAssetManager* get_ExternalAssetManager();
        __property String* get_ApplicationName();
        __property String* get_ImageSubfolder();
        __property bool get_PerformingLongOperation();

        __property String* get_CurrentFilename();
        __property void set_CurrentFilename(String* strCurrentFilename);

		__property void set_DoNotUpdate(bool bUpdate) {m_bDoNotUpdate = bUpdate;}

        MScene* NewScene(unsigned int uiEntityArraySize);
        MScene* LoadScene(String* strFilename, String* strFormat);
        void InitNewScene(MScene* pmNewScene, bool bClearLightList);
        bool SaveScene(String* strFilename, String* strFormat);
		
		// 设置 [12/1/2009 hemeng]
		void SetRenderWindow(bool bRender);

        void Update(); 

		void UpdateMetrics();

		//  [10/23/2009 hemeng]
		NiAVObject* InitAVObjectFromFile(const char* pszFilePath);

		// 获得点光源范围图标
		static NiAVObject* GetPointLightBound();	
		static NiAVObject* GetChunkNumSignAt(int iNum);

		// 获取框选纹理 [12/11/2009 hemeng]
		static NiTexture* GetRectSelTexture();

		//  [10/23/2009 hemeng]
		static int	GetTerrainWaterTextureCount()	{ return ms_nTextureCount;};
		static NiSourceTexturePtr GetTerrainWaterTexture(int iIndex);

		/// 初始化 
		//void InitDAEffectManager(NiRendererPtr pkRenderer);

        // *** begin Emergent internal use only ***
        void BeginLongOperation();
        void EndLongOperation();

        void RestartAnimation();
        // *** end Emergent internal use only ***

		// 设置fake player 的当前位置
		void SetCurrentPosition(NiPoint3* pkPos);
		NiPoint3* GetCurrentPosition();

		float GetCurrentX();
		float GetCurrentY();

        __property static ISelectionService* get_SelectionService();
		
    private:
        // Helper functions.
        void PerformUpdate();
        void InitNewScene(MScene* pmNewScene, bool bRaiseEvent, bool bClearLightList);
        void RegisterSettingsAndOptions();
        void OnImageSubfolderSettingChanged(Object* pmSender,
            SettingChangedEventArgs* pmEventArgs);
		void ClearOldSceneRes();	/// 清除 old scene 中各种资源。比如 tree view 节点， CWaterRenderView, CWaterManager

		//081226 add by 和萌
		////////////////////////////////////////////////////////////////////////////////
		//函数名:CheckFileInDisc
		//功能: 检测场景中所有NIF文件是否在硬盘上都存在
		//入口参数: MScene* pScene 场景
		//出口参数: bool 都存在返回true，反之返回false
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		void CheckFileInDisc( MScene* pScene );

		// add [6/9/2009 hemeng]
		bool CheckSceneLight( MScene* pScene, bool bClearLightList );

        // Service accessors.
        __property static IInteractionModeService*
            get_InteractionModeService();
        static IInteractionModeService* ms_pmInteractionModeService;

        static ISelectionService* ms_pmSelectionService;
        __property static ISettingsService* get_SettingsService();
        static ISettingsService* ms_pmSettingsService;
        __property static IOptionsService* get_OptionsService();
        static IOptionsService* ms_pmOptionsService;

        // Member variables.
        MScene* m_pmScene;
        MScene* m_pmToolScene;
        MPickUtility* m_pmPickUtility;
        NiExternalAssetManager* m_pkAssetManager;
        String* m_strImageSubfolder;
        bool m_bDoNotUpdate;
        bool m_bInitSuccessful;
        unsigned int m_uiLongOperationCount;
        String* m_strCurrentFilename;

		// 090121 add by 和萌
		// 添加点光源影响范围标志
		NiAVObject* m_pmLightBound;

		// 地表水波相关 [10/22/2009 hemeng]
		const static UINT ms_nTextureCount = 32;

		//  [7/13/2009 hemeng]
		// chunk num sgins
		 NiAVObject* m_pmNumSigns __nogc[10];

		 // 绘制选框的图片 [12/10/2009 hemeng]
		 NiTexture* m_pkRectSelTexture;

		 // 检测是否正在使用纹理导出 [12/1/2009 hemeng]
		 bool	m_bRenderWindow;

        static String* ms_strImageSubfolderSettingName = "Image Subfolder";
        static String* ms_strImageSubfolderOptionName = String::Concat(
            "External File Settings.", ms_strImageSubfolderSettingName);
		//MTextureCombination* m_pTextureCombination;

		float m_fFrameRate;
		float m_fCullTime;
		float m_fUpdateTime;
		float m_fRenderTime;
		

    // MDisposable members.
    protected:
        virtual void Do_Dispose(bool bDisposing);

		// 090121 add by 和萌
		// 初始化点光源范围标志
		bool InitPointLightBoundFile();

		// initial num sgins [7/13/2009 hemeng]
		bool InitChunkNumSigns();
		// initial rectangle select texture [12/11/2009 hemeng]
		static bool InitRectSelTexture();

    // Singleton members.
    public:
        static void Init();
        static void Shutdown();
        static bool InstanceIsValid();
        __property static MFramework* get_Instance();
    private:
        static MFramework* ms_pmThis = NULL;
		NiPoint3*			m_pkCurrentPos;
		MFramework();
    };
}}}}
