﻿

#pragma once
#include "MNiProperty.h"
#include "MPoint3.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		[SerializableAttribute]
		public __gc class MMaterialProperty : public MNiProperty
		{
		public:
			MMaterialProperty(NiMaterialProperty* pkProperty, NiAVObject* pkTagAVObj);
	
			// 以 NiMaterialProperty 方式获取 TagProperty
			NiMaterialProperty* GetNiMaterialProperty(){return static_cast<NiMaterialProperty*>(m_pkTagProperty);}

			// NiMaterialProperty 属性设置
			__property float get_Alpha();
			__property void set_Alpha(float fX);

			__property float get_Shineness();
			__property void set_Shineness(float fY);

			__property MPoint3* get_Ambient();
			__property void set_Ambient(MPoint3* pmColor);

			__property MPoint3* get_Diffuse();
			__property void set_Diffuse(MPoint3* pmColor);

			__property MPoint3* get_Specular();
			__property void set_Specular(MPoint3* pmColor);

			__property MPoint3* get_Emittance();
			__property void set_Emittance(MPoint3* pmColor);

			//System::Object overrides
			virtual bool Equals(Object* pmObj);
			virtual String* ToString() {return "NiMaterialProperty";}

		private:

		};
	}}}}
