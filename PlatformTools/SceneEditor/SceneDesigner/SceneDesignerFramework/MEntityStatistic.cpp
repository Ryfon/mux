﻿#include "SceneDesignerFrameworkPCH.h"
#include "MEntityStatistic.h"
#include "MFramework.h"
#include "MLightManager.h"
#include <NiFlipController.h>
#include <NiSourceTexture.h>
#include "MUtility.h"
#include "MTerrain.h"
#include "Terrain.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{
//-----------------------------------------------------
MEntityStatInst::MEntityStatInst(MEntity* pmEntity, const NiBound& kBound, int iLights)
: m_pmTagEntity(pmEntity), m_iLights(iLights)
{
	m_pmBound = new MBound(kBound);
}
//-----------------------------------------------------
MEntityStatInfo::MEntityStatInfo(String* strNifFilename)
: m_strNifFilename(strNifFilename), m_iGeomCount(0), m_iVertCount(0),
	m_iTriCount(0), m_iTexInstCount(0), m_iMaxLights(0), m_iParticleCount(0), m_iMaxParticleCount(0),
	m_i64TotalTextureLength(0)
{
	m_arrInstStats = new ArrayList();
	m_FlipControllers = NiNew NiTPrimitiveSet<NiFlipController*>();

	m_htTextures = new Hashtable();
}
//-----------------------------------------------------
MEntityStatInfo::~MEntityStatInfo()
{
	NiDelete m_FlipControllers;
}
//-----------------------------------------------------
//NiFlipController* GetObjectFlipController(NiAVObject* pkObj)
//{
//	
//}
//-----------------------------------------------------
void MEntityStatInfo::GetObjectGeomInfo(NiAVObject* pkAVObj)
{
	if (!pkAVObj) return;

	// 如果物件被剔除，不考虑
	if (pkAVObj->GetAppCulled()) return;

	NiFlipController* pkFlipCtrl = (NiFlipController*)(pkAVObj->GetController(&NiFlipController::ms_RTTI));
	if (pkFlipCtrl)
	{
		m_FlipControllers->Add(pkFlipCtrl);
	}

	if (NiIsKindOf(NiNode, pkAVObj))
	{
		NiNode* pkNode = NiDynamicCast(NiNode, pkAVObj);
		pkNode->CompactChildArray();
		for (int i=0; i<pkNode->GetChildCount(); i++)
		{
			NiAVObject* pkChild = pkNode->GetAt(i);
			GetObjectGeomInfo(pkChild);
		}
	}
	else if (NiIsKindOf(NiGeometry, pkAVObj))
	{
		NiGeometry* pkGeom = NiDynamicCast(NiGeometry, pkAVObj);

		NiTexturingProperty* pkTexProp = (NiTexturingProperty*)pkGeom->GetProperty(NiProperty::TEXTURING);

		if (pkTexProp)
		{
			pkFlipCtrl = (NiFlipController*)(pkTexProp->GetController(&NiFlipController::ms_RTTI));
			if (pkFlipCtrl)
			{
				m_FlipControllers->Add(pkFlipCtrl);
			}

			int iNumTex = GetTextureCount(pkTexProp);
			m_iTexInstCount += iNumTex;

			const NiTexturingProperty::NiMapArray& arrMaps = pkTexProp->GetMaps();
			for (unsigned int i=0; i<arrMaps.GetSize(); i++)
			{
				CollectTextureStatInfo(arrMaps.GetAt(i));
			}

			for (unsigned int i=0; i<pkTexProp->GetShaderMapCount(); i++)
			{
				CollectTextureStatInfo(pkTexProp->GetShaderMap(i));
			}

		}

		if (NiIsKindOf(NiTriBasedGeom, pkAVObj))
		{
			NiTriBasedGeom* pkTriGeom = NiDynamicCast(NiTriBasedGeom, pkAVObj);
			++m_iGeomCount;
			m_iVertCount += pkTriGeom->GetActiveVertexCount();
			m_iTriCount += pkTriGeom->GetActiveTriangleCount();	
		}
		else if (NiIsKindOf(NiParticleSystem, pkAVObj))
		{
			NiParticleSystem* pkPS = NiDynamicCast(NiParticleSystem, pkAVObj);
			m_iParticleCount += pkPS->GetNumParticles();
			m_iMaxParticleCount += pkPS->GetMaxNumParticles();
		}

		

	}
	else if(NiIsKindOf(NiTextureEffect, pkAVObj))
	{
		++m_iTexInstCount;	// 环境贴图
	}

}
//----------------------------------------------------
NiFlipController* MEntityStatInfo::GetMapFlipController(NiTexturingProperty::Map* pMap)
{
	for (unsigned int i=0; i<m_FlipControllers->GetSize(); i++)
	{
		NiFlipController* pController = m_FlipControllers->GetAt(i);
		if (pController->GetAffectedMap() == pMap)
		{
			return pController;
		}
	}

	return NULL;
}
//-----------------------------------------------------
int MEntityStatInfo::GetTextureCount(NiTexturingProperty* pTexProp)
{
	if (pTexProp == NULL) return 0;

	int iCount = 0;
	if (pTexProp->GetBaseMap()) ++iCount;
	if (pTexProp->GetDarkMap()) ++iCount;
	if (pTexProp->GetDetailMap()) ++iCount;
	if (pTexProp->GetGlossMap()) ++iCount;
	if (pTexProp->GetGlowMap()) ++iCount;
	if (pTexProp->GetNormalMap()) ++iCount;
	if (pTexProp->GetParallaxMap()) ++iCount;

	iCount += pTexProp->GetDecalMapCount();

	return iCount;
}
//-----------------------------------------------------
void MEntityStatInfo::CollectTextureStatInfo(NiTexturingProperty::Map* pkMap)
{
	if (!pkMap) return;

	NiFlipController* pkFlipCtrl = GetMapFlipController(pkMap);

	NiSourceTexture* pkSrcTexture = NiDynamicCast(NiSourceTexture, pkMap->GetTexture());
	if (!pkSrcTexture) return;

	NiFixedString strTexName = pkSrcTexture->GetFilename();
	if (strTexName.Contains("chunk_")) return; // 忽略 chunk blend 和 chunk property 纹理

	String* strFileName = strTexName; // 这里纹理路径只是文件名

	strFileName = Path::GetFileName(strFileName);
	if (!m_htTextures->ContainsKey(strFileName))
	{
		int iWidth = pkSrcTexture->GetWidth();
		int iHeight = pkSrcTexture->GetHeight();
		MTextureStatInfo* pTexStatInfo = new MTextureStatInfo(strFileName, 1, 
			0, iWidth, iHeight);

		if (pkFlipCtrl)
		{
			// 是序列纹理
			pTexStatInfo->m_iSeqTextureNum = pkFlipCtrl->GetTextureArraySize();
		}
		m_htTextures->Add(strFileName, pTexStatInfo);
		//m_i64TotalTextureLength += fileInfo->get_Length();
	}
	else
	{
		// 添加过了
		MTextureStatInfo* pTexStatInfo = (MTextureStatInfo*)m_htTextures->get_Item(strFileName);
		pTexStatInfo->m_iInstCount++;
	}
}
//-----------------------------------------------------
__int64 MEntityStatInfo::GetSrcTextureSize(NiSourceTexture* pkSrcTex)
{
	if (pkSrcTex)
	{
		NiFixedString strTexName = pkSrcTex->GetFilename();
		String* strFileName = strTexName;
		FileInfo* fileInfo = new FileInfo(strFileName);
		return fileInfo->get_Length();
	}

	return 0;
}
//-----------------------------------------------------
MSceneEntityStatistic::MSceneEntityStatistic()
{
	m_htEntityStat = new Hashtable();
}
//-----------------------------------------------------
MSceneEntityStatistic* MSceneEntityStatistic::GenerateFromCurrentScene(bool bCollectLightInfo)
{
	// 遍历当前场景，生成所有 Entity 的统计信息
	if (MFramework::Instance && MFramework::Instance->Scene)
	{
		// 清除旧数据
		m_htEntityStat->Clear();

		MEntity* arrEntities[] = MFramework::Instance->Scene->GetEntities();
		for (int i=0; i<arrEntities->Length; ++i)
		{
			MEntity* pmEntity = arrEntities[i];
			// 从 entity 中获取 NIF File Path
			if (pmEntity->HasProperty("NIF File Path"))
			{
				// entity 对应的 nif 文件
				String* strNifFilePath = dynamic_cast<String*>(pmEntity->GetPropertyData("NIF File Path"));
				if (strNifFilePath != NULL)
				{
					// entity 对应的 SceneGraph
					NiAVObject* pkSceneRootPointer = pmEntity->GetSceneRootPointer(0);
					if (pkSceneRootPointer == NULL) {continue;}
					int iNumLights = 0; 
					if (bCollectLightInfo)
					{
						iNumLights = MLightManager::Instance->GetAffectLights(pmEntity)->Count;	// 影响当前 entity 的灯光数
					}

					NiBound kBound = pkSceneRootPointer->GetWorldBound();

					if (m_htEntityStat->ContainsKey(strNifFilePath))
					{
						// 如果已经采集过该 entity 其他实例信息，
						// 只将该 Entity Instance 信息添加到已存在的 MEntityStatInfo 中
						MEntityStatInfo* pmStatInfo = 
							dynamic_cast<MEntityStatInfo*>(m_htEntityStat->get_Item(strNifFilePath));
						MEntityStatInst* pmStatInst = new MEntityStatInst(pmEntity, kBound, iNumLights);

						pmStatInfo->m_arrInstStats->Add(pmStatInst);
						// 更新最大灯光影响数
						if (iNumLights > pmStatInfo->m_iMaxLights)
						{
							pmStatInfo->m_iMaxLights = iNumLights;
						}
					}
					else
					{
						// 第一次统计该 entity
						MEntityStatInfo* pmStatInfo = new MEntityStatInfo(strNifFilePath);
						pmStatInfo->GetObjectGeomInfo(pkSceneRootPointer);
						pmStatInfo->m_iMaxLights = iNumLights;

						MEntityStatInst* pmStatInst = new MEntityStatInst(pmEntity, kBound, iNumLights);
						pmStatInfo->m_arrInstStats->Add(pmStatInst);
						m_htEntityStat->Add(strNifFilePath, pmStatInfo);

					}
				}
			}
		}

		// 统计地形
		if (MTerrain::Instance->Terrain)
		{
			MEntityStatInfo* pmStatInfo = new MEntityStatInfo("Terrain");
			pmStatInfo->GetObjectGeomInfo(MTerrain::Instance->Terrain->GetTerrainRootNode());
			pmStatInfo->m_iMaxLights = 2;
			m_htEntityStat->Add(pmStatInfo->m_strNifFilename, pmStatInfo);
		}

	}
	return NULL;
}
//-----------------------------------------------------
Hashtable* MSceneEntityStatistic::GetStatisticResult()
{
	return m_htEntityStat;
	
}
}}}}
