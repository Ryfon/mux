﻿#include "SceneDesignerFrameworkPCH.h"
#include "MBound.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{

MBound::MBound(void)
{
	m_pmCenter = new MPoint3();
}
//--------------------------------------------------
MBound::MBound(float fX, float fY, float fZ, float fR)
: m_fR(fR)
{
	m_pmCenter = new MPoint3(fX, fY, fZ);
}
//--------------------------------------------------
MBound::MBound(MPoint3* pmCenter, float fR)
: m_fR(fR)
{
	NiPoint3 kData;
	pmCenter->ToNiPoint3(kData);
	m_pmCenter = new MPoint3(kData);
}
//--------------------------------------------------
MBound::MBound(const NiBound& kBound)
{
	m_pmCenter = new MPoint3(kBound.GetCenter());
	m_fR = kBound.GetRadius();
}
//--------------------------------------------------
void MBound::SetData(const NiBound& kBound)
{
	m_pmCenter->SetData(kBound.GetCenter());
	m_fR = kBound.GetRadius();
}
//--------------------------------------------------
void MBound::ToNiBound(NiBound& kBound)
{
	NiPoint3 kCenter;
	m_pmCenter->ToNiPoint3(kCenter);
	kBound.SetCenter(kCenter);
	kBound.SetRadius(m_fR);
}
//--------------------------------------------------
float MBound::get_R()
{
	return m_fR;
}
//--------------------------------------------------
void MBound::set_R(float fR)
{
	m_fR = fR;
}
//--------------------------------------------------
MPoint3* MBound::get_Center()
{
	return m_pmCenter;
}
//--------------------------------------------------
void MBound::set_Center(MPoint3* pmCenter)
{
	NiPoint3 kCenter;
	pmCenter->ToNiPoint3(kCenter);
	m_pmCenter->SetData(kCenter);
}
//--------------------------------------------------
//System::Object overrides
bool MBound::Equals(Object* pmObj)
{
	MBound* pmBound = dynamic_cast<MBound*>(pmObj);
	if (pmBound == NULL)
	{
		return false;
	}

	NiBound kBound;
	pmBound->ToNiBound(kBound);

	NiBound kThisBound;
	this->ToNiBound(kThisBound);
	return (kThisBound == kBound);
}
//--------------------------------------------------
}}}}