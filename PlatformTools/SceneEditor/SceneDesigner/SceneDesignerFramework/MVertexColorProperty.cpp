﻿#include "SceneDesignerFrameworkPCH.h"

#include "MVertexColorProperty.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
MVertexColorProperty::MVertexColorProperty(NiVertexColorProperty* pkProperty, NiAVObject* pkTagAVObj)
: MNiProperty(pkProperty, pkTagAVObj)
{
}
//---------------------------------------------------------------------------
//System::Object overrides
bool MVertexColorProperty::Equals(Object* pmObj)
{
	// 不需要比较
	return false;
}
