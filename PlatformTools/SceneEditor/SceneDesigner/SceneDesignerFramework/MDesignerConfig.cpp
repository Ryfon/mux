﻿#include "SceneDesignerFrameworkPCH.h"
#include "MDesignerConfig.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

MTerrainSurfaceProperty::MTerrainSurfaceProperty(stTerrainSurfaceProperty*	pProp)
{
	m_iID = pProp->iID;
	m_strName = new System::String(pProp->szName);
	m_fR = pProp->kColor.r;
	m_fG = pProp->kColor.g;
	m_fB = pProp->kColor.b;
}

MTerrainSurfaceProperty::~MTerrainSurfaceProperty(void)
{
}

void MTerrainSurfaceProperty::Do_Dispose(bool bDisposing)
{
}

int MTerrainSurfaceProperty::get_ID()
{
	return m_iID;
}

System::String __gc * MTerrainSurfaceProperty::get_Name()
{
	return m_strName;
}

float MTerrainSurfaceProperty::get_R()
{
	return m_fR;
}

float MTerrainSurfaceProperty::get_G()
{
	return m_fG;
}

float MTerrainSurfaceProperty::get_B()
{
	return m_fB;
}
