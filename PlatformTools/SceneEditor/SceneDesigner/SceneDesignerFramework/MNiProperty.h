﻿#pragma once

#include "MPoint3.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		// 所有 managed property 基类 
		[SerializableAttribute]
		public  __gc class MNiProperty
		{
		public:
			MNiProperty(NiProperty* pkProperty, NiAVObject* pkTagAVObj);

			// 调用 m_pkTagAVObject 的 UpdateProperties()
			void UpdateProperties();

			// 从 TagAVObject 中删除 TagProperty
			void RemoveProperty();

		protected:
			NiProperty* m_pkTagProperty;
			NiAVObject*	m_pkTagAVObject;		// 目标对象

		};
	}}}}
