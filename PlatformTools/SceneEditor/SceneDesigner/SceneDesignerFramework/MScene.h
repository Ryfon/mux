﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#pragma once

#include "MDisposable.h"
#include "MEntity.h"
#include "MSelectionSet.h"
#include "MRenderingContext.h"
#include "MPoint3.h"
#include "IMessageService.h"
#include "ICommandService.h"


#include <vcclr.h>
#pragma unmanaged
	#include "Terrain.h"
	#include "TerrainMaterial.h"
	#include "TerrainModifier.h"
	#include "TerrainTextureMgr.h"
	#include "QuadTree.h"
	#include "TerrainOptimizer.h"
	#include <NiOptimize.h>
	#include "TerrainChangeVertexCommand.h"
	#include "WaterRenderView.h"
	#include "WaterBody.h"
	#include "SceneDesignerConfig.h"
	#include "Utility.h"
//	#include "NiSpeedTreeManager.h"
	#include "ShadowMapProcess.h"
	#include "LayerFog.h"
	#include "AOPerVertexShadowGenerator.h"
	#include "AOPerVertexShadowGeneratorGPU.h"
	#include "PerEntityShadowGenerator.h"
	#include "SimpleFog.h"

	#include "LightsManager.h"
	#include "TerrainVertexColorBaker.h"
	#include "TerrainChangeVertexColorCommand.h"
	#include <set>
	// 修改包围盒 [9/15/2009 hemeng]
	#include "BoundGeometroy.h"
#pragma managed

// 场景主灯光名称
#define MAIN_LIGHT_NAME "sun"

using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;
using namespace SceneCore;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace Framework
{
    public __gc class MScene : public MDisposable
    {
    public:
        MScene(NiScene* pkScene);

        NiScene* GetNiScene();

		/// 得到Terrain实例
		__property CTerrain* get_Terrain();
		__property void set_Terrain(CTerrain* pkTerrain);

        __property String* get_Name();
        __property void set_Name(String* strName);

        __property unsigned int get_EntityCount();
        __property bool get_Dirty();
        __property void set_Dirty(bool bDirty);

		/// 测试用property
//		__property CLayerFog* get_LayerFog() {return m_pkLayerFog;}
//		__property void set_LayerFog(CLayerFog* pkFog) {m_pkLayerFog = pkFog;}

		bool Get_BakeShadow();
		bool Set_BakeShadow( bool bBake );

        MEntity* GetEntities()[];
        MEntity* GetEntityByName(String* strName);
        bool AddEntity(MEntity* pmEntity, bool bUndoable);
		/// 将可渲染得 entity 添加到 water render view
		void AddRenderableEntityToWaterRenderView(MEntity* pmEntity);
		void RemoveRenderableEntityFromWaterRenderView(MEntity* pmEntity);
        void RemoveEntity(MEntity* pmEntity, bool bUndoable, bool bResolveDependencies);
        void RemoveAllEntities(bool bUndoable, bool bResolveDependencies);
        bool IsEntityInScene(MEntity* pmEntity);
        MEntity* GetDependentEntities(MEntity* pmEntity)[]; 

        __property unsigned int get_SelectionSetCount();
        MSelectionSet* GetSelectionSets()[];
        MSelectionSet* GetSelectionSetByName(String* strName);
        bool AddSelectionSet(MSelectionSet* pmSelectionSet);
        void RemoveSelectionSet(MSelectionSet* pmSelectionSet);
        void RemoveAllSelectionSets();
        bool IsSelectionSetInScene(MSelectionSet* pmSelectionSet);

        void Update(float fTime, NiExternalAssetManager* pkAssetManager);
        void UpdateEffects();

        String* GetUniqueEntityName(String* strProposedName);
        String* GetUniqueSelectionSetName(String* strProposedName);

        void GetBound(NiBound* pkBound);

		static MScene* FindSceneContainingEntity(MEntity* pmEntity);

		void	BeginTerrain( MRenderingContext* );
		void	RenderTerrain( MRenderingContext* );
		void	EndTerrain( MRenderingContext* );

		/// Terrain 导入导出函数
		bool ImportTerrain(String* strFileName);
		bool ExportTerrain(String* strFileName);

		/// 测试流载入
		bool ImportTerrainFromStream(String* strFileName);

		/// 导出地图的碰撞信息 for server
		bool ExportCollisionData(String* strFileName, String* strMapID);

		bool LoadTerrain(String* strFileName);
		bool SaveTerrain(String* strFileName);

		/// 保存场景信息 (每个chunk用的纹理,每个 chunk 上的物件 id
		bool ExportTerrainInfo(String* strFileName, String* strMapID);

		/// 导出场景所有 Entity
		bool ExportSceneEntity(String* strFileName, String* strSrcLightMapDir);
		// 生成 pmEntity 下所有 light map material geometry 对应的 light map 配置表
		TiXmlElement* GenEntityLightMapXmlElmt(MEntity* pmEntity, String* strSrcLightMapDir);

		/// 导出所有物件的 light map
		bool ExportLightMaps(String* strLightMapXmlFileName, String* strLightMapDir);

		// 导出场景物件简表 [5/21/2009 hemeng]
		bool ExportSceneEntityList(String* strFileName);

		/// 设置物件是否吸附到地面
		void SetSnapToTerrain(bool bSnapToTerrain);
		bool GetSnapState()		{return m_bSnapToTerrain;};

		/// 设置地形优化敏感度
		void SetTerrainOptimizeSensitivity(int iSensitivity);

		/// 设置地形优化及还原 bOptimize - true - 优化   false - 还原
		void OptimizeTerrain(bool bOptimize);

		// 修改为由物件生成地表属性，如果bBlcok存在，则生成碰撞，其他情况生成属性 [9/25/2009 hemeng]
		void BakeObjectsToCollisionBlock();

		// 由物件修改地形属性 [9/25/2009 hemeng]
		//void BackPropertyByGeometry(NiGeometry* pkGeometry,int iProperty);

		// 烘焙物件灯光 [4/15/2010 hemeng]
		void BackLightToEntities();

		/// 根据地形斜度计算碰撞块 任何倾斜角度大于 iBoundDegree 的地表被标志为碰撞 
		void TerrainToCollisionBlock(int iBoundDegree, bool bRenderOnly);

		/// 清除所有碰撞块
		void ClearAllCollisionBlock();

		//导出NPC生成器的函数
		bool ExportNPCCreator();

		/// Terrain 创建函数
		void CreateTerrain(int iChunkX, int iChunkY, const char *pszBaseTex, float fOutRadius, int iMapNo);

		/// 执行调整地形高度命令
		void ExecuteAdjustTerrainHeightCommand(CTerrainChangeVertexCommand* pCommand);

		/// 用QuadTree组织场景，并将场景保存为 nif 文件	
		void ExportScene(System::String* strFileName);

		/// 是否显示碰撞信息
		void ShowCollisionData( bool bShow );

		// 显示隐藏地形 [6/11/2009 hemeng]
		void ShowHiddenTerrain( bool bShow );

		void SetCollisionSignal( bool bOpen );	
		bool GetCollisionSignal(){ return m_bCollisionWriteOpen; }

		// water scene [10/26/2009 hemeng]
		bool GetWaterSceneEnabled();
		void SetWaterSceneEnabled(bool bEnabled);
		float GetWaterSceneCauseSize();
		float GetWaterSceneCauseXOffset();
		float GetWaterSceneCauseYOffset();
		float GetWaterSceneCauseFactor();
		void SetWaterSceneCauseParameter(float fSize, float fXOffset, float fYOffset,float fFactor);

		/// 获取 entity 类型 0 - 未知   1 - geometry   2 - light   4 - npc creater
		static int  GetEntityType(MEntity* mEntity);

		/// 获取地形当前 chunk 的纹理
		System::String* GetTerrainCurrentChunkTexture()[];

		/// 获取当前正使用的纹理层
		int	GetTerrainInUseTextureLayer();

		/// 获取当前正被编辑 chunk id
		int GetCurrentEditChunkID();

		/// 改变当前 chunk 的第 iLayer 层纹理为 strFileName
		bool ReplaceChunkTexture(int iLayer, System::String* strFileName);

		// 删除chunk的第iLayer层纹理 [5/4/2009 hemeng]
		bool DeleteChunkTexture(int iLayer);

		/// 设置当前绘制状态
		void SetPaintTerrainState(bool bPaintTexture, bool bPaintVertexColor,bool bPaintWaterScene);

		/// 设置当前使用的顶点颜色
		void SetInUseVertexColor(System::Drawing::Color color);

		/// 设置笔触纹理
		void SetBrushTexture( System::String* strFileName);

		/// 设置地形操作模式
		void SetTerrainOperation(CTerrainModifier::ETerrainOperation op);

		/// 设置 paint terrain 纹理效果是否叠加
		void SetPaintTerrainAddTo(bool bAddTo);

		/// 添加地形地表属性
		bool AddTerrainProperty(const stTerrainSurfaceProperty pTerrProp);

		/// 设置当前显示的属性
		void SetCurrentTerrainProperty(int iProperty);

		/// 是否地形已经存在
		bool HasTerrain() {return m_pkTerrain != NULL; }

		/// 获取地形尺寸
		int GetTerrainNumChunkX();
		int GetTerrainNumChunkY();

		/// 获取 scene render view
		CWaterRenderView* GetWaterRenderView();

		/// 渲染水体
		//void RenderWaterbody(MRenderingContext* pRenderContext);

		/// 获取地形表面属性列表
		ArrayList* GetTerrainSurfacePropertyList();

		/// 通过文件名获取 template 的显示名称
		String* GetTemplateViewName(String* strFileName);

		//081229 add by 和萌 
		//通过palate菜单重新导入NIF
		void ReImportFile( MEntity* pEntity );

		__property static ICommandService* get_CommandService();
		static ICommandService* ms_pmCommandService;

		/// 是否渲染场景物件
		void SetRenderSceneObject(bool bRender);
		bool GetRenderSceneObject();

		/// 是否渲染 npc 生成器
		void SetRenderNPCCreator(bool bRender);
		bool GetRenderNPCCreator();

		/// 渲染场景中的过高点
		void SetRenderOverTopVertex(bool bRender);
		bool GetRenderOverTopVertex();
		void CalculateOverTopVertex(float fLimitedHeight, float fAngle, float fDistance, int nChechType);

		//////////////////////////////////////////////////////////////////////////
		// 区域相关的函数，add by 徐磊
		/// 获取区域属性
		String* GetRegionPropViewName(int nIndex);
		int GetRegionShapeByType(int nType);
		int GetRegionColorByType(int nType, int nColorType);
		int GetTopRegionID();

		/// 是否渲染区域
		void SetRenderRegion(bool bRender);
		bool GetRenderRegion();

		/// 区域操作的类型
		void SetRegionOperType(int nRegionOperType);
		int GetRegionOperType();

		/// 区域的类型
		void SetGlobalRegionType(int nGlobalRegionType);
		int GetGlobalRegionType();

		/// 当前操作的区域ID
		void SetCurrentRegionID(int nCurrentRegionID);
		int GetCurrentRegionID();

		/// 通过ID更新制定区域的信息
		void SetRegionTypeByID(int uiAreaID, int nRegionType);
		int GetRegionTypeByID(int uiAreaID);
		void SetRegionNameByID(int uiAreaID, String* strRegionName);
		String* GetRegionNameByID(int uiAreaID);
		void SetRegionMusicInfoByID(int uiAreaID, String* strPropName, String* strPropValue);
		String* GetRegionMusicInfoByID(int uiAreaID, String* strPropName);

		/// 文件的导入和导出
		bool SaveRegionToXml(String* strFileName);
		//////////////////////////////////////////////////////////////////////////

		bool		Bake( NiEntityRenderingContext* pRenderContext );

		/// 从 ShadowMapProcess 中获取渲染生成的阴影纹理
		NiRenderedTexturePtr GetShadowTexture();

		/// 载入用户编辑过的阴影纹理
		bool ImportShadowTexture(String* strShadowTexture);

		/// 从 blend texture 的 alpha 通道导出阴影纹理
		bool ExportShadowTexture(String* strShadowTexture);

		/// AO
		bool GenerateAODisk();	// 生成所有 AODisk (包括地形，物件)
		void TerrainAmbientOcclusion(int iChunkID, float fTerrainAffectFactor, float fSceneAffectFactor, float fMaxShadowed);// 生成一个 chunk 的 AO shadow
		void FakeEntityAmbientOcclusion(float fMaxShadow, float fBlur, float fBlurTexSize);

		bool SaveSceneDiskInfo(String* strFileName);	// 保存
		bool LoadSceneDiskInfo(String* strFileName);	// 载入
		
		/// project texture shadow
		void ProjectionShadow(MPoint3* pvLitDir, float fMaxShadowValue, float fBlur);

		int GetTerrainDiskSize();
		int GetSceneDiskSize();

		/// 获取场景中名为 "sun" 的方向光方向
		MPoint3* GetSunDirection();

		/// 测试用，烘焙 FogDepthAlpha
		void Debug_BakeFogDepthToAlpha();

		/// 测试用，检查 scene 中所有 entity
		void Debug_CheckEntitys();

		/// 创建层雾
		void CreateSimpleFog(float fStartZ, float fEndZ, float fLeft, float fRight, float fTop, float fBottom, MPoint3* pColor, int iTexSize, float fSpeedU, float fSpeedV, String* strAnimTexture);

		/// 清理层雾
		void ClearSimpleFog();
		/// 设置是否 preview 层雾
		void SetPreviewSimpleFog(bool bPreview);
		/// 保存层雾纹理及表面
		bool SaveLayerFog(System::String* strTexFile, System::String* strSurfaceFile);

		/// 将场景保存到 3ds 格式
		bool SaveSceneTo3DS();

		//////////////////////////////////////////////////////////////////////////
		//add by hemeng烘焙顶点
		bool RenderColorToVertex();

		//////////////////////////////////////////////////////////////////////////
		//add by 和萌水体生成
		bool InitialRiver(float nZPos,MPoint3* pColor, float fAlphaDet);
		bool IsInitalRiver();
		bool ExportRiverTo3DS();
		void ClearRiver();

		//-------------------------------------------------------------------------
		// 灯光烘焙到地形顶点色相关函数
		// 预览地形光照效果
		void PreviewLights(String* lightList[]);
		// 烘焙灯光到顶点色
		void BakeLightsToTerrain(String* lightList[], bool bMultiply);
		// 填充地形顶点色
		void FloodTerrainVertexColor(float fR, float fG, float fB);

		// 导出场景草图，地形信息给任务编辑器 bDynamicShadow - 是否带动态阴影
		void ExportTerrainInfoToMissionEditor(String* strPath, bool bDynamicShaodow);

		// 使当前选择物件不受光影响
		void BreakLightConnections();

		// add [8/10/2009 hemeng]
		// 添加由palette批量生成物件的功能 [8/10/2009 hemeng]
		// 取源palette所有entity的位置生成新palette物件
		bool BatchCreateByPalette(MEntity* pmSrcEntity, MEntity* pmDesEntity, float fZOffset);
		// end [8/10/2009 hemeng]

		// 设置是否预览物件的光照贴图
		void SetPreviewEntityLightMap(bool bPreview);

		// 设置包围盒 [9/15/2009 hemeng]
		void SetAABBScale(String* strEntityName,float fScale);

		// 物件随机 [10/27/2009 hemeng]
		void SetEntityRandomEnabled(bool bEnable);
		bool GetEntityRandomEnabled();

		// 检查场景中所有灯光影响列表。如果列表中某物件在场景中没有，将其从列表中删除
		//  [11/13/2009 shiyazheng]
		void VerifyLightsAffectList();

		// 检查灯光合法性
		bool ValifySceneLights();
		

		// 设置最大最小缩放值 [12/1/2009 hemeng]
		void SetEntityRandomSize(float fDown, float fUpper);

		//  [12/14/2009 hemeng]
		bool	GetRectSelectState()	{return m_bRectSel;};
		void	SetRectSelectState(bool bRectSel);	

		int		GetRectSelectPrec()	{return m_iRectPrec;};
		void	SetRectSelectPrec(int iPrec);


		// 设置高度检查颜色类型 [2/1/2010 hemeng]
		void SetTerrainHeightCheckColorType(int iType);
		int	 GetTerrainHeightCheckColorType();


    protected:
        virtual void Do_Dispose(bool bDisposing);

    private:
        void UpdateEffects(MEntity* pmEntity);

        void OnEntityComponentAdded(MEntity* pmEntity, 
            MComponent* pmComponent);
        void OnEntityComponentRemoved(MEntity* pmEntity, 
            MComponent* pmComponent);
        void OnEntityRemovedFromScene(MScene* pmScene, MEntity* pmEntity, bool bResolveDependencies);
        void OnEntityPropertyChanged(MEntity* pmEntity,
            String* strPropertyName, unsigned int uiPropertyIndex,
            bool bInBatch);

        void ResolveAddedComponentDependencies(MEntity* pmEntity,
            MEntity* pmMasterEntity, MComponent* pmAddedComponent);
        void ResolveRemovedComponentDependencies(MEntity* pmEntity,
            MEntity* pmMasterEntity, MComponent* pmRemovedComponent);
        void ResolveRemovedEntityDependencies(MEntity* pmEntity,
            MEntity* pmRemovedEntity);
        void ResolveRemovedEntityDependencies(MSelectionSet* pmSelectionSet,
            MEntity* pmRemovedEntity);
        void RefreshEntityReferences(MEntity* pmChangedEntity);

		//void SetEntityChunkID(MEntity* pmEntity);	// 根据 entity 的位置获取所属 chunkID 并设置给 entity 的 ChunkID property
		//081229 add by 和萌
		// 更新palate属性后更新场景中所有entities的对应属性
		void UpdateEntityProperty(MEntity* pmEntity,NiFixedString strString);
		
		// 创建地形时初始化3盏等
		bool AddInitialLights();


		// 从物件映射地表属性 [9/25/2009 hemeng]
		void BackObjToTerrainProperty(NiGeometry* pkGeometry, int iProperty);

		// 随机设置放入entity的旋转缩放 [10/27/2009 hemeng]
		void SetEntityRandomProperty(MEntity* pmEntity);	

		// 物件是否为sun的不影响阴影物件,bCheckCaster=true则检查是否为产生阴影，bCheckCaster=false则检查是否为不接受阴影 [11/19/2009 hemeng]
		bool IsUnaffectedShadow(NiEntityInterface* pkEntityInterface,bool bCheckCaster);

        NiScene* m_pkScene;
        NiDefaultErrorHandler* m_pkErrors;
        bool m_bDirtyBit;

		bool m_bCollisionWriteOpen;		

		CTerrain *m_pkTerrain;

		bool m_bSnapToTerrain;	// 物件是否吸附到地面

		bool m_bRenderSceneObject;	// 是否渲染场景物件
		bool m_bRenderNPCCerator;	// 是否渲染 npc creator
		bool m_bRenderOverTopVertex;//是否渲染场景中的过高点
		bool m_bRenderRegion;		//是否渲染区域

		int m_nGlobalRegionType;	//区域的类型
		int m_nRegionOperType;	//区域操作类型
		int m_nCurrentRegionID;	//当前操作的区域ID

		int m_iTerrainOptimizeSensitivity;	// 地形优化敏感度

        static ArrayList* ms_pmScenes = new ArrayList();

        static String* ms_strTranslationPropertyName = "Translation";
        static String* ms_strApplyTransformsPropertyName = "Apply Transforms";

        __property static IMessageService* get_MessageService();
        static IMessageService* ms_pmMessageService;

		// 测试部分
		//////////////////////////////////////////////////////////
		//CLayerFog*			m_pkLayerFog;	// 层雾，测使用
		NiTriShape*			m_pkFogGeom;	// 层雾表面几何体
		////////////////////////////////////////////////////////////////////////////
		// shadow
		CShadowMapProcess	*m_pShadowMapProcess;

		bool				m_bBakeShadow;

		SceneCore::CAOPerVertexShadowGenerator*		m_pAOShadowGenerator;	// ambient occlusion 生成器
		SceneCore::CPerEntityShadowGenerator*		m_pProjectionShadowGenerator;	// projection shadow 生成器
		CSimpleFog*									m_pSimpleFog;	// simple layer fog 生成器
		bool										m_bPreviewSimpleFog;	// 是否预览 simple fog

		//////////////////////////////////////////////////////////////////////////
		//add by 和萌
		bool			m_bRenderRiver;
		bool			m_bRandomAddEntity;
		//  [12/1/2009 hemeng]
		float			m_fRandomSizeDown;	// 随机大小最低值 [12/1/2009 hemeng]
		float			m_fRandowSizeUpper;	// 随机大小最大值 [12/1/2009 hemeng]
		//-----------------------------------------------------------------------------
		
		//  [11/17/2009 shiyazheng]
		bool			m_bNeedRefreshTerrainLights;	// 是否需要更新地形的灯光
		bool			m_bNeedRecacheUnaffactedCaster;	// 是否需要重新构建不投射列表 cache
		bool			m_bNeedRecacheUnaffactedReceiver;	// 是否需要重新构建不接受列表 cache
		bool			m_bNeedRefreshTerrainShadow;	// 是否需要更新阴影

		//  [12/14/2009 hemeng]
		bool			m_bRectSel;			// 是否框选状态 [12/14/2009 hemeng]
		int				m_iRectPrec;		// 框选精度 [12/14/2009 hemeng]

		// 地形高度检查标志颜色类型 [2/1/2010 hemeng]
		int				m_iTerrHeightCheckColorType;

    };
}}}}

