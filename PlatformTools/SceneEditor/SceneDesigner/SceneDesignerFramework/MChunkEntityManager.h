﻿
#pragma once

#include "MDisposable.h"
#include "MScene.h"
#include "ISettingsService.h"
#include "ICommandService.h"
#include "ServiceProvider.h"
#include "MViewport.h"
#include "MPoint3.h"
#include "MMatrix3.h"
#include "UICommandHandlerAttribute.h"
#include "MProxyManager.h"

using namespace System::Collections;
using namespace System::Collections::Generic;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
{
	public __gc class MChunkEntityManager : public MDisposable
	{

	public:
		// 添加一个 Entity,自动判断其所在的 chunk并添加到相应 list 中
		void AddEntity(MEntity* pmEntity);

		// 移除一个 Entity
		void RemoveEntity(MEntity* pmEntity);

		// 当 Entity 发生移动可能会跨 chunk，将其从 list 中 remove,再添加
		void EntityMoved(MEntity* pmEntity);

		// 获取 参数列表中 所有chunk 对应的 entity
		MEntity* GetEntity(vector<int>* chunksID)[];
		MEntity* GetEntity(int iChunksID)[];

		// 获取静态 chunks 列表中的所有 entity
		MEntity* GetStaticEntityList()[];

		__value enum EntityCullMode
		{
			ECM_CULL_NONE,	// 不剔除
			ECM_STATIC,	// 静态
			ECM_CAMERA_CHUNK,	// 动态，显示 camera 所在 chunk 物件
			ECM_CAMERA_AROUND,	// 动态，显示 camera 周围 9 个 chunk 物件
		};

		// properties
		EntityCullMode GetEntityCullMode();
		void SetEntityCullMode(EntityCullMode mode);
		__property ArrayList* get_StaticChunkDisplayList();
		__property void set_StaticChunkDisplayList(ArrayList* staticChunkList);

	protected:
		virtual void Do_Dispose(bool bDisposing);

		// Singleton members.
	public:
		static void Init(int iNumChunksX, int iNumChunksY);
		static void Shutdown();
		static bool InstanceIsValid();
		__property static MChunkEntityManager* get_Instance();
	private:
		static MChunkEntityManager* ms_pmThis = NULL;
		MChunkEntityManager(int iNumChunksX, int iNumChunksY);

		int m_iNumChunkX;	// 目标地形尺寸
		int m_iNumChunkY;

		ArrayList* m_ChunksEntityList;	// 下标为 chunkID. list 每一个元素也是 ArrayList*.保存该 chunk 对应的所有 entity 
		ArrayList* m_StaticChunksDisplayList;	// 当前显示哪些
		EntityCullMode m_EntityCullMode;	// 
	};
}}}}

