﻿#include "SceneDesignerFrameworkPCH.h"

#include "MNiProperty.h"
#include "MFramework.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
MNiProperty::MNiProperty(NiProperty* pkProperty, NiAVObject* pkTagAVObj)
{
	m_pkTagProperty = pkProperty;
	m_pkTagAVObject = pkTagAVObj;
}
//---------------------------------------------------------------------------
void MNiProperty::UpdateProperties()
{
	if (m_pkTagAVObject != NULL)
	{
		m_pkTagAVObject->UpdateProperties();
		MFramework::Instance->Update();
	}
}
//---------------------------------------------------------------------------
void MNiProperty::RemoveProperty()
{
	if (m_pkTagAVObject && m_pkTagProperty)
	{
		m_pkTagAVObject->RemoveProperty(m_pkTagProperty->Type());
		m_pkTagAVObject->UpdateProperties();

		MFramework::Instance->Update();
	}
}