﻿
#pragma once

#include "MDisposable.h"
#include "MScene.h"
#include "ISettingsService.h"
#include "ICommandService.h"
#include "ServiceProvider.h"
#include "MViewport.h"
#include "MPoint3.h"
#include "MMatrix3.h"
#include "UICommandHandlerAttribute.h"
#include "MProxyManager.h"

using namespace System;
using namespace System::Xml;
using namespace System::IO;
using namespace System::Collections;
using namespace System::Collections::Generic;

// 地形的纹理层数
#define NUM_TERRAIN_TEXTURES		4
// 城市的等级上限
#define NUM_CITY_LEVELS				16

// 管理城市级别相关内容,包括地表纹理,物件表现等
namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		public __gc class MCityLevelManager
		{
		public:
			__property static MCityLevelManager* get_Instance();

			// 城市等级配置
			__gc class MCityLevelSetting
			{
			public:
				MCityLevelSetting(int iLevel);

				// Xml IO
				static MCityLevelSetting* FromXml(XmlElement* elmtLevel);
				XmlElement* ToXml(XmlDocument* xmlDoc);

				int m_iLevel;						// 对应的等级
				String* m_arrTerrainTextures[];		// max 4, String*
			};

			// 获取城市等级数量
			int GetNumCityLevels() {return NUM_CITY_LEVELS;}

			// 获取某个等级的城市配置
			MCityLevelSetting* GetCityLevel(int iLevel);
			// 
			void SetCityLevel(MCityLevelSetting* pLevelSetting);

			// 清除所有 level
			void ClearAllLevels();
			// 添加一个新 level setting
			//MCityLevelSetting* AddCityLevel(int iLevel);

			// 应用第 iLevel 
			void ApplyLevel(int iLevel);

			// Xml IO
			void SaveToXml(String* strFileName);
			void LoadFromXml(String* strFileName);

			// Export to SceneInfo
			XmlElement* ToXml(XmlDocument* xmlDoc);

			static const String* CITY_LEVEL_SAVING_FILE = "LevelSettings.xml";

		protected:
			MCityLevelManager();
			static MCityLevelManager* ms_Instance;	// 唯一实例
			MCityLevelSetting* m_arrLevels[];		// 城市等级列表
		};
	}}}}

