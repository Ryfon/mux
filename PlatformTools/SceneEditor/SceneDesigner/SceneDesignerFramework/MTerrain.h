﻿
#pragma once

#include "MDisposable.h"
#include "MEntity.h"
#include "MSelectionSet.h"
#include "MRenderingContext.h"
#include "MPoint3.h"
#include "IMessageService.h"
#include "ICommandService.h"

#include <vcclr.h>
#pragma unmanaged
#include <NiScene.h>
#include "TerrainGridBaseRegion.h"
#include "AdvanceWater.h"
#include "SprayGenerator.h"
#include "SnapTool.h"
#pragma managed

using namespace System::IO;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;

using namespace SceneCore;


namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
{
	// 主要用于 界面 和 CTerrain 类的通信,减少 MScene 中的代码量.
	public __gc class MTerrain : public MDisposable
	{

	public:
		~MTerrain(void);

		// 地形 paint 工具 类型枚举
		__value enum EPainterType
		{
			PT_TERRAIN_VERTEX = 0,	// 地形顶点高度 
			PT_TERRAIN_MATERIAL,	// 地表纹理,顶点色
			PT_TERRAIN_PROPERTY,	// 地表属性
			PT_TERRAIN_GRID_AREA,	// 以 grid 为单位的区域
			PT_REMOVE_ENTITY,		// 删除 Entity
			//PT_WATER_SURFACE,		// add by 和萌 刷水体
			PT_MAX,						
		};	


		//------------------------------------------------------
		//与 CTerrainGridBaseRegionManager 交互函数 
		//------------------------------------------------------
		// 获取所有 Grid base area 的名称
		String* GetGridBaseRegionList()[];

		// 判断是否存在该名称的 region
		bool HasGridBaseRegion(String* strName);

		// 添加一个 Grid Base Region
		bool AddGridBaseRegion(String* strName);

		// 设置/获取当前被编辑的区域
		void SetOnEditGridBaseRegion(String* strName);
		String* GetOnEditGridBaseRegion();

		// 获取当前被编辑区域 grid 数量
		int GetOnEditRegionGridCount();

		// 按名称删除一个区域
		void RemoveGridBaseRegionRegion(String* strName);

		// 预览区域生长模型
		void PreviewGridBaseRegionGenerateModel(ArrayList* eneitiyList, ArrayList* rateList, 
			float fRandRotaBegin, float fRandRotaEnd, float fRandScaleBegin, float fRandScaleEnd);

		// 将预览场景中的模型应用到正式场景中.
		void ApplyGridBaseRegionGeneratedModel();

		// 渲染预览区域生长模型
		MEntity* GetPreviewSceneEntities()[];

		// 获取某地形位置的 chunk ID
		int GetTerrainChunkIndex(NiPoint3& kPos);

		// 设置地形正被编辑 chunk ID
		void SetTerrainOnEditChunk(int iChunk);

		void Update(float fTime);

		// 测试 AdvanceWater
		void TestAdvanceWater();
		void BuildVisibleSet(NiEntityRenderingContextPtr	pkRenderingCondtex);

		// 渲染预览排浪
		void RenderSprayImmediate(NiRenderer* pkRenderer);

		// 生成排浪
		// fSeaLevel - 海平面高度
		// fWidth - 排浪宽度
		// fAlpha - UV 动画速度
		// fTx, fVx - 动画关键帧，　v 偏移值
		// strTexFilename - 排浪纹理名称
		void GenerateSprayGeometry(float fSeaLevel, float fWidth, float fAlpha, float fT0, float fV0, 
			float fT1, float fV1, float fT2, float fV2, float fT3, float fV3, String* strTexFilename);

		// 导出完整的水体
		void ExportAdvanceWater(String* strNifFilename);

		// 导出排浪 nif 文件
		bool ExportSprayNif(String* strSprayNifFilename);

		// 清除排浪预览
		void ClearSprayPreview();

		// 设置顶点 alpha 值
		//void SetTerrainAlpha(float fAlpha);
		// 修改为设置地形隐藏 [11/24/2009 hemeng]
		void SetTerrainHidden(bool bHidden);

		// 将当前区域的属性设为 iProp
		void SetTerrainProperty(int iProp);

		// 自动填充地形碰撞块中的空洞 iNumHoleBlock - 多少个grid的连续区域被认为是 hole
		void FillTerrainCollisionHole(int iNumHoleBlock);

		//////////////////////////////////////////////////////////////////////////
		//add by 和萌 
		//导出地形中的Grid属性
		void ExportGridProperty();
		void ImportGridProperty();

		// 导出高度图 [7/14/2009 hemeng]
		bool ExportHeightMap(float __gc * iLow,float __gc * iHeigh);
		// 最低到最高区间 [7/14/2009 hemeng]
		bool ImportHeightMap(float iLow, float iHeigh);

		// add [5/25/2009 hemeng]
		// 计算地表属性占整个地表的百分比
		int GetGridPropertyPercent(int iGridProperty);

		//由纹理生成地表属性
		void CreateGridPropertyByMaterial();

		void CameraGoToPos(int iPosX, int iPosY);

		CSnapTool* GetSnapTool()	{return m_pSnapTool;};

		bool SetSnapSrcEntity(MEntity* pkEntity);
		bool SetSnapDesEntity(MEntity* pkEntity);
		bool ClearSnapEntity();

		// 应用顶点色纹理到地形
		//void ApplyVertexColorTexture(String* strFileName, float fLum, bool bFixLum, float fIncMultiple);

		// 预览地形光照贴图
		void SetPreviewTerrainLightMap(bool bPreview);
		
		// 将地形保存到 nif
		void SaveTerrainToNif(String* strFileName);
		//------------------------------------------------------
		static bool ExportClusterNif(MEntity* pEntities[]);

		// 获取地形尺寸
		int GetNumChunkX();
		int GetNumChunkY();

		/// 将纹理写到地形特定 chunk 的 blend texture 的 alpha 通道
		bool ApplyShadowTexture(String* strTexName, int iChunkID);

		/// 将纹理颜色值根据 UV 写入地形特定 chunk 顶点色中
		bool ApplyVertexColorTexture(String* strTexName, const int iChunkID, const float fLum, const bool bFixLum, const float fIncMultiple);

		/// 设置阴影预览 
		void SetShadowPreview(bool bStaticShadow, bool bDynamicShadow);
		__property bool get_DynamicShadow() {return m_bDynamicShadow;}
		__property void set_DynamicShadow(bool bDynamic) {m_bDynamicShadow = bDynamic;}
		__property bool get_StaticShadow() {return m_bStaticShadow;}
		__property void set_StaticShadow(bool bStaticShadow) {m_bStaticShadow = bStaticShadow;}

		/// 设置地形 ambient 系数
		__property float get_TerrainAmbient();
		__property void set_TerrainAmbient(float fAmbient);

		/// 创建/加载新场景后调用
		void OnNewScene();

	protected:
		// MDisposable members.
		virtual void Do_Dispose(bool bDisposing);

		// 在一个区域内按某几率生长 entity, 
		void _GrowEntityInRange(CTerrainGridBaseRegion* pRegion, MEntity* pmEntity, float fRate, 
				float fRandRotaBegin, float fRandRotaEnd, float fRandScaleBegin, float fRandScaleEnd);

		// 递归获取相邻所有可行走 grids
		// openList - 待搜索列表
		// pGrids - grid 列表
		// pGridMark - grid 状态标记
		// iNumGridX - grid 数量
		// iNumGridY - 
		// adjacent - 相邻 grid id.
		void _GetAdjacentWalkableGrids(std::set<int>& openList, const CTerrain::stGrid* pGrids, int* pGridMark, 
							const int iNumGridX, const int iNumGridY, std::set<int>& adjacent, const int iNumHoleBlock);

		// Singleton members.
	public:
		static void Init();
		static void Shutdown();
		static bool InstanceIsValid();
		__property static MTerrain* get_Instance();
		__property CTerrain* get_Terrain();
		
	private:
		static MTerrain*	ms_pmThis = NULL;
		MTerrain();
		//NiScene*			m_pkPreviewScene;	// 用于预览的场景
		ArrayList*			m_pPreviewEntityList;
		CSprayGenerator*	m_pSprayGenerator;	// 排浪生成器
		CAdvanceWater*		m_testWater;
		CSnapTool*			m_pSnapTool;
		bool				m_bStaticShadow;	// 是否使用静态阴影
		bool				m_bDynamicShadow;	// 是否适用动态阴影
	};
}}}}

