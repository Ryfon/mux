﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#pragma once

#include "MDisposable.h"
#include "MScene.h"
#include "ISettingsService.h"
#include "IOptionsService.h"
#include "MProxyManager.h"
// 090119 add by 和萌
#include "MChunkEntityManager.h"

// 090310 add by 和萌
// fFinalyIntensity = 0.2 用于限制灯光范围
#define INVERSE_FINAL_INTENSITY 10 // (1 / fFinalIntensity)

#define MAX_AFFECT_POINTLIGHT_COUNT 3		// 最大物件影响点光源个数 [3/29/2010 hemeng]
#define MAX_AFFECT_POINTLIGHT_DIST	3		// 最大可接受灯光距离 [3/29/2010 hemeng]

using namespace System::Collections;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI
    ::StandardServices;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace Framework
{
    public __gc class MLightManager : public MDisposable, public IProxyHandler
    {
    private public:
        void Startup();

    public:
        static bool EntityIsLight(MEntity* pmEntity);
        MEntity* GetSceneLights()[];

		//  [10/29/2009 hemeng]
		int GetSceneLightsCount();

        void AddEntityToDefaultLights(MEntity* pmEntity);
        void AddEntitiesToDefaultLights(MEntity* amEntities[]);
        void RemoveEntityFromDefaultLights(MEntity* pmEntity);
        void RemoveEntitiesFromDefaultLights(MEntity* amEntities[]);

        void Update(float fTime);

        // From IProxyHandler
        virtual MEntity* GetMasterProxyEntity(MEntity* pmEntity);

		// get light by name
		NiLightPtr	GetNiLight(String* strLightName);
		NiEntityInterface* GetLightEntity(String* strLightName);
		MEntity* GetLight(String* strLightName);

		// 优化 Entity 的灯光影响, pmEntity 必须是已添加到场景中的灯光
		void OptimizeAffectedLights(MEntity* pmEntity);

		// 获取影响当前选择 Entity 的所有灯光
		ArrayList* GetAffectLights(MEntity* pmEntity);

		// 设置某灯光对某物件的影响关系
		void SetAffect(String* strLight, MEntity* pmEntity, bool bAffect);

		// 010119 add by hemeng
		// 计算指定灯光到指定物件之间的距离
		bool IsEntityInLightBound(MEntity* pmLight,MEntity* pmEntity);

		// 更新灯光影响物件
		void UpdateLightAffectBound(MEntity* pmLight,NiFixedString strPropertyName);

    private:
        __property bool get_UseDefaultLights();
        __property void set_UseDefaultLights(bool bUseDefaultLights);
        void CreateLightProxyHandler();

        void OnSceneClosing(MScene* pmScene);
        void OnNewSceneLoaded(MScene* pmScene);
        void OnEntityAddedToScene(MScene* pmScene, MEntity* pmEntity);
        void OnEntityRemovedFromScene(MScene* pmScene, MEntity* pmEntity, bool bResolveDependencies);
        void OnEntityPropertyChanged(MEntity* pmEntity,
            String* strPropertyName, unsigned int uiPropertyIndex,
            bool bInBatch);
        void OnEntityComponentAdded(MEntity* pmEntity,
            MComponent* pmComponent);
        void OnEntityComponentRemoved(MEntity* pmEntity,
            MComponent* pmComponent);

        void RefreshLightAffectedEntities();

        void CreateDefaultLights();
        void DisposeDefaultLights();
        void ClearDefaultLightAffectedEntities();

        void RegisterForDefaultLightsSetting();
        void RegisterForAddEntitiesToLightsSetting();
        void OnDefaultLightsSettingChanged(Object* pmSender,
            SettingChangedEventArgs* pmEventArgs);
        void OnAddEntitiesToLightsSettingChanged(Object* pmSender,
            SettingChangedEventArgs* pmEventArgs);

        void CollectSceneLights(MScene* pmScene);
        void ProcessAddedEntity(MScene* pmScene, MEntity* pmEntity);
        void ProcessRemovedEntity(MScene* pmScene, MEntity* pmEntity);

		// 090309 add by 和萌
		// 更新灯光参数，返回更新后的灯光scale
		void _UpdateLightParam(MEntity* pmLight,NiFixedString strPropertyName);
		// 090310 add by 和萌
		// 更新灯光Dimmer或scale
		float _ModifyScale(MEntity* pmLight,NiFixedString strPropertyName);
		// 更新灯光衰减值
		float _ModifyAtten(MEntity* pmLight,NiFixedString strPropertyName);
		// 更新影响物件的灯光，仅用于非灯光，返回更新后的灯光scale
		void _UpdateEntityAffectLight(MEntity* pmEntity,NiFixedString strPropertyName);

        ArrayList* m_pmLights;
        MEntity* m_pmMasterProxy;
        MEntity* m_amDefaultLights[];
        bool m_bUseDefaultLights;
        bool m_bAddEntitiesToLights;
        bool m_bUpdateDefaultLights;

        static String* ms_strAffectedEntitiesName = "Affected Entities";
        static String* ms_strCastShadowsName = "Cast Shadows";

        static String* ms_strOptionCategoryName = "Light Settings";

        static String* ms_strDefaultLightsSettingName = "Use Default Lights";
        static String* ms_strDefaultLightsOptionName =
            String::Concat(ms_strOptionCategoryName, ".",
            ms_strDefaultLightsSettingName);

        static String* ms_strAddEntitiesToLightsSettingName =
            "Automatically Add Entities to Lights";
        static String* ms_strAddEntitiesToLightsOptionName =
            String::Concat(ms_strOptionCategoryName, ".",
            ms_strAddEntitiesToLightsSettingName);

        static ISettingsService* ms_pmSettingsService;
        __property static ISettingsService* get_SettingsService();

        static IOptionsService* ms_pmOptionsService;
        __property static IOptionsService* get_OptionsService();

    // MDisposable members.
    protected:
        virtual void Do_Dispose(bool bDisposing);

    // Singleton members.
    public:
        static void Init();
        static void Shutdown();
        static bool InstanceIsValid();
        __property static MLightManager* get_Instance();
    private:
        static MLightManager* ms_pmThis = NULL;
        MLightManager();
    };
}}}}
