﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MFramework.h"
#include "NiLightProxyComponent.h"
#include "ServiceProvider.h"
#include "MPoint3.h"
#include "MUtility.h"
#include "MFolderLocation.h"
#include <NiMemTracker.h>
#include <NiStandardAllocator.h>
#include "MChunkEntityManager.h"
#include "MTemplatePreviewer.h"
#include "MGlobalSetting.h"
#include "MTerrain.h"
#include "MCityLevelManager.h"
#include "MTimeStamp.h"

// add [6/10/2009 hemeng
#include <sstream>

#pragma unmanaged
#include "WaterManager.h"
#include "WaterEntityComponent.h"
#include "WaterRenderView.h"
#include "DepthAlphaEffectManager.h"
#include "IDepthAlphaEffect.h"
#include "LayerFog.h"
#include "Terrain.h"
//#include "RiverManager.h"
#include "TerrainGridBaseRegionManager.h"
#include <NiParticleMetrics.h>
#include <NiDx9RendererMetrics.h>
#include <NiMetricsOutput.h>
#include <NiVisualTrackerOutput.h>


#pragma managed


//The following is related to a bug in VC7.1/.Net CLR 1.1
// see http://support.microsoft.com/?id=814472
#if _MSC_VER == 1310
#pragma unmanaged
#include <windows.h>
#include <_vcclrit.h>
#pragma managed
#endif

//#define _DEBUG

using namespace System::Windows::Forms;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
//---------------------------------------------------------------------------
void MFramework::Init()
{
#if _MSC_VER == 1310
    __crt_dll_initialize();
#endif
    if (ms_pmThis == NULL)
    {
        ms_pmThis = new MFramework();
		//从NPCInfo.xml文件生成Pal
		// NPC已转移至任务编辑器制作 [7/21/2009 hemeng]
		//CTerrain::CreateNpcCreatorPal();
		//CTerrain::LoadNpcCreator();
    }

	if (!ms_pmThis->InitPointLightBoundFile())
	{
		NiMessageBox("初始化点光源范围图标失败！","Error",0);
	}

	if (!ms_pmThis->InitChunkNumSigns())
	{
		NiMessageBox("初始化点chunk编号图标失败！","Erro",0);
	}

}
//---------------------------------------------------------------------------
void MFramework::Shutdown()
{
	if (ms_pmThis->m_pmLightBound != NULL)
	{
		delete ms_pmThis->m_pmLightBound;
		ms_pmThis->m_pmLightBound = NULL;
	}

	//  [7/13/2009 hemeng]
	for (int i = 0; i < 10; i++)
	{
		if (ms_pmThis->m_pmNumSigns[i])
		{
			NiDelete ms_pmThis->m_pmNumSigns[i];
			ms_pmThis->m_pmNumSigns[i] = NULL;
		}
	}	

    if (ms_pmThis != NULL)
    {
		//CWaterManager::GetInstance()->Destory();
        ms_pmThis->Dispose();
        ms_pmThis = NULL;
    }
#if _MSC_VER == 1310
    __crt_dll_terminate();
#endif
}
//---------------------------------------------------------------------------
bool MFramework::InstanceIsValid()
{
    return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
MFramework* MFramework::get_Instance()
{
    return ms_pmThis;
}
//---------------------------------------------------------------------------
MFramework::MFramework() : m_strImageSubfolder(String::Empty),
    m_bDoNotUpdate(false), m_bInitSuccessful(false), m_uiLongOperationCount(0),
    m_strCurrentFilename(String::Empty), m_pkCurrentPos(NULL),
	m_fFrameRate(0.0f), m_fCullTime(0.0f), m_fUpdateTime(0.0f), m_fRenderTime(0.0f),
	m_bRenderWindow(true)
{
    NiInitOptions* pkInitOptions = NiExternalNew NiInitOptions(
#ifdef NI_MEMORY_DEBUGGER
        NiExternalNew NiMemTracker(NiExternalNew NiStandardAllocator(), false)
#else
        NiExternalNew NiStandardAllocator()
#endif
        );

    NiInit(pkInitOptions);
    m_bInitSuccessful = true;

	// initial num sgins [7/13/2009 hemeng]	
	for (int i = 0; i < 10; i++)
	{
		m_pmNumSigns[i] = NULL;
	}

	m_pkRectSelTexture = NULL;

    // Initialize the shadow manager. It is initially set to be inactive.
    // Rendering modes that draw shadows are responsible for activating it
    // and then deactivating when done. Rendering modes that do not draw
    // shadows need not take any action.
    NiShadowManager::Initialize();
    NiShadowManager::SetActive(false);
    NiShadowManager::SetCullingProcess(NiNew NiCullingProcess(NULL));

    NiProxyComponent::_SDMInit();
    NiLightProxyComponent::_SDMInit();
    MViewport::_SDMInit();
    MEntity::_SDMInit();

    MSceneFactory::Init();
    MEntityFactory::Init();
    MSelectionSetFactory::Init();
    MComponentFactory::Init();

    MEventManager::Init();
    MRenderer::Init();
    MTimeManager::Init();
    MCameraManager::Init();
    MViewportManager::Init();
    MPaletteManager::Init();
    MProxyManager::Init();
    MLightManager::Init();
    MBoundManager::Init();

    // Ensure that all registered assets clone from a pristine copy
    NiTFactory<NiExternalAssetHandler*>* pkAssetFactory = 
        NiFactories::GetAssetFactory();
    NiTMapIterator kIter = pkAssetFactory->GetFirstPos();
    while (kIter)
    {
        NiFixedString kKey;
        pkAssetFactory->GetNext(kIter, kKey);

        NiExternalAssetHandler* pkHandler = 
            pkAssetFactory->GetPersistent(kKey);

        NIVERIFY(pkHandler->SetCloneFromPristine(true));
    }

    m_pmPickUtility = new MPickUtility();

    m_pkAssetManager = NiNew NiExternalAssetManager(
        NiFactories::GetAssetFactory());

    // To make clear that error handling, if used, will be set explicitly.
    m_pkAssetManager->SetErrorHandler(NULL);

    MInitRefObject(m_pkAssetManager);

	NiFile::SetFileCreateFunc(TextureFileCreateFunc);

	MScene* pmScene = NewScene(10);
	InitNewScene(pmScene, false,false);

	// 初始化 Water Manager
	//CWaterManager* pWaterMgr = CWaterManager::GetInstance();
	//if (!pWaterMgr->Initialized())
	//{
	//	CWaterRenderViewPtr pWaterRenderView = NiNew CWaterRenderView(
	//		NiNew NiCamera,
	//		NiNew NiCullingProcess(NULL), false);

	//	pWaterMgr->Init(pWaterRenderView);
	//}

	//m_pTextureCombination = new MTextureCombination();

	// Create the visual tracker render click.
	//NiRect<float> kRect;
	//kRect.m_left=0.05f; 
	//kRect.m_right = 0.95f;
	//kRect.m_top = 0.5f;
	//kRect.m_bottom = 0.95f;
	//m_pkVisualTracker = NiNew NiVisualTracker(1000.0f, 5, kRect, "Scene Metric");
	//MInitRefObject(m_pkVisualTracker);
	//CreateOutputModules();

    this->TimeManager->Enabled = true;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void MFramework::Do_Dispose(bool bDisposing)
{
    if (!m_bInitSuccessful)
    {
        return;
    }

    MDisposeRefObject(m_pkAssetManager);

    if (bDisposing)
    {

#ifdef _DEBUG
		MTimeStamp::Instance->SaveToFile();
		MTimeManager::Shutdown();
#endif

		MProxyManager::Shutdown();

        if (m_pmPickUtility != NULL)
        {
            m_pmPickUtility->Dispose();
            m_pmPickUtility = NULL;
        }
		//CRiverManager::Do_Dispos();
		MTemplatePreviewer::Shutdown();
        MLightManager::Shutdown();
        MPaletteManager::Shutdown();
        MViewportManager::Shutdown();
        MCameraManager::Shutdown();
        MTimeManager::Shutdown();
        MBoundManager::Shutdown();
		MGlobalSetting::Shutdown();

        // The factories are shut down after the managers because some of the
        // managers access them in their Dispose functions. The factories must
        // also be shutdown in the order listed here because some factories
        // depend on other factories.
        MSceneFactory::Shutdown();
        MEntityFactory::Shutdown();
        MSelectionSetFactory::Shutdown();
        MComponentFactory::Shutdown();
	
        // The shadow manager must be shut down prior to the renderer being
        // destroyed.
        NiShadowManager::Shutdown();

		CWaterManager::GetInstance()->Destory();
		CDepthAlphaEffectManager::Destory();
		CTerrainGridBaseRegionManager::GetInstance()->Dispose();
		MTerrain::Shutdown();

        MRenderer::Shutdown();
        MEventManager::Shutdown();
    }

    NiLightProxyComponent::_SDMShutdown();
    NiProxyComponent::_SDMShutdown();
    MEntity::_SDMShutdown();
    MViewport::_SDMShutdown();

    const NiInitOptions* pkInitOptions = NiStaticDataManager
        ::GetInitOptions();
    NiShutdown();
    NiAllocator* pkAllocator = pkInitOptions->GetAllocator();
    NiExternalDelete pkInitOptions;
    NiExternalDelete pkAllocator;

	
}
//---------------------------------------------------------------------------
void MFramework::Startup()
{
    MVerifyValidInstance;

    IUICommandService* pmCommand = MGetService(IUICommandService);
    pmCommand->BindCommands(MViewportManager::Instance);
    pmCommand->BindCommands(MCameraManager::Instance);

    this->ProxyManager->Startup();
    this->LightManager->Startup();
    this->CameraManager->Startup();
    this->ViewportManager->Startup();
    this->BoundManager->Startup();

    RegisterSettingsAndOptions();


}
//---------------------------------------------------------------------------
void MFramework::RegisterSettingsAndOptions()
{
    MVerifyValidInstance;

    SettingsService->RegisterSettingsObject(ms_strImageSubfolderSettingName,
        new MFolderLocation(m_strImageSubfolder), SettingsCategory::PerScene);
    SettingsService->SetChangedSettingHandler(ms_strImageSubfolderSettingName,
        SettingsCategory::PerScene, new SettingChangedHandler(this,
        &MFramework::OnImageSubfolderSettingChanged));
    OnImageSubfolderSettingChanged(NULL, NULL);

    OptionsService->AddOption(ms_strImageSubfolderOptionName,
        SettingsCategory::PerScene, ms_strImageSubfolderSettingName);
    OptionsService->SetHelpDescription(ms_strImageSubfolderOptionName,
        "The directory to use when loading external images referenced by NIF "
        "files. A blank path indicates that the same directory as the NIF "
        "file should be used.\n\nNote: this option is stored per scene and "
        "does not take effect until the scene is saved and reloaded.");
}
//---------------------------------------------------------------------------
void MFramework::OnImageSubfolderSettingChanged(Object* pmSender,
    SettingChangedEventArgs* pmEventArgs)
{
    MVerifyValidInstance;

    MFolderLocation* pmImageSubfolder = dynamic_cast<MFolderLocation*>(
        SettingsService->GetSettingsObject(ms_strImageSubfolderSettingName,
        SettingsCategory::PerScene));
    if (pmImageSubfolder != NULL)
    {
        // Store new image subfolder.
        m_strImageSubfolder = pmImageSubfolder->Path;

        // Set platform-specific subdirectory with new image subfolder.
        const char* pcImageSubfolder = MStringToCharPointer(
            m_strImageSubfolder);
        NiDevImageConverter::SetPlatformSpecificSubdirectory(
            pcImageSubfolder);
        MFreeCharPointer(pcImageSubfolder);
    }
}
//---------------------------------------------------------------------------
MSceneFactory* MFramework::get_SceneFactory()
{
    MVerifyValidInstance;

    return MSceneFactory::Instance;
}
//---------------------------------------------------------------------------
MEntityFactory* MFramework::get_EntityFactory()
{
    MVerifyValidInstance;

    return MEntityFactory::Instance;
}
//---------------------------------------------------------------------------
MSelectionSetFactory* MFramework::get_SelectionSetFactory()
{
    MVerifyValidInstance;

    return MSelectionSetFactory::Instance;
}
//---------------------------------------------------------------------------
MComponentFactory* MFramework::get_ComponentFactory()
{
    MVerifyValidInstance;

    return MComponentFactory::Instance;
}
//---------------------------------------------------------------------------
MRenderer* MFramework::get_Renderer()
{
    MVerifyValidInstance;

    return MRenderer::Instance;
}
//---------------------------------------------------------------------------
MEventManager* MFramework::get_EventManager()
{
    MVerifyValidInstance;

    return MEventManager::Instance;
}
//---------------------------------------------------------------------------
MTimeManager* MFramework::get_TimeManager()
{
    MVerifyValidInstance;

    return MTimeManager::Instance;
}
//---------------------------------------------------------------------------
MCameraManager* MFramework::get_CameraManager()
{
    MVerifyValidInstance;

    return MCameraManager::Instance;
}
//---------------------------------------------------------------------------
MViewportManager* MFramework::get_ViewportManager()
{
    MVerifyValidInstance;

    return MViewportManager::Instance;
}
//---------------------------------------------------------------------------
MPaletteManager* MFramework::get_PaletteManager()
{
    MVerifyValidInstance;

    return MPaletteManager::Instance;
}
//---------------------------------------------------------------------------
MProxyManager* MFramework::get_ProxyManager()
{
    MVerifyValidInstance;

    return MProxyManager::Instance;
}
//---------------------------------------------------------------------------
MLightManager* MFramework::get_LightManager()
{
    MVerifyValidInstance;

    return MLightManager::Instance;
}
//---------------------------------------------------------------------------
MBoundManager* MFramework::get_BoundManager()
{
    MVerifyValidInstance;

    return MBoundManager::Instance;
}
//---------------------------------------------------------------------------
String* MFramework::get_AppStartupPath()
{
    MVerifyValidInstance;

    return String::Format(S"{0}{1}",Application::StartupPath, S"\\");
}
//---------------------------------------------------------------------------
MScene* MFramework::get_Scene()
{
    MVerifyValidInstance;

    return m_pmScene;
}
//---------------------------------------------------------------------------
MPickUtility* MFramework::get_PickUtility()
{
    MVerifyValidInstance;

    return m_pmPickUtility;
}
//---------------------------------------------------------------------------
NiExternalAssetManager* MFramework::get_ExternalAssetManager()
{
    MVerifyValidInstance;

    return m_pkAssetManager;
}
//---------------------------------------------------------------------------
String* MFramework::get_ApplicationName()
{
    MVerifyValidInstance;

    return Application::ProductName;
}
//---------------------------------------------------------------------------
String* MFramework::get_ImageSubfolder()
{
    MVerifyValidInstance;

    return m_strImageSubfolder;
}
//---------------------------------------------------------------------------
MScene* MFramework::NewScene(unsigned int uiEntityArraySize)
{
    MVerifyValidInstance;
	ClearOldSceneRes();

    return this->SceneFactory->Get(NiNew NiScene("Main Scene",
        uiEntityArraySize));
}
//---------------------------------------------------------------------------
void MFramework::ClearOldSceneRes()
{
	if (MChunkEntityManager::Instance != NULL)
	{
		MChunkEntityManager::Instance->Shutdown();
	}

	CWaterManager::GetInstance()->ClearWaterInstance();
	//CDepthAlphaEffectManager::GetInstance()->UnregisteAllEffect();
	CWaterRenderViewPtr pkRenderView = CWaterManager::GetInstance()->GetMainWaterRenderView();
	if (pkRenderView)
	{
//		pkRenderView->RemoveAllRenderObject();
	}

}

//081226 add by 和萌
////////////////////////////////////////////////////////////////////////////////
//函数名:CheckFileInDisc
//功能: 检测场景中所有NIF文件是否在硬盘上都存在
//入口参数: MScene* pScene 场景 
//出口参数: bool 都存在返回true，反之返回false
//说明:
////////////////////////////////////////////////////////////////////////////////
void	MFramework::CheckFileInDisc(  MScene* pScene )
{
	MEntity* pEntity[] = pScene->GetEntities();
	
	// 对所有不存在的entity报错 [7/10/2009 hemeng]
	map<string ,int> mapErrorFiles;
	
	for ( int uiIndex = 0; uiIndex < pEntity->Length; uiIndex++ )
	{
		NiFixedString szFilePath;
		
		//如果找到NIF文件路径对应的component
		if ( pEntity[uiIndex]->PropertyInterface->GetPropertyData( "NIF File Path", szFilePath, 0 ) )
		{
			if ( NULL == szFilePath )
			{
				continue;
			}
			
			//查找文件是否存在
			WIN32_FIND_DATA FindFileData;
			HANDLE hFind;

			hFind = FindFirstFile( szFilePath, &FindFileData );

			if ( hFind == INVALID_HANDLE_VALUE )
			{
				string strFilePath = string(szFilePath);
				strFilePath = strFilePath.substr(strFilePath.find_last_of("\\") + 1);
				map<string, int>::iterator it = mapErrorFiles.find(strFilePath);
				if (it == mapErrorFiles.end())
				{
					mapErrorFiles.insert(pair<string, int>(strFilePath,1));
				}
				
				//NiMessageBox( szFilePath, "文件丢失！" );
				//return;
			}
			else
			{
				FindClose( hFind );
			}
		}
		
	}

	char szError[256] = "丢失文件：\n";
	
	for (map<string, int>::iterator it = mapErrorFiles.begin();
		 it != mapErrorFiles.end() ; it ++)
	{
		string strFileName = it->first;
		// 如果拼接字符串失败退出
		if (strlen(szError) >= 240)
		{
			break;
		}
		if(strcat_s(szError,256,strFileName.c_str()))
		{
			break;
		}
		if(strcat_s(szError,256," "))
		{
			break;
		}
	}
	if(mapErrorFiles.size() > 0)
		NiMessageBox( szError, "文件丢失！" );
}

//---------------------------------------------------------------------------
MScene* MFramework::LoadScene(String* strFilename, String* strFormat)
{
    MVerifyValidInstance;

    // Create the appropriate file format handler based on strFormat
    const char* pcFormat = MStringToCharPointer(strFormat);
    NiEntityStreaming* pkEntityStreaming =
        NiFactories::GetStreamingFactory()->GetPersistent(pcFormat);
    MFreeCharPointer(pcFormat);

    // Create error handler.
    NiDefaultErrorHandlerPtr spErrors = NiNew NiDefaultErrorHandler();
    pkEntityStreaming->SetErrorHandler(spErrors);

	// 清除老场景水资源
	ClearOldSceneRes();

	if (MChunkEntityManager::Instance != NULL)
	{
		MChunkEntityManager::Instance->Dispose();
	}
	
    // Load the scene
    const char* pcFilename = MStringToCharPointer(strFilename);
    NiBool bSuccess = pkEntityStreaming->Load(pcFilename);
    MFreeCharPointer(pcFilename);

    // Report errors.
    MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, spErrors);
    pkEntityStreaming->SetErrorHandler(NULL);

    if (!bSuccess)
    {
        return NULL;
    }
    
    NIASSERT(pkEntityStreaming->GetSceneCount() == 1);
    MScene* pmScene = this->SceneFactory->Get(pkEntityStreaming->GetSceneAt(
        0));

	pkEntityStreaming->RemoveAllScenes(); // Because static instance

    return pmScene;
}
//---------------------------------------------------------------------------
void MFramework::InitNewScene(MScene* pmNewScene,bool bClearLightList)
{
    MVerifyValidInstance;

	InitNewScene(pmNewScene, true,bClearLightList);

	if ( pmNewScene )
	{
		CheckFileInDisc( pmNewScene );
		pmNewScene->ValifySceneLights();
	}

}

bool MFramework::CheckSceneLight(MScene* pmScene, bool bClearLightList)
{
	if (pmScene == NULL)
	{
		return false;
	}	

	NiScene* pScene = pmScene->GetNiScene();

	if (pScene == NULL)
	{
		return false;
	}

	int iEntityCount = pScene->GetEntityCount();
	if (iEntityCount > 0)
	{
		for (int k=0; k<iEntityCount; k++)
		{
			NiEntityInterface* pkEntity = pScene->GetEntityAt(k);
			NiFixedString strLightName = pkEntity->GetName();
			
			int iCompCount = pkEntity->GetComponentCount();
			for (int i=0; i<iCompCount; i++)
			{
				NiEntityComponentInterface* pkComp = pkEntity->GetComponentAt(i);
				NiFixedString kInterfaceType = pkComp->GetInterfaceType();
				NiFixedString kName = pkComp->GetName();
				if (kName == "Light")
				{
					NiLightComponent* pkLightComp = (NiLightComponent*)(pkComp);
					if (bClearLightList)
					{
						pkLightComp->RemoveAllAffectedEntities();
					}
					else
					{
						unsigned int uiAffectEntityCount = pkLightComp->GetAffectedEntitiesCount();
						std::vector<NiEntityInterface*> vEntityList;
						for (unsigned int uiIndex = 0 ; uiIndex < uiAffectEntityCount; uiIndex++)
						{
							NiEntityInterface* pkAffectEntity = pkLightComp->GetAffectedEntityAt(uiIndex);
							if (pkAffectEntity != NULL)
							{
								NiGeneralEntity* pkGEntity = (NiGeneralEntity*)pkAffectEntity;
								if (pkGEntity->GetRefCount() == 0)
								{
									vEntityList.push_back(pkAffectEntity);
								}							
							}

						}

						// remove bad entity
						string strErrorFileNames = "灯光名称";
						strErrorFileNames += strLightName;
						strErrorFileNames += "\n 影响物件序号:\n";
						for (vector<NiEntityInterface*>::size_type uiIndex = 0; uiIndex < vEntityList.size(); uiIndex++)
						{
							pkLightComp->RemoveAffectedEntity(vEntityList[uiIndex]);
							stringstream ss;
							ss << uiIndex;
							strErrorFileNames += ss.str();
						}
						if (vEntityList.size() > 0 && !bClearLightList)
						{
							NiMessageBox(strErrorFileNames.c_str(),"Error:错误的影响物件");
						}
					}
				}
			}
		}
	}
	return true;
}

//---------------------------------------------------------------------------
void MFramework::InitNewScene(MScene* pmNewScene, bool bRaiseEvent, bool bClearLightList)
{
    MVerifyValidInstance;

    MAssert(pmNewScene != NULL, "Null scene provided to function!");

    m_bDoNotUpdate = true;
    BeginLongOperation();

    if (m_pmScene != NULL)
    {
        this->EventManager->RaiseSceneClosing(m_pmScene);
        MSceneFactory::Instance->Remove(m_pmScene);
        m_pmScene = NULL;
    }	
	//ClearOldSceneRes();

    m_pkAssetManager->RemoveAll();
    this->TimeManager->ResetTime(0.0f);


    m_pmScene = pmNewScene;

	//{
	//	
	//	// 这里是 debug 测试代码
	//	NiScene* pkScene = m_pmScene->GetNiScene();
	//	for (int i=0; i<pkScene->GetEntityCount(); i++)
	//	{
	//		NiEntityInterface* pkentity = pkScene->GetEntityAt(i);
	//		int iEntityID = 0;

	//		bool bResult = pkentity->GetPropertyData("EntityID", iEntityID, 0);
	//		NiFixedString kStrCategory;
	//		bResult = pkentity->GetPropertyData("Entity Category", kStrCategory, 0);

	//	}
	//}



	MCityLevelManager::Instance->ClearAllLevels();


	// add [6/9/2009 hemeng]
	if (!CheckSceneLight(m_pmScene,bClearLightList))
	{
		NiMessageBox("Error","灯光影响列表检查出错！");
		// return;
	}
    m_pmScene->Update(TimeManager->CurrentTime, m_pkAssetManager);
	//{

	//	// 这里是 debug 测试代码
	//	NiScene* pkScene = m_pmScene->GetNiScene();
	//	for (int i=0; i<pkScene->GetEntityCount(); i++)
	//	{
	//		NiEntityInterface* pkentity = pkScene->GetEntityAt(i);
	//		int iEntityID = 0;

	//		bool bResult = pkentity->GetPropertyData("EntityID", iEntityID, 0);
	//		NiFixedString kStrCategory;
	//		bResult = pkentity->GetPropertyData("Entity Category", kStrCategory, 0);

	//	}
	//}
	// 将所有非水体 entity 添加到 water render view
	//MEntity* amEntities[] = m_pmScene->GetEntities();
	//for (int i = 0; i < amEntities->Length; i++)
	//{
	//	m_pmScene->AddRenderableEntityToWaterRenderView(amEntities[i]);
	//}

    if (bRaiseEvent)
    {
        this->EventManager->RaiseNewSceneLoaded(m_pmScene);
    }

    EndLongOperation();
    m_bDoNotUpdate = false;
}
//---------------------------------------------------------------------------
bool MFramework::SaveScene(String* strFilename, String* strFormat)
{
    MVerifyValidInstance;

    if (!m_pmScene)
        return false;

    if (!m_pmScene->GetNiScene())
        return false;

    m_bDoNotUpdate = true;

	// 检查所有灯光列表，如果某物件在灯光列表中而不在场景中，将其删除。
	m_pmScene->VerifyLightsAffectList();

    // Create the appropriate file format handler based on strFormat
    const char* pcFormat = MStringToCharPointer(strFormat);
    NiEntityStreaming* pkEntityStreaming =
        NiFactories::GetStreamingFactory()->GetPersistent(pcFormat);
    MFreeCharPointer(pcFormat);

    // Create error handler.
    NiDefaultErrorHandlerPtr spErrors = NiNew NiDefaultErrorHandler();
    pkEntityStreaming->SetErrorHandler(spErrors);

    // Insert desired scenes
    pkEntityStreaming->InsertScene(m_pmScene->GetNiScene());

    // Save the scene
    const char* pcFilename = MStringToCharPointer(strFilename);
    bool bSuccess = NIBOOL_IS_TRUE(pkEntityStreaming->Save(pcFilename));
	CTerrain::SetGsaEncodingCode(pcFilename);

	MFreeCharPointer(pcFilename);
    pkEntityStreaming->RemoveAllScenes(); // Because static instance

    // Report errors.
    MUtility::AddErrorInterfaceMessages(MessageChannelType::Errors, spErrors);
    pkEntityStreaming->SetErrorHandler(NULL);

    m_bDoNotUpdate = false;


    return bSuccess;
}
//---------------------------------------------------------------------------
void MFramework::Update()
{
    MVerifyValidInstance;

	// 如果正在进行纹理导出线程，则要停止场景更新 [12/1/2009 hemeng]
	if (!m_bRenderWindow)
	{
		return;
	}

    if (m_bDoNotUpdate)
    {
        return;
    }

    m_bDoNotUpdate = true;


    this->TimeManager->UpdateTime();

	static float fLastTime = TimeManager->CurrentTime;
	float fTimeStart = TimeManager->CurrentTime;
	m_fFrameRate = 1.0f / (fTimeStart-fLastTime);	// 帧率
	fLastTime = fTimeStart;

    PerformUpdate();
	TimeManager->UpdateTime();
	float fTimeAfterUpdate = TimeManager->CurrentTime;
	float fUpdateTime = fTimeAfterUpdate - fTimeStart;

    this->Renderer->Render();
	TimeManager->UpdateTime();
	float fTimeAfterRender = TimeManager->CurrentTime;
	m_fRenderTime = fTimeAfterRender - fTimeAfterUpdate;

	UpdateMetrics();

    m_bDoNotUpdate = false;
}
//---------------------------------------------------------------------------
void MFramework::UpdateMetrics()
{
	NIMETRICS_MAIN_RECORDMETRICS();

	//NIMETRICS_APPLICATION_ADDVALUE(FRAMERATE, m_fFrameRate);
	////NIMETRICS_APPLICATION_ADDVALUE(CULL_TIME, m_fCullTime);
	//NIMETRICS_APPLICATION_ADDVALUE(UPDATE_TIME, m_fUpdateTime);
	//NIMETRICS_APPLICATION_ADDVALUE(RENDER_TIME, m_fRenderTime);

//#ifdef NI_MEMORY_DEBUGGER
//	NiMemTracker* pkT = NiMemTracker::Get();
//	NIMETRICS_APPLICATION_ADDVALUE(ACTIVE_MEMORY, pkT->m_stActiveMemory);
//	NIMETRICS_APPLICATION_ADDVALUE(ACCUMULATED_MEMORY,
//		pkT->m_stAccumulatedMemory);
//	NIMETRICS_APPLICATION_ADDVALUE(ACTIVE_ALLOCATIONS, 
//		pkT->m_stActiveAllocationCount);
//	NIMETRICS_APPLICATION_ADDVALUE(ACCUMULATED_ALLOCATIONS,
//		pkT->m_stAccumulatedAllocationCount);
//	NIMETRICS_APPLICATION_ADDVALUE(ACTIVE_TRACKER_OVERHEAD, 
//		pkT->m_stActiveTrackerOverhead);
//	NIMETRICS_APPLICATION_ADDVALUE(ACCUMULATED_TRACKER_OVERHEAD,
//		pkT->m_stAccumulatedTrackerOverhead);
//	NIMETRICS_APPLICATION_ADDVALUE(UNUSED_BUT_ALLOCATED_MEMORY, 
//		pkT->m_stUnusedButAllocatedMemory);
//#endif // #ifdef NI_MEMORY_DEBUGGER

	NIMETRICS_UPDATE();
}

//---------------------------------------------------------------------------
void MFramework::PerformUpdate()
{
    MVerifyValidInstance;

#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("Enter MFramework::PerformUpdate"));
#endif

    // Continuous time never pauses and is used to update UI elements. Current
    // time is used to update the main scene.
    float fContinuousTime = this->TimeManager->ContinuousTime;
    float fCurrentTime = this->TimeManager->CurrentTime;

    InteractionModeService->Update(fContinuousTime);

    LightManager->Update(fContinuousTime);

    CameraManager->Update(fContinuousTime);

    if (m_pmScene != NULL)
    {
        m_pmScene->Update(fCurrentTime, m_pkAssetManager);
    }

    if (this->ProxyManager->ProxyScene != NULL)
    {
        this->ProxyManager->ProxyScene->Update(fContinuousTime,
            m_pkAssetManager);
    }

	if (ViewportManager != NULL)
	{
		for (unsigned int ui = 0; ui < this->ViewportManager->ViewportCount; ui++)
		{
			this->ViewportManager->GetViewport(ui)->ToolScene->Update(
				fContinuousTime, m_pkAssetManager);
		}
	}

	if (m_pmScene->GetRenderSceneObject())
	{
		BoundManager->Update(fContinuousTime);
	}

#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("Exit MFramework::PerformUpdate"));
#endif
}
//---------------------------------------------------------------------------
bool MFramework::get_PerformingLongOperation()
{
    MVerifyValidInstance;

    return (m_uiLongOperationCount > 0);
}
//---------------------------------------------------------------------------
void MFramework::BeginLongOperation()
{
    MVerifyValidInstance;

    if (m_uiLongOperationCount == 0)
    {
        this->EventManager->RaiseLongOperationStarted();
    }
    m_uiLongOperationCount++;
}
//---------------------------------------------------------------------------
void MFramework::EndLongOperation()
{
    MVerifyValidInstance;

    if (m_uiLongOperationCount > 0)
    {
        m_uiLongOperationCount--;

        if (m_uiLongOperationCount == 0)
        {
            this->EventManager->RaiseLongOperationCompleted();
        }
    }
}
//---------------------------------------------------------------------------
void MFramework::RestartAnimation()
{
    MEntity* pmEntities[] = m_pmScene->GetEntities();
    int iEntityCount = pmEntities->Count;
    NiFixedString kActorClassName = "NiActorComponent";
    for (int iEntityIndex = 0; iEntityIndex < iEntityCount; iEntityIndex++)
    {
        MComponent* pmComponents[] = pmEntities[iEntityIndex]->GetComponents();
        int iComponentCount = pmComponents->Count;
        for (int iComponentIndex = 0; iComponentIndex < iComponentCount; 
            iComponentIndex++)
        {
            MComponent* pmComponent = pmComponents[iComponentIndex];
            NiEntityComponentInterface* pkComponent = 
                pmComponent->GetNiEntityComponentInterface();
            NiFixedString kClassName = pkComponent->GetClassName();
            if (kClassName == kActorClassName)
            {
                NiActorComponent* pkActor = static_cast<NiActorComponent*>
                    (pkComponent);
                pkActor->ResetAnimation();
            }            
        }
    }
}
//---------------------------------------------------------------------------
String* MFramework::get_CurrentFilename()
{
    return m_strCurrentFilename;
}
//---------------------------------------------------------------------------
void MFramework::set_CurrentFilename(String* strCurrentFilename)
{
    if (strCurrentFilename == NULL)
    {
        strCurrentFilename = String::Empty;
    }
    m_strCurrentFilename = strCurrentFilename;
}
//---------------------------------------------------------------------------
IInteractionModeService* MFramework::get_InteractionModeService()
{
    if (ms_pmInteractionModeService == NULL)
    {
        ms_pmInteractionModeService = MGetService(IInteractionModeService);
        MAssert(ms_pmInteractionModeService != NULL, "Interaction mode "
            "service not found!");
    }
    return ms_pmInteractionModeService;
}
//---------------------------------------------------------------------------
ISelectionService* MFramework::get_SelectionService()
{
    if (ms_pmSelectionService == NULL)
    {
        ms_pmSelectionService = MGetService(ISelectionService);
        MAssert(ms_pmSelectionService != NULL, "Selection service not "
            "found!");
    }
    return ms_pmSelectionService;
}
//---------------------------------------------------------------------------
ISettingsService* MFramework::get_SettingsService()
{
    if (ms_pmSettingsService == NULL)
    {
        ms_pmSettingsService = MGetService(ISettingsService);
        MAssert(ms_pmSettingsService != NULL, "Settings service not "
            "found!");
    }
    return ms_pmSettingsService;
}
//---------------------------------------------------------------------------
IOptionsService* MFramework::get_OptionsService()
{
    if (ms_pmOptionsService == NULL)
    {
        ms_pmOptionsService = MGetService(IOptionsService);
        MAssert(ms_pmOptionsService != NULL, "Options service not "
            "found!");
    }
    return ms_pmOptionsService;
}
//---------------------------------------------------------------------------
//void MFramework::InitDAEffectManager(NiRendererPtr pkRenderer)
//{
//	//NiRendererPtr pkRenderer = NiRenderer::GetRenderer();
//	if (pkRenderer != NULL)
//	{
//		// 初始化 DAEffect manager
//		//CDepthAlphaEffectManager::GetInstance()->Initialize(pkRenderer);
//		//CDepthAlphaEffectManager::GetInstance()->CreateRenderTargetGroup(MRenderer::Instance->GetMainRenderWnd());
//	
//		/// 测试 LayerFog  
//		//NiStream kStream;
//		////kStream.Load(".\\Data\\Model\\SceneObj\\1101\\11010301101.nif");
//		//kStream.Load("e:\\test\\plane.nif");
//		//NiAVObjectPtr pkSceneRoot = NiDynamicCast(NiAVObject, kStream.GetObjectAt(0));
//	
//		//if (NULL != pkSceneRoot)
//		//{
//		//	m_pmScene->LayerFog = NiNew CLayerFog();
//		//	NiPoint3 kPosition(0, 0, 1);
//		//	NiMatrix3 kMatRotation;
//		//	kMatRotation.MakeIdentity();
//		//	NiColor kColor(0.5, 0.6, 0.7);
//		//	m_pmScene->LayerFog->Init(pkSceneRoot, kPosition, kMatRotation, 5.0f, 0.04f, kColor);
//		//}
//	}
//}

//---------------------------------------------------------------------------
void MFramework::SetCurrentPosition(NiPoint3* pkPos)
{
	m_pkCurrentPos = pkPos;
}
//---------------------------------------------------------------------------
NiPoint3* MFramework::GetCurrentPosition()
{
	return m_pkCurrentPos;
}
//---------------------------------------------------------------------------
float MFramework::GetCurrentX()
{
	if (m_pkCurrentPos)
		return m_pkCurrentPos->x;
	else
		return 0.0f;
}
//---------------------------------------------------------------------------
float MFramework::GetCurrentY()
{
	if (m_pkCurrentPos)
		return m_pkCurrentPos->y;
	else
		return 0.0f;
}
//---------------------------------------------------------------------------
NiAVObject* MFramework::InitAVObjectFromFile(const char *pszFilePath)
{
	bool bSuccess = true;

	NiStream kStream;
	NiAVObject* pkObj = NULL;
	bSuccess = kStream.Load(pszFilePath);
	
	if (bSuccess)
	{
		pkObj = (NiAVObject*)kStream.GetObjectAt(0);		
		MInitRefObject(pkObj);
		pkObj->UpdateProperties();
		pkObj->UpdateEffects();
		pkObj->Update(0.0f);
	}
	else
	{
		pkObj = NULL;		
	}

	return pkObj;
}


//---------------------------------------------------------------------------
bool MFramework::InitPointLightBoundFile()
{
	bool bSuccess = true;

	//NiStream kStream;
	const char* pcAppStartupPath = 
		MStringToCharPointer(MFramework::Instance->AppStartupPath);
	char acAbsGeomPath[NI_MAX_PATH];       
	size_t stSize = NiPath::ConvertToAbsolute(acAbsGeomPath, NI_MAX_PATH, 
		".\\AppData\\LightBound.nif", pcAppStartupPath);
	NIASSERT(stSize > 0);

	m_pmLightBound = InitAVObjectFromFile(acAbsGeomPath);
	MFreeCharPointer(pcAppStartupPath);

	if (m_pmLightBound == NULL)
	{
		bSuccess = false;
	}

	return bSuccess;
}

//------------------------------------------------------------------------------
bool MFramework::InitChunkNumSigns()
{
	bool bSuccess = true;

	for (int i = 0; i < 10; i ++)
	{
		const char* pcAppStartupPath = 
			MStringToCharPointer(MFramework::Instance->AppStartupPath);
		char acAbsGeomPath[NI_MAX_PATH];       
		char szTemp[NI_MAX_PATH];
		sprintf(szTemp,"%s%d.nif",".\\AppData\\Num_Sgin\\",i);
		size_t stSize = NiPath::ConvertToAbsolute(acAbsGeomPath, NI_MAX_PATH, 
			szTemp, pcAppStartupPath);
		NIASSERT(stSize > 0);

		m_pmNumSigns[i] = InitAVObjectFromFile(acAbsGeomPath);
		MFreeCharPointer(pcAppStartupPath);

		if (m_pmNumSigns[i] == NULL)
		{
			bSuccess = false;
			break;
		}
	}

	return bSuccess;
}

bool MFramework::InitRectSelTexture()
{
	const char* pcAppStartupPath = 
		MStringToCharPointer(MFramework::Instance->AppStartupPath);
	char acAbsGeomPath[NI_MAX_PATH];       
	char szTemp[NI_MAX_PATH];
	sprintf(szTemp,"%s",".\\AppData\\rectsel.tga");
	size_t stSize = NiPath::ConvertToAbsolute(acAbsGeomPath, NI_MAX_PATH, 
		szTemp, pcAppStartupPath);
	NIASSERT(stSize > 0);
	MFreeCharPointer(pcAppStartupPath);

	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;
	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::NONE;

	ms_pmThis->m_pkRectSelTexture = NiSourceTexture::Create(acAbsGeomPath/*,kPrefs*/);

	if (ms_pmThis->m_pkRectSelTexture)
	{
		return true;
	}

	return false;
}

//----------------------------------------------------------------------------
NiAVObject* MFramework::GetPointLightBound()
{
	return ms_pmThis->m_pmLightBound;
}

//-------------------------------------------------------------------------------
NiTexture* MFramework::GetRectSelTexture()
{
	if (ms_pmThis->m_pkRectSelTexture == NULL)
	{
		InitRectSelTexture();
	}

	return ms_pmThis->m_pkRectSelTexture;
}

//--------------------------------------------------------------------------------
NiAVObject* MFramework::GetChunkNumSignAt(int iNum)
{
	return ms_pmThis->m_pmNumSigns[iNum];
}

NiSourceTexturePtr MFramework::GetTerrainWaterTexture(int iIndex)
{
	//return ms_pmThis->m_pTerrainWaterTexture[iIndex];
	const char* pcAppStartupPath = 
		MStringToCharPointer(MFramework::Instance->AppStartupPath);
	char acAbsGeomPath[MAX_PATH];   
	sprintf_s(acAbsGeomPath,MAX_PATH,"%s",pcAppStartupPath);
	MFreeCharPointer(pcAppStartupPath);

	char szTmp[MAX_PATH];
	int iTmp = 0;
	if ( iIndex < 10 )		
		iTmp = sprintf_s(szTmp, MAX_PATH, ".\\data\\Texture\\Terrain\\common\\water\\caust0%d.tga", iIndex);
	else
		iTmp = sprintf_s(szTmp, MAX_PATH, ".\\data\\Texture\\Terrain\\common\\water\\caust%d.tga", iIndex);

	strcat_s(acAbsGeomPath,MAX_PATH,szTmp);
	NiSourceTexturePtr pTex = CTerrainTextureMgr::GetInstance()->GetTexture(acAbsGeomPath);

	if (pTex)
	{
		return pTex;
	}

	return NULL;
}

void MFramework::SetRenderWindow(bool bRender)
{
	m_bRenderWindow = bRender;
}

//NiVisualTracker* MFramework::get_VisualTracker()
//{
//	return m_pkVisualTracker;
//}

//#undef _DEBUG