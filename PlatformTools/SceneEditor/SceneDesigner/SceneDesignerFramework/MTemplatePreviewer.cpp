﻿#include "SceneDesignerFrameworkPCH.h"
#include "MTemplatePreviewer.h"
#include "MFramework.h"


using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
using namespace System::Drawing;

//using namespace SceneCore;

MTemplatePreviewer::MTemplatePreviewer(void)
{
	m_hTargetWnd = NULL;
	m_pkTargetObj = NULL;
	m_pkDefLight = NiNew NiDirectionalLight();
	m_pkDefLight->SetDimmer(2.0f);
	m_pkDefLight->SetAmbientColor(NiColor(1,1,1));
	m_pkDefLight->SetDiffuseColor(NiColor(1,1,1));
	m_pkDefLight->SetSpecularColor(NiColor(1,1,1));
	m_pkDefLight->SetTranslate(1.0f, -100.0f, 1.0f);

	NiMatrix3 kMatRot;
	kMatRot.FromEulerAnglesXYZ(0.3f, -0.2f, -0.3f);
	m_pkDefLight->SetRotate(kMatRot);
	m_pkDefLight->Update(0.0f);
	MInitRefObject(m_pkDefLight);
	
	m_pkWorldCamera = NiNew NiCamera();
	m_iTextureWidth = 256;
	m_iTextureHeight = 256;

	m_fCamerX = 0.0f;
	m_fCamerY = 1.0f;
	m_fCamerZ = 0.0f;
}

MTemplatePreviewer::~MTemplatePreviewer(void)
{
	NiDelete m_pkDefLight;
	m_pkDefLight = NULL;

	NiDelete m_pkWorldCamera;
	m_pkWorldCamera = NULL;
}

//---------------------------------------------------------------------------
void MTemplatePreviewer::Init()
{
	if (ms_pmThis == NULL)
	{
		ms_pmThis = new MTemplatePreviewer();
	}
}
//---------------------------------------------------------------------------
void MTemplatePreviewer::Shutdown()
{
	if (ms_pmThis != NULL)
	{
		ms_pmThis->Dispose();
		ms_pmThis = NULL;
	}
}
//---------------------------------------------------------------------------
MTemplatePreviewer* MTemplatePreviewer::get_Instance()
{
	if (ms_pmThis == NULL)
	{
		ms_pmThis = new MTemplatePreviewer();
	}

	return ms_pmThis;
}
//---------------------------------------------------------------------------
void MTemplatePreviewer::Do_Dispose(bool bDisposing)
{

}
//---------------------------------------------------------------------------
bool MTemplatePreviewer::InstanceIsValid()
{
	return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
// 设置目标窗口
void MTemplatePreviewer::SetTargetWnd(IntPtr hRendererWndPtr)
{
	m_hTargetWnd = (HWND)hRendererWndPtr.ToInt32();
	_RenderToTargetWnd();
}
//---------------------------------------------------------------------------
// 设置要渲染的物件
void MTemplatePreviewer::SetScene(MEntity* pmEntity)
{
	pmEntity->Update(0.0f, MFramework::Instance->ExternalAssetManager);
	if (m_pkTargetObj!=NULL && NiIsKindOf(NiNode, m_pkTargetObj))
	{
		//m_pkDefLight->DetachAffectedNode((NiNode*)m_pkTargetObj);
		((NiNode*)m_pkTargetObj)->DetachEffect(m_pkDefLight);
	}
	m_pkTargetObj = pmEntity->GetSceneRootPointer(0);
	if (m_pkTargetObj!=NULL && NiIsKindOf(NiNode, m_pkTargetObj))
	{
		//m_pkDefLight->AttachAffectedNode((NiNode*)m_pkTargetObj);
		((NiNode*)m_pkTargetObj)->AttachEffect(m_pkDefLight);
		m_pkTargetObj->UpdateEffects();
		m_pkTargetObj->Update(0.0f);
	}

	_RenderToTargetWnd();
}
//---------------------------------------------------------------------------
void MTemplatePreviewer::_RenderToTargetWnd()
{
	if (m_hTargetWnd==NULL || m_pkTargetObj==NULL)
	{
		return;
	}

	// 获取 renderer
	NiRenderer* pkRenderer = NiRenderer::GetRenderer();
	if (pkRenderer == NULL)
	{
		return;
	}

	// 获取目标物件包围球，根据其半径确定摄像机位置
	const NiBound& kBound = m_pkTargetObj->GetWorldBound();
	if (kBound.GetRadius() < EPSILON)
	{
		return;
	}

	_UpdateCamera(kBound.GetCenter(),kBound.GetRadius());

	// 创建 RTG
	RECT rect;
	GetClientRect(m_hTargetWnd, &rect);
	int iWidth = rect.right - rect.left;
	int iHeight = rect.bottom - rect.top;

	m_PreviewBitmap = new System::Drawing::Bitmap(iWidth, iHeight);//

	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout	= NiTexture::FormatPrefs::TRUE_COLOR_32;
	kPrefs.m_eMipMapped		= NiTexture::FormatPrefs::NO;

	NiRenderedTexturePtr pkRTTexture = NiRenderedTexture::Create(iWidth,
						iHeight, pkRenderer, kPrefs, Ni2DBuffer::MULTISAMPLE_NONE);

	NiRenderTargetGroupPtr pkRTG = NiRenderTargetGroup::Create(pkRTTexture->GetBuffer(), pkRenderer, true, true);

	if (!pkRenderer->BeginOffScreenFrame())
	{
		return;
	}
	
	NiVisibleArray kVisibleArray;
	NiCullingProcess kCullProcess(&kVisibleArray);

	NiEntityRenderingContext kRenderingContext;
	kRenderingContext.m_pkCamera = m_pkWorldCamera;
	kRenderingContext.m_pkCullingProcess = &kCullProcess;
	kRenderingContext.m_pkRenderer = pkRenderer;

	pkRenderer->BeginUsingRenderTargetGroup(pkRTG, NiRenderer::CLEAR_ALL);
	pkRenderer->SetCameraData(m_pkWorldCamera);


	NiCullScene(m_pkWorldCamera, m_pkTargetObj, kCullProcess, kVisibleArray, true);
	NiDrawVisibleArray(m_pkWorldCamera, kVisibleArray);

	pkRenderer->EndUsingRenderTargetGroup();

	pkRenderer->EndOffScreenFrame();

	// 创建一个 sysmem 中的 surface, 将 pkRTTexture 的像素从显存中拷贝出来
	LPDIRECT3DDEVICE9 pkDevice = ((NiDX9Renderer*)pkRenderer)->GetD3DDevice();
	LPDIRECT3DSURFACE9 pTextureSurface = NULL;
	HRESULT hr = pkDevice->CreateOffscreenPlainSurface(
		iWidth,
		iHeight,
		D3DFMT_X8R8G8B8, 
		D3DPOOL_SYSTEMMEM,
		&pTextureSurface,
		NULL
		);

	if (FAILED(hr))
	{
		return;
	}

	Ni2DBuffer* pkBuffer = pkRTTexture->GetBuffer();
	NiDX92DBufferData* pkBufferData = 
		(NiDX92DBufferData*)pkBuffer->GetRendererData();

	LPDIRECT3DSURFACE9 pkSourceSurface = pkBufferData->GetSurface();

	hr = D3DXLoadSurfaceFromSurface(
		pTextureSurface,
		NULL,
		NULL,
		pkSourceSurface,
		NULL,
		NULL,
		D3DX_FILTER_POINT,
		0xFF000000
		);

	// 将 shadowTextureSurface lock, 获取像素值
	D3DLOCKED_RECT lockedRect;
	pTextureSurface->LockRect(&lockedRect, 0, 0);
	DWORD* pImageData = (DWORD*)lockedRect.pBits;

	// 写像素到窗口
	for (int i=0; i<iHeight; i++)
	{
		for (int j=0; j<iWidth; j++)
		{
			DWORD dwColor = pImageData[i*iWidth+j];
			Color color =  Color::FromArgb(255, Color::FromArgb(dwColor));
			m_PreviewBitmap->SetPixel(j, i, color);
		}
	}

	pTextureSurface->UnlockRect();
	SAFE_RELEASE(pTextureSurface);

	InvalidateRgn(m_hTargetWnd, NULL, false);
	
	// 把 pkRTTexture 的像素写到
	//SaveTextureToDDS(pkRTTexture, "e:/preview.dds");

}
void MTemplatePreviewer::SetCameraViewPoint(float fX, float fY, float fZ)
{
	m_fCamerX = fX;
	m_fCamerY = fY;
	m_fCamerZ = fZ;
}

// add preview exportor functions [11/30/2009 hemeng]
//--------------------------------------------------------------------------
void MTemplatePreviewer::_UpdateCamera(NiPoint3 kPoint, float fRadius)
{
	m_pkWorldCamera->SetTranslate(NiPoint3(0,-fRadius*2, 0));
	m_pkWorldCamera->LookAtWorldPoint(NiPoint3(m_fCamerX,m_fCamerY,m_fCamerZ), NiPoint3::UNIT_Z);
	m_pkWorldCamera->Update(0.0f);

	float fOffset = fRadius*0.8f;
	NiFrustum kFrustum(kPoint.x-fOffset, kPoint.x+fOffset, 
		kPoint.z+fOffset, kPoint.z-fOffset, 0.1f, 1000.0f, true);
	m_pkWorldCamera->SetViewFrustum(kFrustum);
	m_pkWorldCamera->Update(0.0f);

}

bool MTemplatePreviewer::ExportModelScreenShot(String* szFilePath, String* szExportPath)
{
	if (szExportPath == NULL || szFilePath == NULL)
	{
		return false;
	}

	const char* szTmp = (char*)(void*)Marshal::StringToHGlobalAnsi(szFilePath);

	// 对于特效物件不进行截图 [12/4/2009 hemeng]
	String* strEntityName = System::IO::Path::GetFileNameWithoutExtension(szTmp);
	// 检查文件名5、6位是否为04
	if (strEntityName->Length < 9)
	{
		return false;
	}
	String* strSub = strEntityName->Substring(4,2);
	if (strSub->Equals("04"))
	{
		return true;
	}	


	NiStream kStream;
	if (!kStream.Load(szTmp))
	{
		Marshal::FreeHGlobal((System::IntPtr)(void*)szTmp);
		return false;
	}
	Marshal::FreeHGlobal((System::IntPtr)(void*)szTmp);
	

	NiAVObject* pkObj = NULL;

	unsigned int uiCount = kStream.GetObjectCount();
	for (unsigned int uiIndex = 0; uiIndex < uiCount; uiCount++)
	{
		NiObject* pkTmp = kStream.GetObjectAt(uiIndex);
		NiAVObject* pkAVTmp = (NiAVObject*)pkTmp;
		if (pkAVTmp->GetObjectByName("Scene Root"))
		{
			pkObj = pkAVTmp;
			pkAVTmp = NULL;
			pkTmp = NULL;
			break;
		}
	}

	if (pkObj!=NULL && NiIsKindOf(NiNode, pkObj))
	{
		//m_pkDefLight->AttachAffectedNode((NiNode*)m_pkTargetObj);
		pkObj->UpdateProperties();
		pkObj->Update(0.0f);
		((NiNode*)pkObj)->AttachEffect(m_pkDefLight);
		pkObj->UpdateEffects();
		//pkObj->Update(0.0f);
	}
	else
	{
		kStream.RemoveAllObjects();

		return false;
	}

	const NiBound& kBound = pkObj->GetWorldBound();
	if (kBound.GetRadius() < EPSILON)
	{
		pkObj = NULL;
		kStream.RemoveAllObjects();
		return false;
	}

	_UpdateCamera(kBound.GetCenter(),kBound.GetRadius());

	
	
	// 获取目标物件包围球，根据其半径确定摄像机位置	
	NiRenderer* pkRenderer = NiRenderer::GetRenderer();

	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout	= NiTexture::FormatPrefs::TRUE_COLOR_32;
	kPrefs.m_eMipMapped		= NiTexture::FormatPrefs::NO;

	NiRenderedTexturePtr pkRTTexture = NiRenderedTexture::Create(m_iTextureWidth,
		m_iTextureHeight, pkRenderer, kPrefs, Ni2DBuffer::MULTISAMPLE_NONE);

	NiRenderTargetGroupPtr pkRTG = NiRenderTargetGroup::Create(pkRTTexture->GetBuffer(), pkRenderer, true, true);

	if (!pkRenderer->BeginOffScreenFrame())
	{
		pkObj = NULL;
		kStream.RemoveAllObjects();
		return false;
	}

	NiVisibleArray kVisibleArray;
	NiCullingProcess kCullProcess(&kVisibleArray);

	NiEntityRenderingContext kRenderingContext;
	kRenderingContext.m_pkCamera = m_pkWorldCamera;
	kRenderingContext.m_pkCullingProcess = &kCullProcess;
	kRenderingContext.m_pkRenderer = pkRenderer;

	pkRenderer->BeginUsingRenderTargetGroup(pkRTG, NiRenderer::CLEAR_ALL);
	pkRenderer->SetCameraData(m_pkWorldCamera);


	NiCullScene(m_pkWorldCamera, pkObj, kCullProcess, kVisibleArray, true);
	NiDrawVisibleArray(m_pkWorldCamera, kVisibleArray);

	pkRenderer->EndUsingRenderTargetGroup();

	pkRenderer->EndOffScreenFrame();

	szTmp = (char*)(void*)Marshal::StringToHGlobalAnsi(szExportPath);
	bool bSuccess = SaveTextureToJPG(pkRTTexture, szTmp);
	Marshal::FreeHGlobal((System::IntPtr)(void*)szTmp);

	pkObj = NULL;

	kStream.RemoveAllObjects();

	return bSuccess;
}

void MTemplatePreviewer::SetTextureWidth(int iWidth)
{
	if (iWidth != m_iTextureWidth)
	{
		m_iTextureWidth = iWidth;

	}
}

void MTemplatePreviewer::SetTextureHeight(int iHeight)
{
	if (iHeight != m_iTextureHeight)
	{
		m_iTextureHeight = iHeight;

	}
}

// [11/30/2009 hemeng]

//---------------------------------------------------------------------------
System::Drawing::Bitmap* MTemplatePreviewer::get_PreviewBitmap()
{
	return m_PreviewBitmap;
}
//---------------------------------------------------------------------------
