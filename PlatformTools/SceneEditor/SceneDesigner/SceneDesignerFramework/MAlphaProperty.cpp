﻿#include "SceneDesignerFrameworkPCH.h"

#include "MAlphaProperty.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
MAlphaProperty::MAlphaProperty(NiAlphaProperty* pkProperty, NiAVObject* pkTagAVObj)
: MNiProperty(pkProperty, pkTagAVObj)
{
}
//---------------------------------------------------------------------------
bool MAlphaProperty::get_AlphaBlending()
{
	if (m_pkTagProperty)
	{
		return GetTagProperty()->GetAlphaBlending();
	}
}
//---------------------------------------------------------------------------
void MAlphaProperty::set_AlphaBlending(bool bBlend)
{
	if (m_pkTagProperty)
	{
		return GetTagProperty()->SetAlphaBlending(bBlend);
	}
}
//---------------------------------------------------------------------------
const String* MAlphaProperty::get_SrcAlphaFun()
{
	if (m_pkTagProperty)
	{
		return (AlphaFunEnumToStr(GetTagProperty()->GetSrcBlendMode()));
	}
}
//---------------------------------------------------------------------------
void MAlphaProperty::set_SrcAlphaFun(const String* strFun)
{
	if (m_pkTagProperty && strFun)
	{
		GetTagProperty()->SetSrcBlendMode(AlphaFunStrToEnum(strFun));
	}
}
//---------------------------------------------------------------------------
const String* MAlphaProperty::get_DestAlphaFun()
{
	if (m_pkTagProperty)
	{
		return (AlphaFunEnumToStr(GetTagProperty()->GetDestBlendMode()));
	}
}
//---------------------------------------------------------------------------
void MAlphaProperty::set_DestAlphaFun(const String* strFun)
{
	if (m_pkTagProperty && strFun)
	{
		GetTagProperty()->SetDestBlendMode(AlphaFunStrToEnum(strFun));
	}
}
//---------------------------------------------------------------------------
bool MAlphaProperty::get_AlphaTesting()
{
	if (m_pkTagProperty)
	{
		return GetTagProperty()->GetAlphaTesting();
	}
}
//---------------------------------------------------------------------------
void MAlphaProperty::set_AlphaTesting(bool bTest)
{
	if (m_pkTagProperty)
	{
		return GetTagProperty()->SetAlphaTesting(bTest);
	}
}
//---------------------------------------------------------------------------
int  MAlphaProperty::get_TestRef()
{
	if (m_pkTagProperty)
	{
		return GetTagProperty()->GetTestRef();
	}
}
//---------------------------------------------------------------------------
void  MAlphaProperty::set_TestRef(int iRef)
{
	if (m_pkTagProperty)
	{
		GetTagProperty()->SetTestRef(iRef);
	}
}
//---------------------------------------------------------------------------
bool MAlphaProperty::Equals(Object* pmObj)
{
	MAlphaProperty* pmAlphaProp = dynamic_cast<MAlphaProperty*>(pmObj);
	if (pmAlphaProp == NULL)
	{
		return false;
	}

	return (pmAlphaProp->AlphaBlending == AlphaBlending
			&& pmAlphaProp->AlphaTesting == AlphaTesting);
}
//---------------------------------------------------------------------------
NiAlphaProperty::AlphaFunction MAlphaProperty::AlphaFunStrToEnum(const String* strAlphaFun)
{
	if (strAlphaFun == NULL)
	{
		return NiAlphaProperty::ALPHA_MAX_MODES;
	}

	for (int i=0; i<ALPHA_FUNS->Length; i++)
	{
		if (ALPHA_FUNS[i] == (strAlphaFun))
		{
			return (NiAlphaProperty::AlphaFunction)i;
		}
	}

	return NiAlphaProperty::ALPHA_MAX_MODES;
}
//---------------------------------------------------------------------------
const String* MAlphaProperty::AlphaFunEnumToStr(NiAlphaProperty::AlphaFunction eAlphaFun)
{
	return ALPHA_FUNS[(int)eAlphaFun];
}