﻿#include "SceneDesignerFrameworkPCH.h"
#include "MChunkEntityManager.h"
#include "MFramework.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;
//---------------------------------------------------------------------------
MChunkEntityManager::MChunkEntityManager(int iNumChunksX, int iNumChunksY)
:	m_iNumChunkX(iNumChunksX),
	m_iNumChunkY(iNumChunksY)
{
	m_ChunksEntityList = new ArrayList();
	for (int i=0; i<m_iNumChunkX*m_iNumChunkY; i++)
	{
		m_ChunksEntityList->Add(new ArrayList());
	}
	m_StaticChunksDisplayList = new ArrayList();

	m_EntityCullMode = ECM_CULL_NONE;
}
//---------------------------------------------------------------------------
MChunkEntityManager::EntityCullMode MChunkEntityManager::GetEntityCullMode()
{
	return m_EntityCullMode;
}
//---------------------------------------------------------------------------
void MChunkEntityManager::SetEntityCullMode(EntityCullMode mode)
{
	m_EntityCullMode = mode;
}
//---------------------------------------------------------------------------
ArrayList* MChunkEntityManager::get_StaticChunkDisplayList()
{
	return m_StaticChunksDisplayList;
}
//---------------------------------------------------------------------------
void MChunkEntityManager::set_StaticChunkDisplayList(ArrayList* staticChunksList)
{
	m_StaticChunksDisplayList->Clear();
	for (int i=0; i<staticChunksList->Count; i++)
	{
		m_StaticChunksDisplayList->Add(staticChunksList->Item[i]);
	}
}
//---------------------------------------------------------------------------
// 添加一个 Entity,自动判断其所在的 chunk并添加到相应 list 中
void MChunkEntityManager::AddEntity(MEntity* pmEntity)
{
	if (pmEntity == NULL)
	{
		return;
	}
	
	if (MLightManager::EntityIsLight(pmEntity) || MCameraManager::EntityIsCamera(pmEntity))
	{
		return;
	}

	NiFixedString szTemp = "";
	if (pmEntity->Name->Contains("Light Proxy")/*GetNiEntityInterface()->GetPropertyData("Light Proxy",szTemp)*/)
	{
		return;
	}

	NiAVObject* pkAVObj = pmEntity->GetSceneRootPointer(0);
	if (pkAVObj==NULL || pkAVObj->GetAppCulled())
	{
		return;
	}

	// 保证 entity 在地形范围内
	NiPoint3 kCenter = pkAVObj->GetWorldBound().GetCenter();

	int iChunkID = MFramework::Instance->Scene->Terrain->GetChunkIndex(kCenter);
	if (iChunkID != -1)
	{
		(dynamic_cast<ArrayList*>(m_ChunksEntityList->Item[iChunkID]))->Add(pmEntity);
	}

}
//---------------------------------------------------------------------------
// 移除一个 Entity
void MChunkEntityManager::RemoveEntity(MEntity* pmEntity)
{
	if (pmEntity == NULL)
	{
		return;
	}

	for (int i=0; i<m_iNumChunkX*m_iNumChunkY; i++)
	{
		(dynamic_cast<ArrayList*>(m_ChunksEntityList->Item[i]))->Remove(pmEntity);
	}
}
//---------------------------------------------------------------------------
// 当 Entity 发生移动可能会跨 chunk，将其从 list 中 remove,再添加
void MChunkEntityManager::EntityMoved(MEntity* pmEntity)
{
	if (this == NULL) return;
	RemoveEntity(pmEntity);
	AddEntity(pmEntity);
}
//---------------------------------------------------------------------------
// 获取 参数列表中 所有chunk 对应的 entity
MEntity* MChunkEntityManager::GetEntity(vector<int>* chunksID)[]
{
	// 统计 chunksID 中的 chunk 的 entity 总数
	unsigned int uiEntityCount = 0;
	for (unsigned int i=0; i<chunksID->size(); i++)
	{
		int iChunkID = (int)((*chunksID)[i]);
		uiEntityCount += (dynamic_cast<ArrayList*>(m_ChunksEntityList->Item[iChunkID]))->Count;
	}

	MEntity* amEntities[] = new MEntity*[uiEntityCount];

	unsigned int uiIdx = 0;
	for (unsigned int i=0; i<chunksID->size(); i++)
	{
		int iChunkID = (int)((*chunksID)[i]);
		ArrayList* chunkEntities = dynamic_cast<ArrayList*>(m_ChunksEntityList->Item[iChunkID]);
		for (int j=0; j<chunkEntities->Count; j++)
		{
			amEntities[uiIdx++] = dynamic_cast<MEntity*>(chunkEntities->Item[j]);
		}
	}
	return amEntities;
}
//---------------------------------------------------------------------------
MEntity* MChunkEntityManager::GetEntity(int iChunkID)[]
{
	if (iChunkID < 0)
	{
		return NULL;
	}

	ArrayList* chunkEntities = dynamic_cast<ArrayList*>(m_ChunksEntityList->Item[iChunkID]);
	MEntity* amEntities[] = new MEntity*[chunkEntities->Count];

	for (int j=0; j<chunkEntities->Count; j++)
	{
		amEntities[j] = dynamic_cast<MEntity*>(chunkEntities->Item[j]);
	}

	return amEntities;
};
//---------------------------------------------------------------------------
MEntity* MChunkEntityManager::GetStaticEntityList()[]
{
	vector<int> chunkList;
	for (int i=0; i<m_StaticChunksDisplayList->Count; i++)
	{
		chunkList.push_back(Int32::Parse((m_StaticChunksDisplayList->Item[i])->ToString()));
	}
	return GetEntity(&chunkList);
};

//---------------------------------------------------------------------------
void MChunkEntityManager::Init(int iNumChunksX, int iNumChunksY)
{
	if (ms_pmThis == NULL)
	{
		ms_pmThis = new MChunkEntityManager(iNumChunksX, iNumChunksY);
	}
}
//---------------------------------------------------------------------------
void MChunkEntityManager::Shutdown()
{
	if (ms_pmThis != NULL)
	{
		ms_pmThis->Dispose();
		ms_pmThis = NULL;
	}
}
//---------------------------------------------------------------------------
bool MChunkEntityManager::InstanceIsValid()
{
	return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
MChunkEntityManager* MChunkEntityManager::get_Instance()
{
	return ms_pmThis;
}
//---------------------------------------------------------------------------
void MChunkEntityManager::Do_Dispose(bool bDisposing)
{
}
