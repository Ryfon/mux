﻿#include "SceneDesignerFrameworkPCH.h"
#include "MAdvanceWaterParameter.h"
#include "float.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
MAdvanceWaterParameter::MAdvanceWaterParameter(NiAVObject* pkTagAVObj)
: m_pkTagObject(pkTagAVObj)
{
}
//---------------------------------------------------------------------------
MQuaternion* MAdvanceWaterParameter::_GetFloat4ExtraData(const char* pszExtraDataName)
{
	NiExtraData* pkExtraData = m_pkTagObject->GetExtraData(pszExtraDataName);

	if (pkExtraData)
	{
		NiFloatsExtraData* pkFloatsExtraData 
			= static_cast<NiFloatsExtraData*>(pkExtraData);

		if (pkFloatsExtraData)
		{
			unsigned int uiSize = 0;
			float* arrExtraData = new float[4];
			pkFloatsExtraData->GetArray(uiSize, arrExtraData);
			MQuaternion* pmExtraData = new MQuaternion();
			pmExtraData->W = arrExtraData[0];
			pmExtraData->X = arrExtraData[1];
			pmExtraData->Y = arrExtraData[2];
			pmExtraData->Z = arrExtraData[3];
			delete[] arrExtraData;

			return pmExtraData;
		}
	}

	return NULL;
}
//---------------------------------------------------------------------------
MPoint3* MAdvanceWaterParameter::_GetFloat3ExtraData(const char* pszExtraDataName)
{
	NiExtraData* pkExtraData = m_pkTagObject->GetExtraData(pszExtraDataName);

	if (pkExtraData)
	{
		NiFloatsExtraData* pkFloatsExtraData 
			= static_cast<NiFloatsExtraData*>(pkExtraData);

		if (pkFloatsExtraData)
		{
			unsigned int uiSize = 0;
			float* arrExtraData = new float[3];
			pkFloatsExtraData->GetArray(uiSize, arrExtraData);
			MPoint3* pmExtraData = new MPoint3();
			pmExtraData->X = arrExtraData[0];
			pmExtraData->Y = arrExtraData[1];
			pmExtraData->Z = arrExtraData[2];
			delete[] arrExtraData;

			return pmExtraData;
		}
	}

	return NULL;
}
//---------------------------------------------------------------------------
MPoint2* MAdvanceWaterParameter::_GetFloat2ExtraData(const char* pszExtraDataName)
{
	NiExtraData* pkExtraData = m_pkTagObject->GetExtraData(pszExtraDataName);

	if (pkExtraData)
	{
		NiFloatsExtraData* pkFloatsExtraData 
			= static_cast<NiFloatsExtraData*>(pkExtraData);

		if (pkFloatsExtraData)
		{
			unsigned int uiSize = 0;
			float* arrExtraData = new float[2];
			pkFloatsExtraData->GetArray(uiSize, arrExtraData);
			MPoint2* pmExtraData = new MPoint2();
			pmExtraData->X = arrExtraData[0];
			pmExtraData->Y = arrExtraData[1];
			delete[] arrExtraData;

			return pmExtraData;
		}
	}

	return NULL;
}
//---------------------------------------------------------------------------
float MAdvanceWaterParameter::_GetFloatExtraData(const char* pszExtraDataName)
{
	NiExtraData* pkExtraData = m_pkTagObject->GetExtraData(pszExtraDataName);

	if (pkExtraData)
	{
		NiFloatExtraData* pkFloatExtraData 
			= static_cast<NiFloatExtraData*>(pkExtraData);

		if (pkFloatExtraData)
		{
			return pkFloatExtraData->GetValue();
		}
	}

	// Error Return
	return FLT_MIN;
}
//---------------------------------------------------------------------------
// 设置 Extra Data
void MAdvanceWaterParameter::_SetFloat4ExtraData(const char* pszExtraDataName, MQuaternion* pmExtraData)
{
	NiExtraData* pkExtraData = m_pkTagObject->GetExtraData(pszExtraDataName);

	if (pkExtraData)
	{
		NiFloatsExtraData* pkFloatsExtraData 
			= static_cast<NiFloatsExtraData*>(pkExtraData);

		if (pkFloatsExtraData)
		{
			float arrExtraData[4] = {pmExtraData->W, pmExtraData->X, pmExtraData->Y, pmExtraData->Z};
			pkFloatsExtraData->SetArray(4, arrExtraData);
		}
	}
}
//---------------------------------------------------------------------------
// 设置 Extra Data
void MAdvanceWaterParameter::_SetFloat3ExtraData(const char* pszExtraDataName, MPoint3* pmExtraData)
{
	NiExtraData* pkExtraData = m_pkTagObject->GetExtraData(pszExtraDataName);

	if (pkExtraData)
	{
		NiFloatsExtraData* pkFloatsExtraData 
			= static_cast<NiFloatsExtraData*>(pkExtraData);

		if (pkFloatsExtraData)
		{
			float arrExtraData[3] = {pmExtraData->X, pmExtraData->Y, pmExtraData->Z};
			pkFloatsExtraData->SetArray(3, arrExtraData);
		}
	}
}
//---------------------------------------------------------------------------
void MAdvanceWaterParameter::_SetFloat2ExtraData(const char* pszExtraDataName, MPoint2* pmExtraData)
{
	NiExtraData* pkExtraData = m_pkTagObject->GetExtraData(pszExtraDataName);

	if (pkExtraData)
	{
		NiFloatsExtraData* pkFloatsExtraData 
			= static_cast<NiFloatsExtraData*>(pkExtraData);

		if (pkFloatsExtraData)
		{
			float arrExtraData[2] = {pmExtraData->X, pmExtraData->Y};
			pkFloatsExtraData->SetArray(2, arrExtraData);
		}
	}
}
//---------------------------------------------------------------------------
void MAdvanceWaterParameter::_SetFloatExtraData(const char* pszExtraDataName, float fExtraData)
{
	NiExtraData* pkExtraData = m_pkTagObject->GetExtraData(pszExtraDataName);

	if (pkExtraData)
	{
		NiFloatExtraData* pkFloatExtraData 
			= static_cast<NiFloatExtraData*>(pkExtraData);

		if (pkFloatExtraData)
		{
			pkFloatExtraData->SetValue(fExtraData);
		}
	}
}
//---------------------------------------------------------------------------
MQuaternion* MAdvanceWaterParameter::get_Amplitude()
{
	return _GetFloat4ExtraData("Amplitude");
}
//---------------------------------------------------------------------------
void MAdvanceWaterParameter::set_Amplitude(MQuaternion* pmAmplitude)
{
	_SetFloat4ExtraData("Amplitude", pmAmplitude);
}
//---------------------------------------------------------------------------
// 频率 每个 2Pi 距离波的重复次数
MQuaternion* MAdvanceWaterParameter::get_WaveFrequency()
{
	return _GetFloat4ExtraData("WaveFrequency");
}
//---------------------------------------------------------------------------
void MAdvanceWaterParameter::set_WaveFrequency(MQuaternion* pmWaveFrequency)
{
	_SetFloat4ExtraData("WaveFrequency", pmWaveFrequency);
}
//---------------------------------------------------------------------------
// 相位常熟，每秒相位偏移次数
MQuaternion* MAdvanceWaterParameter::get_WavePhi()
{
	return _GetFloat4ExtraData("WavePhi");
}
void MAdvanceWaterParameter::set_WavePhi(MQuaternion* pmPhi)
{
	_SetFloat4ExtraData("WavePhi", pmPhi);
}
//---------------------------------------------------------------------------
// 四个波的方向
MPoint2* MAdvanceWaterParameter::get_WaveDir0()
{
	return _GetFloat2ExtraData("WaveDir0");
}
void MAdvanceWaterParameter::set_WaveDir0(MPoint2* pmDir)
{
	_SetFloat2ExtraData("WaveDir0", pmDir);
}
MPoint2* MAdvanceWaterParameter::get_WaveDir1()
{
	return _GetFloat2ExtraData("WaveDir1");
}
void MAdvanceWaterParameter::set_WaveDir1(MPoint2* pmDir)
{
	_SetFloat2ExtraData("WaveDir1", pmDir);
}
MPoint2* MAdvanceWaterParameter::get_WaveDir2()
{
	return _GetFloat2ExtraData("WaveDir2");
}
void MAdvanceWaterParameter::set_WaveDir2(MPoint2* pmDir)
{
	_SetFloat2ExtraData("WaveDir2", pmDir);
}
MPoint2* MAdvanceWaterParameter::get_WaveDir3()
{
	return _GetFloat2ExtraData("WaveDir3");
}
void MAdvanceWaterParameter::set_WaveDir3(MPoint2* pmDir)
{
	_SetFloat2ExtraData("WaveDir3", pmDir);
}
//---------------------------------------------------------------------------
// 透明度
float MAdvanceWaterParameter::get_WaterAlpha()
{
	return _GetFloatExtraData("WaterAlpha");
}
void MAdvanceWaterParameter::set_WaterAlpha(float fAlpha)
{
	_SetFloatExtraData("WaterAlpha", fAlpha);
}
//---------------------------------------------------------------------------
// 反射色
MPoint3* MAdvanceWaterParameter::get_ColorReflect()
{
	return _GetFloat3ExtraData("ColorReflect");
}
//---------------------------------------------------------------------------
void MAdvanceWaterParameter::set_ColorReflect(MPoint3* pmColor)
{
	_SetFloat3ExtraData("ColorReflect", pmColor);
}
//---------------------------------------------------------------------------
// 高光色
MPoint3* MAdvanceWaterParameter::get_ColorSpecular()
{
	return _GetFloat3ExtraData("ColorSpecular");
}
//---------------------------------------------------------------------------
void MAdvanceWaterParameter::set_ColorSpecular(MPoint3* pmColor)
{
	_SetFloat3ExtraData("ColorSpecular", pmColor);
}

