﻿// 光照分布图
#pragma once

#include "MLightManager.h"
#include "MEntity.h"

using namespace System;
using namespace System::Collections;
using namespace System::Windows;
using namespace System::Windows::Forms;
using namespace System::IO;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{

		// 场景中一个物件实例的统计信息
		public __gc class MDistributeMapGenerator
		{
		public:
			// 分布图类型
			__value enum EMapType
			{
				MT_Lighting = 0,	// 光照分布图
				MT_Entity = 1,		// 物件分布图
			};

			// Constructor, 
			MDistributeMapGenerator(float fLumPreSphere);

			// 生成光照/物件分布图
			System::Drawing::Bitmap* GenerateDistributeMap(EMapType eType);


			__property System::Drawing::Bitmap* get_DistributeMap(){return m_DistributeMap;}

			// 设置目标窗口
			void SetTargetWnd(IntPtr hRendererWndPtr);

		protected:
			// 获取当前场景的灯光球
			void _GetSceneLightSpheres(NiTPrimitiveArray<NiTriShape*>& kLightSpheres);

			// 获取场景物件包围球
			void _GetSceneEntitySpheres(NiTPrimitiveArray<NiTriShape*>& kEntitySpheres);

			// 设置摄像机
			void _SetCamera(NiCamera& kCamera);

			float						m_fLumPreSphere;	// 每个灯光的最大亮度( 1.0 为纯白.默认为 0.2)
			HWND						m_hTargetWnd;	// 目标窗口
			System::Drawing::Bitmap*	m_DistributeMap;// 生成的分布图
		};


	}}}}

