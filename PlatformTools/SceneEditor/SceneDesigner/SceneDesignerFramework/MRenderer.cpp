﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MRenderer.h"
#include <NiMaterialToolkit.h>
#include <NiD3DUtility.h>
#include <NiRTTI.h>
#include <NiMetricsOutput.h>
#include "MFramework.h"
#include "MUtility.h"
#include "MViewportManager.h"
#include "ServiceProvider.h"
#include "DepthAlphaEffectManager.h"
#include "MTimeStamp.h"
#include <NiVisualTracker.h>
using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
void MRenderer::Init()
{
    if (ms_pmThis == NULL)
    {
        ms_pmThis = new MRenderer();
    }
}
//---------------------------------------------------------------------------
void MRenderer::Shutdown()
{
    if (ms_pmThis != NULL)
    {
        ms_pmThis->Dispose();
        ms_pmThis = NULL;
    }
}
//---------------------------------------------------------------------------
bool MRenderer::InstanceIsValid()
{
    return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
MRenderer* MRenderer::get_Instance()
{
    return ms_pmThis;
}
//---------------------------------------------------------------------------
MRenderer::MRenderer() : m_pkRenderer(NULL), m_pkMainRenderTarget(NULL),
    m_hRendererWnd(0), m_pkVisibleArray(NULL), m_pkCullingProcess(NULL),
    m_hNiCgShaderLib(0), m_pkTexture1(NULL), m_pkTexture2(NULL),
    m_pkTarget1(NULL), m_pkTarget2(NULL), m_pkScreenQuad(NULL),
    m_pkScreenQuadTexProp(NULL), m_pkScreenQuadAlphaProp(NULL),
    m_pkScreenQuadVertexColorProp(NULL), m_pkScreenQuadStencilProp(NULL),
    m_pkScreenQuadShaderMaterial(NULL), m_pkHighlightColor(NULL),
    m_bD3D10(false), m_bNeedsRecreate(false), m_bNeedsPrecache(false)
	,m_pkVisualTracker(0), m_bShowVisualTracker(false)
{
    m_mBackgroundColor = Color::FromArgb(255, 128, 128, 128);

    m_pmRenderingContext = new MRenderingContext(
        NiNew NiEntityRenderingContext());

    m_pkVisibleArray = NiNew NiVisibleArray(1024, 1024);
    m_pkCullingProcess = NiNew NiCullingProcess(m_pkVisibleArray);

	//080903 delete by hemeng
	//m_pkBloomEffect = NiNew CBloomEffect();
	//add by 和萌 ColorAdjustEffect
	m_bInitPostMgr = false;
	m_pkColorAdjustEffect = NiNew CColorAdjustEffect();
	m_pkBloomEffect = NiNew CBloomEffect();
	m_pkPostEffectMgr = NiNew CPostEffectManager();
	m_pkPostEffectMgr->AddEffect(m_pkColorAdjustEffect);
	m_pkPostEffectMgr->AddEffect(m_pkBloomEffect);

	m_pmRenderingContext->GetRenderingContext()->m_pkCullingProcess = m_pkCullingProcess;

    NiImageConverter::SetImageConverter(NiNew NiDevImageConverter());

    // Turn off debug output messages from shader system for speed of
    // debugging purposes.
    //NiD3DUtility::SetLogEnabled(false);
    //NiLogger::SetOutputToDebugWindow(NIMESSAGE_GENERAL_0, false);
    //NiLogger::SetOutputToDebugWindow(NIMESSAGE_GENERAL_1, false);

	//NiRect<float> kRect;
	//kRect.m_left=0.05f; 
	//kRect.m_right = 0.95f;
	//kRect.m_top = 0.5f;
	//kRect.m_bottom = 0.95f;
	//NiVisualTracker* pkVisualTracker = NiNew NiVisualTracker(1000.0f, 5, kRect, "Scene Metric");

    __hook(&MEventManager::NewSceneLoaded, MEventManager::Instance,
        &MRenderer::OnNewSceneLoaded);
    __hook(&MEventManager::EntityAddedToScene, MEventManager::Instance,
        &MRenderer::OnEntityAddedToScene);
    __hook(&MEventManager::EntityPropertyChanged, MEventManager::Instance,
        &MRenderer::OnEntityPropertyChanged);
}
//---------------------------------------------------------------------------
void MRenderer::Do_Dispose(bool bDisposing)
{
    if (bDisposing)
    {
        m_pmRenderingContext->Dispose();

		//081117 add by 和萌
		NiDelete m_pkPostEffectMgr;
		m_pkColorAdjustEffect = NULL;
		m_pkBloomEffect = NULL;
	}

    if (m_hNiCgShaderLib)
    {
        FreeLibrary(m_hNiCgShaderLib);
    }

    NiDelete m_pkHighlightColor;
    NiDelete m_pkCullingProcess;
    NiDelete m_pkVisibleArray;
	//NiDelete m_pkVisualTracker;

    MDisposeRefObject(m_pkTexture1);
    MDisposeRefObject(m_pkTexture2);
    MDisposeRefObject(m_pkTarget1);
    MDisposeRefObject(m_pkTarget2);
    MDisposeRefObject(m_pkScreenQuad);
    MDisposeRefObject(m_pkScreenQuadTexProp);
    MDisposeRefObject(m_pkScreenQuadAlphaProp);
    MDisposeRefObject(m_pkScreenQuadVertexColorProp);
    MDisposeRefObject(m_pkScreenQuadStencilProp);
    MDisposeRefObject(m_pkScreenQuadShaderMaterial);
	MDisposeRefObject(m_pkVisualTracker);
	MDisposeRefObject(m_pkVisualTrackerOutput);
    NiMaterialToolkit::DestroyToolkit();


    MDisposeRefObject(m_pkRenderer);

    __unhook(&MEventManager::NewSceneLoaded, MEventManager::Instance,
        &MRenderer::OnNewSceneLoaded);
    __unhook(&MEventManager::EntityAddedToScene, MEventManager::Instance,
        &MRenderer::OnEntityAddedToScene);
    __unhook(&MEventManager::EntityPropertyChanged, MEventManager::Instance,
        &MRenderer::OnEntityPropertyChanged);
}
//---------------------------------------------------------------------------
void MRenderer::OnNewSceneLoaded(MScene* pmScene)
{
    MVerifyValidInstance;

    if (MFramework::Instance->Scene == pmScene && pmScene->EntityCount > 0)
    {
        PerformPrecache(pmScene);
    }
}
//---------------------------------------------------------------------------
void MRenderer::OnEntityAddedToScene(MScene* pmScene, MEntity* pmEntity)
{
    MVerifyValidInstance;

	// 记录时间戳
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("Enter MRenderer::OnEntityAddedToScene"));
#endif

    if (MFramework::Instance->Scene == pmScene)
    {
        PerformPrecache(pmEntity);
    }

#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("Quit MRenderer::OnEntityAddedToScene"));
#endif
}
//---------------------------------------------------------------------------
void MRenderer::OnEntityPropertyChanged(MEntity* pmEntity,
    String* strPropertyName, unsigned int uiPropertyIndex, bool bInBatch)
{
    MVerifyValidInstance;

    if (pmEntity->IsExternalAssetPath(strPropertyName, uiPropertyIndex))
    {
        if (MFramework::Instance->Scene->IsEntityInScene(pmEntity))
        {
            // Perform an initial update here to force the entity to load
            // the external asset.
            pmEntity->Update(MTimeManager::Instance->CurrentTime,
                MFramework::Instance->ExternalAssetManager);

            // Precache new external asset.
            PerformPrecache(pmEntity);
        }
        else
        {
            MEntity* amDependentEntities[] = MFramework::Instance->Scene
                ->GetDependentEntities(pmEntity);
            for (int i = 0; i < amDependentEntities->Length; i++)
            {
                MEntity* pmDependentEntity = amDependentEntities[i];
                if (MFramework::Instance->Scene->IsEntityInScene(
                    pmDependentEntity))
                {
                    // Perform an initial update here to force the entity to
                    // load the external asset.
                    pmDependentEntity->Update(
                        MTimeManager::Instance->CurrentTime,
                        MFramework::Instance->ExternalAssetManager);

                    // Precache new external asset.
                    PerformPrecache(pmDependentEntity);
                }
            }
        }
    }
}
//---------------------------------------------------------------------------
Color MRenderer::get_BackgroundColor()
{
    MVerifyValidInstance;

    return m_mBackgroundColor;
}
//---------------------------------------------------------------------------
void MRenderer::set_BackgroundColor(Color mBackgroundColor)
{
    MVerifyValidInstance;

    m_mBackgroundColor = mBackgroundColor;
    if (m_pkRenderer != NULL)
    {
        NiColor kBackgroundColor(MUtility::RGBToFloat(mBackgroundColor.R),
            MUtility::RGBToFloat(mBackgroundColor.G),
            MUtility::RGBToFloat(mBackgroundColor.B));
        m_pkRenderer->SetBackgroundColor(kBackgroundColor);
    }
}
//---------------------------------------------------------------------------
int MRenderer::get_Width()
{
    MVerifyValidInstance;

    if (m_pkRenderer)
    {
        return m_pkMainRenderTarget->GetWidth(0);
    }
    else
    {
        return 0;
    }
}
//---------------------------------------------------------------------------
int MRenderer::get_Height()
{
    MVerifyValidInstance;

    if (m_pkRenderer)
    {
        return m_pkMainRenderTarget->GetHeight(0);
    }
    else
    {
        return 0;
    }
}
//---------------------------------------------------------------------------
MRenderingContext* MRenderer::get_RenderingContext()
{
    MVerifyValidInstance;

    return m_pmRenderingContext;
}
//---------------------------------------------------------------------------
bool MRenderer::get_D3D10()
{
    MVerifyValidInstance;

    return m_bD3D10;
}
//---------------------------------------------------------------------------
void MRenderer::UseD3D10()
{
    MVerifyValidInstance;

    if (!m_pkRenderer)
    {
        m_bD3D10 = true;
    }
}
//---------------------------------------------------------------------------
NiRenderTargetGroup* MRenderer::GetMainRenderTargetGroup()
{
	return m_pkMainRenderTarget;
}
//---------------------------------------------------------------------------
HWND MRenderer::GetMainRenderWnd()
{
	return m_hRendererWnd;
}

//---------------------------------------------------------------------------
void MRenderer::SetColorAdjustEffectEnabled(bool bEnabled)
{
	MGlobalSetting::Instance->SetColorAdjustEffectEnabled(bEnabled);
	m_pkColorAdjustEffect->SetActive(bEnabled);
	
}
//---------------------------------------------------------------------------
bool MRenderer::GetColorAdjustEffectEnabled()
{
	return MGlobalSetting::Instance->GetColorAdjustEffectEnabled();
}
//--------------------------------------------------------------------------
void MRenderer::SetBloomEffectEnabled(bool bEnabled)
{
	MGlobalSetting::Instance->SetBloomEffectEnabled(bEnabled);
	m_pkBloomEffect->SetActive(bEnabled);
}
//--------------------------------------------------------------------------
bool MRenderer::GetBloomEffectEnabled()
{
	return MGlobalSetting::Instance->GetBloomEffectEnabled();
}
//---------------------------------------------------------------------------
void MRenderer::SetPostEffectByType(int type,float fValue)
{
	switch(type)
	{
	case 0:
		MGlobalSetting::Instance->SetAdjustValueByType(MGlobalSetting::Instance->AT_ADJUST_BRIGHT,fValue);
		break;
	case 1:
		MGlobalSetting::Instance->SetAdjustValueByType(MGlobalSetting::Instance->AT_ADJUST_CONTRAST,fValue);
		break;
	case 2:
		MGlobalSetting::Instance->SetAdjustValueByType(MGlobalSetting::Instance->AT_ADJUST_HUE,fValue);
	    break;
	case 3:
		MGlobalSetting::Instance->SetAdjustValueByType(MGlobalSetting::Instance->AT_ADJUST_SAT,fValue);
	    break;
	default:
	    break;
	}
}
float MRenderer::GetPostEffectByType(int type)
{
	switch(type)
	{
	case 0:
		return MGlobalSetting::Instance->GetAdjustValueByType(MGlobalSetting::Instance->AT_ADJUST_BRIGHT);
		break;
	case 1:
		return MGlobalSetting::Instance->GetAdjustValueByType(MGlobalSetting::Instance->AT_ADJUST_CONTRAST);
		break;
	case 2:
		return MGlobalSetting::Instance->GetAdjustValueByType(MGlobalSetting::Instance->AT_ADJUST_HUE);
	    break;
	case 3:
		return MGlobalSetting::Instance->GetAdjustValueByType(MGlobalSetting::Instance->AT_ADJUST_SAT);
	    break;
	default:
		return 0;
	    break;
	}
}

// add [5/27/2009 hemeng]
void MRenderer::SetBloomEffectByType(int type, float fValue)
{
	switch(type)
	{
	case 0:
		MGlobalSetting::Instance->SetBloomValueByType(MGlobalSetting::Instance->EBloomType::BT_LUM, fValue);
		break;
	case 1:
		MGlobalSetting::Instance->SetBloomValueByType(MGlobalSetting::Instance->EBloomType::BT_SCALE, fValue);
	    break;
	default:
	    break;
	}
}

float MRenderer::GetBloomValueByType(int type)
{
	switch(type)
	{
	case 0:
		return MGlobalSetting::Instance->GetBloomValueByType(MGlobalSetting::Instance->EBloomType::BT_LUM);
		break;
	case 1:
		return MGlobalSetting::Instance->GetBloomValueByType(MGlobalSetting::Instance->EBloomType::BT_SCALE);
	    break;
	default:
		return -1;
	    break;
	}
}

//----------------------------------------------------------------------------
bool MRenderer::Create(IntPtr hTopLevelWndPtr, IntPtr hRendererWndPtr)
{
    MVerifyValidInstance;

    if (m_pkRenderer != NULL)
    {
        return false;
    }

    HWND hTopLevelWnd = (HWND) hTopLevelWndPtr.ToInt32();
    m_hRendererWnd = (HWND) hRendererWndPtr.ToInt32();

    RECT rect;
    NIVERIFY(::GetClientRect(m_hRendererWnd, &rect));
    if ((rect.right - rect.left) <= 0 || (rect.bottom - rect.top) <= 0)
    {
        return false;
    }

    try 
    {
        // Try catch is just for extra precaution in case something
        // goes wrong in the creation of the Direct3D renderer.

        if (m_bD3D10)
        {
            // Create D3D10 renderer.
            NiD3D10Renderer::CreationParameters kParams(m_hRendererWnd);
            kParams.m_bAssociateWithWindow = false;
            NiD3D10RendererPtr spD3D10Renderer;
            bool bSuccess = NiD3D10Renderer::Create(kParams, spD3D10Renderer);
            if (bSuccess)
            {
                m_pkRenderer = spD3D10Renderer;
                MInitRefObject(m_pkRenderer);

                m_pkMainRenderTarget =
                    spD3D10Renderer->GetSwapChainRenderTargetGroup(
                    m_hRendererWnd);
                MAssert(m_pkMainRenderTarget != NULL, "Swap chain render "
                    "target group not found!");
            }
            else
            {
                ::MessageBox(m_hRendererWnd, "A D3D10 renderer could not be "
                    "created. Using DX9 instead.", "D3D10 Renderer Creation "
                    "Failure", MB_OK | MB_ICONEXCLAMATION);
                m_bD3D10 = false;
            }
        }
        if (!m_bD3D10)
        {
            // Create DX9 renderer.
			m_pkRenderer = NiDX9Renderer::Create(0, 0,
				NiDX9Renderer::USE_STENCIL | 
				NiDX9Renderer::USE_MANUALDEPTHSTENCIL, 
				hTopLevelWnd, hTopLevelWnd, D3DADAPTER_DEFAULT, 
				NiDX9Renderer::DEVDESC_HAL_HWVERTEX);

            if (m_pkRenderer)
            {
                MInitRefObject(m_pkRenderer);

                // Create swap chain render target group for render window.
                NiDX9Renderer* pkDX9Renderer = (NiDX9Renderer*) m_pkRenderer;
                if (pkDX9Renderer->CreateSwapChainRenderTargetGroup(
                    NiDX9Renderer::USE_STENCIL, m_hRendererWnd))
                {
                    m_pkMainRenderTarget =
                        pkDX9Renderer->GetSwapChainRenderTargetGroup(
                        m_hRendererWnd);
                    MAssert(m_pkMainRenderTarget != NULL, "Swap chain render "
                        "target group not found!");
                }
                else
                {
                    MDisposeRefObject(m_pkRenderer);
                    m_pkRenderer = NULL;
                }

                pkDX9Renderer->AddLostDeviceNotificationFunc(
                    RendererInfo::OnDeviceLost, NULL);
                pkDX9Renderer->AddResetNotificationFunc(
                    RendererInfo::OnDeviceReset, NULL);
            }
        }

        if (m_pkRenderer == NULL)
        {
            return false;
        }

        m_pmRenderingContext->GetRenderingContext()->m_pkRenderer =
            m_pkRenderer;
    }
    catch (Exception*)
    {
        return false;
    }

    // Background color for renderer
    RegisterForBackgroundColorSetting();

    CreateShaderSystem();

    if (!m_bD3D10)
    {
        HMODULE hCgCheck = LoadLibrary("CgD3D9.dll");
        if (hCgCheck)
        {
            FreeLibrary(hCgCheck);

            // Load Cg Shader Library, if it exists
            const char* const pcCgShaderLibName = "NiCgShaderLib"
                "DX9"
                NI_DLL_SUFFIX
                ".dll";

            m_hNiCgShaderLib = LoadLibrary(pcCgShaderLibName);
            if (m_hNiCgShaderLib)
            {
                unsigned int (*pfnGetCompilerVersionFunc)(void) =
                    (unsigned int (*)(void))GetProcAddress(m_hNiCgShaderLib, 
                    "GetCompilerVersion");
                if (pfnGetCompilerVersionFunc)
                {
                    unsigned int uiPluginVersion = pfnGetCompilerVersionFunc();
                    if (uiPluginVersion != (_MSC_VER))
                    {
                        FreeLibrary(m_hNiCgShaderLib);
                    }
                }
            }
        }
    }

    ReloadShaders();

    float fAspectRatio = (float) Width / Height;
    for (unsigned int ui = 0; ui < MViewportManager::Instance->ViewportCount;
        ui++)
    {
        MViewport* pmViewport = MViewportManager::Instance->GetViewport(ui);
        MCameraManager::Instance->SetAspectRatioOnStandardCameras(pmViewport,
            fAspectRatio);
        pmViewport->CreateScreenConsole();
    }

	// 初始化 DepthAlphaEffect. Added by Shi Yazheng
	CDepthAlphaEffectManager::GetInstance()->Initialize(m_pkRenderer);	

    CreateRenderedTextures();
    CreateScreenQuad();

    if (m_bNeedsPrecache)
    {
        PerformPrecache(MFramework::Instance->Scene);
    }

	// 迁移到 RenderMode 的渲染中处理

	// 创建 visual tracker
	//CreateVisualTracker(0.25f, 500.0f);
	/// renderer 初始化完成后才可以初始化 Depth alpha effect 
	//MFramework::Instance->InitDAEffectManager(m_pkRenderer);

    return true;
}
//---------------------------------------------------------------------------
void MRenderer::ChangeVisualTracker()
{
	// 每次执行都将 style 递增1。 在 0~5 之间循环
	static int s_iVisualTrackerStyle = -1;
	s_iVisualTrackerStyle++;
	if (s_iVisualTrackerStyle == 6)
	{
		s_iVisualTrackerStyle = -1;
	}

	if (s_iVisualTrackerStyle == -1)
	{
		m_bShowVisualTracker = false;
		return;
	}
	else
	{
		m_bShowVisualTracker = true;
	}

	// 6 个 style 数组
	float arrHeight[6] = {0.25f, 0.25f, 0.5f, 0.5f, 0.75f, 0.75f};
	float arrMax[6] = {100, 200, 500, 1000, 5000, 10000};

	CreateVisualTracker(arrHeight[s_iVisualTrackerStyle], arrMax[s_iVisualTrackerStyle]);
}
//---------------------------------------------------------------------------
void MRenderer::CreateVisualTracker(float fHeight, float fMaxValue)
{
	// 清理之前的 VisualTrackerOutput
	if (m_pkVisualTrackerOutput)
	{
		NiMetricsLayer::RemoveOutputModule(m_pkVisualTrackerOutput);
		MDisposeRefObject(m_pkVisualTrackerOutput);
	}

	if (m_pkVisualTracker)
	{
		m_pkVisualTracker->RemoveAll();
		MDisposeRefObject(m_pkVisualTracker);
	}

	// Create the visual tracker
	NiRect<float> kRect;
	kRect.m_left=0.02f; 
	kRect.m_right = 1.0f;
	kRect.m_top = (1.0f - fHeight);
	kRect.m_bottom = 0.99f;
	m_pkVisualTracker = NiNew NiVisualTracker(fMaxValue, 2, kRect, "Scene Metric");
	MInitRefObject(m_pkVisualTracker);

	const unsigned int uiVTFramePeriod = 6;
	m_pkVisualTrackerOutput = NiNew NiVisualTrackerOutput(uiVTFramePeriod);
	MInitRefObject(m_pkVisualTrackerOutput);

	NiMetricsLayer::AddOutputModule(m_pkVisualTrackerOutput);
	NiMetricsLayer::Update();

	m_pkVisualTracker->RemoveAll();

	m_pkVisualTrackerOutput->AddGraph(m_pkVisualTracker, 
		NiDx9RendererMetrics::ms_acNames[
			NiDx9RendererMetrics::TEXTURE_CHANGES],
				NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(0.0f, 0.5f, 0.0f),
				100, 0.5f, true, 1.0f, false, "Tex Changes");

	m_pkVisualTrackerOutput->AddGraph(m_pkVisualTracker, 
		NiDx9RendererMetrics::ms_acNames[
			NiDx9RendererMetrics::VERTEX_SHADER_CHANGES],
				NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(0.0f, 0.0f, 0.0f),
				100, 0.5f, true, 1.0f, false, "VS Changes");

	m_pkVisualTrackerOutput->AddGraph(m_pkVisualTracker, 
		NiDx9RendererMetrics::ms_acNames[
			NiDx9RendererMetrics::PIXEL_SHADER_CHANGES],
				NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(1.0f, 1.0f, 1.0f),
				100, 0.5f, true, 1.0f, false, "PS Changes");
		
	// Add visual tracker graphs
	m_pkVisualTrackerOutput->AddGraph(m_pkVisualTracker, 
		NiDx9RendererMetrics::ms_acNames[
			NiDx9RendererMetrics::DRAW_TRIS],
				NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(0.0f, 1.0f, 0.0f),
				100, 0.5f, true, 1000.0f, false, "TRIS/1000");

	m_pkVisualTrackerOutput->AddGraph(m_pkVisualTracker, 
		NiDx9RendererMetrics::ms_acNames[
			NiDx9RendererMetrics::RENDER_STATE_CHANGES],
				NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(0.0f, 0.0f, 1.0f),
				100, 0.5f, true, 1.0f, false, "RState Changes");

	m_pkVisualTrackerOutput->AddGraph(m_pkVisualTracker, 
		NiDx9RendererMetrics::ms_acNames[
			NiDx9RendererMetrics::DRAW_PRIMITIVE],
				NiVisualTrackerOutput::FUNC_SUM_PER_FRAME, NiColor(1.0f, 0.0f, 0.0f),
				100, 0.5f, true, 1.0f, false, "DrawCalls");

}
//---------------------------------------------------------------------------
bool MRenderer::Recreate(IntPtr hRendererWndPtr)
{
    MVerifyValidInstance;

    // We must have a renderer first.
    if (m_pkRenderer == NULL)
    {
        return false;
    }

    m_bNeedsRecreate = true;

    HWND hNewRendererWnd = (HWND) hRendererWndPtr.ToInt32();

    if (m_bD3D10)
    {
        NiD3D10Renderer* pkD3D10Renderer = (NiD3D10Renderer*) m_pkRenderer;
        NIASSERT(pkD3D10Renderer);
        NiD3D10Renderer::CreationParameters kParams;
        pkD3D10Renderer->GetCreationParameters(kParams);
        if (hNewRendererWnd != m_hRendererWnd)
        {
            NiD3D10Renderer::CreationParameters kNewParams(hNewRendererWnd);

            if (!pkD3D10Renderer->CreateSwapChainRenderTargetGroup(
                kNewParams.m_kSwapChain))
            {
                return false;
            }
            m_pkMainRenderTarget = 
                pkD3D10Renderer->GetSwapChainRenderTargetGroup(
                hNewRendererWnd);
            MAssert(m_pkMainRenderTarget != NULL, "Swap chain render target "
                "group not found!");
            pkD3D10Renderer->DestroySwapChainRenderTargetGroup(m_hRendererWnd);
            pkD3D10Renderer->SetDefaultSwapChainRenderTargetGroup(
                hNewRendererWnd);
        }

        pkD3D10Renderer->ResizeBuffers(0, 0, hNewRendererWnd);
    }
    else
    {
        NIASSERT(NiIsKindOf(NiDX9Renderer, m_pkRenderer));
        NiDX9Renderer* pkDX9Renderer = (NiDX9Renderer*) m_pkRenderer;
        pkDX9Renderer->DestroySwapChainRenderTargetGroup(m_hRendererWnd);
        if (!pkDX9Renderer->CreateSwapChainRenderTargetGroup(
            NiDX9Renderer::USE_STENCIL, hNewRendererWnd))
        {
            return false;
        }
        m_pkMainRenderTarget = pkDX9Renderer->GetSwapChainRenderTargetGroup(
            hNewRendererWnd);
        MAssert(m_pkMainRenderTarget != NULL, "Swap chain render target group "
            "not found!");

        RendererInfo::m_bDeviceReset = false;
    }

    m_hRendererWnd = hNewRendererWnd;

    float fAspectRatio = (float) Width / Height;
    for (unsigned int ui = 0; ui < MViewportManager::Instance->ViewportCount;
        ui++)
    {
        MViewport* pmViewport = MViewportManager::Instance->GetViewport(ui);
        MCameraManager::Instance->SetAspectRatioOnStandardCameras(pmViewport,
            fAspectRatio);
        pmViewport->UpdateScreenConsole();
    }

    MViewportManager::Instance->OnResize();

    CreateRenderedTextures();

    m_bNeedsRecreate = false;

    return true;
}
//---------------------------------------------------------------------------
void MRenderer::Render()
{
    MVerifyValidInstance;

	// 记录时间戳
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("Enter MRenderer::Render()"));
#endif

    if (m_pkRenderer == NULL || m_bNeedsRecreate)
    {
        return;
    }

	NiMaterial* pkMaterial = NiRenderer::GetRenderer()->GetDefaultMaterial();

    // Allow the viewports to do any pre-processing
    MViewportManager::Instance->PreRender();

    // Begin the rendering frame.
    m_pkRenderer->BeginFrame();

    // Handle a lost DX9 device.
    if (RendererInfo::m_bDeviceLost)
    {
        m_pkRenderer->EndFrame();
        m_pkRenderer->DisplayFrame();
        return;
    }
    else if (RendererInfo::m_bDeviceReset)
    {
        Recreate(m_hRendererWnd);
    }
	//m_pkRenderer->SetBackgroundColor(NiColorA(0.0f, 0.0f, 0.0f, 0.0f));
    // Render unselected entities to the first rendered texture.
    m_pkRenderer->BeginUsingRenderTargetGroup(m_pkTarget1,
        NiRenderer::CLEAR_ALL);    

    MViewportManager::Instance->RenderUnselectedEntities();
    m_pkRenderer->EndUsingRenderTargetGroup();	

	// update post shader parameter and performrendering [6/15/2009 hemeng]
	UpDatePostShaderParameter();
	m_pkPostEffectMgr->PerformRendering();

	if (m_pkRenderer->IsRenderTargetGroupActive())
	{
		m_pkRenderer->EndUsingRenderTargetGroup();
	}
	// 记录时间戳
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MRenderer::Render() After Post Effect Render."));
#endif	
    // Render selected entities to the second rendered texture.
    if (SelectionService->NumSelectedEntities > 0)
    {
        // Clear second rendered texture using a shader that uses the colors
        // from the unselected entities texture but with an alpha value of
        // 0.0 for each pixel.
        NiColorA kOldClearColor;
        m_pkRenderer->GetBackgroundColor(kOldClearColor);
        m_pkRenderer->SetBackgroundColor(NiColorA(0.0f, 0.0f, 0.0f, 0.0f));
        m_pkRenderer->BeginUsingRenderTargetGroup(m_pkTarget2,
            NiRenderer::CLEAR_BACKBUFFER); 
        m_pkRenderer->SetBackgroundColor(kOldClearColor);
        m_pkRenderer->SetScreenSpaceCameraData();
        m_pkScreenQuadTexProp->SetBaseTexture(m_pkTexture1);
        const NiMaterial* pkOldMaterial = m_pkScreenQuad->GetActiveMaterial();
        m_pkScreenQuad->SetActiveMaterial(m_pkScreenQuadShaderMaterial);
        m_pkScreenQuad->Draw(m_pkRenderer);
        m_pkScreenQuad->SetActiveMaterial(pkOldMaterial);

        // Render selected entities to cleared second texture.
        MViewportManager::Instance->RenderSelectedEntities();
        m_pkRenderer->EndUsingRenderTargetGroup();
    }

    // Switch to main render target group.
    m_pkRenderer->BeginUsingRenderTargetGroup(m_pkMainRenderTarget,
        NiRenderer::CLEAR_ALL);
    m_pkRenderer->SetScreenSpaceCameraData();

    // Render screen quad with first texture.
    m_pkScreenQuadTexProp->SetBaseTexture(m_pkTexture1);
    m_pkScreenQuadAlphaProp->SetAlphaBlending(false);
    m_pkScreenQuad->Draw(m_pkRenderer);

    // Render screen quad with second texture.
    if (SelectionService->NumSelectedEntities > 0)
    {
        // Render quad with standard alpha blending.
        m_pkScreenQuadTexProp->SetBaseTexture(m_pkTexture2);
        m_pkScreenQuadStencilProp->SetStencilOn(true);
        m_pkScreenQuadStencilProp->SetStencilFunction(
            NiStencilProperty::TEST_ALWAYS);
        m_pkScreenQuadStencilProp->SetStencilReference(1);
        m_pkScreenQuadStencilProp->SetStencilPassAction(
            NiStencilProperty::ACTION_REPLACE);
        m_pkScreenQuadAlphaProp->SetAlphaBlending(true);
        m_pkScreenQuadAlphaProp->SetSrcBlendMode(
            NiAlphaProperty::ALPHA_SRCALPHA);
        m_pkScreenQuadAlphaProp->SetDestBlendMode(
            NiAlphaProperty::ALPHA_INVSRCALPHA);
        m_pkScreenQuadAlphaProp->SetAlphaTesting(true);
        m_pkScreenQuad->Draw(m_pkRenderer);
        m_pkScreenQuadAlphaProp->SetAlphaTesting(false);

        // Render quad with vertex colors using the stencil buffer to
        // highlight the selected entities.
        m_pkScreenQuadTexProp->SetBaseTexture(NULL);
        NiTexturingProperty::ApplyMode eOldApplyMode =
            m_pkScreenQuadTexProp->GetApplyMode();
        m_pkScreenQuadTexProp->SetApplyMode(
            NiTexturingProperty::APPLY_MODULATE);
        m_pkScreenQuadStencilProp->SetStencilFunction(
            NiStencilProperty::TEST_EQUAL);
        m_pkScreenQuadStencilProp->SetStencilReference(1);
        m_pkScreenQuadStencilProp->SetStencilPassAction(
            NiStencilProperty::ACTION_KEEP);
        NiVertexColorProperty::SourceVertexMode eOldSourceMode =
            m_pkScreenQuadVertexColorProp->GetSourceMode();
        NiVertexColorProperty::LightingMode eOldLightingMode =
            m_pkScreenQuadVertexColorProp->GetLightingMode();
        m_pkScreenQuadVertexColorProp->SetSourceMode(
            NiVertexColorProperty::SOURCE_EMISSIVE);
        m_pkScreenQuadVertexColorProp->SetLightingMode(
            NiVertexColorProperty::LIGHTING_E);
        m_pkScreenQuad->Draw(m_pkRenderer);
        m_pkScreenQuadVertexColorProp->SetLightingMode(eOldLightingMode);
        m_pkScreenQuadVertexColorProp->SetSourceMode(eOldSourceMode);
        m_pkScreenQuadTexProp->SetApplyMode(eOldApplyMode);
        m_pkScreenQuadStencilProp->SetStencilOn(false);
    }
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MRenderer::Render() After Selected Entity Render."));
#endif	

	// [11/15/2009 shiyazheng]
	// render visual track
	if (m_bShowVisualTracker)
	{
		m_pkVisualTracker->Update();
		m_pkVisualTracker->Draw();
	}

    // Render gizmo and screen elements for each viewport.
    MViewportManager::Instance->RenderGizmo();
    MViewportManager::Instance->RenderScreenElements();


#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MRenderer::Render() After Screen Elements Render."));
#endif	

    // End the rendering frame.
    m_pkRenderer->EndUsingRenderTargetGroup();
    m_pkRenderer->EndFrame();


#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MRenderer::Render() After EndFrame()."));
#endif	
    // Display the frame.
    m_pkRenderer->DisplayFrame();
	// 记录时间戳
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("Exit MRenderer::Render()"));
#endif
}
//---------------------------------------------------------------------------
void MRenderer::CreateShaderSystem()
{
    NiMaterialToolkit* pkShaderKit = NiMaterialToolkit::CreateToolkit();

//#if _MSC_VER >= 1400
//    size_t iSize = 0;
//    getenv_s(&iSize, NULL, 0, "EGB_SHADER_LIBRARY_PATH");
//    NIASSERT(iSize > 0);
//
//    char* pcShaderPath = NiExternalNew char[iSize];
//
//    if (pcShaderPath)
//    {
//        getenv_s(&iSize, pcShaderPath, iSize, "EGB_SHADER_LIBRARY_PATH");
//    }
//#else
//    char* pcShaderPath = getenv("EGB_SHADER_LIBRARY_PATH");
//#endif
	//OutputDebugString( pcShaderPath );
	char pcShaderPath[MAX_PATH] = "shaders";
    if (pcShaderPath == NULL)
    {
        const char* pcWarning = "The environment variable "
            "EGB_SHADER_LIBRARY_PATH must\nbe defined for this application"
            " to properly execute.";
        ::MessageBox(NULL, pcWarning, "Missing Environment Variable",
            MB_OK | MB_ICONERROR);
        MessageService->AddMessage(MessageChannelType::Errors, pcWarning);
    }
    else
    {
        // Load shader libraries.
        pkShaderKit->LoadFromDLL(pcShaderPath);

        // Add Gamebryo shader path.
        char acShaderDir[2048];
        NiSprintf(acShaderDir, 2048, "%s%s", pcShaderPath, "\\Data\\");
        if (m_bD3D10)
        {
            NiStrcat(acShaderDir, 2048, "D3D10\\");
        }
        else
        {
            NiStrcat(acShaderDir, 2048, "DX9\\");
        }
        NiMaterialToolkit::GetToolkit()->SetMaterialDirectory(acShaderDir);
		//NiMaterial* litMapMat = NiLightMapMaterial::GetMaterial();
    }
}
//---------------------------------------------------------------------------
void MRenderer::ReloadShaders()
{
    // Unregister shaders in the main scene.
    MScene* pmScene = MFramework::Instance->Scene;
    MEntity* amEntities[] = pmScene->GetEntities();
    for (int i = 0; i < amEntities->Length; i++)
    {
        MEntity* pmEntity = amEntities[i];
        unsigned int uiSceneRootCount = pmEntity->GetSceneRootPointerCount();
        for (unsigned int ui = 0; ui < uiSceneRootCount; ui++)
        {
            NiAVObject* pkSceneRoot = pmEntity->GetSceneRootPointer(ui);
            if (pkSceneRoot)
            {
                NiMaterialHelpers::UnRegisterMaterials(pkSceneRoot, 
                    m_pkRenderer, true);
            }
        }
    }
    NiMaterialToolkit::UnloadShaders();

    // Purge all existing renderer data for the main scene.
    for (int i = 0; i < amEntities->Length; i++)
    {
        MEntity* pmEntity = amEntities[i];
        unsigned int uiSceneRootCount = pmEntity->GetSceneRootPointerCount();
        for (unsigned int ui = 0; ui < uiSceneRootCount; ui++)
        {
            NiAVObject* pkSceneRoot = pmEntity->GetSceneRootPointer(ui);
            if (pkSceneRoot)
            {
                m_pkRenderer->PurgeAllRendererData(pkSceneRoot);
            }
        }
    }

    // Reload screen texture pixel data to avoid crash during rendering.
    for (unsigned int ui = 0; ui < MViewportManager::Instance->ViewportCount;
        ui++)
    {
        MViewportManager::Instance->GetViewport(ui)
            ->ReloadScreenConsolePixelData();
    }

    NiMaterialToolkit::ReloadShaders();

    // Re-register all existing shaders for main scene.
    bool bSet = false;
    pmScene = MFramework::Instance->Scene;
    amEntities = pmScene->GetEntities();
    for (int i = 0; i < amEntities->Length; i++)
    {
        MEntity* pmEntity = amEntities[i];
        unsigned int uiSceneRootCount = pmEntity->GetSceneRootPointerCount();
        for (unsigned int ui = 0; ui < uiSceneRootCount; ui++)
        {
            NiAVObject* pkSceneRoot = pmEntity->GetSceneRootPointer(ui);
            if (pkSceneRoot)
            {
                NiMaterialHelpers::RegisterMaterials(pkSceneRoot,
                    m_pkRenderer);
                bSet = true;
            }
        }
    }

    // Re-register all existing shaders for the tool scene.
    pmScene = MFramework::Instance->Scene;
    amEntities = pmScene->GetEntities();
    for (int i = 0; i < amEntities->Length; i++)
    {
        MEntity* pmEntity = amEntities[i];
        unsigned int uiSceneRootCount = pmEntity->GetSceneRootPointerCount();
        for (unsigned int ui = 0; ui < uiSceneRootCount; ui++)
        {
            NiAVObject* pkSceneRoot = pmEntity->GetSceneRootPointer(ui);
            if (pkSceneRoot)
            {
                NiMaterialHelpers::RegisterMaterials(pkSceneRoot,
                    m_pkRenderer);
                bSet = true;
            }
        }
    }

    // If we don't have any entities, we still need to register the shader
    // library directories.
    if (!bSet)
    {
        NiMaterialToolkit::UpdateMaterialDirectory();
    }
}
//---------------------------------------------------------------------------
void MRenderer::CreateRenderedTextures()
{
    MVerifyValidInstance;

    MAssert(m_pkRenderer != NULL, "Renderer not created!");

    NiTexture::FormatPrefs kPrefs;
    kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;
    kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
    kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::SMOOTH;

    MDisposeRefObject(m_pkTexture1);
    m_pkTexture1 = NiRenderedTexture::Create(Width, Height, m_pkRenderer,
        kPrefs);
    MInitRefObject(m_pkTexture1);

    MDisposeRefObject(m_pkTexture2);
    m_pkTexture2 = NiRenderedTexture::Create(Width, Height, m_pkRenderer,
        kPrefs);
    MInitRefObject(m_pkTexture2);

    MDisposeRefObject(m_pkTarget1);
	// 使用 DAEffectMgr 创建 RTG
	m_pkTarget1 = CDepthAlphaEffectManager::GetInstance()->CreateRenderTargetGroup(m_pkTexture1);	
	//m_pkTarget1 = NiRenderTargetGroup::Create(m_pkTexture1->GetBuffer(), m_pkRenderer, true, true);
    MInitRefObject(m_pkTarget1);

    NiDepthStencilBuffer* pkDSB = m_pkTarget1->GetDepthStencilBuffer();
    NIASSERT(pkDSB);

    MDisposeRefObject(m_pkTarget2);
    m_pkTarget2 = NiRenderTargetGroup::Create(m_pkTexture2->GetBuffer(),
        m_pkRenderer, pkDSB);
    MInitRefObject(m_pkTarget2);

	////080902 add by 和萌
	//post effect:添加effect到manager中，初始化manager
	m_bInitPostMgr = m_pkPostEffectMgr->Init(m_pkTexture1);
	UpDatePostShaderParameter();

}
//---------------------------------------------------------------------------
void MRenderer::CreateScreenQuad()
{
    MVerifyValidInstance;

    if (m_pkScreenQuad)
    {
        return;
    }

    MAssert(m_pkRenderer != NULL, "Renderer not created!");

    // Create screen quad.
    m_pkScreenQuad = NiNew NiScreenElements(NiNew NiScreenElementsData(false,
        true, 1));
    MInitRefObject(m_pkScreenQuad);

    // Add single polygon.
    int iPolygon = m_pkScreenQuad->Insert(4);
    MAssert(iPolygon == 0, "Unexpected screen quad polygon index!");
    m_pkScreenQuad->SetRectangle(0, 0.0f, 0.0f, 1.0f, 1.0f);
    m_pkScreenQuad->UpdateBound();
    m_pkScreenQuad->SetTextures(0, 0, 0.0f, 0.0f, 1.0f, 1.0f);
    SetScreenQuadVertexColors();

    // Create and add texturing property.
    m_pkScreenQuadTexProp = NiNew NiTexturingProperty();
    MInitRefObject(m_pkScreenQuadTexProp);
    m_pkScreenQuadTexProp->SetApplyMode(NiTexturingProperty::APPLY_REPLACE);
    m_pkScreenQuadTexProp->SetBaseMap(NiNew NiTexturingProperty::Map());
    m_pkScreenQuadTexProp->SetBaseFilterMode(
        NiTexturingProperty::FILTER_NEAREST);
    m_pkScreenQuadTexProp->SetBaseClampMode(
        NiTexturingProperty::CLAMP_S_CLAMP_T);
    m_pkScreenQuad->AttachProperty(m_pkScreenQuadTexProp);

    // Create and add alpha property.
    m_pkScreenQuadAlphaProp = NiNew NiAlphaProperty();
    MInitRefObject(m_pkScreenQuadAlphaProp);
    m_pkScreenQuadAlphaProp->SetAlphaBlending(false);
    m_pkScreenQuadAlphaProp->SetAlphaTesting(false);
    m_pkScreenQuadAlphaProp->SetTestMode(NiAlphaProperty::TEST_GREATER);
    m_pkScreenQuadAlphaProp->SetTestRef(0);
    m_pkScreenQuad->AttachProperty(m_pkScreenQuadAlphaProp);

    // Create and add vertex color property.
    m_pkScreenQuadVertexColorProp = NiNew NiVertexColorProperty();
    MInitRefObject(m_pkScreenQuadVertexColorProp);
    m_pkScreenQuadVertexColorProp->SetSourceMode(
        NiVertexColorProperty::SOURCE_IGNORE);
    m_pkScreenQuadVertexColorProp->SetLightingMode(
        NiVertexColorProperty::LIGHTING_E_A_D);
    m_pkScreenQuad->AttachProperty(m_pkScreenQuadVertexColorProp);

    // Create and add stencil property.
    m_pkScreenQuadStencilProp = NiNew NiStencilProperty();
    MInitRefObject(m_pkScreenQuadStencilProp);
    m_pkScreenQuadStencilProp->SetStencilOn(false);
    m_pkScreenQuad->AttachProperty(m_pkScreenQuadStencilProp);

    // Create and add z-buffer property.
    NiZBufferProperty* pkZBufferProp = NiNew NiZBufferProperty();
    pkZBufferProp->SetZBufferTest(false);
    pkZBufferProp->SetZBufferWrite(false);
    m_pkScreenQuad->AttachProperty(pkZBufferProp);

    // Create screen quad shader material.
    m_pkScreenQuadShaderMaterial = NiSingleShaderMaterial::Create(
        "TexColorNoAlpha");
    MInitRefObject(m_pkScreenQuadShaderMaterial);
    // Don't set it as active yet
    m_pkScreenQuad->ApplyMaterial(m_pkScreenQuadShaderMaterial);

    // Perform initial update.
    m_pkScreenQuad->Update(0.0f);
    m_pkScreenQuad->UpdateProperties();
    m_pkScreenQuad->UpdateEffects();
    m_pkScreenQuad->UpdateNodeBound();
}
//---------------------------------------------------------------------------
void MRenderer::SetScreenQuadVertexColors()
{
    MVerifyValidInstance;

    if (m_pkScreenQuad)
    {
        if (!m_pkHighlightColor)
        {
            RegisterForHighlightColorSetting();
            RegisterForTransparencySetting();
        }

        m_pkScreenQuad->SetColors(0, *m_pkHighlightColor, *m_pkHighlightColor,
            *m_pkHighlightColor, *m_pkHighlightColor);
    }
}
//---------------------------------------------------------------------------
void MRenderer::PerformPrecache(MScene* pmScene)
{
    MAssert(pmScene != NULL, "Null scene provided to function!");

    if (m_pkRenderer)
    {
        MEntity* amAllEntitiesInScene[] = pmScene->GetEntities();  
        for (int i = 0; i < amAllEntitiesInScene->Length; i++)
        {
            PrecacheEntity(amAllEntitiesInScene[i]);
        }
        m_bNeedsPrecache = false;
    }
    else
    {
        m_bNeedsPrecache = true;
    }
}
//---------------------------------------------------------------------------
void MRenderer::PerformPrecache(MEntity* pmEntity)
{
    MAssert(pmEntity != NULL, "Null entity provided to function!");

    if (m_pkRenderer)
    {
        PrecacheEntity(pmEntity);
        m_bNeedsPrecache = false;
    }
    else
    {
        m_bNeedsPrecache = true;
    }
}
//---------------------------------------------------------------------------
void MRenderer::PrecacheEntity(MEntity* pmEntity)
{
    MAssert(pmEntity != NULL, "Null entity provided to function!");

    for (unsigned int ui = 0; ui < pmEntity->GetSceneRootPointerCount(); ui++)
    {
        NiAVObject* pkSceneRoot = pmEntity->GetSceneRootPointer(ui);
        if (pkSceneRoot)
        {
            RecursivePrecacheScene(pkSceneRoot);
        }
    }
}
//---------------------------------------------------------------------------
void MRenderer::RecursivePrecacheScene(NiAVObject* pkObject)
{
    NIASSERT(pkObject);

    // Apply the "Scene" shader to the geometry
    if (NiIsKindOf(NiGeometry, pkObject))
    {
        NiGeometry* pkGeometry = (NiGeometry*)pkObject;
        NiGeometryData* pkData = pkGeometry->GetModelData();
        if (pkData)
            pkData->SetKeepFlags(NiGeometryData::KEEP_ALL);
        m_pkRenderer->PrecacheGeometry(pkGeometry, 0, 0);
    }
    else if (NiIsKindOf(NiNode, pkObject))
    {
        // Recurse to children
        NiNode* pkNode = (NiNode*)pkObject;
        unsigned int uiChildCount = pkNode->GetArrayCount();
        for (unsigned int i = 0; i < uiChildCount; i++)
        {
            NiAVObject* pkChild = pkNode->GetAt(i);
            if (pkChild)
            {
                RecursivePrecacheScene(pkChild);
            }
        }
    }
}
//---------------------------------------------------------------------------
void MRenderer::RegisterForHighlightColorSetting()
{
    MVerifyValidInstance;

    if (!m_pkHighlightColor)
    {
        m_pkHighlightColor = NiNew NiColorA();
    }

    SettingsService->RegisterSettingsObject(ms_strHighlightColorSettingName,
        __box(Color::Yellow), SettingsCategory::PerUser);
    SettingsService->SetChangedSettingHandler(ms_strHighlightColorSettingName,
        SettingsCategory::PerUser, new SettingChangedHandler(this,
        &MRenderer::OnHighlightColorChanged));
    OnHighlightColorChanged(NULL, NULL);

    OptionsService->AddOption(ms_strHighlightColorOptionName,
        SettingsCategory::PerUser, ms_strHighlightColorSettingName);
    OptionsService->SetHelpDescription(ms_strHighlightColorOptionName,
        "The color with which selected entities are highlighted.");
}
//---------------------------------------------------------------------------
void MRenderer::OnHighlightColorChanged(Object* pmSender,
    SettingChangedEventArgs* pmEventArgs)
{
    MVerifyValidInstance;

    __box Color* pmColor = dynamic_cast<__box Color*>(
        SettingsService->GetSettingsObject(ms_strHighlightColorSettingName,
        SettingsCategory::PerUser));
    if (pmColor != NULL)
    {
        m_pkHighlightColor->r = MUtility::RGBToFloat((*pmColor).R);
        m_pkHighlightColor->g = MUtility::RGBToFloat((*pmColor).G);
        m_pkHighlightColor->b = MUtility::RGBToFloat((*pmColor).B);
        SetScreenQuadVertexColors();
    }
}
//---------------------------------------------------------------------------
void MRenderer::RegisterForTransparencySetting()
{
    MVerifyValidInstance;

    if (!m_pkHighlightColor)
    {
        m_pkHighlightColor = NiNew NiColorA();
    }

    SettingsService->RegisterSettingsObject(ms_strTransparencySettingName,
        __box(0.25f), SettingsCategory::PerUser);
    SettingsService->SetChangedSettingHandler(ms_strTransparencySettingName,
        SettingsCategory::PerUser, new SettingChangedHandler(this,
        &MRenderer::OnTransparencyChanged));
    OnTransparencyChanged(NULL, NULL);

    OptionsService->AddOption(ms_strTransparencyOptionName,
        SettingsCategory::PerUser, ms_strTransparencySettingName);
    OptionsService->SetHelpDescription(ms_strTransparencyOptionName,
        "The transparency of the selection highlighting. This should be a "
        "number between 0 and 1, where 0 is transparent and 1 is opaque.");
}
//---------------------------------------------------------------------------
void MRenderer::OnTransparencyChanged(Object* pmSender,
    SettingChangedEventArgs* pmEventArgs)
{
    MVerifyValidInstance;

    __box float* pmTransparency = dynamic_cast<__box float*>(
        SettingsService->GetSettingsObject(ms_strTransparencySettingName,
        SettingsCategory::PerUser));
    if (pmTransparency != NULL)
    {
        m_pkHighlightColor->a = *pmTransparency;
        SetScreenQuadVertexColors();
    }
}
//---------------------------------------------------------------------------
void MRenderer::RegisterForBackgroundColorSetting()
{
    MVerifyValidInstance;

    SettingsService->RegisterSettingsObject(ms_strBackgroundColorSettingName,
        __box(this->BackgroundColor), SettingsCategory::PerUser);
    SettingsService->SetChangedSettingHandler(ms_strBackgroundColorSettingName,
        SettingsCategory::PerUser, new SettingChangedHandler(this,
        &MRenderer::OnBackgroundColorChanged));
    OnBackgroundColorChanged(NULL, NULL);

    OptionsService->AddOption(ms_strBackgroundColorOptionName,
        SettingsCategory::PerUser, ms_strBackgroundColorSettingName);
    OptionsService->SetHelpDescription(ms_strBackgroundColorOptionName,
        "The background color that is drawn in the viewports.");
}
//---------------------------------------------------------------------------
void MRenderer::OnBackgroundColorChanged(Object* pmSender,
    SettingChangedEventArgs* pmEventArgs)
{
    MVerifyValidInstance;

    __box Color* pmColor = dynamic_cast<__box Color*>(
        SettingsService->GetSettingsObject(ms_strBackgroundColorSettingName,
        SettingsCategory::PerUser));
    if (pmColor != NULL)
    {
        this->BackgroundColor = *pmColor;
    }
}
//---------------------------------------------------------------------------
IRenderingModeService* MRenderer::get_RenderingModeService()
{
    if (ms_pmRenderingModeService == NULL)
    {
        ms_pmRenderingModeService = MGetService(IRenderingModeService);
        MAssert(ms_pmRenderingModeService != NULL, "Rendering mode service "
            "not found!");
    }
    return ms_pmRenderingModeService;
}
//---------------------------------------------------------------------------
IInteractionModeService* MRenderer::get_InteractionModeService()
{
    if (ms_pmInteractionModeService == NULL)
    {
        ms_pmInteractionModeService = MGetService(IInteractionModeService);
        MAssert(ms_pmInteractionModeService != NULL, "Interaction mode "
            "service not found!");
    }
    return ms_pmInteractionModeService;
}
//---------------------------------------------------------------------------
IMessageService* MRenderer::get_MessageService()
{
    if (ms_pmMessageService == NULL)
    {
        ms_pmMessageService = MGetService(IMessageService);
        MAssert(ms_pmMessageService != NULL, "Message service "
            "not found!");
    }
    return ms_pmMessageService;
}
//---------------------------------------------------------------------------
ISelectionService* MRenderer::get_SelectionService()
{
    if (ms_pmSelectionService == NULL)
    {
        ms_pmSelectionService = MGetService(ISelectionService);
        MAssert(ms_pmSelectionService != NULL, "Selection service "
            "not found!");
    }
    return ms_pmSelectionService;
}
//---------------------------------------------------------------------------
ISettingsService* MRenderer::get_SettingsService()
{
    if (ms_pmSettingsService == NULL)
    {
        ms_pmSettingsService = MGetService(ISettingsService);
        MAssert(ms_pmSettingsService != NULL, "Settings service not found!");
    }
    return ms_pmSettingsService;
}
//---------------------------------------------------------------------------
IOptionsService* MRenderer::get_OptionsService()
{
    if (ms_pmOptionsService == NULL)
    {
        ms_pmOptionsService = MGetService(IOptionsService);
        MAssert(ms_pmOptionsService != NULL, "Options service not found!");
    }
    return ms_pmOptionsService;
}

// initial post shader parameters [6/15/2009 hemeng]
void MRenderer::UpDatePostShaderParameter()
{
	if (m_pkPostEffectMgr != NULL && m_bInitPostMgr != false)
	{
		//初始化manager中effect
		if ( MGlobalSetting::Instance->GetColorAdjustEffectEnabled() )
		{
			m_pkColorAdjustEffect->SetActive(true);
			m_pkColorAdjustEffect->UpdateData(GetPostEffectByType(0),
				GetPostEffectByType(1),
				GetPostEffectByType(2),
				GetPostEffectByType(3));
		}
		else
		{
			m_pkColorAdjustEffect->SetActive(false);
		}
		if ( MGlobalSetting::Instance->GetBloomEffectEnabled())
		{
			m_pkBloomEffect->SetActive(true);
			float fTemp = GetBloomValueByType(0);
			m_pkBloomEffect->UpDateLum(GetBloomValueByType(0));
			m_pkBloomEffect->UpDateScale(GetBloomValueByType(1));
		}
		else
		{
			m_pkBloomEffect->SetActive(false);
		}

	}
}
//---------------------------------------------------------------------------
//// 是否显示统计数据
bool MRenderer::get_ShowVisualTracker()
{
	return m_bShowVisualTracker;
}	
//---------------------------------------------------------------------------
void MRenderer::set_ShowVisualTracker(bool bShow)
{
	m_bShowVisualTracker = bShow;
}
//---------------------------------------------------------------------------
// Unmanaged RendererInfo class that receives DX9 callbacks.
//---------------------------------------------------------------------------
#pragma unmanaged
//---------------------------------------------------------------------------
bool RendererInfo::m_bDeviceLost = false;
bool RendererInfo::m_bDeviceReset = false;
//---------------------------------------------------------------------------
bool RendererInfo::OnDeviceLost(void* pvData)
{
    m_bDeviceLost = true;
    return true;
}
//---------------------------------------------------------------------------
bool RendererInfo::OnDeviceReset(bool bBeforeReset, void* pvData)
{
    if (!bBeforeReset)
    {
        m_bDeviceReset = true;
        m_bDeviceLost = false;
    }
    return true;
}

//---------------------------------------------------------------------------
#pragma managed
//---------------------------------------------------------------------------
