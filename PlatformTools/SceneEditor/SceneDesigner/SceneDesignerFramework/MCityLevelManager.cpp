﻿#include "SceneDesignerFrameworkPCH.h"
#include "MCityLevelManager.h"
#include "MFramework.h"
#include "TerrainModifier.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

MCityLevelManager::MCityLevelManager()
{
	m_arrLevels = new MCityLevelSetting*[NUM_CITY_LEVELS];
}
//------------------------------------------------------------------------------
MCityLevelManager* MCityLevelManager::get_Instance()
{
	if (ms_Instance == NULL)
	{
		ms_Instance = new MCityLevelManager();
	}

	return ms_Instance;
}
//------------------------------------------------------------------------------
MCityLevelManager::MCityLevelSetting* MCityLevelManager::GetCityLevel(int iLevel)
{
	if (iLevel < 0 || iLevel >= NUM_CITY_LEVELS)
	{
		return NULL;
	}

	if (m_arrLevels[iLevel] == NULL)
	{
		m_arrLevels[iLevel] = new MCityLevelSetting(iLevel);
	}

	return m_arrLevels[iLevel];
}
//------------------------------------------------------------------------------
void MCityLevelManager::SetCityLevel(MCityLevelManager::MCityLevelSetting* pLevelSetting)
{
	if (pLevelSetting == NULL) return;

	m_arrLevels[pLevelSetting->m_iLevel] = pLevelSetting;
}
//------------------------------------------------------------------------------
void MCityLevelManager::ApplyLevel(int iLevel)
{
	MCityLevelManager::MCityLevelSetting* pLevelSetting = GetCityLevel(iLevel);
	if (pLevelSetting == NULL) return;

	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (pTerrain == NULL) return;

	// 遍历每层纹理
	for (int i=0; i<NUM_TERRAIN_TEXTURES; i++)
	{
		String* strTexName = pLevelSetting->m_arrTerrainTextures[i];
		if (strTexName != NULL)
		{
			const char* pszTexName = MStringToCharPointer(strTexName);
			CTerrainModifier::ApplyTextureToTerrain(pTerrain, i, pszTexName);
			MFreeCharPointer(pszTexName);
		}

	}
}
//------------------------------------------------------------------------------
void MCityLevelManager::SaveToXml(String* strFileName)
{
	if (strFileName == NULL) return;

	XmlDocument* xmlDoc = new XmlDocument();

	XmlDeclaration* xmlDecl = xmlDoc->CreateXmlDeclaration("1.0", "gb2312", "yes");
	xmlDoc->AppendChild(xmlDecl);

	// 根结点
	XmlElement* elmtLevelSettings = ToXml(xmlDoc);
	xmlDoc->AppendChild(elmtLevelSettings);

	xmlDoc->Save(strFileName);
}
//------------------------------------------------------------------------------
void MCityLevelManager::LoadFromXml(String* strFileName)
{
	if (strFileName==NULL || !File::Exists(strFileName))
	{
		return;
	}

	try
	{
		XmlDocument* xmlDoc = new XmlDocument();
		xmlDoc->Load(strFileName);

		// 清除原 LevelSetting
		for (int i=0; i<NUM_CITY_LEVELS; i++) {m_arrLevels[i] = NULL;}

		XmlNode* xmlNode = xmlDoc->FirstChild;
		xmlNode = xmlNode->NextSibling;

		XmlElement* elmtLevelSettings = dynamic_cast<XmlElement*>(xmlNode);
		if (elmtLevelSettings == NULL) return;

		XmlElement* elmtSetting = (XmlElement*)(elmtLevelSettings->FirstChild);
		while (elmtSetting != NULL)
		{
			MCityLevelSetting* pLevelSetting = MCityLevelSetting::FromXml(elmtSetting);
			if (pLevelSetting != NULL)
			{
				SetCityLevel(pLevelSetting);
			}

			elmtSetting = (XmlElement*)(elmtSetting->NextSibling);
		}
	}
	catch(Exception* e)
	{
		MessageBox::Show(e->ToString());
	}

}
//------------------------------------------------------------------------------
void MCityLevelManager::ClearAllLevels()
{
	for (int i=0; i<NUM_CITY_LEVELS; i++)
	{
		m_arrLevels[i] = NULL;
	}
}
//------------------------------------------------------------------------------
XmlElement* MCityLevelManager::ToXml(XmlDocument* xmlDoc)
{
	XmlElement* elmtLevelSettings = xmlDoc->CreateElement("CityLevelSettings");

	for (int i=0; i<NUM_CITY_LEVELS; i++)
	{
		MCityLevelSetting* setting = m_arrLevels[i];
		if (setting != NULL)
		{
			XmlElement* elmtSetting = setting->ToXml(xmlDoc);
			elmtLevelSettings->AppendChild(elmtSetting);
		}
	}

	return elmtLevelSettings;
}
//------------------------------------------------------------------------------
MCityLevelManager::MCityLevelSetting::MCityLevelSetting(int iLevel)
{
	m_iLevel = iLevel;
	m_arrTerrainTextures = new String*[NUM_TERRAIN_TEXTURES];
}
//------------------------------------------------------------------------------
MCityLevelManager::MCityLevelSetting* 
	MCityLevelManager::MCityLevelSetting::FromXml(XmlElement* elmtLevel)
{
	if (elmtLevel == NULL) return NULL;

	try
	{
		int iLevel = Int32::Parse(elmtLevel->GetAttribute("Level"));
		MCityLevelSetting* pSetting = new MCityLevelSetting(iLevel);
		for (int i=0; i<NUM_TERRAIN_TEXTURES; i++)
		{
			String* strAttName = String::Concat("Tex", i.ToString());
			String* strTexName = elmtLevel->GetAttribute(strAttName);
			if (strTexName->Length > 0)
			{
				pSetting->m_arrTerrainTextures[i] = strTexName;
			}
		}
		return pSetting;
	}
	catch(Exception*)
	{
		return NULL;
	}

}
//------------------------------------------------------------------------------
XmlElement* MCityLevelManager::MCityLevelSetting::ToXml(XmlDocument* xmlDoc)
{
	if (xmlDoc == NULL) return NULL;

	XmlElement* elmtSetting = xmlDoc->CreateElement("LevelSetting");
	elmtSetting->SetAttribute("Level", m_iLevel.ToString());
	
	for (int i=0; i<NUM_TERRAIN_TEXTURES; i++)
	{
		String* strAttrName = String::Concat("Tex", i.ToString());
		if (m_arrTerrainTextures[i] != NULL)
		{
			elmtSetting->SetAttribute(strAttrName, m_arrTerrainTextures[i]);
		}
	}

	return elmtSetting;
}
//------------------------------------------------------------------------------
