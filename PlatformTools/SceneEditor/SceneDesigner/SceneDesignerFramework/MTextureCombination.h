﻿#pragma once
#include <vcclr.h>
#pragma unmanaged
	#include "TerrainTextureMgr.h"
#pragma managed

using namespace System::Collections;
//using namespace SceneCore;
// 纹理合并工具
namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{

public __gc class MTextureCombination
{
public:
	// 获取所有纹理文件名
	void GetModelByTexture(String* textureName, String* modelArray[]);

	// 根据纹理尺寸获取纹理名称
	void GetTextureFileNamesBySize(int iSize, String* textureArray[]);

	// 设置目标纹理目录
	void SetTextureFolder(System::String* strTextureFolder);
	
	// 设置目标模型目录
	void SetModelFolder(System::String* strModelfolder);

	// 重新装载纹理/模型信息.重设了目标纹理,模型路径后调用
	void RelodeTextureModelInfo();

	// 预览纹理合并
	bool PreviewCombineTextures(String* strSourceTextures[], int iTargetSize, String* strCompressType);

	// 合并纹理并且修改相关模型的 UV 值
	bool CombineTexturesAndChangeModelUV(String* strSourceTextures[], 
		int iTargetSize, String* strCompressType, String* strCombinedTextureFullPath, bool bBackupOldModel);

public:
	// Singleton members.
	static void Init(System::Windows::Forms::Control* topLevelWnd, System::Windows::Forms::Control* renderWnd);
	static void Shutdown();
	static bool InstanceIsValid();
	__property static MTextureCombination* get_Instance();

private:
	static MTextureCombination* ms_pmThis = NULL;
	MTextureCombination(System::Windows::Forms::Control* topLevelWnd, System::Windows::Forms::Control* renderWnd);

protected:	

	void _Dispose();

	// 获取一个 NiAVObject 中所使用的所有纹理名称
	void _GetUseTextures(NiAVObject* pAVObj, vector<string>& textureList);

	// 为纹理分配 rect
	bool _ArrangeRectForTexture(list<RECT>& openRect, NiSourceTexture* pkTexture, RECT& rectTexture);

	// 将 pkSource 拷贝到 pkTarget 的 rectTarget 中.pkSource 尺寸必须与 rectTarget 完全相同
	bool _CopyTextureToTexture(NiSourceTexture* pkSource, RECT rectTarget, NiSourceTexture* pkTarget);

	// 合并纹理
	NiSourceTexturePtr _CombineTextures(String* strSourceTextures[], int iTargetSize);

	// 检查一个模型是否可以重设 UV.
	// 因为可能出现两层纹理共用一套 UV 的情况,这时该模型就不能修改该 UV
	bool _CanResetUV(NiAVObject* pkAVObj, string strTexName);

	// 检查超过 0~1.0 的 UV
	bool _CheckOverBoardUV(NiAVObject* pkAVObj, NiTexturingProperty* pkParentTexProp, string strTargetTexName);

	// 收集一个 AVObj 中所有的 TexturingProperty 及其对应 geometry
	void _CollectAllTexProp(NiAVObject* pkAVObj, NiTexturingProperty* pkParentTexProp, map<NiTexturingProperty*, vector<NiGeometry*>*>& mapTexProp2Geom);

	// 重设模型 uv 
	bool _ResetModelUV(const char* pszCombinedTextureName, bool bBackUpOldModel);

	// 重设置
	bool _ResetModelUV(NiGeometry* pkGeom, unsigned int iUVCoordID, NiPoint2 kUTarget, NiPoint2 kVTarget);

	// 父节点存在 texturing property, 并与已
	//bool _ForceResetAllChildrenUV()

	// 创建 renderer
	bool _CreateRenderer(IntPtr hTopLevelWndPtr, IntPtr hRendererWndPtr);

	// 重新影射 UV
	void _RemapUV(NiPoint2* pkUV, NiPoint2 kTargetU, NiPoint2 kTargetV);

	// 清空所有旧信息, RebuildTexture2ModelMap 前调用
	void _Clear();

private:
	String* m_strTextureFolder;   // 纹理所在目录
	String* m_strModelFolder;     // 模型所在目录	

	map<string, vector<string>*>*	m_mapTexture2Model;	// 纹理名称-使用该纹理的模型 影射
	map<string, vector<string>*>*	m_mapModel2Texture;	// 模型-模型使用的纹理 影射
	map<string, int>*				m_mapTexture2Size;  // 纹理名称 - 纹理尺寸 影射
	map<string, RECT>*				m_mapTextureRectes;	// 纹理名称 - 分配的 RECT 影射
	int								m_iTargetWidth;		// 目标纹理尺寸
	int								m_iTargetHeight;

	NiStream*						m_pkStream;
	NiRenderer*						m_pkRenderer;		// Renderer
	NiRenderTargetGroup*			m_pkMainRenderTarget; // 主 RTG
};

inline void MTextureCombination::_RemapUV(NiPoint2* pkUV, NiPoint2 kUTarget, NiPoint2 kVTarget)
{
	pkUV->x = kUTarget.x + pkUV->x*(kUTarget.y - kUTarget.x);
	pkUV->y = kVTarget.x + pkUV->y*(kVTarget.y - kVTarget.x);
}

}}}}

