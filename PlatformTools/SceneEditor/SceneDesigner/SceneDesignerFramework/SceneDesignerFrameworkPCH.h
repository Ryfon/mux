﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#pragma once

#pragma unmanaged

#include <NiSystem.h>
#include <NiMain.h>
#include <NiAnimation.h>
#include <NiParticle.h>
#include <NiCollision.h>
#include <NiPortal.h>
#include <NiDX9Renderer.h>
#include <NiD3D10Renderer.h>
#include <NiEntity.h>
#include <NiViewMath.h>
#include <NiMaterialToolkit.h>
#include <NiVisualTrackerRenderClick.h>
#include <NiMetricsOutput.h>
#include <NiGeometry.h>

#include "GlobalHeader.h"
#include "GlobalMacro.h"
#include "TSingleton.h"
#include "EntryDef.h"
#include "Terrain.h"
#include "TerrainMaterial.h"
#include "TerrainModifier.h"
#include "ShadowMapProcess.h"
#include "NiLightMapMaterial.h"
#include "NiLightMapUtility.h"
#include "NiLightMapMaterialHelper.h"

#pragma managed

#include "ManagedMacros.h"

#if !(_MSC_VER < 1400) 
    // Make unmanaged types that are used in public managed classes
    // accessible outside this assembly.
    #pragma make_public(NiAddRemoveComponentCommand)
    #pragma make_public(NiAddRemoveEntityCommand)
    #pragma make_public(NiAddRemovePropertyCommand)
    #pragma make_public(NiAddRemoveSelectionSetCommand)
    #pragma make_public(NiAVObject)
    #pragma make_public(NiBound)
    #pragma make_public(NiCamera)
    #pragma make_public(NiChangeHiddenStateCommand)
    #pragma make_public(NiChangePropertyCommand)
    #pragma make_public(NiEntityCommandInterface)
    #pragma make_public(NiEntityComponentInterface)
    #pragma make_public(NiEntityErrorInterface)
    #pragma make_public(NiEntityInterface)
    #pragma make_public(NiEntityPropertyInterface)
    #pragma make_public(NiEntityRenderingContext)
    #pragma make_public(NiEntitySelectionSet)
    #pragma make_public(NiExternalAssetManager)
    #pragma make_public(NiFrustum)
    #pragma make_public(NiMakePropertyUniqueCommand)
    #pragma make_public(NiMatrix3)
    #pragma make_public(NiPick)
    #pragma make_public(NiPoint2)
    #pragma make_public(NiPoint3)
    #pragma make_public(NiQuaternion)
    #pragma make_public(NiRenameEntityCommand)
    #pragma make_public(NiRenameSelectionSetCommand)
    #pragma make_public(NiScene)
	#pragma make_publie(NiGeometry)

	#pragma make_public(NiRenderedTexture)
	#pragma make_public(NiSingleShaderMaterial)

    #pragma make_public(NiSelectEntitiesCommand)
    #pragma make_public(NiUniqueID)
	#pragma  make_public( CTerrain )
	#pragma  make_public( CShadowMapProcess )
	#pragma make_public(NiLightMapUtility)
	#pragma make_public(NiLightMapMaterial)
	#pragma make_public(NiLightMapMaterialSDM)
	#pragma make_public(NiLightMapMaterialHelper)
	#pragma make_public(NiVisualTrackerRenderClick) 
	#pragma make_public(NiVisualTracker) 
	#pragma make_public(NiVisualTrackerOutput)
	#pragma make_public(NiCalculatingOutput)
#endif  // !(_MSC_VER < 1400)

using namespace System;
