﻿
#pragma once

#include "MDisposable.h"
#include "MEntity.h"
#include "MSelectionSet.h"
#include "MRenderingContext.h"
#include "MPoint3.h"
#include "IMessageService.h"
#include "ICommandService.h"

#include <vcclr.h>

using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::Xml;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;


namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		// 单个 Entity 的导出配置
		public __gc class MEntityExportSetting
		{
		public:
			MEntityExportSetting();

			// Xml IO
			XmlElement* ToXml(XmlDocument* xmlDoc);
			static MEntityExportSetting* FromXml(XmlElement* xmlElement);

			// Members
			String* m_strName;		// 名称
			String* m_strSection;	// 在 entity info 中所属的段.
			int		m_iExtraID;		// 附加 ID
		};

		// 设置特殊的 Entity 要导出到 EntityInfo.xml 中的不同段, 包含特殊 ID
		public __gc class MEntityExportManager : public MDisposable
		{
		public:
			// Xml IO
			void SaveToXml(String* strDirectory);
			void LoadFromXml(String* strDirectory);

			// 获取唯一实例
			__property static MEntityExportManager* get_Instance();
			
			// 根据名称获取一个导出配置
			MEntityExportSetting* GetEntityExportSetting(String* strName);

			// 添加一个
			void AddEntityExportSetting(MEntityExportSetting* pmSetting);

			// 删除一个
			void RemoveEntityExportSetting(MEntityExportSetting* pmSetting);
			void RemoveEntityExportSetting(String* strName);

			// 清除所有
			void Clear();

			// 获取所有的 sections
			ArrayList* GetAllSections();

			// 或者 Export Setting 列表
			__property ArrayList* get_EntityExportSettingList();

		protected:			
			MEntityExportManager();
			virtual void Do_Dispose(bool bDisposing);

			static MEntityExportManager*	ms_Instance = NULL;							// 唯一实例
			static String*					SAVING_FILE = "EntityExportSetting.xml";	// xml 保存文件
			ArrayList*						m_arrEntitySettings;						// 所有的 entity export setting


		};
	}
}
}
}