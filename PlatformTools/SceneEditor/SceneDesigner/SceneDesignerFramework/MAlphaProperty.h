﻿#pragma once

#include "MNiProperty.h"
#include "MPoint3.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		[SerializableAttribute]
		public __gc class MAlphaProperty : public MNiProperty
		{
		public:
			MAlphaProperty(NiAlphaProperty* pkProperty, NiAVObject* pkTagAVObj);

			NiAlphaProperty* GetTagProperty() {return static_cast<NiAlphaProperty*>(m_pkTagProperty);}

			// NiMaterialProperty 属性设置
			__property bool get_AlphaBlending();
			__property void set_AlphaBlending(bool bBlend);

			__property const String* get_SrcAlphaFun();
			__property void set_SrcAlphaFun(const String* strFun);

			__property const String* get_DestAlphaFun();
			__property void set_DestAlphaFun(const String* strFun);

			__property bool get_AlphaTesting();
			__property void set_AlphaTesting(bool bTest);

			__property int get_TestRef();
			__property void set_TestRef(int iRef);

			//System::Object overrides
			virtual bool Equals(Object* pmObj);
			virtual String* ToString() {return "NiAlphaProperty";}

			// AlphaFunction 枚举和对应字符串互转函数
			static NiAlphaProperty::AlphaFunction AlphaFunStrToEnum(const String* strAlphaFun);
			static const String* AlphaFunEnumToStr(NiAlphaProperty::AlphaFunction eAlphaFun);

			static const String* GetAllAlphaFuns()[] {return ALPHA_FUNS;}
		private:
			// alpha function 枚举对应的字符串，下标与 NiAlphaProperty::AlphaFunction 对应
			static String* ALPHA_FUNS[] = {				
				"ALPHA_ONE",
				"ALPHA_ZERO",
				"ALPHA_SRCCOLOR",
				"ALPHA_INVSRCCOLOR",
				"ALPHA_DESTCOLOR",
				"ALPHA_INVDESTCOLOR",
				"ALPHA_SRCALPHA",
				"ALPHA_INVSRCALPHA",
				"ALPHA_DESTALPHA",
				"ALPHA_INVDESTALPHA",
				"ALPHA_SRCALPHASAT"
				};	
		};
	}}}}
