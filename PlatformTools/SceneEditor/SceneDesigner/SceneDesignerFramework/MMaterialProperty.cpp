﻿#include "SceneDesignerFrameworkPCH.h"

#include "MMaterialProperty.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
MMaterialProperty::MMaterialProperty(NiMaterialProperty* pkProperty, NiAVObject* pkTagAVObj)
: MNiProperty(pkProperty, pkTagAVObj)
{
}
//---------------------------------------------------------------------------
float MMaterialProperty::get_Alpha()
{
	return GetNiMaterialProperty()->GetAlpha();
}
//---------------------------------------------------------------------------
void MMaterialProperty::set_Alpha(float fAlpha)
{
	GetNiMaterialProperty()->SetAlpha(fAlpha);
}
//---------------------------------------------------------------------------

float MMaterialProperty::get_Shineness()
{
	return GetNiMaterialProperty()->GetShineness();
}
//---------------------------------------------------------------------------
void MMaterialProperty::set_Shineness(float fShineness)
{
	GetNiMaterialProperty()->SetShineness(fShineness);
}
//---------------------------------------------------------------------------

MPoint3* MMaterialProperty::get_Ambient()
{
	const NiColor& kColor = GetNiMaterialProperty()->GetAmbientColor();
	MPoint3* pmColor = new MPoint3(kColor.r, kColor.g, kColor.b);

	return pmColor;
}
//---------------------------------------------------------------------------
void MMaterialProperty::set_Ambient(MPoint3* pmColor)
{
	if (m_pkTagProperty != NULL && pmColor != NULL)
	{
		GetNiMaterialProperty()->SetAmbientColor(NiColor(pmColor->X, pmColor->Y, pmColor->Z));
	}
}
//---------------------------------------------------------------------------

MPoint3* MMaterialProperty::get_Diffuse()
{
	const NiColor& kColor = GetNiMaterialProperty()->GetDiffuseColor();
	MPoint3* pmColor = new MPoint3(kColor.r, kColor.g, kColor.b);

	return pmColor;
}
//---------------------------------------------------------------------------
void MMaterialProperty::set_Diffuse(MPoint3* pmColor)
{
	if (m_pkTagProperty != NULL && pmColor != NULL)
	{
		GetNiMaterialProperty()->SetDiffuseColor(NiColor(pmColor->X, pmColor->Y, pmColor->Z));
	}
}
//---------------------------------------------------------------------------

MPoint3* MMaterialProperty::get_Specular()
{
	const NiColor& kColor = GetNiMaterialProperty()->GetSpecularColor();
	MPoint3* pmColor = new MPoint3(kColor.r, kColor.g, kColor.b);

	return pmColor;
}
//---------------------------------------------------------------------------
void MMaterialProperty::set_Specular(MPoint3* pmColor)
{
	if (m_pkTagProperty != NULL && pmColor != NULL)
	{
		GetNiMaterialProperty()->SetSpecularColor(NiColor(pmColor->X, pmColor->Y, pmColor->Z));
	}
}
//---------------------------------------------------------------------------

MPoint3* MMaterialProperty::get_Emittance()
{
	const NiColor& kColor = GetNiMaterialProperty()->GetEmittance();
	MPoint3* pmColor = new MPoint3(kColor.r, kColor.g, kColor.b);

	return pmColor;
}
//---------------------------------------------------------------------------
void MMaterialProperty::set_Emittance(MPoint3* pmColor)
{
	if (m_pkTagProperty != NULL && pmColor != NULL)
	{
		GetNiMaterialProperty()->SetEmittance(NiColor(pmColor->X, pmColor->Y, pmColor->Z));
	}
}
//---------------------------------------------------------------------------
bool MMaterialProperty::Equals(Object* pmObj)
{
	MMaterialProperty* pmProperty = dynamic_cast<MMaterialProperty*>(pmObj);
	if (pmProperty == NULL)
	{
		return false;
	}

	return (pmProperty->Alpha==Alpha && pmProperty->Shineness==Shineness
		&& pmProperty->Ambient==Ambient && pmProperty->Diffuse==Diffuse
		&& pmProperty->Specular==Specular && pmProperty->Emittance==Emittance);
}
//---------------------------------------------------------------------------