﻿#pragma once

#include "MNiProperty.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		// MVertexColorProperty 只用于删除结点的 vertex color property
		[SerializableAttribute]
		public __gc class MVertexColorProperty : public MNiProperty
		{
		public:
			MVertexColorProperty(NiVertexColorProperty* pkProperty, NiAVObject* pkTagAVObj);

			//System::Object overrides
			virtual bool Equals(Object* pmObj);
			virtual String* ToString() {return "NiVertexColorProperty";}


		};
	}}}}
