﻿#include "SceneDesignerFrameworkPCH.h"
#include "MColorPalette.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//------------------------------------------------------
MColorPalette::MColorPalette(void)
{
	m_pColorList = new ArrayList();
}
//------------------------------------------------------
MColorPalette::~MColorPalette(void)
{

}
//------------------------------------------------------
void MColorPalette::Save(String* strFileName)
{

}
//------------------------------------------------------
void MColorPalette::Load(String* strFileName)
{

}
//------------------------------------------------------
void MColorPalette::Add(System::Drawing::Color color)
{
	m_pColorList->Add(__box(color));
}
//------------------------------------------------------
void MColorPalette::Remove(int iIdx)
{
	if (m_pColorList->Count > iIdx)
	{
		m_pColorList->RemoveAt(iIdx);
	}
}
//------------------------------------------------------
void MColorPalette::Do_Dispose(bool bDisposing)
{
	m_pColorList->Clear();
	m_pColorList = NULL;
}
//------------------------------------------------------
void MColorPalette::Init()
{

}
//------------------------------------------------------
void MColorPalette::Shutdown()
{
	ms_pmThis->Dispose();
	ms_pmThis = NULL;
}
//------------------------------------------------------
bool MColorPalette::InstanceIsValid()
{
	return (ms_pmThis != NULL);
}
//------------------------------------------------------
MColorPalette* MColorPalette::get_Instance()
{
	if (ms_pmThis == NULL)
	{
		ms_pmThis = new MColorPalette();
	}

	return ms_pmThis;
}
//------------------------------------------------------



