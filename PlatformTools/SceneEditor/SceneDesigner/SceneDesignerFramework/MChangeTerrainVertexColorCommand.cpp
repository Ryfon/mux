﻿
#include "SceneDesignerFrameworkPCH.h"
#include "MChangeTerrainVertexColorCommand.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

MChangeTerrainVertexColorCommand::MChangeTerrainVertexColorCommand(CTerrainChangeVertexColorCommand* pkCommand)
: m_pkCommand(pkCommand)
{
}

//---------------------------------------------------------------------------
// ICommand members.
//---------------------------------------------------------------------------
String* MChangeTerrainVertexColorCommand::get_Name()
{
	MVerifyValidInstance;

	return new String("Change terrain vertex color");
}
//---------------------------------------------------------------------------
void MChangeTerrainVertexColorCommand::Do_Dispose(bool bDisposing)
{}
//---------------------------------------------------------------------------
NiEntityCommandInterface*
MChangeTerrainVertexColorCommand::GetNiEntityCommandInterface()
{
	MVerifyValidInstance;

	return NULL;
}
//---------------------------------------------------------------------------
void MChangeTerrainVertexColorCommand::DoCommand(bool bInBatch, bool bUndoable)
{
	MVerifyValidInstance;

	// Execute command.
	m_pkCommand->DoCommand();
}
//---------------------------------------------------------------------------
void MChangeTerrainVertexColorCommand::UndoCommand(bool bInBatch)
{
	MVerifyValidInstance;
	m_pkCommand->UndoCommand();

}
//---------------------------------------------------------------------------
