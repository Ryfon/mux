﻿#include "SceneDesignerFrameworkPCH.h"
#include "MSceneGraphView.h"
#include "MLightMapMaterialHelper.h"
#include "MFramework.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//--------------------------------------------------------------------------
MSceneGraphNode::MSceneGraphNode(NiAVObject* pkAVObj)
{
	m_aChildren = new ArrayList();

	if (pkAVObj == NULL)
	{
		m_strName = "";
		m_strType = "";
	}
	else
	{
		m_strName = pkAVObj->GetName();
		if (NiIsKindOf(NiNode, pkAVObj))
		{
			m_strType = "NiNode";
		}
		else if(NiIsKindOf(NiGeometry, pkAVObj))
		{
			m_strType = "NiGeometry";
		}
		else
		{
			m_strType = "Unknow";
		}
	}
	
	m_pkTagAVObject = pkAVObj;
}
//--------------------------------------------------------------------------
TreeNode* MSceneGraphNode::ToTreeNode()
{
	String* strTNodeName = String::Concat(m_strType, " \"", m_strName, "\"");
	TreeNode* treeNode = new TreeNode(strTNodeName);
	treeNode->Tag = this;
	treeNode->Text = strTNodeName;
	for (int i=0; i<m_aChildren->Count; i++)
	{
		MSceneGraphNode* pmChild 
			= dynamic_cast<MSceneGraphNode*>(m_aChildren->Item[i]);
		
		TreeNode* tnChild= pmChild->ToTreeNode();
		treeNode->Nodes->Add(tnChild);
	}

	return treeNode;
}
//--------------------------------------------------------------------------
void MSceneGraphNode::AddChild(MSceneGraphNode* pmChild)
{
	if (pmChild != NULL && !m_aChildren->Contains(pmChild))
	{
		m_aChildren->Add(pmChild);
	}
}
//--------------------------------------------------------------------------
MMaterialProperty* MSceneGraphNode::GetMaterialProperty()
{
	if (m_pkTagAVObject == NULL) return NULL;

	NiMaterialProperty* pkMatProp = 
		NiDynamicCast(NiMaterialProperty, m_pkTagAVObject->GetProperty(NiProperty::MATERIAL));

	if (pkMatProp != NULL)
	{
		return new MMaterialProperty(pkMatProp, m_pkTagAVObject);
	}
	else
	{
		return NULL;
	}
	
}
//--------------------------------------------------------------------------
MAlphaProperty* MSceneGraphNode::GetAlphaProperty()
{
	if (m_pkTagAVObject == NULL) return NULL;

	NiAlphaProperty* pkAlphaProp = 
		NiDynamicCast(NiAlphaProperty, m_pkTagAVObject->GetProperty(NiProperty::ALPHA));

	if (pkAlphaProp != NULL)
	{
		return new MAlphaProperty(pkAlphaProp, m_pkTagAVObject);
	}
	else
	{
		return NULL;
	}
}
//--------------------------------------------------------------------------
MVertexColorProperty* MSceneGraphNode::GetVertexColorProperty()
{
	if (m_pkTagAVObject == NULL) return NULL;

	NiVertexColorProperty* pkVertexColorProp = 
		NiDynamicCast(NiVertexColorProperty, m_pkTagAVObject->GetProperty(NiProperty::VERTEX_COLOR));

	if (pkVertexColorProp != NULL)
	{
		return new MVertexColorProperty(pkVertexColorProp, m_pkTagAVObject);
	}
	else
	{
		return NULL;
	}
}
//--------------------------------------------------------------------------
MSpecularProperty* MSceneGraphNode::GetSpecularProperty()
{
	if (m_pkTagAVObject == NULL) return NULL;

	NiSpecularProperty* pkSpecularProp = 
		NiDynamicCast(NiSpecularProperty, m_pkTagAVObject->GetProperty(NiProperty::SPECULAR));

	if (pkSpecularProp != NULL)
	{
		return new MSpecularProperty(pkSpecularProp, m_pkTagAVObject);
	}
	else
	{
		return NULL;
	}
}
//--------------------------------------------------------------------------
void MSceneGraphNode::AddMaterialProperty()
{
	if (m_pkTagAVObject && !m_pkTagAVObject->GetProperty(NiProperty::MATERIAL))
	{
		m_pkTagAVObject->AttachProperty(NiNew NiMaterialProperty());
		m_pkTagAVObject->UpdateProperties();
	}
}
//--------------------------------------------------------------------------
void MSceneGraphNode::AddAlphaProperty()
{
	if (m_pkTagAVObject && !m_pkTagAVObject->GetProperty(NiProperty::ALPHA))
	{
		m_pkTagAVObject->AttachProperty(NiNew NiAlphaProperty());
		m_pkTagAVObject->UpdateProperties();
	}
}
//--------------------------------------------------------------------------
void MSceneGraphNode::AddSpecularProperty()
{
	if (m_pkTagAVObject && !m_pkTagAVObject->GetProperty(NiProperty::SPECULAR))
	{
		m_pkTagAVObject->AttachProperty(NiNew NiSpecularProperty());
		m_pkTagAVObject->UpdateProperties();
	}
}
//--------------------------------------------------------------------------
void MSceneGraphNode::Hide()
{
	if (m_pkTagAVObject)
	{
		m_pkTagAVObject->SetAppCulled(true);
		MFramework::Instance->Update();
	}
}
//--------------------------------------------------------------------------
void MSceneGraphNode::Show()
{
	if (m_pkTagAVObject)
	{
		m_pkTagAVObject->SetAppCulled(false);
		MFramework::Instance->Update();
	}
}
//--------------------------------------------------------------------------
MSceneGraphView::MSceneGraphView(MEntity* pmEntity)
{
	m_pmTagEntity = pmEntity;
}
//--------------------------------------------------------------------------
MSceneGraphView* MSceneGraphView::GenSceneGraphView(MEntity* pmEntity)
{
	// 唯一生成 MSceneGraphView 对象的静态函数
	if (pmEntity == NULL) return NULL;

	NiAVObject* pkSceneRoot = pmEntity->GetSceneRootPointer(0);
	if (pkSceneRoot != NULL)
	{
		MSceneGraphView* pmSceneGraphView = new MSceneGraphView(pmEntity);
		pmSceneGraphView->m_RootSceneGraphNode = _GenSceneGraph(NULL, pkSceneRoot);
		return pmSceneGraphView;
	}
	else
	{
		return NULL;
	}

}
//--------------------------------------------------------------------------
MSceneGraphNode* MSceneGraphView::_GenSceneGraph(MSceneGraphNode* pmParent, NiAVObject* pkAVObj)
{
	if (pkAVObj == NULL) return NULL;

	MSceneGraphNode* pmSceneGraphNode = new MSceneGraphNode(pkAVObj);
	if (pmParent != NULL)
	{
		pmParent->AddChild(pmSceneGraphNode);
	}

	if (NiIsKindOf(NiNode, pkAVObj))
	{
		NiNode* pkThisNode = NiDynamicCast(NiNode, pkAVObj);
		for (int i=0; i<pkThisNode->GetChildCount(); i++)
		{
			_GenSceneGraph(pmSceneGraphNode, pkThisNode->GetAt(i));
		}
	}

	return pmSceneGraphNode;
}
//--------------------------------------------------------------------------
TreeNode* MSceneGraphView::ToTreeNode()
{
	if (m_RootSceneGraphNode != NULL)
	{
		return m_RootSceneGraphNode->ToTreeNode();
	}
	else
	{
		return NULL;
	}
}
//--------------------------------------------------------------------------
void MSceneGraphView::ClearForPropertyAndDynamicEffect(NiAVObject* pkAVObj)
{
	if (pkAVObj == NULL) return;
	
	// 删除 fog property
	if (pkAVObj->GetProperty(NiProperty::FOG))
	{
		pkAVObj->RemoveProperty(NiProperty::FOG);
		pkAVObj->UpdateProperties();
	}

	// 删除 dynamic effects
	if (NiIsKindOf(NiNode, pkAVObj))
	{
		NiNode* pkNode = NiDynamicCast(NiNode, pkAVObj);
		pkNode->CompactChildArray();
		// 遍历所有子结点
		for (int i=pkNode->GetChildCount()-1; i>=0; i--)
		{
			if (NiIsKindOf(NiLight, pkNode->GetAt(i)) 
				|| NiIsKindOf(NiShadowGenerator, pkNode->GetAt(i)))
			{
				pkNode->DetachChildAt(i);
			}

			ClearForPropertyAndDynamicEffect(pkNode->GetAt(i));
		}

		pkNode->Update(0.0f);
	}
}
//--------------------------------------------------------------------------
bool MSceneGraphView::ClearNifFogPropertyAndDynamicEffect(String* strFilename)
{
	if (strFilename == NULL) return false;

	const char* pszFilename = MStringToCharPointer(strFilename);
	NiStream kStream;
	if (!kStream.Load(pszFilename)) 
	{
		return false;
	}

	if (kStream.GetObjectCount() <= 0) return false;

	NiAVObject* pkScene = NiDynamicCast(NiAVObject, kStream.GetObjectAt(0));
	pkScene->Update(0.0f);

	MLightMapMaterialHelper::EmptyAllTextureDatas(pkScene);
	ClearForPropertyAndDynamicEffect(pkScene);

	NiStream kStremSave;
	kStremSave.InsertObject(pkScene);
	if (!kStremSave.Save(pszFilename)) return false;

	MFreeCharPointer(pszFilename);

	return true;
}

//--------------------------------------------------------------------------
bool MSceneGraphView::CheckFogPropertyAndDynamicEffect(NiAVObject* pkAVObj)
{
	if (pkAVObj == NULL) return false;

	// 删除 fog property
	if (pkAVObj->GetProperty(NiProperty::FOG))
	{
		return true;
	}

	// 删除 dynamic effects
	if (NiIsKindOf(NiNode, pkAVObj))
	{
		NiNode* pkNode = NiDynamicCast(NiNode, pkAVObj);
		pkNode->CompactChildArray();
		// 遍历所有子结点
		for (int i=pkNode->GetChildCount()-1; i>=0; i--)
		{
			if (NiIsKindOf(NiLight, pkNode->GetAt(i)) 
				|| NiIsKindOf(NiShadowGenerator, pkNode->GetAt(i)))
			{
				return true;
			}

			if (CheckFogPropertyAndDynamicEffect(pkNode->GetAt(i)))
			{
				return true;
			}
		}
	}

	return false;
}
//--------------------------------------------------------------------------

bool MSceneGraphView::SaveToNif()
{
	if (m_pmTagEntity == NULL || !m_pmTagEntity->HasProperty("NIF File Path"))
	{
		return false;
	}

	String* strNifFilePath 
		= dynamic_cast<String*>(m_pmTagEntity->GetPropertyData("NIF File Path"));

	NiAVObject* pkSceneRoot = m_pmTagEntity->GetSceneRootPointer(0);

	NiObjectNET::CopyType ct = NiObjectNET::GetDefaultCopyType();
	NiObjectNET ::SetDefaultCopyType(NiObjectNET::COPY_EXACT);
	NiAVObject* pkCloneScene = NiDynamicCast(NiAVObject, pkSceneRoot->Clone());
	NiObjectNET ::SetDefaultCopyType(ct);

	if (pkCloneScene != NULL)
	{	
		MLightMapMaterialHelper::EmptyAllTextureDatas(pkCloneScene);
		ClearForPropertyAndDynamicEffect(pkCloneScene);
		pkCloneScene->Update(0.0f);
		pkCloneScene->UpdateEffects();
		pkCloneScene->UpdateProperties();

		SaveFileDialog  *saveFileDlg = new SaveFileDialog();
		saveFileDlg->Filter = "物件信息文件(*.nif)|*.nif|All files(*.*)|*.*";
		saveFileDlg->FileName = strNifFilePath;
		saveFileDlg->FilterIndex = 1;
		
		if ( saveFileDlg->ShowDialog() == DialogResult::OK )
		{
			NiStream kStream;
			kStream.InsertObject(pkCloneScene);
			const char* pszFileName = MStringToCharPointer(saveFileDlg->FileName);
			kStream.Save(pszFileName);
			MFreeCharPointer(pszFileName);
		}

		return true;
	}

	return false;
}
//--------------------------------------------------------------------------
bool MSceneGraphView::CheckFogPropertyAndDynamicEffect(String* strFilename)
{
	if (strFilename == NULL) return NULL;

	const char* pszFilename = MStringToCharPointer(strFilename);
	NiStream kStream;
	//NiShadowGenerator::ms_bStreamLoaded = false;    //Modified By Touch 2010.12.29
	kStream.Load(pszFilename);
	MFreeCharPointer(pszFilename);

	if ( false )//(NiShadowGenerator::ms_bStreamLoaded)   //Modified By Touch 2010.12.29
	{
		return true;
	}

	NiAVObject* pkScene = NiDynamicCast(NiAVObject, kStream.GetObjectAt(0));



	return CheckFogPropertyAndDynamicEffect(pkScene);

}
//--------------------------------------------------------------------------
// 测试用函数，读取nif
void MSceneGraphView::TestLoadNif(String* strFilename)
{
	const char* pszFilename = MStringToCharPointer(strFilename);
	NiStream kStream;
	kStream.Load(pszFilename);
	NiObject* pkScene = kStream.GetObjectAt(0);
	MFreeCharPointer(pszFilename);
}