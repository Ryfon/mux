﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MBrushParameter.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
MBrushParameter::MBrushParameter() 
:	m_fInnerRadius(1.0f), 
	m_fOuterRadius(1.0f), 
	m_fValue(0.0f),
	m_pcTexFile(""), 
	m_ePainterMode(MTerrain::EPainterType::PT_TERRAIN_VERTEX)
{}

//---------------------------------------------------------------------------
MBrushParameter::MBrushParameter(const MBrushParameter& kBrushParameter)
{
	SetData(kBrushParameter);
}
//---------------------------------------------------------------------------
MBrushParameter::MBrushParameter( float fInnerRadius, float fOuterRadius, float fValue, String *pcTexFile, MTerrain::EPainterType ePainterMode)
: m_fInnerRadius(fInnerRadius)
, m_fOuterRadius(fOuterRadius)
, m_fValue(fValue)
, m_pcTexFile( pcTexFile )
, m_ePainterMode(ePainterMode)
{}

//---------------------------------------------------------------------------
MBrushParameter::~MBrushParameter()
{}

//---------------------------------------------------------------------------
void MBrushParameter::SetData(const MBrushParameter& kBrushParameter)
{
	m_fInnerRadius = kBrushParameter.m_fInnerRadius;
	m_fOuterRadius = kBrushParameter.m_fOuterRadius;
	m_fValue	   = kBrushParameter.m_fValue;

	m_pcTexFile	   = kBrushParameter.m_pcTexFile;
	m_ePainterMode = kBrushParameter.m_ePainterMode;	
}

//---------------------------------------------------------------------------
float MBrushParameter::get_InnerRadius()
{
	return m_fInnerRadius;
}
//---------------------------------------------------------------------------
float MBrushParameter::get_OuterRadius()
{
	return m_fOuterRadius;
}
//---------------------------------------------------------------------------
float MBrushParameter::get_Value()
{
	return m_fValue;
}
//---------------------------------------------------------------------------
String *MBrushParameter::get_TexFile()
{
	return m_pcTexFile;
}
//---------------------------------------------------------------------------
MTerrain::EPainterType MBrushParameter::get_PainterMode()
{
	return m_ePainterMode;
}
//---------------------------------------------------------------------------
void MBrushParameter::SetInnerRadius(const float fInnerRadius)
{
	m_fInnerRadius = fInnerRadius;
}
//---------------------------------------------------------------------------
void MBrushParameter::SetOuterRadius(const float fOuterRadius)
{
	m_fOuterRadius = fOuterRadius;
}
//---------------------------------------------------------------------------
void MBrushParameter::SetValue( float fV )
{
	m_fValue = fV;
}
//---------------------------------------------------------------------------
void MBrushParameter::SetTexFile( String *pcTexFile )
{
	m_pcTexFile = pcTexFile;
}
//---------------------------------------------------------------------------
void MBrushParameter::SetPainterMode( const MTerrain::EPainterType ePainterMode )
{
	m_ePainterMode = ePainterMode;
}
//---------------------------------------------------------------------------
bool MBrushParameter::Equals(Object* pmObj)
{
	MBrushParameter* pmBrush2 = dynamic_cast<MBrushParameter*>(pmObj);
	if (pmBrush2 == NULL)	return false;
	
	return ( m_fInnerRadius == pmBrush2->get_InnerRadius() 
		&& m_fOuterRadius == pmBrush2->get_OuterRadius()
		&& m_fValue == pmBrush2->get_Value()
		&& m_pcTexFile ==pmBrush2->get_TexFile()
		&& m_ePainterMode == pmBrush2->get_PainterMode() );
}