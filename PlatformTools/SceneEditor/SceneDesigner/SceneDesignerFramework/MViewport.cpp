﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MViewport.h"
#include "MFramework.h"
#include "MEntityFactory.h"
#include "MEventManager.h"
#include "MSceneFactory.h"
#include "ServiceProvider.h"
#include "MUtility.h"
#include "MCameraManager.h"
#include "MChangeViewportCameraCommand.h"
#include "MChunkEntityManager.h"
#include "MGlobalSetting.h"
#include "MTimeStamp.h"

#pragma unmanaged
#include "WaterManager.h"
//#include "RiverManager.h"
#include "GrideBaseRiver.h"

// 包围盒绘制 [9/11/2009 hemeng]
#include "BoundGeometroy.h"

//--for test RiverOptimizer only
#include "RiverOptimizer.h"

#include "DepthAlphaEffectManager.h"
#include "TerrainModifier.h"
#include "TerrainGridBaseRegionManager.h"
#include "MTerrain.h"
#pragma  managed

using namespace Emergent::Gamebryo::SceneDesigner::Framework;


//---------------------------------------------------------------------------
MViewport::MViewport() : m_fLeft(0.0f), m_fRight(1.0f), m_fTop(1.0f),
m_fBottom(0.0f), m_pkBorder(NULL), m_pkBorderMaterial(NULL),
m_pkHighlightColor(NULL), m_pkScreenConsole(NULL),m_bRenderRectSel(false),m_pkRectSelTexture(NULL)
{
	InitToolScene();
	CreateBorderGeometry();
	m_pmUnselectedEntities = new ArrayList();

	m_iSelStartX = m_iSelStartY = m_iSelLength = m_iSelHeight = 0;
	
	
	// Register event handlers.
	__hook(&MEventManager::EntityNameChanged, MEventManager::Instance,
		&MViewport::OnEntityNameChanged);
}
//---------------------------------------------------------------------------
MViewport::MViewport(float fLeft, float fRight, float fTop, float fBottom) :
m_fLeft(fLeft), m_fRight(fRight), m_fTop(fTop), m_fBottom(fBottom),
m_pkBorder(NULL), m_pkBorderMaterial(NULL), m_pkHighlightColor(NULL),
m_pkScreenConsole(NULL)
{
	InitToolScene();
	CreateBorderGeometry();
	m_pmUnselectedEntities = new ArrayList();
}
//---------------------------------------------------------------------------
MViewport::MViewport(float fLeft, float fRight, float fTop, float fBottom,
					 MEntity* pmCamera) : m_fLeft(fLeft), m_fRight(fRight), m_fTop(fTop),
					 m_fBottom(fBottom), m_pkBorder(NULL), m_pkBorderMaterial(NULL),
					 m_pkHighlightColor(NULL), m_pkScreenConsole(NULL)
{
	InitToolScene();
	CreateBorderGeometry();
	InternalSetCamera(pmCamera);
	m_pmUnselectedEntities = new ArrayList();
}
//---------------------------------------------------------------------------
void MViewport::_SDMInit()
{
	ms_pkTranslationName = NiNew NiFixedString("Translation");
	ms_pkRotationName = NiNew NiFixedString("Rotation");
	ms_pkNearClipName = NiNew NiFixedString("Near Clipping Plane");
	ms_pkFarClipName = NiNew NiFixedString("Far Clipping Plane");
}
//---------------------------------------------------------------------------
void MViewport::_SDMShutdown()
{
	NiDelete ms_pkTranslationName;
	NiDelete ms_pkRotationName;
	NiDelete ms_pkNearClipName;
	NiDelete ms_pkFarClipName;	
}
//---------------------------------------------------------------------------
void MViewport::Do_Dispose(bool bDisposing)
{
	// Unregister event handlers.
	__unhook(&MEventManager::EntityNameChanged, MEventManager::Instance,
		&MViewport::OnEntityNameChanged);

	MDisposeRefObject(m_pkScreenConsole);
	NiDelete m_pkHighlightColor;
	MDisposeRefObject(m_pkBorder);

	if (bDisposing)
	{
		if (m_pmCamera != NULL)
		{
			m_pmCamera->Dispose();
		}
	}

	CSnapTool::Instance()->Do_Dispos();

	if (m_pkRectSelTexture)
	{
		NiDelete m_pkRectSelTexture;
		m_pkRectSelTexture = NULL;
	}

}
//---------------------------------------------------------------------------
MEntity* MViewport::get_CameraEntity()
{
	MVerifyValidInstance;

	return m_pmCamera;
}
//---------------------------------------------------------------------------
NiCamera* MViewport::GetNiCamera()
{
	MVerifyValidInstance;

	if (m_pmCamera != NULL)
	{
		NiAVObject* pkNiCam = m_pmCamera->GetSceneRootPointer(0);
		return NiDynamicCast(NiCamera, pkNiCam);
	}
	else
	{
		return NULL;
	}
}
//---------------------------------------------------------------------------
void MViewport::SetCamera(MEntity* pmCamera, bool bUndoable)
{
	MVerifyValidInstance;

	CommandService->ExecuteCommand(new MChangeViewportCameraCommand(this,
		pmCamera), bUndoable);
}
//---------------------------------------------------------------------------
void MViewport::InternalSetCamera(MEntity* pmCamera)
{
	MVerifyValidInstance;

	MAssert(pmCamera != NULL, "Null camera provided to function!");
	MAssert(MCameraManager::EntityIsCamera(pmCamera), "Invalid camera entity "
		"provided to function!");

	m_pmCamera = pmCamera;
	UpdateCameraViewport(GetNiCamera());
	UpdateScreenConsole();
}
//---------------------------------------------------------------------------
void MViewport::Update(float fTime)
{
	MVerifyValidInstance;

	if (m_pmToolScene != NULL)
	{
		m_pmToolScene->Update(fTime, MFramework::Instance
			->ExternalAssetManager);
	}
	if (m_pmRenderingMode != NULL)
	{
		m_pmRenderingMode->Update(fTime);
	}
}
//---------------------------------------------------------------------------
void MViewport::UpdateClippingPlanes()
{
	NiBound* pkBound;
	pkBound = MFramework::Instance->BoundManager->GetToolSceneBound(this);
	if (pkBound)
	{
		// only standard cameras get automatic clipping plane management
		if (MFramework::Instance->CameraManager->IsStandardCamera(this,
			m_pmCamera))
		{
			NiCamera* pkCamera = NiDynamicCast(NiCamera, 
				m_pmCamera->GetSceneRootPointer(0));
			if (!pkCamera)
				return;
			// get the most up-to-date camera position for calculation
			NiEntityInterface* pkEntity = m_pmCamera->GetNiEntityInterface();
			if (!pkEntity)
				return;
			pkCamera->FitNearAndFarToBound(*pkBound);
			float fNear, fFar;
			fNear = pkCamera->GetViewFrustum().m_fNear;
			fFar = pkCamera->GetViewFrustum().m_fFar;
			if (MGlobalSetting::Instance->FogEnabled)
			{
				pkEntity->SetPropertyData(*ms_pkNearClipName, MGlobalSetting::Instance->NearPlane);//fNear
				pkEntity->SetPropertyData(*ms_pkFarClipName, MGlobalSetting::Instance->FarPlane); //);fFar
			}
			else
			{
				pkEntity->SetPropertyData(*ms_pkNearClipName, fNear);//fNear
				pkEntity->SetPropertyData(*ms_pkFarClipName, fFar); //);fFar
			}

			m_pmCamera->Update(
				MFramework::Instance->TimeManager->ContinuousTime, 
				MFramework::Instance->ExternalAssetManager);
		}
	}
}
//---------------------------------------------------------------------------
void MViewport::RenderUnselectedEntities()
{
	MVerifyValidInstance;
	// 记录时间戳
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("Enter MViewport::RenderUnselectedEntities"));
#endif

	NiRenderer* pkRenderer = NiRenderer::GetRenderer();
	if (NULL == pkRenderer)
	{
		return;
	}

	pkRenderer->SetMaxFogValue(1.0f);

	if (m_pmRenderingMode == NULL)
	{
		// If no rendering mode is set, select the first one available in
		// the rendering mode service.
		IRenderingMode* amRenderingModes[] =
			RenderingModeService->GetRenderingModes();
		for (int i = 0; i < amRenderingModes->Length; i++)
		{
			IRenderingMode* pmRenderingMode = amRenderingModes[i];
			if (pmRenderingMode->DisplayToUser)
			{
				m_pmRenderingMode = pmRenderingMode;
				break;
			}
		}

		// If there are no rendering modes available, don't render.
		if (m_pmRenderingMode == NULL)
		{
			return;
		}
	}

	// Get viewport camera.
	NiCamera* pkCamera = GetNiCamera();

	if (!pkCamera)
	{
		// Don't render if there is no camera.
		return;
	}

	// Set viewport on camera.
	UpdateCameraViewport(pkCamera);
	pkCamera->Update(0.0f);

	// Set camera on rendering context.
	MRenderingContext* pmRenderingContext = MRenderer::Instance
		->RenderingContext;
	pmRenderingContext->GetRenderingContext()->m_pkCamera = pkCamera;

	// Begin rendering.
	m_pmRenderingMode->Begin(pmRenderingContext);

	// Render tool scene.
	m_pmRenderingMode->Render(ToolScene->GetEntities(), pmRenderingContext);
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MViewport::RenderUnselectedEntities After Render tool scene."));
#endif
	// Render grid base region preview scene
	m_pmRenderingMode->Render(MTerrain::Instance->GetPreviewSceneEntities(), pmRenderingContext);
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MViewport::RenderUnselectedEntities After Render Preview Scene."));
#endif

	// Render Terrain
	MFramework::Instance->Scene->BeginTerrain( pmRenderingContext );
	MFramework::Instance->Scene->RenderTerrain( pmRenderingContext );
	MFramework::Instance->Scene->EndTerrain( pmRenderingContext );	
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MViewport::RenderUnselectedEntities After Render Terrain."));
#endif

	bool bRenderSceneObject = MFramework::get_Instance()->Scene->GetRenderSceneObject();
	bool bRenderNPC = MFramework::get_Instance()->Scene->GetRenderNPCCreator();
	bool bRenderRegion = MFramework::get_Instance()->Scene->GetRenderRegion();
	
	//  [11/24/2009 shiyazheng] 场景编辑器现在不处理策划区域及 npc creator
	if (bRenderSceneObject ) // || bRenderNPC || bRenderRegion
	{
		// Render main scene.
		// comment by Shi. 使用 ChunkEntityManager 来进行第一次筛选。
		// MEntity* amSceneEntities[] = MFramework::Instance->Scene->GetEntities();
		MEntity* amSceneEntities[] = new MEntity*[0];

		if (MChunkEntityManager::Instance==NULL 
			|| MChunkEntityManager::Instance->GetEntityCullMode()==MChunkEntityManager::ECM_CULL_NONE)
		{
			amSceneEntities = MFramework::Instance->Scene->GetEntities();
		}
		else if (MChunkEntityManager::Instance->GetEntityCullMode() == MChunkEntityManager::ECM_CAMERA_CHUNK)
		{
			// 获取当前 camera 所在 chunk id
			NiCamera* pkCamera = GetNiCamera();
			if (pkCamera != NULL)
			{
				NiPoint3 kCamPos = pkCamera->GetWorldLocation();
				int iChunkID = MFramework::Instance->Scene->Terrain->GetChunkIndex(kCamPos);
				if (iChunkID != -1)
				{
					vector<int> chunkList;
					chunkList.push_back(iChunkID);
					amSceneEntities = MChunkEntityManager::Instance->GetEntity(&chunkList);
				}
			}
		}
		else if (MChunkEntityManager::Instance->GetEntityCullMode() == MChunkEntityManager::ECM_CAMERA_AROUND)
		{
			// 获取当前 camera 所在 chunk id
			NiCamera* pkCamera = GetNiCamera();
			if (pkCamera != NULL)
			{
				NiPoint3 kCamPos = pkCamera->GetWorldLocation();
				vector<int> chunkList;
				for (int i=-1; i<=1; i++)
				{
					for (int j=-1; j<=1; j++)
					{
						NiPoint3 kPos = kCamPos + NiPoint3(i*GRIDINCHUNK, j*GRIDINCHUNK, 0);
						int iChunkID = MFramework::Instance->Scene->Terrain->GetChunkIndex(kPos);
						if (iChunkID != -1)
						{
							chunkList.push_back(iChunkID);
						}
					}
				}
				amSceneEntities = MChunkEntityManager::Instance->GetEntity(&chunkList);
			}
		}
		else if (MChunkEntityManager::Instance->GetEntityCullMode() == MChunkEntityManager::ECM_STATIC)
		{
			amSceneEntities = MChunkEntityManager::Instance->GetStaticEntityList();
		}

		m_pmUnselectedEntities->Clear();
		for (int i = 0; i < amSceneEntities->Length; i++)
		{
			MEntity* pmSceneEntity = amSceneEntities[i];
			if (pmSceneEntity->HasBeenDisposed())
			{
				MChunkEntityManager::Instance->RemoveEntity(pmSceneEntity);
				continue;
			}

			if (pmSceneEntity->Hidden) continue;
			
			// 判断该 entity 是否 npc creator, 如果是，增加范围框
			//bool bTmp;
			//NiBool bPropExist = pmSceneEntity->GetNiEntityInterface()->CanResetProperty("CreatorName", bTmp);
			//if (bPropExist )// 是 npc
			//{
			//	if (bRenderNPC)
			//	{
			//		m_pmUnselectedEntities->Add(pmSceneEntity);
			//		// 如果 CreateType 为 1，说明是 npc 群，需要渲染框框
			//		RenderNPCCreatorRange(pmSceneEntity, pkRenderer);
			//	}

			//}
			//else 
			if (bRenderSceneObject && !SelectionService->IsEntitySelected(pmSceneEntity) )// && !pmSceneEntity->GetNiEntityInterface()->CanResetProperty("PointID", bTmp)
			{
				m_pmUnselectedEntities->Add(pmSceneEntity);
			}

			//// 区域的渲染
			//if (bRenderRegion)
			//{
			//	// 判断是否是区域的顶点
			//	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
			//	if (pTerrain != NULL)
			//	{
			//		bool bIsAreaPoint = pTerrain->m_pAreaMgr->EntityIsAreaPoint(pmSceneEntity->GetNiEntityInterface());
			//		if (bIsAreaPoint)
			//		{
			//			m_pmUnselectedEntities->Add(pmSceneEntity);
			//		}
			//	}

			//	RenderRegions(pkRenderer, MFramework::Instance->Scene->GetGlobalRegionType());
			//}

		}

		m_pmRenderingMode->Render(static_cast<MEntity*[]>(m_pmUnselectedEntities
			->ToArray(__typeof(MEntity))), pmRenderingContext);

		m_pmRenderingMode->Render(MProxyManager::Instance->ProxyScene
			->GetEntities(), pmRenderingContext);

		// 水体渲染反射纹理
		//CWaterManager::GetInstance()->GetMainWaterRenderView()->SetVisibleArray(pmRenderingContext->GetRenderingContext()->m_pkCullingProcess->GetVisibleSet());
		//CWaterManager::GetInstance()->RenderReflectTexture(pmRenderingContext->GetRenderingContext());
	}
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MViewport::RenderUnselectedEntities After Render Scene Object."));
#endif
	// shadow map 方法的
	//bool bRenderShadow = MFramework::Instance->Scene->Get_BakeShadow();
	//if ( bRenderShadow )
	//{
	//	MFramework::Instance->Scene->Bake( pmRenderingContext->GetRenderingContext() );

	//	NiRenderedTexturePtr pkShaodwTexture = MFramework::Instance->Scene->GetShadowTexture();
	//	if (pkShaodwTexture != NULL)
	//	{
	//		//CTerrainModifier::ApplyShadowTexture(MFramework::Instance->Scene->Terrain, NiSmartPointerCast(NiTexture, pkShaodwTexture));
	//		SaveTextureToDDS(pkShaodwTexture, "e:\\shadowTexture.dds");
	//	}
	//	MFramework::Instance->Scene->Set_BakeShadow(false);
	//	// 将 bake 出来的 shadow 写到 chunk blend texture 的 alpha 通道中

	//}
	//
	// End rendering.

	//渲染场景中过高的那些点
	bool bRenderOverTopVertex = MFramework::get_Instance()->Scene->GetRenderOverTopVertex();
	if (bRenderOverTopVertex)
	{
		map<int,set<int>>::iterator mapIt =  MFramework::Instance->Scene->Terrain->m_mapOverTopVertexs.begin();
		for (; mapIt != MFramework::Instance->Scene->Terrain->m_mapOverTopVertexs.end(); mapIt++)
		{
			RenderOverTopVertexs(pkRenderer, mapIt->second);
		}

	}

	///////////////////////////////////////////////////////////////////////////
	//渲染水域
	if ( MFramework::Instance->Scene->IsInitalRiver() )
	{
		RenderWaterRegion(pkRenderer);

		//DrawWaterRegionFrame( pkRenderer);
		
	}
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MViewport::RenderUnselectedEntities After Render Water Region."));
#endif
	// 渲染 GridBaseRegion
	NiGeometry* pkGridBaseRegionGeom = CTerrainGridBaseRegionManager::GetInstance()->GetOnEditRegionGeometry(MFramework::Instance->Scene->Terrain);
	if (pkGridBaseRegionGeom != NULL)
	{
		pkGridBaseRegionGeom->RenderImmediate(pkRenderer);
	}

	m_pmRenderingMode->End(pmRenderingContext);
#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("In MViewport::RenderUnselectedEntities After m_pmRenderingMode->End."));
#endif
	MTerrain::Instance->RenderSprayImmediate(pkRenderer);	// 渲染预览的海岸排浪

	//渲染吸点工具，entity顶点
	//RenderSnapEntityVertice(CSnapTool::EP_SRC);
	//RenderSnapEntityVertice(CSnapTool::EP_DES);

#ifdef _DEBUG
	MTimeStamp::Instance->AddTimeStamp(new String("Quit MViewport::RenderUnselectedEntities"));
#endif

}
//---------------------------------------------------------------------------
void MViewport::RenderSelectedEntities()
{
	MVerifyValidInstance;

	if (m_pmRenderingMode == NULL)
	{
		// If no rendering mode is set, select the first one available in
		// the rendering mode service.
		IRenderingMode* amRenderingModes[] =
			RenderingModeService->GetRenderingModes();
		for (int i = 0; i < amRenderingModes->Length; i++)
		{
			IRenderingMode* pmRenderingMode = amRenderingModes[i];
			if (pmRenderingMode->DisplayToUser)
			{
				m_pmRenderingMode = pmRenderingMode;
				break;
			}
		}

		// If there are no rendering modes available, don't render.
		if (m_pmRenderingMode == NULL)
		{
			return;
		}
	}

	// Get viewport camera.
	NiCamera* pkCamera = GetNiCamera();
	if (!pkCamera)
	{
		// Don't render if there is no camera.
		return;
	}

	// Set viewport on camera.
	UpdateCameraViewport(pkCamera);

	// Set camera on rendering context.
	MRenderingContext* pmRenderingContext = MRenderer::Instance
		->RenderingContext;
	pmRenderingContext->GetRenderingContext()->m_pkCamera = pkCamera;

	// modified in 2007/12/20
	/*MFramework::Instance->Scene->BeginTerrain( pmRenderingContext );
	MFramework::Instance->Scene->RenderTerrain( pmRenderingContext );
	MFramework::Instance->Scene->EndTerrain( pmRenderingContext );	*/

	// Begin rendering.
	m_pmRenderingMode->Begin(pmRenderingContext);

	// Render selected entities.

	MEntity* pmSelectedEntities[] = SelectionService->GetSelectedEntities();

	MEntity* pmEntitesToRender[] = new MEntity*[pmSelectedEntities->Length];

	for (int i = 0; i < pmSelectedEntities->Length; i++)
	{
		MEntity* pmProxy = MFramework::Instance->ProxyManager->
			GetProxyForEntity(pmSelectedEntities[i]);
		if (pmProxy != NULL)
		{
			// 如果是点光源则绘制其影响范围
			if (MLightManager::EntityIsLight(pmSelectedEntities[i]))
			{
				String* strType = dynamic_cast<String*>(pmSelectedEntities[i]->GetPropertyData(
					"Light Type"));
				if (strType->Equals(NiLightComponent::LT_POINT))
				{
					// test [7/14/2009 hemeng]
					NiAVObject* spLightBound = MFramework::GetPointLightBound();	
					if (spLightBound != NULL)
					{
						NiEntityInterface* pEntityInterface = pmSelectedEntities[i]->GetNiEntityInterface();
						NiPoint3 kTranslation;
						pEntityInterface->GetPropertyData("Translation",kTranslation);
						spLightBound->SetTranslate(kTranslation);
						float fScale;
						pEntityInterface->GetPropertyData("Scale",fScale);
						spLightBound->SetScale(fScale);
						spLightBound->Update(0.0);
						// 绘制
						NiDrawScene(pkCamera,spLightBound,*pmRenderingContext->GetRenderingContext()->m_pkCullingProcess);
					}					
				}
			}
			// 如果是特效除渲染特效proxy外，也应渲染特效本身 [7/8/2009 hemeng]
			else if (MFramework::Instance->ProxyManager->IsEntitySFX(pmSelectedEntities[i]))
			{
				m_pmRenderingMode->Render(pmSelectedEntities[i], pmRenderingContext);
			}
			pmEntitesToRender[i] = pmProxy;			
		}
		else
		{
			pmEntitesToRender[i] = pmSelectedEntities[i];
		}

	}

	m_pmRenderingMode->Render(pmEntitesToRender,  pmRenderingContext);

	// End rendering.
	m_pmRenderingMode->End(pmRenderingContext);

	//渲染吸点工具，entity顶点
	// modified by hemeng 090304
	if (CSnapTool::Instance())
	{
		/*CSnapTool::Instance()->RenderSnapEntityVertice(NiRenderer::GetRenderer());*/
		RenderSnapEntityVertice(CSnapTool::EP_SRC);
		RenderSnapEntityVertice(CSnapTool::EP_DES);
	}	
}
//---------------------------------------------------------------------------
void MViewport::RenderGizmo()
{
	MVerifyValidInstance;

	// Get active interaction mode.
	IInteractionMode* pmInteractionMode = InteractionModeService->ActiveMode;
	if (pmInteractionMode != NULL)
	{
		// Get viewport camera.
		NiCamera* pkCamera = GetNiCamera();
		if (!pkCamera)
		{
			// Don't render if there is no camera.
			return;
		}

		// Set viewport on camera.
		UpdateCameraViewport(pkCamera);

		// Set camera on rendering context.
		MRenderingContext* pmRenderingContext = MRenderer::Instance
			->RenderingContext;
		pmRenderingContext->GetRenderingContext()->m_pkCamera = pkCamera;

		// Render gizmo.
		pmInteractionMode->RenderGizmo(pmRenderingContext);

		//渲染吸点工具，entity顶点
		/*RenderSnapEntityVertice(CSnapTool::EP_SRC);
		RenderSnapEntityVertice(CSnapTool::EP_DES);*/
	}
}

NiScreenTexture* MViewport::CreateRectSelScreenTexture()
{
	NiPixelFormat kPixFormat;
	kPixFormat.SetFormat(NiPixelFormat::FORMAT_RGBA);

	NiPixelData* pkPixData = NiNew NiPixelData(512,512,kPixFormat);

	NiTexture::FormatPrefs kPrefs;
	kPrefs.m_ePixelLayout = NiTexture::FormatPrefs::TRUE_COLOR_32;
	kPrefs.m_eMipMapped = NiTexture::FormatPrefs::NO;
	kPrefs.m_eAlphaFmt = NiTexture::FormatPrefs::ALPHA_DEFAULT;

	NiSourceTexture* pkSourceTex = NiSourceTexture::Create(pkPixData,kPrefs);

	NiScreenTexture* pkScreenTex = NULL;

	return pkScreenTex;
}

//---------------------------------------------------------------------------
void MViewport::RenderScreenElements(bool bActiveViewport)
{
	MVerifyValidInstance;

	// Render border.
	if (bActiveViewport)
	{
		if (!m_pkHighlightColor)
		{
			RegisterForHighlightColorSetting();
		}
		m_pkBorderMaterial->SetEmittance(*m_pkHighlightColor);
	}
	else
	{
		m_pkBorderMaterial->SetEmittance(NiColor::BLACK);
	}
	NiRenderer* pkRenderer = MRenderer::Instance->RenderingContext
		->GetRenderingContext()->m_pkRenderer;
	pkRenderer->SetScreenSpaceCameraData();
	m_pkBorder->RenderImmediate(pkRenderer);

	// Render screen console.
	if (bActiveViewport)
	{
		// Get active interaction mode.
		IInteractionMode* pmInteractionMode =
			InteractionModeService->ActiveMode;
		if (pmInteractionMode != NULL)
		{
			int iHoverX;
			int iHoverY;
			String* strHoverText = 
				pmInteractionMode->GetHoverData(&iHoverX, &iHoverY);

			const char* pcHoverText = MStringToCharPointer(strHoverText);

			m_pkScreenConsole->CreateHoverText(pcHoverText, iHoverX,
				iHoverY);
			MFreeCharPointer(pcHoverText);
			if (strHoverText->Length)
			{
				NiScreenTexture* pkHoverTexture = m_pkScreenConsole
					->GetHoverScreenTexture();
				if (pkHoverTexture)
				{
					pkHoverTexture->Draw(pkRenderer);
				}
			}

			// 绘制框选框 [12/14/2009 hemeng]
			if(MFramework::Instance->Scene->GetRectSelectState() && m_bRenderRectSel)
			{
				if (m_pkRectSelTexture == NULL)
				{
					m_pkRectSelTexture = NiNew NiScreenTexture(MFramework::GetRectSelTexture()); 
					m_pkRectSelTexture->SetApplyMode(
						NiTexturingProperty::APPLY_REPLACE);
				}


				if (m_pkRectSelTexture)
				{
					unsigned int uiIndex = m_pkRectSelTexture->GetNumScreenRects();

					if (uiIndex != 0)
					{
						m_pkRectSelTexture->RemoveAllScreenRects();
					}
					m_pkRectSelTexture->AddNewScreenRect(m_iSelStartY,m_iSelStartX,m_iSelLength ,10,4,4,NiColorA::BLACK);
					m_pkRectSelTexture->AddNewScreenRect(m_iSelStartY,m_iSelStartX,10,m_iSelHeight,4,4,NiColorA::BLACK);

					m_pkRectSelTexture->AddNewScreenRect(m_iSelStartY + m_iSelHeight,m_iSelStartX,m_iSelLength ,10,4,4,NiColorA::BLACK);
					m_pkRectSelTexture->AddNewScreenRect(m_iSelStartY,m_iSelStartX + m_iSelLength,10,m_iSelHeight,4,4,NiColorA::BLACK);

					m_pkRectSelTexture->MarkAsChanged(NiScreenTexture::EVERYTHING_MASK);
					m_pkRectSelTexture->Draw(pkRenderer);
				}
			}
		}
	}

	if (m_pkScreenConsole)
	{
		NiScreenTexture* pkConsoleTexture = m_pkScreenConsole
			->GetActiveScreenTexture();
		if (pkConsoleTexture)
		{
			pkConsoleTexture->Draw(pkRenderer);
		}	
	}
}

void MViewport::RenderNPCCreatorRange(MEntity* pmNPCCreator, NiRenderer* pkRenderer)
{
	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (NULL == pTerrain)
	{
		return;
	}

//	int nCreateType;
	NiFixedString strTemp;	
	NiBool bPropExist = pmNPCCreator->GetNiEntityInterface()->GetPropertyData(CREATETYPE, strTemp);

	/*if (!strTemp.Equals("2 衍生生成"))
	{
		return;
	}

	int iRadius = 0;
	pmNPCCreator->GetNiEntityInterface()->GetPropertyData(RADIUS, iRadius);

	if (iRadius < 1)
	{
		return;
	}*/
	int iRadiusX = 0;
	int iRadiusY = 0;
	bPropExist = pmNPCCreator->GetNiEntityInterface()->GetPropertyData(RADIUSX, iRadiusX);
	if (bPropExist)
	{
		pmNPCCreator->GetNiEntityInterface()->GetPropertyData(RADIUSY, iRadiusY);
	}
	else
	{
		return;
	}

	NiPoint3 vPos;
	pmNPCCreator->GetNiEntityInterface()->GetPropertyData(TRANSLATION, vPos);

	//int iVertNum = (iRadius*2+2) * 4;
	int iVertNum = (iRadiusX + iRadiusY + 2) * 4;

	int *pVertsIndex = (int *)NiMalloc( sizeof(int)* iVertNum );

	NiPoint3 *pVerts  = NiNew NiPoint3[ iVertNum ];
	NiColorA *pColors = NiNew NiColorA[ iVertNum ];
	NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* iVertNum );

	//得到范围框所包含的点


	//if ( (iVertNum = pTerrain->GetCreateNPCRangeArray(vPos, iRadius, pVertsIndex)) == -1)
	//{
	//	return;
	//}
	if ( (iVertNum = pTerrain->GetCreateNPCRangeArrayM1(vPos, iRadiusX, iRadiusY, pVertsIndex)) == -1)
	{
		return;
	}

	//填充颜色和链接顺序
	int index = 0;
	for(index = 0; index < iVertNum; ++index)
	{
		int pointIndex = pVertsIndex[index];
		NiPoint3 tempPoint = pTerrain->GetVertex(pointIndex);
		tempPoint.z += COLLISION_DATA_OFFSET;
		pVerts[index] = tempPoint;
		pColors[ index ] = NiColorA( 0, 1, 0, 1 );
		bConnect[ index ] = true;
	}

	//生成新的范围框
	NiLinesPtr pLine = NiNew NiLines( iVertNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );
	//pLine->SetName(strNPCCreateName);
	//m_spPhyxRoot->AttachChild(pLine);
	//m_spTerrainRoot->AttachChild( m_spPhyxRoot );

	NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
	pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
	pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
	pLine->AttachProperty( pVCProp );

	pLine->Update(.0f);
	pLine->UpdateProperties();
	pLine->UpdateEffects();

	pLine->RenderImmediate(pkRenderer);
	return;
}

void MViewport::RenderOverTopVertexs(NiRenderer* pkRenderer,set<int> setIndex)
{
	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (NULL == pTerrain)
	{
		return;
	}

	unsigned long nVertexNum = setIndex.size(); //统计点的数量
	if (nVertexNum <=0 || nVertexNum > 65535)
	{
		return;
	}

	/*/////////////////////////////////////////////////////////////////////////
	//用粒子来渲染点的方法
	NiPoint3 *pVerts  = NiNew NiPoint3[ nVertexNum ];
	NiColorA *pColors = NiNew NiColorA[ nVertexNum ];

	set<int>::iterator itor = pTerrain->m_OverTopVertexs.begin();
	int index = 0;
	for (;itor != pTerrain->m_OverTopVertexs.end();++itor)
	{
		int pointIndex = *itor;
		NiPoint3 tempPoint = pTerrain->GetVertex(pointIndex);
		tempPoint.z += COLLISION_DATA_OFFSET;
		pVerts[index] = tempPoint;
		pColors[ index ] = NiColorA( 0, 1, 0, 1 );	
		index++;
	}

	NiParticlesPtr pParticles = NiNew NiParticles(nVertexNum, pVerts, NULL, pColors);

	NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
	pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
	pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
	pParticles->AttachProperty( pVCProp );

	pParticles->Update(.0f);
	pParticles->UpdateProperties();
	pParticles->UpdateEffects();

	pParticles->RenderImmediate(pkRenderer);
	/////////////////////////////////////////////////////////////////////////*/


	//////////////////////////////////////////////////////////////////////////
	//用小矩形来表示点的渲染方法
	nVertexNum *= 4;
	NiPoint3 *pVerts  = NiNew NiPoint3[ nVertexNum ];
	NiColorA *pColors = NiNew NiColorA[ nVertexNum ];
	NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* nVertexNum );

	// 初始化颜色 [2/1/2010 hemeng]
	float pColorTag[3];
	int iColorType = MFramework::Instance->Scene->GetTerrainHeightCheckColorType();
	for (int i = 0; i < 3; i ++)
	{
		if ( i == iColorType)
		{
			pColorTag[i] = 1.0f;
		}
		else
		{
			pColorTag[i] = 0.0f;
		}
	}

	set<int>::iterator itor = setIndex.begin();
	unsigned long index = 0;
	for (;itor != setIndex.end();++itor)
	{
		int pointIndex = *itor;
		NiPoint3 tempPoint = pTerrain->GetVertex(pointIndex);
		tempPoint.z += COLLISION_DATA_OFFSET;
		//计算点
		NiPoint3 tempPointUp(tempPoint);
		NiPoint3 tempPointLeft(tempPoint);
		NiPoint3 tempPointDown(tempPoint);
		NiPoint3 tempPointRight(tempPoint);
		tempPointUp.y += 0.1f;
		tempPointLeft.x -= 0.1f;
		tempPointDown.y -= 0.1f;
		tempPointRight.x += 0.1f;
		pVerts[ 4*index ] = tempPointUp;
		pVerts[ 4*index + 1 ] = tempPointLeft;
		pVerts[ 4*index + 2 ] = tempPointDown;
		pVerts[ 4*index + 3 ] = tempPointRight;
		//颜色
		pColors[ 4*index ] = NiColorA( pColorTag[0],pColorTag[1],pColorTag[2],1);	
		pColors[ 4*index + 1 ] = NiColorA( pColorTag[0],pColorTag[1],pColorTag[2],1);
		pColors[ 4*index + 2 ] = NiColorA( pColorTag[0],pColorTag[1],pColorTag[2],1);
		pColors[ 4*index + 3 ] = NiColorA( pColorTag[0],pColorTag[1],pColorTag[2],1);
		//点的链接
		bConnect[ 4*index ] = true;
		bConnect[ 4*index + 1 ] = true;
		bConnect[ 4*index + 2 ] = true;
		bConnect[ 4*index + 3 ] = false;
		index++;
	}

	NiLinesPtr pLine = NiNew NiLines( nVertexNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );
	//pLine->SetName(strNPCCreateName);
	//m_spPhyxRoot->AttachChild(pLine);
	//m_spTerrainRoot->AttachChild( m_spPhyxRoot );

	NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
	pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
	pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
	pLine->AttachProperty( pVCProp );

	pLine->Update(.0f);
	pLine->UpdateProperties();
	pLine->UpdateEffects();

	pLine->RenderImmediate(pkRenderer);
	//////////////////////////////////////////////////////////////////////////
}

void MViewport::RenderRegions(NiRenderer* pkRenderer, int nRegionType)
{
	static int ncolor = 0;//每次更新，使颜色不断变化，用来标识选中的区域

	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (NULL == pTerrain)
	{
		return;
	}

	CAreaManager* pAreaMgr = pTerrain->m_pAreaMgr;
	vector<CArea*>::iterator it = pAreaMgr->m_AreaList.begin();
	vector<CArea*>::iterator itEnd = pAreaMgr->m_AreaList.end();
	for (;it != itEnd;it++)
	{
		bool bRegionNeedRender = true;

		//if (MFramework::Instance->Scene->GetRegionOperType() == 1)//创建区域模式
		//{
		//	bRegionNeedRender = true;
		//}

		//if (MFramework::Instance->Scene->GetRegionOperType() == 2)//更新区域模式
		//{
		//	if ((*it)->m_uiAreaID == MFramework::Instance->Scene->GetCurrentRegionID())
		//	{
		//		bRegionNeedRender = true;
		//	}
		//}

		//if (MFramework::Instance->Scene->GetRegionOperType() == 3)//显示区域模式
		//{
		//	if (nRegionType == 0)
		//	{
		//		bRegionNeedRender = true;
		//	}

		//	if ((*it)->m_usAreaType == nRegionType)
		//	{
		//		bRegionNeedRender = true;
		//	}
		//}

		// 获取区域的颜色
		float fRegionColorR = float(MFramework::Instance->Scene->GetRegionColorByType((*it)->m_usAreaType, 1)) / 255;
		float fRegionColorG = float(MFramework::Instance->Scene->GetRegionColorByType((*it)->m_usAreaType, 2)) / 255;
		float fRegionColorB = float(MFramework::Instance->Scene->GetRegionColorByType((*it)->m_usAreaType, 3)) / 255;
		float fRegionColorA = float(MFramework::Instance->Scene->GetRegionColorByType((*it)->m_usAreaType, 4)) / 255;

		if ((*it)->m_uiAreaID == MFramework::Instance->Scene->GetCurrentRegionID())
		{

			ncolor += 1;
			ncolor = ncolor % 255;

			fRegionColorR  = float(ncolor) /255;


			fRegionColorG  = float(ncolor) /255;

			fRegionColorB  = float(ncolor) /255;

			fRegionColorA  = float(ncolor) /255;

		}
		if (bRegionNeedRender)
		{
			//获取区域的形状
			int nRegionShape = (*it)->m_usAreaShape;

			//获取顶点的数量
			int nRegionPointNum = (*it)->GetAreaPointNum();
			if (nRegionShape == 0)
			{
				nRegionPointNum = 4;	//如果区域的形状为正方形，则只渲染由前两个点构成的四边形
			}
			//初始化临时空间
			NiPoint3 *pVerts  = NiNew NiPoint3[ nRegionPointNum ];
			NiColorA *pColors = NiNew NiColorA[ nRegionPointNum ];
			NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* nRegionPointNum );
			//计算出点，颜色，链接关系
			if (nRegionShape == 0)
			{
				AreaPoint* pFirstPoint = (*it)->m_AreaPointList[0];
				AreaPoint* pSecondPoint = (*it)->m_AreaPointList[0];
				if ((*it)->GetAreaPointNum() > 1)
				{
					pSecondPoint = (*it)->m_AreaPointList[1];
				}

				//第一个点
				NiPoint3 tempPoint1 = pTerrain->GetVertex(pFirstPoint->nXPos, pFirstPoint->nYPos);
				tempPoint1.z += COLLISION_DATA_OFFSET;
				pVerts[ 0 ] = tempPoint1;
				pColors[ 0 ] = NiColorA( fRegionColorR, fRegionColorG, fRegionColorB, fRegionColorA );
				bConnect[ 0 ] = true;

				//第一点的X，第二点的Y
				NiPoint3 tempPoint2 = pTerrain->GetVertex(pFirstPoint->nXPos, pSecondPoint->nYPos);
				tempPoint2.z += COLLISION_DATA_OFFSET;
				pVerts[ 1 ] = tempPoint2;
				pColors[ 1 ] = NiColorA( fRegionColorR, fRegionColorG, fRegionColorB, fRegionColorA );
				bConnect[ 1 ] = true;

				//第二个点
				NiPoint3 tempPoint3 = pTerrain->GetVertex(pSecondPoint->nXPos, pSecondPoint->nYPos);
				tempPoint3.z += COLLISION_DATA_OFFSET;
				pVerts[ 2 ] = tempPoint3;
				pColors[ 2 ] = NiColorA( fRegionColorR, fRegionColorG, fRegionColorB, fRegionColorA );
				bConnect[ 2 ] = true;

				//第二点的X，第一点的Y
				NiPoint3 tempPoint4 = pTerrain->GetVertex(pSecondPoint->nXPos, pFirstPoint->nYPos);
				tempPoint4.z += COLLISION_DATA_OFFSET;
				pVerts[ 3 ] = tempPoint4;
				pColors[ 3 ] = NiColorA( fRegionColorR, fRegionColorG, fRegionColorB, fRegionColorA );
				bConnect[ 3 ] = true;

			} 
			else
			{
				for (int nIndex = 0;nIndex < nRegionPointNum;nIndex++)
				{
					AreaPoint* pTemp = (*it)->m_AreaPointList[nIndex];
					NiPoint3 tempPoint = pTerrain->GetVertex(pTemp->nXPos, pTemp->nYPos);
					tempPoint.z += COLLISION_DATA_OFFSET;

					pVerts[ nIndex ] = tempPoint;
					pColors[ nIndex ] = NiColorA( fRegionColorR, fRegionColorG, fRegionColorB, fRegionColorA );
					bConnect[ nIndex ] = true;
				}
			}
			//开始渲染
			NiLinesPtr pLine = NiNew NiLines( nRegionPointNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );

			NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
			pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
			pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
			pLine->AttachProperty( pVCProp );

			pLine->Update(.0f);
			pLine->UpdateProperties();
			pLine->UpdateEffects();

			pLine->RenderImmediate(pkRenderer);
		}
	}


}

//////////////////////////////////////////////////////////////////////////
//add by  和萌 渲染水域
void MViewport::RenderWaterRegion(NiRenderer* pkRenderer)
{
	if ( CGrideBaseRiver::GetInstance()->GetRiverGeom() )
	{
		//NiTriShapePtr pRiver = CRiverManager::GetInstance()->GetRiverGeom();
		NiTriShapePtr pRiver = CGrideBaseRiver::GetInstance()->GetRiverGeom();
		if (pRiver)
		{
			pRiver->RenderImmediate( pkRenderer );
		}
		
	}
	
}

void	MViewport::DrawWaterRegionFrame(NiRenderer* pkRenderer)
{
	/*if (CRiverManager::GetInstance() == NULL)
	{
		return;
	}
	if (CRiverManager::GetInstance()->GetRiverGeom() == NULL)
	{
		return;
	}*/
	//初始水体优化器
	//CTerrain *pTerrain = MFramework::Instance->Scene->get_Terrain();
	//CRiverOptimizer	*pRiverOpt = new CRiverOptimizer(CRiverManager::GetInstance() , pTerrain );
	//if ( pRiverOpt == NULL )
	//{
	//	return;
	//}
	//NiPoint3 kMinPoint = pRiverOpt->m_kMinPoint;
	//NiPoint3 kMaxPoint = pRiverOpt->m_kMaxPoint;
	//kMaxPoint.z = pTerrain->GetRawHeight( NiPoint2(kMaxPoint.x, kMaxPoint.y) ) + 0.5f;
	//kMinPoint.z = pTerrain->GetRawHeight( NiPoint2(kMinPoint.x, kMinPoint.y) ) + 0.5f;

	//int nRegionPointNum = 4;
	//NiPoint3 *pVerts  = NiNew NiPoint3[ nRegionPointNum ];
	//NiColorA *pColors = NiNew NiColorA[ nRegionPointNum ];
	//NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* nRegionPointNum );

	////////////////////////////////////////////////////////////////////////////
	//// 3 *------* 2
	////   |      |
	////   |      |
	//// 0 *------* 1
	//for ( int i = 0; i < nRegionPointNum; i ++ )
	//{
	//	pVerts[i] = kMinPoint;
	//	//red
	//	pColors[i] = NiColorA( 1.0,0.0,0.0);
	//	bConnect[i] = true;
	//}
	//pVerts[1].x = kMaxPoint.x;
	//pVerts[2] = kMaxPoint;
	//pVerts[3].y = kMaxPoint.y;

	//NiLinesPtr pLine = NiNew NiLines( nRegionPointNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );

	//NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
	//pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
	//pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
	//pLine->AttachProperty( pVCProp );

	//pLine->Update(.0f);
	//pLine->UpdateProperties();
	//pLine->UpdateEffects();

	//pLine->RenderImmediate(pkRenderer);

	//delete pRiverOpt;
}

void MViewport::SetRectSelectData(bool bRenderRectSel,int iSelStartX,int iSelStartY,int iSelLength,int iSelHeight)
{
	m_bRenderRectSel = bRenderRectSel;
	m_iSelStartX = iSelStartX;
	m_iSelStartY = iSelStartY;
	m_iSelLength = iSelLength;
	m_iSelHeight = iSelHeight;

}

void MViewport::SetRectSelectRenderState(bool bRenderRectSel)
{
	m_bRenderRectSel = bRenderRectSel;
}

bool MViewport::RenderSnapEntityVertice(unsigned int eType)
{
	if (eType == CSnapTool::EP_NONE)
	{
		return false;
	}
	vector<NiPoint3*> kVertices;
	RecursiveFindGeometries kFunctor;

	if (NULL == CSnapTool::Instance()->GetSrcEntity() || NULL == CSnapTool::Instance()->GetDesEntity())
	{
		return false;
	}

	//获得几何形位置
	if (eType == CSnapTool::EP_SRC)
	{
		NiTNodeTraversal::DepthFirst_AllObjects(CSnapTool::Instance()->GetSrcEntity(), kFunctor);		
	}
	else if (eType == CSnapTool::EP_DES)
	{
		NiTNodeTraversal::DepthFirst_AllObjects(CSnapTool::Instance()->GetDesEntity(), kFunctor);	
	}
	kVertices = kFunctor.m_pkPos;
	
	for (unsigned int uiIndex = 0; uiIndex < kVertices.size(); uiIndex++)
	{
		unsigned int nVertexNum = 4;
		NiPoint3 *pVerts  = NiNew NiPoint3[nVertexNum];
		NiColorA *pColors = NiNew NiColorA[nVertexNum];
		NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* nVertexNum );

		NiPoint3 tmpPointTop(*kVertices[uiIndex]);
		NiPoint3 tmpPointLeft(*kVertices[uiIndex]);
		NiPoint3 tmpPointRight(*kVertices[uiIndex]);
		NiPoint3 tmpPointBtm(*kVertices[uiIndex]);
		NiPoint3 kOffset = NiPoint3(0.5f,0.5f,0.0f);

		tmpPointTop.z += 0.5f;
		tmpPointBtm.z -= 0.5f;
		tmpPointLeft -= kOffset;
		tmpPointRight += kOffset;

		//位置
		pVerts[0] = tmpPointTop;
		pVerts[1] = tmpPointBtm;
		pVerts[2] = tmpPointLeft;		
		pVerts[3] = tmpPointRight;

		//颜色
		for (unsigned int uiCount = 0; uiCount < nVertexNum; uiCount++)
		{
			if (eType == CSnapTool::EP_SRC)
			{
				pColors[uiCount] = NiColorA(1.0f,0.0f,0.0f,1.0f);
			}
			else
			{
				pColors[uiCount] = NiColorA(0.0f,1.0f,0.0f,1.0f);
			}
			
		}
		//连接
		bConnect[0] = true;
		bConnect[1] = false;
		bConnect[2] = true;
		bConnect[3] = false;

		
		NiLinesPtr pLine1 = NiNew NiLines( nVertexNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );		
		NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
		pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
		pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
		pLine1->AttachProperty( pVCProp );

		pLine1->Update(0.0f);
		pLine1->UpdateProperties();
		pLine1->UpdateEffects();

		pLine1->RenderImmediate(NiRenderer::GetRenderer());
	}
	
	for (UINT uiIndex = 0; uiIndex < kVertices.size(); uiIndex++)
	{
		NiDelete kVertices[uiIndex];
		kVertices[uiIndex] = NULL;
	}
	kVertices.clear();

	return true;
}

//----------------------------------------------------------------------------------
void MViewport::RenderChunkID()
{
	// Get viewport camera.
	NiCamera* pkCamera = GetNiCamera();
	if (!pkCamera)
	{
		// Don't render if there is no camera.
		return;
	}

	// Set camera on rendering context.
	MRenderingContext* pmRenderingContext = MRenderer::Instance
		->RenderingContext;
	pmRenderingContext->GetRenderingContext()->m_pkCamera = pkCamera;

	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (NULL == pTerrain)
	{
		return;
	}

	int iNumChunkX = pTerrain->GetChunkNumX();
	int iNumChunkY = pTerrain->GetChunkNumY();
	int iChunkID = 0;
	int iVerNum = iNumChunkX * GRIDINCHUNK + 1; 

	for (int iChunkY = 0; iChunkY < iNumChunkY; iChunkY++)
	{
		for (int iChunkX = 0; iChunkX < iNumChunkX; iChunkX++)
		{
			// find center position of chunk [7/14/2009 hemeng]
			int iPointIndex = (iChunkY * GRIDINCHUNK + GRIDINCHUNK / 4) * iVerNum +
								GRIDINCHUNK * iChunkX + (GRIDINCHUNK / 4);
			NiPoint3 kPos = pTerrain->GetVertex(iPointIndex);
			kPos.z += 1.0f;
			
			// get num sign [7/14/2009 hemeng]
			NiAVObject* pkChunkHeighID = MFramework::GetChunkNumSignAt( iChunkID / 10 );
			NiAVObject* pkChunkLowID = MFramework::GetChunkNumSignAt( iChunkID % 10 );
			if (pkChunkHeighID == NULL || pkChunkLowID == NULL)
			{
				continue;
			}
			
			pkChunkHeighID->SetTranslate(kPos);
			pkChunkHeighID->SetScale(5.0f);
			pkChunkHeighID->Update(0.0f);

			NiDrawScene(pkCamera,pkChunkHeighID,*pmRenderingContext->GetRenderingContext()->m_pkCullingProcess);

			kPos.x += 5.0f;
			pkChunkLowID->SetTranslate(kPos);
			pkChunkLowID->SetScale(5.0f);
			pkChunkLowID->Update(0.0f);
			
			NiDrawScene(pkCamera,pkChunkLowID,*pmRenderingContext->GetRenderingContext()->m_pkCullingProcess);

			iChunkID ++;
		}
	}

}

//----------------------------------------------------------------------------------
void MViewport::RenderAABB()
{
	MVerifyValidInstance;
	NiRenderer* pkRenderer = MRenderer::Instance->RenderingContext
		->GetRenderingContext()->m_pkRenderer;

	MEntity* pmSelectedEntities[] = SelectionService->GetSelectedEntities();

	for (int i = 0; i < pmSelectedEntities->Length; i++)
	{
		if (MLightManager::EntityIsLight(pmSelectedEntities[i]))
		{
			continue;
		}

		NiAVObject* pAVObj = pmSelectedEntities[i]->GetSceneRootPointer(0);
		if (NULL == pAVObj)
		{
			continue;
		}

		NiBox aabb;
		CreateAABBFromNode((NiNode*)pAVObj, aabb);

		NiEntityInterface* pkEntityInterface = pmSelectedEntities[i]->GetNiEntityInterface();
		NiPoint3 kTranslation;
		if (!pkEntityInterface->GetPropertyData("Translation",kTranslation))
		{
			continue;
		}

		NiMatrix3 kRotation;
		if (!pkEntityInterface->GetPropertyData("Rotation",kRotation))
		{
			continue;
		}

		CBoundGeometroy::GetInstance()->SetBoundPosCenter(kTranslation);
		CBoundGeometroy::GetInstance()->SetBoundRadius(1.0f);
		NiPoint3 kExtent = NiPoint3(aabb.m_afExtent[0],aabb.m_afExtent[1],aabb.m_afExtent[2]);

		String* strEntityName = pmSelectedEntities[i]->Name;
		const char* szEntityName = MStringToCharPointer(strEntityName);
		CBoundGeometroy::GetInstance()->SetBox(aabb.m_kCenter,kExtent,kRotation,szEntityName);
		MFreeCharPointer(szEntityName);
		
		NiLinesPtr pLine = CBoundGeometroy::GetInstance()->GetBox();

		if (NULL == pLine)
		{
			continue;
		}
		pLine->RenderImmediate(pkRenderer);		

		// draw center [9/14/2009 hemeng]
		NiPoint3 kCenter = kTranslation + aabb.m_kCenter;
		unsigned int nVertexNum = 4;
		NiPoint3 *pVerts  = NiNew NiPoint3[nVertexNum];
		NiColorA *pColors = NiNew NiColorA[nVertexNum];
		NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* nVertexNum );

		NiPoint3 tmpPointTop(kCenter);
		NiPoint3 tmpPointLeft(kCenter);
		NiPoint3 tmpPointRight(kCenter);
		NiPoint3 tmpPointBtm(kCenter);
		NiPoint3 kOffset = NiPoint3(0.5f,0.5f,0.0f);

		tmpPointTop.z += 0.5f;
		tmpPointBtm.z -= 0.5f;
		tmpPointLeft -= kOffset;
		tmpPointRight += kOffset;

		//位置
		pVerts[0] = tmpPointTop;
		pVerts[1] = tmpPointBtm;
		pVerts[2] = tmpPointLeft;		
		pVerts[3] = tmpPointRight;

		//颜色
		for (unsigned int uiCount = 0; uiCount < nVertexNum; uiCount++)
		{
			pColors[uiCount] = NiColorA(0.0f,1.0f,0.0f,1.0f);
		}
		//连接
		bConnect[0] = true;
		bConnect[1] = false;
		bConnect[2] = true;
		bConnect[3] = false;


		NiLinesPtr pLine1 = NiNew NiLines( nVertexNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );		
		NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
		pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
		pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
		pLine1->AttachProperty( pVCProp );

		pLine1->Update(0.0f);
		pLine1->UpdateProperties();
		pLine1->UpdateEffects();

		pLine1->RenderImmediate(pkRenderer);
	}

}

//----------------------------------------------------------------------------------

void MViewport::RenderChunkBorder()
{
	NiRenderer* pkRenderer = MRenderer::Instance->RenderingContext
		->GetRenderingContext()->m_pkRenderer;

	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (NULL == pTerrain)
	{
		return;
	}

	int iNumChunkX = pTerrain->GetChunkNumX();
	int iNumChunkY = pTerrain->GetChunkNumY();

	// 绘制行方向的 border
	for (int i=1; i<iNumChunkY; i++) // 遍历行
	{
		int iVertNum = iNumChunkX*GRIDINCHUNK+1;	// 一行上的顶点数

		NiPoint3 *pVerts  = NiNew NiPoint3[ iVertNum ];
		NiColorA *pColors = NiNew NiColorA[ iVertNum ];
		NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* iVertNum );

		//填充颜色和链接顺序
		int index = 0;
		for(index = 0; index < iVertNum; ++index)
		{
			int pointIndex = i*(iVertNum*GRIDINCHUNK) + index;
			NiPoint3 tempPoint = pTerrain->GetVertex(pointIndex);
			tempPoint.z += COLLISION_DATA_OFFSET;
			pVerts[index] = tempPoint;
			pColors[ index ] = NiColorA( 0, 0, 1, 1 );
			bConnect[ index ] = true;
		}

		//生成新的 border
		NiLinesPtr pLine = NiNew NiLines( iVertNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );

		NiVertexColorPropertyPtr spVertexColorProp = NiNew NiVertexColorProperty;
		spVertexColorProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
		spVertexColorProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
		pLine->AttachProperty( spVertexColorProp );

		pLine->UpdateProperties();
		pLine->Update( 0.0f );

		pLine->RenderImmediate(pkRenderer);

		pLine = NULL;	
	}

	// 绘制列方向的 border
	for (int i=1; i<iNumChunkX; i++) // 遍历列chunk
	{
		int iVertNum = iNumChunkY*GRIDINCHUNK+1;	// 一列上的顶点数
	
		NiPoint3 *pVerts  = NiNew NiPoint3[ iVertNum ];
		NiColorA *pColors = NiNew NiColorA[ iVertNum ];
		NiBool* bConnect = (NiBool*)NiMalloc( sizeof(NiBool)* iVertNum );

		//填充颜色和链接顺序
		int index = 0;
		for(index = 0; index < iVertNum; ++index)	// 遍历一列上的顶点
		{
			int pointIndex = (iVertNum*index) + i*GRIDINCHUNK;
			NiPoint3 tempPoint = pTerrain->GetVertex(pointIndex);
			tempPoint.z += COLLISION_DATA_OFFSET;
			pVerts[index] = tempPoint;
			pColors[ index ] = NiColorA( 0, 0, 1, 1 );
			bConnect[ index ] = true;
		}

		//生成新的 border
		NiLinesPtr pLine = NiNew NiLines( iVertNum, pVerts, pColors, 0, 0, NiGeometryData::NBT_METHOD_NONE, bConnect );

		NiVertexColorProperty * pVCProp = NiNew NiVertexColorProperty;
		pVCProp->SetLightingMode( NiVertexColorProperty::LIGHTING_E );
		pVCProp->SetSourceMode( NiVertexColorProperty::SOURCE_EMISSIVE );
		pLine->AttachProperty( pVCProp );

		pLine->Update(.0f);
		pLine->UpdateProperties();
		pLine->UpdateEffects();

		pLine->RenderImmediate(pkRenderer);

		pLine = NULL;
	}

	// render chunk id [7/14/2009 hemeng]
	RenderChunkID();
}

//---------------------------------------------------------------------------
float MViewport::get_Left()
{
	MVerifyValidInstance;

	return m_fLeft;
}
//---------------------------------------------------------------------------
float MViewport::get_Right()
{
	MVerifyValidInstance;

	return m_fRight;
}
//---------------------------------------------------------------------------
float MViewport::get_Top()
{
	MVerifyValidInstance;

	return m_fTop;
}
//---------------------------------------------------------------------------
float MViewport::get_Bottom()
{
	MVerifyValidInstance;

	return m_fBottom;
}
//---------------------------------------------------------------------------
bool MViewport::SetViewportValues(float fLeft, float fRight, float fTop,
								  float fBottom)
{
	MVerifyValidInstance;

	if (fLeft < 0.0f || fLeft > 1.0f || fRight < 0.0f || fRight > 1.0f ||
		fTop < 0.0f || fTop > 1.0f || fBottom < 0.0f || fBottom > 1.0f)
	{
		return false;
	}

	m_fLeft = fLeft;
	m_fRight = fRight;
	m_fTop = fTop;
	m_fBottom = fBottom;

	UpdateCameraViewport(GetNiCamera());
	UpdateBorderGeometry();
	UpdateScreenConsole();

	return true;
}
//---------------------------------------------------------------------------
int MViewport::get_Width()
{
	MVerifyValidInstance;

	return (int) ((m_fRight - m_fLeft) * MRenderer::Instance->Width);
}
//---------------------------------------------------------------------------
int MViewport::get_Height()
{
	MVerifyValidInstance;

	return (int) ((m_fTop - m_fBottom) * MRenderer::Instance->Height);
}
//---------------------------------------------------------------------------
MScene* MViewport::get_ToolScene()
{
	MVerifyValidInstance;

	return m_pmToolScene;
}
//---------------------------------------------------------------------------
IRenderingMode* MViewport::get_RenderingMode()
{
	MVerifyValidInstance;

	return m_pmRenderingMode;
}
//---------------------------------------------------------------------------
void MViewport::set_RenderingMode(IRenderingMode* pmRenderingMode)
{
	MVerifyValidInstance;

	m_pmRenderingMode = pmRenderingMode;
	MEventManager::Instance->RaiseViewportRenderingModeChanged(this,
		m_pmRenderingMode);
}
//---------------------------------------------------------------------------
System::Drawing::Rectangle MViewport::get_CameraNameRect()
{
	MVerifyValidInstance;

	System::Drawing::Rectangle mRect(0, 0, 0, 0);
	if (m_pkScreenConsole)
	{
		const char* pcCameraName = m_pkScreenConsole->GetLine(0);
		MAssert(pcCameraName != NULL, "Null screen console text!");
		size_t stCharCount = strlen(pcCameraName);

		NiScreenConsole::NiConsoleFont* pkFont = m_pkScreenConsole->GetFont();
		MAssert(pkFont != NULL, "Null screen console font!");

		mRect.X = (int)((m_fLeft * MRenderer::Instance->Width)
			+ ms_iScreenTextureOffsetX);
		mRect.Y = (int)(((1-m_fTop) * MRenderer::Instance->Height));
		mRect.Width = pkFont->m_uiCharSpacingX * stCharCount;
		mRect.Height = pkFont->m_uiCharSpacingY * 2;
	}

	return mRect;
}
//---------------------------------------------------------------------------
void MViewport::InitToolScene()
{
	MVerifyValidInstance;

	NiScene* pkScene = NiNew NiScene("Tool Scene", 10);
	m_pmToolScene = MSceneFactory::Instance->Get(pkScene);
}
//---------------------------------------------------------------------------
void MViewport::CreateBorderGeometry()
{
	MVerifyValidInstance;

	// Create NiLines geometry.
	unsigned short usNumVertices = 4;
	NiPoint3* pkVertices = NiNew NiPoint3[usNumVertices];
	NiBool* pbFlags = NiAlloc(NiBool, usNumVertices);
	for (unsigned int ui = 0; ui < usNumVertices; ui++)
	{
		pbFlags[ui] = true;
	}
	m_pkBorder = NiNew NiLines(usNumVertices, pkVertices, NULL, NULL, 0,
		NiGeometryData::NBT_METHOD_NONE, pbFlags);
	MInitRefObject(m_pkBorder);
	m_pkBorder->SetConsistency(NiGeometryData::MUTABLE);
	UpdateBorderGeometry();

	// Attach material property.
	m_pkBorderMaterial = NiNew NiMaterialProperty();
	m_pkBorderMaterial->SetEmittance(NiColor::BLACK);
	m_pkBorder->AttachProperty(m_pkBorderMaterial);

	// Attach vertex color property.
	NiVertexColorProperty* pkVertexColorProperty = NiNew
		NiVertexColorProperty();
	pkVertexColorProperty->SetSourceMode(
		NiVertexColorProperty::SOURCE_IGNORE);
	pkVertexColorProperty->SetLightingMode(NiVertexColorProperty::LIGHTING_E);
	m_pkBorder->AttachProperty(pkVertexColorProperty);

	// Attach z-buffer property.
	NiZBufferProperty* pkZBufferProperty = NiNew NiZBufferProperty();
	pkZBufferProperty->SetZBufferTest(false);
	pkZBufferProperty->SetZBufferWrite(false);
	m_pkBorder->AttachProperty(pkZBufferProperty);

	// Perform initial update.
	m_pkBorder->Update(0.0f);
	m_pkBorder->UpdateProperties();
	m_pkBorder->UpdateEffects();
	m_pkBorder->UpdateNodeBound();
}
//---------------------------------------------------------------------------
void MViewport::UpdateBorderGeometry()
{
	MVerifyValidInstance;

	if (!m_pkBorder)
	{
		return;
	}

	// Adjust left and top locations to ensure that the lines are visible.
	float fLeft = (m_fLeft == 0.0f ? 0.001f : m_fLeft);
	float fTop = (m_fTop == 1.0f ? 0.999f : m_fTop);

	NiPoint3* pkVertices = m_pkBorder->GetVertices();
	pkVertices[0] = NiPoint3(fLeft, 1.0f - fTop, 0.0f);
	pkVertices[1] = NiPoint3(fLeft, 1.0f - m_fBottom, 0.0f);
	pkVertices[2] = NiPoint3(m_fRight, 1.0f - m_fBottom, 0.0f);
	pkVertices[3] = NiPoint3(m_fRight, 1.0f - fTop, 0.0f);
	m_pkBorder->GetModelData()->MarkAsChanged(NiGeometryData::VERTEX_MASK);
}
//---------------------------------------------------------------------------
void MViewport::CreateScreenConsole()
{
	MVerifyValidInstance;

	if (!m_pkScreenConsole)
	{
		m_pkScreenConsole = NiNew NiScreenConsole();
		MInitRefObject(m_pkScreenConsole);

		const char* pcPath = MStringToCharPointer(String::Concat(
			MFramework::Instance->AppStartupPath, ".\\AppData\\"));
		m_pkScreenConsole->SetDefaultFontPath(pcPath);
		MFreeCharPointer(pcPath);

		m_pkScreenConsole->SetFont(m_pkScreenConsole->CreateConsoleFont());
		UpdateScreenConsole();
		m_pkScreenConsole->Enable(true);
	}
}
//---------------------------------------------------------------------------
void MViewport::ReloadScreenConsolePixelData()
{
	MVerifyValidInstance;

	if (m_pkScreenConsole)
	{
		NiScreenTexture* pkConsoleTexture = m_pkScreenConsole
			->GetActiveScreenTexture();
		if (pkConsoleTexture)
		{
			NiSourceTexture* pkSourceTexture = NiDynamicCast(NiSourceTexture,
				pkConsoleTexture->GetTexture());
			if (pkSourceTexture)
			{
				pkSourceTexture->LoadPixelDataFromFile();
			}
		}
	}
}
//---------------------------------------------------------------------------
void MViewport::UpdateScreenConsole()
{
	MVerifyValidInstance;

	if (m_pkScreenConsole)
	{
		m_pkScreenConsole->SetDimensions(NiPoint2((float) Width, (float)
			Height));
		m_pkScreenConsole->SetOrigin(NiPoint2(ms_iScreenTextureOffsetX +
			m_fLeft * MRenderer::Instance->Width, (1-m_fTop) *
			MRenderer::Instance->Height));

		m_pkScreenConsole->SetCamera(GetNiCamera());
		if (m_pmCamera != NULL)
		{
			const char* pcCameraName = MStringToCharPointer(m_pmCamera->Name);
			m_pkScreenConsole->SetLine(pcCameraName, 0);
			MFreeCharPointer(pcCameraName);
		}
		else
		{
			m_pkScreenConsole->SetLine(NULL, 0);
		}
		m_pkScreenConsole->RecreateText();
	}
}
//---------------------------------------------------------------------------
void MViewport::UpdateCameraViewport(NiCamera* pkCamera)
{
	MVerifyValidInstance;

	if (pkCamera)
	{
		NiRect<float> kViewport(m_fLeft, m_fRight, m_fTop, m_fBottom);
		pkCamera->SetViewPort(kViewport);
	}
}
//---------------------------------------------------------------------------
void MViewport::RegisterForHighlightColorSetting()
{
	MVerifyValidInstance;

	m_pkHighlightColor = NiNew NiColor();

	SettingsService->RegisterSettingsObject(ms_strHighlightColorSettingName,
		__box(Color::Yellow), SettingsCategory::PerUser);
	SettingsService->SetChangedSettingHandler(ms_strHighlightColorSettingName,
		SettingsCategory::PerUser, new SettingChangedHandler(this,
		&MViewport::OnHighlightColorChanged));
	OnHighlightColorChanged(NULL, NULL);

	OptionsService->AddOption(ms_strHighlightColorOptionName,
		SettingsCategory::PerUser, ms_strHighlightColorSettingName);
	OptionsService->SetHelpDescription(ms_strHighlightColorOptionName,
		"The color with which the active viewport is highlighted.");
}
//---------------------------------------------------------------------------
void MViewport::OnHighlightColorChanged(Object* pmSender,
										SettingChangedEventArgs* pmEventArgs)
{
	MVerifyValidInstance;

	__box Color* pmColor = dynamic_cast<__box Color*>(
		SettingsService->GetSettingsObject(ms_strHighlightColorSettingName,
		SettingsCategory::PerUser));
	if (pmColor != NULL)
	{
		*m_pkHighlightColor = NiColor(MUtility::RGBToFloat((*pmColor).R),
			MUtility::RGBToFloat((*pmColor).G),
			MUtility::RGBToFloat((*pmColor).B));
	}
}
//---------------------------------------------------------------------------
void MViewport::OnEntityNameChanged(MEntity* pmEntity, String* strOldName,
									bool bInBatch)
{
	if (pmEntity == m_pmCamera)
	{
		UpdateScreenConsole();
	}
}
//---------------------------------------------------------------------------
IRenderingModeService* MViewport::get_RenderingModeService()
{
	if (ms_pmRenderingModeService == NULL)
	{
		ms_pmRenderingModeService = MGetService(IRenderingModeService);
		MAssert(ms_pmRenderingModeService != NULL, "Rendering mode service "
			"not found!");
	}
	return ms_pmRenderingModeService;
}
//---------------------------------------------------------------------------
IInteractionModeService* MViewport::get_InteractionModeService()
{
	if (ms_pmInteractionModeService == NULL)
	{
		ms_pmInteractionModeService = MGetService(IInteractionModeService);
		MAssert(ms_pmInteractionModeService != NULL, "Interaction mode "
			"service not found!");
	}
	return ms_pmInteractionModeService;
}
//---------------------------------------------------------------------------
ISettingsService* MViewport::get_SettingsService()
{
	if (ms_pmSettingsService == NULL)
	{
		ms_pmSettingsService = MGetService(ISettingsService);
		MAssert(ms_pmSettingsService != NULL, "Settings service not found!");
	}
	return ms_pmSettingsService;
}
//---------------------------------------------------------------------------
IOptionsService* MViewport::get_OptionsService()
{
	if (ms_pmOptionsService == NULL)
	{
		ms_pmOptionsService = MGetService(IOptionsService);
		MAssert(ms_pmOptionsService != NULL, "Options service not found!");
	}
	return ms_pmOptionsService;
}
//---------------------------------------------------------------------------
ISelectionService* MViewport::get_SelectionService()
{
	if (ms_pmSelectionService == NULL)
	{
		ms_pmSelectionService = MGetService(ISelectionService);
		MAssert(ms_pmSelectionService != NULL, "Selection service not found!");
	}
	return ms_pmSelectionService;
}
//---------------------------------------------------------------------------
ICommandService* MViewport::get_CommandService()
{
	if (ms_pmCommandService == NULL)
	{
		ms_pmCommandService = MGetService(ICommandService);
		MAssert(ms_pmCommandService != NULL, "Command service not found.");
	}
	return ms_pmCommandService;
}
//---------------------------------------------------------------------------
