﻿#include "SceneDesignerFrameworkPCH.h"
#include "MTimeStamp.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace System::IO;

//--------------------------------------------------------------------------
MTimeStamp::~MTimeStamp(void)
{
	
}
//--------------------------------------------------------------------------
// 添加一条时间戳记录
void MTimeStamp::AddTimeStamp(System::String* strMsg)
{
#ifdef _DEBUG
	//DateTime dt = System::DateTime::Now;
	//String* strTimeStame = String::Concat(dt.Minute.ToString(), " : ", dt.Second.ToString());
	//strTimeStame = String::Concat(strTimeStame, " : ", dt.Millisecond.ToString());
	//strTimeStame = String::Concat(strTimeStame, " - ", strMsg);
	//strTimeStame = String::Concat(strTimeStame, "\r\n");
	////Console::Write(strTimeStame);
	//m_TimeStampList->Add(strTimeStame);
#endif

}
//--------------------------------------------------------------------------
// 保存到文件
void MTimeStamp::SaveToFile()
{
#ifdef _DEBUG

	try
	{
		String* strFileName = new String("TimeStame.log");
		if (File::Exists(strFileName))
		{
			File::Delete(strFileName);
		}

		Stream* stream = System::IO::File::Open(strFileName, FileMode::CreateNew);
		StreamWriter* writer = new StreamWriter(stream);

		for (int i=0; i<m_TimeStampList->Count; i++)
		{
			writer->Write(m_TimeStampList->Item[i]->ToString());
			writer->Write(new String("------------------------------------------------------------\r\n"));
			//writer->Write(m_TimeStampList->);
		}
		writer->Flush();
		writer->Close();
		//stream->Flush();
		//stream->Close();
		//stream->CanTimeout

	}
	catch (Exception* e)
	{
		System::Windows::Forms::MessageBox::Show(e->ToString());
	}

#endif
}
//--------------------------------------------------------------------------
// 输出到控制台
void MTimeStamp::Output()
{

}
//--------------------------------------------------------------------------
void MTimeStamp::Do_Dispose(bool bDisposing)
{
	m_TimeStampList->Clear();
	m_TimeStampList = NULL;
}
//--------------------------------------------------------------------------
void MTimeStamp::Init()
{
}
//--------------------------------------------------------------------------
void MTimeStamp::Shutdown()
{
	ms_pmThis->Dispose();
	ms_pmThis = NULL;
}
//--------------------------------------------------------------------------
bool MTimeStamp::InstanceIsValid()
{
	return (ms_pmThis != NULL);
}	
//--------------------------------------------------------------------------
MTimeStamp* MTimeStamp::get_Instance()
{
	if (ms_pmThis == NULL)
	{
		ms_pmThis = new MTimeStamp();
	}

	return ms_pmThis;
}
//--------------------------------------------------------------------------
MTimeStamp::MTimeStamp()
{
	m_TimeStampList = new ArrayList();
}
//--------------------------------------------------------------------------