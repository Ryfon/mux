﻿#pragma once

#include "MDisposable.h"
#include "MEntity.h"
#include "MSelectionSet.h"
#include "MRenderingContext.h"
#include "MPoint3.h"
#include "IMessageService.h"
#include "ICommandService.h"

#include <vcclr.h>
#pragma unmanaged
#include <NiScene.h>
#include <NiAVObject.h>
#pragma managed

using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;

//using namespace SceneCore;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{

// 模版预览器。用于把模版模型渲染到 buffer,再贴到预览窗口上。
public __gc class MTemplatePreviewer : public MDisposable
{
public:
	MTemplatePreviewer(void);
	~MTemplatePreviewer(void);
	
	// 设置目标窗口
	void SetTargetWnd(IntPtr hRendererWndPtr);

	// 设置要渲染的物件
	void SetScene(MEntity* pmEntity);

	// 导出预览图 [11/27/2009 hemeng]
	bool ExportModelScreenShot(String* szFilePath,String* szExportPath); 
	
	void SetTextureWidth(int iWidth);
	void SetTextureHeight(int iHeight);
	void SetCameraViewPoint(float fX, float fY, float fZ);
	//  [11/30/2009 hemeng]

	__property System::Drawing::Bitmap* get_PreviewBitmap();

protected:
	// MDisposable members.
	virtual void Do_Dispose(bool bDisposing);

	// Singleton members.
public:
	static void Init();
	static void Shutdown();
	static bool InstanceIsValid();
	__property static MTemplatePreviewer* get_Instance();


private:

	// 渲染目标物件到目标窗口
	void _RenderToTargetWnd();
	void _UpdateCamera(NiPoint3 kPoint, float fRadius);

	static	MTemplatePreviewer*	ms_pmThis = NULL;
	HWND						m_hTargetWnd;	// 目标窗口句柄
	NiAVObject*					m_pkTargetObj;	// 目标物件
	System::Drawing::Bitmap*	m_PreviewBitmap;// 渲染目标物件生成的 bitmap
	NiLight*					m_pkDefLight;
	// 摄像机 [11/30/2009 hemeng]
	NiCamera*					m_pkWorldCamera;
	int		m_iTextureWidth;
	int		m_iTextureHeight;
	float	m_fCamerX;
	float	m_fCamerY;
	float	m_fCamerZ;
};	// End of class

}}}}

