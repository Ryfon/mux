﻿#include "SceneDesignerFrameworkPCH.h"
#include "MTerrainParameter.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
MTerrainParameter::MTerrainParameter(void)
: m_iChunkX(1)
, m_iChunkY(1)
, m_pcMaterialName("")
, m_iMode(0)
, m_iMapNo(0)
{
}
//---------------------------------------------------------------------------
MTerrainParameter::MTerrainParameter(const int iChunkX, const int iChunkY, String* pcMaterialName, const int iMode, int iMapNo)
: m_iChunkX(iChunkX)
, m_iChunkY(iChunkY)
, m_pcMaterialName( pcMaterialName )
, m_iMode(iMode)
, m_iMapNo(iMapNo)
{
}
//---------------------------------------------------------------------------
MTerrainParameter::MTerrainParameter(const MTerrainParameter& kTerrainParameter)
{
	SetData(kTerrainParameter);
}
//---------------------------------------------------------------------------
MTerrainParameter::~MTerrainParameter(void)
{
}
//---------------------------------------------------------------------------
void MTerrainParameter::SetData(const MTerrainParameter& kTerrainParameter)
{
	m_iChunkX = kTerrainParameter.m_iChunkX;
	m_iChunkY = kTerrainParameter.m_iChunkY;
	m_pcMaterialName = kTerrainParameter.m_pcMaterialName;
	m_iMode = kTerrainParameter.m_iMode;
	m_iMapNo = kTerrainParameter.m_iMapNo;
}
//---------------------------------------------------------------------------
int MTerrainParameter::get_ChunkX()
{
	return m_iChunkX;
}
//---------------------------------------------------------------------------
int MTerrainParameter::get_ChunkY()
{
	return m_iChunkY;
}
//---------------------------------------------------------------------------
String* MTerrainParameter::get_MaterialName()
{
	return m_pcMaterialName;
}
//---------------------------------------------------------------------------
int MTerrainParameter::get_Mode()
{
	return m_iMode;
}
//---------------------------------------------------------------------------
int MTerrainParameter::get_MapNo()
{
	return m_iMapNo;
}
//---------------------------------------------------------------------------
void MTerrainParameter::SetChunkX(const int iChunkX)
{
	m_iChunkX = iChunkX;
}
//---------------------------------------------------------------------------
void MTerrainParameter::SetChunkY(const int iChunkY)
{
	m_iChunkY = iChunkY;
}
//---------------------------------------------------------------------------
void MTerrainParameter::SetMaterialName( String* pcMaterialName)
{
	m_pcMaterialName = pcMaterialName;
}
//---------------------------------------------------------------------------
void MTerrainParameter::SetMode(const int iMode)
{
	m_iMode = iMode;
}
//---------------------------------------------------------------------------
void MTerrainParameter::SetMapNo(const int iMapNo)
{
	m_iMapNo = iMapNo;
}
//---------------------------------------------------------------------------
bool MTerrainParameter::Equals(Object* pmObj)
{
	MTerrainParameter* pmTerrain2 = dynamic_cast<MTerrainParameter*>(pmObj);
	if (pmTerrain2 == NULL)		return false;

	const char* pszFile = MStringToCharPointer( m_pcMaterialName );

	return ( m_iChunkX == pmTerrain2->get_ChunkX() 
			&& m_iChunkY == pmTerrain2->get_ChunkY()
			&& pszFile == pmTerrain2->get_MaterialName()
			&& m_iMode == pmTerrain2->get_Mode() 
			&& m_iMapNo == pmTerrain2->get_MapNo());
}