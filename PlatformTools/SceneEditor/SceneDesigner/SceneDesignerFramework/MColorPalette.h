﻿#pragma once

#include "MDisposable.h"

#include <vcclr.h>

using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{
// 自定义调色板
public __gc class MColorPalette : public MDisposable
{
public:

	~MColorPalette(void);

	// 保存/载入模板文件
	void Save(String* strFileName);
	void Load(String* strFileName);

	// 添加删除颜色
	void Add(System::Drawing::Color color);
	void Remove(int iIdx);

	//------------------------------------------------------
protected:
	// MDisposable members.
	virtual void Do_Dispose(bool bDisposing);

	// Singleton members.
public:
	static void Init();
	static void Shutdown();
	static bool InstanceIsValid();
	__property static MColorPalette* get_Instance();

private:
	MColorPalette(void);

	static MColorPalette*	ms_pmThis = NULL;
	ArrayList*				m_pColorList;		// 颜色列表
};

}}}}