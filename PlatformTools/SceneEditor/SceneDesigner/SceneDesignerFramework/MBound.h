﻿#pragma once
#include <NiBound.h>
#include "MPoint3.h"

// 托管包围球类型，对应 NiBound
namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		[SerializableAttribute]
		public __gc class MBound
		{
		public:
			MBound();
			MBound(float fX, float fY, float fZ, float fR);
			MBound(MPoint3* pmCenter, float fR);
			MBound(const NiBound& kBound);

			void SetData(const NiBound& kBound);
			void ToNiBound(NiBound& kBound);

			__property float get_R();
			__property void set_R(float fR);

			__property MPoint3* get_Center();
			__property void set_Center(MPoint3* pmCenter);

			//System::Object overrides
			virtual bool Equals(Object* pmObj);

		private:
			MPoint3* m_pmCenter;	// 坐标
			float m_fR;	// 半径
		};
	}}}}
