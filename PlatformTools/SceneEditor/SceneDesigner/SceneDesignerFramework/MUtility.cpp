﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MUtility.h"
#include <NiVersion.h>
#include "ServiceProvider.h"
#include <NiFactories.h>
#include <d3dx9.h>

using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;

//---------------------------------------------------------------------------
IMessageService* MUtility::get_MessageService()
{
    if (ms_pmMessageService == NULL)
    {
        ms_pmMessageService = MGetService(IMessageService);
        MAssert(ms_pmMessageService != NULL, "Message service not found!");
    }
    return ms_pmMessageService;
}
//---------------------------------------------------------------------------
String* MUtility::GetStreamingFormatDescriptions()[]
{
	NiTObjectArray< NiFixedString > kKeys;
    NiFactories::GetStreamingFactory()->GetKeys(kKeys);
    unsigned int uiKeySize = kKeys.GetSize();

    String* astrKeys[] = new String*[uiKeySize];

    for (unsigned int ui = 0; ui < uiKeySize; ui++)
    {
        const char* pcKey = kKeys.GetAt(ui);
        NiEntityStreaming* pkStreamingHandler = 
            NiFactories::GetStreamingFactory()->GetPersistent(pcKey);

        astrKeys[ui] = pkStreamingHandler->GetFileDescription();
    }

    return astrKeys;
}
//---------------------------------------------------------------------------
String* MUtility::GetStreamingFormatExtensions()[]
{
	NiTObjectArray< NiFixedString > kKeys;
    NiFactories::GetStreamingFactory()->GetKeys(kKeys);
    unsigned int uiKeySize = kKeys.GetSize();

    String* astrKeys[] = new String*[uiKeySize];

    for(unsigned int ui = 0; ui < uiKeySize; ui++)
    {
        astrKeys[ui] = kKeys.GetAt(ui);
    }

    return astrKeys;
}
//---------------------------------------------------------------------------
float MUtility::GetCurrentTimeInSec()
{
    return NiGetCurrentTimeInSec();
}
//---------------------------------------------------------------------------
String* MUtility::GetGamebryoVersion()
{
    return GAMEBRYO_SDK_VERSION_STRING;
}
//---------------------------------------------------------------------------
String* MUtility::GetGamebryoBuildDate()
{
    return GAMEBRYO_BUILD_DATE_STRING;
}
//---------------------------------------------------------------------------
int MUtility::GetVersionFromString(String* strVersion)
{
    const char* pcVersion = MStringToCharPointer(strVersion);
    int iVersion = NiStream::GetVersionFromString(pcVersion);
    MFreeCharPointer(pcVersion);

    return iVersion;
}
//---------------------------------------------------------------------------
void MUtility::SetImageSubFolder(String* strPath)
{
    const char* pcPath = MStringToCharPointer(strPath);
    NiDevImageConverter::SetPlatformSpecificSubdirectory(pcPath);
    MFreeCharPointer(pcPath);
}
//---------------------------------------------------------------------------
float MUtility::RadiansToDegrees(float fRadians)
{
    return (fRadians * 180.0f) / NI_PI;
}
//---------------------------------------------------------------------------
float MUtility::DegreesToRadians(float fDegrees)
{
    return (fDegrees * NI_PI) / 180.0f;
}
//---------------------------------------------------------------------------
unsigned char MUtility::FloatToRGB(float fColor)
{
    return (unsigned char) (fColor * 255.0f);
}
//---------------------------------------------------------------------------
float MUtility::RGBToFloat(unsigned char ucColor)
{
    return ((float) ucColor) / 255.0f;
}
//---------------------------------------------------------------------------
void MUtility::AddErrorInterfaceMessages(MessageChannelType eChannel,
    NiEntityErrorInterface* pkErrors)
{
    unsigned int uiErrorCount = pkErrors->GetErrorCount();
    for (unsigned int ui = 0; ui < uiErrorCount; ui++)
    {
        MessageService->AddMessage(eChannel, new Message(String::Format(
            "{0} {{{1}}} [{2}]",
            new String(pkErrors->GetErrorMessage(ui)),
            new String(pkErrors->GetEntityName(ui)),
            new String(pkErrors->GetPropertyName(ui))),
            pkErrors->GetErrorDescription(ui), NULL));
    }
}
//---------------------------------------------------------------------------
Guid MUtility::IDToGuid(const NiUniqueID& kID)
{
    int iSize = sizeof(kID.m_aucValue);
    System::Byte amBytes[] = new Byte[iSize];
    for (int i = 0; i < iSize; i++)
    {
        amBytes[i] = kID.m_aucValue[i];
    }
    return Guid(amBytes);
}
//---------------------------------------------------------------------------
void MUtility::GuidToID(Guid mGuid, NiUniqueID& kUniqueID)
{
    Byte pmGuidBytes[] = mGuid.ToByteArray();
    int iSize = pmGuidBytes->Count;
    for (int i = 0; i<iSize; i++)
    {
        kUniqueID.m_aucValue[i] = pmGuidBytes[i];
    }
}
//---------------------------------------------------------------------------
NiAVObject* MUtility::_RecursiveNode(NiAVObject* pkAVObj, const char* szContentName)
{
	if (pkAVObj == NULL || szContentName == NULL)
	{
		return NULL;
	}

	if (NiIsKindOf(NiNode, pkAVObj))
	{			
		NiNode* pkNode = (NiNode*)pkAVObj;
		NiAVObject* pkResult = NULL;
		for (unsigned int i=0; i<pkNode->GetChildCount(); i++)
		{
			if (pkResult != NULL)
			{
				return pkResult;
			}
			NiAVObject* pkChild = pkNode->GetAt(i);
			const char* pName = pkNode->GetName();
			if (pName == NULL)
			{
				continue;
			}
			if (strcmp(szContentName,pName) == 0)
			{
				return (NiAVObject*)pkNode;
			}
			else
			{
				pkResult = _RecursiveNode(pkChild,szContentName);
			}
		}
	}
	return NULL;
}

//---------------------------------------------------------------------------
NiAVObject* MUtility::ExistsContent(NiAVObject *pkAVObj, const char *szContentName)
{
	if (pkAVObj == NULL)
	{
		return 0;
	}


	NiAVObject* pkContentObj = pkAVObj->GetObjectByName(szContentName);
	if (pkContentObj)
	{
		return pkContentObj;
	}
	// 遍历node
	else 
	{			
		return _RecursiveNode(pkAVObj,szContentName);
	}

	return NULL;
}
//------------------------------------------------------------------------------
bool MUtility::Texture2DDS(String* strFileName, UINT uiFmt)
{
	if (strFileName == NULL || !File::Exists(strFileName)) return false;

	NiDX9Renderer* pkDX9Renderer 
		= NiDynamicCast(NiDX9Renderer, NiRenderer::GetRenderer());
	if (pkDX9Renderer == NULL) return false;

	LPDIRECT3DDEVICE9 pDevice = pkDX9Renderer->GetD3DDevice();
	if (pDevice == NULL) return false;

	bool bResult = true;
	LPDIRECT3DTEXTURE9 pSrcTex = NULL;
	LPDIRECT3DSURFACE9 pSrcSurface = NULL;
	LPDIRECT3DTEXTURE9 pDestTex = NULL;
	LPDIRECT3DSURFACE9 pDestSurface = NULL;
	const char* pszTagFileName = NULL;
	// 载入原纹理
	const char* pszFileName = MStringToCharPointer(strFileName);

	if (FAILED(D3DXCreateTextureFromFile(pDevice, pszFileName, &pSrcTex)))
	{
		bResult = false;
		goto theend;
	}

	D3DSURFACE_DESC desc;
	pSrcTex->GetLevelDesc(0, &desc);
	UINT uiWidth = desc.Width;
	UINT uiHeight = desc.Height;
	
	D3DFORMAT dxtFormat = D3DFMT_A8R8G8B8;

	if (uiFmt == 1) {dxtFormat = D3DFMT_DXT1;}
	else if (uiFmt == 3) {dxtFormat = D3DFMT_DXT3;}
	else {dxtFormat = D3DFMT_DXT5; }

	// 创建目标纹理
	if (FAILED(D3DXCreateTexture(pDevice, uiWidth, uiHeight, 1, 0, dxtFormat, D3DPOOL_MANAGED, &pDestTex)))// D3DPOOL_DEFAULT
	{
		bResult = false;
		goto theend;
	}

	pSrcTex->GetSurfaceLevel(0, &pSrcSurface);

	//pDestTex->LockRect(0, NULL, NULL, 0);

	pDestTex->GetSurfaceLevel(0, &pDestSurface);

	if (FAILED(D3DXLoadSurfaceFromSurface(pDestSurface, 0, 0, pSrcSurface, 0, 0, D3DX_DEFAULT, 0)))
	{
		bResult = false;
		goto theend;
	}

	SAFE_RELEASE(pSrcSurface);
	SAFE_RELEASE(pDestSurface);

	// 目标文件名
	int iExtLen = Path::GetExtension(strFileName)->Length;	// 扩展名长度
	String* strNameWithoutExt =  strFileName->Substring(0, strFileName->Length - iExtLen);
	String* strNewName = String::Concat(strNameWithoutExt, ".dds");
	pszTagFileName = MStringToCharPointer(strNewName);

	if (FAILED(D3DXSaveTextureToFile(pszTagFileName, D3DXIFF_DDS, pDestTex, 0)))
	{
		bResult = false;
		goto theend;
	}

theend:
	MFreeCharPointer(pszFileName);
	if (pszTagFileName != NULL)
	{
		MFreeCharPointer(pszTagFileName);
	}

	SAFE_RELEASE(pSrcSurface);
	SAFE_RELEASE(pSrcTex);
	SAFE_RELEASE(pDestSurface);
	SAFE_RELEASE(pDestTex);

	return bResult;
}