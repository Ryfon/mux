﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "SceneDesignerFrameworkPCH.h"

#include "MEntityFactory.h"
#include "MComponentFactory.h"
#include "Utility.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
void MEntityFactory::Init()
{
    if (ms_pmThis == NULL)
    {
        ms_pmThis = new MEntityFactory();
    }
}
//---------------------------------------------------------------------------
void MEntityFactory::Shutdown()
{
    if (ms_pmThis != NULL)
    {
        ms_pmThis->Dispose();
        ms_pmThis = NULL;
    }
}
//---------------------------------------------------------------------------
bool MEntityFactory::InstanceIsValid()
{
    return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
MEntityFactory* MEntityFactory::get_Instance()
{
    return ms_pmThis;
}
//---------------------------------------------------------------------------
MEntityFactory::MEntityFactory()
{
    m_pmUnmanagedToManaged = new Hashtable();
	m_iCurrMaxEntityID = 0;
}
//---------------------------------------------------------------------------
void MEntityFactory::Do_Dispose(bool bDisposing)
{
    if (bDisposing)
    {
        Clear();
		m_iCurrMaxEntityID = 0;
    }
}
//---------------------------------------------------------------------------
MEntity* MEntityFactory::Get(NiEntityInterface* pkUnmanaged)
{
    MVerifyValidInstance;

    if (pkUnmanaged == NULL)
    {
        return NULL;
    }

    Object* pmKey = __box((unsigned int) pkUnmanaged);

    MEntity* pmManaged = dynamic_cast<MEntity*>(
        m_pmUnmanagedToManaged->Item[pmKey]);
    if (pmManaged == NULL)
    {
		// 添加一个新的 entity
        pmManaged = new MEntity(pkUnmanaged);
        m_pmUnmanagedToManaged->Item[pmKey] = pmManaged;

		int iEntityID = 0;

		if (pkUnmanaged->GetPropertyData("EntityID", iEntityID))
		{
			//pmManaged->MakeEntityUnique();
			//pmManaged->MasterEntity = NULL;
			//if (iEntityID <= 0)
			{
				// 每次添加新物件都重新设置 entity id
				//pkUnmanaged->SetPropertyData("EntityID", ++m_iCurrMaxEntityID);
				pmManaged->SetPropertyData("EntityID", __box(++m_iCurrMaxEntityID), false);
				
				/*
				*	解决修改过 component property 后的兼容性问题
				*/
				// entityID 的 UID
				NiUniqueID kTemplateID(0xD8, 0xB0, 0xD0, 0x0B, 0x89, 0x06, 0x44, 0xfb, 0x8D, 0x6A, 0xDD, 0xC3, 0xD3, 0x91, 0xF1, 0xC9);
				//
				NiEntityComponentInterface* pkComp = pkUnmanaged->GetComponentByTemplateID(kTemplateID);//GetComponentFromEntityByName(pkUnmanaged, "EntityID");
				if (pkComp!=NULL)
				{
					bool bTransparent = false;
					if (!pkComp->GetPropertyData("Transparent", bTransparent, 0))
					{
						pkComp->AddProperty("Transparent", "Transparent", NiEntityPropertyInterface::PT_BOOL , NiEntityPropertyInterface::PT_BOOL, "是否透明");
						pkUnmanaged->SetPropertyData("Transparent", true);
					}
				}

				NiUniqueID kTemplateIDMusic(0x77, 0x9a, 0x73, 0x5c, 0x7e, 0x6d, 0x40, 0x87, 0x97, 0x08, 0xe9, 0x6a, 0x6c, 0xcf, 0x23, 0x74);
				// 
				pkComp = pkUnmanaged->GetComponentByTemplateID(kTemplateIDMusic); // GetComponentFromEntityByName(pkUnmanaged, "BGMusic");
				if (pkComp != NULL)
				{
					bool bLoop = false;
					if (!pkComp->GetPropertyData("Loop", bLoop, 0))
					{
						pkComp->AddProperty("Loop", "Loop", NiEntityPropertyInterface::PT_BOOL , NiEntityPropertyInterface::PT_BOOL, "是否循环播放");
						pkUnmanaged->SetPropertyData("Loop", true);
					}

					float fVolume = 0.8f;
					if (!pkComp->GetPropertyData("Volume", fVolume, 0))
					{
						pkComp->AddProperty("Volume", "Volume", "Float", "Float", "音量 0.0~1.0");
						pkUnmanaged->SetPropertyData("Volume", 0.8f);
					}
				}
			}

			//bool bUnique = true;
			//pkUnmanaged->MakePropertyUnique("Transparent", bUnique);
			//bUnique = true;
			//pkUnmanaged->MakePropertyUnique("Loop", bUnique);
			//bUnique = true;
			//pkUnmanaged->MakePropertyUnique("Volume", bUnique);
			//bool bTransparent = false;
			//if (!pkUnmanaged->GetPropertyData("Transparent", bTransparent))
			//{
			//	MComponent* pmComponent = MComponentFactory::Instance->Get("Transparent");
			//	if (pmManaged->CanAddComponent(pmComponent))
			//	{
			//		pmManaged->AddComponent(pmComponent->Clone(false), true, false);
			//		pmManaged->SetPropertyData("Transparent", __box(true), 0, false);
			//	}
			//}
		}
    }

    return pmManaged;
}
//---------------------------------------------------------------------------
void MEntityFactory::Remove(NiEntityInterface* pkUnmanaged)
{
    MVerifyValidInstance;

    Object* pmKey = __box((unsigned int) pkUnmanaged);

    if (m_pmUnmanagedToManaged->Contains(pmKey))
    {
        IDisposable* pmDisposable = dynamic_cast<IDisposable*>(
            m_pmUnmanagedToManaged->Item[pmKey]);
        if (pmDisposable != NULL)
        {
            pmDisposable->Dispose();
        }
        m_pmUnmanagedToManaged->Remove(pmKey);
    }
}
//---------------------------------------------------------------------------
void MEntityFactory::Remove(MEntity* pmManaged)
{
    MVerifyValidInstance;

    Remove(pmManaged->GetNiEntityInterface());
}
//---------------------------------------------------------------------------
void MEntityFactory::Clear()
{
    MVerifyValidInstance;

    IDictionaryEnumerator* pmEnumerator =
        m_pmUnmanagedToManaged->GetEnumerator();
    while (pmEnumerator->MoveNext())
    {
        IDisposable* pmDisposable = dynamic_cast<IDisposable*>(
            pmEnumerator->Value);
        if (pmDisposable != NULL)
        {
            pmDisposable->Dispose();
        }
    }
    m_pmUnmanagedToManaged->Clear();
}
//---------------------------------------------------------------------------
int MEntityFactory::ClearAllDisposedMEntity()
{
	MVerifyValidInstance;

	int iCount = 0;
	IDictionaryEnumerator* pmEnumerator =
		m_pmUnmanagedToManaged->GetEnumerator();
	while (pmEnumerator->MoveNext())
	{
		MEntity* pmEntity = dynamic_cast<MEntity*>(
			pmEnumerator->Value);
		if (pmEntity->HasBeenDisposed())
		{
			m_pmUnmanagedToManaged->Remove(pmEnumerator->Key);
			iCount++;
		}
	}
	return iCount;
}
//---------------------------------------------------------------------------
MEntity* MEntityFactory::Get(String* strName)
{
	IDictionaryEnumerator* pmEnumerator =
		m_pmUnmanagedToManaged->GetEnumerator();
	while (pmEnumerator->MoveNext())
	{
		MEntity* pmEntity = dynamic_cast<MEntity*>(
			pmEnumerator->Value);
		if (pmEntity->Name->CompareTo(strName) == 0)
		{
			return pmEntity;
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------
int MEntityFactory::GetCurrMaxEntityID()
{
	return m_iCurrMaxEntityID;
}
//---------------------------------------------------------------------------
