﻿#include "SceneDesignerFrameworkPCH.h"

#include "MSpecularProperty.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;

//---------------------------------------------------------------------------
MSpecularProperty::MSpecularProperty(NiSpecularProperty* pkProperty, NiAVObject* pkTagAVObj)
: MNiProperty(pkProperty, pkTagAVObj)
{
}
//---------------------------------------------------------------------------
//System::Object overrides
bool MSpecularProperty::Equals(Object* pmObj)
{
	// 不需要比较
	return false;
}
//---------------------------------------------------------------------------
bool MSpecularProperty::get_Specular()
{
	if (m_pkTagProperty)
	{
		return ((NiSpecularProperty*)m_pkTagProperty)->GetSpecular();
	}
}
//---------------------------------------------------------------------------
void MSpecularProperty::set_Specular(bool bSpe)
{
	if (m_pkTagProperty)
	{
		((NiSpecularProperty*)m_pkTagProperty)->SetSpecular(bSpe);
	}
}
