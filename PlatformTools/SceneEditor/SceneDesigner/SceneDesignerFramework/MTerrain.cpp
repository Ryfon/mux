﻿#include "SceneDesignerFrameworkPCH.h"
#include "MTerrain.h"
#include "MFramework.h"
#include "MSceneFactory.h"
#include "IEntityPathService.h"
#include "TerrainGridBaseRegionManager.h"
#include "AdvanceWaterComponent.h"
#include "MGlobalSetting.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
using namespace SceneCore;

MTerrain::MTerrain(void)
{
	//m_pkPreviewScene = NiNew NiScene("PreviewScene");
	m_pPreviewEntityList = new ArrayList();
	m_testWater = NULL;
	m_pSprayGenerator = NULL;
	m_pSnapTool = CSnapTool::Instance();
	m_bDynamicShadow = true;
}
//---------------------------------------------------------------------------
MTerrain::~MTerrain(void)
{
	SAFE_DELETE(m_pSprayGenerator);
	if (m_pSnapTool)
	{
		delete m_pSnapTool;
		m_pSnapTool = NULL;
	}
}

//---------------------------------------------------------------------------
void MTerrain::Init()
{
	if (ms_pmThis == NULL)
	{
		ms_pmThis = new MTerrain();
	}
}
//---------------------------------------------------------------------------
void MTerrain::Shutdown()
{
	if (ms_pmThis != NULL)
	{
		ms_pmThis->Dispose();
		ms_pmThis = NULL;
	}
}
//---------------------------------------------------------------------------
void MTerrain::Update(float fTime)
{
	if (m_testWater != NULL)
	{
		m_testWater->Update(fTime);
	}

	if (m_pSprayGenerator!=NULL && m_pSprayGenerator->GetSprayGeometry()!=NULL)
	{
		m_pSprayGenerator->GetSprayGeometry()->Update(fTime);
	}
}
//---------------------------------------------------------------------------
String* MTerrain::GetGridBaseRegionList()[]
{
	const vector<string>* pNameList = CTerrainGridBaseRegionManager::GetInstance()->GetRegionNameList();
	int iSize = pNameList->size();
	String* strRegionList[] = new String*[iSize];

	for (int i=0; i<iSize; i++)
	{
		strRegionList[i] = new String((*pNameList)[i].c_str());
	}
	return strRegionList;
}
//---------------------------------------------------------------------------
// 判断是否存在该名称的区域
bool MTerrain::HasGridBaseRegion(String* strName)
{
	const char* pszAreaName = MStringToCharPointer(strName);
	bool bResult = false;
	if (NULL != CTerrainGridBaseRegionManager::GetInstance()->GetGridBaseRegion(pszAreaName))
	{
		bResult = true;
	}
	else
	{
		bResult = false;
	}
	MFreeCharPointer(pszAreaName);

	return bResult;
}
//---------------------------------------------------------------------------
// 添加一个 Grid Base Region
bool MTerrain::AddGridBaseRegion(String* strName)
{
	const char* pszName = MStringToCharPointer(strName);
	bool bResult = CTerrainGridBaseRegionManager::GetInstance()->AddGridBaseRegion(pszName);
	MFreeCharPointer(pszName);
	return bResult;
}
//---------------------------------------------------------------------------
void MTerrain::SetOnEditGridBaseRegion(String* strName)
{
	if (strName == NULL)
	{
		return;
	}

	const char* pszName = MStringToCharPointer(strName);
	CTerrainGridBaseRegionManager::GetInstance()->SetOnEditRegion(pszName);
	MFreeCharPointer(pszName);
}
//---------------------------------------------------------------------------
String* MTerrain::GetOnEditGridBaseRegion()
{
	CTerrainGridBaseRegion* pOnEditRegion = CTerrainGridBaseRegionManager::GetInstance()->GetOnEditRegion();
	String* strName = NULL;
	if (pOnEditRegion != NULL)
	{
		strName = new String(pOnEditRegion->GetName());

	}
	return strName;
}
//---------------------------------------------------------------------------
int MTerrain::GetOnEditRegionGridCount()
{
	CTerrainGridBaseRegion* pOnEditRegion = CTerrainGridBaseRegionManager::GetInstance()->GetOnEditRegion();
	int iNumGrids = 0;
	if (pOnEditRegion != NULL)
	{
		iNumGrids = pOnEditRegion->GetGridSet().size();
	}
	return iNumGrids;
}
//---------------------------------------------------------------------------
// 按名称删除一个区域
void MTerrain::RemoveGridBaseRegionRegion(String* strName)
{
	if (strName == NULL)
	{
		return;
	}

	const char* pszName = MStringToCharPointer(strName);
	CTerrainGridBaseRegionManager::GetInstance()->RemoveRegion(pszName);
	MFreeCharPointer(pszName);
}
//---------------------------------------------------------------------------
void MTerrain::PreviewGridBaseRegionGenerateModel(ArrayList* eneitiyList, ArrayList* rateList, 
				float fRandRotaBegin, float fRandRotaEnd, float fRandScaleBegin, float fRandScaleEnd)
{
	if (get_Terrain() == NULL)
	{
		return;
	}

	// 清空预览场景内所有物件
	m_pPreviewEntityList->Clear();

	for (int i=0; i<eneitiyList->Count; i++)
	{
		String* strEntity = dynamic_cast<String*>((eneitiyList->Item[i]));
		String* strRate = dynamic_cast<String*>((rateList->Item[i]));
		float fRate = 0.0f;
		try
		{
			fRate = Single::Parse(strRate);
		}
		catch(Exception* )
		{
			return;
		}


		// 解析 palette name 和 template name
		int iStart = strEntity->IndexOf("[");
		int iEnd = strEntity->IndexOf("]");
		String* strPalette = strEntity->Substring(iStart+1, iEnd-1);
		String* strTemplate = strEntity->Substring(iEnd+1);
		// 获取 Entity 所属 Palette
		MPalette* pmPalette = MPaletteManager::Instance->GetPaletteByName(strPalette);

		CTerrainGridBaseRegion* pRegion = CTerrainGridBaseRegionManager::GetInstance()->GetOnEditRegion();
		if (pRegion == NULL)
		{
			return;
		}

		if (pmPalette != NULL)
		{
			MEntity* pmEntity = pmPalette->GetEntityByName(strEntity);
			if (pmEntity != NULL)
			{
				_GrowEntityInRange(pRegion, pmEntity, fRate, fRandRotaBegin, fRandRotaEnd, fRandScaleBegin, fRandScaleEnd);
			}
		}

	}
}
//---------------------------------------------------------------------------
// 在一个区域内按某几率生长 entity, 
void MTerrain::_GrowEntityInRange(CTerrainGridBaseRegion* pRegion, MEntity* pmTemplate, float fRate, 
								  float fRandRotaBegin, float fRandRotaEnd, float fRandScaleBegin, float fRandScaleEnd)
{
	CTerrain* pTerrain = get_Terrain();
	if (NULL == pTerrain)
	{
		return;
	}

	System::Random* ra = new System::Random();
	//MScene* pmPreviewScene = MSceneFactory::Instance->Get(m_pkPreviewScene);

	IEntityPathService* psEntityPathService = MGetService(IEntityPathService);
	MAssert(psEntityPathService != NULL, "Entity Path service not found.");
		

	int iNumGridInRow = pTerrain->GetChunkNumX() * GRIDINCHUNK;	// 每行 grid 数

	// 遍历 区域中每一个 Grid, 随机在 grid 上生长 pmEntity
	std::set<int> gridSet = pRegion->GetGridSet();
	std::set<int>::iterator iter = gridSet.begin();
	
	while (iter != gridSet.end())
	{
		if (((float)(ra->Next(10000)))/100.0f < fRate)
		{
			int iGridID = *iter;
			int iX = iGridID % iNumGridInRow;
			int iY = iGridID / iNumGridInRow;

			String* strCloneName = MFramework::Instance->Scene->GetUniqueEntityName(
				String::Concat(psEntityPathService->GetSimpleName(pmTemplate->Name),
				" 01"));
			MEntity* pmNewEntity = pmTemplate->Clone(strCloneName, true);	// 继承 Clone
			pmNewEntity->Update(MFramework::Instance->TimeManager->CurrentTime,
				MFramework::Instance->ExternalAssetManager);

			Guid oldID = pmTemplate->TemplateID;
			Guid newID = pmNewEntity->TemplateID;
			MAssert(oldID == newID);
			//pmPreviewScene->AddEntity(pmNewEntity, true);
			m_pPreviewEntityList->Add(pmNewEntity);
			float fHeight = (float)pTerrain->GetHeight(NiPoint2((float)iX, (float)iY));
			pmNewEntity->GetNiEntityInterface()->SetPropertyData("Translation", 
				NiPoint3((float)iX+ra->NextDouble()-0.5f, (float)iY+ra->NextDouble()-0.5f, fHeight));
			float fInte = ra->Next(10000)*0.0001f;
			float fRotation = Lerp(fRandRotaBegin, fRandRotaEnd, fInte);
			fInte = ra->Next(10000)*0.0001f;
			float fScale = Lerp(fRandScaleBegin, fRandScaleEnd, fInte);

			NiMatrix3 matRotation;
			matRotation.MakeZRotation(fRotation);
			pmNewEntity->GetNiEntityInterface()->SetPropertyData("Rotation", matRotation);
			pmNewEntity->GetNiEntityInterface()->SetPropertyData("Scale", fScale);

			pmNewEntity->Update(MFramework::Instance->TimeManager->CurrentTime,
				MFramework::Instance->ExternalAssetManager);

		}
		iter++;
	}
}
//---------------------------------------------------------------------------
void MTerrain::ApplyGridBaseRegionGeneratedModel()
{
	//MScene* pmPreviewScene = MSceneFactory::Instance->Get(m_pkPreviewScene);

	IEntityPathService* psEntityPathService = MGetService(IEntityPathService);
	MAssert(psEntityPathService != NULL, "Entity Path service not found.");

	MEntity* amEntities[] = GetPreviewSceneEntities();
	for (int i=0; i<amEntities->Count; i++)
	{
		MEntity* pmEntity = amEntities[i];
		pmEntity->Name = MFramework::Instance->Scene->GetUniqueEntityName(pmEntity->Name);
		pmEntity->Update(MFramework::Instance->TimeManager->CurrentTime,
			MFramework::Instance->ExternalAssetManager);
		MFramework::Instance->Scene->AddEntity(pmEntity, false);

	}

	m_pPreviewEntityList->Clear();

}
//---------------------------------------------------------------------------
MEntity* MTerrain::GetPreviewSceneEntities()[]
{
	MVerifyValidInstance;

	return dynamic_cast<MEntity*[]>(m_pPreviewEntityList->ToArray(__typeof(MEntity)));
}
//---------------------------------------------------------------------------
// 获取某地形位置的 chunk ID
int MTerrain::GetTerrainChunkIndex(NiPoint3& kPos)
{
	if (get_Terrain() == NULL)
	{
		return -1;
	}
	else
	{
		return get_Terrain()->GetChunkIndex(kPos);
	}
}
//---------------------------------------------------------------------------
// 设置地形正被编辑 chunk ID
void MTerrain::SetTerrainOnEditChunk(int iChunk)
{
	if (get_Terrain() != NULL)
	{
		get_Terrain()->SetLastChangedChunk(iChunk);
	}
}

//---------------------------------------------------------------------------
bool MTerrain::InstanceIsValid()
{
	return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
MTerrain* MTerrain::get_Instance()
{
	if (ms_pmThis == NULL)
	{
		ms_pmThis = new MTerrain();
	}

	return ms_pmThis;
}
//---------------------------------------------------------------------------
CTerrain* MTerrain::get_Terrain()
{
	return MFramework::Instance->Scene->Terrain;
}
//---------------------------------------------------------------------------
void MTerrain::Do_Dispose(bool bDisposing)
{
	SAFE_DELETE(m_pSprayGenerator);

	if (m_pSnapTool)
	{
		delete m_pSnapTool;
		m_pSnapTool = NULL;
	}
}
//---------------------------------------------------------------------------
void MTerrain::ExportGridProperty()
{
	CTerrain *pTerrain = get_Terrain();

	if (NULL == pTerrain)
	{
		NiMessageBox("请先创建地形","error",0);
		return;
	}

	SaveFileDialog *saveFileDlg = new SaveFileDialog();
	saveFileDlg->Filter = "数据文件(*.dat)|*.dat|All files(*.*)|*.*";
	saveFileDlg->FileName = "GridProperty";
	saveFileDlg->FilterIndex = 1;

	if (saveFileDlg->ShowDialog() == DialogResult::OK)
	{
		const char *strFilePath = MStringToCharPointer( saveFileDlg->FileName );
		pTerrain->ExportGridProPerty(strFilePath);
		MFreeCharPointer( strFilePath );
	}
	else
	{
		return;
	}

	/*String* strDirectory = NULL;
	int iLastSlash = -1;
	if ((iLastSlash=saveFileDlg->FileName->LastIndexOf("/")) > 0)
	{
		strDirectory = saveFileDlg->FileName->Substring(0, iLastSlash);
	}
	else if ((iLastSlash=saveFileDlg->FileName->LastIndexOf("\\")) > 0)
	{
		strDirectory = saveFileDlg->FileName->Substring(0, iLastSlash);
	}

	const char* pcDirectory =  MStringToCharPointer(strDirectory);
	pTerrain->ExportGridProPertyTexture(pcDirectory);

	MFreeCharPointer(pcDirectory);*/
}
//---------------------------------------------------------------------------
void MTerrain::ImportGridProperty()
{
	CTerrain *pTerrain = get_Terrain();

	if (NULL == pTerrain)
	{
		NiMessageBox("请先创建地形","error",0);
		return;
	}

	OpenFileDialog *openFileDlg = new OpenFileDialog();
	openFileDlg->Filter = "数据文件(*.dat)|*.dat|All files(*.*)|*.*";
	openFileDlg->FilterIndex = 1;

	if (openFileDlg->ShowDialog() == DialogResult::OK)
	{
		const char *strFilePath = MStringToCharPointer(openFileDlg->FileName);
		pTerrain->ImportGridProPerty(strFilePath);
		MFreeCharPointer(strFilePath);
	}
	else
	{
		return;
	}

	
}
//---------------------------------------------------------------------------
bool MTerrain::ExportHeightMap(float __gc * iLow, float __gc * iHeigh)
{
	CTerrain *pTerrain = get_Terrain();

	if (NULL == pTerrain)
	{
		NiMessageBox("请先创建地形","error",0);
		return false;
	}

	SaveFileDialog *saveFileDlg = new SaveFileDialog();
	saveFileDlg->Filter = "*.raw|*.raw|All files(*.*)|*.*";
	saveFileDlg->FileName = "TerrainHeightMap";
	saveFileDlg->FilterIndex = 1;

	bool bSuccess = false;

	if (saveFileDlg->ShowDialog() == DialogResult::OK)
	{
		const char *strFilePath = MStringToCharPointer( saveFileDlg->FileName );
		float l, h;
		bSuccess = CTerrainModifier::ExportTerrainHeightMap(pTerrain,strFilePath, l, h);
		// test only [7/15/2009 hemeng]
		*iLow = l;
		*iHeigh = h;		
		MFreeCharPointer( strFilePath );
	}
	else
	{
		bSuccess = false;
	}

	return bSuccess;
}
//---------------------------------------------------------------------------
bool MTerrain::ImportHeightMap(float iLow, float iHeigh)
{
	CTerrain *pTerrain = get_Terrain();

	if (NULL == pTerrain)
	{
		NiMessageBox("请先创建地形","error",0);
		return false;
	}

	OpenFileDialog *openFileDlg = new OpenFileDialog();
	openFileDlg->Filter = "*.raw|*.raw|All files(*.*)|*.*";
	openFileDlg->FilterIndex = 1;

	bool bSuccess = false;

	if (openFileDlg->ShowDialog() == DialogResult::OK)
	{
		const char *strFilePath = MStringToCharPointer(openFileDlg->FileName);
		CTerrainChangeVertexCommand* pCurrentCommand = new CTerrainChangeVertexCommand(pTerrain);
		bSuccess = CTerrainModifier::LoadTerrainHeightMap(pTerrain,strFilePath,pCurrentCommand,iLow,iHeigh);		
		MFreeCharPointer(strFilePath);
		MFramework::Instance->Scene->ExecuteAdjustTerrainHeightCommand(pCurrentCommand);
		if (pCurrentCommand)
		{
			delete pCurrentCommand;
			pCurrentCommand = NULL;
		}
	}
	else
	{
		bSuccess = false;
	}

	return bSuccess;
}

//---------------------------------------------------------------------------
// add [5/25/2009 hemeng]
int MTerrain::GetGridPropertyPercent(int iGridProperty)
{
	CTerrain *pTerrain = get_Terrain();

	if (NULL == pTerrain)
	{
		NiMessageBox("请先创建地形","error",0);
		return 0;
	}

	return pTerrain->GetGridPropertyPercent(iGridProperty);
}

//---------------------------------------------------------------------------
void MTerrain::CreateGridPropertyByMaterial()
{
	CTerrain *pTerrain = get_Terrain();
	if (NULL == pTerrain)
	{
		NiMessageBox("请先创建地形","error",0);
		return;
	}
	CTerrainModifier::SetRegionPropertyByMaterial(pTerrain);
}
//----------------------------------------------------------------------------
void MTerrain::CameraGoToPos( int iPosX, int iPosY )
{
	NiMatrix3 kRotation;
	NiFrustum kFrustum;
	NiPoint3 kDestPoint;
	MEntity* pmCamera = MFramework::Instance->ViewportManager->ActiveViewport->CameraEntity;
	if (pmCamera != NULL)
	{
		NiEntityInterface* pkEntity = pmCamera->GetNiEntityInterface();
		pkEntity->GetPropertyData("Rotation", kRotation);
	}
	kFrustum = MFramework::Instance->ViewportManager->ActiveViewport->GetNiCamera()->GetViewFrustum();


	float fPosZ = get_Terrain()->GetRawHeight( NiPoint2( (float)iPosX, (float)iPosY )) + 15.0f;
	kDestPoint = NiPoint3( (float)iPosX, (float)iPosY, fPosZ );

	NiBound kBoundWithGrid = 
		*(MFramework::Instance->BoundManager->GetToolSceneBound(MFramework::Instance->ViewportManager->ActiveViewport));
	if (kFrustum.m_bOrtho)
	{
		NiPoint3 kLook;
		kRotation.GetCol(0, kLook);
		float fAspect;
		fAspect = kFrustum.m_fRight / kFrustum.m_fTop;

		// add the grid if it is static
		kDestPoint = kDestPoint - (kBoundWithGrid.GetRadius() + 
			(kDestPoint - kBoundWithGrid.GetCenter()).Dot(kLook)) * kLook;
		
	}

	MFramework::Instance->CameraManager->TransitionCamera( MFramework::Instance->ViewportManager->ActiveViewport,
														   &kDestPoint, &kRotation, &kFrustum 
														);
}

//---------------------------------------------------------------------------
void MTerrain::TestAdvanceWater()
{
	NiStream kStream;
	kStream.Load("e:\\test\\plane.nif");
	m_testWater = NiNew CAdvanceWater();
	// 填充初始化参数表
	stAdvWaterInitParam defParam;

	// 设置4个叠加波的参数
	defParam.afAmplitude[0] = 0.04f;
	defParam.afAmplitude[1] = 0.02f;
	defParam.afAmplitude[2] = 0.02f;
	defParam.afAmplitude[3] = 0.04f;
	defParam.afWaveLength[0] = 0.32f;
	defParam.afWaveLength[1] = 0.16f;
	defParam.afWaveLength[2] = 0.25f;
	defParam.afWaveLength[3] = 0.4f;
	defParam.afSpeed[0] = 0.02f;
	defParam.afSpeed[1] = 0.03f;
	defParam.afSpeed[2] = 0.04f;
	defParam.afSpeed[3] = 0.02f;
	defParam.avDirection[0] = NiPoint2(1.0f, 0.2f);
	defParam.avDirection[1] = NiPoint2(1.0f, -0.1f);
	defParam.avDirection[2] = NiPoint2(1.0f, 0.12f);
	defParam.avDirection[3] = NiPoint2(1.0f, -0.25f);

	// 水体参数
	defParam.fAlpha = 0.0f;
	defParam.kReflectColor = NiColor(0.7f, 0.7f, 0.8f);	// 反射色偏蓝
	defParam.kSpecularColor = NiColor(1.0f, 1.0f, 1.0f);	// 高光色

	// 环境参数
	defParam.kLightDir = NiPoint3(0.0f, 1.0f, 2.0f);
	defParam.kLightDir /= defParam.kLightDir.Length();
	defParam.spEnvMap = NiSourceTexture::Create("e:/test/skybox02.dds");
	//defParam.spRefractMap = NULL;

	NiAVObject* pkAVObj = (NiAVObject*)kStream.GetObjectAt(0);
	m_testWater->Init(pkAVObj, defParam);
}
//---------------------------------------------------------------------------
void MTerrain::ExportAdvanceWater(String* strNifFilename)
{
	MEntity* amEntities[] = MFramework::Instance->Scene->GetEntities();
	for (int i=0; i<amEntities->Length; i++)
	{
		MEntity* pmEntity = amEntities[i];
		if (CAdvanceWaterComponent::EntityIsAdvanceWater(pmEntity->GetNiEntityInterface()))
		{
			NiAVObject* pkWaterNode = pmEntity->GetSceneRootPointer(0);
			if ( NiIsKindOf( NiNode, pkWaterNode ))
			{

				NiNode * pNode = (NiNode *)pkWaterNode;
				NiStream kStream;
				//NiNode* pkNode = (NiNode*)pkWaterNode;
				NiGeometry* pkGeom = (NiGeometry*)(pNode->GetAt(0));
				NiTimeController* pController = pkGeom->GetControllers();
				pController->IncRefCount();
				//kStream.InsertObject( pController );
				//kStream.InsertObject( (NiObject *)pkGeom->GetActiveMaterial() );
				pController->SaveBinary( kStream );
				kStream.InsertObject(pNode);

				kStream.Save("e:/test/AdvWater.nif");
			}
		}
	}
}
//---------------------------------------------------------------------------
void MTerrain::GenerateSprayGeometry(float fSeaLevel, float fWidth, float fAlpha, float fT0, float fV0, 
									 float fT1, float fV1, float fT2, float fV2, float fT3, float fV3, String* strTexFilename)
{
	if (get_Terrain() == NULL)
	{
		return;
	}

	SAFE_DELETE(m_pSprayGenerator);

	CTerrainGridBaseRegion* pGridBaseRegion = CTerrainGridBaseRegionManager::GetInstance()->GetOnEditRegion();

	m_pSprayGenerator = new CSprayGenerator();
	const char* pcFilename = MStringToCharPointer(strTexFilename);
	m_pSprayGenerator->Init(get_Terrain(), fSeaLevel, fWidth, fAlpha, fT0, fV0, fT1, fV1, fT2, fV2, fT3, fV3, pcFilename);
		
	MFreeCharPointer(pcFilename);

	if (pGridBaseRegion == NULL)
	{
		m_pSprayGenerator->GenerateSprayGeometry();
	}
	else
	{
		m_pSprayGenerator->GenerateSprayGeometry(pGridBaseRegion->GetGridSet());
	}

}
//---------------------------------------------------------------------------
void MTerrain::ClearSprayPreview()
{
	SAFE_DELETE(m_pSprayGenerator);
}
//---------------------------------------------------------------------------
bool MTerrain::ExportSprayNif(String* strSprayNifFilename)
{

	if (m_pSprayGenerator != NULL && m_pSprayGenerator->GetSprayGeometry()!=NULL)
	{
		NiGeometryPtr pkSprayGeom = m_pSprayGenerator->GetSprayGeometry();
		const char* pcFilename = MStringToCharPointer(strSprayNifFilename);
		NiNodePtr pkRoot = NiNew NiNode();
		pkRoot->AttachChild(pkSprayGeom);
		pkRoot->Update(0.0f);
		NiStream kStream;
		kStream.InsertObject(pkRoot);
		bool bResult = kStream.Save(pcFilename);
		MFreeCharPointer(pcFilename);
		return bResult;
	}
	else
	{
		return false;
	}
}
//---------------------------------------------------------------------------
void MTerrain::RenderSprayImmediate(NiRenderer* pkRenderer)
{
	if (m_pSprayGenerator != NULL && m_pSprayGenerator->GetSprayGeometry()!=NULL)
	{
		m_pSprayGenerator->GetSprayGeometry()->RenderImmediate(pkRenderer);
	}
}
//---------------------------------------------------------------------------
void MTerrain::BuildVisibleSet(NiEntityRenderingContextPtr	pkRenderingCondtex)
{
	if (m_testWater != NULL)
	{
		m_testWater->BuildVisibleSet(pkRenderingCondtex);
	}
}
//---------------------------------------------------------------------------
//void MTerrain::SetTerrainAlpha(float fAlpha)
// modify [11/24/2009 hemeng]
void MTerrain::SetTerrainHidden(bool bHidden)
{
	// 设置地形顶点 alpha 值
	if (Terrain == NULL)
	{
		return;
	}

	CTerrainGridBaseRegion* pGridBaseRegion = CTerrainGridBaseRegionManager::GetInstance()->GetOnEditRegion();
	if (pGridBaseRegion == NULL)
	{
		return;
	}

	set<int>& gridSet = pGridBaseRegion->GetGridSet();

	//CTerrainModifier::SetTerrainVertexAlpha(Terrain, gridSet, fAlpha);
	// modify [11/24/2009 hemeng]
	CTerrainModifier::SetTerrainVertexHidden(Terrain,gridSet, bHidden);
}
//---------------------------------------------------------------------------
void MTerrain::SetTerrainProperty(int iProp)
{
	if (Terrain == NULL)
	{
		return;
	}

	CTerrainGridBaseRegion* pGridBaseRegion = CTerrainGridBaseRegionManager::GetInstance()->GetOnEditRegion();
	if (pGridBaseRegion == NULL)
	{
		return;
	}

	set<int>& gridSet = pGridBaseRegion->GetGridSet();
	CTerrainModifier::SetTerrainSurfaceProperty(Terrain, pGridBaseRegion->GetGridSet(), 1,false );
}

void MTerrain::FillTerrainCollisionHole(int iNumHoleBlock)
{
	if (Terrain == NULL)
	{
		return;
	}

	CTerrain* pTerrain = get_Terrain();
	int iNumGridX = pTerrain->GetChunkNumX()*GRIDINCHUNK;
	int iNumGridY = pTerrain->GetChunkNumY()*GRIDINCHUNK;
	const CTerrain::stGrid* pGrids = pTerrain->GetGrids();

	// 地形上的 grid 数量
	int iNumGrids = iNumGridX*iNumGridY;
	int* pGridMark = new int[iNumGrids];	// 用来标记每个 grid 属性
	ZeroMemory(pGridMark, iNumGrids*sizeof(int));

	// grid 的属性
	// 0 - 未标记
	// 1 - block
	// 2 - 遍历过

	// 遍历所有 grid,如果
	// 1. 不可行走 或 遍历过 skip
	// 2. 可行走, 递归搜索该 grid 所连接的所有 grid 保存到列表.
	// 3. 标记列表中所有 grid 为遍历过,如果列表长度小于 iNumHoleBlock
	//	  标记所有 grid 为不可行走
	for (int i=0; i<iNumGridY; i++)		// 遍历行
	{
		for (int j=0; j<iNumGridX; j++)	// 遍历列
		{
			int iGridIdx = i*iNumGridX + j;
			// terrain property 只用低4位
			if ((pGrids[iGridIdx].iTerrainProperty & 0x0000000F) == 0x00000001)
			{
				// 不可行走.
				pGridMark[iGridIdx] = 1;
				continue;	
			}
			else if (pGridMark[iGridIdx] == 2)
			{
				// 遍历过了
				continue;
			}
			else
			{
				// 遇到一个没有遍历过的可行走 grid
				std::set<int> adjacentResult;
				std::set<int> openList;	// 待搜索的 grid

				openList.insert(iGridIdx);

				while (openList.size() > 0)
				{
					// 设置本 grid 状态
					_GetAdjacentWalkableGrids(openList, pGrids, pGridMark, iNumGridX, iNumGridY, adjacentResult, iNumHoleBlock);
				}

				// 相邻 grid 数量小于临界值
				if (adjacentResult.size() < (unsigned int)iNumHoleBlock)
				{
					// 将这些 grid 设置为碰撞
					CTerrainModifier::SetTerrainSurfaceProperty(Terrain, adjacentResult, 1,false);
				}
			}
		}
	}

	SAFE_DELETE_ARRAY(pGridMark);
}

void MTerrain::_GetAdjacentWalkableGrids(std::set<int>& openList, const CTerrain::stGrid* pGrids, 
						int* pGridMark, const int iNumGridX, const int iNumGridY, std::set<int>& adjacent, const int iNumHoleBlock)
{
	//static int iNumGridChanged = 0;

	// 判断 iX, iY 是否合法
	//if (iX>=0 && iX<iNumGridX && iY>=0 && iY<iNumGridY)

	static std::set<int> nextPassOpenList;	// 下次调用本函数时需搜索的 grid
	nextPassOpenList.clear();

	std::set<int>::iterator iter = openList.begin();
	while (iter != openList.end())
	{
		int iGridIdx = *iter;
		int iX = iGridIdx%iNumGridX;
		int iY = iGridIdx/iNumGridX;

		// 如果 iGridIdx 未被遍历过
		if (pGridMark[iGridIdx]==0)
		{
			//iNumGridChanged++;
			//int k = iNumGridChanged;

			bool bBlock = ((pGrids[iGridIdx].iTerrainProperty&0x0000000F) == 0x00000001);
			if (bBlock)	// 不可行走
			{
				pGridMark[iGridIdx] = 1;	// 设为碰撞
			}
			else	// 该 grid 可行走
			{
				pGridMark[iGridIdx] = 2;	// 设为遍历过

				adjacent.insert(iGridIdx);

				// 递归调用上,下,左,右 4 个 grid
				if (iY+1<iNumGridY && pGridMark[iGridIdx+iNumGridX]==0)
				{
					nextPassOpenList.insert(iGridIdx+iNumGridX);
					//_GetAdjacentWalkableGrids(iX, iY+1, pGrids, pGridMark, iNumGridX, iNumGridY, adjacent, iNumHoleBlock);
				}

				if (iY-1>=0 && pGridMark[iGridIdx-iNumGridX]==0)
				{
					nextPassOpenList.insert(iGridIdx-iNumGridX);
					//_GetAdjacentWalkableGrids(iX, iY-1, pGrids, pGridMark, iNumGridX, iNumGridY, adjacent, iNumHoleBlock);
				}

				if (iX-1>=0 && pGridMark[iGridIdx-1]==0)
				{
					nextPassOpenList.insert(iGridIdx-1);
					//_GetAdjacentWalkableGrids(iX-1, iY, pGrids, pGridMark, iNumGridX, iNumGridY, adjacent, iNumHoleBlock);
				}

				if (iX+1<iNumGridX && pGridMark[iGridIdx+1]==0)
				{
					nextPassOpenList.insert(iGridIdx+1);
					//_GetAdjacentWalkableGrids(iX+1, iY, pGrids, pGridMark, iNumGridX, iNumGridY, adjacent, iNumHoleBlock);
				}
			}

		}	// if (pGridMa

		iter++;
	}

	// 将 nextPassOpenList 拷贝到 openList
	openList.clear();
	iter = nextPassOpenList.begin();
	while (iter != nextPassOpenList.end())
	{
		openList.insert(*iter);
		iter++;
	}


}

//---------------------------------------------------------------------------------------------
bool MTerrain::SetSnapSrcEntity(MEntity* pkEntity)
{
	if (m_pSnapTool == NULL)
	{
		return false;
	}
	
	NiAVObject* pkObj = pkEntity->GetSceneRootPointer(0);
	if (pkObj == NULL)
	{
		return false;
	}

	return m_pSnapTool->SetSrcEntity(pkObj);
}

//-------------------------------------------------------------------------------------------------
bool MTerrain::SetSnapDesEntity(MEntity* pkEntity)
{
	if (m_pSnapTool == NULL)
	{
		return false;
	}

	NiAVObject* pkObj = pkEntity->GetSceneRootPointer(0);
	if (pkObj == NULL)
	{
		return false;
	}

	return m_pSnapTool->SetDesEntity(pkObj);
}

//--------------------------------------------------------------------------------------------------
bool MTerrain::ClearSnapEntity()
{
	if (NULL == m_pSnapTool)
	{
		return false;
	}
	
	return m_pSnapTool->ClearSnapEntity();
}
//------------------------------------------------------------------------------
//void MTerrain::ApplyVertexColorTexture(String* strFileName, float fLum, bool bFixLum, float fIncMultiple)
//{
//	if (strFileName == NULL) return;
//
//	const char* pszFileName = MStringToCharPointer(strFileName);
//	NiSourceTexturePtr pkTexture = NiSourceTexture::Create(pszFileName);
//	CTerrainModifier::ApplyVertexColorTexture(MFramework::Instance->Scene->Terrain, pkTexture, fLum, bFixLum, fIncMultiple);
//
//	MFreeCharPointer(pszFileName);
//}
//------------------------------------------------------------------------------
void MTerrain::SetPreviewTerrainLightMap(bool bPreview)
{
	if (bPreview)
	{
		//MFrame
	}
}
//------------------------------------------------------------------------------
void MTerrain::SaveTerrainToNif(String* strFileName)
{
	if (strFileName == NULL || MFramework::Instance->Scene == NULL) return;

	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;

	if (pTerrain == NULL) return;

	const char* pszFileName = MStringToCharPointer(strFileName);

	pTerrain->SaveTerrainToNif(pszFileName);

	MFreeCharPointer(pszFileName);
}
//------------------------------------------------------------------------------
int MTerrain::GetNumChunkX()
{
	if (MFramework::Instance->Scene == NULL 
		|| MFramework::Instance->Scene->Terrain == NULL)
	{
		return 0;
	}

	return MFramework::Instance->Scene->Terrain->GetChunkNumX();
}
//------------------------------------------------------------------------------
int MTerrain::GetNumChunkY()
{
	if (MFramework::Instance->Scene == NULL 
		|| MFramework::Instance->Scene->Terrain == NULL)
	{
		return 0;
	}

	return MFramework::Instance->Scene->Terrain->GetChunkNumY();
}
//------------------------------------------------------------------------------
/// 将纹理写到地形特定 chunk 的 blend texture 的 alpha 通道
bool MTerrain::ApplyShadowTexture(String* strTexName, int iChunkID)
{
	if (MFramework::Instance->Scene == NULL || strTexName == NULL || iChunkID < 0) 
	{
		return false;
	}

	if (!File::Exists(strTexName)) return false;

	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (pTerrain == NULL) return false;

	const char* pszTexName = MStringToCharPointer(strTexName);
	
	NiTexturePtr spTexture = NiSourceTexture::Create(pszTexName);

	MFreeCharPointer(pszTexName);

	return CTerrainModifier::ApplyShadowTexture(pTerrain, spTexture, iChunkID);
}
//------------------------------------------------------------------------------
/// 将纹理颜色值根据 UV 写入地形特定 chunk 顶点色中
bool MTerrain::ApplyVertexColorTexture(String* strTexName, const int iChunkID, const float fLum, const bool bFixLum, const float fIncMultiple)
{
	if (MFramework::Instance->Scene == NULL || strTexName == NULL || iChunkID < 0) 
	{
		return false;
	}

	if (!File::Exists(strTexName)) return false;

	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (pTerrain == NULL) return false;

	const char* pszTexName = MStringToCharPointer(strTexName);

	NiSourceTexturePtr spTexture = NiSourceTexture::Create(pszTexName);

	MFreeCharPointer(pszTexName);

	return CTerrainModifier::ApplyVertexColorTexture(pTerrain, spTexture, iChunkID, fLum, bFixLum, fIncMultiple);
}
//------------------------------------------------------------------------------
void MTerrain::SetShadowPreview(bool bStaticShadow, bool bDynamicShadow)
{
	m_bStaticShadow = bStaticShadow;
	m_bDynamicShadow = bDynamicShadow;
	NiPoint2 kShadowAttr(bStaticShadow ? 1.0f : 0.0f, bDynamicShadow ? 1.0f : 0.0f);
	NiShaderFactory::UpdateGlobalShaderConstant("g_vShadowSwitch", sizeof( kShadowAttr), &kShadowAttr );
}
	//Invalidator.Instance.Update();
//------------------------------------------------------------------------------
void MTerrain::OnNewScene()
{
	// 将动态阴影设为 true
	m_bDynamicShadow = true;
}
//------------------------------------------------------------------------------
float MTerrain::get_TerrainAmbient()
{
	return MGlobalSetting::Instance->TerrainAmbient;
}
//------------------------------------------------------------------------------
void MTerrain::set_TerrainAmbient(float fAmbient)
{
	MGlobalSetting::Instance->TerrainAmbient = fAmbient;
	if (get_Terrain() != NULL)
	{
		NiShaderFactory::UpdateGlobalShaderConstant("g_fTerrainAmbient", sizeof( float), &fAmbient);
	}

}
//------------------------------------------------------------------------------
bool MTerrain::ExportClusterNif(MEntity* pEntities[])
{
	// validate [6/2/2010 hemeng]
	CTerrain* pTerrain = MFramework::Instance->Scene->Terrain;
	if (pTerrain == NULL) return false;

	SaveFileDialog  *saveFileDlg = new SaveFileDialog();
	saveFileDlg->Filter = "物件信息文件(*.nif)|*.nif|All files(*.*)|*.*";
	saveFileDlg->FilterIndex = 1;

	// 计算出中心位置 [6/21/2010 hemeng]
	// copy from MSelectinService RecaluclateSelectionCenter [6/21/2010 hemeng]
	NiPoint3 kCenterPos = NiPoint3(0,0,0);
	for (int i = 0; i < pEntities->Length; i++)
	{
		MEntity* pmEntity = pEntities[i];
		if (pmEntity->HasProperty("Translation"))
		{
			MPoint3* pmEntityTranslation = dynamic_cast<MPoint3*>(pmEntity->
				GetPropertyData("Translation"));
			NiPoint3 kEntityTranslation;
			pmEntityTranslation->ToNiPoint3(kEntityTranslation);
			kCenterPos += kEntityTranslation;
		}
	}

	if (pEntities->Length > 0)
	{
		kCenterPos = kCenterPos / ((float) pEntities->Length);
	}


	NiNodePtr pkNewRoot = NiNew NiNode();
	pkNewRoot->SetName("Scene Root");
	if ( saveFileDlg->ShowDialog() == DialogResult::OK )
	{
		for (int i = 0; i < pEntities->Length; i++)
		{
			if (MLightManager::EntityIsLight(pEntities[i]))
			{
				continue;
			}
			else if (MCameraManager::EntityIsCamera(pEntities[i]))
			{
				continue;
			}
		
			NiAVObject* pkSceneRoot = pEntities[i]->GetSceneRootPointer(0);

			NiObjectNET::CopyType ct = NiObjectNET::GetDefaultCopyType();
			NiObjectNET ::SetDefaultCopyType(NiObjectNET::COPY_EXACT);
			NiAVObject* pkCloneScene = NiDynamicCast(NiAVObject, pkSceneRoot->Clone());
			NiObjectNET ::SetDefaultCopyType(ct);			

			if (pkCloneScene)
			{
				NiPoint3 kTranslate = pkCloneScene->GetTranslate();
				kTranslate -= kCenterPos;
				
				pkCloneScene->SetTranslate(kTranslate);
				pkCloneScene->Update(0.0f);

				pkNewRoot->AttachChild(pkCloneScene);
				pkNewRoot->Update(0.0f);							
			}
		}

		NiStream kStream;
		kStream.InsertObject(pkNewRoot);
		const char* pszFileName = MStringToCharPointer(saveFileDlg->FileName);
		kStream.Save(pszFileName);
		MFreeCharPointer(pszFileName);
	}

	return true;
}
