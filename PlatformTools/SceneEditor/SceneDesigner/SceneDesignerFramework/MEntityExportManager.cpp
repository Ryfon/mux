﻿#include "SceneDesignerFrameworkPCH.h"
#include "MEntityExportManager.h"
#include "MFramework.h"
#include "MSceneFactory.h"
#include "IEntityPathService.h"
#include "TerrainGridBaseRegionManager.h"
#include "AdvanceWaterComponent.h"

using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
using namespace SceneCore;

//------------------------------------------------------------------------------
MEntityExportManager::MEntityExportManager()
{
	m_arrEntitySettings = new ArrayList();
}
//------------------------------------------------------------------------------
void MEntityExportManager::Do_Dispose(bool bDisposing)
{

}
//------------------------------------------------------------------------------
MEntityExportManager* MEntityExportManager::get_Instance()
{
	if (ms_Instance == NULL)
	{
		ms_Instance = new MEntityExportManager();
	}

	return ms_Instance;
}
//------------------------------------------------------------------------------
// 根据名称获取一个导出配置
MEntityExportSetting* MEntityExportManager::GetEntityExportSetting(String* strName)
{
	if (strName == NULL || strName->Length==0) return NULL;

	// 遍历配置列表
	for (int i=0; i<m_arrEntitySettings->Count; i++)
	{
		MEntityExportSetting* pmSetting = 
			dynamic_cast<MEntityExportSetting*>(m_arrEntitySettings->Item[i]);
		
		if (pmSetting != NULL)
		{
			if (strName->Equals(pmSetting->m_strName))
			{
				return pmSetting;
			}
		}
	}

	return NULL;
}
// 添加一个
void MEntityExportManager::AddEntityExportSetting(MEntityExportSetting* pmSetting)
{
	if (pmSetting == NULL) return;

	if (GetEntityExportSetting(pmSetting->m_strName) == NULL)
	{
		m_arrEntitySettings->Add(pmSetting);
	}
}
//------------------------------------------------------------------------------
// 删除一个
void MEntityExportManager::RemoveEntityExportSetting(MEntityExportSetting* pmSetting)
{
	if (pmSetting == NULL) return;

	if (m_arrEntitySettings->Contains(pmSetting))
	{
		m_arrEntitySettings->Remove(pmSetting);
	}
}
//------------------------------------------------------------------------------
void MEntityExportManager::RemoveEntityExportSetting(String* strName)
{
	MEntityExportSetting* pmSetting = GetEntityExportSetting(strName);
	if (pmSetting != NULL)
	{
		RemoveEntityExportSetting(pmSetting);
	}
}
//------------------------------------------------------------------------------
void MEntityExportManager::Clear()
{
	m_arrEntitySettings->Clear();
}
//------------------------------------------------------------------------------
// 获取所有的 sections
ArrayList* MEntityExportManager::GetAllSections()
{
	ArrayList* arrSections = new ArrayList();

	// 遍历配置列表
	for (int i=0; i<m_arrEntitySettings->Count; i++)
	{
		MEntityExportSetting* pmSetting = 
			dynamic_cast<MEntityExportSetting*>(m_arrEntitySettings->Item[i]);

		if (pmSetting != NULL && !arrSections->Contains(pmSetting->m_strSection))
		{
			arrSections->Add(pmSetting->m_strSection);
		}
	}

	return arrSections;
}
//------------------------------------------------------------------------------
// 或者 Export Setting 列表
ArrayList* MEntityExportManager::get_EntityExportSettingList()
{
	return m_arrEntitySettings;
}
//------------------------------------------------------------------------------
void MEntityExportManager::SaveToXml(String* strDirectory)
{
	if (strDirectory == NULL) return;

	try
	{
		String* strPath = String::Concat(strDirectory, MEntityExportManager::SAVING_FILE);

		// 创建 xml doc
		XmlDocument* xmlDoc = new XmlDocument();

		XmlDeclaration* xmlDecl = xmlDoc->CreateXmlDeclaration("1.0", "gb2312", "yes");
		xmlDoc->AppendChild(xmlDecl);

		// 根结点
		XmlElement* elmtRoot = xmlDoc->CreateElement("root");
		xmlDoc->AppendChild(elmtRoot);

		// 添加所有 Export Setting
		for (int i=0; i<m_arrEntitySettings->Count; i++)
		{
			MEntityExportSetting* pmSetting =
				dynamic_cast<MEntityExportSetting*>(m_arrEntitySettings->Item[i]);

			if (pmSetting != NULL)
			{
				XmlElement* elmtSetting = pmSetting->ToXml(xmlDoc);
				elmtRoot->AppendChild(elmtSetting);
			}
		}

		xmlDoc->Save(strPath);
	}
	catch (Exception* e)
	{
		MessageBox::Show(e->ToString());
	}

}
//------------------------------------------------------------------------------
void MEntityExportManager::LoadFromXml(String* strDirectory)
{
	if (strDirectory == NULL) return;

	// 从 xml 加载
	try
	{
		String* strPath = String::Concat(strDirectory, SAVING_FILE);
		XmlDocument* xmlDoc = new XmlDocument();
		xmlDoc->Load(strPath);

		// 清空旧数据
		m_arrEntitySettings->Clear();

		// 定位到 Root Element
		XmlElement* elmtRoot = NULL;
		XmlNode* nodeRoot = xmlDoc->FirstChild;
		while (nodeRoot != NULL)
		{
			elmtRoot = dynamic_cast<XmlElement*>(nodeRoot);
			if (elmtRoot != NULL) break;
			nodeRoot = nodeRoot->NextSibling;
		}

		// 遍历 elmtRoot 下所有结点
		XmlNode* nodeSetting = elmtRoot->FirstChild;
		while (nodeSetting != NULL)
		{
			XmlElement* elmtSetting = dynamic_cast<XmlElement*>(nodeSetting);
			if (elmtSetting != NULL)
			{
				MEntityExportSetting* pmSetting = MEntityExportSetting::FromXml(elmtSetting);
				if (pmSetting != NULL)
				{
					m_arrEntitySettings->Add(pmSetting);
				}
			}
			nodeSetting = nodeSetting->NextSibling;
		}
	}
	catch (Exception* e)
	{
		MessageBox::Show(e->ToString());
	}
}
//------------------------------------------------------------------------------
MEntityExportSetting::MEntityExportSetting()
: m_iExtraID(-1)
{}
//------------------------------------------------------------------------------
// Xml IO
XmlElement* MEntityExportSetting::ToXml(XmlDocument* xmlDoc)
{
	if (xmlDoc == NULL) return NULL;

	XmlElement* elmtSetting = xmlDoc->CreateElement("EntityExportSetting");
	elmtSetting->SetAttribute("name", m_strName);
	elmtSetting->SetAttribute("section", m_strSection);
	elmtSetting->SetAttribute("id", m_iExtraID.ToString());

	return elmtSetting;
}
//------------------------------------------------------------------------------
MEntityExportSetting* MEntityExportSetting::FromXml(XmlElement* xmlElement)
{
	if (xmlElement == NULL) return NULL;

	String* strName = xmlElement->GetAttribute("name");
	String* strSection = xmlElement->GetAttribute("section");
	int iExtraID = Int32::Parse(xmlElement->GetAttribute("id"));

	MEntityExportSetting* pmSetting = new MEntityExportSetting();
	pmSetting->m_strName = strName;
	pmSetting->m_strSection = strSection;
	pmSetting->m_iExtraID = iExtraID;

	return pmSetting;
}