﻿/** @file MBrushParameter.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangxu
*	完成日期：2007-11-14
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
#include "MTerrain.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		[SerializableAttribute]
		public __gc class MBrushParameter
		{
		public:
			MBrushParameter();
			MBrushParameter( float fInnerRadius, float fOuterRadius, float fValue, String *pcTexFile,  MTerrain::EPainterType ePainterMode);
			MBrushParameter(const MBrushParameter& kBrushParameter);

			~MBrushParameter();
			void SetData(const MBrushParameter& kBrushParameter);

			void SetInnerRadius( float fInnerRadius );
			void SetOuterRadius( float fOuterRadius );
			void SetValue( float fV );
			void SetTexFile( String *pStrTexFile );

			void SetPainterMode(const MTerrain::EPainterType ePainterMode);

			__property float get_InnerRadius();
			__property float get_OuterRadius();
			__property float get_Value();
			__property String *get_TexFile();
			__property MTerrain::EPainterType get_PainterMode();

			//System::Object overrides
			virtual bool Equals(Object* pmObj);

		private:

			float m_fInnerRadius;
			float m_fOuterRadius;
			float m_fValue;

			String*	m_pcTexFile;
			MTerrain::EPainterType	  m_ePainterMode;	// 绘制工具的绘制类型
		};
	}}}}
