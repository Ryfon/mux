﻿#pragma once
// 水体参数
#include "MQuaternion.h"
#include "MPoint2.h"
#include "MPoint3.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		[SerializableAttribute]
		public __gc class MAdvanceWaterParameter
		{
		public:
			MAdvanceWaterParameter(NiAVObject* pkTagAVObj);

			// 属性
			// 振幅
			__property MQuaternion* get_Amplitude();
			__property void set_Amplitude(MQuaternion* pmAmplitude);

			// 频率 每个 2Pi 距离波的重复次数
			__property MQuaternion* get_WaveFrequency();
			__property void set_WaveFrequency(MQuaternion* pmAmplitude);

			// 相位常熟，每秒相位偏移次数
			__property MQuaternion* get_WavePhi();
			__property void set_WavePhi(MQuaternion* pmPhi);

			// 四个波的方向
			__property MPoint2* get_WaveDir0();
			__property void set_WaveDir0(MPoint2* pmDir);
			__property MPoint2* get_WaveDir1();
			__property void set_WaveDir1(MPoint2* pmDir);
			__property MPoint2* get_WaveDir2();
			__property void set_WaveDir2(MPoint2* pmDir);
			__property MPoint2* get_WaveDir3();
			__property void set_WaveDir3(MPoint2* pmDir);

			// 透明度
			__property float get_WaterAlpha();
			__property void set_WaterAlpha(float fAlpha);

			// 反射色
			__property MPoint3* get_ColorReflect();
			__property void set_ColorReflect(MPoint3* pmColor);

			// 高光色
			__property MPoint3* get_ColorSpecular();
			__property void set_ColorSpecular(MPoint3* pmColor);

			//System::Object overrides
			virtual String* ToString() {return "AdvanceWaterParameter";}

		protected:

			// 获取 Extra Data 函数
			MQuaternion* _GetFloat4ExtraData(const char* pszExtraDataName);
			MPoint2* _GetFloat2ExtraData(const char* pszExtraDataName);
			MPoint3* _GetFloat3ExtraData(const char* pszExtraDataName);
			float _GetFloatExtraData(const char* pszExtraDataName);
			// 设置 Extra Data
			void _SetFloat4ExtraData(const char* pszExtraDataName, MQuaternion* pmExtraData);
			void _SetFloat2ExtraData(const char* pszExtraDataName, MPoint2* pmExtraData);
			void _SetFloat3ExtraData(const char* pszExtraDataName, MPoint3* pmExtraData);
			void _SetFloatExtraData(const char* pszExtraDataName, float fExtraData);

			NiAVObject* m_pkTagObject;	// 目标对象
		};
	}}}}