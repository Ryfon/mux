﻿#pragma once

#include "MNiProperty.h"

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
	namespace Framework
	{
		[SerializableAttribute]
		public __gc class MSpecularProperty : public MNiProperty
		{
		public:
			MSpecularProperty(NiSpecularProperty* pkProperty, NiAVObject* pkTagAVObj);

			// 属性
			__property bool get_Specular();
			__property void set_Specular(bool bSpe);

			//System::Object overrides
			virtual bool Equals(Object* pmObj);
			virtual String* ToString() {return "NiSpecularProperty";}


		};
	}}}}
