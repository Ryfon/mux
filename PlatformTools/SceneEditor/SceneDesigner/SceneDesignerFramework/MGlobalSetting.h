﻿#pragma once

#include "MEntity.h"
#include "MSelectionSet.h"
#include "MRenderingContext.h"
#include "MPoint3.h"
#include "IMessageService.h"
#include "ICommandService.h"
#include "tinyxml.h"
// 包围盒缩放数据 [9/15/2009 hemeng]
#include "BoundGeometroy.h"

#include <vcclr.h>

#define	MINOR_ENTITY_LEVEL 2

using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::Xml;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{
// 雾效管理
public __gc class MGlobalSetting
{
public:
	MGlobalSetting(void);
	~MGlobalSetting(void);

	__value enum EAdjustType
	{
		AT_ADJUST_BRIGHT = 0,		//亮度调节
		AT_ADJUST_CONTRAST,			//对比度调节
		AT_ADJUST_HUE,				//色相调节
		AT_ADJUST_SAT,				//饱和度调节
		AT_MAX,
	};

	// add [5/27/2009 hemeng]
	__value enum EBloomType
	{
		BT_LUM = 0,					// Bloom 亮度
		BT_SCALE,					// Bloom 范围
		BT_MAX,
	};


	// 保存/载入 xml 配置
	void SaveToXml(String* strXmlFilename);
	void LoadFromXml(String* strXmlFilename);

	TiXmlElement* GetFogAsXmlElement();
	TiXmlElement* GetPostEffectAsXmlElement();
	TiXmlElement* GetMinorListAsXmlElement();
	// 获取包围盒缩放数据 [9/15/2009 hemeng]
	TiXmlElement* GetAABBScale();

	// add [5/27/2009 hemeng]
	TiXmlElement* GetBloomAsXmlElement();

	// 导出时不导出次要物品列表 [6/25/2009 hemeng]
	TiXmlElement*	GetAsXmlElement(bool bExprot);

	// Get water cause parameter [10/26/2009 hemeng]
	TiXmlElement* GetWaterSceneAsXmlElement();

	// [11/16/2009 shiyazheng] 获取地形系数 xml 节点
	TiXmlElement* GetTerrainParamAsXmlElement();

	//
	void			LoadFog(TiXmlElement* pElmt);
	void			LoadPostEffect(TiXmlElement* pElmt);
	void			LoadMinorList(TiXmlElement* pElmt);
	void			LoadAABBScaleInfo(TiXmlElement* pElmt);

	// add [5/27/2009 hemeng]
	void			LoadBloomEffect(TiXmlElement* pElmt);
	void			LoadWaterScene(TiXmlElement* pElmt);

	//  [11/16/2009 shiyazheng]
	void	LoadTerrainParam(TiXmlElement* pElmt);
	
		// 清空之前配置
	void Clear();

	// 为一个 node 添加 fog property
	void AttachFogProperty(NiAVObject* pkAVObj);
	void AttachFogProperty(MEntity* amEntity[]);
	void AttachFogProperty(MEntity* pmEntity);

	// 设置深度
	void SetFogDepth(float fDepth);

	// 设置颜色
	void SetFogColor(NiColor& kColor);

	// 设置是否开启雾效
	void SetFogEnabled(bool bEnabled);

	// 设置场景所有物件雾效属性
	void SetFogProperties(System::Drawing::Color* color, float fDepth, bool bEnabled);

	// 设置远近平面
	void SetNearFarPlane(float fNear, float fFar);

	// 应用雾效到所有场景物件
	void ApplyFogToScene();

	// 添加/删除名称到次要物件列表
	//081230 modified by 和萌
	void AddMinorEntity(System::String* strEntityName, int iMinorLevel);
	void RemoveMinorEntity(System::String* strEntityName, int iMinorLevel);

	// 清空次要物件列表
	void ClearMinorEntityList();

	// 获取唯一实例
	__property static MGlobalSetting* get_Instance();

	// 获取远近平面
	__property float get_NearPlane();
	__property float get_FarPlane();
	__property float get_Depth();
	__property int get_R();
	__property int get_G();
	__property int get_B();
	__property bool get_FogEnabled();
	__property float get_Bloom_Lum();
	__property float get_Bloom_Scale();

	__property ArrayList* get_MinorEntityList()[];

	// 地形的 ambient 光影响系数
	__property float get_TerrainAmbient();
	__property void set_TerrainAmbient(float fAmbient);

	// 销毁
	static void Shutdown();

	//080905 add by 和萌 post effect相关
	void	SetColorAdjustEffectEnabled(bool bEnabled);
	bool	GetColorAdjustEffectEnabled();
	void	SetAdjustValueByType(EAdjustType type,float value);
	float	GetAdjustValueByType(EAdjustType type);

	// add [5/27/2009 hemeng]
	// bloom 
	void	SetBloomEffectEnabled(bool bEnabled);
	bool	GetBloomEffectEnabled();
	void	SetBloomValueByType(EBloomType type, float value);
	float	GetBloomValueByType(EBloomType type);

	// water scene parameter [10/26/2009 hemeng]
	void	SetWaterSceneEnabled(bool bEnabled);
	bool	GetWaterSceneEnabled();
	void	SetWaterSceneCauseSize(float fSzie);
	float	GetWaterSceneCauseSize();
	void	SetWaterSceneCauseXOffset(float fXOffset);
	float	GetWaterSceneCauseXOffset();
	void	SetWaterSceneCauseYOffset(float fYOffset);
	float	GetWaterSceneCauseYOffset();
	void	SetWaterSceneCauseFactor(float fFactor);
	float	GetWaterSceneCauseFactor();


private:

	static MGlobalSetting*	ms_pmThis = NULL;
	NiColor*		m_pkColor;	// 雾的颜色
	float			m_fDepth;	// 深度
	bool			m_bEnabled;	// 是否开启
	float			m_fNear;	// 远近平面
	float			m_fFar;
	
	ArrayList*	m_pMinorEntity[];		// 次要物品列表 目前支持1、2两级次要物品等级

	//080905 add by 和萌 post effect相关
	bool			m_bPostEffectEnabled;		//是否开启post effect
	float *			m_fArrAdjustValue;	

	// add [5/27/2009 hemeng]
	// bloom effect config
	bool			m_bBloomEffectEnabled;		// 是否打开bloom effect
	float*			m_fArrBloomConfig;			// bloom参数

	// Water scene cause parameters [10/26/2009 hemeng]
	bool			m_bWaterScene;
	float			m_fWaterCauseSize;		
	float			m_fWaterCauseXOffset;
	float			m_fWaterCauseYOffset;
	float			m_fWaterCauseFactor;

	// [11/16/2009 shiyazheng] 地形系数
	float			m_fTerrainAmbient;		// 地形的 ambient 光影响系数
};

}}}}

