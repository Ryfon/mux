﻿#pragma once

#include "MDisposable.h"

using namespace System::Collections;
using namespace System::Collections::Generic;


namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{
// 用于 记录时间戳 log，用作性能分析。
public __gc class MTimeStamp : public MDisposable
{

public:
	~MTimeStamp(void);
	
	// 添加一条时间戳记录
	void AddTimeStamp(System::String* strMsg);

	// 保存到文件
	void SaveToFile();

	// 输出到控制台
	void Output();

protected:
	// MDisposable members.
	virtual void Do_Dispose(bool bDisposing);

	// Singleton members.
public:
	static void Init();
	static void Shutdown();
	static bool InstanceIsValid();
	__property static MTimeStamp* get_Instance();


private:
	static MTimeStamp*	ms_pmThis = NULL;
	MTimeStamp();

	ArrayList*	m_TimeStampList;

};
}}}}