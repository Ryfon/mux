﻿
#pragma once

#include "MDisposable.h"
#include "MEntity.h"
#include "MSelectionSet.h"
#include "MRenderingContext.h"
#include "MPoint3.h"
#include "IMessageService.h"
#include "ICommandService.h"

#include <vcclr.h>
#pragma unmanaged
#include <NiScene.h>
#include "TerrainGridBaseRegion.h"
#include "AdvanceWater.h"
#include "SprayGenerator.h"
#include "SnapTool.h"
#pragma managed

using namespace System;
using namespace System::IO;
using namespace System::Windows;
using namespace System::Windows::Forms;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;

using namespace SceneCore;


namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
namespace Framework
{
	// Managed LightMapMaterial 辅助类
	public __gc class MLightMapMaterialHelper
	{
	public:
		__gc struct sConvertResultInfo
		{
			sConvertResultInfo(String* strTarget, bool bSucc, String* strInfo)
			{
				m_strTargetFile = strTarget;
				m_bSuccess = bSucc;
				m_strInfo = strInfo;
			}

			String* m_strTargetFile;	// 转换的目标文件
			bool	m_bSuccess;			// 是否转换成功
			String* m_strInfo;			// 转换信息
		};

		// 将一个文件转换到 LightMapmaterial
		static sConvertResultInfo* ConvertToLightMapMaterial(String* strFileName, String* strBackupDir);
		// 将列表中的文件批量转换为 light map material, 返回 ConvertResultInfo 列表	
		static ArrayList* ConvertToLightMapMaterial(ArrayList* arrFileList, String* strBackupDir);
		// 将 pkInputNode 中所有 geometry 转换为 适合烘焙 light map 的 geometry
		static bool ConvertToLightMapMaterial(NiNode* pkInputNode, NiNode*& pkOutputNode, String*& strInfo);

		// 清除结点下所有纹理的 PersistentSrcRendererData 和 PixelData, 只保留名称
		static void EmptyAllTextureDatas(NiAVObject* pkAVObj);
		static void EmptyTextureData(NiTexturingProperty::Map* pkMap);

		// 测试函数
		static void Test();
	};
}}}}

