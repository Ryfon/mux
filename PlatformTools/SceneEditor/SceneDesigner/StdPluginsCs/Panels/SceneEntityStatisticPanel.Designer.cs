﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class SceneEntityStatisticPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.m_dgvSceneEntityStatistic = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_menuDGV = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.在场景中选择ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.m_dgvEntityInst = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_dgvTextureInfo = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_tbTexSize = new System.Windows.Forms.TextBox();
            this.m_tbTexCount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_tbNifSize = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_tbNifCount = new System.Windows.Forms.TextBox();
            this.m_btnLightDistribute = new System.Windows.Forms.Button();
            this.m_cbStatLights = new System.Windows.Forms.CheckBox();
            this.m_btnStatisticSceneEntity = new System.Windows.Forms.Button();
            this.m_tbEntityCount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvSceneEntityStatistic)).BeginInit();
            this.m_menuDGV.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvEntityInst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvTextureInfo)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.splitContainer1);
            this.groupBox1.Location = new System.Drawing.Point(3, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(686, 426);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "场景物件";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 17);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.m_dgvSceneEntityStatistic);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(680, 406);
            this.splitContainer1.SplitterDistance = 267;
            this.splitContainer1.TabIndex = 1;
            // 
            // m_dgvSceneEntityStatistic
            // 
            this.m_dgvSceneEntityStatistic.AllowUserToAddRows = false;
            this.m_dgvSceneEntityStatistic.AllowUserToDeleteRows = false;
            this.m_dgvSceneEntityStatistic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvSceneEntityStatistic.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column10,
            this.Column9,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17});
            this.m_dgvSceneEntityStatistic.ContextMenuStrip = this.m_menuDGV;
            this.m_dgvSceneEntityStatistic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_dgvSceneEntityStatistic.Location = new System.Drawing.Point(0, 0);
            this.m_dgvSceneEntityStatistic.Name = "m_dgvSceneEntityStatistic";
            this.m_dgvSceneEntityStatistic.RowTemplate.Height = 23;
            this.m_dgvSceneEntityStatistic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_dgvSceneEntityStatistic.Size = new System.Drawing.Size(680, 267);
            this.m_dgvSceneEntityStatistic.TabIndex = 0;
            this.m_dgvSceneEntityStatistic.SelectionChanged += new System.EventHandler(this.m_dgvSceneEntityStatistic_SelectionChanged);
            this.m_dgvSceneEntityStatistic.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_dgvSceneEntityStatistic_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nif File Path";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "实例数量";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "几何体数量";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "顶点数";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "三角形数";
            this.Column5.Name = "Column5";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "最多灯光影响数";
            this.Column10.Name = "Column10";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "纹理数量";
            this.Column9.Name = "Column9";
            // 
            // Column14
            // 
            this.Column14.HeaderText = "总纹理大小(KB)";
            this.Column14.Name = "Column14";
            // 
            // Column15
            // 
            this.Column15.HeaderText = "粒子数量";
            this.Column15.Name = "Column15";
            // 
            // Column16
            // 
            this.Column16.HeaderText = "最大粒子数量";
            this.Column16.Name = "Column16";
            // 
            // Column17
            // 
            this.Column17.HeaderText = "nif 文件大小(KB)";
            this.Column17.Name = "Column17";
            // 
            // m_menuDGV
            // 
            this.m_menuDGV.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.在场景中选择ToolStripMenuItem});
            this.m_menuDGV.Name = "m_menuDGV";
            this.m_menuDGV.Size = new System.Drawing.Size(143, 26);
            // 
            // 在场景中选择ToolStripMenuItem
            // 
            this.在场景中选择ToolStripMenuItem.Name = "在场景中选择ToolStripMenuItem";
            this.在场景中选择ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.在场景中选择ToolStripMenuItem.Text = "在场景中选择";
            this.在场景中选择ToolStripMenuItem.Click += new System.EventHandler(this.在场景中选择ToolStripMenuItem_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.m_dgvEntityInst);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.m_dgvTextureInfo);
            this.splitContainer2.Size = new System.Drawing.Size(680, 135);
            this.splitContainer2.SplitterDistance = 352;
            this.splitContainer2.TabIndex = 1;
            // 
            // m_dgvEntityInst
            // 
            this.m_dgvEntityInst.AllowUserToAddRows = false;
            this.m_dgvEntityInst.AllowUserToDeleteRows = false;
            this.m_dgvEntityInst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvEntityInst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column7,
            this.Column8});
            this.m_dgvEntityInst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_dgvEntityInst.Location = new System.Drawing.Point(0, 0);
            this.m_dgvEntityInst.Name = "m_dgvEntityInst";
            this.m_dgvEntityInst.RowTemplate.Height = 23;
            this.m_dgvEntityInst.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_dgvEntityInst.Size = new System.Drawing.Size(352, 135);
            this.m_dgvEntityInst.TabIndex = 0;
            this.m_dgvEntityInst.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.m_dgvEntityInst_CellMouseClick);
            this.m_dgvEntityInst.SelectionChanged += new System.EventHandler(this.m_dgvEntityInst_SelectionChanged);
            // 
            // Column6
            // 
            this.Column6.HeaderText = "灯光影响数量";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "位置";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "包围球半径";
            this.Column8.Name = "Column8";
            // 
            // m_dgvTextureInfo
            // 
            this.m_dgvTextureInfo.AllowUserToAddRows = false;
            this.m_dgvTextureInfo.AllowUserToDeleteRows = false;
            this.m_dgvTextureInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvTextureInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column11,
            this.Column12,
            this.Column13});
            this.m_dgvTextureInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_dgvTextureInfo.Location = new System.Drawing.Point(0, 0);
            this.m_dgvTextureInfo.Name = "m_dgvTextureInfo";
            this.m_dgvTextureInfo.RowTemplate.Height = 23;
            this.m_dgvTextureInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_dgvTextureInfo.Size = new System.Drawing.Size(324, 135);
            this.m_dgvTextureInfo.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.m_tbTexSize);
            this.groupBox2.Controls.Add(this.m_tbTexCount);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.m_tbNifSize);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.m_tbNifCount);
            this.groupBox2.Controls.Add(this.m_btnLightDistribute);
            this.groupBox2.Controls.Add(this.m_cbStatLights);
            this.groupBox2.Controls.Add(this.m_btnStatisticSceneEntity);
            this.groupBox2.Controls.Add(this.m_tbEntityCount);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(3, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(686, 66);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "场景信息";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(349, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "KB";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(349, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "KB";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "总大小";
            // 
            // m_tbTexSize
            // 
            this.m_tbTexSize.Location = new System.Drawing.Point(286, 42);
            this.m_tbTexSize.Name = "m_tbTexSize";
            this.m_tbTexSize.Size = new System.Drawing.Size(56, 21);
            this.m_tbTexSize.TabIndex = 11;
            // 
            // m_tbTexCount
            // 
            this.m_tbTexCount.Location = new System.Drawing.Point(171, 42);
            this.m_tbTexCount.Name = "m_tbTexCount";
            this.m_tbTexCount.Size = new System.Drawing.Size(56, 21);
            this.m_tbTexCount.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(125, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "纹理数";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(241, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "总大小";
            // 
            // m_tbNifSize
            // 
            this.m_tbNifSize.Location = new System.Drawing.Point(286, 17);
            this.m_tbNifSize.Name = "m_tbNifSize";
            this.m_tbNifSize.Size = new System.Drawing.Size(56, 21);
            this.m_tbNifSize.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(125, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "NIF 数";
            // 
            // m_tbNifCount
            // 
            this.m_tbNifCount.Location = new System.Drawing.Point(171, 17);
            this.m_tbNifCount.Name = "m_tbNifCount";
            this.m_tbNifCount.Size = new System.Drawing.Size(56, 21);
            this.m_tbNifCount.TabIndex = 5;
            // 
            // m_btnLightDistribute
            // 
            this.m_btnLightDistribute.Location = new System.Drawing.Point(525, 16);
            this.m_btnLightDistribute.Name = "m_btnLightDistribute";
            this.m_btnLightDistribute.Size = new System.Drawing.Size(90, 43);
            this.m_btnLightDistribute.TabIndex = 4;
            this.m_btnLightDistribute.Text = "分布图";
            this.m_btnLightDistribute.UseVisualStyleBackColor = true;
            this.m_btnLightDistribute.Click += new System.EventHandler(this.m_btnLightDistribute_Click);
            // 
            // m_cbStatLights
            // 
            this.m_cbStatLights.AutoSize = true;
            this.m_cbStatLights.Location = new System.Drawing.Point(13, 44);
            this.m_cbStatLights.Name = "m_cbStatLights";
            this.m_cbStatLights.Size = new System.Drawing.Size(96, 16);
            this.m_cbStatLights.TabIndex = 3;
            this.m_cbStatLights.Text = "统计灯光影响";
            this.m_cbStatLights.UseVisualStyleBackColor = true;
            // 
            // m_btnStatisticSceneEntity
            // 
            this.m_btnStatisticSceneEntity.Location = new System.Drawing.Point(383, 17);
            this.m_btnStatisticSceneEntity.Name = "m_btnStatisticSceneEntity";
            this.m_btnStatisticSceneEntity.Size = new System.Drawing.Size(125, 43);
            this.m_btnStatisticSceneEntity.TabIndex = 2;
            this.m_btnStatisticSceneEntity.Text = "统计场景物件信息";
            this.m_btnStatisticSceneEntity.UseVisualStyleBackColor = true;
            this.m_btnStatisticSceneEntity.Click += new System.EventHandler(this.m_btnStatisticSceneEntity_Click);
            // 
            // m_tbEntityCount
            // 
            this.m_tbEntityCount.Location = new System.Drawing.Point(56, 17);
            this.m_tbEntityCount.Name = "m_tbEntityCount";
            this.m_tbEntityCount.Size = new System.Drawing.Size(56, 21);
            this.m_tbEntityCount.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "物件数";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "纹理名称";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "使用次数";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "纹理大小(KB)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // Column11
            // 
            this.Column11.HeaderText = "宽";
            this.Column11.Name = "Column11";
            // 
            // Column12
            // 
            this.Column12.HeaderText = "高";
            this.Column12.Name = "Column12";
            // 
            // Column13
            // 
            this.Column13.HeaderText = "序列数量";
            this.Column13.Name = "Column13";
            // 
            // SceneEntityStatisticPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 513);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "SceneEntityStatisticPanel";
            this.Text = "场景物件统计";
            this.Load += new System.EventHandler(this.SceneEntityStatisticPanel_Load);
            this.groupBox1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvSceneEntityStatistic)).EndInit();
            this.m_menuDGV.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvEntityInst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvTextureInfo)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView m_dgvSceneEntityStatistic;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button m_btnStatisticSceneEntity;
        private System.Windows.Forms.TextBox m_tbEntityCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip m_menuDGV;
        private System.Windows.Forms.ToolStripMenuItem 在场景中选择ToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView m_dgvEntityInst;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.CheckBox m_cbStatLights;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView m_dgvTextureInfo;
        private System.Windows.Forms.Button m_btnLightDistribute;
        private System.Windows.Forms.TextBox m_tbNifCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_tbNifSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox m_tbTexSize;
        private System.Windows.Forms.TextBox m_tbTexCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
    }
}