﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class SnapToolPanel : Form
    {
        public SnapToolPanel()
        {
            InitializeComponent();
        }

        private void btn_GetSelectSrcEntity_Click(object sender, EventArgs e)
        {
            // 获取当前被选择的物件
            MEntity[] aEntities = MFramework.SelectionService.GetSelectedEntities();
            if (aEntities.Length == 0)
            {
                return;
            }

            m_SrcEntity = aEntities[0];
            
            if (MTerrain.Instance.SetSnapSrcEntity(m_SrcEntity) == true)
            {
                m_textBox_SrcEntityName.Text = m_SrcEntity.Name;
            }            
        }

        private void btn_GetSelectDesEntity_Click(object sender, EventArgs e)
        {
            // 获取当前被选择的物件
            MEntity[] aEntities = MFramework.SelectionService.GetSelectedEntities();
            if (aEntities.Length == 0)
            {
                return;
            }

            m_DesEntity = aEntities[0];
            
            if( MTerrain.Instance.SetSnapDesEntity(m_DesEntity) == true)
            {
                m_textBox_DesEntityName.Text = m_DesEntity.Name;
            }           
        }

        private void btn_ClearEntity_Click(object sender, EventArgs e)
        {
            MTerrain.Instance.ClearSnapEntity();
            m_textBox_SrcEntityName.Text = "";
            m_textBox_DesEntityName.Text = "";
        }

        private void SnapToolPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
            MTerrain.Instance.ClearSnapEntity();
            m_textBox_SrcEntityName.Text = "";
            m_textBox_DesEntityName.Text = "";
        }
    }
}