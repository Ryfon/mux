﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class ShadowPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnGenerateAODisk = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_listBoxAllChunks = new System.Windows.Forms.ListBox();
            this.m_listBoxShadowedChunks = new System.Windows.Forms.ListBox();
            this.m_btnAddChunkToList = new System.Windows.Forms.Button();
            this.m_btnRemoveFromShadowedChunk = new System.Windows.Forms.Button();
            this.m_btnGenerateTerrainAOShadow = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_textBoxTerrAOMaxShadowed = new System.Windows.Forms.TextBox();
            this.m_textBoxMessage = new System.Windows.Forms.TextBox();
            this.m_btnSaveDiskInfo = new System.Windows.Forms.Button();
            this.m_btnLoadDiskInfo = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.m_textBoxTerrainAffectFactor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_btnProjectionShadow = new System.Windows.Forms.Button();
            this.m_btnClear = new System.Windows.Forms.Button();
            this.m_btnSelectAll = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.m_textBoxDirMaxShadowed = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.m_btnGenerateEntityAOShadow = new System.Windows.Forms.Button();
            this.m_tbBlurParam = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.m_tbBlurEntityAO = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.m_tbBlurTexSize = new System.Windows.Forms.TextBox();
            this.m_tbEntityAOMaxShadow = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.m_cbViewStaticShadow = new System.Windows.Forms.CheckBox();
            this.m_cbViewDynamicShadow = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // m_btnGenerateAODisk
            // 
            this.m_btnGenerateAODisk.Location = new System.Drawing.Point(124, 8);
            this.m_btnGenerateAODisk.Name = "m_btnGenerateAODisk";
            this.m_btnGenerateAODisk.Size = new System.Drawing.Size(64, 22);
            this.m_btnGenerateAODisk.TabIndex = 0;
            this.m_btnGenerateAODisk.Text = "开始";
            this.m_btnGenerateAODisk.UseVisualStyleBackColor = true;
            this.m_btnGenerateAODisk.Click += new System.EventHandler(this.m_btnGenerateAODisk_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "1. 搜集场景信息";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "3.环境阴影";
            // 
            // m_listBoxAllChunks
            // 
            this.m_listBoxAllChunks.FormattingEnabled = true;
            this.m_listBoxAllChunks.ItemHeight = 12;
            this.m_listBoxAllChunks.Location = new System.Drawing.Point(57, 90);
            this.m_listBoxAllChunks.Name = "m_listBoxAllChunks";
            this.m_listBoxAllChunks.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.m_listBoxAllChunks.Size = new System.Drawing.Size(67, 88);
            this.m_listBoxAllChunks.TabIndex = 3;
            // 
            // m_listBoxShadowedChunks
            // 
            this.m_listBoxShadowedChunks.FormattingEnabled = true;
            this.m_listBoxShadowedChunks.ItemHeight = 12;
            this.m_listBoxShadowedChunks.Location = new System.Drawing.Point(175, 90);
            this.m_listBoxShadowedChunks.Name = "m_listBoxShadowedChunks";
            this.m_listBoxShadowedChunks.Size = new System.Drawing.Size(67, 88);
            this.m_listBoxShadowedChunks.TabIndex = 4;
            // 
            // m_btnAddChunkToList
            // 
            this.m_btnAddChunkToList.Location = new System.Drawing.Point(137, 109);
            this.m_btnAddChunkToList.Name = "m_btnAddChunkToList";
            this.m_btnAddChunkToList.Size = new System.Drawing.Size(32, 23);
            this.m_btnAddChunkToList.TabIndex = 5;
            this.m_btnAddChunkToList.Text = "->";
            this.m_btnAddChunkToList.UseVisualStyleBackColor = true;
            this.m_btnAddChunkToList.Click += new System.EventHandler(this.m_btnAddChunkToList_Click);
            // 
            // m_btnRemoveFromShadowedChunk
            // 
            this.m_btnRemoveFromShadowedChunk.Location = new System.Drawing.Point(137, 142);
            this.m_btnRemoveFromShadowedChunk.Name = "m_btnRemoveFromShadowedChunk";
            this.m_btnRemoveFromShadowedChunk.Size = new System.Drawing.Size(32, 24);
            this.m_btnRemoveFromShadowedChunk.TabIndex = 6;
            this.m_btnRemoveFromShadowedChunk.Text = "<-";
            this.m_btnRemoveFromShadowedChunk.UseVisualStyleBackColor = true;
            this.m_btnRemoveFromShadowedChunk.Click += new System.EventHandler(this.button1_Click);
            // 
            // m_btnGenerateTerrainAOShadow
            // 
            this.m_btnGenerateTerrainAOShadow.Location = new System.Drawing.Point(62, 249);
            this.m_btnGenerateTerrainAOShadow.Name = "m_btnGenerateTerrainAOShadow";
            this.m_btnGenerateTerrainAOShadow.Size = new System.Drawing.Size(214, 24);
            this.m_btnGenerateTerrainAOShadow.TabIndex = 9;
            this.m_btnGenerateTerrainAOShadow.Text = "生成地形环境阴影";
            this.m_btnGenerateTerrainAOShadow.UseVisualStyleBackColor = true;
            this.m_btnGenerateTerrainAOShadow.Click += new System.EventHandler(this.m_btnGenerateShadow_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(173, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "需要生成阴影的 chunk";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "所有 chunk ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(190, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "最大阴影深度(0~1)";
            // 
            // m_textBoxTerrAOMaxShadowed
            // 
            this.m_textBoxTerrAOMaxShadowed.Location = new System.Drawing.Point(299, 221);
            this.m_textBoxTerrAOMaxShadowed.Name = "m_textBoxTerrAOMaxShadowed";
            this.m_textBoxTerrAOMaxShadowed.Size = new System.Drawing.Size(51, 21);
            this.m_textBoxTerrAOMaxShadowed.TabIndex = 11;
            this.m_textBoxTerrAOMaxShadowed.Text = "0.3";
            this.m_textBoxTerrAOMaxShadowed.TextChanged += new System.EventHandler(this.m_textBoxMaxShadowed_TextChanged);
            // 
            // m_textBoxMessage
            // 
            this.m_textBoxMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_textBoxMessage.Location = new System.Drawing.Point(0, 478);
            this.m_textBoxMessage.Multiline = true;
            this.m_textBoxMessage.Name = "m_textBoxMessage";
            this.m_textBoxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_textBoxMessage.Size = new System.Drawing.Size(350, 114);
            this.m_textBoxMessage.TabIndex = 12;
            // 
            // m_btnSaveDiskInfo
            // 
            this.m_btnSaveDiskInfo.Enabled = false;
            this.m_btnSaveDiskInfo.Location = new System.Drawing.Point(197, 8);
            this.m_btnSaveDiskInfo.Name = "m_btnSaveDiskInfo";
            this.m_btnSaveDiskInfo.Size = new System.Drawing.Size(64, 22);
            this.m_btnSaveDiskInfo.TabIndex = 13;
            this.m_btnSaveDiskInfo.Text = "保存";
            this.m_btnSaveDiskInfo.UseVisualStyleBackColor = true;
            this.m_btnSaveDiskInfo.Visible = false;
            this.m_btnSaveDiskInfo.Click += new System.EventHandler(this.m_btnSaveDiskInfo_Click);
            // 
            // m_btnLoadDiskInfo
            // 
            this.m_btnLoadDiskInfo.Location = new System.Drawing.Point(270, 8);
            this.m_btnLoadDiskInfo.Name = "m_btnLoadDiskInfo";
            this.m_btnLoadDiskInfo.Size = new System.Drawing.Size(64, 22);
            this.m_btnLoadDiskInfo.TabIndex = 14;
            this.m_btnLoadDiskInfo.Text = "载入";
            this.m_btnLoadDiskInfo.UseVisualStyleBackColor = true;
            this.m_btnLoadDiskInfo.Visible = false;
            this.m_btnLoadDiskInfo.Click += new System.EventHandler(this.m_btnLoadDiskInfo_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 224);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "地形影响系数";
            // 
            // m_textBoxTerrainAffectFactor
            // 
            this.m_textBoxTerrainAffectFactor.Location = new System.Drawing.Point(128, 221);
            this.m_textBoxTerrainAffectFactor.Name = "m_textBoxTerrainAffectFactor";
            this.m_textBoxTerrainAffectFactor.Size = new System.Drawing.Size(51, 21);
            this.m_textBoxTerrainAffectFactor.TabIndex = 16;
            this.m_textBoxTerrainAffectFactor.Text = "20";
            this.m_textBoxTerrainAffectFactor.TextChanged += new System.EventHandler(this.m_textBoxTerrainAffectFactor_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(167, 12);
            this.label8.TabIndex = 19;
            this.label8.Text = "2. 选择需要生成阴影的 chunk";
            // 
            // m_btnProjectionShadow
            // 
            this.m_btnProjectionShadow.Location = new System.Drawing.Point(62, 415);
            this.m_btnProjectionShadow.Name = "m_btnProjectionShadow";
            this.m_btnProjectionShadow.Size = new System.Drawing.Size(214, 24);
            this.m_btnProjectionShadow.TabIndex = 20;
            this.m_btnProjectionShadow.Text = "生成方向光阴影";
            this.m_btnProjectionShadow.UseVisualStyleBackColor = true;
            this.m_btnProjectionShadow.Click += new System.EventHandler(this.m_btnProjectionShadow_Click);
            // 
            // m_btnClear
            // 
            this.m_btnClear.Location = new System.Drawing.Point(262, 158);
            this.m_btnClear.Name = "m_btnClear";
            this.m_btnClear.Size = new System.Drawing.Size(48, 20);
            this.m_btnClear.TabIndex = 21;
            this.m_btnClear.Text = "清除";
            this.m_btnClear.UseVisualStyleBackColor = true;
            this.m_btnClear.Click += new System.EventHandler(this.m_btnClear_Click);
            // 
            // m_btnSelectAll
            // 
            this.m_btnSelectAll.Location = new System.Drawing.Point(0, 158);
            this.m_btnSelectAll.Name = "m_btnSelectAll";
            this.m_btnSelectAll.Size = new System.Drawing.Size(51, 20);
            this.m_btnSelectAll.TabIndex = 22;
            this.m_btnSelectAll.Text = "全选";
            this.m_btnSelectAll.UseVisualStyleBackColor = true;
            this.m_btnSelectAll.Click += new System.EventHandler(this.m_btnSelectAll_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 371);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 12);
            this.label9.TabIndex = 23;
            this.label9.Text = "4. 方向光阴影";
            // 
            // m_textBoxDirMaxShadowed
            // 
            this.m_textBoxDirMaxShadowed.Location = new System.Drawing.Point(129, 388);
            this.m_textBoxDirMaxShadowed.Name = "m_textBoxDirMaxShadowed";
            this.m_textBoxDirMaxShadowed.Size = new System.Drawing.Size(40, 21);
            this.m_textBoxDirMaxShadowed.TabIndex = 28;
            this.m_textBoxDirMaxShadowed.Text = "1.0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 392);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 12);
            this.label11.TabIndex = 27;
            this.label11.Text = "最大阴影深度(0~1)";
            // 
            // m_btnGenerateEntityAOShadow
            // 
            this.m_btnGenerateEntityAOShadow.Location = new System.Drawing.Point(62, 332);
            this.m_btnGenerateEntityAOShadow.Name = "m_btnGenerateEntityAOShadow";
            this.m_btnGenerateEntityAOShadow.Size = new System.Drawing.Size(214, 24);
            this.m_btnGenerateEntityAOShadow.TabIndex = 29;
            this.m_btnGenerateEntityAOShadow.Text = "生成物件环境阴影";
            this.m_btnGenerateEntityAOShadow.UseVisualStyleBackColor = true;
            this.m_btnGenerateEntityAOShadow.Click += new System.EventHandler(this.m_btnGenerateEntityAOShadow_Click);
            // 
            // m_tbBlurParam
            // 
            this.m_tbBlurParam.Location = new System.Drawing.Point(266, 388);
            this.m_tbBlurParam.Name = "m_tbBlurParam";
            this.m_tbBlurParam.Size = new System.Drawing.Size(38, 21);
            this.m_tbBlurParam.TabIndex = 30;
            this.m_tbBlurParam.Text = "0.0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(181, 392);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 12);
            this.label10.TabIndex = 31;
            this.label10.Text = "模糊程度(0~1)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(192, 311);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 12);
            this.label12.TabIndex = 33;
            this.label12.Text = "模糊程度(0~1)";
            // 
            // m_tbBlurEntityAO
            // 
            this.m_tbBlurEntityAO.Location = new System.Drawing.Point(300, 307);
            this.m_tbBlurEntityAO.Name = "m_tbBlurEntityAO";
            this.m_tbBlurEntityAO.Size = new System.Drawing.Size(38, 21);
            this.m_tbBlurEntityAO.TabIndex = 32;
            this.m_tbBlurEntityAO.Text = "0.75";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 310);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 12);
            this.label7.TabIndex = 35;
            this.label7.Text = "模糊纹理尺寸(0~1)";
            // 
            // m_tbBlurTexSize
            // 
            this.m_tbBlurTexSize.Location = new System.Drawing.Point(128, 307);
            this.m_tbBlurTexSize.Name = "m_tbBlurTexSize";
            this.m_tbBlurTexSize.Size = new System.Drawing.Size(38, 21);
            this.m_tbBlurTexSize.TabIndex = 34;
            this.m_tbBlurTexSize.Text = "0.5";
            // 
            // m_tbEntityAOMaxShadow
            // 
            this.m_tbEntityAOMaxShadow.Location = new System.Drawing.Point(129, 283);
            this.m_tbEntityAOMaxShadow.Name = "m_tbEntityAOMaxShadow";
            this.m_tbEntityAOMaxShadow.Size = new System.Drawing.Size(37, 21);
            this.m_tbEntityAOMaxShadow.TabIndex = 37;
            this.m_tbEntityAOMaxShadow.Text = "0.6";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 287);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 12);
            this.label13.TabIndex = 36;
            this.label13.Text = "最大阴影深度(0~1)";
            // 
            // m_cbViewStaticShadow
            // 
            this.m_cbViewStaticShadow.AutoSize = true;
            this.m_cbViewStaticShadow.Location = new System.Drawing.Point(29, 456);
            this.m_cbViewStaticShadow.Name = "m_cbViewStaticShadow";
            this.m_cbViewStaticShadow.Size = new System.Drawing.Size(96, 16);
            this.m_cbViewStaticShadow.TabIndex = 38;
            this.m_cbViewStaticShadow.Text = "静态阴影预浏";
            this.m_cbViewStaticShadow.UseVisualStyleBackColor = true;
            this.m_cbViewStaticShadow.CheckedChanged += new System.EventHandler(this.m_cbViewStaticShadow_CheckedChanged);
            // 
            // m_cbViewDynamicShadow
            // 
            this.m_cbViewDynamicShadow.AutoSize = true;
            this.m_cbViewDynamicShadow.Location = new System.Drawing.Point(153, 456);
            this.m_cbViewDynamicShadow.Name = "m_cbViewDynamicShadow";
            this.m_cbViewDynamicShadow.Size = new System.Drawing.Size(96, 16);
            this.m_cbViewDynamicShadow.TabIndex = 39;
            this.m_cbViewDynamicShadow.Text = "动态阴影预浏";
            this.m_cbViewDynamicShadow.UseVisualStyleBackColor = true;
            this.m_cbViewDynamicShadow.CheckedChanged += new System.EventHandler(this.m_cbViewDynamicShadow_CheckedChanged);
            // 
            // ShadowPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 592);
            this.Controls.Add(this.m_cbViewDynamicShadow);
            this.Controls.Add(this.m_cbViewStaticShadow);
            this.Controls.Add(this.m_tbEntityAOMaxShadow);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.m_tbBlurTexSize);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.m_tbBlurEntityAO);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.m_tbBlurParam);
            this.Controls.Add(this.m_btnGenerateEntityAOShadow);
            this.Controls.Add(this.m_textBoxDirMaxShadowed);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.m_btnSelectAll);
            this.Controls.Add(this.m_btnClear);
            this.Controls.Add(this.m_btnProjectionShadow);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.m_textBoxTerrainAffectFactor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.m_btnLoadDiskInfo);
            this.Controls.Add(this.m_btnSaveDiskInfo);
            this.Controls.Add(this.m_textBoxMessage);
            this.Controls.Add(this.m_textBoxTerrAOMaxShadowed);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_btnGenerateTerrainAOShadow);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_btnRemoveFromShadowedChunk);
            this.Controls.Add(this.m_btnAddChunkToList);
            this.Controls.Add(this.m_listBoxShadowedChunks);
            this.Controls.Add(this.m_listBoxAllChunks);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_btnGenerateAODisk);
            this.Name = "ShadowPanel";
            this.Text = "阴影工具";
            this.Load += new System.EventHandler(this.ShadowPanel_Load);
            this.Shown += new System.EventHandler(this.ShadowPanel_Shown);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ShadowPanel_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnGenerateAODisk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox m_listBoxAllChunks;
        private System.Windows.Forms.ListBox m_listBoxShadowedChunks;
        private System.Windows.Forms.Button m_btnAddChunkToList;
        private System.Windows.Forms.Button m_btnRemoveFromShadowedChunk;
        private System.Windows.Forms.Button m_btnGenerateTerrainAOShadow;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox m_textBoxTerrAOMaxShadowed;
        private System.Windows.Forms.TextBox m_textBoxMessage;
        private System.Windows.Forms.Button m_btnSaveDiskInfo;
        private System.Windows.Forms.Button m_btnLoadDiskInfo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox m_textBoxTerrainAffectFactor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button m_btnProjectionShadow;
        private System.Windows.Forms.Button m_btnClear;
        private System.Windows.Forms.Button m_btnSelectAll;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox m_textBoxDirMaxShadowed;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button m_btnGenerateEntityAOShadow;
        private System.Windows.Forms.TextBox m_tbBlurParam;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox m_tbBlurEntityAO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox m_tbBlurTexSize;
        private System.Windows.Forms.TextBox m_tbEntityAOMaxShadow;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox m_cbViewStaticShadow;
        private System.Windows.Forms.CheckBox m_cbViewDynamicShadow;
    }
}