﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class WaterPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.m_pictureBoxColor = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_textBoxRiverHeight = new System.Windows.Forms.TextBox();
            this.button_CreateRiver = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.button_ExportRiver = new System.Windows.Forms.Button();
            this.button_ClearRiver = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxRiverAlphaDet = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxColor)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "水体颜色";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.button1.Location = new System.Drawing.Point(100, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 20);
            this.button1.TabIndex = 3;
            this.button1.Text = "选择颜色...";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // m_pictureBoxColor
            // 
            this.m_pictureBoxColor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_pictureBoxColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxColor.Location = new System.Drawing.Point(252, 19);
            this.m_pictureBoxColor.Name = "m_pictureBoxColor";
            this.m_pictureBoxColor.Size = new System.Drawing.Size(49, 20);
            this.m_pictureBoxColor.TabIndex = 4;
            this.m_pictureBoxColor.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "水体高度";
            // 
            // m_textBoxRiverHeight
            // 
            this.m_textBoxRiverHeight.Location = new System.Drawing.Point(100, 53);
            this.m_textBoxRiverHeight.Name = "m_textBoxRiverHeight";
            this.m_textBoxRiverHeight.Size = new System.Drawing.Size(49, 21);
            this.m_textBoxRiverHeight.TabIndex = 6;
            this.m_textBoxRiverHeight.Text = "-0.5";
            // 
            // button_CreateRiver
            // 
            this.button_CreateRiver.BackColor = System.Drawing.SystemColors.MenuBar;
            this.button_CreateRiver.Location = new System.Drawing.Point(14, 123);
            this.button_CreateRiver.Name = "button_CreateRiver";
            this.button_CreateRiver.Size = new System.Drawing.Size(75, 23);
            this.button_CreateRiver.TabIndex = 7;
            this.button_CreateRiver.Text = "创建水面";
            this.button_CreateRiver.UseVisualStyleBackColor = false;
            this.button_CreateRiver.Click += new System.EventHandler(this.button_CreateRiver_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(169, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "输入整数";
            // 
            // button_ExportRiver
            // 
            this.button_ExportRiver.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.button_ExportRiver.ForeColor = System.Drawing.SystemColors.Highlight;
            this.button_ExportRiver.Location = new System.Drawing.Point(118, 123);
            this.button_ExportRiver.Name = "button_ExportRiver";
            this.button_ExportRiver.Size = new System.Drawing.Size(75, 23);
            this.button_ExportRiver.TabIndex = 17;
            this.button_ExportRiver.Text = "导出水面";
            this.button_ExportRiver.UseVisualStyleBackColor = false;
            this.button_ExportRiver.Click += new System.EventHandler(this.button_ExportRiver_Click);
            // 
            // button_ClearRiver
            // 
            this.button_ClearRiver.Location = new System.Drawing.Point(226, 123);
            this.button_ClearRiver.Name = "button_ClearRiver";
            this.button_ClearRiver.Size = new System.Drawing.Size(75, 23);
            this.button_ClearRiver.TabIndex = 18;
            this.button_ClearRiver.Text = "清除水面";
            this.button_ClearRiver.UseVisualStyleBackColor = true;
            this.button_ClearRiver.Click += new System.EventHandler(this.button_ClearRiver_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 19;
            this.label3.Text = "Alpha增量值";
            // 
            // m_textBoxRiverAlphaDet
            // 
            this.m_textBoxRiverAlphaDet.Location = new System.Drawing.Point(100, 90);
            this.m_textBoxRiverAlphaDet.Name = "m_textBoxRiverAlphaDet";
            this.m_textBoxRiverAlphaDet.Size = new System.Drawing.Size(45, 21);
            this.m_textBoxRiverAlphaDet.TabIndex = 20;
            this.m_textBoxRiverAlphaDet.Text = "0.35";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(169, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 12);
            this.label4.TabIndex = 21;
            this.label4.Text = "请输入0.0-1.0小数";
            // 
            // WaterPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 159);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_textBoxRiverAlphaDet);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_ClearRiver);
            this.Controls.Add(this.button_ExportRiver);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button_CreateRiver);
            this.Controls.Add(this.m_textBoxRiverHeight);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_pictureBoxColor);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "WaterPanel";
            this.Text = "水体工具";
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox m_pictureBoxColor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_textBoxRiverHeight;
        private System.Windows.Forms.Button button_CreateRiver;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button_ExportRiver;
        private System.Windows.Forms.Button button_ClearRiver;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxRiverAlphaDet;
        private System.Windows.Forms.Label label4;
    }
}