﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    public partial class GenerateEntityPanel : Form
    {
        public GenerateEntityPanel()
        {
            InitializeComponent();
        }

        private void GenerateEntityPanel_Load(object sender, EventArgs e)
        {
            // 获取 palette 列表
            MPalette[] palettes = MPaletteManager.Instance.GetPalettes();
            foreach (MPalette palette in palettes)
            {
                m_comboBoxPalette.Items.Add(palette.Name);
            }

        }

        private void m_comboBoxPalette_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 获取所选择 palette 的所有 template
            if (m_comboBoxPalette.SelectedIndex != -1)
            {
                String strName = m_comboBoxPalette.SelectedItem.ToString();
                MPalette palette = MPaletteManager.Instance.GetPaletteByName(strName);
                MEntity[] entities = palette.GetEntities();
                
                // 刷新 template list
                m_listBoxAllTemplates.Items.Clear();
                foreach (MEntity entity in entities)
                {
                    m_listBoxAllTemplates.Items.Add(entity.Name);
                }
            }
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            // 添加 template
            if (m_listBoxAllTemplates.SelectedIndex != -1)
            {
                String strSelectedTemplate = m_listBoxAllTemplates.SelectedItem.ToString();
                if (!m_listBoxSelectedTemplates.Items.Contains(strSelectedTemplate))
                {
                    float fRate = 0.0f;
                    try
                    {
                        fRate = float.Parse(m_textBoxRate.Text);
                    }
                    catch(Exception e2)
                    {}
                    m_listBoxSelectedTemplates.Items.Add(strSelectedTemplate);
                    m_listBoxRate.Items.Add(fRate.ToString());
                }
            }
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            int iIdx = m_listBoxSelectedTemplates.SelectedIndex;
            if (iIdx >= 0)
            {
                m_listBoxSelectedTemplates.Items.RemoveAt(iIdx);
                m_listBoxRate.Items.RemoveAt(iIdx);
            }
        }

        private void m_listBoxSelectedTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_listBoxRate.SelectedIndex = m_listBoxSelectedTemplates.SelectedIndex;
        }

        private void m_btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                // 预览区域生长的模型
                ArrayList entityList = new ArrayList();
                ArrayList rateList = new ArrayList();
                for (int i=0; i<m_listBoxSelectedTemplates.Items.Count; i++)
                {
                    entityList.Add(m_listBoxSelectedTemplates.Items[i]);
                    rateList.Add(m_listBoxRate.Items[i]);
                }
                float fBeginRota = float.Parse(m_textBoxRandRotaBegin.Text);
                float fEndRota = float.Parse(m_textBoxRandRotaEnd.Text);
                float fBeginScale = float.Parse(m_textBoxRandScaleBegin.Text);
                float fEndScale = float.Parse(m_textBoxRandScaleEnd.Text);


                MTerrain.Instance.PreviewGridBaseRegionGenerateModel(entityList, rateList, fBeginRota, fEndRota, fBeginScale, fEndScale);
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void m_btnGenerate_Click(object sender, EventArgs e)
        {
            MTerrain.Instance.ApplyGridBaseRegionGeneratedModel();
        }

        private void m_listBoxAllTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_listBoxAllTemplates.SelectedItem != null)
            {
                String strEntityName = m_listBoxAllTemplates.SelectedItem as String;
                MEntity entity = MFramework.Instance.PaletteManager.GetEntity(strEntityName);
                if (entity != null)
                {
                    MTemplatePreviewer.Instance.SetScene(entity);
                }
            }
        }

        // 修改获取palette方式 [9/28/2009]
        private void btn_GetSelPals_Click(object sender, EventArgs e)
        {
            MEntity selEntity = MFramework.Instance.PaletteManager.ActivePalette.ActiveEntity;
            if (!m_listBoxAllTemplates.Items.Contains(selEntity.Name))
            {
                m_listBoxAllTemplates.Items.Add(selEntity.Name);
            }
        }

        private void m_listBoxAllTemplates_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyData)
            {
                case Keys.Delete:
                {
                    int iSelIndex = m_listBoxAllTemplates.SelectedIndex;
                    if(iSelIndex >= 0)
                    {
                        m_listBoxAllTemplates.Items.RemoveAt(iSelIndex);
                    }
                    break;
                }
            }
        }

        private void btn_ClearList_Click(object sender, EventArgs e)
        {
           //for(int i = 0; i < m_listBoxAllTemplates.Items.Count; i++)
           //{
           //    m_listBoxAllTemplates.SetSelected(i, false);
           //}
            m_listBoxAllTemplates.Items.Clear();
         }


    }
}