﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    partial class EntityAffectLightsPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnGetSelectedEntity = new System.Windows.Forms.Button();
            this.m_textBoxSelectedEntity = new System.Windows.Forms.TextBox();
            this.m_listBoxAffectList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_listBoxAllLights = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_btnRemove = new System.Windows.Forms.Button();
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_btnAddSceneSelectedLight = new System.Windows.Forms.Button();
            this.m_btnRemoveSceneSelectedLight = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_btnGetSelectedEntity
            // 
            this.m_btnGetSelectedEntity.Location = new System.Drawing.Point(22, 22);
            this.m_btnGetSelectedEntity.Name = "m_btnGetSelectedEntity";
            this.m_btnGetSelectedEntity.Size = new System.Drawing.Size(121, 24);
            this.m_btnGetSelectedEntity.TabIndex = 0;
            this.m_btnGetSelectedEntity.Text = "获取物件";
            this.m_btnGetSelectedEntity.UseVisualStyleBackColor = true;
            this.m_btnGetSelectedEntity.Click += new System.EventHandler(this.m_btnGetSelectedEntity_Click);
            // 
            // m_textBoxSelectedEntity
            // 
            this.m_textBoxSelectedEntity.Location = new System.Drawing.Point(162, 23);
            this.m_textBoxSelectedEntity.Name = "m_textBoxSelectedEntity";
            this.m_textBoxSelectedEntity.Size = new System.Drawing.Size(167, 21);
            this.m_textBoxSelectedEntity.TabIndex = 1;
            // 
            // m_listBoxAffectList
            // 
            this.m_listBoxAffectList.FormattingEnabled = true;
            this.m_listBoxAffectList.ItemHeight = 12;
            this.m_listBoxAffectList.Location = new System.Drawing.Point(26, 78);
            this.m_listBoxAffectList.Name = "m_listBoxAffectList";
            this.m_listBoxAffectList.Size = new System.Drawing.Size(305, 232);
            this.m_listBoxAffectList.TabIndex = 2;
            this.m_listBoxAffectList.SelectedIndexChanged += new System.EventHandler(this.m_listBoxAffectList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "影响该物件的灯光";
            // 
            // m_listBoxAllLights
            // 
            this.m_listBoxAllLights.FormattingEnabled = true;
            this.m_listBoxAllLights.ItemHeight = 12;
            this.m_listBoxAllLights.Location = new System.Drawing.Point(371, 78);
            this.m_listBoxAllLights.Name = "m_listBoxAllLights";
            this.m_listBoxAllLights.Size = new System.Drawing.Size(305, 232);
            this.m_listBoxAllLights.TabIndex = 4;
            this.m_listBoxAllLights.SelectedIndexChanged += new System.EventHandler(this.m_listBoxAllLights_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(369, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "场景中所有灯光";
            // 
            // m_btnRemove
            // 
            this.m_btnRemove.Location = new System.Drawing.Point(334, 133);
            this.m_btnRemove.Name = "m_btnRemove";
            this.m_btnRemove.Size = new System.Drawing.Size(33, 23);
            this.m_btnRemove.TabIndex = 6;
            this.m_btnRemove.Text = "->";
            this.m_btnRemove.UseVisualStyleBackColor = true;
            this.m_btnRemove.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Location = new System.Drawing.Point(334, 168);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(33, 23);
            this.m_btnAdd.TabIndex = 7;
            this.m_btnAdd.Text = "<-";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_btnAddSceneSelectedLight
            // 
            this.m_btnAddSceneSelectedLight.Location = new System.Drawing.Point(44, 317);
            this.m_btnAddSceneSelectedLight.Name = "m_btnAddSceneSelectedLight";
            this.m_btnAddSceneSelectedLight.Size = new System.Drawing.Size(276, 27);
            this.m_btnAddSceneSelectedLight.TabIndex = 8;
            this.m_btnAddSceneSelectedLight.Text = "添加场景选择灯光到影响列表";
            this.m_btnAddSceneSelectedLight.UseVisualStyleBackColor = true;
            this.m_btnAddSceneSelectedLight.Click += new System.EventHandler(this.m_btnAddSceneSelectedLight_Click);
            // 
            // m_btnRemoveSceneSelectedLight
            // 
            this.m_btnRemoveSceneSelectedLight.Location = new System.Drawing.Point(44, 350);
            this.m_btnRemoveSceneSelectedLight.Name = "m_btnRemoveSceneSelectedLight";
            this.m_btnRemoveSceneSelectedLight.Size = new System.Drawing.Size(276, 27);
            this.m_btnRemoveSceneSelectedLight.TabIndex = 9;
            this.m_btnRemoveSceneSelectedLight.Text = "从影响列表移除场景选择灯光";
            this.m_btnRemoveSceneSelectedLight.UseVisualStyleBackColor = true;
            this.m_btnRemoveSceneSelectedLight.Click += new System.EventHandler(this.m_btnRemoveSceneSelectedLight_Click);
            // 
            // EntityAffectLightsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 382);
            this.Controls.Add(this.m_btnRemoveSceneSelectedLight);
            this.Controls.Add(this.m_btnAddSceneSelectedLight);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.m_btnRemove);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_listBoxAllLights);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_listBoxAffectList);
            this.Controls.Add(this.m_textBoxSelectedEntity);
            this.Controls.Add(this.m_btnGetSelectedEntity);
            this.Name = "EntityAffectLightsPanel";
            this.Text = "物件影响灯光编辑";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnGetSelectedEntity;
        private System.Windows.Forms.TextBox m_textBoxSelectedEntity;
        private System.Windows.Forms.ListBox m_listBoxAffectList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox m_listBoxAllLights;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button m_btnRemove;
        private System.Windows.Forms.Button m_btnAdd;
        private Emergent.Gamebryo.SceneDesigner.Framework.MEntity m_targetEntity = null;
        private System.Windows.Forms.Button m_btnAddSceneSelectedLight;
        private System.Windows.Forms.Button m_btnRemoveSceneSelectedLight;
    }
}