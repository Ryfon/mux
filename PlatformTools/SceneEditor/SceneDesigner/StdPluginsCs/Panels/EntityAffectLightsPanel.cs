﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;


namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    public partial class EntityAffectLightsPanel : Form
    {
        public EntityAffectLightsPanel()
        {
            InitializeComponent();
        }

        private void m_btnGetSelectedEntity_Click(object sender, EventArgs e)
        {
            // 获取当前被选择的物件
            MEntity[] aEntities = MFramework.SelectionService.GetSelectedEntities();
            if (aEntities.Length == 0)
            {
                return;
            }

            m_listBoxAffectList.Items.Clear();
            m_listBoxAllLights.Items.Clear();

            // 
            m_targetEntity = aEntities[0];
            m_textBoxSelectedEntity.Text = m_targetEntity.Name;
            ArrayList aAffectLights = MLightManager.Instance.GetAffectLights(aEntities[0]);

            if (aAffectLights == null) 
            {
                return;
            }

            foreach (MEntity pmEntity in aAffectLights)
            {
                m_listBoxAffectList.Items.Add(pmEntity.Name);
            }

            MEntity[] aAllLights = MLightManager.Instance.GetSceneLights();
            foreach (MEntity mEntity in aAllLights)
            {
                m_listBoxAllLights.Items.Add(mEntity.Name);
            }
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            if (m_targetEntity==null || m_listBoxAffectList.SelectedIndex==-1)
            {
                return;
            }

            MLightManager.Instance.SetAffect(m_listBoxAffectList.SelectedItem.ToString(), m_targetEntity, false);
            m_listBoxAffectList.Items.RemoveAt(m_listBoxAffectList.SelectedIndex);
            MFramework.Instance.Update();
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            if (m_targetEntity == null || m_listBoxAllLights.SelectedIndex == -1)
            {
                return;
            }

            if (!m_listBoxAffectList.Items.Contains(m_listBoxAllLights.SelectedItem))
            {
                MLightManager.Instance.SetAffect(m_listBoxAllLights.SelectedItem.ToString(), m_targetEntity, true);
                m_listBoxAffectList.Items.Add(m_listBoxAllLights.SelectedItem);
            }
            MFramework.Instance.Update();
        }

        private void m_btnAddSceneSelectedLight_Click(object sender, EventArgs e)
        {
            // 添加场景中被选择的灯光到影响列表
            if (m_targetEntity == null)
            {
                return;
            }

            ISelectionService selectionService = ServiceProvider.Instance
                       .GetService(typeof(ISelectionService)) as
                       ISelectionService;

            MEntity[] aEntities = selectionService.GetSelectedEntities();
            if (aEntities.Length > 0)
            {
                MEntity entity = aEntities[0];
                if (MLightManager.EntityIsLight(entity))
                {
                    if (!m_listBoxAffectList.Items.Contains(entity.Name))
                    {
                        MLightManager.Instance.SetAffect(entity.Name, m_targetEntity, true);
                        m_listBoxAffectList.Items.Add(entity.Name);
                    }
                    MFramework.Instance.Update();
                }
            }

        }


        private void m_btnRemoveSceneSelectedLight_Click(object sender, EventArgs e)
        {
            // 将场景中选择的灯光从影响列表中移除
            if (m_targetEntity == null)
            {
                return;
            }

            ISelectionService selectionService = ServiceProvider.Instance
                       .GetService(typeof(ISelectionService)) as
                       ISelectionService;

            MEntity[] aEntities = selectionService.GetSelectedEntities();
            if (aEntities.Length > 0)
            {
                MEntity entity = aEntities[0];
                if (MLightManager.EntityIsLight(entity))
                {
                    if (m_listBoxAffectList.Items.Contains(entity.Name))
                    {
                        MLightManager.Instance.SetAffect(entity.Name, m_targetEntity, false);
                        m_listBoxAffectList.Items.Remove(entity.Name);
                    }
                    MFramework.Instance.Update();
                }
            }
        }

        private void m_listBoxAffectList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_listBoxAffectList.SelectedIndex == -1)
            {
                return;
            }

            ISelectionService selectionService = ServiceProvider.Instance
                       .GetService(typeof(ISelectionService)) as
                       ISelectionService;
            selectionService.ClearSelectedEntities();
            MEntity pmEntity = MLightManager.Instance.GetLight(m_listBoxAffectList.SelectedItem.ToString());
            if (pmEntity != null)
            {
                selectionService.AddEntityToSelection(pmEntity);
            }
            MFramework.Instance.Update();
        }

        private void m_listBoxAllLights_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_listBoxAllLights.SelectedIndex == -1)
            {
                return;
            }

            ISelectionService selectionService = ServiceProvider.Instance
                       .GetService(typeof(ISelectionService)) as
                       ISelectionService;
            selectionService.ClearSelectedEntities();
            MEntity pmEntity = MLightManager.Instance.GetLight(m_listBoxAllLights.SelectedItem.ToString());
            if (pmEntity != null)
            {
                selectionService.AddEntityToSelection(pmEntity);
            }
            MFramework.Instance.Update();
        }



    }
}