﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class FogPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_pictureBoxColor = new System.Windows.Forms.PictureBox();
            this.m_btnSelectColor = new System.Windows.Forms.Button();
            this.m_textBoxStartZ = new System.Windows.Forms.TextBox();
            this.m_textBoxEndZ = new System.Windows.Forms.TextBox();
            this.m_textBoxLeft = new System.Windows.Forms.TextBox();
            this.m_textBoxRight = new System.Windows.Forms.TextBox();
            this.m_textBoxTop = new System.Windows.Forms.TextBox();
            this.m_textBoxBottom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.m_btnTexSize64 = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.m_btnTexSize128 = new System.Windows.Forms.RadioButton();
            this.m_btnTexSize256 = new System.Windows.Forms.RadioButton();
            this.m_btnTexSize2048 = new System.Windows.Forms.RadioButton();
            this.m_btnTexSize1024 = new System.Windows.Forms.RadioButton();
            this.m_btnTexSize512 = new System.Windows.Forms.RadioButton();
            this.m_btnSelectFogTextureFile = new System.Windows.Forms.Button();
            this.m_btnSelectFogSurfaceFile = new System.Windows.Forms.Button();
            this.m_btnGenerateFog = new System.Windows.Forms.Button();
            this.m_btnClear = new System.Windows.Forms.Button();
            this.m_checkBoxPreview = new System.Windows.Forms.CheckBox();
            this.m_btnSave = new System.Windows.Forms.Button();
            this.m_textBoxTexFile = new System.Windows.Forms.TextBox();
            this.m_textBoxSurfaceFile = new System.Windows.Forms.TextBox();
            this.m_textBoxAnimTexFile = new System.Windows.Forms.TextBox();
            this.m_btnSelectAnimTexture = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.m_textBoxSpeedU = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxColor)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "颜色";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "起始高度";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "终止高度";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "范围";
            // 
            // m_pictureBoxColor
            // 
            this.m_pictureBoxColor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_pictureBoxColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxColor.Location = new System.Drawing.Point(145, 22);
            this.m_pictureBoxColor.Name = "m_pictureBoxColor";
            this.m_pictureBoxColor.Size = new System.Drawing.Size(46, 22);
            this.m_pictureBoxColor.TabIndex = 4;
            this.m_pictureBoxColor.TabStop = false;
            // 
            // m_btnSelectColor
            // 
            this.m_btnSelectColor.Location = new System.Drawing.Point(86, 24);
            this.m_btnSelectColor.Name = "m_btnSelectColor";
            this.m_btnSelectColor.Size = new System.Drawing.Size(32, 19);
            this.m_btnSelectColor.TabIndex = 5;
            this.m_btnSelectColor.Text = "...";
            this.m_btnSelectColor.UseVisualStyleBackColor = true;
            this.m_btnSelectColor.Click += new System.EventHandler(this.m_btnSelectColor_Click);
            // 
            // m_textBoxStartZ
            // 
            this.m_textBoxStartZ.Location = new System.Drawing.Point(85, 48);
            this.m_textBoxStartZ.Name = "m_textBoxStartZ";
            this.m_textBoxStartZ.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxStartZ.TabIndex = 6;
            this.m_textBoxStartZ.Text = "2.5";
            // 
            // m_textBoxEndZ
            // 
            this.m_textBoxEndZ.Location = new System.Drawing.Point(85, 74);
            this.m_textBoxEndZ.Name = "m_textBoxEndZ";
            this.m_textBoxEndZ.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxEndZ.TabIndex = 7;
            this.m_textBoxEndZ.Text = "-10";
            // 
            // m_textBoxLeft
            // 
            this.m_textBoxLeft.Location = new System.Drawing.Point(74, 122);
            this.m_textBoxLeft.Name = "m_textBoxLeft";
            this.m_textBoxLeft.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxLeft.TabIndex = 8;
            this.m_textBoxLeft.Text = "0";
            // 
            // m_textBoxRight
            // 
            this.m_textBoxRight.Location = new System.Drawing.Point(122, 122);
            this.m_textBoxRight.Name = "m_textBoxRight";
            this.m_textBoxRight.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxRight.TabIndex = 9;
            this.m_textBoxRight.Text = "64";
            // 
            // m_textBoxTop
            // 
            this.m_textBoxTop.Location = new System.Drawing.Point(170, 122);
            this.m_textBoxTop.Name = "m_textBoxTop";
            this.m_textBoxTop.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxTop.TabIndex = 10;
            this.m_textBoxTop.Text = "64";
            // 
            // m_textBoxBottom
            // 
            this.m_textBoxBottom.Location = new System.Drawing.Point(218, 122);
            this.m_textBoxBottom.Name = "m_textBoxBottom";
            this.m_textBoxBottom.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxBottom.TabIndex = 11;
            this.m_textBoxBottom.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(83, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "左";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(131, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "右";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(179, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "上";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(227, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "下";
            // 
            // m_btnTexSize64
            // 
            this.m_btnTexSize64.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_btnTexSize64.AutoSize = true;
            this.m_btnTexSize64.Location = new System.Drawing.Point(24, 179);
            this.m_btnTexSize64.Name = "m_btnTexSize64";
            this.m_btnTexSize64.Size = new System.Drawing.Size(27, 22);
            this.m_btnTexSize64.TabIndex = 17;
            this.m_btnTexSize64.Text = "64";
            this.m_btnTexSize64.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 12);
            this.label9.TabIndex = 18;
            this.label9.Text = "alpha 纹理尺寸";
            // 
            // m_btnTexSize128
            // 
            this.m_btnTexSize128.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_btnTexSize128.AutoSize = true;
            this.m_btnTexSize128.Location = new System.Drawing.Point(59, 179);
            this.m_btnTexSize128.Name = "m_btnTexSize128";
            this.m_btnTexSize128.Size = new System.Drawing.Size(33, 22);
            this.m_btnTexSize128.TabIndex = 19;
            this.m_btnTexSize128.Text = "128";
            this.m_btnTexSize128.UseVisualStyleBackColor = true;
            // 
            // m_btnTexSize256
            // 
            this.m_btnTexSize256.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_btnTexSize256.AutoSize = true;
            this.m_btnTexSize256.Location = new System.Drawing.Point(100, 179);
            this.m_btnTexSize256.Name = "m_btnTexSize256";
            this.m_btnTexSize256.Size = new System.Drawing.Size(33, 22);
            this.m_btnTexSize256.TabIndex = 20;
            this.m_btnTexSize256.Text = "256";
            this.m_btnTexSize256.UseVisualStyleBackColor = true;
            // 
            // m_btnTexSize2048
            // 
            this.m_btnTexSize2048.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_btnTexSize2048.AutoSize = true;
            this.m_btnTexSize2048.Location = new System.Drawing.Point(229, 179);
            this.m_btnTexSize2048.Name = "m_btnTexSize2048";
            this.m_btnTexSize2048.Size = new System.Drawing.Size(39, 22);
            this.m_btnTexSize2048.TabIndex = 23;
            this.m_btnTexSize2048.Text = "2048";
            this.m_btnTexSize2048.UseVisualStyleBackColor = true;
            // 
            // m_btnTexSize1024
            // 
            this.m_btnTexSize1024.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_btnTexSize1024.AutoSize = true;
            this.m_btnTexSize1024.Location = new System.Drawing.Point(182, 179);
            this.m_btnTexSize1024.Name = "m_btnTexSize1024";
            this.m_btnTexSize1024.Size = new System.Drawing.Size(39, 22);
            this.m_btnTexSize1024.TabIndex = 22;
            this.m_btnTexSize1024.Text = "1024";
            this.m_btnTexSize1024.UseVisualStyleBackColor = true;
            // 
            // m_btnTexSize512
            // 
            this.m_btnTexSize512.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_btnTexSize512.AutoSize = true;
            this.m_btnTexSize512.Checked = true;
            this.m_btnTexSize512.Location = new System.Drawing.Point(141, 179);
            this.m_btnTexSize512.Name = "m_btnTexSize512";
            this.m_btnTexSize512.Size = new System.Drawing.Size(33, 22);
            this.m_btnTexSize512.TabIndex = 21;
            this.m_btnTexSize512.TabStop = true;
            this.m_btnTexSize512.Text = "512";
            this.m_btnTexSize512.UseVisualStyleBackColor = true;
            // 
            // m_btnSelectFogTextureFile
            // 
            this.m_btnSelectFogTextureFile.Location = new System.Drawing.Point(9, 315);
            this.m_btnSelectFogTextureFile.Name = "m_btnSelectFogTextureFile";
            this.m_btnSelectFogTextureFile.Size = new System.Drawing.Size(116, 28);
            this.m_btnSelectFogTextureFile.TabIndex = 25;
            this.m_btnSelectFogTextureFile.Text = "选择纹理保存文件";
            this.m_btnSelectFogTextureFile.UseVisualStyleBackColor = true;
            this.m_btnSelectFogTextureFile.Click += new System.EventHandler(this.m_btnSelectFogTextureFile_Click);
            // 
            // m_btnSelectFogSurfaceFile
            // 
            this.m_btnSelectFogSurfaceFile.Location = new System.Drawing.Point(9, 347);
            this.m_btnSelectFogSurfaceFile.Name = "m_btnSelectFogSurfaceFile";
            this.m_btnSelectFogSurfaceFile.Size = new System.Drawing.Size(116, 28);
            this.m_btnSelectFogSurfaceFile.TabIndex = 26;
            this.m_btnSelectFogSurfaceFile.Text = "选择面片保存文件";
            this.m_btnSelectFogSurfaceFile.UseVisualStyleBackColor = true;
            this.m_btnSelectFogSurfaceFile.Click += new System.EventHandler(this.m_btnSelectFogSurfaceFile_Click);
            // 
            // m_btnGenerateFog
            // 
            this.m_btnGenerateFog.Location = new System.Drawing.Point(59, 277);
            this.m_btnGenerateFog.Name = "m_btnGenerateFog";
            this.m_btnGenerateFog.Size = new System.Drawing.Size(151, 28);
            this.m_btnGenerateFog.TabIndex = 28;
            this.m_btnGenerateFog.Text = "生成层雾";
            this.m_btnGenerateFog.UseVisualStyleBackColor = true;
            this.m_btnGenerateFog.Click += new System.EventHandler(this.m_btnGenerateFog_Click);
            // 
            // m_btnClear
            // 
            this.m_btnClear.Location = new System.Drawing.Point(229, 388);
            this.m_btnClear.Name = "m_btnClear";
            this.m_btnClear.Size = new System.Drawing.Size(69, 19);
            this.m_btnClear.TabIndex = 29;
            this.m_btnClear.Text = "清理";
            this.m_btnClear.UseVisualStyleBackColor = true;
            this.m_btnClear.Click += new System.EventHandler(this.m_btnClear_Click);
            // 
            // m_checkBoxPreview
            // 
            this.m_checkBoxPreview.AutoSize = true;
            this.m_checkBoxPreview.Location = new System.Drawing.Point(229, 284);
            this.m_checkBoxPreview.Name = "m_checkBoxPreview";
            this.m_checkBoxPreview.Size = new System.Drawing.Size(48, 16);
            this.m_checkBoxPreview.TabIndex = 30;
            this.m_checkBoxPreview.Text = "预览";
            this.m_checkBoxPreview.UseVisualStyleBackColor = true;
            this.m_checkBoxPreview.CheckedChanged += new System.EventHandler(this.m_checkBoxPreview_CheckedChanged);
            // 
            // m_btnSave
            // 
            this.m_btnSave.Location = new System.Drawing.Point(9, 379);
            this.m_btnSave.Name = "m_btnSave";
            this.m_btnSave.Size = new System.Drawing.Size(116, 28);
            this.m_btnSave.TabIndex = 31;
            this.m_btnSave.Text = "保存";
            this.m_btnSave.UseVisualStyleBackColor = true;
            this.m_btnSave.Click += new System.EventHandler(this.m_btnSave_Click);
            // 
            // m_textBoxTexFile
            // 
            this.m_textBoxTexFile.Location = new System.Drawing.Point(135, 322);
            this.m_textBoxTexFile.Name = "m_textBoxTexFile";
            this.m_textBoxTexFile.Size = new System.Drawing.Size(155, 21);
            this.m_textBoxTexFile.TabIndex = 32;
            // 
            // m_textBoxSurfaceFile
            // 
            this.m_textBoxSurfaceFile.Location = new System.Drawing.Point(135, 352);
            this.m_textBoxSurfaceFile.Name = "m_textBoxSurfaceFile";
            this.m_textBoxSurfaceFile.Size = new System.Drawing.Size(155, 21);
            this.m_textBoxSurfaceFile.TabIndex = 33;
            // 
            // m_textBoxAnimTexFile
            // 
            this.m_textBoxAnimTexFile.Location = new System.Drawing.Point(135, 221);
            this.m_textBoxAnimTexFile.Name = "m_textBoxAnimTexFile";
            this.m_textBoxAnimTexFile.Size = new System.Drawing.Size(155, 21);
            this.m_textBoxAnimTexFile.TabIndex = 35;
            // 
            // m_btnSelectAnimTexture
            // 
            this.m_btnSelectAnimTexture.Location = new System.Drawing.Point(9, 214);
            this.m_btnSelectAnimTexture.Name = "m_btnSelectAnimTexture";
            this.m_btnSelectAnimTexture.Size = new System.Drawing.Size(116, 28);
            this.m_btnSelectAnimTexture.TabIndex = 34;
            this.m_btnSelectAnimTexture.Text = "选择动画纹理文件";
            this.m_btnSelectAnimTexture.UseVisualStyleBackColor = true;
            this.m_btnSelectAnimTexture.Click += new System.EventHandler(this.m_btnSelectAnimTexture_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 251);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 36;
            this.label10.Text = "UV动画速度";
            // 
            // m_textBoxSpeedU
            // 
            this.m_textBoxSpeedU.Location = new System.Drawing.Point(91, 248);
            this.m_textBoxSpeedU.Name = "m_textBoxSpeedU";
            this.m_textBoxSpeedU.Size = new System.Drawing.Size(48, 21);
            this.m_textBoxSpeedU.TabIndex = 37;
            this.m_textBoxSpeedU.Text = "1";
            // 
            // FogPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 419);
            this.Controls.Add(this.m_textBoxSpeedU);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.m_textBoxAnimTexFile);
            this.Controls.Add(this.m_btnSelectAnimTexture);
            this.Controls.Add(this.m_textBoxSurfaceFile);
            this.Controls.Add(this.m_textBoxTexFile);
            this.Controls.Add(this.m_btnSave);
            this.Controls.Add(this.m_checkBoxPreview);
            this.Controls.Add(this.m_btnClear);
            this.Controls.Add(this.m_btnGenerateFog);
            this.Controls.Add(this.m_btnSelectFogSurfaceFile);
            this.Controls.Add(this.m_btnSelectFogTextureFile);
            this.Controls.Add(this.m_btnTexSize2048);
            this.Controls.Add(this.m_btnTexSize1024);
            this.Controls.Add(this.m_btnTexSize512);
            this.Controls.Add(this.m_btnTexSize256);
            this.Controls.Add(this.m_btnTexSize128);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.m_btnTexSize64);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_textBoxBottom);
            this.Controls.Add(this.m_textBoxTop);
            this.Controls.Add(this.m_textBoxRight);
            this.Controls.Add(this.m_textBoxLeft);
            this.Controls.Add(this.m_textBoxEndZ);
            this.Controls.Add(this.m_textBoxStartZ);
            this.Controls.Add(this.m_btnSelectColor);
            this.Controls.Add(this.m_pictureBoxColor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FogPanel";
            this.Text = "层雾工具";
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox m_pictureBoxColor;
        private System.Windows.Forms.Button m_btnSelectColor;
        private System.Windows.Forms.TextBox m_textBoxStartZ;
        private System.Windows.Forms.TextBox m_textBoxEndZ;
        private System.Windows.Forms.TextBox m_textBoxLeft;
        private System.Windows.Forms.TextBox m_textBoxRight;
        private System.Windows.Forms.TextBox m_textBoxTop;
        private System.Windows.Forms.TextBox m_textBoxBottom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton m_btnTexSize64;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton m_btnTexSize128;
        private System.Windows.Forms.RadioButton m_btnTexSize256;
        private System.Windows.Forms.RadioButton m_btnTexSize2048;
        private System.Windows.Forms.RadioButton m_btnTexSize1024;
        private System.Windows.Forms.RadioButton m_btnTexSize512;
        private System.Windows.Forms.Button m_btnSelectFogTextureFile;
        private System.Windows.Forms.Button m_btnSelectFogSurfaceFile;
        private System.Windows.Forms.Button m_btnGenerateFog;
        private System.Windows.Forms.Button m_btnClear;
        private System.Windows.Forms.CheckBox m_checkBoxPreview;
        private System.Windows.Forms.Button m_btnSave;
        private System.Windows.Forms.TextBox m_textBoxTexFile;
        private System.Windows.Forms.TextBox m_textBoxSurfaceFile;
        private System.Windows.Forms.TextBox m_textBoxAnimTexFile;
        private System.Windows.Forms.Button m_btnSelectAnimTexture;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox m_textBoxSpeedU;
    }
}