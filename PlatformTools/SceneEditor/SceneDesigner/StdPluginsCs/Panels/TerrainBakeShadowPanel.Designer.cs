﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class TerrainBakeShadowPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnExportBakeScene = new System.Windows.Forms.Button();
            this.m_btnImprotBakeScene = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_btnExportBakeScene
            // 
            this.m_btnExportBakeScene.Location = new System.Drawing.Point(67, 12);
            this.m_btnExportBakeScene.Name = "m_btnExportBakeScene";
            this.m_btnExportBakeScene.Size = new System.Drawing.Size(139, 22);
            this.m_btnExportBakeScene.TabIndex = 2;
            this.m_btnExportBakeScene.Text = "导出场景到3ds文件";
            this.m_btnExportBakeScene.UseVisualStyleBackColor = true;
            this.m_btnExportBakeScene.Click += new System.EventHandler(this.m_btnExportBakeScene_Click);
            // 
            // m_btnImprotBakeScene
            // 
            this.m_btnImprotBakeScene.Location = new System.Drawing.Point(67, 40);
            this.m_btnImprotBakeScene.Name = "m_btnImprotBakeScene";
            this.m_btnImprotBakeScene.Size = new System.Drawing.Size(139, 23);
            this.m_btnImprotBakeScene.TabIndex = 3;
            this.m_btnImprotBakeScene.Text = "导入烘焙数据";
            this.m_btnImprotBakeScene.UseVisualStyleBackColor = true;
            this.m_btnImprotBakeScene.Click += new System.EventHandler(this.m_btnImprotBakeScene_Click);
            // 
            // TerrainBakeShadowPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 88);
            this.Controls.Add(this.m_btnImprotBakeScene);
            this.Controls.Add(this.m_btnExportBakeScene);
            this.Name = "TerrainBakeShadowPanel";
            this.Text = "烘焙灯光到场景";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button m_btnExportBakeScene;
        private System.Windows.Forms.Button m_btnImprotBakeScene;
    }
}