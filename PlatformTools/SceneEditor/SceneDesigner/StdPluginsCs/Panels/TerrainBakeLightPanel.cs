﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class TerrainBakeLightPanel : Form
    {
        public TerrainBakeLightPanel()
        {
            InitializeComponent();
        }

        private void TerrainBakeLightPanel_Load(object sender, EventArgs e)
        {
            MEntity[] lights = MLightManager.Instance.GetSceneLights();
           // m_listBoxAllLights.Items.Clear();

            foreach (MEntity entity in lights)
            {
                m_listBoxAllLights.Items.Add(entity.Name);
            }
        }

        private void m_btnAddLight_Click(object sender, EventArgs e)
        {
            if (m_listBoxAllLights.SelectedItems.Count == 0)
            {
                return;
            }

            foreach (String strLight in m_listBoxAllLights.SelectedItems)
            {
                if (!m_listBoxSelectedLights.Items.Contains(strLight))
                {
                    m_listBoxSelectedLights.Items.Add(strLight);
                }
            }

        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            ArrayList removeList = new ArrayList();

            foreach (String strLight in m_listBoxSelectedLights.SelectedItems)
            {
                removeList.Add(strLight);

            }

            foreach (String strLight in removeList)
            {
                m_listBoxSelectedLights.Items.Remove(strLight);
            }
        }

        private void m_btnPreview_Click(object sender, EventArgs e)
        {
            String[] strLightList = new String[m_listBoxSelectedLights.Items.Count];

            for (int i = 0; i < m_listBoxSelectedLights.Items.Count; i++)
            {
                strLightList[i] = m_listBoxSelectedLights.Items[i].ToString();
            }
            MFramework.Instance.Scene.PreviewLights(strLightList);
        }

        private void m_btnBake_Click(object sender, EventArgs e)
        {
            // shi yazheng [11/10/2009] 一律使用相加方法烘焙灯光到顶点色
            bool bMultiply = false;
            //if (m_radioButtonMultiply.Checked)
            //{
            //}
            //else if(m_radioButtonAdd.Checked)
            //{
            //    bMultiply = false;
            //}

            String[] strLightList = new String[m_listBoxSelectedLights.Items.Count];

            for (int i = 0; i < m_listBoxSelectedLights.Items.Count; i++)
            {
                strLightList[i] = m_listBoxSelectedLights.Items[i].ToString();
            }
            MFramework.Instance.Scene.BakeLightsToTerrain(strLightList, bMultiply);
        }

        private void m_pictureBoxColor_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.ShowDialog();
            m_pictureBoxColor.BackColor = dlg.Color;

            m_textBoxR.Text = (dlg.Color.R / 255.0f).ToString();
            m_textBoxG.Text = (dlg.Color.G / 255.0f).ToString();
            m_textBoxB.Text = (dlg.Color.B / 255.0f).ToString();
        }

        private void m_btnFlood_Click(object sender, EventArgs e)
        {
            try
            {
                float fR = float.Parse(m_textBoxR.Text);
                float fG = float.Parse(m_textBoxG.Text);
                float fB = float.Parse(m_textBoxB.Text);

                MFramework.Instance.Scene.FloodTerrainVertexColor(fR, fG, fB);
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void m_btnAllLightSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < m_listBoxAllLights.Items.Count; i++)
            {
                m_listBoxAllLights.SetSelected(i, true);

            }
            m_listBoxAllLights.Update();
        }

        private void m_btnAllLightClear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < m_listBoxAllLights.Items.Count; i++)
            {
                m_listBoxAllLights.SetSelected(i, false);

            }
            m_listBoxAllLights.Update();
        }

        private void m_btnSelLightSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < m_listBoxSelectedLights.Items.Count; i++)
            {
                m_listBoxSelectedLights.SetSelected(i, true);

            }
            m_listBoxSelectedLights.Update();
        }

        private void m_btnSelLightClear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < m_listBoxSelectedLights.Items.Count; i++)
            {
                m_listBoxSelectedLights.SetSelected(i, false);

            }
            m_listBoxSelectedLights.Update();
        }

        // 全场景灯光烘焙 [10/29/2009]
        private void button1_Click(object sender, EventArgs e)
        {
           // 首先填充顶点 [10/29/2009]
            MFramework.Instance.Scene.FloodTerrainVertexColor(0.0f, 0.0f, 0.0f);

            MEntity[] lights = MLightManager.Instance.GetSceneLights();
            int iLightsCount = MLightManager.Instance.GetSceneLightsCount();

            ArrayList vLights = new ArrayList();

            for (int i = 0; i < iLightsCount; i++)
            {
               String strLightName = lights[i].Name;

               // 除去preview和character_sun [10/29/2009]
               if(strLightName.Equals("preview") || strLightName.Equals("character_sun") || strLightName.Equals("sun") || strLightName.Equals("character_ambient"))
               {
                   continue;
               }

                // constant light也不烘焙至地面 [10/29/2009]
               if (strLightName.Contains("constant"))
               {
                   continue;
               }

               vLights.Add(strLightName);
            }

            bool bMultiply = true;
            if (m_radioButtonMultiply.Checked)
            {
            }
            else if (m_radioButtonAdd.Checked)
            {
                bMultiply = false;
            }

            String[] pLightList = (String[])vLights.ToArray(typeof(String));
           
            MFramework.Instance.Scene.BakeLightsToTerrain(pLightList, bMultiply);

            String[] strLightList = new String[1];
            // 已去除preview灯光，改用sun预览 [11/18/2009]
            strLightList[0] = "sun";
            MFramework.Instance.Scene.PreviewLights(strLightList);
        }

        private void btn_Preview_By_preview_Click(object sender, EventArgs e)
        {
            String[] strLightList = new String[1];
            strLightList[0] = "preview";
            MFramework.Instance.Scene.PreviewLights(strLightList);
        }

        private void btn_Preview_By_sun_Click(object sender, EventArgs e)
        {
            String[] strLightList = new String[1];
            strLightList[0] = "sun";
            MFramework.Instance.Scene.PreviewLights(strLightList);
        }

        private void m_btnOneBtnPreview_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.FloodTerrainVertexColor(0.0f, 0.0f, 0.0f);

            MEntity[] lights = MLightManager.Instance.GetSceneLights();
            int iLightsCount = MLightManager.Instance.GetSceneLightsCount();

            ArrayList vLights = new ArrayList();

            for (int i = 0; i < iLightsCount; i++)
            {
                String strLightName = lights[i].Name;

                // 除去preview和character_sun [10/29/2009]
                if (strLightName.Equals("character_sun"))
                {
                    continue;
                }             

                vLights.Add(strLightName);
            }
            
            String[] pLightList = (String[])vLights.ToArray(typeof(String));

            MFramework.Instance.Scene.PreviewLights(pLightList);
        }
    }
}