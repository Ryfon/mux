﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class ChunkEntityManagePanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_radioButtonCameraChunk = new System.Windows.Forms.RadioButton();
            this.m_radioButton9CameraChunk = new System.Windows.Forms.RadioButton();
            this.m_radioButtonStaticCulled = new System.Windows.Forms.RadioButton();
            this.m_listBoxAllChunks = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_btnRemove = new System.Windows.Forms.Button();
            this.m_listBoxSelectedChunks = new System.Windows.Forms.ListBox();
            this.m_btnClearAllChunks = new System.Windows.Forms.Button();
            this.m_btnOk = new System.Windows.Forms.Button();
            this.m_radioButtonCullNone = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // m_radioButtonCameraChunk
            // 
            this.m_radioButtonCameraChunk.AutoSize = true;
            this.m_radioButtonCameraChunk.Location = new System.Drawing.Point(14, 32);
            this.m_radioButtonCameraChunk.Name = "m_radioButtonCameraChunk";
            this.m_radioButtonCameraChunk.Size = new System.Drawing.Size(173, 16);
            this.m_radioButtonCameraChunk.TabIndex = 2;
            this.m_radioButtonCameraChunk.TabStop = true;
            this.m_radioButtonCameraChunk.Text = "显示摄象机所在 chunk 物件";
            this.m_radioButtonCameraChunk.UseVisualStyleBackColor = true;
            // 
            // m_radioButton9CameraChunk
            // 
            this.m_radioButton9CameraChunk.AutoSize = true;
            this.m_radioButton9CameraChunk.Location = new System.Drawing.Point(14, 54);
            this.m_radioButton9CameraChunk.Name = "m_radioButton9CameraChunk";
            this.m_radioButton9CameraChunk.Size = new System.Drawing.Size(203, 16);
            this.m_radioButton9CameraChunk.TabIndex = 3;
            this.m_radioButton9CameraChunk.TabStop = true;
            this.m_radioButton9CameraChunk.Text = "显示摄象机周围 9 个 chunk 物件";
            this.m_radioButton9CameraChunk.UseVisualStyleBackColor = true;
            // 
            // m_radioButtonStaticCulled
            // 
            this.m_radioButtonStaticCulled.AutoSize = true;
            this.m_radioButtonStaticCulled.Checked = true;
            this.m_radioButtonStaticCulled.Location = new System.Drawing.Point(15, 78);
            this.m_radioButtonStaticCulled.Name = "m_radioButtonStaticCulled";
            this.m_radioButtonStaticCulled.Size = new System.Drawing.Size(161, 16);
            this.m_radioButtonStaticCulled.TabIndex = 4;
            this.m_radioButtonStaticCulled.TabStop = true;
            this.m_radioButtonStaticCulled.Text = "显示列表中的 chunk 物件";
            this.m_radioButtonStaticCulled.UseVisualStyleBackColor = true;
            // 
            // m_listBoxAllChunks
            // 
            this.m_listBoxAllChunks.FormattingEnabled = true;
            this.m_listBoxAllChunks.ItemHeight = 12;
            this.m_listBoxAllChunks.Location = new System.Drawing.Point(15, 131);
            this.m_listBoxAllChunks.Name = "m_listBoxAllChunks";
            this.m_listBoxAllChunks.Size = new System.Drawing.Size(117, 196);
            this.m_listBoxAllChunks.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(169, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "显示下列 chunk 物件";
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Location = new System.Drawing.Point(138, 179);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(27, 21);
            this.m_btnAdd.TabIndex = 7;
            this.m_btnAdd.Text = "->";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_btnRemove
            // 
            this.m_btnRemove.Location = new System.Drawing.Point(138, 206);
            this.m_btnRemove.Name = "m_btnRemove";
            this.m_btnRemove.Size = new System.Drawing.Size(27, 21);
            this.m_btnRemove.TabIndex = 8;
            this.m_btnRemove.Text = "<-";
            this.m_btnRemove.UseVisualStyleBackColor = true;
            this.m_btnRemove.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // m_listBoxSelectedChunks
            // 
            this.m_listBoxSelectedChunks.FormattingEnabled = true;
            this.m_listBoxSelectedChunks.ItemHeight = 12;
            this.m_listBoxSelectedChunks.Location = new System.Drawing.Point(171, 131);
            this.m_listBoxSelectedChunks.Name = "m_listBoxSelectedChunks";
            this.m_listBoxSelectedChunks.Size = new System.Drawing.Size(117, 196);
            this.m_listBoxSelectedChunks.TabIndex = 9;
            // 
            // m_btnClearAllChunks
            // 
            this.m_btnClearAllChunks.Location = new System.Drawing.Point(294, 306);
            this.m_btnClearAllChunks.Name = "m_btnClearAllChunks";
            this.m_btnClearAllChunks.Size = new System.Drawing.Size(44, 21);
            this.m_btnClearAllChunks.TabIndex = 10;
            this.m_btnClearAllChunks.Text = "clear";
            this.m_btnClearAllChunks.UseVisualStyleBackColor = true;
            this.m_btnClearAllChunks.Click += new System.EventHandler(this.m_btnClearAllChunks_Click);
            // 
            // m_btnOk
            // 
            this.m_btnOk.Location = new System.Drawing.Point(121, 343);
            this.m_btnOk.Name = "m_btnOk";
            this.m_btnOk.Size = new System.Drawing.Size(66, 24);
            this.m_btnOk.TabIndex = 11;
            this.m_btnOk.Text = "确定";
            this.m_btnOk.UseVisualStyleBackColor = true;
            this.m_btnOk.Click += new System.EventHandler(this.m_btnOk_Click);
            // 
            // m_radioButtonCullNone
            // 
            this.m_radioButtonCullNone.AutoSize = true;
            this.m_radioButtonCullNone.Location = new System.Drawing.Point(15, 10);
            this.m_radioButtonCullNone.Name = "m_radioButtonCullNone";
            this.m_radioButtonCullNone.Size = new System.Drawing.Size(95, 16);
            this.m_radioButtonCullNone.TabIndex = 12;
            this.m_radioButtonCullNone.TabStop = true;
            this.m_radioButtonCullNone.Text = "显示全部物件";
            this.m_radioButtonCullNone.UseVisualStyleBackColor = true;
            // 
            // ChunkEntityManagePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 379);
            this.Controls.Add(this.m_radioButtonCullNone);
            this.Controls.Add(this.m_btnOk);
            this.Controls.Add(this.m_btnClearAllChunks);
            this.Controls.Add(this.m_listBoxSelectedChunks);
            this.Controls.Add(this.m_btnRemove);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_listBoxAllChunks);
            this.Controls.Add(this.m_radioButtonStaticCulled);
            this.Controls.Add(this.m_radioButton9CameraChunk);
            this.Controls.Add(this.m_radioButtonCameraChunk);
            this.Name = "ChunkEntityManagePanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chunk Entity 管理";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ChunkEntityManagePanel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private static ChunkEntityManagePanel m_Instance = null;
        private System.Windows.Forms.RadioButton m_radioButtonCameraChunk;
        private System.Windows.Forms.RadioButton m_radioButton9CameraChunk;
        private System.Windows.Forms.RadioButton m_radioButtonStaticCulled;
        private System.Windows.Forms.ListBox m_listBoxAllChunks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button m_btnAdd;
        private System.Windows.Forms.Button m_btnRemove;
        private System.Windows.Forms.ListBox m_listBoxSelectedChunks;
        private System.Windows.Forms.Button m_btnClearAllChunks;
        private System.Windows.Forms.Button m_btnOk;
        private System.Windows.Forms.RadioButton m_radioButtonCullNone;
    }
}