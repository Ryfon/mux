﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class ShadowPanel : Form
    {
        public ShadowPanel()
        {
            InitializeComponent();
        }

        private void InitChunkList()
        {
            m_listBoxAllChunks.Items.Clear();
            int iNumChunksX = MFramework.Instance.Scene.GetTerrainNumChunkX();
            int iNumChunksY = MFramework.Instance.Scene.GetTerrainNumChunkY();

            int iNumChunks = iNumChunksX * iNumChunksY;
            for (int i=0; i<iNumChunks; i++)
            {
                m_listBoxAllChunks.Items.Add(i);
            }
        }

        private void ShadowPanel_Shown(object sender, EventArgs e)
        {
            InitChunkList();
            m_cbViewStaticShadow.Checked = MTerrain.Instance.StaticShadow;
            m_cbViewDynamicShadow.Checked = MTerrain.Instance.DynamicShadow;
        }

        private void ShadowPanel_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void m_btnAddChunkToList_Click(object sender, EventArgs e)
        {
            foreach (Object obj in m_listBoxAllChunks.SelectedItems)
            {
                if (m_listBoxShadowedChunks.Items.Contains(obj))
                {
                    continue;
                }

                m_listBoxShadowedChunks.Items.Add(obj);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int iSelectedIdx = m_listBoxShadowedChunks.SelectedIndex;

            if (iSelectedIdx>=0 && iSelectedIdx<m_listBoxShadowedChunks.Items.Count)
            {
                m_listBoxShadowedChunks.Items.RemoveAt(iSelectedIdx);
            }
        }

        // 生成场景 disk 信息
        private void m_btnGenerateAODisk_Click(object sender, EventArgs e)
        {
           
            m_textBoxMessage.Text = "开始收集场景信息.大场景可能会稍慢,请等一下下...\r\n";
            m_textBoxMessage.Update();
            if (MFramework.Instance.Scene.GenerateAODisk())
            {
                m_textBoxMessage.Text = "场景信息收集完毕.\r\n" + m_textBoxMessage.Text;
                int iNumTerrainDisks = MFramework.Instance.Scene.GetTerrainDiskSize();
                int iNumEntity = MFramework.Instance.Scene.GetSceneDiskSize();
                m_textBoxMessage.Text = "从地形中收集到 disk " + iNumTerrainDisks + "个\r\n" + m_textBoxMessage.Text;
                m_textBoxMessage.Text = "从场景物件收集到 Entity " + iNumEntity + "个\r\n" + m_textBoxMessage.Text;
            }
            else
            {
                m_textBoxMessage.Text = "场景信息收集中断.\r\n" + m_textBoxMessage.Text;
            }
        }

        // 保存 disk 信息
        private void m_btnSaveDiskInfo_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDlg.FilterIndex = 1;
            saveFileDlg.RestoreDirectory = true;

            if (saveFileDlg.ShowDialog() == DialogResult.OK)
            {
                MFramework.Instance.Scene.SaveSceneDiskInfo(saveFileDlg.FileName);
            }
        }

        // 载入 disk 信息
        private void m_btnLoadDiskInfo_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFiledlg = new OpenFileDialog();
            openFiledlg.InitialDirectory = Application.StartupPath;
            openFiledlg.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            openFiledlg.FilterIndex = 1;
            openFiledlg.RestoreDirectory = true;

            if (openFiledlg.ShowDialog() == DialogResult.OK)
            {
                MFramework.Instance.Scene.LoadSceneDiskInfo(openFiledlg.FileName);
            }  
        }

        private void m_btnGenerateShadow_Click(object sender, EventArgs e)
        {
            int iNumChunks = m_listBoxShadowedChunks.Items.Count;

            float fMaxShadow = float.Parse(m_textBoxTerrAOMaxShadowed.Text);
            float fTerrainAffectFactor = float.Parse(m_textBoxTerrainAffectFactor.Text);

            for(int i=0; i<iNumChunks; i++)
            {
                int iChunkID = (int)m_listBoxShadowedChunks.Items[i];
                m_textBoxMessage.Text = "开始计算 chunk" + iChunkID + " 的环境阴影.请耐心的等待.\r\n" + m_textBoxMessage.Text;
                m_textBoxMessage.Update();

                System.DateTime beginTim = System.DateTime.Now;
                MFramework.Instance.Scene.TerrainAmbientOcclusion(iChunkID, 
                            fTerrainAffectFactor, fTerrainAffectFactor, fMaxShadow);
                System.DateTime endTim = System.DateTime.Now;
                System.TimeSpan dTime = endTim - beginTim;
                m_textBoxMessage.Text = "chunk " + iChunkID + " 的环境阴影计算完毕.共耗时 " + dTime.TotalSeconds + " 秒.\r\n" + m_textBoxMessage.Text;
                m_textBoxMessage.Update();

            }
        }

        private void m_textBoxMaxShadowed_TextChanged(object sender, EventArgs e)
        {
            try
            {
                float fValue = float.Parse(m_textBoxTerrAOMaxShadowed.Text);
                if (fValue<0.0f || fValue>1.0f)
                {
                    m_textBoxTerrAOMaxShadowed.Text = "0.5f";
                }
            }
            catch(Exception e2)
            {
                m_textBoxTerrAOMaxShadowed.Text = "0.5f";
            }
        }

        private void m_textBoxTerrainAffectFactor_TextChanged(object sender, EventArgs e)
        {
            try
            {
                float fValue = float.Parse(m_textBoxTerrainAffectFactor.Text);
                if (fValue < 0.0f || fValue > 100.0f)
                {
                    m_textBoxTerrainAffectFactor.Text = "20.0f";
                }
            }
            catch (Exception e2)
            {
                m_textBoxTerrainAffectFactor.Text = "20.0f";
            }
        }

        private void m_btnProjectionShadow_Click(object sender, EventArgs e)
        {
            try
            {
                MPoint3 sunDir = MFramework.Instance.Scene.GetSunDirection();
                if (sunDir == null)
                {
                    m_textBoxMessage.Text += "在场景中没有找到名为 sun 的方向光. 阴影生成中断。\r\n";
                    m_textBoxMessage.Update();
                    m_textBoxMessage.ScrollToCaret();
                    return;
                }


                int iNumChunks = m_listBoxShadowedChunks.Items.Count;

                float fMaxShadow = float.Parse(m_textBoxDirMaxShadowed.Text);
                float fBlur = float.Parse(m_tbBlurParam.Text);
                m_textBoxMessage.Text += "开始计算方向光阴影.请耐心的等待.\r\n";
                m_textBoxMessage.Update();

                System.DateTime beginTim = System.DateTime.Now;
                MFramework.Instance.Scene.ProjectionShadow(sunDir, fMaxShadow, fBlur);
                System.DateTime endTim = System.DateTime.Now;
                System.TimeSpan dTime = endTim - beginTim;
                m_textBoxMessage.Text += "方向光阴影计算完毕.共耗时 " + dTime.TotalSeconds + " 秒.\r\n";
                m_textBoxMessage.Update();
                m_textBoxMessage.ScrollToCaret();

            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }

        }

        private void m_btnGenerateEntityAOShadow_Click(object sender, EventArgs e)
        {
            try
            {
                float fMaxShadow = float.Parse(m_tbEntityAOMaxShadow.Text);
                float fBlur = float.Parse(m_tbBlurEntityAO.Text);
                float fBlurTexSize = float.Parse(m_tbBlurTexSize.Text);
                MFramework.Instance.Scene.FakeEntityAmbientOcclusion(fMaxShadow, fBlur, fBlurTexSize);
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }

        }

        private void m_btnClear_Click(object sender, EventArgs e)
        {
            m_listBoxShadowedChunks.Items.Clear();
            m_listBoxAllChunks.SelectedItems.Clear();
        }

        private void m_btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < m_listBoxAllChunks.Items.Count; i++)
            {
                m_listBoxAllChunks.SetSelected(i, true);

            }
            m_listBoxAllChunks.Update();

        }

        private void ShadowPanel_Load(object sender, EventArgs e)
        {

        }

        private void m_cbViewStaticShadow_CheckedChanged(object sender, EventArgs e)
        {
            MTerrain.Instance.SetShadowPreview(m_cbViewStaticShadow.Checked, m_cbViewDynamicShadow.Checked);
        }

        private void m_cbViewDynamicShadow_CheckedChanged(object sender, EventArgs e)
        {
            MTerrain.Instance.SetShadowPreview(m_cbViewStaticShadow.Checked, m_cbViewDynamicShadow.Checked);
        }



    }
}