﻿using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class RegionToolPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_treeViewRegionList = new System.Windows.Forms.TreeView();
            this.m_radioButtonCreatrRegion = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.m_radioButtonUpdateRegion = new System.Windows.Forms.RadioButton();
            this.m_comboBoxRegionType = new System.Windows.Forms.ComboBox();
            this.m_checkBoxShow = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_textBoxRegionName = new System.Windows.Forms.TextBox();
            this.m_radioButtonShowRegion = new System.Windows.Forms.RadioButton();
            this.m_buttonRefresh = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_checkBoxMusicLoop = new System.Windows.Forms.CheckBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.m_textBoxMusicFileName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_textBoxVolume = new System.Windows.Forms.TextBox();
            this.m_buttonCreateRectRegion = new System.Windows.Forms.Button();
            this.m_buttonCreatePolyRegion = new System.Windows.Forms.Button();
            this.m_buttonOpenMusicFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_treeViewRegionList
            // 
            this.m_treeViewRegionList.HideSelection = false;
            this.m_treeViewRegionList.Location = new System.Drawing.Point(12, 45);
            this.m_treeViewRegionList.Name = "m_treeViewRegionList";
            this.m_treeViewRegionList.Size = new System.Drawing.Size(268, 268);
            this.m_treeViewRegionList.TabIndex = 0;
            this.m_treeViewRegionList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.m_treeViewRegionList_AfterSelect);
            this.m_treeViewRegionList.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.m_treeViewRegionList_NodeMouseClick);
            // 
            // m_radioButtonCreatrRegion
            // 
            this.m_radioButtonCreatrRegion.AutoSize = true;
            this.m_radioButtonCreatrRegion.Location = new System.Drawing.Point(12, 324);
            this.m_radioButtonCreatrRegion.Name = "m_radioButtonCreatrRegion";
            this.m_radioButtonCreatrRegion.Size = new System.Drawing.Size(71, 16);
            this.m_radioButtonCreatrRegion.TabIndex = 1;
            this.m_radioButtonCreatrRegion.TabStop = true;
            this.m_radioButtonCreatrRegion.Text = "新建区域";
            this.m_radioButtonCreatrRegion.UseVisualStyleBackColor = true;
            this.m_radioButtonCreatrRegion.Visible = false;
            this.m_radioButtonCreatrRegion.CheckedChanged += new System.EventHandler(this.m_radioButtonCreatrRegion_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 360);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "区域类型";
            // 
            // m_radioButtonUpdateRegion
            // 
            this.m_radioButtonUpdateRegion.AutoSize = true;
            this.m_radioButtonUpdateRegion.Location = new System.Drawing.Point(110, 324);
            this.m_radioButtonUpdateRegion.Name = "m_radioButtonUpdateRegion";
            this.m_radioButtonUpdateRegion.Size = new System.Drawing.Size(71, 16);
            this.m_radioButtonUpdateRegion.TabIndex = 3;
            this.m_radioButtonUpdateRegion.TabStop = true;
            this.m_radioButtonUpdateRegion.Text = "更新区域";
            this.m_radioButtonUpdateRegion.UseVisualStyleBackColor = true;
            this.m_radioButtonUpdateRegion.Visible = false;
            this.m_radioButtonUpdateRegion.CheckedChanged += new System.EventHandler(this.m_radioButtonUpdateRegion_CheckedChanged);
            // 
            // m_comboBoxRegionType
            // 
            this.m_comboBoxRegionType.FormattingEnabled = true;
            this.m_comboBoxRegionType.Location = new System.Drawing.Point(90, 355);
            this.m_comboBoxRegionType.Name = "m_comboBoxRegionType";
            this.m_comboBoxRegionType.Size = new System.Drawing.Size(121, 20);
            this.m_comboBoxRegionType.TabIndex = 4;
            this.m_comboBoxRegionType.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxRegionType_SelectedIndexChanged);
            // 
            // m_checkBoxShow
            // 
            this.m_checkBoxShow.AutoSize = true;
            this.m_checkBoxShow.Location = new System.Drawing.Point(12, 538);
            this.m_checkBoxShow.Name = "m_checkBoxShow";
            this.m_checkBoxShow.Size = new System.Drawing.Size(120, 16);
            this.m_checkBoxShow.TabIndex = 5;
            this.m_checkBoxShow.Text = "在场景中显示区域";
            this.m_checkBoxShow.UseVisualStyleBackColor = true;
            this.m_checkBoxShow.CheckedChanged += new System.EventHandler(this.m_checkBoxShow_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 385);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "区域名称";
            // 
            // m_textBoxRegionName
            // 
            this.m_textBoxRegionName.Location = new System.Drawing.Point(90, 380);
            this.m_textBoxRegionName.Name = "m_textBoxRegionName";
            this.m_textBoxRegionName.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxRegionName.TabIndex = 7;
            this.m_textBoxRegionName.TextChanged += new System.EventHandler(this.m_textBoxRegionName_TextChanged);
            // 
            // m_radioButtonShowRegion
            // 
            this.m_radioButtonShowRegion.AutoSize = true;
            this.m_radioButtonShowRegion.Location = new System.Drawing.Point(208, 324);
            this.m_radioButtonShowRegion.Name = "m_radioButtonShowRegion";
            this.m_radioButtonShowRegion.Size = new System.Drawing.Size(71, 16);
            this.m_radioButtonShowRegion.TabIndex = 8;
            this.m_radioButtonShowRegion.TabStop = true;
            this.m_radioButtonShowRegion.Text = "显示区域";
            this.m_radioButtonShowRegion.UseVisualStyleBackColor = true;
            this.m_radioButtonShowRegion.Visible = false;
            this.m_radioButtonShowRegion.CheckedChanged += new System.EventHandler(this.m_radioButtonShowRegion_CheckedChanged);
            // 
            // m_buttonRefresh
            // 
            this.m_buttonRefresh.Location = new System.Drawing.Point(204, 531);
            this.m_buttonRefresh.Name = "m_buttonRefresh";
            this.m_buttonRefresh.Size = new System.Drawing.Size(75, 23);
            this.m_buttonRefresh.TabIndex = 9;
            this.m_buttonRefresh.Text = "刷新";
            this.m_buttonRefresh.UseVisualStyleBackColor = true;
            this.m_buttonRefresh.Click += new System.EventHandler(this.m_buttonRefresh_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 420);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "声音文件";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 445);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "音量大小";
            // 
            // m_checkBoxMusicLoop
            // 
            this.m_checkBoxMusicLoop.AutoSize = true;
            this.m_checkBoxMusicLoop.Location = new System.Drawing.Point(12, 470);
            this.m_checkBoxMusicLoop.Name = "m_checkBoxMusicLoop";
            this.m_checkBoxMusicLoop.Size = new System.Drawing.Size(72, 16);
            this.m_checkBoxMusicLoop.TabIndex = 12;
            this.m_checkBoxMusicLoop.Text = "循环播放";
            this.m_checkBoxMusicLoop.UseVisualStyleBackColor = true;
            this.m_checkBoxMusicLoop.CheckedChanged += new System.EventHandler(this.m_checkBoxMusicLoop_CheckedChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // m_textBoxMusicFileName
            // 
            this.m_textBoxMusicFileName.Location = new System.Drawing.Point(90, 415);
            this.m_textBoxMusicFileName.Name = "m_textBoxMusicFileName";
            this.m_textBoxMusicFileName.Size = new System.Drawing.Size(180, 21);
            this.m_textBoxMusicFileName.TabIndex = 13;
            this.m_textBoxMusicFileName.TextChanged += new System.EventHandler(this.m_textBoxMusicFileName_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(137, 445);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "%(0~100)";
            // 
            // m_textBoxVolume
            // 
            this.m_textBoxVolume.Location = new System.Drawing.Point(90, 440);
            this.m_textBoxVolume.Name = "m_textBoxVolume";
            this.m_textBoxVolume.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxVolume.TabIndex = 15;
            this.m_textBoxVolume.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_textBoxVolume.TextChanged += new System.EventHandler(this.m_textBoxVolume_TextChanged);
            // 
            // m_buttonCreateRectRegion
            // 
            this.m_buttonCreateRectRegion.Location = new System.Drawing.Point(12, 12);
            this.m_buttonCreateRectRegion.Name = "m_buttonCreateRectRegion";
            this.m_buttonCreateRectRegion.Size = new System.Drawing.Size(75, 23);
            this.m_buttonCreateRectRegion.TabIndex = 16;
            this.m_buttonCreateRectRegion.Text = "矩形区域";
            this.m_buttonCreateRectRegion.UseVisualStyleBackColor = true;
            this.m_buttonCreateRectRegion.Click += new System.EventHandler(this.m_buttonCreateRectRegion_Click);
            // 
            // m_buttonCreatePolyRegion
            // 
            this.m_buttonCreatePolyRegion.Location = new System.Drawing.Point(205, 12);
            this.m_buttonCreatePolyRegion.Name = "m_buttonCreatePolyRegion";
            this.m_buttonCreatePolyRegion.Size = new System.Drawing.Size(75, 23);
            this.m_buttonCreatePolyRegion.TabIndex = 17;
            this.m_buttonCreatePolyRegion.Text = "多边形区域";
            this.m_buttonCreatePolyRegion.UseVisualStyleBackColor = true;
            this.m_buttonCreatePolyRegion.Click += new System.EventHandler(this.m_buttonCreatePolyRegion_Click);
            // 
            // m_buttonOpenMusicFile
            // 
            this.m_buttonOpenMusicFile.Location = new System.Drawing.Point(260, 415);
            this.m_buttonOpenMusicFile.Name = "m_buttonOpenMusicFile";
            this.m_buttonOpenMusicFile.Size = new System.Drawing.Size(20, 21);
            this.m_buttonOpenMusicFile.TabIndex = 18;
            this.m_buttonOpenMusicFile.Text = ".";
            this.m_buttonOpenMusicFile.UseVisualStyleBackColor = true;
            this.m_buttonOpenMusicFile.Click += new System.EventHandler(this.m_buttonOpenMusicFile_Click);
            // 
            // RegionToolPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 566);
            this.Controls.Add(this.m_buttonOpenMusicFile);
            this.Controls.Add(this.m_buttonCreatePolyRegion);
            this.Controls.Add(this.m_buttonCreateRectRegion);
            this.Controls.Add(this.m_textBoxVolume);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_textBoxMusicFileName);
            this.Controls.Add(this.m_checkBoxMusicLoop);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_buttonRefresh);
            this.Controls.Add(this.m_radioButtonShowRegion);
            this.Controls.Add(this.m_textBoxRegionName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_checkBoxShow);
            this.Controls.Add(this.m_comboBoxRegionType);
            this.Controls.Add(this.m_radioButtonUpdateRegion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_radioButtonCreatrRegion);
            this.Controls.Add(this.m_treeViewRegionList);
            this.Name = "RegionToolPanel";
            this.Text = "RegionToolPanel";
            this.TopMost = true;
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.RegionToolPanel_Paint);
            this.Click += new System.EventHandler(this.RegionToolPanel_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView m_treeViewRegionList;
        private System.Windows.Forms.RadioButton m_radioButtonCreatrRegion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton m_radioButtonUpdateRegion;
        private System.Windows.Forms.ComboBox m_comboBoxRegionType;
        private System.Windows.Forms.CheckBox m_checkBoxShow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_textBoxRegionName;
        private System.Windows.Forms.RadioButton m_radioButtonShowRegion;

        private System.Windows.Forms.TreeNode m_LastSelectNode;
        private System.Windows.Forms.Button m_buttonRefresh;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox m_checkBoxMusicLoop;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox m_textBoxMusicFileName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox m_textBoxVolume;
        IUICommandService m_uiCommandService;
        private System.Windows.Forms.Button m_buttonCreateRectRegion;
        private System.Windows.Forms.Button m_buttonCreatePolyRegion;
        private System.Windows.Forms.Button m_buttonOpenMusicFile;
    }
}