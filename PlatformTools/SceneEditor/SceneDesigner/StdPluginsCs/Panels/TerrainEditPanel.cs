﻿/** @file TerrainEditPanel.cs 
 @brief 
 <pre>
 *	Copyright (c) 2007，第九城市游戏研发中心
 *	All rights reserved.
 *
 *	当前版本：
 *	作    者：zhangxu
 *	完成日期：2007-11-6
*
 *	取代版本：
 *	作    者：
 *	完成日期：
 </pre>*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class TerrainEditPanel : Form
    {
        // 使用 MTerrain 的 EPaintType 枚举
        // 绘制模式
        //public enum PaintMode
        //{
        //    PM_TERRAIN_VERTEX = 0,  // 地形顶点
        //    PM_TERRAIN_MATERIAL,    // 地表纹理
        //    PM_TERRAIN_PROPERTY,    // 地表属性
        //    PM_WATER_SURFACE,       // 水体表面生成
        //    PM_GRID_AREA,           // 以 grid 为单位的区域。目前用于在区域中刷模型
        //    PM_MAX,
        //}

        public TerrainEditPanel()
        {
            InitializeComponent();

            SettingsService.RegisterSettingsObject(
            "TerrainEditPanel.Raise", true, SettingsCategory.PerUser);

            UICommandService.BindCommands(this);
            
            this.m_textBoxinnerradius.Text = "0";
            this.m_textBoxouterradius.Text = "0.1";
            this.m_textBoxheight.Text = "1";

            this.m_trackHeight.Minimum = -100;
            this.m_trackHeight.Maximum = 255;

            this.m_trackBarInnerRadius.Minimum = 0;
            this.m_trackBarInnerRadius.Maximum = (int)m_MaxRadius;

            this.m_trackBarOuterRadius.Minimum = 0;
            this.m_trackBarOuterRadius.Maximum = (int)m_MaxRadius;
            this.m_trackBarOuterRadius.Value = 1;
            

            this.m_selectedMaterial = "";
            this.m_lastSelectedMaterial = "";
            this.m_previewImage = "";
            this.m_lastPreviewImage = "";
            this.m_lastSelectedHeight = 0.0f;
            this.m_lastSelectedGradual = 1.0f;

            this.m_fInnerRadius = 0.0f;
            this.m_fOuterRadius = 0.1f;
            this.m_fValue = 0.0f;

            this.m_iCurrentEditChunk = 0;

            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            // 从 CSceneDesignerConfig 中获取 terrain surface property 列表
            m_TerrSurPropList = MFramework.Instance.Scene.GetTerrainSurfacePropertyList();

             // 刷新 combobox
            RefreshTerrSurPropComboBox();

            //m_PanelShadowTools = new ShadowPanel();
        }
        /// <summary>
        /// 返回内半径
        /// </summary>
        /// <returns>笔刷内半径值</returns>
        public int get_InnerRadius()
        {
            return this.m_trackBarInnerRadius.Value;
        }
        /// <summary>
        /// 返回外半径
        /// </summary>
        /// <returns>笔刷外半径值</returns>
        public int get_OuterRadius()
        {
            return this.m_trackBarOuterRadius.Value;
        }
        /// <summary>
        /// 返回高度
        /// </summary>
        /// <returns>笔刷高度值</returns>
        public int get_Height()
        {
            return this.m_trackHeight.Value;
        }

        #region const data
        /// <summary>
        /// 最大半径值
        /// </summary>
        private const float m_MaxRadius = 200.0f;
        /// <summary>
        /// 预览最大半径
        /// </summary>
        private const float m_PreviewMax = 90.0f;
        /// <summary>
        /// 预览图大小
        /// </summary>
        private const int m_previewpicSize = 80;

        /// <summary>
        /// 1除以该变量即为半径变动最小单位
        /// </summary>
        private const float m_RadiusDivider = 10.0f;
        private const float m_GradualDivider = 100.0f;
        private const float m_HeightDivider = 1.0f;

        private float m_fInnerRadius;
        private float m_fOuterRadius;
        private float m_fValue;

        #endregion
        #region Private Data

        private ISelectionService m_selectionService;
        private ISettingsService m_settingsService;
        private IUICommandService m_uiCommandService;
        private ICommandPanelService m_commandPanelService;
        private ICommandService m_commandService;

        private string m_selectedMaterial;
        private string m_lastSelectedMaterial;
        private string m_previewImage;  // 用于预览的 bmp 纹理
        private string m_lastPreviewImage;
        private float m_lastSelectedHeight;
        private float m_lastSelectedGradual;
       
        #endregion

        private MFramework FW
        {
            get
            { return MFramework.Instance; }
        }



        private ISettingsService SettingsService
        {
            get
            {
                if (m_settingsService == null)
                {
                    ServiceProvider sp = ServiceProvider.Instance;
                    m_settingsService =
                        sp.GetService(typeof(ISettingsService))
                        as ISettingsService;
                }
                return m_settingsService;
            }
        }

        private IUICommandService UICommandService
        {
            get
            {
                if (m_uiCommandService == null)
                {
                    ServiceProvider sp = ServiceProvider.Instance;
                    m_uiCommandService =
                        sp.GetService(typeof(IUICommandService))
                        as IUICommandService;
                }
                return m_uiCommandService;
            }
        }

        [UICommandHandler("TerrainBrushEnlarge")]
        private void OnTerrainBrushEnlarge(object sender, EventArgs args)
        {
            if (m_trackBarOuterRadius.Value < m_trackBarInnerRadius.Value)
            {
                m_trackBarInnerRadius.Value = m_trackBarOuterRadius.Value;
            }

            this.m_trackBarOuterRadius.Value += (int)(0.3 * m_RadiusDivider);

            m_fInnerRadius = (float)this.m_trackBarInnerRadius.Value / m_RadiusDivider;
            m_textBoxinnerradius.Text = m_fInnerRadius.ToString();

            m_fOuterRadius = (float)this.m_trackBarOuterRadius.Value / m_RadiusDivider;
            m_textBoxouterradius.Text = m_fOuterRadius.ToString();

            SendSettings(m_PaintMode);
            m_panelbrushpreview.Invalidate();
        }

        [UICommandHandler("TerrainBrushReduce")]
        private void OnTerrainBrushReduce(object sender, EventArgs args)
        {
            if (m_trackBarOuterRadius.Value < m_trackBarInnerRadius.Value)
            {
                m_trackBarInnerRadius.Value = m_trackBarOuterRadius.Value;
            }

            this.m_trackBarOuterRadius.Value -= (int)(0.3 * m_RadiusDivider);
            if (this.m_trackBarOuterRadius.Value / m_RadiusDivider < 0.1)
                this.m_trackBarOuterRadius.Value = (int)(0.1 * m_RadiusDivider);

            m_fInnerRadius = (float)this.m_trackBarInnerRadius.Value / m_RadiusDivider;
            m_textBoxinnerradius.Text = m_fInnerRadius.ToString();

            m_fOuterRadius = (float)this.m_trackBarOuterRadius.Value / m_RadiusDivider;
            m_textBoxouterradius.Text = m_fOuterRadius.ToString();

            SendSettings(m_PaintMode);
            m_panelbrushpreview.Invalidate();
        }

        private ICommandPanelService CommandPanelService
        {
            get
            {
                if (m_commandPanelService == null)
                {
                    ServiceProvider sp = ServiceProvider.Instance;

                    m_commandPanelService =
                        sp.GetService(typeof(ICommandPanelService))
                        as ICommandPanelService;
                }
                return m_commandPanelService;
            }
        }

        private ICommandService CommandService
        {
            get
            {
                if (m_commandService == null)
                {
                    ServiceProvider sp = ServiceProvider.Instance;

                    m_commandService =
                        sp.GetService(typeof(ICommandService))
                        as ICommandService;
                }
                return m_commandService;
            }
        }

        #region Winform handlers
        private Graphics m_Graphics;
        private System.Drawing.Bitmap m_BrushBitmap;

        /// <summary>
        /// 绘制预览图
        /// </summary>
        private void ReDrawEllipse()
        {
            m_Graphics.Clear(SystemColors.Control);
            GraphicsPath path = new GraphicsPath();///构造一个路径

            int g_centerX = this.m_panelbrushpreview.Width / 2;
            int g_centerY = this.m_panelbrushpreview.Height / 2;

            float finner = (this.m_trackBarInnerRadius.Value / m_MaxRadius) * m_PreviewMax;
            float fouter = (this.m_trackBarOuterRadius.Value / m_MaxRadius) * m_PreviewMax;

            path.AddEllipse(g_centerX - fouter / 2, g_centerY - fouter / 2, fouter, fouter);

            ///使用路径构建一个画刷
            ///Position参数数组
            float[] ff = { 0.0f, 1.0f, 1.0f };
            ///Factor参数数组
            float[] fp = { 0.0f, 0.1f, 1.0f };///中间的0.1f为任意值,需要根据实际内外半径值之比进行修正

            float fio = finner / fouter;
            fp[1] = (1.0f - fio);///修正Position数据

            ///根据路径构建出路径渐变画刷
            PathGradientBrush pthGrBrush = new PathGradientBrush(path);

            pthGrBrush.CenterPoint = new Point(g_centerX, g_centerY);
            pthGrBrush.CenterColor = Color.Black;///中心设为黑色

            pthGrBrush.SurroundColors = new Color[] { Color.LightGray };
            Blend blend = new Blend();
            blend.Positions = fp;
            blend.Factors = ff;

            pthGrBrush.Blend = blend;

            m_Graphics.FillEllipse(pthGrBrush, g_centerX - fouter / 2, g_centerY - fouter / 2, fouter, fouter);
            pthGrBrush.Dispose();
            path.Dispose();

        }

        private void TerrainEditPanel_Load(object sender, EventArgs e)
        {
            this.m_Graphics = this.m_panelbrushpreview.CreateGraphics();
        }
        #endregion
        /// <summary>
        /// 消息发送函数
        /// </summary>
        /// <param name="strName">消息类型</param>
        /// <param name="fValue">消息值</param>

        private void m_trackBarinnerradius_Scroll(object sender, EventArgs e)
        {
            if (this.m_trackBarInnerRadius.Value > this.m_trackBarOuterRadius.Value)
            {
                this.m_trackBarOuterRadius.Value = this.m_trackBarInnerRadius.Value;
            }

            if (this.m_trackBarOuterRadius.Value / m_RadiusDivider < 0.1)
                this.m_trackBarOuterRadius.Value = (int)(0.1 * m_RadiusDivider);

            m_fInnerRadius = (float)this.m_trackBarInnerRadius.Value / m_RadiusDivider;
            m_textBoxinnerradius.Text = m_fInnerRadius.ToString();

            m_fOuterRadius = (float)this.m_trackBarOuterRadius.Value / m_RadiusDivider;
            m_textBoxouterradius.Text = m_fOuterRadius.ToString();

            SendSettings(m_PaintMode);
            m_panelbrushpreview.Invalidate();
        }

        private void m_trackBarouterradius_Scroll(object sender, EventArgs e)
        {
            if (m_trackBarOuterRadius.Value < m_trackBarInnerRadius.Value)
            {
                m_trackBarInnerRadius.Value = m_trackBarOuterRadius.Value;
            }

            if (this.m_trackBarOuterRadius.Value / m_RadiusDivider < 0.1)
                this.m_trackBarOuterRadius.Value = (int)(0.1 * m_RadiusDivider);

            m_fInnerRadius = (float)this.m_trackBarInnerRadius.Value / m_RadiusDivider;
            m_textBoxinnerradius.Text = m_fInnerRadius.ToString();

            m_fOuterRadius = (float)this.m_trackBarOuterRadius.Value / m_RadiusDivider;
            m_textBoxouterradius.Text = m_fOuterRadius.ToString();

            SendSettings(m_PaintMode);
            m_panelbrushpreview.Invalidate();
        }

        private void m_trackBargradualchange_Scroll(object sender, EventArgs e)
        {
            if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_VERTEX)
            {
                m_fValue = (float)m_trackHeight.Value / m_HeightDivider;
                m_lastSelectedHeight = m_fValue;
                m_textBoxheight.Text = m_fValue.ToString();
            }
            else if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_MATERIAL)
            {
                m_fValue = m_trackHeight.Value / m_GradualDivider;
                m_lastSelectedGradual = m_fValue;
                m_textBoxheight.Text= m_fValue.ToString();
            }

            SendSettings(m_PaintMode);
        }


        private void TerrainEditPanel_Paint(object sender, PaintEventArgs e)
        {
            ReDrawEllipse();
        }

        // 设置当前绘制模式
        // 0 - 地形高度
        // 1 - 地形纹理
        // 2 - 地表属性
        // 3 - 水面
        // 4 - grid 为单位的地形区域
        private void SetPaintMode(MTerrain.EPainterType type)
        {
            m_PaintMode = type;

            // 改变 UI 到相应 Paint 类型
            if (type == MTerrain.EPainterType.PT_TERRAIN_VERTEX)
            {
                this.m_fValue = this.m_lastSelectedHeight;
                this.m_labelheight.Text = "高度";
                this.m_trackHeight.Maximum = 255;
                this.m_trackHeight.Minimum = -100;
                this.m_trackHeight.Value = (int)Math.Ceiling(m_fValue * m_HeightDivider);
                this.m_textBoxheight.Text = this.m_fValue.ToString();
                

                // 将面板设置为 地形高度
                m_tabControlTools.SelectedIndex = 0;

                // 设置 高度/浓度 track bar 的显示
                m_labelheight.Enabled = true;
                m_trackHeight.Enabled = true;
                m_textBoxheight.Enabled = true;
                m_HeightLabel.Show();
            }
            else if(type == MTerrain.EPainterType.PT_TERRAIN_MATERIAL)
            {
                this.m_fValue = this.m_lastSelectedGradual;

                this.m_labelheight.Text = "渐变度";
                this.m_trackHeight.Maximum = 100;
                this.m_trackHeight.Minimum = -100;
                this.m_trackHeight.Value = (int)Math.Ceiling(m_fValue * m_GradualDivider);

                this.m_textBoxheight.Text = this.m_fValue.ToString();

    
                // 将面板设置为 地表纹理
                m_tabControlTools.SelectedIndex = 1;

                // 设置 高度/浓度 track bar 的显示
                m_labelheight.Enabled = true;
                m_trackHeight.Enabled = true;
                m_textBoxheight.Enabled = true;
                m_HeightLabel.Hide();             
            }
            else if (type == MTerrain.EPainterType.PT_TERRAIN_PROPERTY)
            {
                // 将面板设置为 地表属性
                m_tabControlTools.SelectedIndex = 2;

                m_labelheight.Enabled = false;
                m_trackHeight.Enabled = false;
                m_textBoxheight.Enabled = false;
                m_HeightLabel.Hide();  
            }
            else if (type == MTerrain.EPainterType.PT_TERRAIN_GRID_AREA)
            {
                // 将面板设置为 其他工具
                m_tabControlTools.SelectedIndex = 3;

                m_labelheight.Enabled = false;
                m_trackHeight.Enabled = false;
                m_textBoxheight.Enabled = false;
                m_HeightLabel.Hide();  
            }
            //else if (type == MTerrain.EPainterType.PT_WATER_SURFACE)
            //{
            //    // 将面板设置为 其他工具
            //    m_tabControlTools.SelectedIndex = 3;

            //    m_labelheight.Enabled = false;
            //    m_trackHeight.Enabled = false;
            //    m_textBoxheight.Enabled = false;
            //    m_HeightLabel.Hide();  
            //}
            else if (type == MTerrain.EPainterType.PT_REMOVE_ENTITY)
            {
                // 将面板设置为 其他工具
                m_tabControlTools.SelectedIndex = 3;

                m_labelheight.Enabled = false;
                m_trackHeight.Enabled = false;
                m_textBoxheight.Enabled = false;
                m_HeightLabel.Hide();
            }

            SendSettings(type);
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_VERTEX)
            {
                m_tabControlTools.SelectedIndex = 0;

            }
            else if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_MATERIAL)
            {
                m_tabControlTools.SelectedIndex = 1;
            }
            else if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_PROPERTY)
            {
                m_tabControlTools.SelectedIndex = 2;
            }
            else if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_GRID_AREA)
            {
                m_tabControlTools.SelectedIndex = 3;
            }
            //else if (m_PaintMode == MTerrain.EPainterType.PT_WATER_SURFACE)
            //{
            //    m_tabControlTools.SelectedIndex = 3;
            //}

            return;

        }

        private void m_panelbrushpreview_Paint(object sender, PaintEventArgs e)
        {
            ReDrawEllipse();
        }

        private void m_buttonselectmaterial_Click(object sender, EventArgs e)
        {
            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);
            SelectMaterialDlg m_picselectdlg = new SelectMaterialDlg();
            m_picselectdlg.ShowDialog();
            m_selectedMaterial = m_picselectdlg.get_CurrentSelectedPic();
            m_previewImage = m_picselectdlg.get_CurrentPreviewPic();
            if (!m_selectedMaterial.Equals(""))
            {
                m_lastSelectedMaterial = m_selectedMaterial;
                m_lastPreviewImage = m_previewImage;
                m_pictureBoxMaterialPreview.Load(m_previewImage);
                m_pictureBoxMaterialPreview2.Load(m_previewImage);

                SendSettings(MTerrain.EPainterType.PT_TERRAIN_MATERIAL);
            }
            else
            {
                m_selectedMaterial = m_lastSelectedMaterial;
                m_previewImage = m_lastPreviewImage;
            }


        }

        private void TerrainCreateButton_Click(object sender, EventArgs e)
        {
            if (MFramework.Instance.Scene.HasTerrain())
            {
                MessageBox.Show("地形已存在。");
                return;  
            }

            if ( m_TerrainChunkX.Text.Equals("") )
            {
                MessageBox.Show("请输入参数X");
                return;    
            }
            else if (m_TerrainChunkY.Text.Equals(""))
            {
                MessageBox.Show("请输入参数Y");
                return;  
            }
            else if (m_selectedMaterial.Equals(""))
            {
                MessageBox.Show("请选择一种材质");
                return;  
            }
            InputDlg inputDlg = new InputDlg();
            inputDlg.ShowDialog(this);
            System.String strInput = inputDlg.GetText();
            int iMapNo = 0;
            try
            {
                iMapNo = Int32.Parse(strInput);
            }
            catch (System.Exception e2)
            {
                MessageBox.Show("地图编号请输入整数。如果为非正式地图请输入0。");
                return;
            }
            
            int iTerrainChunkX;
            int iTerrainChunkY;
            try
            {
                iTerrainChunkX = Convert.ToInt32(m_TerrainChunkX.Text);
                iTerrainChunkY = Convert.ToInt32(m_TerrainChunkY.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("参数只能为数字");
                return ;
            }

            /// 限定X,Y的大小，不能超过8
            if (iTerrainChunkX > 16 || iTerrainChunkX < 1 )
            {
                MessageBox.Show("参数X必须在1~16之间");
                return; 
            }
            if (iTerrainChunkY > 16 || iTerrainChunkY < 1)
            {
                MessageBox.Show("参数Y必须在1~16之间");
                return;
            }

            MTerrainParameter terrainParam = new MTerrainParameter(iTerrainChunkX, iTerrainChunkY, m_selectedMaterial, 0, iMapNo);
            SettingsService.SetSettingsObject("TerrainParam", terrainParam, SettingsCategory.Temp);

            m_radioButtonTex0.Enabled = true;
            m_radioButtonTex1.Enabled = true;
            m_radioButtonTex2.Enabled = true;
            m_radioButtonTex3.Enabled = true;

            m_radioButtonTex0.PerformClick();
            m_textBoxTex0.Text = m_selectedMaterial;
        }

        private void m_SelectMatiralButton_Click(object sender, EventArgs e)
        {
            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);

            SelectMaterialDlg m_picselectdlg = new SelectMaterialDlg();
            m_picselectdlg.ShowDialog();
            m_selectedMaterial = m_picselectdlg.get_CurrentSelectedPic();
            m_previewImage = m_picselectdlg.get_CurrentPreviewPic();
            if (!m_selectedMaterial.Equals(""))
            {
                m_lastSelectedMaterial = m_selectedMaterial;
                m_lastPreviewImage = m_previewImage;
                m_pictureBoxMaterialPreview.Load(m_previewImage);
                m_pictureBoxMaterialPreview2.Load(m_previewImage);

                if (1 == m_tabControlTools.SelectedIndex)
                {
                    SendSettings(MTerrain.EPainterType.PT_TERRAIN_MATERIAL);                    
                }
            }
            else
            {
                m_selectedMaterial = m_lastSelectedMaterial;
                m_previewImage = m_lastPreviewImage;
            }
            
        }

        private void SendSettings(MTerrain.EPainterType eType)
        {
            MBrushParameter tempBrush = null;
		
            if (eType == MTerrain.EPainterType.PT_TERRAIN_VERTEX ||
                eType == MTerrain.EPainterType.PT_TERRAIN_PROPERTY ||
                eType == MTerrain.EPainterType.PT_TERRAIN_GRID_AREA ||
                //eType == MTerrain.EPainterType.PT_WATER_SURFACE ||
                eType == MTerrain.EPainterType.PT_REMOVE_ENTITY)
            {
                tempBrush = new MBrushParameter(m_fInnerRadius,
                                                m_fOuterRadius,
                                                m_fValue, "", eType);
            }
            else if (eType == MTerrain.EPainterType.PT_TERRAIN_MATERIAL)
            {
                tempBrush = new MBrushParameter(m_fInnerRadius,
                                                m_fOuterRadius,
                                                m_fValue, m_selectedMaterial, eType);
            }

            if (tempBrush != null)
            {
                SettingsService.SetSettingsObject("TerrainBrush", tempBrush, SettingsCategory.Temp);
            }

        }

        // 用户调整 地形高度/纹理深度 等属性，稍后完成
        private void m_textBoxheight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    m_fValue = (float)Convert.ToDouble(m_textBoxheight.Text);
                }
                catch (Exception)
                {
                    m_fValue = 0.0f;
                    m_textBoxheight.Text = "0";
                    m_trackHeight.Value = 0;
                    return;
                }

                //if (edit.Equals("terrain"))
                if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_VERTEX)
                {
                    if (m_fValue < -100.0f || m_fValue > 255.0f)
                    {
                        m_fValue = 0.0f;
                        m_textBoxheight.Text = "0";
                    }

                    this.m_lastSelectedHeight = this.m_fValue;
                    // trackBar上的数值响应更改
                    m_trackHeight.Value = (int)(m_fValue * m_HeightDivider);
                    SendSettings(MTerrain.EPainterType.PT_TERRAIN_VERTEX);
                }
                else if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_MATERIAL)
                {
                    if (m_fValue < 0.0f || m_fValue > 1.0f)
                    {
                        m_fValue = 0.0f;
                        m_textBoxheight.Text = "0";
                    }

                    this.m_lastSelectedGradual = this.m_fValue;
                    // trackBar上的数值响应更改
                    m_trackHeight.Value = (int)(m_fValue * m_GradualDivider);
                    SendSettings(MTerrain.EPainterType.PT_TERRAIN_MATERIAL);
                }
            }
        }

        private void m_textBoxouterradius_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    m_fOuterRadius = (float)Convert.ToDouble(m_textBoxouterradius.Text);
                }
                catch (Exception)
                {
                    m_fOuterRadius = 0.5f;
                    m_textBoxouterradius.Text = "0.5";
                    m_trackBarOuterRadius.Value = 1;
                    return;
                }

                if (m_fOuterRadius < 0.5f || m_fOuterRadius > 10)
                {
                    m_fOuterRadius = 0.5f;
                    m_textBoxouterradius.Text = "0.5";
                }

                if (m_fOuterRadius < m_fInnerRadius)
                {
                    m_fOuterRadius = m_fInnerRadius;
                    m_textBoxouterradius.Text = m_textBoxinnerradius.Text;
                }

                // trackBar上的数值响应更改
                m_trackBarOuterRadius.Value = (int)Math.Ceiling(m_fOuterRadius * m_RadiusDivider);

                //if (edit.Equals("terrain"))
                //{
                //    SendSettings(0);
                //}
                //else
                //{
                //    SendSettings(1);
                //}
            }
        }

        private void m_textBoxinnerradius_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    m_fInnerRadius = (float)Convert.ToDouble(m_textBoxinnerradius.Text);
                }
                catch (Exception)
                {
                    m_fInnerRadius = 0.0f;
                    m_textBoxinnerradius.Text = "0";
                    m_trackBarInnerRadius.Value = 0;
                    return;
                }

                if (m_fInnerRadius < 0 || m_fInnerRadius > 10)
                {
                    m_fInnerRadius = 0.0f;
                    m_textBoxinnerradius.Text = "0";
                }

                if (m_fInnerRadius > m_fOuterRadius)
                {
                    m_fInnerRadius = m_fOuterRadius;
                    m_textBoxinnerradius.Text = m_textBoxouterradius.Text;
                }

                // trackBar上的数值响应更改
                m_trackBarInnerRadius.Value = (int)Math.Round(m_fInnerRadius * m_RadiusDivider);

                //if (edit.Equals("terrain"))
                //{
                //    SendSettings(0);
                //}
                //else
                //{
                //    SendSettings(1);
                //}
            }
        }

        private void m_ShowCollisionDataCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            bool bShow = m_ShowCollisionDataCheckBox.Checked;
            MFramework.Instance.Scene.ShowCollisionData(bShow);
            MFramework.Instance.Scene.ShowHiddenTerrain(bShow);
        }

        private void TerrainRevertBtn_Click(object sender, EventArgs e)
        {
            // 地形还原
            MFramework.Instance.Scene.OptimizeTerrain(false);
        }

        private void TerrainOptimizeBtn_Click(object sender, EventArgs e)
        {
            // 地形优化
            MFramework.Instance.Scene.OptimizeTerrain(true);
        }

        private void BakeCollisionBlockBtn_Click(object sender, EventArgs e)
        {
            // 烘焙物件碰撞块
            MFramework.Instance.Scene.BakeObjectsToCollisionBlock();

            //MFramework.Instance.Scene.SM_Init();
            //MFramework.Instance.Scene.SM_Process();
            //MFramework.Instance.Scene.SM_UnInit();
        }

        private void OptimizeSensitivityStrackBar_Scroll(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetTerrainOptimizeSensitivity(m_trackBarOptimizeSensitivity.Value);
            m_textBoxOptimizeSensitivity.Text = m_trackBarOptimizeSensitivity.Value.ToString();
        }

        // 刷新 地表属性下拉列表
        private void RefreshTerrSurPropComboBox()
        {
            m_TerrSurPropComboBox.Items.Clear();
            foreach(MTerrainSurfaceProperty surProp in m_TerrSurPropList)
            {
                m_TerrSurPropComboBox.Items.Add(surProp.Name);
            }
        }

        private void m_textBoxOptimizeSensitivity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int iSensitivity = 0;
                try
                {
                    iSensitivity = Convert.ToInt32(m_textBoxOptimizeSensitivity.Text);
                }
                catch (Exception)
                {
                    iSensitivity = 5;
                    m_textBoxOptimizeSensitivity.Text = "5";
                    //return;
                }

                if (iSensitivity < 1 || iSensitivity > 50)
                {
                    iSensitivity = 5;
                    m_textBoxOptimizeSensitivity.Text = "5";
                }

                m_trackBarOptimizeSensitivity.Value = iSensitivity;
                MFramework.Instance.Scene.SetTerrainOptimizeSensitivity(m_trackBarOptimizeSensitivity.Value);

            }
        }

        private void TerrToCollisionBlockBtn_Click(object sender, EventArgs e)
        {
            // 根据地形生成碰状块
            int iDegree = 0;
            try
            {
                iDegree = Convert.ToInt32(DegreeTextBox.Text);
            }
            catch (Exception)
            {
                iDegree = 45;
                DegreeTextBox.Text = "45";
                MessageBox.Show("临界角度必须 1 至 89 之间的整数。");
                return;
            }

            if (iDegree<1 || iDegree>89)
            {
                iDegree = 45;
                DegreeTextBox.Text = "45";
                MessageBox.Show("临界角度必须 1 至 89 之间的整数。");
                return;
            }

            MFramework.Instance.Scene.TerrainToCollisionBlock(iDegree,false);
            MTerrain.Instance.FillTerrainCollisionHole(10000);  // 填充碰撞块空洞

        }

        private void ClearCollisionBlockBtn_Click(object sender, EventArgs e)
        {
            // 清除所有碰撞块
            DialogResult result = MessageBox.Show("本操作不可逆，确定要清除所有碰撞块吗？", "重要提示", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                MFramework.Instance.Scene.ClearAllCollisionBlock();
            }
        }

        private void ChangeChunkTexture(int iLayer)
        {
            // 判断当前显示的chunk纹理信息与当前正编辑chunk是否相同
            if (m_iCurrentEditChunk != MFramework.Instance.Scene.GetCurrentEditChunkID())
            {
                MessageBox.Show("当前显示纹理信息与正编辑 Chunk 不符,请重新获取Chunk纹理.");
                return;
            }

            // 获取当前 chunk 纹理
            System.String[] strTexture = MFramework.Instance.Scene.GetTerrainCurrentChunkTexture();

            if (m_textBoxTex0.Text != strTexture[0] ||
                m_textBoxTex1.Text != strTexture[1] ||
                m_textBoxTex2.Text != strTexture[2] ||
                m_textBoxTex3.Text != strTexture[3])
            {
                MessageBox.Show("当前显示纹理信息与正编辑 Chunk 不符,请重新获取Chunk纹理.");
                return;
            }

            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);
            SelectMaterialDlg m_picselectdlg = new SelectMaterialDlg();
            m_picselectdlg.ShowDialog();
            string selectedMaterial = m_picselectdlg.get_CurrentSelectedPic();
            if (!selectedMaterial.Equals(""))
            {

               if (MFramework.Instance.Scene.ReplaceChunkTexture(iLayer, selectedMaterial))
               {
                   m_buttonGetCurrentChunkTex.PerformClick();

                   if (m_radioButtonTex0.Checked)
                   {
                       m_radioButtonTex0.Checked = true;
                       m_radioButtonTex0_CheckedChanged(m_radioButtonTex0, new EventArgs());
                   }
                   else if (m_radioButtonTex1.Checked)
                   {
                       m_radioButtonTex1.Checked = true;
                       m_radioButtonTex1_CheckedChanged(m_radioButtonTex1, new EventArgs());
                   }
                   else if (m_radioButtonTex2.Checked)
                   {
                       m_radioButtonTex2.Checked = true;
                       m_radioButtonTex2_CheckedChanged(m_radioButtonTex2, new EventArgs());
                   }
                   else if (m_radioButtonTex3.Checked)
                   {
                       m_radioButtonTex3.Checked = true;
                       m_radioButtonTex3_CheckedChanged(m_radioButtonTex3, new EventArgs());
                   }

               }
               else
               {
                   MessageBox.Show("该纹理不存在或已在某层使用。");
               }

            }

        }

        // 删除纹理 [5/4/2009 hemeng ]
        private void DeleteChunkTexture(int iLayer)
        {
            // 判断当前显示的chunk纹理信息与当前正编辑chunk是否相同
            if (m_iCurrentEditChunk != MFramework.Instance.Scene.GetCurrentEditChunkID())
            {
                MessageBox.Show("当前显示纹理信息与正编辑 Chunk 不符,请重新获取Chunk纹理.");
                return;
            }

            // 获取当前 chunk 纹理
            System.String[] strTexture = MFramework.Instance.Scene.GetTerrainCurrentChunkTexture();

            if (m_textBoxTex0.Text != strTexture[0] ||
                m_textBoxTex1.Text != strTexture[1] ||
                m_textBoxTex2.Text != strTexture[2] ||
                m_textBoxTex3.Text != strTexture[3])
            {
                MessageBox.Show("当前显示纹理信息与正编辑 Chunk 不符,请重新获取Chunk纹理.");
                return;
            }

            if (MFramework.Instance.Scene.DeleteChunkTexture(iLayer))
            {
                m_buttonGetCurrentChunkTex.PerformClick();

                //if (m_radioButtonTex0.Checked)
                //{
                //    m_radioButtonTex0.Checked = true;
                //    m_radioButtonTex0_CheckedChanged(m_radioButtonTex0, new EventArgs());
                //}
                //else if (m_radioButtonTex1.Checked)
                //{
                //    m_radioButtonTex1.Checked = true;
                //    m_radioButtonTex1_CheckedChanged(m_radioButtonTex1, new EventArgs());
                //}
                //else if (m_radioButtonTex2.Checked)
                //{
                //    m_radioButtonTex2.Checked = true;
                //    m_radioButtonTex2_CheckedChanged(m_radioButtonTex2, new EventArgs());
                //}
                //else if (m_radioButtonTex3.Checked)
                //{
                //    m_radioButtonTex3.Checked = true;
                //    m_radioButtonTex3_CheckedChanged(m_radioButtonTex3, new EventArgs());
                //}
            }
        }

        private void m_buttonTex0_Click(object sender, EventArgs e)
        {
            // 更换第一层纹理
            ChangeChunkTexture(0);
        }

        private void m_buttonTex1_Click(object sender, EventArgs e)
        {
            ChangeChunkTexture(1);
        }

        private void m_buttonTex2_Click(object sender, EventArgs e)
        {
            ChangeChunkTexture(2);
        }

        private void m_buttonTex3_Click(object sender, EventArgs e)
        {
            ChangeChunkTexture(3);
        }

        public void SetCurrentEditChunk(int iIndex)
        {
            m_iCurrentEditChunk = iIndex;

        }

        private void m_buttonGetCurrentChunkTex_Click(object sender, EventArgs e)
        {

            // 获取当前 chunk 纹理
            System.String[] strTexture = MFramework.Instance.Scene.GetTerrainCurrentChunkTexture();

            m_textBoxTex0.Text = strTexture[0];
            m_textBoxTex1.Text = strTexture[1];
            m_textBoxTex2.Text = strTexture[2];
            m_textBoxTex3.Text = strTexture[3];

            int iInUseLayer = MFramework.Instance.Scene.GetTerrainInUseTextureLayer();
            m_iCurrentEditChunk = MFramework.Instance.Scene.GetCurrentEditChunkID();

            if (iInUseLayer==0)
            {
                m_radioButtonTex0.Checked = true;
                m_radioButtonTex0.PerformClick();
            }
            else if (iInUseLayer == 1)
            {
                m_radioButtonTex1.Checked = true;
                m_radioButtonTex1.PerformClick();
            }
            else if (iInUseLayer == 2)
            {
                m_radioButtonTex2.Checked = true;
                m_radioButtonTex2.PerformClick();
            }
            else if (iInUseLayer == 3)
            {
                m_radioButtonTex3.Checked = true;
                m_radioButtonTex3.PerformClick();
            }         
        }

        // 将 dds 扩展名文件名转换为 bmp 扩展名文件名
        private string DDSFileNameToBMPFileName(string strDDSFile)
        {
            int iLastDotPos = strDDSFile.LastIndexOf('.');
            if (iLastDotPos > 0)
            {
                return strDDSFile.Substring(0, iLastDotPos) + ".bmp";
            }
            else
            {
                return null;
            }
        }

        private void m_radioButtonTex0_CheckedChanged(object sender, EventArgs e)
        {
            if (m_radioButtonTex0.Checked != true)
                return;

            if (m_iCurrentEditChunk != MFramework.Instance.Scene.GetCurrentEditChunkID())
            {
                MessageBox.Show("当前显示纹理信息与正编辑 Chunk 不符,请重新获取Chunk纹理.");
                return;
            }

            if (m_textBoxTex0.Text == "")
            {
                m_radioButtonTex0.Checked = false;
                MessageBox.Show("请不要选择空纹理.");
                return;
            }
            m_lastSelectedMaterial = m_selectedMaterial;
            m_selectedMaterial = m_textBoxTex0.Text;
            m_lastPreviewImage = m_previewImage;
            m_previewImage = DDSFileNameToBMPFileName(m_selectedMaterial);

            m_pictureBoxMaterialPreview.Load(m_previewImage);
            m_pictureBoxMaterialPreview2.Load(m_previewImage);

            SendSettings(MTerrain.EPainterType.PT_TERRAIN_MATERIAL);

        }

        private void m_radioButtonTex1_CheckedChanged(object sender, EventArgs e)
        {
            if (m_radioButtonTex1.Checked != true)
                return;

            if (m_iCurrentEditChunk != MFramework.Instance.Scene.GetCurrentEditChunkID())
            {
                MessageBox.Show("当前显示纹理信息与正编辑 Chunk 不符,请重新获取Chunk纹理.");
                return;
            }

            if (m_textBoxTex1.Text == "")
            {
                m_radioButtonTex1.Checked = false;
                MessageBox.Show("请不要选择空纹理.");
                return;
            }

            m_lastSelectedMaterial = m_selectedMaterial;
            m_selectedMaterial = m_textBoxTex1.Text;
            m_lastPreviewImage = m_previewImage;
            m_previewImage = DDSFileNameToBMPFileName(m_selectedMaterial);

            m_pictureBoxMaterialPreview.Load(m_previewImage);
            m_pictureBoxMaterialPreview2.Load(m_previewImage);

            SendSettings(MTerrain.EPainterType.PT_TERRAIN_MATERIAL);
        }

        private void m_radioButtonTex2_CheckedChanged(object sender, EventArgs e)
        {
            if (m_radioButtonTex2.Checked != true)
                return;

            if (m_iCurrentEditChunk != MFramework.Instance.Scene.GetCurrentEditChunkID())
            {
                MessageBox.Show("当前显示纹理信息与正编辑 Chunk 不符,请重新获取Chunk纹理.");
                return;
            }

            if (m_textBoxTex2.Text == "")
            {
                m_radioButtonTex2.Checked = false;
                MessageBox.Show("请不要选择空纹理.");
                return;
            }
            m_lastSelectedMaterial = m_selectedMaterial;
            m_selectedMaterial = m_textBoxTex2.Text;
            m_lastPreviewImage = m_previewImage;
            m_previewImage = DDSFileNameToBMPFileName(m_selectedMaterial);

            m_pictureBoxMaterialPreview.Load(m_previewImage);
            m_pictureBoxMaterialPreview2.Load(m_previewImage);

            SendSettings(MTerrain.EPainterType.PT_TERRAIN_MATERIAL);
        }

        private void m_radioButtonTex3_CheckedChanged(object sender, EventArgs e)
        {
            if (m_radioButtonTex3.Checked != true)
                return;

            if (m_iCurrentEditChunk != MFramework.Instance.Scene.GetCurrentEditChunkID())
            {
                MessageBox.Show("当前显示纹理信息与正编辑 Chunk 不符,请重新获取Chunk纹理.");
                return;
            }

            if (m_textBoxTex3.Text == "")
            {
                m_radioButtonTex3.Checked = false;
                MessageBox.Show("请不要选择空纹理.");
                return;
            }
            m_lastSelectedMaterial = m_selectedMaterial;
            m_selectedMaterial = m_textBoxTex3.Text;
            m_lastPreviewImage = m_previewImage;
            m_previewImage = DDSFileNameToBMPFileName(m_selectedMaterial);

            m_pictureBoxMaterialPreview.Load(m_previewImage);
            m_pictureBoxMaterialPreview2.Load(m_previewImage);

            SendSettings(MTerrain.EPainterType.PT_TERRAIN_MATERIAL);
        }

        private void m_SelectBrushTextureMenuItem_Click(object sender, EventArgs e)
        {
            // 选择笔触纹理
            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);

            OpenFileDialog openFiledlg = new OpenFileDialog();
            openFiledlg.InitialDirectory = Application.StartupPath+"\\Brush";
            openFiledlg.Filter = "bmp files (*.bmp)|*.bmp|All files (*.*)|*.*";
            openFiledlg.FilterIndex = 1;
            openFiledlg.RestoreDirectory = true;

            if (openFiledlg.ShowDialog() == DialogResult.OK)
            {
                MFramework.Instance.Scene.SetBrushTexture(openFiledlg.FileName);
                m_BrushBitmap = new Bitmap(openFiledlg.FileName);
            }   
        }

        private void m_ClearBrushTextureMenuItem_Click(object sender, EventArgs e)
        {
            // 清除笔触纹理
            MFramework.Instance.Scene.SetBrushTexture(null);
            m_BrushBitmap = null;
        }

        private void m_SelectColorBtn_Click(object sender, EventArgs e)
        {
            // 选择顶点颜色
            ColorDialog colorDlg = new ColorDialog();
            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                m_pictureBoxBrushColorPreview.BackColor = colorDlg.Color;
                MFramework.Instance.Scene.SetInUseVertexColor(colorDlg.Color);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            // 刷地表纹理
            if (m_checkBoxPaintTexture.Checked==false 
                    && m_checkBoxPaintColor.Checked==false
                && m_checkBoxPaintWater.Checked == false)
            {
                MessageBox.Show("刷地表纹理 、 刷顶点颜色 、刷水波至少得选一个。");
                m_checkBoxPaintTexture.Checked = true;
            }

            if (m_checkBoxPaintColor.Checked || m_checkBoxPaintTexture.Checked)
            {
                m_checkBoxPaintWater.Checked = false;
            }

            MFramework.Instance.Scene.SetPaintTerrainState(m_checkBoxPaintTexture.Checked, m_checkBoxPaintColor.Checked, m_checkBoxPaintWater.Checked);
            
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            // 刷顶点颜色
            if (m_checkBoxPaintTexture.Checked == false
                    && m_checkBoxPaintColor.Checked == false
                    && m_checkBoxPaintWater.Checked == false)
            {
                MessageBox.Show("刷地表纹理 、 刷顶点颜色 、刷水波至少得选一个。");
                m_checkBoxPaintColor.Checked = true;
            }

            if(m_checkBoxPaintColor.Checked || m_checkBoxPaintTexture.Checked)
            {
                m_checkBoxPaintWater.Checked = false;
            }

            MFramework.Instance.Scene.SetPaintTerrainState(m_checkBoxPaintTexture.Checked, m_checkBoxPaintColor.Checked, m_checkBoxPaintWater.Checked);
        }

        private void m_checkBoxPaintAddTo_CheckedChanged(object sender, EventArgs e)
        {
            // 叠加 check box
        }

        private void m_FlatTerrainMenuItem_Click(object sender, EventArgs e)
        {
            m_FlatTerrainMenuItem.Checked = true;
            m_UpTerrainMenuItem.Checked = false;
            m_FallTerrainMenuItem.Checked = false;
            m_SmoothTerrainMenuItem.Checked = false;
            MFramework.Instance.Scene.SetTerrainOperation(0);
        }

        private void m_UpTerrainMenuItem_Click(object sender, EventArgs e)
        {
            m_FlatTerrainMenuItem.Checked = false;
            m_UpTerrainMenuItem.Checked = true;
            m_FallTerrainMenuItem.Checked = false;
            m_SmoothTerrainMenuItem.Checked = false;
            MFramework.Instance.Scene.SetTerrainOperation(1);
        }

        private void m_FallTerrainMenuItem_Click(object sender, EventArgs e)
        {
            m_FlatTerrainMenuItem.Checked = false;
            m_UpTerrainMenuItem.Checked = false;
            m_FallTerrainMenuItem.Checked = true;
            m_SmoothTerrainMenuItem.Checked = false;
            MFramework.Instance.Scene.SetTerrainOperation(2);
        }

        private void m_SmoothTerrainMenuItem_Click(object sender, EventArgs e)
        {
            m_FlatTerrainMenuItem.Checked = false;
            m_UpTerrainMenuItem.Checked = false;
            m_FallTerrainMenuItem.Checked = false;
            m_SmoothTerrainMenuItem.Checked = true;
            MFramework.Instance.Scene.SetTerrainOperation(3);
        }

        private void m_PaintAddUpMenuItem_Click(object sender, EventArgs e)
        {
            // 纹理刷图叠加
            if (m_PaintAddUpMenuItem.Checked == true)
            {
                m_PaintAddUpMenuItem.Checked = false;
            }
            else
            {
                m_PaintAddUpMenuItem.Checked = true;
            }

            MFramework.Instance.Scene.SetPaintTerrainAddTo(m_PaintAddUpMenuItem.Checked);

        }

        private void m_ShowChunkBorderCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            MViewportManager.Instance.SetShowChunkBorder(m_ShowChunkBorderCheckBox.Checked);
        }

        private void m_pictureBoxPropColorPreview_Click(object sender, EventArgs e)
        {
            // 点击表示色框,弹出颜色选择框，改变当前属性表示色

        }

        private void m_AddSurPropBtn_Click(object sender, EventArgs e)
        {
            // 添加 地表属性
            stTerrainSurfaceProperty pTerrProp = new stTerrainSurfaceProperty();
            MFramework.Instance.Scene.AddTerrainProperty(pTerrProp);
        }

        private void m_TerrSurPropComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.String selPropName = m_TerrSurPropComboBox.SelectedItem as System.String;
            foreach(MTerrainSurfaceProperty terrProp in m_TerrSurPropList)
            {
                if (terrProp.Name.Equals(selPropName))
                {
                    m_TerrPropIDLabel.Text = terrProp.ID.ToString();
                    m_pictureBoxPropColorPreview.BackColor = System.Drawing.Color.FromArgb((int)(terrProp.R * 255), (int)(terrProp.G * 255), (int)(terrProp.B * 255));
                    MFramework.Instance.Scene.SetCurrentTerrainProperty(terrProp.ID);
                    
                }
            }
        }

        private void btn_BakeShadow_Click(object sender, EventArgs e)
        {
            // 之前老翟 shadow map 的烘焙
            //MFramework.Instance.Scene.Set_BakeShadow( true );

            if (m_PanelShadowTools==null || m_PanelShadowTools.IsDisposed)
            {
                m_PanelShadowTools = new ShadowPanel();
                m_PanelShadowTools.Show();
            }
            else
            {
                m_PanelShadowTools.Activate();
            }

        }

        private void m_btnImportShadow_Click(object sender, EventArgs e)
        {
            // 选择阴影纹理`
            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);

            OpenFileDialog openFiledlg = new OpenFileDialog();
            openFiledlg.InitialDirectory = Application.StartupPath;
            openFiledlg.Filter = "dds files (*.dds)|*.dds|All files (*.*)|*.*";
            openFiledlg.FilterIndex = 1;
            openFiledlg.RestoreDirectory = true;

            if (openFiledlg.ShowDialog() == DialogResult.OK)
            {
                MFramework.Instance.Scene.ImportShadowTexture(openFiledlg.FileName);
            }  
        }

        private void m_btnExportShadow_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Filter = "dds files (*.dds)|*.dds|All files (*.*)|*.*";
            saveFileDlg.FilterIndex = 1;
            saveFileDlg.RestoreDirectory = true;

            if (saveFileDlg.ShowDialog() == DialogResult.OK)
            {
                MFramework.Instance.Scene.ExportShadowTexture(saveFileDlg.FileName);
            }
        }

        private void m_btnTextureCombineTool_Click(object sender, EventArgs e)
        {

        }

        private void m_btnLayerFogTools_Click(object sender, EventArgs e)
        {
            if (m_panelFogTools == null || m_panelFogTools.IsDisposed)
            {
                m_panelFogTools = new FogPanel();
                m_panelFogTools.Show();
            }
            else
            {
                m_panelFogTools.Activate();
            }
        }

        private void m_btnHeightCheck_Click(object sender, EventArgs e)
        {
            if (m_panelHeightCheckTool == null || m_panelHeightCheckTool.IsDisposed)
            {
                m_panelHeightCheckTool = new TerrainHeightCheckPanel();
                m_panelHeightCheckTool.Show();
            }
            else
            {
                m_panelHeightCheckTool.Activate();
            }
        }

        private void m_btnBakeLightColorToVertex_Click(object sender, EventArgs e)
        {
            //MFramework.Instance.Scene.SaveSceneTo3DS("e:\\scene.3ds");
            if (m_panelBaker == null || m_panelBaker.IsDisposed)
            {
                m_panelBaker = new TerrainBakeShadowPanel();
                m_panelBaker.Show();
            }
            else
            {
                m_panelBaker.Activate();
            }
        }

        private void m_btnRegionTool_Click(object sender, EventArgs e)
        {
            if (!MFramework.Instance.Scene.HasTerrain())
            {
                MessageBox.Show("请先创建地形！");
            }
            else
            {
                if (m_panelRegionTool == null || m_panelRegionTool.IsDisposed)
                {
                    m_panelRegionTool = new RegionToolPanel();
                    m_panelRegionTool.Show();
                }
                else
                {
                    m_panelRegionTool.Activate();
                }
            }
        }

        private void m_btnWaterTools_Click(object sender, EventArgs e)
        {
            //设置为绘制水域
            //SendSettings(3);
            if (m_panelWater == null || m_panelWater.IsDisposed)
            {
                m_panelWater = new WaterPanel();
                m_panelWater.Show();
            }
            else
            {
                m_panelWater.Activate();
            }
        }

        private void m_btnBakeLightToTerrainVertexColor_Click(object sender, EventArgs e)
        {
            // 测试地形顶点烘焙
            TerrainBakeLightPanel form = new TerrainBakeLightPanel();
            form.Show();
        }

        private void m_btnChunkEntityManager_Click(object sender, EventArgs e)
        {

            if (MChunkEntityManager.Instance == null)
            {
                MessageBox.Show("请先创建地形。");
                return;
            }

            ChunkEntityManagePanel panel = ChunkEntityManagePanel.Instance;
            panel.Show();
            panel.Activate();
        }

        private void m_toolSelectPaintMode_DropDownOpening(object sender, EventArgs e)
        {
            //获得当前笔刷类型
            //MBrushParameter tempBrush = ( SettingsService.GetSettingsObject("TerrainBrush", SettingsCategory.Temp) as MBrushParameter);

            m_ToolStripMenuItem_Water.Checked = false;
            m_ToolStripMenuItem_Terrain.Checked = false;
            m_ToolStripMenuItem_Material.Checked = false;
            m_ToolStripMenuItem_Proprity.Checked = false;
            m_ToolStripMenuItem_GridArea.Checked = false;
            m_ToolStripMenuItemRemoveEntity.Checked = false;

            if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_VERTEX)
            {
               m_ToolStripMenuItem_Terrain.Checked = true;
            }
            else if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_MATERIAL)
            {
                m_ToolStripMenuItem_Material.Checked = true;
            }
            else if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_PROPERTY) 
            {
                m_ToolStripMenuItem_Proprity.Checked = true;
            }
            //else if (m_PaintMode == MTerrain.EPainterType.PT_WATER_SURFACE)
            //{
            //    m_ToolStripMenuItem_Water.Checked = true;
            //}
            else if (m_PaintMode == MTerrain.EPainterType.PT_TERRAIN_GRID_AREA)
            {
                m_ToolStripMenuItem_GridArea.Checked = true;
            }
            else if (m_PaintMode == MTerrain.EPainterType.PT_REMOVE_ENTITY)
            {
                m_ToolStripMenuItemRemoveEntity.Checked = true;
            }
        }

        private void m_ToolStripMenuItem_Terrain_Click(object sender, EventArgs e)
        {
            SetPaintMode(MTerrain.EPainterType.PT_TERRAIN_VERTEX);
        }

        private void m_ToolStripMenuItem_Material_Click(object sender, EventArgs e)
        {
            SetPaintMode(MTerrain.EPainterType.PT_TERRAIN_MATERIAL);
        }

        private void m_ToolStripMenuItem_Proprity_Click(object sender, EventArgs e)
        {
            SetPaintMode(MTerrain.EPainterType.PT_TERRAIN_PROPERTY);
        }

        private void m_ToolStripMenuItem_Water_Click(object sender, EventArgs e)
        {
            //SetPaintMode(MTerrain.EPainterType.PT_WATER_SURFACE);
            SetPaintMode(MTerrain.EPainterType.PT_TERRAIN_GRID_AREA);
        }

        private void m_ToolStripMenuItem_GridArea_Click(object sender, EventArgs e)
        {
            SetPaintMode(MTerrain.EPainterType.PT_TERRAIN_GRID_AREA);
        }

        private void m_ToolStripMenuItemRemoveEntity_Click(object sender, EventArgs e)
        {
            SetPaintMode(MTerrain.EPainterType.PT_REMOVE_ENTITY);
        }

        private void m_btnGridBaseArea_Click(object sender, EventArgs e)
        {
            GridBaseAreaPanel form = GridBaseAreaPanel.Instance;
            form.Show();
            form.Activate();
        }

        private void ExportCollisionBlockBtn_Click(object sender, EventArgs e)
        {
            MTerrain.Instance.ExportGridProperty();
        }

        private void ImportCollisionBlockBtn_Click(object sender, EventArgs e)
        {
            MTerrain.Instance.ImportGridProperty();
        }

        private void m_btnPostEffectTools_Click(object sender, EventArgs e)
        {
            if (m_panelPostEffect == null || m_panelPostEffect.IsDisposed)
            {
                m_panelPostEffect = new PostEffectPanel();
                m_panelPostEffect.Show();
            }
            else
            {
                m_panelPostEffect.Activate();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SnapToolPanel form = new SnapToolPanel();
            form.Show();

        }

        private void CreateRegProByMtlBtn_Click(object sender, EventArgs e)
        {
            MTerrain.Instance.CreateGridPropertyByMaterial();
        }

        private void m_btnColorPalette_Click(object sender, EventArgs e)
        {
            // 打开自定义调色板
            ColorPalettePanel panel = new ColorPalettePanel();
            panel.TopMost = true;
            panel.Show();
        }

        private void m_btnEntityAffectLights_Click(object sender, EventArgs e)
        {
            EntityAffectLightsPanel form = new EntityAffectLightsPanel();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ( m_textBox_FindPos_X.Text.Equals("") )
            {
                MessageBox.Show("请输入参数X");
                return; 
            }
            if (m_textBox_FindPos_Y.Text.Equals(""))
            {
                MessageBox.Show("请输入参数Y");
                return; 
            }

            int iFindPosX, iFindPosY;

            iFindPosX = Convert.ToInt32( m_textBox_FindPos_X.Text );
            iFindPosY = Convert.ToInt32( m_textBox_FindPos_Y.Text );

            MTerrain.Instance.CameraGoToPos( iFindPosX, iFindPosY );
        }

        private void m_buttonTexDelete1_Click(object sender, EventArgs e)
        {
            DeleteChunkTexture(1);
        }

        private void m_buttonTexDelete2_Click(object sender, EventArgs e)
        {
            DeleteChunkTexture(2);
        }

        private void m_buttonTexDelete3_Click(object sender, EventArgs e)
        {
            DeleteChunkTexture(3);
        }

        private void GetGridPropertyPercentBtn_Click(object sender, EventArgs e)
        {
            System.String selPropName = m_TerrSurPropComboBox.SelectedItem as System.String;
            foreach (MTerrainSurfaceProperty terrProp in m_TerrSurPropList)
            {
                if (terrProp.Name.Equals(selPropName))
                {
                    m_TerrPropIDLabel.Text = terrProp.ID.ToString();
                    m_pictureBoxPropColorPreview.BackColor = System.Drawing.Color.FromArgb((int)(terrProp.R * 255), (int)(terrProp.G * 255), (int)(terrProp.B * 255));
                    int iPercent = MTerrain.Instance.GetGridPropertyPercent(terrProp.ID);
                    GridPropertyPerctextBox.Text = Convert.ToString(iPercent);
                }
            }
        }

        private void btn_HeightMap_Click(object sender, EventArgs e)
        {
            HeightMapConfig form = HeightMapConfig.Instance;
            form.Visible = true;
            form.Show();
        }

        private void m_btnEntityExportSetting_Click(object sender, EventArgs e)
        {
            // Entity 导出设置
            EntityExportSettingPanel dlg = new EntityExportSettingPanel();
            dlg.ShowDialog();
        }

        private void m_btnImportVertexColor_Click(object sender, EventArgs e)
        {
            //// 选择阴影纹理`
            //System.IO.Directory.SetCurrentDirectory(Application.StartupPath);

            //OpenFileDialog openFiledlg = new OpenFileDialog();
            //openFiledlg.InitialDirectory = Application.StartupPath;
            //openFiledlg.Filter = "dds files (*.dds)|*.dds|tga files (*.tga)|*.tga|All files (*.*)|*.*";
            //openFiledlg.FilterIndex = 1;
            //openFiledlg.RestoreDirectory = true;

            //if (openFiledlg.ShowDialog() == DialogResult.OK)
            //{
            //    MTerrain.Instance.ApplyVertexColorTexture(openFiledlg.FileName, 80);
            //}  
        }



        private void m_btnLightMapPanel_Click(object sender, EventArgs e)
        {
            GlobalIlluminationPanel dlg = GlobalIlluminationPanel.Instance;
            dlg.ShowDialog(null);
        }

        private void m_btnPreview_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetPreviewEntityLightMap(true);
        }

        private void m_ShowAABBCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            MViewportManager.Instance.SetShowAABB(m_ShowAABBCheckBox.Checked);
         }

        private void m_btnSetAABBScale_Click(object sender, EventArgs e)
        {
            AABBEditPanel form = new AABBEditPanel();
            form.Show();
        }

        private void m_ShowChunkBorderCheckBox_CheckedChanged_1(object sender, EventArgs e)
        {
            MViewportManager.Instance.SetShowChunkBorder(m_ShowChunkBorderCheckBox.Checked);
        }

        private void m_btnCityLevelEditor_Click(object sender, EventArgs e)
        {
            // 城市等级编辑
            CityLevelEditPanel panel = new CityLevelEditPanel();
            panel.Show();
        }
        
        private void m_checkBoxPaintWater_CheckedChanged(object sender, EventArgs e)
        {
            if (m_checkBoxPaintWater.Checked == true)
            {
                m_checkBoxPaintTexture.Checked = false;
                m_checkBoxPaintColor.Checked = false;
            }

            // 刷水波
            if (m_checkBoxPaintTexture.Checked == false
                    && m_checkBoxPaintColor.Checked == false
                && m_checkBoxPaintWater.Checked == false)
            {
                MessageBox.Show("刷地表纹理 、 刷顶点颜色 、刷水波至少得选一个。");
                m_checkBoxPaintWater.Checked = true;
            }           

            MFramework.Instance.Scene.SetPaintTerrainState(m_checkBoxPaintTexture.Checked, m_checkBoxPaintColor.Checked,m_checkBoxPaintWater.Checked);
            MFramework.Instance.Scene.SetWaterSceneEnabled(m_checkBoxPaintWater.Checked);
        }

        private void btn_PostEffect_Click(object sender, EventArgs e)
        {
            if (m_panelPostEffect == null || m_panelPostEffect.IsDisposed)
            {
                m_panelPostEffect = new PostEffectPanel();
                m_panelPostEffect.Show();
            }
            else
            {
                m_panelPostEffect.Activate();
            }
        }

        private void btn_ModelScreenShotor_Click(object sender, EventArgs e)
        {
            if (m_panelModelScreenShotor == null || m_panelModelScreenShotor.IsDisposed)
            {
                m_panelModelScreenShotor = new ModelScreenshot();
                m_panelModelScreenShotor.Show();
            }
            else
            {
                m_panelModelScreenShotor.Activate();
            }
        }

        private void btn_BackLightToEntity_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.BackLightToEntities();
        }
  

        //private void m_btnPreviewLightMap_Click(object sender, EventArgs e)
        //{
        //    MFramework.Instance.Scene.SetPreviewEntityLightMap(m_cbPreviewLightMap.Checked);
        //}

    }
}