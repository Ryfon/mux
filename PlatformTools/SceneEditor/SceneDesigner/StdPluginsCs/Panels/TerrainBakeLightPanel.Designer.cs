﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class TerrainBakeLightPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_listBoxAllLights = new System.Windows.Forms.ListBox();
            this.m_listBoxSelectedLights = new System.Windows.Forms.ListBox();
            this.m_btnAddLight = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_btnRemove = new System.Windows.Forms.Button();
            this.m_btnBake = new System.Windows.Forms.Button();
            this.m_radioButtonMultiply = new System.Windows.Forms.RadioButton();
            this.m_radioButtonAdd = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.m_btnPreview = new System.Windows.Forms.Button();
            this.m_pictureBoxColor = new System.Windows.Forms.PictureBox();
            this.m_textBoxR = new System.Windows.Forms.TextBox();
            this.m_textBoxG = new System.Windows.Forms.TextBox();
            this.m_textBoxB = new System.Windows.Forms.TextBox();
            this.m_btnFlood = new System.Windows.Forms.Button();
            this.m_btnAllLightSelectAll = new System.Windows.Forms.Button();
            this.m_btnAllLightClear = new System.Windows.Forms.Button();
            this.m_btnSelLightSelectAll = new System.Windows.Forms.Button();
            this.m_btnSelLightClear = new System.Windows.Forms.Button();
            this.m_btnOneBtnBack = new System.Windows.Forms.Button();
            this.btn_Preview_By_preview = new System.Windows.Forms.Button();
            this.btn_Preview_By_sun = new System.Windows.Forms.Button();
            this.m_btnOneBtnPreview = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxColor)).BeginInit();
            this.SuspendLayout();
            // 
            // m_listBoxAllLights
            // 
            this.m_listBoxAllLights.FormattingEnabled = true;
            this.m_listBoxAllLights.ItemHeight = 12;
            this.m_listBoxAllLights.Location = new System.Drawing.Point(16, 86);
            this.m_listBoxAllLights.Name = "m_listBoxAllLights";
            this.m_listBoxAllLights.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.m_listBoxAllLights.Size = new System.Drawing.Size(223, 184);
            this.m_listBoxAllLights.TabIndex = 0;
            // 
            // m_listBoxSelectedLights
            // 
            this.m_listBoxSelectedLights.FormattingEnabled = true;
            this.m_listBoxSelectedLights.ItemHeight = 12;
            this.m_listBoxSelectedLights.Location = new System.Drawing.Point(278, 86);
            this.m_listBoxSelectedLights.Name = "m_listBoxSelectedLights";
            this.m_listBoxSelectedLights.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.m_listBoxSelectedLights.Size = new System.Drawing.Size(223, 184);
            this.m_listBoxSelectedLights.TabIndex = 1;
            // 
            // m_btnAddLight
            // 
            this.m_btnAddLight.Location = new System.Drawing.Point(245, 133);
            this.m_btnAddLight.Name = "m_btnAddLight";
            this.m_btnAddLight.Size = new System.Drawing.Size(27, 25);
            this.m_btnAddLight.TabIndex = 2;
            this.m_btnAddLight.Text = "->";
            this.m_btnAddLight.UseVisualStyleBackColor = true;
            this.m_btnAddLight.Click += new System.EventHandler(this.m_btnAddLight_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "场景所有灯光";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(276, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "影响地形的灯光";
            // 
            // m_btnRemove
            // 
            this.m_btnRemove.Location = new System.Drawing.Point(245, 175);
            this.m_btnRemove.Name = "m_btnRemove";
            this.m_btnRemove.Size = new System.Drawing.Size(27, 25);
            this.m_btnRemove.TabIndex = 5;
            this.m_btnRemove.Text = "<-";
            this.m_btnRemove.UseVisualStyleBackColor = true;
            this.m_btnRemove.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // m_btnBake
            // 
            this.m_btnBake.Location = new System.Drawing.Point(388, 319);
            this.m_btnBake.Name = "m_btnBake";
            this.m_btnBake.Size = new System.Drawing.Size(75, 28);
            this.m_btnBake.TabIndex = 7;
            this.m_btnBake.Text = "烘焙";
            this.m_btnBake.UseVisualStyleBackColor = true;
            this.m_btnBake.Click += new System.EventHandler(this.m_btnBake_Click);
            // 
            // m_radioButtonMultiply
            // 
            this.m_radioButtonMultiply.AutoSize = true;
            this.m_radioButtonMultiply.Location = new System.Drawing.Point(13, 325);
            this.m_radioButtonMultiply.Name = "m_radioButtonMultiply";
            this.m_radioButtonMultiply.Size = new System.Drawing.Size(107, 16);
            this.m_radioButtonMultiply.TabIndex = 8;
            this.m_radioButtonMultiply.Text = "相乘(默认方法)";
            this.m_radioButtonMultiply.UseVisualStyleBackColor = true;
            this.m_radioButtonMultiply.Visible = false;
            // 
            // m_radioButtonAdd
            // 
            this.m_radioButtonAdd.AutoSize = true;
            this.m_radioButtonAdd.Checked = true;
            this.m_radioButtonAdd.Location = new System.Drawing.Point(158, 325);
            this.m_radioButtonAdd.Name = "m_radioButtonAdd";
            this.m_radioButtonAdd.Size = new System.Drawing.Size(107, 16);
            this.m_radioButtonAdd.TabIndex = 9;
            this.m_radioButtonAdd.TabStop = true;
            this.m_radioButtonAdd.Text = "相加(默认方法)";
            this.m_radioButtonAdd.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 308);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "光照与原顶点色融合方式";
            // 
            // m_btnPreview
            // 
            this.m_btnPreview.Location = new System.Drawing.Point(298, 319);
            this.m_btnPreview.Name = "m_btnPreview";
            this.m_btnPreview.Size = new System.Drawing.Size(75, 28);
            this.m_btnPreview.TabIndex = 11;
            this.m_btnPreview.Text = "预览";
            this.m_btnPreview.UseVisualStyleBackColor = true;
            this.m_btnPreview.Click += new System.EventHandler(this.m_btnPreview_Click);
            // 
            // m_pictureBoxColor
            // 
            this.m_pictureBoxColor.BackColor = System.Drawing.Color.Black;
            this.m_pictureBoxColor.Location = new System.Drawing.Point(12, 12);
            this.m_pictureBoxColor.Name = "m_pictureBoxColor";
            this.m_pictureBoxColor.Size = new System.Drawing.Size(45, 25);
            this.m_pictureBoxColor.TabIndex = 12;
            this.m_pictureBoxColor.TabStop = false;
            this.m_pictureBoxColor.Click += new System.EventHandler(this.m_pictureBoxColor_Click);
            // 
            // m_textBoxR
            // 
            this.m_textBoxR.Location = new System.Drawing.Point(66, 18);
            this.m_textBoxR.Name = "m_textBoxR";
            this.m_textBoxR.Size = new System.Drawing.Size(44, 21);
            this.m_textBoxR.TabIndex = 13;
            this.m_textBoxR.Text = "0";
            // 
            // m_textBoxG
            // 
            this.m_textBoxG.Location = new System.Drawing.Point(116, 18);
            this.m_textBoxG.Name = "m_textBoxG";
            this.m_textBoxG.Size = new System.Drawing.Size(44, 21);
            this.m_textBoxG.TabIndex = 14;
            this.m_textBoxG.Text = "0";
            // 
            // m_textBoxB
            // 
            this.m_textBoxB.Location = new System.Drawing.Point(166, 18);
            this.m_textBoxB.Name = "m_textBoxB";
            this.m_textBoxB.Size = new System.Drawing.Size(44, 21);
            this.m_textBoxB.TabIndex = 15;
            this.m_textBoxB.Text = "0";
            // 
            // m_btnFlood
            // 
            this.m_btnFlood.Location = new System.Drawing.Point(244, 17);
            this.m_btnFlood.Name = "m_btnFlood";
            this.m_btnFlood.Size = new System.Drawing.Size(129, 20);
            this.m_btnFlood.TabIndex = 16;
            this.m_btnFlood.Text = "填充地形顶点色";
            this.m_btnFlood.UseVisualStyleBackColor = true;
            this.m_btnFlood.Click += new System.EventHandler(this.m_btnFlood_Click);
            // 
            // m_btnAllLightSelectAll
            // 
            this.m_btnAllLightSelectAll.Location = new System.Drawing.Point(47, 273);
            this.m_btnAllLightSelectAll.Name = "m_btnAllLightSelectAll";
            this.m_btnAllLightSelectAll.Size = new System.Drawing.Size(58, 25);
            this.m_btnAllLightSelectAll.TabIndex = 17;
            this.m_btnAllLightSelectAll.Text = "全选";
            this.m_btnAllLightSelectAll.UseVisualStyleBackColor = true;
            this.m_btnAllLightSelectAll.Click += new System.EventHandler(this.m_btnAllLightSelectAll_Click);
            // 
            // m_btnAllLightClear
            // 
            this.m_btnAllLightClear.Location = new System.Drawing.Point(132, 273);
            this.m_btnAllLightClear.Name = "m_btnAllLightClear";
            this.m_btnAllLightClear.Size = new System.Drawing.Size(58, 25);
            this.m_btnAllLightClear.TabIndex = 18;
            this.m_btnAllLightClear.Text = "清除";
            this.m_btnAllLightClear.UseVisualStyleBackColor = true;
            this.m_btnAllLightClear.Click += new System.EventHandler(this.m_btnAllLightClear_Click);
            // 
            // m_btnSelLightSelectAll
            // 
            this.m_btnSelLightSelectAll.Location = new System.Drawing.Point(316, 276);
            this.m_btnSelLightSelectAll.Name = "m_btnSelLightSelectAll";
            this.m_btnSelLightSelectAll.Size = new System.Drawing.Size(58, 25);
            this.m_btnSelLightSelectAll.TabIndex = 19;
            this.m_btnSelLightSelectAll.Text = "全选";
            this.m_btnSelLightSelectAll.UseVisualStyleBackColor = true;
            this.m_btnSelLightSelectAll.Click += new System.EventHandler(this.m_btnSelLightSelectAll_Click);
            // 
            // m_btnSelLightClear
            // 
            this.m_btnSelLightClear.Location = new System.Drawing.Point(405, 276);
            this.m_btnSelLightClear.Name = "m_btnSelLightClear";
            this.m_btnSelLightClear.Size = new System.Drawing.Size(58, 25);
            this.m_btnSelLightClear.TabIndex = 20;
            this.m_btnSelLightClear.Text = "清除";
            this.m_btnSelLightClear.UseVisualStyleBackColor = true;
            this.m_btnSelLightClear.Click += new System.EventHandler(this.m_btnSelLightClear_Click);
            // 
            // m_btnOneBtnBack
            // 
            this.m_btnOneBtnBack.Location = new System.Drawing.Point(18, 370);
            this.m_btnOneBtnBack.Name = "m_btnOneBtnBack";
            this.m_btnOneBtnBack.Size = new System.Drawing.Size(114, 28);
            this.m_btnOneBtnBack.TabIndex = 21;
            this.m_btnOneBtnBack.Text = "快速烘焙";
            this.m_btnOneBtnBack.UseVisualStyleBackColor = true;
            this.m_btnOneBtnBack.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_Preview_By_preview
            // 
            this.btn_Preview_By_preview.Location = new System.Drawing.Point(18, 417);
            this.btn_Preview_By_preview.Name = "btn_Preview_By_preview";
            this.btn_Preview_By_preview.Size = new System.Drawing.Size(114, 23);
            this.btn_Preview_By_preview.TabIndex = 23;
            this.btn_Preview_By_preview.Text = "preview预览";
            this.btn_Preview_By_preview.UseVisualStyleBackColor = true;
            this.btn_Preview_By_preview.Visible = false;
            this.btn_Preview_By_preview.Click += new System.EventHandler(this.btn_Preview_By_preview_Click);
            // 
            // btn_Preview_By_sun
            // 
            this.btn_Preview_By_sun.Location = new System.Drawing.Point(388, 370);
            this.btn_Preview_By_sun.Name = "btn_Preview_By_sun";
            this.btn_Preview_By_sun.Size = new System.Drawing.Size(75, 23);
            this.btn_Preview_By_sun.TabIndex = 24;
            this.btn_Preview_By_sun.Text = "sun预览";
            this.btn_Preview_By_sun.UseVisualStyleBackColor = true;
            this.btn_Preview_By_sun.Click += new System.EventHandler(this.btn_Preview_By_sun_Click);
            // 
            // m_btnOneBtnPreview
            // 
            this.m_btnOneBtnPreview.Location = new System.Drawing.Point(148, 370);
            this.m_btnOneBtnPreview.Name = "m_btnOneBtnPreview";
            this.m_btnOneBtnPreview.Size = new System.Drawing.Size(117, 26);
            this.m_btnOneBtnPreview.TabIndex = 25;
            this.m_btnOneBtnPreview.Text = "快速预览";
            this.m_btnOneBtnPreview.UseVisualStyleBackColor = true;
            this.m_btnOneBtnPreview.Click += new System.EventHandler(this.m_btnOneBtnPreview_Click);
            // 
            // TerrainBakeLightPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 413);
            this.Controls.Add(this.m_btnOneBtnPreview);
            this.Controls.Add(this.btn_Preview_By_sun);
            this.Controls.Add(this.btn_Preview_By_preview);
            this.Controls.Add(this.m_btnOneBtnBack);
            this.Controls.Add(this.m_btnSelLightClear);
            this.Controls.Add(this.m_btnSelLightSelectAll);
            this.Controls.Add(this.m_btnAllLightClear);
            this.Controls.Add(this.m_btnAllLightSelectAll);
            this.Controls.Add(this.m_btnFlood);
            this.Controls.Add(this.m_textBoxB);
            this.Controls.Add(this.m_textBoxG);
            this.Controls.Add(this.m_textBoxR);
            this.Controls.Add(this.m_pictureBoxColor);
            this.Controls.Add(this.m_btnPreview);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_radioButtonAdd);
            this.Controls.Add(this.m_radioButtonMultiply);
            this.Controls.Add(this.m_btnBake);
            this.Controls.Add(this.m_btnRemove);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_btnAddLight);
            this.Controls.Add(this.m_listBoxSelectedLights);
            this.Controls.Add(this.m_listBoxAllLights);
            this.Name = "TerrainBakeLightPanel";
            this.Text = "地形烘焙灯光工具";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.TerrainBakeLightPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox m_listBoxAllLights;
        private System.Windows.Forms.ListBox m_listBoxSelectedLights;
        private System.Windows.Forms.Button m_btnAddLight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button m_btnRemove;
        private System.Windows.Forms.Button m_btnBake;
        private System.Windows.Forms.RadioButton m_radioButtonMultiply;
        private System.Windows.Forms.RadioButton m_radioButtonAdd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button m_btnPreview;
        private System.Windows.Forms.PictureBox m_pictureBoxColor;
        private System.Windows.Forms.TextBox m_textBoxR;
        private System.Windows.Forms.TextBox m_textBoxG;
        private System.Windows.Forms.TextBox m_textBoxB;
        private System.Windows.Forms.Button m_btnFlood;
        private System.Windows.Forms.Button m_btnAllLightSelectAll;
        private System.Windows.Forms.Button m_btnAllLightClear;
        private System.Windows.Forms.Button m_btnSelLightSelectAll;
        private System.Windows.Forms.Button m_btnSelLightClear;
        private System.Windows.Forms.Button m_btnOneBtnBack;
        private System.Windows.Forms.Button btn_Preview_By_preview;
        private System.Windows.Forms.Button btn_Preview_By_sun;
        private System.Windows.Forms.Button m_btnOneBtnPreview;
    }
}