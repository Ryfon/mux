﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class LightDistributeMap : Form
    {
        public LightDistributeMap()
        {
            InitializeComponent();
            m_cbDistributeMapType.SelectedIndex = 0;
        }

        private void m_btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                float fLum = float.Parse(m_tbLightLum.Text);
                MDistributeMapGenerator distributeMap = new MDistributeMapGenerator(fLum);
                distributeMap.SetTargetWnd(m_picBoxDistributeMap.Handle);
                m_picBoxDistributeMap.Image 
                    = distributeMap.GenerateDistributeMap((MDistributeMapGenerator.EMapType)m_cbDistributeMapType.SelectedIndex);
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void m_picBoxDistributeMap_MouseDown(object sender, MouseEventArgs e)
        {
            // 鼠标按下，显示地图坐标
        }
    }
}