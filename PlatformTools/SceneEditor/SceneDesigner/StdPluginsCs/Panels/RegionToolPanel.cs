﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class RegionToolPanel : Form
    {
        public RegionToolPanel()
        {
            InitializeComponent();
            //m_comboBoxRegionType.Items.AddRange(new object[] { "全部区域" });
            m_treeViewRegionList.Nodes.Add("全部区域");

            //初始化区域类型下拉框和区域列表树
            int nIndex = 0;
            string strViewName = MFramework.Instance.Scene.GetRegionPropViewName(nIndex);
            while (!strViewName.Equals(""))
            {
                m_comboBoxRegionType.Items.Add(strViewName);//下拉框
                m_treeViewRegionList.Nodes[0].Nodes.Add(strViewName);//树
                nIndex++;
                strViewName = MFramework.Instance.Scene.GetRegionPropViewName(nIndex);
            }
            m_comboBoxRegionType.SelectedIndex = 1;
            m_LastSelectNode = m_treeViewRegionList.Nodes[0].Nodes[0];
            m_treeViewRegionList.SelectedNode = m_treeViewRegionList.Nodes[0].Nodes[0];

            //////////////////////////////////////////////////////////////////////////
            ///导入刷新区域
            MFramework.Instance.Scene.SetCurrentRegionID(0);
            RefreshRegionTree();
            m_treeViewRegionList.ExpandAll();

            ///默认为更新区域
            m_radioButtonUpdateRegion.Select();

            ///初始化是否渲染区域的checkbox
            m_checkBoxShow.Checked = MFramework.Instance.Scene.GetRenderRegion();
            //////////////////////////////////////////////////////////////////////////
            
        }

        private void m_comboBoxRegionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //创建区域模式
            if (m_radioButtonCreatrRegion.Checked)
            {
                int nRegionTypeSelectIndex = m_comboBoxRegionType.SelectedIndex;
                if (nRegionTypeSelectIndex < 1)
                {
                    m_comboBoxRegionType.SelectedIndex = 1;
                    nRegionTypeSelectIndex = m_comboBoxRegionType.SelectedIndex;
                }
                m_treeViewRegionList.SelectedNode = m_treeViewRegionList.Nodes[0].Nodes[nRegionTypeSelectIndex - 1];
            }

            //更新区域模式
            if (m_radioButtonUpdateRegion.Checked)
            {
                int nRegionTypeSelectIndex = m_comboBoxRegionType.SelectedIndex;
                //if (nRegionTypeSelectIndex < 1)
                //{
                //    m_comboBoxRegionType.SelectedIndex = 1;
                //    nRegionTypeSelectIndex = m_comboBoxRegionType.SelectedIndex;
                //}
                int nCurrentRegionID = MFramework.Instance.Scene.GetCurrentRegionID();
                MFramework.Instance.Scene.SetRegionTypeByID(nCurrentRegionID, nRegionTypeSelectIndex);
                RefreshRegionTree();
                //RefreshRegionProp();
            }

            //显示区域模式

            //MFramework.Instance.Scene.SetGlobalRegionType(m_comboBoxRegionType.SelectedIndex);
        }

        private void m_checkBoxShow_CheckedChanged(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetRenderRegion(m_checkBoxShow.Checked);
        }

        // 区域操作类型的改变
        private void m_radioButtonUpdateRegion_CheckedChanged(object sender, EventArgs e)
        {
            if (MFramework.Instance.Scene.GetCurrentRegionID() == 0)
            {
                //m_radioButtonCreatrRegion.Select();
                //MessageBox.Show("请先创建一个区域！");
            }

            if (m_radioButtonUpdateRegion.Checked)
            {
                MFramework.Instance.Scene.SetRegionOperType(2);
            }
        }

        private void m_radioButtonShowRegion_CheckedChanged(object sender, EventArgs e)
        {
            m_checkBoxShow.Enabled = m_radioButtonShowRegion.Checked;
            m_textBoxRegionName.Enabled = !(m_radioButtonShowRegion.Checked);
            MFramework.Instance.Scene.SetRenderRegion(m_checkBoxShow.Checked);

            if (m_radioButtonShowRegion.Checked)
            {
                MFramework.Instance.Scene.SetRegionOperType(3);
            }
        }

        private void m_radioButtonCreatrRegion_CheckedChanged(object sender, EventArgs e)
        {
            if (m_radioButtonCreatrRegion.Checked)
            {
                MFramework.Instance.Scene.SetRegionOperType(1);
            }
        }

        /// 区域显示树的操作
        private void m_treeViewRegionList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (m_radioButtonUpdateRegion.Checked)
            {
                if (m_treeViewRegionList.SelectedNode.Tag == null)
                {
                    m_treeViewRegionList.SelectedNode = m_LastSelectNode;
                }
                else
                {
                    string strAreaID = m_treeViewRegionList.SelectedNode.Tag.ToString();
                    int nAreaID = int.Parse(strAreaID);
                    MFramework.Instance.Scene.SetCurrentRegionID(nAreaID);
                    
                    //刷新区域属性
                    RefreshRegionProp();
                }

            }
            else
            {
                m_treeViewRegionList.SelectedNode = m_LastSelectNode;
            }
        }

        private void m_treeViewRegionList_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            m_LastSelectNode = m_treeViewRegionList.SelectedNode;
        }

        /// 区域树的刷新函数
        private void RefreshRegionTree()
        {
            int nCurrentRegionID = MFramework.Instance.Scene.GetCurrentRegionID();//记录选中的区域
            int nRegionTypeNum = m_treeViewRegionList.Nodes[0].GetNodeCount(false);//总共有多少种类型的区域
            int nIndex = 0;
            int nTopRegionID = MFramework.Instance.Scene.GetTopRegionID();

            // 清除原先的数据
            while (nIndex < nRegionTypeNum)
            {
                m_treeViewRegionList.Nodes[0].Nodes[nIndex].Nodes.Clear();
                nIndex++;
            }

            nIndex = 1;
            while (nIndex <= nTopRegionID)
            {
                int nRegionType = MFramework.Instance.Scene.GetRegionTypeByID(nIndex);
                if (nRegionType >= 0 && nRegionType <= nRegionTypeNum)
                {
                    string strRegionName = MFramework.Instance.Scene.GetRegionNameByID(nIndex);
                    string strKey = nIndex.ToString();

                    /// 插入数节点
                    TreeNode tn = m_treeViewRegionList.Nodes[0].Nodes[nRegionType].Nodes.Add(strRegionName);
                    tn.Tag = strKey;

                    /// 更新选中的区域
                    if (nCurrentRegionID == 0)
                    {
                        MFramework.Instance.Scene.SetCurrentRegionID(nIndex);
                        m_treeViewRegionList.SelectedNode = tn;
                    }
                    else if (nIndex == nCurrentRegionID)
                    {
                        m_treeViewRegionList.SelectedNode = tn;
                    }
                }

                nIndex++;
            }
        }

        //区域属性刷新函数
        private void RefreshRegionProp()
        {
            int nCurrentRegionID = MFramework.Instance.Scene.GetCurrentRegionID();

            if(nCurrentRegionID == 0)
            {
                DisableRegionProp(1);
            }
            else
            {
                int nCurrentRegionType = MFramework.Instance.Scene.GetRegionTypeByID(nCurrentRegionID);
                if(nCurrentRegionType == 4 || nCurrentRegionType == 5)
                {
                    DisableRegionProp(0);
                    string strMusicFile = MFramework.Instance.Scene.GetRegionMusicInfoByID(nCurrentRegionID,"MusicFile");
                    m_textBoxMusicFileName.Text = strMusicFile;
                    string strVolume = MFramework.Instance.Scene.GetRegionMusicInfoByID(nCurrentRegionID, "Volume");
                    m_textBoxVolume.Text = strVolume;
                    string strMusicLoop = MFramework.Instance.Scene.GetRegionMusicInfoByID(nCurrentRegionID, "MusicLoop");
                    if (strMusicLoop.Equals("1"))
                    {
                        m_checkBoxMusicLoop.Checked = true;
                    } 
                    else
                    {
                        m_checkBoxMusicLoop.Checked = false;
                    }
                    
                }
                else
                {
                    DisableRegionProp(2);
                }

                string strCurrentRegionName = MFramework.Instance.Scene.GetRegionNameByID(nCurrentRegionID);
                m_comboBoxRegionType.SelectedIndex = nCurrentRegionType;
                m_textBoxRegionName.Text = strCurrentRegionName;
            }
        }

        // 区域属性框的禁止与开放
        private void DisableRegionProp(int nDisableType)
        {
            //不禁止，开启所有属性，一般为音乐区域
            if(nDisableType == 0)
            {
                m_comboBoxRegionType.Enabled = true;
                m_textBoxRegionName.Enabled = true;

                m_textBoxMusicFileName.Enabled = true;
                m_textBoxVolume.Enabled = true;
                m_checkBoxMusicLoop.Enabled = true;
            }

            //完全禁止，开启所有属性，一般为音乐区域
            if(nDisableType == 1)
            {
                m_comboBoxRegionType.Enabled = false;
                m_textBoxRegionName.Enabled = false;

                m_textBoxMusicFileName.Enabled = false;
                m_textBoxVolume.Enabled = false;
                m_checkBoxMusicLoop.Enabled = false;
            }

            //半禁止，非音乐区域，没有音乐属性
            if (nDisableType == 2)
            {
                m_comboBoxRegionType.Enabled = true;
                m_textBoxRegionName.Enabled = true;

                m_textBoxMusicFileName.Enabled = false;
                m_textBoxVolume.Enabled = false;
                m_checkBoxMusicLoop.Enabled = false;
            }
            
        }

        private void m_buttonRefresh_Click(object sender, EventArgs e)
        {
            RefreshRegionTree();
            RefreshRegionProp();
        }

        private IUICommandService UICommandService
        {
            get
            {
                if (m_uiCommandService == null)
                {
                    ServiceProvider sp = ServiceProvider.Instance;
                    m_uiCommandService =
                        sp.GetService(typeof(IUICommandService))
                        as IUICommandService;
                }
                return m_uiCommandService;
            }
        }

        private void m_textBoxRegionName_TextChanged(object sender, EventArgs e)
        {
            if (m_radioButtonUpdateRegion.Checked)
            {
                if (m_treeViewRegionList.SelectedNode.Tag != null)
                {
                    string strAreaID = m_treeViewRegionList.SelectedNode.Tag.ToString();
                    int nAreaID = int.Parse(strAreaID);
                    MFramework.Instance.Scene.SetRegionNameByID(nAreaID, m_textBoxRegionName.Text);
                    //RefreshRegionTree();
                }
            }
        }

        // 界面的实时跟新
        private void RegionToolPanel_Paint(object sender, PaintEventArgs e)
        {
            RefreshRegionTree();
            RefreshRegionTree();
        }

        private void RegionToolPanel_Click(object sender, EventArgs e)
        {
            RefreshRegionTree();
            RefreshRegionTree();
        }

        private void CreateRegion()
        {
            MFramework.Instance.PaletteManager.ActivePalette = MFramework.Instance.PaletteManager.GetPaletteByName("AreaTool");
            MFramework.Instance.PaletteManager.ActivePalette.ActiveEntity = MFramework.Instance.PaletteManager.ActivePalette.GetEntityByName("[AreaTool]Vertex");
             /// 切换到创建模式
            UICommand command = UICommandService.GetCommand(
                                "CreateInteractionMode");
            if (command != null)
            {
                command.DoClick(this, null);
            }

            MFramework.Instance.Scene.SetRegionOperType(1);
        }

        // 创建矩形区域
        private void m_buttonCreateRectRegion_Click(object sender, EventArgs e)
        {
            CreateRegion();
            MFramework.Instance.Scene.SetGlobalRegionType(0);
        }

        // 创建多边形区域
        private void m_buttonCreatePolyRegion_Click(object sender, EventArgs e)
        {
            CreateRegion();
            MFramework.Instance.Scene.SetGlobalRegionType(1);
        }

        // 设置是否循环播放音乐
        private void m_checkBoxMusicLoop_CheckedChanged(object sender, EventArgs e)
        {
            if (m_treeViewRegionList.SelectedNode.Tag != null)
            {
                string strAreaID = m_treeViewRegionList.SelectedNode.Tag.ToString();
                int nAreaID = int.Parse(strAreaID);

                string strMusicLoop = "0";
                if(m_checkBoxMusicLoop.Checked)
                {
                    strMusicLoop = "1";
                }
                else
                {
                    strMusicLoop = "0";
                }
                MFramework.Instance.Scene.SetRegionMusicInfoByID(nAreaID, "MusicLoop", strMusicLoop);
            }
        }

        // 设置音乐文件的路径
        private void m_textBoxMusicFileName_TextChanged(object sender, EventArgs e)
        {
            if (m_treeViewRegionList.SelectedNode.Tag != null)
            {
                string strAreaID = m_treeViewRegionList.SelectedNode.Tag.ToString();
                int nAreaID = int.Parse(strAreaID);
                MFramework.Instance.Scene.SetRegionMusicInfoByID(nAreaID, "MusicFile", m_textBoxMusicFileName.Text);
            }
        }

        // 设置音量
        private void m_textBoxVolume_TextChanged(object sender, EventArgs e)
        {
            if (m_treeViewRegionList.SelectedNode.Tag != null)
            {
                string strAreaID = m_treeViewRegionList.SelectedNode.Tag.ToString();
                int nAreaID = int.Parse(strAreaID);
                MFramework.Instance.Scene.SetRegionMusicInfoByID(nAreaID, "Volume", m_textBoxVolume.Text);
            }
        }

        private void m_buttonOpenMusicFile_Click(object sender, EventArgs e)
        {
            string FileName;
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileName = openFileDialog1.FileName;
                m_textBoxMusicFileName.Text = GetRelativePath(FileName);
            }
        }

        private string GetRelativePath(string strPath)
        {
            string strSouucePath = Application.ExecutablePath;
            string strResult  = strPath;

            /// 可执行文件的路径，去掉文件名，保留路径
            int intFilePos = strSouucePath.LastIndexOf("\\");
            strSouucePath = strSouucePath.Substring(0, intFilePos);

            /// 如果不是“\”结尾，补上
            if(!strSouucePath.EndsWith("\\"))
            {
                strSouucePath += "\\";
            }

            //////////////////////////////////////////////////////////////////////////
            int intIndex = -1;
            int intPos = strSouucePath.IndexOf("\\");
            while(intPos >= 0)
            {
                intPos++;
                if(string.Compare(strSouucePath, 0, strResult, 0, intPos, true) != 0)
                {
                    break;
                }
                intIndex = intPos;
                intPos = strSouucePath.IndexOf("\\", intPos);
            }

            //////////////////////////////////////////////////////////////////////////
            if(intIndex >= 0)
            {
                strResult = strResult.Substring(intIndex);
                intPos = strSouucePath.IndexOf("\\", intIndex);
                while(intPos >= 0)
                {
                    strResult = "..\\" + strResult;
                    intPos = strSouucePath.IndexOf("\\", intPos+1);
                }
            }

            return strResult;
        }

    }
}