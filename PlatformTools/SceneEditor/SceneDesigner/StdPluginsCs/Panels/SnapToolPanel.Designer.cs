﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class SnapToolPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_GetSelectSrcEntity = new System.Windows.Forms.Button();
            this.btn_GetSelectDesEntity = new System.Windows.Forms.Button();
            this.m_textBox_SrcEntityName = new System.Windows.Forms.TextBox();
            this.m_textBox_DesEntityName = new System.Windows.Forms.TextBox();
            this.btn_ClearEntity = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_GetSelectSrcEntity
            // 
            this.btn_GetSelectSrcEntity.Location = new System.Drawing.Point(25, 12);
            this.btn_GetSelectSrcEntity.Name = "btn_GetSelectSrcEntity";
            this.btn_GetSelectSrcEntity.Size = new System.Drawing.Size(75, 23);
            this.btn_GetSelectSrcEntity.TabIndex = 0;
            this.btn_GetSelectSrcEntity.Text = "选取源物件";
            this.btn_GetSelectSrcEntity.UseVisualStyleBackColor = true;
            this.btn_GetSelectSrcEntity.Click += new System.EventHandler(this.btn_GetSelectSrcEntity_Click);
            // 
            // btn_GetSelectDesEntity
            // 
            this.btn_GetSelectDesEntity.Location = new System.Drawing.Point(25, 54);
            this.btn_GetSelectDesEntity.Name = "btn_GetSelectDesEntity";
            this.btn_GetSelectDesEntity.Size = new System.Drawing.Size(75, 23);
            this.btn_GetSelectDesEntity.TabIndex = 1;
            this.btn_GetSelectDesEntity.Text = "选取目标物件";
            this.btn_GetSelectDesEntity.UseVisualStyleBackColor = true;
            this.btn_GetSelectDesEntity.Click += new System.EventHandler(this.btn_GetSelectDesEntity_Click);
            // 
            // m_textBox_SrcEntityName
            // 
            this.m_textBox_SrcEntityName.Location = new System.Drawing.Point(121, 14);
            this.m_textBox_SrcEntityName.Name = "m_textBox_SrcEntityName";
            this.m_textBox_SrcEntityName.Size = new System.Drawing.Size(165, 21);
            this.m_textBox_SrcEntityName.TabIndex = 2;
            // 
            // m_textBox_DesEntityName
            // 
            this.m_textBox_DesEntityName.Location = new System.Drawing.Point(121, 56);
            this.m_textBox_DesEntityName.Name = "m_textBox_DesEntityName";
            this.m_textBox_DesEntityName.Size = new System.Drawing.Size(165, 21);
            this.m_textBox_DesEntityName.TabIndex = 3;
            // 
            // btn_ClearEntity
            // 
            this.btn_ClearEntity.Location = new System.Drawing.Point(211, 107);
            this.btn_ClearEntity.Name = "btn_ClearEntity";
            this.btn_ClearEntity.Size = new System.Drawing.Size(75, 23);
            this.btn_ClearEntity.TabIndex = 4;
            this.btn_ClearEntity.Text = "清除所选";
            this.btn_ClearEntity.UseVisualStyleBackColor = true;
            this.btn_ClearEntity.Click += new System.EventHandler(this.btn_ClearEntity_Click);
            // 
            // SnapToolPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 142);
            this.Controls.Add(this.btn_ClearEntity);
            this.Controls.Add(this.m_textBox_DesEntityName);
            this.Controls.Add(this.m_textBox_SrcEntityName);
            this.Controls.Add(this.btn_GetSelectDesEntity);
            this.Controls.Add(this.btn_GetSelectSrcEntity);
            this.Name = "SnapToolPanel";
            this.Text = "吸点工具";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SnapToolPanel_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_GetSelectSrcEntity;
        private System.Windows.Forms.Button btn_GetSelectDesEntity;
        private System.Windows.Forms.TextBox m_textBox_SrcEntityName;
        private System.Windows.Forms.TextBox m_textBox_DesEntityName;
        private Emergent.Gamebryo.SceneDesigner.Framework.MEntity m_SrcEntity = null;
        private Emergent.Gamebryo.SceneDesigner.Framework.MEntity m_DesEntity = null;
        private System.Windows.Forms.Button btn_ClearEntity;
    }
}