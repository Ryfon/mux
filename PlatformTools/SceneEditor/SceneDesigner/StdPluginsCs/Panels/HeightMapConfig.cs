﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class HeightMapConfig : Form
    {
        protected HeightMapConfig()
        {
            InitializeComponent();
        }

        public static HeightMapConfig Instance = new HeightMapConfig();


        private void UpdateConfig()
        {
            try
            {
                m_iLowHeight = float.Parse(textBox_LowHeight.Text);
            }
            catch (Exception)
            {
                m_iLowHeight = 0;
                textBox_LowHeight.Text = "0";
                return;
            }

            try
            {
                m_iHeighHeight = float.Parse(textBox_HeighHeight.Text);
            }
            catch (Exception)
            {
                m_iHeighHeight = 255.0f;
                textBox_HeighHeight.Text = "255.0";
                return;
            }
           
        }

        private void textBox_LowHeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    m_iLowHeight = float.Parse(textBox_LowHeight.Text);
                }
                catch (Exception)
                {
                    m_iLowHeight = 0f;
                    textBox_LowHeight.Text = "0";
                    return;
                }
            }
        }

        private void textBox_HeighHeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    m_iHeighHeight = float.Parse(textBox_HeighHeight.Text);
                }
                catch (Exception)
                {
                    m_iHeighHeight = 255.0f;
                    textBox_HeighHeight.Text = "255.0";
                    return;
                }
            }
        }

        private bool ValidateConfig()
        {
            bool bValidate = true;

            if(m_iLowHeight >= m_iHeighHeight)
            {
                bValidate = false;
            }
            else if((m_iHeighHeight - m_iLowHeight) < 10.0f)
            {
                bValidate = false;
            }

            return bValidate;
        }

        private void btn_ExportHeightMap_Click(object sender, EventArgs e)
        {
            if (MTerrain.Instance.ExportHeightMap(ref m_iLowHeight, ref m_iHeighHeight) == false)
            {
                MessageBox.Show("导出高度图失败！");
            }

            // 更新高度 [7/15/2009]
            textBox_HeighHeight.Text = m_iHeighHeight.ToString("N");
            textBox_LowHeight.Text = m_iLowHeight.ToString("N");
        }

        private void btn_ImportHeightMap_Click(object sender, EventArgs e)
        {
            UpdateConfig();

            if (ValidateConfig() == false)
            {
                MessageBox.Show("错误的参数，请重新填写参数！");
            }
            else if(MTerrain.Instance.ImportHeightMap(m_iLowHeight,m_iHeighHeight) == false)
            {
                MessageBox.Show("导入高度图失败！");

            }
        }

        private void m_btnClose_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}