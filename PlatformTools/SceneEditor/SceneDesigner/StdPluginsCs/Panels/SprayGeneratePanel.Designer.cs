﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    partial class SprayGeneratePanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_textBoxSeaLevel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_textBoxWidth = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxAlpha = new System.Windows.Forms.TextBox();
            this.m_textBoxTexture = new System.Windows.Forms.TextBox();
            this.m_btnSprayTexture = new System.Windows.Forms.Button();
            this.m_btnGenerate = new System.Windows.Forms.Button();
            this.m_btnExport = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_textBoxTime0 = new System.Windows.Forms.TextBox();
            this.m_textBoxTV0 = new System.Windows.Forms.TextBox();
            this.m_textBoxTV1 = new System.Windows.Forms.TextBox();
            this.m_textBoxTime1 = new System.Windows.Forms.TextBox();
            this.m_textBoxTV2 = new System.Windows.Forms.TextBox();
            this.m_textBoxTime2 = new System.Windows.Forms.TextBox();
            this.m_textBoxTV3 = new System.Windows.Forms.TextBox();
            this.m_textBoxTime3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_textBoxSeaLevel
            // 
            this.m_textBoxSeaLevel.Location = new System.Drawing.Point(92, 12);
            this.m_textBoxSeaLevel.Name = "m_textBoxSeaLevel";
            this.m_textBoxSeaLevel.Size = new System.Drawing.Size(60, 21);
            this.m_textBoxSeaLevel.TabIndex = 0;
            this.m_textBoxSeaLevel.Text = "-1.0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "海平面高度";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "排浪宽度";
            // 
            // m_textBoxWidth
            // 
            this.m_textBoxWidth.Location = new System.Drawing.Point(92, 45);
            this.m_textBoxWidth.Name = "m_textBoxWidth";
            this.m_textBoxWidth.Size = new System.Drawing.Size(60, 21);
            this.m_textBoxWidth.TabIndex = 2;
            this.m_textBoxWidth.Text = "1.0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "透明度";
            // 
            // m_textBoxAlpha
            // 
            this.m_textBoxAlpha.Location = new System.Drawing.Point(92, 78);
            this.m_textBoxAlpha.Name = "m_textBoxAlpha";
            this.m_textBoxAlpha.Size = new System.Drawing.Size(60, 21);
            this.m_textBoxAlpha.TabIndex = 4;
            this.m_textBoxAlpha.Text = "0.6";
            // 
            // m_textBoxTexture
            // 
            this.m_textBoxTexture.Location = new System.Drawing.Point(91, 257);
            this.m_textBoxTexture.Name = "m_textBoxTexture";
            this.m_textBoxTexture.Size = new System.Drawing.Size(132, 21);
            this.m_textBoxTexture.TabIndex = 6;
            // 
            // m_btnSprayTexture
            // 
            this.m_btnSprayTexture.Location = new System.Drawing.Point(11, 257);
            this.m_btnSprayTexture.Name = "m_btnSprayTexture";
            this.m_btnSprayTexture.Size = new System.Drawing.Size(41, 21);
            this.m_btnSprayTexture.TabIndex = 8;
            this.m_btnSprayTexture.Text = "纹理";
            this.m_btnSprayTexture.UseVisualStyleBackColor = true;
            this.m_btnSprayTexture.Click += new System.EventHandler(this.m_btnSprayTexture_Click);
            // 
            // m_btnGenerate
            // 
            this.m_btnGenerate.Location = new System.Drawing.Point(14, 299);
            this.m_btnGenerate.Name = "m_btnGenerate";
            this.m_btnGenerate.Size = new System.Drawing.Size(62, 25);
            this.m_btnGenerate.TabIndex = 9;
            this.m_btnGenerate.Text = "生成";
            this.m_btnGenerate.UseVisualStyleBackColor = true;
            this.m_btnGenerate.Click += new System.EventHandler(this.m_btnGenerate_Click);
            // 
            // m_btnExport
            // 
            this.m_btnExport.Location = new System.Drawing.Point(90, 299);
            this.m_btnExport.Name = "m_btnExport";
            this.m_btnExport.Size = new System.Drawing.Size(62, 25);
            this.m_btnExport.TabIndex = 10;
            this.m_btnExport.Text = "导出";
            this.m_btnExport.UseVisualStyleBackColor = true;
            this.m_btnExport.Click += new System.EventHandler(this.m_btnExport_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "时间";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(93, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "v 偏移值";
            // 
            // m_textBoxTime0
            // 
            this.m_textBoxTime0.Location = new System.Drawing.Point(17, 143);
            this.m_textBoxTime0.Name = "m_textBoxTime0";
            this.m_textBoxTime0.Size = new System.Drawing.Size(36, 21);
            this.m_textBoxTime0.TabIndex = 13;
            this.m_textBoxTime0.Text = "0.0";
            // 
            // m_textBoxTV0
            // 
            this.m_textBoxTV0.Location = new System.Drawing.Point(95, 143);
            this.m_textBoxTV0.Name = "m_textBoxTV0";
            this.m_textBoxTV0.Size = new System.Drawing.Size(51, 21);
            this.m_textBoxTV0.TabIndex = 14;
            this.m_textBoxTV0.Text = "0.2";
            // 
            // m_textBoxTV1
            // 
            this.m_textBoxTV1.Location = new System.Drawing.Point(95, 170);
            this.m_textBoxTV1.Name = "m_textBoxTV1";
            this.m_textBoxTV1.Size = new System.Drawing.Size(51, 21);
            this.m_textBoxTV1.TabIndex = 16;
            this.m_textBoxTV1.Text = "-0.75";
            // 
            // m_textBoxTime1
            // 
            this.m_textBoxTime1.Location = new System.Drawing.Point(17, 170);
            this.m_textBoxTime1.Name = "m_textBoxTime1";
            this.m_textBoxTime1.Size = new System.Drawing.Size(36, 21);
            this.m_textBoxTime1.TabIndex = 15;
            this.m_textBoxTime1.Text = "3.0";
            // 
            // m_textBoxTV2
            // 
            this.m_textBoxTV2.Location = new System.Drawing.Point(95, 197);
            this.m_textBoxTV2.Name = "m_textBoxTV2";
            this.m_textBoxTV2.Size = new System.Drawing.Size(51, 21);
            this.m_textBoxTV2.TabIndex = 18;
            this.m_textBoxTV2.Text = "-0.55";
            // 
            // m_textBoxTime2
            // 
            this.m_textBoxTime2.Location = new System.Drawing.Point(17, 197);
            this.m_textBoxTime2.Name = "m_textBoxTime2";
            this.m_textBoxTime2.Size = new System.Drawing.Size(36, 21);
            this.m_textBoxTime2.TabIndex = 17;
            this.m_textBoxTime2.Text = "5.0";
            // 
            // m_textBoxTV3
            // 
            this.m_textBoxTV3.Location = new System.Drawing.Point(95, 224);
            this.m_textBoxTV3.Name = "m_textBoxTV3";
            this.m_textBoxTV3.Size = new System.Drawing.Size(51, 21);
            this.m_textBoxTV3.TabIndex = 20;
            this.m_textBoxTV3.Text = "0.2";
            // 
            // m_textBoxTime3
            // 
            this.m_textBoxTime3.Location = new System.Drawing.Point(17, 224);
            this.m_textBoxTime3.Name = "m_textBoxTime3";
            this.m_textBoxTime3.Size = new System.Drawing.Size(36, 21);
            this.m_textBoxTime3.TabIndex = 19;
            this.m_textBoxTime3.Text = "10.0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 21;
            this.label6.Text = "动画关键帧";
            // 
            // m_btnClear
            // 
            this.m_btnClear.Location = new System.Drawing.Point(161, 299);
            this.m_btnClear.Name = "m_btnClear";
            this.m_btnClear.Size = new System.Drawing.Size(62, 25);
            this.m_btnClear.TabIndex = 22;
            this.m_btnClear.Text = "清除";
            this.m_btnClear.UseVisualStyleBackColor = true;
            this.m_btnClear.Click += new System.EventHandler(this.m_btnClear_Click);
            // 
            // SprayGeneratePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(235, 336);
            this.Controls.Add(this.m_btnClear);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.m_textBoxTV3);
            this.Controls.Add(this.m_textBoxTime3);
            this.Controls.Add(this.m_textBoxTV2);
            this.Controls.Add(this.m_textBoxTime2);
            this.Controls.Add(this.m_textBoxTV1);
            this.Controls.Add(this.m_textBoxTime1);
            this.Controls.Add(this.m_textBoxTV0);
            this.Controls.Add(this.m_textBoxTime0);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_btnExport);
            this.Controls.Add(this.m_btnGenerate);
            this.Controls.Add(this.m_btnSprayTexture);
            this.Controls.Add(this.m_textBoxTexture);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_textBoxAlpha);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_textBoxWidth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_textBoxSeaLevel);
            this.Name = "SprayGeneratePanel";
            this.Text = "排浪生成工具";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_textBoxSeaLevel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_textBoxWidth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxAlpha;
        private System.Windows.Forms.TextBox m_textBoxTexture;
        private System.Windows.Forms.Button m_btnSprayTexture;
        private System.Windows.Forms.Button m_btnGenerate;
        private System.Windows.Forms.Button m_btnExport;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox m_textBoxTime0;
        private System.Windows.Forms.TextBox m_textBoxTV0;
        private System.Windows.Forms.TextBox m_textBoxTV1;
        private System.Windows.Forms.TextBox m_textBoxTime1;
        private System.Windows.Forms.TextBox m_textBoxTV2;
        private System.Windows.Forms.TextBox m_textBoxTime2;
        private System.Windows.Forms.TextBox m_textBoxTV3;
        private System.Windows.Forms.TextBox m_textBoxTime3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button m_btnClear;
    }
}