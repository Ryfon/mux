﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class TerrainHeightCheckPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_radioBtnHeightCheck = new System.Windows.Forms.RadioButton();
            this.m_radioBtnRayCheck = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.m_textBoxLimitedHeight = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_checkBoxOverTop = new System.Windows.Forms.CheckBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.m_textBoxAngle = new System.Windows.Forms.TextBox();
            this.m_textBoxDistance = new System.Windows.Forms.TextBox();
            this.btn_blue = new System.Windows.Forms.Button();
            this.btn_green = new System.Windows.Forms.Button();
            this.btn_red = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_TerrRever = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_radioBtnHeightCheck
            // 
            this.m_radioBtnHeightCheck.AutoSize = true;
            this.m_radioBtnHeightCheck.Location = new System.Drawing.Point(20, 56);
            this.m_radioBtnHeightCheck.Name = "m_radioBtnHeightCheck";
            this.m_radioBtnHeightCheck.Size = new System.Drawing.Size(71, 16);
            this.m_radioBtnHeightCheck.TabIndex = 0;
            this.m_radioBtnHeightCheck.TabStop = true;
            this.m_radioBtnHeightCheck.Text = "高度检查";
            this.m_radioBtnHeightCheck.UseVisualStyleBackColor = true;
            // 
            // m_radioBtnRayCheck
            // 
            this.m_radioBtnRayCheck.AutoSize = true;
            this.m_radioBtnRayCheck.Location = new System.Drawing.Point(20, 125);
            this.m_radioBtnRayCheck.Name = "m_radioBtnRayCheck";
            this.m_radioBtnRayCheck.Size = new System.Drawing.Size(71, 16);
            this.m_radioBtnRayCheck.TabIndex = 1;
            this.m_radioBtnRayCheck.TabStop = true;
            this.m_radioBtnRayCheck.Text = "角度检查";
            this.m_radioBtnRayCheck.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "临界高度";
            // 
            // m_textBoxLimitedHeight
            // 
            this.m_textBoxLimitedHeight.Location = new System.Drawing.Point(108, 76);
            this.m_textBoxLimitedHeight.Name = "m_textBoxLimitedHeight";
            this.m_textBoxLimitedHeight.Size = new System.Drawing.Size(62, 21);
            this.m_textBoxLimitedHeight.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "起伏角度";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(42, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "目标距离";
            this.label3.Visible = false;
            // 
            // m_checkBoxOverTop
            // 
            this.m_checkBoxOverTop.AutoSize = true;
            this.m_checkBoxOverTop.Location = new System.Drawing.Point(12, 12);
            this.m_checkBoxOverTop.Name = "m_checkBoxOverTop";
            this.m_checkBoxOverTop.Size = new System.Drawing.Size(96, 16);
            this.m_checkBoxOverTop.TabIndex = 6;
            this.m_checkBoxOverTop.Text = "开启高度检测";
            this.m_checkBoxOverTop.UseVisualStyleBackColor = true;
            this.m_checkBoxOverTop.CheckedChanged += new System.EventHandler(this.m_checkBoxOverTop_CheckedChanged);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(42, 239);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 23);
            this.buttonRefresh.TabIndex = 7;
            this.buttonRefresh.Text = "重新计算";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // m_textBoxAngle
            // 
            this.m_textBoxAngle.Location = new System.Drawing.Point(108, 164);
            this.m_textBoxAngle.Name = "m_textBoxAngle";
            this.m_textBoxAngle.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxAngle.TabIndex = 8;
            // 
            // m_textBoxDistance
            // 
            this.m_textBoxDistance.Enabled = false;
            this.m_textBoxDistance.Location = new System.Drawing.Point(108, 196);
            this.m_textBoxDistance.Name = "m_textBoxDistance";
            this.m_textBoxDistance.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxDistance.TabIndex = 9;
            this.m_textBoxDistance.Visible = false;
            // 
            // btn_blue
            // 
            this.btn_blue.BackColor = System.Drawing.Color.Blue;
            this.btn_blue.Location = new System.Drawing.Point(289, 162);
            this.btn_blue.Name = "btn_blue";
            this.btn_blue.Size = new System.Drawing.Size(32, 23);
            this.btn_blue.TabIndex = 10;
            this.btn_blue.UseVisualStyleBackColor = false;
            this.btn_blue.Click += new System.EventHandler(this.btn_red_Click);
            // 
            // btn_green
            // 
            this.btn_green.BackColor = System.Drawing.Color.Lime;
            this.btn_green.Location = new System.Drawing.Point(252, 162);
            this.btn_green.Name = "btn_green";
            this.btn_green.Size = new System.Drawing.Size(31, 23);
            this.btn_green.TabIndex = 11;
            this.btn_green.UseVisualStyleBackColor = false;
            this.btn_green.Click += new System.EventHandler(this.btn_blue_Click);
            // 
            // btn_red
            // 
            this.btn_red.BackColor = System.Drawing.Color.Red;
            this.btn_red.Location = new System.Drawing.Point(214, 162);
            this.btn_red.Name = "btn_red";
            this.btn_red.Size = new System.Drawing.Size(35, 23);
            this.btn_red.TabIndex = 12;
            this.btn_red.UseVisualStyleBackColor = false;
            this.btn_red.Click += new System.EventHandler(this.btn_green_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(293, 12);
            this.label4.TabIndex = 13;
            this.label4.Text = "计算前将进行地形优化，请还原地形后再进行地形编辑";
            // 
            // btn_TerrRever
            // 
            this.btn_TerrRever.Location = new System.Drawing.Point(44, 292);
            this.btn_TerrRever.Name = "btn_TerrRever";
            this.btn_TerrRever.Size = new System.Drawing.Size(75, 23);
            this.btn_TerrRever.TabIndex = 14;
            this.btn_TerrRever.Text = "地形还原";
            this.btn_TerrRever.UseVisualStyleBackColor = true;
            this.btn_TerrRever.Click += new System.EventHandler(this.btn_TerrRever_Click);
            // 
            // TerrainHeightCheckPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 351);
            this.Controls.Add(this.btn_TerrRever);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_red);
            this.Controls.Add(this.btn_green);
            this.Controls.Add(this.btn_blue);
            this.Controls.Add(this.m_textBoxDistance);
            this.Controls.Add(this.m_textBoxAngle);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.m_checkBoxOverTop);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_textBoxLimitedHeight);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_radioBtnRayCheck);
            this.Controls.Add(this.m_radioBtnHeightCheck);
            this.Name = "TerrainHeightCheckPanel";
            this.Text = "地形高度检查工具";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TerrainHeightCheckPanel_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton m_radioBtnHeightCheck;
        private System.Windows.Forms.RadioButton m_radioBtnRayCheck;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_textBoxLimitedHeight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox m_checkBoxOverTop;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.TextBox m_textBoxAngle;
        private System.Windows.Forms.TextBox m_textBoxDistance;
        private System.Windows.Forms.Button btn_blue;
        private System.Windows.Forms.Button btn_green;
        private System.Windows.Forms.Button btn_red;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_TerrRever;
    }
}