﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    partial class EntityExportSettingPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_splitContainerMain = new System.Windows.Forms.SplitContainer();
            this.m_splitContainerLeft = new System.Windows.Forms.SplitContainer();
            this.m_listBoxSceneEntities = new System.Windows.Forms.ListBox();
            this.m_listBoxSections = new System.Windows.Forms.ListBox();
            this.m_btnSave = new System.Windows.Forms.Button();
            this.m_dgvExportSetting = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_splitContainerMain.Panel1.SuspendLayout();
            this.m_splitContainerMain.Panel2.SuspendLayout();
            this.m_splitContainerMain.SuspendLayout();
            this.m_splitContainerLeft.Panel1.SuspendLayout();
            this.m_splitContainerLeft.Panel2.SuspendLayout();
            this.m_splitContainerLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvExportSetting)).BeginInit();
            this.SuspendLayout();
            // 
            // m_splitContainerMain
            // 
            this.m_splitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainerMain.Location = new System.Drawing.Point(0, 0);
            this.m_splitContainerMain.Name = "m_splitContainerMain";
            // 
            // m_splitContainerMain.Panel1
            // 
            this.m_splitContainerMain.Panel1.Controls.Add(this.m_splitContainerLeft);
            // 
            // m_splitContainerMain.Panel2
            // 
            this.m_splitContainerMain.Panel2.Controls.Add(this.m_btnSave);
            this.m_splitContainerMain.Panel2.Controls.Add(this.m_dgvExportSetting);
            this.m_splitContainerMain.Size = new System.Drawing.Size(486, 424);
            this.m_splitContainerMain.SplitterDistance = 162;
            this.m_splitContainerMain.TabIndex = 0;
            // 
            // m_splitContainerLeft
            // 
            this.m_splitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.m_splitContainerLeft.Name = "m_splitContainerLeft";
            this.m_splitContainerLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // m_splitContainerLeft.Panel1
            // 
            this.m_splitContainerLeft.Panel1.Controls.Add(this.m_listBoxSceneEntities);
            // 
            // m_splitContainerLeft.Panel2
            // 
            this.m_splitContainerLeft.Panel2.Controls.Add(this.m_listBoxSections);
            this.m_splitContainerLeft.Size = new System.Drawing.Size(162, 424);
            this.m_splitContainerLeft.SplitterDistance = 212;
            this.m_splitContainerLeft.TabIndex = 1;
            // 
            // m_listBoxSceneEntities
            // 
            this.m_listBoxSceneEntities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_listBoxSceneEntities.FormattingEnabled = true;
            this.m_listBoxSceneEntities.ItemHeight = 12;
            this.m_listBoxSceneEntities.Location = new System.Drawing.Point(0, 0);
            this.m_listBoxSceneEntities.Name = "m_listBoxSceneEntities";
            this.m_listBoxSceneEntities.Size = new System.Drawing.Size(162, 208);
            this.m_listBoxSceneEntities.TabIndex = 0;
            this.m_listBoxSceneEntities.DoubleClick += new System.EventHandler(this.m_listBoxSceneEntities_DoubleClick);
            // 
            // m_listBoxSections
            // 
            this.m_listBoxSections.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_listBoxSections.FormattingEnabled = true;
            this.m_listBoxSections.ItemHeight = 12;
            this.m_listBoxSections.Location = new System.Drawing.Point(0, 0);
            this.m_listBoxSections.Name = "m_listBoxSections";
            this.m_listBoxSections.Size = new System.Drawing.Size(162, 208);
            this.m_listBoxSections.TabIndex = 0;
            this.m_listBoxSections.DoubleClick += new System.EventHandler(this.m_listBoxSections_DoubleClick);
            // 
            // m_btnSave
            // 
            this.m_btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnSave.Location = new System.Drawing.Point(78, 389);
            this.m_btnSave.Name = "m_btnSave";
            this.m_btnSave.Size = new System.Drawing.Size(173, 23);
            this.m_btnSave.TabIndex = 1;
            this.m_btnSave.Text = "保存";
            this.m_btnSave.UseVisualStyleBackColor = true;
            this.m_btnSave.Click += new System.EventHandler(this.m_btnSave_Click);
            // 
            // m_dgvExportSetting
            // 
            this.m_dgvExportSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_dgvExportSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvExportSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.m_dgvExportSetting.Location = new System.Drawing.Point(2, 3);
            this.m_dgvExportSetting.MultiSelect = false;
            this.m_dgvExportSetting.Name = "m_dgvExportSetting";
            this.m_dgvExportSetting.RowHeadersVisible = false;
            this.m_dgvExportSetting.RowTemplate.Height = 23;
            this.m_dgvExportSetting.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.m_dgvExportSetting.Size = new System.Drawing.Size(315, 368);
            this.m_dgvExportSetting.TabIndex = 0;
            this.m_dgvExportSetting.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.m_dgvExportSetting_RowsAdded);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Name";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Section";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "ExtraID";
            this.Column3.Name = "Column3";
            // 
            // EntityExportSettingPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 424);
            this.Controls.Add(this.m_splitContainerMain);
            this.Name = "EntityExportSettingPanel";
            this.Text = "Entity 导出设置";
            this.Load += new System.EventHandler(this.EntityExportSettingPanel_Load);
            this.m_splitContainerMain.Panel1.ResumeLayout(false);
            this.m_splitContainerMain.Panel2.ResumeLayout(false);
            this.m_splitContainerMain.ResumeLayout(false);
            this.m_splitContainerLeft.Panel1.ResumeLayout(false);
            this.m_splitContainerLeft.Panel2.ResumeLayout(false);
            this.m_splitContainerLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvExportSetting)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer m_splitContainerMain;
        private System.Windows.Forms.ListBox m_listBoxSceneEntities;
        private System.Windows.Forms.SplitContainer m_splitContainerLeft;
        private System.Windows.Forms.DataGridView m_dgvExportSetting;
        private System.Windows.Forms.ListBox m_listBoxSections;
        private System.Windows.Forms.Button m_btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}