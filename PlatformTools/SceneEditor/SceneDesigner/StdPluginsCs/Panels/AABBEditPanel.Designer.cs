﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class AABBEditPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btn_GetSelEntity = new System.Windows.Forms.Button();
            this.m_textBox_SelEntityName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_SetAABBScale = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_btn_GetSelEntity
            // 
            this.m_btn_GetSelEntity.Location = new System.Drawing.Point(189, 21);
            this.m_btn_GetSelEntity.Name = "m_btn_GetSelEntity";
            this.m_btn_GetSelEntity.Size = new System.Drawing.Size(65, 23);
            this.m_btn_GetSelEntity.TabIndex = 0;
            this.m_btn_GetSelEntity.Text = "重新获取";
            this.m_btn_GetSelEntity.UseVisualStyleBackColor = true;
            this.m_btn_GetSelEntity.Click += new System.EventHandler(this.m_btn_GetSelEntity_Click);
            // 
            // m_textBox_SelEntityName
            // 
            this.m_textBox_SelEntityName.Location = new System.Drawing.Point(71, 23);
            this.m_textBox_SelEntityName.Name = "m_textBox_SelEntityName";
            this.m_textBox_SelEntityName.ReadOnly = true;
            this.m_textBox_SelEntityName.Size = new System.Drawing.Size(100, 21);
            this.m_textBox_SelEntityName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "物件名称";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(71, 79);
            this.textBox1.MaxLength = 8;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "缩放值";
            // 
            // btn_SetAABBScale
            // 
            this.btn_SetAABBScale.Location = new System.Drawing.Point(189, 77);
            this.btn_SetAABBScale.Name = "btn_SetAABBScale";
            this.btn_SetAABBScale.Size = new System.Drawing.Size(65, 23);
            this.btn_SetAABBScale.TabIndex = 5;
            this.btn_SetAABBScale.Text = "设置";
            this.btn_SetAABBScale.UseVisualStyleBackColor = true;
            this.btn_SetAABBScale.Click += new System.EventHandler(this.btn_SetAABBScale_Click);
            // 
            // AABBEditPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 124);
            this.Controls.Add(this.btn_SetAABBScale);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_textBox_SelEntityName);
            this.Controls.Add(this.m_btn_GetSelEntity);
            this.Name = "AABBEditPanel";
            this.Text = "AABBEditPanel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btn_GetSelEntity;
        private System.Windows.Forms.TextBox m_textBox_SelEntityName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_SetAABBScale;

        private float m_fScale;
    }
}