﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    partial class DirectionalShadowPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_listBoxCast = new System.Windows.Forms.ListBox();
            this.m_listBoxRemove = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_btnGetAllEntities = new System.Windows.Forms.Button();
            this.m_btnGetSelectedEntities = new System.Windows.Forms.Button();
            this.m_btnRemoveFromCastList = new System.Windows.Forms.Button();
            this.m_btnAddToCastList = new System.Windows.Forms.Button();
            this.m_textBoxMessage = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_textBoxShadowedValue = new System.Windows.Forms.TextBox();
            this.m_btnCastShadow = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // m_listBoxCast
            // 
            this.m_listBoxCast.FormattingEnabled = true;
            this.m_listBoxCast.ItemHeight = 12;
            this.m_listBoxCast.Location = new System.Drawing.Point(26, 87);
            this.m_listBoxCast.Name = "m_listBoxCast";
            this.m_listBoxCast.Size = new System.Drawing.Size(165, 220);
            this.m_listBoxCast.TabIndex = 0;
            this.m_listBoxCast.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // m_listBoxRemove
            // 
            this.m_listBoxRemove.FormattingEnabled = true;
            this.m_listBoxRemove.ItemHeight = 12;
            this.m_listBoxRemove.Location = new System.Drawing.Point(231, 87);
            this.m_listBoxRemove.Name = "m_listBoxRemove";
            this.m_listBoxRemove.Size = new System.Drawing.Size(165, 220);
            this.m_listBoxRemove.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "投射阴影 Entity 列表";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(234, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "排除列表";
            // 
            // m_btnGetAllEntities
            // 
            this.m_btnGetAllEntities.Location = new System.Drawing.Point(26, 313);
            this.m_btnGetAllEntities.Name = "m_btnGetAllEntities";
            this.m_btnGetAllEntities.Size = new System.Drawing.Size(165, 23);
            this.m_btnGetAllEntities.TabIndex = 6;
            this.m_btnGetAllEntities.Text = "获取所有物件";
            this.m_btnGetAllEntities.UseVisualStyleBackColor = true;
            // 
            // m_btnGetSelectedEntities
            // 
            this.m_btnGetSelectedEntities.Location = new System.Drawing.Point(26, 342);
            this.m_btnGetSelectedEntities.Name = "m_btnGetSelectedEntities";
            this.m_btnGetSelectedEntities.Size = new System.Drawing.Size(165, 23);
            this.m_btnGetSelectedEntities.TabIndex = 7;
            this.m_btnGetSelectedEntities.Text = "获取被选择物件";
            this.m_btnGetSelectedEntities.UseVisualStyleBackColor = true;
            // 
            // m_btnRemoveFromCastList
            // 
            this.m_btnRemoveFromCastList.Location = new System.Drawing.Point(196, 143);
            this.m_btnRemoveFromCastList.Name = "m_btnRemoveFromCastList";
            this.m_btnRemoveFromCastList.Size = new System.Drawing.Size(30, 23);
            this.m_btnRemoveFromCastList.TabIndex = 8;
            this.m_btnRemoveFromCastList.Text = "->";
            this.m_btnRemoveFromCastList.UseVisualStyleBackColor = true;
            // 
            // m_btnAddToCastList
            // 
            this.m_btnAddToCastList.Location = new System.Drawing.Point(196, 181);
            this.m_btnAddToCastList.Name = "m_btnAddToCastList";
            this.m_btnAddToCastList.Size = new System.Drawing.Size(30, 23);
            this.m_btnAddToCastList.TabIndex = 9;
            this.m_btnAddToCastList.Text = "<-";
            this.m_btnAddToCastList.UseVisualStyleBackColor = true;
            // 
            // m_textBoxMessage
            // 
            this.m_textBoxMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_textBoxMessage.Location = new System.Drawing.Point(0, 420);
            this.m_textBoxMessage.Multiline = true;
            this.m_textBoxMessage.Name = "m_textBoxMessage";
            this.m_textBoxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_textBoxMessage.Size = new System.Drawing.Size(425, 130);
            this.m_textBoxMessage.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 385);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "最大阴影深度";
            // 
            // m_textBoxShadowedValue
            // 
            this.m_textBoxShadowedValue.Location = new System.Drawing.Point(111, 382);
            this.m_textBoxShadowedValue.Name = "m_textBoxShadowedValue";
            this.m_textBoxShadowedValue.Size = new System.Drawing.Size(80, 21);
            this.m_textBoxShadowedValue.TabIndex = 12;
            // 
            // m_btnCastShadow
            // 
            this.m_btnCastShadow.Location = new System.Drawing.Point(207, 382);
            this.m_btnCastShadow.Name = "m_btnCastShadow";
            this.m_btnCastShadow.Size = new System.Drawing.Size(80, 21);
            this.m_btnCastShadow.TabIndex = 13;
            this.m_btnCastShadow.Text = "生成阴影";
            this.m_btnCastShadow.UseVisualStyleBackColor = true;
            this.m_btnCastShadow.Click += new System.EventHandler(this.m_btnCastShadow_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "投射阴影的方向:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(134, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(34, 21);
            this.textBox1.TabIndex = 15;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(174, 22);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(34, 21);
            this.textBox2.TabIndex = 16;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(214, 22);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(34, 21);
            this.textBox3.TabIndex = 17;
            // 
            // DirectionalShadowPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 550);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.m_btnCastShadow);
            this.Controls.Add(this.m_textBoxShadowedValue);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_textBoxMessage);
            this.Controls.Add(this.m_btnAddToCastList);
            this.Controls.Add(this.m_btnRemoveFromCastList);
            this.Controls.Add(this.m_btnGetSelectedEntities);
            this.Controls.Add(this.m_btnGetAllEntities);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_listBoxRemove);
            this.Controls.Add(this.m_listBoxCast);
            this.Name = "DirectionalShadowPanel";
            this.Text = "DirectionalShadowPanel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox m_listBoxCast;
        private System.Windows.Forms.ListBox m_listBoxRemove;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button m_btnGetAllEntities;
        private System.Windows.Forms.Button m_btnGetSelectedEntities;
        private System.Windows.Forms.Button m_btnRemoveFromCastList;
        private System.Windows.Forms.Button m_btnAddToCastList;
        private System.Windows.Forms.TextBox m_textBoxMessage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox m_textBoxShadowedValue;
        private System.Windows.Forms.Button m_btnCastShadow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
    }
}