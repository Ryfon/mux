﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    public partial class SprayGeneratePanel : Form
    {
        public SprayGeneratePanel()
        {
            InitializeComponent();
        }

        private void m_btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                float fSeaLevel = float.Parse(m_textBoxSeaLevel.Text);
                float fWidth = float.Parse(m_textBoxWidth.Text);
                float fAlpha = float.Parse(m_textBoxAlpha.Text);
                float fTime0 = float.Parse(m_textBoxTime0.Text);
                float fTime1 = float.Parse(m_textBoxTime1.Text);
                float fTime2 = float.Parse(m_textBoxTime2.Text);
                float fTime3 = float.Parse(m_textBoxTime3.Text);
                float fV0 = float.Parse(m_textBoxTV0.Text);
                float fV1 = float.Parse(m_textBoxTV1.Text);
                float fV2 = float.Parse(m_textBoxTV2.Text);
                float fV3 = float.Parse(m_textBoxTV3.Text);

                String strSprayFilename = m_textBoxTexture.Text;
                if (strSprayFilename.Length == 0)
                {
                    MessageBox.Show("请输入排浪纹理名称.");
                    return;
                }
                MTerrain.Instance.GenerateSprayGeometry(fSeaLevel, fWidth, fAlpha, fTime0, fV0, fTime1, fV1, 
                                                                        fTime2, fV2, fTime3, fV3, strSprayFilename);
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void m_btnSprayTexture_Click(object sender, EventArgs e)
        {
            // 选择排浪纹理
            OpenFileDialog openFileDlg = new OpenFileDialog();
            openFileDlg.Filter = "dds files (*.dds)|*.dds|All files (*.*)|*.*";
            openFileDlg.FilterIndex = 1;
            openFileDlg.RestoreDirectory = true;

            if (openFileDlg.ShowDialog() == DialogResult.OK)
            {
                m_textBoxTexture.Text = openFileDlg.FileName;
            }
        }

        private void m_btnExport_Click(object sender, EventArgs e)
        {
            // 导出 nif
            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Filter = "NIF files (*.nif)|*.nif|All files (*.*)|*.*";
            saveFileDlg.FilterIndex = 1;
            saveFileDlg.RestoreDirectory = true;
            if (saveFileDlg.ShowDialog() == DialogResult.OK)
            {
                if (!MTerrain.Instance.ExportSprayNif(saveFileDlg.FileName))
                {
                    MessageBox.Show("导出排浪 NIF 失败.");
                }
                else
                {
                    MessageBox.Show("导出排浪 NIF 成功.");
                }
            }
        }

        private void m_btnClear_Click(object sender, EventArgs e)
        {
            MTerrain.Instance.ClearSprayPreview();
        }
    }
}