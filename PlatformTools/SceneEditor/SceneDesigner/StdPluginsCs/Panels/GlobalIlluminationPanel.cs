﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Diagnostics;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class GlobalIlluminationPanel : Form
    {

        protected GlobalIlluminationPanel()
        {
            InitializeComponent();
            m_cbPrimaryIntegrator.SelectedIndex = 0;
            m_cbSecondaryIntegrator.SelectedIndex = 0;
        }

        protected static GlobalIlluminationPanel m_Instance = new GlobalIlluminationPanel();

        // 获取唯一实例
        public static GlobalIlluminationPanel Instance
        {
            get { return m_Instance; }
        }

        // 执行 dos 命令
        private void RunCommand(String strFileName, String strArguments, String strWorkingDir)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = strFileName;
            startInfo.Arguments = strArguments;
            startInfo.WorkingDirectory = Application.StartupPath + strWorkingDir;
            process.StartInfo = startInfo;

            try
            {
                if (process.Start())
                {
                    process.WaitForExit();
                }
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void m_btnBakeLightMap_Click(object sender, EventArgs e)
        {
            if (MFramework.Instance.Scene == null) return;

            String strAppPath = Application.StartupPath;    // 应用程序启动路径
            Directory.SetCurrentDirectory(strAppPath);

            String strBeastPath = strAppPath + "\\Beast\\"; // 场景编辑器下 Beast 所在路径
            String strBeastPluginExePath = strBeastPath + "BeastPlugin.exe";
            String strDefConfig = strBeastPath + "templateBeast.xml";
            String strGsaPath = MFramework.Instance.CurrentFilename;
            String strScenePath = GetScenePath();
            int iNumChunkX = MTerrain.Instance.GetNumChunkX();
            int iNumChunkY = MTerrain.Instance.GetNumChunkY();
            String strTagChunks = m_tbTagChunks.Text;

            String strGIPath = strScenePath + "/GI/";   // 本场景 GI 计算的临时目录
            String strTextureFolder = strGIPath + "Textures";
            String strBeastConfig = strGIPath; //+ "Scene.xml"
            String strFbx = strGIPath;//+ "Scene.fbx"
            String strLightMapFolder = strScenePath + "LightMaps";

            String strArguments = String.Format("{0}"
                + "-defaultbeastconfig \"{1}\" "
                + "-gsa \"{2}\" "
                + "-NumChunkX \"{3}\" "
                + "-NumChunkY \"{4}\" "
                + "-TargetChunks \"{5}\" "
                + "-texturefolder \"{6}\" "
                + "-beastconfig \"{7}\" "
                + "-fbx \"{8}\" "
                + "-lightmapfolder \"{9}\" ",
                "", 
                strDefConfig,
                strGsaPath,
                iNumChunkX.ToString(),
                iNumChunkY.ToString(),
                strTagChunks,
                strTextureFolder,
                strBeastConfig,
                strFbx,
                strLightMapFolder
            );

            // 解析用户参数
            int iPixelsPerWorldUnit = 1;
            int iMinLightMapSize = 64;
            int iMaxLightMapSize = 256;
            int iEmissiveMultiplier = 20;
            int iFakeLightEmissive = 80;

            String strPrimaryIntegrator = "finalgather";
            String strSecondaryIntegrator = "finalgather";

            int iFGRays = 300;
            float fFGAccuracy = 1.0f;
            float fFGSmooth = 1.0f;
            int iFGDepth = 1;
            int iPTPaths = 100000;
            float fPTCacheSpacing = 128.0f;

            String strEnvType = "constant";
            float fEnvironmentIntensity = 1.0f;
            float fEnvR = 0.2f;
            float fEnvG = 0.2f;
            float fEnvB = 0.4f;

            String strHdrFile = "";
            try
            {
                iPixelsPerWorldUnit = int.Parse(m_tbPixelsPerWorldUnit.Text);
                iMinLightMapSize = int.Parse(m_tbMinLightMapSize.Text);
                iMaxLightMapSize = int.Parse(m_tbMaxLightMapSize.Text);
                iEmissiveMultiplier = int.Parse(m_tbEmissiveMultiplier.Text);
                iFakeLightEmissive = int.Parse(m_tbFakeLightEmissive.Text);
                strPrimaryIntegrator = m_cbPrimaryIntegrator.Text;
                strSecondaryIntegrator = m_cbSecondaryIntegrator.Text;
                iFGRays = int.Parse(m_tbFGrays.Text);
                fFGAccuracy = float.Parse(m_tbFGAccuracy.Text);
                fFGSmooth = float.Parse(m_tbFGSmooth.Text);
                iFGDepth = int.Parse(m_tbFGDepth.Text);
                iPTPaths = int.Parse(m_tbPTPaths.Text);
                fPTCacheSpacing = float.Parse(m_tbPTCachesPacing.Text);

                strEnvType = m_tbEnvType.Text;
                fEnvironmentIntensity = float.Parse(m_tbEnviromentintensity.Text);
                fEnvR = float.Parse(m_tbEnvironmentColorR.Text);
                fEnvG = float.Parse(m_tbEnvironmentColorG.Text);
                fEnvB = float.Parse(m_tbEnvironmentColorB.Text);
                strHdrFile = m_tbHdrFile.Text;
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
                return;
            }

            strArguments += (" -pixelsperworldunit " + iPixelsPerWorldUnit.ToString());
            strArguments += (" -minlightmapsize " + iMinLightMapSize.ToString());
            strArguments += (" -maxlightmapsize " + iMaxLightMapSize.ToString());

            strArguments += (" -emissivemultiplier " + iEmissiveMultiplier.ToString());
            strArguments += (" -fakelightemissive " + iFakeLightEmissive.ToString());
            strArguments += (" -primaryintegrator " + strPrimaryIntegrator);
            strArguments += (" -secondaryintegrator " + strSecondaryIntegrator);
            strArguments += (" -fgrays " + iFGRays.ToString());
            strArguments += (" -fgaccuracy " + fFGAccuracy.ToString());
            strArguments += (" -fgsmooth " + fFGSmooth.ToString());
            strArguments += (" -fgdepth " + iFGDepth.ToString());
            strArguments += (" -ptpaths " + iFGDepth.ToString());
            strArguments += (" -ptcachespacing " + fPTCacheSpacing.ToString());

            strArguments += (" -envtype " + strEnvType);
            strArguments += (" -environmentintensity " + fEnvironmentIntensity.ToString());
            strArguments += (" -environmentcolorr " + fEnvR.ToString());
            strArguments += (" -environmentcolorg " + fEnvG.ToString());
            strArguments += (" -environmentcolorb " + fEnvB.ToString());
            strArguments += (" -hdrfile " + strHdrFile);

            strArguments += (" > lightbake.log");

            m_rtbCommand.Text = strBeastPluginExePath + " " + strArguments;

            // 修改系统时间到 2009.09.01
            RunCommand(strBeastPluginExePath, strArguments, "\\Beast\\");
            // 改回系统时间
       }

        private void m_btnTerrainImportLightMap_Click(object sender, EventArgs e)
        {
            if (MFramework.Instance.Scene == null) return;

            int iNumChunks = MTerrain.Instance.GetNumChunkX() * MTerrain.Instance.GetNumChunkY();

            float fLum = 80;
            float fInc = 3.0f;
            try
            {
                fLum = float.Parse(m_tbLum.Text);
                fInc = float.Parse(m_tbIncMultiple.Text);
            }
            catch (Exception) { }

            // 地形自动加载 light map
            String strLightMapDir = GetScenePath() + "LightMaps\\";
            for (int i=0; i<iNumChunks; i++)
            {
                String strTerrainLightMap = strLightMapDir + "chunk_" + i.ToString() + ".tga";

                if (File.Exists(strTerrainLightMap))
                {
                    // 分别加载颜色信息和阴影信息
                    MTerrain.Instance.ApplyVertexColorTexture(strTerrainLightMap, i, fLum, m_cbFixTerrainLum.Checked, fInc);
                    MTerrain.Instance.ApplyShadowTexture(strTerrainLightMap, i);
                   // MFramework.Instance.Scene.ImportShadowTexture(strTerrainLightMap);
                }
                else 
                {
                    strTerrainLightMap = strLightMapDir + "chunk_" + i.ToString() + ".dds";
                    MTerrain.Instance.ApplyVertexColorTexture(strTerrainLightMap, i, fLum, m_cbFixTerrainLum.Checked, fInc);
                    MTerrain.Instance.ApplyShadowTexture(strTerrainLightMap, i);
                }
            }

            // 保存地形
            String strTerrainFile = GetScenePath() + "terrain.trr";
            MFramework.Instance.Scene.SaveTerrain(strTerrainFile);

            MessageBox.Show("地形光照贴图载入成功.");

        }

        private void m_SceneAppPreview_Click(object sender, EventArgs e)
        {
            if (MFramework.Instance.Scene == null) return;

            // 预览场景
            String strAppPath = Application.StartupPath;    // 应用程序启动路径
            Directory.SetCurrentDirectory(strAppPath);
            String strBeastSceneAppPath = "SceneApp.exe";

            String strGsaPath = MFramework.Instance.CurrentFilename;
            String strLightMapPath = GetScenePath() + "LightMaps";

            String strArguments = strGsaPath + " " + strLightMapPath + " " + strAppPath;
            RunCommand(strBeastSceneAppPath, strArguments, "\\Beast\\");

            m_rtbCommand.Text = strBeastSceneAppPath + " " + strArguments;
         }
        
        // 获取场景路径
        private String GetScenePath()
        {
            if (MFramework.Instance.CurrentFilename != null)
            {
                int iLastDot = MFramework.Instance.CurrentFilename.LastIndexOf('.');
                if (iLastDot > -1)
                {
                    return MFramework.Instance.CurrentFilename.Substring(0, iLastDot) + "\\";
                }
            }

            return null;

        }

        // 添加目录下所有文件到文件列表, 递归调用
        void AddDirectoryToFileList(String strSelPath)
        {
            if (strSelPath == null)
            {
                return;
            }

            String[] arrFiles = Directory.GetFiles(strSelPath);
            foreach (String strFile in arrFiles)
            {
                AddFileToFileList(strFile);
            }
            
        }

        // 添加一个文件到文件列表
        void AddFileToFileList(String strFile)
        {
            if (strFile == null) return;

            if (Path.GetExtension(strFile).ToLower().Equals(".nif"))
            {
                int iIdx = m_dgvFileList.Rows.Add();
                m_dgvFileList[0, iIdx].Value = strFile;
                m_dgvFileList.Rows[iIdx].Tag = strFile;

            }
        }

        private void m_btnSelectHdr_Click(object sender, EventArgs e)
        {
            // 选择 hdr 纹理
            OpenFileDialog openFileDlg = new OpenFileDialog();
	        openFileDlg.Filter = "hdr file(*.hdr)|*.hdr|ext file(*.exr)|*.exr|All files(*.*)|*.*";
	        openFileDlg.FilterIndex = 1;

	        if (openFileDlg.ShowDialog() == DialogResult.OK)
	        {
                m_tbHdrFile.Text = openFileDlg.FileName;
                m_tbEnvType.Text = "hdrfile";
            }
        }

        private void m_tbHdrFile_TextChanged(object sender, EventArgs e)
        {
            if (m_tbHdrFile.Text.Length == 0)
            {
                m_tbEnvType.Text = "constant";
            }
        }

        private void m_btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void m_btnTestConvert_Click(object sender, EventArgs e)
        {
            // 测试转换 nif  到 light map material
            MLightMapMaterialHelper.Test();
        }

        private void m_MenuStripFileList_Opening(object sender, CancelEventArgs e)
        {

        }

        private void 添加转换目录ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                AddDirectoryToFileList(dlg.SelectedPath);
            }
        }

        private void 添加文件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "nif files (*.nif)|*.nif|All files (*.*)|*.*";
            dlg.FilterIndex = 1;
            dlg.Multiselect = true;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                String[] arrFiles = dlg.FileNames;
                foreach (String strFileName in arrFiles)
                {
                    AddFileToFileList(strFileName);
                }
            }
        }

        private void m_ToolStripMenuItemMakeBackup_Click(object sender, EventArgs e)
        {
            m_ToolStripMenuItemMakeBackup.Checked =
                !m_ToolStripMenuItemMakeBackup.Checked;
        }

        private void 转换ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool bBackUp = m_ToolStripMenuItemMakeBackup.Checked;
            String strBackupPath = null;
            if (bBackUp) { strBackupPath = "_BackupNif"; }

            ArrayList arrResult = new ArrayList();
            foreach (DataGridViewRow row in m_dgvFileList.Rows)
            {
                String strFile = row.Cells[0].Value as String;
                if (strFile != null)
                {
                    MLightMapMaterialHelper.sConvertResultInfo convInfo =
                        MLightMapMaterialHelper.ConvertToLightMapMaterial(strFile, strBackupPath);
                    row.Cells[1].Value = convInfo.m_bSuccess.ToString();
                    row.Cells[2].Value = convInfo.m_strInfo;
                    row.Selected = true;
                    arrResult.Add(convInfo);
                    m_dgvFileList.Invalidate();
                }
            }

            String strAllInfo = "";
            foreach (MLightMapMaterialHelper.sConvertResultInfo convInfo in arrResult)
            {

                strAllInfo = strAllInfo + convInfo.m_strTargetFile + " " + 
                    convInfo.m_bSuccess.ToString() + " " + convInfo.m_strInfo + "";
                strAllInfo += "\r\n============================================================\r\n";
            }

            m_rtbOutputInfo.Text = strAllInfo;
        }

        private void 清除所有文件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_dgvFileList.Rows.Clear();
        }

        private void m_btnAllChunks_Click(object sender, EventArgs e)
        {
            // 全部 chunk
            if (MTerrain.Instance == null)
            {   
                return;
            }

            int iNumChunks = MTerrain.Instance.GetNumChunkX() * MTerrain.Instance.GetNumChunkY();

            String strChunks = "";
            for (int i=0; i<iNumChunks; i++)
            {
                strChunks += i.ToString();
                if (i != iNumChunks-1)
                {
                    strChunks += ",";
                }
            }

            m_tbTagChunks.Text = strChunks;
        }

        private void test_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                String strErrInfo = "";
                String[] arrFiles = Directory.GetFiles(dlg.SelectedPath);
                foreach (String strFile in arrFiles)
                {
                    if (!MUtility.Texture2DDS(strFile, 1))
                    {
                        strErrInfo += strFile + ", ";
                    }
                }

                if (strErrInfo.Length > 0)
                {
                    MessageBox.Show("转换失败的纹理: " + strErrInfo);
                }
            }

        }
    }
}