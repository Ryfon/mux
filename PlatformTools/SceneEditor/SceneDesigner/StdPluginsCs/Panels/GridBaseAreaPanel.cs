﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class GridBaseAreaPanel : Form
    {
        private GridBaseAreaPanel()
        {
            InitializeComponent();
        }

        public static GridBaseAreaPanel Instance
        {
            get
            {
                if (ms_pInstance.IsDisposed)
                {
                    ms_pInstance = new GridBaseAreaPanel();
                }

                return ms_pInstance;
            }
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            InputForm form = new InputForm();
            form.ShowDialog();

            String strAreaName = form.InputValue;

            if (!MTerrain.Instance.AddGridBaseRegion(strAreaName))
            {
                MessageBox.Show("已存在：" + strAreaName + " 区域。请换一个名字。");
                return;
            }

            RefreshRegionList();
        }

        // 刷新区域列表
        private void RefreshRegionList()
        {
            // 获取所有区域名称
            String[] strRegionList = MTerrain.Instance.GetGridBaseRegionList();
            m_listBoxGridBaseRegion.Items.Clear();
            foreach (String strRegion in strRegionList)
            {
                m_listBoxGridBaseRegion.Items.Add(strRegion);
            }

            String strSelected = MTerrain.Instance.GetOnEditGridBaseRegion();
            if (null != strSelected)
            {
                if (m_listBoxGridBaseRegion.Items.Contains(strSelected))
                {
                    m_listBoxGridBaseRegion.SelectedItem = strSelected;
                }
            }
        }

        private void GridBaseAreaPanel_Load(object sender, EventArgs e)
        {
            RefreshRegionList();
        }

        private void m_listBoxGridBaseRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 用户选择一个区域
            if (m_listBoxGridBaseRegion.SelectedItem != null)
            {
                String strName = m_listBoxGridBaseRegion.SelectedItem.ToString();
                MTerrain.Instance.SetOnEditGridBaseRegion(strName);

                int iNumGrids = MTerrain.Instance.GetOnEditRegionGridCount();
                m_textBoxGridCount.Text = iNumGrids.ToString();
            }
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            // 删除区域
            if (m_listBoxGridBaseRegion.SelectedItem != null)
            {
                String strName = m_listBoxGridBaseRegion.SelectedItem.ToString();
                MTerrain.Instance.RemoveGridBaseRegionRegion(strName);
                RefreshRegionList();
                int iNumGrids = MTerrain.Instance.GetOnEditRegionGridCount();
                m_textBoxGridCount.Text = iNumGrids.ToString();
            }
        }

        private void GridBaseAreaPanel_Enter(object sender, EventArgs e)
        {
            // 进入焦点，刷新当前区域 grid 数量
            int iNumGrids = MTerrain.Instance.GetOnEditRegionGridCount();
            m_textBoxGridCount.Text = iNumGrids.ToString();
        }

        private void m_btnGenerateEntity_Click(object sender, EventArgs e)
        {
            if (m_listBoxGridBaseRegion.SelectedIndex == -1)
            {
                MessageBox.Show("请先选择一个区域再种植模型.");
                return;
            }

            GenerateEntityPanel form = new GenerateEntityPanel();
            form.Show();
        }

        private void m_toolStripMenuItemGrow_Click(object sender, EventArgs e)
        {
            // 生长模型
            m_btnGenerateEntity_Click(sender, e);
        }

        private void m_toolStripMenuItemSpray_Click(object sender, EventArgs e)
        {
            // 生成海岸线
            if (m_listBoxGridBaseRegion.SelectedIndex == -1)
            {
                MessageBox.Show("请先选择一个区域再生成排浪.");
                return;
            }

            SprayGeneratePanel form = new SprayGeneratePanel();
            form.ShowDialog();
        }

        private void m_toolStripMenuItemSetAlpha_Click(object sender, EventArgs e)
        {
            // 设置 alpha 值
            //MTerrain.Instance.SetTerrainAlpha(1.0f);
            // modify [11/24/2009]
            MTerrain.Instance.SetTerrainHidden(false);
        }

        private void toolStripMenuItemHideTerrain_Click(object sender, EventArgs e)
        {
            // 设置 alpha 值
            //MTerrain.Instance.SetTerrainAlpha(0.0f);
            // modify [11/24/2009]
            MTerrain.Instance.SetTerrainHidden(true);

            if (m_checkBoxInvisableWithBlock.Checked)
            {
                // 将当前区域设置为碰撞
                MTerrain.Instance.SetTerrainProperty(1);
            }
        }

        private void m_toolStripMenuItemWater_Click(object sender, EventArgs e)
        {
            // 生成海岸线
            if (m_listBoxGridBaseRegion.SelectedIndex == -1)
            {
                MessageBox.Show("请先选择一个区域再生成排浪.");
                return;
            }

            WaterPanel form = new WaterPanel();
            form.ShowDialog();
        }

    }
}