﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class PostEffectPanel : Form
    {
        public PostEffectPanel()
        {
            InitializeComponent();
            InitEffectParamters();
           
          }
        private void InitEffectParamters()
        {
            m_IsPostEffectEnableCheckBox.Checked = MFramework.Instance.Renderer.GetColorAdjustEffectEnabled();

            m_fBrightAdjustValue = MFramework.Instance.Renderer.GetPostEffectByType(0);
            m_textBoxBrightAdjustValue.Text = m_fBrightAdjustValue.ToString();

            m_fContrastAdjustValue = MFramework.Instance.Renderer.GetPostEffectByType(1);
               m_textBoxContrastAdjustValue.Text = m_fContrastAdjustValue.ToString();

            m_kHSLAdjustValueH = MFramework.Instance.Renderer.GetPostEffectByType(2);
             m_textBoxHLSAdjustHValue.Text = m_kHSLAdjustValueH.ToString();

            m_kHSLAdjustValueS = MFramework.Instance.Renderer.GetPostEffectByType(3);
            m_textBoxHLSAdjustSValue.Text = m_kHSLAdjustValueS.ToString();

           

            // initail bloom state [6/15/2009]
            m_IsBloomEffectEnableCheckBox.Checked = MFramework.Instance.Renderer.GetBloomEffectEnabled();

            m_IsWaterScene.Checked = MFramework.Instance.Scene.GetWaterSceneEnabled();
            m_fWaterSceneCauseSize = MFramework.Instance.Scene.GetWaterSceneCauseSize();
            m_fWaterSceneCauseXOffset = MFramework.Instance.Scene.GetWaterSceneCauseXOffset();
            m_fWaterSceneCauseYOffset = MFramework.Instance.Scene.GetWaterSceneCauseYOffset();
            m_fWaterSceneCauseFactor = MFramework.Instance.Scene.GetWaterSceneCauseFactor();
            textBox_WaterCauseSize.Text = m_fWaterSceneCauseSize.ToString();
            textBox_WaterCauseXOffset.Text = m_fWaterSceneCauseXOffset.ToString();
            textBox_WaterCauseYOffset.Text = m_fWaterSceneCauseYOffset.ToString();
            textBox_WaterCauseFactor.Text = m_fWaterSceneCauseFactor.ToString();

            MFramework.Instance.Scene.SetWaterSceneCauseParameter(m_fWaterSceneCauseSize, m_fWaterSceneCauseXOffset, m_fWaterSceneCauseYOffset, m_fWaterSceneCauseFactor);

            m_fBloomLumValue = MFramework.Instance.Renderer.GetBloomValueByType(0);
            m_textBoxBloomLumValue.Text = m_fBloomLumValue.ToString();

            m_fBloomScaleValue = MFramework.Instance.Renderer.GetBloomValueByType(1);
            m_textBoxBloomScaleValue.Text = m_fBloomScaleValue.ToString();
        }

        private void UpdateWaterSceneParameter()
        {
            MFramework.Instance.Scene.SetWaterSceneCauseParameter(m_fWaterSceneCauseSize, m_fWaterSceneCauseXOffset, m_fWaterSceneCauseYOffset,m_fWaterSceneCauseFactor);
        }

        private void m_textBoxBrightAdjustValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    float fTop = 4.0f;
                    float fBottom = -6.0f;
                    if ((float)Convert.ToDouble(m_textBoxBrightAdjustValue.Text) > fTop)
                    {
                        m_textBoxBrightAdjustValue.Text = fTop.ToString();
                    }
                    else if ((float)Convert.ToDouble(m_textBoxBrightAdjustValue.Text) < fBottom)
                    {
                        m_textBoxBrightAdjustValue.Text = fBottom.ToString();
                    }
                    m_fBrightAdjustValue = (float)Convert.ToDouble(m_textBoxBrightAdjustValue.Text);
                    
                }
                catch (Exception)
                {
                    m_fBrightAdjustValue = 0.0f;
                    m_textBoxBrightAdjustValue.Text = "0";
                    return;
                }

                // trackBar上的数值响应更改
                //m_BrightAdjust.Value = (int)Math.Round(m_fBrightAdjustValue * m_Divider);
                if (m_IsPostEffectEnableCheckBox.Checked)
                {
                    MFramework.Instance.Renderer.SetPostEffectByType(0,m_fBrightAdjustValue);
                    MFramework.Instance.Renderer.Render();
                }


            }
        }

        private void m_IsPostEffectCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            MFramework.Instance.Renderer.SetColorAdjustEffectEnabled(m_IsPostEffectEnableCheckBox.Checked);
        }

        private void m_textBoxContrastAdjustValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    float fTop = 4.0f;
                    float fBottom = -6.0f;
                    if ((float)Convert.ToDouble(m_textBoxContrastAdjustValue.Text) > fTop)
                    {
                        m_textBoxContrastAdjustValue.Text = fTop.ToString();
                    }
                    else if ((float)Convert.ToDouble(m_textBoxContrastAdjustValue.Text) < fBottom)
                    {
                        m_textBoxContrastAdjustValue.Text = fBottom.ToString();
                    }
                    m_fContrastAdjustValue = (float)Convert.ToDouble(m_textBoxContrastAdjustValue.Text);

                }
                catch (Exception)
                {
                    m_fContrastAdjustValue = 0.0f;
                    m_textBoxContrastAdjustValue.Text = "0";
                    return;
                }

                // trackBar上的数值响应更改
                //m_BrightAdjust.Value = (int)Math.Round(m_fBrightAdjustValue * m_Divider);
                if (m_IsPostEffectEnableCheckBox.Checked)
                {
                    MFramework.Instance.Renderer.SetPostEffectByType(1, m_fContrastAdjustValue);
                    MFramework.Instance.Renderer.Render();
                }
            }             
        }

        private void m_textBoxHLSAdjustHValue_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                float fTop = 100.0f;
                float fBottom = -100.0f;

                if ((float)Convert.ToDouble(m_textBoxHLSAdjustHValue.Text) > fTop)
                {
                    m_textBoxHLSAdjustHValue.Text = fTop.ToString();
                }
                else if ((float)Convert.ToDouble(m_textBoxHLSAdjustHValue.Text) < fBottom)
                {
                    m_textBoxHLSAdjustHValue.Text = fBottom.ToString();
                }

                try
                {
                    m_kHSLAdjustValueH = (float)Convert.ToDouble(m_textBoxHLSAdjustHValue.Text) / m_Divider;
                }
                catch (Exception)
                {
                    m_kHSLAdjustValueH = 0.0f;
                    m_textBoxHLSAdjustHValue.Text = "0";
                    return;
                }

                if (m_IsPostEffectEnableCheckBox.Checked)
                {
                    MFramework.Instance.Renderer.SetPostEffectByType(2,m_kHSLAdjustValueH);
                    MFramework.Instance.Renderer.Render();
                }
            }
        }


        private void m_textBoxHLSAdjustSValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                float fTop = 4.0f;
                float fBottom = -6.0f;

                if ((float)Convert.ToDouble(m_textBoxHLSAdjustSValue.Text) > fTop)
                {
                    m_textBoxHLSAdjustSValue.Text = fTop.ToString();
                }
                else if ((float)Convert.ToDouble(m_textBoxHLSAdjustSValue.Text) < fBottom)
                {
                    m_textBoxHLSAdjustSValue.Text = fBottom.ToString();
                }
   
                m_kHSLAdjustValueS = (float)Convert.ToDouble(m_textBoxHLSAdjustSValue.Text);
                if (m_IsPostEffectEnableCheckBox.Checked)
                {
                    MFramework.Instance.Renderer.SetPostEffectByType(3,m_kHSLAdjustValueS);
                    MFramework.Instance.Renderer.Render();
                }
            }
        }

        private void m_IsBloomEffectEnableCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            MFramework.Instance.Renderer.SetBloomEffectEnabled(m_IsBloomEffectEnableCheckBox.Checked);
        }

        private void m_textBoxBloomLumValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    m_fBloomLumValue = (float)Convert.ToDouble(m_textBoxBloomLumValue.Text);
                }
                catch (Exception)
                {
                    m_fBloomLumValue = 1.0f;
                    m_textBoxBloomLumValue.Text = "1.00f";                 
                    return;
                }
            
                if (m_IsBloomEffectEnableCheckBox.Checked)
                {
                    MFramework.Instance.Renderer.SetBloomEffectByType(0, m_fBloomLumValue);
                    MFramework.Instance.Renderer.Render();
                }


            }
        }

        private void m_textBoxBloomScaleValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    m_fBloomScaleValue = (float)Convert.ToDouble(m_textBoxBloomScaleValue.Text);
                }
                catch (Exception)
                {
                    m_fBloomScaleValue = 0.0f;
                    m_textBoxBloomScaleValue.Text = "0.00f";
                    return;
                }

                if (m_IsBloomEffectEnableCheckBox.Checked)
                {
                    MFramework.Instance.Renderer.SetBloomEffectByType(1, m_fBloomScaleValue);
                    MFramework.Instance.Renderer.Render();
                }


            }
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            InitEffectParamters();
        }

        private void m_textBoxHLSAdjustSValue_TextChanged(object sender, EventArgs e)
        {

        }

        private void m_IsWaterScene_CheckedChanged(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetWaterSceneEnabled(m_IsWaterScene.Checked);
        }

        private void textBox_WaterCauseSize_TextChanged(object sender, EventArgs e)
        {
           // UpdateWaterSceneParameter();
        }

        private void textBox_WaterCauseSize_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    m_fWaterSceneCauseSize = (float)Convert.ToDouble(textBox_WaterCauseSize.Text);
                }
                catch (Exception)
                {
                    m_fWaterSceneCauseSize = 0.05f;
                    textBox_WaterCauseSize.Text = m_fWaterSceneCauseSize.ToString();
                }

                UpdateWaterSceneParameter();
            }
        }

        private void textBox_WaterCauseXOffset_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                m_fWaterSceneCauseXOffset = (float)Convert.ToDouble(textBox_WaterCauseXOffset.Text);
            }
            catch (Exception)
            {
                m_fWaterSceneCauseXOffset = 0.00f;
                textBox_WaterCauseXOffset.Text = m_fWaterSceneCauseXOffset.ToString();
            }

            UpdateWaterSceneParameter();
        }

        private void textBox_WaterCauseYOffset_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                m_fWaterSceneCauseYOffset = (float)Convert.ToDouble(textBox_WaterCauseYOffset.Text);
            }
            catch (Exception)
            {
                m_fWaterSceneCauseYOffset = 0.1f;
                textBox_WaterCauseYOffset.Text = m_fWaterSceneCauseYOffset.ToString();
            }

            UpdateWaterSceneParameter();
        }

        private void textBox_WaterCauseFactor_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                m_fWaterSceneCauseFactor = (float)Convert.ToDouble(textBox_WaterCauseFactor.Text);
            }
            catch (Exception)
            {
                m_fWaterSceneCauseFactor = 1.0f;
                textBox_WaterCauseFactor.Text = m_fWaterSceneCauseFactor.ToString();
            }

            UpdateWaterSceneParameter();
        }
    }
}