﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    public partial class DirectionalShadowPanel : Form
    {
        public DirectionalShadowPanel()
        {
            InitializeComponent();
        }

        private void InitLightList()
        {
            MPoint3 sunDir = MFramework.Instance.Scene.GetSunDirection();
            if (sunDir == null)
            {
                m_textBoxMessage.Text = "在场景中没有找到名为 sun 的方向光. 使用默认方向.\r\n";
            }
            else
            {
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void m_btnCastShadow_Click(object sender, EventArgs e)
        {

        }
    }
}