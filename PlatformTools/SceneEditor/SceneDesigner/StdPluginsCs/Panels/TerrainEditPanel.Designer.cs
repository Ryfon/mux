﻿using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class TerrainEditPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_labelinnerradius = new System.Windows.Forms.Label();
            this.m_labelouterradius = new System.Windows.Forms.Label();
            this.m_labelheight = new System.Windows.Forms.Label();
            this.m_trackBarInnerRadius = new System.Windows.Forms.TrackBar();
            this.m_trackBarOuterRadius = new System.Windows.Forms.TrackBar();
            this.m_trackHeight = new System.Windows.Forms.TrackBar();
            this.m_textBoxinnerradius = new System.Windows.Forms.TextBox();
            this.m_textBoxouterradius = new System.Windows.Forms.TextBox();
            this.m_textBoxheight = new System.Windows.Forms.TextBox();
            this.m_groupBoxbrushpreview = new System.Windows.Forms.GroupBox();
            this.m_SelectBrushTextureMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_SelectBrushTextureMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ClearBrushTextureMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_FlatTerrainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_UpTerrainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_FallTerrainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_SmoothTerrainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.m_PaintAddUpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolSelectPaintMode = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripMenuItem_Terrain = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripMenuItem_Material = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripMenuItem_Proprity = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripMenuItem_Water = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripMenuItem_GridArea = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripMenuItemRemoveEntity = new System.Windows.Forms.ToolStripMenuItem();
            this.m_panelbrushpreview = new System.Windows.Forms.Panel();
            this.m_groupBoxBrush = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.m_button_FindPos = new System.Windows.Forms.Button();
            this.m_textBox_FindPos_Y = new System.Windows.Forms.TextBox();
            this.btn_SnapTool = new System.Windows.Forms.Button();
            this.m_HeightLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_tabControlTools = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.m_btnSetAABBScale = new System.Windows.Forms.Button();
            this.m_ShowAABBCheckBox = new System.Windows.Forms.CheckBox();
            this.m_btnPreview = new System.Windows.Forms.Button();
            this.m_btnLightMapPanel = new System.Windows.Forms.Button();
            this.btn_HeightMap = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.m_btnImportShadow = new System.Windows.Forms.Button();
            this.m_btnExportShadow = new System.Windows.Forms.Button();
            this.btn_AmbientOcclusionShadow = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.m_textBoxOptimizeSensitivity = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_trackBarOptimizeSensitivity = new System.Windows.Forms.TrackBar();
            this.TerrainOptimizeBtn = new System.Windows.Forms.Button();
            this.TerrainRevertBtn = new System.Windows.Forms.Button();
            this.m_pictureBoxMaterialPreview2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_SelectMatiralButton = new System.Windows.Forms.Button();
            this.m_TerrainChunkY = new System.Windows.Forms.TextBox();
            this.m_TerrainChunkX = new System.Windows.Forms.TextBox();
            this.TerrainCreateButton = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_PostEffect = new System.Windows.Forms.Button();
            this.m_checkBoxPaintWater = new System.Windows.Forms.CheckBox();
            this.m_ShowChunkBorderCheckBox = new System.Windows.Forms.CheckBox();
            this.m_buttonTexDelete3 = new System.Windows.Forms.Button();
            this.m_buttonTexDelete2 = new System.Windows.Forms.Button();
            this.m_buttonTexDelete1 = new System.Windows.Forms.Button();
            this.m_checkBoxPaintColor = new System.Windows.Forms.CheckBox();
            this.m_checkBoxPaintTexture = new System.Windows.Forms.CheckBox();
            this.m_pictureBoxBrushColorPreview = new System.Windows.Forms.PictureBox();
            this.m_SelectColorBtn = new System.Windows.Forms.Button();
            this.m_buttonGetCurrentChunkTex = new System.Windows.Forms.Button();
            this.m_radioButtonTex3 = new System.Windows.Forms.RadioButton();
            this.m_radioButtonTex2 = new System.Windows.Forms.RadioButton();
            this.m_radioButtonTex1 = new System.Windows.Forms.RadioButton();
            this.m_radioButtonTex0 = new System.Windows.Forms.RadioButton();
            this.m_buttonTex3 = new System.Windows.Forms.Button();
            this.m_buttonTex2 = new System.Windows.Forms.Button();
            this.m_buttonTex1 = new System.Windows.Forms.Button();
            this.m_buttonTex0 = new System.Windows.Forms.Button();
            this.m_textBoxTex3 = new System.Windows.Forms.TextBox();
            this.m_textBoxTex2 = new System.Windows.Forms.TextBox();
            this.m_textBoxTex1 = new System.Windows.Forms.TextBox();
            this.m_textBoxTex0 = new System.Windows.Forms.TextBox();
            this.m_SelectMtlBtn = new System.Windows.Forms.Button();
            this.m_pictureBoxMaterialPreview = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.GridPropertyPerctextBox = new System.Windows.Forms.TextBox();
            this.GetGridPropertyPercentBtn = new System.Windows.Forms.Button();
            this.CreateRegProByMtlBtn = new System.Windows.Forms.Button();
            this.ImportCollisionBlockBtn = new System.Windows.Forms.Button();
            this.ExportCollisionBlockBtn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.m_TerrPropIDLabel = new System.Windows.Forms.Label();
            this.m_pictureBoxPropColorPreview = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_TerrSurPropComboBox = new System.Windows.Forms.ComboBox();
            this.DegreeTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ClearCollisionBlockBtn = new System.Windows.Forms.Button();
            this.TerrToCollisionBlockBtn = new System.Windows.Forms.Button();
            this.BakeCollisionBlockBtn = new System.Windows.Forms.Button();
            this.m_ShowCollisionDataCheckBox = new System.Windows.Forms.CheckBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btn_BackLightToEntity = new System.Windows.Forms.Button();
            this.btn_ModelScreenShotor = new System.Windows.Forms.Button();
            this.m_btnCityLevelEditor = new System.Windows.Forms.Button();
            this.m_btnEntityExportSetting = new System.Windows.Forms.Button();
            this.m_btnEntityAffectLights = new System.Windows.Forms.Button();
            this.m_btnColorPalette = new System.Windows.Forms.Button();
            this.m_btnPostEffectTools = new System.Windows.Forms.Button();
            this.m_btnGridBaseArea = new System.Windows.Forms.Button();
            this.m_btnChunkEntityManager = new System.Windows.Forms.Button();
            this.m_btnBakeLightToTerrainVertexColor = new System.Windows.Forms.Button();
            this.m_btnRegionTool = new System.Windows.Forms.Button();
            this.m_btnLayerFogTools = new System.Windows.Forms.Button();
            this.m_btnBakeLightColorToVertex = new System.Windows.Forms.Button();
            this.m_btnHeightCheck = new System.Windows.Forms.Button();
            this.m_textBox_FindPos_X = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_trackBarInnerRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_trackBarOuterRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_trackHeight)).BeginInit();
            this.m_groupBoxbrushpreview.SuspendLayout();
            this.m_SelectBrushTextureMenu.SuspendLayout();
            this.m_groupBoxBrush.SuspendLayout();
            this.m_tabControlTools.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_trackBarOptimizeSensitivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxMaterialPreview2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxBrushColorPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxMaterialPreview)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxPropColorPreview)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_labelinnerradius
            // 
            this.m_labelinnerradius.Location = new System.Drawing.Point(6, 21);
            this.m_labelinnerradius.Name = "m_labelinnerradius";
            this.m_labelinnerradius.Size = new System.Drawing.Size(48, 21);
            this.m_labelinnerradius.TabIndex = 0;
            this.m_labelinnerradius.Text = "内半径";
            // 
            // m_labelouterradius
            // 
            this.m_labelouterradius.Location = new System.Drawing.Point(6, 57);
            this.m_labelouterradius.Name = "m_labelouterradius";
            this.m_labelouterradius.Size = new System.Drawing.Size(48, 15);
            this.m_labelouterradius.TabIndex = 1;
            this.m_labelouterradius.Text = "外半径";
            // 
            // m_labelheight
            // 
            this.m_labelheight.Location = new System.Drawing.Point(6, 90);
            this.m_labelheight.Name = "m_labelheight";
            this.m_labelheight.Size = new System.Drawing.Size(48, 12);
            this.m_labelheight.TabIndex = 2;
            this.m_labelheight.Text = "高度";
            // 
            // m_trackBarInnerRadius
            // 
            this.m_trackBarInnerRadius.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.m_trackBarInnerRadius.AutoSize = false;
            this.m_trackBarInnerRadius.Location = new System.Drawing.Point(66, 20);
            this.m_trackBarInnerRadius.Maximum = 200;
            this.m_trackBarInnerRadius.Name = "m_trackBarInnerRadius";
            this.m_trackBarInnerRadius.Size = new System.Drawing.Size(123, 45);
            this.m_trackBarInnerRadius.TabIndex = 3;
            this.m_trackBarInnerRadius.Scroll += new System.EventHandler(this.m_trackBarinnerradius_Scroll);
            // 
            // m_trackBarOuterRadius
            // 
            this.m_trackBarOuterRadius.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.m_trackBarOuterRadius.AutoSize = false;
            this.m_trackBarOuterRadius.Location = new System.Drawing.Point(66, 48);
            this.m_trackBarOuterRadius.Maximum = 200;
            this.m_trackBarOuterRadius.Name = "m_trackBarOuterRadius";
            this.m_trackBarOuterRadius.Size = new System.Drawing.Size(123, 51);
            this.m_trackBarOuterRadius.TabIndex = 4;
            this.m_trackBarOuterRadius.Scroll += new System.EventHandler(this.m_trackBarouterradius_Scroll);
            // 
            // m_trackHeight
            // 
            this.m_trackHeight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.m_trackHeight.AutoSize = false;
            this.m_trackHeight.LargeChange = 1;
            this.m_trackHeight.Location = new System.Drawing.Point(66, 81);
            this.m_trackHeight.Maximum = 100;
            this.m_trackHeight.Minimum = 1;
            this.m_trackHeight.Name = "m_trackHeight";
            this.m_trackHeight.Size = new System.Drawing.Size(123, 43);
            this.m_trackHeight.TabIndex = 5;
            this.m_trackHeight.Value = 1;
            this.m_trackHeight.Scroll += new System.EventHandler(this.m_trackBargradualchange_Scroll);
            // 
            // m_textBoxinnerradius
            // 
            this.m_textBoxinnerradius.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_textBoxinnerradius.Location = new System.Drawing.Point(216, 21);
            this.m_textBoxinnerradius.Name = "m_textBoxinnerradius";
            this.m_textBoxinnerradius.Size = new System.Drawing.Size(32, 21);
            this.m_textBoxinnerradius.TabIndex = 6;
            this.m_textBoxinnerradius.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxinnerradius_KeyDown);
            // 
            // m_textBoxouterradius
            // 
            this.m_textBoxouterradius.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_textBoxouterradius.Location = new System.Drawing.Point(216, 51);
            this.m_textBoxouterradius.Name = "m_textBoxouterradius";
            this.m_textBoxouterradius.Size = new System.Drawing.Size(32, 21);
            this.m_textBoxouterradius.TabIndex = 7;
            this.m_textBoxouterradius.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxouterradius_KeyDown);
            // 
            // m_textBoxheight
            // 
            this.m_textBoxheight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_textBoxheight.Location = new System.Drawing.Point(216, 81);
            this.m_textBoxheight.Name = "m_textBoxheight";
            this.m_textBoxheight.Size = new System.Drawing.Size(32, 21);
            this.m_textBoxheight.TabIndex = 8;
            this.m_textBoxheight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxheight_KeyDown);
            // 
            // m_groupBoxbrushpreview
            // 
            this.m_groupBoxbrushpreview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_groupBoxbrushpreview.ContextMenuStrip = this.m_SelectBrushTextureMenu;
            this.m_groupBoxbrushpreview.Controls.Add(this.m_panelbrushpreview);
            this.m_groupBoxbrushpreview.Location = new System.Drawing.Point(8, 117);
            this.m_groupBoxbrushpreview.MaximumSize = new System.Drawing.Size(101, 117);
            this.m_groupBoxbrushpreview.MinimumSize = new System.Drawing.Size(101, 117);
            this.m_groupBoxbrushpreview.Name = "m_groupBoxbrushpreview";
            this.m_groupBoxbrushpreview.Size = new System.Drawing.Size(101, 117);
            this.m_groupBoxbrushpreview.TabIndex = 9;
            this.m_groupBoxbrushpreview.TabStop = false;
            this.m_groupBoxbrushpreview.Text = "笔刷预览";
            // 
            // m_SelectBrushTextureMenu
            // 
            this.m_SelectBrushTextureMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_SelectBrushTextureMenuItem,
            this.m_ClearBrushTextureMenuItem,
            this.toolStripSeparator1,
            this.m_FlatTerrainMenuItem,
            this.m_UpTerrainMenuItem,
            this.m_FallTerrainMenuItem,
            this.m_SmoothTerrainMenuItem,
            this.toolStripSeparator2,
            this.m_PaintAddUpMenuItem,
            this.m_toolSelectPaintMode});
            this.m_SelectBrushTextureMenu.Name = "m_SelectBrushTextureMenu";
            this.m_SelectBrushTextureMenu.Size = new System.Drawing.Size(143, 192);
            // 
            // m_SelectBrushTextureMenuItem
            // 
            this.m_SelectBrushTextureMenuItem.Name = "m_SelectBrushTextureMenuItem";
            this.m_SelectBrushTextureMenuItem.Size = new System.Drawing.Size(142, 22);
            this.m_SelectBrushTextureMenuItem.Text = "选择纹理";
            this.m_SelectBrushTextureMenuItem.Click += new System.EventHandler(this.m_SelectBrushTextureMenuItem_Click);
            // 
            // m_ClearBrushTextureMenuItem
            // 
            this.m_ClearBrushTextureMenuItem.Name = "m_ClearBrushTextureMenuItem";
            this.m_ClearBrushTextureMenuItem.Size = new System.Drawing.Size(142, 22);
            this.m_ClearBrushTextureMenuItem.Text = "清除纹理";
            this.m_ClearBrushTextureMenuItem.Click += new System.EventHandler(this.m_ClearBrushTextureMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
            // 
            // m_FlatTerrainMenuItem
            // 
            this.m_FlatTerrainMenuItem.Checked = true;
            this.m_FlatTerrainMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_FlatTerrainMenuItem.Name = "m_FlatTerrainMenuItem";
            this.m_FlatTerrainMenuItem.Size = new System.Drawing.Size(142, 22);
            this.m_FlatTerrainMenuItem.Text = "刷平地形";
            this.m_FlatTerrainMenuItem.Click += new System.EventHandler(this.m_FlatTerrainMenuItem_Click);
            // 
            // m_UpTerrainMenuItem
            // 
            this.m_UpTerrainMenuItem.Name = "m_UpTerrainMenuItem";
            this.m_UpTerrainMenuItem.Size = new System.Drawing.Size(142, 22);
            this.m_UpTerrainMenuItem.Text = "抬高地形";
            this.m_UpTerrainMenuItem.Click += new System.EventHandler(this.m_UpTerrainMenuItem_Click);
            // 
            // m_FallTerrainMenuItem
            // 
            this.m_FallTerrainMenuItem.Name = "m_FallTerrainMenuItem";
            this.m_FallTerrainMenuItem.Size = new System.Drawing.Size(142, 22);
            this.m_FallTerrainMenuItem.Text = "降低地形";
            this.m_FallTerrainMenuItem.Click += new System.EventHandler(this.m_FallTerrainMenuItem_Click);
            // 
            // m_SmoothTerrainMenuItem
            // 
            this.m_SmoothTerrainMenuItem.Name = "m_SmoothTerrainMenuItem";
            this.m_SmoothTerrainMenuItem.Size = new System.Drawing.Size(142, 22);
            this.m_SmoothTerrainMenuItem.Text = "平滑地形";
            this.m_SmoothTerrainMenuItem.Click += new System.EventHandler(this.m_SmoothTerrainMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(139, 6);
            // 
            // m_PaintAddUpMenuItem
            // 
            this.m_PaintAddUpMenuItem.Checked = true;
            this.m_PaintAddUpMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_PaintAddUpMenuItem.Name = "m_PaintAddUpMenuItem";
            this.m_PaintAddUpMenuItem.Size = new System.Drawing.Size(142, 22);
            this.m_PaintAddUpMenuItem.Text = "绘制效果叠加";
            this.m_PaintAddUpMenuItem.Click += new System.EventHandler(this.m_PaintAddUpMenuItem_Click);
            // 
            // m_toolSelectPaintMode
            // 
            this.m_toolSelectPaintMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_ToolStripMenuItem_Terrain,
            this.m_ToolStripMenuItem_Material,
            this.m_ToolStripMenuItem_Proprity,
            this.m_ToolStripMenuItem_Water,
            this.m_ToolStripMenuItem_GridArea,
            this.m_ToolStripMenuItemRemoveEntity});
            this.m_toolSelectPaintMode.Name = "m_toolSelectPaintMode";
            this.m_toolSelectPaintMode.Size = new System.Drawing.Size(142, 22);
            this.m_toolSelectPaintMode.Text = "笔刷类型";
            this.m_toolSelectPaintMode.DropDownOpening += new System.EventHandler(this.m_toolSelectPaintMode_DropDownOpening);
            // 
            // m_ToolStripMenuItem_Terrain
            // 
            this.m_ToolStripMenuItem_Terrain.Name = "m_ToolStripMenuItem_Terrain";
            this.m_ToolStripMenuItem_Terrain.Size = new System.Drawing.Size(142, 22);
            this.m_ToolStripMenuItem_Terrain.Text = "地形顶点高度";
            this.m_ToolStripMenuItem_Terrain.Click += new System.EventHandler(this.m_ToolStripMenuItem_Terrain_Click);
            // 
            // m_ToolStripMenuItem_Material
            // 
            this.m_ToolStripMenuItem_Material.Name = "m_ToolStripMenuItem_Material";
            this.m_ToolStripMenuItem_Material.Size = new System.Drawing.Size(142, 22);
            this.m_ToolStripMenuItem_Material.Text = "纹理/顶点色";
            this.m_ToolStripMenuItem_Material.Click += new System.EventHandler(this.m_ToolStripMenuItem_Material_Click);
            // 
            // m_ToolStripMenuItem_Proprity
            // 
            this.m_ToolStripMenuItem_Proprity.Name = "m_ToolStripMenuItem_Proprity";
            this.m_ToolStripMenuItem_Proprity.Size = new System.Drawing.Size(142, 22);
            this.m_ToolStripMenuItem_Proprity.Text = "地表属性";
            this.m_ToolStripMenuItem_Proprity.Click += new System.EventHandler(this.m_ToolStripMenuItem_Proprity_Click);
            // 
            // m_ToolStripMenuItem_Water
            // 
            this.m_ToolStripMenuItem_Water.Name = "m_ToolStripMenuItem_Water";
            this.m_ToolStripMenuItem_Water.Size = new System.Drawing.Size(142, 22);
            this.m_ToolStripMenuItem_Water.Text = "其他工具";
            this.m_ToolStripMenuItem_Water.Click += new System.EventHandler(this.m_ToolStripMenuItem_Water_Click);
            // 
            // m_ToolStripMenuItem_GridArea
            // 
            this.m_ToolStripMenuItem_GridArea.Name = "m_ToolStripMenuItem_GridArea";
            this.m_ToolStripMenuItem_GridArea.Size = new System.Drawing.Size(142, 22);
            this.m_ToolStripMenuItem_GridArea.Text = "地表区域";
            this.m_ToolStripMenuItem_GridArea.Click += new System.EventHandler(this.m_ToolStripMenuItem_GridArea_Click);
            // 
            // m_ToolStripMenuItemRemoveEntity
            // 
            this.m_ToolStripMenuItemRemoveEntity.Name = "m_ToolStripMenuItemRemoveEntity";
            this.m_ToolStripMenuItemRemoveEntity.Size = new System.Drawing.Size(142, 22);
            this.m_ToolStripMenuItemRemoveEntity.Text = "删除物件";
            this.m_ToolStripMenuItemRemoveEntity.Click += new System.EventHandler(this.m_ToolStripMenuItemRemoveEntity_Click);
            // 
            // m_panelbrushpreview
            // 
            this.m_panelbrushpreview.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.m_panelbrushpreview.Location = new System.Drawing.Point(6, 14);
            this.m_panelbrushpreview.MaximumSize = new System.Drawing.Size(89, 97);
            this.m_panelbrushpreview.MinimumSize = new System.Drawing.Size(89, 97);
            this.m_panelbrushpreview.Name = "m_panelbrushpreview";
            this.m_panelbrushpreview.Size = new System.Drawing.Size(89, 97);
            this.m_panelbrushpreview.TabIndex = 10;
            this.m_panelbrushpreview.Paint += new System.Windows.Forms.PaintEventHandler(this.m_panelbrushpreview_Paint);
            // 
            // m_groupBoxBrush
            // 
            this.m_groupBoxBrush.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_groupBoxBrush.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_groupBoxBrush.Controls.Add(this.label9);
            this.m_groupBoxBrush.Controls.Add(this.m_button_FindPos);
            this.m_groupBoxBrush.Controls.Add(this.m_textBox_FindPos_Y);
            this.m_groupBoxBrush.Controls.Add(this.btn_SnapTool);
            this.m_groupBoxBrush.Controls.Add(this.m_HeightLabel);
            this.m_groupBoxBrush.Controls.Add(this.label13);
            this.m_groupBoxBrush.Controls.Add(this.label4);
            this.m_groupBoxBrush.Controls.Add(this.label12);
            this.m_groupBoxBrush.Controls.Add(this.label3);
            this.m_groupBoxBrush.Controls.Add(this.m_tabControlTools);
            this.m_groupBoxBrush.Controls.Add(this.m_groupBoxbrushpreview);
            this.m_groupBoxBrush.Controls.Add(this.m_textBox_FindPos_X);
            this.m_groupBoxBrush.Controls.Add(this.m_textBoxheight);
            this.m_groupBoxBrush.Controls.Add(this.m_textBoxouterradius);
            this.m_groupBoxBrush.Controls.Add(this.m_textBoxinnerradius);
            this.m_groupBoxBrush.Controls.Add(this.m_trackHeight);
            this.m_groupBoxBrush.Controls.Add(this.m_trackBarOuterRadius);
            this.m_groupBoxBrush.Controls.Add(this.m_trackBarInnerRadius);
            this.m_groupBoxBrush.Controls.Add(this.m_labelheight);
            this.m_groupBoxBrush.Controls.Add(this.m_labelouterradius);
            this.m_groupBoxBrush.Controls.Add(this.m_labelinnerradius);
            this.m_groupBoxBrush.Location = new System.Drawing.Point(0, 0);
            this.m_groupBoxBrush.MaximumSize = new System.Drawing.Size(400, 800);
            this.m_groupBoxBrush.Name = "m_groupBoxBrush";
            this.m_groupBoxBrush.Size = new System.Drawing.Size(266, 625);
            this.m_groupBoxBrush.TabIndex = 0;
            this.m_groupBoxBrush.TabStop = false;
            this.m_groupBoxBrush.Text = "笔刷";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(120, 147);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 14;
            this.label9.Text = "坐标查找";
            // 
            // m_button_FindPos
            // 
            this.m_button_FindPos.Location = new System.Drawing.Point(173, 139);
            this.m_button_FindPos.Name = "m_button_FindPos";
            this.m_button_FindPos.Size = new System.Drawing.Size(43, 23);
            this.m_button_FindPos.TabIndex = 6;
            this.m_button_FindPos.Text = "查找";
            this.m_button_FindPos.UseVisualStyleBackColor = true;
            this.m_button_FindPos.Click += new System.EventHandler(this.button2_Click);
            // 
            // m_textBox_FindPos_Y
            // 
            this.m_textBox_FindPos_Y.Location = new System.Drawing.Point(183, 168);
            this.m_textBox_FindPos_Y.Name = "m_textBox_FindPos_Y";
            this.m_textBox_FindPos_Y.Size = new System.Drawing.Size(33, 21);
            this.m_textBox_FindPos_Y.TabIndex = 2;
            // 
            // btn_SnapTool
            // 
            this.btn_SnapTool.Location = new System.Drawing.Point(137, 202);
            this.btn_SnapTool.Name = "btn_SnapTool";
            this.btn_SnapTool.Size = new System.Drawing.Size(67, 24);
            this.btn_SnapTool.TabIndex = 13;
            this.btn_SnapTool.Text = "吸点工具";
            this.btn_SnapTool.UseVisualStyleBackColor = true;
            this.btn_SnapTool.Click += new System.EventHandler(this.button1_Click);
            // 
            // m_HeightLabel
            // 
            this.m_HeightLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_HeightLabel.AutoSize = true;
            this.m_HeightLabel.Location = new System.Drawing.Point(249, 87);
            this.m_HeightLabel.Name = "m_HeightLabel";
            this.m_HeightLabel.Size = new System.Drawing.Size(11, 12);
            this.m_HeightLabel.TabIndex = 12;
            this.m_HeightLabel.Text = "m";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(169, 171);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 4;
            this.label13.Text = "Y:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(249, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "m";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(118, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 3;
            this.label12.Text = "X:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(248, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "m";
            // 
            // m_tabControlTools
            // 
            this.m_tabControlTools.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tabControlTools.Controls.Add(this.tabPage1);
            this.m_tabControlTools.Controls.Add(this.tabPage2);
            this.m_tabControlTools.Controls.Add(this.tabPage3);
            this.m_tabControlTools.Controls.Add(this.tabPage4);
            this.m_tabControlTools.Location = new System.Drawing.Point(5, 245);
            this.m_tabControlTools.Name = "m_tabControlTools";
            this.m_tabControlTools.SelectedIndex = 0;
            this.m_tabControlTools.Size = new System.Drawing.Size(251, 374);
            this.m_tabControlTools.TabIndex = 1;
            this.m_tabControlTools.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.m_btnSetAABBScale);
            this.tabPage1.Controls.Add(this.m_ShowAABBCheckBox);
            this.tabPage1.Controls.Add(this.m_btnPreview);
            this.tabPage1.Controls.Add(this.m_btnLightMapPanel);
            this.tabPage1.Controls.Add(this.btn_HeightMap);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.m_btnImportShadow);
            this.tabPage1.Controls.Add(this.m_btnExportShadow);
            this.tabPage1.Controls.Add(this.btn_AmbientOcclusionShadow);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.m_textBoxOptimizeSensitivity);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.m_trackBarOptimizeSensitivity);
            this.tabPage1.Controls.Add(this.TerrainOptimizeBtn);
            this.tabPage1.Controls.Add(this.TerrainRevertBtn);
            this.tabPage1.Controls.Add(this.m_pictureBoxMaterialPreview2);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.m_SelectMatiralButton);
            this.tabPage1.Controls.Add(this.m_TerrainChunkY);
            this.tabPage1.Controls.Add(this.m_TerrainChunkX);
            this.tabPage1.Controls.Add(this.TerrainCreateButton);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(243, 349);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "地形";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // m_btnSetAABBScale
            // 
            this.m_btnSetAABBScale.Location = new System.Drawing.Point(97, 147);
            this.m_btnSetAABBScale.Name = "m_btnSetAABBScale";
            this.m_btnSetAABBScale.Size = new System.Drawing.Size(75, 23);
            this.m_btnSetAABBScale.TabIndex = 27;
            this.m_btnSetAABBScale.Text = "包围盒缩放";
            this.m_btnSetAABBScale.UseVisualStyleBackColor = true;
            this.m_btnSetAABBScale.Click += new System.EventHandler(this.m_btnSetAABBScale_Click);
            // 
            // m_ShowAABBCheckBox
            // 
            this.m_ShowAABBCheckBox.AutoSize = true;
            this.m_ShowAABBCheckBox.Location = new System.Drawing.Point(7, 151);
            this.m_ShowAABBCheckBox.Name = "m_ShowAABBCheckBox";
            this.m_ShowAABBCheckBox.Size = new System.Drawing.Size(84, 16);
            this.m_ShowAABBCheckBox.TabIndex = 15;
            this.m_ShowAABBCheckBox.Text = "显示包围盒";
            this.m_ShowAABBCheckBox.UseVisualStyleBackColor = true;
            this.m_ShowAABBCheckBox.CheckedChanged += new System.EventHandler(this.m_ShowAABBCheckBox_CheckedChanged);
            // 
            // m_btnPreview
            // 
            this.m_btnPreview.Location = new System.Drawing.Point(137, 346);
            this.m_btnPreview.Name = "m_btnPreview";
            this.m_btnPreview.Size = new System.Drawing.Size(39, 23);
            this.m_btnPreview.TabIndex = 26;
            this.m_btnPreview.Text = "预览";
            this.m_btnPreview.UseVisualStyleBackColor = true;
            this.m_btnPreview.Visible = false;
            this.m_btnPreview.Click += new System.EventHandler(this.m_btnPreview_Click);
            // 
            // m_btnLightMapPanel
            // 
            this.m_btnLightMapPanel.Location = new System.Drawing.Point(9, 346);
            this.m_btnLightMapPanel.Name = "m_btnLightMapPanel";
            this.m_btnLightMapPanel.Size = new System.Drawing.Size(114, 23);
            this.m_btnLightMapPanel.TabIndex = 25;
            this.m_btnLightMapPanel.Text = "全局光烘焙";
            this.m_btnLightMapPanel.UseVisualStyleBackColor = true;
            this.m_btnLightMapPanel.Click += new System.EventHandler(this.m_btnLightMapPanel_Click);
            // 
            // btn_HeightMap
            // 
            this.btn_HeightMap.Location = new System.Drawing.Point(9, 318);
            this.btn_HeightMap.Name = "btn_HeightMap";
            this.btn_HeightMap.Size = new System.Drawing.Size(165, 22);
            this.btn_HeightMap.TabIndex = 21;
            this.btn_HeightMap.Text = "高度图";
            this.btn_HeightMap.UseVisualStyleBackColor = true;
            this.btn_HeightMap.Click += new System.EventHandler(this.btn_HeightMap_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 291);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 12);
            this.label11.TabIndex = 19;
            this.label11.Text = "阴影纹理:";
            // 
            // m_btnImportShadow
            // 
            this.m_btnImportShadow.Location = new System.Drawing.Point(130, 285);
            this.m_btnImportShadow.Name = "m_btnImportShadow";
            this.m_btnImportShadow.Size = new System.Drawing.Size(46, 23);
            this.m_btnImportShadow.TabIndex = 16;
            this.m_btnImportShadow.Text = "导入";
            this.m_btnImportShadow.UseVisualStyleBackColor = true;
            this.m_btnImportShadow.Click += new System.EventHandler(this.m_btnImportShadow_Click);
            // 
            // m_btnExportShadow
            // 
            this.m_btnExportShadow.Location = new System.Drawing.Point(72, 285);
            this.m_btnExportShadow.Name = "m_btnExportShadow";
            this.m_btnExportShadow.Size = new System.Drawing.Size(46, 23);
            this.m_btnExportShadow.TabIndex = 15;
            this.m_btnExportShadow.Text = "导出";
            this.m_btnExportShadow.UseVisualStyleBackColor = true;
            this.m_btnExportShadow.Click += new System.EventHandler(this.m_btnExportShadow_Click);
            // 
            // btn_AmbientOcclusionShadow
            // 
            this.btn_AmbientOcclusionShadow.Location = new System.Drawing.Point(14, 256);
            this.btn_AmbientOcclusionShadow.Name = "btn_AmbientOcclusionShadow";
            this.btn_AmbientOcclusionShadow.Size = new System.Drawing.Size(165, 23);
            this.btn_AmbientOcclusionShadow.TabIndex = 14;
            this.btn_AmbientOcclusionShadow.Text = "阴影工具";
            this.btn_AmbientOcclusionShadow.UseVisualStyleBackColor = true;
            this.btn_AmbientOcclusionShadow.Click += new System.EventHandler(this.btn_BakeShadow_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(179, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "设置chunk数. chunk 边长为64米";
            // 
            // m_textBoxOptimizeSensitivity
            // 
            this.m_textBoxOptimizeSensitivity.Location = new System.Drawing.Point(159, 209);
            this.m_textBoxOptimizeSensitivity.Name = "m_textBoxOptimizeSensitivity";
            this.m_textBoxOptimizeSensitivity.Size = new System.Drawing.Size(20, 21);
            this.m_textBoxOptimizeSensitivity.TabIndex = 11;
            this.m_textBoxOptimizeSensitivity.Text = "5";
            this.m_textBoxOptimizeSensitivity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxOptimizeSensitivity_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(-2, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "精确度";
            // 
            // m_trackBarOptimizeSensitivity
            // 
            this.m_trackBarOptimizeSensitivity.Location = new System.Drawing.Point(31, 206);
            this.m_trackBarOptimizeSensitivity.Maximum = 50;
            this.m_trackBarOptimizeSensitivity.Minimum = 1;
            this.m_trackBarOptimizeSensitivity.Name = "m_trackBarOptimizeSensitivity";
            this.m_trackBarOptimizeSensitivity.Size = new System.Drawing.Size(130, 42);
            this.m_trackBarOptimizeSensitivity.TabIndex = 9;
            this.m_trackBarOptimizeSensitivity.Value = 5;
            this.m_trackBarOptimizeSensitivity.Scroll += new System.EventHandler(this.OptimizeSensitivityStrackBar_Scroll);
            // 
            // TerrainOptimizeBtn
            // 
            this.TerrainOptimizeBtn.Location = new System.Drawing.Point(104, 176);
            this.TerrainOptimizeBtn.Name = "TerrainOptimizeBtn";
            this.TerrainOptimizeBtn.Size = new System.Drawing.Size(75, 23);
            this.TerrainOptimizeBtn.TabIndex = 8;
            this.TerrainOptimizeBtn.Text = "地形优化";
            this.TerrainOptimizeBtn.UseVisualStyleBackColor = true;
            this.TerrainOptimizeBtn.Click += new System.EventHandler(this.TerrainOptimizeBtn_Click);
            // 
            // TerrainRevertBtn
            // 
            this.TerrainRevertBtn.Location = new System.Drawing.Point(14, 176);
            this.TerrainRevertBtn.Name = "TerrainRevertBtn";
            this.TerrainRevertBtn.Size = new System.Drawing.Size(75, 23);
            this.TerrainRevertBtn.TabIndex = 7;
            this.TerrainRevertBtn.Text = "地形还原";
            this.TerrainRevertBtn.UseVisualStyleBackColor = true;
            this.TerrainRevertBtn.Click += new System.EventHandler(this.TerrainRevertBtn_Click);
            // 
            // m_pictureBoxMaterialPreview2
            // 
            this.m_pictureBoxMaterialPreview2.BackColor = System.Drawing.Color.DarkGray;
            this.m_pictureBoxMaterialPreview2.Location = new System.Drawing.Point(104, 56);
            this.m_pictureBoxMaterialPreview2.Name = "m_pictureBoxMaterialPreview2";
            this.m_pictureBoxMaterialPreview2.Size = new System.Drawing.Size(80, 80);
            this.m_pictureBoxMaterialPreview2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.m_pictureBoxMaterialPreview2.TabIndex = 6;
            this.m_pictureBoxMaterialPreview2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "Y:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "X:";
            // 
            // m_SelectMatiralButton
            // 
            this.m_SelectMatiralButton.Location = new System.Drawing.Point(6, 56);
            this.m_SelectMatiralButton.Name = "m_SelectMatiralButton";
            this.m_SelectMatiralButton.Size = new System.Drawing.Size(88, 23);
            this.m_SelectMatiralButton.TabIndex = 3;
            this.m_SelectMatiralButton.Text = "选择基本材质";
            this.m_SelectMatiralButton.UseVisualStyleBackColor = true;
            this.m_SelectMatiralButton.Click += new System.EventHandler(this.m_SelectMatiralButton_Click);
            // 
            // m_TerrainChunkY
            // 
            this.m_TerrainChunkY.Location = new System.Drawing.Point(125, 26);
            this.m_TerrainChunkY.Name = "m_TerrainChunkY";
            this.m_TerrainChunkY.Size = new System.Drawing.Size(51, 21);
            this.m_TerrainChunkY.TabIndex = 2;
            // 
            // m_TerrainChunkX
            // 
            this.m_TerrainChunkX.Location = new System.Drawing.Point(34, 26);
            this.m_TerrainChunkX.Name = "m_TerrainChunkX";
            this.m_TerrainChunkX.Size = new System.Drawing.Size(51, 21);
            this.m_TerrainChunkX.TabIndex = 1;
            // 
            // TerrainCreateButton
            // 
            this.TerrainCreateButton.Location = new System.Drawing.Point(6, 85);
            this.TerrainCreateButton.Name = "TerrainCreateButton";
            this.TerrainCreateButton.Size = new System.Drawing.Size(88, 23);
            this.TerrainCreateButton.TabIndex = 0;
            this.TerrainCreateButton.Text = "创建地形";
            this.TerrainCreateButton.UseVisualStyleBackColor = true;
            this.TerrainCreateButton.Click += new System.EventHandler(this.TerrainCreateButton_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.btn_PostEffect);
            this.tabPage2.Controls.Add(this.m_checkBoxPaintWater);
            this.tabPage2.Controls.Add(this.m_ShowChunkBorderCheckBox);
            this.tabPage2.Controls.Add(this.m_buttonTexDelete3);
            this.tabPage2.Controls.Add(this.m_buttonTexDelete2);
            this.tabPage2.Controls.Add(this.m_buttonTexDelete1);
            this.tabPage2.Controls.Add(this.m_checkBoxPaintColor);
            this.tabPage2.Controls.Add(this.m_checkBoxPaintTexture);
            this.tabPage2.Controls.Add(this.m_pictureBoxBrushColorPreview);
            this.tabPage2.Controls.Add(this.m_SelectColorBtn);
            this.tabPage2.Controls.Add(this.m_buttonGetCurrentChunkTex);
            this.tabPage2.Controls.Add(this.m_radioButtonTex3);
            this.tabPage2.Controls.Add(this.m_radioButtonTex2);
            this.tabPage2.Controls.Add(this.m_radioButtonTex1);
            this.tabPage2.Controls.Add(this.m_radioButtonTex0);
            this.tabPage2.Controls.Add(this.m_buttonTex3);
            this.tabPage2.Controls.Add(this.m_buttonTex2);
            this.tabPage2.Controls.Add(this.m_buttonTex1);
            this.tabPage2.Controls.Add(this.m_buttonTex0);
            this.tabPage2.Controls.Add(this.m_textBoxTex3);
            this.tabPage2.Controls.Add(this.m_textBoxTex2);
            this.tabPage2.Controls.Add(this.m_textBoxTex1);
            this.tabPage2.Controls.Add(this.m_textBoxTex0);
            this.tabPage2.Controls.Add(this.m_SelectMtlBtn);
            this.tabPage2.Controls.Add(this.m_pictureBoxMaterialPreview);
            this.tabPage2.Location = new System.Drawing.Point(4, 21);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(243, 349);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "地表材质";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_PostEffect
            // 
            this.btn_PostEffect.Location = new System.Drawing.Point(11, 297);
            this.btn_PostEffect.Name = "btn_PostEffect";
            this.btn_PostEffect.Size = new System.Drawing.Size(124, 23);
            this.btn_PostEffect.TabIndex = 38;
            this.btn_PostEffect.Text = "后期处理";
            this.btn_PostEffect.UseVisualStyleBackColor = true;
            this.btn_PostEffect.Click += new System.EventHandler(this.btn_PostEffect_Click);
            // 
            // m_checkBoxPaintWater
            // 
            this.m_checkBoxPaintWater.AutoSize = true;
            this.m_checkBoxPaintWater.Location = new System.Drawing.Point(176, 6);
            this.m_checkBoxPaintWater.Name = "m_checkBoxPaintWater";
            this.m_checkBoxPaintWater.Size = new System.Drawing.Size(60, 16);
            this.m_checkBoxPaintWater.TabIndex = 37;
            this.m_checkBoxPaintWater.Text = "刷水波";
            this.m_checkBoxPaintWater.UseVisualStyleBackColor = true;
            this.m_checkBoxPaintWater.CheckedChanged += new System.EventHandler(this.m_checkBoxPaintWater_CheckedChanged);
            // 
            // m_ShowChunkBorderCheckBox
            // 
            this.m_ShowChunkBorderCheckBox.AutoSize = true;
            this.m_ShowChunkBorderCheckBox.Location = new System.Drawing.Point(10, 259);
            this.m_ShowChunkBorderCheckBox.Name = "m_ShowChunkBorderCheckBox";
            this.m_ShowChunkBorderCheckBox.Size = new System.Drawing.Size(96, 16);
            this.m_ShowChunkBorderCheckBox.TabIndex = 35;
            this.m_ShowChunkBorderCheckBox.Text = "显示地块边界";
            this.m_ShowChunkBorderCheckBox.UseVisualStyleBackColor = true;
            this.m_ShowChunkBorderCheckBox.CheckedChanged += new System.EventHandler(this.m_ShowChunkBorderCheckBox_CheckedChanged_1);
            // 
            // m_buttonTexDelete3
            // 
            this.m_buttonTexDelete3.Location = new System.Drawing.Point(171, 233);
            this.m_buttonTexDelete3.Name = "m_buttonTexDelete3";
            this.m_buttonTexDelete3.Size = new System.Drawing.Size(23, 20);
            this.m_buttonTexDelete3.TabIndex = 34;
            this.m_buttonTexDelete3.Text = "D";
            this.m_buttonTexDelete3.UseVisualStyleBackColor = true;
            this.m_buttonTexDelete3.Click += new System.EventHandler(this.m_buttonTexDelete3_Click);
            // 
            // m_buttonTexDelete2
            // 
            this.m_buttonTexDelete2.Location = new System.Drawing.Point(171, 204);
            this.m_buttonTexDelete2.Name = "m_buttonTexDelete2";
            this.m_buttonTexDelete2.Size = new System.Drawing.Size(23, 20);
            this.m_buttonTexDelete2.TabIndex = 33;
            this.m_buttonTexDelete2.Text = "D";
            this.m_buttonTexDelete2.UseVisualStyleBackColor = true;
            this.m_buttonTexDelete2.Click += new System.EventHandler(this.m_buttonTexDelete2_Click);
            // 
            // m_buttonTexDelete1
            // 
            this.m_buttonTexDelete1.Location = new System.Drawing.Point(171, 174);
            this.m_buttonTexDelete1.Name = "m_buttonTexDelete1";
            this.m_buttonTexDelete1.Size = new System.Drawing.Size(23, 20);
            this.m_buttonTexDelete1.TabIndex = 32;
            this.m_buttonTexDelete1.Text = "D";
            this.m_buttonTexDelete1.UseVisualStyleBackColor = true;
            this.m_buttonTexDelete1.Click += new System.EventHandler(this.m_buttonTexDelete1_Click);
            // 
            // m_checkBoxPaintColor
            // 
            this.m_checkBoxPaintColor.AutoSize = true;
            this.m_checkBoxPaintColor.Location = new System.Drawing.Point(92, 6);
            this.m_checkBoxPaintColor.Name = "m_checkBoxPaintColor";
            this.m_checkBoxPaintColor.Size = new System.Drawing.Size(84, 16);
            this.m_checkBoxPaintColor.TabIndex = 31;
            this.m_checkBoxPaintColor.Text = "刷顶点颜色";
            this.m_checkBoxPaintColor.UseVisualStyleBackColor = true;
            this.m_checkBoxPaintColor.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // m_checkBoxPaintTexture
            // 
            this.m_checkBoxPaintTexture.AutoSize = true;
            this.m_checkBoxPaintTexture.Checked = true;
            this.m_checkBoxPaintTexture.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxPaintTexture.Location = new System.Drawing.Point(6, 6);
            this.m_checkBoxPaintTexture.Name = "m_checkBoxPaintTexture";
            this.m_checkBoxPaintTexture.Size = new System.Drawing.Size(84, 16);
            this.m_checkBoxPaintTexture.TabIndex = 30;
            this.m_checkBoxPaintTexture.Text = "刷地表纹理";
            this.m_checkBoxPaintTexture.UseVisualStyleBackColor = true;
            this.m_checkBoxPaintTexture.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // m_pictureBoxBrushColorPreview
            // 
            this.m_pictureBoxBrushColorPreview.BackColor = System.Drawing.Color.White;
            this.m_pictureBoxBrushColorPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxBrushColorPreview.Location = new System.Drawing.Point(119, 49);
            this.m_pictureBoxBrushColorPreview.Name = "m_pictureBoxBrushColorPreview";
            this.m_pictureBoxBrushColorPreview.Size = new System.Drawing.Size(64, 64);
            this.m_pictureBoxBrushColorPreview.TabIndex = 29;
            this.m_pictureBoxBrushColorPreview.TabStop = false;
            // 
            // m_SelectColorBtn
            // 
            this.m_SelectColorBtn.Location = new System.Drawing.Point(103, 21);
            this.m_SelectColorBtn.Name = "m_SelectColorBtn";
            this.m_SelectColorBtn.Size = new System.Drawing.Size(93, 23);
            this.m_SelectColorBtn.TabIndex = 28;
            this.m_SelectColorBtn.Text = "选择顶点颜色";
            this.m_SelectColorBtn.UseVisualStyleBackColor = true;
            this.m_SelectColorBtn.Click += new System.EventHandler(this.m_SelectColorBtn_Click);
            // 
            // m_buttonGetCurrentChunkTex
            // 
            this.m_buttonGetCurrentChunkTex.Location = new System.Drawing.Point(34, 117);
            this.m_buttonGetCurrentChunkTex.Name = "m_buttonGetCurrentChunkTex";
            this.m_buttonGetCurrentChunkTex.Size = new System.Drawing.Size(101, 19);
            this.m_buttonGetCurrentChunkTex.TabIndex = 27;
            this.m_buttonGetCurrentChunkTex.Text = "获取chunk纹理";
            this.m_buttonGetCurrentChunkTex.UseVisualStyleBackColor = true;
            this.m_buttonGetCurrentChunkTex.Click += new System.EventHandler(this.m_buttonGetCurrentChunkTex_Click);
            // 
            // m_radioButtonTex3
            // 
            this.m_radioButtonTex3.AutoSize = true;
            this.m_radioButtonTex3.Location = new System.Drawing.Point(6, 237);
            this.m_radioButtonTex3.Name = "m_radioButtonTex3";
            this.m_radioButtonTex3.Size = new System.Drawing.Size(14, 13);
            this.m_radioButtonTex3.TabIndex = 26;
            this.m_radioButtonTex3.TabStop = true;
            this.m_radioButtonTex3.UseVisualStyleBackColor = true;
            this.m_radioButtonTex3.CheckedChanged += new System.EventHandler(this.m_radioButtonTex3_CheckedChanged);
            // 
            // m_radioButtonTex2
            // 
            this.m_radioButtonTex2.AutoSize = true;
            this.m_radioButtonTex2.Location = new System.Drawing.Point(5, 208);
            this.m_radioButtonTex2.Name = "m_radioButtonTex2";
            this.m_radioButtonTex2.Size = new System.Drawing.Size(14, 13);
            this.m_radioButtonTex2.TabIndex = 25;
            this.m_radioButtonTex2.TabStop = true;
            this.m_radioButtonTex2.UseVisualStyleBackColor = true;
            this.m_radioButtonTex2.CheckedChanged += new System.EventHandler(this.m_radioButtonTex2_CheckedChanged);
            // 
            // m_radioButtonTex1
            // 
            this.m_radioButtonTex1.AutoSize = true;
            this.m_radioButtonTex1.Location = new System.Drawing.Point(5, 178);
            this.m_radioButtonTex1.Name = "m_radioButtonTex1";
            this.m_radioButtonTex1.Size = new System.Drawing.Size(14, 13);
            this.m_radioButtonTex1.TabIndex = 24;
            this.m_radioButtonTex1.TabStop = true;
            this.m_radioButtonTex1.UseVisualStyleBackColor = true;
            this.m_radioButtonTex1.CheckedChanged += new System.EventHandler(this.m_radioButtonTex1_CheckedChanged);
            // 
            // m_radioButtonTex0
            // 
            this.m_radioButtonTex0.AutoSize = true;
            this.m_radioButtonTex0.Location = new System.Drawing.Point(5, 148);
            this.m_radioButtonTex0.Name = "m_radioButtonTex0";
            this.m_radioButtonTex0.Size = new System.Drawing.Size(14, 13);
            this.m_radioButtonTex0.TabIndex = 23;
            this.m_radioButtonTex0.TabStop = true;
            this.m_radioButtonTex0.UseVisualStyleBackColor = true;
            this.m_radioButtonTex0.CheckedChanged += new System.EventHandler(this.m_radioButtonTex0_CheckedChanged);
            // 
            // m_buttonTex3
            // 
            this.m_buttonTex3.Location = new System.Drawing.Point(143, 233);
            this.m_buttonTex3.Name = "m_buttonTex3";
            this.m_buttonTex3.Size = new System.Drawing.Size(23, 20);
            this.m_buttonTex3.TabIndex = 16;
            this.m_buttonTex3.Text = "...";
            this.m_buttonTex3.UseVisualStyleBackColor = true;
            this.m_buttonTex3.Click += new System.EventHandler(this.m_buttonTex3_Click);
            // 
            // m_buttonTex2
            // 
            this.m_buttonTex2.Location = new System.Drawing.Point(143, 204);
            this.m_buttonTex2.Name = "m_buttonTex2";
            this.m_buttonTex2.Size = new System.Drawing.Size(23, 20);
            this.m_buttonTex2.TabIndex = 15;
            this.m_buttonTex2.Text = "...";
            this.m_buttonTex2.UseVisualStyleBackColor = true;
            this.m_buttonTex2.Click += new System.EventHandler(this.m_buttonTex2_Click);
            // 
            // m_buttonTex1
            // 
            this.m_buttonTex1.Location = new System.Drawing.Point(143, 174);
            this.m_buttonTex1.Name = "m_buttonTex1";
            this.m_buttonTex1.Size = new System.Drawing.Size(23, 20);
            this.m_buttonTex1.TabIndex = 14;
            this.m_buttonTex1.Text = "...";
            this.m_buttonTex1.UseVisualStyleBackColor = true;
            this.m_buttonTex1.Click += new System.EventHandler(this.m_buttonTex1_Click);
            // 
            // m_buttonTex0
            // 
            this.m_buttonTex0.Location = new System.Drawing.Point(143, 141);
            this.m_buttonTex0.Name = "m_buttonTex0";
            this.m_buttonTex0.Size = new System.Drawing.Size(23, 20);
            this.m_buttonTex0.TabIndex = 13;
            this.m_buttonTex0.Text = "...";
            this.m_buttonTex0.UseVisualStyleBackColor = true;
            this.m_buttonTex0.Click += new System.EventHandler(this.m_buttonTex0_Click);
            // 
            // m_textBoxTex3
            // 
            this.m_textBoxTex3.Location = new System.Drawing.Point(25, 232);
            this.m_textBoxTex3.Name = "m_textBoxTex3";
            this.m_textBoxTex3.Size = new System.Drawing.Size(110, 21);
            this.m_textBoxTex3.TabIndex = 12;
            // 
            // m_textBoxTex2
            // 
            this.m_textBoxTex2.Location = new System.Drawing.Point(25, 200);
            this.m_textBoxTex2.Name = "m_textBoxTex2";
            this.m_textBoxTex2.Size = new System.Drawing.Size(110, 21);
            this.m_textBoxTex2.TabIndex = 11;
            // 
            // m_textBoxTex1
            // 
            this.m_textBoxTex1.Location = new System.Drawing.Point(25, 170);
            this.m_textBoxTex1.Name = "m_textBoxTex1";
            this.m_textBoxTex1.Size = new System.Drawing.Size(110, 21);
            this.m_textBoxTex1.TabIndex = 10;
            // 
            // m_textBoxTex0
            // 
            this.m_textBoxTex0.Location = new System.Drawing.Point(25, 140);
            this.m_textBoxTex0.Name = "m_textBoxTex0";
            this.m_textBoxTex0.Size = new System.Drawing.Size(110, 21);
            this.m_textBoxTex0.TabIndex = 9;
            // 
            // m_SelectMtlBtn
            // 
            this.m_SelectMtlBtn.Location = new System.Drawing.Point(6, 21);
            this.m_SelectMtlBtn.Name = "m_SelectMtlBtn";
            this.m_SelectMtlBtn.Size = new System.Drawing.Size(80, 23);
            this.m_SelectMtlBtn.TabIndex = 4;
            this.m_SelectMtlBtn.Text = "选择新材质";
            this.m_SelectMtlBtn.UseVisualStyleBackColor = true;
            this.m_SelectMtlBtn.Click += new System.EventHandler(this.m_buttonselectmaterial_Click);
            // 
            // m_pictureBoxMaterialPreview
            // 
            this.m_pictureBoxMaterialPreview.BackColor = System.Drawing.Color.DarkGray;
            this.m_pictureBoxMaterialPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxMaterialPreview.Location = new System.Drawing.Point(10, 49);
            this.m_pictureBoxMaterialPreview.Name = "m_pictureBoxMaterialPreview";
            this.m_pictureBoxMaterialPreview.Size = new System.Drawing.Size(64, 64);
            this.m_pictureBoxMaterialPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.m_pictureBoxMaterialPreview.TabIndex = 1;
            this.m_pictureBoxMaterialPreview.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.GridPropertyPerctextBox);
            this.tabPage3.Controls.Add(this.GetGridPropertyPercentBtn);
            this.tabPage3.Controls.Add(this.CreateRegProByMtlBtn);
            this.tabPage3.Controls.Add(this.ImportCollisionBlockBtn);
            this.tabPage3.Controls.Add(this.ExportCollisionBlockBtn);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.m_TerrPropIDLabel);
            this.tabPage3.Controls.Add(this.m_pictureBoxPropColorPreview);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.m_TerrSurPropComboBox);
            this.tabPage3.Controls.Add(this.DegreeTextBox);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.ClearCollisionBlockBtn);
            this.tabPage3.Controls.Add(this.TerrToCollisionBlockBtn);
            this.tabPage3.Controls.Add(this.BakeCollisionBlockBtn);
            this.tabPage3.Controls.Add(this.m_ShowCollisionDataCheckBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 21);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(243, 349);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "地表属性";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(167, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(11, 12);
            this.label14.TabIndex = 23;
            this.label14.Text = "%";
            // 
            // GridPropertyPerctextBox
            // 
            this.GridPropertyPerctextBox.Location = new System.Drawing.Point(115, 75);
            this.GridPropertyPerctextBox.Name = "GridPropertyPerctextBox";
            this.GridPropertyPerctextBox.Size = new System.Drawing.Size(46, 21);
            this.GridPropertyPerctextBox.TabIndex = 22;
            this.GridPropertyPerctextBox.Text = "0";
            // 
            // GetGridPropertyPercentBtn
            // 
            this.GetGridPropertyPercentBtn.Location = new System.Drawing.Point(21, 75);
            this.GetGridPropertyPercentBtn.Name = "GetGridPropertyPercentBtn";
            this.GetGridPropertyPercentBtn.Size = new System.Drawing.Size(81, 23);
            this.GetGridPropertyPercentBtn.TabIndex = 21;
            this.GetGridPropertyPercentBtn.Text = "获取百分比";
            this.GetGridPropertyPercentBtn.UseVisualStyleBackColor = true;
            this.GetGridPropertyPercentBtn.Click += new System.EventHandler(this.GetGridPropertyPercentBtn_Click);
            // 
            // CreateRegProByMtlBtn
            // 
            this.CreateRegProByMtlBtn.Location = new System.Drawing.Point(7, 250);
            this.CreateRegProByMtlBtn.Name = "CreateRegProByMtlBtn";
            this.CreateRegProByMtlBtn.Size = new System.Drawing.Size(172, 24);
            this.CreateRegProByMtlBtn.TabIndex = 20;
            this.CreateRegProByMtlBtn.Text = "地形纹理生成属性";
            this.CreateRegProByMtlBtn.UseVisualStyleBackColor = true;
            this.CreateRegProByMtlBtn.Click += new System.EventHandler(this.CreateRegProByMtlBtn_Click);
            // 
            // ImportCollisionBlockBtn
            // 
            this.ImportCollisionBlockBtn.Location = new System.Drawing.Point(7, 221);
            this.ImportCollisionBlockBtn.Name = "ImportCollisionBlockBtn";
            this.ImportCollisionBlockBtn.Size = new System.Drawing.Size(174, 23);
            this.ImportCollisionBlockBtn.TabIndex = 19;
            this.ImportCollisionBlockBtn.Text = "导入地表属性";
            this.ImportCollisionBlockBtn.UseVisualStyleBackColor = true;
            this.ImportCollisionBlockBtn.Click += new System.EventHandler(this.ImportCollisionBlockBtn_Click);
            // 
            // ExportCollisionBlockBtn
            // 
            this.ExportCollisionBlockBtn.Location = new System.Drawing.Point(7, 191);
            this.ExportCollisionBlockBtn.Name = "ExportCollisionBlockBtn";
            this.ExportCollisionBlockBtn.Size = new System.Drawing.Size(176, 24);
            this.ExportCollisionBlockBtn.TabIndex = 18;
            this.ExportCollisionBlockBtn.Text = "导出地表属性到文件";
            this.ExportCollisionBlockBtn.UseVisualStyleBackColor = true;
            this.ExportCollisionBlockBtn.Click += new System.EventHandler(this.ExportCollisionBlockBtn_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(108, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 12);
            this.label10.TabIndex = 17;
            this.label10.Text = "ID:";
            // 
            // m_TerrPropIDLabel
            // 
            this.m_TerrPropIDLabel.AutoSize = true;
            this.m_TerrPropIDLabel.Location = new System.Drawing.Point(132, 21);
            this.m_TerrPropIDLabel.Name = "m_TerrPropIDLabel";
            this.m_TerrPropIDLabel.Size = new System.Drawing.Size(11, 12);
            this.m_TerrPropIDLabel.TabIndex = 16;
            this.m_TerrPropIDLabel.Text = "0";
            // 
            // m_pictureBoxPropColorPreview
            // 
            this.m_pictureBoxPropColorPreview.BackColor = System.Drawing.SystemColors.Window;
            this.m_pictureBoxPropColorPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxPropColorPreview.Location = new System.Drawing.Point(152, 7);
            this.m_pictureBoxPropColorPreview.Name = "m_pictureBoxPropColorPreview";
            this.m_pictureBoxPropColorPreview.Size = new System.Drawing.Size(36, 36);
            this.m_pictureBoxPropColorPreview.TabIndex = 12;
            this.m_pictureBoxPropColorPreview.TabStop = false;
            this.m_pictureBoxPropColorPreview.Click += new System.EventHandler(this.m_pictureBoxPropColorPreview_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 11;
            this.label8.Text = "属性";
            // 
            // m_TerrSurPropComboBox
            // 
            this.m_TerrSurPropComboBox.FormattingEnabled = true;
            this.m_TerrSurPropComboBox.Location = new System.Drawing.Point(34, 18);
            this.m_TerrSurPropComboBox.Name = "m_TerrSurPropComboBox";
            this.m_TerrSurPropComboBox.Size = new System.Drawing.Size(71, 20);
            this.m_TerrSurPropComboBox.TabIndex = 10;
            this.m_TerrSurPropComboBox.SelectedIndexChanged += new System.EventHandler(this.m_TerrSurPropComboBox_SelectedIndexChanged);
            // 
            // DegreeTextBox
            // 
            this.DegreeTextBox.Location = new System.Drawing.Point(160, 136);
            this.DegreeTextBox.MaxLength = 2;
            this.DegreeTextBox.Name = "DegreeTextBox";
            this.DegreeTextBox.Size = new System.Drawing.Size(23, 21);
            this.DegreeTextBox.TabIndex = 9;
            this.DegreeTextBox.Text = "45";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(126, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 8;
            this.label7.Text = "角度";
            // 
            // ClearCollisionBlockBtn
            // 
            this.ClearCollisionBlockBtn.Location = new System.Drawing.Point(7, 162);
            this.ClearCollisionBlockBtn.Name = "ClearCollisionBlockBtn";
            this.ClearCollisionBlockBtn.Size = new System.Drawing.Size(176, 23);
            this.ClearCollisionBlockBtn.TabIndex = 7;
            this.ClearCollisionBlockBtn.Text = "清除所有地表属性";
            this.ClearCollisionBlockBtn.UseVisualStyleBackColor = true;
            this.ClearCollisionBlockBtn.Click += new System.EventHandler(this.ClearCollisionBlockBtn_Click);
            // 
            // TerrToCollisionBlockBtn
            // 
            this.TerrToCollisionBlockBtn.Location = new System.Drawing.Point(7, 133);
            this.TerrToCollisionBlockBtn.Name = "TerrToCollisionBlockBtn";
            this.TerrToCollisionBlockBtn.Size = new System.Drawing.Size(113, 23);
            this.TerrToCollisionBlockBtn.TabIndex = 6;
            this.TerrToCollisionBlockBtn.Text = "地形生成碰撞块";
            this.TerrToCollisionBlockBtn.UseVisualStyleBackColor = true;
            this.TerrToCollisionBlockBtn.Click += new System.EventHandler(this.TerrToCollisionBlockBtn_Click);
            // 
            // BakeCollisionBlockBtn
            // 
            this.BakeCollisionBlockBtn.Location = new System.Drawing.Point(7, 104);
            this.BakeCollisionBlockBtn.Name = "BakeCollisionBlockBtn";
            this.BakeCollisionBlockBtn.Size = new System.Drawing.Size(176, 23);
            this.BakeCollisionBlockBtn.TabIndex = 5;
            this.BakeCollisionBlockBtn.Text = "烘焙物件到地形属性";
            this.BakeCollisionBlockBtn.UseVisualStyleBackColor = true;
            this.BakeCollisionBlockBtn.Click += new System.EventHandler(this.BakeCollisionBlockBtn_Click);
            // 
            // m_ShowCollisionDataCheckBox
            // 
            this.m_ShowCollisionDataCheckBox.AutoSize = true;
            this.m_ShowCollisionDataCheckBox.Location = new System.Drawing.Point(9, 44);
            this.m_ShowCollisionDataCheckBox.Name = "m_ShowCollisionDataCheckBox";
            this.m_ShowCollisionDataCheckBox.Size = new System.Drawing.Size(96, 16);
            this.m_ShowCollisionDataCheckBox.TabIndex = 1;
            this.m_ShowCollisionDataCheckBox.Text = "显示地表属性";
            this.m_ShowCollisionDataCheckBox.UseVisualStyleBackColor = true;
            this.m_ShowCollisionDataCheckBox.CheckedChanged += new System.EventHandler(this.m_ShowCollisionDataCheckBox_CheckedChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.Controls.Add(this.btn_BackLightToEntity);
            this.tabPage4.Controls.Add(this.btn_ModelScreenShotor);
            this.tabPage4.Controls.Add(this.m_btnCityLevelEditor);
            this.tabPage4.Controls.Add(this.m_btnEntityExportSetting);
            this.tabPage4.Controls.Add(this.m_btnEntityAffectLights);
            this.tabPage4.Controls.Add(this.m_btnColorPalette);
            this.tabPage4.Controls.Add(this.m_btnPostEffectTools);
            this.tabPage4.Controls.Add(this.m_btnGridBaseArea);
            this.tabPage4.Controls.Add(this.m_btnChunkEntityManager);
            this.tabPage4.Controls.Add(this.m_btnBakeLightToTerrainVertexColor);
            this.tabPage4.Controls.Add(this.m_btnRegionTool);
            this.tabPage4.Controls.Add(this.m_btnLayerFogTools);
            this.tabPage4.Controls.Add(this.m_btnBakeLightColorToVertex);
            this.tabPage4.Controls.Add(this.m_btnHeightCheck);
            this.tabPage4.Location = new System.Drawing.Point(4, 21);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(243, 349);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "其他工具";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btn_BackLightToEntity
            // 
            this.btn_BackLightToEntity.Location = new System.Drawing.Point(17, 326);
            this.btn_BackLightToEntity.Name = "btn_BackLightToEntity";
            this.btn_BackLightToEntity.Size = new System.Drawing.Size(160, 24);
            this.btn_BackLightToEntity.TabIndex = 31;
            this.btn_BackLightToEntity.Text = "烘焙物件灯光";
            this.btn_BackLightToEntity.UseVisualStyleBackColor = true;
            this.btn_BackLightToEntity.Click += new System.EventHandler(this.btn_BackLightToEntity_Click);
            // 
            // btn_ModelScreenShotor
            // 
            this.btn_ModelScreenShotor.Location = new System.Drawing.Point(17, 298);
            this.btn_ModelScreenShotor.Name = "btn_ModelScreenShotor";
            this.btn_ModelScreenShotor.Size = new System.Drawing.Size(160, 23);
            this.btn_ModelScreenShotor.TabIndex = 30;
            this.btn_ModelScreenShotor.Text = "模型截图导出";
            this.btn_ModelScreenShotor.UseVisualStyleBackColor = true;
            this.btn_ModelScreenShotor.Click += new System.EventHandler(this.btn_ModelScreenShotor_Click);
            // 
            // m_btnCityLevelEditor
            // 
            this.m_btnCityLevelEditor.Location = new System.Drawing.Point(17, 269);
            this.m_btnCityLevelEditor.Name = "m_btnCityLevelEditor";
            this.m_btnCityLevelEditor.Size = new System.Drawing.Size(161, 23);
            this.m_btnCityLevelEditor.TabIndex = 29;
            this.m_btnCityLevelEditor.Text = "城市等级编辑";
            this.m_btnCityLevelEditor.UseVisualStyleBackColor = true;
            this.m_btnCityLevelEditor.Click += new System.EventHandler(this.m_btnCityLevelEditor_Click);
            // 
            // m_btnEntityExportSetting
            // 
            this.m_btnEntityExportSetting.Location = new System.Drawing.Point(19, 240);
            this.m_btnEntityExportSetting.Name = "m_btnEntityExportSetting";
            this.m_btnEntityExportSetting.Size = new System.Drawing.Size(161, 23);
            this.m_btnEntityExportSetting.TabIndex = 28;
            this.m_btnEntityExportSetting.Text = "Entity 导出设置";
            this.m_btnEntityExportSetting.UseVisualStyleBackColor = true;
            this.m_btnEntityExportSetting.Click += new System.EventHandler(this.m_btnEntityExportSetting_Click);
            // 
            // m_btnEntityAffectLights
            // 
            this.m_btnEntityAffectLights.Location = new System.Drawing.Point(17, 212);
            this.m_btnEntityAffectLights.Name = "m_btnEntityAffectLights";
            this.m_btnEntityAffectLights.Size = new System.Drawing.Size(163, 22);
            this.m_btnEntityAffectLights.TabIndex = 27;
            this.m_btnEntityAffectLights.Text = "物件灯光联系编辑";
            this.m_btnEntityAffectLights.UseVisualStyleBackColor = true;
            this.m_btnEntityAffectLights.Click += new System.EventHandler(this.m_btnEntityAffectLights_Click);
            // 
            // m_btnColorPalette
            // 
            this.m_btnColorPalette.Location = new System.Drawing.Point(14, 184);
            this.m_btnColorPalette.Name = "m_btnColorPalette";
            this.m_btnColorPalette.Size = new System.Drawing.Size(163, 22);
            this.m_btnColorPalette.TabIndex = 26;
            this.m_btnColorPalette.Text = "自定义调色板";
            this.m_btnColorPalette.UseVisualStyleBackColor = true;
            this.m_btnColorPalette.Click += new System.EventHandler(this.m_btnColorPalette_Click);
            // 
            // m_btnPostEffectTools
            // 
            this.m_btnPostEffectTools.Location = new System.Drawing.Point(14, 156);
            this.m_btnPostEffectTools.Name = "m_btnPostEffectTools";
            this.m_btnPostEffectTools.Size = new System.Drawing.Size(163, 22);
            this.m_btnPostEffectTools.TabIndex = 25;
            this.m_btnPostEffectTools.Text = "后期处理";
            this.m_btnPostEffectTools.UseVisualStyleBackColor = true;
            this.m_btnPostEffectTools.Click += new System.EventHandler(this.m_btnPostEffectTools_Click);
            // 
            // m_btnGridBaseArea
            // 
            this.m_btnGridBaseArea.Location = new System.Drawing.Point(14, 126);
            this.m_btnGridBaseArea.Name = "m_btnGridBaseArea";
            this.m_btnGridBaseArea.Size = new System.Drawing.Size(163, 24);
            this.m_btnGridBaseArea.TabIndex = 24;
            this.m_btnGridBaseArea.Text = "Grid 区域相关工具";
            this.m_btnGridBaseArea.UseVisualStyleBackColor = true;
            this.m_btnGridBaseArea.Click += new System.EventHandler(this.m_btnGridBaseArea_Click);
            // 
            // m_btnChunkEntityManager
            // 
            this.m_btnChunkEntityManager.Location = new System.Drawing.Point(14, 96);
            this.m_btnChunkEntityManager.Name = "m_btnChunkEntityManager";
            this.m_btnChunkEntityManager.Size = new System.Drawing.Size(163, 24);
            this.m_btnChunkEntityManager.TabIndex = 23;
            this.m_btnChunkEntityManager.Text = "Chunk 物件筛选";
            this.m_btnChunkEntityManager.UseVisualStyleBackColor = true;
            this.m_btnChunkEntityManager.Click += new System.EventHandler(this.m_btnChunkEntityManager_Click);
            // 
            // m_btnBakeLightToTerrainVertexColor
            // 
            this.m_btnBakeLightToTerrainVertexColor.Location = new System.Drawing.Point(14, 66);
            this.m_btnBakeLightToTerrainVertexColor.Name = "m_btnBakeLightToTerrainVertexColor";
            this.m_btnBakeLightToTerrainVertexColor.Size = new System.Drawing.Size(163, 24);
            this.m_btnBakeLightToTerrainVertexColor.TabIndex = 22;
            this.m_btnBakeLightToTerrainVertexColor.Text = "灯光烘焙到地形(内部)";
            this.m_btnBakeLightToTerrainVertexColor.UseVisualStyleBackColor = true;
            this.m_btnBakeLightToTerrainVertexColor.Click += new System.EventHandler(this.m_btnBakeLightToTerrainVertexColor_Click);
            // 
            // m_btnRegionTool
            // 
            this.m_btnRegionTool.Location = new System.Drawing.Point(17, 386);
            this.m_btnRegionTool.Name = "m_btnRegionTool";
            this.m_btnRegionTool.Size = new System.Drawing.Size(163, 24);
            this.m_btnRegionTool.TabIndex = 21;
            this.m_btnRegionTool.Text = "区域工具";
            this.m_btnRegionTool.UseVisualStyleBackColor = true;
            this.m_btnRegionTool.Visible = false;
            this.m_btnRegionTool.Click += new System.EventHandler(this.m_btnRegionTool_Click);
            // 
            // m_btnLayerFogTools
            // 
            this.m_btnLayerFogTools.Location = new System.Drawing.Point(14, 356);
            this.m_btnLayerFogTools.Name = "m_btnLayerFogTools";
            this.m_btnLayerFogTools.Size = new System.Drawing.Size(163, 24);
            this.m_btnLayerFogTools.TabIndex = 19;
            this.m_btnLayerFogTools.Text = "层雾工具";
            this.m_btnLayerFogTools.UseVisualStyleBackColor = true;
            this.m_btnLayerFogTools.Visible = false;
            this.m_btnLayerFogTools.Click += new System.EventHandler(this.m_btnLayerFogTools_Click);
            // 
            // m_btnBakeLightColorToVertex
            // 
            this.m_btnBakeLightColorToVertex.Location = new System.Drawing.Point(14, 37);
            this.m_btnBakeLightColorToVertex.Name = "m_btnBakeLightColorToVertex";
            this.m_btnBakeLightColorToVertex.Size = new System.Drawing.Size(163, 24);
            this.m_btnBakeLightColorToVertex.TabIndex = 1;
            this.m_btnBakeLightColorToVertex.Text = "灯光烘焙到地形(外部)";
            this.m_btnBakeLightColorToVertex.UseVisualStyleBackColor = true;
            this.m_btnBakeLightColorToVertex.Click += new System.EventHandler(this.m_btnBakeLightColorToVertex_Click);
            // 
            // m_btnHeightCheck
            // 
            this.m_btnHeightCheck.Location = new System.Drawing.Point(14, 8);
            this.m_btnHeightCheck.Name = "m_btnHeightCheck";
            this.m_btnHeightCheck.Size = new System.Drawing.Size(163, 24);
            this.m_btnHeightCheck.TabIndex = 0;
            this.m_btnHeightCheck.Text = "地形高度检查";
            this.m_btnHeightCheck.UseVisualStyleBackColor = true;
            this.m_btnHeightCheck.Click += new System.EventHandler(this.m_btnHeightCheck_Click);
            // 
            // m_textBox_FindPos_X
            // 
            this.m_textBox_FindPos_X.Location = new System.Drawing.Point(135, 168);
            this.m_textBox_FindPos_X.Name = "m_textBox_FindPos_X";
            this.m_textBox_FindPos_X.Size = new System.Drawing.Size(33, 21);
            this.m_textBox_FindPos_X.TabIndex = 1;
            // 
            // TerrainEditPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(269, 683);
            this.Controls.Add(this.m_groupBoxBrush);
            this.Name = "TerrainEditPanel";
            this.Text = "TerrainEditPanel";
            this.Load += new System.EventHandler(this.TerrainEditPanel_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.TerrainEditPanel_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.m_trackBarInnerRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_trackBarOuterRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_trackHeight)).EndInit();
            this.m_groupBoxbrushpreview.ResumeLayout(false);
            this.m_SelectBrushTextureMenu.ResumeLayout(false);
            this.m_groupBoxBrush.ResumeLayout(false);
            this.m_groupBoxBrush.PerformLayout();
            this.m_tabControlTools.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_trackBarOptimizeSensitivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxMaterialPreview2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxBrushColorPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxMaterialPreview)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxPropColorPreview)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label m_labelinnerradius;
        private System.Windows.Forms.Label m_labelouterradius;
        private System.Windows.Forms.Label m_labelheight;
        private System.Windows.Forms.TrackBar m_trackBarInnerRadius;
        private System.Windows.Forms.TrackBar m_trackBarOuterRadius;
        private System.Windows.Forms.TrackBar m_trackHeight;
        private System.Windows.Forms.TextBox m_textBoxinnerradius;
        private System.Windows.Forms.TextBox m_textBoxouterradius;
        private System.Windows.Forms.TextBox m_textBoxheight;
        private System.Windows.Forms.GroupBox m_groupBoxbrushpreview;
        private System.Windows.Forms.GroupBox m_groupBoxBrush;
        private System.Windows.Forms.Panel m_panelbrushpreview;
        private System.Windows.Forms.Label m_HeightLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox m_pictureBoxMaterialPreview2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button m_SelectMatiralButton;
        private System.Windows.Forms.TextBox m_TerrainChunkY;
        private System.Windows.Forms.TextBox m_TerrainChunkX;
        private System.Windows.Forms.Button TerrainCreateButton;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox m_pictureBoxMaterialPreview;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button m_SelectMtlBtn;
        private System.Windows.Forms.CheckBox m_ShowCollisionDataCheckBox;
        private System.Windows.Forms.Button TerrainOptimizeBtn;
        private System.Windows.Forms.Button TerrainRevertBtn;
        private System.Windows.Forms.Button BakeCollisionBlockBtn;
        private System.Windows.Forms.TrackBar m_trackBarOptimizeSensitivity;
        private System.Windows.Forms.TextBox m_textBoxOptimizeSensitivity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ClearCollisionBlockBtn;
        private System.Windows.Forms.Button TerrToCollisionBlockBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox DegreeTextBox;
        private System.Windows.Forms.TextBox m_textBoxTex0;
        private System.Windows.Forms.TextBox m_textBoxTex3;
        private System.Windows.Forms.TextBox m_textBoxTex2;
        private System.Windows.Forms.TextBox m_textBoxTex1;
        private System.Windows.Forms.Button m_buttonTex3;
        private System.Windows.Forms.Button m_buttonTex2;
        private System.Windows.Forms.Button m_buttonTex1;
        private System.Windows.Forms.Button m_buttonTex0;
        private System.Windows.Forms.RadioButton m_radioButtonTex3;
        private System.Windows.Forms.RadioButton m_radioButtonTex2;
        private System.Windows.Forms.RadioButton m_radioButtonTex1;
        private System.Windows.Forms.RadioButton m_radioButtonTex0;
        private int m_iCurrentEditChunk;
        private System.Windows.Forms.Button m_buttonGetCurrentChunkTex;
        private System.Windows.Forms.Button m_SelectColorBtn;
        private System.Windows.Forms.PictureBox m_pictureBoxBrushColorPreview;
        private System.Windows.Forms.ContextMenuStrip m_SelectBrushTextureMenu;
        private System.Windows.Forms.ToolStripMenuItem m_SelectBrushTextureMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_ClearBrushTextureMenuItem;
        private System.Windows.Forms.CheckBox m_checkBoxPaintColor;
        private System.Windows.Forms.CheckBox m_checkBoxPaintTexture;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem m_FlatTerrainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_UpTerrainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_FallTerrainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_SmoothTerrainMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem m_PaintAddUpMenuItem;
        private System.Windows.Forms.PictureBox m_pictureBoxPropColorPreview;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox m_TerrSurPropComboBox;
        private System.Windows.Forms.Label m_TerrPropIDLabel;
        private System.Windows.Forms.Label label10;    // 当前正被编辑的 chunk 索引
        private System.Collections.ArrayList m_TerrSurPropList;
        private System.Windows.Forms.Button btn_AmbientOcclusionShadow;
        private System.Windows.Forms.Button m_btnImportShadow;
        private System.Windows.Forms.Button m_btnExportShadow;    // 地表属性列表
        private ShadowPanel m_PanelShadowTools;
        private FogPanel m_panelFogTools;
        private TerrainHeightCheckPanel m_panelHeightCheckTool;     //地表高度检测工具
        private RegionToolPanel m_panelRegionTool;                  //区域工具
        private TerrainBakeShadowPanel m_panelBaker;                //顶点烘焙
        private WaterPanel m_panelWater;                            //水体
        private PostEffectPanel m_panelPostEffect;                  //后处理工具
        private ModelScreenshot m_panelModelScreenShotor;           // 模型截图导出 [11/30/2009]
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button m_btnBakeLightColorToVertex;
        private System.Windows.Forms.Button m_btnHeightCheck;
        private System.Windows.Forms.Button m_btnLayerFogTools;
        private System.Windows.Forms.Button m_btnRegionTool;
        private System.Windows.Forms.Button m_btnBakeLightToTerrainVertexColor;
        private System.Windows.Forms.ToolStripMenuItem m_toolSelectPaintMode;
        private System.Windows.Forms.ToolStripMenuItem m_ToolStripMenuItem_Terrain;
        private System.Windows.Forms.ToolStripMenuItem m_ToolStripMenuItem_Material;
        private System.Windows.Forms.ToolStripMenuItem m_ToolStripMenuItem_Proprity;
        private System.Windows.Forms.ToolStripMenuItem m_ToolStripMenuItem_Water;
        private System.Windows.Forms.Button m_btnChunkEntityManager;

        private MTerrain.EPainterType m_PaintMode = MTerrain.EPainterType.PT_TERRAIN_VERTEX;
        private System.Windows.Forms.ToolStripMenuItem m_ToolStripMenuItem_GridArea;
        private System.Windows.Forms.Button m_btnGridBaseArea;
        private System.Windows.Forms.ToolStripMenuItem m_ToolStripMenuItemRemoveEntity;
        private System.Windows.Forms.Button ImportCollisionBlockBtn;
        private System.Windows.Forms.Button ExportCollisionBlockBtn;
        private System.Windows.Forms.Button m_btnPostEffectTools;
        private System.Windows.Forms.Button btn_SnapTool;
        private System.Windows.Forms.Button CreateRegProByMtlBtn;
        private System.Windows.Forms.Button m_btnColorPalette;
        private System.Windows.Forms.Button m_btnEntityAffectLights;
        private System.Windows.Forms.TextBox m_textBox_FindPos_X;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox m_textBox_FindPos_Y;
        private System.Windows.Forms.Button m_button_FindPos;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button m_buttonTexDelete3;
        private System.Windows.Forms.Button m_buttonTexDelete2;
        private System.Windows.Forms.Button m_buttonTexDelete1;
        private System.Windows.Forms.Button GetGridPropertyPercentBtn;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox GridPropertyPerctextBox;
        private System.Windows.Forms.Button btn_HeightMap;
        private System.Windows.Forms.Button m_btnEntityExportSetting;
        private System.Windows.Forms.Button m_btnLightMapPanel;
        private System.Windows.Forms.Button m_btnPreview;
        private System.Windows.Forms.CheckBox m_ShowAABBCheckBox;
        private System.Windows.Forms.Button m_btnSetAABBScale;
        private System.Windows.Forms.CheckBox m_ShowChunkBorderCheckBox;
        private System.Windows.Forms.Button m_btnCityLevelEditor;
        private System.Windows.Forms.CheckBox m_checkBoxPaintWater;
        private System.Windows.Forms.Button btn_PostEffect;
        private System.Windows.Forms.Button btn_ModelScreenShotor;
        private System.Windows.Forms.Button btn_BackLightToEntity;
        private System.Windows.Forms.TabControl m_tabControlTools;  // 当前的绘制模式
    }
}