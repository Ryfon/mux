﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class GlobalIlluminationPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.m_tbFakeLightEmissive = new System.Windows.Forms.TextBox();
            this.m_btnAllChunks = new System.Windows.Forms.Button();
            this.m_tbTagChunks = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.m_cbSecondaryIntegrator = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.m_cbPrimaryIntegrator = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.m_tbPTCachesPacing = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.m_tbPTPaths = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.m_tbFGDepth = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.m_tbFGSmooth = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.m_tbFGAccuracy = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.m_tbFGrays = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.m_tbEmissiveMultiplier = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_rtbCommand = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_btnSelectHdr = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.m_tbHdrFile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_tbEnvironmentColorB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_tbEnvironmentColorG = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.m_tbEnvironmentColorR = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_tbEnviromentintensity = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_tbEnvType = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.m_tbMaxLightMapSize = new System.Windows.Forms.TextBox();
            this.m_tbPixelsPerWorldUnit = new System.Windows.Forms.TextBox();
            this.m_tbMinLightMapSize = new System.Windows.Forms.TextBox();
            this.m_btnBakeLightMap = new System.Windows.Forms.Button();
            this.m_SceneAppPreview = new System.Windows.Forms.Button();
            this.m_btnTerrainImportLightMap = new System.Windows.Forms.Button();
            this.m_tbLum = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.m_btnClose = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.m_dgvFileList = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_MenuStripFileList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加转换目录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.添加文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清除所有文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripMenuItemMakeBackup = new System.Windows.Forms.ToolStripMenuItem();
            this.转换ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_rtbOutputInfo = new System.Windows.Forms.RichTextBox();
            this.m_cbFixTerrainLum = new System.Windows.Forms.CheckBox();
            this.m_tbIncMultiple = new System.Windows.Forms.TextBox();
            this.test = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvFileList)).BeginInit();
            this.m_MenuStripFileList.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(430, 447);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "参数";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.m_tbFakeLightEmissive);
            this.panel1.Controls.Add(this.m_btnAllChunks);
            this.panel1.Controls.Add(this.m_tbTagChunks);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.m_cbSecondaryIntegrator);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.m_cbPrimaryIntegrator);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.m_tbPTCachesPacing);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.m_tbPTPaths);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.m_tbFGDepth);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.m_tbFGSmooth);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.m_tbFGAccuracy);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.m_tbFGrays);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.m_tbEmissiveMultiplier);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.m_rtbCommand);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.m_btnSelectHdr);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.m_tbHdrFile);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.m_tbEnvironmentColorB);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.m_tbEnvironmentColorG);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.m_tbEnvironmentColorR);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.m_tbEnviromentintensity);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.m_tbEnvType);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.m_tbMaxLightMapSize);
            this.panel1.Controls.Add(this.m_tbPixelsPerWorldUnit);
            this.panel1.Controls.Add(this.m_tbMinLightMapSize);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(424, 427);
            this.panel1.TabIndex = 20;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(10, 62);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(113, 12);
            this.label21.TabIndex = 41;
            this.label21.Text = "FakeLight Emissive";
            // 
            // m_tbFakeLightEmissive
            // 
            this.m_tbFakeLightEmissive.Location = new System.Drawing.Point(131, 59);
            this.m_tbFakeLightEmissive.Name = "m_tbFakeLightEmissive";
            this.m_tbFakeLightEmissive.Size = new System.Drawing.Size(50, 21);
            this.m_tbFakeLightEmissive.TabIndex = 42;
            this.m_tbFakeLightEmissive.Text = "80";
            // 
            // m_btnAllChunks
            // 
            this.m_btnAllChunks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnAllChunks.Location = new System.Drawing.Point(370, 366);
            this.m_btnAllChunks.Name = "m_btnAllChunks";
            this.m_btnAllChunks.Size = new System.Drawing.Size(47, 23);
            this.m_btnAllChunks.TabIndex = 40;
            this.m_btnAllChunks.Text = "全部";
            this.m_btnAllChunks.UseVisualStyleBackColor = true;
            this.m_btnAllChunks.Click += new System.EventHandler(this.m_btnAllChunks_Click);
            // 
            // m_tbTagChunks
            // 
            this.m_tbTagChunks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tbTagChunks.Location = new System.Drawing.Point(83, 366);
            this.m_tbTagChunks.Name = "m_tbTagChunks";
            this.m_tbTagChunks.Size = new System.Drawing.Size(280, 21);
            this.m_tbTagChunks.TabIndex = 39;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 369);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(71, 12);
            this.label20.TabIndex = 38;
            this.label20.Text = "目标 chunk:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 116);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(119, 12);
            this.label19.TabIndex = 37;
            this.label19.Text = "secondaryintegrator";
            // 
            // m_cbSecondaryIntegrator
            // 
            this.m_cbSecondaryIntegrator.FormattingEnabled = true;
            this.m_cbSecondaryIntegrator.Items.AddRange(new object[] {
            "finalgather",
            "pathtracer"});
            this.m_cbSecondaryIntegrator.Location = new System.Drawing.Point(135, 113);
            this.m_cbSecondaryIntegrator.Name = "m_cbSecondaryIntegrator";
            this.m_cbSecondaryIntegrator.Size = new System.Drawing.Size(134, 20);
            this.m_cbSecondaryIntegrator.TabIndex = 36;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(107, 12);
            this.label18.TabIndex = 35;
            this.label18.Text = "primaryintegrator";
            // 
            // m_cbPrimaryIntegrator
            // 
            this.m_cbPrimaryIntegrator.FormattingEnabled = true;
            this.m_cbPrimaryIntegrator.Items.AddRange(new object[] {
            "finalgather",
            "pathtracer"});
            this.m_cbPrimaryIntegrator.Location = new System.Drawing.Point(135, 86);
            this.m_cbPrimaryIntegrator.Name = "m_cbPrimaryIntegrator";
            this.m_cbPrimaryIntegrator.Size = new System.Drawing.Size(134, 20);
            this.m_cbPrimaryIntegrator.TabIndex = 34;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(166, 204);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 12);
            this.label17.TabIndex = 32;
            this.label17.Text = "ptcachespacing";
            // 
            // m_tbPTCachesPacing
            // 
            this.m_tbPTCachesPacing.Location = new System.Drawing.Point(270, 201);
            this.m_tbPTCachesPacing.Name = "m_tbPTCachesPacing";
            this.m_tbPTCachesPacing.Size = new System.Drawing.Size(50, 21);
            this.m_tbPTCachesPacing.TabIndex = 33;
            this.m_tbPTCachesPacing.Text = "128.0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 204);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 12);
            this.label15.TabIndex = 30;
            this.label15.Text = "ptpaths";
            // 
            // m_tbPTPaths
            // 
            this.m_tbPTPaths.Location = new System.Drawing.Point(75, 201);
            this.m_tbPTPaths.Name = "m_tbPTPaths";
            this.m_tbPTPaths.Size = new System.Drawing.Size(50, 21);
            this.m_tbPTPaths.TabIndex = 31;
            this.m_tbPTPaths.Text = "100000";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(166, 177);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 12);
            this.label16.TabIndex = 28;
            this.label16.Text = "fgdepth";
            // 
            // m_tbFGDepth
            // 
            this.m_tbFGDepth.Location = new System.Drawing.Point(270, 174);
            this.m_tbFGDepth.Name = "m_tbFGDepth";
            this.m_tbFGDepth.Size = new System.Drawing.Size(50, 21);
            this.m_tbFGDepth.TabIndex = 29;
            this.m_tbFGDepth.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 177);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 26;
            this.label13.Text = "fgsmooth";
            // 
            // m_tbFGSmooth
            // 
            this.m_tbFGSmooth.Location = new System.Drawing.Point(75, 174);
            this.m_tbFGSmooth.Name = "m_tbFGSmooth";
            this.m_tbFGSmooth.Size = new System.Drawing.Size(50, 21);
            this.m_tbFGSmooth.TabIndex = 27;
            this.m_tbFGSmooth.Text = "1.0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(166, 150);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 12);
            this.label14.TabIndex = 24;
            this.label14.Text = "fgaccuracy";
            // 
            // m_tbFGAccuracy
            // 
            this.m_tbFGAccuracy.Location = new System.Drawing.Point(270, 147);
            this.m_tbFGAccuracy.Name = "m_tbFGAccuracy";
            this.m_tbFGAccuracy.Size = new System.Drawing.Size(50, 21);
            this.m_tbFGAccuracy.TabIndex = 25;
            this.m_tbFGAccuracy.Text = "1.0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 150);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 22;
            this.label12.Text = "fgrays";
            // 
            // m_tbFGrays
            // 
            this.m_tbFGrays.Location = new System.Drawing.Point(75, 147);
            this.m_tbFGrays.Name = "m_tbFGrays";
            this.m_tbFGrays.Size = new System.Drawing.Size(50, 21);
            this.m_tbFGrays.TabIndex = 23;
            this.m_tbFGrays.Text = "300";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 12);
            this.label11.TabIndex = 20;
            this.label11.Text = "emissivemultiplier";
            // 
            // m_tbEmissiveMultiplier
            // 
            this.m_tbEmissiveMultiplier.Location = new System.Drawing.Point(131, 35);
            this.m_tbEmissiveMultiplier.Name = "m_tbEmissiveMultiplier";
            this.m_tbEmissiveMultiplier.Size = new System.Drawing.Size(50, 21);
            this.m_tbEmissiveMultiplier.TabIndex = 21;
            this.m_tbEmissiveMultiplier.Text = "80";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "pixelsperworldunit";
            // 
            // m_rtbCommand
            // 
            this.m_rtbCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_rtbCommand.Location = new System.Drawing.Point(4, 396);
            this.m_rtbCommand.Name = "m_rtbCommand";
            this.m_rtbCommand.Size = new System.Drawing.Size(403, 28);
            this.m_rtbCommand.TabIndex = 19;
            this.m_rtbCommand.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(202, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "minlightmapsize";
            // 
            // m_btnSelectHdr
            // 
            this.m_btnSelectHdr.Location = new System.Drawing.Point(132, 341);
            this.m_btnSelectHdr.Name = "m_btnSelectHdr";
            this.m_btnSelectHdr.Size = new System.Drawing.Size(22, 20);
            this.m_btnSelectHdr.TabIndex = 18;
            this.m_btnSelectHdr.Text = "...";
            this.m_btnSelectHdr.UseVisualStyleBackColor = true;
            this.m_btnSelectHdr.Click += new System.EventHandler(this.m_btnSelectHdr_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "maxlightmapsize";
            // 
            // m_tbHdrFile
            // 
            this.m_tbHdrFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tbHdrFile.Location = new System.Drawing.Point(165, 341);
            this.m_tbHdrFile.Name = "m_tbHdrFile";
            this.m_tbHdrFile.Size = new System.Drawing.Size(238, 21);
            this.m_tbHdrFile.TabIndex = 17;
            this.m_tbHdrFile.TextChanged += new System.EventHandler(this.m_tbHdrFile_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 236);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "envtype (constant, hdrfile, none) ";
            // 
            // m_tbEnvironmentColorB
            // 
            this.m_tbEnvironmentColorB.Location = new System.Drawing.Point(135, 314);
            this.m_tbEnvironmentColorB.Name = "m_tbEnvironmentColorB";
            this.m_tbEnvironmentColorB.Size = new System.Drawing.Size(50, 21);
            this.m_tbEnvironmentColorB.TabIndex = 16;
            this.m_tbEnvironmentColorB.Text = "0.6";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "environmentintensity";
            // 
            // m_tbEnvironmentColorG
            // 
            this.m_tbEnvironmentColorG.Location = new System.Drawing.Point(306, 287);
            this.m_tbEnvironmentColorG.Name = "m_tbEnvironmentColorG";
            this.m_tbEnvironmentColorG.Size = new System.Drawing.Size(50, 21);
            this.m_tbEnvironmentColorG.TabIndex = 15;
            this.m_tbEnvironmentColorG.Text = "0.45";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 290);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "environmentcolorr";
            // 
            // m_tbEnvironmentColorR
            // 
            this.m_tbEnvironmentColorR.Location = new System.Drawing.Point(135, 287);
            this.m_tbEnvironmentColorR.Name = "m_tbEnvironmentColorR";
            this.m_tbEnvironmentColorR.Size = new System.Drawing.Size(50, 21);
            this.m_tbEnvironmentColorR.TabIndex = 14;
            this.m_tbEnvironmentColorR.Text = "0.4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(193, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "environmentcolorg";
            // 
            // m_tbEnviromentintensity
            // 
            this.m_tbEnviromentintensity.Location = new System.Drawing.Point(135, 260);
            this.m_tbEnviromentintensity.Name = "m_tbEnviromentintensity";
            this.m_tbEnviromentintensity.Size = new System.Drawing.Size(50, 21);
            this.m_tbEnviromentintensity.TabIndex = 13;
            this.m_tbEnviromentintensity.Text = "1.0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 317);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "environmentcolorb";
            // 
            // m_tbEnvType
            // 
            this.m_tbEnvType.Location = new System.Drawing.Point(228, 233);
            this.m_tbEnvType.Name = "m_tbEnvType";
            this.m_tbEnvType.Size = new System.Drawing.Size(86, 21);
            this.m_tbEnvType.TabIndex = 12;
            this.m_tbEnvType.Text = "constant";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 344);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 12);
            this.label9.TabIndex = 8;
            this.label9.Text = "hdrfile (.hdr, .exr)";
            // 
            // m_tbMaxLightMapSize
            // 
            this.m_tbMaxLightMapSize.Location = new System.Drawing.Point(303, 35);
            this.m_tbMaxLightMapSize.Name = "m_tbMaxLightMapSize";
            this.m_tbMaxLightMapSize.Size = new System.Drawing.Size(49, 21);
            this.m_tbMaxLightMapSize.TabIndex = 11;
            this.m_tbMaxLightMapSize.Text = "256";
            // 
            // m_tbPixelsPerWorldUnit
            // 
            this.m_tbPixelsPerWorldUnit.Location = new System.Drawing.Point(131, 8);
            this.m_tbPixelsPerWorldUnit.Name = "m_tbPixelsPerWorldUnit";
            this.m_tbPixelsPerWorldUnit.Size = new System.Drawing.Size(50, 21);
            this.m_tbPixelsPerWorldUnit.TabIndex = 9;
            this.m_tbPixelsPerWorldUnit.Text = "4";
            // 
            // m_tbMinLightMapSize
            // 
            this.m_tbMinLightMapSize.Location = new System.Drawing.Point(303, 8);
            this.m_tbMinLightMapSize.Name = "m_tbMinLightMapSize";
            this.m_tbMinLightMapSize.Size = new System.Drawing.Size(50, 21);
            this.m_tbMinLightMapSize.TabIndex = 10;
            this.m_tbMinLightMapSize.Text = "64";
            // 
            // m_btnBakeLightMap
            // 
            this.m_btnBakeLightMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnBakeLightMap.Location = new System.Drawing.Point(29, 491);
            this.m_btnBakeLightMap.Name = "m_btnBakeLightMap";
            this.m_btnBakeLightMap.Size = new System.Drawing.Size(322, 23);
            this.m_btnBakeLightMap.TabIndex = 1;
            this.m_btnBakeLightMap.Text = "烘焙光照贴图";
            this.m_btnBakeLightMap.UseVisualStyleBackColor = true;
            this.m_btnBakeLightMap.Click += new System.EventHandler(this.m_btnBakeLightMap_Click);
            // 
            // m_SceneAppPreview
            // 
            this.m_SceneAppPreview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_SceneAppPreview.Location = new System.Drawing.Point(29, 574);
            this.m_SceneAppPreview.Name = "m_SceneAppPreview";
            this.m_SceneAppPreview.Size = new System.Drawing.Size(322, 23);
            this.m_SceneAppPreview.TabIndex = 2;
            this.m_SceneAppPreview.Text = "外部程序预览场景";
            this.m_SceneAppPreview.UseVisualStyleBackColor = true;
            this.m_SceneAppPreview.Click += new System.EventHandler(this.m_SceneAppPreview_Click);
            // 
            // m_btnTerrainImportLightMap
            // 
            this.m_btnTerrainImportLightMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnTerrainImportLightMap.Location = new System.Drawing.Point(29, 545);
            this.m_btnTerrainImportLightMap.Name = "m_btnTerrainImportLightMap";
            this.m_btnTerrainImportLightMap.Size = new System.Drawing.Size(322, 23);
            this.m_btnTerrainImportLightMap.TabIndex = 3;
            this.m_btnTerrainImportLightMap.Text = "地形载入光照贴图";
            this.m_btnTerrainImportLightMap.UseVisualStyleBackColor = true;
            this.m_btnTerrainImportLightMap.Click += new System.EventHandler(this.m_btnTerrainImportLightMap_Click);
            // 
            // m_tbLum
            // 
            this.m_tbLum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_tbLum.Location = new System.Drawing.Point(168, 520);
            this.m_tbLum.Name = "m_tbLum";
            this.m_tbLum.Size = new System.Drawing.Size(42, 21);
            this.m_tbLum.TabIndex = 4;
            this.m_tbLum.Text = "80";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 525);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(137, 12);
            this.label10.TabIndex = 5;
            this.label10.Text = "地形 LightMap 转换亮度";
            // 
            // m_btnClose
            // 
            this.m_btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnClose.Location = new System.Drawing.Point(382, 591);
            this.m_btnClose.Name = "m_btnClose";
            this.m_btnClose.Size = new System.Drawing.Size(53, 23);
            this.m_btnClose.TabIndex = 6;
            this.m_btnClose.Text = "关闭";
            this.m_btnClose.UseVisualStyleBackColor = true;
            this.m_btnClose.Click += new System.EventHandler(this.m_btnClose_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(1, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(444, 478);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(436, 453);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "参数设置";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer1);
            this.tabPage2.Location = new System.Drawing.Point(4, 21);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(436, 453);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "文件转换";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.m_dgvFileList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.m_rtbOutputInfo);
            this.splitContainer1.Size = new System.Drawing.Size(430, 447);
            this.splitContainer1.SplitterDistance = 329;
            this.splitContainer1.TabIndex = 1;
            // 
            // m_dgvFileList
            // 
            this.m_dgvFileList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_dgvFileList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.m_dgvFileList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvFileList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.m_dgvFileList.ContextMenuStrip = this.m_MenuStripFileList;
            this.m_dgvFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_dgvFileList.Location = new System.Drawing.Point(0, 0);
            this.m_dgvFileList.Name = "m_dgvFileList";
            this.m_dgvFileList.RowHeadersVisible = false;
            this.m_dgvFileList.RowTemplate.Height = 23;
            this.m_dgvFileList.Size = new System.Drawing.Size(430, 329);
            this.m_dgvFileList.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "目标文件";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 78;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "状态";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 54;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "信息";
            this.Column3.Name = "Column3";
            this.Column3.Width = 54;
            // 
            // m_MenuStripFileList
            // 
            this.m_MenuStripFileList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加转换目录ToolStripMenuItem,
            this.添加文件ToolStripMenuItem,
            this.清除所有文件ToolStripMenuItem,
            this.m_ToolStripMenuItemMakeBackup,
            this.转换ToolStripMenuItem});
            this.m_MenuStripFileList.Name = "m_MenuStripFileList";
            this.m_MenuStripFileList.Size = new System.Drawing.Size(147, 114);
            this.m_MenuStripFileList.Opening += new System.ComponentModel.CancelEventHandler(this.m_MenuStripFileList_Opening);
            // 
            // 添加转换目录ToolStripMenuItem
            // 
            this.添加转换目录ToolStripMenuItem.Name = "添加转换目录ToolStripMenuItem";
            this.添加转换目录ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.添加转换目录ToolStripMenuItem.Text = "添加目录";
            this.添加转换目录ToolStripMenuItem.Click += new System.EventHandler(this.添加转换目录ToolStripMenuItem_Click);
            // 
            // 添加文件ToolStripMenuItem
            // 
            this.添加文件ToolStripMenuItem.Name = "添加文件ToolStripMenuItem";
            this.添加文件ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.添加文件ToolStripMenuItem.Text = "添加文件";
            this.添加文件ToolStripMenuItem.Click += new System.EventHandler(this.添加文件ToolStripMenuItem_Click);
            // 
            // 清除所有文件ToolStripMenuItem
            // 
            this.清除所有文件ToolStripMenuItem.Name = "清除所有文件ToolStripMenuItem";
            this.清除所有文件ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.清除所有文件ToolStripMenuItem.Text = "清除所有文件";
            this.清除所有文件ToolStripMenuItem.Click += new System.EventHandler(this.清除所有文件ToolStripMenuItem_Click);
            // 
            // m_ToolStripMenuItemMakeBackup
            // 
            this.m_ToolStripMenuItemMakeBackup.Name = "m_ToolStripMenuItemMakeBackup";
            this.m_ToolStripMenuItemMakeBackup.Size = new System.Drawing.Size(146, 22);
            this.m_ToolStripMenuItemMakeBackup.Text = "创建倍份";
            this.m_ToolStripMenuItemMakeBackup.Click += new System.EventHandler(this.m_ToolStripMenuItemMakeBackup_Click);
            // 
            // 转换ToolStripMenuItem
            // 
            this.转换ToolStripMenuItem.Name = "转换ToolStripMenuItem";
            this.转换ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.转换ToolStripMenuItem.Text = "转换";
            this.转换ToolStripMenuItem.Click += new System.EventHandler(this.转换ToolStripMenuItem_Click);
            // 
            // m_rtbOutputInfo
            // 
            this.m_rtbOutputInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_rtbOutputInfo.Location = new System.Drawing.Point(0, 0);
            this.m_rtbOutputInfo.Name = "m_rtbOutputInfo";
            this.m_rtbOutputInfo.Size = new System.Drawing.Size(430, 114);
            this.m_rtbOutputInfo.TabIndex = 0;
            this.m_rtbOutputInfo.Text = "";
            // 
            // m_cbFixTerrainLum
            // 
            this.m_cbFixTerrainLum.AutoSize = true;
            this.m_cbFixTerrainLum.Checked = true;
            this.m_cbFixTerrainLum.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_cbFixTerrainLum.Location = new System.Drawing.Point(227, 523);
            this.m_cbFixTerrainLum.Name = "m_cbFixTerrainLum";
            this.m_cbFixTerrainLum.Size = new System.Drawing.Size(96, 16);
            this.m_cbFixTerrainLum.TabIndex = 9;
            this.m_cbFixTerrainLum.Text = "修正地表亮度";
            this.m_cbFixTerrainLum.UseVisualStyleBackColor = true;
            // 
            // m_tbIncMultiple
            // 
            this.m_tbIncMultiple.Location = new System.Drawing.Point(322, 520);
            this.m_tbIncMultiple.Name = "m_tbIncMultiple";
            this.m_tbIncMultiple.Size = new System.Drawing.Size(29, 21);
            this.m_tbIncMultiple.TabIndex = 10;
            this.m_tbIncMultiple.Text = "3.0";
            // 
            // test
            // 
            this.test.Location = new System.Drawing.Point(370, 562);
            this.test.Name = "test";
            this.test.Size = new System.Drawing.Size(75, 23);
            this.test.TabIndex = 11;
            this.test.Text = "测试";
            this.test.UseVisualStyleBackColor = true;
            this.test.Click += new System.EventHandler(this.test_Click);
            // 
            // GlobalIlluminationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 630);
            this.ControlBox = false;
            this.Controls.Add(this.test);
            this.Controls.Add(this.m_tbIncMultiple);
            this.Controls.Add(this.m_cbFixTerrainLum);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.m_btnClose);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.m_tbLum);
            this.Controls.Add(this.m_btnTerrainImportLightMap);
            this.Controls.Add(this.m_SceneAppPreview);
            this.Controls.Add(this.m_btnBakeLightMap);
            this.Name = "GlobalIlluminationPanel";
            this.Text = "全局光烘焙";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvFileList)).EndInit();
            this.m_MenuStripFileList.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button m_btnBakeLightMap;
        private System.Windows.Forms.Button m_SceneAppPreview;
        private System.Windows.Forms.Button m_btnTerrainImportLightMap;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_tbHdrFile;
        private System.Windows.Forms.TextBox m_tbEnvironmentColorB;
        private System.Windows.Forms.TextBox m_tbEnvironmentColorG;
        private System.Windows.Forms.TextBox m_tbEnvironmentColorR;
        private System.Windows.Forms.TextBox m_tbEnviromentintensity;
        private System.Windows.Forms.TextBox m_tbEnvType;
        private System.Windows.Forms.TextBox m_tbMaxLightMapSize;
        private System.Windows.Forms.TextBox m_tbMinLightMapSize;
        private System.Windows.Forms.TextBox m_tbPixelsPerWorldUnit;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox m_tbLum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button m_btnSelectHdr;
        private System.Windows.Forms.RichTextBox m_rtbCommand;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox m_tbFGSmooth;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox m_tbFGAccuracy;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox m_tbFGrays;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox m_tbEmissiveMultiplier;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox m_tbPTPaths;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox m_tbFGDepth;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox m_tbPTCachesPacing;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox m_cbSecondaryIntegrator;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox m_cbPrimaryIntegrator;
        private System.Windows.Forms.Button m_btnClose;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox m_rtbOutputInfo;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView m_dgvFileList;
        private System.Windows.Forms.ContextMenuStrip m_MenuStripFileList;
        private System.Windows.Forms.ToolStripMenuItem 添加转换目录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_ToolStripMenuItemMakeBackup;
        private System.Windows.Forms.ToolStripMenuItem 转换ToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.ToolStripMenuItem 清除所有文件ToolStripMenuItem;
        private System.Windows.Forms.Button m_btnAllChunks;
        private System.Windows.Forms.TextBox m_tbTagChunks;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox m_tbFakeLightEmissive;
        private System.Windows.Forms.CheckBox m_cbFixTerrainLum;
        private System.Windows.Forms.TextBox m_tbIncMultiple;
        private System.Windows.Forms.Button test;
    }
}