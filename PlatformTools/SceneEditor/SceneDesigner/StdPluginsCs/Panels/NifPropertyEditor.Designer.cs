﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class NifPropertyEditor
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_tvModelView = new System.Windows.Forms.TreeView();
            this.m_contextMenuTreeView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.expandAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_btnGetTemplate = new System.Windows.Forms.Button();
            this.m_listBoxPropertyView = new System.Windows.Forms.ListBox();
            this.m_contextMenuPropList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addNiAlphaPropertyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNiSpecularPropertyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNiMaterialPropertyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletePropertyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_groupBoxPropertyEditor = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.m_btnGetSelectedEntity = new System.Windows.Forms.Button();
            this.m_btnSaveToNif = new System.Windows.Forms.Button();
            this.m_btnCheckNif = new System.Windows.Forms.Button();
            this.m_contextMenuTreeView.SuspendLayout();
            this.m_contextMenuPropList.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_tvModelView
            // 
            this.m_tvModelView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tvModelView.ContextMenuStrip = this.m_contextMenuTreeView;
            this.m_tvModelView.Location = new System.Drawing.Point(3, 59);
            this.m_tvModelView.Name = "m_tvModelView";
            this.m_tvModelView.Size = new System.Drawing.Size(180, 377);
            this.m_tvModelView.TabIndex = 0;
            this.m_tvModelView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.m_tvModelView_AfterSelect);
            // 
            // m_contextMenuTreeView
            // 
            this.m_contextMenuTreeView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.expandAllToolStripMenuItem,
            this.collapseAllToolStripMenuItem,
            this.hideToolStripMenuItem,
            this.showToolStripMenuItem});
            this.m_contextMenuTreeView.Name = "m_contextMenuTreeView";
            this.m_contextMenuTreeView.Size = new System.Drawing.Size(143, 92);
            // 
            // expandAllToolStripMenuItem
            // 
            this.expandAllToolStripMenuItem.Name = "expandAllToolStripMenuItem";
            this.expandAllToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.expandAllToolStripMenuItem.Text = "Expand All";
            this.expandAllToolStripMenuItem.Click += new System.EventHandler(this.expandAllToolStripMenuItem_Click);
            // 
            // collapseAllToolStripMenuItem
            // 
            this.collapseAllToolStripMenuItem.Name = "collapseAllToolStripMenuItem";
            this.collapseAllToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.collapseAllToolStripMenuItem.Text = "Collapse All";
            this.collapseAllToolStripMenuItem.Click += new System.EventHandler(this.collapseAllToolStripMenuItem_Click);
            // 
            // hideToolStripMenuItem
            // 
            this.hideToolStripMenuItem.Name = "hideToolStripMenuItem";
            this.hideToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.hideToolStripMenuItem.Text = "Hide";
            this.hideToolStripMenuItem.Click += new System.EventHandler(this.hideToolStripMenuItem_Click);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // m_btnGetTemplate
            // 
            this.m_btnGetTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnGetTemplate.Location = new System.Drawing.Point(3, 3);
            this.m_btnGetTemplate.Name = "m_btnGetTemplate";
            this.m_btnGetTemplate.Size = new System.Drawing.Size(180, 23);
            this.m_btnGetTemplate.TabIndex = 1;
            this.m_btnGetTemplate.Text = "获取当前选择Template";
            this.m_btnGetTemplate.UseVisualStyleBackColor = true;
            this.m_btnGetTemplate.Click += new System.EventHandler(this.m_btnGetModel_Click);
            // 
            // m_listBoxPropertyView
            // 
            this.m_listBoxPropertyView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listBoxPropertyView.ContextMenuStrip = this.m_contextMenuPropList;
            this.m_listBoxPropertyView.FormattingEnabled = true;
            this.m_listBoxPropertyView.ItemHeight = 12;
            this.m_listBoxPropertyView.Location = new System.Drawing.Point(3, 3);
            this.m_listBoxPropertyView.Name = "m_listBoxPropertyView";
            this.m_listBoxPropertyView.Size = new System.Drawing.Size(362, 76);
            this.m_listBoxPropertyView.TabIndex = 2;
            this.m_listBoxPropertyView.SelectedIndexChanged += new System.EventHandler(this.m_listBoxPropertyView_SelectedIndexChanged);
            // 
            // m_contextMenuPropList
            // 
            this.m_contextMenuPropList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNiAlphaPropertyToolStripMenuItem,
            this.addNiSpecularPropertyToolStripMenuItem,
            this.addNiMaterialPropertyToolStripMenuItem,
            this.deletePropertyToolStripMenuItem});
            this.m_contextMenuPropList.Name = "m_contextMenuPropList";
            this.m_contextMenuPropList.Size = new System.Drawing.Size(203, 92);
            // 
            // addNiAlphaPropertyToolStripMenuItem
            // 
            this.addNiAlphaPropertyToolStripMenuItem.Name = "addNiAlphaPropertyToolStripMenuItem";
            this.addNiAlphaPropertyToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.addNiAlphaPropertyToolStripMenuItem.Text = "Add NiAlphaProperty";
            this.addNiAlphaPropertyToolStripMenuItem.Click += new System.EventHandler(this.addNiAlphaPropertyToolStripMenuItem_Click);
            // 
            // addNiSpecularPropertyToolStripMenuItem
            // 
            this.addNiSpecularPropertyToolStripMenuItem.Name = "addNiSpecularPropertyToolStripMenuItem";
            this.addNiSpecularPropertyToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.addNiSpecularPropertyToolStripMenuItem.Text = "Add NiSpecularProperty";
            this.addNiSpecularPropertyToolStripMenuItem.Click += new System.EventHandler(this.addNiSpecularPropertyToolStripMenuItem_Click);
            // 
            // addNiMaterialPropertyToolStripMenuItem
            // 
            this.addNiMaterialPropertyToolStripMenuItem.Name = "addNiMaterialPropertyToolStripMenuItem";
            this.addNiMaterialPropertyToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.addNiMaterialPropertyToolStripMenuItem.Text = "Add NiMaterialProperty";
            this.addNiMaterialPropertyToolStripMenuItem.Click += new System.EventHandler(this.addNiMaterialPropertyToolStripMenuItem_Click);
            // 
            // deletePropertyToolStripMenuItem
            // 
            this.deletePropertyToolStripMenuItem.Name = "deletePropertyToolStripMenuItem";
            this.deletePropertyToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.deletePropertyToolStripMenuItem.Text = "Delete Property";
            this.deletePropertyToolStripMenuItem.Click += new System.EventHandler(this.deletePropertyToolStripMenuItem_Click);
            // 
            // m_groupBoxPropertyEditor
            // 
            this.m_groupBoxPropertyEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_groupBoxPropertyEditor.Location = new System.Drawing.Point(6, 85);
            this.m_groupBoxPropertyEditor.Name = "m_groupBoxPropertyEditor";
            this.m_groupBoxPropertyEditor.Size = new System.Drawing.Size(362, 304);
            this.m_groupBoxPropertyEditor.TabIndex = 3;
            this.m_groupBoxPropertyEditor.TabStop = false;
            this.m_groupBoxPropertyEditor.Text = "属性编辑";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.m_btnGetSelectedEntity);
            this.splitContainer1.Panel1.Controls.Add(this.m_btnGetTemplate);
            this.splitContainer1.Panel1.Controls.Add(this.m_tvModelView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.m_btnCheckNif);
            this.splitContainer1.Panel2.Controls.Add(this.m_btnSaveToNif);
            this.splitContainer1.Panel2.Controls.Add(this.m_groupBoxPropertyEditor);
            this.splitContainer1.Panel2.Controls.Add(this.m_listBoxPropertyView);
            this.splitContainer1.Size = new System.Drawing.Size(558, 439);
            this.splitContainer1.SplitterDistance = 186;
            this.splitContainer1.TabIndex = 4;
            // 
            // m_btnGetSelectedEntity
            // 
            this.m_btnGetSelectedEntity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnGetSelectedEntity.Location = new System.Drawing.Point(3, 30);
            this.m_btnGetSelectedEntity.Name = "m_btnGetSelectedEntity";
            this.m_btnGetSelectedEntity.Size = new System.Drawing.Size(180, 23);
            this.m_btnGetSelectedEntity.TabIndex = 2;
            this.m_btnGetSelectedEntity.Text = "获取当前选择场景物件";
            this.m_btnGetSelectedEntity.UseVisualStyleBackColor = true;
            this.m_btnGetSelectedEntity.Click += new System.EventHandler(this.m_btnGetSelectedEntity_Click);
            // 
            // m_btnSaveToNif
            // 
            this.m_btnSaveToNif.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnSaveToNif.Location = new System.Drawing.Point(281, 404);
            this.m_btnSaveToNif.Name = "m_btnSaveToNif";
            this.m_btnSaveToNif.Size = new System.Drawing.Size(75, 23);
            this.m_btnSaveToNif.TabIndex = 26;
            this.m_btnSaveToNif.Text = "保存到NIF";
            this.m_btnSaveToNif.UseVisualStyleBackColor = true;
            this.m_btnSaveToNif.Click += new System.EventHandler(this.m_btnSaveToNif_Click);
            // 
            // m_btnCheckNif
            // 
            this.m_btnCheckNif.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnCheckNif.Location = new System.Drawing.Point(6, 404);
            this.m_btnCheckNif.Name = "m_btnCheckNif";
            this.m_btnCheckNif.Size = new System.Drawing.Size(75, 23);
            this.m_btnCheckNif.TabIndex = 27;
            this.m_btnCheckNif.Text = "NIF检查";
            this.m_btnCheckNif.UseVisualStyleBackColor = true;
            this.m_btnCheckNif.Click += new System.EventHandler(this.m_btnCheckNif_Click);
            // 
            // NifPropertyEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 439);
            this.Controls.Add(this.splitContainer1);
            this.Name = "NifPropertyEditor";
            this.Text = "NIF 属性编辑器";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.NifPropertyEditor_Load);
            this.m_contextMenuTreeView.ResumeLayout(false);
            this.m_contextMenuPropList.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView m_tvModelView;
        private System.Windows.Forms.Button m_btnGetTemplate;
        private System.Windows.Forms.ListBox m_listBoxPropertyView;
        private System.Windows.Forms.GroupBox m_groupBoxPropertyEditor;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuTreeView;
        private System.Windows.Forms.ToolStripMenuItem expandAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collapseAllToolStripMenuItem;
        private System.Windows.Forms.Button m_btnGetSelectedEntity;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuPropList;
        private System.Windows.Forms.ToolStripMenuItem addNiAlphaPropertyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNiSpecularPropertyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deletePropertyToolStripMenuItem;
        private System.Windows.Forms.Button m_btnSaveToNif;
        private System.Windows.Forms.ToolStripMenuItem addNiMaterialPropertyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.Button m_btnCheckNif;
    }
}