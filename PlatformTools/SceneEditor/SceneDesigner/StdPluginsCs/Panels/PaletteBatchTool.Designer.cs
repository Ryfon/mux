﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class PaletteBatchTool
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.m_textBoxDesPalette = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_BatchCreate = new System.Windows.Forms.Button();
            this.btn_GetDesPalette = new System.Windows.Forms.Button();
            this.m_textBoxSrcPalette = new System.Windows.Forms.TextBox();
            this.textBox_ZOffset = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "请输入添加物件的Palette名称";
            // 
            // m_textBoxDesPalette
            // 
            this.m_textBoxDesPalette.Location = new System.Drawing.Point(14, 158);
            this.m_textBoxDesPalette.Name = "m_textBoxDesPalette";
            this.m_textBoxDesPalette.ReadOnly = true;
            this.m_textBoxDesPalette.Size = new System.Drawing.Size(165, 21);
            this.m_textBoxDesPalette.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "源Palette名称";
            // 
            // btn_BatchCreate
            // 
            this.btn_BatchCreate.Location = new System.Drawing.Point(114, 198);
            this.btn_BatchCreate.Name = "btn_BatchCreate";
            this.btn_BatchCreate.Size = new System.Drawing.Size(75, 22);
            this.btn_BatchCreate.TabIndex = 4;
            this.btn_BatchCreate.Text = "生成";
            this.btn_BatchCreate.UseVisualStyleBackColor = true;
            this.btn_BatchCreate.Click += new System.EventHandler(this.btn_BatchCreate_Click);
            // 
            // btn_GetDesPalette
            // 
            this.btn_GetDesPalette.Location = new System.Drawing.Point(198, 158);
            this.btn_GetDesPalette.Name = "btn_GetDesPalette";
            this.btn_GetDesPalette.Size = new System.Drawing.Size(109, 24);
            this.btn_GetDesPalette.TabIndex = 5;
            this.btn_GetDesPalette.Text = "获取目标palette";
            this.btn_GetDesPalette.UseVisualStyleBackColor = true;
            this.btn_GetDesPalette.Click += new System.EventHandler(this.btn_GetDesPalette_Click);
            // 
            // m_textBoxSrcPalette
            // 
            this.m_textBoxSrcPalette.Location = new System.Drawing.Point(14, 47);
            this.m_textBoxSrcPalette.Name = "m_textBoxSrcPalette";
            this.m_textBoxSrcPalette.ReadOnly = true;
            this.m_textBoxSrcPalette.Size = new System.Drawing.Size(165, 21);
            this.m_textBoxSrcPalette.TabIndex = 6;
            // 
            // textBox_ZOffset
            // 
            this.textBox_ZOffset.Location = new System.Drawing.Point(14, 131);
            this.textBox_ZOffset.MaxLength = 10;
            this.textBox_ZOffset.Name = "textBox_ZOffset";
            this.textBox_ZOffset.Size = new System.Drawing.Size(48, 21);
            this.textBox_ZOffset.TabIndex = 7;
            this.textBox_ZOffset.Text = "0.0";
            this.textBox_ZOffset.TextChanged += new System.EventHandler(this.textBox_ZOffset_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "Z方向偏移(将与缩放比相乘)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "m";
            // 
            // PaletteBatchTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 232);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_ZOffset);
            this.Controls.Add(this.m_textBoxSrcPalette);
            this.Controls.Add(this.btn_GetDesPalette);
            this.Controls.Add(this.btn_BatchCreate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_textBoxDesPalette);
            this.Controls.Add(this.label1);
            this.Name = "PaletteBatchTool";
            this.Text = "PaletteBatchTool";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_textBoxDesPalette;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_BatchCreate;
        private Framework.MEntity m_pmDesEntity;
        private Framework.MEntity m_pmSrcEntity;
        private float m_fZOffset;
        private System.Windows.Forms.Button btn_GetDesPalette;
        private System.Windows.Forms.TextBox m_textBoxSrcPalette;
        private System.Windows.Forms.TextBox textBox_ZOffset;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;

    }
}