﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class WaterPanel : Form
    {
        public WaterPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // 选择颜色
            ColorDialog colorDlg = new ColorDialog();
            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                m_pictureBoxColor.BackColor = colorDlg.Color;
            }
           
        }

        private void button_CreateRiver_Click(object sender, EventArgs e)
        {
            //生成水体
            float nZPos;              //水体高度
            float fAlhpaDet;

            // 检查用户输入合法性
            try
            {
                nZPos = float.Parse(m_textBoxRiverHeight.Text);
                fAlhpaDet = float.Parse(m_textBoxRiverAlphaDet.Text);
            }
            catch (Exception ee)
            {
                MessageBox.Show("请在 水体高度 和 网格密度 中输入数值。");
                return;
            }

            Color winColor = m_pictureBoxColor.BackColor;
            MPoint3 pColor = new MPoint3(winColor.R / 255.0f, winColor.G / 255.0f, winColor.B / 255.0f);
          
            MFramework.Instance.Scene.InitialRiver(nZPos,pColor,fAlhpaDet);
        }

        private void button_ExportRiver_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.ExportRiverTo3DS();
        }

        private void button_ClearRiver_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.ClearRiver();
        }

    }
}