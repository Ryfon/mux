﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class PostEffectPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private const float m_Divider = 100.0f;

        private float m_fBrightAdjustValue;
        private float m_fContrastAdjustValue;
        private float m_kHSLAdjustValueH;
        private float m_kHSLAdjustValueS;

        private float m_fBloomLumValue;
        private float m_fBloomScaleValue;

        // water scene [10/26/2009 hemeng]
        private float m_fWaterSceneCauseSize;
        private float m_fWaterSceneCauseXOffset;
        private float m_fWaterSceneCauseYOffset;
        private float m_fWaterSceneCauseFactor;
     

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_IsPostEffectEnableCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_textBoxBrightAdjustValue = new System.Windows.Forms.TextBox();
            this.m_textBoxContrastAdjustValue = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_textBoxHLSAdjustHValue = new System.Windows.Forms.TextBox();
            this.m_textBoxHLSAdjustSValue = new System.Windows.Forms.TextBox();
            this.tex1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_IsBloomEffectEnableCheckBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxBloomLumValue = new System.Windows.Forms.TextBox();
            this.m_textBoxBloomScaleValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.m_IsWaterScene = new System.Windows.Forms.CheckBox();
            this.textBox_WaterCauseSize = new System.Windows.Forms.TextBox();
            this.textBox_WaterCauseXOffset = new System.Windows.Forms.TextBox();
            this.textBox_WaterCauseYOffset = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_WaterCauseFactor = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_IsPostEffectEnableCheckBox
            // 
            this.m_IsPostEffectEnableCheckBox.AutoSize = true;
            this.m_IsPostEffectEnableCheckBox.Location = new System.Drawing.Point(23, 121);
            this.m_IsPostEffectEnableCheckBox.Name = "m_IsPostEffectEnableCheckBox";
            this.m_IsPostEffectEnableCheckBox.Size = new System.Drawing.Size(96, 16);
            this.m_IsPostEffectEnableCheckBox.TabIndex = 0;
            this.m_IsPostEffectEnableCheckBox.Text = "开启颜色调整";
            this.m_IsPostEffectEnableCheckBox.UseVisualStyleBackColor = true;
            this.m_IsPostEffectEnableCheckBox.CheckedChanged += new System.EventHandler(this.m_IsPostEffectCheckBox_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "亮度";
            // 
            // m_textBoxBrightAdjustValue
            // 
            this.m_textBoxBrightAdjustValue.Location = new System.Drawing.Point(24, 161);
            this.m_textBoxBrightAdjustValue.MaxLength = 4;
            this.m_textBoxBrightAdjustValue.Name = "m_textBoxBrightAdjustValue";
            this.m_textBoxBrightAdjustValue.Size = new System.Drawing.Size(37, 21);
            this.m_textBoxBrightAdjustValue.TabIndex = 3;
            this.m_textBoxBrightAdjustValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxBrightAdjustValue_KeyDown);
            // 
            // m_textBoxContrastAdjustValue
            // 
            this.m_textBoxContrastAdjustValue.Location = new System.Drawing.Point(23, 258);
            this.m_textBoxContrastAdjustValue.MaxLength = 10;
            this.m_textBoxContrastAdjustValue.Name = "m_textBoxContrastAdjustValue";
            this.m_textBoxContrastAdjustValue.Size = new System.Drawing.Size(37, 21);
            this.m_textBoxContrastAdjustValue.TabIndex = 5;
            this.m_textBoxContrastAdjustValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxContrastAdjustValue_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 267);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "对比度[-6.0,4.0]";
            // 
            // m_textBoxHLSAdjustHValue
            // 
            this.m_textBoxHLSAdjustHValue.Location = new System.Drawing.Point(17, 466);
            this.m_textBoxHLSAdjustHValue.MaxLength = 4;
            this.m_textBoxHLSAdjustHValue.Name = "m_textBoxHLSAdjustHValue";
            this.m_textBoxHLSAdjustHValue.Size = new System.Drawing.Size(37, 21);
            this.m_textBoxHLSAdjustHValue.TabIndex = 9;
            this.m_textBoxHLSAdjustHValue.Visible = false;
            this.m_textBoxHLSAdjustHValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxHLSAdjustHValue_KeyDown);
            // 
            // m_textBoxHLSAdjustSValue
            // 
            this.m_textBoxHLSAdjustSValue.Location = new System.Drawing.Point(24, 212);
            this.m_textBoxHLSAdjustSValue.MaxLength = 4;
            this.m_textBoxHLSAdjustSValue.Name = "m_textBoxHLSAdjustSValue";
            this.m_textBoxHLSAdjustSValue.Size = new System.Drawing.Size(37, 21);
            this.m_textBoxHLSAdjustSValue.TabIndex = 4;
            this.m_textBoxHLSAdjustSValue.TextChanged += new System.EventHandler(this.m_textBoxHLSAdjustSValue_TextChanged);
            this.m_textBoxHLSAdjustSValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxHLSAdjustSValue_KeyDown);
            // 
            // tex1
            // 
            this.tex1.AutoSize = true;
            this.tex1.Location = new System.Drawing.Point(67, 469);
            this.tex1.Name = "tex1";
            this.tex1.Size = new System.Drawing.Size(29, 12);
            this.tex1.TabIndex = 11;
            this.tex1.Text = "色相";
            this.tex1.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(67, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "饱和度";
            // 
            // m_IsBloomEffectEnableCheckBox
            // 
            this.m_IsBloomEffectEnableCheckBox.AutoSize = true;
            this.m_IsBloomEffectEnableCheckBox.Location = new System.Drawing.Point(23, 12);
            this.m_IsBloomEffectEnableCheckBox.Name = "m_IsBloomEffectEnableCheckBox";
            this.m_IsBloomEffectEnableCheckBox.Size = new System.Drawing.Size(78, 16);
            this.m_IsBloomEffectEnableCheckBox.TabIndex = 13;
            this.m_IsBloomEffectEnableCheckBox.Text = "开启bloom";
            this.m_IsBloomEffectEnableCheckBox.UseVisualStyleBackColor = true;
            this.m_IsBloomEffectEnableCheckBox.CheckedChanged += new System.EventHandler(this.m_IsBloomEffectEnableCheckBox_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "Bloom 亮度";
            // 
            // m_textBoxBloomLumValue
            // 
            this.m_textBoxBloomLumValue.Location = new System.Drawing.Point(89, 37);
            this.m_textBoxBloomLumValue.MaxLength = 8;
            this.m_textBoxBloomLumValue.Name = "m_textBoxBloomLumValue";
            this.m_textBoxBloomLumValue.Size = new System.Drawing.Size(54, 21);
            this.m_textBoxBloomLumValue.TabIndex = 15;
            this.m_textBoxBloomLumValue.Text = "0.068";
            this.m_textBoxBloomLumValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxBloomLumValue_KeyDown);
            // 
            // m_textBoxBloomScaleValue
            // 
            this.m_textBoxBloomScaleValue.Location = new System.Drawing.Point(89, 78);
            this.m_textBoxBloomScaleValue.MaxLength = 15;
            this.m_textBoxBloomScaleValue.Name = "m_textBoxBloomScaleValue";
            this.m_textBoxBloomScaleValue.Size = new System.Drawing.Size(54, 21);
            this.m_textBoxBloomScaleValue.TabIndex = 16;
            this.m_textBoxBloomScaleValue.Text = "1.5";
            this.m_textBoxBloomScaleValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxBloomScaleValue_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "Bloom 范围";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(160, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 12);
            this.label6.TabIndex = 18;
            this.label6.Text = "(0-1)随数值下降亮度提高";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(99, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 19;
            this.label7.Text = "[-6.0,4.0]";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(105, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 20;
            this.label8.Text = "[-6.0,4.0]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(102, 469);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 21;
            this.label9.Text = "[-100,100]";
            this.label9.Visible = false;
            // 
            // btn_refresh
            // 
            this.btn_refresh.Location = new System.Drawing.Point(241, 73);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(53, 20);
            this.btn_refresh.TabIndex = 22;
            this.btn_refresh.Text = "刷新";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // m_IsWaterScene
            // 
            this.m_IsWaterScene.AutoSize = true;
            this.m_IsWaterScene.Location = new System.Drawing.Point(24, 296);
            this.m_IsWaterScene.Name = "m_IsWaterScene";
            this.m_IsWaterScene.Size = new System.Drawing.Size(72, 16);
            this.m_IsWaterScene.TabIndex = 23;
            this.m_IsWaterScene.Text = "地表水体";
            this.m_IsWaterScene.UseVisualStyleBackColor = true;
            this.m_IsWaterScene.CheckedChanged += new System.EventHandler(this.m_IsWaterScene_CheckedChanged);
            // 
            // textBox_WaterCauseSize
            // 
            this.textBox_WaterCauseSize.Location = new System.Drawing.Point(24, 318);
            this.textBox_WaterCauseSize.MaxLength = 10;
            this.textBox_WaterCauseSize.Name = "textBox_WaterCauseSize";
            this.textBox_WaterCauseSize.Size = new System.Drawing.Size(48, 21);
            this.textBox_WaterCauseSize.TabIndex = 24;
            this.textBox_WaterCauseSize.TextChanged += new System.EventHandler(this.textBox_WaterCauseSize_TextChanged);
            this.textBox_WaterCauseSize.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_WaterCauseSize_KeyDown);
            // 
            // textBox_WaterCauseXOffset
            // 
            this.textBox_WaterCauseXOffset.Location = new System.Drawing.Point(23, 355);
            this.textBox_WaterCauseXOffset.MaxLength = 10;
            this.textBox_WaterCauseXOffset.Name = "textBox_WaterCauseXOffset";
            this.textBox_WaterCauseXOffset.Size = new System.Drawing.Size(49, 21);
            this.textBox_WaterCauseXOffset.TabIndex = 25;
            this.textBox_WaterCauseXOffset.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_WaterCauseXOffset_KeyDown);
            // 
            // textBox_WaterCauseYOffset
            // 
            this.textBox_WaterCauseYOffset.Location = new System.Drawing.Point(23, 395);
            this.textBox_WaterCauseYOffset.MaxLength = 10;
            this.textBox_WaterCauseYOffset.Name = "textBox_WaterCauseYOffset";
            this.textBox_WaterCauseYOffset.Size = new System.Drawing.Size(49, 21);
            this.textBox_WaterCauseYOffset.TabIndex = 26;
            this.textBox_WaterCauseYOffset.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_WaterCauseYOffset_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(87, 321);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 27;
            this.label10.Text = "水波大小";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(84, 364);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 12);
            this.label11.TabIndex = 28;
            this.label11.Text = "水波X位移";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(84, 404);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 12);
            this.label12.TabIndex = 29;
            this.label12.Text = "水波Y位移";
            // 
            // textBox_WaterCauseFactor
            // 
            this.textBox_WaterCauseFactor.Location = new System.Drawing.Point(24, 432);
            this.textBox_WaterCauseFactor.Name = "textBox_WaterCauseFactor";
            this.textBox_WaterCauseFactor.Size = new System.Drawing.Size(48, 21);
            this.textBox_WaterCauseFactor.TabIndex = 30;
            this.textBox_WaterCauseFactor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_WaterCauseFactor_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(87, 441);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 31;
            this.label13.Text = "水波亮度";
            // 
            // PostEffectPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 499);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBox_WaterCauseFactor);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox_WaterCauseYOffset);
            this.Controls.Add(this.textBox_WaterCauseXOffset);
            this.Controls.Add(this.textBox_WaterCauseSize);
            this.Controls.Add(this.m_IsWaterScene);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_textBoxBloomScaleValue);
            this.Controls.Add(this.m_textBoxBloomLumValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_IsBloomEffectEnableCheckBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tex1);
            this.Controls.Add(this.m_textBoxHLSAdjustSValue);
            this.Controls.Add(this.m_textBoxHLSAdjustHValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_textBoxContrastAdjustValue);
            this.Controls.Add(this.m_textBoxBrightAdjustValue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_IsPostEffectEnableCheckBox);
            this.Name = "PostEffectPanel";
            this.Text = "1.0";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox m_IsPostEffectEnableCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_textBoxBrightAdjustValue;
        private System.Windows.Forms.TextBox m_textBoxContrastAdjustValue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_textBoxHLSAdjustHValue;
        private System.Windows.Forms.TextBox m_textBoxHLSAdjustSValue;
        private System.Windows.Forms.Label tex1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox m_IsBloomEffectEnableCheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxBloomLumValue;
        private System.Windows.Forms.TextBox m_textBoxBloomScaleValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.CheckBox m_IsWaterScene;
        private System.Windows.Forms.TextBox textBox_WaterCauseSize;
        private System.Windows.Forms.TextBox textBox_WaterCauseXOffset;
        private System.Windows.Forms.TextBox textBox_WaterCauseYOffset;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_WaterCauseFactor;
        private System.Windows.Forms.Label label13;
    }
}