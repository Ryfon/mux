﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class CityLevelEditPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_btnPreview = new System.Windows.Forms.Button();
            this.m_cbCityLevels = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_tbTex3 = new System.Windows.Forms.TextBox();
            this.m_btnTex3 = new System.Windows.Forms.Button();
            this.m_tbTex2 = new System.Windows.Forms.TextBox();
            this.m_btnTex2 = new System.Windows.Forms.Button();
            this.m_tbTex1 = new System.Windows.Forms.TextBox();
            this.m_btnTex1 = new System.Windows.Forms.Button();
            this.m_tbTex0 = new System.Windows.Forms.TextBox();
            this.m_btnTex0 = new System.Windows.Forms.Button();
            this.m_btnSave = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.m_btnPreview);
            this.groupBox1.Controls.Add(this.m_cbCityLevels);
            this.groupBox1.Location = new System.Drawing.Point(6, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(337, 61);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "城市等级选择";
            // 
            // m_btnPreview
            // 
            this.m_btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnPreview.Location = new System.Drawing.Point(240, 20);
            this.m_btnPreview.Name = "m_btnPreview";
            this.m_btnPreview.Size = new System.Drawing.Size(75, 23);
            this.m_btnPreview.TabIndex = 1;
            this.m_btnPreview.Text = "预览";
            this.m_btnPreview.UseVisualStyleBackColor = true;
            this.m_btnPreview.Click += new System.EventHandler(this.m_btnPreview_Click);
            // 
            // m_cbCityLevels
            // 
            this.m_cbCityLevels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_cbCityLevels.FormattingEnabled = true;
            this.m_cbCityLevels.Location = new System.Drawing.Point(6, 20);
            this.m_cbCityLevels.Name = "m_cbCityLevels";
            this.m_cbCityLevels.Size = new System.Drawing.Size(211, 20);
            this.m_cbCityLevels.TabIndex = 0;
            this.m_cbCityLevels.SelectedIndexChanged += new System.EventHandler(this.m_cbCityLevels_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.m_tbTex3);
            this.groupBox2.Controls.Add(this.m_btnTex3);
            this.groupBox2.Controls.Add(this.m_tbTex2);
            this.groupBox2.Controls.Add(this.m_btnTex2);
            this.groupBox2.Controls.Add(this.m_tbTex1);
            this.groupBox2.Controls.Add(this.m_btnTex1);
            this.groupBox2.Controls.Add(this.m_tbTex0);
            this.groupBox2.Controls.Add(this.m_btnTex0);
            this.groupBox2.Location = new System.Drawing.Point(6, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(337, 131);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "地形纹理";
            // 
            // m_tbTex3
            // 
            this.m_tbTex3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tbTex3.Location = new System.Drawing.Point(55, 102);
            this.m_tbTex3.Name = "m_tbTex3";
            this.m_tbTex3.Size = new System.Drawing.Size(274, 21);
            this.m_tbTex3.TabIndex = 7;
            this.m_tbTex3.TextChanged += new System.EventHandler(this.m_tbTex3_TextChanged);
            // 
            // m_btnTex3
            // 
            this.m_btnTex3.Location = new System.Drawing.Point(12, 101);
            this.m_btnTex3.Name = "m_btnTex3";
            this.m_btnTex3.Size = new System.Drawing.Size(37, 21);
            this.m_btnTex3.TabIndex = 6;
            this.m_btnTex3.Text = "Tex3";
            this.m_btnTex3.UseVisualStyleBackColor = true;
            this.m_btnTex3.Click += new System.EventHandler(this.m_btnTex_Click);
            // 
            // m_tbTex2
            // 
            this.m_tbTex2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tbTex2.Location = new System.Drawing.Point(55, 75);
            this.m_tbTex2.Name = "m_tbTex2";
            this.m_tbTex2.Size = new System.Drawing.Size(274, 21);
            this.m_tbTex2.TabIndex = 5;
            this.m_tbTex2.TextChanged += new System.EventHandler(this.m_tbTex2_TextChanged);
            // 
            // m_btnTex2
            // 
            this.m_btnTex2.Location = new System.Drawing.Point(12, 74);
            this.m_btnTex2.Name = "m_btnTex2";
            this.m_btnTex2.Size = new System.Drawing.Size(37, 21);
            this.m_btnTex2.TabIndex = 4;
            this.m_btnTex2.Text = "Tex2";
            this.m_btnTex2.UseVisualStyleBackColor = true;
            this.m_btnTex2.Click += new System.EventHandler(this.m_btnTex_Click);
            // 
            // m_tbTex1
            // 
            this.m_tbTex1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tbTex1.Location = new System.Drawing.Point(55, 48);
            this.m_tbTex1.Name = "m_tbTex1";
            this.m_tbTex1.Size = new System.Drawing.Size(274, 21);
            this.m_tbTex1.TabIndex = 3;
            this.m_tbTex1.TextChanged += new System.EventHandler(this.m_tbTex1_TextChanged);
            // 
            // m_btnTex1
            // 
            this.m_btnTex1.Location = new System.Drawing.Point(12, 47);
            this.m_btnTex1.Name = "m_btnTex1";
            this.m_btnTex1.Size = new System.Drawing.Size(37, 21);
            this.m_btnTex1.TabIndex = 2;
            this.m_btnTex1.Text = "Tex1";
            this.m_btnTex1.UseVisualStyleBackColor = true;
            this.m_btnTex1.Click += new System.EventHandler(this.m_btnTex_Click);
            // 
            // m_tbTex0
            // 
            this.m_tbTex0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_tbTex0.Location = new System.Drawing.Point(55, 21);
            this.m_tbTex0.Name = "m_tbTex0";
            this.m_tbTex0.Size = new System.Drawing.Size(274, 21);
            this.m_tbTex0.TabIndex = 1;
            this.m_tbTex0.TextChanged += new System.EventHandler(this.m_tbTex0_TextChanged);
            // 
            // m_btnTex0
            // 
            this.m_btnTex0.Location = new System.Drawing.Point(12, 20);
            this.m_btnTex0.Name = "m_btnTex0";
            this.m_btnTex0.Size = new System.Drawing.Size(37, 21);
            this.m_btnTex0.TabIndex = 0;
            this.m_btnTex0.Text = "Tex0";
            this.m_btnTex0.UseVisualStyleBackColor = true;
            this.m_btnTex0.Click += new System.EventHandler(this.m_btnTex_Click);
            // 
            // m_btnSave
            // 
            this.m_btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnSave.Location = new System.Drawing.Point(268, 212);
            this.m_btnSave.Name = "m_btnSave";
            this.m_btnSave.Size = new System.Drawing.Size(75, 23);
            this.m_btnSave.TabIndex = 2;
            this.m_btnSave.Text = "保存";
            this.m_btnSave.UseVisualStyleBackColor = true;
            this.m_btnSave.Click += new System.EventHandler(this.m_btnSave_Click);
            // 
            // CityLevelEditPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 240);
            this.Controls.Add(this.m_btnSave);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CityLevelEditPanel";
            this.Text = "城市等级编辑";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.CityLevelEditPanel_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button m_btnPreview;
        private System.Windows.Forms.ComboBox m_cbCityLevels;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox m_tbTex0;
        private System.Windows.Forms.Button m_btnTex0;
        private System.Windows.Forms.TextBox m_tbTex3;
        private System.Windows.Forms.Button m_btnTex3;
        private System.Windows.Forms.TextBox m_tbTex2;
        private System.Windows.Forms.Button m_btnTex2;
        private System.Windows.Forms.TextBox m_tbTex1;
        private System.Windows.Forms.Button m_btnTex1;
        private System.Windows.Forms.Button m_btnSave;
    }
}