﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Emergent.Gamebryo.SceneDesigner.Framework;
namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    public partial class EntityExportSettingPanel : Form
    {
        public EntityExportSettingPanel()
        {
            InitializeComponent();
        }

        private void EntityExportSettingPanel_Load(object sender, EventArgs e)
        {
            InitUI();
        }

        // 初始化界面
        private void InitUI()
        {
            FillEntities();
            FillSections();
            FillExportSettings();

        }

        private void FillEntities()
        {
            m_listBoxSceneEntities.Items.Clear();

            MEntity[] arrEntities = MFramework.Instance.Scene.GetEntities();

            foreach (MEntity entity in arrEntities)
            {
                if (entity.Name != null)
                {
                    m_listBoxSceneEntities.Items.Add(entity.Name);
                }
            }
        }

        private void FillSections()
        {
            m_listBoxSections.Items.Clear();
            ArrayList arrSections = MEntityExportManager.Instance.GetAllSections();

            foreach (String strSection in arrSections)
            {
                m_listBoxSections.Items.Add(strSection);
            }
        }

        private void FillExportSettings()
        {
            m_dgvExportSetting.Rows.Clear();

            ArrayList arrExportSettings 
                = MEntityExportManager.Instance.EntityExportSettingList;
            if (arrExportSettings.Count == 0) return;

            m_dgvExportSetting.Rows.Add(arrExportSettings.Count);
            for (int i = 0; i < arrExportSettings.Count; i++)
            {
                MEntityExportSetting setting 
                    = arrExportSettings[i] as MEntityExportSetting;

                if (setting != null)
                {
                    m_dgvExportSetting.Rows[i].Tag = setting;
                    m_dgvExportSetting[0, i].Value = setting.m_strName;
                    m_dgvExportSetting[1, i].Value = setting.m_strSection;
                    m_dgvExportSetting[2, i].Value = setting.m_iExtraID.ToString();
                }
            }

        }

        private void m_btnSave_Click(object sender, EventArgs e)
        {
            // 确定当前有场景被打开
            String strSceneName = MFramework.Instance.CurrentFilename;
            if (strSceneName == null || strSceneName.Length == 0) return;

            // 保存到 EntityExportManager 中
            MEntityExportManager.Instance.Clear();
            foreach (DataGridViewRow row in m_dgvExportSetting.Rows)
            {
                try
                {
                    String strName = "";
                    if (row.Cells[0].Value != null) { strName = row.Cells[0].Value as String; }
                    String strSection = "";
                    if (row.Cells[1].Value != null) {strSection = row.Cells[1].Value as String;}

                    int iExtraID = -1;
                    try
                    {
                        iExtraID = int.Parse(row.Cells[2].Value as String);
                    }
                    catch(Exception){}

                    if (iExtraID == -1) continue;

                    MEntityExportSetting setting = new MEntityExportSetting();
                    setting.m_strName = strName;
                    setting.m_strSection = strSection;
                    setting.m_iExtraID = iExtraID;

                    MEntityExportManager.Instance.AddEntityExportSetting(setting);
                }
                catch(Exception e2)
                {
                    MessageBox.Show(e2.ToString());
                }
            }

            // 保存到 xml
            int iLastDot = strSceneName.LastIndexOf('.');
            String strDirectory = strSceneName.Substring(0, iLastDot);

            MEntityExportManager.Instance.SaveToXml(strDirectory + "\\");
            // 刷新 ui

            FillExportSettings();
            FillSections();
        }

        private void m_listBoxSceneEntities_DoubleClick(object sender, EventArgs e)
        {
            if (m_dgvExportSetting.SelectedRows.Count > 0 
                    && m_listBoxSceneEntities.SelectedItem != null)
            {
                DataGridViewRow row = m_dgvExportSetting.SelectedRows[0];
                String strName = m_listBoxSceneEntities.SelectedItem.ToString();
                row.Cells[0].Value = strName;
            }
        }

        private void m_listBoxSections_DoubleClick(object sender, EventArgs e)
        {
            if (m_dgvExportSetting.SelectedRows.Count > 0
                    && m_listBoxSections.SelectedItem != null)
            {
                DataGridViewRow row = m_dgvExportSetting.SelectedRows[0];
                String strSection = m_listBoxSections.SelectedItem.ToString();
                row.Cells[1].Value = strSection;
            }
        }

        private void m_dgvExportSetting_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
        }
    }
}