﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class ChunkEntityManagePanel : Form
    {
        private ChunkEntityManagePanel()
        {
            InitializeComponent();
        }

        public static ChunkEntityManagePanel Instance
        {
            get
            {
                if (m_Instance == null || m_Instance.IsDisposed)
                {
                    m_Instance = new ChunkEntityManagePanel();
                }
                return m_Instance;
            }
        }

        private void ChunkEntityManagePanel_Load(object sender, EventArgs e)
        {
            //  从 MChunkEntityManager 中获取 entity cull mode
            m_radioButtonCameraChunk.Checked = false;
            m_radioButton9CameraChunk.Checked = false;
            m_radioButtonStaticCulled.Checked = false;

            MChunkEntityManager.EntityCullMode mode = MChunkEntityManager.Instance.GetEntityCullMode();
            if (mode == MChunkEntityManager.EntityCullMode.ECM_STATIC)
            {
                m_radioButtonStaticCulled.Checked = true; 
                m_listBoxSelectedChunks.Items.Clear();
                ArrayList chunkList = MChunkEntityManager.Instance.StaticChunkDisplayList;
                foreach (Object obj in chunkList)
                {
                    m_listBoxSelectedChunks.Items.Add(obj.ToString());
                }

            }
            else if (mode == MChunkEntityManager.EntityCullMode.ECM_CAMERA_AROUND)
            {
                m_radioButton9CameraChunk.Checked = true;
            }
            else if (mode == MChunkEntityManager.EntityCullMode.ECM_CAMERA_CHUNK)
            {
                m_radioButtonCameraChunk.Checked = true;
            }
            else if (mode == MChunkEntityManager.EntityCullMode.ECM_CULL_NONE)
            {
                m_radioButtonCullNone.Checked = true;
            }

            int iNumChunks = MFramework.Instance.Scene.GetTerrainNumChunkX() * MFramework.Instance.Scene.GetTerrainNumChunkY();
            m_listBoxAllChunks.Items.Clear();
            for (int i=0; i<iNumChunks; i++)
            {
                m_listBoxAllChunks.Items.Add(i.ToString());
            }
            
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            if (m_listBoxAllChunks.SelectedIndex == -1
                || m_listBoxSelectedChunks.Items.Contains(m_listBoxAllChunks.SelectedItem))
            {
                return;
            }

            m_listBoxSelectedChunks.Items.Add(m_listBoxAllChunks.SelectedItem);
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            if (m_listBoxSelectedChunks.SelectedIndex == -1)
            {
                return;
            }

            m_listBoxSelectedChunks.Items.Remove(m_listBoxSelectedChunks.SelectedItem);
        }

        private void m_btnClearAllChunks_Click(object sender, EventArgs e)
        {
            m_listBoxSelectedChunks.Items.Clear();
        }

        private void m_btnOk_Click(object sender, EventArgs e)
        {
            ArrayList selectedChunks = new ArrayList();
            foreach (Object obj in m_listBoxSelectedChunks.Items)
            {
                selectedChunks.Add(obj);
            }
            MChunkEntityManager.Instance.StaticChunkDisplayList = selectedChunks;

            if (m_radioButtonStaticCulled.Checked)
            {
                MChunkEntityManager.Instance.SetEntityCullMode(MChunkEntityManager.EntityCullMode.ECM_STATIC);
            }
            else if (m_radioButtonCameraChunk.Checked)
            {
                MChunkEntityManager.Instance.SetEntityCullMode(MChunkEntityManager.EntityCullMode.ECM_CAMERA_CHUNK);
            }
            else if (m_radioButton9CameraChunk.Checked)
            {
                MChunkEntityManager.Instance.SetEntityCullMode(MChunkEntityManager.EntityCullMode.ECM_CAMERA_AROUND);
            }
            else if (m_radioButtonCullNone.Checked)
            {
                MChunkEntityManager.Instance.SetEntityCullMode(MChunkEntityManager.EntityCullMode.ECM_CULL_NONE);
            }

            this.Close();
        }
    }
}