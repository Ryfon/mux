﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class FogPanel : Form
    {
        public FogPanel()
        {
            InitializeComponent();
        }

        private void m_btnSelectColor_Click(object sender, EventArgs e)
        {
            // 选择颜色
            ColorDialog colorDlg = new ColorDialog();
            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                m_pictureBoxColor.BackColor = colorDlg.Color;
            }
        }

        private void m_btnGenerateFog_Click(object sender, EventArgs e)
        {
            // 生成层雾

            float fStartZ, fEndZ, fLeft, fRight, fTop, fBottom;
            int iTexSize = 512;
            float fSpeedU;

            // 检查用户输入合法性
            try
            {
                fStartZ = float.Parse(m_textBoxStartZ.Text);
                fEndZ = float.Parse(m_textBoxEndZ.Text);
            }
            catch(Exception ee)
            {
                MessageBox.Show("请在 开始位置 和 结束位置 中输入数值。");
                return;
            }

            try
            {
                fLeft = float.Parse(m_textBoxLeft.Text);
                fRight = float.Parse(m_textBoxRight.Text);
                fTop = float.Parse(m_textBoxTop.Text);
                fBottom = float.Parse(m_textBoxBottom.Text);
            }
            catch (Exception ee)
            {
                MessageBox.Show("请在 体雾范围 中输入数值。");
                return;
            }

            try
            {
                fSpeedU = float.Parse(m_textBoxSpeedU.Text);
            }
            catch (Exception ee)
            {
                MessageBox.Show("请在 UV 动画速度 中输入数值。");
                return;
            }

            if (m_btnTexSize64.Checked)
            {
                iTexSize = 64;
            }
            else if (m_btnTexSize128.Checked)
            {
                iTexSize = 128;
            }
            else if (m_btnTexSize256.Checked)
            {
                iTexSize = 256;
            }
            else if (m_btnTexSize512.Checked)
            {
                iTexSize = 512;
            }
            else if (m_btnTexSize1024.Checked)
            {
                iTexSize = 1024;
            }
            else if (m_btnTexSize2048.Checked)
            {
                iTexSize = 2048;
            }

            Color winColor = m_pictureBoxColor.BackColor;
            MPoint3 pColor = new MPoint3(winColor.R/255.0f, winColor.G/255.0f, winColor.B/255.0f);

            MFramework.Instance.Scene.CreateSimpleFog(fStartZ, fEndZ, fLeft, fRight, fTop, fBottom, 
                                            pColor, iTexSize, fSpeedU, 0, m_textBoxAnimTexFile.Text);

            MFramework.Instance.Scene.SetPreviewSimpleFog(m_checkBoxPreview.Checked);
        }

        private void m_checkBoxPreview_CheckedChanged(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetPreviewSimpleFog(m_checkBoxPreview.Checked);
        }

        private void m_btnSelectFogTextureFile_Click(object sender, EventArgs e)
        {
            // 选择纹理保存文件
            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Filter = "dds files (*.dds)|*.dds|All files (*.*)|*.*";
            saveFileDlg.FilterIndex = 1;
            saveFileDlg.RestoreDirectory = true;

            if (saveFileDlg.ShowDialog() == DialogResult.OK)
            {
                m_textBoxTexFile.Text = saveFileDlg.FileName;
            }
        }

        private void m_btnSelectFogSurfaceFile_Click(object sender, EventArgs e)
        {
            // 选择雾表面保存文件
            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.Filter = "nif files (*.nif)|*.nif|All files (*.*)|*.*";
            saveFileDlg.FilterIndex = 1;
            saveFileDlg.RestoreDirectory = true;

            if (saveFileDlg.ShowDialog() == DialogResult.OK)
            {
                m_textBoxSurfaceFile.Text = saveFileDlg.FileName;
            }
        }

        private void m_btnSave_Click(object sender, EventArgs e)
        {
            if (!MFramework.Instance.Scene.SaveLayerFog(m_textBoxTexFile.Text, m_textBoxSurfaceFile.Text))
            {
               //
            }
        }

        private void m_btnClear_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.ClearSimpleFog();
        }

        private void m_btnSelectAnimTexture_Click(object sender, EventArgs e)
        {
            // 选择纹理保存文件
            OpenFileDialog openFileDlg = new OpenFileDialog();
            openFileDlg.Filter = "dds files (*.dds)|*.dds|All files (*.*)|*.*";
            openFileDlg.FilterIndex = 1;
            openFileDlg.RestoreDirectory = true;

            if (openFileDlg.ShowDialog() == DialogResult.OK)
            {
                m_textBoxAnimTexFile.Text = openFileDlg.FileName;
            }
        }
    }
}