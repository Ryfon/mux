﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class TextureCombinationPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnSelectTexturePath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_textBoxTexturePath = new System.Windows.Forms.TextBox();
            this.m_comboBoxSizeFilter = new System.Windows.Forms.ComboBox();
            this.m_listBoxAllTexture = new System.Windows.Forms.ListBox();
            this.m_listBoxBombineTexture = new System.Windows.Forms.ListBox();
            this.m_btnAddToCombineList = new System.Windows.Forms.Button();
            this.m_btnRemoveFromCombineList = new System.Windows.Forms.Button();
            this.m_btnClearAll = new System.Windows.Forms.Button();
            this.m_btnCombine = new System.Windows.Forms.Button();
            this.m_listBoxSelectAffectModel = new System.Windows.Forms.ListBox();
            this.m_textBoxModelPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_btnSelectModelPath = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.m_listBoxAllAffectModel = new System.Windows.Forms.ListBox();
            this.m_textBoxTargetTexture = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_btnSelectTargetTexture = new System.Windows.Forms.Button();
            this.m_checkBoxBackUpOldModel = new System.Windows.Forms.CheckBox();
            this.m_btnPreview = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_btnSelectTexturePath
            // 
            this.m_btnSelectTexturePath.Location = new System.Drawing.Point(68, 11);
            this.m_btnSelectTexturePath.Name = "m_btnSelectTexturePath";
            this.m_btnSelectTexturePath.Size = new System.Drawing.Size(33, 20);
            this.m_btnSelectTexturePath.TabIndex = 0;
            this.m_btnSelectTexturePath.Text = "...";
            this.m_btnSelectTexturePath.UseVisualStyleBackColor = true;
            this.m_btnSelectTexturePath.Click += new System.EventHandler(this.m_btnSelectTexturePath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "纹理目录";
            // 
            // m_textBoxTexturePath
            // 
            this.m_textBoxTexturePath.Location = new System.Drawing.Point(107, 12);
            this.m_textBoxTexturePath.Name = "m_textBoxTexturePath";
            this.m_textBoxTexturePath.Size = new System.Drawing.Size(204, 21);
            this.m_textBoxTexturePath.TabIndex = 2;
            // 
            // m_comboBoxSizeFilter
            // 
            this.m_comboBoxSizeFilter.FormattingEnabled = true;
            this.m_comboBoxSizeFilter.Location = new System.Drawing.Point(10, 60);
            this.m_comboBoxSizeFilter.Name = "m_comboBoxSizeFilter";
            this.m_comboBoxSizeFilter.Size = new System.Drawing.Size(147, 20);
            this.m_comboBoxSizeFilter.TabIndex = 3;
            // 
            // m_listBoxAllTexture
            // 
            this.m_listBoxAllTexture.FormattingEnabled = true;
            this.m_listBoxAllTexture.ItemHeight = 12;
            this.m_listBoxAllTexture.Location = new System.Drawing.Point(10, 95);
            this.m_listBoxAllTexture.Name = "m_listBoxAllTexture";
            this.m_listBoxAllTexture.Size = new System.Drawing.Size(147, 136);
            this.m_listBoxAllTexture.TabIndex = 4;
            // 
            // m_listBoxBombineTexture
            // 
            this.m_listBoxBombineTexture.FormattingEnabled = true;
            this.m_listBoxBombineTexture.ItemHeight = 12;
            this.m_listBoxBombineTexture.Location = new System.Drawing.Point(216, 71);
            this.m_listBoxBombineTexture.Name = "m_listBoxBombineTexture";
            this.m_listBoxBombineTexture.Size = new System.Drawing.Size(147, 160);
            this.m_listBoxBombineTexture.TabIndex = 5;
            // 
            // m_btnAddToCombineList
            // 
            this.m_btnAddToCombineList.Location = new System.Drawing.Point(163, 121);
            this.m_btnAddToCombineList.Name = "m_btnAddToCombineList";
            this.m_btnAddToCombineList.Size = new System.Drawing.Size(47, 22);
            this.m_btnAddToCombineList.TabIndex = 6;
            this.m_btnAddToCombineList.Text = "->";
            this.m_btnAddToCombineList.UseVisualStyleBackColor = true;
            // 
            // m_btnRemoveFromCombineList
            // 
            this.m_btnRemoveFromCombineList.Location = new System.Drawing.Point(163, 149);
            this.m_btnRemoveFromCombineList.Name = "m_btnRemoveFromCombineList";
            this.m_btnRemoveFromCombineList.Size = new System.Drawing.Size(47, 22);
            this.m_btnRemoveFromCombineList.TabIndex = 7;
            this.m_btnRemoveFromCombineList.Text = "<-";
            this.m_btnRemoveFromCombineList.UseVisualStyleBackColor = true;
            // 
            // m_btnClearAll
            // 
            this.m_btnClearAll.Location = new System.Drawing.Point(369, 208);
            this.m_btnClearAll.Name = "m_btnClearAll";
            this.m_btnClearAll.Size = new System.Drawing.Size(47, 23);
            this.m_btnClearAll.TabIndex = 8;
            this.m_btnClearAll.Text = "清除";
            this.m_btnClearAll.UseVisualStyleBackColor = true;
            // 
            // m_btnCombine
            // 
            this.m_btnCombine.Location = new System.Drawing.Point(163, 382);
            this.m_btnCombine.Name = "m_btnCombine";
            this.m_btnCombine.Size = new System.Drawing.Size(78, 24);
            this.m_btnCombine.TabIndex = 9;
            this.m_btnCombine.Text = "合并纹理";
            this.m_btnCombine.UseVisualStyleBackColor = true;
            // 
            // m_listBoxSelectAffectModel
            // 
            this.m_listBoxSelectAffectModel.FormattingEnabled = true;
            this.m_listBoxSelectAffectModel.ItemHeight = 12;
            this.m_listBoxSelectAffectModel.Location = new System.Drawing.Point(11, 257);
            this.m_listBoxSelectAffectModel.Name = "m_listBoxSelectAffectModel";
            this.m_listBoxSelectAffectModel.Size = new System.Drawing.Size(146, 64);
            this.m_listBoxSelectAffectModel.TabIndex = 10;
            // 
            // m_textBoxModelPath
            // 
            this.m_textBoxModelPath.Location = new System.Drawing.Point(107, 33);
            this.m_textBoxModelPath.Name = "m_textBoxModelPath";
            this.m_textBoxModelPath.Size = new System.Drawing.Size(204, 21);
            this.m_textBoxModelPath.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "模型目录";
            // 
            // m_btnSelectModelPath
            // 
            this.m_btnSelectModelPath.Location = new System.Drawing.Point(68, 32);
            this.m_btnSelectModelPath.Name = "m_btnSelectModelPath";
            this.m_btnSelectModelPath.Size = new System.Drawing.Size(33, 20);
            this.m_btnSelectModelPath.TabIndex = 11;
            this.m_btnSelectModelPath.Text = "...";
            this.m_btnSelectModelPath.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "相关模型";
            // 
            // m_listBoxAllAffectModel
            // 
            this.m_listBoxAllAffectModel.FormattingEnabled = true;
            this.m_listBoxAllAffectModel.ItemHeight = 12;
            this.m_listBoxAllAffectModel.Location = new System.Drawing.Point(217, 257);
            this.m_listBoxAllAffectModel.Name = "m_listBoxAllAffectModel";
            this.m_listBoxAllAffectModel.Size = new System.Drawing.Size(146, 64);
            this.m_listBoxAllAffectModel.TabIndex = 15;
            // 
            // m_textBoxTargetTexture
            // 
            this.m_textBoxTargetTexture.Location = new System.Drawing.Point(107, 354);
            this.m_textBoxTargetTexture.Name = "m_textBoxTargetTexture";
            this.m_textBoxTargetTexture.Size = new System.Drawing.Size(204, 21);
            this.m_textBoxTargetTexture.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 358);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 17;
            this.label4.Text = "目标纹理";
            // 
            // m_btnSelectTargetTexture
            // 
            this.m_btnSelectTargetTexture.Location = new System.Drawing.Point(68, 354);
            this.m_btnSelectTargetTexture.Name = "m_btnSelectTargetTexture";
            this.m_btnSelectTargetTexture.Size = new System.Drawing.Size(33, 20);
            this.m_btnSelectTargetTexture.TabIndex = 16;
            this.m_btnSelectTargetTexture.Text = "...";
            this.m_btnSelectTargetTexture.UseVisualStyleBackColor = true;
            // 
            // m_checkBoxBackUpOldModel
            // 
            this.m_checkBoxBackUpOldModel.AutoSize = true;
            this.m_checkBoxBackUpOldModel.Checked = true;
            this.m_checkBoxBackUpOldModel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxBackUpOldModel.Location = new System.Drawing.Point(217, 333);
            this.m_checkBoxBackUpOldModel.Name = "m_checkBoxBackUpOldModel";
            this.m_checkBoxBackUpOldModel.Size = new System.Drawing.Size(108, 16);
            this.m_checkBoxBackUpOldModel.TabIndex = 19;
            this.m_checkBoxBackUpOldModel.Text = "备份原模型文件";
            this.m_checkBoxBackUpOldModel.UseVisualStyleBackColor = true;
            // 
            // m_btnPreview
            // 
            this.m_btnPreview.Location = new System.Drawing.Point(50, 382);
            this.m_btnPreview.Name = "m_btnPreview";
            this.m_btnPreview.Size = new System.Drawing.Size(78, 24);
            this.m_btnPreview.TabIndex = 20;
            this.m_btnPreview.Text = "预览";
            this.m_btnPreview.UseVisualStyleBackColor = true;
            // 
            // TextureCombinationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 418);
            this.Controls.Add(this.m_btnPreview);
            this.Controls.Add(this.m_checkBoxBackUpOldModel);
            this.Controls.Add(this.m_textBoxTargetTexture);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_btnSelectTargetTexture);
            this.Controls.Add(this.m_listBoxAllAffectModel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_textBoxModelPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_btnSelectModelPath);
            this.Controls.Add(this.m_listBoxSelectAffectModel);
            this.Controls.Add(this.m_btnCombine);
            this.Controls.Add(this.m_btnClearAll);
            this.Controls.Add(this.m_btnRemoveFromCombineList);
            this.Controls.Add(this.m_btnAddToCombineList);
            this.Controls.Add(this.m_listBoxBombineTexture);
            this.Controls.Add(this.m_listBoxAllTexture);
            this.Controls.Add(this.m_comboBoxSizeFilter);
            this.Controls.Add(this.m_textBoxTexturePath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_btnSelectTexturePath);
            this.Name = "TextureCombinationPanel";
            this.Text = "TextureCombinationPanel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnSelectTexturePath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_textBoxTexturePath;
        private System.Windows.Forms.ComboBox m_comboBoxSizeFilter;
        private System.Windows.Forms.ListBox m_listBoxAllTexture;
        private System.Windows.Forms.ListBox m_listBoxBombineTexture;
        private System.Windows.Forms.Button m_btnAddToCombineList;
        private System.Windows.Forms.Button m_btnRemoveFromCombineList;
        private System.Windows.Forms.Button m_btnClearAll;
        private System.Windows.Forms.Button m_btnCombine;
        private System.Windows.Forms.ListBox m_listBoxSelectAffectModel;
        private System.Windows.Forms.TextBox m_textBoxModelPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button m_btnSelectModelPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox m_listBoxAllAffectModel;
        private System.Windows.Forms.TextBox m_textBoxTargetTexture;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button m_btnSelectTargetTexture;
        private System.Windows.Forms.CheckBox m_checkBoxBackUpOldModel;
        private System.Windows.Forms.Button m_btnPreview;
    }
}