﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class CityLevelEditPanel : Form
    {
        // 当前 level 对应的设置
        private MCityLevelManager.MCityLevelSetting m_CurrLevelSetting = null;

        public CityLevelEditPanel()
        {
            InitializeComponent();

            // 4个按纽共用同一个响应函数,用于分辨按纽
            m_btnTex0.Tag = 0;
            m_btnTex1.Tag = 1;
            m_btnTex2.Tag = 2;
            m_btnTex3.Tag = 3;

            int iNumCityLevels = MCityLevelManager.Instance.GetNumCityLevels();
            // 填充城市等级选择 ComboBox
            m_cbCityLevels.Items.Clear();
            for (int i=0; i<iNumCityLevels; i++)
            {
                m_cbCityLevels.Items.Add("Level_" + i.ToString());
            }
            m_cbCityLevels.SelectedIndex = 0;
        }

        void RefreshUI()
        {
            String[] arrTexs = m_CurrLevelSetting.m_arrTerrainTextures;
            m_tbTex0.Text = arrTexs[0] == null ? "" : arrTexs[0];
            m_tbTex1.Text = arrTexs[1] == null ? "" : arrTexs[1];
            m_tbTex2.Text = arrTexs[2] == null ? "" : arrTexs[2];
            m_tbTex3.Text = arrTexs[3] == null ? "" : arrTexs[3];
        }

        private void CityLevelEditPanel_Load(object sender, EventArgs e)
        {

        }

        private void m_btnTex_Click(object sender, EventArgs e)
        {
            if (m_CurrLevelSetting == null) return;

            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);

            int iWhich = (int)((sender as Button).Tag);

            System.IO.Directory.SetCurrentDirectory(Application.StartupPath);
            SelectMaterialDlg m_picselectdlg = new SelectMaterialDlg();
            m_picselectdlg.ShowDialog();
            string selectedMaterial = m_picselectdlg.get_CurrentSelectedPic();

            String[] arrTex = m_CurrLevelSetting.m_arrTerrainTextures;

            if (iWhich>=0 && iWhich<4 && selectedMaterial.Length > 0)
            {
                arrTex[iWhich] = selectedMaterial;
                switch (iWhich)
                {
                    case 0:
                        m_tbTex0.Text = selectedMaterial;
                        break;
                    case 1:
                        m_tbTex1.Text = selectedMaterial;
                        break;
                    case 2:
                        m_tbTex2.Text = selectedMaterial;
                        break;
                    case 3:
                        m_tbTex3.Text = selectedMaterial;
                        break;
                    default:
                        break;
                }

            }
        }

        private void m_cbCityLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iLevel = m_cbCityLevels.SelectedIndex;
            if (iLevel >= 0)
            {
                m_CurrLevelSetting = MCityLevelManager.Instance.GetCityLevel(iLevel);
                RefreshUI();
            }
        }

        private void m_btnSave_Click(object sender, EventArgs e)
        {
            // 将各等级的地形纹理保存到 xml
            String strCurFile = MFramework.Instance.CurrentFilename;
            if (strCurFile != null)
            {
                int iLastDot = strCurFile.LastIndexOf(".");
                String strSceneDirectory = MFramework.Instance.CurrentFilename.Substring(0, iLastDot);
                String strSaveFileName = strSceneDirectory + "\\" + MCityLevelManager.CITY_LEVEL_SAVING_FILE;
                MCityLevelManager.Instance.SaveToXml(strSaveFileName);
            }
        }

        private void m_tbTex0_TextChanged(object sender, EventArgs e)
        {
            if (m_CurrLevelSetting != null)
            {
                m_CurrLevelSetting.m_arrTerrainTextures[0] = m_tbTex0.Text;
            }
        }

        private void m_tbTex1_TextChanged(object sender, EventArgs e)
        {
            if (m_CurrLevelSetting != null)
            {
                m_CurrLevelSetting.m_arrTerrainTextures[1] = m_tbTex1.Text;
            }
        }

        private void m_tbTex2_TextChanged(object sender, EventArgs e)
        {
            if (m_CurrLevelSetting != null)
            {
                m_CurrLevelSetting.m_arrTerrainTextures[2] = m_tbTex2.Text;
            }
        }

        private void m_tbTex3_TextChanged(object sender, EventArgs e)
        {
            if (m_CurrLevelSetting != null)
            {
                m_CurrLevelSetting.m_arrTerrainTextures[3] = m_tbTex3.Text;
            }
        }

        private void m_btnPreview_Click(object sender, EventArgs e)
        {
            // 预览该等级对应的纹理

        }

    }
}