﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class InputForm : Form
    {
        public InputForm()
        {
            InitializeComponent();
        }

        private void m_btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public String InputValue
        {
            get
            {
                return m_textBoxInput.Text;
            }
        }
    }
}