﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class HeightMapConfig
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ExportHeightMap = new System.Windows.Forms.Button();
            this.btn_ImportHeightMap = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_LowHeight = new System.Windows.Forms.TextBox();
            this.textBox_HeighHeight = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_ExportHeightMap
            // 
            this.btn_ExportHeightMap.Location = new System.Drawing.Point(43, 150);
            this.btn_ExportHeightMap.Name = "btn_ExportHeightMap";
            this.btn_ExportHeightMap.Size = new System.Drawing.Size(126, 25);
            this.btn_ExportHeightMap.TabIndex = 0;
            this.btn_ExportHeightMap.Text = "导出高度图";
            this.btn_ExportHeightMap.UseVisualStyleBackColor = true;
            this.btn_ExportHeightMap.Click += new System.EventHandler(this.btn_ExportHeightMap_Click);
            // 
            // btn_ImportHeightMap
            // 
            this.btn_ImportHeightMap.Location = new System.Drawing.Point(43, 193);
            this.btn_ImportHeightMap.Name = "btn_ImportHeightMap";
            this.btn_ImportHeightMap.Size = new System.Drawing.Size(125, 24);
            this.btn_ImportHeightMap.TabIndex = 1;
            this.btn_ImportHeightMap.Text = "导入高度图";
            this.btn_ImportHeightMap.UseVisualStyleBackColor = true;
            this.btn_ImportHeightMap.Click += new System.EventHandler(this.btn_ImportHeightMap_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "最低高度";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "最高高度";
            // 
            // textBox_LowHeight
            // 
            this.textBox_LowHeight.Location = new System.Drawing.Point(112, 54);
            this.textBox_LowHeight.MaxLength = 4;
            this.textBox_LowHeight.Name = "textBox_LowHeight";
            this.textBox_LowHeight.Size = new System.Drawing.Size(42, 21);
            this.textBox_LowHeight.TabIndex = 4;
            this.textBox_LowHeight.Text = "0";
            this.textBox_LowHeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_LowHeight_KeyDown);
            // 
            // textBox_HeighHeight
            // 
            this.textBox_HeighHeight.Location = new System.Drawing.Point(112, 96);
            this.textBox_HeighHeight.MaxLength = 4;
            this.textBox_HeighHeight.Name = "textBox_HeighHeight";
            this.textBox_HeighHeight.Size = new System.Drawing.Size(42, 21);
            this.textBox_HeighHeight.TabIndex = 5;
            this.textBox_HeighHeight.Text = "255";
            this.textBox_HeighHeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_HeighHeight_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "高度范围";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "m";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(160, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "m";
            // 
            // m_btnClose
            // 
            this.m_btnClose.Location = new System.Drawing.Point(144, 231);
            this.m_btnClose.Name = "m_btnClose";
            this.m_btnClose.Size = new System.Drawing.Size(75, 23);
            this.m_btnClose.TabIndex = 9;
            this.m_btnClose.Text = "关闭";
            this.m_btnClose.UseVisualStyleBackColor = true;
            this.m_btnClose.Click += new System.EventHandler(this.m_btnClose_Click);
            // 
            // HeightMapConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 266);
            this.ControlBox = false;
            this.Controls.Add(this.m_btnClose);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_HeighHeight);
            this.Controls.Add(this.textBox_LowHeight);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_ImportHeightMap);
            this.Controls.Add(this.btn_ExportHeightMap);
            this.Name = "HeightMapConfig";
            this.Text = "HeightMapConfig";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_ExportHeightMap;
        private System.Windows.Forms.Button btn_ImportHeightMap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_LowHeight;
        private System.Windows.Forms.TextBox textBox_HeighHeight;
        private System.Windows.Forms.Label label3;

        float m_iLowHeight;
        float m_iHeighHeight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button m_btnClose;
    }
}