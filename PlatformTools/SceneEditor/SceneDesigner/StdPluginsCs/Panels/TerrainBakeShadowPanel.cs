﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class TerrainBakeShadowPanel : Form
    {
        public TerrainBakeShadowPanel()
        {
            InitializeComponent();
        }

        private void m_btnExportBakeScene_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SaveSceneTo3DS();
        }

        private void m_btnImprotBakeScene_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.RenderColorToVertex();
        }

  

    }
}