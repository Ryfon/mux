﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class AABBEditPanel : Form
    {
        public AABBEditPanel()
        {
            InitializeComponent();

            m_fScale = 1.0f;
            textBox1.Text = "1.0";
            GetSelEntity();
        }

        private void m_btn_GetSelEntity_Click(object sender, EventArgs e)
        {
            GetSelEntity();
         }

        private void GetSelEntity()
        {
            // 获取当前被选择的物件
            MEntity[] aEntities = MFramework.SelectionService.GetSelectedEntities();
            if (aEntities.Length == 0)
            {
                return;
            }

            m_textBox_SelEntityName.Text = aEntities[0].Name;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            m_fScale = (float)Convert.ToDouble(textBox1.Text);
            if(m_fScale <= 0.0f)
            {
                m_fScale = 1.0f;
                textBox1.Text = "1.0";
            }
        }

        private void btn_SetAABBScale_Click(object sender, EventArgs e)
        {
            textBox1_TextChanged(sender, e);

            MFramework.Instance.Scene.SetAABBScale(m_textBox_SelEntityName.Text, m_fScale);
        }
    }
}