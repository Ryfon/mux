﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using PluginAPI = Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Controls;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class NifPropertyEditor : Form
    {
        public NifPropertyEditor(MEntity pmTagEntity)
        {
            InitializeComponent();
            ActiveTemplateEntity = pmTagEntity;
        }

        private MEntity m_ActiveTemplateEntity;   // 当前的模板中的 Entity
        public MEntity ActiveTemplateEntity
        {
            set
            {
                m_ActiveTemplateEntity = value;
                m_ActiveSceneGraphView 
                    = MSceneGraphView.GenSceneGraphView(m_ActiveTemplateEntity);

                RefreshUI();
            }
        }

        private MSceneGraphView m_ActiveSceneGraphView;

        public void RefreshUI()
        {
            // 刷新界面
            m_tvModelView.Nodes.Clear();
            m_listBoxPropertyView.Items.Clear();
            m_groupBoxPropertyEditor.Controls.Clear();

            if (m_ActiveSceneGraphView != null)
            {
                m_tvModelView.Nodes.Add(m_ActiveSceneGraphView.ToTreeNode());
            }

        }

        private void m_btnGetModel_Click(object sender, EventArgs e)
        {
            // 获取当前模型对应的 nif
            MPalette actPalette = MFramework.Instance.PaletteManager.ActivePalette;
            if (actPalette != null)
            {
                ActiveTemplateEntity = actPalette.ActiveEntity;
            }
        }

        private void m_btnGetSelectedEntity_Click(object sender, EventArgs e)
        {
            MEntity[] arrEntitySeleteced = SelectionService
                    .GetSelectedEntities();

            if (arrEntitySeleteced.Length > 0)
            {
                MEntity entity = arrEntitySeleteced[0];
                ActiveTemplateEntity = entity;
            }

        }

        #region Service Accessors
        private static ISelectionService ms_pmSelectionService = null;
        private static ISelectionService SelectionService
        {
            get
            {
                if (ms_pmSelectionService == null)
                {
                    ms_pmSelectionService = ServiceProvider.Instance
                        .GetService(typeof(ISelectionService)) as
                        ISelectionService;
                    Debug.Assert(ms_pmSelectionService != null,
                        "Selection service not found!");
                }
                return ms_pmSelectionService;
            }
        }
        #endregion

        private void expandAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 全部展开
            m_tvModelView.ExpandAll();
        }

        private void collapseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 全部关闭
            m_tvModelView.CollapseAll();
        }

        private void m_tvModelView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (m_tvModelView.SelectedNode != null)
            {
                MSceneGraphNode sceneGraphNode = m_tvModelView.SelectedNode.Tag as MSceneGraphNode;
                if (sceneGraphNode != null)
                {
                    m_listBoxPropertyView.Items.Clear();
                    m_groupBoxPropertyEditor.Controls.Clear();

                    MAlphaProperty alphaProp = sceneGraphNode.GetAlphaProperty();
                    MMaterialProperty matProp = sceneGraphNode.GetMaterialProperty();
                    MVertexColorProperty vertColorProp = sceneGraphNode.GetVertexColorProperty();
                    MSpecularProperty specularProp = sceneGraphNode.GetSpecularProperty();

                    if (alphaProp != null)
                    {
                        m_listBoxPropertyView.Items.Add(alphaProp);
                    }

                    if (matProp != null)
                    {
                        m_listBoxPropertyView.Items.Add(matProp);
                    }

                    if (vertColorProp != null)
                    {
                        m_listBoxPropertyView.Items.Add(vertColorProp);
                    }

                    if (specularProp != null)
                    {
                        m_listBoxPropertyView.Items.Add(specularProp);
                    }
                }
            }
        }

        private void m_listBoxPropertyView_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_groupBoxPropertyEditor.Controls.Clear();

            if (m_listBoxPropertyView.SelectedItem is MAlphaProperty)
            {
                AlphaPropertyEditorCtrl ctrl
                = new AlphaPropertyEditorCtrl(m_listBoxPropertyView.SelectedItem as MAlphaProperty);
                m_groupBoxPropertyEditor.Controls.Add(ctrl);
                ctrl.Dock = DockStyle.Fill;
            }
            else if (m_listBoxPropertyView.SelectedItem is MMaterialProperty)
            {
                MaterialPropertyEditorCtrl ctrl 
                    = new MaterialPropertyEditorCtrl(m_listBoxPropertyView.SelectedItem as MMaterialProperty);
                m_groupBoxPropertyEditor.Controls.Add(ctrl);
                ctrl.Dock = DockStyle.Fill;

            }
            else if (m_listBoxPropertyView.SelectedItem is MSpecularProperty)
            {
                SpecularPropertyEditorCtrl ctrl
                    = new SpecularPropertyEditorCtrl(m_listBoxPropertyView.SelectedItem as MSpecularProperty);
                m_groupBoxPropertyEditor.Controls.Add(ctrl);
                ctrl.Dock = DockStyle.Fill;

            }
        }

        private void NifPropertyEditor_Load(object sender, EventArgs e)
        {

        }

        private void m_btnSaveToNif_Click(object sender, EventArgs e)
        {
            if (m_ActiveSceneGraphView != null)
            {
                m_ActiveSceneGraphView.SaveToNif();
            }
        }

        private void deletePropertyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 删除 property
            try
            {
                if (m_listBoxPropertyView.SelectedItem != null)
                {
                    // 是否删除对话框
                    if (MessageBox.Show("要删除 "+m_listBoxPropertyView.SelectedItem.ToString() + "吗？"
                            , "警告", MessageBoxButtons.YesNo)
                        == System.Windows.Forms.DialogResult.Yes)
                    {
                        (m_listBoxPropertyView.SelectedItem as MNiProperty).RemoveProperty();
                        m_tvModelView_AfterSelect(null, null);
                    }
                }
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }

        }

        private void addNiAlphaPropertyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MSceneGraphNode sceneGraphNode = m_tvModelView.SelectedNode.Tag as MSceneGraphNode;
            if (sceneGraphNode != null)
            {
                sceneGraphNode.AddAlphaProperty();
                m_tvModelView_AfterSelect(null, null);

            }
        }

        private void addNiSpecularPropertyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MSceneGraphNode sceneGraphNode = m_tvModelView.SelectedNode.Tag as MSceneGraphNode;
            if (sceneGraphNode != null)
            {
                sceneGraphNode.AddSpecularProperty();
                m_tvModelView_AfterSelect(null, null);

            }
        }

        private void addNiMaterialPropertyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MSceneGraphNode sceneGraphNode = m_tvModelView.SelectedNode.Tag as MSceneGraphNode;
            if (sceneGraphNode != null)
            {
                sceneGraphNode.AddMaterialProperty();
                m_tvModelView_AfterSelect(null, null);

            }
        }

        private void hideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MSceneGraphNode sceneGraphNode = m_tvModelView.SelectedNode.Tag as MSceneGraphNode;
            if (sceneGraphNode != null)
            {
                sceneGraphNode.Hide();

            }
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MSceneGraphNode sceneGraphNode = m_tvModelView.SelectedNode.Tag as MSceneGraphNode;
            if (sceneGraphNode != null)
            {
                sceneGraphNode.Show();

            }
        }

        private void m_btnTest_Click(object sender, EventArgs e)
        {
            // 测试用按钮
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "nif files (*.nif)|*.nif|All files (*.*)|*.*";
            dlg.FilterIndex = 1;
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                MSceneGraphView.TestLoadNif(dlg.FileName);
            }
        }

        private void m_btnCheckNif_Click(object sender, EventArgs e)
        {
            // 检查 nif 中是否有 fog property 和 dynamic effect
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            ArrayList checkInfo = new ArrayList();    // 有问题的 nif 列表
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                // 查找目录下的所有 nif 文件
                CheckNif(new DirectoryInfo(dlg.SelectedPath), checkInfo);

                if (checkInfo.Count > 0)
                {
                    String strAllInfo = "包含 Fog 或者 DynamicEffect 的 Nif: \r\n";
                    foreach (String strInfo in checkInfo)
                    {
                        strAllInfo += strInfo + "\r\n";
                    }

                    strAllInfo += "\r\n 是否要批量修改这些文件？";
                    if (DialogResult.Yes == 
                        MessageBox.Show(this, strAllInfo, "警告", MessageBoxButtons.YesNo))
                    {
                        ArrayList errorList = new ArrayList();

                        foreach (String strInfo in checkInfo)
                        {
                            if (!MSceneGraphView.ClearNifFogPropertyAndDynamicEffect(strInfo))
                            {
                                errorList.Add(strInfo);
                            }
                        }

                        if (errorList.Count > 0)
                        {
                            String strAllErrors = "以下文件清除失败: \r\n";
                            foreach (String strInfo in errorList)
                            {
                                strAllErrors += strInfo + "\r\n";
                            }

                            MessageBox.Show(strAllErrors);
                        }
                        else
                        {
                            MessageBox.Show("清理完成.");
                        }


                    }
                }
                else
                {
                    MessageBox.Show("没有异常的 NIf.");
                }
            }

        }

        private void CheckNif(DirectoryInfo dirInfo, ArrayList checkInfo)
        {
            FileInfo[] fileList = dirInfo.GetFiles();
            foreach (FileInfo fileInfo in fileList)
            {
                if (fileInfo.Extension.ToLower().Equals(".nif"))
                {
                    // 检查这个 nif
                    if (MSceneGraphView.CheckFogPropertyAndDynamicEffect(fileInfo.FullName))
                    {
                        checkInfo.Add(fileInfo.FullName);
                    }
                }
            }

            DirectoryInfo[] subDirList = dirInfo.GetDirectories();
            foreach (DirectoryInfo subDirInfo in subDirList)
            {
                CheckNif(subDirInfo, checkInfo);
            }
        }
    }
}