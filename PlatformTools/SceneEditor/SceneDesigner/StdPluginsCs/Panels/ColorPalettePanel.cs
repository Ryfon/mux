﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    public partial class ColorPalettePanel : Form
    {
        public ColorPalettePanel()
        {
            InitializeComponent();
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            AddColorBlock();
        }

        private PictureBox AddColorBlock()
        {
            PictureBox pictureBox = new PictureBox();
            pictureBox.BackColor = Color.White;
            pictureBox.Size = new Size(16, 16);
            m_ColorButtonFlowLayoutPanel.Controls.Add(pictureBox);
            pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.colorButton_MouseDown);
            return pictureBox;
        }

        private void colorButton_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Right)
            {
                // 右键改变颜色
                ColorDialog dlg = new ColorDialog();
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    (sender as PictureBox).BackColor = dlg.Color;
                }
            }

            Color color = (sender as PictureBox).BackColor;
            m_pictureBoxColorPreview.BackColor = color;
            String strColor = color.R.ToString() + "," + color.G.ToString() + "," + color.B;
            m_textBoxColor.Text = strColor;
        }

        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 保存调色板文件
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            dlg.FilterIndex = 1;
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlElement xmlElmtRoot = xmlDoc.CreateElement("Root");
                foreach (PictureBox pb in m_ColorButtonFlowLayoutPanel.Controls)
                {
                    XmlElement xmlElmtColor = xmlDoc.CreateElement("Color");
                    xmlElmtColor.SetAttribute("R", pb.BackColor.R.ToString());
                    xmlElmtColor.SetAttribute("G", pb.BackColor.G.ToString());
                    xmlElmtColor.SetAttribute("B", pb.BackColor.B.ToString());
                    xmlElmtRoot.AppendChild(xmlElmtColor);
                }
                xmlDoc.AppendChild(xmlElmtRoot);
                xmlDoc.Save(dlg.FileName);
            }
        }

        private void 载入ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 载入调色板文件
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            dlg.FilterIndex = 1;
            dlg.RestoreDirectory = true;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    // 清空原色块
                    m_ColorButtonFlowLayoutPanel.Controls.Clear();

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(dlg.FileName);
                    XmlElement xmlElmtRoot = xmlDoc.FirstChild as XmlElement;
                    XmlElement xmlElmtColor = xmlElmtRoot.FirstChild as XmlElement;
                    while (xmlElmtColor != null)
                    {
                        int r = int.Parse(xmlElmtColor.GetAttribute("R"));
                        int g = int.Parse(xmlElmtColor.GetAttribute("G"));
                        int b = int.Parse(xmlElmtColor.GetAttribute("B"));

                        PictureBox pb = AddColorBlock();
                        pb.BackColor = Color.FromArgb(255, r, g, b);

                        xmlElmtColor = xmlElmtColor.NextSibling as XmlElement;
                    }
                }
                catch (Exception e2)
                {
                    MessageBox.Show(e2.ToString());
                }

            }
        }

    }
}