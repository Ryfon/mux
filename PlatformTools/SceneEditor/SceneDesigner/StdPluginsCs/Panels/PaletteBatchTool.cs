﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Utility;
using Message = Emergent.Gamebryo.SceneDesigner.PluginAPI.Message;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class PaletteBatchTool : Form
    {
        public PaletteBatchTool()
        {
            InitializeComponent();

            m_pmSrcEntity = MFramework.Instance.PaletteManager.ActivePalette.ActiveEntity;

            m_textBoxSrcPalette.Text = m_pmSrcEntity.Name;
        }
     
        private void btn_BatchCreate_Click(object sender, EventArgs e)
        {
            if(m_pmSrcEntity == null || m_pmDesEntity == null)
            {
                return;
            }

            MFramework.Instance.Scene.BatchCreateByPalette(m_pmSrcEntity, m_pmDesEntity, m_fZOffset);
        }

        private void btn_GetDesPalette_Click(object sender, EventArgs e)
        {
            m_pmDesEntity = MFramework.Instance.PaletteManager.ActivePalette.ActiveEntity;

            if(m_pmDesEntity == null)
            {
                MessageBox.Show("错误的Palette名称");
                return;
            }

            if (m_pmDesEntity == m_pmSrcEntity)
            {
                MessageBox.Show("目标palette与源palette相同");
                return;
            }

            m_textBoxDesPalette.Text = m_pmDesEntity.Name;
        }

        private void textBox_ZOffset_TextChanged(object sender, EventArgs e)
        {
            m_fZOffset = float.Parse(textBox_ZOffset.Text);
        }
    }
}