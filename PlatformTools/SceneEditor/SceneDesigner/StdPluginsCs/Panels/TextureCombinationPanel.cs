﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class TextureCombinationPanel : Form
    {
        public TextureCombinationPanel()
        {
            InitializeComponent();
        }

        private void m_btnSelectTexturePath_Click(object sender, EventArgs e)
        {
            // 选择纹理所在路径
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                m_textBoxTexturePath.Text = fbd.SelectedPath;
            }
        }
    }
}