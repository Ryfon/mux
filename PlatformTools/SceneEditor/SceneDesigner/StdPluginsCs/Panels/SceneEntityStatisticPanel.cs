﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;
using Emergent.Gamebryo.SceneDesigner.Framework;
using Emergent.Gamebryo.SceneDesigner.PluginAPI;
using Emergent.Gamebryo.SceneDesigner.PluginAPI.StandardServices;
using Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class SceneEntityStatisticPanel : Form
    {
        private MSceneEntityStatistic m_pmSceneEntityStatistic;

        private Hashtable m_htNifs;
        private Hashtable m_htTextures;

        public SceneEntityStatisticPanel()
        {
            InitializeComponent();
            RegisterEventHandlers();

            m_htNifs = new Hashtable();
            m_htTextures = new Hashtable();
        }

        public void RegisterEventHandlers()
        {
            MFramework FW = MFramework.Instance;
            FW.EventManager.NewSceneLoaded +=
                new MEventManager.__Delegate_NewSceneLoaded(
                EventManager_OnNewSceneLoaded);
 
        }

        private static ISelectionService ms_pmSelectionService = null;
        private static ISelectionService SelectionService
        {
            get
            {
                if (ms_pmSelectionService == null)
                {
                    ms_pmSelectionService = ServiceProvider.Instance
                        .GetService(typeof(ISelectionService)) as
                        ISelectionService;
                }
                return ms_pmSelectionService;
            }
        }

        private static ICommandService ms_pmCommandService = null;
        private static ICommandService CommandService
        {
            get
            {
                if (ms_pmCommandService == null)
                {
                    ms_pmCommandService = ServiceProvider.Instance
                        .GetService(typeof(ICommandService)) as
                        ICommandService;
                }
                return ms_pmCommandService;
            }
        }

        // 收集strPath 路径下所有纹理和nif文件信息并保存到 m_htNIfs 和 m_htTextures
        private void _CollectNifFileInfo(String strPath)
        {
            String[] arrFiles = Directory.GetFiles(strPath);
            foreach (String strFile in arrFiles)
            {
                FileInfo fileInfo = new FileInfo(strFile);
                if (fileInfo.Extension.ToLower().Equals("nif"))
                {
                    // nif 文件
                    m_htNifs[strFile] = fileInfo;
                }
            }

            String[] arrSubDirs = Directory.GetDirectories(strPath);
            foreach (String strSubDir in arrSubDirs)
            {
                _CollectNifFileInfo(strSubDir);
            }

        }

        private void _CollectTextureInfo(String strPath)
        {
            String[] arrFiles = Directory.GetFiles(strPath);
            foreach (String strFile in arrFiles)
            {
                FileInfo fileInfo = new FileInfo(strFile);
                if (fileInfo.Extension.ToLower().Equals(".dds") 
                    || fileInfo.Extension.ToLower().Equals(".bmp"))
                {
                    // nif 文件
                    m_htTextures[fileInfo.Name] = fileInfo;
                }
            }

            String[] arrSubDirs = Directory.GetDirectories(strPath);
            foreach (String strSubDir in arrSubDirs)
            {
                _CollectTextureInfo(strSubDir);
            }
        }

        private void m_btnStatisticSceneEntity_Click(object sender, EventArgs e)
        {
            if (!MFramework.Instance.Scene.HasTerrain()) return;

            if (m_htTextures.Count == 0)
            {
                // 第一次收集场景信息
                //_CollectNifFileInfo(Application.StartupPath + "Data\\Model\\SceneObj");
                //_CollectNifFileInfo(Application.StartupPath + "Data\\Effect");

                String strTexPath = Application.StartupPath + "\\Data\\Texture\\SceneObj";
                if (Directory.Exists(strTexPath))
                {
                    _CollectTextureInfo(strTexPath);
                }
                strTexPath = Application.StartupPath + "\\Data\\Effect";
                if (Directory.Exists(strTexPath))
                {
                    _CollectTextureInfo(strTexPath);
                }

                strTexPath = Application.StartupPath + "\\Data\\Texture\\Terrain";
                if (Directory.Exists(strTexPath))
                {
                    _CollectTextureInfo(strTexPath);
                }

            }

            // 场景整体数据
            int iEntityCount = 0;
            int iNifCount = 0;
            int iNifTotalSize = 0;
            int iTextureCount = 0;
            int iTextureTotalSize = 0;
            Hashtable htSceneTextures = new Hashtable();    // 场景中所有使用的纹理

           // m_tbEntityCount.Text = MFramework.Instance.CurrentFilename;
            m_pmSceneEntityStatistic = new MSceneEntityStatistic();
            bool bStatLishts = m_cbStatLights.Checked;
            m_pmSceneEntityStatistic.GenerateFromCurrentScene(bStatLishts);
            m_dgvSceneEntityStatistic.Rows.Clear();
            Hashtable htStatResult = m_pmSceneEntityStatistic.GetStatisticResult();
            m_dgvSceneEntityStatistic.Rows.Add(htStatResult.Values.Count);
            int iRow = 0;
            foreach (MEntityStatInfo entityStatInfo in htStatResult.Values)
            {
                DataGridViewRow row = m_dgvSceneEntityStatistic.Rows[iRow];
                row.Tag = entityStatInfo;
                row.Cells[0].Value = entityStatInfo.m_strNifFilename;
                row.Cells[1].Value = entityStatInfo.m_arrInstStats.Count;
                row.Cells[2].Value = entityStatInfo.m_iGeomCount;
                row.Cells[3].Value = entityStatInfo.m_iVertCount;
                row.Cells[4].Value = entityStatInfo.m_iTriCount;
                row.Cells[5].Value = entityStatInfo.m_iMaxLights;
                row.Cells[6].Value = entityStatInfo.m_htTextures.Count;
                // 如果纹理大小没有计算过,在这里计算
                if (entityStatInfo.m_i64TotalTextureLength == 0)
                {
                    foreach (MTextureStatInfo texStatInfo in entityStatInfo.m_htTextures.Values)
                    {
                        if (texStatInfo.m_i64Size == 0)
                        {
                            if (m_htTextures.ContainsKey(texStatInfo.m_strFilename))
                            {
                                FileInfo texFileInfo = m_htTextures[texStatInfo.m_strFilename] as FileInfo;

                                texStatInfo.m_i64Size = texFileInfo.Length * texStatInfo.m_iSeqTextureNum;
                            }
                        }

                        entityStatInfo.m_i64TotalTextureLength += texStatInfo.m_i64Size;
                    }
                }

                row.Cells[7].Value = entityStatInfo.m_i64TotalTextureLength / 1000; // to KB
                row.Cells[8].Value = entityStatInfo.m_iParticleCount;
                row.Cells[9].Value = entityStatInfo.m_iMaxParticleCount;

                // 如果 nif 尺寸没有计算过，在这里统计
                if (entityStatInfo.m_i64NifSize == 0)
                {
                    FileInfo fileInfo = new FileInfo(entityStatInfo.m_strNifFilename);
                    if (fileInfo.Exists)
                    {
                        entityStatInfo.m_i64NifSize = fileInfo.Length;
                    }
                }
                row.Cells[10].Value = entityStatInfo.m_i64NifSize / 1000;

                // 统计场景整体数据
                iEntityCount += entityStatInfo.m_arrInstStats.Count;    // 物件数量
                iNifCount++;
                iNifTotalSize += (int)(entityStatInfo.m_i64NifSize / 1000);
                foreach (MTextureStatInfo texStatInfo in entityStatInfo.m_htTextures.Values)
                {
                    if (!htSceneTextures.Contains(texStatInfo.m_strFilename) 
                        && m_htTextures.ContainsKey(texStatInfo.m_strFilename))
                    {
                        FileInfo texFileInfo = m_htTextures[texStatInfo.m_strFilename] as FileInfo;
                        htSceneTextures.Add(texStatInfo.m_strFilename, texFileInfo);
                        iTextureCount++;
                        iTextureTotalSize += (int)(texFileInfo.Length / 1000);
                    }
                }

                ++iRow;
            }

            m_dgvSceneEntityStatistic.AutoResizeColumns();

            m_dgvSceneEntityStatistic.ClearSelection();

            // 填充场景数据
            m_tbEntityCount.Text = iEntityCount.ToString();
            m_tbNifCount.Text = iNifCount.ToString();
            m_tbNifSize.Text = iNifTotalSize.ToString();
            m_tbTexCount.Text = iTextureCount.ToString();
            m_tbTexSize.Text = iTextureTotalSize.ToString();

        }

        void EventManager_OnNewSceneLoaded(MScene pmScene)
        {
            m_pmSceneEntityStatistic = null;
            m_dgvSceneEntityStatistic.Rows.Clear();
            m_dgvEntityInst.Rows.Clear();
            m_tbEntityCount.Text = "";
        }

        private void 在场景中选择ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_dgvSceneEntityStatistic.SelectedRows.Count > 0)
            {

                DataGridViewRow row = m_dgvSceneEntityStatistic.SelectedRows[0];
                if (row != null)
                {
                    MEntityStatInfo entityStatInfo = row.Tag as MEntityStatInfo;

                    CommandService.BeginUndoFrame("Replace main selection.");

                    SelectionService.ClearSelectedEntities();
                    foreach (MEntityStatInst statInst in entityStatInfo.m_arrInstStats)
                    {
                        SelectionService.AddEntityToSelection(statInst.m_pmTagEntity);
                    }

                    CommandService.EndUndoFrame(SelectionService.CommandsAreUndoable);
                }
            }
        }

        private void m_dgvSceneEntityStatistic_SelectionChanged(object sender, EventArgs e)
        {
            if (m_dgvSceneEntityStatistic.SelectedRows.Count == 0) return;


            m_dgvEntityInst.Rows.Clear();
            m_dgvTextureInfo.Rows.Clear();

            DataGridViewRow entityRow = m_dgvSceneEntityStatistic.SelectedRows[0];
            if (entityRow != null)
            {
                MEntityStatInfo entityStatInfo = entityRow.Tag as MEntityStatInfo;
                if (entityStatInfo == null) return;

                // 填充 entity inst信息 dgv
                if (entityStatInfo.m_arrInstStats.Count > 0)
                {
                    m_dgvEntityInst.Rows.Add(entityStatInfo.m_arrInstStats.Count);
                    int iRow = 0;
                    foreach (MEntityStatInst statInst in entityStatInfo.m_arrInstStats)
                    {
                        DataGridViewRow row = m_dgvEntityInst.Rows[iRow];
                        row.Tag = statInst;
                        if (statInst.m_iLights == 0)
                        {
                            // 从 light manager 中得到
                            statInst.m_iLights = MLightManager.Instance.GetAffectLights(statInst.m_pmTagEntity).Count;
                            if (statInst.m_iLights > entityStatInfo.m_iMaxLights)
                            {
                                entityStatInfo.m_iMaxLights = statInst.m_iLights;
                            }
                        }

                        row.Cells[0].Value = statInst.m_iLights;
                        row.Cells[1].Value = statInst.m_pmBound.Center.ToString();
                        row.Cells[2].Value = statInst.m_pmBound.R;

                        iRow++;
                    }

                    m_dgvEntityInst.ClearSelection();

                    // 更新 max lights
                    entityRow.Cells[5].Value = entityStatInfo.m_iMaxLights;
                }

                // 填充纹理信息
                if (entityStatInfo.m_htTextures.Count > 0)
                {
                    m_dgvTextureInfo.Rows.Add(entityStatInfo.m_htTextures.Count);
                    int iRow = 0;
                    foreach (MTextureStatInfo textStatInfo in entityStatInfo.m_htTextures.Values)
                    {
                        DataGridViewRow row = m_dgvTextureInfo.Rows[iRow];
                        row.Tag = textStatInfo;
                        row.Cells[0].Value = textStatInfo.m_strFilename;
                        row.Cells[1].Value = textStatInfo.m_iInstCount;
                        row.Cells[2].Value = textStatInfo.m_i64Size / 1000;
                        row.Cells[3].Value = textStatInfo.m_iWidth;
                        row.Cells[4].Value = textStatInfo.m_iHeight;
                        row.Cells[5].Value = textStatInfo.m_iSeqTextureNum;

                        iRow++;
                    }
                }

            }
            m_dgvEntityInst.AutoResizeRows();



        }

        private void m_dgvEntityInst_SelectionChanged(object sender, EventArgs e)
        {
            if (m_dgvEntityInst.SelectedRows.Count == 0) return;

            DataGridViewRow row = m_dgvEntityInst.SelectedRows[0];
            if (row != null)
            {
                MEntityStatInst statInst = row.Tag as MEntityStatInst;
                if (statInst != null && statInst.m_pmTagEntity != null)
                {
                    CommandService.BeginUndoFrame("Replace main selection.");

                    SelectionService.ClearSelectedEntities();
                    SelectionService.AddEntityToSelection(statInst.m_pmTagEntity);

                    CommandService.EndUndoFrame(SelectionService.CommandsAreUndoable);
                }

                //ServiceProvider sp = ServiceProvider.Instance;
                //IUICommandService uiCommandService =
                //    sp.GetService(typeof(IUICommandService)) as IUICommandService;

                //UICommand zoomCommand =
                //    uiCommandService.GetCommand("MoveToSelection");
                // zoomCommand.DoClick(this, null);
            }
        }

        private void m_dgvEntityInst_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            m_dgvEntityInst_SelectionChanged(sender, null);
        }

        private void SceneEntityStatisticPanel_Load(object sender, EventArgs e)
        {

        }

        private void m_dgvSceneEntityStatistic_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void m_btnLightDistribute_Click(object sender, EventArgs e)
        {
            LightDistributeMap distMap = new LightDistributeMap();
            distMap.Show();
        }
    }
}