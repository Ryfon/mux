﻿using System;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class ModelScreenshot : Form
    {
        public ModelScreenshot()
        {
            InitializeComponent();

            if(m_checkBoxExprotSameDir.Checked)
            {
                m_textBoxExportPath.Enabled = false;
            }
        }

        private void m_btnExportBrowser_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            if (folderDlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
			 {
				m_textBoxExportPath.Text = folderDlg.SelectedPath;
			 }
        }

        private void m_btnModelBrowser_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if(dlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                m_textBoxModleDir.Text = dlg.SelectedPath;
            }
        }

        private void m_checkBoxExprotSameDir_CheckedChanged(object sender, EventArgs e)
        {
            if(m_checkBoxExprotSameDir.Checked)
            {
                m_textBoxExportPath.Enabled = false;
            }
            else
            {
                m_textBoxExportPath.Enabled = true;
            }
        }

        private void ProcessFile(String strFileName)
        {
            if(!Path.GetExtension(strFileName).Equals(".nif"))
            {
                return;
            }

            String strExportFileName = Path.GetFileNameWithoutExtension(strFileName) + ".jpg";
            
            String strExportPath = "";
            if(m_checkBoxExprotSameDir.Checked)
            {
               strExportPath = Path.GetDirectoryName(strFileName) + "\\" + strExportFileName;
                
            }
            else
            {
                strExportPath = m_textBoxExportPath.Text + "\\" + strExportFileName;
            }

            if (m_checkBoxAddExport.Checked)
            {
                if (File.Exists(strExportPath))
                {
                    return;
                }
            }

            MTemplatePreviewer.Instance.ExportModelScreenShot(strFileName, strExportPath);
        }

        private void ThreacProcDirectory()
        {
            lock (this)
            {
                MFramework.Instance.SetRenderWindow(false);

                ProcessDirectory(m_textBoxModleDir.Text);

                MFramework.Instance.SetRenderWindow(true);

                MessageBox.Show("导出完毕");
            }

        }

        private void ProcessDirectory(String strDirName)
        {
            String[] SubFiles = Directory.GetFiles(strDirName);
            foreach(String strSubFile in SubFiles)
            {
                ProcessFile(strSubFile);
            }

            String[] SubPaths = Directory.GetDirectories(strDirName);
            foreach (String strSubPath in SubPaths)
            {
                // is file [11/30/2009 hemeng]
                if (File.Exists(strSubPath))
                {
                    ProcessFile(strSubPath);
                }
                else if (Directory.Exists(strSubPath))
                {
                    ProcessDirectory(strSubPath);
                }
            }
        }

        private void m_btnDoExport_Click(object sender, EventArgs e)
        {
            if(m_textBoxModleDir.Text == "")
            {
                return;
            }

            if(!m_checkBoxExprotSameDir.Checked && m_textBoxExportPath.Text == "")
            {
                return;
            }


            Thread thrd = new Thread(new ThreadStart(ThreacProcDirectory));
            thrd.Start();
            
        }

        private void m_textBoxTexWidth_KeyDown(object sender, KeyEventArgs e)
        {
            if(m_textBoxTexWidth.Text != "")
            {
                int iWidth = Convert.ToInt32(m_textBoxTexWidth.Text);
                MTemplatePreviewer.Instance.SetTextureWidth(iWidth);
            }
        }

        private void m_textBoxTexHeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (m_textBoxTexHeight.Text != "")
            {
                int iHeight = Convert.ToInt32(m_textBoxTexHeight.Text);
                MTemplatePreviewer.Instance.SetTextureHeight(iHeight);
            }
        }   

        private void m_btnSetCamera_Click(object sender, EventArgs e)
        {
            if(m_textBoxCamerX.Text == "" || m_textBoxCamerY.Text == "" || m_textBoxCamerZ.Text == "")
            {
                return;
            }

            float fX = (float)Convert.ToDouble(m_textBoxCamerX.Text);
            float fY = (float)Convert.ToDouble(m_textBoxCamerY.Text);
            float fZ = (float)Convert.ToDouble(m_textBoxCamerZ.Text);

            MTemplatePreviewer.Instance.SetCameraViewPoint(fX, fY, fZ);
        }

    }
}