﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class LightDistributeMap
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_picBoxDistributeMap = new System.Windows.Forms.PictureBox();
            this.m_btnRefresh = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_tbLightLum = new System.Windows.Forms.TextBox();
            this.m_cbDistributeMapType = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_picBoxDistributeMap)).BeginInit();
            this.SuspendLayout();
            // 
            // m_picBoxDistributeMap
            // 
            this.m_picBoxDistributeMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_picBoxDistributeMap.Location = new System.Drawing.Point(1, 2);
            this.m_picBoxDistributeMap.Name = "m_picBoxDistributeMap";
            this.m_picBoxDistributeMap.Size = new System.Drawing.Size(525, 503);
            this.m_picBoxDistributeMap.TabIndex = 0;
            this.m_picBoxDistributeMap.TabStop = false;
            this.m_picBoxDistributeMap.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_picBoxDistributeMap_MouseDown);
            // 
            // m_btnRefresh
            // 
            this.m_btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_btnRefresh.Location = new System.Drawing.Point(349, 509);
            this.m_btnRefresh.Name = "m_btnRefresh";
            this.m_btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.m_btnRefresh.TabIndex = 1;
            this.m_btnRefresh.Text = "刷新";
            this.m_btnRefresh.UseVisualStyleBackColor = true;
            this.m_btnRefresh.Click += new System.EventHandler(this.m_btnRefresh_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(142, 514);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "单个灯光/物件显示亮度";
            // 
            // m_tbLightLum
            // 
            this.m_tbLightLum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_tbLightLum.Location = new System.Drawing.Point(279, 511);
            this.m_tbLightLum.Name = "m_tbLightLum";
            this.m_tbLightLum.Size = new System.Drawing.Size(50, 21);
            this.m_tbLightLum.TabIndex = 3;
            this.m_tbLightLum.Text = "0.1";
            // 
            // m_cbDistributeMapType
            // 
            this.m_cbDistributeMapType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_cbDistributeMapType.FormattingEnabled = true;
            this.m_cbDistributeMapType.Items.AddRange(new object[] {
            "光照分布图",
            "物件分布图"});
            this.m_cbDistributeMapType.Location = new System.Drawing.Point(1, 511);
            this.m_cbDistributeMapType.Name = "m_cbDistributeMapType";
            this.m_cbDistributeMapType.Size = new System.Drawing.Size(121, 20);
            this.m_cbDistributeMapType.TabIndex = 4;
            // 
            // LightDistributeMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 534);
            this.Controls.Add(this.m_cbDistributeMapType);
            this.Controls.Add(this.m_tbLightLum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_btnRefresh);
            this.Controls.Add(this.m_picBoxDistributeMap);
            this.Name = "LightDistributeMap";
            this.Text = "光照/物件分布图";
            ((System.ComponentModel.ISupportInitialize)(this.m_picBoxDistributeMap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox m_picBoxDistributeMap;
        private System.Windows.Forms.Button m_btnRefresh;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_tbLightLum;
        private System.Windows.Forms.ComboBox m_cbDistributeMapType;
    }
}