﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emergent.Gamebryo.SceneDesigner.Framework;
namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    public partial class TerrainHeightCheckPanel : Form
    {
        public TerrainHeightCheckPanel()
        {
            InitializeComponent();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            float fLimitedHeight = 20.0f;
            int iAngle = 45;
            float fDistance = 100.0f;
            int nChechType = 0;

            if(m_radioBtnHeightCheck.Checked)   //高度检测
            {
                try
                {
                    fLimitedHeight = float.Parse(m_textBoxLimitedHeight.Text);
                    
                }
                catch (Exception ee)
                {
                    MessageBox.Show("请输入一个合法的浮点数");
                    return;
                }
                nChechType = 0;

                MFramework.Instance.Scene.CalculateOverTopVertex(fLimitedHeight, (float)iAngle, fDistance, nChechType);
            }

            if(m_radioBtnRayCheck.Checked)  //射线检测
            {
                try
                {
                    iAngle = int.Parse(m_textBoxAngle.Text);
                    //fDistance = float.Parse(m_textBoxDistance.Text);
                }
                catch (Exception ee)
                {
                    MessageBox.Show("请输入一个合法的浮点数");
                    return;
                }
                //nChechType = 1;
                //MFramework.Instance.Scene.CalculateOverTopVertex(fLimitedHeight, fAngle, fDistance, nChechType);
                // 地形优化
                MFramework.Instance.Scene.OptimizeTerrain(true);
                MFramework.Instance.Scene.TerrainToCollisionBlock(iAngle, true);
            }

            

        }

        private void m_checkBoxOverTop_CheckedChanged(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetRenderOverTopVertex(m_checkBoxOverTop.Checked);
        }

        private void btn_green_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetTerrainHeightCheckColorType(0);
        }

        private void btn_blue_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetTerrainHeightCheckColorType(1);
        }

        private void btn_red_Click(object sender, EventArgs e)
        {
            MFramework.Instance.Scene.SetTerrainHeightCheckColorType(2);
        }

        private void btn_TerrRever_Click(object sender, EventArgs e)
        {
            // 地形还原
            MFramework.Instance.Scene.OptimizeTerrain(false);
        }

        private void TerrainHeightCheckPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            // 地形还原
            MFramework.Instance.Scene.SetRenderOverTopVertex(false);
            MFramework.Instance.Scene.OptimizeTerrain(false);
        }
    }
}