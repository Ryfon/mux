﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class GridBaseAreaPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_listBoxGridBaseRegion = new System.Windows.Forms.ListBox();
            this.m_contextMenuStripGridBasedRegion = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_toolStripMenuItemGrow = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStripMenuItemSpray = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStripMenuItemVisableTerrain = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemHideTerrain = new System.Windows.Forms.ToolStripMenuItem();
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_btnRemove = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_textBoxGridCount = new System.Windows.Forms.TextBox();
            this.m_checkBoxInvisableWithBlock = new System.Windows.Forms.CheckBox();
            this.m_toolStripMenuItemWater = new System.Windows.Forms.ToolStripMenuItem();
            this.m_contextMenuStripGridBasedRegion.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_listBoxGridBaseRegion
            // 
            this.m_listBoxGridBaseRegion.ContextMenuStrip = this.m_contextMenuStripGridBasedRegion;
            this.m_listBoxGridBaseRegion.FormattingEnabled = true;
            this.m_listBoxGridBaseRegion.ItemHeight = 12;
            this.m_listBoxGridBaseRegion.Location = new System.Drawing.Point(13, 12);
            this.m_listBoxGridBaseRegion.Name = "m_listBoxGridBaseRegion";
            this.m_listBoxGridBaseRegion.Size = new System.Drawing.Size(172, 208);
            this.m_listBoxGridBaseRegion.TabIndex = 0;
            this.m_listBoxGridBaseRegion.SelectedIndexChanged += new System.EventHandler(this.m_listBoxGridBaseRegion_SelectedIndexChanged);
            // 
            // m_contextMenuStripGridBasedRegion
            // 
            this.m_contextMenuStripGridBasedRegion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_toolStripMenuItemGrow,
            this.m_toolStripMenuItemSpray,
            this.m_toolStripMenuItemVisableTerrain,
            this.toolStripMenuItemHideTerrain,
            this.m_toolStripMenuItemWater});
            this.m_contextMenuStripGridBasedRegion.Name = "m_contextMenuStripGridBasedRegion";
            this.m_contextMenuStripGridBasedRegion.Size = new System.Drawing.Size(153, 136);
            // 
            // m_toolStripMenuItemGrow
            // 
            this.m_toolStripMenuItemGrow.Name = "m_toolStripMenuItemGrow";
            this.m_toolStripMenuItemGrow.Size = new System.Drawing.Size(152, 22);
            this.m_toolStripMenuItemGrow.Text = "生长模型";
            this.m_toolStripMenuItemGrow.Click += new System.EventHandler(this.m_toolStripMenuItemGrow_Click);
            // 
            // m_toolStripMenuItemSpray
            // 
            this.m_toolStripMenuItemSpray.Name = "m_toolStripMenuItemSpray";
            this.m_toolStripMenuItemSpray.Size = new System.Drawing.Size(152, 22);
            this.m_toolStripMenuItemSpray.Text = "生成排浪";
            this.m_toolStripMenuItemSpray.Click += new System.EventHandler(this.m_toolStripMenuItemSpray_Click);
            // 
            // m_toolStripMenuItemVisableTerrain
            // 
            this.m_toolStripMenuItemVisableTerrain.Name = "m_toolStripMenuItemVisableTerrain";
            this.m_toolStripMenuItemVisableTerrain.Size = new System.Drawing.Size(152, 22);
            this.m_toolStripMenuItemVisableTerrain.Text = "使地形可见";
            this.m_toolStripMenuItemVisableTerrain.Click += new System.EventHandler(this.m_toolStripMenuItemSetAlpha_Click);
            // 
            // toolStripMenuItemHideTerrain
            // 
            this.toolStripMenuItemHideTerrain.Name = "toolStripMenuItemHideTerrain";
            this.toolStripMenuItemHideTerrain.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemHideTerrain.Text = "隐藏地形";
            this.toolStripMenuItemHideTerrain.Click += new System.EventHandler(this.toolStripMenuItemHideTerrain_Click);
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Location = new System.Drawing.Point(25, 233);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(58, 24);
            this.m_btnAdd.TabIndex = 1;
            this.m_btnAdd.Text = "添加";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_btnRemove
            // 
            this.m_btnRemove.Location = new System.Drawing.Point(113, 233);
            this.m_btnRemove.Name = "m_btnRemove";
            this.m_btnRemove.Size = new System.Drawing.Size(58, 24);
            this.m_btnRemove.TabIndex = 2;
            this.m_btnRemove.Text = "删除";
            this.m_btnRemove.UseVisualStyleBackColor = true;
            this.m_btnRemove.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 269);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "包含 Grid 数量:";
            // 
            // m_textBoxGridCount
            // 
            this.m_textBoxGridCount.Location = new System.Drawing.Point(113, 267);
            this.m_textBoxGridCount.Name = "m_textBoxGridCount";
            this.m_textBoxGridCount.Size = new System.Drawing.Size(71, 21);
            this.m_textBoxGridCount.TabIndex = 5;
            this.m_textBoxGridCount.Text = "0";
            // 
            // m_checkBoxInvisableWithBlock
            // 
            this.m_checkBoxInvisableWithBlock.AutoSize = true;
            this.m_checkBoxInvisableWithBlock.Checked = true;
            this.m_checkBoxInvisableWithBlock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxInvisableWithBlock.Location = new System.Drawing.Point(13, 294);
            this.m_checkBoxInvisableWithBlock.Name = "m_checkBoxInvisableWithBlock";
            this.m_checkBoxInvisableWithBlock.Size = new System.Drawing.Size(156, 16);
            this.m_checkBoxInvisableWithBlock.TabIndex = 6;
            this.m_checkBoxInvisableWithBlock.Text = "隐藏地形同时设置碰撞块";
            this.m_checkBoxInvisableWithBlock.UseVisualStyleBackColor = true;
            // 
            // m_toolStripMenuItemWater
            // 
            this.m_toolStripMenuItemWater.Name = "m_toolStripMenuItemWater";
            this.m_toolStripMenuItemWater.Size = new System.Drawing.Size(152, 22);
            this.m_toolStripMenuItemWater.Text = "生成水体";
            this.m_toolStripMenuItemWater.Click += new System.EventHandler(this.m_toolStripMenuItemWater_Click);
            // 
            // GridBaseAreaPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(197, 328);
            this.Controls.Add(this.m_textBoxGridCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_checkBoxInvisableWithBlock);
            this.Controls.Add(this.m_btnRemove);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.m_listBoxGridBaseRegion);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GridBaseAreaPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grid 区域相关工具";
            this.Enter += new System.EventHandler(this.GridBaseAreaPanel_Enter);
            this.Load += new System.EventHandler(this.GridBaseAreaPanel_Load);
            this.m_contextMenuStripGridBasedRegion.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox m_listBoxGridBaseRegion;
        private System.Windows.Forms.Button m_btnAdd;
        private System.Windows.Forms.Button m_btnRemove;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_textBoxGridCount;
        private static GridBaseAreaPanel ms_pInstance = new GridBaseAreaPanel();
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStripGridBasedRegion;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemGrow;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemSpray;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemVisableTerrain;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemHideTerrain;
        private System.Windows.Forms.CheckBox m_checkBoxInvisableWithBlock;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemWater;
    }
}