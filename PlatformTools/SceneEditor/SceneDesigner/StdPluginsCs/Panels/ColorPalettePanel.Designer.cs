﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    partial class ColorPalettePanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_ColorButtonFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.m_pictureBoxColorPreview = new System.Windows.Forms.PictureBox();
            this.m_textBoxColor = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.载入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxColorPreview)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnAdd.Location = new System.Drawing.Point(66, 330);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(152, 30);
            this.m_btnAdd.TabIndex = 1;
            this.m_btnAdd.Text = "添加颜色";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_ColorButtonFlowLayoutPanel
            // 
            this.m_ColorButtonFlowLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_ColorButtonFlowLayoutPanel.AutoScroll = true;
            this.m_ColorButtonFlowLayoutPanel.Location = new System.Drawing.Point(14, 58);
            this.m_ColorButtonFlowLayoutPanel.Name = "m_ColorButtonFlowLayoutPanel";
            this.m_ColorButtonFlowLayoutPanel.Size = new System.Drawing.Size(243, 270);
            this.m_ColorButtonFlowLayoutPanel.TabIndex = 3;
            // 
            // m_pictureBoxColorPreview
            // 
            this.m_pictureBoxColorPreview.BackColor = System.Drawing.Color.White;
            this.m_pictureBoxColorPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxColorPreview.Location = new System.Drawing.Point(40, 26);
            this.m_pictureBoxColorPreview.Name = "m_pictureBoxColorPreview";
            this.m_pictureBoxColorPreview.Size = new System.Drawing.Size(52, 23);
            this.m_pictureBoxColorPreview.TabIndex = 4;
            this.m_pictureBoxColorPreview.TabStop = false;
            // 
            // m_textBoxColor
            // 
            this.m_textBoxColor.Location = new System.Drawing.Point(116, 28);
            this.m_textBoxColor.Name = "m_textBoxColor";
            this.m_textBoxColor.Size = new System.Drawing.Size(93, 21);
            this.m_textBoxColor.TabIndex = 5;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(272, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.保存ToolStripMenuItem,
            this.载入ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.保存ToolStripMenuItem.Text = "保存";
            this.保存ToolStripMenuItem.Click += new System.EventHandler(this.保存ToolStripMenuItem_Click);
            // 
            // 载入ToolStripMenuItem
            // 
            this.载入ToolStripMenuItem.Name = "载入ToolStripMenuItem";
            this.载入ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.载入ToolStripMenuItem.Text = "载入";
            this.载入ToolStripMenuItem.Click += new System.EventHandler(this.载入ToolStripMenuItem_Click);
            // 
            // ColorPalettePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 360);
            this.Controls.Add(this.m_textBoxColor);
            this.Controls.Add(this.m_pictureBoxColorPreview);
            this.Controls.Add(this.m_ColorButtonFlowLayoutPanel);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ColorPalettePanel";
            this.Text = "颜色调色板";
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxColorPreview)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnAdd;
        private System.Windows.Forms.FlowLayoutPanel m_ColorButtonFlowLayoutPanel;
        private System.Windows.Forms.PictureBox m_pictureBoxColorPreview;
        private System.Windows.Forms.TextBox m_textBoxColor;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 载入ToolStripMenuItem;

    }
}