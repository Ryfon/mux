﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs
{
    partial class GenerateEntityPanel
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_comboBoxPalette = new System.Windows.Forms.ComboBox();
            this.m_listBoxAllTemplates = new System.Windows.Forms.ListBox();
            this.m_listBoxSelectedTemplates = new System.Windows.Forms.ListBox();
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_btnRemove = new System.Windows.Forms.Button();
            this.m_listBoxRate = new System.Windows.Forms.ListBox();
            this.m_textBoxRate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_btnPreview = new System.Windows.Forms.Button();
            this.m_btnGenerate = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxRandRotaBegin = new System.Windows.Forms.TextBox();
            this.m_textBoxRandRotaEnd = new System.Windows.Forms.TextBox();
            this.m_textBoxRandScaleEnd = new System.Windows.Forms.TextBox();
            this.m_textBoxRandScaleBegin = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_GetSelPals = new System.Windows.Forms.Button();
            this.btn_ClearList = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_comboBoxPalette
            // 
            this.m_comboBoxPalette.FormattingEnabled = true;
            this.m_comboBoxPalette.Location = new System.Drawing.Point(268, 266);
            this.m_comboBoxPalette.Name = "m_comboBoxPalette";
            this.m_comboBoxPalette.Size = new System.Drawing.Size(130, 20);
            this.m_comboBoxPalette.TabIndex = 0;
            this.m_comboBoxPalette.Visible = false;
            this.m_comboBoxPalette.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxPalette_SelectedIndexChanged);
            // 
            // m_listBoxAllTemplates
            // 
            this.m_listBoxAllTemplates.FormattingEnabled = true;
            this.m_listBoxAllTemplates.HorizontalScrollbar = true;
            this.m_listBoxAllTemplates.ItemHeight = 12;
            this.m_listBoxAllTemplates.Location = new System.Drawing.Point(14, 12);
            this.m_listBoxAllTemplates.Name = "m_listBoxAllTemplates";
            this.m_listBoxAllTemplates.Size = new System.Drawing.Size(183, 196);
            this.m_listBoxAllTemplates.TabIndex = 1;
            this.m_listBoxAllTemplates.SelectedIndexChanged += new System.EventHandler(this.m_listBoxAllTemplates_SelectedIndexChanged);
            this.m_listBoxAllTemplates.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_listBoxAllTemplates_KeyDown);
            // 
            // m_listBoxSelectedTemplates
            // 
            this.m_listBoxSelectedTemplates.FormattingEnabled = true;
            this.m_listBoxSelectedTemplates.HorizontalScrollbar = true;
            this.m_listBoxSelectedTemplates.ItemHeight = 12;
            this.m_listBoxSelectedTemplates.Location = new System.Drawing.Point(234, 12);
            this.m_listBoxSelectedTemplates.Name = "m_listBoxSelectedTemplates";
            this.m_listBoxSelectedTemplates.Size = new System.Drawing.Size(164, 196);
            this.m_listBoxSelectedTemplates.TabIndex = 2;
            this.m_listBoxSelectedTemplates.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSelectedTemplates_SelectedIndexChanged);
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Location = new System.Drawing.Point(203, 54);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(25, 24);
            this.m_btnAdd.TabIndex = 3;
            this.m_btnAdd.Text = "->";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_btnRemove
            // 
            this.m_btnRemove.Location = new System.Drawing.Point(203, 84);
            this.m_btnRemove.Name = "m_btnRemove";
            this.m_btnRemove.Size = new System.Drawing.Size(25, 24);
            this.m_btnRemove.TabIndex = 4;
            this.m_btnRemove.Text = "<-";
            this.m_btnRemove.UseVisualStyleBackColor = true;
            this.m_btnRemove.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // m_listBoxRate
            // 
            this.m_listBoxRate.FormattingEnabled = true;
            this.m_listBoxRate.HorizontalScrollbar = true;
            this.m_listBoxRate.ItemHeight = 12;
            this.m_listBoxRate.Location = new System.Drawing.Point(404, 12);
            this.m_listBoxRate.Name = "m_listBoxRate";
            this.m_listBoxRate.Size = new System.Drawing.Size(34, 196);
            this.m_listBoxRate.TabIndex = 5;
            // 
            // m_textBoxRate
            // 
            this.m_textBoxRate.Location = new System.Drawing.Point(59, 245);
            this.m_textBoxRate.Name = "m_textBoxRate";
            this.m_textBoxRate.Size = new System.Drawing.Size(34, 21);
            this.m_textBoxRate.TabIndex = 6;
            this.m_textBoxRate.Text = "10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 248);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "生长率";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(99, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "%";
            // 
            // m_btnPreview
            // 
            this.m_btnPreview.Location = new System.Drawing.Point(245, 295);
            this.m_btnPreview.Name = "m_btnPreview";
            this.m_btnPreview.Size = new System.Drawing.Size(70, 25);
            this.m_btnPreview.TabIndex = 9;
            this.m_btnPreview.Text = "随机预览";
            this.m_btnPreview.UseVisualStyleBackColor = true;
            this.m_btnPreview.Click += new System.EventHandler(this.m_btnPreview_Click);
            // 
            // m_btnGenerate
            // 
            this.m_btnGenerate.Location = new System.Drawing.Point(331, 295);
            this.m_btnGenerate.Name = "m_btnGenerate";
            this.m_btnGenerate.Size = new System.Drawing.Size(70, 25);
            this.m_btnGenerate.TabIndex = 10;
            this.m_btnGenerate.Text = "确定生成";
            this.m_btnGenerate.UseVisualStyleBackColor = true;
            this.m_btnGenerate.Click += new System.EventHandler(this.m_btnGenerate_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 274);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 11;
            this.label3.Text = "随机角度范围";
            // 
            // m_textBoxRandRotaBegin
            // 
            this.m_textBoxRandRotaBegin.Location = new System.Drawing.Point(101, 270);
            this.m_textBoxRandRotaBegin.Name = "m_textBoxRandRotaBegin";
            this.m_textBoxRandRotaBegin.Size = new System.Drawing.Size(54, 21);
            this.m_textBoxRandRotaBegin.TabIndex = 12;
            this.m_textBoxRandRotaBegin.Text = "0";
            // 
            // m_textBoxRandRotaEnd
            // 
            this.m_textBoxRandRotaEnd.Location = new System.Drawing.Point(161, 270);
            this.m_textBoxRandRotaEnd.Name = "m_textBoxRandRotaEnd";
            this.m_textBoxRandRotaEnd.Size = new System.Drawing.Size(54, 21);
            this.m_textBoxRandRotaEnd.TabIndex = 13;
            this.m_textBoxRandRotaEnd.Text = "6.2832";
            // 
            // m_textBoxRandScaleEnd
            // 
            this.m_textBoxRandScaleEnd.Location = new System.Drawing.Point(161, 297);
            this.m_textBoxRandScaleEnd.Name = "m_textBoxRandScaleEnd";
            this.m_textBoxRandScaleEnd.Size = new System.Drawing.Size(54, 21);
            this.m_textBoxRandScaleEnd.TabIndex = 16;
            this.m_textBoxRandScaleEnd.Text = "2.0";
            // 
            // m_textBoxRandScaleBegin
            // 
            this.m_textBoxRandScaleBegin.Location = new System.Drawing.Point(101, 297);
            this.m_textBoxRandScaleBegin.Name = "m_textBoxRandScaleBegin";
            this.m_textBoxRandScaleBegin.Size = new System.Drawing.Size(54, 21);
            this.m_textBoxRandScaleBegin.TabIndex = 15;
            this.m_textBoxRandScaleBegin.Text = "0.5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 301);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 14;
            this.label4.Text = "随机缩放范围";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "弧度";
            // 
            // btn_GetSelPals
            // 
            this.btn_GetSelPals.Location = new System.Drawing.Point(14, 214);
            this.btn_GetSelPals.Name = "btn_GetSelPals";
            this.btn_GetSelPals.Size = new System.Drawing.Size(69, 23);
            this.btn_GetSelPals.TabIndex = 18;
            this.btn_GetSelPals.Text = "获取物件";
            this.btn_GetSelPals.UseVisualStyleBackColor = true;
            this.btn_GetSelPals.Click += new System.EventHandler(this.btn_GetSelPals_Click);
            // 
            // btn_ClearList
            // 
            this.btn_ClearList.Location = new System.Drawing.Point(117, 214);
            this.btn_ClearList.Name = "btn_ClearList";
            this.btn_ClearList.Size = new System.Drawing.Size(75, 23);
            this.btn_ClearList.TabIndex = 19;
            this.btn_ClearList.Text = "清理所有";
            this.btn_ClearList.UseVisualStyleBackColor = true;
            this.btn_ClearList.Click += new System.EventHandler(this.btn_ClearList_Click);
            // 
            // GenerateEntityPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 332);
            this.Controls.Add(this.btn_ClearList);
            this.Controls.Add(this.btn_GetSelPals);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_textBoxRandScaleEnd);
            this.Controls.Add(this.m_textBoxRandScaleBegin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_textBoxRandRotaEnd);
            this.Controls.Add(this.m_textBoxRandRotaBegin);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_btnGenerate);
            this.Controls.Add(this.m_btnPreview);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_textBoxRate);
            this.Controls.Add(this.m_listBoxRate);
            this.Controls.Add(this.m_btnRemove);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.m_listBoxSelectedTemplates);
            this.Controls.Add(this.m_listBoxAllTemplates);
            this.Controls.Add(this.m_comboBoxPalette);
            this.Name = "GenerateEntityPanel";
            this.Text = "物件生长工具";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.GenerateEntityPanel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox m_comboBoxPalette;
        private System.Windows.Forms.ListBox m_listBoxAllTemplates;
        private System.Windows.Forms.ListBox m_listBoxSelectedTemplates;
        private System.Windows.Forms.Button m_btnAdd;
        private System.Windows.Forms.Button m_btnRemove;
        private System.Windows.Forms.ListBox m_listBoxRate;
        private System.Windows.Forms.TextBox m_textBoxRate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button m_btnPreview;
        private System.Windows.Forms.Button m_btnGenerate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxRandRotaBegin;
        private System.Windows.Forms.TextBox m_textBoxRandRotaEnd;
        private System.Windows.Forms.TextBox m_textBoxRandScaleEnd;
        private System.Windows.Forms.TextBox m_textBoxRandScaleBegin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_GetSelPals;
        private System.Windows.Forms.Button btn_ClearList;
    }
}