﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Panels
{
    partial class ModelScreenshot
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_textBoxTexWidth = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxTexHeight = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_textBoxModleDir = new System.Windows.Forms.TextBox();
            this.m_btnModelBrowser = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.m_textBoxExportPath = new System.Windows.Forms.TextBox();
            this.m_btnExportBrowser = new System.Windows.Forms.Button();
            this.m_btnDoExport = new System.Windows.Forms.Button();
            this.m_checkBoxExprotSameDir = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_textBoxCamerX = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.m_textBoxCamerY = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_textBoxCamerZ = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.m_btnSetCamera = new System.Windows.Forms.Button();
            this.m_checkBoxAddExport = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // m_textBoxTexWidth
            // 
            this.m_textBoxTexWidth.Location = new System.Drawing.Point(47, 41);
            this.m_textBoxTexWidth.MaxLength = 10;
            this.m_textBoxTexWidth.Name = "m_textBoxTexWidth";
            this.m_textBoxTexWidth.Size = new System.Drawing.Size(40, 21);
            this.m_textBoxTexWidth.TabIndex = 0;
            this.m_textBoxTexWidth.Text = "256";
            this.m_textBoxTexWidth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxTexWidth_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "纹理大小";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "宽度";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "高度";
            // 
            // m_textBoxTexHeight
            // 
            this.m_textBoxTexHeight.Location = new System.Drawing.Point(140, 41);
            this.m_textBoxTexHeight.MaxLength = 10;
            this.m_textBoxTexHeight.Name = "m_textBoxTexHeight";
            this.m_textBoxTexHeight.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxTexHeight.TabIndex = 4;
            this.m_textBoxTexHeight.Text = "256";
            this.m_textBoxTexHeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_textBoxTexHeight_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "模型目录";
            // 
            // m_textBoxModleDir
            // 
            this.m_textBoxModleDir.Location = new System.Drawing.Point(12, 140);
            this.m_textBoxModleDir.Name = "m_textBoxModleDir";
            this.m_textBoxModleDir.Size = new System.Drawing.Size(209, 21);
            this.m_textBoxModleDir.TabIndex = 6;
            // 
            // m_btnModelBrowser
            // 
            this.m_btnModelBrowser.Location = new System.Drawing.Point(227, 138);
            this.m_btnModelBrowser.Name = "m_btnModelBrowser";
            this.m_btnModelBrowser.Size = new System.Drawing.Size(54, 23);
            this.m_btnModelBrowser.TabIndex = 7;
            this.m_btnModelBrowser.Text = "浏览...";
            this.m_btnModelBrowser.UseVisualStyleBackColor = true;
            this.m_btnModelBrowser.Click += new System.EventHandler(this.m_btnModelBrowser_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "导出目录";
            // 
            // m_textBoxExportPath
            // 
            this.m_textBoxExportPath.Location = new System.Drawing.Point(14, 193);
            this.m_textBoxExportPath.Name = "m_textBoxExportPath";
            this.m_textBoxExportPath.Size = new System.Drawing.Size(207, 21);
            this.m_textBoxExportPath.TabIndex = 9;
            // 
            // m_btnExportBrowser
            // 
            this.m_btnExportBrowser.Location = new System.Drawing.Point(227, 191);
            this.m_btnExportBrowser.Name = "m_btnExportBrowser";
            this.m_btnExportBrowser.Size = new System.Drawing.Size(54, 23);
            this.m_btnExportBrowser.TabIndex = 10;
            this.m_btnExportBrowser.Text = "浏览...";
            this.m_btnExportBrowser.UseVisualStyleBackColor = true;
            this.m_btnExportBrowser.Click += new System.EventHandler(this.m_btnExportBrowser_Click);
            // 
            // m_btnDoExport
            // 
            this.m_btnDoExport.Location = new System.Drawing.Point(159, 233);
            this.m_btnDoExport.Name = "m_btnDoExport";
            this.m_btnDoExport.Size = new System.Drawing.Size(75, 23);
            this.m_btnDoExport.TabIndex = 11;
            this.m_btnDoExport.Text = "导出截图";
            this.m_btnDoExport.UseVisualStyleBackColor = true;
            this.m_btnDoExport.Click += new System.EventHandler(this.m_btnDoExport_Click);
            // 
            // m_checkBoxExprotSameDir
            // 
            this.m_checkBoxExprotSameDir.AutoSize = true;
            this.m_checkBoxExprotSameDir.Checked = true;
            this.m_checkBoxExprotSameDir.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxExprotSameDir.Location = new System.Drawing.Point(38, 237);
            this.m_checkBoxExprotSameDir.Name = "m_checkBoxExprotSameDir";
            this.m_checkBoxExprotSameDir.Size = new System.Drawing.Size(96, 16);
            this.m_checkBoxExprotSameDir.TabIndex = 12;
            this.m_checkBoxExprotSameDir.Text = "与模型同目录";
            this.m_checkBoxExprotSameDir.UseVisualStyleBackColor = true;
            this.m_checkBoxExprotSameDir.CheckedChanged += new System.EventHandler(this.m_checkBoxExprotSameDir_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "摄像机角度";
            // 
            // m_textBoxCamerX
            // 
            this.m_textBoxCamerX.Location = new System.Drawing.Point(14, 91);
            this.m_textBoxCamerX.Name = "m_textBoxCamerX";
            this.m_textBoxCamerX.Size = new System.Drawing.Size(27, 21);
            this.m_textBoxCamerX.TabIndex = 14;
            this.m_textBoxCamerX.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "x";
            // 
            // m_textBoxCamerY
            // 
            this.m_textBoxCamerY.Location = new System.Drawing.Point(73, 91);
            this.m_textBoxCamerY.Name = "m_textBoxCamerY";
            this.m_textBoxCamerY.Size = new System.Drawing.Size(24, 21);
            this.m_textBoxCamerY.TabIndex = 16;
            this.m_textBoxCamerY.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(103, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 12);
            this.label8.TabIndex = 17;
            this.label8.Text = "y";
            // 
            // m_textBoxCamerZ
            // 
            this.m_textBoxCamerZ.Location = new System.Drawing.Point(131, 91);
            this.m_textBoxCamerZ.Name = "m_textBoxCamerZ";
            this.m_textBoxCamerZ.Size = new System.Drawing.Size(28, 21);
            this.m_textBoxCamerZ.TabIndex = 18;
            this.m_textBoxCamerZ.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(165, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 12);
            this.label9.TabIndex = 19;
            this.label9.Text = "z";
            // 
            // m_btnSetCamera
            // 
            this.m_btnSetCamera.Location = new System.Drawing.Point(182, 91);
            this.m_btnSetCamera.Name = "m_btnSetCamera";
            this.m_btnSetCamera.Size = new System.Drawing.Size(75, 23);
            this.m_btnSetCamera.TabIndex = 20;
            this.m_btnSetCamera.Text = "设置";
            this.m_btnSetCamera.UseVisualStyleBackColor = true;
            this.m_btnSetCamera.Click += new System.EventHandler(this.m_btnSetCamera_Click);
            // 
            // m_checkBoxAddExport
            // 
            this.m_checkBoxAddExport.AutoSize = true;
            this.m_checkBoxAddExport.Checked = true;
            this.m_checkBoxAddExport.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_checkBoxAddExport.Location = new System.Drawing.Point(38, 269);
            this.m_checkBoxAddExport.Name = "m_checkBoxAddExport";
            this.m_checkBoxAddExport.Size = new System.Drawing.Size(96, 16);
            this.m_checkBoxAddExport.TabIndex = 21;
            this.m_checkBoxAddExport.Text = "仅处理新文件";
            this.m_checkBoxAddExport.UseVisualStyleBackColor = true;
            // 
            // ModelScreenshot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 316);
            this.Controls.Add(this.m_checkBoxAddExport);
            this.Controls.Add(this.m_btnSetCamera);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.m_textBoxCamerZ);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.m_textBoxCamerY);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.m_textBoxCamerX);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.m_checkBoxExprotSameDir);
            this.Controls.Add(this.m_btnDoExport);
            this.Controls.Add(this.m_btnExportBrowser);
            this.Controls.Add(this.m_textBoxExportPath);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_btnModelBrowser);
            this.Controls.Add(this.m_textBoxModleDir);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_textBoxTexHeight);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_textBoxTexWidth);
            this.Name = "ModelScreenshot";
            this.Text = "模型截图导出工具";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_textBoxTexWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxTexHeight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox m_textBoxModleDir;
        private System.Windows.Forms.Button m_btnModelBrowser;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox m_textBoxExportPath;
        private System.Windows.Forms.Button m_btnExportBrowser;
        private System.Windows.Forms.Button m_btnDoExport;
        private System.Windows.Forms.CheckBox m_checkBoxExprotSameDir;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox m_textBoxCamerX;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox m_textBoxCamerY;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox m_textBoxCamerZ;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button m_btnSetCamera;
        private System.Windows.Forms.CheckBox m_checkBoxAddExport;
    }
}