﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Controls
{
    partial class MaterialPropertyEditorCtrl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.m_tbShineness = new System.Windows.Forms.TextBox();
            this.m_tbAlpha = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_tbAmbR = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_btnAmbientColor = new System.Windows.Forms.Button();
            this.m_tbAmbG = new System.Windows.Forms.TextBox();
            this.m_tbAmbB = new System.Windows.Forms.TextBox();
            this.m_tbDifB = new System.Windows.Forms.TextBox();
            this.m_tbDifG = new System.Windows.Forms.TextBox();
            this.m_btnDiffuseColor = new System.Windows.Forms.Button();
            this.m_tbDifR = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_tbSpeB = new System.Windows.Forms.TextBox();
            this.m_tbSpeG = new System.Windows.Forms.TextBox();
            this.m_btnSpecularColor = new System.Windows.Forms.Button();
            this.m_tbSpeR = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_tbEmiB = new System.Windows.Forms.TextBox();
            this.m_tbEmiG = new System.Windows.Forms.TextBox();
            this.m_btnEmissiveColor = new System.Windows.Forms.Button();
            this.m_tbEmiR = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Shineness";
            // 
            // m_tbShineness
            // 
            this.m_tbShineness.Location = new System.Drawing.Point(82, 21);
            this.m_tbShineness.Name = "m_tbShineness";
            this.m_tbShineness.Size = new System.Drawing.Size(52, 21);
            this.m_tbShineness.TabIndex = 1;
            // 
            // m_tbAlpha
            // 
            this.m_tbAlpha.Location = new System.Drawing.Point(82, 48);
            this.m_tbAlpha.Name = "m_tbAlpha";
            this.m_tbAlpha.Size = new System.Drawing.Size(52, 21);
            this.m_tbAlpha.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Alpha";
            // 
            // m_tbAmbR
            // 
            this.m_tbAmbR.Location = new System.Drawing.Point(146, 79);
            this.m_tbAmbR.Name = "m_tbAmbR";
            this.m_tbAmbR.Size = new System.Drawing.Size(42, 21);
            this.m_tbAmbR.TabIndex = 5;
            this.m_tbAmbR.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ambient Color:";
            // 
            // m_btnAmbientColor
            // 
            this.m_btnAmbientColor.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.m_btnAmbientColor.Location = new System.Drawing.Point(109, 77);
            this.m_btnAmbientColor.Name = "m_btnAmbientColor";
            this.m_btnAmbientColor.Size = new System.Drawing.Size(25, 23);
            this.m_btnAmbientColor.TabIndex = 6;
            this.m_btnAmbientColor.UseVisualStyleBackColor = false;
            this.m_btnAmbientColor.Click += new System.EventHandler(this.m_btnAmbientColor_Click);
            // 
            // m_tbAmbG
            // 
            this.m_tbAmbG.Location = new System.Drawing.Point(195, 79);
            this.m_tbAmbG.Name = "m_tbAmbG";
            this.m_tbAmbG.Size = new System.Drawing.Size(42, 21);
            this.m_tbAmbG.TabIndex = 7;
            this.m_tbAmbG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // m_tbAmbB
            // 
            this.m_tbAmbB.Location = new System.Drawing.Point(247, 79);
            this.m_tbAmbB.Name = "m_tbAmbB";
            this.m_tbAmbB.Size = new System.Drawing.Size(42, 21);
            this.m_tbAmbB.TabIndex = 8;
            this.m_tbAmbB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // m_tbDifB
            // 
            this.m_tbDifB.Location = new System.Drawing.Point(247, 108);
            this.m_tbDifB.Name = "m_tbDifB";
            this.m_tbDifB.Size = new System.Drawing.Size(42, 21);
            this.m_tbDifB.TabIndex = 13;
            this.m_tbDifB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // m_tbDifG
            // 
            this.m_tbDifG.Location = new System.Drawing.Point(195, 108);
            this.m_tbDifG.Name = "m_tbDifG";
            this.m_tbDifG.Size = new System.Drawing.Size(42, 21);
            this.m_tbDifG.TabIndex = 12;
            this.m_tbDifG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // m_btnDiffuseColor
            // 
            this.m_btnDiffuseColor.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.m_btnDiffuseColor.Location = new System.Drawing.Point(109, 106);
            this.m_btnDiffuseColor.Name = "m_btnDiffuseColor";
            this.m_btnDiffuseColor.Size = new System.Drawing.Size(25, 23);
            this.m_btnDiffuseColor.TabIndex = 11;
            this.m_btnDiffuseColor.UseVisualStyleBackColor = false;
            this.m_btnDiffuseColor.Click += new System.EventHandler(this.m_btnDiffuseColor_Click);
            // 
            // m_tbDifR
            // 
            this.m_tbDifR.Location = new System.Drawing.Point(146, 108);
            this.m_tbDifR.Name = "m_tbDifR";
            this.m_tbDifR.Size = new System.Drawing.Size(42, 21);
            this.m_tbDifR.TabIndex = 10;
            this.m_tbDifR.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "Diffuse Color:";
            // 
            // m_tbSpeB
            // 
            this.m_tbSpeB.Location = new System.Drawing.Point(247, 137);
            this.m_tbSpeB.Name = "m_tbSpeB";
            this.m_tbSpeB.Size = new System.Drawing.Size(42, 21);
            this.m_tbSpeB.TabIndex = 18;
            this.m_tbSpeB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // m_tbSpeG
            // 
            this.m_tbSpeG.Location = new System.Drawing.Point(195, 137);
            this.m_tbSpeG.Name = "m_tbSpeG";
            this.m_tbSpeG.Size = new System.Drawing.Size(42, 21);
            this.m_tbSpeG.TabIndex = 17;
            this.m_tbSpeG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // m_btnSpecularColor
            // 
            this.m_btnSpecularColor.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.m_btnSpecularColor.Location = new System.Drawing.Point(109, 135);
            this.m_btnSpecularColor.Name = "m_btnSpecularColor";
            this.m_btnSpecularColor.Size = new System.Drawing.Size(25, 23);
            this.m_btnSpecularColor.TabIndex = 16;
            this.m_btnSpecularColor.UseVisualStyleBackColor = false;
            this.m_btnSpecularColor.Click += new System.EventHandler(this.m_btnSpecularColor_Click);
            // 
            // m_tbSpeR
            // 
            this.m_tbSpeR.Location = new System.Drawing.Point(146, 137);
            this.m_tbSpeR.Name = "m_tbSpeR";
            this.m_tbSpeR.Size = new System.Drawing.Size(42, 21);
            this.m_tbSpeR.TabIndex = 15;
            this.m_tbSpeR.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "Specular Color:";
            // 
            // m_tbEmiB
            // 
            this.m_tbEmiB.Location = new System.Drawing.Point(247, 166);
            this.m_tbEmiB.Name = "m_tbEmiB";
            this.m_tbEmiB.Size = new System.Drawing.Size(42, 21);
            this.m_tbEmiB.TabIndex = 23;
            this.m_tbEmiB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // m_tbEmiG
            // 
            this.m_tbEmiG.Location = new System.Drawing.Point(195, 166);
            this.m_tbEmiG.Name = "m_tbEmiG";
            this.m_tbEmiG.Size = new System.Drawing.Size(42, 21);
            this.m_tbEmiG.TabIndex = 22;
            this.m_tbEmiG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // m_btnEmissiveColor
            // 
            this.m_btnEmissiveColor.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.m_btnEmissiveColor.Location = new System.Drawing.Point(109, 164);
            this.m_btnEmissiveColor.Name = "m_btnEmissiveColor";
            this.m_btnEmissiveColor.Size = new System.Drawing.Size(25, 23);
            this.m_btnEmissiveColor.TabIndex = 21;
            this.m_btnEmissiveColor.UseVisualStyleBackColor = false;
            this.m_btnEmissiveColor.Click += new System.EventHandler(this.m_btnEmissiveColor_Click);
            // 
            // m_tbEmiR
            // 
            this.m_tbEmiR.Location = new System.Drawing.Point(146, 166);
            this.m_tbEmiR.Name = "m_tbEmiR";
            this.m_tbEmiR.Size = new System.Drawing.Size(42, 21);
            this.m_tbEmiR.TabIndex = 20;
            this.m_tbEmiR.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ColorTextBox_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 12);
            this.label6.TabIndex = 19;
            this.label6.Text = "Emissive Color:";
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(113, 214);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 23);
            this.m_btnOK.TabIndex = 24;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // MaterialPropertyEditorCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_tbEmiB);
            this.Controls.Add(this.m_tbEmiG);
            this.Controls.Add(this.m_btnEmissiveColor);
            this.Controls.Add(this.m_tbEmiR);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.m_tbSpeB);
            this.Controls.Add(this.m_tbSpeG);
            this.Controls.Add(this.m_btnSpecularColor);
            this.Controls.Add(this.m_tbSpeR);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_tbDifB);
            this.Controls.Add(this.m_tbDifG);
            this.Controls.Add(this.m_btnDiffuseColor);
            this.Controls.Add(this.m_tbDifR);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_tbAmbB);
            this.Controls.Add(this.m_tbAmbG);
            this.Controls.Add(this.m_btnAmbientColor);
            this.Controls.Add(this.m_tbAmbR);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_tbAlpha);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_tbShineness);
            this.Controls.Add(this.label1);
            this.Name = "MaterialPropertyEditorCtrl";
            this.Size = new System.Drawing.Size(318, 270);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_tbShineness;
        private System.Windows.Forms.TextBox m_tbAlpha;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_tbAmbR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button m_btnAmbientColor;
        private System.Windows.Forms.TextBox m_tbAmbG;
        private System.Windows.Forms.TextBox m_tbAmbB;
        private System.Windows.Forms.TextBox m_tbDifB;
        private System.Windows.Forms.TextBox m_tbDifG;
        private System.Windows.Forms.Button m_btnDiffuseColor;
        private System.Windows.Forms.TextBox m_tbDifR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox m_tbSpeB;
        private System.Windows.Forms.TextBox m_tbSpeG;
        private System.Windows.Forms.Button m_btnSpecularColor;
        private System.Windows.Forms.TextBox m_tbSpeR;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox m_tbEmiB;
        private System.Windows.Forms.TextBox m_tbEmiG;
        private System.Windows.Forms.Button m_btnEmissiveColor;
        private System.Windows.Forms.TextBox m_tbEmiR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button m_btnOK;
    }
}
