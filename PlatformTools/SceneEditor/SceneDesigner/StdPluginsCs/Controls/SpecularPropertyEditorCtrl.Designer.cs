﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Controls
{
    partial class SpecularPropertyEditorCtrl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_cbSpecular = new System.Windows.Forms.CheckBox();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_cbSpecular
            // 
            this.m_cbSpecular.AutoSize = true;
            this.m_cbSpecular.Location = new System.Drawing.Point(20, 18);
            this.m_cbSpecular.Name = "m_cbSpecular";
            this.m_cbSpecular.Size = new System.Drawing.Size(72, 16);
            this.m_cbSpecular.TabIndex = 0;
            this.m_cbSpecular.Text = "Specular";
            this.m_cbSpecular.UseVisualStyleBackColor = true;
            // 
            // m_btnOK
            // 
            this.m_btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnOK.Location = new System.Drawing.Point(139, 93);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 23);
            this.m_btnOK.TabIndex = 1;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // SpecularPropertyEditorCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_cbSpecular);
            this.Name = "SpecularPropertyEditorCtrl";
            this.Size = new System.Drawing.Size(228, 129);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox m_cbSpecular;
        private System.Windows.Forms.Button m_btnOK;
    }
}
