﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Controls
{

    public partial class AlphaPropertyEditorCtrl : UserControl
    {
        protected MAlphaProperty m_TagProperty; // 修改的目标属性

        public AlphaPropertyEditorCtrl(MAlphaProperty tagProp)
        {
            InitializeComponent();
            m_TagProperty = tagProp;

            // 初始化 AlphaFunc ComboBox
            String[] aAlphaFuns = MAlphaProperty.GetAllAlphaFuns();
            foreach(String strAlphaFun in aAlphaFuns)
            {
                m_cbSrcBlendMode.Items.Add(strAlphaFun);
                m_cbDestBlendMode.Items.Add(strAlphaFun);
            }

            SyncPropertyToUI();
        }

        // 将 TagProperty 中的值同步到 UI
        protected void SyncPropertyToUI()
        {

            m_cbAlphaBlending.Checked = m_TagProperty.AlphaBlending;

            m_cbSrcBlendMode.SelectedItem = m_TagProperty.SrcAlphaFun;
            m_cbDestBlendMode.SelectedItem = m_TagProperty.DestAlphaFun;

            m_cbAlphaTesting.Checked = m_TagProperty.AlphaTesting;
            m_tbTestRef.Text = m_TagProperty.TestRef.ToString();

        }

        // 将 UI 的值同步到 m_TagProperty 中
        protected void SyncUIToProperty()
        {
            try
            {
                m_TagProperty.AlphaBlending = m_cbAlphaBlending.Checked;

                m_TagProperty.SrcAlphaFun = m_cbSrcBlendMode.SelectedItem as String;
                m_TagProperty.DestAlphaFun = m_cbDestBlendMode.SelectedItem as String;

                m_TagProperty.AlphaTesting = m_cbAlphaTesting.Checked;
                m_TagProperty.TestRef = int.Parse(m_tbTestRef.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            SyncUIToProperty();
            m_TagProperty.UpdateProperties();
        }
    }
}
