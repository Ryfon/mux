﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Controls
{
    public partial class SpecularPropertyEditorCtrl : UserControl
    {
        protected MSpecularProperty m_TagSpecularProperty;

        public SpecularPropertyEditorCtrl(MSpecularProperty specularProp)
        {
            InitializeComponent();

            m_TagSpecularProperty = specularProp;
            m_cbSpecular.Checked = m_TagSpecularProperty.Specular;
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            if (m_TagSpecularProperty != null)
            {
                m_TagSpecularProperty.Specular = m_cbSpecular.Checked;
                m_TagSpecularProperty.UpdateProperties();
            }
        }
    }
}
