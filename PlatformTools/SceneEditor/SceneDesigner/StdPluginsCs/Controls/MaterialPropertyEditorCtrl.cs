﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Emergent.Gamebryo.SceneDesigner.Framework;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Controls
{
    public partial class MaterialPropertyEditorCtrl : UserControl
    {
        protected MMaterialProperty m_TagMaterialProperty;

        public MaterialPropertyEditorCtrl(MMaterialProperty matProp)
        {
            InitializeComponent();

            m_TagMaterialProperty = matProp;
            if (m_TagMaterialProperty != null)
            {
                m_tbShineness.Text = matProp.Shineness.ToString("#0.00");
                m_tbAlpha.Text = matProp.Alpha.ToString("#0.00");

                m_btnAmbientColor.BackColor 
                    = Color.FromArgb(255, 
                    (int)(matProp.Ambient.X * 255),
                    (int)(matProp.Ambient.Y * 255), 
                    (int)(matProp.Ambient.Z * 255));

                m_btnDiffuseColor.BackColor
                    = Color.FromArgb(255,
                    (int)(matProp.Diffuse.X * 255),
                    (int)(matProp.Diffuse.Y * 255),
                    (int)(matProp.Diffuse.Z * 255));

                m_btnSpecularColor.BackColor
                    = Color.FromArgb(255,
                    (int)(matProp.Specular.X * 255),
                    (int)(matProp.Specular.Y * 255),
                    (int)(matProp.Specular.Z * 255));

                m_btnEmissiveColor.BackColor
                    = Color.FromArgb(255,
                    (int)(matProp.Emittance.X * 255),
                    (int)(matProp.Emittance.Y * 255),
                    (int)(matProp.Emittance.Z * 255));

                SyncColorToTextBox();
            }
         }

        protected void SyncColorToTextBox()
        {
            // 将颜色同步到颜色 text box

            m_tbAmbR.Text = m_btnAmbientColor.BackColor.R.ToString();
            m_tbAmbG.Text = m_btnAmbientColor.BackColor.G.ToString();
            m_tbAmbB.Text = m_btnAmbientColor.BackColor.B.ToString();

            m_tbDifR.Text = m_btnDiffuseColor.BackColor.R.ToString();
            m_tbDifG.Text = m_btnDiffuseColor.BackColor.G.ToString();
            m_tbDifB.Text = m_btnDiffuseColor.BackColor.B.ToString();

            m_tbSpeR.Text = m_btnSpecularColor.BackColor.R.ToString();
            m_tbSpeG.Text = m_btnSpecularColor.BackColor.G.ToString();
            m_tbSpeB.Text = m_btnSpecularColor.BackColor.B.ToString();

            m_tbEmiR.Text = m_btnEmissiveColor.BackColor.R.ToString();
            m_tbEmiG.Text = m_btnEmissiveColor.BackColor.G.ToString();
            m_tbEmiB.Text = m_btnEmissiveColor.BackColor.B.ToString();
        }

        protected void SyncTextBoxToColor()
        {
            // 将 text box 中的值同步到颜色
            try
            {
                m_btnAmbientColor.BackColor
                     = Color.FromArgb(255,
                     int.Parse(m_tbAmbR.Text),
                     int.Parse(m_tbAmbG.Text),
                     int.Parse(m_tbAmbB.Text));

                m_btnDiffuseColor.BackColor
                    = Color.FromArgb(255,
                     int.Parse(m_tbDifR.Text),
                     int.Parse(m_tbDifG.Text),
                     int.Parse(m_tbDifB.Text));

                m_btnSpecularColor.BackColor
                    = Color.FromArgb(255,
                     int.Parse(m_tbSpeR.Text),
                     int.Parse(m_tbSpeG.Text),
                     int.Parse(m_tbSpeB.Text));

                m_btnEmissiveColor.BackColor
                    = Color.FromArgb(255,
                     int.Parse(m_tbEmiR.Text),
                     int.Parse(m_tbEmiG.Text),
                     int.Parse(m_tbEmiB.Text));
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
  
        }

        // 材质属性
        public MPoint3 Ambient
        {
            get
            {
                try
                {
                    MPoint3 color = new MPoint3();
                    color.X = float.Parse(m_tbAmbR.Text) / 255.0f;
                    color.Y = float.Parse(m_tbAmbG.Text) / 255.0f;
                    color.Z = float.Parse(m_tbAmbB.Text) / 255.0f;
                    return color;
                
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return null;
                }
            }
        }

        public MPoint3 Diffuse
        {
            get 
            {
                try
                {
                    MPoint3 color = new MPoint3();
                    color.X = float.Parse(m_tbDifR.Text) / 255.0f;
                    color.Y = float.Parse(m_tbDifG.Text) / 255.0f;
                    color.Z = float.Parse(m_tbDifB.Text) / 255.0f;
                    return color;

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return null;
                }
            }
        }

        public MPoint3 Specular
        {
            get 
            { 
                try
                {
                    MPoint3 color = new MPoint3();
                    color.X = float.Parse(m_tbSpeR.Text) / 255.0f;
                    color.Y = float.Parse(m_tbSpeG.Text) / 255.0f;
                    color.Z = float.Parse(m_tbSpeB.Text) / 255.0f;
                    return color;

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return null;
                }
            }
        }

        public MPoint3 Emissive
        {
            get
            {
                try
                {
                    MPoint3 color = new MPoint3();
                    color.X = float.Parse(m_tbEmiR.Text) / 255.0f;
                    color.Y = float.Parse(m_tbEmiG.Text) / 255.0f;
                    color.Z = float.Parse(m_tbEmiB.Text) / 255.0f;
                    return color;

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return null;
                }
            }
        }

        public float Shininess
        {
            get 
            { 
                try
                {
                    return float.Parse(m_tbShineness.Text); 
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return -1.0f;
                }

            }
        }

        public float Alpha
        {
            get
            {
                try
                {
                    return float.Parse(m_tbAlpha.Text);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return -1.0f;
                }

            }
        }

        private Color OpenColorDialog(Color color)
        {
            ColorDialog colorDlg = new ColorDialog();
            colorDlg.Color = color;

            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                return colorDlg.Color;
            }
            else
            {
                return color;
            }
        }

        private void m_btnAmbientColor_Click(object sender, EventArgs e)
        {
            // 选择 ambient 色
            m_btnAmbientColor.BackColor = OpenColorDialog(m_btnAmbientColor.BackColor);
            SyncColorToTextBox();
            
        }

        private void m_btnDiffuseColor_Click(object sender, EventArgs e)
        {
            m_btnDiffuseColor.BackColor = OpenColorDialog(m_btnDiffuseColor.BackColor);
            SyncColorToTextBox();
        }

        private void m_btnSpecularColor_Click(object sender, EventArgs e)
        {
            m_btnSpecularColor.BackColor = OpenColorDialog(m_btnSpecularColor.BackColor);
            SyncColorToTextBox();
        }

        private void m_btnEmissiveColor_Click(object sender, EventArgs e)
        {
            m_btnEmissiveColor.BackColor = OpenColorDialog(m_btnEmissiveColor.BackColor);
            SyncColorToTextBox();
        }

        private void m_ColorTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SyncTextBoxToColor();
            }
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            // 应用到对应结点
            m_TagMaterialProperty.Alpha = Alpha;
            m_TagMaterialProperty.Shineness = Shininess;
            m_TagMaterialProperty.Ambient = Ambient;
            m_TagMaterialProperty.Diffuse = Diffuse;
            m_TagMaterialProperty.Specular = Specular;
            m_TagMaterialProperty.Emittance = Emissive;

            m_TagMaterialProperty.UpdateProperties();

        }

        private void m_btnSaveToNif_Click(object sender, EventArgs e)
        {
        }

    }
}
