﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Controls
{
    partial class AlphaPropertyEditorCtrl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_cbAlphaBlending = new System.Windows.Forms.CheckBox();
            this.m_cbAlphaTesting = new System.Windows.Forms.CheckBox();
            this.m_gbAlphaBlending = new System.Windows.Forms.GroupBox();
            this.m_gbAlphaTesting = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_cbSrcBlendMode = new System.Windows.Forms.ComboBox();
            this.m_cbDestBlendMode = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_tbTestRef = new System.Windows.Forms.TextBox();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_gbAlphaBlending.SuspendLayout();
            this.m_gbAlphaTesting.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_cbAlphaBlending
            // 
            this.m_cbAlphaBlending.AutoSize = true;
            this.m_cbAlphaBlending.Location = new System.Drawing.Point(16, 20);
            this.m_cbAlphaBlending.Name = "m_cbAlphaBlending";
            this.m_cbAlphaBlending.Size = new System.Drawing.Size(108, 16);
            this.m_cbAlphaBlending.TabIndex = 0;
            this.m_cbAlphaBlending.Text = "Alpha Blending";
            this.m_cbAlphaBlending.UseVisualStyleBackColor = true;
            // 
            // m_cbAlphaTesting
            // 
            this.m_cbAlphaTesting.AutoSize = true;
            this.m_cbAlphaTesting.Location = new System.Drawing.Point(16, 20);
            this.m_cbAlphaTesting.Name = "m_cbAlphaTesting";
            this.m_cbAlphaTesting.Size = new System.Drawing.Size(102, 16);
            this.m_cbAlphaTesting.TabIndex = 1;
            this.m_cbAlphaTesting.Text = "Alpha Testing";
            this.m_cbAlphaTesting.UseVisualStyleBackColor = true;
            // 
            // m_gbAlphaBlending
            // 
            this.m_gbAlphaBlending.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_gbAlphaBlending.Controls.Add(this.m_cbDestBlendMode);
            this.m_gbAlphaBlending.Controls.Add(this.m_cbSrcBlendMode);
            this.m_gbAlphaBlending.Controls.Add(this.label2);
            this.m_gbAlphaBlending.Controls.Add(this.label1);
            this.m_gbAlphaBlending.Controls.Add(this.m_cbAlphaBlending);
            this.m_gbAlphaBlending.Location = new System.Drawing.Point(17, 8);
            this.m_gbAlphaBlending.Name = "m_gbAlphaBlending";
            this.m_gbAlphaBlending.Size = new System.Drawing.Size(276, 100);
            this.m_gbAlphaBlending.TabIndex = 2;
            this.m_gbAlphaBlending.TabStop = false;
            this.m_gbAlphaBlending.Text = "Alpha Blending";
            // 
            // m_gbAlphaTesting
            // 
            this.m_gbAlphaTesting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_gbAlphaTesting.Controls.Add(this.m_tbTestRef);
            this.m_gbAlphaTesting.Controls.Add(this.label3);
            this.m_gbAlphaTesting.Controls.Add(this.m_cbAlphaTesting);
            this.m_gbAlphaTesting.Location = new System.Drawing.Point(17, 114);
            this.m_gbAlphaTesting.Name = "m_gbAlphaTesting";
            this.m_gbAlphaTesting.Size = new System.Drawing.Size(276, 100);
            this.m_gbAlphaTesting.TabIndex = 3;
            this.m_gbAlphaTesting.TabStop = false;
            this.m_gbAlphaTesting.Text = "Alpha Testing";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Src:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dest:";
            // 
            // m_cbSrcBlendMode
            // 
            this.m_cbSrcBlendMode.FormattingEnabled = true;
            this.m_cbSrcBlendMode.Location = new System.Drawing.Point(51, 41);
            this.m_cbSrcBlendMode.Name = "m_cbSrcBlendMode";
            this.m_cbSrcBlendMode.Size = new System.Drawing.Size(166, 20);
            this.m_cbSrcBlendMode.TabIndex = 3;
            // 
            // m_cbDestBlendMode
            // 
            this.m_cbDestBlendMode.FormattingEnabled = true;
            this.m_cbDestBlendMode.Location = new System.Drawing.Point(51, 66);
            this.m_cbDestBlendMode.Name = "m_cbDestBlendMode";
            this.m_cbDestBlendMode.Size = new System.Drawing.Size(166, 20);
            this.m_cbDestBlendMode.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "Test Ref(0~255):";
            // 
            // m_tbTestRef
            // 
            this.m_tbTestRef.Location = new System.Drawing.Point(125, 51);
            this.m_tbTestRef.Name = "m_tbTestRef";
            this.m_tbTestRef.Size = new System.Drawing.Size(41, 21);
            this.m_tbTestRef.TabIndex = 3;
            this.m_tbTestRef.Text = "88";
            // 
            // m_btnOK
            // 
            this.m_btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnOK.Location = new System.Drawing.Point(218, 229);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 23);
            this.m_btnOK.TabIndex = 25;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // AlphaPropertyEditorCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_gbAlphaTesting);
            this.Controls.Add(this.m_gbAlphaBlending);
            this.Name = "AlphaPropertyEditorCtrl";
            this.Size = new System.Drawing.Size(306, 268);
            this.m_gbAlphaBlending.ResumeLayout(false);
            this.m_gbAlphaBlending.PerformLayout();
            this.m_gbAlphaTesting.ResumeLayout(false);
            this.m_gbAlphaTesting.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox m_cbAlphaBlending;
        private System.Windows.Forms.CheckBox m_cbAlphaTesting;
        private System.Windows.Forms.GroupBox m_gbAlphaBlending;
        private System.Windows.Forms.GroupBox m_gbAlphaTesting;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox m_cbDestBlendMode;
        private System.Windows.Forms.ComboBox m_cbSrcBlendMode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_tbTestRef;
        private System.Windows.Forms.Button m_btnOK;
    }
}
