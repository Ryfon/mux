﻿namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs
{
    partial class SelectMaterialDlg
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_FileChooseListView = new System.Windows.Forms.ListView();
            this.m_ImageChooseListView = new System.Windows.Forms.ListView();
            this.m_PreviewIL = new System.Windows.Forms.ImageList(this.components);
            this.m_PreviewPicBox = new System.Windows.Forms.PictureBox();
            this.m_Enter = new System.Windows.Forms.Button();
            this.m_Cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_PreviewPicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // m_FileChooseListView
            // 
            this.m_FileChooseListView.Location = new System.Drawing.Point(24, 22);
            this.m_FileChooseListView.Name = "m_FileChooseListView";
            this.m_FileChooseListView.Size = new System.Drawing.Size(161, 148);
            this.m_FileChooseListView.TabIndex = 1;
            this.m_FileChooseListView.TileSize = new System.Drawing.Size(128, 12);
            this.m_FileChooseListView.UseCompatibleStateImageBehavior = false;
            this.m_FileChooseListView.View = System.Windows.Forms.View.Tile;
            this.m_FileChooseListView.ItemActivate += new System.EventHandler(this.m_FileChooseListView_ItemActivate);
            this.m_FileChooseListView.SelectedIndexChanged += new System.EventHandler(this.m_FileChooseListView_SelectedIndexChanged);
            // 
            // m_ImageChooseListView
            // 
            this.m_ImageChooseListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_ImageChooseListView.LargeImageList = this.m_PreviewIL;
            this.m_ImageChooseListView.Location = new System.Drawing.Point(203, 22);
            this.m_ImageChooseListView.Name = "m_ImageChooseListView";
            this.m_ImageChooseListView.Size = new System.Drawing.Size(266, 301);
            this.m_ImageChooseListView.SmallImageList = this.m_PreviewIL;
            this.m_ImageChooseListView.TabIndex = 2;
            this.m_ImageChooseListView.UseCompatibleStateImageBehavior = false;
            this.m_ImageChooseListView.ItemActivate += new System.EventHandler(this.m_ImageChooseListView_ItemActivate);
            this.m_ImageChooseListView.SelectedIndexChanged += new System.EventHandler(this.m_ImageChooseListView_SelectedIndexChanged);
            // 
            // m_PreviewIL
            // 
            this.m_PreviewIL.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.m_PreviewIL.ImageSize = new System.Drawing.Size(64, 64);
            this.m_PreviewIL.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // m_PreviewPicBox
            // 
            this.m_PreviewPicBox.Location = new System.Drawing.Point(24, 189);
            this.m_PreviewPicBox.Name = "m_PreviewPicBox";
            this.m_PreviewPicBox.Size = new System.Drawing.Size(161, 188);
            this.m_PreviewPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.m_PreviewPicBox.TabIndex = 3;
            this.m_PreviewPicBox.TabStop = false;
            // 
            // m_Enter
            // 
            this.m_Enter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_Enter.Location = new System.Drawing.Point(301, 353);
            this.m_Enter.Name = "m_Enter";
            this.m_Enter.Size = new System.Drawing.Size(75, 23);
            this.m_Enter.TabIndex = 4;
            this.m_Enter.Text = "确定";
            this.m_Enter.UseVisualStyleBackColor = true;
            this.m_Enter.Click += new System.EventHandler(this.m_Enter_Click);
            // 
            // m_Cancel
            // 
            this.m_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_Cancel.Location = new System.Drawing.Point(382, 353);
            this.m_Cancel.Name = "m_Cancel";
            this.m_Cancel.Size = new System.Drawing.Size(75, 23);
            this.m_Cancel.TabIndex = 5;
            this.m_Cancel.Text = "取消";
            this.m_Cancel.UseVisualStyleBackColor = true;
            this.m_Cancel.Click += new System.EventHandler(this.m_Cancel_Click);
            // 
            // SelectMaterialDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 410);
            this.Controls.Add(this.m_Cancel);
            this.Controls.Add(this.m_Enter);
            this.Controls.Add(this.m_PreviewPicBox);
            this.Controls.Add(this.m_ImageChooseListView);
            this.Controls.Add(this.m_FileChooseListView);
            this.Name = "SelectMaterialDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SelectMaterialDlg";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SelectMaterialDlg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_PreviewPicBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView m_FileChooseListView;
        private System.Windows.Forms.ListView m_ImageChooseListView;
        private System.Windows.Forms.PictureBox m_PreviewPicBox;
        private System.Windows.Forms.ImageList m_PreviewIL;
        private System.Windows.Forms.Button m_Enter;
        private System.Windows.Forms.Button m_Cancel;
    }
}