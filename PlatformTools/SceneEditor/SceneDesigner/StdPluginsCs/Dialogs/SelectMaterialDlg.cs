﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace Emergent.Gamebryo.SceneDesigner.StdPluginsCs.Dialogs
{
    public partial class SelectMaterialDlg : Form
    {
        public SelectMaterialDlg()
        {
            InitializeComponent();
        }

        #region private Data
        private const string m_picfirpath = "./Data/Texture/Terrain/Tiles"; 
        private string m_currentselectedPicpath;
        private string m_selectedImage; // 选择的 dds 文件
        private string m_previewImage;  // 对应选择的文件，用来预览的 bmp 文件
        private string[] m_FileArray;
        private string[] m_SubDirs;
        private bool m_bIsRoot;

        #endregion
        private void SelectMaterialDlg_Load(object sender, EventArgs e)
        {
            m_selectedImage = "";
            m_previewImage = "";
            m_currentselectedPicpath = m_picfirpath;
            m_bIsRoot = true;

            GoToPath(m_currentselectedPicpath);
        }

        public string get_CurrentSelectedPic()
        {
            if (m_selectedImage.Equals(""))
            {
                return m_selectedImage;
            }

            return m_currentselectedPicpath + "/" + m_selectedImage;
        }

        public string get_CurrentPreviewPic()
        {
            if (m_previewImage.Equals(""))
            {
                return m_previewImage;
            }

            return m_currentselectedPicpath + "/" + m_previewImage;
        }

        // 将 bmp 扩展名文件名转换为 dds 扩展名文件名
        private string BMPFileNameToDDSFileName(string strBMPFile)
        {
            int iLastDotPos = strBMPFile.LastIndexOf('.');
            if (iLastDotPos > 0)
            {
                return strBMPFile.Substring(0, iLastDotPos) + ".dds";
            }
            else
            {
                return null;
            }
        }

        private void m_FileChooseListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_FileChooseListView.SelectedItems.Count > 0)
            {
                string s = m_FileChooseListView.SelectedItems[0].Text;
                if (s.ToLower().EndsWith(".bmp"))
                {
                    this.m_PreviewPicBox.Image = new Bitmap(m_currentselectedPicpath + "/" + s);
                    m_selectedImage = BMPFileNameToDDSFileName(s);
                    m_previewImage = s;
                }
            }
            else
            {
                this.m_PreviewPicBox.Image = null;
            }
        }

        private void m_FileChooseListView_ItemActivate(object sender, EventArgs e)
        {
            try
            {
                int selectedIndex = m_FileChooseListView.SelectedIndices[0];
                ListViewItem item = m_FileChooseListView.SelectedItems[0];

                // 如果当前目录不是根目录，则把selectedIndex减1，以去掉".."的影响
                if (!m_bIsRoot)
                {
                    selectedIndex--;
                }

                if (selectedIndex == -1)                    /// 说明双击的是".."
                {
                    GoToPath(Path.GetDirectoryName(m_currentselectedPicpath));
                }
                else if (selectedIndex < m_SubDirs.Length)       /// 说明双击的是目录
                {
                    GoToPath( m_SubDirs[selectedIndex] );
                }
                else                                            ///说明双击的是文件
                {
                    m_selectedImage =  BMPFileNameToDDSFileName(item.Text);
                    DialogResult = DialogResult.OK; 
                }
            }
            catch(Exception e2)
            {
                return;
            }


        }

        private void PreviewAllPics(string[] strFiles)
        {
            m_ImageChooseListView.Items.Clear();

            foreach (string s in strFiles)
            {
                PictureBox pb = new PictureBox();
                if (s.ToLower().EndsWith(".bmp"))
                {
                    try
                    {
                        pb.Image = Image.FromFile(s);
                    }
                    catch (Exception e)
                    {
                        continue;
                    }

                    m_PreviewIL.Images.Add(pb.Image);
                    m_ImageChooseListView.Items.Add(Path.GetFileName(s), m_PreviewIL.Images.Keys.Count - 1);
                }

            }
        }

        private void m_ImageChooseListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_ImageChooseListView.SelectedItems.Count > 0)
            {
                string s = m_ImageChooseListView.SelectedItems[0].Text;
                if (s.ToLower().EndsWith(".bmp"))
                {
                    this.m_PreviewPicBox.Image = new Bitmap(m_currentselectedPicpath + "/" + s);
                    m_selectedImage = BMPFileNameToDDSFileName(s);
                    m_previewImage = s;
                }
            }
            else
            {
                this.m_PreviewPicBox.Image = null;
            }

        }

        private void m_Cancel_Click(object sender, EventArgs e)
        {
            m_selectedImage = "";
            DialogResult = DialogResult.Cancel;
        }

        private void m_Enter_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK; 
        }

        private void m_ImageChooseListView_ItemActivate(object sender, EventArgs e)
        {
            try
            {
                m_selectedImage = BMPFileNameToDDSFileName(m_ImageChooseListView.SelectedItems[0].Text);
                DialogResult = DialogResult.OK;
            }
            catch(Exception e2)
            {

            }

        }

        private void GoToPath(string strPath)
        {
            m_currentselectedPicpath = strPath;
            m_FileChooseListView.Items.Clear();
            if (strPath.Equals(m_picfirpath))                       // 如果是回到根目录
            {
                m_bIsRoot = true;
            }
            else
            {
                m_bIsRoot = false;
                m_FileChooseListView.Items.Add("..");
            }

            // 找出所有当前目录下的子目录
            m_SubDirs = System.IO.Directory.GetDirectories(m_currentselectedPicpath);


            foreach (string s in m_SubDirs)
            {
                m_FileChooseListView.Items.Add(Path.GetFileName(s));
            }

            // 找出所有当前目录下的图片
            m_FileArray = System.IO.Directory.GetFiles(m_currentselectedPicpath, "*.bmp");
            foreach (string s in m_FileArray)
            {
                m_FileChooseListView.Items.Add(Path.GetFileName(s));
            }

            PreviewAllPics(m_FileArray);
        }
    }
}