﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27514
// http://www.emergentgametech.com

#include "NiSpeedTreePluginPCH.h"
#include "MEntityPointerEditorDialog.h"

using namespace Emergent::Gamebryo::SceneDesigner::NiSpeedTreePlugin;

//---------------------------------------------------------------------------
MEntity* MEntityPointerEditorDialog::get_SelectedEntity()
{
    return m_pmSelectedEntity;
}
//---------------------------------------------------------------------------
void MEntityPointerEditorDialog::set_SelectedEntity(MEntity* pmValue)
{
    m_pmSelectedEntity = pmValue;
}
//---------------------------------------------------------------------------
MEntity* MEntityPointerEditorDialog::get_ActiveEntity()
{
    return m_pmActiveEntity;
}
//---------------------------------------------------------------------------
void MEntityPointerEditorDialog::set_ActiveEntity(MEntity* pmValue)
{
    m_pmActiveEntity = pmValue;
}
//---------------------------------------------------------------------------
String* MEntityPointerEditorDialog::get_Title()
{
    return this->Text;
}
//---------------------------------------------------------------------------
void MEntityPointerEditorDialog::set_Title(String* pmValue)
{
    this->Text = pmValue;
}
//---------------------------------------------------------------------------
String* MEntityPointerEditorDialog::get_Prompt()
{
    return m_lblChooseSceneEntity->Text;
}
//---------------------------------------------------------------------------
void MEntityPointerEditorDialog::set_Prompt(String* pmValue)
{
    m_lblChooseSceneEntity->Text = pmValue;
}
//---------------------------------------------------------------------------
void MEntityPointerEditorDialog::EntityPointerEditorDialog_Load(
    Object* pmSender, System::EventArgs* eArgs)
{
    m_lbSceneEntities->Items->Clear();
    m_lbSceneEntities->Items->Add(S"<None>");

    MEntity* amSceneEntities[] = MFramework::Instance->Scene->GetEntities();

    if (amSceneEntities->Count == 0)
        return;

    for (int j = 0; j < amSceneEntities->Count; j++)
    {
        MEntity* pmEntity = amSceneEntities[j];
        if (pmEntity != ActiveEntity)
        {
            m_lbSceneEntities->Items->Add(pmEntity);
        }
    }

    if (SelectedEntity != NULL)
    {
        int iIndex = m_lbSceneEntities->Items->IndexOf(
            SelectedEntity);
        if (iIndex > -1)
        {
            m_lbSceneEntities->SelectedIndex = iIndex;
        }
        else if (m_lbSceneEntities->Items->Count > 0)
        {
            m_lbSceneEntities->SelectedIndex = 0;
        }
        else
        {
            SelectedEntity = NULL;
        }
    }
    else if (m_lbSceneEntities->Items->Count > 0)
    {
        m_lbSceneEntities->SelectedIndex = 0;
    }
}
//---------------------------------------------------------------------------
void MEntityPointerEditorDialog::m_lbSceneEntities_SelectedIndexChanged(
    Object* pmSender, System::EventArgs* eArgs)
{
    SelectedEntity = dynamic_cast<MEntity*>(m_lbSceneEntities->SelectedItem);
}
//---------------------------------------------------------------------------
void MEntityPointerEditorDialog::m_lbSceneEntities_DoubleClick(
    Object* pmSender, System::EventArgs* eArgs)
{
    SelectedEntity = dynamic_cast<MEntity*>(m_lbSceneEntities->SelectedItem);
    this->DialogResult = DialogResult::OK;
    this->Close();
}
//---------------------------------------------------------------------------
