﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#pragma once

using namespace System::Drawing;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace NiSpeedTreePlugin
{
    public __gc class MSettingsHelper
    {
    public:
        // this is how external classes will get at standard settings
        __value enum StandardSetting
        {
            NEAR_LOD_DISTANCE = 0,
            FAR_LOD_DISTANCE,
            ORTHO_LOD_PERCENT,
            RENDER_AS_BILLBOARDS,
            RENDER_WIREFRAME,
            SHADER_PATH,
            FOG_ENABLED,
            FOG_NEAR_DISTANCE,
            FOG_COLOR,

            SETTING_COUNT
        };

        static String* GetSettingName(StandardSetting eSetting);
        static String* GetSettingDescription(StandardSetting eSetting);
        static SettingsCategory GetSettingCategory(StandardSetting eSetting);
        static bool IsArgStandardSetting(SettingChangedEventArgs* pmEventArgs, 
            StandardSetting eSetting);

        static bool GetStandardSetting(StandardSetting eSetting, 
            float __gc& fValue);
        static bool GetStandardSetting(StandardSetting eSetting, 
            bool __gc& bValue);
        static bool GetStandardSetting(StandardSetting eSetting, 
            NiColor& kValue);
        static bool GetStandardSetting(StandardSetting eSetting, 
            MFolderLocation*& pmValue);

        static void SetStandardSetting(StandardSetting eSetting, float fValue);
        static void SetStandardSetting(StandardSetting eSetting, bool bValue);
        static void SetStandardSetting(StandardSetting eSetting, 
            NiColor& kValue);
        static void SetStandardSetting(StandardSetting eSetting, 
            MFolderLocation* pmValue);

        static void RegisterSetting(StandardSetting eSetting, 
            float __gc& fValue, SettingChangedHandler* pmHandler);
        static void RegisterSetting(StandardSetting eSetting, 
            bool __gc& fValue, SettingChangedHandler* pmHandler);
        static void RegisterSetting(StandardSetting eSetting, 
            NiColor& kValue, SettingChangedHandler* pmHandler);
        static void RegisterSetting(StandardSetting eSetting, 
            MFolderLocation*& pmValue, SettingChangedHandler* pmHandler);

        __property static ISettingsService* get_SettingsService();
        static ISettingsService* ms_pmSettingsService;

        __property static IOptionsService* get_OptionsService();
        static IOptionsService* ms_pmOptionsService;

    protected:

        static String* ms_astrSettingName[];
        static String* ms_astrSettingDescription[];
        static String* ms_astrSettingOption[];
        static SettingsCategory ms_aeSettingCategory[];

        // Singleton members.
    private public:
        static void Init();
        static void Shutdown();
        static bool InstanceIsValid();
        __property static MSettingsHelper* get_Instance();
    private:
        static MSettingsHelper* ms_pmThis = NULL;
        MSettingsHelper();
    };
}}}}
