﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27514
// http://www.emergentgametech.com

#pragma once
using namespace System;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;

#include "MSettingsHelper.h"

namespace Emergent { namespace Gamebryo { namespace SceneDesigner { 
    namespace NiSpeedTreePlugin 
{

    public __gc class MPlugin : public MDisposable, public IProxyHandler, 
        public IPlugin
    {
    private:
        
    public:
        MPlugin();
        ~MPlugin();

        //IPlugin implementation
        __property String* get_Name();
        __property System::Version* get_Version();
        __property System::Version* get_ExpectedVersion();
        void Load(int iToolMajorVersion, int iToolMinorVersion);
        IService* GetProvidedServices()[];
        void Start();

        [DllInit]
        static void InitStatics();

        [DllShutdown]
        static void ShutdownStatics();

    // MDisposable members.
    protected:
        virtual void Do_Dispose(bool bDisposing);

    // IProxyHandler members.
    public:
        virtual MEntity* GetMasterProxyEntity(MEntity* pmEntity);
        MEntity* m_pmMasterProxy;

    private:
        void OnNewSceneLoaded(MScene* pmScene);
        void OnSettingChanged(Object* pmSender,
            SettingChangedEventArgs* pmEventArgs);
        bool SetValue(MSettingsHelper::StandardSetting eSetting, float fValue);
        bool SetValue(MSettingsHelper::StandardSetting eSetting, bool bValue);
        bool SetValue(MSettingsHelper::StandardSetting eSetting, 
            NiColor& kColor);
        bool SetValue(MSettingsHelper::StandardSetting eSetting, 
            MFolderLocation* pmLoc);

        void RegisterProperties();
        void RegisterComponents();
        void RegisterSettings();
        void RegisterProxies();

        float m_fLODNear;
        float m_fLODFar;
        float m_fOrthoPercent;
        bool m_bDrawWireframe;
        bool m_bDrawBillboard;
        MFolderLocation* m_pmShaderPath;
        bool m_bFogEnabled;
        float m_fFogNear;
        NiColor* m_pkFogColor;
    };

}}}}
