﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27514
// http://www.emergentgametech.com

#pragma once

using namespace System;
using namespace System::Drawing;
using namespace System::Collections;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;

// Note: This class is simply a managed C++ conversion of
// Emergent::Gamebryo::SceneDesigner::StdPluginCs::EntityPointerEditorDialog.

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace NiSpeedTreePlugin
{
    public __gc class MEntityPointerEditorDialog : 
        public System::Windows::Forms::Form
	{
    public:
        MEntityPointerEditorDialog()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        __property MEntity* get_SelectedEntity();
        __property void set_SelectedEntity(MEntity* pmValue);

        __property MEntity* get_ActiveEntity();
        __property void set_ActiveEntity(MEntity* pmValue);

        __property String* get_Title();
        __property void set_Title(String* pmValue);

        __property String* get_Prompt();
        __property void set_Prompt(String* pmValue);

    protected:
		void Dispose(Boolean disposing)
        {
            if (disposing && components)
            {
                components->Dispose();
            }
            __super::Dispose(disposing);
        }

    private:
        System::Windows::Forms::Label* m_lblChooseSceneEntity;
        System::Windows::Forms::Button* m_btnOK;
        System::Windows::Forms::Button* m_btnCancel;
        System::Windows::Forms::ListBox* m_lbSceneEntities;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        System::ComponentModel::Container* components;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
		void InitializeComponent(void)
        {
            this->m_lblChooseSceneEntity = new System::Windows::Forms::Label();
            this->m_lbSceneEntities = new System::Windows::Forms::ListBox();
            this->m_btnOK = new System::Windows::Forms::Button();
            this->m_btnCancel = new System::Windows::Forms::Button();
            this->SuspendLayout();
            // 
            // m_lblChooseSceneEntity
            // 
            this->m_lblChooseSceneEntity->AutoSize = true;
            this->m_lblChooseSceneEntity->Location = 
                System::Drawing::Point(8, 8);
            this->m_lblChooseSceneEntity->Name = "m_lblChooseSceneEntity";
            this->m_lblChooseSceneEntity->Size = 
                System::Drawing::Size(113, 16);
            this->m_lblChooseSceneEntity->TabIndex = 0;
            this->m_lblChooseSceneEntity->Text = "Choose Scene Entity:";
            // 
            // m_lbSceneEntities
            // 
            this->m_lbSceneEntities->Anchor = 
                ((System::Windows::Forms::AnchorStyles)
                ((((System::Windows::Forms::AnchorStyles::Top 
                | System::Windows::Forms::AnchorStyles::Bottom) 
                | System::Windows::Forms::AnchorStyles::Left) 
                | System::Windows::Forms::AnchorStyles::Right)));
            this->m_lbSceneEntities->IntegralHeight = false;
            this->m_lbSceneEntities->Location = System::Drawing::Point(8, 32);
            this->m_lbSceneEntities->Name = "m_lbSceneEntities";
            this->m_lbSceneEntities->Size = System::Drawing::Size(336, 255);
            this->m_lbSceneEntities->Sorted = true;
            this->m_lbSceneEntities->TabIndex = 1;
            this->m_lbSceneEntities->DoubleClick += 
                new System::EventHandler(this, 
                &MEntityPointerEditorDialog::m_lbSceneEntities_DoubleClick);
            this->m_lbSceneEntities->SelectedIndexChanged += 
                new System::EventHandler(this, 
                &MEntityPointerEditorDialog::
                m_lbSceneEntities_SelectedIndexChanged);
            // 
            // m_btnOK
            // 
            this->m_btnOK->Anchor = 
                System::Windows::Forms::AnchorStyles::Bottom;
            this->m_btnOK->DialogResult = 
                System::Windows::Forms::DialogResult::OK;
            this->m_btnOK->FlatStyle = 
                System::Windows::Forms::FlatStyle::System;
            this->m_btnOK->Location = System::Drawing::Point(96, 296);
            this->m_btnOK->Name = "m_btnOK";
            this->m_btnOK->TabIndex = 2;
            this->m_btnOK->Text = "OK";
            // 
            // m_btnCancel
            // 
            this->m_btnCancel->Anchor = 
                System::Windows::Forms::AnchorStyles::Bottom;
            this->m_btnCancel->DialogResult = 
                System::Windows::Forms::DialogResult::Cancel;
            this->m_btnCancel->FlatStyle = 
                System::Windows::Forms::FlatStyle::System;
            this->m_btnCancel->Location = System::Drawing::Point(181, 296);
            this->m_btnCancel->Name = "m_btnCancel";
            this->m_btnCancel->TabIndex = 3;
            this->m_btnCancel->Text = "Cancel";
            // 
            // EntityPointerEditorDialog
            // 
            this->AcceptButton = this->m_btnOK;
            this->AutoScaleBaseSize = System::Drawing::Size(5, 13);
            this->CancelButton = this->m_btnCancel;
            this->ClientSize = System::Drawing::Size(352, 326);
            this->Controls->Add(this->m_lbSceneEntities);
            this->Controls->Add(this->m_btnCancel);
            this->Controls->Add(this->m_btnOK);
            this->Controls->Add(this->m_lblChooseSceneEntity);
            this->MinimizeBox = false;
            this->MinimumSize = System::Drawing::Size(192, 176);
            this->Name = "EntityPointerEditorDialog";
            this->ShowInTaskbar = false;
            this->StartPosition = 
                System::Windows::Forms::FormStartPosition::CenterParent;
            this->Text = "Scene Entity Selector";
            this->Load += new System::EventHandler(this, 
                &MEntityPointerEditorDialog::EntityPointerEditorDialog_Load);
            this->ResumeLayout(false);
        }

        MEntity* m_pmSelectedEntity;
        MEntity* m_pmActiveEntity;

        void EntityPointerEditorDialog_Load(Object* pmSender,
            System::EventArgs* eArgs);

        void m_lbSceneEntities_SelectedIndexChanged(Object* pmSender,
            System::EventArgs* eArgs);

        void m_lbSceneEntities_DoubleClick(Object* pmSender,
            System::EventArgs* eArgs);
	};
}}}}
