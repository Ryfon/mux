﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#pragma once

using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace NiSpeedTreePlugin
{
    public __gc class MUICommands
    {
    public:
        [UICommandHandlerAttribute("AddTreeTemplates")]
        static void AddTreeTemplatesHandler(Object* pmSender,
            EventArgs* pmEventArgs);

        [UICommandValidatorAttribute("AddTreeTemplates")]
        static void AddTreeTemplatesValidator(Object* pmSender, 
             UIState* pmState);

        [UICommandHandlerAttribute("ApplyWindToSelected")]
        static void ApplyWindToSelectedHandler(Object* pmSender,
            EventArgs* pmEventArgs);

        [UICommandValidatorAttribute("ApplyWindToSelected")]
         static void ApplyWindToSelectedValidator(Object* pmSender, 
             UIState* pmState);

         [UICommandHandlerAttribute("BillboardAllTrees")]
         static void BillboardAllTreesHandler(Object* pmSender,
             EventArgs* pmEventArgs);

         [UICommandValidatorAttribute("BillboardAllTrees")]
         static void BillboardAllTreesValidator(Object* pmSender, 
             UIState* pmState);

        static MEntity* CreateTemplateFromFile(String* strFileName);
    };
}}}}
