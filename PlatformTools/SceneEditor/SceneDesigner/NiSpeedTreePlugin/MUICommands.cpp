﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "NiSpeedTreePluginPCH.h"

#include "MUICommands.h"
#include "MSettingsHelper.h"
#include "MEntityPointerEditorDialog.h"

#include <NiSpeedTreeInstance.h>
#include <NiSpeedTreeCuller.h>
#include <NiSpeedTreeComponent.h>
#include <NiSpeedWindComponent.h>

using namespace Emergent::Gamebryo::SceneDesigner::NiSpeedTreePlugin;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;

using namespace System::Windows::Forms;

//---------------------------------------------------------------------------
void MUICommands::AddTreeTemplatesHandler(Object* pmSender, 
    EventArgs* pmEventArgs)
{
    OpenFileDialog* pmOpenFileDialog = new OpenFileDialog();
    pmOpenFileDialog->Title = 
        "Choose SpeedTree Assets to Add to Current Palette";
    pmOpenFileDialog->Filter = "All Files (*.*)|*.*";
    pmOpenFileDialog->Multiselect = true;
    if (pmOpenFileDialog->ShowDialog() == DialogResult::OK)
    {
        ICommandService* pmCommandService = MGetService(ICommandService);

        pmCommandService->BeginUndoFrame("Add SpeedTree assets to palette");

        MPaletteManager* pmPaletteManager = 
            MFramework::Instance->PaletteManager;

        MPalette* pmPalette = pmPaletteManager->ActivePalette;

        MEntity* pmNewTemplate = NULL;
        String* astrFileNames[] = pmOpenFileDialog->FileNames;
        for (int j = 0; j < astrFileNames->Count; j++)
        {
            String* strFileName = astrFileNames[j];

            pmNewTemplate = CreateTemplateFromFile(strFileName);
            if (pmNewTemplate != NULL)
            {
                pmNewTemplate->Name = String::Format("[{0}]{1}", 
                    pmPalette->Name, pmNewTemplate->Name);

                // Add the new template to the palette.
                pmPalette->AddEntity(pmNewTemplate, String::Empty, true);
            }
        }
    
        pmPalette->ActiveEntity = pmNewTemplate;

        pmCommandService->EndUndoFrame(true);
    }
}
//---------------------------------------------------------------------------
void MUICommands::AddTreeTemplatesValidator(Object* pmSender, 
    UIState* pmState)
{
    MPaletteManager* pmPaletteManager = 
        MFramework::Instance->PaletteManager;

    MPalette* pmPalette = pmPaletteManager->ActivePalette;

    pmState->Enabled = (pmPalette != NULL);
}
//---------------------------------------------------------------------------
void MUICommands::ApplyWindToSelectedHandler(Object* pmSender,
    EventArgs* pmEventArgs)
{
    ISelectionService* pmSelectionService = MGetService(ISelectionService);
    if (pmSelectionService->NumSelectedEntities == 0)
        return;
   
    MEntityPointerEditorDialog* pmSelectEntityDlg = 
        new MEntityPointerEditorDialog();
    pmSelectEntityDlg->Title = S"Select Wind Entity";
    pmSelectEntityDlg->Prompt = S"Choose wind entity to apply to selected "
        S"trees:";
    if (pmSelectEntityDlg->ShowDialog() != DialogResult::OK)
        return;

    // Selected wind
    NiEntityInterface* pkWind = NULL;
    MEntity* pmSelected = pmSelectEntityDlg->SelectedEntity;
    if (pmSelected)
    {
        pkWind = pmSelected->GetNiEntityInterface();
    }

    MEntity* amEntities[] = pmSelectionService->GetSelectedEntities();
    for (int j = 0; j < amEntities->Count; j++)
    {
        MEntity* pmEntity = amEntities[j];

        MComponent* amComponents[] = pmEntity->GetComponents();
        for (int c = 0; c < amComponents->Count; 
            c++)
        {
            MComponent* pmComponent = amComponents[c];
            if (pmComponent->GetNiEntityComponentInterface()->GetClassName() 
                == NiSpeedTreeComponent::ClassName())
            {
                NiSpeedTreeComponent* pkComponent = (NiSpeedTreeComponent*)
                    pmComponent->GetNiEntityComponentInterface();
                pkComponent->SetWindEntity(pkWind);
                break;
            }
        }
    }
}
//---------------------------------------------------------------------------
void MUICommands::ApplyWindToSelectedValidator(Object* pmSender, 
    UIState* pmState)
{
    ISelectionService* pmSelectionService = MGetService(ISelectionService);

    pmState->Enabled = (pmSelectionService->NumSelectedEntities > 0);
}
//---------------------------------------------------------------------------
void MUICommands::BillboardAllTreesHandler(Object* pmSender,
    EventArgs* pmEventArgs)
{
    bool bChecked;
    MSettingsHelper::Instance->GetStandardSetting(
        MSettingsHelper::RENDER_AS_BILLBOARDS, bChecked);

    MSettingsHelper::Instance->SetStandardSetting(
        MSettingsHelper::RENDER_AS_BILLBOARDS, !bChecked);
}
//---------------------------------------------------------------------------
void MUICommands::BillboardAllTreesValidator(Object* pmSender, 
    UIState* pmState)
{
    bool bChecked;
    MSettingsHelper::Instance->GetStandardSetting(
        MSettingsHelper::RENDER_AS_BILLBOARDS, bChecked);
    pmState->Checked = bChecked;
    pmState->Enabled = true;
}
//---------------------------------------------------------------------------
// Utility Functions
//---------------------------------------------------------------------------
MEntity* MUICommands::CreateTemplateFromFile(
    String* strFileName)
{
    MAssert(strFileName != NULL && strFileName != String::Empty, "Null or "
        "empty filename provided to function!");

    // Get filename.
    const char* pcFileName = MStringToCharPointer(strFileName);

    // Get base name.
    const char* pcBaseName = strrchr(pcFileName, '\\') + 1;

    // Copy base name to a new buffer.
    size_t stBufferSize = strlen(pcBaseName) + 1;
    char* pcEntityName = NiAlloc(char, stBufferSize);
    NiStrcpy(pcEntityName, stBufferSize, pcBaseName);

    // Clear off extension from base name.
    char* pcPtr = strrchr(pcEntityName, '.');
    *pcPtr = '\0';

    // Create new entity with name constructed above.
    NiUniqueID kTemplateID;
    MUtility::GuidToID(Guid::NewGuid(), kTemplateID);
    NiEntityInterfaceIPtr spEntity = NiNew NiGeneralEntity(pcEntityName,
        kTemplateID, 2);

    // Free entity name buffer.
    NiFree(pcEntityName);

    // Add transformation component.
    spEntity->AddComponent(NiNew NiTransformationComponent());

    // Check file extension and add additional components.
    const char* pcExtension = strrchr(pcBaseName, '.') + 1;
    bool bAddedComponent = false;
    if (_stricmp(pcExtension, "spt") == 0)
    {
        NiSpeedTreeComponent* pkSpeedTreeComponent = NiNew
            NiSpeedTreeComponent();
        pkSpeedTreeComponent->SetSptFilePath(pcFileName);
        spEntity->AddComponent(pkSpeedTreeComponent);
        bAddedComponent = true;
    }
    else if (_stricmp(pcExtension, "ini") == 0)
    {
        NiSpeedWindComponent* pkSpeedWindComponent = NiNew
            NiSpeedWindComponent();
        pkSpeedWindComponent->SetIniFilePath(pcFileName);
        spEntity->AddComponent(pkSpeedWindComponent);
        bAddedComponent = true;
    }

    // Free the filename pointer.
    MFreeCharPointer(pcFileName);

    // If no component was added, the file format is unknown. The entity
    // cannot be created.
    if (!bAddedComponent)
    {
        return NULL;
    }

    // Return the managed entity.

    return MFramework::Instance->EntityFactory->Get(spEntity);
}
//---------------------------------------------------------------------------
