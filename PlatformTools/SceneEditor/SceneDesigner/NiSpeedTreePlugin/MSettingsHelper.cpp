﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

#include "NiSpeedTreePluginPCH.h"

#include "MSettingsHelper.h"

using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI;
using namespace Emergent::Gamebryo::SceneDesigner::PluginAPI::StandardServices;
using namespace Emergent::Gamebryo::SceneDesigner::Framework;
using namespace Emergent::Gamebryo::SceneDesigner::NiSpeedTreePlugin;

//---------------------------------------------------------------------------
void MSettingsHelper::Init()
{
    if (ms_pmThis == NULL)
    {
        ms_pmThis = new MSettingsHelper();
    }
}
//---------------------------------------------------------------------------
void MSettingsHelper::Shutdown()
{
    if (ms_pmThis != NULL)
    {
        ms_pmThis = NULL;
    }
}
//---------------------------------------------------------------------------
bool MSettingsHelper::InstanceIsValid()
{
    return (ms_pmThis != NULL);
}
//---------------------------------------------------------------------------
MSettingsHelper* MSettingsHelper::get_Instance()
{
    return ms_pmThis;
}
//---------------------------------------------------------------------------
MSettingsHelper::MSettingsHelper()
{
    // initialized standard settings arrays here
    ms_astrSettingName = new String*[SETTING_COUNT];
    ms_astrSettingDescription = new String*[SETTING_COUNT];
    ms_astrSettingOption  = new String*[SETTING_COUNT];
    ms_aeSettingCategory = new SettingsCategory[SETTING_COUNT];

    ms_astrSettingName[NEAR_LOD_DISTANCE] = "NearLODDistance";
    ms_astrSettingDescription[NEAR_LOD_DISTANCE] = 
        "Specifies the near distance for tree LOD, where"
        " trees first begin to be billboarded.";
    ms_astrSettingOption[NEAR_LOD_DISTANCE] = 
        "Speed Tree.LOD Options.Near LOD Distance";
    ms_aeSettingCategory[NEAR_LOD_DISTANCE] = SettingsCategory::PerScene;

    ms_astrSettingName[FAR_LOD_DISTANCE] = "FarLODDistance";
    ms_astrSettingDescription[FAR_LOD_DISTANCE] = 
        "Specifies the far distance for tree LOD, where"
        " trees become fully billboarded.";
    ms_astrSettingOption[FAR_LOD_DISTANCE] = 
        "Speed Tree.LOD Options.Far LOD Distance";
    ms_aeSettingCategory[FAR_LOD_DISTANCE] = SettingsCategory::PerScene;

    ms_astrSettingName[ORTHO_LOD_PERCENT] = "OrthoLODPercent";
    ms_astrSettingDescription[ORTHO_LOD_PERCENT] = 
        "When using an orthogonal camera, trees whose bounding volumes take "
        "up less than this fraction of the viewport will be billboarded.";
    ms_astrSettingOption[ORTHO_LOD_PERCENT] = 
        "Speed Tree.LOD Options.Ortho LOD Percent";
    ms_aeSettingCategory[ORTHO_LOD_PERCENT] = SettingsCategory::PerScene;

    ms_astrSettingName[RENDER_AS_BILLBOARDS] = "RenderAsBillboards";
    ms_astrSettingDescription[RENDER_AS_BILLBOARDS] = 
        "If this option is enabled, then all trees will be rendered as "
        "billboards, regardless of camera distance.";
    ms_astrSettingOption[RENDER_AS_BILLBOARDS] = 
        "Speed Tree.Rendering Options.Render as Billboards";
    ms_aeSettingCategory[RENDER_AS_BILLBOARDS] = SettingsCategory::PerUser;

    ms_astrSettingName[RENDER_WIREFRAME] = "WireframeRendering";
    ms_astrSettingDescription[RENDER_WIREFRAME] = 
        "If this option is enabled, then wireframe rendering will be turned "
        "on for all trees.";
        ms_astrSettingOption[RENDER_WIREFRAME] = 
        "Speed Tree.Rendering Options.Wireframe Rendering";
    ms_aeSettingCategory[RENDER_WIREFRAME] = SettingsCategory::PerUser;

    ms_astrSettingName[SHADER_PATH] = "ShaderPath";
    ms_astrSettingDescription[SHADER_PATH] = "This is the path to the "
        "directory that contains the SpeedTree.fx shader.";
    ms_astrSettingOption[SHADER_PATH] = 
        "Speed Tree.Resource Paths.Shader Path";
    ms_aeSettingCategory[SHADER_PATH] = SettingsCategory::PerUser;

    ms_astrSettingName[FOG_ENABLED] = "FogEnabled";
    ms_astrSettingDescription[FOG_ENABLED] = "This setting enables or "
        "disables fog.";
    ms_astrSettingOption[FOG_ENABLED] = "Speed Tree.Fog.Fog Enabled";
    ms_aeSettingCategory[FOG_ENABLED] = SettingsCategory::PerScene;

    ms_astrSettingName[FOG_NEAR_DISTANCE] = "FogNearDistance";
    ms_astrSettingDescription[FOG_NEAR_DISTANCE] = "The value of this "
        "parameter is the distance in world space that fog begins for all "
        "trees.";
    ms_astrSettingOption[FOG_NEAR_DISTANCE] = 
        "Speed Tree.Fog.Fog Near Distance";
    ms_aeSettingCategory[FOG_NEAR_DISTANCE] = SettingsCategory::PerScene;

    ms_astrSettingName[FOG_COLOR] = "FogColor";
    ms_astrSettingDescription[FOG_COLOR] = "This is the color of fog to use "
        "for all trees.";
    ms_astrSettingOption[FOG_COLOR] = "Speed Tree.Fog.Fog Color";
    ms_aeSettingCategory[FOG_COLOR] = SettingsCategory::PerScene;
}
//---------------------------------------------------------------------------
String* MSettingsHelper::GetSettingName(StandardSetting eSetting)
{
    return ms_astrSettingName[eSetting];
}
//---------------------------------------------------------------------------
String* MSettingsHelper::GetSettingDescription(StandardSetting eSetting)
{
    return ms_astrSettingDescription[eSetting];
}
//---------------------------------------------------------------------------
SettingsCategory MSettingsHelper::GetSettingCategory(StandardSetting eSetting)
{
    return ms_aeSettingCategory[eSetting];
}
//---------------------------------------------------------------------------
bool MSettingsHelper::IsArgStandardSetting(
    SettingChangedEventArgs* pmEventArgs, StandardSetting eSetting)
{
    String* strName = pmEventArgs->Name;
    SettingsCategory eCategory = pmEventArgs->Category;

    return ((strName->Equals(GetSettingName(eSetting))) && (eCategory == 
        GetSettingCategory(eSetting)));
}
//---------------------------------------------------------------------------
bool MSettingsHelper::GetStandardSetting(StandardSetting eSetting, 
    float __gc& fValue)
{
    Object* pmObj;
    pmObj = SettingsService->GetSettingsObject(ms_astrSettingName[eSetting], 
        ms_aeSettingCategory[eSetting]);
    __box float* pfVal = dynamic_cast<__box float*>(pmObj);
    if (pfVal != NULL)
    {
        fValue = *pfVal;
        return true;
    }
    else
    {
        return false;
    }
}
//---------------------------------------------------------------------------
bool MSettingsHelper::GetStandardSetting(StandardSetting eSetting, 
    bool __gc& bValue)
{
    Object* pmObj;
    pmObj = SettingsService->GetSettingsObject(ms_astrSettingName[eSetting], 
        ms_aeSettingCategory[eSetting]);
    __box bool* pbVal = dynamic_cast<__box bool*>(pmObj);
    if (pbVal != NULL)
    {
        bValue = *pbVal;
        return true;
    }
    else
    {
        return false;
    }
}
//---------------------------------------------------------------------------
bool MSettingsHelper::GetStandardSetting(StandardSetting eSetting, 
    NiColor& kValue)
{
    Object* pmObj;
    __box Color* pmValue;
    pmObj = SettingsService->GetSettingsObject(ms_astrSettingName[eSetting], 
        ms_aeSettingCategory[eSetting]);
    pmValue = dynamic_cast<__box Color*>(pmObj);
    if (pmValue != NULL)
    {
        kValue.r = MUtility::RGBToFloat((*pmValue).R);
        kValue.g = MUtility::RGBToFloat((*pmValue).G);
        kValue.b = MUtility::RGBToFloat((*pmValue).B);
        return true;
    }
    else
    {
        return false;
    }
}
//---------------------------------------------------------------------------
bool MSettingsHelper::GetStandardSetting(StandardSetting eSetting, 
    MFolderLocation*& pmValue)
{
    Object* pmObj;
    MFolderLocation* pmLoc = new MFolderLocation("");
    pmObj = SettingsService->GetSettingsObject(ms_astrSettingName[eSetting], 
        ms_aeSettingCategory[eSetting]);
    pmLoc = dynamic_cast<MFolderLocation*>(pmObj);
    if (pmLoc != NULL)
    {
        pmValue = pmLoc;
        return true;
    }
    else
    {
        return false;
    }
}
//---------------------------------------------------------------------------
void MSettingsHelper::SetStandardSetting(StandardSetting eSetting, 
    float fValue)
{
    SettingsService->SetSettingsObject(ms_astrSettingName[eSetting],
        __box(fValue), ms_aeSettingCategory[eSetting]);
}
//---------------------------------------------------------------------------
void MSettingsHelper::SetStandardSetting(StandardSetting eSetting,
    bool bValue)
{
    SettingsService->SetSettingsObject(ms_astrSettingName[eSetting],
        __box(bValue), ms_aeSettingCategory[eSetting]);
}
//---------------------------------------------------------------------------
void MSettingsHelper::SetStandardSetting(StandardSetting eSetting, 
    NiColor& kValue)
{
    SettingsService->SetSettingsObject(ms_astrSettingName[eSetting],
        __box(Color::FromArgb(255, MUtility::FloatToRGB(kValue.r),
        MUtility::FloatToRGB(kValue.g), MUtility::FloatToRGB(kValue.b))), 
        ms_aeSettingCategory[eSetting]);
}
//---------------------------------------------------------------------------
void MSettingsHelper::SetStandardSetting(StandardSetting eSetting, 
    MFolderLocation* pmValue)
{
    SettingsService->SetSettingsObject(ms_astrSettingName[eSetting],
        pmValue, ms_aeSettingCategory[eSetting]);
}
//---------------------------------------------------------------------------
void MSettingsHelper::RegisterSetting(StandardSetting eSetting, 
    float __gc& fValue, SettingChangedHandler* pmHandler)
{
    String* strName = ms_astrSettingName[eSetting];
    String* strDescription = ms_astrSettingDescription[eSetting];
    SettingsCategory eCategory = ms_aeSettingCategory[eSetting];

    // first register the setting with the service if it doesn't already exist
    SettingsService->RegisterSettingsObject(strName, __box(fValue), eCategory);
    // set the event handler for when that setting changes
    if (pmHandler != NULL)
    {
        SettingsService->SetChangedSettingHandler(strName, eCategory, 
            pmHandler);
    }

    if (GetStandardSetting(eSetting, fValue))
    {
        String* strOptionCategory = ms_astrSettingOption[eSetting];
        if (strOptionCategory != NULL)
        {
            OptionsService->AddOption(strOptionCategory, eCategory, strName); 
        }
        if (strDescription != NULL)
        {
            OptionsService->SetHelpDescription(strOptionCategory, 
                strDescription);
        }
    }
}
//---------------------------------------------------------------------------
void MSettingsHelper::RegisterSetting(StandardSetting eSetting, 
    bool __gc& fValue, SettingChangedHandler* pmHandler)
{
    String* strName = ms_astrSettingName[eSetting];
    String* strDescription = ms_astrSettingDescription[eSetting];
    SettingsCategory eCategory = ms_aeSettingCategory[eSetting];

    // first register the setting with the service if it doesn't already exist
    SettingsService->RegisterSettingsObject(strName, __box(fValue), eCategory);
    // set the event handler for when that setting changes
    if (pmHandler != NULL)
    {
        SettingsService->SetChangedSettingHandler(strName, eCategory, 
            pmHandler);
    }

    if (GetStandardSetting(eSetting, fValue))
    {
        String* strOptionCategory = ms_astrSettingOption[eSetting];
        if (strOptionCategory != NULL)
        {
            OptionsService->AddOption(strOptionCategory, eCategory, strName); 
        }
        if (strDescription != NULL)
        {
            OptionsService->SetHelpDescription(strOptionCategory, 
                strDescription);
        }
    }
}
//---------------------------------------------------------------------------
void MSettingsHelper::RegisterSetting(StandardSetting eSetting, 
    NiColor& kValue, SettingChangedHandler* pmHandler)
{
    String* strName = ms_astrSettingName[eSetting];
    String* strDescription = ms_astrSettingDescription[eSetting];
    SettingsCategory eCategory = ms_aeSettingCategory[eSetting];

    // first register the setting in case it doesn't already exist
    SettingsService->RegisterSettingsObject(strName, __box(Color::FromArgb(255,
        MUtility::FloatToRGB(kValue.r), MUtility::FloatToRGB(kValue.g), 
        MUtility::FloatToRGB(kValue.b))), eCategory);
    // set the event handler for when that setting changes
    if (pmHandler != NULL)
    {
        SettingsService->SetChangedSettingHandler(strName, eCategory, 
            pmHandler);
    }

    if (GetStandardSetting(eSetting, kValue))
    {
        String* strOptionCategory = ms_astrSettingOption[eSetting];
        if (strOptionCategory != NULL)
        {
            OptionsService->AddOption(strOptionCategory, eCategory, strName); 
        }
        if (strDescription != NULL)
        {
            OptionsService->SetHelpDescription(strOptionCategory, 
                strDescription);
        }
    }
}
//---------------------------------------------------------------------------
void MSettingsHelper::RegisterSetting(StandardSetting eSetting, 
    MFolderLocation*& pmValue, SettingChangedHandler* pmHandler)
{
    String* strName = ms_astrSettingName[eSetting];
    String* strDescription = ms_astrSettingDescription[eSetting];
    SettingsCategory eCategory = ms_aeSettingCategory[eSetting];

    SettingsService->RegisterSettingsObject(strName, pmValue, 
        eCategory);
    // set the event handler for when that setting changes
    if (pmHandler != NULL)
    {
        SettingsService->SetChangedSettingHandler(strName, eCategory, 
            pmHandler);
    }

    if (GetStandardSetting(eSetting, pmValue))
    {
        String* strOptionCategory = ms_astrSettingOption[eSetting];
        if (strOptionCategory != NULL)
        {
            OptionsService->AddOption(strOptionCategory, eCategory, strName); 
        }
        if (strDescription != NULL)
        {
            OptionsService->SetHelpDescription(strOptionCategory, 
                strDescription);
        }
    }
}
//---------------------------------------------------------------------------
ISettingsService* MSettingsHelper::get_SettingsService()
{
    if (ms_pmSettingsService == NULL)
    {
        ms_pmSettingsService = MGetService(ISettingsService);
        MAssert(ms_pmSettingsService != NULL, "Settings service not found.");
    }
    return ms_pmSettingsService;
}
//---------------------------------------------------------------------------
IOptionsService* MSettingsHelper::get_OptionsService()
{
    if (ms_pmOptionsService == NULL)
    {
        ms_pmOptionsService = MGetService(IOptionsService);
        MAssert(ms_pmOptionsService != NULL, "Options service not found.");
    }
    return ms_pmOptionsService;
}
//---------------------------------------------------------------------------
