﻿#pragma once

#include "MSelectionInteractionMode.h"


#pragma unmanaged
	#include "TerrainModifier.h"
	#include "TerrainChangeVertexCommand.h"	
	//#include "RiverManager.h"
#pragma managed

namespace Emergent{ namespace Gamebryo{ namespace SceneDesigner{
    namespace StdPluginsCpp
{
    public __gc class MPainterMode : public MViewInteractionMode
	{
	public:
		MPainterMode();

        void RegisterSettings();
        void OnSettingChanged(Object* pmSender,
            SettingChangedEventArgs* pmEventArgs);

        [UICommandHandlerAttribute("UpRiseTerrainMode")]
        void SetInteractionMode(Object* pmObject, EventArgs* mArgs);

        [UICommandValidatorAttribute("UpRiseTerrainMode")]
        void ValidateInteractionMode(Object* pmSender, UIState* pmState);
//
	
        __property String* get_Name();
        bool Initialize();
        void Update(float fTime);
        void RenderGizmo(MRenderingContext* pmRenderingContext);
        void MouseDown(MouseButtonType eType, int iX, int iY);
        void MouseUp(MouseButtonType eType, int iX, int iY);
        void MouseMove(int iX, int iY);

	protected:
        virtual void Do_Dispose(bool bDisposing);

	private:

		MBrushParameter		*m_pBrushParameter;
		MTerrainParameter	*m_pTerrainParameter;

		NiTriShape			*m_pSelectionRegion;
		bool				m_bDraging;

		/// 记录鼠标的操作
		int					m_iStartX;
		int					m_iStartY;
		int					m_iEndX;
		int					m_iEndY;

		// 笔刷工具绘制方式 这里的枚举
		//__value enum EPainterType
		//{
		//	PT_Terrain = 0,	// 地形顶点高度 
		//	PT_Material,	// 地表纹理,顶点色
		//	PT_Property,	// 地表属性
		//	PT_GridArea,	// 以 grid 为单位的区域
		//	PT_Water,		// add by 和萌 刷水体
		//	PT_MAX,						
		//};	

		__value struct STerrainBrush
		{
			float		fInRadius;
			float		fOutRadius;
			float		fValue;

			String		*pcTexFile;
			MTerrain::EPainterType	ePainterType;	//EPainterType
		};

		STerrainBrush		m_terrainBrush;
		
		CTerrainModifier	*m_pTerrainModifier;

		CTerrainChangeVertexCommand* m_pCurrentCommand;	// 当次操作所生成的 command

		void _AdjustTerrain( int iX, int iY, bool bDraging );
		void _PaintTerrain( int iX, int iY, bool bDraging );

		// 编辑 GridBaseRegion
		void _PaintGridBaseRegion(int iX, int iY);

		// 刷涂方式删除 Entity
		void _PaintRemoveEntity(int iX, int iY);

		//void SetCollisionOnTerrain( int iX, int iY, bool bDraging );
		void SetTerrainProperty( int iX, int iY, bool bDraging , bool bDelete );
		//////////////////////////////////////////////////////////////////////////
		//add by 和萌 添加水域
		/*void _AddWater( int iX, int iY, bool bDraging);
		void _DeleteWater(int iX, int iY,bool bDraging);
		void _BuildWaterGeom( int iX,int iY);*/

		void AdjustSelectionRegion( int iX, int iY );		
		bool GetInterPtFromMousePos( int iX, int iY, NiPoint3& vInterPt );
	};

}}}}