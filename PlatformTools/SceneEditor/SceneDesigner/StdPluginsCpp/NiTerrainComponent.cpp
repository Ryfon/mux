﻿#include "StdPluginsCppPCH.h"

#include "NiTerrainComponent.h"
#include "NiEntityErrorInterface.h"

NiFixedString NiTerrainComponent::ms_kClassName;
NiFixedString NiTerrainComponent::ms_kComponentName;
NiFixedString NiTerrainComponent::ms_kSceneRootString;

NiTerrainComponent::NiTerrainComponent()
{}

NiTerrainComponent::~NiTerrainComponent()
{}

//---------------------------------------------------------------------------
void NiTerrainComponent::_SDMInit()
{
    ms_kClassName = "NiTerrainComponent";
    ms_kComponentName = "Terrain";
    ms_kSceneRootString = "Scene Root Pointer";

	CTerrainTextureMgr::GetInstance()->Initialize( "./" );
}

//---------------------------------------------------------------------------
void NiTerrainComponent::_SDMShutdown()
{       
    ms_kClassName = NULL;
    ms_kComponentName = NULL;
	ms_kSceneRootString = NULL;

	MFramework::Instance->Scene->Terrain->Destroy();
	CTerrainTextureMgr::GetInstance()->ClearAll();
}

//bool NiTerrainComponent::Create()
//{		
//	return true;
//}

//CTerrain * NiTerrainComponent::GetTerrainInstance()
//{
//	if ( !ms_pTerrain )
//		ms_pTerrain  = new CTerrain();
//	
//	return ms_pTerrain;
//}

//bool NiTerrainComponent::CreateTerrain( int iChunkX, int iChunkY, const char* pszBaseTexFile )
//{ 
//	//if ( ms_pTerrain )		delete ms_pTerrain;
//
//	if ( !ms_pTerrain->Create( iChunkX, iChunkY, pszBaseTexFile ) )
//	{
//		delete ms_pTerrain;
//		ms_pTerrain = NULL;
//
//		OutputDebugString( "cannot create terrain.\n" );
//		return false;
//	}
//
//	ms_pTerrain->SetCursorTexture( "Materials/Circle.tga" );
//
//	return true;
//}

//bool NiTerrainComponent::CreateTerrain( const char *pszFile )
//{
//	assert( ms_pTerrain );
//
//	if ( !ms_pTerrain->LoadFromFile( pszFile ) )
//	{
//		delete ms_pTerrain;
//		ms_pTerrain = NULL;
//
//		OutputDebugString( "cannot create terrain.\n" );
//		return false;
//	}
//
//	ms_pTerrain->SetCursorTexture( "Materials/Circle.tga" );
//	return true;
//}

//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* NiTerrainComponent::Clone(bool bInheritProperties)
{
    return NiNew NiTerrainComponent();
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* NiTerrainComponent::GetMasterComponent()
{
    // This component does not have a master component.
    return NULL;
}
//---------------------------------------------------------------------------
void NiTerrainComponent::SetMasterComponent(
    NiEntityComponentInterface* pkMasterComponent)
{
    // This component does not have a master component.
}
//---------------------------------------------------------------------------
void NiTerrainComponent::GetDependentPropertyNames(
    NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  NiTerrainComponent::GetTemplateID()
{
    static const NiUniqueID kUniqueID = 
        NiUniqueID(0x35,0x83,0x33,0x47,0xFE,0x1E,0x1F,0x45,0xB5,0x83,0xAB,0xD6,
        0xB2,0x83,0x5B,0xC2);
    return kUniqueID;
}
//---------------------------------------------------------------------------
void NiTerrainComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void NiTerrainComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString NiTerrainComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString NiTerrainComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
void NiTerrainComponent::Update(NiEntityPropertyInterface* pkParentEntity, 
    float fTime, NiEntityErrorInterface* pkErrors,
    NiExternalAssetManager* pkAssetManager)
{
}
//---------------------------------------------------------------------------
void NiTerrainComponent::BuildVisibleSet(NiEntityRenderingContext* 
    pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
	if ( !MFramework::Instance->Scene->Terrain )	return;
	
    NiVisibleArray* pkVisibleSet = pkRenderingContext->m_pkCullingProcess
        ->GetVisibleSet();
    NIASSERT(pkVisibleSet);
	MFramework::Instance->Scene->Terrain->SetActiveCamera( pkRenderingContext->m_pkCamera );
	MFramework::Instance->Scene->Terrain->Update( 0.0f, 0.0f );
	NiCullScene( pkRenderingContext->m_pkCamera, MFramework::Instance->Scene->Terrain->GetTerrainRootNode(),
		*pkRenderingContext->m_pkCullingProcess, *pkVisibleSet, false );

	//ms_pTerrain->SaveTerrainToNif( "c:\\terrain.nif" );

	//ms_pTerrain->BuilderVisibleSet( pkRenderingContext->m_pkCamera, pkVisibleSet );
}
//---------------------------------------------------------------------------
void NiTerrainComponent::GetPropertyNames(
    NiTObjectSet<NiFixedString>& kPropertyNames) const
{
	kPropertyNames.RemoveAll();
	kPropertyNames.Add(ms_kSceneRootString);
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::CanResetProperty(const NiFixedString& kPropertyName, 
    bool& bCanReset) const
{
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    // No properties can be reset.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::MakePropertyUnique(const NiFixedString& kPropertyName,
    bool& bMadeUnique)
{
	return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetDisplayName(const NiFixedString& kPropertyName, 
    NiFixedString& kDisplayName) const
{
	if ( ms_kSceneRootString == kPropertyName )
	{
		kDisplayName = ms_kSceneRootString;
		return true;
	}
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetDisplayName(const NiFixedString& kPropertyName,
    const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetPrimitiveType(const NiFixedString& kPropertyName, 
    NiFixedString& kPrimitiveType) const
{
	if (kPropertyName == ms_kSceneRootString)
    {
        kPrimitiveType = PT_NIOBJECTPOINTER;
    }
	return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetPrimitiveType(const NiFixedString& kPropertyName,
    const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetSemanticType(const NiFixedString& kPropertyName, 
    NiFixedString& kSemanticType) const
{
    if (kPropertyName == ms_kSceneRootString)
    {
        kSemanticType = PT_NIOBJECTPOINTER;
    }
	else
		return false;

	return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetSemanticType(const NiFixedString& kPropertyName,
    const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetDescription(const NiFixedString& kPropertyName, 
    NiFixedString& kDescription) const
{
	if ( kPropertyName == ms_kSceneRootString )
		kDescription = kPropertyName;
	else
		return false;
	return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetDescription(const NiFixedString& kPropertyName,
    const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetCategory(const NiFixedString& kPropertyName, 
    NiFixedString& kCategory) const
{
	if ( kPropertyName == ms_kSceneRootString )
		kCategory = ms_kComponentName;
	else
		return false;
	return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, 
    bool& bIsReadOnly)
{
	if ( kPropertyName == ms_kSceneRootString )
	{
		bIsReadOnly = true;
	}
	else
		return false;
	return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::IsPropertyUnique(
    const NiFixedString& kPropertyName, bool& bIsUnique)
{
	if ( kPropertyName == ms_kSceneRootString )
	{
		bIsUnique = true;
	}
	else
		return false;
    return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::IsPropertySerializable(
    const NiFixedString& kPropertyName, bool& bIsSerializable)
{
    bool bIsUnique;
    NiBool bSuccess = IsPropertyUnique(kPropertyName, bIsUnique);
    if (bSuccess)
    {
        bool bIsReadOnly;
        bSuccess = IsPropertyReadOnly(kPropertyName, bIsReadOnly);
        NIASSERT(bSuccess);

        bIsSerializable = bIsUnique && !bIsReadOnly;
    }

    return bSuccess;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::IsPropertyInheritable(
    const NiFixedString& kPropertyName, bool& bIsInheritable)
{
	if ( kPropertyName == ms_kSceneRootString )
	{
		bIsInheritable = false;
	}
	else
		return false;
    return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::IsExternalAssetPath(
    const NiFixedString& kPropertyName, unsigned int uiIndex,
    bool& bIsExternalAssetPath) const
{
	if ( kPropertyName == ms_kSceneRootString )
	{
		bIsExternalAssetPath = false;
	}
	else
		return false;
    return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetElementCount(const NiFixedString& kPropertyName,
    unsigned int& uiCount) const
{
	if ( kPropertyName == ms_kSceneRootString )
	{
		uiCount = 1;
	}
	else
		return false;
    return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetElementCount(const NiFixedString& kPropertyName,
    unsigned int uiCount, bool& bCountSet)
{
	if ( kPropertyName == ms_kSceneRootString )
	{
		bCountSet = false;
	}
	else
		return false;
    return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::IsCollection(const NiFixedString& kPropertyName,
    bool& bIsCollection) const
{
	if ( kPropertyName == ms_kSceneRootString )
	{
		bIsCollection = false;
	}
	else
		return false;
    return true;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetPropertyData(const NiFixedString& kPropertyName,
    bool& bData, unsigned int uiIndex) const
{
	return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetPropertyData(const NiFixedString& kPropertyName, 
    int& iData, unsigned int uiIndex) const
{
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetPropertyData(const NiFixedString& kPropertyName, 
    int iData, unsigned int uiIndex)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetPropertyData(const NiFixedString& kPropertyName, 
    NiColor& kData, unsigned int uiIndex) const
{
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::SetPropertyData(const NiFixedString& kPropertyName, 
    const NiColor& kData, unsigned int uiIndex)
{
    return false;
}
//---------------------------------------------------------------------------
NiBool NiTerrainComponent::GetPropertyData(const NiFixedString& kPropertyName,
    NiObject*& pkData, unsigned int uiIndex) const
{

	if ( kPropertyName == ms_kSceneRootString && uiIndex == 0 )
	{
		if ( MFramework::Instance->Scene->Terrain )
			pkData = MFramework::Instance->Scene->Terrain->GetTerrainRootNode();
		else
			pkData = 0;
	}
	else
		return false;

    return true;
}
//---------------------------------------------------------------------------