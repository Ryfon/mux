﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "StdPluginsCppPCH.h"

#include "MCreateTypeConverter.h"

using namespace Emergent::Gamebryo::SceneDesigner::StdPluginsCpp;
using namespace System::Collections;
using namespace System;

//---------------------------------------------------------------------------
bool MCreateTypeConverter::GetStandardValuesSupported(
	ITypeDescriptorContext* pmContext)
{
	return true;
}
//---------------------------------------------------------------------------
bool MCreateTypeConverter::GetStandardValuesExclusive(
	ITypeDescriptorContext* pmContext)
{
	return true;
}
//---------------------------------------------------------------------------
TypeConverter::StandardValuesCollection*
MCreateTypeConverter::GetStandardValues(ITypeDescriptorContext* pmContext)
{
	ArrayList* pmValues = new ArrayList();
	pmValues->Add(new String("1 一直不断开启"));
	pmValues->Add(new String("2 固定时间段中开启"));

	return new StandardValuesCollection(pmValues);
}
//---------------------------------------------------------------------------
