﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header

#include "StdPluginsCppPCH.h"
#include "MEntityCategoryConverter.h"
#include "SceneDesignerConfig.h"

using namespace Emergent::Gamebryo::SceneDesigner::StdPluginsCpp;
using namespace System::Collections;

//---------------------------------------------------------------------------
bool MEntityCategoryConverter::GetStandardValuesSupported(
	ITypeDescriptorContext* pmContext)
{
	return true;
}
//---------------------------------------------------------------------------
bool MEntityCategoryConverter::GetStandardValuesExclusive(
	ITypeDescriptorContext* pmContext)
{
	return true;
}
//---------------------------------------------------------------------------
TypeConverter::StandardValuesCollection*
MEntityCategoryConverter::GetStandardValues(
	ITypeDescriptorContext* pmContext)
{
	const vector<string> entityCategoryList = CSceneDesignerConfig::GetInstance()->GetEntityCategoryList();

	ArrayList* pmValues = new ArrayList();
	//const NiTObjectPtrSet<NiShadowClickGeneratorPtr>& kShadowClickGenerators =
	//	NiShadowManager::GetShadowClickGenerators();
	for (unsigned int ui = 0; ui < entityCategoryList.size(); ui++)
	{
		pmValues->Add(new String(entityCategoryList[ui].c_str()));
	}

	return new StandardValuesCollection(pmValues);
}
//---------------------------------------------------------------------------