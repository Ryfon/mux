﻿// EMERGENT GAME TECHNOLOGIES PROPRIETARY INFORMATION
//
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Emergent Game Technologies and may not 
// be copied or disclosed except in accordance with the terms of that 
// agreement.
//
//      Copyright (c) 1996-2007 Emergent Game Technologies.
//      All Rights Reserved.
//
// Emergent Game Technologies, Chapel Hill, North Carolina 27517
// http://www.emergent.net

// Precompiled Header
#include "StdPluginsCppPCH.h"

#include "NiGridComponent.h"
#include "NiEntityErrorInterface.h"

NiFixedString NiGridComponent::ms_kMajorLinesString;
NiFixedString NiGridComponent::ms_kGridExtentString;
NiFixedString NiGridComponent::ms_kGridColorString;
NiFixedString NiGridComponent::ms_kMajorColorString;
NiFixedString NiGridComponent::ms_kStaticString;
NiFixedString NiGridComponent::ms_kSceneRootString;

NiFixedString NiGridComponent::ms_kClassName;
NiFixedString NiGridComponent::ms_kComponentName;

CTerrain * NiGridComponent::sm_pTerrain = NULL;
//---------------------------------------------------------------------------
void NiGridComponent::RebuildInvalidGrid()
{
}
//---------------------------------------------------------------------------
void NiGridComponent::_SDMInit()
{
    ms_kMajorLinesString = "MajorLines";
    ms_kGridExtentString = "GridExtent";
    ms_kGridColorString = "GridColor";
    ms_kMajorColorString = "MajorColor";
    ms_kStaticString = "Static";
    ms_kSceneRootString = "Scene Root Pointer";

    ms_kClassName = "NiGridComponent";
    ms_kComponentName = "Grid";

	sm_pTerrain = NULL;
}
//---------------------------------------------------------------------------
void NiGridComponent::_SDMShutdown()
{
    ms_kMajorLinesString = NULL;
    ms_kGridExtentString = NULL;
    ms_kGridColorString = NULL;
    ms_kMajorColorString = NULL;
    ms_kStaticString = NULL;
    ms_kSceneRootString = NULL;
        
    ms_kClassName = NULL;
    ms_kComponentName = NULL;
	if ( sm_pTerrain )
	{
		delete sm_pTerrain;
		sm_pTerrain = NULL;
	}
}

CTerrain * NiGridComponent::GetTerrainInstance()
{
	return sm_pTerrain;
}

void NiGridComponent::CreateTerrain()
{ 
	/*if ( sm_pTerrain )
		delete sm_pTerrain;
	sm_pTerrain = new CTerrain();
	if ( !sm_pTerrain->Create( 8, 8 ) )
	{
		delete sm_pTerrain;
		sm_pTerrain = NULL;
		OutputDebugString( "cannot create terrain.\n" );
		return;
	}*/
}

//---------------------------------------------------------------------------
// NiEntityComponentInterface overrides.
//---------------------------------------------------------------------------
NiEntityComponentInterface* NiGridComponent::Clone(bool bInheritProperties)
{
    return NiNew NiGridComponent(m_fGridSpacing, m_iMajorLines, m_iGridExtent,
        m_kGridColor, m_kMajorColor, m_bStaticGrid);
}
//---------------------------------------------------------------------------
NiEntityComponentInterface* NiGridComponent::GetMasterComponent()
{
    // This component does not have a master component.
    return NULL;
}
//---------------------------------------------------------------------------
void NiGridComponent::SetMasterComponent(
    NiEntityComponentInterface* pkMasterComponent)
{
    // This component does not have a master component.
}
//---------------------------------------------------------------------------
void NiGridComponent::GetDependentPropertyNames(
    NiTObjectSet<NiFixedString>& kDependentPropertyNames)
{
}
//---------------------------------------------------------------------------
// NiEntityPropertyInterface overrides.
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetTemplateID(const NiUniqueID& kID)
{
    //Not supported for custom components
    return false;
}
//---------------------------------------------------------------------------
NiUniqueID  NiGridComponent::GetTemplateID()
{
    static const NiUniqueID kUniqueID = 
        NiUniqueID(0x35,0x83,0x33,0x47,0xFE,0x1E,0x1F,0x45,0xB5,0x83,0xAB,0xD6,
        0xB2,0x83,0x5B,0xC2);
    return kUniqueID;
}
//---------------------------------------------------------------------------
void NiGridComponent::AddReference()
{
    this->IncRefCount();
}
//---------------------------------------------------------------------------
void NiGridComponent::RemoveReference()
{
    this->DecRefCount();
}
//---------------------------------------------------------------------------
NiFixedString NiGridComponent::GetClassName() const
{
    return ms_kClassName;
}
//---------------------------------------------------------------------------
NiFixedString NiGridComponent::GetName() const
{
    return ms_kComponentName;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetName(const NiFixedString& kName)
{
    // This component does not allow its name to be set.
    return false;
}
//---------------------------------------------------------------------------
void NiGridComponent::Update(NiEntityPropertyInterface* pkParentEntity, 
    float fTime, NiEntityErrorInterface* pkErrors,
    NiExternalAssetManager* pkAssetManager)
{
//	if ( m_pTerrain )
//		m_pTerrain->Update( fTime, 0.0f );
    // this component has no need to update
}
//---------------------------------------------------------------------------
void NiGridComponent::BuildVisibleSet(NiEntityRenderingContext* 
    pkRenderingContext, NiEntityErrorInterface* pkErrors)
{
 //   RebuildInvalidGrid();
 //   NiVisibleArray* pkVisibleSet = pkRenderingContext->m_pkCullingProcess
 //       ->GetVisibleSet();
 //   NIASSERT(pkVisibleSet);
	//sm_pTerrain->SetActiveCamera( pkRenderingContext->m_pkCamera );
	//sm_pTerrain->Update( 0.0f, 0.0f );
	//sm_pTerrain->BuilderVisibleSet( pkRenderingContext->m_pkCamera, pkVisibleSet );
}
//---------------------------------------------------------------------------
void NiGridComponent::GetPropertyNames(
    NiTObjectSet<NiFixedString>& kPropertyNames) const
{
    kPropertyNames.Add(ms_kMajorLinesString);
    kPropertyNames.Add(ms_kGridExtentString);
    kPropertyNames.Add(ms_kGridColorString);
    kPropertyNames.Add(ms_kMajorColorString);
    kPropertyNames.Add(ms_kStaticString);
    kPropertyNames.Add(ms_kSceneRootString);
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::CanResetProperty(const NiFixedString& kPropertyName, 
    bool& bCanReset) const
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        bCanReset = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::ResetProperty(const NiFixedString& kPropertyName)
{
    // No properties can be reset.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::MakePropertyUnique(const NiFixedString& kPropertyName,
    bool& bMadeUnique)
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        // No properties are inherited, so they are all already unique.
        bMadeUnique = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetDisplayName(const NiFixedString& kPropertyName, 
    NiFixedString& kDisplayName) const
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString)
    {
        kDisplayName = kPropertyName;
        return true;
    }
    else if ((kPropertyName == ms_kSceneRootString) ||
        (kPropertyName == ms_kStaticString))
    {
        kDisplayName = NULL;
        return true;
    }
    
    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetDisplayName(const NiFixedString& kPropertyName,
    const NiFixedString& kDisplayName)
{
    // This component does not allow the display name to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetPrimitiveType(const NiFixedString& kPropertyName, 
    NiFixedString& kPrimitiveType) const
{
    if (kPropertyName == ms_kMajorLinesString)
    {
        kPrimitiveType = PT_INT;
    }
    else if (kPropertyName == ms_kGridExtentString)
    {
        kPrimitiveType = PT_INT;
    }
    else if (kPropertyName == ms_kGridColorString)
    {
        kPrimitiveType = PT_COLOR;
    }
    else if (kPropertyName == ms_kMajorColorString)
    {
        kPrimitiveType = PT_COLOR;
    }
    else if (kPropertyName == ms_kStaticString)
    {
        kPrimitiveType = PT_BOOL;
    }
    else if (kPropertyName == ms_kSceneRootString)
    {
        kPrimitiveType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetPrimitiveType(const NiFixedString& kPropertyName,
    const NiFixedString& kPrimitiveType)
{
    // This component does not allow the primitive type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetSemanticType(const NiFixedString& kPropertyName, 
    NiFixedString& kSemanticType) const
{
    if (kPropertyName == ms_kMajorLinesString)
    {
        kSemanticType = PT_INT;
    }
    else if (kPropertyName == ms_kGridExtentString)
    {
        kSemanticType = PT_INT;
    }
    else if (kPropertyName == ms_kGridColorString)
    {
        kSemanticType = PT_COLOR;
    }
    else if (kPropertyName == ms_kMajorColorString)
    {
        kSemanticType = PT_COLOR;
    }
    else if (kPropertyName == ms_kStaticString)
    {
        kSemanticType = PT_BOOL;
    }
    else if (kPropertyName == ms_kSceneRootString)
    {
        kSemanticType = PT_NIOBJECTPOINTER;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetSemanticType(const NiFixedString& kPropertyName,
    const NiFixedString& kSemanticType)
{
    // This component does not allow the semantic type to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetDescription(const NiFixedString& kPropertyName, 
    NiFixedString& kDescription) const
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        kDescription = kPropertyName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetDescription(const NiFixedString& kPropertyName,
    const NiFixedString& kDescription)
{
    // This component does not allow the description to be set.
    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetCategory(const NiFixedString& kPropertyName, 
    NiFixedString& kCategory) const
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        kCategory = ms_kComponentName;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::IsPropertyReadOnly(const NiFixedString& kPropertyName, 
    bool& bIsReadOnly)
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString)
    {
        bIsReadOnly = false;
        return true;
    }
    else if ((kPropertyName == ms_kSceneRootString) ||
        (kPropertyName == ms_kStaticString))
    {
        bIsReadOnly = true;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::IsPropertyUnique(
    const NiFixedString& kPropertyName, bool& bIsUnique)
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        bIsUnique = true;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::IsPropertySerializable(
    const NiFixedString& kPropertyName, bool& bIsSerializable)
{
    bool bIsUnique;
    NiBool bSuccess = IsPropertyUnique(kPropertyName, bIsUnique);
    if (bSuccess)
    {
        bool bIsReadOnly;
        bSuccess = IsPropertyReadOnly(kPropertyName, bIsReadOnly);
        NIASSERT(bSuccess);

        bIsSerializable = bIsUnique && !bIsReadOnly;
    }

    return bSuccess;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::IsPropertyInheritable(
    const NiFixedString& kPropertyName, bool& bIsInheritable)
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        bIsInheritable = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::IsExternalAssetPath(
    const NiFixedString& kPropertyName, unsigned int uiIndex,
    bool& bIsExternalAssetPath) const
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        bIsExternalAssetPath = false;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetElementCount(const NiFixedString& kPropertyName,
    unsigned int& uiCount) const
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        uiCount = 1;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetElementCount(const NiFixedString& kPropertyName,
    unsigned int uiCount, bool& bCountSet)
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString ||
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        bCountSet = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::IsCollection(const NiFixedString& kPropertyName,
    bool& bIsCollection) const
{
    if (kPropertyName == ms_kMajorLinesString ||
        kPropertyName == ms_kGridExtentString ||
        kPropertyName == ms_kGridColorString ||
        kPropertyName == ms_kMajorColorString || 
        kPropertyName == ms_kStaticString ||
        kPropertyName == ms_kSceneRootString)
    {
        bIsCollection = false;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetPropertyData(const NiFixedString& kPropertyName,
    bool& bData, unsigned int uiIndex) const
{
    if ((kPropertyName == ms_kStaticString) && (uiIndex == 0))
    {
        bData = m_bStaticGrid;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetPropertyData(const NiFixedString& kPropertyName, 
    int& iData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kMajorLinesString)
    {
        iData = m_iMajorLines;
    }
    else if (kPropertyName == ms_kGridExtentString)
    {
        iData = m_iGridExtent;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetPropertyData(const NiFixedString& kPropertyName, 
    int iData, unsigned int uiIndex)
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kMajorLinesString)
    {
        SetMajorLineSpacing(iData);
    }
    else if (kPropertyName == ms_kGridExtentString)
    {
        SetGridExtent(iData);
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetPropertyData(const NiFixedString& kPropertyName, 
    NiColor& kData, unsigned int uiIndex) const
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kGridColorString)
    {
        kData = m_kGridColor;
    }
    else if (kPropertyName == ms_kMajorColorString)
    {
        kData = m_kMajorColor;
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::SetPropertyData(const NiFixedString& kPropertyName, 
    const NiColor& kData, unsigned int uiIndex)
{
    if (uiIndex != 0)
    {
        return false;
    }
    else if (kPropertyName == ms_kGridColorString)
    {
        SetGridColor(kData);
    }
    else if (kPropertyName == ms_kMajorColorString)
    {
        SetMajorColor(kData);
    }
    else
    {
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------
NiBool NiGridComponent::GetPropertyData(const NiFixedString& kPropertyName,
    NiObject*& pkData, unsigned int uiIndex) const
{
    if (kPropertyName == ms_kSceneRootString)
    {
		if ( sm_pTerrain )
			pkData = sm_pTerrain->GetTerrainRootNode();
		else
			pkData = NULL;
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------
