﻿namespace MissionEditor
{
    partial class DescEditControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DescEditControl));
            this.m_textBoxDesc = new System.Windows.Forms.TextBox();
            this.m_toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonBold = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonColor = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonEnter = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPlayerName = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLink = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPreview = new System.Windows.Forms.ToolStripButton();
            this.m_webBrowser = new System.Windows.Forms.WebBrowser();
            this.m_toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_textBoxDesc
            // 
            this.m_textBoxDesc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.m_textBoxDesc.Location = new System.Drawing.Point(0, 25);
            this.m_textBoxDesc.Multiline = true;
            this.m_textBoxDesc.Name = "m_textBoxDesc";
            this.m_textBoxDesc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.m_textBoxDesc.Size = new System.Drawing.Size(280, 215);
            this.m_textBoxDesc.TabIndex = 0;
            this.m_textBoxDesc.Visible = false;
            // 
            // m_toolStrip
            // 
            this.m_toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonBold,
            this.toolStripButtonColor,
            this.toolStripButtonEnter,
            this.toolStripButtonPlayerName,
            this.toolStripButtonLink,
            this.toolStripButtonPreview});
            this.m_toolStrip.Location = new System.Drawing.Point(0, 0);
            this.m_toolStrip.Name = "m_toolStrip";
            this.m_toolStrip.Size = new System.Drawing.Size(280, 25);
            this.m_toolStrip.TabIndex = 1;
            this.m_toolStrip.Text = "toolStrip1";
            // 
            // toolStripButtonBold
            // 
            this.toolStripButtonBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonBold.Enabled = false;
            this.toolStripButtonBold.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonBold.Image")));
            this.toolStripButtonBold.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBold.Name = "toolStripButtonBold";
            this.toolStripButtonBold.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonBold.Text = "加粗<b></b>";
            this.toolStripButtonBold.Click += new System.EventHandler(this.toolStripButtonBold_Click);
            // 
            // toolStripButtonColor
            // 
            this.toolStripButtonColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonColor.Enabled = false;
            this.toolStripButtonColor.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonColor.Image")));
            this.toolStripButtonColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonColor.Name = "toolStripButtonColor";
            this.toolStripButtonColor.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonColor.Text = "字体颜色<font color=\"\"></font>";
            this.toolStripButtonColor.Click += new System.EventHandler(this.toolStripButtonColor_Click);
            // 
            // toolStripButtonEnter
            // 
            this.toolStripButtonEnter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonEnter.Enabled = false;
            this.toolStripButtonEnter.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonEnter.Image")));
            this.toolStripButtonEnter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEnter.Name = "toolStripButtonEnter";
            this.toolStripButtonEnter.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonEnter.Text = "换行符<br>";
            this.toolStripButtonEnter.Click += new System.EventHandler(this.toolStripButtonEnter_Click);
            // 
            // toolStripButtonPlayerName
            // 
            this.toolStripButtonPlayerName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPlayerName.Enabled = false;
            this.toolStripButtonPlayerName.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPlayerName.Image")));
            this.toolStripButtonPlayerName.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPlayerName.Name = "toolStripButtonPlayerName";
            this.toolStripButtonPlayerName.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonPlayerName.Text = "添加玩家姓名<Player>";
            this.toolStripButtonPlayerName.Click += new System.EventHandler(this.toolStripButtonPlayerName_Click);
            // 
            // toolStripButtonLink
            // 
            this.toolStripButtonLink.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLink.Enabled = false;
            this.toolStripButtonLink.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLink.Image")));
            this.toolStripButtonLink.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLink.Name = "toolStripButtonLink";
            this.toolStripButtonLink.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonLink.Text = "超级链接<a link=\"\"></a>";
            this.toolStripButtonLink.Click += new System.EventHandler(this.toolStripButtonLink_Click);
            // 
            // toolStripButtonPreview
            // 
            this.toolStripButtonPreview.Checked = true;
            this.toolStripButtonPreview.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripButtonPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPreview.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPreview.Image")));
            this.toolStripButtonPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPreview.Name = "toolStripButtonPreview";
            this.toolStripButtonPreview.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonPreview.Text = "预览";
            this.toolStripButtonPreview.Click += new System.EventHandler(this.toolStripButtonPreview_Click);
            // 
            // m_webBrowser
            // 
            this.m_webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.m_webBrowser.Location = new System.Drawing.Point(0, 25);
            this.m_webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.m_webBrowser.Name = "m_webBrowser";
            this.m_webBrowser.Size = new System.Drawing.Size(280, 215);
            this.m_webBrowser.TabIndex = 2;
            // 
            // DescEditControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_webBrowser);
            this.Controls.Add(this.m_toolStrip);
            this.Controls.Add(this.m_textBoxDesc);
            this.Name = "DescEditControl";
            this.Size = new System.Drawing.Size(280, 240);
            this.m_toolStrip.ResumeLayout(false);
            this.m_toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_textBoxDesc;
        private System.Windows.Forms.ToolStrip m_toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonBold;
        private System.Windows.Forms.ToolStripButton toolStripButtonColor;
        private System.Windows.Forms.ToolStripButton toolStripButtonPlayerName;
        private System.Windows.Forms.ToolStripButton toolStripButtonLink;
        private System.Windows.Forms.ToolStripButton toolStripButtonPreview;
        private System.Windows.Forms.WebBrowser m_webBrowser;
        private System.Windows.Forms.ToolStripButton toolStripButtonEnter;
    }
}
