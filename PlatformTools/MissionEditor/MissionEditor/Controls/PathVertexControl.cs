﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class PathVertexControl : UserControl
    {
        const int iSmallRectSize = 6;

        public PathVertexControl(VertexNode vn, int iSceneWidth, int iSceneHeight)
        {
            InitializeComponent();
            m_VertexNode = vn;
            m_iSceneWidth = iSceneWidth;
            m_iSceneHeight = iSceneHeight;
        }

        private GraphicsPath GetLineGraphicPath(Point _StartPoint, Point _EndPoint)
        {
            PointF vDir = new PointF();
            vDir.X = _EndPoint.X - _StartPoint.X;
            vDir.Y = _EndPoint.Y - _StartPoint.Y;

            float fLen = (float)System.Math.Sqrt(vDir.X * vDir.X + vDir.Y * vDir.Y);
            if (fLen <= float.Epsilon)
            {
                return null;
            }

            vDir.X /= fLen;
            vDir.Y /= fLen;

            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            for (int i = 0; i <= fLen; i += 2)
            {
                PointF p = new PointF(_StartPoint.X + vDir.X * i, _StartPoint.Y + vDir.Y * i);
                path.AddRectangle(new RectangleF(p.X - 1, p.Y - 1, 2, 2));
            }

            return path;
        }

        private void PathVertexControl_Paint(object sender, PaintEventArgs e)
        {   
            if(m_VertexNode == null)
            {
                return;
            }

            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Alternate);
            GraphicsPath EdgePath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            // 计算场景尺寸到 1024 纹理的缩放
            float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

            // 坐标的转换
            Point oldPos = m_VertexNode.Pos;
            Point scenePos = new Point(oldPos.X, m_iSceneHeight - oldPos.Y);
            scenePos.X = (int)(scenePos.X * fScale);
            scenePos.Y = (int)(scenePos.Y * fScale);

            Rectangle rect = new Rectangle((int)(scenePos.X - iSmallRectSize), (int)(scenePos.Y - iSmallRectSize), 2 * iSmallRectSize, 2 * iSmallRectSize);

            //显示名称          
            FontFamily family = new FontFamily("Times New Roman");
            int fontStyle = (int)FontStyle.Italic;
            int emSize = 16;
            StringFormat format = StringFormat.GenericDefault;
            path.AddString(m_VertexNode.ID.ToString(), family, fontStyle, emSize, scenePos, format);

            // 添加物件
            path.AddEllipse(rect);

            Hashtable edgeList = m_VertexNode.EdgeList;
            foreach (EdgeNode en in edgeList.Values)
            {
                Point enPt = PathManager.Instance.GetVertexPosByID(m_VertexNode.MapID, en.AdjNode);
                Point enNewPt = new Point(enPt.X, m_iSceneHeight - enPt.Y);
                enNewPt.X = (int)(enNewPt.X * fScale);
                enNewPt.Y = (int)(enNewPt.Y * fScale);
                path.AddPath(GetLineGraphicPath(scenePos, enNewPt), true);
            }
            

            this.Region = new Region(path);

            // 开始渲染
            Color color = Color.Yellow;
            if(m_bSelected)
            {
                color = Color.Red;
            }

            SolidBrush brush = new SolidBrush(color);
            Pen pen = new Pen(color, 10);
            RectangleF bound = path.GetBounds();
            //this.Left = (int)(bound.Location.X);
            //this.Top = (int)(bound.Location.Y);
            //this.Width = (int)(bound.Width);//
            //this.Height = (int)(bound.Height);//
            this.Left = 0; // (int)(bound.Location.X);
            this.Top = 0; // (int)(bound.Location.Y);
            this.Width = (int)(bound.Location.X + bound.Width);//
            this.Height = (int)(bound.Location.Y + bound.Height);//

            e.Graphics.FillPath(brush, path);
        }

        private void PathVertexControl_MouseDown(object sender, MouseEventArgs e)
        {
            // 用户在当前控件上按下
            if (e.Button == MouseButtons.Left)
            {
                _DownLocation.X = e.X;
                _DownLocation.Y = e.Y;

                // 计算场景尺寸到 1024 纹理的缩放
                float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

                // 坐标的转换
                Point oldPos = m_VertexNode.Pos;
                Point scenePos = new Point(oldPos.X, m_iSceneHeight - oldPos.Y);
                scenePos.X = (int)(scenePos.X * fScale);
                scenePos.Y = (int)(scenePos.Y * fScale);

                if (Math.Abs(e.X - scenePos.X) < 2*iSmallRectSize && Math.Abs(e.Y - scenePos.Y) < 2*iSmallRectSize)
                {
                    m_bSelected = true;
                    // 测试寻路功能
                    PathManager.Instance.HomeID = m_VertexNode.ID;
                }
                else
                {
                    m_bSelected = false;
                }
            }

            if (e.Button == MouseButtons.Middle)
            {
                Point StartPoint = new Point();
                StartPoint.X = e.X;
                StartPoint.Y = e.Y;

                // 计算场景尺寸到 1024 纹理的缩放
                float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

                // 坐标的转换
                Point RealPos = new Point((int)(StartPoint.X / fScale), (int)(StartPoint.Y / fScale));
                RealPos.Y = m_iSceneHeight - RealPos.Y;

                PathManager.Instance.StartPoint = new Point(RealPos.X, RealPos.Y);
                PathManager.Instance.EndPoint = new Point(RealPos.X, RealPos.Y);
            }

            if(e.Button == MouseButtons.Right)
            {
                Point StartPoint = new Point();
                StartPoint.X = e.X;
                StartPoint.Y = e.Y;

                // 计算场景尺寸到 1024 纹理的缩放
                float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

                // 坐标的转换
                Point RealPos = new Point((int)(StartPoint.X / fScale), (int)(StartPoint.Y / fScale));
                RealPos.Y = m_iSceneHeight - RealPos.Y;


                int iMapID = m_VertexNode.MapID;//保留MapID，作为后面刷屏函数的参数
                VertexNode delVertex = PathManager.Instance.GetVertexByPos(iMapID, RealPos);
                DialogResult result = MessageBox.Show("是否删除第" + delVertex.ID.ToString() + "号节点", "提示", MessageBoxButtons.YesNo);
                if(result == DialogResult.Yes)
                {

                    PathManager.Instance.DelVertex(iMapID, delVertex);
                    MainForm.Instance.RefreshSceneSketch(iMapID);
                }
                else
                {
                    // 测试寻路功能
                    PathManager.Instance.DestinationID = delVertex.ID;
                }
            }
        }

        private void PathVertexControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && m_bSelected)
            {
                int newLeft = e.X - _DownLocation.X;
                int newTop = e.Y - _DownLocation.Y;


                float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);
                int dx = (int)(newLeft / fScale);
                int dy = (int)((newTop) / fScale);
                this.m_VertexNode.Move(dx, dy, m_iSceneWidth, m_iSceneHeight);



                if (dx != 0 || dy != 0)
                {
                    _DownLocation.X += (int)(dx * fScale);
                    _DownLocation.Y += (int)(dy * fScale);
                    //this.SetStyle(ControlStyles.Opaque, false);
                    //this.SetStyle(ControlStyles.Opaque, true);
                    this.Invalidate();
                }
                
                
            }
        }

        private void PathVertexControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                m_bSelected = false;
                _DownLocation = new System.Drawing.Point();
                MainForm.Instance.RefreshSceneSketch(m_VertexNode.MapID);
            }
            if (e.Button == MouseButtons.Middle)
            {
                Point StartPoint = new Point();
                StartPoint.X = e.X;
                StartPoint.Y = e.Y;

                // 计算场景尺寸到 1024 纹理的缩放
                float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

                // 坐标的转换
                Point RealPos = new Point((int)(StartPoint.X / fScale), (int)(StartPoint.Y / fScale));
                RealPos.Y = m_iSceneHeight - RealPos.Y;

                PathManager.Instance.EndPoint = RealPos;
                PathManager.Instance.Modify(m_VertexNode.MapID);
                MainForm.Instance.RefreshSceneSketch(m_VertexNode.MapID);

                PathManager.Instance.StartPoint = new Point(-100, -100);
                PathManager.Instance.EndPoint = new Point(-100, -100);
            }
        }
    }
}
