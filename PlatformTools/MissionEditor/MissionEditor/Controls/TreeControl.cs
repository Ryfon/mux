﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class TreeControl : UserControl
    {
        public TreeControl(TextBox text)
        {
            InitializeComponent();
            m_textBox = text;
        }

        private void TreeControl_Load(object sender, EventArgs e)
        {
            TreeNode root = m_treeView.Nodes.Add("任务类型");
            TreeNode node1 = root.Nodes.Add("计数器类");
            TreeNode node2 = root.Nodes.Add("对话类");
            TreeNode node3 = root.Nodes.Add("护送类");
            TreeNode node4 = root.Nodes.Add("探索类");
            //TreeNode node5 = root.Nodes.Add("职业类");

            node1.Nodes.Add("杀怪A类");
            //node1.Nodes.Add("杀怪B类");
            node1.Nodes.Add("收集物品A类");
            //node1.Nodes.Add("收集物品B类");
            node1.Nodes.Add("杀人A类");
            //node1.Nodes.Add("杀人B类");
            node1.Nodes.Add("交友A类");
            //node1.Nodes.Add("交友B类");
            node1.Nodes.Add("答题A类");

            node1.Nodes.Add("道具1型A类");
            node1.Nodes.Add("道具2型A类");

            node2.Nodes.Add("单纯对话A类");
            node2.Nodes.Add("单纯对话B类");
            node2.Nodes.Add("送物品A类");
            node2.Nodes.Add("送物品B类");
            //node2.Nodes.Add("取物品A类");
            node2.Nodes.Add("取物品B类");

            node3.Nodes.Add("护送A类");
            //node3.Nodes.Add("护送B类");

            //node4.Nodes.Add("探索A类");
            node4.Nodes.Add("探索B类");

            //node5.Nodes.Add("职业A类");
            //node5.Nodes.Add("职业B类");

            root.Expand();
        }

        private void m_buttonClose_Click(object sender, EventArgs e)
        {
            this.Parent.Hide();
        }

        private void m_buttonApply_Click(object sender, EventArgs e)
        {
            TreeNode node = m_treeView.SelectedNode;
            if(node != null)
            {
                if(node.Nodes.Count == 0)
                {
                    m_textBox.Text = node.Parent.Text + "_" + node.Text;
                }
            }

            this.Parent.Hide();
        }

        public void ChangeSelect()
        {
            if (m_textBox != null)
            {
                String strText = m_textBox.Text;
                if(strText.Contains("_"))
                {
                    int iIndex = strText.IndexOf("_");
                    String strType = strText.Substring(0, iIndex);
                    String strSubType = strText.Substring(iIndex + 1);

                    TreeNode root = m_treeView.TopNode;
                    if(root != null)
                    {
                        foreach(TreeNode node in root.Nodes)
                        {
                            if(node.Text.Equals(strType))
                            {
                                node.Expand();
                                foreach(TreeNode subNode in node.Nodes)
                                {
                                    if (subNode.Text.Equals(strSubType))
                                    {
                                        m_treeView.SelectedNode = subNode;
                                    }
                                }
                            }
                            else
                            {
                                node.Collapse();
                            }
                        }
                    }
                }
            }

            m_treeView.Focus();
        }
    }

    public class MissionType
    {
        // 接口
        public bool Convert(String strDisplay)
        {
            int iIndex = strDisplay.IndexOf('_');
            if (iIndex == -1)
            {
                return false;
            }
            else
            {
                int iIndex2 = strDisplay.Length - 2;
                m_strMainType = strDisplay.Substring(0, iIndex);
                m_strSubType = strDisplay.Substring(iIndex + 1, iIndex2 - iIndex - 1);
                m_strFlowType = strDisplay.Substring(iIndex2, 2);
                return true;
            }
        }

        // 属性
        public String MainType
        {
            get
            {
                return m_strMainType;
            }
            set
            {
                m_strMainType = value;
            }
        }
        public String SubType
        {
            get
            {
                return m_strSubType;
            }
            set
            {
                m_strSubType = value;
            }
        }
        public String FlowType
        {
            get
            {
                return m_strFlowType;
            }
            set
            {
                m_strFlowType = value;
            }
        }
        public String Display
        {
            get
            {
                return m_strMainType + "_" + m_strSubType + m_strFlowType;
            }
        }
        // 成员变量
        String m_strMainType = "";
        String m_strSubType = "";
        String m_strFlowType = "";
    };
}
