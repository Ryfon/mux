﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class DescEditControl : UserControl
    {
        public DescEditControl()
        {
            InitializeComponent();
            InitImages();   // 初始化按钮图片

        }

        private void InitImages()
        {
            toolStripButtonBold.Image = Image.FromFile(OptionManager.ImagePath + "boldhs.png");
            toolStripButtonColor.Image = Image.FromFile(OptionManager.ImagePath + "Color_fontHS.png");
            toolStripButtonEnter.Image = Image.FromFile(OptionManager.ImagePath + "AlignObjectsLeftHS.png");
            toolStripButtonPlayerName.Image = Image.FromFile(OptionManager.ImagePath + "HomeHS.png");
            toolStripButtonLink.Image = Image.FromFile(OptionManager.ImagePath + "WebInsertHyperlinkHS.png");
            toolStripButtonPreview.Image = Image.FromFile(OptionManager.ImagePath + "PrintPreviewHS.png");
        }

        public String DescText
        {
            get
            {
                return m_textBoxDesc.Text;
            }
            set
            {
                m_textBoxDesc.Text = value;
                m_webBrowser.DocumentText = AddText(value, 0, value.Length, "<font size='2'>", "</font>");
            }
        }

        private String AddText(String strSource, int iStartIndex, int iLength, String strPrefix, String strPostfix)
        {
            // 初始化结果
            String strResult = strSource;

            // 插入前缀
            strResult = strResult.Insert(iStartIndex, strPrefix);

            // 插入后缀
            strResult = strResult.Insert(iStartIndex + iLength + strPrefix.Length, strPostfix);

            return strResult;
        }

        private void SetButtonStatus(bool value)
        {
            toolStripButtonBold.Enabled = value;
            toolStripButtonColor.Enabled = value;
            toolStripButtonEnter.Enabled = value;
            toolStripButtonPlayerName.Enabled = value;
            toolStripButtonLink.Enabled = value;
        }

        private void toolStripButtonPreview_Click(object sender, EventArgs e)
        {
            toolStripButtonPreview.Checked = !toolStripButtonPreview.Checked;

            if (toolStripButtonPreview.Checked)
            {
                m_webBrowser.Visible = true;
                m_textBoxDesc.Visible = false;
                m_webBrowser.DocumentText = AddText(m_textBoxDesc.Text, 0, m_textBoxDesc.Text.Length, "<font size='2'>", "</font>");

                // 预览状态屏蔽编辑操作
                SetButtonStatus(false);
            }
            else
            {
                m_webBrowser.Visible = false;
                m_textBoxDesc.Visible = true;

                // 编辑状态开启编辑操作
                SetButtonStatus(true);
            }
        }

        private void toolStripButtonBold_Click(object sender, EventArgs e)
        {
            m_textBoxDesc.Text = AddText(m_textBoxDesc.Text, m_textBoxDesc.SelectionStart, m_textBoxDesc.SelectionLength, "<b>", "</b>");
        }

        private void toolStripButtonColor_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            DialogResult result = dlg.ShowDialog();
            if(result == DialogResult.OK)
            {
                Color color = dlg.Color;
                String strResult = color.R.ToString("x2") + color.G.ToString("x2") + color.B.ToString("x2");

                // 将颜色加入
                m_textBoxDesc.Text = AddText(m_textBoxDesc.Text, m_textBoxDesc.SelectionStart, m_textBoxDesc.SelectionLength, "<font color='#" + strResult +"'>", "</font>");
            }
        }

        private void toolStripButtonEnter_Click(object sender, EventArgs e)
        {
            // 计算出新的光标位置
            int iNewStart = m_textBoxDesc.SelectionStart + m_textBoxDesc.SelectionLength + 4;
            
            // 插入<br>换行符
            m_textBoxDesc.Text = AddText(m_textBoxDesc.Text, m_textBoxDesc.SelectionStart, m_textBoxDesc.SelectionLength, "", "<br>");

            // 恢复光标
            m_textBoxDesc.SelectionStart = iNewStart;
        }

        private void toolStripButtonPlayerName_Click(object sender, EventArgs e)
        {
            m_textBoxDesc.Text = AddText(m_textBoxDesc.Text, m_textBoxDesc.SelectionStart, m_textBoxDesc.SelectionLength, "<Player>", "");
        }

        private void toolStripButtonLink_Click(object sender, EventArgs e)
        {
            m_textBoxDesc.Text = AddText(m_textBoxDesc.Text, m_textBoxDesc.SelectionStart, m_textBoxDesc.SelectionLength, "<a href='asfunction:GotoMonster,'>", "</a>");
        }
    }
}
