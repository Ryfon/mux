﻿namespace MissionEditor
{
    partial class PointSoundControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_toolTipShowDisplay = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // PointSoundControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Name = "PointSoundControl";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PointSoundControl_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PointSoundControl_MouseMove);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.PointSoundControl_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PointSoundControl_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        PointSoundInfo m_SoundInfo = null;
        private int m_iSceneWidth;   // 所在场景的宽度
        private int m_iSceneHeight;
        private System.Windows.Forms.ToolTip m_toolTipShowDisplay;  // 所在场景高度
        private System.Drawing.Point _DownLocation = new System.Drawing.Point(); // 鼠标按下时的相对位置
        private System.Drawing.Point _DrawOffset = new System.Drawing.Point(0, 0); // 渲染时的偏移量
        private int _InnerRadiusOffset = 0;//内半径偏移
        private int _OuterRadiusOffset = 0;//外半径偏移
    }
}
