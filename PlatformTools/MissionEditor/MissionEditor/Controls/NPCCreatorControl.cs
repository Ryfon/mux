﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;
//using Microsoft.DirectX;
//using Microsoft.DirectX.Direct3D;

namespace MissionEditor
{
    // npc creator 对应控件
    public partial class NPCCreatorControl : BaseDrawControl
    {
        const int iSmallRectSize = 5;
        //Device device = null;

        public NPCCreatorControl(NPCCreatorInfo creator, int iSceneWidth, int iSceneHeight)
        {
            InitializeComponent();
            m_NPCCreator = creator;
            m_iSceneWidth = iSceneWidth;
            m_iSceneHeight = iSceneHeight;

            DrawNPC(null);
            //// 初始化DX设备
            
            //PresentParameters presentParams = new PresentParameters();
            //presentParams.Windowed = true;
            //presentParams.SwapEffect = SwapEffect.Discard;
            //device = new Device(0, DeviceType.Hardware, this, CreateFlags.SoftwareVertexProcessing, presentParams);
        }

        private void NPCCreatorControl_Load(object sender, EventArgs e)
        {
            m_Icon = Image.FromFile(OptionManager.MonsterIconPath);
        }

//         private void Render()
//         {
//             if (device == null)
//                 return;
// 
//             //Clear the backbuffer to a blue color 
//             device.Clear(ClearFlags.Target, System.Drawing.Color.FromArgb(64, 0, 64, 0), 1.0f, 0);//System.Drawing.Color.Red
//             //Begin the scene
//             device.BeginScene();
// 
//             // Rendering of scene objects can happen here
// 
//             //End the scene
//             device.EndScene();
//             device.Present();
//         }
        public override void DrawOnBmp(Graphics g, int iBmpWidth, int iBmpHeight)
        {
            // 原场景原点在左下角,场景草图的屏幕坐标原点在左上.所以绘制应该上下颠倒.
            // 现不考虑该问题,反向绘制场景
            float fZoom = OptionManager.SceneSketchZoom;

            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath PointPath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath RangePath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            // 从 场景坐标转换到屏幕坐标。（从左下原点坐标系转换到左上原点坐标系）
            Rectangle rectScreen = new Rectangle(m_NPCCreator.Rectangle.Left, m_iSceneHeight - m_NPCCreator.Rectangle.Top - m_NPCCreator.Rectangle.Height,
                                                        m_NPCCreator.Rectangle.Width, m_NPCCreator.Rectangle.Height);
            // 计算场景尺寸到纹理的缩放
            float fScale = iBmpHeight / System.Math.Max(m_iSceneHeight, m_iSceneWidth);
            Rectangle rectTexture = new Rectangle((int)(rectScreen.X * fScale), (int)(rectScreen.Y * fScale),
                (int)(rectScreen.Width * fScale), (int)(rectScreen.Height * fScale));

            // 计算寻路范围框
            int iRangeSize = m_NPCCreator.MoveRange;
            int iScreenRangeSize = (int)(iRangeSize * fScale);
            Rectangle rectRange = new Rectangle((int)((rectScreen.X - iRangeSize) * fScale),
                                                (int)((rectScreen.Y - iRangeSize) * fScale),
                                                (int)((rectScreen.Width + iRangeSize + iRangeSize) * fScale),
                                                (int)((rectScreen.Height + iRangeSize + iRangeSize) * fScale));
            Point[] plist = new Point[4];
            plist[0] = new Point(rectRange.Left, rectRange.Top);
            plist[1] = new Point(rectRange.Right, rectRange.Top);
            plist[2] = new Point(rectRange.Right, rectRange.Bottom);
            plist[3] = new Point(rectRange.Left, rectRange.Bottom);

            //显示NPC名称
            FontFamily family = new FontFamily("黑体");
            int fontStyle = (int)FontStyle.Italic;
            int emSize = 14;
            Point origin = new Point(rectTexture.Left - iScreenRangeSize, rectTexture.Top - emSize - iScreenRangeSize);
            StringFormat format = StringFormat.GenericDefault;
            path.AddString(NPCCreator.Name, family, fontStyle, emSize, origin, format);

            Color color;
            if (rectTexture.Width == 0 || rectTexture.Height == 0)
            {
                int iNpcSize = OptionManager.SingleNpcSize;
                rectTexture = new Rectangle((int)(rectTexture.Left - iNpcSize + fScale / 2), (int)(rectTexture.Top - iNpcSize - fScale / 2), 2 * iNpcSize, 2 * iNpcSize);
                path.AddEllipse(rectTexture);

                if (m_Selected)
                {
                    const int iRange = 10;
                    const float fRangeSize = 10.0f;
                    float fDirection = (float)(m_NPCCreator.Direction - 90) - fRangeSize / 2;
                    PointPath.AddPie(rectTexture.Left - iRange, rectTexture.Top - iRange, 2 * (iRange + iNpcSize), 2 * (iRange + iNpcSize), fDirection, fRangeSize);
                    path.AddPie(rectTexture.Left - iRange, rectTexture.Top - iRange, 2 * (iRange + iNpcSize), 2 * (iRange + iNpcSize), fDirection, fRangeSize);

                    color = Color.Green; // npc 
                }
                else
                {
                    color = Color.Green;
                }

                //path.AddEllipse(rectTexture.Left, rectTexture.Top, rectTexture.Left - 150, rectTexture.Top - 150);
            }
            else
            {
                if (m_Selected)
                {
                    //color = Color.Red; // monster
                    color = GetColorByAggressive();
                }
                else
                {
                    //color = Color.Red;
                    color = GetColorByAggressive();
                }
                path.AddRectangle(rectTexture);
                // 添加拖动小方点
                if (m_Selected)
                {
                    path.AddRectangle(new Rectangle(rectTexture.Right - iSmallRectSize, rectTexture.Bottom - iSmallRectSize, 2 * iSmallRectSize, 2 * iSmallRectSize));
                    if (m_bResizing)
                    {
                        PointPath.AddRectangle(new Rectangle(rectTexture.Right - iSmallRectSize, rectTexture.Bottom - iSmallRectSize, 2 * iSmallRectSize, 2 * iSmallRectSize));
                    }
                }
                // 添加寻路范围框
                path.AddPath(GetLineGraphicPath(plist[0], plist[1]), false);
                path.AddPath(GetLineGraphicPath(plist[1], plist[2]), false);
                path.AddPath(GetLineGraphicPath(plist[2], plist[3]), false);
                path.AddPath(GetLineGraphicPath(plist[3], plist[0]), false);
            }

            // 高亮显示
            if (MainForm.Instance.bNpcHighLight)
            {
                if (m_NPCCreator.Contains(MainForm.Instance.StatisticsNpcID) > 0)
                {
                    color = Color.Yellow;
                }
            }

            SolidBrush brush = new SolidBrush(color);
            Pen GreenPen = new Pen(Color.Green);

            if (g != null)
            {
                g.FillPath(brush, path);
            }

            brush.Dispose();
            GreenPen.Dispose();
        }

        public void DrawNPC(Graphics g)
        {
            // 原场景原点在左下角,场景草图的屏幕坐标原点在左上.所以绘制应该上下颠倒.
            // 现不考虑该问题,反向绘制场景
            float fZoom = OptionManager.SceneSketchZoom;

            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath PointPath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath RangePath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            // 从 场景坐标转换到屏幕坐标。（从左下原点坐标系转换到左上原点坐标系）
            Rectangle rectScreen = new Rectangle(m_NPCCreator.Rectangle.Left, m_iSceneHeight - m_NPCCreator.Rectangle.Top - m_NPCCreator.Rectangle.Height,
                                                        m_NPCCreator.Rectangle.Width, m_NPCCreator.Rectangle.Height);
            // 计算场景尺寸到纹理的缩放
            float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);
            Rectangle rectTexture = new Rectangle((int)(rectScreen.X * fScale), (int)(rectScreen.Y * fScale),
                (int)(rectScreen.Width * fScale), (int)(rectScreen.Height * fScale));

            // 计算寻路范围框
            int iRangeSize = m_NPCCreator.MoveRange;
            int iScreenRangeSize = (int)(iRangeSize * fScale);
            Rectangle rectRange = new Rectangle((int)((rectScreen.X - iRangeSize) * fScale),
                                                (int)((rectScreen.Y - iRangeSize) * fScale),
                                                (int)((rectScreen.Width + iRangeSize + iRangeSize) * fScale),
                                                (int)((rectScreen.Height + iRangeSize + iRangeSize) * fScale));
            Point[] plist = new Point[4];
            plist[0] = new Point(rectRange.Left, rectRange.Top);
            plist[1] = new Point(rectRange.Right, rectRange.Top);
            plist[2] = new Point(rectRange.Right, rectRange.Bottom);
            plist[3] = new Point(rectRange.Left, rectRange.Bottom);

            //显示NPC名称
            FontFamily family = new FontFamily("黑体");
            int fontStyle = (int)FontStyle.Italic;
            int emSize = 14;
            Point origin = new Point(rectTexture.Left - iScreenRangeSize, rectTexture.Top - emSize - iScreenRangeSize);
            StringFormat format = StringFormat.GenericDefault;
            path.AddString(NPCCreator.Name, family, fontStyle, emSize, origin, format);

            Color color;
            if (rectTexture.Width == 0 || rectTexture.Height == 0)
            {
                int iNpcSize = OptionManager.SingleNpcSize;
                rectTexture = new Rectangle((int)(rectTexture.Left - iNpcSize + fScale / 2), (int)(rectTexture.Top - iNpcSize - fScale / 2), 2 * iNpcSize, 2 * iNpcSize);
                path.AddEllipse(rectTexture);

                if (m_Selected)
                {
                    const int iRange = 10;
                    const float fRangeSize = 10.0f;
                    float fDirection = (float)(m_NPCCreator.Direction - 90) - fRangeSize / 2;
                    PointPath.AddPie(rectTexture.Left - iRange, rectTexture.Top - iRange, 2 * (iRange + iNpcSize), 2 * (iRange + iNpcSize), fDirection, fRangeSize);
                    path.AddPie(rectTexture.Left - iRange, rectTexture.Top - iRange, 2 * (iRange + iNpcSize), 2 * (iRange + iNpcSize), fDirection, fRangeSize);

                    color = Color.Yellow; // npc
                }
                else
                {
                    color = Color.Green; 
                }

                //path.AddEllipse(rectTexture.Left, rectTexture.Top, rectTexture.Left - 150, rectTexture.Top - 150);
            }
            else
            {
                if (m_Selected)
                {
                    color = Color.Yellow; // monster
                }
                else
                {
                    //color = Color.Red;
                    color = GetColorByAggressive();
                }
                path.AddRectangle(rectTexture);
                // 添加拖动小方点
                if (m_Selected)
                {
                    path.AddRectangle(new Rectangle(rectTexture.Right - iSmallRectSize, rectTexture.Bottom - iSmallRectSize, 2 * iSmallRectSize, 2 * iSmallRectSize));
                    if (m_bResizing)
                    {
                        PointPath.AddRectangle(new Rectangle(rectTexture.Right - iSmallRectSize, rectTexture.Bottom - iSmallRectSize, 2 * iSmallRectSize, 2 * iSmallRectSize));
                    }
                }
                // 添加寻路范围框
                path.AddPath(GetLineGraphicPath(plist[0], plist[1]), false);
                path.AddPath(GetLineGraphicPath(plist[1], plist[2]), false);
                path.AddPath(GetLineGraphicPath(plist[2], plist[3]), false);
                path.AddPath(GetLineGraphicPath(plist[3], plist[0]), false);

                RangePath.AddPath(GetLineGraphicPath(plist[0], plist[1]), false);
                RangePath.AddPath(GetLineGraphicPath(plist[1], plist[2]), false);
                RangePath.AddPath(GetLineGraphicPath(plist[2], plist[3]), false);
                RangePath.AddPath(GetLineGraphicPath(plist[3], plist[0]), false);
            }

            RectangleF locationbound = path.GetBounds();
            path.Transform(new Matrix(1.0f, 0.0f, 0.0f, 1.0f, -locationbound.Left, -locationbound.Top - 1));
            PointPath.Transform(new Matrix(1.0f, 0.0f, 0.0f, 1.0f, -locationbound.Left, -locationbound.Top - 1));

            if (MainForm.Instance.SpeedMode)
            {
                this.Region = new Region(path.GetBounds());
            }
            else
            {
                this.Region = new Region(path);
            }

            if(m_NPCCreator.IsBlock())
            {
                if(m_Selected)
                {
                    color = Color.LightGray;
                }
                else
                {
                    color = Color.Gray;
                }
            }

            // 高亮显示
            if(MainForm.Instance.bNpcHighLight)
            {
                if(m_NPCCreator.Contains(MainForm.Instance.StatisticsNpcID) > 0)
                {
                    color = Color.Yellow;
                }
            }

            SolidBrush brush = new SolidBrush(color);
            Pen pen = new Pen(color, 1.0f);
            SolidBrush RedBrush = new SolidBrush(Color.Red);
            Pen GreenPen = new Pen(Color.Green);
            RectangleF bound = path.GetBounds();
            //this.Left = 0; // (int)(bound.Location.X);
            //this.Top = 0; // (int)(bound.Location.Y);
            //this.Width = (int)(bound.Location.X + bound.Width);//
            //this.Height = (int)(bound.Location.Y + bound.Height);//
            this.Left = (int)(locationbound.Location.X + 0.5f);
            this.Top = (int)(locationbound.Location.Y + 0.5f);
            this.Width = (int)(locationbound.Width + 0.5f);//
            this.Height = (int)(locationbound.Height + 0.5f);//

            if(g != null)
            {
                // 显示区域主体
                if (m_bTransparent)
                {
                    // 透明显示
                    g.DrawPath(pen, path);
                }
                else
                {
                    // 不透明显示
                    g.FillPath(brush, path);
                }

                // 显示点
                g.FillPath(RedBrush, PointPath);

                // 显示寻路范围
                g.FillPath(brush, RangePath);
            }
            
            brush.Dispose();
        }
        private void NPCCreatorControl_Paint(object sender, PaintEventArgs e)
        {
            
            if(m_NPCCreator.Hidden)
            {
                this.Hide();
                return;
            }

            DrawNPC(e.Graphics);
        }

        private GraphicsPath GetLineGraphicPath(Point _StartPoint, Point _EndPoint)
        {
            PointF vDir = new PointF();
            vDir.X = _EndPoint.X - _StartPoint.X;
            vDir.Y = _EndPoint.Y - _StartPoint.Y;

            float fLen = (float)System.Math.Sqrt(vDir.X * vDir.X + vDir.Y * vDir.Y);
            if (fLen <= float.Epsilon)
            {
                return null;
            }

            vDir.X /= fLen;
            vDir.Y /= fLen;

            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            for (int i = 0; i <= fLen; i += 2)
            {
                PointF p = new PointF(_StartPoint.X + vDir.X * i, _StartPoint.Y + vDir.Y * i);
                path.AddRectangle(new RectangleF(p.X, p.Y, 1, 1));
            }

            return path;
        }

        // 根据怪物是否主动攻击，来显示颜色
        private Color GetColorByAggressive()
        {
            Color color = Color.Pink;    // 初始为粉红色
            int iIsAggressive = m_NPCCreator.IsAggressive();

            if (iIsAggressive == 0)
            {
                color = Color.OrangeRed;   // 非主动攻击怪为橙色
            }
            else if (iIsAggressive == 1)
            {
                color = Color.Red;   // 主动攻击怪为红色
            }

            return color;
        }

        private void NPCCreatorControl_MouseDown(object sender, MouseEventArgs e)
        {
            // 用户在当前控件上按下
            if (e.Button == MouseButtons.Left)
            {
                _DownLocation.X = e.X;
                _DownLocation.Y = e.Y;
                _iLastMoveDx = 0;
                _iLastMoveDy = 0;

                float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);
                //int iX = (int)(m_NPCCreator.Rectangle.Right * fScale);
                //int iY = (int)((m_iSceneHeight - m_NPCCreator.Rectangle.Top) * fScale);
                int iX = (int)((m_NPCCreator.Rectangle.Width + m_NPCCreator.MoveRange) * fScale);
                int iY = (int)((m_NPCCreator.Rectangle.Height + m_NPCCreator.MoveRange) * fScale) + 14;

                int iSize = iSmallRectSize;

                if (System.Math.Abs(e.X - iX) < iSize && System.Math.Abs(e.Y - iY) < iSize)
                {
                    if (m_NPCCreator.Rectangle.Height == 0 || m_NPCCreator.Rectangle.Width == 0)
                    {
                        m_bResizing = false;
                    }
                    else
                    {
                        m_bResizing = true;
                    }
                }
                else
                {
                    m_bResizing = false;
                }


                if (MainForm.Instance.SelectedNPCCreator != null)
                {
                    MainForm.Instance.SelectedNPCCreator.Invalidate();
                }

                MainForm.Instance.SelectedNPCCreator = this;

                // 设置NPC所处的层
                if (m_iNpcLayer == 1)
                {
                    MainForm.Instance.SetNpcLayer(m_NPCCreator.ID, 3);
                    m_iNpcLayer = 3;
                }
                else
                {
                    MainForm.Instance.SetNpcLayer(m_NPCCreator.ID, 1);
                    m_iNpcLayer = 1;
                }

                this.Invalidate();
            }
        }

        // 当前控件是否被选择
        public bool Selected
        {
            get
            {
                return m_Selected;
            }
            set
            {
                m_Selected = value;
            }
        }

        public NPCCreatorInfo NPCCreator
        {
            get
            {
                return m_NPCCreator;
            }
        }

        public bool bResizing
        {
            get
            {
                return m_bResizing;
            }
            set
            {
                m_bResizing = value;
            }
        }

        public Point DownLocation
        {
            get
            {
                return _DownLocation;
            }
            set
            {
                _DownLocation = value;
            }
        }
        public void NPCCreatorControl_MouseMove(object sender, MouseEventArgs e)
        {
            bool bBlockCheck = !(System.Windows.Forms.Control.ModifierKeys == Keys.Control);
            // 显示鼠标当前的坐标
            float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);
            String strLocation = ",";

            if (e.Button == MouseButtons.Left && m_Selected)
            {
                int newLeft = e.X - _DownLocation.X;
                int newTop = e.Y - _DownLocation.Y;

                int dx = (int)(newLeft / fScale);
                int dy = (int)((newTop) / fScale);

                if(m_bResizing)
                {
                    /*bool bSizeOver = */m_NPCCreator.ReSize((int)(newLeft / fScale), (int)((newTop) / fScale));
                    //if (bBlockCheck)//是否按下ctrl
                    //{
                    //    if (bSizeOver)
                    //    {
                    //        m_NPCCreator.ReSize(-(int)(newLeft / fScale), -(int)((newTop) / fScale));
                    //    }
                    //}
                }
                else
                {
                    bool bInBlock = m_NPCCreator.Move(dx, dy, m_iSceneWidth, m_iSceneHeight);
                    if (bBlockCheck)//是否按下ctrl
                    {
                        if (bInBlock)
                        {
                            m_NPCCreator.Move(-dx, -dy, m_iSceneWidth, m_iSceneHeight);
                        }
                        else
                        {
                            _iLastMoveDx += dx;
                            _iLastMoveDy += dy;
                        }
                    }
                    else
                    {
                        _iLastMoveDx += dx;
                        _iLastMoveDy += dy;
                    }
                }
                if (dx != 0 || dy != 0)
                {
                    if(m_bResizing)
                    {
                        _DownLocation.X += (int)(dx * fScale);
                        _DownLocation.Y += (int)(dy * fScale);
                    }
                    this.Invalidate();
                }

                strLocation = this.m_NPCCreator.Rectangle.X + "," + this.m_NPCCreator.Rectangle.Y;
            }
            else
            {
                int iX = (int)((e.X + this.Left)/ fScale);
                int iY = (int)((e.Y + this.Top)/ fScale);
                iY = m_iSceneHeight - iY - 1;
                strLocation = iX.ToString() + "," + iY.ToString();
            }

            MainForm.Instance.Text = strLocation;
        }

        private void NPCCreatorControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (m_Selected)
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult result = MessageBox.Show("确认删除---" + m_NPCCreator.Name, "提示", MessageBoxButtons.YesNo);
                    if(result == DialogResult.Yes)
                    {
                        NPCManager.Instance.DelNPCCreator(NPCCreator.ID);
                        MainForm.Instance.SelectedNPCCreator = null;
                    }
                }

                if (e.KeyCode == Keys.Space)
                {
                    m_NPCCreator.SetAreaMethod();
                    this.Invalidate();
                }

                // 复制
                if (e.KeyCode == Keys.C)
                {
                    if(e.Control)// 复制
                    {
                        NPCCreatorInfo npc = NPCManager.Instance.CloneNPCCreator(m_NPCCreator.ID);
                        if(npc != null)
                        {
                            MainForm.Instance.AddNPCCreator(npc);
                            //MainForm.Instance.RefreshSceneSketchImage(m_NPCCreator.MapID);
                        }
                        //NPCManager.Instance.CloneNPCCreator(m_NPCCreator.ID);
                        //MainForm.Instance.RefreshSceneSketch(m_NPCCreator.MapID);
                    }
                }

                if (e.KeyCode == Keys.W)//向上
                {
                    if (e.Shift)
                    {
                        m_NPCCreator.ReSize(0, -1);
                    }
                    else
                    {
                        m_NPCCreator.Move(0, -1, m_iSceneWidth, m_iSceneHeight);
                        if (m_NPCCreator.IsBlock() == true)
                        {
                            //MessageBox.Show(m_NPCCreator.Name + "---位置错误，进入碰撞区域！");
                            m_NPCCreator.Move(0, 1, m_iSceneWidth, m_iSceneHeight);
                        }
                    }
                    
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.S)//向下
                {
                    if (e.Shift)
                    {
                        m_NPCCreator.ReSize(0, 1);
                    }
                    else
                    {
                        m_NPCCreator.Move(0, 1, m_iSceneWidth, m_iSceneHeight);
                        if (m_NPCCreator.IsBlock() == true)
                        {
                            //MessageBox.Show(m_NPCCreator.Name + "---位置错误，进入碰撞区域！");
                            m_NPCCreator.Move(0, -1, m_iSceneWidth, m_iSceneHeight);
                        }
                    }
                   
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.A)//向左
                {
                    if (e.Shift)
                    {
                        m_NPCCreator.ReSize(-1, 0);
                    }
                    else
                    {
                        m_NPCCreator.Move(-1, 0, m_iSceneWidth, m_iSceneHeight);
                        if (m_NPCCreator.IsBlock() == true)
                        {
                            //MessageBox.Show(m_NPCCreator.Name + "---位置错误，进入碰撞区域！");
                            m_NPCCreator.Move(1, 0, m_iSceneWidth, m_iSceneHeight);
                        }
                    }
                    
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.D)//向右
                {
                    if(e.Shift)
                    {
                        m_NPCCreator.ReSize(1, 0);
                    }
                    else
                    {
                        m_NPCCreator.Move(1, 0, m_iSceneWidth, m_iSceneHeight);
                        if (m_NPCCreator.IsBlock() == true)
                        {
                            //MessageBox.Show(m_NPCCreator.Name + "---位置错误，进入碰撞区域！");
                            m_NPCCreator.Move(-1, 0, m_iSceneWidth, m_iSceneHeight);
                        }
                    }
                    
                    this.Invalidate();
                }
                if(e.KeyCode == Keys.Q)// 逆时针旋转
                {
                    m_NPCCreator.Whirl(-1);
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.E)// 顺时针旋转
                {
                    m_NPCCreator.Whirl(1);
                    this.Invalidate();
                }

                if (e.KeyCode == Keys.T)     // 透明显示
                {
                    m_bTransparent = !m_bTransparent;
                    this.Invalidate();
                }

                #region 小键盘控制朝向
                if (e.KeyCode == Keys.NumPad1)
                {
                    m_NPCCreator.Direction = 225;
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.NumPad2)
                {
                    m_NPCCreator.Direction = 180;
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.NumPad3)
                {
                    m_NPCCreator.Direction = 135;
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.NumPad4)
                {
                    m_NPCCreator.Direction = 270;
                    this.Invalidate();
                }
                
                if (e.KeyCode == Keys.NumPad6)
                {
                    m_NPCCreator.Direction = 90;
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.NumPad7)
                {
                    m_NPCCreator.Direction = 315;
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.NumPad8)
                {
                    m_NPCCreator.Direction = 0;
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.NumPad9)
                {
                    m_NPCCreator.Direction = 45;
                    this.Invalidate();
                }
                #endregion
                
            }
            // 显示当前位置
            MainForm.Instance.Text = this.m_NPCCreator.Rectangle.X + "," + this.m_NPCCreator.Rectangle.Y;
        }

        private void m_MenuItemDialog_Click(object sender, EventArgs e)
        {
            // 与该 npc 对话
            if ( m_NPCCreator==null )
            {
                return;
            }

            Player.Instance.TalkTo(m_NPCCreator.NPC0);
        }

        private void m_MenuItemKill_Click(object sender, EventArgs e)
        {
            // 与该 npc 对话
            if (m_NPCCreator == null)
            {
                return;
            }

            if (m_NPCCreator.NPC0 > 0)
            {
                Player.Instance.Kill(m_NPCCreator.NPC0);
            }
            if (m_NPCCreator.NPC1 > 0)
            {
                Player.Instance.Kill(m_NPCCreator.NPC1);
            }
            if (m_NPCCreator.NPC2 > 0)
            {
                Player.Instance.Kill(m_NPCCreator.NPC2);
            }
            if (m_NPCCreator.NPC3 > 0)
            {
                Player.Instance.Kill(m_NPCCreator.NPC3);
            }

        }

        private void NPCCreatorControl_MouseUp(object sender, MouseEventArgs e)
        {
            //if (m_NPCCreator.IsBlock() == true)
            //{
            //    MessageBox.Show(m_NPCCreator.Name + "---位置错误，进入碰撞区域！");
            //    m_NPCCreator.Move(-_iLastMoveDx, -_iLastMoveDy, m_iSceneWidth, m_iSceneHeight);
            //    this.Invalidate();
            //}
        }
    }

    public class BaseDrawControl : UserControl
    {
        public virtual void DrawOnBmp(Graphics g, int iBmpWidth, int iBmpHeight){}
    };
}
