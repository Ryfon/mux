﻿using System.Drawing;

namespace MissionEditor
{
    partial class NPCCreatorControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_MenuItemDialog = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuItemKill = new System.Windows.Forms.ToolStripMenuItem();
            this.m_contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_contextMenuStrip
            // 
            this.m_contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_MenuItemDialog,
            this.m_MenuItemKill});
            this.m_contextMenuStrip.Name = "m_contextMenuStrip";
            this.m_contextMenuStrip.Size = new System.Drawing.Size(95, 48);
            // 
            // m_MenuItemDialog
            // 
            this.m_MenuItemDialog.Name = "m_MenuItemDialog";
            this.m_MenuItemDialog.Size = new System.Drawing.Size(94, 22);
            this.m_MenuItemDialog.Text = "对话";
            this.m_MenuItemDialog.Click += new System.EventHandler(this.m_MenuItemDialog_Click);
            // 
            // m_MenuItemKill
            // 
            this.m_MenuItemKill.Name = "m_MenuItemKill";
            this.m_MenuItemKill.Size = new System.Drawing.Size(94, 22);
            this.m_MenuItemKill.Text = "杀死";
            this.m_MenuItemKill.Click += new System.EventHandler(this.m_MenuItemKill_Click);
            // 
            // NPCCreatorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Name = "NPCCreatorControl";
            this.Size = new System.Drawing.Size(62, 58);
            this.Load += new System.EventHandler(this.NPCCreatorControl_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NPCCreatorControl_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.NPCCreatorControl_MouseMove);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.NPCCreatorControl_Paint);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NPCCreatorControl_MouseUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NPCCreatorControl_KeyDown);
            this.m_contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private NPCCreatorInfo m_NPCCreator = null;
        private System.Drawing.Image m_Icon;
        private int m_iSceneWidth;   // 所在场景的宽度
        private int m_iSceneHeight;  // 所在场景高度
        private bool m_Selected = false;        // 当前 creator 是否被选择.
        private Point _DownLocation = new System.Drawing.Point(); // 鼠标按下时的相对位置
        private bool m_bResizing = false;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem m_MenuItemDialog;
        private System.Windows.Forms.ToolStripMenuItem m_MenuItemKill;       // 是否处于改变大小状态
        private int _iLastMoveDx = 0;// 记录移动的偏移量，便于恢复
        private int _iLastMoveDy = 0;
        private int m_iNpcLayer = 2;    // 记录NPC所处的层，1为置顶，2为普通，3为置底
        private bool m_bTransparent = false;    // 区域是否透明
    }
}
