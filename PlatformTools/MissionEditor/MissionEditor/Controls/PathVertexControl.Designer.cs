﻿using System.Drawing;

namespace MissionEditor
{
    partial class PathVertexControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // PathVertexControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "PathVertexControl";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PathVertexControl_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PathVertexControl_MouseMove);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.PathVertexControl_Paint);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PathVertexControl_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

        private VertexNode m_VertexNode = null;
        private int m_iSceneWidth;   // 所在场景的宽度
        private int m_iSceneHeight;  // 所在场景高度
        private Point _DownLocation = new System.Drawing.Point(); // 鼠标按下时的相对位置
        private bool m_bSelected = false;
    }
}
