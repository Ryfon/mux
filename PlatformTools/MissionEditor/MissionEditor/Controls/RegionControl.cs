﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class RegionControl : BaseDrawControl
    {
        const int RectSize = 5;
        const int CircleSize = 6;

        public RegionControl(RegionInfo region, int iSceneWidth, int iSceneHeight)
        {
            InitializeComponent();
            //this.SetStyle(ControlStyles.Opaque, true);
            m_RegionInfo = region;
            m_iSceneWidth = iSceneWidth;
            m_iSceneHeight = iSceneHeight;
            DrawRegion(null);
        }

        public override void DrawOnBmp(Graphics g, int iBmpWidth, int iBmpHeight)
        {
            // 原场景原点在左下角,场景草图的屏幕坐标原点在左上.所以绘制应该上下颠倒.
            // 现不考虑该问题,反向绘制场景
            float fZoom = OptionManager.SceneSketchZoom;

            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath Pointpath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath Vertexpath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            // 计算场景尺寸到  纹理的缩放
            float fScale = iBmpHeight / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

            //显示NPC名称
            Rectangle rectTexture = m_RegionInfo.GetBound();
            FontFamily family = new FontFamily("黑体");
            int fontStyle = (int)FontStyle.Italic;
            int emSize = 16;
            int iOX = (int)(rectTexture.Left * fScale);
            int iOY = (int)((m_iSceneHeight - rectTexture.Bottom) * fScale) - emSize;
            Point origin = new Point(iOX, iOY);
            StringFormat format = StringFormat.GenericDefault;
            path.AddString(m_RegionInfo.Name, family, fontStyle, emSize, origin, format);


            ArrayList list = m_RegionInfo.PointList;
            Point[] newPointList = new Point[list.Count / 2];
            int iListIndex = 0;
            foreach (RegionPointInfo pointInfo in list)
            {
                int iX = (int)(pointInfo.Pos.X * fScale);
                int iY = (int)((m_iSceneHeight - pointInfo.Pos.Y) * fScale);
                Point tempPoint = new Point(iX, iY);

                if (pointInfo.Type == 0)
                {
                    newPointList.SetValue(tempPoint, iListIndex / 2);
                }


                // 如果在选中状态，则渲染顶点
                if (m_Selected)
                {
                    switch (pointInfo.Type)
                    {
                        case 0:
                            if (m_ShowVertex)
                            {
                                path.AddRectangle(new Rectangle(tempPoint.X - RectSize, tempPoint.Y - RectSize, RectSize * 2, RectSize * 2));
                                Vertexpath.AddRectangle(new Rectangle(tempPoint.X - RectSize, tempPoint.Y - RectSize, RectSize * 2, RectSize * 2));
                            }
                            break;
                        case 1:
                            if (m_OperMode && m_RegionInfo.Shape == 0)
                            {
                                path.AddEllipse(new Rectangle(tempPoint.X - CircleSize, tempPoint.Y - CircleSize, CircleSize * 2, CircleSize * 2));
                                Pointpath.AddEllipse(new Rectangle(tempPoint.X - CircleSize, tempPoint.Y - CircleSize, CircleSize * 2, CircleSize * 2));
                            }
                            break;
                        default:

                            break;
                    }
                    if (pointInfo.Selected)
                    {
                        switch (pointInfo.Type)
                        {
                            case 0:
                                Pointpath.AddRectangle(new Rectangle(tempPoint.X - RectSize, tempPoint.Y - RectSize, RectSize * 2, RectSize * 2));
                                break;
                            case 1:
                                if (m_OperMode && m_RegionInfo.Shape == 0)
                                {
                                    Pointpath.AddEllipse(new Rectangle(tempPoint.X - CircleSize, tempPoint.Y - CircleSize, CircleSize * 2, CircleSize * 2));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
                iListIndex++;
            }

            path.AddPolygon(newPointList);

            // 获取当前区域的颜色
            Color color = Color.Purple;
            RegionGroup bGroup = RegionManager.Instance.GetGroupByID(m_RegionInfo.GroupID);
            if (bGroup != null)
            {
                color = bGroup.RegionColor;
            }

            // 设置画笔和画刷
            SolidBrush brush = new SolidBrush(color);
            Pen pen = new Pen(color, 1.0f);
            SolidBrush RedBrush = new SolidBrush(Color.Red);

            if (g != null)
            {
                // 不透明显示
                g.FillPath(brush, path);
            }

            brush.Dispose();
            path.Dispose();
            RedBrush.Dispose();
            Pointpath.Dispose();
        }

        public void DrawRegion(Graphics g)
        {
            // 原场景原点在左下角,场景草图的屏幕坐标原点在左上.所以绘制应该上下颠倒.
            // 现不考虑该问题,反向绘制场景
            float fZoom = OptionManager.SceneSketchZoom;

            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath Pointpath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath Vertexpath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            // 计算场景尺寸到  纹理的缩放
            float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

            //显示NPC名称
            Rectangle rectTexture = m_RegionInfo.GetBound();
            FontFamily family = new FontFamily("黑体");
            int fontStyle = (int)FontStyle.Italic;
            int emSize = 16;
            int iOX = (int)(rectTexture.Left * fScale);
            int iOY = (int)((m_iSceneHeight - rectTexture.Bottom) * fScale) - emSize;
            Point origin = new Point(iOX, iOY);
            StringFormat format = StringFormat.GenericDefault;
            path.AddString(m_RegionInfo.Name, family, fontStyle, emSize, origin, format);


            ArrayList list = m_RegionInfo.PointList;
            Point[] newPointList = new Point[list.Count / 2];
            int iListIndex = 0;
            foreach (RegionPointInfo pointInfo in list)
            {
                int iX = (int)(pointInfo.Pos.X * fScale);
                int iY = (int)((m_iSceneHeight - pointInfo.Pos.Y) * fScale);
                Point tempPoint = new Point(iX, iY);

                if (pointInfo.Type == 0)
                {
                    newPointList.SetValue(tempPoint, iListIndex / 2);
                }


                // 如果在选中状态，则渲染顶点
                //if (m_Selected)   // 可能会造成偏移问题，故取消
                {
                    switch (pointInfo.Type)
                    {
                        case 0:
                            if (m_ShowVertex)
                            {
                                path.AddRectangle(new Rectangle(tempPoint.X - RectSize, tempPoint.Y - RectSize, RectSize * 2, RectSize * 2));
                                Vertexpath.AddRectangle(new Rectangle(tempPoint.X - RectSize, tempPoint.Y - RectSize, RectSize * 2, RectSize * 2));
                            }
                            break;
                        case 1:
                            if (m_OperMode && m_RegionInfo.Shape == 0)
                            {
                                path.AddEllipse(new Rectangle(tempPoint.X - CircleSize, tempPoint.Y - CircleSize, CircleSize * 2, CircleSize * 2));
                                Pointpath.AddEllipse(new Rectangle(tempPoint.X - CircleSize, tempPoint.Y - CircleSize, CircleSize * 2, CircleSize * 2));
                            }
                            break;
                        default:

                            break;
                    }
                    if (pointInfo.Selected)
                    {
                        switch (pointInfo.Type)
                        {
                            case 0:
                                Pointpath.AddRectangle(new Rectangle(tempPoint.X - RectSize, tempPoint.Y - RectSize, RectSize * 2, RectSize * 2));
                                break;
                            case 1:
                                if (m_OperMode && m_RegionInfo.Shape == 0)
                                {
                                    Pointpath.AddEllipse(new Rectangle(tempPoint.X - CircleSize, tempPoint.Y - CircleSize, CircleSize * 2, CircleSize * 2));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
                iListIndex++;
            }

            path.AddPolygon(newPointList);

            // 获取控件的包围框
            RectangleF bound = path.GetBounds();
            path.Transform(new Matrix(1.0f, 0.0f, 0.0f, 1.0f, -bound.Left, -bound.Top - 1));
            Vertexpath.Transform(new Matrix(1.0f, 0.0f, 0.0f, 1.0f, -bound.Left, -bound.Top - 1));
            Pointpath.Transform(new Matrix(1.0f, 0.0f, 0.0f, 1.0f, -bound.Left, -bound.Top - 1));

            // 设置刷新区域
            if (m_RegionInfo.Shape == 0)
            {
                this.Region = new Region(path);
            }
            else
            {
                this.Region = new Region(path.GetBounds());
            }

            // 获取当前区域的颜色
            Color color = Color.Purple;
            if (m_Selected)
            {
                color = Color.Yellow;
            }
            else
            {
                color = Color.Purple;
                RegionGroup bGroup = RegionManager.Instance.GetGroupByID(m_RegionInfo.GroupID);
                if (bGroup != null)
                {
                    color = bGroup.RegionColor;
                }
            }

            // 设置画笔和画刷
            SolidBrush brush = new SolidBrush(color);
            Pen pen = new Pen(color, 1.0f);
            SolidBrush RedBrush = new SolidBrush(Color.Red);

            // 设置控件的位置
            this.Left = (int)(bound.Location.X);
            this.Top = (int)(bound.Location.Y);
            this.Width = (int)(bound.Width);//
            this.Height = (int)(bound.Height);//

            if(g != null)
            {
                // 显示区域主体
                if (m_bTransparent)
                {
                    // 透明显示
                    g.DrawPath(pen, path);
                }
                else
                {
                    // 不透明显示
                    g.FillPath(brush, path);
                }

                // 显示顶点
                g.FillPath(brush, Vertexpath);
                // 显示选中的点
                g.FillPath(RedBrush, Pointpath);
            }
            
            brush.Dispose();
            path.Dispose();
            RedBrush.Dispose();
            Pointpath.Dispose();
        }

        private void RegionControl_Paint(object sender, PaintEventArgs e)
        {
            if(m_RegionInfo.Hidden)
            {
                this.Hide();
                return;
            }
            DrawRegion(e.Graphics);
        }

        // 当前控件是否被选择
        public bool Selected
        {
            get
            {
                return m_Selected;
            }
            set
            {
                m_Selected = value;
            }
        }

        public RegionInfo MyRegionInfo
        {
            get
            {
                return m_RegionInfo;
            }
        }

        private void RegionControl_MouseDown(object sender, MouseEventArgs e)
        {
            // 用户在当前控件上按下
            if (e.Button == MouseButtons.Left)
            {
                _DownLocation.X = e.X;
                _DownLocation.Y = e.Y;


                if (MainForm.Instance.SelectedRegion != null)
                {
                    MainForm.Instance.SelectedRegion.Invalidate();
                }
                //MainForm.Instance.SelectedRegion = this;
                MainForm.Instance.SetSelectRegionByID(m_RegionInfo.ID);

                float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);
                foreach (RegionPointInfo pointInfo in m_RegionInfo.PointList)
                {
                    Point location = new Point(pointInfo.Pos.X, pointInfo.Pos.Y);
                    int iX = (int)(pointInfo.Pos.X * fScale);
                    int iY = (int)((m_iSceneHeight - pointInfo.Pos.Y) * fScale);
                    iX -= this.Left;
                    iY -= this.Top;

                    int iSize = 1;

                    switch (pointInfo.Type)
                    {
                        case 0:
                            if (m_ShowVertex)
                            {
                                iSize = RectSize;
                            }
                            else
                            {
                                iSize = 0;
                            }

                            break;
                        case 1:
                            if (m_OperMode && m_RegionInfo.Shape == 0)
                            {
                                iSize = CircleSize;
                            }
                            else
                            {
                                iSize = 0;
                            }
                            break;
                        default:
                            break;
                    }

                    if (System.Math.Abs(e.X - iX) < iSize && System.Math.Abs(e.Y - iY) < iSize)
                    {
                        pointInfo.Selected = true;
                        MainForm.Instance.MainPropertyGrid.SelectedObject = pointInfo;
                    }
                    else
                    {
                        pointInfo.Selected = false;
                    }

                }

                // 设置NPC所处的层
                if (m_iLayer == 1)
                {
                    MainForm.Instance.SetRegionLayer(m_RegionInfo.ID, 3);
                    m_iLayer = 3;
                }
                else
                {
                    MainForm.Instance.SetRegionLayer(m_RegionInfo.ID, 1);
                    m_iLayer = 1;
                }

                this.Invalidate();
            }
        }

        private void RegionControl_MouseMove(object sender, MouseEventArgs e)
        {
            // 显示鼠标当前的坐标
            float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);
            int iX = (int)((e.X + this.Left) / fScale);
            int iY = (int)((e.Y + this.Top) / fScale); 
            iY = m_iSceneHeight - iY - 1;
            String strLocation = iX.ToString() + "," + iY.ToString();
            MainForm.Instance.Text = strLocation;

            if (e.Button == MouseButtons.Left)
            {
                int newLeft = e.X - _DownLocation.X;
                int newTop = e.Y - _DownLocation.Y;


                //float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);
                int dx = (int)(newLeft / fScale);
                int dy = (int)((newTop) / fScale);
                Rectangle oldBound = this.m_RegionInfo.GetBound();
                int iMoveType = this.m_RegionInfo.Move(dx, dy, m_iSceneWidth, m_iSceneHeight, m_OperMode);
                Rectangle newBound = this.m_RegionInfo.GetBound();

                if(dx != 0 || dy != 0)
                {
                    if(iMoveType == 0)
                    {
                        if(newBound.Left == oldBound.Left)
                        {
                            _DownLocation.X += (int)(dx * fScale);
                        }
                        if (newBound.Bottom == oldBound.Bottom)
                        {
                            _DownLocation.Y += (int)(dy * fScale);
                        }  
                    }                  
                    this.Invalidate();
                }
            }
        }

        private void RegionControl_MouseUp(object sender, MouseEventArgs e)
        {
            _DownLocation = new System.Drawing.Point();
            this.m_RegionInfo.FormatPos();
            this.Invalidate();
        }

        private void RegionControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (m_Selected)
            {
                if (e.KeyCode == Keys.J)    // 操作模式的切换
                {
                    if(m_RegionInfo.Shape == 0)
                    {
                        m_OperMode = !m_OperMode;
                        this.Invalidate();
                    }
                    else
                    {
                        MessageBox.Show("矩形区域无法新增顶点，请按空格键切换！");
                    }
                    
                }

                if (e.KeyCode == Keys.Delete)   // 删除区域
                {
                    m_RegionInfo.Delete();
                    this.Invalidate();
                }

                if (e.KeyCode == Keys.Space)    // 区域形状的切换
                {
                    m_RegionInfo.AdjustShape();
                    if (m_RegionInfo.Shape == 0)
                    {
                        m_ShowVertex = true;
                        m_OperMode = true;
                    }
                    else if (m_RegionInfo.Shape == 1)
                    {
                        m_ShowVertex = true;
                        m_OperMode = false;
                    }
                    this.Invalidate();
                }

                if(e.KeyCode == Keys.H)     // 是否显示顶点
                {
                    m_ShowVertex = !m_ShowVertex;
                    this.Invalidate();
                }

                if(e.KeyCode == Keys.T)     // 透明显示
                {
                    m_bTransparent = !m_bTransparent;
                    this.Invalidate();
                }

                if (e.KeyCode == Keys.W)//向上
                {
                    m_RegionInfo.Move(0, -1, m_iSceneWidth, m_iSceneHeight, m_OperMode);
                    this.m_RegionInfo.FormatPos();
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.S)//向下
                {
                    m_RegionInfo.Move(0, 1, m_iSceneWidth, m_iSceneHeight, m_OperMode);
                    this.m_RegionInfo.FormatPos();
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.A)//向左
                {
                    m_RegionInfo.Move(-1, 0, m_iSceneWidth, m_iSceneHeight, m_OperMode);
                    this.m_RegionInfo.FormatPos();
                    this.Invalidate();
                }
                if (e.KeyCode == Keys.D)//向右
                {
                    m_RegionInfo.Move(1, 0, m_iSceneWidth, m_iSceneHeight, m_OperMode);
                    this.m_RegionInfo.FormatPos();
                    this.Invalidate();
                }

            }
        }

    }
}
