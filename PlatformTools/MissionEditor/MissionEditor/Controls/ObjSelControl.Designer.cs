﻿namespace MissionEditor
{
    partial class ObjSelControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_tabControl = new System.Windows.Forms.TabControl();
            this.m_tabPageNPC = new System.Windows.Forms.TabPage();
            this.m_comboBoxNPCFilterType = new System.Windows.Forms.ComboBox();
            this.m_textBoxNPCFilterName = new System.Windows.Forms.TextBox();
            this.m_comboBoxNPCFilterMap = new System.Windows.Forms.ComboBox();
            this.m_buttonNPCFilter = new System.Windows.Forms.Button();
            this.m_listViewNPC = new System.Windows.Forms.ListView();
            this.m_tabPageItem = new System.Windows.Forms.TabPage();
            this.m_listViewItem = new System.Windows.Forms.ListView();
            this.m_tabPageArea = new System.Windows.Forms.TabPage();
            this.m_listViewArea = new System.Windows.Forms.ListView();
            this.m_tabControl.SuspendLayout();
            this.m_tabPageNPC.SuspendLayout();
            this.m_tabPageItem.SuspendLayout();
            this.m_tabPageArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_tabControl
            // 
            this.m_tabControl.Controls.Add(this.m_tabPageNPC);
            this.m_tabControl.Controls.Add(this.m_tabPageItem);
            this.m_tabControl.Controls.Add(this.m_tabPageArea);
            this.m_tabControl.Location = new System.Drawing.Point(0, 30);
            this.m_tabControl.Name = "m_tabControl";
            this.m_tabControl.SelectedIndex = 0;
            this.m_tabControl.Size = new System.Drawing.Size(340, 260);
            this.m_tabControl.TabIndex = 0;
            this.m_tabControl.SelectedIndexChanged += new System.EventHandler(this.m_tabControl_SelectedIndexChanged);
            // 
            // m_tabPageNPC
            // 
            this.m_tabPageNPC.Controls.Add(this.m_listViewNPC);
            this.m_tabPageNPC.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageNPC.Name = "m_tabPageNPC";
            this.m_tabPageNPC.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageNPC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.m_tabPageNPC.Size = new System.Drawing.Size(332, 235);
            this.m_tabPageNPC.TabIndex = 0;
            this.m_tabPageNPC.Text = "NPC";
            this.m_tabPageNPC.UseVisualStyleBackColor = true;
            // 
            // m_comboBoxNPCFilterType
            // 
            this.m_comboBoxNPCFilterType.FormattingEnabled = true;
            this.m_comboBoxNPCFilterType.Items.AddRange(new object[] {
            "(类型)",
            "NPC",
            "怪物"});
            this.m_comboBoxNPCFilterType.Location = new System.Drawing.Point(70, 5);
            this.m_comboBoxNPCFilterType.Name = "m_comboBoxNPCFilterType";
            this.m_comboBoxNPCFilterType.Size = new System.Drawing.Size(60, 20);
            this.m_comboBoxNPCFilterType.TabIndex = 4;
            this.m_comboBoxNPCFilterType.Text = "(类型)";
            // 
            // m_textBoxNPCFilterName
            // 
            this.m_textBoxNPCFilterName.Location = new System.Drawing.Point(130, 4);
            this.m_textBoxNPCFilterName.Name = "m_textBoxNPCFilterName";
            this.m_textBoxNPCFilterName.Size = new System.Drawing.Size(135, 21);
            this.m_textBoxNPCFilterName.TabIndex = 3;
            // 
            // m_comboBoxNPCFilterMap
            // 
            this.m_comboBoxNPCFilterMap.FormattingEnabled = true;
            this.m_comboBoxNPCFilterMap.Items.AddRange(new object[] {
            "(地图)"});
            this.m_comboBoxNPCFilterMap.Location = new System.Drawing.Point(0, 5);
            this.m_comboBoxNPCFilterMap.Name = "m_comboBoxNPCFilterMap";
            this.m_comboBoxNPCFilterMap.Size = new System.Drawing.Size(70, 20);
            this.m_comboBoxNPCFilterMap.TabIndex = 2;
            this.m_comboBoxNPCFilterMap.Text = "(地图)";
            // 
            // m_buttonNPCFilter
            // 
            this.m_buttonNPCFilter.Location = new System.Drawing.Point(265, 3);
            this.m_buttonNPCFilter.Name = "m_buttonNPCFilter";
            this.m_buttonNPCFilter.Size = new System.Drawing.Size(75, 23);
            this.m_buttonNPCFilter.TabIndex = 1;
            this.m_buttonNPCFilter.Text = "筛选";
            this.m_buttonNPCFilter.UseVisualStyleBackColor = true;
            this.m_buttonNPCFilter.Click += new System.EventHandler(this.m_buttonNPCFilter_Click);
            // 
            // m_listViewNPC
            // 
            this.m_listViewNPC.AllowDrop = true;
            this.m_listViewNPC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listViewNPC.FullRowSelect = true;
            this.m_listViewNPC.GridLines = true;
            this.m_listViewNPC.Location = new System.Drawing.Point(0, 0);
            this.m_listViewNPC.MultiSelect = false;
            this.m_listViewNPC.Name = "m_listViewNPC";
            this.m_listViewNPC.Size = new System.Drawing.Size(330, 235);
            this.m_listViewNPC.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.m_listViewNPC.TabIndex = 0;
            this.m_listViewNPC.UseCompatibleStateImageBehavior = false;
            this.m_listViewNPC.View = System.Windows.Forms.View.Details;
            this.m_listViewNPC.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.m_listViewNPC_ItemDrag);
            // 
            // m_tabPageItem
            // 
            this.m_tabPageItem.Controls.Add(this.m_listViewItem);
            this.m_tabPageItem.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageItem.Name = "m_tabPageItem";
            this.m_tabPageItem.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageItem.Size = new System.Drawing.Size(332, 235);
            this.m_tabPageItem.TabIndex = 1;
            this.m_tabPageItem.Text = "道具";
            this.m_tabPageItem.UseVisualStyleBackColor = true;
            // 
            // m_listViewItem
            // 
            this.m_listViewItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listViewItem.FullRowSelect = true;
            this.m_listViewItem.Location = new System.Drawing.Point(0, 0);
            this.m_listViewItem.Name = "m_listViewItem";
            this.m_listViewItem.Size = new System.Drawing.Size(330, 235);
            this.m_listViewItem.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.m_listViewItem.TabIndex = 1;
            this.m_listViewItem.UseCompatibleStateImageBehavior = false;
            this.m_listViewItem.View = System.Windows.Forms.View.Details;
            this.m_listViewItem.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.m_listViewItem_ItemDrag);
            // 
            // m_tabPageArea
            // 
            this.m_tabPageArea.Controls.Add(this.m_listViewArea);
            this.m_tabPageArea.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageArea.Name = "m_tabPageArea";
            this.m_tabPageArea.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageArea.Size = new System.Drawing.Size(332, 235);
            this.m_tabPageArea.TabIndex = 2;
            this.m_tabPageArea.Text = "区域";
            this.m_tabPageArea.UseVisualStyleBackColor = true;
            // 
            // m_listViewArea
            // 
            this.m_listViewArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listViewArea.FullRowSelect = true;
            this.m_listViewArea.GridLines = true;
            this.m_listViewArea.Location = new System.Drawing.Point(0, 0);
            this.m_listViewArea.MultiSelect = false;
            this.m_listViewArea.Name = "m_listViewArea";
            this.m_listViewArea.Size = new System.Drawing.Size(330, 235);
            this.m_listViewArea.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.m_listViewArea.TabIndex = 1;
            this.m_listViewArea.UseCompatibleStateImageBehavior = false;
            this.m_listViewArea.View = System.Windows.Forms.View.Details;
            this.m_listViewArea.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.m_listViewArea_ItemDrag);
            // 
            // ObjSelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_comboBoxNPCFilterType);
            this.Controls.Add(this.m_tabControl);
            this.Controls.Add(this.m_textBoxNPCFilterName);
            this.Controls.Add(this.m_comboBoxNPCFilterMap);
            this.Controls.Add(this.m_buttonNPCFilter);
            this.Name = "ObjSelControl";
            this.Size = new System.Drawing.Size(340, 290);
            this.m_tabControl.ResumeLayout(false);
            this.m_tabPageNPC.ResumeLayout(false);
            this.m_tabPageItem.ResumeLayout(false);
            this.m_tabPageArea.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl m_tabControl;
        private System.Windows.Forms.TabPage m_tabPageNPC;
        private System.Windows.Forms.TabPage m_tabPageItem;
        private System.Windows.Forms.TabPage m_tabPageArea;
        private System.Windows.Forms.ListView m_listViewNPC;
        private System.Windows.Forms.ListView m_listViewItem;
        private System.Windows.Forms.ListView m_listViewArea;
        private System.Windows.Forms.Button m_buttonNPCFilter;
        private System.Windows.Forms.TextBox m_textBoxNPCFilterName;
        private System.Windows.Forms.ComboBox m_comboBoxNPCFilterMap;
        private System.Windows.Forms.ComboBox m_comboBoxNPCFilterType;
    }
}
