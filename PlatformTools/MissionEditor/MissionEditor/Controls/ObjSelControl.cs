﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace MissionEditor
{
    public partial class ObjSelControl : UserControl
    {
        public ObjSelControl()
        {
            InitializeComponent();
            InitListView(); // 初始化物件显示列表
        }

        public void InitListView()
        {
            // 地图下拉框
            ArrayList arrMapName = SceneSketchManager.Instance.GetSceneNameList();
            foreach (String strMapName in arrMapName)
            {
                m_comboBoxNPCFilterMap.Items.Add(strMapName);
            }


            // NPC列表
            m_listViewNPC.Columns.Clear();
            m_listViewNPC.Columns.Add("ID", 60, HorizontalAlignment.Center);
            m_listViewNPC.Columns.Add("名称", 250, HorizontalAlignment.Center);

            // 道具列表
            m_listViewItem.Columns.Clear();
            m_listViewItem.Columns.Add("ID", 70, HorizontalAlignment.Center);
            m_listViewItem.Columns.Add("名称", 250, HorizontalAlignment.Center);

            // 区域列表
            m_listViewArea.Columns.Clear();
            m_listViewArea.Columns.Add("ID", 60, HorizontalAlignment.Center);
            m_listViewArea.Columns.Add("名称", 250, HorizontalAlignment.Center);
        }

        private void RefreshNPCList(Hashtable result)
        {
            // 清空旧的数据
            m_listViewNPC.Items.Clear();

            // 添加新的数据
            foreach (int iNPCID in result.Keys)
            {
                NPCInfo info = result[iNPCID] as NPCInfo;
                AddToListView(m_listViewNPC, info.ID.ToString(), info.Name, "1");
            }
        }

        private void RefreshItemList(Hashtable result)
        {
            // 清空旧的数据
            m_listViewItem.Items.Clear();

            // 添加新的数据
            foreach (int id in result.Keys)
            {
                Item item = result[id] as Item;
                AddToListView(m_listViewItem, item.ID.ToString(), item.Name, "2");
            }
        }

        private void RefreshRegionList(Hashtable result)
        {
            // 清空旧的数据
            m_listViewArea.Items.Clear();

            // 添加新的数据
            foreach (String GroupID in result.Keys)
            {
                RegionGroup group = result[GroupID] as RegionGroup;
                AddToListView(m_listViewArea, group.OutPutID, group.Name, "3");
            }
        }

        private void AddToListView(ListView view, String strID, String strName, String strType)
        {
            ListViewItem lv = new ListViewItem();

            lv.SubItems[0].Text = strID;
            lv.SubItems.Add(strName);

            lv.Tag = strType;

            view.Items.Add(lv);
        }

        private void m_buttonNPCFilter_Click(object sender, EventArgs e)
        {
            // 获取地图号
            int iMapID = -1;
            if (m_comboBoxNPCFilterMap.SelectedItem != null)
            {
                iMapID = SceneSketchManager.Instance.GetSceneIDByName(m_comboBoxNPCFilterMap.SelectedItem.ToString());
            }

            // 获取NPC类型， monster or NPC
            int iType = m_comboBoxNPCFilterType.SelectedIndex;
            if (iType <= 0)
            {
                iType = -1;
            }

            // 获取NPC名称
            String strNPCName = m_textBoxNPCFilterName.Text;

            // 根据索引页筛选
            int iIndex = m_tabControl.SelectedIndex;

            if (iIndex == 0)
            {
                // 开始筛选
                Hashtable result = NPCManager.Instance.NPCFilter(iMapID, iType, strNPCName);

                // 刷新列表
                RefreshNPCList(result);
            }
            else if (iIndex == 1)
            {
                // 开始筛选
                Hashtable result = ItemManager.Instance.ItemFilter(strNPCName);

                // 刷新列表
                RefreshItemList(result);
            }
            else if (iIndex == 2)
            {
                // 开始筛选
                Hashtable result = RegionManager.Instance.RegionFilter(iMapID,strNPCName);

                // 刷新列表
                RefreshRegionList(result);
            }
            else
            {
               
            }   
        }

        private void m_listViewNPC_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
        }

        private void m_listViewArea_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
        }

        private void m_listViewItem_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
        }

        private void m_tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iIndex = m_tabControl.SelectedIndex;

            if (iIndex == 0)
            {
                m_comboBoxNPCFilterMap.Enabled = true;
                m_comboBoxNPCFilterType.Enabled = true;
            }
            else if (iIndex == 1)
            {
                m_comboBoxNPCFilterMap.Enabled = false;
                m_comboBoxNPCFilterType.Enabled = false;
            }
            else if (iIndex == 2)
            {
                m_comboBoxNPCFilterMap.Enabled = true;
                m_comboBoxNPCFilterType.Enabled = false;
            }
            else
            {
                m_comboBoxNPCFilterMap.Enabled = true;
                m_comboBoxNPCFilterType.Enabled = true;
            }
        }
    }
}
