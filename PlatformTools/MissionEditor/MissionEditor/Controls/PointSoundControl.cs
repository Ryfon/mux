﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class PointSoundControl : BaseDrawControl
    {
        public PointSoundControl(PointSoundInfo soundInfo, int iSceneWidth, int iSceneHeight)
        {
            InitializeComponent();
            m_SoundInfo = soundInfo;
            m_iSceneWidth = iSceneWidth;
            m_iSceneHeight = iSceneHeight;
            DrawPointSound(null);
        }

        public override void DrawOnBmp(Graphics g, int iBmpWidth, int iBmpHeight)
        {
            // 初始化图形path
            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath stringPath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            // 计算场景尺寸到 1024 纹理的缩放
            float fScale = iBmpHeight / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

            // 坐标的转换
            Point oldPos = m_SoundInfo.GetPosition();
            oldPos.Offset(_DrawOffset);
            Point scenePos = new Point(oldPos.X, m_iSceneHeight - oldPos.Y);
            scenePos.X = (int)(scenePos.X * fScale);
            scenePos.Y = (int)(scenePos.Y * fScale);

            // 获取内外半径
            int iOuterRadius = int.Parse(m_SoundInfo.OuterRadius);
            int iInnerRadius = int.Parse(m_SoundInfo.InnerRadius);
            iOuterRadius += _OuterRadiusOffset;
            iInnerRadius += _InnerRadiusOffset;
            iOuterRadius = (int)(iOuterRadius * fScale);
            iInnerRadius = (int)(iInnerRadius * fScale);
           

            // 生成内外范围框
            Rectangle rectOuter = new Rectangle((int)(scenePos.X - iOuterRadius), (int)(scenePos.Y - iOuterRadius), 2 * iOuterRadius, 2 * iOuterRadius);
            Rectangle rectInner = new Rectangle((int)(scenePos.X - iInnerRadius), (int)(scenePos.Y - iInnerRadius), 2 * iInnerRadius, 2 * iInnerRadius);

            //显示名称          
            FontFamily family = new FontFamily("黑体");
            int fontStyle = (int)FontStyle.Italic;
            int emSize = 14;
            Point origin = new Point(rectOuter.Left, rectOuter.Top - emSize);
            StringFormat format = StringFormat.GenericDefault;
            path.AddString(m_SoundInfo.GetDisplayName(), family, fontStyle, emSize, origin, format);
            stringPath.AddString(m_SoundInfo.GetDisplayName(), family, fontStyle, emSize, origin, format);

            // 添加内外半径
            path.AddEllipse(rectOuter);
            path.AddEllipse(rectInner);

            if (g != null)
            {
                // 开始渲染
                SolidBrush redBrush = new SolidBrush(Color.Red);
                Pen yellowPen = new Pen(Color.Yellow);
                Pen redPen = new Pen(Color.Red);
                g.FillPath(redBrush, stringPath);
                g.DrawEllipse(yellowPen, rectOuter.X, rectOuter.Y - 1, rectOuter.Width, rectOuter.Height);
                g.DrawEllipse(redPen, rectInner.X, rectInner.Y - 1, rectInner.Width, rectInner.Height);

                // 释放资源
                redBrush.Dispose();
                yellowPen.Dispose();
                redPen.Dispose();
            }      
        }

        public void DrawPointSound(Graphics g)
        {
            // 初始化图形path
            GraphicsPath path = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);
            GraphicsPath stringPath = new GraphicsPath(System.Drawing.Drawing2D.FillMode.Winding);

            // 计算场景尺寸到 1024 纹理的缩放
            float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

            // 坐标的转换
            Point oldPos = m_SoundInfo.GetPosition();
            //Console.WriteLine("_DrawOffset" + _DrawOffset.X.ToString() + "," + _DrawOffset.Y.ToString());
            oldPos.Offset(_DrawOffset);
            //Console.WriteLine("oldPos" + oldPos.X.ToString() + "," + oldPos.Y.ToString());
            Point scenePos = new Point(oldPos.X, m_iSceneHeight - oldPos.Y);
            scenePos.X = (int)(scenePos.X * fScale + 0.5f);
            scenePos.Y = (int)(scenePos.Y * fScale + 0.5f);

            // 获取内外半径
            int iOuterRadius = int.Parse(m_SoundInfo.OuterRadius);
            int iInnerRadius = int.Parse(m_SoundInfo.InnerRadius);
            iOuterRadius += _OuterRadiusOffset;
            iInnerRadius += _InnerRadiusOffset;
            iOuterRadius = (int)(iOuterRadius * fScale + 0.5f);
            iInnerRadius = (int)(iInnerRadius * fScale + 0.5f);

            // 生成内外范围框
            Rectangle rectOuter = new Rectangle((int)(scenePos.X - iOuterRadius), (int)(scenePos.Y - iOuterRadius), 2 * iOuterRadius, 2 * iOuterRadius);
            Rectangle rectInner = new Rectangle((int)(scenePos.X - iInnerRadius), (int)(scenePos.Y - iInnerRadius), 2 * iInnerRadius, 2 * iInnerRadius);

            //显示名称          
            FontFamily family = new FontFamily("黑体");
            int fontStyle = (int)FontStyle.Italic;
            int emSize = 14;
            Point origin = new Point(rectOuter.Left, rectOuter.Top - emSize);
            StringFormat format = StringFormat.GenericDefault;
            path.AddString(m_SoundInfo.GetDisplayName(), family, fontStyle, emSize, origin, format);
            stringPath.AddString(m_SoundInfo.GetDisplayName(), family, fontStyle, emSize, origin, format);

            // 添加内外半径
            path.AddEllipse(rectOuter);
            path.AddEllipse(rectInner);

            // 调整为相对坐标
            RectangleF bound = path.GetBounds();
            path.Transform(new Matrix(1.0f, 0.0f, 0.0f, 1.0f, -bound.Left, -bound.Top - 1.0f));
            stringPath.Transform(new Matrix(1.0f, 0.0f, 0.0f, 1.0f, -bound.Left, -bound.Top - 1.0f));
            rectOuter.Offset(-(int)bound.Left, -(int)bound.Top - 1);
            rectInner.Offset(-(int)bound.Left, -(int)bound.Top - 1);

            // 是否启用加速模式
            if (MainForm.Instance.SpeedMode)
            {
                this.Region = new Region(path.GetBounds());
            }
            else
            {
                this.Region = new Region(path);
            }

            // 设定控件位置
            this.Left = (int)(bound.Location.X);
            this.Top = (int)(bound.Location.Y);
            this.Width = (int)(bound.Width);//
            this.Height = (int)(bound.Height);//

            Console.WriteLine("bound" + this.Left.ToString() + "," + this.Top.ToString());

            if(g != null)
            {
                // 开始渲染
                SolidBrush redBrush = new SolidBrush(Color.Red);
                Pen yellowPen = new Pen(Color.Yellow);
                Pen redPen = new Pen(Color.Red);
                g.FillPath(redBrush, stringPath);
                g.DrawEllipse(yellowPen, rectOuter.X, rectOuter.Y - 1, rectOuter.Width, rectOuter.Height);
                g.DrawEllipse(redPen, rectInner.X, rectInner.Y - 1, rectInner.Width, rectInner.Height);

                // 释放资源
                redBrush.Dispose();
                yellowPen.Dispose();
                redPen.Dispose();
            }      
        }

        private void PointSoundControl_Paint(object sender, PaintEventArgs e)
        {
            DrawPointSound(e.Graphics);
        }

        private void PointSoundControl_MouseMove(object sender, MouseEventArgs e)
        {
            // 显示tip
            m_toolTipShowDisplay.SetToolTip(this, m_SoundInfo.GetDisplayName());

            // 计算缩放
            float fScale = OptionManager.SceneSketchSize / System.Math.Max(m_iSceneHeight, m_iSceneWidth);

            // 显示鼠标当前的坐标
            String strLocation = ",";

            // 移动物件
            if (e.Button == MouseButtons.Left)
            {
                // 限制鼠标的位置在控件之内
                int iMouseX = e.X;
                int iMouseY = e.Y;
                Console.WriteLine(iMouseX.ToString() + "," + iMouseY.ToString());
                if(iMouseX < 0)
                {
                    iMouseX = 0;
                }
                if (iMouseX > this.Width)
                {
                    iMouseX = this.Width;
                }
                if (iMouseY < 0)
                {
                    iMouseY = 0;
                }
                if (iMouseY > this.Height)
                {
                    iMouseY = this.Height;
                }
                // 计算偏移量
                int newLeft = iMouseX - _DownLocation.X;
                int newTop = iMouseY - _DownLocation.Y;

                int dx = (int)((float)newLeft / fScale);
                int dy = (int)((float)newTop / fScale);            

                // 重置鼠标位置
                if (dx != 0 || dy != 0)
                {
                    // 移动控件
                    _DrawOffset.X += dx;
                    _DrawOffset.Y -= dy;

                    // 判断是否出界
                    Point pNewLocation = m_SoundInfo.GetPosition();
                    int iRadius = int.Parse(m_SoundInfo.OuterRadius);
                    pNewLocation.Offset(_DrawOffset);
                    if (pNewLocation.X - iRadius < 0 || pNewLocation.Y - iRadius < 0 || pNewLocation.X + iRadius > m_iSceneWidth || pNewLocation.Y + iRadius > m_iSceneHeight)
                    {
                        _DrawOffset.X -= dx;
                        _DrawOffset.Y += dy;
                    }
                    else
                    {
                        this.Invalidate();
                    }
                    //_DownLocation.X += (int)(dx * fScale);
                    //_DownLocation.Y += (int)(dy * fScale);
                    //this.Invalidate();
                }
            }

            // 计算出具体位置
            int iX = (int)(e.X / fScale);
            int iY = (int)(e.Y / fScale);
            iY = m_iSceneHeight - iY - 1;
            strLocation = iX.ToString() + "," + iY.ToString();

            // 在标题栏显示
            MainForm.Instance.Text = strLocation;
        }

        private void PointSoundControl_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                _DownLocation.X = e.X;
                _DownLocation.Y = e.Y;
                this.Invalidate();
            }           
        }

        private void PointSoundControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                _DrawOffset.X = 0;
                _DrawOffset.Y = 0;
                _OuterRadiusOffset = 0;
                _InnerRadiusOffset = 0;
                this.Invalidate();
            }
            if (e.KeyCode == Keys.W)
            {
                _DrawOffset.Y += 1;
                this.Invalidate();
            }
            if (e.KeyCode == Keys.S)
            {
                _DrawOffset.Y -= 1;
                this.Invalidate();
            }
            if (e.KeyCode == Keys.A)
            {
                _DrawOffset.X -= 1;
                this.Invalidate();
            }
            if (e.KeyCode == Keys.D)
            {
                _DrawOffset.X += 1;
                this.Invalidate();
            }

            #region 内外半径调整
            if (e.KeyCode == Keys.Q)
            {
                _OuterRadiusOffset -= 1;
                this.Invalidate();
            }
            if (e.KeyCode == Keys.E)
            {
                _OuterRadiusOffset += 1;
                this.Invalidate();
            }
            if (e.KeyCode == Keys.Z)
            {
                _InnerRadiusOffset -= 1;
                this.Invalidate();
            }
            if (e.KeyCode == Keys.C)
            {
                _InnerRadiusOffset += 1;
                this.Invalidate();
            }
            #endregion
        }
    }
}
