﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class ConditionSelControl : UserControl
    {
        public ConditionSelControl()
        {
            InitializeComponent();
            InitUI();
        }

        private void InitUI()
        {
            // 初始化分类筛选器
            m_listViewFilter.Columns.Clear();
            m_listViewFilter.Columns.Add("分类", 96, HorizontalAlignment.Center);

            // 初始化结果预览器
            m_listViewPreView.Columns.Clear();
            m_listViewPreView.Columns.Add("分类", 60, HorizontalAlignment.Center);
            m_listViewPreView.Columns.Add("名称", 140, HorizontalAlignment.Center);
            m_listViewPreView.Columns.Add("参数", 150, HorizontalAlignment.Center);
            m_listViewPreView.Columns.Add("描述", 130, HorizontalAlignment.Center);
            m_listViewPreView.Columns.Add("参数说明", 140, HorizontalAlignment.Center);
        }

        public void InitDataBase(DataSet database, ListView result)
        {
            // 变量赋值
            m_DataBase = database;
            m_listViewResult = result;

            // 显示筛选框
            ShowFilter(database);

            // 显示选择框
            ShowTable(null);

            // 显示预览框
            ShowPreview(database, result);
        }

        private void ShowFilter(DataSet database)
        {
            // 清空旧的数据
            m_listViewFilter.Items.Clear();

            // 插入数据
            if (database != null)
            {
                foreach (DataTable dt in database.Tables)
                {
                    m_listViewFilter.Items.Add(dt.TableName);
                }
            }
        }

        private void ShowTable(DataTable dt)
        {
            // 清空旧的数据
            m_dataGridViewDataBase.Rows.Clear();

            if (dt != null)
            {
                // 插入空的行
                m_dataGridViewDataBase.Rows.Add(dt.Rows.Count);

                // 填充每一行的数据
                int iRowIndex = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    int iCellIndex = 0;
                    while (iCellIndex < 5)
                    {
                        m_dataGridViewDataBase.Rows[iRowIndex].Cells[iCellIndex].Value = dr[iCellIndex];
                        iCellIndex++;
                    }

                    iRowIndex++;
                }
            }
        }

        private void ShowPreview(DataSet database, ListView result)
        {
            // 清空就的预览数据
            m_listViewPreView.Items.Clear();

            // 遍历结果数据栏
            foreach(ListViewItem lv in result.Items)
            {
                // 定义需要的数据
                String strFilter = "";
                String strName = lv.SubItems[0].Text;
                String strValue = lv.SubItems[1].Text;
                String strDesc = "";
                String strParam = "";

                bool bFind = false;
                foreach(DataTable dt in database.Tables)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        if (strName.Equals(dr[0].ToString()))
                        {
                            strFilter = dt.TableName;

                            strDesc = dr[3].ToString();
                            strParam = dr[4].ToString();

                            // 插入数据
                            InsertToPreview(strFilter, strName, strValue, strDesc, strParam);
                            
                            // 跳出返回
                            bFind = true;
                            break;
                        }
                    }

                    if(bFind)
                    {
                        break;
                    }
                }
            }
        }

        private void InsertToPreview(String strFilter, String strName, String strValue, String strDesc, String strParam)
        {
            ListViewItem lv = new ListViewItem();

            lv.SubItems[0].Text = strFilter;
            lv.SubItems.Add(strName);
            lv.SubItems.Add(strValue);
            lv.SubItems.Add(strDesc);
            lv.SubItems.Add(strParam);

            m_listViewPreView.Items.Add(lv);
        }

        private void m_dataGridViewDataBase_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2 && e.RowIndex != -1)
            {
                // 获取分类信息
                ListViewItem lv = m_listViewFilter.SelectedItems[0];
                String strFilter = lv.SubItems[0].Text;

                // 获取参数信息
                String strName = m_dataGridViewDataBase.Rows[e.RowIndex].Cells[0].Value.ToString();
                String strValue = m_dataGridViewDataBase.Rows[e.RowIndex].Cells[1].Value.ToString();
                String strDesc = m_dataGridViewDataBase.Rows[e.RowIndex].Cells[3].Value.ToString();
                String strParam = m_dataGridViewDataBase.Rows[e.RowIndex].Cells[4].Value.ToString();

                // 插入数据
                InsertToPreview(strFilter, strName, strValue, strDesc, strParam);

                // 清空上一次填入的数据
                m_dataGridViewDataBase.Rows[e.RowIndex].Cells[1].Value = "";
            }
        }

        private void m_buttonClose_Click(object sender, EventArgs e)
        {
            Parent.Hide();
        }

        private void m_listViewFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_listViewFilter.SelectedItems.Count > 0)
            {
                ListViewItem lv = m_listViewFilter.SelectedItems[0];

                if (lv != null)
                {
                    String strTableName = lv.SubItems[0].Text;
                    int iTableIndex = m_DataBase.Tables.IndexOf(strTableName);
                    if (iTableIndex >= 0)
                    {
                        ShowTable(m_DataBase.Tables[iTableIndex]);
                    }
                }
            }
        }

        private void m_buttonReset_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem lv in m_listViewPreView.SelectedItems)
            {
                m_listViewPreView.Items.Remove(lv);
            }
        }

        private void m_buttonApp_Click(object sender, EventArgs e)
        {
            // 清空旧的数据
            m_listViewResult.Items.Clear();

            // 插入新的数据
            foreach(ListViewItem lv in m_listViewPreView.Items)
            {
                // 定义新的行
                ListViewItem newLV = new ListViewItem();

                // 填充数据
                newLV.SubItems[0].Text = lv.SubItems[1].Text;
                newLV.SubItems.Add(lv.SubItems[2].Text);

                // 将新的行插入
                m_listViewResult.Items.Add(newLV);
            }

            // 关闭窗口
            Parent.Hide();
        }
    }
}
