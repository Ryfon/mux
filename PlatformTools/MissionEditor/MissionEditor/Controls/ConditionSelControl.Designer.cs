﻿namespace MissionEditor
{
    partial class ConditionSelControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_dataGridViewDataBase = new System.Windows.Forms.DataGridView();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnUse = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnParam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_groupBoxConfig = new System.Windows.Forms.GroupBox();
            this.m_listViewFilter = new System.Windows.Forms.ListView();
            this.m_groupBoxPreview = new System.Windows.Forms.GroupBox();
            this.m_listViewPreView = new System.Windows.Forms.ListView();
            this.m_buttonApp = new System.Windows.Forms.Button();
            this.m_buttonReset = new System.Windows.Forms.Button();
            this.m_buttonClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewDataBase)).BeginInit();
            this.m_groupBoxConfig.SuspendLayout();
            this.m_groupBoxPreview.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_dataGridViewDataBase
            // 
            this.m_dataGridViewDataBase.AllowUserToAddRows = false;
            this.m_dataGridViewDataBase.AllowUserToDeleteRows = false;
            this.m_dataGridViewDataBase.AllowUserToResizeRows = false;
            this.m_dataGridViewDataBase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewDataBase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnName,
            this.ColumnValue,
            this.ColumnUse,
            this.ColumnDesc,
            this.ColumnParam});
            this.m_dataGridViewDataBase.Location = new System.Drawing.Point(115, 20);
            this.m_dataGridViewDataBase.MultiSelect = false;
            this.m_dataGridViewDataBase.Name = "m_dataGridViewDataBase";
            this.m_dataGridViewDataBase.RowTemplate.Height = 23;
            this.m_dataGridViewDataBase.Size = new System.Drawing.Size(520, 190);
            this.m_dataGridViewDataBase.TabIndex = 0;
            this.m_dataGridViewDataBase.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_dataGridViewDataBase_CellContentClick);
            // 
            // ColumnName
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColumnName.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnName.HeaderText = "名称";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            this.ColumnName.Width = 135;
            // 
            // ColumnValue
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColumnValue.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnValue.HeaderText = "参数";
            this.ColumnValue.Name = "ColumnValue";
            this.ColumnValue.Width = 150;
            // 
            // ColumnUse
            // 
            this.ColumnUse.HeaderText = "应用";
            this.ColumnUse.Name = "ColumnUse";
            this.ColumnUse.ReadOnly = true;
            this.ColumnUse.Text = "+";
            this.ColumnUse.UseColumnTextForButtonValue = true;
            this.ColumnUse.Width = 35;
            // 
            // ColumnDesc
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColumnDesc.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColumnDesc.HeaderText = "描述";
            this.ColumnDesc.Name = "ColumnDesc";
            this.ColumnDesc.ReadOnly = true;
            this.ColumnDesc.Width = 150;
            // 
            // ColumnParam
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColumnParam.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColumnParam.HeaderText = "参数说明";
            this.ColumnParam.Name = "ColumnParam";
            this.ColumnParam.ReadOnly = true;
            this.ColumnParam.Width = 250;
            // 
            // m_groupBoxConfig
            // 
            this.m_groupBoxConfig.Controls.Add(this.m_listViewFilter);
            this.m_groupBoxConfig.Controls.Add(this.m_dataGridViewDataBase);
            this.m_groupBoxConfig.Location = new System.Drawing.Point(10, 5);
            this.m_groupBoxConfig.Name = "m_groupBoxConfig";
            this.m_groupBoxConfig.Size = new System.Drawing.Size(645, 220);
            this.m_groupBoxConfig.TabIndex = 1;
            this.m_groupBoxConfig.TabStop = false;
            this.m_groupBoxConfig.Text = "设置";
            // 
            // m_listViewFilter
            // 
            this.m_listViewFilter.FullRowSelect = true;
            this.m_listViewFilter.GridLines = true;
            this.m_listViewFilter.HideSelection = false;
            this.m_listViewFilter.Location = new System.Drawing.Point(10, 20);
            this.m_listViewFilter.MultiSelect = false;
            this.m_listViewFilter.Name = "m_listViewFilter";
            this.m_listViewFilter.Size = new System.Drawing.Size(100, 190);
            this.m_listViewFilter.TabIndex = 1;
            this.m_listViewFilter.UseCompatibleStateImageBehavior = false;
            this.m_listViewFilter.View = System.Windows.Forms.View.Details;
            this.m_listViewFilter.SelectedIndexChanged += new System.EventHandler(this.m_listViewFilter_SelectedIndexChanged);
            // 
            // m_groupBoxPreview
            // 
            this.m_groupBoxPreview.Controls.Add(this.m_listViewPreView);
            this.m_groupBoxPreview.Location = new System.Drawing.Point(10, 230);
            this.m_groupBoxPreview.Name = "m_groupBoxPreview";
            this.m_groupBoxPreview.Size = new System.Drawing.Size(645, 135);
            this.m_groupBoxPreview.TabIndex = 2;
            this.m_groupBoxPreview.TabStop = false;
            this.m_groupBoxPreview.Text = "预览";
            // 
            // m_listViewPreView
            // 
            this.m_listViewPreView.FullRowSelect = true;
            this.m_listViewPreView.GridLines = true;
            this.m_listViewPreView.HideSelection = false;
            this.m_listViewPreView.Location = new System.Drawing.Point(10, 20);
            this.m_listViewPreView.Name = "m_listViewPreView";
            this.m_listViewPreView.Size = new System.Drawing.Size(625, 105);
            this.m_listViewPreView.TabIndex = 0;
            this.m_listViewPreView.UseCompatibleStateImageBehavior = false;
            this.m_listViewPreView.View = System.Windows.Forms.View.Details;
            // 
            // m_buttonApp
            // 
            this.m_buttonApp.Location = new System.Drawing.Point(420, 370);
            this.m_buttonApp.Name = "m_buttonApp";
            this.m_buttonApp.Size = new System.Drawing.Size(75, 23);
            this.m_buttonApp.TabIndex = 3;
            this.m_buttonApp.Text = "应用";
            this.m_buttonApp.UseVisualStyleBackColor = true;
            this.m_buttonApp.Click += new System.EventHandler(this.m_buttonApp_Click);
            // 
            // m_buttonReset
            // 
            this.m_buttonReset.Location = new System.Drawing.Point(500, 370);
            this.m_buttonReset.Name = "m_buttonReset";
            this.m_buttonReset.Size = new System.Drawing.Size(75, 23);
            this.m_buttonReset.TabIndex = 4;
            this.m_buttonReset.Text = "撤销";
            this.m_buttonReset.UseVisualStyleBackColor = true;
            this.m_buttonReset.Click += new System.EventHandler(this.m_buttonReset_Click);
            // 
            // m_buttonClose
            // 
            this.m_buttonClose.Location = new System.Drawing.Point(580, 370);
            this.m_buttonClose.Name = "m_buttonClose";
            this.m_buttonClose.Size = new System.Drawing.Size(75, 23);
            this.m_buttonClose.TabIndex = 5;
            this.m_buttonClose.Text = "关闭";
            this.m_buttonClose.UseVisualStyleBackColor = true;
            this.m_buttonClose.Click += new System.EventHandler(this.m_buttonClose_Click);
            // 
            // ConditionSelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_buttonClose);
            this.Controls.Add(this.m_buttonReset);
            this.Controls.Add(this.m_buttonApp);
            this.Controls.Add(this.m_groupBoxPreview);
            this.Controls.Add(this.m_groupBoxConfig);
            this.Name = "ConditionSelControl";
            this.Size = new System.Drawing.Size(665, 400);
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewDataBase)).EndInit();
            this.m_groupBoxConfig.ResumeLayout(false);
            this.m_groupBoxPreview.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView m_dataGridViewDataBase;
        private System.Windows.Forms.GroupBox m_groupBoxConfig;
        private System.Windows.Forms.GroupBox m_groupBoxPreview;
        private System.Windows.Forms.Button m_buttonApp;
        private System.Windows.Forms.Button m_buttonReset;
        private System.Windows.Forms.Button m_buttonClose;
        private System.Windows.Forms.ListView m_listViewPreView;
        private System.Windows.Forms.ListView m_listViewFilter;

        private System.Data.DataSet m_DataBase;
        private System.Windows.Forms.ListView m_listViewResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnValue;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnUse;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnParam;
    }
}
