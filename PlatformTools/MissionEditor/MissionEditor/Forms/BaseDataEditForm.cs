﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MissionEditor
{
    public partial class BaseDataEditForm : Form
    {
        //////////////////////////////////////////////////////////////////////////
        // 私有成员变量
        private static BaseDataEditForm m_Instance = null;
        private Hashtable m_DataTable = null;
        private bool m_bNewObj = false;
        //////////////////////////////////////////////////////////////////////////

        public static BaseDataEditForm Instance
        {
            get
            {
                if (m_Instance == null || m_Instance.IsDisposed)
                {
                    m_Instance = new BaseDataEditForm();
                }

                return m_Instance;
            }
        }

        public BaseDataEditForm()
        {
            InitializeComponent();
            InitUI();
        }

        public void InitUI()
        {
            // 设置分类显示框
            m_listViewType.Columns.Clear();
            m_listViewType.Columns.Add("显示分类", 96, HorizontalAlignment.Center);

            // 选择框
            m_listViewSel.Columns.Clear();
            m_listViewSel.Columns.Add("显示名称", 150, HorizontalAlignment.Center);
            m_listViewSel.Columns.Add("原子操作", 200, HorizontalAlignment.Left);
            m_listViewSel.Columns.Add("功能描述", 200, HorizontalAlignment.Center);
            m_listViewSel.Columns.Add("参数说明", 300, HorizontalAlignment.Left);
        }

        public void ShowReadOnly(bool bReadOnly)
        {
            m_textBoxName.ReadOnly = bReadOnly;
            m_textBoxAtom.ReadOnly = bReadOnly;
            m_textBoxDesc.ReadOnly = bReadOnly;
            m_textBoxParam.ReadOnly = bReadOnly;

            m_buttonAddType.Visible = bReadOnly;
            m_buttonNew.Enabled = bReadOnly;
            m_buttonEdit.Enabled = bReadOnly;
            m_buttonDel.Enabled = bReadOnly;
            m_buttonSave.Enabled = !bReadOnly;

            if(bReadOnly)
            {
                m_comboBoxType.DropDownStyle = ComboBoxStyle.DropDown;
            }
            else
            {
                m_comboBoxType.DropDownStyle = ComboBoxStyle.DropDownList;
            }
        }

        public void RefreshTypeList(Hashtable table)
        {
            // 设置datatable
            m_DataTable = table;

            // 清空旧的数据
            m_listViewSel.Items.Clear();
            m_listViewType.Items.Clear();
            SetViewDefaultValue();
            m_comboBoxType.Items.Clear();

            // 插入新的数据
            foreach (String strType in table.Keys)
            {
                m_listViewType.Items.Add(strType);
                m_comboBoxType.Items.Add(strType);
            }
        }

        private void SetViewDefaultValue()
        {
            m_comboBoxType.SelectedIndex = -1;
            m_textBoxName.Text = "";
            m_textBoxAtom.Text = "";
            m_textBoxDesc.Text = "";
            m_textBoxParam.Text = "";

            m_buttonEdit.Enabled = false;
            m_buttonDel.Enabled = false;
        }

        private void RefreshSelectList(Hashtable table)
        {
            // 清空旧的数据
            m_listViewSel.Items.Clear();
            SetViewDefaultValue();

            // 插入新的数据
            if(table != null)
            {
                foreach(MissionBaseData m in table.Values)
                {
                    ListViewItem lv = new ListViewItem();

                    lv.SubItems[0].Text = m.Display;
                    lv.SubItems.Add(m.Atom);
                    lv.SubItems.Add(m.Desc);
                    lv.SubItems.Add(m.Param);

                    m_listViewSel.Items.Add(lv);
                }
            }
        }

        private void RefreshView(String strTypeName, String strSelName)
        {
            // 清空旧的数据
            SetViewDefaultValue();

            // 获取sel
            Hashtable table = m_DataTable[strTypeName] as Hashtable;
            MissionBaseData m = table[strSelName] as MissionBaseData;

            // 显示
            m_comboBoxType.SelectedItem = strTypeName;
            m_textBoxName.Text = strSelName;
            m_textBoxAtom.Text = m.Atom;
            m_textBoxDesc.Text = m.Desc;
            m_textBoxParam.Text = m.Param;
        }

        private void m_listViewType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(m_listViewType.SelectedItems.Count > 0)
            {
                ListViewItem lv = m_listViewType.SelectedItems[0];

                if (lv != null)
                {
                    String strTypeName = lv.Text;
                    Hashtable objList = m_DataTable[strTypeName] as Hashtable;
                    RefreshSelectList(objList);
                    ShowReadOnly(true);

                    m_buttonEdit.Enabled = false;
                    m_buttonDel.Enabled = false;
                }
            }
        }

        private void m_listViewSel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_listViewType.SelectedItems.Count > 0)
            {
                ListViewItem lv = m_listViewType.SelectedItems[0];

                if (lv != null)
                {
                    String strTypeName = lv.Text;
                    if (m_listViewSel.SelectedItems.Count > 0)
                    {
                        ListViewItem sellv = m_listViewSel.SelectedItems[0];

                        if (sellv != null)
                        {
                            String strName = sellv.SubItems[0].Text;
                            RefreshView(strTypeName, strName);
                            ShowReadOnly(true);

                            m_buttonEdit.Enabled = true;
                            m_buttonDel.Enabled = true;
                        }
                    }
                }
            }
        }

        private void m_buttonAddType_Click(object sender, EventArgs e)
        {
            String strNewTypeName = m_comboBoxType.Text;
            if(strNewTypeName.Length == 0 || m_DataTable[strNewTypeName] != null)
            {
                MessageBox.Show("新的分类名称不正确或已经存在。");
            }
            else
            {
                DialogResult result = MessageBox.Show("确认添加新的分类---" + strNewTypeName  + "---?", "提示", MessageBoxButtons.YesNo);
                if(result == DialogResult.Yes)
                {
                    m_DataTable[strNewTypeName] = new Hashtable();
                    RefreshTypeList(m_DataTable);
                    MissionBaseDataManager.Instance.SaveToXml(OptionManager.MissionBaseDataPath);
                }
            }
        }

        private void m_buttonSave_Click(object sender, EventArgs e)
        {
            // 获取属性
            String strSaveTypeName = m_comboBoxType.Text;
            String strSaveDisplay = m_textBoxName.Text;
            String strSaveAtom = m_textBoxAtom.Text;
            String strSaveDesc = m_textBoxDesc.Text;
            String StrSaveParam = m_textBoxParam.Text;

            if(m_bNewObj)
            {
                // 建立新的
                Hashtable table = m_DataTable[strSaveTypeName] as Hashtable;
                if (table == null || strSaveDisplay.Length == 0)
                {
                    MessageBox.Show("无法保存！");
                    return;
                }
                MissionBaseData mdb = new MissionBaseData();
                table[strSaveDisplay] = mdb;

                // 写入数据
                mdb.Display = strSaveDisplay;
                mdb.Atom = strSaveAtom;
                mdb.Desc = strSaveDesc;
                mdb.Param = StrSaveParam;

                // 写入文件
                MissionBaseDataManager.Instance.SaveToXml(OptionManager.MissionBaseDataPath);

                // 重置UI
                RefreshTypeList(m_DataTable);
                ShowReadOnly(true);
            }
            else
            {
                if (m_listViewType.SelectedItems.Count > 0)
                {
                    ListViewItem lv = m_listViewType.SelectedItems[0];

                    if (lv != null)
                    {
                        String strTypeName = lv.Text;
                        if (m_listViewSel.SelectedItems.Count > 0)
                        {
                            ListViewItem sellv = m_listViewSel.SelectedItems[0];

                            if (sellv != null)
                            {
                                String strName = sellv.SubItems[0].Text;
                                Hashtable table = m_DataTable[strTypeName] as Hashtable;
                                MissionBaseData mdb = table[strName] as MissionBaseData;
                                
                                if(!strSaveTypeName.Equals(strTypeName) || !strSaveDisplay.Equals(strName))
                                {
                                    // 删除旧的
                                    table.Remove(strName);

                                    // 建立新的
                                    table = m_DataTable[strSaveTypeName] as Hashtable;
                                    mdb = new MissionBaseData();
                                    table[strSaveDisplay] = mdb;
                                    // 写入数据
                                    mdb.Display = strSaveDisplay;
                                    mdb.Atom = strSaveAtom;
                                    mdb.Desc = strSaveDesc;
                                    mdb.Param = StrSaveParam;

                                    // 重置UI
                                    RefreshTypeList(m_DataTable);
                                    ShowReadOnly(true);
                                }
                                else
                                {
                                    mdb.Atom = strSaveAtom;
                                    mdb.Desc = strSaveDesc;
                                    mdb.Param = StrSaveParam;
                                    // 重置UI
                                    RefreshSelectList(table);
                                    ShowReadOnly(true);
                                }

                                // 写入文件
                                MissionBaseDataManager.Instance.SaveToXml(OptionManager.MissionBaseDataPath);
                            }
                        }
                    }
                }
            }

            m_bNewObj = false;
        }

        private void m_buttonEdit_Click(object sender, EventArgs e)
        {
            // 设置
            m_bNewObj = false;
            ShowReadOnly(false);
        }

        private void m_buttonNew_Click(object sender, EventArgs e)
        {
            // 设置
            m_bNewObj = true;
            SetViewDefaultValue();
            ShowReadOnly(false);
        }

        private void m_buttonDel_Click(object sender, EventArgs e)
        {
            if (m_listViewType.SelectedItems.Count > 0)
            {
                ListViewItem lv = m_listViewType.SelectedItems[0];

                if (lv != null)
                {
                    String strTypeName = lv.Text;
                    if (m_listViewSel.SelectedItems.Count > 0)
                    {
                        ListViewItem sellv = m_listViewSel.SelectedItems[0];

                        if (sellv != null)
                        {
                            String strName = sellv.SubItems[0].Text;
                            DialogResult result = MessageBox.Show("确认删除------" + strName, "提示", MessageBoxButtons.YesNo);
                            if(result == DialogResult.Yes)
                            {                               
                                Hashtable table = m_DataTable[strTypeName] as Hashtable;
                                table.Remove(strName);

                                // 写入文件
                                MissionBaseDataManager.Instance.SaveToXml(OptionManager.MissionBaseDataPath);

                                // 重置UI
                                RefreshTypeList(m_DataTable);
                                ShowReadOnly(true);
                            }
                        }
                    }
                }
            }
        }

        private void BaseDataEditForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(MissionBaseDataManager.Instance.Changed)
            {
                MessageBox.Show("数据已发生修改，如果想使用最新的数据，可以‘工具->数据编辑->重新载入’。");
            }
        }
    }
}