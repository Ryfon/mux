﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    // 转换编辑窗口
    public partial class TranslationEditForm : Form
    {
        public TranslationEditForm(TranslationControl ctrl)
        {
            InitializeComponent();
            m_Ctrl = ctrl;
            m_textBoxCurOption.Enabled = false;



        }

        private void SetConditionAttribute(MissionConditionInstance mci)
        {
            if (mci == null)
            {
                m_textBoxConditionDesc.Text = "";
                m_textBoxConditionParam.Text = "";
            }
            else
            {
                m_textBoxConditionDesc.Text = mci.Desc;
                m_textBoxConditionParam.Text = mci.Param;
            }
        }

        private void TranslationEditForm_Load(object sender, EventArgs e)
        {
            // 从 MissionCondition 中获取第一级条件名称
            foreach (String strCond in MissionConditionManager.Instance.ConditionsMap.Keys)
            {
                m_comboBoxFilter.Items.Add(strCond);
            }

            // 显示 转换的 状态名称
            if (m_Ctrl != null && m_Ctrl.StartStateControl != null)
            {
                m_labelFromState.Text = m_Ctrl.StartStateControl.StateName;
            }
            else
            {
                m_labelFromState.Text = null;
            }

            if (m_Ctrl != null && m_Ctrl.EndStateControl != null)
            {
                m_labelToState.Text = m_Ctrl.EndStateControl.StateName;
            }
            else
            {
                m_labelToState.Text = null;
            }

            // 显示转换条件
            Translation trans = MainForm.Instance.OnEditStateDiagram.GetTranslationByID(m_Ctrl.ID);
            foreach (MissionCondition cond in trans.Conditions)
            {
                m_listBoxSelectedConditions.Items.Add(cond.Condition);
                m_listBoxValue.Items.Add(cond.Value);
            }

            m_textBoxCurOption.Text = trans.CurrentOption.ToString();
            if (trans.CurrentOption >= 0)
            {
                m_checkBoxCurOption.Checked = true;
            }
        }

        private void m_comboBoxFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 选择过滤器
            if (m_comboBoxFilter.SelectedItem == null)
            {
                return;
            }

            Hashtable conditionMap = MissionConditionManager.Instance.ConditionsMap[m_comboBoxFilter.SelectedItem.ToString()] as Hashtable;
            if (conditionMap != null)
            {
                m_listBoxAllConditions.Items.Clear();
                foreach (String strCond in conditionMap.Keys)
                {
                    m_listBoxAllConditions.Items.Add(strCond);
                }
            }

            // 更新任务条件描述
            SetConditionAttribute(null);
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void m_checkBoxCurOption_CheckedChanged(object sender, EventArgs e)
        {
            m_textBoxCurOption.Enabled = m_checkBoxCurOption.Checked;
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            // 添加 condition
            if (m_listBoxAllConditions.SelectedItem == null)
            {
                return;
            }

            String strCondition = m_listBoxAllConditions.SelectedItem.ToString();
            if (strCondition == null)
            {
                return;
            }

            try
            {
                m_listBoxSelectedConditions.Items.Add(strCondition);
                m_listBoxValue.Items.Add(m_textBoxValue.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("请检查 对应值 的合法性.");
                return;
            }
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            // 删除 condition
            int iIdx = m_listBoxSelectedConditions.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxValue.Items.RemoveAt(iIdx);
                m_listBoxSelectedConditions.Items.RemoveAt(iIdx);
            }
        }

        private void m_listBoxSelectedConditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iIdx = m_listBoxSelectedConditions.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxValue.SelectedIndex = iIdx;
            }

            // 更新任务条件描述
            if (m_listBoxSelectedConditions.SelectedItem != null)
            {
                String strConditionName = m_listBoxSelectedConditions.SelectedItem.ToString();
                MissionConditionInstance mci = MissionConditionManager.Instance.GetConditionByDisplay(strConditionName);
                SetConditionAttribute(mci);
            }
        }

        public ListBox.ObjectCollection ConditionList
        {
            get
            {
                return m_listBoxSelectedConditions.Items;
            }
        }

        public ListBox.ObjectCollection ValueList
        {
            get
            {
                return m_listBoxValue.Items;
            }
        }

        //public ListBox.ObjectCollection ValueList1
        //{
        //    get
        //    {
        //        return m_listBoxValue1.Items;
        //    }
        //}

        public int CurrentOption
        {
            get
            {
                if (m_checkBoxCurOption.Checked)
                {
                    try
                    {
                        int iCurOption = int.Parse(m_textBoxCurOption.Text);
                        return iCurOption;
                    }
                    catch(Exception)
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }
            }
        }

        private void m_listBoxAllConditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 更新任务条件描述
            if (m_listBoxAllConditions.SelectedItem != null)
            {
                String strConditionName = m_listBoxAllConditions.SelectedItem.ToString();
                MissionConditionInstance mci = MissionConditionManager.Instance.GetConditionByDisplay(strConditionName);
                SetConditionAttribute(mci);
            }
        }

        private void m_listBoxValue_DoubleClick(object sender, EventArgs e)
        {
            if (m_listBoxValue.SelectedIndex < 0)
            {
                return;
            }
            else
            {
                NameInputForm inputForm = new NameInputForm(m_listBoxValue.SelectedItem.ToString());
                inputForm.ShowDialog();

                if (inputForm.IsOk)
                {
                    int iIdx = m_listBoxValue.SelectedIndex;
                    m_listBoxValue.Items[iIdx] = inputForm.InputName;
                }
            }
        }
    }
}