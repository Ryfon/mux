﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MissionEditor
{
    public partial class ReportExportForm : Form
    {
        public ReportExportForm()
        {
            InitializeComponent();
            InitUI();
        }

        public static ReportExportForm Instance
        {
            get
            {
                if (m_Instance == null || m_Instance.IsDisposed)
                {
                    m_Instance = new ReportExportForm();
                }

                return m_Instance;
            }
        }

        private void InitUI()
        {
            // 地图下拉框
            ArrayList arrMapName = SceneSketchManager.Instance.GetSceneNameList();
            foreach (String strMapName in arrMapName)
            {
                m_comboBoxMapFilter.Items.Add(strMapName);
            }
            m_comboBoxMapFilter.SelectedIndex = 0;

            // 任务class下拉框
            Hashtable classTable = MainForm.Instance.MissionMgr.MissionClasses;
            int iClassNum = classTable.Count;

            //for (int index = 0; index < iClassNum; index++)
            //{
            //    String strClassName = classTable[index] as String;
            //    m_comboBoxClassFilter.Items.Add(strClassName);
            //}
            foreach(String strClassName in classTable.Values)
            {
                m_comboBoxClassFilter.Items.Add(strClassName);
            }
            m_comboBoxClassFilter.SelectedIndex = 0;

            // 导出路径
            m_textBoxExportPath.Text = Application.StartupPath;
        }

        private void m_buttonFolderSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.RootFolder = Environment.SpecialFolder.MyComputer;
            DialogResult result = dlg.ShowDialog();
            if(result == DialogResult.OK)
            {
                m_textBoxExportPath.Text = dlg.SelectedPath;
            }
        }

        private void ReportExportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void m_buttonExport_Click(object sender, EventArgs e)
        {
            if (System.IO.Directory.Exists(m_textBoxExportPath.Text))
            {
                int iExportNum = ReportManager.Instance.ExportAll(m_comboBoxMapFilter.SelectedItem.ToString(), m_comboBoxClassFilter.SelectedItem.ToString(), m_textBoxExportPath.Text);
                MessageBox.Show("成功导出任务" + iExportNum.ToString() + "条。");
            } 
            else
            {
                MessageBox.Show("导出路径不存在，请重新设置！");
            }
        }
    }
}