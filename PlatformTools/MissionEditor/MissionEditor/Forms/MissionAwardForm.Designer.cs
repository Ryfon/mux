﻿namespace MissionEditor
{
    partial class MissionAwardForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_btnRemove = new System.Windows.Forms.Button();
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_listBoxValue = new System.Windows.Forms.ListBox();
            this.m_listBoxSelectedAwards = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_textBoxValue = new System.Windows.Forms.TextBox();
            this.m_listBoxAllAwards = new System.Windows.Forms.ListBox();
            this.m_comboBoxFilter = new System.Windows.Forms.ComboBox();
            this.m_textBoxAwardParam = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_textBoxAwardDesc = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(262, 221);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(57, 25);
            this.m_btnOK.TabIndex = 17;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_btnRemove
            // 
            this.m_btnRemove.Location = new System.Drawing.Point(151, 108);
            this.m_btnRemove.Name = "m_btnRemove";
            this.m_btnRemove.Size = new System.Drawing.Size(26, 21);
            this.m_btnRemove.TabIndex = 16;
            this.m_btnRemove.Text = "<-";
            this.m_btnRemove.UseVisualStyleBackColor = true;
            this.m_btnRemove.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Location = new System.Drawing.Point(151, 81);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(26, 21);
            this.m_btnAdd.TabIndex = 15;
            this.m_btnAdd.Text = "->";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_listBoxValue
            // 
            this.m_listBoxValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listBoxValue.FormattingEnabled = true;
            this.m_listBoxValue.ItemHeight = 12;
            this.m_listBoxValue.Location = new System.Drawing.Point(325, 43);
            this.m_listBoxValue.Name = "m_listBoxValue";
            this.m_listBoxValue.Size = new System.Drawing.Size(380, 172);
            this.m_listBoxValue.TabIndex = 14;
            // 
            // m_listBoxSelectedAwards
            // 
            this.m_listBoxSelectedAwards.FormattingEnabled = true;
            this.m_listBoxSelectedAwards.ItemHeight = 12;
            this.m_listBoxSelectedAwards.Location = new System.Drawing.Point(186, 43);
            this.m_listBoxSelectedAwards.Name = "m_listBoxSelectedAwards";
            this.m_listBoxSelectedAwards.Size = new System.Drawing.Size(133, 172);
            this.m_listBoxSelectedAwards.TabIndex = 13;
            this.m_listBoxSelectedAwards.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSelectedAwards_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 224);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 12;
            this.label1.Text = "对应值";
            // 
            // m_textBoxValue
            // 
            this.m_textBoxValue.Location = new System.Drawing.Point(65, 221);
            this.m_textBoxValue.Name = "m_textBoxValue";
            this.m_textBoxValue.Size = new System.Drawing.Size(78, 21);
            this.m_textBoxValue.TabIndex = 11;
            this.m_textBoxValue.Text = "0";
            // 
            // m_listBoxAllAwards
            // 
            this.m_listBoxAllAwards.FormattingEnabled = true;
            this.m_listBoxAllAwards.ItemHeight = 12;
            this.m_listBoxAllAwards.Location = new System.Drawing.Point(15, 43);
            this.m_listBoxAllAwards.Name = "m_listBoxAllAwards";
            this.m_listBoxAllAwards.Size = new System.Drawing.Size(129, 172);
            this.m_listBoxAllAwards.TabIndex = 10;
            this.m_listBoxAllAwards.SelectedIndexChanged += new System.EventHandler(this.m_listBoxAllAwards_SelectedIndexChanged);
            // 
            // m_comboBoxFilter
            // 
            this.m_comboBoxFilter.FormattingEnabled = true;
            this.m_comboBoxFilter.Location = new System.Drawing.Point(14, 14);
            this.m_comboBoxFilter.Name = "m_comboBoxFilter";
            this.m_comboBoxFilter.Size = new System.Drawing.Size(131, 20);
            this.m_comboBoxFilter.TabIndex = 9;
            this.m_comboBoxFilter.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxFilter_SelectedIndexChanged);
            // 
            // m_textBoxAwardParam
            // 
            this.m_textBoxAwardParam.Location = new System.Drawing.Point(80, 282);
            this.m_textBoxAwardParam.Multiline = true;
            this.m_textBoxAwardParam.Name = "m_textBoxAwardParam";
            this.m_textBoxAwardParam.ReadOnly = true;
            this.m_textBoxAwardParam.Size = new System.Drawing.Size(370, 105);
            this.m_textBoxAwardParam.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 286);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 20;
            this.label3.Text = "参数说明:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 259);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 19;
            this.label2.Text = "功能描述:";
            // 
            // m_textBoxAwardDesc
            // 
            this.m_textBoxAwardDesc.Location = new System.Drawing.Point(80, 255);
            this.m_textBoxAwardDesc.Name = "m_textBoxAwardDesc";
            this.m_textBoxAwardDesc.ReadOnly = true;
            this.m_textBoxAwardDesc.Size = new System.Drawing.Size(370, 21);
            this.m_textBoxAwardDesc.TabIndex = 18;
            // 
            // MissionAwardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 396);
            this.Controls.Add(this.m_textBoxAwardParam);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_textBoxAwardDesc);
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_btnRemove);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.m_listBoxValue);
            this.Controls.Add(this.m_listBoxSelectedAwards);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_textBoxValue);
            this.Controls.Add(this.m_listBoxAllAwards);
            this.Controls.Add(this.m_comboBoxFilter);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MissionAwardForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "任务奖励编辑";
            this.Load += new System.EventHandler(this.MissionAwardForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.Button m_btnRemove;
        private System.Windows.Forms.Button m_btnAdd;
        private System.Windows.Forms.ListBox m_listBoxValue;
        private System.Windows.Forms.ListBox m_listBoxSelectedAwards;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_textBoxValue;
        private System.Windows.Forms.ListBox m_listBoxAllAwards;
        private System.Windows.Forms.ComboBox m_comboBoxFilter;
        private System.Windows.Forms.TextBox m_textBoxAwardParam;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_textBoxAwardDesc;
    }
}