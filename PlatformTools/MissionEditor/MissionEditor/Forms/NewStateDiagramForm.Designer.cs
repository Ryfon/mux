﻿namespace MissionEditor
{
    partial class NewStateDiagramForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_comboBoxTarget = new System.Windows.Forms.ComboBox();
            this.m_radioButtonNPC = new System.Windows.Forms.RadioButton();
            this.m_radioButtonItem = new System.Windows.Forms.RadioButton();
            this.m_radioButtonArea = new System.Windows.Forms.RadioButton();
            this.m_btnCreate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_radioButtonDeath = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // m_comboBoxTarget
            // 
            this.m_comboBoxTarget.FormattingEnabled = true;
            this.m_comboBoxTarget.Location = new System.Drawing.Point(12, 117);
            this.m_comboBoxTarget.Name = "m_comboBoxTarget";
            this.m_comboBoxTarget.Size = new System.Drawing.Size(128, 20);
            this.m_comboBoxTarget.TabIndex = 0;
            // 
            // m_radioButtonNPC
            // 
            this.m_radioButtonNPC.AutoSize = true;
            this.m_radioButtonNPC.Location = new System.Drawing.Point(12, 29);
            this.m_radioButtonNPC.Name = "m_radioButtonNPC";
            this.m_radioButtonNPC.Size = new System.Drawing.Size(41, 16);
            this.m_radioButtonNPC.TabIndex = 1;
            this.m_radioButtonNPC.TabStop = true;
            this.m_radioButtonNPC.Text = "NPC";
            this.m_radioButtonNPC.UseVisualStyleBackColor = true;
            this.m_radioButtonNPC.CheckedChanged += new System.EventHandler(this.m_radioButtonNPC_CheckedChanged);
            // 
            // m_radioButtonItem
            // 
            this.m_radioButtonItem.AutoSize = true;
            this.m_radioButtonItem.Location = new System.Drawing.Point(12, 51);
            this.m_radioButtonItem.Name = "m_radioButtonItem";
            this.m_radioButtonItem.Size = new System.Drawing.Size(47, 16);
            this.m_radioButtonItem.TabIndex = 2;
            this.m_radioButtonItem.TabStop = true;
            this.m_radioButtonItem.Text = "Item";
            this.m_radioButtonItem.UseVisualStyleBackColor = true;
            this.m_radioButtonItem.CheckedChanged += new System.EventHandler(this.m_radioButtonItem_CheckedChanged);
            // 
            // m_radioButtonArea
            // 
            this.m_radioButtonArea.AutoSize = true;
            this.m_radioButtonArea.Location = new System.Drawing.Point(12, 73);
            this.m_radioButtonArea.Name = "m_radioButtonArea";
            this.m_radioButtonArea.Size = new System.Drawing.Size(47, 16);
            this.m_radioButtonArea.TabIndex = 3;
            this.m_radioButtonArea.TabStop = true;
            this.m_radioButtonArea.Text = "Area";
            this.m_radioButtonArea.UseVisualStyleBackColor = true;
            this.m_radioButtonArea.CheckedChanged += new System.EventHandler(this.m_radioButtonArea_CheckedChanged);
            // 
            // m_btnCreate
            // 
            this.m_btnCreate.Location = new System.Drawing.Point(28, 150);
            this.m_btnCreate.Name = "m_btnCreate";
            this.m_btnCreate.Size = new System.Drawing.Size(86, 23);
            this.m_btnCreate.TabIndex = 4;
            this.m_btnCreate.Text = "创建";
            this.m_btnCreate.UseVisualStyleBackColor = true;
            this.m_btnCreate.Click += new System.EventHandler(this.m_btnCreate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "请选择状态图的目标";
            // 
            // m_radioButtonDeath
            // 
            this.m_radioButtonDeath.AutoSize = true;
            this.m_radioButtonDeath.Location = new System.Drawing.Point(12, 95);
            this.m_radioButtonDeath.Name = "m_radioButtonDeath";
            this.m_radioButtonDeath.Size = new System.Drawing.Size(53, 16);
            this.m_radioButtonDeath.TabIndex = 6;
            this.m_radioButtonDeath.TabStop = true;
            this.m_radioButtonDeath.Text = "Death";
            this.m_radioButtonDeath.UseVisualStyleBackColor = true;
            this.m_radioButtonDeath.CheckedChanged += new System.EventHandler(this.m_radioButtonDeath_CheckedChanged);
            // 
            // NewStateDiagramForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(155, 184);
            this.Controls.Add(this.m_radioButtonDeath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_btnCreate);
            this.Controls.Add(this.m_radioButtonArea);
            this.Controls.Add(this.m_radioButtonItem);
            this.Controls.Add(this.m_radioButtonNPC);
            this.Controls.Add(this.m_comboBoxTarget);
            this.Name = "NewStateDiagramForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "创建状态图";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox m_comboBoxTarget;
        private System.Windows.Forms.RadioButton m_radioButtonNPC;
        private System.Windows.Forms.RadioButton m_radioButtonItem;
        private System.Windows.Forms.RadioButton m_radioButtonArea;
        private System.Windows.Forms.Button m_btnCreate;
        private System.String m_strDiaName = null;
        private int m_iDiaID = -1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton m_radioButtonDeath;
    }
}