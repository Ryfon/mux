﻿namespace MissionEditor
{
    partial class ReportExportForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_comboBoxMapFilter = new System.Windows.Forms.ComboBox();
            this.m_comboBoxClassFilter = new System.Windows.Forms.ComboBox();
            this.m_textBoxExportPath = new System.Windows.Forms.TextBox();
            this.m_buttonFolderSelect = new System.Windows.Forms.Button();
            this.m_buttonExport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_comboBoxMapFilter
            // 
            this.m_comboBoxMapFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxMapFilter.FormattingEnabled = true;
            this.m_comboBoxMapFilter.Items.AddRange(new object[] {
            "全部地图"});
            this.m_comboBoxMapFilter.Location = new System.Drawing.Point(10, 20);
            this.m_comboBoxMapFilter.Name = "m_comboBoxMapFilter";
            this.m_comboBoxMapFilter.Size = new System.Drawing.Size(90, 20);
            this.m_comboBoxMapFilter.TabIndex = 0;
            // 
            // m_comboBoxClassFilter
            // 
            this.m_comboBoxClassFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxClassFilter.FormattingEnabled = true;
            this.m_comboBoxClassFilter.Items.AddRange(new object[] {
            "全部类别"});
            this.m_comboBoxClassFilter.Location = new System.Drawing.Point(100, 20);
            this.m_comboBoxClassFilter.Name = "m_comboBoxClassFilter";
            this.m_comboBoxClassFilter.Size = new System.Drawing.Size(90, 20);
            this.m_comboBoxClassFilter.TabIndex = 1;
            // 
            // m_textBoxExportPath
            // 
            this.m_textBoxExportPath.Location = new System.Drawing.Point(190, 20);
            this.m_textBoxExportPath.Name = "m_textBoxExportPath";
            this.m_textBoxExportPath.Size = new System.Drawing.Size(290, 21);
            this.m_textBoxExportPath.TabIndex = 2;
            // 
            // m_buttonFolderSelect
            // 
            this.m_buttonFolderSelect.Location = new System.Drawing.Point(480, 19);
            this.m_buttonFolderSelect.Name = "m_buttonFolderSelect";
            this.m_buttonFolderSelect.Size = new System.Drawing.Size(31, 23);
            this.m_buttonFolderSelect.TabIndex = 3;
            this.m_buttonFolderSelect.Text = "...";
            this.m_buttonFolderSelect.UseVisualStyleBackColor = true;
            this.m_buttonFolderSelect.Click += new System.EventHandler(this.m_buttonFolderSelect_Click);
            // 
            // m_buttonExport
            // 
            this.m_buttonExport.Location = new System.Drawing.Point(515, 19);
            this.m_buttonExport.Name = "m_buttonExport";
            this.m_buttonExport.Size = new System.Drawing.Size(75, 23);
            this.m_buttonExport.TabIndex = 4;
            this.m_buttonExport.Text = "导出";
            this.m_buttonExport.UseVisualStyleBackColor = true;
            this.m_buttonExport.Click += new System.EventHandler(this.m_buttonExport_Click);
            // 
            // ReportExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 66);
            this.Controls.Add(this.m_buttonExport);
            this.Controls.Add(this.m_buttonFolderSelect);
            this.Controls.Add(this.m_textBoxExportPath);
            this.Controls.Add(this.m_comboBoxClassFilter);
            this.Controls.Add(this.m_comboBoxMapFilter);
            this.Name = "ReportExportForm";
            this.Text = "导出任务报告";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportExportForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox m_comboBoxMapFilter;
        private System.Windows.Forms.ComboBox m_comboBoxClassFilter;
        private System.Windows.Forms.TextBox m_textBoxExportPath;
        private System.Windows.Forms.Button m_buttonFolderSelect;
        private System.Windows.Forms.Button m_buttonExport;
        private static ReportExportForm m_Instance = null;
    }
}