﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace MissionEditor
{
    // 状态编辑窗口
    public partial class StateEditForm : Form
    {
        public StateEditForm(StateControl ctrl, String stateDiaName)
        {
            InitializeComponent();
            m_Ctrl = ctrl;
            m_textBoxStateName.Text = m_Ctrl.StateName;
            m_strStateDiagramName = stateDiaName;
            ShowStateDialog();
            //m_comboBoxTrigger.SelectedIndex = 0;
        }

        // 设置原子操作参数说明
        private void SetAtomAttribute(MissionAtomInstance mti)
        {
            if(mti == null)
            {
                m_textBoxAtomDesc.Text = "";
                m_textBoxAtomParam.Text = "";
            }
            else
            {
                m_textBoxAtomDesc.Text = mti.Desc;
                m_textBoxAtomParam.Text = mti.Param;
            }
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            SaveCurrentState();

            m_Ctrl.StateName = m_textBoxStateName.Text;
            this.Dispose();
        }

        private void m_btnCancel_Click(object sender, EventArgs e)
        {
               this.Dispose();
        }

        private void StateEditForm_Load(object sender, EventArgs e)
        {
            // 事件触发器列表
            m_comboBoxTrigger.Items.Clear();
            String[] names = System.Enum.GetNames(typeof(OptionManager.StateActionTrigger));
            foreach (String strName in names)
            {
                m_comboBoxTrigger.Items.Add(strName);
            }

            // AtomAction 列表
            m_listBoxAllActions.Items.Clear();
            foreach (String strActionName in AtomActionManager.Instance.AtomActionList.Keys)
            {
                m_listBoxAllActions.Items.Add(strActionName);
            }
            m_comboBoxTrigger.SelectedItem = OptionManager.StateActionTrigger.OnEnter.ToString();
            //names = System.Enum.GetNames(typeof(OptionManager.StateAction));
            //foreach (String strName in names)
            //{
            //    m_listBoxAllActions.Items.Add(strName);
            //}

        }

        private void m_btnAddAction_Click(object sender, EventArgs e)
        {
            if (m_comboBoxTrigger.SelectedIndex == -1)
            {
                return;
            }

            try
            {
                // 添加行为
                if (m_listBoxAllActions.SelectedItem != null)
                {
                    //if (!m_listBoxSelectedActions.Items.Contains(m_listBoxAllActions.SelectedItem))
                    m_listBoxSelectedActions.Items.Add(m_listBoxAllActions.SelectedItem);
                    m_listBoxActionValue.Items.Add(m_textBoxActionValue.Text);
                    // 在 m_AtomActionInstanceList 中同步添加
                    //m_AtomActionInstanceList[m_listBoxAllActions.SelectedItem.ToString()] = m_textBoxActionValue.Text;
                    m_bActionsChanged = true;
                }
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void m_btnRemoveAction_Click(object sender, EventArgs e)
        {
            // 移除行为
            int iIdx = m_listBoxSelectedActions.SelectedIndex;
            if (iIdx != -1)
            {
                // 同步删除
                //m_AtomActionInstanceList.Remove(m_listBoxSelectedActions.SelectedItem.ToString());
                m_listBoxActionValue.Items.RemoveAt(iIdx);
                m_listBoxSelectedActions.Items.RemoveAt(iIdx);

                m_bActionsChanged = true;
            }
        }

        private void m_comboBoxTrigger_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 从当前正编辑 State 中获取用户选取的 trigger 对应的行为列表
            State state = GetOnEditState();
            ArrayList actionList = null;
            if (m_comboBoxTrigger.SelectedItem.ToString().Equals(OptionManager.StateActionTrigger.OnEnter.ToString()))
            {
                actionList = state.ActionListOnEnter;
            }
            else if (m_comboBoxTrigger.SelectedItem.ToString().Equals(OptionManager.StateActionTrigger.OnOption.ToString()))
            {
                actionList = state.ActionListOnOption;
            }
            else if (m_comboBoxTrigger.SelectedItem.ToString().Equals(OptionManager.StateActionTrigger.OnExit.ToString()))
            {
                actionList = state.ActionListOnExit;
            }

            if (actionList == null)
            {
                return;
            }

            //m_AtomActionInstanceList.Clear();
            m_listBoxSelectedActions.Items.Clear();
            m_listBoxActionValue.Items.Clear();
            foreach (AtomActionInstance actionInst in actionList)
            {
                m_listBoxSelectedActions.Items.Add(actionInst.AtomDisplay);
                //m_AtomActionInstanceList[actionInst.AtomDisplay] = actionList[obj];
                m_listBoxActionValue.Items.Add(actionInst.Parameters);
            }

            // 清空参数说明
            SetAtomAttribute(null);

            m_bActionsChanged = false;
        }

        private void m_comboBoxTrigger_DropDown(object sender, EventArgs e)
        {
            // 判断当前的 Action 是否有改变。如果发生改变提示用户是否保存。
            // 选择触发器
            if (m_bActionsChanged)
            {
                // 如果行为信息发生改变，提示是否保存当前改变。
                //DialogResult result = MessageBox.Show("行为信息已经改变。是否保存当前更改？", "提示", MessageBoxButtons.YesNoCancel);

                //if (result == DialogResult.Yes)
                //{
                SaveCurrentState();
                    
                //}
            }
        }

        // 获取当前正编辑的状态
        private State GetOnEditState()
        {
            // 从 StateDiagramManager 中找到当前 StateDiagram.
            StateDiagram stateDia = StateDiagramManager.Instance.GetStateDiagram(m_strStateDiagramName);
            if (stateDia == null)
            {
                // 不应该发生的异常。
                MessageBox.Show("发生异常，找不到名为 " + m_strStateDiagramName + " 的状态图。");
                return null;
            }

            // 从 StateDia 中找到当前编辑的状态
            State state = stateDia.GetStateByID(m_Ctrl.StateID);
            if (state == null)
            {
                // 不应该发生的异常。
                MessageBox.Show("发生异常，从状态图 "+m_strStateDiagramName+" 中找不到名为 " +m_Ctrl.StateName+ " 的状态。");
                return null;
            }

            return state;
        }

        
        private void SaveCurrentState()
        {
            // 保存当前状态的触发条件的行为列表
            if (m_comboBoxTrigger.SelectedItem == null)
            {
                m_bActionsChanged = false;
                return;
            }

            // 获取当前正被编辑的状态
            State state = GetOnEditState();

            // 将改变的行为保存到 state 中
            ArrayList actionList = null;
            if (m_comboBoxTrigger.SelectedItem.ToString().Equals(OptionManager.StateActionTrigger.OnEnter.ToString()))
            {
                actionList = state.ActionListOnEnter;
            }
            else if (m_comboBoxTrigger.SelectedItem.ToString().Equals(OptionManager.StateActionTrigger.OnOption.ToString()))
            {
                actionList = state.ActionListOnOption;
            }
            else if (m_comboBoxTrigger.SelectedItem.ToString().Equals(OptionManager.StateActionTrigger.OnExit.ToString()))
            {
                actionList = state.ActionListOnExit;
            }

            // 将 m_AtomActionInstanceList 拷贝到对应 trigger 的 action list 中
            actionList.Clear();
            for (int i = 0; i < m_listBoxSelectedActions.Items.Count; i++)
            {
                String strAtomDisplay = m_listBoxSelectedActions.Items[i] as String;
                String strParameter = m_listBoxActionValue.Items[i] as String;
                actionList.Add(new AtomActionInstance(strAtomDisplay, strParameter));
            }
            //foreach (Object obj in m_AtomActionInstanceList.Keys)
            //{
            //    actionList[obj] = m_AtomActionInstanceList[obj];
            //}
            //m_AtomActionInstanceList.Clear();

            // 保存对话
                state.Dialog = m_richTextBoxNPCToPlayer.Text;

            // 保存 Options
            //state.OptionList.Clear();
            //for (int i = 0; i < m_listBoxOptions.Items.Count; i++)
            //{
            //    String strOption = m_listBoxOptions.Items[i].ToString();
            //    int iValue = int.Parse(m_listBoxOptionValues.Items[i].ToString());
            //    state.OptionList[strOption] = iValue;
            //}

            m_bActionsChanged = false;

            StateDiagram stateDia = StateDiagramManager.Instance.GetStateDiagram(m_strStateDiagramName);

            StateDiagramManager.SaveStateDiagram(stateDia);
            
        }

        // 显示当前 State 的 dialog / option
        void ShowStateDialog()
        {
            State state = GetOnEditState();
            m_richTextBoxNPCToPlayer.Text = state.Dialog;
            m_listBoxOptions.Items.Clear();
            m_listBoxOptionValues.Items.Clear();
            foreach (DialogOption option in state.OptionList)
            {
                m_listBoxOptions.Items.Add(option.OptionString);
                m_listBoxOptionValues.Items.Add(option.Value.ToString());
            }
        }

        // 添加选项
        private void m_btnAddOption_Click(object sender, EventArgs e)
        {
            if (m_textBoxToAdd.Text == null)
            {
                return;
            }

            int iValue = -1;

            try
            {
                iValue = int.Parse(m_textBoxOptionValue.Text);
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
                return;
            }

            m_listBoxOptions.Items.Add(m_textBoxToAdd.Text);
            m_listBoxOptionValues.Items.Add(iValue.ToString());
            GetOnEditState().OptionList.Add(new DialogOption(m_textBoxToAdd.Text, iValue, -1, StateDiagram.ElementCategory.EC_OTHER));
            m_textBoxToAdd.Text = "";

        }

        // 修改按钮相应
        private void m_btnDeleteOption_Click(object sender, EventArgs e)
        {
            int iIdx = m_listBoxOptions.SelectedIndex;
            if (iIdx >= 0)
            {
                State state = GetOnEditState();
                state.OptionList.RemoveAt(iIdx);
                m_listBoxOptionValues.Items.RemoveAt(iIdx);
                m_listBoxOptions.Items.RemoveAt(iIdx);
            }
        }

        private void m_listBoxOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_listBoxOptionValues.SelectedIndex = m_listBoxOptions.SelectedIndex;
        }

        private void m_listBoxSelectedActions_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_listBoxActionValue.SelectedIndex = m_listBoxSelectedActions.SelectedIndex;

            // 设置参数说明
            if(m_listBoxSelectedActions.SelectedItem != null)
            {
                String strAtomName = m_listBoxSelectedActions.SelectedItem.ToString();
                MissionAtomInstance mti = AtomActionManager.Instance.GetAtomByDisplay(strAtomName);
                SetAtomAttribute(mti);
            }
        }

        private void RefreshOptionList()
        {
            m_listBoxOptions.Items.Clear();
            m_listBoxOptionValues.Items.Clear();

            State state = GetOnEditState();
            foreach (DialogOption option in state.OptionList)
            {
                m_listBoxOptions.Items.Add(option.OptionString);
                m_listBoxOptionValues.Items.Add(option.Value.ToString());
            }
        }
        private void m_listBoxOptions_DoubleClick(object sender, EventArgs e)
        {
            // 选项列表中双击，编辑当前被选中的选项
            int iSelected = m_listBoxOptions.SelectedIndex;
            if (iSelected < 0)
            {
                return;
            }

            State state = GetOnEditState();
            DialogOptionEditForm form = new DialogOptionEditForm(state.OptionList[iSelected] as DialogOption);
            form.ShowDialog();

            // Option 字符串和对应的值可能发生改变.刷新 Option list
            RefreshOptionList();
        }

        private void m_listBoxActionValue_DoubleClick(object sender, EventArgs e)
        {
            // 双击 action value list box. 设置值
            if (m_listBoxActionValue.SelectedIndex < 0)
            {
                return;
            }

            NameInputForm inputForm = new NameInputForm(m_listBoxActionValue.SelectedItem.ToString());
            inputForm.ShowDialog();

            if (inputForm.IsOk)
            {
                int iIdx = m_listBoxActionValue.SelectedIndex;
                m_listBoxActionValue.Items[iIdx] = inputForm.InputName;
            }
        }

        private void m_listBoxAllActions_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 设置参数说明
            if (m_listBoxAllActions.SelectedItem != null)
            {
                String strAtomName = m_listBoxAllActions.SelectedItem.ToString();
                MissionAtomInstance mti = AtomActionManager.Instance.GetAtomByDisplay(strAtomName);
                SetAtomAttribute(mti);
            }
        }

        private void m_btnUpOption_Click(object sender, EventArgs e)
        {
            int iIdx = m_listBoxOptions.SelectedIndex;
            if (iIdx >= 0)
            {
                State state = GetOnEditState();
                if (state != null)
                {
                    if(state.OptionUp(iIdx))
                    {
                        RefreshOptionList();
                        m_listBoxOptions.SelectedIndex = iIdx - 1;
                    }
                }
            }
        }

        private void m_btnDownOption_Click(object sender, EventArgs e)
        {
            int iIdx = m_listBoxOptions.SelectedIndex;
            if (iIdx >= 0)
            {
                State state = GetOnEditState();
                if (state != null)
                {
                    if (state.OptionDown(iIdx))
                    {
                        RefreshOptionList();
                        m_listBoxOptions.SelectedIndex = iIdx + 1;
                    }
                }
            }
        }


    }
}