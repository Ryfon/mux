﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class NameInputForm : Form
    {
        public String InputName
        {
            get
            {
                return m_textBoxName.Text;
            }
        }

        public bool IsOk
        {
            get
            {
                return m_bOk;
            }
        }

        public NameInputForm(String strInitTex)
        {
            InitializeComponent();
            m_textBoxName.Text = strInitTex;
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            m_bOk = true;
            this.Close();
        }
    }
}