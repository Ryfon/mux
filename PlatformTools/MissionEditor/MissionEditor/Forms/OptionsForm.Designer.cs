﻿namespace MissionEditor
{
    partial class OptionsForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_btnCancel = new System.Windows.Forms.Button();
            this.m_comboBoxMissionDisplayMode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_comboBoxNPCDisplayMode = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_comboBoxItemDisplayMode = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_pictureBoxAutoState = new System.Windows.Forms.PictureBox();
            this.m_pictureBoxUserState = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_pictureBoxAutoTranslation = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_pictureBoxUserTranslation = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.m_pictureBoxInvalidateTranslation = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_pictureBoxInvalidateState = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.m_comboBoxSceneSketchSize = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.m_textBoxSingleNpcSize = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.m_textBoxLevelDifference = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.m_textBoxNpcMoveRangeSize = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.m_textBoxTeamWorkServerPath = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxAutoState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxUserState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxAutoTranslation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxUserTranslation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxInvalidateTranslation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxInvalidateState)).BeginInit();
            this.SuspendLayout();
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(81, 320);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(69, 28);
            this.m_btnOK.TabIndex = 0;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_btnCancel
            // 
            this.m_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_btnCancel.Location = new System.Drawing.Point(227, 320);
            this.m_btnCancel.Name = "m_btnCancel";
            this.m_btnCancel.Size = new System.Drawing.Size(63, 27);
            this.m_btnCancel.TabIndex = 1;
            this.m_btnCancel.Text = "取消";
            this.m_btnCancel.UseVisualStyleBackColor = true;
            this.m_btnCancel.Click += new System.EventHandler(this.m_btnCancel_Click);
            // 
            // m_comboBoxMissionDisplayMode
            // 
            this.m_comboBoxMissionDisplayMode.FormattingEnabled = true;
            this.m_comboBoxMissionDisplayMode.Items.AddRange(new object[] {
            "ID",
            "名称",
            "ID_名称"});
            this.m_comboBoxMissionDisplayMode.Location = new System.Drawing.Point(126, 12);
            this.m_comboBoxMissionDisplayMode.Name = "m_comboBoxMissionDisplayMode";
            this.m_comboBoxMissionDisplayMode.Size = new System.Drawing.Size(108, 20);
            this.m_comboBoxMissionDisplayMode.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "任务列表显示模式";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "NPC 列表显示模式";
            // 
            // m_comboBoxNPCDisplayMode
            // 
            this.m_comboBoxNPCDisplayMode.FormattingEnabled = true;
            this.m_comboBoxNPCDisplayMode.Items.AddRange(new object[] {
            "ID",
            "名称",
            "ID_名称"});
            this.m_comboBoxNPCDisplayMode.Location = new System.Drawing.Point(126, 38);
            this.m_comboBoxNPCDisplayMode.Name = "m_comboBoxNPCDisplayMode";
            this.m_comboBoxNPCDisplayMode.Size = new System.Drawing.Size(108, 20);
            this.m_comboBoxNPCDisplayMode.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "道具列表显示模式";
            // 
            // m_comboBoxItemDisplayMode
            // 
            this.m_comboBoxItemDisplayMode.FormattingEnabled = true;
            this.m_comboBoxItemDisplayMode.Items.AddRange(new object[] {
            "ID",
            "名称",
            "ID_名称"});
            this.m_comboBoxItemDisplayMode.Location = new System.Drawing.Point(126, 64);
            this.m_comboBoxItemDisplayMode.Name = "m_comboBoxItemDisplayMode";
            this.m_comboBoxItemDisplayMode.Size = new System.Drawing.Size(108, 20);
            this.m_comboBoxItemDisplayMode.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "自动创建状态色";
            // 
            // m_pictureBoxAutoState
            // 
            this.m_pictureBoxAutoState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pictureBoxAutoState.Location = new System.Drawing.Point(115, 97);
            this.m_pictureBoxAutoState.Name = "m_pictureBoxAutoState";
            this.m_pictureBoxAutoState.Size = new System.Drawing.Size(42, 21);
            this.m_pictureBoxAutoState.TabIndex = 9;
            this.m_pictureBoxAutoState.TabStop = false;
            this.m_pictureBoxAutoState.Click += new System.EventHandler(this.m_pictureBoxAutoState_Click);
            // 
            // m_pictureBoxUserState
            // 
            this.m_pictureBoxUserState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pictureBoxUserState.Location = new System.Drawing.Point(115, 124);
            this.m_pictureBoxUserState.Name = "m_pictureBoxUserState";
            this.m_pictureBoxUserState.Size = new System.Drawing.Size(42, 21);
            this.m_pictureBoxUserState.TabIndex = 11;
            this.m_pictureBoxUserState.TabStop = false;
            this.m_pictureBoxUserState.Click += new System.EventHandler(this.m_pictureBoxUserState_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "用户创建状态色";
            // 
            // m_pictureBoxAutoTranslation
            // 
            this.m_pictureBoxAutoTranslation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pictureBoxAutoTranslation.Location = new System.Drawing.Point(274, 97);
            this.m_pictureBoxAutoTranslation.Name = "m_pictureBoxAutoTranslation";
            this.m_pictureBoxAutoTranslation.Size = new System.Drawing.Size(42, 21);
            this.m_pictureBoxAutoTranslation.TabIndex = 13;
            this.m_pictureBoxAutoTranslation.TabStop = false;
            this.m_pictureBoxAutoTranslation.Click += new System.EventHandler(this.m_pictureBoxAutoTranslation_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(176, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "自动创建转换色";
            // 
            // m_pictureBoxUserTranslation
            // 
            this.m_pictureBoxUserTranslation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pictureBoxUserTranslation.Location = new System.Drawing.Point(274, 124);
            this.m_pictureBoxUserTranslation.Name = "m_pictureBoxUserTranslation";
            this.m_pictureBoxUserTranslation.Size = new System.Drawing.Size(42, 21);
            this.m_pictureBoxUserTranslation.TabIndex = 15;
            this.m_pictureBoxUserTranslation.TabStop = false;
            this.m_pictureBoxUserTranslation.Click += new System.EventHandler(this.m_pictureBoxUserTranslation_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(176, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "用户创建转换色";
            // 
            // m_pictureBoxInvalidateTranslation
            // 
            this.m_pictureBoxInvalidateTranslation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pictureBoxInvalidateTranslation.Location = new System.Drawing.Point(274, 151);
            this.m_pictureBoxInvalidateTranslation.Name = "m_pictureBoxInvalidateTranslation";
            this.m_pictureBoxInvalidateTranslation.Size = new System.Drawing.Size(42, 21);
            this.m_pictureBoxInvalidateTranslation.TabIndex = 19;
            this.m_pictureBoxInvalidateTranslation.TabStop = false;
            this.m_pictureBoxInvalidateTranslation.Click += new System.EventHandler(this.m_pictureBoxInvalidateTranslation_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(176, 154);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "无效转换色";
            // 
            // m_pictureBoxInvalidateState
            // 
            this.m_pictureBoxInvalidateState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pictureBoxInvalidateState.Location = new System.Drawing.Point(115, 151);
            this.m_pictureBoxInvalidateState.Name = "m_pictureBoxInvalidateState";
            this.m_pictureBoxInvalidateState.Size = new System.Drawing.Size(42, 21);
            this.m_pictureBoxInvalidateState.TabIndex = 17;
            this.m_pictureBoxInvalidateState.TabStop = false;
            this.m_pictureBoxInvalidateState.Click += new System.EventHandler(this.m_pictureBoxInvalidateState_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 16;
            this.label9.Text = "无效状态色";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 184);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 12);
            this.label10.TabIndex = 20;
            this.label10.Text = "场景草图显示比率";
            // 
            // m_comboBoxSceneSketchSize
            // 
            this.m_comboBoxSceneSketchSize.FormattingEnabled = true;
            this.m_comboBoxSceneSketchSize.Items.AddRange(new object[] {
            "0.5",
            "1",
            "2",
            "3",
            "4"});
            this.m_comboBoxSceneSketchSize.Location = new System.Drawing.Point(135, 181);
            this.m_comboBoxSceneSketchSize.Name = "m_comboBoxSceneSketchSize";
            this.m_comboBoxSceneSketchSize.Size = new System.Drawing.Size(61, 20);
            this.m_comboBoxSceneSketchSize.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 222);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 12);
            this.label11.TabIndex = 22;
            this.label11.Text = "单点NPC显示尺寸";
            // 
            // m_textBoxSingleNpcSize
            // 
            this.m_textBoxSingleNpcSize.Location = new System.Drawing.Point(115, 218);
            this.m_textBoxSingleNpcSize.Name = "m_textBoxSingleNpcSize";
            this.m_textBoxSingleNpcSize.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxSingleNpcSize.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 248);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 12);
            this.label12.TabIndex = 24;
            this.label12.Text = "可接任务等级差";
            // 
            // m_textBoxLevelDifference
            // 
            this.m_textBoxLevelDifference.Location = new System.Drawing.Point(126, 245);
            this.m_textBoxLevelDifference.Name = "m_textBoxLevelDifference";
            this.m_textBoxLevelDifference.Size = new System.Drawing.Size(53, 21);
            this.m_textBoxLevelDifference.TabIndex = 25;
            this.m_textBoxLevelDifference.Text = "5";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(176, 222);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 12);
            this.label13.TabIndex = 26;
            this.label13.Text = "寻路范围默认值";
            // 
            // m_textBoxNpcMoveRangeSize
            // 
            this.m_textBoxNpcMoveRangeSize.Location = new System.Drawing.Point(274, 218);
            this.m_textBoxNpcMoveRangeSize.Name = "m_textBoxNpcMoveRangeSize";
            this.m_textBoxNpcMoveRangeSize.Size = new System.Drawing.Size(42, 21);
            this.m_textBoxNpcMoveRangeSize.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 274);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(113, 12);
            this.label14.TabIndex = 28;
            this.label14.Text = "多人模式服务器路径";
            // 
            // m_textBoxTeamWorkServerPath
            // 
            this.m_textBoxTeamWorkServerPath.Location = new System.Drawing.Point(20, 289);
            this.m_textBoxTeamWorkServerPath.Name = "m_textBoxTeamWorkServerPath";
            this.m_textBoxTeamWorkServerPath.Size = new System.Drawing.Size(330, 21);
            this.m_textBoxTeamWorkServerPath.TabIndex = 29;
            // 
            // OptionsForm
            // 
            this.AcceptButton = this.m_btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.m_btnCancel;
            this.ClientSize = new System.Drawing.Size(377, 357);
            this.ControlBox = false;
            this.Controls.Add(this.m_textBoxTeamWorkServerPath);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.m_textBoxNpcMoveRangeSize);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.m_textBoxLevelDifference);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.m_textBoxSingleNpcSize);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.m_comboBoxSceneSketchSize);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.m_pictureBoxInvalidateTranslation);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.m_pictureBoxInvalidateState);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.m_pictureBoxUserTranslation);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.m_pictureBoxAutoTranslation);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.m_pictureBoxUserState);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_pictureBoxAutoState);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_comboBoxItemDisplayMode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_comboBoxNPCDisplayMode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_comboBoxMissionDisplayMode);
            this.Controls.Add(this.m_btnCancel);
            this.Controls.Add(this.m_btnOK);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "选项";
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxAutoState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxUserState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxAutoTranslation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxUserTranslation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxInvalidateTranslation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxInvalidateState)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.Button m_btnCancel;
        private System.Windows.Forms.ComboBox m_comboBoxMissionDisplayMode;
        private System.Windows.Forms.Label label1;
        private MainForm m_MainForm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox m_comboBoxNPCDisplayMode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox m_comboBoxItemDisplayMode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox m_pictureBoxAutoState;
        private System.Windows.Forms.PictureBox m_pictureBoxUserState;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox m_pictureBoxAutoTranslation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox m_pictureBoxUserTranslation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox m_pictureBoxInvalidateTranslation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox m_pictureBoxInvalidateState;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox m_comboBoxSceneSketchSize;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox m_textBoxSingleNpcSize;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox m_textBoxLevelDifference;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox m_textBoxNpcMoveRangeSize;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox m_textBoxTeamWorkServerPath;

        
    }
}