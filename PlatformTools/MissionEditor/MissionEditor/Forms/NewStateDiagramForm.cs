﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class NewStateDiagramForm : Form
    {
        public NewStateDiagramForm()
        {
            InitializeComponent();
        }

        public String DiaName
        {
            get
            {
                return m_strDiaName;
            }
        }

        public int DiaID
        {
            get
            {
                return m_iDiaID;
            }
        }



        private void m_btnCreate_Click(object sender, EventArgs e)
        {

            // StateDiagram 名称为 类型前缀 + 目标ID号后5位或6位
            // StateDiagram ID 的规则：
            // NPC对话：1 + 5位NPCID
            // NPC死亡：2 + 5位NPCID
            // 怪物死亡：3 + 怪物ID后5位
            // 物品脚本：4 + ItemID后5位号
            // 区域脚本：5 + 6位区域ID
            // StateDiagram ID 与 字符串前缀ID一致

            if (m_comboBoxTarget.SelectedIndex < 0)
            {
                return;
            }

            try
            {
                int iTargetID = 0;

                StateDiagram.OwnerType ownerType = StateDiagram.OwnerType.DT_INVALID;
                String strNamePrefix = "";

                if (m_radioButtonNPC.Checked)
                {
                    ownerType = StateDiagram.OwnerType.DT_NPC;
                    strNamePrefix = OptionManager.NPCPrefix;
                    iTargetID = MainForm.ExtractNPCInfoIDFromDisplayText(m_comboBoxTarget.SelectedItem.ToString());
                }
                else if(m_radioButtonItem.Checked)
                {
                    strNamePrefix = OptionManager.ItemPrefix;
                    ownerType = StateDiagram.OwnerType.DT_ITEM;
                    iTargetID = MainForm.ExtractItemIDFromDisplayText(m_comboBoxTarget.SelectedItem.ToString());
                }
                else if(m_radioButtonArea.Checked)
                {
                    iTargetID = int.Parse(m_comboBoxTarget.SelectedItem.ToString());
                    strNamePrefix = OptionManager.AreaPrefix;
                    ownerType = StateDiagram.OwnerType.DT_AREA;
                }
                else if(m_radioButtonDeath.Checked)
                {
                    iTargetID = MainForm.ExtractNPCInfoIDFromDisplayText(m_comboBoxTarget.SelectedItem.ToString());
                    if (iTargetID.ToString()[0] == '1')
                    {
                        // 目标是 NPC
                        strNamePrefix = OptionManager.DeathPrefix;
                        ownerType = StateDiagram.OwnerType.DT_NPC_DEATH;
                    }
                    else if (iTargetID.ToString()[0] == '2')
                    {
                        // 目标是 Monster
                        strNamePrefix = OptionManager.DeathPrefix;
                        ownerType = StateDiagram.OwnerType.DT_MONSTER_DEATH;
                    }

                }


                m_iDiaID = DialogManager.GetDialogIDPrefix(ownerType, iTargetID);
                m_strDiaName = strNamePrefix + m_iDiaID.ToString();
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }


            this.Close();
        }

        private void m_radioButtonNPC_CheckedChanged(object sender, EventArgs e)
        {
            m_comboBoxTarget.Items.Clear();
            m_comboBoxTarget.Text = "";

            ArrayList npcList = NPCManager.Instance.NPCInfoList;
            foreach (NPCInfo npc in npcList)
            {
                m_comboBoxTarget.Items.Add(MainForm.GetDisplayText(npc.ID, npc.Name, OptionManager.NPCDisplayMode));
            }
        }

        private void m_radioButtonItem_CheckedChanged(object sender, EventArgs e)
        {
            m_comboBoxTarget.Items.Clear();
            m_comboBoxTarget.Text = "";

            ArrayList itemList = ItemManager.Instance.ItemList;
            foreach (Item item in itemList)
            {
                m_comboBoxTarget.Items.Add(MainForm.GetDisplayText(item.ID, item.Name, OptionManager.ItemDisplayMode));
            }
        }

        private void m_radioButtonArea_CheckedChanged(object sender, EventArgs e)
        {
            m_comboBoxTarget.Items.Clear();
            m_comboBoxTarget.Text = "";

            Hashtable regionTable = RegionManager.Instance.MapID2GroupList;
            foreach(ArrayList regionList in regionTable.Values)
            {
                foreach(RegionGroup region in regionList)
                {
                    // 只处理服务器区域
                    if(region.BigType == RegionGroup.RegionType.服务器相关区域)
                    {
                        m_comboBoxTarget.Items.Add(region.OutPutID);
                    }
                }
            }
        }

        private void m_radioButtonDeath_CheckedChanged(object sender, EventArgs e)
        {
            m_comboBoxTarget.Items.Clear();
            m_comboBoxTarget.Text = "";

            ArrayList npcList = NPCManager.Instance.NPCInfoList;
            foreach (NPCInfo npc in npcList)
            {
                m_comboBoxTarget.Items.Add(MainForm.GetDisplayText(npc.ID, npc.Name, OptionManager.NPCDisplayMode));
            }
        }
    }
}