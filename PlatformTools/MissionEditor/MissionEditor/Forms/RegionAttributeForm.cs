﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class RegionAttributeForm : Form
    {
        public static RegionAttributeForm Instance
        {
            get
            {
                if (m_Instance == null || m_Instance.IsDisposed)
                {
                    m_Instance = new RegionAttributeForm();
                }

                return m_Instance;
            }
        }

        private RegionAttributeForm()
        {
            InitializeComponent();

            // 初始化区域类型选择下拉框
            m_comboBoxRegionType.Items.Add("服务器相关区域");
            m_comboBoxRegionType.Items.Add("客户端表现区域");
            m_comboBoxRegionType.SelectedIndex = m_iRegionType;

            // 改变小类型的显示方式
            if (m_iRegionType == 0)
            {
                m_groupBoxServerGroup.Enabled = true;
                m_groupBoxClientGroup.Enabled = false;
            }

            if (m_iRegionType == 1)
            {
                m_groupBoxServerGroup.Enabled = false;
                m_groupBoxClientGroup.Enabled = true;
            }

            // 设置小类型的默认选项
            m_radioButtonTask.Checked = true;
            m_radioButtonMusic.Checked = true;
        }

        private void m_buttonOK_Click(object sender, EventArgs e)
        {
            // 获取当前的MapID
            int iMapID = MainForm.Instance.GetCurrentMapID();
            
            // 获取小ID
            int iSmallType = 0;
            if (m_iRegionType == 0)
            {
                if(m_radioButtonTask.Checked)
                {
                    iSmallType = 0;
                }

                if(m_radioButtonSafe.Checked)
                {
                    iSmallType = 1;
                }

                if(m_radioButtonNoPK.Checked)
                {
                    iSmallType = 2;
                }

                if(m_radioButtonNoRide.Checked)
                {
                    iSmallType = 3;
                }

                if(m_radioButtonPrePath.Checked)
                {
                    iSmallType = 4;
                }

                if(m_radioButtonBlockQuad.Checked)
                {
                    iSmallType = 5;
                }
            }

            if (m_iRegionType == 1)
            {
                if(m_radioButtonMusic.Checked)
                {
                    iSmallType = 1;
                }
                
                if(m_radioButtonEffect.Checked)
                {
                    iSmallType = 2;
                }
            }

            // 添加区域
            if(iMapID != -1)
            {
                RegionManager.Instance.AddRegionGroup(iMapID, m_iRegionType, iSmallType);
            }
            this.Close();
        }

        private void m_buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void m_comboBoxRegionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 设置区域变量属性
            m_iRegionType = m_comboBoxRegionType.SelectedIndex;

            // 改变小类型的显示方式
            if(m_iRegionType == 0)
            {
                m_groupBoxServerGroup.Enabled = true;
                m_groupBoxClientGroup.Enabled = false;
            }

            if(m_iRegionType == 1)
            {
                m_groupBoxServerGroup.Enabled = false;
                m_groupBoxClientGroup.Enabled = true;
            }
        }

    }
}