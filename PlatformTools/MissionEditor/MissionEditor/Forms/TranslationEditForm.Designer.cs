﻿namespace MissionEditor
{
    partial class TranslationEditForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_labelFromState = new System.Windows.Forms.Label();
            this.m_labelToState = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_btnRemove = new System.Windows.Forms.Button();
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_listBoxValue = new System.Windows.Forms.ListBox();
            this.m_listBoxSelectedConditions = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_textBoxValue = new System.Windows.Forms.TextBox();
            this.m_listBoxAllConditions = new System.Windows.Forms.ListBox();
            this.m_comboBoxFilter = new System.Windows.Forms.ComboBox();
            this.m_checkBoxCurOption = new System.Windows.Forms.CheckBox();
            this.m_textBoxCurOption = new System.Windows.Forms.TextBox();
            this.m_textBoxConditionParam = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.m_textBoxConditionDesc = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "从状态：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "到状态：";
            // 
            // m_labelFromState
            // 
            this.m_labelFromState.AutoSize = true;
            this.m_labelFromState.Location = new System.Drawing.Point(71, 9);
            this.m_labelFromState.Name = "m_labelFromState";
            this.m_labelFromState.Size = new System.Drawing.Size(35, 12);
            this.m_labelFromState.TabIndex = 2;
            this.m_labelFromState.Text = "State";
            // 
            // m_labelToState
            // 
            this.m_labelToState.AutoSize = true;
            this.m_labelToState.Location = new System.Drawing.Point(71, 30);
            this.m_labelToState.Name = "m_labelToState";
            this.m_labelToState.Size = new System.Drawing.Size(35, 12);
            this.m_labelToState.TabIndex = 3;
            this.m_labelToState.Text = "State";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(183, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "转换条件：";
            // 
            // m_btnOK
            // 
            this.m_btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.m_btnOK.Location = new System.Drawing.Point(185, 294);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(84, 22);
            this.m_btnOK.TabIndex = 6;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_btnRemove
            // 
            this.m_btnRemove.Location = new System.Drawing.Point(150, 151);
            this.m_btnRemove.Name = "m_btnRemove";
            this.m_btnRemove.Size = new System.Drawing.Size(26, 21);
            this.m_btnRemove.TabIndex = 18;
            this.m_btnRemove.Text = "<-";
            this.m_btnRemove.UseVisualStyleBackColor = true;
            this.m_btnRemove.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Location = new System.Drawing.Point(150, 124);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(26, 21);
            this.m_btnAdd.TabIndex = 17;
            this.m_btnAdd.Text = "->";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_listBoxValue
            // 
            this.m_listBoxValue.FormattingEnabled = true;
            this.m_listBoxValue.ItemHeight = 12;
            this.m_listBoxValue.Location = new System.Drawing.Point(324, 86);
            this.m_listBoxValue.Name = "m_listBoxValue";
            this.m_listBoxValue.Size = new System.Drawing.Size(95, 172);
            this.m_listBoxValue.TabIndex = 16;
            this.m_listBoxValue.DoubleClick += new System.EventHandler(this.m_listBoxValue_DoubleClick);
            // 
            // m_listBoxSelectedConditions
            // 
            this.m_listBoxSelectedConditions.FormattingEnabled = true;
            this.m_listBoxSelectedConditions.ItemHeight = 12;
            this.m_listBoxSelectedConditions.Location = new System.Drawing.Point(185, 86);
            this.m_listBoxSelectedConditions.Name = "m_listBoxSelectedConditions";
            this.m_listBoxSelectedConditions.Size = new System.Drawing.Size(133, 172);
            this.m_listBoxSelectedConditions.TabIndex = 15;
            this.m_listBoxSelectedConditions.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSelectedConditions_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 14;
            this.label4.Text = "对应值";
            // 
            // m_textBoxValue
            // 
            this.m_textBoxValue.Location = new System.Drawing.Point(64, 264);
            this.m_textBoxValue.Name = "m_textBoxValue";
            this.m_textBoxValue.Size = new System.Drawing.Size(78, 21);
            this.m_textBoxValue.TabIndex = 13;
            this.m_textBoxValue.Text = "0";
            // 
            // m_listBoxAllConditions
            // 
            this.m_listBoxAllConditions.FormattingEnabled = true;
            this.m_listBoxAllConditions.ItemHeight = 12;
            this.m_listBoxAllConditions.Location = new System.Drawing.Point(14, 86);
            this.m_listBoxAllConditions.Name = "m_listBoxAllConditions";
            this.m_listBoxAllConditions.Size = new System.Drawing.Size(129, 172);
            this.m_listBoxAllConditions.TabIndex = 12;
            this.m_listBoxAllConditions.SelectedIndexChanged += new System.EventHandler(this.m_listBoxAllConditions_SelectedIndexChanged);
            // 
            // m_comboBoxFilter
            // 
            this.m_comboBoxFilter.FormattingEnabled = true;
            this.m_comboBoxFilter.Location = new System.Drawing.Point(13, 57);
            this.m_comboBoxFilter.Name = "m_comboBoxFilter";
            this.m_comboBoxFilter.Size = new System.Drawing.Size(131, 20);
            this.m_comboBoxFilter.TabIndex = 11;
            this.m_comboBoxFilter.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxFilter_SelectedIndexChanged);
            // 
            // m_checkBoxCurOption
            // 
            this.m_checkBoxCurOption.AutoSize = true;
            this.m_checkBoxCurOption.Location = new System.Drawing.Point(18, 300);
            this.m_checkBoxCurOption.Name = "m_checkBoxCurOption";
            this.m_checkBoxCurOption.Size = new System.Drawing.Size(108, 16);
            this.m_checkBoxCurOption.TabIndex = 21;
            this.m_checkBoxCurOption.Text = "检查 curOption";
            this.m_checkBoxCurOption.UseVisualStyleBackColor = true;
            this.m_checkBoxCurOption.CheckedChanged += new System.EventHandler(this.m_checkBoxCurOption_CheckedChanged);
            // 
            // m_textBoxCurOption
            // 
            this.m_textBoxCurOption.Location = new System.Drawing.Point(132, 298);
            this.m_textBoxCurOption.Name = "m_textBoxCurOption";
            this.m_textBoxCurOption.Size = new System.Drawing.Size(36, 21);
            this.m_textBoxCurOption.TabIndex = 22;
            // 
            // m_textBoxConditionParam
            // 
            this.m_textBoxConditionParam.Location = new System.Drawing.Point(79, 357);
            this.m_textBoxConditionParam.Multiline = true;
            this.m_textBoxConditionParam.Name = "m_textBoxConditionParam";
            this.m_textBoxConditionParam.ReadOnly = true;
            this.m_textBoxConditionParam.Size = new System.Drawing.Size(310, 105);
            this.m_textBoxConditionParam.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 361);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 25;
            this.label5.Text = "参数说明:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 334);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 24;
            this.label6.Text = "功能描述:";
            // 
            // m_textBoxConditionDesc
            // 
            this.m_textBoxConditionDesc.Location = new System.Drawing.Point(79, 330);
            this.m_textBoxConditionDesc.Name = "m_textBoxConditionDesc";
            this.m_textBoxConditionDesc.ReadOnly = true;
            this.m_textBoxConditionDesc.Size = new System.Drawing.Size(310, 21);
            this.m_textBoxConditionDesc.TabIndex = 23;
            // 
            // TranslationEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 471);
            this.Controls.Add(this.m_textBoxConditionParam);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.m_textBoxConditionDesc);
            this.Controls.Add(this.m_textBoxCurOption);
            this.Controls.Add(this.m_checkBoxCurOption);
            this.Controls.Add(this.m_btnRemove);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.m_listBoxValue);
            this.Controls.Add(this.m_listBoxSelectedConditions);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_textBoxValue);
            this.Controls.Add(this.m_listBoxAllConditions);
            this.Controls.Add(this.m_comboBoxFilter);
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_labelToState);
            this.Controls.Add(this.m_labelFromState);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TranslationEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "状态转换定义";
            this.Load += new System.EventHandler(this.TranslationEditForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label m_labelFromState;
        private System.Windows.Forms.Label m_labelToState;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button m_btnOK;
        private TranslationControl m_Ctrl;
        private System.Windows.Forms.Button m_btnRemove;
        private System.Windows.Forms.Button m_btnAdd;
        private System.Windows.Forms.ListBox m_listBoxValue;
        private System.Windows.Forms.ListBox m_listBoxSelectedConditions;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox m_textBoxValue;
        private System.Windows.Forms.ListBox m_listBoxAllConditions;
        private System.Windows.Forms.ComboBox m_comboBoxFilter;
        private System.Windows.Forms.CheckBox m_checkBoxCurOption;
        private System.Windows.Forms.TextBox m_textBoxCurOption;
        private System.Windows.Forms.TextBox m_textBoxConditionParam;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox m_textBoxConditionDesc;
    }
}