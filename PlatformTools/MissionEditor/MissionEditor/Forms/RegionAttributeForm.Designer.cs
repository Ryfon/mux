﻿namespace MissionEditor
{
    partial class RegionAttributeForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.m_comboBoxRegionType = new System.Windows.Forms.ComboBox();
            this.m_radioButtonTask = new System.Windows.Forms.RadioButton();
            this.m_radioButtonSafe = new System.Windows.Forms.RadioButton();
            this.m_groupBoxServerGroup = new System.Windows.Forms.GroupBox();
            this.m_radioButtonPrePath = new System.Windows.Forms.RadioButton();
            this.m_radioButtonNoRide = new System.Windows.Forms.RadioButton();
            this.m_radioButtonNoPK = new System.Windows.Forms.RadioButton();
            this.m_groupBoxClientGroup = new System.Windows.Forms.GroupBox();
            this.m_radioButtonEffect = new System.Windows.Forms.RadioButton();
            this.m_radioButtonMusic = new System.Windows.Forms.RadioButton();
            this.m_buttonOK = new System.Windows.Forms.Button();
            this.m_buttonCancel = new System.Windows.Forms.Button();
            this.m_radioButtonBlockQuad = new System.Windows.Forms.RadioButton();
            this.m_groupBoxServerGroup.SuspendLayout();
            this.m_groupBoxClientGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "区域类型";
            // 
            // m_comboBoxRegionType
            // 
            this.m_comboBoxRegionType.FormattingEnabled = true;
            this.m_comboBoxRegionType.Location = new System.Drawing.Point(75, 10);
            this.m_comboBoxRegionType.Name = "m_comboBoxRegionType";
            this.m_comboBoxRegionType.Size = new System.Drawing.Size(121, 20);
            this.m_comboBoxRegionType.TabIndex = 1;
            this.m_comboBoxRegionType.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxRegionType_SelectedIndexChanged);
            // 
            // m_radioButtonTask
            // 
            this.m_radioButtonTask.AutoSize = true;
            this.m_radioButtonTask.Location = new System.Drawing.Point(10, 20);
            this.m_radioButtonTask.Name = "m_radioButtonTask";
            this.m_radioButtonTask.Size = new System.Drawing.Size(95, 16);
            this.m_radioButtonTask.TabIndex = 2;
            this.m_radioButtonTask.TabStop = true;
            this.m_radioButtonTask.Text = "任务触发区域";
            this.m_radioButtonTask.UseVisualStyleBackColor = true;
            // 
            // m_radioButtonSafe
            // 
            this.m_radioButtonSafe.AutoSize = true;
            this.m_radioButtonSafe.Location = new System.Drawing.Point(10, 45);
            this.m_radioButtonSafe.Name = "m_radioButtonSafe";
            this.m_radioButtonSafe.Size = new System.Drawing.Size(59, 16);
            this.m_radioButtonSafe.TabIndex = 3;
            this.m_radioButtonSafe.TabStop = true;
            this.m_radioButtonSafe.Text = "安全区";
            this.m_radioButtonSafe.UseVisualStyleBackColor = true;
            // 
            // m_groupBoxServerGroup
            // 
            this.m_groupBoxServerGroup.Controls.Add(this.m_radioButtonBlockQuad);
            this.m_groupBoxServerGroup.Controls.Add(this.m_radioButtonPrePath);
            this.m_groupBoxServerGroup.Controls.Add(this.m_radioButtonNoRide);
            this.m_groupBoxServerGroup.Controls.Add(this.m_radioButtonNoPK);
            this.m_groupBoxServerGroup.Controls.Add(this.m_radioButtonSafe);
            this.m_groupBoxServerGroup.Controls.Add(this.m_radioButtonTask);
            this.m_groupBoxServerGroup.Location = new System.Drawing.Point(15, 35);
            this.m_groupBoxServerGroup.Name = "m_groupBoxServerGroup";
            this.m_groupBoxServerGroup.Size = new System.Drawing.Size(270, 170);
            this.m_groupBoxServerGroup.TabIndex = 4;
            this.m_groupBoxServerGroup.TabStop = false;
            this.m_groupBoxServerGroup.Text = "服务器";
            // 
            // m_radioButtonPrePath
            // 
            this.m_radioButtonPrePath.AutoSize = true;
            this.m_radioButtonPrePath.Location = new System.Drawing.Point(10, 120);
            this.m_radioButtonPrePath.Name = "m_radioButtonPrePath";
            this.m_radioButtonPrePath.Size = new System.Drawing.Size(83, 16);
            this.m_radioButtonPrePath.TabIndex = 6;
            this.m_radioButtonPrePath.TabStop = true;
            this.m_radioButtonPrePath.Text = "寻路预判区";
            this.m_radioButtonPrePath.UseVisualStyleBackColor = true;
            // 
            // m_radioButtonNoRide
            // 
            this.m_radioButtonNoRide.AutoSize = true;
            this.m_radioButtonNoRide.Location = new System.Drawing.Point(10, 95);
            this.m_radioButtonNoRide.Name = "m_radioButtonNoRide";
            this.m_radioButtonNoRide.Size = new System.Drawing.Size(83, 16);
            this.m_radioButtonNoRide.TabIndex = 5;
            this.m_radioButtonNoRide.TabStop = true;
            this.m_radioButtonNoRide.Text = "不可骑乘区";
            this.m_radioButtonNoRide.UseVisualStyleBackColor = true;
            // 
            // m_radioButtonNoPK
            // 
            this.m_radioButtonNoPK.AutoSize = true;
            this.m_radioButtonNoPK.Location = new System.Drawing.Point(10, 70);
            this.m_radioButtonNoPK.Name = "m_radioButtonNoPK";
            this.m_radioButtonNoPK.Size = new System.Drawing.Size(71, 16);
            this.m_radioButtonNoPK.TabIndex = 4;
            this.m_radioButtonNoPK.TabStop = true;
            this.m_radioButtonNoPK.Text = "不可PK区";
            this.m_radioButtonNoPK.UseVisualStyleBackColor = true;
            // 
            // m_groupBoxClientGroup
            // 
            this.m_groupBoxClientGroup.Controls.Add(this.m_radioButtonEffect);
            this.m_groupBoxClientGroup.Controls.Add(this.m_radioButtonMusic);
            this.m_groupBoxClientGroup.Location = new System.Drawing.Point(15, 210);
            this.m_groupBoxClientGroup.Name = "m_groupBoxClientGroup";
            this.m_groupBoxClientGroup.Size = new System.Drawing.Size(270, 75);
            this.m_groupBoxClientGroup.TabIndex = 5;
            this.m_groupBoxClientGroup.TabStop = false;
            this.m_groupBoxClientGroup.Text = "客户端";
            // 
            // m_radioButtonEffect
            // 
            this.m_radioButtonEffect.AutoSize = true;
            this.m_radioButtonEffect.Location = new System.Drawing.Point(10, 45);
            this.m_radioButtonEffect.Name = "m_radioButtonEffect";
            this.m_radioButtonEffect.Size = new System.Drawing.Size(131, 16);
            this.m_radioButtonEffect.TabIndex = 1;
            this.m_radioButtonEffect.TabStop = true;
            this.m_radioButtonEffect.Text = "音效、特效触发区域";
            this.m_radioButtonEffect.UseVisualStyleBackColor = true;
            // 
            // m_radioButtonMusic
            // 
            this.m_radioButtonMusic.AutoSize = true;
            this.m_radioButtonMusic.Location = new System.Drawing.Point(10, 20);
            this.m_radioButtonMusic.Name = "m_radioButtonMusic";
            this.m_radioButtonMusic.Size = new System.Drawing.Size(95, 16);
            this.m_radioButtonMusic.TabIndex = 0;
            this.m_radioButtonMusic.TabStop = true;
            this.m_radioButtonMusic.Text = "音乐触发区域";
            this.m_radioButtonMusic.UseVisualStyleBackColor = true;
            // 
            // m_buttonOK
            // 
            this.m_buttonOK.Location = new System.Drawing.Point(130, 295);
            this.m_buttonOK.Name = "m_buttonOK";
            this.m_buttonOK.Size = new System.Drawing.Size(75, 23);
            this.m_buttonOK.TabIndex = 6;
            this.m_buttonOK.Text = "确定";
            this.m_buttonOK.UseVisualStyleBackColor = true;
            this.m_buttonOK.Click += new System.EventHandler(this.m_buttonOK_Click);
            // 
            // m_buttonCancel
            // 
            this.m_buttonCancel.Location = new System.Drawing.Point(210, 295);
            this.m_buttonCancel.Name = "m_buttonCancel";
            this.m_buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.m_buttonCancel.TabIndex = 7;
            this.m_buttonCancel.Text = "取消";
            this.m_buttonCancel.UseVisualStyleBackColor = true;
            this.m_buttonCancel.Click += new System.EventHandler(this.m_buttonCancel_Click);
            // 
            // m_radioButtonBlockQuad
            // 
            this.m_radioButtonBlockQuad.AutoSize = true;
            this.m_radioButtonBlockQuad.Location = new System.Drawing.Point(10, 145);
            this.m_radioButtonBlockQuad.Name = "m_radioButtonBlockQuad";
            this.m_radioButtonBlockQuad.Size = new System.Drawing.Size(89, 16);
            this.m_radioButtonBlockQuad.TabIndex = 7;
            this.m_radioButtonBlockQuad.TabStop = true;
            this.m_radioButtonBlockQuad.Text = "BlockQuad区";
            this.m_radioButtonBlockQuad.UseVisualStyleBackColor = true;
            // 
            // RegionAttributeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 326);
            this.Controls.Add(this.m_buttonCancel);
            this.Controls.Add(this.m_buttonOK);
            this.Controls.Add(this.m_groupBoxClientGroup);
            this.Controls.Add(this.m_groupBoxServerGroup);
            this.Controls.Add(this.m_comboBoxRegionType);
            this.Controls.Add(this.label1);
            this.Name = "RegionAttributeForm";
            this.Text = "RegionAttributeForm";
            this.m_groupBoxServerGroup.ResumeLayout(false);
            this.m_groupBoxServerGroup.PerformLayout();
            this.m_groupBoxClientGroup.ResumeLayout(false);
            this.m_groupBoxClientGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private static RegionAttributeForm m_Instance = null;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox m_comboBoxRegionType;
        private System.Windows.Forms.RadioButton m_radioButtonTask;
        private System.Windows.Forms.RadioButton m_radioButtonSafe;
        private System.Windows.Forms.GroupBox m_groupBoxServerGroup;
        private System.Windows.Forms.GroupBox m_groupBoxClientGroup;
        private System.Windows.Forms.RadioButton m_radioButtonMusic;
        private System.Windows.Forms.RadioButton m_radioButtonEffect;
        private System.Windows.Forms.Button m_buttonOK;
        private System.Windows.Forms.Button m_buttonCancel;

        // 属性
        private int m_iRegionType = 0;
        private System.Windows.Forms.RadioButton m_radioButtonNoRide;
        private System.Windows.Forms.RadioButton m_radioButtonNoPK;
        private System.Windows.Forms.RadioButton m_radioButtonPrePath;
        private System.Windows.Forms.RadioButton m_radioButtonBlockQuad;
    }
}