﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace MissionEditor
{
    public partial class RegionToolForm : Form
    {
        private RegionToolForm()
        {
            InitializeComponent();
            m_comboBoxShowType.Items.Add("全部区域");
            m_comboBoxShowType.Items.Add("可见的区域");
            m_comboBoxShowType.Items.Add("隐藏的区域");
            m_comboBoxShowType.SelectedIndex = 0;
            m_checkBoxShowGrid.Checked = MainForm.Instance.m_bShowGrid;
            m_checkBoxShowNPC.Checked = MainForm.Instance.m_bShowNPC;
            m_checkBoxShowRegion.Checked = MainForm.Instance.m_bShowRegion;
            m_checkBoxShowPtSound.Checked = MainForm.Instance.m_bShowPtSound;
            
            foreach (LayerInfo info in LayerManager.Instance.LayerList)
            {
                m_comboBoxActiveLayer.Items.Add(info.LayerType.ToString());
            }
            m_comboBoxActiveLayer.SelectedIndex = 9;

            int iGridSize = MainForm.Instance.m_iGridSize;
            //m_textBoxGridSize.Text = iGridSize.ToString();

            RefreshTree();
        }
        //--------------------------------------------------------------------------------------------------------//
        public int ActiveLayer
        {
            get
            {
                return m_comboBoxActiveLayer.SelectedIndex;
            }
        }

        public bool bDirtyTree
        {
            get
            {
                return m_bDirtyTree;
            }
            set
            {
                m_bDirtyTree = value;
            }
        }
        public static RegionToolForm Instance
        {
            get
            {
                if (m_Instance == null || m_Instance.IsDisposed)
                {
                    m_Instance = new RegionToolForm();
                }

                return m_Instance;
            }
        }

        //--------------------------------------------------------------------------------------------------------//
        public void RefreshTree()
        {
            // 保存旧的选择
            object oldSelectNode = null;
            if(m_treeViewRegionList.SelectedNode != null)
            {
                oldSelectNode = m_treeViewRegionList.SelectedNode.Tag;
            }
            
            //
            m_treeViewRegionList.Nodes.Clear();

            TreeNode noNode = new TreeNode("未分配的区域");
            noNode.Tag = 0;
            m_treeViewRegionList.Nodes.Add(noNode);


            int iMapID = MainForm.Instance.GetCurrentMapID();
            if(iMapID != -1)
            {
                ArrayList grouplist = RegionManager.Instance.MapID2GroupList[iMapID] as ArrayList;
                if(grouplist != null)
                {
                    foreach(RegionGroup group in grouplist)
                    {
                        TreeNode node = new TreeNode(group.ID.ToString());
                        node.Tag = group.ID.ToString();
                        node.Text = group.Name + "(" + group.OutPutID.ToString() + ")";
                        m_treeViewRegionList.Nodes.Add(node);
                    }
                }

                ArrayList regionList = RegionManager.Instance.MapID2RegionList[iMapID] as ArrayList;
                if(regionList != null)
                {
                    foreach(RegionInfo region in regionList)
                    {
                        TreeNode regionNode = new TreeNode(region.ID.ToString());
                        regionNode.Tag = region.ID.ToString();
                        regionNode.Text = region.Name;
                        bool bFindGroup = false;

                        foreach(TreeNode node in m_treeViewRegionList.Nodes)
                        {
                            if(region.GroupID.ToString().CompareTo(node.Tag.ToString()) == 0)
                            {
                                node.Nodes.Add(regionNode);
                                bFindGroup = true;
                            }
                        }

                        if(!bFindGroup)
                        {
                            m_treeViewRegionList.Nodes[0].Nodes.Add(regionNode);
                        }

                        // 根据显示情况筛选
                        int iType = m_comboBoxShowType.SelectedIndex;

                        if (iType == 0)
                        {
                            if(region.Hidden)
                            {
                                regionNode.Text += "(隐藏)";
                            }
                        }
                        if (iType == 1)// 可见的区域
                        {
                            if(region.Hidden)
                            {
                                regionNode.Remove();
                            }
                        }
                        if (iType == 2)// 隐藏的区域
                        {
                            if (!region.Hidden)
                            {
                                regionNode.Remove();
                            }
                        }
                    }
                }
            }

            m_treeViewRegionList.CollapseAll();
            ArrayList removeList = new ArrayList();
            foreach(TreeNode node in m_treeViewRegionList.Nodes)
            {
                // 没有子节点的node
                if(node.Nodes.Count == 0)
                {
                    if(m_comboBoxShowType.SelectedIndex != 0)
                    {
                        removeList.Add(node);
                    }
                    continue;
                }

                if(node.Tag.Equals(oldSelectNode))
                {
                    m_treeViewRegionList.SelectedNode = node;
                }
                else
                {
                    foreach(TreeNode childNode in node.Nodes)
                    {
                        if(childNode.Tag.Equals(oldSelectNode))
                        {
                            m_treeViewRegionList.SelectedNode = childNode;
                        }
                    }
                }
            }

            // 删除多余的node
            foreach(TreeNode node in removeList)
            {
                node.Remove();
            }

            if(m_treeViewRegionList.SelectedNode != null)
            {
                m_treeViewRegionList.SelectedNode.ExpandAll();
            }

            m_treeViewRegionList.Focus();
            m_treeViewRegionList.Invalidate();
            m_treeViewRegionList.Update();
        }

        private void m_buttonAddGroup_Click(object sender, EventArgs e)
        {
            int iMapID = MainForm.Instance.GetCurrentMapID();
            if(iMapID != -1)
            {
                RegionAttributeForm form = RegionAttributeForm.Instance;
                form.Show();
                m_bDirtyTree = true;
                RefreshTree();
            }
        }

        private void m_buttonDelGroup_Click(object sender, EventArgs e)
        {
            if (m_treeViewRegionList.SelectedNode != null)
            {
                int GroupID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
                RegionGroup delGroup = RegionManager.Instance.GetGroupByID(GroupID);
                if(delGroup != null)
                {
                    DialogResult result = MessageBox.Show("确认删除---" + delGroup.Name + "？该操作将删除组内所有的区域！", "提示", MessageBoxButtons.YesNo);
                    if(result == DialogResult.Yes)
                    {
                        RegionManager.Instance.DeleteGroup(GroupID);
                        RefreshTree();
                    }
                }
            }
        }

        private void RegionToolForm_MouseDown(object sender, MouseEventArgs e)
        {
            RefreshTree();
        }

        private void m_treeViewRegionList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            int iRegionID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionInfo region = RegionManager.Instance.GetRegionByID(iRegionID);
            if(region != null)
            {
                MainForm.Instance.MainPropertyGrid.SelectedObject = region;
                MainForm.Instance.SetSelectRegionByID(iRegionID);
            }
            else
            {
                RegionGroup group = RegionManager.Instance.GetGroupByID(iRegionID);
                MainForm.Instance.SetSelectRegionByID(iRegionID);
                MainForm.Instance.MainPropertyGrid.SelectedObject = group;
            }
        }

        private void m_buttonShow_Click(object sender, EventArgs e)
        {
            if (m_treeViewRegionList.SelectedNode != null)
            {
                int iID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
                RegionManager.Instance.HideRegion(iID, false);
                RegionManager.Instance.HideGroup(iID, false);
                RefreshTree();
            }
        }

        private void m_buttonHide_Click(object sender, EventArgs e)
        {
            if (m_treeViewRegionList.SelectedNode != null)
            {
                int iID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
                RegionManager.Instance.HideRegion(iID, true);
                RegionManager.Instance.HideGroup(iID, true);
                RefreshTree();
            }
        }

        private void m_comboBoxShowType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iType = m_comboBoxShowType.SelectedIndex;

            if(iType == 0)
            {
                m_buttonHide.Enabled = true;
                m_buttonShow.Enabled = true;
            }
            if (iType == 1)
            {
                m_buttonHide.Enabled = true;
                m_buttonShow.Enabled = false;
            }
            if (iType == 2)
            {
                m_buttonHide.Enabled = false;
                m_buttonShow.Enabled = true;
            }

            RefreshTree();
        }

        private void m_treeViewRegionList_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                TreeNode node = e.Item as TreeNode;
                int iID = int.Parse(node.Tag.ToString());
                if (RegionManager.Instance.GetRegionByID(iID) != null)
                {
                    DoDragDrop(e.Item, DragDropEffects.Move);
                }
            }
        }

        private void m_treeViewRegionList_DragEnter(object sender, DragEventArgs e)
        {
            if(e.Data.GetDataPresent("System.Windows.Forms.TreeNode"))
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }

            Point pt;
            TreeNode enterNode;
            pt = ((TreeView)(sender)).PointToClient(new Point(e.X, e.Y));
            enterNode = this.m_treeViewRegionList.GetNodeAt(pt);
            if(enterNode != null)
            {
                enterNode.BackColor = Color.FromName("HighLight");
                enterNode.ForeColor = Color.White;
                oldenterNode = enterNode;
            }
        }

        private void m_treeViewRegionList_DragDrop(object sender, DragEventArgs e)
        {
            TreeNode moveNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

            Point pt;
            TreeNode targetNode;
            pt = ((TreeView)(sender)).PointToClient(new Point(e.X, e.Y));
            targetNode = this.m_treeViewRegionList.GetNodeAt(pt);

            TreeNode NewMoveNode = (TreeNode)moveNode.Clone();

            int iRegionID = int.Parse(NewMoveNode.Tag.ToString());
            int iGroupID = int.Parse(targetNode.Tag.ToString());
            RegionInfo info = RegionManager.Instance.GetRegionByID(iRegionID);
            RegionGroup group = RegionManager.Instance.GetGroupByID(iGroupID);
            if(info != null)
            {
                if(group != null || iGroupID == 0)
                {
                    moveNode.Remove();
                    targetNode.Nodes.Insert(targetNode.Nodes.Count, NewMoveNode);
                    info.GroupID = iGroupID;
                    m_treeViewRegionList.SelectedNode = targetNode;
                    targetNode.Expand();
                }
            }

            NewMoveNode.BackColor = Color.White;
            NewMoveNode.ForeColor = Color.Black;
            targetNode.BackColor = Color.White;
            targetNode.ForeColor = Color.Black;
        }

        private void m_treeViewRegionList_DragOver(object sender, DragEventArgs e)
        {
            Point pt;
            TreeNode enterNode;
            pt = ((TreeView)(sender)).PointToClient(new Point(e.X, e.Y));
            enterNode = this.m_treeViewRegionList.GetNodeAt(pt);
            m_treeViewRegionList.SelectedNode = enterNode;
            
            if(enterNode != null && oldenterNode != null)
            {
                oldenterNode.BackColor = Color.White;
                oldenterNode.ForeColor = Color.Black;

                enterNode.BackColor = Color.FromName("HighLight");
                enterNode.ForeColor = Color.White;
                oldenterNode = enterNode;
            }
        }

        private void m_checkBoxShowGrid_CheckedChanged(object sender, EventArgs e)
        {
            MainForm.Instance.m_bShowGrid = m_checkBoxShowGrid.Checked;
            MainForm.Instance.RefreshSceneSketch(MainForm.Instance.GetCurrentMapID());
        }

        private void m_textBoxGridSize_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int size = int.Parse(m_textBoxGridSize.Text);
                MainForm.Instance.m_iGridSize = size;
                MainForm.Instance.RefreshSceneSketch(MainForm.Instance.GetCurrentMapID());
            }
            catch(Exception)
            {
                
            }
        }

        private void m_checkBoxShowNPC_CheckedChanged(object sender, EventArgs e)
        {
            m_checkBoxSingleNPC.Enabled = m_checkBoxShowNPC.Checked;
            m_checkBoxAreaNPC.Enabled = m_checkBoxShowNPC.Checked;

            MainForm.Instance.m_bShowNPC = m_checkBoxShowNPC.Checked;
            LayerManager.Instance.SetLayerDirty(LayType.单点NPC);
            LayerManager.Instance.SetLayerVisiable(LayType.单点NPC, m_checkBoxShowNPC.Checked);
            LayerManager.Instance.SetLayerDirty(LayType.区域NPC);
            LayerManager.Instance.SetLayerVisiable(LayType.区域NPC, m_checkBoxShowNPC.Checked);
            MainForm.Instance.RefreshSceneSketch(MainForm.Instance.GetCurrentMapID());
        }

        private void m_checkBoxShowRegion_CheckedChanged(object sender, EventArgs e)
        {
            m_checkBoxMusicRegion.Enabled = m_checkBoxShowRegion.Checked;
            m_checkBoxSoundRegion.Enabled = m_checkBoxShowRegion.Checked;
            m_checkBoxTaskRegion.Enabled = m_checkBoxShowRegion.Checked;
            m_checkBoxOtherRegion.Enabled = m_checkBoxShowRegion.Checked;
            m_checkBoxPrePathRegion.Enabled = m_checkBoxShowRegion.Checked;
            m_checkBoxBlockQuadRegion.Enabled = m_checkBoxShowRegion.Checked;

            MainForm.Instance.m_bShowRegion = m_checkBoxShowRegion.Checked;
            LayerManager.Instance.SetLayerDirty(LayType.音乐区);
            LayerManager.Instance.SetLayerVisiable(LayType.音乐区, m_checkBoxShowRegion.Checked); 

            LayerManager.Instance.SetLayerDirty(LayType.音效特效区);
            LayerManager.Instance.SetLayerVisiable(LayType.音效特效区, m_checkBoxShowRegion.Checked); 

            LayerManager.Instance.SetLayerDirty(LayType.任务触发区);
            LayerManager.Instance.SetLayerVisiable(LayType.任务触发区, m_checkBoxShowRegion.Checked); 

            LayerManager.Instance.SetLayerDirty(LayType.安全区);
            LayerManager.Instance.SetLayerVisiable(LayType.安全区, m_checkBoxShowRegion.Checked); 

            LayerManager.Instance.SetLayerDirty(LayType.不可PK区);
            LayerManager.Instance.SetLayerVisiable(LayType.不可PK区, m_checkBoxShowRegion.Checked); 

            LayerManager.Instance.SetLayerDirty(LayType.不可骑乘区);
            LayerManager.Instance.SetLayerVisiable(LayType.不可骑乘区, m_checkBoxShowRegion.Checked);

            LayerManager.Instance.SetLayerDirty(LayType.寻路预判区);
            LayerManager.Instance.SetLayerVisiable(LayType.寻路预判区, m_checkBoxShowRegion.Checked);

            LayerManager.Instance.SetLayerDirty(LayType.BlockQuad区);
            LayerManager.Instance.SetLayerVisiable(LayType.BlockQuad区, m_checkBoxShowRegion.Checked); 

            MainForm.Instance.RefreshSceneSketch(MainForm.Instance.GetCurrentMapID());
        }

        private void m_buttonDesc_Click(object sender, EventArgs e)
        {
            if (m_treeViewRegionList.SelectedNode != null)
            {
                int GroupID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
                RegionGroup showGroup = RegionManager.Instance.GetGroupByID(GroupID);
                if (showGroup != null)
                {
                    RegionDescForm form = RegionDescForm.Instance;
                    form.Text = m_treeViewRegionList.SelectedNode.Text;
                    form.RefreshUI(showGroup.DescInfo);
                    form.Show();
                }
            }
        }

        private void m_treeViewRegionList_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right)
            {
                Point ClickPoint = new Point(e.X, e.Y);
                TreeNode CurrentNode = m_treeViewRegionList.GetNodeAt(ClickPoint);
                if(CurrentNode != null)
                {
                    int iRegionID = int.Parse(CurrentNode.Tag.ToString());
                    RegionInfo region = RegionManager.Instance.GetRegionByID(iRegionID);
                    if (region != null)
                    {
                        ToolStripMenuItem item = m_contextMenuStripRegion.Items[m_contextMenuStripRegion.Items.Count-1] as ToolStripMenuItem;
                        item.DropDownItems.Clear();

                        ToolStripMenuItem newItem = new ToolStripMenuItem("未分配的区域");
                        newItem.Tag = "0";
                        newItem.MouseDown += new MouseEventHandler(ChangeGroupItem_MouseDown);
                        item.DropDownItems.Add(newItem);

                        ArrayList grouplist = RegionManager.Instance.MapID2GroupList[region.MapID] as ArrayList;
                        foreach(RegionGroup group in grouplist)
                        {
                            newItem = new ToolStripMenuItem(group.Name);
                            newItem.Tag = group.ID.ToString();
                            newItem.MouseDown += new MouseEventHandler(ChangeGroupItem_MouseDown);
                            item.DropDownItems.Add(newItem);
                        }

                        CurrentNode.ContextMenuStrip = m_contextMenuStripRegion;
                    }
                    else
                    {
                        RegionGroup group = RegionManager.Instance.GetGroupByID(iRegionID);
                        if(group != null)
                        {
                            CurrentNode.ContextMenuStrip = m_contextMenuStripGroup;
                        }
                    }
                    
                    m_treeViewRegionList.SelectedNode = CurrentNode;
                }
            }
        }

        void ChangeGroupItem_MouseDown(object sender, MouseEventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            
            int iRegionID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            int iGroupID = int.Parse(item.Tag.ToString());

            RegionInfo region = RegionManager.Instance.GetRegionByID(iRegionID);
            if(region != null)
            {
                region.GroupID = iGroupID;
                foreach (TreeNode node in m_treeViewRegionList.Nodes)
                {
                    if (node.Tag.ToString().Equals(iRegionID.ToString()))
                    {
                        m_treeViewRegionList.SelectedNode = node;
                    }
                }
                RefreshTree();
            }
        }

        private void 显示组ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionManager.Instance.HideGroup(iID, false);
            RefreshTree();
        }

        private void 隐藏组ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionManager.Instance.HideGroup(iID, true);
            RefreshTree();
        }

        private void 显示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionManager.Instance.HideRegion(iID, false);
            RefreshTree();
        }

        private void 隐藏ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionManager.Instance.HideRegion(iID, true);
            RefreshTree();
        }

        private void 属性ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int GroupID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionGroup showGroup = RegionManager.Instance.GetGroupByID(GroupID);
            if (showGroup != null)
            {
                RegionDescForm form = RegionDescForm.Instance;
                form.Text = m_treeViewRegionList.SelectedNode.Text;
                form.RefreshUI(showGroup.DescInfo);
                form.Show();
            }
        }

        private void 添加区域ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iGroupID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionGroup group = RegionManager.Instance.GetGroupByID(iGroupID);
            if(group != null)
            {
                int iNewRegionID = RegionManager.Instance.AddRegion(group.MapID).ID;
                RegionInfo region = RegionManager.Instance.GetRegionByID(iNewRegionID);
                if(region != null)
                {
                    region.GroupID = iGroupID;
                    MainForm.Instance.RefreshSceneSketch(region.MapID);
                    MainForm.Instance.SetSelectRegionByID(iNewRegionID);
                    RefreshTree();
                }
            }
        }

        private void m_checkBoxXXXXRegion_CheckedChanged(object sender, EventArgs e)
        {
            RefreshHidenGroup();
        }

        private void m_checkBoxXXXXNPC_CheckedChanged(object sender, EventArgs e)
        {
            RefreshHidenNPC();
        }

        public void RefreshHidenNPC()
        {
            int iMapID = MainForm.Instance.GetCurrentMapID();
            if (iMapID != -1)
            {
                ArrayList creatorList = NPCManager.Instance.MapID2NPCCreatorList[iMapID] as ArrayList;
                if (creatorList != null)
                {
                    foreach(NPCCreatorInfo creator in creatorList)
                    {
                        if(creator.IsSingle())// 单点NPC
                        {
                            creator.Hidden = !m_checkBoxSingleNPC.Checked;
                        }
                        else// 区域NPC
                        {
                            creator.Hidden = !m_checkBoxAreaNPC.Checked;
                        }

                        MainForm.Instance.UpdateNPCCreator(creator.ID, creator.Hidden);
                    }
                }

                LayerManager.Instance.SetLayerDirty(LayType.单点NPC);
                LayerManager.Instance.SetLayerDirty(LayType.区域NPC);

                MainForm.Instance.RefreshSceneSketchImage(iMapID);
            }
        }

        public void RefreshHidenGroup()
        {
            int iMapID = MainForm.Instance.GetCurrentMapID();
            if(iMapID != -1)
            {
                ArrayList groupList = RegionManager.Instance.MapID2GroupList[iMapID] as ArrayList;
                if(groupList != null)
                {
                    foreach(RegionGroup group in groupList)
                    {
                        if(group.BigType == RegionGroup.RegionType.服务器相关区域)
                        {
                            if(group.ServerType == RegionGroup.ServerRegionType.任务触发区域)
                            {
                                RegionManager.Instance.HideGroup(group.ID, !m_checkBoxTaskRegion.Checked);
                            }

                            if(group.ServerType == RegionGroup.ServerRegionType.安全区)
                            {
                                RegionManager.Instance.HideGroup(group.ID, !m_checkBoxOtherRegion.Checked);
                            }

                            if (group.ServerType == RegionGroup.ServerRegionType.不可PK区)
                            {
                                RegionManager.Instance.HideGroup(group.ID, !m_checkBoxOtherRegion.Checked);
                            }

                            if (group.ServerType == RegionGroup.ServerRegionType.不可骑乘区)
                            {
                                RegionManager.Instance.HideGroup(group.ID, !m_checkBoxOtherRegion.Checked);
                            }

                            if (group.ServerType == RegionGroup.ServerRegionType.寻路预判区)
                            {
                                RegionManager.Instance.HideGroup(group.ID, !m_checkBoxPrePathRegion.Checked);
                            }

                            if (group.ServerType == RegionGroup.ServerRegionType.BlockQuad区)
                            {
                                RegionManager.Instance.HideGroup(group.ID, !m_checkBoxBlockQuadRegion.Checked);
                            }
                        }

                        if(group.BigType == RegionGroup.RegionType.客户端表现区域)
                        {
                            if(group.ClientType == RegionGroup.ClientRegionType.音乐触发区域)
                            {
                                RegionManager.Instance.HideGroup(group.ID, !m_checkBoxMusicRegion.Checked);
                            }

                            if (group.ClientType == RegionGroup.ClientRegionType.音效特效触发区域)
                            {
                                RegionManager.Instance.HideGroup(group.ID, !m_checkBoxSoundRegion.Checked);
                            }
                        }
                    }

                    LayerManager.Instance.SetLayerDirty(LayType.音乐区);
                    LayerManager.Instance.SetLayerDirty(LayType.音效特效区);
                    LayerManager.Instance.SetLayerDirty(LayType.任务触发区);
                    LayerManager.Instance.SetLayerDirty(LayType.安全区);
                    LayerManager.Instance.SetLayerDirty(LayType.不可PK区);
                    LayerManager.Instance.SetLayerDirty(LayType.不可骑乘区);
                    LayerManager.Instance.SetLayerDirty(LayType.寻路预判区);
                    LayerManager.Instance.SetLayerDirty(LayType.BlockQuad区);
                    
                    MainForm.Instance.RefreshSceneSketchImage(iMapID);
                }
            }
        }

        private void m_treeViewRegionList_MouseEnter(object sender, EventArgs e)
        {
            if(m_bDirtyTree)
            {
                RefreshTree();
                m_bDirtyTree = false;
            }
            
        }

        private bool IsMouseOut()
        {
            int ix = Cursor.Position.X;
            int iy = Cursor.Position.Y;

            return (ix < this.Left || ix > this.Right || iy < this.Top || iy > this.Bottom);
        }

        private void RegionToolForm_LocationChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                m_iEdgeHidenState = 0;
                this.m_timerEdgeHiden.Enabled = false;
                return;
            }

            if (m_iEdgeHidenState == 0) // 自由状态
            {
                if (this.Location.Y <= 0)
                {
                    m_iEdgeHidenState = 1;
                    this.Top = 0;
                    this.m_timerEdgeHiden.Enabled = true;
                    //this.ShowInTaskbar = false;
                }        
            }
            else if (m_iEdgeHidenState == 1 || m_iEdgeHidenState == 2)
            {
                if (this.WindowState == FormWindowState.Minimized || this.Location.Y > 0)
                {
                    m_iEdgeHidenState = 0;
                    this.m_timerEdgeHiden.Enabled = false;
                    //this.ShowInTaskbar = true;
                }        
            }
        }

        private void m_timerEdgeHiden_Tick(object sender, EventArgs e)
        {
            if (this.Location.Y > 0)
            {
                m_iEdgeHidenState = 0;
                this.m_timerEdgeHiden.Enabled = false;
                //this.ShowInTaskbar = true;
            }        

            if (this.WindowState == FormWindowState.Minimized)
            {
                //this.ShowInTaskbar = true;
            }

            if (m_iEdgeHidenState == 0) // 自由状态
            {
               
            }
            else if (m_iEdgeHidenState == 1)
            {
                if (IsMouseOut())
                {
                    this.Top = 5 - this.Height;
                    m_iEdgeHidenState = 2;
                }
            }
            else if (m_iEdgeHidenState == 2)
            {
                if(!IsMouseOut())
                {
                    this.Top = 0;
                    m_iEdgeHidenState = 1;
                }
            }
        }

        private void RegionToolForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void m_checkBoxShowPtSound_CheckedChanged(object sender, EventArgs e)
        {
            MainForm.Instance.m_bShowPtSound = m_checkBoxShowPtSound.Checked;
            LayerManager.Instance.SetLayerDirty(LayType.点音源);
            LayerManager.Instance.SetLayerVisiable(LayType.点音源, m_checkBoxShowPtSound.Checked);
            MainForm.Instance.RefreshSceneSketch(MainForm.Instance.GetCurrentMapID());
        }

        private void m_comboBoxActiveLayer_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if(m_Instance != null)
            {
                if (MainForm.Instance.PathEditMode || m_comboBoxActiveLayer.SelectedIndex == 2)
                {
                    MainForm.Instance.路径编辑ToolStripMenuItem_Click(null, null);
                }

                if (m_comboBoxActiveLayer.SelectedIndex == 0 || m_comboBoxActiveLayer.SelectedIndex == 1 || m_comboBoxActiveLayer.SelectedIndex == 12)
                {
                    MessageBox.Show("不能编辑当前所选的图层！");
                }
                LayerManager.Instance.SetTopLayer(MainForm.Instance.GetCurrentMapID(), m_comboBoxActiveLayer.SelectedIndex);
            }
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionManager.Instance.DeleteRegion(iID);
            RefreshTree();
        }

        private void m_treeViewRegionList_Leave(object sender, EventArgs e)
        {
            m_bDirtyTree = true;
        }

        private void 同步到音乐库ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int GroupID = int.Parse(m_treeViewRegionList.SelectedNode.Tag.ToString());
            RegionGroup showGroup = RegionManager.Instance.GetGroupByID(GroupID);
            if (showGroup != null)
            {
                showGroup.DescInfo.Coordinate(1, RegionDescForm.Instance.MusicLibTable);
                showGroup.DescInfo.Coordinate(2, RegionDescForm.Instance.SoundLibTable);
                MessageBox.Show("同步更新成功！");
            }
        }
    }

    
}