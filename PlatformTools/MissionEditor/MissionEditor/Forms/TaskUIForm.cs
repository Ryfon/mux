﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace MissionEditor
{
    public partial class TaskUIForm : UserControl
    {
        public TaskUIForm(MissionManager missionMgr)
        {
            InitializeComponent();
            m_MissionMgr = missionMgr;
        }

        private void TaskUIForm_Load(object sender, EventArgs e)
        {
            // 初始化任务筛选列表
            InitTaskFilterList();

            // 初始化任务基本信息
            InitTaskBasePropGroup();

            // 初始化 任务领取条件 任务目标 任务奖励
            InitTaskCTAList();

            // 初始化 任务描述编辑界面
            InitTaskDescEdit();
        }

        private void UpdateTaskStatus(ArrayList missionList)
        {
            foreach (Mission mission in missionList)
            {
                mission.TeamWorkStatus = TeamWorkManager.Instance.GetMissionStatus(mission.ID).ID;
            }
        }
        private void InitTaskFilterList()
        {
            // 地图下拉框
            ArrayList arrMapName = SceneSketchManager.Instance.GetSceneNameList();
            foreach (String strMapName in arrMapName)
            {
                m_comboBoxFilterMap.Items.Add(strMapName);
            }
            m_comboBoxFilterMap.SelectedIndex = 0;

            // 任务class下拉框
            Hashtable classTable = m_MissionMgr.MissionClasses;
            int iClassNum = classTable.Count;
            //for (int index = 0; index < iClassNum; index++)
            //{
            //    String strClassName = classTable[index] as String;
            //    m_comboBoxFilterTaskType.Items.Add(strClassName);
            //}
            foreach (String strClassName in classTable.Values)
            {
                m_comboBoxFilterTaskType.Items.Add(strClassName);
            }
            m_comboBoxFilterTaskType.SelectedIndex = 0;

            // 任务显示列表
            m_listViewTaskList.Columns.Clear();
            m_listViewTaskList.Columns.Add("", 16, HorizontalAlignment.Center);
            m_listViewTaskList.Columns.Add("地图", 60, HorizontalAlignment.Center);
            m_listViewTaskList.Columns.Add("类别", 39, HorizontalAlignment.Center);
            m_listViewTaskList.Columns.Add("ID", 60, HorizontalAlignment.Center);
            m_listViewTaskList.Columns.Add("名称", 151, HorizontalAlignment.Left);
        }

        private void InitTaskBasePropGroup()
        {
            // 地图下拉框
            ArrayList arrMapName = SceneSketchManager.Instance.GetSceneNameList();
            foreach (String strMapName in arrMapName)
            {
                m_comboBoxMapName.Items.Add(strMapName);
            }

            // 任务class下拉框
            Hashtable classTable = m_MissionMgr.MissionClasses;
            int iClassNum = classTable.Count;

            //for (int index = 0; index < iClassNum; index++)
            //{
            //    String strClassName = classTable[index] as String;
            //    m_comboBoxTaskClass.Items.Add(strClassName);
            //}
            foreach (String strClassName in classTable.Values)
            {
                m_comboBoxTaskClass.Items.Add(strClassName);
            }

            // 任务类型选择树
            m_TaskTypeTree = new TreeControl(m_textBoxTaskType);
            m_panelShowTypeTree.Controls.Add(m_TaskTypeTree);

            // 接还任务对象选择
            m_ProviderSubmitSel = new ObjSelControl();
            m_ProviderSubmitSel.Top = 25;
            m_panelShowObjSel.Controls.Add(m_ProviderSubmitSel);

            // 任务目标对象选择
            m_TaskTargetSel = new ObjSelControl();
            m_TaskTargetSel.Top = 25;
            m_panelShowTaskTargetSel.Controls.Add(m_TaskTargetSel);

            // 任务数据库
            InitDataBase();

            // 任务条件 任务奖励 选择框
            m_TaskConditionSel = new ConditionSelControl();
            m_panelShowTaskConditionSel.Controls.Add(m_TaskConditionSel);

            // 时间限制
            m_textBoxTimeLimit.Text = "00:00:00";
        }

        public void InitDataBase()
        {
            // 任务条件数据库
            m_TaskConditionDB = InitTaskBaseDataBase(MissionConditionManager.Instance.ConditionsMap);

            // 任务奖励数据库
            m_TaskAwardDB = InitTaskBaseDataBase(MissionAwardManager.Instance.AwardMaps);
        }

        private void InitTaskCTAList()
        {
            // 设置TaskCondition
            m_listViewTaskCondition.Columns.Clear();
            m_listViewTaskCondition.Columns.Add("条件名称", 140, HorizontalAlignment.Center);
            m_listViewTaskCondition.Columns.Add("条件参数", 196, HorizontalAlignment.Left);

            // 设置TaskTarget
            m_listViewTaskTarget.Columns.Clear();
            m_listViewTaskTarget.Columns.Add("目标分类", 70, HorizontalAlignment.Center);
            m_listViewTaskTarget.Columns.Add("任务目标", 266, HorizontalAlignment.Left);

            // 设置TaskAward
            m_listViewTaskAward.Columns.Clear();
            m_listViewTaskAward.Columns.Add("奖励内容", 140, HorizontalAlignment.Center);
            m_listViewTaskAward.Columns.Add("奖励参数", 196, HorizontalAlignment.Left);
        }

        private void InitTaskDescEdit()
        {
            // 任务简单描述
            m_SimpleDescEdit = new DescEditControl();
            m_SimpleDescEdit.Height = m_panelSimpleDesc.Height;
            m_panelSimpleDesc.Controls.Add(m_SimpleDescEdit);

            // 接任务描述
            m_GetDescEdit = new DescEditControl();
            m_GetDescEdit.Height = m_panelGetDesc.Height;
            m_panelGetDesc.Controls.Add(m_GetDescEdit);

            // 还任务描述
            m_SubmitDescEdit = new DescEditControl();
            m_SubmitDescEdit.Height = m_panelSubmitDesc.Height;
            m_panelSubmitDesc.Controls.Add(m_SubmitDescEdit);

            // 接任务追踪
            m_GetHintEdit = new DescEditControl();
            m_GetHintEdit.Height = m_panelHintDesc.Height;
            m_panelHintDesc.Controls.Add(m_GetHintEdit);
        }

        private DataSet InitTaskBaseDataBase(Hashtable map)
        {
            // 初始化变量
            DataSet result = new DataSet();

            // 从ConditionManager中获取数据
            // 第一层表
            foreach (String strTableName in map.Keys)
            {
                // 获取第二层表
                Hashtable ht = map[strTableName] as Hashtable;

                // 创建表
                DataTable dt = new DataTable(strTableName);

                // 设置列名
                DataColumn name = new DataColumn("名称", System.Type.GetType("System.String"));
                DataColumn value = new DataColumn("参数", System.Type.GetType("System.String"));
                DataColumn use = new DataColumn("应用", System.Type.GetType("System.String"));
                DataColumn desc = new DataColumn("描述", System.Type.GetType("System.String"));
                DataColumn param = new DataColumn("参数说明", System.Type.GetType("System.String"));
                dt.Columns.Add(name);
                dt.Columns.Add(value);
                dt.Columns.Add(use);
                dt.Columns.Add(desc);
                dt.Columns.Add(param);

                // 插入每一行
                foreach (String strRowName in ht.Keys)
                {
                    // 获取该行的信息
                    MissionBaseData mci = ht[strRowName] as MissionBaseData;

                    // 将该行插入
                    DataRow dr = dt.NewRow();
                    dr["名称"] = mci.Display;
                    dr["参数"] = "";
                    dr["应用"] = "";
                    dr["描述"] = mci.Desc;
                    dr["参数说明"] = mci.Param;

                    dt.Rows.Add(dr);
                }

                // 将表格插入
                result.Tables.Add(dt);
            }

            // 将结果返回
            return result;
        }

        private void InitUIDefaultValue()
        {
            // 基本设置
            m_textBoxTaskID.Text = "0000000";
            m_textBoxTaskName.Text = "任务名称";
            m_comboBoxMapName.SelectedIndex = 0;
            m_comboBoxTaskClass.SelectedIndex = 0;
            m_textBoxTaskLevel.Text = "1";
            m_comboBoxTaskStarLevel.SelectedIndex = 0;

            m_radioButtonPermanentSave.Checked = true;
            m_textBoxSaveDays.Text = "0";
            m_textBoxSaveTimes.Text = "0";

            m_textBoxTaskType.Text = "";
            m_textBoxTaskProvider.Text = "";
            m_textBoxTaskSubmit.Text = "";
            m_textBoxTaskProviderID.Text = "-1";
            m_textBoxTaskSubmitID.Text = "-1";

            m_checkBoxTimeLimit.Checked = false;
            m_textBoxTimeLimit.Text = "00:00:00";

            m_checkBoxCircleTask.Checked = false;
            m_checkBoxShareTask.Checked = false;
            m_checkBoxAbandonTask.Checked = true;

            // 条件 目标 奖励
            m_textBoxStartTargetID.Text = "-1";
            m_textBoxEndTargetID.Text = "-1";
            m_listViewTaskCondition.Items.Clear();
            m_listViewTaskTarget.Items.Clear();
            m_listViewTaskAward.Items.Clear();

            // 任务描述
            m_SimpleDescEdit.DescText = "";
            m_GetDescEdit.DescText = "";
            m_SubmitDescEdit.DescText = "";
            m_GetHintEdit.DescText = "";
        }

        private bool InitTaskTargetSelPanel()
        {
            // ID设置
            m_textBoxTempStartTargetID.Text = m_textBoxStartTargetID.Text;
            m_textBoxTempEndTargetID.Text = m_textBoxEndTargetID.Text;
            m_textBoxTempStartTargetID.Location = m_textBoxTempStartTarget.Location;
            m_textBoxTempEndTargetID.Location = m_textBoxTempEndTarget.Location;
            m_textBoxTempStartTargetID.Visible = false;
            m_textBoxTempEndTargetID.Visible = false;
            int iStartID = int.Parse(m_textBoxStartTargetID.Text);
            int iEndID = int.Parse(m_textBoxEndTargetID.Text);

            // 获取任务的类型
            String strMissionType = m_textBoxTaskType.Text;

            // 根据任务类型显示
            if (strMissionType.Length == 0)
            {
                MessageBox.Show("请先设置任务类型。");
                return false;
            }
            #region 计数器类
            else if (strMissionType.Equals("计数器类_杀怪A类"))
            {
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "起始怪物:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 1);

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "终止怪物:";
                m_textBoxTempEndTarget.Visible = true;
                m_textBoxTempEndTarget.Text = GetObjNameByID(iEndID, 1);
            }
            else if (strMissionType.Equals("计数器类_收集物品A类"))
            {
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "起始道具:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 2);

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "终止道具:";
                m_textBoxTempEndTarget.Visible = true;
                m_textBoxTempEndTarget.Text = GetObjNameByID(iEndID, 2);
            }
            else if (strMissionType.Equals("计数器类_杀人A类"))
            {
                //MessageBox.Show("该版本暂时不支持此类任务。");
                //return false;
                m_textBoxTempStartTargetID.Visible = true;
                m_textBoxTempEndTargetID.Visible = true;

                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "起始玩家:";
                m_textBoxTempStartTarget.Visible = false;

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "终止玩家:";
                m_textBoxTempEndTarget.Visible = false;
            }
            else if (strMissionType.Equals("计数器类_交友A类"))
            {
                //MessageBox.Show("该版本暂时不支持此类任务。");
                //return false;
                m_textBoxTempStartTargetID.Visible = true;
                m_textBoxTempEndTargetID.Visible = true;

                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "起始玩家:";
                m_textBoxTempStartTarget.Visible = false;

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "终止玩家:";
                m_textBoxTempEndTarget.Visible = false;
            }
            else if (strMissionType.Equals("计数器类_答题A类"))
            {
                m_textBoxTempStartTargetID.Visible = true;
                m_textBoxTempEndTargetID.Visible = true;

                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "起始题目:";
                m_textBoxTempStartTarget.Visible = false;
                
                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "终止题目:";
                m_textBoxTempEndTarget.Visible = false;              
            }
            else if (strMissionType.Equals("计数器类_道具1型A类"))
            {
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "起始道具:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 2);

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "终止道具:";
                m_textBoxTempEndTarget.Visible = true;
                m_textBoxTempEndTarget.Text = GetObjNameByID(iEndID, 2);
            }
            else if (strMissionType.Equals("计数器类_道具2型A类"))
            {
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "起始道具:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 2);

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "终止道具:";
                m_textBoxTempEndTarget.Visible = true;
                m_textBoxTempEndTarget.Text = GetObjNameByID(iEndID, 2);
            }
            #endregion

            #region 对话类
            else if (strMissionType.Equals("对话类_单纯对话A类"))
            {
                m_labelStartTarget.Visible = false;
                m_textBoxTempStartTarget.Visible = false;

                m_labelEndTarget.Visible = false;
                m_textBoxTempEndTarget.Visible = false;
            }
            else if (strMissionType.Equals("对话类_单纯对话B类"))
            {
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "目标NPC:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 1);

                m_labelEndTarget.Visible = false;
                m_textBoxTempEndTarget.Visible = false;
            }
            else if (strMissionType.Equals("对话类_送物品A类"))
            {
                m_labelStartTarget.Visible = false;
                m_textBoxTempStartTarget.Visible = false;

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "目标道具:";
                m_textBoxTempEndTarget.Visible = true;
                m_textBoxTempEndTarget.Text = GetObjNameByID(iEndID, 2);
            }
            else if (strMissionType.Equals("对话类_送物品B类"))
            {
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "目标NPC:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 1);

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "目标道具:";
                m_textBoxTempEndTarget.Visible = true;
                m_textBoxTempEndTarget.Text = GetObjNameByID(iEndID, 2);
            }
            else if (strMissionType.Equals("对话类_取物品B类"))
            {
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "目标NPC:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 1);

                m_labelEndTarget.Visible = true;
                m_labelEndTarget.Text = "目标道具:";
                m_textBoxTempEndTarget.Visible = true;
                m_textBoxTempEndTarget.Text = GetObjNameByID(iEndID, 2);
            }
            #endregion

            #region 护送类
            else if (strMissionType.Equals("护送类_护送A类"))
            {
                //MessageBox.Show("该版本暂时不支持此类任务。");
                //return false;
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "目标NPC:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 1);

                m_labelEndTarget.Visible = false;
                m_textBoxTempEndTarget.Visible = false;
                
            }
            #endregion

            #region 探索类
            else if (strMissionType.Equals("探索类_探索B类"))
            {
                m_labelStartTarget.Visible = true;
                m_labelStartTarget.Text = "目标区域:";
                m_textBoxTempStartTarget.Visible = true;
                m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, 3);

                m_labelEndTarget.Visible = false;
                m_textBoxTempEndTarget.Visible = false;
            }
            #endregion

            else
            {
                MessageBox.Show("未知的任务类型，无法设置任务目标。");
                return false;
            }

            // 追踪名称，目标值
            if (m_listViewTaskTarget.Items.Count > 0)
            {
                // 获取数据
                ListViewItem lv = m_listViewTaskTarget.Items[0];

                // 类型
                String strType = lv.SubItems[0].Text;
                strType = strType.Replace("目标", "");
                m_comboBoxTempTaskTargetType.SelectedItem = strType;

                String strNameValue = lv.SubItems[1].Text;
                int iIndex = strNameValue.IndexOf("x");
                String strName = strNameValue.Substring(0, iIndex);
                String strValue = strNameValue.Substring(iIndex + 1, strNameValue.Length - iIndex - 1);
                m_textBoxTempTraceName.Text = strName;
                m_textBoxTempTargetValue.Text = strValue;
            }
            else
            {
                m_comboBoxTempTaskTargetType.SelectedItem = "NPC";
                m_textBoxTempTraceName.Text = m_textBoxTaskSubmit.Text;
                m_textBoxTempTargetValue.Text = "-1";
            }

            return true;
        }

        private void RefreshTaskList(ArrayList result)
        {
            // 清空旧的数据
            m_listViewTaskList.Items.Clear();

            // 遍历添加新的数据
            foreach (Mission mission in result)
            {
                String strTaskType = "未知任务";
                Object obTaskType = m_MissionMgr.MissionClasses[mission.Class];
                if (obTaskType != null)
                {
                    strTaskType = obTaskType.ToString();
                }
                else
                {
                    MessageBox.Show("无法在TaskBaseData.xml中找到任务类型编号"+mission.Class.ToString() + "，任务"+ mission.ID.ToString() +"无法显示");
                    continue;
                }
                AddTaskToList(m_listViewTaskList, mission.TeamWorkMark, SceneSketchManager.Instance.GetSceneNameByID(mission.LeadMap), strTaskType, mission.ID.ToString(), mission.Name);
            }
        }

        private void AddToListView(ListView view, String strName, String strValue)
        {
            ListViewItem lv = new ListViewItem();

            lv.SubItems[0].Text = strName;
            lv.SubItems.Add(strValue);

            view.Items.Add(lv);
        }

        private void AddTaskToList(ListView view, String strStatus, String strMapName, String strTaskType, String strTaskID, String strTaskName)
        {
            ListViewItem lv = new ListViewItem();

            lv.SubItems[0].Text = strStatus;
            lv.SubItems.Add(strMapName);
            lv.SubItems.Add(strTaskType.Substring(0, 2));
            lv.SubItems.Add(strTaskID);
            lv.SubItems.Add(strTaskName);

            view.Items.Add(lv);
        }

        private void RemoveDiaByMission(Mission m_currMission)
        {
            String strChangedDia = "";
            //DialogResult result = MessageBox.Show("要删除任务:" + m_currMission.Name + " 的相关状态图吗?", "提示", MessageBoxButtons.YesNo);
            //if (result == DialogResult.Yes)
            {
                if (m_currMission.Provider > 0)
                {
                    // 获取 Provider 状态图。删除本  mission 信息
                    String strProviderDiaName = null;
                    if (!m_currMission.ItemProvider)
                    {
                        int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, m_currMission.Provider);
                        strProviderDiaName = OptionManager.NPCPrefix + iProviderStateDiaID.ToString();
                    }
                    else
                    {
                        int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_ITEM, m_currMission.Provider);
                        strProviderDiaName = OptionManager.ItemPrefix + iProviderStateDiaID.ToString();
                    }

                    StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strProviderDiaName);
                    if (dia != null)
                    {
                        dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_GET);
                        dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_GET_HINT);
                        dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                        dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                        StateDiagramManager.SaveStateDiagram(dia);
                        strChangedDia += (strProviderDiaName + ", ");
                    }
                }

                if (m_currMission.TargetNPC > 0)
                {
                    // 获取 Provider 状态图。删除本  mission 信息
                    int iTargetNPCStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, m_currMission.TargetNPC);
                    String strTargetNPCDiaName = OptionManager.NPCPrefix + iTargetNPCStateDiaID.ToString();
                    StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strTargetNPCDiaName);
                    if (dia != null)
                    {
                        dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                        dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                        StateDiagramManager.SaveStateDiagram(dia);
                        strChangedDia += (strTargetNPCDiaName + ", ");
                    }
                }

                if (m_currMission.SubmitTo > 0)
                {
                    // 获取 SubmitTo 状态图。删除本  mission 信息
                    int iSubmitToStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, m_currMission.SubmitTo);
                    String strSubmitToDiaName = OptionManager.NPCPrefix + iSubmitToStateDiaID.ToString();
                    StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strSubmitToDiaName);
                    if (dia != null)
                    {
                        dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_SUBMIT);
                        StateDiagramManager.SaveStateDiagram(dia);
                        strChangedDia += (strSubmitToDiaName + ", ");
                    }
                }

                //MessageBox.Show("任务删除完成.发生改变的状态图有:" + strChangedDia);
            }
        }

        private void m_listViewTaskList_MouseMove(object sender, MouseEventArgs e)
        {
            ListViewItem lv = m_listViewTaskList.GetItemAt(e.X, e.Y);
            if (lv != null && lv.Selected)
            {
                m_toolTipTaskDetail.SetToolTip(m_listViewTaskList, lv.SubItems[4].Text);
            }
            else
            {
                m_toolTipTaskDetail.RemoveAll();
            }
        }

        private void m_buttonTaskType_Click(object sender, EventArgs e)
        {
            if (m_panelShowTypeTree.Visible)
            {
                m_panelShowTypeTree.Hide();
            }
            else
            {
                m_panelShowTypeTree.Left = 80;
                m_panelShowTypeTree.Top = 155;//176;

                m_panelShowTypeTree.Show();
                m_TaskTypeTree.ChangeSelect();
            }
        }

        private void m_buttonFilter_Click(object sender, EventArgs e)
        {
            // 定义3个筛选变量
            String strMapName = "";
            String strClassName = "";
            String strTaskName = m_textBoxFilterTaskName.Text;

            // 获取变量
            if (m_comboBoxFilterMap.SelectedItem != null)
            {
                strMapName = m_comboBoxFilterMap.SelectedItem.ToString();
            }
            if (m_comboBoxFilterTaskType.SelectedItem != null)
            {
                strClassName = m_comboBoxFilterTaskType.SelectedItem.ToString();
            }

            // 查找中
            m_buttonFilter.Enabled = false;
            m_labelFilterResultNum.Text = "查找中。。。。。。";
            Update();

            // 开始筛选
            ArrayList result = m_MissionMgr.Filter(strMapName, strClassName, strTaskName);

            // 更新任务状态
            UpdateTaskStatus(result);

            // 刷新
            RefreshTaskList(result);

            // 刷新查找结果数量
            m_labelFilterResultNum.Text = "共查找到" + result.Count.ToString() + "个任务";
            m_buttonFilter.Enabled = true;
        }

        private void m_listViewTaskList_DoubleClick(object sender, EventArgs e)
        {
            ListViewItem lv = m_listViewTaskList.SelectedItems[0];
            if (lv != null && lv.Selected)
            {
                int iMissionID;
                if (int.TryParse(lv.SubItems[3].Text, out iMissionID))
                {
                    Mission mission = m_MissionMgr.FindByID(iMissionID);
                    if (mission != null)
                    {
                        // 如果有新建的任务，提示保存
                        if (m_bNewMission)
                        {
                            DialogResult result = MessageBox.Show("切换任务前，是否保存新建的任务？", "", MessageBoxButtons.YesNoCancel);
                            if (result == DialogResult.Yes)
                            {
                                SaveMission(m_CurrMission);
                            }
                            else if (result == DialogResult.Cancel)
                            {
                                return;
                            }
                        }

                        // 显示任务
                        m_CurrMission = mission;
                        m_bNewMission = false;
                        ShowMission(mission);
                    }
                }
            }
        }

        private int GetRecordReserve()
        {
            if (m_radioButtonNotSave.Checked)    // 不存
            {
                return 0;
            }
            else if (m_radioButtonPermanentSave.Checked) // 永久
            {
                return 9999;
            }
            else if (m_radioButtonDailySave.Checked) // 天数
            {
                int iDays = 0;
                if (m_textBoxSaveDays.TextLength == 0 || !int.TryParse(m_textBoxSaveDays.Text, out iDays))
                {
                    MessageBox.Show("存盘方式：格式不正确，应该为1~999之间的整数。");
                    return -1;
                }
                if (iDays >= 1 && iDays <= 999)
                {
                    return iDays;
                }
                else
                {
                    MessageBox.Show("存盘方式：格式不正确，应该为1~999之间的整数。");
                    return -1;
                }
            }
            else if (m_radioButtonTimesSave.Checked) // 次数
            {
                int iTimes = 0;
                if (m_textBoxSaveTimes.TextLength == 0 || !int.TryParse(m_textBoxSaveTimes.Text, out iTimes))
                {
                    MessageBox.Show("存盘方式：格式不正确，应该为1~999之间的整数。");
                    return -1;
                }
                if (iTimes >= 1 && iTimes <= 999)
                {
                    return (iTimes + 1000);
                }
                else
                {
                    MessageBox.Show("存盘方式：格式不正确，应该为1~999之间的整数。");
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }

        private void SetRecordReserve(Mission mission)
        {
            // 默认的初始值为0
            m_textBoxSaveDays.Text = "0";
            m_textBoxSaveTimes.Text = "0";

            // 解析字串并填写
            int iTempReserved = mission.RecordReserve;  // 存盘方式
            if (iTempReserved == 0) // 不存
            {
                m_radioButtonNotSave.Checked = true;
            }
            else if (iTempReserved >= 1 && iTempReserved <= 999)    // 天数
            {
                m_radioButtonDailySave.Checked = true;
                m_textBoxSaveDays.Text = iTempReserved.ToString();
            }
            else if (iTempReserved >= 1001 && iTempReserved <= 1999)    // 次数
            {
                m_radioButtonTimesSave.Checked = true;
                int iRealTimes = iTempReserved - 1000;
                m_textBoxSaveTimes.Text = iRealTimes.ToString();
            }
            else if (iTempReserved == 9999) // 永久
            {
                m_radioButtonPermanentSave.Checked = true;
            }
            else
            {
                m_radioButtonPermanentSave.Checked = true;  // 默认为永久保存
            }
        }

        private int GetTimeLimit()
        {
            if (m_checkBoxTimeLimit.Checked)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(m_textBoxTimeLimit.Text, "[0-9]{2}:[0-5][0-9]:[0-5][0-9]"))
                {
                    String strHour = m_textBoxTimeLimit.Text.Substring(0, 2);
                    String strMin = m_textBoxTimeLimit.Text.Substring(3, 2);
                    String strSec = m_textBoxTimeLimit.Text.Substring(6, 2);
                    int iHour = int.Parse(strHour);
                    int iMin = int.Parse(strMin);
                    int iSec = int.Parse(strSec);

                    return (iHour * 3600 + iMin * 60 + iSec);
                }
                else
                {
                    MessageBox.Show("时间限制：格式不正确。");
                    return -1;
                }
            }
            else
            {
                return 0;
            }
        }

        private void SetTimeLimit(Mission mission)
        {
            int iTime = mission.TimeLimit;
            if (iTime == 0)
            {
                m_checkBoxTimeLimit.Checked = false;
                m_textBoxTimeLimit.Text = "00:00:00";
            }
            else if (iTime > 0)
            {
                m_checkBoxTimeLimit.Checked = true;

                int iHour = iTime / 3600;   // 时
                String strHour = iHour.ToString("d2");
                iTime = iTime % 3600;

                int iMin = iTime / 60;   // 分
                String strMin = iMin.ToString("d2");
                iTime = iTime % 60;

                int iSec = iTime;   // 秒
                String strSec = iSec.ToString("d2");

                m_textBoxTimeLimit.Text = strHour + ":" + strMin + ":" + strSec;
            }
            else
            {
                m_checkBoxTimeLimit.Checked = false;
                m_textBoxTimeLimit.Text = "00:00:00";
            }
        }

        private void ShowMission(Mission mission)
        {
            // 关闭所有浮动窗口
            m_panelShowTypeTree.Hide();
            m_panelShowObjSel.Hide();
            m_panelShowTaskTargetSel.Hide();
            m_panelShowTaskConditionSel.Hide();

            // 显示任务
            if (mission != null)
            {
                ShowMissionBasicPropGroup(mission); // 显示任务基本属性

                ShowMissionCTAGroup(mission);       // 显示任务目标，条件，奖励

                ShowMissionDescGroup(mission);      // 显示任务描述信息
            }
        }

        private void ShowMissionBasicPropGroup(Mission mission)
        {
            if (mission != null)
            {
                m_textBoxTaskID.Text = mission.ID.ToString();  // 任务ID
                m_textBoxTaskName.Text = mission.Name;  // 任务名称

                String strMapName = SceneSketchManager.Instance.GetSceneNameByID(mission.LeadMap);   // 地图名称
                m_comboBoxMapName.SelectedItem = strMapName;

                String strClassName = m_MissionMgr.MissionClasses[mission.Class] as String; // 任务类别
                m_comboBoxTaskClass.SelectedItem = strClassName;

                m_textBoxTaskLevel.Text = mission.Level.ToString(); // 任务等级

                m_comboBoxTaskStarLevel.SelectedIndex = mission.StarLevel - 1;  // 任务难度

                SetRecordReserve(mission);  // 存盘方式

                m_textBoxTaskType.Text = mission.Type + "_" + mission.SubMissionType + mission.FlowType;    // 任务类型

                m_textBoxTaskProviderID.Text = mission.Provider.ToString(); // 接任务对象ID
                if (mission.ItemProvider)  // 接任务对象名称
                {
                    m_textBoxProviderType.Text = "2";
                    Item item = ItemManager.Instance.GetItemByID(mission.Provider);
                    if (item != null)
                    {
                        m_textBoxTaskProvider.Text = item.Name;
                    }
                }
                else if (mission.RegionProvider)
                {
                    m_textBoxProviderType.Text = "3";
                    RegionGroup group = RegionManager.Instance.GetGroupByID(mission.Provider);
                    if (group != null)
                    {
                        m_textBoxTaskProvider.Text = group.Name;
                    }
                }
                else
                {
                    m_textBoxProviderType.Text = "1";
                    NPCInfo npc = NPCManager.Instance.GetNPCInfoByID(mission.Provider);
                    if (npc != null)
                    {
                        m_textBoxTaskProvider.Text = npc.Name;
                    }
                }

                m_textBoxTaskSubmitID.Text = mission.SubmitTo.ToString(); // 还任务对象ID
                NPCInfo SubMitNpc = NPCManager.Instance.GetNPCInfoByID(mission.SubmitTo); // 还任务对象
                if (SubMitNpc != null)
                {
                    m_textBoxTaskSubmit.Text = SubMitNpc.Name;
                }

                m_checkBoxCircleTask.Checked = mission.Repeat;  // 可循环

                m_checkBoxShareTask.Checked = mission.Share;    // 可共享

                m_checkBoxAbandonTask.Checked = mission.GiveUp; // 可放弃

                SetTimeLimit(mission);  // 时间限制
            }
        }

        private void ShowMissionTarget(Mission mission)
        {
            if (mission != null)
            {
                // 变量定义
                String strType = "目标";
                String strNameValue = mission.TrackName + "x" + mission.TargetValue.ToString();

                // 默认值
                m_textBoxStartTargetID.Text = "-1";
                m_textBoxEndTargetID.Text = "-1";

                // 根据类型设置Start和End
                #region 计数器类

                if (mission.Type.Equals("计数器类"))
                {
                    if (mission.SubMissionType.Equals("杀怪"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetBegin.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetEnd.ToString();
                            strType += "怪物";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else if (mission.SubMissionType.Equals("收集物品"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetBegin.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetEnd.ToString();
                            strType += "道具";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else if (mission.SubMissionType.Equals("杀人"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetBegin.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetEnd.ToString();
                            strType += "玩家";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else if (mission.SubMissionType.Equals("交友"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetBegin.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetEnd.ToString();
                            strType += "玩家";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else if (mission.SubMissionType.Equals("答题"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetBegin.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetEnd.ToString();
                            strType += "道具";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else if (mission.SubMissionType.Equals("道具1型"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetBegin.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetEnd.ToString();
                            strType += "道具";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else if (mission.SubMissionType.Equals("道具2型"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetBegin.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetEnd.ToString();
                            strType += "道具";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else
                    {
                        MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                    }

                }
                #endregion

                #region 对话类

                else if (mission.Type.Equals("对话类"))
                {
                    if (mission.SubMissionType.Equals("单纯对话"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = "-1";
                            m_textBoxEndTargetID.Text = "-1";
                            strType += "NPC";
                        }
                        else if (mission.FlowType.Equals("B类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetNPC.ToString();
                            m_textBoxEndTargetID.Text = "-1";
                            strType += "NPC";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else if (mission.SubMissionType.Equals("送物品"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = "-1";
                            m_textBoxEndTargetID.Text = mission.TargetBegin.ToString();
                            strType += "NPC";
                        }
                        else if (mission.FlowType.Equals("B类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetNPC.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetBegin.ToString();
                            strType += "NPC";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else if (mission.SubMissionType.Equals("取物品"))
                    {
                        if (mission.FlowType.Equals("B类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetNPC.ToString();
                            m_textBoxEndTargetID.Text = mission.TargetBegin.ToString();
                            strType += "NPC";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else
                    {
                        MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                    }
                }
                #endregion

                #region 护送类

                else if (mission.Type.Equals("护送类"))
                {
                    if (mission.SubMissionType.Equals("护送"))
                    {
                        if (mission.FlowType.Equals("A类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetNPC.ToString();
                            m_textBoxEndTargetID.Text = "-1";
                            strType += "NPC";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else
                    {
                        MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                    }
                }
                #endregion

                #region 探索类

                else if (mission.Type.Equals("探索类"))
                {
                    if (mission.SubMissionType.Equals("探索"))
                    {
                        if (mission.FlowType.Equals("B类"))
                        {
                            m_textBoxStartTargetID.Text = mission.TargetArea.ToString();
                            m_textBoxEndTargetID.Text = "-1";
                            strType += "区域";
                        }
                        else
                        {
                            MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                        }
                    }
                    else
                    {
                        MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                    }
                }
                else// 未知的类型
                {
                    MessageBox.Show(mission.ID.ToString() + "任务类型错误：无法显示任务目标！");
                }
                #endregion

                // UI显示
                m_listViewTaskTarget.Items.Clear();
                if (strType.Length > 2)
                {
                    AddToListView(m_listViewTaskTarget, strType, strNameValue);
                }
            }
        }

        private void ShowMissionCTAGroup(Mission mission)
        {
            if (mission != null)
            {
                // 任务目标
                ShowMissionTarget(mission);

                //String strType = "目标";
                //String strNameValue = mission.TrackName + "x" + mission.TargetValue.ToString();

                //if (mission.TargetNPC != -1)
                //{
                //    m_textBoxStartTargetID.Text = mission.TargetNPC.ToString();
                //    m_textBoxEndTargetID.Text = mission.TargetNPC.ToString();
                //    strType += "NPC";
                //}
                //else if (mission.TargetItemBegin != -1 || mission.TargetItemEnd != -1)
                //{
                //    m_textBoxStartTargetID.Text = mission.TargetItemBegin.ToString();
                //    m_textBoxEndTargetID.Text = mission.TargetItemEnd.ToString();
                //    strType += "道具";
                //}
                //else if (mission.TargetArea != -1)
                //{
                //    m_textBoxStartTargetID.Text = mission.TargetArea.ToString();
                //    m_textBoxEndTargetID.Text = mission.TargetArea.ToString();
                //    strType += "区域";
                //}
                //else if (mission.TargetMonsterBegin != -1 || mission.TargetMonsterEnd != -1)
                //{
                //    m_textBoxStartTargetID.Text = mission.TargetMonsterBegin.ToString();
                //    m_textBoxEndTargetID.Text = mission.TargetMonsterEnd.ToString();
                //    strType += "怪物";
                //}

                //m_listViewTaskTarget.Items.Clear();
                //if(strType.Length > 2)
                //{
                //    AddToListView(m_listViewTaskTarget, strType, strNameValue);
                //}

                // 任务条件
                m_listViewTaskCondition.Items.Clear();
                foreach (MissionCondition mc in mission.Conditions)
                {
                    AddToListView(m_listViewTaskCondition, mc.Condition, mc.Value);
                }

                // 任务奖励
                m_listViewTaskAward.Items.Clear();
                foreach (MissionAward ma in mission.FixedAwards)
                {
                    AddToListView(m_listViewTaskAward, ma.Award, ma.Value);
                }
            }
        }

        private void ShowMissionDescGroup(Mission mission)
        {
            if (mission != null)
            {
                m_SimpleDescEdit.DescText = mission.SimpleDesc; // 任务简单描述

                m_GetDescEdit.DescText = mission.GetDescription;    // 接任务描述

                m_SubmitDescEdit.DescText = mission.SubmitDescription;  // 还任务描述

                // 接任务追踪
                m_GetHintEdit.DescText = mission.GetHint;
            }
        }

        private bool SaveMission(Mission mission)
        {
            if (SaveMissionBasicPropGroup(mission))
            {
                // 保存任务条件 任务奖励
                SaveMissionCTAGroup(mission);

                // 保存任务的描述
                SaveMissionDescGroup(mission);

                // 保存新任务
                if (m_bNewMission)
                {
                    m_CurrMission.TeamWorkStatus = 0;
                    m_MissionMgr.MissionList.Add(m_CurrMission);
                    m_bNewMission = false;
                }

                // 写入文件
                m_MissionMgr.SaveMissions(OptionManager.MissionPath);
                return true;
            }
            return false;
        }

        private bool SaveMissionBasicPropGroup(Mission mission)
        {
            #region 从UI中获取属性

            #region 基本属性
            // 任务ID
            int iMissionID = 0;
            if (m_textBoxTaskID.TextLength != 7 || !int.TryParse(m_textBoxTaskID.Text, out iMissionID))
            {
                MessageBox.Show("任务ID：填写不规范，ID为7位整数。");
                return false;
            }

            // 任务名称
            String strMissionName = m_textBoxTaskName.Text;
            if (strMissionName.Length == 0)
            {
                MessageBox.Show("任务名称：不能为空。");
                return false;
            }

            // 地图名称
            int iMissionMapID = 0;
            if (m_comboBoxMapName.SelectedItem == null)
            {
                MessageBox.Show("地图名称：不能为空。");
                return false;
            }
            else
            {
                iMissionMapID = SceneSketchManager.Instance.GetSceneIDByName(m_comboBoxMapName.SelectedItem.ToString());
            }

            // 任务类别
            int iMissionClass = 0;
            if (m_comboBoxTaskClass.SelectedItem == null)
            {
                MessageBox.Show("任务类别：不能为空。");
                return false;
            }
            else
            {
                iMissionClass = m_MissionMgr.GetClassIDByName(m_comboBoxTaskClass.SelectedItem.ToString());
            }

            // 任务等级
            int iMissionLevel = 0;
            if (m_textBoxTaskLevel.TextLength == 0 || !int.TryParse(m_textBoxTaskLevel.Text, out iMissionLevel))
            {
                MessageBox.Show("任务等级：填写不规范，应该为非负整数。");
                return false;
            }
            if (iMissionLevel < 0)
            {
                MessageBox.Show("任务等级：填写不规范，应该为非负整数。");
                return false;
            }

            // 任务难度
            int iMissionStarLevel = 0;
            if (m_comboBoxTaskStarLevel.SelectedIndex == -1)
            {
                MessageBox.Show("任务难度：不能为空。");
                return false;
            }
            else
            {
                iMissionStarLevel = m_comboBoxTaskStarLevel.SelectedIndex + 1;
            }

            // 存盘方式
            int iMissionRecordReserve = GetRecordReserve();
            if (iMissionRecordReserve == -1)
            {
                return false;
            }

            // 任务类型
            MissionType missiontype = new MissionType();
            if (!missiontype.Convert(m_textBoxTaskType.Text))
            {
                MessageBox.Show("任务类型：格式不正确。");
                return false;
            }

            // 接任务
            int iMissionProvider = -1;
            if (m_textBoxTaskProviderID.TextLength == 0 || !int.TryParse(m_textBoxTaskProviderID.Text, out iMissionProvider))
            {
                MessageBox.Show("任务领取：不能为空。");
                return false;
            }

            // 任务触发类型
            bool bItemProvider = false;
            bool bRegionProvider = false;

            if (m_textBoxProviderType.Text.Equals("1"))//NPC触发
            {
                bItemProvider = false;
                bRegionProvider = false;
            }
            else if (m_textBoxProviderType.Text.Equals("2"))//道具触发
            {
                bItemProvider = true;
                bRegionProvider = false;
            }
            else if (m_textBoxProviderType.Text.Equals("3"))//区域触发
            {
                bItemProvider = false;
                bRegionProvider = true;
            }

            // 还任务
            int iMissionSubmit = -1;
            if (m_textBoxTaskSubmitID.TextLength == 0 || !int.TryParse(m_textBoxTaskSubmitID.Text, out iMissionSubmit))
            {
                MessageBox.Show("任务提交：不能为空。");
                return false;
            }

            #endregion

            #region 任务目标
            // 任务目标

            int iTargetNPC = -1;
            int iTargetArea = -1;
            //int iTargetItemBegin = -1;
            //int iTargetItemEnd = -1;
            //int iTargetMonsterBegin = -1;
            //int iTargetMonsterEnd = -1;
            int iTargetBegin = -1;
            int iTargetEnd = -1;

            // 临时变量
            String strMissionType = missiontype.Display;
            int iStartID = int.Parse(m_textBoxStartTargetID.Text);
            int iEndID = int.Parse(m_textBoxEndTargetID.Text);

            #region 计数器类
            if (strMissionType.Equals("计数器类_杀怪A类"))
            {
                iTargetBegin = iStartID;
                iTargetEnd = iEndID;
            }
            else if (strMissionType.Equals("计数器类_收集物品A类"))
            {
                iTargetBegin = iStartID;
                iTargetEnd = iEndID;
            }
            else if (strMissionType.Equals("计数器类_杀人A类"))
            {
                //MessageBox.Show("该版本暂时不支持此类任务。");
                //return false;
                iTargetBegin = iStartID;
                iTargetEnd = iEndID;
            }
            else if (strMissionType.Equals("计数器类_交友A类"))
            {
                //MessageBox.Show("该版本暂时不支持此类任务。");
                //return false;
                iTargetBegin = iStartID;
                iTargetEnd = iEndID;
            }
            else if (strMissionType.Equals("计数器类_答题A类"))
            {
                iTargetBegin = iStartID;
                iTargetEnd = iEndID;
            }
            else if (strMissionType.Equals("计数器类_道具1型A类"))
            {
                iTargetBegin = iStartID;
                iTargetEnd = iEndID;
            }
            else if (strMissionType.Equals("计数器类_道具2型A类"))
            {
                iTargetBegin = iStartID;
                iTargetEnd = iEndID;
            }
            #endregion

            #region 对话类
            else if (strMissionType.Equals("对话类_单纯对话A类"))
            {

            }
            else if (strMissionType.Equals("对话类_单纯对话B类"))
            {
                iTargetNPC = iStartID;
            }
            else if (strMissionType.Equals("对话类_送物品A类"))
            {
                iTargetBegin = iEndID;
                iTargetEnd = iTargetBegin;
            }
            else if (strMissionType.Equals("对话类_送物品B类"))
            {
                iTargetNPC = iStartID;
                iTargetBegin = iEndID;
                iTargetEnd = iTargetBegin;
            }
            else if (strMissionType.Equals("对话类_取物品B类"))
            {
                iTargetNPC = iStartID;
                iTargetBegin = iEndID;
                iTargetEnd = iTargetBegin;
            }
            #endregion

            #region 护送类
            else if (strMissionType.Equals("护送类_护送A类"))
            {
                //MessageBox.Show("该版本暂时不支持此类任务。");
                //return false;
                iTargetNPC = iStartID;
            }
            #endregion

            #region 探索类
            else if (strMissionType.Equals("探索类_探索B类"))
            {
                iTargetArea = iStartID;
            }
            #endregion

            #endregion

            // 追踪名称，目标值
            String strTraceName = "";
            int iTargetValue = -1;
            if (m_listViewTaskTarget.Items.Count > 0)
            {
                ListViewItem lv = m_listViewTaskTarget.Items[0];
                String strNameValue = lv.SubItems[1].Text;
                int iIndex = strNameValue.IndexOf("x");
                String strName = strNameValue.Substring(0, iIndex);
                String strValue = strNameValue.Substring(iIndex + 1, strNameValue.Length - iIndex - 1);
                strTraceName = strName;
                iTargetValue = int.Parse(strValue);
            }



            //int iTargetType = 0;
            //if (m_listViewTaskTarget.Items.Count > 0)
            //{
            //    ListViewItem lv = m_listViewTaskTarget.Items[0];

            //    // 类型
            //    String strType = lv.SubItems[0].Text;
            //    strType = strType.Replace("目标", "");
            //    if (strType.Equals("NPC"))
            //    {
            //        //iTargetType = 1;
            //        iTargetNPC = int.Parse(m_textBoxStartTargetID.Text);
            //    }
            //    else if (strType.Equals("怪物"))
            //    {
            //        //iTargetType = 4;
            //        iTargetMonsterBegin = int.Parse(m_textBoxStartTargetID.Text);
            //        iTargetMonsterEnd = int.Parse(m_textBoxEndTargetID.Text);
            //    }
            //    else if (strType.Equals("道具"))
            //    {
            //        //iTargetType = 2;
            //        iTargetItemBegin = int.Parse(m_textBoxStartTargetID.Text);
            //        iTargetItemEnd = int.Parse(m_textBoxEndTargetID.Text);
            //    }
            //    else if (strType.Equals("区域"))
            //    {
            //        //iTargetType = 3;
            //        iTargetArea = int.Parse(m_textBoxStartTargetID.Text);
            //    }

            //    // 追踪名称，目标值
            //    String strNameValue = lv.SubItems[1].Text;
            //    int iIndex = strNameValue.IndexOf("x");
            //    String strName = strNameValue.Substring(0, iIndex);
            //    String strValue = strNameValue.Substring(iIndex + 1, strNameValue.Length - iIndex - 1);
            //    strTraceName = strName;
            //    iTargetValue = int.Parse(strValue);
            //}
            //else
            //{
            //    MessageBox.Show("任务目标：不能为空。");
            //    return false;
            //}

            // 时间限制
            int iMissionTimeLimit = GetTimeLimit();
            if (iMissionTimeLimit == -1)
            {
                return false;
            }

            // 三种任务属性
            bool bMissionCircle = m_checkBoxCircleTask.Checked;
            bool bMissionShare = m_checkBoxShareTask.Checked;
            bool bMissionAbandon = m_checkBoxAbandonTask.Checked;

            #endregion

            #region 保存前的判断与修改

            // 判断是否已经存在
            if (m_bNewMission && m_MissionMgr.FindByID(iMissionID) != null)
            {
                MessageBox.Show("已存在相同ID的任务， " + iMissionID.ToString());
                return false;
            }

            #endregion

            #region 判断 mission 原相关 NPC 是否有改变。如果发生改变 断掉原 NPC 状态图关于本 mission 的信息。

            if (mission.Provider > 0 && mission.Provider != iMissionProvider || !mission.FlowType.Equals(missiontype.FlowType))
            {
                // 获取 Provider 状态图。删除本  mission 信息
                String strProviderDiaName = null;
                if (mission.ItemProvider)
                {
                    int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_ITEM, mission.Provider);
                    strProviderDiaName = OptionManager.ItemPrefix + iProviderStateDiaID.ToString();
                }
                else if (mission.RegionProvider)
                {
                    int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_AREA, mission.Provider);
                    strProviderDiaName = OptionManager.AreaPrefix + iProviderStateDiaID.ToString();
                }
                else
                {
                    int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, mission.Provider);
                    strProviderDiaName = OptionManager.NPCPrefix + iProviderStateDiaID.ToString();
                }

                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strProviderDiaName);
                if (dia != null)
                {
                    //dia.RemoveStateByMissionID(mission.ID);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_GET);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_GET_HINT);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                    StateDiagramManager.SaveStateDiagram(dia);
                }
            }

            if (mission.TargetNPC > 0 && mission.TargetNPC != iTargetNPC || !mission.FlowType.Equals(missiontype.FlowType))
            {
                // 获取 Provider 状态图。删除本  mission 信息
                int iTargetNPCStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, mission.TargetNPC);
                String strTargetNPCDiaName = OptionManager.NPCPrefix + iTargetNPCStateDiaID.ToString();
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strTargetNPCDiaName);
                if (dia != null)
                {
                    //dia.RemoveStateByMissionID(mission.ID);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                    StateDiagramManager.SaveStateDiagram(dia);
                }
            }

            if (mission.TargetArea > 0 && mission.TargetArea != iTargetArea || !mission.FlowType.Equals(missiontype.FlowType))
            {
                // 获取 Provider 状态图。删除本  mission 信息
                int iTargetAreaStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_AREA, mission.TargetArea);
                String strTargetAreaDiaName = OptionManager.AreaPrefix + iTargetAreaStateDiaID.ToString();
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strTargetAreaDiaName);
                if (dia != null)
                {
                    //dia.RemoveStateByMissionID(mission.ID);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                    StateDiagramManager.SaveStateDiagram(dia);
                }
            }

            if (mission.SubmitTo > 0 && mission.SubmitTo != iMissionSubmit)
            {
                // 获取 SubmitTo 状态图。删除本  mission 信息
                int iSubmitToStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, mission.SubmitTo);
                String strSubmitToDiaName = OptionManager.NPCPrefix + iSubmitToStateDiaID.ToString();
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strSubmitToDiaName);
                if (dia != null)
                {
                    //dia.RemoveStateByMissionID(mission.ID);
                    dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_SUBMIT);
                    StateDiagramManager.SaveStateDiagram(dia);
                }
            }
            #endregion

            #region 将属性写入mission

            mission.ID = iMissionID;
            mission.Name = strMissionName;
            mission.LeadMap = iMissionMapID;
            mission.Class = iMissionClass;
            mission.Level = iMissionLevel;
            mission.StarLevel = iMissionStarLevel;

            mission.Type = missiontype.MainType;
            mission.SubMissionType = missiontype.SubType;
            mission.FlowType = missiontype.FlowType;

            mission.Provider = iMissionProvider;
            mission.SubmitTo = iMissionSubmit;
            mission.ItemProvider = bItemProvider;
            mission.RegionProvider = bRegionProvider;

            mission.TargetNPC = iTargetNPC;
            mission.TargetArea = iTargetArea;
            //mission.TargetItemBegin = iTargetItemBegin;
            //mission.TargetItemEnd = iTargetItemEnd;
            //mission.TargetMonsterBegin = iTargetMonsterBegin;
            //mission.TargetMonsterEnd = iTargetMonsterEnd;
            mission.TargetBegin = iTargetBegin;
            mission.TargetEnd = iTargetEnd;

            mission.TrackName = strTraceName;
            mission.TargetValue = iTargetValue;

            mission.RecordReserve = iMissionRecordReserve;
            mission.TimeLimit = iMissionTimeLimit;

            mission.Repeat = bMissionCircle;
            mission.Share = bMissionShare;
            mission.GiveUp = bMissionAbandon;

            #endregion

            return true;
        }

        private void SaveMissionCTAGroup(Mission mission)
        {
            if (mission != null)
            {
                // 任务条件
                mission.Conditions.Clear();
                foreach (ListViewItem clv in m_listViewTaskCondition.Items)
                {
                    String strName = clv.SubItems[0].Text;
                    String strValue = clv.SubItems[1].Text;
                    mission.Conditions.Add(new MissionCondition(strName, strValue));
                }

                // 任务奖励
                mission.FixedAwards.Clear();
                foreach (ListViewItem alv in m_listViewTaskAward.Items)
                {
                    String strName = alv.SubItems[0].Text;
                    String strValue = alv.SubItems[1].Text;
                    mission.FixedAwards.Add(new MissionAward(strName, strValue));
                }
            }
        }

        private void SaveMissionDescGroup(Mission mission)
        {
            if (mission != null)
            {
                mission.SimpleDesc = m_SimpleDescEdit.DescText;
                mission.GetDescription = m_GetDescEdit.DescText;
                mission.SubmitDescription = m_SubmitDescEdit.DescText;
                mission.GetHint = m_GetHintEdit.DescText;
            }
        }

        private String GetObjNameByID(int iID, int iType)
        {
            if (iType == 2)  // 接任务对象名称
            {

                Item item = ItemManager.Instance.GetItemByID(iID);
                if (item != null)
                {
                    return item.Name;
                }
            }
            else if (iType == 3)
            {

                RegionGroup group = RegionManager.Instance.GetGroupByID(iID);
                if (group != null)
                {
                    return group.Name;
                }
            }
            else
            {
                NPCInfo npc = NPCManager.Instance.GetNPCInfoByID(iID);
                if (npc != null)
                {
                    return npc.Name;
                }
            }

            return "";
        }

        private void m_checkBoxTimeLimit_CheckedChanged(object sender, EventArgs e)
        {
            m_textBoxTimeLimit.Enabled = m_checkBoxTimeLimit.Checked;
        }

        private void m_radioButtonTaskSave_CheckedChanged(object sender, EventArgs e)
        {
            m_textBoxSaveDays.Enabled = m_radioButtonDailySave.Checked;
            m_textBoxSaveTimes.Enabled = m_radioButtonTimesSave.Checked;
        }

        private void m_buttonSaveTask_Click(object sender, EventArgs e)
        {
            if (m_CurrMission != null)
            {
                DialogResult result = MessageBox.Show("是否保存任务？", "", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    if (SaveMission(m_CurrMission))
                    {
                        MessageBox.Show("任务保存成功。");
                    }
                    else
                    {
                        MessageBox.Show("保存失败！");
                    }
                }
            }
            else
            {
                MessageBox.Show("请先选择或新建一个任务。");
            }

        }

        private void m_buttonNewTask_Click(object sender, EventArgs e)
        {
            // 如果有新建的任务，提示保存
            if (m_CurrMission != null)
            {
                DialogResult result = MessageBox.Show("新建任务前，是否保存正在编辑的任务？", "", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    SaveMission(m_CurrMission);
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }

            // 新建的任务
            m_CurrMission = new Mission();
            m_bNewMission = true;
            // 刷新UI
            InitUIDefaultValue();
        }

        private void m_buttonNPCDiagram_Click(object sender, EventArgs e)
        {
            if (m_CurrMission != null)
            {
                DialogResult result = MessageBox.Show("是否转换为状态图？", "", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    if (SaveMission(m_CurrMission))
                    {
                        StateDiagramConvertor convertor = new StateDiagramConvertor();
                        convertor.ConvertMission(m_CurrMission);
                        MainForm.Instance.Canvas.Controls.Clear();
                        MainForm.Instance.RefreshStateDiagaramList();
                        MessageBox.Show("状态图转换完毕！");
                    }
                    else
                    {
                        MessageBox.Show("保存当前任务失败！无法转换为状态图。");
                    }
                }
            }
            else
            {
                MessageBox.Show("请先选择或新建一个任务。");
            }
        }

        private void m_buttonOutLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "配置文件 (*.xml)|*.xml||";
            dlg.InitialDirectory = Application.StartupPath;
            dlg.RestoreDirectory = true;
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                m_MissionMgr.LoadFromExternal(dlg.FileName);
            }
        }

        private void m_textBoxFilterTaskName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                m_buttonFilter_Click(sender, e);
            }
        }

        private void m_listViewTaskList_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (m_listViewTaskList.SelectedItems.Count > 0)
            {
                // 显示
                if (e.KeyChar == 13)
                {
                    m_listViewTaskList_DoubleClick(sender, e);
                }
            }
        }

        private void m_buttonTaskSubmit_Click(object sender, EventArgs e)
        {
            if (m_panelShowObjSel.Visible)
            {
                m_panelShowObjSel.Hide();
            }
            else
            {
                m_textBoxTempTaskProvider.Text = m_textBoxTaskProvider.Text;
                m_textBoxTempTaskSubmit.Text = m_textBoxTaskSubmit.Text;

                m_textBoxTempTaskProviderID.Text = m_textBoxTaskProviderID.Text;
                m_textBoxTempTaskSubmitID.Text = m_textBoxTaskSubmitID.Text;

                m_textBoxTempProviderType.Text = m_textBoxProviderType.Text;

                m_panelShowObjSel.Left = 20;
                m_panelShowObjSel.Top = 180;
                m_panelShowObjSel.Show();
            }
        }

        private void m_buttonCloseObjSel_Click(object sender, EventArgs e)
        {
            m_panelShowObjSel.Hide();
        }

        private void m_textBoxTempTaskProvider_DragDrop(object sender, DragEventArgs e)
        {
            ListViewItem lv = (ListViewItem)e.Data.GetData("System.Windows.Forms.ListViewItem");

            m_textBoxTempTaskProvider.Text = lv.SubItems[1].Text;
            m_textBoxTempTaskProviderID.Text = lv.SubItems[0].Text;

            m_textBoxTempProviderType.Text = lv.Tag.ToString();
        }

        private void m_textBox_ListViewItem_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("System.Windows.Forms.ListViewItem"))
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void m_textBoxTempTaskSubmit_DragDrop(object sender, DragEventArgs e)
        {
            ListViewItem lv = (ListViewItem)e.Data.GetData("System.Windows.Forms.ListViewItem");

            m_textBoxTempTaskSubmit.Text = lv.SubItems[1].Text;
            m_textBoxTempTaskSubmitID.Text = lv.SubItems[0].Text;
        }

        private void m_textBoxTempStartTarget_DragDrop(object sender, DragEventArgs e)
        {
            ListViewItem lv = (ListViewItem)e.Data.GetData("System.Windows.Forms.ListViewItem");

            m_textBoxTempStartTarget.Text = lv.SubItems[1].Text;
            m_textBoxTempStartTargetID.Text = lv.SubItems[0].Text;

            m_textBoxTempTraceName.Text = m_textBoxTempStartTargetID.Text;

            int iType = int.Parse(lv.Tag.ToString());
            if (iType == 1)  // 1为NPC 4为怪物 无法区分不做改变
            {
                int iNPCType = int.Parse(m_textBoxTempStartTargetID.Text.Substring(0, 1));
                if (iNPCType == 1)
                {
                    m_comboBoxTempTaskTargetType.SelectedIndex = 0;
                    m_textBoxTempTargetValue.Text = "1";
                }
                else if (iNPCType == 2)
                {
                    m_comboBoxTempTaskTargetType.SelectedIndex = 3;
                    m_textBoxTempTargetValue.Text = "1";
                }
            }
            else
            {
                m_comboBoxTempTaskTargetType.SelectedIndex = iType - 1;
                m_textBoxTempTargetValue.Text = "1";
            }
        }

        private void m_textBoxTempEndTarget_DragDrop(object sender, DragEventArgs e)
        {
            ListViewItem lv = (ListViewItem)e.Data.GetData("System.Windows.Forms.ListViewItem");

            m_textBoxTempEndTarget.Text = lv.SubItems[1].Text;
            m_textBoxTempEndTargetID.Text = lv.SubItems[0].Text;
        }

        private void m_buttonResetObjSel_Click(object sender, EventArgs e)
        {
            m_textBoxTempTaskProvider.Text = "";
            m_textBoxTempTaskSubmit.Text = "";

            m_textBoxTempTaskProviderID.Text = "";
            m_textBoxTempTaskSubmitID.Text = "";

            m_textBoxTempProviderType.Text = "";
        }

        private void m_buttonAppObjSel_Click(object sender, EventArgs e)
        {
            m_textBoxTaskProvider.Text = m_textBoxTempTaskProvider.Text;
            m_textBoxTaskSubmit.Text = m_textBoxTempTaskSubmit.Text;

            m_textBoxTaskProviderID.Text = m_textBoxTempTaskProviderID.Text;
            m_textBoxTaskSubmitID.Text = m_textBoxTempTaskSubmitID.Text;

            m_textBoxProviderType.Text = m_textBoxTempProviderType.Text;

            m_panelShowObjSel.Hide();
        }

        private void m_textBoxTaskProvider_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_textBoxTaskProvider.Focused)
            {
                String strProviderType = "";

                if (m_textBoxProviderType.Text.Equals("1"))//NPC触发
                {
                    strProviderType = "NPC触发";
                }
                else if (m_textBoxProviderType.Text.Equals("2"))//道具触发
                {
                    strProviderType = "道具触发";
                }
                else if (m_textBoxProviderType.Text.Equals("3"))//区域触发
                {
                    strProviderType = "区域触发";
                }

                m_toolTipTaskDetail.SetToolTip(m_textBoxTaskProvider, strProviderType + m_textBoxTaskProviderID.Text);
            }
            else
            {
                m_toolTipTaskDetail.RemoveAll();
            }
        }

        private void m_textBoxTaskSubmit_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_textBoxTaskSubmit.Focused)
            {
                m_toolTipTaskDetail.SetToolTip(m_textBoxTaskSubmit, m_textBoxTaskSubmitID.Text);
            }
            else
            {
                m_toolTipTaskDetail.RemoveAll();
            }
        }

        private void m_textBoxTempTaskProvider_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_textBoxTempTaskProvider.Focused)
            {
                String strProviderType = "";

                if (m_textBoxTempProviderType.Text.Equals("1"))//NPC触发
                {
                    strProviderType = "NPC触发";
                }
                else if (m_textBoxTempProviderType.Text.Equals("2"))//道具触发
                {
                    strProviderType = "道具触发";
                }
                else if (m_textBoxTempProviderType.Text.Equals("3"))//区域触发
                {
                    strProviderType = "区域触发";
                }

                m_toolTipTaskDetail.SetToolTip(m_textBoxTempTaskProvider, strProviderType + m_textBoxTempTaskProviderID.Text);
            }
            else
            {
                m_toolTipTaskDetail.RemoveAll();
            }
        }

        private void m_textBoxTempTaskSubmit_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_textBoxTempTaskSubmit.Focused)
            {
                m_toolTipTaskDetail.SetToolTip(m_textBoxTempTaskSubmit, m_textBoxTempTaskSubmitID.Text);
            }
            else
            {
                m_toolTipTaskDetail.RemoveAll();
            }
        }

        private void m_buttonSetTaskTarget_Click(object sender, EventArgs e)
        {
            if (m_panelShowTaskTargetSel.Visible)
            {
                m_panelShowTaskTargetSel.Hide();
            }
            else
            {

                //m_textBoxTempStartTargetID.Text = m_textBoxStartTargetID.Text;
                //m_textBoxTempEndTargetID.Text = m_textBoxEndTargetID.Text;

                //if (m_listViewTaskTarget.Items.Count > 0)
                //{
                //    ListViewItem lv = m_listViewTaskTarget.Items[0];

                //    // 类型
                //    String strType = lv.SubItems[0].Text;
                //    strType = strType.Replace("目标", "");
                //    m_comboBoxTempTaskTargetType.SelectedItem = strType;

                //    // 追踪名称，目标值
                //    String strNameValue = lv.SubItems[1].Text;
                //    int iIndex = strNameValue.IndexOf("x");
                //    String strName = strNameValue.Substring(0, iIndex);
                //    String strValue = strNameValue.Substring(iIndex + 1, strNameValue.Length - iIndex - 1);
                //    m_textBoxTempTraceName.Text = strName;
                //    m_textBoxTempTargetValue.Text = strValue;

                //    // 名称显示
                //    int iTargetType = m_comboBoxTempTaskTargetType.SelectedIndex + 1;
                //    int iStartID = -1;
                //    int iEndID = -1;
                //    if (int.TryParse(m_textBoxTempStartTargetID.Text, out iStartID))
                //    {
                //        m_textBoxTempStartTarget.Text = GetObjNameByID(iStartID, iTargetType);
                //    }
                //    if (int.TryParse(m_textBoxTempEndTargetID.Text, out iEndID))
                //    {
                //        m_textBoxTempEndTarget.Text = GetObjNameByID(iEndID, iTargetType);
                //    }
                //}
                //else
                //{
                //    m_comboBoxTempTaskTargetType.SelectedIndex = -1;

                //    m_textBoxTempTraceName.Text = "";
                //    m_textBoxTempTargetValue.Text = "1";
                //}

                if (InitTaskTargetSelPanel())
                {
                    m_panelShowTaskTargetSel.Left = 20;
                    m_panelShowTaskTargetSel.Top = 280;
                    m_panelShowTaskTargetSel.Show();
                }
            }
        }

        private void m_buttonCloseTaskTargetSel_Click(object sender, EventArgs e)
        {
            m_panelShowTaskTargetSel.Hide();
        }

        private void m_buttonAppTaskTargetSel_Click(object sender, EventArgs e)
        {
            if (m_comboBoxTempTaskTargetType.SelectedItem == null)
            {
                MessageBox.Show("请选择目标类型。");
            }
            else
            {
                m_textBoxStartTargetID.Text = m_textBoxTempStartTargetID.Text;
                m_textBoxEndTargetID.Text = m_textBoxTempEndTargetID.Text;

                m_listViewTaskTarget.Items.Clear();
                String strName = "目标" + m_comboBoxTempTaskTargetType.SelectedItem.ToString();
                String strValue = m_textBoxTempTraceName.Text + "x" + m_textBoxTempTargetValue.Text;
                AddToListView(m_listViewTaskTarget, strName, strValue);
                m_panelShowTaskTargetSel.Hide();
            }
        }

        private void m_buttonResetTaskTargetSel_Click(object sender, EventArgs e)
        {
            m_textBoxTempStartTarget.Text = "";
            m_textBoxTempEndTarget.Text = "";

            m_textBoxTempStartTargetID.Text = "";
            m_textBoxTempEndTargetID.Text = "";

            m_comboBoxTempTaskTargetType.SelectedIndex = -1;

            m_textBoxTempTraceName.Text = "";
            m_textBoxTempTargetValue.Text = "1";

        }

        private void m_textBoxTempStartTarget_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_textBoxTempStartTarget.Focused)
            {
                m_toolTipTaskDetail.SetToolTip(m_textBoxTempStartTarget, m_textBoxTempStartTargetID.Text);
            }
            else
            {
                m_toolTipTaskDetail.RemoveAll();
            }
        }

        private void m_textBoxTempEndTarget_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_textBoxTempEndTarget.Focused)
            {
                m_toolTipTaskDetail.SetToolTip(m_textBoxTempEndTarget, m_textBoxTempEndTargetID.Text);
            }
            else
            {
                m_toolTipTaskDetail.RemoveAll();
            }
        }

        private void m_buttonSetTaskCondition_Click(object sender, EventArgs e)
        {
            m_TaskConditionSel.InitDataBase(m_TaskConditionDB, m_listViewTaskCondition);
            m_panelShowTaskConditionSel.Left = 10;
            m_panelShowTaskConditionSel.Top = 265;
            m_panelShowTaskConditionSel.Show();
        }

        private void m_buttonSetTaskAward_Click(object sender, EventArgs e)
        {
            m_TaskConditionSel.InitDataBase(m_TaskAwardDB, m_listViewTaskAward);
            m_panelShowTaskConditionSel.Left = 10;
            m_panelShowTaskConditionSel.Top = 265;
            m_panelShowTaskConditionSel.Show();
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listViewTaskList.SelectedItems.Count > 0)
            {
                for (int iIndex = 0; iIndex < m_listViewTaskList.SelectedItems.Count; iIndex++)
                {
                    // 获取ID
                    ListViewItem lv = m_listViewTaskList.SelectedItems[iIndex];
                    int iMissionID = int.Parse(lv.SubItems[3].Text);

                    // 获取任务
                    Mission delMission = m_MissionMgr.FindByID(iMissionID);

                    // 删除任务
                    if (delMission != null)
                    {
                        //DialogResult result = MessageBox.Show("是否删除任务" + iMissionID.ToString(), "", MessageBoxButtons.YesNo);
                        //if (result == DialogResult.Yes)
                        {
                            // 判断是否是正在编辑任务
                            if (m_CurrMission != null)
                            {
                                if (m_CurrMission.ID == iMissionID)
                                {
                                    m_CurrMission = null;
                                    m_bNewMission = false;

                                    // 恢复UI
                                    InitUIDefaultValue();
                                }
                            }

                            // 从任务列表中移除
                            m_listViewTaskList.Items.Remove(lv);

                            // 删除任务关联的状态图
                            RemoveDiaByMission(delMission);

                            // 从任务管理器中删除
                            m_MissionMgr.RemoveMission(delMission);

                            // 写入文件
                            m_MissionMgr.SaveMissions(OptionManager.MissionPath);

                            // 删除服务器上的版本
                            TeamWorkManager.Instance.Remove(iMissionID);

                            // 状态图界面更新
                            MainForm.Instance.RefreshMissionTree();
                            MainForm.Instance.ShowStateDiagram(null);
                        }
                    }
                }
            }
        }

        private void UpdateMissionStatus(ListViewItem lv, String strMark)
        {
            if (lv != null)
            {
                lv.SubItems[0].Text = strMark;
            }
        }

        private void 签出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listViewTaskList.SelectedItems.Count > 0)
            {
                for (int iIndex = 0; iIndex < m_listViewTaskList.SelectedItems.Count; iIndex++)
                {
                    // 获取ID
                    ListViewItem lv = m_listViewTaskList.SelectedItems[iIndex];
                    int iMissionID = int.Parse(lv.SubItems[3].Text);

                    // 获取任务
                    Mission mission = m_MissionMgr.FindByID(iMissionID);

                    // 更新状态
                    if (mission != null)
                    {
                        int iresult = TeamWorkManager.Instance.CheckOut(iMissionID);
                        mission.TeamWorkStatus = iresult;
                        if (iresult == -1)
                        {
                            MessageBox.Show("服务器正忙或路径配置错误。");
                        }
                        else if (iresult == 0)
                        {
                            MessageBox.Show("任务不存在！请先导入任务");
                            mission.TeamWorkStatus = iresult;
                        }
                        else if (iresult == 3)
                        {
                            String strLocker = TeamWorkManager.Instance.GetMissionStatus(iMissionID).Msg;
                            MessageBox.Show("任务已被" + strLocker + "锁住");
                            mission.TeamWorkStatus = iresult;
                        }
                        else
                        {
                            mission.TeamWorkStatus = 2;
                            UpdateLatestMissionVersion(mission);
                        }

                        UpdateMissionStatus(lv, mission.TeamWorkMark);
                    }
                }
            }
        }

        private void 签入ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listViewTaskList.SelectedItems.Count > 0)
            {
                for (int iIndex = 0; iIndex < m_listViewTaskList.SelectedItems.Count; iIndex++)
                {
                    // 获取ID
                    ListViewItem lv = m_listViewTaskList.SelectedItems[iIndex];
                    int iMissionID = int.Parse(lv.SubItems[3].Text);

                    // 获取任务
                    Mission mission = m_MissionMgr.FindByID(iMissionID);

                    // 更新状态
                    if (mission != null)
                    {
                        int iresult = TeamWorkManager.Instance.CheckIn(mission);
                        mission.TeamWorkStatus = iresult;
                        if (iresult == -1)
                        {
                            MessageBox.Show("服务器正忙或路径配置错误。");
                        }
                        else if (iresult == 0)
                        {
                            MessageBox.Show("任务不存在！请先导入任务");
                            mission.TeamWorkStatus = iresult;
                        }
                        else if (iresult == 1)
                        {
                            MessageBox.Show("请先签出任务！");
                            mission.TeamWorkStatus = iresult;
                        }
                        else if (iresult == 3)
                        {
                            String strLocker = TeamWorkManager.Instance.GetMissionStatus(iMissionID).Msg;
                            MessageBox.Show("任务已被" + strLocker + "锁住");
                            mission.TeamWorkStatus = iresult;
                        }
                        else
                        {
                            mission.TeamWorkStatus = 1;
                        }

                        UpdateMissionStatus(lv, mission.TeamWorkMark);
                    }
                }
            }
        }

        private void 撤销更改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listViewTaskList.SelectedItems.Count > 0)
            {
                for (int iIndex = 0; iIndex < m_listViewTaskList.SelectedItems.Count; iIndex++)
                {
                    // 获取ID
                    ListViewItem lv = m_listViewTaskList.SelectedItems[iIndex];
                    int iMissionID = int.Parse(lv.SubItems[3].Text);

                    // 获取任务
                    Mission mission = m_MissionMgr.FindByID(iMissionID);

                    // 更新状态
                    if (mission != null)
                    {
                        int iresult = TeamWorkManager.Instance.Repeal(iMissionID);
                        mission.TeamWorkStatus = iresult;
                        if (iresult == -1)
                        {
                            MessageBox.Show("服务器正忙或路径配置错误。");
                        }
                        else if (iresult == 0)
                        {
                            MessageBox.Show("任务不存在！请先导入任务");
                            mission.TeamWorkStatus = iresult;
                        }
                        else if (iresult == 1)
                        {
                            MessageBox.Show("请先签出任务！");
                            mission.TeamWorkStatus = iresult;
                        }
                        else if (iresult == 3)
                        {
                            String strLocker = TeamWorkManager.Instance.GetMissionStatus(iMissionID).Msg;
                            MessageBox.Show("任务已被" + strLocker + "锁住");
                            mission.TeamWorkStatus = iresult;
                        }
                        else
                        {
                            mission.TeamWorkStatus = 1;
                            UpdateLatestMissionVersion(mission);
                        }

                        UpdateMissionStatus(lv, mission.TeamWorkMark);
                    }
                }
            }
        }

        private void 导入新任务ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listViewTaskList.SelectedItems.Count > 0)
            {
                for (int iIndex = 0; iIndex < m_listViewTaskList.SelectedItems.Count; iIndex++)
                {
                    // 获取ID
                    ListViewItem lv = m_listViewTaskList.SelectedItems[iIndex];
                    int iMissionID = int.Parse(lv.SubItems[3].Text);

                    // 获取任务
                    Mission mission = m_MissionMgr.FindByID(iMissionID);

                    // 更新状态
                    if (mission != null)
                    {
                        int iresult = TeamWorkManager.Instance.Import(mission);
                        mission.TeamWorkStatus = iresult;
                        if (iresult == -1)
                        {
                            MessageBox.Show("服务器正忙或路径配置错误。");
                        }
                        else if (iresult == 0)
                        {
                            //MessageBox.Show("导入成功！");
                            mission.TeamWorkStatus = 1;
                        }
                        else
                        {
                            MessageBox.Show("任务已存在！");
                            mission.TeamWorkStatus = iresult;
                        }

                        UpdateMissionStatus(lv, mission.TeamWorkMark);
                    }
                }
            }
        }

        private void 获取最新版本ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listViewTaskList.SelectedItems.Count > 0)
            {
                for (int iIndex = 0; iIndex < m_listViewTaskList.SelectedItems.Count; iIndex++)
                {
                    // 获取ID
                    ListViewItem lv = m_listViewTaskList.SelectedItems[iIndex];
                    int iMissionID = int.Parse(lv.SubItems[3].Text);

                    // 获取任务
                    Mission mission = m_MissionMgr.FindByID(iMissionID);

                    // 更新状态
                    if (mission != null)
                    {
                        UpdateLatestMissionVersion(mission);

                        UpdateMissionStatus(lv, mission.TeamWorkMark);

                        //MessageBox.Show("获取最新版本成功！");
                    }
                    else
                    {
                        MessageBox.Show("任务不存在！");
                    }
                }
            }
        }

        private void UpdateLatestMissionVersion(Mission mission)
        {
            if (mission != null)
            {
                Mission latestMission = TeamWorkManager.Instance.GetLatestVersion(mission.ID);

                if (latestMission != null)
                {
                    // 更新任务，注意签出的任务不用更新
                    mission.TeamWorkStatus = latestMission.TeamWorkStatus;
                    if(mission.TeamWorkStatus != 2)
                    {
                        mission.Copy(latestMission);
                    }
                    

                    // 刷新界面
                    if (m_CurrMission != null && m_CurrMission.ID == mission.ID)
                    {
                        ShowMission(mission);
                    }
                }
            }
        }

        private void m_listViewTaskList_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            m_listViewTaskList.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }

        private void m_textBoxTaskType_TextChanged(object sender, EventArgs e)
        {
            // 接任务追踪
            if (m_textBoxTaskType.Text.Equals("对话类_单纯对话A类") || m_textBoxTaskType.Text.Equals("对话类_送物品A类"))
            {
                //m_GetHintEdit.DescText = "";
                m_GetHintEdit.Visible = false;
            }
            else
            {
                //m_GetHintEdit.DescText = m_CurrMission.GetHint;
                m_GetHintEdit.Visible = true;
            }
        }

    }

    class ListViewItemComparer : IComparer
    {
        private int col;

        public ListViewItemComparer()
        {
            col = 0;
        }

        public ListViewItemComparer(int column)
        {
            col = column;
        }

        public int Compare(object x, object y)
        {
            return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
        }
    };
}