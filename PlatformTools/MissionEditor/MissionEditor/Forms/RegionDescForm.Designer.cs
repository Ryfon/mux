﻿using System.Xml;
using System.Collections;


namespace MissionEditor
{
    partial class RegionDescForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.m_buttonLoadMusicLib = new System.Windows.Forms.Button();
            this.m_comboBoxSubMusic3 = new System.Windows.Forms.ComboBox();
            this.m_comboBoxSubMusic2 = new System.Windows.Forms.ComboBox();
            this.m_comboBoxSubMusic1 = new System.Windows.Forms.ComboBox();
            this.m_comboBoxMainMusic = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_dataGridViewSubMusic3 = new System.Windows.Forms.DataGridView();
            this.m_dataGridViewSubMusic1 = new System.Windows.Forms.DataGridView();
            this.m_dataGridViewSubMusic2 = new System.Windows.Forms.DataGridView();
            this.m_dataGridViewMainMusic = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.m_buttonLoadSoundLib = new System.Windows.Forms.Button();
            this.m_comboBoxSubSound2 = new System.Windows.Forms.ComboBox();
            this.m_comboBoxSubSound1 = new System.Windows.Forms.ComboBox();
            this.m_comboBoxMainSound = new System.Windows.Forms.ComboBox();
            this.m_checkBoxEnableEffect = new System.Windows.Forms.CheckBox();
            this.m_checkBoxEnableSound = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.m_dataGridViewEffect = new System.Windows.Forms.DataGridView();
            this.m_dataGridViewSubSound1 = new System.Windows.Forms.DataGridView();
            this.m_dataGridViewSubSound2 = new System.Windows.Forms.DataGridView();
            this.m_dataGridViewMainSound = new System.Windows.Forms.DataGridView();
            this.m_buttonExit = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubMusic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubMusic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubMusic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewMainMusic)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewEffect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubSound1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubSound2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewMainSound)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(840, 520);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.m_buttonLoadMusicLib);
            this.tabPage1.Controls.Add(this.m_comboBoxSubMusic3);
            this.tabPage1.Controls.Add(this.m_comboBoxSubMusic2);
            this.tabPage1.Controls.Add(this.m_comboBoxSubMusic1);
            this.tabPage1.Controls.Add(this.m_comboBoxMainMusic);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.m_dataGridViewSubMusic3);
            this.tabPage1.Controls.Add(this.m_dataGridViewSubMusic1);
            this.tabPage1.Controls.Add(this.m_dataGridViewSubMusic2);
            this.tabPage1.Controls.Add(this.m_dataGridViewMainMusic);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(832, 495);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "音乐";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // m_buttonLoadMusicLib
            // 
            this.m_buttonLoadMusicLib.Location = new System.Drawing.Point(749, 460);
            this.m_buttonLoadMusicLib.Name = "m_buttonLoadMusicLib";
            this.m_buttonLoadMusicLib.Size = new System.Drawing.Size(75, 23);
            this.m_buttonLoadMusicLib.TabIndex = 12;
            this.m_buttonLoadMusicLib.Text = "加载音乐库";
            this.m_buttonLoadMusicLib.UseVisualStyleBackColor = true;
            this.m_buttonLoadMusicLib.Click += new System.EventHandler(this.m_buttonLoadMusicLib_Click);
            // 
            // m_comboBoxSubMusic3
            // 
            this.m_comboBoxSubMusic3.FormattingEnabled = true;
            this.m_comboBoxSubMusic3.Location = new System.Drawing.Point(624, 24);
            this.m_comboBoxSubMusic3.Name = "m_comboBoxSubMusic3";
            this.m_comboBoxSubMusic3.Size = new System.Drawing.Size(200, 20);
            this.m_comboBoxSubMusic3.Sorted = true;
            this.m_comboBoxSubMusic3.TabIndex = 11;
            this.m_comboBoxSubMusic3.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxSubMusic3_SelectedIndexChanged);
            // 
            // m_comboBoxSubMusic2
            // 
            this.m_comboBoxSubMusic2.FormattingEnabled = true;
            this.m_comboBoxSubMusic2.Location = new System.Drawing.Point(418, 24);
            this.m_comboBoxSubMusic2.Name = "m_comboBoxSubMusic2";
            this.m_comboBoxSubMusic2.Size = new System.Drawing.Size(200, 20);
            this.m_comboBoxSubMusic2.Sorted = true;
            this.m_comboBoxSubMusic2.TabIndex = 10;
            this.m_comboBoxSubMusic2.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxSubMusic2_SelectedIndexChanged);
            // 
            // m_comboBoxSubMusic1
            // 
            this.m_comboBoxSubMusic1.FormattingEnabled = true;
            this.m_comboBoxSubMusic1.Location = new System.Drawing.Point(212, 24);
            this.m_comboBoxSubMusic1.Name = "m_comboBoxSubMusic1";
            this.m_comboBoxSubMusic1.Size = new System.Drawing.Size(200, 20);
            this.m_comboBoxSubMusic1.Sorted = true;
            this.m_comboBoxSubMusic1.TabIndex = 9;
            this.m_comboBoxSubMusic1.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxSubMusic1_SelectedIndexChanged);
            // 
            // m_comboBoxMainMusic
            // 
            this.m_comboBoxMainMusic.FormattingEnabled = true;
            this.m_comboBoxMainMusic.Location = new System.Drawing.Point(6, 24);
            this.m_comboBoxMainMusic.Name = "m_comboBoxMainMusic";
            this.m_comboBoxMainMusic.Size = new System.Drawing.Size(200, 20);
            this.m_comboBoxMainMusic.Sorted = true;
            this.m_comboBoxMainMusic.TabIndex = 8;
            this.m_comboBoxMainMusic.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxMainMusic_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(624, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "辅音乐3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(418, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "辅音乐2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "辅音乐1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "主音乐";
            // 
            // m_dataGridViewSubMusic3
            // 
            this.m_dataGridViewSubMusic3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewSubMusic3.Location = new System.Drawing.Point(624, 54);
            this.m_dataGridViewSubMusic3.Name = "m_dataGridViewSubMusic3";
            this.m_dataGridViewSubMusic3.RowTemplate.Height = 23;
            this.m_dataGridViewSubMusic3.Size = new System.Drawing.Size(200, 400);
            this.m_dataGridViewSubMusic3.TabIndex = 3;
            // 
            // m_dataGridViewSubMusic1
            // 
            this.m_dataGridViewSubMusic1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewSubMusic1.Location = new System.Drawing.Point(212, 54);
            this.m_dataGridViewSubMusic1.Name = "m_dataGridViewSubMusic1";
            this.m_dataGridViewSubMusic1.RowTemplate.Height = 23;
            this.m_dataGridViewSubMusic1.Size = new System.Drawing.Size(200, 400);
            this.m_dataGridViewSubMusic1.TabIndex = 2;
            // 
            // m_dataGridViewSubMusic2
            // 
            this.m_dataGridViewSubMusic2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewSubMusic2.Location = new System.Drawing.Point(418, 54);
            this.m_dataGridViewSubMusic2.Name = "m_dataGridViewSubMusic2";
            this.m_dataGridViewSubMusic2.RowTemplate.Height = 23;
            this.m_dataGridViewSubMusic2.Size = new System.Drawing.Size(200, 400);
            this.m_dataGridViewSubMusic2.TabIndex = 1;
            // 
            // m_dataGridViewMainMusic
            // 
            this.m_dataGridViewMainMusic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewMainMusic.Location = new System.Drawing.Point(6, 54);
            this.m_dataGridViewMainMusic.Name = "m_dataGridViewMainMusic";
            this.m_dataGridViewMainMusic.RowTemplate.Height = 23;
            this.m_dataGridViewMainMusic.Size = new System.Drawing.Size(200, 400);
            this.m_dataGridViewMainMusic.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.m_buttonLoadSoundLib);
            this.tabPage2.Controls.Add(this.m_comboBoxSubSound2);
            this.tabPage2.Controls.Add(this.m_comboBoxSubSound1);
            this.tabPage2.Controls.Add(this.m_comboBoxMainSound);
            this.tabPage2.Controls.Add(this.m_checkBoxEnableEffect);
            this.tabPage2.Controls.Add(this.m_checkBoxEnableSound);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.m_dataGridViewEffect);
            this.tabPage2.Controls.Add(this.m_dataGridViewSubSound1);
            this.tabPage2.Controls.Add(this.m_dataGridViewSubSound2);
            this.tabPage2.Controls.Add(this.m_dataGridViewMainSound);
            this.tabPage2.Location = new System.Drawing.Point(4, 21);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(832, 495);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "音效特效";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // m_buttonLoadSoundLib
            // 
            this.m_buttonLoadSoundLib.Location = new System.Drawing.Point(749, 463);
            this.m_buttonLoadSoundLib.Name = "m_buttonLoadSoundLib";
            this.m_buttonLoadSoundLib.Size = new System.Drawing.Size(75, 23);
            this.m_buttonLoadSoundLib.TabIndex = 16;
            this.m_buttonLoadSoundLib.Text = "加载音效库";
            this.m_buttonLoadSoundLib.UseVisualStyleBackColor = true;
            this.m_buttonLoadSoundLib.Click += new System.EventHandler(this.m_buttonLoadSoundLib_Click);
            // 
            // m_comboBoxSubSound2
            // 
            this.m_comboBoxSubSound2.FormattingEnabled = true;
            this.m_comboBoxSubSound2.Location = new System.Drawing.Point(418, 31);
            this.m_comboBoxSubSound2.Name = "m_comboBoxSubSound2";
            this.m_comboBoxSubSound2.Size = new System.Drawing.Size(200, 20);
            this.m_comboBoxSubSound2.Sorted = true;
            this.m_comboBoxSubSound2.TabIndex = 15;
            this.m_comboBoxSubSound2.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxSubSound2_SelectedIndexChanged);
            // 
            // m_comboBoxSubSound1
            // 
            this.m_comboBoxSubSound1.FormattingEnabled = true;
            this.m_comboBoxSubSound1.Location = new System.Drawing.Point(212, 31);
            this.m_comboBoxSubSound1.Name = "m_comboBoxSubSound1";
            this.m_comboBoxSubSound1.Size = new System.Drawing.Size(200, 20);
            this.m_comboBoxSubSound1.Sorted = true;
            this.m_comboBoxSubSound1.TabIndex = 14;
            this.m_comboBoxSubSound1.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxSubSound1_SelectedIndexChanged);
            // 
            // m_comboBoxMainSound
            // 
            this.m_comboBoxMainSound.FormattingEnabled = true;
            this.m_comboBoxMainSound.Location = new System.Drawing.Point(6, 31);
            this.m_comboBoxMainSound.Name = "m_comboBoxMainSound";
            this.m_comboBoxMainSound.Size = new System.Drawing.Size(200, 20);
            this.m_comboBoxMainSound.Sorted = true;
            this.m_comboBoxMainSound.TabIndex = 13;
            this.m_comboBoxMainSound.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxMainSound_SelectedIndexChanged);
            // 
            // m_checkBoxEnableEffect
            // 
            this.m_checkBoxEnableEffect.AutoSize = true;
            this.m_checkBoxEnableEffect.Location = new System.Drawing.Point(624, 9);
            this.m_checkBoxEnableEffect.Name = "m_checkBoxEnableEffect";
            this.m_checkBoxEnableEffect.Size = new System.Drawing.Size(72, 16);
            this.m_checkBoxEnableEffect.TabIndex = 12;
            this.m_checkBoxEnableEffect.Text = "启用特效";
            this.m_checkBoxEnableEffect.UseVisualStyleBackColor = true;
            this.m_checkBoxEnableEffect.CheckedChanged += new System.EventHandler(this.m_checkBoxEnableEffect_CheckedChanged);
            // 
            // m_checkBoxEnableSound
            // 
            this.m_checkBoxEnableSound.AutoSize = true;
            this.m_checkBoxEnableSound.Location = new System.Drawing.Point(6, 9);
            this.m_checkBoxEnableSound.Name = "m_checkBoxEnableSound";
            this.m_checkBoxEnableSound.Size = new System.Drawing.Size(72, 16);
            this.m_checkBoxEnableSound.TabIndex = 11;
            this.m_checkBoxEnableSound.Text = "启用音效";
            this.m_checkBoxEnableSound.UseVisualStyleBackColor = true;
            this.m_checkBoxEnableSound.CheckedChanged += new System.EventHandler(this.m_checkBoxEnableSound_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(418, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "辅音效";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(212, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "辅音效";
            // 
            // m_dataGridViewEffect
            // 
            this.m_dataGridViewEffect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewEffect.Location = new System.Drawing.Point(624, 57);
            this.m_dataGridViewEffect.Name = "m_dataGridViewEffect";
            this.m_dataGridViewEffect.RowTemplate.Height = 23;
            this.m_dataGridViewEffect.Size = new System.Drawing.Size(200, 400);
            this.m_dataGridViewEffect.TabIndex = 7;
            // 
            // m_dataGridViewSubSound1
            // 
            this.m_dataGridViewSubSound1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewSubSound1.Location = new System.Drawing.Point(212, 57);
            this.m_dataGridViewSubSound1.Name = "m_dataGridViewSubSound1";
            this.m_dataGridViewSubSound1.RowTemplate.Height = 23;
            this.m_dataGridViewSubSound1.Size = new System.Drawing.Size(200, 400);
            this.m_dataGridViewSubSound1.TabIndex = 6;
            // 
            // m_dataGridViewSubSound2
            // 
            this.m_dataGridViewSubSound2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewSubSound2.Location = new System.Drawing.Point(418, 57);
            this.m_dataGridViewSubSound2.Name = "m_dataGridViewSubSound2";
            this.m_dataGridViewSubSound2.RowTemplate.Height = 23;
            this.m_dataGridViewSubSound2.Size = new System.Drawing.Size(200, 400);
            this.m_dataGridViewSubSound2.TabIndex = 5;
            // 
            // m_dataGridViewMainSound
            // 
            this.m_dataGridViewMainSound.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataGridViewMainSound.Location = new System.Drawing.Point(6, 57);
            this.m_dataGridViewMainSound.Name = "m_dataGridViewMainSound";
            this.m_dataGridViewMainSound.RowTemplate.Height = 23;
            this.m_dataGridViewMainSound.Size = new System.Drawing.Size(200, 400);
            this.m_dataGridViewMainSound.TabIndex = 4;
            // 
            // m_buttonExit
            // 
            this.m_buttonExit.Location = new System.Drawing.Point(775, 554);
            this.m_buttonExit.Name = "m_buttonExit";
            this.m_buttonExit.Size = new System.Drawing.Size(75, 23);
            this.m_buttonExit.TabIndex = 1;
            this.m_buttonExit.Text = "保存";
            this.m_buttonExit.UseVisualStyleBackColor = true;
            this.m_buttonExit.Click += new System.EventHandler(this.m_buttonExit_Click);
            // 
            // RegionDescForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 589);
            this.ControlBox = false;
            this.Controls.Add(this.m_buttonExit);
            this.Controls.Add(this.tabControl1);
            this.Name = "RegionDescForm";
            this.Text = "RegionDescForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubMusic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubMusic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubMusic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewMainMusic)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewEffect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubSound1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewSubSound2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataGridViewMainSound)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private static RegionDescForm m_Instance = null;
        private RegionDesc m_RegionDesc = null;
        private XmlDocument m_MusicLibDoc = new XmlDocument();   // 音乐库
        private XmlDocument m_SoundLibDoc = new XmlDocument();   // 音效库
        private Hashtable m_MusicLibTable = new Hashtable();
        private Hashtable m_SoundLibTable = new Hashtable();
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView m_dataGridViewMainMusic;
        private System.Windows.Forms.DataGridView m_dataGridViewSubMusic3;
        private System.Windows.Forms.DataGridView m_dataGridViewSubMusic1;
        private System.Windows.Forms.DataGridView m_dataGridViewSubMusic2;
        private System.Windows.Forms.DataGridView m_dataGridViewEffect;
        private System.Windows.Forms.DataGridView m_dataGridViewSubSound1;
        private System.Windows.Forms.DataGridView m_dataGridViewSubSound2;
        private System.Windows.Forms.DataGridView m_dataGridViewMainSound;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox m_checkBoxEnableEffect;
        private System.Windows.Forms.CheckBox m_checkBoxEnableSound;
        private System.Windows.Forms.ComboBox m_comboBoxMainMusic;
        private System.Windows.Forms.ComboBox m_comboBoxSubMusic3;
        private System.Windows.Forms.ComboBox m_comboBoxSubMusic2;
        private System.Windows.Forms.ComboBox m_comboBoxSubMusic1;
        private System.Windows.Forms.ComboBox m_comboBoxMainSound;
        private System.Windows.Forms.ComboBox m_comboBoxSubSound2;
        private System.Windows.Forms.ComboBox m_comboBoxSubSound1;
        private System.Windows.Forms.Button m_buttonLoadMusicLib;
        private System.Windows.Forms.Button m_buttonLoadSoundLib;
        private System.Windows.Forms.Button m_buttonExit;
    }
}