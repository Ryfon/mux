﻿
namespace MissionEditor
{
    partial class HtmlEditorForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_splitContainer = new System.Windows.Forms.SplitContainer();
            this.m_textBoxHtmlCode = new System.Windows.Forms.TextBox();
            this.m_contextMenuStripHtmlTag = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.插入标签ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_webBrowserPreview = new System.Windows.Forms.WebBrowser();
            this.m_splitContainer.Panel1.SuspendLayout();
            this.m_splitContainer.Panel2.SuspendLayout();
            this.m_splitContainer.SuspendLayout();
            this.m_contextMenuStripHtmlTag.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_splitContainer
            // 
            this.m_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainer.Location = new System.Drawing.Point(0, 0);
            this.m_splitContainer.Name = "m_splitContainer";
            this.m_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // m_splitContainer.Panel1
            // 
            this.m_splitContainer.Panel1.Controls.Add(this.m_textBoxHtmlCode);
            // 
            // m_splitContainer.Panel2
            // 
            this.m_splitContainer.Panel2.Controls.Add(this.m_btnOK);
            this.m_splitContainer.Panel2.Controls.Add(this.m_webBrowserPreview);
            this.m_splitContainer.Size = new System.Drawing.Size(355, 498);
            this.m_splitContainer.SplitterDistance = 254;
            this.m_splitContainer.TabIndex = 0;
            // 
            // m_textBoxHtmlCode
            // 
            this.m_textBoxHtmlCode.ContextMenuStrip = this.m_contextMenuStripHtmlTag;
            this.m_textBoxHtmlCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_textBoxHtmlCode.Location = new System.Drawing.Point(0, 0);
            this.m_textBoxHtmlCode.Multiline = true;
            this.m_textBoxHtmlCode.Name = "m_textBoxHtmlCode";
            this.m_textBoxHtmlCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_textBoxHtmlCode.Size = new System.Drawing.Size(355, 254);
            this.m_textBoxHtmlCode.TabIndex = 0;
            this.m_textBoxHtmlCode.TextChanged += new System.EventHandler(this.m_textBoxHtmlCode_TextChanged);
            // 
            // m_contextMenuStripHtmlTag
            // 
            this.m_contextMenuStripHtmlTag.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.插入标签ToolStripMenuItem});
            this.m_contextMenuStripHtmlTag.Name = "m_contextMenuStripHtmlTag";
            this.m_contextMenuStripHtmlTag.Size = new System.Drawing.Size(314, 26);
            // 
            // 插入标签ToolStripMenuItem
            // 
            this.插入标签ToolStripMenuItem.Name = "插入标签ToolStripMenuItem";
            this.插入标签ToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.插入标签ToolStripMenuItem.Text = "<FONT SIZE=4 COLOR=\"#000000\">...</FONT>";
            this.插入标签ToolStripMenuItem.Click += new System.EventHandler(this.插入标签ToolStripMenuItem_Click);
            // 
            // m_btnOK
            // 
            this.m_btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.m_btnOK.Location = new System.Drawing.Point(151, 211);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(69, 23);
            this.m_btnOK.TabIndex = 1;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_webBrowserPreview
            // 
            this.m_webBrowserPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_webBrowserPreview.Location = new System.Drawing.Point(0, 0);
            this.m_webBrowserPreview.MinimumSize = new System.Drawing.Size(20, 20);
            this.m_webBrowserPreview.Name = "m_webBrowserPreview";
            this.m_webBrowserPreview.Size = new System.Drawing.Size(355, 205);
            this.m_webBrowserPreview.TabIndex = 0;
            // 
            // HtmlEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 498);
            this.Controls.Add(this.m_splitContainer);
            this.Name = "HtmlEditorForm";
            this.Text = "HtmlEditorForm";
            this.m_splitContainer.Panel1.ResumeLayout(false);
            this.m_splitContainer.Panel1.PerformLayout();
            this.m_splitContainer.Panel2.ResumeLayout(false);
            this.m_splitContainer.ResumeLayout(false);
            this.m_contextMenuStripHtmlTag.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer m_splitContainer;
        private System.Windows.Forms.TextBox m_textBoxHtmlCode;
        private System.Windows.Forms.WebBrowser m_webBrowserPreview;
        private System.Windows.Forms.Button m_btnOK;
        private bool m_bOK = false;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStripHtmlTag;
        private System.Windows.Forms.ToolStripMenuItem 插入标签ToolStripMenuItem;


    }
}