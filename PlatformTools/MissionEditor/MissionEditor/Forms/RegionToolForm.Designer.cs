﻿namespace MissionEditor
{
    partial class RegionToolForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_treeViewRegionList = new System.Windows.Forms.TreeView();
            this.m_buttonAddGroup = new System.Windows.Forms.Button();
            this.m_buttonDelGroup = new System.Windows.Forms.Button();
            this.m_buttonShow = new System.Windows.Forms.Button();
            this.m_buttonHide = new System.Windows.Forms.Button();
            this.m_comboBoxShowType = new System.Windows.Forms.ComboBox();
            this.m_checkBoxShowGrid = new System.Windows.Forms.CheckBox();
            this.m_textBoxGridSize = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_checkBoxShowNPC = new System.Windows.Forms.CheckBox();
            this.m_checkBoxShowRegion = new System.Windows.Forms.CheckBox();
            this.m_buttonDesc = new System.Windows.Forms.Button();
            this.m_contextMenuStripGroup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示组ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.隐藏组ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.添加区域ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.同步到音乐库ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.属性ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_contextMenuStripRegion = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.隐藏ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.移动到ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_checkBoxMusicRegion = new System.Windows.Forms.CheckBox();
            this.m_checkBoxSoundRegion = new System.Windows.Forms.CheckBox();
            this.m_checkBoxOtherRegion = new System.Windows.Forms.CheckBox();
            this.m_checkBoxTaskRegion = new System.Windows.Forms.CheckBox();
            this.m_checkBoxSingleNPC = new System.Windows.Forms.CheckBox();
            this.m_checkBoxAreaNPC = new System.Windows.Forms.CheckBox();
            this.m_timerEdgeHiden = new System.Windows.Forms.Timer(this.components);
            this.m_checkBoxShowPtSound = new System.Windows.Forms.CheckBox();
            this.m_comboBoxActiveLayer = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_checkBoxPrePathRegion = new System.Windows.Forms.CheckBox();
            this.m_checkBoxBlockQuadRegion = new System.Windows.Forms.CheckBox();
            this.m_contextMenuStripGroup.SuspendLayout();
            this.m_contextMenuStripRegion.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_treeViewRegionList
            // 
            this.m_treeViewRegionList.AllowDrop = true;
            this.m_treeViewRegionList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_treeViewRegionList.Location = new System.Drawing.Point(13, 39);
            this.m_treeViewRegionList.Name = "m_treeViewRegionList";
            this.m_treeViewRegionList.Size = new System.Drawing.Size(268, 400);
            this.m_treeViewRegionList.TabIndex = 0;
            this.m_treeViewRegionList.DragDrop += new System.Windows.Forms.DragEventHandler(this.m_treeViewRegionList_DragDrop);
            this.m_treeViewRegionList.MouseEnter += new System.EventHandler(this.m_treeViewRegionList_MouseEnter);
            this.m_treeViewRegionList.DragOver += new System.Windows.Forms.DragEventHandler(this.m_treeViewRegionList_DragOver);
            this.m_treeViewRegionList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.m_treeViewRegionList_AfterSelect);
            this.m_treeViewRegionList.DragEnter += new System.Windows.Forms.DragEventHandler(this.m_treeViewRegionList_DragEnter);
            this.m_treeViewRegionList.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.m_treeViewRegionList_ItemDrag);
            this.m_treeViewRegionList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_treeViewRegionList_MouseDown);
            this.m_treeViewRegionList.Leave += new System.EventHandler(this.m_treeViewRegionList_Leave);
            // 
            // m_buttonAddGroup
            // 
            this.m_buttonAddGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_buttonAddGroup.Location = new System.Drawing.Point(13, 445);
            this.m_buttonAddGroup.Name = "m_buttonAddGroup";
            this.m_buttonAddGroup.Size = new System.Drawing.Size(75, 23);
            this.m_buttonAddGroup.TabIndex = 1;
            this.m_buttonAddGroup.Text = "添加区域组";
            this.m_buttonAddGroup.UseVisualStyleBackColor = true;
            this.m_buttonAddGroup.Click += new System.EventHandler(this.m_buttonAddGroup_Click);
            // 
            // m_buttonDelGroup
            // 
            this.m_buttonDelGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_buttonDelGroup.Location = new System.Drawing.Point(13, 475);
            this.m_buttonDelGroup.Name = "m_buttonDelGroup";
            this.m_buttonDelGroup.Size = new System.Drawing.Size(75, 23);
            this.m_buttonDelGroup.TabIndex = 2;
            this.m_buttonDelGroup.Text = "删除区域组";
            this.m_buttonDelGroup.UseVisualStyleBackColor = true;
            this.m_buttonDelGroup.Click += new System.EventHandler(this.m_buttonDelGroup_Click);
            // 
            // m_buttonShow
            // 
            this.m_buttonShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_buttonShow.Location = new System.Drawing.Point(12, 545);
            this.m_buttonShow.Name = "m_buttonShow";
            this.m_buttonShow.Size = new System.Drawing.Size(75, 23);
            this.m_buttonShow.TabIndex = 3;
            this.m_buttonShow.Text = "显示";
            this.m_buttonShow.UseVisualStyleBackColor = true;
            this.m_buttonShow.Visible = false;
            this.m_buttonShow.Click += new System.EventHandler(this.m_buttonShow_Click);
            // 
            // m_buttonHide
            // 
            this.m_buttonHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_buttonHide.Location = new System.Drawing.Point(83, 545);
            this.m_buttonHide.Name = "m_buttonHide";
            this.m_buttonHide.Size = new System.Drawing.Size(75, 23);
            this.m_buttonHide.TabIndex = 4;
            this.m_buttonHide.Text = "隐藏";
            this.m_buttonHide.UseVisualStyleBackColor = true;
            this.m_buttonHide.Visible = false;
            this.m_buttonHide.Click += new System.EventHandler(this.m_buttonHide_Click);
            // 
            // m_comboBoxShowType
            // 
            this.m_comboBoxShowType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxShowType.FormattingEnabled = true;
            this.m_comboBoxShowType.Location = new System.Drawing.Point(13, 13);
            this.m_comboBoxShowType.Name = "m_comboBoxShowType";
            this.m_comboBoxShowType.Size = new System.Drawing.Size(268, 20);
            this.m_comboBoxShowType.TabIndex = 5;
            this.m_comboBoxShowType.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxShowType_SelectedIndexChanged);
            // 
            // m_checkBoxShowGrid
            // 
            this.m_checkBoxShowGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxShowGrid.AutoSize = true;
            this.m_checkBoxShowGrid.Location = new System.Drawing.Point(13, 505);
            this.m_checkBoxShowGrid.Name = "m_checkBoxShowGrid";
            this.m_checkBoxShowGrid.Size = new System.Drawing.Size(96, 16);
            this.m_checkBoxShowGrid.TabIndex = 6;
            this.m_checkBoxShowGrid.Text = "是否显示网格";
            this.m_checkBoxShowGrid.UseVisualStyleBackColor = true;
            this.m_checkBoxShowGrid.CheckedChanged += new System.EventHandler(this.m_checkBoxShowGrid_CheckedChanged);
            // 
            // m_textBoxGridSize
            // 
            this.m_textBoxGridSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_textBoxGridSize.Location = new System.Drawing.Point(70, 530);
            this.m_textBoxGridSize.Name = "m_textBoxGridSize";
            this.m_textBoxGridSize.Size = new System.Drawing.Size(24, 21);
            this.m_textBoxGridSize.TabIndex = 7;
            this.m_textBoxGridSize.TextChanged += new System.EventHandler(this.m_textBoxGridSize_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 535);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "网格大小";
            // 
            // m_checkBoxShowNPC
            // 
            this.m_checkBoxShowNPC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxShowNPC.AutoSize = true;
            this.m_checkBoxShowNPC.Location = new System.Drawing.Point(210, 449);
            this.m_checkBoxShowNPC.Name = "m_checkBoxShowNPC";
            this.m_checkBoxShowNPC.Size = new System.Drawing.Size(66, 16);
            this.m_checkBoxShowNPC.TabIndex = 9;
            this.m_checkBoxShowNPC.Text = "显示NPC";
            this.m_checkBoxShowNPC.UseVisualStyleBackColor = true;
            this.m_checkBoxShowNPC.CheckedChanged += new System.EventHandler(this.m_checkBoxShowNPC_CheckedChanged);
            // 
            // m_checkBoxShowRegion
            // 
            this.m_checkBoxShowRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxShowRegion.AutoSize = true;
            this.m_checkBoxShowRegion.Location = new System.Drawing.Point(125, 449);
            this.m_checkBoxShowRegion.Name = "m_checkBoxShowRegion";
            this.m_checkBoxShowRegion.Size = new System.Drawing.Size(72, 16);
            this.m_checkBoxShowRegion.TabIndex = 10;
            this.m_checkBoxShowRegion.Text = "显示区域";
            this.m_checkBoxShowRegion.UseVisualStyleBackColor = true;
            this.m_checkBoxShowRegion.CheckedChanged += new System.EventHandler(this.m_checkBoxShowRegion_CheckedChanged);
            // 
            // m_buttonDesc
            // 
            this.m_buttonDesc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_buttonDesc.Location = new System.Drawing.Point(210, 545);
            this.m_buttonDesc.Name = "m_buttonDesc";
            this.m_buttonDesc.Size = new System.Drawing.Size(75, 23);
            this.m_buttonDesc.TabIndex = 11;
            this.m_buttonDesc.Text = "属性";
            this.m_buttonDesc.UseVisualStyleBackColor = true;
            this.m_buttonDesc.Visible = false;
            this.m_buttonDesc.Click += new System.EventHandler(this.m_buttonDesc_Click);
            // 
            // m_contextMenuStripGroup
            // 
            this.m_contextMenuStripGroup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示组ToolStripMenuItem,
            this.隐藏组ToolStripMenuItem,
            this.toolStripSeparator1,
            this.添加区域ToolStripMenuItem,
            this.toolStripSeparator3,
            this.同步到音乐库ToolStripMenuItem,
            this.属性ToolStripMenuItem});
            this.m_contextMenuStripGroup.Name = "m_contextMenuStripGroup";
            this.m_contextMenuStripGroup.Size = new System.Drawing.Size(143, 126);
            // 
            // 显示组ToolStripMenuItem
            // 
            this.显示组ToolStripMenuItem.Name = "显示组ToolStripMenuItem";
            this.显示组ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.显示组ToolStripMenuItem.Text = "显示组";
            this.显示组ToolStripMenuItem.Click += new System.EventHandler(this.显示组ToolStripMenuItem_Click);
            // 
            // 隐藏组ToolStripMenuItem
            // 
            this.隐藏组ToolStripMenuItem.Name = "隐藏组ToolStripMenuItem";
            this.隐藏组ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.隐藏组ToolStripMenuItem.Text = "隐藏组";
            this.隐藏组ToolStripMenuItem.Click += new System.EventHandler(this.隐藏组ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
            // 
            // 添加区域ToolStripMenuItem
            // 
            this.添加区域ToolStripMenuItem.Name = "添加区域ToolStripMenuItem";
            this.添加区域ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.添加区域ToolStripMenuItem.Text = "添加区域";
            this.添加区域ToolStripMenuItem.Click += new System.EventHandler(this.添加区域ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(139, 6);
            // 
            // 同步到音乐库ToolStripMenuItem
            // 
            this.同步到音乐库ToolStripMenuItem.Name = "同步到音乐库ToolStripMenuItem";
            this.同步到音乐库ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.同步到音乐库ToolStripMenuItem.Text = "同步到音乐库";
            this.同步到音乐库ToolStripMenuItem.Click += new System.EventHandler(this.同步到音乐库ToolStripMenuItem_Click);
            // 
            // 属性ToolStripMenuItem
            // 
            this.属性ToolStripMenuItem.Name = "属性ToolStripMenuItem";
            this.属性ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.属性ToolStripMenuItem.Text = "属性";
            this.属性ToolStripMenuItem.Click += new System.EventHandler(this.属性ToolStripMenuItem_Click);
            // 
            // m_contextMenuStripRegion
            // 
            this.m_contextMenuStripRegion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示ToolStripMenuItem,
            this.隐藏ToolStripMenuItem,
            this.toolStripSeparator2,
            this.删除ToolStripMenuItem,
            this.移动到ToolStripMenuItem});
            this.m_contextMenuStripRegion.Name = "m_contextMenuStripRegion";
            this.m_contextMenuStripRegion.Size = new System.Drawing.Size(107, 98);
            // 
            // 显示ToolStripMenuItem
            // 
            this.显示ToolStripMenuItem.Name = "显示ToolStripMenuItem";
            this.显示ToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.显示ToolStripMenuItem.Text = "显示";
            this.显示ToolStripMenuItem.Click += new System.EventHandler(this.显示ToolStripMenuItem_Click);
            // 
            // 隐藏ToolStripMenuItem
            // 
            this.隐藏ToolStripMenuItem.Name = "隐藏ToolStripMenuItem";
            this.隐藏ToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.隐藏ToolStripMenuItem.Text = "隐藏";
            this.隐藏ToolStripMenuItem.Click += new System.EventHandler(this.隐藏ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(103, 6);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // 移动到ToolStripMenuItem
            // 
            this.移动到ToolStripMenuItem.Name = "移动到ToolStripMenuItem";
            this.移动到ToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.移动到ToolStripMenuItem.Text = "移动到";
            // 
            // m_checkBoxMusicRegion
            // 
            this.m_checkBoxMusicRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxMusicRegion.AutoSize = true;
            this.m_checkBoxMusicRegion.Enabled = false;
            this.m_checkBoxMusicRegion.Location = new System.Drawing.Point(125, 475);
            this.m_checkBoxMusicRegion.Name = "m_checkBoxMusicRegion";
            this.m_checkBoxMusicRegion.Size = new System.Drawing.Size(48, 16);
            this.m_checkBoxMusicRegion.TabIndex = 12;
            this.m_checkBoxMusicRegion.Text = "音乐";
            this.m_checkBoxMusicRegion.UseVisualStyleBackColor = true;
            this.m_checkBoxMusicRegion.CheckedChanged += new System.EventHandler(this.m_checkBoxXXXXRegion_CheckedChanged);
            // 
            // m_checkBoxSoundRegion
            // 
            this.m_checkBoxSoundRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxSoundRegion.AutoSize = true;
            this.m_checkBoxSoundRegion.Enabled = false;
            this.m_checkBoxSoundRegion.Location = new System.Drawing.Point(125, 495);
            this.m_checkBoxSoundRegion.Name = "m_checkBoxSoundRegion";
            this.m_checkBoxSoundRegion.Size = new System.Drawing.Size(72, 16);
            this.m_checkBoxSoundRegion.TabIndex = 13;
            this.m_checkBoxSoundRegion.Text = "音效特效";
            this.m_checkBoxSoundRegion.UseVisualStyleBackColor = true;
            this.m_checkBoxSoundRegion.CheckedChanged += new System.EventHandler(this.m_checkBoxXXXXRegion_CheckedChanged);
            // 
            // m_checkBoxOtherRegion
            // 
            this.m_checkBoxOtherRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxOtherRegion.AutoSize = true;
            this.m_checkBoxOtherRegion.Enabled = false;
            this.m_checkBoxOtherRegion.Location = new System.Drawing.Point(125, 535);
            this.m_checkBoxOtherRegion.Name = "m_checkBoxOtherRegion";
            this.m_checkBoxOtherRegion.Size = new System.Drawing.Size(48, 16);
            this.m_checkBoxOtherRegion.TabIndex = 14;
            this.m_checkBoxOtherRegion.Text = "其他";
            this.m_checkBoxOtherRegion.UseVisualStyleBackColor = true;
            this.m_checkBoxOtherRegion.CheckedChanged += new System.EventHandler(this.m_checkBoxXXXXRegion_CheckedChanged);
            // 
            // m_checkBoxTaskRegion
            // 
            this.m_checkBoxTaskRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxTaskRegion.AutoSize = true;
            this.m_checkBoxTaskRegion.Enabled = false;
            this.m_checkBoxTaskRegion.Location = new System.Drawing.Point(125, 515);
            this.m_checkBoxTaskRegion.Name = "m_checkBoxTaskRegion";
            this.m_checkBoxTaskRegion.Size = new System.Drawing.Size(48, 16);
            this.m_checkBoxTaskRegion.TabIndex = 15;
            this.m_checkBoxTaskRegion.Text = "任务";
            this.m_checkBoxTaskRegion.UseVisualStyleBackColor = true;
            this.m_checkBoxTaskRegion.CheckedChanged += new System.EventHandler(this.m_checkBoxXXXXRegion_CheckedChanged);
            // 
            // m_checkBoxSingleNPC
            // 
            this.m_checkBoxSingleNPC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxSingleNPC.AutoSize = true;
            this.m_checkBoxSingleNPC.Enabled = false;
            this.m_checkBoxSingleNPC.Location = new System.Drawing.Point(210, 475);
            this.m_checkBoxSingleNPC.Name = "m_checkBoxSingleNPC";
            this.m_checkBoxSingleNPC.Size = new System.Drawing.Size(48, 16);
            this.m_checkBoxSingleNPC.TabIndex = 16;
            this.m_checkBoxSingleNPC.Text = "单点";
            this.m_checkBoxSingleNPC.UseVisualStyleBackColor = true;
            this.m_checkBoxSingleNPC.CheckedChanged += new System.EventHandler(this.m_checkBoxXXXXNPC_CheckedChanged);
            // 
            // m_checkBoxAreaNPC
            // 
            this.m_checkBoxAreaNPC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxAreaNPC.AutoSize = true;
            this.m_checkBoxAreaNPC.Enabled = false;
            this.m_checkBoxAreaNPC.Location = new System.Drawing.Point(210, 495);
            this.m_checkBoxAreaNPC.Name = "m_checkBoxAreaNPC";
            this.m_checkBoxAreaNPC.Size = new System.Drawing.Size(48, 16);
            this.m_checkBoxAreaNPC.TabIndex = 17;
            this.m_checkBoxAreaNPC.Text = "区域";
            this.m_checkBoxAreaNPC.UseVisualStyleBackColor = true;
            this.m_checkBoxAreaNPC.CheckedChanged += new System.EventHandler(this.m_checkBoxXXXXNPC_CheckedChanged);
            // 
            // m_timerEdgeHiden
            // 
            this.m_timerEdgeHiden.Tick += new System.EventHandler(this.m_timerEdgeHiden_Tick);
            // 
            // m_checkBoxShowPtSound
            // 
            this.m_checkBoxShowPtSound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxShowPtSound.AutoSize = true;
            this.m_checkBoxShowPtSound.Location = new System.Drawing.Point(210, 535);
            this.m_checkBoxShowPtSound.Name = "m_checkBoxShowPtSound";
            this.m_checkBoxShowPtSound.Size = new System.Drawing.Size(60, 16);
            this.m_checkBoxShowPtSound.TabIndex = 18;
            this.m_checkBoxShowPtSound.Text = "点音源";
            this.m_checkBoxShowPtSound.UseVisualStyleBackColor = true;
            this.m_checkBoxShowPtSound.CheckedChanged += new System.EventHandler(this.m_checkBoxShowPtSound_CheckedChanged);
            // 
            // m_comboBoxActiveLayer
            // 
            this.m_comboBoxActiveLayer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_comboBoxActiveLayer.DropDownHeight = 158;
            this.m_comboBoxActiveLayer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxActiveLayer.FormattingEnabled = true;
            this.m_comboBoxActiveLayer.IntegralHeight = false;
            this.m_comboBoxActiveLayer.Location = new System.Drawing.Point(90, 595);
            this.m_comboBoxActiveLayer.Name = "m_comboBoxActiveLayer";
            this.m_comboBoxActiveLayer.Size = new System.Drawing.Size(121, 20);
            this.m_comboBoxActiveLayer.TabIndex = 19;
            this.m_comboBoxActiveLayer.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxActiveLayer_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 599);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "当前编辑层:";
            // 
            // m_checkBoxPrePathRegion
            // 
            this.m_checkBoxPrePathRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxPrePathRegion.AutoSize = true;
            this.m_checkBoxPrePathRegion.Enabled = false;
            this.m_checkBoxPrePathRegion.Location = new System.Drawing.Point(125, 555);
            this.m_checkBoxPrePathRegion.Name = "m_checkBoxPrePathRegion";
            this.m_checkBoxPrePathRegion.Size = new System.Drawing.Size(84, 16);
            this.m_checkBoxPrePathRegion.TabIndex = 21;
            this.m_checkBoxPrePathRegion.Text = "寻路预判区";
            this.m_checkBoxPrePathRegion.UseVisualStyleBackColor = true;
            this.m_checkBoxPrePathRegion.CheckedChanged += new System.EventHandler(this.m_checkBoxXXXXRegion_CheckedChanged);
            // 
            // m_checkBoxBlockQuadRegion
            // 
            this.m_checkBoxBlockQuadRegion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_checkBoxBlockQuadRegion.AutoSize = true;
            this.m_checkBoxBlockQuadRegion.Enabled = false;
            this.m_checkBoxBlockQuadRegion.Location = new System.Drawing.Point(125, 575);
            this.m_checkBoxBlockQuadRegion.Name = "m_checkBoxBlockQuadRegion";
            this.m_checkBoxBlockQuadRegion.Size = new System.Drawing.Size(90, 16);
            this.m_checkBoxBlockQuadRegion.TabIndex = 22;
            this.m_checkBoxBlockQuadRegion.Text = "BlockQuad区";
            this.m_checkBoxBlockQuadRegion.UseVisualStyleBackColor = true;
            this.m_checkBoxBlockQuadRegion.CheckedChanged += new System.EventHandler(this.m_checkBoxXXXXRegion_CheckedChanged);
            // 
            // RegionToolForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 621);
            this.Controls.Add(this.m_checkBoxBlockQuadRegion);
            this.Controls.Add(this.m_checkBoxPrePathRegion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_comboBoxActiveLayer);
            this.Controls.Add(this.m_checkBoxShowPtSound);
            this.Controls.Add(this.m_checkBoxAreaNPC);
            this.Controls.Add(this.m_checkBoxSingleNPC);
            this.Controls.Add(this.m_checkBoxTaskRegion);
            this.Controls.Add(this.m_checkBoxOtherRegion);
            this.Controls.Add(this.m_checkBoxSoundRegion);
            this.Controls.Add(this.m_checkBoxMusicRegion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_textBoxGridSize);
            this.Controls.Add(this.m_checkBoxShowRegion);
            this.Controls.Add(this.m_checkBoxShowGrid);
            this.Controls.Add(this.m_comboBoxShowType);
            this.Controls.Add(this.m_buttonDelGroup);
            this.Controls.Add(this.m_buttonDesc);
            this.Controls.Add(this.m_buttonAddGroup);
            this.Controls.Add(this.m_checkBoxShowNPC);
            this.Controls.Add(this.m_treeViewRegionList);
            this.Controls.Add(this.m_buttonShow);
            this.Controls.Add(this.m_buttonHide);
            this.Name = "RegionToolForm";
            this.Text = "RegionToolForm";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegionToolForm_FormClosing);
            this.LocationChanged += new System.EventHandler(this.RegionToolForm_LocationChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RegionToolForm_MouseDown);
            this.m_contextMenuStripGroup.ResumeLayout(false);
            this.m_contextMenuStripRegion.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private int m_iEdgeHidenState = 0;

        private static RegionToolForm m_Instance = null;
        private bool m_bDirtyTree = false;
        private System.Windows.Forms.TreeView m_treeViewRegionList;
        private System.Windows.Forms.Button m_buttonAddGroup;
        private System.Windows.Forms.Button m_buttonDelGroup;
        private System.Windows.Forms.Button m_buttonShow;
        private System.Windows.Forms.Button m_buttonHide;
        private System.Windows.Forms.ComboBox m_comboBoxShowType;
        private System.Windows.Forms.TreeNode oldenterNode = null;
        private System.Windows.Forms.CheckBox m_checkBoxShowGrid;
        private System.Windows.Forms.TextBox m_textBoxGridSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox m_checkBoxShowNPC;
        private System.Windows.Forms.CheckBox m_checkBoxShowRegion;
        private System.Windows.Forms.Button m_buttonDesc;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStripGroup;
        private System.Windows.Forms.ToolStripMenuItem 显示组ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 隐藏组ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 属性ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStripRegion;
        private System.Windows.Forms.ToolStripMenuItem 显示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 隐藏ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 移动到ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 添加区域ToolStripMenuItem;
        private System.Windows.Forms.CheckBox m_checkBoxMusicRegion;
        private System.Windows.Forms.CheckBox m_checkBoxSoundRegion;
        private System.Windows.Forms.CheckBox m_checkBoxOtherRegion;
        private System.Windows.Forms.CheckBox m_checkBoxTaskRegion;
        private System.Windows.Forms.CheckBox m_checkBoxSingleNPC;
        private System.Windows.Forms.CheckBox m_checkBoxAreaNPC;
        private System.Windows.Forms.Timer m_timerEdgeHiden;
        private System.Windows.Forms.CheckBox m_checkBoxShowPtSound;
        private System.Windows.Forms.ComboBox m_comboBoxActiveLayer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 同步到音乐库ToolStripMenuItem;
        private System.Windows.Forms.CheckBox m_checkBoxPrePathRegion;
        private System.Windows.Forms.CheckBox m_checkBoxBlockQuadRegion;
    }
}