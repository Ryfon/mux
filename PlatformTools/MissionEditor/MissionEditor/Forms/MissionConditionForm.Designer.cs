﻿namespace MissionEditor
{
    partial class MissionConditionForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_comboBoxFilter = new System.Windows.Forms.ComboBox();
            this.m_listBoxAllConditions = new System.Windows.Forms.ListBox();
            this.m_textBoxValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_listBoxSelectedConditions = new System.Windows.Forms.ListBox();
            this.m_listBoxValue = new System.Windows.Forms.ListBox();
            this.m_btnAdd = new System.Windows.Forms.Button();
            this.m_btnRemove = new System.Windows.Forms.Button();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_textBoxConditionDesc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxConditionParam = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // m_comboBoxFilter
            // 
            this.m_comboBoxFilter.FormattingEnabled = true;
            this.m_comboBoxFilter.Location = new System.Drawing.Point(12, 12);
            this.m_comboBoxFilter.Name = "m_comboBoxFilter";
            this.m_comboBoxFilter.Size = new System.Drawing.Size(131, 20);
            this.m_comboBoxFilter.TabIndex = 0;
            this.m_comboBoxFilter.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxFilter_SelectedIndexChanged);
            // 
            // m_listBoxAllConditions
            // 
            this.m_listBoxAllConditions.FormattingEnabled = true;
            this.m_listBoxAllConditions.ItemHeight = 12;
            this.m_listBoxAllConditions.Location = new System.Drawing.Point(13, 41);
            this.m_listBoxAllConditions.Name = "m_listBoxAllConditions";
            this.m_listBoxAllConditions.Size = new System.Drawing.Size(129, 172);
            this.m_listBoxAllConditions.TabIndex = 1;
            this.m_listBoxAllConditions.SelectedIndexChanged += new System.EventHandler(this.m_listBoxAllConditions_SelectedIndexChanged);
            // 
            // m_textBoxValue
            // 
            this.m_textBoxValue.Location = new System.Drawing.Point(63, 219);
            this.m_textBoxValue.Name = "m_textBoxValue";
            this.m_textBoxValue.Size = new System.Drawing.Size(78, 21);
            this.m_textBoxValue.TabIndex = 2;
            this.m_textBoxValue.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 222);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "对应值";
            // 
            // m_listBoxSelectedConditions
            // 
            this.m_listBoxSelectedConditions.FormattingEnabled = true;
            this.m_listBoxSelectedConditions.ItemHeight = 12;
            this.m_listBoxSelectedConditions.Location = new System.Drawing.Point(184, 41);
            this.m_listBoxSelectedConditions.Name = "m_listBoxSelectedConditions";
            this.m_listBoxSelectedConditions.Size = new System.Drawing.Size(133, 172);
            this.m_listBoxSelectedConditions.TabIndex = 4;
            this.m_listBoxSelectedConditions.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSelectedConditions_SelectedIndexChanged);
            // 
            // m_listBoxValue
            // 
            this.m_listBoxValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listBoxValue.FormattingEnabled = true;
            this.m_listBoxValue.ItemHeight = 12;
            this.m_listBoxValue.Location = new System.Drawing.Point(323, 41);
            this.m_listBoxValue.Name = "m_listBoxValue";
            this.m_listBoxValue.Size = new System.Drawing.Size(124, 172);
            this.m_listBoxValue.TabIndex = 5;
            // 
            // m_btnAdd
            // 
            this.m_btnAdd.Location = new System.Drawing.Point(149, 79);
            this.m_btnAdd.Name = "m_btnAdd";
            this.m_btnAdd.Size = new System.Drawing.Size(26, 21);
            this.m_btnAdd.TabIndex = 6;
            this.m_btnAdd.Text = "->";
            this.m_btnAdd.UseVisualStyleBackColor = true;
            this.m_btnAdd.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_btnRemove
            // 
            this.m_btnRemove.Location = new System.Drawing.Point(149, 106);
            this.m_btnRemove.Name = "m_btnRemove";
            this.m_btnRemove.Size = new System.Drawing.Size(26, 21);
            this.m_btnRemove.TabIndex = 7;
            this.m_btnRemove.Text = "<-";
            this.m_btnRemove.UseVisualStyleBackColor = true;
            this.m_btnRemove.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(260, 222);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(57, 25);
            this.m_btnOK.TabIndex = 8;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_textBoxConditionDesc
            // 
            this.m_textBoxConditionDesc.Location = new System.Drawing.Point(77, 260);
            this.m_textBoxConditionDesc.Name = "m_textBoxConditionDesc";
            this.m_textBoxConditionDesc.ReadOnly = true;
            this.m_textBoxConditionDesc.Size = new System.Drawing.Size(370, 21);
            this.m_textBoxConditionDesc.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "功能描述:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 11;
            this.label3.Text = "参数说明:";
            // 
            // m_textBoxConditionParam
            // 
            this.m_textBoxConditionParam.Location = new System.Drawing.Point(77, 287);
            this.m_textBoxConditionParam.Multiline = true;
            this.m_textBoxConditionParam.Name = "m_textBoxConditionParam";
            this.m_textBoxConditionParam.ReadOnly = true;
            this.m_textBoxConditionParam.Size = new System.Drawing.Size(370, 105);
            this.m_textBoxConditionParam.TabIndex = 12;
            // 
            // MissionConditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 406);
            this.Controls.Add(this.m_textBoxConditionParam);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_textBoxConditionDesc);
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_btnRemove);
            this.Controls.Add(this.m_btnAdd);
            this.Controls.Add(this.m_listBoxValue);
            this.Controls.Add(this.m_listBoxSelectedConditions);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_textBoxValue);
            this.Controls.Add(this.m_listBoxAllConditions);
            this.Controls.Add(this.m_comboBoxFilter);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MissionConditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "任务接受条件编辑";
            this.Load += new System.EventHandler(this.MissionConditionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox m_comboBoxFilter;
        private System.Windows.Forms.ListBox m_listBoxAllConditions;
        private System.Windows.Forms.TextBox m_textBoxValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox m_listBoxSelectedConditions;
        private System.Windows.Forms.ListBox m_listBoxValue;
        private System.Windows.Forms.Button m_btnAdd;
        private System.Windows.Forms.Button m_btnRemove;
        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.TextBox m_textBoxConditionDesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxConditionParam;
    }
}