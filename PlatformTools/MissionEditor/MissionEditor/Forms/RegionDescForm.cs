﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;

namespace MissionEditor
{
    public partial class RegionDescForm : Form
    {
        private RegionDescForm()
        {
            InitializeComponent();
        }

        public static RegionDescForm Instance
        {
            get
            {
                if (m_Instance == null || m_Instance.IsDisposed)
                {
                    m_Instance = new RegionDescForm();
                }

                return m_Instance;
            }
        }

        public Hashtable MusicLibTable
        {
            get
            {
                return m_MusicLibTable;
            }
        }

        public Hashtable SoundLibTable
        {
            get
            {
                return m_SoundLibTable;
            }
        }

        public void RefreshUI(RegionDesc desc)
        {
            m_RegionDesc = desc;

            // 基本属性
            m_checkBoxEnableSound.Checked = desc.EnableSound;
            m_checkBoxEnableEffect.Checked = desc.EnableEffect;

            // 音乐
            m_dataGridViewMainMusic.DataSource = desc.MusicList[0];
            m_dataGridViewSubMusic1.DataSource = desc.MusicList[1];
            m_dataGridViewSubMusic2.DataSource = desc.MusicList[2];
            m_dataGridViewSubMusic3.DataSource = desc.MusicList[3];
            
            // 音效
            m_dataGridViewMainSound.DataSource = desc.SoundList[0];
            m_dataGridViewSubSound1.DataSource = desc.SoundList[1];
            m_dataGridViewSubSound2.DataSource = desc.SoundList[2];

            // 特效
            m_dataGridViewEffect.DataSource = desc.EffectList[0];

            if(m_MusicLibTable.Keys.Count != 0)
            {
                m_comboBoxMainMusic.SelectedIndex = 0;
                m_comboBoxSubMusic1.SelectedIndex = 0;
                m_comboBoxSubMusic2.SelectedIndex = 0;
                m_comboBoxSubMusic3.SelectedIndex = 0;
            }
            if(m_SoundLibTable.Keys.Count != 0)
            {
                m_comboBoxMainSound.SelectedIndex = 0;
                m_comboBoxSubSound1.SelectedIndex = 0;
                m_comboBoxSubSound2.SelectedIndex = 0;
            }
        }

        public void RefreshMusicLib()
        {
            m_comboBoxMainMusic.Items.Clear();
            m_comboBoxSubMusic1.Items.Clear();
            m_comboBoxSubMusic2.Items.Clear();
            m_comboBoxSubMusic3.Items.Clear();
            m_comboBoxMainMusic.Items.Add("");
            m_comboBoxSubMusic1.Items.Add("");
            m_comboBoxSubMusic2.Items.Add("");
            m_comboBoxSubMusic3.Items.Add("");

            foreach(String key in m_MusicLibTable.Keys)
            {
                m_comboBoxMainMusic.Items.Add(key);
                m_comboBoxSubMusic1.Items.Add(key);
                m_comboBoxSubMusic2.Items.Add(key);
                m_comboBoxSubMusic3.Items.Add(key);
            }
        }

        public void RefreshSoundLib()
        {
            m_comboBoxMainSound.Items.Clear();
            m_comboBoxSubSound1.Items.Clear();
            m_comboBoxSubSound2.Items.Clear();
            m_comboBoxMainSound.Items.Add("");
            m_comboBoxSubSound1.Items.Add("");
            m_comboBoxSubSound2.Items.Add("");

            foreach (String key in m_SoundLibTable.Keys)
            {
                m_comboBoxMainSound.Items.Add(key);
                m_comboBoxSubSound1.Items.Add(key);
                m_comboBoxSubSound2.Items.Add(key);
            }
        }

        // 从xml文件中加载音乐库
        public bool LoadMusicLib(String strFileName)
        {
            m_MusicLibDoc.Load(strFileName);
            if(m_MusicLibDoc == null)
            {
                return false;            
            }
            else
            {
                XmlElement root = m_MusicLibDoc.LastChild as XmlElement;
                if(root == null)
                {
                    return false;
                }
                else
                {
                    m_MusicLibTable.Clear();

                    XmlElement xmlMusic = root.FirstChild as XmlElement;

                    while(xmlMusic != null)
                    {
                        String strIndex = OptionManager.MusicTemplate.TableName;
                        if(xmlMusic.HasAttribute(strIndex))
                        {
                            String strKey = xmlMusic.GetAttribute(strIndex);
                            if(strKey.Length > 0)
                            {
                                m_MusicLibTable[strKey] = xmlMusic;
                            }
                        }
                        xmlMusic = xmlMusic.NextSibling as XmlElement;
                    }
                }
            }
            return true;
        }

        // 从xml文件中加载音效库
        public bool LoadSoundLib(String strFileName)
        {
            m_SoundLibDoc.Load(strFileName);
            if (m_SoundLibDoc == null)
            {
                return false;
            }
            else
            {
                XmlElement root = m_SoundLibDoc.LastChild as XmlElement;
                if (root == null)
                {
                    return false;
                }
                else
                {
                    m_SoundLibTable.Clear();

                    XmlElement xmlSound = root.FirstChild as XmlElement;

                    while (xmlSound != null)
                    {
                        String strIndex = OptionManager.SoundTemplate.TableName;
                        if (xmlSound.HasAttribute(strIndex))
                        {
                            String strKey = xmlSound.GetAttribute(strIndex);
                            if(strKey.Length > 0)
                            {
                                m_SoundLibTable[strKey] = xmlSound;
                            }
                        }
                        xmlSound = xmlSound.NextSibling as XmlElement;
                    }
                }
            }
            return true;
        }

        private void m_checkBoxEnableSound_CheckedChanged(object sender, EventArgs e)
        {
            m_RegionDesc.EnableSound = m_checkBoxEnableSound.Checked;
        }

        private void m_checkBoxEnableEffect_CheckedChanged(object sender, EventArgs e)
        {
            m_RegionDesc.EnableEffect = m_checkBoxEnableEffect.Checked;
        }

        private void m_buttonLoadMusicLib_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "配置文件 (*.xml)|*.xml||";
            dlg.InitialDirectory = Application.StartupPath;
            dlg.RestoreDirectory = true;
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (LoadMusicLib(dlg.FileName))
                {
                    MessageBox.Show("加载成功！");
                    RefreshMusicLib();
                }
                else
                {
                    MessageBox.Show("加载失败！");
                }
            }
        }

        private void m_buttonLoadSoundLib_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "配置文件 (*.xml)|*.xml||";
            dlg.InitialDirectory = Application.StartupPath;
            dlg.RestoreDirectory = true;
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (LoadSoundLib(dlg.FileName))
                {
                    MessageBox.Show("加载成功！");
                    RefreshSoundLib();
                }
                else
                {
                    MessageBox.Show("加载失败！");
                }
            }
        }

        private void m_comboBoxMainMusic_SelectedIndexChanged(object sender, EventArgs e)
        {
            String strKey = m_comboBoxMainMusic.SelectedItem.ToString();
            XmlElement xmlValue = m_MusicLibTable[strKey] as XmlElement;
            if(xmlValue != null)
            {
                m_RegionDesc.Update(1, "MainMusic", xmlValue);
            }
        }

        private void m_comboBoxSubMusic1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String strKey = m_comboBoxSubMusic1.SelectedItem.ToString();
            XmlElement xmlValue = m_MusicLibTable[strKey] as XmlElement;
            if (xmlValue != null)
            {
                m_RegionDesc.Update(1, "SubMusic1", xmlValue);
            }
        }

        private void m_comboBoxSubMusic2_SelectedIndexChanged(object sender, EventArgs e)
        {
            String strKey = m_comboBoxSubMusic2.SelectedItem.ToString();
            XmlElement xmlValue = m_MusicLibTable[strKey] as XmlElement;
            if (xmlValue != null)
            {
                m_RegionDesc.Update(1, "SubMusic2", xmlValue);
            }
        }

        private void m_comboBoxSubMusic3_SelectedIndexChanged(object sender, EventArgs e)
        {
            String strKey = m_comboBoxSubMusic3.SelectedItem.ToString();
            XmlElement xmlValue = m_MusicLibTable[strKey] as XmlElement;
            if (xmlValue != null)
            {
                m_RegionDesc.Update(1, "SubMusic3", xmlValue);
            }
        }

        private void m_comboBoxMainSound_SelectedIndexChanged(object sender, EventArgs e)
        {
            String strKey = m_comboBoxMainSound.SelectedItem.ToString();
            XmlElement xmlValue = m_SoundLibTable[strKey] as XmlElement;
            if (xmlValue != null)
            {
                m_RegionDesc.Update(2, "MainSound", xmlValue);
            }
        }

        private void m_comboBoxSubSound1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String strKey = m_comboBoxSubSound1.SelectedItem.ToString();
            XmlElement xmlValue = m_SoundLibTable[strKey] as XmlElement;
            if (xmlValue != null)
            {
                m_RegionDesc.Update(2, "SubSound1", xmlValue);
            }
        }

        private void m_comboBoxSubSound2_SelectedIndexChanged(object sender, EventArgs e)
        {
            String strKey = m_comboBoxSubSound2.SelectedItem.ToString();
            XmlElement xmlValue = m_SoundLibTable[strKey] as XmlElement;
            if (xmlValue != null)
            {
                m_RegionDesc.Update(2, "SubSound2", xmlValue);
            }
        }

        private void m_buttonExit_Click(object sender, EventArgs e)
        {
            this.Hide(); 
        }
    }
}