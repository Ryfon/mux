﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class HtmlEditorForm : Form
    {
        public HtmlEditorForm()
        {
            InitializeComponent();
        }

        
        public String HtmlText
        {
            get
            {
                return m_textBoxHtmlCode.Text;
            }
            set
            {
                //m_webBrowserPreview.DocumentText = value;
                m_textBoxHtmlCode.Text = value;
            }
        }

        public bool Ok
        {
            get
            {
                return m_bOK;
            }
        }

        private void m_textBoxHtmlCode_TextChanged(object sender, EventArgs e)
        {
            m_webBrowserPreview.DocumentText = m_textBoxHtmlCode.Text;
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            // 消除硬换行
            m_textBoxHtmlCode.Text = m_textBoxHtmlCode.Text.Replace("\r\n", "");
            m_textBoxHtmlCode.Text = m_textBoxHtmlCode.Text.Replace("\0", "");
            m_bOK = true;
            this.Close();
        }

        private void 插入标签ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_textBoxHtmlCode.SelectedText = "<FONT SIZE=4 COLOR=\"#000000\">"+ m_textBoxHtmlCode.SelectedText+"<FONT>";

        }
    }
}