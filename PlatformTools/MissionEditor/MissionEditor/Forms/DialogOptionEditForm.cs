﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class DialogOptionEditForm : Form
    {
        public DialogOptionEditForm(DialogOption option)
        {
            InitializeComponent();

            m_OnEditOption = option;

            // 将参数  option 中的值赋到界面元素
            m_textBoxOption.Text = option.OptionString;
            m_textBoxCurOption.Text = option.Value.ToString();

            m_listBoxSelectedConditions.Items.Clear();
            m_listBoxValue.Items.Clear();

            foreach (MissionCondition cond in option.ConditionList)
            {
                m_listBoxSelectedConditions.Items.Add(cond.Condition);
                m_listBoxValue.Items.Add(cond.Value);
            }

        }

        private void SetConditionAttribute(MissionConditionInstance mci)
        {
            if (mci == null)
            {
                m_textBoxConditionDesc.Text = "";
                m_textBoxConditionParam.Text = "";
            }
            else
            {
                m_textBoxConditionDesc.Text = mci.Desc;
                m_textBoxConditionParam.Text = mci.Param;
            }
        }

        private void DialogOptionEditForm_Load(object sender, EventArgs e)
        {
            // 从 MissionRequirementManager 中找到第一级分类,添加到 m_comboBoxFilter 中
            foreach (String str1stLevel in MissionConditionManager.Instance.ConditionsMap.Keys)
            {
                m_comboBoxFilter.Items.Add(str1stLevel);
            }
        }

        private void m_comboBoxFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_comboBoxFilter.SelectedIndex < 0)
            {
                return;
            }

            String strFilter = m_comboBoxFilter.SelectedItem.ToString();

            // 从 MissionConditionManager 中找到 strFilter 的下一级 condition 列表
            Hashtable conditionMap = MissionConditionManager.Instance.ConditionsMap[strFilter] as Hashtable;
            if (conditionMap != null)
            {
                m_listBoxAllConditions.Items.Clear();
                foreach (String strCondition in conditionMap.Keys)
                {
                    m_listBoxAllConditions.Items.Add(strCondition);
                }
            }

            // 更新任务条件描述
            SetConditionAttribute(null);
        }


        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            // 添加 condition
            if (m_listBoxAllConditions.SelectedItem == null)
            {
                return;
            }

            String strCondition = m_listBoxAllConditions.SelectedItem.ToString();
            if (strCondition == null ) //|| m_listBoxSelectedConditions.Items.Contains(strCondition))
            {
                return;
            }

            m_listBoxSelectedConditions.Items.Add(strCondition);
            m_listBoxValue.Items.Add(m_textBoxValue.Text);

        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            // 删除 condition
            int iIdx = m_listBoxSelectedConditions.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxValue.Items.RemoveAt(iIdx);
                m_listBoxSelectedConditions.Items.RemoveAt(iIdx);
            }
        }

        private void m_listBoxSelectedConditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iIdx = m_listBoxSelectedConditions.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxValue.SelectedIndex = iIdx;
            }

            // 更新任务条件描述
            if (m_listBoxSelectedConditions.SelectedItem != null)
            {
                String strConditionName = m_listBoxSelectedConditions.SelectedItem.ToString();
                MissionConditionInstance mci = MissionConditionManager.Instance.GetConditionByDisplay(strConditionName);
                SetConditionAttribute(mci);
            }
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            // 将界面值保存到 state
            m_OnEditOption.OptionString = m_textBoxOption.Text;
            m_OnEditOption.Value = int.Parse(m_textBoxCurOption.Text);
            m_OnEditOption.ConditionList.Clear();

            for (int i=0; i<m_listBoxSelectedConditions.Items.Count; i++)
            {
                String strCond = m_listBoxSelectedConditions.Items[i].ToString();
                String strValue = m_listBoxValue.Items[i].ToString();

                MissionCondition condition = new MissionCondition(strCond, strValue);
                m_OnEditOption.ConditionList.Add(condition);
            }

            this.Close();
        }

        private void m_btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void m_listBoxAllConditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 更新任务条件描述
            if (m_listBoxAllConditions.SelectedItem != null)
            {
                String strConditionName = m_listBoxAllConditions.SelectedItem.ToString();
                MissionConditionInstance mci = MissionConditionManager.Instance.GetConditionByDisplay(strConditionName);
                SetConditionAttribute(mci);
            }
        }


    }
}