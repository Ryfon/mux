﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class PathPaintForm : Form
    {
        public static PathPaintForm Instance
        {
            get
            {
                if (m_Instance == null || m_Instance.IsDisposed)
                {
                    m_Instance = new PathPaintForm();
                }

                return m_Instance;
            }
        }

        public int Radius
        {
            get
            {
                return int.Parse(m_textBoxRadius.Text);
            }
            set
            {
                m_textBoxRadius.Text = value.ToString();
                m_trackBarRadius.Value = value;
            }
        }

        public Color PenColor
        {
            get
            {
                return m_pictureBoxPenColor.BackColor;
            }
        }

        public Color SmallColor
        {
            get
            {
                return m_pictureBoxSmallColor.BackColor;
            }
        }

        public Color BigColor
        {
            get
            {
                return m_pictureBoxBigColor.BackColor;
            }
        }

        public Color CurrColor
        {
            get
            {
                if(m_comboBoxCurrPen.SelectedIndex == 0)
                {
                    return m_pictureBoxPenColor.BackColor;
                }
                else if (m_comboBoxCurrPen.SelectedIndex == 1)
                {
                    return m_pictureBoxSmallColor.BackColor;
                }
                else if (m_comboBoxCurrPen.SelectedIndex == 2)
                {
                    return m_pictureBoxBigColor.BackColor;
                }
                else
                {
                    return m_pictureBoxPenColor.BackColor;
                }
            }
        }

        public int CurrPen
        {
            get
            {
                return m_comboBoxCurrPen.SelectedIndex;
            }
            set
            {
                m_comboBoxCurrPen.SelectedIndex = value;
            }
        }

        private PathPaintForm()
        {
            InitializeComponent();

            // 初始化下拉框
            m_comboBoxCurrPen.Items.Add("野地");
            m_comboBoxCurrPen.Items.Add("小路");
            m_comboBoxCurrPen.Items.Add("大路");
            m_comboBoxCurrPen.SelectedIndex = 0;

        }

        private void m_trackBarRadius_Scroll(object sender, EventArgs e)
        {
            m_textBoxRadius.Text = m_trackBarRadius.Value.ToString();
        }

        private void m_textBoxRadius_TextChanged(object sender, EventArgs e)
        {
            int iRadius = m_trackBarRadius.Value;
            
            try
            {
                iRadius = int.Parse(m_textBoxRadius.Text);
            }
            catch(Exception)
            {
                m_textBoxRadius.Text = m_trackBarRadius.Value.ToString();
            }
            
            if(iRadius >= m_trackBarRadius.Minimum && iRadius <= m_trackBarRadius.Maximum)
            {
                m_trackBarRadius.Value = iRadius;
            }
            else
            {
                m_textBoxRadius.Text = m_trackBarRadius.Value.ToString();
            }
        }

        private void m_buttonPenColor_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            DialogResult result = dlg.ShowDialog();
            if(result == DialogResult.OK)
            {
                m_pictureBoxPenColor.BackColor = dlg.Color;
            }
        }

        private void m_buttonSmallColor_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                m_pictureBoxSmallColor.BackColor = dlg.Color;
                SceneSketch scene = SceneSketchManager.Instance.GetSceneSketch(MainForm.Instance.GetCurrentMapID());
                if(scene != null)
                {
                    scene.ChangeColor(1, dlg.Color, 0.3f);
                }
            }
        }

        private void m_buttonBigColor_Click(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                m_pictureBoxBigColor.BackColor = dlg.Color;
                SceneSketch scene = SceneSketchManager.Instance.GetSceneSketch(MainForm.Instance.GetCurrentMapID());
                if(scene != null)
                {
                    scene.ChangeColor(2, dlg.Color, 0.3f);
                }
            }
        }
    }
}