﻿namespace MissionEditor
{
    partial class BaseDataEditForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_listViewSel = new System.Windows.Forms.ListView();
            this.m_listViewType = new System.Windows.Forms.ListView();
            this.m_groupBoxShow = new System.Windows.Forms.GroupBox();
            this.m_groupBoxEdit = new System.Windows.Forms.GroupBox();
            this.m_buttonSave = new System.Windows.Forms.Button();
            this.m_buttonDel = new System.Windows.Forms.Button();
            this.m_buttonEdit = new System.Windows.Forms.Button();
            this.m_buttonNew = new System.Windows.Forms.Button();
            this.m_buttonAddType = new System.Windows.Forms.Button();
            this.m_textBoxAtom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_textBoxParam = new System.Windows.Forms.TextBox();
            this.m_textBoxDesc = new System.Windows.Forms.TextBox();
            this.m_textBoxName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_comboBoxType = new System.Windows.Forms.ComboBox();
            this.m_groupBoxShow.SuspendLayout();
            this.m_groupBoxEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_listViewSel
            // 
            this.m_listViewSel.FullRowSelect = true;
            this.m_listViewSel.GridLines = true;
            this.m_listViewSel.HideSelection = false;
            this.m_listViewSel.Location = new System.Drawing.Point(115, 20);
            this.m_listViewSel.MultiSelect = false;
            this.m_listViewSel.Name = "m_listViewSel";
            this.m_listViewSel.Size = new System.Drawing.Size(545, 195);
            this.m_listViewSel.TabIndex = 0;
            this.m_listViewSel.UseCompatibleStateImageBehavior = false;
            this.m_listViewSel.View = System.Windows.Forms.View.Details;
            this.m_listViewSel.SelectedIndexChanged += new System.EventHandler(this.m_listViewSel_SelectedIndexChanged);
            // 
            // m_listViewType
            // 
            this.m_listViewType.FullRowSelect = true;
            this.m_listViewType.GridLines = true;
            this.m_listViewType.HideSelection = false;
            this.m_listViewType.Location = new System.Drawing.Point(10, 20);
            this.m_listViewType.MultiSelect = false;
            this.m_listViewType.Name = "m_listViewType";
            this.m_listViewType.Size = new System.Drawing.Size(100, 195);
            this.m_listViewType.TabIndex = 1;
            this.m_listViewType.UseCompatibleStateImageBehavior = false;
            this.m_listViewType.View = System.Windows.Forms.View.Details;
            this.m_listViewType.SelectedIndexChanged += new System.EventHandler(this.m_listViewType_SelectedIndexChanged);
            // 
            // m_groupBoxShow
            // 
            this.m_groupBoxShow.Controls.Add(this.m_listViewType);
            this.m_groupBoxShow.Controls.Add(this.m_listViewSel);
            this.m_groupBoxShow.Location = new System.Drawing.Point(10, 10);
            this.m_groupBoxShow.Name = "m_groupBoxShow";
            this.m_groupBoxShow.Size = new System.Drawing.Size(670, 225);
            this.m_groupBoxShow.TabIndex = 2;
            this.m_groupBoxShow.TabStop = false;
            this.m_groupBoxShow.Text = "设置";
            // 
            // m_groupBoxEdit
            // 
            this.m_groupBoxEdit.Controls.Add(this.m_buttonSave);
            this.m_groupBoxEdit.Controls.Add(this.m_buttonDel);
            this.m_groupBoxEdit.Controls.Add(this.m_buttonEdit);
            this.m_groupBoxEdit.Controls.Add(this.m_buttonNew);
            this.m_groupBoxEdit.Controls.Add(this.m_buttonAddType);
            this.m_groupBoxEdit.Controls.Add(this.m_textBoxAtom);
            this.m_groupBoxEdit.Controls.Add(this.label5);
            this.m_groupBoxEdit.Controls.Add(this.m_textBoxParam);
            this.m_groupBoxEdit.Controls.Add(this.m_textBoxDesc);
            this.m_groupBoxEdit.Controls.Add(this.m_textBoxName);
            this.m_groupBoxEdit.Controls.Add(this.label4);
            this.m_groupBoxEdit.Controls.Add(this.label3);
            this.m_groupBoxEdit.Controls.Add(this.label2);
            this.m_groupBoxEdit.Controls.Add(this.label1);
            this.m_groupBoxEdit.Controls.Add(this.m_comboBoxType);
            this.m_groupBoxEdit.Location = new System.Drawing.Point(10, 240);
            this.m_groupBoxEdit.Name = "m_groupBoxEdit";
            this.m_groupBoxEdit.Size = new System.Drawing.Size(670, 240);
            this.m_groupBoxEdit.TabIndex = 3;
            this.m_groupBoxEdit.TabStop = false;
            this.m_groupBoxEdit.Text = "编辑";
            // 
            // m_buttonSave
            // 
            this.m_buttonSave.Location = new System.Drawing.Point(610, 15);
            this.m_buttonSave.Name = "m_buttonSave";
            this.m_buttonSave.Size = new System.Drawing.Size(50, 23);
            this.m_buttonSave.TabIndex = 15;
            this.m_buttonSave.Text = "保存";
            this.m_buttonSave.UseVisualStyleBackColor = true;
            this.m_buttonSave.Click += new System.EventHandler(this.m_buttonSave_Click);
            // 
            // m_buttonDel
            // 
            this.m_buttonDel.Location = new System.Drawing.Point(550, 15);
            this.m_buttonDel.Name = "m_buttonDel";
            this.m_buttonDel.Size = new System.Drawing.Size(50, 23);
            this.m_buttonDel.TabIndex = 14;
            this.m_buttonDel.Text = "删除";
            this.m_buttonDel.UseVisualStyleBackColor = true;
            this.m_buttonDel.Click += new System.EventHandler(this.m_buttonDel_Click);
            // 
            // m_buttonEdit
            // 
            this.m_buttonEdit.Location = new System.Drawing.Point(490, 15);
            this.m_buttonEdit.Name = "m_buttonEdit";
            this.m_buttonEdit.Size = new System.Drawing.Size(50, 23);
            this.m_buttonEdit.TabIndex = 13;
            this.m_buttonEdit.Text = "修改";
            this.m_buttonEdit.UseVisualStyleBackColor = true;
            this.m_buttonEdit.Click += new System.EventHandler(this.m_buttonEdit_Click);
            // 
            // m_buttonNew
            // 
            this.m_buttonNew.Location = new System.Drawing.Point(430, 15);
            this.m_buttonNew.Name = "m_buttonNew";
            this.m_buttonNew.Size = new System.Drawing.Size(50, 23);
            this.m_buttonNew.TabIndex = 12;
            this.m_buttonNew.Text = "新建";
            this.m_buttonNew.UseVisualStyleBackColor = true;
            this.m_buttonNew.Click += new System.EventHandler(this.m_buttonNew_Click);
            // 
            // m_buttonAddType
            // 
            this.m_buttonAddType.Location = new System.Drawing.Point(220, 16);
            this.m_buttonAddType.Name = "m_buttonAddType";
            this.m_buttonAddType.Size = new System.Drawing.Size(20, 20);
            this.m_buttonAddType.TabIndex = 11;
            this.m_buttonAddType.Text = "+";
            this.m_buttonAddType.UseVisualStyleBackColor = true;
            this.m_buttonAddType.Click += new System.EventHandler(this.m_buttonAddType_Click);
            // 
            // m_textBoxAtom
            // 
            this.m_textBoxAtom.Location = new System.Drawing.Point(290, 42);
            this.m_textBoxAtom.Name = "m_textBoxAtom";
            this.m_textBoxAtom.Size = new System.Drawing.Size(370, 21);
            this.m_textBoxAtom.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(225, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "原子操作:";
            // 
            // m_textBoxParam
            // 
            this.m_textBoxParam.AcceptsReturn = true;
            this.m_textBoxParam.Location = new System.Drawing.Point(70, 155);
            this.m_textBoxParam.Multiline = true;
            this.m_textBoxParam.Name = "m_textBoxParam";
            this.m_textBoxParam.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_textBoxParam.Size = new System.Drawing.Size(590, 80);
            this.m_textBoxParam.TabIndex = 8;
            // 
            // m_textBoxDesc
            // 
            this.m_textBoxDesc.AcceptsReturn = true;
            this.m_textBoxDesc.Location = new System.Drawing.Point(70, 70);
            this.m_textBoxDesc.Multiline = true;
            this.m_textBoxDesc.Name = "m_textBoxDesc";
            this.m_textBoxDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_textBoxDesc.Size = new System.Drawing.Size(590, 80);
            this.m_textBoxDesc.TabIndex = 7;
            // 
            // m_textBoxName
            // 
            this.m_textBoxName.Location = new System.Drawing.Point(70, 41);
            this.m_textBoxName.Name = "m_textBoxName";
            this.m_textBoxName.Size = new System.Drawing.Size(150, 21);
            this.m_textBoxName.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "参数说明:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "功能描述:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "显示名称:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "显示分类:";
            // 
            // m_comboBoxType
            // 
            this.m_comboBoxType.FormattingEnabled = true;
            this.m_comboBoxType.Location = new System.Drawing.Point(70, 16);
            this.m_comboBoxType.Name = "m_comboBoxType";
            this.m_comboBoxType.Size = new System.Drawing.Size(150, 20);
            this.m_comboBoxType.TabIndex = 0;
            // 
            // BaseDataEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 492);
            this.Controls.Add(this.m_groupBoxEdit);
            this.Controls.Add(this.m_groupBoxShow);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BaseDataEditForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "编辑";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BaseDataEditForm_FormClosed);
            this.m_groupBoxShow.ResumeLayout(false);
            this.m_groupBoxEdit.ResumeLayout(false);
            this.m_groupBoxEdit.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView m_listViewSel;
        private System.Windows.Forms.ListView m_listViewType;
        private System.Windows.Forms.GroupBox m_groupBoxShow;
        private System.Windows.Forms.GroupBox m_groupBoxEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox m_comboBoxType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_textBoxName;
        private System.Windows.Forms.TextBox m_textBoxDesc;
        private System.Windows.Forms.TextBox m_textBoxParam;
        private System.Windows.Forms.Button m_buttonAddType;
        private System.Windows.Forms.TextBox m_textBoxAtom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button m_buttonNew;
        private System.Windows.Forms.Button m_buttonSave;
        private System.Windows.Forms.Button m_buttonDel;
        private System.Windows.Forms.Button m_buttonEdit;
    }
}