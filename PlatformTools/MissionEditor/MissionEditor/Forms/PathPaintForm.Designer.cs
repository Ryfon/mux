﻿namespace MissionEditor
{
    partial class PathPaintForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_trackBarRadius = new System.Windows.Forms.TrackBar();
            this.m_textBoxRadius = new System.Windows.Forms.TextBox();
            this.m_buttonPenColor = new System.Windows.Forms.Button();
            this.m_pictureBoxPenColor = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_buttonSmallColor = new System.Windows.Forms.Button();
            this.m_buttonBigColor = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.m_pictureBoxSmallColor = new System.Windows.Forms.PictureBox();
            this.m_pictureBoxBigColor = new System.Windows.Forms.PictureBox();
            this.m_comboBoxCurrPen = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.m_trackBarRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxPenColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxSmallColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxBigColor)).BeginInit();
            this.SuspendLayout();
            // 
            // m_trackBarRadius
            // 
            this.m_trackBarRadius.Location = new System.Drawing.Point(88, 160);
            this.m_trackBarRadius.Maximum = 20;
            this.m_trackBarRadius.Name = "m_trackBarRadius";
            this.m_trackBarRadius.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.m_trackBarRadius.Size = new System.Drawing.Size(150, 45);
            this.m_trackBarRadius.TabIndex = 0;
            this.m_trackBarRadius.Value = 5;
            this.m_trackBarRadius.Scroll += new System.EventHandler(this.m_trackBarRadius_Scroll);
            // 
            // m_textBoxRadius
            // 
            this.m_textBoxRadius.Location = new System.Drawing.Point(240, 162);
            this.m_textBoxRadius.Name = "m_textBoxRadius";
            this.m_textBoxRadius.Size = new System.Drawing.Size(30, 21);
            this.m_textBoxRadius.TabIndex = 1;
            this.m_textBoxRadius.Text = "5";
            this.m_textBoxRadius.TextChanged += new System.EventHandler(this.m_textBoxRadius_TextChanged);
            // 
            // m_buttonPenColor
            // 
            this.m_buttonPenColor.Location = new System.Drawing.Point(100, 12);
            this.m_buttonPenColor.Name = "m_buttonPenColor";
            this.m_buttonPenColor.Size = new System.Drawing.Size(75, 23);
            this.m_buttonPenColor.TabIndex = 2;
            this.m_buttonPenColor.Text = "更改颜色";
            this.m_buttonPenColor.UseVisualStyleBackColor = true;
            this.m_buttonPenColor.Click += new System.EventHandler(this.m_buttonPenColor_Click);
            // 
            // m_pictureBoxPenColor
            // 
            this.m_pictureBoxPenColor.BackColor = System.Drawing.Color.Red;
            this.m_pictureBoxPenColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxPenColor.Location = new System.Drawing.Point(240, 13);
            this.m_pictureBoxPenColor.Name = "m_pictureBoxPenColor";
            this.m_pictureBoxPenColor.Size = new System.Drawing.Size(30, 21);
            this.m_pictureBoxPenColor.TabIndex = 3;
            this.m_pictureBoxPenColor.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "笔刷颜色:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "笔刷半径:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "小路颜色:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "大路颜色:";
            // 
            // m_buttonSmallColor
            // 
            this.m_buttonSmallColor.Location = new System.Drawing.Point(100, 46);
            this.m_buttonSmallColor.Name = "m_buttonSmallColor";
            this.m_buttonSmallColor.Size = new System.Drawing.Size(75, 23);
            this.m_buttonSmallColor.TabIndex = 8;
            this.m_buttonSmallColor.Text = "更改颜色";
            this.m_buttonSmallColor.UseVisualStyleBackColor = true;
            this.m_buttonSmallColor.Click += new System.EventHandler(this.m_buttonSmallColor_Click);
            // 
            // m_buttonBigColor
            // 
            this.m_buttonBigColor.Location = new System.Drawing.Point(100, 81);
            this.m_buttonBigColor.Name = "m_buttonBigColor";
            this.m_buttonBigColor.Size = new System.Drawing.Size(75, 23);
            this.m_buttonBigColor.TabIndex = 9;
            this.m_buttonBigColor.Text = "更改颜色";
            this.m_buttonBigColor.UseVisualStyleBackColor = true;
            this.m_buttonBigColor.Click += new System.EventHandler(this.m_buttonBigColor_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "当前笔刷:";
            // 
            // m_pictureBoxSmallColor
            // 
            this.m_pictureBoxSmallColor.BackColor = System.Drawing.Color.Yellow;
            this.m_pictureBoxSmallColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxSmallColor.Location = new System.Drawing.Point(240, 47);
            this.m_pictureBoxSmallColor.Name = "m_pictureBoxSmallColor";
            this.m_pictureBoxSmallColor.Size = new System.Drawing.Size(30, 21);
            this.m_pictureBoxSmallColor.TabIndex = 11;
            this.m_pictureBoxSmallColor.TabStop = false;
            // 
            // m_pictureBoxBigColor
            // 
            this.m_pictureBoxBigColor.BackColor = System.Drawing.Color.Green;
            this.m_pictureBoxBigColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_pictureBoxBigColor.Location = new System.Drawing.Point(240, 82);
            this.m_pictureBoxBigColor.Name = "m_pictureBoxBigColor";
            this.m_pictureBoxBigColor.Size = new System.Drawing.Size(30, 21);
            this.m_pictureBoxBigColor.TabIndex = 12;
            this.m_pictureBoxBigColor.TabStop = false;
            // 
            // m_comboBoxCurrPen
            // 
            this.m_comboBoxCurrPen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxCurrPen.FormattingEnabled = true;
            this.m_comboBoxCurrPen.Location = new System.Drawing.Point(100, 225);
            this.m_comboBoxCurrPen.Name = "m_comboBoxCurrPen";
            this.m_comboBoxCurrPen.Size = new System.Drawing.Size(121, 20);
            this.m_comboBoxCurrPen.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(217, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "20";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(156, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 16;
            this.label8.Text = "10";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(97, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 12);
            this.label9.TabIndex = 17;
            this.label9.Text = "0";
            // 
            // PathPaintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.ControlBox = false;
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.m_comboBoxCurrPen);
            this.Controls.Add(this.m_pictureBoxBigColor);
            this.Controls.Add(this.m_pictureBoxSmallColor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_buttonBigColor);
            this.Controls.Add(this.m_buttonSmallColor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_pictureBoxPenColor);
            this.Controls.Add(this.m_buttonPenColor);
            this.Controls.Add(this.m_textBoxRadius);
            this.Controls.Add(this.m_trackBarRadius);
            this.Name = "PathPaintForm";
            this.Text = "PathPaintForm";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.m_trackBarRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxPenColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxSmallColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxBigColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private static PathPaintForm m_Instance = null;
        private System.Windows.Forms.TrackBar m_trackBarRadius;
        private System.Windows.Forms.TextBox m_textBoxRadius;
        private System.Windows.Forms.Button m_buttonPenColor;
        private System.Windows.Forms.PictureBox m_pictureBoxPenColor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button m_buttonSmallColor;
        private System.Windows.Forms.Button m_buttonBigColor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox m_pictureBoxSmallColor;
        private System.Windows.Forms.PictureBox m_pictureBoxBigColor;
        private System.Windows.Forms.ComboBox m_comboBoxCurrPen;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}