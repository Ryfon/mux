﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class MissionConditionForm : Form
    {
        public MissionConditionForm(ListBox.ObjectCollection conditions, ListBox.ObjectCollection values)
        {
            InitializeComponent();

            // 将参数中的 conditions, values 复制到窗口中
            m_listBoxSelectedConditions.Items.Clear();
            m_listBoxValue.Items.Clear();
            foreach (String strCondition in conditions)
            {
                m_listBoxSelectedConditions.Items.Add(strCondition);
            }

            foreach (String strValue in values)
            {
                m_listBoxValue.Items.Add(strValue);
            }

        }

        private void MissionConditionForm_Load(object sender, EventArgs e)
        {
            // 从 MissionRequirementManager 中找到第一级分类,添加到 m_comboBoxFilter 中
            foreach (String str1stLevel in MissionConditionManager.Instance.ConditionsMap.Keys)
            {
                m_comboBoxFilter.Items.Add(str1stLevel);
            }
        }

        private void SetConditionAttribute(MissionConditionInstance mci)
        {
            if(mci == null)
            {
                m_textBoxConditionDesc.Text = "";
                m_textBoxConditionParam.Text = "";
            }
            else
            {
                m_textBoxConditionDesc.Text = mci.Desc;
                m_textBoxConditionParam.Text = mci.Param;
            }
        }

        private void m_comboBoxFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_comboBoxFilter.SelectedIndex<0)
            {
                return;
            }

            String strFilter = m_comboBoxFilter.SelectedItem.ToString();

            // 从 MissionConditionManager 中找到 strFilter 的下一级 condition 列表
            Hashtable conditionMap = MissionConditionManager.Instance.ConditionsMap[strFilter] as Hashtable;
            if (conditionMap != null)
            {
                m_listBoxAllConditions.Items.Clear();
                foreach (String strCondition in conditionMap.Keys)
                {
                    m_listBoxAllConditions.Items.Add(strCondition);
                }
            }

            // 更新任务条件描述
            SetConditionAttribute(null);
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            // 添加 condition
            if (m_listBoxAllConditions.SelectedItem == null)
            {
                return;
            }

            String strCondition = m_listBoxAllConditions.SelectedItem.ToString();
            if (strCondition == null || m_listBoxSelectedConditions.Items.Contains(strCondition))
            {
                return;
            }

            try
            {
                m_listBoxSelectedConditions.Items.Add(strCondition);
                m_listBoxValue.Items.Add(m_textBoxValue.Text);

            }
            catch(Exception)
            {
                MessageBox.Show("请检查 对应值 的合法性.");
                return;
            }
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            // 删除 condition
            int iIdx = m_listBoxSelectedConditions.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxValue.Items.RemoveAt(iIdx);
                m_listBoxSelectedConditions.Items.RemoveAt(iIdx);
            }
        }

        private void m_listBoxSelectedConditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iIdx = m_listBoxSelectedConditions.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxValue.SelectedIndex = iIdx;
            }

            // 更新任务条件描述
            if (m_listBoxSelectedConditions.SelectedItem != null)
            {
                String strConditionName = m_listBoxSelectedConditions.SelectedItem.ToString();
                MissionConditionInstance mci = MissionConditionManager.Instance.GetConditionByDisplay(strConditionName);
                SetConditionAttribute(mci);
            }
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public ListBox.ObjectCollection Conditions
        {
            get
            {
                return m_listBoxSelectedConditions.Items;
            }
        }

        public ListBox.ObjectCollection Values
        {
            get
            {
                return m_listBoxValue.Items;
            }
        }

        private void m_listBoxAllConditions_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 更新任务条件描述
            if(m_listBoxAllConditions.SelectedItem != null)
            {
                String strConditionName = m_listBoxAllConditions.SelectedItem.ToString();
                MissionConditionInstance mci = MissionConditionManager.Instance.GetConditionByDisplay(strConditionName);
                SetConditionAttribute(mci);
            }
        }

    }
}