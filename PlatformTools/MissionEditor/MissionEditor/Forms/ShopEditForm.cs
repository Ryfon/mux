﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class ShopEditForm : Form
    {
        public ShopEditForm()
        {
            InitializeComponent();
        }

        private void ShopEditForm_Load(object sender, EventArgs e)
        {
            // 刷新 item list
            m_listBoxAllItems.Items.Clear();
            foreach (Item item in ItemManager.Instance.ItemList)
            {
                m_listBoxAllItems.Items.Add(MainForm.GetDisplayText(item.ID, item.Name, OptionManager.DisplayMode.DM_ID_NAME));
            }

            // 刷新 skill list
            m_listBoxAllSkills.Items.Clear();
            foreach(Skill skill in SkillManager.Instance.Skills.Values)
            {
                m_listBoxAllSkills.Items.Add(MainForm.GetDisplayText(skill.ID, skill.Name, OptionManager.DisplayMode.DM_ID_NAME));
            }

            // 填充 produce type combobox
            String[] strNames = Enum.GetNames(typeof(CurrencyType));
            foreach (String strName in strNames)
            {
                m_comboBoxCurrencyType.Items.Add(strName);
                m_comboBoxSkillCurrencyType.Items.Add(strName);
            }

            m_comboBoxCurrencyType.SelectedIndex = 0;

            RefreshShopList();
        }

        private void RefreshShopList()
        {
            // 刷新 商店列表
            m_listBoxItemShop.Items.Clear();
            foreach (ItemShop shop in ItemShopManager.Instance.ShopList.Values)
            {
                m_listBoxItemShop.Items.Add(MainForm.GetDisplayText(shop.ID, shop.Desc, OptionManager.DisplayMode.DM_ID_NAME));
            }

            // 刷新技能商店列表
            m_listBoxSkillShop.Items.Clear();
            foreach (SkillShop shop in SkillShopManager.Instance.ShopList.Values)
            {
                m_listBoxSkillShop.Items.Add(MainForm.GetDisplayText(shop.ID, shop.Desc, OptionManager.DisplayMode.DM_ID_NAME));
            }
        }

        // 当前 OnEditItemShop 被编辑的 table
        public Hashtable CurrentItemTable
        {
            get
            {
                if (m_OnEditItemShop == null || m_listBoxItemTable.SelectedIndex < 0)
                {
                    return null;
                }
                String strTableName = m_listBoxItemTable.SelectedItem.ToString();
                Hashtable itemTable = m_OnEditItemShop.TableList[strTableName] as Hashtable;

                return itemTable;
            }
        }

        // 当前 OnEditSkillShop 被编辑的 table
        public Hashtable CurrentSkillTable
        {
            get
            {
                if (m_OnEditSkillShop == null || m_listBoxSkillTable.SelectedIndex < 0)
                {
                    return null;
                }
                String strTableName = m_listBoxSkillTable.SelectedItem.ToString();
                Hashtable skillTable = m_OnEditSkillShop.Tables[strTableName] as Hashtable;
                return skillTable;
            }
        }

        // 添加 item 到当前 shop
        private void m_btnAdd_Click(object sender, EventArgs e)
        {

            if (m_OnEditItemShop == null)
            {
                MessageBox.Show("请先选择一个商店。");
                return;
            }

            if (m_listBoxItemTable.SelectedIndex == -1)
            {
                MessageBox.Show("请先选择一 Table。");
                return;
            }

            if (m_listBoxAllItems.SelectedItems.Count == 0)
            {
                return;
            }
            
            try
            {
                int iPerPurchaseNum = int.Parse(m_textBoxPerPurchaseNum.Text);
                String strTradePrice = m_textBoxItemTradePrice.Text;
                String strProduceType = m_textBoxProduceType.Text;
                String strDailyProduceTime = m_textBoxDailyProduceTime.Text;
                String strProduceNum = m_textBoxProduceNum.Text;
                String strCapability = m_textBoxCapability.Text;

                String strTableName = m_listBoxItemTable.SelectedItem.ToString();
                Hashtable tableItemList = m_OnEditItemShop.TableList[strTableName] as Hashtable;
                if (tableItemList == null)
                {
                    return;
                }

                foreach (String strSelected in m_listBoxAllItems.SelectedItems)
                {
                    int iItemID = MainForm.ExtractItemIDFromDisplayText(strSelected);
                    if (tableItemList.ContainsKey(iItemID))
                    {
                        continue;
                    }
                    ShopItem shopItem = new ShopItem(iItemID, iPerPurchaseNum, strTradePrice, 
                                    strProduceType, strDailyProduceTime, strProduceNum, strCapability);
                    tableItemList[iItemID] = shopItem;
                }
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }

            //ShowShop(m_OnEditItemShop);
            ShowSelectedTableItems();
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            if (m_OnEditItemShop == null)
            {
                MessageBox.Show("请先选择一个商店。");
                return;
            }

            if (m_listBoxItemTable.SelectedIndex == -1)
            {
                MessageBox.Show("请先选择一 Table。");
                return;
            }

            if (m_listBoxSelectedItems.SelectedItems.Count == 0)
            {
                return;
            }

            String strTableName = m_listBoxItemTable.SelectedItem.ToString();
            Hashtable tableItemList = m_OnEditItemShop.TableList[strTableName] as Hashtable;
            if (tableItemList == null)
            {
                return;
            }

            ArrayList removeList = new ArrayList();
            foreach (String strSelItem in m_listBoxSelectedItems.SelectedItems)
            {
                int iItemID = MainForm.ExtractItemIDFromDisplayText(strSelItem);
                tableItemList.Remove(iItemID);
            }

            ShowSelectedTableItems();
        }

        private void m_btnAddShop_Click(object sender, EventArgs e)
        {
            try
            {
                int iShopID = int.Parse(m_textBoxItemShopID.Text);
                String strShopDesc = m_textBoxItemShopDesc.Text;
                float fPriceRate = float.Parse(m_textBoxItemShopPriceRate.Text);
                CurrencyType eCurrType = (CurrencyType)m_comboBoxCurrencyType.SelectedIndex;
                int iTradeItemID = int.Parse(m_textBoxItemTradeItemID.Text);

                // 是否已存在该 ID 的 shop
                if (ItemShopManager.Instance.ShopList.ContainsKey(iShopID))
                {
                    // 修改已有的商店]
                    ItemShop shop = ItemShopManager.Instance.ShopList[iShopID] as ItemShop;
                    shop.Desc = strShopDesc;
                    shop.PriceRate = fPriceRate;
                    shop.ShopCurrencyType = eCurrType;
                    shop.TradeItemID = iTradeItemID;
                }
                else
                {
                    // 添加新的商店
                    ItemShop shop = new ItemShop(iShopID, strShopDesc, fPriceRate, eCurrType, iTradeItemID);
                    ItemShopManager.Instance.ShopList[iShopID] = shop;
                }

                RefreshShopList();
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void ShowShop(ItemShop shop)
        {
            // 将商店属性写入 UI
            m_textBoxItemShopID.Text = shop.ID.ToString();
            m_textBoxItemShopDesc.Text = shop.Desc;
            m_textBoxItemShopPriceRate.Text = shop.PriceRate.ToString();
            m_comboBoxCurrencyType.SelectedIndex = (int)shop.ShopCurrencyType;
            m_textBoxItemTradeItemID.Text = shop.TradeItemID.ToString();

            // 清空 list box
            m_listBoxItemTable.Items.Clear();
            m_listBoxSelectedItems.Items.Clear();
            m_listBoxPerPurchaseNum.Items.Clear();
            m_listBoxItemTradePrice.Items.Clear();
            m_listBoxProduceType.Items.Clear();
            m_listBoxDailyProduceTime.Items.Clear();
            m_listBoxProduceNum.Items.Clear();
            m_listBoxCapability.Items.Clear();

            // 显示 table list
            foreach (String strTabName in shop.TableList.Keys)
            {
                m_listBoxItemTable.Items.Add(strTabName);
            }

            if (m_listBoxItemTable.Items.Count > 0)
            {
                m_listBoxItemTable.SelectedIndex = 0;
            }
        }

        private void m_listBoxShop_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 选择商店
            if (m_listBoxItemShop.SelectedIndex < 0)
            {
                return;
            }

            String strDisplay = m_listBoxItemShop.SelectedItem.ToString();
            int iShopID = MainForm.ExtractItemIDFromDisplayText(strDisplay);
            ItemShop shop = ItemShopManager.Instance.ShopList[iShopID] as ItemShop;

            if (shop != null)
            {
                m_OnEditItemShop = shop;
                ShowShop(m_OnEditItemShop);
            }

        }


        private void m_listBoxSelectedItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 选择 ShopItem
            int iIdx = m_listBoxSelectedItems.SelectedIndex;
            if (iIdx >= 0)
            {
                m_listBoxPerPurchaseNum.SelectedIndex = iIdx;
                m_listBoxItemTradePrice.SelectedIndex = iIdx;
                m_listBoxProduceType.SelectedIndex = iIdx;
                m_listBoxDailyProduceTime.SelectedIndex = iIdx;
                m_listBoxProduceNum.SelectedIndex = iIdx;
                m_listBoxCapability.SelectedIndex = iIdx;
            }
        }

        private void m_btnSave_Click(object sender, EventArgs e)
        {
            // 保存商店到 xml
            ItemShopManager.Instance.SaveToXml(OptionManager.ItemShopPath);
        }

        //private void m_listBoxPrice_DoubleClick(object sender, EventArgs e)
        //{
        //    // 修改 price
        //    int iIdx = m_listBoxItemPrice.SelectedIndex;
        //    if (iIdx < 0)
        //    {
        //        return;
        //    }

        //    NameInputForm form = new NameInputForm(m_listBoxItemPrice.SelectedItem.ToString());
        //    form.ShowDialog();

        //    try
        //    {
        //        if (form.IsOk)
        //        {
        //            int iPrice = int.Parse(form.InputName);
        //            m_listBoxItemPrice.Items[iIdx] = iPrice.ToString();
        //            int iItemID = MainForm.ExtractItemIDFromDisplayText(m_listBoxSelectedItems.Items[iIdx].ToString());
        //            (m_OnEditShop.ItemList[iItemID] as ShopItem).Price = iPrice;
        //        }
        //    }
        //    catch(Exception e2)
        //    {
        //        MessageBox.Show(e2.ToString());
        //    }

        //}

        private void 全选ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            ContextMenuStrip menu = menuItem.GetCurrentParent() as ContextMenuStrip;

            if (menu.SourceControl == m_listBoxAllItems)
            {
                for (int i=0; i<m_listBoxAllItems.Items.Count; i++)
                {
                    m_listBoxAllItems.SetSelected(i, true);
                }
            }
            else if (menu.SourceControl == m_listBoxSelectedItems)
            {
                for (int i = 0; i < m_listBoxSelectedItems.Items.Count; i++)
                {
                    m_listBoxSelectedItems.SetSelected(i, true);
                }
            }
            else if(menu.SourceControl == m_listBoxAllSkills)
            {
                for (int i = 0; i < m_listBoxAllSkills.Items.Count; i++)
                {
                    m_listBoxAllSkills.SetSelected(i, true);
                }
            }
            else if (menu.SourceControl == m_listBoxSelectedSkills)
            {
                for (int i = 0; i < m_listBoxSelectedSkills.Items.Count; i++)
                {
                    m_listBoxSelectedSkills.SetSelected(i, true);
                }
            }
        }

        private void 清除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            ContextMenuStrip menu = menuItem.GetCurrentParent() as ContextMenuStrip;

            if (menu.SourceControl == m_listBoxAllItems)
            {
                for (int i = 0; i < m_listBoxAllItems.Items.Count; i++)
                {
                    m_listBoxAllItems.SetSelected(i, false);
                }
            }
            else if (menu.SourceControl == m_listBoxSelectedItems)
            {
                for (int i = 0; i < m_listBoxSelectedItems.Items.Count; i++)
                {
                    m_listBoxSelectedItems.SetSelected(i, false);
                }
            }
            else if (menu.SourceControl == m_listBoxAllSkills)
            {
                for (int i = 0; i < m_listBoxAllSkills.Items.Count; i++)
                {
                    m_listBoxAllSkills.SetSelected(i, false);
                }
            }
            else if (menu.SourceControl == m_listBoxSelectedSkills)
            {
                for (int i = 0; i < m_listBoxSelectedSkills.Items.Count; i++)
                {
                    m_listBoxSelectedSkills.SetSelected(i, false);
                }
            }
        }

        #region SkillShop相关
        // 添加技能商店
        private void m_btnAddSkillShop_Click(object sender, EventArgs e)
        {
            try
            {
                int iShopID = int.Parse(m_textBoxSkillShopID.Text);
                String strDesc = m_textBoxSkillShopDesc.Text;
                float fPriceRate = float.Parse(m_textBoxSkillShopPriceRate.Text);
                CurrencyType eCurrencyType = (CurrencyType)m_comboBoxSkillCurrencyType.SelectedIndex;
                int iTradeItemID = int.Parse(m_textBoxSkillTradeItemID.Text);

                if (SkillShopManager.Instance.ShopList.ContainsKey(iShopID))
                {
                    // 之前已存在该 shop
                    SkillShop shop = SkillShopManager.Instance.ShopList[iShopID] as SkillShop;
                    shop.Desc = strDesc;
                    shop.PriceRate = fPriceRate;
                    shop.ShopCurrencyType = eCurrencyType;
                    shop.TradeItemID = iTradeItemID;
                }
                else
                {
                    SkillShop shop = new SkillShop(iShopID, strDesc, fPriceRate, eCurrencyType, iTradeItemID);
                    SkillShopManager.Instance.AddSkillShop(shop);
                }
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
            RefreshShopList();
        }

        // 添加技能到商店
        private void m_btnAddSkill_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_listBoxAllSkills.SelectedItems.Count == 0)
                {
                    return;
                }

                Hashtable skillTable = CurrentSkillTable;
                if (skillTable == null)
                {
                    return;
                }

                int iRequireLevel = 0;
                String strTradePrice = m_textBoxSkillTradePrice.Text;
                String strProfession = m_textBoxSkillProfession.Text;
                try
                {
                    iRequireLevel = int.Parse(m_textBoxSkillLevel.Text);
                }
                catch(Exception e2)
                {
                    MessageBox.Show(e2.ToString());
                    return;
                }


                foreach (String strSkill in m_listBoxAllSkills.SelectedItems)
                {
                    int iSkillID = MainForm.ExtractIDFromID_Name(strSkill);

                    if (!skillTable.ContainsKey(iSkillID))
                    {
                        // 如果未包含该 skill, 添加到 OnEditSkillShop
                        ShopSkill shopSkill = new ShopSkill(iSkillID, iRequireLevel, strTradePrice, strProfession);
                        skillTable[iSkillID] = shopSkill;
                    }
                }

                ShowSelectedTableSkills();
            }
            catch (Exception e3)
            {
                MessageBox.Show(e3.ToString());
                return;
            }
        }

        // 从商店中删除技能
        private void m_btnRemoveSkill_Click(object sender, EventArgs e)
        {
            if (m_listBoxAllSkills.SelectedItems.Count == 0)
            {
                return;
            }

            Hashtable skillTable = CurrentSkillTable;
            if (skillTable == null)
            {
                return;
            }

            foreach (String strSkill in m_listBoxSelectedSkills.SelectedItems)
            {
                int iSkillID = MainForm.ExtractIDFromID_Name(strSkill);
                skillTable.Remove(iSkillID);
            }

            ShowSelectedTableSkills();
        }

        public void ShowSkillShop(SkillShop shop)
        {
            if (null == shop)
            {
                return;
            }

            // 将 skillShop 信息写入 UI
            m_textBoxSkillShopID.Text = shop.ID.ToString();
            m_textBoxSkillShopDesc.Text = shop.Desc;
            m_textBoxSkillShopPriceRate.Text = shop.PriceRate.ToString();
            m_comboBoxSkillCurrencyType.SelectedIndex = (int)shop.ShopCurrencyType;
            m_textBoxSkillTradeItemID.Text = shop.TradeItemID.ToString();

            // 清除 listBox
            m_listBoxSkillTable.Items.Clear();
            m_listBoxSelectedSkills.Items.Clear();
            m_listBoxSkillLevel.Items.Clear();
            m_listBoxSkillTradePrice.Items.Clear();
            m_listBoxSkillProfession.Items.Clear();

            // 填充 skill table list
            foreach (String strTabName in shop.Tables.Keys)
            {
                m_listBoxSkillTable.Items.Add(strTabName);
            }

            //foreach (ShopSkill shopSkill in shop.Skills.Values)
            //{
            //    Skill skill = SkillManager.Instance.Skills[shopSkill.SkillID] as Skill;
            //    m_listBoxSelectedSkills.Items.Add(MainForm.GetDisplayText(skill.ID, skill.Name, OptionManager.DisplayMode.DM_ID_NAME));
            //    m_listBoxSkillLevel.Items.Add(shopSkill.Level.ToString());
            //    m_listBoxSkillGoldPrice.Items.Add(shopSkill.GoldPrice);
            //    m_listBoxSkillRMBPrice.Items.Add(shopSkill.RMBPrice);
            //    m_listBoxSkillTradeItemID.Items.Add(shopSkill.TradeItemID.ToString());
            //    m_listBoxSkillTradePrice.Items.Add(shopSkill.TradeItemNum);
            //}
        }

        // 选择技能商店
        private void m_listBoxSkillShop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_listBoxSkillShop.SelectedIndex < 0)
            {
                return;
            }

            int iShopID = MainForm.ExtractIDFromID_Name(m_listBoxSkillShop.SelectedItem.ToString());
            SkillShop shop = SkillShopManager.Instance.ShopList[iShopID] as SkillShop;
            m_OnEditSkillShop = shop;
            ShowSkillShop(shop);
        }

        // 保存技能商店
        private void m_btnSaveSkillShop_Click(object sender, EventArgs e)
        {
            SkillShopManager.Instance.SaveToXml(OptionManager.SkillShopPath);
        }

        private void m_listBoxSelectedSkills_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 选择一个商店中的 skill
            int iIdx = m_listBoxSelectedSkills.SelectedIndex;
            if (iIdx != -1)
            {
                m_listBoxSkillLevel.SelectedIndex = iIdx;
                m_listBoxSkillTradePrice.SelectedIndex = iIdx;
                m_listBoxSkillProfession.SelectedIndex = iIdx;
            }
        }

        #endregion

        #region 直接编辑ShopItem


        private void m_listBoxProduceNum_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int iIdx = m_listBoxProduceNum.SelectedIndex;
            if (iIdx < 0)
            {
                return;
            }

            Hashtable itemTable = CurrentItemTable;
            if (itemTable == null)
            {
                return;
            }

            String strProduceNum = m_listBoxProduceNum.SelectedItem.ToString();
            NameInputForm form = new NameInputForm(strProduceNum);
            form.ShowDialog();

            try
            {
                strProduceNum = form.InputName;
                int iItemID = MainForm.ExtractIDFromID_Name(m_listBoxSelectedItems.Items[iIdx].ToString());
                (itemTable[iItemID] as ShopItem).ProduceNum = strProduceNum;
            }
            catch (Exception)
            {
                return;
            }

            ShowShop(m_OnEditItemShop);
        }

        private void m_listBoxCapability_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int iIdx = m_listBoxCapability.SelectedIndex;
            if (iIdx < 0)
            {
                return;
            }

            Hashtable itemTable = CurrentItemTable;
            if (itemTable == null)
            {
                return;
            }

            String strCapability = m_listBoxCapability.SelectedItem.ToString();
            NameInputForm form = new NameInputForm(strCapability);
            form.ShowDialog();

            try
            {
                strCapability = form.InputName;
                int iItemID = MainForm.ExtractIDFromID_Name(m_listBoxSelectedItems.Items[iIdx].ToString());
                (itemTable[iItemID] as ShopItem).Capability = strCapability;
            }
            catch (Exception)
            {
                return;
            }

            ShowShop(m_OnEditItemShop);
        }
        #endregion

        // 编辑技能所需等级
        private void m_listBoxSkillLevel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int iIdx = m_listBoxSkillLevel.SelectedIndex;
            if (iIdx < 0)
            {
                return;
            }

            String strLevel = m_listBoxSkillLevel.SelectedItem.ToString();
            NameInputForm form = new NameInputForm(strLevel);
            form.ShowDialog();

            try
            {
                int iLevel = int.Parse(form.InputName);
                int iSkillID = MainForm.ExtractIDFromID_Name(m_listBoxSelectedSkills.Items[iIdx].ToString());
                //(m_OnEditSkillShop.Skills[iSkillID] as ShopSkill).Level = iLevel;
            }
            catch (Exception)
            {
                return;
            }

            ShowSkillShop(m_OnEditSkillShop);
        }

        private void ShowSelectedTableItems()
        {
            if (m_OnEditItemShop==null || m_listBoxItemTable.SelectedIndex==-1)
            {
                return;
            }

            m_listBoxSelectedItems.Items.Clear();
            m_listBoxPerPurchaseNum.Items.Clear();
            m_listBoxItemTradePrice.Items.Clear();
            m_listBoxProduceType.Items.Clear();
            m_listBoxDailyProduceTime.Items.Clear();
            m_listBoxProduceNum.Items.Clear();
            m_listBoxCapability.Items.Clear();

            // 显示该 Tab 对应的所有 shopItem
            Hashtable items = m_OnEditItemShop.TableList[m_listBoxItemTable.SelectedItem.ToString()] as Hashtable;
            ArrayList indexList = m_OnEditItemShop.IndexTables[m_listBoxItemTable.SelectedItem.ToString()] as ArrayList;
            if (items != null)
            {
                if(indexList != null)
                {
                    foreach (int iItemID in indexList)
                    {
                        ShopItem shopItem = items[iItemID] as ShopItem;
                        Item item = ItemManager.Instance.GetItemByID(iItemID);
                        if (item != null)
                        {
                            m_listBoxSelectedItems.Items.Add(MainForm.GetDisplayText(item.ID, item.Name, OptionManager.DisplayMode.DM_ID_NAME));
                            m_listBoxPerPurchaseNum.Items.Add(shopItem.PerPurchaseNum.ToString());
                            m_listBoxItemTradePrice.Items.Add(shopItem.TradePrice);
                            m_listBoxProduceType.Items.Add(shopItem.ProduceType);
                            m_listBoxDailyProduceTime.Items.Add(shopItem.DailyProduceTime);
                            m_listBoxProduceNum.Items.Add(shopItem.ProduceNum);
                            m_listBoxCapability.Items.Add(shopItem.Capability);
                        }
                    }
                }
                else
                {
                    foreach (ShopItem shopItem in items.Values)
                    {
                        Item item = ItemManager.Instance.GetItemByID(shopItem.ItemID);
                        if (item != null)
                        {
                            m_listBoxSelectedItems.Items.Add(MainForm.GetDisplayText(item.ID, item.Name, OptionManager.DisplayMode.DM_ID_NAME));
                            m_listBoxPerPurchaseNum.Items.Add(shopItem.PerPurchaseNum.ToString());
                            m_listBoxItemTradePrice.Items.Add(shopItem.TradePrice);
                            m_listBoxProduceType.Items.Add(shopItem.ProduceType);
                            m_listBoxDailyProduceTime.Items.Add(shopItem.DailyProduceTime);
                            m_listBoxProduceNum.Items.Add(shopItem.ProduceNum);
                            m_listBoxCapability.Items.Add(shopItem.Capability);
                        }
                    }
                }
            }
        }

        private void m_listBoxItemTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 选择一个 Tab
            ShowSelectedTableItems();
        }

        private void m_toolStripMenuItemAddTab_Click(object sender, EventArgs e)
        {

            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            ContextMenuStrip menu = menuItem.GetCurrentParent() as ContextMenuStrip;

            if (menu.SourceControl == m_listBoxItemTable)
            {
                if (m_OnEditItemShop==null)
                {
                    MessageBox.Show("请先选择一个商店再添加 Table.");
                    return;
                }

                // 添加 Tab
                NameInputForm form = new NameInputForm("");
                form.ShowDialog();

                String strTabName = form.InputName;
                if (strTabName.Length>0 && !m_OnEditItemShop.TableList.ContainsKey(strTabName))
                {
                    m_OnEditItemShop.TableList[strTabName] = new Hashtable();
                    m_listBoxItemTable.Items.Add(strTabName);
                }
            }
            else if(menu.SourceControl == m_listBoxSkillTable)
            {
                if (m_OnEditSkillShop == null)
                {
                    MessageBox.Show("请先选择一个技能商店再添加 Table.");
                    return;
                }

                // 添加 Tab
                NameInputForm form = new NameInputForm("");
                form.ShowDialog();

                String strTabName = form.InputName;
                if (strTabName.Length > 0 && !m_OnEditSkillShop.Tables.ContainsKey(strTabName))
                {
                    m_OnEditSkillShop.Tables[strTabName] = new Hashtable();
                    m_listBoxSkillTable.Items.Add(strTabName);
                }
            }


        }

        private void m_toolStripMenuItemDelTab_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            ContextMenuStrip menu = menuItem.GetCurrentParent() as ContextMenuStrip;

            if (menu.SourceControl == m_listBoxItemTable)
            {
                if (m_OnEditItemShop == null)
                {
                    MessageBox.Show("请先选择一个商店再删除 Table.");
                    return;
                }

                // 删除 Tab
                if (m_listBoxItemTable.SelectedIndex == -1)
                {
                    return;
                }

                DialogResult result = MessageBox.Show("确定要删除 Tab:" + m_listBoxItemTable.SelectedItem.ToString() + " 吗?", "提示", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    m_OnEditItemShop.TableList.Remove(m_listBoxItemTable.SelectedItem.ToString());
                    m_listBoxItemTable.Items.Remove(m_listBoxItemTable.SelectedItem);
                }
            }
            else if(menu.SourceControl == m_listBoxSkillTable)
            {
                if (m_OnEditSkillShop == null)
                {
                    MessageBox.Show("请先选择一个商店再删除 Table.");
                    return;
                }

                // 删除 Tab
                if (m_listBoxSkillTable.SelectedIndex == -1)
                {
                    return;
                }

                DialogResult result = MessageBox.Show("确定要删除 Tab:" + m_listBoxSkillTable.SelectedItem.ToString() + " 吗?", "提示", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    m_OnEditSkillShop.Tables.Remove(m_listBoxSkillTable.SelectedItem.ToString());
                    m_listBoxSkillTable.Items.Remove(m_listBoxSkillTable.SelectedItem);
                }
            }
        }

        private void m_listBoxItemTable_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // 修改 table name
            //if (m_listBoxItemTable.SelectedIndex == -1)
            //{
            //    return;
            //}

            //String strOldName = m_listBoxItemTable.SelectedItem.ToString();
            //Hashtable itemList = m_OnEditItemShop.TableList[strOldName];
        }

        // 显示当前技能列表
        private void ShowSelectedTableSkills()
        {
            if (m_OnEditSkillShop == null || m_listBoxSkillTable.SelectedIndex == -1)
            {
                return;
            }

            m_listBoxSelectedSkills.Items.Clear();
            m_listBoxSkillLevel.Items.Clear();
            m_listBoxSkillTradePrice.Items.Clear();
            m_listBoxSkillProfession.Items.Clear();

            // 显示该 Tab 对应的所有 shopSkill
            Hashtable skills = m_OnEditSkillShop.Tables[m_listBoxSkillTable.SelectedItem.ToString()] as Hashtable;
            ArrayList indexList = m_OnEditSkillShop.IndexTables[m_listBoxSkillTable.SelectedItem.ToString()] as ArrayList;
            if (skills != null)
            {
                if(indexList != null)
                {
                    foreach (int iSkillID in indexList)
                    {
                        ShopSkill shopSkill = skills[iSkillID] as ShopSkill;
                        Skill skill = SkillManager.Instance.Skills[iSkillID] as Skill;

                        if (shopSkill != null && skill != null)
                        {
                            m_listBoxSelectedSkills.Items.Add(MainForm.GetDisplayText(skill.ID, skill.Name, OptionManager.DisplayMode.DM_ID_NAME));
                            m_listBoxSkillLevel.Items.Add(shopSkill.Level.ToString());
                            m_listBoxSkillTradePrice.Items.Add(shopSkill.TradePrice.ToString());
                            m_listBoxSkillProfession.Items.Add(shopSkill.Profession.ToString());
                        }
                    }
                }
                else
                {
                    foreach (ShopSkill shopSkill in skills.Values)
                    {
                        Skill skill = SkillManager.Instance.Skills[shopSkill.SkillID] as Skill;

                        if (skill != null)
                        {
                            m_listBoxSelectedSkills.Items.Add(MainForm.GetDisplayText(skill.ID, skill.Name, OptionManager.DisplayMode.DM_ID_NAME));
                            m_listBoxSkillLevel.Items.Add(shopSkill.Level.ToString());
                            m_listBoxSkillTradePrice.Items.Add(shopSkill.TradePrice.ToString());
                            m_listBoxSkillProfession.Items.Add(shopSkill.Profession.ToString());
                        }
                    }
                }
            }
        }

        private void m_listBoxSkillTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowSelectedTableSkills();
        }

        private void m_buttonLoadFromExternal_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "配置文件 (*.xml)|*.xml||";
            dlg.InitialDirectory = Application.StartupPath;
            dlg.RestoreDirectory = true;
            DialogResult result = dlg.ShowDialog();
            if(result == DialogResult.OK)
            {
                ItemShopManager.Instance.LoadFromExternalXml(dlg.FileName);
                RefreshShopList();
            }
        }

        private void m_buttonLoadFromExternalSkill_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "配置文件 (*.xml)|*.xml||";
            dlg.InitialDirectory = Application.StartupPath;
            dlg.RestoreDirectory = true;
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                SkillShopManager.Instance.LoadFromExternalXml(dlg.FileName);
                RefreshShopList();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // 测试按钮，暂时放在这里
            //SDLuaConvertScript.Instance.Load(OptionManager.StateDiagramConvScriptPath + "/sd_convertor.lua");
            //Mission mission = MainForm.Instance.MissionMgr.MissionList[0] as Mission;
            //SDLuaConvertScript.Instance.Convert(mission);
            //MainForm.Instance.RefreshStateDiagaramList();

        }
    }
}