﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    public partial class MissionAwardForm : Form
    {
        public MissionAwardForm(ListBox.ObjectCollection awards, ListBox.ObjectCollection values)
        {
            InitializeComponent();

            // 将参数中的 awards, values 复制到窗口中
            m_listBoxSelectedAwards.Items.Clear();
            m_listBoxValue.Items.Clear();

            foreach (String strAward in awards)
            {
                m_listBoxSelectedAwards.Items.Add(strAward);
            }

            foreach (String strValue in values)
            {
                m_listBoxValue.Items.Add(strValue);
            }

        }

        private void MissionAwardForm_Load(object sender, EventArgs e)
        {
            // 从 MissionAwardManager 中找出第一级 Award ,添加到 filter 中.
            foreach(String strAward in MissionAwardManager.Instance.AwardMaps.Keys)
            {
                m_comboBoxFilter.Items.Add(strAward);
            }
        }

        private void SetAwardAttribute(MissionAwardInstance mai)
        {
            if(mai == null)
            {
                m_textBoxAwardDesc.Text = "";
                m_textBoxAwardParam.Text = "";
            }
            else
            {
                m_textBoxAwardDesc.Text = mai.Desc;
                m_textBoxAwardParam.Text = mai.Param;
            }
        }

        private void m_comboBoxFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 选择 filter
            if (m_comboBoxFilter.SelectedItem != null)
            {
                String str1stLevel = m_comboBoxFilter.SelectedItem.ToString();
                Hashtable awardMap = MissionAwardManager.Instance.AwardMaps[str1stLevel] as Hashtable;

                if (awardMap != null)
                {
                    m_listBoxAllAwards.Items.Clear();
                    foreach (String strAward in awardMap.Keys)
                    {
                        m_listBoxAllAwards.Items.Add(strAward);
                    }
                }
            }

            // 设置奖励参数说明
            SetAwardAttribute(null);
            
        }

        private void m_btnAdd_Click(object sender, EventArgs e)
        {
            // 添加 Award
            if (m_listBoxAllAwards.SelectedItem == null)
            {
                return;
            }

            String strAward = m_listBoxAllAwards.SelectedItem.ToString();
            if (strAward == null)   // 奖励可以重复设置,比如道具
            {
                return;
            }

            try
            {
                m_listBoxSelectedAwards.Items.Add(strAward);
                m_listBoxValue.Items.Add(m_textBoxValue.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("请检查 对应值 的合法性.");
                return;
            }
        }

        private void m_btnRemove_Click(object sender, EventArgs e)
        {
            int iIdx = m_listBoxSelectedAwards.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxValue.Items.RemoveAt(iIdx);
                m_listBoxSelectedAwards.Items.RemoveAt(iIdx);
            }
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void m_listBoxSelectedAwards_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iIdx = m_listBoxSelectedAwards.SelectedIndex;
            m_listBoxValue.SelectedIndex = iIdx;

            // 设置奖励参数说明
            if(m_listBoxSelectedAwards.SelectedItem != null)
            {
                String strAwardName = m_listBoxSelectedAwards.SelectedItem.ToString();
                MissionAwardInstance mai = MissionAwardManager.Instance.GetAwardByDisplay(strAwardName);
                SetAwardAttribute(mai);
            }
        }

        public ListBox.ObjectCollection Awards
        {
            get
            {
                return m_listBoxSelectedAwards.Items;
            }
        }

        public ListBox.ObjectCollection Values
        {
            get
            {
                return m_listBoxValue.Items;
            }
        }

        private void m_listBoxAllAwards_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 设置奖励参数说明
            if (m_listBoxAllAwards.SelectedItem != null)
            {
                String strAwardName = m_listBoxAllAwards.SelectedItem.ToString();
                MissionAwardInstance mai = MissionAwardManager.Instance.GetAwardByDisplay(strAwardName);
                SetAwardAttribute(mai);
            }
        }

    }
}