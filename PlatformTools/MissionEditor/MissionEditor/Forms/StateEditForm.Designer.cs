﻿using System.Collections;
namespace MissionEditor
{
    partial class StateEditForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_textBoxStateName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_comboBoxTrigger = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_listBoxSelectedActions = new System.Windows.Forms.ListBox();
            this.m_listBoxAllActions = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_btnAddAction = new System.Windows.Forms.Button();
            this.m_richTextBoxNPCToPlayer = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_listBoxOptions = new System.Windows.Forms.ListBox();
            this.m_textBoxToAdd = new System.Windows.Forms.TextBox();
            this.m_btnAddOption = new System.Windows.Forms.Button();
            this.m_btnDeleteOption = new System.Windows.Forms.Button();
            this.m_btnRemoveAction = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.m_textBoxOptionValue = new System.Windows.Forms.TextBox();
            this.m_listBoxOptionValues = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.m_listBoxActionValue = new System.Windows.Forms.ListBox();
            this.m_textBoxActionValue = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_textBoxAtomParam = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.m_textBoxAtomDesc = new System.Windows.Forms.TextBox();
            this.m_btnUpOption = new System.Windows.Forms.Button();
            this.m_btnDownOption = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_textBoxStateName
            // 
            this.m_textBoxStateName.Location = new System.Drawing.Point(71, 12);
            this.m_textBoxStateName.Name = "m_textBoxStateName";
            this.m_textBoxStateName.Size = new System.Drawing.Size(195, 21);
            this.m_textBoxStateName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "状态名称";
            // 
            // m_comboBoxTrigger
            // 
            this.m_comboBoxTrigger.FormattingEnabled = true;
            this.m_comboBoxTrigger.Items.AddRange(new object[] {
            "OnEnter:",
            "OnClick:",
            "OnExit:"});
            this.m_comboBoxTrigger.Location = new System.Drawing.Point(71, 56);
            this.m_comboBoxTrigger.Name = "m_comboBoxTrigger";
            this.m_comboBoxTrigger.Size = new System.Drawing.Size(195, 20);
            this.m_comboBoxTrigger.TabIndex = 3;
            this.m_comboBoxTrigger.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxTrigger_SelectedIndexChanged);
            this.m_comboBoxTrigger.DropDown += new System.EventHandler(this.m_comboBoxTrigger_DropDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "行为描述:";
            // 
            // m_btnOK
            // 
            this.m_btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.m_btnOK.Location = new System.Drawing.Point(517, 436);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(98, 21);
            this.m_btnOK.TabIndex = 5;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_listBoxSelectedActions
            // 
            this.m_listBoxSelectedActions.FormattingEnabled = true;
            this.m_listBoxSelectedActions.ItemHeight = 12;
            this.m_listBoxSelectedActions.Location = new System.Drawing.Point(183, 105);
            this.m_listBoxSelectedActions.Name = "m_listBoxSelectedActions";
            this.m_listBoxSelectedActions.Size = new System.Drawing.Size(132, 160);
            this.m_listBoxSelectedActions.TabIndex = 7;
            this.m_listBoxSelectedActions.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSelectedActions_SelectedIndexChanged);
            // 
            // m_listBoxAllActions
            // 
            this.m_listBoxAllActions.FormattingEnabled = true;
            this.m_listBoxAllActions.ItemHeight = 12;
            this.m_listBoxAllActions.Location = new System.Drawing.Point(12, 105);
            this.m_listBoxAllActions.Name = "m_listBoxAllActions";
            this.m_listBoxAllActions.Size = new System.Drawing.Size(132, 160);
            this.m_listBoxAllActions.TabIndex = 8;
            this.m_listBoxAllActions.SelectedIndexChanged += new System.EventHandler(this.m_listBoxAllActions_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "行为触发";
            // 
            // m_btnAddAction
            // 
            this.m_btnAddAction.Location = new System.Drawing.Point(150, 156);
            this.m_btnAddAction.Name = "m_btnAddAction";
            this.m_btnAddAction.Size = new System.Drawing.Size(27, 20);
            this.m_btnAddAction.TabIndex = 10;
            this.m_btnAddAction.Text = "->";
            this.m_btnAddAction.UseVisualStyleBackColor = true;
            this.m_btnAddAction.Click += new System.EventHandler(this.m_btnAddAction_Click);
            // 
            // m_richTextBoxNPCToPlayer
            // 
            this.m_richTextBoxNPCToPlayer.Location = new System.Drawing.Point(517, 30);
            this.m_richTextBoxNPCToPlayer.Name = "m_richTextBoxNPCToPlayer";
            this.m_richTextBoxNPCToPlayer.Size = new System.Drawing.Size(265, 92);
            this.m_richTextBoxNPCToPlayer.TabIndex = 11;
            this.m_richTextBoxNPCToPlayer.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(515, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "对话编辑";
            // 
            // m_listBoxOptions
            // 
            this.m_listBoxOptions.FormattingEnabled = true;
            this.m_listBoxOptions.ItemHeight = 12;
            this.m_listBoxOptions.Location = new System.Drawing.Point(517, 149);
            this.m_listBoxOptions.Name = "m_listBoxOptions";
            this.m_listBoxOptions.Size = new System.Drawing.Size(219, 148);
            this.m_listBoxOptions.TabIndex = 13;
            this.m_listBoxOptions.DoubleClick += new System.EventHandler(this.m_listBoxOptions_DoubleClick);
            this.m_listBoxOptions.SelectedIndexChanged += new System.EventHandler(this.m_listBoxOptions_SelectedIndexChanged);
            // 
            // m_textBoxToAdd
            // 
            this.m_textBoxToAdd.Location = new System.Drawing.Point(517, 305);
            this.m_textBoxToAdd.Multiline = true;
            this.m_textBoxToAdd.Name = "m_textBoxToAdd";
            this.m_textBoxToAdd.Size = new System.Drawing.Size(219, 40);
            this.m_textBoxToAdd.TabIndex = 14;
            // 
            // m_btnAddOption
            // 
            this.m_btnAddOption.Location = new System.Drawing.Point(517, 374);
            this.m_btnAddOption.Name = "m_btnAddOption";
            this.m_btnAddOption.Size = new System.Drawing.Size(51, 21);
            this.m_btnAddOption.TabIndex = 15;
            this.m_btnAddOption.Text = "添加";
            this.m_btnAddOption.UseVisualStyleBackColor = true;
            this.m_btnAddOption.Click += new System.EventHandler(this.m_btnAddOption_Click);
            // 
            // m_btnDeleteOption
            // 
            this.m_btnDeleteOption.Location = new System.Drawing.Point(574, 374);
            this.m_btnDeleteOption.Name = "m_btnDeleteOption";
            this.m_btnDeleteOption.Size = new System.Drawing.Size(51, 21);
            this.m_btnDeleteOption.TabIndex = 16;
            this.m_btnDeleteOption.Text = "删除";
            this.m_btnDeleteOption.UseVisualStyleBackColor = true;
            this.m_btnDeleteOption.Click += new System.EventHandler(this.m_btnDeleteOption_Click);
            // 
            // m_btnRemoveAction
            // 
            this.m_btnRemoveAction.Location = new System.Drawing.Point(150, 182);
            this.m_btnRemoveAction.Name = "m_btnRemoveAction";
            this.m_btnRemoveAction.Size = new System.Drawing.Size(27, 20);
            this.m_btnRemoveAction.TabIndex = 17;
            this.m_btnRemoveAction.Text = "<-";
            this.m_btnRemoveAction.UseVisualStyleBackColor = true;
            this.m_btnRemoveAction.Click += new System.EventHandler(this.m_btnRemoveAction_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(516, 353);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 12);
            this.label5.TabIndex = 19;
            this.label5.Text = "对应 curOption 值：";
            // 
            // m_textBoxOptionValue
            // 
            this.m_textBoxOptionValue.Location = new System.Drawing.Point(637, 350);
            this.m_textBoxOptionValue.Name = "m_textBoxOptionValue";
            this.m_textBoxOptionValue.Size = new System.Drawing.Size(34, 21);
            this.m_textBoxOptionValue.TabIndex = 20;
            this.m_textBoxOptionValue.Text = "1";
            // 
            // m_listBoxOptionValues
            // 
            this.m_listBoxOptionValues.FormattingEnabled = true;
            this.m_listBoxOptionValues.ItemHeight = 12;
            this.m_listBoxOptionValues.Location = new System.Drawing.Point(742, 149);
            this.m_listBoxOptionValues.Name = "m_listBoxOptionValues";
            this.m_listBoxOptionValues.Size = new System.Drawing.Size(35, 148);
            this.m_listBoxOptionValues.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(519, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 23;
            this.label6.Text = "选项：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(740, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 24;
            this.label7.Text = "curOption:";
            // 
            // m_listBoxActionValue
            // 
            this.m_listBoxActionValue.FormattingEnabled = true;
            this.m_listBoxActionValue.ItemHeight = 12;
            this.m_listBoxActionValue.Location = new System.Drawing.Point(321, 105);
            this.m_listBoxActionValue.Name = "m_listBoxActionValue";
            this.m_listBoxActionValue.Size = new System.Drawing.Size(142, 160);
            this.m_listBoxActionValue.TabIndex = 25;
            this.m_listBoxActionValue.DoubleClick += new System.EventHandler(this.m_listBoxActionValue_DoubleClick);
            // 
            // m_textBoxActionValue
            // 
            this.m_textBoxActionValue.Location = new System.Drawing.Point(14, 299);
            this.m_textBoxActionValue.Name = "m_textBoxActionValue";
            this.m_textBoxActionValue.Size = new System.Drawing.Size(104, 21);
            this.m_textBoxActionValue.TabIndex = 26;
            this.m_textBoxActionValue.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 285);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 12);
            this.label8.TabIndex = 27;
            this.label8.Text = "行为对应值：";
            // 
            // m_textBoxAtomParam
            // 
            this.m_textBoxAtomParam.Location = new System.Drawing.Point(77, 352);
            this.m_textBoxAtomParam.Multiline = true;
            this.m_textBoxAtomParam.Name = "m_textBoxAtomParam";
            this.m_textBoxAtomParam.ReadOnly = true;
            this.m_textBoxAtomParam.Size = new System.Drawing.Size(370, 105);
            this.m_textBoxAtomParam.TabIndex = 31;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 356);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 30;
            this.label9.Text = "参数说明:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 329);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 12);
            this.label10.TabIndex = 29;
            this.label10.Text = "功能描述:";
            // 
            // m_textBoxAtomDesc
            // 
            this.m_textBoxAtomDesc.Location = new System.Drawing.Point(77, 325);
            this.m_textBoxAtomDesc.Name = "m_textBoxAtomDesc";
            this.m_textBoxAtomDesc.ReadOnly = true;
            this.m_textBoxAtomDesc.Size = new System.Drawing.Size(370, 21);
            this.m_textBoxAtomDesc.TabIndex = 28;
            // 
            // m_btnUpOption
            // 
            this.m_btnUpOption.Location = new System.Drawing.Point(778, 190);
            this.m_btnUpOption.Name = "m_btnUpOption";
            this.m_btnUpOption.Size = new System.Drawing.Size(22, 22);
            this.m_btnUpOption.TabIndex = 32;
            this.m_btnUpOption.Text = "∧";
            this.m_btnUpOption.UseVisualStyleBackColor = true;
            this.m_btnUpOption.Click += new System.EventHandler(this.m_btnUpOption_Click);
            // 
            // m_btnDownOption
            // 
            this.m_btnDownOption.Location = new System.Drawing.Point(778, 240);
            this.m_btnDownOption.Name = "m_btnDownOption";
            this.m_btnDownOption.Size = new System.Drawing.Size(22, 22);
            this.m_btnDownOption.TabIndex = 33;
            this.m_btnDownOption.Text = "∨";
            this.m_btnDownOption.UseVisualStyleBackColor = true;
            this.m_btnDownOption.Click += new System.EventHandler(this.m_btnDownOption_Click);
            // 
            // StateEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 476);
            this.Controls.Add(this.m_btnDownOption);
            this.Controls.Add(this.m_btnUpOption);
            this.Controls.Add(this.m_textBoxAtomParam);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.m_textBoxAtomDesc);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.m_textBoxActionValue);
            this.Controls.Add(this.m_listBoxActionValue);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.m_listBoxOptionValues);
            this.Controls.Add(this.m_textBoxOptionValue);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_btnRemoveAction);
            this.Controls.Add(this.m_btnDeleteOption);
            this.Controls.Add(this.m_btnAddOption);
            this.Controls.Add(this.m_textBoxToAdd);
            this.Controls.Add(this.m_listBoxOptions);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_richTextBoxNPCToPlayer);
            this.Controls.Add(this.m_btnAddAction);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_listBoxAllActions);
            this.Controls.Add(this.m_listBoxSelectedActions);
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_comboBoxTrigger);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_textBoxStateName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StateEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "状态编辑";
            this.Load += new System.EventHandler(this.StateEditForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_textBoxStateName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox m_comboBoxTrigger;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button m_btnOK;
        private StateControl m_Ctrl;
        private System.Windows.Forms.ListBox m_listBoxSelectedActions;
        private System.Windows.Forms.ListBox m_listBoxAllActions;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button m_btnAddAction;
        private System.Windows.Forms.RichTextBox m_richTextBoxNPCToPlayer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox m_listBoxOptions;
        private System.Windows.Forms.TextBox m_textBoxToAdd;
        private System.Windows.Forms.Button m_btnAddOption;
        private System.Windows.Forms.Button m_btnDeleteOption;
        private System.Windows.Forms.Button m_btnRemoveAction;
        private System.String m_strStateDiagramName; // 状态图名称，也是状态图对应 NPC 编号
        private bool m_bActionsChanged = false; // 行为信息是否发生改变
        //private ArrayList m_AtomActionInstanceList = new ArrayList();
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox m_textBoxOptionValue;
        private System.Windows.Forms.ListBox m_listBoxOptionValues;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox m_listBoxActionValue;
        private System.Windows.Forms.TextBox m_textBoxActionValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox m_textBoxAtomParam;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox m_textBoxAtomDesc;
        private System.Windows.Forms.Button m_btnUpOption;
        private System.Windows.Forms.Button m_btnDownOption;   // 临时保存当前 trigger 对应的 action list
    }
}