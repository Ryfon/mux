﻿namespace MissionEditor
{
    partial class TaskUIForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_groupBoxTaskList = new System.Windows.Forms.GroupBox();
            this.m_labelFilterResultNum = new System.Windows.Forms.Label();
            this.m_buttonFilter = new System.Windows.Forms.Button();
            this.m_textBoxFilterTaskName = new System.Windows.Forms.TextBox();
            this.m_comboBoxFilterTaskType = new System.Windows.Forms.ComboBox();
            this.m_comboBoxFilterMap = new System.Windows.Forms.ComboBox();
            this.m_listViewTaskList = new System.Windows.Forms.ListView();
            this.m_contextMenuStripTaskList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.获取最新版本ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.签出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.签入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导入新任务ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.撤销更改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_groupBoxTaskProp = new System.Windows.Forms.GroupBox();
            this.m_panelShowTaskConditionSel = new System.Windows.Forms.Panel();
            this.m_panelShowTaskTargetSel = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.m_comboBoxTempTaskTargetType = new System.Windows.Forms.ComboBox();
            this.m_textBoxTempTargetValue = new System.Windows.Forms.TextBox();
            this.m_textBoxTempTraceName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.m_textBoxTempEndTargetID = new System.Windows.Forms.TextBox();
            this.m_textBoxTempStartTargetID = new System.Windows.Forms.TextBox();
            this.m_textBoxTempEndTarget = new System.Windows.Forms.TextBox();
            this.m_textBoxTempStartTarget = new System.Windows.Forms.TextBox();
            this.m_labelEndTarget = new System.Windows.Forms.Label();
            this.m_labelStartTarget = new System.Windows.Forms.Label();
            this.m_buttonResetTaskTargetSel = new System.Windows.Forms.Button();
            this.m_buttonAppTaskTargetSel = new System.Windows.Forms.Button();
            this.m_buttonCloseTaskTargetSel = new System.Windows.Forms.Button();
            this.m_panelShowObjSel = new System.Windows.Forms.Panel();
            this.m_textBoxTempProviderType = new System.Windows.Forms.TextBox();
            this.m_textBoxTempTaskSubmitID = new System.Windows.Forms.TextBox();
            this.m_textBoxTempTaskProviderID = new System.Windows.Forms.TextBox();
            this.m_textBoxTempTaskSubmit = new System.Windows.Forms.TextBox();
            this.m_textBoxTempTaskProvider = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.m_buttonResetObjSel = new System.Windows.Forms.Button();
            this.m_buttonAppObjSel = new System.Windows.Forms.Button();
            this.m_buttonCloseObjSel = new System.Windows.Forms.Button();
            this.m_panelShowTypeTree = new System.Windows.Forms.Panel();
            this.m_buttonOutLoad = new System.Windows.Forms.Button();
            this.m_buttonNPCDiagram = new System.Windows.Forms.Button();
            this.m_buttonSaveTask = new System.Windows.Forms.Button();
            this.m_buttonNewTask = new System.Windows.Forms.Button();
            this.m_groupBoxTaskDesc = new System.Windows.Forms.GroupBox();
            this.m_groupBoxHintTaskDesc = new System.Windows.Forms.GroupBox();
            this.m_panelHintDesc = new System.Windows.Forms.Panel();
            this.m_groupBoxSubmitTaskDesc = new System.Windows.Forms.GroupBox();
            this.m_panelSubmitDesc = new System.Windows.Forms.Panel();
            this.m_groupBoxGetTaskDesc = new System.Windows.Forms.GroupBox();
            this.m_panelGetDesc = new System.Windows.Forms.Panel();
            this.m_groupBoxSimpleTaskDesc = new System.Windows.Forms.GroupBox();
            this.m_panelSimpleDesc = new System.Windows.Forms.Panel();
            this.m_groupBoxTaskAward = new System.Windows.Forms.GroupBox();
            this.m_buttonSetTaskAward = new System.Windows.Forms.Button();
            this.m_listViewTaskAward = new System.Windows.Forms.ListView();
            this.m_groupBoxTaskTarget = new System.Windows.Forms.GroupBox();
            this.m_textBoxEndTargetID = new System.Windows.Forms.TextBox();
            this.m_textBoxStartTargetID = new System.Windows.Forms.TextBox();
            this.m_buttonSetTaskTarget = new System.Windows.Forms.Button();
            this.m_listViewTaskTarget = new System.Windows.Forms.ListView();
            this.m_groupBoxCondition = new System.Windows.Forms.GroupBox();
            this.m_buttonSetTaskCondition = new System.Windows.Forms.Button();
            this.m_listViewTaskCondition = new System.Windows.Forms.ListView();
            this.m_groupBoxBasicProp = new System.Windows.Forms.GroupBox();
            this.m_textBoxProviderType = new System.Windows.Forms.TextBox();
            this.m_textBoxTaskSubmitID = new System.Windows.Forms.TextBox();
            this.m_textBoxTaskProviderID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.m_textBoxTimeLimit = new System.Windows.Forms.TextBox();
            this.m_checkBoxTimeLimit = new System.Windows.Forms.CheckBox();
            this.m_checkBoxAbandonTask = new System.Windows.Forms.CheckBox();
            this.m_checkBoxShareTask = new System.Windows.Forms.CheckBox();
            this.m_checkBoxCircleTask = new System.Windows.Forms.CheckBox();
            this.m_buttonTaskSubmit = new System.Windows.Forms.Button();
            this.m_buttonTaskType = new System.Windows.Forms.Button();
            this.m_textBoxTaskSubmit = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.m_textBoxTaskProvider = new System.Windows.Forms.TextBox();
            this.m_textBoxTaskType = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.m_textBoxTaskName = new System.Windows.Forms.TextBox();
            this.m_comboBoxTaskStarLevel = new System.Windows.Forms.ComboBox();
            this.m_comboBoxTaskClass = new System.Windows.Forms.ComboBox();
            this.m_textBoxTaskLevel = new System.Windows.Forms.TextBox();
            this.m_comboBoxMapName = new System.Windows.Forms.ComboBox();
            this.m_textBoxTaskID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_groupBoxSaveMethod = new System.Windows.Forms.GroupBox();
            this.m_textBoxSaveTimes = new System.Windows.Forms.TextBox();
            this.m_textBoxSaveDays = new System.Windows.Forms.TextBox();
            this.m_radioButtonTimesSave = new System.Windows.Forms.RadioButton();
            this.m_radioButtonDailySave = new System.Windows.Forms.RadioButton();
            this.m_radioButtonPermanentSave = new System.Windows.Forms.RadioButton();
            this.m_radioButtonNotSave = new System.Windows.Forms.RadioButton();
            this.m_toolTipTaskDetail = new System.Windows.Forms.ToolTip(this.components);
            this.m_groupBoxTaskList.SuspendLayout();
            this.m_contextMenuStripTaskList.SuspendLayout();
            this.m_groupBoxTaskProp.SuspendLayout();
            this.m_panelShowTaskTargetSel.SuspendLayout();
            this.m_panelShowObjSel.SuspendLayout();
            this.m_groupBoxTaskDesc.SuspendLayout();
            this.m_groupBoxHintTaskDesc.SuspendLayout();
            this.m_groupBoxSubmitTaskDesc.SuspendLayout();
            this.m_groupBoxGetTaskDesc.SuspendLayout();
            this.m_groupBoxSimpleTaskDesc.SuspendLayout();
            this.m_groupBoxTaskAward.SuspendLayout();
            this.m_groupBoxTaskTarget.SuspendLayout();
            this.m_groupBoxCondition.SuspendLayout();
            this.m_groupBoxBasicProp.SuspendLayout();
            this.m_groupBoxSaveMethod.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_groupBoxTaskList
            // 
            this.m_groupBoxTaskList.Controls.Add(this.m_labelFilterResultNum);
            this.m_groupBoxTaskList.Controls.Add(this.m_buttonFilter);
            this.m_groupBoxTaskList.Controls.Add(this.m_textBoxFilterTaskName);
            this.m_groupBoxTaskList.Controls.Add(this.m_comboBoxFilterTaskType);
            this.m_groupBoxTaskList.Controls.Add(this.m_comboBoxFilterMap);
            this.m_groupBoxTaskList.Controls.Add(this.m_listViewTaskList);
            this.m_groupBoxTaskList.Location = new System.Drawing.Point(10, 0);
            this.m_groupBoxTaskList.Name = "m_groupBoxTaskList";
            this.m_groupBoxTaskList.Size = new System.Drawing.Size(300, 675);
            this.m_groupBoxTaskList.TabIndex = 0;
            this.m_groupBoxTaskList.TabStop = false;
            // 
            // m_labelFilterResultNum
            // 
            this.m_labelFilterResultNum.AutoSize = true;
            this.m_labelFilterResultNum.Location = new System.Drawing.Point(10, 650);
            this.m_labelFilterResultNum.Name = "m_labelFilterResultNum";
            this.m_labelFilterResultNum.Size = new System.Drawing.Size(95, 12);
            this.m_labelFilterResultNum.TabIndex = 5;
            this.m_labelFilterResultNum.Text = "共查找到0个任务";
            // 
            // m_buttonFilter
            // 
            this.m_buttonFilter.Location = new System.Drawing.Point(250, 18);
            this.m_buttonFilter.Name = "m_buttonFilter";
            this.m_buttonFilter.Size = new System.Drawing.Size(40, 23);
            this.m_buttonFilter.TabIndex = 4;
            this.m_buttonFilter.Text = "筛选";
            this.m_buttonFilter.UseVisualStyleBackColor = true;
            this.m_buttonFilter.Click += new System.EventHandler(this.m_buttonFilter_Click);
            // 
            // m_textBoxFilterTaskName
            // 
            this.m_textBoxFilterTaskName.Location = new System.Drawing.Point(150, 19);
            this.m_textBoxFilterTaskName.Name = "m_textBoxFilterTaskName";
            this.m_textBoxFilterTaskName.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxFilterTaskName.TabIndex = 3;
            this.m_textBoxFilterTaskName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.m_textBoxFilterTaskName_KeyPress);
            // 
            // m_comboBoxFilterTaskType
            // 
            this.m_comboBoxFilterTaskType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxFilterTaskType.FormattingEnabled = true;
            this.m_comboBoxFilterTaskType.Items.AddRange(new object[] {
            "(类别)"});
            this.m_comboBoxFilterTaskType.Location = new System.Drawing.Point(80, 20);
            this.m_comboBoxFilterTaskType.Name = "m_comboBoxFilterTaskType";
            this.m_comboBoxFilterTaskType.Size = new System.Drawing.Size(70, 20);
            this.m_comboBoxFilterTaskType.TabIndex = 2;
            // 
            // m_comboBoxFilterMap
            // 
            this.m_comboBoxFilterMap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxFilterMap.FormattingEnabled = true;
            this.m_comboBoxFilterMap.Items.AddRange(new object[] {
            "(地图)"});
            this.m_comboBoxFilterMap.Location = new System.Drawing.Point(10, 20);
            this.m_comboBoxFilterMap.Name = "m_comboBoxFilterMap";
            this.m_comboBoxFilterMap.Size = new System.Drawing.Size(70, 20);
            this.m_comboBoxFilterMap.TabIndex = 1;
            // 
            // m_listViewTaskList
            // 
            this.m_listViewTaskList.ContextMenuStrip = this.m_contextMenuStripTaskList;
            this.m_listViewTaskList.FullRowSelect = true;
            this.m_listViewTaskList.HideSelection = false;
            this.m_listViewTaskList.Location = new System.Drawing.Point(10, 45);
            this.m_listViewTaskList.Name = "m_listViewTaskList";
            this.m_listViewTaskList.Size = new System.Drawing.Size(280, 600);
            this.m_listViewTaskList.TabIndex = 0;
            this.m_listViewTaskList.UseCompatibleStateImageBehavior = false;
            this.m_listViewTaskList.View = System.Windows.Forms.View.Details;
            this.m_listViewTaskList.DoubleClick += new System.EventHandler(this.m_listViewTaskList_DoubleClick);
            this.m_listViewTaskList.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.m_listViewTaskList_ColumnClick);
            this.m_listViewTaskList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_listViewTaskList_MouseMove);
            this.m_listViewTaskList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.m_listViewTaskList_KeyPress);
            // 
            // m_contextMenuStripTaskList
            // 
            this.m_contextMenuStripTaskList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.获取最新版本ToolStripMenuItem,
            this.toolStripSeparator1,
            this.签出ToolStripMenuItem,
            this.签入ToolStripMenuItem,
            this.导入新任务ToolStripMenuItem,
            this.撤销更改ToolStripMenuItem,
            this.toolStripSeparator2,
            this.删除ToolStripMenuItem});
            this.m_contextMenuStripTaskList.Name = "m_contextMenuStripTaskList";
            this.m_contextMenuStripTaskList.Size = new System.Drawing.Size(143, 148);
            // 
            // 获取最新版本ToolStripMenuItem
            // 
            this.获取最新版本ToolStripMenuItem.Name = "获取最新版本ToolStripMenuItem";
            this.获取最新版本ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.获取最新版本ToolStripMenuItem.Text = "获取最新版本";
            this.获取最新版本ToolStripMenuItem.Click += new System.EventHandler(this.获取最新版本ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
            // 
            // 签出ToolStripMenuItem
            // 
            this.签出ToolStripMenuItem.Name = "签出ToolStripMenuItem";
            this.签出ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.签出ToolStripMenuItem.Text = "签出";
            this.签出ToolStripMenuItem.Click += new System.EventHandler(this.签出ToolStripMenuItem_Click);
            // 
            // 签入ToolStripMenuItem
            // 
            this.签入ToolStripMenuItem.Name = "签入ToolStripMenuItem";
            this.签入ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.签入ToolStripMenuItem.Text = "签入";
            this.签入ToolStripMenuItem.Click += new System.EventHandler(this.签入ToolStripMenuItem_Click);
            // 
            // 导入新任务ToolStripMenuItem
            // 
            this.导入新任务ToolStripMenuItem.Name = "导入新任务ToolStripMenuItem";
            this.导入新任务ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.导入新任务ToolStripMenuItem.Text = "导入新任务";
            this.导入新任务ToolStripMenuItem.Click += new System.EventHandler(this.导入新任务ToolStripMenuItem_Click);
            // 
            // 撤销更改ToolStripMenuItem
            // 
            this.撤销更改ToolStripMenuItem.Name = "撤销更改ToolStripMenuItem";
            this.撤销更改ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.撤销更改ToolStripMenuItem.Text = "撤销更改";
            this.撤销更改ToolStripMenuItem.Click += new System.EventHandler(this.撤销更改ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(139, 6);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // m_groupBoxTaskProp
            // 
            this.m_groupBoxTaskProp.Controls.Add(this.m_panelShowTaskConditionSel);
            this.m_groupBoxTaskProp.Controls.Add(this.m_panelShowTaskTargetSel);
            this.m_groupBoxTaskProp.Controls.Add(this.m_panelShowObjSel);
            this.m_groupBoxTaskProp.Controls.Add(this.m_panelShowTypeTree);
            this.m_groupBoxTaskProp.Controls.Add(this.m_buttonOutLoad);
            this.m_groupBoxTaskProp.Controls.Add(this.m_buttonNPCDiagram);
            this.m_groupBoxTaskProp.Controls.Add(this.m_buttonSaveTask);
            this.m_groupBoxTaskProp.Controls.Add(this.m_buttonNewTask);
            this.m_groupBoxTaskProp.Controls.Add(this.m_groupBoxTaskDesc);
            this.m_groupBoxTaskProp.Controls.Add(this.m_groupBoxTaskAward);
            this.m_groupBoxTaskProp.Controls.Add(this.m_groupBoxTaskTarget);
            this.m_groupBoxTaskProp.Controls.Add(this.m_groupBoxCondition);
            this.m_groupBoxTaskProp.Controls.Add(this.m_groupBoxBasicProp);
            this.m_groupBoxTaskProp.Location = new System.Drawing.Point(315, 0);
            this.m_groupBoxTaskProp.Name = "m_groupBoxTaskProp";
            this.m_groupBoxTaskProp.Size = new System.Drawing.Size(685, 675);
            this.m_groupBoxTaskProp.TabIndex = 1;
            this.m_groupBoxTaskProp.TabStop = false;
            // 
            // m_panelShowTaskConditionSel
            // 
            this.m_panelShowTaskConditionSel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_panelShowTaskConditionSel.Location = new System.Drawing.Point(-314, 626);
            this.m_panelShowTaskConditionSel.Name = "m_panelShowTaskConditionSel";
            this.m_panelShowTaskConditionSel.Size = new System.Drawing.Size(665, 400);
            this.m_panelShowTaskConditionSel.TabIndex = 29;
            this.m_panelShowTaskConditionSel.Visible = false;
            // 
            // m_panelShowTaskTargetSel
            // 
            this.m_panelShowTaskTargetSel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_panelShowTaskTargetSel.Controls.Add(this.label17);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_comboBoxTempTaskTargetType);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_textBoxTempTargetValue);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_textBoxTempTraceName);
            this.m_panelShowTaskTargetSel.Controls.Add(this.label15);
            this.m_panelShowTaskTargetSel.Controls.Add(this.label16);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_textBoxTempEndTargetID);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_textBoxTempStartTargetID);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_textBoxTempEndTarget);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_textBoxTempStartTarget);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_labelEndTarget);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_labelStartTarget);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_buttonResetTaskTargetSel);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_buttonAppTaskTargetSel);
            this.m_panelShowTaskTargetSel.Controls.Add(this.m_buttonCloseTaskTargetSel);
            this.m_panelShowTaskTargetSel.Location = new System.Drawing.Point(-45, 220);
            this.m_panelShowTaskTargetSel.Name = "m_panelShowTaskTargetSel";
            this.m_panelShowTaskTargetSel.Size = new System.Drawing.Size(340, 380);
            this.m_panelShowTaskTargetSel.TabIndex = 28;
            this.m_panelShowTaskTargetSel.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 325);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 12);
            this.label17.TabIndex = 20;
            this.label17.Text = "类型:";
            // 
            // m_comboBoxTempTaskTargetType
            // 
            this.m_comboBoxTempTaskTargetType.FormattingEnabled = true;
            this.m_comboBoxTempTaskTargetType.Items.AddRange(new object[] {
            "NPC",
            "道具",
            "区域",
            "怪物",
            "玩家"});
            this.m_comboBoxTempTaskTargetType.Location = new System.Drawing.Point(45, 321);
            this.m_comboBoxTempTaskTargetType.Name = "m_comboBoxTempTaskTargetType";
            this.m_comboBoxTempTaskTargetType.Size = new System.Drawing.Size(50, 20);
            this.m_comboBoxTempTaskTargetType.TabIndex = 19;
            // 
            // m_textBoxTempTargetValue
            // 
            this.m_textBoxTempTargetValue.AllowDrop = true;
            this.m_textBoxTempTargetValue.Location = new System.Drawing.Point(305, 321);
            this.m_textBoxTempTargetValue.Name = "m_textBoxTempTargetValue";
            this.m_textBoxTempTargetValue.Size = new System.Drawing.Size(25, 21);
            this.m_textBoxTempTargetValue.TabIndex = 18;
            this.m_textBoxTempTargetValue.Text = "1";
            // 
            // m_textBoxTempTraceName
            // 
            this.m_textBoxTempTraceName.AllowDrop = true;
            this.m_textBoxTempTraceName.Location = new System.Drawing.Point(160, 321);
            this.m_textBoxTempTraceName.Name = "m_textBoxTempTraceName";
            this.m_textBoxTempTraceName.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxTempTraceName.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(260, 325);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 12);
            this.label15.TabIndex = 16;
            this.label15.Text = "目标值:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(100, 325);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 12);
            this.label16.TabIndex = 15;
            this.label16.Text = "追踪名称:";
            // 
            // m_textBoxTempEndTargetID
            // 
            this.m_textBoxTempEndTargetID.Location = new System.Drawing.Point(224, 30);
            this.m_textBoxTempEndTargetID.Name = "m_textBoxTempEndTargetID";
            this.m_textBoxTempEndTargetID.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxTempEndTargetID.TabIndex = 12;
            this.m_textBoxTempEndTargetID.Visible = false;
            // 
            // m_textBoxTempStartTargetID
            // 
            this.m_textBoxTempStartTargetID.Location = new System.Drawing.Point(62, 27);
            this.m_textBoxTempStartTargetID.Name = "m_textBoxTempStartTargetID";
            this.m_textBoxTempStartTargetID.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxTempStartTargetID.TabIndex = 11;
            this.m_textBoxTempStartTargetID.Visible = false;
            // 
            // m_textBoxTempEndTarget
            // 
            this.m_textBoxTempEndTarget.AllowDrop = true;
            this.m_textBoxTempEndTarget.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_textBoxTempEndTarget.Location = new System.Drawing.Point(230, 5);
            this.m_textBoxTempEndTarget.Name = "m_textBoxTempEndTarget";
            this.m_textBoxTempEndTarget.ReadOnly = true;
            this.m_textBoxTempEndTarget.Size = new System.Drawing.Size(100, 14);
            this.m_textBoxTempEndTarget.TabIndex = 10;
            this.m_textBoxTempEndTarget.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.m_textBoxTempEndTarget.DragDrop += new System.Windows.Forms.DragEventHandler(this.m_textBoxTempEndTarget_DragDrop);
            this.m_textBoxTempEndTarget.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_textBoxTempEndTarget_MouseMove);
            this.m_textBoxTempEndTarget.DragEnter += new System.Windows.Forms.DragEventHandler(this.m_textBox_ListViewItem_DragEnter);
            // 
            // m_textBoxTempStartTarget
            // 
            this.m_textBoxTempStartTarget.AllowDrop = true;
            this.m_textBoxTempStartTarget.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_textBoxTempStartTarget.Location = new System.Drawing.Point(65, 5);
            this.m_textBoxTempStartTarget.Name = "m_textBoxTempStartTarget";
            this.m_textBoxTempStartTarget.ReadOnly = true;
            this.m_textBoxTempStartTarget.Size = new System.Drawing.Size(100, 14);
            this.m_textBoxTempStartTarget.TabIndex = 9;
            this.m_textBoxTempStartTarget.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.m_textBoxTempStartTarget.DragDrop += new System.Windows.Forms.DragEventHandler(this.m_textBoxTempStartTarget_DragDrop);
            this.m_textBoxTempStartTarget.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_textBoxTempStartTarget_MouseMove);
            this.m_textBoxTempStartTarget.DragEnter += new System.Windows.Forms.DragEventHandler(this.m_textBox_ListViewItem_DragEnter);
            // 
            // m_labelEndTarget
            // 
            this.m_labelEndTarget.AutoSize = true;
            this.m_labelEndTarget.Location = new System.Drawing.Point(170, 7);
            this.m_labelEndTarget.Name = "m_labelEndTarget";
            this.m_labelEndTarget.Size = new System.Drawing.Size(59, 12);
            this.m_labelEndTarget.TabIndex = 8;
            this.m_labelEndTarget.Text = "终止目标:";
            // 
            // m_labelStartTarget
            // 
            this.m_labelStartTarget.AutoSize = true;
            this.m_labelStartTarget.Location = new System.Drawing.Point(5, 7);
            this.m_labelStartTarget.Name = "m_labelStartTarget";
            this.m_labelStartTarget.Size = new System.Drawing.Size(59, 12);
            this.m_labelStartTarget.TabIndex = 7;
            this.m_labelStartTarget.Text = "起始目标:";
            // 
            // m_buttonResetTaskTargetSel
            // 
            this.m_buttonResetTaskTargetSel.Location = new System.Drawing.Point(25, 350);
            this.m_buttonResetTaskTargetSel.Name = "m_buttonResetTaskTargetSel";
            this.m_buttonResetTaskTargetSel.Size = new System.Drawing.Size(80, 23);
            this.m_buttonResetTaskTargetSel.TabIndex = 2;
            this.m_buttonResetTaskTargetSel.Text = "重新设置";
            this.m_buttonResetTaskTargetSel.UseVisualStyleBackColor = true;
            this.m_buttonResetTaskTargetSel.Click += new System.EventHandler(this.m_buttonResetTaskTargetSel_Click);
            // 
            // m_buttonAppTaskTargetSel
            // 
            this.m_buttonAppTaskTargetSel.Location = new System.Drawing.Point(130, 350);
            this.m_buttonAppTaskTargetSel.Name = "m_buttonAppTaskTargetSel";
            this.m_buttonAppTaskTargetSel.Size = new System.Drawing.Size(80, 23);
            this.m_buttonAppTaskTargetSel.TabIndex = 1;
            this.m_buttonAppTaskTargetSel.Text = "应用设置";
            this.m_buttonAppTaskTargetSel.UseVisualStyleBackColor = true;
            this.m_buttonAppTaskTargetSel.Click += new System.EventHandler(this.m_buttonAppTaskTargetSel_Click);
            // 
            // m_buttonCloseTaskTargetSel
            // 
            this.m_buttonCloseTaskTargetSel.Location = new System.Drawing.Point(235, 350);
            this.m_buttonCloseTaskTargetSel.Name = "m_buttonCloseTaskTargetSel";
            this.m_buttonCloseTaskTargetSel.Size = new System.Drawing.Size(80, 23);
            this.m_buttonCloseTaskTargetSel.TabIndex = 0;
            this.m_buttonCloseTaskTargetSel.Text = "关闭窗口";
            this.m_buttonCloseTaskTargetSel.UseVisualStyleBackColor = true;
            this.m_buttonCloseTaskTargetSel.Click += new System.EventHandler(this.m_buttonCloseTaskTargetSel_Click);
            // 
            // m_panelShowObjSel
            // 
            this.m_panelShowObjSel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_panelShowObjSel.Controls.Add(this.m_textBoxTempProviderType);
            this.m_panelShowObjSel.Controls.Add(this.m_textBoxTempTaskSubmitID);
            this.m_panelShowObjSel.Controls.Add(this.m_textBoxTempTaskProviderID);
            this.m_panelShowObjSel.Controls.Add(this.m_textBoxTempTaskSubmit);
            this.m_panelShowObjSel.Controls.Add(this.m_textBoxTempTaskProvider);
            this.m_panelShowObjSel.Controls.Add(this.label12);
            this.m_panelShowObjSel.Controls.Add(this.label11);
            this.m_panelShowObjSel.Controls.Add(this.m_buttonResetObjSel);
            this.m_panelShowObjSel.Controls.Add(this.m_buttonAppObjSel);
            this.m_panelShowObjSel.Controls.Add(this.m_buttonCloseObjSel);
            this.m_panelShowObjSel.Location = new System.Drawing.Point(-112, 170);
            this.m_panelShowObjSel.Name = "m_panelShowObjSel";
            this.m_panelShowObjSel.Size = new System.Drawing.Size(340, 350);
            this.m_panelShowObjSel.TabIndex = 27;
            this.m_panelShowObjSel.Visible = false;
            // 
            // m_textBoxTempProviderType
            // 
            this.m_textBoxTempProviderType.Location = new System.Drawing.Point(134, 69);
            this.m_textBoxTempProviderType.Name = "m_textBoxTempProviderType";
            this.m_textBoxTempProviderType.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxTempProviderType.TabIndex = 13;
            this.m_textBoxTempProviderType.Visible = false;
            // 
            // m_textBoxTempTaskSubmitID
            // 
            this.m_textBoxTempTaskSubmitID.Location = new System.Drawing.Point(224, 30);
            this.m_textBoxTempTaskSubmitID.Name = "m_textBoxTempTaskSubmitID";
            this.m_textBoxTempTaskSubmitID.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxTempTaskSubmitID.TabIndex = 12;
            this.m_textBoxTempTaskSubmitID.Visible = false;
            // 
            // m_textBoxTempTaskProviderID
            // 
            this.m_textBoxTempTaskProviderID.Location = new System.Drawing.Point(62, 27);
            this.m_textBoxTempTaskProviderID.Name = "m_textBoxTempTaskProviderID";
            this.m_textBoxTempTaskProviderID.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxTempTaskProviderID.TabIndex = 11;
            this.m_textBoxTempTaskProviderID.Visible = false;
            // 
            // m_textBoxTempTaskSubmit
            // 
            this.m_textBoxTempTaskSubmit.AllowDrop = true;
            this.m_textBoxTempTaskSubmit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_textBoxTempTaskSubmit.Location = new System.Drawing.Point(230, 5);
            this.m_textBoxTempTaskSubmit.Name = "m_textBoxTempTaskSubmit";
            this.m_textBoxTempTaskSubmit.ReadOnly = true;
            this.m_textBoxTempTaskSubmit.Size = new System.Drawing.Size(100, 14);
            this.m_textBoxTempTaskSubmit.TabIndex = 10;
            this.m_textBoxTempTaskSubmit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.m_textBoxTempTaskSubmit.DragDrop += new System.Windows.Forms.DragEventHandler(this.m_textBoxTempTaskSubmit_DragDrop);
            this.m_textBoxTempTaskSubmit.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_textBoxTempTaskSubmit_MouseMove);
            this.m_textBoxTempTaskSubmit.DragEnter += new System.Windows.Forms.DragEventHandler(this.m_textBox_ListViewItem_DragEnter);
            // 
            // m_textBoxTempTaskProvider
            // 
            this.m_textBoxTempTaskProvider.AllowDrop = true;
            this.m_textBoxTempTaskProvider.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_textBoxTempTaskProvider.Location = new System.Drawing.Point(65, 5);
            this.m_textBoxTempTaskProvider.Name = "m_textBoxTempTaskProvider";
            this.m_textBoxTempTaskProvider.ReadOnly = true;
            this.m_textBoxTempTaskProvider.Size = new System.Drawing.Size(100, 14);
            this.m_textBoxTempTaskProvider.TabIndex = 9;
            this.m_textBoxTempTaskProvider.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.m_textBoxTempTaskProvider.DragDrop += new System.Windows.Forms.DragEventHandler(this.m_textBoxTempTaskProvider_DragDrop);
            this.m_textBoxTempTaskProvider.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_textBoxTempTaskProvider_MouseMove);
            this.m_textBoxTempTaskProvider.DragEnter += new System.Windows.Forms.DragEventHandler(this.m_textBox_ListViewItem_DragEnter);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(170, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 12);
            this.label12.TabIndex = 8;
            this.label12.Text = "提交任务:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 12);
            this.label11.TabIndex = 7;
            this.label11.Text = "提供任务:";
            // 
            // m_buttonResetObjSel
            // 
            this.m_buttonResetObjSel.Location = new System.Drawing.Point(25, 320);
            this.m_buttonResetObjSel.Name = "m_buttonResetObjSel";
            this.m_buttonResetObjSel.Size = new System.Drawing.Size(80, 23);
            this.m_buttonResetObjSel.TabIndex = 2;
            this.m_buttonResetObjSel.Text = "重新设置";
            this.m_buttonResetObjSel.UseVisualStyleBackColor = true;
            this.m_buttonResetObjSel.Click += new System.EventHandler(this.m_buttonResetObjSel_Click);
            // 
            // m_buttonAppObjSel
            // 
            this.m_buttonAppObjSel.Location = new System.Drawing.Point(130, 320);
            this.m_buttonAppObjSel.Name = "m_buttonAppObjSel";
            this.m_buttonAppObjSel.Size = new System.Drawing.Size(80, 23);
            this.m_buttonAppObjSel.TabIndex = 1;
            this.m_buttonAppObjSel.Text = "应用设置";
            this.m_buttonAppObjSel.UseVisualStyleBackColor = true;
            this.m_buttonAppObjSel.Click += new System.EventHandler(this.m_buttonAppObjSel_Click);
            // 
            // m_buttonCloseObjSel
            // 
            this.m_buttonCloseObjSel.Location = new System.Drawing.Point(235, 320);
            this.m_buttonCloseObjSel.Name = "m_buttonCloseObjSel";
            this.m_buttonCloseObjSel.Size = new System.Drawing.Size(80, 23);
            this.m_buttonCloseObjSel.TabIndex = 0;
            this.m_buttonCloseObjSel.Text = "关闭窗口";
            this.m_buttonCloseObjSel.UseVisualStyleBackColor = true;
            this.m_buttonCloseObjSel.Click += new System.EventHandler(this.m_buttonCloseObjSel_Click);
            // 
            // m_panelShowTypeTree
            // 
            this.m_panelShowTypeTree.Location = new System.Drawing.Point(130, 347);
            this.m_panelShowTypeTree.Name = "m_panelShowTypeTree";
            this.m_panelShowTypeTree.Size = new System.Drawing.Size(235, 240);
            this.m_panelShowTypeTree.TabIndex = 26;
            this.m_panelShowTypeTree.Visible = false;
            // 
            // m_buttonOutLoad
            // 
            this.m_buttonOutLoad.Location = new System.Drawing.Point(605, 15);
            this.m_buttonOutLoad.Name = "m_buttonOutLoad";
            this.m_buttonOutLoad.Size = new System.Drawing.Size(70, 23);
            this.m_buttonOutLoad.TabIndex = 7;
            this.m_buttonOutLoad.Text = "导入数据";
            this.m_buttonOutLoad.UseVisualStyleBackColor = true;
            this.m_buttonOutLoad.Click += new System.EventHandler(this.m_buttonOutLoad_Click);
            // 
            // m_buttonNPCDiagram
            // 
            this.m_buttonNPCDiagram.Location = new System.Drawing.Point(530, 15);
            this.m_buttonNPCDiagram.Name = "m_buttonNPCDiagram";
            this.m_buttonNPCDiagram.Size = new System.Drawing.Size(70, 23);
            this.m_buttonNPCDiagram.TabIndex = 6;
            this.m_buttonNPCDiagram.Text = "NPC状态图";
            this.m_buttonNPCDiagram.UseVisualStyleBackColor = true;
            this.m_buttonNPCDiagram.Click += new System.EventHandler(this.m_buttonNPCDiagram_Click);
            // 
            // m_buttonSaveTask
            // 
            this.m_buttonSaveTask.Location = new System.Drawing.Point(450, 15);
            this.m_buttonSaveTask.Name = "m_buttonSaveTask";
            this.m_buttonSaveTask.Size = new System.Drawing.Size(70, 23);
            this.m_buttonSaveTask.TabIndex = 5;
            this.m_buttonSaveTask.Text = "保存任务";
            this.m_buttonSaveTask.UseVisualStyleBackColor = true;
            this.m_buttonSaveTask.Click += new System.EventHandler(this.m_buttonSaveTask_Click);
            // 
            // m_buttonNewTask
            // 
            this.m_buttonNewTask.Location = new System.Drawing.Point(375, 15);
            this.m_buttonNewTask.Name = "m_buttonNewTask";
            this.m_buttonNewTask.Size = new System.Drawing.Size(70, 23);
            this.m_buttonNewTask.TabIndex = 4;
            this.m_buttonNewTask.Text = "新建任务";
            this.m_buttonNewTask.UseVisualStyleBackColor = true;
            this.m_buttonNewTask.Click += new System.EventHandler(this.m_buttonNewTask_Click);
            // 
            // m_groupBoxTaskDesc
            // 
            this.m_groupBoxTaskDesc.Controls.Add(this.m_groupBoxHintTaskDesc);
            this.m_groupBoxTaskDesc.Controls.Add(this.m_groupBoxSubmitTaskDesc);
            this.m_groupBoxTaskDesc.Controls.Add(this.m_groupBoxGetTaskDesc);
            this.m_groupBoxTaskDesc.Controls.Add(this.m_groupBoxSimpleTaskDesc);
            this.m_groupBoxTaskDesc.Location = new System.Drawing.Point(375, 45);
            this.m_groupBoxTaskDesc.Name = "m_groupBoxTaskDesc";
            this.m_groupBoxTaskDesc.Size = new System.Drawing.Size(300, 620);
            this.m_groupBoxTaskDesc.TabIndex = 2;
            this.m_groupBoxTaskDesc.TabStop = false;
            this.m_groupBoxTaskDesc.Text = "任务描述";
            // 
            // m_groupBoxHintTaskDesc
            // 
            this.m_groupBoxHintTaskDesc.Controls.Add(this.m_panelHintDesc);
            this.m_groupBoxHintTaskDesc.Location = new System.Drawing.Point(5, 355);
            this.m_groupBoxHintTaskDesc.Name = "m_groupBoxHintTaskDesc";
            this.m_groupBoxHintTaskDesc.Size = new System.Drawing.Size(290, 105);
            this.m_groupBoxHintTaskDesc.TabIndex = 2;
            this.m_groupBoxHintTaskDesc.TabStop = false;
            this.m_groupBoxHintTaskDesc.Text = "任务接取跟踪";
            // 
            // m_panelHintDesc
            // 
            this.m_panelHintDesc.Location = new System.Drawing.Point(5, 15);
            this.m_panelHintDesc.Name = "m_panelHintDesc";
            this.m_panelHintDesc.Size = new System.Drawing.Size(280, 85);
            this.m_panelHintDesc.TabIndex = 0;
            // 
            // m_groupBoxSubmitTaskDesc
            // 
            this.m_groupBoxSubmitTaskDesc.Controls.Add(this.m_panelSubmitDesc);
            this.m_groupBoxSubmitTaskDesc.Location = new System.Drawing.Point(5, 465);
            this.m_groupBoxSubmitTaskDesc.Name = "m_groupBoxSubmitTaskDesc";
            this.m_groupBoxSubmitTaskDesc.Size = new System.Drawing.Size(290, 150);
            this.m_groupBoxSubmitTaskDesc.TabIndex = 1;
            this.m_groupBoxSubmitTaskDesc.TabStop = false;
            this.m_groupBoxSubmitTaskDesc.Text = "任务完成描述";
            // 
            // m_panelSubmitDesc
            // 
            this.m_panelSubmitDesc.Location = new System.Drawing.Point(5, 15);
            this.m_panelSubmitDesc.Name = "m_panelSubmitDesc";
            this.m_panelSubmitDesc.Size = new System.Drawing.Size(280, 130);
            this.m_panelSubmitDesc.TabIndex = 1;
            // 
            // m_groupBoxGetTaskDesc
            // 
            this.m_groupBoxGetTaskDesc.Controls.Add(this.m_panelGetDesc);
            this.m_groupBoxGetTaskDesc.Location = new System.Drawing.Point(5, 125);
            this.m_groupBoxGetTaskDesc.Name = "m_groupBoxGetTaskDesc";
            this.m_groupBoxGetTaskDesc.Size = new System.Drawing.Size(290, 225);
            this.m_groupBoxGetTaskDesc.TabIndex = 1;
            this.m_groupBoxGetTaskDesc.TabStop = false;
            this.m_groupBoxGetTaskDesc.Text = "领取任务描述";
            // 
            // m_panelGetDesc
            // 
            this.m_panelGetDesc.Location = new System.Drawing.Point(5, 15);
            this.m_panelGetDesc.Name = "m_panelGetDesc";
            this.m_panelGetDesc.Size = new System.Drawing.Size(280, 205);
            this.m_panelGetDesc.TabIndex = 1;
            // 
            // m_groupBoxSimpleTaskDesc
            // 
            this.m_groupBoxSimpleTaskDesc.Controls.Add(this.m_panelSimpleDesc);
            this.m_groupBoxSimpleTaskDesc.Location = new System.Drawing.Point(5, 15);
            this.m_groupBoxSimpleTaskDesc.Name = "m_groupBoxSimpleTaskDesc";
            this.m_groupBoxSimpleTaskDesc.Size = new System.Drawing.Size(290, 105);
            this.m_groupBoxSimpleTaskDesc.TabIndex = 0;
            this.m_groupBoxSimpleTaskDesc.TabStop = false;
            this.m_groupBoxSimpleTaskDesc.Text = "任务简单描述";
            // 
            // m_panelSimpleDesc
            // 
            this.m_panelSimpleDesc.Location = new System.Drawing.Point(5, 15);
            this.m_panelSimpleDesc.Name = "m_panelSimpleDesc";
            this.m_panelSimpleDesc.Size = new System.Drawing.Size(280, 85);
            this.m_panelSimpleDesc.TabIndex = 0;
            // 
            // m_groupBoxTaskAward
            // 
            this.m_groupBoxTaskAward.Controls.Add(this.m_buttonSetTaskAward);
            this.m_groupBoxTaskAward.Controls.Add(this.m_listViewTaskAward);
            this.m_groupBoxTaskAward.Location = new System.Drawing.Point(10, 535);
            this.m_groupBoxTaskAward.Name = "m_groupBoxTaskAward";
            this.m_groupBoxTaskAward.Size = new System.Drawing.Size(360, 130);
            this.m_groupBoxTaskAward.TabIndex = 3;
            this.m_groupBoxTaskAward.TabStop = false;
            this.m_groupBoxTaskAward.Text = "任务奖励";
            // 
            // m_buttonSetTaskAward
            // 
            this.m_buttonSetTaskAward.Location = new System.Drawing.Point(260, 100);
            this.m_buttonSetTaskAward.Name = "m_buttonSetTaskAward";
            this.m_buttonSetTaskAward.Size = new System.Drawing.Size(90, 23);
            this.m_buttonSetTaskAward.TabIndex = 1;
            this.m_buttonSetTaskAward.Text = "设置任务奖励";
            this.m_buttonSetTaskAward.UseVisualStyleBackColor = true;
            this.m_buttonSetTaskAward.Click += new System.EventHandler(this.m_buttonSetTaskAward_Click);
            // 
            // m_listViewTaskAward
            // 
            this.m_listViewTaskAward.FullRowSelect = true;
            this.m_listViewTaskAward.GridLines = true;
            this.m_listViewTaskAward.Location = new System.Drawing.Point(10, 15);
            this.m_listViewTaskAward.Name = "m_listViewTaskAward";
            this.m_listViewTaskAward.Size = new System.Drawing.Size(340, 80);
            this.m_listViewTaskAward.TabIndex = 0;
            this.m_listViewTaskAward.UseCompatibleStateImageBehavior = false;
            this.m_listViewTaskAward.View = System.Windows.Forms.View.Details;
            // 
            // m_groupBoxTaskTarget
            // 
            this.m_groupBoxTaskTarget.Controls.Add(this.m_textBoxEndTargetID);
            this.m_groupBoxTaskTarget.Controls.Add(this.m_textBoxStartTargetID);
            this.m_groupBoxTaskTarget.Controls.Add(this.m_buttonSetTaskTarget);
            this.m_groupBoxTaskTarget.Controls.Add(this.m_listViewTaskTarget);
            this.m_groupBoxTaskTarget.Location = new System.Drawing.Point(10, 265);
            this.m_groupBoxTaskTarget.Name = "m_groupBoxTaskTarget";
            this.m_groupBoxTaskTarget.Size = new System.Drawing.Size(360, 130);
            this.m_groupBoxTaskTarget.TabIndex = 2;
            this.m_groupBoxTaskTarget.TabStop = false;
            this.m_groupBoxTaskTarget.Text = "任务目标";
            // 
            // m_textBoxEndTargetID
            // 
            this.m_textBoxEndTargetID.Location = new System.Drawing.Point(259, 49);
            this.m_textBoxEndTargetID.Name = "m_textBoxEndTargetID";
            this.m_textBoxEndTargetID.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxEndTargetID.TabIndex = 14;
            this.m_textBoxEndTargetID.Text = "-1";
            this.m_textBoxEndTargetID.Visible = false;
            // 
            // m_textBoxStartTargetID
            // 
            this.m_textBoxStartTargetID.Location = new System.Drawing.Point(259, 21);
            this.m_textBoxStartTargetID.Name = "m_textBoxStartTargetID";
            this.m_textBoxStartTargetID.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxStartTargetID.TabIndex = 13;
            this.m_textBoxStartTargetID.Text = "-1";
            this.m_textBoxStartTargetID.Visible = false;
            // 
            // m_buttonSetTaskTarget
            // 
            this.m_buttonSetTaskTarget.Location = new System.Drawing.Point(260, 100);
            this.m_buttonSetTaskTarget.Name = "m_buttonSetTaskTarget";
            this.m_buttonSetTaskTarget.Size = new System.Drawing.Size(90, 23);
            this.m_buttonSetTaskTarget.TabIndex = 1;
            this.m_buttonSetTaskTarget.Text = "设置任务目标";
            this.m_buttonSetTaskTarget.UseVisualStyleBackColor = true;
            this.m_buttonSetTaskTarget.Click += new System.EventHandler(this.m_buttonSetTaskTarget_Click);
            // 
            // m_listViewTaskTarget
            // 
            this.m_listViewTaskTarget.FullRowSelect = true;
            this.m_listViewTaskTarget.GridLines = true;
            this.m_listViewTaskTarget.Location = new System.Drawing.Point(10, 15);
            this.m_listViewTaskTarget.Name = "m_listViewTaskTarget";
            this.m_listViewTaskTarget.Size = new System.Drawing.Size(340, 80);
            this.m_listViewTaskTarget.TabIndex = 0;
            this.m_listViewTaskTarget.UseCompatibleStateImageBehavior = false;
            this.m_listViewTaskTarget.View = System.Windows.Forms.View.Details;
            // 
            // m_groupBoxCondition
            // 
            this.m_groupBoxCondition.Controls.Add(this.m_buttonSetTaskCondition);
            this.m_groupBoxCondition.Controls.Add(this.m_listViewTaskCondition);
            this.m_groupBoxCondition.Location = new System.Drawing.Point(10, 400);
            this.m_groupBoxCondition.Name = "m_groupBoxCondition";
            this.m_groupBoxCondition.Size = new System.Drawing.Size(360, 130);
            this.m_groupBoxCondition.TabIndex = 1;
            this.m_groupBoxCondition.TabStop = false;
            this.m_groupBoxCondition.Text = "领取条件";
            // 
            // m_buttonSetTaskCondition
            // 
            this.m_buttonSetTaskCondition.Location = new System.Drawing.Point(260, 100);
            this.m_buttonSetTaskCondition.Name = "m_buttonSetTaskCondition";
            this.m_buttonSetTaskCondition.Size = new System.Drawing.Size(90, 23);
            this.m_buttonSetTaskCondition.TabIndex = 1;
            this.m_buttonSetTaskCondition.Text = "设置任务条件";
            this.m_buttonSetTaskCondition.UseVisualStyleBackColor = true;
            this.m_buttonSetTaskCondition.Click += new System.EventHandler(this.m_buttonSetTaskCondition_Click);
            // 
            // m_listViewTaskCondition
            // 
            this.m_listViewTaskCondition.FullRowSelect = true;
            this.m_listViewTaskCondition.GridLines = true;
            this.m_listViewTaskCondition.Location = new System.Drawing.Point(10, 15);
            this.m_listViewTaskCondition.Name = "m_listViewTaskCondition";
            this.m_listViewTaskCondition.Size = new System.Drawing.Size(340, 80);
            this.m_listViewTaskCondition.TabIndex = 0;
            this.m_listViewTaskCondition.UseCompatibleStateImageBehavior = false;
            this.m_listViewTaskCondition.View = System.Windows.Forms.View.Details;
            // 
            // m_groupBoxBasicProp
            // 
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxProviderType);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTaskSubmitID);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTaskProviderID);
            this.m_groupBoxBasicProp.Controls.Add(this.label10);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTimeLimit);
            this.m_groupBoxBasicProp.Controls.Add(this.m_checkBoxTimeLimit);
            this.m_groupBoxBasicProp.Controls.Add(this.m_checkBoxAbandonTask);
            this.m_groupBoxBasicProp.Controls.Add(this.m_checkBoxShareTask);
            this.m_groupBoxBasicProp.Controls.Add(this.m_checkBoxCircleTask);
            this.m_groupBoxBasicProp.Controls.Add(this.m_buttonTaskSubmit);
            this.m_groupBoxBasicProp.Controls.Add(this.m_buttonTaskType);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTaskSubmit);
            this.m_groupBoxBasicProp.Controls.Add(this.label9);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTaskProvider);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTaskType);
            this.m_groupBoxBasicProp.Controls.Add(this.label8);
            this.m_groupBoxBasicProp.Controls.Add(this.label7);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTaskName);
            this.m_groupBoxBasicProp.Controls.Add(this.m_comboBoxTaskStarLevel);
            this.m_groupBoxBasicProp.Controls.Add(this.m_comboBoxTaskClass);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTaskLevel);
            this.m_groupBoxBasicProp.Controls.Add(this.m_comboBoxMapName);
            this.m_groupBoxBasicProp.Controls.Add(this.m_textBoxTaskID);
            this.m_groupBoxBasicProp.Controls.Add(this.label6);
            this.m_groupBoxBasicProp.Controls.Add(this.label5);
            this.m_groupBoxBasicProp.Controls.Add(this.label4);
            this.m_groupBoxBasicProp.Controls.Add(this.label3);
            this.m_groupBoxBasicProp.Controls.Add(this.label2);
            this.m_groupBoxBasicProp.Controls.Add(this.label1);
            this.m_groupBoxBasicProp.Controls.Add(this.m_groupBoxSaveMethod);
            this.m_groupBoxBasicProp.Location = new System.Drawing.Point(10, 10);
            this.m_groupBoxBasicProp.Name = "m_groupBoxBasicProp";
            this.m_groupBoxBasicProp.Size = new System.Drawing.Size(360, 250);
            this.m_groupBoxBasicProp.TabIndex = 0;
            this.m_groupBoxBasicProp.TabStop = false;
            this.m_groupBoxBasicProp.Text = "基本设置";
            // 
            // m_textBoxProviderType
            // 
            this.m_textBoxProviderType.Location = new System.Drawing.Point(265, 141);
            this.m_textBoxProviderType.Name = "m_textBoxProviderType";
            this.m_textBoxProviderType.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxProviderType.TabIndex = 29;
            this.m_textBoxProviderType.Visible = false;
            // 
            // m_textBoxTaskSubmitID
            // 
            this.m_textBoxTaskSubmitID.Location = new System.Drawing.Point(254, 175);
            this.m_textBoxTaskSubmitID.Name = "m_textBoxTaskSubmitID";
            this.m_textBoxTaskSubmitID.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxTaskSubmitID.TabIndex = 28;
            this.m_textBoxTaskSubmitID.Visible = false;
            // 
            // m_textBoxTaskProviderID
            // 
            this.m_textBoxTaskProviderID.Location = new System.Drawing.Point(90, 172);
            this.m_textBoxTaskProviderID.Name = "m_textBoxTaskProviderID";
            this.m_textBoxTaskProviderID.Size = new System.Drawing.Size(100, 21);
            this.m_textBoxTaskProviderID.TabIndex = 27;
            this.m_textBoxTaskProviderID.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(150, 225);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 12);
            this.label10.TabIndex = 26;
            this.label10.Text = "(格式:hh:mm:ss)";
            // 
            // m_textBoxTimeLimit
            // 
            this.m_textBoxTimeLimit.Enabled = false;
            this.m_textBoxTimeLimit.Location = new System.Drawing.Point(90, 222);
            this.m_textBoxTimeLimit.Name = "m_textBoxTimeLimit";
            this.m_textBoxTimeLimit.Size = new System.Drawing.Size(55, 21);
            this.m_textBoxTimeLimit.TabIndex = 25;
            this.m_textBoxTimeLimit.Text = "00:00:00";
            // 
            // m_checkBoxTimeLimit
            // 
            this.m_checkBoxTimeLimit.AutoSize = true;
            this.m_checkBoxTimeLimit.Location = new System.Drawing.Point(15, 225);
            this.m_checkBoxTimeLimit.Name = "m_checkBoxTimeLimit";
            this.m_checkBoxTimeLimit.Size = new System.Drawing.Size(78, 16);
            this.m_checkBoxTimeLimit.TabIndex = 24;
            this.m_checkBoxTimeLimit.Text = "时间限制:";
            this.m_checkBoxTimeLimit.UseVisualStyleBackColor = true;
            this.m_checkBoxTimeLimit.CheckedChanged += new System.EventHandler(this.m_checkBoxTimeLimit_CheckedChanged);
            // 
            // m_checkBoxAbandonTask
            // 
            this.m_checkBoxAbandonTask.AutoSize = true;
            this.m_checkBoxAbandonTask.Location = new System.Drawing.Point(235, 200);
            this.m_checkBoxAbandonTask.Name = "m_checkBoxAbandonTask";
            this.m_checkBoxAbandonTask.Size = new System.Drawing.Size(84, 16);
            this.m_checkBoxAbandonTask.TabIndex = 23;
            this.m_checkBoxAbandonTask.Text = "可放弃任务";
            this.m_checkBoxAbandonTask.UseVisualStyleBackColor = true;
            // 
            // m_checkBoxShareTask
            // 
            this.m_checkBoxShareTask.AutoSize = true;
            this.m_checkBoxShareTask.Location = new System.Drawing.Point(130, 200);
            this.m_checkBoxShareTask.Name = "m_checkBoxShareTask";
            this.m_checkBoxShareTask.Size = new System.Drawing.Size(84, 16);
            this.m_checkBoxShareTask.TabIndex = 22;
            this.m_checkBoxShareTask.Text = "可共享任务";
            this.m_checkBoxShareTask.UseVisualStyleBackColor = true;
            // 
            // m_checkBoxCircleTask
            // 
            this.m_checkBoxCircleTask.AutoSize = true;
            this.m_checkBoxCircleTask.Location = new System.Drawing.Point(15, 200);
            this.m_checkBoxCircleTask.Name = "m_checkBoxCircleTask";
            this.m_checkBoxCircleTask.Size = new System.Drawing.Size(84, 16);
            this.m_checkBoxCircleTask.TabIndex = 21;
            this.m_checkBoxCircleTask.Text = "可循环任务";
            this.m_checkBoxCircleTask.UseVisualStyleBackColor = true;
            // 
            // m_buttonTaskSubmit
            // 
            this.m_buttonTaskSubmit.Location = new System.Drawing.Point(315, 169);
            this.m_buttonTaskSubmit.Name = "m_buttonTaskSubmit";
            this.m_buttonTaskSubmit.Size = new System.Drawing.Size(37, 23);
            this.m_buttonTaskSubmit.TabIndex = 18;
            this.m_buttonTaskSubmit.Text = "设置";
            this.m_buttonTaskSubmit.UseVisualStyleBackColor = true;
            this.m_buttonTaskSubmit.Click += new System.EventHandler(this.m_buttonTaskSubmit_Click);
            // 
            // m_buttonTaskType
            // 
            this.m_buttonTaskType.Location = new System.Drawing.Point(225, 144);
            this.m_buttonTaskType.Name = "m_buttonTaskType";
            this.m_buttonTaskType.Size = new System.Drawing.Size(37, 23);
            this.m_buttonTaskType.TabIndex = 17;
            this.m_buttonTaskType.Text = "设置";
            this.m_buttonTaskType.UseVisualStyleBackColor = true;
            this.m_buttonTaskType.Click += new System.EventHandler(this.m_buttonTaskType_Click);
            // 
            // m_textBoxTaskSubmit
            // 
            this.m_textBoxTaskSubmit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_textBoxTaskSubmit.Location = new System.Drawing.Point(225, 173);
            this.m_textBoxTaskSubmit.Name = "m_textBoxTaskSubmit";
            this.m_textBoxTaskSubmit.ReadOnly = true;
            this.m_textBoxTaskSubmit.Size = new System.Drawing.Size(90, 14);
            this.m_textBoxTaskSubmit.TabIndex = 16;
            this.m_textBoxTaskSubmit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.m_textBoxTaskSubmit.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_textBoxTaskSubmit_MouseMove);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(160, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 15;
            this.label9.Text = "任务提交:";
            // 
            // m_textBoxTaskProvider
            // 
            this.m_textBoxTaskProvider.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_textBoxTaskProvider.Location = new System.Drawing.Point(70, 173);
            this.m_textBoxTaskProvider.Name = "m_textBoxTaskProvider";
            this.m_textBoxTaskProvider.ReadOnly = true;
            this.m_textBoxTaskProvider.Size = new System.Drawing.Size(90, 14);
            this.m_textBoxTaskProvider.TabIndex = 14;
            this.m_textBoxTaskProvider.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.m_textBoxTaskProvider.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_textBoxTaskProvider_MouseMove);
            // 
            // m_textBoxTaskType
            // 
            this.m_textBoxTaskType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.m_textBoxTaskType.Location = new System.Drawing.Point(70, 148);
            this.m_textBoxTaskType.Name = "m_textBoxTaskType";
            this.m_textBoxTaskType.ReadOnly = true;
            this.m_textBoxTaskType.Size = new System.Drawing.Size(155, 14);
            this.m_textBoxTaskType.TabIndex = 13;
            this.m_textBoxTaskType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.m_textBoxTaskType.TextChanged += new System.EventHandler(this.m_textBoxTaskType_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 12);
            this.label8.TabIndex = 12;
            this.label8.Text = "任务领取:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "任务类型:";
            // 
            // m_textBoxTaskName
            // 
            this.m_textBoxTaskName.Location = new System.Drawing.Point(225, 15);
            this.m_textBoxTaskName.Name = "m_textBoxTaskName";
            this.m_textBoxTaskName.Size = new System.Drawing.Size(125, 21);
            this.m_textBoxTaskName.TabIndex = 10;
            // 
            // m_comboBoxTaskStarLevel
            // 
            this.m_comboBoxTaskStarLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxTaskStarLevel.FormattingEnabled = true;
            this.m_comboBoxTaskStarLevel.Items.AddRange(new object[] {
            "一星",
            "二星",
            "三星",
            "四星",
            "五星"});
            this.m_comboBoxTaskStarLevel.Location = new System.Drawing.Point(225, 75);
            this.m_comboBoxTaskStarLevel.Name = "m_comboBoxTaskStarLevel";
            this.m_comboBoxTaskStarLevel.Size = new System.Drawing.Size(80, 20);
            this.m_comboBoxTaskStarLevel.TabIndex = 9;
            // 
            // m_comboBoxTaskClass
            // 
            this.m_comboBoxTaskClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxTaskClass.FormattingEnabled = true;
            this.m_comboBoxTaskClass.Location = new System.Drawing.Point(225, 45);
            this.m_comboBoxTaskClass.Name = "m_comboBoxTaskClass";
            this.m_comboBoxTaskClass.Size = new System.Drawing.Size(80, 20);
            this.m_comboBoxTaskClass.TabIndex = 5;
            // 
            // m_textBoxTaskLevel
            // 
            this.m_textBoxTaskLevel.Location = new System.Drawing.Point(70, 75);
            this.m_textBoxTaskLevel.Name = "m_textBoxTaskLevel";
            this.m_textBoxTaskLevel.Size = new System.Drawing.Size(30, 21);
            this.m_textBoxTaskLevel.TabIndex = 6;
            // 
            // m_comboBoxMapName
            // 
            this.m_comboBoxMapName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_comboBoxMapName.FormattingEnabled = true;
            this.m_comboBoxMapName.Location = new System.Drawing.Point(70, 45);
            this.m_comboBoxMapName.Name = "m_comboBoxMapName";
            this.m_comboBoxMapName.Size = new System.Drawing.Size(80, 20);
            this.m_comboBoxMapName.TabIndex = 8;
            // 
            // m_textBoxTaskID
            // 
            this.m_textBoxTaskID.Location = new System.Drawing.Point(70, 15);
            this.m_textBoxTaskID.Name = "m_textBoxTaskID";
            this.m_textBoxTaskID.Size = new System.Drawing.Size(80, 21);
            this.m_textBoxTaskID.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "地图名称:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "任务等级:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "任务名称:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(160, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "任务类别:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "任务难度:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "任务 ID :";
            // 
            // m_groupBoxSaveMethod
            // 
            this.m_groupBoxSaveMethod.Controls.Add(this.m_textBoxSaveTimes);
            this.m_groupBoxSaveMethod.Controls.Add(this.m_textBoxSaveDays);
            this.m_groupBoxSaveMethod.Controls.Add(this.m_radioButtonTimesSave);
            this.m_groupBoxSaveMethod.Controls.Add(this.m_radioButtonDailySave);
            this.m_groupBoxSaveMethod.Controls.Add(this.m_radioButtonPermanentSave);
            this.m_groupBoxSaveMethod.Controls.Add(this.m_radioButtonNotSave);
            this.m_groupBoxSaveMethod.Location = new System.Drawing.Point(10, 100);
            this.m_groupBoxSaveMethod.Name = "m_groupBoxSaveMethod";
            this.m_groupBoxSaveMethod.Size = new System.Drawing.Size(290, 40);
            this.m_groupBoxSaveMethod.TabIndex = 0;
            this.m_groupBoxSaveMethod.TabStop = false;
            this.m_groupBoxSaveMethod.Text = "存盘方式";
            // 
            // m_textBoxSaveTimes
            // 
            this.m_textBoxSaveTimes.Enabled = false;
            this.m_textBoxSaveTimes.Location = new System.Drawing.Point(255, 13);
            this.m_textBoxSaveTimes.Name = "m_textBoxSaveTimes";
            this.m_textBoxSaveTimes.Size = new System.Drawing.Size(24, 21);
            this.m_textBoxSaveTimes.TabIndex = 5;
            this.m_textBoxSaveTimes.Text = "0";
            // 
            // m_textBoxSaveDays
            // 
            this.m_textBoxSaveDays.Enabled = false;
            this.m_textBoxSaveDays.Location = new System.Drawing.Point(175, 13);
            this.m_textBoxSaveDays.Name = "m_textBoxSaveDays";
            this.m_textBoxSaveDays.Size = new System.Drawing.Size(24, 21);
            this.m_textBoxSaveDays.TabIndex = 4;
            this.m_textBoxSaveDays.Text = "0";
            // 
            // m_radioButtonTimesSave
            // 
            this.m_radioButtonTimesSave.AutoSize = true;
            this.m_radioButtonTimesSave.Location = new System.Drawing.Point(210, 16);
            this.m_radioButtonTimesSave.Name = "m_radioButtonTimesSave";
            this.m_radioButtonTimesSave.Size = new System.Drawing.Size(47, 16);
            this.m_radioButtonTimesSave.TabIndex = 3;
            this.m_radioButtonTimesSave.TabStop = true;
            this.m_radioButtonTimesSave.Text = "次数";
            this.m_radioButtonTimesSave.UseVisualStyleBackColor = true;
            this.m_radioButtonTimesSave.CheckedChanged += new System.EventHandler(this.m_radioButtonTaskSave_CheckedChanged);
            // 
            // m_radioButtonDailySave
            // 
            this.m_radioButtonDailySave.AutoSize = true;
            this.m_radioButtonDailySave.Location = new System.Drawing.Point(130, 16);
            this.m_radioButtonDailySave.Name = "m_radioButtonDailySave";
            this.m_radioButtonDailySave.Size = new System.Drawing.Size(47, 16);
            this.m_radioButtonDailySave.TabIndex = 2;
            this.m_radioButtonDailySave.TabStop = true;
            this.m_radioButtonDailySave.Text = "天数";
            this.m_radioButtonDailySave.UseVisualStyleBackColor = true;
            this.m_radioButtonDailySave.CheckedChanged += new System.EventHandler(this.m_radioButtonTaskSave_CheckedChanged);
            // 
            // m_radioButtonPermanentSave
            // 
            this.m_radioButtonPermanentSave.AutoSize = true;
            this.m_radioButtonPermanentSave.Location = new System.Drawing.Point(70, 16);
            this.m_radioButtonPermanentSave.Name = "m_radioButtonPermanentSave";
            this.m_radioButtonPermanentSave.Size = new System.Drawing.Size(47, 16);
            this.m_radioButtonPermanentSave.TabIndex = 1;
            this.m_radioButtonPermanentSave.TabStop = true;
            this.m_radioButtonPermanentSave.Text = "永久";
            this.m_radioButtonPermanentSave.UseVisualStyleBackColor = true;
            // 
            // m_radioButtonNotSave
            // 
            this.m_radioButtonNotSave.AutoSize = true;
            this.m_radioButtonNotSave.Location = new System.Drawing.Point(10, 16);
            this.m_radioButtonNotSave.Name = "m_radioButtonNotSave";
            this.m_radioButtonNotSave.Size = new System.Drawing.Size(47, 16);
            this.m_radioButtonNotSave.TabIndex = 0;
            this.m_radioButtonNotSave.TabStop = true;
            this.m_radioButtonNotSave.Text = "不存";
            this.m_radioButtonNotSave.UseVisualStyleBackColor = true;
            // 
            // TaskUIForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_groupBoxTaskProp);
            this.Controls.Add(this.m_groupBoxTaskList);
            this.Name = "TaskUIForm";
            this.Size = new System.Drawing.Size(1010, 680);
            this.Load += new System.EventHandler(this.TaskUIForm_Load);
            this.m_groupBoxTaskList.ResumeLayout(false);
            this.m_groupBoxTaskList.PerformLayout();
            this.m_contextMenuStripTaskList.ResumeLayout(false);
            this.m_groupBoxTaskProp.ResumeLayout(false);
            this.m_panelShowTaskTargetSel.ResumeLayout(false);
            this.m_panelShowTaskTargetSel.PerformLayout();
            this.m_panelShowObjSel.ResumeLayout(false);
            this.m_panelShowObjSel.PerformLayout();
            this.m_groupBoxTaskDesc.ResumeLayout(false);
            this.m_groupBoxHintTaskDesc.ResumeLayout(false);
            this.m_groupBoxSubmitTaskDesc.ResumeLayout(false);
            this.m_groupBoxGetTaskDesc.ResumeLayout(false);
            this.m_groupBoxSimpleTaskDesc.ResumeLayout(false);
            this.m_groupBoxTaskAward.ResumeLayout(false);
            this.m_groupBoxTaskTarget.ResumeLayout(false);
            this.m_groupBoxTaskTarget.PerformLayout();
            this.m_groupBoxCondition.ResumeLayout(false);
            this.m_groupBoxBasicProp.ResumeLayout(false);
            this.m_groupBoxBasicProp.PerformLayout();
            this.m_groupBoxSaveMethod.ResumeLayout(false);
            this.m_groupBoxSaveMethod.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox m_groupBoxTaskList;
        private System.Windows.Forms.GroupBox m_groupBoxTaskProp;
        private System.Windows.Forms.ListView m_listViewTaskList;
        private System.Windows.Forms.ToolTip m_toolTipTaskDetail;
        private System.Windows.Forms.ComboBox m_comboBoxFilterMap;
        private System.Windows.Forms.ComboBox m_comboBoxFilterTaskType;
        private System.Windows.Forms.Button m_buttonFilter;
        private System.Windows.Forms.TextBox m_textBoxFilterTaskName;
        private System.Windows.Forms.GroupBox m_groupBoxBasicProp;
        private System.Windows.Forms.GroupBox m_groupBoxSaveMethod;
        private System.Windows.Forms.RadioButton m_radioButtonNotSave;
        private System.Windows.Forms.TextBox m_textBoxSaveDays;
        private System.Windows.Forms.RadioButton m_radioButtonTimesSave;
        private System.Windows.Forms.RadioButton m_radioButtonDailySave;
        private System.Windows.Forms.RadioButton m_radioButtonPermanentSave;
        private System.Windows.Forms.TextBox m_textBoxSaveTimes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_textBoxTaskID;
        private System.Windows.Forms.ComboBox m_comboBoxMapName;
        private System.Windows.Forms.ComboBox m_comboBoxTaskStarLevel;
        private System.Windows.Forms.ComboBox m_comboBoxTaskClass;
        private System.Windows.Forms.TextBox m_textBoxTaskLevel;
        private System.Windows.Forms.TextBox m_textBoxTaskName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button m_buttonTaskSubmit;
        private System.Windows.Forms.Button m_buttonTaskType;
        private System.Windows.Forms.TextBox m_textBoxTaskSubmit;
        private System.Windows.Forms.TextBox m_textBoxTaskProvider;
        private System.Windows.Forms.TextBox m_textBoxTaskType;
        private System.Windows.Forms.CheckBox m_checkBoxTimeLimit;
        private System.Windows.Forms.CheckBox m_checkBoxAbandonTask;
        private System.Windows.Forms.CheckBox m_checkBoxShareTask;
        private System.Windows.Forms.CheckBox m_checkBoxCircleTask;
        private System.Windows.Forms.GroupBox m_groupBoxCondition;
        private System.Windows.Forms.ListView m_listViewTaskCondition;
        private System.Windows.Forms.Button m_buttonSetTaskCondition;
        private System.Windows.Forms.GroupBox m_groupBoxTaskTarget;
        private System.Windows.Forms.Button m_buttonSetTaskTarget;
        private System.Windows.Forms.ListView m_listViewTaskTarget;
        private System.Windows.Forms.GroupBox m_groupBoxTaskAward;
        private System.Windows.Forms.Button m_buttonSetTaskAward;
        private System.Windows.Forms.ListView m_listViewTaskAward;
        private System.Windows.Forms.GroupBox m_groupBoxTaskDesc;
        private System.Windows.Forms.Button m_buttonOutLoad;
        private System.Windows.Forms.Button m_buttonNPCDiagram;
        private System.Windows.Forms.Button m_buttonSaveTask;
        private System.Windows.Forms.Button m_buttonNewTask;
        private System.Windows.Forms.GroupBox m_groupBoxSubmitTaskDesc;
        private System.Windows.Forms.GroupBox m_groupBoxGetTaskDesc;
        private System.Windows.Forms.GroupBox m_groupBoxSimpleTaskDesc;
        private System.Windows.Forms.Panel m_panelShowTypeTree;
        //  自定义数据
        private TreeControl m_TaskTypeTree = null;
        private MissionManager m_MissionMgr = null;
        private DescEditControl m_SimpleDescEdit = null;
        private DescEditControl m_GetDescEdit = null;
        private DescEditControl m_SubmitDescEdit = null;
        private DescEditControl m_GetHintEdit = null;
        private ObjSelControl m_ProviderSubmitSel = null;
        private ObjSelControl m_TaskTargetSel = null;
        private ConditionSelControl m_TaskConditionSel = null;
        private Mission m_CurrMission = null;
        private bool m_bNewMission = false;
        private System.Data.DataSet m_TaskConditionDB = null;
        private System.Data.DataSet m_TaskAwardDB = null;
        //  自定义数据
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox m_textBoxTimeLimit;
        private System.Windows.Forms.Panel m_panelSimpleDesc;
        private System.Windows.Forms.Panel m_panelSubmitDesc;
        private System.Windows.Forms.Panel m_panelGetDesc;
        private System.Windows.Forms.Panel m_panelShowObjSel;
        private System.Windows.Forms.Button m_buttonResetObjSel;
        private System.Windows.Forms.Button m_buttonAppObjSel;
        private System.Windows.Forms.Button m_buttonCloseObjSel;
        private System.Windows.Forms.TextBox m_textBoxTempTaskSubmit;
        private System.Windows.Forms.TextBox m_textBoxTempTaskProvider;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox m_textBoxTempTaskSubmitID;
        private System.Windows.Forms.TextBox m_textBoxTempTaskProviderID;
        private System.Windows.Forms.TextBox m_textBoxTaskSubmitID;
        private System.Windows.Forms.TextBox m_textBoxTaskProviderID;
        private System.Windows.Forms.TextBox m_textBoxProviderType;
        private System.Windows.Forms.TextBox m_textBoxTempProviderType;
        private System.Windows.Forms.Panel m_panelShowTaskTargetSel;
        private System.Windows.Forms.TextBox m_textBoxTempEndTargetID;
        private System.Windows.Forms.TextBox m_textBoxTempStartTargetID;
        private System.Windows.Forms.TextBox m_textBoxTempEndTarget;
        private System.Windows.Forms.TextBox m_textBoxTempStartTarget;
        private System.Windows.Forms.Label m_labelEndTarget;
        private System.Windows.Forms.Label m_labelStartTarget;
        private System.Windows.Forms.Button m_buttonResetTaskTargetSel;
        private System.Windows.Forms.Button m_buttonAppTaskTargetSel;
        private System.Windows.Forms.Button m_buttonCloseTaskTargetSel;
        private System.Windows.Forms.TextBox m_textBoxTempTargetValue;
        private System.Windows.Forms.TextBox m_textBoxTempTraceName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox m_comboBoxTempTaskTargetType;
        private System.Windows.Forms.TextBox m_textBoxEndTargetID;
        private System.Windows.Forms.TextBox m_textBoxStartTargetID;
        private System.Windows.Forms.Panel m_panelShowTaskConditionSel;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStripTaskList;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.Label m_labelFilterResultNum;
        private System.Windows.Forms.ToolStripMenuItem 获取最新版本ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 签出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 签入ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导入新任务ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 撤销更改ToolStripMenuItem;
        private System.Windows.Forms.GroupBox m_groupBoxHintTaskDesc;
        private System.Windows.Forms.Panel m_panelHintDesc;
    }
}