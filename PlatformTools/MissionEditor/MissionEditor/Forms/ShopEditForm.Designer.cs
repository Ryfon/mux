﻿namespace MissionEditor
{
    partial class ShopEditForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_listBoxAllItems = new System.Windows.Forms.ListBox();
            this.m_contextMenuStripListBox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.全选ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_btnAddItem = new System.Windows.Forms.Button();
            this.m_btnRemoveItem = new System.Windows.Forms.Button();
            this.m_comboBoxItemFilter = new System.Windows.Forms.ComboBox();
            this.m_listBoxSelectedItems = new System.Windows.Forms.ListBox();
            this.m_listBoxItemShop = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_textBoxItemShopID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.m_textBoxItemShopDesc = new System.Windows.Forms.TextBox();
            this.m_btnAddItemShop = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.m_textBoxItemShopPriceRate = new System.Windows.Forms.TextBox();
            this.m_btnSaveItemShop = new System.Windows.Forms.Button();
            this.m_tabControlShops = new System.Windows.Forms.TabControl();
            this.m_tabPageItemShop = new System.Windows.Forms.TabPage();
            this.m_buttonLoadFromExternalItem = new System.Windows.Forms.Button();
            this.m_comboBoxCurrencyType = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.m_listBoxDailyProduceTime = new System.Windows.Forms.ListBox();
            this.label单次购买数量 = new System.Windows.Forms.Label();
            this.m_listBoxPerPurchaseNum = new System.Windows.Forms.ListBox();
            this.label22 = new System.Windows.Forms.Label();
            this.m_textBoxDailyProduceTime = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.m_textBoxPerPurchaseNum = new System.Windows.Forms.TextBox();
            this.m_textBoxProduceType = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.m_listBoxItemTable = new System.Windows.Forms.ListBox();
            this.m_contextMenuStripTable = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_toolStripMenuItemAddTab = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStripMenuItemDelTab = new System.Windows.Forms.ToolStripMenuItem();
            this.label11 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.m_listBoxItemTradePrice = new System.Windows.Forms.ListBox();
            this.m_textBoxItemTradePrice = new System.Windows.Forms.TextBox();
            this.m_textBoxItemTradeItemID = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.m_listBoxCapability = new System.Windows.Forms.ListBox();
            this.m_listBoxProduceNum = new System.Windows.Forms.ListBox();
            this.m_listBoxProduceType = new System.Windows.Forms.ListBox();
            this.m_textBoxCapability = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.m_textBoxProduceNum = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.m_tabPageSkillShop = new System.Windows.Forms.TabPage();
            this.m_buttonLoadFromExternalSkill = new System.Windows.Forms.Button();
            this.m_listBoxSkillProfession = new System.Windows.Forms.ListBox();
            this.label28 = new System.Windows.Forms.Label();
            this.m_textBoxSkillProfession = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.m_comboBoxSkillCurrencyType = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.m_textBoxSkillTradeItemID = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.m_listBoxSkillTable = new System.Windows.Forms.ListBox();
            this.m_textBoxSkillTradePrice = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.m_listBoxSkillTradePrice = new System.Windows.Forms.ListBox();
            this.label26 = new System.Windows.Forms.Label();
            this.m_textBoxSkillLevel = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.m_listBoxSkillLevel = new System.Windows.Forms.ListBox();
            this.m_listBoxAllSkills = new System.Windows.Forms.ListBox();
            this.m_btnAddSkill = new System.Windows.Forms.Button();
            this.m_btnSaveSkillShop = new System.Windows.Forms.Button();
            this.m_btnRemoveSkill = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.m_textBoxSkillShopPriceRate = new System.Windows.Forms.TextBox();
            this.m_listBoxSelectedSkills = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_listBoxSkillShop = new System.Windows.Forms.ListBox();
            this.m_btnAddSkillShop = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.m_textBoxSkillShopDesc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.m_textBoxSkillShopID = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.m_contextMenuStripListBox.SuspendLayout();
            this.m_tabControlShops.SuspendLayout();
            this.m_tabPageItemShop.SuspendLayout();
            this.m_contextMenuStripTable.SuspendLayout();
            this.m_tabPageSkillShop.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_listBoxAllItems
            // 
            this.m_listBoxAllItems.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxAllItems.FormattingEnabled = true;
            this.m_listBoxAllItems.ItemHeight = 12;
            this.m_listBoxAllItems.Location = new System.Drawing.Point(6, 30);
            this.m_listBoxAllItems.Name = "m_listBoxAllItems";
            this.m_listBoxAllItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxAllItems.Size = new System.Drawing.Size(180, 292);
            this.m_listBoxAllItems.TabIndex = 0;
            // 
            // m_contextMenuStripListBox
            // 
            this.m_contextMenuStripListBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.全选ToolStripMenuItem,
            this.清除ToolStripMenuItem});
            this.m_contextMenuStripListBox.Name = "m_contextMenuStripListBox";
            this.m_contextMenuStripListBox.Size = new System.Drawing.Size(99, 48);
            // 
            // 全选ToolStripMenuItem
            // 
            this.全选ToolStripMenuItem.Name = "全选ToolStripMenuItem";
            this.全选ToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.全选ToolStripMenuItem.Text = "全选";
            this.全选ToolStripMenuItem.Click += new System.EventHandler(this.全选ToolStripMenuItem_Click);
            // 
            // 清除ToolStripMenuItem
            // 
            this.清除ToolStripMenuItem.Name = "清除ToolStripMenuItem";
            this.清除ToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.清除ToolStripMenuItem.Text = "清除";
            this.清除ToolStripMenuItem.Click += new System.EventHandler(this.清除ToolStripMenuItem_Click);
            // 
            // m_btnAddItem
            // 
            this.m_btnAddItem.Location = new System.Drawing.Point(193, 126);
            this.m_btnAddItem.Name = "m_btnAddItem";
            this.m_btnAddItem.Size = new System.Drawing.Size(28, 22);
            this.m_btnAddItem.TabIndex = 1;
            this.m_btnAddItem.Text = "->";
            this.m_btnAddItem.UseVisualStyleBackColor = true;
            this.m_btnAddItem.Click += new System.EventHandler(this.m_btnAdd_Click);
            // 
            // m_btnRemoveItem
            // 
            this.m_btnRemoveItem.Location = new System.Drawing.Point(193, 154);
            this.m_btnRemoveItem.Name = "m_btnRemoveItem";
            this.m_btnRemoveItem.Size = new System.Drawing.Size(28, 22);
            this.m_btnRemoveItem.TabIndex = 2;
            this.m_btnRemoveItem.Text = "<-";
            this.m_btnRemoveItem.UseVisualStyleBackColor = true;
            this.m_btnRemoveItem.Click += new System.EventHandler(this.m_btnRemove_Click);
            // 
            // m_comboBoxItemFilter
            // 
            this.m_comboBoxItemFilter.FormattingEnabled = true;
            this.m_comboBoxItemFilter.Location = new System.Drawing.Point(6, 4);
            this.m_comboBoxItemFilter.Name = "m_comboBoxItemFilter";
            this.m_comboBoxItemFilter.Size = new System.Drawing.Size(180, 20);
            this.m_comboBoxItemFilter.TabIndex = 3;
            // 
            // m_listBoxSelectedItems
            // 
            this.m_listBoxSelectedItems.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxSelectedItems.FormattingEnabled = true;
            this.m_listBoxSelectedItems.ItemHeight = 12;
            this.m_listBoxSelectedItems.Location = new System.Drawing.Point(227, 78);
            this.m_listBoxSelectedItems.Name = "m_listBoxSelectedItems";
            this.m_listBoxSelectedItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxSelectedItems.Size = new System.Drawing.Size(180, 268);
            this.m_listBoxSelectedItems.TabIndex = 4;
            this.m_listBoxSelectedItems.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSelectedItems_SelectedIndexChanged);
            // 
            // m_listBoxItemShop
            // 
            this.m_listBoxItemShop.FormattingEnabled = true;
            this.m_listBoxItemShop.ItemHeight = 12;
            this.m_listBoxItemShop.Location = new System.Drawing.Point(848, 30);
            this.m_listBoxItemShop.Name = "m_listBoxItemShop";
            this.m_listBoxItemShop.Size = new System.Drawing.Size(119, 244);
            this.m_listBoxItemShop.Sorted = true;
            this.m_listBoxItemShop.TabIndex = 5;
            this.m_listBoxItemShop.SelectedIndexChanged += new System.EventHandler(this.m_listBoxShop_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(879, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "商店列表";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(844, 287);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "ID";
            // 
            // m_textBoxItemShopID
            // 
            this.m_textBoxItemShopID.Location = new System.Drawing.Point(913, 284);
            this.m_textBoxItemShopID.Name = "m_textBoxItemShopID";
            this.m_textBoxItemShopID.Size = new System.Drawing.Size(82, 21);
            this.m_textBoxItemShopID.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(844, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "描述";
            // 
            // m_textBoxItemShopDesc
            // 
            this.m_textBoxItemShopDesc.Location = new System.Drawing.Point(913, 308);
            this.m_textBoxItemShopDesc.Name = "m_textBoxItemShopDesc";
            this.m_textBoxItemShopDesc.Size = new System.Drawing.Size(82, 21);
            this.m_textBoxItemShopDesc.TabIndex = 10;
            // 
            // m_btnAddItemShop
            // 
            this.m_btnAddItemShop.Location = new System.Drawing.Point(881, 417);
            this.m_btnAddItemShop.Name = "m_btnAddItemShop";
            this.m_btnAddItemShop.Size = new System.Drawing.Size(87, 28);
            this.m_btnAddItemShop.TabIndex = 11;
            this.m_btnAddItemShop.Text = "增加/修改";
            this.m_btnAddItemShop.UseVisualStyleBackColor = true;
            this.m_btnAddItemShop.Click += new System.EventHandler(this.m_btnAddShop_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(844, 335);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "价格率";
            // 
            // m_textBoxItemShopPriceRate
            // 
            this.m_textBoxItemShopPriceRate.Location = new System.Drawing.Point(913, 332);
            this.m_textBoxItemShopPriceRate.Name = "m_textBoxItemShopPriceRate";
            this.m_textBoxItemShopPriceRate.Size = new System.Drawing.Size(82, 21);
            this.m_textBoxItemShopPriceRate.TabIndex = 13;
            this.m_textBoxItemShopPriceRate.Text = "1.0";
            // 
            // m_btnSaveItemShop
            // 
            this.m_btnSaveItemShop.Location = new System.Drawing.Point(227, 383);
            this.m_btnSaveItemShop.Name = "m_btnSaveItemShop";
            this.m_btnSaveItemShop.Size = new System.Drawing.Size(214, 76);
            this.m_btnSaveItemShop.TabIndex = 15;
            this.m_btnSaveItemShop.Text = "保存";
            this.m_btnSaveItemShop.UseVisualStyleBackColor = true;
            this.m_btnSaveItemShop.Click += new System.EventHandler(this.m_btnSave_Click);
            // 
            // m_tabControlShops
            // 
            this.m_tabControlShops.Controls.Add(this.m_tabPageItemShop);
            this.m_tabControlShops.Controls.Add(this.m_tabPageSkillShop);
            this.m_tabControlShops.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_tabControlShops.Location = new System.Drawing.Point(0, 0);
            this.m_tabControlShops.Name = "m_tabControlShops";
            this.m_tabControlShops.SelectedIndex = 0;
            this.m_tabControlShops.Size = new System.Drawing.Size(1025, 539);
            this.m_tabControlShops.TabIndex = 16;
            // 
            // m_tabPageItemShop
            // 
            this.m_tabPageItemShop.Controls.Add(this.button1);
            this.m_tabPageItemShop.Controls.Add(this.m_buttonLoadFromExternalItem);
            this.m_tabPageItemShop.Controls.Add(this.m_comboBoxCurrencyType);
            this.m_tabPageItemShop.Controls.Add(this.label31);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxDailyProduceTime);
            this.m_tabPageItemShop.Controls.Add(this.label单次购买数量);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxPerPurchaseNum);
            this.m_tabPageItemShop.Controls.Add(this.label22);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxDailyProduceTime);
            this.m_tabPageItemShop.Controls.Add(this.label21);
            this.m_tabPageItemShop.Controls.Add(this.label19);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxPerPurchaseNum);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxProduceType);
            this.m_tabPageItemShop.Controls.Add(this.label18);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxItemTable);
            this.m_tabPageItemShop.Controls.Add(this.label11);
            this.m_tabPageItemShop.Controls.Add(this.label23);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxItemTradePrice);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxItemTradePrice);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxItemTradeItemID);
            this.m_tabPageItemShop.Controls.Add(this.label20);
            this.m_tabPageItemShop.Controls.Add(this.label15);
            this.m_tabPageItemShop.Controls.Add(this.label16);
            this.m_tabPageItemShop.Controls.Add(this.label17);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxCapability);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxProduceNum);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxProduceType);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxCapability);
            this.m_tabPageItemShop.Controls.Add(this.label14);
            this.m_tabPageItemShop.Controls.Add(this.label13);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxProduceNum);
            this.m_tabPageItemShop.Controls.Add(this.label12);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxAllItems);
            this.m_tabPageItemShop.Controls.Add(this.m_btnAddItem);
            this.m_tabPageItemShop.Controls.Add(this.m_btnSaveItemShop);
            this.m_tabPageItemShop.Controls.Add(this.m_btnRemoveItem);
            this.m_tabPageItemShop.Controls.Add(this.m_comboBoxItemFilter);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxItemShopPriceRate);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxSelectedItems);
            this.m_tabPageItemShop.Controls.Add(this.label4);
            this.m_tabPageItemShop.Controls.Add(this.m_listBoxItemShop);
            this.m_tabPageItemShop.Controls.Add(this.m_btnAddItemShop);
            this.m_tabPageItemShop.Controls.Add(this.label1);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxItemShopDesc);
            this.m_tabPageItemShop.Controls.Add(this.label2);
            this.m_tabPageItemShop.Controls.Add(this.label3);
            this.m_tabPageItemShop.Controls.Add(this.m_textBoxItemShopID);
            this.m_tabPageItemShop.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageItemShop.Name = "m_tabPageItemShop";
            this.m_tabPageItemShop.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageItemShop.Size = new System.Drawing.Size(1017, 514);
            this.m_tabPageItemShop.TabIndex = 0;
            this.m_tabPageItemShop.Text = "道具商店";
            this.m_tabPageItemShop.UseVisualStyleBackColor = true;
            // 
            // m_buttonLoadFromExternalItem
            // 
            this.m_buttonLoadFromExternalItem.Location = new System.Drawing.Point(617, 383);
            this.m_buttonLoadFromExternalItem.Name = "m_buttonLoadFromExternalItem";
            this.m_buttonLoadFromExternalItem.Size = new System.Drawing.Size(214, 76);
            this.m_buttonLoadFromExternalItem.TabIndex = 58;
            this.m_buttonLoadFromExternalItem.Text = "外部加载";
            this.m_buttonLoadFromExternalItem.UseVisualStyleBackColor = true;
            this.m_buttonLoadFromExternalItem.Click += new System.EventHandler(this.m_buttonLoadFromExternal_Click);
            // 
            // m_comboBoxCurrencyType
            // 
            this.m_comboBoxCurrencyType.FormattingEnabled = true;
            this.m_comboBoxCurrencyType.Location = new System.Drawing.Point(913, 357);
            this.m_comboBoxCurrencyType.Name = "m_comboBoxCurrencyType";
            this.m_comboBoxCurrencyType.Size = new System.Drawing.Size(81, 20);
            this.m_comboBoxCurrencyType.TabIndex = 57;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(633, 58);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 12);
            this.label31.TabIndex = 56;
            this.label31.Text = "出产商品时间";
            // 
            // m_listBoxDailyProduceTime
            // 
            this.m_listBoxDailyProduceTime.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxDailyProduceTime.FormattingEnabled = true;
            this.m_listBoxDailyProduceTime.ItemHeight = 12;
            this.m_listBoxDailyProduceTime.Location = new System.Drawing.Point(636, 78);
            this.m_listBoxDailyProduceTime.Name = "m_listBoxDailyProduceTime";
            this.m_listBoxDailyProduceTime.Size = new System.Drawing.Size(74, 268);
            this.m_listBoxDailyProduceTime.TabIndex = 55;
            // 
            // label单次购买数量
            // 
            this.label单次购买数量.AutoSize = true;
            this.label单次购买数量.Location = new System.Drawing.Point(411, 58);
            this.label单次购买数量.Name = "label单次购买数量";
            this.label单次购买数量.Size = new System.Drawing.Size(77, 12);
            this.label单次购买数量.TabIndex = 54;
            this.label单次购买数量.Text = "单次购买数量";
            // 
            // m_listBoxPerPurchaseNum
            // 
            this.m_listBoxPerPurchaseNum.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxPerPurchaseNum.FormattingEnabled = true;
            this.m_listBoxPerPurchaseNum.ItemHeight = 12;
            this.m_listBoxPerPurchaseNum.Location = new System.Drawing.Point(414, 78);
            this.m_listBoxPerPurchaseNum.Name = "m_listBoxPerPurchaseNum";
            this.m_listBoxPerPurchaseNum.Size = new System.Drawing.Size(74, 268);
            this.m_listBoxPerPurchaseNum.TabIndex = 53;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(844, 359);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 51;
            this.label22.Text = "货币类型";
            // 
            // m_textBoxDailyProduceTime
            // 
            this.m_textBoxDailyProduceTime.Location = new System.Drawing.Point(94, 424);
            this.m_textBoxDailyProduceTime.Name = "m_textBoxDailyProduceTime";
            this.m_textBoxDailyProduceTime.Size = new System.Drawing.Size(66, 21);
            this.m_textBoxDailyProduceTime.TabIndex = 50;
            this.m_textBoxDailyProduceTime.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 427);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 12);
            this.label21.TabIndex = 49;
            this.label21.Text = "出产商品时间";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 346);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 12);
            this.label19.TabIndex = 48;
            this.label19.Text = "单次购买数量";
            // 
            // m_textBoxPerPurchaseNum
            // 
            this.m_textBoxPerPurchaseNum.Location = new System.Drawing.Point(94, 343);
            this.m_textBoxPerPurchaseNum.Name = "m_textBoxPerPurchaseNum";
            this.m_textBoxPerPurchaseNum.Size = new System.Drawing.Size(49, 21);
            this.m_textBoxPerPurchaseNum.TabIndex = 47;
            this.m_textBoxPerPurchaseNum.Text = "1";
            // 
            // m_textBoxProduceType
            // 
            this.m_textBoxProduceType.Location = new System.Drawing.Point(94, 397);
            this.m_textBoxProduceType.Name = "m_textBoxProduceType";
            this.m_textBoxProduceType.Size = new System.Drawing.Size(49, 21);
            this.m_textBoxProduceType.TabIndex = 46;
            this.m_textBoxProduceType.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(225, 12);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 12);
            this.label18.TabIndex = 45;
            this.label18.Text = "Table 列表";
            // 
            // m_listBoxItemTable
            // 
            this.m_listBoxItemTable.ContextMenuStrip = this.m_contextMenuStripTable;
            this.m_listBoxItemTable.FormattingEnabled = true;
            this.m_listBoxItemTable.ItemHeight = 12;
            this.m_listBoxItemTable.Location = new System.Drawing.Point(227, 30);
            this.m_listBoxItemTable.Name = "m_listBoxItemTable";
            this.m_listBoxItemTable.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxItemTable.Size = new System.Drawing.Size(180, 40);
            this.m_listBoxItemTable.TabIndex = 44;
            this.m_listBoxItemTable.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.m_listBoxItemTable_MouseDoubleClick);
            this.m_listBoxItemTable.SelectedIndexChanged += new System.EventHandler(this.m_listBoxItemTable_SelectedIndexChanged);
            // 
            // m_contextMenuStripTable
            // 
            this.m_contextMenuStripTable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_toolStripMenuItemAddTab,
            this.m_toolStripMenuItemDelTab});
            this.m_contextMenuStripTable.Name = "m_contextMenuStripTable";
            this.m_contextMenuStripTable.Size = new System.Drawing.Size(120, 48);
            // 
            // m_toolStripMenuItemAddTab
            // 
            this.m_toolStripMenuItemAddTab.Name = "m_toolStripMenuItemAddTab";
            this.m_toolStripMenuItemAddTab.Size = new System.Drawing.Size(119, 22);
            this.m_toolStripMenuItemAddTab.Text = "添加 Tab";
            this.m_toolStripMenuItemAddTab.Click += new System.EventHandler(this.m_toolStripMenuItemAddTab_Click);
            // 
            // m_toolStripMenuItemDelTab
            // 
            this.m_toolStripMenuItemDelTab.Name = "m_toolStripMenuItemDelTab";
            this.m_toolStripMenuItemDelTab.Size = new System.Drawing.Size(119, 22);
            this.m_toolStripMenuItemDelTab.Text = "删除 Tab";
            this.m_toolStripMenuItemDelTab.Click += new System.EventHandler(this.m_toolStripMenuItemDelTab_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 373);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 43;
            this.label11.Text = "交易价格";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(494, 58);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 42;
            this.label23.Text = "交易价格";
            // 
            // m_listBoxItemTradePrice
            // 
            this.m_listBoxItemTradePrice.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxItemTradePrice.FormattingEnabled = true;
            this.m_listBoxItemTradePrice.ItemHeight = 12;
            this.m_listBoxItemTradePrice.Location = new System.Drawing.Point(494, 78);
            this.m_listBoxItemTradePrice.Name = "m_listBoxItemTradePrice";
            this.m_listBoxItemTradePrice.Size = new System.Drawing.Size(74, 268);
            this.m_listBoxItemTradePrice.TabIndex = 41;
            // 
            // m_textBoxItemTradePrice
            // 
            this.m_textBoxItemTradePrice.Location = new System.Drawing.Point(94, 370);
            this.m_textBoxItemTradePrice.Name = "m_textBoxItemTradePrice";
            this.m_textBoxItemTradePrice.Size = new System.Drawing.Size(49, 21);
            this.m_textBoxItemTradePrice.TabIndex = 36;
            this.m_textBoxItemTradePrice.Text = "0";
            // 
            // m_textBoxItemTradeItemID
            // 
            this.m_textBoxItemTradeItemID.Location = new System.Drawing.Point(913, 380);
            this.m_textBoxItemTradeItemID.Name = "m_textBoxItemTradeItemID";
            this.m_textBoxItemTradeItemID.Size = new System.Drawing.Size(97, 21);
            this.m_textBoxItemTradeItemID.TabIndex = 35;
            this.m_textBoxItemTradeItemID.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(844, 383);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 12);
            this.label20.TabIndex = 34;
            this.label20.Text = "交易物品ID";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(778, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 29;
            this.label15.Text = "最大容量";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(571, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 28;
            this.label16.Text = "生产类型";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(716, 58);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 27;
            this.label17.Text = "产出数量";
            // 
            // m_listBoxCapability
            // 
            this.m_listBoxCapability.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxCapability.FormattingEnabled = true;
            this.m_listBoxCapability.ItemHeight = 12;
            this.m_listBoxCapability.Location = new System.Drawing.Point(778, 78);
            this.m_listBoxCapability.Name = "m_listBoxCapability";
            this.m_listBoxCapability.Size = new System.Drawing.Size(56, 268);
            this.m_listBoxCapability.TabIndex = 26;
            this.m_listBoxCapability.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.m_listBoxCapability_MouseDoubleClick);
            // 
            // m_listBoxProduceNum
            // 
            this.m_listBoxProduceNum.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxProduceNum.FormattingEnabled = true;
            this.m_listBoxProduceNum.ItemHeight = 12;
            this.m_listBoxProduceNum.Location = new System.Drawing.Point(716, 78);
            this.m_listBoxProduceNum.Name = "m_listBoxProduceNum";
            this.m_listBoxProduceNum.Size = new System.Drawing.Size(56, 268);
            this.m_listBoxProduceNum.TabIndex = 25;
            this.m_listBoxProduceNum.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.m_listBoxProduceNum_MouseDoubleClick);
            // 
            // m_listBoxProduceType
            // 
            this.m_listBoxProduceType.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxProduceType.FormattingEnabled = true;
            this.m_listBoxProduceType.ItemHeight = 12;
            this.m_listBoxProduceType.Location = new System.Drawing.Point(574, 78);
            this.m_listBoxProduceType.Name = "m_listBoxProduceType";
            this.m_listBoxProduceType.Size = new System.Drawing.Size(56, 268);
            this.m_listBoxProduceType.TabIndex = 24;
            // 
            // m_textBoxCapability
            // 
            this.m_textBoxCapability.Location = new System.Drawing.Point(94, 478);
            this.m_textBoxCapability.Name = "m_textBoxCapability";
            this.m_textBoxCapability.Size = new System.Drawing.Size(66, 21);
            this.m_textBoxCapability.TabIndex = 23;
            this.m_textBoxCapability.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 481);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 22;
            this.label14.Text = "最大容量";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 400);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 20;
            this.label13.Text = "生产类型";
            // 
            // m_textBoxProduceNum
            // 
            this.m_textBoxProduceNum.Location = new System.Drawing.Point(94, 451);
            this.m_textBoxProduceNum.Name = "m_textBoxProduceNum";
            this.m_textBoxProduceNum.Size = new System.Drawing.Size(66, 21);
            this.m_textBoxProduceNum.TabIndex = 19;
            this.m_textBoxProduceNum.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 454);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 18;
            this.label12.Text = "产出数量";
            // 
            // m_tabPageSkillShop
            // 
            this.m_tabPageSkillShop.Controls.Add(this.m_buttonLoadFromExternalSkill);
            this.m_tabPageSkillShop.Controls.Add(this.m_listBoxSkillProfession);
            this.m_tabPageSkillShop.Controls.Add(this.label28);
            this.m_tabPageSkillShop.Controls.Add(this.m_textBoxSkillProfession);
            this.m_tabPageSkillShop.Controls.Add(this.label25);
            this.m_tabPageSkillShop.Controls.Add(this.m_comboBoxSkillCurrencyType);
            this.m_tabPageSkillShop.Controls.Add(this.label24);
            this.m_tabPageSkillShop.Controls.Add(this.m_textBoxSkillTradeItemID);
            this.m_tabPageSkillShop.Controls.Add(this.label27);
            this.m_tabPageSkillShop.Controls.Add(this.label32);
            this.m_tabPageSkillShop.Controls.Add(this.m_listBoxSkillTable);
            this.m_tabPageSkillShop.Controls.Add(this.m_textBoxSkillTradePrice);
            this.m_tabPageSkillShop.Controls.Add(this.label30);
            this.m_tabPageSkillShop.Controls.Add(this.m_listBoxSkillTradePrice);
            this.m_tabPageSkillShop.Controls.Add(this.label26);
            this.m_tabPageSkillShop.Controls.Add(this.m_textBoxSkillLevel);
            this.m_tabPageSkillShop.Controls.Add(this.label9);
            this.m_tabPageSkillShop.Controls.Add(this.label10);
            this.m_tabPageSkillShop.Controls.Add(this.m_listBoxSkillLevel);
            this.m_tabPageSkillShop.Controls.Add(this.m_listBoxAllSkills);
            this.m_tabPageSkillShop.Controls.Add(this.m_btnAddSkill);
            this.m_tabPageSkillShop.Controls.Add(this.m_btnSaveSkillShop);
            this.m_tabPageSkillShop.Controls.Add(this.m_btnRemoveSkill);
            this.m_tabPageSkillShop.Controls.Add(this.comboBox1);
            this.m_tabPageSkillShop.Controls.Add(this.m_textBoxSkillShopPriceRate);
            this.m_tabPageSkillShop.Controls.Add(this.m_listBoxSelectedSkills);
            this.m_tabPageSkillShop.Controls.Add(this.label5);
            this.m_tabPageSkillShop.Controls.Add(this.m_listBoxSkillShop);
            this.m_tabPageSkillShop.Controls.Add(this.m_btnAddSkillShop);
            this.m_tabPageSkillShop.Controls.Add(this.label6);
            this.m_tabPageSkillShop.Controls.Add(this.m_textBoxSkillShopDesc);
            this.m_tabPageSkillShop.Controls.Add(this.label7);
            this.m_tabPageSkillShop.Controls.Add(this.label8);
            this.m_tabPageSkillShop.Controls.Add(this.m_textBoxSkillShopID);
            this.m_tabPageSkillShop.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageSkillShop.Name = "m_tabPageSkillShop";
            this.m_tabPageSkillShop.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageSkillShop.Size = new System.Drawing.Size(1017, 514);
            this.m_tabPageSkillShop.TabIndex = 1;
            this.m_tabPageSkillShop.Text = "技能商店";
            this.m_tabPageSkillShop.UseVisualStyleBackColor = true;
            // 
            // m_buttonLoadFromExternalSkill
            // 
            this.m_buttonLoadFromExternalSkill.Location = new System.Drawing.Point(453, 368);
            this.m_buttonLoadFromExternalSkill.Name = "m_buttonLoadFromExternalSkill";
            this.m_buttonLoadFromExternalSkill.Size = new System.Drawing.Size(195, 77);
            this.m_buttonLoadFromExternalSkill.TabIndex = 68;
            this.m_buttonLoadFromExternalSkill.Text = "外部加载";
            this.m_buttonLoadFromExternalSkill.UseVisualStyleBackColor = true;
            this.m_buttonLoadFromExternalSkill.Click += new System.EventHandler(this.m_buttonLoadFromExternalSkill_Click);
            // 
            // m_listBoxSkillProfession
            // 
            this.m_listBoxSkillProfession.FormattingEnabled = true;
            this.m_listBoxSkillProfession.ItemHeight = 12;
            this.m_listBoxSkillProfession.Location = new System.Drawing.Point(595, 77);
            this.m_listBoxSkillProfession.Name = "m_listBoxSkillProfession";
            this.m_listBoxSkillProfession.Size = new System.Drawing.Size(53, 268);
            this.m_listBoxSkillProfession.TabIndex = 67;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(595, 59);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 66;
            this.label28.Text = "所需职业";
            // 
            // m_textBoxSkillProfession
            // 
            this.m_textBoxSkillProfession.Location = new System.Drawing.Point(76, 422);
            this.m_textBoxSkillProfession.Name = "m_textBoxSkillProfession";
            this.m_textBoxSkillProfession.Size = new System.Drawing.Size(96, 21);
            this.m_textBoxSkillProfession.TabIndex = 65;
            this.m_textBoxSkillProfession.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(17, 425);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 64;
            this.label25.Text = "所需职业";
            // 
            // m_comboBoxSkillCurrencyType
            // 
            this.m_comboBoxSkillCurrencyType.FormattingEnabled = true;
            this.m_comboBoxSkillCurrencyType.Location = new System.Drawing.Point(888, 362);
            this.m_comboBoxSkillCurrencyType.Name = "m_comboBoxSkillCurrencyType";
            this.m_comboBoxSkillCurrencyType.Size = new System.Drawing.Size(124, 20);
            this.m_comboBoxSkillCurrencyType.TabIndex = 63;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(816, 365);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 62;
            this.label24.Text = "货币类型";
            // 
            // m_textBoxSkillTradeItemID
            // 
            this.m_textBoxSkillTradeItemID.Location = new System.Drawing.Point(888, 387);
            this.m_textBoxSkillTradeItemID.Name = "m_textBoxSkillTradeItemID";
            this.m_textBoxSkillTradeItemID.Size = new System.Drawing.Size(124, 21);
            this.m_textBoxSkillTradeItemID.TabIndex = 61;
            this.m_textBoxSkillTradeItemID.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(816, 391);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(65, 12);
            this.label27.TabIndex = 60;
            this.label27.Text = "交易物品ID";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(230, 13);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(65, 12);
            this.label32.TabIndex = 59;
            this.label32.Text = "Table 列表";
            // 
            // m_listBoxSkillTable
            // 
            this.m_listBoxSkillTable.ContextMenuStrip = this.m_contextMenuStripTable;
            this.m_listBoxSkillTable.FormattingEnabled = true;
            this.m_listBoxSkillTable.ItemHeight = 12;
            this.m_listBoxSkillTable.Location = new System.Drawing.Point(232, 31);
            this.m_listBoxSkillTable.Name = "m_listBoxSkillTable";
            this.m_listBoxSkillTable.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxSkillTable.Size = new System.Drawing.Size(186, 40);
            this.m_listBoxSkillTable.TabIndex = 58;
            this.m_listBoxSkillTable.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSkillTable_SelectedIndexChanged);
            // 
            // m_textBoxSkillTradePrice
            // 
            this.m_textBoxSkillTradePrice.Location = new System.Drawing.Point(76, 395);
            this.m_textBoxSkillTradePrice.Name = "m_textBoxSkillTradePrice";
            this.m_textBoxSkillTradePrice.Size = new System.Drawing.Size(96, 21);
            this.m_textBoxSkillTradePrice.TabIndex = 52;
            this.m_textBoxSkillTradePrice.Text = "0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(17, 398);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 12);
            this.label30.TabIndex = 51;
            this.label30.Text = "交易价格";
            // 
            // m_listBoxSkillTradePrice
            // 
            this.m_listBoxSkillTradePrice.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxSkillTradePrice.FormattingEnabled = true;
            this.m_listBoxSkillTradePrice.ItemHeight = 12;
            this.m_listBoxSkillTradePrice.Location = new System.Drawing.Point(489, 77);
            this.m_listBoxSkillTradePrice.Name = "m_listBoxSkillTradePrice";
            this.m_listBoxSkillTradePrice.Size = new System.Drawing.Size(100, 268);
            this.m_listBoxSkillTradePrice.TabIndex = 49;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(487, 59);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 12);
            this.label26.TabIndex = 46;
            this.label26.Text = "交易价格";
            // 
            // m_textBoxSkillLevel
            // 
            this.m_textBoxSkillLevel.Location = new System.Drawing.Point(76, 368);
            this.m_textBoxSkillLevel.Name = "m_textBoxSkillLevel";
            this.m_textBoxSkillLevel.Size = new System.Drawing.Size(96, 21);
            this.m_textBoxSkillLevel.TabIndex = 36;
            this.m_textBoxSkillLevel.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 371);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 35;
            this.label9.Text = "所需等级";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(427, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 34;
            this.label10.Text = "所需等级";
            // 
            // m_listBoxSkillLevel
            // 
            this.m_listBoxSkillLevel.FormattingEnabled = true;
            this.m_listBoxSkillLevel.ItemHeight = 12;
            this.m_listBoxSkillLevel.Location = new System.Drawing.Point(424, 77);
            this.m_listBoxSkillLevel.Name = "m_listBoxSkillLevel";
            this.m_listBoxSkillLevel.Size = new System.Drawing.Size(56, 268);
            this.m_listBoxSkillLevel.TabIndex = 32;
            this.m_listBoxSkillLevel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.m_listBoxSkillLevel_MouseDoubleClick);
            // 
            // m_listBoxAllSkills
            // 
            this.m_listBoxAllSkills.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxAllSkills.FormattingEnabled = true;
            this.m_listBoxAllSkills.ItemHeight = 12;
            this.m_listBoxAllSkills.Location = new System.Drawing.Point(6, 31);
            this.m_listBoxAllSkills.Name = "m_listBoxAllSkills";
            this.m_listBoxAllSkills.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxAllSkills.Size = new System.Drawing.Size(186, 316);
            this.m_listBoxAllSkills.TabIndex = 16;
            // 
            // m_btnAddSkill
            // 
            this.m_btnAddSkill.Location = new System.Drawing.Point(198, 126);
            this.m_btnAddSkill.Name = "m_btnAddSkill";
            this.m_btnAddSkill.Size = new System.Drawing.Size(28, 22);
            this.m_btnAddSkill.TabIndex = 17;
            this.m_btnAddSkill.Text = "->";
            this.m_btnAddSkill.UseVisualStyleBackColor = true;
            this.m_btnAddSkill.Click += new System.EventHandler(this.m_btnAddSkill_Click);
            // 
            // m_btnSaveSkillShop
            // 
            this.m_btnSaveSkillShop.Location = new System.Drawing.Point(232, 368);
            this.m_btnSaveSkillShop.Name = "m_btnSaveSkillShop";
            this.m_btnSaveSkillShop.Size = new System.Drawing.Size(195, 77);
            this.m_btnSaveSkillShop.TabIndex = 31;
            this.m_btnSaveSkillShop.Text = "保存";
            this.m_btnSaveSkillShop.UseVisualStyleBackColor = true;
            this.m_btnSaveSkillShop.Click += new System.EventHandler(this.m_btnSaveSkillShop_Click);
            // 
            // m_btnRemoveSkill
            // 
            this.m_btnRemoveSkill.Location = new System.Drawing.Point(198, 154);
            this.m_btnRemoveSkill.Name = "m_btnRemoveSkill";
            this.m_btnRemoveSkill.Size = new System.Drawing.Size(28, 22);
            this.m_btnRemoveSkill.TabIndex = 18;
            this.m_btnRemoveSkill.Text = "<-";
            this.m_btnRemoveSkill.UseVisualStyleBackColor = true;
            this.m_btnRemoveSkill.Click += new System.EventHandler(this.m_btnRemoveSkill_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(186, 20);
            this.comboBox1.TabIndex = 19;
            // 
            // m_textBoxSkillShopPriceRate
            // 
            this.m_textBoxSkillShopPriceRate.Location = new System.Drawing.Point(888, 336);
            this.m_textBoxSkillShopPriceRate.Name = "m_textBoxSkillShopPriceRate";
            this.m_textBoxSkillShopPriceRate.Size = new System.Drawing.Size(124, 21);
            this.m_textBoxSkillShopPriceRate.TabIndex = 29;
            this.m_textBoxSkillShopPriceRate.Text = "1.0";
            // 
            // m_listBoxSelectedSkills
            // 
            this.m_listBoxSelectedSkills.ContextMenuStrip = this.m_contextMenuStripListBox;
            this.m_listBoxSelectedSkills.FormattingEnabled = true;
            this.m_listBoxSelectedSkills.ItemHeight = 12;
            this.m_listBoxSelectedSkills.Location = new System.Drawing.Point(232, 77);
            this.m_listBoxSelectedSkills.Name = "m_listBoxSelectedSkills";
            this.m_listBoxSelectedSkills.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_listBoxSelectedSkills.Size = new System.Drawing.Size(186, 268);
            this.m_listBoxSelectedSkills.TabIndex = 20;
            this.m_listBoxSelectedSkills.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSelectedSkills_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(816, 339);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 28;
            this.label5.Text = "价格率";
            // 
            // m_listBoxSkillShop
            // 
            this.m_listBoxSkillShop.FormattingEnabled = true;
            this.m_listBoxSkillShop.ItemHeight = 12;
            this.m_listBoxSkillShop.Location = new System.Drawing.Point(814, 31);
            this.m_listBoxSkillShop.Name = "m_listBoxSkillShop";
            this.m_listBoxSkillShop.Size = new System.Drawing.Size(165, 244);
            this.m_listBoxSkillShop.TabIndex = 21;
            this.m_listBoxSkillShop.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSkillShop_SelectedIndexChanged);
            // 
            // m_btnAddSkillShop
            // 
            this.m_btnAddSkillShop.Location = new System.Drawing.Point(864, 427);
            this.m_btnAddSkillShop.Name = "m_btnAddSkillShop";
            this.m_btnAddSkillShop.Size = new System.Drawing.Size(87, 28);
            this.m_btnAddSkillShop.TabIndex = 27;
            this.m_btnAddSkillShop.Text = "增加/修改";
            this.m_btnAddSkillShop.UseVisualStyleBackColor = true;
            this.m_btnAddSkillShop.Click += new System.EventHandler(this.m_btnAddSkillShop_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(812, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 22;
            this.label6.Text = "商店列表";
            // 
            // m_textBoxSkillShopDesc
            // 
            this.m_textBoxSkillShopDesc.Location = new System.Drawing.Point(888, 310);
            this.m_textBoxSkillShopDesc.Name = "m_textBoxSkillShopDesc";
            this.m_textBoxSkillShopDesc.Size = new System.Drawing.Size(124, 21);
            this.m_textBoxSkillShopDesc.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(816, 287);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 23;
            this.label7.Text = "ID";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(816, 313);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 25;
            this.label8.Text = "描述";
            // 
            // m_textBoxSkillShopID
            // 
            this.m_textBoxSkillShopID.Location = new System.Drawing.Point(888, 284);
            this.m_textBoxSkillShopID.Name = "m_textBoxSkillShopID";
            this.m_textBoxSkillShopID.Size = new System.Drawing.Size(124, 21);
            this.m_textBoxSkillShopID.TabIndex = 24;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(204, 363);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(17, 14);
            this.button1.TabIndex = 59;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ShopEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 539);
            this.Controls.Add(this.m_tabControlShops);
            this.Name = "ShopEditForm";
            this.Text = "商店编辑";
            this.Load += new System.EventHandler(this.ShopEditForm_Load);
            this.m_contextMenuStripListBox.ResumeLayout(false);
            this.m_tabControlShops.ResumeLayout(false);
            this.m_tabPageItemShop.ResumeLayout(false);
            this.m_tabPageItemShop.PerformLayout();
            this.m_contextMenuStripTable.ResumeLayout(false);
            this.m_tabPageSkillShop.ResumeLayout(false);
            this.m_tabPageSkillShop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox m_listBoxAllItems;
        private System.Windows.Forms.Button m_btnAddItem;
        private System.Windows.Forms.Button m_btnRemoveItem;
        private System.Windows.Forms.ComboBox m_comboBoxItemFilter;
        private System.Windows.Forms.ListBox m_listBoxSelectedItems;
        private System.Windows.Forms.ListBox m_listBoxItemShop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_textBoxItemShopID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox m_textBoxItemShopDesc;
        private System.Windows.Forms.Button m_btnAddItemShop;
        private ItemShop m_OnEditItemShop = null;
        private SkillShop m_OnEditSkillShop = null;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox m_textBoxItemShopPriceRate;
        private System.Windows.Forms.Button m_btnSaveItemShop;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStripListBox;
        private System.Windows.Forms.ToolStripMenuItem 全选ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 清除ToolStripMenuItem;
        private System.Windows.Forms.TabControl m_tabControlShops;
        private System.Windows.Forms.TabPage m_tabPageItemShop;
        private System.Windows.Forms.TabPage m_tabPageSkillShop;
        private System.Windows.Forms.ListBox m_listBoxAllSkills;
        private System.Windows.Forms.Button m_btnAddSkill;
        private System.Windows.Forms.Button m_btnSaveSkillShop;
        private System.Windows.Forms.Button m_btnRemoveSkill;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox m_textBoxSkillShopPriceRate;
        private System.Windows.Forms.ListBox m_listBoxSelectedSkills;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox m_listBoxSkillShop;
        private System.Windows.Forms.Button m_btnAddSkillShop;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox m_textBoxSkillShopDesc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox m_textBoxSkillShopID;
        private System.Windows.Forms.ListBox m_listBoxSkillLevel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox m_textBoxSkillLevel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox m_textBoxProduceNum;
        private System.Windows.Forms.TextBox m_textBoxCapability;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ListBox m_listBoxCapability;
        private System.Windows.Forms.ListBox m_listBoxProduceNum;
        private System.Windows.Forms.ListBox m_listBoxProduceType;
        private System.Windows.Forms.TextBox m_textBoxItemTradeItemID;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox m_textBoxItemTradePrice;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ListBox m_listBoxItemTradePrice;
        private System.Windows.Forms.ListBox m_listBoxSkillTradePrice;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox m_textBoxSkillTradePrice;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox m_textBoxProduceType;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListBox m_listBoxItemTable;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox m_textBoxPerPurchaseNum;
        private System.Windows.Forms.TextBox m_textBoxDailyProduceTime;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label单次购买数量;
        private System.Windows.Forms.ListBox m_listBoxPerPurchaseNum;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ListBox m_listBoxDailyProduceTime;
        private System.Windows.Forms.ComboBox m_comboBoxCurrencyType;
        private System.Windows.Forms.ContextMenuStrip m_contextMenuStripTable;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemAddTab;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemDelTab;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ListBox m_listBoxSkillTable;
        private System.Windows.Forms.ComboBox m_comboBoxSkillCurrencyType;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox m_textBoxSkillTradeItemID;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox m_textBoxSkillProfession;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ListBox m_listBoxSkillProfession;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button m_buttonLoadFromExternalItem;
        private System.Windows.Forms.Button m_buttonLoadFromExternalSkill;
        private System.Windows.Forms.Button button1;   // 当前 shop
    }
}