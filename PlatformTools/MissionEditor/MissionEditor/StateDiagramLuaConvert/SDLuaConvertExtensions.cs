﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using System.IO;

/* 定义在 StateDiagram.cs 中的 enum
// 组成状态图元素所属的类型，是与领取任务相关的，执行任务相关的或者还任务相关的。
public enum ElementCategory : int
{
    EC_OTHER,
    EC_GET,
    EC_PROCESS,
    EC_SUBMIT,
    EC_INVALIDATE,
}

// 状态图宿主的类型
public enum OwnerType : int
{
    DT_NPC = 1,
    DT_NPC_DEATH = 2,
    DT_MONSTER_DEATH = 3,
    DT_ITEM = 4,
    DT_AREA = 5,
    DT_INVALID,
}
*/

namespace MissionEditor
{
    // 扩展接口定义
    public class SDLuaConvertExtensions
    {
        // 将任务转换为 luaTable.
        public Hashtable EX_GetMissionTable(int iMissionID)
        {
            // 获取任务管理类
            MissionManager missionMgr = MainForm.Instance.MissionMgr;
            
            // 获取要查找的任务
            Mission mission = missionMgr.FindByID(iMissionID);

            // 任务属性表
            Hashtable attrTable = new Hashtable();

            // 获取任务的属性
            if(mission == null)
            {
                return null;
            }
            else
            {
                // 所有任务都有的属性
                attrTable["id"] = mission.ID;                               // m_iID                任务编号
                attrTable["name"] = mission.Name;                           // m_strName            任务名称
                //m_strGetDesc;           // 任务描述
                //m_strSubmitDesc;           // 任务描述
                attrTable["type"] = mission.Type;                           // m_strType            任务类型
                attrTable["sub_mission_type"] = mission.SubMissionType;     // m_strSubMissionType  细分任务类型。 计数器类对应 杀人，杀怪，收集物品。对话类对应简单对话，送物品，取物品 
                attrTable["flow_type"] = mission.FlowType;                  // m_strFlowType        任务流程类型，A为2阶，B为3阶
                attrTable["class"] = mission.Class;                         // m_iClass             任务阶级 0-新手任务 1-主线任务 2-支线任务 3-副本任务 4-活动任务 5-系统任务
                attrTable["level"] = mission.Level;                         // m_iLevel;            任务等级
                //m_iRecordReserve;   // 是否保留记录
                //m_strSimpleDesc = "";    // 任务的简单描述
                attrTable["lead_map"] = mission.LeadMap;                    // m_iLeadMap           任务所属的地图
                attrTable["star_level"] = mission.StarLevel;                // m_iStarLevel         任务的难度（星级）
                attrTable["time_limit"] = mission.TimeLimit;                // m_iTimeLimit         任务的时间限制



                attrTable["item_provider"] = mission.ItemProvider;          // m_bItemProvider      true - 使用道具获取任务   false - 和 NPC 对话获取任务
                attrTable["provider"] = mission.Provider;                   // m_iProvider          任务提供者 creator id
                attrTable["submit_to"] = mission.SubmitTo;                  // m_iSubmitTo          交任务对象 creator id

                // m_Conditions         接任务条件  MissionAward 列表
                object[] conditionList = new object[mission.Conditions.Count];
                for (int index = 0; index < conditionList.Length; index++)
                {
                    MissionCondition condition = mission.Conditions[index] as MissionCondition;
                    object[] arrCond = { condition.Condition, condition.Value };
                    conditionList[index] = arrCond;
                }
                attrTable["conditions"] = conditionList;

                // m_FixedAwards        固定奖励奖励 String - int
                object[] fixedAwardList = new object[mission.FixedAwards.Count];
                for (int index = 0; index < fixedAwardList.Length; index++)
                {
                    MissionAward award = mission.FixedAwards[index] as MissionAward;
                    object[] arrAwd = { award.Award, award.Value };
                    fixedAwardList[index] = arrAwd;
                }
                attrTable["fixed_awards"] = fixedAwardList;

                //m_SelectedAwards = new ArrayList(); // 可选奖励 String - int



                // 以下根据任务类型可选
                attrTable["target_npc"] = mission.TargetNPC;                        // m_iTargetNPC             目标 npc creator id
                //attrTable["target_monster_begin"] = mission.TargetMonsterBegin;     // m_iTargetMonsterBegin    目标怪物 npc id 范围
                //attrTable["target_monster_end"] = mission.TargetMonsterEnd;         // m_iTargetMonsterEnd      目标怪物 npc id
                //attrTable["target_item_begin"] = mission.TargetItemBegin;           // m_iTargetItemBegin       目标物件
                //attrTable["target_item_end"] = mission.TargetItemEnd;               // m_iTargetItemEnd         目标物件

                attrTable["target_begin"] = mission.TargetBegin;                    // m_iTargetBegin           目标起始
                attrTable["target_end"] = mission.TargetEnd;                        // m_iTargetEnd             目标终止
                
                attrTable["target_property"] = mission.TargetProperty;              // m_iTargetProperty        目标属性
                attrTable["target_value"] = mission.TargetValue;                    // m_iTargetValue           目标值
                attrTable["track_name"] = mission.TrackName;                        // m_strTrackName           追踪目标名称
                attrTable["share"] = mission.Share;                                 // m_bShare                 共享任务
                attrTable["repeat"] = mission.Repeat;                               // m_bRepeat                可重复任务
                attrTable["give_up"] = mission.GiveUp;                              // m_bGiveUp                可放弃任务
                attrTable["get_hint"] = mission.GetHint;                            // m_strGetHint             接任务追踪
            }

            return attrTable;
        }

        // 判断状态图是否存在
        public bool EX_IsStateDiagramExisted(String strStateName)
        {
            return StateDiagramManager.Instance.DiagramMap.ContainsKey(strStateName);
        }

        // 根据状态图名称获取 ID
        public int EX_GetStateDiagram(String strDiaName)
        {
            StateDiagram stateDia = StateDiagramManager.Instance.GetStateDiagram(strDiaName);
            if (stateDia != null)
            {
                return stateDia.ID;
            }
            else
            {
                return -1;
            }
        }

        // 获取某任务对应状态图名称
        public object[] EX_GenerateStateDiagramID_Name(int iMissionID, int iElementCategory)
        {
            // 状态图 ID 生成规则：
            // NPC对话：1 + NPCID后5位
            // NPC死亡：2 + NPCID后5位
            // 怪物死亡：3 + 怪物ID后5位
            // 物品脚本：4 + ItemID后5位
            // 区域脚本：5 + 6位区域ID

            // 状态图名称规则：
            // NPCPrefix/ITEMPrefix + ID

            // 根据 iMissionID 找到 任务数据。
            MissionManager missionMgr = MainForm.Instance.MissionMgr;
            Mission mission = missionMgr.FindByID(iMissionID);
            if(mission == null)
            {
                return null;
            }

            // 根据 iElementCategory 找到 get/process/submit npc/item 的编号。
            StateDiagram.OwnerType ownerType = StateDiagram.OwnerType.DT_NPC;
            int iOwnerID = 0;

            if(iElementCategory == 0)   // get
            {
                if(mission.ItemProvider)
                {
                    ownerType = StateDiagram.OwnerType.DT_ITEM;
                }
                else if(mission.RegionProvider)
                {
                    ownerType = StateDiagram.OwnerType.DT_AREA;
                }
                else
                {
                    ownerType = StateDiagram.OwnerType.DT_NPC;
                }

                iOwnerID = mission.Provider;
            }
            else if(iElementCategory == 1)  // process
            {
                // 探索类任务的特殊处理，区域完成任务
                if (mission.Type.Equals(OptionManager.MissionType.探索类.ToString()))
                {
                    iOwnerID = mission.TargetArea;
                    ownerType = StateDiagram.OwnerType.DT_AREA;
                }
                else
                {
                    // 其余情况
                    iOwnerID = mission.TargetNPC;
                }
            }
            else if (iElementCategory == 2) // submit
            {
                iOwnerID = mission.SubmitTo;
            }
            else
            {
                return null;
            }

            // 调用 StateDiagram.GenerateStateDiagramID 生成状态图 ID
            int iStateDiagramID = StateDiagram.GenerateStateDiagramID(ownerType, iOwnerID);
            if(iStateDiagramID == -1)
            {
                return null;
            }

            //  取出iOwnerID的后5位用于生成name
            String strOwnerID = iOwnerID.ToString();
            String strLast5 = strOwnerID.Substring(strOwnerID.Length - 5, 5);
            String strStateDiagramName = "";

            if(ownerType == StateDiagram.OwnerType.DT_NPC)
            {
                strStateDiagramName = OptionManager.NPCPrefix + "1" + strLast5;
            }
            else if(ownerType == StateDiagram.OwnerType.DT_ITEM)
            {
                strStateDiagramName = OptionManager.ItemPrefix + "4" + strLast5;
            }
            else if(ownerType == StateDiagram.OwnerType.DT_AREA)
            {
                strStateDiagramName = OptionManager.AreaPrefix + "5" + iOwnerID.ToString();
            }
            else
            {
                return null;
            }

            // 将结果返回
            if(strStateDiagramName.Length == 0)
            {
                return null;
            }
            object[] result = new object[2];
            result[0] = iStateDiagramID;    // StateDia ID
            result[1] = strStateDiagramName;    // StateDia Name
            return result;
        }

        // 将状态图保存到磁盘
        public int EX_SaveStateDiagram(String strDiaName)
        {
            StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strDiaName);

            if (dia == null)
            {
                return -1;
            }

            StateDiagramManager.SaveStateDiagram(dia);

            return 0;   // sucess return
        }

        // 创建新的状态图并保存到 StateDiagramManager
        public int EX_GenerateStateDiagram(int iDiaID, String strDiaName)
        {
            // 判断状态图是否已经存在
            if (StateDiagramManager.Instance.DiagramMap.ContainsKey(strDiaName))
            {
                return 0;
            }
            else
            {
                // 生成一个新的状态图
                StateDiagram newDiagram = new StateDiagram();
                newDiagram.ID = iDiaID;
                newDiagram.Name = strDiaName;

                // 保存到manager
                StateDiagramManager.Instance.SetDiagram(strDiaName, newDiagram);

                // 返回ID用于链式操作
                return iDiaID;
            }     
        }

        // 设置当前被操作的状态图
        public int EX_SetOpStateDiagram(String strDiaName)
        {
            // 根据name获取状态图
            StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strDiaName);
            if(dia == null)
            {
                return -1;
            }
            else
            {
                SDLuaConvertScript.Instance.OpStateDiagram = dia;
                return dia.ID;
            }
        }

        // 获取状态图的类型，npc，item，area
        public int EX_GetStateDiagramOwnerType()
        {
            // 根据name获取状态图
            StateDiagram dia = SDLuaConvertScript.Instance.OpStateDiagram;
            String strDiaName = dia.Name;

            if (dia == null)
            {
                return -1;
            }
            else
            {
                if (strDiaName.Contains(OptionManager.NPCPrefix))
                {
                    return 1;
                }
                else if(strDiaName.Contains(OptionManager.ItemPrefix))
                {
                    return 4;
                }
                else if (strDiaName.Contains(OptionManager.AreaPrefix))
                {
                    return 5;
                } 
                else
                {
                    return -1;
                }
                
            }
        }

        // 添加一个状态
        public int EX_AddState(String strStateName, int iMissionID, int iElementCategory)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if(currDia == null)
            {
                return -1;
            }

            // 判断该状态是否已经存在
            State newState = currDia.GetStateByName(strStateName);
            if(newState != null)
            {
                return -1;
            }

            // 生成一个新的state
            int iID = currDia.GetMaxStateID() + 1;
            StateDiagram.ElementCategory category = (StateDiagram.ElementCategory)iElementCategory;
            newState = new State(iID, strStateName, StateDiagramConvertor.GetLocationOnCanvas(iID), category);
            newState.MissionID = iMissionID;
            newState.AutoGenerated = true;

            // 将新的state添加入dia中
            currDia.StateList.Add(newState);

            return iID;
        }

        // 添加链接
        public int EX_AddTranslation(int iBeginID, int iEndID, int iOption, object[] arrCondition)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 获取开始状态
            State beginState = currDia.GetStateByID(iBeginID);
            if (beginState == null)
            {
                return -1;
            }

            // 获取结束状态
            State endState = currDia.GetStateByID(iEndID);
            if (endState == null)
            {
                return -1;
            }

            // 生成一个新的Translation
            Translation newTrans = new Translation(beginState, endState, (currDia.GetMaxTranslationID()+1));

            // 设置条件
            if(arrCondition != null)
            {
                for (int index = 0; index < arrCondition.Length;index++ )
                {
                    object[] condition = arrCondition[index] as object[];
                    String strCondition = condition[0] as String;
                    String strValue = condition[1] as String;
                    MissionCondition mc = new MissionCondition(strCondition, strValue);
                    newTrans.Conditions.Add(mc);
                }
            }

            // 设置translation属性，并加入dia
            newTrans.CurrentOption = iOption;
            newTrans.AutoGenerated = true;
            currDia.TranslationList.Add(newTrans);

            // 返回Translation的ID
            return newTrans.ID;
        }

        // 根据状态名获取ID，没有找到返回-1
        public int EX_GetStateID(String strStateName)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据Name找到State
            State state = currDia.GetStateByName(strStateName);
            if(state == null)
            {
                return -1;
            }

            // 返回State的ID
            return state.ID;
        }

        // 根据begionID和endID获取TranslationID，没有找到返回-1
        public int EX_GetTranslation(int iBeginID, int iEndID)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 获取开始状态
            State beginState = currDia.GetStateByID(iBeginID);
            if (beginState == null)
            {
                return -1;
            }

            // 获取结束状态
            State endState = currDia.GetStateByID(iEndID);
            if (endState == null)
            {
                return -1;
            }

            // 遍历TanslationList
            foreach (Translation trans in currDia.TranslationList)
            {
                if (trans.StartStateID == beginState.ID
                    && trans.EndStateID == endState.ID)
                {
                    return trans.ID;
                }
            }

            // 没有找到，返回-1
            return -1;
        }

        // 设置一个状态的dialog
        public int EX_SetDialog(int iStateID, String strDialog)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 设置Dialog
            state.Dialog = strDialog;

            return iStateID;
        }

        // 添加一个option，成功则返回对应的optionID，失败返回-1
        public int EX_AddOption(int iStateID, int iMissionID, String strOption, object[] arrCondition, int iElementCategory)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 设置option的一些属性
            int iOptionID = state.MaxOptionValue + 1;
            StateDiagram.ElementCategory category = (StateDiagram.ElementCategory)iElementCategory;
            
            // 生成一个新的option
            DialogOption option = new DialogOption(strOption, iOptionID, iMissionID, category);

            // 设置条件
            if (arrCondition != null)
            {
                for (int index = 0; index < arrCondition.Length; index++)
                {
                    object[] condition = arrCondition[index] as object[];
                    String strCondition = condition[0] as String;
                    String strValue = condition[1] as String;
                    MissionCondition mc = new MissionCondition(strCondition, strValue);
                    option.ConditionList.Add(mc);
                }
            }

            // 加入列表，返回ID
            state.OptionList.Add(option);
            return iOptionID;
        }

        // 给一个Translation添加条件
        public int EX_AddTranslationConditions(int iTrans, object[] arrCondition)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 获取Translation
            Translation trans = currDia.GetTranslationByID(iTrans);
            if (trans == null)
            {
                return -1;
            }

            // 设置条件
            if (arrCondition != null)
            {
                for (int index = 0; index < arrCondition.Length; index++)
                {
                    object[] condition = arrCondition[index] as object[];
                    String strCondition = condition[0] as String;
                    String strValue = condition[1] as String;
                    MissionCondition mc = new MissionCondition(strCondition, strValue);
                    trans.Conditions.Add(mc);
                }
            }

            return iTrans;
        }

        // 给一个选项添加条件
        public int EX_AddOptionConditions(int iStateID, int iOption, object[] arrCondition)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 遍历寻找option
            DialogOption currOption = null;
            foreach (DialogOption option in state.OptionList)
            {
                if (option.Value == iOption)
                {
                    currOption = option;
                    break;
                }
            }

            // 设置条件
            if(currOption == null)
            {
                return -1;
            }
            else
            {
                if (arrCondition != null)
                {
                    for (int index = 0; index < arrCondition.Length; index++)
                    {
                        object[] condition = arrCondition[index] as object[];
                        String strCondition = condition[0] as String;
                        String strValue = condition[1] as String;
                        MissionCondition mc = new MissionCondition(strCondition, strValue);
                        currOption.ConditionList.Add(mc);
                    }
                }
            }

            return currOption.Value;
        }

        // 给Translation添加一个条件
        public int EX_AddTranslationCondition(int iTrans, String strCondition, String strValue)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 获取Translation
            Translation trans = currDia.GetTranslationByID(iTrans);
            if (trans == null)
            {
                return -1;
            }

            // 设置条件
            MissionCondition mc = new MissionCondition(strCondition, strValue);
            trans.Conditions.Add(mc);

            return iTrans;
        }
        // 给选项添加一个条件
        public int EX_AddOptionCondition(int iStateID, int iOption, String strCondition, String strValue)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 遍历寻找option
            DialogOption currOption = null;
            foreach (DialogOption option in state.OptionList)
            {
                if (option.Value == iOption)
                {
                    currOption = option;
                    break;
                }
            }

            // 设置条件
            if (currOption == null)
            {
                return -1;
            }
            else
            {
                MissionCondition mc = new MissionCondition(strCondition, strValue);
                currOption.ConditionList.Add(mc);
            }

            return currOption.Value;
        }

        // 从iStateID状态图中找到，属于iMissionID任务，iElementCategory类别，的option
        public int EX_GetOption(int iStateID, int iMissionID, int iElementCategory)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 遍历寻找option
            StateDiagram.ElementCategory category = (StateDiagram.ElementCategory)iElementCategory;
            foreach(DialogOption option in state.OptionList)
            {
                if (option.MissionID == iMissionID && option.Category == category)
                {
                    return option.Value;
                }
            }

            return -1;
        }

        // 清空一个State的ActionList
        // iStateID - 目标状态
        // iWhere - 0:OnEnter 1:OnOption 2:OnExit
        public int EX_ClearAtomActions(int iStateID, int iWhere)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 根据iWhere寻找要操作的list
            ArrayList currList = null;
            if (iWhere == 0)
            {
                currList = state.ActionListOnEnter;
            }
            else if (iWhere == 1)
            {
                currList = state.ActionListOnOption;
            }
            else if (iWhere == 2)
            {
                currList = state.ActionListOnExit;
            }
            else
            {
                return -1;
            }

            // 清空actionList
            currList.Clear();

            return iWhere;
        }

        public int EX_ClearTranslationConditions(int iTrans)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID寻找Translation
            Translation trans = currDia.GetTranslationByID(iTrans);
            if(trans == null)
            {
                return -1;
            }

            // 清空
            trans.Conditions.Clear();

            return iTrans;
        }
        // 清空一个选项的条件列表
        public int EX_ClearOptionConditions(int iStateID, int iOption)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 遍历寻找option
            DialogOption currOption = null;
            foreach (DialogOption option in state.OptionList)
            {
                if (option.Value == iOption)
                {
                    currOption = option;
                    break;
                }
            }

            // 清空条件
            if (currOption == null)
            {
                return -1;
            }
            else
            {
                currOption.ConditionList.Clear();
            }

            return currOption.Value;
        }
        // 设置一个Option的显示字串
        public int EX_SetOptionString(int iStateID, int iOption, String strOptionString)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 遍历寻找option
            DialogOption currOption = null;
            foreach (DialogOption option in state.OptionList)
            {
                if (option.Value == iOption)
                {
                    option.OptionString = strOptionString;
                    break;
                }
            }

            // 将Option的ID返回
            if (currOption == null)
            {
                return -1;
            }
            else
            {
                return currOption.Value;
            }
        }    

        // 添加原子操作
        // iStateID - 目标状态
        // iWhere - 0:OnEnter 1:OnOption 2:OnExit
        // arrAtomActions - 操作列表
        public int EX_AddAtomActions(int iStateID, int iWhere, object[] arrAtomActions)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 根据iWhere寻找要操作的list
            ArrayList currList = null;
            if(iWhere == 0)
            {
                currList = state.ActionListOnEnter;
            }
            else if (iWhere == 1)
            {
                currList = state.ActionListOnOption;
            }
            else if (iWhere == 2)
            {
                currList = state.ActionListOnExit;
            }
            else
            {
                return -1;
            }

            // 添加原子操作
            if (arrAtomActions == null)
            {
                return -1;
            }
            else
            {
                for (int index = 0; index < arrAtomActions.Length;index++ )
                {
                    object[] atomAction = arrAtomActions[index] as object[];
                    String strAtomAction = atomAction[0] as String;
                    String strValue = atomAction[1] as String;
                    AtomActionInstance aai = new AtomActionInstance(strAtomAction, strValue);
                    currList.Add(aai);
                }
            }

            return iWhere;
        }

        // 添加一个原子操作
        // iStateID - 目标状态
        // iWhere - 0:OnEnter 1:OnOption 2:OnExit
        // strAtomAction - 操作
        // strValue - 操作对应参数
        public int EX_AddAtomAction(int iStateID, int iWhere, String strAtomAction, String strValue)
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 根据ID找到State
            State state = currDia.GetStateByID(iStateID);
            if (state == null)
            {
                return -1;
            }

            // 根据iWhere寻找要操作的list
            ArrayList currList = null;
            if (iWhere == 0)
            {
                currList = state.ActionListOnEnter;
            }
            else if (iWhere == 1)
            {
                currList = state.ActionListOnOption;
            }
            else if (iWhere == 2)
            {
                currList = state.ActionListOnExit;
            }
            else
            {
                return -1;
            }

            // 添加原子操作
            if (strAtomAction == null || strValue == null)
            {
                return -1;
            }
            else
            {
                AtomActionInstance aai = new AtomActionInstance(strAtomAction, strValue);
                currList.Add(aai);
            }

            return iWhere;
        }

        // 根据NPC的ID获取名字
        public String EX_GetNpcNameById(int iNpcID)
        {
            NPCInfo info = NPCManager.Instance.GetNPCInfoByID(iNpcID);
            if(info == null)
            {
                return "";
            }
            else
            {
                return info.Name;
            }
        }

        // 获取状态图的TeamWorkStatus
        public int EX_GetStateDiagramStatus()
        {
            // 获取当前的状态图
            StateDiagram currDia = SDLuaConvertScript.Instance.OpStateDiagram;
            if (currDia == null)
            {
                return -1;
            }

            // 查询并返回Status
            return TeamWorkManager.Instance.GetDiagramStatus(currDia.Name).ID;
        }

        // 显示错误信息
        public int EX_ShowErrorMessage(String strError)
        {
            MessageBox.Show(strError);
            return 0;
        }

        // 创建 Log 文件
        public bool EX_CreateLogFile(String strFileName, int iMissionID)
        {
            //if (strFileName == null || strFileName.Length == 0)
            //{
            //    MessageBox.Show("非法的 Log 文件名。");
            //    return false;
            //}

            //// 保存前一份文件
            //if(m_logWriter != null)
            //{
            //    EX_SaveLogFile();
            //}
            ////if (m_logWriter == null)
            ////{
            //m_logWriter = new System.IO.StreamWriter(strFileName, true, System.Text.Encoding.GetEncoding("GB2312"));

            //m_logWriter.Write("//////////////////////////////////////////////////////////////////////////\n");
            //m_logWriter.Write("//////////" + iMissionID.ToString() + "\n");    
            //m_logWriter.Write("//////////////////////////////////////////////////////////////////////////\n");

            return true;
            //}
            //else
            //{
            //    MessageBox.Show("Log file already existed.");
            //    return false;
            //}
        }

        // 输出LOG信息
        public void EX_Log(String strLog)
        {
            //if (m_logWriter != null)
            //{
            //    m_logWriter.Write(strLog);
            //    Console.Write(strLog);
            //} 
        }

        // 保存 Log 文件
        public void EX_SaveLogFile()
        {
            //if(m_logWriter != null)
            //{
            //    m_logWriter.Flush();
            //    m_logWriter.Close();
            //    m_logWriter = null;
            //}
        }

        // 测试
        public void EX_TestInt(int iTest)
        {
            return;
        }

        public void EX_TestString(String strTest)
        {
            //MessageBox.Show(strTest);
            return;
        }

        public void EX_TestBool(bool bValue)
        {
            //MessageBox.Show(bValue);
            return;
        }

        // properties
        public static SDLuaConvertExtensions Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new SDLuaConvertExtensions();
                }
                return m_Instance;
            }
        }

        // 唯一实例
        private SDLuaConvertExtensions(){}
        private static SDLuaConvertExtensions m_Instance = null;
        //private StreamWriter m_logWriter = null;  
    }
}
