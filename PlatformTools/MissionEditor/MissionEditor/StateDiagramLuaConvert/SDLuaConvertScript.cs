﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using LuaInterface;
using System.Threading;

namespace MissionEditor
{
    // 从任务表单到状态图转换脚本的执行类
    class SDLuaConvertScript
    {
        // 初始化 Lua VM, 注册扩展函数
        private static void InitLuaVM()
        {
            m_LuaVM = new Lua();

            // 注册所有扩展函数
            Type type = SDLuaConvertExtensions.Instance.GetType();
            MethodInfo[] methods = type.GetMethods();
            foreach (MethodInfo methodInfo in methods)
            {
                LuaFunction luaFunc = m_LuaVM.RegisterFunction(methodInfo.Name, SDLuaConvertExtensions.Instance, methodInfo);//
            }
        }

        // 载入转换脚本。 只在系统启动时载入一次。
        public bool Load(String strLuaFile)
        {
            if (m_LuaVM == null)
            {
                InitLuaVM();
            }
            else
            {
                m_LuaVM.Dispose();
                m_LuaVM = null;
                InitLuaVM();
            }

            try
            {
                m_LuaVM.DoFile(strLuaFile);
            }
            catch (Exception)
            {
                MessageBox.Show("无法加载文件" + strLuaFile);
                return false;
            }

            return true;
        }

        // 将 Mission 表单数据转换到 状态图
        public bool Convert(Mission mission)
        {
            if (m_LuaVM == null)
            {
                MessageBox.Show("请先载入用来转换任务表单的 Lua 脚本。");
                return false;
            }

            if (mission == null)
            {
                return false;
            }

            LuaFunction luafunc_convert = m_LuaVM.GetFunction("convert");
            if(luafunc_convert == null)
            {
                MessageBox.Show("获取convert函数出错");
                return false;
            }

            try
            {
                //params object[] args = { 99000000 };
                object[] value = luafunc_convert.Call(mission.ID); // mission id
                if (value == null)
                {
                    SDLuaConvertExtensions.Instance.EX_SaveLogFile();
                }
                else
                {
                    int result = int.Parse(value[0].ToString());
                    if (result == -1)
                    {
                        SDLuaConvertExtensions.Instance.EX_SaveLogFile();
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }

            return true;

        }

        public StateDiagram OpStateDiagram
        {
            set
            {
                m_OpStateDiagram = value;
            }
            get
            {
                return m_OpStateDiagram;
            }
        }


        public static SDLuaConvertScript Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new SDLuaConvertScript();
                }

                return m_Instance;
            }
        }

        public bool Used
        {
            get
            {
                return m_LuaVM != null;
            }
        }

        public void Lock()
{
    if (m_uiLockCount == 0)
    {
        Monitor.Enter(this);
    }
    m_uiLockCount++;
}
//---------------------------------------------------------------------------
public void Unlock()
{
    m_uiLockCount--;
    if (m_uiLockCount == 0)
    {
        Monitor.Exit(this);
    }
}

        private uint m_uiLockCount = 0;

        private static Lua m_LuaVM = null;      // Lua 虚拟机
        private StateDiagram m_OpStateDiagram;           // 当前被操作的状态图 id

        //private Mission m_Mission = null;       // 目标任务
        //private LuaTable m_MissionTable;        // 目标任务转换成的 LuaTable

        // 构造函数
        private SDLuaConvertScript(){}
        private static SDLuaConvertScript m_Instance = null;    // 唯一实例

    }
}
