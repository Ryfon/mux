﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TextureTools;

namespace MissionEditor
{
    // 场景草图类
    public class SceneSketch
    {
        public SceneSketch(int iSceneID, int iWidth, int iHeight)
        {
            m_iSceneID = iSceneID;
            m_iWidth = iWidth;
            m_iHeight = iHeight;
            m_bShowBlock = false;
        }

        // 共有属性
        public int SceneID
        {
            get
            {
                return m_iSceneID;
            }
        }

        public String SceneName
        {
            get
            {
                return m_strSceneName;
            }
            set
            {
                m_strSceneName = value;
            }
        }

        public int SceneWidth
        {
            get
            {
                return m_iWidth;
            }
        }

        public int SceneHeight
        {
            get
            {
                return m_iHeight;
            }
        }

        public Image PureSketchImage
        {
            get
            {
                return SketchImage;
            }
        }

        public Image PureBlockImage
        {
            get
            {
                return m_BlockImage;
            }
        }

        public Image PurePathImage
        {
            get
            {
                return m_PathImage;
            }
        }

        public Image SketchImage
        {
            get
            {
                if (m_bShowBlock)
                {
                    TextureMerger.Instance.ClearBitMaps();
                    TextureMerger.Instance.AddBitMap(m_SketchImage, 1.0f);
                    TextureMerger.Instance.AddBitMap(m_BlockImage, 0.3f);
                    Bitmap bmp = new Bitmap(1024, 1024);
                    TextureMerger.Instance.Render(bmp);
                    //TextureMerger.Instance.ClearBitMaps();
                    //TextureMerger.Instance.Dispose();
                    return bmp;
                    //return m_BlockImage;
                }
                else
                {
                    return m_SketchImage;
                }
            }
        }

        public Image PathImage
        {
            get
            {
                if(m_PathImage == null)
                {
                    return null;
                }

                TextureMerger.Instance.ClearBitMaps();
                TextureMerger.Instance.AddBitMap(m_SketchImage, 1.0f);
                TextureMerger.Instance.AddBitMap(m_PathImage, 0.3f);
                Bitmap bmp = new Bitmap(1024, 1024);
                TextureMerger.Instance.Render(bmp);
                //TextureMerger.Instance.ClearBitMaps();
                //TextureMerger.Instance.Dispose();
                return bmp;
                //return m_PathImage;
            }
        }

        public bool ShowBlock
        {
            get
            {
                return m_bShowBlock;
            }
            set
            {
                m_bShowBlock = value;
            }
        }

        public bool ShowPath
        {
            get
            {
                return m_bShowPath;
            }
            set
            {
                m_bShowPath = value;
            }
        }

        public int TotalBlockNum
        {
            get
            {
                return m_iTotalBlockNum;
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // 加载场景草图
        public void LoadSceneImage(String strFileName)
        {
            Bitmap bmp = new Bitmap(strFileName);
            m_SketchImage = bmp;
        }

        // 改变颜色
        public void ChangeColor(int iPaintType, Color color, float fBlockPercent)
        {
            Bitmap srcImage = m_BlockImage.Clone() as Bitmap;
            Bitmap destImage = m_PathImage.Clone() as Bitmap;


            float fScale = 1024.0f / System.Math.Max(m_iGridX, m_iGridY);
            int iScale = (int)(fScale + 0.5f);
            for (int iIndexY = 0; iIndexY < m_iGridY; iIndexY++)
            {
                for (int iIndexX = 0; iIndexX < m_iGridX; iIndexX++)
                {
                    if (GetBlockInfo(iIndexX, iIndexY) != 1)
                    {
                        for (int i = iScale * iIndexX; i < iScale * iIndexX + iScale; i++)
                        {
                            for (int j = iScale * iIndexY; j < iScale * iIndexY + iScale; j++)
                            {
                                if (GetWayInfo(iIndexX, iIndexY) == iPaintType)
                                {
                                    Color wayColor = color;
                                    Color destColor = AlphaBlend(srcImage.GetPixel(i, m_iGridY * iScale - 1 - j), wayColor, fBlockPercent);
                                    destImage.SetPixel(i, m_iGridY * iScale - 1 - j, destColor);
                                }
                            }
                        }
                    }

                }
            }

            // 设置way
            m_PathImage = destImage;

            MainForm.Instance.RefreshSceneSketch(m_iSceneID);
        }

        public void RefreshPathImage(int iX, int iY, int iRadius, int iPaintType, float fBlockPercent, PictureBox pictureBoxSceneSketch)
        {
            //Bitmap srcImage = m_BlockImage.Clone() as Bitmap;
            //Bitmap destImage = m_PathImage.Clone() as Bitmap;
            Bitmap srcImage = pictureBoxSceneSketch.Image.Clone() as Bitmap;
            Bitmap destImage = pictureBoxSceneSketch.Image.Clone() as Bitmap;
            Bitmap pathDestImage = m_PathImage.Clone() as Bitmap;

            float fScale = 1024.0f / System.Math.Max(m_iGridX, m_iGridY);
            int iScale = (int)(fScale + 0.5f);
            for (int iIndexY = iY - iRadius; iIndexY <= iY + iRadius; iIndexY++)
            {
                for (int iIndexX = iX - iRadius; iIndexX <= iX + iRadius; iIndexX++)
                {
                    if (GetBlockInfo(iIndexX, iIndexY) != 1)
                    {
                        for (int i = iScale * iIndexX; i < iScale * iIndexX + iScale; i++)
                        {
                            for (int j = iScale * iIndexY; j < iScale * iIndexY + iScale; j++)
                            {
                                if (iPaintType == 1 || iPaintType == 2)
                                {
                                    Color wayColor = PathPaintForm.Instance.SmallColor;
                                    if (iPaintType == 2)
                                    {
                                        wayColor = PathPaintForm.Instance.BigColor;
                                    }
                                    Color destColor = AlphaBlend(srcImage.GetPixel(i, m_iGridY * iScale - 1 - j), wayColor, fBlockPercent);
                                    destImage.SetPixel(i, m_iGridY * iScale - 1 - j, wayColor);
                                    pathDestImage.SetPixel(i, m_iGridY * iScale - 1 - j, wayColor);
                                }

                                if (iPaintType == 0)
                                {
                                    Color srcColor = srcImage.GetPixel(i, m_iGridY * iScale - 1 - j);
                                    Color destColor = srcColor;
                                    int iWayInfo = GetWayInfo(iIndexX, iIndexY);
                                    if(iWayInfo == 0)
                                    {
                                        destColor = srcColor;
                                    }
                                    else if(iWayInfo == 1)
                                    {
                                        destColor = ColorRestore(srcColor, PathPaintForm.Instance.SmallColor, fBlockPercent);
                                    }
                                    else if(iWayInfo == 2)
                                    {
                                        destColor = ColorRestore(srcColor, PathPaintForm.Instance.BigColor, fBlockPercent);
                                    }
                                    destImage.SetPixel(i, m_iGridY * iScale - 1 - j, destColor);
                                    pathDestImage.SetPixel(i, m_iGridY * iScale - 1 - j, Color.Black);
                                }
                            }
                        }
                    }

                }
            }

            // 设置way
            m_PathImage = pathDestImage;
            pictureBoxSceneSketch.Image = destImage;
            
            //MainForm.Instance.RefreshSceneSketchImage(m_iSceneID);
        }

        public void LoadWayInfo(String strFileName, Color SmallColor, Color BigColor, float fBlockPercent)
        {
            Bitmap srcImage = m_BlockImage.Clone() as Bitmap;
            Bitmap destImage = m_BlockImage.Clone() as Bitmap;
            //Bitmap destImage = new Bitmap(m_BlockImage.Width, m_BlockImage.Height);

            // 生成带path信息的草图
            if (File.Exists(strFileName))
            {
                m_PathImage = m_BlockImage.Clone() as Image;

                BinaryReader br = new BinaryReader(File.Open(strFileName, FileMode.Open, FileAccess.Read));
                br.BaseStream.Seek(0, SeekOrigin.Begin);

                try
                {
                    // 读取文件头信息
                    int iMapID = br.ReadInt32();
                    int iWidth = br.ReadInt32();
                    int iHeight = br.ReadInt32();

                    // 大小不匹配， 直接返回
                    if (m_iWidth != m_iGridX || m_iHeight != m_iGridY)
                    {
                        return;
                    }

                    // 清空旧的数据
                    //int num = m_WayInfoList.Count;
                    //int k = 0;
                    //while(k < num)
                    //{
                    //    m_WayInfoList[0] = 0;
                    //    k++;
                    //}

                    // 读取路径权重信息
                    float fScale = 1024.0f / System.Math.Max(m_iGridX, m_iGridY);
                    int iScale = (int)(fScale + 0.5f);
                    for (int iIndexY = 0; iIndexY < m_iGridY; iIndexY++)
                    {
                        for (int iIndexX = 0; iIndexX < m_iGridX; iIndexX++)
                        {
                            int iWay = br.ReadInt32();
                            int iOrgInfo = (int)m_WayInfoList[iIndexX + iIndexY * m_iGridX];

                            // 不是碰撞的地方，才能刷路径
                            // 碰撞信息，完全由.map文件决定，way文件不能更新碰撞信息
                            if(iOrgInfo != 3 &&　iWay != 3)
                            {
                                m_WayInfoList[iIndexX + iIndexY * m_iGridX] = iWay;

                                if (iWay == 1 || iWay == 2)
                                {
                                    for (int i = iScale * iIndexX; i < iScale * iIndexX + iScale; i++)
                                    {
                                        for (int j = iScale * iIndexY; j < iScale * iIndexY + iScale; j++)
                                        {
                                            Color wayColor = SmallColor;
                                            if (iWay == 2)
                                            {
                                                wayColor = BigColor;
                                            }
                                            //Color destColor = AlphaBlend(srcImage.GetPixel(i, m_iGridY * iScale - 1 - j), wayColor, fBlockPercent);
                                            //destImage.SetPixel(i, m_iGridY * iScale - 1 - j, destColor);
                                            destImage.SetPixel(i, m_iGridY * iScale - 1 - j, wayColor);
                                        }
                                    }
                                }
                            }
              
                        }
                    }
                }
                catch(Exception /*FileExp*/)
                {
                    MessageBox.Show(strFileName +"文件错误，无法加载路径权重信息。");
                    br.Close();
                    return;
                }
                finally
                {
                    br.Close();
                }

                // 设置way
                m_PathImage = destImage;
            }
            else
            {
                m_PathImage = destImage;
            }
        }

        // 加载场景碰撞信息
        public void LoadBlockInfo(String strFileName, Color blockColor, float fBlockPercent)
        {
            Bitmap srcImage = m_SketchImage.Clone() as Bitmap;
            //Bitmap destImage = m_SketchImage.Clone() as Bitmap;
            Bitmap destImage = new Bitmap(srcImage.Width, srcImage.Height);

            if (File.Exists(strFileName))
            {
                BinaryReader br = new BinaryReader(File.Open(strFileName, FileMode.Open, FileAccess.Read));
                br.BaseStream.Seek(0, SeekOrigin.Begin);

                try
                {
                    // 读取文件信息
                    m_iVerson = br.ReadInt32();
                    m_iMapID = br.ReadInt32();
                    m_iTotalX = br.ReadInt32();
                    m_iTotalY = br.ReadInt32();
                    m_iGridX = br.ReadInt32();
                    m_iGridY = br.ReadInt32();

                    // 大小不匹配， 直接返回
                    if (m_iWidth != m_iGridX || m_iHeight != m_iGridY)
                    {
                        m_BlockImage = m_SketchImage;
                        return;
                    }

                    // 初始化碰撞信息
                    if (m_PropertyList == null)
                    {
                        m_PropertyList = new ArrayList();
                    }
                    else
                    {
                        m_PropertyList.Clear();
                    }

                    // 写入block信息
                    float fScale = 1024.0f / System.Math.Max(m_iGridX, m_iGridY);
                    int iScale = (int)(fScale + 0.5f);
                    m_iTotalBlockNum = 0;
                    for (int iIndexY = 0; iIndexY < m_iGridY; iIndexY++)
                    {
                        for (int iIndexX = 0; iIndexX < m_iGridX; iIndexX++)
                        {
                            int iBlock = br.ReadInt32();
                            m_PropertyList.Add(iBlock);
                            m_PrePathInfoList.Add(0);

                            if (/*iBlock % 2*/ GetInt32FromInt32ByBit(iBlock, 0, 4) == 1)
                            {
                                m_WayInfoList.Add(3);

                                m_iTotalBlockNum++;

                                for (int i = iScale * iIndexX; i < iScale * iIndexX + iScale; i++)
                                {
                                    for (int j = iScale * iIndexY; j < iScale * iIndexY + iScale; j++)
                                    {
                                        //Color destColor = AlphaBlend(srcImage.GetPixel(i, m_iGridY * iScale - 1 - j), blockColor, fBlockPercent);
                                        //destImage.SetPixel(i, m_iGridY * iScale - 1 - j, destColor);
                                        destImage.SetPixel(i, m_iGridY * iScale - 1 - j, blockColor);
                                    }
                                }
                            }
                            else
                            {
                                m_WayInfoList.Add(0);
                            }
                        }
                    }
                }
                catch (EndOfStreamException)
                {

                }
                finally
                {
                    br.Close();
                }

                // 设置碰撞image
                m_BlockImage = destImage;
            }
            else
            {
                m_BlockImage = destImage;
            }
        }

        // 写路径权重信息
        public void SaveWayInfo(String strFileName)
        {
            // 动态加载，必须动态保存
            if (m_PathImage == null)
            {
                return;
            }

            // 取消只读属性
            if (File.Exists(strFileName))
            {
                File.SetAttributes(strFileName, FileAttributes.Normal);
            }

            // 创建一个二进制文件
            BinaryWriter br = new BinaryWriter(File.Open(strFileName, FileMode.Create, FileAccess.Write));

            // 写入文件头信息
            br.Write(m_iMapID);
            br.Write(m_iGridX);
            br.Write(m_iGridY);

            // 写入格子信息
            foreach (int WayInfo in m_WayInfoList)
            {
                br.Write(WayInfo);
            }

            // 清空写缓存，关闭文件
            br.Flush();
            br.Close();
        }

        // 写地图格子信息
        public void SaveGridInfo(String strFileName)
        {
            // 设置格子信息
            SetGridInfo();

            // 取消只读属性
            if(File.Exists(strFileName))
            {
                File.SetAttributes(strFileName, FileAttributes.Normal);
            }

            // 创建一个二进制文件
            BinaryWriter br = new BinaryWriter(File.Open(strFileName, FileMode.Create, FileAccess.Write));

            // 写入文件头信息
            br.Write(m_iVerson);
            br.Write(m_iMapID);
            br.Write(m_iTotalX);
            br.Write(m_iTotalY);
            br.Write(m_iGridX);
            br.Write(m_iGridY);

            // 写入格子信息
            foreach(int GridInfo in m_PropertyList)
            {
                br.Write(GridInfo);
            }

            // 写入扩展结构
            // 扩展结构的数量
            br.Write(m_ZoneList.Count);
            // 遍历写入zone信息
            foreach(RegionZone rz in m_ZoneList)
            {
                // 写入zone的ID
                br.Write(rz.ID);

                // zone中，所包含的group数量
                br.Write(rz.GroupNum());

                // 依次写入group
                foreach(int groupID in rz.RegionGroupList)
                {
                    br.Write(groupID);
                }
            }

            // 清空写缓存，关闭文件
            br.Flush();
            br.Close();
        }

        // 写地图格子信息
        public void SavePrePathInfo(String strFileName)
        {
            // 设置格子信息
            SetGridInfo();

            // 取消只读属性
            if (File.Exists(strFileName))
            {
                File.SetAttributes(strFileName, FileAttributes.Normal);
            }

            // 创建一个二进制文件
            BinaryWriter br = new BinaryWriter(File.Open(strFileName, FileMode.Create, FileAccess.Write));

            // 写入文件头信息
            br.Write(m_iVerson);
            br.Write(m_iMapID);
            br.Write(m_iTotalX);
            br.Write(m_iTotalY);
            br.Write(m_iGridX);
            br.Write(m_iGridY);

            // 写入格子信息
            foreach (int GridInfo in m_PrePathInfoList)
            {
                br.Write(GridInfo);
            }

            // 清空写缓存，关闭文件
            br.Flush();
            br.Close();
        }

        // 获取路径权重信息
        public int GetWayInfo(int iX, int iY)
        {
            if (m_WayInfoList == null)
            {
                return 0;
            }

            int iIndex = iX + iY * m_iGridX;
            if (iIndex >= m_WayInfoList.Count || iIndex < 0)
            {
                return 0;
            }
            else
            {
                int result = (int)m_WayInfoList[iIndex];
                return result;
            } 
        }

        // 设置路径权重信息
        public bool SetWayInfo(int iX, int iY, int iRadius, int iPaintType)
        {
            if (m_WayInfoList == null)
            {
                return false;
            }

            int iIndex = iX + iY * m_iGridX;
            if (iIndex >= m_WayInfoList.Count || iIndex < 0)
            {
                return false;
            }
            else
            {
                for(int iCurrX = iX - iRadius;iCurrX <= iX + iRadius;iCurrX++)
                {
                    for (int iCurrY = iY - iRadius; iCurrY <= iY + iRadius; iCurrY++)
                    {
                        if (GetBlockInfo(iCurrX, iCurrY) != 1)
                        {
                            int iCurrIndex = iCurrX + iCurrY * m_iGridX;
                            m_WayInfoList[iCurrIndex] = iPaintType;                            
                        }
                    }
                }
                return true;
            }
        }

        // 获取地图碰撞信息
        public int GetBlockInfo(int iX, int iY)
        {
            if (m_PropertyList == null)
            {
                return 0;
            }

            int iIndex = iX + iY * m_iGridX;
            if (iIndex >= m_PropertyList.Count || iIndex < 0)
            {
                return 0;
            }
            else
            {
                //int result = (int)m_PropertyList[iIndex] % 2;
                //return result;
                int result = (int)m_PropertyList[iIndex];
                return GetInt32FromInt32ByBit(result, 0, 4);
            }
        }

        // 获取地图格子信息
        public int GetGridInfo(int iX, int iY)
        {
            if (m_PropertyList == null)
            {
                return 0;
            }

            int iIndex = iX + iY * m_iGridX;
            if (iIndex >= m_PropertyList.Count || iIndex < 0)
            {
                return 0;
            }
            else
            {
                int result = (int)m_PropertyList[iIndex];
                return result;
            }
        }

        // alpha混合的辅助函数
        private Color AlphaBlend(Color srcColor, Color blendColor, float alpha)
        {
            int cA = srcColor.A;
            int cR = (int)(blendColor.R * alpha) + (int)((1.0f - alpha) * srcColor.R);
            int cG = (int)(blendColor.G * alpha) + (int)((1.0f - alpha) * srcColor.G);
            int cB = (int)(blendColor.B * alpha) + (int)((1.0f - alpha) * srcColor.B);
            Color destColor = Color.FromArgb(cA, cR, cG, cB);
            return destColor;
        }

        // 色彩还原函数
        private Color ColorRestore(Color destColor, Color blendColor, float alpha)
        {
            int cA = destColor.A;
            int cR = (int)((float)(destColor.R - (int)(blendColor.R * alpha)) / (1.0 - alpha));
            int cG = (int)((float)(destColor.G - (int)(blendColor.G * alpha)) / (1.0 - alpha));
            int cB = (int)((float)(destColor.B - (int)(blendColor.B * alpha)) / (1.0 - alpha));
            cR = Math.Max(0, cR);
            cR = Math.Min(cR, 255);
            cG = Math.Max(0, cG);
            cG = Math.Min(cG, 255);
            cB = Math.Max(0, cB);
            cB = Math.Min(cB, 255);
            Color srcColor = Color.FromArgb(cA, cR, cG, cB);
            return srcColor;
        }

        // 将地形的信息，写入grid信息中
        public void SetGridInfo()
        {
            // 获取当前地图的所有区域
            Hashtable table = RegionManager.Instance.MapID2RegionList;
            ArrayList regionList = table[m_iSceneID] as ArrayList;

            // 两个临时的list, 用于将客户端区域和服务器区域分开
            ArrayList ServerRegionList = new ArrayList();
            ArrayList ClientRegionList = new ArrayList();

            // 加入了寻路预判区域
            ArrayList PrePathRegionList = new ArrayList();

            // 遍历所有区域
            if (regionList != null)
            {
                foreach (RegionInfo info in regionList)
                {
                    // 获取当前区域所属的区域组
                    RegionGroup group = RegionManager.Instance.GetGroupByID(info.GroupID);
                    if (group != null)
                    {
                        // 是服务器区域
                        if (group.BigType == RegionGroup.RegionType.服务器相关区域)
                        {
                            if(group.ServerType == RegionGroup.ServerRegionType.寻路预判区)
                            {
                                PrePathRegionList.Add(info);
                            }
                            else if (group.ServerType == RegionGroup.ServerRegionType.BlockQuad区)
                            {

                            }
                            else
                            {
                                ServerRegionList.Add(info);
                            }
                        }

                        // 是客户端区域
                        if (group.BigType == RegionGroup.RegionType.客户端表现区域)
                        {
                            ClientRegionList.Add(info);
                        }
                    }
                }
            }

            // 写入服务器区域格子信息
            SetServerGridInfo(ServerRegionList);

            // 写入客户端区域格子信息
            SetClientGridInfo(ClientRegionList);

            // 写入寻路预判区域格子信息
            SetPrePathGridInfo(PrePathRegionList);

            // 写入路径权重信息
            SetWayGridInfo();
        }

        private void SetServerGridInfo(ArrayList regionList)
        {
            // 清空原先的数据，32位的高10位是服务器区域信息
            for (int iIndex = 0; iIndex < m_PropertyList.Count; ++iIndex)
            {
                int source = (int)m_PropertyList[iIndex];
                m_PropertyList[iIndex] = SetBitForInt32(source, 22, 10, false);
            }

            // 遍历所有的区域，并写入信息
            foreach (RegionInfo region in regionList)
            {
                // 获取当前区域所属的区域组
                RegionGroup group = RegionManager.Instance.GetGroupByID(region.GroupID);
                if (group != null)
                {
                    // 计算区域所包含的点
                    region.CalContainPoint();

                    // 将GroupID转化为7位二进制，方便读写
                    int flowID = int.Parse(group.FlowID);
                    BitArray groupID = GetBitFromInt32(flowID, 7);

                    // 获取区域的三种属性
                    bool bIsDuplicate = group.IsDuplicate;
                    bool bIsBattle = group.IsBattle;
                    bool bIsPK = group.IsPK;

                    // 开始遍历区域中所包含的点，并将信息写入
                    foreach (Point p in region.ContainPointList)
                    {
                        // 获取x和y的值，并计算出索引
                        int iX = p.X;
                        int iY = p.Y;
                        int iIndex = iX + iY * m_iGridX;

                        // 获取当前格子的信息
                        int iGridInfo = (int)m_PropertyList[iIndex];

                        /*
                        // 设置7位ID信息
                        iGridInfo = SetBitForInt32(iGridInfo, 25, groupID);

                        // 设置3位区域属性信息
                        iGridInfo = SetBitForInt32(iGridInfo, 22, 1, bIsDuplicate);
                        iGridInfo = SetBitForInt32(iGridInfo, 23, 1, bIsBattle);
                        iGridInfo = SetBitForInt32(iGridInfo, 24, 1, bIsPK);
                        */

                        //按区域的类型写入格子信息
                        // 任务区域，写入区域的7位ID信息
                        if (group.ServerType == RegionGroup.ServerRegionType.任务触发区域)
                        {
                            iGridInfo = SetBitForInt32(iGridInfo, 25, groupID);
                        }

                        // 安全区，将23，24位设置为true
                        if (group.ServerType == RegionGroup.ServerRegionType.安全区)
                        {
                            iGridInfo = SetBitForInt32(iGridInfo, 23, 1, true);
                            iGridInfo = SetBitForInt32(iGridInfo, 24, 1, true);
                        }

                        // 不可PK区，将24位设置为true
                        if (group.ServerType == RegionGroup.ServerRegionType.不可PK区)
                        {
                            iGridInfo = SetBitForInt32(iGridInfo, 24, 1, true);
                        }

                        // 不可骑乘区，将22位设置为true
                        if (group.ServerType == RegionGroup.ServerRegionType.不可骑乘区)
                        {
                            iGridInfo = SetBitForInt32(iGridInfo, 22, 1, true);
                        }

                        // 将格子信息写回
                        m_PropertyList[iIndex] = iGridInfo;
                    }
                }
            }
        }

        private void SetClientGridInfo(ArrayList regionList)
        {
            // 清空原先的数据，32位的从第5位开始的9位是客户端区域信息
            for (int iIndex = 0; iIndex < m_PropertyList.Count; ++iIndex)
            {
                int source = (int)m_PropertyList[iIndex];
                m_PropertyList[iIndex] = SetBitForInt32(source, 4, 9, false);
            }

            // 清空原先的zone列表
            m_ZoneList.Clear();

            // 初始化一张zone列表
            for (int iZoneID = 0; iZoneID < m_PropertyList.Count;iZoneID++ )
            {
                RegionZone rz = new RegionZone(iZoneID);
                m_ZoneList.Add(rz);
            }

            // 遍历所有的区域，并写入信息到zone列表
            foreach (RegionInfo region in regionList)
            {
                // 获取当前区域所属的区域组
                RegionGroup group = RegionManager.Instance.GetGroupByID(region.GroupID);
                if (group != null)
                {
                    // 获得group的OutID
                    int groupOutID = int.Parse(group.OutPutID);

                    // 计算区域所包含的点
                    region.CalContainPoint();

                    // 开始遍历区域中所包含的点，并将信息写入
                    foreach (Point p in region.ContainPointList)
                    {
                        // 获取x和y的值，并计算出索引
                        int iX = p.X;
                        int iY = p.Y;
                        int iIndex = iX + iY * m_iGridX;

                        RegionZone RZ = m_ZoneList[iIndex] as RegionZone;
                        
                        RZ.AddGroup(groupOutID);
                    }
                }
            }

            // 对zone列表进行合并
            ArrayList newZoneList = new ArrayList();
            for (int rzIndex = 0; rzIndex < m_PropertyList.Count;rzIndex++ )
            {
                // 获取当前的区域
                RegionZone CurrRZ = m_ZoneList[rzIndex] as RegionZone;

                // 空的区域，直接关闭
                if(CurrRZ.Empty())
                {
                    CurrRZ.Active = false;
                }
                else
                {
                    // 遍历之前的zone寻找可以合并的zone
                    //for (int preIndex = rzIndex - 1; preIndex >= 0; preIndex--)
                    //{
                    //    RegionZone preRZ = m_ZoneList[preIndex] as RegionZone;

                    //    if(preRZ.Active)
                    //    {
                    //        if(CurrRZ.Same(preRZ))
                    //        {
                    //            CurrRZ.ID = preRZ.ID;
                    //            CurrRZ.Active = false;
                    //        }
                    //    }
                    //}
                    if(newZoneList.Count == 0)
                    {
                        CurrRZ.Active = true;
                        newZoneList.Add(CurrRZ);
                    }
                    else
                    {
                        foreach (RegionZone activeZone in newZoneList)
                        {
                            if (CurrRZ.Same(activeZone))
                            {
                                CurrRZ.ID = activeZone.ID;
                                CurrRZ.Active = false;
                                break;
                            }
                        }

                        if(CurrRZ.Active)
                        {
                            newZoneList.Add(CurrRZ);
                        }
                    }
                }
            }

            // 为所有active的zone进行编号从1开始
            int iSaveZoneID = 1;
            foreach(RegionZone arz in m_ZoneList)
            {
                if(arz.Active)
                {
                    arz.ID = iSaveZoneID;
                    iSaveZoneID++;
                }
            }

            // 写入格子信息
            for (int j = 0;j < m_PropertyList.Count ;j++ )
            {
                RegionZone z = m_ZoneList[j] as RegionZone;

                if(!z.Empty())
                {
                    // 获取对应的active的zone
                    int activeID = z.ID;
                    RegionZone az = m_ZoneList[activeID] as RegionZone;
                    int saveID = az.ID;

                    // 转化为9位的ID
                    BitArray SaveZoneID = GetBitFromInt32(saveID, 9);

                    // 获取当前格子的信息
                    int iGridInfo = (int)m_PropertyList[j];

                    // 设置9位ID信息
                    iGridInfo = SetBitForInt32(iGridInfo, 4, SaveZoneID);

                    // 将格子信息写回
                    m_PropertyList[j] = iGridInfo;
                }
            }

            // 清理zone列表，只保留active的zone，释放内存
            //ArrayList newZoneList = new ArrayList();
            //foreach(RegionZone rz in m_ZoneList)
            //{
            //    if(rz.Active)
            //    {
            //        newZoneList.Add(rz);
            //    }
            //}
            m_ZoneList = newZoneList;
        }

        private void SetPrePathGridInfo(ArrayList regionList)
        {
            // 清空原先的数据
            for (int iIndex = 0; iIndex < m_PropertyList.Count; ++iIndex)
            {
                int source = (int)m_PrePathInfoList[iIndex];
                m_PrePathInfoList[iIndex] = SetBitForInt32(source, 0, 32, false);
            }

            // 遍历所有的区域，并写入信息
            int iRegionIndex = 0;// 区域的索引，限定为0~31
            foreach (RegionInfo region in regionList)
            {
                // 获取当前区域所属的区域组
                RegionGroup group = RegionManager.Instance.GetGroupByID(region.GroupID);
                if (group != null)
                {
                    // 计算区域所包含的点
                    region.CalContainPoint();

                    // 开始遍历区域中所包含的点，并将信息写入
                    foreach (Point p in region.ContainPointList)
                    {
                        // 获取x和y的值，并计算出索引
                        int iX = p.X;
                        int iY = p.Y;
                        int iIndex = iX + iY * m_iGridX;

                        // 获取当前格子的信息
                        int iGridInfo = (int)m_PrePathInfoList[iIndex];

                        // 在相应的index位置写入
                        if(iRegionIndex < 32)
                        {
                            iGridInfo = SetBitForInt32(iGridInfo, iRegionIndex, 1, true);
                        }

                        // 将格子信息写回
                        m_PrePathInfoList[iIndex] = iGridInfo;
                    }
                }

                // 索引自增
                ++iRegionIndex;
            }
        }

        // 设置路径权重信息
        private void SetWayGridInfo()
        {
            // 32位的第20，21位，是路径权重信息
            for (int iIndex = 0; iIndex < m_PropertyList.Count; ++iIndex)
            {
                int source = (int)m_PropertyList[iIndex];
                int iWay = (int)m_WayInfoList[iIndex];
                BitArray bitWay = GetBitFromInt32(iWay, 2);
                m_PropertyList[iIndex] = SetBitForInt32(source, 20, bitWay);
            }
        }

        // 辅助函数，用于设置每一个位的值
        private int SetBitForInt32(int source, int index, int length, bool value)
        {
            // int到bit的转换
            int[] ia = { source };
            BitArray ba = new BitArray(ia);

            // 开始设置
            for (int j = 0; j < length; j++)
            {
                ba.Set(index + j, value);
            }

            // bit到int的转换
            int[] result = new int[1];
            ba.CopyTo(result, 0);

            return result[0];
        }

        // 辅助函数，通过一个BitArray来设置
        private int SetBitForInt32(int source, int index, BitArray value)
        {
            // int到bit的转换
            int[] ia = { source };
            BitArray ba = new BitArray(ia);

            // 开始设置
            for (int j = 0; j < value.Count; j++)
            {
                ba.Set(index + j, value.Get(j));
            }

            // bit到int的转换
            int[] result = new int[1];
            ba.CopyTo(result, 0);

            return result[0];
        }

        // 辅助函数，将一个int， 转化为一个BitArray，参数length制定了BitArray的长度，多余的位将被忽略
        private BitArray GetBitFromInt32(int source, int length)
        {
            // 初始化一个length长度的BitArray
            BitArray result = new BitArray(length);

            // 将source做int到bit的转换
            int[] ia = { source };
            BitArray ba = new BitArray(ia);

            // 设置result的值
            for (int j = 0; j < length; ++j)
            {
                result.Set(j, ba.Get(j));
            }

            // 返回result
            return result;
        }

        // 辅助函数，将一个int中的某几位取出，转化为int后再返回
        private int GetInt32FromInt32ByBit(int source, int index, int length)
        {
            // int到bit的转换
            int[] ia = { source };
            BitArray Tempba = new BitArray(ia);
            BitArray ba = new BitArray(length);

            // 开始设置
            for (int j = 0; j < length; j++)
            {
                ba.Set(j, Tempba.Get(index + j));
            }

            // bit到int的转换
            int[] result = new int[1];
            ba.CopyTo(result, 0);

            return result[0];
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // 成员变量
        private int m_iSceneID; // 场景编号
        private String m_strSceneName = ""; // 场景名称
        private int m_iWidth;   // 场景宽度
        private int m_iHeight;  // 场景高度
        private Image m_SketchImage = null;    // 场景缩略草图
        private Image m_BlockImage = null;     // 带碰撞信息的草图
        private Image m_PathImage = null;       // 带路径权重信息的草图
        private bool m_bShowBlock = false;      // 是否显示碰撞信息
        private bool m_bShowPath = false;       // 是否显示路径权重信息
        private ArrayList m_PropertyList = new ArrayList();  // 地表每个 grid 属性
        private ArrayList m_WayInfoList = new ArrayList();  // 路径权重值
        private ArrayList m_PrePathInfoList = new ArrayList();  // 寻路预判信息
        private ArrayList m_ZoneList = new ArrayList();    // 区域zone列表
        private int m_iTotalBlockNum = 0;// 统计Block点的个数
        //---------------------------//
        //文件头信息,保存后用于写文件//
        private int m_iVerson = 0;
        private int m_iMapID = 0;
        private int m_iTotalX = 0;
        private int m_iTotalY = 0;
        private int m_iGridX = 0;
        private int m_iGridY = 0;
        //---------------------------//
        //private String m_strImageFileName;  // 地图草图文件名称 场景草图文件名称
    }

    // 区域划分类
    public class RegionZone
    {
        public RegionZone(int id)
        {
            m_iZoneID = id;
        }

        public void AddGroup(int groupID)
        {
            if(!m_RegionGroupList.Contains(groupID))
            {
                m_RegionGroupList.Add(groupID);
            }
        }

        public bool Empty()
        {
            return (m_RegionGroupList.Count == 0);
        }

        public bool Same(RegionZone rz)
        {
            // 包含区域的数量不同，直接返回不相等
            if(m_RegionGroupList.Count != rz.RegionGroupList.Count)
            {
                return false;
            }

            // 判断是否相等
            foreach(int groupID in m_RegionGroupList)
            {
                if(!rz.RegionGroupList.Contains(groupID))
                {
                    return false;
                }
            }

            return true;
        }

        public int GroupNum()
        {
            return m_RegionGroupList.Count;
        }
        //////////////////////////////////////////////////////////////////////////
        public int ID
        {
            get
            {
                return m_iZoneID;
            }
            set
            {
                m_iZoneID = value;
            }
        }

        public bool Active
        {
            get
            {
                return m_bActive;
            }
            set
            {
                m_bActive = value;
            }
        }

        public ArrayList RegionGroupList
        {
            get
            {
                return m_RegionGroupList;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // 成员变量
        private ArrayList m_RegionGroupList = new ArrayList();  // 用于存放该zone所包含的区域组
        private bool m_bActive = true;  // 标识该zone是否为激活状态
        private int m_iZoneID;  // 该zone的ID
    };
}
