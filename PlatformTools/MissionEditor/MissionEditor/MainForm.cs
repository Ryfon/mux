﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
//.NET MICROSOFT.DIRECTX版本是 1.0.2902.0
namespace MissionEditor
{
    public partial class MainForm : Form
    {

        public MainForm()
        {
            InitializeComponent();
            m_CanvasForm = m_splitContainerState.Panel2;
            ms_Instance = this;
        }

        #region User32 Win32API

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool SetCursorPos(int X, int Y);

        #endregion

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Data文件夹不存在，程序直接退出
            if (!System.IO.Directory.Exists(Application.StartupPath + "/Data/"))
            {
                MessageBox.Show("Data文件夹不存在，程序无法启动！");
                m_bErrorClose = true;
                Application.Exit();
                return;
            }

            // 主窗口载入的时候，做一些初始化工作
            // 初始化 manager 的基础数据

            // 去掉所有文件的只读属性
            AssertFileAttribute(Application.StartupPath + "/Data/");

            OptionManager.LoadFromXml(OptionManager.OptionPath);

            m_MissionManager.LoadMissions(OptionManager.MissionPath);
            //原来它是读取 NPC和Mob表的合成表，现在直接读取2个表，不会覆盖掉之前的表
            //m_NPCManager.LoadNPCInfoFromXml(OptionManager.NPCInfoPath);
            m_NPCManager.LoadNPCInfoFromXml("Data/npc_info_m.xml", false);
            m_NPCManager.LoadNPCInfoFromXml("Data/npc_info_n.xml", false);

            m_NPCManager.LoadNPCCreatorInfoFromXml(OptionManager.NPCCreatorInfoPath);

            m_RegionManager.LoadFromXML(OptionManager.RegionInfoPath);
            m_PathManager.LoadFromDirectory(OptionManager.PathInfoPath);
            m_ItemManager.LoadItemInfoFromXml(OptionManager.ItemPath);
            //任务条件
            MissionConditionManager.Instance.LoadFromXml(OptionManager.MissionBaseDataPath);
            //任务补偿
            MissionAwardManager.Instance.LoadFromXml(OptionManager.MissionBaseDataPath);
            //行为（打开商店等）
            AtomActionManager.Instance.LoadFromXml(OptionManager.MissionBaseDataPath);

            DialogManager.Instance.LoadFromXml(OptionManager.StringResourcePath);
            SceneSketchManager.Instance.LoadAllSceneSketch();
            ItemShopManager.Instance.LoadFromXml(OptionManager.ItemShopPath);
            SkillShopManager.Instance.LoadFromXml(OptionManager.SkillShopPath);
            SkillManager.Instance.LoadFromXml(OptionManager.SkillPath);

            // 先载入所有状态图到 manager
            StateDiagramManager.Instance.LoadAllStateDiagram();

            // 加载脚本
            SDLuaConvertScript.Instance.Load(OptionManager.StateDiagramConvScriptPath + "/sd_convertor.lua");

            // 填充任务类型
            m_comboBoxType.Items.Clear();
            String[] missionTypes = System.Enum.GetNames(typeof(OptionManager.MissionType));
            foreach (String strMissionType in missionTypes)
            {
                m_comboBoxType.Items.Add(strMissionType);
            }

            // 填充任务阶级
            m_comboBoxClass.Items.Clear();
            foreach (int iValue in m_MissionManager.MissionClasses.Keys)
            {
                m_comboBoxClass.Items.Add(iValue.ToString() + "_" + m_MissionManager.MissionClasses[iValue].ToString());
            }

            // 任务奖励，条件编辑器载入
            MissionBaseDataManager.Instance.LoadFromXml(OptionManager.MissionBaseDataPath);

            // 点音源的处理
            /*
            DialogResult ptSoundResult = MessageBox.Show("是否加载点音源信息？这可能会花上一段时间。", "提示", MessageBoxButtons.YesNo);
            if (ptSoundResult == DialogResult.Yes)
            {
                PointSoundManager.Instance.LoadAll(OptionManager.PointSoundInfo);
            }*/

            // 加载手工脚本
            LuaScriptConvertor.LoadManualScript(OptionManager.ManualScriptPath);

            // 初始化菜单栏
            InitMenuItem();

            // 最后刷新 ui
            RefreshUI();
            //if (m_treeViewMission.Nodes.Count > 0)
            //{
            //    m_treeViewMission.SelectedNode = m_treeViewMission.Nodes[0];
            //}

            // 添加 新任务编辑UI
            m_TaskUIForm = new TaskUIForm(m_MissionManager);
            m_tabPageTaskUI.Controls.Add(m_TaskUIForm);

            // 分层管理后的场景草图
            LayerManager.Instance.Palatte = m_pictureBoxSceneSketch;
        }

        private void AssertFileAttribute(String strDirectory)
        {
            // 递归遍历 strDirectory 目录,确定该目录下所有文件没有只读属性的.
            String[] aFiles = System.IO.Directory.GetFiles(strDirectory);
            foreach (String strFile in aFiles)
            {
                //System.IO.FileAttributes attriFile = System.IO.File.GetAttributes(strFile);
                //if ((attriFile & System.IO.FileAttributes.ReadOnly) == System.IO.FileAttributes.ReadOnly)
                //{
                System.IO.File.SetAttributes(strFile, System.IO.FileAttributes.Normal);
                //}
            }

            // 递归遍历下一级目录
            String[] aDirectories = System.IO.Directory.GetDirectories(strDirectory);
            foreach (String strDir in aDirectories)
            {
                AssertFileAttribute(strDir);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // 有错误发生，直接关闭
            if (m_bErrorClose)
            {
                return;
            }

            // 保存对话资源
            DialogManager.Instance.SaveToXml(OptionManager.StringResourcePath);
            OptionManager.SaveToXml(OptionManager.OptionPath);

            // 保存NPC及区域
            if (m_bSceneChanged)
            {
                DialogResult result = MessageBox.Show("退出前是否保存所有的修改?", "提示", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    m_NPCManager.SaveNPCCreatorInfoToXml(OptionManager.NPCCreatorInfoPath);
                    m_RegionManager.SaveAll();
                    SceneSketchManager.Instance.SaveAll();
                    RegionManager.Instance.SaveBlockQuadRegion(OptionManager.SceneSketchPath + "BlockQuadInfo.xml");
                }
                else if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }

            // 清除TextureMerger
            TextureTools.TextureMerger.Instance.Dispose();
        }

        #region 主菜单响应函数

        private void 保存任务ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_MissionManager.SaveMissions(OptionManager.MissionPath);
        }

        private void 选项ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionsForm form = new OptionsForm(this);
            form.ShowDialog();
        }

        #endregion

        #region 任务相关函数

        private void ChangeUIByMissionType(String strMissionType)
        {
            // 任务类型

            //消灭怪物
            //收集物品
            //对话类
            //护送
            //探索
            //职业类

            // reset gui
            m_comboBoxTargetItemBegin.Enabled = false;
            m_comboBoxTargetItemBegin.SelectedIndex = -1;
            m_labelTargetItem.Enabled = false;
            m_textBoxTargetValue.Enabled = false;
            m_labelTargetValue.Enabled = false;
            m_comboBoxTargetNPC.Enabled = false;
            m_labelTargetNPC.Enabled = false;
            m_comboBoxTargetArea.Enabled = false;
            m_labelTargetArea.Enabled = false;
            m_comboBoxSubType.Enabled = false;

            m_comboBoxTargetProp.Enabled = false;
            m_labelTargetProp.Enabled = false;

            m_checkBoxShare.Enabled = true;
            m_checkBoxRepeat.Enabled = true;
            m_checkBoxGiveUp.Enabled = true;

            m_labelTargetMonster.Enabled = false;
            m_comboBoxTargetMonsterBegin.Enabled = false;

            // 目标 怪物/道具 范围
            m_comboBoxTargetMonsterBegin.Enabled = false;
            m_comboBoxTargetMonsterEnd.Enabled = false;
            m_comboBoxTargetItemBegin.Enabled = false;
            m_comboBoxTargetItemEnd.Enabled = false;

            if (strMissionType.Equals(OptionManager.MissionType.计数器类.ToString()))
            {

                m_comboBoxSubType.Enabled = true;
                m_comboBoxSubType.Items.Clear();
                String[] names = Enum.GetNames(typeof(OptionManager.MissionTypeCounterSubType));
                foreach (String strName in names)
                {
                    m_comboBoxSubType.Items.Add(strName);
                }

                m_comboBoxSubType.SelectedIndex = 0;
                m_comboBoxTargetArea.SelectedIndex = -1;
            }
            else if (strMissionType.Equals(OptionManager.MissionType.对话类.ToString()))
            {
                m_comboBoxSubType.Enabled = true;
                m_comboBoxSubType.Items.Clear();
                String[] names = Enum.GetNames(typeof(OptionManager.MissionTypeDialogSubType));
                foreach (String strName in names)
                {
                    m_comboBoxSubType.Items.Add(strName);
                }

                m_comboBoxSubType.SelectedIndex = 0;
                m_comboBoxTargetArea.SelectedIndex = -1;
            }
            else if (strMissionType.Equals(OptionManager.MissionType.护送类.ToString()))
            {
                m_comboBoxTargetNPC.Enabled = true;
                m_labelTargetNPC.Enabled = true;
                m_comboBoxTargetArea.SelectedIndex = -1;
            }
            else if (strMissionType.Equals(OptionManager.MissionType.探索类.ToString()))
            {
                m_comboBoxTargetArea.Enabled = true;
                m_labelTargetArea.Enabled = true;

                m_comboBoxTargetNPC.SelectedIndex = -1;
            }
            else if (strMissionType.Equals(OptionManager.MissionType.职业类.ToString()))
            {
                m_comboBoxTargetProp.Enabled = true;
                m_labelTargetProp.Enabled = true;
                m_textBoxTargetValue.Enabled = true;
                m_labelTargetValue.Enabled = true;
                m_comboBoxTargetArea.SelectedIndex = -1;
            }

        }

        private void m_comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 任务类型 changed
            String strSelected = m_comboBoxType.SelectedItem.ToString();
            ChangeUIByMissionType(strSelected);
        }

        private void m_comboBoxDlgMissionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_comboBoxSubType.SelectedIndex == -1)
            {
                return;
            }
            // 对话任务类型
            //单纯对话
            //送物品
            //取物品
            m_comboBoxTargetNPC.Enabled = false;
            m_labelTargetNPC.Enabled = false;
            m_comboBoxTargetItemBegin.Enabled = false;
            m_labelTargetItem.Enabled = false;
            m_comboBoxTargetMonsterBegin.Enabled = false;
            m_labelTargetMonster.Enabled = false;

            // 目标 怪物/道具 范围
            m_comboBoxTargetMonsterBegin.Enabled = false;
            m_comboBoxTargetMonsterEnd.Enabled = false;
            m_comboBoxTargetItemBegin.Enabled = false;
            m_comboBoxTargetItemEnd.Enabled = false;

            String strSelected = m_comboBoxSubType.SelectedItem.ToString();
            if (strSelected == OptionManager.MissionTypeDialogSubType.单纯对话.ToString())
            {
                m_comboBoxTargetNPC.Enabled = true;
                m_labelTargetNPC.Enabled = true;
            }
            else if (strSelected == OptionManager.MissionTypeDialogSubType.送物品.ToString())
            {
                m_comboBoxTargetNPC.Enabled = true;
                m_labelTargetNPC.Enabled = true;
                m_comboBoxTargetItemBegin.Enabled = true;
                m_labelTargetItem.Enabled = true;
            }
            else if (strSelected == OptionManager.MissionTypeDialogSubType.取物品.ToString())
            {
                m_comboBoxTargetNPC.Enabled = true;
                m_labelTargetNPC.Enabled = true;
                m_comboBoxTargetItemBegin.Enabled = true;
                m_labelTargetItem.Enabled = true;
            }
            else if (strSelected == OptionManager.MissionTypeCounterSubType.杀人.ToString())
            {
                m_comboBoxTargetNPC.Enabled = true;
                m_labelTargetNPC.Enabled = true;
                m_labelTargetValue.Enabled = true;
                m_textBoxTargetValue.Enabled = true;
                m_comboBoxTargetItemBegin.Enabled = false;
                m_labelTargetItem.Enabled = false;

            }
            else if (strSelected == OptionManager.MissionTypeCounterSubType.杀怪.ToString())
            {
                m_labelTargetValue.Enabled = true;
                m_textBoxTargetValue.Enabled = true;
                m_labelTargetMonster.Enabled = true;
                m_comboBoxTargetMonsterBegin.Enabled = true;

                // 目标 怪物 范围
                m_comboBoxTargetMonsterBegin.Enabled = true;
                m_comboBoxTargetMonsterEnd.Enabled = true;

            }
            else if (strSelected == OptionManager.MissionTypeCounterSubType.收集物品.ToString())
            {
                m_labelTargetValue.Enabled = true;
                m_textBoxTargetValue.Enabled = true;
                m_comboBoxTargetItemBegin.Enabled = true;
                m_labelTargetItem.Enabled = true;

                // 目标 道具 范围
                m_comboBoxTargetItemBegin.Enabled = true;
                m_comboBoxTargetItemEnd.Enabled = true;
            }
        }

        private void m_btnRequirementEdit_Click(object sender, EventArgs e)
        {
            // 编辑 接受任务条件 按钮
            MissionConditionForm form = new MissionConditionForm(m_listBoxCondition.Items, m_listBoxConditionValue.Items);
            form.ShowDialog();

            // 从 form 中取得需求列表,写到条件中.
            m_listBoxCondition.Items.Clear();
            foreach (String strCondition in form.Conditions)
            {
                m_listBoxCondition.Items.Add(strCondition);
            }

            m_listBoxConditionValue.Items.Clear();
            foreach (String strValue in form.Values)
            {
                m_listBoxConditionValue.Items.Add(strValue);
            }

        }


        private void m_btnFixedAwardEdit_Click(object sender, EventArgs e)
        {
            // 编辑 固定奖励
            MissionAwardForm form = new MissionAwardForm(m_listBoxFixedAward.Items, m_listBoxFixedAwardValue.Items);
            form.ShowDialog();

            // 从 form 中取得Award列表,写到条件中.
            m_listBoxFixedAward.Items.Clear();
            foreach (String strAward in form.Awards)
            {
                m_listBoxFixedAward.Items.Add(strAward);
            }

            m_listBoxFixedAwardValue.Items.Clear();
            foreach (String strValue in form.Values)
            {
                m_listBoxFixedAwardValue.Items.Add(strValue);
            }

        }

        private void m_btnSelectedAwardEdit_Click(object sender, EventArgs e)
        {
            // 编辑 可选奖励
            MissionAwardForm form = new MissionAwardForm(m_listBoxSelectedAward.Items, m_listBoxSelectedAwardValue.Items);
            form.ShowDialog();

            // 从 form 中取得Award列表,写到条件中.
            m_listBoxSelectedAward.Items.Clear();
            foreach (String strAward in form.Awards)
            {
                m_listBoxSelectedAward.Items.Add(strAward);
            }

            m_listBoxSelectedAwardValue.Items.Clear();
            foreach (String strValue in form.Values)
            {
                m_listBoxSelectedAwardValue.Items.Add(strValue);
            }

        }

        private void m_listBoxCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iIdx = m_listBoxCondition.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxConditionValue.SelectedIndex = iIdx;

            }
        }

        private void m_listBoxFixedAward_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iIdx = m_listBoxFixedAward.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxFixedAwardValue.SelectedIndex = iIdx;
            }
        }

        private void m_listBoxSelectedAward_SelectedIndexChanged(object sender, EventArgs e)
        {
            int iIdx = m_listBoxSelectedAward.SelectedIndex;
            if (iIdx > -1)
            {
                m_listBoxSelectedAwardValue.SelectedIndex = iIdx;
            }
        }

        private void ResetMissionInputUI()
        {
            // 重置所有 UI
            m_webBrowserGetMissionDesc.DocumentText = "<HTML></HTML>".Replace("\0", "");
            m_webBrowserSubmitMissionDesc.DocumentText = "<HTML></HTML>".Replace("\0", "");
            m_webBrowserSimpleMissionDesc.DocumentText = "<HTML></HTML>".Replace("\0", "");

            m_textBoxName.Text = "";
            m_textBoxID.Text = "";
            m_textBoxReserved.Text = "0";
            m_textBoxLevel.Text = "0";
            m_listBoxFixedAward.Items.Clear();
            m_listBoxFixedAwardValue.Items.Clear();

            m_listBoxCondition.Items.Clear();
            m_listBoxConditionValue.Items.Clear();

            m_textBoxTargetValue.Text = "1";

            RefreshRegionList(m_comboBoxTargetArea);
            m_comboBoxTargetArea.SelectedIndex = -1;
            m_comboBoxTargetArea.Text = "";

            m_comboBoxType.SelectedIndex = 0;

            m_listBoxSelectedAward.Items.Clear();
            m_listBoxSelectedAwardValue.Items.Clear();

            m_checkBoxShare.Checked = false;
            m_checkBoxRepeat.Checked = false;
            m_checkBoxGiveUp.Checked = false;

            m_comboBoxTargetNPC.SelectedIndex = -1;
            m_comboBoxTargetNPC.Text = "";

            m_comboBoxTargetMonsterBegin.SelectedIndex = -1;
            m_comboBoxTargetMonsterBegin.Text = "";
            m_comboBoxTargetMonsterEnd.SelectedIndex = -1;
            m_comboBoxTargetMonsterEnd.Text = "";

            m_comboBoxTargetItemBegin.SelectedIndex = -1;
            m_comboBoxTargetItemBegin.Text = "";
            m_comboBoxTargetItemEnd.SelectedIndex = -1;
            m_comboBoxTargetItemEnd.Text = "";

            m_comboBoxSubmitTo.SelectedIndex = -1;
            m_comboBoxSubmitTo.Text = "";

            m_comboBoxProvider.SelectedIndex = -1;
            m_comboBoxProvider.Text = "";

            m_comboBoxSubType.SelectedIndex = -1;
            m_comboBoxSubType.Text = "";
            m_comboBoxTargetProp.Text = "";

            m_comboBoxFlowType.SelectedIndex = -1;
            //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
            //m_textBoxSimpleDesc.Text = "";    // 任务的简单描述

            //m_comboBoxLeadMap.Items.Clear(); // 任务所属的地图
            //foreach(int MapID in SceneSketchManager.Instance.GetSceneIDList())
            //{
            //    m_comboBoxLeadMap.Items.Add(MapID);
            //}
            m_comboBoxLeadMap.SelectedIndex = -1;

            //m_comboBoxStarLevel.Items.Clear();      // 任务的难度（星级）
            //m_comboBoxStarLevel.Items.Add("一星");
            //m_comboBoxStarLevel.Items.Add("二星");
            //m_comboBoxStarLevel.Items.Add("三星");
            //m_comboBoxStarLevel.Items.Add("四星");
            //m_comboBoxStarLevel.Items.Add("五星");
            m_comboBoxStarLevel.SelectedIndex = -1;

            m_textBoxTimeLimit.Text = "0";          // 任务的时间限制
            //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
            // 2009-01-06 徐磊 存盘方式UI界面的优化
            m_textBoxReserved.Text = "1";
            m_textBoxReserved.Enabled = false;
            m_comboBoxReservedFilter.SelectedIndex = 3;
            // 2009-01-06 徐磊 存盘方式UI界面的优化
        }

        private void m_btnNew_Click(object sender, EventArgs e)
        {
            // 新任务
            if (m_bMissionChanged)
            {
                // 如果任务有改动，弹出 是否保存当前任务 对话框
            }
            m_currMission = null;
            ResetMissionInputUI();

        }

        #region 从显示文本中解析 ID 相关函数

        static public int ExtractIDFromID_Name(String strID_Name)
        {
            try
            {
                String strID = strID_Name.Substring(0, strID_Name.IndexOf('_'));
                return int.Parse(strID);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return -1;
            }
        }

        static public int ExtractMissionIDFromDisplayText(String strDisplay)
        {
            int iID = -1;
            if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_ID)
            {
                iID = int.Parse(strDisplay);
            }
            else if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_NAME)
            {
                iID = MainForm.Instance.m_MissionManager.FindByName(strDisplay).ID;
            }
            else if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_ID_NAME)
            {
                iID = ExtractIDFromID_Name(strDisplay);
            }

            return iID;
        }

        // 根据 OptionManager中 NPC display mode 不同， npc text 可能为 id, name, 或 id_name
        static public int ExtractNPCInfoIDFromDisplayText(String strNPCText)
        {
            int iID = -1;
            if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_ID)
            {
                iID = int.Parse(strNPCText);
            }
            else if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_NAME)
            {
                iID = NPCManager.Instance.GetNPCInfoIDByName(strNPCText);
            }
            else if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_ID_NAME)
            {
                iID = ExtractIDFromID_Name(strNPCText);
            }

            return iID;
        }

        static public int ExtractNPCCreatorIDFromDisplayText(String strNPCText)
        {
            int iID = -1;
            if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_ID)
            {
                iID = int.Parse(strNPCText);
            }
            else if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_NAME)
            {
                iID = NPCManager.Instance.GetNPCCreatorIDByName(strNPCText);
            }
            else if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_ID_NAME)
            {
                iID = ExtractIDFromID_Name(strNPCText);
            }

            return iID;
        }

        static public int ExtractItemIDFromDisplayText(String strItemText)
        {
            int iID = -1;
            if (OptionManager.ItemDisplayMode == OptionManager.DisplayMode.DM_ID)
            {
                iID = int.Parse(strItemText);
            }
            else if (OptionManager.ItemDisplayMode == OptionManager.DisplayMode.DM_NAME)
            {
                iID = ItemManager.Instance.GetItemIDByName(strItemText);
            }
            else if (OptionManager.ItemDisplayMode == OptionManager.DisplayMode.DM_ID_NAME)
            {
                iID = ExtractIDFromID_Name(strItemText);
            }

            return iID;
        }

        #endregion


        private bool SaveMission()
        {
            // 保存当前任务
            try
            {
                #region 从 UI 获取用户的输入数据

                // 这些是所有任务都具有的属性
                int iID = int.Parse(m_textBoxID.Text);
                String strName = m_textBoxName.Text;
                String strGetDesc = m_webBrowserGetMissionDesc.DocumentText;
                String strSubmitDesc = m_webBrowserSubmitMissionDesc.DocumentText;
                String strSimpleDesc = m_webBrowserSimpleMissionDesc.DocumentText;
                strSubmitDesc = strSubmitDesc.Replace("\0", "");
                strGetDesc = strGetDesc.Replace("\0", "");
                String strType = m_comboBoxType.SelectedItem.ToString();
                int iClass = ExtractIDFromID_Name(m_comboBoxClass.SelectedItem.ToString());
                int iLevel = int.Parse(m_textBoxLevel.Text);
                // 2009-01-06 徐磊 存盘方式UI界面的优化
                int iReserved = int.Parse(m_textBoxReserved.Text);
                if (m_comboBoxReservedFilter.SelectedIndex == 0)
                {
                    iReserved = 0;
                }
                else if (m_comboBoxReservedFilter.SelectedIndex == 1)
                {

                }
                else if (m_comboBoxReservedFilter.SelectedIndex == 2)
                {
                    iReserved += 1000;
                }
                else if (m_comboBoxReservedFilter.SelectedIndex == 3)
                {
                    iReserved = 9999;
                }
                // 2009-01-06 徐磊 存盘方式UI界面的优化

                // 2009-01-08 徐磊 增加任务流程分类
                String strFlowType = null;
                if (m_comboBoxFlowType.SelectedItem == null)
                {
                    MessageBox.Show("请输入任务流程类型！");
                    return false;
                }
                else
                {
                    strFlowType = m_comboBoxFlowType.SelectedItem.ToString();
                }
                // 2009-01-08 徐磊 增加任务流程分类

                //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
                //String strSimpleDesc = m_textBoxSimpleDesc.Text;    // 任务的简单描述
                int iLeadMap = 0;                                   // 任务所属的地图
                if (m_comboBoxLeadMap.SelectedItem == null)
                {
                    MessageBox.Show("请设置--任务所属区域--！");
                    return false;
                }
                if (!int.TryParse(m_comboBoxLeadMap.SelectedItem.ToString(), out iLeadMap))
                {
                    MessageBox.Show("请设置--任务所属区域--！");
                    return false;
                }
                int iStarLevel = m_comboBoxStarLevel.SelectedIndex + 1;           // 任务的难度（星级）
                int iTimeLimit = int.Parse(m_textBoxTimeLimit.Text);           // 任务的时间限制
                //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//

                // 这些是可选属性
                String strSubMissionType = "";   // 对话类任务类型 简单对话 
                int iProvider = -1;              // 任务提供者 id
                int iSubmitTo = -1;              // 交任务对象
                bool bItemProvider = false;      // 是否是从 Item 处获取的任务
                bool bRegionProvider = false;    // 是否区域触发任务
                ArrayList conditions = new ArrayList();       // 接任务条件
                ArrayList fixedAwards = new ArrayList();      // 固定奖励
                ArrayList selectedAwards = new ArrayList();   // 可选奖励
                int iTargetNPC = -1;       // 目标怪物 或 npc
                int iTargetMonsterBegin = -1;   // 目标怪物范围
                int iTargetMonsterEnd = -1;     //
                int iTargetItemBegin = -1;      // 目标物件
                int iTargetItemEnd = -1;        // 目标物件
                int iTargetArea = -1;
                String strTrackName = "";       // 追踪名称

                Hashtable targetMonsterList = new Hashtable(); // 目标怪物列表
                Hashtable targetItemList = new Hashtable(); // 目标物件列表

                int iTargetProperty = -1;  // 目标属性
                int iTargetValue = -1;     // 目标值


                bool bShare = false;          // 共享任务
                bool bRepeat = false;         // 可重复任务
                bool bGiveUp = false;         // 可放弃任务
                //bool bOrder = false;          // 按顺序完成的任务

                if (m_comboBoxSubType.SelectedItem != null)
                {
                    strSubMissionType = m_comboBoxSubType.SelectedItem.ToString();
                }

                if (m_comboBoxProvider.SelectedItem != null)
                {
                    // 区域做特殊处理
                    if (m_checkBoxRegionProvider.Checked)
                    {
                        iProvider = int.Parse(m_comboBoxProvider.SelectedItem.ToString());
                    }
                    else
                    {
                        iProvider = ExtractNPCInfoIDFromDisplayText(m_comboBoxProvider.SelectedItem.ToString());
                    }
                }

                bItemProvider = m_checkBoxItemProvider.Checked;
                bRegionProvider = m_checkBoxRegionProvider.Checked;

                if (m_comboBoxSubmitTo.SelectedItem != null)
                {
                    iSubmitTo = ExtractNPCInfoIDFromDisplayText(m_comboBoxSubmitTo.SelectedItem.ToString());
                }

                if (m_comboBoxTargetNPC.SelectedItem != null)
                {
                    iTargetNPC = ExtractNPCInfoIDFromDisplayText(m_comboBoxTargetNPC.SelectedItem.ToString());
                }

                // 接受任务条件
                for (int i = 0; i < m_listBoxCondition.Items.Count; i++)
                {
                    String strCondition = m_listBoxCondition.Items[i].ToString();
                    String strValue = m_listBoxConditionValue.Items[i].ToString();
                    conditions.Add(new MissionCondition(strCondition, strValue));
                }

                // 固定任务奖励
                for (int i = 0; i < m_listBoxFixedAward.Items.Count; i++)
                {
                    String strAward = m_listBoxFixedAward.Items[i].ToString();
                    String strValue = m_listBoxFixedAwardValue.Items[i].ToString();

                    fixedAwards.Add(new MissionAward(strAward, strValue));
                }

                // 可选任务奖励
                for (int i = 0; i < m_listBoxSelectedAward.Items.Count; i++)
                {
                    String strAward = m_listBoxSelectedAward.Items[i].ToString();
                    String strValue = m_listBoxSelectedAwardValue.Items[i].ToString();
                    selectedAwards.Add(new MissionAward(strAward, strValue));
                }

                if (m_comboBoxTargetMonsterBegin.SelectedItem != null)
                {
                    iTargetMonsterBegin = ExtractNPCInfoIDFromDisplayText(m_comboBoxTargetMonsterBegin.SelectedItem.ToString());
                }

                if (m_comboBoxTargetMonsterEnd.SelectedItem != null)
                {
                    iTargetMonsterEnd = ExtractNPCInfoIDFromDisplayText(m_comboBoxTargetMonsterEnd.SelectedItem.ToString());
                }

                if (m_comboBoxTargetItemBegin.SelectedItem != null)
                {
                    iTargetItemBegin = ExtractItemIDFromDisplayText(m_comboBoxTargetItemBegin.SelectedItem.ToString());
                }

                if (m_comboBoxTargetItemEnd.SelectedItem != null)
                {
                    iTargetItemEnd = ExtractItemIDFromDisplayText(m_comboBoxTargetItemEnd.SelectedItem.ToString());
                }

                if (m_comboBoxTargetProp.SelectedItem != null)
                {
                    iTargetProperty = int.Parse(m_comboBoxTargetProp.SelectedItem.ToString());
                }

                if (m_textBoxTargetValue.Text != "")
                {
                    iTargetValue = int.Parse(m_textBoxTargetValue.Text);
                }

                // 2009-01-20 徐磊新增 区域探索类任务
                if (m_comboBoxTargetArea.SelectedItem != null)
                {
                    iTargetArea = int.Parse(m_comboBoxTargetArea.SelectedItem.ToString());
                }
                // 2009-01-20 徐磊新增 区域探索类任务

                strTrackName = m_textBoxTrackName.Text;     // 追踪名称

                bShare = m_checkBoxShare.Checked;
                bRepeat = m_checkBoxRepeat.Checked;
                bGiveUp = m_checkBoxGiveUp.Checked;

                #endregion  // 从 UI 获取数据完成

                // 从 MissionManager 中查找是否已经有相同 id 任务
                Mission mission = m_MissionManager.FindByID(iID);
                if (mission != null)
                {
                    // 警告用户已经存在该 id 的任务，是否覆盖。
                    DialogResult result = MessageBox.Show("已存在相同 ID 的任务。是否覆盖？", "警告", MessageBoxButtons.YesNo);
                    if (result == DialogResult.No)
                    {
                        return false;
                    }

                    // 判断 mission 原相关 NPC 是否有改变。如果发生改变 断掉原 NPC 状态图关于本 mission 的信息。
                    if (mission.Provider > 0 && mission.Provider != iProvider || !mission.FlowType.Equals(strFlowType))
                    {
                        // 获取 Provider 状态图。删除本  mission 信息
                        String strProviderDiaName = null;
                        if (mission.ItemProvider)
                        {
                            int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_ITEM, mission.Provider);
                            strProviderDiaName = OptionManager.ItemPrefix + iProviderStateDiaID.ToString();
                        }
                        else if (mission.RegionProvider)
                        {
                            int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_AREA, mission.Provider);
                            strProviderDiaName = OptionManager.AreaPrefix + iProviderStateDiaID.ToString();
                        }
                        else
                        {
                            int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, mission.Provider);
                            strProviderDiaName = OptionManager.NPCPrefix + iProviderStateDiaID.ToString();
                        }

                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strProviderDiaName);
                        if (dia != null)
                        {
                            //dia.RemoveStateByMissionID(mission.ID);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_GET);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_GET_HINT);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                            StateDiagramManager.SaveStateDiagram(dia);
                        }
                    }

                    if (mission.TargetNPC > 0 && mission.TargetNPC != iTargetNPC || !mission.FlowType.Equals(strFlowType))
                    {
                        // 获取 Provider 状态图。删除本  mission 信息
                        int iTargetNPCStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, mission.TargetNPC);
                        String strTargetNPCDiaName = OptionManager.NPCPrefix + iTargetNPCStateDiaID.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strTargetNPCDiaName);
                        if (dia != null)
                        {
                            //dia.RemoveStateByMissionID(mission.ID);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                            StateDiagramManager.SaveStateDiagram(dia);
                        }
                    }

                    if (mission.TargetArea > 0 && mission.TargetArea != iTargetArea || !mission.FlowType.Equals(strFlowType))
                    {
                        // 获取 Provider 状态图。删除本  mission 信息
                        int iTargetAreaStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_AREA, mission.TargetArea);
                        String strTargetAreaDiaName = OptionManager.AreaPrefix + iTargetAreaStateDiaID.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strTargetAreaDiaName);
                        if (dia != null)
                        {
                            //dia.RemoveStateByMissionID(mission.ID);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                            StateDiagramManager.SaveStateDiagram(dia);
                        }
                    }

                    if (mission.SubmitTo > 0 && mission.SubmitTo != iSubmitTo)
                    {
                        // 获取 SubmitTo 状态图。删除本  mission 信息
                        int iSubmitToStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, mission.SubmitTo);
                        String strSubmitToDiaName = OptionManager.NPCPrefix + iSubmitToStateDiaID.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strSubmitToDiaName);
                        if (dia != null)
                        {
                            //dia.RemoveStateByMissionID(mission.ID);
                            dia.RemoveElement(mission.ID, StateDiagram.ElementCategory.EC_SUBMIT);
                            StateDiagramManager.SaveStateDiagram(dia);
                        }
                    }

                    // 移除原目标 item, monster 脚本
                    #region

                    /*
                    if (mission.TargetItemBegin != -1)
                    {
                        String strItemDiaName = OptionManager.ItemPrefix + mission.TargetItemBegin.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strItemDiaName);
                        if (dia != null)
                        {
                            dia.RemoveStateByMissionID(mission.ID);
                            // 保存改变过的 dia
                        }
                    }

                    if (mission.TargetItemEnd != -1)
                    {
                        String strItemDiaName = OptionManager.ItemPrefix + mission.TargetItemEnd.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strItemDiaName);
                        if (dia != null)
                        {
                            dia.RemoveStateByMissionID(mission.ID);
                        }
                    }

                    if (mission.TargetMonsterBegin != -1)
                    {
                        String strMonsterDiaName = OptionManager.ItemPrefix + mission.TargetMonsterBegin.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strMonsterDiaName);
                        if (dia != null)
                        {
                            dia.RemoveStateByMissionID(mission.ID);
                            // 保存改变过的 dia
                        }
                    }

                    if (mission.TargetMonsterEnd != -1)
                    {
                        String strMonsterDiaName = OptionManager.ItemPrefix + mission.TargetMonsterEnd.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strMonsterDiaName);
                        if (dia != null)
                        {
                            dia.RemoveStateByMissionID(mission.ID);
                            // 保存改变过的 dia
                        }
                    }
                    */
                    #endregion
                }
                else
                {
                    // 新任务
                    mission = new Mission();
                    m_currMission = mission;
                    m_MissionManager.MissionList.Add(mission);
                    String strDisplayText = GetDisplayText(mission.ID, mission.Name, OptionManager.MissionDisplayMode);

                }

                #region 将 UI 属性写入任务
                mission.ID = iID;
                mission.Name = strName;
                mission.GetDescription = strGetDesc;
                mission.SubmitDescription = strSubmitDesc;
                mission.Type = strType;
                mission.SubMissionType = strSubMissionType;
                mission.Class = iClass;
                mission.Level = iLevel;
                mission.RecordReserve = iReserved;
                mission.Provider = iProvider;
                mission.SubmitTo = iSubmitTo;
                mission.ItemProvider = bItemProvider;
                mission.RegionProvider = bRegionProvider;
                mission.TargetItemBegin = iTargetItemBegin;
                mission.TargetItemEnd = iTargetItemEnd;
                mission.TargetNPC = iTargetNPC;
                mission.TargetMonsterBegin = iTargetMonsterBegin;
                mission.TargetMonsterEnd = iTargetMonsterEnd;
                mission.TargetArea = iTargetArea;
                mission.TargetProperty = iTargetProperty;
                mission.TargetValue = iTargetValue;
                mission.TrackName = strTrackName;
                mission.Share = bShare;
                mission.Repeat = bRepeat;
                mission.GiveUp = bGiveUp;

                // 2009-01-08 徐磊 增加任务流程分类
                mission.FlowType = strFlowType;
                // 2009-01-08 徐磊 增加任务流程分类

                //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
                mission.SimpleDesc = strSimpleDesc;    // 任务的简单描述
                mission.LeadMap = iLeadMap;          // 任务所属的地图
                mission.StarLevel = iStarLevel; // 任务的难度（星级）
                mission.TimeLimit = iTimeLimit;// 任务的时间限制
                //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//

                // 拷贝 任务接受条件列表
                mission.Conditions.Clear();
                foreach (MissionCondition condition in conditions)
                {
                    mission.Conditions.Add(condition);
                }

                // 拷贝固定奖励列表
                mission.FixedAwards.Clear();
                foreach (MissionAward award in fixedAwards)
                {
                    mission.FixedAwards.Add(award);
                }
                // 拷贝可选奖励列表。
                mission.SelectedAwards.Clear();
                foreach (MissionAward award in selectedAwards)
                {
                    mission.SelectedAwards.Add(award);
                }

                #endregion

                m_MissionManager.SaveMissions(OptionManager.MissionPath);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }
            m_bMissionChanged = false;
            return true;
        }

        private void m_btnSave_Click(object sender, EventArgs e)
        {
            // 保存任务
            if (!SaveMission())
            {
                MessageBox.Show("任务保存失败！");
            }
            else
            {
                MessageBox.Show("任务保存完成！");
                RefreshMissionTree();
            }
        }

        private void m_btnConvertToStateDiagrom_Click(object sender, EventArgs e)
        {
            // 转换到状态图
            if (m_currMission == null)
            {
                return;
            }

            // 保存当前任务
            if (!SaveMission())
            {
                MessageBox.Show("当前任务保存失败.");
                return;
            }

            StateDiagramConvertor convertor = new StateDiagramConvertor();
            convertor.ConvertMission(m_currMission);
            m_CanvasForm.Controls.Clear();
            //m_currStateDiagram = null;
            RefreshStateDiagaramList();

        }

        static public String GetDisplayText(int iID, String strName, OptionManager.DisplayMode dm)
        {
            if (dm == OptionManager.DisplayMode.DM_ID)
            {
                return iID.ToString();
            }
            else if (dm == OptionManager.DisplayMode.DM_NAME)
            {
                return strName;
            }
            else if (dm == OptionManager.DisplayMode.DM_ID_NAME)
            {
                return iID.ToString() + "_" + strName;
            }
            else
            {
                return "";
            }
        }

        private void ShowMission(Mission mission)
        {
            // 在 UI 上显示 mission 的数据
            if (m_bMissionChanged)
            {
                DialogResult result = MessageBox.Show("当前任务已经被修改过，是否放弃该修改？", "警告", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    return;
                }
            }
            // 设为当前任务
            m_currMission = mission;


            // 将 mission 的值写到 UI 上
            //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
            //m_textBoxSimpleDesc.Text = mission.SimpleDesc;    // 任务的简单描述
            if (m_comboBoxLeadMap.Items.Contains(mission.LeadMap.ToString()))    // 任务所属的地图
            {
                m_comboBoxLeadMap.SelectedItem = mission.LeadMap.ToString();
            }
            else
            {
                m_comboBoxLeadMap.SelectedIndex = -1;
            }
            m_comboBoxStarLevel.SelectedIndex = mission.StarLevel - 1;      // 任务的难度（星级）
            m_textBoxTimeLimit.Text = mission.TimeLimit.ToString();          // 任务的时间限制
            //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
            m_textBoxID.Text = mission.ID.ToString();
            m_textBoxName.Text = mission.Name;

            m_webBrowserGetMissionDesc.DocumentText = mission.GetDescription;
            m_webBrowserSubmitMissionDesc.DocumentText = mission.SubmitDescription;
            m_webBrowserSimpleMissionDesc.DocumentText = mission.SimpleDesc;

            int idx = m_comboBoxType.Items.IndexOf(mission.Type);
            m_comboBoxType.SelectedIndex = idx;
            m_comboBoxType.Invalidate();
            if ((mission.Type.Equals(OptionManager.MissionType.对话类.ToString())
                    || mission.Type.Equals(OptionManager.MissionType.计数器类.ToString()))
                && mission.SubMissionType != null)
            {
                idx = m_comboBoxSubType.Items.IndexOf(mission.SubMissionType);
                m_comboBoxSubType.SelectedIndex = idx;
            }

            m_comboBoxClass.SelectedItem = mission.Class.ToString() + "_" + m_MissionManager.MissionClasses[mission.Class].ToString();
            m_textBoxLevel.Text = mission.Level.ToString();
            //m_textBoxReserved.Text = mission.RecordReserve.ToString();

            // 2009-01-08 徐磊 增加任务流程分类
            if (m_comboBoxFlowType.Items.Contains(mission.FlowType))
            {
                m_comboBoxFlowType.SelectedItem = mission.FlowType;
            }
            else
            {
                m_comboBoxFlowType.SelectedIndex = -1;
            }
            // 2009-01-08 徐磊 增加任务流程分类

            // 2009-01-06 徐磊 存盘方式UI界面的优化
            int iTempReserved = mission.RecordReserve;
            if (iTempReserved == 0)
            {
                m_textBoxReserved.Text = "1";
                m_textBoxReserved.Enabled = false;
                m_comboBoxReservedFilter.SelectedIndex = 0;
            }
            else if (iTempReserved >= 1 && iTempReserved <= 999)
            {
                m_textBoxReserved.Text = iTempReserved.ToString();
                m_textBoxReserved.Enabled = true;
                m_comboBoxReservedFilter.SelectedIndex = 1;
            }
            else if (iTempReserved >= 1001 && iTempReserved <= 1999)
            {
                m_textBoxReserved.Text = iTempReserved.ToString().Substring(1, 3);
                m_textBoxReserved.Enabled = true;
                m_comboBoxReservedFilter.SelectedIndex = 2;
            }
            else if (iTempReserved == 9999)
            {
                m_textBoxReserved.Text = "1";
                m_textBoxReserved.Enabled = false;
                m_comboBoxReservedFilter.SelectedIndex = 3;
            }
            else
            {
                m_textBoxReserved.Text = "1";
                m_textBoxReserved.Enabled = false;
                m_comboBoxReservedFilter.SelectedIndex = 3;
            }

            // 2009-01-06 徐磊 存盘方式UI界面的优化
            m_checkBoxItemProvider.Checked = mission.ItemProvider;
            m_checkBoxRegionProvider.Checked = mission.RegionProvider;  // 2008-01-19 徐磊 新增区域触发任务
            if (mission.ItemProvider)
            {
                // 如果是通过使用 Item 获取的任务
                Item item = m_ItemManager.GetItemByID(mission.Provider);
                if (item != null)
                {
                    // 根据不同的显示模式生成显示文本
                    String strDisplayName = GetDisplayText(item.ID, item.Name, OptionManager.ItemDisplayMode);
                    idx = m_comboBoxProvider.Items.IndexOf(strDisplayName);
                    m_comboBoxProvider.SelectedIndex = idx;
                }
                else
                {
                    m_comboBoxProvider.SelectedIndex = -1;
                }
            }
            else if (mission.RegionProvider)
            {
                // 如果是区域触发任务
                idx = m_comboBoxProvider.Items.IndexOf(mission.Provider.ToString());
                m_comboBoxProvider.SelectedIndex = idx;
            }
            else
            {
                // 如果是从NPC处获取的任务
                m_comboBoxProviderFilter.SelectedIndex = 0;  // 全部NPC

                // 任务提供者
                NPCInfo provider = m_NPCManager.GetNPCInfoByID(mission.Provider);
                if (provider != null)
                {
                    // 根据不同的显示模式生成显示文本
                    String strDisplayName = GetDisplayText(provider.ID, provider.Name, OptionManager.NPCDisplayMode);
                    idx = m_comboBoxProvider.Items.IndexOf(strDisplayName);
                    m_comboBoxProvider.SelectedIndex = idx;
                }
                else
                {
                    m_comboBoxProvider.SelectedIndex = -1;
                }
            }


            m_comboBoxSubmitToFilter.SelectedIndex = 0;  // 全部NPC

            // 还任务对象
            NPCInfo submitTo = m_NPCManager.GetNPCInfoByID(mission.SubmitTo);
            if (submitTo != null)
            {
                // 根据不同的显示模式生成显示文本
                String strDisplayName = GetDisplayText(submitTo.ID, submitTo.Name, OptionManager.NPCDisplayMode);
                idx = m_comboBoxSubmitTo.Items.IndexOf(strDisplayName);
                m_comboBoxSubmitTo.SelectedIndex = idx;
            }
            else
            {
                m_comboBoxSubmitTo.SelectedIndex = -1;
            }

            // 任务接受条件
            m_listBoxCondition.Items.Clear();
            m_listBoxConditionValue.Items.Clear();

            foreach (MissionCondition condition in mission.Conditions)
            {
                m_listBoxCondition.Items.Add(condition.Condition);
                m_listBoxConditionValue.Items.Add(condition.Value);

            }

            // 任务固定奖励
            m_listBoxFixedAward.Items.Clear();
            m_listBoxFixedAwardValue.Items.Clear();

            foreach (MissionAward award in mission.FixedAwards)
            {
                m_listBoxFixedAward.Items.Add(award.Award);
                m_listBoxFixedAwardValue.Items.Add(award.Value);

            }

            // 任务可选奖励
            m_listBoxSelectedAward.Items.Clear();
            m_listBoxSelectedAwardValue.Items.Clear();

            foreach (MissionAward award in mission.SelectedAwards)
            {
                m_listBoxSelectedAward.Items.Add(award.Award);
                m_listBoxSelectedAwardValue.Items.Add(award.Value);

            }

            m_comboBoxTargetNPCFilter.SelectedIndex = 0;  // 全部NPC
            // 目标 NPC
            NPCInfo targetNPC = m_NPCManager.GetNPCInfoByID(mission.TargetNPC);
            if (targetNPC != null)
            {
                // 根据不同的显示模式生成显示文本
                String strDisplayName = GetDisplayText(targetNPC.ID, targetNPC.Name, OptionManager.NPCDisplayMode);
                idx = m_comboBoxTargetNPC.Items.IndexOf(strDisplayName);
                m_comboBoxTargetNPC.SelectedIndex = idx;
            }
            else
            {
                m_comboBoxTargetNPC.SelectedIndex = -1;
            }

            // 目标怪物
            NPCInfo targetMonsterBegin = m_NPCManager.GetNPCInfoByID(mission.TargetMonsterBegin);
            if (targetMonsterBegin != null)
            {
                // 根据不同的显示模式生成显示文本
                String strDisplayName = GetDisplayText(targetMonsterBegin.ID, targetMonsterBegin.Name, OptionManager.NPCDisplayMode);
                idx = m_comboBoxTargetMonsterBegin.Items.IndexOf(strDisplayName);
                m_comboBoxTargetMonsterBegin.SelectedIndex = idx;
            }
            else
            {
                m_comboBoxTargetMonsterBegin.SelectedIndex = -1;
            }
            NPCInfo targetMonsterEnd = m_NPCManager.GetNPCInfoByID(mission.TargetMonsterEnd);
            if (targetMonsterEnd != null)
            {
                // 根据不同的显示模式生成显示文本
                String strDisplayName = GetDisplayText(targetMonsterEnd.ID, targetMonsterEnd.Name, OptionManager.NPCDisplayMode);
                idx = m_comboBoxTargetMonsterEnd.Items.IndexOf(strDisplayName);
                m_comboBoxTargetMonsterEnd.SelectedIndex = idx;
            }
            else
            {
                m_comboBoxTargetMonsterEnd.SelectedIndex = -1;
            }

            // 目标 Item
            Item targetItemBegin = m_ItemManager.GetItemByID(mission.TargetItemBegin);
            if (targetItemBegin != null)
            {
                String strDisplayName = GetDisplayText(targetItemBegin.ID, targetItemBegin.Name, OptionManager.ItemDisplayMode);
                idx = m_comboBoxTargetItemBegin.Items.IndexOf(strDisplayName);
                m_comboBoxTargetItemBegin.SelectedIndex = idx;
            }
            else
            {
                m_comboBoxTargetItemBegin.SelectedIndex = -1;
            }
            Item targetItemEnd = m_ItemManager.GetItemByID(mission.TargetItemEnd);
            if (targetItemEnd != null)
            {
                String strDisplayName = GetDisplayText(targetItemEnd.ID, targetItemEnd.Name, OptionManager.ItemDisplayMode);
                idx = m_comboBoxTargetItemEnd.Items.IndexOf(strDisplayName);
                m_comboBoxTargetItemEnd.SelectedIndex = idx;
            }
            else
            {
                m_comboBoxTargetItemEnd.SelectedIndex = -1;
            }

            // Target Area
            // 2008-01-19 徐磊 新增区域探索类任务
            idx = m_comboBoxTargetArea.Items.IndexOf(mission.TargetArea.ToString());
            m_comboBoxTargetArea.SelectedIndex = idx;
            // 2008-01-19 徐磊 新增区域探索类任务

            // Target Value
            if (mission.TargetValue > -1)
            {
                m_textBoxTargetValue.Text = mission.TargetValue.ToString();
            }
            else
            {
                m_textBoxTargetValue.Text = "";
            }

            // 追踪名称 
            m_textBoxTrackName.Text = mission.TrackName;

            m_checkBoxShare.Checked = mission.Share;
            m_checkBoxGiveUp.Checked = mission.GiveUp;
            m_checkBoxRepeat.Checked = mission.Repeat;

        }

        private void m_treeViewMission_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // 在 tree view 中选择一个任务
            try
            {
                //  从 mission 列表中 选择 mission
                TreeNode node = m_treeViewMission.SelectedNode;
                if (node.Parent == null)
                {
                    return;
                }

                if (node != null)
                {
                    Mission mission = null;
                    if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_ID)
                    {
                        mission = m_MissionManager.FindByID(int.Parse(node.Text));
                    }
                    else if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_NAME)
                    {
                        mission = m_MissionManager.FindByName(node.Text);
                    }
                    else if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_ID_NAME)
                    {
                        int id = int.Parse(node.Text.Substring(0, node.Text.IndexOf('_')));
                        mission = m_MissionManager.FindByID(id);
                    }

                    if (node == null)
                    {
                        MessageBox.Show("结点 " + node.Text + " 没有找到相应任务。请速与程序相关人员联系。");
                        return;
                    }

                    // 显示当前任务
                    ResetMissionInputUI();
                    //ChangeUIByMissionType(strSelected);
                    ShowMission(mission);
                }
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }

        private void m_treeViewMission_KeyDown(object sender, KeyEventArgs e)
        {
            // 删除一个任务
            if (e.KeyCode == Keys.Delete)
            {
                if (m_currMission == null)
                {
                    return;
                }

                DialogResult result = MessageBox.Show("确定要删除任务:" + m_currMission.Name + "吗?", "提示", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    m_MissionManager.RemoveMission(m_currMission);
                    m_MissionManager.SaveMissions(OptionManager.MissionPath);
                }

                String strChangedDia = "";
                result = MessageBox.Show("要删除任务:" + m_currMission.Name + " 的相关状态图吗?", "提示", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    if (m_currMission.Provider > 0)
                    {
                        // 获取 Provider 状态图。删除本  mission 信息
                        String strProviderDiaName = null;
                        if (!m_currMission.ItemProvider)
                        {
                            int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, m_currMission.Provider);
                            strProviderDiaName = OptionManager.NPCPrefix + iProviderStateDiaID.ToString();
                        }
                        else
                        {
                            int iProviderStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_ITEM, m_currMission.Provider);
                            strProviderDiaName = OptionManager.ItemPrefix + iProviderStateDiaID.ToString();
                        }

                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strProviderDiaName);
                        if (dia != null)
                        {
                            dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_GET);
                            dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_GET_HINT);
                            StateDiagramManager.SaveStateDiagram(dia);
                            strChangedDia += (strProviderDiaName + ", ");
                        }
                    }

                    if (m_currMission.TargetNPC > 0)
                    {
                        // 获取 Provider 状态图。删除本  mission 信息
                        int iTargetNPCStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, m_currMission.TargetNPC);
                        String strTargetNPCDiaName = OptionManager.NPCPrefix + iTargetNPCStateDiaID.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strTargetNPCDiaName);
                        if (dia != null)
                        {
                            dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                            dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_PROCESS_HINT);
                            StateDiagramManager.SaveStateDiagram(dia);
                            strChangedDia += (strTargetNPCDiaName + ", ");
                        }
                    }

                    if (m_currMission.SubmitTo > 0)
                    {
                        // 获取 SubmitTo 状态图。删除本  mission 信息
                        int iSubmitToStateDiaID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, m_currMission.SubmitTo);
                        String strSubmitToDiaName = OptionManager.NPCPrefix + iSubmitToStateDiaID.ToString();
                        StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strSubmitToDiaName);
                        if (dia != null)
                        {
                            dia.RemoveElement(m_currMission.ID, StateDiagram.ElementCategory.EC_SUBMIT);
                            StateDiagramManager.SaveStateDiagram(dia);
                            strChangedDia += (strSubmitToDiaName + ", ");
                        }
                    }
                }

                MessageBox.Show("任务删除完成.发生改变的状态图有:" + strChangedDia);
                m_currMission = null;
                RefreshMissionTree();
                ShowStateDiagram(null);
                m_listBoxMO.SelectedIndex = -1;

            }
        }

        #endregion

        #region 状态图相关函数

        #region 快捷菜单响应函数
        private void 添加状态ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            StateControl stateCtrl = new StateControl(m_iCurrStateID++);
            ToolStripDropDownItem mItem = sender as ToolStripDropDownItem;
            stateCtrl.Location = m_CanvasForm.PointToClient(mItem.GetCurrentParent().Location);
            //stateCtrl.Size = new System.Drawing.Size(100, 50);
            stateCtrl.AutoGenerated = false;
            m_CanvasForm.Controls.Add(stateCtrl);
            m_CanvasForm.Invalidate();

            StateDiagramChanged = true;
        }

        private void 添加连接ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TranslationControl transCtrl = new TranslationControl(m_iCurrTranslationID++);
            ToolStripDropDownItem mItem = sender as ToolStripDropDownItem;
            transCtrl.Location = new System.Drawing.Point(0, 0);
            transCtrl.StartPoint = m_CanvasForm.PointToClient(mItem.GetCurrentParent().Location);
            transCtrl.EndPoint = new Point(transCtrl.StartPoint.X + 100, transCtrl.StartPoint.Y + 100);
            transCtrl.AutoGenerated = false;
            m_CanvasForm.Controls.Add(transCtrl);
            m_CanvasForm.Invalidate();
            StateDiagramChanged = true;
        }

        private void 保存当前状态图ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (m_strStateDiagramName == null || m_strStateDiagramName.Length == 0)
            {
                // 显示输入名称  
                NameInputForm form = new NameInputForm("");
                form.ShowDialog();
                m_strStateDiagramName = form.InputName;
                if (m_strStateDiagramName == null || m_strStateDiagramName.Length == 0)
                {
                    MessageBox.Show("输入的名称不可为空。");
                    return;
                }

            }

            // 看看该名称的 dia 是否已经存在
            StateDiagram dia = m_StateDiagramManager.GetStateDiagram(m_strStateDiagramName);
            if (dia != null)
            {
                // 显示 是否覆盖 对话框
                //DialogResult result = MessageBox.Show("存在相同名称的状态图:" + m_strStateDiagramName + "，是否覆盖？", "警告", MessageBoxButtons.YesNo);
                //if (result == DialogResult.No)
                //{
                //    return;
                //}
            }
            else
            {
                dia = new StateDiagram();
            }

            // 遍历 canvas 中所有 state, translation control。从 StateDiagram 中找到对应的元素并更新位置
            // 如果没有找到则创建新的并保存
            foreach (Control ctrl in m_CanvasForm.Controls)
            {
                if (ctrl is StateControl)
                {
                    State state = dia.GetStateByID((ctrl as StateControl).StateID);
                    if (state == null)
                    {
                        state = new State(ctrl as StateControl);
                        dia.StateList.Add(state);
                    }
                    else
                    {
                        state.Update(ctrl as StateControl);
                        //state.Location = ctrl.Location;
                        //state.Name = ().StateName;
                    }
                }
                else if (ctrl is TranslationControl)
                {
                    Translation trans = dia.GetTranslationByID((ctrl as TranslationControl).ID);
                    if (trans == null)
                    {
                        trans = new Translation(ctrl as TranslationControl);
                        dia.TranslationList.Add(trans);
                    }
                    else
                    {
                        TranslationControl transCtrl = ctrl as TranslationControl;
                        trans.Update(transCtrl);
                    }
                }
            }

            m_StateDiagramManager.SetDiagram(m_strStateDiagramName, dia);
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement xmlElem = dia.GetAsXmlElement(xmlDoc);
            xmlDoc.AppendChild(xmlElem);
            xmlDoc.Save(OptionManager.StateDiagramPath + m_strStateDiagramName + ".xml");

            StateDiagramChanged = false;
            RefreshStateDiagaramList();
        }

        private void 新建状态图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (StateDiagramChanged)
            {
                // 弹出是否保存当前状态图对话框
                DialogResult result = MessageBox.Show("当前状态图已被改变，是否保存？", "警告", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    保存当前状态图ToolStripMenuItem_Click(sender, e);
                }

                StateDiagramChanged = false;
            }

            m_iCurrStateID = 0;
            m_iCurrTranslationID = 0;
            m_CanvasForm.Controls.Clear();

            // 打开新建 StateDiagram 窗口
            NewStateDiagramForm form = new NewStateDiagramForm();
            form.ShowDialog();

            String strDiaName = form.DiaName;
            int iDiaID = form.DiaID;

            if (strDiaName == null || iDiaID == -1)
            {
                MessageBox.Show("缺失目标,创建失败.");
                return;
            }

            if (StateDiagramManager.Instance.GetStateDiagram(strDiaName) != null)
            {
                MessageBox.Show("该对象的状态图已经存在。");
                m_strStateDiagramName = null;
                return;
            }

            // 创建新的状态图
            m_strStateDiagramName = strDiaName;
            StateDiagram dia = StateDiagramConvertor.InitStateDiagram(strDiaName, iDiaID);
            StateDiagramConvertor.AddDefaultDialog(dia);
            m_StateDiagramManager.SetDiagram(m_strStateDiagramName, dia);
            StateDiagramManager.SaveStateDiagram(dia);
            StateDiagramChanged = false;
            RefreshStateDiagaramList();
        }

        private void 刷新状态图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RejudgeStateDiagramConnectToRoot();
        }

        private void m_toolStripMenuItemConvertDiaToLua_Click(object sender, EventArgs e)
        {
            if (StateDiagramChanged)
            {
                // 弹出是否保存当前状态图对话框
                DialogResult result = MessageBox.Show("当前状态图已被改变，是否保存？", "警告", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    保存当前状态图ToolStripMenuItem_Click(sender, e);
                }

                StateDiagramChanged = false;
            }

            // 转换状态图到 Lua 脚本
            if (OnEditStateDiagram != null)
            {
                LuaScriptConvertor.Convert(OnEditStateDiagram);
                // 保存 StringRes.xml
                DialogManager.Instance.SaveToXml(OptionManager.StringResourcePath);

                // 显示转换成功对话框
                MessageBox.Show("Lua 脚本生成完成.");
            }
        }

        #endregion

        // 重新判断状态图控件是否和根结点连接
        public void RejudgeStateDiagramConnectToRoot()
        {
            StateDiagramManager.RejudgeAllConnectedToRoot(m_CanvasForm);

            foreach (Control ctrl in m_CanvasForm.Controls)
            {
                ctrl.Invalidate();
            }
            m_CanvasForm.Invalidate();
        }

        public void ShowStateDiagram(StateDiagram dia, int iMissionID)
        {
            // 清除 canvas
            m_CanvasForm.Controls.Clear();
            if (dia == null)
            {
                return;
            }

            int iMaxStateID = -1;
            int iMaxTranslationID = -1;
            foreach (State state in dia.StateList)
            {
                if (state.Name.Contains(iMissionID.ToString()) || state.Name.Equals("IDLE") || state.Name.Equals("DEFAULT_DLG"))
                {
                    StateControl stateCtrl = new StateControl(state);
                    m_CanvasForm.Controls.Add(stateCtrl);
                    if (stateCtrl.StateID > iMaxStateID)
                    {
                        iMaxStateID = stateCtrl.StateID;
                    }
                }
            }
            m_iCurrStateID = iMaxStateID + 1;
            m_CanvasForm.Invalidate();

            foreach (Translation trans in dia.TranslationList)
            {
                if (trans.ID > iMaxTranslationID)
                {
                    iMaxTranslationID = trans.ID;
                }

                StateControl startCtrl = null;
                StateControl endCtrl = null;
                foreach (Control ctrl in m_CanvasForm.Controls)
                {
                    if (ctrl is StateControl)
                    {
                        if (trans.StartStateID == (ctrl as StateControl).StateID)
                        {
                            startCtrl = ctrl as StateControl;
                        }

                        if (trans.EndStateID == (ctrl as StateControl).StateID)
                        {
                            endCtrl = ctrl as StateControl;
                        }
                    }

                }
                if (startCtrl != null && endCtrl != null)
                {
                    TranslationControl transCtrl = new TranslationControl(trans, startCtrl, endCtrl);
                    m_CanvasForm.Controls.Add(transCtrl);
                }
            }
            m_iCurrTranslationID = iMaxTranslationID + 1;
        }

        public void ShowStateDiagram(StateDiagram dia)
        {
            // 清除 canvas
            m_CanvasForm.Controls.Clear();
            if (dia == null)
            {
                return;
            }

            int iMaxStateID = -1;
            int iMaxTranslationID = -1;
            foreach (State state in dia.StateList)
            {
                StateControl stateCtrl = new StateControl(state);
                m_CanvasForm.Controls.Add(stateCtrl);
                if (stateCtrl.StateID > iMaxStateID)
                {
                    iMaxStateID = stateCtrl.StateID;
                }
            }
            m_iCurrStateID = iMaxStateID + 1;
            m_CanvasForm.Invalidate();

            foreach (Translation trans in dia.TranslationList)
            {
                if (trans.ID > iMaxTranslationID)
                {
                    iMaxTranslationID = trans.ID;
                }

                StateControl startCtrl = null;
                StateControl endCtrl = null;
                foreach (Control ctrl in m_CanvasForm.Controls)
                {
                    if (ctrl is StateControl)
                    {
                        if (trans.StartStateID == (ctrl as StateControl).StateID)
                        {
                            startCtrl = ctrl as StateControl;
                        }

                        if (trans.EndStateID == (ctrl as StateControl).StateID)
                        {
                            endCtrl = ctrl as StateControl;
                        }
                    }

                }
                TranslationControl transCtrl = new TranslationControl(trans, startCtrl, endCtrl);
                m_CanvasForm.Controls.Add(transCtrl);
            }
            m_iCurrTranslationID = iMaxTranslationID + 1;
        }

        private void m_listBoxMO_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_listBoxMO.SelectedItem == null)
            {
                return;
            }

            String strMOName = m_listBoxMO.SelectedItem.ToString();

            if (StateDiagramChanged)
            {
                // 判断当前状态图是否发生改变
                // 如果发生改变，提示是否保存
                DialogResult result = MessageBox.Show("当前状态图已被改变，是否保存？", "警告", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    保存当前状态图ToolStripMenuItem_Click(sender, e);
                }
            }
            StateDiagramChanged = false;

            // StateDiagram 列表选择改变
            StateDiagram stateDia = m_StateDiagramManager.GetStateDiagram(strMOName);
            if (stateDia != null)
            {
                m_strStateDiagramName = strMOName;
                //m_currStateDiagram = stateDia;
                //ShowStateDiagram(OnEditStateDiagram);

                // 状态图按任务筛选
                if (m_MOFilter.SelectedItem.ToString().Equals("全部"))
                {
                    ShowStateDiagram(OnEditStateDiagram);
                }
                else
                {

                    int iMissionID = ExtractMissionIDFromDisplayText(m_MOFilter.SelectedItem.ToString());
                    ShowStateDiagram(OnEditStateDiagram, iMissionID);
                }

            }

            // 重新判断每个结点是否和 root 连接
            RejudgeStateDiagramConnectToRoot();
        }

        private void m_listBoxMO_KeyDown(object sender, KeyEventArgs e)
        {
            // 删除状态图
            if (e.KeyCode == Keys.Delete)
            {
                if (m_strStateDiagramName == null)
                {
                    return;
                }

                DialogResult result = MessageBox.Show("是否删除状态图:" + m_strStateDiagramName + "?", "提示", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    m_StateDiagramManager.RemoveStateDiagram(m_strStateDiagramName);
                    try
                    {
                        System.IO.File.Delete(OptionManager.StateDiagramPath + m_strStateDiagramName + ".xml");
                    }
                    catch (Exception e2)
                    {
                        MessageBox.Show(e2.ToString());
                    }
                    m_CanvasForm.Controls.Clear();
                    RefreshStateDiagaramList();

                }
            }
        }

        private void m_MOFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 状态图 任务筛选
            if (m_MOFilter.SelectedIndex == -1)
            {
                return;
            }
            //m_listBoxMO.Items.Clear();

            //int iMissionID = ExtractIDFromID_Name(m_MOFilter.SelectedItem.ToString());
            //ArrayList npcList = m_MissionManager.Mission2NPC[iMissionID] as ArrayList;
            //foreach (int iNPCID in npcList)
            //{
            //    String strDiaName = OptionManager.NPCPrefix + iNPCID.ToString();
            //    if (m_StateDiagramManager.DiagramMap.ContainsKey(strDiaName))
            //    {
            //        m_listBoxMO.Items.Add(strDiaName);
            //    }
            //}
            RefreshStateDiagaramList();
        }

        public void SetStateDiagramChanged(bool bChanged)
        {
            StateDiagramChanged = bChanged;
        }


        #endregion

        #region UI相关函数
        // 刷新所有 UI
        public void RefreshUI()
        {
            // 将 MapID 添加到 NPC 过滤器 m_comboBoxNPCFilter
            //m_comboBoxNPCFilter.Items.Clear();
            //m_comboBoxNPCFilter.Items.Add("所有NPC");
            //foreach (int iKey in m_NPCManager.MapID2NPCCreatorList.Keys)
            //{
            //    m_comboBoxNPCFilter.Items.Add(iKey.ToString());
            //}
            //m_comboBoxNPCFilter.SelectedIndex = 0;

            RefreshStateDiagaramList();
            RefreshMissionTree();
            RefreshNPCCreatorList();
            RefreshNPCInfoList();
            RefreshItemList();
            RefreshSceneIDList();
            m_webBrowserGetMissionDesc.DocumentText = "Get";
            m_webBrowserSubmitMissionDesc.DocumentText = "Submit";
            m_webBrowserSimpleMissionDesc.DocumentText = "Simple";
            m_toolStripComboBoxSceneScale.SelectedIndex = 0;
            //UserControl[] rollControl = new UserControl[]
            //{
            //    new TreeControl(m_textBoxID),
            //    new ObjSelControl(),
            //    new ConditionSelControl(),
            //};
            //string[] astrNames = new string[]
            //        {
            //            "Sequence Group General",
            //            "Sequence Group Properties",
            //            "Keystroke Settings"
            //        };
            //bool[] abCheckStates = abCheckStates = new bool[]
            //            {
            //                true,
            //                true,
            //                false
            //            };

            //AnimationToolUI.RollBarControl m_rollBar = new AnimationToolUI.RollBarControl(rollControl, astrNames, abCheckStates, 4);
            //m_pictureBoxSceneSketch.Controls.Add(m_rollBar);
        }

        // 初始化菜单栏
        private void InitMenuItem()
        {
            ToolStripMenuItem newItem = new ToolStripMenuItem("全部");
            newItem.Tag = "0";
            newItem.MouseDown += new MouseEventHandler(批量转换到状态图ToolStripMenuItem_Click);
            批量转换到状态图ToolStripMenuItem.DropDownItems.Add(newItem);   
    
            foreach(SceneSketch scene in SceneSketchManager.Instance.SceneSketchList)
            {
                newItem = new ToolStripMenuItem(scene.SceneName);
                newItem.Tag = scene.SceneID.ToString();
                newItem.MouseDown += new MouseEventHandler(批量转换到状态图ToolStripMenuItem_Click);
                批量转换到状态图ToolStripMenuItem.DropDownItems.Add(newItem);   
            }
        }

        // 刷新任务 TreeView
        public void RefreshMissionTree()
        {
            // 记录老的SelectNode
            int iOldTN = -1;
            if (m_treeViewMission.SelectedNode != null)
            {
                iOldTN = (int)m_treeViewMission.SelectedNode.Tag;
            }

            m_treeViewMission.Nodes.Clear();

            int iOldMOSelected = m_MOFilter.SelectedIndex;
            m_MOFilter.Items.Clear();

            foreach (Mission mission in m_MissionManager.MissionList)
            {
                String strName = null;
                if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_ID)
                {
                    strName = mission.ID.ToString();
                }
                else if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_NAME)
                {
                    strName = mission.Name;
                }
                else if (OptionManager.MissionDisplayMode == OptionManager.DisplayMode.DM_ID_NAME)
                {
                    strName = mission.ID.ToString() + "_" + mission.Name;
                }

                TreeNode missionNode = new TreeNode(strName);
                missionNode.Tag = mission.ID;
                String strClassName = m_MissionManager.MissionClasses[mission.Class] as String;

                TreeNode classNode = m_treeViewMission.Nodes[strClassName];
                if (classNode == null)
                {
                    classNode = new TreeNode(strClassName);
                    classNode.Name = strClassName;
                    m_treeViewMission.Nodes.Add(classNode);

                }

                classNode.Nodes.Add(missionNode);
                if (mission.ID == iOldTN)
                {
                    classNode.ExpandAll();
                    m_treeViewMission.SelectedNode = missionNode;
                }

                m_MOFilter.Items.Add(strName);
            }
            m_MOFilter.Items.Add("全部");
            m_MOFilter.SelectedIndex = iOldMOSelected;
        }

        // 刷新 NPC/NPC Creator 列表
        private void RefreshNPCCreatorList()
        {
        }

        private void RefreshNPCInfoList()
        {
            m_comboBoxTargetMonsterBegin.Items.Clear();
            m_comboBoxProvider.Items.Clear();
            m_comboBoxSubmitTo.Items.Clear();
            m_comboBoxTargetNPC.Items.Clear();
            m_comboBoxProviderFilter_SelectedIndexChanged(null, null);
            m_comboBoxSubmitToFilter_SelectedIndexChanged(null, null);
            m_comboBoxTargetNPCFilter_SelectedIndexChanged(null, null);
            RefreshNPCList(m_comboBoxTargetMonsterBegin, 1, 0);
            RefreshNPCList(m_comboBoxTargetMonsterEnd, 1, 0);
        }

        // first filter 是 npc 大分类. 1 - NPC   2 - 怪
        // second filter 是 npc 小分类. 1 2 3...
        private void RefreshNPCList(ComboBox comboBox, int iFirstFilter, int iSecondFilter)
        {
            comboBox.Items.Clear();
            ArrayList npcInfoList = m_NPCManager.NPCInfoList;
            foreach (NPCInfo npc in npcInfoList)
            {
                if ((iFirstFilter == 1 && iSecondFilter == 0) || (npc.ID.ToString().Substring(0, 2)).Equals((iFirstFilter * 10 + iSecondFilter).ToString()))
                {
                    String strName = null;
                    if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_ID)
                    {
                        strName = npc.ID.ToString();
                    }
                    else if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_NAME)
                    {
                        strName = npc.Name;
                    }
                    else if (OptionManager.NPCDisplayMode == OptionManager.DisplayMode.DM_ID_NAME)
                    {
                        strName = npc.ID.ToString() + "_" + npc.Name;
                    }
                    comboBox.Items.Add(strName);
                }
            }
        }

        private void RefreshRegionList(ComboBox comboBox)
        {
            comboBox.Items.Clear();
            comboBox.Text = "";

            Hashtable regionTable = RegionManager.Instance.MapID2GroupList;
            foreach (ArrayList regionList in regionTable.Values)
            {
                foreach (RegionGroup region in regionList)
                {
                    // 只处理服务器区域
                    if (region.BigType == RegionGroup.RegionType.服务器相关区域)
                    {
                        comboBox.Items.Add(region.OutPutID);
                    }
                }
            }
        }

        private void m_comboBoxProviderFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_checkBoxItemProvider.Checked)
            {
                return;
            }

            if (m_checkBoxRegionProvider.Checked)
            {
                return;
            }

            // 任务提供者 过滤器
            if (m_comboBoxProviderFilter.SelectedIndex > -1)
            {
                RefreshNPCList(m_comboBoxProvider, 1, m_comboBoxProviderFilter.SelectedIndex);
                //m_comboBoxProvider.SelectedIndex = 0;
            }
        }

        private void m_comboBoxSubmitToFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 交任务者 过滤器
            if (m_comboBoxSubmitToFilter.SelectedIndex > -1)
            {
                RefreshNPCList(m_comboBoxSubmitTo, 1, m_comboBoxSubmitToFilter.SelectedIndex);
                //m_comboBoxSubmitTo.SelectedIndex = 0;
            }
        }

        private void m_comboBoxTargetNPCFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 目标 NPC 过滤器
            if (m_comboBoxTargetNPCFilter.SelectedIndex > -1)
            {
                RefreshNPCList(m_comboBoxTargetNPC, 1, m_comboBoxTargetNPCFilter.SelectedIndex);
                // m_comboBoxTargetNPC.SelectedIndex = 0;
            }
        }

        private String GetItemDisplayName(Item item)
        {
            String strName = null;
            if (OptionManager.ItemDisplayMode == OptionManager.DisplayMode.DM_ID)
            {
                strName = item.ID.ToString();
            }
            else if (OptionManager.ItemDisplayMode == OptionManager.DisplayMode.DM_NAME)
            {
                strName = item.Name;
            }
            else if (OptionManager.ItemDisplayMode == OptionManager.DisplayMode.DM_ID_NAME)
            {
                strName = item.ID.ToString() + "_" + item.Name;
            }

            return strName;
        }

        private void RefreshItemList()
        {
            // 刷新道具列表
            m_comboBoxTargetItemBegin.Items.Clear();
            m_comboBoxTargetItemEnd.Items.Clear();
            // 如果是从道具获取的任务
            if (m_checkBoxItemProvider.Checked)
            {
                m_comboBoxProvider.Items.Clear();
            }
            foreach (Item item in m_ItemManager.ItemList)
            {
                String strName = GetItemDisplayName(item);
                m_comboBoxTargetItemBegin.Items.Add(strName);
                m_comboBoxTargetItemEnd.Items.Add(strName);
                if (m_checkBoxItemProvider.Checked)
                {
                    m_comboBoxProvider.Items.Add(strName);
                }
            }

            // 刷新当前任务的 TargetItemBegin, TargetItemEnd
            if (m_currMission != null)
            {
                if (m_currMission.TargetItemBegin > 0)
                {
                    Item item = m_ItemManager.GetItemByID(m_currMission.TargetItemBegin);
                    if (item != null)
                    {
                        String strName = GetItemDisplayName(item);
                        if (m_comboBoxTargetItemBegin.Items.Contains(strName))
                        {
                            int iIdx = m_comboBoxTargetItemBegin.Items.IndexOf(strName);
                            m_comboBoxTargetItemBegin.SelectedIndex = iIdx;
                        }
                    }
                }

                if (m_currMission.TargetItemEnd > 0)
                {
                    Item item = m_ItemManager.GetItemByID(m_currMission.TargetItemEnd);
                    if (item != null)
                    {
                        String strName = GetItemDisplayName(item);
                        if (m_comboBoxTargetItemEnd.Items.Contains(strName))
                        {
                            int iIdx = m_comboBoxTargetItemEnd.Items.IndexOf(strName);
                            m_comboBoxTargetItemEnd.SelectedIndex = iIdx;
                        }
                    }
                }

                // 如果是从道具获取的任务
                if (m_checkBoxItemProvider.Checked)
                {
                    int iItemID = m_currMission.Provider;
                    Item item = m_ItemManager.GetItemByID(iItemID);
                    if (item != null)
                    {
                        String strName = GetItemDisplayName(item);
                        if (m_comboBoxProvider.Items.Contains(strName))
                        {
                            int iIdx = m_comboBoxProvider.Items.IndexOf(strName);
                            m_comboBoxProvider.SelectedIndex = iIdx;
                        }
                    }
                }
            }
        }

        // 刷新状态图列表
        public void RefreshStateDiagaramList()
        {

            // 状态图 任务筛选
            if (m_MOFilter.SelectedIndex == -1)
            {
                return;
            }
            m_listBoxMO.Items.Clear();

            if (m_MOFilter.SelectedItem.ToString().Equals("全部"))
            {
                // 遍历 Data/StateDiagram 下所有 xml 文件
                String[] fileList = System.IO.Directory.GetFileSystemEntries(OptionManager.StateDiagramPath);
                foreach (String strFilePath in fileList)
                {
                    if (System.IO.Path.GetExtension(strFilePath).Equals(".xml"))
                    {
                        String fileName = System.IO.Path.GetFileNameWithoutExtension(strFilePath);
                        m_listBoxMO.Items.Add(fileName);
                    }
                }
            }
            else
            {
                // npc
                int iMissionID = ExtractMissionIDFromDisplayText(m_MOFilter.SelectedItem.ToString());
                ArrayList npcList = m_MissionManager.Mission2NPC[iMissionID] as ArrayList;
                foreach (int iNPCID in npcList)
                {
                    int iID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, iNPCID);

                    String strDiaName = OptionManager.NPCPrefix + iID.ToString();
                    m_StateDiagramManager.GetStateDiagram(strDiaName);
                    if (m_StateDiagramManager.DiagramMap.ContainsKey(strDiaName)
                        && !m_listBoxMO.Items.Contains(strDiaName))
                    {
                        m_listBoxMO.Items.Add(strDiaName);
                    }
                }

                // item
                ArrayList itemList = m_MissionManager.Mission2Item[iMissionID] as ArrayList;
                foreach (int iItemID in itemList)
                {
                    int iID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_ITEM, iItemID);

                    String strDiaName = OptionManager.ItemPrefix + iID.ToString();
                    m_StateDiagramManager.GetStateDiagram(strDiaName);
                    if (m_StateDiagramManager.DiagramMap.ContainsKey(strDiaName)
                        && !m_listBoxMO.Items.Contains(strDiaName))
                    {
                        m_listBoxMO.Items.Add(strDiaName);
                    }
                }

                // area
                ArrayList areaList = m_MissionManager.Mission2Area[iMissionID] as ArrayList;
                foreach (int iAreaID in areaList)
                {
                    int iID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_AREA, iAreaID);

                    String strDiaName = OptionManager.AreaPrefix + iID.ToString();
                    m_StateDiagramManager.GetStateDiagram(strDiaName);
                    if (m_StateDiagramManager.DiagramMap.ContainsKey(strDiaName)
                        && !m_listBoxMO.Items.Contains(strDiaName))
                    {
                        m_listBoxMO.Items.Add(strDiaName);
                    }
                }
            }
            //m_listBoxMO.Items.Clear();

            RefreshStateDiagaramStatus();
        }

        // 刷新状态图列表中状态图的Mark
        private void RefreshStateDiagaramStatus()
        {
            foreach(String strDiaName in m_listBoxMO.Items)
            {
                // 根据名称获取状态图
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strDiaName);

                // 获取状态图的status
                if(dia != null)
                {
                    dia.TeamWorkStatus = TeamWorkManager.Instance.GetDiagramStatus(strDiaName).ID;
                }
            }
        }

        // 刷新场景 ID 列表
        private void RefreshSceneIDList()
        {
            m_listBoxSceneID.Items.Clear();

            // 从 NPC Creator 中获取场景 ID
            //foreach (int iSceneID in NPCManager.Instance.MapID2NPCCreatorList.Keys)
            //{
            //    m_listBoxSceneID.Items.Add(iSceneID.ToString());
            //}
            ArrayList sceneIDList = SceneSketchManager.Instance.GetSceneIDList();
            foreach (int iSceneID in sceneIDList)
            {
                m_listBoxSceneID.Items.Add(iSceneID.ToString());
                m_comboBoxLeadMap.Items.Add(iSceneID.ToString());
            }
        }

        // 动态加载地图信息
        private void SceneDynamicLoad(int iSceneID)
        {
            SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(iSceneID);
            if (sceneSketch == null)
            {
                return;
            }

            // 动态加载block和wayinfo
            if (sceneSketch.PathImage == null)
            {
                // 加载地图碰撞信息
                String strBlockFileName = OptionManager.SceneSketchPath + "terrainProperty" + iSceneID.ToString() + ".map";
                sceneSketch.LoadBlockInfo(strBlockFileName, Color.Red, 0.3f);

                // 加载路径权重信息
                String strWayInfoFileName = OptionManager.WayInfoPath + "WayInfo" + iSceneID.ToString() + ".way";
                sceneSketch.LoadWayInfo(strWayInfoFileName, PathPaintForm.Instance.SmallColor, PathPaintForm.Instance.BigColor, 0.3f);
            }
        }

        // 刷新 SceneSketch
        public void RefreshSceneSketchImage(int iSceneID)
        {
            SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(iSceneID);
            if (sceneSketch == null)
            {
                return;
            }

            //////////////////////////////////////////////////////////////////////////
            int iTopLayer = RegionToolForm.Instance.ActiveLayer;
            m_pictureBoxSceneSketch.Image = LayerManager.Instance.SetTopLayer(iSceneID, iTopLayer);
            m_pictureBoxSceneSketch.Image = LayerManager.Instance.BlendImage;
            //return;
            //////////////////////////////////////////////////////////////////////////
            
            //Image showImage = sceneSketch.SketchImage;
            //// 判定是否显示路径
            //if (this.路径编辑ToolStripMenuItem.Checked)
            //{
            //    showImage = sceneSketch.PathImage;
            //}

            // 绘制格子线
            Image showImage = m_pictureBoxSceneSketch.Image;
            if (m_bShowGrid)
            {
                Bitmap b = new Bitmap((int)OptionManager.SceneSketchSize, (int)OptionManager.SceneSketchSize);
                Graphics g = Graphics.FromImage(b);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                Pen pen = new Pen(Color.LightGray);
                pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                float fScale = OptionManager.SceneSketchSize / (float)System.Math.Max(sceneSketch.SceneHeight, sceneSketch.SceneWidth);

                // 画场景草图
                g.DrawImage(showImage,
                            new Rectangle(0, 0, (int)OptionManager.SceneSketchSize, (int)OptionManager.SceneSketchSize),
                            new Rectangle(0, 0, showImage.Width, showImage.Height),
                            GraphicsUnit.Pixel);

                // 画线
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                for (float i = m_iGridSize * fScale; i < OptionManager.SceneSketchSize; )
                {
                    PointF p1 = new PointF(i, 0.0f);
                    PointF p2 = new PointF(i, OptionManager.SceneSketchSize);
                    PointF p3 = new PointF(0.0f, i);
                    PointF p4 = new PointF(OptionManager.SceneSketchSize, i);
                    //Point p1 = new Point((int)i, 0);
                    //Point p2 = new Point((int)i, (int)OptionManager.SceneSketchSize);
                    //Point p3 = new Point(0, (int)i);
                    //Point p4 = new Point((int)OptionManager.SceneSketchSize, (int)i);
                    g.DrawLine(pen, p1, p2);
                    g.DrawLine(pen, p3, p4);
                    i += m_iGridSize * fScale;
                }
                pen.Dispose();
                g.Dispose();

                // 设置 picture box 对应图片
                m_pictureBoxSceneSketch.Image = b;
            }
            else
            {
                m_pictureBoxSceneSketch.Image = showImage;
            }

            //Point oldLocation = m_pictureBoxSceneSketch.Location;
            //m_pictureBoxSceneSketch.Left = 0;
            //m_pictureBoxSceneSketch.Top = 0;
            //m_pictureBoxSceneSketch.Invalidate();

            //m_pictureBoxSceneSketch.Location = oldLocation;
            //m_pictureBoxSceneSketch.Invalidate();
        }

        // 刷新 SceneSketch
        public void RefreshSceneSketch(int iSceneID)
        {
            SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(iSceneID);
            if (sceneSketch == null)
            {
                return;
            }

            // 动态加载block和wayinfo
            if (sceneSketch.PathImage == null)
            {
                // 加载地图碰撞信息
                String strBlockFileName = OptionManager.SceneSketchPath + "terrainProperty" + iSceneID.ToString() + ".map";
                sceneSketch.LoadBlockInfo(strBlockFileName, Color.Red, 0.3f);

                // 加载路径权重信息
                String strWayInfoFileName = OptionManager.WayInfoPath + "WayInfo" + iSceneID.ToString() + ".way";
                sceneSketch.LoadWayInfo(strWayInfoFileName, PathPaintForm.Instance.SmallColor, PathPaintForm.Instance.BigColor, 0.3f);

                // 分层管理
                m_pictureBoxSceneSketch.Image = LayerManager.Instance.LoadScene(iSceneID, LayType.单点NPC);
            }

            if (LayerManager.Instance.CurrSceneID != GetCurrentMapID())
            {
                // 分层管理
                m_pictureBoxSceneSketch.Image = LayerManager.Instance.LoadScene(GetCurrentMapID(), LayType.单点NPC);
            }

            // 刷新场景草图
            RefreshSceneSketchImage(iSceneID);

            // 测试新的分层管理
            return;

            #region 旧的刷新方式
            /*
            // 清除原先的一切控件
            m_pictureBoxSceneSketch.Controls.Clear();
            m_pictureBoxSceneSketch.Invalidate();

            // 首先获取该场景所有 npc creator
            if (m_bShowNPC)
            {
                ArrayList npcCreatorList = NPCManager.Instance.MapID2NPCCreatorList[iSceneID] as ArrayList;
                if (npcCreatorList != null)
                {
                    foreach (NPCCreatorInfo creator in npcCreatorList)
                    {
                        NPCCreatorControl ctrl = new NPCCreatorControl(creator, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                        if (creator.Hidden)
                        {
                            ctrl.Hide();
                        }
                        m_pictureBoxSceneSketch.Controls.Add(ctrl);
                    }
                }
            }

            // 点音源刷新
            if (m_bShowPtSound)
            {
                PointSoundList ptSoundList = PointSoundManager.Instance.MapID2SoundList[iSceneID] as PointSoundList;
                if (ptSoundList != null)
                {
                    foreach (PointSoundInfo soundInfo in ptSoundList.SoundInfoList)
                    {
                        PointSoundControl sCtrl = new PointSoundControl(soundInfo, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                        m_pictureBoxSceneSketch.Controls.Add(sCtrl);
                    }
                }
            }

            // 区域刷新
            if (m_bShowRegion)
            {
                ArrayList regionList = RegionManager.Instance.MapID2RegionList[iSceneID] as ArrayList;
                if (regionList != null)
                {
                    foreach (RegionInfo info in regionList)
                    {
                        RegionControl rCtrl = new RegionControl(info, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                        if (info.Hidden)
                        {
                            rCtrl.Hide();
                        }
                        m_pictureBoxSceneSketch.Controls.Add(rCtrl);
                    }
                }
            }

            // 地图路径锚点的刷新
            ArrayList pathList = PathManager.Instance.MapID2VertexList[iSceneID] as ArrayList;
            if (pathList != null)
            {
                foreach (VertexNode vn in pathList)
                {
                    PathVertexControl pCtrl = new PathVertexControl(vn, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                    m_pictureBoxSceneSketch.Controls.Add(pCtrl);
                }
            }

            //m_pictureBoxSceneSketch.Invalidate();
            //Point oldLocation = m_pictureBoxSceneSketch.Location;
            //m_pictureBoxSceneSketch.Left = 0;
            //m_pictureBoxSceneSketch.Top = 0;
            //m_pictureBoxSceneSketch.Invalidate();

            //m_pictureBoxSceneSketch.Location = oldLocation;
            //m_pictureBoxSceneSketch.Invalidate();
            */
            #endregion


        }

        #endregion  // UI 相关

        // 场景草图相关
        #region SceneSketch

        private void m_listBoxSceneID_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 选择场景编号,显示该场景草图和 NPC Creator
            if (m_listBoxSceneID.SelectedItem != null)
            {
                Cursor = Cursors.WaitCursor;
                m_bSceneChanged = true;
                RegionToolForm.Instance.RefreshHidenNPC();
                RegionToolForm.Instance.RefreshHidenGroup();
                SceneDynamicLoad(GetCurrentMapID());
                RefreshSceneSketch(GetCurrentMapID());
                RefreshNpcStatistics(GetCurrentMapID(), int.Parse(m_toolStripStatusLabelNpcShow.Text));
                RegionToolForm.Instance.bDirtyTree = true;  // 刷新区域列表
                Cursor = Cursors.Default;
            }

            // 恢复
            m_fScale = 1.0f;
            OptionManager.SceneSketchSize = m_fOgSize * m_fScale;
            m_pictureBoxSceneSketch.Size = new Size((int)OptionManager.SceneSketchSize, (int)OptionManager.SceneSketchSize);
            m_pictureBoxSceneSketch.Left = 0;
            m_pictureBoxSceneSketch.Top = 0;
            m_pictureBoxSceneSketch.Invalidate();
        }

        #endregion

        #region 公有属性
        public int CurrentMaxStateID
        {
            get
            {
                return m_iCurrStateID;
            }
            set
            {
                m_iCurrStateID = value;
            }
        }

        public int CurrentMaxTranslationID
        {
            get
            {
                return m_iCurrTranslationID;
            }
            set
            {
                m_iCurrTranslationID = value;
            }
        }

        public String OnEditStateDiagramName
        {
            get
            {
                return m_strStateDiagramName;
            }
            set
            {
                m_strStateDiagramName = value;
            }
        }

        public NPCCreatorControl SelectedNPCCreator
        {
            get
            {
                return m_SelectedNPCCretorCtl;
            }
            set
            {
                // 取消区域的选择
                if (m_SelectedRegionCtrl != null)
                {
                    m_SelectedRegionCtrl.Selected = false;
                    m_SelectedRegionCtrl.Invalidate();
                    m_SelectedRegionCtrl = null;
                }

                if (m_SelectedNPCCretorCtl != null)
                {
                    m_SelectedNPCCretorCtl.Selected = false;
                }
                m_SelectedNPCCretorCtl = value;
                if (m_SelectedNPCCretorCtl != null)
                {
                    m_SelectedNPCCretorCtl.Selected = true;
                    MainPropertyGrid.SelectedObject = m_SelectedNPCCretorCtl.NPCCreator;
                }
                else
                {
                    MainPropertyGrid.SelectedObject = null;
                }

            }
        }

        public RegionControl SelectedRegion
        {
            get
            {
                return m_SelectedRegionCtrl;
            }
            set
            {
                // 取消NPC的选择
                if (m_SelectedNPCCretorCtl != null)
                {
                    m_SelectedNPCCretorCtl.Selected = false;
                    m_SelectedNPCCretorCtl.Invalidate();
                    m_SelectedNPCCretorCtl = null;
                }

                if (m_SelectedRegionCtrl != null)
                {
                    m_SelectedRegionCtrl.Selected = false;
                    m_SelectedRegionCtrl.Invalidate();
                }
                m_SelectedRegionCtrl = value;
                if (m_SelectedRegionCtrl != null)
                {
                    m_SelectedRegionCtrl.Selected = true;
                    m_SelectedRegionCtrl.Invalidate();
                    MainPropertyGrid.SelectedObject = m_SelectedRegionCtrl.MyRegionInfo;
                }
                else
                {
                    MainPropertyGrid.SelectedObject = null;
                }
            }
        }

        public PropertyGrid MainPropertyGrid
        {
            get
            {
                return m_propertyGrid;
            }
        }

        static public MainForm Instance
        {
            get
            {
                return ms_Instance;
            }
        }

        public Panel Canvas
        {
            get
            {
                return m_CanvasForm;
            }
        }

        public bool PathEditMode
        {
            get
            {
                return this.路径编辑ToolStripMenuItem.Checked;
            }
        }
        //public Control SelectedControl
        //{
        //    get
        //    {
        //        return m_SelectedCtrl;
        //    }
        //    set
        //    {
        //        m_SelectedCtrl = value;
        //    }
        //}

        public StateDiagram OnEditStateDiagram
        {
            get
            {
                return StateDiagramManager.Instance.GetStateDiagram(m_strStateDiagramName);
            }
        }

        public int StatisticsNpcID
        {
            get
            {
                return int.Parse(m_toolStripStatusLabelNpcShow.Text);
            }
        }

        #endregion

        private void m_pictureBoxSceneSketch_MouseDown(object sender, MouseEventArgs e)
        {
            m_pictureBoxSceneSketch.Focus();

            if (MainForm.Instance.SelectedNPCCreator != null)
            {
                MainForm.Instance.SelectedNPCCreator.Invalidate();
                MainForm.Instance.SelectedNPCCreator = null;
            }
            if (MainForm.Instance.SelectedRegion != null)
            {
                MainForm.Instance.SelectedRegion.Invalidate();
                MainForm.Instance.SelectedRegion = null;
            }
            m_MouseDownPosition = new Point(e.X, e.Y);
            //计算出相对坐标
            int iX = 0;
            int iY = 0;
            float fScale = 1.0f;
            SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(MainForm.Instance.GetCurrentMapID());
            if (sceneSketch != null)
            {
                fScale = OptionManager.SceneSketchSize / System.Math.Max(sceneSketch.SceneHeight, sceneSketch.SceneWidth);
                iX = (int)(e.X / fScale);
                iY = (int)(e.Y / fScale);
                iY = sceneSketch.SceneHeight - iY - 1;
            }




            if (e.Button == MouseButtons.Right)
            {
                //if (m_listBoxSceneID.SelectedItem != null)
                //{
                //    int iMapID = int.Parse(m_listBoxSceneID.SelectedItem.ToString());
                //    SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(iMapID);
                //    if (sceneSketch != null)
                //    {
                //        Cursor = Cursors.WaitCursor;
                //        sceneSketch.ShowBlock = !sceneSketch.ShowBlock;
                //        RefreshSceneSketchImage(iMapID);

                //        // 恢复
                //        //m_fScale = 1.0f;
                //        //OptionManager.SceneSketchSize = m_fOgSize * m_fScale;
                //        //m_pictureBoxSceneSketch.Size = new Size((int)OptionManager.SceneSketchSize, (int)OptionManager.SceneSketchSize);
                //        //m_pictureBoxSceneSketch.Left = 0;
                //        //m_pictureBoxSceneSketch.Top = 0;

                //        //m_pictureBoxSceneSketch.Invalidate();

                //        Cursor = Cursors.Default;
                //    }
                //}

                
                // 单击添加NPC
                if (m_listBoxSceneID.SelectedItem != null && !this.路径编辑ToolStripMenuItem.Checked && (RegionToolForm.Instance.ActiveLayer == 9 || RegionToolForm.Instance.ActiveLayer == 10))
                {
                    int iMapID = GetCurrentMapID();
                    NPCCreatorInfo npc = m_NPCManager.AddNPCCreator(iMapID);

                    npc.Rectangle = new Rectangle(iX, iY, 0, 0);

                    if (npc != null)
                    {
                        NPCCreatorControl npcCtrl = AddNPCCreator(npc);
                        if (npcCtrl != null)
                        {
                            m_bNewNPCState = true;

                            npcCtrl.Selected = true;
                            npcCtrl.bResizing = true;
                            SelectedNPCCreator = npcCtrl;
                            SelectedNPCCreator.Focus();
                        }
                    }
                    //RefreshSceneSketchImage(iMapID);
                    //RefreshSceneSketch(iMapID);
                    //m_pictureBoxSceneSketch.Left = 0;
                    //m_pictureBoxSceneSketch.Top = 0;
                    //m_pictureBoxSceneSketch.Invalidate();
                }
            }
            else if (e.Button == MouseButtons.Left)
            {
                if (sceneSketch == null)
                {
                    return;
                }
                else
                {
                    if (this.路径编辑ToolStripMenuItem.Checked)
                    {
                        sceneSketch.RefreshPathImage(iX, iY, PathPaintForm.Instance.Radius, PathPaintForm.Instance.CurrPen, 0.3f, m_pictureBoxSceneSketch);
                        sceneSketch.SetWayInfo(iX, iY, PathPaintForm.Instance.Radius, PathPaintForm.Instance.CurrPen);
                        //sceneSketch.RefreshPathImage(iX, iY, PathPaintForm.Instance.Radius, PathPaintForm.Instance.CurrPen, 0.3f, m_pictureBoxSceneSketch);
                    }
                }
            }
        }

        private void m_checkBoxItemProvider_CheckedChanged(object sender, EventArgs e)
        {
            if (m_checkBoxItemProvider.Checked)
            {
                // 与区域触发互斥
                m_checkBoxRegionProvider.Checked = false;

                // 从 Item 获取的任务
                this.RefreshItemList();
            }
            else
            {
                // 从 NPC 获取的任务
                m_comboBoxProvider.Items.Clear();
                m_comboBoxProviderFilter_SelectedIndexChanged(null, null);
            }
            m_comboBoxProvider.SelectedIndex = -1;
            m_comboBoxProvider.Text = "";
        }

        private void m_splitContainerState_Panel2_MouseDown(object sender, MouseEventArgs e)
        {
            m_MouseDownPosition = new Point(e.X, e.Y);
            if (e.Button == MouseButtons.Left)
            {
                StateDiagramOperationManager.Instance.ClearSelectionList();
                //foreach (Canvas.Invalidate();
            }
        }

        private void m_splitContainerState_Panel2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                int dX = e.X - m_MouseDownPosition.X;
                int dY = e.Y - m_MouseDownPosition.Y;

                this.m_splitContainerState.Panel2.Left += dX;
                this.m_splitContainerState.Panel2.Top += dY;

                foreach (Control control in m_CanvasForm.Controls)
                {
                    control.Left += dX;
                    control.Top += dY;
                }
            }

        }

        // 主 Table 控件获得按键消息
        private void m_tabControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)  // ctrl 键按下
            {
                if (e.KeyCode == Keys.S)
                {
                    // 用户按下 ctrl + s
                    if (m_tabControl.SelectedTab == m_tabPageMission)
                    {
                        // 保存任务表单
                        m_btnSave_Click(null, null);
                    }
                    else if (m_tabControl.SelectedTab == m_tabPageState)
                    {
                        // 保存当前状态图
                        保存当前状态图ToolStripMenuItem_Click(null, null);
                    }
                }
            }
        }

        private void 生成器ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if(SelectedNPCCreator != null)
            //{
            //    if (m_NPCManager.DelNPCCreator(SelectedNPCCreator.NPCCreator.ID))
            //    {
            //        SelectedNPCCreator.Hide();
            //        SelectedNPCCreator = null;
            //    }
            //}
            m_NPCManager.SaveNPCCreatorInfoToXml(OptionManager.NPCCreatorInfoPath);
            if (m_listBoxSceneID.SelectedItem != null)
            {
                int iMapID = GetCurrentMapID();
                m_RegionManager.SaveToXML(iMapID);
                SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(iMapID);
                if (sceneSketch == null)
                {
                    return;
                }
                else
                {
                    sceneSketch.SaveWayInfo(OptionManager.WayInfoPath + "WayInfo" + iMapID.ToString() + ".way");
                }
            }

            RegionManager.Instance.SaveBlockQuadRegion(OptionManager.SceneSketchPath + "BlockQuadInfo.xml");

            MessageBox.Show("保存完毕！");
        }

        private void 添加NPCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listBoxSceneID.SelectedItem != null)
            {
                int iMapID = GetCurrentMapID();
                NPCCreatorInfo npc = m_NPCManager.AddNPCCreator(iMapID);
                if (npc != null)
                {
                    AddNPCCreator(npc);
                }
                //RefreshSceneSketchImage(iMapID);
                //RefreshSceneSketch(iMapID);
                //m_pictureBoxSceneSketch.Left = 0;
                //m_pictureBoxSceneSketch.Top = 0;
                //m_pictureBoxSceneSketch.Invalidate();
            }
        }


        private void m_pictureBoxSceneSketch_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                int dX = e.X - m_MouseDownPosition.X;
                int dY = e.Y - m_MouseDownPosition.Y;

                this.m_pictureBoxSceneSketch.Left += dX;
                this.m_pictureBoxSceneSketch.Top += dY;
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (!this.路径编辑ToolStripMenuItem.Checked)
                {
                    if (m_bNewNPCState)
                    {
                        if (SelectedNPCCreator == null)
                        {
                            m_bNewNPCState = false;
                        }
                        else
                        {
                            SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(MainForm.Instance.GetCurrentMapID());
                            if (sceneSketch == null)
                            {
                                return;
                            }
                            else
                            {
                                // 获取NPC生成器
                                NPCCreatorInfo m_NPCCreator = SelectedNPCCreator.NPCCreator;

                                // 从 场景坐标转换到屏幕坐标。（从左下原点坐标系转换到左上原点坐标系）
                                Rectangle rectScreen = new Rectangle(m_NPCCreator.Rectangle.Left, sceneSketch.SceneHeight - m_NPCCreator.Rectangle.Top - m_NPCCreator.Rectangle.Height,
                                                                            m_NPCCreator.Rectangle.Width, m_NPCCreator.Rectangle.Height);
                                // 计算场景尺寸到纹理的缩放
                                float fScale = OptionManager.SceneSketchSize / System.Math.Max(sceneSketch.SceneHeight, sceneSketch.SceneWidth);
                                Rectangle rectTexture = new Rectangle((int)(rectScreen.X * fScale), (int)(rectScreen.Y * fScale),
                                    (int)(rectScreen.Width * fScale), (int)(rectScreen.Height * fScale));

                                int dx = e.X - rectTexture.Left;
                                int dy = e.Y - rectTexture.Top;
                                if (dx <= (2 * fScale) || dy <= (2 * fScale))
                                {
                                    return;
                                }
                                else
                                {
                                    Rectangle newSize = new Rectangle(m_NPCCreator.Rectangle.X, m_NPCCreator.Rectangle.Bottom + -(int)(dy / fScale), (int)(dx / fScale), (int)(dy / fScale));
                                    if (!m_NPCCreator.Rectangle.Equals(newSize))
                                    {
                                        m_NPCCreator.Rectangle = newSize;
                                        SelectedNPCCreator.Invalidate();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(MainForm.Instance.GetCurrentMapID());
                if (sceneSketch == null)
                {
                    return;
                }
                else
                {
                    float fScale = OptionManager.SceneSketchSize / System.Math.Max(sceneSketch.SceneHeight, sceneSketch.SceneWidth);
                    int iX = (int)(e.X / fScale);
                    int iY = (int)(e.Y / fScale);
                    iY = sceneSketch.SceneHeight - iY - 1;
                    String strLocation = iX.ToString() + "," + iY.ToString();
                    /* 测试用，获取当前grid的信息*/
                    int gridinfo = sceneSketch.GetGridInfo(iX, iY);
                    strLocation += "|";
                    float fBlockPercent = (float) sceneSketch.TotalBlockNum/ (float)(sceneSketch.SceneWidth * sceneSketch.SceneHeight) * 100;
                    strLocation += fBlockPercent.ToString("f2") + "%";

                    this.Text = strLocation;

                    if (this.路径编辑ToolStripMenuItem.Checked)
                    {
                        // 画矩形框
                        m_oldCur = DrawCur(iX, iY, PathPaintForm.Instance.Radius, PathPaintForm.Instance.CurrColor, m_oldCur);

                        // 左键按下
                        if (e.Button == MouseButtons.Left)
                        {
                            sceneSketch.RefreshPathImage(iX, iY, PathPaintForm.Instance.Radius, PathPaintForm.Instance.CurrPen, 0.3f, m_pictureBoxSceneSketch);
                            sceneSketch.SetWayInfo(iX, iY, PathPaintForm.Instance.Radius, PathPaintForm.Instance.CurrPen);
                            //sceneSketch.RefreshPathImage(iX, iY, PathPaintForm.Instance.Radius, PathPaintForm.Instance.CurrPen, 0.3f, m_pictureBoxSceneSketch);
                        }
                    }
                }
            }


            //m_MouseDownPosition = new Point(e.X, e.Y);
        }

        private Rectangle DrawCur(int x, int y, int radius, Color color, Rectangle old)
        {
            // 计算出屏幕坐标
            SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(MainForm.Instance.GetCurrentMapID());
            if (sceneSketch == null)
            {
                return new Rectangle();
            }
            else
            {
                float fScale = OptionManager.SceneSketchSize / System.Math.Max(sceneSketch.SceneHeight, sceneSketch.SceneWidth);
                float fScreenX = x * fScale;
                float fScreenY = (sceneSketch.SceneHeight - y - 1) * fScale;

                // 限定清空范围
                int iClearRadius = (int)(fScale * 20);
                Rectangle rectRef = new Rectangle((int)(fScreenX - iClearRadius), (int)(fScreenY - iClearRadius), iClearRadius * 2, iClearRadius * 2);

                // 清空上一帧的绘制
                m_pictureBoxSceneSketch.Invalidate(old);
                m_pictureBoxSceneSketch.Update();

                Graphics g = Graphics.FromHwndInternal(m_pictureBoxSceneSketch.Handle);
                Pen pen = new Pen(color);
                float fRadius = (float)(radius + 0.5f) * fScale;
                g.DrawRectangle(pen, fScreenX + fScale / 2 - fRadius, fScreenY + fScale / 2 - fRadius, 2 * fRadius, 2 * fRadius);
                return new Rectangle((int)(fScreenX + fScale / 2 - fRadius - 0.5f), (int)(fScreenY + fScale / 2 - fRadius - 0.5f), (int)(2 * fRadius + 3.0f), (int)(2 * fRadius + 3.0f));
            }
        }

        private void m_pictureBoxSceneSketch_MouseWheel(object sender, MouseEventArgs e)
        {
            if (System.Windows.Forms.Control.ModifierKeys == Keys.Control)//是否按下ctrl
            {
                // 保存旧的大小
                float fOldSize = OptionManager.SceneSketchSize;

                // 向上滚动，放大图像
                if (e.Delta > 0)
                {
                    m_fScale += 0.4f;
                    if (m_fScale > 3.9f)
                    {
                        m_fScale = 4.0f;
                    }
                }

                // 向下滚动，缩小图像
                if (e.Delta < 0)
                {
                    m_fScale -= 0.4f;
                    if (m_fScale < 1.1f)
                    {
                        m_fScale = 1.0f;
                    }

                }

                OptionManager.SceneSketchSize = m_fOgSize * m_fScale;

                m_timerSceneRefresh.Start();
            }
            else
            {
                if (this.路径编辑ToolStripMenuItem.Checked)
                {
                    SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(MainForm.Instance.GetCurrentMapID());
                    if (sceneSketch == null)
                    {
                        return;
                    }
                    else
                    {
                        float fScale = OptionManager.SceneSketchSize / System.Math.Max(sceneSketch.SceneHeight, sceneSketch.SceneWidth);
                        int iX = (int)(e.X / fScale);
                        int iY = (int)(e.Y / fScale);
                        iY = sceneSketch.SceneHeight - iY - 1;
   
                        // 调整大小
                        // 向上滚动，放大图像
                        if (e.Delta > 0)
                        {
                            if (PathPaintForm.Instance.Radius < 20)
                            {
                                PathPaintForm.Instance.Radius += 1;
                            }                            
                        }

                        // 向下滚动，缩小图像
                        if (e.Delta < 0)
                        {
                            if (PathPaintForm.Instance.Radius > 0)
                            {
                                PathPaintForm.Instance.Radius -= 1;
                            }    
                        }

                        // 画矩形框
                        m_oldCur = DrawCur(iX, iY, PathPaintForm.Instance.Radius, PathPaintForm.Instance.CurrColor, m_oldCur);
                    }
                }
            }
        }

        private void m_btnEditMissionDec_Click(object sender, EventArgs e)
        {
            // 编辑接任务描述信息
            HtmlEditorForm form = new HtmlEditorForm();
            form.HtmlText = m_webBrowserGetMissionDesc.DocumentText;
            form.ShowDialog();
            if (form.Ok)
            {
                m_webBrowserGetMissionDesc.DocumentText = form.HtmlText;
            }

        }

        private void m_btnEditSubmitMissionDec_Click(object sender, EventArgs e)
        {
            // 编辑还任务描述信息
            HtmlEditorForm form = new HtmlEditorForm();
            form.HtmlText = m_webBrowserSubmitMissionDesc.DocumentText;
            form.ShowDialog();
            if (form.Ok)
            {
                m_webBrowserSubmitMissionDesc.DocumentText = form.HtmlText;
            }
        }

        private void m_btnEditSimpleMissionDec_Click(object sender, EventArgs e)
        {
            // 编辑任务简单描述信息
            HtmlEditorForm form = new HtmlEditorForm();
            form.HtmlText = m_webBrowserSimpleMissionDesc.DocumentText;
            form.ShowDialog();
            if (form.Ok)
            {
                m_webBrowserSimpleMissionDesc.DocumentText = form.HtmlText;
            }
        }

        private void 任务脚本测试ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LuaScriptTestForm form = LuaScriptTestForm.Instance;
            form.Show();
        }

        private void 添加区域ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listBoxSceneID.SelectedItem != null)
            {
                int iMapID = GetCurrentMapID();
                RegionInfo info = m_RegionManager.AddRegion(iMapID);

                if (info != null)
                {
                    AddRegion(info);
                    SetSelectRegionByID(info.ID);
                }
            }
        }

        private void 区域管理工具ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegionToolForm form = RegionToolForm.Instance;
            form.Show();
        }

        public int GetCurrentMapID()
        {
            if (m_listBoxSceneID.SelectedItem != null)
            {
                return int.Parse(m_listBoxSceneID.SelectedItem.ToString());
            }
            else
            {
                return -1;
            }
        }

        private void 转换到状态图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_btnConvertToStateDiagrom_Click(sender, e);
        }

        private void 批量转换到状态图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            int iMapID = int.Parse(item.Tag.ToString());

            DialogResult dr = MessageBox.Show("是否转换 " + item.Text + " 任务到状态图？", "警告", MessageBoxButtons.YesNo);
            if (dr == DialogResult.No)
            {
                return;
            }

            // 保存当前任务
            if (m_currMission != null
                && !SaveMission())
            {
                MessageBox.Show("当前任务保存失败.");
                return;
            }

            // 加载脚本
            SDLuaConvertScript.Instance.Load(OptionManager.StateDiagramConvScriptPath + "/sd_convertor.lua");

            // 全部任务转换到状态图
            foreach (Mission mission in m_MissionManager.MissionList)
            {
                if(iMapID == 0 || mission.LeadMap == iMapID)
                {
                    if(!m_MissionManager.BanList.Contains(mission.ID))
                    {
                        StateDiagramConvertor convertor = new StateDiagramConvertor();
                        convertor.ConvertMission(mission);
                    }
                }
            }

            m_CanvasForm.Controls.Clear();
            //m_currStateDiagram = null;
            RefreshStateDiagaramList();

            MessageBox.Show("转换完毕!");
        }

        private void 转换到脚本ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_toolStripMenuItemConvertDiaToLua_Click(sender, e);
        }

        private void 批量转换到脚本ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (StateDiagramChanged)
            {
                // 弹出是否保存当前状态图对话框
                DialogResult result = MessageBox.Show("当前状态图已被改变，是否保存？", "警告", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    保存当前状态图ToolStripMenuItem_Click(sender, e);
                }

                StateDiagramChanged = false;
            }

            DialogManager.Instance.Clear();

            //// 先载入所有状态图到 manager
            //StateDiagramManager.Instance.LoadAllStateDiagram();

            // 转换所有状态图到 Lua 脚本
            foreach (StateDiagram dia in StateDiagramManager.Instance.DiagramMap.Values)
            {
                LuaScriptConvertor.Convert(dia);
            }
            // 保存 StringRes.xml
            DialogManager.Instance.SaveToXml(OptionManager.StringResourcePath);

            // 显示转换成功对话框
            MessageBox.Show("Lua 脚本生成完成.");

        }

        private void 商店编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 显示商店编辑界面
            ShopEditForm form = new ShopEditForm();
            form.ShowDialog();
        }

        public void SetSelectRegionByID(int id)
        {
            SelectedRegion = null;
            foreach (UserControl ctrl in m_pictureBoxSceneSketch.Controls)
            {
                Type t = ctrl.GetType();
                if (t.Name.Equals("RegionControl"))
                {
                    RegionControl cl = ctrl as RegionControl;
                    if (cl.MyRegionInfo.ID == id)
                    {
                        SelectedRegion = cl;
                    }
                }
            }
            if (SelectedRegion != null)
            {
                m_pictureBoxSceneSketch.Controls.SetChildIndex(SelectedRegion, 0);
            }
        }

        // 设置Region所处的层，便于显示
        public void SetRegionLayer(int id, int iLayer)
        {
            RegionControl cl = null;

            foreach (UserControl ctrl in m_pictureBoxSceneSketch.Controls)
            {
                Type t = ctrl.GetType();
                if (t.Name.Equals("RegionControl"))
                {

                    RegionControl rg = ctrl as RegionControl;
                    if (rg.MyRegionInfo.ID == id)
                    {
                        cl = rg;
                    }
                }
            }
            if (cl != null)
            {
                if (iLayer == 1)
                {
                    m_pictureBoxSceneSketch.Controls.SetChildIndex(cl, 0);
                }
                if (iLayer == 3)
                {
                    m_pictureBoxSceneSketch.Controls.SetChildIndex(cl, m_pictureBoxSceneSketch.Controls.Count - 1);
                }
            }
        }

        // 设置NPC所处的层，便于显示
        public void SetNpcLayer(int id, int iLayer)
        {
            NPCCreatorControl cl = null;

            foreach (UserControl ctrl in m_pictureBoxSceneSketch.Controls)
            {
                Type t = ctrl.GetType();
                if (t.Name.Equals("NPCCreatorControl"))
                {

                    NPCCreatorControl npc = ctrl as NPCCreatorControl;
                    if (npc.NPCCreator.ID == id)
                    {
                        cl = npc;
                    }
                }
            }
            if (cl != null)
            {
                if (iLayer == 1)
                {
                    m_pictureBoxSceneSketch.Controls.SetChildIndex(cl, 0);
                }
                if (iLayer == 3)
                {
                    m_pictureBoxSceneSketch.Controls.SetChildIndex(cl, m_pictureBoxSceneSketch.Controls.Count - 1);
                }
            }
        }

        public void UpdateNPCCreator(int id, bool bHidden)
        {
            foreach (UserControl ctrl in m_pictureBoxSceneSketch.Controls)
            {
                Type t = ctrl.GetType();
                if (t.Name.Equals("NPCCreatorControl"))
                {
                    NPCCreatorControl cl = ctrl as NPCCreatorControl;
                    if (cl.NPCCreator.ID == id)
                    {
                        //if (bHidden)
                        //{
                        //    //cl.Hide();
                        //    cl.Visible = false;
                        //}
                        //else
                        //{
                        //    //cl.Show();
                        //    cl.Visible = true;
                        //}
                        //cl.Invalidate();
                        if (cl.Visible == bHidden)
                        {
                            cl.Visible = !bHidden;
                            cl.Invalidate();
                        }
                    }
                }
            }
        }

        public void UpdateRegion(int id, bool bHidden)
        {
            foreach (UserControl ctrl in m_pictureBoxSceneSketch.Controls)
            {
                Type t = ctrl.GetType();
                if (t.Name.Equals("RegionControl"))
                {
                    RegionControl cl = ctrl as RegionControl;
                    if (cl.MyRegionInfo.ID == id)
                    {
                        if (bHidden)
                        {
                            cl.Hide();
                        }
                        else
                        {
                            cl.Show();
                        }
                        cl.Invalidate();
                    }
                }
            }
        }

        private void 导出当前地图的路径表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_listBoxSceneID.SelectedItem != null)
            {
                int iMapID = GetCurrentMapID();
                PathManager.Instance.SaveRoutToXml(iMapID, OptionManager.PathInfoPath);
            }
        }

        private void m_ToolStripMenuItemMissionConditionLua_Click(object sender, EventArgs e)
        {
            MissionConditionLuaConvertor.Convert();
        }

        private void m_btnClearTargetNpc_Click(object sender, EventArgs e)
        {
            m_comboBoxTargetNPC.SelectedIndex = -1;
            m_comboBoxTargetNPC.Text = "";
        }

        private void m_btnClearTargetProp_Click(object sender, EventArgs e)
        {
            m_comboBoxTargetProp.SelectedIndex = -1;
            m_comboBoxTargetProp.Text = "";
        }

        private void m_btnClearTargetItem_Click(object sender, EventArgs e)
        {
            m_comboBoxTargetItemBegin.SelectedIndex = -1;
            m_comboBoxTargetItemBegin.Text = "";
            m_comboBoxTargetItemEnd.SelectedIndex = -1;
            m_comboBoxTargetItemEnd.Text = "";
        }

        private void m_btnClearTargetMonster_Click(object sender, EventArgs e)
        {
            m_comboBoxTargetMonsterBegin.SelectedIndex = -1;
            m_comboBoxTargetMonsterBegin.Text = "";
            m_comboBoxTargetMonsterEnd.SelectedIndex = -1;
            m_comboBoxTargetMonsterEnd.Text = "";
        }

        private void m_btnClearTargetValue_Click(object sender, EventArgs e)
        {
            m_textBoxTargetValue.Text = "-1";
        }

        private void m_toolStripMenuItemCopy_Click(object sender, EventArgs e)
        {
            // 复制
            StateDiagramOperationManager.Instance.Copy();
        }

        private void m_toolStripMenuItemPaste_Click(object sender, EventArgs e)
        {
            // 粘贴
            StateDiagramOperationManager.Instance.Paste(this.OnEditStateDiagram);
        }

        private void 导出地图格子信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 写入格子信息
            int iMapID = GetCurrentMapID();
            if (iMapID != -1)
            {
                SceneSketch scene = SceneSketchManager.Instance.GetSceneSketch(iMapID);
                if (scene != null)
                {
                    String strBlockFileName = OptionManager.SceneSketchPath + "terrainProperty" + iMapID.ToString() + ".map";
                    String strPrePathFileName = OptionManager.SceneSketchPath + "prePath" + iMapID.ToString() + ".map";
                    Cursor = Cursors.WaitCursor;
                    scene.SaveGridInfo(strBlockFileName);
                    scene.SavePrePathInfo(strPrePathFileName);
                    Cursor = Cursors.Default;
                    MessageBox.Show("映射完毕！");
                }
            }
        }

        private void m_btnExternalLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "配置文件 (*.xml)|*.xml||";
            dlg.InitialDirectory = Application.StartupPath;
            dlg.RestoreDirectory = true;
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                MissionMgr.LoadFromExternal(dlg.FileName);
                // 显示当前任务
                ResetMissionInputUI();
            }
        }

        public void 路径编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.路径编辑ToolStripMenuItem.Checked = !this.路径编辑ToolStripMenuItem.Checked;
            LayerManager.Instance.SetLayerVisiable(LayType.路径权重, this.路径编辑ToolStripMenuItem.Checked);

            if (this.路径编辑ToolStripMenuItem.Checked)
            {
                // 不显示NPC，网格，和区域，方便显示地形
                m_bShowGrid = false;
                m_bShowNPC = false;
                m_bShowRegion = false;

                // 关闭多余的窗口
                //RegionToolForm.Instance.Close();
                RegionDescForm.Instance.Close();

                // 打开路径绘制属性窗口
                PathPaintForm.Instance.Show();

                // 改变鼠标样式
                try
                {
                    m_pictureBoxSceneSketch.Cursor = Cursors.Cross;
                }
                catch (Exception)
                {
                    m_pictureBoxSceneSketch.Cursor = Cursors.Default;
                }

            }
            else
            {
                // 关闭路径绘制属性窗口
                PathPaintForm.Instance.Hide();

                // 恢复鼠标样式
                m_pictureBoxSceneSketch.Cursor = Cursors.Default;
            }

            if (m_listBoxSceneID.SelectedItem != null)
            {
                int iMapID = GetCurrentMapID();
                RefreshSceneSketch(iMapID);
            }
        }

        private void m_comboBoxReservedFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_comboBoxReservedFilter.SelectedIndex == 0 || m_comboBoxReservedFilter.SelectedIndex == 3)
            {
                m_textBoxReserved.Enabled = false;
            }
            else
            {
                m_textBoxReserved.Enabled = true;
            }
        }

        private void m_comboBoxFlowType_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_comboBoxTargetNPC.SelectedIndex = -1;
            m_comboBoxTargetNPC.Text = "";

            m_comboBoxTargetArea.SelectedIndex = -1;
            m_comboBoxTargetArea.Text = "";

            if (m_comboBoxType.SelectedItem.ToString() == "对话类")
            {
                if (m_comboBoxFlowType.SelectedIndex == 0)   //  A类
                {
                    m_comboBoxTargetNPC.Enabled = false;
                }
                else if (m_comboBoxFlowType.SelectedIndex == 1)  //  B类
                {
                    m_comboBoxTargetNPC.Enabled = true;
                }
            }

            if (m_comboBoxType.SelectedItem.ToString() == "探索")
            {
                if (m_comboBoxFlowType.SelectedIndex == 0)   //  A类
                {
                    m_comboBoxTargetArea.Enabled = false;
                }
                else if (m_comboBoxFlowType.SelectedIndex == 1)  //  B类
                {
                    m_comboBoxTargetArea.Enabled = true;
                }
            }
        }

        private void m_checkBoxRegionProvider_CheckedChanged(object sender, EventArgs e)
        {
            if (m_checkBoxRegionProvider.Checked)
            {
                // 与道具触发互斥
                m_checkBoxItemProvider.Checked = false;

                // 区域触发任务
                RefreshRegionList(m_comboBoxProvider);
            }
            else
            {
                // 从 NPC 获取的任务
                m_comboBoxProvider.Items.Clear();
                m_comboBoxProviderFilter_SelectedIndexChanged(null, null);
            }
            m_comboBoxProvider.SelectedIndex = -1;
            m_comboBoxProvider.Text = "";
        }

        private void m_btnClearTargetArea_Click(object sender, EventArgs e)
        {
            m_comboBoxTargetArea.SelectedIndex = -1;
            m_comboBoxTargetArea.Text = "";
        }

        private void 客户端文件导出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SceneSketchManager.Instance.ExportAllConfigFile(OptionManager.ExportPath);
            PointSoundManager.Instance.ExportAll(OptionManager.ExportPath);
            String strMaxDistance = Microsoft.VisualBasic.Interaction.InputBox("请输入怪物间距", "输入", "30", 200, 300);
            int iMaxDistance = 30;
            if(int.TryParse(strMaxDistance, out iMaxDistance))
            {
                m_NPCManager.ExportMonsterDistribute(OptionManager.ExportPath + "monsterdistribute.xml", iMaxDistance);
            }
            else
            {
                MessageBox.Show("输入不合法，无法导出。");
            }
            MessageBox.Show("导出完毕！");
        }

        private void m_timerSceneRefresh_Tick(object sender, EventArgs e)
        {
            m_timerSceneRefresh.Stop();
            Cursor = Cursors.WaitCursor;

            float fOldSize = m_pictureBoxSceneSketch.Size.Width;

            m_pictureBoxSceneSketch.Size = new Size((int)OptionManager.SceneSketchSize, (int)OptionManager.SceneSketchSize);

            if (m_listBoxSceneID.SelectedItem != null)
            {
                RefreshSceneSketch(GetCurrentMapID());
                if (m_fScale < 1.1f)
                {
                    this.m_pictureBoxSceneSketch.Left = 0;
                    this.m_pictureBoxSceneSketch.Top = 0;
                }
                else
                {
                    int iSizeChange = (int)((OptionManager.SceneSketchSize - fOldSize) / 2.0f);
                    this.m_pictureBoxSceneSketch.Left -= iSizeChange;
                    this.m_pictureBoxSceneSketch.Top -= iSizeChange;
                }
                //m_pictureBoxSceneSketch.Invalidate();
            }
            //m_MouseDownPosition = new Point(e.X, e.Y);
            Cursor = Cursors.Default;
        }

        public NPCCreatorControl AddNPCCreator(NPCCreatorInfo creator)
        {
            SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(GetCurrentMapID());
            if (sceneSketch != null)
            {
                NPCCreatorControl ctrl = new NPCCreatorControl(creator, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                m_pictureBoxSceneSketch.Controls.Add(ctrl);
                return ctrl;
            }
            return null;
        }

        public void AddRegion(RegionInfo info)
        {
            SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(GetCurrentMapID());
            if (sceneSketch != null)
            {
                RegionControl rCtrl = new RegionControl(info, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                m_pictureBoxSceneSketch.Controls.Add(rCtrl);
            }
        }

        private void m_toolStripComboBoxSceneScale_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            // 保存旧的大小
            float fOldSize = OptionManager.SceneSketchSize;

            // 设置新的大小
            m_fScale = (float)(m_toolStripComboBoxSceneScale.SelectedIndex + 1);
            OptionManager.SceneSketchSize = m_fOgSize * m_fScale;

            // 改变大小
            m_pictureBoxSceneSketch.Size = new Size((int)OptionManager.SceneSketchSize, (int)OptionManager.SceneSketchSize);

            if (m_listBoxSceneID.SelectedItem != null)
            {
                RefreshSceneSketch(GetCurrentMapID());
                if (m_fScale < 1.1f)
                {
                    this.m_pictureBoxSceneSketch.Left = 0;
                    this.m_pictureBoxSceneSketch.Top = 0;
                }
                else
                {
                    int iSizeChange = (int)((OptionManager.SceneSketchSize - fOldSize) / 2.0f);
                    this.m_pictureBoxSceneSketch.Left -= iSizeChange;
                    this.m_pictureBoxSceneSketch.Top -= iSizeChange;
                }
            }


            Cursor = Cursors.Default;
        }

        public bool SpeedMode
        {
            get
            {
                return this.启用加速模式ToolStripMenuItem.Checked;
            }
        }

        public bool bNpcHighLight
        {
            get
            {
                return m_bNpcHighLight;
            }
        }

        private void 启用加速模式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.启用加速模式ToolStripMenuItem.Checked = !this.启用加速模式ToolStripMenuItem.Checked;
            RefreshSceneSketchImage(GetCurrentMapID());
        }

        private void 任务奖励编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BaseDataEditForm.Instance.Text = "任务奖励编辑";
            BaseDataEditForm.Instance.ShowReadOnly(true);
            BaseDataEditForm.Instance.RefreshTypeList(MissionBaseDataManager.Instance.AwardList);
            BaseDataEditForm.Instance.Show();
        }

        private void 任务条件编辑ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BaseDataEditForm.Instance.Text = "任务条件编辑";
            BaseDataEditForm.Instance.ShowReadOnly(true);
            BaseDataEditForm.Instance.RefreshTypeList(MissionBaseDataManager.Instance.RequireList);
            BaseDataEditForm.Instance.Show();
        }

        private void 重新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MissionConditionManager.Instance.LoadFromXml(OptionManager.MissionBaseDataPath);
            MissionAwardManager.Instance.LoadFromXml(OptionManager.MissionBaseDataPath);

            m_TaskUIForm.InitDataBase();

            MissionBaseDataManager.Instance.Changed = false;
            MessageBox.Show("载入成功！");
        }

        private void 重新加载点音源信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PointSoundManager.Instance.LoadAll(OptionManager.PointSoundInfo);
            MessageBox.Show("加载完毕！");
            RefreshSceneSketch(GetCurrentMapID());
        }

        [System.Runtime.InteropServices.DllImportAttribute("gdi32.dll")]
        private static extern bool BitBlt(
            IntPtr hdcDest,
            int nXDest,
            int nYDest,
            int nWidth,
            int nHeight,
            IntPtr hdcSrc,
            int nXSrc,
            int nYSrc,
            System.Int32 dwRop
            );

        private void m_pictureBoxSceneSketch_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.F12)// 截屏
            {
                Bitmap bmp = new Bitmap(1024, 1024);
                Graphics g = Graphics.FromImage(bmp);
                foreach (Control ctrl in m_pictureBoxSceneSketch.Controls)
                {
                    if (ctrl.Visible)
                    {
                        BaseDrawControl baseDrawCtrl = ctrl as BaseDrawControl;
                        baseDrawCtrl.DrawOnBmp(g, bmp.Width, bmp.Height);
                    }
                }
                bmp.Save("scene.jpg");
            }
            if (e.KeyCode == Keys.F5)// 刷新
            {
                RefreshSceneSketch(GetCurrentMapID());
            }
            if (e.KeyCode == Keys.F1)// 地表碰撞
            {
                if (m_listBoxSceneID.SelectedItem != null)
                {
                    int iMapID = GetCurrentMapID();
                    SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(iMapID);
                    if (sceneSketch != null)
                    {
                        Cursor = Cursors.WaitCursor;
                        sceneSketch.ShowBlock = !sceneSketch.ShowBlock;
                        LayerManager.Instance.SetLayerVisiable(LayType.碰撞信息, sceneSketch.ShowBlock);
                        RefreshSceneSketchImage(iMapID);

                        Cursor = Cursors.Default;
                    }
                }
            }
            if (e.KeyCode == Keys.F2)// 路径权重
            {
                if (m_listBoxSceneID.SelectedItem != null)
                {
                    int iMapID = GetCurrentMapID();
                    SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(iMapID);
                    if (sceneSketch != null)
                    {
                        Cursor = Cursors.WaitCursor;
                        sceneSketch.ShowPath = !sceneSketch.ShowPath;
                        LayerManager.Instance.SetLayerVisiable(LayType.路径权重, sceneSketch.ShowPath);
                        RefreshSceneSketchImage(iMapID);

                        Cursor = Cursors.Default;
                    }
                }
            }
            if (this.路径编辑ToolStripMenuItem.Checked)
            {
                if (e.KeyCode == Keys.D1)// 野地笔刷
                {
                    PathPaintForm.Instance.CurrPen = 0;
                }
                if (e.KeyCode == Keys.D2)// 小路笔刷
                {
                    PathPaintForm.Instance.CurrPen = 1;
                }
                if (e.KeyCode == Keys.D3)// 大路笔刷
                {
                    PathPaintForm.Instance.CurrPen = 2;
                }
            }
        }

        private void m_pictureBoxSceneSketch_MouseUp(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                if (this.路径编辑ToolStripMenuItem.Checked)
                {
                    RefreshSceneSketchImage(GetCurrentMapID());
                }
            }
            else if(e.Button == MouseButtons.Right)
            {
                m_bNewNPCState = false;
            }
        }

        private void 导出任务报告ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportExportForm.Instance.Show();
        }

        private void NPC统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iMapID = GetCurrentMapID();
            if(iMapID == -1)
            {
                MessageBox.Show("请先选择地图");
                return;
            }

            String strNpcID = Microsoft.VisualBasic.Interaction.InputBox("请输入要统计NPC的ID", "输入", m_toolStripStatusLabelNpcShow.Text, 200, 300);
            this.ImeMode = ImeMode.Hangul;
            this.m_propertyGrid.ImeMode = ImeMode.Hangul;
            if(strNpcID.Length > 0)
            {
                int iNpcID = 0;
                if(int.TryParse(strNpcID, out iNpcID))
                {
                    RefreshNpcStatistics(iMapID, iNpcID);
                }
                else
                {
                    MessageBox.Show("输入的ID包含不合法字符");
                }
                
            }
        }

        public void RefreshNpcStatistics(int iMapID, int iNpcID)
        {
            int iOldID = int.Parse(m_toolStripStatusLabelNpcShow.Text); // 保留旧的ID
            m_toolStripStatusLabelNpcShow.Text = iNpcID.ToString(); ;
            m_toolStripStatusLabelNpcNum.Text = NPCManager.Instance.CalNpcNumByID(iMapID, iNpcID).ToString();

            // 细分处理
            String strNpcID = iNpcID.ToString();
            if(strNpcID.StartsWith("2") && strNpcID.Length == 6)
            {
                int iSub1 = NPCManager.Instance.CalNpcNumByID(iMapID, int.Parse(strNpcID + "1"));
                int iSub2 = NPCManager.Instance.CalNpcNumByID(iMapID, int.Parse(strNpcID + "2"));
                int iSub3 = NPCManager.Instance.CalNpcNumByID(iMapID, int.Parse(strNpcID + "3"));

                m_toolStripStatusLabelSubNpcShow1.Text = "(" + strNpcID + "1" + ":" + iSub1.ToString() + ")";
                m_toolStripStatusLabelSubNpcShow2.Text = "(" + strNpcID + "2" + ":" + iSub2.ToString() + ")";
                m_toolStripStatusLabelSubNpcShow3.Text = "(" + strNpcID + "3" + ":" + iSub3.ToString() + ")";
            }
            else
            {
                m_toolStripStatusLabelSubNpcShow1.Text = "(:)";
                m_toolStripStatusLabelSubNpcShow2.Text = "(:)";
                m_toolStripStatusLabelSubNpcShow3.Text = "(:)";
            }


            // 界面刷新
            RefreshNpcHighLight(iOldID);
        }

        private void m_toolStripSplitButtonNpcHighLight_ButtonClick(object sender, EventArgs e)
        {
            // 反置highlight属性
            m_bNpcHighLight = !m_bNpcHighLight;

            // UI调整
            if(m_bNpcHighLight)
            {
                m_toolStripSplitButtonNpcHighLight.Text = "取消高亮";
                m_toolStripSplitButtonNpcHighLight.ForeColor = Color.Red;
            }
            else
            {
                m_toolStripSplitButtonNpcHighLight.Text = "高亮显示";
                m_toolStripSplitButtonNpcHighLight.ForeColor = Color.Black;
            }

            // 界面刷新
            RefreshNpcHighLight(0);
        }

        public void RefreshNpcHighLight(int iOldID)
        {
            if (RegionToolForm.Instance.ActiveLayer == 10)
            {
                foreach (UserControl ctrl in m_pictureBoxSceneSketch.Controls)
                {
                    Type t = ctrl.GetType();
                    if (t.Name.Equals("NPCCreatorControl"))
                    {

                        NPCCreatorControl npc = ctrl as NPCCreatorControl;
                        if (npc.NPCCreator.Contains(StatisticsNpcID) > 0 || npc.NPCCreator.Contains(iOldID) > 0)
                        {
                            npc.Invalidate();
                        }
                    }
                }
            }
            else
            {
                RefreshSceneSketchImage(GetCurrentMapID());
            }
        }

        private void 导出任务存盘栏位规则ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_MissionManager.PutTaskIDToIndex(m_MissionManager.GetMissionIDList());
            System.IO.File.Copy("Data/task.xml", "Data/TaskIndex/task.xml", true);
            MessageBox.Show("成功导出到路径 Data/TaskIndex/ 下");
        }

        private void m_listBoxMO_DrawItem(object sender, DrawItemEventArgs e)
        {
            // 索引值小于0，直接返回
            if(e.Index < 0)
            {
                return;
            }

            // 渲染背景
            e.DrawBackground();

            // 渲染字体
            String strDiaName = m_listBoxMO.Items[e.Index].ToString();
            StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(strDiaName);
            String strMark = "×";
            if(dia != null)
            {
                strMark = dia.TeamWorkMark;
            }

            e.Graphics.DrawString(strMark + strDiaName, e.Font, Brushes.Black, e.Bounds);

            // 高亮选中
            e.DrawFocusRectangle();
        }

        private void 导入新状态图ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_strStateDiagramName == null || m_strStateDiagramName.Length == 0)
            {
                return;
            }
            else
            {
                // 获取状态图
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(m_strStateDiagramName);
                if(dia == null)
                {
                    MessageBox.Show("请先保存状态图！");
                }
                else
                {
                    // 导入
                    int iResult = TeamWorkManager.Instance.Import(dia);

                    // 刷新结果
                    if (iResult == -1)
                    {
                        MessageBox.Show("服务器正忙或路径配置错误。");
                    }
                    else if (iResult == 0)
                    {
                        MessageBox.Show("导入成功！");
                        dia.TeamWorkStatus = 1;
                    }
                    else
                    {
                        MessageBox.Show("状态图已存在！");
                        dia.TeamWorkStatus = iResult;
                    }

                    // 列表
                    m_listBoxMO.Invalidate();
                }
                
            }
        }

        private void 签出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_strStateDiagramName == null || m_strStateDiagramName.Length == 0)
            {
                return;
            }
            else
            {
                // 获取状态图
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(m_strStateDiagramName);
                if (dia == null)
                {
                    MessageBox.Show("请先保存状态图！");
                }
                else
                {
                    int iresult = TeamWorkManager.Instance.CheckOut(m_strStateDiagramName);

                    if (iresult == -1)
                    {
                        MessageBox.Show("服务器正忙或路径配置错误。");
                    }
                    else if (iresult == 0)
                    {
                        MessageBox.Show("状态图不存在！请先导入状态图");
                        dia.TeamWorkStatus = iresult;
                    }
                    else if (iresult == 3)
                    {
                        String strLocker = TeamWorkManager.Instance.GetDiagramStatus(m_strStateDiagramName).Msg;
                        MessageBox.Show("状态图已被" + strLocker + "锁住");
                        dia.TeamWorkStatus = iresult;
                    }
                    else
                    {
                        dia.TeamWorkStatus = 2;
                        UpdateLatestStateDiagramVersion();
                    }

                    // 列表
                    m_listBoxMO.Invalidate();
                }
            }
            

        }

        private void 撤销更改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_strStateDiagramName == null || m_strStateDiagramName.Length == 0)
            {
                return;
            }
            else
            {
                // 获取状态图
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(m_strStateDiagramName);
                if (dia == null)
                {
                    MessageBox.Show("请先保存状态图！");
                }
                else
                {
                    int iresult = TeamWorkManager.Instance.Repeal(m_strStateDiagramName);

                    if (iresult == -1)
                    {
                        MessageBox.Show("服务器正忙或路径配置错误。");
                    }
                    else if (iresult == 0)
                    {
                        MessageBox.Show("状态图不存在！请先导入状态图");
                        dia.TeamWorkStatus = iresult;
                    }
                    else if(iresult == 1)
                    {
                        MessageBox.Show("请先签出状态图");
                        dia.TeamWorkStatus = iresult;
                    }
                    else if (iresult == 3)
                    {
                        String strLocker = TeamWorkManager.Instance.GetDiagramStatus(m_strStateDiagramName).Msg;
                        MessageBox.Show("状态图已被" + strLocker + "锁住");
                        dia.TeamWorkStatus = iresult;
                    }
                    else
                    {
                        dia.TeamWorkStatus = 1;
                        UpdateLatestStateDiagramVersion();
                    }

                    // 列表
                    m_listBoxMO.Invalidate();
                }
            }
        }

        private void 获取最新版本ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_strStateDiagramName == null || m_strStateDiagramName.Length == 0)
            {
                return;
            }
            else
            {
                // 获取状态图
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(m_strStateDiagramName);
                if (dia == null)
                {
                    MessageBox.Show("请先保存状态图！");
                }
                else
                {
                    // 更新
                    bool bSuccess = UpdateLatestStateDiagramVersion();

                    // 列表
                    m_listBoxMO.Invalidate();

                    // 提示信息
                    if(bSuccess)
                    {
                        MessageBox.Show("获取最新版本成功！");
                    }
                    else
                    {
                        MessageBox.Show("请先导入状态图！");
                    }
                    
                }
            }


        }

        private void 签入ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_strStateDiagramName == null || m_strStateDiagramName.Length == 0)
            {
                return;
            }
            else
            {
                // 获取状态图
                StateDiagram dia = StateDiagramManager.Instance.GetStateDiagram(m_strStateDiagramName);
                if (dia == null)
                {
                    MessageBox.Show("请先保存状态图！");
                }
                else
                {
                    int iresult = TeamWorkManager.Instance.CheckIn(dia);

                    if (iresult == -1)
                    {
                        MessageBox.Show("服务器正忙或路径配置错误。");
                    }
                    else if (iresult == 0)
                    {
                        MessageBox.Show("状态图不存在！请先导入状态图");
                        dia.TeamWorkStatus = iresult;
                    }
                    else if (iresult == 1)
                    {
                        MessageBox.Show("请先签出状态图");
                        dia.TeamWorkStatus = iresult;
                    }
                    else if (iresult == 3)
                    {
                        String strLocker = TeamWorkManager.Instance.GetDiagramStatus(m_strStateDiagramName).Msg;
                        MessageBox.Show("状态图已被" + strLocker + "锁住");
                        dia.TeamWorkStatus = iresult;
                    }
                    else
                    {
                        dia.TeamWorkStatus = 1;
                    }

                    // 列表
                    m_listBoxMO.Invalidate();
                }
            }
        }

        private bool UpdateLatestStateDiagramVersion()
        {
            // 更新状态图
            if (OnEditStateDiagram != null)
            {
                StateDiagram latestDia = TeamWorkManager.Instance.GetLatestVersion(m_strStateDiagramName);
                if (latestDia != null)
                {
                    // 内容更新, 注意签出的状态图不更新
                    if(OnEditStateDiagram.TeamWorkStatus != 2)
                    {
                        OnEditStateDiagram.Copy(latestDia);
                    }
                    

                    // 状态图按任务筛选
                    if (m_MOFilter.SelectedItem.ToString().Equals("全部"))
                    {
                        ShowStateDiagram(OnEditStateDiagram);
                    }
                    else
                    {
                        int iMissionID = ExtractMissionIDFromDisplayText(m_MOFilter.SelectedItem.ToString());
                        ShowStateDiagram(OnEditStateDiagram, iMissionID);
                    }

                    // 根节点检测
                    RejudgeStateDiagramConnectToRoot();

                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        private void 打开所在文件夹ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (StateDiagramChanged)
            {
                // 弹出是否保存当前状态图对话框
                DialogResult result = MessageBox.Show("当前状态图已被改变，是否保存？", "警告", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    保存当前状态图ToolStripMenuItem_Click(sender, e);
                }

                StateDiagramChanged = false;
            }

            // 转换状态图到 Lua 脚本
            if (OnEditStateDiagram != null)
            {
                String strFileName = LuaScriptConvertor.Convert(OnEditStateDiagram);
                // 保存 StringRes.xml
                DialogManager.Instance.SaveToXml(OptionManager.StringResourcePath);

                // 打开文件夹
                String path = Application.StartupPath + "\\Data\\Lua\\" + strFileName;
                System.Diagnostics.Process.Start("explorer", @"/select," + path);
            }
        }

        private void 获取最新任务数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("确认要从服务器上获取最新版本么？这可能会花一定的时间。", "提示", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
            {
                return;
            }

            ArrayList latestList = TeamWorkManager.Instance.GetLatestMissionList();
            if(latestList == null)
            {
                MessageBox.Show("服务器错误，请检查服务器路径是否设置正确！");
            }
            else
            {
                foreach(Mission latest in latestList)
                {
                    // 找到本地任务
                    Mission local = m_MissionManager.FindByID(latest.ID);

                    // 没有找到的直接更新
                    if(local == null)
                    {
                        local = new Mission();
                        local.Copy(latest);
                        local.TeamWorkStatus = latest.TeamWorkStatus;
                        m_MissionManager.MissionList.Add(local);
                    }
                    else
                    {
                        if(latest.TeamWorkStatus != 2)
                        {
                            local.Copy(latest);
                            local.TeamWorkStatus = latest.TeamWorkStatus;
                        }
                    }
                }

                m_MissionManager.SaveMissions(OptionManager.MissionPath);
                RefreshMissionTree();
                MessageBox.Show("任务已更新，请点击<筛选>按钮刷新任务列表！");
            }
        }

        private void 获取最新状态图数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("确认要从服务器上获取最新版本么？这可能会花一定的时间。", "提示", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
            {
                return;
            }

            ArrayList latestList = TeamWorkManager.Instance.GetLatestDiagramList();
            if (latestList == null)
            {
                MessageBox.Show("服务器错误，请检查服务器路径是否设置正确！");
            }
            else
            {
                foreach (StateDiagram latest in latestList)
                {
                    // 找到本地任务
                    StateDiagram local = StateDiagramManager.Instance.GetStateDiagram(latest.Name);

                    // 没有找到的直接更新
                    if (local == null)
                    {
                        local = new StateDiagram();
                        local.Copy(latest);
                        local.TeamWorkStatus = latest.TeamWorkStatus;
                        StateDiagramManager.Instance.SetDiagram(local.Name, local);
                    }
                    else
                    {
                        if (latest.TeamWorkStatus != 2)
                        {
                            local.Copy(latest);
                            local.TeamWorkStatus = latest.TeamWorkStatus;
                        }
                    }

                    StateDiagramManager.SaveStateDiagram(local);
                }

                
                RefreshMissionTree();
                MessageBox.Show("状态图更新完毕！");
            }
        }
    }
}