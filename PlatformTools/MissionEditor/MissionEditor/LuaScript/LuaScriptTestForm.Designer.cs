﻿namespace MissionEditor
{
    partial class LuaScriptTestForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_propertyGridPlayer = new System.Windows.Forms.PropertyGrid();
            this.m_splitContainer = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_listBoxOptions = new System.Windows.Forms.ListBox();
            this.m_webBrowserDialog = new System.Windows.Forms.WebBrowser();
            this.m_tabControlItems = new System.Windows.Forms.TabControl();
            this.m_tabPageItem = new System.Windows.Forms.TabPage();
            this.m_btnRemoveItem = new System.Windows.Forms.Button();
            this.m_btnAddItem = new System.Windows.Forms.Button();
            this.m_textBoxItemID = new System.Windows.Forms.TextBox();
            this.m_listBoxItems = new System.Windows.Forms.ListBox();
            this.m_btnUse = new System.Windows.Forms.Button();
            this.m_tabPageCounter = new System.Windows.Forms.TabPage();
            this.m_btnAddCounterValue = new System.Windows.Forms.Button();
            this.m_textBoxCounterValue = new System.Windows.Forms.TextBox();
            this.m_listBoxCounter = new System.Windows.Forms.ListBox();
            this.m_tabPageMission = new System.Windows.Forms.TabPage();
            this.m_comboBoxMissionState = new System.Windows.Forms.ComboBox();
            this.m_btnSetMissionState = new System.Windows.Forms.Button();
            this.m_textBoxMissionID = new System.Windows.Forms.TextBox();
            this.m_listBoxMission = new System.Windows.Forms.ListBox();
            this.m_tabPageFollowing = new System.Windows.Forms.TabPage();
            this.m_listBoxFollowing = new System.Windows.Forms.ListBox();
            this.m_textBoxOutput = new System.Windows.Forms.TextBox();
            this.m_splitContainer.Panel1.SuspendLayout();
            this.m_splitContainer.Panel2.SuspendLayout();
            this.m_splitContainer.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.m_tabControlItems.SuspendLayout();
            this.m_tabPageItem.SuspendLayout();
            this.m_tabPageCounter.SuspendLayout();
            this.m_tabPageMission.SuspendLayout();
            this.m_tabPageFollowing.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_propertyGridPlayer
            // 
            this.m_propertyGridPlayer.Dock = System.Windows.Forms.DockStyle.Right;
            this.m_propertyGridPlayer.Location = new System.Drawing.Point(590, 0);
            this.m_propertyGridPlayer.Name = "m_propertyGridPlayer";
            this.m_propertyGridPlayer.Size = new System.Drawing.Size(133, 566);
            this.m_propertyGridPlayer.TabIndex = 0;
            // 
            // m_splitContainer
            // 
            this.m_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainer.Location = new System.Drawing.Point(0, 0);
            this.m_splitContainer.Name = "m_splitContainer";
            this.m_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // m_splitContainer.Panel1
            // 
            this.m_splitContainer.Panel1.Controls.Add(this.splitContainer1);
            // 
            // m_splitContainer.Panel2
            // 
            this.m_splitContainer.Panel2.Controls.Add(this.m_textBoxOutput);
            this.m_splitContainer.Size = new System.Drawing.Size(590, 566);
            this.m_splitContainer.SplitterDistance = 429;
            this.m_splitContainer.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.m_btnOK);
            this.splitContainer1.Panel1.Controls.Add(this.m_listBoxOptions);
            this.splitContainer1.Panel1.Controls.Add(this.m_webBrowserDialog);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.m_tabControlItems);
            this.splitContainer1.Size = new System.Drawing.Size(590, 429);
            this.splitContainer1.SplitterDistance = 246;
            this.splitContainer1.TabIndex = 2;
            // 
            // m_btnOK
            // 
            this.m_btnOK.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_btnOK.Location = new System.Drawing.Point(0, 397);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(246, 32);
            this.m_btnOK.TabIndex = 3;
            this.m_btnOK.Text = "确定";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_listBoxOptions
            // 
            this.m_listBoxOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listBoxOptions.FormattingEnabled = true;
            this.m_listBoxOptions.ItemHeight = 12;
            this.m_listBoxOptions.Location = new System.Drawing.Point(0, 128);
            this.m_listBoxOptions.Name = "m_listBoxOptions";
            this.m_listBoxOptions.Size = new System.Drawing.Size(243, 268);
            this.m_listBoxOptions.TabIndex = 2;
            // 
            // m_webBrowserDialog
            // 
            this.m_webBrowserDialog.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_webBrowserDialog.Location = new System.Drawing.Point(0, 0);
            this.m_webBrowserDialog.MinimumSize = new System.Drawing.Size(20, 20);
            this.m_webBrowserDialog.Name = "m_webBrowserDialog";
            this.m_webBrowserDialog.Size = new System.Drawing.Size(246, 122);
            this.m_webBrowserDialog.TabIndex = 1;
            // 
            // m_tabControlItems
            // 
            this.m_tabControlItems.Controls.Add(this.m_tabPageItem);
            this.m_tabControlItems.Controls.Add(this.m_tabPageCounter);
            this.m_tabControlItems.Controls.Add(this.m_tabPageMission);
            this.m_tabControlItems.Controls.Add(this.m_tabPageFollowing);
            this.m_tabControlItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_tabControlItems.Location = new System.Drawing.Point(0, 0);
            this.m_tabControlItems.Name = "m_tabControlItems";
            this.m_tabControlItems.SelectedIndex = 0;
            this.m_tabControlItems.Size = new System.Drawing.Size(340, 429);
            this.m_tabControlItems.TabIndex = 3;
            // 
            // m_tabPageItem
            // 
            this.m_tabPageItem.Controls.Add(this.m_btnRemoveItem);
            this.m_tabPageItem.Controls.Add(this.m_btnAddItem);
            this.m_tabPageItem.Controls.Add(this.m_textBoxItemID);
            this.m_tabPageItem.Controls.Add(this.m_listBoxItems);
            this.m_tabPageItem.Controls.Add(this.m_btnUse);
            this.m_tabPageItem.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageItem.Name = "m_tabPageItem";
            this.m_tabPageItem.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageItem.Size = new System.Drawing.Size(332, 404);
            this.m_tabPageItem.TabIndex = 0;
            this.m_tabPageItem.Text = "物品";
            this.m_tabPageItem.UseVisualStyleBackColor = true;
            // 
            // m_btnRemoveItem
            // 
            this.m_btnRemoveItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_btnRemoveItem.Location = new System.Drawing.Point(77, 371);
            this.m_btnRemoveItem.Name = "m_btnRemoveItem";
            this.m_btnRemoveItem.Size = new System.Drawing.Size(68, 27);
            this.m_btnRemoveItem.TabIndex = 6;
            this.m_btnRemoveItem.Text = "删除";
            this.m_btnRemoveItem.UseVisualStyleBackColor = true;
            this.m_btnRemoveItem.Click += new System.EventHandler(this.m_btnRemoveItem_Click);
            // 
            // m_btnAddItem
            // 
            this.m_btnAddItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_btnAddItem.Location = new System.Drawing.Point(159, 370);
            this.m_btnAddItem.Name = "m_btnAddItem";
            this.m_btnAddItem.Size = new System.Drawing.Size(84, 27);
            this.m_btnAddItem.TabIndex = 5;
            this.m_btnAddItem.Text = "添加物品";
            this.m_btnAddItem.UseVisualStyleBackColor = true;
            this.m_btnAddItem.Click += new System.EventHandler(this.m_btnAddItem_Click);
            // 
            // m_textBoxItemID
            // 
            this.m_textBoxItemID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_textBoxItemID.Location = new System.Drawing.Point(249, 374);
            this.m_textBoxItemID.Name = "m_textBoxItemID";
            this.m_textBoxItemID.Size = new System.Drawing.Size(62, 21);
            this.m_textBoxItemID.TabIndex = 4;
            this.m_textBoxItemID.Text = "1";
            // 
            // m_listBoxItems
            // 
            this.m_listBoxItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listBoxItems.FormattingEnabled = true;
            this.m_listBoxItems.ItemHeight = 12;
            this.m_listBoxItems.Location = new System.Drawing.Point(6, 0);
            this.m_listBoxItems.Name = "m_listBoxItems";
            this.m_listBoxItems.Size = new System.Drawing.Size(320, 364);
            this.m_listBoxItems.TabIndex = 0;
            // 
            // m_btnUse
            // 
            this.m_btnUse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_btnUse.Location = new System.Drawing.Point(3, 371);
            this.m_btnUse.Name = "m_btnUse";
            this.m_btnUse.Size = new System.Drawing.Size(68, 27);
            this.m_btnUse.TabIndex = 2;
            this.m_btnUse.Text = "使用";
            this.m_btnUse.UseVisualStyleBackColor = true;
            this.m_btnUse.Click += new System.EventHandler(this.m_btnUse_Click);
            // 
            // m_tabPageCounter
            // 
            this.m_tabPageCounter.Controls.Add(this.m_btnAddCounterValue);
            this.m_tabPageCounter.Controls.Add(this.m_textBoxCounterValue);
            this.m_tabPageCounter.Controls.Add(this.m_listBoxCounter);
            this.m_tabPageCounter.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageCounter.Name = "m_tabPageCounter";
            this.m_tabPageCounter.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageCounter.Size = new System.Drawing.Size(332, 404);
            this.m_tabPageCounter.TabIndex = 1;
            this.m_tabPageCounter.Text = "计数器";
            this.m_tabPageCounter.UseVisualStyleBackColor = true;
            // 
            // m_btnAddCounterValue
            // 
            this.m_btnAddCounterValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_btnAddCounterValue.Location = new System.Drawing.Point(86, 374);
            this.m_btnAddCounterValue.Name = "m_btnAddCounterValue";
            this.m_btnAddCounterValue.Size = new System.Drawing.Size(84, 27);
            this.m_btnAddCounterValue.TabIndex = 3;
            this.m_btnAddCounterValue.Text = "增加计数值";
            this.m_btnAddCounterValue.UseVisualStyleBackColor = true;
            this.m_btnAddCounterValue.Click += new System.EventHandler(this.m_btnAddCounterValue_Click);
            // 
            // m_textBoxCounterValue
            // 
            this.m_textBoxCounterValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_textBoxCounterValue.Location = new System.Drawing.Point(176, 378);
            this.m_textBoxCounterValue.Name = "m_textBoxCounterValue";
            this.m_textBoxCounterValue.Size = new System.Drawing.Size(62, 21);
            this.m_textBoxCounterValue.TabIndex = 2;
            this.m_textBoxCounterValue.Text = "1";
            // 
            // m_listBoxCounter
            // 
            this.m_listBoxCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listBoxCounter.FormattingEnabled = true;
            this.m_listBoxCounter.ItemHeight = 12;
            this.m_listBoxCounter.Location = new System.Drawing.Point(6, 0);
            this.m_listBoxCounter.Name = "m_listBoxCounter";
            this.m_listBoxCounter.Size = new System.Drawing.Size(320, 364);
            this.m_listBoxCounter.TabIndex = 1;
            // 
            // m_tabPageMission
            // 
            this.m_tabPageMission.Controls.Add(this.m_comboBoxMissionState);
            this.m_tabPageMission.Controls.Add(this.m_btnSetMissionState);
            this.m_tabPageMission.Controls.Add(this.m_textBoxMissionID);
            this.m_tabPageMission.Controls.Add(this.m_listBoxMission);
            this.m_tabPageMission.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageMission.Name = "m_tabPageMission";
            this.m_tabPageMission.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageMission.Size = new System.Drawing.Size(332, 404);
            this.m_tabPageMission.TabIndex = 2;
            this.m_tabPageMission.Text = "任务";
            this.m_tabPageMission.UseVisualStyleBackColor = true;
            // 
            // m_comboBoxMissionState
            // 
            this.m_comboBoxMissionState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_comboBoxMissionState.FormattingEnabled = true;
            this.m_comboBoxMissionState.Location = new System.Drawing.Point(173, 376);
            this.m_comboBoxMissionState.Name = "m_comboBoxMissionState";
            this.m_comboBoxMissionState.Size = new System.Drawing.Size(152, 20);
            this.m_comboBoxMissionState.TabIndex = 3;
            // 
            // m_btnSetMissionState
            // 
            this.m_btnSetMissionState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_btnSetMissionState.Location = new System.Drawing.Point(6, 374);
            this.m_btnSetMissionState.Name = "m_btnSetMissionState";
            this.m_btnSetMissionState.Size = new System.Drawing.Size(95, 24);
            this.m_btnSetMissionState.TabIndex = 2;
            this.m_btnSetMissionState.Text = "设置任务状态";
            this.m_btnSetMissionState.UseVisualStyleBackColor = true;
            this.m_btnSetMissionState.Click += new System.EventHandler(this.m_btnSetMissionState_Click);
            // 
            // m_textBoxMissionID
            // 
            this.m_textBoxMissionID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_textBoxMissionID.Location = new System.Drawing.Point(107, 376);
            this.m_textBoxMissionID.Name = "m_textBoxMissionID";
            this.m_textBoxMissionID.Size = new System.Drawing.Size(60, 21);
            this.m_textBoxMissionID.TabIndex = 1;
            // 
            // m_listBoxMission
            // 
            this.m_listBoxMission.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_listBoxMission.FormattingEnabled = true;
            this.m_listBoxMission.ItemHeight = 12;
            this.m_listBoxMission.Location = new System.Drawing.Point(3, 6);
            this.m_listBoxMission.Name = "m_listBoxMission";
            this.m_listBoxMission.Size = new System.Drawing.Size(323, 352);
            this.m_listBoxMission.TabIndex = 0;
            // 
            // m_tabPageFollowing
            // 
            this.m_tabPageFollowing.Controls.Add(this.m_listBoxFollowing);
            this.m_tabPageFollowing.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageFollowing.Name = "m_tabPageFollowing";
            this.m_tabPageFollowing.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageFollowing.Size = new System.Drawing.Size(332, 404);
            this.m_tabPageFollowing.TabIndex = 3;
            this.m_tabPageFollowing.Text = "跟随";
            this.m_tabPageFollowing.UseVisualStyleBackColor = true;
            // 
            // m_listBoxFollowing
            // 
            this.m_listBoxFollowing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_listBoxFollowing.FormattingEnabled = true;
            this.m_listBoxFollowing.ItemHeight = 12;
            this.m_listBoxFollowing.Location = new System.Drawing.Point(3, 3);
            this.m_listBoxFollowing.Name = "m_listBoxFollowing";
            this.m_listBoxFollowing.Size = new System.Drawing.Size(326, 388);
            this.m_listBoxFollowing.TabIndex = 0;
            // 
            // m_textBoxOutput
            // 
            this.m_textBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_textBoxOutput.Location = new System.Drawing.Point(0, 0);
            this.m_textBoxOutput.Multiline = true;
            this.m_textBoxOutput.Name = "m_textBoxOutput";
            this.m_textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_textBoxOutput.Size = new System.Drawing.Size(590, 133);
            this.m_textBoxOutput.TabIndex = 0;
            // 
            // LuaScriptTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 566);
            this.Controls.Add(this.m_splitContainer);
            this.Controls.Add(this.m_propertyGridPlayer);
            this.Name = "LuaScriptTestForm";
            this.Text = "任务脚本测试环境";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.LuaScriptTestForm_Load);
            this.m_splitContainer.Panel1.ResumeLayout(false);
            this.m_splitContainer.Panel2.ResumeLayout(false);
            this.m_splitContainer.Panel2.PerformLayout();
            this.m_splitContainer.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.m_tabControlItems.ResumeLayout(false);
            this.m_tabPageItem.ResumeLayout(false);
            this.m_tabPageItem.PerformLayout();
            this.m_tabPageCounter.ResumeLayout(false);
            this.m_tabPageCounter.PerformLayout();
            this.m_tabPageMission.ResumeLayout(false);
            this.m_tabPageMission.PerformLayout();
            this.m_tabPageFollowing.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PropertyGrid m_propertyGridPlayer;
        private LuaInterface.Lua m_LuaVM = new LuaInterface.Lua();
        private System.Windows.Forms.SplitContainer m_splitContainer;
        private System.Windows.Forms.TextBox m_textBoxOutput;
        private System.Windows.Forms.WebBrowser m_webBrowserDialog;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.ListBox m_listBoxOptions;
        private static LuaScriptTestForm m_Instance = null;
        private StateDiagramScript m_OnRunningScript = null;
        private System.Windows.Forms.ListBox m_listBoxCounter;
        private System.Windows.Forms.ListBox m_listBoxItems;
        private System.Windows.Forms.Button m_btnUse;
        private System.Windows.Forms.TabControl m_tabControlItems;
        private System.Windows.Forms.TabPage m_tabPageItem;
        private System.Windows.Forms.TabPage m_tabPageCounter;
        private System.Windows.Forms.TabPage m_tabPageMission;
        private System.Windows.Forms.Button m_btnAddCounterValue;
        private System.Windows.Forms.TextBox m_textBoxCounterValue;
        private System.Windows.Forms.Button m_btnAddItem;
        private System.Windows.Forms.TextBox m_textBoxItemID;
        private System.Windows.Forms.ListBox m_listBoxMission;
        private System.Windows.Forms.ComboBox m_comboBoxMissionState;
        private System.Windows.Forms.Button m_btnSetMissionState;
        private System.Windows.Forms.TextBox m_textBoxMissionID;
        private System.Windows.Forms.TabPage m_tabPageFollowing;
        private System.Windows.Forms.ListBox m_listBoxFollowing;
        private System.Windows.Forms.Button m_btnRemoveItem;
    }
}