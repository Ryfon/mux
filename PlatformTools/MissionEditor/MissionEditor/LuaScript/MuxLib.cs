﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    // MuxLib 提供的脚本接口
    class MuxLib
    {
        // 供 Lua 脚本调用的扩展接口
        // 显示消息, 参数为 String ID
        public void NpcShowMsg(String strID)
        {
            try
            {
                int iStrID = int.Parse(strID.Substring(1));
                StringRes strRes = DialogManager.Instance.GetStringRes(iStrID);

                if (strRes != null)
                {
                    LuaScriptTestForm.Instance.SetDialogString(strRes);
                }

            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        #region UI相关
        // 设置选项. 选项对应值
        public void NpcSetOption(int iIdx, String strID)
        {
            int iStrID = int.Parse(strID.Substring(1));
            String strRes = DialogManager.Instance.GetStringRes(iStrID).MainString;
            LuaScriptTestForm.Instance.AddOption(iIdx, strRes);
        }

        // 显示对话 UI 
        public void NpcSendUiInfo()
        {

        }

        // 关闭对话 UI 
        public void NpcCloseUiInfo()
        {
            LuaScriptTestForm.Instance.ClearDialogUI();
        }
        
        // 检查转换 
        public void NpcCheckTransCon()
        {
            LuaScriptTestForm.Instance.OnRunnerScript.CheckTransCon();
        }
        #endregion

        #region Player属性相关


        // 查询玩家等级
        public bool NpcQueryLevelMoreThan(int iLevel)
        {
            return (Player.Instance.Level >= iLevel);
        }
        
        // 给玩家金钱
        public void NpcAddMoney(int iMoney)
        {
            Player.Instance.Money += iMoney;
            LuaScriptTestForm.Instance.MessageText =
                "AddMoney " + iMoney.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }

        // 给玩家增加经验
        public void NpcAddExp(int iExp)
        {
            Player.Instance.Exp += iExp;
            LuaScriptTestForm.Instance.MessageText =
                "AddExp " + iExp.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }

        // 查询玩家的智力是否大于等于 iValue
        public bool NpcQueryIntelligenceMoreThan(int iValue)
        {
            return (Player.Instance.Intelligence >= iValue);
        }

        // 查询玩家的智力是否小于 iValue
        public bool NpcQueryIntelligenceLessThan(int iValue)
        {
            return (Player.Instance.Intelligence < iValue);
        }

        // 增加属性
        public void NpcAddProperty(int iProp, int iValue)
        {

        }

        // 设置属性
        public void NpcSetProperty(int iProp, int iValue)
        {

        }

        #endregion

        #region 玩家物件相关

        // 生成一个跟随玩家的 npc
        public void NpcCreatePlayerNpc(int iNpcID)
        {
            Player.Instance.CreateFollowing(iNpcID);
            LuaScriptTestForm.Instance.MessageText =
                "CreatePlayerNpc " + iNpcID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }

        // 检查是否有 npc 跟随
        public bool NpcCheckPlayerNpc(int iNpcID)
        {
            foreach (int iID in Player.Instance.FollowingList)
            {
                if (iID == iNpcID)
                {
                    return true;
                }
            }
            return false;
        }

        // 删除跟随 npc
        public void NpcDelPlayerNpc(int iNpcID)
        {
            Player.Instance.DelFollowing(iNpcID);
            LuaScriptTestForm.Instance.MessageText =
                "DelPlayerNpc " + iNpcID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }

        // 给玩家道具
        public void NpcGiveItem(int iItemID, int iNum)
        {
            for (int i = 0; i < iNum; i++)
            {
                Player.Instance.GetItem(iItemID);
            }

            LuaScriptTestForm.Instance.MessageText =
                "GiveItem " + iItemID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }

        // 从玩家身上移除道具
        public void NpcRemoveItem(int iItemID)
        {
            Player.Instance.RemoveItem(iItemID);
            LuaScriptTestForm.Instance.MessageText =
                "RemoveItem " + iItemID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }

        // 查询玩家是否有道具
        public bool NpcQueryPackageHave(int iItemID)
        {
            return Player.Instance.HaveItem(iItemID);
        }

        public bool NpcQueryPackageHaveNot(int iItemID)
        {
            return Player.Instance.HaveNotItem(iItemID);
        }

        #endregion      
  
        #region 任务相关

        // 给玩家一个任务
        public void NpcGiveTask(int iMissionID)
        {
            Player.Instance.SetMissionState(iMissionID, Player.MissionState.MS_NOT_ACHIEVE_TARGET);
            LuaScriptTestForm.Instance.MessageText =
                "GiveTask " + iMissionID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }

        // 执行任务
        public void NpcProcessTask(int iMissionID)
        {
            Player.Instance.SetMissionState(iMissionID, Player.MissionState.MS_ACHIEVE_TARGET);
            LuaScriptTestForm.Instance.MessageText =
                "ProcessTask " + iMissionID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }

        // 完成任务
        public void NpcFinishTask(int iMissionID)
        {
            Player.Instance.SetMissionState(iMissionID, Player.MissionState.MS_FINISHED);
            LuaScriptTestForm.Instance.MessageText =
                "FinishTask " + iMissionID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
        }


        // 给玩家增加计数器
        // (CounterType(0-杀怪 1-物品 2-杀玩家 3-交友), DataTypeBegin, DataTypeEnd, Value, MissionID)
        public void NpcAddPlayerCounter(int iCounterType, int iTargetStart, int iTargetEnd, int iCount, int iMissionID)
        {
            if (iTargetStart<0 || iTargetEnd<0)
            {
                MessageBox.Show("脚本中计数器参数不正确. Mission ID: "+iMissionID.ToString());
                return;
            }

            Player.Instance.AddCounter(iCounterType, iTargetStart, iTargetEnd, iCount, iMissionID);
        }

        // 检查是否从未接过任务
        public bool NpcQueryNeverAcceptMission(int iMissionID)
        {
            Player.MissionState state = Player.Instance.GetMissionState(iMissionID);
            return (state == Player.MissionState.MS_NEVER_ACCEPTED);
        }

        // 检查任务是否不在玩家任务列表
        public bool NpcQueryIsMissionNotOnList(int iMissionID)
        {
            Player.MissionState state = Player.Instance.GetMissionState(iMissionID);
            return (state == Player.MissionState.MS_NEVER_ACCEPTED 
                        || state == Player.MissionState.MS_FINISHED);
        }

        // 检查任务是否在任务列表
        public bool NpcQueryOnMissionList(int iMissionID)
        {
            Player.MissionState state = Player.Instance.GetMissionState(iMissionID);
            return (state == Player.MissionState.MS_ACHIEVE_TARGET
                        || state == Player.MissionState.MS_NOT_ACHIEVE_TARGET);
        }

        // 检查玩家之前是否完成过任务
        public bool NpcQueryIsFinishMissionBefore(int iMissionID)
        {
            Player.MissionState state = Player.Instance.GetMissionState(iMissionID);
            return (state == Player.MissionState.MS_FINISHED);
        }

        // 查询 player 是否没有达到任务 iMissionID 的目标
        public bool NpcQueryNotAchieveMissionTarget(int iMissionID)
        {
            Player.MissionState state = Player.Instance.GetMissionState(iMissionID);
            return (state == Player.MissionState.MS_NOT_ACHIEVE_TARGET);
        }

        // 查询 player 是否达到任务 iMissionID 的目标
        public bool NpcQueryAchieveMissionTarget(int iMissionID)
        {
            Player.MissionState state = Player.Instance.GetMissionState(iMissionID);
            return (state == Player.MissionState.MS_ACHIEVE_TARGET);
        }

        #endregion

        public static MuxLib Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new MuxLib();
                }
                return m_Instance;
            }
        }

        private static MuxLib m_Instance = null;
    }
}
