﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace MissionEditor
{
    // 从状态图转换到 Lua 脚本转换器
    public class LuaScriptConvertor
    {
        // 转换参数中的状态图到同名 Lua 脚本
        public static String Convert(StateDiagram dia)
        {
            m_strType = dia.Name.Substring(0, dia.Name.IndexOf("_"));
            m_iTargetID = int.Parse(dia.Name.Substring(dia.Name.IndexOf("_")+1));
            String strScriptFileName = "";

            if (m_strType.Equals("NPC"))
            {
                m_eDialogType = StateDiagram.OwnerType.DT_NPC;
                m_iStringIDPrefix = m_iTargetID; // DialogManager.GetDialogIDPrefix(m_eDialogType, m_iTargetID);
                m_StringResList = new Hashtable();
                DialogManager.Instance.NPCDialogList[m_iStringIDPrefix] = m_StringResList;
                strScriptFileName += "npc" + dia.ID + ".lua";
                
            }
            else if (m_strType.Equals("ITEM"))
            {
                m_eDialogType = StateDiagram.OwnerType.DT_ITEM;
                m_iStringIDPrefix = m_iTargetID; // DialogManager.GetDialogIDPrefix(m_eDialogType, m_iTargetID);
                m_StringResList = new Hashtable();
                DialogManager.Instance.NPCDialogList[m_iStringIDPrefix] = m_StringResList;
                strScriptFileName += "item" + dia.ID + ".lua";
            }
            else if (m_strType.Equals("DEATH"))
            {
                // m_iTargetID 为 5 位，说明是 NPC. 超过 5 位为 Monster
                if (m_iTargetID.ToString().Length == 5)
                {
                    m_eDialogType = StateDiagram.OwnerType.DT_NPC_DEATH;
                }
                else 
                {
                    m_eDialogType = StateDiagram.OwnerType.DT_MONSTER_DEATH;
                }
                m_iStringIDPrefix = m_iTargetID; // DialogManager.GetDialogIDPrefix(m_eDialogType, m_iTargetID);
                m_StringResList = new Hashtable();
                DialogManager.Instance.NPCDialogList[m_iStringIDPrefix] = m_StringResList;
                strScriptFileName += "death" + dia.ID + ".lua";
            }
            else if (m_strType.Equals("AREA"))
            {
                m_eDialogType = StateDiagram.OwnerType.DT_AREA;
                m_StringResList = new Hashtable();
                DialogManager.Instance.NPCDialogList[m_iTargetID] = m_StringResList;
                strScriptFileName += "area" + dia.ID + ".lua";
            }

            String strLuaScript = "";
            strLuaScript += "--" + dia.Name + " \n";
            strLuaScript += "nScriptID = " + dia.ID.ToString() + "\n";
            strLuaScript += "states, start = {}, 1 \n";

            // 添加 NpcCloseUiInfo 状态
            //int iCloseUIStateID = dia.GetMaxStateID() + 1;
            //State closeUIState = new State(iCloseUIStateID, "CloseUIState", new System.Drawing.Point());
            //closeUIState.ActionListOnEnter[AtomActionManager.Display_CloseUIInfo] = "";
            //strLuaScript += "\n";
            //strLuaScript += "--自动添加的用来关闭 UI 的状态脚本\n";
            //strLuaScript += ConvertState(dia, closeUIState);

            // 遍历生成每一个state
            foreach (State state in dia.StateList)
            {
                strLuaScript += ConvertState(dia, state);
            }

            // 打开文件
            String strFileName = OptionManager.LuaPath+ strScriptFileName;
            //StreamWriter writer = File.CreateText(strFileName);
            StreamWriter writer = new System.IO.StreamWriter(strFileName, false, System.Text.Encoding.GetEncoding("GB2312"));
            
            // 添加手写脚本
            foreach(String strFuncName in m_ManualScriptTable.Keys)
            {
                if (strLuaScript.Contains(strFuncName))
                {
                    writer.Write(m_ManualScriptTable[strFuncName]);
                }
            }

            // 写入脚本
            writer.Write(strLuaScript);
            
            // 保存关闭
            writer.Flush();
            writer.Close();

            m_strType = "";
            m_iTargetID = -1;
            m_StringResList = null;

            // 将文件名返回
            return strScriptFileName;
        }

        // 获取 StateDiagram 中的默认对话 State
        private State GetDefaultDlgState(StateDiagram dia)
        {
            State defDlgState = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            return defDlgState;
        }

        // 判断一个 State 的 OnEnter Action List 中是否有显示对话框行为
        private static bool HaveShowDialogAction(State state)
        {
            foreach (AtomActionInstance atomInst in state.ActionListOnEnter)
            {
                if (atomInst.AtomDisplay.Equals(AtomActionManager.Display_ShowDialog))
                {
                    //if (state.OptionList.Count>0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        // 判断一个 State 的 OnEnter Action List 中是否有显示商店行为
        private static bool HaveShowShop(State state)
        {
            foreach (AtomActionInstance atomInst in state.ActionListOnEnter)
            {
                if (atomInst.AtomDisplay.Equals("ShowShop"))
                {
                     return true;
                }
            }
            return false;
        }

        // 找出 state 中 没有对应 translation 的 option
        private static ArrayList GetOptionsHaveNoTranslation(StateDiagram dia, State state)
        {
            ArrayList optionList = new ArrayList();
            foreach (DialogOption option in state.OptionList)
            {
                if (option.Value==-1)
                {
                    optionList.Add(option);
                }
                else
                {
                    bool bHaveTranslation = false;  // option 是否有对应 translation
                    foreach (Translation trans in dia.TranslationList)
                    {
                        // 找到起点为 state 的 translation
                        if (trans.StartStateID == state.ID)
                        {
                            if (option.Value == trans.CurrentOption)
                            {
                                bHaveTranslation = true;
                                break;
                            }
                        }
                    }
                    if (!bHaveTranslation)  // 如果 option 没有找到对应 translation
                    {
                        optionList.Add(option);
                    }
                }
            }
            return optionList;
        }

        // 判断一个状态是否是 SUMBIT_DLG
        private static bool IsMissionSubmitState(State state)
        {
            int iComma = state.Dialog.IndexOf(",");
            if (iComma != -1)
            {
                String strKey = state.Dialog.Substring(0, iComma);
                if (strKey.CompareTo("TaskRewardUI") == 0)
                {
                    return true;
                }
            }
            return false;
        }

        // 判断一个状态是否是 GET_DLG
        private static bool IsMissionGetState(State state)
        {
            int iComma = state.Name.LastIndexOf("_");
            if (iComma < 0)
            {
                return false;
            }

            if (state.Name.Substring(iComma).CompareTo("_GET") == 0)
            {
                return true;
            }
            return false;
        }

        // 从 atom action 列表中找出所有 add money 选项,生成 NpcCanGiveMoney 参数 
        private static String ExtractCanGiveMoneyParamFromAtomActions(ArrayList atomList)
        {
            // 金钱总数
            int iTotalMoney = 0;

            // 遍历叠加金钱总数
            foreach (AtomActionInstance atomInst in atomList)
            {
                if(atomInst.Atom == null)
                {
                    MessageBox.Show("找不到原子操作---" + atomInst.AtomDisplay);
                    continue;
                }

                if (atomInst.Atom.CompareTo("MuxLib.NpcAddMoney") == 0)
                {
                    iTotalMoney += int.Parse(atomInst.Parameters);
                }
            }

            // 若money为0，则返回空串
            if(iTotalMoney == 0)
            {
                return "";
            }
            else
            {
                return iTotalMoney.ToString();
            }
        }

        // 从 atom action 列表中找出所有 give item 选项,生成 NpcQueryPackageCanContain 参数 
        private static String ExtractPackageCanContainParamFromAtomActions(ArrayList atomList)
        {
            const int ciMaxItemNum = 10;

            String strParam = "";
            int iNum = 0;
            foreach (AtomActionInstance atomInst in atomList)
            {
                if (atomInst.Atom == null)
                {
                    MessageBox.Show("找不到原子操作---" + atomInst.AtomDisplay);
                    continue;
                }

                if (atomInst.Atom.CompareTo("MuxLib.NpcGiveItem") == 0)
                {
                    iNum++;
                    strParam += atomInst.Parameters;
                    if (iNum != ciMaxItemNum)
                    {
                        strParam += ",";
                    }
                }
                else if(atomInst.Atom.CompareTo("MuxLib.NpcGiveItemFrom") == 0)
                {
                    iNum++;
                    // 获取 
                    int iCommaIndex = atomInst.Parameters.IndexOf(",");
                    String strFirstItem = "";
                    if(iCommaIndex == -1)
                    {
                        strFirstItem = atomInst.Parameters + ",1";
                    }
                    else
                    {
                        strFirstItem = atomInst.Parameters.Substring(0, iCommaIndex) + ",1";
                    }

                    strParam += strFirstItem;
                    if (iNum != ciMaxItemNum)
                    {
                        strParam += ",";
                    }
                }

            }

            if (iNum > 0)
            {
                for (int i = iNum; i < ciMaxItemNum; i++)
                {
                    strParam += "0,0";
                    if (i != ciMaxItemNum - 1)
                    {
                        strParam += ",";
                    }

                }
            }

            return strParam;
        }

        // 找出 finish task 操作
        private static AtomActionInstance FindFinishTaskAtomAction(ArrayList actions)
        {
            foreach (AtomActionInstance action in actions)
            {
                if (action.Atom.Equals("MuxLib.NpcFinishTask") || action.Atom.Equals("MuxLib.NpcFinishWarTask") || action.Atom.Equals("MuxLib.NpcFinishPunishTask"))
                {
                    return action;
                }
            }

            return null;
        }

        // 找出NpcShowCustomizeMsg 操作
        private static AtomActionInstance FindShowCustomizeMsgAtomAction(ArrayList actions)
        {
            foreach (AtomActionInstance action in actions)
            {
                if (action.Atom == null)
                {
                    MessageBox.Show("找不到原子操作---" + action.AtomDisplay);
                    continue;
                }

                if (action.Atom.Equals("MuxLib.NpcShowCustomizeMsg"))
                {
                    return action;
                }
            }

            return null;
        }

        // 将一个 State 转换为 Lua 脚本
        private static String ConvertState(StateDiagram dia, State state)
        {
            String strLua = "";
            strLua += "states[" + state.ID.ToString() + "] = \n";   // 状态编号
            strLua += "{\n";                                        // 状态开始 左大括号
            strLua += "\tname = \"" + state.Name + "\",\n";         // 状态名称
            int iStateType = 2; // 0-接任务   1-还任务  2-其他
            if (IsMissionGetState(state))
            {
                iStateType = 0;
            }
            else if(IsMissionSubmitState(state))
            {
                iStateType = 1;
            }

            // 如果状态中有对话，添加 OnOption 项
            if (HaveShowDialogAction(state))
            {
                strLua += "\tOnOption = function(option)\n";
                strLua += "\t\t\t\t\t\t\t\tcurOption = option; \n";
                if (IsMissionSubmitState(state))
                {
                    // 如果是 Mission Submit State， 当返回 7 (取消还任务) 时立刻返回
                    strLua += "\t\t\t\t\t\t\t\tif (option == 7) then \n";
                    strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcCloseUiInfo();\n";
                    strLua += "\t\t\t\t\t\t\t\t\treturn; \n";
                    strLua += "\t\t\t\t\t\t\t\tend \n";
                }

                //--------------------------------------------------添加了对超出金钱上限的判断--------------------------------------------------//
                String strMoneyParam = ExtractCanGiveMoneyParamFromAtomActions(state.ActionListOnOption);
                bool bHaveAddMoney = !strMoneyParam.Equals("");
                if (bHaveAddMoney)   // 有 give item
                {
                    strLua += "\t\t\t\t\t\t\t\tif (MuxLib.NpcCanGiveMoney(" + strMoneyParam + ")) then \n";
                }
                //--------------------------------------------------添加了对超出金钱上限的判断--------------------------------------------------//

                // 如果有 give item action, 要判断背包能否包含这些 item
                String strParam = ExtractPackageCanContainParamFromAtomActions(state.ActionListOnOption);
                bool bHaveGiveItem = !strParam.Equals("");
                // 添加包裹是否可容纳判断

                if (bHaveGiveItem)   // 有 give item
                {
                    strLua += "\t\t\t\t\t\t\t\tif (MuxLib.NpcQueryPackageCanContain(" + strParam + ")) then \n";
                }

                {
                    // 如果是 submit state，OnOption 选项要特殊处理。
                    if (IsMissionSubmitState(state))
                    {
                        strLua += "\t\t\t\t\t\t\t\t\tif (option <= 6) then \n";

                        // 先执行 FinishTask 并且执行成功 (返回 0) 后再执行其他操作
                        AtomActionInstance finishTaskAtom = FindFinishTaskAtomAction(state.ActionListOnOption);

                        if (finishTaskAtom != null)
                        {
                            strLua += "\t\t\t\t\t\t\t\t\t\tif (" + finishTaskAtom.Atom + "(" + finishTaskAtom.Parameters + ") == 0) then -- finish task 成功后再给奖励 \n";
                        }

                        foreach (AtomActionInstance atomInst in state.ActionListOnOption)
                        {
                            // 如果有可选奖励，需要特殊处理
                            if (atomInst.AtomDisplay.CompareTo("GiveItemFrom") == 0)
                            {
                                String[] split = atomInst.Parameters.Split(new Char[] { ',' });
                                for (int i = 1; i <= split.Length; i++)
                                {
                                    strLua += "\t\t\t\t\t\t\t\t\t\t\tif (option == " + i + ") then \n";
                                    strLua += "\t\t\t\t\t\t\t\t\t\t\t\tMuxLib.NpcGiveItem(" + split[i - 1] + ",1); \n";
                                    strLua += "\t\t\t\t\t\t\t\t\t\t\tend\n";
                                }
                            }
                            else
                            {
                                // FinishTask 已经在开始执行过了.
                                if (atomInst.Atom.CompareTo("MuxLib.NpcFinishTask") != 0 && !atomInst.Atom.Equals("MuxLib.NpcFinishWarTask") && !atomInst.Atom.Equals("MuxLib.NpcFinishPunishTask"))
                                {
                                    strLua += "\t\t\t\t\t\t\t\t\t\t\t" + atomInst.Atom + "(" + atomInst.Parameters + ");\n";
                                }

                            }
                        }

                        if (finishTaskAtom != null)
                        {
                            strLua += "\t\t\t\t\t\t\t\t\t\tend\n";
                        }

                        strLua += "\t\t\t\t\t\t\t\t\tend\n";
                        strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcCloseUiInfo();\n";
                        //strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcCheckTransCon();\n";
                        // 判断 state 之后是否有 translation
                    }
                    else
                    {
                        // 找出 state 中 没有对应 translation 的 option
                        ArrayList noLinkOptions = GetOptionsHaveNoTranslation(dia, state);
                        if (noLinkOptions.Count > 0)
                        {
                            strLua += "\t\t\t\t\t\t\t\t\tif (false \n";
                            foreach (DialogOption option in noLinkOptions)
                            {
                                strLua += "\t\t\t\t\t\t\t\t\t\t\tor curOption==" + option.Value.ToString();
                            }
                            strLua += ") then\n";
                            strLua += "\t\t\t\t\t\t\t\t\t\tMuxLib.NpcCloseUiInfo();\n";
                            strLua += "\t\t\t\t\t\t\t\t\t\treturn;\n";
                            //strLua += "\t\t\t\t\t\t\t\telse\n";
                            //strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcCheckTransCon(); \n";
                            strLua += "\t\t\t\t\t\t\t\t\tend\n";

                        }
                    }
                }

                if (bHaveGiveItem)   // 有 give item
                {
                    strLua += "\t\t\t\t\t\t\t\telse\n";
                    //strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcCloseUiInfo();\n";
                    strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcOpenStandardUI("+iStateType.ToString()+");  --显示提背包满示框 \n";  // 显示提背包满示框
                    strLua += "\t\t\t\t\t\t\t\t\tcurOption = -1;\n";
                    strLua += "\t\t\t\t\t\t\t\tend\n";
                }

                //--------------------------------------------------添加了对超出金钱上限的判断--------------------------------------------------//
                if (bHaveAddMoney)   // 有 give item
                {
                    strLua += "\t\t\t\t\t\t\t\telse\n";
                    //strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcCloseUiInfo();\n";
                    strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcOpenStandardUI(3);  --显示超出金钱上限提示框 \n";
                    strLua += "\t\t\t\t\t\t\t\t\tcurOption = -1;\n";
                    strLua += "\t\t\t\t\t\t\t\tend\n";
                }
                //--------------------------------------------------添加了对超出金钱上限的判断--------------------------------------------------//

                if (dia.ConnectedTranslation(state))
                {
                    strLua += "\t\t\t\t\t\t\t\tMuxLib.NpcCheckTransCon(); \n";
                }
                strLua += "\t\t\t\t\t\t\tend,\n";   // end function
            }
            else if (HaveShowShop(state))
            {
                // 商店目前不用特殊处理
            }

            if (state.ActionListOnEnter.Count>0)
            {
                strLua += "\tOnEnter = function()\n";

                //--------------------------------------------------添加了对超出金钱上限的判断--------------------------------------------------//
                String strMoneyParam = ExtractCanGiveMoneyParamFromAtomActions(state.ActionListOnEnter);
                bool bHaveAddMoney = !strMoneyParam.Equals("");
                if (bHaveAddMoney)   // 有 give item
                {
                    strLua += "\t\t\t\t\t\t\t\tif (MuxLib.NpcCanGiveMoney(" + strMoneyParam + ")) then \n";
                }
                //--------------------------------------------------添加了对超出金钱上限的判断--------------------------------------------------//


                String strParam = ExtractPackageCanContainParamFromAtomActions(state.ActionListOnEnter);
                bool bHaveGiveItem = !strParam.Equals("");
                // 添加包裹是否可容纳判断

                
                if (bHaveGiveItem/* && IsMissionGetState(state)*/)   // 有 give item
                {
                    strLua += "\t\t\t\t\t\t\t\tif (MuxLib.NpcQueryPackageCanContain(" + strParam + ")) then \n";
                }
                strLua += ConvertActionList(state, state.ActionListOnEnter);

                if (bHaveGiveItem/* && IsMissionGetState(state)*/)   // 有 give item
                {
                    strLua += "\t\t\t\t\t\t\t\telse\n";
                    //strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcCloseUiInfo();\n";
                    strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcOpenStandardUI(" + iStateType.ToString() + ");  --显示提背包满示框 \n";  // 显示提背包满示框
                    strLua += "\t\t\t\t\t\t\t\t\tcurOption = -1;\n";
                    strLua += "\t\t\t\t\t\t\t\tend\n";
                }

                //--------------------------------------------------添加了对超出金钱上限的判断--------------------------------------------------//
                if (bHaveAddMoney)
                {
                    strLua += "\t\t\t\t\t\t\t\telse\n";
                    //strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcCloseUiInfo();\n";
                    strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcOpenStandardUI(3);  --显示超出金钱上限提示框 \n";
                    strLua += "\t\t\t\t\t\t\t\t\tcurOption = -1;\n";
                    strLua += "\t\t\t\t\t\t\t\tend\n";
                }
                //--------------------------------------------------添加了对超出金钱上限的判断--------------------------------------------------//

                strLua += "\t          end,\n";
            }

            if (state.ActionListOnExit.Count>0)
            {
                strLua += "\tOnExit = function()\n";
                strLua += ConvertActionList(state, state.ActionListOnExit);
                strLua += "\t          end,\n";
            }

            strLua += ConvertTranslationsToLinks(dia, state);
            strLua += "}\n";                                        // 状态结束 右大括号
            strLua += "\n";                                         // 空行
            return strLua;
        }

        // 将一个字符串添加到对话管理器中,同时返回该对话 ID
        private static int AddStringToDialogManager(String strDialog)
        {
            // 关于 字符串 ID 的规则：
            // NPC对话：1 + 5位NPCID + 3位流水号
            // NPC死亡：2 + 5位NPCID + 3位流水号
            // 怪物死亡：3 + 怪物ID后5位 + 3位流水号
            // 物品脚本：4 + ItemID后5位 + 3位流水号
            // 区域脚本：5 + 6位区域ID + 2位流水号

            if (m_StringResList == null)
            {
                return -1;
            }

            // 字符串 ID 前缀
            String strPrefix = m_iStringIDPrefix.ToString();
            String strStrID = "";
            if (m_eDialogType == StateDiagram.OwnerType.DT_AREA)
            {
                // Area 流水号为两位
                int iStrCount = m_StringResList.Count;
                if (iStrCount < 10)
                {
                    strStrID = strPrefix + "0" + iStrCount.ToString();
                }
                else if (iStrCount >= 100)
                {
                    MessageBox.Show("单个 AREA 绑定的对话不能超过100个.");
                    return -1;
                }
                else
                {
                    strStrID = strPrefix + iStrCount.ToString();
                } 
            }
            else // NPC, NPC_DEATH, MONSTER_DEATH, ITEM
            {
                // 除 Area 外，对话流水号为 3 位
                int iStrCount = m_StringResList.Count;
                if (iStrCount < 10)
                {
                    strStrID = strPrefix + "00" + iStrCount.ToString();
                }
                else if (iStrCount < 100)
                {
                    strStrID = strPrefix + "0" + iStrCount.ToString();
                }
                else if (iStrCount >= 1000)
                {
                    MessageBox.Show("单个 NPC/Monster/Item 身上绑定的对话不能超过1000个.");
                    return -1;
                }
                else
                {
                    strStrID = strPrefix + iStrCount.ToString();
                }
            }

            int iStrID = int.Parse(strStrID);

            // 在这里解析 strRes 中的标签, key1, key2
            //strDialog = RemoveComment(strDialog);

            // 判断 strDialog 是否具有 标准任务 UI 关键字
            int iComma = strDialog.IndexOf(",");
            String strKey1 = null;
            String strKey2 = null;
            if (iComma != -1)
            {
                String strKey = strDialog.Substring(0, iComma);
                if (strKey.CompareTo("TaskAcceptUI")==0 ||
                    strKey.CompareTo("TaskRewardUI") == 0)
                {
                    strKey1 = strKey;
                    strKey2 = strDialog.Substring(iComma + 1);
                    strDialog = "";
                }
            }
            StringRes strRes = new StringRes(iStrID, strDialog);
            strRes.Key1 = strKey1;
            strRes.Key2 = strKey2;

            m_StringResList[strRes.ID] = strRes;
            return iStrID;
        }

        // 移除 [[ ]] 注释
        private static String RemoveComment(String strRes)
        {
            int iStart = Find(strRes, "[[");
            int iEnd = Find(strRes, "]]");
            if (iStart!=-1 && iEnd!=-1)
            {
                String strResult = strRes.Substring(0, iStart) + strRes.Substring(iEnd + 2);
                return strResult;
            }
            return strRes;
        }
        
        // 在 strSource 中查找 strTarget.返回 strTarget 首字母在 strSource 的位置 
        private static int Find(String strSource, String strTarget)
        {
            // 遍历 strSource 所有字母
            for (int i=0; i<=strSource.Length-strTarget.Length; i++)
            {
                // 如果发现 strSource 中有字母与 strTarget 首字母相同
                if (strSource[i] == strTarget[0])
                {
                    bool bSame = true;
                    for (int j=0; j<strTarget.Length; j++)
                    {
                        if (strSource[i+j] != strTarget[j])
                        {
                            bSame = false;
                            break;
                        }
                    }
                    if (bSame)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        private static String ConvertOptionCondition(ArrayList conditionList)
        {
            String strCondition = "";

            for (int i = 0; i < conditionList.Count; i++)
            {
                MissionCondition cond = conditionList[i] as MissionCondition;
                if (i != 0)
                {
                    strCondition += "\n\t\t\t\t\t\t\t\t\tand ";
                }
                
                strCondition += MissionConditionManager.Instance.GetConditionAtomByDisplay(cond.Condition) + "(" + cond.Value + ")";
            }

            return strCondition;
        }

        private static String ConvertActionList(State state, ArrayList actionList)
        {
            String strLua = "";
            AtomActionInstance CustomizeMsg = FindShowCustomizeMsgAtomAction(actionList);

            foreach (AtomActionInstance atomInst in actionList)
            {
                String strAtomDisplay = atomInst.AtomDisplay;
                // 获取原子操作
                // String strAtom = AtomActionManager.Instance.AtomActionList[strAtomDisplay].ToString();
                String strAtom = null;
                MissionAtomInstance mti = AtomActionManager.Instance.AtomActionList[strAtomDisplay] as MissionAtomInstance;
                if(mti != null)
                {
                    strAtom = mti.Atom;
                }

                if (strAtom != null)
                {
                    if (strAtom.Equals(AtomActionManager.Atom_ShowDialog))
                    {
                        // 有显示对话的 原子操作,特别处理
                        strLua += "\t\t\t\t\t\t\t\t\t" + strAtom + "( \"#" + AddStringToDialogManager(state.Dialog).ToString() + "\");";
                        strLua += "\t--[[" +state.Dialog + "]]\n";

                        // 扩展对话处理
                        if (CustomizeMsg != null)
                        {
                            strLua += "\t\t\t\t\t\t\t\t\t" + CustomizeMsg.Atom + "(" + "\"&\".." + CustomizeMsg.Parameters + ");\n";
                        }

                        foreach (DialogOption option in state.OptionList)
                        {
                            if (option.ConditionList.Count == 0)
                            {
                                strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcSetOption(" + (option.Value).ToString() + ", \"#" + AddStringToDialogManager(option.OptionString).ToString() + "\");";
                                strLua += "\t--[[" + RemoveComment(option.OptionString) + "]]\n";
                            }
                            else
                            {
                                strLua += "\t\t\t\t\t\t\t\t\tif (" + ConvertOptionCondition(option.ConditionList) + ") then\n";
                                strLua += "\t\t\t\t\t\t\t\t\t\tMuxLib.NpcSetOption(" + (option.Value).ToString() + ", \"#" + AddStringToDialogManager(option.OptionString).ToString() + "\");";
                                strLua += "\t--[[" + RemoveComment(option.OptionString) + "]]\n";
                                strLua += "\t\t\t\t\t\t\t\t\tend \n";
                            }

                        }
                        strLua += "\t\t\t\t\t\t\t\t\tMuxLib.NpcSendUiInfo(); \n";
                    }
                    else
                    {
                        strLua += "\t\t\t\t\t\t\t\t\t" + strAtom + "(" + atomInst.Parameters + ");\n";
                    }
                }
            }
            return strLua;
        }

        // 获取 state 中 Option 列表
        private static ArrayList GetTranslation(StateDiagram dia, State state, int iOption)
        {
            ArrayList optionList = new ArrayList();
            foreach (DialogOption option in state.OptionList)
            {
                if (option.Value == iOption)
                {
                    optionList.Add(option);
                }
            }

            return optionList;
        }

        // 转换 State 的 Translation 到 Link
        private static String ConvertTranslationsToLinks(StateDiagram dia, State state)
        {
            // 将 state 对应的 translation 分成三类：
            // 1。只有 option 条件的
            // 2。没有条件，绝对转换的
            // 3。复杂条件的
            Hashtable opt2tar = new Hashtable();      // 单纯的 option - target 转换表
            ArrayList noOptionList = new ArrayList();       // 没有option,无条件转换的的 translation 列表 应该最多一个元素
            ArrayList multiCondTransList = new ArrayList();     // 有多个条件的 translation 列表
            int iMaxOption = -1;    // state 所对应 translation 中对应 option 的最大值

            // 遍历 state 对应的所有 translation, 将其分类。
            foreach (Translation trans in dia.TranslationList)
            {
                if (trans.StartStateID == state.ID)
                {
                    if (trans.Conditions.Count == 0)    // 该 trans 无额外条件
                    {
                        if (trans.CurrentOption >= 0)   // 有 option
                        {
                            opt2tar[trans.CurrentOption] = trans.EndStateID;
                            if (trans.CurrentOption > iMaxOption) iMaxOption = trans.CurrentOption;
                        }
                        else    // 无 option
                        {
                            noOptionList.Add(trans);
                        }
                    }
                    else
                    {
                        // 复杂条件的 translation
                        multiCondTransList.Add(trans);
                    }
                }
            }

            // 将 links 转换为函数。
            String strLua = "";
            strLua += "\tlinks = function()\n";
            String strIndent = "\t\t\t\t\t\t\t";
            // 无条件转换
            if (noOptionList.Count > 0)
            {
                // 只转换到第一个 translation 的 EndState
                int iTarget = (noOptionList[0] as Translation).EndStateID;
                strLua += strIndent + "\treturn " + iTarget.ToString()+";\t --无条件的转换。\n";
            }
            else
            {
                if (opt2tar.Keys.Count > 0)
                {
                    // 创建 option - target 静态表
                    strLua += strIndent + "-- ";
                    foreach (int k in opt2tar.Keys)
                    {
                        strLua += k.ToString() + ":" + opt2tar[k].ToString() + " "; 
                    }
                    strLua += "\n";

                    strLua += strIndent + "linksTable = {";
                    for (int i = 1; i <= iMaxOption; i++)   // Lua 表下标从 1 开始.
                    {
                        if (opt2tar.ContainsKey(i))
                        {
                            strLua += opt2tar[i].ToString() + ", ";
                        }
                        else
                        {
                            strLua += "nil, ";
                        }
                    }
                    strLua += "}; \t -- option-target 静态表. 元素在表中的下标为 option, 元素本身值为 target.\n";

                    // 如果 option 在 linksTable 中对应值,直接返回
                    strLua += strIndent + "target = linksTable[curOption]; \n";
                    strLua += strIndent + "if (target ~= nil) then\n";
                    strLua += strIndent + "\treturn target; \n";
                    strLua += strIndent + "end\n";
                }

                // 多条件的转换
                foreach (Translation trans in multiCondTransList)
                {
                    strLua += strIndent + "if (true \n ";
                    if (trans.CurrentOption >= 1)
                    {
                        strLua +=strIndent +"\t\tand option == " + trans.CurrentOption.ToString();
                    }
                    strLua += ConvertConditionList(trans.Conditions, strIndent+"\t\t") + ")  then \n";
                    strLua += strIndent + "\t\treturn " + trans.EndStateID.ToString() + ";\n";
                    strLua += strIndent + "end\n";
                }
                strLua += strIndent + "return nil;\n";
            }

            strLua += "\t        end,\n"; // function() end
            return strLua;

        }

        // 转换条件列表到 if 语句后的条件块
        private static String ConvertConditionList(ArrayList conditionList, String strIndent)
        {
            String strCondition = "";

            for (int i=0; i<conditionList.Count; i++)
            {
                MissionCondition cond = conditionList[i] as MissionCondition;
                strCondition += "\n"+strIndent +"and ";
                strCondition += MissionConditionManager.Instance.GetConditionAtomByDisplay(cond.Condition)+"("+cond.Value+")";
            }

            return strCondition;
        }

        // 载入手工脚本
        public static void LoadManualScript(String strPath)
        {
            // 初始化脚本的table
            m_ManualScriptTable = new Hashtable();

            // 判断路径是否正确
            if (Directory.Exists(strPath))
            {
                // 获取制定文件夹下的所有文件列表
                String[] fileList = System.IO.Directory.GetFileSystemEntries(strPath);

                // 遍历文件读取信息
                foreach (String strFileName in fileList)
                {
                    // 获取文件名的前缀(即函数名)
                    int iStart = strFileName.LastIndexOf("/");
                    int iEnd = strFileName.LastIndexOf(".");
                    String strName = strFileName.Substring(iStart + 1, iEnd - iStart - 1);

                    // 获取函数内容
                    String strText = File.ReadAllText(strFileName, Encoding.GetEncoding("GB2312"));

                    // 加入表中
                    m_ManualScriptTable[strName] = strText;
                }
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private static String m_strType;
        private static StateDiagram.OwnerType m_eDialogType;  // 当前解析的状态图对应字符串类型
        private static int m_iStringIDPrefix = -1;              // 当前解析状态图对应字符串ID前缀
        private static int m_iTargetID = -1;                    // 当前转换的状态图绑定与哪个 NPC/Item/Area
        private static Hashtable m_StringResList = null;        // 当前 NPC 对应的 字符资源列表

        private static Hashtable m_ManualScriptTable = null;    // 手写的脚本
    }
}
