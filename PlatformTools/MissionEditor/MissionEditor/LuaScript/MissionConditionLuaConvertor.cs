﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

// 任务获取条件脚本,客户端使用
namespace MissionEditor
{
    public class MissionConditionLuaConvertor
    {
        static public void Convert()
        {

            String strLua = "-- "+OptionManager.MissionConditionLuaPath+"\n";
            strLua += "npcMissions = {}\n";
            // 生成 npc-mission map
            Hashtable npc2Mission = GetNpc2MissionMap();

            // 遍历所有任务 npc,
            foreach (int iNpcID in npc2Mission.Keys)
            {
                // 任务 npc 第一位为 1
                strLua += GetNpcMissionConditionScript(iNpcID, npc2Mission[iNpcID] as ArrayList);
                strLua += "\n";
            }

            System.IO.StreamWriter writer = new System.IO.StreamWriter(OptionManager.MissionConditionLuaPath, false, System.Text.Encoding.GetEncoding("GB2312"));
            writer.Write(strLua);
            writer.Flush();
            writer.Close();

        }

        // 生成一个 npc 的所有任务条件脚本.
        static private String GetNpcMissionConditionScript(int iNpcID, ArrayList missionList)
        {

            String strLua = "";
            strLua += "npcMissions[" + iNpcID.ToString() + "] =\n";
            strLua += "{\n";
            // 遍历该 npc 对应所有任务
            foreach (Mission mission in missionList)
            {
                  strLua += GetMissionConditionScript(mission);
                  strLua += "\n";
            }
            strLua += "}\n";
            return strLua;
        }

        static private String GetMissionConditionScript(Mission mission)
        {
            String strLua = "";
            strLua += "\tM" + mission.ID + " = function()\n";
            strLua += "\t\tif (true ";
            
            if (mission.Repeat == false)
            {
                // 如果是不可重复任务,添加 "从未接受" 条件
                strLua += "\n\t\t\t\tand " + "MuxLib.NpcQueryNeverAcceptMission" + "(" + mission.ID.ToString() + ")";
            }
            else
            {
                // 否则添加 "不在任务列表" 条件
                strLua += "\n\t\t\t\tand " + "MuxLib.NpcQueryIsMissionNotOnList" + "(" + mission.ID.ToString() + ")";
            }

            // 等级条件
            strLua += "\n\t\t\t\tand " + "MuxLib.NpcQueryPropertyMoreThan(1, " + (mission.Level - OptionManager.LevelDifference).ToString() + ")";
            
            foreach (MissionCondition condition in mission.Conditions)
            {
                strLua += "\n\t\t\t\tand " + MissionConditionManager.Instance.GetConditionAtomByDisplay(condition.Condition) + "(" + condition.Value + ")";
            }
            strLua += ") then\n";
            strLua += "\t\t\treturn " + mission.ID.ToString() + ";\n";
            strLua += "\t\telse\n";
            strLua += "\t\t\treturn -1;\n";
            strLua += "\t\tend\n";
            strLua += "\tend,\n";

            return strLua;
        }

        // 生成 npc - 该 npc 相关 mission  映射表
        static private Hashtable GetNpc2MissionMap()
        {
            Hashtable npc2mission = new Hashtable();
            // 遍历所有任务
            foreach (Mission mission in MainForm.Instance.MissionMgr.MissionList)
            {
                if (!npc2mission.ContainsKey(mission.Provider))
                {
                    ArrayList npcList = new ArrayList();
                    npc2mission[mission.Provider] = npcList;
                }

                (npc2mission[mission.Provider] as ArrayList).Add(mission);
            }
            return npc2mission;
        }
    }
}
