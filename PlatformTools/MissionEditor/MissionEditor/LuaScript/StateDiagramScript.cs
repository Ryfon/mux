﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using LuaInterface;

namespace MissionEditor
{
    // 封装一个表示状态图的 lua 脚本
    public class StateDiagramScript
    {        
        // 初始化 Lua VM, 注册扩展函数
        public static void InitLuaVM()
        {
            m_LuaVM = new Lua();
        
            // 创建 MuxLib 表,存放所有扩展函数
            m_LuaVM.NewTable("MuxLib");

            // 注册所有扩展函数
            Type muxLibType = MuxLib.Instance.GetType();
            MethodInfo[] methods = muxLibType.GetMethods();
            foreach (MethodInfo methodInfo in methods)
            {
                m_LuaVM.RegisterFunction("MuxLib." + methodInfo.Name, MuxLib.Instance, methodInfo);//
            }
        }

        public bool Load(String strLuaFile)
        {
            if (m_LuaVM == null)
            {
                InitLuaVM();
            }

            try
            {
                m_LuaVM.DoFile(strLuaFile);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }

            m_States = m_LuaVM.GetTable("states") as LuaTable;
            if (null == m_States)
            {
                return false;
            }

            m_iScriptID = (int)m_LuaVM.GetNumber("nScriptID");
            m_iStartState = (int)m_LuaVM.GetNumber("start");
            m_iCurState = m_iStartState;
            return true;
        }


        // 设置用户选项
        public void SetCurOption(int iOption)
        {
            if (null != m_LuaVM)
            {
                m_LuaVM.DoString("curOption = " + iOption.ToString());
            }
        }

        public void CheckTransCon()
        {
            if (null != m_LuaVM)
            {
                // 遍历 CurState 的 links,找到第一个符合条件
                LuaTable tableLinks = CurState["links"] as LuaTable;
                foreach(object obj in tableLinks.Values)
                {
                    if (obj is LuaTable)
                    {
                        LuaTable tableLink = obj as LuaTable;
                        int iTarget = (int)((double)(tableLink["target"]));
                        LuaFunction funAdvance = tableLink["advance"] as LuaFunction;
                        object[] results = funAdvance.Call();
                        if ((bool)(results[0]) == true)
                        {
                            // 找到转换的目标 state 
                            SetCurState(iTarget);
                            return;
                        }
                    }
                }
            }
        }

        private void SetCurState(int iStateID)
        {
            m_iCurState = iStateID;
            LuaScriptTestForm.Instance.ClearDialogUI();
            CurOnEnter.Call();
        }

        // 当前所处状态
        public LuaTable CurState
        {
            get
            {
                return m_States[m_iCurState] as LuaTable;
            }
        }

        public LuaFunction CurOnEnter
        {
            get
            {
                return CurState["OnEnter"] as LuaFunction;
            }
        }

        public LuaFunction CurOnOption
        {
            get
            {
                return CurState["OnOption"] as LuaFunction;
            }
        }

        private int m_iScriptID;            // 编号
        private int m_iStartState;          // 初始 State
        private int m_iCurState;            // 当前状态
        private static Lua m_LuaVM = null;  // Lua 虚拟机
        private LuaTable m_States;          // 状态图的各个状态
    }
}
