﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LuaInterface;

namespace MissionEditor
{
    public partial class LuaScriptTestForm : Form
    {
        public LuaScriptTestForm()
        {
            InitializeComponent();
        }

        // 窗口启动
        private void LuaScriptTestForm_Load(object sender, EventArgs e)
        {
            // 填充 Player 信息
            RefreshPlayerInfo(Player.Instance);

            // 填充 m_comboBoxMissionState
            String [] strStates = Enum.GetNames(typeof(Player.MissionState));
            foreach (String strState in strStates)
            {
                m_comboBoxMissionState.Items.Add(strState);
            }
            
        }

        public void RefreshPlayerInfo(Player player)
        {
            // 刷新 Item 列表
            ItemListBox.Items.Clear();
            foreach (Item item in player.ItemList)
            {
                LuaScriptTestForm.Instance.ItemListBox.Items.Add(MainForm.GetDisplayText(item.ID, item.Name, OptionManager.ItemDisplayMode));
            }

            // 刷新 Counter 列表
            CounterListBox.Items.Clear();
            foreach (Counter ct in player.CounterList)
            {
                LuaScriptTestForm.Instance.CounterListBox.Items.Add(ct.ToString());
            }

            // 刷新任务列表
            m_listBoxMission.Items.Clear();
            foreach (int iMissionID in player.MissionStates.Keys)
            {
                m_listBoxMission.Items.Add(iMissionID.ToString() + "," + player.MissionStates[iMissionID].ToString());
            }

            // 刷新跟随列表
            m_listBoxFollowing.Items.Clear();
            foreach (int iFollowing in player.FollowingList)
            {
                NPCInfo npcInfo = NPCManager.Instance.GetNPCInfoByID(iFollowing);
                if (npcInfo!=null)
                {
                    m_listBoxFollowing.Items.Add(npcInfo.ID.ToString() + "," + npcInfo.Name);
                }
            }

            // property grid
            m_propertyGridPlayer.SelectedObject = player;
        }

        public void ExecuteLuaScript(String strLuaFilename)
        {
            ClearDialogUI();

            try
            {
                m_OnRunningScript = new StateDiagramScript();
                m_OnRunningScript.Load(strLuaFilename);
                m_OnRunningScript.CurOnEnter.Call();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public void AddOption(int iValue, String strOption)
        {
            m_listBoxOptions.Items.Add(iValue.ToString() + "," + strOption);
        }

        // 清除对话界面
        public void ClearDialogUI()
        {
            m_webBrowserDialog.DocumentText = "";
            m_listBoxOptions.Items.Clear();
            //m_textBoxOutput.Text = "";
        }

        #region 公有属性
        public static LuaScriptTestForm Instance
        {
            get
            {
                if (m_Instance == null || m_Instance.IsDisposed)
                {
                    m_Instance = new LuaScriptTestForm();
                }

                return m_Instance;
            }
        }

        public void SetDialogString(StringRes strRes)
        {
            if (strRes.Key1 == null || strRes.Key1.CompareTo("")==0)
            {
                m_webBrowserDialog.DocumentText = strRes.MainString;
            }
            else
            {
                // 标准任务 UI
                if (strRes.Key1.CompareTo("TaskAcceptUI") == 0)
                {
                    m_webBrowserDialog.DocumentText = "标准接受任务 UI " + strRes.Key2;
                }
                else if (strRes.Key1.CompareTo("TaskRewardUI") == 0)
                {
                    m_webBrowserDialog.DocumentText = "标准奖励任务 UI " + strRes.Key2;
                    for (int i = 1; i <= 6; i++)
                    {
                        m_listBoxOptions.Items.Add(i.ToString() + ",接受奖励");
                    }
                    m_listBoxOptions.Items.Add("7,取消");
                }
            }
        }

        public System.String DialogText
        {
            get
            {
                return m_webBrowserDialog.DocumentText;
            }
        }

        public StateDiagramScript OnRunnerScript
        {
            get
            {
                return m_OnRunningScript;
            }
        }

        public ListBox ItemListBox
        {
            get
            {
                return m_listBoxItems;
            }
        }

        public ListBox CounterListBox
        {
            get
            {
                return m_listBoxCounter;
            }
        }

        public ListBox MissionListBox
        {
            get
            {
                return m_listBoxMission;
            }
        }

        public PropertyGrid PlayerPropertyGrid
        {
            get
            {
                return m_propertyGridPlayer;
            }
        }

        public String MessageText
        {
            get
            {
                return m_textBoxOutput.Text;
            }
            set
            {
                m_textBoxOutput.Text = value;
            }
        }

        #endregion

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            if (m_OnRunningScript == null)
            {
                return;
            }

            // 选择一个选项
            if (m_listBoxOptions.SelectedIndex > -1)
            {
                String strOption = m_listBoxOptions.SelectedItem.ToString();
                int iComma = strOption.IndexOf(",");
                int iValue = int.Parse(strOption.Substring(0, iComma));

                //m_OnRunningScript.SetCurOption(iValue);
                m_OnRunningScript.CurOnOption.Call(iValue);   // 调用 当前状态的 OnOption

            }
        }

        private void m_btnSetMissionState_Click(object sender, EventArgs e)
        {
            // 设置任务状态

            if (m_comboBoxMissionState.SelectedIndex == -1)
            {
                return;
            }

            try
            {
                int iMissionID = int.Parse(m_textBoxMissionID.Text);
                String strState = m_comboBoxMissionState.SelectedItem.ToString();
                Player.MissionState eState = (Player.MissionState)Enum.Parse(typeof(Player.MissionState), strState);
                Player.Instance.MissionStates[iMissionID] = eState;
                RefreshPlayerInfo(Player.Instance);
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }

        }

        private void m_btnUse_Click(object sender, EventArgs e)
        {
            // 使用物品
            int iSelIndex = m_listBoxItems.SelectedIndex;
            if (iSelIndex < 0)
            {
                return;
            }

            // 从物品列表中找到对应物品
            Item item = Player.Instance.ItemList[iSelIndex] as Item;
            if (item != null)
            {
                int iScriptID = item.ScriptID;
                String strLuaFilename = OptionManager.LuaPath+"item" + iScriptID.ToString() + ".lua";
                if (System.IO.File.Exists(strLuaFilename))
                {
                    ExecuteLuaScript(strLuaFilename);
                }
                else
                {
                    // 没有找到脚本文件
                    MessageText = "脚本文件 " + strLuaFilename + "没有找到 \r\n" + MessageText;
                }
            }
        }

        private void m_btnAddItem_Click(object sender, EventArgs e)
        {
            // 添加物品

            // 用 item id 获取 item
            try
            {
                int iItemID = int.Parse(m_textBoxItemID.Text);
                Item item = ItemManager.Instance.GetItemByID(iItemID);
                if (item == null)
                {
                    MessageBox.Show("没有找到 ID 为:" + iItemID.ToString() + "的 Item.");
                    return;
                }
                else
                {
                    Player.Instance.GetItem(iItemID);
                    //Player.Instance.ItemList.Add(item);
                    //RefreshPlayerInfo(Player.Instance);
                }
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
           
        }

        private void m_btnRemoveItem_Click(object sender, EventArgs e)
        {
            if (m_listBoxItems.SelectedIndex < 0)
            {
                return;
            }

            // 删除物品
            int iSelItemIdx = m_listBoxItems.SelectedIndex;
            Player.Instance.ItemList.RemoveAt(iSelItemIdx);

            RefreshPlayerInfo(Player.Instance);
        }

        private void m_btnAddCounterValue_Click(object sender, EventArgs e)
        {
            // 增加计数器记数值
            int iSelIndex = m_listBoxCounter.SelectedIndex;

            if (iSelIndex < 0)
            {
                return;
            }
            try
            {
                int iValue = int.Parse(m_textBoxCounterValue.Text);
                Counter ct = Player.Instance.CounterList[iSelIndex] as Counter;
                if (ct.AddCount(iValue))
                {
                    Player.Instance.MissionStates[ct.MissionID] = Player.MissionState.MS_ACHIEVE_TARGET;
                    Player.Instance.CounterList.Remove(ct);
                }
            }
            catch(Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }

            RefreshPlayerInfo(Player.Instance);

        }
    }
}