﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace MissionEditor
{
    // 计数器类
    public class Counter
    {
        // 0-杀怪 1-物品 2-杀玩家 3-交友
        public enum CounterType : int
        {
            CT_MONSTER = 0,
            CT_ITEM = 1,
            CT_KILLPLAYER = 2,
            CT_FRIEND = 3,
            CT_INVALIDATE,

        }

        public Counter(int iCounterType, int iTargetStart, int iTargetEnd, int iTargetCount, int iMissionID)
        {
            m_eCounterType = (CounterType)iCounterType;
            m_iMissionID = iMissionID;
            m_iTargetStart = iTargetStart;
            m_iTargetEnd = iTargetEnd;
            m_iTargetCount = iTargetCount;
        }

        // 是否达到计数目标值
        public bool IsAchievedTargetCounter()
        {
            return (m_iCount >= m_iTargetCount);
        }

        // 添加计数值.返回是否达到目标数
        public bool AddCount(int iValue)
        {
            m_iCount += iValue;
            return IsAchievedTargetCounter();
        }

        public override String ToString()
        {
            return m_eCounterType.ToString() + "," + m_iMissionID.ToString() + "," +
                m_iTargetStart.ToString() + "," + m_iTargetEnd.ToString() + "," + m_iCount + "/" + m_iTargetCount;
        }

        public int TargetStart
        {
            get
            {
                return m_iTargetStart;
            }
        }

        public int TargetEnd
        {
            get
            {
                return m_iTargetEnd;
            }
        }

        public int MissionID
        {
            get
            {
                return m_iMissionID;
            }
        }

        private CounterType m_eCounterType;
        private int m_iMissionID;     // 对应任务
        private int m_iTargetStart;
        private int m_iTargetEnd;
        private int m_iCount = 0;     // 当前值
        private int m_iTargetCount;   // 目标值
    };

    public class Player
    {

        public enum MissionState
        {
            MS_NEVER_ACCEPTED,      // 从未接过
            MS_NOT_ACHIEVE_TARGET,  // 在任务列表,未达目标
            MS_ACHIEVE_TARGET,      // 在任务列表,达目标
            MS_FINISHED,            // 完成
        }

        // player 与某 npc 对话,引发对话脚本
        public void TalkTo(int iNpcID)
        {
            // 从 NPCManager 中找到该 NPC 绑定的对话脚本
            NPCInfo npcInfo = NPCManager.Instance.GetNPCInfoByID(iNpcID);
            if (npcInfo == null)
            {
                LuaScriptTestForm.Instance.MessageText = "找不到相关的 NPC " + iNpcID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
                return;
            }
            if (npcInfo.DialogScriptID > 0)
            {
                String strLuaFilename = OptionManager.LuaPath+"/npc" + npcInfo.DialogScriptID.ToString() + ".lua";
                if (System.IO.File.Exists(strLuaFilename))
                {
                    // 找到脚本文件了
                    LuaScriptTestForm.Instance.ExecuteLuaScript(strLuaFilename);
                }
                else
                {
                    LuaScriptTestForm.Instance.MessageText = "脚本文件" + strLuaFilename + "不存在." + "\r\n" + LuaScriptTestForm.Instance.MessageText;
                    return;
                }

            }
            //m_iCurrDialogNpc = iNpcID;
        }

        // player 杀死某 npc/monster
        public void Kill(int iNpcID)
        {
            // 判断是否有杀 iNpcID 的计数器
            // 遍历计数器,查找是否有搜集该物品的计数
            ArrayList removeList = new ArrayList();
            foreach (Counter ct in m_Counters)
            {
                if (ct.TargetStart <= iNpcID && ct.TargetEnd >= iNpcID)
                {
                    bool bAchieve = ct.AddCount(1);
                    if (bAchieve)   // 达成目标
                    {
                        SetMissionState(ct.MissionID, MissionState.MS_ACHIEVE_TARGET);
                        removeList.Add(ct);
                    }
                }
            }

            foreach (Counter ct in removeList)
            {
                m_Counters.Remove(ct);
            }

            LuaScriptTestForm.Instance.RefreshPlayerInfo(this);

            // 从 NPCManager 中找到该 NPC 绑定的对话脚本
            NPCInfo npcInfo = NPCManager.Instance.GetNPCInfoByID(iNpcID);
            if (npcInfo == null)
            {
                LuaScriptTestForm.Instance.MessageText = "找不到相关的 NPC " + iNpcID.ToString() + "\r\n" + LuaScriptTestForm.Instance.MessageText;
                return;
            }
            if (npcInfo.DeathScriptID > 0)
            {
                String strLuaFilename = OptionManager.LuaPath + "/death" + npcInfo.DeathScriptID.ToString() + ".lua";
                if (System.IO.File.Exists(strLuaFilename))
                {
                    // 找到脚本文件了
                    LuaScriptTestForm.Instance.ExecuteLuaScript(strLuaFilename);
                }
                else
                {
                    LuaScriptTestForm.Instance.MessageText = "脚本文件" + strLuaFilename + "不存在." + "\r\n" + LuaScriptTestForm.Instance.MessageText;
                    return;
                }

            }
            //m_iCurrDialogNpc = iNpcID;
        }


        // 玩家得到物品
        public void GetItem(int iID)
        {
            // 遍历计数器,查找是否有搜集该物品的计数
            foreach (Counter ct in m_Counters)
            {
                if (ct.TargetStart<=iID && ct.TargetEnd>=iID)
                {
                    bool bAchieve = ct.AddCount(1);
                    if (bAchieve)   // 达成目标
                    {
                        SetMissionState(ct.MissionID, MissionState.MS_ACHIEVE_TARGET);
                        m_Counters.Remove(ct);
                    }
                }
            }

            Item item = ItemManager.Instance.GetItemByID(iID);
            if (item != null)
            {
                m_Items.Add(item);
            }
            else
            {
                MessageBox.Show("没有找到 ID 为 " + iID.ToString() + " 的物品");
            }

            // 刷新 UI 上的 Counter 列表
            LuaScriptTestForm.Instance.RefreshPlayerInfo(this);
        }

        public void RemoveItem(int iID)
        {
            foreach (Item item in m_Items)
            {
                if (item.ID == iID)
                {
                    m_Items.Remove(item);
                    LuaScriptTestForm.Instance.RefreshPlayerInfo(this);
                    return;
                }
            }
        }

        // 玩家是否有道具
        public bool HaveItem(int iID)
        {
            foreach (Item item in m_Items)
            {
                if (item.ID == iID)
                {
                    return true;
                }
            }
            return false;
        }

        // 是否没有
        public bool HaveNotItem(int iID)
        {
            foreach (Item item in m_Items)
            {
                if (item.ID == iID)
                {
                    return false;
                }
            }
            return true;
        }

        // 玩家杀死怪物
        public void KillMonster(int iID)
        {
            // 遍历计数器,查找是否有杀该怪的计数
        }

        public void CreateFollowing(int iNpcID)
        {
            FollowingList.Add(iNpcID);
            LuaScriptTestForm.Instance.RefreshPlayerInfo(this);
        }

        public void DelFollowing(int iNpcID)
        {
            if (Player.Instance.FollowingList.Contains(iNpcID))
            {
                Player.Instance.FollowingList.Remove(iNpcID);
                LuaScriptTestForm.Instance.RefreshPlayerInfo(this);
            }

        }

        public void SetMissionState(int iMissionID, MissionState state)
        {
            m_MissionStates[iMissionID] = state;
            LuaScriptTestForm.Instance.RefreshPlayerInfo(this);
        }

        public MissionState GetMissionState(int iMissionID)
        {
            if (m_MissionStates.ContainsKey(iMissionID))
            {
                return (MissionState)(m_MissionStates[iMissionID]);
            }
            else
            {
                return MissionState.MS_NEVER_ACCEPTED;
            }
        }

        // 为玩家添加计数器
        public void AddCounter(int iCounterType, int iTargetStart, int iTargetEnd, int iCount, int iMissionID)
        {
            Counter ct = new Counter(iCounterType, iTargetStart, iTargetEnd, iCount, iMissionID);
            m_Counters.Add(ct);
            LuaScriptTestForm.Instance.RefreshPlayerInfo(this);
        }

        #region 公有属性
        public Hashtable MissionStates
        {
            get
            {
                return m_MissionStates;
            }
        }

        public ArrayList CounterList
        {
            get
            {
                return m_Counters;
            }
        }

        public ArrayList ItemList
        {
            get
            {
                return m_Items;
            }
        }


        public ArrayList FollowingList
        {
            get
            {
                return m_FollowList;
            }
        }

        [CategoryAttribute("玩家属性"), DescriptionAttribute("等级")]
        public int Level
        {
            get
            {
                return m_iLevel;
            }
            set
            {
                m_iLevel = value;
            }
        }
        [CategoryAttribute("玩家属性"), DescriptionAttribute("钱")]
        public int Money
        {
            get
            {
                return m_iMoney;
            }
            set
            {
                m_iMoney = value;
            }
        }
        [CategoryAttribute("玩家属性"), DescriptionAttribute("经验")]
        public int Exp
        {
            get
            {
                return m_iExp;
            }
            set
            {
                m_iExp = value;
            }
        }

        [CategoryAttribute("玩家属性"), DescriptionAttribute("力量")]
        public int Strength
        {
            get
            {
                return m_iStrength;
            }
            set
            {
                m_iStrength = value;
            }
        }

        [CategoryAttribute("玩家属性"), DescriptionAttribute("敏捷")]
        public int Agility
        {
            get
            {
                return m_iAgility;
            }
            set
            {
                m_iAgility = value;
            }
        }

        [CategoryAttribute("玩家属性"), DescriptionAttribute("智力")]
        public int Intelligence
        {
            get
            {
                return m_iIntelligence;
            }
            set
            {
                m_iIntelligence = value;
            }
        }

        public static Player Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new Player();
                }

                return m_Instance;
            }
        }
        #endregion

        private static Player m_Instance = null;
        //private int m_iCurrDialogNpc;       // 当前对话目标 ID
        private Hashtable m_MissionStates = new Hashtable();    // (mission ID) - (mission state)
        private ArrayList m_Counters = new ArrayList();         // Counter 列表
        private ArrayList m_Items = new ArrayList();            // 物品列表
        private ArrayList m_FollowList = new ArrayList();       // 跟随玩家的 npc 列表
        private int m_iLevel = 1;                               // 玩家等级
        private int m_iMoney = 0;                               // 钱
        private int m_iExp = 0;                                 // 经验值
        private int m_iStrength = 0;		                    //力量
        private int m_iAgility = 0;		                        //敏捷
        private int m_iIntelligence = 0;	                    //智力

    }
}
