﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;

// 原子操作管理器
namespace MissionEditor
{
    // 封装了任务的原子操作
    class MissionAtomInstance : MissionBaseData
    {
        public MissionAtomInstance(XmlElement xml)
            : base(xml)
        {

        }
    };

    class AtomActionManager
    {
        public static AtomActionManager Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new AtomActionManager();
                }

                return m_Instance;
            }
        }

        private AtomActionManager()
        {}

        public void LoadFromXml(String strXmlFilename)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);

            XmlNode xmlNode = xmlDoc.FirstChild;

            while (xmlNode != null)
            {
                if (xmlNode is XmlElement && xmlNode.Name == "root")
                {
                    XmlElement xmlElemSubRoot = xmlNode.FirstChild as XmlElement;
                    while (xmlElemSubRoot != null)
                    {
                        if (xmlElemSubRoot.Name == "AtomActions")
                        {
                            XmlElement xmlElemAtomRoot = xmlElemSubRoot;
                            XmlElement xmlElemAtom = xmlElemAtomRoot.FirstChild as XmlElement;

                            while (xmlElemAtom != null)
                            {
                                //String strDisplay = xmlElemAtom.GetAttribute("Display");
                                //String strAtom = xmlElemAtom.GetAttribute("Atom");
                                //String strDesc = xmlElemAtom.GetAttribute("Desc");

                                //m_AtomActionList[strDisplay] = strAtom;
                                //m_AtomDescriptionList[strDisplay] = strDesc;
                                if (xmlElemAtom.HasAttribute("Display"))
                                {
                                    String strDisplay = xmlElemAtom.GetAttribute("Display");
                                    MissionAtomInstance mti = new MissionAtomInstance(xmlElemAtom);
                                    m_AtomActionList[strDisplay] = mti;
                                }

                                xmlElemAtom = xmlElemAtom.NextSibling as XmlElement;
                            }

                            break;
                        }

                        xmlElemSubRoot = xmlElemSubRoot.NextSibling as XmlElement;
                    }

                }

                xmlNode = xmlNode.NextSibling;
            }
        }

        public MissionAtomInstance GetAtomByDisplay(String strDisplay)
        {
            return m_AtomActionList[strDisplay] as MissionAtomInstance;
        }

        public Hashtable AtomActionList
        {
            get
            {
                return m_AtomActionList;
            }
        }

        //public Hashtable AtomDescList
        //{
        //    get
        //    {
        //        return m_AtomDescriptionList;
        //    }
        //}

        // 以下是静态函数，程序中获取某 Atom Action 。
        public static String Atom_ShowDialog
        {
            get
            {
                return "MuxLib.NpcShowMsg";
            }
        }
        public static String Display_ShowDialog
        {
            get
            {
                return "ShowDialog";
            }
        }
        public static String Atom_GiveTask
        {
            get
            {
                return "MuxLib.NpcGiveTask";
            }
        }
        public static String Display_GiveTask
        {
            get
            {
                return "GiveTask";
            }
        }
        public static String Atom_GiveItem
        {
            get
            {
                return "MuxLib.NpcGiveItem";
            }
        }
        public static String Display_GiveItem
        {
            get
            {
                return "GiveItem";
            }
        }
        // 选择性给与道具
        public static String Atom_GiveItemFrom
        {
            get
            {
                return "MuxLib.NpcGiveItemFrom";
            }
        }
        public static String Display_GiveItemFrom
        {
            get
            {
                return "GiveItemFrom";
            }
        }
        // 给玩家钱
        public static String Atom_GiveMoney
        {
            get
            {
                return "MuxLib.NpcGiveMoney";
            }
        }

        public static String Display_GiveMoney
        {
            get
            {
                return "GiveMoney";
            }
        }
        // 给玩家经验
        public static String Atom_GiveExp
        {
            get
            {
                return "MuxLib.NpcGiveExp";
            }
        }

        public static String Display_GiveExp
        {
            get
            {
                return "GiveExp";
            }
        }

        public static String Atom_RemoveItem
        {
            get
            {
                return "MuxLib.NpcRemoveItem";
            }
        }

        public static String Display_RemoveItem
        {
            get
            {
                return "RemoveItem";
            }
        }
        public static String Atom_ProcessTask
        {
            get
            {
                return "MuxLib.NpcProcessTask";
            }
        }

        public static String Display_ProcessTask
        {
            get
            {
                return "ProcessTask";
            }
        }
        public static String Atom_FinishTask
        {
            get
            {
                return "MuxLib.NpcFinishTask";
            }
        }

        public static String Display_FinishTask
        {
            get
            {
                return "FinishTask";
            }
        }

        public static String Atom_AddCounter
        {
            get
            {
                return "MuxLib.NpcAddCounter";
            }
        }
        public static String Display_AddCounter
        {
            get
            {
                return "AddCounter";
            }
        }
        public static String Atom_CloseUIInfo
        {
            get
            {
                return "NpcCloseUiInfo";
            }
        }
        public static String Display_CloseUIInfo
        {
            get
            {
                return "CloseUiInfo";
            }
        }
        // 创建一个跟随玩家的 NPC
        public static String Atom_NpcCreatePlayerNpc
        {
            get
            {
                return "MuxLib.NpcCreatePlayerNpc";
            }
        }
        public static String Display_NpcCreatePlayerNpc
        {
            get
            {
                return "NpcCreatePlayerNpc";
            }
        }
        // 检查是否有跟随玩家的NPC
        public static String Atom_NpcCheckPlayerNpc
        {
            get
            {
                return "MuxLib.NpcCheckPlayerNpc";
            }
        }
        public static String Display_NpcCheckPlayerNpc
        {
            get
            {
                return "NpcCheckPlayerNpc";
            }
        }
        // 删除跟随玩家的 NPC
        public static String Atom_NpcDelPlayerNpc
        {
            get
            {
                return "MuxLib.NpcDelPlayerNpc";
            }
        }
        public static String Display_NpcDelPlayerNpc
        {
            get
            {
                return "NpcDelPlayerNpc";
            }
        }
        // 显示商店
        public static String Atom_NpcShopShop
        {
            get
            {
                return "MuxLib.NpcShowShop";
            }
        }
        public static String Display_NpcShopShop
        {
            get
            {
                return "ShowShop";
            }
        }
        // 购买物品
        public static String Atom_NpcBuyItem
        {
            get
            {
                return "MuxLib.NpcBuyItem";
            }
        }
        public static String Display_NpcBuyItem
        {
            get
            {
                return "BuyItem";
            }
        }

        private static AtomActionManager m_Instance = null;
        private Hashtable m_AtomActionList = new Hashtable();     // String - String Display - Atom
        //private Hashtable m_AtomDescriptionList = new Hashtable();  // String - String Display - Description
    }
}
