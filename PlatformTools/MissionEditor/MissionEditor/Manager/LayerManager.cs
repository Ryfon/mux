﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using TextureTools;

namespace MissionEditor
{   
    public enum LayType : int 
    {
        未知        = 0000,
        单点NPC     = 1001,
        区域NPC     = 1002,
        音乐区      = 2101,
        音效特效区  = 2102,
        任务触发区  = 2201,
        安全区      = 2202,
        不可PK区    = 2203,
        不可骑乘区  = 2204,
        场景草图    = 9001,
        碰撞信息    = 9002,
        路径权重    = 9003,
        点音源      = 9004,
        网格        = 9005,
        寻路预判区  = 2205,
        BlockQuad区 = 2206,
    }

    public class LayerInfo
    {
        public LayType Init(SceneSketch scene)
        {
            // scene为空，直接返回
            if(scene == null)
            {
                return LayType.未知;
            }

            // 清除旧的数据
            m_ControlList.Clear();

            // 根据层的类型，进行初始化
            if (m_LayerType == LayType.单点NPC)
            {
                InitByNPC(scene, true);
            }
            else if(m_LayerType == LayType.区域NPC)
            {
                InitByNPC(scene, false);
            }
            else if(m_LayerType == LayType.音乐区)
            {
                InitByRegion(scene, RegionGroup.RegionType.客户端表现区域, RegionGroup.ClientRegionType.音乐触发区域, RegionGroup.ServerRegionType.任务触发区域);
            }
            else if(m_LayerType == LayType.音效特效区)
            {
                InitByRegion(scene, RegionGroup.RegionType.客户端表现区域, RegionGroup.ClientRegionType.音效特效触发区域, RegionGroup.ServerRegionType.任务触发区域);
            }
            else if(m_LayerType == LayType.任务触发区)
            {
                InitByRegion(scene, RegionGroup.RegionType.服务器相关区域, RegionGroup.ClientRegionType.音乐触发区域, RegionGroup.ServerRegionType.任务触发区域);
            }
            else if(m_LayerType == LayType.安全区)
            {
                InitByRegion(scene, RegionGroup.RegionType.服务器相关区域, RegionGroup.ClientRegionType.音乐触发区域, RegionGroup.ServerRegionType.安全区);
            }
            else if(m_LayerType == LayType.不可PK区)
            {
                InitByRegion(scene, RegionGroup.RegionType.服务器相关区域, RegionGroup.ClientRegionType.音乐触发区域, RegionGroup.ServerRegionType.不可PK区);
            }
            else if(m_LayerType == LayType.不可骑乘区)
            {
                InitByRegion(scene, RegionGroup.RegionType.服务器相关区域, RegionGroup.ClientRegionType.音乐触发区域, RegionGroup.ServerRegionType.不可骑乘区);
            }
            else if(m_LayerType == LayType.场景草图)
            {
                InitByImage(scene.PureSketchImage);
                m_bShow = true;
                m_fAlpha = 1.0f;
            }
            else if(m_LayerType == LayType.碰撞信息)
            {
                InitByImage(scene.PureBlockImage);
                m_fAlpha = 0.3f;
            }
            else if(m_LayerType == LayType.路径权重)
            {
                InitByImage(scene.PurePathImage);
                m_fAlpha = 0.3f;
            }
            else if(m_LayerType == LayType.点音源)
            {
                InitByPtSound(scene);
            }
            else if(m_LayerType == LayType.网格)
            {

            }
            else if(m_LayerType == LayType.寻路预判区)
            {
                InitByRegion(scene, RegionGroup.RegionType.服务器相关区域, RegionGroup.ClientRegionType.音乐触发区域, RegionGroup.ServerRegionType.寻路预判区);
            }
            else if(m_LayerType == LayType.BlockQuad区)
            {
                InitByRegion(scene, RegionGroup.RegionType.服务器相关区域, RegionGroup.ClientRegionType.音乐触发区域, RegionGroup.ServerRegionType.BlockQuad区);
            }

            // 返回层的类型
            return m_LayerType;
        }

        private void InitByNPC(SceneSketch sceneSketch, bool bIsSingle)
        {
            if (sceneSketch == null)
            {
                return;
            }
            ArrayList npcCreatorList = NPCManager.Instance.MapID2NPCCreatorList[sceneSketch.SceneID] as ArrayList;

            if (npcCreatorList != null)
            {
                foreach (NPCCreatorInfo creator in npcCreatorList)
                {
                    if(creator.IsSingle() == bIsSingle)
                    {
                        NPCCreatorControl ctrl = new NPCCreatorControl(creator, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                        if (creator.Hidden)
                        {
                            ctrl.Hide();
                        }
                        m_ControlList.Add(ctrl);
                    }
                }
            }

            m_LayerImage = new Bitmap(1024, 1024);
            m_bDirty = true;
        }

        private void InitByRegion(SceneSketch sceneSketch, RegionGroup.RegionType bigType, RegionGroup.ClientRegionType clientType, RegionGroup.ServerRegionType serverType)
        {
            if (sceneSketch == null)
            {
                return;
            }
            ArrayList regionList = RegionManager.Instance.MapID2RegionList[sceneSketch.SceneID] as ArrayList;

            if (regionList != null)
            {
                foreach (RegionInfo info in regionList)
                {
                    bool bIsFind = false;
                    if(info.Group == null)
                    {

                    }
                    else if(info.Group.BigType == bigType)
                    {
                        if(bigType == RegionGroup.RegionType.服务器相关区域)
                        {
                            if(info.Group.ServerType == serverType)
                            {
                                bIsFind = true;
                            }
                        }
                        else if(bigType == RegionGroup.RegionType.客户端表现区域)
                        {
                            if(info.Group.ClientType == clientType)
                            {
                                bIsFind = true;
                            }
                        }
                    }

                    if(bIsFind)
                    {
                        RegionControl rCtrl = new RegionControl(info, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                        if (info.Hidden)
                        {
                            rCtrl.Hide();
                        }
                        m_ControlList.Add(rCtrl);
                    }
                }
            }

            m_LayerImage = new Bitmap(1024, 1024);
            m_bDirty = true;
        }

        private void InitByPtSound(SceneSketch sceneSketch)
        {
            if (sceneSketch == null)
            {
                return;
            }
            PointSoundList ptSoundList = PointSoundManager.Instance.MapID2SoundList[sceneSketch.SceneID] as PointSoundList;
            if (ptSoundList != null)
            {
                foreach (PointSoundInfo soundInfo in ptSoundList.SoundInfoList)
                {
                    PointSoundControl sCtrl = new PointSoundControl(soundInfo, sceneSketch.SceneWidth, sceneSketch.SceneHeight);
                    m_ControlList.Add(sCtrl);
                }
            }

            m_LayerImage = new Bitmap(1024, 1024);
            m_bDirty = true;
        }

        private void InitByImage(Image image)
        {
            if(image != null)
            {
                m_LayerImage = image;
            }
        }

        public void IntoPalette(PictureBox palette)
        {
            if(palette != null)
            {
                // 清空老的数据
                palette.Controls.Clear();

                // 添加新的数据
                foreach(Control ctrl in m_ControlList)
                {
                    palette.Controls.Add(ctrl);
                }
            }
        }

        public Image RefreshLayerImage()
        {
            // 已经是最新的图片，直接返回
            if(!m_bDirty)
            {
                return m_LayerImage;
            }

            // 创建空的图片
            m_LayerImage = new Bitmap(1024, 1024);
            
            // 获取画刷
            Graphics g = Graphics.FromImage(m_LayerImage);

            // 遍历所有显示的控件
            foreach (Control ctrl in m_ControlList)
            {
                if (ctrl.Visible)
                {
                    BaseDrawControl baseDrawCtrl = ctrl as BaseDrawControl;
                    baseDrawCtrl.DrawOnBmp(g, m_LayerImage.Width, m_LayerImage.Height);
                }
            }

            // 将标识位设置为最新
            m_bDirty = false;

            // 返回结果
            //m_LayerImage.Save(m_LayerType.ToString() + "2" + ".jpg");
            return m_LayerImage;
        }
        //////////////////////////////////////////////////////////////////////////
        public LayerInfo(LayType type)
        {
            m_LayerType = type;
        }

        public LayType LayerType
        {
            get
            {
                return m_LayerType;
            }
        }

        public bool bShow
        {
            get
            {
                return m_bShow;
            }
            set
            {
                m_bShow = value;
            }
        }

        public float Alpha
        {
            get
            {
                return m_fAlpha;
            }
            set
            {
                m_fAlpha = value;
            }
        }

        public bool bDirty
        {
            get
            {
                return m_bDirty;
            }
            set
            {
                m_bDirty = value;
            }
        }

        public Image LayerImage
        {
            get
            {
                return m_LayerImage;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        private ArrayList m_ControlList = new ArrayList();  // 存放层上所有的控件
        private Image m_LayerImage = new Bitmap(1024, 1024);                  // 层所对应的草图
        private LayType m_LayerType = LayType.未知;         // 层的类型
        private bool m_bShow = false;                       // 是否显示该层
        private bool m_bDirty = true;                       // 用于控制是否需要重新刷新图片
        private float m_fAlpha = 0.7f;                      // 透明度，1.0为完全覆盖
    };

    public class LayerManager
    {
        public Image SetLayerVisiable(LayType type, bool bVisiable)
        {
            // 画板为空，直接返回
            if (m_Palette == null)
            {
                return null;
            }

            // 清空老的图层
            TextureMerger.Instance.ClearBitMaps();

            // 遍历设置每一个层
            //int iLayIndex = 0;
            foreach (LayerInfo layer in m_LayerList)
            {
                //if (iLayIndex == m_iTopLayerIndex)
                //{
                //    if(layer.LayerType == type)
                //    {
                //        if(bVisiable == false)
                //        {
                //            // 清空界面
                //            m_Palette.Controls.Clear();

                //            // 设置为非显示
                //            layer.bShow = bVisiable;

                //            // 将显示的非活动层加入Shader中混合
                //            TextureMerger.Instance.AddBitMap(layer.RefreshLayerImage(), layer.Alpha);

                //            // 设置m_iTopLayerIndex
                //            m_iTopLayerIndex = -1;
                //        }
                //    }
                //}
                //else
                //{
                    if (layer.LayerType == type)
                    {
                        // 设置显示
                        layer.bShow = bVisiable;
                    }

                    if (layer.bShow)
                    {
                        // 将显示的非活动层加入Shader中混合
                        TextureMerger.Instance.AddBitMap(layer.RefreshLayerImage(), layer.Alpha);
                    }
                //}
                //iLayIndex++;
            }

            // 混合后返回
            TextureMerger.Instance.Render(m_BlendImage);
            return m_BlendImage;
        }

        public Image SetTopLayer(int iSceneID, int topLayer)
        {
            LayerInfo top = m_LayerList[topLayer] as LayerInfo;
            return SetTopLayer(iSceneID, top.LayerType);
        }

        public Image SetTopLayer(int iSceneID, LayType topLayer)
        {
            // 画板为空，直接返回
            if (m_Palette == null)
            {
                return null;
            }

            // 获取场景信息
            SceneSketch scene = SceneSketchManager.Instance.GetSceneSketch(iSceneID);
            if (scene == null)
            {
                return null;
            }

            // 清空老的图层
            m_Palette.Controls.Clear();
            TextureMerger.Instance.ClearBitMaps();

            // 遍历设置每一个层
            int iLayIndex = 0;
            foreach (LayerInfo layer in m_LayerList)
            {
                if (layer.LayerType == topLayer)
                {
                    // 设置当前活动层
                    m_iTopLayerIndex = iLayIndex;

                    // 活动层一定是可见的
                    //layer.bShow = true;

                    // 考虑缩放问题，重新Init一下
                    layer.Init(scene);

                    // 将活动层中的控件，放入画板中
                    if(layer.LayerType == LayType.路径权重)
                    {
                        TextureMerger.Instance.AddBitMap(layer.RefreshLayerImage(), layer.Alpha);
                    }
                    else
                    {
                        layer.IntoPalette(m_Palette);
                        // 设置Dirty位
                        layer.bDirty = true;
                    }
                }
                else
                {
                    if (layer.bShow)
                    {
                        // 考虑缩放问题，重新Init一下
                        layer.Init(scene);

                        // 将显示的非活动层加入Shader中混合
                        TextureMerger.Instance.AddBitMap(layer.RefreshLayerImage(), layer.Alpha);
                    }
                }
                iLayIndex++;
            }

            // 混合后返回
            TextureMerger.Instance.Render(m_BlendImage);
            return m_BlendImage;
        }

        public void SetLayerDirty(LayType type)
        {
            foreach (LayerInfo layer in m_LayerList)
            {
                if(layer.LayerType == type)
                {
                    layer.bDirty = true;
                }
            }
        }

        public Image LoadScene(int iSceneID, LayType topLayer)
        {
            // 画板为空，直接返回
            if(m_Palette == null)
            {
                return null;
            }

            // 获取场景信息
            SceneSketch scene = SceneSketchManager.Instance.GetSceneSketch(iSceneID);
            if(scene == null)
            {
                return null;
            }

            // 设置ID
            m_iCurrSceneID = iSceneID;

            // 清空老的图层
            m_Palette.Controls.Clear();
            TextureMerger.Instance.ClearBitMaps();

            // 遍历设置每一个层
            int iLayIndex = 0;
            foreach(LayerInfo layer in m_LayerList)
            {
                if (layer.Init(scene) == topLayer)
                {
                    // 设置当前活动层
                    m_iTopLayerIndex = iLayIndex;
                    
                    // 绘制活动层的图片
                    layer.RefreshLayerImage();

                    // 将活动层中的控件，放入画板中
                    layer.IntoPalette(m_Palette);
                }
                else
                {
                    if(layer.bShow)
                    {
                        // 将显示的非活动层加入Shader中混合
                        Image subImage = layer.RefreshLayerImage();
                        //subImage.Save(layer.LayerType.ToString() + ".jpg");
                        TextureMerger.Instance.AddBitMap(subImage, layer.Alpha);
                    }
                }
                iLayIndex++;
            }

            // 混合后返回
            TextureMerger.Instance.Render(m_BlendImage);
            return m_BlendImage;
        }
        //////////////////////////////////////////////////////////////////////////
        private LayerManager() 
        {
            m_LayerList.Add(new LayerInfo(LayType.场景草图));
            m_LayerList.Add(new LayerInfo(LayType.碰撞信息));
            m_LayerList.Add(new LayerInfo(LayType.路径权重));
            
            m_LayerList.Add(new LayerInfo(LayType.音乐区));
            m_LayerList.Add(new LayerInfo(LayType.音效特效区));

            m_LayerList.Add(new LayerInfo(LayType.任务触发区));
            m_LayerList.Add(new LayerInfo(LayType.安全区));
            m_LayerList.Add(new LayerInfo(LayType.不可PK区));
            m_LayerList.Add(new LayerInfo(LayType.不可骑乘区));

            m_LayerList.Add(new LayerInfo(LayType.单点NPC));
            m_LayerList.Add(new LayerInfo(LayType.区域NPC));

            m_LayerList.Add(new LayerInfo(LayType.点音源));
            m_LayerList.Add(new LayerInfo(LayType.网格));

            m_LayerList.Add(new LayerInfo(LayType.寻路预判区));

            m_LayerList.Add(new LayerInfo(LayType.BlockQuad区));
        }

        public static LayerManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new LayerManager();
                }
                return ms_Instance;
            }
        }

        public PictureBox Palatte
        {
            set
            {
                m_Palette = value;
            }
        }

        public int CurrSceneID
        {
            get
            {
                return m_iCurrSceneID;
            }
        }

        public Image BlendImage
        {
            get
            {
                return m_BlendImage;
            }
        }

        public ArrayList LayerList
        {
            get
            {
                return m_LayerList;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        private int m_iCurrSceneID = 0; // 当前的场景ID
        private static LayerManager ms_Instance = null; // 单件模式，唯一的实例
        private ArrayList m_LayerList = new ArrayList();    // 存放所有的层
        private int m_iTopLayerIndex = 0;   // 当前正在编辑层的索引值
        private PictureBox m_Palette = null;    // 保存画板的引用
        private Image m_BlendImage = new Bitmap(1024, 1024);       // 非活动层混合后的背景图片
    }
}
