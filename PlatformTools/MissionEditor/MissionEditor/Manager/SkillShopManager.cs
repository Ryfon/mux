﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace MissionEditor
{
    // 商店中的技能
    public class ShopSkill
    {
        public ShopSkill(int iSkillID, int iLevel, String strTradePrice, String strProfession)
        {
            m_iSkillID = iSkillID;
            m_iLevel = iLevel;
            m_strTradePrice = strTradePrice;
            m_strProfession = strProfession;
        }

        public int SkillID
        {
            get
            {
                return m_iSkillID;
            }
            set
            {
                m_iSkillID = value;
            }
        }

        public int Level
        {
            get
            {
                return m_iLevel;
            }
            set
            {
                m_iLevel = value;
            }
        }

        public String TradePrice
        {
            get
            {
                return m_strTradePrice;
            }
            set
            {
                m_strTradePrice = value;
            }
        }

        public String Profession
        {
            get
            {
                return m_strProfession;
            }
            set
            {
                m_strProfession = value;
            }
        }

        private int m_iSkillID;             // 技能 ID
        private int m_iLevel;               // 所需最小等级
        private String m_strTradePrice;     // 交易价格
        private String m_strProfession;     // 学习该技能所需的职业
    };

    // 技能商店
    public class SkillShop
    {
        public SkillShop(int iID, String strDesc, float fPriceRate, CurrencyType eType, int iTradeItemID)
        {
            m_iID = iID;
            m_strDesc = strDesc;
            m_fPriceRate = fPriceRate;
            m_eCurrencyType = eType;
            m_iTradeItemID = iTradeItemID;
        }

        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        public String Desc
        {
            get
            {
                return m_strDesc;
            }
            set
            {
                m_strDesc = value;
            }
        }

        public float PriceRate
        {
            get
            {
                return m_fPriceRate;
            }
            set
            {
                m_fPriceRate = value;
            }
        }

        public CurrencyType ShopCurrencyType
        {
            get
            {
                return m_eCurrencyType;
            }
            set
            {
                m_eCurrencyType = value;
            }
        }

        public int TradeItemID
        {
            get
            {
                return m_iTradeItemID;
            }
            set
            {
                m_iTradeItemID = value;
            }
        }

        public Hashtable Tables
        {
            get
            {
                return m_Tables;
            }
        }

        public Hashtable IndexTables
        {
            get
            {
                return m_IndexTables;
            }
        }

        private int m_iID;          // 商店 ID
        private String m_strDesc;   // 描述
        private float m_fPriceRate; // 价格比率
        private CurrencyType m_eCurrencyType;   // 交易类型
        private int m_iTradeItemID; // 交换物 ID
        private Hashtable m_Tables = new Hashtable();   // String - Hashtable
        private Hashtable m_IndexTables = new Hashtable();  // String - ArrayList
    };

    public class SkillShopManager
    {


        // 将所有商店保存到 xml
        public void SaveToXml(String strXmlFilename)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", null);
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement elmtShopRoot = xmlDoc.CreateElement("ShopRoot");
            xmlDoc.AppendChild(elmtShopRoot);
            // 添加 skill shop
            foreach (SkillShop shop in m_Shops.Values)
            {
                XmlElement elmtShop = xmlDoc.CreateElement("SkillShop");
                elmtShop.SetAttribute("ID", shop.ID.ToString());
                elmtShop.SetAttribute("Desc", shop.Desc);
                elmtShop.SetAttribute("PriceRate", shop.PriceRate.ToString());
                elmtShop.SetAttribute("CurrencyType", ((int)shop.ShopCurrencyType).ToString());
                elmtShop.SetAttribute("TradeItemID", shop.TradeItemID.ToString());
                elmtShopRoot.AppendChild(elmtShop);

                // 添加 tables
                foreach (String strTabName in shop.Tables.Keys)
                {
                    XmlElement elmtTable = xmlDoc.CreateElement("Label");
                    elmtTable.SetAttribute("Name", strTabName);
                    elmtShop.AppendChild(elmtTable);

                    // 添加 skill
                    Hashtable skillList = shop.Tables[strTabName] as Hashtable;
                    ArrayList indexList = shop.IndexTables[strTabName] as ArrayList;
                    if(indexList != null)
                    {
                        foreach(int iSkillID in indexList)
                        {
                            ShopSkill skill = skillList[iSkillID] as ShopSkill;
                            XmlElement elmtSkill = xmlDoc.CreateElement("Skill");
                            elmtSkill.SetAttribute("SkillID", skill.SkillID.ToString());
                            //elmtSkill.SetAttribute("Level", skill.Level.ToString());
                            elmtSkill.SetAttribute("TradePrice", skill.TradePrice);
                            //elmtSkill.SetAttribute("Profession", skill.Profession);
                            elmtTable.AppendChild(elmtSkill);
                        }
                    }
                    else
                    {
                        foreach (ShopSkill skill in skillList.Values)
                        {
                            //private int m_iSkillID; // 技能 ID
                            //private int m_iLevel;   // 所需最小等级
                            //private String m_strTradePrice;   // 交易价格

                            XmlElement elmtSkill = xmlDoc.CreateElement("Skill");
                            elmtSkill.SetAttribute("SkillID", skill.SkillID.ToString());
                            //elmtSkill.SetAttribute("Level", skill.Level.ToString());
                            elmtSkill.SetAttribute("TradePrice", skill.TradePrice);
                            //elmtSkill.SetAttribute("Profession", skill.Profession);
                            elmtTable.AppendChild(elmtSkill);
                        }
                    }    
                }
            }

            xmlDoc.Save(strXmlFilename);
        }

        // 从xml 中载入技能商店
        public void LoadFromXml(String strXmlFilename)
        {
            m_Shops.Clear();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strXmlFilename);

                XmlNode xmlNode = xmlDoc.FirstChild;
                // 找到 ShopRoot
                while (!xmlNode.Name.Equals("ShopRoot"))
                {
                    xmlNode = xmlNode.NextSibling;
                }

                // 遍历所有 SkillShop
                XmlElement elmtShop = xmlNode.FirstChild as XmlElement;
                while (elmtShop != null)
                {
                    int iID = int.Parse(elmtShop.GetAttribute("ID"));
                    String strDesc = elmtShop.GetAttribute("Desc");
                    float fPriceRate = float.Parse(elmtShop.GetAttribute("PriceRate"));
                    CurrencyType eCurrencyType = (CurrencyType)int.Parse(elmtShop.GetAttribute("CurrencyType"));
                    int iTradeItemID = int.Parse(elmtShop.GetAttribute("TradeItemID"));

                    SkillShop shop = new SkillShop(iID, strDesc, fPriceRate, eCurrencyType, iTradeItemID);
                    m_Shops[iID] = shop;

                    // 遍历 tables
                    XmlElement elmtTable = elmtShop.FirstChild as XmlElement;
                    while (elmtTable != null)
                    {
                        // 添加 table 到 skill shop
                        String strTabName = elmtTable.GetAttribute("Name");
                        Hashtable skillList = new Hashtable();
                        shop.Tables[strTabName] = skillList;
                        // 添加索引list
                        ArrayList indexList = new ArrayList();
                        shop.IndexTables[strTabName] = indexList;

                        // 遍历 table 中的所有 shopSkill
                        XmlElement elmtSkill = elmtTable.FirstChild as XmlElement;
                        while (elmtSkill != null)
                        {
                            int iSkillID = 0;
                            iSkillID = int.Parse(elmtSkill.GetAttribute("SkillID"));

                            int iLevel = 0;
                            //iLevel = int.Parse(elmtSkill.GetAttribute("Level"));

                            String strTradePrice = "";
                            strTradePrice = elmtSkill.GetAttribute("TradePrice");

                            String strProfession = "0";
                            //if(elmtSkill.HasAttribute("Profession"))
                            //{
                            //    strProfession = elmtSkill.GetAttribute("Profession");
                            //}

                            ShopSkill shopSkill = new ShopSkill(iSkillID, iLevel, strTradePrice, strProfession);
                            skillList[iSkillID] = shopSkill;
                            indexList.Add(iSkillID);
                            elmtSkill = elmtSkill.NextSibling as XmlElement;
                        }
                        elmtTable = elmtTable.NextSibling as XmlElement;
                    }
                    elmtShop = elmtShop.NextSibling as XmlElement;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        // 从外部xml中载入技能商店
        public void LoadFromExternalXml(String strXmlFilename)
        {
            // 创建一个新的商店列表
            Hashtable newShopList = new Hashtable();

            // 开始读取
            try
            {
                // 加载文件
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strXmlFilename);

                // 获取节点
                XmlElement elmtRoot = xmlDoc.LastChild as XmlElement;
                XmlElement elmtShop = elmtRoot.FirstChild as XmlElement;

                // 循环遍历
                while (elmtShop != null)
                {
                    // 商店节点
                    if (elmtShop.Name.Equals("Shop"))
                    {
                        // 获取节点的属性
                        int iShopID = int.Parse(elmtShop.GetAttribute("ID"));
                        String strDesc = elmtShop.GetAttribute("Desc");
                        float fPriceRate = float.Parse(elmtShop.GetAttribute("PriceRate"));
                        CurrencyType eCurrencyType = (CurrencyType)int.Parse(elmtShop.GetAttribute("CurrencyType"));
                        int iTradeItemID = int.Parse(elmtShop.GetAttribute("TradeItemID"));

                        // 生成一个新的商店，并加入商店列表hashtable
                        SkillShop shop = new SkillShop(iShopID, strDesc, fPriceRate, eCurrencyType, iTradeItemID);
                        newShopList[iShopID] = shop;
                    }

                    // 物品节点
                    if (elmtShop.Name.Equals("ShopSkill"))
                    {
                        // 获取技能所属的shop和table的属性
                        int iShopID = int.Parse(elmtShop.GetAttribute("ID"));
                        String strTableName = elmtShop.GetAttribute("Name");

                        // 寻找商店
                        SkillShop shop = newShopList[iShopID] as SkillShop;
                        Hashtable table = null;
                        ArrayList indexList = null;
                        if (shop == null)
                        {
                            // 如果商店不存在，直接跳过，遍历下一个节点
                            elmtShop = elmtShop.NextSibling as XmlElement;
                            continue;
                        }
                        else
                        {
                            // 寻找table
                            table = shop.Tables[strTableName] as Hashtable;
                            if (table == null)
                            {
                                // 如果table不存在，则创建table
                                table = new Hashtable();
                                shop.Tables[strTableName] = table;

                                indexList = new ArrayList();
                                shop.IndexTables[strTableName] = indexList;
                            }
                            else
                            {
                                indexList = shop.IndexTables[strTableName] as ArrayList;
                            }
                        }

                        String strSkillID = elmtShop.GetAttribute("SkillID");
                        if (strSkillID.Length != 0)
                        {
                            // 获取技能属性
                            //int iSkillID = int.Parse(elmtShop.GetAttribute("SkillID"));
                            //int iLevel = int.Parse(elmtShop.GetAttribute("Level"));
                            //String strTradePrice = elmtShop.GetAttribute("TradePrice");
                            //String strProfession = elmtShop.GetAttribute("Profession");
                            int iSkillID = 0;
                            iSkillID = int.Parse(elmtShop.GetAttribute("SkillID"));

                            int iLevel = 0;

                            String strTradePrice = "";
                            strTradePrice = elmtShop.GetAttribute("TradePrice");

                            String strProfession = "0";

                            // 将物品加入table
                            ShopSkill shopSkill = new ShopSkill(iSkillID, iLevel, strTradePrice, strProfession);
                            table[iSkillID] = shopSkill;
                            indexList.Add(iSkillID);
                        }
                    }

                    // 继续遍历下一个节点
                    elmtShop = elmtShop.NextSibling as XmlElement;
                }

                // 清空原先的数据
                m_Shops.Clear();
                m_Shops = newShopList;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        // 添加一个技能商店
        public void AddSkillShop(SkillShop shop)
        {
            m_Shops[shop.ID] = shop;
        }

        public Hashtable ShopList
        {
            get
            {
                return m_Shops;
            }
        }

        public static SkillShopManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new SkillShopManager();
                }

                return ms_Instance;
            }
        }

        private static SkillShopManager ms_Instance = null; 
        private Hashtable m_Shops = new Hashtable();
    }
}
