﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;

namespace MissionEditor
{
    public class PointSoundInfo
    {
        //////////////////////////////////////////////////////////////////////////
        #region 接口
        public bool LoadFromConfXml(XmlElement node)
        {
            if (node == null)
            {
                return false;
            }
            else
            {
                // nif文件
                if (node.HasAttribute("ID"))
                {
                    m_strNifName = node.GetAttribute("ID");
                }
                else
                {
                    return false;
                }

                // sound文件
                if (node.HasAttribute("SoundFile"))
                {
                    m_strSoundName = node.GetAttribute("SoundFile");
                }
                else
                {
                    return false;
                }

                // Volume
                if (node.HasAttribute("Volume"))
                {
                    m_strVolume = node.GetAttribute("Volume");
                }

                // Loop
                if (node.HasAttribute("Loop"))
                {
                    m_strLoop = node.GetAttribute("Loop");
                }

                // OuterRadius
                if (node.HasAttribute("OuterRadius"))
                {
                    m_strOuterRadius = node.GetAttribute("OuterRadius");
                }

                // InnerRadius
                if (node.HasAttribute("InnerRadius"))
                {
                    m_strInnerRadius = node.GetAttribute("InnerRadius");
                }

                return true;
            }
        }

        public XmlElement ToEntityInfoNode(XmlDocument xmlDoc)
        {
            XmlElement result = xmlDoc.CreateElement("Entity_BGMusic");

            result.SetAttribute("EntityType", "BGMusic");
            result.SetAttribute("Filename", m_strSoundName);
            result.SetAttribute("Translation", m_strTranslation);
            result.SetAttribute("Radius", m_strOuterRadius);
            result.SetAttribute("InnerRadius", m_strInnerRadius);
            result.SetAttribute("Loop", m_strLoop);
            result.SetAttribute("Volume", m_strVolume);
            result.SetAttribute("EntityID", m_iEntityID.ToString());

            return result;
        }

        public XmlElement ToSceneInfoNode(XmlDocument xmlDoc)
        {
            XmlElement result = xmlDoc.CreateElement("BGMusic");

            result.SetAttribute("Filename", m_strSoundName);
            result.SetAttribute("Translation", m_strTranslation);
            result.SetAttribute("Radius", m_strOuterRadius);
            result.SetAttribute("InnerRadius", m_strInnerRadius);
            result.SetAttribute("Loop", m_strLoop);
            result.SetAttribute("Volume", m_strVolume);

            return result;
        }

        public PointSoundInfo Clone()
        {
            PointSoundInfo info = new PointSoundInfo();

            info.NifName = m_strNifName;
            info.SoundName = m_strSoundName;
            info.Translation = m_strTranslation;
            info.InnerRadius = m_strInnerRadius;
            info.OuterRadius = m_strOuterRadius;       // 外半径
            info.Loop = m_strLoop;               // 是否循环播放
            info.Volume = m_strVolume;           // 音量大小
            info.EntityID = m_iEntityID;
            info.BrotherEntityID = m_iBrotherEntityID;             // 关联的EntityID，用于更新SceneInfo.xml   

            return info;
        }

        public Point GetPosition()
        {
            Point position = new Point();

            int i1st = m_strTranslation.IndexOf(",");
            int i2nd = m_strTranslation.LastIndexOf(",");
            String strX = m_strTranslation.Substring(0, i1st);
            String strY = m_strTranslation.Substring(i1st + 1, i2nd - i1st - 1);
            float fX = -1;
            float fY = -1;
            if (float.TryParse(strX, out fX) && float.TryParse(strY, out fY))
            {
                position.X = (int)fX;
                position.Y = (int)fY;
            }
            else
            {
                position.X = -1;
                position.Y = -1;
            }

            return position;
        }

        public String GetDisplayName()
        {
            String strDisplay = "";

            // 获取不带路径的音效文件名
            int iIndex = m_strSoundName.LastIndexOf("\\");
            String strShortName = m_strSoundName.Substring(iIndex + 1, m_strSoundName.Length - iIndex - 1);

            // 合并
            strDisplay = m_strNifName + "(" + strShortName + ")";

            return strDisplay;
        }

        #endregion
        //////////////////////////////////////////////////////////////////////////
        #region 属性
        public String NifName
        {
            get
            {
                return m_strNifName;
            }
            set
            {
                m_strNifName = value;
            }
        }
        public String SoundName
        {
            get
            {
                return m_strSoundName;
            }
            set
            {
                m_strSoundName = value;
            }
        }
        public String Translation
        {
            get
            {
                return m_strTranslation;
            }
            set
            {
                m_strTranslation = value;
            }
        }
        public String InnerRadius
        {
            get
            {
                return m_strInnerRadius;
            }
            set
            {
                m_strInnerRadius = value;
            }
        }
        public String OuterRadius
        {
            get
            {
                return m_strOuterRadius;
            }
            set
            {
                m_strOuterRadius = value;
            }
        }
        public String Loop
        {
            get
            {
                return m_strLoop;
            }
            set
            {
                m_strLoop = value;
            }
        }
        public String Volume
        {
            get
            {
                return m_strVolume;
            }
            set
            {
                m_strVolume = value;
            }
        }
        public int EntityID
        {
            get
            {
                return m_iEntityID;
            }
            set
            {
                m_iEntityID = value;
            }
        }
        public int BrotherEntityID
        {
            get
            {
                return m_iBrotherEntityID;
            }
            set
            {
                m_iBrotherEntityID = value;
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////
        #region 私有成员变量
        private String m_strNifName = "";             // 模型nif文件的名称
        private String m_strSoundName = "";           // 音乐文件的名称
        private String m_strTranslation = "0,0,0";    // 坐标点位置
        private String m_strInnerRadius = "5";        // 内半径
        private String m_strOuterRadius = "10";       // 外半径
        private String m_strLoop = "1";               // 是否循环播放
        private String m_strVolume = "1.0";           // 音量大小
        private int m_iEntityID = 0;
        private int m_iBrotherEntityID = 0;             // 关联的EntityID，用于更新SceneInfo.xml            
        #endregion
    }

    class PointSoundList
    {
        //////////////////////////////////////////////////////////////////////////
        #region 外部接口
        public PointSoundList(int iMapID)
        {
            m_iMapID = iMapID;
        }

        public bool Convert(String EntityInfo, String SceneInfo, ArrayList ptConf)
        {
            // 处理EntityInfo.xml
            if (!ConvertEntityInfo(EntityInfo, ptConf))
            {
                return false;
            }

            // 处理SceneInfo.xml
            //if (!ConvertSceneInfo(SceneInfo))
            //{
            //    return false;
            //}

            return true;
        }

        public void Export(String EntityInfo, String SceneInfo)
        {
            if(m_xmlDocEntityInfo != null)
            {
                m_xmlDocEntityInfo.Save(EntityInfo);
            }

            //if(m_xmlDocSceneInfo != null)
            //{
            //    m_xmlDocSceneInfo.Save(SceneInfo);
            //}
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////
        #region 内部函数
        private bool ConvertEntityInfo(String EntityInfo, ArrayList ptConf)
        {
            // 清空旧的数据
            int iMaxEntityID = -1;
            m_SoundInfoList.Clear();

            // XmlDoc
            m_xmlDocEntityInfo = new XmlDocument();
            m_xmlDocEntityInfo.Load(EntityInfo);

            // root
            XmlElement xmlRoot = m_xmlDocEntityInfo.LastChild as XmlElement;
            if (xmlRoot == null)
            {
                return false;
            }

            // EntityRoot
            XmlElement xmlEntityRoot = null;
            foreach (XmlElement node in xmlRoot.ChildNodes)
            {
                if (node.Name.Equals("EntityRoot"))
                {
                    xmlEntityRoot = node;
                    break;
                }
            }
            if (xmlEntityRoot == null)
            {
                return false;
            }

            // 遍历获取
            for (XmlElement xmlEntity = xmlEntityRoot.FirstChild as XmlElement; xmlEntity != null; xmlEntity = xmlEntity.NextSibling as XmlElement)
            {
                int iEntityID = 0;
                if (int.TryParse(xmlEntity.GetAttribute("EntityID"), out iEntityID))
                {
                    // 最大ID更新
                    if (iEntityID > iMaxEntityID)
                    {
                        iMaxEntityID = iEntityID;
                    }

                    // nif文件名
                    String strFullName = xmlEntity.GetAttribute("NIF_File_Path");
                    int iIndex = strFullName.LastIndexOf("\\");
                    String strShortName = strFullName.Substring(iIndex + 1, strFullName.Length - iIndex - 1);

                    // 遍历寻找是否为要发声的物件
                    foreach (PointSoundInfo conf in ptConf)
                    {
                        if (conf.NifName.Equals(strShortName))
                        {
                            PointSoundInfo newInfo = conf.Clone();
                            newInfo.BrotherEntityID = iEntityID;
                            newInfo.Translation = xmlEntity.GetAttribute("Translation");
                            m_SoundInfoList.Add(newInfo);
                        }
                    }
                }
            }

            // 给新的BGMusic节点分配ID，并更新EntityInfo.xml
            foreach(PointSoundInfo BGMusic in m_SoundInfoList)
            {
                // 分配ID
                BGMusic.EntityID = iMaxEntityID + 1;
                iMaxEntityID = BGMusic.EntityID;

                // 生成xml节点
                XmlElement xmlBGMusic = BGMusic.ToEntityInfoNode(m_xmlDocEntityInfo);

                // 加入EntityRoot中
                xmlEntityRoot.AppendChild(xmlBGMusic);
            }

            return true;
        }

        private bool ConvertSceneInfo(String SceneInfo)
        {
            // xmlDoc
            m_xmlDocSceneInfo = new XmlDocument();
            m_xmlDocSceneInfo.Load(SceneInfo);

            // Root
            XmlElement xmlRoot = m_xmlDocSceneInfo.LastChild as XmlElement;
            if (xmlRoot == null)
            {
                return false;
            }

            // EntityInfoRoot BGMusicRoot
            XmlElement xmlEntityInfoRoot = null;
            XmlElement xmlBGMusicRoot = null;
            foreach(XmlElement node in xmlRoot.ChildNodes)
            {
                if (node.Name.Equals("EntityInfoRoot"))
                {
                    xmlEntityInfoRoot = node;
                }
                else if (node.Name.Equals("BGMusicRoot"))
                {
                    xmlBGMusicRoot = node;
                }
            }
            if(xmlEntityInfoRoot == null || xmlBGMusicRoot == null)
            {
                return false;
            }

            // 遍历EntityInfoRoot
            foreach(XmlElement xmlChunkEntity in xmlEntityInfoRoot)
            {
                if(xmlChunkEntity.HasChildNodes)
                {
                    XmlElement xmlNodeRoot = xmlChunkEntity.FirstChild as XmlElement;
                    foreach (PointSoundInfo BGMusicEntity in m_SoundInfoList)
                    {
                        if (HasBrother(xmlNodeRoot, BGMusicEntity.BrotherEntityID))
                        {
                            // 生成xml节点
                            XmlElement xmlNewEntity = m_xmlDocSceneInfo.CreateElement("Entity");
                            xmlNewEntity.SetAttribute("EntityID", BGMusicEntity.EntityID.ToString());

                            // 加入EntityRoot中
                            xmlNodeRoot.InsertBefore(xmlNewEntity, xmlNodeRoot.FirstChild);
                        }
                    }
                }
            }

            // 填写BGMusicRoot
            foreach (PointSoundInfo BGMusic in m_SoundInfoList)
            {               
                // 生成xml节点
                XmlElement xmlBGMusic = BGMusic.ToSceneInfoNode(m_xmlDocSceneInfo);

                // 加入EntityRoot中
                xmlBGMusicRoot.AppendChild(xmlBGMusic);
            }

            return true;
        }

        private bool HasBrother(XmlElement node, int iBrotherID)
        {
            // 判断本身
            if (node.Name.Equals("Entity"))
            {
                if (node.HasAttribute("EntityID"))
                {
                    int id = 0;
                    if (int.TryParse(node.GetAttribute("EntityID"), out id))
                    {
                        if(id == iBrotherID)
                        {
                            return true;
                        }
                    }
                }
            }

            // 判断子节点
            foreach(XmlElement child in node.ChildNodes)
            {
                if (HasBrother(child, iBrotherID))
                {
                    return true;
                }
            }

            return false;
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////
        #region 属性
        public int MapID
        {
            get
            {
                return m_iMapID;
            }
            set
            {
                m_iMapID = value;
            }
        }
        public ArrayList SoundInfoList
        {
            get
            {
                return m_SoundInfoList;
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////
        #region 私有成员变量
        private int m_iMapID = 0;   // 地图号
        private ArrayList m_SoundInfoList = new ArrayList();    // 点音源列表
        private XmlDocument m_xmlDocEntityInfo = null;          // EntityInfo.xml
        private XmlDocument m_xmlDocSceneInfo = null;          // EntityInfo.xml
        #endregion
    }

    class PointSoundManager
    {
        //////////////////////////////////////////////////////////////////////////
        #region 接口
        public void LoadAll(String strDirPath)
        {
            if (!Directory.Exists(strDirPath))
            {
                MessageBox.Show("无法加载点音源信息，文件夹" + strDirPath + "不存在。");
                return;
            }

            // 清空旧的数据
            m_MapID2SoundList.Clear();

            // 遍历文件夹，取消只读属性
            AssertFileAttribute(strDirPath);

            // 首先读取点音源总配置表
            String strPtSoundConf = strDirPath + "PtSoundConf.xml";
            if (!File.Exists(strPtSoundConf))
            {
                MessageBox.Show("无法加载点音源文件" + strPtSoundConf);
                return;
            }
            else
            {
                LoadPtSoundConf(strPtSoundConf);
            }

            // 遍历子文件夹
            String strClientDir = strDirPath + "TerrainScene/";
            foreach (String strMapDir in Directory.GetDirectories(strClientDir))
            {
                String strMapID = strMapDir.Replace(strClientDir, "");
                int iMapID = 0;
                if (strMapID.Length == 4 && int.TryParse(strMapID, out iMapID))
                {
                    PointSoundList newList = new PointSoundList(iMapID);
                    if (newList.Convert(strMapDir + "/EntityInfo.xml", strMapDir + "/SceneInfo.xml", m_PtSoundConfList))
                    {
                        m_MapID2SoundList[newList.MapID] = newList;
                    }
                }
            }
        }

        public void ExportAll(String strExportPath)
        {
             String path = strExportPath;

            // 根目录的创建
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // 创建客户端的TerrainScene文件夹
            path += "TerrainScene/";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // 为所有的scene创建独立的文件夹
            foreach (int iSceneID in m_MapID2SoundList.Keys)
            {
                // 创建文件夹
                String strSceneDir = path + iSceneID.ToString() + "/";
                if (!Directory.Exists(strSceneDir))
                {
                    Directory.CreateDirectory(strSceneDir);
                }

                // 写入文件
                PointSoundList list = m_MapID2SoundList[iSceneID] as PointSoundList;
                list.Export(strSceneDir + "EntityInfo.xml", strSceneDir + "SceneInfo.xml");
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////
        #region 私有函数
        private PointSoundManager() { }// 私有的构造函数

        private void AssertFileAttribute(String strDirectory)// 去掉文件的只读属性
        {
            // 递归遍历 strDirectory 目录,确定该目录下所有文件没有只读属性的.
            String[] aFiles = System.IO.Directory.GetFiles(strDirectory);
            foreach (String strFile in aFiles)
            {
                System.IO.File.SetAttributes(strFile, System.IO.FileAttributes.Normal);
            }

            // 递归遍历下一级目录
            String[] aDirectories = System.IO.Directory.GetDirectories(strDirectory);
            foreach (String strDir in aDirectories)
            {
                AssertFileAttribute(strDir);
            }
        }

        private void LoadPtSoundConf(String strFileName)
        {
            // 清空旧的数据
            m_PtSoundConfList.Clear();

            // XmlDoc
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strFileName);

            // root
            XmlElement xmlRoot = xmlDoc.LastChild as XmlElement;
            if (xmlRoot == null)
            {
                return;
            }

            // 遍历
            XmlElement xmlConf = xmlRoot.FirstChild as XmlElement;
            while (xmlConf != null)
            {
                PointSoundInfo info = new PointSoundInfo();
                if (info.LoadFromConfXml(xmlConf))
                {
                    m_PtSoundConfList.Add(info);
                }
                // 下一个节点
                xmlConf = xmlConf.NextSibling as XmlElement;
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////
        #region 属性
        public static PointSoundManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new PointSoundManager();
                }
                return ms_Instance;
            }
        }
        public Hashtable MapID2SoundList
        {
            get
            {
                return m_MapID2SoundList;
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////
        #region 私有成员变量
        private static PointSoundManager ms_Instance = null;    // 实例引用，用于实现单件模式
        private Hashtable m_MapID2SoundList = new Hashtable();  // 地图号---点音源列表 之间的映射
        private ArrayList m_PtSoundConfList = new ArrayList();
        #endregion
    }
}
