﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;

// 关于 字符串 ID 的规则：
// NPC对话：1 + 5位NPCID + 3位流水号
// NPC死亡：2 + 5位NPCID + 3位流水号
// 怪物死亡：3 + 怪物ID后5位 + 3位流水号
// 物品脚本：4 + ItemID后5位 + 3位流水号
// 区域脚本：5 + 6位区域ID + 2位流水号
namespace MissionEditor
{
    public class StringRes
    {
        public StringRes(int iID, String strMainString)
        {
            m_iID = iID;
            m_strMainString = strMainString;
        }

        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        public String Key1
        {
            get
            {
                return m_strKey1;
            }
            set
            {
                m_strKey1 = value;
            }
        }

        public String Key2
        {
            get
            {
                return m_strKey2;
            }
            set
            {
                m_strKey2 = value;
            }
        }

        public String MainString
        {
            get
            {
                return m_strMainString;
            }
            set
            {
                m_strMainString = value;
            }
        }

        private int m_iID;  //
        private String m_strKey1 = null;
        private String m_strKey2 = null;
        private String m_strMainString = null;
    };

    // 对话管理器
    class DialogManager
    {
        private DialogManager()
        { }



        // 获取某种类型对话的 ID前缀
        public static int GetDialogIDPrefix(StateDiagram.OwnerType dt, int iOwnerID)
        {
            // 关于 字符串 ID 的规则：
            // NPC对话：1 + 5位NPCID + 3位流水号
            // NPC死亡：2 + 5位NPCID + 3位流水号
            // 怪物死亡：3 + 怪物ID后5位 + 3位流水号
            // 物品脚本：4 + ItemID后5位 + 3位流水号
            // 区域脚本：5 + 6位区域ID + 2位流水号

            return StateDiagram.GenerateStateDiagramID(dt, iOwnerID);
        }

        public static DialogManager Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new DialogManager();
                }

                return m_Instance;
            }
        }

        public StringRes GetStringRes(int iStrID)
        {
            foreach(Hashtable npcStrList in m_NPCDialogList.Values)
            {
                if (npcStrList.ContainsKey(iStrID))
                {
                    return npcStrList[iStrID] as StringRes;
                }
            }
            return null;
        }

        public void SaveToXml(String strXmlFileName)
        {
            //Encoding.Convert(UTF8Encoding, ASCIIEncoding, )

            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", null);//utf-8
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("Root");
            xmlDoc.AppendChild(root);

            foreach (int iNpcID in m_NPCDialogList.Keys)
            {
                Hashtable strResList = m_NPCDialogList[iNpcID] as Hashtable;
                XmlElement xmlElemNpcStrResRoot = xmlDoc.CreateElement("NPCStringRes");
                root.AppendChild(xmlElemNpcStrResRoot);
                xmlElemNpcStrResRoot.SetAttribute("ID", iNpcID.ToString());
                foreach (StringRes strRes in strResList.Values)
                {
                    XmlElement xmlElemStrRes = xmlDoc.CreateElement("StringRes");
                    xmlElemStrRes.SetAttribute("ID", strRes.ID.ToString());
                    //xmlElemStrRes.SetAttribute("MainStr", strRes.MainString);
                    xmlElemStrRes.SetAttribute("Key1", strRes.Key1);
                    xmlElemStrRes.SetAttribute("Key2", strRes.Key2);
                    XmlCDataSection xmlCData = xmlDoc.CreateCDataSection(strRes.MainString);
                    xmlElemStrRes.AppendChild(xmlCData);
                    xmlElemNpcStrResRoot.AppendChild(xmlElemStrRes);
                }
               
            }

            xmlDoc.Save(strXmlFileName);
        }

        public void LoadFromXml(String strXmlFilename)
        {
            m_NPCDialogList.Clear();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);

            XmlNode xmlNode = xmlDoc.FirstChild;
            while (!(xmlNode is XmlElement))
            {
                xmlNode = xmlNode.NextSibling;
            }

            XmlElement xmlElemRoot = xmlNode as XmlElement;
            XmlElement xmlElemNpcStrResRoot = xmlElemRoot.FirstChild as XmlElement;

            // 遍历所有 NPC
            while (xmlElemNpcStrResRoot != null)
            {
                String strNpcID = xmlElemNpcStrResRoot.GetAttribute("ID");
                int iNpcID = int.Parse(strNpcID);
                Hashtable strResList = new Hashtable();
                m_NPCDialogList[iNpcID] = strResList;

                // 遍历该 NPC 所有 StrRes
                XmlElement xmlElemStrRes = xmlElemNpcStrResRoot.FirstChild as XmlElement;
                while (xmlElemStrRes != null)
                {
                    String strMainStr = (xmlElemStrRes.FirstChild as XmlCDataSection).Data;
                    String strID = xmlElemStrRes.GetAttribute("ID");
                    String strKey1 = xmlElemStrRes.GetAttribute("Key1");
                    String strKey2 = xmlElemStrRes.GetAttribute("Key2");
                    StringRes strRes = new StringRes(int.Parse(strID), strMainStr);
                    strRes.Key1 = strKey1;
                    strRes.Key2 = strKey2;
                    strResList[strRes.ID] = (strRes);
                    xmlElemStrRes = xmlElemStrRes.NextSibling as XmlElement;
                }
                xmlElemNpcStrResRoot = xmlElemNpcStrResRoot.NextSibling as XmlElement;
            }

        }

        // 清空老的数据
        public void Clear()
        {
            m_NPCDialogList.Clear();
        }

        // 所有 NPC 对话列表
        public Hashtable NPCDialogList
        {
            get
            {
                return m_NPCDialogList;
            }
        }

        private static DialogManager m_Instance = null;
        private Hashtable m_NPCDialogList = new Hashtable();    // NPC_ID - HashMap<int ID,StringRes> 记录每一个 npc 对应的所有对话

    }
}
