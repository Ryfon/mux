﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace MissionEditor
{
    public class Skill
    {
        public Skill(int iID, int iType, String strName, int iPrice)
        {
            m_iID = iID;
            m_iType = iType;
            m_strName = strName;
            m_iPrice = iPrice;
        }

        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        public int Type
        {
            get
            {
                return m_iType;
            }
            set
            {
                m_iType = value;
            }
        }

        public String Name
        {
            get
            {
                return m_strName;
            }
            set
            {
                m_strName = value;
            }
        }

        public int Price
        {
            get
            {
                return m_iPrice;
            }
            set
            {
                m_iPrice = value;
            }
        }


        private int m_iID;      // 编号
        private int m_iType;    // 类型
        private String m_strName;  // 名称
        private int m_iPrice;   // 价格
    };

    public class SkillManager
    {
        // 从 xml 文件中载入所有技能
        public void LoadFromXml(String strXmlFilename)
        {
            m_Skills.Clear();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strXmlFilename);
                //XmlNode node = xmlDoc.FirstChild;
                //while (!node.Name.Equals("directory"))
                //{
                //    node = node.NextSibling;
                //}

                XmlElement elmtSkillRoot = xmlDoc.LastChild as XmlElement;
                XmlElement elmtSkill = elmtSkillRoot.FirstChild as XmlElement;
                while (elmtSkill != null)
                {
                    int iID = int.Parse(elmtSkill.GetAttribute("nID"));
                    int iType = int.Parse(elmtSkill.GetAttribute("nType"));
                    String strName = elmtSkill.GetAttribute("nName");

                    try
                    {
                        int iPrice = int.Parse(elmtSkill.GetAttribute("uUpgradeMoney"));
                        //if (iPrice > 0)
                        //{
                            Skill skill = new Skill(iID, iType, strName, iPrice);
                            m_Skills[iID] = skill;
                        //}
                    }
                    catch (Exception)
                    {}

                    elmtSkill = elmtSkill.NextSibling as XmlElement;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static SkillManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new SkillManager();
                }

                return ms_Instance;
            }
        }

        public Hashtable Skills
        {
            get
            {
                return m_Skills;
            }
        }

        private static SkillManager ms_Instance = null;
        private Hashtable m_Skills = new Hashtable();
    }
}
