﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;
using System.IO;
using System.Data;

namespace MissionEditor
{
    class RegionManager
    {
        private RegionManager()
        {
            
        }

        public void LoadRegionDesc()
        {
            // 获取路径
            String strXmlFilename = OptionManager.RegionDescPath;

            // 遍历文件夹
            String[] fileList = System.IO.Directory.GetFileSystemEntries(strXmlFilename);
            foreach (String strFileName in fileList)
            {
                // 获取文件名 
                String strExtendName = System.IO.Path.GetExtension(strFileName);
                if (strExtendName.CompareTo(".xml") != 0)
                {
                    continue;
                }

                // 读取文件
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strFileName);

                // 获取根节点
                XmlElement elmtRegionRoot = xmlDoc.LastChild as XmlElement;
                if (elmtRegionRoot == null)
                {
                    continue;
                }

                // 开始读取
                XmlElement elmtGroup = elmtRegionRoot.FirstChild as XmlElement;

                while (elmtGroup != null)
                {
                    int iGroupID = int.Parse(elmtGroup.GetAttribute("id"));

                    RegionGroup group = GetGroupByID(iGroupID);
                    if(group != null)
                    {
                        group.DescInfo.Load(elmtGroup);
                    }

                    elmtGroup = elmtGroup.NextSibling as XmlElement;
                }
            }
        }

        // 从XML中加载区域
        private void LoadRegionFromXmlElem(int iMapID, XmlElement elmtRegionRoot, ArrayList groupList, ArrayList regionList)
        {
            XmlElement elmtGroup = elmtRegionRoot.FirstChild as XmlElement;

            while (elmtGroup != null)
            {
                int iGroupID = 0;
                RegionGroup group = null;
                try
                {
                    String strGroupID = elmtGroup.GetAttribute("RegionID");
                    //if(strGroupID.Length != 8)
                    //{
                    //   String strFlowID = GetRegionGroupFlowID(iMapID, RegionGroup.RegionType.客户端表现区域/*, RegionGroup.RegionSmallType.安全区*/);
                    //    strGroupID = strMapID + "00" + strFlowID;                    
                    //}

                    
                    // 判断区域是否已经存在
                    iGroupID = int.Parse(strGroupID);
                    if(GetGroupByID(iGroupID) != null)
                    {
                        MessageBox.Show("地图" + iMapID.ToString() + "发现相同编号的区域" + strGroupID);
                    }

                    // 创建一个新的区域
                    group = new RegionGroup(iMapID, iGroupID);

                    if (strGroupID.Length == 6)// 6位的为服务器区域
                    {
                        group.SetRegionType(strGroupID.Substring(4, 2));
                    }
                    else if (strGroupID.Length == 8)// 8位的为客户端区域
                    {
                        group.SetRegionType(strGroupID.Substring(4, 4));
                    }
                    else// 其余的为错误，忽略
                    {
                        continue;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("地图" + iMapID + "存在没有ID的区域组");
                    elmtGroup = elmtGroup.NextSibling as XmlElement;
                    continue;
                }


                XmlElement elmtRegion = elmtGroup.FirstChild as XmlElement;
                String strGroupName = "新的区域组";
                Color cGroupColor = Color.FromArgb(255, 0, 255, 0);
                bool bGroupPK = true;
                bool bGroupBattle = true;
                bool bGroupDuplicate = true;
                int iServerRegionType = -1;   // 2009-3-27 徐磊添加 服务器区域类型的细分（安全， PK，战斗）
                while (elmtRegion != null)
                {
                    if (elmtRegion.Name.CompareTo("RegionDesc") == 0)
                    {
                        // 获取区域组的名字
                        if (elmtRegion.HasAttribute("Name"))
                        {
                            strGroupName = elmtRegion.GetAttribute("Name");
                        }
                        // 获取区域组的颜色
                        try
                        {
                            String strColor = elmtRegion.GetAttribute("Color");
                            int argb = int.Parse(strColor);
                            cGroupColor = Color.FromArgb(argb);
                        }
                        catch (Exception)
                        {

                        }
                        // 获取区域的属性
                        // PK
                        if (elmtRegion.HasAttribute("PK"))
                        {
                            bGroupPK = bool.Parse(elmtRegion.GetAttribute("PK"));
                        }
                        // 战斗
                        if (elmtRegion.HasAttribute("Battle"))
                        {
                            bGroupBattle = bool.Parse(elmtRegion.GetAttribute("Battle"));
                        }
                        // 副本前置
                        if (elmtRegion.HasAttribute("Duplicate"))
                        {
                            bGroupDuplicate = bool.Parse(elmtRegion.GetAttribute("Duplicate"));
                        }

                        // 获取服务器区域细分属性
                        if(elmtRegion.HasAttribute("ServerRegionType"))
                        {
                            iServerRegionType = int.Parse(elmtRegion.GetAttribute("ServerRegionType"));
                        }
                        elmtRegion = elmtRegion.NextSibling as XmlElement;
                        continue;
                    }

                    ArrayList pointListRect = new ArrayList();
                    ArrayList pointListCir = new ArrayList();
                    ArrayList pointList = new ArrayList();



                    RegionInfo info = new RegionInfo(pointList, iMapID);
                    String strRegionName = "新的区域";
                    info.ID = GetBigRegion(iMapID) + 1;
                    info.GroupID = iGroupID;

                    if (elmtRegion.Name.CompareTo("Polygon") == 0)
                    {
                        if (elmtRegion.HasAttribute("Name"))
                        {
                            strRegionName = elmtRegion.GetAttribute("Name");
                        }

                        info.Shape = 0;

                        XmlElement elmtPoint = elmtRegion.FirstChild as XmlElement;
                        // 读取方点
                        while (elmtPoint != null)
                        {
                            int iX = 0;
                            int iY = 0;
                            try
                            {
                                iX = int.Parse(elmtPoint.GetAttribute("XPos"));
                                iY = int.Parse(elmtPoint.GetAttribute("YPos"));
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("区域组" + iGroupID.ToString() + "Polygon格式错误");
                                elmtRegion = elmtRegion.NextSibling as XmlElement;
                                continue;
                            }
                            RegionPointInfo p = new RegionPointInfo(new Point(iX, iY));
                            pointListRect.Add(p);
                            elmtPoint = elmtPoint.NextSibling as XmlElement;
                        }
                        // 填充圆点
                        int iIndex = 0;
                        foreach (RegionPointInfo point in pointListRect)
                        {
                            RegionPointInfo pointLast = pointListRect[iIndex + 1] as RegionPointInfo;
                            if (iIndex != pointListRect.Count - 1)
                            {
                                pointLast = pointListRect[iIndex + 1] as RegionPointInfo;
                            }
                            else
                            {
                                pointLast = pointListRect[0] as RegionPointInfo;
                            }
                            int x = (int)((point.Pos.X + pointLast.Pos.X) / 2);
                            int y = (int)((point.Pos.Y + pointLast.Pos.Y) / 2);
                            RegionPointInfo p = new RegionPointInfo(new Point(x, y), 1);
                            pointListCir.Add(p);
                        }
                        // 合并
                        iIndex = 0;
                        foreach (RegionPointInfo point in pointListRect)
                        {
                            pointList.Add(point);
                            pointList.Add(pointListCir[iIndex] as RegionPointInfo);
                            iIndex++;
                        }
                    }
                    if (elmtRegion.Name.CompareTo("Rect") == 0)
                    {
                        if (elmtRegion.HasAttribute("Name"))
                        {
                            strRegionName = elmtRegion.GetAttribute("Name");
                        }

                        info.Shape = 1;

                        int iLeft = 0;
                        int iTop = 0;
                        int iRight = 0;
                        int iBottom = 0;
                        try
                        {
                            iLeft = int.Parse(elmtRegion.GetAttribute("Left"));
                            iTop = int.Parse(elmtRegion.GetAttribute("Top"));
                            iRight = int.Parse(elmtRegion.GetAttribute("Right"));
                            iBottom = int.Parse(elmtRegion.GetAttribute("Bottom"));
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("区域组" + iGroupID.ToString() + "Rect格式错误");
                            elmtRegion = elmtRegion.NextSibling as XmlElement;
                            continue;
                        }

                        pointList.Clear();
                        RegionPointInfo p4 = new RegionPointInfo(new Point(iLeft, iTop));
                        RegionPointInfo p3 = new RegionPointInfo(new Point(iRight, iTop));
                        RegionPointInfo p2 = new RegionPointInfo(new Point(iRight, iBottom));
                        RegionPointInfo p1 = new RegionPointInfo(new Point(iLeft, iBottom));
                        RegionPointInfo p12 = new RegionPointInfo(new Point(iLeft, iTop), 1);
                        RegionPointInfo p23 = new RegionPointInfo(new Point(iRight, iTop), 1);
                        RegionPointInfo p34 = new RegionPointInfo(new Point(iRight, iBottom), 1);
                        RegionPointInfo p14 = new RegionPointInfo(new Point(iLeft, iBottom), 1);
                        pointList.Add(p1);
                        pointList.Add(p12);
                        pointList.Add(p2);
                        pointList.Add(p23);
                        pointList.Add(p3);
                        pointList.Add(p34);
                        pointList.Add(p4);
                        pointList.Add(p14);
                        info.FormatPos();
                    }

                    info.Name = strRegionName;
                    regionList.Add(info);
                    elmtRegion = elmtRegion.NextSibling as XmlElement;
                }

                // 设置group的属性
                group.Name = strGroupName;
                group.RegionColor = cGroupColor;
                group.IsPK = bGroupPK;
                group.IsBattle = bGroupBattle;
                group.IsDuplicate = bGroupDuplicate;
                if(iServerRegionType != -1)
                {
                    group.ServerType = (RegionGroup.ServerRegionType)iServerRegionType;
                }
                groupList.Add(group);

                elmtGroup = elmtGroup.NextSibling as XmlElement;
            }
        }

        // 加载全部的区域
        public void LoadFromXML(String strXmlFilename)
        {
            // 清空旧的数据
            m_MapID2RegionList.Clear();

            // 获取制定文件夹下的所有文件列表
            String[] fileList = System.IO.Directory.GetFileSystemEntries(strXmlFilename);

            // 遍历文件读取信息
            foreach(String strFileName in fileList)
            {
                // 文件筛选，只读取后缀名为xml的文件
                String strExtendName = System.IO.Path.GetExtension(strFileName);
                if (strExtendName.CompareTo(".xml") != 0)
                {
                    continue;
                }

                // 加载xml文件
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strFileName);

                // 获取根节点，并判断
                XmlElement elmtRegionRoot = xmlDoc.LastChild as XmlElement;
                if (elmtRegionRoot == null)
                {
                    continue;
                }
                
                // 获取文件名中的地图号
                int iDot = strFileName.LastIndexOf('.');
                int ihen = strFileName.LastIndexOf('_');
                String strMapID = strFileName.Substring(ihen + 1, iDot - ihen - 1);
                int iMapID = int.Parse(strMapID);

                // 获取或建立两张表
                ArrayList groupList = m_MapID2RegionGroupList[iMapID] as ArrayList;
                ArrayList regionList = m_MapID2RegionList[iMapID] as ArrayList;
                if(groupList == null || regionList == null)
                {
                    groupList = new ArrayList();
                    regionList = new ArrayList();
                    m_MapID2RegionList[iMapID] = regionList;
                    m_MapID2RegionGroupList[iMapID] = groupList;
                }

                LoadRegionFromXmlElem(iMapID, elmtRegionRoot, groupList, regionList);
            }
            LoadRegionDesc();
        }

        public void ExportRegionDesc(int iMapID)
        {
            String strPath = OptionManager.RegionDescPath + "RegionDesc_";
            String strXmlFilename = strPath + iMapID.ToString() + ".xml";

            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("Regions");
            root.SetAttribute("FileVersion", "0.9");
            xmlDoc.AppendChild(root);

            ArrayList RegionList = m_MapID2RegionList[iMapID] as ArrayList;
            ArrayList GroupList = m_MapID2RegionGroupList[iMapID] as ArrayList;

            if (GroupList != null)
            {
                
                foreach (RegionGroup group in GroupList)
                {
                    // 只导出客户端区域
                    if (group.BigType == RegionGroup.RegionType.客户端表现区域)
                    {
                        XmlElement desc = xmlDoc.CreateElement("Region");

                        // 基本属性
                        desc.SetAttribute("id", group.OutPutID);
                        int iType = (int)group.ClientType;
                        desc.SetAttribute("RegionType", iType.ToString());
                        desc.SetAttribute("RegionName", group.Name);

                        //desc属性
                        group.DescInfo.Save(desc);

                        root.AppendChild(desc);
                    }
                }
            }

            xmlDoc.Save(strXmlFilename);
        }

        public void SaveAll()
        {
            foreach (int iMapID in m_MapID2RegionGroupList.Keys)
            {
                SaveToXML(iMapID);
            }
        }

        public void SaveBlockQuadRegion(String strFileName)
        {
            // 文件结构初始化
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc.AppendChild(xmlDeclaration);

            // 加入根节点
            XmlElement root = xmlDoc.CreateElement("BlockQuads");
            root.SetAttribute("FileVersion", "1.0");
            xmlDoc.AppendChild(root);

            // 获取所有的场景ID
            ArrayList MapIDList = SceneSketchManager.Instance.GetSceneIDList();
            
            // 循环遍历每一张map
            if(MapIDList != null)
            {
                foreach(int iMapID in MapIDList)
                {
                    // 地图根节点
                    XmlElement xmlMap = xmlDoc.CreateElement("Map");
                    xmlMap.SetAttribute("MapID", iMapID.ToString());
                    
                    // 获取该地图中所有的BlockQuad区域
                    ArrayList RegionList = m_MapID2RegionList[iMapID] as ArrayList;
                    ArrayList GroupList = m_MapID2RegionGroupList[iMapID] as ArrayList;
                    if(RegionList == null || GroupList == null)
                    {
                        continue;
                    }

                    // 找到group
                    foreach(RegionGroup group in GroupList)
                    {
                        if(group.BigType == RegionGroup.RegionType.服务器相关区域 && group.ServerType == RegionGroup.ServerRegionType.BlockQuad区)
                        {
                            foreach(RegionInfo region in RegionList)
                            {
                                if(region.GroupID == group.ID)
                                {
                                    // 获取bound
                                    Rectangle bound = region.GetBound();

                                    // 建立xml节点
                                    XmlElement xmlRegion = xmlDoc.CreateElement("Region");
                                    xmlRegion.SetAttribute("ID", region.Name);
                                    xmlRegion.SetAttribute("MinX", bound.Left.ToString());
                                    xmlRegion.SetAttribute("MinY", bound.Top.ToString());
                                    xmlRegion.SetAttribute("MaxX", bound.Right.ToString());
                                    xmlRegion.SetAttribute("MaxY", bound.Bottom.ToString());

                                    // 添加节点
                                    xmlMap.AppendChild(xmlRegion);
                                }
                            }
                        }
                    }

                    // 将地图添加入根节点
                    root.AppendChild(xmlMap);
                }
            }
            // 保存文件
            xmlDoc.Save(strFileName);
        }

        public void SaveToXML(int iMapID)
        {
            String strPath_N = OptionManager.RegionInfoPath + "RegionInfo_";
            String strXmlFilename_N = strPath_N + iMapID.ToString() + ".xml";

            XmlDocument xmlDoc_N = new XmlDocument();
            XmlDeclaration xmlDeclaration_N = xmlDoc_N.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc_N.AppendChild(xmlDeclaration_N);

            XmlElement root_N = xmlDoc_N.CreateElement("Regions");
            root_N.SetAttribute("FileVersion", "0.9");
            xmlDoc_N.AppendChild(root_N);

            //--------------------------------------------------2009-01-07 徐磊新增 区域存盘文件的分离--------------------------------------------------//
            String strPath_T = OptionManager.RegionInfoPath + "RegionInfo_T_";
            String strXmlFilename_T = strPath_T + iMapID.ToString() + ".xml";

            XmlDocument xmlDoc_T = new XmlDocument();
            XmlDeclaration xmlDeclaration_T = xmlDoc_T.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc_T.AppendChild(xmlDeclaration_T);

            XmlElement root_T = xmlDoc_T.CreateElement("Regions");
            root_T.SetAttribute("FileVersion", "0.9");
            xmlDoc_T.AppendChild(root_T);
            //--------------------------------------------------2009-01-07 徐磊新增 区域存盘文件的分离--------------------------------------------------//

            //--------------------------------------------------2009-05-31 徐磊新增 区域存盘文件的分离--------------------------------------------------//
            String strPath_C = OptionManager.RegionInfoPath + "RegionInfo_C_";
            String strXmlFilename_C = strPath_C + iMapID.ToString() + ".xml";

            XmlDocument xmlDoc_C = new XmlDocument();
            XmlDeclaration xmlDeclaration_C = xmlDoc_C.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc_C.AppendChild(xmlDeclaration_C);

            XmlElement root_C = xmlDoc_C.CreateElement("Regions");
            root_C.SetAttribute("FileVersion", "0.9");
            xmlDoc_C.AppendChild(root_C);
            //--------------------------------------------------2009-05-31 徐磊新增 区域存盘文件的分离--------------------------------------------------//

            ArrayList RegionList = m_MapID2RegionList[iMapID] as ArrayList;
            ArrayList GroupList = m_MapID2RegionGroupList[iMapID] as ArrayList;

            XmlDocument xmlDoc = null;
            XmlElement root = null;

            if (GroupList != null)
            {
                foreach (RegionGroup group in GroupList)
                {
                    if(group.BigType == RegionGroup.RegionType.客户端表现区域)
                    {
                        xmlDoc = xmlDoc_C;
                        root = root_C;
                    }
                    else if (group.BigType == RegionGroup.RegionType.服务器相关区域)
                    {
                        if(group.ServerType == RegionGroup.ServerRegionType.任务触发区域)
                        {
                            // 任务区域特别处理，独立存储
                            xmlDoc = xmlDoc_T;
                            root = root_T;
                        }
                        else
                        {
                            xmlDoc = xmlDoc_N;
                            root = root_N;
                        }
                    }
                    else
                    {
                        xmlDoc = xmlDoc_N;
                        root = root_N;
                    }

                    XmlElement xmlGroup = xmlDoc.CreateElement("Region");
                    //xmlGroup.SetAttribute("RegionID", group.ID.ToString());
                    xmlGroup.SetAttribute("RegionID", group.OutPutID);
                    int iLeft = 10000;
                    int iTop = 10000;
                    int iRight = 0;
                    int iBottom = 0;


                    XmlElement xmlGroupDesc = xmlDoc.CreateElement("RegionDesc");
                    xmlGroupDesc.SetAttribute("Name", group.Name);
                    xmlGroupDesc.SetAttribute("Color", group.RegionColor.ToArgb().ToString());
                    xmlGroupDesc.SetAttribute("PK", group.IsPK.ToString());
                    xmlGroupDesc.SetAttribute("Battle", group.IsBattle.ToString());
                    xmlGroupDesc.SetAttribute("Duplicate", group.IsDuplicate.ToString());
                    int iServerRegionType = (int)group.ServerType;
                    xmlGroupDesc.SetAttribute("ServerRegionType", iServerRegionType.ToString());
                    xmlGroup.AppendChild(xmlGroupDesc);

                    if (RegionList != null)
                    {

                        foreach (RegionInfo region in RegionList)
                        {
                            if (region.GroupID == group.ID)
                            {
                                XmlElement xmlRegion = null;
                                if (region.Shape == 1)
                                {
                                    xmlRegion = xmlDoc.CreateElement("Rect");
                                    xmlRegion.SetAttribute("Name", region.Name);
                                    Rectangle bound = region.GetBound();
                                    xmlRegion.SetAttribute("Left", bound.Left.ToString());
                                    xmlRegion.SetAttribute("Top", bound.Top.ToString());
                                    xmlRegion.SetAttribute("Right", bound.Right.ToString());
                                    xmlRegion.SetAttribute("Bottom", bound.Bottom.ToString());
                                    if (bound.Left < iLeft)
                                    {
                                        iLeft = bound.Left;
                                    }
                                    if (bound.Top < iTop)
                                    {
                                        iTop = bound.Top;
                                    }
                                    if (bound.Right > iRight)
                                    {
                                        iRight = bound.Right;
                                    }
                                    if (bound.Bottom > iBottom)
                                    {
                                        iBottom = bound.Bottom;
                                    }
                                }
                                else
                                {
                                    xmlRegion = xmlDoc.CreateElement("Polygon");
                                    xmlRegion.SetAttribute("Name", region.Name);
                                    foreach (RegionPointInfo point in region.PointList)
                                    {
                                        if (point.Type == 0)
                                        {
                                            XmlElement xmlPoint = xmlDoc.CreateElement("Point");
                                            xmlPoint.SetAttribute("XPos", point.Pos.X.ToString());
                                            xmlPoint.SetAttribute("YPos", point.Pos.Y.ToString());
                                            if (point.Pos.X < iLeft)
                                            {
                                                iLeft = point.Pos.X;
                                            }
                                            if (point.Pos.Y < iTop)
                                            {
                                                iTop = point.Pos.Y;
                                            }
                                            if (point.Pos.X > iRight)
                                            {
                                                iRight = point.Pos.X;
                                            }
                                            if (point.Pos.Y > iBottom)
                                            {
                                                iBottom = point.Pos.Y;
                                            }
                                            xmlRegion.AppendChild(xmlPoint);
                                        }
                                    }
                                }

                                if (xmlRegion != null)
                                {
                                    xmlGroup.AppendChild(xmlRegion);
                                }
                            }
                        }
                    }
                    if (iLeft != 10000 && iTop != 10000)
                    {
                        xmlGroup.SetAttribute("Left", iLeft.ToString());
                        xmlGroup.SetAttribute("Top", iTop.ToString());
                        xmlGroup.SetAttribute("Right", iRight.ToString());
                        xmlGroup.SetAttribute("Bottom", iBottom.ToString());
                        root.AppendChild(xmlGroup);
                    }
                }
            }

            xmlDoc_N.Save(strXmlFilename_N);
            xmlDoc_T.Save(strXmlFilename_T);
            xmlDoc_C.Save(strXmlFilename_C);

            // 导出RegionDesc信息
            ExportRegionDesc(iMapID);
        }

        public RegionInfo GetRegionByID(int iID)
        {
            foreach(ArrayList regionList in m_MapID2RegionList.Values)
            {
                foreach(RegionInfo regionInfo in regionList)
                {
                    if(regionInfo.ID == iID)
                    {
                        return regionInfo;
                    }
                }
            }

            return null;
        }

        public bool DeleteRegion(int iID)
        {
            RegionInfo delRegion = GetRegionByID(iID);

            if (delRegion != null)
            {
                DialogResult result = MessageBox.Show("确定要删除区域--" + delRegion.Name + "--么？", "提示", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    ArrayList regionList = MapID2RegionList[delRegion.MapID] as ArrayList;
                    regionList.Remove(delRegion);
                    if (MainForm.Instance.SelectedRegion != null)
                    {
                        MainForm.Instance.SelectedRegion.Hide();
                    }

                    return true;
                }
            }

            return false;
        }

        public bool HideRegion(int iID, bool bHidden)
        {
            RegionInfo delRegion = GetRegionByID(iID);

            if (delRegion != null)
            {
                delRegion.Hidden = bHidden;
                MainForm.Instance.UpdateRegion(iID, bHidden);
                return true;
            }

            return false;
        }
        // 获取编号最大的Region
        public int GetBigRegion(int iMapID)
        {
            String strBig = iMapID.ToString() + "00000";

            int iBigID = int.Parse(strBig);

            ArrayList regionList = m_MapID2RegionList[iMapID] as ArrayList;

            if (regionList == null)
            {
                return 0;
            }

            foreach (RegionInfo region in regionList)
            {
                if (region.ID >= iBigID)
                {
                    iBigID = region.ID;
                }
            }

            return iBigID;
        }

        public RegionInfo AddRegion(int iMapID)
        {
            ArrayList regionList = m_MapID2RegionList[iMapID] as ArrayList;

            if (regionList == null)
            {
                regionList = new ArrayList();
            }
            m_MapID2RegionList[iMapID] = regionList;

            // 创建一个三角形的区域
            ArrayList pointList = new ArrayList();
            RegionPointInfo p1 = new RegionPointInfo(new Point(20, 20));
            RegionPointInfo p2 = new RegionPointInfo(new Point(20, 40));
            RegionPointInfo p3 = new RegionPointInfo(new Point(50, 40));
            RegionPointInfo p12 = new RegionPointInfo(new Point(20, 30), 1);
            RegionPointInfo p23 = new RegionPointInfo(new Point(35, 40), 1);
            RegionPointInfo p13 = new RegionPointInfo(new Point(35, 30), 1);
            pointList.Add(p1);
            pointList.Add(p12);
            pointList.Add(p2);
            pointList.Add(p23);
            pointList.Add(p3);        
            pointList.Add(p13);

            RegionInfo newRegion = new RegionInfo(pointList, iMapID);
            int iNewRegionID = GetBigRegion(iMapID);
            if(iNewRegionID == 0)
            {
                return null;
            }
            else
            {
                newRegion.ID = iNewRegionID + 1;
            }

            regionList.Add(newRegion);

            return newRegion;
        }

        // 获取编号最大的Group
        public int GetBigGroup(int iMapID)
        {
            String strBig = iMapID.ToString() + "0000";

            int iBigID = int.Parse(strBig);

            ArrayList groupList = m_MapID2RegionGroupList[iMapID] as ArrayList;

            if (groupList == null)
            {
                return 0;
            }

            foreach (RegionGroup group in groupList)
            {
                if (group.ID >= iBigID)
                {
                    iBigID = group.ID;
                }
            }

            return iBigID;
        }
        
        public bool AddRegionGroup(int iMapID, int bigType, int iSmallType)
        { 
            ArrayList groupList = m_MapID2RegionGroupList[iMapID] as ArrayList;
            if(groupList == null)
            {
                groupList = new ArrayList();
            }
            m_MapID2RegionGroupList[iMapID] = groupList;

            int iID = GetBigGroup(iMapID);
            if(iID == 0)
            {
                return false;
            }

            RegionGroup group = new RegionGroup(iMapID, iID + 1);
            RegionGroup.RegionType type = (RegionGroup.RegionType)bigType;
            group.BigType = type;
            bool bTask = true;
            if(type == RegionGroup.RegionType.服务器相关区域)
            {
                if(iSmallType == 0)
                {
                    bTask = true;
                }
                else
                {
                    bTask = false;
                }
                group.ServerType = (RegionGroup.ServerRegionType)iSmallType;
            }
            else if(type == RegionGroup.RegionType.客户端表现区域)
            {
                group.ClientType = (RegionGroup.ClientRegionType)iSmallType;
            }
            group.FlowID = RegionManager.Instance.GetRegionGroupFlowID(iMapID, type, bTask/*, group.SmallType*/);
            groupList.Add(group);
            
            return true;
        }

        public RegionGroup GetGroupByID(int iID)
        {
            foreach (ArrayList groupList in m_MapID2RegionGroupList.Values)
            {
                foreach (RegionGroup group in groupList)
                {
                    if (group.ID == iID)
                    {
                        return group;
                    }
                }
            }

            return null;
        }

        public bool DeleteGroup(int iID)
        {
            RegionGroup delGroup = GetGroupByID(iID);

            if (delGroup != null)
            {
                int iMapID = delGroup.MapID;
                ArrayList groupList = m_MapID2RegionGroupList[iMapID] as ArrayList;
                groupList.Remove(delGroup);

                // 删除组内的所有区域
                ArrayList regionList = MapID2RegionList[iMapID] as ArrayList;
                ArrayList delList = new ArrayList();
                foreach (RegionInfo regionInfo in regionList)
                {
                    if (regionInfo.GroupID == iID)
                    {
                        delList.Add(regionInfo.ID);
                    }
                }
                foreach(int delID in delList)
                {
                    DeleteRegion(delID);
                }
                if(MainForm.Instance.SelectedRegion != null)
                {
                    MainForm.Instance.SelectedRegion.Invalidate();
                }

                MainForm.Instance.RefreshSceneSketch(iMapID);
                return true;
            }

            return false;
        }

        public void HideGroup(int iID, bool bHidden)
        {
            RegionGroup delGroup = GetGroupByID(iID);

            if (delGroup != null)
            {
                int iMapID = delGroup.MapID;

                ArrayList regionList = MapID2RegionList[iMapID] as ArrayList;

                foreach (RegionInfo regionInfo in regionList)
                {
                    if (regionInfo.GroupID == iID)
                    {
                        HideRegion(regionInfo.ID, bHidden);
                    }
                }
            }
        }

        public String GetRegionGroupFlowID(int iMapID, RegionGroup.RegionType bigType, bool bTaskRegion/*, RegionGroup.RegionSmallType smallType*/)
        {
            // 默认的最大ID为“00”
            String strFlowID = "00";

            // 服务器的非任务区域，ID从50开始，所以默认值为“49”
            if(bigType == RegionGroup.RegionType.服务器相关区域)
            {
                if(!bTaskRegion)
                {
                    strFlowID = "49";
                }
            }

            // 获取当前map的区域组列表
            ArrayList groupList = m_MapID2RegionGroupList[iMapID] as ArrayList;

            // 列表不存在，直接返回01
            if (groupList == null)
            {
                return "01";
            }


            foreach (RegionGroup group in groupList)
            {
                if (group.BigType == bigType/* && group.SmallType == smallType*/)
                {
                    bool bTemp = true;
                    if(bigType == RegionGroup.RegionType.服务器相关区域)
                    {
                        if(bTaskRegion)
                        {
                            if(group.ServerType != RegionGroup.ServerRegionType.任务触发区域)
                            {
                                bTemp = false;
                            }
                        }
                    }
                   
                    if(bTemp)
                    {
                        if (int.Parse(group.FlowID) > int.Parse(strFlowID))
                        {
                            strFlowID = group.FlowID;
                        }
                    }
                }
            }
            
            // 数字和字符串的处理
            int LastID = int.Parse(strFlowID) + 1;  // 当前最大的ID加1，获取新的ID
            strFlowID = LastID.ToString();
            // 小于10的区域，因为只有1位，所以需要补齐
            if (LastID < 10)
            {
                strFlowID = "0" + strFlowID;
            }
            
            return strFlowID;
        }

        public Hashtable RegionFilter(int iMapID, String strRegionName)
        {
            // 创建结果数据集
            Hashtable result = new Hashtable();

            // 先找出地图所对应的列表
            foreach (int id in m_MapID2RegionGroupList.Keys)
            {
                if (iMapID == -1 || iMapID == id)
                {
                    ArrayList groupList = m_MapID2RegionGroupList[id] as ArrayList;
                    foreach(RegionGroup group in groupList)
                    {
                        if(group.BigType == RegionGroup.RegionType.服务器相关区域)
                        {
                            if(group.ServerType == RegionGroup.ServerRegionType.任务触发区域)
                            {
                                if (strRegionName.Length == 0 || group.Name.Contains(strRegionName) || group.OutPutID.ToString().Contains(strRegionName))
                                {
                                    result[group.OutPutID] = group;
                                }
                            }
                        }
                    }
                }
            }

            // 返回结果
            return result;
        }

        // 将音乐信息与音乐库进行同步
        public void Coordinate(int type, Hashtable libTable)
        {
            foreach (ArrayList groupList in m_MapID2RegionGroupList.Values)
            {
                foreach (RegionGroup group in groupList)
                {
                    group.DescInfo.Coordinate(type, libTable);
                }
            }
        }
        //---------------------------------------------------------------------------------
        // 公有属性
        public Hashtable MapID2RegionList
        {
            get
            {
                return m_MapID2RegionList;
            }
        }

        public Hashtable MapID2GroupList
        {
            get
            {
                return m_MapID2RegionGroupList;
            }
        }

        public static RegionManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new RegionManager();
                }
                return ms_Instance;
            }
        }
        //--------------------------------------------------------------------------------
        private Hashtable m_MapID2RegionList = new Hashtable();    // 地图编号 - 该地图 Region List 映射
        private Hashtable m_MapID2RegionGroupList = new Hashtable();    // 地图编号 - 该地图 Region Group List 映射
        private static RegionManager ms_Instance = null;
        //--------------------------------------------------------------------------------

    }

    public class RegionGroup
    {
        public enum RegionType : int
        {
            服务器相关区域 = 0,
            客户端表现区域 = 1,
        }

        public enum ServerRegionType
        {
            任务触发区域 = 0,
            安全区 = 1,
            不可PK区 = 2,
            不可骑乘区 = 3,
            寻路预判区 = 4,
            BlockQuad区 = 5,
        }

        public enum ClientRegionType
        {
            音乐触发区域 = 1,
            音效特效触发区域 = 2,
        }

        //public enum RegionSmallType
        //{
        //    安全区 = 1,
        //    战斗区1 = 2,
        //    战斗区2 = 3,
        //    任务触发区域 = 4,
        //    音乐触发区域 = 5,
        //    音效触发区域 = 6,
        //    特效触发区域 = 7,
        //}

        public RegionGroup(int mapID, int id)
        {
            m_iMapID = mapID;
            m_iID = id;
        }

        // 共有属性
        [BrowsableAttribute(false)]
        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        [ReadOnlyAttribute(true), CategoryAttribute("调试用ID信息"), DescriptionAttribute("4位地图号")]
        public int MapID
        {
            get
            {
                return m_iMapID;
            }
            set
            {
                m_iMapID = value;
            }
        }

        [CategoryAttribute("基本信息"), DescriptionAttribute("区域的名称")]
        public String Name
        {
            get
            {
                return m_strName;
            }
            set
            {
                m_strName = value;
            }
        }

        [CategoryAttribute("基本信息"), DescriptionAttribute("区域的显示颜色")]
        public Color RegionColor
        {
            get
            {
                return m_color;
            }
            set
            {
                m_color = value;
                ArrayList regionList = RegionManager.Instance.MapID2RegionList[m_iMapID] as ArrayList;
                if (regionList != null)
                {
                    foreach (RegionInfo region in regionList)
                    {
                        if (region.GroupID == m_iID)
                        {
                            MainForm.Instance.UpdateRegion(region.ID, false);
                        }
                    }
                }
            }
        }

        [ReadOnlyAttribute(true), CategoryAttribute("基本信息"), DescriptionAttribute("区域的类型")]
        public RegionType BigType
        {
           get
           {
               return m_RegionBigType;
           }
           set
           {
               //RegionSmallType small = RegionSmallType.安全区;
               //if (value == RegionType.服务器相关区域)
               //{
               //    small = RegionSmallType.安全区;
               //}
               //else
               //{
               //    small = RegionSmallType.任务触发区域;
               //}
               //m_RegionSmallType = small;

               //String NewFlowID = RegionManager.Instance.GetRegionGroupFlowID(m_iMapID, value);

               //if(m_RegionBigType == RegionType.服务器相关区域)
               //{
                   // 服务器区域修改需要特别确认，误操作，可能导致ID不一致
               //    DialogResult result = MessageBox.Show("确认要修改区域类型？该操作可能造成ID不一致！", "警告", MessageBoxButtons.YesNo);
               //    if(result == DialogResult.Yes)
               //    {
               //        m_RegionBigType = value;
               //        m_strFlowID = NewFlowID;
               //    }
               //}
               //else
               //{
                //   m_RegionBigType = value;
                //   m_strFlowID = NewFlowID;
               //}

               m_RegionBigType = value;
           }
        }

        //public RegionSmallType SmallType
        //{
        //    get
        //    {
        //        return m_RegionSmallType;
        //    }
        //    set
        //    {

        //        int iTemp = (int)value;

        //        if (m_RegionBigType == RegionType.服务器相关区域)
        //        {
        //            if(iTemp > 3)
        //            {
        //                iTemp = 1;
        //            }
        //        }
        //        else
        //        {
        //            if(iTemp < 4)
        //            {
        //                iTemp = 4;
        //            }
        //        }
     
        //        if (m_RegionSmallType != (RegionSmallType)iTemp)
        //        {
        //            String NewFlowID = RegionManager.Instance.GetRegionGroupFlowID(m_iMapID, m_RegionBigType, (RegionSmallType)iTemp);
        //            m_RegionSmallType = (RegionSmallType)iTemp;

        //            m_strFlowID = NewFlowID;
        //        }
        //    }
        //}

        [ReadOnlyAttribute(true), CategoryAttribute("调试用ID信息"), DescriptionAttribute("6~7位信息:4位地图号+2~3位流水号")]
        public String OutPutID
        {
            get
            {
                //int iBigType = (int)m_RegionBigType;
                //int iSmallType = (int)m_RegionSmallType;
                //iSmallType = iSmallType - 3 * iBigType;
                //return m_iMapID.ToString() + iBigType.ToString() + iSmallType.ToString() + m_strFlowID;
                if (m_RegionBigType == RegionType.服务器相关区域)
                {
                    return m_iMapID.ToString() + m_strFlowID;
                }
                else if (m_RegionBigType == RegionType.客户端表现区域)
                {
                    int iSmalltype = (int)m_ClientType;
                    String strSmallType = iSmalltype.ToString();
                    if (iSmalltype < 10)
                    {
                        strSmallType = "0" + strSmallType;
                    }
                    return m_iMapID.ToString() + strSmallType + m_strFlowID;
                }
                else
                {
                    return "";
                }
            }
        }

        [ReadOnlyAttribute(true), CategoryAttribute("调试用ID信息"), DescriptionAttribute("2~3位流水号")]
        public String FlowID
        {
            get
            {
                return m_strFlowID;
            }
            set
            {
                m_strFlowID = value;
            }
        }

        [BrowsableAttribute(false), CategoryAttribute("服务器相关区域特有属性"), DescriptionAttribute("该区域是否可PK")]
        public bool IsPK
        {
            get
            {
                return m_bPK;
            }
            set
            {
                m_bPK = value;
            }
        }

        [BrowsableAttribute(false), CategoryAttribute("服务器相关区域特有属性"), DescriptionAttribute("该区域是否可战斗")]
        public bool IsBattle
        {
            get
            {
                return m_bBattle;
            }
            set
            {
                m_bBattle = value;
            }
        }

        [BrowsableAttribute(false), CategoryAttribute("服务器相关区域特有属性"), DescriptionAttribute("该区域是否为副本前置区")]
        public bool IsDuplicate
        {
            get
            {
                return m_bDuplicate;
            }
            set
            {
                m_bDuplicate = value;
            }
        }

        [ReadOnlyAttribute(true), CategoryAttribute("服务器相关区域特有属性"), DescriptionAttribute("服务器区域类型")]
        public ServerRegionType ServerType
        {
            get
            {
                return m_ServerType;
            }
            set
            {
                m_ServerType = value;
            }
        }

        [CategoryAttribute("客户端区域特有属性"), DescriptionAttribute("客户端区域类型")]
        public ClientRegionType ClientType
        {
            get
            {
                return m_ClientType;
            }
            set
            {
                m_ClientType = value;
            }
        }

        [BrowsableAttribute(false)]
        public RegionDesc DescInfo
        {
            get
            {
                return m_RegionDesc;
            }
            set
            {
                m_RegionDesc = value;
            }
        }

        /*--------------------------------------------------------------------------------------------------*/
        public void SetRegionType(String strFour)
        {
            // 通过字符串的长度，判断区域的类型
            if(strFour.Length == 2)
            {
                m_RegionBigType = RegionType.服务器相关区域;
                m_strFlowID = strFour.Substring(0, 2);
                int iFlowID = int.Parse(m_strFlowID);
                if(iFlowID < 50)
                {
                    m_ServerType = ServerRegionType.任务触发区域;
                }
                else
                {
                    m_ServerType = ServerRegionType.安全区;
                }
            }
            else if (strFour.Length == 4)
            {
                m_RegionBigType = RegionType.客户端表现区域;
                m_strFlowID = strFour.Substring(2, 2);
                m_ClientType = (ClientRegionType)(int.Parse(strFour.Substring(0, 2)));
            }
            else
            {
                return;
            }
            
            //int SmallType = int.Parse(strFour.Substring(1, 1));
            //SmallType += BigType * 3;
            //m_RegionSmallType = (RegionSmallType)SmallType;    // 区域的小类
        }
        //////////////////////////////////////////////////////////////////////////
        // 私有成员
        private int m_iID;          // 区域组的唯一编号,固定为8位=4位地图+1位类型+3位流水 
        private int m_iMapID;       // 地图号
        private String m_strName = "新的区域组";   // 区域组的名字
        private Color m_color = Color.FromArgb(255, 0, 255, 0);      // 区域组的颜色
        //private RegionSmallType m_RegionSmallType = (RegionSmallType)1;    // 区域的小类
        private String m_strFlowID = "101";      // 2~3位流水号, 服务器相关为2位, 客户端表现为3位
        // 区域的类型
        private RegionType m_RegionBigType = (RegionType)1;    // 区域的大类
        private ClientRegionType m_ClientType = ClientRegionType.音乐触发区域;  // 客户端区域的类型
        private ServerRegionType m_ServerType = ServerRegionType.任务触发区域;  // 服务器区域的类型
        // 服务器区域特有的属性
        private bool m_bPK = true;          // 是否可PK区域
        private bool m_bBattle = true;      // 是否可战斗区域
        private bool m_bDuplicate = true;   // 是否副本前置区域
        // 区域描述信息
        private RegionDesc m_RegionDesc = new RegionDesc();
        //////////////////////////////////////////////////////////////////////////
    };

    public class RegionInfo
    {
        const int MaxWidth = 512;
        const int MaxHeight = 512;

        public RegionInfo(ArrayList PointList, int mapID)
        {
            m_PointList = PointList;
            m_iMapID = mapID;
        }

        // 共有属性
        [BrowsableAttribute(false)]
        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        [BrowsableAttribute(false)]
        public int GroupID
        {
            get
            {
                return m_iGroupID;
            }
            set
            {
                m_iGroupID = value;
            }
        }

        [BrowsableAttribute(false)]
        public RegionGroup Group
        {
            get
            {
                return RegionManager.Instance.GetGroupByID(m_iGroupID);
            }
        }

        [ReadOnlyAttribute(true), CategoryAttribute("基本信息"), DescriptionAttribute("地图号")]
        public int MapID
        {
            get
            {
                return m_iMapID;
            }
            set
            {
                m_iMapID = value;
            }
        }
    
        [CategoryAttribute("基本信息"), DescriptionAttribute("用于显示标识的名称")]
        public String Name
        {
            get
            {
                return m_strName;
            }
            set
            {
                m_strName = value;
            }
        }

        [BrowsableAttribute(false)]
        public ArrayList PointList
        {
            get
            {
                return m_PointList;
            }
        }

        [BrowsableAttribute(false)]
        public ArrayList ContainPointList
        {
            get
            {
                return m_ContainList;
            }
        }

        [ReadOnlyAttribute(true), CategoryAttribute("基本信息"), DescriptionAttribute("0多边形\r\n1矩形")]
        public int Shape
        {
            get
            {
                return m_iShape;
            }
            set
            {
                m_iShape = value;
            }
        }

        [BrowsableAttribute(false)]
        public bool Hidden
        {
            get
            {
                return m_bHidden;
            }
            set
            {
                m_bHidden = value;
            }
        }

        // 调整所有的圆点
        public void FormatPos()
        {
            int iIndex = 0;
            foreach (RegionPointInfo pointInfo in m_PointList)
            {
                if(iIndex > m_PointList.Count - 2)
                {
                    break;
                }

                if(iIndex % 2 == 1)
                {
                    RegionPointInfo startPoint = m_PointList[iIndex - 1] as RegionPointInfo;
                    RegionPointInfo endPoint = m_PointList[iIndex + 1] as RegionPointInfo;
                    int iX = (startPoint.Pos.X + endPoint.Pos.X) / 2;
                    int iY = (startPoint.Pos.Y + endPoint.Pos.Y) / 2;
                    pointInfo.Pos = new Point(iX, iY);
                }
                iIndex++;
            }

            RegionPointInfo startPoint2 = m_PointList[0] as RegionPointInfo;
            RegionPointInfo middlePoint2 = m_PointList[m_PointList.Count - 1] as RegionPointInfo;
            RegionPointInfo endPoint2 = m_PointList[m_PointList.Count - 2] as RegionPointInfo;
            int iX2 = (startPoint2.Pos.X + endPoint2.Pos.X) / 2;
            int iY2 = (startPoint2.Pos.Y + endPoint2.Pos.Y) / 2;
            middlePoint2.Pos = new Point(iX2, iY2);
        }

        // 调整跟随移动的圆点
        private void _formatPos(RegionPointInfo pointInfo, int dx, int dy)
        {
            int iIndex = m_PointList.IndexOf(pointInfo);
            int ix = (int)(dx / 2);
            int iy = (int)(dy / 2);

            if(iIndex == 0)
            {
                RegionPointInfo startPoint = m_PointList[m_PointList.Count - 1] as RegionPointInfo;
                RegionPointInfo endPoint = m_PointList[iIndex + 1] as RegionPointInfo;
                startPoint.Pos = new Point(startPoint.Pos.X + ix, startPoint.Pos.Y - iy);
                endPoint.Pos = new Point(endPoint.Pos.X + ix, endPoint.Pos.Y - iy);
            }
            else
            {
                RegionPointInfo startPoint = m_PointList[iIndex - 1] as RegionPointInfo;
                RegionPointInfo endPoint = m_PointList[iIndex + 1] as RegionPointInfo;
                startPoint.Pos = new Point(startPoint.Pos.X + ix, startPoint.Pos.Y - iy);
                endPoint.Pos = new Point(endPoint.Pos.X + ix, endPoint.Pos.Y - iy);
            }
        }

        // 将圆形顶点改为方形，即添加一个新的顶点
        private void _change(RegionPointInfo pointInfo)
        {
            pointInfo.Type = 0;

            Point pNewPrevious = new Point(pointInfo.Pos.X, pointInfo.Pos.Y);
            Point pNewNext = new Point(pointInfo.Pos.X, pointInfo.Pos.Y);

            int iIndex = m_PointList.IndexOf(pointInfo);

            RegionPointInfo pOldPrevious = m_PointList[iIndex - 1] as RegionPointInfo;
            RegionPointInfo pOldNext = m_PointList[0] as RegionPointInfo;
            if(iIndex == m_PointList.Count - 1)
            {
                pOldNext = m_PointList[0] as RegionPointInfo;
            }
            else
            {
                pOldNext = m_PointList[iIndex + 1] as RegionPointInfo;
            }

            pNewPrevious.X = (int)((pOldPrevious.Pos.X + pointInfo.Pos.X) / 2);
            pNewPrevious.Y = (int)((pOldPrevious.Pos.Y + pointInfo.Pos.Y) / 2);
            pNewNext.X = (int)((pOldNext.Pos.X + pointInfo.Pos.X) / 2);
            pNewNext.Y = (int)((pOldNext.Pos.Y + pointInfo.Pos.Y) / 2);
            RegionPointInfo startPoint = new RegionPointInfo(pNewPrevious, 1);
            RegionPointInfo endPoint = new RegionPointInfo(pNewNext, 1);
            m_PointList.Insert(iIndex, startPoint);
            iIndex = m_PointList.IndexOf(pointInfo);
            m_PointList.Insert(iIndex + 1, endPoint);

        }

        public int Move(int dx, int dy, int width, int height, bool operMode)
        { 
            // 如果有选中的点，优先移动那些点
            int iIndex = 0;
            foreach (RegionPointInfo pointInfo in m_PointList)
            {
                if(pointInfo.Selected)
                {
                    switch (pointInfo.Type)
                    {
                        case 0:
                            pointInfo.Move(dx, dy, width, height);                          
                            _formatPos(pointInfo, dx, dy);
                            if(m_iShape == 1)
                            {
                                RegionPointInfo xPoint = null;
                                RegionPointInfo yPoint = null;
                                if(iIndex == 0)
                                {
                                    xPoint = m_PointList[6] as RegionPointInfo;
                                    yPoint = m_PointList[2] as RegionPointInfo;
                                }
                                if (iIndex == 2)
                                {
                                    xPoint = m_PointList[4] as RegionPointInfo;
                                    yPoint = m_PointList[0] as RegionPointInfo;
                                }
                                if (iIndex == 4)
                                {
                                    xPoint = m_PointList[2] as RegionPointInfo;
                                    yPoint = m_PointList[6] as RegionPointInfo;
                                }
                                if (iIndex == 6)
                                {
                                    xPoint = m_PointList[0] as RegionPointInfo;
                                    yPoint = m_PointList[4] as RegionPointInfo;
                                }
                                xPoint.Move(dx, 0, width, height);
                                _formatPos(xPoint, dx, 0);
                                yPoint.Move(0, dy, width, height);
                                _formatPos(yPoint, 0, dy);
                            }
                            break;
                        case 1:
                            if (operMode && m_iShape == 0)
                            {
                                pointInfo.Move(dx, dy, width, height);
                                _change(pointInfo);
                            }
                            break;
                        default:
                            break;
                    }
                    return 0;
                }
                iIndex++;
            }

            // 没有移动的点，则移动整个区域
            ArrayList tempList = new ArrayList();
            foreach(RegionPointInfo pointInfo in m_PointList)
            {
                Point tpoint = new Point(pointInfo.Pos.X, pointInfo.Pos.Y);
                int tType = pointInfo.Type;
                RegionPointInfo tp = new RegionPointInfo(tpoint, tType);
                tp.Selected = pointInfo.Selected;
                tempList.Add(tp);
                pointInfo.Move(dx, dy);  
            }
            // 判断是否出界
            Rectangle bound = GetBound();
            if(bound.Left < 0 || bound.Top < 0 || bound.Right >= width || bound.Bottom >= height)
            {
                m_PointList = tempList;
            }

            return 1;
        }

        private void _deletePoint(RegionPointInfo pointInfo)
        {
            pointInfo.Type = 1;
            pointInfo.Selected = false;

            int iIndex = m_PointList.IndexOf(pointInfo);

            RegionPointInfo startPoint = m_PointList[m_PointList.Count - 1] as RegionPointInfo;
            RegionPointInfo endPoint = m_PointList[iIndex + 1] as RegionPointInfo;
            m_PointList.Remove(endPoint);

            if (iIndex == 0)
            {
                startPoint = m_PointList[m_PointList.Count - 1] as RegionPointInfo;
                m_PointList.Remove(pointInfo);
            }
            else
            {
                startPoint = m_PointList[iIndex - 1] as RegionPointInfo;
                m_PointList.Remove(startPoint);
            }
            

            FormatPos();
        }

        public void Delete()
        {
            // 顶点数小于3， 直接删除区域
            if(m_PointList.Count <= 6)
            {
                RegionManager.Instance.DeleteRegion(m_iID);
                return;
            }

            // 如果有选中的点，优先删除那些点
            foreach (RegionPointInfo pointInfo in m_PointList)
            {
                if(pointInfo.Selected)
                {
                    switch (pointInfo.Type)
                    {
                        case 0:
                            if(m_iShape == 0)
                            {
                                _deletePoint(pointInfo);
                            }
                            break;
                        case 1:
                            break;
                        default:
                            break;
                    }
                    return;
                }
            }

            // 没有移动的点，则删除整个区域
            RegionManager.Instance.DeleteRegion(m_iID);
        }

        // 返回区域的bound，包围矩形框
        public Rectangle GetBound()
        {
            RegionPointInfo firstPoint = m_PointList[0] as RegionPointInfo;
            Rectangle result = new Rectangle(firstPoint.Pos.X, firstPoint.Pos.Y, 0, 0);

            foreach(RegionPointInfo point in m_PointList)
            {
                int iX = point.Pos.X;
                int iY = point.Pos.Y;

                if(iX < result.Left)
                {
                    result.Width += (result.Left - iX);
                    result.X = iX;
                }

                if(iX > result.Right)
                {
                    result.Width = iX - result.X;
                }

                if(iY < result.Top)
                {
                    result.Height += (result.Top - iY);
                    result.Y = iY;
                }

                if(iY > result.Bottom)
                {
                    result.Height = iY - result.Y;
                }
            }

            return result;
        }

        // 调整区域的形状
        public void AdjustShape()
        {
            if(m_iShape == 0)
            {
                m_iShape = 1;
                Rectangle rect = GetBound();

                m_PointList.Clear();
                RegionPointInfo p4 = new RegionPointInfo(new Point(rect.Left, rect.Top));
                RegionPointInfo p3 = new RegionPointInfo(new Point(rect.Right, rect.Top));
                RegionPointInfo p2 = new RegionPointInfo(new Point(rect.Right, rect.Bottom));
                RegionPointInfo p1 = new RegionPointInfo(new Point(rect.Left, rect.Bottom));
                RegionPointInfo p12 = new RegionPointInfo(new Point(rect.Left, rect.Top), 1);
                RegionPointInfo p23 = new RegionPointInfo(new Point(rect.Right, rect.Top), 1);
                RegionPointInfo p34 = new RegionPointInfo(new Point(rect.Right, rect.Bottom), 1);
                RegionPointInfo p14 = new RegionPointInfo(new Point(rect.Left, rect.Bottom), 1);
                m_PointList.Add(p1);
                m_PointList.Add(p12);
                m_PointList.Add(p2);
                m_PointList.Add(p23);
                m_PointList.Add(p3);
                m_PointList.Add(p34);
                m_PointList.Add(p4);
                m_PointList.Add(p14);
                FormatPos();
            }
            else
            {
                m_iShape = 0;
            }
        }

        // 计算区域中所有顶点的高度差，用于拐点的判断，和多余点的删除
        private void CalHeightDifference()
        {
            foreach(RegionPointInfo info in m_PointList)
            {
                int index = m_PointList.IndexOf(info);

                // 只计算真正的顶点，也就是方点
                if(info.Type == 0)
                {
                    RegionPointInfo LeftPoint = null;
                    RegionPointInfo RightPoint = null;

                    if(index == 0)  // 第一个点
                    {
                        LeftPoint = m_PointList[m_PointList.Count - 2] as RegionPointInfo;
                        RightPoint = m_PointList[2] as RegionPointInfo;
                    }
                    else if(index == m_PointList.Count - 2) // 最后一个点 
                    {
                        LeftPoint = m_PointList[m_PointList.Count - 4] as RegionPointInfo;
                        RightPoint = m_PointList[0] as RegionPointInfo;
                    }
                    else// 中间的点
                    {
                        LeftPoint = m_PointList[index - 2] as RegionPointInfo;
                        RightPoint = m_PointList[index + 2] as RegionPointInfo;
                    }

                    info.LeftHeight = info.Pos.Y - LeftPoint.Pos.Y;
                    info.RightHeight = info.Pos.Y - RightPoint.Pos.Y;
                }
            }
        }

        // 删除多余的点，三点一线时，删除冗余的点
        private void RemoveRedundancyPoint()
        {
            // 首先重现计算所有的高度差
            CalHeightDifference();

            // 用于存放冗余的点
            ArrayList RedundancyList = new ArrayList();

            // 遍历寻找冗余的点
            foreach(RegionPointInfo info in m_PointList)
            {
                if(info.Type == 0)
                {
                    if(info.LeftHeight == 0 && info.RightHeight == 0)
                    {
                        RedundancyList.Add(info);
                    }
                }
            }

            // 删除冗余的点
            foreach(RegionPointInfo rInfo in RedundancyList)
            {
                _deletePoint(rInfo);
            }

            // 设置顶点的格子标志
            foreach (RegionPointInfo info in m_PointList)
            {
                if (info.Type == 0)
                {
                    m_GridInfo[info.Pos.X, info.Pos.Y] = 2;
                }
            }
        }

        // 计算在边上的格子
        public void GetGridInLine(RegionPointInfo p1, RegionPointInfo p2)
        {
            /*
            // 计算dy，来判断线段的方向，
            int dy = p2.Pos.Y - p1.Pos.Y;
            int dx = p2.Pos.X - p1.Pos.X;
            float dxy = (float)dx / (float)dy;

            if(dy == 0)
            {
                if(dx == 0)
                {
                    return gridList;
                }
                else if(dx < 0)
                {
                    for (int ix = p2.Pos.X + 1; ix != p1.Pos.X; ix++)
                    {
                        Point p = new Point(ix, p2.Pos.Y);
                        gridList.Add(p);
                    }
                }
                else
                {
                    for (int ix = p1.Pos.X + 1; ix != p2.Pos.X;ix++ )
                    {
                        Point p = new Point(ix, p1.Pos.Y);
                        gridList.Add(p);
                    }
                }
            }
            else if(dy < 0)
            {
                float xs = (float)p2.Pos.X;

                for(int y = p2.Pos.Y + 1; y < p1.Pos.Y;y++)
                {
                    xs += dxy;

                    Point p = new Point((int)(xs + 0.5f), y);
                    gridList.Add(p);
                }
            }
            else
            {
                float xs = (float)p1.Pos.X;

                for (int y = p1.Pos.Y + 1; y < p2.Pos.Y; y++)
                {
                    xs += dxy;

                    Point p = new Point((int)(xs + 0.5f), y);
                    gridList.Add(p);
                }
            }
            */

            int x1 = p1.Pos.X;
            int y1 = p1.Pos.Y;
            int x2 = p2.Pos.X;
            int y2 = p2.Pos.Y;

            int e, t, x, y, yi, tx, ty, dx, dy;

            x = x1;
            y = y1;
            yi = y1;

            tx = Math.Abs(x2 - x1);
            ty = Math.Abs(y2 - y1);

            if (tx > ty)
            {
                t = tx;
                e = 2 * ty - tx;
            }
            else
            {
                t = ty;
                e = 2 * tx - ty;
            }

            if (x2 >= x1)
            {
                dx = 1;
            }
            else
            {
                dx = -1;
            }

            if (y2 >= y1)
            {
                dy = 1;
            }
            else
            {
                dy = -1;
            }

            for (int i = 1; i < t; i++)
            {
                if (tx >= ty)
                {
                    x = x + dx;
                    if (e >= 0)
                    {
                        y = y + dy;
                        e = e - 2 * tx;
                    }
                    e = e + 2 * ty;
                }
                else
                {
                    y = y + dy;
                    if (e >= 0)
                    {
                        x = x + dx;
                        e = e - 2 * ty;
                    }
                    e = e + 2 * tx;
                }

                if(m_GridInfo[x, y] == 0)
                {
                    m_GridInfo[x, y] = 1;
                }
                else if(m_GridInfo[x, y] == 1)
                {
                    m_GridInfo[x, y] = 2;
                }
                
            }
        }

        //  计算出区域边缘的点
        private void CalEdgePoint()
        {
            // 计算新的数据
            foreach (RegionPointInfo info in m_PointList)
            {
                int index = m_PointList.IndexOf(info);

                // 只计算真正的顶点，也就是方点
                if (info.Type == 0)
                {
                    RegionPointInfo nextPoint = null;

                    if (index == m_PointList.Count - 2) // 最后一个点 
                    {
                        nextPoint = m_PointList[0] as RegionPointInfo;
                    }
                    else// 中间的点
                    {
                        nextPoint = m_PointList[index + 2] as RegionPointInfo;
                    }

                    // 计算一条边上的格子
                    GetGridInLine(info, nextPoint);
                }
            }
        }

        // 计算出区域内部的点
        private void CalInnerPoint()
        {
            // 清空原先的数据
            InitGridInfo();

            // 删除冗余的点，并写入顶点信息
            RemoveRedundancyPoint();

            // 计算出在边上的格子
            CalEdgePoint();

            //TestPrint();
            // 计算在区域中的点

            // 计算出一个种子,即一个内部的点
            // 首先要先获得工作区, 也就是区域的包围框, 这样可以减少计算量。
            Rectangle workArea = this.GetBound();
            //workArea.X -= 1;
            //workArea.Y -= 1;
            //workArea.Width += 2;
            //workArea.Height += 2;

            // 在工作区内，计算出一个种子
            bool bFindSeed = false;
            int iSeedX = 0;
            int iSeedY = 0;

            //////////////////////////////////////////////////////////////////////////
            while (true)
            {
                // 从左向右扫描
                if (!bFindSeed)
                {
                    for (int y = workArea.Top; y <= workArea.Bottom; y++)
                    {
                        if (bFindSeed)
                        {
                            break;
                        }
                        else
                        {
                            bool bFindEdge = false;
                            for (int x = workArea.Left; x <= workArea.Right; x++)
                            {
                                if (bFindSeed)
                                {
                                    break;
                                }
                                else
                                {
                                    if (bFindEdge)
                                    {
                                        if (!IsEdgePoint(x, y) && !IsVertexPoint(x, y))
                                        {
                                            if (!IsInnerPoint(x, y) && !IsOutPoint(x, y))
                                            {
                                                bFindSeed = true;
                                                iSeedX = x;
                                                iSeedY = y;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (IsVertexPoint(x, y))    // 如果是顶点，直接跳出
                                        {
                                            break;
                                        }
                                        else if (IsEdgePoint(x, y)) // 如果是边，打上标记
                                        {
                                            bFindEdge = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // 从右向左扫描
                if (!bFindSeed)
                {
                    for (int y = workArea.Top; y <= workArea.Bottom; y++)
                    {
                        if (bFindSeed)
                        {
                            break;
                        }
                        else
                        {
                            bool bFindEdge = false;
                            for (int x = workArea.Right; x >= workArea.Left; x--)
                            {
                                if (bFindSeed)
                                {
                                    break;
                                }
                                else
                                {
                                    if (bFindEdge)
                                    {
                                        if (!IsEdgePoint(x, y) && !IsVertexPoint(x, y))
                                        {
                                            if (!IsInnerPoint(x, y) && !IsOutPoint(x, y))
                                            {
                                                bFindSeed = true;
                                                iSeedX = x;
                                                iSeedY = y;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (IsVertexPoint(x, y))    // 如果是顶点，直接跳出
                                        {
                                            break;
                                        }
                                        else if (IsEdgePoint(x, y)) // 如果是边，打上标记
                                        {
                                            bFindEdge = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // 从上向下扫描
                if (!bFindSeed)
                {
                    for (int x = workArea.Left; x <= workArea.Right; x++)
                    {
                        if (bFindSeed)
                        {
                            break;
                        }
                        else
                        {
                            bool bFindEdge = false;
                            for (int y = workArea.Top; y <= workArea.Bottom; y++)
                            {
                                if (bFindSeed)
                                {
                                    break;
                                }
                                else
                                {
                                    if (bFindEdge)
                                    {
                                        if (!IsEdgePoint(x, y) && !IsVertexPoint(x, y))
                                        {
                                            if (!IsInnerPoint(x, y) && !IsOutPoint(x, y))
                                            {
                                                bFindSeed = true;
                                                iSeedX = x;
                                                iSeedY = y;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (IsVertexPoint(x, y))    // 如果是顶点，直接跳出
                                        {
                                            break;
                                        }
                                        else if (IsEdgePoint(x, y)) // 如果是边，打上标记
                                        {
                                            bFindEdge = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // 从下向上扫描
                if (!bFindSeed)
                {
                    for (int x = workArea.Left; x <= workArea.Right; x++)
                    {
                        if (bFindSeed)
                        {
                            break;
                        }
                        else
                        {
                            bool bFindEdge = false;
                            for (int y = workArea.Bottom; y >= workArea.Top; y--)
                            {
                                if (bFindSeed)
                                {
                                    break;
                                }
                                else
                                {
                                    if (bFindEdge)
                                    {
                                        if (!IsEdgePoint(x, y) && !IsVertexPoint(x, y))
                                        {
                                            if(!IsInnerPoint(x, y) && !IsOutPoint(x, y))
                                            {
                                                bFindSeed = true;
                                                iSeedX = x;
                                                iSeedY = y;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (IsVertexPoint(x, y))    // 如果是顶点，直接跳出
                                        {
                                            break;
                                        }
                                        else if (IsEdgePoint(x, y)) // 如果是边，打上标记
                                        {
                                            bFindEdge = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //////////////////////////////////////////////////////////////////////////

                // 根据种子，进行递归
                if (bFindSeed)
                {
                    SeedNeighbour(iSeedX, iSeedY);
                    bFindSeed = false;
                }
                else
                {
                    break;
                }
            }

            // 测试输出
            //TestPrint();
        }

        private void SeedNeighbourOld(int x, int y)
        {
            // 如果已经是内部点，直接返回
            if(IsContainPoint(x, y))
            {
                return;
            }
            else
            {
                // 将当前点标记为内部点
                m_GridInfo[x, y] = 3;

                // 四个方向进行扩展
                // 上
                if (!IsEdgePoint(x, y + 1) && !IsVertexPoint(x, y + 1))
                {
                    SeedNeighbour(x, y + 1);
                }
                // 下
                if (!IsEdgePoint(x, y - 1) && !IsVertexPoint(x, y - 1))
                {
                    SeedNeighbour(x, y - 1);
                }
                // 左
                if (!IsEdgePoint(x - 1, y) && !IsVertexPoint(x - 1, y))
                {
                    SeedNeighbour(x - 1, y);
                }
                // 右
                if (!IsEdgePoint(x + 1, y) && !IsVertexPoint(x + 1, y))
                {
                    SeedNeighbour(x + 1, y);
                }
            }
        }

        private void SeedNeighbour(int x, int y)
        {
            ArrayList oldList = new ArrayList();
            ArrayList newList = new ArrayList();

            Point seedPoint = new Point(x, y);
            oldList.Add(seedPoint);

            while(oldList.Count != 0)
            {
                newList.Clear();
                foreach(Point p in oldList)
                {
                    if(IsContainPoint(p.X, p.Y))
                    {
                        continue;
                    }

                    m_GridInfo[p.X, p.Y] = 3;

                    // 四个方向进行扩展
                    // 上
                    if (!IsContainPoint(p.X, p.Y + 1))
                    {
                        Point upPoint = new Point(p.X, p.Y + 1);
                        newList.Add(upPoint);
                    }
                    // 下
                    if (!IsContainPoint(p.X, p.Y - 1))
                    {
                        Point downPoint = new Point(p.X, p.Y - 1);
                        newList.Add(downPoint);
                    }
                    // 左
                    if (!IsContainPoint(p.X - 1, p.Y))
                    {
                        Point leftPoint = new Point(p.X - 1, p.Y);
                        newList.Add(leftPoint);
                    }
                    // 右
                    if (!IsContainPoint(p.X + 1, p.Y))
                    {
                        Point rightPoint = new Point(p.X + 1, p.Y);
                        newList.Add(rightPoint);
                    }
                }

                oldList.Clear();

                foreach(Point newPoint in newList)
                {
                    Point temp = new Point(newPoint.X, newPoint.Y);
                    oldList.Add(temp);
                }

                newList.Clear();
            }
        }

        private bool IsEdgePoint(int x, int y)
        {
            if (m_GridInfo[x, y] == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsVertexPoint(int x, int y)
        {
            if (m_GridInfo[x, y] == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsInnerPoint(int x, int y)
        {
            if(m_GridInfo[x, y] == 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsContainPoint(int x, int y)
        {
            return (IsVertexPoint(x, y) || IsEdgePoint(x, y) || IsInnerPoint(x, y));
        }

        public bool IsOutPoint(int x, int y)
        {
            if (m_GridInfo[x, y] == 4)
            {
                return true;
            }
            else
            {
                // 四方向检测
                // 上 
                bool bUp = false;
                for (int i = 0; y+i < MaxHeight;i++)
                {
                    if (IsVertexPoint(x, y + i) || IsEdgePoint(x, y + i))
                    {
                        bUp = true;
                    }
                }
                // 下
                bool bDown = false;
                for (int i = 0; y+i >= 0; i--)
                {
                    if (IsVertexPoint(x, y + i) || IsEdgePoint(x, y + i))
                    {
                        bDown = true;
                    }
                }
                // 左 
                bool bLeft = false;
                for (int i = 0; x+i >= 0; i--)
                {
                    if (IsVertexPoint(x + i, y) || IsEdgePoint(x + i, y))
                    {
                        bLeft = true;
                    }
                }
                // 右 
                bool bRight = false;
                for (int i = 0; x+i < MaxWidth; i++)
                {
                    if (IsVertexPoint(x + i, y) || IsEdgePoint(x + i, y))
                    {
                        bRight = true;
                    }
                }

                bool bIn = bUp & bDown & bLeft & bRight;

                if(bIn)
                {
                    return false;
                }
                else
                {
                    m_GridInfo[x, y] = 4;
                    return true;
                }
            }
        }

        private void InitGridInfo()
        {
            if(m_GridInfo == null)
            {
                m_GridInfo = new int[MaxWidth, MaxHeight];
            }

            for(int x = 0;x < MaxWidth; x++)
            {
                for (int y = 0; y < MaxHeight; y++)
                {
                    m_GridInfo[x, y] = 0;
                }
            }
        }

        public void CalContainPoint()
        {
            // 清空老的数据
            m_ContainList.Clear();

            // 计算出临时数据
            CalInnerPoint();

            // 首先要先获得工作区, 也就是区域的包围框, 这样可以减少计算量。
            Rectangle workArea = this.GetBound();

            for (int y = workArea.Top; y <= workArea.Bottom; y++)
            {
                for (int x = workArea.Left; x <= workArea.Right; x++)
                { 
                    if(IsContainPoint(x, y))
                    {
                        Point p = new Point(x, y);
                        m_ContainList.Add(p);
                    }
                }
            }

            // 清空数据，释放内存
            m_GridInfo = null;
        }

        private void TestPrint()
        {
            //////////////////////////////////////////////////////////////////////////
            // 测试输出
            StreamWriter writer = new System.IO.StreamWriter("edge.txt", false, System.Text.Encoding.GetEncoding("GB2312"));

            for (int y = MaxHeight - 1; y >= 0; y--)
            {
                for (int x = 0; x < MaxWidth; x++)
                {
                    writer.Write(m_GridInfo[x, y]);
                }

                writer.Write("\r\n");
            }

            writer.Flush();
            writer.Close();

            MessageBox.Show("输出完毕");
            //////////////////////////////////////////////////////////////////////////
        }
        //////////////////////////////////////////////////////////////////////////
        // 私有成员
        private int m_iID;          // 区域 的唯一编号
        private int m_iGroupID;     // 区域所属的组
        private int m_iMapID;       // 所在场景编号
        private String m_strName = "新的区域";   // 区域的名字
        private ArrayList m_PointList = new ArrayList();    //保存区域所有的点
        //private ArrayList m_EdgePointList = new ArrayList();    // 用于存放在边上的格子位置，类型为Point
        //private ArrayList m_InnerPointList = new ArrayList();      // 用于存放内部的格子位置，类型为Point
        private ArrayList m_ContainList = new ArrayList();  // 用于存放区域所覆盖到的点，包括顶点，边点，内部点。
        private int[,] m_GridInfo;     // 区域映射的格子属性
        private int m_iShape = 0;       // 区域的形状, 0为多边形, 1为矩形 
        private bool m_bHidden = false;    // 区域是否可见  
        //////////////////////////////////////////////////////////////////////////
        
    }

    public class RegionPointInfo
    {
        public RegionPointInfo(Point point)
        {
            m_Pos = point;
        }

        public RegionPointInfo(Point point, int type)
        {
            m_Pos = point;
            m_iType = type;
        }

        // 共有属性
        [CategoryAttribute("当前位置")]
        public Point Pos
        {
            get
            {
                return m_Pos;
            }
            set
            {
                m_Pos = value;
            }
        }

        [BrowsableAttribute(false)]
        public int Type
        {
            get
            {
                return m_iType;
            }
            set
            {
                m_iType = value;
            }
        }

        // 当前控件是否被选择
        [BrowsableAttribute(false)]
        public bool Selected
        {
            get
            {
                return m_Selected;
            }
            set
            {
                m_Selected = value;
            }
        }

        [BrowsableAttribute(false)]
        public int LeftHeight
        {
            get
            {
                return m_iLeftHeight;
            }
            set
            {
                m_iLeftHeight = value;
            }
        }

        [BrowsableAttribute(false)]
        public int RightHeight
        {
            get
            {
                return m_iRightHeight;
            }
            set
            {
                m_iRightHeight = value;
            }
        }

        //////////////////////////////////////////////////////////////////////////
        
        // 带边界检测
        public void Move(int dx, int dy, int width, int height)
        {
            int iNewX = m_Pos.X + dx;
            int iNewY = m_Pos.Y - dy;

            if(iNewX < 0)
            {
                iNewX = m_Pos.X;
            }
            if (iNewX >= width)
            {
                iNewX = m_Pos.X;
            }
            if (iNewY < 0)
            {
                iNewY = m_Pos.Y;
            }
            if (iNewY >= height)
            {
                iNewY = m_Pos.Y;
            }

            Point newPoint = new Point(iNewX, iNewY);
            m_Pos = newPoint;
        }

        // 不带边界检测
        public void Move(int dx, int dy)
        {
            int iNewX = m_Pos.X + dx;
            int iNewY = m_Pos.Y - dy;

            Point newPoint = new Point(iNewX, iNewY);
            m_Pos = newPoint;
        }

        //////////////////////////////////////////////////////////////////////////
        // 私有成员
        private Point m_Pos = new System.Drawing.Point();     //记录点的位置 
        private int m_iType = 0;    //点的类型，0为方型顶点，1为圆形顶点
        private bool m_Selected = false;        // 当前 区域 是否被选择.
        private int m_iLeftHeight = 0;    // 顶点与左边相邻顶点的高度差，用于判断该顶点是否为拐点。
        private int m_iRightHeight = 0;    // 顶点与右边相邻顶点的高度差，用于判断该顶点是否为拐点。
        //////////////////////////////////////////////////////////////////////////
        
    }

    public class RegionDesc
    {
        // 初始化
        public RegionDesc()
        {
            // 音乐属性
            DataTable m_MainMusic = OptionManager.MusicTemplate.Copy();
            m_MainMusic.TableName = "MainMusic";
            m_MusicList.Add(m_MainMusic);

            DataTable m_SubMusic1 = OptionManager.MusicTemplate.Copy();
            m_SubMusic1.TableName = "SubMusic1";
            m_MusicList.Add(m_SubMusic1);

            DataTable m_SubMusic2 = OptionManager.MusicTemplate.Copy();
            m_SubMusic2.TableName = "SubMusic2";
            m_MusicList.Add(m_SubMusic2);

            DataTable m_SubMusic3 = OptionManager.MusicTemplate.Copy();
            m_SubMusic3.TableName = "SubMusic3";
            m_MusicList.Add(m_SubMusic3);

            // 音效属性
            DataTable m_MainSound = OptionManager.SoundTemplate.Copy();
            m_MainSound.TableName = "MainSound";
            m_SoundList.Add(m_MainSound);

            DataTable m_SubSound1 = OptionManager.SoundTemplate.Copy();
            m_SubSound1.TableName = "SubSound1";
            m_SoundList.Add(m_SubSound1);

            DataTable m_SubSound2 = OptionManager.SoundTemplate.Copy();
            m_SubSound2.TableName = "SubSound2";
            m_SoundList.Add(m_SubSound2);

            // 特效属性
            DataTable m_Effect = OptionManager.EffectTemplate.Copy();
            m_Effect.TableName = "Effect";
            m_EffectList.Add(m_Effect);
        }

        // 根据音乐，音效库，更新文件
        public void Update(int type, String tablename, XmlElement elmt)
        {
            // 音乐属性
            if (type == 1)
            {
                foreach (DataTable dt in m_MusicList)
                {
                    if (dt.TableName.Equals(tablename))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            String strPropName = dr["属性"].ToString();
                            if (elmt.HasAttribute(strPropName))
                            {
                                dr["值"] = elmt.GetAttribute(strPropName);
                            }
                        }
                    }
                }
            }

            if (type == 2)
            {
                // 音效属性
                foreach (DataTable dt in m_SoundList)
                {
                    if (dt.TableName.Equals(tablename))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            String strPropName = dr["属性"].ToString();
                            if (elmt.HasAttribute(strPropName))
                            {
                                dr["值"] = elmt.GetAttribute(strPropName);
                            }
                        }
                    }
                }
            }
        }

        // 将音乐信息与音乐库进行同步
        public void Coordinate(int type, Hashtable libTable)
        {
            // 音乐属性
            if (type == 1)
            {
                foreach (DataTable dt in m_MusicList)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        String strPropName = dr["属性"].ToString();
                        if (strPropName.Equals("Filename"))
                        {
                            String strPropValue = dr["值"].ToString();
                            if (strPropValue.Length > 0)
                            {
                                XmlElement xmlElmt = libTable[strPropValue] as XmlElement;
                                if (xmlElmt != null)
                                {
                                    this.Update(type, dt.TableName, xmlElmt);
                                }
                            }
                        }
                    }
                }
            }

            if (type == 2)
            {
                // 音效属性
                foreach (DataTable dt in m_SoundList)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        String strPropName = dr["属性"].ToString();
                        if (strPropName.Equals("Filename"))
                        {
                            String strPropValue = dr["值"].ToString();
                            if (strPropValue.Length > 0)
                            {
                                XmlElement xmlElmt = libTable[strPropValue] as XmlElement;
                                if (xmlElmt != null)
                                {
                                    this.Update(type, dt.TableName, xmlElmt);
                                }
                            }
                        }
                    }
                }

            }
        }

        // 加载在节点中的信息
        public void Load(XmlElement elmt)
        {
            // 基本属性
            if(elmt.HasAttribute("EnableSound"))
            {
                if(elmt.GetAttribute("EnableSound").Equals("1"))
                {
                    m_bEnableSound = true;
                }
                else
                {
                    m_bEnableSound = false;
                }
            }

            if (elmt.HasAttribute("EnableEffect"))
            {
                if (elmt.GetAttribute("EnableEffect").Equals("1"))
                {
                    m_bEnableEffect = true;
                }
                else
                {
                    m_bEnableEffect = false;
                }
            }

            // 音乐属性
            foreach (DataTable dt in m_MusicList)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    String strPropName = dt.TableName + dr["属性"].ToString();
                    if(elmt.HasAttribute(strPropName))
                    {
                        dr["值"] = elmt.GetAttribute(strPropName);
                    }
                }
            }

            // 音效属性
            foreach (DataTable dt in m_SoundList)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    String strPropName = dt.TableName + dr["属性"].ToString();
                    if (elmt.HasAttribute(strPropName))
                    {
                        dr["值"] = elmt.GetAttribute(strPropName);
                    }
                }
            }

            // 特效属性
            foreach (DataTable dt in m_EffectList)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    String strPropName = dt.TableName + dr["属性"].ToString();
                    if (elmt.HasAttribute(strPropName))
                    {
                        dr["值"] = elmt.GetAttribute(strPropName);
                    }
                }
            }

        }

        // 在xml节点上添加属性
        public void Save(XmlElement elmt)
        {
            // 基本属性
            if(m_bEnableSound)
            {
                elmt.SetAttribute("EnableSound", "1");
            }
            else
            {
                elmt.SetAttribute("EnableSound", "0");
            }
            
            if(m_bEnableEffect)
            {
                elmt.SetAttribute("EnableEffect", "1");
            }
            else
            {
                elmt.SetAttribute("EnableEffect", "0");
            }

            // 音乐属性
            foreach(DataTable dt in m_MusicList)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    elmt.SetAttribute(dt.TableName + dr["属性"].ToString(), dr["值"].ToString());
                }
            }

            // 音效属性
            foreach (DataTable dt in m_SoundList)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    elmt.SetAttribute(dt.TableName + dr["属性"].ToString(), dr["值"].ToString());
                }
            }

            // 特效属性
            foreach (DataTable dt in m_EffectList)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    elmt.SetAttribute(dt.TableName + dr["属性"].ToString(), dr["值"].ToString());
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////
        public bool EnableSound
        {
            get
            {
                return m_bEnableSound;
            }
            set
            {
                m_bEnableSound = value;
            }
        }

        public bool EnableEffect
        {
            get
            {
                return m_bEnableEffect;
            }
            set
            {
                m_bEnableEffect = value;
            }
        }

        public ArrayList MusicList
        {
            get
            {
                return m_MusicList;
            }
            set
            {
                m_MusicList = value;
            }
        }

        public ArrayList SoundList
        {
            get
            {
                return m_SoundList;
            }
            set
            {
                m_SoundList = value;
            }
        }

        public ArrayList EffectList
        {
            get
            {
                return m_EffectList;
            }
            set
            {
                m_EffectList = value;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // 私有成员

        // 基本属性
        bool m_bEnableSound = false;
        bool m_bEnableEffect = false;

        // 音乐属性
        ArrayList m_MusicList = new ArrayList();

        // 音效属性
        ArrayList m_SoundList = new ArrayList();

        // 特效属性
        ArrayList m_EffectList = new ArrayList();
    };
}
