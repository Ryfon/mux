﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace MissionEditor
{
    public class SceneSketchManager
    {
        private SceneSketchManager()
        {
        }

        static public SceneSketchManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    // 初始化唯一实例，载入所有场景缩率图信息
                    ms_Instance = new SceneSketchManager();
                    //LoadAllSceneSketch();
                }
                return ms_Instance;
            }
        }

        // 获取场景缩略图列表
        public ArrayList SceneSketchList
        {
            get
            {
                return m_SceneSketchList;
            }
        }

        // 载入单张场景
        private SceneSketch LoadSceneSketch(int iSceneID)
        {
            String strFilePath = OptionManager.SceneSketchPath + iSceneID.ToString() + ".xml";
            if (File.Exists(strFilePath))
            {
                // 载入该 xml
                XmlDocument xmlDoc = new XmlDocument();
                StreamReader reader = new StreamReader(strFilePath, Encoding.UTF8);
                xmlDoc.Load(reader);
                XmlElement xmlElemRoot = xmlDoc.FirstChild as XmlElement;
                XmlElement xmlElemMapInfo = xmlElemRoot.FirstChild as XmlElement;
                if (xmlElemMapInfo == null || !xmlElemMapInfo.Name.Equals("MapInfo"))
                {
                    return null;
                }
                //int iSceneID = int.Parse(xmlElemMapInfo.GetAttribute("MapNumber"));
                int iDimX = int.Parse(xmlElemMapInfo.GetAttribute("DimX"));
                int iDimY = int.Parse(xmlElemMapInfo.GetAttribute("DimY"));

                // 根据得到的ID创建场景
                SceneSketch sketch = new SceneSketch(iSceneID, iDimX, iDimY);

                // 加载场景草图
                String strImageFilename = OptionManager.SceneSketchPath + iSceneID.ToString() + ".jpg";
                sketch.LoadSceneImage(strImageFilename);

                //// 加载地图碰撞信息
                //String strBlockFileName = OptionManager.SceneSketchPath + "terrainProperty" + iSceneID.ToString() + ".map";
                //sketch.LoadBlockInfo(strBlockFileName, Color.Red, 0.3f);

                //// 加载路径权重信息
                //String strWayInfoFileName = OptionManager.WayInfoPath + "WayInfo" + iSceneID.ToString() + ".way";
                //sketch.LoadWayInfo(strWayInfoFileName, PathPaintForm.Instance.SmallColor, PathPaintForm.Instance.BigColor, 0.3f);

                // 判断是否存在重名
                foreach (SceneSketch sceneSketch in m_SceneSketchList)
                {
                    if (sceneSketch.SceneID == iSceneID)
                    {
                        MessageBox.Show("发现多个 ID 为 " + iSceneID.ToString() + " 的场景缩略图.请检查 SceneSketch 目录.");
                    }
                }

                // 将sketch添加入列表，进行统一管理
                m_SceneSketchList.Add(sketch);
                m_SceneIDList.Add(iSceneID);

                // 将结果返回
                return sketch;
            }
            else
            {
                return null;
            }

        }

        // 载入所有场景草图信息
        public void LoadAllSceneSketch()
        {
            m_SceneSketchList.Clear();
            m_SceneIDList.Clear();
            m_SceneNameList.Clear();

            //String[] fileList = System.IO.Directory.GetFileSystemEntries(OptionManager.SceneSketchPath);
            //foreach (String strFilePath in fileList)
            //{

            String strSceneListFile = OptionManager.SceneSketchPath + "scene.xml";
            if(File.Exists(strSceneListFile))
            {
                XmlDocument xmlDocSceneList = new XmlDocument();
                StreamReader listReader = new StreamReader(strSceneListFile, Encoding.GetEncoding("GB2312"));
                xmlDocSceneList.Load(listReader);
                XmlElement xmlListRoot = xmlDocSceneList.LastChild as XmlElement;
                XmlElement xmlMapName = xmlListRoot.FirstChild as XmlElement;
                
                // 开始遍历读取
                while(xmlMapName != null)
                {
                    int iSceneID = int.Parse(xmlMapName.GetAttribute("sceneId"));
                    String strSceneName = xmlMapName.GetAttribute("scenename");

                    SceneSketch scene = LoadSceneSketch(iSceneID);
                    if(scene != null)
                    {
                        scene.SceneName = strSceneName;
                        m_SceneNameList.Add(strSceneName);
                    }
                    // 遍历下一个节点
                    xmlMapName = xmlMapName.NextSibling as XmlElement;
                }

            }
             
        }

        // 导出配置文件，方便客户端更新（只是文件路径和文件名的更新）
        public int ExportAllConfigFile(String strDestDirPath)
        {
            String path = strDestDirPath;

            // 根目录的创建
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // 创建客户端的TerrainScene文件夹
            path += "TerrainScene/";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // 为所有的scene创建独立的文件夹
            foreach(int iSceneID in m_SceneIDList)
            {
                // 创建文件夹
                String strSceneDir = path + iSceneID.ToString() + "/";
                if (!Directory.Exists(strSceneDir))
                {
                    Directory.CreateDirectory(strSceneDir);
                }

                // copy文件，并重命名
                // -- map文件
                String strMapSrcFile = OptionManager.SceneSketchPath + "terrainProperty" + iSceneID.ToString() + ".map";
                String strMapDestFile = strSceneDir + "terrainProperty.map";
                if(File.Exists(strMapSrcFile))
                {
                    File.Copy(strMapSrcFile, strMapDestFile, true);
                }
                
                // -- RegionDesc文件
                String strDescSrcFile = OptionManager.RegionDescPath + "RegionDesc_" + iSceneID.ToString() + ".xml";
                String strDescDestFile = strSceneDir + "RegionDesc.xml";
                if(File.Exists(strDescSrcFile))
                {
                    File.Copy(strDescSrcFile, strDescDestFile, true);
                }
            }

            return 0;
        }

        public void SaveAll()
        {
            foreach (SceneSketch sceneSketch in m_SceneSketchList)
            {
                sceneSketch.SaveWayInfo(OptionManager.WayInfoPath + "WayInfo" + sceneSketch.SceneID.ToString() + ".way");
            }
        }

        // 根据场景 ID 获取场景草图信息
        public SceneSketch GetSceneSketch(int iSceneID)
        {
            foreach (SceneSketch sceneSketch in m_SceneSketchList)
            {
                if (sceneSketch.SceneID == iSceneID)
                {
                    return sceneSketch;
                }
            }
            return null;
        }

        public String GetSceneNameByID(int iSceneID)
        {
            foreach (SceneSketch sceneSketch in m_SceneSketchList)
            {
                if (sceneSketch.SceneID == iSceneID)
                {
                    return sceneSketch.SceneName;
                }
            }
            return "";
        }

        public int GetSceneIDByName(String strName)
        {
            foreach (SceneSketch sceneSketch in m_SceneSketchList)
            {
                if (sceneSketch.SceneName.Equals(strName))
                {
                    return sceneSketch.SceneID;
                }
            }
            return -1;
        }

        public ArrayList GetSceneIDList()
        {
            return m_SceneIDList;  
        }

        public ArrayList GetSceneNameList()
        {
            return m_SceneNameList;
        }

        public int GetBlockInfo(int iMapID, int iX, int iY)
        {
            SceneSketch scene = GetSceneSketch(iMapID);

            if(scene == null)
            {
                return 0;
            }
            else
            {
                return scene.GetBlockInfo(iX, iY);
            }
        }

        static private SceneSketchManager ms_Instance = null;
        private ArrayList m_SceneSketchList = new ArrayList();  // 场景缩略图列表。
        private ArrayList m_SceneIDList = new ArrayList();      // 场景ID列表
        private ArrayList m_SceneNameList = new ArrayList();    // 场景名称列表
    }
}
