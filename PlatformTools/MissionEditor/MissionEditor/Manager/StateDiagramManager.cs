﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MissionEditor
{
    // 状态图管理器
    class StateDiagramManager
    {


        private StateDiagramManager()
        {

        }

        public static StateDiagramManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new StateDiagramManager();
                }

                return ms_Instance;
            }
        }

        public static void SaveStateDiagram(StateDiagram dia)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement xmlElem = dia.GetAsXmlElement(xmlDoc);
            xmlDoc.AppendChild(xmlElem);
            xmlDoc.Save(OptionManager.StateDiagramPath + dia.Name + ".xml");
            dia.Dirty = false;
        }

        // 载入所有的状态图
        public void LoadAllStateDiagram()
        {
            // 遍历 Data/StateDiagram 下所有 xml 文件
            String[] fileList = System.IO.Directory.GetFileSystemEntries(OptionManager.StateDiagramPath);
            foreach (String strFilePath in fileList)
            {
                if (System.IO.Path.GetExtension(strFilePath).Equals(".xml"))
                {
                    String fileName = System.IO.Path.GetFileNameWithoutExtension(strFilePath);
                    GetStateDiagram(fileName);
                }
            }
        }

        public StateDiagram GetStateDiagram(String strDiagramName)
        {
            if (strDiagramName == null)
            {
                return null;
            }

            StateDiagram dia = null;
            // 判断该 diagram 是否已经载入。如果载入，返回。否则从 xml 中载入后返回。
            if (m_Name2Diagram.ContainsKey(strDiagramName))
            {
                dia = m_Name2Diagram[strDiagramName] as StateDiagram;
                return dia;
            }

            dia = GetStateDiagramFormXml("Data/StateDiagram/" + strDiagramName + ".xml");
            return dia;

        }

        public void SetDiagram(String strName, StateDiagram dia)
        {
            if (strName != null)
            {
                m_Name2Diagram[strName] = dia;
            }
        }

        public Hashtable DiagramMap
        {
            get
            {
                return m_Name2Diagram;
            }
        }

        // 从 xml 中载入 state diagram
        private StateDiagram GetStateDiagramFormXml(String strXmlFileName)
        {
            // 判断该文件是否存在。如果不存在返回 null
            if (!System.IO.File.Exists(strXmlFileName))
            {
                return null;
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFileName);
            XmlElement rootElem = xmlDoc.FirstChild as XmlElement;
            StateDiagram dia = new StateDiagram();
            dia.Name = rootElem.GetAttribute("Name");
            dia.ID = int.Parse(rootElem.GetAttribute("ID"));
            XmlElement xmlElem = rootElem.FirstChild as XmlElement;
            while (xmlElem != null)
            {
                if (xmlElem.Name == "State")
                {
                    State state = new State(xmlElem);
                    dia.StateList.Add(state);
                }
                else if(xmlElem.Name == "Translation")
                {
                    Translation trans = new Translation(xmlElem);
                    dia.TranslationList.Add(trans);
                }
                xmlElem = xmlElem.NextSibling as XmlElement;
            }
            m_Name2Diagram[dia.Name] = dia;
            dia.Dirty = false;
            return dia;
        }


        static public void RejudgeAllConnectedToRoot(Panel canvas)
        {
            // 获取 idle state
            StateControl rootCtrl = null;
            foreach (Control ctrl in canvas.Controls)
            {
                if (ctrl is StateControl)
                {
                    (ctrl as StateControl).ConnectToRoot = false;
                    if ((ctrl as StateControl).StateName.Equals(OptionManager.IdleStateName))
                    {
                        rootCtrl = ctrl as StateControl;
                    }
                }
                else if (ctrl is TranslationControl)
                {
                    (ctrl as TranslationControl).ConnectToRoot = false;
                }
            }

            if (rootCtrl != null)
            {
                ArrayList overList = new ArrayList();
                SetConnectToRoot(rootCtrl, true, overList);
            }

        }

        static public void SetConnectToRoot(TranslationControl ctrl, bool bConnect, ArrayList overList)
        {
            if (ctrl==null || overList.Contains(ctrl))
            {
                return;
            }

            ctrl.ConnectToRoot = bConnect;
            overList.Add(ctrl);
            SetConnectToRoot(ctrl.EndStateControl, bConnect, overList);
        }

        static public void SetConnectToRoot(StateControl ctrl, bool bConnect, ArrayList overList)
        {
            if (ctrl==null || overList.Contains(ctrl))
            {
                return;
            }

            ctrl.ConnectToRoot = bConnect;
            overList.Add(ctrl);
            foreach (TranslationControl trans in ctrl.TranslationCtrlList)
            {
                if (trans.StartStateControl == ctrl)
                {
                    SetConnectToRoot(trans, bConnect, overList);
                }

            }
        }

        public void RemoveStateDiagram(String strDiaName)
        {
            if (m_Name2Diagram.ContainsKey(strDiaName))
            {
                m_Name2Diagram.Remove(strDiaName);
            }
        }

        private Hashtable m_Name2Diagram = new Hashtable();     // 状态图名称 - 状态图 map
        private static StateDiagramManager ms_Instance = null;
    }
}
