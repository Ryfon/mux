﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

// 状态图操作管理器.处理状态图的 选择/复制/粘贴/剪切 等操作
namespace MissionEditor
{
    public class StateDiagramOperationManager
    {

        // 添加一个选择控件
        public void AddSelect(StateDiagram dia, UserControl ctrl)
        {
            // 如果新选择的控件所在状态图与之前的不等,清除选择列表
            if (dia != m_TargetStateDiagram)
            {
                ClearSelectionList();
                m_TargetStateDiagram = dia;
            }

            if (!m_SelectionList.Contains(ctrl))
            {
                m_SelectionList.Add(ctrl);
                if (ctrl is StateControl)
                {
                    (ctrl as StateControl).Selected = true;
                }
                else if (ctrl is TranslationControl)
                {
                    (ctrl as TranslationControl).Selected = true;
                }
            }
        }

        // 设置一个选择控件
        public void SetSelect(StateDiagram dia, UserControl ctrl)
        {
            ClearSelectionList();
            AddSelect(dia, ctrl);
        }

        // 
        public bool Copy()
        {
            m_CopyList.Clear();
            foreach (UserControl ctrl in m_SelectionList)
            {
                m_CopyList.Add(ctrl);
            }
            return true;
        }

        // dia - 向哪个状态图粘贴,即粘贴的目标
        public bool Paste(StateDiagram dia)
        {
            foreach (UserControl ctrl in m_CopyList)
            {
                if (ctrl is StateControl)
                {
                    State oldState = m_TargetStateDiagram.GetStateByID((ctrl as StateControl).StateID);
                    State newState = oldState.Clone();
                    newState.ID = dia.GetMaxStateID() + 1;
                    Point location = newState.Location;
                    location.X = location.X + 5;
                    location.Y = location.Y + 5;
                    newState.Location = location;
                    dia.StateList.Add(newState);
                    MainForm.Instance.Canvas.Controls.Add(new StateControl(newState));
                }
                else if(ctrl is TranslationControl)
                {
                    //Translation trans = new Translation(ctrl as TranslationControl);
                    //MainForm.Instance.Canvas.Controls.Add(new TranslationControl(trans));
                }
               
            }
            MainForm.Instance.StateDiagramChanged = true;

            return true;
        }

        // 清空选择列表
        public void ClearSelectionList()
        {
            foreach (UserControl ctrl in m_SelectionList)
            {
                if (ctrl is StateControl)
                {
                    (ctrl as StateControl).Selected = false;
                }
                else if(ctrl is TranslationControl)
                {
                    (ctrl as TranslationControl).Selected = false;
                }
                ctrl.Invalidate();
            }
            m_SelectionList.Clear();
        }

        // 清空拷贝列表
        public void ClearCopyList()
        {
            m_CopyList.Clear();
        }

        public static StateDiagramOperationManager Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new StateDiagramOperationManager();
                }
                return m_Instance;
            }
        }

        private static StateDiagramOperationManager m_Instance = null;
        StateDiagram m_TargetStateDiagram = null;  // 当前保存的控件所属的状态图
        ArrayList m_SelectionList = new ArrayList();
        ArrayList m_CopyList = new ArrayList();

    }
}
