﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Collections;

namespace MissionEditor
{
    public class MissionStatus
    {
        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }
        public String Msg
        {
            get
            {
                return m_strMsg;
            }
            set
            {
                m_strMsg = value;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // 返回值的含义
        // -1表示有错误发生
        // 0表示没有找到任务
        // 1表示任务为正常状态
        // 2表示任务为签出状态
        //////////////////////////////////////////////////////////////////////////
        int m_iID = -1;
        String m_strMsg = ""; // 附带信息
    };

    public class DiagramStatus : MissionStatus
    {

    };

    public class MissionLog
    {
        public MissionLog()
        {
            iLogID = 0;

            oldMission = null;
            oldStatus = null;

            strPerson = "";
            strOperation = "";

            newMission = null;
            newStatus = null;
        }

        public int iLogID;

        public Mission oldMission;
        public MissionStatus oldStatus;

        public String strPerson;
        public DateTime timeNow;
        public String strOperation;

        public Mission newMission;
        public MissionStatus newStatus;
    }

    public class TeamWorkManager
    {
        //--------------------------------------------------------------------------------
        // 对外接口
        #region 任务方面的接口
        public Mission GetLatestVersion(int iMissionID)
        {
            Mission latestMission = new Mission();

            // 获取文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\task.xml");
            if (stream == null)
            {
                UnlockFile(stream);
                return null;
            }

            // 解析文件
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                return null;
            }

            // 根据任务ID，寻找对应的Mission
            for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
            {
                if (xmlMission.GetAttribute("ID").Equals(iMissionID.ToString()))
                {
                    latestMission.LoadFromXmlNode(xmlMission);
                    latestMission.TeamWorkStatus = GetMissionStatus(iMissionID).ID;
                    UnlockFile(stream);
                    return latestMission;
                }
            }

            UnlockFile(stream);
            return null;
        }

        public ArrayList GetLatestMissionList()
        {
            ArrayList missionList = new ArrayList();

            // 获取文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\task.xml");
            if (stream == null)
            {
                UnlockFile(stream);
                return null;
            }

            // 解析文件
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                return null;
            }

            // 根据任务ID，寻找对应的Mission
            for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
            {
                Mission latestMission = new Mission();
                latestMission.LoadFromXmlNode(xmlMission);
                latestMission.TeamWorkStatus = GetMissionStatus(latestMission.ID).ID;
                missionList.Add(latestMission);
            }

            UnlockFile(stream);
            return missionList;
        }

        public int CheckOut(int iMissionID)
        {
            MissionLog log = new MissionLog();
            log.iLogID = iMissionID;
            log.strOperation = "CheckOut";
            log.strPerson = System.Net.Dns.GetHostName();
            log.timeNow = System.DateTime.Now;
            log.oldMission = GetLatestVersion(log.iLogID);
            log.oldStatus = GetMissionStatus(log.iLogID);

            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\taskStatus.xml");

            // 获取文件状态
            MissionStatus status = GetMissionStatus(stream, iMissionID);

            // 判断文件状态
            if (status.ID == 1)
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception)
                {
                    UnlockFile(stream);
                    AddLog(log);
                    return -1;
                }

                // 找到并修改要修改的状态
                for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
                {
                    if (xmlMission.GetAttribute("ID").Equals(iMissionID.ToString()))
                    {
                        xmlMission.SetAttribute("Status", "2");
                        xmlMission.SetAttribute("LastChangeUser", System.Net.Dns.GetHostName());
                    }
                }

                // 写入文件
                stream.SetLength(0);
                doc.Save(stream);
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            log.newMission = GetLatestVersion(log.iLogID);
            log.newStatus = GetMissionStatus(log.iLogID);
            AddLog(log);
            return status.ID;
        }

        public int Repeal(int iMissionID)
        {
            // 旧的任务信息
            MissionLog log = new MissionLog();
            log.iLogID = iMissionID;
            log.strOperation = "Repeal";
            log.strPerson = System.Net.Dns.GetHostName();
            log.timeNow = System.DateTime.Now;
            log.oldMission = GetLatestVersion(log.iLogID);
            log.oldStatus = GetMissionStatus(log.iLogID);

            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\taskStatus.xml");

            // 获取文件状态
            MissionStatus status = GetMissionStatus(stream, iMissionID);

            // 判断文件状态
            if (status.ID == 2 && status.Msg.Equals(System.Net.Dns.GetHostName()))
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception)
                {
                    UnlockFile(stream);
                    AddLog(log);
                    return -1;
                }

                // 找到并修改要修改的状态
                for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
                {
                    if (xmlMission.GetAttribute("ID").Equals(iMissionID.ToString()))
                    {
                        xmlMission.SetAttribute("Status", "1");
                        xmlMission.SetAttribute("LastChangeUser", System.Net.Dns.GetHostName());
                    }
                }

                // 写入文件
                stream.SetLength(0);
                doc.Save(stream);
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            log.newMission = GetLatestVersion(log.iLogID);
            log.newStatus = GetMissionStatus(log.iLogID);
            AddLog(log);
            return status.ID;
        }

        public int CheckIn(Mission mission)
        {
            // 旧的任务信息
            MissionLog log = new MissionLog();
            log.iLogID = mission.ID;
            log.strOperation = "CheckIn";
            log.strPerson = System.Net.Dns.GetHostName();
            log.timeNow = System.DateTime.Now;
            log.oldMission = GetLatestVersion(log.iLogID);
            log.oldStatus = GetMissionStatus(log.iLogID);

            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\taskStatus.xml");

            // 获取文件状态
            MissionStatus status = GetMissionStatus(stream, mission.ID);

            // 判断文件状态
            if (status.ID == 2 && status.Msg.Equals(System.Net.Dns.GetHostName()))
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception)
                {
                    UnlockFile(stream);
                    AddLog(log);
                    return -1;
                }

                // 更新任务，出错则返回
                if (UpdateMission(mission) == -1)
                {
                    UnlockFile(stream);
                    AddLog(log);
                    return -1;
                }

                // 找到并修改要修改的状态
                for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
                {
                    if (xmlMission.GetAttribute("ID").Equals(mission.ID.ToString()))
                    {
                        xmlMission.SetAttribute("Status", "1");
                        xmlMission.SetAttribute("LastChangeUser", System.Net.Dns.GetHostName());
                    }
                }

                // 写入文件
                stream.SetLength(0);
                doc.Save(stream);
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            log.newMission = GetLatestVersion(log.iLogID);
            log.newStatus = GetMissionStatus(log.iLogID);
            AddLog(log);
            return status.ID;

        }

        public int Import(Mission mission)
        {
            MissionLog log = new MissionLog();
            log.iLogID = mission.ID;
            log.strOperation = "Import";
            log.strPerson = System.Net.Dns.GetHostName();
            log.timeNow = System.DateTime.Now;
            
            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\taskStatus.xml");

            // 获取文件状态
            MissionStatus status = GetMissionStatus(stream, mission.ID);

            // 判断文件状态
            if (status.ID == 0)
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    UnlockFile(stream);
                    AddLog(log);
                    return -1;
                }

                // 插入任务，出错则返回
                if (ImportMission(mission) == -1)
                {
                    UnlockFile(stream);
                    AddLog(log);
                    return -1;
                }

                // 新建一条任务记录
                XmlElement xmlMission = doc.CreateElement("Task");
                xmlMission.SetAttribute("ID", mission.ID.ToString());
                xmlMission.SetAttribute("Status", "1");
                xmlMission.SetAttribute("LastChangeUser", System.Net.Dns.GetHostName());
                root.AppendChild(xmlMission);

                // 写入文件
                stream.SetLength(0);
                doc.Save(stream);
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            log.newMission = GetLatestVersion(log.iLogID);
            log.newStatus = GetMissionStatus(log.iLogID);
            AddLog(log);
            return status.ID;
        }

        public int Remove(int iMissionID)
        {
            // 旧的任务信息
            MissionLog log = new MissionLog();
            log.iLogID = iMissionID;
            log.strOperation = "Remove";
            log.strPerson = System.Net.Dns.GetHostName();
            log.timeNow = System.DateTime.Now;
            log.oldMission = GetLatestVersion(log.iLogID);
            log.oldStatus = GetMissionStatus(log.iLogID);

            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\taskStatus.xml");

            // 获取文件状态
            MissionStatus status = GetMissionStatus(stream, iMissionID);

            // 判断文件状态
            if (status.ID == 2 && status.Msg.Equals(System.Net.Dns.GetHostName()))
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception)
                {
                    UnlockFile(stream);
                    AddLog(log);
                    return -1;
                }

                // 删除任务，出错则返回
                if (RemoveMission(iMissionID) == -1)
                {
                    UnlockFile(stream);
                    AddLog(log);
                    return -1;
                }

                // 删除一条任务记录
                for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
                {
                    if (xmlMission.GetAttribute("ID").Equals(iMissionID.ToString()))
                    {
                        root.RemoveChild(xmlMission);
                        // 写入文件
                        stream.SetLength(0);
                        doc.Save(stream);
                        // 解锁文件，返回状态
                        UnlockFile(stream);
                        return status.ID;
                    }
                }

                // 执行到这里，说明没有找到
                return -1;
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            AddLog(log);
            return status.ID;
        }


        private int UpdateMission(Mission mission)
        {
            // 任务为null，直接返回-1，说明出错
            if (mission == null)
            {
                return -1;
            }

            // 开始锁住文件，进行更新
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\task.xml");

            // 解析文件
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                return -1;
            }

            // 找到并修改要修改的状态
            for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
            {
                if (xmlMission.GetAttribute("ID").Equals(mission.ID.ToString()))
                {
                    xmlMission.RemoveAll(); // 清空旧的数据

                    mission.SaveToXmlNode(doc, xmlMission);    // 写入新的数据

                    // 写入文件
                    stream.SetLength(0);
                    doc.Save(stream);
                    UnlockFile(stream);
                    return 0;   // 结果正确的话返回0
                }
            }

            UnlockFile(stream);
            return -1; // 执行到这里的话，说明没有找到对应的任务，返回-1
        }

        private int ImportMission(Mission mission)
        {
            // 任务为null，直接返回-1，说明出错
            if (mission == null)
            {
                return -1;
            }

            // 开始锁住文件，进行更新
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\task.xml");

            // 解析文件
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                return -1;
            }

            // 创建一个新的任务节点
            XmlElement xmlMission = doc.CreateElement("Task");
            mission.SaveToXmlNode(doc, xmlMission);    // 写入新的数据
            root.AppendChild(xmlMission);

            // 写入文件
            stream.SetLength(0);
            doc.Save(stream);
            UnlockFile(stream);
            return 0;   // 结果正确的话返回0     
        }

        private int RemoveMission(int iMissionID)
        {
            // 开始锁住文件，进行更新
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\task.xml");

            // 解析文件
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                return -1;
            }

            // 找到并修改要修改的状态
            for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
            {
                if (xmlMission.GetAttribute("ID").Equals(iMissionID.ToString()))
                {
                    // 移除该节点
                    root.RemoveChild(xmlMission);

                    // 写入文件
                    stream.SetLength(0);
                    doc.Save(stream);
                    UnlockFile(stream);
                    return 0;   // 结果正确的话返回0
                }
            }

            UnlockFile(stream);
            return -1; // 执行到这里的话，说明没有找到对应的任务，返回-1
        }

        private MissionStatus GetMissionStatus(FileStream stream, int iMissionID)
        {
            // 定义变量
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            MissionStatus status = new MissionStatus();

            // 成功打开文件后，对xml进行解析
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                status.ID = -1;
                status.Msg = "taskStatus.xml文件错误，请检查其存放路径或格式是否正确";
                return status;
            }

            // 根据任务ID，寻找对应的Mission的状态
            status.ID = 0;
            for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
            {
                if (xmlMission.GetAttribute("ID").Equals(iMissionID.ToString()))
                {
                    status.ID = int.Parse(xmlMission.GetAttribute("Status"));
                    status.Msg = xmlMission.GetAttribute("LastChangeUser");

                    // 判断签出者
                    if (status.ID == 2 && !status.Msg.Equals(System.Net.Dns.GetHostName()))
                    {
                        status.ID = 3;
                    }
                }
            }

            return status;
        }

        public MissionStatus GetMissionStatus(int iMissionID)
        {
            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\taskStatus.xml");

            // 定义变量
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            MissionStatus status = new MissionStatus();

            // 成功打开文件后，对xml进行解析
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                status.ID = -1;
                status.Msg = "taskStatus.xml文件错误，请检查其存放路径或格式是否正确";
                return status;
            }

            // 根据任务ID，寻找对应的Mission的状态
            status.ID = 0;
            for (XmlElement xmlMission = root.FirstChild as XmlElement; xmlMission != null; xmlMission = xmlMission.NextSibling as XmlElement)
            {
                if (xmlMission.GetAttribute("ID").Equals(iMissionID.ToString()))
                {
                    status.ID = int.Parse(xmlMission.GetAttribute("Status"));
                    status.Msg = xmlMission.GetAttribute("LastChangeUser");

                    // 判断签出者
                    if (status.ID == 2 && !status.Msg.Equals(System.Net.Dns.GetHostName()))
                    {
                        status.ID = 3;
                    }
                }
            }

            UnlockFile(stream);
            return status;
        }
        #endregion

        #region 文件锁
        public FileStream LockFile(String strFileName)
        {
            // 定义stream
            FileStream stream = null;

            // 尝试打开文件
            try
            {
                stream = System.IO.File.Open(strFileName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);
            }
            catch (Exception)
            {
                return null;
            }

            return stream;
        }

        public void UnlockFile(FileStream stream)
        {
            if (stream != null)
            {
                stream.Flush();
                stream.Close();
                stream.Dispose();
                stream = null;
            }
        }
        #endregion

        #region 状态图方面的接口
        public StateDiagram GetLatestVersion(String strDiaName)
        {
            // 获取文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\StateDiagram\\" + strDiaName + ".xml");
            if (stream == null)
            {
                UnlockFile(stream);
                return null;
            }

            // 解析文件
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                return null;
            }

            // 用xml节点来填充dia
            StateDiagram latestDia = new StateDiagram(root);

            UnlockFile(stream);
            return latestDia;
        }

        public ArrayList GetLatestDiagramList()
        {
            ArrayList DiagramList = new ArrayList();

            String[] fileList = System.IO.Directory.GetFileSystemEntries(OptionManager.TeamWorkServerPath + "\\StateDiagram\\");
            foreach (String strFilePath in fileList)
            {
                if (System.IO.Path.GetExtension(strFilePath).Equals(".xml"))
                {
                    String fileName = System.IO.Path.GetFileNameWithoutExtension(strFilePath);

                    // 获取文件流
                    FileStream stream = LockFile(strFilePath);
                    if (stream == null)
                    {
                        UnlockFile(stream);
                        return null;
                    }

                    // 解析文件
                    XmlDocument doc = new XmlDocument();
                    XmlElement root = null;
                    try
                    {
                        doc.Load(stream);
                        root = doc.LastChild as XmlElement;
                    }
                    catch (Exception)
                    {
                        UnlockFile(stream);
                        return null;
                    }

                    // 用xml节点来填充dia
                    StateDiagram latestDia = new StateDiagram(root);
                    UnlockFile(stream);

                    // 放入列表
                    DiagramList.Add(latestDia);
                }
            }
            
            return DiagramList;
        }

        public int CheckOut(String strDiaName)
        {
            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\DiagramStatus.xml");

            // 获取文件状态
            DiagramStatus status = GetDiagramStatus(stream, strDiaName);

            // 判断文件状态
            if (status.ID == 1)
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception)
                {
                    UnlockFile(stream);
                    return -1;
                }

                // 找到并修改要修改的状态
                for (XmlElement xmlDiagram = root.FirstChild as XmlElement; xmlDiagram != null; xmlDiagram = xmlDiagram.NextSibling as XmlElement)
                {
                    if (xmlDiagram.GetAttribute("Name").Equals(strDiaName))
                    {
                        xmlDiagram.SetAttribute("Status", "2");
                        xmlDiagram.SetAttribute("LastChangeUser", System.Net.Dns.GetHostName());
                    }
                }

                // 写入文件
                stream.SetLength(0);
                doc.Save(stream);
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            return status.ID;
        }

        public int Repeal(String strDiaName)
        {
            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\DiagramStatus.xml");

            // 获取文件状态
            DiagramStatus status = GetDiagramStatus(stream, strDiaName);

            // 判断文件状态
            if (status.ID == 2 && status.Msg.Equals(System.Net.Dns.GetHostName()))
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception)
                {
                    UnlockFile(stream);
                    return -1;
                }

                // 找到并修改要修改的状态
                for (XmlElement xmlDiagram = root.FirstChild as XmlElement; xmlDiagram != null; xmlDiagram = xmlDiagram.NextSibling as XmlElement)
                {
                    if (xmlDiagram.GetAttribute("Name").Equals(strDiaName))
                    {
                        xmlDiagram.SetAttribute("Status", "1");
                        xmlDiagram.SetAttribute("LastChangeUser", System.Net.Dns.GetHostName());
                    }
                }

                // 写入文件
                stream.SetLength(0);
                doc.Save(stream);
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            return status.ID;
        }

        public int CheckIn(StateDiagram diagram)
        {
            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\DiagramStatus.xml");

            // 获取文件状态
            DiagramStatus status = GetDiagramStatus(stream, diagram.Name);

            // 判断文件状态
            if (status.ID == 2 && status.Msg.Equals(System.Net.Dns.GetHostName()))
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception)
                {
                    UnlockFile(stream);
                    return -1;
                }

                // 更新状态图，出错则返回
                if (UpdateDiagram(diagram) == -1)
                {
                    UnlockFile(stream);
                    return -1;
                }

                // 找到并修改要修改的状态
                for (XmlElement xmlDiagram = root.FirstChild as XmlElement; xmlDiagram != null; xmlDiagram = xmlDiagram.NextSibling as XmlElement)
                {
                    if (xmlDiagram.GetAttribute("Name").Equals(diagram.Name))
                    {
                        xmlDiagram.SetAttribute("Status", "1");
                        xmlDiagram.SetAttribute("LastChangeUser", System.Net.Dns.GetHostName());
                    }
                }

                // 写入文件
                stream.SetLength(0);
                doc.Save(stream);
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            return status.ID;

        }

        public int Import(StateDiagram diagram)
        {
            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\DiagramStatus.xml");

            // 获取文件状态
            DiagramStatus status = GetDiagramStatus(stream, diagram.Name);

            // 判断文件状态
            if (status.ID == 0)
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    UnlockFile(stream);
                    return -1;
                }

                // 插入状态图，出错则返回
                if (ImportDiagram(diagram) == -1)
                {
                    UnlockFile(stream);
                    return -1;
                }

                // 新建一条任务记录
                XmlElement xmlMission = doc.CreateElement("StateDiagram");
                xmlMission.SetAttribute("Name", diagram.Name);
                xmlMission.SetAttribute("Status", "1");
                xmlMission.SetAttribute("LastChangeUser", System.Net.Dns.GetHostName());
                root.AppendChild(xmlMission);

                // 写入文件
                stream.SetLength(0);
                doc.Save(stream);
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            return status.ID;
        }

        public int Remove(String strDiaName)
        {
            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\DiagramStatus.xml");

            // 获取文件状态
            DiagramStatus status = GetDiagramStatus(stream, strDiaName);

            // 判断文件状态
            if (status.ID == 2 && status.Msg.Equals(System.Net.Dns.GetHostName()))
            {
                // 解析文件
                stream.Position = 0;
                XmlDocument doc = new XmlDocument();
                XmlElement root = null;
                try
                {
                    doc.Load(stream);
                    root = doc.LastChild as XmlElement;
                }
                catch (Exception)
                {
                    UnlockFile(stream);
                    return -1;
                }

                // 删除状态图，出错则返回
                if (RemoveDiagram(strDiaName) == -1)
                {
                    UnlockFile(stream);
                    return -1;
                }

                // 删除一条任务记录
                for (XmlElement xmlDiagram = root.FirstChild as XmlElement; xmlDiagram != null; xmlDiagram = xmlDiagram.NextSibling as XmlElement)
                {
                    if (xmlDiagram.GetAttribute("Name").Equals(strDiaName))
                    {
                        root.RemoveChild(xmlDiagram);
                        // 写入文件
                        stream.SetLength(0);
                        doc.Save(stream);
                        // 解锁文件，返回状态
                        UnlockFile(stream);
                        return status.ID;
                    }
                }

                // 执行到这里，说明没有找到
                return -1;
            }

            // 解锁文件，返回状态
            UnlockFile(stream);
            return status.ID;
        }


        private int UpdateDiagram(StateDiagram diagram)
        {
            // 为null，直接返回-1，说明出错
            if (diagram == null)
            {
                return -1;
            }

            // 开始锁住文件，进行更新
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\StateDiagram\\" + diagram.Name + ".xml");
            if(stream == null)
            {
                return -1;
            }

            // 修改内容
            XmlDocument doc = new XmlDocument();
            XmlElement root = diagram.GetAsXmlElement(doc);
            doc.AppendChild(root);

            // 写入文件
            stream.SetLength(0);
            doc.Save(stream);
            UnlockFile(stream);
            return 0;   // 结果正确的话返回0
        }

        private int ImportDiagram(StateDiagram diagram)
        {
            // 为null，直接返回-1，说明出错
            if (diagram == null)
            {
                return -1;
            }

            // 开始锁住文件，进行更新
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\StateDiagram\\" + diagram.Name + ".xml");
            if (stream == null)
            {
                return -1;
            }

            // 修改内容
            XmlDocument doc = new XmlDocument();
            XmlElement root = diagram.GetAsXmlElement(doc);
            doc.AppendChild(root);

            // 写入文件
            stream.SetLength(0);
            doc.Save(stream);
            UnlockFile(stream);
            return 0;   // 结果正确的话返回0    
        }

        private int RemoveDiagram(String strDiaName)
        {
            // 尝试删除文件
            try
            {
                File.Delete(OptionManager.TeamWorkServerPath + "\\StateDiagram\\" + strDiaName + ".xml");
            }
            catch (Exception)
            {
                return -1;
            }

            // 没有异常的话，返回0表示正确
            return 0;
        }

        public DiagramStatus GetDiagramStatus(String strDiaName)
        {
            // 锁住文件，得到文件流
            FileStream stream = LockFile(OptionManager.TeamWorkServerPath + "\\DiagramStatus.xml");

            // 定义变量
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            DiagramStatus status = new DiagramStatus();

            // 成功打开文件后，对xml进行解析
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                status.ID = -1;
                status.Msg = "DiagramStatus.xml文件错误，请检查其存放路径或格式是否正确";
                return status;
            }

            // 根据任务ID，寻找对应的Mission的状态
            status.ID = 0;
            for (XmlElement xmlDiagram = root.FirstChild as XmlElement; xmlDiagram != null; xmlDiagram = xmlDiagram.NextSibling as XmlElement)
            {
                if (xmlDiagram.GetAttribute("Name").Equals(strDiaName))
                {
                    status.ID = int.Parse(xmlDiagram.GetAttribute("Status"));
                    status.Msg = xmlDiagram.GetAttribute("LastChangeUser");

                    // 判断签出者
                    if (status.ID == 2 && !status.Msg.Equals(System.Net.Dns.GetHostName()))
                    {
                        status.ID = 3;
                    }
                }
            }

            UnlockFile(stream);
            return status;
        }

        private DiagramStatus GetDiagramStatus(FileStream stream, String strDiaName)
        {
            // 定义变量
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            DiagramStatus status = new DiagramStatus();

            // 成功打开文件后，对xml进行解析
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                status.ID = -1;
                status.Msg = "DiagramStatus.xml文件错误，请检查其存放路径或格式是否正确";
                return status;
            }

            // 根据任务ID，寻找对应的Mission的状态
            status.ID = 0;
            for (XmlElement xmlDiagram = root.FirstChild as XmlElement; xmlDiagram != null; xmlDiagram = xmlDiagram.NextSibling as XmlElement)
            {
                if (xmlDiagram.GetAttribute("Name").Equals(strDiaName))
                {
                    status.ID = int.Parse(xmlDiagram.GetAttribute("Status"));
                    status.Msg = xmlDiagram.GetAttribute("LastChangeUser");

                    // 判断签出者
                    if (status.ID == 2 && !status.Msg.Equals(System.Net.Dns.GetHostName()))
                    {
                        status.ID = 3;
                    }
                }
            }

            return status;
        }

        #endregion

        #region 任务修改的日志文件
        private bool CreateLog(String directory, String fileName)
        {
            try
            {
                // 完整的文件路径
                String strFullPath = directory + fileName;

                // 处理文件夹
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                // 处理文件
                if (!File.Exists(strFullPath))
                {
                    // 创建xml文件
                    XmlDocument xmlDoc = new XmlDocument();
                    XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", null);
                    xmlDoc.AppendChild(xmlDeclaration);

                    XmlElement root = xmlDoc.CreateElement("Logs");
                    xmlDoc.AppendChild(root);

                    xmlDoc.Save(strFullPath);
                }

                // 正确返回
                return true;
            }
            catch(Exception)
            {
                // 出错
                return false;  
            }      
        }

        private bool SetLog(XmlDocument doc, XmlElement node, MissionLog log)
        {
            // 旧的任务
            XmlElement oldMissionNode = doc.CreateElement("OldMission");
            if(log.oldMission != null)
            {
                log.oldMission.SaveToXmlNode(doc, oldMissionNode);
            }
            node.AppendChild(oldMissionNode);

            // 旧的状态
            XmlElement oldStatusNode = doc.CreateElement("OldStatus");
            if(log.oldStatus != null)
            {
                oldStatusNode.SetAttribute("ID", log.oldStatus.ID.ToString());
                oldStatusNode.SetAttribute("Msg", log.oldStatus.Msg);
            }
            node.AppendChild(oldStatusNode);

            // 操作
            XmlElement OpNode = doc.CreateElement("Operation");
            String strOpTime = log.timeNow.ToString("G");
            OpNode.SetAttribute("Time", strOpTime);
            OpNode.SetAttribute("Person", log.strPerson);
            OpNode.SetAttribute("Value", log.strOperation);
            node.AppendChild(OpNode);

            // 新的任务
            XmlElement newMissionNode = doc.CreateElement("NewMission");
            if (log.newMission != null)
            {
                log.newMission.SaveToXmlNode(doc, newMissionNode);
            }
            node.AppendChild(newMissionNode);

            // 新的状态
            XmlElement newStatusNode = doc.CreateElement("NewStatus");
            if (log.newStatus != null)
            {
                newStatusNode.SetAttribute("ID", log.newStatus.ID.ToString());
                newStatusNode.SetAttribute("Msg", log.newStatus.Msg);
            }
            node.AppendChild(newStatusNode);


            return true;
        }

        private bool AddLog(MissionLog log)
        {
            // 创建文件
            String strPath = OptionManager.TeamWorkServerPath + "\\TaskLogs\\";
            String strFileName = log.iLogID.ToString() + ".xml";
            String strFullPath = strPath + strFileName;
            if(!CreateLog(strPath, strFileName))
            {
                MessageBox.Show("创建日志文件出错" + strFullPath);
                return false;
            }

            // 锁住日志文件
            FileStream stream = LockFile(strFullPath);

            // 读取日志文件
            XmlDocument doc = new XmlDocument();
            XmlElement root = null;
            try
            {
                doc.Load(stream);
                root = doc.LastChild as XmlElement;
            }
            catch (Exception)
            {
                UnlockFile(stream);
                return false;
            }

            // 根节点
            if(root == null)
            {
                UnlockFile(stream);
                return false; 
            }

            // 创建新的log节点
            XmlElement newLog = doc.CreateElement("Log");
            if (SetLog(doc, newLog, log))
            {
                root.AppendChild(newLog);
            }
            else
            {
                UnlockFile(stream);
                return false;  
            }
            

            // 写入文件
            stream.SetLength(0);
            doc.Save(stream);

            // 解锁后返回
            UnlockFile(stream);
            return true;
        }
        #endregion
        //--------------------------------------------------------------------------------
        // 属性
        public static TeamWorkManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new TeamWorkManager();
                }
                return ms_Instance;
            }
        }

        //--------------------------------------------------------------------------------
        // 私有成员
        private static TeamWorkManager ms_Instance = null;
        //--------------------------------------------------------------------------------
    }
}
