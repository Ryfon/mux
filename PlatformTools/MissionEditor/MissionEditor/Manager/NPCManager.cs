﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace MissionEditor
{
    public class NPCInfo
    {
        // 公有属性
        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        public String Name
        {
            get
            {
                return m_strName;
            }
            set
            {
                m_strName = value;
            }
        }

        public String Desc
        {
            get
            {
                return m_strDesc;
            }
            set
            {
                m_strDesc = value;
            }
        }

        public int Num
        {
            get { return m_iNum; }
            set { m_iNum = value; }
        }

        public int DialogScriptID
        {
            get
            {
                return m_iDlgScriptID;
            }
            set
            {
                m_iDlgScriptID = value;
            }
        }

        public int DeathScriptID
        {
            get
            {
                return m_iDeathScriptID;
            }
            set
            {
                m_iDeathScriptID = value;
            }
        }

        public int IsAggressive
        {
            get
            {
                return m_iIsAggressive;
            }
            set
            {
                m_iIsAggressive = value;
            }
        }
        //-------------------------------------------------------------------------

        // 私有成员
        private int m_iID;          // NPC 的唯一编号
        private String m_strName;   // 名称
        private String m_strDesc;   // 描述
        private int m_iNum;         // 数量
        private int m_iDlgScriptID; // 对话脚本 ID
        private int m_iDeathScriptID;// 死亡脚本 ID
        private int m_iIsAggressive = 1;    // 是否为主动攻击怪物
        //--------------------------------------------------------------------------
    };

    public class NPCCreatorInfo
    {
        public enum SupplyModeType
        {
            永不补充 = 1,
            逐个补充 = 2,
            整批补充 = 3,
            固定时间间隔补充 = 4,
            //固定时间补充 = 1,
            //整批补充,
        }

        public enum TimeMethodType
        {
            一直不断开启 = 1,
            固定时间段中开启,
        }

        // 公有属性
        [ReadOnlyAttribute(true), CategoryAttribute("基本信息"), DescriptionAttribute("自动生成的编号")]
        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        [CategoryAttribute("基本信息"), DescriptionAttribute("生成器名称")]
        public String Name
        {
            get
            {
                return m_strName;
            }
            set
            {
                m_strName = value;
            }
        }

        [ReadOnlyAttribute(true), CategoryAttribute("基本信息"), DescriptionAttribute("生成器所在的地图编号")]
        public int MapID
        {
            get
            {
                return m_iMapID;
            }
            set
            {
                m_iMapID = value;
            }
        }

        [CategoryAttribute("基本信息"), DescriptionAttribute("在初始化时是否激活该生成器\r\n0：不激活\r\n1：激活")]
        public int Active
        {
            get
            {
                return m_iActive;
            }
            set
            {
                m_iActive = value;
            }
        }

        [CategoryAttribute("基本信息"), DescriptionAttribute("与场景编辑器中的ID对应，用于实现物件NPC机制")]
        public int ExtraID
        {
            get
            {
                return m_iExtraID;
            }
            set
            {
                m_iExtraID = value;
            }
        }

        [CategoryAttribute("基本信息"), DescriptionAttribute("是否为物件NPC")]
        public bool Isitemnpc
        {
            get
            {
                return m_bIsitemnpc;
            }
            set
            {
                m_bIsitemnpc = value;
            }
        }

        [CategoryAttribute("基本信息"), DescriptionAttribute("物件NPC序号")]
        public int Itemnpcseq
        {
            get
            {
                return m_iItemnpcseq;
            }
            set
            {
                m_iItemnpcseq = value;
            }
        }

        [CategoryAttribute("位置朝向信息"), DescriptionAttribute("生成器位置范围")]
        public Rectangle Rectangle
        {
            get
            {
                return m_Rect;
            }
            set
            {
                m_Rect = value;
            }
        }

        [CategoryAttribute("位置朝向信息"), DescriptionAttribute("0: 随机朝向\r\n45~360度: 规定的八个角度朝向（45的倍数）")]
        public int Direction
        {
            get
            {
                return m_iDirection;
            }
            set
            {
                m_iDirection = value;
            }
        }
        /*
        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC0
        {
            get
            {
                if (m_NPC2Num.Keys.Count < 1)
                {
                    return -1;
                }

                IDictionaryEnumerator enumerator = m_NPC2Num.GetEnumerator();
                enumerator.MoveNext();
                return (int)(enumerator.Key);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC0Num
        {
            get
            {
                if (NPC0 == -1)
                {
                    return 0;
                }
                else
                {
                    int num = (int)(m_NPC2Num[NPC0]);
                    return num;
                }
            }
            set
            {
                m_NPC2Num[NPC0] = value;
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC1
        {
            get
            {
                if (m_NPC2Num.Keys.Count < 2)
                {
                    return -1;
                }

                IDictionaryEnumerator enumerator = m_NPC2Num.GetEnumerator();
                enumerator.MoveNext();
                enumerator.MoveNext();
                return (int)(enumerator.Key);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC1Num
        {
            get
            {
                if (NPC1 == -1)
                {
                    return 0;
                }
                else
                {
                    int num = (int)(m_NPC2Num[NPC1]);
                    return num;
                }
            }
            set
            {
                m_NPC2Num[NPC1] = value;
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC2
        {
            get
            {
                if (m_NPC2Num.Keys.Count < 3)
                {
                    return -1;
                }

                IDictionaryEnumerator enumerator = m_NPC2Num.GetEnumerator();
                enumerator.MoveNext();
                enumerator.MoveNext();
                enumerator.MoveNext();
                return (int)(enumerator.Key);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC2Num
        {
            get
            {
                if (NPC2 == -1)
                {
                    return 0;
                }
                else
                {
                    int num = (int)(m_NPC2Num[NPC2]);
                    return num;
                }
            }
            set
            {
                m_NPC2Num[NPC2] = value;
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC3
        {
            get
            {
                if (m_NPC2Num.Keys.Count < 4)
                {
                    return -1;
                }

                IDictionaryEnumerator enumerator = m_NPC2Num.GetEnumerator();
                enumerator.MoveNext();
                enumerator.MoveNext();
                enumerator.MoveNext();
                enumerator.MoveNext();
                return (int)(enumerator.Key);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC3Num
        {
            get
            {
                if (NPC3 == -1)
                {
                    return 0;
                }
                else
                {
                    int num = (int)(m_NPC2Num[NPC3]);
                    return num;
                }
            }
            set
            {
                m_NPC2Num[NPC3] = value;
            }
        }
        */
        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC0
        {
            get
            {
                return (int)m_NPC2ID[0];
            }
            set
            {
                //int iOldID = NPC0;
                //int iNewID = value;

                NPCID2ID[0] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }
        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC0Num
        {
            get
            {
                return (int)m_NPC2Num[0];
            }
            set
            {
                //int iOldID = NPC0;
                //int iNewID = NPC0;

                m_NPC2Num[0] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC1
        {
            get
            {
                return (int)m_NPC2ID[1];
            }
            set
            {
                //int iOldID = NPC1;
                //int iNewID = value;

                NPCID2ID[1] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }
        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC1Num
        {
            get
            {
                return (int)m_NPC2Num[1];
            }
            set
            {
                //int iOldID = NPC1;
                //int iNewID = NPC1;

                m_NPC2Num[1] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC2
        {
            get
            {
                return (int)m_NPC2ID[2];
            }
            set
            {
                //int iOldID = NPC2;
                //int iNewID = value;

                NPCID2ID[2] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }
        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC2Num
        {
            get
            {
                return (int)m_NPC2Num[2];
            }
            set
            {
                //int iOldID = NPC2;
                //int iNewID = NPC2;

                m_NPC2Num[2] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC3
        {
            get
            {
                return (int)m_NPC2ID[3];
            }
            set
            {
                //int iOldID = NPC3;
                //int iNewID = value;

                NPCID2ID[3] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }
        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC3Num
        {
            get
            {
                return (int)m_NPC2Num[3];
            }
            set
            {
                //int iOldID = NPC3;
                //int iNewID = NPC3;

                m_NPC2Num[3] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("生成器所包含的NPC")]
        public int NPC4
        {
            get
            {
                return (int)m_NPC2ID[4];
            }
            set
            {
                //int iOldID = NPC4;
                //int iNewID = value;

                NPCID2ID[4] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }
        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("NPC的数量")]
        public int NPC4Num
        {
            get
            {
                return (int)m_NPC2Num[4];
            }
            set
            {
                //int iOldID = NPC4;
                //int iNewID = NPC4;

                m_NPC2Num[4] = value;

                //RefreshNpcStatistic(iOldID, iNewID);
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("补充方式\n1:永不补充 2:逐个补充 3:整批补充 4:固定时间间隔补充")]
        public SupplyModeType SupplyMothed
        {
            get
            {
                return m_strSupplyMothed;
            }
            set
            {
                m_strSupplyMothed = value;
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("刷新间隔时间\n单位：秒")]
        public String RefreshTime
        {
            get
            {
                return m_strRefreshTime;
            }
            set
            {
                m_strRefreshTime = value;
            }
        }

        [CategoryAttribute("NPC 生成器属性"), DescriptionAttribute("排队间隔\r\nInt型,单位为秒\r\n进入逐个刷新的NPC将额外累加上该时间间隔，防止同时死亡的NPC同时刷新。")]
        public int QueueIntervalTime
        {
            get
            {
                return m_iQueueIntervalTime;
            }
            set
            {
                m_iQueueIntervalTime = value;
            }
        }

        [CategoryAttribute("时间属性"), DescriptionAttribute("时间方式\n1一直不断开启 2固定时间段中开启")]
        public TimeMethodType TimeMothed
        {
            get
            {
                return m_strTimeMothed;
            }
            set
            {
                m_strTimeMothed = value;
            }
        }
        [CategoryAttribute("时间属性"), DescriptionAttribute("时间格式")]
        public String TimeFormat
        {
            get
            {
                return m_strTimeFormat;
            }
            set
            {
                m_strTimeFormat = value;
            }
        }
        [CategoryAttribute("时间属性"), DescriptionAttribute("起始时间")]
        public String StartTime
        {
            get
            {
                return m_strStartTime;
            }
            set
            {
                m_strStartTime = value;
            }
        }
        [CategoryAttribute("时间属性"), DescriptionAttribute("终止时间")]
        public String EndTime
        {
            get
            {
                return m_strEndTime;
            }
            set
            {
                m_strEndTime = value;
            }
        }

        [CategoryAttribute("寻路半径"), DescriptionAttribute("怪物的寻路半径")]
        public int MoveRange
        {
            get
            {
                return m_iMoveRange;
            }
            set
            {
                m_iMoveRange = value;
            }
        }

        [BrowsableAttribute(false)]
        public Hashtable NPCID2Num
        {
            get
            {
                return m_NPC2Num;
            }
        }

        [BrowsableAttribute(false)]
        public Hashtable NPCID2ID
        {
            get
            {
                return m_NPC2ID;
            }
        }

        [BrowsableAttribute(false)]
        public bool Hidden
        {
            get
            {
                return m_bHidden;
            }
            set
            {
                m_bHidden = value;
            }
        }

        [BrowsableAttribute(false)]
        public Point CenterPoint
        {
            get
            {
                Point center = new Point();
                center.X = (m_Rect.Left + m_Rect.Right) / 2;
                center.Y = (m_Rect.Top + m_Rect.Bottom) / 2;
                return center;
            }
        }

        //---------------------------------------------------------------------------------------

        // 物件的移动函数
        public bool Move(int dx, int dy, int width, int height)
        {
            bool bOutOfBound = false;   // 是否出界

            m_Rect.X += dx;
            m_Rect.Y -= dy;

            if (m_Rect.X < 0)
            {
                m_Rect.X = 0;
                bOutOfBound = true;
            }
            if (m_Rect.X > width - m_Rect.Width - 1)
            {
                m_Rect.X = width - m_Rect.Width - 1;
                bOutOfBound = true;
            }

            if (m_Rect.Y < 0)
            {
                m_Rect.Y = 0;
                bOutOfBound = true;
            }
            if (m_Rect.Y > height - m_Rect.Height - 1)
            {
                m_Rect.Y = height - m_Rect.Height - 1;
                bOutOfBound = true;
            }

            if (bOutOfBound)
            {
                return false;
            }
            else
            {
                return IsBlock();
            }
        }

        // 物件的旋转函数
        public void Whirl(int delta)
        {
            // 增加旋转的度数
            m_iDirection += delta;

            // 度数不能为负数
            while (m_iDirection < 0)
            {
                m_iDirection += 360;
            }

            // 保持度数在0~360之间
            m_iDirection %= 361;
        }

        // 物件大小调整函数
        public bool ReSize(int dx, int dy)
        {
            if (IsSingle() && (dx < 0 || dy < 0))
            {

            }
            else
            {
                m_Rect.Width += dx;
                m_Rect.Height += dy;
                m_Rect.Y -= dy;

                if (m_Rect.Width < 2)
                {
                    m_Rect.Width = 2;
                }
                if (m_Rect.Height < 2)
                {
                    m_Rect.Height = 2;
                    m_Rect.Y += dy;
                }
            }

            return IsBlock();
        }

        // 切换生成器的类型，单点<-->区域
        public void SetAreaMethod()
        {
            if (m_Rect.Width == 0 || m_Rect.Height == 0)
            {
                m_Rect.Width = 10;
                m_Rect.Height = 10;
            }
            else
            {
                m_Rect.Width = 0;
                m_Rect.Height = 0;
            }
        }

        // 检测是否发生碰撞
        public bool IsBlock()
        {
            if (m_Rect.Width == 0 || m_Rect.Height == 0)
            {
                int iX = m_Rect.X;
                int iY = m_Rect.Y;
                int iBlock = SceneSketchManager.Instance.GetBlockInfo(m_iMapID, iX, iY);
                if (iBlock == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                for (int iX = m_Rect.X - m_iMoveRange; iX < m_Rect.Right + m_iMoveRange; iX++)
                {
                    for (int iY = m_Rect.Y - m_iMoveRange; iY < m_Rect.Bottom + m_iMoveRange; iY++)
                    {
                        int iBlock = SceneSketchManager.Instance.GetBlockInfo(m_iMapID, iX, iY);
                        if (iBlock == 1)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        // clone一个NPC生成器
        public NPCCreatorInfo Clone()
        {
            // 生成一个新的NPC
            NPCCreatorInfo newCreatorInfo = new NPCCreatorInfo();

            // 设置属性
            newCreatorInfo.ID = NPCManager.Instance.GetBigNPC(m_iMapID) + 1;
            newCreatorInfo.Name = m_strName;
            newCreatorInfo.Rectangle = new Rectangle(m_Rect.X + 5, m_Rect.Y - 5, m_Rect.Width, m_Rect.Height);
            newCreatorInfo.MapID = m_iMapID;
            newCreatorInfo.Direction = m_iDirection;

            newCreatorInfo.SupplyMothed = m_strSupplyMothed;
            newCreatorInfo.RefreshTime = m_strRefreshTime;
            newCreatorInfo.TimeMothed = m_strTimeMothed;
            newCreatorInfo.TimeFormat = m_strTimeFormat;
            newCreatorInfo.StartTime = m_strStartTime;
            newCreatorInfo.EndTime = m_strEndTime;

            newCreatorInfo.MoveRange = m_iMoveRange;

            newCreatorInfo.NPC0 = this.NPC0;
            newCreatorInfo.NPC0Num = this.NPC0Num;
            newCreatorInfo.NPC1 = this.NPC1;
            newCreatorInfo.NPC1Num = this.NPC1Num;
            newCreatorInfo.NPC2 = this.NPC2;
            newCreatorInfo.NPC2Num = this.NPC2Num;
            newCreatorInfo.NPC3 = this.NPC3;
            newCreatorInfo.NPC3Num = this.NPC3Num;
            newCreatorInfo.NPC4 = this.NPC4;
            newCreatorInfo.NPC4Num = this.NPC4Num;

            // 返回NPC
            return newCreatorInfo;
        }

        // NPC属性正确性检测
        public bool ValidateCheck()
        {
            String strLocation = "(" + m_Rect.X.ToString() + "," + m_Rect.Y.ToString() + ")";

            // 验证是否进入碰撞地带
            if (IsBlock())
            {
                MessageBox.Show(m_strName + strLocation + "进入碰撞地带！");
                return false;
            }

            // 验证是否为空生成器
            if (NPC0 == 0 || NPC0Num == 0)
            {
                MessageBox.Show(m_strName + strLocation + "没有包含任何怪物！");
                return false;
            }

            return true;
        }

        // 是否是单点NPC
        public bool IsSingle()
        {
            return (m_Rect.Width == 0 || m_Rect.Height == 0);
        }

        // 是否含有指定ID的NPC
        public int Contains(int iNpcID)
        {
            // 初始化
            int iTotalNum = 0;

            // 计算
            for (int iNpcIndex = 0; iNpcIndex < 5; iNpcIndex++)
            {
                int iID = (int)m_NPC2ID[iNpcIndex];
                if (iID == 0)
                {
                    break;
                }
                else
                {
                    if (iID.ToString().StartsWith(iNpcID.ToString()))
                    {
                        int iNum = (int)m_NPC2Num[iNpcIndex];
                        iTotalNum += iNum;
                    }
                }
            }

            // 将统计出的数量返回
            return iTotalNum;
        }

        // 更新NPC统计信息
        public void RefreshNpcStatistic(int iOldID, int iNewID)
        {
            String strNpcID = MainForm.Instance.StatisticsNpcID.ToString();
            if (iOldID.ToString().StartsWith(strNpcID) || iNewID.ToString().StartsWith(strNpcID))
            {
                MainForm.Instance.RefreshNpcStatistics(m_iMapID, MainForm.Instance.StatisticsNpcID);
            }
        }

        // 判断2个npc之间的距离是否足够近
        public bool IsNear(NPCCreatorInfo brother, int iDistance)
        {
            Point pa = CenterPoint;
            Point pb = brother.CenterPoint;
            int dx = pb.X - pa.X;
            int dy = pb.Y - pa.Y;
            if(dx*dx + dy*dy < iDistance*iDistance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // 获取NPC是否主动攻击
        public int IsAggressive()
        {
            // 获取第一个NPC
            NPCInfo npc = NPCManager.Instance.GetNPCInfoByID(NPC0);
            if(npc == null)
            {
                return -1;//表示出错
            }
            else
            {
                // 获取第一个值
                int result = npc.IsAggressive;

                // 遍历剩下的4个，查看是否一致
                for (int index = 1; index < 5;++index )
                {
                    int id = (int)m_NPC2ID[index];
                    if(id == 0)
                    {
                        return result;  // 结束
                    }
                    else
                    {
                        NPCInfo subnpc = NPCManager.Instance.GetNPCInfoByID(id);
                        if(subnpc == null)
                        {
                            return -1;
                        }
                        else 
                        {
                            if(subnpc.IsAggressive != result)
                            {
                                return -1;
                            }
                        }
                    }
                }

                return result;
            }
        }
        //-----------------------------------------------------------

        // 私有成员
        private int m_iID;          // NPC 生成器的唯一编号  
        private String m_strName;      // NPC 生成器名称
        private Rectangle m_Rect = new Rectangle();   // 范围
        private int m_iMapID;       // 所在场景编号
        private int m_iDirection;   //单点生成NPC的朝向

        private SupplyModeType m_strSupplyMothed = (SupplyModeType)1;   //补充方式
        private String m_strRefreshTime = "10"; // 刷新间隔时间
        private TimeMethodType m_strTimeMothed = (TimeMethodType)1;   //时间方式
        private String m_strTimeFormat = "4";      //时间格式
        private String m_strStartTime = "00";      //开始时间
        private String m_strEndTime = "01";      //终止时间

        private int m_iMoveRange = OptionManager.NpcMoveRangeSize;   // 怪物的寻路半径

        private Hashtable m_NPC2Num = new Hashtable();  // 这个生成器具有的 npc 编号-数量 映射
        private Hashtable m_NPC2ID = new Hashtable();

        private bool m_bHidden = false;    // 生成器是否可见

        // 2009-03-23 徐磊添加Active和QueueIntervalTime属性
        private int m_iActive = 1;  // 在初始化时是否激活该生成器(0：不激活 1：激活)
        private int m_iQueueIntervalTime = 0;   // 排队间隔(Int型,单位为秒)进入逐个刷新的NPC将额外累加上该时间间隔，防止同时死亡的NPC同时刷新。

        // 2009-09-02 徐磊添加ExtraID
        private int m_iExtraID = -1;    // 实现物件NPC机制
        private bool m_bIsitemnpc = false;  // 是否物件npc
        private int m_iItemnpcseq = -1;//物件序号，对应哪一个物件npc，据此在itemnpc.xml中查找对应的itemnpc配置(==itemnpc.xml中的itemnpcseq )，同类物件npc的每个不同实例要用不同的生成器生成，因为每个物件都要有唯一的序号标识自己，生成时校验下mapid
        //---------------------------------------------------------------------------------------
    };

    // 读入 npc 数据并维护
    class NPCManager
    {
        private NPCManager()
        { }

        // 公有函数

        // 从 xml 文件中载入 npc info
        public void LoadNPCInfoFromXml(String strXmlFilename, bool bRefresh)
        {
            // 清除原来的 npc 数据
            if(bRefresh)
            {
                m_NPCInfoList.Clear();
            }
            

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);

            XmlElement elmtNpcRoot = xmlDoc.LastChild as XmlElement;
            if (elmtNpcRoot == null)
            {
                return;
            }

            XmlElement elmtNPC = elmtNpcRoot.FirstChild as XmlElement;

            while (elmtNPC != null)
            {
                NPCInfo npc = new NPCInfo();
                try
                {
                    npc.ID = int.Parse(elmtNPC.GetAttribute("nId"));
                    npc.Name = elmtNPC.GetAttribute("nNPCName");
                    npc.Desc = elmtNPC.GetAttribute("nNPCDepict");
                    npc.DialogScriptID = int.Parse(elmtNPC.GetAttribute("nScriptId"));
                    String strDeathScript = elmtNPC.GetAttribute("DeathScript");
                    if (strDeathScript != null
                        && !strDeathScript.Equals(""))
                    {
                        npc.DeathScriptID = int.Parse(strDeathScript);
                    }
                    else
                    {
                        npc.DeathScriptID = -1;
                    }

                    npc.IsAggressive = int.Parse(elmtNPC.HasAttribute("nIsAggressive") ? elmtNPC.GetAttribute("nIsAggressive") : "1" );

                    m_NPCInfoList.Add(npc);

                    elmtNPC = elmtNPC.NextSibling as XmlElement;
                }
                catch (Exception e)
                {
                    int iNPCIndex = m_NPCInfoList.Count + 1;
                    MessageBox.Show("文件" +　strXmlFilename + "载入ID为"+ npc.ID.ToString() +"的npc时发生异常:" + e.ToString());
                    elmtNPC = elmtNPC.NextSibling as XmlElement;
                }
            }
        }

        // 从 xml 文件中载入 npc creator info
        public void LoadNPCCreatorInfoFromXml(String strXmlFilename)
        {
            // 清除原来的生成器数据
            m_MapID2CreatorList.Clear();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);

            XmlElement elmtCreatorRoot = xmlDoc.LastChild as XmlElement;
            if (elmtCreatorRoot == null)
            {
                return;
            }

            XmlElement elmtMapID = elmtCreatorRoot.FirstChild as XmlElement;
            // 遍历每一个 MapID 
            while (elmtMapID != null)
            {
                int iMapID = int.Parse(elmtMapID.GetAttribute("MapNo"));
                ArrayList creatorList = new ArrayList();

                XmlElement elmtCreator = elmtMapID.FirstChild as XmlElement;
                while (elmtCreator != null)
                {
                    try
                    {
                        NPCCreatorInfo creatorInfo = new NPCCreatorInfo();
                        creatorInfo.ID = int.Parse(elmtCreator.GetAttribute("NPCCreatorNO"));
                        creatorInfo.Name = elmtCreator.GetAttribute("NPCCreatorName");
                        creatorInfo.MapID = iMapID;
                        int iLeft = int.Parse(elmtCreator.GetAttribute("Left"));
                        int iRight = int.Parse(elmtCreator.GetAttribute("Right"));
                        int iTop = int.Parse(elmtCreator.GetAttribute("Top"));
                        int iBottom = int.Parse(elmtCreator.GetAttribute("Bottom"));
                        int iAreaMethod = int.Parse(elmtCreator.GetAttribute("AreaMethod"));

                        int iTimeMethodTemp = int.Parse(elmtCreator.GetAttribute("TimeMethod"));
                        creatorInfo.TimeMothed = (NPCCreatorInfo.TimeMethodType)iTimeMethodTemp;

                        int iTemp = int.Parse(elmtCreator.GetAttribute("SupplyMethod"));
                        creatorInfo.SupplyMothed = (NPCCreatorInfo.SupplyModeType)iTemp;

                        // 刷新间隔时间
                        if (elmtCreator.HasAttribute("RefreshTime"))
                        {
                            creatorInfo.RefreshTime = elmtCreator.GetAttribute("RefreshTime");
                        }

                        // 2009-03-23 徐磊添加Active和QueueIntervalTime属性
                        // 是否激活
                        if (elmtCreator.HasAttribute("Active"))
                        {
                            creatorInfo.Active = int.Parse(elmtCreator.GetAttribute("Active"));
                        }

                        // 排队间隔
                        if (elmtCreator.HasAttribute("QueueIntervalTime"))
                        {
                            creatorInfo.QueueIntervalTime = int.Parse(elmtCreator.GetAttribute("QueueIntervalTime"));
                        }

                        // 物件NPC机制
                        if (elmtCreator.HasAttribute("ExtraID"))
                        {
                            creatorInfo.ExtraID = int.Parse(elmtCreator.GetAttribute("ExtraID"));
                        }

                        creatorInfo.Isitemnpc = elmtCreator.HasAttribute("Isitemnpc") ? (int.Parse(elmtCreator.GetAttribute("Isitemnpc")) == 0 ? false : true) : false;
                        creatorInfo.Itemnpcseq = elmtCreator.HasAttribute("Itemnpcseq") ? int.Parse(elmtCreator.GetAttribute("Itemnpcseq")) : -1;

                        creatorInfo.TimeFormat = elmtCreator.GetAttribute("TimeFormat");
                        creatorInfo.StartTime = elmtCreator.GetAttribute("StartTime");
                        creatorInfo.EndTime = elmtCreator.GetAttribute("EndTime");

                        // 添加怪物寻路范围框的读写
                        if (elmtCreator.HasAttribute("MoveRange"))
                        {
                            creatorInfo.MoveRange = int.Parse(elmtCreator.GetAttribute("MoveRange"));
                        }

                        if (iAreaMethod == 1)
                        {
                            creatorInfo.Rectangle = new Rectangle(iLeft, iTop, 0, 0);
                        }
                        else
                        {
                            creatorInfo.Rectangle = new Rectangle(iLeft, iTop, iRight - iLeft, iBottom - iTop);
                        }

                        XmlElement elmtNPC = elmtCreator.FirstChild as XmlElement;

                        int iNPCTargetIndex = 0;
                        while (elmtNPC != null)
                        {
                            int iID = int.Parse(elmtNPC.GetAttribute("NPCNo"));
                            int iNum = int.Parse(elmtNPC.GetAttribute("Num"));
                            creatorInfo.NPCID2ID[iNPCTargetIndex] = iID;
                            creatorInfo.NPCID2Num[iNPCTargetIndex] = iNum;
                            if (iNPCTargetIndex == 0)
                            {
                                int iDirection = int.Parse(elmtNPC.GetAttribute("Direction"));
                                creatorInfo.Direction = iDirection;
                            }
                            elmtNPC = elmtNPC.NextSibling as XmlElement;
                            iNPCTargetIndex++;
                        }

                        while (iNPCTargetIndex <= 4)
                        {
                            creatorInfo.NPCID2ID[iNPCTargetIndex] = 0;
                            creatorInfo.NPCID2Num[iNPCTargetIndex] = 0;
                            iNPCTargetIndex++;
                        }
                        creatorList.Add(creatorInfo);

                        elmtCreator = elmtCreator.NextSibling as XmlElement;
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.ToString());
                        elmtCreator = elmtCreator.NextSibling as XmlElement;
                    }
                }

                m_MapID2CreatorList[iMapID] = creatorList;
                elmtMapID = elmtMapID.NextSibling as XmlElement;
            }
        }

        // 将NpcCreator信息写入xml文件中
        public void SaveNPCCreatorInfoToXml(String strXmlFilename)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("NPCCreatorInfo");
            root.SetAttribute("FileVersion", "0.9");
            xmlDoc.AppendChild(root);

            foreach (int nMapID in m_MapID2CreatorList.Keys)
            {
                // 首先检测数据的正确性
                if (!ValidateCheck(nMapID))
                {
                    MessageBox.Show("地图" + nMapID.ToString() + "可能存在数据不完整问题！");
                }

                SceneSketch sceneSketch = SceneSketchManager.Instance.GetSceneSketch(nMapID);
                if (sceneSketch == null)
                {
                    continue;
                }
                XmlElement xmlMap = xmlDoc.CreateElement("Map");
                xmlMap.SetAttribute("MapNo", nMapID.ToString());

                ArrayList creatorList = m_MapID2CreatorList[nMapID] as ArrayList;
                foreach (NPCCreatorInfo creator in creatorList)
                {
                    XmlElement xmlNPCCreator = xmlDoc.CreateElement("NPCCreator");
                    xmlNPCCreator.SetAttribute("NPCCreatorNO", creator.ID.ToString());
                    xmlNPCCreator.SetAttribute("NPCCreatorName", creator.Name);
                    int nLeft = creator.Rectangle.Left;
                    int nTop = creator.Rectangle.Top;
                    int nRight = creator.Rectangle.Right;
                    int nBottom = creator.Rectangle.Bottom;

                    int nSortNum = 0;
                    // 添加NPC节点
                    if (creator.Rectangle.Height == 0 || creator.Rectangle.Width == 0)
                    {
                        xmlNPCCreator.SetAttribute("AreaMethod", "1");

                        XmlElement xmlNPCTarget = xmlDoc.CreateElement("NPC");
                        xmlNPCTarget.SetAttribute("NPCNo", creator.NPCID2ID[0].ToString());
                        xmlNPCTarget.SetAttribute("Num", creator.NPCID2Num[0].ToString());
                        xmlNPCTarget.SetAttribute("Direction", creator.Direction.ToString());
                        xmlNPCCreator.AppendChild(xmlNPCTarget);

                        nRight = nLeft + 1;
                        nBottom = nTop + 1;

                        nSortNum = 1;
                    }
                    else
                    {
                        xmlNPCCreator.SetAttribute("AreaMethod", "2");

                        int nTargetIndex = 0;
                        while (nTargetIndex <= 4)
                        {
                            int nID = (int)creator.NPCID2ID[nTargetIndex];
                            if (nID != 0)
                            {
                                XmlElement xmlNPCTarget = xmlDoc.CreateElement("NPC");
                                xmlNPCTarget.SetAttribute("NPCNo", creator.NPCID2ID[nTargetIndex].ToString());
                                xmlNPCTarget.SetAttribute("Num", creator.NPCID2Num[nTargetIndex].ToString());
                                xmlNPCTarget.SetAttribute("Direction", "0");
                                xmlNPCCreator.AppendChild(xmlNPCTarget);
                                nTargetIndex++;
                                nSortNum++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    xmlNPCCreator.SetAttribute("TimeMethod", ((int)creator.TimeMothed).ToString());
                    xmlNPCCreator.SetAttribute("SupplyMethod", ((int)creator.SupplyMothed).ToString());
                    xmlNPCCreator.SetAttribute("RefreshTime", creator.RefreshTime);

                    xmlNPCCreator.SetAttribute("Left", nLeft.ToString());
                    xmlNPCCreator.SetAttribute("Top", nTop.ToString());
                    xmlNPCCreator.SetAttribute("Right", nRight.ToString());
                    xmlNPCCreator.SetAttribute("Bottom", nBottom.ToString());

                    xmlNPCCreator.SetAttribute("TimeFormat", creator.TimeFormat);
                    xmlNPCCreator.SetAttribute("StartTime", creator.StartTime);
                    xmlNPCCreator.SetAttribute("EndTime", creator.EndTime);
                    xmlNPCCreator.SetAttribute("SortNum", nSortNum.ToString());

                    // 添加怪物移动范围的读写
                    xmlNPCCreator.SetAttribute("MoveRange", creator.MoveRange.ToString());

                    // 2009-03-23 徐磊添加Active和QueueIntervalTime属性
                    xmlNPCCreator.SetAttribute("Active", creator.Active.ToString());
                    xmlNPCCreator.SetAttribute("QueueIntervalTime", creator.QueueIntervalTime.ToString());

                    // 2009-09-02 徐磊添加ExtraID
                    xmlNPCCreator.SetAttribute("ExtraID", creator.ExtraID.ToString());
                    xmlNPCCreator.SetAttribute("Isitemnpc", creator.Isitemnpc?"1":"0");
                    xmlNPCCreator.SetAttribute("Itemnpcseq", creator.Itemnpcseq.ToString());

                    xmlMap.AppendChild(xmlNPCCreator);

                }

                root.AppendChild(xmlMap);
            }

            xmlDoc.Save(strXmlFilename);
        }

        public int GetNPCCreatorIDByName(String strName)
        {
            foreach (ArrayList creatorList in m_MapID2CreatorList.Values)
            {
                foreach (NPCCreatorInfo creator in creatorList)
                {
                    if (creator.Name.Equals(strName))
                    {
                        return creator.ID;
                    }
                }
            }
            return -1;
        }

        public int GetNPCInfoIDByName(String strName)
        {
            foreach (NPCInfo npc in m_NPCInfoList)
            {
                if (npc.Name.Equals(strName))
                {
                    return npc.ID;
                }
            }

            return -1;
        }


        public NPCCreatorInfo GetNPCCreatorByID(int iID)
        {
            foreach (ArrayList creatorList in m_MapID2CreatorList.Values)
            {
                foreach (NPCCreatorInfo creator in creatorList)
                {
                    if (creator.ID == iID)
                    {
                        return creator;
                    }
                }
            }
            return null;
        }

        public NPCInfo GetNPCInfoByID(int iID)
        {
            foreach (NPCInfo npc in m_NPCInfoList)
            {
                if (npc.ID == iID)
                {
                    return npc;
                }
            }

            return null;
        }

        // 根据 NPCInfo ID 获取该 NPC 在 m_NPCInfoList 中的索引
        public int GetNPCInfoIndexByID(int iID)
        {
            for (int i = 0; i < m_NPCInfoList.Count; i++)
            {
                if ((m_NPCInfoList[i] as NPCInfo).ID == iID)
                {
                    return i;
                }
            }
            return -1;
        }

        // 删除NPC生成器
        public bool DelNPCCreator(int iID)
        {
            NPCCreatorInfo delNPC = GetNPCCreatorByID(iID);
            if (delNPC != null)
            {
                // 删除前，更新NPC统计信息
                int iMapID = -1;
                if (delNPC.Contains(MainForm.Instance.StatisticsNpcID) > 0)
                {
                    iMapID = delNPC.MapID;
                }
                // 删除前，更新NPC统计信息

                ArrayList npclist = m_MapID2CreatorList[delNPC.MapID] as ArrayList;
                npclist.Remove(delNPC);
                MainForm.Instance.SelectedNPCCreator.Hide();

                if (iMapID != -1)
                {
                    MainForm.Instance.RefreshNpcStatistics(iMapID, MainForm.Instance.StatisticsNpcID);
                }

                return true;
            }
            return false;
        }

        // 获取编号最大的NPC
        public int GetBigNPC(int iMapID)
        {
            String strBig = iMapID.ToString() + "0000";

            int iBigID = int.Parse(strBig);

            ArrayList creatorList = m_MapID2CreatorList[iMapID] as ArrayList;

            if (creatorList == null)
            {
                return 0;
            }

            foreach (NPCCreatorInfo creator in creatorList)
            {
                if (creator.ID >= iBigID)
                {
                    iBigID = creator.ID;
                }
            }

            return iBigID;
        }

        public NPCCreatorInfo AddNPCCreator(int iMapID)
        {
            ArrayList npclist = m_MapID2CreatorList[iMapID] as ArrayList;

            if (npclist == null)
            {
                npclist = new ArrayList();
            }
            m_MapID2CreatorList[iMapID] = npclist;

            NPCCreatorInfo creatorInfo = new NPCCreatorInfo();
            int iNewNPCID = GetBigNPC(iMapID);
            if (iMapID == 0)
            {
                return null;
            }
            else
            {
                creatorInfo.ID = iNewNPCID + 1;
            }

            creatorInfo.Name = "新的生成器";
            creatorInfo.MapID = iMapID;
            int iLeft = 50;
            int iRight = 70;
            int iTop = 50;
            int iBottom = 70;
            creatorInfo.Rectangle = new Rectangle(iLeft, iBottom, iRight - iLeft, iBottom - iTop);


            int iNPCTargetIndex = 0;

            while (iNPCTargetIndex <= 4)
            {
                creatorInfo.NPCID2ID[iNPCTargetIndex] = 0;
                creatorInfo.NPCID2Num[iNPCTargetIndex] = 0;
                iNPCTargetIndex++;
            }
            npclist.Add(creatorInfo);

            return creatorInfo;
        }

        // clone一个生成器
        public NPCCreatorInfo CloneNPCCreator(int iCreatorID)
        {
            // 获取要clone的生成器
            NPCCreatorInfo oldCreator = NPCManager.Instance.GetNPCCreatorByID(iCreatorID);
            if (oldCreator == null)
            {
                return null;
            }

            // 获取地图
            ArrayList npclist = m_MapID2CreatorList[oldCreator.MapID] as ArrayList;
            if (npclist == null)
            {
                return null;
            }

            // clone并添加
            NPCCreatorInfo newCreator = oldCreator.Clone();
            npclist.Add(newCreator);

            return newCreator;
        }

        // 数据正确性验证
        public bool ValidateCheck(int iMapID)
        {
            ArrayList creatorList = m_MapID2CreatorList[iMapID] as ArrayList;

            if (creatorList == null)
            {
                return true;
            }

            foreach (NPCCreatorInfo creator in creatorList)
            {
                if (!creator.ValidateCheck())
                {
                    return false;
                }
            }

            return true;
        }

        // 根据地图号和名称， 对NPC进行筛选
        public Hashtable NPCFilter(int iMapID, int iType, String strNPCName)
        {
            // 创建结果数据集
            Hashtable result = new Hashtable();

            // 先找出地图所对应的npc列表
            foreach (int id in m_MapID2CreatorList.Keys)
            {
                if (iMapID == -1 || iMapID == id)
                {
                    ArrayList creatorList = m_MapID2CreatorList[id] as ArrayList;
                    foreach (NPCCreatorInfo creator in creatorList)
                    {
                        Hashtable idTable = creator.NPCID2ID;
                        int index = 0;
                        while (index < 5)
                        {
                            int iNPCID = (int)idTable[index];
                            if (iNPCID == 0)
                            {
                                break;
                            }
                            else
                            {
                                NPCInfo info = GetNPCInfoByID(iNPCID);
                                if (info != null)
                                {
                                    int iNPCType = int.Parse(iNPCID.ToString().Substring(0, 1));
                                    if (iType == -1 || iType == iNPCType)
                                    {
                                        if (strNPCName.Length == 0 || info.Name.Contains(strNPCName) || info.ID.ToString().Contains(strNPCName))
                                        {
                                            result[info.ID] = info;
                                        }
                                    }
                                }

                                index++;
                            }
                        }
                    }
                }
            }

            // 将结果返回
            return result;
        }

        // 统计一张地图中某个NPC的数量
        public int CalNpcNumByID(int iMapID, int iNpcID)
        {
            // 初始化变量
            ArrayList npclist = m_MapID2CreatorList[iMapID] as ArrayList;
            int iResultNum = 0;

            // 遍历统计数量
            if (npclist == null)
            {
                return 0;
            }
            else
            {
                foreach (NPCCreatorInfo info in npclist)
                {
                    iResultNum += info.Contains(iNpcID);
                }
            }

            // 返回结果
            return iResultNum;
        }

        // 生成怪物分布图
        public void ExportMonsterDistribute(String strFileName, int iMaxDistance)
        {
            // 最终的MapID和合并列表的HashTable
            Hashtable MapID2MergeList = new Hashtable();

            foreach (int iMapID in m_MapID2CreatorList.Keys)
            {
                // 怪物分类的HashTable
                Hashtable monsterTable = new Hashtable();

                // 获取Monster列表
                ArrayList creatorList = m_MapID2CreatorList[iMapID] as ArrayList;

                // 将怪物刷选出来放入hashtable，ID以“2”开始的为Monster
                foreach (NPCCreatorInfo info in creatorList)
                {
                    if (info.NPC0.ToString().StartsWith("2"))
                    {
                        // 获取ID的前六位，作为种族编号
                        //String strMonsterRace = info.NPC0.ToString().Substring(0, 6);
                        String strMonsterRace = info.NPC0.ToString();

                        // 用种族编号作为key，获取种族List，不存在则新建
                        ArrayList MonsterRaceList = monsterTable[strMonsterRace] as ArrayList;
                        if (MonsterRaceList == null)
                        {
                            MonsterRaceList = new ArrayList();
                            monsterTable[strMonsterRace] = MonsterRaceList;
                        }

                        // 将info存放入List中
                        MonsterRaceList.Add(info);
                    }
                }

                // 存放最终结果的resultList
                ArrayList resultList = new ArrayList();

                #region 元素合并
                // 将hashtable中的元素合并
                foreach (String strRaceID in monsterTable.Keys)
                {
                    // 获取种族列表
                    ArrayList raceList = monsterTable[strRaceID] as ArrayList;
                    if (raceList == null || raceList.Count == 0)
                    {
                        continue;
                    }

                    // 开始合并
                    if (raceList.Count == 1)// 只有一个，无需合并
                    {
                        NPCCreatorInfo singleMonster = raceList[0] as NPCCreatorInfo;
                        NPCCreatorInfo newRace = singleMonster.Clone();
                        newRace.ID = int.Parse(strRaceID);
                        resultList.Add(newRace);
                    }
                    else// 多个的情况下，判断是否需要分裂
                    {
                        while (raceList.Count > 0)
                        {
                            // 临时合并列表
                            ArrayList tempList = new ArrayList();

                            // 将第一个元素存放入tempList中
                            tempList.Add(raceList[0]);
                            raceList.RemoveAt(0);

                            int iCurrIndex = 0; // 当前tempList中用于寻找的Index
                            while (iCurrIndex < tempList.Count)
                            {
                                // 获取当前的Monster
                                NPCCreatorInfo currMonster = tempList[iCurrIndex] as NPCCreatorInfo;

                                // 遍历BeginList寻找适合的Monster
                                ArrayList removeList = new ArrayList();
                                foreach (NPCCreatorInfo brotherMonster in raceList)
                                {
                                    if (currMonster.IsNear(brotherMonster, iMaxDistance))
                                    {
                                        tempList.Add(brotherMonster);
                                        removeList.Add(brotherMonster);
                                    }
                                }

                                // 将找到的monster从beginList中移除
                                foreach (NPCCreatorInfo delNPC in removeList)
                                {
                                    raceList.Remove(delNPC);
                                }

                                // iCurrIndex 自增加，用于下一次遍历
                                iCurrIndex++;
                            }

                            // 将tempList合并
                            NPCCreatorInfo mergeMonster = (tempList[0] as NPCCreatorInfo).Clone();
                            mergeMonster.ID = int.Parse(strRaceID);
                            int iLeft = mergeMonster.Rectangle.Left;
                            int iTop = mergeMonster.Rectangle.Top;
                            int iRight = mergeMonster.Rectangle.Right;
                            int iBottom = mergeMonster.Rectangle.Bottom;
                            foreach(NPCCreatorInfo tempNPC in tempList)
                            {
                                // 上
                                if (tempNPC.Rectangle.Top < iTop)
                                {
                                    iTop = tempNPC.Rectangle.Top;
                                }
                                // 下
                                if (tempNPC.Rectangle.Bottom > iBottom)
                                {
                                    iBottom = tempNPC.Rectangle.Bottom;
                                }
                                // 左
                                if (tempNPC.Rectangle.Left < iLeft)
                                {
                                    iLeft = tempNPC.Rectangle.Left;
                                }
                                // 右
                                if (tempNPC.Rectangle.Right > iRight)
                                {
                                    iRight = tempNPC.Rectangle.Right;
                                }
                            }
                            mergeMonster.Rectangle = new Rectangle(iLeft, iTop, iRight - iLeft, iBottom - iTop);

                            // 加入最终结果列表中
                            resultList.Add(mergeMonster);
                        }
                    }
                }
                #endregion

                // 加入总表中
                MapID2MergeList[iMapID] = resultList;
            }

            // 写入文件
            WriteMonsterDistributeFile(strFileName, MapID2MergeList);
        }

        private void WriteMonsterDistributeFile(String strFileName, Hashtable MapID2MergeList)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("MonsterDistribute");
            root.SetAttribute("FileVersion", "0.1");
            xmlDoc.AppendChild(root);

            foreach (int nMapID in MapID2MergeList.Keys)
            {

                XmlElement xmlMap = xmlDoc.CreateElement("Map");
                xmlMap.SetAttribute("id", nMapID.ToString());

                ArrayList creatorList = MapID2MergeList[nMapID] as ArrayList;
                foreach (NPCCreatorInfo creator in creatorList)
                {
                    XmlElement xmlNPCCreator = xmlDoc.CreateElement("Monster");

                    xmlNPCCreator.SetAttribute("id", creator.ID.ToString());

                    xmlNPCCreator.SetAttribute("posx", creator.CenterPoint.X.ToString());
                    xmlNPCCreator.SetAttribute("posy", creator.CenterPoint.Y.ToString());

                    xmlMap.AppendChild(xmlNPCCreator);
                }

                root.AppendChild(xmlMap);
            }

            xmlDoc.Save(strFileName);
        }
        //---------------------------------------------------------------------------------

        // 公有属性
        public ArrayList NPCInfoList
        {
            get
            {
                return m_NPCInfoList;
            }
        }

        public Hashtable MapID2NPCCreatorList
        {
            get
            {
                return m_MapID2CreatorList;
            }
        }

        public static NPCManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new NPCManager();
                }
                return ms_Instance;
            }
        }
        //--------------------------------------------------------------------------------

        private ArrayList m_NPCInfoList = new ArrayList();    // NPC info 列表
        private Hashtable m_MapID2CreatorList = new Hashtable();    // 地图编号 - 该地图 NPC Creator 映射
        private static NPCManager ms_Instance = null;
    }
}
