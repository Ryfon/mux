﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml;
using System.Data;

namespace MissionEditor
{
    // 用户定制选项管理器
    public enum CurrencyType : int
    {
        游戏币购买 = 0,
        RMB购买 = 1,
        以物易物 = 2,
    }

    public class OptionManager
    {
        // 显示模式
        public enum DisplayMode
        {
            DM_ID,
            DM_NAME,
            DM_ID_NAME,
        }

        public enum MissionType : int
        {
            //任务包,
            计数器类 = 1,
            对话类 = 2,
            护送类 = 3,
            探索类 = 4,
            职业类 = 5,
        }

        public enum MissionTypeDialogSubType : int
        {
            单纯对话 = 1,
            送物品 = 2,
            取物品 = 3,
        }

        public enum MissionTypeCounterSubType : int
        {
            杀怪 = 1,
            收集物品 = 2,
            杀人 = 3,
            交友 = 4,
            答题 = 6,
            道具1型 = 7,
            道具2型 = 7,
        }

        public enum MissionClass : int
        {
            新手任务 = 0,
            主线任务 = 1,
            支线任务 = 2,
            副本任务 = 3,
            活动任务 = 4,
            系统任务 = 5,
            每日任务 = 6,
            节日任务 = 7,
        }

        // 状态图中行为触发条件
        public enum StateActionTrigger
        {
            OnEnter,    // 进入状态
            OnOption,   // 选择某选项
            OnExit,     // 离开状态
        }

        public enum CounterType : int
        {
            CT_KILL_MONSTER = 1,
            CT_COLLECT = 2,
            CT_KILL_PLAYER = 3,
            CT_MAKE_FRIEND = 4,
            CT_INVALID,
        }

        #region 公有属性定义
        static public DisplayMode MissionDisplayMode
        {
            get
            {
                return m_MissionDisplayMode;
            }
            set
            {
                m_MissionDisplayMode = value;
            }
        }

        static public DisplayMode NPCDisplayMode
        {
            get
            {
                return m_NPCDisplayMode;
            }
            set
            {
                m_NPCDisplayMode = value;
            }
        }

        static public DisplayMode ItemDisplayMode
        {
            get
            {
                return m_ItemDisplayMode;
            }
            set
            {
                m_ItemDisplayMode = value;
            }
        }

        static public int SceneSketchSizeIndex
        {
            get
            {
                return m_iSceneSketchSizeSelectedIdx;
            }
            set
            {
                m_iSceneSketchSizeSelectedIdx = value;
            }
        }

        static public float SceneSketchZoom
        {
            get
            {
                if (m_iSceneSketchSizeSelectedIdx == 0)
                {
                    return 0.5f;
                }
                else
                {
                    return m_iSceneSketchSizeSelectedIdx;
                }
            }
        }

        static public String DataPath
        {
            get
            {
                return "Data";
            }
        }
        static public String MissionPath
        {
            get
            {
                return "Data/Task.xml";
            }
        }
        static public String StateDiagramPath
        {
            get
            {
                return "Data/StateDiagram/";
            }
        }

        static public String SceneSketchPath
        {
            get
            {
                return "Data/SceneSketch/";
            }
        }

        static public String WayInfoPath
        {
            get
            {
                return "Data/SceneSketch/";
            }
        }

        static public String LuaPath
        {
            get
            {
                return "Data/Lua/";
            }
        }

        static public String MissionConditionLuaPath
        {
            get
            {
                return "Data/Client/TaskCondition.lua";
            }
        }

        static public String NPCInfoPath
        {
            get
            {
                return "Data/NPCInfo.xml";
            }
        }

        static public String NPCCreatorInfoPath
        {
            get
            {
                return "Data/npc_createinfo.xml";
            }
        }

        static public String ItemShopPath
        {
            get
            {
                return "Data/npcshop.xml";
            }
        }

        static public String SkillShopPath
        {
            get
            {
                return "Data/skillshop.xml";
            }
        }

        static public String SkillPath
        {
            get
            {
                return "Data/skill.xml";
            }
        }

        static public String ItemPath
        {
            // Item 信息保存在若干文件中,这里只记录 item 文件所在目录
            get
            {
                return "Data/Items";
            }
        }

        static public String NPCPrefix
        {
            get
            {
                return "NPC_";
            }
        }

        static public String MonsterPrefix
        {
            get
            {
                return "MONSTER_";
            }
        }

        static public String ItemPrefix
        {
            get
            {
                return "ITEM_";
            }
        }

        static public String AreaPrefix
        {
            get
            {
                return "AREA_";
            }
        }

        static public String DeathPrefix
        {
            get
            {
                return "DEATH_";
            }
        }

        static public String OptionPath
        {
            get
            {
                return "Data/Options.xml";
            }
        }

        static public String StringResourcePath
        {
            get
            {
                return "Data/StringRes.xml";
            }
        }

        static public String MissionBaseDataPath
        {
            get
            {
                return "Data/TaskBaseData.xml";
            }
        }

        static public String NPCIconPath
        {
            get
            {
                return "Data/SceneSketch/icon_npc.bmp";
            }
        }

        static public String MonsterIconPath
        {
            get
            {
                return "Data/SceneSketch/icon_monster.bmp";
            }
        }

        static public String RegionInfoPath
        {
            get
            {
                return "Data/RegionInfo/";
            }
        }

        static public String RegionDescPath
        {
            get
            {
                return "Data/RegionDesc/";
            }
        }

        static public String PathInfoPath
        {
            get
            {
                return "Data/PathInfo/";
            }
        }

        static public String RegionDescTemplatePath
        {
            get
            {
                return "Data/RegionDescTemplate.xml";
            }
        }

        static public String StateDiagramConvScriptPath
        {
            get
            {
                return "Data/StateDiagramConvertScript";
            }
        }

        static public String ExportPath
        {
            get
            {
                return "Data/Export/";
            }
        }

        static public String ImagePath
        {
            get
            {
                return "Data/Images/";
            }
        }

        static public String PointSoundInfo
        {
            get
            {
                return "Data/PointSoundInfo/";
            }
        }

        static public String ManualScriptPath
        {
            get
            {
                return "Data/ManualScript/";
            }
        }

        static public Color AutoGeneratedStateColor
        {
            get
            {
                return m_AutoGeneratedStateColor;
            }
            set
            {
                m_AutoGeneratedStateColor = value;
            }
        }

        static public Color UserGeneratedStateColor
        {
            get
            {
                return m_UserGeneratedStateColor;
            }
            set
            {
                m_UserGeneratedStateColor = value;
            }
        }

        static public Color AutoGeneratedTranslationColor
        {
            get
            {
                return m_AutoGeneratedTranslationColor;
            }
            set
            {
                m_AutoGeneratedTranslationColor = value;
            }
        }

        static public Color UserGeneratedTranslationColor
        {
            get
            {
                return m_UserGeneratedTranslationColor;
            }
            set
            {
                m_UserGeneratedTranslationColor = value;
            }
        }

        static public Color InvalidateTranslationColor
        {
            get
            {
                return m_InvalidateTranslationColor;
            }
            set
            {
                m_InvalidateTranslationColor = value;
            }
        }

        static public Color InvalidateStateColor
        {
            get
            {
                return m_InvalidateStateColor;
            }
            set
            {
                m_InvalidateStateColor = value;
            }
        }

        static public String DefaultDialogStateName
        {
            get
            {
                return "DEFAULT_DLG";
            }
        }

        static public String IdleStateName
        {
            get
            {
                return "IDLE";
            }
        }

        static public int SingleNpcSize
        {
            get
            {
                return m_iSingleNpcCreatorSize;
            }
            set
            {
                m_iSingleNpcCreatorSize = value;
            }
        }

        static public int NpcMoveRangeSize
        {
            get
            {
                return m_iNpcMoveRangeSize;
            }
            set
            {
                m_iNpcMoveRangeSize = value;
            }
        }

        static public int LevelDifference
        {
            get
            {
                return m_iLevelDifference;
            }
            set
            {
                m_iLevelDifference = value;
            }
        }

        static public float SceneSketchSize
        {
            get
            {
                return m_fSceneSketchSize;
            }
            set
            {
                m_fSceneSketchSize = value;
            }
        }

        static public DataTable MusicTemplate
        {
            get
            {
                return m_MusicTemplate;
            }
            set
            {
                m_MusicTemplate = value;
            }
        }

        static public DataTable SoundTemplate
        {
            get
            {
                return m_SoundTemplate;
            }
            set
            {
                m_SoundTemplate = value;
            }
        }

        static public DataTable EffectTemplate
        {
            get
            {
                return m_EffectTemplate;
            }
            set
            {
                m_EffectTemplate = value;
            }
        }

        static public String TeamWorkServerPath
        {
            get
            {
                return m_strTeamWorkServerPath;
            }
            set
            {
                m_strTeamWorkServerPath = value;
            }
        }

        #endregion

        #region 公有函数定义

        // 将当前用户选项保存到 xml
        static public void SaveToXml(String xmlFilename)
        {
            XmlDocument xmlDoc = new XmlDocument();

            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("Root");
            xmlDoc.AppendChild(root);

            XmlElement elem = xmlDoc.CreateElement("DisplayMode");
            elem.SetAttribute("MissionDisplayMode", ((int)m_MissionDisplayMode).ToString());
            elem.SetAttribute("NPCDisplayMode", ((int)m_NPCDisplayMode).ToString());
            elem.SetAttribute("ItemDisplayMode", ((int)m_ItemDisplayMode).ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("AutoGeneratedStateColor");
            elem.SetAttribute("R", m_AutoGeneratedStateColor.R.ToString());
            elem.SetAttribute("G", m_AutoGeneratedStateColor.G.ToString());
            elem.SetAttribute("B", m_AutoGeneratedStateColor.B.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("UserGeneratedStateColor");
            elem.SetAttribute("R", m_UserGeneratedStateColor.R.ToString());
            elem.SetAttribute("G", m_UserGeneratedStateColor.G.ToString());
            elem.SetAttribute("B", m_UserGeneratedStateColor.B.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("InvalidateStateColor");
            elem.SetAttribute("R", m_InvalidateStateColor.R.ToString());
            elem.SetAttribute("G", m_InvalidateStateColor.G.ToString());
            elem.SetAttribute("B", m_InvalidateStateColor.B.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("AutoGeneratedTranslationColor");
            elem.SetAttribute("R", m_AutoGeneratedTranslationColor.R.ToString());
            elem.SetAttribute("G", m_AutoGeneratedTranslationColor.G.ToString());
            elem.SetAttribute("B", m_AutoGeneratedTranslationColor.B.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("UserGeneratedTranslationColor");
            elem.SetAttribute("R", m_UserGeneratedTranslationColor.R.ToString());
            elem.SetAttribute("G", m_UserGeneratedTranslationColor.G.ToString());
            elem.SetAttribute("B", m_UserGeneratedTranslationColor.B.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("InvalidateTranslationColor");
            elem.SetAttribute("R", m_InvalidateTranslationColor.R.ToString());
            elem.SetAttribute("G", m_InvalidateTranslationColor.G.ToString());
            elem.SetAttribute("B", m_InvalidateTranslationColor.B.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("SceneSketchSizeIdx");
            elem.SetAttribute("SizeIdx", m_iSceneSketchSizeSelectedIdx.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("NpcCreatorSize");
            elem.SetAttribute("SingleNpcSize", m_iSingleNpcCreatorSize.ToString());
            elem.SetAttribute("NpcMoveRangeSize", m_iNpcMoveRangeSize.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("LevelDifference");
            elem.SetAttribute("LevelDifference", m_iLevelDifference.ToString());
            root.AppendChild(elem);

            elem = xmlDoc.CreateElement("TeamWork");
            elem.SetAttribute("ServerPath", m_strTeamWorkServerPath);
            root.AppendChild(elem);

            xmlDoc.Save(OptionPath);

            return;
        }

        // 从 xml 载入用户选项
        static public void LoadFromXml(String xmlFilename)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(OptionPath);

            XmlElement root = xmlDoc.LastChild as XmlElement;
            XmlElement elmt = root.FirstChild as XmlElement;

            while (elmt != null)
            {
                if (elmt.Name.Equals("DisplayMode"))
                {
                    m_MissionDisplayMode = (DisplayMode)int.Parse(elmt.GetAttribute("MissionDisplayMode"));
                    m_NPCDisplayMode = (DisplayMode)int.Parse(elmt.GetAttribute("NPCDisplayMode"));
                    m_ItemDisplayMode = (DisplayMode)int.Parse(elmt.GetAttribute("ItemDisplayMode"));
                }
                else if (elmt.Name.Equals("AutoGeneratedStateColor"))
                {
                    m_AutoGeneratedStateColor = Color.FromArgb(byte.Parse(elmt.GetAttribute("R")),
                                                   byte.Parse(elmt.GetAttribute("G")),
                                                   byte.Parse(elmt.GetAttribute("B")));
                }
                else if (elmt.Name.Equals("UserGeneratedStateColor"))
                {
                    m_UserGeneratedStateColor = Color.FromArgb(byte.Parse(elmt.GetAttribute("R")),
                                                   byte.Parse(elmt.GetAttribute("G")),
                                                   byte.Parse(elmt.GetAttribute("B")));
                }
                else if (elmt.Name.Equals("InvalidateStateColor"))
                {
                    m_InvalidateStateColor = Color.FromArgb(byte.Parse(elmt.GetAttribute("R")),
                                                   byte.Parse(elmt.GetAttribute("G")),
                                                   byte.Parse(elmt.GetAttribute("B")));
                }
                else if (elmt.Name.Equals("AutoGeneratedTranslationColor"))
                {
                    m_AutoGeneratedTranslationColor = Color.FromArgb(byte.Parse(elmt.GetAttribute("R")),
                                                   byte.Parse(elmt.GetAttribute("G")),
                                                   byte.Parse(elmt.GetAttribute("B")));
                }
                else if (elmt.Name.Equals("UserGeneratedTranslationColor"))
                {
                    m_UserGeneratedTranslationColor = Color.FromArgb(byte.Parse(elmt.GetAttribute("R")),
                                                   byte.Parse(elmt.GetAttribute("G")),
                                                   byte.Parse(elmt.GetAttribute("B")));
                }
                else if (elmt.Name.Equals("InvalidateTranslationColor"))
                {
                    m_InvalidateTranslationColor = Color.FromArgb(byte.Parse(elmt.GetAttribute("R")),
                                                   byte.Parse(elmt.GetAttribute("G")),
                                                   byte.Parse(elmt.GetAttribute("B")));
                }
                else if (elmt.Name.Equals("SceneSketchSizeIdx"))
                {
                    m_iSceneSketchSizeSelectedIdx = int.Parse(elmt.GetAttribute("SizeIdx"));
                }
                else if (elmt.Name.Equals("NpcCreatorSize"))
                {
                    m_iSingleNpcCreatorSize = int.Parse(elmt.GetAttribute("SingleNpcSize"));
                    if(elmt.HasAttribute("NpcMoveRangeSize"))
                    {
                        m_iNpcMoveRangeSize = int.Parse(elmt.GetAttribute("NpcMoveRangeSize"));
                    }
                }
                else if (elmt.Name.Equals("LevelDifference"))
                {
                    m_iLevelDifference = int.Parse(elmt.GetAttribute("LevelDifference"));
                }
                else if(elmt.Name.Equals("TeamWork"))
                {
                    m_strTeamWorkServerPath = elmt.GetAttribute("ServerPath");
                }
                elmt = elmt.NextSibling as XmlElement;
            }

            // 加载RegionDesc模板文件
            LoadRegionDescTemplate();
        }

        // 加载RegionDesc模板文件
        static public void LoadRegionDescTemplate()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(RegionDescTemplatePath);

            XmlElement root = xmlDoc.LastChild as XmlElement;
            XmlElement elmt = root.FirstChild as XmlElement;

            while(elmt != null)
            {
                if(elmt.Name.Equals("Music"))
                {
                    // 将Index作为表名
                    m_MusicTemplate.TableName = elmt.GetAttribute("Index");

                    // 设置列名
                    DataColumn name = new DataColumn("属性", System.Type.GetType("System.String"));
                    DataColumn value = new DataColumn("值", System.Type.GetType("System.String"));
                    m_MusicTemplate.Columns.Add(name);
                    m_MusicTemplate.Columns.Add(value);

                    XmlElement prop = elmt.FirstChild as XmlElement;
                    while(prop != null)
                    {
                        String strName = prop.GetAttribute("Name");
                        String strValue = prop.GetAttribute("Value");

                        DataRow dr = m_MusicTemplate.NewRow();
                        dr["属性"] = strName;
                        dr["值"] = strValue;
                        m_MusicTemplate.Rows.Add(dr);

                        prop = prop.NextSibling as XmlElement;
                    }
                }
                else if (elmt.Name.Equals("Sound"))
                {
                    // 将Index作为表名
                    m_SoundTemplate.TableName = elmt.GetAttribute("Index");

                    // 设置列名
                    DataColumn name = new DataColumn("属性", System.Type.GetType("System.String"));
                    DataColumn value = new DataColumn("值", System.Type.GetType("System.String"));
                    m_SoundTemplate.Columns.Add(name);
                    m_SoundTemplate.Columns.Add(value);

                    XmlElement prop = elmt.FirstChild as XmlElement;
                    while (prop != null)
                    {
                        String strName = prop.GetAttribute("Name");
                        String strValue = prop.GetAttribute("Value");

                        DataRow dr = m_SoundTemplate.NewRow();
                        dr["属性"] = strName;
                        dr["值"] = strValue;
                        m_SoundTemplate.Rows.Add(dr);

                        prop = prop.NextSibling as XmlElement;
                    }
                }
                else if (elmt.Name.Equals("Effect"))
                {
                    // 将Index作为表名
                    m_EffectTemplate.TableName = elmt.GetAttribute("Index");

                    // 设置列名
                    DataColumn name = new DataColumn("属性", System.Type.GetType("System.String"));
                    DataColumn value = new DataColumn("值", System.Type.GetType("System.String"));
                    m_EffectTemplate.Columns.Add(name);
                    m_EffectTemplate.Columns.Add(value);

                    XmlElement prop = elmt.FirstChild as XmlElement;
                    while (prop != null)
                    {
                        String strName = prop.GetAttribute("Name");
                        String strValue = prop.GetAttribute("Value");

                        DataRow dr = m_EffectTemplate.NewRow();
                        dr["属性"] = strName;
                        dr["值"] = strValue;
                        m_EffectTemplate.Rows.Add(dr);

                        prop = prop.NextSibling as XmlElement;
                    }
                }

                elmt = elmt.NextSibling as XmlElement;
            }
        }

        #endregion

        #region 私有成员定义

        private static DisplayMode m_MissionDisplayMode = DisplayMode.DM_ID;   // 任务的显示模式
        private static DisplayMode m_NPCDisplayMode = DisplayMode.DM_ID;       // NPC 显示模式
        private static DisplayMode m_ItemDisplayMode = DisplayMode.DM_ID;      // 道具显示模式

        private static Color m_AutoGeneratedStateColor = new Color();   // 自动创建的状态颜色
        private static Color m_UserGeneratedStateColor = new Color();   // 用户创建的状态的颜色
        private static Color m_InvalidateStateColor = new Color();      // 无效状态颜色

        private static Color m_AutoGeneratedTranslationColor = new Color(); // 自动创建的转换的颜色
        private static Color m_UserGeneratedTranslationColor = new Color(); // 用户创建的转换的颜色
        private static Color m_InvalidateTranslationColor = new Color();

        private static int m_iSceneSketchSizeSelectedIdx = 0;    // 场景草图尺寸 combobox 选项 id

        private static int m_iSingleNpcCreatorSize = 4;     // 单点NPC的尺寸大小
        private static int m_iNpcMoveRangeSize = 2;         // 怪物寻路范围默认值

        private static int m_iLevelDifference = 5;          // 玩家可接任务的与任务等级的最大等级差

        private static float m_fSceneSketchSize = 1024.0f;    // 场景草图大小

        private static DataTable m_MusicTemplate = new DataTable(); // 音乐属性的模板
        private static DataTable m_SoundTemplate = new DataTable(); // 音效属性的模板
        private static DataTable m_EffectTemplate = new DataTable(); // 特效属性的模板

        private static String m_strTeamWorkServerPath = "";     // 多人协作模式，服务器路径
        #endregion
    }
}
