﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;

namespace MissionEditor
{
    public class Item
    {
        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        public String Name
        {
            get
            {
                return m_strName;
            }
            set
            {
                m_strName = value;
            }
        }

        public int ScriptID
        {
            get
            {
                return m_iScriptID;
            }
            set
            {
                m_iScriptID = value;
            }
        }

        public int Price
        {
            get
            {
                return m_iPrice;
            }
            set
            {
                m_iPrice = value;
            }
        }

        private int m_iID;
        private String m_strName;
        private int m_iPrice;
        private int m_iScriptID;    // 脚本 ID
    };

    // 读入 Item 数据并维护
    public class ItemManager
    {
        private ItemManager()
        {
        }

        public void LoadItemInfoFromXml(String strItemPath)
        {
            m_ItemList.Clear();
            // 遍历 strItemPath 下的所有文件
            String[] fileList = System.IO.Directory.GetFileSystemEntries(strItemPath);
             foreach (String strFilePath in fileList)
             {
                 String strExtension = System.IO.Path.GetExtension(strFilePath);
                 if (strExtension.CompareTo(".xml") == 0)
                 {
                     // 载入该 xml
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(strFilePath);

                    XmlElement elmtItemRoot = xmlDoc.LastChild as XmlElement;
                    XmlElement elmtItem = elmtItemRoot.FirstChild as XmlElement;
                    // 遍历所有 Item
                    while (elmtItem != null)
                    {
                        Item item = new Item();
                        item.ID = int.Parse(elmtItem.GetAttribute("ItemID"));
                        item.Name = elmtItem.GetAttribute("Name");
                        String strPrice = elmtItem.GetAttribute("Price");

                        if (strPrice==null || strPrice.Equals(""))
                        {
                            item.Price = 0;
                        }
                        else
                        {                        
                            try
                            {
                                item.Price = int.Parse(strPrice);
                            }
                            catch(Exception)
                            {
                                item.Price = 0;
                            }
                        }

                        String strTaskScript = elmtItem.GetAttribute("TaskScript");
                        if (strTaskScript!=null && strTaskScript.CompareTo("")!=0)
                        {
                            item.ScriptID = int.Parse(strTaskScript);
                        }
                        else
                        {
                            item.ScriptID = -1;
                        }

                        m_ItemList.Add(item);
                        elmtItem = elmtItem.NextSibling as XmlElement;
                    }
                 }
             }
        }

        public int GetItemIDByName(String strItemName)
        {
            foreach (Item item in m_ItemList)
            {
                if (item.Name.Equals(strItemName))
                {
                    return item.ID;
                }
            }
            return -1;
        }

        public Item GetItemByID(int iID)
        {
            foreach (Item item in m_ItemList)
            {
                if (item.ID == iID)
                {
                    return item;
                }
            }
            return null;
        }

        // 获取一个 Item 在 m_ItemList 中排第几个
        public int GetIndexByID(int iID)
        {
            for (int i = 0; i < m_ItemList.Count; i++)
            {
                if ((m_ItemList[i] as Item).ID == iID)
                {
                    return i;
                }
            }

            return -1;
        }

        public Hashtable ItemFilter(String strItemName)
        {
            // 创建结果数据集
            Hashtable result = new Hashtable();

            // 遍历筛选
            foreach(Item item in m_ItemList)
            {
                int iItemType = int.Parse(item.ID.ToString().Substring(0, 2));
                //if(iItemType == 38 || iItemType == 39)
                //{
                    if (strItemName.Length == 0 || item.Name.Contains(strItemName) || item.ID.ToString().Contains(strItemName))
                    {
                        result[item.ID] = item;
                    }
                //}
            }

            // 返回结果
            return result;
        }

        public ArrayList ItemList
        {
            get
            {
                return m_ItemList;
            }
        }

        static public ItemManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new ItemManager();
                }
                return ms_Instance;
            }
        }

        private ArrayList m_ItemList = new ArrayList();
        static private ItemManager ms_Instance = null;
    }


}
