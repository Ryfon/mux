﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;

namespace MissionEditor
{
    // 任务管理器
    public class MissionManager
    {
        #region  成员函数定义
        public Mission FindByID(int iID)
        {
            foreach (Mission mission in m_MissionList)
            {
                if (mission.ID == iID)
                {
                    return mission;
                }
            }
            return null;
        }

        public Mission FindByName(String strName)
        {
            foreach (Mission mission in m_MissionList)
            {
                if (mission.Name.Equals(strName))
                {
                    return mission;
                }
            }
            return null;
        }

        public int GetClassIDByName(String strClassName)
        {
            foreach (int iKey in m_MissionClasses.Keys)
            {
                String strValue = m_MissionClasses[iKey] as String;
                if (strValue.Equals(strClassName))
                {
                    return iKey;
                }
            }

            return -1;
        }

        public ArrayList Filter(String strMapName, String strClassName, String strTaskName)
        {
            // 建立结果数据集
            ArrayList result = new ArrayList();

            // 通过名字获取ID
            int iMapID = SceneSketchManager.Instance.GetSceneIDByName(strMapName);
            int iClassID = -1;
            iClassID = GetClassIDByName(strClassName);

            // 开始筛选
            foreach(Mission mission in m_MissionList)
            {
                if(iMapID == -1 || mission.LeadMap == iMapID)
                {
                    if(iClassID == -1 || mission.Class == iClassID)
                    {
                        if (strTaskName.Length == 0 || mission.Name.Contains(strTaskName) || mission.ID.ToString().Contains(strTaskName))
                        {
                            result.Add(mission);
                        }
                    }
                }
            }

            return result;
        }
        public bool SaveMissions(String strXmlFilename)
        {
            m_Mission2NPCList.Clear();
            m_Mission2ItemList.Clear();
            m_Mission2AreaList.Clear();

            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", null);
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("Root");
            xmlDoc.AppendChild(root);

            foreach(Mission mission in m_MissionList)
            {

                XmlElement elem = xmlDoc.CreateElement("Task");
                /*elem.SetAttribute("ID", mission.ID.ToString());
                elem.SetAttribute("Name", mission.Name);
                elem.SetAttribute("Desc", "");
                //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
                //elem.SetAttribute("SimpleDesc", mission.SimpleDesc);    // 任务的简单描述
                elem.SetAttribute("LeadMap", mission.LeadMap.ToString());          // 任务所属的地图
                elem.SetAttribute("StarLevel", mission.StarLevel.ToString()); // 任务的难度（星级）
                elem.SetAttribute("TimeLimit", mission.TimeLimit.ToString());// 任务的时间限制
                //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//

                elem.SetAttribute("Type", mission.Type);
                // 2009-01-08 徐磊 增加任务流程分类
                elem.SetAttribute("FlowType", mission.FlowType);
                // 2009-01-08 徐磊 增加任务流程分类
                // 将 MissionType 解析成整数
                OptionManager.MissionType missionType = (OptionManager.MissionType)Enum.Parse(typeof(OptionManager.MissionType), mission.Type) ;
                elem.SetAttribute("IntType", ((int)missionType).ToString());
                elem.SetAttribute("Class", mission.Class.ToString());
                elem.SetAttribute("Level", mission.Level.ToString());
                elem.SetAttribute("RecordReserve", mission.RecordReserve.ToString());
                elem.SetAttribute("SubTaskType", mission.SubMissionType);
                // 将 SubType 解析成整数
                int iIntSubType = -1;
                if (missionType == OptionManager.MissionType.对话类)
                {
                    OptionManager.MissionTypeDialogSubType subType = (OptionManager.MissionTypeDialogSubType)Enum.Parse(typeof(OptionManager.MissionTypeDialogSubType), mission.SubMissionType);
                    iIntSubType = (int)subType;
                }
                else if (missionType == OptionManager.MissionType.计数器类)
                {
                    OptionManager.MissionTypeCounterSubType subType = (OptionManager.MissionTypeCounterSubType)Enum.Parse(typeof(OptionManager.MissionTypeCounterSubType), mission.SubMissionType);
                    iIntSubType = (int)subType;
                }
                elem.SetAttribute("IntSubTaskType", iIntSubType.ToString());
                elem.SetAttribute("ItemProvider", mission.ItemProvider.ToString());
                elem.SetAttribute("RegionProvider", mission.RegionProvider.ToString()); // 2009-01-19 徐磊 增加区域触发任务
                elem.SetAttribute("Provider", mission.Provider.ToString());
                elem.SetAttribute("SubmitTo", mission.SubmitTo.ToString());

                // 任务接受条件
                foreach (MissionCondition condition in mission.Conditions)
                {
                    XmlElement xmlElemCondition = xmlDoc.CreateElement("TaskCondition");
                    xmlElemCondition.SetAttribute("Condition", condition.Condition);
                    xmlElemCondition.SetAttribute("Value", condition.Value);

                    elem.AppendChild(xmlElemCondition);
                }

                // 固定任务奖励
                foreach (MissionAward award in mission.FixedAwards)
                {
                    XmlElement xmlElemAward = xmlDoc.CreateElement("FixedAwards");
                    xmlElemAward.SetAttribute("Award", award.Award);
                    xmlElemAward.SetAttribute("Value", award.Value);

                    elem.AppendChild(xmlElemAward);
                }

                // 可选任务奖励
                foreach (MissionAward award in mission.SelectedAwards)
                {
                    XmlElement xmlElemAward = xmlDoc.CreateElement("SelectedAwards");
                    xmlElemAward.SetAttribute("Award", award.Award);
                    xmlElemAward.SetAttribute("Value", award.Value);

                    elem.AppendChild(xmlElemAward);
                }

                elem.SetAttribute("TargetNPC", mission.TargetNPC.ToString());
                elem.SetAttribute("TargetMonsterBegin", mission.TargetMonsterBegin.ToString());
                elem.SetAttribute("TargetMonsterEnd", mission.TargetMonsterEnd.ToString());
                elem.SetAttribute("TargetItemBegin", mission.TargetItemBegin.ToString());
                elem.SetAttribute("TargetItemEnd", mission.TargetItemEnd.ToString());
                elem.SetAttribute("TargetArea", mission.TargetArea.ToString());
                elem.SetAttribute("TargetProperty", mission.TargetProperty.ToString());
                elem.SetAttribute("TargetValue", mission.TargetValue.ToString());
                elem.SetAttribute("TrackName", mission.TrackName);
                // 目标区域
                // ...

                elem.SetAttribute("Share", mission.Share.ToString());
                elem.SetAttribute("Repeat", mission.Repeat.ToString());
                elem.SetAttribute("GiveUp", mission.GiveUp.ToString());

                mission.GetDescription.Replace("\0", "");
                mission.SubMissionType.Replace("\0", "");

                XmlCDataSection elemCData = xmlDoc.CreateCDataSection(mission.GetDescription);
                elem.AppendChild(elemCData);
                elemCData = xmlDoc.CreateCDataSection(mission.SubmitDescription);
                elem.AppendChild(elemCData);
                elemCData = xmlDoc.CreateCDataSection(mission.SimpleDesc);
                elem.AppendChild(elemCData);*/

                mission.SaveToXmlNode(xmlDoc, elem);

                //// 目标道具列表
                //foreach (int iItemID in mission.TargetItemList.Keys)
                //{
                //    XmlElement elemTargetItem = xmlDoc.CreateElement("TargetItem");
                //    elemTargetItem.SetAttribute("ItemID", iItemID.ToString());
                //    elemTargetItem.SetAttribute("Value", mission.TargetItemList[iItemID].ToString());
                //    elem.AppendChild(elemTargetItem);
                //}

                //// 目标怪物列表
                //foreach (int iMonsterID in mission.TargetMonsterList.Keys)
                //{
                //    XmlElement elemTargetMonster = xmlDoc.CreateElement("TargetMonster");
                //    elemTargetMonster.SetAttribute("MonsterID", iMonsterID.ToString());
                //    elemTargetMonster.SetAttribute("Value", mission.TargetMonsterList[iMonsterID].ToString());
                //    elem.AppendChild(elemTargetMonster);
                //}

                // 填充与任务相关 NPC Item 列表
                ArrayList relatedNpcList = new ArrayList();
                ArrayList relatedItemList = new ArrayList();
                ArrayList relatedAreaList = new ArrayList();

                if (mission.ItemProvider)
                {
                    relatedItemList.Add(mission.Provider);
                }
                else if(mission.RegionProvider)
                {
                    relatedAreaList.Add(mission.Provider);
                }
                else
                {
                    relatedNpcList.Add(mission.Provider);
                }

               // relatedNpcList.Add(mission.Provider);
                relatedNpcList.Add(mission.SubmitTo);
                if (mission.TargetNPC >= 0)
                {
                    relatedNpcList.Add(mission.TargetNPC);
                }

                if (mission.TargetArea >= 0)
                {
                    relatedAreaList.Add(mission.TargetArea);
                }
                //if (mission.TargetMonsterBegin > 0)
                //{
                //    relatedNpcList.Add(mission.TargetMonsterBegin);
                //}
                //if (mission.TargetMonsterEnd > 0)
                //{
                //    relatedNpcList.Add(mission.TargetMonsterEnd);
                //}

                //if (mission.TargetItemBegin > 0)
                //{
                //    relatedItemList.Add(mission.TargetItemBegin);
                //}

                //if (mission.TargetItemEnd > 0)
                //{
                //    relatedItemList.Add(mission.TargetItemEnd);
                //}

                m_Mission2NPCList[mission.ID] = relatedNpcList;
                m_Mission2ItemList[mission.ID] = relatedItemList;
                m_Mission2AreaList[mission.ID] = relatedAreaList;

                root.AppendChild(elem);
            }
            xmlDoc.Save(strXmlFilename);
        
            return true;
        }

        private void LoadBanList(String strXmlFilename)
        {
            // 清空旧的数据
            m_BanList.Clear();

            // 读xml
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(strXmlFilename);
                if (xmlDoc != null)
                {
                    XmlElement root = xmlDoc.LastChild as XmlElement;
                    if (root != null)
                    {
                        foreach (XmlElement xmlElem in root.ChildNodes)
                        {
                            m_BanList.Add(int.Parse(xmlElem.GetAttribute("ID")));
                        }
                    }
                }
            }
            catch(Exception)
            {

            }           
        }

        public bool LoadMissions(String strXmlFilename)
        {
            // 载入任务阶级列表
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(OptionManager.MissionBaseDataPath);
            XmlElement root = xmlDoc.LastChild as XmlElement;
            foreach (XmlElement xmlElem in root.ChildNodes)
            {
                if (xmlElem.Name.Equals("MissionClasses"))
                {
                    m_MissionClasses.Clear();
                    foreach (XmlElement elmtClass in xmlElem.ChildNodes)
                    {
                        String strDisplay = elmtClass.GetAttribute("Display");
                        int iValue = int.Parse(elmtClass.GetAttribute("Value"));
                        m_MissionClasses[iValue] = strDisplay;
                    }
                }
            }
            
            // 加入被屏蔽的task
            LoadBanList(OptionManager.DataPath + "/banlist.xml");

            // 载入所有任务
            xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);
            
            XmlDeclaration xmlDec = xmlDoc.FirstChild as XmlDeclaration;
            root = xmlDec.NextSibling as XmlElement;

            if (root == null)
            {
                return false;
            }

            m_MissionList.Clear();
            foreach (XmlElement xmlElem in root.ChildNodes)
            {
                try
                {
                    // <Mission ID="1" Name="11" Desc="111" Type="计数器类" SubMissionType="杀人" 
                    // Provider="33554440" SubmitTo="-1" TargetNPC="-1" TargetMonster="-1" TargetItem="-1" 
                    // TargetProperty="-1" TargetValue="-1" Share="False" Repeat="False" GiveUp="False" Order="False" />

                    Mission mission = new Mission();
                    /*mission.ID = int.Parse(xmlElem.GetAttribute("ID"));
                    mission.Name = xmlElem.GetAttribute("Name");
                    //mission.GetDescription = xmlElem.GetAttribute("Desc");
                    mission.Type = xmlElem.GetAttribute("Type");
                    //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
                    int iTryTemp = 0;
                    if(xmlElem.HasAttribute("SimpleDesc"))
                    {
                        mission.SimpleDesc = xmlElem.GetAttribute("SimpleDesc") ;    // 任务的简单描述
                    }
                    if (xmlElem.HasAttribute("LeadMap"))
                    {

                        if(int.TryParse(xmlElem.GetAttribute("LeadMap"), out iTryTemp))
                        {
                            mission.LeadMap = iTryTemp;          // 任务所属的地图
                        }
                    }
                    if (xmlElem.HasAttribute("StarLevel"))
                    {
                        if (int.TryParse(xmlElem.GetAttribute("StarLevel"), out iTryTemp))
                        {
                            mission.StarLevel = iTryTemp; // 任务的难度（星级）
                        }
                    }
                    if (xmlElem.HasAttribute("TimeLimit"))
                    {
                        if (int.TryParse(xmlElem.GetAttribute("TimeLimit"), out iTryTemp))
                        {
                            mission.TimeLimit = iTryTemp;// 任务的时间限制
                        }
                    }
                    //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//

                    
                    try
                    {
                        mission.Class = int.Parse(xmlElem.GetAttribute("Class"));
                        mission.Level = int.Parse(xmlElem.GetAttribute("Level"));
                    }
                    catch(Exception)
                    {
                        mission.Class = 0;
                        mission.Level = 1;
                    }

                    try
                    {
                        mission.RecordReserve = int.Parse(xmlElem.GetAttribute("RecordReserve"));
                    }
                    catch (Exception)
                    {
                        mission.RecordReserve = 0;
                    }

                    // 2009-01-08 徐磊 增加任务流程分类
                    if(xmlElem.HasAttribute("FlowType"))
                    {
                        mission.FlowType = xmlElem.GetAttribute("FlowType");
                    }
                    // 2009-01-08 徐磊 增加任务流程分类

                    // 2009-01-19 徐磊 增加区域触发任务
                    if(xmlElem.HasAttribute("RegionProvider"))
                    {
                        mission.RegionProvider = bool.Parse(xmlElem.GetAttribute("RegionProvider"));
                    }
                    // 2009-01-19 徐磊 增加区域触发任务

                    mission.SubMissionType = xmlElem.GetAttribute("SubTaskType");
                    mission.ItemProvider = bool.Parse(xmlElem.GetAttribute("ItemProvider"));
                    mission.Provider = int.Parse(xmlElem.GetAttribute("Provider"));
                    mission.SubmitTo = int.Parse(xmlElem.GetAttribute("SubmitTo"));
                    mission.TargetNPC = int.Parse(xmlElem.GetAttribute("TargetNPC"));
                    mission.TargetMonsterBegin = int.Parse(xmlElem.GetAttribute("TargetMonsterBegin"));
                    mission.TargetMonsterEnd = int.Parse(xmlElem.GetAttribute("TargetMonsterEnd"));
                    mission.TargetItemBegin = int.Parse(xmlElem.GetAttribute("TargetItemBegin"));
                    mission.TargetItemEnd = int.Parse(xmlElem.GetAttribute("TargetItemEnd"));
                    // 2009-01-20 徐磊 增加区域探索类任务
                    if(xmlElem.HasAttribute("TargetArea"))
                    {
                        mission.TargetArea = int.Parse(xmlElem.GetAttribute("TargetArea"));
                    }
                    // 2009-01-20 徐磊 增加区域探索类任务
                    mission.TargetProperty = int.Parse(xmlElem.GetAttribute("TargetProperty"));
                    mission.TargetValue = int.Parse(xmlElem.GetAttribute("TargetValue"));
                    try
                    {
                        mission.TrackName = xmlElem.GetAttribute("TrackName");
                    }
                    catch (Exception)
                    {
                        mission.TrackName = "";
                    }

                    mission.Share = bool.Parse(xmlElem.GetAttribute("Share"));
                    mission.Repeat = bool.Parse(xmlElem.GetAttribute("Repeat"));
                    mission.GiveUp = bool.Parse(xmlElem.GetAttribute("GiveUp"));

                    XmlNode xmlNode = xmlElem.FirstChild;
                    int iNumCData = 0;
                    while (xmlNode != null)
                    {
                        if (xmlNode is XmlElement)
                        {
                            XmlElement xmlElemSub =  xmlNode as XmlElement;
                            if (xmlElemSub.Name.Equals("TaskCondition"))
                            {
                                String strCondition = xmlElemSub.GetAttribute("Condition");
                                String strValue = xmlElemSub.GetAttribute("Value");
                                mission.Conditions.Add(new MissionCondition(strCondition, strValue));
                            }
                            else if (xmlElemSub.Name.Equals("FixedAwards"))
                            {
                                String strAward = xmlElemSub.GetAttribute("Award");
                                String strValue = xmlElemSub.GetAttribute("Value");
                                mission.FixedAwards.Add(new MissionAward(strAward, strValue));
                            }
                            else if (xmlElemSub.Name.Equals("SelectedAwards"))
                            {
                                String strAward = xmlElemSub.GetAttribute("Award");
                                String strValue = xmlElemSub.GetAttribute("Value");
                                mission.SelectedAwards.Add(new MissionAward(strAward, strValue));
                            }
                        }
                        else if (xmlNode is XmlCDataSection)
                        {

                            XmlCDataSection elmtCData = xmlNode as XmlCDataSection;
                            if (iNumCData == 0)
                            {
                                //Encoding encGB = Encoding.GetEncoding("GB2312");
                                //byte[] asciiBytes = System.Convert.FromBase64String(elmtCData.Data);
                                //char[] asciiChars = new char[encGB.GetCharCount(asciiBytes, 0, asciiBytes.Length)];

                                //encGB.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
                                //mission.GetDescription = new String(asciiChars);
                                mission.GetDescription = elmtCData.Data;
                                iNumCData++;
                            }
                            else if (iNumCData == 1)
                            {
                                mission.SubmitDescription = elmtCData.Data;
                                iNumCData++;
                            }
                            else if(iNumCData == 2)
                            {
                                mission.SimpleDesc = elmtCData.Data;
                                iNumCData++;
                            }
                            else
                            {

                            }
 
                        }
                        xmlNode = xmlNode.NextSibling;
                    }*/

                    mission.LoadFromXmlNode(xmlElem);

                    m_MissionList.Add(mission);

                    ArrayList relatedNpcList = new ArrayList();
                    ArrayList relatedItemList = new ArrayList();
                    ArrayList relatedAreaList = new ArrayList();

                    if (mission.ItemProvider)
                    {
                        relatedItemList.Add(mission.Provider);
                    }
                    else if(mission.RegionProvider)
                    {
                        relatedAreaList.Add(mission.Provider);
                    }
                    else
                    {
                        relatedNpcList.Add(mission.Provider);
                    }

                    relatedNpcList.Add(mission.SubmitTo);
                    if (mission.TargetNPC >= 0)
                    {
                        relatedNpcList.Add(mission.TargetNPC);
                    }
                    if (mission.TargetArea >= 0)
                    {
                        relatedAreaList.Add(mission.TargetArea);
                    }

                    m_Mission2NPCList[mission.ID] = relatedNpcList;
                    m_Mission2ItemList[mission.ID] = relatedItemList;
                    m_Mission2AreaList[mission.ID] = relatedAreaList;
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.ToString());
                    continue;
                }

            }

            return true;
        }

        // 删除任务
        public void RemoveMission(Mission mission)
        {
            m_MissionList.Remove(mission);
            m_Mission2NPCList.Remove(mission.ID);
            m_Mission2ItemList.Remove(mission.ID);
            m_Mission2AreaList.Remove(mission.ID);
        }

        // 从外部文件导入
        public void LoadFromExternal(String strFileName)
        {
            // 加载文件
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strFileName);

            //获取root节点
            XmlElement xmlRoot = xmlDoc.LastChild as XmlElement;
            if(xmlRoot != null)
            {
                // 开始遍历节点
                XmlElement xmlElmt = xmlRoot.FirstChild as XmlElement;
                while(xmlElmt != null)
                {
                    // 设置当前节点
                    m_CurrXmlElmt = xmlElmt;

                    try
                    {
                        // 读取属性确定当前任务
                        int iTaskID = int.Parse(xmlElmt.GetAttribute("ID"));
                        m_CurrMission = FindByID(iTaskID);
                        if(m_CurrMission != null)
                        {
                            // 清空旧的任务奖励
                            m_CurrMission.FixedAwards.Clear();

                            // 设置任务的属性
                            String strExp = xmlElmt.GetAttribute("经验");
                            if (!strExp.Equals(""))
                            {
                                MissionAward ma = new MissionAward("AddExp", strExp);
                                m_CurrMission.FixedAwards.Add(ma);
                            }

                            String strMoney = xmlElmt.GetAttribute("金钱");
                            if(!strMoney.Equals(""))
                            {
                                MissionAward ma = new MissionAward("AddMoney", strMoney);
                                m_CurrMission.FixedAwards.Add(ma);
                            }

                            String strChosenItems = xmlElmt.GetAttribute("可选道具");
                            if(!strChosenItems.Equals(""))
                            {
                                MissionAward ma = new MissionAward("GiveItemFrom", strChosenItems);
                                m_CurrMission.FixedAwards.Add(ma);
                            }

                            String strFixedItems = xmlElmt.GetAttribute("固定道具");
                            String strFixedItemNum = xmlElmt.GetAttribute("固定道具数量");
                            if(!strFixedItems.Equals("") && !strFixedItemNum.Equals(""))
                            {
                                int iItemStart = 0;
                                int iNumStart = 0;
                                int iItemEnd = strFixedItems.Length - 1;
                                int iNumEnd = strFixedItemNum.Length - 1;
                                
                                while(iItemEnd != -1 && iNumEnd != -1)
                                {
                                    iItemEnd = strFixedItems.IndexOf(",", iItemStart);
                                    iNumEnd = strFixedItemNum.IndexOf(",", iNumStart);

                                    String strItem = "";
                                    String strNum = "";

                                    if(iItemEnd != -1)
                                    {
                                        strItem = strFixedItems.Substring(iItemStart, iItemEnd - iItemStart);
                                        iItemStart = iItemEnd + 1;
                                    }
                                    else
                                    {
                                        strItem = strFixedItems.Substring(iItemStart, strFixedItems.Length - iItemStart);
                                    }

                                    if(iNumEnd != -1)
                                    {
                                        strNum = strFixedItemNum.Substring(iNumStart, iNumEnd - iNumStart);
                                        iNumStart = iNumEnd + 1;
                                    }
                                    else
                                    {
                                        strNum = strFixedItemNum.Substring(iNumStart, strFixedItemNum.Length - iNumStart);
                                    }

                                    MissionAward ma = new MissionAward("GiveItem", strItem + "," + strNum);
                                    m_CurrMission.FixedAwards.Add(ma);
                                }
                                
                            }
                        }
                    }
                    catch(Exception XmlExp)
                    {
                        MessageBox.Show(XmlExp.Message);
                    }

                    // 继续下一个节点
                    xmlElmt = xmlElmt.NextSibling as XmlElement;
                }
            }
        }

        // 通过TaskID获取需要更新的任务存盘栏位文件
        public ArrayList GetIndexFileNameByTaskID(int iTaskID)
        {
            // 转化为String
            String strTaskID = iTaskID.ToString();

            // ID不是7位的，直接返回null
            if(strTaskID.Length != 7)
            {
                return null;
            }

            // 结果集合
            ArrayList resultList = new ArrayList();

            // 初始化列表，36张表
            ArrayList FileNameList = new ArrayList();
            for(int gender = 1;gender <= 2;gender++)
            {
                for (int race = 1; race <= 3; race++)
                {
                    for (int job = 1; job <= 6; job++)
                    {
                        String strFileName = gender.ToString() + "_" + race.ToString() + "_" + job.ToString();
                        FileNameList.Add(strFileName);
                    }
                }
            }

            // 获取需要的信息
            Char cFirst = strTaskID[0];
            Char cSecond = strTaskID[2];
            
            // 根据信息，计算出文件名正则表达式 ---》性别_种族_职业.xml
            String strRegex = "[0-9]_[0-9]_[0-9]";
            if (cSecond.Equals('9'))//通用型
            {
                strRegex = "[0-9]_[0-9]_[0-9]";
            }
            else
            {
                if (cFirst.Equals('8'))//职业
                {
                    strRegex = "[0-9]_[0-9]_" + cSecond.ToString();
                }
                else if (cFirst.Equals('9'))//性别
                {
                    strRegex = cSecond.ToString() + "_[0-9]_[0-9]";
                }
                else//种族
                {
                    strRegex = "[0-9]_" + cSecond.ToString() + "_[0-9]";
                }
            }
            
            // 通过正则表达式筛选结果
            foreach(String name in FileNameList)
            {
                if(System.Text.RegularExpressions.Regex.IsMatch(name, strRegex))
                {
                    resultList.Add(name);
                }
            }

            // 返回值
            return resultList;
        }

        // 将任务ID写入36张Index表中
        public void PutTaskIDToIndex(ArrayList TaskIDList)
        {
            // 文件存放路径
            String strPath = "Data/TaskIndex/";
            if(!System.IO.Directory.Exists(strPath))
            {
                System.IO.Directory.CreateDirectory(strPath);
            }

            // 初始化表，但不加载，在后面用到的时候，动态加载
            Hashtable IndexFileTable = new Hashtable();

            // 将列表中的ID，一个个写入文件中
            foreach(int iTaskID in TaskIDList)
            {
                // 计算出要改写的文件
                ArrayList fileList = GetIndexFileNameByTaskID(iTaskID);

                //逐个处理文件
                if(fileList != null)
                {
                    foreach(String name in fileList)
                    {
                        // 获取文件
                        XmlDocument xmlDoc = GetIndexFile(IndexFileTable, name, strPath);

                        // 将TaskID写入
                        TaskIDIntoIndexFile(xmlDoc, iTaskID);
                    }
                }
            }

            // 将所有修改过的文件存盘
            foreach(String strFileName in IndexFileTable.Keys)
            {
                XmlDocument doc = IndexFileTable[strFileName] as XmlDocument;
                doc.Save(strPath + strFileName + ".xml");
            }
        }

        // 获取Index文件
        public XmlDocument GetIndexFile(Hashtable table, String name, String path)
        {
            // 检测table
            if(table == null)
            {
                table = new Hashtable();
            }

            //没有加载过的话，动态加载，文件不存在的话，创建文件
            String strFullName = path + name;
            XmlDocument xmlResult = table[name] as XmlDocument;
            if (xmlResult == null)
            {
                if(System.IO.File.Exists(strFullName))
                {
                    xmlResult = new XmlDocument();
                    xmlResult.LoadXml(strFullName);
                }
                else
                {
                    xmlResult = new XmlDocument();

                    XmlDeclaration xmlDeclaration = xmlResult.CreateXmlDeclaration("1.0", "GB2312", null);
                    xmlResult.AppendChild(xmlDeclaration);

                    XmlElement root = xmlResult.CreateElement("Root");
                    xmlResult.AppendChild(root);
                }

                table[name] = xmlResult;
            }

            return xmlResult;
        }

        // 改写Index文件
        public void TaskIDIntoIndexFile(XmlDocument xmlDoc, int iTaskID)
        {
            if(xmlDoc != null)
            {
                // 获取根节点
                XmlElement xmlRoot = xmlDoc.LastChild as XmlElement;
                if(xmlRoot != null)
                {
                    if(xmlRoot.HasChildNodes)//已经有过记录
                    {
                        // 遍历查找是否存在
                        bool bFind = false;
                        XmlElement xmlBrother = xmlRoot.FirstChild as XmlElement;
                        while(xmlBrother !=null)
                        {
                            if(xmlBrother.GetAttribute("TaskID").Equals(iTaskID.ToString()))
                            {
                                bFind = true;
                            }
                            xmlBrother = xmlBrother.NextSibling as XmlElement;
                        }

                        // 不存在的话，在尾部添加
                        if(!bFind)
                        {
                            // 寻找当前的最大Index
                            xmlBrother = xmlRoot.LastChild as XmlElement;
                            int iMaxIndex = int.Parse(xmlBrother.GetAttribute("Index"));

                            // 创建新的节点，Index为iMaxIndex+1
                            int iNewIndex = iMaxIndex + 1;

                            XmlElement xmlTask = xmlDoc.CreateElement("Task");

                            xmlTask.SetAttribute("TaskID", iTaskID.ToString());
                            xmlTask.SetAttribute("Index", iNewIndex.ToString());

                            xmlRoot.AppendChild(xmlTask);
                        }
                    }
                    else// 没有记录
                    {
                        // 直接创建新的Node，Index为1
                        XmlElement xmlTask = xmlDoc.CreateElement("Task");
                        
                        xmlTask.SetAttribute("TaskID", iTaskID.ToString());
                        xmlTask.SetAttribute("Index", "1");

                        xmlRoot.AppendChild(xmlTask);
                    }
                }
            }
        }

        // 获取任务ID列表
        public ArrayList GetMissionIDList()
        {
            ArrayList idList = new ArrayList();

            foreach(Mission mission in m_MissionList)
            {
                idList.Add(mission.ID);
            }

            return idList;
        }
        #endregion

        #region 公有属性定义
        public ArrayList MissionList
        {
            get
            {
                return m_MissionList;
            }
        }

        public Hashtable Mission2NPC
        {
            get
            {
                return m_Mission2NPCList;
            }
        }

        public Hashtable Mission2Item
        {
            get
            {
                return m_Mission2ItemList;
            }
        }

        public Hashtable Mission2Area
        {
            get
            {
                return m_Mission2AreaList;
            }
        }

        public Hashtable MissionClasses
        {
            get
            {
                return m_MissionClasses;
            }
        }

        public ArrayList BanList
        {
            get
            {
                return m_BanList;
            }
        }
        #endregion

        #region 私有成员变量定义
        private Hashtable m_MissionClasses = new Hashtable();   // 任务阶级列表

        private ArrayList m_MissionList = new ArrayList();      // 所有的任务列表
        private Hashtable m_Mission2NPCList = new Hashtable();  // 任务编号 - 与之相关  NPC 列表  int - ArrayList
        private Hashtable m_Mission2ItemList = new Hashtable(); // 任务编号　－与之相关　Item 列表
        private Hashtable m_Mission2AreaList = new Hashtable(); // 任务编号　－与之相关　Area 列表

        private ArrayList m_BanList = new ArrayList(); // 被屏蔽的
        // 在外部文件导入时使用
        private XmlElement m_CurrXmlElmt = null;
        private Mission m_CurrMission = null;
        #endregion

    }
}
