﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

namespace MissionEditor
{
    public class MissionReportInfo
    {
        public MissionReportInfo(Mission mission)
        {
            m_Mission = mission;
        }

        public void Export(StreamWriter writer)
        {
            writer.Write(GetExportString());
        }

        private String GetExportString()
        {
            // 初始化
            String result = "";

            // 任务ID
            result += "<" + m_Mission.ID.ToString() + ">";

            // 任务名称
            result += "<" + m_Mission.Name + ">";

            // 任务奖励
            result += GetMissionAwardString();

            // 结尾换行符
            result += "\r\n\r\n";

            // 返回结果
            return result;
        }

        private String GetMissionAwardString()
        {
            // 初始化
            String result = "";

            // 遍历所有的奖励
            foreach (MissionAward award in m_Mission.FixedAwards)
            {
                result += "<";

                result += AwardConvert(award);

                result += ">";
            }

            // 返回结果
            return result;
        }

        private String AwardConvert(MissionAward award)
        {
            // 初始化
            String result = "";

            // 链接上对应的奖励
            result += award.Award;

            // 分各种情况进行处理
            if (award.Award.Equals("GiveItem"))// GiveItem 道具名称处理
            {
                result += GiveItemConvert(award.Value);
            }
            else if (award.Award.Equals("GiveItemFrom"))// GiveItemFrom 道具名称处理
            {
                result += GiveItemFromConvert(award.Value);
            }
            else// 一般情况
            {
                result += award.Value;
            }

            // 返回结果
            return result;
        }

        private String GiveItemConvert(String strValue)
        {
            // 初始化
            String result = "";

            if (Regex.IsMatch(strValue, "^[0-9]+,[0-9]+$"))
            {
                int iIndex = strValue.IndexOf(",");
                String strItemID = strValue.Substring(0, iIndex);
                String strItemNum = strValue.Substring(iIndex, strValue.Length - iIndex);

                int iItemID = int.Parse(strItemID);
                String strItemName = GetItemNameByID(iItemID);

                result += strItemID + "(" + strItemName + ")" + strItemNum;
            }
            else
            {
                result += "(参数错误)";
            }

            // 返回结果
            return result;
        }

        private String GiveItemFromConvert(String strValue)
        {
            // 初始化
            String result = "";
            String temp = strValue.Substring(0);

            if (Regex.IsMatch(strValue, "^([0-9]+,)*[0-9]+$"))
            {
                int iIndex = temp.IndexOf(",");
                while(iIndex != -1)
                {
                    String strItemID = temp.Substring(0, iIndex);
                    int iItemID = int.Parse(strItemID);
                    String strItemName = GetItemNameByID(iItemID);
                    result += strItemID + "(" + strItemName + ")" + ";";

                    // 下一次的遍历
                    temp = temp.Substring(iIndex + 1);
                    iIndex = temp.IndexOf(",");
                }

                // 最后一个物品
                int iLastItemID = int.Parse(temp);
                String strLastItemName = GetItemNameByID(iLastItemID);
                result += iLastItemID + "(" + strLastItemName + ")";

            }
            else
            {
                result += "(参数错误)";
            }

            // 返回结果
            return result;

        }

        private String GetItemNameByID(int iItemID)
        {
            Item item = ItemManager.Instance.GetItemByID(iItemID);
            if (item == null)
            {
                return "???";
            }
            else
            {
                return item.Name;
            }
        }

        //--------------------------------------------------------------------------------
        public Mission m_Mission = null;
    };

    public class ReportManager
    {
        public int ExportAll(String strMapName, String strClassName, String strExportPath)
        {
            // 创建文件
            String strFullPath = strExportPath + "\\" + strMapName + "_" + strClassName + ".txt";
            StreamWriter writer = new StreamWriter(strFullPath, false, Encoding.GetEncoding("GB2312"));

            // 找出符合条件的任务
            ArrayList missionList = MainForm.Instance.MissionMgr.Filter(strMapName, strClassName, "");

            //  生成文件
            foreach (Mission mission in missionList)
            {
                MissionReportInfo info = new MissionReportInfo(mission);
                info.Export(writer);
            }

            // 写入文件
            writer.Flush();
            writer.Close();
            writer.Dispose();

            // 返回成功
            return missionList.Count;
        }
        //--------------------------------------------------------------------------------
        private ReportManager() { }

        public static ReportManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new ReportManager();
                }
                return ms_Instance;
            }
        }
        //--------------------------------------------------------------------------------
        private static ReportManager ms_Instance = null;    // 唯一实例，单件模式
    }
}
