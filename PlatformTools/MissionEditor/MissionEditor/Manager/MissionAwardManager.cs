﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace MissionEditor
{
    // 封装了任务的奖励
    class MissionAwardInstance : MissionBaseData
    {
        public MissionAwardInstance(XmlElement xml) : base(xml)
        {

        }
    };

    // 任务奖励管理器
    class MissionAwardManager
    {
               public static MissionAwardManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new MissionAwardManager();
                }

                return ms_Instance;
            }
        }

        private MissionAwardManager()
        {}

        // 从 xml 文件中载入任务接受条件
        public void LoadFromXml(String strXmlFilename)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);

            XmlElement xmlElemRoot = xmlDoc.LastChild as XmlElement;
            XmlElement xmlElemAwardRoot = xmlElemRoot.FirstChild as XmlElement;
            while (xmlElemAwardRoot != null && !xmlElemAwardRoot.Name.Equals("Awards"))
            {
                xmlElemAwardRoot = xmlElemAwardRoot.NextSibling as XmlElement;
            }

            if (xmlElemAwardRoot == null)
            {
                MessageBox.Show("任务奖励信息载入失败.请检查 "+strXmlFilename +" 中的 Award 段." );
                return;
            }

            m_AwardMaps.Clear();

            // 载入任务接受条件大类
            XmlElement xmlElemAward1stLevel = xmlElemAwardRoot.FirstChild as XmlElement;

            while (xmlElemAward1stLevel != null)
            {
                String strName = xmlElemAward1stLevel.GetAttribute("Display");
                Hashtable awardMap = new Hashtable();
                m_AwardMaps[strName] = awardMap;
                XmlElement xmlElemAward2ndLevel = xmlElemAward1stLevel.FirstChild as XmlElement;
                while (xmlElemAward2ndLevel != null)
                {
                    //strName = xmlElemAward2ndLevel.GetAttribute("Display");
                    //String strAtom = xmlElemAward2ndLevel.GetAttribute("Atom");
                    //awardMap[strName] = strAtom;
                    if(xmlElemAward2ndLevel.HasAttribute("Display"))
                    {
                        strName = xmlElemAward2ndLevel.GetAttribute("Display");
                        MissionAwardInstance mai = new MissionAwardInstance(xmlElemAward2ndLevel);
                        awardMap[strName] = mai;
                    }

                    xmlElemAward2ndLevel = xmlElemAward2ndLevel.NextSibling as XmlElement;
                }
                xmlElemAward1stLevel = xmlElemAward1stLevel.NextSibling as XmlElement;
            }
        }

        public MissionAwardInstance GetAwardByDisplay(String strDisplay)
        {
            foreach(Hashtable awardList in m_AwardMaps.Values)
            {
                if(awardList[strDisplay] != null)
                {
                    return awardList[strDisplay] as MissionAwardInstance;
                }
            }
            return null;
        }

        public Hashtable AwardMaps
        {
            get
            {
                return m_AwardMaps;
            }
        }

        private static MissionAwardManager ms_Instance = null;
        private Hashtable m_AwardMaps = new Hashtable();  // 任务接受条件分类列表. String(大类名称) - Hashmap 映射
                                                                // 第二级的 Hashmap String - String  奖励名称 - 对应的原子操作
    }
}
