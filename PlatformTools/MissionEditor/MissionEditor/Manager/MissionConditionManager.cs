﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace MissionEditor
{
    // 封装了任务接受条件
    class MissionConditionInstance : MissionBaseData
    {
        public MissionConditionInstance(XmlElement xml) : base(xml)
        {
        
        }
    };

    // 任务接受条件管理器
    class MissionConditionManager
    {
        public static MissionConditionManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new MissionConditionManager();
                }

                return ms_Instance;
            }
        }

        private MissionConditionManager()
        {}

        // 从 xml 文件中载入任务接受条件
        public void LoadFromXml(String strXmlFilename)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);

            XmlElement xmlElemRoot = xmlDoc.LastChild as XmlElement;
            XmlElement xmlElemRequireRoot = xmlElemRoot.FirstChild as XmlElement;
            while (xmlElemRequireRoot!=null && !xmlElemRequireRoot.Name.Equals("Requirements"))
            {
                xmlElemRequireRoot = xmlElemRequireRoot.NextSibling as XmlElement;
            }

            if (xmlElemRequireRoot == null)
            {
                MessageBox.Show("接受任务条件信息载入失败.请检查 "+strXmlFilename +" 中的 Requirements 段." );
                return;
            }

            m_ConditionsMap.Clear();

            // 载入任务接受条件大类
            XmlElement xmlElemRequ1stLevel = xmlElemRequireRoot.FirstChild as XmlElement;
            while (xmlElemRequ1stLevel != null)
            {
                String strName = xmlElemRequ1stLevel.GetAttribute("Display");
                Hashtable reqMap = new Hashtable();
                m_ConditionsMap[strName] = reqMap;
                XmlElement xmlElemRequ2ndLeven = xmlElemRequ1stLevel.FirstChild as XmlElement;
                while (xmlElemRequ2ndLeven != null)
                {
                    //strName = xmlElemRequ2ndLeven.GetAttribute("Display");
                    //String strAtom = xmlElemRequ2ndLeven.GetAttribute("Atom");
                    //String strDesc = "";

                    //try
                    //{
                    //    strDesc = xmlElemRequ2ndLeven.GetAttribute("Desc");
                    //}
                    //catch(Exception e)
                    //{}

                    //reqMap[strName] = strAtom;
                    //m_ConditionDescMap[strName] = strDesc;

                    if (xmlElemRequ2ndLeven.HasAttribute("Display"))
                    {
                        strName = xmlElemRequ2ndLeven.GetAttribute("Display");
                        MissionConditionInstance mci = new MissionConditionInstance(xmlElemRequ2ndLeven);
                        reqMap[strName] = mci;
                    }

                    xmlElemRequ2ndLeven = xmlElemRequ2ndLeven.NextSibling as XmlElement;
                }
                xmlElemRequ1stLevel = xmlElemRequ1stLevel.NextSibling as XmlElement;
            }
        }

        public MissionConditionInstance GetConditionByDisplay(String strDisplay)
        {
            foreach (Hashtable atomList in m_ConditionsMap.Values)
            {
                if (atomList[strDisplay] != null)
                {
                    return atomList[strDisplay] as MissionConditionInstance;    
                }
            }
            return null;
        }

        public String GetConditionAtomByDisplay(String strDisplay)
        {
            foreach (Hashtable atomList in m_ConditionsMap.Values)
            {
                if (atomList[strDisplay] != null)
                {
                    //return atomList[strDisplay].ToString();
                    MissionConditionInstance mci = atomList[strDisplay] as MissionConditionInstance;
                    return mci.Atom;
                }
            }
            return null;
        }

        public Hashtable ConditionsMap
        {
            get
            {
                return m_ConditionsMap;
            }
        }

        private static MissionConditionManager ms_Instance = null;
        private Hashtable m_ConditionsMap = new Hashtable();  // 任务接受条件分类列表. String(大类名称) - Hashmap 映射
                                                                // 第二级的 Hashmap String - String  条件名称 - 对应的原子操作

        private Hashtable m_ConditionDescMap = new Hashtable(); // String - String, 任务显示 - 描述 映射
    }
}
