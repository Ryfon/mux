﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Drawing;

namespace MissionEditor
{
    public class VertexNode
    {
        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        public int MapID
        {
            get
            {
                return m_iMapID;
            }
            set
            {
                m_iMapID = value;
            }
        }

        public Point Pos
        {
            get
            {
                return m_Pos;
            }
            set
            {
                m_Pos = value;
            }
        }

        public Hashtable EdgeList
        {
            get
            {
                return m_EdgeList;
            }
        }

        public Hashtable RoutList
        {
            get
            {
                return m_RoutList;
            }
        }

        public void Move(int dx, int dy, int width, int height)
        {
            int iNewX = m_Pos.X + dx;
            int iNewY = m_Pos.Y - dy;

            if (iNewX < 0)
            {
                iNewX = m_Pos.X;
            }
            if (iNewX >= width)
            {
                iNewX = m_Pos.X;
            }
            if (iNewY < 0)
            {
                iNewY = m_Pos.Y;
            }
            if (iNewY >= height)
            {
                iNewY = m_Pos.Y;
            }

            Point newPoint = new Point(iNewX, iNewY);
            m_Pos = newPoint;
        }
        //////////////////////////////////////////////////////////////////////////
        // 私有成员
        private int m_iID;  // 唯一标识一个点的编号
        private int m_iMapID;   // 地图编号
        private Hashtable m_EdgeList = new Hashtable();// 邻接表
        private Hashtable m_RoutList = new Hashtable();// 路由表
        private Point m_Pos = new System.Drawing.Point();     //记录点的位置
    };

    public class RoutNode
    {
        public RoutNode(int target, int next, float distance)
        {
            m_iTarget = target;
            m_iNext = next;
            m_fDistance = distance;
        }

        public int TargetNode
        {
            get
            {
                return m_iTarget;
            }
            set
            {
                m_iTarget = value;
            }
        }

        public int NextNode
        {
            get
            {
                return m_iNext;
            }
            set
            {
                m_iNext = value;
            }
        }

        public float Distance
        {
            get
            {
                return m_fDistance;
            }
            set
            {
                m_fDistance = value;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // 私有成员
        private int m_iTarget;  // 目标节点
        private int m_iNext;    // 下一跳的节点
        private float m_fDistance;  // 到目标节点的距离
    };

    public class EdgeNode
    {
        public EdgeNode(int adj, float distance)
        {
            m_iAdjVertex = adj;
            m_fDistance = distance;
        }

        public int AdjNode
        {
            get
            {
                return m_iAdjVertex;
            }
            set
            {
                m_iAdjVertex = value;
            }
        }

        public float Distance
        {
            get
            {
                return m_fDistance;
            }
            set
            {
                m_fDistance = value;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // 私有成员
        private int m_iAdjVertex;  // 邻居节点
        private float m_fDistance;  // 到目标节点的距离
    };

    class PathManager
    {
        const int iSmallSize = 6;

        public void LoadFromXml(int iMapID, String strXmlFilename)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;

            if(m_VertexList == null)
            {
                m_VertexList = new ArrayList();
            }
            else
            {
                m_VertexList.Clear();
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);

            XmlElement elmtRoot = xmlDoc.LastChild as XmlElement;
            if(elmtRoot == null)
            {
                return;
            }

            XmlElement elmtVertex = elmtRoot.FirstChild as XmlElement;

            while (elmtVertex != null)
            {
                VertexNode vn = new VertexNode();
                try
                {
                    int iVertexID = int.Parse(elmtVertex.GetAttribute("Id"));
                    int iPosX = int.Parse(elmtVertex.GetAttribute("PosX"));
                    int iPoxY = int.Parse(elmtVertex.GetAttribute("PosY"));
                    vn.ID = iVertexID;
                    vn.Pos = new Point(iPosX, iPoxY);
                    vn.MapID = iMapID;

                    XmlElement elmtEdge = elmtVertex.FirstChild as XmlElement;
                    while (elmtEdge != null)
                    {
                        int iEdgeID = int.Parse(elmtEdge.GetAttribute("Id"));
                        float fDistance = float.Parse(elmtEdge.GetAttribute("Distance"));
                        EdgeNode en = new EdgeNode(iEdgeID, fDistance);
                        vn.EdgeList[iEdgeID] = en;
                        elmtEdge = elmtEdge.NextSibling as XmlElement;
                    }

                    m_VertexList.Add(vn);
                }
                catch(Exception)
                {

                }
                elmtVertex = elmtVertex.NextSibling as XmlElement;
            }

            // 添加入地图映射列表
            m_MapID2VertexList[iMapID] = m_VertexList;

            // 初始化路由表
            InitRoutList(iMapID);
        }

        public void SaveVertexToXml(int iMapID, String strPathName)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;

            if (m_VertexList == null)
            {
                return;
            }

            // 开始写文件
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("PathInfo");
            root.SetAttribute("FileVersion", "0.9");
            xmlDoc.AppendChild(root);

            foreach (VertexNode vn in m_VertexList)
            {
                XmlElement xmlVertex = xmlDoc.CreateElement("Vertex");
                xmlVertex.SetAttribute("Id", vn.ID.ToString());
                xmlVertex.SetAttribute("PosX", vn.Pos.X.ToString());
                xmlVertex.SetAttribute("PosY", vn.Pos.Y.ToString());

                Hashtable edgeList = vn.EdgeList;

                foreach (EdgeNode en in edgeList.Values)
                {
                    XmlElement xmlEdge = xmlDoc.CreateElement("Edge");
                    xmlEdge.SetAttribute("Id", en.AdjNode.ToString());
                    xmlEdge.SetAttribute("Distance", en.Distance.ToString());
                    xmlVertex.AppendChild(xmlEdge);
                }

                root.AppendChild(xmlVertex);
            }

            xmlDoc.Save(strPathName + "PathInfo_" + iMapID.ToString() + ".xml");
        }

        public void SaveRoutToXml(int iMapID, String strPathName)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;

            if (m_VertexList == null)
            {
                return;
            }

            // 保存前进行更新，确保正确
            UpdateEdgeDistance(iMapID);
            InitRoutList(iMapID);
            RefreshRoutListAll(iMapID, m_VertexList.Count);

            // 开始写文件
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("RoutInfo");
            root.SetAttribute("FileVersion", "0.9");
            xmlDoc.AppendChild(root);

            foreach(VertexNode vn in m_VertexList)
            {
                XmlElement xmlVertex = xmlDoc.CreateElement("Vertex");
                xmlVertex.SetAttribute("Id", vn.ID.ToString());
                xmlVertex.SetAttribute("PosX", vn.Pos.X.ToString());
                xmlVertex.SetAttribute("PosY", vn.Pos.Y.ToString());

                Hashtable routList = vn.RoutList;
                
                foreach(RoutNode rn in routList.Values)
                {
                    XmlElement xmlRout = xmlDoc.CreateElement("Rout");
                    xmlRout.SetAttribute("Target", rn.TargetNode.ToString());
                    xmlRout.SetAttribute("Next", rn.NextNode.ToString());
                    xmlVertex.AppendChild(xmlRout);
                }

                root.AppendChild(xmlVertex);
            }

            xmlDoc.Save(strPathName+"RoutInfo_"+iMapID.ToString()+".xml");

            // 保存新的顶点数据
            SaveVertexToXml(iMapID, strPathName);

            // 测试寻路
            ArrayList path = FindPath(iMapID, PathManager.Instance.HomeID, PathManager.Instance.DestinationID);
            if(path != null)
            {
                String strLastPath = "";
                foreach(int VertexID in path)
                {
                    strLastPath += VertexID.ToString();
                    strLastPath += "->";
                }

                System.Windows.Forms.MessageBox.Show(strLastPath);
            }
        }

        public void LoadFromDirectory(String strDirectPath)
        {
            m_MapID2VertexList.Clear();

            if(!System.IO.Directory.Exists(strDirectPath))
            {
                return;
            }

            String[] fileList = System.IO.Directory.GetFileSystemEntries(strDirectPath);
            
            foreach (String strFileName in fileList)
            {
                // 获取文件
                String strExtendName = System.IO.Path.GetExtension(strFileName);
                if (strExtendName.CompareTo(".xml") != 0)
                {
                    continue;
                }

                // 获取地图号
                int iDot = strFileName.LastIndexOf('.');
                int ihen = strFileName.LastIndexOf('_');
                
                String strMapID = strFileName.Substring(ihen + 1, iDot - ihen - 1);
                int iMapID = int.Parse(strMapID);

                if(strFileName.CompareTo(strDirectPath + "PathInfo_" + iMapID.ToString() + strExtendName) != 0)
                {
                    continue;
                }
                // 读取文件
                LoadFromXml(iMapID, strFileName);
            }
        }

        // 根据图的结构，初始化路由表
        public void InitRoutList(int iMapID)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return;
            }

            int iVertexNum = m_VertexList.Count;

            foreach(VertexNode vn in m_VertexList)
            {
                Hashtable edge = vn.EdgeList;
                Hashtable rout = vn.RoutList;
                rout.Clear();

                // 添加自回路，距离为0
                RoutNode selfRn = new RoutNode(vn.ID, vn.ID, 0.0f);
                rout[selfRn.TargetNode] = selfRn;

                // 添加到邻居节点的路径
                foreach(EdgeNode en in edge.Values)
                {
                    RoutNode rn = new RoutNode(en.AdjNode, en.AdjNode, en.Distance);
                    rout[rn.TargetNode] = rn;
                }
            }
        }

        public VertexNode GetVertexByID(int iMapID, int iID)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return null;
            }

            foreach(VertexNode vn in m_VertexList)
            {
                if(vn.ID == iID)
                {
                    return vn;
                }
            }

            return null;
        }

        public VertexNode GetVertexByPos(int iMapID, Point Pos)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return null;
            }

            foreach (VertexNode vn in m_VertexList)
            {
                if (Math.Abs(vn.Pos.X - Pos.X) < 2 * iSmallSize && Math.Abs(vn.Pos.Y - Pos.Y) < 2 * iSmallSize)
                {
                    return vn;
                }
            }

            return null;
        }

        public Point GetVertexPosByID(int iMapID, int iID)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return new Point(-1, -1);
            }

            foreach (VertexNode vn in m_VertexList)
            {
                if (vn.ID == iID)
                {
                    return vn.Pos;
                }
            }

            return new Point(-1, -1);
        }

        // 单个节点，通过copy邻居的路由表，进行更新
        public void RefreshRoutList(int iMapID, VertexNode vn)
        {
            Hashtable MyEdgeList = vn.EdgeList;
            Hashtable MyRoutList = vn.RoutList;

            foreach (EdgeNode en in MyEdgeList.Values)
            {
                VertexNode adjVertex = GetVertexByID(iMapID, en.AdjNode);
                if(adjVertex != null)
                {
                    Hashtable adjRoutList = adjVertex.RoutList;
                    foreach (RoutNode adjRn in adjRoutList.Values)
                    {
                        RoutNode MyRn = MyRoutList[adjRn.TargetNode] as RoutNode;
                        if(MyRn == null)// 不存在路由表，则copy邻居的路由表
                        {
                            RoutNode newRn = new RoutNode(adjRn.TargetNode, en.AdjNode, en.Distance + adjRn.Distance);
                            MyRoutList[newRn.TargetNode] = newRn;
                        }
                        else // 已存在，则比较更新
                        {
                            if(en.Distance + adjRn.Distance < MyRn.Distance)
                            {
                                MyRn.NextNode = en.AdjNode;
                                MyRn.Distance = en.Distance + adjRn.Distance;
                            }
                        }
                    }
                }
            }
        }

        public void RefreshRoutListAll(int iMapID, int times)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return;
            }

            int index = 0;
            while(index < times)
            {
                foreach(VertexNode vn in m_VertexList)
                {
                    RefreshRoutList(iMapID, vn);
                }
                index++;
            }
        }

        public void UpdateEdgeDistance(int iMapID)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return;
            }

            foreach(VertexNode vn in m_VertexList)
            {
                Hashtable edgeList = vn.EdgeList;

                foreach(EdgeNode en in edgeList.Values)
                {
                    int adjID = en.AdjNode;
                    VertexNode adjVn = GetVertexByID(iMapID, adjID);
                    if(adjVn != null)
                    {
                        int dx = Math.Abs(adjVn.Pos.X - vn.Pos.X);
                        int dy = Math.Abs(adjVn.Pos.Y - vn.Pos.Y);

                        float fNewDistance = (float)Math.Sqrt(dx*dx + dy*dy);
                        en.Distance = fNewDistance;
                    }
                }
            }

        }

        // 顶点和边的添加和删除
        public void Modify(int iMapID)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return;
            }

            VertexNode StartNode = GetVertexByPos(iMapID, m_StartPoint);
            if(StartNode == null)
            {
                return;
            }

            VertexNode EndNode = GetVertexByPos(iMapID, m_EndPoint);
            if(StartNode == EndNode)// 起始点和终止点重合，直接返回
            {
                return;
            }

            if(EndNode == null) // 终止点没有落在别的点上，创建一个新的点
            {
                VertexNode NewVertex = AddVertex(iMapID, m_EndPoint);
                // 从起点到终点的链接
                StartNode.EdgeList[NewVertex.ID] = new EdgeNode(NewVertex.ID, 0.0f);
                // 从终点到起点的链接
                NewVertex.EdgeList[StartNode.ID] = new EdgeNode(StartNode.ID, 0.0f);
            }
            else
            {
                EdgeNode S2E = StartNode.EdgeList[EndNode.ID] as EdgeNode;
                EdgeNode E2S = EndNode.EdgeList[StartNode.ID] as EdgeNode;

                if (S2E == null)  // 不存在边，添加一条边
                {
                    StartNode.EdgeList[EndNode.ID] = new EdgeNode(EndNode.ID, 0.0f);
                    // 从终点到起点的链接
                    EndNode.EdgeList[StartNode.ID] = new EdgeNode(StartNode.ID, 0.0f);
                }
                else//边已经存在，删除这条边
                {
                    StartNode.EdgeList.Remove(EndNode.ID);
                    EndNode.EdgeList.Remove(StartNode.ID);
                }
            }
        }

        public int GetBigVertexID(int iMapID)
        {
            int iBigID = 0;

            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return iBigID;
            }

            foreach (VertexNode vn in m_VertexList)
            {
                if (vn.ID > iBigID)
                {
                    iBigID = vn.ID;
                }
            }

            return iBigID;
        }

        // 创建一个新的顶点，并返回它的ID
        public VertexNode AddVertex(int iMapID, Point newPos)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return null;
            }

            int iBigID = GetBigVertexID(iMapID);

            VertexNode newVertex = new VertexNode();
            newVertex.ID = iBigID + 1;
            newVertex.MapID = iMapID;
            newVertex.Pos = new Point(newPos.X, newPos.Y);

            m_VertexList.Add(newVertex);

            return newVertex;
        }

        // 删除当前节点
        public void DelVertex(int iMapID, VertexNode vn)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return;
            }

            // 删除该节点所有的边
            foreach(EdgeNode en in vn.EdgeList.Values)
            {
                VertexNode adjVertex = GetVertexByID(iMapID, en.AdjNode);
                if(adjVertex != null)
                {
                    adjVertex.EdgeList.Remove(vn.ID);
                }
            }

            // 删除该节点本身
            m_VertexList.Remove(vn);
        }

        // 测试寻路
        public ArrayList FindPath(int iMapID, int StartID, int EndID)
        {
            ArrayList m_VertexList = m_MapID2VertexList[iMapID] as ArrayList;
            if (m_VertexList == null)
            {
                return null;
            }

            ArrayList pathList = new ArrayList();

            VertexNode StartVertex = GetVertexByID(iMapID, StartID);
            VertexNode EndVertex = GetVertexByID(iMapID, EndID);

            if(StartVertex == null || EndVertex == null)
            {
                return null;
            }

            int iCurrentID = StartID;
            pathList.Add(iCurrentID);

            while(iCurrentID != EndID)
            {
                VertexNode currentVertex = GetVertexByID(iMapID, iCurrentID);
                if(currentVertex == null)
                {
                    return null;
                }

                RoutNode rout = currentVertex.RoutList[EndID] as RoutNode;
                if(rout == null)
                {
                    return null;
                }

                iCurrentID = rout.NextNode;
                pathList.Add(iCurrentID);
            }

            return pathList;
        }

        public static PathManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new PathManager();
                }
                return ms_Instance;
            }
        }

        public Hashtable MapID2VertexList
        {
            get
            {
                return m_MapID2VertexList;
            }
        }

        public Point StartPoint
        {
            get
            {
                return m_StartPoint;
            }
            set
            {
                m_StartPoint = value;
            }
        }

        public Point EndPoint
        {
            get
            {
                return m_EndPoint;
            }
            set
            {
                m_EndPoint = value;
            }
        }

        public int HomeID
        {
            get
            {
                return m_iHomeID;
            }
            set
            {
                m_iHomeID = value;
            }
        }

        public int DestinationID
        {
            get
            {
                return m_iDestinationID;
            }
            set
            {
                m_iDestinationID = value;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // 私有成员
        private Hashtable m_MapID2VertexList = new Hashtable(); // 地图号与锚点列表的映射
        private static PathManager ms_Instance = null;
        private Point m_StartPoint = new Point(-100, -100);// 修改操作时，起始点
        private Point m_EndPoint = new Point(-100, -100);// 终止点
        private int m_iHomeID = 0;    // 寻路测试的起点
        private int m_iDestinationID = 0;   // 寻路测试的终点
    };
}
