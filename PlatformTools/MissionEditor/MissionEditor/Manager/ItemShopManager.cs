﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace MissionEditor
{
    // 商品
    public class ShopItem
    {
        public ShopItem(int iItemID, int iPerPurchaseNum,  String strTradePrice, 
           String strProduceType, String strDailyProduceTime, String strProduceNum, String strCapability)
        {
            m_iItemID = iItemID;
            m_iPerPurchaseNum = iPerPurchaseNum;
            m_strTradePrice = strTradePrice;
            m_strProduceType = strProduceType;
            m_strDailyProduceTime = strDailyProduceTime;
            m_strProduceNum = strProduceNum;
            m_strCapability = strCapability;
        }

        public int ItemID
        {
            get
            {
                return m_iItemID;
            }
            set
            {
                m_iItemID = value;
            }
        }

        public int PerPurchaseNum
        {
            get
            {
                return m_iPerPurchaseNum;
            }
            set
            {
                m_iPerPurchaseNum = value;
            }
        }

        public String TradePrice
        {
            get
            {
                return m_strTradePrice;
            }
            set
            {
                m_strTradePrice = value;
            }
        }

        public String ProduceType
        {
            get
            {
                return m_strProduceType;
            }
            set
            {
                m_strProduceType = value;
            }
        }

        public String DailyProduceTime
        {
            get
            {
                return m_strDailyProduceTime;
            }
            set
            {
                m_strDailyProduceTime = value;
            }
        }

        public String ProduceNum
        {
            get
            {
                return m_strProduceNum;
            }
            set
            {
                m_strProduceNum = value;
            }
        }

        public String Capability
        {
            get
            {
                return m_strCapability;
            }
            set
            {
                m_strCapability = value;
            }
        }

        private int m_iItemID;  // 对应 item id
        private int m_iPerPurchaseNum;  // 单次购买数量
        private String m_strTradePrice;   // 交易物品数量
        private String m_strProduceType; // 生产类型
        private String m_strDailyProduceTime;   // 出产商品时间
        private String m_strProduceNum;  // 每次生产数量公式
        private String m_strCapability;  // 最大容量公式
    };

    public class ItemShop
    {

        public ItemShop(int iID, String strDesc, float fPriceRate, CurrencyType eCurrencyType, int iTradeItemID)
        {
            m_iID = iID;
            m_strDesc = strDesc;
            m_fPriceRate = fPriceRate;
            m_CurrencyType = eCurrencyType;
            m_iTradeItemID = iTradeItemID;
        }

        public void AddItem(String strTabName, ShopItem item)
        {
            if (strTabName==null || item==null)
            {
                return;
            }

            // 判断名为 strTabName 的 HashTable 是否存在
            Hashtable table = null;
            if (m_Tables.ContainsKey(strTabName))
            {
                table = m_Tables[strTabName] as Hashtable;
            }
            else
            {
                table = new Hashtable();
                m_Tables[strTabName] = table;
            }

            table[item.ItemID] = item;
        }

        public void RemoveItem(String strTabName, ShopItem item)
        {
            if (strTabName == null || item == null)
            {
                return;
            }
 
            if (m_Tables.ContainsKey(strTabName))
            {
                Hashtable table = m_Tables[strTabName] as Hashtable;
                if (table.ContainsKey(item.ItemID))
                {
                    table.Remove(item.ItemID);
                }
            }
        }

        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        public String Desc
        {
            get
            {
                return m_strDesc;
            }
            set
            {
                m_strDesc = value;
            }
        }

        public float PriceRate
        {
            get
            {
                return m_fPriceRate;
            }
            set
            {
                m_fPriceRate = value;
            }
        }

        public CurrencyType ShopCurrencyType
        {
            get
            {
                return m_CurrencyType;
            }
            set
            {
                m_CurrencyType = value;
            }
        }

        public int TradeItemID
        {
            get
            {
                return m_iTradeItemID;
            }
            set
            {
                m_iTradeItemID = value;
            }
        }

        public Hashtable TableList
        {
            get
            {
                return m_Tables;
            }
        }

        public Hashtable IndexTables
        {
            get
            {
                return m_IndexTables;
            }
        }

        private int m_iID;          // 商店编号
        private String m_strDesc;   // 商店描述
        private float m_fPriceRate = 1.0f;       // 价格率 默认 1.0
        private CurrencyType m_CurrencyType;     // 货币类型
        private int m_iTradeItemID;   // 交易物品 ID
        private Hashtable m_Tables = new Hashtable();  // TableName - TableList
        private Hashtable m_IndexTables = new Hashtable();  // String - ArrayList
    };
    // 商店管理器
    public class ItemShopManager
    {
        public static ItemShopManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new ItemShopManager();
                }

                return ms_Instance;
            }
        }

        public void SaveToXml(String strXmlFilename)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", null);
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement elmtRoot = xmlDoc.CreateElement("Root");
            xmlDoc.AppendChild(elmtRoot);
            foreach (ItemShop shop in m_ShopList.Values)
            {
                XmlElement elmtShopRoot = xmlDoc.CreateElement("Shop");
                elmtShopRoot.SetAttribute("ID", shop.ID.ToString());
                elmtShopRoot.SetAttribute("Desc", shop.Desc);
                elmtShopRoot.SetAttribute("PriceRate", shop.PriceRate.ToString());
                elmtShopRoot.SetAttribute("CurrencyType", ((int)shop.ShopCurrencyType).ToString());
                elmtShopRoot.SetAttribute("TradeItemID", shop.TradeItemID.ToString());
                elmtRoot.AppendChild(elmtShopRoot);

                // 保存每个 table
                foreach (String strTabName in shop.TableList.Keys)
                {
                    XmlElement elmtTable = xmlDoc.CreateElement("Label");
                    elmtTable.SetAttribute("Name", strTabName);
                    elmtShopRoot.AppendChild(elmtTable);
                    Hashtable table = shop.TableList[strTabName] as Hashtable;
                    ArrayList indexList = shop.IndexTables[strTabName] as ArrayList;

                    if (indexList != null)
                    {
                        foreach(int iItemID in indexList)
                        {
                            ShopItem shopItem = table[iItemID] as ShopItem;
                            XmlElement elmtShopItem = xmlDoc.CreateElement("ShopItem");
                            elmtShopItem.SetAttribute("ItemID", shopItem.ItemID.ToString());
                            elmtShopItem.SetAttribute("PerPurchaseNum", shopItem.PerPurchaseNum.ToString());
                            elmtShopItem.SetAttribute("TradePrice", shopItem.TradePrice);
                            elmtShopItem.SetAttribute("ProduceType", shopItem.ProduceType);
                            elmtShopItem.SetAttribute("ProduceNum", shopItem.ProduceNum);
                            elmtShopItem.SetAttribute("Capability", shopItem.Capability);
                            elmtShopItem.SetAttribute("DailyProduceTime", shopItem.DailyProduceTime);
                            elmtTable.AppendChild(elmtShopItem);
                        }
                    }
                    else
                    {
                        // 保存每个 table 的 shop item
                        foreach (ShopItem shopItem in table.Values)
                        {
                            XmlElement elmtShopItem = xmlDoc.CreateElement("ShopItem");
                            elmtShopItem.SetAttribute("ItemID", shopItem.ItemID.ToString());
                            elmtShopItem.SetAttribute("PerPurchaseNum", shopItem.PerPurchaseNum.ToString());
                            elmtShopItem.SetAttribute("TradePrice", shopItem.TradePrice);
                            elmtShopItem.SetAttribute("ProduceType", shopItem.ProduceType);
                            elmtShopItem.SetAttribute("ProduceNum", shopItem.ProduceNum);
                            elmtShopItem.SetAttribute("Capability", shopItem.Capability);
                            elmtShopItem.SetAttribute("DailyProduceTime", shopItem.DailyProduceTime);
                            elmtTable.AppendChild(elmtShopItem);
                        }
                    }
                }
            }
            xmlDoc.Save(strXmlFilename);
        }

        public void LoadFromXml(String strXmlFilename)
        {
            m_ShopList.Clear();

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strXmlFilename);
                XmlElement elmtRoot = xmlDoc.LastChild as XmlElement;
                XmlElement elmtShop = elmtRoot.FirstChild as XmlElement;
                while (elmtShop != null)
                {
                    int iShopID = int.Parse(elmtShop.GetAttribute("ID"));
                    String strDesc = elmtShop.GetAttribute("Desc");
                    float fPriceRate = float.Parse(elmtShop.GetAttribute("PriceRate"));
                    CurrencyType eCurrencyType = (CurrencyType)int.Parse(elmtShop.GetAttribute("CurrencyType"));
                    int iTradeItemID = int.Parse(elmtShop.GetAttribute("TradeItemID"));
                    ItemShop shop = new ItemShop(iShopID, strDesc, fPriceRate, eCurrencyType, iTradeItemID);
                    m_ShopList[iShopID] = shop;

                    // 读取 table
                    XmlElement elmtTable = elmtShop.FirstChild as XmlElement;
                    while (elmtTable != null)
                    {
                        String strTableName = elmtTable.GetAttribute("Name");
                        Hashtable table = new Hashtable();
                        shop.TableList[strTableName] = table;
                        // 添加索引list
                        ArrayList indexList = new ArrayList();
                        shop.IndexTables[strTableName] = indexList;

                        // 读取 shop item
                        XmlElement elmtItem = elmtTable.FirstChild as XmlElement;
                        while (elmtItem != null)
                        {
                            int iItemID = int.Parse(elmtItem.GetAttribute("ItemID"));
                            int iPerPurchaseNum = int.Parse(elmtItem.GetAttribute("PerPurchaseNum"));
                            String strTradePrice = elmtItem.GetAttribute("TradePrice");
                            String strProduceType = elmtItem.GetAttribute("ProduceType");
                            String strProduceNum = elmtItem.GetAttribute("ProduceNum");
                            String strCapability = elmtItem.GetAttribute("Capability");
                            String strDailyProduceTime = elmtItem.GetAttribute("DailyProduceTime");

                            ShopItem shopItem = new ShopItem(iItemID, iPerPurchaseNum, strTradePrice, strProduceType, strDailyProduceTime, strProduceNum, strCapability);
                            table[iItemID] = shopItem;
                            indexList.Add(iItemID);
                            elmtItem = elmtItem.NextSibling as XmlElement;
                        }
                        elmtTable = elmtTable.NextSibling as XmlElement;
                    }
                    elmtShop = elmtShop.NextSibling as XmlElement;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        // 读取由excel导出的xml
        public void LoadFromExternalXml(String strXmlFilename)
        {
            // 创建一个新的商店列表
            Hashtable newShopList = new Hashtable();

            // 开始读取
            try
            {
                // 加载文件
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(strXmlFilename);

                // 获取节点
                XmlElement elmtRoot = xmlDoc.LastChild as XmlElement;
                XmlElement elmtShop = elmtRoot.FirstChild as XmlElement;

                // 循环遍历
                while (elmtShop != null)
                {
                    // 商店节点
                    if (elmtShop.Name.Equals("Shop"))
                    {
                        // 获取节点的属性
                        int iShopID = int.Parse(elmtShop.GetAttribute("ID"));
                        String strDesc = elmtShop.GetAttribute("Desc");
                        float fPriceRate = float.Parse(elmtShop.GetAttribute("PriceRate"));
                        CurrencyType eCurrencyType = (CurrencyType)int.Parse(elmtShop.GetAttribute("CurrencyType"));
                        int iTradeItemID = int.Parse(elmtShop.GetAttribute("TradeItemID"));

                        // 生成一个新的商店，并加入商店列表hashtable
                        ItemShop shop = new ItemShop(iShopID, strDesc, fPriceRate, eCurrencyType, iTradeItemID);
                        newShopList[iShopID] = shop;
                    }

                    // 物品节点
                    if (elmtShop.Name.Equals("ShopItem"))
                    {
                        // 获取物品所属的shop和table的属性
                        int iShopID = int.Parse(elmtShop.GetAttribute("ID"));
                        String strTableName = elmtShop.GetAttribute("Name");

                        // 获取物品属性
                        int iItemID = int.Parse(elmtShop.GetAttribute("ItemID"));
                        int iPerPurchaseNum = int.Parse(elmtShop.GetAttribute("PerPurchaseNum"));
                        String strTradePrice = elmtShop.GetAttribute("TradePrice");
                        String strProduceType = elmtShop.GetAttribute("ProduceType");
                        String strProduceNum = elmtShop.GetAttribute("ProduceNum");
                        String strCapability = elmtShop.GetAttribute("Capability");
                        String strDailyProduceTime = elmtShop.GetAttribute("DailyProduceTime");

                        // 寻找商店
                        ItemShop shop = newShopList[iShopID] as ItemShop;
                        ArrayList indexList = null;
                        if (shop == null)
                        {
                            // 如果商店不存在，直接跳过，遍历下一个节点
                            elmtShop = elmtShop.NextSibling as XmlElement;
                            continue;
                        }
                        else
                        {
                            // 寻找table
                            Hashtable table = shop.TableList[strTableName] as Hashtable;
                            if(table == null)
                            {
                                // 如果table不存在，则创建table
                                table = new Hashtable();
                                shop.TableList[strTableName] = table;
                                indexList = new ArrayList();
                                shop.IndexTables[strTableName] = indexList;
                            }
                            else
                            {
                                indexList = shop.IndexTables[strTableName] as ArrayList;
                            }

                            // 将物品加入table
                            ShopItem shopItem = new ShopItem(iItemID, iPerPurchaseNum, strTradePrice, strProduceType, strDailyProduceTime, strProduceNum, strCapability);
                            table[iItemID] = shopItem;
                            indexList.Add(iItemID);
                        }
                    }

                    // 继续遍历下一个节点
                    elmtShop = elmtShop.NextSibling as XmlElement;
                }

                // 清空原先的数据
                m_ShopList.Clear();
                m_ShopList = newShopList;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        // 获取所有商店列表
        public Hashtable ShopList
        {
            get
            {
                return m_ShopList;
            }
        }

        // 获取一个 shop 中的所有 item
        public ItemShop GetShop(int iShopID)
        {
            if (m_ShopList.ContainsKey(iShopID))
            {
                return m_ShopList[iShopID] as ItemShop;
            }
            else
            {
                return null;
            }
        }

        // 为 iShopID 商店添加一个 item
        //public void AddItem(int iShopID, ShopItem item)
        //{
        //    if (iShopID<0)
        //    {
        //        return;
        //    }

        //    if (m_ShopList.ContainsKey(iShopID))
        //    {
        //        ItemShop shop = m_ShopList[iShopID] as ItemShop;
        //        shop.AddItem(item);
        //    }
        //}

        private static ItemShopManager ms_Instance = null;
        private Hashtable m_ShopList = new Hashtable(); // ShopID - Shop
    }
}
