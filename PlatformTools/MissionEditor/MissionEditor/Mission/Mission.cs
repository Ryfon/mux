﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MissionEditor
{
    public class Mission
    {
        #region 公有属性定义
        public int ID
        {
            set
            {
                m_iID = value;
            }
            get
            {
                return m_iID;
            }
        }

        public String Name
        {
            set
            {
                m_strName = value;
            }
            get
            {
                return m_strName;
            }
        }

        public String GetDescription
        {
            set
            {
                m_strGetDesc = value;
            }
            get
            {
                return m_strGetDesc;
            }
        }

        public String SubmitDescription
        {
            set
            {
                m_strSubmitDesc = value;
            }
            get
            {
                return m_strSubmitDesc;
            }
        }

        public String Type
        {
            set
            {
                m_strType = value;
            }
            get
            {
                return m_strType;
            }
        }

        public String SubMissionType
        {
            set
            {
                m_strSubMissionType = value;
            }
            get
            {
                return m_strSubMissionType;
            }
        }

        public String FlowType
        {
            set
            {
                m_strFlowType = value;
            }
            get
            {
                return m_strFlowType;
            }
        }

        public int Class
        {
            set
            {
                m_iClass = value;
            }
            get
            {
                return m_iClass;
            }
        }

        public int Level
        {
            set
            {
                m_iLevel = value;
            }
            get
            {
                return m_iLevel;
            }
        }

        public bool ItemProvider
        {
            set
            {
                m_bItemProvider = value;
            }
            get
            {
                return m_bItemProvider;
            }
        }

        public bool RegionProvider
        {
            set
            {
                m_bRegionProvider = value;
            }
            get
            {
                return m_bRegionProvider;
            }
        }

        public int Provider
        {
            set
            {
                m_iProvider = value;
            }
            get
            {
                return m_iProvider;
            }
        }

        // 向谁还任务
        public int SubmitTo
        {
            set
            {
                m_iSubmitTo = value;
            }
            get
            {
                return m_iSubmitTo;
            }
        }

        public int TargetNPC
        {
            set
            {
                m_iTargetNPC = value;
            }
            get
            {
                return m_iTargetNPC;
            }
        }

        public int TargetMonsterBegin 
        {
            set
            {
                m_iTargetMonsterBegin = value;
            }
            get
            {
                return m_iTargetMonsterBegin;
            }
        }

        public int TargetMonsterEnd
        {
            set
            {
                m_iTargetMonsterEnd = value;
            }
            get
            {
                return m_iTargetMonsterEnd;
            }
        }

        public int TargetItemBegin
        {
            set
            {
                m_iTargetItemBegin = value;
            }
            get
            {
                return m_iTargetItemBegin;
            }
        }

        public int TargetItemEnd
        {
            set
            {
                m_iTargetItemEnd = value;
            }
            get
            {
                return m_iTargetItemEnd;
            }
        }
        //--------------------------------------------------2009-11-24 徐磊新增--------------------------------------------------//
        public int TargetBegin
        {
            set
            {
                m_iTargetBegin = value;
            }
            get
            {
                return m_iTargetBegin;
            }
        }

        public int TargetEnd
        {
            set
            {
                m_iTargetEnd = value;
            }
            get
            {
                return m_iTargetEnd;
            }
        }
        //--------------------------------------------------2009-11-24 徐磊新增--------------------------------------------------//

        public int TargetArea
        {
            set
            {
                m_iTargetArea = value;
            }
            get
            {
                return m_iTargetArea;
            }
        }

        public int TargetProperty
        {
            set
            {
                m_iTargetProperty = value;
            }
            get
            {
                return m_iTargetProperty;
            }
        }

        public int TargetValue
        {
            set
            {
                m_iTargetValue = value;
            }
            get
            {
                return m_iTargetValue;
            }
        }

        public String TrackName
        {
            set
            {
                m_strTrackName = value;
            }
            get
            {
                return m_strTrackName;
            }
        }

        public bool Share
        {
            set
            {
                m_bShare = value;
            }
            get
            {
                return m_bShare;
            }
        }

        public bool Repeat
        {
            set
            {
                m_bRepeat = value;
            }
            get
            {
                return m_bRepeat;
            }
        }

        public bool GiveUp
        {
            set
            {
                m_bGiveUp = value;
            }
            get
            {
                return m_bGiveUp;
            }
        }

        public int RecordReserve
        {
            set
            {
                m_iRecordReserve = value;
            }
            get
            {
                return m_iRecordReserve;
            }
        }

        public ArrayList Conditions
        {
            get
            {
                return m_Conditions;
            }
        }

        public ArrayList FixedAwards
        {
            get
            {
                return m_FixedAwards;
            }
        }

        public ArrayList SelectedAwards
        {
            get
            {
                return m_SelectedAwards;
            }
        }

        public String SimpleDesc
        {
            get
            {
                return m_strSimpleDesc;
            }
            set
            {
                m_strSimpleDesc = value;
            }
        }

        public int LeadMap
        {
            get
            {
                return m_iLeadMap;
            }
            set
            {
                m_iLeadMap = value;
            }
        }

        public int StarLevel
        {
            get
            {
                return m_iStarLevel;
            }
            set
            {
                m_iStarLevel = value;
            }
        }

        public int TimeLimit
        {
            get
            {
                return m_iTimeLimit;
            }
            set
            {
                m_iTimeLimit = value;
            }
        }

        public int TeamWorkStatus
        {
            get
            {
                return m_iTeamWorkStatus;
            }
            set
            {
                m_iTeamWorkStatus = value;
            }
        }

        public String TeamWorkMark
        {
            get
            {
                if(m_iTeamWorkStatus == 0)
                {
                    return "＋";
                }
                if (m_iTeamWorkStatus == 1)
                {
                    return "";
                }
                if (m_iTeamWorkStatus == 2)
                {
                    return "√";
                }
                if (m_iTeamWorkStatus == 3)
                {
                    return "∞";
                }

                return "×";
            }
        }

        public String GetHint
        {
            get
            {
                return m_strGetHint;
            }
            set
            {
                m_strGetHint = value;
            }
        }

        #endregion

        #region 对外接口

        public void LoadFromXmlNode(XmlElement xmlElem)
        {
            if (xmlElem != null)
            {
                int iTryTemp = 0;   // 临时变量，用于String到Int的转化

                if (xmlElem.HasAttribute("ID"))
                {
                    if (int.TryParse(xmlElem.GetAttribute("ID"), out iTryTemp))
                    {
                        m_iID = iTryTemp;          // 任务ID
                    }
                }

                if (xmlElem.HasAttribute("Name"))
                {
                    m_strName = xmlElem.GetAttribute("Name");    // 任务名称
                }

                if (xmlElem.HasAttribute("Type"))
                {
                    m_strType = xmlElem.GetAttribute("Type");    // 任务类型
                }

                if (xmlElem.HasAttribute("SimpleDesc"))
                {
                    m_strSimpleDesc = xmlElem.GetAttribute("SimpleDesc");    // 任务的简单描述
                }

                if (xmlElem.HasAttribute("LeadMap"))
                {
                    if (int.TryParse(xmlElem.GetAttribute("LeadMap"), out iTryTemp))
                    {
                        m_iLeadMap = iTryTemp;          // 任务所属的地图
                    }
                }

                if (xmlElem.HasAttribute("StarLevel"))
                {
                    if (int.TryParse(xmlElem.GetAttribute("StarLevel"), out iTryTemp))
                    {
                        m_iStarLevel = iTryTemp; // 任务的难度（星级）
                    }
                }

                if (xmlElem.HasAttribute("TimeLimit"))
                {
                    if (int.TryParse(xmlElem.GetAttribute("TimeLimit"), out iTryTemp))
                    {
                        m_iTimeLimit = iTryTemp;// 任务的时间限制
                    }
                }


                try
                {
                    m_iClass = int.Parse(xmlElem.GetAttribute("Class"));
                    m_iLevel = int.Parse(xmlElem.GetAttribute("Level"));
                }
                catch (Exception)
                {
                    m_iClass = 0;
                    m_iLevel = 1;
                }

                try
                {
                    m_iRecordReserve = int.Parse(xmlElem.GetAttribute("RecordReserve"));
                }
                catch (Exception)
                {
                    m_iRecordReserve = 0;
                }

                if (xmlElem.HasAttribute("FlowType"))
                {
                    m_strFlowType = xmlElem.GetAttribute("FlowType");
                }

                if (xmlElem.HasAttribute("RegionProvider"))
                {
                    m_bRegionProvider = bool.Parse(xmlElem.GetAttribute("RegionProvider"));
                }

                m_strSubMissionType = xmlElem.GetAttribute("SubTaskType");
                m_bItemProvider = bool.Parse(xmlElem.GetAttribute("ItemProvider"));
                m_iProvider = int.Parse(xmlElem.GetAttribute("Provider"));
                m_iSubmitTo = int.Parse(xmlElem.GetAttribute("SubmitTo"));
                m_iTargetNPC = int.Parse(xmlElem.GetAttribute("TargetNPC"));

                //m_iTargetMonsterBegin = int.Parse(xmlElem.GetAttribute("TargetMonsterBegin"));
                //m_iTargetMonsterEnd = int.Parse(xmlElem.GetAttribute("TargetMonsterEnd"));
                //m_iTargetItemBegin = int.Parse(xmlElem.GetAttribute("TargetItemBegin"));
                //m_iTargetItemEnd = int.Parse(xmlElem.GetAttribute("TargetItemEnd"));

                //--------------------------------------------------2009-11-24 徐磊新增--------------------------------------------------//
                m_iTargetBegin = GetIntAttribute(xmlElem, "TargetBegin", -1);
                m_iTargetEnd = GetIntAttribute(xmlElem, "TargetEnd", -1);
                //--------------------------------------------------2009-11-24 徐磊新增--------------------------------------------------//

                if (xmlElem.HasAttribute("TargetArea"))
                {
                    m_iTargetArea = int.Parse(xmlElem.GetAttribute("TargetArea"));
                }
                m_iTargetProperty = int.Parse(xmlElem.GetAttribute("TargetProperty"));
                m_iTargetValue = int.Parse(xmlElem.GetAttribute("TargetValue"));
                try
                {
                    m_strTrackName = xmlElem.GetAttribute("TrackName");
                }
                catch (Exception)
                {
                    m_strTrackName = "";
                }

                m_bShare = bool.Parse(xmlElem.GetAttribute("Share"));
                m_bRepeat = bool.Parse(xmlElem.GetAttribute("Repeat"));
                m_bGiveUp = bool.Parse(xmlElem.GetAttribute("GiveUp"));

                XmlNode xmlNode = xmlElem.FirstChild;
                
                m_Conditions.Clear();
                m_FixedAwards.Clear();
                m_SelectedAwards.Clear();

                int iNumCData = 0;
                while (xmlNode != null)
                {
                    if (xmlNode is XmlElement)
                    {
                        XmlElement xmlElemSub = xmlNode as XmlElement;
                        if (xmlElemSub.Name.Equals("TaskCondition"))
                        {
                            String strCondition = xmlElemSub.GetAttribute("Condition");
                            String strValue = xmlElemSub.GetAttribute("Value");
                            m_Conditions.Add(new MissionCondition(strCondition, strValue));
                        }
                        else if (xmlElemSub.Name.Equals("FixedAwards"))
                        {
                            String strAward = xmlElemSub.GetAttribute("Award");
                            String strValue = xmlElemSub.GetAttribute("Value");
                            m_FixedAwards.Add(new MissionAward(strAward, strValue));
                        }
                        else if (xmlElemSub.Name.Equals("SelectedAwards"))
                        {
                            String strAward = xmlElemSub.GetAttribute("Award");
                            String strValue = xmlElemSub.GetAttribute("Value");
                            m_SelectedAwards.Add(new MissionAward(strAward, strValue));
                        }
                    }
                    else if (xmlNode is XmlCDataSection)
                    {

                        XmlCDataSection elmtCData = xmlNode as XmlCDataSection;
                        if (iNumCData == 0)
                        {
                            m_strGetDesc = elmtCData.Data;
                            iNumCData++;
                        }
                        else if (iNumCData == 1)
                        {
                            m_strSubmitDesc = elmtCData.Data;
                            iNumCData++;
                        }
                        else if (iNumCData == 2)
                        {
                            m_strSimpleDesc = elmtCData.Data;
                            iNumCData++;
                        }
                        else if(iNumCData == 3)
                        {
                            // 有4个CData说明是新版本
                            // 先处理错位的旧数据
                            m_strGetHint = m_strGetDesc;
                            m_strGetDesc = m_strSubmitDesc;
                            m_strSubmitDesc = m_strSimpleDesc;
                            // 读取数据
                            m_strSimpleDesc = elmtCData.Data;
                            iNumCData++;
                        }

                    }
                    xmlNode = xmlNode.NextSibling;
                }
            }
        }

        public XmlElement SaveToXmlNode(XmlDocument xmlDoc, XmlElement elem)
        {
            Mission mission = this;
            
            elem.SetAttribute("ID", mission.ID.ToString());
            elem.SetAttribute("Name", mission.Name);
            elem.SetAttribute("Desc", "");
            //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
            //elem.SetAttribute("SimpleDesc", mission.SimpleDesc);    // 任务的简单描述
            elem.SetAttribute("LeadMap", mission.LeadMap.ToString());          // 任务所属的地图
            elem.SetAttribute("StarLevel", mission.StarLevel.ToString()); // 任务的难度（星级）
            elem.SetAttribute("TimeLimit", mission.TimeLimit.ToString());// 任务的时间限制
            //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//

            elem.SetAttribute("Type", mission.Type);
            // 2009-01-08 徐磊 增加任务流程分类
            elem.SetAttribute("FlowType", mission.FlowType);
            // 2009-01-08 徐磊 增加任务流程分类
            // 将 MissionType 解析成整数
            OptionManager.MissionType missionType = (OptionManager.MissionType)Enum.Parse(typeof(OptionManager.MissionType), mission.Type);
            elem.SetAttribute("IntType", ((int)missionType).ToString());
            elem.SetAttribute("Class", mission.Class.ToString());
            elem.SetAttribute("Level", mission.Level.ToString());
            elem.SetAttribute("RecordReserve", mission.RecordReserve.ToString());
            elem.SetAttribute("SubTaskType", mission.SubMissionType);
            // 将 SubType 解析成整数
            int iIntSubType = -1;
            if (missionType == OptionManager.MissionType.对话类)
            {
                OptionManager.MissionTypeDialogSubType subType = (OptionManager.MissionTypeDialogSubType)Enum.Parse(typeof(OptionManager.MissionTypeDialogSubType), mission.SubMissionType);
                iIntSubType = (int)subType;
            }
            else if (missionType == OptionManager.MissionType.计数器类)
            {
                OptionManager.MissionTypeCounterSubType subType = (OptionManager.MissionTypeCounterSubType)Enum.Parse(typeof(OptionManager.MissionTypeCounterSubType), mission.SubMissionType);
                iIntSubType = (int)subType;
            }
            elem.SetAttribute("IntSubTaskType", iIntSubType.ToString());
            elem.SetAttribute("ItemProvider", mission.ItemProvider.ToString());
            elem.SetAttribute("RegionProvider", mission.RegionProvider.ToString()); // 2009-01-19 徐磊 增加区域触发任务
            elem.SetAttribute("Provider", mission.Provider.ToString());
            elem.SetAttribute("SubmitTo", mission.SubmitTo.ToString());

            // 任务接受条件
            foreach (MissionCondition condition in mission.Conditions)
            {
                XmlElement xmlElemCondition = xmlDoc.CreateElement("TaskCondition");
                xmlElemCondition.SetAttribute("Condition", condition.Condition);
                xmlElemCondition.SetAttribute("Value", condition.Value);

                elem.AppendChild(xmlElemCondition);
            }

            // 固定任务奖励
            foreach (MissionAward award in mission.FixedAwards)
            {
                XmlElement xmlElemAward = xmlDoc.CreateElement("FixedAwards");
                xmlElemAward.SetAttribute("Award", award.Award);
                xmlElemAward.SetAttribute("Value", award.Value);

                elem.AppendChild(xmlElemAward);
            }

            // 可选任务奖励
            foreach (MissionAward award in mission.SelectedAwards)
            {
                XmlElement xmlElemAward = xmlDoc.CreateElement("SelectedAwards");
                xmlElemAward.SetAttribute("Award", award.Award);
                xmlElemAward.SetAttribute("Value", award.Value);

                elem.AppendChild(xmlElemAward);
            }

            elem.SetAttribute("TargetNPC", mission.TargetNPC.ToString());
            //elem.SetAttribute("TargetMonsterBegin", mission.TargetMonsterBegin.ToString());
            //elem.SetAttribute("TargetMonsterEnd", mission.TargetMonsterEnd.ToString());
            //elem.SetAttribute("TargetItemBegin", mission.TargetItemBegin.ToString());
            //elem.SetAttribute("TargetItemEnd", mission.TargetItemEnd.ToString());
            //--------------------------------------------------2009-11-24 徐磊新增--------------------------------------------------//
            elem.SetAttribute("TargetBegin", mission.TargetBegin.ToString());
            elem.SetAttribute("TargetEnd", mission.TargetEnd.ToString());
            //--------------------------------------------------2009-11-24 徐磊新增--------------------------------------------------//
            elem.SetAttribute("TargetArea", mission.TargetArea.ToString());
            elem.SetAttribute("TargetProperty", mission.TargetProperty.ToString());
            elem.SetAttribute("TargetValue", mission.TargetValue.ToString());
            elem.SetAttribute("TrackName", mission.TrackName);
            // 目标区域
            // ...

            elem.SetAttribute("Share", mission.Share.ToString());
            elem.SetAttribute("Repeat", mission.Repeat.ToString());
            elem.SetAttribute("GiveUp", mission.GiveUp.ToString());

            mission.GetDescription.Replace("\0", "");
            mission.SubMissionType.Replace("\0", "");

            XmlCDataSection elemCData = xmlDoc.CreateCDataSection(mission.GetHint);
            elem.AppendChild(elemCData);
            elemCData = xmlDoc.CreateCDataSection(mission.GetDescription);
            elem.AppendChild(elemCData);
            elemCData = xmlDoc.CreateCDataSection(mission.SubmitDescription);
            elem.AppendChild(elemCData);
            elemCData = xmlDoc.CreateCDataSection(mission.SimpleDesc);
            elem.AppendChild(elemCData);

            return elem;
        }

        public void Copy(Mission mission)
        {
            if(mission != null)
            {
                XmlDocument doc = new XmlDocument();
                XmlElement elem = doc.CreateElement("Task");
                LoadFromXmlNode(mission.SaveToXmlNode(doc, elem));
            }            
        }

        #endregion

        #region 内部使用函数
        
        private int GetIntAttribute(XmlElement elem, String name, int defaultvalue)
        {
            if(elem == null || name == null)
            {
                return defaultvalue;
            }

            if (elem.HasAttribute(name))
            {
                int iTemp;
                String strTemp = elem.GetAttribute(name);
                if (int.TryParse(strTemp, out iTemp))
                {
                    return iTemp;
                }
                else
                {
                    return defaultvalue;
                }
            }
            else
            {
                return defaultvalue;
            }
        }
        
        #endregion

        #region 私有成员变量定义

        // 所有任务都有的属性
        private int m_iID;                  // 任务编号
        private String m_strName;           // 任务名称
        private String m_strGetDesc;           // 任务描述
        private String m_strSubmitDesc;           // 任务描述
        private String m_strType;           // 任务类型
        // 2009-01-08 徐磊 增加任务流程分类
        private String m_strSubMissionType; // 细分任务类型。 计数器类对应 杀人，杀怪，收集物品。对话类对应简单对话，送物品，取物品 
        // 2009-01-08 徐磊 增加任务流程分类
        private String m_strFlowType = "B类";       // 任务流程类型，A为2阶，B为3阶
        private int m_iClass;          // 任务阶级 0-新手任务 1-主线任务 2-支线任务 3-副本任务 4-活动任务 5-系统任务
        private int m_iLevel;          // 任务等级
        private int m_iRecordReserve;   // 是否保留记录

        //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
        private String m_strSimpleDesc = "";    // 任务的简单描述
        private int m_iLeadMap = 0;          // 任务所属的地图
        private int m_iStarLevel = 1;           // 任务的难度（星级）
        private int m_iTimeLimit = 0;           // 任务的时间限制
        //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//

        //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//
        private String m_strGetHint = "";
        //--------------------------------------------------2009-01-05 徐磊新增--------------------------------------------------//

        private bool m_bItemProvider;       // true - 使用道具获取任务   false - 和 NPC 对话获取任务
        private bool m_bRegionProvider = false; // 区域触发任务     
        private int m_iProvider;            // 任务提供者 creator id
        private int m_iSubmitTo;            // 交任务对象 creator id
        private ArrayList m_Conditions = new ArrayList();     // 接任务条件  MissionAward 列表
        private ArrayList m_FixedAwards = new ArrayList();    // 固定奖励奖励 String - int
        private ArrayList m_SelectedAwards = new ArrayList(); // 可选奖励 String - int

        // 以下根据任务类型可选
        private int m_iTargetNPC;       // 目标 npc creator id
        
        private int m_iTargetMonsterBegin;   // 目标怪物 npc id 范围
        private int m_iTargetMonsterEnd;   // 目标怪物 npc id
        private int m_iTargetItemBegin;      // 目标物件
        private int m_iTargetItemEnd;      // 目标物件

        //--------------------------------------------------2009-11-24 徐磊新增--------------------------------------------------//
        // 不再区分类型，统一的处理
        private int m_iTargetBegin;     // 目标起始
        private int m_iTargetEnd;       // 目标终止
        //--------------------------------------------------2009-11-24 徐磊新增--------------------------------------------------//

        private int m_iTargetArea;      // 目标区域
        private int m_iTargetProperty;  // 目标属性
        private int m_iTargetValue;     // 目标值
        private String m_strTrackName;  // 追踪目标名称
        private bool m_bShare;          // 共享任务
        private bool m_bRepeat;         // 可重复任务
        private bool m_bGiveUp;         // 可放弃任务

        // 任务的状态
        private int m_iTeamWorkStatus = 1;  // 这个状态用于标记，任务处于签入，签出，新建状态
#endregion
    }
}
