﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MissionEditor
{
    public class MissionAward
    {
        public MissionAward(String strAward, String  strValue)
        {
            m_strAward = strAward;
            m_strValue = strValue;
        }

        public String Award
        {
            get
            {
                return m_strAward;
            }
            set
            {
                m_strAward = value;
            }
        }

        public String Value
        {
            get
            {
                return m_strValue;
            }
            set
            {
                m_strValue = value;
            }
        }

        // 将
        public String ToLua(int iTabs)
        {
            return "";
        }

        private String m_strAward;    // 需求,对应 MissionBaseData.xml 中 Award 的第二级
        private String m_strValue;   // 对应值 0
    }
}
