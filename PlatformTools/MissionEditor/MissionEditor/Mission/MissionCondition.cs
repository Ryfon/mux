﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MissionEditor
{
    // 任务接受条件实例类.
    class MissionCondition
    {
        //public MissionCondition(String strCondition, int iValue0, int iValue1)
        //{
        //    m_strCondition = strCondition;
        //    m_iValue0 = iValue0;
        //    m_iValue1 = iValue1;
        //}

        public MissionCondition(String strCondition, String strValue)
        {
            m_strCondition = strCondition;
            m_strValue = strValue;
        }

        // 克隆
        public MissionCondition Clone()
        {
            return new MissionCondition(m_strCondition, m_strValue);
        }

        public String Condition
        {
            get
            {
                return m_strCondition;
            }
            set
            {
                m_strCondition = value;
            }
        }

        public String Value
        {
            get
            {
                return m_strValue;
            }
            set
            {
                m_strValue = value;
            }
        }

        //public int Value0
        //{
        //    get
        //    {
        //        return m_iValue0;
        //    }
        //    set
        //    {
        //        m_iValue0 = value;
        //    }
        //}

        //public int Value1
        //{
        //    get
        //    {
        //        return m_iValue1;
        //    }
        //    set
        //    {
        //        m_iValue1 = value;
        //    }
        //}

        private String m_strCondition;    // 需求,对应 MissionBaseData.xml 中的第二级
        private String m_strValue;        // 条件对应值
        //private int m_iValue0;   // 对应值0
        //private int m_iValue1;   // 对应值1
    }
}
