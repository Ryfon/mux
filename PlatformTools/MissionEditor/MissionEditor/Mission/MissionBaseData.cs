﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;

namespace MissionEditor
{
    class MissionBaseData
    {
        // 操作

        // 默认构造函数
        public MissionBaseData()
        {

        }

        // 通过一个xml节点来构造
        public MissionBaseData(XmlElement xmlNode)
        {
            if(xmlNode != null)
            {
                if(xmlNode.HasAttribute("Display"))
                {
                    m_strDisplay = xmlNode.GetAttribute("Display");
                }

                if (xmlNode.HasAttribute("Atom"))
                {
                    m_strAtom = xmlNode.GetAttribute("Atom");
                }

                if (xmlNode.HasAttribute("Desc"))
                {
                    m_strDesc = xmlNode.GetAttribute("Desc");
                }

                if (xmlNode.HasAttribute("Param"))
                {
                    m_strParam = xmlNode.GetAttribute("Param");
                    m_strParam = m_strParam.Replace("@@", "\r\n");
                }
            }
        }

        // 将属性转化到一个xml节点中
        public XmlElement ToXmlElement(XmlDocument xmlDoc, String strName)
        {
            if(xmlDoc == null)
            {
                return null;
            }
            else
            {
                XmlElement elem = xmlDoc.CreateElement(strName);
                elem.SetAttribute("Display", m_strDisplay);
                elem.SetAttribute("Atom", m_strAtom);
                elem.SetAttribute("Desc", m_strDesc);
                elem.SetAttribute("Param", m_strParam.Replace("\r\n", "@@"));
                return elem;
            }
        }

        //////////////////////////////////////////////////////////////////////////
        // 公有属性
        public String Display
        {
            get
            {
                return m_strDisplay;
            }
            set
            {
                m_strDisplay = value;
            }
        }

        public String Atom
        {
            get
            {
                return m_strAtom;
            }
            set
            {
                m_strAtom = value;
            }
        }

        public String Desc
        {
            get
            {
                return m_strDesc;
            }
            set
            {
                m_strDesc = value;
            }
        }

        public String Param
        {
            get
            {
                return m_strParam;
            }
            set
            {
                m_strParam = value;
            }
        }

        //////////////////////////////////////////////////////////////////////////
        // 私有成员变量
        private String m_strDisplay = "";   // 显示的名称
        private String m_strAtom = "";      // 原子操作
        private String m_strDesc = "";      // 功能描述
        private String m_strParam = "";     // 参数说明
    }

    class MissionBaseDataManager
    {
        // 对外接口
        public bool LoadFromXml(String strXmlFilename)
        {
            // 加载文件
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(strXmlFilename);
            if(xmlDoc == null)
            {
                return false;
            }

            // 获取节点
            XmlElement xmlElemRoot = xmlDoc.LastChild as XmlElement;
            if(xmlElemRoot == null)
            {
                return false;
            }
            
            // 遍历读取数据
            foreach(XmlElement xmlElemNode in xmlElemRoot.ChildNodes)
            {
                // 根据节点类型，分别读取
                if (xmlElemNode.Name.Equals("MissionClasses"))
                {
                    m_XmlMissionClassesNode = xmlElemNode.CloneNode(true) as XmlElement;
                }
                else if (xmlElemNode.Name.Equals("Requirements"))
                {
                    m_RequirementsList = new Hashtable();

                    foreach(XmlElement xmlReqType in xmlElemNode.ChildNodes)
                    {
                        if (xmlReqType.HasAttribute("Display"))
                        {
                            String strReqTypeName = xmlReqType.GetAttribute("Display");
                            Hashtable ReqType = new Hashtable();
                            m_RequirementsList[strReqTypeName] = ReqType;

                            foreach(XmlElement xmlReq in xmlReqType.ChildNodes)
                            {
                                MissionBaseData req = new MissionBaseData(xmlReq);
                                ReqType[req.Display] = req;
                            }
                        }
                    }
                }
                else if (xmlElemNode.Name.Equals("Awards"))
                {
                    m_AwardsList = new Hashtable();

                    foreach (XmlElement xmlAwdType in xmlElemNode.ChildNodes)
                    {
                        if (xmlAwdType.HasAttribute("Display"))
                        {
                            String strAwdTypeName = xmlAwdType.GetAttribute("Display");
                            Hashtable AwdType = new Hashtable();
                            m_AwardsList[strAwdTypeName] = AwdType;

                            foreach (XmlElement xmlAwd in xmlAwdType.ChildNodes)
                            {
                                MissionBaseData awd = new MissionBaseData(xmlAwd);
                                AwdType[awd.Display] = awd;
                            }
                        }
                    }
                }
                else if (xmlElemNode.Name.Equals("AtomActions"))
                {
                    m_AtomActionsList = new Hashtable();

                    foreach(XmlElement xmlAtom in xmlElemNode.ChildNodes)
                    {
                        if (xmlAtom.HasAttribute("Display"))
                        {
                            String strAtomName = xmlAtom.GetAttribute("Display");
                            MissionBaseData atom = new MissionBaseData(xmlAtom);
                            m_AtomActionsList[atom.Display] = atom;
                        }
                    }
                }
            }

            return true;
        }

        public void SaveToXml(String strXmlFilename)
        {
            // 定义doc文件
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "GB2312", "yes");
            xmlDoc.AppendChild(xmlDeclaration);

            // 根节点
            XmlElement root = xmlDoc.CreateElement("root");
            root.SetAttribute("FileVersion", "0.9");
            xmlDoc.AppendChild(root);

            //任务Class节点
            if(m_XmlMissionClassesNode != null)
            {
                XmlElement xmlClass = xmlDoc.CreateElement("MissionClasses");
                foreach (XmlElement xmlClassNode in m_XmlMissionClassesNode.ChildNodes)
                {
                    XmlElement newClass = xmlDoc.CreateElement("MissionClass");
                    newClass.SetAttribute("Display", xmlClassNode.GetAttribute("Display"));
                    newClass.SetAttribute("Value", xmlClassNode.GetAttribute("Value"));
                    xmlClass.AppendChild(newClass);
                }
                root.AppendChild(xmlClass);
            }

            // 任务条件节点
            XmlElement xmlReq = xmlDoc.CreateElement("Requirements");
            root.AppendChild(xmlReq);

            foreach(String strTypeName in m_RequirementsList.Keys)
            {
                XmlElement xmlReqType = xmlDoc.CreateElement("RequirementType");
                xmlReqType.SetAttribute("Display", strTypeName);
                xmlReq.AppendChild(xmlReqType);

                Hashtable table = m_RequirementsList[strTypeName] as Hashtable;
                foreach(MissionBaseData req in table.Values)
                {
                    xmlReqType.AppendChild(req.ToXmlElement(xmlDoc, "Requirement"));
                }
            }

            // 任务奖励节点
            XmlElement xmlAwd = xmlDoc.CreateElement("Awards");
            root.AppendChild(xmlAwd);

            foreach (String strTypeName in m_AwardsList.Keys)
            {
                XmlElement xmlAwdType = xmlDoc.CreateElement("AwardType");
                xmlAwdType.SetAttribute("Display", strTypeName);
                xmlAwd.AppendChild(xmlAwdType);

                Hashtable table = m_AwardsList[strTypeName] as Hashtable;
                foreach (MissionBaseData awd in table.Values)
                {
                    xmlAwdType.AppendChild(awd.ToXmlElement(xmlDoc, "Award"));
                }
            }

            // 任务atom节点
            XmlElement xmlAtom = xmlDoc.CreateElement("AtomActions");
            root.AppendChild(xmlAtom);

            foreach (MissionBaseData atom in m_AtomActionsList.Values)
            {
                xmlAtom.AppendChild(atom.ToXmlElement(xmlDoc, "AtomAction"));
            }

            // 保存文件
            xmlDoc.Save(strXmlFilename);

            m_bChanged = true;
        }

        // 公有属性
        public static MissionBaseDataManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new MissionBaseDataManager();
                }

                return ms_Instance;
            }
        }

        public bool Changed
        {
            get
            {
                return m_bChanged;
            }
            set
            {
                m_bChanged = value;
            }
        }

        public Hashtable RequireList
        {
            get
            {
                return m_RequirementsList;
            }
        }

        public Hashtable AwardList
        {
            get
            {
                return m_AwardsList;
            }
        }

        private MissionBaseDataManager()
        {}

        ///////////////////////////////////////////////////////////////////////////////
        // 私有成员变量
        private static MissionBaseDataManager ms_Instance = null;   // singleton模式

        private XmlElement m_XmlMissionClassesNode = null;  // 任务class节点

        private Hashtable m_RequirementsList = null;    // 任务条件列表
        private Hashtable m_AwardsList = null;          // 任务奖励列表
        private Hashtable m_AtomActionsList = null;         // 原子操作列表

        private bool m_bChanged = false;    // 是否被编辑过
    };
}
