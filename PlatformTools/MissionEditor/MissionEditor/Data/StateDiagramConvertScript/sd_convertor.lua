-- 将任务表单 -> 状态图 转换器

--///////////////////////////
-- 转换函数，唯一的对外接口。
--///////////////////////////

function convert(iMissionID)
	-- 首先按照一般情况将任务转换为转态图。
	-- 然后根据不同的任务类型进行特别的修改。
	
	EX_CreateLogFile("sd_convert.log");	-- 创建 Log 文件
	
	-------------------------------------------------
	-- 1. 根据任务 ID 获取 LuaTable 形式的任务数据 --
	-------------------------------------------------
	mission_tab = EX_GetMissionTable(iMissionID);	-- 任务数据
	if (mission_tab == nil) then	-- 如果该 id 的任务不存在
		EX_ShowErrorMessage("目标任务没有找到。请检查任务 ID。");
		return -1;
	end
	EX_Log("1 - 获取 LuaTable 完成.\n");
	
	----------------------------------------------
	-- 2. 找出任务领取者对应状态图 --
	----------------------------------------------
	stateDiaID_Name = EX_GenerateStateDiagramID_Name(iMissionID, 0);	-- 任务提供者对应的状态图ID/名称
	iDiaID = stateDiaID_Name[0];
	strDiaName = stateDiaID_Name[1];
	EX_Log("生成状态图 name, id 完成.\n");
	
	if (EX_IsStateDiagramExisted(strDiaName)) then
		EX_TestBool(true);
	else
		EX_TestBool(false);
	end
	
	-- 2.1 如果状态图还不存在，创建并初始化. 设置其为当前操作状态图
	if (EX_IsStateDiagramExisted(strDiaName) == false) then
		if (EX_GenerateStateDiagram(iDiaID, strDiaName) < 0) then	-- 创建失败
			EX_ShowErrorMessage("创建状态图: "..stateDiaID_Name[1].." 失败.");
			return;
		end
		EX_SetOpStateDiagram(strDiaName);	-- 设置为当前被操作状态图
		
		if (_InitStateDiagram(iMissionID) < 0) then
			-- 如果初始化状态图失败
			EX_ShowErrorMessage("初始化状态图失败.");
			return -1;
		end
	else
		EX_SetOpStateDiagram(strDiaName);	-- 设置为当前被操作状态图
	end
	EX_Log("2 - 创建状态图"..strDiaName.." 完成.\n");
		
	----------------------------------------------
	-- 3. 添加/更新 获取任务相关状态 --
	----------------------------------------------
	-- 3.1 MXXX_GET_DLG
	iGET_DLG_StateID = _Add_MXXX_GET_DLG_State(iMissionID, mission_tab);
	EX_Log("3.1 - 创建状态 MXXX_GET_DLG 完成.\n");
	
	-- 3.2 MXXX_GET
	_Add_MXXX_GET_State(iMissionID, mission_tab, iGET_DLG_StateID);
	EX_Log("3.2 - 创建状态 MXXX_GET 完成.\n");
	
	-- 3.3 MXXX_GET_HINT
	_Add_MXXX_GET_HINT_DLG_State(iMissionID, mission_tab);
	EX_Log("3.3 - 创建状态 MXXX_GET_HINT 完成.\n");
	
	
	----------------------------------------------
	-- 4. 找出任务执行者对应的状态图 --
	----------------------------------------------
	if (mission_tab["flow_type"] == "A类") then
		-- A类 两阶段任务 无任务执行者
	elseif (mission_tab["flow_type"] == "B类") then
		-- B类 三阶段任务 
		stateDiaID_Name = EX_GenerateStateDiagramID_Name(iMissionID, 1);	-- 任务提供者对应的状态图ID/名称
		iDiaID = stateDiaID_Name[0];
		strDiaName = stateDiaID_Name[1];
		
	end
	
	EX_SaveStateDiagram(strDiaName);	-- 每创建完一个状态图后将其保存到磁盘
	
	EX_SaveLogFile();		-- 保存 Log 文件.
	
end	-- End of function

--////////////////////////////////////////////////////////////
-- 初始化一个状态图,为其添加 IDLE 状态 和 DEFAULT_DLG 状态
--////////////////////////////////////////////////////////////
function _InitStateDiagram(iMissionID)
	-- 添加状态
	iIdleID = EX_AddState("IDLE", iMissionID, 0);
	iDefDlgID = EX_AddState("DEFAULT_DLG", iMissionID, 0);
	
	if (iIdleID<0 or iDefDlgID<0) then
		return -1;
	end
	
	-- 为 DEFAULT_DLG 添加默认对话
	EX_AddAtomAction(iDefDlgID, 0, "ShowDialog", "");
	EX_SetDialog(iDefDlgID, "天气好热阿!");
	EX_AddOption(iDefDlgID, iMissionID, "是啊.", nil, 1);
	
	-- 为两个状态建立连接
	if (EX_AddTranslation(iIdleID, iDefDlgID, -1, nil) < 0) then
		return -1;
	end
	
	return 0;	-- Sucess return
end

--////////////////////////////////////////////////////////////
-- 3.1 添加/更新 MXXX_GET_DLG 状态 --
--////////////////////////////////////////////////////////////
function _Add_MXXX_GET_DLG_State(iMissionID, mission_tab)
		-- DEFAULT_DLG ID
	iDefDlgID = EX_GetStateID("DEFAULT_DLG");	
	
	-- 3.1 MXXX_GET_DLG
	strStateName = "M"..iMissionID.."_GET_DLG";			-- MXXX_GET_DLG 状态名称
	iGET_DLG_StateID = EX_GetStateID(strStateName);		-- MXXX_GET_DLG 状态 ID
	
	iOp = -1;	-- MXXX_GET_DLG 对应在 DEFAULT_DLG 中的选项
	if (iGET_DLG_StateID < 0) then	-- 该状态不存在
		-- 3.1.a 添加 MXXX_GET_DLG 状态
		
		-- 3.1.1 添加对话
		iGET_DLG_StateID = EX_AddState(strStateName, iMissionID, 1);	-- 添加状态
		EX_AddAtomAction(iGET_DLG_StateID, 0, "ShowDialog", "");			-- 添加"显示对话"原子操作
		EX_SetDialog(iGET_DLG_StateID, "TaskAcceptUI,"..iMissionID);	-- 标准接受任务 UI
		EX_Log("3.1.1 - done \n");
		
		-- 3.1.2 添加"接收","不接受"两个选项
		-- 这里必须先添加 "接受" 再添加 "不接受"."接收"对应 option 值为 1, "不接受"对应值为 2
		iAcceptOpID = EX_AddOption(iGET_DLG_StateID, iMissionID, "接受", nil, 1);		-- 添加"接受"选项
		iRefuseOpID = EX_AddOption(iGET_DLG_StateID, iMissionID, "不接受", nil, 1);	-- 添加"不接受"选项
		EX_Log("3.1.2 - done \n");
		
		-- 3.1.3 在 DEFAULT_DLG 中添加选项
		iOp = EX_AddOption(iDefDlgID, iMissionID, "[[接任务]]关于任务"..mission_tab["name"], nil, 1);	-- 添加选项
		EX_Log("3.1.3 - done \n");
		
		-- 3.1.4 建立 DEFAULT_DLG 到 MXXX_GET_DLG 的连接
		EX_AddTranslation(iDefDlgID, iGET_DLG_StateID, iOp, nil);
		EX_Log("3.1.4 - done \n");
		
		-- 3.1.5 建立 MXXX_GET_DLG 到 DEFAULT_DLG 的连接
		EX_AddTranslation(iGET_DLG_StateID ,iDefDlgID , 2, nil);
		EX_Log("3.1.5 - done \n");
		
	else
		-- 3.1.b 更新
		-- 获取 DEFAULT_DLG 中对应 MXXX_GET_DLG 的选项
		iOp = EX_GetOption(iDefDlgID, iMissionID, 1);	-- (iStateID, iMissionID, iElementCategory)
		EX_ClearOptionConditions(iDefDlgID, iOp);	--	清空条件		
		EX_Log("3.1.b - update done \n");
	end	-- MXXX_GET_DLG 状态完成
	
	-- 设置 DEFAULT_DLG 中的选项条件
	EX_AddOptionConditions(iDefDlgID, iOp, mission_tab["conditions"]);

	-- 任务接受条件列表
		
	-- 在 arrConditions 中添加其他隐含条件. 比如不在列表中,角色等级
	if (mission_tab["repeat"]) then
		-- 如果是可重复任务
		EX_AddOptionCondition(iDefDlgID, iOp, "不在任务列表", ""..iMissionID);	-- 	
	else
		-- 不可重复任务
		EX_AddOptionCondition(iDefDlgID, iOp, "从未接受过任务", ""..iMissionID);	-- 	
	end

	-- 任务的等级条件
	iReqLevel = math.max(0, mission_tab["level"]-6);
	EX_AddOptionCondition(iDefDlgID, iOp, "属性超过", ""..iReqLevel);
				
	return iGET_DLG_StateID;	-- 返回 MXXX_GET_DLG 的 ID
end	-- End of function


--////////////////////////////////////////////////////////////
-- 3.2 添加/更新 MXXX_GET_DLG 状态 --
--////////////////////////////////////////////////////////////
function _Add_MXXX_GET_State(iMissionID, mission_tab, iGET_DLG_StateID)
	strStateName = "M"..iMissionID.."_GET";		-- MXXX_GET_DLG 状态名称
	iGET_StateID = EX_GetStateID(strStateName);	-- MXXX_GET 状态 ID
	
	if (iGET_StateID < 0) then	-- 该状态不存在
		-- 3.2.1 创建 MXXX_GET 状态
		iGET_StateID = EX_AddState(strStateName, iMissionID, 1);
		if (iGET_StateID < 0) then
			EX_ShowErrorMessage("创建 MXXX_GET state 失败.");
			return -1;
		end
		EX_Log("3.2.1 - done \n");
		
		-- 3.2.2 在 MXXX_GET 状态 OnEnter 段添加 GiveTask 操作
		EX_AddAtomAction(iGET_StateID, 0, "GiveTask", ""..iMissionID);
		EX_Log("3.2.2 - done \n");
		
		-- 3.2.3 在 OnEnter 段中添加其他与任务相关的操作
		-- 放在基本结构生成完毕后再做
		
		-- 3.2.4 建立 MXXX_GET_DLG 到 MXXX_GET 的连接, 对应 iOp 值为 1
		EX_AddTranslation(iGET_DLG_StateID, iGET_StateID, 1, nil);
		EX_Log("3.2.4 - done \n");
		
	else
		-- 3.2.b Update
		EX_ClearAtomActions(iGET_StateID, 0);
		EX_AddAtomAction(iGET_StateID, 0, "GiveTask", ""..iMissionID);
		EX_Log("3.2.b - upeate done \n");
	end
	
	return iGET_StaetID;
end -- end of function

--////////////////////////////////////////////////////////////
-- 3.3 添加/更新 MXXX_GET_HINT_DLG 状态 --
--////////////////////////////////////////////////////////////
function _Add_MXXX_GET_HINT_DLG_State(iMissionID, mission_tab)
	-- DEFAULT_DLG ID
	iDEFAULT_DLG_ID = EX_GetStateID("DEFAULT_DLG");	
	
	strStateName = "M"..iMissionID.."_GET_HINT_DLG";			-- MXXX_GET_HINT_DLG 状态名称
	iGET_HINT_DLG_StateID = EX_GetStateID(strStateName);	-- MXXX_GET_HINT_DLG 状态 ID
	
	-- 3.3.1 添加 MXXX_GET_HINT_DLG 状态
	if (iGET_HINT_DLG_StateID < 0) then	-- 该状态不存在
		-- 3.3.1 创建 MXXX_GET_HINT_DLG 状态
		iGET_HINT_DLG_StateID = EX_AddState(strStateName, iMissionID, 1);
		if (iGET_HINT_DLG_StateID < 0) then
			EX_ShowErrorMessage("创建 MXXX_GET_HINT_DLG state 失败.");
			return -1;
		end
		EX_Log("3.3.1 - done \n");
		
		-- 3.3.2 在 MXXX_GET_HINT_DLG 状态 OnEnter 段添加 ShowDialog 操作
		EX_AddAtomAction(iGET_HINT_DLG_StateID, 0, "ShowDialog", "");			-- 添加"显示对话"原子操作
		EX_SetDialog(iGET_HINT_DLG_StateID, "快去做任务 "..mission_tab["name"].."吧.");	-- 任务提示
		EX_Log("3.3.2 - done \n");
		
		-- 3.3.3 在 DEFAULT_DLG 中添加选项
		arrConditions = {"在任务列表未达成目标", ""..iMissionID};	--	选项条件
		iOp = EX_AddOption(iDEFAULT_DLG_ID, iMissionID, "[[任务提示]]关于任务 "..mission_tab["name"], nil, 2);
		EX_Log("3.3.3 - done \n");
		
		-- 3.3.4 建立 DEFAULT_DLG 到 MXXX_GET 的连接, 对应 iOp 值为 1
		EX_AddTranslation(iDEFAULT_DLG_ID, iGET_HINT_DLG_StateID, iOp, nil);
		EX_Log("3.3.4 - done \n");
		
	else
		-- do nothing
	end
	
	return iGET_HINT_DLG_StateID;
end -- end of function