﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace MissionEditor
{
    // 原子操作实例
    public class AtomActionInstance
    {
        public AtomActionInstance(String strDisplay, String strParameters)
        {
            m_strAtomDisplay = strDisplay;
            // m_strAtom = AtomActionManager.Instance.AtomActionList[strDisplay] as String;
            MissionAtomInstance mti = AtomActionManager.Instance.AtomActionList[strDisplay] as MissionAtomInstance;
            if(mti != null)
            {
                m_strAtom = mti.Atom;
                m_strParameters = strParameters;
            }
            else
            {
                MessageBox.Show("找不到原子操作---"+strDisplay);
            }
        }

        public AtomActionInstance Clone()
        {
            return new AtomActionInstance(m_strAtomDisplay, m_strParameters);
        }

        public String Atom
        {
            get
            {
                return m_strAtom;
            }
            set
            {
                m_strAtom = value;
            }
        }

        public String AtomDisplay
        {
            get
            {
                return m_strAtomDisplay;
            }
            set
            {
                m_strAtomDisplay = value;
            }
        }

        public String Parameters
        {
            get
            {
                return m_strParameters;
            }
            set
            {
                m_strParameters = value;
            }
        }

        private String m_strAtom;           // 实际 Lua 脚本中调用的原子操作
        private String m_strAtomDisplay;    // 编辑器 UI 中显示的原子操作名称。
        private String m_strParameters;     // 原子操作对应的参数。
    }
}
