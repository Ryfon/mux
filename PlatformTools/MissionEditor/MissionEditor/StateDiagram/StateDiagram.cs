﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Xml;

namespace MissionEditor
{
 

    // 状态图。包含若干 state 和 translation
    public class StateDiagram
    {
        public StateDiagram()
        {
        }

        public StateDiagram(XmlElement xmlDiagramElem)
        {
            m_strName = xmlDiagramElem.GetAttribute("Name");
            m_iID = int.Parse(xmlDiagramElem.GetAttribute("ID"));
            XmlElement xmlElem = xmlDiagramElem.FirstChild as XmlElement;
            while (xmlElem != null)
            {
                if (xmlElem.Name == "State")
                {
                    State state = new State(xmlElem);
                    m_StateList.Add(state);
                }
                else if (xmlElem.Name == "Translation")
                {
                    Translation trans = new Translation(xmlElem);
                    m_TranslationList.Add(trans);
                }
                xmlElem = xmlElem.NextSibling as XmlElement;
            }

            m_bDirty = false;
        }

        // 组成状态图元素所属的类型，是与领取任务相关的，执行任务相关的或者还任务相关的。
        public enum ElementCategory : int
        {
            EC_OTHER = 0,
            EC_GET,
            EC_GET_HINT,
            EC_PROCESS,
            EC_PROCESS_HINT,
            EC_SUBMIT,
            EC_INVALIDATE,
        }

        // 状态图宿主的类型
        public enum OwnerType : int
        {
            DT_NPC = 1,
            DT_NPC_DEATH = 2,
            DT_MONSTER_DEATH = 3,
            DT_ITEM = 4,
            DT_AREA = 5,
            DT_INVALID,
        }


        // 获取某种类型对话的 ID前缀
        public static int GenerateStateDiagramID(StateDiagram.OwnerType dt, int iOwnerID)
        {
            // 关于 字符串 ID 的规则：
            // NPC对话：1 + NPCID后5位 + 3位流水号
            // NPC死亡：2 + NPCID后5位 + 3位流水号
            // 怪物死亡：3 + 怪物ID后5位 + 3位流水号
            // 物品脚本：4 + ItemID后5位 + 3位流水号
            // 区域脚本：5 + 6位区域ID + 2位流水号

            // 检查 iOwnerID 位数是否正确
            int iID = -1;
            String strOwnerID = iOwnerID.ToString();
            if (dt == StateDiagram.OwnerType.DT_NPC ||
                dt == StateDiagram.OwnerType.DT_NPC_DEATH)
            {
                if (strOwnerID.Length < 5)
                {
                    return -1;
                }
                else if (strOwnerID.Length == 5)
                {
                    iID = int.Parse(((int)(dt)).ToString() + iOwnerID.ToString());
                }
                else
                {
                    iID = int.Parse(((int)(dt)).ToString() + strOwnerID.Substring(strOwnerID.Length-5));
                }
            }
            else if (dt == StateDiagram.OwnerType.DT_MONSTER_DEATH ||
                     dt == StateDiagram.OwnerType.DT_ITEM)
            {
                if (strOwnerID.Length < 6)
                {
                    return -1;
                }
                else
                {
                    int iLen = strOwnerID.Length;
                    iID = int.Parse(((int)dt).ToString() + iOwnerID.ToString().Substring(iLen - 5));
                }
            }
            else if (dt == StateDiagram.OwnerType.DT_AREA)
            {
                if (strOwnerID.Length != 6)
                {
                    return -1;
                }
                else
                {
                    iID = int.Parse(((int)(dt)).ToString() + iOwnerID.ToString());
                }
            }
            else
            {
                return -1;
            }

            return iID;
        }

        public XmlElement GetAsXmlElement(XmlDocument xmlDoc)
        {
            XmlElement xmlDiagramElem = xmlDoc.CreateElement("StateDiagram");
            xmlDiagramElem.SetAttribute("ID", m_iID.ToString());
            xmlDiagramElem.SetAttribute("Name", (m_strName == null) ? "" : m_strName);
            foreach (State state in m_StateList)
            {
                XmlElement xmlStateElem = state.GetAsXmlElement(xmlDoc);
                xmlDiagramElem.AppendChild(xmlStateElem);
            }

            foreach (Translation translation in m_TranslationList)
            {
                XmlElement xmlTransElem = translation.GetAsXmlElement(xmlDoc);
                xmlDiagramElem.AppendChild(xmlTransElem);
            }

            return xmlDiagramElem;
        }

        public void LoadFromXmlElement(XmlElement xmlDiagramElem)
        {
            // 清空旧的数据
            m_StateList.Clear();
            m_TranslationList.Clear();

            // 获取名字和ID
            m_strName = xmlDiagramElem.GetAttribute("Name");
            m_iID = int.Parse(xmlDiagramElem.GetAttribute("ID"));

            // 遍历获取状态和连接
            XmlElement xmlElem = xmlDiagramElem.FirstChild as XmlElement;
            while (xmlElem != null)
            {
                if (xmlElem.Name == "State")
                {
                    State state = new State(xmlElem);
                    m_StateList.Add(state);
                }
                else if (xmlElem.Name == "Translation")
                {
                    Translation trans = new Translation(xmlElem);
                    m_TranslationList.Add(trans);
                }
                xmlElem = xmlElem.NextSibling as XmlElement;
            }

            // 标志位
            m_bDirty = false;
        }

        public void Copy(StateDiagram dia)
        {
            if(dia != null)
            {
                XmlDocument doc = new XmlDocument();
                LoadFromXmlElement(dia.GetAsXmlElement(doc));
            }
        }
        // 根据 mission id 删除状态
        public void RemoveStateByMissionID(int iID)
        {
            ArrayList removeList = new ArrayList();
            foreach (State state in m_StateList)
            {
                if (state.MissionID == iID)
                {
                    foreach (Translation translation in m_TranslationList)
                    {
                        if (translation.StartStateID == state.ID || translation.EndStateID == state.ID)
                        {
                            removeList.Add(translation);
                        }
                    }

                    removeList.Add(state);
                }
                else 
                {
                    // 如果 state 与该任务无关,检查该s state 对应 option 是否与该任务相关
                    ArrayList optRemoveList = new ArrayList();
                    foreach (DialogOption option in state.OptionList)
                    {
                        if (option.MissionID == iID)
                        {
                            optRemoveList.Add(option);
                        }
                    }

                    foreach (DialogOption option in optRemoveList)
                    {
                        state.OptionList.Remove(option);
                    }
                }
            }

            foreach (Object obj in removeList)
            {
                if (obj is State)
                {
                    m_StateList.Remove(obj);
                }
                else
                {
                    m_TranslationList.Remove(obj);
                }
                
            }

            m_bDirty = true;
        }

        // 根据任务 id 和类型删除状态图中的元素
        public void RemoveElement(int iMissionID, StateDiagram.ElementCategory category)
        {
            ArrayList removeList = new ArrayList();
            foreach (State state in m_StateList)
            {
                if (state.MissionID == iMissionID && state.Category == category)
                {
                    foreach (Translation translation in m_TranslationList)
                    {
                        if (translation.StartStateID == state.ID || translation.EndStateID == state.ID)
                        {
                            removeList.Add(translation);
                        }
                    }

                    removeList.Add(state);
                }
                else
                {
                    // 如果 state 与该任务无关,检查该s state 对应 option 是否与该任务相关
                    ArrayList optRemoveList = new ArrayList();
                    foreach (DialogOption option in state.OptionList)
                    {
                        if (option.MissionID==iMissionID && option.Category==category)
                        {
                            optRemoveList.Add(option);
                        }
                    }

                    foreach (DialogOption option in optRemoveList)
                    {
                        state.OptionList.Remove(option);
                    }
                }
            }

            foreach (Object obj in removeList)
            {
                if (obj is State)
                {
                    m_StateList.Remove(obj);
                }
                else
                {
                    m_TranslationList.Remove(obj);
                }

            }

            m_bDirty = true;
        }

        // 获取当前所有 State 中的最大 ID
        public int GetMaxStateID()
        {
            int iMaxID = -1;
            foreach (State state in m_StateList)
            {
                if (state.ID > iMaxID)
                {
                    iMaxID = state.ID;
                }
            }

            return iMaxID;
        }

        // 获取当前所有 Translation 中的最大 ID
        public int GetMaxTranslationID()
        {
            int iMaxID = -1;
            foreach (Translation trans in m_TranslationList)
            {
                if (trans.ID > iMaxID)
                {
                    iMaxID = trans.ID;
                }
            }

            return iMaxID;
        }

        // 根据名称获取 State
        public State GetStateByName(String strName)
        {
            foreach (State state in m_StateList)
            {
                if (state.Name.Equals(strName))
                {
                    return state;
                }
            }

            return null;
        }

        // 根据 ID 删除 State
        public bool RemoveStateByID(int iID)
        {
            State state = GetStateByID(iID);

            if (state == null)
            {
                return true;
            }

            String strMsg = "";
            if (state.AutoGenerated)
            {
                strMsg = "该 State 是自动创建的。确定要删除吗？";
            }
            else
            {
                strMsg = "确定要删除该 State 吗？";
            }
            DialogResult dr = MessageBox.Show(strMsg, "警告", MessageBoxButtons.YesNo);

            if (dr == DialogResult.No)
            {
                return false;
            }

            m_StateList.Remove(state);
            // 遍历所有 Translation, 删除连接关系
            foreach (Translation trans in m_TranslationList)
            {
                if (trans.StartStateID == iID)
                {
                    trans.StartStateID = -1;
                }

                if (trans.EndStateID == iID)
                {
                    trans.EndStateID = -1;
                }
            }
            m_bDirty = true;
            return true;

        }

        // 根据 ID 删除 Translation
        public bool RemoveTranslationByID(int iID)
        {
            Translation trans = GetTranslationByID(iID);
            m_bDirty = true;
            if (trans == null)
            {
                return true;
            }

            String strMsg = "";
            if (trans.AutoGenerated)
            {
                strMsg = "该 Translation 是自动创建的。确定要删除吗？";
            }
            else
            {
                strMsg = "确定要删除该 Translation 吗？";
            }
            DialogResult dr = MessageBox.Show(strMsg, "警告", MessageBoxButtons.YesNo);

            if (dr == DialogResult.No)
            {
                return false;
            }

            if (null != trans)
            {
                m_TranslationList.Remove(trans);
            }

            return true;
        }

        public State GetStateByID(int iID)
        {
            foreach (State state in m_StateList)
            {
                if (state.ID == iID)
                {
                    return state;
                }
            }

            return null;
        }

        public Translation GetTranslationByID(int iID)
        {
            foreach (Translation trans in m_TranslationList)
            {
                if (trans.ID == iID)
                {
                    return trans;
                }
            }

            return null;
        }

        // 判断该 state 是否有 translation 
        public bool ConnectedTranslation(State state)
        {
            foreach (Translation trans in m_TranslationList)
            {
                if (trans.StartStateID == state.ID)
                {
                    return true;
                }
            }

            return false;
        }

        public int ID
        {
            get
            {
                return m_iID;
            }
            set
            {
                m_iID = value;
            }
        }

        public String Name
        {
            get
            {
                return m_strName;
            }
            set 
            {
                m_strName = value;
            }
        }

        public ArrayList StateList
        {
            get
            {
                return m_StateList;
            }
        }

        public ArrayList TranslationList
        {
            get
            {
                return m_TranslationList;
            }
        }

        public bool Dirty
        {
            get
            {
                return m_bDirty;
            }
            set
            {
                m_bDirty = value;
            }
        }

        public int TeamWorkStatus
        {
            get
            {
                return m_iTeamWorkStatus;
            }
            set
            {
                m_iTeamWorkStatus = value;
            }
        }

        public String TeamWorkMark
        {
            get
            {
                if (m_iTeamWorkStatus == 0)
                {
                    return "＋";
                }
                if (m_iTeamWorkStatus == 1)
                {
                    return "  ";
                }
                if (m_iTeamWorkStatus == 2)
                {
                    return "√";
                }
                if (m_iTeamWorkStatus == 3)
                {
                    return "∞";
                }

                return "×";
            }
        }

        private int m_iID;                      // 状态图编号。NPC 首位为 1， Item 首位为 2.然后后边跟 NPC 编号或者 Item 编号。
        private String m_strName;               // 状态图名称。应该和被绑定的 npc 一致。
        private ArrayList m_StateList = new ArrayList();          // 状态列表
        private ArrayList m_TranslationList = new ArrayList();    // 变换列表
        private bool m_bDirty = true;           // 当前状态图是否和对应文件一致  
        
        private int m_iTeamWorkStatus = 1;  // 这个状态用于标记，任务处于签入，签出，新建状态
    }
}
