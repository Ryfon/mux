﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace MissionEditor
{
    // 对话选项
    public class DialogOption
    {
        public DialogOption(String strOption, int iValue, int iMissionID, StateDiagram.ElementCategory category)
        {
            m_strOption = strOption;
            m_iValue = iValue;
            m_iMissionID = iMissionID;
            m_Category = category;
        }

        public DialogOption Clone()
        {
            DialogOption dlgOption = new DialogOption(m_strOption, m_iValue, m_iMissionID, m_Category);
            foreach (MissionCondition condition in m_ConditionList)
            {
                dlgOption.ConditionList.Add(condition.Clone());
            }
            return dlgOption;
        }

        // 选项的字符串
        public String OptionString
        {
            get
            {
                return m_strOption;
            }
            set
            {
                m_strOption = value;
            }
        }

        // 选项对应值
        public int Value
        {
            get
            {
                return m_iValue;
            }
            set
            {
                m_iValue = value;
            }
        }

        // 选项与哪个任务相关
        public int MissionID
        {
            get
            {
                return m_iMissionID;
            }
            set
            {
                m_iMissionID = value;
            }
        }

        // 选项所属的类型
        public StateDiagram.ElementCategory Category
        {
            get
            {
                return m_Category;
            }
            set
            {
                m_Category = value;
            }
        }

        // 选项的显示条件
        public ArrayList ConditionList
        {
            get
            {
                return m_ConditionList;
            }
        }

        private String m_strOption;         // 该选项显示的文字
        private int m_iValue;               // 选项对应值
        private int m_iMissionID;           // 该选项与哪个任务相关
        private StateDiagram.ElementCategory m_Category;    // 选项所属的类型　GET, PROCESS, SUBMIT
        private ArrayList m_ConditionList = new ArrayList();  // 选项显示条件
    }
}
