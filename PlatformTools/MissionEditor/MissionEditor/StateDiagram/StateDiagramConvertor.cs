﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Xml;

namespace MissionEditor
{
    // 从用户输入的任务数据转换到 npc 状态图的转换器。整个任务编辑器的核心部分
    class StateDiagramConvertor
    {
        public void ConvertMission(Mission mission)
        {
            // 检查任务
            if (!CheckMissionCompleted(mission))
            {
                MessageBox.Show(mission.ID.ToString() + "转化状态图失败！");
                return;
            }

            if(SDLuaConvertScript.Instance.Used)
            {
                SDLuaConvertScript.Instance.Lock();
                if (!SDLuaConvertScript.Instance.Convert(mission))
                {
                    MessageBox.Show(mission.ID.ToString() + "转化错误！");
                }
                SDLuaConvertScript.Instance.Unlock();
                
            }
            else
            {
                if (mission.Type.Equals(OptionManager.MissionType.对话类.ToString()))
                {
                    if (!ConvertProviderTargetSubmitTypeMission(mission))
                    {

                    }
                }
                else if (mission.Type.Equals(OptionManager.MissionType.计数器类.ToString())
                || mission.Type.Equals(OptionManager.MissionType.护送类.ToString()))
                {
                    if (!ConvertProviderSubmitTypeMission(mission))
                    {

                    }
                }
            }
        }

        // 检查任务是否完整
        public bool CheckMissionCompleted(Mission mission)
        {
            try
            {
                // 任务公有属性
                if (mission.ID<0 || mission.Name.Length==0)
                {
                    MessageBox.Show("任务 ID, 名称 或者 描述 不完整。");
                    return false;
                }

                // 所有任务都必须有领取人和提交人
                if (mission.Provider<0 || mission.SubmitTo<0)
                {
                    MessageBox.Show("任务信息不完整，请检查 任务提供者，还任务对象。");
                    return false;
                }

                if (mission.Type == OptionManager.MissionType.对话类.ToString())
                {
                    // B对话类任务必须要有 targetNPC. targetItem 可选
                    if ( mission.TargetNPC<0 && mission.FlowType.Equals("B类"))
                    {
                        MessageBox.Show("目标 NPC 信息是否完整。");
                        return false;
                    }

                    if (mission.SubMissionType==OptionManager.MissionTypeDialogSubType.取物品.ToString() || 
                                mission.SubMissionType==OptionManager.MissionTypeDialogSubType.送物品.ToString())
                    {
                        if (mission.TargetItemBegin < 0)
                        {
                            MessageBox.Show("任务信息不完整，请检查 目标道具 信息是否完整。");
                            return false;
                        }
                    }
                }
                else if (mission.Type == OptionManager.MissionType.护送类.ToString())
                {
                    // 对话类任务必须要有 targetNPC
                    if (mission.TargetNPC < 0)
                    {
                        MessageBox.Show("目标 NPC 信息是否完整。");
                        return false;
                    }
                }
                else if (mission.Type == OptionManager.MissionType.计数器类.ToString())
                {
                    if(mission.SubMissionType == OptionManager.MissionTypeCounterSubType.收集物品.ToString())
                    {
                        if (mission.TargetItemBegin < 0 || mission.TargetItemEnd < 0)
                        {
                            MessageBox.Show("任务信息不完整，请检查 目标道具 信息是否完整。");
                            return false;
                        }
                    }
                    else if (mission.SubMissionType == OptionManager.MissionTypeCounterSubType.杀怪.ToString())
                    {
                        if (mission.TargetMonsterBegin < 0 || mission.TargetMonsterEnd < 0)
                        {
                            MessageBox.Show("任务信息不完整，请检查 目标怪物 信息是否完整。");
                            return false;
                        }
                    }
                }
                else if(mission.Type == OptionManager.MissionType.探索类.ToString())
                {
                    if(mission.TargetArea < 0)
                    {
                        MessageBox.Show("任务信息不完整，请检查 目标区域 信息是否完整。");
                        return false;
                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }

            return true;
        }

        // 添加一个状态到 目标 state diagram 中.同时为父节点和新节点添加 translation
        // Params: 
        // targetDia    - 目标 state diagram. 为哪一个状态图添加状态
        // parent       - 要添加 state 的 前继 state
        // stateName    - 新添加的 state 的名称
        // iMissionID   - 新添加的 state 属于哪个 任务
        // condition    - 从 parent 转换到新 state 的转换条件。为 null 说明无转换条件
        // iCurOption   - 转换条件对应的 选项值。 -1 说明不检查 CurOption
        public static State AddState(StateDiagram targetDia, State parent, String stateName, int iMissionID, StateDiagram.ElementCategory category, ArrayList conditions, int iCurOption)
        {
            State state = targetDia.GetStateByName(stateName);
            if (state != null)
            {
                return state;
            }

            int iID = targetDia.GetMaxStateID() + 1;
            state = new State(iID, stateName, GetLocationOnCanvas(iID), category);
            state.MissionID = iMissionID;
            state.AutoGenerated = true;
            targetDia.StateList.Add(state);

            Translation trans = new Translation(parent, state, ++MainForm.Instance.CurrentMaxTranslationID);
            if (conditions != null)
            {
                foreach (MissionCondition cond in conditions)
                {
                    trans.Conditions.Add(cond);
                }
            }
            trans.CurrentOption = iCurOption;
            trans.AutoGenerated = true;
            targetDia.TranslationList.Add(trans);

            return state;
        }

        // 初始化一个状态图，自动生成 idle 状态
        public static StateDiagram InitStateDiagram(String strName, int iDiaID)
        {
            StateDiagram dia = new StateDiagram();
            dia.Name = strName;
            dia.ID = iDiaID;
            State idleState = new State(0, "IDLE", new Point(200, 200), StateDiagram.ElementCategory.EC_OTHER);
            idleState.AutoGenerated = true;
            dia.StateList.Add(idleState);
            dia.Dirty = false;
            return dia;
        }

        public static Point GetLocationOnCanvas(int iID)
        {
            return new Point((iID % 8) * 120 + 10, (iID / 8) * 120 + 10);
        }

        // 为 状态图 添加默认对话
        public static void AddDefaultDialog(StateDiagram dia)
        {
            State stateDefDlg = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            if (stateDefDlg == null)
            {
                State idleState = dia.GetStateByName("IDLE");
                stateDefDlg = AddState(dia, idleState, OptionManager.DefaultDialogStateName, -1, StateDiagram.ElementCategory.EC_OTHER, null, -1);

                stateDefDlg.Dialog = "你好！";
                stateDefDlg.OptionList.Add(new DialogOption("[[离开]]再见", 1, -1, StateDiagram.ElementCategory.EC_OTHER));
                stateDefDlg.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_ShowDialog, " "));
            }
        }

        Translation GetTranslation(StateDiagram dia, State start, State end)
        {
            foreach (Translation trans in dia.TranslationList)
            {
                if (trans.StartStateID == start.ID
                    && trans.EndStateID == end.ID)
                {
                    return trans;
                }
            }
            return null;
        }

        // 添加获取任务对话状态 并返回该状态
        private State AddMissionGetDlgState(StateDiagram dia, Mission mission)
        {
            int iMissionID = mission.ID;

            // 获取 DEFAULT_DLG state
            State defDlgState = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            if (defDlgState == null)
            {
                // 
                MessageBox.Show("找不到 Default Dialog 结点.转换失败.");
                return null;
            }

            // 添加/更新 接受任务对话 State
            String strDlgStataName = "M" + iMissionID.ToString() + "_GET_DLG";
            State stateGetDlg = dia.GetStateByName(strDlgStataName);
            if (stateGetDlg != null)
            {
                // 更新
                // 找到从 defDlgState 到 stateGetDlg 的 translation
                Translation transDefToGetDlg = GetTranslation(dia, defDlgState, stateGetDlg);
                int iOption = -1;   // 在 
                if (transDefToGetDlg == null)
                {
                    // 之前的转换已经断开.
                }
                else
                {
                    iOption = transDefToGetDlg.CurrentOption;
                }

                foreach (DialogOption option in defDlgState.OptionList)
                {
                    if (option.Value == iOption)
                    {
                        // 找到 MissionGetDlg 在 DefDlg 中的对应 option
                        // 更新任务接受条件
                        option.ConditionList.Clear();
                        // 如果是不可重复任务,条件中加入未完成过该任务.
                        if (mission.Repeat)
                        {
                            option.ConditionList.Add(new MissionCondition("不在任务列表", mission.ID.ToString()));
                        }
                        else
                        {
                            option.ConditionList.Add(new MissionCondition("从未接受过任务", mission.ID.ToString()));
                        }
                        // 添加等级条件
                        option.ConditionList.Add(new MissionCondition("属性超过", "1, " + System.Math.Max((mission.Level - OptionManager.LevelDifference), 0).ToString()));

                        // 如果是送物品任务,添加包裹可容纳被送物品的条件
                        //if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.送物品.ToString()))
                        //{
                        //    option.ConditionList.Add(new MissionCondition("背包可容纳物品", mission.TargetItemBegin.ToString()+",1,0,0,0,0,0,0"));
                        //}

                        foreach (MissionCondition condition in mission.Conditions)
                        {
                            option.ConditionList.Add(condition);
                        }
                    }
                }
            }
            else
            {
                // 为 DEFAULT_DLG 的选项中添加 条件显示选项
                int iOptionID = defDlgState.MaxOptionValue + 1;
                DialogOption missionOption = new DialogOption("[[接任务]]关于任务 " + mission.Name, iOptionID, mission.ID, StateDiagram.ElementCategory.EC_GET);
                foreach (MissionCondition condition in mission.Conditions)
                {
                    missionOption.ConditionList.Add(condition);
                }

                missionOption.ConditionList.Add(new MissionCondition("属性超过", "1, " + System.Math.Max((mission.Level - OptionManager.LevelDifference), 0).ToString()));

                // 如果是送物品任务,添加包裹可容纳被送物品的条件
                //if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.送物品.ToString()))
                //{
                //    missionOption.ConditionList.Add(new MissionCondition("背包可容纳物品", mission.TargetItemBegin.ToString() + ",1,0,0,0,0,0,0"));
                //}

                if (mission.Repeat)
                {
                    // 可重复任务,不在列表就可以接
                    MissionCondition missCond = new MissionCondition("不在任务列表", mission.ID.ToString());
                    missionOption.ConditionList.Add(missCond);
                }
                else
                {
                    // 不可重复任务,从未接受过才可以接
                    MissionCondition missCond = new MissionCondition("从未接受过任务", mission.ID.ToString());
                    missionOption.ConditionList.Add(missCond);
                }

                defDlgState.OptionList.Add(missionOption);

                // 添加 接受任务对话 State
                stateGetDlg = AddState(dia, defDlgState, strDlgStataName, iMissionID, StateDiagram.ElementCategory.EC_GET, null, iOptionID);

                //stateGetDlg.Dialog = "接受任务 " + mission.Name + " 吗?";
                // 显示 标准接受任务 UI
                stateGetDlg.Dialog = "TaskAcceptUI,"+mission.ID.ToString();
                stateGetDlg.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_ShowDialog, " "));
                stateGetDlg.OptionList.Add(new DialogOption("接受", 1, mission.ID, StateDiagram.ElementCategory.EC_GET));
                stateGetDlg.OptionList.Add(new DialogOption("不接受", 2, mission.ID, StateDiagram.ElementCategory.EC_GET));

                // 为不接受选项 和 DEFAULT_DLG 添加转换
                Translation trans = new Translation(stateGetDlg, defDlgState, ++MainForm.Instance.CurrentMaxTranslationID);
                trans.AutoGenerated = true;
                trans.CurrentOption = 2;
                dia.TranslationList.Add(trans);
            }
            return stateGetDlg;
        }

        // 添加实际获取任务的状态 并返回该状态
        private State AddMissionGetState(StateDiagram dia, State stateGetDlg, Mission mission)
        {
            int iMissionID = mission.ID;

            // 获取 DEFAULT_DLG state
            //State defDlgState = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            // 实际接受任务的状态
            String strGetStateName = "M" + iMissionID.ToString() + "_GET";
            State stateGet = dia.GetStateByName(strGetStateName);
            if (stateGet != null)
            {
                // 更新
                // 清空原 OnEnter List
                stateGet.ActionListOnEnter.Clear();
                stateGet.ActionListOnOption.Clear();
                stateGet.ActionListOnExit.Clear();
               
            }
            else
            {
                // 添加 接受任务实现 State
                int iID = dia.GetMaxStateID() + 1;
                stateGet = AddState(dia, stateGetDlg, strGetStateName, iMissionID, StateDiagram.ElementCategory.EC_GET, null, 1);
            }

            stateGet.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_GiveTask, mission.ID.ToString()));

            if (mission.Type.Equals(OptionManager.MissionType.对话类.ToString()))
            {
                if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.送物品.ToString()))
                {
                    // 如果是送物品的任务，把物品交给玩家
                    stateGet.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_GiveItem, mission.TargetItemBegin.ToString()+",1"));
                }
            }
            else if (mission.Type.Equals(OptionManager.MissionType.计数器类.ToString()))
            {
                // 如果是计数器任务，为玩家添加相应计数器。
                if (mission.SubMissionType.Equals(OptionManager.MissionTypeCounterSubType.收集物品.ToString()))
                {
                    // MissionID, CounterType, DataBegin, DataEnd, num
                    String strValue = ((int)OptionManager.CounterType.CT_COLLECT).ToString() +
                        "," + mission.TargetItemBegin.ToString() +
                        "," + mission.TargetItemEnd.ToString() +
                        "," + mission.TargetValue.ToString() +
                        "," + mission.ID.ToString();
                        
                    stateGet.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_AddCounter, strValue));
                }
                else if (mission.SubMissionType.Equals(OptionManager.MissionTypeCounterSubType.杀怪.ToString()))
                {
                    // CounterType, dataBegin, dataEnd, num
                    String strValue = ((int)OptionManager.CounterType.CT_KILL_MONSTER).ToString() +
                        "," + mission.TargetMonsterBegin.ToString() +
                        "," + mission.TargetMonsterEnd.ToString() +
                        "," + mission.TargetValue.ToString() +
                        "," + mission.ID.ToString();

                    stateGet.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_AddCounter, strValue));
                }
            }
            else if (mission.Type.Equals(OptionManager.MissionType.护送类.ToString()))
            {
                // 如果是护送任务.生成目标 NPC
                String strValue = mission.TargetNPC.ToString();
                stateGet.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_NpcCreatePlayerNpc, strValue));
            }

 
            return stateGet;
        }

        // 添加获取任务后的提示对话状态
        private State AddMissionGetHintDlgState(StateDiagram dia, Mission mission)
        {
            int iMissionID = mission.ID;

            // 获取 DEFAULT_DLG state
            State defDlgState = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            // 添加/更新 催促提示任务对话 State
            String strDlgStataHINTName = "M" + iMissionID.ToString() + "_GET_HINT_DLG";
            State stateHintDlg = dia.GetStateByName(strDlgStataHINTName);
            if (stateHintDlg != null)
            {
                // 更新
            }
            else
            {
                // 为 DEFAULT_DLG 的选项中添加 条件显示选项
                int iOptionID = defDlgState.MaxOptionValue + 1;
                DialogOption missionOption = new DialogOption("[[任务提示]]关于任务 " + mission.Name, iOptionID, mission.ID, StateDiagram.ElementCategory.EC_GET);
                MissionCondition missCond = new MissionCondition("在任务列表未达成目标", mission.ID.ToString());
                missionOption.ConditionList.Add(missCond);
                defDlgState.OptionList.Add(missionOption);

                // 添加 接受任务对话 State
                stateHintDlg = AddState(dia, defDlgState, strDlgStataHINTName, iMissionID, StateDiagram.ElementCategory.EC_GET, null, iOptionID);

                stateHintDlg.Dialog = "快去做 " + mission.Name + " 任务吧?";
                stateHintDlg.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_ShowDialog, " "));
                stateHintDlg.OptionList.Add(new DialogOption("好的", 1, mission.ID, StateDiagram.ElementCategory.EC_GET));
            }

            return stateHintDlg;
        }

        // 为 NPC 添加/更新 获取任务 的状态
        private void AddMissionGetStatesToNPC(StateDiagram dia, Mission mission)
        {
            State defDlgState = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            if (defDlgState == null)
            {
                MessageBox.Show("在状态图 " + dia.Name + "中没有找到 DEFAULT_DLG 状态.添加状态图失败.");
                return;
            }
                
            State stateGetDlg = AddMissionGetDlgState(dia, mission);    // 显示固定接受任务 UI 
            State stateGet = AddMissionGetState(dia, stateGetDlg, mission);
            AddMissionGetHintDlgState(dia, mission);
        }

        // 为 Item 添加/更新 获取任务 的状态
        private void AddMissionGetStatesToItem(StateDiagram dia, Mission mission)
        {
            State defDlgState = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            if (defDlgState == null)
            {
                MessageBox.Show("在状态图 " + dia.Name + "中没有找到 DEFAULT_DLG 状态.添加状态图失败.");
                return;
            }

            State stateGetDlg = AddMissionGetDlgState(dia, mission);    // 显示固定接受任务 UI 
            State stateGet = AddMissionGetState(dia, stateGetDlg, mission);

        }

        // 添加 任务进行 的状态
        private void AddMissionProcessState(StateDiagram dia, Mission mission)
        {
            int iMissionID = mission.ID;

            // 获取 DEFAULT_DLG state
            State defDlgState = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            if (defDlgState == null)
            {
                MessageBox.Show("在状态图 " + dia.Name + "中没有找到 DEFAULT_DLG 状态.添加状态图失败.");
                return;
            }

            // 添加/更新 接受任务对话 State
            String strDlgStataName = "M" + iMissionID.ToString() + "_PROCESS_DLG";
            State stateProcessDlg = dia.GetStateByName(strDlgStataName);

            bool bNewState = (stateProcessDlg == null);
            int iOptionID = 0;

            if (!bNewState)
            {
                // 更新
                stateProcessDlg.ActionListOnEnter.Clear();
                stateProcessDlg.ActionListOnOption.Clear();
                stateProcessDlg.ActionListOnExit.Clear();
            }
            else
            {
                iOptionID = defDlgState.MaxOptionValue + 1;
                String strSubmitTo = NPCManager.Instance.GetNPCInfoByID(mission.SubmitTo).Name;

                // 添加 接受任务对话 State
                stateProcessDlg = AddState(dia, defDlgState, strDlgStataName, iMissionID, StateDiagram.ElementCategory.EC_PROCESS, null, iOptionID);
                stateProcessDlg.Dialog = "任务 " + mission.Name +" 的目标就是和我对话.现在你完成了,去找 " + strSubmitTo + " 还任务吧";
            }

            stateProcessDlg.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_ShowDialog, " "));
            stateProcessDlg.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_ProcessTask, iMissionID.ToString()));

            if (mission.Type.Equals(OptionManager.MissionType.对话类.ToString()))
            {
                if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.送物品.ToString()))
                {
                    // 如果是送物品的任务，把物品从玩家背包中移除
                    stateProcessDlg.ActionListOnEnter.Add(
                        new AtomActionInstance(AtomActionManager.Display_RemoveItem, mission.TargetItemBegin.ToString()+",1"));
                }
                else if(mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.取物品.ToString()))
                {
                    // 如果是取物品的任务，把物品交给玩家
                    stateProcessDlg.ActionListOnEnter.Add(
                        new AtomActionInstance(AtomActionManager.Display_GiveItem, mission.TargetItemBegin.ToString()+",1"));
                }
            }
            else if (mission.Type.Equals(OptionManager.MissionType.计数器类.ToString()))
            {
                // 计数器类没有 process 状态
            }
            else if (mission.Type.Equals(OptionManager.MissionType.护送类.ToString()))
            {
                // 护送类没有 process 状态
            }

            if (bNewState)
            {
                stateProcessDlg.OptionList.Add(new DialogOption("好的", 1, mission.ID, StateDiagram.ElementCategory.EC_PROCESS));
                // 在 DEFAULT_DLG 中添加相应选项
                DialogOption option = new DialogOption("[[执行任务]]关于任务 " + mission.Name + "你知道些什么?", iOptionID, mission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                option.ConditionList.Add(new MissionCondition("在任务列表未达成目标", mission.ID.ToString()));
                defDlgState.OptionList.Add(option);
            }

            // 添加 Process_Hint_Dlg
            String strProcessHintDlgName = "M" + iMissionID.ToString() + "_PROCESS_HINT_DLG";
            State stateProcessHintDlg = dia.GetStateByName(strProcessHintDlgName);
            if (stateProcessHintDlg != null)
            {
                // 更新
            }
            else
            {
                iOptionID = defDlgState.MaxOptionValue + 1;
                String strSubmitTo = NPCManager.Instance.GetNPCInfoByID(mission.SubmitTo).Name;
                stateProcessHintDlg = AddState(dia, defDlgState, strProcessHintDlgName, iMissionID, StateDiagram.ElementCategory.EC_PROCESS, null, iOptionID);
                stateProcessHintDlg.Dialog = "任务 " + mission.Name + " 的目标已经达到了.去找 " + strSubmitTo + " 还任务领奖品吧";
                stateProcessHintDlg.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_ShowDialog, " "));
                stateProcessHintDlg.OptionList.Add(new DialogOption("好的", 1, mission.ID, StateDiagram.ElementCategory.EC_PROCESS));

                // 在 DEFAULT_DLG 中添加相应选项
                DialogOption option = new DialogOption("[[任务提示]]关于任务 " + mission.Name + "你知道些什么?", iOptionID, mission.ID, StateDiagram.ElementCategory.EC_PROCESS);
                option.ConditionList.Add(new MissionCondition("在任务列表达成目标", mission.ID.ToString()));
                defDlgState.OptionList.Add(option);
            }

        }

        // 添加 还任务 的状态
        private void AddMissionSubmitState(StateDiagram dia, Mission mission)
        {
            int iMissionID = mission.ID;

            // 获取 DEFAULT_DLG state
            State defDlgState = dia.GetStateByName(OptionManager.DefaultDialogStateName);
            if (defDlgState == null)
            {
                MessageBox.Show("在状态图 " + dia.Name + "中没有找到 DEFAULT_DLG 状态.添加状态图失败.");
                return;
            }

            // 添加/更新 接受任务对话 State
            String strDlgSubmitName = "M" + iMissionID.ToString() + "_SUBMIT_DLG";
            State stateSubmitDlg = dia.GetStateByName(strDlgSubmitName);
            bool bNewState = (stateSubmitDlg == null);
            int iOptionID = -1;
            if (!bNewState)
            {
                // 更新奖励列表
                stateSubmitDlg.ActionListOnOption.Clear();
                stateSubmitDlg.ActionListOnEnter.Clear();
                stateSubmitDlg.ActionListOnExit.Clear();
                //// 添加完成任务
                //stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(AtomActionManager.Display_FinishTask, mission.ID.ToString()));

                //foreach (MissionAward award in mission.FixedAwards)
                //{
                //    stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(award.Award, award.Value));
                //}
                //if (mission.Type.Equals(OptionManager.MissionType.对话类.ToString()))
                //{
                //    if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.送物品.ToString()))
                //    {
                //    }
                //    else if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.取物品.ToString()))
                //    {
                //        // 如果是取物品的任务，交任务领取奖品时没收该物品
                //        stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(AtomActionManager.Display_RemoveItem, mission.TargetItemBegin.ToString()+",1"));
                //    }
                //}
                //else if (mission.Type.Equals(OptionManager.MissionType.计数器类.ToString()))
                //{
                //    // 收集物品任务,交任务时没收收集的物品
                //    if (mission.SubMissionType.Equals(OptionManager.MissionTypeCounterSubType.收集物品.ToString()))
                //    {
                //        stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(
                //            AtomActionManager.Display_RemoveItem, mission.TargetItemBegin.ToString()+"," + mission.TargetValue.ToString()));
                //    }
                //}
                //else if (mission.Type.Equals(OptionManager.MissionType.护送.ToString()))
                //{
                //    // 护送任务,交任务取消跟随
                //    stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(AtomActionManager.Display_NpcDelPlayerNpc, mission.TargetNPC.ToString()));
                //}

            }
            else
            {
                iOptionID = defDlgState.MaxOptionValue + 1;

                // 添加 提交任务对话 State

                stateSubmitDlg = AddState(dia, defDlgState, strDlgSubmitName, iMissionID, StateDiagram.ElementCategory.EC_SUBMIT, null, iOptionID);
                // 显示固定接受任务奖励 UI
                stateSubmitDlg.Dialog = "TaskRewardUI," + mission.ID.ToString();
            }

            // 在 OnEnter 中添加显示对话
            stateSubmitDlg.ActionListOnEnter.Add(new AtomActionInstance(AtomActionManager.Display_ShowDialog, " "));
            
            // 添加完成任务
            stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(AtomActionManager.Display_FinishTask, mission.ID.ToString()));

            // 固定奖励
            foreach (MissionAward award in mission.FixedAwards)
            {
                stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(award.Award, award.Value));
            }
            if (mission.Type.Equals(OptionManager.MissionType.对话类.ToString()))
            {
                if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.送物品.ToString()))
                {
                }
                else if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.取物品.ToString()))
                {
                    // 如果是取物品的任务，交任务领取奖品时没收该物品
                    stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(AtomActionManager.Display_RemoveItem, mission.TargetItemBegin.ToString()+",1"));
                }
            }
            else if (mission.Type.Equals(OptionManager.MissionType.计数器类.ToString()))
            {
                if (mission.SubMissionType.Equals(OptionManager.MissionTypeCounterSubType.收集物品.ToString()))
                {
                    stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(
                        AtomActionManager.Display_RemoveItem, mission.TargetItemBegin.ToString()+"," + mission.TargetValue.ToString()));
                }
            }
            else if (mission.Type.Equals(OptionManager.MissionType.护送类.ToString()))
            {
                // 护送任务,交任务取消跟随
                stateSubmitDlg.ActionListOnOption.Add(new AtomActionInstance(AtomActionManager.Display_NpcDelPlayerNpc, mission.TargetNPC.ToString()));
            }

            if (bNewState)
            {
                // 在 DEFAULT_DLG 中添加相应选项
                DialogOption option =
                    new DialogOption("[[完成任务]]任务 " + mission.Name + " 终于完成了.我是来领取奖品的.", iOptionID, mission.ID, StateDiagram.ElementCategory.EC_SUBMIT);
                if (mission.Type.Equals(OptionManager.MissionType.对话类.ToString())
                    || mission.Type.Equals(OptionManager.MissionType.计数器类.ToString()))
                {
                    option.ConditionList.Add(new MissionCondition("在任务列表达成目标", mission.ID.ToString()));
                }
                else if (mission.Type.Equals(OptionManager.MissionType.护送类.ToString()))
                {
                    option.ConditionList.Add(new MissionCondition("在任务列表", mission.ID.ToString()));
                }


                if (mission.Type.Equals(OptionManager.MissionType.对话类.ToString()))
                {
                    if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.送物品.ToString()))
                    {
                    }
                    else if (mission.SubMissionType.Equals(OptionManager.MissionTypeDialogSubType.取物品.ToString()))
                    {
                        // 如果是取物品的任务，交任务领取奖品时没收该物品
                        option.ConditionList.Add(new MissionCondition("背包有", mission.TargetItemBegin.ToString() + ", 1"));
                    }
                }
                else if (mission.Type.Equals(OptionManager.MissionType.计数器类.ToString()))
                {
                    // 计数器类的任务 在计数器达到计数值的时候服务器端会将对应的任务状态设置为 达成目标但未交还。
                    // 所以这里不用做计数器的检查
                }
                else if (mission.Type.Equals(OptionManager.MissionType.护送类.ToString()))
                {
                    // 护送类任务 还任务的条件是有 目标 NPC 跟随
                    option.ConditionList.Add(new MissionCondition("NpcCheckPlayerNpc", mission.TargetNPC.ToString()));
                }

                defDlgState.OptionList.Add(option);
            }


 
        }

        private void SaveStateDiagram(StateDiagram dia)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement xmlElem = dia.GetAsXmlElement(xmlDoc);
            xmlDoc.AppendChild(xmlElem);
            xmlDoc.Save(OptionManager.StateDiagramPath + dia.Name + ".xml");
        }

        // 转换Provider Target Submit类任务
        private bool ConvertProviderTargetSubmitTypeMission(Mission mission)
        {
            // 注意：
            // 如果 npc 对应的状态图已存在，做升级。
            // 否则创建新的。

            //bool bNewProviderDia = false;
            //bool bNewSubmitToDia = false;
            //bool bNewTargetNPCDia = false;

            
            // 根据是否是 item provider 确定 provider dia 的 id 和 name
            StateDiagram.OwnerType ownerType;
            String strNamePrefix = "";

            //  绑定在 npc 上的脚本 ID 第一位为 1, 绑在 item 上的第一位为 2. 区域上的首位为 3
            if (!mission.ItemProvider)
            {
                //strIDPrefix = "1";
                ownerType = StateDiagram.OwnerType.DT_NPC;
                strNamePrefix = OptionManager.NPCPrefix;
            }
            else
            {
                //strIDPrefix = "2";
                ownerType = StateDiagram.OwnerType.DT_ITEM;
                strNamePrefix = OptionManager.ItemPrefix;
            }

            // 先将 provider, submitTo, targetNPC 的 StateDiagram 取得/新建

            String strProviderDiaName = strNamePrefix + StateDiagram.GenerateStateDiagramID(ownerType, mission.Provider).ToString();
            StateDiagram providerDia = StateDiagramManager.Instance.GetStateDiagram(strProviderDiaName);

            if (providerDia == null)
            {
                //bNewProviderDia = true;
                int iID = StateDiagram.GenerateStateDiagramID(ownerType, mission.Provider);
                providerDia = InitStateDiagram(strProviderDiaName, iID);
                StateDiagramManager.Instance.SetDiagram(strProviderDiaName, providerDia);
            }

            String strSubmitToDiaName = OptionManager.NPCPrefix +
                DialogManager.GetDialogIDPrefix(StateDiagram.OwnerType.DT_NPC, mission.SubmitTo).ToString();
            StateDiagram submitToDia = StateDiagramManager.Instance.GetStateDiagram(strSubmitToDiaName);
            if (submitToDia == null)
            {
                //bNewSubmitToDia = true;
                int iID = StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, mission.SubmitTo);
                submitToDia = InitStateDiagram(strSubmitToDiaName, iID);
                StateDiagramManager.Instance.SetDiagram(strSubmitToDiaName, submitToDia);
            }

            String strTargetNPCToDiaName = OptionManager.NPCPrefix +
                DialogManager.GetDialogIDPrefix(StateDiagram.OwnerType.DT_NPC, mission.TargetNPC).ToString();
            StateDiagram targetNPCDia = StateDiagramManager.Instance.GetStateDiagram(strTargetNPCToDiaName);
            if (targetNPCDia == null)
            {
                //bNewTargetNPCDia = true;
                int iID = DialogManager.GetDialogIDPrefix(StateDiagram.OwnerType.DT_NPC, mission.TargetNPC);
                targetNPCDia = InitStateDiagram(strTargetNPCToDiaName, iID);
                StateDiagramManager.Instance.SetDiagram(strTargetNPCToDiaName, targetNPCDia);
            }

            // 删除原状态图中 与 mission 相关的状态. 
            //providerDia.RemoveStateByMissionID(mission.ID);
            //submitToDia.RemoveStateByMissionID(mission.ID);
            //targetNPCDia.RemoveStateByMissionID(mission.ID);

            SetCurrentStateDiagram(providerDia);
            //if (!mission.ItemProvider)
            //{
                // 从 NPC 获取的任务
            AddDefaultDialog(providerDia);
            //}

            // 为 provider 添加接受任务 State
            //AddMissionGetState(providerDia, mission);

            if (!mission.ItemProvider)
            {
                AddMissionGetStatesToNPC(providerDia, mission);
            }
            else
            {
                AddMissionGetStatesToItem(providerDia, mission);
            }

            SetCurrentStateDiagram(submitToDia);
            AddDefaultDialog(submitToDia);
            // 为 submitTo 添加进行任务 State
            AddMissionSubmitState(submitToDia, mission);

            SetCurrentStateDiagram(targetNPCDia);
            AddDefaultDialog(targetNPCDia);
            // 为 tergetNPC 添加还任务 State
            AddMissionProcessState(targetNPCDia, mission);

            // 保存所有的 StateDiagram
            SaveStateDiagram(providerDia);
            SaveStateDiagram(submitToDia);
            SaveStateDiagram(targetNPCDia);
            SetCurrentStateDiagram(null);

            MessageBox.Show("任务:"+mission.ID.ToString()+" 转换到状态图完成。" );

            return true;
        }

        // 转换 Provider Submit 类任务
        private bool ConvertProviderSubmitTypeMission(Mission mission)
        {
            // 注意：
            // 如果 npc 对应的状态图已存在，做升级。
            // 否则创建新的。

            //bool bNewProviderDia = false;
            //bool bNewSubmitToDia = false;

            // 根据是否是 item provider 确定 provider dia 的 id 和 name
            StateDiagram.OwnerType ownerType;
            String strNamePrefix = "";

            //  绑定在 npc 上的脚本 ID 第一位为 1, 绑在 item 上的第一位为 2. 区域上的首位为 3
            if (!mission.ItemProvider)
            {
                //strIDPrefix = "1";
                ownerType = StateDiagram.OwnerType.DT_NPC;
                strNamePrefix = OptionManager.NPCPrefix;
            }
            else
            {
                //strIDPrefix = "2";
                ownerType = StateDiagram.OwnerType.DT_ITEM;
                strNamePrefix = OptionManager.ItemPrefix;
            }


            String strProviderDiaName = strNamePrefix + StateDiagram.GenerateStateDiagramID(ownerType, mission.Provider).ToString();
            StateDiagram providerDia = StateDiagramManager.Instance.GetStateDiagram(strProviderDiaName);
            if (providerDia == null)
            {
                //bNewProviderDia = true;
                int iID = StateDiagram.GenerateStateDiagramID(ownerType, mission.Provider);
                providerDia = InitStateDiagram(strProviderDiaName, iID);
                StateDiagramManager.Instance.SetDiagram(strProviderDiaName, providerDia);
            }

            String strSubmitToDiaName = OptionManager.NPCPrefix +
                StateDiagram.GenerateStateDiagramID(StateDiagram.OwnerType.DT_NPC, mission.SubmitTo).ToString();
            StateDiagram submitToDia = StateDiagramManager.Instance.GetStateDiagram(strSubmitToDiaName);
            if (submitToDia == null)
            {
                //bNewSubmitToDia = true;
                int iID = DialogManager.GetDialogIDPrefix(StateDiagram.OwnerType.DT_NPC, mission.SubmitTo);
                submitToDia = InitStateDiagram(strSubmitToDiaName, iID);
                StateDiagramManager.Instance.SetDiagram(strSubmitToDiaName, submitToDia);
            }

            // 删除原状态图中 与 mission 相关的状态
            //providerDia.RemoveStateByMissionID(mission.ID);
            //submitToDia.RemoveStateByMissionID(mission.ID);

            SetCurrentStateDiagram(providerDia);
            //if (!mission.ItemProvider)
            //{
            AddDefaultDialog(providerDia);
            //}
            // 为 provider 添加接受任务 State
            if (!mission.ItemProvider)
            {
                AddMissionGetStatesToNPC(providerDia, mission);
            }
            else
            {
                AddMissionGetStatesToItem(providerDia, mission);
            }

            SetCurrentStateDiagram(submitToDia);
            AddDefaultDialog(submitToDia);
            // 为 submitNPC 添加进行任务 State
            AddMissionSubmitState(submitToDia, mission);


            // 保存所有的 StateDiagram
            SaveStateDiagram(providerDia);
            SaveStateDiagram(submitToDia);
            SetCurrentStateDiagram(null);

            MessageBox.Show("任务:" + mission.ID.ToString() + " 转换到状态图完成。");

            return true;
        }


        // 将状态图设置为正被编辑状态
        private void SetCurrentStateDiagram(StateDiagram dia)
        {
            if (dia != null)
            {
                MainForm.Instance.CurrentMaxStateID = dia.GetMaxStateID();
                MainForm.Instance.CurrentMaxTranslationID = dia.GetMaxTranslationID();
                MainForm.Instance.OnEditStateDiagramName = dia.Name;
            }
            else
            {
                MainForm.Instance.CurrentMaxStateID = 0;
                MainForm.Instance.CurrentMaxTranslationID = 0;
                MainForm.Instance.OnEditStateDiagramName = null;
            }


        }
    }
}

