﻿using System;
using System.Collections;
using System.Drawing;

namespace MissionEditor
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        // 当前 状态图 名称
        public String StateDiagramName
        {
            get
            {
                return m_strStateDiagramName;
            }
        }

        public bool StateDiagramChanged
        {
            get
            {
                if (OnEditStateDiagram != null)
                {
                    return OnEditStateDiagram.Dirty;
                }
                else
                {
                    return false;
                }

            }
            set
            {
                if (OnEditStateDiagram != null)
                {
                    OnEditStateDiagram.Dirty = value;
                }
            }
        }

        public MissionManager MissionMgr
        {
            get
            {
                return m_MissionManager;
            }
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.m_MainMenu = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存任务ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重新加载点音源信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.客户端文件导出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.行为ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.原子操作ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.选项ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.获取最新任务数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.获取最新状态图数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStripMenuItemMission = new System.Windows.Forms.ToolStripMenuItem();
            this.转换到状态图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.批量转换到状态图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripMenuItemMissionConditionLua = new System.Windows.Forms.ToolStripMenuItem();
            this.导出任务存盘栏位规则ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStripMenuItemStateDia = new System.Windows.Forms.ToolStripMenuItem();
            this.转换到脚本ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.批量转换到脚本ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sceneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.区域管理工具ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.添加NPCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.添加区域ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.生成器ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.任务脚本测试ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出当前地图的路径表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出地图格子信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.路径编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.启用加速模式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStripComboBoxSceneScale = new System.Windows.Forms.ToolStripComboBox();
            this.工具ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.商店编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.数据编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.任务奖励编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.任务条件编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NPC统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.报告ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出任务报告ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_tabControl = new System.Windows.Forms.TabControl();
            this.m_tabPageTaskUI = new System.Windows.Forms.TabPage();
            this.m_tabPageState = new System.Windows.Forms.TabPage();
            this.m_splitContainerState = new System.Windows.Forms.SplitContainer();
            this.m_listBoxMO = new System.Windows.Forms.ListBox();
            this.m_MOFilter = new System.Windows.Forms.ComboBox();
            this.m_menuStateDiagram = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.版本管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.获取最新版本ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.签出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.签入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.导入新状态图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.撤销更改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.添加状态ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.添加连接ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.新建状态图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存状态图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新状态图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.m_toolStripMenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStripMenuItemPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.打开所在文件夹ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_toolStripMenuItemConvertDiaToLua = new System.Windows.Forms.ToolStripMenuItem();
            this.m_tabPageSkeneScetch = new System.Windows.Forms.TabPage();
            this.m_splitContainerSceneSketch = new System.Windows.Forms.SplitContainer();
            this.m_splitContainerSceneScketchLeft = new System.Windows.Forms.SplitContainer();
            this.m_listBoxSceneID = new System.Windows.Forms.ListBox();
            this.m_propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.m_statusStripScene = new System.Windows.Forms.StatusStrip();
            this.m_toolStripStatusLabelTitle = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_toolStripStatusLabelNpcShow = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_toolStripStatusLabelNpcNum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_toolStripStatusLabelSubNpcShow1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_toolStripStatusLabelSubNpcShow2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_toolStripStatusLabelSubNpcShow3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_toolStripSplitButtonNpcHighLight = new System.Windows.Forms.ToolStripSplitButton();
            this.m_pictureBoxSceneSketch = new System.Windows.Forms.PictureBox();
            this.m_tabPageMission = new System.Windows.Forms.TabPage();
            this.m_splitContainerMission = new System.Windows.Forms.SplitContainer();
            this.m_treeViewMission = new System.Windows.Forms.TreeView();
            this.m_btnEditSimpleMissionDec = new System.Windows.Forms.Button();
            this.m_webBrowserSimpleMissionDesc = new System.Windows.Forms.WebBrowser();
            this.m_btnClearTargetArea = new System.Windows.Forms.Button();
            this.m_comboBoxTargetArea = new System.Windows.Forms.ComboBox();
            this.m_checkBoxRegionProvider = new System.Windows.Forms.CheckBox();
            this.m_comboBoxFlowType = new System.Windows.Forms.ComboBox();
            this.m_comboBoxReservedFilter = new System.Windows.Forms.ComboBox();
            this.m_textBoxTimeLimit = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.m_comboBoxStarLevel = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.m_comboBoxLeadMap = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.m_btnExternalLoad = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.m_btnClearTargetValue = new System.Windows.Forms.Button();
            this.m_webBrowserSubmitMissionDesc = new System.Windows.Forms.WebBrowser();
            this.m_btnEditSubmitMissionDec = new System.Windows.Forms.Button();
            this.m_textBoxTrackName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.m_textBoxReserved = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_btnClearTargetMonster = new System.Windows.Forms.Button();
            this.m_btnClearTargetItem = new System.Windows.Forms.Button();
            this.m_btnClearTargetProp = new System.Windows.Forms.Button();
            this.m_btnClearTargetNpc = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.m_textBoxLevel = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.m_comboBoxClass = new System.Windows.Forms.ComboBox();
            this.m_webBrowserGetMissionDesc = new System.Windows.Forms.WebBrowser();
            this.m_btnEditGetMissionDec = new System.Windows.Forms.Button();
            this.m_checkBoxItemProvider = new System.Windows.Forms.CheckBox();
            this.m_comboBoxTargetMonsterEnd = new System.Windows.Forms.ComboBox();
            this.m_comboBoxTargetItemEnd = new System.Windows.Forms.ComboBox();
            this.m_comboBoxTargetNPCFilter = new System.Windows.Forms.ComboBox();
            this.m_comboBoxSubmitToFilter = new System.Windows.Forms.ComboBox();
            this.m_listBoxSelectedAwardValue = new System.Windows.Forms.ListBox();
            this.m_btnSelectedAwardEdit = new System.Windows.Forms.Button();
            this.m_listBoxSelectedAward = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_listBoxFixedAwardValue = new System.Windows.Forms.ListBox();
            this.m_listBoxConditionValue = new System.Windows.Forms.ListBox();
            this.m_comboBoxTargetMonsterBegin = new System.Windows.Forms.ComboBox();
            this.m_labelTargetMonster = new System.Windows.Forms.Label();
            this.m_comboBoxProviderFilter = new System.Windows.Forms.ComboBox();
            this.m_checkBoxGiveUp = new System.Windows.Forms.CheckBox();
            this.m_comboBoxTargetProp = new System.Windows.Forms.ComboBox();
            this.m_labelTargetProp = new System.Windows.Forms.Label();
            this.m_btnNew = new System.Windows.Forms.Button();
            this.m_checkBoxShare = new System.Windows.Forms.CheckBox();
            this.m_checkBoxRepeat = new System.Windows.Forms.CheckBox();
            this.m_labelTargetArea = new System.Windows.Forms.Label();
            this.m_comboBoxSubType = new System.Windows.Forms.ComboBox();
            this.m_comboBoxTargetItemBegin = new System.Windows.Forms.ComboBox();
            this.m_labelTargetItem = new System.Windows.Forms.Label();
            this.m_textBoxTargetValue = new System.Windows.Forms.TextBox();
            this.m_btnSave = new System.Windows.Forms.Button();
            this.m_labelTargetValue = new System.Windows.Forms.Label();
            this.m_comboBoxTargetNPC = new System.Windows.Forms.ComboBox();
            this.m_labelTargetNPC = new System.Windows.Forms.Label();
            this.m_btnFixedAwardEdit = new System.Windows.Forms.Button();
            this.m_btnConvertToStateDiagrom = new System.Windows.Forms.Button();
            this.m_btnConditionEdit = new System.Windows.Forms.Button();
            this.m_listBoxFixedAward = new System.Windows.Forms.ListBox();
            this.m_listBoxCondition = new System.Windows.Forms.ListBox();
            this.m_comboBoxSubmitTo = new System.Windows.Forms.ComboBox();
            this.m_comboBoxProvider = new System.Windows.Forms.ComboBox();
            this.m_comboBoxType = new System.Windows.Forms.ComboBox();
            this.m_textBoxName = new System.Windows.Forms.TextBox();
            this.m_textBoxID = new System.Windows.Forms.TextBox();
            this.m_labelAward = new System.Windows.Forms.Label();
            this.m_labelSubmitTo = new System.Windows.Forms.Label();
            this.m_labelProvider = new System.Windows.Forms.Label();
            this.m_labelCondition = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_timerSceneRefresh = new System.Windows.Forms.Timer(this.components);
            this.m_MainMenu.SuspendLayout();
            this.m_tabControl.SuspendLayout();
            this.m_tabPageState.SuspendLayout();
            this.m_splitContainerState.Panel1.SuspendLayout();
            this.m_splitContainerState.SuspendLayout();
            this.m_menuStateDiagram.SuspendLayout();
            this.m_tabPageSkeneScetch.SuspendLayout();
            this.m_splitContainerSceneSketch.Panel1.SuspendLayout();
            this.m_splitContainerSceneSketch.Panel2.SuspendLayout();
            this.m_splitContainerSceneSketch.SuspendLayout();
            this.m_splitContainerSceneScketchLeft.Panel1.SuspendLayout();
            this.m_splitContainerSceneScketchLeft.Panel2.SuspendLayout();
            this.m_splitContainerSceneScketchLeft.SuspendLayout();
            this.m_statusStripScene.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxSceneSketch)).BeginInit();
            this.m_tabPageMission.SuspendLayout();
            this.m_splitContainerMission.Panel1.SuspendLayout();
            this.m_splitContainerMission.Panel2.SuspendLayout();
            this.m_splitContainerMission.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_MainMenu
            // 
            this.m_MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.编辑ToolStripMenuItem,
            this.m_toolStripMenuItemMission,
            this.m_toolStripMenuItemStateDia,
            this.sceneToolStripMenuItem,
            this.m_toolStripComboBoxSceneScale,
            this.工具ToolStripMenuItem,
            this.报告ToolStripMenuItem});
            this.m_MainMenu.Location = new System.Drawing.Point(0, 0);
            this.m_MainMenu.Name = "m_MainMenu";
            this.m_MainMenu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.m_MainMenu.Size = new System.Drawing.Size(1016, 24);
            this.m_MainMenu.TabIndex = 1;
            this.m_MainMenu.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.保存任务ToolStripMenuItem,
            this.重新加载点音源信息ToolStripMenuItem,
            this.客户端文件导出ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 保存任务ToolStripMenuItem
            // 
            this.保存任务ToolStripMenuItem.Name = "保存任务ToolStripMenuItem";
            this.保存任务ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.保存任务ToolStripMenuItem.Text = "保存全部任务";
            this.保存任务ToolStripMenuItem.Click += new System.EventHandler(this.保存任务ToolStripMenuItem_Click);
            // 
            // 重新加载点音源信息ToolStripMenuItem
            // 
            this.重新加载点音源信息ToolStripMenuItem.Name = "重新加载点音源信息ToolStripMenuItem";
            this.重新加载点音源信息ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.重新加载点音源信息ToolStripMenuItem.Text = "重新加载点音源信息";
            this.重新加载点音源信息ToolStripMenuItem.Click += new System.EventHandler(this.重新加载点音源信息ToolStripMenuItem_Click);
            // 
            // 客户端文件导出ToolStripMenuItem
            // 
            this.客户端文件导出ToolStripMenuItem.Name = "客户端文件导出ToolStripMenuItem";
            this.客户端文件导出ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.客户端文件导出ToolStripMenuItem.Text = "客户端文件导出";
            this.客户端文件导出ToolStripMenuItem.Click += new System.EventHandler(this.客户端文件导出ToolStripMenuItem_Click);
            // 
            // 编辑ToolStripMenuItem
            // 
            this.编辑ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.行为ToolStripMenuItem,
            this.原子操作ToolStripMenuItem,
            this.选项ToolStripMenuItem,
            this.获取最新任务数据ToolStripMenuItem,
            this.获取最新状态图数据ToolStripMenuItem});
            this.编辑ToolStripMenuItem.Name = "编辑ToolStripMenuItem";
            this.编辑ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.编辑ToolStripMenuItem.Text = "编辑";
            // 
            // 行为ToolStripMenuItem
            // 
            this.行为ToolStripMenuItem.Name = "行为ToolStripMenuItem";
            this.行为ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.行为ToolStripMenuItem.Text = "转换任务数据到状态图";
            this.行为ToolStripMenuItem.Visible = false;
            // 
            // 原子操作ToolStripMenuItem
            // 
            this.原子操作ToolStripMenuItem.Name = "原子操作ToolStripMenuItem";
            this.原子操作ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.原子操作ToolStripMenuItem.Text = "原子操作";
            this.原子操作ToolStripMenuItem.Visible = false;
            // 
            // 选项ToolStripMenuItem
            // 
            this.选项ToolStripMenuItem.Name = "选项ToolStripMenuItem";
            this.选项ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.选项ToolStripMenuItem.Text = "选项";
            this.选项ToolStripMenuItem.Click += new System.EventHandler(this.选项ToolStripMenuItem_Click);
            // 
            // 获取最新任务数据ToolStripMenuItem
            // 
            this.获取最新任务数据ToolStripMenuItem.Name = "获取最新任务数据ToolStripMenuItem";
            this.获取最新任务数据ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.获取最新任务数据ToolStripMenuItem.Text = "获取最新任务数据";
            this.获取最新任务数据ToolStripMenuItem.Click += new System.EventHandler(this.获取最新任务数据ToolStripMenuItem_Click);
            // 
            // 获取最新状态图数据ToolStripMenuItem
            // 
            this.获取最新状态图数据ToolStripMenuItem.Name = "获取最新状态图数据ToolStripMenuItem";
            this.获取最新状态图数据ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.获取最新状态图数据ToolStripMenuItem.Text = "获取最新状态图数据";
            this.获取最新状态图数据ToolStripMenuItem.Click += new System.EventHandler(this.获取最新状态图数据ToolStripMenuItem_Click);
            // 
            // m_toolStripMenuItemMission
            // 
            this.m_toolStripMenuItemMission.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.转换到状态图ToolStripMenuItem,
            this.批量转换到状态图ToolStripMenuItem,
            this.m_ToolStripMenuItemMissionConditionLua,
            this.导出任务存盘栏位规则ToolStripMenuItem});
            this.m_toolStripMenuItemMission.Name = "m_toolStripMenuItemMission";
            this.m_toolStripMenuItemMission.Size = new System.Drawing.Size(41, 20);
            this.m_toolStripMenuItemMission.Text = "任务";
            // 
            // 转换到状态图ToolStripMenuItem
            // 
            this.转换到状态图ToolStripMenuItem.Name = "转换到状态图ToolStripMenuItem";
            this.转换到状态图ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.转换到状态图ToolStripMenuItem.Text = "转换到状态图";
            this.转换到状态图ToolStripMenuItem.Visible = false;
            this.转换到状态图ToolStripMenuItem.Click += new System.EventHandler(this.转换到状态图ToolStripMenuItem_Click);
            // 
            // 批量转换到状态图ToolStripMenuItem
            // 
            this.批量转换到状态图ToolStripMenuItem.Name = "批量转换到状态图ToolStripMenuItem";
            this.批量转换到状态图ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.批量转换到状态图ToolStripMenuItem.Text = "批量转换到状态图";
            // 
            // m_ToolStripMenuItemMissionConditionLua
            // 
            this.m_ToolStripMenuItemMissionConditionLua.Name = "m_ToolStripMenuItemMissionConditionLua";
            this.m_ToolStripMenuItemMissionConditionLua.Size = new System.Drawing.Size(214, 22);
            this.m_ToolStripMenuItemMissionConditionLua.Text = "导出任务条件脚本(客户端)";
            this.m_ToolStripMenuItemMissionConditionLua.Click += new System.EventHandler(this.m_ToolStripMenuItemMissionConditionLua_Click);
            // 
            // 导出任务存盘栏位规则ToolStripMenuItem
            // 
            this.导出任务存盘栏位规则ToolStripMenuItem.Name = "导出任务存盘栏位规则ToolStripMenuItem";
            this.导出任务存盘栏位规则ToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.导出任务存盘栏位规则ToolStripMenuItem.Text = "导出任务存盘栏位规则";
            this.导出任务存盘栏位规则ToolStripMenuItem.Click += new System.EventHandler(this.导出任务存盘栏位规则ToolStripMenuItem_Click);
            // 
            // m_toolStripMenuItemStateDia
            // 
            this.m_toolStripMenuItemStateDia.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.转换到脚本ToolStripMenuItem,
            this.批量转换到脚本ToolStripMenuItem});
            this.m_toolStripMenuItemStateDia.Name = "m_toolStripMenuItemStateDia";
            this.m_toolStripMenuItemStateDia.Size = new System.Drawing.Size(53, 20);
            this.m_toolStripMenuItemStateDia.Text = "状态图";
            // 
            // 转换到脚本ToolStripMenuItem
            // 
            this.转换到脚本ToolStripMenuItem.Name = "转换到脚本ToolStripMenuItem";
            this.转换到脚本ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.转换到脚本ToolStripMenuItem.Text = "转换到脚本";
            this.转换到脚本ToolStripMenuItem.Click += new System.EventHandler(this.转换到脚本ToolStripMenuItem_Click);
            // 
            // 批量转换到脚本ToolStripMenuItem
            // 
            this.批量转换到脚本ToolStripMenuItem.Name = "批量转换到脚本ToolStripMenuItem";
            this.批量转换到脚本ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.批量转换到脚本ToolStripMenuItem.Text = "批量转换到 脚本";
            this.批量转换到脚本ToolStripMenuItem.Click += new System.EventHandler(this.批量转换到脚本ToolStripMenuItem_Click);
            // 
            // sceneToolStripMenuItem
            // 
            this.sceneToolStripMenuItem.Checked = true;
            this.sceneToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sceneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.区域管理工具ToolStripMenuItem,
            this.toolStripSeparator4,
            this.添加NPCToolStripMenuItem,
            this.添加区域ToolStripMenuItem,
            this.生成器ToolStripMenuItem,
            this.任务脚本测试ToolStripMenuItem,
            this.导出当前地图的路径表ToolStripMenuItem,
            this.导出地图格子信息ToolStripMenuItem,
            this.路径编辑ToolStripMenuItem,
            this.toolStripSeparator5,
            this.启用加速模式ToolStripMenuItem});
            this.sceneToolStripMenuItem.Name = "sceneToolStripMenuItem";
            this.sceneToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.sceneToolStripMenuItem.Text = "场景草图";
            // 
            // 区域管理工具ToolStripMenuItem
            // 
            this.区域管理工具ToolStripMenuItem.Name = "区域管理工具ToolStripMenuItem";
            this.区域管理工具ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.区域管理工具ToolStripMenuItem.Text = "区域管理工具";
            this.区域管理工具ToolStripMenuItem.Click += new System.EventHandler(this.区域管理工具ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(187, 6);
            // 
            // 添加NPCToolStripMenuItem
            // 
            this.添加NPCToolStripMenuItem.Name = "添加NPCToolStripMenuItem";
            this.添加NPCToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.添加NPCToolStripMenuItem.Text = "添加NPC生成器";
            this.添加NPCToolStripMenuItem.Visible = false;
            this.添加NPCToolStripMenuItem.Click += new System.EventHandler(this.添加NPCToolStripMenuItem_Click);
            // 
            // 添加区域ToolStripMenuItem
            // 
            this.添加区域ToolStripMenuItem.Name = "添加区域ToolStripMenuItem";
            this.添加区域ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.添加区域ToolStripMenuItem.Text = "添加区域";
            this.添加区域ToolStripMenuItem.Visible = false;
            this.添加区域ToolStripMenuItem.Click += new System.EventHandler(this.添加区域ToolStripMenuItem_Click);
            // 
            // 生成器ToolStripMenuItem
            // 
            this.生成器ToolStripMenuItem.Name = "生成器ToolStripMenuItem";
            this.生成器ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.生成器ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.生成器ToolStripMenuItem.Text = "保存修改";
            this.生成器ToolStripMenuItem.Click += new System.EventHandler(this.生成器ToolStripMenuItem_Click);
            // 
            // 任务脚本测试ToolStripMenuItem
            // 
            this.任务脚本测试ToolStripMenuItem.Name = "任务脚本测试ToolStripMenuItem";
            this.任务脚本测试ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.任务脚本测试ToolStripMenuItem.Text = "任务脚本测试";
            this.任务脚本测试ToolStripMenuItem.Visible = false;
            this.任务脚本测试ToolStripMenuItem.Click += new System.EventHandler(this.任务脚本测试ToolStripMenuItem_Click);
            // 
            // 导出当前地图的路径表ToolStripMenuItem
            // 
            this.导出当前地图的路径表ToolStripMenuItem.Name = "导出当前地图的路径表ToolStripMenuItem";
            this.导出当前地图的路径表ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.导出当前地图的路径表ToolStripMenuItem.Text = "导出当前地图的路径表";
            this.导出当前地图的路径表ToolStripMenuItem.Visible = false;
            this.导出当前地图的路径表ToolStripMenuItem.Click += new System.EventHandler(this.导出当前地图的路径表ToolStripMenuItem_Click);
            // 
            // 导出地图格子信息ToolStripMenuItem
            // 
            this.导出地图格子信息ToolStripMenuItem.Name = "导出地图格子信息ToolStripMenuItem";
            this.导出地图格子信息ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.导出地图格子信息ToolStripMenuItem.Text = "导出地图格子信息";
            this.导出地图格子信息ToolStripMenuItem.Click += new System.EventHandler(this.导出地图格子信息ToolStripMenuItem_Click);
            // 
            // 路径编辑ToolStripMenuItem
            // 
            this.路径编辑ToolStripMenuItem.Name = "路径编辑ToolStripMenuItem";
            this.路径编辑ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.路径编辑ToolStripMenuItem.Text = "路径编辑";
            this.路径编辑ToolStripMenuItem.Visible = false;
            this.路径编辑ToolStripMenuItem.Click += new System.EventHandler(this.路径编辑ToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(187, 6);
            // 
            // 启用加速模式ToolStripMenuItem
            // 
            this.启用加速模式ToolStripMenuItem.Checked = true;
            this.启用加速模式ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.启用加速模式ToolStripMenuItem.Name = "启用加速模式ToolStripMenuItem";
            this.启用加速模式ToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.启用加速模式ToolStripMenuItem.Text = "启用加速模式";
            this.启用加速模式ToolStripMenuItem.Click += new System.EventHandler(this.启用加速模式ToolStripMenuItem_Click);
            // 
            // m_toolStripComboBoxSceneScale
            // 
            this.m_toolStripComboBoxSceneScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_toolStripComboBoxSceneScale.Items.AddRange(new object[] {
            "原始大小",
            "2x",
            "3x",
            "4x"});
            this.m_toolStripComboBoxSceneScale.Name = "m_toolStripComboBoxSceneScale";
            this.m_toolStripComboBoxSceneScale.Size = new System.Drawing.Size(121, 20);
            this.m_toolStripComboBoxSceneScale.SelectedIndexChanged += new System.EventHandler(this.m_toolStripComboBoxSceneScale_SelectedIndexChanged);
            // 
            // 工具ToolStripMenuItem
            // 
            this.工具ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.商店编辑ToolStripMenuItem,
            this.数据编辑ToolStripMenuItem,
            this.NPC统计ToolStripMenuItem});
            this.工具ToolStripMenuItem.Name = "工具ToolStripMenuItem";
            this.工具ToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.工具ToolStripMenuItem.Text = " 工具";
            // 
            // 商店编辑ToolStripMenuItem
            // 
            this.商店编辑ToolStripMenuItem.Name = "商店编辑ToolStripMenuItem";
            this.商店编辑ToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.商店编辑ToolStripMenuItem.Text = "商店编辑";
            this.商店编辑ToolStripMenuItem.Click += new System.EventHandler(this.商店编辑ToolStripMenuItem_Click);
            // 
            // 数据编辑ToolStripMenuItem
            // 
            this.数据编辑ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.任务奖励编辑ToolStripMenuItem,
            this.任务条件编辑ToolStripMenuItem,
            this.重新ToolStripMenuItem});
            this.数据编辑ToolStripMenuItem.Name = "数据编辑ToolStripMenuItem";
            this.数据编辑ToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.数据编辑ToolStripMenuItem.Text = "数据编辑";
            // 
            // 任务奖励编辑ToolStripMenuItem
            // 
            this.任务奖励编辑ToolStripMenuItem.Name = "任务奖励编辑ToolStripMenuItem";
            this.任务奖励编辑ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.任务奖励编辑ToolStripMenuItem.Text = "任务奖励编辑";
            this.任务奖励编辑ToolStripMenuItem.Click += new System.EventHandler(this.任务奖励编辑ToolStripMenuItem_Click);
            // 
            // 任务条件编辑ToolStripMenuItem
            // 
            this.任务条件编辑ToolStripMenuItem.Name = "任务条件编辑ToolStripMenuItem";
            this.任务条件编辑ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.任务条件编辑ToolStripMenuItem.Text = "任务条件编辑";
            this.任务条件编辑ToolStripMenuItem.Click += new System.EventHandler(this.任务条件编辑ToolStripMenuItem_Click);
            // 
            // 重新ToolStripMenuItem
            // 
            this.重新ToolStripMenuItem.Name = "重新ToolStripMenuItem";
            this.重新ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.重新ToolStripMenuItem.Text = "重新载入";
            this.重新ToolStripMenuItem.Click += new System.EventHandler(this.重新ToolStripMenuItem_Click);
            // 
            // NPC统计ToolStripMenuItem
            // 
            this.NPC统计ToolStripMenuItem.Name = "NPC统计ToolStripMenuItem";
            this.NPC统计ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.NPC统计ToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.NPC统计ToolStripMenuItem.Text = "NPC统计";
            this.NPC统计ToolStripMenuItem.Click += new System.EventHandler(this.NPC统计ToolStripMenuItem_Click);
            // 
            // 报告ToolStripMenuItem
            // 
            this.报告ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导出任务报告ToolStripMenuItem});
            this.报告ToolStripMenuItem.Name = "报告ToolStripMenuItem";
            this.报告ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.报告ToolStripMenuItem.Text = "报告";
            // 
            // 导出任务报告ToolStripMenuItem
            // 
            this.导出任务报告ToolStripMenuItem.Name = "导出任务报告ToolStripMenuItem";
            this.导出任务报告ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.导出任务报告ToolStripMenuItem.Text = "导出任务报告";
            this.导出任务报告ToolStripMenuItem.Click += new System.EventHandler(this.导出任务报告ToolStripMenuItem_Click);
            // 
            // m_tabControl
            // 
            this.m_tabControl.Controls.Add(this.m_tabPageTaskUI);
            this.m_tabControl.Controls.Add(this.m_tabPageState);
            this.m_tabControl.Controls.Add(this.m_tabPageSkeneScetch);
            this.m_tabControl.Controls.Add(this.m_tabPageMission);
            this.m_tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_tabControl.Location = new System.Drawing.Point(0, 24);
            this.m_tabControl.Name = "m_tabControl";
            this.m_tabControl.SelectedIndex = 0;
            this.m_tabControl.Size = new System.Drawing.Size(1016, 710);
            this.m_tabControl.TabIndex = 2;
            this.m_tabControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_tabControl_KeyDown);
            // 
            // m_tabPageTaskUI
            // 
            this.m_tabPageTaskUI.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageTaskUI.Name = "m_tabPageTaskUI";
            this.m_tabPageTaskUI.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageTaskUI.Size = new System.Drawing.Size(1008, 685);
            this.m_tabPageTaskUI.TabIndex = 3;
            this.m_tabPageTaskUI.Text = "新任务界面";
            this.m_tabPageTaskUI.UseVisualStyleBackColor = true;
            // 
            // m_tabPageState
            // 
            this.m_tabPageState.Controls.Add(this.m_splitContainerState);
            this.m_tabPageState.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageState.Name = "m_tabPageState";
            this.m_tabPageState.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageState.Size = new System.Drawing.Size(1008, 685);
            this.m_tabPageState.TabIndex = 0;
            this.m_tabPageState.Text = "状态图";
            this.m_tabPageState.UseVisualStyleBackColor = true;
            // 
            // m_splitContainerState
            // 
            this.m_splitContainerState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainerState.Location = new System.Drawing.Point(3, 3);
            this.m_splitContainerState.Name = "m_splitContainerState";
            // 
            // m_splitContainerState.Panel1
            // 
            this.m_splitContainerState.Panel1.Controls.Add(this.m_listBoxMO);
            this.m_splitContainerState.Panel1.Controls.Add(this.m_MOFilter);
            // 
            // m_splitContainerState.Panel2
            // 
            this.m_splitContainerState.Panel2.AutoScroll = true;
            this.m_splitContainerState.Panel2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.m_splitContainerState.Panel2.ContextMenuStrip = this.m_menuStateDiagram;
            this.m_splitContainerState.Panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_splitContainerState_Panel2_MouseDown);
            this.m_splitContainerState.Panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.m_splitContainerState_Panel2_MouseUp);
            this.m_splitContainerState.Size = new System.Drawing.Size(1002, 679);
            this.m_splitContainerState.SplitterDistance = 189;
            this.m_splitContainerState.TabIndex = 0;
            // 
            // m_listBoxMO
            // 
            this.m_listBoxMO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_listBoxMO.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.m_listBoxMO.FormattingEnabled = true;
            this.m_listBoxMO.ItemHeight = 12;
            this.m_listBoxMO.Location = new System.Drawing.Point(0, 20);
            this.m_listBoxMO.Name = "m_listBoxMO";
            this.m_listBoxMO.Size = new System.Drawing.Size(189, 652);
            this.m_listBoxMO.TabIndex = 1;
            this.m_listBoxMO.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.m_listBoxMO_DrawItem);
            this.m_listBoxMO.SelectedIndexChanged += new System.EventHandler(this.m_listBoxMO_SelectedIndexChanged);
            this.m_listBoxMO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_listBoxMO_KeyDown);
            // 
            // m_MOFilter
            // 
            this.m_MOFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_MOFilter.FormattingEnabled = true;
            this.m_MOFilter.Location = new System.Drawing.Point(0, 0);
            this.m_MOFilter.Name = "m_MOFilter";
            this.m_MOFilter.Size = new System.Drawing.Size(189, 20);
            this.m_MOFilter.TabIndex = 0;
            this.m_MOFilter.SelectedIndexChanged += new System.EventHandler(this.m_MOFilter_SelectedIndexChanged);
            // 
            // m_menuStateDiagram
            // 
            this.m_menuStateDiagram.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.版本管理ToolStripMenuItem,
            this.toolStripSeparator6,
            this.添加状态ToolStripMenuItem,
            this.添加连接ToolStripMenuItem,
            this.toolStripSeparator1,
            this.新建状态图ToolStripMenuItem,
            this.保存状态图ToolStripMenuItem,
            this.刷新状态图ToolStripMenuItem,
            this.toolStripSeparator2,
            this.m_toolStripMenuItemCopy,
            this.m_toolStripMenuItemPaste,
            this.toolStripSeparator3,
            this.打开所在文件夹ToolStripMenuItem,
            this.m_toolStripMenuItemConvertDiaToLua});
            this.m_menuStateDiagram.Name = "m_menuStrip1";
            this.m_menuStateDiagram.Size = new System.Drawing.Size(197, 248);
            // 
            // 版本管理ToolStripMenuItem
            // 
            this.版本管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.获取最新版本ToolStripMenuItem,
            this.toolStripSeparator7,
            this.签出ToolStripMenuItem,
            this.签入ToolStripMenuItem,
            this.toolStripSeparator8,
            this.导入新状态图ToolStripMenuItem,
            this.toolStripSeparator9,
            this.撤销更改ToolStripMenuItem});
            this.版本管理ToolStripMenuItem.Name = "版本管理ToolStripMenuItem";
            this.版本管理ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.版本管理ToolStripMenuItem.Text = "版本管理";
            // 
            // 获取最新版本ToolStripMenuItem
            // 
            this.获取最新版本ToolStripMenuItem.Name = "获取最新版本ToolStripMenuItem";
            this.获取最新版本ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.获取最新版本ToolStripMenuItem.Text = "获取最新版本";
            this.获取最新版本ToolStripMenuItem.Click += new System.EventHandler(this.获取最新版本ToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(139, 6);
            // 
            // 签出ToolStripMenuItem
            // 
            this.签出ToolStripMenuItem.Name = "签出ToolStripMenuItem";
            this.签出ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.签出ToolStripMenuItem.Text = "签出";
            this.签出ToolStripMenuItem.Click += new System.EventHandler(this.签出ToolStripMenuItem_Click);
            // 
            // 签入ToolStripMenuItem
            // 
            this.签入ToolStripMenuItem.Name = "签入ToolStripMenuItem";
            this.签入ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.签入ToolStripMenuItem.Text = "签入";
            this.签入ToolStripMenuItem.Click += new System.EventHandler(this.签入ToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(139, 6);
            // 
            // 导入新状态图ToolStripMenuItem
            // 
            this.导入新状态图ToolStripMenuItem.Name = "导入新状态图ToolStripMenuItem";
            this.导入新状态图ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.导入新状态图ToolStripMenuItem.Text = "导入新状态图";
            this.导入新状态图ToolStripMenuItem.Click += new System.EventHandler(this.导入新状态图ToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(139, 6);
            // 
            // 撤销更改ToolStripMenuItem
            // 
            this.撤销更改ToolStripMenuItem.Name = "撤销更改ToolStripMenuItem";
            this.撤销更改ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.撤销更改ToolStripMenuItem.Text = "撤销更改";
            this.撤销更改ToolStripMenuItem.Click += new System.EventHandler(this.撤销更改ToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(193, 6);
            // 
            // 添加状态ToolStripMenuItem
            // 
            this.添加状态ToolStripMenuItem.Name = "添加状态ToolStripMenuItem";
            this.添加状态ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.添加状态ToolStripMenuItem.Text = "添加状态";
            this.添加状态ToolStripMenuItem.Click += new System.EventHandler(this.添加状态ToolStripMenuItem_Click);
            // 
            // 添加连接ToolStripMenuItem
            // 
            this.添加连接ToolStripMenuItem.Name = "添加连接ToolStripMenuItem";
            this.添加连接ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.添加连接ToolStripMenuItem.Text = "添加转换";
            this.添加连接ToolStripMenuItem.Click += new System.EventHandler(this.添加连接ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(193, 6);
            // 
            // 新建状态图ToolStripMenuItem
            // 
            this.新建状态图ToolStripMenuItem.Name = "新建状态图ToolStripMenuItem";
            this.新建状态图ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.新建状态图ToolStripMenuItem.Text = "新建状态图";
            this.新建状态图ToolStripMenuItem.Click += new System.EventHandler(this.新建状态图ToolStripMenuItem_Click);
            // 
            // 保存状态图ToolStripMenuItem
            // 
            this.保存状态图ToolStripMenuItem.Name = "保存状态图ToolStripMenuItem";
            this.保存状态图ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.保存状态图ToolStripMenuItem.Text = "保存状态图";
            this.保存状态图ToolStripMenuItem.Click += new System.EventHandler(this.保存当前状态图ToolStripMenuItem_Click);
            // 
            // 刷新状态图ToolStripMenuItem
            // 
            this.刷新状态图ToolStripMenuItem.Name = "刷新状态图ToolStripMenuItem";
            this.刷新状态图ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.刷新状态图ToolStripMenuItem.Text = "刷新状态图";
            this.刷新状态图ToolStripMenuItem.Click += new System.EventHandler(this.刷新状态图ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(193, 6);
            // 
            // m_toolStripMenuItemCopy
            // 
            this.m_toolStripMenuItemCopy.Name = "m_toolStripMenuItemCopy";
            this.m_toolStripMenuItemCopy.Size = new System.Drawing.Size(196, 22);
            this.m_toolStripMenuItemCopy.Text = "复制";
            this.m_toolStripMenuItemCopy.Click += new System.EventHandler(this.m_toolStripMenuItemCopy_Click);
            // 
            // m_toolStripMenuItemPaste
            // 
            this.m_toolStripMenuItemPaste.Name = "m_toolStripMenuItemPaste";
            this.m_toolStripMenuItemPaste.Size = new System.Drawing.Size(196, 22);
            this.m_toolStripMenuItemPaste.Text = "粘贴";
            this.m_toolStripMenuItemPaste.Click += new System.EventHandler(this.m_toolStripMenuItemPaste_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(193, 6);
            // 
            // 打开所在文件夹ToolStripMenuItem
            // 
            this.打开所在文件夹ToolStripMenuItem.Name = "打开所在文件夹ToolStripMenuItem";
            this.打开所在文件夹ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.打开所在文件夹ToolStripMenuItem.Text = "打开所在文件夹";
            this.打开所在文件夹ToolStripMenuItem.Click += new System.EventHandler(this.打开所在文件夹ToolStripMenuItem_Click);
            // 
            // m_toolStripMenuItemConvertDiaToLua
            // 
            this.m_toolStripMenuItemConvertDiaToLua.Name = "m_toolStripMenuItemConvertDiaToLua";
            this.m_toolStripMenuItemConvertDiaToLua.Size = new System.Drawing.Size(196, 22);
            this.m_toolStripMenuItemConvertDiaToLua.Text = "转换状态图到 Lua 脚本";
            this.m_toolStripMenuItemConvertDiaToLua.Click += new System.EventHandler(this.m_toolStripMenuItemConvertDiaToLua_Click);
            // 
            // m_tabPageSkeneScetch
            // 
            this.m_tabPageSkeneScetch.Controls.Add(this.m_splitContainerSceneSketch);
            this.m_tabPageSkeneScetch.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageSkeneScetch.Name = "m_tabPageSkeneScetch";
            this.m_tabPageSkeneScetch.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageSkeneScetch.Size = new System.Drawing.Size(1008, 685);
            this.m_tabPageSkeneScetch.TabIndex = 1;
            this.m_tabPageSkeneScetch.Text = "场景草图";
            this.m_tabPageSkeneScetch.UseVisualStyleBackColor = true;
            // 
            // m_splitContainerSceneSketch
            // 
            this.m_splitContainerSceneSketch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainerSceneSketch.Location = new System.Drawing.Point(3, 3);
            this.m_splitContainerSceneSketch.Name = "m_splitContainerSceneSketch";
            // 
            // m_splitContainerSceneSketch.Panel1
            // 
            this.m_splitContainerSceneSketch.Panel1.Controls.Add(this.m_splitContainerSceneScketchLeft);
            // 
            // m_splitContainerSceneSketch.Panel2
            // 
            this.m_splitContainerSceneSketch.Panel2.Controls.Add(this.m_statusStripScene);
            this.m_splitContainerSceneSketch.Panel2.Controls.Add(this.m_pictureBoxSceneSketch);
            this.m_splitContainerSceneSketch.Size = new System.Drawing.Size(1002, 679);
            this.m_splitContainerSceneSketch.SplitterDistance = 192;
            this.m_splitContainerSceneSketch.TabIndex = 1;
            // 
            // m_splitContainerSceneScketchLeft
            // 
            this.m_splitContainerSceneScketchLeft.BackColor = System.Drawing.SystemColors.ControlDark;
            this.m_splitContainerSceneScketchLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainerSceneScketchLeft.Location = new System.Drawing.Point(0, 0);
            this.m_splitContainerSceneScketchLeft.Name = "m_splitContainerSceneScketchLeft";
            this.m_splitContainerSceneScketchLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // m_splitContainerSceneScketchLeft.Panel1
            // 
            this.m_splitContainerSceneScketchLeft.Panel1.Controls.Add(this.m_listBoxSceneID);
            // 
            // m_splitContainerSceneScketchLeft.Panel2
            // 
            this.m_splitContainerSceneScketchLeft.Panel2.Controls.Add(this.m_propertyGrid);
            this.m_splitContainerSceneScketchLeft.Size = new System.Drawing.Size(192, 679);
            this.m_splitContainerSceneScketchLeft.SplitterDistance = 334;
            this.m_splitContainerSceneScketchLeft.TabIndex = 1;
            // 
            // m_listBoxSceneID
            // 
            this.m_listBoxSceneID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_listBoxSceneID.FormattingEnabled = true;
            this.m_listBoxSceneID.ItemHeight = 12;
            this.m_listBoxSceneID.Location = new System.Drawing.Point(0, 0);
            this.m_listBoxSceneID.Name = "m_listBoxSceneID";
            this.m_listBoxSceneID.Size = new System.Drawing.Size(192, 328);
            this.m_listBoxSceneID.TabIndex = 0;
            this.m_listBoxSceneID.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSceneID_SelectedIndexChanged);
            // 
            // m_propertyGrid
            // 
            this.m_propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_propertyGrid.Location = new System.Drawing.Point(0, 0);
            this.m_propertyGrid.Name = "m_propertyGrid";
            this.m_propertyGrid.Size = new System.Drawing.Size(192, 341);
            this.m_propertyGrid.TabIndex = 0;
            // 
            // m_statusStripScene
            // 
            this.m_statusStripScene.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_toolStripStatusLabelTitle,
            this.m_toolStripStatusLabelNpcShow,
            this.toolStripStatusLabel1,
            this.m_toolStripStatusLabelNpcNum,
            this.toolStripStatusLabel2,
            this.m_toolStripStatusLabelSubNpcShow1,
            this.toolStripStatusLabel3,
            this.m_toolStripStatusLabelSubNpcShow2,
            this.toolStripStatusLabel5,
            this.m_toolStripStatusLabelSubNpcShow3,
            this.m_toolStripSplitButtonNpcHighLight});
            this.m_statusStripScene.Location = new System.Drawing.Point(0, 657);
            this.m_statusStripScene.Name = "m_statusStripScene";
            this.m_statusStripScene.Size = new System.Drawing.Size(806, 22);
            this.m_statusStripScene.TabIndex = 1;
            this.m_statusStripScene.Text = "statusStrip1";
            // 
            // m_toolStripStatusLabelTitle
            // 
            this.m_toolStripStatusLabelTitle.Name = "m_toolStripStatusLabelTitle";
            this.m_toolStripStatusLabelTitle.Size = new System.Drawing.Size(53, 17);
            this.m_toolStripStatusLabelTitle.Text = "NPC统计\"";
            // 
            // m_toolStripStatusLabelNpcShow
            // 
            this.m_toolStripStatusLabelNpcShow.Name = "m_toolStripStatusLabelNpcShow";
            this.m_toolStripStatusLabelNpcShow.Size = new System.Drawing.Size(47, 17);
            this.m_toolStripStatusLabelNpcShow.Text = "1000000";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(17, 17);
            this.toolStripStatusLabel1.Text = "\":";
            // 
            // m_toolStripStatusLabelNpcNum
            // 
            this.m_toolStripStatusLabelNpcNum.Name = "m_toolStripStatusLabelNpcNum";
            this.m_toolStripStatusLabelNpcNum.Size = new System.Drawing.Size(11, 17);
            this.m_toolStripStatusLabelNpcNum.Text = "0";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(47, 17);
            this.toolStripStatusLabel2.Text = "------>";
            // 
            // m_toolStripStatusLabelSubNpcShow1
            // 
            this.m_toolStripStatusLabelSubNpcShow1.Name = "m_toolStripStatusLabelSubNpcShow1";
            this.m_toolStripStatusLabelSubNpcShow1.Size = new System.Drawing.Size(23, 17);
            this.m_toolStripStatusLabelSubNpcShow1.Text = "(:)";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(11, 17);
            this.toolStripStatusLabel3.Text = ",";
            // 
            // m_toolStripStatusLabelSubNpcShow2
            // 
            this.m_toolStripStatusLabelSubNpcShow2.Name = "m_toolStripStatusLabelSubNpcShow2";
            this.m_toolStripStatusLabelSubNpcShow2.Size = new System.Drawing.Size(23, 17);
            this.m_toolStripStatusLabelSubNpcShow2.Text = "(:)";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(11, 17);
            this.toolStripStatusLabel5.Text = ",";
            // 
            // m_toolStripStatusLabelSubNpcShow3
            // 
            this.m_toolStripStatusLabelSubNpcShow3.Name = "m_toolStripStatusLabelSubNpcShow3";
            this.m_toolStripStatusLabelSubNpcShow3.Size = new System.Drawing.Size(23, 17);
            this.m_toolStripStatusLabelSubNpcShow3.Text = "(:)";
            // 
            // m_toolStripSplitButtonNpcHighLight
            // 
            this.m_toolStripSplitButtonNpcHighLight.BackColor = System.Drawing.Color.White;
            this.m_toolStripSplitButtonNpcHighLight.ForeColor = System.Drawing.Color.Black;
            this.m_toolStripSplitButtonNpcHighLight.Image = ((System.Drawing.Image)(resources.GetObject("m_toolStripSplitButtonNpcHighLight.Image")));
            this.m_toolStripSplitButtonNpcHighLight.ImageTransparentColor = System.Drawing.Color.White;
            this.m_toolStripSplitButtonNpcHighLight.Name = "m_toolStripSplitButtonNpcHighLight";
            this.m_toolStripSplitButtonNpcHighLight.Size = new System.Drawing.Size(85, 20);
            this.m_toolStripSplitButtonNpcHighLight.Text = "高亮显示";
            this.m_toolStripSplitButtonNpcHighLight.ButtonClick += new System.EventHandler(this.m_toolStripSplitButtonNpcHighLight_ButtonClick);
            // 
            // m_pictureBoxSceneSketch
            // 
            this.m_pictureBoxSceneSketch.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.m_pictureBoxSceneSketch.Location = new System.Drawing.Point(0, 0);
            this.m_pictureBoxSceneSketch.Name = "m_pictureBoxSceneSketch";
            this.m_pictureBoxSceneSketch.Size = new System.Drawing.Size(1024, 1024);
            this.m_pictureBoxSceneSketch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.m_pictureBoxSceneSketch.TabIndex = 0;
            this.m_pictureBoxSceneSketch.TabStop = false;
            this.m_pictureBoxSceneSketch.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.m_pictureBoxSceneSketch_MouseWheel);
            this.m_pictureBoxSceneSketch.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.m_pictureBoxSceneSketch_PreviewKeyDown);
            this.m_pictureBoxSceneSketch.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_pictureBoxSceneSketch_MouseDown);
            this.m_pictureBoxSceneSketch.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_pictureBoxSceneSketch_MouseMove);
            this.m_pictureBoxSceneSketch.MouseUp += new System.Windows.Forms.MouseEventHandler(this.m_pictureBoxSceneSketch_MouseUp);
            // 
            // m_tabPageMission
            // 
            this.m_tabPageMission.Controls.Add(this.m_splitContainerMission);
            this.m_tabPageMission.Location = new System.Drawing.Point(4, 21);
            this.m_tabPageMission.Name = "m_tabPageMission";
            this.m_tabPageMission.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabPageMission.Size = new System.Drawing.Size(1008, 685);
            this.m_tabPageMission.TabIndex = 2;
            this.m_tabPageMission.Text = "任务";
            this.m_tabPageMission.UseVisualStyleBackColor = true;
            // 
            // m_splitContainerMission
            // 
            this.m_splitContainerMission.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainerMission.Location = new System.Drawing.Point(3, 3);
            this.m_splitContainerMission.Name = "m_splitContainerMission";
            // 
            // m_splitContainerMission.Panel1
            // 
            this.m_splitContainerMission.Panel1.Controls.Add(this.m_treeViewMission);
            // 
            // m_splitContainerMission.Panel2
            // 
            this.m_splitContainerMission.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnEditSimpleMissionDec);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_webBrowserSimpleMissionDesc);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnClearTargetArea);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxTargetArea);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_checkBoxRegionProvider);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxFlowType);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxReservedFilter);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_textBoxTimeLimit);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label14);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxStarLevel);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label13);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label12);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxLeadMap);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label11);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnExternalLoad);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label10);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnClearTargetValue);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_webBrowserSubmitMissionDesc);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnEditSubmitMissionDec);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_textBoxTrackName);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label9);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_textBoxReserved);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label8);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnClearTargetMonster);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnClearTargetItem);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnClearTargetProp);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnClearTargetNpc);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label7);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_textBoxLevel);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label5);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxClass);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_webBrowserGetMissionDesc);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnEditGetMissionDec);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_checkBoxItemProvider);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxTargetMonsterEnd);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxTargetItemEnd);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxTargetNPCFilter);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxSubmitToFilter);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_listBoxSelectedAwardValue);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnSelectedAwardEdit);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_listBoxSelectedAward);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label6);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_listBoxFixedAwardValue);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_listBoxConditionValue);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxTargetMonsterBegin);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelTargetMonster);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxProviderFilter);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_checkBoxGiveUp);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxTargetProp);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelTargetProp);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnNew);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_checkBoxShare);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_checkBoxRepeat);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelTargetArea);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxSubType);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxTargetItemBegin);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelTargetItem);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_textBoxTargetValue);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnSave);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelTargetValue);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxTargetNPC);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelTargetNPC);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnFixedAwardEdit);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnConvertToStateDiagrom);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_btnConditionEdit);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_listBoxFixedAward);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_listBoxCondition);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxSubmitTo);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxProvider);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_comboBoxType);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_textBoxName);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_textBoxID);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelAward);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelSubmitTo);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelProvider);
            this.m_splitContainerMission.Panel2.Controls.Add(this.m_labelCondition);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label4);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label3);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label2);
            this.m_splitContainerMission.Panel2.Controls.Add(this.label1);
            this.m_splitContainerMission.Size = new System.Drawing.Size(1002, 679);
            this.m_splitContainerMission.SplitterDistance = 121;
            this.m_splitContainerMission.TabIndex = 0;
            // 
            // m_treeViewMission
            // 
            this.m_treeViewMission.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_treeViewMission.Location = new System.Drawing.Point(0, 0);
            this.m_treeViewMission.Name = "m_treeViewMission";
            this.m_treeViewMission.Size = new System.Drawing.Size(121, 679);
            this.m_treeViewMission.TabIndex = 0;
            this.m_treeViewMission.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.m_treeViewMission_AfterSelect);
            this.m_treeViewMission.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_treeViewMission_KeyDown);
            // 
            // m_btnEditSimpleMissionDec
            // 
            this.m_btnEditSimpleMissionDec.Location = new System.Drawing.Point(510, 45);
            this.m_btnEditSimpleMissionDec.Name = "m_btnEditSimpleMissionDec";
            this.m_btnEditSimpleMissionDec.Size = new System.Drawing.Size(47, 20);
            this.m_btnEditSimpleMissionDec.TabIndex = 105;
            this.m_btnEditSimpleMissionDec.Text = "编辑";
            this.m_btnEditSimpleMissionDec.UseVisualStyleBackColor = true;
            this.m_btnEditSimpleMissionDec.Click += new System.EventHandler(this.m_btnEditSimpleMissionDec_Click);
            // 
            // m_webBrowserSimpleMissionDesc
            // 
            this.m_webBrowserSimpleMissionDesc.Location = new System.Drawing.Point(105, 45);
            this.m_webBrowserSimpleMissionDesc.MinimumSize = new System.Drawing.Size(20, 20);
            this.m_webBrowserSimpleMissionDesc.Name = "m_webBrowserSimpleMissionDesc";
            this.m_webBrowserSimpleMissionDesc.Size = new System.Drawing.Size(367, 60);
            this.m_webBrowserSimpleMissionDesc.TabIndex = 104;
            // 
            // m_btnClearTargetArea
            // 
            this.m_btnClearTargetArea.Location = new System.Drawing.Point(591, 535);
            this.m_btnClearTargetArea.Name = "m_btnClearTargetArea";
            this.m_btnClearTargetArea.Size = new System.Drawing.Size(10, 10);
            this.m_btnClearTargetArea.TabIndex = 103;
            this.m_btnClearTargetArea.UseVisualStyleBackColor = true;
            this.m_btnClearTargetArea.Click += new System.EventHandler(this.m_btnClearTargetArea_Click);
            // 
            // m_comboBoxTargetArea
            // 
            this.m_comboBoxTargetArea.Enabled = false;
            this.m_comboBoxTargetArea.FormattingEnabled = true;
            this.m_comboBoxTargetArea.Location = new System.Drawing.Point(425, 535);
            this.m_comboBoxTargetArea.Name = "m_comboBoxTargetArea";
            this.m_comboBoxTargetArea.Size = new System.Drawing.Size(167, 20);
            this.m_comboBoxTargetArea.TabIndex = 102;
            // 
            // m_checkBoxRegionProvider
            // 
            this.m_checkBoxRegionProvider.AutoSize = true;
            this.m_checkBoxRegionProvider.Location = new System.Drawing.Point(535, 319);
            this.m_checkBoxRegionProvider.Name = "m_checkBoxRegionProvider";
            this.m_checkBoxRegionProvider.Size = new System.Drawing.Size(96, 16);
            this.m_checkBoxRegionProvider.TabIndex = 101;
            this.m_checkBoxRegionProvider.Text = "区域触发任务";
            this.m_checkBoxRegionProvider.UseVisualStyleBackColor = true;
            this.m_checkBoxRegionProvider.CheckedChanged += new System.EventHandler(this.m_checkBoxRegionProvider_CheckedChanged);
            // 
            // m_comboBoxFlowType
            // 
            this.m_comboBoxFlowType.FormattingEnabled = true;
            this.m_comboBoxFlowType.Items.AddRange(new object[] {
            "A类",
            "B类"});
            this.m_comboBoxFlowType.Location = new System.Drawing.Point(373, 255);
            this.m_comboBoxFlowType.Name = "m_comboBoxFlowType";
            this.m_comboBoxFlowType.Size = new System.Drawing.Size(45, 20);
            this.m_comboBoxFlowType.TabIndex = 100;
            this.m_comboBoxFlowType.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxFlowType_SelectedIndexChanged);
            // 
            // m_comboBoxReservedFilter
            // 
            this.m_comboBoxReservedFilter.FormattingEnabled = true;
            this.m_comboBoxReservedFilter.Items.AddRange(new object[] {
            "不存",
            "天",
            "遍",
            "永久保存"});
            this.m_comboBoxReservedFilter.Location = new System.Drawing.Point(151, 315);
            this.m_comboBoxReservedFilter.Name = "m_comboBoxReservedFilter";
            this.m_comboBoxReservedFilter.Size = new System.Drawing.Size(80, 20);
            this.m_comboBoxReservedFilter.TabIndex = 99;
            this.m_comboBoxReservedFilter.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxReservedFilter_SelectedIndexChanged);
            // 
            // m_textBoxTimeLimit
            // 
            this.m_textBoxTimeLimit.Location = new System.Drawing.Point(346, 315);
            this.m_textBoxTimeLimit.Name = "m_textBoxTimeLimit";
            this.m_textBoxTimeLimit.Size = new System.Drawing.Size(80, 21);
            this.m_textBoxTimeLimit.TabIndex = 98;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(280, 319);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 12);
            this.label14.TabIndex = 97;
            this.label14.Text = "时间限制:";
            // 
            // m_comboBoxStarLevel
            // 
            this.m_comboBoxStarLevel.FormattingEnabled = true;
            this.m_comboBoxStarLevel.Items.AddRange(new object[] {
            "一星",
            "二星",
            "三星",
            "四星",
            "五星"});
            this.m_comboBoxStarLevel.Location = new System.Drawing.Point(474, 285);
            this.m_comboBoxStarLevel.Name = "m_comboBoxStarLevel";
            this.m_comboBoxStarLevel.Size = new System.Drawing.Size(121, 20);
            this.m_comboBoxStarLevel.TabIndex = 96;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(430, 289);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 12);
            this.label13.TabIndex = 95;
            this.label13.Text = "难度:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 12);
            this.label12.TabIndex = 93;
            this.label12.Text = "简单描述:";
            // 
            // m_comboBoxLeadMap
            // 
            this.m_comboBoxLeadMap.FormattingEnabled = true;
            this.m_comboBoxLeadMap.Location = new System.Drawing.Point(605, 15);
            this.m_comboBoxLeadMap.Name = "m_comboBoxLeadMap";
            this.m_comboBoxLeadMap.Size = new System.Drawing.Size(121, 20);
            this.m_comboBoxLeadMap.Sorted = true;
            this.m_comboBoxLeadMap.TabIndex = 92;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(540, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 12);
            this.label11.TabIndex = 91;
            this.label11.Text = "所处区域:";
            // 
            // m_btnExternalLoad
            // 
            this.m_btnExternalLoad.Location = new System.Drawing.Point(624, 645);
            this.m_btnExternalLoad.Name = "m_btnExternalLoad";
            this.m_btnExternalLoad.Size = new System.Drawing.Size(169, 25);
            this.m_btnExternalLoad.TabIndex = 90;
            this.m_btnExternalLoad.Text = "外部加载";
            this.m_btnExternalLoad.UseVisualStyleBackColor = true;
            this.m_btnExternalLoad.Click += new System.EventHandler(this.m_btnExternalLoad_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(30, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 12);
            this.label10.TabIndex = 89;
            this.label10.Text = "还任务描述:";
            // 
            // m_btnClearTargetValue
            // 
            this.m_btnClearTargetValue.Location = new System.Drawing.Point(444, 590);
            this.m_btnClearTargetValue.Name = "m_btnClearTargetValue";
            this.m_btnClearTargetValue.Size = new System.Drawing.Size(10, 10);
            this.m_btnClearTargetValue.TabIndex = 88;
            this.m_btnClearTargetValue.UseVisualStyleBackColor = true;
            this.m_btnClearTargetValue.Click += new System.EventHandler(this.m_btnClearTargetValue_Click);
            // 
            // m_webBrowserSubmitMissionDesc
            // 
            this.m_webBrowserSubmitMissionDesc.Location = new System.Drawing.Point(105, 185);
            this.m_webBrowserSubmitMissionDesc.MinimumSize = new System.Drawing.Size(20, 20);
            this.m_webBrowserSubmitMissionDesc.Name = "m_webBrowserSubmitMissionDesc";
            this.m_webBrowserSubmitMissionDesc.Size = new System.Drawing.Size(367, 60);
            this.m_webBrowserSubmitMissionDesc.TabIndex = 87;
            // 
            // m_btnEditSubmitMissionDec
            // 
            this.m_btnEditSubmitMissionDec.Location = new System.Drawing.Point(510, 185);
            this.m_btnEditSubmitMissionDec.Name = "m_btnEditSubmitMissionDec";
            this.m_btnEditSubmitMissionDec.Size = new System.Drawing.Size(47, 20);
            this.m_btnEditSubmitMissionDec.TabIndex = 86;
            this.m_btnEditSubmitMissionDec.Text = "编辑";
            this.m_btnEditSubmitMissionDec.UseVisualStyleBackColor = true;
            this.m_btnEditSubmitMissionDec.Click += new System.EventHandler(this.m_btnEditSubmitMissionDec_Click);
            // 
            // m_textBoxTrackName
            // 
            this.m_textBoxTrackName.Location = new System.Drawing.Point(105, 590);
            this.m_textBoxTrackName.Name = "m_textBoxTrackName";
            this.m_textBoxTrackName.Size = new System.Drawing.Size(96, 21);
            this.m_textBoxTrackName.TabIndex = 85;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 594);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 84;
            this.label9.Text = "追踪名称:";
            // 
            // m_textBoxReserved
            // 
            this.m_textBoxReserved.Location = new System.Drawing.Point(105, 315);
            this.m_textBoxReserved.Name = "m_textBoxReserved";
            this.m_textBoxReserved.Size = new System.Drawing.Size(40, 21);
            this.m_textBoxReserved.TabIndex = 83;
            this.m_textBoxReserved.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 319);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 12);
            this.label8.TabIndex = 82;
            this.label8.Text = "存盘方式:";
            // 
            // m_btnClearTargetMonster
            // 
            this.m_btnClearTargetMonster.Location = new System.Drawing.Point(591, 475);
            this.m_btnClearTargetMonster.Name = "m_btnClearTargetMonster";
            this.m_btnClearTargetMonster.Size = new System.Drawing.Size(10, 10);
            this.m_btnClearTargetMonster.TabIndex = 81;
            this.m_btnClearTargetMonster.UseVisualStyleBackColor = true;
            this.m_btnClearTargetMonster.Click += new System.EventHandler(this.m_btnClearTargetMonster_Click);
            // 
            // m_btnClearTargetItem
            // 
            this.m_btnClearTargetItem.Location = new System.Drawing.Point(271, 535);
            this.m_btnClearTargetItem.Name = "m_btnClearTargetItem";
            this.m_btnClearTargetItem.Size = new System.Drawing.Size(10, 10);
            this.m_btnClearTargetItem.TabIndex = 80;
            this.m_btnClearTargetItem.UseVisualStyleBackColor = true;
            this.m_btnClearTargetItem.Click += new System.EventHandler(this.m_btnClearTargetItem_Click);
            // 
            // m_btnClearTargetProp
            // 
            this.m_btnClearTargetProp.Location = new System.Drawing.Point(271, 505);
            this.m_btnClearTargetProp.Name = "m_btnClearTargetProp";
            this.m_btnClearTargetProp.Size = new System.Drawing.Size(10, 10);
            this.m_btnClearTargetProp.TabIndex = 79;
            this.m_btnClearTargetProp.UseVisualStyleBackColor = true;
            this.m_btnClearTargetProp.Visible = false;
            this.m_btnClearTargetProp.Click += new System.EventHandler(this.m_btnClearTargetProp_Click);
            // 
            // m_btnClearTargetNpc
            // 
            this.m_btnClearTargetNpc.Location = new System.Drawing.Point(296, 475);
            this.m_btnClearTargetNpc.Name = "m_btnClearTargetNpc";
            this.m_btnClearTargetNpc.Size = new System.Drawing.Size(10, 10);
            this.m_btnClearTargetNpc.TabIndex = 78;
            this.m_btnClearTargetNpc.UseVisualStyleBackColor = true;
            this.m_btnClearTargetNpc.Click += new System.EventHandler(this.m_btnClearTargetNpc_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 289);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 77;
            this.label7.Text = "类别:";
            // 
            // m_textBoxLevel
            // 
            this.m_textBoxLevel.Location = new System.Drawing.Point(320, 285);
            this.m_textBoxLevel.Name = "m_textBoxLevel";
            this.m_textBoxLevel.Size = new System.Drawing.Size(96, 21);
            this.m_textBoxLevel.TabIndex = 76;
            this.m_textBoxLevel.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(280, 289);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 75;
            this.label5.Text = "等级:";
            // 
            // m_comboBoxClass
            // 
            this.m_comboBoxClass.FormattingEnabled = true;
            this.m_comboBoxClass.Location = new System.Drawing.Point(105, 285);
            this.m_comboBoxClass.Name = "m_comboBoxClass";
            this.m_comboBoxClass.Size = new System.Drawing.Size(167, 20);
            this.m_comboBoxClass.Sorted = true;
            this.m_comboBoxClass.TabIndex = 74;
            // 
            // m_webBrowserGetMissionDesc
            // 
            this.m_webBrowserGetMissionDesc.Location = new System.Drawing.Point(105, 115);
            this.m_webBrowserGetMissionDesc.MinimumSize = new System.Drawing.Size(20, 20);
            this.m_webBrowserGetMissionDesc.Name = "m_webBrowserGetMissionDesc";
            this.m_webBrowserGetMissionDesc.Size = new System.Drawing.Size(367, 60);
            this.m_webBrowserGetMissionDesc.TabIndex = 73;
            // 
            // m_btnEditGetMissionDec
            // 
            this.m_btnEditGetMissionDec.Location = new System.Drawing.Point(510, 115);
            this.m_btnEditGetMissionDec.Name = "m_btnEditGetMissionDec";
            this.m_btnEditGetMissionDec.Size = new System.Drawing.Size(47, 20);
            this.m_btnEditGetMissionDec.TabIndex = 72;
            this.m_btnEditGetMissionDec.Text = "编辑";
            this.m_btnEditGetMissionDec.UseVisualStyleBackColor = true;
            this.m_btnEditGetMissionDec.Click += new System.EventHandler(this.m_btnEditMissionDec_Click);
            // 
            // m_checkBoxItemProvider
            // 
            this.m_checkBoxItemProvider.AutoSize = true;
            this.m_checkBoxItemProvider.Location = new System.Drawing.Point(432, 319);
            this.m_checkBoxItemProvider.Name = "m_checkBoxItemProvider";
            this.m_checkBoxItemProvider.Size = new System.Drawing.Size(96, 16);
            this.m_checkBoxItemProvider.TabIndex = 71;
            this.m_checkBoxItemProvider.Text = "道具获取任务";
            this.m_checkBoxItemProvider.UseVisualStyleBackColor = true;
            this.m_checkBoxItemProvider.CheckedChanged += new System.EventHandler(this.m_checkBoxItemProvider_CheckedChanged);
            // 
            // m_comboBoxTargetMonsterEnd
            // 
            this.m_comboBoxTargetMonsterEnd.Enabled = false;
            this.m_comboBoxTargetMonsterEnd.FormattingEnabled = true;
            this.m_comboBoxTargetMonsterEnd.Location = new System.Drawing.Point(425, 505);
            this.m_comboBoxTargetMonsterEnd.Name = "m_comboBoxTargetMonsterEnd";
            this.m_comboBoxTargetMonsterEnd.Size = new System.Drawing.Size(167, 20);
            this.m_comboBoxTargetMonsterEnd.TabIndex = 70;
            // 
            // m_comboBoxTargetItemEnd
            // 
            this.m_comboBoxTargetItemEnd.Enabled = false;
            this.m_comboBoxTargetItemEnd.FormattingEnabled = true;
            this.m_comboBoxTargetItemEnd.Location = new System.Drawing.Point(105, 560);
            this.m_comboBoxTargetItemEnd.Name = "m_comboBoxTargetItemEnd";
            this.m_comboBoxTargetItemEnd.Size = new System.Drawing.Size(167, 20);
            this.m_comboBoxTargetItemEnd.TabIndex = 69;
            // 
            // m_comboBoxTargetNPCFilter
            // 
            this.m_comboBoxTargetNPCFilter.FormattingEnabled = true;
            this.m_comboBoxTargetNPCFilter.Items.AddRange(new object[] {
            "全部",
            "1",
            "2",
            "3"});
            this.m_comboBoxTargetNPCFilter.Location = new System.Drawing.Point(105, 475);
            this.m_comboBoxTargetNPCFilter.Name = "m_comboBoxTargetNPCFilter";
            this.m_comboBoxTargetNPCFilter.Size = new System.Drawing.Size(42, 20);
            this.m_comboBoxTargetNPCFilter.TabIndex = 68;
            this.m_comboBoxTargetNPCFilter.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxTargetNPCFilter_SelectedIndexChanged);
            // 
            // m_comboBoxSubmitToFilter
            // 
            this.m_comboBoxSubmitToFilter.FormattingEnabled = true;
            this.m_comboBoxSubmitToFilter.Items.AddRange(new object[] {
            "全部",
            "1",
            "2",
            "3"});
            this.m_comboBoxSubmitToFilter.Location = new System.Drawing.Point(395, 345);
            this.m_comboBoxSubmitToFilter.Name = "m_comboBoxSubmitToFilter";
            this.m_comboBoxSubmitToFilter.Size = new System.Drawing.Size(42, 20);
            this.m_comboBoxSubmitToFilter.TabIndex = 67;
            this.m_comboBoxSubmitToFilter.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxSubmitToFilter_SelectedIndexChanged);
            // 
            // m_listBoxSelectedAwardValue
            // 
            this.m_listBoxSelectedAwardValue.FormattingEnabled = true;
            this.m_listBoxSelectedAwardValue.ItemHeight = 12;
            this.m_listBoxSelectedAwardValue.Location = new System.Drawing.Point(692, 382);
            this.m_listBoxSelectedAwardValue.Name = "m_listBoxSelectedAwardValue";
            this.m_listBoxSelectedAwardValue.Size = new System.Drawing.Size(45, 40);
            this.m_listBoxSelectedAwardValue.TabIndex = 63;
            this.m_listBoxSelectedAwardValue.Visible = false;
            // 
            // m_btnSelectedAwardEdit
            // 
            this.m_btnSelectedAwardEdit.Location = new System.Drawing.Point(746, 382);
            this.m_btnSelectedAwardEdit.Name = "m_btnSelectedAwardEdit";
            this.m_btnSelectedAwardEdit.Size = new System.Drawing.Size(47, 20);
            this.m_btnSelectedAwardEdit.TabIndex = 62;
            this.m_btnSelectedAwardEdit.Text = "编辑";
            this.m_btnSelectedAwardEdit.UseVisualStyleBackColor = true;
            this.m_btnSelectedAwardEdit.Visible = false;
            this.m_btnSelectedAwardEdit.Click += new System.EventHandler(this.m_btnSelectedAwardEdit_Click);
            // 
            // m_listBoxSelectedAward
            // 
            this.m_listBoxSelectedAward.FormattingEnabled = true;
            this.m_listBoxSelectedAward.ItemHeight = 12;
            this.m_listBoxSelectedAward.Location = new System.Drawing.Point(570, 382);
            this.m_listBoxSelectedAward.Name = "m_listBoxSelectedAward";
            this.m_listBoxSelectedAward.Size = new System.Drawing.Size(116, 40);
            this.m_listBoxSelectedAward.TabIndex = 61;
            this.m_listBoxSelectedAward.Visible = false;
            this.m_listBoxSelectedAward.SelectedIndexChanged += new System.EventHandler(this.m_listBoxSelectedAward_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(505, 386);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 60;
            this.label6.Text = "可选奖励:";
            this.label6.Visible = false;
            // 
            // m_listBoxFixedAwardValue
            // 
            this.m_listBoxFixedAwardValue.FormattingEnabled = true;
            this.m_listBoxFixedAwardValue.ItemHeight = 12;
            this.m_listBoxFixedAwardValue.Location = new System.Drawing.Point(254, 425);
            this.m_listBoxFixedAwardValue.Name = "m_listBoxFixedAwardValue";
            this.m_listBoxFixedAwardValue.Size = new System.Drawing.Size(446, 40);
            this.m_listBoxFixedAwardValue.TabIndex = 59;
            // 
            // m_listBoxConditionValue
            // 
            this.m_listBoxConditionValue.FormattingEnabled = true;
            this.m_listBoxConditionValue.ItemHeight = 12;
            this.m_listBoxConditionValue.Location = new System.Drawing.Point(254, 375);
            this.m_listBoxConditionValue.Name = "m_listBoxConditionValue";
            this.m_listBoxConditionValue.Size = new System.Drawing.Size(185, 40);
            this.m_listBoxConditionValue.TabIndex = 58;
            // 
            // m_comboBoxTargetMonsterBegin
            // 
            this.m_comboBoxTargetMonsterBegin.Enabled = false;
            this.m_comboBoxTargetMonsterBegin.FormattingEnabled = true;
            this.m_comboBoxTargetMonsterBegin.Location = new System.Drawing.Point(425, 475);
            this.m_comboBoxTargetMonsterBegin.Name = "m_comboBoxTargetMonsterBegin";
            this.m_comboBoxTargetMonsterBegin.Size = new System.Drawing.Size(167, 20);
            this.m_comboBoxTargetMonsterBegin.TabIndex = 48;
            // 
            // m_labelTargetMonster
            // 
            this.m_labelTargetMonster.AutoSize = true;
            this.m_labelTargetMonster.Enabled = false;
            this.m_labelTargetMonster.Location = new System.Drawing.Point(354, 479);
            this.m_labelTargetMonster.Name = "m_labelTargetMonster";
            this.m_labelTargetMonster.Size = new System.Drawing.Size(59, 12);
            this.m_labelTargetMonster.TabIndex = 47;
            this.m_labelTargetMonster.Text = "目标怪物:";
            // 
            // m_comboBoxProviderFilter
            // 
            this.m_comboBoxProviderFilter.FormattingEnabled = true;
            this.m_comboBoxProviderFilter.Items.AddRange(new object[] {
            "全部",
            "1",
            "2",
            "3"});
            this.m_comboBoxProviderFilter.Location = new System.Drawing.Point(105, 345);
            this.m_comboBoxProviderFilter.Name = "m_comboBoxProviderFilter";
            this.m_comboBoxProviderFilter.Size = new System.Drawing.Size(42, 20);
            this.m_comboBoxProviderFilter.TabIndex = 45;
            this.m_comboBoxProviderFilter.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxProviderFilter_SelectedIndexChanged);
            // 
            // m_checkBoxGiveUp
            // 
            this.m_checkBoxGiveUp.AutoSize = true;
            this.m_checkBoxGiveUp.Location = new System.Drawing.Point(404, 625);
            this.m_checkBoxGiveUp.Name = "m_checkBoxGiveUp";
            this.m_checkBoxGiveUp.Size = new System.Drawing.Size(96, 16);
            this.m_checkBoxGiveUp.TabIndex = 43;
            this.m_checkBoxGiveUp.Text = "可放弃的任务";
            this.m_checkBoxGiveUp.UseVisualStyleBackColor = true;
            // 
            // m_comboBoxTargetProp
            // 
            this.m_comboBoxTargetProp.Enabled = false;
            this.m_comboBoxTargetProp.FormattingEnabled = true;
            this.m_comboBoxTargetProp.Location = new System.Drawing.Point(105, 505);
            this.m_comboBoxTargetProp.Name = "m_comboBoxTargetProp";
            this.m_comboBoxTargetProp.Size = new System.Drawing.Size(167, 20);
            this.m_comboBoxTargetProp.TabIndex = 42;
            this.m_comboBoxTargetProp.Visible = false;
            // 
            // m_labelTargetProp
            // 
            this.m_labelTargetProp.AutoSize = true;
            this.m_labelTargetProp.Enabled = false;
            this.m_labelTargetProp.Location = new System.Drawing.Point(30, 509);
            this.m_labelTargetProp.Name = "m_labelTargetProp";
            this.m_labelTargetProp.Size = new System.Drawing.Size(59, 12);
            this.m_labelTargetProp.TabIndex = 41;
            this.m_labelTargetProp.Text = "目标属性:";
            this.m_labelTargetProp.Visible = false;
            // 
            // m_btnNew
            // 
            this.m_btnNew.Location = new System.Drawing.Point(30, 645);
            this.m_btnNew.Name = "m_btnNew";
            this.m_btnNew.Size = new System.Drawing.Size(169, 25);
            this.m_btnNew.TabIndex = 38;
            this.m_btnNew.Text = "新任务";
            this.m_btnNew.UseVisualStyleBackColor = true;
            this.m_btnNew.Click += new System.EventHandler(this.m_btnNew_Click);
            // 
            // m_checkBoxShare
            // 
            this.m_checkBoxShare.AutoSize = true;
            this.m_checkBoxShare.Enabled = false;
            this.m_checkBoxShare.Location = new System.Drawing.Point(218, 625);
            this.m_checkBoxShare.Name = "m_checkBoxShare";
            this.m_checkBoxShare.Size = new System.Drawing.Size(132, 16);
            this.m_checkBoxShare.TabIndex = 37;
            this.m_checkBoxShare.Text = "可多人共享的任务包";
            this.m_checkBoxShare.UseVisualStyleBackColor = true;
            // 
            // m_checkBoxRepeat
            // 
            this.m_checkBoxRepeat.AutoSize = true;
            this.m_checkBoxRepeat.Enabled = false;
            this.m_checkBoxRepeat.Location = new System.Drawing.Point(30, 625);
            this.m_checkBoxRepeat.Name = "m_checkBoxRepeat";
            this.m_checkBoxRepeat.Size = new System.Drawing.Size(132, 16);
            this.m_checkBoxRepeat.TabIndex = 36;
            this.m_checkBoxRepeat.Text = "可重复完成的任务包";
            this.m_checkBoxRepeat.UseVisualStyleBackColor = true;
            // 
            // m_labelTargetArea
            // 
            this.m_labelTargetArea.AutoSize = true;
            this.m_labelTargetArea.Enabled = false;
            this.m_labelTargetArea.Location = new System.Drawing.Point(354, 539);
            this.m_labelTargetArea.Name = "m_labelTargetArea";
            this.m_labelTargetArea.Size = new System.Drawing.Size(59, 12);
            this.m_labelTargetArea.TabIndex = 31;
            this.m_labelTargetArea.Text = "目标区域:";
            // 
            // m_comboBoxSubType
            // 
            this.m_comboBoxSubType.Enabled = false;
            this.m_comboBoxSubType.FormattingEnabled = true;
            this.m_comboBoxSubType.Location = new System.Drawing.Point(278, 255);
            this.m_comboBoxSubType.Name = "m_comboBoxSubType";
            this.m_comboBoxSubType.Size = new System.Drawing.Size(90, 20);
            this.m_comboBoxSubType.TabIndex = 30;
            this.m_comboBoxSubType.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxDlgMissionType_SelectedIndexChanged);
            // 
            // m_comboBoxTargetItemBegin
            // 
            this.m_comboBoxTargetItemBegin.Enabled = false;
            this.m_comboBoxTargetItemBegin.FormattingEnabled = true;
            this.m_comboBoxTargetItemBegin.Location = new System.Drawing.Point(105, 535);
            this.m_comboBoxTargetItemBegin.Name = "m_comboBoxTargetItemBegin";
            this.m_comboBoxTargetItemBegin.Size = new System.Drawing.Size(167, 20);
            this.m_comboBoxTargetItemBegin.TabIndex = 28;
            // 
            // m_labelTargetItem
            // 
            this.m_labelTargetItem.AutoSize = true;
            this.m_labelTargetItem.Enabled = false;
            this.m_labelTargetItem.Location = new System.Drawing.Point(30, 539);
            this.m_labelTargetItem.Name = "m_labelTargetItem";
            this.m_labelTargetItem.Size = new System.Drawing.Size(59, 12);
            this.m_labelTargetItem.TabIndex = 27;
            this.m_labelTargetItem.Text = "目标道具:";
            // 
            // m_textBoxTargetValue
            // 
            this.m_textBoxTargetValue.Enabled = false;
            this.m_textBoxTargetValue.Location = new System.Drawing.Point(278, 590);
            this.m_textBoxTargetValue.Name = "m_textBoxTargetValue";
            this.m_textBoxTargetValue.Size = new System.Drawing.Size(167, 21);
            this.m_textBoxTargetValue.TabIndex = 26;
            this.m_textBoxTargetValue.Text = "1";
            // 
            // m_btnSave
            // 
            this.m_btnSave.Location = new System.Drawing.Point(218, 645);
            this.m_btnSave.Name = "m_btnSave";
            this.m_btnSave.Size = new System.Drawing.Size(169, 25);
            this.m_btnSave.TabIndex = 25;
            this.m_btnSave.Text = "保存";
            this.m_btnSave.UseVisualStyleBackColor = true;
            this.m_btnSave.Click += new System.EventHandler(this.m_btnSave_Click);
            // 
            // m_labelTargetValue
            // 
            this.m_labelTargetValue.AutoSize = true;
            this.m_labelTargetValue.Enabled = false;
            this.m_labelTargetValue.Location = new System.Drawing.Point(225, 594);
            this.m_labelTargetValue.Name = "m_labelTargetValue";
            this.m_labelTargetValue.Size = new System.Drawing.Size(47, 12);
            this.m_labelTargetValue.TabIndex = 24;
            this.m_labelTargetValue.Text = "目标值:";
            // 
            // m_comboBoxTargetNPC
            // 
            this.m_comboBoxTargetNPC.Enabled = false;
            this.m_comboBoxTargetNPC.FormattingEnabled = true;
            this.m_comboBoxTargetNPC.Location = new System.Drawing.Point(155, 475);
            this.m_comboBoxTargetNPC.Name = "m_comboBoxTargetNPC";
            this.m_comboBoxTargetNPC.Size = new System.Drawing.Size(144, 20);
            this.m_comboBoxTargetNPC.TabIndex = 23;
            // 
            // m_labelTargetNPC
            // 
            this.m_labelTargetNPC.AutoSize = true;
            this.m_labelTargetNPC.Enabled = false;
            this.m_labelTargetNPC.Location = new System.Drawing.Point(30, 479);
            this.m_labelTargetNPC.Name = "m_labelTargetNPC";
            this.m_labelTargetNPC.Size = new System.Drawing.Size(59, 12);
            this.m_labelTargetNPC.TabIndex = 22;
            this.m_labelTargetNPC.Text = "目标 NPC:";
            // 
            // m_btnFixedAwardEdit
            // 
            this.m_btnFixedAwardEdit.Location = new System.Drawing.Point(706, 425);
            this.m_btnFixedAwardEdit.Name = "m_btnFixedAwardEdit";
            this.m_btnFixedAwardEdit.Size = new System.Drawing.Size(47, 20);
            this.m_btnFixedAwardEdit.TabIndex = 20;
            this.m_btnFixedAwardEdit.Text = "编辑";
            this.m_btnFixedAwardEdit.UseVisualStyleBackColor = true;
            this.m_btnFixedAwardEdit.Click += new System.EventHandler(this.m_btnFixedAwardEdit_Click);
            // 
            // m_btnConvertToStateDiagrom
            // 
            this.m_btnConvertToStateDiagrom.Location = new System.Drawing.Point(423, 645);
            this.m_btnConvertToStateDiagrom.Name = "m_btnConvertToStateDiagrom";
            this.m_btnConvertToStateDiagrom.Size = new System.Drawing.Size(169, 25);
            this.m_btnConvertToStateDiagrom.TabIndex = 19;
            this.m_btnConvertToStateDiagrom.Text = "转换到 NPC 状态图";
            this.m_btnConvertToStateDiagrom.UseVisualStyleBackColor = true;
            this.m_btnConvertToStateDiagrom.Click += new System.EventHandler(this.m_btnConvertToStateDiagrom_Click);
            // 
            // m_btnConditionEdit
            // 
            this.m_btnConditionEdit.Location = new System.Drawing.Point(453, 375);
            this.m_btnConditionEdit.Name = "m_btnConditionEdit";
            this.m_btnConditionEdit.Size = new System.Drawing.Size(47, 20);
            this.m_btnConditionEdit.TabIndex = 17;
            this.m_btnConditionEdit.Text = "编辑";
            this.m_btnConditionEdit.UseVisualStyleBackColor = true;
            this.m_btnConditionEdit.Click += new System.EventHandler(this.m_btnRequirementEdit_Click);
            // 
            // m_listBoxFixedAward
            // 
            this.m_listBoxFixedAward.FormattingEnabled = true;
            this.m_listBoxFixedAward.ItemHeight = 12;
            this.m_listBoxFixedAward.Location = new System.Drawing.Point(105, 425);
            this.m_listBoxFixedAward.Name = "m_listBoxFixedAward";
            this.m_listBoxFixedAward.Size = new System.Drawing.Size(116, 40);
            this.m_listBoxFixedAward.TabIndex = 16;
            this.m_listBoxFixedAward.SelectedIndexChanged += new System.EventHandler(this.m_listBoxFixedAward_SelectedIndexChanged);
            // 
            // m_listBoxCondition
            // 
            this.m_listBoxCondition.FormattingEnabled = true;
            this.m_listBoxCondition.ItemHeight = 12;
            this.m_listBoxCondition.Location = new System.Drawing.Point(105, 375);
            this.m_listBoxCondition.Name = "m_listBoxCondition";
            this.m_listBoxCondition.Size = new System.Drawing.Size(116, 40);
            this.m_listBoxCondition.TabIndex = 15;
            this.m_listBoxCondition.SelectedIndexChanged += new System.EventHandler(this.m_listBoxCondition_SelectedIndexChanged);
            // 
            // m_comboBoxSubmitTo
            // 
            this.m_comboBoxSubmitTo.FormattingEnabled = true;
            this.m_comboBoxSubmitTo.Location = new System.Drawing.Point(445, 345);
            this.m_comboBoxSubmitTo.Name = "m_comboBoxSubmitTo";
            this.m_comboBoxSubmitTo.Size = new System.Drawing.Size(137, 20);
            this.m_comboBoxSubmitTo.TabIndex = 13;
            // 
            // m_comboBoxProvider
            // 
            this.m_comboBoxProvider.FormattingEnabled = true;
            this.m_comboBoxProvider.Location = new System.Drawing.Point(155, 345);
            this.m_comboBoxProvider.Name = "m_comboBoxProvider";
            this.m_comboBoxProvider.Size = new System.Drawing.Size(144, 20);
            this.m_comboBoxProvider.TabIndex = 12;
            // 
            // m_comboBoxType
            // 
            this.m_comboBoxType.FormattingEnabled = true;
            this.m_comboBoxType.Location = new System.Drawing.Point(105, 255);
            this.m_comboBoxType.Name = "m_comboBoxType";
            this.m_comboBoxType.Size = new System.Drawing.Size(167, 20);
            this.m_comboBoxType.TabIndex = 11;
            this.m_comboBoxType.SelectedIndexChanged += new System.EventHandler(this.m_comboBoxType_SelectedIndexChanged);
            // 
            // m_textBoxName
            // 
            this.m_textBoxName.Location = new System.Drawing.Point(280, 15);
            this.m_textBoxName.Name = "m_textBoxName";
            this.m_textBoxName.Size = new System.Drawing.Size(250, 21);
            this.m_textBoxName.TabIndex = 9;
            // 
            // m_textBoxID
            // 
            this.m_textBoxID.Location = new System.Drawing.Point(105, 15);
            this.m_textBoxID.Name = "m_textBoxID";
            this.m_textBoxID.Size = new System.Drawing.Size(130, 21);
            this.m_textBoxID.TabIndex = 8;
            // 
            // m_labelAward
            // 
            this.m_labelAward.AutoSize = true;
            this.m_labelAward.Location = new System.Drawing.Point(30, 429);
            this.m_labelAward.Name = "m_labelAward";
            this.m_labelAward.Size = new System.Drawing.Size(35, 12);
            this.m_labelAward.TabIndex = 7;
            this.m_labelAward.Text = "奖励:";
            // 
            // m_labelSubmitTo
            // 
            this.m_labelSubmitTo.AutoSize = true;
            this.m_labelSubmitTo.Location = new System.Drawing.Point(320, 349);
            this.m_labelSubmitTo.Name = "m_labelSubmitTo";
            this.m_labelSubmitTo.Size = new System.Drawing.Size(71, 12);
            this.m_labelSubmitTo.TabIndex = 6;
            this.m_labelSubmitTo.Text = "交任务对象:";
            // 
            // m_labelProvider
            // 
            this.m_labelProvider.AutoSize = true;
            this.m_labelProvider.Location = new System.Drawing.Point(30, 349);
            this.m_labelProvider.Name = "m_labelProvider";
            this.m_labelProvider.Size = new System.Drawing.Size(71, 12);
            this.m_labelProvider.TabIndex = 5;
            this.m_labelProvider.Text = "任务提供者:";
            // 
            // m_labelCondition
            // 
            this.m_labelCondition.AutoSize = true;
            this.m_labelCondition.Location = new System.Drawing.Point(30, 379);
            this.m_labelCondition.Name = "m_labelCondition";
            this.m_labelCondition.Size = new System.Drawing.Size(59, 12);
            this.m_labelCondition.TabIndex = 4;
            this.m_labelCondition.Text = "领取条件:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 259);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "类型:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "接任务描述:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(240, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "名称:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID:";
            // 
            // m_timerSceneRefresh
            // 
            this.m_timerSceneRefresh.Interval = 300;
            this.m_timerSceneRefresh.Tick += new System.EventHandler(this.m_timerSceneRefresh_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1016, 734);
            this.Controls.Add(this.m_tabControl);
            this.Controls.Add(this.m_MainMenu);
            this.MainMenuStrip = this.m_MainMenu;
            this.Name = "MainForm";
            this.Text = "任务编辑器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.m_MainMenu.ResumeLayout(false);
            this.m_MainMenu.PerformLayout();
            this.m_tabControl.ResumeLayout(false);
            this.m_tabPageState.ResumeLayout(false);
            this.m_splitContainerState.Panel1.ResumeLayout(false);
            this.m_splitContainerState.ResumeLayout(false);
            this.m_menuStateDiagram.ResumeLayout(false);
            this.m_tabPageSkeneScetch.ResumeLayout(false);
            this.m_splitContainerSceneSketch.Panel1.ResumeLayout(false);
            this.m_splitContainerSceneSketch.Panel2.ResumeLayout(false);
            this.m_splitContainerSceneSketch.Panel2.PerformLayout();
            this.m_splitContainerSceneSketch.ResumeLayout(false);
            this.m_splitContainerSceneScketchLeft.Panel1.ResumeLayout(false);
            this.m_splitContainerSceneScketchLeft.Panel2.ResumeLayout(false);
            this.m_splitContainerSceneScketchLeft.ResumeLayout(false);
            this.m_statusStripScene.ResumeLayout(false);
            this.m_statusStripScene.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_pictureBoxSceneSketch)).EndInit();
            this.m_tabPageMission.ResumeLayout(false);
            this.m_splitContainerMission.Panel1.ResumeLayout(false);
            this.m_splitContainerMission.Panel2.ResumeLayout(false);
            this.m_splitContainerMission.Panel2.PerformLayout();
            this.m_splitContainerMission.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel m_CanvasForm = null;
        private System.Windows.Forms.MenuStrip m_MainMenu;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 编辑ToolStripMenuItem;
        private System.Windows.Forms.TabControl m_tabControl;
        private System.Windows.Forms.TabPage m_tabPageState;
        private System.Windows.Forms.TabPage m_tabPageSkeneScetch;
        private System.Windows.Forms.ToolStripMenuItem sceneToolStripMenuItem;
        private System.Windows.Forms.SplitContainer m_splitContainerState;
        private System.Windows.Forms.TabPage m_tabPageMission;
        private System.Windows.Forms.SplitContainer m_splitContainerMission;
        private System.Windows.Forms.ListBox m_listBoxMO;
        private System.Windows.Forms.ComboBox m_MOFilter;
        private System.Windows.Forms.ToolStripMenuItem 行为ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 原子操作ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip m_menuStateDiagram;
        private System.Windows.Forms.ToolStripMenuItem 添加状态ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加连接ToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label m_labelAward;
        private System.Windows.Forms.Label m_labelSubmitTo;
        private System.Windows.Forms.Label m_labelProvider;
        private System.Windows.Forms.Label m_labelCondition;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox m_textBoxName;
        private System.Windows.Forms.TextBox m_textBoxID;
        private System.Windows.Forms.ComboBox m_comboBoxSubmitTo;
        private System.Windows.Forms.ComboBox m_comboBoxProvider;
        private System.Windows.Forms.ComboBox m_comboBoxType;
        private System.Windows.Forms.ListBox m_listBoxFixedAward;
        private System.Windows.Forms.ListBox m_listBoxCondition;
        private System.Windows.Forms.Button m_btnConditionEdit;
        private System.Windows.Forms.Button m_btnFixedAwardEdit;
        private System.Windows.Forms.Button m_btnConvertToStateDiagrom;

        private System.Windows.Forms.ComboBox m_comboBoxTargetItemBegin;
        private System.Windows.Forms.Label m_labelTargetItem;
        private System.Windows.Forms.TextBox m_textBoxTargetValue;
        private System.Windows.Forms.Button m_btnSave;
        private System.Windows.Forms.Label m_labelTargetValue;
        private System.Windows.Forms.ComboBox m_comboBoxTargetNPC;
        private System.Windows.Forms.Label m_labelTargetNPC;
        private System.Windows.Forms.Label m_labelTargetArea;
        private System.Windows.Forms.ComboBox m_comboBoxSubType;
        private System.Windows.Forms.CheckBox m_checkBoxShare;
        private System.Windows.Forms.CheckBox m_checkBoxRepeat;
        private System.Windows.Forms.CheckBox m_checkBoxGiveUp;
        private System.Windows.Forms.Button m_btnNew;
        private System.Windows.Forms.ComboBox m_comboBoxTargetProp;
        private System.Windows.Forms.Label m_labelTargetProp;
        private System.Windows.Forms.TreeView m_treeViewMission;
        private System.Windows.Forms.ToolStripMenuItem 选项ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存任务ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存状态图ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 新建状态图ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ComboBox m_comboBoxProviderFilter;
        private System.Windows.Forms.ToolStripMenuItem 刷新状态图ToolStripMenuItem;
        private System.Windows.Forms.ComboBox m_comboBoxTargetMonsterBegin;
        private System.Windows.Forms.Label m_labelTargetMonster;
        private System.Windows.Forms.ListBox m_listBoxFixedAwardValue;
        private System.Windows.Forms.ListBox m_listBoxConditionValue;
        private System.Windows.Forms.ListBox m_listBoxSelectedAwardValue;
        private System.Windows.Forms.Button m_btnSelectedAwardEdit;
        private System.Windows.Forms.ListBox m_listBoxSelectedAward;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox m_comboBoxSubmitToFilter;
        private System.Windows.Forms.ComboBox m_comboBoxTargetNPCFilter; // MainForm 唯一实例
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemConvertDiaToLua;
        private System.Windows.Forms.ListBox m_listBoxSceneID;
        private System.Windows.Forms.SplitContainer m_splitContainerSceneSketch;
        private System.Windows.Forms.SplitContainer m_splitContainerSceneScketchLeft;
        private System.Windows.Forms.PropertyGrid m_propertyGrid;
        private System.Windows.Forms.PictureBox m_pictureBoxSceneSketch;
        private System.Windows.Forms.ComboBox m_comboBoxTargetMonsterEnd;
        private System.Windows.Forms.ComboBox m_comboBoxTargetItemEnd;
        private System.Windows.Forms.CheckBox m_checkBoxItemProvider;

        // 程序相关属性
        private bool m_bErrorClose = false;     // 程序是否发生致命error

        // 任务编辑相关属性
        private bool m_bMissionChanged = false; // 当前任务界面是否有改动

        // 状态图相关属性
        //private bool m_bStateDiagramChanged = false; // 当前状态图是否有改动
        private int m_iCurrStateID = 0; // 状态编号，每次新建 state diagram 后恢复为0。载入 diagram 后设置为该 diagram 中 state id 最大值 +1
        private int m_iCurrTranslationID = 0;
        private String m_strStateDiagramName = null; // 状态图名称
        private System.Drawing.Point m_MouseDownPosition;  // 鼠标按下时的位置
        //private System.Windows.Forms.Control m_SelectedCtrl;    // 当前状态图中被选择的 State/Translation 控件 功能放入 StateDiagramOperationManager
        // Managers
        private MissionManager m_MissionManager = new MissionManager();
        private StateDiagramManager m_StateDiagramManager = StateDiagramManager.Instance;
        private NPCManager m_NPCManager = NPCManager.Instance;
        private RegionManager m_RegionManager = RegionManager.Instance;
        private PathManager m_PathManager = PathManager.Instance;
        private ItemManager m_ItemManager = ItemManager.Instance;
        private Mission m_currMission = null;       // 当前正被编辑的 Mission
        //private StateDiagram m_currStateDiagram = null; // 当前正被编辑的 状态图
        private NPCCreatorControl m_SelectedNPCCretorCtl = null;    // 当前选择的 NPC creator ctrl
        private RegionControl m_SelectedRegionCtrl = null;          // 当前选中的区域
        private float m_fOgSize = 1024.0f;
        private float m_fScale = 1.0f;
        public bool m_bShowGrid = false;    // 是否显示网格
        public bool m_bShowNPC = false;      // 是否显示NPC
        public bool m_bShowRegion = false;   // 是否显示区域
        public bool m_bShowPtSound = false;  // 是否显示点音源
        public int m_iGridSize = 2;         // 网格大小
        public bool m_bSceneChanged = false;    // 场景草图是否被修改过
        private bool m_bNpcHighLight = false;    // 是否高亮显示NPC

        public Rectangle m_oldCur = new Rectangle();    // 保存上一帧鼠标渲染的位置，便于更新，用于刷路径

        public bool m_bNewNPCState = false; // 鼠标处于新建NPC状态

        // 新任务编辑UI
        TaskUIForm m_TaskUIForm = null;

        private static MainForm ms_Instance = null;
        private System.Windows.Forms.ToolStripMenuItem 添加NPCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 生成器ToolStripMenuItem;
        private System.Windows.Forms.Button m_btnEditGetMissionDec;
        private System.Windows.Forms.WebBrowser m_webBrowserGetMissionDesc;
        private System.Windows.Forms.ToolStripMenuItem 任务脚本测试ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加区域ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 区域管理工具ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemMission;
        private System.Windows.Forms.ToolStripMenuItem 转换到状态图ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 批量转换到状态图ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemStateDia;
        private System.Windows.Forms.ToolStripMenuItem 转换到脚本ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 批量转换到脚本ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 商店编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出当前地图的路径表ToolStripMenuItem;
        private System.Windows.Forms.TextBox m_textBoxLevel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox m_comboBoxClass;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem m_ToolStripMenuItemMissionConditionLua;
        private System.Windows.Forms.Button m_btnClearTargetNpc;
        private System.Windows.Forms.Button m_btnClearTargetMonster;
        private System.Windows.Forms.Button m_btnClearTargetItem;
        private System.Windows.Forms.Button m_btnClearTargetProp;
        private System.Windows.Forms.TextBox m_textBoxReserved;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox m_textBoxTrackName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button m_btnClearTargetValue;
        private System.Windows.Forms.WebBrowser m_webBrowserSubmitMissionDesc;
        private System.Windows.Forms.Button m_btnEditSubmitMissionDec;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemCopy;
        private System.Windows.Forms.ToolStripMenuItem m_toolStripMenuItemPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 导出地图格子信息ToolStripMenuItem;
        private System.Windows.Forms.Button m_btnExternalLoad;
        private System.Windows.Forms.ComboBox m_comboBoxLeadMap;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox m_comboBoxStarLevel;
        private System.Windows.Forms.TextBox m_textBoxTimeLimit;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripMenuItem 路径编辑ToolStripMenuItem;
        private System.Windows.Forms.ComboBox m_comboBoxReservedFilter;
        private System.Windows.Forms.ComboBox m_comboBoxFlowType;
        private System.Windows.Forms.CheckBox m_checkBoxRegionProvider;
        private System.Windows.Forms.ComboBox m_comboBoxTargetArea;
        private System.Windows.Forms.Button m_btnClearTargetArea;
        private System.Windows.Forms.ToolStripMenuItem 客户端文件导出ToolStripMenuItem;
        private System.Windows.Forms.Button m_btnEditSimpleMissionDec;
        private System.Windows.Forms.WebBrowser m_webBrowserSimpleMissionDesc;
        private System.Windows.Forms.Timer m_timerSceneRefresh;
        private System.Windows.Forms.ToolStripComboBox m_toolStripComboBoxSceneScale;
        private System.Windows.Forms.ToolStripMenuItem 启用加速模式ToolStripMenuItem;
        private System.Windows.Forms.TabPage m_tabPageTaskUI;
        private System.Windows.Forms.ToolStripMenuItem 工具ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 数据编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 任务奖励编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 任务条件编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 重新ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 重新加载点音源信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 报告ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出任务报告ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip m_statusStripScene;
        private System.Windows.Forms.ToolStripStatusLabel m_toolStripStatusLabelNpcShow;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel m_toolStripStatusLabelNpcNum;
        private System.Windows.Forms.ToolStripStatusLabel m_toolStripStatusLabelTitle;
        private System.Windows.Forms.ToolStripMenuItem NPC统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel m_toolStripStatusLabelSubNpcShow1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel m_toolStripStatusLabelSubNpcShow2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel m_toolStripStatusLabelSubNpcShow3;
        private System.Windows.Forms.ToolStripSplitButton m_toolStripSplitButtonNpcHighLight;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem 导出任务存盘栏位规则ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 版本管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 获取最新版本ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem 签出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 签入ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导入新状态图ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 撤销更改ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem 打开所在文件夹ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 获取最新任务数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 获取最新状态图数据ToolStripMenuItem;





    }
}

