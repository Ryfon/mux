﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Collections;

namespace launch
{
    public partial class MainForm : Form
    {
        //////////////////////////////////////////////////////////////////////////
        private String m_strUpdateInfoFile = "UpdateInfo.xml";  // 包含更新信息的配置文件
        private static MainForm ms_Instance = null;             // 全局是实例
        //////////////////////////////////////////////////////////////////////////

        public MainForm()
        {
            InitializeComponent();
            ms_Instance = this;

            // 初始化ListView
            // 添加列名
            m_listViewLog.Columns.Add("", 25, HorizontalAlignment.Center);
            //m_listViewLog.Columns.Add("服务器文件", 100, HorizontalAlignment.Center);
            m_listViewLog.Columns.Add("文件名", 200, HorizontalAlignment.Left);
            //m_listViewLog.Columns.Add("更新后文件", 100, HorizontalAlignment.Center);
            //m_listViewLog.Columns.Add("删除本地", 63, HorizontalAlignment.Center);
            m_listViewLog.Columns.Add("状态", 500, HorizontalAlignment.Left);
        }

        static public MainForm Instance
        {
            get
            {
                return ms_Instance;
            }
        }
 
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            // 链接服务器
            m_labelState.Text = "链接文件服务器...";
            Update();
            Thread.Sleep(1000);
            int iUpdateNum = UpdateManager.Instance.ConnectServer();
            if(iUpdateNum == -1)
            {
                m_labelState.Text = "链接文件服务器失败！";
                return; 
            }
            else if(iUpdateNum == 0)
            {
                m_labelState.Text = "链接文件服务器成功,没有需要的更新！";
                return;
            }
            else
            {
                m_labelState.Text = "链接文件服务器成功,开始下载更新！";
            }
            
            // 更新UI
            m_progressBar.Maximum = iUpdateNum;
            m_progressBar.Minimum = 0;
            UpdateUI(0, "");

            // 获取用户最新设置
            GetUserSettings();

            // 开始更新
            int iErrorNum = UpdateManager.Instance.UpdateFile();
 
            // 显示错误日志
            UpdateErrorLog();
            if(iErrorNum > 0)
            {
                m_labelState.Text = "更新完毕！" + iErrorNum.ToString() + "文件存在错误！";
            }
            else
            {
                m_labelState.Text = "更新完毕！请启动程序。";
            }
        }

        public void UpdateUI(int iCurrentProgress, String strCurrentState)
        {
            m_progressBar.Value = iCurrentProgress;

            m_labelProgress.Text = "(" + m_progressBar.Value + "/" + m_progressBar.Maximum + ")";

            m_labelState.Text = strCurrentState;

            Update();
        }

        private void UpdateErrorLog()
        {
            //m_listBoxErrorLog.Items.Clear();

            //ArrayList errorList = UpdateManager.Instance.ErrorList;

            //foreach (String errorInfo in errorList)
            //{
            //    m_listBoxErrorLog.Items.Add(errorInfo);    
            //}
            
            // 清空旧的数据
            m_listViewLog.Items.Clear();

            // 获取更新文件列表
            ArrayList statusList = UpdateManager.Instance.UpdateList;
            
            // 遍历写入更新文件信息
            foreach(UpdateInfo info in statusList)
            {
                ListViewItem lv = new ListViewItem();
                lv.SubItems.Clear();
                lv.Checked = info.NeedUpdate;
                lv.SubItems.Add(info.LocalFile);
                lv.SubItems.Add(info.Status);
                m_listViewLog.Items.Add(lv);
            }

            // 更新
            m_checkBoxAllCheck.CheckState = CheckState.Indeterminate;
            Update();
        }

        // 获取用户最新的设置
        private void GetUserSettings()
        {
            // 获取更新文件列表
            ArrayList statusList = UpdateManager.Instance.UpdateList;

            // 设置最新的设置
            int iIndex = 0;
            foreach(UpdateInfo info in statusList)
            {
                info.NeedUpdate = m_listViewLog.Items[iIndex].Checked;
                iIndex++;
            }
        }

        private void m_buttonStart_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(UpdateManager.Instance.FileName);
            startInfo.Arguments = UpdateManager.Instance.Arguments;

            try
            {
                Process.Start(startInfo);
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message + UpdateManager.Instance.FileName + "无法启动！");
            }

            Application.Exit();
        }

        private void m_buttonCheck_Click(object sender, EventArgs e)
        {
            // 获取更新信息
            m_labelState.Text = "获取更新信息...";
            Update();
            Thread.Sleep(1000);
            if (UpdateManager.Instance.LoadUpdateInfo(m_strUpdateInfoFile))
            {
                m_labelState.Text = "获取更新信息成功!";
                m_buttonUpdate.Enabled = true;
            }
            else
            {
                m_labelState.Text = "获取更新信息失败!";
                m_buttonUpdate.Enabled = false;
                return;
            }

            // 链接服务器
            m_labelState.Text = "链接更新服务器...";
            Update();
            Thread.Sleep(1000);
            int iUpdateNum = UpdateManager.Instance.ConnectServer();
            if (iUpdateNum == -1)
            {
                m_labelState.Text = "链接更新服务器失败！";
                return;
            }
            else if (iUpdateNum == 0)
            {
                m_labelState.Text = "链接更新服务器成功,没有需要的更新！";
                return;
            }
            else
            {
                m_labelState.Text = "链接更新服务器成功！";
            }

            // 更新检查
            m_labelState.Text = "检查文件更新状态...";
            Update();
            Thread.Sleep(1000);
            UpdateManager.Instance.CheckUpdate();
            m_labelState.Text = "检查文件更新状态完毕！";
            Update();

            // 更新UI
            m_progressBar.Maximum = iUpdateNum;
            m_progressBar.Minimum = 0;
            UpdateUI(0, "准备更新");

            // 更新文件状态列表
            UpdateErrorLog();
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e)
        {
            if(m_checkBoxAllCheck.CheckState == CheckState.Checked)
            {
                foreach(ListViewItem lv in m_listViewLog.Items)
                {
                    lv.Checked = true;
                }
            }

            if (m_checkBoxAllCheck.CheckState == CheckState.Unchecked)
            {
                foreach (ListViewItem lv in m_listViewLog.Items)
                {
                    lv.Checked = false;
                }
            }

            if (m_checkBoxAllCheck.CheckState == CheckState.Indeterminate)
            {
                UpdateErrorLog();
            }
        }
        
    }
}