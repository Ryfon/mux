﻿namespace launch
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.m_buttonUpdate = new System.Windows.Forms.Button();
            this.m_progressBar = new System.Windows.Forms.ProgressBar();
            this.m_buttonStart = new System.Windows.Forms.Button();
            this.m_labelProgress = new System.Windows.Forms.Label();
            this.m_labelState = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_listViewLog = new System.Windows.Forms.ListView();
            this.m_buttonCheck = new System.Windows.Forms.Button();
            this.m_checkBoxAllCheck = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // m_buttonUpdate
            // 
            this.m_buttonUpdate.Enabled = false;
            this.m_buttonUpdate.Location = new System.Drawing.Point(629, 431);
            this.m_buttonUpdate.Name = "m_buttonUpdate";
            this.m_buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.m_buttonUpdate.TabIndex = 0;
            this.m_buttonUpdate.Text = "更新";
            this.m_buttonUpdate.UseVisualStyleBackColor = true;
            this.m_buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // m_progressBar
            // 
            this.m_progressBar.Location = new System.Drawing.Point(15, 25);
            this.m_progressBar.Name = "m_progressBar";
            this.m_progressBar.Size = new System.Drawing.Size(770, 23);
            this.m_progressBar.TabIndex = 1;
            // 
            // m_buttonStart
            // 
            this.m_buttonStart.Location = new System.Drawing.Point(710, 431);
            this.m_buttonStart.Name = "m_buttonStart";
            this.m_buttonStart.Size = new System.Drawing.Size(75, 23);
            this.m_buttonStart.TabIndex = 2;
            this.m_buttonStart.Text = "启动";
            this.m_buttonStart.UseVisualStyleBackColor = true;
            this.m_buttonStart.Click += new System.EventHandler(this.m_buttonStart_Click);
            // 
            // m_labelProgress
            // 
            this.m_labelProgress.AutoSize = true;
            this.m_labelProgress.Location = new System.Drawing.Point(75, 10);
            this.m_labelProgress.Name = "m_labelProgress";
            this.m_labelProgress.Size = new System.Drawing.Size(35, 12);
            this.m_labelProgress.TabIndex = 3;
            this.m_labelProgress.Text = "(0/0)";
            // 
            // m_labelState
            // 
            this.m_labelState.AutoSize = true;
            this.m_labelState.Location = new System.Drawing.Point(75, 55);
            this.m_labelState.Name = "m_labelState";
            this.m_labelState.Size = new System.Drawing.Size(77, 12);
            this.m_labelState.TabIndex = 4;
            this.m_labelState.Text = "等待用户操作";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "当前状态:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "总体进度:";
            // 
            // m_listViewLog
            // 
            this.m_listViewLog.CheckBoxes = true;
            this.m_listViewLog.FullRowSelect = true;
            this.m_listViewLog.GridLines = true;
            this.m_listViewLog.Location = new System.Drawing.Point(15, 77);
            this.m_listViewLog.Name = "m_listViewLog";
            this.m_listViewLog.Size = new System.Drawing.Size(770, 348);
            this.m_listViewLog.TabIndex = 7;
            this.m_listViewLog.UseCompatibleStateImageBehavior = false;
            this.m_listViewLog.View = System.Windows.Forms.View.Details;
            // 
            // m_buttonCheck
            // 
            this.m_buttonCheck.Location = new System.Drawing.Point(15, 431);
            this.m_buttonCheck.Name = "m_buttonCheck";
            this.m_buttonCheck.Size = new System.Drawing.Size(75, 23);
            this.m_buttonCheck.TabIndex = 8;
            this.m_buttonCheck.Text = "检查更新";
            this.m_buttonCheck.UseVisualStyleBackColor = true;
            this.m_buttonCheck.Click += new System.EventHandler(this.m_buttonCheck_Click);
            // 
            // m_checkBoxAllCheck
            // 
            this.m_checkBoxAllCheck.AutoSize = true;
            this.m_checkBoxAllCheck.Checked = true;
            this.m_checkBoxAllCheck.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.m_checkBoxAllCheck.Location = new System.Drawing.Point(695, 55);
            this.m_checkBoxAllCheck.Name = "m_checkBoxAllCheck";
            this.m_checkBoxAllCheck.Size = new System.Drawing.Size(90, 16);
            this.m_checkBoxAllCheck.TabIndex = 9;
            this.m_checkBoxAllCheck.Text = "全选/全不选";
            this.m_checkBoxAllCheck.ThreeState = true;
            this.m_checkBoxAllCheck.UseVisualStyleBackColor = true;
            this.m_checkBoxAllCheck.CheckStateChanged += new System.EventHandler(this.checkBox1_CheckStateChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 466);
            this.Controls.Add(this.m_checkBoxAllCheck);
            this.Controls.Add(this.m_buttonCheck);
            this.Controls.Add(this.m_listViewLog);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_labelState);
            this.Controls.Add(this.m_labelProgress);
            this.Controls.Add(this.m_buttonStart);
            this.Controls.Add(this.m_progressBar);
            this.Controls.Add(this.m_buttonUpdate);
            this.Name = "MainForm";
            this.Text = "launch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_buttonUpdate;
        private System.Windows.Forms.ProgressBar m_progressBar;
        private System.Windows.Forms.Button m_buttonStart;
        private System.Windows.Forms.Label m_labelProgress;
        private System.Windows.Forms.Label m_labelState;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView m_listViewLog;
        private System.Windows.Forms.Button m_buttonCheck;
        private System.Windows.Forms.CheckBox m_checkBoxAllCheck;
    }
}

