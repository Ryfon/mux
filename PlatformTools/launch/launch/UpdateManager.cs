﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using System.Windows.Forms;
using System.IO;

namespace launch
{
    class UpdateManager
    {
        // 私有的构造函数，用来时间singleton模式
        private UpdateManager()
        {

        }

        // 外部只能通过这个接口来使用UpdateManager
        public static UpdateManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new UpdateManager();
                }
                return ms_Instance;
            }
        }

        public String FileName
        {
            get
            {
                return m_strFileName;
            }
        }

        public String Arguments
        {
            get
            {
                return m_strArguments;
            }
        }

        public ArrayList UpdateList
        {
            get
            {
                return m_UpdateList;
            }
        }
        //---------------------------------------------------------------------------------------
        // 读取更新文件信息
        public bool LoadUpdateInfo(String strXmlFileName)
        {
            // 清空数据列表
            m_UpdateList.Clear();

            // 读取文件
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(strXmlFileName);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            
            //////////////////////////////////////////////////////////////////////////
            // 获取根节点的信息
            XmlElement elmtRoot = xmlDoc.LastChild as XmlElement;
            if(elmtRoot == null)
            {
                return false;
            }

            if (elmtRoot.Name.CompareTo("UpdateList") != 0)
            {
                return false;
            }

            // 获取服务器路径
            if (elmtRoot.HasAttribute("ServerPath"))
            {
                m_strServerPath = elmtRoot.GetAttribute("ServerPath");
            }
            else
            {
                return false;
            }

            // 获取本地文件夹路径
            if (elmtRoot.HasAttribute("LocalPath"))
            {
                m_strLocalPath = elmtRoot.GetAttribute("LocalPath");
            }
            else
            {
                return false;
            }

            // 获取需要启动的程序名称
            if (elmtRoot.HasAttribute("FileName"))
            {
                m_strFileName = elmtRoot.GetAttribute("FileName");
            }
            else
            {
                return false;
            }

            // 获取需要启动的程序参数
            if (elmtRoot.HasAttribute("Arguments"))
            {
                m_strArguments = elmtRoot.GetAttribute("Arguments");
            }
            else
            {
                return false;
            }

            //////////////////////////////////////////////////////////////////////////
            // 开始循环读取
            XmlElement elmtUpdate = elmtRoot.FirstChild as XmlElement;
            while(elmtUpdate != null)
            {
                UpdateInfo info = new UpdateInfo();

                // 获取服务器文件名
                if(elmtUpdate.HasAttribute("ServerFile"))
                {
                    info.ServerFile = elmtUpdate.GetAttribute("ServerFile");
                }
                else
                {
                    return false;
                }

                // 获取要替换的本地文件名
                if (elmtUpdate.HasAttribute("LocalFile"))
                {
                    info.LocalFile = elmtUpdate.GetAttribute("LocalFile");
                }
                else
                {
                    return false;
                }

                // 获取更新后的文件名
                if (elmtUpdate.HasAttribute("LatestFile"))
                {
                    info.LatestFile = elmtUpdate.GetAttribute("LatestFile");
                }
                else
                {
                    return false;
                }

                // 获取是否覆盖源文件
                if (elmtUpdate.HasAttribute("Cover"))
                {
                    try
                    {
                        info.Cover = int.Parse(elmtUpdate.GetAttribute("Cover"));
                    }
                    catch (System.Exception e)
                    {
                        MessageBox.Show(e.Message);
                        return false;
                    }
                }
                else
                {
                    return false;
                }

                // 将UpdateInfo插入更新列表中，并继续下一个循环
                m_UpdateList.Add(info);
                elmtUpdate = elmtUpdate.NextSibling as XmlElement;
            }

            return true;
        }

        // 链接服务器
        public int ConnectServer()
        {
            if (Directory.Exists(m_strServerPath))
            {
                return m_UpdateList.Count;
            }
            else
            {
                return -1;
            }
        }

        // 检查是否需要更新
        public void CheckUpdate()
        {
            foreach (UpdateInfo info in m_UpdateList)
            {
                info.isNeedUpdate(m_strServerPath, m_strLocalPath);
            }
        }

        // 逐个更新文件
        public int UpdateFile()
        {
            // 出错的文件个数
            int iErrorNum = 0;

            if(m_UpdateList == null)
            {
                //AddErrorLog("程序内部错误!");
                return 1;
            }

            int iProgress = 0;
            foreach (UpdateInfo info in m_UpdateList)
            {
                MainForm.Instance.UpdateUI(iProgress, "更新" + info.LatestFile);

                if (!info.UpdateFile(m_strServerPath, m_strLocalPath))
                {
                    //AddErrorLog("---(文件" + info.LatestFile + "未能更新!)---");
                    iErrorNum++;
                }
                
                iProgress++;

                MainForm.Instance.UpdateUI(iProgress, "更新" + info.LatestFile);
            }

            return iErrorNum;
        }

        //---------------------------------------------------------------------------------------
        private static UpdateManager ms_Instance = null;        // 唯一的全局实例
        private String m_strServerPath = "";                    // 服务器路径
        private String m_strLocalPath = "";                     // 本地文件家路径
        private String m_strFileName = "";                      // 完成更新后，需要启动的程序名称
        private String m_strArguments = "";                     // 启动程序的参数
        private ArrayList m_UpdateList = new ArrayList();       // 包含了要更新的文件
    }

    class UpdateInfo
    {
        public String ServerFile
        {
            get
            {
                return m_strServerFile;
            }
            set
            {
                m_strServerFile = value;
            }
        }

        public String LocalFile
        {
            get
            {
                return m_strLocalFile;
            }
            set
            {
                m_strLocalFile = value;
            }
        }

        public String LatestFile
        {
            get
            {
                return m_strLatestFile;
            }
            set
            {
                m_strLatestFile = value;
            }
        }

        public int Cover
        {
            get
            {
                return m_bCover;
            }
            set
            {
                m_bCover = value;
            }
        }

        public String Status
        {
            get
            {
                return m_strStatus;
            }
            set
            {
                m_strStatus = value;
            }
        }

        public bool NeedUpdate
        {
            get
            {
                return m_bNeedUpdate;
            }
            set
            {
                m_bNeedUpdate = value;
            }
        }
        //---------------------------------------------------------------------------------------
        public bool UpdateFile(String strServerPath, String strLocalPath)
        {
            if (m_bNeedUpdate)
            {
                // 判断服务器上是否存在更新文件
                if (!File.Exists(strServerPath + m_strServerFile))
                {
                    //UpdateManager.Instance.AddErrorLog("无法从更新服务器获得" + m_strServerFile);
                    m_strStatus = "无法找到服务器文件，更新失败！";
                    return false;
                }
                else
                {
                    if (m_bCover == 1)
                    {
                        if (File.Exists(strLocalPath + m_strLocalFile))
                        {
                            // 取消只读属性
                            File.SetAttributes(strLocalPath + m_strLocalFile, FileAttributes.Normal);
                            // 删除老版本的文件
                            File.Delete(strLocalPath + m_strLocalFile);
                        }
                    }
                }

                // 取消只读属性
                if (File.Exists(strLocalPath + m_strLatestFile))
                {
                    File.SetAttributes(strLocalPath + m_strLatestFile, FileAttributes.Normal);
                }

                // 更新新文件
                try
                {
                    File.Copy(strServerPath + m_strServerFile, strLocalPath + m_strLatestFile, true);
                    m_strStatus = "更新成功";
                }
                catch (System.Exception e)
                {
                    //UpdateManager.Instance.AddErrorLog(e.Message);
                    m_strStatus = e.Message;
                    return false;
                }
            }

            return true;
        }

        // 根据文件的日期，来判断文件是否需要更新
        public bool isNeedUpdate(String strServerPath, String strLocalPath)
        {
            // 服务器文件不存在，无法更新
            if(!File.Exists(strServerPath + m_strServerFile))
            {
                m_strStatus = "无法更新，服务器文件" + m_strServerFile + "不存在";
                m_bNeedUpdate = false;
                return false;
            }

            // 本地文件不存在，必须更新
            if(!File.Exists(strLocalPath + m_strLocalFile))
            {
                m_strStatus = "需要更新";
                m_bNeedUpdate = true;
                return true;
            }

            // 比较文件的日期，比较的是修改时间

            DateTime dtServerTime = File.GetLastWriteTime(strServerPath + m_strServerFile);
            DateTime dtLocalTime = File.GetLastWriteTime(strLocalPath + m_strLocalFile);
            if(dtLocalTime.Equals(dtServerTime))
            {
                m_strStatus = "不需要更新";
                m_bNeedUpdate = false;
                return false;
            }
            else
            {
                m_strStatus = "需要更新";
                m_bNeedUpdate = true;
                return true;
            }
        }
        //---------------------------------------------------------------------------------------
        private String m_strServerFile = "";    // 服务器文件名
        private String m_strLocalFile = "";     // 要替换的本地文件名
        private String m_strLatestFile = "";    // 更新后的文件名
        private int m_bCover = 1;               // 是否覆盖原文件
        private String m_strStatus = "";        // 更新状态
        private bool m_bNeedUpdate = true;      // 是否需要更新
    };
}
