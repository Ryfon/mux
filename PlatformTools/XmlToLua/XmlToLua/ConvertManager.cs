﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;

namespace XmlToLua
{
    class ConvertManager
    {
        public static ConvertManager Instance
        {
            get
            {
                if (ms_Instance == null)
                {
                    ms_Instance = new ConvertManager();
                }
                return ms_Instance;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // 初始化，主要是确定要处理的文件，及输出路径
        public bool Init(String strFileName)
        {
            // 数据清空初始化
            m_fileList.Clear();
            m_strExportPath = "";

            // 获取新的数据
            if(strFileName.Length == 0) // 文件名为空，处理当前路径下全部的xml
            {
                // 输出文件夹
                String strCurrPath = Directory.GetCurrentDirectory();
                m_strExportPath = strCurrPath + "\\Export";

                // 遍历文件
                String[] fileList = Directory.GetFileSystemEntries(strCurrPath);
                foreach(String name in fileList)
                {
                    if(Path.GetExtension(name).Equals(".xml"))
                    {
                        m_fileList.Add(name);
                    }
                }
            }
            else
            {
                if(System.IO.File.Exists(strFileName))
                {
                    m_fileList.Add(strFileName);
                    String strParentPath = Path.GetDirectoryName(strFileName);
                    m_strExportPath = strParentPath + "\\Export";
                }
                else
                {
                    return false;
                }
            }

            // 初始化输出文件夹
            return InitExportDirectory(m_strExportPath);
        }

        public void ExportAll()
        {
            foreach(String fileName in m_fileList)
            {
                Console.WriteLine(fileName + "处理中。。。。。。\n");
                
                FileConvert fc = new FileConvert();
                if (!fc.Export(fileName, m_strExportPath))
                {
                    Console.WriteLine(fileName + "转化失败！\n");
                }

                Console.WriteLine(fileName + "处理完毕！\n");
            }
        }
        //////////////////////////////////////////////////////////////////////////
        private bool InitExportDirectory(String strPath)
        {
            if(!Directory.Exists(strPath))
            {
                try
                {
                    DirectoryInfo info = Directory.CreateDirectory(strPath);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }

                return true;
            }
            else
            {
                return true;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        // 成员变量
        private static ConvertManager ms_Instance = null; // 单件模式，唯一的实例
        private ArrayList m_fileList = new ArrayList();     // 所有要处理的文件
        private String m_strExportPath;                     // 导出lua文件的输出路径
    }
}
