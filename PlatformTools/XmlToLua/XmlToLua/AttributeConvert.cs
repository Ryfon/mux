﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XmlToLua
{
    class AttributeConvert
    {
        public String Convert(String strValue)
        {
            // 先判断有没有逗号
            if (strValue.Contains(","))
            {
                return "\"" + strValue + "\"";
            }

            int iValue = 0;
            float fValue = 0.0f;

            if (int.TryParse(strValue, out iValue))
            {
                return iValue.ToString();
            }
            else
            {
                if(float.TryParse(strValue, out fValue))
                {
                    return fValue.ToString();
                }
                else
                {
                    return "\"" + strValue + "\"";
                }
            }
            
        }
    }
}
