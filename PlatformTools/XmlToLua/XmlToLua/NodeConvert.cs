﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace XmlToLua
{
    class NodeConvert
    {
        public NodeConvert(StreamWriter writer)
        {
            m_streamWriter = writer;
        }

        public String Convert(XmlElement xmlNode, int tabNum)
        {
            String result = "";
            String tabs = MakeTabs(tabNum);

            result += MakeHead(tabs);

            result += ConvertName(xmlNode, tabNum + 1);

            result += ConvertAttributes(xmlNode, tabNum + 1);

            // 20100105分段输出
            m_streamWriter.Write(result);
            result = "";

            result += ConvertChilds(xmlNode, tabNum + 1);

            result += MakeTrail(tabs);

            // 20100105分段输出
            m_streamWriter.Write(result);
            result = "";

            return result;
        }
        //////////////////////////////////////////////////////////////////////////
        private String ConvertName(XmlElement xmlNode, int tabNum)
        {

            String result = "";
            String tabs = MakeTabs(tabNum);

            result += tabs;

            result += "XmlNodeName = ";

            result += "\"" + xmlNode.Name + "\"" + ",\r\n";

            return result;
        }

        private String ConvertAttributes(XmlElement xmlNode, int tabNum)
        {
            String result = "";
            String headTabs = MakeTabs(tabNum);
            String trailtabs = MakeTabs(tabNum + 1);
            String childTabs = MakeTabs(tabNum);

            // 属性列表名
            //result += headTabs;
            //result += "Attributes = ";

            //result += MakeHead("");

            // 遍历所有属性
            foreach(XmlAttribute attribute in xmlNode.Attributes)
            {
                result += childTabs;
                result += attribute.Name + " = ";
                
                AttributeConvert ac = new AttributeConvert();
                String value = ac.Convert(attribute.Value);

                result += value;
                result += ",\r\n";
            }

            //result += MakeTrail(trailtabs);

            return result;
        }

        private String ConvertChilds(XmlElement xmlNode, int tabNum)
        {
            String result = "";
            String headTabs = MakeTabs(tabNum);
            String trailtabs = MakeTabs(tabNum + 1);
            
            // 属性列表名
            //result += headTabs;
            //result += "ChildNodes = ";

            //result += MakeHead("");

            // 遍历所有属性
            foreach (XmlElement xmlChild in xmlNode.ChildNodes)
            {
                result += headTabs;
                result += "[";
                result += xmlChild.Attributes[0].Value;
                result += "]";
                //result += xmlChild.GetAttribute("nID");
                result += " = ";

                // 20100105分段输出
                m_streamWriter.Write(result);
                result = "";

                result += Convert(xmlChild, tabNum + 1);
                result += ",\r\n";
            }

            //result += MakeTrail(trailtabs);

            return result;
        }

        private String MakeTabs(int tabNum)
        {
            String result = "";
            for (int i = 0; i < tabNum;++i )
            {
                result += "\t";
            }
            return result;
        }

        private String MakeHead(String tabs)
        {
            return "{\r\n";
        }

        private String MakeTrail(String tabs)
        {
            return tabs + "}";
        }

        //////////////////////////////////////////////////////////////////////////
        StreamWriter m_streamWriter = null;//数据文件流
    }
}
