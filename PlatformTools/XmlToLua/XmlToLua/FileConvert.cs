﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;

namespace XmlToLua
{
    class FileConvert
    {
        public bool Export(String strFileName, String strExportPath)
        {
            if(LoadXmlFile(strFileName))
            {
                // 创建文件流
                String name = Path.GetFileNameWithoutExtension(strFileName);
                String strExportName = strExportPath + "\\" + name + ".lua";
                m_streamWriter = new StreamWriter(strExportName, false, Encoding.GetEncoding("GB2312"));

                if(Convert())
                {
                    if(SaveLuaFile(strExportName))
                    {
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("保存lua文件失败！\n");
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine("xml转lua失败！\n");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("加载xml文件失败！\n");
                return false;
            }
        }
        //////////////////////////////////////////////////////////////////////////
        private bool LoadXmlFile(String strFileName)
        {
            try
            {
                m_xmlDoc = new XmlDocument();
                m_xmlDoc.Load(strFileName);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        private bool SaveLuaFile(String strFileName)
        {
            //StreamWriter writer = new StreamWriter(strFileName, false, Encoding.GetEncoding("GB2312"));
            if (m_streamWriter == null)
            {
                return false;
            }
            else
            {
                //writer.Write(m_strLuaData);
                m_streamWriter.Flush();
                m_streamWriter.Close();
            }
            return true;
        }

        private bool Convert()
        {
            // 获取根节点
            XmlElement xmlRoot = m_xmlDoc.LastChild as XmlElement;
            if (xmlRoot == null)
            {
                Console.WriteLine("xml格式错误！\n");
                return false;
            }
            else
            {
                // 根节点的名字，作为表名
                m_strLuaData = xmlRoot.Name + " = ";
                m_streamWriter.Write(m_strLuaData);

                // 转化节点数据
                NodeConvert nc = new NodeConvert(m_streamWriter);
                m_strLuaData += nc.Convert(xmlRoot, 1);
            }

            return true;
        }
        //////////////////////////////////////////////////////////////////////////
        XmlDocument     m_xmlDoc;           // xml的数据
        String          m_strLuaData = "";       // lua数据
        StreamWriter m_streamWriter = null;//数据文件流
    }
}
