﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XmlToLua
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length > 0)
            {
                ConvertManager.Instance.Init(args[0]);
            }
            else
            {
                ConvertManager.Instance.Init("");
            }

            ConvertManager.Instance.ExportAll();
        }
    }
}
