﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ViewMapFile.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_VIEWMAPFILE_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     142
#define IDB_BITMAP2                     143
#define IDB_BITMAP3                     144
#define IDB_BITMAP4                     145
#define IDB_BITMAP5                     146
#define IDB_BITMAP6                     147
#define IDB_BITMAP7                     148
#define IDB_BITMAP8                     149
#define IDB_BITMAP9                     150
#define IDB_BITMAP10                    151
#define IDB_BITMAP11                    152
#define IDC_BUTTON1                     1000
#define IDC_STATIC_BMP                  1003
#define IDC_BUTTON5                     1004
#define IDC_STATIC0                     1005
#define IDC_STATIC1                     1006
#define IDC_STATIC2                     1007
#define IDC_STATIC3                     1008
#define IDC_STATIC4                     1009
#define IDC_STATIC5                     1010
#define IDC_STATIC6                     1011
#define IDC_STATIC7                     1012
#define IDC_STATIC8                     1013
#define IDC_STATIC9                     1014
#define IDC_STATIC10                    1015
#define IDC_COMBO1                      1018
#define IDC_COMB0BOX_TYPE               1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        153
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
