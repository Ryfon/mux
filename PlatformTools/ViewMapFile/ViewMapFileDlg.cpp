﻿// ViewMapFileDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ViewMapFile.h"
#include "ViewMapFileDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int* g_pMap = NULL;
Gdiplus::Bitmap* g_pBitmap; 
Gdiplus::Graphics* g_graphics;
int g_iWith = 0;
int g_iHeight = 0;
int g_iType = 0;

enum
{
	TYPE_CLIENT = 0,
	TYPE_MAP,
	TYPE_SERVER
};

struct SGridAttachInfo
{
	vector<int> vRegionIds; 
};

Color g_BlockColor[11] =
{
	Color(0,0,0),		//Empty
	Color(255,0,0),		//Block
	Color(0,255,0),		//Grass
	Color(204,204,230),	//Snow
	Color(255,240,0),	//Earth
	Color(255,240,255),	//Ice
	Color(128,128,128),	//Stone
	Color(204,153,128),	//Mud
	Color(0,50,128),	//Water
	Color(204,153,204),	//Wood
	Color(255,100,204),	//gameplay

};
//<TerrainProperty ID="0" NAME="Empty" R="0.000000" G="0.000000" B="0.000000" />
//<TerrainProperty ID="1" NAME="Block" R="1.000000" G="0.000000" B="0.000000" />
//<TerrainProperty ID="2" NAME="Grass" R="0.000000" G="1.000000" B="0.000000" />
//<TerrainProperty ID="3" NAME="Snow" R="204 0.800000" G="0.800000" B="230 0.900000" />
//<TerrainProperty ID="4" NAME="Earth" R="0.990000" G="0.940000" B="0.000000" />
//<TerrainProperty ID="5" NAME="Ice" R="0.990000" G="0.940000" B="0.990000" />
//<TerrainProperty ID="6" NAME="Stone" R="0.50000" G="0.540000" B="0.500000" />
//<TerrainProperty ID="7" NAME="Mud" R="0.80000" G="0.540000" B="0.500000" />
//<TerrainProperty ID="8" NAME="Water" R="0.00000" G="0.200000" B="0.500000" />
//<TerrainProperty ID="9" NAME="Wood" R="0.80000" G="0.600000" B="0.800000" />
//<TerrainProperty ID="10" NAME="gameplay" R="0.98000" G="0.370000" B="0.800000" />

vector< SGridAttachInfo > g_GridAttachInfos;

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CViewMapFileDlg 对话框




CViewMapFileDlg::CViewMapFileDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CViewMapFileDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CViewMapFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_COMB0BOX_TYPE, m_ComboType);
	DDX_Control(pDX, IDC_COMB0BOX_TYPE, m_ComboType);
}

BEGIN_MESSAGE_MAP(CViewMapFileDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CViewMapFileDlg::OnBnClickedButton1)
	ON_WM_DROPFILES()
	ON_CBN_SELCHANGE(IDC_COMB0BOX_TYPE, &CViewMapFileDlg::OnCbnSelchangeComb0boxType)
END_MESSAGE_MAP()


// CViewMapFileDlg 消息处理程序

BOOL CViewMapFileDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	m_ComboType.InsertString(0,"客户端信息");
	m_ComboType.InsertString(1,"地图信息");
	m_ComboType.InsertString(2,"服务器信息");

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CViewMapFileDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。
void CViewMapFileDlg::DrawMinimap( int nCtrlID)
{
	CWnd* pWnd = GetDlgItem(nCtrlID);
	if( pWnd && g_pBitmap )
	{
		g_graphics = ::new Graphics( pWnd->GetDC()->GetSafeHdc() ); 	
		if ( g_graphics )
		{
			g_graphics->DrawImage(g_pBitmap,0,0);
		}		 	
		pWnd->Invalidate(TRUE);		
	}

}
void CViewMapFileDlg::DrawBlockTypeIcon( int nCtrlID,int nBmpID )
{
	CWnd* pWnd = GetDlgItem(nCtrlID);

	HBITMAP     hBmp;    
	hBmp   =   LoadBitmap(AfxGetApp()->m_hInstance,MAKEINTRESOURCE(nBmpID)); 
	Bitmap* aMap = Bitmap::FromHBITMAP(hBmp,NULL); 	
	if ( pWnd && aMap )
	{		
		g_graphics = ::new Graphics( pWnd->GetDC()->GetSafeHdc() ); 	
		g_graphics->DrawImage(aMap,0,0); 	
		pWnd->Invalidate(TRUE);
	}
}

void CViewMapFileDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);

	}
	else
	{
		CDialog::OnPaint();
		DrawBlockTypeIcon(IDC_STATIC0,IDB_BITMAP1);
		DrawBlockTypeIcon(IDC_STATIC1,IDB_BITMAP2);
		DrawBlockTypeIcon(IDC_STATIC2,IDB_BITMAP3);
		DrawBlockTypeIcon(IDC_STATIC3,IDB_BITMAP4);
		DrawBlockTypeIcon(IDC_STATIC4,IDB_BITMAP5);
		DrawBlockTypeIcon(IDC_STATIC5,IDB_BITMAP6);
		DrawBlockTypeIcon(IDC_STATIC6,IDB_BITMAP7);
		DrawBlockTypeIcon(IDC_STATIC7,IDB_BITMAP8);
		DrawBlockTypeIcon(IDC_STATIC8,IDB_BITMAP9);
		DrawBlockTypeIcon(IDC_STATIC9,IDB_BITMAP10);
		DrawBlockTypeIcon(IDC_STATIC10,IDB_BITMAP11);
		DrawMinimap(IDC_STATIC_BMP);
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CViewMapFileDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
void CViewMapFileDlg::OnOK()
{
	//
}

void CViewMapFileDlg::OnCancel()
{
	int* g_pMap = NULL;
	if ( g_pMap )
	{
		delete [] g_pMap;
	}

	CDialog::OnCancel();
}
BOOL OnFileOpen( HWND hWnd, char* lpstrFilter,char* szOutFileName)
{
	OPENFILENAME	ofn;
	char			szFile[MAX_PATH];

	::ZeroMemory(szFile,MAX_PATH);
	::ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFile = (LPSTR)szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter =(LPCSTR)lpstrFilter;
	ofn.nFilterIndex = 0;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if(FALSE==::GetOpenFileName(&ofn))
	{
		return FALSE;
	}
	strcpy(szOutFileName,szFile);

	return TRUE;
}

#define MAPINFO			0x00300000 //地图信息所在位置
#define MAPINFO_NOR		0x00000000 //野地
#define MAPINFO_LOW		0x00100000 //小路
#define MAPINFO_HIG		0x00200000 //大路
#define MAPINFO_BLK		0x00300000 //阻挡
#define MAPINFO_RID		0x00400000 //骑乘区
#define MAPINFO_BATTLE	0x00800000 //战斗区
#define MAPINFO_PK		0x01000000 //PK区

void DebugOutPathFindFile( int *pMaps,unsigned int iW, unsigned int iH )
{
	//保存成BMP
	struct t_RGBINFO
	{
		BYTE b;
		BYTE g;
		BYTE r;
	};

	t_RGBINFO *pRGB = new  t_RGBINFO[iH*iW];  // 定义位图数据
	memset( pRGB, 0, sizeof(t_RGBINFO)*(iH*iW) ); // 设置背景为黑色

	unsigned int x, y;
	for ( x = 0; x < iW; ++x )	// 遍历行
	{		
		for ( y = 0; y < iH; ++y ) // 遍历列
		{
			/*地图编辑器里编辑之后，一定要在任务编辑器里导出格子信息*/
			
			switch( g_iType )
			{
			case TYPE_CLIENT:
				{
					int iRet = pMaps[y+x*iW] & MAPINFO;

					switch ( iRet )
					{
					case MAPINFO_BLK://碰撞属性
						{
							pRGB[y + x * iW].r = 255;
							break;
						}
					case MAPINFO_NOR:
						{
							//pRGB[y + x * iW].r = 255;
							break;
						}
					case MAPINFO_RID://骑乘区
						{
							pRGB[y + x * iW].g = 255;
							break;
						}
					case MAPINFO_BATTLE://战斗区
						{
							pRGB[y + x * iW].b = 255;
							break;
						}
					case MAPINFO_PK://PK区
						{
							pRGB[y + x * iW].r = 255;
							pRGB[y + x * iW].g = 255;
							break;
						}
					default:
						{
							break;
						}
					}
					break;
				}
			case TYPE_MAP:
				{
					//编辑器算法
					int iRet = pMaps[y+x*iW];
					if ( iRet >= 0 && iRet <= 10 )
					{
						pRGB[y + x * iW].r = g_BlockColor[iRet].GetR();
						pRGB[y + x * iW].g = g_BlockColor[iRet].GetG();
						pRGB[y + x * iW].b = g_BlockColor[iRet].GetB();
					}
					break;
				}
			case TYPE_SERVER:
				{
					break;
				}
			default: break;
			}
			
			int i = 0;
			//原来客户端算法
			if ( pMaps[y+x*iW] == MAPINFO )
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 0  )
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 25165824)//安全区 = 不可战斗+不可PK
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 3145729 )//碰撞区
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 28311553 )//带碰撞的安全区
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 19922945)//带碰撞的不可PK区
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 16777216 )//不带碰撞的不可PK去
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 4194304 )//骑乘区 & 副本前置
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 7340033 )//骑乘区 & 副本前置+碰撞
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 9 )
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 10 )
			{
				i = 0;
			}
			else if ( pMaps[y+x*iW] == 25165829 )
			{
				i = 0;
			}
			else 
			{
				char szBuffer[MAX_PATH];
				sprintf(szBuffer,"未知的值 - [ %d ]",pMaps[y+x*iW]);
				//AfxMessageBox(szBuffer);
			}

		}
	}

	// 每个像素点3个字节
	int size = iW*iH*3; 

	// 位图第一部分，文件信息
	BITMAPFILEHEADER bfh;
	bfh.bfType = 0x4d42;
	bfh.bfSize = size + sizeof( BITMAPFILEHEADER )+ sizeof( BITMAPINFOHEADER );
	bfh.bfReserved1 = 0; // reserved
	bfh.bfReserved2 = 0; // reserved
	bfh.bfOffBits = bfh.bfSize - size;

	// 位图第二部分，数据信息
	BITMAPINFOHEADER bih;
	bih.biSize = sizeof(BITMAPINFOHEADER);
	bih.biWidth = iW;
	bih.biHeight = iH;
	bih.biPlanes = 1;
	bih.biBitCount = 24;
	bih.biCompression = 0;
	bih.biSizeImage = size;
	bih.biXPelsPerMeter = 0;
	bih.biYPelsPerMeter = 0;
	bih.biClrUsed = 0;
	bih.biClrImportant = 0;
	FILE * fp = fopen( "info.bmp", "wb" );
	if( !fp ) 
	{
		return;
	}
	fwrite( &bfh, 1, sizeof(BITMAPFILEHEADER), fp );
	fwrite( &bih, 1, sizeof(BITMAPINFOHEADER), fp );
	fwrite( pRGB, 1, size, fp );
	fclose( fp );

	delete []pRGB;

}
void CViewMapFileDlg::UpdateBitmap( int* pMap ,unsigned int iW, unsigned int iH )
{
	if ( g_pBitmap )
	{
		::delete g_pBitmap;
		g_pBitmap = NULL;		
	}

	if ( g_graphics )
	{
		::delete g_graphics;
		g_graphics = NULL;
	}

	DebugOutPathFindFile( pMap, iW, iH );
	g_pBitmap = ::new Bitmap(L"info.bmp");	
	DrawMinimap(IDC_STATIC_BMP);
}
void CViewMapFileDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	char szFileName[MAX_PATH];
	if(OnFileOpen(GetSafeHwnd(),"所有文件 (*.map)\0*.map\0\0",szFileName))
	{
		if( g_pMap )
		{
			::delete[] g_pMap;
			g_pMap = NULL;
		}
		FILE* fp = NULL;
		fp = fopen(szFileName,"rb");

		//1.读取基本地图信息
	
		int iMapW,iMapH,iMapX,iMapY,iSize;		
		DWORD dwVersion, dwMapNo;

		// 读版本号
		fread( &dwVersion, sizeof(DWORD), 1, fp );
		fread( &dwMapNo, sizeof(DWORD), 1, fp );

		fread( &iMapX, sizeof(int), 1, fp );
		fread( &iMapY, sizeof(int), 1, fp );
		fread( &iMapW, sizeof(int), 1, fp );
		fread( &iMapH, sizeof(int), 1, fp );

		iSize = iMapW * iMapH;
		if (!iSize)
		{
			assert(0);
		}

		g_pMap = ::new int[iSize];
		
		fread( g_pMap, sizeof(int), iSize, fp );


		UpdateBitmap(g_pMap,iMapW,iMapH);
		//2.读取附加格子信息
		/*
		int nGridAttachInfos = 0;
		fread( &nGridAttachInfos, sizeof(int), 1, fp );
		if ( 0 == nGridAttachInfos )
		{
			assert(0);
		}
		g_GridAttachInfos.resize( nGridAttachInfos );

		for ( int nIndex = 0; nIndex < nGridAttachInfos; nIndex ++ )
		{
			int nRegionIndex = 0;
			fread( &nRegionIndex, sizeof(int), 1, fp );
			
			if ( 0 == nRegionIndex )
			{
				break;;
			}
			
			if ( nRegionIndex != nIndex + 1 )
			{
				assert(0);
			}
			
			int nRegionCount = 0;
			fread( &nRegionCount, sizeof(int), 1, fp );
			
			if ( 0 == nRegionCount )
			{
				break;;
			}

			for ( int nRegionIndex = 0; nRegionIndex < nRegionCount; nRegionIndex ++ )
			{
				int nRegionId = 0;
				fread( &nRegionId, sizeof(int), 1, fp );
				if ( 0 == nRegionId )
				{
					break;;
				}
				g_GridAttachInfos[nIndex].vRegionIds.push_back( nRegionId );
			}
		}	*/	
	}
	
}

void CViewMapFileDlg::OnDropFiles(HDROP hDropInfo)
{

	UINT count;
	char filePath[MAX_PATH];

	count = DragQueryFile(hDropInfo, 0xFFFFFFFF, NULL, 0);
	if(count == 1)
	{        
		int pathLen = DragQueryFile(hDropInfo, 0, filePath, sizeof(filePath));
		if ( strlen(filePath ) <= 1 )
		{
			return;
		}

		char szExt[MAX_PATH];
		_splitpath(filePath,NULL,NULL,NULL,szExt );

		if ( stricmp(".map",szExt) !=0 )
		{
			return;
		}

		if( g_pMap )
		{
			::delete[] g_pMap;
			g_pMap = NULL;
		}
		FILE* fp = NULL;
		fp = fopen(filePath,"rb");

		//1.读取基本地图信息

		int iMapW,iMapH,iMapX,iMapY,iSize;		
		DWORD dwVersion, dwMapNo;

		// 读版本号
		fread( &dwVersion, sizeof(DWORD), 1, fp );
		fread( &dwMapNo, sizeof(DWORD), 1, fp );

		fread( &iMapX, sizeof(int), 1, fp );
		fread( &iMapY, sizeof(int), 1, fp );
		fread( &iMapW, sizeof(int), 1, fp );
		fread( &iMapH, sizeof(int), 1, fp );

		iSize = iMapW * iMapH;
		if (!iSize)
		{
			assert(0);
		}

		g_pMap = ::new int[iSize];

		fread( g_pMap, sizeof(int), iSize, fp );


		UpdateBitmap(g_pMap,iMapW,iMapH);
	}

	DragFinish(hDropInfo);

	CDialog::OnDropFiles(hDropInfo);
}

void CViewMapFileDlg::OnCbnSelchangeComb0boxType()
{
	g_iType = m_ComboType.GetCurSel();
	// TODO: 在此添加控件通知处理程序代码
}
