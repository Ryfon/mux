﻿// PatchToolDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "PatchTool.h"
#include "PatchToolDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CPatchToolDlg 对话框




CPatchToolDlg::CPatchToolDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPatchToolDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPatchToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_PatchPackPath);
}

BEGIN_MESSAGE_MAP(CPatchToolDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_PACKPATH, &CPatchToolDlg::OnBnClickedBtnPackpath)
	ON_BN_CLICKED(IDC_BTN_RELEASEPATH, &CPatchToolDlg::OnBnClickedBtnReleasepath)
	ON_BN_CLICKED(IDC_BTN_UPDATE, &CPatchToolDlg::OnBnClickedBtnUpdate)
END_MESSAGE_MAP()


// CPatchToolDlg 消息处理程序

BOOL CPatchToolDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CPatchToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CPatchToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CPatchToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CPatchToolDlg::OnBnClickedBtnPackpath()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog *openFileDlg = new CFileDialog(true,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openFileDlg->m_ofn.lpstrFilter = "资源包文件(*.pkg)|*.pkg|All files(*.*)|*.*";

	if ( openFileDlg->DoModal() == IDOK )
	{
		strncpy_s( m_pPatchPack, (LPCTSTR)openFileDlg->GetPathName(),sizeof(m_pPatchPack) );		
	}
	else
	{
		//ShowMessage("物件信息读入失败");
		return;
	}

	GetDlgItem( IDC_EDIT1 )->SetWindowText( m_pPatchPack );
}

string CPatchToolDlg::GetDir()
{
	//查找文件夹
	string strPath;
	BROWSEINFO	bi;

	LPITEMIDLIST pidl;
	LPMALLOC	pMalloc;

	TCHAR szDirPath[MAX_PATH];

	if ( SUCCEEDED( SHGetMalloc(&pMalloc) ) )
	{
		ZeroMemory( &bi,sizeof(bi) );

		bi.hwndOwner = this->m_hWnd;
		bi.pszDisplayName = 0;
		bi.pidlRoot = 0;
		bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_VALIDATE;

		pidl = SHBrowseForFolder( &bi );

		if ( pidl )
		{
			if ( !SHGetPathFromIDList( pidl, szDirPath ) )
			{
				//ShowMessage("读取文件夹路径出错！");
				return "";
			}
			else
			{
				strPath = string( szDirPath );
			}
		}

		pMalloc->Free( pidl );
		pMalloc->Release();
	}

	return strPath;
}


void CPatchToolDlg::OnBnClickedBtnReleasepath()
{
	// TODO: 在此添加控件通知处理程序代码
	string strReleasePath = GetDir();

	if (strReleasePath == "")
	{
		return;
	}

	strncpy_s( m_pReleasePath, (LPCTSTR)strReleasePath.c_str(),sizeof(m_pReleasePath) );		

	GetDlgItem( IDC_EDIT2 )->SetWindowText( m_pReleasePath );
}

void CPatchToolDlg::OnBnClickedBtnUpdate()
{
	// TODO: 在此添加控件通知处理程序代码

	if (m_pPatchPack == NULL || m_pReleasePath == NULL)
	{
		return;
	}

	CReleasePatch* pkPatch = new CReleasePatch(m_pPatchPack);

	if (NULL == pkPatch)
	{
		return ;
	}

	pkPatch->ReleasePatchPackage(m_pReleasePath);

	pkPatch->Destroy();

	pkPatch = NULL;

	MessageBox("释放完毕！");

}
