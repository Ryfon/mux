﻿#include "StdAfx.h"
#include "ReleasePatch.h"

CReleasePatch::CReleasePatch(void)
{
	m_pkPackage = new CPackage();
}

CReleasePatch::~CReleasePatch(void)
{
	if (m_pkPackage)
	{
		Destroy();
	}
}

void CReleasePatch::Destroy()
{
	if (m_pkPackage)
	{
		m_pkPackage->ClosePackage();
		delete m_pkPackage;
		m_pkPackage = NULL;
	}

	map<string, CPackage*>::iterator it = m_mapReleasePackList.begin();
	for (;it != m_mapReleasePackList.end(); it++)
	{
		if (it->second != NULL)
		{
			CPackage* pkTmp = it->second;
			pkTmp->ClosePackage();
			delete pkTmp;
			pkTmp = NULL;
		}
	}
	m_mapReleasePackList.clear();

	m_mapUpdateFiles.clear();
}

CReleasePatch::CReleasePatch(const char *pszPackFilePath)
{
	m_bInit = false;
	if (NULL == pszPackFilePath)
	{
		return;
	}

	m_pkPackage = new CPackage();

	if (!m_pkPackage->OpenPackage(pszPackFilePath))
	{
		return;
	}

	m_bInit = true;
}

bool CReleasePatch::ReleasePatchPackage(const char *pszReleasePath)
{
	if (NULL == pszReleasePath)
	{
		return false;
	}

	if (NULL == m_pkPackage)
	{
		return false;
	}

	if (!m_bInit)
	{
		PATCH_MEG_REPROT("Un initialed patch package","");
		return false;
	}
	m_strReleasePath = pszReleasePath;
	StringLower(m_strReleasePath);

	string strConfigXMLPath = m_strReleasePath + "\\" + PATCH_CONFIGXML_FILE_NMAE;

	if (!_LoadCurrentVersion(strConfigXMLPath))
	{
		PATCH_MEG_REPROT("Fail to load current version",strConfigXMLPath);
		return false;
	}

	if(!_Init())
	{
		PATCH_MEG_REPROT("Fail to initial releaser","");
		return false;
	}	

	if (!_Patch())
	{
		PATCH_MEG_REPROT("Fail during patching","");
		return false;
	}
	
	if (!_UpdatePatchClientXML(strConfigXMLPath))
	{
		PATCH_MEG_REPROT("Fail during update local version","");
		return false;
	}
	
	return true;
}

bool CReleasePatch::_Init()
{
	if (m_strReleasePath == "")
	{
		return false;
	}

	if (!_LoadLogFile())
	{
		PATCH_MEG_REPROT("Fail during load log file","");
		return false;
	}

	if (_NeedUpdate() == false)
	{
		return false;
	}

	// 载入所有包 [8/6/2009 hemeng]
	if (!_InitPackages())
	{
		PATCH_MEG_REPROT("Fail druing initial packages","");
		return false;
	}

	return true;
}

bool CReleasePatch::_NeedUpdate()
{
	if (NULL == m_pkPackage || m_strReleasePath == "")
	{
		return false;
	}

	if (CPatchHelper::CompareVersion(m_CurrVersion,m_OldVersion) != 0)
	{
		PATCH_MEG_REPROT("当前版本与本更新包适用版本不符，不可更新",m_CurrVersion);
		return false;
	}
	if (CPatchHelper::CompareVersion(m_CurrVersion,m_NewVersion) != -1)
	{
		PATCH_MEG_REPROT("当前版本与本更新包最高版本相同或更高，不可更新",m_CurrVersion);
		return false;
	}

	return true;
}

map<string, CPackage*>::iterator CReleasePatch::_PackageMapFinder(string strFileName)
{
	StringLower(strFileName);

	if (strFileName == "")
	{
		return m_mapReleasePackList.end();
	}

	map<string,CPackage*>::iterator it = m_mapReleasePackList.begin();
	string strPackName = "";

	for (;it != m_mapReleasePackList.end(); it++)
	{
		strPackName = it->first;

		// 找到包 [8/7/2009 hemeng]
		if (strFileName.find(strPackName) != string::npos)
		{
			return it;
		}
	}

	return it;
}

// 文件名必须为相对路径 [8/7/2009 hemeng]
bool CReleasePatch::_IsPackageFile(std::string strFileName)
{
	StringLower(strFileName);
	map<string,CPackage*>::iterator it = _PackageMapFinder(strFileName);

	if (it == m_mapReleasePackList.end())
	{
		return false;
	}

	return true;
}

string CReleasePatch::_GetPackNameByFileName(string strFileName)
{
	StringLower(strFileName);

	string strPackName = "";

	map<string,CPackage*>::iterator it = _PackageMapFinder(strFileName);

	if (it != m_mapReleasePackList.end())
	{
		strPackName = it->first;
	}

	return strPackName;
}

// strFileName必须为相对路径 [8/6/2009 hemeng]
CPackage* CReleasePatch::_GetPackByFileName(string strFileName)
{
	StringLower(strFileName);

	CPackage* pkPackage = NULL;

	map<string, CPackage*>::iterator it = _PackageMapFinder(strFileName);

	if (it != m_mapReleasePackList.end())
	{
		pkPackage = it->second;
	}

	return pkPackage;
}

bool CReleasePatch::_DeleteUnuseFiles()
{
	map<PATCH_COMPARE_RESULT_TYPE, vector<string>>::iterator it = m_mapUpdateFiles.find(PCRT_DELETE_FILE);

	// 需要删除的文件 [8/10/2009 hemeng]
	if (it != m_mapUpdateFiles.end())
	{
		vector<string> vDeleteFileList = it->second;

		for (unsigned int uiIndex = 0; uiIndex < vDeleteFileList.size(); uiIndex++)
		{
			string strFileName = vDeleteFileList[uiIndex];

			if (_IsPackageFile(strFileName))
			{
				if (!_DeleteFileFromPackage(strFileName))
				{
					PATCH_MEG_REPROT("Fail to delete file from package",strFileName);
					return false;
				}
			}
			else
			{
				string strAbsFilePath = _ConvertMediaFileName(strFileName);
				if (strAbsFilePath == "")
				{
					return false;
				}
				// 是否存在且可读 [8/10/2009 hemeng]
				if (_access_s(strAbsFilePath.c_str(), 2) != 0)
				{
					PATCH_MEG_REPROT("Fail to delete file due to wrong file access",strAbsFilePath);
					return false;
				}
				else
				{
					// 删除文件 [8/10/2009 hemeng]
					if(!DeleteFile(strAbsFilePath.c_str()))
					{
						PATCH_MEG_REPROT("Fail to delete file",strAbsFilePath);
						return false;
					}
				}
			}
		}
	}

	return true;
}

bool CReleasePatch::_Patch()
{
	bool bSuccess = true;

	bSuccess = _DeleteUnuseFiles();
		
	if (bSuccess)
	{
		bSuccess = _DeleteUnuseDirs();		

		if (bSuccess)
		{
			bSuccess = _UpdateFiles();			
		}
	
		if (bSuccess)
		{
			bSuccess = _AddFiles();			
		}
	}

	return bSuccess;
}

bool CReleasePatch::_LoadLogFile()
{
	CPackageFileReader* pkReader = m_pkPackage->GetPackageFileReader(PATCH_RESULT_FILE_NAME);

	if (NULL == pkReader)
	{
		return false;
	}

	SPackageFileInfo* pkFileInfo = pkReader->GetPackageFileInfo();
	if (NULL == pkFileInfo)
	{
		delete pkReader;
		pkReader = NULL;

		return false;
	}

	UINT nBufferSize = pkFileInfo->nFileOrgSize;

	char* pBuffer = new char[nBufferSize];

	if (pkReader->ReadBuffer((unsigned char*)pBuffer) == 0)
	{
		delete [] pBuffer;
		pBuffer = NULL;
		
		delete pkFileInfo;
		pkFileInfo = NULL;

		delete pkReader;
		pkReader = NULL;		
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->Parse(pBuffer);

	bool bSuccess = true;
	TiXmlElement* pElmt = pXmlDoc->FirstChildElement();
	
	if (NULL == pElmt)
	{
		bSuccess = false;
	}

	while (pElmt != NULL && bSuccess != false)
	{
		const char* pszName = pElmt->Value();
		if (strcmp(pszName, "Version") == 0)
		{
			_LoadVersion(pElmt);
		}
		else if (strcmp(pszName, "AddFileList") == 0)
		{
			if(!_LoadFileList(pElmt,PCRT_ADD_FILE))
			{
				bSuccess = false;
			}
		}
		else if(strcmp(pszName, "DeleteFileList") == 0)
		{
			if(!_LoadFileList(pElmt,PCRT_DELETE_FILE))
			{
				bSuccess = false;
			}
		}
		else if(strcmp(pszName,"UpdateFileList") == 0)
		{
			if(!_LoadFileList(pElmt,PCRT_UPDATE_FILE))
			{
				bSuccess = false;
			}
		}
		else if(strcmp(pszName,"DeleteDirList") == 0)
		{
			if(!_LoadFileList(pElmt,PCRT_DELETE_DIR))
			{
				bSuccess = false;
			}
		}

		pElmt = pElmt->NextSiblingElement();
	}


	pkFileInfo = NULL;

	delete pkReader;
	pkReader = NULL;

	delete [] pBuffer;
	pBuffer = NULL;

	delete pXmlDoc;
	pXmlDoc = NULL;

	return bSuccess;
}

void CReleasePatch::_LoadVersion(TiXmlElement* pElmt)
{
	if (pElmt == NULL)
	{
		return;
	}

	m_NewVersion = pElmt->Attribute("NewVersion");
	m_OldVersion = pElmt->Attribute("OldVersion");
}

bool CReleasePatch::_LoadFileList(TiXmlElement* pElmt,PATCH_COMPARE_RESULT_TYPE nType)
{
	if (pElmt == NULL)
	{
		return false;
	}

	map<PATCH_COMPARE_RESULT_TYPE,vector<string>>::iterator it = m_mapUpdateFiles.find(nType);

	if (it == m_mapUpdateFiles.end())
	{
		vector<string> vTmp;
		m_mapUpdateFiles.insert(pair<PATCH_COMPARE_RESULT_TYPE,vector<string>>(nType,vTmp));

		it = m_mapUpdateFiles.find(nType);
		if (it == m_mapUpdateFiles.end())
		{
			return false;
		}
	}

	TiXmlElement* pElmtSub = pElmt->FirstChildElement();

	while (pElmtSub != NULL)
	{
		string strTmp = pElmtSub->Attribute("FileName");

		if (strTmp != "")
		{
			it->second.push_back(strTmp);
		}
			
		pElmtSub = pElmtSub->NextSiblingElement();
	}

	return true;
}

bool CReleasePatch::_UpdatePatchClientXML(string strXmlFilePath)
{
	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->LoadFile(strXmlFilePath.c_str());

	TiXmlElement* pElmtRoot = pXmlDoc->RootElement();
	TiXmlElement* pElmt = pElmtRoot->FirstChildElement();

	const char* pszName = pElmt->Value();

	if (strcmp(pszName, "ClientVersion") == 0)
	{
		pElmt->SetAttribute("Version",m_NewVersion.c_str());
	}

	bool bSuccess = pXmlDoc->SaveFile();

	delete pXmlDoc;
	pXmlDoc = NULL;

	return bSuccess;
}

string CReleasePatch::_ConvertMediaFileName(string strFilePath)
{
	if (strFilePath == "")
	{
		return "";
	}

	StringLower(strFilePath);
	
	string strMediaFilePath = "";
	// 属于包文件 [8/6/2009 hemeng]
	if (_IsPackageFile(strFilePath))
	{
		string strPackName = _GetPackNameByFileName(strFilePath);
		if (strPackName == "")
		{
			return "";
		}
		strMediaFilePath = m_strReleasePath + "\\" + strPackName;
		
	}
	// 属于文件 [8/6/2009 hemeng]
	else
	{
		strMediaFilePath = m_strReleasePath + "\\" + strFilePath;
	}

	
	return strMediaFilePath;
}



CPackage* CReleasePatch::_LoadPackage(string strPackFilePath)
{
	if (strPackFilePath == "")
	{
		return false;
	}

	StringLower(strPackFilePath);

	CPackage* pkPackage = new CPackage();

	if (NULL == pkPackage)
	{
		return NULL;
	}

	if (!pkPackage->OpenPackage(strPackFilePath,CPackage::EO_WRITE))
	{
		PATCH_MEG_REPROT("Fail to load package",strPackFilePath);
		delete pkPackage;
		pkPackage = NULL;

		return NULL;
	}

	return pkPackage;
}

string CReleasePatch::_ConvertRelPath(const string strFilePath)
{
	if (strFilePath == "")
	{
		return "";
	}

	string strTmp = strFilePath;
	StringLower(strTmp);

	// 找不到相对路径 [8/6/2009 hemeng]
	size_t nPos = strTmp.find(m_strReleasePath);
	if (nPos == string::npos)
	{
		return "";
	}

	nPos += m_strReleasePath.length();
	string strRelPath = strTmp.substr(nPos);
	strRelPath = strRelPath.substr(strRelPath.find("\\") + 1);

	return strRelPath;
}

bool CReleasePatch::_InitPackages()
{
	m_mapReleasePackList.clear();

	vector<string> vAllFiles;

	FindAllFiles(m_strReleasePath,vAllFiles);

	for (unsigned int uiIndex = 0; uiIndex < vAllFiles.size(); uiIndex++)
	{
		string strTmp = vAllFiles[uiIndex];
		StringLower(strTmp);
		string strExtention = strTmp;
		strExtention = strExtention.substr(strExtention.find_last_of("."));

		if (strExtention == PACKAGE_EXTENTION)
		{
			CPackage* pkPackage = _LoadPackage(strTmp);
			if (NULL == pkPackage)
			{
				return false;
			}

			string strRelPath = _ConvertRelPath(strTmp);
			if (strRelPath == "")
			{
				pkPackage->ClosePackage();
				delete pkPackage;
				pkPackage = NULL;

				return false;
			}

			// 如果没有载入过，则载入 [8/6/2009 hemeng]
			map<string, CPackage*>::iterator it = m_mapReleasePackList.find(strRelPath);
			if (it == m_mapReleasePackList.end())
			{
				// 去掉pkg标志 [8/10/2009 hemeng]
				strRelPath = strRelPath.substr(0,strRelPath.find_last_of("."));
				StringLower(strRelPath);

				m_mapReleasePackList.insert(pair<string,CPackage*>(strRelPath,pkPackage));
			}
			
		}
	}

	return true;
}


bool CReleasePatch::_AddFiles()
{
	map<PATCH_COMPARE_RESULT_TYPE, vector<string>>::iterator it = m_mapUpdateFiles.find(PCRT_ADD_FILE);

	// 有需要添加的文件 [8/7/2009 hemeng]
	if (it != m_mapUpdateFiles.end())
	{
		vector<string> vFilesToAdd = it->second;

		for (unsigned int uiIndex = 0; uiIndex < vFilesToAdd.size(); uiIndex++)
		{
			string strFileName = vFilesToAdd[uiIndex];

			// 是包文件 [8/10/2009 hemeng]
			if (_IsPackageFile(strFileName))
			{
				if (!_AddFileToPackage(strFileName))
				{
					PATCH_MEG_REPROT("Fail to add file to package",strFileName);
					return false;
				}
			}
			// 文件 [8/10/2009 hemeng]
			else
			{
				if (!_AddFile(strFileName))
				{
					PATCH_MEG_REPROT("Fail to add file",strFileName);
					return false;
				}
			}
		}
	}


	return true;
}

bool CReleasePatch::_UpdateFiles()
{
	map<PATCH_COMPARE_RESULT_TYPE, vector<string>>::iterator it = m_mapUpdateFiles.find(PCRT_UPDATE_FILE);

	// 有需要更新的文件 [8/7/2009 hemeng]
	if (it != m_mapUpdateFiles.end())
	{
		vector<string> vFilesToAdd = it->second;

		for (unsigned int uiIndex = 0; uiIndex < vFilesToAdd.size(); uiIndex++)
		{
			string strFileName = vFilesToAdd[uiIndex];

			// 是包文件 [8/10/2009 hemeng]
			if (_IsPackageFile(strFileName))
			{
				if (!_AddFileToPackage(strFileName))
				{
					PATCH_MEG_REPROT("Fail to update file in package",strFileName);
					return false;
				}
			}
			// 文件 [8/10/2009 hemeng]
			else
			{
				if (!_AddFile(strFileName))
				{
					PATCH_MEG_REPROT("Fail to update file",strFileName);
					return false;
				}
			}
		}
	}


	return true;
}

bool CReleasePatch::_AddFileToPackage(string strFileName)
{
	if (strFileName == "")
	{
		return false;
	}
	StringLower(strFileName);

	CPackage* pkPackage = _GetPackByFileName(strFileName);
	if (NULL == pkPackage)
	{
		return false;
	}
	string strPackName = pkPackage->GetPackageRootDir();

	string strTmp = strFileName.substr(strFileName.find(strPackName) + strPackName.size() + 1);

	// 将文件名转换为相对于包的名称 [8/10/2009 hemeng]
	bool bSuccess = true;

	CPackageFileReader* pkReader = m_pkPackage->GetPackageFileReader(strFileName);
	if (NULL == pkReader)
	{
		PATCH_MEG_REPROT("Fail to read file data",strFileName);
		bSuccess = false;
	}

	if(bSuccess)
	{
		SPackageFileInfo* pkFileInfo = pkReader->GetPackageFileInfo();
				
		if (NULL == pkFileInfo)
		{
			bSuccess = false;
		}

		if (bSuccess)
		{
			UINT nUnZipBuffer = pkFileInfo->nFileSize;
			string strFileNameInPKG = strTmp;
			if (strTmp.find_last_of("\\") != string::npos)
			{
				strFileNameInPKG = strTmp.substr(strTmp.find_last_of("\\") + 1);
			}
			 
			pkFileInfo->szFilename = strFileNameInPKG;
			pkFileInfo->szFullPathName = strTmp;
			BYTE* pBuffer = new BYTE[nUnZipBuffer];
			
			bSuccess = pkReader->ReadUnZipBuffer(pBuffer);

			if (bSuccess)
			{
				bSuccess = pkPackage->AddFile(*pkFileInfo,pBuffer,true);
			}
			
			if (!bSuccess)
			{
				PATCH_MEG_REPROT("Fail to add file",pkFileInfo->szFullPathName);
			}
			
			delete [] pBuffer;
			pBuffer = NULL;
		}
	}

	if (pkPackage)
	{
		// 所有的package由析构函数最终关闭及释放 [8/10/2009 hemeng]
		pkPackage = NULL;
	}

	if (pkReader)
	{
		delete pkReader;
		pkReader = NULL;
	}

	return bSuccess;
}

bool CReleasePatch::_DeleteFileFromPackage(string strFileName)
{
	if (strFileName == "")
	{
		return false;
	}
	StringLower(strFileName);

	CPackage* pkPackage = _GetPackByFileName(strFileName);

	if (NULL == pkPackage)
	{
		return false;
	}

	string strPackName = _GetPackNameByFileName(strFileName);
	
	if (strPackName == "")
	{
		PATCH_MEG_REPROT("Fail to find file package",strFileName);
		return false;
	}

	StringLower(strFileName);
	string strRelPath = strFileName.substr(strFileName.find(strPackName) + strPackName.length() + 1);	

	if (strRelPath == "")
	{
		return false;
	}

	return pkPackage->DeletePackFile(strRelPath);

}

bool CReleasePatch::_AddFile(string strFileName)
{
	if (strFileName == "")
	{
		return false;
	}

	StringLower(strFileName);
	string strAbsPath = _ConvertMediaFileName(strFileName);

	if (strAbsPath == "")
	{
		return false;
	}

	// 如果文件已存在则删除重建 [8/10/2009 hemeng]
	if (_access_s(strAbsPath.c_str(),0) == 0)
	{
		if (DeleteFile(strAbsPath.c_str()) == 0)
		{
			PATCH_MEG_REPROT("Fail to update file,delete old file fail",strAbsPath);
			return false;
		}
	}

	if (!_ValidateDirs(strAbsPath))
	{
		PATCH_MEG_REPROT("Fail to update file,invalidate directory",strAbsPath);
		return false;
	}

	return m_pkPackage->CreateNewFileFromPackage(strFileName,strAbsPath);

}

bool CReleasePatch::_DeleteUnuseDirs()
{
	map<PATCH_COMPARE_RESULT_TYPE, vector<string>>::iterator it = m_mapUpdateFiles.find(PCRT_DELETE_DIR);

	if (it != m_mapUpdateFiles.end())
	{
		vector<string> vDeleteDirs = it->second;
		for (unsigned int uiIndex = 0; uiIndex < vDeleteDirs.size(); uiIndex++)
		{
			if (_IsPackageFile(vDeleteDirs[uiIndex]))
			{
				if (!_DeleteDirFromPackage(vDeleteDirs[uiIndex]))
				{
					PATCH_MEG_REPROT("Fail to delete directory from package",vDeleteDirs[uiIndex]);
					return false;
				}
			}
			else
			{
				string strDirPath = _ConvertMediaFileName(vDeleteDirs[uiIndex]);
				if (strDirPath == "")
				{
					return false;
				}

				// 是否存在 [8/10/2009 hemeng]
				if ( _access_s(strDirPath.c_str(),2) != 0 )
				{
					PATCH_MEG_REPROT("Fail to delete directoy, bad file access",strDirPath);
					return false;
				}

				if (!DeleteFile(strDirPath.c_str()))
				{
					PATCH_MEG_REPROT("Fail to delete directory",strDirPath);
					return false;
				}
			}			
		}
	}

	return true;
}

bool	CReleasePatch::_LoadCurrentVersion(string strPatchClientFileName)
{
	if (strPatchClientFileName == "")
	{
		return false;
	}

	StringLower(strPatchClientFileName);
	if (_access_s(strPatchClientFileName.c_str(),0) != 0)
	{
		PATCH_MEG_REPROT("Fail to find current version file",strPatchClientFileName);
		return false;
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	pXmlDoc->LoadFile(strPatchClientFileName.c_str());

	bool bSuccess = true;

	TiXmlElement* pElmtRoot = pXmlDoc->RootElement();
	TiXmlElement* pElmt = pElmtRoot->FirstChildElement();

	if (NULL == pElmt)
	{
		bSuccess = false;
	}

	const char* pszName = pElmt->Value();
	if (strcmp(pszName, "ClientVersion") == 0)
	{
		m_CurrVersion = pElmt->Attribute("Version");
	}

	delete pXmlDoc;
	pXmlDoc = NULL;

	return bSuccess;
}

bool	CReleasePatch::_DeleteDirFromPackage(string strDirName)
{
	if (strDirName == "")
	{
		return false;
	}

	StringLower(strDirName);

	CPackage* pkPackge = _GetPackByFileName(strDirName);
	if (NULL == pkPackge)
	{
		return false;
	}

	CPackageFileSystem* pkFileSys = pkPackge->GetPackageFileSystem();
	if (NULL == pkPackge)
	{
		pkPackge = NULL;
		return false;
	}

	string strPackName = pkPackge->GetPackageRootDir();
	string strRelDirName = strDirName.substr(strDirName.find(strPackName) + strPackName.size() + 1);

	SPackageDirInfo* pkDelDirInfo = pkFileSys->GetPackDirInfo(strRelDirName);
	if (NULL == pkDelDirInfo)
	{
		pkFileSys = NULL;
		pkPackge = NULL;
		return false;
	}
	
	bool bSuccess = _DeleteDirFromPackage(pkPackge,pkDelDirInfo);

	pkDelDirInfo = NULL;
	pkFileSys = NULL;
	pkPackge = NULL;

	return bSuccess;
}

bool	CReleasePatch::_DeleteDirFromPackage(CPackage* pkPackge,SPackageDirInfo* pkDirInfo)
{
	if (NULL == pkPackge || NULL == pkDirInfo)
	{
		return false;
	}
	
	// 首先删除文件 [8/20/2009 hemeng]
	map<string,SPackageFileInfo>::iterator itFiles = pkDirInfo->vPackageFileInfos.begin();	
	for (; itFiles != pkDirInfo->vPackageFileInfos.end(); itFiles++)
	{
		SPackageFileInfo nFileInfo = itFiles->second;
		if(!pkPackge->DeletePackFile(nFileInfo.szFullPathName))
		{
			return false;
		}
	}

	// 删除子目录文件 [8/20/2009 hemeng]
	map<string,SPackageDirInfo>::iterator itDirs = pkDirInfo->vPackageDirInfos.begin();
	for (; itDirs != pkDirInfo->vPackageDirInfos.end(); itDirs++)
	{
		SPackageDirInfo nFileInfo = itDirs->second;
		if (!_DeleteDirFromPackage(pkPackge,&nFileInfo))
		{
			return false;
		}
	}

	return true;
}