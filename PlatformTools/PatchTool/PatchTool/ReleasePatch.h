﻿/** @file ReleasePatch.h 
@brief 文件打包：patch工具 第一版测试
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-08-06 
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

// 说明 [8/6/2009 hemeng]
// 必须将打包配置文件至于可执行文件同目录下

#pragma once

#include "Package.h"
#include "PatchDef.h"	
#include "tinyxml.h"
#include "PatchHelper.h"

class CReleasePatch
{
public:
	CReleasePatch(const char* pszPackFilePath);
	~CReleasePatch(void);

	bool	ReleasePatchPackage(const char* pszReleasePath);
	void	Destroy();

protected:
	CReleasePatch(void);
	

	// 比较类型 [8/17/2009 hemeng]
	enum PATCH_COMPARE_RESULT_TYPE
	{
		PCRT_ADD_FILE = 0,
		PCRT_DELETE_FILE,
		PCRT_UPDATE_FILE,
		PCRT_DELETE_DIR,
	};

	// 初始化 [8/6/2009 hemeng]	
	bool		_Init();
	bool		_IsPackageFile(string strFileName);
	// 检查release目录下的version是否需要更新
	bool		_NeedUpdate();

	CPackage*	_GetPackByFileName(string strFileName);

	string		_GetPackNameByFileName(string strFileName);

	bool		_Patch();

	bool		_AddFiles();

	bool		_UpdateFiles();

	bool		_DeleteUnuseFiles();

	bool		_DeleteUnuseDirs();

	// 检查是否目录下的所有PKG文件 [8/6/2009 hemeng]
	CPackage*	_LoadPackage(string strPackFilePath);

	// 从文件读取log数据 [8/6/2009 hemeng]
	bool		_LoadLogFile();
	void		_LoadVersion(TiXmlElement* pElmt);
	bool		_LoadFileList(TiXmlElement* pElmt,PATCH_COMPARE_RESULT_TYPE nType);

	// 获取当前版本 [8/19/2009 hemeng]
	bool		_LoadCurrentVersion(string strPatchClientFileName);

	// 转换文件路径为绝对路径 [8/6/2009 hemeng]
	string		_ConvertMediaFileName(const string strFilePath);

	// 获取相对路径 [8/6/2009 hemeng]
	string		_ConvertRelPath(const string strFilePath);

	// 装载安装位置下的所有包文件 [8/10/2009 hemeng]
	bool		_InitPackages();

	// 添加包数据 [8/10/2009 hemeng]
	bool		_AddFileToPackage(string strFileName);

	// 删除包数据 [8/10/2009 hemeng]
	bool		_DeleteFileFromPackage(string strFileName);
	bool		_DeleteDirFromPackage(string strDirName);
	bool		_DeleteDirFromPackage(CPackage* pkPackge,SPackageDirInfo* pkDirInfo);

	// 添加文件数据 [8/10/2009 hemeng]
	bool		_AddFile(string strFileName);

	bool		_UpdatePatchClientXML(string strXmlFilePath);

	// 辅助函数s [8/10/2009 hemeng]
	// 辅助函数用于查找 [8/10/2009 hemeng]
	map<string,CPackage*>::iterator _PackageMapFinder(string strFileName);

protected:
	CPackage*				m_pkPackage;			// patch包文件 [8/6/2009 hemeng]
	string					m_strReleasePath;		// 释放目录 [8/6/2009 hemeng]
	// 释放目录中所有的包文件 [8/6/2009 hemeng]
	// key为包路径，值为已打开的包指针，可写方式打开 [8/6/2009 hemeng]
	map<string,CPackage*>	m_mapReleasePackList;	
	// 所有需要删除、更新、添加的文件及文件夹 [8/6/2009 hemeng]
	// key为类型（删除、添加etc） 值为文件 key都为相对路径
	map<PATCH_COMPARE_RESULT_TYPE,vector<string>> m_mapUpdateFiles;
	string					m_NewVersion;
	string					m_OldVersion;
	string					m_CurrVersion;
	bool					m_bInit;
};
