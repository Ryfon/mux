﻿#include "StdAfx.h"
#include "PatchHelper.h"

CPatchHelper::CPatchHelper(void)
{
}

CPatchHelper::~CPatchHelper(void)
{
}

string CPatchHelper::GetCurrWorkDir()
{
	char pTemp[MAX_PATH];
	GetModuleFileName(NULL,pTemp,MAX_PATH);
	if (NULL == pTemp)
	{
		return "";
	}

	string strConfigFilePath = string(pTemp);

	strConfigFilePath = strConfigFilePath.substr(0,strConfigFilePath.find_last_of("\\"));
	strConfigFilePath += "\\";

	return strConfigFilePath;
}

bool CPatchHelper::StringToBYTE(string str, BYTE* pBuffer, UINT nBufferSize)
{
	if (str == "")
	{
		return false;
	}

	if (NULL == pBuffer)
	{
		return false;
	}

	if (nBufferSize <= str.size())
	{
		return false;
	}

	for (unsigned int uiIndex = 0; uiIndex < str.size(); uiIndex++)
	{
		pBuffer[uiIndex] = (BYTE)str[uiIndex];
	}

	return true;
}

bool CPatchHelper::DeleteVectorElem(vector<string>& vec,string strInfo)
{
	vector<string>::iterator it = vec.begin();
	bool bSuccess = false;

	for (unsigned int i = 0;it != vec.end(); it++, i++)
	{
		if (vec[i] == strInfo)
		{
			bSuccess = true;
			break;
		}
	}

	if (bSuccess)
	{
		vec.erase(it);
	}

	return bSuccess;
}

DWORD CPatchHelper::MyStringToDWORD(string str)
{
	DWORD num;

	stringstream sstr;

	sstr << str;

	sstr >> num;

	sstr.clear();

	return num;
}	

string CPatchHelper::MyIntToString(int num)
{
	string str = "";

	stringstream sstr;

	sstr << num;

	sstr >> str;

	sstr.clear();

	return str;
}

int CPatchHelper::CompareVersion(string strSrcVer,string strDesVer)
{
	// version 0.XXXX.X [8/13/2009 hemeng]
	//  [8/13/2009 hemeng]
	string strHigh = strSrcVer.substr(strSrcVer.find(".") + 1,strSrcVer.find_last_of("."));
	string strLow = strSrcVer.substr(strSrcVer.find_last_of(".") + 1);

	if (strHigh == "" || strLow == "")
	{
		return -2;
	}

	DWORD iSrcHigh =MyStringToDWORD(strHigh);
	DWORD iSrcLow = MyStringToDWORD(strLow);

	strHigh = strDesVer.substr(strDesVer.find(".") + 1,strDesVer.find_last_of("."));
	strLow = strDesVer.substr(strDesVer.find_last_of(".") + 1);

	if (strHigh == "" || strLow == "")
	{
		return -2;
	}

	DWORD iDesHigh = MyStringToDWORD(strHigh);
	DWORD iDesLow = MyStringToDWORD(strLow);

	// 比较 [8/13/2009 hemeng]
	if (iSrcHigh > iDesHigh)
	{
		return 1;

	}
	else if (iSrcHigh < iDesHigh)
	{
		return -1;
	}
	else if (iSrcHigh == iDesHigh)
	{
		if (iSrcLow > iDesLow)
		{
			return 1;
		}
		else if (iSrcLow < iDesLow)
		{
			return -1;
		}
		else if (iSrcLow == iDesLow)
		{
			return 0;
		}
	}

	return -2;
}