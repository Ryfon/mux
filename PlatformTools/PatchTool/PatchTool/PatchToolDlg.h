﻿// PatchToolDlg.h : 头文件
//

#pragma once
#include "afxwin.h"


// CPatchToolDlg 对话框
class CPatchToolDlg : public CDialog
{
// 构造
public:
	CPatchToolDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_PATCHTOOL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnPackpath();
	string GetDir();

protected:
	char m_pPatchPack[MAX_PATH];
	char m_pReleasePath[MAX_PATH];
public:
	CEdit m_PatchPackPath;
	afx_msg void OnBnClickedBtnReleasepath();
	afx_msg void OnBnClickedBtnUpdate();
};
