﻿/** @file PatchDef.h 
@brief 文件打包：patch工具一些定义 第一版测试
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-08-06 
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#if	!defined _PATCHDEF_H_ 
#define 	_PATCHDEF_H_

// 说明 [8/6/2009 hemeng]
#define PACKAGE_EXTENTION		".pkg"		
#define PATCH_DATAPACK_NAME		".mux"						// patch文件的类型 [8/12/2009 hemeng]
// 生成差异包后生成的log文件 [8/17/2009 hemeng]
// 包含 差异数据列表、新旧版本号等
#define PATCH_RESULT_FILE_NAME		"patch_log.xml"	
#define PATCH_CONFIGXML_FILE_NMAE	"PatchClient.xml"
#endif