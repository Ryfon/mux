﻿// PatchTool.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号

#include "ReleasePatch.h"
// CPatchToolApp:
// 有关此类的实现，请参阅 PatchTool.cpp
//

class CPatchToolApp : public CWinApp
{
public:
	CPatchToolApp();

// 重写
	public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()

protected:
	void _SplitString( const char * szStr, vector< string >& vStrs, char chToken );

	////////////////////////////////////////////////////////////////////////////////
	//函数名:_ProcessReleasePatching
	//功能:命令行打包
	//入口参数:string strFilePath		增量包文件路径
	//		   string strReleasePath	释放路径
	//出口参数:bool
	////////////////////////////////////////////////////////////////////////////////
	bool _ProcessReleasePatching(string strFilePath, string strReleasePath);

};

extern CPatchToolApp theApp;