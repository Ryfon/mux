﻿#if	!defined _PATCHHELPER_H_
#define _PATCHHELPER_H_

#pragma once

#include <string>
#include <windows.h>
#include <vector>
#include <sstream>

using namespace std;

// msg 报错注释，filePath 报错文件名
#ifdef CODE_IN_PATCH
#define PATCH_MEG_REPROT(msg,fileName) do {   char szInfo[MAX_PATH * 3];\
	sprintf_s( szInfo, "%s %s\n", msg,fileName );\
	MessageBox(NULL,szInfo,"Error",MB_OK); } while(0)
#else
#define PATCH_MEG_REPROT(msg,filePath)do {   char szInfo[MAX_PATH * 3];\
	sprintf_s( szInfo, "%s %s\n", msg,filePath );\
	OutputDebugString(szInfo); } while(0)
#endif //#ifndef 


class CPatchHelper
{
public:
	CPatchHelper(void);
	~CPatchHelper(void);

	// 获取当前工作目录 [8/14/2009 hemeng]
	// 参数		
	// 返回值	工作目录以“\\”结尾
	static string	GetCurrWorkDir();

	// 将string转换为byte* [8/14/2009 hemeng]
	// 参数		str 转换源	pBuffer 转换目标 nBufferSize pBuffer的大小
	// 返回值	成功 true 失败 false 
	static bool		StringToBYTE(string str, BYTE* pBuffer, UINT nBufferSize);

	// 删除vector中的数据 [8/14/2009 hemeng]
	// 参数		vector 源	strInfo 查找目标
	// 返回值	成功 true 失败 false
	static bool		DeleteVectorElem(vector<string>& vec,string strInfo);

	// string 与DWORD互转
	static DWORD MyStringToDWORD(string str);
	static string MyIntToString(int num);
	
	// 版本号比较 [8/13/2009 hemeng]
	// 比较错误 return -2
	// strSrcVer > strDesVer return 1	strSrcVer < strDesVer return -1		strSrcVer == strDesVer return 0
	static int		CompareVersion(string strSrcVer,string strDesVer);
};

#endif