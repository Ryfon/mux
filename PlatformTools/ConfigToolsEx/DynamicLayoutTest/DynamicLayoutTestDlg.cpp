﻿// DynamicLayoutTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DynamicLayoutTest.h"
#include "DynamicLayoutTestDlg.h"
#include "DlgModal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDynamicLayoutTestDlg dialog

CDynamicLayoutTestDlg::CDynamicLayoutTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDynamicLayoutTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDynamicLayoutTestDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDynamicLayoutTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDynamicLayoutTestDlg)
	DDX_Control(pDX, IDC_TREE1, m_tree);
	DDX_Control(pDX, IDC_LIST_LIST, m_list);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDynamicLayoutTestDlg, CDialog)
	//{{AFX_MSG_MAP(CDynamicLayoutTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(IDM_SHOWBORDER, OnShowborder)
	ON_BN_CLICKED(IDC_BTN_MODALNESS, OnBtnModalness)
	ON_BN_CLICKED(IDC_BTN_MODAL, OnBtnModal)
	ON_NOTIFY(HDN_ITEMCLICK, IDC_LIST_LIST, OnItemclickList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDynamicLayoutTestDlg message handlers

BOOL CDynamicLayoutTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	m_list.InsertColumn(0,_T("列表控件1"),LVCFMT_CENTER,150,0);
	m_list.InsertColumn(1,_T("B"),LVCFMT_CENTER,100,1);
	m_list.SetExtendedStyle(LVS_EX_GRIDLINES);
	
	m_list.InsertItem(0,_T("asdf"));
	m_list.SetItemText(0,1,_T("abcdefg"));
	m_list.InsertItem(0,_T("fdsa"));
	m_list.SetItemText(0,1,_T("gfedcba"));

	HTREEITEM hRoot = m_tree.InsertItem(_T("树根"));
	m_tree.InsertItem(_T("叶子1"),hRoot);
	m_tree.InsertItem(_T("叶子2"),hRoot);

	m_dlgModalness.Create(CDlgModalness::IDD,this);

	//////////////////////////////////////////////////////////////////////////
	m_DLayout.InitLayout( this );
	m_DLayout.SetDialogMaxSize( 800,600 );
	m_DLayout.SetDialogMinSize( 200,100 );

	m_DLayout.AddPanel("上","",1,1,1,1);
	m_DLayout.AddPanel("下","",0,1,1,1);

	m_DLayout.AddPanel("上右","上",1,1,1,1);
	m_DLayout.AddControl(IDC_LIST_LIST,"上右",1,1,1,1);
	m_DLayout.AddPanel("上右下","上右",0,1,1,0);
	m_DLayout.AddControl(IDC_BTN_MODAL,"上右下",1,1,1,0);
	m_DLayout.AddControl(IDC_BTN_MODALNESS,"上右下",1,1,1,0);
	m_DLayout.AddControl(IDOK,"上右下",1,1,1,0);

	m_DLayout.AddPanel("上左","上",1,1,1,0);
	m_DLayout.AddControl(IDC_TREE1,"上左",1,1,1,1);

	m_DLayout.AddPanel("下左","下",1,1,1,0);
	m_DLayout.AddControl(IDC_LIST2,"下左",1,1,1,1);
	m_DLayout.AddPanel("下右","下",1,1,1,1);	
	m_DLayout.AddControl(IDC_EDIT1,"下右",1,1,1,1);
	m_DLayout.AddControl(IDC_STATIC_COMMAND,"下右",1,0,1,0);
	m_DLayout.AddControl(IDC_EDIT2,"下右",1,0,1,1);

	m_DLayout.SetBkColor( "上",    RGB(160,160,255));
	m_DLayout.SetBkColor( "上右",  RGB(128,128,255));
	m_DLayout.SetBkColor("上右下", RGB(128,255,128));

	//m_DLayout.UpdateLayout();
	m_DLayout.ShowPanel("",TRUE);
	m_DLayout.SetSplit("");
	m_DLayout.SetSplit("上");
	m_DLayout.SetSplit("下" );

	//////////////////////////////////////////////////////////////////////////
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDynamicLayoutTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDynamicLayoutTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDynamicLayoutTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LRESULT CDynamicLayoutTestDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_DLayout.FilterDynamicLayoutMessage(message,wParam,lParam);

	return CDialog::WindowProc(message, wParam, lParam); 
}

BOOL CDynamicLayoutTestDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if( pMsg->message == WM_KEYDOWN && 
		pMsg->wParam == VK_RETURN &&
		IDC_EDIT2 == GetFocus()->GetDlgCtrlID() )
	{
		CString strMsg;
		CString strInput;
		GetFocus()->GetWindowText( strInput );
		CString strTemp( strInput );
		strTemp.MakeLower();
		if( strTemp == _T("cls") ) SetDlgItemText(IDC_EDIT1,_T(""));
		else if( strTemp == _T("about") ) { CAboutDlg dlg; dlg.DoModal(); }
		else
		{
			strTemp.Format(_T("ShellExecute.open( \"%s\" ) "),strInput);
			HINSTANCE hIns = ::ShellExecute(m_hWnd,"open",strInput,0,0,SW_SHOWNORMAL);
			if( (int)(hIns) <= 32 )
				strTemp += _T("执行失败！");

			GetDlgItem(IDC_EDIT1)->GetWindowText( strMsg );
			strMsg = CTime::GetCurrentTime().Format(_T("%H:%M:%S ")) + strTemp + "\r\n" + strMsg;
			SetDlgItemText(IDC_EDIT1,strMsg);
			BringWindowToTop();
		}

		SetDlgItemText(IDC_EDIT2,"");
		return TRUE;
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}


void CDynamicLayoutTestDlg::OnShowborder() 
{
	// TODO: Add your command handler code here
	m_DLayout.ShowPanel("",(m_bShowBorder = !m_bShowBorder));
}

void CDynamicLayoutTestDlg::OnBtnModalness() 
{
	// TODO: Add your control notification handler code here
	m_dlgModalness.ShowWindow(SW_SHOW);
}

void CDynamicLayoutTestDlg::OnBtnModal() 
{
	// TODO: Add your control notification handler code here
	CDlgModal	dlg;
	dlg.DoModal();
	GetDlgItem(IDC_EDIT2)->SetFocus();
}

static int CALLBACK
MyCompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
   // lParamSort contains a pointer to the list view control.
   CListCtrl* pListCtrl = (CListCtrl*) lParamSort;
   CString    strItem1 = pListCtrl->GetItemText(lParam1, 0);
   CString    strItem2 = pListCtrl->GetItemText(lParam2, 0);

   return strcmp(strItem2, strItem1);
}


void CDynamicLayoutTestDlg::OnItemclickList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	HD_NOTIFY *phdn = (HD_NOTIFY *) pNMHDR;

	AfxMessageBox(_T(""));
	m_list.SortItems( MyCompareProc, (LPARAM)&m_list );

	*pResult = 0;
}
