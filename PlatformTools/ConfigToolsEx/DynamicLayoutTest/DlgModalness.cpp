﻿// DlgModalness.cpp : implementation file
//

#include "stdafx.h"
#include "DynamicLayoutTest.h"
#include "DlgModalness.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgModalness dialog


CDlgModalness::CDlgModalness(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgModalness::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgModalness)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgModalness::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgModalness)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgModalness, CDialog)
	//{{AFX_MSG_MAP(CDlgModalness)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgModalness message handlers

BOOL CDlgModalness::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_layout.InitLayout( this );

	m_layout.AddPanel("A","",1,1,1,0);
	m_layout.AddControl(IDOK,"A");
	m_layout.AddControl(IDC_STATIC_A,"A",0,1,0,1);
	
	m_layout.AddPanel("B","",1,1,0,1);
	m_layout.AddControl(IDCANCEL,"B");
	m_layout.AddControl(IDC_STATIC_B,"B");

	m_layout.UpdateLayout();
	m_layout.SetBkColor("A",RGB(100,0,0));
	m_layout.SetBkColor("B",RGB(0,100,0));

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

LRESULT CDlgModalness::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_layout.FilterDynamicLayoutMessage(message,wParam,lParam);
	return CDialog::WindowProc(message, wParam, lParam);
}
