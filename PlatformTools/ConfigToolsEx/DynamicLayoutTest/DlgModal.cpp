﻿// DlgModal.cpp : implementation file
//

#include "stdafx.h"
#include "DynamicLayoutTest.h"
#include "DlgModal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgModal dialog


CDlgModal::CDlgModal(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgModal::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgModal)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgModal::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgModal)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgModal, CDialog)
	//{{AFX_MSG_MAP(CDlgModal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgModal message handlers

BOOL CDlgModal::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_layout.InitLayout(this);
	m_layout.AddControl(IDC_EDIT1,_T(""),1,1,1,1);
	m_layout.AddPanel("A","",0,1,1,1);
	m_layout.AddControl(IDC_STATIC_COMMAND,_T("A"),1,1,1,0);
	m_layout.AddControl(IDC_EDIT2,_T("A"),1,1,1,1);
	m_layout.SetBkColor("", RGB(250,250,250));
	m_layout.SetDialogMaxSize(600,400);

	m_layout.UpdateLayout();	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

LRESULT CDlgModal::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_layout.FilterDynamicLayoutMessage(message,wParam,lParam);
	
	return CDialog::WindowProc(message, wParam, lParam);
}
