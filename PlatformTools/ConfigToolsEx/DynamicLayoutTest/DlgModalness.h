﻿#if !defined(AFX_DLGMODALNESS_H__926E128A_8449_40DB_9E0A_7EB0AB5700DA__INCLUDED_)
#define AFX_DLGMODALNESS_H__926E128A_8449_40DB_9E0A_7EB0AB5700DA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgModalness.h : header file
//
#include "DynamicLayoutExport.h"

/////////////////////////////////////////////////////////////////////////////
// CDlgModalness dialog

class CDlgModalness : public CDialog
{
// Construction
public:
	CDlgModalness(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgModalness)
	enum { IDD = IDD_MODALNESS };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgModalness)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgModalness)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	DynamicLayout::CDynamicLayout	m_layout;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGMODALNESS_H__926E128A_8449_40DB_9E0A_7EB0AB5700DA__INCLUDED_)
