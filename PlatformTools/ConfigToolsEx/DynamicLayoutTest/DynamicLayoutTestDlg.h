﻿// DynamicLayoutTestDlg.h : header file
//

#if !defined(AFX_DYNAMICLAYOUTTESTDLG_H__DA2E6F1A_4FA7_4201_BAAF_62278E162E5D__INCLUDED_)
#define AFX_DYNAMICLAYOUTTESTDLG_H__DA2E6F1A_4FA7_4201_BAAF_62278E162E5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DynamicLayoutExport.h"
#include "DlgModalness.h"

/////////////////////////////////////////////////////////////////////////////
// CDynamicLayoutTestDlg dialog

class CDynamicLayoutTestDlg : public CDialog
{
// Construction
public:
	CDynamicLayoutTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDynamicLayoutTestDlg)
	enum { IDD = IDD_DYNAMICLAYOUTTEST_DIALOG };
	CTreeCtrl	m_tree;
	CTabCtrl	m_tab;
	CListCtrl	m_list;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDynamicLayoutTestDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool m_bShowBorder;
	HICON m_hIcon;
	CDlgModalness	m_dlgModalness;

	// Generated message map functions
	//{{AFX_MSG(CDynamicLayoutTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnShowborder();
	afx_msg void OnBtnModalness();
	afx_msg void OnBtnModal();
	afx_msg void OnItemclickList1(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	DynamicLayout::CDynamicLayout	m_DLayout;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNAMICLAYOUTTESTDLG_H__DA2E6F1A_4FA7_4201_BAAF_62278E162E5D__INCLUDED_)
