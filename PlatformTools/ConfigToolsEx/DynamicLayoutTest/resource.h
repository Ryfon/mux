﻿//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DynamicLayoutTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DYNAMICLAYOUTTEST_DIALOG    102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDD_MODALNESS                   130
#define IDD_DIALOG2                     131
#define IDC_TREE1                       1000
#define IDC_LIST_LIST                   1002
#define IDC_BTN_MODALNESS               1005
#define IDC_BTN_MODAL                   1006
#define IDC_EDIT1                       1007
#define IDC_LIST2                       1008
#define IDC_STATIC_B                    1009
#define IDC_STATIC_A                    1010
#define IDC_EDIT2                       1011
#define IDC_STATIC_COMMAND              1012
#define IDC_BUTTON1                     1013
#define IDM_OPEN                        32771
#define IDM_SHOWBORDER                  32772
#define IDM_ABOUT                       32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
