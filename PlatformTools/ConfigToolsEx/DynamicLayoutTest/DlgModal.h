﻿#if !defined(AFX_DLGMODAL_H__7877CA2B_E0D8_466F_B166_0639E6E03919__INCLUDED_)
#define AFX_DLGMODAL_H__7877CA2B_E0D8_466F_B166_0639E6E03919__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgModal.h : header file
//
#include "DynamicLayoutExport.h"

/////////////////////////////////////////////////////////////////////////////
// CDlgModal dialog

class CDlgModal : public CDialog
{
// Construction
public:
	CDlgModal(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgModal)
	enum { IDD = IDD_DIALOG2 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgModal)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgModal)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	DynamicLayout::CDynamicLayout m_layout;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGMODAL_H__7877CA2B_E0D8_466F_B166_0639E6E03919__INCLUDED_)
