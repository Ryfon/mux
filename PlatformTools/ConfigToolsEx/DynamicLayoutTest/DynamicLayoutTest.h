﻿// DynamicLayoutTest.h : main header file for the DYNAMICLAYOUTTEST application
//

#if !defined(AFX_DYNAMICLAYOUTTEST_H__3306E086_1A5C_4EF2_AC90_2E78798AA1A8__INCLUDED_)
#define AFX_DYNAMICLAYOUTTEST_H__3306E086_1A5C_4EF2_AC90_2E78798AA1A8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDynamicLayoutTestApp:
// See DynamicLayoutTest.cpp for the implementation of this class
//

class CDynamicLayoutTestApp : public CWinApp
{
public:
	CDynamicLayoutTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDynamicLayoutTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDynamicLayoutTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNAMICLAYOUTTEST_H__3306E086_1A5C_4EF2_AC90_2E78798AA1A8__INCLUDED_)
