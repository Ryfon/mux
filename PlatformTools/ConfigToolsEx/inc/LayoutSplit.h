﻿// LayoutSplit.h: interface for the CLayoutSplit class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYOUTSPLIT_H__C6BD97A9_E115_4B4B_9E31_0C78988E79F4__INCLUDED_)
#define AFX_LAYOUTSPLIT_H__C6BD97A9_E115_4B4B_9E31_0C78988E79F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DynamicLayoutShare.h"

namespace DynamicLayout
{
	//
	//	Panel分割条类
	//	Yangzj 2007.09.20
	class CLayoutBase;
	class CLayoutPanel;
	class CLayoutSplit  
	{
		//只能在CLayoutPanel类内创建和销毁CLayoutSplit类
		friend class CLayoutPanel;
		CLayoutSplit( CWnd* pWnd, COLORREF dwColor=1 );
		~CLayoutSplit(){}

	public:
		void SetWidth(int nWidth){ if( nWidth>1 && nWidth<100) m_nWidth=nWidth;}
		void SetStyle( SplitStyle nStyle ){ m_nStyle = nStyle; }
		void SetPanelSizing(CLayoutBase* pLayout){ m_pPanelSizing = pLayout; }
		void SetPanelAdhere(CLayoutBase* pLayout,Border nBorder)
		{
			m_pPanelAdhere = pLayout;
			m_nBorder = nBorder;
		}

	public:
		BOOL OnMouseMove(int x, int y);
		BOOL OnLButtonDown(int x, int y);
		BOOL OnLButtonUp(int x, int y);

	protected:
		BOOL IsPointOn(int x, int y);		//点(x,y)是否在Split上
		BOOL IsPosValid( int x, int y );	//设置新的位置，如果位置非法，返回false;
		void SetCursor();		//设置光标
		void RestoreCursor();	//恢复光标
		void DrawSplit(int x, int y);		//绘制分割条
		void EraseLast();		//清除上次的绘制

	protected:
		CLayoutBase*		m_pPanelSizing; //可变大小的布局
		CLayoutBase*		m_pPanelAdhere;	//依附的布局
		Border				m_nBorder;		//依附的边1/2/4/8
 		CWnd*				m_pOwnerWnd;	//对话框
		SplitStyle			m_nStyle;		//分割条风格
		int					m_nWidth;		//分割条宽度
		COLORREF			m_dwColor;		//分割条移动时的颜色

	private://状态记录
		HCURSOR	m_hCursor;
		CDC*	m_pDC;		//保存绘图DC，同时用来区分是否正在"分割"
		CPen*	m_pPen;		//绘制分割条用的画笔
		int		m_nPosLast;	//上次绘制分割条的位置
		CPoint	m_ptSplit;//首次移动时分割条有效点的位置
		int		m_nPosBegin;//首次移动时的位置
		int		m_nSplitLen;//分割条的长度
		int		m_nPosMax;	//本次鼠标有效的最大位置
		int		m_nPosMin;	//本次鼠标有效的最小位置
	};
}



#endif // !defined(AFX_LAYOUTSPLIT_H__C6BD97A9_E115_4B4B_9E31_0C78988E79F4__INCLUDED_)
