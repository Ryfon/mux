﻿// LayoutPanel.h: interface for the CLayoutPanel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYOUTPANEL_H__9C233489_4225_4522_B06E_3874B0CC0311__INCLUDED_)
#define AFX_LAYOUTPANEL_H__9C233489_4225_4522_B06E_3874B0CC0311__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "LayoutBase.h"

namespace DynamicLayout
{
	class CLayoutSplit;
	class CLayoutPanel : public CLayoutBase 
	{
	public:
		CLayoutPanel(CWnd* wndOwner,Layout t,Layout b,Layout l,Layout r);
		virtual ~CLayoutPanel();

	public://接口
		virtual void	GetRect( CRect& rect );
		virtual void	SetRect( CRect& rect );
		virtual void	UpdateLayout();
		virtual void	Clear();
		virtual void	Draw();
		virtual void	Show( BOOL bShow=TRUE );//显示、隐藏Panel及子布局
		virtual void	Invalidate();

	public:
		BOOL	IsPanel(){ return TRUE; }
		BOOL	IsVisible(){ return m_bVisible; }
		BOOL	IsShowBorder(){ return m_bShowBorder; }
		void	SetBkColor(COLORREF dwColor){ m_dwBKColor = dwColor; }
		void	ShowBorder( BOOL bShow=FALSE );
		void	AddLayout( CLayoutBase* pLayout ){ m_list_Layout.AddTail( pLayout ); }

	public://分割条接口
		CLayoutSplit*	GetSplit();	//生成本Panel的分割条并返回,如果!CanSplit()，返回NULL
		void	ClearSplit();		//清除本Panel的Split，释放内存
		
	protected:
		inline void GetSubRect( CLayoutBase* pLayout, CRect& subRect, const CRect& rect );
		void	GetRgn(CRect& rect, CRgn& rgn, BOOL bWithBorder = FALSE );//得到Panel未被子布局覆盖且的区域
		void	UnDrawLast();//清除上次的；		
		BOOL	CanSplit();//判断本Panel能否添加分割条		

	protected:
		BOOL	m_bShowBorder;			//是否显示边框
		BOOL	m_bShoudeInvaldeMain;	//是否应该刷新主窗口
		CRect	m_rectLast;				//设置矩形区域时保存上一次的矩形区域
		CListLayoutBase	m_list_Layout;	//子布局列表
		COLORREF	m_dwBKColor;		//背景色
		CLayoutSplit*	m_pSplit;
	};
}


#endif // !defined(AFX_LAYOUTPANEL_H__9C233489_4225_4522_B06E_3874B0CC0311__INCLUDED_)
