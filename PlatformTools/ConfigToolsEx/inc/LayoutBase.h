﻿// LayoutBase.h: interface for the CLayoutBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYOUTBASE_H__75705EA7_73F1_40AA_84F7_CA562025B848__INCLUDED_)
#define AFX_LAYOUTBASE_H__75705EA7_73F1_40AA_84F7_CA562025B848__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "DynamicLayoutShare.h"

namespace DynamicLayout
{
	class CLayoutBase  
	{
	public:
		CLayoutBase(CWnd* wndOwner,Layout t,Layout b,Layout l,Layout r);
		virtual ~CLayoutBase();
	public:	
		virtual	void	GetRect(CRect& rect) = 0;
		virtual void	SetRect(CRect& rect ) = 0;
		virtual void	UpdateLayout() = 0;
		virtual void	Show(BOOL bShow=TRUE) = 0;
		virtual	void	Draw();
		virtual void	Invalidate() = 0;

		virtual void	Clear();
		virtual void	ShowBorder(BOOL bShow=TRUE);
		virtual BOOL	IsPanel() = 0;
		virtual	BOOL	IsVisible() = 0;

	public:;
		BOOL	NeedInvalidate()
		{
			if( m_bShoudInvaldeMainWnd )
			{ m_bShoudInvaldeMainWnd = FALSE; return TRUE; }
			return FALSE; 
		}
		void	RectMore(CRect& rect)
		{
			rect.left--;
			rect.top--;
			rect.right++;
			rect.bottom++;
		}
		void	RectLess(CRect& rect)
		{
			rect.left++;
			rect.top++;
			rect.right--;
			rect.bottom--;
		}
	public:
		Layout	m_eTop;
		Layout	m_eLeft;
		Layout	m_eBottom;
		Layout	m_eRight;
	protected:
		BOOL	m_bShoudInvaldeMainWnd;
		BOOL	m_bVisible;
		CRect	m_cWndRect;//窗口大小
		CWnd*	m_wndOwner;
	};
}
#endif // !defined(AFX_LAYOUTBASE_H__75705EA7_73F1_40AA_84F7_CA562025B848__INCLUDED_)
