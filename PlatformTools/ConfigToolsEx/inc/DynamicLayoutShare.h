﻿#ifndef UFO_X_YANGZHIJUN_200709160018_DYNAMICLAYOUTSHARE_H
#define UFO_X_YANGZHIJUN_200709160018_DYNAMICLAYOUTSHARE_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/********************************************************************
	created:	2007/09/16
	created:	16:9:2007   0:16
	filename: 	D:\UFO-X\DYNAMICLAYOUT\INCLUDE\DynamicLayoutShare.h
	file path:	D:\UFO-X\DYNAMICLAYOUT\INCLUDE
	file base:	DynamicLayoutShare
	file ext:	h
	author:		ue_yangzj
	
	purpose:	定义一些DynamicLayout工程使用的常量及数据类型
*********************************************************************/
#include <Afxtempl.h>

namespace DynamicLayout
{

#ifndef NDEBUG
	#define LAYOUT_ASSERT(t,s)\
	if(!(t)){\
		CString _debug_inf_;\
		_debug_inf_.Format(_T("    原因：%s\n提示信息：%s\n\n继续运行程序吗？"),#t,(s));\
	if(IDNO == ::MessageBox(NULL,_debug_inf_,_T("CDynamicLayout使用错误"),MB_YESNO|MB_ICONWARNING))exit(0);}
#else
	#define LAYOUT_ASSERT(t,s)
#endif

	//类声明
	class CLayoutBase;
	class CLayoutControl;
	class CLayoutPanel;
	class CLayoutPanelFrame;
	class CLayoutSplit;

	//类型定义
	typedef const CString&						Panel;
	typedef CMapPtrToPtr						CMapSplit;
	typedef CMapStringToPtr						CMapNamePanel;
	typedef CList<CLayoutBase*,  CLayoutBase* >	CListLayoutBase;

	//子布局边与父布局对应边之间的锁定关系
	typedef BOOL Layout;
	const Layout LAYOUT_LOCK	=	TRUE;	//正向锁定
	const Layout LAYOUT_UNLOCK	=	FALSE;	//反向锁定
	
	//预定义边
	typedef int Border;
	const Border BORDER_TOP		=	1;	//上
	const Border BORDER_BOTTOM	=	2;	//下
	const Border BORDER_LEFT	=	4;	//左
	const Border BORDER_RIGHT	=	8;	//右

	//分割条风格
	typedef BOOL SplitStyle;
	const SplitStyle SPLIT_LINE	=	0;	//先确定位置，再分割(默认)
	const SplitStyle SPLIT_RT	=	1;	//实时分割
}

#endif//UFO_X_YANGZHIJUN_200709160018_DYNAMICLAYOUTSHARE_H
