﻿// DynamicLayoutExport.h: interface for the CDynamicLayout class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DYNAMICLAYOUTEXPORT_H__6EAF03A9_8360_4AC8_8B1B_E7CDC0086992__INCLUDED_)
#define AFX_DYNAMICLAYOUTEXPORT_H__6EAF03A9_8360_4AC8_8B1B_E7CDC0086992__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/********************************************************************
	created:	2007/09/15 15:16
	filename: 	D:\UFO-X\DynamicLayout\Include\DynamicLayoutExport.h
	file path:	D:\UFO-X\DynamicLayout\Include
	file base:	DynamicLayoutExport
	file ext:	h
	author:		ue_yangzj
	
	purpose:	实现对话框的动态布局管理；
				接口类；
*********************************************************************/

#include "DynamicLayoutShare.h"
namespace DynamicLayout
{
	class _declspec(dllexport) CDynamicLayout  
	{
	public:
		CDynamicLayout();
		~CDynamicLayout();
		
		//	消息处理函数；
		//	该函数必需在对话框的WindowProc(UINT message, WPARAM wParam, LPARAM lParam)函数内被调用；
		LRESULT FilterDynamicLayoutMessage(UINT& message, WPARAM& wParam, LPARAM& lParam);
		
	public://添加布局
		void InitLayout(CWnd* pOwner);//初始化布局
		void AddPanel  (Panel Name, Panel Owner=_T(""),Layout top=LAYOUT_LOCK,Layout bottom=LAYOUT_UNLOCK,Layout left=LAYOUT_LOCK,Layout right=LAYOUT_UNLOCK);//添加Panel
		void AddControl(UINT nID,   Panel Owner=_T(""),Layout top=LAYOUT_LOCK,Layout bottom=LAYOUT_UNLOCK,Layout left=LAYOUT_LOCK,Layout right=LAYOUT_UNLOCK);//添加控件ID
		void AddControl(CWnd* pCtrl,Panel Owner=_T(""),Layout top=LAYOUT_LOCK,Layout bottom=LAYOUT_UNLOCK,Layout left=LAYOUT_LOCK,Layout right=LAYOUT_UNLOCK);//添加控件CWnd*
		BOOL IsPanelExist(Panel Name);//是否存在Panel已命名为Name;
		void SetSplit  (Panel Name, BOOL bAdd = TRUE );//为指定的Panel设置/移除分割条
		void UpdateLayout();//更新布局

	public://属性设置
		void SetDialogMinSize(int cx, int cy);//对话框最小大小
		void SetDialogMaxSize(int cx, int cy);//对话框最大大小
		void ShowLayout(Panel Name, BOOL bShow = TRUE );	//显示/隐藏布局
		void ShowPanel( Panel Name, BOOL bShow = TRUE );	//是否显示Panel边框和背景色
		void SetBkColor(Panel Name, COLORREF dwColor=1);	//设置背景颜色

	private://消息处理
		CLayoutPanel* GetPanel( Panel Name );
		void OnLButtonDown(int x, int y);
		void OnMouseMove(int x, int y );
		void OnLButtonUp(int x, int y);
		void OnPaint();
		static UINT WhenEndSplit(LPVOID lParam);
		
	private://关键属性
		CWnd*		m_wndOwner;		//拥有者对话框
		CSize		m_szMax;		//对话框最大大小
		CSize		m_szMin;		//对话框最小大小
		
		CMapNamePanel	m_mapPanel;		//Panel映射
		CMapSplit		m_mapSplit;		//分割条映射
		CLayoutPanelFrame*	m_pMainPanel;//对话框主面板

	};
}


#endif // !defined(AFX_DYNAMICLAYOUTEXPORT_H__6EAF03A9_8360_4AC8_8B1B_E7CDC0086992__INCLUDED_)
