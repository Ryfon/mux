﻿// LayoutPanelFrame.h: interface for the CLayoutPanelFrame class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYOUTPANELFRAME_H__7720EC2F_4321_4178_99E3_B9FA503F96D9__INCLUDED_)
#define AFX_LAYOUTPANELFRAME_H__7720EC2F_4321_4178_99E3_B9FA503F96D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LayoutPanel.h"

//
//	用作管理一个对话框的布局，自身边框不可见；
//	每个使用DynamicLayou的对话框，有且仅有一个CDynamicLayoutFrame对象,
//	Yangzj 2007.09.16 11：02
namespace DynamicLayout
{
	class CLayoutPanelFrame : public CLayoutPanel 
	{
	public:
		CLayoutPanelFrame(CWnd* wndOwner,Layout t,Layout b,Layout l,Layout r);
		~CLayoutPanelFrame();
	public:
		void SetRect(CRect& rect);
		void UpdateLayout();
		void Invalidate();
	};
}


#endif // !defined(AFX_LAYOUTPANELFRAME_H__7720EC2F_4321_4178_99E3_B9FA503F96D9__INCLUDED_)
