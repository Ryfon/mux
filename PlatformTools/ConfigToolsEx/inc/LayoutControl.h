﻿// LayoutControl.h: interface for the CLayoutControl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYOUTCONTROL_H__A3252BC3_7750_42F0_9573_DDB0AE55B6DC__INCLUDED_)
#define AFX_LAYOUTCONTROL_H__A3252BC3_7750_42F0_9573_DDB0AE55B6DC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LayoutBase.h"
namespace DynamicLayout
{
	class CLayoutControl : public CLayoutBase
	{
	public:
		CLayoutControl(CWnd* wndOwner,CWnd* pCtrl,Layout t,Layout b,Layout l,Layout r);
		void	GetRect(CRect& rect);
		void	SetRect(CRect& rect);
		void	UpdateLayout();
		void	Invalidate();
		void	Show(BOOL bShow=TRUE);

		BOOL	IsVisible(){return m_pThis->IsWindowVisible();}
		BOOL	IsPanel(){return FALSE;}
	protected:
		CWnd*	m_pThis;
	};
}


#endif // !defined(AFX_LAYOUTCONTROL_H__A3252BC3_7750_42F0_9573_DDB0AE55B6DC__INCLUDED_)
