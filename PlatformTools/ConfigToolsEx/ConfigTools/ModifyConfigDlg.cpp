﻿// ModifyConfigDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ConfigTools.h"
#include "ModifyConfigDlg.h"


// CModifyConfigDlg 对话框

IMPLEMENT_DYNAMIC(CModifyConfigDlg, CDialog)

CModifyConfigDlg::CModifyConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModifyConfigDlg::IDD, pParent)
	, m_sModifyKey(_T(""))
	, m_sModifyValute(_T(""))
{

}

CModifyConfigDlg::~CModifyConfigDlg()
{
}

void CModifyConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MODIFY_STATIC, m_sModifyKey);
	DDX_Text(pDX, IDC_EDIT1, m_sModifyValute);
}


BEGIN_MESSAGE_MAP(CModifyConfigDlg, CDialog)
END_MESSAGE_MAP()


// CModifyConfigDlg 消息处理程序


