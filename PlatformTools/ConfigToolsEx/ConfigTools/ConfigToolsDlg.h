﻿// ConfigToolsDlg.h : header file
//

#pragma once

#include "DynamicLayoutExport.h"
#include "afxcmn.h"


#include "TrayIcon.h "
#include "EditListCtrl.h"

#include "MutiTreeCtrl.h"
#include "TinyXML/tinyxml.h"


struct stPARENTS
{
	enum { NUKNOW = 0 , GLOBAL ,MERGE , }  enConfigType ;
	enum { INVALID = 0, VAILD , } enIsVaild ;
};

struct stMERGE : public stPARENTS
{
	CString sMergeTag ;
	CString sMergeSource ;
	CString sMergePageTag ;
	CString sHeadTagRow ;
	CString sExportColumn ;
	CString sMergeCurName ;
};

struct stGLOBLE : public stPARENTS
{
	CString sExportName ;
	CString sOptionChsName ;
	CString sFileTag ;	
	CString sFileVersion ;
};

// CConfigToolsDlg dialog
class CConfigToolsDlg : public CDialog
{
// Construction
public:
	CConfigToolsDlg(CWnd* pParent = NULL);	// standard constructor

	LRESULT OnTrayNotification(WPARAM wParam,LPARAM lParam);

// Dialog Data
	enum { IDD = IDD_CONFIGTOOLS_DIALOG };

	#define COLUMNS_m_ColListCtrl 3
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	virtual BOOL PreTranslateMessage(MSG* pMsg) ;

// Implementation
protected:
	HICON m_hIcon;

	CTrayIcon	m_TrayIcon;		// my tray icon
	int			m_iWhichIcon;	// 0/1 which HICON to use
	BOOL			m_bShutdown;	// OK to terminate TRAYTEST
	BOOL			m_bShowTrayNotifications;	// display info in main window

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	
private:
	DynamicLayout::CDynamicLayout m_DLayout ;

	CImageList m_imgList;
	CImageList m_imgState;
	CMutiTreeCtrl m_ConfigTree;
	CEditListCtrl m_lstConfigContent;
	CColorListCtrl m_lstLog;


	CString m_sExportDir ;

	HTREEITEM m_hBeCopyItem ;

	HTREEITEM AddSubItem(HTREEITEM item,BOOL bFlag,char* szName,DWORD_PTR pData) ;

	void SetGlobalData(stGLOBLE* pGlobal,TiXmlElement *ParnterElement);
	void SetMergeData(stMERGE* pMerge,TiXmlElement *ParnterElement);

	void LoadConfigXml() ;

	bool CheckMergeStruct(CString sName,CString sVal) ;

	BOOL ExportConfigCXml() ;
	BOOL BuildConfigXMLFromMergeTree(HTREEITEM item,TiXmlElement *pFileTage)  ;
	BOOL BuildConfigXML(HTREEITEM item) ;


	deque<int> splitExportColumnStr(CString sExportColumn) ;
	void LogInfo(CString sInfo,CString sState,BOOL bIsCol = FALSE) ;
	void SetInvaild(HTREEITEM item,BOOL IsInVaild=false) ; //false 失效 true 有效

	BOOL CopyItemTo(HTREEITEM hCurItem,HTREEITEM hObjItem) ;

	BOOL CheckPath(LPCTSTR lpszPath) ;
	int StrToInt(CString s) ;
	BOOL ParseExportColumn(CString sExportColumn,deque<int> &dqExportColumn) ;
public:
	
protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

public:
	afx_msg void OnClose();
	afx_msg void OnAppSuspend();
	afx_msg void OnNMRclickConfigTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnAddConfigContent();
	afx_msg void OnTvnSelchangedConfigTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedModifiyButton();
	afx_msg void OnBnClickedSaveButton();
	afx_msg void OnViewMainWnd();
	afx_msg void OnBnClickedImmExportButton();
	afx_msg void OnDelConfigNode();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetInvaild();
	afx_msg void OnSysSetup();

	afx_msg void OnTvnKeydownConfigTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMReturnConfigList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};
