﻿// ConfigToolsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigTools.h"

#include "ModifyConfigDlg.h"
#include "ConfigToolsDlg.h"

#include <io.h>
#include <errno.h>

#include "comdef.h"
#include "excel9.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_MY_TRAY_NOTIFICATION WM_USER+10

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CConfigToolsDlg dialog




CConfigToolsDlg::CConfigToolsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigToolsDlg::IDD, pParent),m_lstConfigContent(COLUMNS_m_ColListCtrl-1),m_lstLog(COLUMNS_m_ColListCtrl),m_hBeCopyItem(NULL)
{
	m_bShutdown = false ;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CConfigToolsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CONFIG_TREE, m_ConfigTree);
	DDX_Control(pDX, IDC_CONFIG_LIST, m_lstConfigContent);
	DDX_Control(pDX, IDC_LOG_LIST, m_lstLog);
}

BEGIN_MESSAGE_MAP(CConfigToolsDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP	
	ON_WM_CLOSE()
	ON_COMMAND(ID__32773, &CConfigToolsDlg::OnAppSuspend)
	ON_NOTIFY(NM_RCLICK, IDC_CONFIG_TREE, &CConfigToolsDlg::OnNMRclickConfigTree)
	ON_COMMAND(ID__32774, &CConfigToolsDlg::OnAddConfigContent)
	ON_NOTIFY(TVN_SELCHANGED, IDC_CONFIG_TREE, &CConfigToolsDlg::OnTvnSelchangedConfigTree)
	ON_BN_CLICKED(IDC_MODIFIY_BUTTON, &CConfigToolsDlg::OnBnClickedModifiyButton)
	ON_BN_CLICKED(IDC_SAVE_BUTTON, &CConfigToolsDlg::OnBnClickedSaveButton)
	ON_COMMAND(ID_SHOW_WINDOW, &CConfigToolsDlg::OnViewMainWnd)
	ON_MESSAGE(WM_MY_TRAY_NOTIFICATION, OnTrayNotification)
	ON_BN_CLICKED(IDC_IMM_EXPORT_BUTTON, &CConfigToolsDlg::OnBnClickedImmExportButton)
	ON_COMMAND(ID_DEL_CONFIG_NODE, &CConfigToolsDlg::OnDelConfigNode)
	ON_WM_SHOWWINDOW()
	ON_WM_GETMINMAXINFO()
	ON_COMMAND(ID_SET_INVAILD, &CConfigToolsDlg::OnSetInvaild)
	ON_COMMAND(ID_SYS_SETUP, &CConfigToolsDlg::OnSysSetup)
	ON_NOTIFY(TVN_KEYDOWN, IDC_CONFIG_TREE, &CConfigToolsDlg::OnTvnKeydownConfigTree)
	ON_NOTIFY(NM_RETURN, IDC_CONFIG_LIST, &CConfigToolsDlg::OnNMReturnConfigList)
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CConfigToolsDlg message handlers

BOOL CConfigToolsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	CStringArray Head;
	CByteArray   Cols;
	Head.Add("名称");
	Cols.Add(40);       //40%
	Head.Add("内容");
	Cols.Add(60);       //70%	
	m_lstConfigContent.InitCtrl(&Head, &Cols);

	m_lstConfigContent.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, (LPARAM)LVS_EX_FULLROWSELECT); 
	
	Head.RemoveAll() ;
	Cols.RemoveAll() ;

	Head.Add("行为");
	Cols.Add(40);       //40%
	Head.Add("状态");
	Cols.Add(30);       //70%
	Head.Add("时间");
	Cols.Add(30);       //100%
	
	m_lstLog.InitCtrl(&Head, &Cols);	
	m_lstLog.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, (LPARAM)LVS_EX_FULLROWSELECT); 
	
	m_DLayout.InitLayout( this );

	m_DLayout.SetDialogMaxSize( 800,600 );
	m_DLayout.SetDialogMinSize( 200,100 );

	m_DLayout.AddPanel("上","",1,1,1,1);
	m_DLayout.AddPanel("下","",0,1,1,1);

	m_DLayout.AddPanel("上左","上",1,1,1,0);
	m_DLayout.AddControl(IDC_CONFIG_TREE,"上左",1,1,1,1);
	

	m_DLayout.AddPanel("上右","上",1,1,1,1);
	m_DLayout.AddControl(IDC_CONFIG_LIST,"上右",1,1,1,1);
	m_DLayout.AddPanel("上右下","上右",0,1,1,0);
	m_DLayout.AddControl(IDC_IMM_EXPORT_BUTTON,"上右下",1,1,1,0);
	m_DLayout.AddControl(IDC_MODIFIY_BUTTON,"上右下",1,1,1,0);
	m_DLayout.AddControl(IDC_SAVE_BUTTON,"上右下",1,1,1,0);

	m_DLayout.AddControl(IDC_LOG_LIST,"下",1,1,1,1);

	m_DLayout.ShowPanel("",TRUE);
	m_DLayout.SetSplit("");
	m_DLayout.SetSplit("上");

	// Set up tray icon
	m_iWhichIcon = 1;
	/*m_trayIcon.SetNotificationWnd(this, WM_MY_TRAY_NOTIFICATION);
	m_trayIcon.SetIcon(IDR_MAINFRAME);*/
	//
	m_imgState.Create(IDB_BITMAP_STATE,13, 1, RGB(255,255,255));
	m_imgList.Create(IDB_BITMAP_LIST,16, 1, RGB(255,255,255));

	m_ConfigTree.SetImageList(&m_imgList,TVSIL_NORMAL);
	m_ConfigTree.SetImageList(&m_imgState,TVSIL_STATE);

	TV_INSERTSTRUCT tvinsert;
	tvinsert.hParent=NULL;
	tvinsert.hInsertAfter=TVI_LAST;
	tvinsert.item.mask=TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_TEXT|TVIF_STATE;
	tvinsert.item.hItem=NULL;
	tvinsert.item.state=INDEXTOSTATEIMAGEMASK( 1 );
	tvinsert.item.stateMask=TVIS_STATEIMAGEMASK;
	tvinsert.item.cchTextMax=6;
	tvinsert.item.iSelectedImage=1;
	tvinsert.item.cChildren=0;
	tvinsert.item.lParam=0;

	tvinsert.item.pszText="配置文件根目录";
	tvinsert.item.iImage=0;
	HTREEITEM hRoot=m_ConfigTree.InsertItem(&tvinsert);

	m_ConfigTree.SetItemState( hRoot, INDEXTOSTATEIMAGEMASK(1), TVIS_STATEIMAGEMASK );
		
	LoadConfigXml() ;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CConfigToolsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{		
		if (nID==61472)
		{
			m_TrayIcon.Create(this,WM_MY_TRAY_NOTIFICATION,"鼠标指向时显示",m_hIcon,IDR_TRAYICON); //构造
			ShowWindow(SW_HIDE); //隐藏窗口		
		}else
			CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CConfigToolsDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CConfigToolsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



LRESULT CConfigToolsDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your specialized code here and/or call the base class
	m_DLayout.FilterDynamicLayoutMessage(message,wParam,lParam);
	return CDialog::WindowProc(message, wParam, lParam);
}


LRESULT CConfigToolsDlg::OnTrayNotification(WPARAM wParam,LPARAM lParam)
{
	return m_TrayIcon.OnTrayNotification(wParam,lParam);
}

void CConfigToolsDlg::OnClose()
{
	m_TrayIcon.RemoveIcon();
	CDialog::OnClose();

	// TODO: Add your message handler code here and/or call default
	
	
}

void CConfigToolsDlg::OnAppSuspend()
{
	// TODO: Add your command handler code here
	m_bShutdown = TRUE;		// really exit
	SendMessage(WM_CLOSE);	
}

void CConfigToolsDlg::OnNMRclickConfigTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	CPoint pos(0,0) ;
	
	UINT nFlag = 0;

	HTREEITEM selItem = m_ConfigTree.GetSelectedItem() ;
	
	CMenu menu,*pSubMenu ;
	

	if (!menu.LoadMenu(IDR_MODIFIYCONFIG_MENU)) return ;
	if (!(pSubMenu=menu.GetSubMenu(0))) return ;

	if (selItem != m_ConfigTree.GetRootItem())
	{
		stPARENTS* p =  (stPARENTS*)m_ConfigTree.GetItemData(selItem) ;	
		if (p->enIsVaild==stPARENTS::INVALID)
			pSubMenu->ModifyMenu(ID_SET_INVAILD,MF_STRING,ID_SET_INVAILD,"设置为有效") ;
		else
			pSubMenu->ModifyMenu(ID_SET_INVAILD,MF_STRING,ID_SET_INVAILD,"设置为无效") ;
	}

	::SetMenuDefaultItem(pSubMenu->m_hMenu,3,TRUE) ;
	SetForegroundWindow() ;
	GetCursorPos(&pos) ;
	
	pSubMenu->TrackPopupMenu(TPM_LEFTALIGN|TPM_BOTTOMALIGN|TPM_LEFTBUTTON,pos.x,pos.y,this) ;
	menu.DestroyMenu() ;
	
	*pResult = 0;
}

void CConfigToolsDlg::OnAddConfigContent()
{
	// TODO: 在此添加命令处理程序代码
	HTREEITEM selItem = m_ConfigTree.GetSelectedItem() ;
	stPARENTS* pParent = (stPARENTS*)m_ConfigTree.GetItemData(selItem) ;
	if (selItem== m_ConfigTree.GetRootItem())
	{
		
		stGLOBLE* pGloble = new stGLOBLE ;
		pGloble->sExportName = "" ;
		pGloble->sFileTag = "" ;
		pGloble->sOptionChsName = "" ;
		pGloble->sFileVersion = "" ;
		pGloble->enConfigType = stPARENTS::GLOBAL ; 
		pGloble->enIsVaild = stPARENTS::VAILD ;
		AddSubItem(selItem,TRUE,"Global",(DWORD_PTR)pGloble) ;
		
	}else
	if( (pParent) && (pParent->enConfigType == stPARENTS::GLOBAL))
	//if (m_ConfigTree.GetItemText(selItem)=="Global")
	{
		
		stMERGE* pMerge = new stMERGE ;
		pMerge->sExportColumn = "" ;
		pMerge->sHeadTagRow = "" ;
		pMerge->sMergeSource = "" ;
		pMerge->sMergePageTag = "" ;
		pMerge->sMergeTag = "" ;
		pMerge->sMergeCurName = "" ;
		pMerge->enConfigType = stPARENTS::MERGE ; 		
		pMerge->enIsVaild = stPARENTS::VAILD ;
		AddSubItem(selItem,FALSE,"Merge",(DWORD_PTR)pMerge) ;		
	}


}

HTREEITEM CConfigToolsDlg::AddSubItem(HTREEITEM item,BOOL bFlag,char* szName,DWORD_PTR pData)
{
	TV_INSERTSTRUCT tvinsert;
	tvinsert.hParent=item;
	tvinsert.hInsertAfter=TVI_LAST;
	tvinsert.item.mask=TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_TEXT|TVIF_STATE;
	tvinsert.item.hItem=NULL;
	
	tvinsert.item.stateMask=TVIS_STATEIMAGEMASK;
	tvinsert.item.cchTextMax=6;
	tvinsert.item.iSelectedImage=1;
	tvinsert.item.cChildren=0;
	tvinsert.item.lParam=0;

	

	tvinsert.item.pszText= szName ;
	tvinsert.item.iImage=0;
	HTREEITEM hResult = m_ConfigTree.InsertItem(&tvinsert);
	m_ConfigTree.SetItemData(hResult,pData) ;
	m_ConfigTree.Expand(item,TVE_EXPAND) ;
	m_ConfigTree.SelectItem(hResult) ;	

	if (bFlag)
	{
		tvinsert.item.state=INDEXTOSTATEIMAGEMASK( 1 );
		m_ConfigTree.SetItemState( hResult, INDEXTOSTATEIMAGEMASK(1), TVIS_STATEIMAGEMASK );
	}else
	{
		tvinsert.item.state=INDEXTOSTATEIMAGEMASK( 0 );
		m_ConfigTree.SetItemState( hResult, INDEXTOSTATEIMAGEMASK(0), TVIS_STATEIMAGEMASK );
	}

	return hResult ;
}

void CConfigToolsDlg::OnTvnSelchangedConfigTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	
	m_lstConfigContent.DeleteAllItems();
	TVITEM item = pNMTreeView->itemNew;
	if(item.hItem == m_ConfigTree.GetRootItem())
		return;
	CString Linetitle;
	CStringArray Head;
	stPARENTS* pParent = (stPARENTS*)m_ConfigTree.GetItemData(item.hItem) ;
	if( (pParent) && (pParent->enConfigType == stPARENTS::GLOBAL))
	//if (m_ConfigTree.GetItemText(item.hItem)=="Global")
	{
		stGLOBLE* pGloble = (stGLOBLE*)m_ConfigTree.GetItemData(item.hItem) ;
		if (!pGloble) return ;
		Head.RemoveAll();
		Linetitle = "ExportName" ;
		Head.Add(Linetitle);		
		Linetitle.Format("%s",pGloble->sExportName);
		Head.Add(Linetitle);	
			
		int pos=m_lstConfigContent.AddItem(&Head);
		pos++ ;
		
		Head.RemoveAll();
		Linetitle = "FileTag" ;
		Head.Add(Linetitle);		
		Linetitle.Format("%s",pGloble->sFileTag);
		Head.Add(Linetitle);			
		pos = m_lstConfigContent.AddItem(&Head,pos);
		pos++ ;
		Head.RemoveAll();
		Linetitle = "OptionChsName" ;
		Head.Add(Linetitle);		
		Linetitle.Format("%s",pGloble->sOptionChsName);
		Head.Add(Linetitle);			
		pos = m_lstConfigContent.AddItem(&Head,pos);

		pos++ ;
		Head.RemoveAll();
		Linetitle = "FileVersion" ;
		Head.Add(Linetitle);		
		Linetitle.Format("%s",pGloble->sFileVersion);
		Head.Add(Linetitle);			
		pos = m_lstConfigContent.AddItem(&Head,pos);


		m_lstConfigContent.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, (LPARAM)LVS_EX_FULLROWSELECT); 

		
		
	}else		
		if( (pParent) && (pParent->enConfigType == stPARENTS::MERGE))
		//if (m_ConfigTree.GetItemText(item.hItem)=="Merge")
		{
			stMERGE* pMerge = (stMERGE*)m_ConfigTree.GetItemData(item.hItem) ;
			if (!pMerge) return ;	

			Head.RemoveAll();
			Linetitle = "MergeTag" ;
			Head.Add(Linetitle);		
			Linetitle.Format("%s",pMerge->sMergeTag);
			Head.Add(Linetitle);				
			int pos=m_lstConfigContent.AddItem(&Head);
			pos++ ;

			Head.RemoveAll();			
			Linetitle = "MergeSource" ;
			Head.Add(Linetitle);		
			Linetitle.Format("%s",pMerge->sMergeSource);
			Head.Add(Linetitle);				
			pos = m_lstConfigContent.AddItem(&Head,pos);
			pos++ ;
			Head.RemoveAll();
			Linetitle = "MergePageTag" ;
			Head.Add(Linetitle);		
			Linetitle.Format("%s",pMerge->sMergePageTag);
			Head.Add(Linetitle);		
			pos = m_lstConfigContent.AddItem(&Head,pos);
			pos++ ;
			Head.RemoveAll();
			Linetitle = "sHeadTagRow" ;
			Head.Add(Linetitle);		
			Linetitle.Format("%s",pMerge->sHeadTagRow);
			Head.Add(Linetitle);		
			pos = m_lstConfigContent.AddItem(&Head,pos);
			pos++ ;
			Head.RemoveAll();
			Linetitle = "ExportColumn" ;
			Head.Add(Linetitle);		
			Linetitle.Format("%s",pMerge->sExportColumn);
			Head.Add(Linetitle);		
			pos = m_lstConfigContent.AddItem(&Head,pos);
			
			pos++ ;
			Head.RemoveAll();
			Linetitle = "MergeCurName" ;
			Head.Add(Linetitle);		
			Linetitle.Format("%s",pMerge->sMergeCurName);
			Head.Add(Linetitle);		
			pos = m_lstConfigContent.AddItem(&Head,pos);


			m_lstConfigContent.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, (LPARAM)LVS_EX_FULLROWSELECT); 

		}
		
	
	*pResult = 0;
}

void CConfigToolsDlg::OnBnClickedModifiyButton()
{
	//// TODO: 在此添加控件通知处理程序代码
	
}


BOOL CConfigToolsDlg::ExportConfigCXml() 
{
	// TODO: 在此添加控件通知处理程序代码
	DeleteFile("ExcelExport.xml") ;

	TiXmlDocument *myDocument = new TiXmlDocument();
	HTREEITEM hItem = m_ConfigTree.GetRootItem() ;

	//创建一个根元素并连接。
	TiXmlElement *RootElement = new TiXmlElement(m_ConfigTree.GetItemText(hItem));
	RootElement->SetAttribute("ExportDir",m_sExportDir);
	myDocument->LinkEndChild(RootElement);

	HTREEITEM hChildItem,hBrotherItem;

	//查找子节点，没有就结束
	hChildItem=m_ConfigTree.GetChildItem(hItem);

	TiXmlElement *pGlobalElement,* pMergeElement;
	stPARENTS* pParent = NULL ;
	while (hChildItem!=NULL)
	{
		pParent = (stPARENTS*)m_ConfigTree.GetItemData(hChildItem) ;		
		if (pParent->enConfigType == stPARENTS::GLOBAL)
		{
			pGlobalElement = new TiXmlElement("Global");

			stGLOBLE* pGlobal = (stGLOBLE*)m_ConfigTree.GetItemData(hChildItem)  ;
			SetGlobalData(pGlobal,pGlobalElement) ;
		}else if (pParent->enConfigType == stPARENTS::MERGE)
		{
			pGlobalElement = new TiXmlElement("Merge");
			stMERGE* pMerge = (stMERGE*)m_ConfigTree.GetItemData(hChildItem)  ;
			SetMergeData(pMerge,pGlobalElement) ;
		}
		RootElement->LinkEndChild(pGlobalElement);

		hBrotherItem=m_ConfigTree.GetChildItem(hChildItem);
		while(hBrotherItem!=NULL)
		{
			pParent = (stPARENTS*)m_ConfigTree.GetItemData(hBrotherItem) ;	
			if (pParent->enConfigType == stPARENTS::GLOBAL)
			{
				pMergeElement = new TiXmlElement("Global");

				stGLOBLE* pGlobal = (stGLOBLE*)m_ConfigTree.GetItemData(hBrotherItem)  ;
				SetGlobalData(pGlobal,pMergeElement) ;
			}else if (pParent->enConfigType == stPARENTS::MERGE)
			{
				pMergeElement = new TiXmlElement("Merge");
				stMERGE* pMerge = (stMERGE*)m_ConfigTree.GetItemData(hBrotherItem)  ;
				SetMergeData(pMerge,pMergeElement) ;
			}
			pGlobalElement->LinkEndChild(pMergeElement) ;
			hBrotherItem=m_ConfigTree.GetNextSiblingItem(hBrotherItem);
		}
		hChildItem=m_ConfigTree.GetNextSiblingItem(hChildItem);
	}
	myDocument->SaveFile("demotest.xml");
	return true ;
}

int CConfigToolsDlg::StrToInt(CString s)
{
	CString str = s ;
	int Value = 0 ,Result = 0 , Index = 0 ;
	char Chr ;	
	while (str.GetLength()>0)
	{
		Chr = str.GetAt(str.GetLength()-1) ;
		Value = 1 ;
		for (int i=0;i<Index;++i)
			Value*=26 ;
		Value *=(Chr-'a'+1) ;
		Result += Value ;
		str.Delete(str.GetLength()-1) ;
		++Index ;
	}
	return Result ;
}

BOOL CConfigToolsDlg::ParseExportColumn(CString sExportColumn,deque<int> &dqExportColumn)
{
	dqExportColumn.clear() ;

	CString sTmp = "" ;
	CString  str = sExportColumn ;
	if (str.GetLength()<1) return false ;
	if (str.Right(1) != ',') str += ',' ;
	str.MakeLower() ;
	char Chr ;
	BOOL bIsLink = FALSE ;
	enum {enUnknow = 0 , enNum,enChar ,} enType = enUnknow ;
	
	for (int i=0; i<str.GetLength();++i)
	{
		Chr = str.GetAt(i) ;
		if ((Chr>= 'a') && (Chr <='z'))
		{
			if (enType == enNum) return FALSE ;
			enType = enChar ;			
		}else if ((Chr>= '0') && (Chr <='9'))
		{
			if (enType == enChar) return FALSE ;
			enType = enNum ;			
		}else if ((Chr == ',') || (Chr == '_'))
		{
			if (enType == enChar)  sTmp.Format("%d", StrToInt(sTmp));
			
			if ((dqExportColumn.size()>0) && (bIsLink))
			{
				int nFirst = dqExportColumn[dqExportColumn.size()-1] ;
				int nLast = ::atoi(sTmp) ;				
				if (nFirst >= nLast ) return FALSE ;
				for (int i=nFirst+1;i<=nLast;++i )
					dqExportColumn.push_back(i) ;
				bIsLink=FALSE ;
			}else
			{
				dqExportColumn.push_back(::atoi(sTmp)) ;				
			}	
			if(Chr=='_') bIsLink=TRUE ;
			sTmp = "" ;
			enType = enUnknow ;
			continue ;			
		}else return FALSE ;
		sTmp += Chr ;
	}
	return TRUE ;
}


bool CConfigToolsDlg::CheckMergeStruct(CString sName,CString sVal) 
{
	bool bFlag = TRUE ;
	CString s ;
	deque<int> dqExportColumn ;
	if ("ExportColumn" == sName)
		if(!ParseExportColumn(sVal,dqExportColumn)) return FALSE ;	
		
	if ("MergePageTag" == sName)
	{
		if (sVal.GetLength() < 1)
		{
			bFlag =FALSE ;
		}
	}
	return bFlag ;

}


void CConfigToolsDlg::OnBnClickedSaveButton()
{
	// TODO: 在此添加控件通知处理程序代码
	CString Linetitle;
	CStringArray Head;
	Linetitle = "保存ConfigXml文件"	 ;
	Head.Add(Linetitle) ;

	HTREEITEM selItem = m_ConfigTree.GetSelectedItem() ;
	stPARENTS* pParent = (stPARENTS*)m_ConfigTree.GetItemData(selItem) ;
	if( (pParent) && (pParent->enConfigType == stPARENTS::GLOBAL))
	//if (m_ConfigTree.GetItemText(selItem)=="Global")
	{	
		stGLOBLE* pGloble = (stGLOBLE*)m_ConfigTree.GetItemData(selItem) ;
		if (!pGloble) return ;
		
		pGloble->sExportName = m_lstConfigContent.GetItemText(0,1) ;
		pGloble->sFileTag = m_lstConfigContent.GetItemText(1,1) ;
		pGloble->sOptionChsName = m_lstConfigContent.GetItemText(2,1) ;
		pGloble->sFileVersion =  m_lstConfigContent.GetItemText(3,1) ;
				 
		if((pGloble->sOptionChsName).GetLength()>0)
			m_ConfigTree.SetItemText(selItem,pGloble->sOptionChsName) ;
		

	}else
		if( (pParent) && (pParent->enConfigType == stPARENTS::MERGE))
		//if (m_ConfigTree.GetItemText(selItem)=="Merge")
		{
			if (!CheckMergeStruct("ExportColumn",m_lstConfigContent.GetItemText(4,1)))
			{
				Linetitle = "失败(ExportColumn 非法)";
				Head.Add(Linetitle) ;				
				CTime t = CTime::GetCurrentTime();
				Linetitle.Format("%d-%d-%d %d:%d:%d",
					t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond()) ;
				Head.Add(Linetitle) ;
							
				m_lstLog.AddItem(&Head,0) ;
				return ;
			}
			if (!CheckMergeStruct("MergePageTag",m_lstConfigContent.GetItemText(2,1)))
			{
				Linetitle = "失败(MergePageTag 不能为空)";
				Head.Add(Linetitle) ;				
				CTime t = CTime::GetCurrentTime();
				Linetitle.Format("%d-%d-%d %d:%d:%d",
					t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond()) ;
				Head.Add(Linetitle) ;

				m_lstLog.AddItem(&Head,0) ;
				return ;
			}
			stMERGE* pMerge = (stMERGE*)m_ConfigTree.GetItemData(selItem) ;
			if (!pMerge) return ;	
			pMerge->sMergeTag = m_lstConfigContent.GetItemText(0,1) ;		
			pMerge->sMergeSource = m_lstConfigContent.GetItemText(1,1) ;
			pMerge->sMergePageTag = m_lstConfigContent.GetItemText(2,1) ;						
			pMerge->sHeadTagRow = m_lstConfigContent.GetItemText(3,1) ;
			pMerge->sExportColumn = m_lstConfigContent.GetItemText(4,1) ;
			pMerge->sMergeCurName = m_lstConfigContent.GetItemText(5,1) ;

			if((pMerge->sMergeCurName).GetLength()>0)
			{
				CString str = pMerge->sMergeCurName ;
				m_ConfigTree.SetItemText(selItem,str) ;
			}
		}	

		ExportConfigCXml() ;
		Linetitle = "成功";
		Head.Add(Linetitle) ;				
		CTime t= CTime::GetCurrentTime();
		Linetitle.Format("%d-%d-%d %d:%d:%d",t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond()) ;
		Head.Add(Linetitle) ;

		m_lstLog.AddItem(&Head,0) ;
}

void CConfigToolsDlg::OnViewMainWnd()
{
	// TODO: 在此添加命令处理程序代码

	CMenu menu ;

	if (!menu.LoadMenu(IDR_TRAYICON)) return ;	

	if (IsWindowVisible())
	{		
		ShowWindow(SW_SHOWMINIMIZED) ;
		ShowWindow(SW_HIDE) ;		
	}else
	{
		ShowWindow(SW_SHOW) ;
		ShowWindow(SW_RESTORE) ;
		m_TrayIcon.RemoveIcon();	
	}
	SetForegroundWindow();
}

void CConfigToolsDlg::SetGlobalData(stGLOBLE* pGlobal,TiXmlElement *ParnterElement)
{	
	ParnterElement->SetAttribute("ExportName",pGlobal->sExportName) ;
	ParnterElement->SetAttribute("FileTag",pGlobal->sFileTag) ;
	ParnterElement->SetAttribute("OptionChsName",pGlobal->sOptionChsName) ;
	ParnterElement->SetAttribute("FileVersion",pGlobal->sFileVersion) ;
	ParnterElement->SetAttribute("IsVaild",pGlobal->enIsVaild) ;

}
void CConfigToolsDlg::SetMergeData(stMERGE* pMerge,TiXmlElement *ParnterElement)
{
	ParnterElement->SetAttribute("ExportColumn",pMerge->sExportColumn) ;
	ParnterElement->SetAttribute("HeadTagRow",pMerge->sHeadTagRow) ;
	ParnterElement->SetAttribute("MergeSource",pMerge->sMergeSource) ;
	ParnterElement->SetAttribute("MergePageTag",pMerge->sMergePageTag) ;
	ParnterElement->SetAttribute("MergeTag",pMerge->sMergeTag) ;
	ParnterElement->SetAttribute("MergeCurName",pMerge->sMergeCurName) ;
	ParnterElement->SetAttribute("IsVaild",pMerge->enIsVaild) ;
}


void CConfigToolsDlg::LoadConfigXml() 
{
	HTREEITEM hItem = m_ConfigTree.GetRootItem() ;
	TiXmlDocument myDocument("demotest.xml") ;
	BOOL bFlag = myDocument.LoadFile() ;
	if (!bFlag) return ;
	
	TiXmlElement* Root = myDocument.RootElement() ;
	if (!Root) return ;
	m_sExportDir = Root->Attribute("ExportDir") ;
	TiXmlElement* FirstLevel = Root->FirstChildElement() ;
	TiXmlElement* SecendLevel ;
	stGLOBLE* pGlobal  = NULL ;
	stMERGE * pMerge = NULL ;
	HTREEITEM hGlobalItem ,hMerge; 
	CString tmp ;
	while(FirstLevel)
	{
		pGlobal = new stGLOBLE ;	
		pGlobal->sExportName =  FirstLevel->Attribute("ExportName") ;
		pGlobal->sFileTag =  FirstLevel->Attribute("FileTag") ;
		pGlobal->sOptionChsName =  FirstLevel->Attribute("OptionChsName") ;
		pGlobal->sFileVersion =  FirstLevel->Attribute("FileVersion") ;
		tmp = FirstLevel->Attribute("IsVaild") ;

		pGlobal->enIsVaild = ( tmp == "0" ? stGLOBLE::INVALID : stGLOBLE::VAILD ) ;	
		
		pGlobal->enConfigType = stGLOBLE::GLOBAL ;		
		hGlobalItem = AddSubItem(hItem,TRUE,"Global",(DWORD_PTR)pGlobal) ;
		if ((pGlobal->sOptionChsName).GetLength()>0)
			m_ConfigTree.SetItemText(hGlobalItem,pGlobal->sOptionChsName) ;
		SecendLevel = FirstLevel->FirstChildElement() ;
		while(SecendLevel)
		{
			pMerge = new stMERGE ;
			pMerge->sExportColumn = SecendLevel->Attribute("ExportColumn") ;
			pMerge->sHeadTagRow = SecendLevel->Attribute("HeadTagRow") ;
			pMerge->sMergeSource = SecendLevel->Attribute("MergeSource") ;
			pMerge->sMergePageTag = SecendLevel->Attribute("MergePageTag") ;
			pMerge->sMergeTag = SecendLevel->Attribute("MergeTag") ;			 
			pMerge->sMergeCurName = SecendLevel->Attribute("MergeCurName") ;
			tmp = SecendLevel->Attribute("IsVaild") ;
			pMerge->enIsVaild = (tmp == "0"  ? stMERGE::INVALID : stMERGE::VAILD) ;	
			pMerge->enConfigType = stMERGE::MERGE ;			
			hMerge = AddSubItem(hGlobalItem,FALSE,"Merge",(DWORD_PTR)pMerge) ;
			CString str = "Merge" ;
			if ((pMerge->sMergeCurName).GetLength()>0)
				str = pMerge->sMergeCurName ;

			m_ConfigTree.SetItemText(hMerge,str) ;
			SecendLevel	= SecendLevel->NextSiblingElement();
		}
		FirstLevel = FirstLevel->NextSiblingElement() ;
	}

}
void CConfigToolsDlg::OnDelConfigNode()
{
	// TODO: 在此添加命令处理程序代码
	HTREEITEM selItem = m_ConfigTree.GetSelectedItem() ;
	/*CPoint nPt ;
	UINT nFlag ;

	HTREEITEM selItem = m_ConfigTree.HitTest(nPt,&nFlag);*/
	HTREEITEM hNext = NULL ;
	stPARENTS* pParent = (stPARENTS*)m_ConfigTree.GetItemData(selItem) ;
	if( (pParent) && (pParent->enConfigType == stPARENTS::GLOBAL))
	//if (m_ConfigTree.GetItemText(selItem)=="Global")
	{	
		stGLOBLE* pGloble = (stGLOBLE*)m_ConfigTree.GetItemData(selItem) ;
		if (!pGloble) return ;
		delete pGloble ;	pGloble = NULL ;
		HTREEITEM hMerge = m_ConfigTree.GetChildItem(selItem) ;
		while (hMerge)
		{
			stMERGE* pMerge = (stMERGE*)m_ConfigTree.GetItemData(hMerge) ;
			if (pMerge) {	delete pMerge ;	pMerge = NULL ;}
			m_ConfigTree.DeleteItem(hMerge) ;
			hMerge = NULL ;
			hMerge = m_ConfigTree.GetChildItem(selItem) ;
		}
		hNext = m_ConfigTree.GetNextSiblingItem(selItem) ;
		if (!hNext) hNext = m_ConfigTree.GetRootItem() ;		
		m_ConfigTree.DeleteItem(selItem) ;
	}else
		if( (pParent) && (pParent->enConfigType == stPARENTS::MERGE))
		//if (m_ConfigTree.GetItemText(selItem)=="Merge")
		{
			stMERGE* pMerge = (stMERGE*)m_ConfigTree.GetItemData(selItem) ;
			if (pMerge) {	delete pMerge ;	pMerge = NULL ;} 
			hNext = m_ConfigTree.GetParentItem(selItem) ;
			m_ConfigTree.DeleteItem(selItem) ;
			selItem = NULL ;			
		}
	if (hNext)
		m_ConfigTree.SelectItem(hNext) ;
	else
		hNext = m_ConfigTree.GetRootItem() ;
	m_ConfigTree.Expand(hNext,TVE_EXPAND) ;
}

deque<int> CConfigToolsDlg::splitExportColumnStr(CString sExportColumn)
{
	deque<int> split ;
	split.clear() ;

	CString s = sExportColumn ;
	int i = s.Find(',') > s.Find("，") ?  s.Find(',') : s.Find("，") ;	
	if (i>0)
	{
		while(i>0)
		{
			CString sub =  s.Left(i) ;	  
			int Value = atoi(sub.GetBuffer()) ;
			split.push_back(Value) ;		  
			s.Find("，")==i ? i+=2 : i++ ;
			s.Delete(0,i) ;
			i = s.Find(',') > s.Find("，") ?  s.Find(',') : s.Find("，") ;	
		}
	}else 
	{
		int Value = atoi(s.GetBuffer()) ;
		split.push_back(Value) ;					  
	}
	return split ;
}


BOOL CConfigToolsDlg::BuildConfigXMLFromMergeTree(HTREEITEM item,TiXmlElement *pFileTage) 
{
	stMERGE* pMerge = (stMERGE*)m_ConfigTree.GetItemData(item) ;
	//
	CString Linetitle;
	CStringArray Head;
	if (pMerge->enIsVaild==stMERGE::INVALID) return true ;	

	CFile f ;	
	if (!f.Open(pMerge->sMergeSource,CFile::readOnly)) {	

		Linetitle.Format("读取(%s)文件",pMerge->sMergeSource) ;
		LogInfo(Linetitle,"失败",true) ;
		return false ;
	}
	CString sFile = f.GetFilePath() ;
	f.Close() ;		
	deque<int> SplitExportColumn ;
	ParseExportColumn(pMerge->sExportColumn,SplitExportColumn) ;

	int nHeadTagRow = atoi(pMerge->sHeadTagRow) ;

	
	
	
	COleVariant
		covTrue((short)TRUE),
		covFalse((short)FALSE),
		covOptional((long)DISP_E_PARAMNOTFOUND, VT_ERROR);

	_Application ExcelApp; 
	Workbooks wbsMyBooks; 
	_Workbook wbMyBook; 
	Worksheets wssMysheets; 
	_Worksheet wsMysheet; 
	Range rgMyRge; 	
	
	if (!ExcelApp.CreateDispatch("Excel.Application",NULL)) 
	{ 
		AfxMessageBox("创建Excel服务失败!");		
		LogInfo("创建Excel服务","失败",true) ;
		return false ;
	} 
	ExcelApp.SetVisible(false); 	
	ExcelApp.SetUserControl(FALSE) ;
	wbsMyBooks.AttachDispatch(ExcelApp.GetWorkbooks()) ;
	LPDISPATCH  pDispatch = wbsMyBooks.Open(sFile,
					covOptional,covOptional,covOptional,covOptional,covOptional,
					covOptional,covOptional,covOptional,covOptional,covOptional,
					covOptional,covOptional) ;	
	wbMyBook.AttachDispatch(pDispatch,TRUE) ;
	wssMysheets.AttachDispatch(wbMyBook.GetWorksheets(),true) ;
	LPDISPATCH  pWorkSheet = NULL ;
	try
	{
		pWorkSheet = wssMysheets.GetItem(_variant_t(pMerge->sMergePageTag)) ;
	}
	catch (...)
	{
		Linetitle.Format("打开(%s)分页",pMerge->sMergePageTag) ;		
		LogInfo(Linetitle,"失败",true) ;

		wbMyBook.Close(covOptional,COleVariant(sFile),covOptional) ;
		wbsMyBooks.Close() ;
		rgMyRge.ReleaseDispatch() ;
		wsMysheet.ReleaseDispatch() ;
		wssMysheets.ReleaseDispatch() ;
		wbMyBook.ReleaseDispatch() ;
		wbsMyBooks.ReleaseDispatch() ;
		ExcelApp.ReleaseDispatch() ;

		return false ;
	}
	
	
	wsMysheet.AttachDispatch(pWorkSheet,true) ;
	
	Range range ;
	rgMyRge.AttachDispatch(wsMysheet.GetUsedRange(),true) ;
	range.AttachDispatch(rgMyRge.GetRows()) ;
	int iRowCount = range.GetCount() ;
	range.ReleaseDispatch() ;
	range.AttachDispatch(rgMyRge.GetColumns()) ;
	int iColCount = range.GetCount() ;
	range.ReleaseDispatch() ;

	deque<CString> strSplitExportColumn  ;
	strSplitExportColumn.clear() ;
	COleVariant vVal ; 
	CString strVal ;	

	for (int i=0;i<(int)SplitExportColumn.size();++i)
	{
		if (SplitExportColumn[i]<=iColCount)
		{
			range.AttachDispatch(rgMyRge.GetItem(COleVariant((long)nHeadTagRow),COleVariant((long)SplitExportColumn[i])).pdispVal) ;
			vVal =range.GetText() ; 
			strVal = vVal.bstrVal ;
			strSplitExportColumn.push_back(strVal) ;
		}else
		{
			//出错代码
		}
		
	}
	int nColNull = 0 ; 
	for (int j=nHeadTagRow+1;j<=iRowCount;++j)
	{
		nColNull = 0 ;
		TiXmlElement *MergeElement = new TiXmlElement(pMerge->sMergeTag);
		for (int i=0;i<(int)SplitExportColumn.size();++i)
		{
			
			if (SplitExportColumn[i]<=iColCount)
			{
				range.AttachDispatch(rgMyRge.GetItem(COleVariant((long)j),COleVariant((long)SplitExportColumn[i])).pdispVal) ;
				vVal =range.GetText() ; 
				strVal = vVal.bstrVal ;
				MergeElement->SetAttribute(strSplitExportColumn[i],strVal) ;
				if (strVal=="") nColNull++ ;				
			}else
			{
				//出错代码
			}
		}	
		if (SplitExportColumn.size()  == nColNull) 
		{
			delete MergeElement ;
			break ;
		}
		pFileTage->LinkEndChild(MergeElement) ;
	}
	
	wbMyBook.Close(covOptional,COleVariant(sFile),covOptional) ;
	wbsMyBooks.Close() ;
	rgMyRge.ReleaseDispatch() ;
	wsMysheet.ReleaseDispatch() ;
	wssMysheets.ReleaseDispatch() ;
	wbMyBook.ReleaseDispatch() ;
	wbsMyBooks.ReleaseDispatch() ;
	ExcelApp.ReleaseDispatch() ;

	return  TRUE ;
}

BOOL CConfigToolsDlg::BuildConfigXML(HTREEITEM item) 
{
	stGLOBLE* pGloble = (stGLOBLE*)m_ConfigTree.GetItemData(item) ;
	if (pGloble->enIsVaild == stGLOBLE::INVALID) return true ;
	
	CString stName = "";

	stName = m_sExportDir+pGloble->sExportName ;

	DeleteFile(stName) ;
	TiXmlDocument *myDocument = new TiXmlDocument();
	
	myDocument->Parse("<?xml version=1.0 encoding=gb2312 standalone=yes>") ;


	
	/*TiXmlElement *RootElement = new TiXmlElement("?xml");
	RootElement->SetAttribute("version","1.0") ;
	RootElement->SetAttribute("encoding","gb2312") ;
	RootElement->SetAttribute("standalone","yes") ;

	
	myDocument->LinkEndChild(RootElement);*/
	//建立根 
	//	
	TiXmlElement *pFileTage = new TiXmlElement(pGloble->sFileTag);
	pFileTage->SetAttribute("FileVersion",pGloble->sFileVersion) ;
	myDocument->LinkEndChild(pFileTage) ;
	//建立真正的内容
	CString Linetitle;
	Linetitle.Format("建立(%s)文件",stName) ;
	LogInfo(Linetitle,"开始") ;

	HTREEITEM hChildItem=m_ConfigTree.GetChildItem(item);
	while (hChildItem)
	{
		BuildConfigXMLFromMergeTree(hChildItem,pFileTage);
		hChildItem=m_ConfigTree.GetNextSiblingItem(hChildItem);
	}
	myDocument->SaveFile(stName);

	Linetitle.Format("建立(%s)文件",stName) ;
	LogInfo(Linetitle,"结束") ;
	

	return TRUE ;
}

void CConfigToolsDlg::OnBnClickedImmExportButton()
{
	HTREEITEM hItem = m_ConfigTree.GetRootItem() ;
	HTREEITEM hChildItem ;
	stPARENTS* pParent = NULL ;
	hChildItem=m_ConfigTree.GetChildItem(hItem);
	
	if(!CheckPath(m_sExportDir)) 
	{
		LogInfo("建立导出文件的路径","失败",1) ;
		return ;
	}

	while (hChildItem!=NULL)
	{
		if (m_ConfigTree.GetItemStateEx(hChildItem))
		{
			BuildConfigXML(hChildItem) ;
		}
		
		hChildItem=m_ConfigTree.GetNextSiblingItem(hChildItem);
	}

}
void CConfigToolsDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: 在此处添加消息处理程序代码
}

void CConfigToolsDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialog::OnGetMinMaxInfo(lpMMI);	
}

void CConfigToolsDlg::LogInfo(CString sInfo,CString sState,BOOL bIsCol) 
{
	CString Linetitle ;
	CStringArray Head;
	Head.Add(sInfo) ;
	Head.Add(sState) ;
	CTime t = CTime::GetCurrentTime();
	Linetitle.Format("%d-%d-%d %d:%d:%d",t.GetYear(),t.GetMonth(),t.GetDay(),t.GetHour(),t.GetMinute(),t.GetSecond()) ;
	Head.Add(Linetitle) ;
	m_lstLog.AddItem(&Head,0) ;
	if (bIsCol)
	{		
		m_lstLog.SetItemBackgndColor(RGB(100,100,100),0,0) ;
		m_lstLog.SetItemBackgndColor(RGB(100,100,100),0,1) ;
		m_lstLog.SetItemBackgndColor(RGB(100,100,100),0,2) ;

		m_lstLog.SetItemTextColor(RGB(255,0,0),0,0) ;
		m_lstLog.SetItemTextColor(RGB(255,0,0),0,1) ;
		m_lstLog.SetItemTextColor(RGB(255,0,0),0,2) ;

	}
	UpdateData() ;
	UpdateWindow() ;
}


void CConfigToolsDlg::SetInvaild(HTREEITEM item,BOOL IsInVaild) 
{

	stGLOBLE* pGlobal  = (stGLOBLE*)m_ConfigTree.GetItemData(item) ;
	if (IsInVaild)
	{
		m_ConfigTree.SetItemImage(item,0,1) ;
		pGlobal->enIsVaild = stGLOBLE::VAILD ;
	}else{
		m_ConfigTree.SetItemImage(item,2,1) ;
		pGlobal->enIsVaild = stGLOBLE::INVALID ;
	}
	
	HTREEITEM hItem = m_ConfigTree.GetChildItem(item) ;
	if (hItem)
	{
		SetInvaild(hItem,IsInVaild) ;
		HTREEITEM hBr = m_ConfigTree.GetNextSiblingItem(hItem) ;
		while (hBr)
		{			
			SetInvaild(hBr,IsInVaild) ;
			hBr = m_ConfigTree.GetNextSiblingItem(hBr) ;
		}
	}	
}

void CConfigToolsDlg::OnSetInvaild()
{
	
	HTREEITEM selItem = m_ConfigTree.GetSelectedItem() ;	
	
	stPARENTS* p =  (stPARENTS*)m_ConfigTree.GetItemData(selItem) ;


	if (p->enIsVaild==stPARENTS::VAILD)
	{
		SetInvaild(selItem) ;
		m_ConfigTree.Expand(selItem,TVE_COLLAPSE) ;
		selItem = m_ConfigTree.GetParentItem(selItem) ;
		if (selItem)
		{
			m_ConfigTree.SelectItem(selItem) ;	
		}else
			m_ConfigTree.SelectItem(m_ConfigTree.GetRootItem()) ;	
	}
	else
	{
		SetInvaild(selItem,true) ;
		m_ConfigTree.Expand(selItem,TVE_EXPAND) ;
		m_ConfigTree.SelectItem(selItem) ;	
	}
}

void CConfigToolsDlg::OnSysSetup()
{
	// TODO: 在此添加命令处理程序代码
	CModifyConfigDlg dlg ;
	dlg.m_sModifyKey =  "当前默认的导出目录" ;
	dlg.m_sModifyValute = m_sExportDir ;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		m_sExportDir = dlg.m_sModifyValute ;
		ExportConfigCXml() ;
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

}

BOOL CConfigToolsDlg::CheckPath(LPCTSTR lpszPath) 
{
	CFileStatus status ;
	string strPath = lpszPath ;
	string::size_type st ;
	if (strPath.size()==0)
		return false ;
	
	if (*strPath.rbegin() == '\\')
		strPath.resize(strPath.size()-1) ;
	if (CFile::GetStatus(strPath.c_str(),status) && (status.m_attribute & CFile::directory))
		return true;
	else
	{
		if (strPath.size()==2 && *strPath.rbegin()=='..')
			return TRUE ;
	}
	st = strPath.find_last_of('\\') ;
	if (st==string::npos) 
		return FALSE ;
	else
		strPath.erase(st) ;

	if (CheckPath(strPath.c_str())==FALSE)
		return false ;
	
	if (CreateDirectory(lpszPath,NULL))
		return true ;

	return false ;
}

void CConfigToolsDlg::OnTvnKeydownConfigTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTVKEYDOWN pTVKeyDown = reinterpret_cast<LPNMTVKEYDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}

void CConfigToolsDlg::OnNMReturnConfigList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}

void CConfigToolsDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CConfigToolsDlg::CopyItemTo(HTREEITEM hCurItem,HTREEITEM hObjItem)
{
	stPARENTS* pObjParents ;
	stPARENTS* pSource = (stPARENTS*)m_ConfigTree.GetItemData(hCurItem) ;
	if (hObjItem!=m_ConfigTree.GetRootItem())
		pObjParents = (stPARENTS*)m_ConfigTree.GetItemData(hObjItem) ;

	if ((pSource->enConfigType == stPARENTS::GLOBAL) && (hObjItem == m_ConfigTree.GetRootItem()))
	{
		stGLOBLE* pGlobal = new stGLOBLE ;
		memcpy_s(pGlobal,sizeof(stGLOBLE),pSource,sizeof(stGLOBLE)) ;
		HTREEITEM hNewItem = AddSubItem(hObjItem,TRUE,"Global",(DWORD_PTR)pGlobal) ;
		
		if ((pGlobal->sOptionChsName).GetLength()>0)
			m_ConfigTree.SetItemText(hNewItem,pGlobal->sOptionChsName) ;

		HTREEITEM hSongItem  = m_ConfigTree.GetChildItem(hCurItem) ;
		while(hSongItem)
		{
			CopyItemTo(hSongItem,hNewItem) ;
			hSongItem  = m_ConfigTree.GetNextSiblingItem(hSongItem) ;
		}
	}else if ((pSource->enConfigType == stPARENTS::MERGE) && (pObjParents->enConfigType == stPARENTS::GLOBAL))
	{
		stMERGE* pMerge = new stMERGE ;
		memcpy_s(pMerge,sizeof(stMERGE),pSource,sizeof(stMERGE)) ;		
		HTREEITEM hMerge =AddSubItem(hObjItem,FALSE,"Merge",(DWORD_PTR)pMerge) ;

		if ((pMerge->sMergeCurName).GetLength()>0)
			m_ConfigTree.SetItemText(hMerge,pMerge->sMergeCurName) ;

	}else
	{
		LogInfo("Copy 中位置不匹配","失败",TRUE) ;
		return false ;
	}
	
	return true ;
}


BOOL CConfigToolsDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		BOOL bCtrl = ::GetKeyState(VK_CONTROL)&0x8000 ;

		if (bCtrl && (pMsg->wParam=='c' || pMsg->wParam =='C') && (pMsg->hwnd == m_ConfigTree.GetSafeHwnd()))
		{
			m_hBeCopyItem = m_ConfigTree.GetSelectedItem() ;
		}else
			if (bCtrl && (pMsg->wParam=='v' || pMsg->wParam =='V') && (pMsg->hwnd == m_ConfigTree.GetSafeHwnd()))
			{
				HTREEITEM hItem = m_ConfigTree.GetSelectedItem() ;

				if (hItem==m_hBeCopyItem)
					LogInfo("Copy 的目标节点和源节点相同","失败",TRUE) ;
				else 
					if(hItem==NULL) 
						LogInfo("Copy 的源节点为空或为根节点","失败",TRUE)  ;
					else 
					{
						if(CopyItemTo(m_hBeCopyItem,hItem))
							ExportConfigCXml() ;
					}

				m_hBeCopyItem = NULL ;
			}	
		


		::TranslateMessage(pMsg) ;
		::DispatchMessage(pMsg) ;
		return true ;
	}
	
	return CDialog::PreTranslateMessage(pMsg) ;


}