﻿#pragma once


// CModifyConfigDlg 对话框

class CModifyConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CModifyConfigDlg)

public:
	CModifyConfigDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CModifyConfigDlg();

// 对话框数据
	enum { IDD = IDD_MODIFY_DIALOG };


	void SetStaticText(CString s) { m_sModifyKey = s; } ;
	void SetEditText(CString s) { m_sModifyValute = s; } ;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()


	
public:
	
	CString m_sModifyKey;
	CString m_sModifyValute;
};
