﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ConfigTools.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CONFIGTOOLS_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     129
#define IDB_BITMAP_STATE                129
#define IDB_BITMAP2                     130
#define IDB_BITMAP_LIST                 130
#define IDR_MENU1                       131
#define IDR_TRAYICON                    131
#define IDR_MENU2                       132
#define IDR_MODIFIYCONFIG_MENU          132
#define IDD_MODIFY_DIALOG               133
#define IDR_MENU3                       134
#define IDC_CONFIG_TREE                 1000
#define IDC_CONFIG_LIST                 1001
#define IDC_LOG_LIST                    1003
#define IDC_IMM_EXPORT_BUTTON           1004
#define IDC_MODIFIY_BUTTON              1005
#define IDC_SAVE_BUTTON                 1006
#define IDC_EDIT1                       1008
#define IDC_MODIFY_STATIC               1009
#define ID__32771                       32771
#define ID__                            32772
#define ID__32773                       32773
#define ID__32774                       32774
#define ID__32775                       32775
#define ID__32776                       32776
#define ID_SHOW_WINDOW                  32777
#define ID_DEL_CONFIG_NODE              32778
#define ID__32779                       32779
#define ID_SET_                         32780
#define ID_SET_INVAILD                  32781
#define ID_32782                        32782
#define ID_32783                        32783
#define ID_32784                        32784
#define ID_SYS_SETUP                    32785

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32786
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
