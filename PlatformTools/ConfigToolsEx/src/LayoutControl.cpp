﻿// LayoutControl.cpp: implementation of the CLayoutControl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LayoutControl.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
//	Construction/Destruction
//	Yangzj 2007.09.13
//////////////////////////////////////////////////////////////////////
namespace DynamicLayout
{
	CLayoutControl::CLayoutControl( CWnd* wndOwner,CWnd* pCtrl,Layout t,Layout b,Layout l,Layout r )
		: CLayoutBase( wndOwner,t,b,l,r )
	{
		m_pThis = pCtrl;
		m_bShoudInvaldeMainWnd = FALSE;
	}
	
	void CLayoutControl::GetRect(CRect& rect)
	{
		if( m_cWndRect.IsRectNull() )
			UpdateLayout();
		rect = m_cWndRect;
	}
	
	void CLayoutControl::SetRect( CRect& rect )
	{
		m_pThis->MoveWindow( rect );
		m_pThis->Invalidate( FALSE);
		m_cWndRect = rect;
	}
	
	void CLayoutControl::UpdateLayout()
	{
		m_pThis->GetWindowRect( m_cWndRect );
		m_wndOwner->ScreenToClient( m_cWndRect );
	}
	
	void CLayoutControl::Show( BOOL bShow/*=TRUE*/ )
	{
		if( !m_bVisible && bShow )
			m_pThis->ShowWindow(SW_SHOW);
		else if( m_bVisible && !bShow )
			m_pThis->ShowWindow(SW_HIDE);
		
		m_bVisible = bShow;
	}
	
	void CLayoutControl::Invalidate()
	{
		m_pThis->Invalidate();		
	}
}

