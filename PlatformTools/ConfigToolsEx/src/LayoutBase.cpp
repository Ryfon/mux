﻿// LayoutBase.cpp: implementation of the CLayoutBase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LayoutBase.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace DynamicLayout
{
	CLayoutBase::CLayoutBase(CWnd* wndOwner,Layout t,Layout b,Layout l,Layout r)
		:m_eTop(t),m_eBottom(b),m_eLeft(l),m_eRight(r)
		,m_wndOwner(wndOwner)
		,m_bVisible(TRUE)
		,m_cWndRect(0,0,0,0)
	{
		
	}
	
	CLayoutBase::~CLayoutBase()
	{

	}

	void CLayoutBase::Clear() /*= 0*/
	{
		m_cWndRect.top = 0;
		m_cWndRect.left = 0;
		m_cWndRect.right = 0;
		m_cWndRect.bottom = 0;
	}
	
	void CLayoutBase::ShowBorder( BOOL bShow/*=TRUE*/ )
	{
		
	}
	
	void CLayoutBase::Draw()
	{
		
	}
}

