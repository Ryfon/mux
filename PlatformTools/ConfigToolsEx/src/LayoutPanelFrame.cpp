﻿// LayoutPanelFrame.cpp: implementation of the CLayoutPanelFrame class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LayoutPanelFrame.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace DynamicLayout
{
	CLayoutPanelFrame::CLayoutPanelFrame(CWnd* wndOwner,Layout t,Layout b,Layout l,Layout r)
		:CLayoutPanel(wndOwner,t,b,l,r)
	{

	}

	CLayoutPanelFrame::~CLayoutPanelFrame()
	{

	}
	
	void CLayoutPanelFrame::SetRect( CRect& rect )
	{
		RectMore( rect );
		CLayoutPanel::SetRect(rect);
	}
	
	//获取Panel矩形区大小
	void CLayoutPanelFrame::UpdateLayout()
	{
		//只调用子布局的UpdateLayout
		POSITION pos = m_list_Layout.GetHeadPosition();
		while( pos != NULL )
			m_list_Layout.GetNext(pos)->UpdateLayout();
	}
	
	void CLayoutPanelFrame::Invalidate()
	{
		//只调用子布局的Invalidate();
		POSITION pos = m_list_Layout.GetHeadPosition();
		while( pos != NULL )
			m_list_Layout.GetNext(pos)->Invalidate();
	}
}

