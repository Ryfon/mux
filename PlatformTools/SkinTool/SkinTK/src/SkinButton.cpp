﻿#include "StdAfx.h"
#include "..\include\skinbutton.h"
#include "..\include\ButtonSkin.h"
#include "..\include\RadioSkin.h"
#include "..\include\CheckBoxSkin.h"
#include "..\include\GroupBoxSkin.h"


CSkinButton::CSkinButton(void)
{
	m_pButtonSkin = GetSkin().GetButtonSkin();
	m_pCheckSkin = GetSkin().GetCheckBoxSkin();
	m_pRadioSkin = GetSkin().GetRadioSkin();
	m_pGroupSkin = GetSkin().GetGroupBoxSkin();
	m_nButtonType = BTNTYPE_PUSHBUTTON;
	m_nButtonState = BUTTON_NORMAL;
}

CSkinButton::~CSkinButton(void)
{
	HookWindow((HWND)NULL);
}
void CSkinButton::LoadSkin()
{
	m_bEnableSkin = FALSE;

	m_pButtonSkin = GetSkin().GetButtonSkin();
	m_pCheckSkin = GetSkin().GetCheckBoxSkin();
	m_pRadioSkin = GetSkin().GetRadioSkin();
	m_pGroupSkin = GetSkin().GetGroupBoxSkin();

	m_bEnableSkin = TRUE;
	//SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
void CSkinButton::InstallSkin(HWND hWnd)
{
	if(!hWnd)
		return;
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);
	
	HookWindow( hWnd );

	if(!m_pButtonSkin)
		m_pButtonSkin = GetSkin().GetButtonSkin();
	if(!m_pCheckSkin)
		m_pCheckSkin = GetSkin().GetCheckBoxSkin();
	if(!m_pRadioSkin)
		m_pRadioSkin = GetSkin().GetRadioSkin();
	if(!m_pGroupSkin)
		m_pGroupSkin = GetSkin().GetGroupBoxSkin();
	

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);

	if(style&WS_DISABLED)
		m_nButtonState |= BUTTON_DISABLED;

	if(style&BS_ICON)
		m_bIcon = TRUE;
	else
		m_bIcon = FALSE;

	switch (style & SS_TYPEMASK)
	{
	case BS_OWNERDRAW:          
		m_nButtonType = BTNTYPE_UNKNOWN;
		break;
	case BS_DEFPUSHBUTTON:	
		m_nButtonType = BTNTYPE_PUSHBUTTON;
		m_nButtonState = BUTTON_DEFAULT;
		break;
	case BS_PUSHBUTTON:
	case BS_PUSHBOX:
		m_nButtonType = BTNTYPE_PUSHBUTTON;
		break;

	case BS_CHECKBOX:			
	case BS_AUTOCHECKBOX:		
	case BS_3STATE:				
	case BS_AUTO3STATE:			
		if (style & BS_PUSHLIKE)
			m_nButtonType = BTNTYPE_PUSHBUTTON;
		else
			m_nButtonType = BTNTYPE_CHECKBOX;
		break;

	case BS_RADIOBUTTON:		
	case BS_AUTORADIOBUTTON:	
		if (style & BS_PUSHLIKE)
			m_nButtonType = BTNTYPE_PUSHBUTTON;
		else
			m_nButtonType = BTNTYPE_RADIOBOX;
		break;
	case BS_GROUPBOX:	
		m_nButtonType = BTNTYPE_GROUPBOX;
		break;
	default:	
		m_nButtonType = BTNTYPE_UNKNOWN;
	}

	if(m_nButtonType == BTNTYPE_UNKNOWN)
		m_bEnableSkin = FALSE;
	if(m_nButtonState == BTNTYPE_PUSHBUTTON && m_pButtonSkin == NULL)
		m_bEnableSkin = FALSE;
	
	if(m_nButtonState == BTNTYPE_CHECKBOX && m_pCheckSkin == NULL)
		m_bEnableSkin = FALSE;

	if(m_nButtonState == BTNTYPE_RADIOBOX && m_pRadioSkin == NULL)
		m_bEnableSkin = FALSE;

	if(m_nButtonState == BTNTYPE_GROUPBOX && m_pGroupSkin == NULL)
		m_bEnableSkin = FALSE;

	DWORD dwState = (LONG) SendMessage(hWnd, BM_GETCHECK, 0, 0);
	if (dwState == BST_CHECKED)
		m_nButtonState |= BUTTON_CHECKED;
	else 
		m_nButtonState &= ~BUTTON_CHECKED;

	if (dwState == BST_INDETERMINATE)
		m_nButtonState |= BUTTON_INDETERMINATE;
	else
		m_nButtonState &= ~BUTTON_INDETERMINATE;

	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 

}
void CSkinButton::InstallSkin(HWND hWnd,CString strSkinName)
{
	if(!hWnd || strSkinName.IsEmpty())
		return;

	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	CObjectSkin* pObjectSkin = GetSkin().FindObjectSkin(strSkinName);
	if(pObjectSkin)
	{
		if(pObjectSkin->GetSkinType() == keButtonSkin)
			m_pButtonSkin = (CButtonSkin*)pObjectSkin;
		else if(pObjectSkin->GetSkinType() == keCheckBoxSkin)
			m_pCheckSkin = (CCheckBoxSkin*)pObjectSkin;
		else if(pObjectSkin->GetSkinType() == keGroupBoxSkin)
			m_pGroupSkin = (CGroupBoxSkin*)pObjectSkin;
		else if(pObjectSkin->GetSkinType() == keRadioSkin)
			m_pRadioSkin = (CRadioSkin*)pObjectSkin;
	}
	if(!m_pButtonSkin)
		m_pButtonSkin = GetSkin().GetButtonSkin();
	if(!m_pCheckSkin)
		m_pCheckSkin = GetSkin().GetCheckBoxSkin();
	if(!m_pRadioSkin)
		m_pRadioSkin = GetSkin().GetRadioSkin();
	if(!m_pGroupSkin)
		m_pGroupSkin = GetSkin().GetGroupBoxSkin();

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);
	if(style&WS_DISABLED)
		m_nButtonState |= BUTTON_DISABLED;
	if(style&BS_ICON)
		m_bIcon = TRUE;
	else
		m_bIcon = FALSE;
	switch (style & SS_TYPEMASK)
	{
	case BS_OWNERDRAW:          
		m_nButtonType = BTNTYPE_UNKNOWN;
		break;
	case BS_DEFPUSHBUTTON:	
		m_nButtonType = BTNTYPE_PUSHBUTTON;
		m_nButtonState = BUTTON_DEFAULT;
		break;
	case BS_PUSHBUTTON:
	case BS_PUSHBOX:
		m_nButtonType = BTNTYPE_PUSHBUTTON;
		break;
	case BS_CHECKBOX:			
	case BS_AUTOCHECKBOX:		
	case BS_3STATE:				
	case BS_AUTO3STATE:			
		if (style & BS_PUSHLIKE)
			m_nButtonType = BTNTYPE_PUSHBUTTON;
		else
			m_nButtonType = BTNTYPE_CHECKBOX;
		break;
	case BS_RADIOBUTTON:		
	case BS_AUTORADIOBUTTON:	
		if (style & BS_PUSHLIKE)
			m_nButtonType = BTNTYPE_PUSHBUTTON;
		else
			m_nButtonType = BTNTYPE_RADIOBOX;
		break;
	case BS_GROUPBOX:	
		m_nButtonType = BTNTYPE_GROUPBOX;
		break;
	default:	
		m_nButtonType = BTNTYPE_UNKNOWN;
	}

	if(m_nButtonType == BTNTYPE_UNKNOWN)
		m_bEnableSkin = FALSE;
	if(m_nButtonState == BTNTYPE_PUSHBUTTON && m_pButtonSkin == NULL)
		m_bEnableSkin = FALSE;

	if(m_nButtonState == BTNTYPE_CHECKBOX && m_pCheckSkin == NULL)
		m_bEnableSkin = FALSE;

	if(m_nButtonState == BTNTYPE_RADIOBOX && m_pRadioSkin == NULL)
		m_bEnableSkin = FALSE;

	if(m_nButtonState == BTNTYPE_GROUPBOX && m_pGroupSkin == NULL)
		m_bEnableSkin = FALSE;

	if(!m_bEnableSkin)
		return;

	int r = HookWindow( hWnd );

	DWORD dwState = (LONG) SendMessage(hWnd, BM_GETCHECK, 0, 0);
	if (dwState == BST_CHECKED)
		m_nButtonState |= BUTTON_CHECKED;
	else 
		m_nButtonState &= ~BUTTON_CHECKED;

	if (dwState == BST_INDETERMINATE)
		m_nButtonState |= BUTTON_INDETERMINATE;
	else
		m_nButtonState &= ~BUTTON_INDETERMINATE;

	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 

}
UINT CSkinButton::ButtonStyle2Format(DWORD style)
{
	UINT dtStyle = DT_NOCLIP;  /* We use SelectClipRgn to limit output */

	if (style & BS_PUSHLIKE)
		style &= ~0x0F;

	if (!(style & BS_MULTILINE))
		dtStyle |= DT_SINGLELINE;
	else
		dtStyle |= DT_WORDBREAK;

	switch (style & BS_CENTER)
	{
	case BS_LEFT:   /* DT_LEFT is 0 */    break;
	case BS_RIGHT:  dtStyle |= DT_RIGHT;  break;
	case BS_CENTER: dtStyle |= DT_CENTER; break;
	default:
	
		if (m_nButtonType == BTNTYPE_PUSHBUTTON) dtStyle |= DT_CENTER;
	}

	if (m_nButtonType!= BTNTYPE_GROUPBOX)
	{
		switch (style & BS_VCENTER)
		{
		case BS_TOP:     /* DT_TOP is 0 */      break;
		case BS_BOTTOM:  dtStyle |= DT_BOTTOM;  break;
		case BS_VCENTER: /* fall through */
		default:         dtStyle |= DT_VCENTER; break;
		}
	}
	else
		dtStyle |= DT_SINGLELINE;

	return dtStyle;
}
LRESULT CSkinButton::OnWndMsg(UINT msg,WPARAM wp,LPARAM lp)
{
	switch(msg)
	{
	case BM_SETSTATE:
		OnBMSetState(wp,lp);
		return 0;
	case BM_SETSTYLE:
		OnBMSetStyle(wp,lp);
		return 0;
	case BM_SETCHECK:
		OnBMSetCheck(wp,lp);
		return 0;
	case WM_MOUSELEAVE:
		OnMouseLeave();
		return 0;
	}
	return CSkinWnd::OnWndMsg(msg,wp,lp);
}
void CSkinButton::OnBMSetState(WPARAM wp,LPARAM lp)
{
	Default();
	SKIN_SETSTATE(m_nButtonState,BUTTON_PRESSED, wp);
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);
}
void CSkinButton::OnBMSetStyle(WPARAM wp,LPARAM lp)
{
	Default();
	SKIN_SETSTATE(m_nButtonState,BUTTON_DEFAULT, wp & BS_DEFPUSHBUTTON);
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);
}
void CSkinButton::OnBMSetCheck(WPARAM wp,LPARAM lp)
{
	Default();
	SKIN_SETSTATE(m_nButtonState, BUTTON_CHECKED, (wp == BST_CHECKED));
	SKIN_SETSTATE(m_nButtonState, BUTTON_INDETERMINATE, (wp == BST_INDETERMINATE));
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);
}
void CSkinButton::OnMouseMove(UINT nFlags, CPoint point)
{
	if((m_nButtonState & BUTTON_HOVER) == 0 && ((nFlags & MK_LBUTTON) == 0) )
	{
		TRACKMOUSEEVENT tme;
		m_nButtonState |= BUTTON_HOVER;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = m_hWnd;
		TrackMouseEvent(&tme);
	}

	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);

}
void CSkinButton::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(m_nButtonType == BTNTYPE_PUSHBUTTON)
	{
		CWnd *pWnd = CWnd::FromHandle(m_hWnd);
		CDC * pDC = pWnd->GetWindowDC();
		SetCapture(m_hWnd);
		SetFocus(m_hWnd);
		SKIN_SETSTATE(m_nButtonState,BUTTON_PRESSED,TRUE);
	}
	Default();
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);
}
void CSkinButton::OnMouseLeave()
{
	if (m_nButtonState & BUTTON_HOVER)
	{
		m_nButtonState &= ~BUTTON_HOVER;
	}
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);
}
void CSkinButton::OnSetFocus(HWND hWnd)
{
	Default();
	m_nButtonState |= BUTTON_FOCUS;
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);
}
void CSkinButton::OnKillFocus(HWND hWnd)
{
	Default();
	m_nButtonState &= ~BUTTON_FOCUS;
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);
}
void CSkinButton::OnEnable(BOOL bEnable)
{
	SKIN_SETSTATE(m_nButtonState, BUTTON_DISABLED, !bEnable);
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	DrawButton(pDC);
}

void CSkinButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	Default();
	if(m_nButtonType == BTNTYPE_PUSHBUTTON)
		ReleaseCapture();
	m_nButtonState = BUTTON_HOVER;
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	if(pWnd)
	{
		CDC * pDC = pWnd->GetDC();
		DrawButton(pDC);
	}
}

void CSkinButton::OnPaint()
{
	CButton *pButton = (CButton*)CWnd::FromHandle(m_hWnd);
	CPaintDC dc(pButton);
	DrawButton(&dc);
}

void CSkinButton::DrawButton(CDC *pDC)
{
	DWORD dwState = (LONG) SendMessage(m_hWnd, BM_GETCHECK, 0, 0);
	if (dwState == BST_CHECKED)
		m_nButtonState |= BUTTON_CHECKED;
	else
		m_nButtonState &= ~BUTTON_CHECKED;

	if (dwState == BST_INDETERMINATE)
		m_nButtonState |= BUTTON_INDETERMINATE;
	else
		m_nButtonState &= ~BUTTON_INDETERMINATE;


	if(m_nButtonType == BTNTYPE_PUSHBUTTON)
		DrawPushButton(pDC);
	else if(m_nButtonType == BTNTYPE_CHECKBOX)
		DrawCheckBoxButton(pDC);
	else if(m_nButtonType == BTNTYPE_RADIOBOX)
		DrawRadioButton(pDC);
	else if(m_nButtonType == BTNTYPE_GROUPBOX)
		DrawGroupBox(pDC);
}
void CSkinButton::DrawPushButtonText(CDC *pDC,CRect rtDest)
{
	CRect rtIcon,rtText;
	CButton *pButton = (CButton*)CWnd::FromHandle(m_hWnd);
	HICON hIcon = pButton->GetIcon();
	ICONINFO piconinfo;
	DWORD dwStyle;
	dwStyle = (DWORD)GetWindowLong(m_hWnd, GWL_STYLE);
	rtText = rtDest;
	if(m_bIcon)
	{
		GetIconInfo(hIcon,&piconinfo); //取图标信息
		rtIcon = rtDest;
		if (dwStyle & BS_TOP) //文字置顶
			rtIcon.top = rtDest.bottom - piconinfo.yHotspot*2 - 4;
		else if (dwStyle & BS_VCENTER)//文字置中
			rtIcon.top = (rtDest.bottom-rtDest.top)/2-piconinfo.yHotspot;
		else
			rtIcon.top = rtDest.top+4;

		if (dwStyle & BS_RIGHT) //文字置右
			rtIcon.left = 4;
		else if(dwStyle & BS_LEFT)//文字置左
			rtIcon.left = rtDest.right - (piconinfo.yHotspot*2 + 4);
		else
			rtIcon.left = (rtDest.right - rtDest.left)/2-piconinfo.yHotspot + 2;
		pDC->DrawIcon( rtIcon.left,rtIcon.top, hIcon);
	}
	else
	{
		CString strText;
		int nOldMode = pDC->SetBkMode(TRANSPARENT);
		UINT uFormat = ButtonStyle2Format(dwStyle);
		pButton->GetWindowText(strText);
		pDC->DrawText(strText,rtText,uFormat);
		pDC->SetBkMode(nOldMode);
	}
	
}
void CSkinButton::DrawPushButton(CDC *pDC)
{
	if(m_pButtonSkin == NULL)
		return;
	CRect rtWindow;
	CButton *pButton = (CButton*)CWnd::FromHandle(m_hWnd);
	pButton->GetWindowRect(rtWindow);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);
	CMemDC memDC(pDC,rtWindow);
	

	if (m_nButtonState & BUTTON_DISABLED)
	{
		m_pButtonSkin->DrawButton(&memDC,rtWindow,keDisabled);
	}
	else
	{
		if (m_nButtonState & BUTTON_PRESSED)
			m_pButtonSkin->DrawButton(&memDC,rtWindow,kePressed);
		else if (m_nButtonState & BUTTON_HOVER)
			m_pButtonSkin->DrawButton(&memDC,rtWindow,keHighlighted);
		else if (m_nButtonState & BUTTON_FOCUS)
			m_pButtonSkin->DrawButton(&memDC,rtWindow,keFocused);
		else if ((m_nButtonState & BUTTON_CHECKED) || (m_nButtonState & BUTTON_INDETERMINATE))
			m_pButtonSkin->DrawButton(&memDC,rtWindow,kePressed);
		else if (m_nButtonState & BUTTON_DEFAULT)
			m_pButtonSkin->DrawButton(&memDC,rtWindow,keDefault);
		else
			m_pButtonSkin->DrawButton(&memDC,rtWindow,keNormal);
	}
	CFont *pFont,*pOld;
	CFont font;
	if(m_pButtonSkin->m_bAutoFont)
	{
		pFont = pButton->GetFont(); 
		pOld = (CFont*)memDC.SelectObject( pFont );
	}
	else
	{
		font.CreateFontIndirect(&(m_pButtonSkin->m_fontButton));
		pOld = (CFont*)memDC.SelectObject( &font );
	}

	DrawPushButtonText(&memDC,rtWindow);

	memDC.SelectObject(pOld);
	if(!m_pButtonSkin->m_bAutoFont)
		font.DeleteObject();
}
void CSkinButton::DrawCheckBoxButton(CDC *pDC)
{
	if(m_pCheckSkin == NULL)
		return;
	CRect rtWindow;
	CButton *pButton = (CButton*)CWnd::FromHandle(m_hWnd);
	pButton->GetWindowRect(rtWindow);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);

//	CMemDC memDC(pDC,rtWindow);

	int width,height;
	CRect rtCheck,rtText;
	width = m_pCheckSkin->m_stateCheckBox[keNormal].imageChecked.rtImagePos.Width();
	height = m_pCheckSkin->m_stateCheckBox[keNormal].imageChecked.rtImagePos.Height();
	rtCheck.left = rtWindow.left;
	rtCheck.right = rtCheck.left + width;
	rtCheck.top = (rtWindow.Height() - height)/2;
	rtCheck.bottom = rtCheck.top + height;

	rtText = rtWindow;
	rtText.left = rtCheck.right +2;

	m_pCheckSkin->DrawBackground(pDC,rtWindow);

	
	if (m_nButtonState & BUTTON_DISABLED)
	{	
		if ((m_nButtonState & BUTTON_CHECKED))
			m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckDisabled,0);
		else if ((m_nButtonState & BUTTON_INDETERMINATE))
			m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckDisabled,2);
		else
			m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckDisabled,1);
	}
	else
	{
		if (m_nButtonState & BUTTON_PRESSED)
		{
			if ((m_nButtonState & BUTTON_CHECKED))
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckPressed,0);
			else if ((m_nButtonState & BUTTON_INDETERMINATE))
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckPressed,2);
			else
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckPressed,1);
		}
		else if (m_nButtonState & BUTTON_HOVER)
		{
			if ((m_nButtonState & BUTTON_CHECKED))
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckHover,0);
			else if ((m_nButtonState & BUTTON_INDETERMINATE))
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckHover,2);
			else
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckHover,1);
		}
		else if (m_nButtonState & BUTTON_FOCUS)
		{
			if ((m_nButtonState & BUTTON_CHECKED))
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckFocused,0);
			else if ((m_nButtonState & BUTTON_INDETERMINATE))
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckFocused,2);
			else
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckFocused,1);
		}
		else
		{
			if ((m_nButtonState & BUTTON_CHECKED))
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckNormal,0);
			else if ((m_nButtonState & BUTTON_INDETERMINATE))
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckNormal,2);
			else
				m_pCheckSkin->DrawBox(pDC,rtCheck,keCheckNormal,1);
		}
	}
	CFont *pFont = NULL;
	CFont *pOld = NULL;
	CFont font;
	if(m_pCheckSkin->m_bAutoFont)
	{
		pFont = pButton->GetFont(); 
		pOld = (CFont*)pDC->SelectObject( pFont );
	}
	else
	{
		font.CreateFontIndirect(&(m_pCheckSkin->m_fontCheckBox));
		pOld = (CFont*)pDC->SelectObject( &font );
	}
	DrawPushButtonText(pDC,rtText);

	pDC->SelectObject(pOld);
	if(!m_pCheckSkin->m_bAutoFont)
		font.DeleteObject();

}
void CSkinButton::DrawRadioButton(CDC *pDC)
{
	if(!m_pRadioSkin)
		return;
	CRect rtWindow;
	CButton *pButton = (CButton*)CWnd::FromHandle(m_hWnd);
	pButton->GetWindowRect(rtWindow);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);

	int width,height;
	CRect rtRadio,rtText;
	width = m_pRadioSkin->m_stateRadio[keNormal].imageChecked.rtImagePos.Width();
	height = m_pRadioSkin->m_stateRadio[keNormal].imageChecked.rtImagePos.Height();
	rtRadio.left = rtWindow.left;
	rtRadio.right = rtRadio.left + width;
	rtRadio.top = (rtWindow.Height() - height)/2;
	rtRadio.bottom = rtRadio.top + height;

	rtText = rtWindow;
	rtText.left = rtRadio.right+2;

	m_pRadioSkin->DrawBackground(pDC,rtWindow);

	int nOldMode = pDC->SetBkMode(TRANSPARENT);
	if (m_nButtonState & BUTTON_DISABLED)
	{	
		if ((m_nButtonState & BUTTON_CHECKED))
			m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioDisabled,0);
		else
			m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioDisabled,1);
	}
	else
	{
		if (m_nButtonState & BUTTON_PRESSED)
		{
			if ((m_nButtonState & BUTTON_CHECKED))
				m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioPressed,0);
			else
				m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioPressed,1);
		}
		else if (m_nButtonState & BUTTON_HOVER)
		{
			if ((m_nButtonState & BUTTON_CHECKED))
				m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioHover,0);
			else
				m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioHover,1);
		}
		else if (m_nButtonState & BUTTON_FOCUS)
		{
			if ((m_nButtonState & BUTTON_CHECKED))
				m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioFocused,0);
			else
				m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioFocused,1);
		}
		else
		{
			if ((m_nButtonState & BUTTON_CHECKED))
				m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioNormal,0);
			else
				m_pRadioSkin->DrawBox(pDC,rtRadio,keRadioNormal,1);
		}
	}

	CFont *pFont = NULL;
	CFont *pOld = NULL;
	CFont font;
	if(m_pRadioSkin->m_bAutoFont)
	{
		pFont = pButton->GetFont(); 
		pOld = (CFont*)pDC->SelectObject( pFont );
	}
	else
	{
		
		font.CreateFontIndirect(&(m_pRadioSkin->m_fontRadio));
		pOld = (CFont*)pDC->SelectObject( &font);
	}
	DrawPushButtonText(pDC,rtText);
	pDC->SelectObject(pOld);
	if(!m_pRadioSkin->m_bAutoFont)
		font.DeleteObject();
	pDC->SetBkMode(nOldMode);
}
void CSkinButton::DrawGroupBox(CDC *pDC)
{
	if(m_pGroupSkin == NULL)
		return;
	CRect rtWindow;
	CButton *pButton = (CButton*)CWnd::FromHandle(m_hWnd);
	pButton->GetWindowRect(rtWindow);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);

//	CMemDC memDC(pDC,rtWindow);
	CString strCaption;
	pButton->GetWindowText(strCaption);
	CFont *pFont,*pOld;
	CFont font;
	if(m_pGroupSkin->m_bAutoFont)
	{
		pFont = pButton->GetFont(); 
		pOld = (CFont*)pDC->SelectObject( pFont );
	}
	else
	{
		
		font.CreateFontIndirect(&(m_pGroupSkin->m_fontGroupBox));
		pOld = (CFont*)pDC->SelectObject( &font );
	}
	
	CSize size =  pDC->GetTextExtent( strCaption );

	rtWindow.top += size.cy/2 ;
	m_pGroupSkin->DrawFrame(pDC,rtWindow);
	CRect rtCaption;
	rtCaption.left = 5;
	rtCaption.right = rtCaption.left + size.cx + 5;
	rtCaption.top = 0;
	rtCaption.bottom = rtCaption.top + size.cy+2;
	m_pGroupSkin->DrawCaption(pDC,rtCaption);

	
	int oldMode = pDC->SetBkMode(TRANSPARENT);
	rtCaption.left += 2;
	pDC->DrawText(strCaption,rtCaption,DT_LEFT|DT_VCENTER|DT_SINGLELINE);

	pDC->SetBkMode(oldMode);
	pDC->SelectObject(pOld);
	if(!m_pGroupSkin->m_bAutoFont)
		font.DeleteObject();

}