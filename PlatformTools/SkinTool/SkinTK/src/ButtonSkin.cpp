﻿#include "StdAfx.h"
#include "..\include\ButtonSkin.h"

IMPLEMENT_SERIAL(CButtonSkin,CObjectSkin,1)
CButtonSkin::CButtonSkin(void)
{
	m_nSkinType = keButtonSkin;
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontButton = nif.lfCaptionFont;
	m_bAutoFont = TRUE;
}
CButtonSkin::CButtonSkin(CString strName)
:CObjectSkin(strName,keButtonSkin)
{
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontButton = nif.lfCaptionFont;
	m_bAutoFont = TRUE;
}

CButtonSkin::~CButtonSkin(void)
{
}
void CButtonSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_fontButton,sizeof(m_fontButton));
		ar.Write(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Write(m_stateButton,sizeof(m_stateButton));
	}
	else
	{
		ar.Read(&m_fontButton,sizeof(m_fontButton));
		ar.Read(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Read(m_stateButton,sizeof(m_stateButton));
	}
}
void CButtonSkin::DrawButton(CDC *pDC,CRect rtDest,int nState)
{
	if(!DrawImageSection(pDC,rtDest,m_stateButton[nState].imageSkin))
	{
		if(!DrawImageSection(pDC,rtDest,m_stateButton[0].imageSkin))
		{
			pDC->Draw3dRect(rtDest,::GetSysColor(COLOR_BTNHILIGHT),::GetSysColor(COLOR_BTNHILIGHT));
			rtDest.DeflateRect(1,1);
			CBrush brush(GetSysColor(CTLCOLOR_BTN));
			pDC->FillRect(rtDest,&brush);
		}
	}
}