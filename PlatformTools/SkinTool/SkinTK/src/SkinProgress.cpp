﻿#include "StdAfx.h"
#include "..\include\skinprogress.h"
#include "..\include\ProgressSkin.h"

CSkinProgress::CSkinProgress(void)
{
	m_pProgressSkin = GetSkin().GetProgressSkin();
	m_bVert = FALSE;
}

CSkinProgress::~CSkinProgress(void)
{
	HookWindow( (HWND)NULL);
}
void CSkinProgress::InstallSkin(HWND hWnd)
{
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );

	
	m_pProgressSkin = GetSkin().GetProgressSkin();
	if(m_pProgressSkin == NULL)
		m_bEnableSkin = FALSE;

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);
	if(style & PBS_VERTICAL)
		m_bVert = TRUE;
	else
		m_bVert = FALSE;

	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0,  SWP_FRAMECHANGED|SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
void CSkinProgress::LoadSkin()
{
	m_bEnableSkin = FALSE;
	m_pProgressSkin = GetSkin().GetProgressSkin();
	m_bEnableSkin = TRUE;
	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
BOOL CSkinProgress::GetPosRect(CRect &rtPos)
{
	CProgressCtrl *pProgress = (CProgressCtrl*)CWnd::FromHandle(m_hWnd);

	CRect rtClient;
	GetClientRect(rtClient);
	int nUpper,nLower,nPos;
	pProgress->GetRange(nLower,nUpper);
	nPos = pProgress->GetPos();

	if(nPos > nUpper)
		nPos = nUpper;
	if(nPos < nLower)
		nPos = nLower;

	if(nLower > nUpper)
	{
		int temp = nLower;
		nLower = nUpper;
		nUpper = temp;
	}

	if(!m_bVert)
	{
		rtPos = rtClient;
		rtPos.right = rtPos.left + rtClient.Width() * nPos/(nUpper - nLower);
	}
	else
	{
		rtPos = rtClient;
		rtPos.bottom = rtPos.top + rtClient.Height() * nPos/(nUpper - nLower);
	}

	return TRUE;
}
void CSkinProgress::OnPaint()
{
	CProgressCtrl *pProgress = (CProgressCtrl*)CWnd::FromHandle(m_hWnd);
	CPaintDC dc(pProgress);
	
	CRect rtClient;
	GetClientRect(rtClient);

	CMemDC memDC(&dc,rtClient);

	if(!m_pProgressSkin->DrawImageSection(&memDC,rtClient,m_pProgressSkin->m_imageBackground))
	{
		if(m_pProgressSkin == NULL)
			memDC.FillSolidRect(rtClient,GetSysColor(COLOR_ACTIVECAPTION));
		else
			memDC.FillSolidRect(rtClient,m_pProgressSkin->m_colorBackground);
	}
	CRect rtPos;
	GetPosRect(rtPos);
	if(!m_pProgressSkin->DrawImageSection(&memDC,rtPos,m_pProgressSkin->m_imageProgress))
	{
		if(m_pProgressSkin == NULL)
			memDC.FillSolidRect(rtPos,RGB(0,233,0));
		else
			memDC.FillSolidRect(rtPos,m_pProgressSkin->m_colorProgress);
	}
}
BOOL CSkinProgress::OnEraseBkgnd(CDC *pDC)
{
	CRect rtClient;
	GetClientRect(rtClient);

	CMemDC memDC(pDC,rtClient);

	if(!m_pProgressSkin->DrawImageSection(&memDC,rtClient,m_pProgressSkin->m_imageBackground))
	{
		if(m_pProgressSkin == NULL)
			memDC.FillSolidRect(rtClient,GetSysColor(COLOR_ACTIVECAPTION));
		else
			memDC.FillSolidRect(rtClient,m_pProgressSkin->m_colorBackground);
	}
	return TRUE;
}