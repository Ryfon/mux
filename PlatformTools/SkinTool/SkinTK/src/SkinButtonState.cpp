﻿#include "StdAfx.h"
#include "..\include\skinbuttonstate.h"
////////////////////////////////////////////////////

////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CSkinScrollBarState,CObject,1)
CSkinScrollBarState::CSkinScrollBarState(void)
: m_bArrow1(FALSE)
, m_bArrow2(FALSE)
, m_bScrollBar(FALSE)
, m_bThurmb(FALSE)
{
	m_imageArrow1		= new CSkinImageRect();
	m_imageArrow2		= new CSkinImageRect();
	m_imageScrollBar	= new CSkinImageSection();
	m_imageThurmb		= new CSkinImageSection();
}

CSkinScrollBarState::~CSkinScrollBarState(void)
{
	if(m_imageArrow1)
		delete m_imageArrow1;
	if(m_imageArrow2)
		delete m_imageArrow2;
	if(m_imageScrollBar)
		delete m_imageScrollBar;
	if(m_imageThurmb)
		delete m_imageThurmb;
	m_imageArrow1 = NULL;
	m_imageArrow2 = NULL;
	m_imageScrollBar = NULL;
	m_imageThurmb = NULL;
}
void CSkinScrollBarState::Serialize(CArchive &ar)
{
	CObject::Serialize(ar);
	if(ar.IsStoring())
	{
		ar << m_bArrow1 << m_bArrow2 << m_bScrollBar << m_bThurmb;
	}
	else
	{
		ar >> m_bArrow1 >> m_bArrow2 >> m_bScrollBar >> m_bThurmb;
	}
	if(m_bArrow1)
		m_imageArrow1->Serialize(ar);
	if(m_bArrow2)
		m_imageArrow2->Serialize(ar);
	if(m_bScrollBar)
		m_imageScrollBar->Serialize(ar);
	if(m_bThurmb)
		m_imageThurmb->Serialize(ar);
}

////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CSkinWindowFrame,CObject,1)
CSkinWindowFrame::CSkinWindowFrame(void)
{

}

CSkinWindowFrame::~CSkinWindowFrame(void)
{

}
void CSkinWindowFrame::Serialize(CArchive &ar)
{
	CObject::Serialize(ar);
	m_imageTop.Serialize(ar);
	m_imageBottom.Serialize(ar);
	m_imageLeft.Serialize(ar);
	m_imageRight.Serialize(ar);
}
bool CSkinWindowFrame::DrawTop(CDC *pDC,SkinRect rectDst)
{
	if(m_imageTop.m_strImageName.IsEmpty())
		return false;
	else
	{
		m_imageTop.Draw(pDC,rectDst);
		return true;
	}
}
bool CSkinWindowFrame::DrawBottom(CDC *pDC,SkinRect rectDst)
{
	if(m_imageBottom.m_strImageName.IsEmpty())
		return false;
	else
	{
		m_imageBottom.Draw(pDC,rectDst);
		return true;
	}
}
bool CSkinWindowFrame::DrawLeft(CDC *pDC,SkinRect rectDst)
{
	if(m_imageLeft.m_strImageName.IsEmpty())
		return false;
	else
	{
		m_imageLeft.Draw(pDC,rectDst);
		return true;
	}
}
bool CSkinWindowFrame::DrawRight(CDC *pDC,SkinRect rectDst)
{
	if(m_imageRight.m_strImageName.IsEmpty())
		return false;
	else
	{
		m_imageRight.Draw(pDC,rectDst);
		return true;
	}
}
////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CSkinMenuBar,CObject,1)
CSkinMenuBar::CSkinMenuBar(void)
{
}

CSkinMenuBar::~CSkinMenuBar(void)
{
}
void CSkinMenuBar::Serialize(CArchive &ar)
{
	CObject::Serialize(ar);
	m_imageNormal.Serialize(ar);
	m_imageActive.Serialize(ar);
	m_imagePressed.Serialize(ar);
	
	m_textNormal.Serialize(ar);
	m_textGrayed.Serialize(ar);
	m_textActive.Serialize(ar);
	m_textPressed.Serialize(ar);
	
	if(ar.IsLoading())
		ar>>m_colorActive>>m_colorPressed;
	else
		ar<<m_colorActive<<m_colorPressed;
	
}