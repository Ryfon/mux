﻿#include "StdAfx.h"
#include "..\include\slideskin.h"


IMPLEMENT_SERIAL(CSliderSkin,CObjectSkin,1)
CSliderSkin::CSliderSkin(void)
{
	m_nSkinType = keSliderSkin;
}
CSliderSkin::CSliderSkin(CString strName)
:CObjectSkin(strName,keSliderSkin)
{
	
}
CSliderSkin::~CSliderSkin(void)
{
}
void CSliderSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar.Write(&m_imageChannel,sizeof(m_imageChannel));
		ar.Write(&m_imageThumbHorz,sizeof(m_imageThumbHorz));
		ar.Write(&m_imageThumbVert,sizeof(m_imageThumbVert));
	}
	else
	{
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar.Read(&m_imageChannel,sizeof(m_imageChannel));
		ar.Read(&m_imageThumbHorz,sizeof(m_imageThumbHorz));
		ar.Read(&m_imageThumbVert,sizeof(m_imageThumbVert));
	}
}