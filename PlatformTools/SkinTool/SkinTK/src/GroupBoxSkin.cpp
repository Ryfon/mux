﻿#include "StdAfx.h"
#include "..\include\groupboxskin.h"

IMPLEMENT_SERIAL(CGroupBoxSkin,CObjectSkin,1)
CGroupBoxSkin::CGroupBoxSkin(void)
{
	m_nSkinType = keGroupBoxSkin;
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontGroupBox = nif.lfCaptionFont;
	m_bAutoFont = TRUE; 
	m_colorCaption = GetSysColor(COLOR_BTNFACE);
}

CGroupBoxSkin::~CGroupBoxSkin(void)
{
}
CGroupBoxSkin::CGroupBoxSkin(CString strNmae)
:CObjectSkin(strNmae,keGroupBoxSkin)
{
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontGroupBox = nif.lfCaptionFont;
	m_bAutoFont = TRUE; 
	m_colorCaption = GetSysColor(COLOR_BTNFACE);
}
BOOL CGroupBoxSkin::DrawFrame(CDC *pDC,CRect rtDest)
{
	return DrawImageBorder(pDC,rtDest,m_imageFrame);
	return TRUE;
}
BOOL CGroupBoxSkin::DrawCaption(CDC *pDC,CRect rtDest)
{
	return DrawImageSection(pDC,rtDest,m_imageCaption);
	return TRUE;
}
void CGroupBoxSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_fontGroupBox,sizeof(m_fontGroupBox));
		ar.Write(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Write(&m_imageFrame,sizeof(m_imageFrame));
		ar.Write(&m_imageCaption,sizeof(m_imageCaption));
		ar.Write(&m_colorCaption,sizeof(m_colorCaption));
		ar.Write(&m_textEffect,sizeof(m_textEffect));
	}
	else
	{
		ar.Read(&m_fontGroupBox,sizeof(m_fontGroupBox));
		ar.Read(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Read(&m_imageFrame,sizeof(m_imageFrame));
		ar.Read(&m_imageCaption,sizeof(m_imageCaption));
		ar.Read(&m_colorCaption,sizeof(m_colorCaption));
		ar.Read(&m_textEffect,sizeof(m_textEffect));
	}
}