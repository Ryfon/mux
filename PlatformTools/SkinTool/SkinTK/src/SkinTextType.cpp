﻿#include "StdAfx.h"
#include "..\include\skintexttype.h"
///////////////////////////////////////
IMPLEMENT_SERIAL(CSkinTextType,CObject,1)
CSkinTextType::CSkinTextType(void)
{
}

CSkinTextType::~CSkinTextType(void)
{
}
void CSkinTextType::Serialize(CArchive &ar)
{
	CObject::Serialize(ar);
	if(ar.IsStoring())
		ar<<m_colorText<<m_colorShadow << m_sizeOffset;
	else
		ar >>m_colorText>>m_colorShadow>> m_sizeOffset;
}