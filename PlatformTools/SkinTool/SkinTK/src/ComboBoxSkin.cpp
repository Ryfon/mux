﻿#include "StdAfx.h"
#include "..\include\comboboxskin.h"


IMPLEMENT_SERIAL(CComboBoxSkin,CObjectSkin,1)
CComboBoxSkin::CComboBoxSkin(void)
{
	m_nSkinType = keComboxSkin;
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontComboBox = nif.lfCaptionFont;
	m_bAutoFont = TRUE;
}
CComboBoxSkin::CComboBoxSkin(CString strName)
: CObjectSkin(strName,keComboxSkin)
{
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontComboBox = nif.lfCaptionFont;
	m_bAutoFont = TRUE;
}

CComboBoxSkin::~CComboBoxSkin(void)
{
}
void CComboBoxSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_fontComboBox,sizeof(m_fontComboBox));
		ar.Write(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Write(&m_imageBorder,sizeof(m_imageBorder));
		ar.Write(m_iamgeThumb,sizeof(m_iamgeThumb));
	}
	else
	{
		ar.Read(&m_fontComboBox,sizeof(m_fontComboBox));
		ar.Read(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Read(&m_imageBorder,sizeof(m_imageBorder));
		ar.Read(m_iamgeThumb,sizeof(m_iamgeThumb));
	}
}