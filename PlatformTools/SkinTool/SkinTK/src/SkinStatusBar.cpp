﻿#include "StdAfx.h"
#include "..\include\skinstatusbar.h"
#include "..\include\StatusbarSkin.h"

CSkinStatusBar::CSkinStatusBar(void)
{
	m_pStatusBarSkin = GetSkin().GetStatusBarSkin();
}

CSkinStatusBar::~CSkinStatusBar(void)
{
	HookWindow( (HWND)NULL);
}
void CSkinStatusBar::LoadSkin()
{
	m_bEnableSkin = FALSE;

	m_pStatusBarSkin = GetSkin().GetStatusBarSkin();
	if(m_pStatusBarSkin == NULL)
		m_bEnableSkin = FALSE;
	else
		m_bEnableSkin = TRUE;
	//SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
void CSkinStatusBar::InstallSkin(HWND hWnd)
{
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );

	if(m_pStatusBarSkin == NULL)
		m_pStatusBarSkin = GetSkin().GetStatusBarSkin();

	if(m_pStatusBarSkin == NULL)
		m_bEnableSkin = FALSE;
	else
		m_bEnableSkin = TRUE;

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);

	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 

}
void CSkinStatusBar::OnPaint()
{
	//Default();
	CStatusBar *pStatusBar = (CStatusBar*)CWnd::FromHandle(m_hWnd);
	CPaintDC dc(pStatusBar);
	DrawStatusBar(&dc);
}
void CSkinStatusBar::OnSetText(WPARAM wp,LPARAM lp)
{
	Default();
	Invalidate(TRUE);
}

BOOL CSkinStatusBar::OnEraseBkgnd(CDC *pDC)
{
	CRect rtWindow;
	CStatusBar *pStatusBar = (CStatusBar*)CWnd::FromHandle(m_hWnd);
	pStatusBar->GetWindowRect(rtWindow);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);
	{
		m_pStatusBarSkin->DrawImageSection(pDC,rtWindow,m_pStatusBarSkin->m_imageBackground);
	}
	return TRUE;
}
void CSkinStatusBar::DrawStatusBar(CDC *pDC)
{
	if(m_pStatusBarSkin == NULL)
		return;

	CRect rtClient;
	GetClientRect(rtClient);
	m_pStatusBarSkin->DrawImageSection(pDC,rtClient,m_pStatusBarSkin->m_imageBackground);
	CStatusBarCtrl *pStatusBar = (CStatusBarCtrl*)CWnd::FromHandle(m_hWnd);
	int nPart[255];
	int nCount = pStatusBar->GetParts(255,nPart);
	CRect rcItem;
	for ( int i = 0; i < nCount; i++ )
	{
		pStatusBar->GetRect(i, rcItem);

		if(i == nCount - 1)
		{
			CRect rcClient;
			GetClientRect(&rcClient);
			if(rcItem.right < rcClient.right)
			{
				rcItem.SetRect(rcItem.left,rcItem.top,rcClient.right,rcItem.bottom);

			}
		}
		m_pStatusBarSkin->DrawImageSection(pDC,rcItem,m_pStatusBarSkin->m_imageItem);
		pDC->Draw3dRect(&rcItem,GetSysColor(COLOR_3DSHADOW),0xFFFFFF);
		rcItem.SetRect(rcItem.left + 1,rcItem.top + 1,rcItem.right - 1,rcItem.bottom - 1);
		CString itemText;
		itemText = pStatusBar->GetText(i);

		if(itemText.IsEmpty())
			continue;

		CFont *pFont = pStatusBar->GetFont();
		CFont *pOldFont = pDC->SelectObject(pFont);
		int oldBkMode = pDC->SetBkMode(TRANSPARENT);
		TEXTMETRIC Metrics;
		pDC->GetTextMetrics(&Metrics);
		rcItem.top = rcItem.top + (rcItem.Height() - Metrics.tmHeight)/2;
		pDC->DrawText(itemText,&rcItem,DT_SINGLELINE|DT_VCENTER|DT_LEFT|DT_NOPREFIX);
		pDC->SelectObject(pOldFont);
	}
}