﻿#include "StdAfx.h"
#include "..\include\skinmdiclient.h"

CSkinMDIClient::CSkinMDIClient(void)
{
}

CSkinMDIClient::~CSkinMDIClient(void)
{
}
LRESULT CSkinMDIClient::OnWndMsg(UINT msg,WPARAM wp,LPARAM lp)
{
	switch (msg) 
	{
	case WM_MDISETMENU:		
		if (wp) 
		{
			if(IsWindow(m_hWnd))
			{
				HWND hParent = ::GetParent(m_hWnd);
				if(IsWindow(hParent))
				{
					CSkinFrame* p = (CSkinFrame*)theHookMap.Lookup(hParent);
					p->m_menuBar.SetMenu((HMENU)wp,NULL);
				}
			}
			
		}
		return 0;
	}
	return Default(msg,wp,lp);
}