﻿#include "StdAfx.h"
#include "..\include\WindowSkin.h"
////////////////////CWinButtonSkin//////////////
IMPLEMENT_SERIAL(CWinButtonSkin,CObjectSkin,1)
CWinButtonSkin::CWinButtonSkin()
{
	m_nSkinType = keWinButtonSkin;
}
CWinButtonSkin::CWinButtonSkin(CString strName,int nWinButtonType)
:CObjectSkin(strName,keWinButtonSkin)
,m_nWinButtonType(nWinButtonType)
{
	
}
CWinButtonSkin::~CWinButtonSkin()
{
}
void CWinButtonSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar << m_nWinButtonType;
		for(int i = 0 ;i < keWinButtonStateSize ; i ++)
			ar.Write(&m_iamgeButton[i],sizeof(SkinImageRect));
	}
	else
	{
		ar >> m_nWinButtonType;
		for(int i = 0 ;i < keWinButtonStateSize ; i ++)
			ar.Read(&m_iamgeButton[i],sizeof(SkinImageRect));
	}
}
void CWinButtonSkin::DrawButton(CDC *pDC,CRect rtDest,int nStateButton)
{
	if(nStateButton < 0 || nStateButton > keWinButtonStateSize)
		nStateButton = 0;
	DrawImageRect(pDC,rtDest,m_iamgeButton[nStateButton]);
}

////////////////////////////////////////////////
IMPLEMENT_SERIAL(CWindowSkin,CObjectSkin,1)
CWindowSkin::CWindowSkin(void)
{
	m_nSkinType = keWindowSkin;
	m_rtMargins = CRect(12,45,12,25);
	::ZeroMemory(&m_imageBackground,sizeof(m_imageBackground));
}
CWindowSkin::CWindowSkin(CString strName)
:CObjectSkin(strName,keWindowSkin)
{
	m_rtMargins = CRect(12,45,12,25);
	m_bSkinClient = TRUE;
	::ZeroMemory(&m_imageBackground,sizeof(m_imageBackground));
}
CWindowSkin::~CWindowSkin(void)
{

}
void CWindowSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_frameActive,sizeof(m_frameActive));
		ar.Write(&m_frameInactive,sizeof(m_frameInactive));
		ar.Write(&m_skinMenuBar,sizeof(m_skinMenuBar));
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar <<m_bSkinClient<<m_colorBack<< m_rtMargins; 

		int nObjectSkin = (int)m_arrayChildrens.GetSize();
		ar.Write(&nObjectSkin,sizeof(nObjectSkin));
		for(int i = 0 ; i < nObjectSkin ; i++)
		{
			CObjectSkin *pObject = m_arrayChildrens.GetAt(i);
			int nType = pObject->GetSkinType();
			ar.Write(&nType,sizeof(nType));
			pObject->Serialize(ar);
		}
	}
	else
	{
		ar.Read(&m_frameActive,sizeof(m_frameActive));
		ar.Read(&m_frameInactive,sizeof(m_frameInactive));
		ar.Read(&m_skinMenuBar,sizeof(m_skinMenuBar));
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar >>m_bSkinClient>>m_colorBack>> m_rtMargins;

		int nObjectSkin = 0;
		ar.Read(&nObjectSkin,sizeof(nObjectSkin));
		for(int i = 0 ;i < nObjectSkin ; i++)
		{
			CObjectSkin *pObject = NULL;
			int nType = keUnknowSkin;
			ar.Read(&nType,sizeof(nType));

			if(nType == keWinButtonSkin)
			{
				pObject = new CWinButtonSkin;
				pObject->SetParent(this);
				pObject->Serialize(ar);
			}
			else
			{

			}
			m_arrayChildrens.Add(pObject);
		}
	}
	
}
void CWindowSkin::CalcPartsRect(CRect rtDest)
{
	if(m_rtWindow == rtDest)
		return;
	else
		m_rtWindow = rtDest;
	m_rectParts[SKINPART_TOP_LEFT].left			= rtDest.left;
	m_rectParts[SKINPART_TOP_LEFT].top			= rtDest.top;
	m_rectParts[SKINPART_TOP_LEFT].right		= rtDest.left + m_rtMargins.left;
	m_rectParts[SKINPART_TOP_LEFT].bottom		= rtDest.top + m_rtMargins.top;

	m_rectParts[SKINPART_TOP].left				= rtDest.left + m_rtMargins.left;
	m_rectParts[SKINPART_TOP].top				= rtDest.top;
	m_rectParts[SKINPART_TOP].right				= rtDest.right - m_rtMargins.right;
	m_rectParts[SKINPART_TOP].bottom			= rtDest.top + m_rtMargins.top;

	m_rectParts[SKINPART_TOP_BORDER]			= m_rectParts[SKINPART_TOP]; 
	m_rectParts[SKINPART_TOP_BORDER].bottom		= m_rectParts[SKINPART_TOP_BORDER].top + m_rtMargins.bottom;

	m_rectParts[SKINPART_TOP_RIGHT].left		= rtDest.right - m_rtMargins.right;
	m_rectParts[SKINPART_TOP_RIGHT].top			= rtDest.top;
	m_rectParts[SKINPART_TOP_RIGHT].right		= rtDest.right;
	m_rectParts[SKINPART_TOP_RIGHT].bottom		= rtDest.top + m_rtMargins.top;

	m_rectParts[SKINPART_LEFT].left				= rtDest.left;
	m_rectParts[SKINPART_LEFT].top				= rtDest.top + m_rtMargins.top;
	m_rectParts[SKINPART_LEFT].right			= rtDest.left + m_rtMargins.left;
	m_rectParts[SKINPART_LEFT].bottom			= rtDest.bottom - m_rtMargins.bottom;

	m_rectParts[SKINPART_RIGHT].left			= rtDest.right - m_rtMargins.right;
	m_rectParts[SKINPART_RIGHT].top				= rtDest.top + m_rtMargins.top;
	m_rectParts[SKINPART_RIGHT].right			= rtDest.right;
	m_rectParts[SKINPART_RIGHT].bottom			= rtDest.bottom - m_rtMargins.bottom;

	m_rectParts[SKINPART_BOTTOM_LEFT].left		= rtDest.left;
	m_rectParts[SKINPART_BOTTOM_LEFT].top		= rtDest.bottom - m_rtMargins.bottom;
	m_rectParts[SKINPART_BOTTOM_LEFT].right		= rtDest.left + m_rtMargins.left;
	m_rectParts[SKINPART_BOTTOM_LEFT].bottom	= rtDest.bottom;

	m_rectParts[SKINPART_BOTTOM].left			= rtDest.left + m_rtMargins.left;
	m_rectParts[SKINPART_BOTTOM].top			= rtDest.bottom - m_rtMargins.bottom;
	m_rectParts[SKINPART_BOTTOM].right			= rtDest.right - m_rtMargins.right;
	m_rectParts[SKINPART_BOTTOM].bottom			= rtDest.bottom;

	m_rectParts[SKINPART_BOTTOM_RIGHT].left		= rtDest.right - m_rtMargins.right;
	m_rectParts[SKINPART_BOTTOM_RIGHT].top		= rtDest.bottom - m_rtMargins.bottom;
	m_rectParts[SKINPART_BOTTOM_RIGHT].right	= rtDest.right;
	m_rectParts[SKINPART_BOTTOM_RIGHT].bottom	= rtDest.bottom;

	CalcButtonRect(rtDest);
}
void CWindowSkin::CalcButtonRect(CRect rtDest)
{

	CObjectSkin *pObject = NULL;
	for(int i = 0 ;i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() ==  keWinButtonSkin)
		{
			CWinButtonSkin *pWinButton = (CWinButtonSkin*)pObject;
			pObject->SetParent(this);
			pObject->CalcOffset();
			if(pWinButton->GetWinButtonType() == keClose)
				m_rtButtons[SKINBUTTON_CLOSE] = pWinButton->CalcPosition(rtDest);
			else if(pWinButton->GetWinButtonType() == keMax)
				m_rtButtons[SKINBUTTON_MAX] = pWinButton->CalcPosition(rtDest);
			else if(pWinButton->GetWinButtonType() == keMin)
				m_rtButtons[SKINBUTTON_MIN] = pWinButton->CalcPosition(rtDest);
			else if(pWinButton->GetWinButtonType() == keHelp)
				m_rtButtons[SKINBUTTON_HELP] = pWinButton->CalcPosition(rtDest);
			else if(pWinButton->GetWinButtonType() == keSysMenu)
				m_rtButtons[SKINBUTTON_SYSTEM] = pWinButton->CalcPosition(rtDest);
		}
	}
}
void CWindowSkin::SetMargins(const SkinMargins &margins)
{
	m_rtMargins = margins; 
}
void CWindowSkin::SetMargins(int left,int top,int right,int bottom)
{
	m_rtMargins.left = left; 
	m_rtMargins.top = top; 
	m_rtMargins.right = right;
	m_rtMargins.bottom = bottom;
}
SkinMargins CWindowSkin::GetMargins()
{ 
	return m_rtMargins; 
}
BOOL CWindowSkin::DrawFrame(CDC *pDC,CRect rtDest,BOOL bActive)
{
	CRect			rtMargins = m_rtMargins;
	CRect			rtParts[4];	
	rtParts[0].left		= rtDest.left + rtMargins.left;
	rtParts[0].top		= rtDest.top;
	rtParts[0].right	= rtDest.right - rtMargins.right;
	rtParts[0].bottom	= rtDest.top + rtMargins.top;
	rtParts[1].left		= rtDest.left;
	rtParts[1].top		= rtDest.top ;
	rtParts[1].right	= rtDest.left + rtMargins.left;
	rtParts[1].bottom	= rtDest.bottom;

	rtParts[2].left		= rtDest.right - rtMargins.right;
	rtParts[2].top		= rtDest.top ;
	rtParts[2].right	= rtDest.right;
	rtParts[2].bottom	= rtDest.bottom;

	rtParts[3].left		= rtDest.left + rtMargins.left;
	rtParts[3].top		= rtDest.bottom - rtMargins.bottom;
	rtParts[3].right	= rtDest.right - rtMargins.right;
	rtParts[3].bottom	= rtDest.bottom;


	if(bActive)
	{
		TransDrawImageSection(pDC,rtParts[0],m_frameActive.imageTop,	RGB(255,0,255));
		TransDrawImageSection(pDC,rtParts[1],m_frameActive.imageLeft,	RGB(255,0,255));
		TransDrawImageSection(pDC,rtParts[2],m_frameActive.imageRight,	RGB(255,0,255));
		TransDrawImageSection(pDC,rtParts[3],m_frameActive.imageBottom,	RGB(255,0,255));
	}
	else
	{
		DrawImageSection(pDC,rtParts[0],m_frameActive.imageTop);
		DrawImageSection(pDC,rtParts[1],m_frameActive.imageLeft);
		DrawImageSection(pDC,rtParts[2],m_frameActive.imageRight);
		DrawImageSection(pDC,rtParts[3],m_frameActive.imageBottom);
	}

	return true;
}
BOOL CWindowSkin::DrawLeft(CDC *pDC,CRect rtWindow,BOOL bActive)
{
	CRect			rtMargins = m_rtMargins;
	CRect			rtParts;	

	rtParts.left	= rtWindow.left;
	rtParts.top		= rtWindow.top ;
	rtParts.right	= rtWindow.left + rtMargins.left;
	rtParts.bottom	= rtWindow.bottom;
	CMemDC dcMen(pDC,&rtParts);
	if(bActive)
		 return TransDrawImageSection(&dcMen,rtParts,m_frameActive.imageLeft,	RGB(255,0,255));
	else
		return DrawImageSection(&dcMen,rtParts,m_frameActive.imageLeft);
}
BOOL CWindowSkin::DrawRight(CDC *pDC,CRect rtWindow,BOOL bActive)
{
	CRect			rtMargins = m_rtMargins;
	CRect			rtParts;	

	rtParts.left	= rtWindow.right - rtMargins.right;
	rtParts.top		= rtWindow.top ;
	rtParts.right	= rtWindow.right;
	rtParts.bottom	= rtWindow.bottom;
	CMemDC dcMen(pDC,&rtParts);
	if(bActive)
		return TransDrawImageSection(&dcMen,rtParts,m_frameActive.imageRight,	RGB(255,0,255));
	else
		return DrawImageSection(&dcMen,rtParts,m_frameActive.imageRight);
}
BOOL CWindowSkin::DrawTop(CDC *pDC,CRect rtWindow,BOOL bActive)
{
	CRect			rtMargins = m_rtMargins;
	CRect			rtParts;	
	rtParts.left	= rtWindow.left + rtMargins.left;
	rtParts.top		= rtWindow.top;
	rtParts.right	= rtWindow.right - rtMargins.right;
	rtParts.bottom	= rtWindow.top + rtMargins.top;
	CMemDC dcMen(pDC,&rtParts);
	if(bActive)
		return TransDrawImageSection(&dcMen,rtParts,m_frameActive.imageTop,	RGB(255,0,255));
	else
		return DrawImageSection(&dcMen,rtParts,m_frameActive.imageTop);
	
}
BOOL CWindowSkin::DrawBottom(CDC *pDC,CRect rtWindow,BOOL bActive)
{
	CRect			rtMargins = m_rtMargins;
	CRect			rtParts;	

	rtParts.left	= rtWindow.left + rtMargins.left;
	rtParts.top		= rtWindow.bottom - rtMargins.bottom;
	rtParts.right	= rtWindow.right - rtMargins.right;
	rtParts.bottom	= rtWindow.bottom;
	CMemDC dcMen(pDC,&rtParts);
	if(bActive)
		return TransDrawImageSection(&dcMen,rtParts,m_frameActive.imageBottom,	RGB(255,0,255));
	else
		return DrawImageSection(&dcMen,rtParts,m_frameActive.imageBottom);

}
HRGN CWindowSkin::GetRgn(CDC *pDC,CRect rtDest)
{
	if(!pDC || rtDest.IsRectEmpty())
		return NULL;
	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CSkinBitmap *bmp = new CSkinBitmap;
	bmp->CreateCompatibleBitmap(pDC, rtDest.right, rtDest.bottom);
	CBitmap *pOld = memDC.SelectObject(bmp);

	CRect			rtMargins = m_rtMargins;
	CRect			rtParts;	
	rtParts.left	= rtDest.left + rtMargins.left;
	rtParts.top		= rtDest.top;
	rtParts.right	= rtDest.right - rtMargins.right;
	rtParts.bottom	= rtDest.top + rtMargins.top;
	DrawImageSection(&memDC,rtParts,m_frameActive.imageTop);

	rtParts.left	= rtDest.right - rtMargins.right;
	rtParts.top		= rtDest.top ;
	rtParts.right	= rtDest.right;
	rtParts.bottom	= rtDest.bottom;
	DrawImageSection(&memDC,rtParts,m_frameActive.imageRight);
	
	rtParts.left	= rtDest.left;
	rtParts.top		= rtDest.top ;
	rtParts.right	= rtDest.left + rtMargins.left;
	rtParts.bottom	= rtDest.bottom;
	DrawImageSection(&memDC,rtParts,m_frameActive.imageLeft);

	rtParts.left	= rtDest.left + rtMargins.left;
	rtParts.top		= rtDest.bottom - rtMargins.bottom;
	rtParts.right	= rtDest.right - rtMargins.right;
	rtParts.bottom	= rtDest.bottom;
	DrawImageSection(&memDC,rtParts,m_frameActive.imageBottom);

	//rgn.CreateRectRgn( 0, 0, rtDest.Width(), rtDest.Height() );
	HRGN hrgn = bmp->CreateRgnFromFile( RGB(255,0,255) );

	memDC.SelectObject(pOld);
	memDC.DeleteDC();
	bmp->DeleteObject();
	if(bmp)
		delete bmp;
	return hrgn;

}
void CWindowSkin::DrawBackground(CDC *pDC,CRect rtDest)
{
	DrawImageRect(pDC,rtDest,m_imageBackground);
}
void CWindowSkin::DrawCaption(CDC *pDC,CRect rtDest,int nStateButton)
{
	
}
void CWindowSkin::DrawButton(CDC *pDC,CRect rtParent,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		CRect rtPos;
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			pSkinButton->CalcOffset();
			rtPos = pSkinButton->CalcPosition(rtParent);
			if(pSkinButton->GetWinButtonType() == keMax)
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
			else if(pSkinButton->GetWinButtonType() == keClose)
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
			else if(pSkinButton->GetWinButtonType() == keSysMenu)
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
			else if(pSkinButton->GetWinButtonType() == keMin)
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
			else if(pSkinButton->GetWinButtonType() == keHelp)
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
			else if(pSkinButton->GetWinButtonType() == keRestore)
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
		}
	}
}
void CWindowSkin::DrawMaxButton(CDC *pDC,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			if(pSkinButton->GetWinButtonType() == keMax)
				pSkinButton->DrawButton(pDC,m_rtButtons[keMax],nStateButton);
		}
	}
}
void CWindowSkin::DrawMinButton(CDC *pDC,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			if(pSkinButton->GetWinButtonType() == keMin)
				pSkinButton->DrawButton(pDC,m_rtButtons[keMin],nStateButton);
		}
	}
}
void CWindowSkin::DrawCloseButton(CDC *pDC,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			if(pSkinButton->GetWinButtonType() == keClose)
				pSkinButton->DrawButton(pDC,m_rtButtons[keClose],nStateButton);
		}
	}
}
void CWindowSkin::DrawRestoreButton(CDC *pDC,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			if(pSkinButton->GetWinButtonType() == keRestore)
				pSkinButton->DrawButton(pDC,m_rtButtons[keRestore],nStateButton);
		}
	}
}

void CWindowSkin::DrawMaxButton(CDC *pDC,CRect rtParent,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	CRect rtPos ;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			if(pSkinButton->GetWinButtonType() == keMax)
			{
				pSkinButton->CalcOffset();
				rtPos = pSkinButton->CalcPosition(rtParent);
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
				return;
			}
		}
	}
}
void CWindowSkin::DrawMinButton(CDC *pDC,CRect rtParent,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	CRect rtPos ;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			
			if(pSkinButton->GetWinButtonType() == keMin)
			{
				pSkinButton->CalcOffset();
				rtPos = pSkinButton->CalcPosition(rtParent);
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
				return;
			}
		}
	}
}
void CWindowSkin::DrawCloseButton(CDC *pDC,CRect rtParent,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	CRect rtPos ;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			
			if(pSkinButton->GetWinButtonType() == keClose)
			{
				pSkinButton->CalcOffset();
				rtPos = pSkinButton->CalcPosition(rtParent);
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
				return;
			}
		}
	}
}
void CWindowSkin::DrawRestoreButton(CDC *pDC,CRect rtParent,int nStateButton)
{
	CObjectSkin *pObject = NULL;
	CRect rtPos ;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			CWinButtonSkin *pSkinButton = (CWinButtonSkin*)pObject;
			
			if(pSkinButton->GetWinButtonType() == keRestore)
			{
				pSkinButton->CalcOffset();
				rtPos = pSkinButton->CalcPosition(rtParent);
				pSkinButton->DrawButton(pDC,rtPos,nStateButton);
				return;
			}
		}
	}
}

UINT CWindowSkin::HitTest(CPoint pt)
{
	
	if(m_rtButtons[SKINBUTTON_SYSTEM].PtInRect(pt))
		return HTSYSMENU;
	else if(m_rtButtons[SKINBUTTON_HELP].PtInRect(pt))
		return HTHELP;
	else if(m_rtButtons[SKINBUTTON_MIN].PtInRect(pt))
		return HTMINBUTTON;
	else if(m_rtButtons[SKINBUTTON_MAX].PtInRect(pt))
		return HTMAXBUTTON;
	else if(m_rtButtons[SKINBUTTON_CLOSE].PtInRect(pt))
		return HTCLOSE;
	else if(m_rectParts[SKINPART_TOP_LEFT].PtInRect(pt))
		return HTTOPLEFT;
	else if(m_rectParts[SKINPART_TOP_BORDER].PtInRect(pt))
		return HTTOP;
	else if(m_rectParts[SKINPART_TOP].PtInRect(pt))
		return HTCAPTION;
	else if(m_rectParts[SKINPART_TOP_RIGHT].PtInRect(pt))
		return HTTOPRIGHT;
	else if(m_rectParts[SKINPART_LEFT].PtInRect(pt))
		return HTLEFT;
	else if(m_rectParts[SKINPART_RIGHT].PtInRect(pt))
		return HTRIGHT;
	else if(m_rectParts[SKINPART_BOTTOM_LEFT].PtInRect(pt))
		return HTBOTTOMLEFT;
	else if(m_rectParts[SKINPART_BOTTOM].PtInRect(pt))
		return HTBOTTOM;
	else if(m_rectParts[SKINPART_BOTTOM_RIGHT].PtInRect(pt))
		return HTBOTTOMRIGHT;
	else
		return HTNOWHERE;
}
void CWindowSkin::AddWinButton(CWinButtonSkin *pSkinButton)
{
	if(pSkinButton == NULL)
		return ;
	pSkinButton->SetParent(this);
	m_arrayChildrens.Add(pSkinButton);
	
}
BOOL CWindowSkin::DeleteWinButton(CString strName)
{	
	if(strName.IsEmpty())
		return FALSE;
	CObjectSkin *pObject;
	for(int i = 0 ;i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() == keWinButtonSkin)
		{
			if(pObject->GetName() == strName)
			{
				delete (CWinButtonSkin*)pObject;
				return TRUE;
				m_arrayChildrens.RemoveAt(i);
			}
		}
	}
	return FALSE;
}
int CWindowSkin::GetButtonCount()
{
	return (int)m_arrayChildrens.GetCount();
}
CWinButtonSkin* CWindowSkin::FindButton(CString strName)
{
	if(strName.IsEmpty())
		return NULL;

	CObjectSkin *pObject = NULL;
	for(int i = 0 ;i < m_arrayChildrens.GetCount() ; i++)
	{
		pObject = m_arrayChildrens.GetAt(i);
		if(pObject->GetSkinType() ==  keWinButtonSkin)
		{
			CString strTemp = pObject->GetName();
			if(strTemp == strName)
				return (CWinButtonSkin*)pObject;
		}
	}
	return NULL;
}

////////////////////////////////////////////////
IMPLEMENT_SERIAL(CShapeWndSkin,CObjectSkin,1)
CShapeWndSkin::CShapeWndSkin(void)
{
	m_nSkinType = keWindowSkin;
}
CShapeWndSkin::CShapeWndSkin(CString strName)
:CObjectSkin(strName,keWindowSkin)
{
}
CShapeWndSkin::~CShapeWndSkin(void)
{

}
void CShapeWndSkin::Serialize(CArchive &ar)
{

}