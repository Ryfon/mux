﻿#include "StdAfx.h"
#include "..\include\objectskin.h"
#include "..\include\Skin.h"
/////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CObjectSkin,CObject,1)
CObjectSkin::CObjectSkin(void)
: m_strName(_T(""))
, m_nSkinType(0)
, m_strTag(_T(""))
, m_pParent(NULL)
, m_szMax(0,0)
, m_szMin(0,0)
, m_rtPosition(0,0,300,200)
, m_rtOffset(0,0,0,0)
, m_rtAnchors(0,0,0,0)
, m_nAnchorType(0)
, m_nCtlID(0)
{
}
CObjectSkin::CObjectSkin(CString &strName,int nType )
: m_strName(strName)
, m_nSkinType(nType)
, m_strTag(_T(""))
, m_pParent(NULL)
, m_szMax(0,0)
, m_szMin(0,0)
, m_rtPosition(0,0,300,200)
, m_rtOffset(0,0,0,0)
, m_rtAnchors(0,0,0,0)
, m_nAnchorType(0)
, m_nCtlID(0)
{
}
CObjectSkin::~CObjectSkin(void)
{
	CObjectSkin *pChild = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount(); i++)
	{
		pChild = m_arrayChildrens.GetAt(i);
		if(pChild != NULL)
			delete pChild;
	}
	
	m_arrayChildrens.RemoveAll();
}
void CObjectSkin::Serialize(CArchive &ar)
{
	CObject::Serialize(ar);
	if(ar.IsStoring())
	{
		ar <<m_nAnchorType<< m_rtPosition << m_rtOffset << m_rtAnchors << m_szMin << m_szMax << m_nSkinType
			<< m_strName << m_strTag ;
	}
	else
	{
		ar >>m_nAnchorType>> m_rtPosition >> m_rtOffset >> m_rtAnchors >> m_szMin >> m_szMax >> m_nSkinType
			>> m_strName >> m_strTag;
	}
}
void CObjectSkin::AddChild(CObjectSkin *pObjectSkin)
{
	if(pObjectSkin == NULL)
		return;
	pObjectSkin->SetParent(this);
	m_arrayChildrens.Add(pObjectSkin);
}

void CObjectSkin::DeleteChild(CString strName)
{
	CObjectSkin *pChild = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount(); i++)
	{
		pChild = m_arrayChildrens.GetAt(i);
		if(pChild ->GetName() == strName)
		{
			delete pChild;
			m_arrayChildrens.RemoveAt(i);
		}
	}
}
int	CObjectSkin::GetChildrenCount()
{
	return (int)m_arrayChildrens.GetCount();
}
CObjectSkin* CObjectSkin::GetAt(int nIndex)
{
	int nCount = m_arrayChildrens.GetCount();
	if(nIndex >= nCount)
		return NULL;
	CObjectSkin *pObject = NULL;
	pObject = m_arrayChildrens.GetAt(nIndex);
	return pObject;
}
CObjectSkin* CObjectSkin::GetParent()
{
	return m_pParent;
}

void CObjectSkin::SetParent(CObjectSkin *pObjectSkin)
{
	m_pParent = pObjectSkin;
}
CObjectSkin* CObjectSkin::FindChild(CString strName)
{
	CObjectSkin* pObjectSkin = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount(); i++)
	{
		pObjectSkin = m_arrayChildrens.GetAt(i);
		if(pObjectSkin->GetName() == strName)
			return pObjectSkin;
	}
	return pObjectSkin;
}
void CObjectSkin::CalcOffset()
{
	if(m_pParent != NULL)
	{
		CRect rectParent	= m_pParent->GetPosition();
		m_rtOffset.left		= m_rtPosition.left - rectParent.Width()*m_rtAnchors.left/100;
		m_rtOffset.top		= m_rtPosition.top	- rectParent.Height()*m_rtAnchors.top/100;
		m_rtOffset.right	= m_rtPosition.right - rectParent.Width()*m_rtAnchors.right/100;
		m_rtOffset.bottom	= m_rtPosition.bottom - rectParent.Height()*m_rtAnchors.bottom/100;
	}
}

void CObjectSkin::CalcPosition()
{
	if(m_pParent != NULL)
	{
		CRect rectParent	= m_pParent->GetPosition();

		m_rtPosition.left = rectParent.Width() * m_rtAnchors.left/100 + m_rtOffset.left;
		m_rtPosition.top = rectParent.Height() * m_rtAnchors.top/100 + m_rtOffset.top;
		m_rtPosition.right = rectParent.Width() * m_rtAnchors.right/100 + m_rtOffset.right;
		m_rtPosition.bottom = rectParent.Height() * m_rtAnchors.bottom/100 + m_rtOffset.bottom;
	}
	CObjectSkin *pObjectSkin = NULL;
	for(int i = 0 ; i < m_arrayChildrens.GetCount() ;i ++)
	{
		pObjectSkin = m_arrayChildrens.GetAt(i);
		if(pObjectSkin)
			pObjectSkin->CalcPosition();
	}
}
CRect CObjectSkin::CalcPosition(CRect rtParent)
{
	CRect rtPosition;
	rtPosition.left		= rtParent.Width() * m_rtAnchors.left/100 + m_rtOffset.left;
	rtPosition.top		= rtParent.Height() * m_rtAnchors.top/100 + m_rtOffset.top;
	rtPosition.right	= rtParent.Width() * m_rtAnchors.right/100 + m_rtOffset.right;
	rtPosition.bottom	= rtParent.Height() * m_rtAnchors.bottom/100 + m_rtOffset.bottom;
	rtPosition.OffsetRect(rtParent.left,rtParent.top);
	return rtPosition;
}
void CObjectSkin::DrawSkin(CDC *pDC)
{

}
int CObjectSkin::GetWidth()
{
	return m_rtPosition.Width();
}
int CObjectSkin::GetHeight()
{
	return m_rtPosition.Height();
}
void CObjectSkin::SetName(const CString strName)
{
	m_strName = strName; 
}
CString	CObjectSkin::GetName()
{
	return m_strName; 
}
void CObjectSkin::SetTag(const CString strTag)
{
	m_strTag = strTag;
}
CString	CObjectSkin::GetTag()
{
	return m_strTag; 
}
void CObjectSkin::SetSkinType(int nSkinType)
{
	m_nSkinType = nSkinType; 
}
int	CObjectSkin::GetSkinType()
{
	return m_nSkinType; 
}
void CObjectSkin::SetPosition(const SkinRect &rect)
{
	m_rtPosition = rect; 
}
SkinRect CObjectSkin::GetPosition()
{
	return m_rtPosition; 
}
void CObjectSkin::SetOffset(const SkinRect &offset)
{
	m_rtOffset = offset; 
}
SkinRect CObjectSkin::GetOffset()
{
	return m_rtOffset;
}
void CObjectSkin::SetAnchors(int anchorType)
{
	m_nAnchorType = anchorType;m_rtAnchors = anchorsType[anchorType];
}
int CObjectSkin::GetAnchors()
{
	return m_nAnchorType;
}
void CObjectSkin::SetID(int nID)
{
	m_nCtlID = nID;
}
int	CObjectSkin::GetID()
{
	return m_nCtlID;
}
BOOL CObjectSkin::DrawImageRect(CDC *pDC,CRect rtDest,SkinImageRect & imageRect)
{
	/*CSkinBitmap *pImage = GetImages()->FindImage(imageRect.strImageName);
	if(pImage == NULL)
		return FALSE;

	pImage->Draw(pDC,&rtDest,imageRect.rtImagePos);*/
	// test only [6/25/2009 hemeng]
	pDC->SetTextColor(RGB(255,255,255));
	return TransDrawImageRect(pDC,rtDest, imageRect,RGB(255,0,255));

	//return TRUE;
}
//*********************
//* 1	*	  2		* 3	*
//*********************
//* 4	*	  5		* 6	* 
//*	*			*	*
//*********************
//* 7	*	  8		* 9	*
//*********************
BOOL CObjectSkin::DrawImageSection(CDC *pDC,CRect rtDest,SkinImageSection &imageSection)
{
	// test only [6/25/2009 hemeng]
	pDC->SetTextColor(RGB(255,255,255));
	return TransDrawImageSection(pDC,rtDest,imageSection,RGB(255,0,255));
	
	//CSkinBitmap *pImage = GetImages()->FindImage(imageSection.strImageName);
	//if(pImage == NULL)
	//	return FALSE;

	//SkinRect src,dst;
	//if(pImage)
	//{
	//	// draw part one
	//	src.left	= imageSection.rtImagePos.left;
	//	src.top		= imageSection.rtImagePos.top ;
	//	src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

	//	dst.left	= rtDest.left;
	//	dst.top		= rtDest.top ;
	//	dst.right	= rtDest.left + imageSection.marginImage.left;
	//	dst.bottom	= rtDest.top + imageSection.marginImage.top ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//		pImage->Draw(pDC,dst,src);
	//	//draw part two
	//	src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.top		= imageSection.rtImagePos.top ;
	//	src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

	//	dst.left	= rtDest.left + imageSection.marginImage.left;
	//	dst.top		= rtDest.top ;
	//	dst.right	= rtDest.right - imageSection.marginImage.right;
	//	dst.bottom	= rtDest.top + imageSection.marginImage.top ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchTop)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);

	//	}
	//	//draw part three
	//	src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.top		= imageSection.rtImagePos.top ;
	//	src.right	= imageSection.rtImagePos.right ;
	//	src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

	//	dst.left	= rtDest.right - imageSection.marginImage.right;
	//	dst.top		= rtDest.top ;
	//	dst.right	= rtDest.right ;
	//	dst.bottom	= rtDest.top + imageSection.marginImage.top ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//		pImage->Draw(pDC,dst,src);
	//	//draw part four
	//	src.left	= imageSection.rtImagePos.left;
	//	src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top ;
	//	src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;

	//	dst.left	= rtDest.left;
	//	dst.top		= rtDest.top + imageSection.marginImage.top ;
	//	dst.right	= rtDest.left + imageSection.marginImage.left;
	//	dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchLeft)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);

	//	}
	//	//draw part five
	//	src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top;
	//	src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom;

	//	dst.left	= rtDest.left + imageSection.marginImage.left;
	//	dst.top		= rtDest.top + imageSection.marginImage.top;
	//	dst.right	= rtDest.right - imageSection.marginImage.right;
	//	dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchClient)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);

	//	}
	//	//draw part six
	//	src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top;
	//	src.right	= imageSection.rtImagePos.right;
	//	src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom;

	//	dst.left	= rtDest.right - imageSection.marginImage.right;
	//	dst.top		= rtDest.top + imageSection.marginImage.top;
	//	dst.right	= rtDest.right;
	//	dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchRight)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);

	//	}

	//	//draw part seven
	//	src.left	= imageSection.rtImagePos.left;
	//	src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
	//	src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.bottom	= imageSection.rtImagePos.bottom ;

	//	dst.left	= rtDest.left;
	//	dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
	//	dst.right	= rtDest.left + imageSection.marginImage.left;
	//	dst.bottom	= rtDest.bottom ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//		pImage->Draw(pDC,dst,src);
	//	//draw part eight
	//	src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
	//	src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.bottom	= imageSection.rtImagePos.bottom ;

	//	dst.left	= rtDest.left + imageSection.marginImage.left;
	//	dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
	//	dst.right	= rtDest.right - imageSection.marginImage.right;
	//	dst.bottom	= rtDest.bottom ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchBottom)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);
	//	}
	//	//draw part nine
	//	src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
	//	src.right	= imageSection.rtImagePos.right;
	//	src.bottom	= imageSection.rtImagePos.bottom ;

	//	dst.left	= rtDest.right - imageSection.marginImage.right;
	//	dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
	//	dst.right	= rtDest.right;
	//	dst.bottom	= rtDest.bottom ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//		pImage->Draw(pDC,dst,src);

	//}

	//return TRUE;
}

BOOL CObjectSkin::TransDrawImageRect(CDC *pDC,CRect rtDest,SkinImageRect &imageRect,COLORREF crColour)
{
	CSkinBitmap *pImage = GetImages()->FindImage(imageRect.strImageName);
	if(!pImage)
		return FALSE;

	pImage->TransparentDraw(pDC,crColour,&rtDest,imageRect.rtImagePos);
	return TRUE;
}
//*********************
//* 1	*	  2		* 3	*
//*********************
//* 4	*	  5		* 6	* 
//*	*			*	*
//*********************
//* 7	*	  8		* 9	*
//*********************
BOOL CObjectSkin::TransDrawImageSection(CDC *pDC,CRect rtDest,SkinImageSection &imageSection,COLORREF crColour)
{
	CSkinBitmap *pImage = GetImages()->FindImage(imageSection.strImageName);
	if(!pImage)
		return FALSE;

	SkinRect src,dst;

	if(pImage)
	{
		// draw part one
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 && src.Height() != 0)
			pImage->AllStretchTransDraw(pDC,crColour,dst,src);
		//draw part two
		src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.left + imageSection.marginImage.left;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.right - imageSection.marginImage.right;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchTop)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);

		}
		//draw part three
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.right ;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.right ;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 && src.Height() != 0)
			pImage->AllStretchTransDraw(pDC,crColour,dst,src);
		//draw part four
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.top + imageSection.marginImage.top ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchLeft)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);

		}
		//draw part five
		src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top;
		src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom;

		dst.left	= rtDest.left + imageSection.marginImage.left;
		dst.top		= rtDest.top + imageSection.marginImage.top;
		dst.right	= rtDest.right - imageSection.marginImage.right;
		dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchClient)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
				
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);

		}
		//draw part six
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top;
		src.right	= imageSection.rtImagePos.right;
		src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.top + imageSection.marginImage.top;
		dst.right	= rtDest.right;
		dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchRight)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);

		}

		//draw part seven
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 && src.Height() != 0)
			pImage->AllStretchTransDraw(pDC,crColour,dst,src);
		//draw part eight
		src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.left + imageSection.marginImage.left;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.right - imageSection.marginImage.right;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchBottom)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);
		}
		//draw part nine
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.right;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.right;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 && src.Height() != 0)
			pImage->AllStretchTransDraw(pDC,crColour,dst,src);

	}

	return TRUE;
}
BOOL CObjectSkin::DrawImageBorder(CDC *pDC,CRect rtDest,SkinImageSection &imageSection)
{
	// test only [6/25/2009 hemeng]
	pDC->SetTextColor(RGB(255,255,255));
	return TransDrawImageBorder(pDC,rtDest,imageSection, RGB(255,0,255));

	//CSkinBitmap *pImage = GetImages()->FindImage(imageSection.strImageName);
	//if(pImage == NULL)
	//	return FALSE;

	//SkinRect src,dst;
	//if(pImage)
	//{
	//	// draw part one
	//	src.left	= imageSection.rtImagePos.left;
	//	src.top		= imageSection.rtImagePos.top ;
	//	src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

	//	dst.left	= rtDest.left;
	//	dst.top		= rtDest.top ;
	//	dst.right	= rtDest.left + imageSection.marginImage.left;
	//	dst.bottom	= rtDest.top + imageSection.marginImage.top ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//		pImage->Draw(pDC,dst,src);
	//	//draw part two
	//	src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.top		= imageSection.rtImagePos.top ;
	//	src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

	//	dst.left	= rtDest.left + imageSection.marginImage.left;
	//	dst.top		= rtDest.top ;
	//	dst.right	= rtDest.right - imageSection.marginImage.right;
	//	dst.bottom	= rtDest.top + imageSection.marginImage.top ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchTop)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);

	//	}
	//	//draw part three
	//	src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.top		= imageSection.rtImagePos.top ;
	//	src.right	= imageSection.rtImagePos.right ;
	//	src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

	//	dst.left	= rtDest.right - imageSection.marginImage.right;
	//	dst.top		= rtDest.top ;
	//	dst.right	= rtDest.right ;
	//	dst.bottom	= rtDest.top + imageSection.marginImage.top ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//		pImage->Draw(pDC,dst,src);
	//	//draw part four
	//	src.left	= imageSection.rtImagePos.left;
	//	src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top ;
	//	src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;

	//	dst.left	= rtDest.left;
	//	dst.top		= rtDest.top + imageSection.marginImage.top ;
	//	dst.right	= rtDest.left + imageSection.marginImage.left;
	//	dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchLeft)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);

	//	}

	//	//draw part six
	//	src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top;
	//	src.right	= imageSection.rtImagePos.right;
	//	src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom;

	//	dst.left	= rtDest.right - imageSection.marginImage.right;
	//	dst.top		= rtDest.top + imageSection.marginImage.top;
	//	dst.right	= rtDest.right;
	//	dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchRight)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);

	//	}

	//	//draw part seven
	//	src.left	= imageSection.rtImagePos.left;
	//	src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
	//	src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.bottom	= imageSection.rtImagePos.bottom ;

	//	dst.left	= rtDest.left;
	//	dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
	//	dst.right	= rtDest.left + imageSection.marginImage.left;
	//	dst.bottom	= rtDest.bottom ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//		pImage->Draw(pDC,dst,src);
	//	//draw part eight
	//	src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
	//	src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
	//	src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.bottom	= imageSection.rtImagePos.bottom ;

	//	dst.left	= rtDest.left + imageSection.marginImage.left;
	//	dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
	//	dst.right	= rtDest.right - imageSection.marginImage.right;
	//	dst.bottom	= rtDest.bottom ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//	{
	//		if(imageSection.stretchDrawMode == keStretchAll ||
	//			imageSection.stretchDrawMode == keStretchBottom)
	//			pImage->Draw(pDC,dst,src);
	//		else
	//			pImage->TitleDraw(pDC,dst,src);
	//	}
	//	//draw part nine
	//	src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
	//	src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
	//	src.right	= imageSection.rtImagePos.right;
	//	src.bottom	= imageSection.rtImagePos.bottom ;

	//	dst.left	= rtDest.right - imageSection.marginImage.right;
	//	dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
	//	dst.right	= rtDest.right;
	//	dst.bottom	= rtDest.bottom ;
	//	if(src.Width() != 0 && src.Height() != 0)
	//		pImage->Draw(pDC,dst,src);

	//}

	//return TRUE;

}
//*********************
//* 1	*	  2		* 3	*
//*********************
//* 4	*	  5		* 6	* 
//*	*			*	*
//*********************
//* 7	*	  8		* 9	*
//*********************
//this function not draw client area.
BOOL CObjectSkin::TransDrawImageBorder(CDC *pDC,CRect rtDest,SkinImageSection &imageSection,COLORREF crColour)
{
	CSkinBitmap *pImage = GetImages()->FindImage(imageSection.strImageName);
	if(!pImage)
		return FALSE;

	SkinRect src,dst;

	if(pImage)
	{
		// draw part one
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 && src.Height() != 0)
			pImage->AllStretchTransDraw(pDC,crColour,dst,src);
		//draw part two
		src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.left + imageSection.marginImage.left;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.right - imageSection.marginImage.right;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchTop)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);

		}
		//draw part three
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.right ;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.right ;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 && src.Height() != 0)
			pImage->AllStretchTransDraw(pDC,crColour,dst,src);
		//draw part four
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.top + imageSection.marginImage.top ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchLeft)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);

		}
		//draw part six
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top;
		src.right	= imageSection.rtImagePos.right;
		src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.top + imageSection.marginImage.top;
		dst.right	= rtDest.right;
		dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchRight)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);

		}

		//draw part seven
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 && src.Height() != 0)
			pImage->AllStretchTransDraw(pDC,crColour,dst,src);
		//draw part eight
		src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.left + imageSection.marginImage.left;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.right - imageSection.marginImage.right;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 && src.Height() != 0)
		{
			if(imageSection.stretchDrawMode == keStretchAll ||
				imageSection.stretchDrawMode == keStretchBottom)
				pImage->AllStretchTransDraw(pDC,crColour,dst,src);
			else
				pImage->TitleTransDraw(pDC,crColour,dst,src);
		}
		//draw part nine
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.right;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.right;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 && src.Height() != 0)
			pImage->AllStretchTransDraw(pDC,crColour,dst,src);

	}

	return TRUE;
}