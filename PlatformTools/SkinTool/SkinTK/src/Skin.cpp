﻿#include "StdAfx.h"
#include "..\include\skin.h"
#include "..\include\WindowSkin.h"
#include "..\include\ButtonSkin.h"
#include "..\include\ScrollBarSkin.h"
#include "..\include\StaticSkin.h"
#include "..\include\ButtonSkin.h"
#include "..\include\RadioSkin.h"
#include "..\include\CheckBoxSkin.h"
#include "..\include\GroupBoxSkin.h"
#include "..\include\EditSkin.h"
#include "..\include\ComboBoxSkin.h"
#include "..\include\ListBoxSkin.h"
#include "..\include\PopupMenuSkin.h"
#include "..\include\ProgressSkin.h"
#include "..\include\SpinSkin.h"
#include "..\include\HeaderSkin.h"
#include "..\include\SlideSkin.h"
#include "..\include\TabSkin.h"
#include "..\include\StatusbarSkin.h"
#include "..\include\ToolBarSkin.h"

///////////////////////////////////////////////////////
#define SKIN_IMAGE_MARK 22;
#define SKIN_OBJECT_MARK 33;
CSkin theSkin;
CSkin& GetSkin()
{
	return theSkin;
}
CSkinImages* GetImages()
{
	return theSkin.GetImages();
}
CSkinBitmap* GetImage(CString in_strName)
{
	return GetImages()->FindImage(in_strName);
}
//IMPLEMENT_SERIAL(CSkin,CObject,1)
CSkin::CSkin(void)
:m_pImages(NULL)
{
	m_pImages = new CSkinImages();
	m_nSkinVersion = 01;
}
CSkin::~CSkin(void)
{
	if(m_pImages)
		delete m_pImages;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);
		delete pObject;
	}
	m_arraySkinObjects.RemoveAll();
}
void CSkin::ClearSkin()
{
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);
		delete pObject;
	}
	m_arraySkinObjects.RemoveAll();
}
void CSkin::LoadSkin(CString strSkinFile)
{
	ClearSkin();
	
	CFile			cFile;
	CFileException	e;

	if (cFile.Open(strSkinFile, CFile::modeRead | CFile::typeBinary, &e))
	{
		CArchive ar(&cFile,CArchive::load);
		ar.Read(&m_nSkinVersion,sizeof(m_nSkinVersion));
		int imageMark;
		ar.Read(&imageMark,sizeof(imageMark));
		if(!m_pImages)
			m_pImages = new CSkinImages;
		m_pImages->Serialize(ar);

		ar.Read(&m_Application,sizeof(m_Application));
		int nObjectSkin = 0;
		ar.Read(&nObjectSkin,sizeof(nObjectSkin));
		for(int i = 0 ;i < nObjectSkin ; i++)
		{
			CObjectSkin *pObject = NULL;
			int nType = keUnknowSkin;
			ar.Read(&nType,sizeof(nType));

			if(nType == keWindowSkin)
			{
				pObject = new CWindowSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keButtonSkin)
			{
				pObject = new CButtonSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keScrollBarSkin)
			{
				pObject = new CScrollBarSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keRadioSkin)
			{
				pObject = new CRadioSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keCheckBoxSkin)
			{
				pObject = new CCheckBoxSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keGroupBoxSkin)
			{
				pObject = new CGroupBoxSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keStaticSkin)
			{
				pObject = new CStaticSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keEditSkin)
			{
				pObject = new CEditSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keComboxSkin)
			{
				pObject = new CComboBoxSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keListBoxSkin)
			{
				pObject = new CListBoxSkin;
				pObject->Serialize(ar);
			}
			else if(nType == kePopMenuSkin)
			{
				pObject = new CPopupMenuSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keProgressSkin)
			{
				pObject = new CProgressSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keSpinSkin)
			{
				pObject = new CSpinSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keHeaderSkin)
			{
				pObject = new CHeaderSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keSliderSkin)
			{
				pObject = new CSliderSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keTabSkin)
			{
				pObject = new CTabSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keStatusbarSkin)
			{
				pObject = new CStatusBarSkin;
				pObject->Serialize(ar);
			}
			else if(nType == keToolbarSkin)
			{
				pObject = new CToolBarSkin;
				pObject->Serialize(ar);
			}
					
			m_arraySkinObjects.Add(pObject);
		}

		ar.Close();
		cFile.Close();

	}
}
void CSkin::SaveSkin(CString strSkinFile)
{
	CFile			cFile;
	CFileException	e;

	if (cFile.Open(strSkinFile, CFile::modeCreate |
		CFile::modeReadWrite | CFile::shareExclusive, &e))
	{
		CArchive ar(&cFile,CArchive::store);
		ar.Write(&m_nSkinVersion,sizeof(m_nSkinVersion));
		int imageMark = SKIN_IMAGE_MARK;
		ar.Write(&imageMark,sizeof(imageMark));
		if(!m_pImages)
			m_pImages = new CSkinImages;
		m_pImages->Serialize(ar);

		ar.Write(&m_Application,sizeof(m_Application));
		
		int nObjectSkin = m_arraySkinObjects.GetCount();
		ar.Write(&nObjectSkin,sizeof(nObjectSkin));
		for(int i = 0 ; i < nObjectSkin ; i++)
		{
			CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);
			int nType = pObject->GetSkinType();
			ar.Write(&nType,sizeof(nType));
			pObject->Serialize(ar);
		}
	}
}

BOOL CSkin::LoadImage(CString strImageFile)
{
	return m_pImages->LoadImage(strImageFile);
}

BOOL CSkin::DeleteImage(CString strImageFile)
{
	return m_pImages->DeleteImage(strImageFile);
}

void CSkin::SaveImage(CString strImageFile)
{
	
}
CSkinBitmap* CSkin::FindIamge(CString strImageFile)
{
	return m_pImages->FindImage(strImageFile);
}
CObjectSkin* CSkin::AddWindowSkin(CString strName)
{
	CObjectSkin *pObject = new CWindowSkin(strName);
	if(!pObject)
		return NULL;
//	pObject->strName = _T("MainFrame");
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CWindowSkin* CSkin::GetWindowSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keWindowSkin)
			return (CWindowSkin*)pObject;
	}
	return NULL;
}

CObjectSkin* CSkin::AddScrollBarSkin(CString strName)
{
	CObjectSkin *pObject = new CScrollBarSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}

CScrollBarSkin* CSkin::GetScrollBarSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keScrollBarSkin)
			return (CScrollBarSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddStaticSkin(CString strName)
{
	CObjectSkin *pObject = new CStaticSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CStaticSkin* CSkin::GetStaticSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keStaticSkin)
			return (CStaticSkin*)pObject;
	}
	return NULL;
}

CObjectSkin* CSkin::AddButtonSkin(CString strName)
{
	CObjectSkin *pObject = new CButtonSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CButtonSkin* CSkin::GetButtonSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keButtonSkin)
			return (CButtonSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddRadioSkin(CString strName)
{
	CObjectSkin *pObject = new CRadioSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CRadioSkin*	 CSkin::GetRadioSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keRadioSkin)
			return (CRadioSkin*)pObject;
	}
	return NULL;
}

CObjectSkin* CSkin::AddCheckBoxSkin(CString strName)
{
	CObjectSkin *pObject = new CCheckBoxSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CCheckBoxSkin* CSkin::GetCheckBoxSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keCheckBoxSkin)
			return (CCheckBoxSkin*)pObject;
	}
	return NULL;
}

CObjectSkin* CSkin::AddGroupBoxSkin(CString strName)
{
	CObjectSkin *pObject = new CGroupBoxSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CGroupBoxSkin* CSkin::GetGroupBoxSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keGroupBoxSkin)
			return (CGroupBoxSkin*)pObject;
	}
	return NULL;
}

CObjectSkin* CSkin::AddEditSkin(CString strName)
{
	CObjectSkin *pObject = new CEditSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CEditSkin* CSkin::GetEditSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keEditSkin)
			return (CEditSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddComboxSkin(CString strName)
{
	CObjectSkin *pObject = new CComboBoxSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CComboBoxSkin* CSkin::GetComboxSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keComboxSkin)
			return (CComboBoxSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddListBoxSkin(CString strName)
{
	CObjectSkin *pObject = new CListBoxSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CListBoxSkin* CSkin::GetListBoxSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keListBoxSkin)
			return (CListBoxSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddPopupMenuSkin(CString strName)
{
	CObjectSkin *pObject = new CPopupMenuSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CPopupMenuSkin*	CSkin::GetPopupMenuSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == kePopMenuSkin)
			return (CPopupMenuSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddProgressSkin(CString strName)
{
	CObjectSkin *pObject = new CProgressSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CProgressSkin* CSkin::GetProgressSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keProgressSkin)
			return (CProgressSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddSpinSkin(CString strName)
{
	CObjectSkin *pObject = new CSpinSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CSpinSkin* CSkin::GetSpinSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keSpinSkin)
			return (CSpinSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddHeaderSkin(CString strName)
{
	CObjectSkin *pObject = new CHeaderSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CHeaderSkin* CSkin::GetHeaderSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keHeaderSkin)
			return (CHeaderSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddSliderSkin(CString strName)
{
	CObjectSkin *pObject = new CSliderSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CSliderSkin* CSkin::GetSliderSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keSliderSkin)
			return (CSliderSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddTabSkin(CString strName)
{
	CObjectSkin *pObject = new CTabSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CTabSkin* CSkin::GetTabSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keTabSkin)
			return (CTabSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddStatusBarSkin(CString strName)
{
	CObjectSkin *pObject = new CStatusBarSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CStatusBarSkin*	CSkin::GetStatusBarSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keStatusbarSkin)
			return (CStatusBarSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::AddToolBarSkin(CString strName)
{
	CObjectSkin *pObject = new CToolBarSkin(strName);
	if(!pObject)
		return NULL;
	m_arraySkinObjects.Add(pObject);
	return pObject;
}
CToolBarSkin* CSkin::GetToolBarSkin()
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		CObjectSkin *pObject = m_arraySkinObjects.GetAt(i);

		if(pObject->GetSkinType() == keToolbarSkin)
			return (CToolBarSkin*)pObject;
	}
	return NULL;
}
CObjectSkin* CSkin::FindObjectSkin(CString strName)
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		pObject = m_arraySkinObjects.GetAt(i);

		CString strTemp = pObject->GetName();
		if(strTemp == strName)
			return pObject;
	}
	return NULL;
}

void CSkin::DeleteObjectSkin(CString strName)
{
	CObjectSkin *pObject = NULL;
	for(int i = 0 ; i < m_arraySkinObjects.GetCount() ; i++)
	{
		pObject = m_arraySkinObjects.GetAt(i);

		CString strTemp = pObject->GetName();
		if(strTemp == strName)
		{
			m_arraySkinObjects.RemoveAt(i);
			delete pObject;
		}
	}
}
//void CSkin::AddChild(CObjectSkin *pObjectSkin)
//{
//	if(pObjectSkin == NULL)
//		return;
//	pObjectSkin->SetParent(NULL);
//	m_listTemplate.AddTail(pObjectSkin);
//}
//void CSkin::DeleteChild(CString strName)
//{
//	CObjectSkin* pObjectSkin = NULL;
//	POSITION pos = m_listTemplate.GetHeadPosition();
//	while(pos)
//	{
//		pObjectSkin = m_listTemplate.GetNext(pos);
//		if(pObjectSkin->GetName() == strName)
//		{
//			delete pObjectSkin;
//			break;
//		}
//	}
//	m_listTemplate.RemoveAt(pos);
//}
//int	CSkin::GetChildrenCount()
//{
//	return (int)m_listTemplate.GetCount();
//}
//CObjectSkin* CSkin::FindChild(CString strName)
//{
//	CObjectSkin* pObjectSkin = NULL;
//	POSITION pos = m_listTemplate.GetHeadPosition();
//	while(pos)
//	{
//		pObjectSkin = m_listTemplate.GetNext(pos);
//		if(pObjectSkin->GetName() == strName)
//			return pObjectSkin;
//	}
//	return pObjectSkin;
//}