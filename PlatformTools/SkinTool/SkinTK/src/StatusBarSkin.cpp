﻿#include "StdAfx.h"
#include "..\include\statusbarskin.h"

IMPLEMENT_SERIAL(CStatusBarSkin,CObjectSkin,1)
CStatusBarSkin::CStatusBarSkin(void)
{
	m_nSkinType = keStatusbarSkin;
}
CStatusBarSkin::CStatusBarSkin(CString strName)
:CObjectSkin(strName,keStatusbarSkin)
{

}
CStatusBarSkin::~CStatusBarSkin(void)
{
}
void CStatusBarSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar.Write(&m_imageItem,sizeof(m_imageItem));
		ar.Write(&m_colorBack,sizeof(m_colorBack));
	}
	else
	{
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar.Read(&m_imageItem,sizeof(m_imageItem));
		ar.Read(&m_colorBack,sizeof(m_colorBack));
	}
}