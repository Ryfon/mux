﻿#include "StdAfx.h"
#include "..\include\spinskin.h"


IMPLEMENT_SERIAL(CSpinSkin,CObjectSkin,1)
CSpinSkin::CSpinSkin(void)
{
	m_nSkinType = keSpinSkin;
}
CSpinSkin::CSpinSkin(CString strName)
:CObjectSkin(strName,keSpinSkin)
{
}
CSpinSkin::~CSpinSkin(void)
{
}
void CSpinSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(m_thumbHort,sizeof(m_thumbHort));
		ar.Write(m_thumbVert,sizeof(m_thumbVert));	
	}
	else
	{
		ar.Read(m_thumbHort,sizeof(m_thumbHort));
		ar.Read(m_thumbVert,sizeof(m_thumbVert));
	}
}