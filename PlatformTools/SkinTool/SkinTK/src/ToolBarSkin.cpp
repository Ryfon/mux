﻿#include "StdAfx.h"
#include "..\include\toolbarskin.h"

IMPLEMENT_SERIAL(CToolBarSkin,CObjectSkin,1)
CToolBarSkin::CToolBarSkin(void)
{
	m_nSkinType = keToolbarSkin;
}
CToolBarSkin::CToolBarSkin(CString strName)
:CObjectSkin(strName,keToolbarSkin)
{
	m_nSkinType = keToolbarSkin;
}

CToolBarSkin::~CToolBarSkin(void)
{
}
void CToolBarSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar.Write(&m_imageItemNormal,sizeof(m_imageItemNormal));
		ar.Write(&m_imageItemHover,sizeof(m_imageItemHover));
		ar.Write(&m_imageItemPressed,sizeof(m_imageItemPressed));
		ar.Write(&m_imageItemDisabled,sizeof(m_imageItemDisabled));
	}
	else
	{
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar.Read(&m_imageItemNormal,sizeof(m_imageItemNormal));
		ar.Read(&m_imageItemHover,sizeof(m_imageItemHover));
		ar.Read(&m_imageItemPressed,sizeof(m_imageItemPressed));
		ar.Read(&m_imageItemDisabled,sizeof(m_imageItemDisabled));
	}
}