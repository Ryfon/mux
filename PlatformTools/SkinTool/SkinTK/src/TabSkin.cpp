﻿#include "StdAfx.h"
#include "..\include\tabskin.h"

IMPLEMENT_SERIAL(CTabSkin,CObjectSkin,1)
CTabSkin::CTabSkin(void)
{
	m_nSkinType = keTabSkin;
}
CTabSkin::CTabSkin(CString strName)
:CObjectSkin(strName,keTabSkin)
{

}
CTabSkin::~CTabSkin(void)
{
}
void CTabSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar.Write(&m_imageBorder,sizeof(m_imageBorder));	
		ar.Write(&m_imageTabNormal,sizeof(m_imageTabNormal));
		ar.Write(&m_imageTabHover,sizeof(m_imageTabHover));
		ar.Write(&m_imageTabPressed,sizeof(m_imageTabPressed));	
	}
	else
	{
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar.Read(&m_imageBorder,sizeof(m_imageBorder));	
		ar.Read(&m_imageTabNormal,sizeof(m_imageTabNormal));
		ar.Read(&m_imageTabHover,sizeof(m_imageTabHover));
		ar.Read(&m_imageTabPressed,sizeof(m_imageTabPressed));
	}
}