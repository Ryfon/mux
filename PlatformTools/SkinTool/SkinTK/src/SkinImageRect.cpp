﻿#include "StdAfx.h"
#include "..\include\skinimagerect.h"
///////////////////////////////////////
//IMPLEMENT_SERIAL(CSkinImageRect,CObject,1)
//CSkinImageRect::CSkinImageRect(void)
//: m_strImageName(_T(""))
//{
//}
//
//CSkinImageRect::~CSkinImageRect(void)
//{
//}
//void CSkinImageRect::Serialize(CArchive &ar)
//{
//	CObject::Serialize(ar);
//	if(ar.IsStoring())
//	{
//		ar << m_strImageName << m_rectImagePos;
//	}
//	else
//	{
//		ar >> m_strImageName >> m_rectImagePos;
//	}
//}
//void CSkinImageRect::Draw(CDC *pDC,SkinRect rectDest)
//{
//	if(m_strImageName.IsEmpty())	
//		pDC->Rectangle(rectDest);
//	else
//	{
//		CSkinBitmap *pImage = GetImage(m_strImageName);
//		if(pImage)
//			pImage->Draw(pDC,rectDest,m_rectImagePos);
//	}
//}
/////////////////////////////////////////////
///*
//
//*********************
//* 1	*	  2		* 3	*
//*********************
//* 4	*	  5		* 6	* 
//*	*			*	*
//*********************
//* 7	*	  8		* 9	*
//*********************
//*/
//IMPLEMENT_SERIAL(CSkinImageSection,CSkinImageRect,1)
//CSkinImageSection::CSkinImageSection(void)
//: m_stretchImage(keNoStretch)
//{
//}
//
//CSkinImageSection::~CSkinImageSection(void)
//{
//}
//void CSkinImageSection::Serialize(CArchive &ar)
//{
//	CSkinImageRect::Serialize(ar);
//	if(ar.IsStoring())
//	{
//		ar << m_marginsImage << m_stretchImage;
//	}
//	else
//	{
//		ar >> m_marginsImage >> m_stretchImage;
//	}
//}
//void CSkinImageSection::Draw(CDC *pDC,SkinRect rectDest)
//{
//	if(m_strImageName.IsEmpty())
//	{
//		pDC->Rectangle(rectDest);
//		return;
//	}
//	CSkinBitmap *pImage = GetImage(m_strImageName);
	SkinRect src,dst;

	if(pImage)
	{
		//part one
		src.left	= m_rectImagePos.left;
		src.top		= m_rectImagePos.top ;
		src.right	= m_rectImagePos.left + m_marginsImage.left;
		src.bottom	= m_rectImagePos.top + m_marginsImage.top ;
		
		dst.left	= rectDest.left;
		dst.top		= rectDest.top ;
		dst.right	= rectDest.left + m_marginsImage.left;
		dst.bottom	= rectDest.top + m_marginsImage.top ;
		if(src.Width() != 0 || src.Height() != 0)
			pImage->Draw(pDC,dst,src);
		//part two
		src.left	= m_rectImagePos.left + m_marginsImage.left;
		src.top		= m_rectImagePos.top ;
		src.right	= m_rectImagePos.right - m_marginsImage.right;
		src.bottom	= m_rectImagePos.top + m_marginsImage.top ;

		dst.left	= rectDest.left + m_marginsImage.left;
		dst.top		= rectDest.top ;
		dst.right	= rectDest.right - m_marginsImage.right;
		dst.bottom	= rectDest.top + m_marginsImage.top ;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(m_stretchImage == keStretchAll ||
			   m_stretchImage == keStretchTop)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);

		}
					//part three
		src.left	= m_rectImagePos.right - m_marginsImage.right;
		src.top		= m_rectImagePos.top ;
		src.right	= m_rectImagePos.right ;
		src.bottom	= m_rectImagePos.top + m_marginsImage.top ;

		dst.left	= rectDest.right - m_marginsImage.right;
		dst.top		= rectDest.top ;
		dst.right	= rectDest.right ;
		dst.bottom	= rectDest.top + m_marginsImage.top ;
		if(src.Width() != 0 || src.Height() != 0)
			pImage->Draw(pDC,dst,src);
		// part four
		src.left	= m_rectImagePos.left;
		src.top		= m_rectImagePos.top + m_marginsImage.top ;
		src.right	= m_rectImagePos.left + m_marginsImage.left;
		src.bottom	= m_rectImagePos.bottom - m_marginsImage.bottom ;

		dst.left	= rectDest.left;
		dst.top		= rectDest.top + m_marginsImage.top ;
		dst.right	= rectDest.left + m_marginsImage.left;
		dst.bottom	= rectDest.bottom - m_marginsImage.bottom;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(m_stretchImage == keStretchAll ||
				m_stretchImage == keStretchLeft)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);

		}
		//part five
		src.left	= m_rectImagePos.left + m_marginsImage.left;
		src.top		= m_rectImagePos.top + m_marginsImage.top;
		src.right	= m_rectImagePos.right - m_marginsImage.right;
		src.bottom	= m_rectImagePos.bottom - m_marginsImage.bottom;

		dst.left	= rectDest.left + m_marginsImage.left;
		dst.top		= rectDest.top + m_marginsImage.top;
		dst.right	= rectDest.right - m_marginsImage.right;
		dst.bottom	= rectDest.bottom - m_marginsImage.bottom;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(m_stretchImage == keStretchAll ||
				m_stretchImage == keStretchClient)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);

		}
		//part six
		src.left	= m_rectImagePos.right - m_marginsImage.right;
		src.top		= m_rectImagePos.top + m_marginsImage.top;
		src.right	= m_rectImagePos.right;
		src.bottom	= m_rectImagePos.bottom - m_marginsImage.bottom;

		dst.left	= rectDest.right - m_marginsImage.right;
		dst.top		= rectDest.top + m_marginsImage.top;
		dst.right	= rectDest.right;
		dst.bottom	= rectDest.bottom - m_marginsImage.bottom;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(m_stretchImage == keStretchAll ||
				m_stretchImage == keStretchRight)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);

		}

		//part seven
		src.left	= m_rectImagePos.left;
		src.top		= m_rectImagePos.bottom - m_marginsImage.bottom ;
		src.right	= m_rectImagePos.left + m_marginsImage.left;
		src.bottom	= m_rectImagePos.bottom ;

		dst.left	= rectDest.left;
		dst.top		= rectDest.bottom - m_marginsImage.bottom ;
		dst.right	= rectDest.left + m_marginsImage.left;
		dst.bottom	= rectDest.bottom ;
		if(src.Width() != 0 || src.Height() != 0)
			pImage->Draw(pDC,dst,src);
		//part eight
		src.left	= m_rectImagePos.left + m_marginsImage.left;
		src.top		= m_rectImagePos.bottom - m_marginsImage.bottom ;
		src.right	= m_rectImagePos.right - m_marginsImage.right;
		src.bottom	= m_marginsImage.bottom ;

		dst.left	= rectDest.left + m_marginsImage.left;
		dst.top		= rectDest.bottom - m_marginsImage.bottom ;
		dst.right	= rectDest.right - m_marginsImage.right;
		dst.bottom	= rectDest.bottom ;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(m_stretchImage == keStretchAll ||
				m_stretchImage == keStretchBottom)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);
/		}
		//part nine
		src.left	= m_rectImagePos.right - m_marginsImage.right;
		src.top		= m_rectImagePos.bottom - m_marginsImage.bottom ;
		src.right	= m_rectImagePos.right;
		src.bottom	= m_rectImagePos.bottom ;

		dst.left	= rectDest.right - m_marginsImage.right;
		dst.top		= rectDest.bottom - m_marginsImage.bottom ;
		dst.right	= rectDest.right;
		dst.bottom	= rectDest.bottom ;
		if(src.Width() != 0 || src.Height() != 0)
			pImage->Draw(pDC,dst,src);
	
	}
//}