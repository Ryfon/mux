﻿#include "StdAfx.h"
#include "..\include\scrollbarskin.h"


IMPLEMENT_SERIAL(CScrollBarSkin,CObjectSkin,1)
CScrollBarSkin::CScrollBarSkin(void)
{
	m_nSkinType = keScrollBarSkin;
}
CScrollBarSkin::CScrollBarSkin(CString strName)
:CObjectSkin(strName,keScrollBarSkin)
{

}
CScrollBarSkin::~CScrollBarSkin(void)
{
}

void CScrollBarSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_skinHScrollBar,sizeof(m_skinHScrollBar));
		ar.Write(&m_skinVScrollBar,sizeof(m_skinVScrollBar));
	}
	else
	{
		ar.Read(&m_skinHScrollBar,sizeof(m_skinHScrollBar));
		ar.Read(&m_skinVScrollBar,sizeof(m_skinVScrollBar));
	}
}
void CScrollBarSkin::DrawHArrowLeft(CDC *pDC,CRect rtDest,int nState)
{
	if(!DrawImageRect(pDC,rtDest,m_skinHScrollBar.imageArrow1[nState]))
	{
		if(!DrawImageRect(pDC,rtDest,m_skinHScrollBar.imageArrow1[0]))
			return;
	}
	
}
void CScrollBarSkin::DrawHArrowRight(CDC *pDC,CRect rtDest,int nState)
{
	if(!DrawImageRect(pDC,rtDest,m_skinHScrollBar.imageArrow2[nState]))
	{
		if(!DrawImageRect(pDC,rtDest,m_skinHScrollBar.imageArrow2[0]))
			return;
	}
}
void CScrollBarSkin::DrawHThumb(CDC *pDC,CRect rtDest,int nState)
{
	if(!DrawImageSection(pDC,rtDest,m_skinHScrollBar.imageThurmb[nState]))
	{
		if(!DrawImageSection(pDC,rtDest,m_skinHScrollBar.imageThurmb[0]))
			return;
	}
}
void CScrollBarSkin::DrawHBackground(CDC *pDC,CRect rtDest)
{
	if(!DrawImageSection(pDC,rtDest,m_skinHScrollBar.imageScrollBar))
		return;
}

void CScrollBarSkin::DrawVArrowTop(CDC *pDC,CRect rtDest,int nState)
{
	if(!DrawImageRect(pDC,rtDest,m_skinVScrollBar.imageArrow1[nState]))
	{
		if(!DrawImageRect(pDC,rtDest,m_skinVScrollBar.imageArrow1[0]))
			return;
	}
}
void CScrollBarSkin::DrawVArrowBottom(CDC *pDC,CRect rtDest,int nState)
{
	if(!DrawImageRect(pDC,rtDest,m_skinVScrollBar.imageArrow2[nState]))
	{
		if(!DrawImageRect(pDC,rtDest,m_skinVScrollBar.imageArrow2[0]))
			return;
	}
}
void CScrollBarSkin::DrawVThumb(CDC *pDC,CRect rtDest,int nState)
{
	if(!DrawImageSection(pDC,rtDest,m_skinVScrollBar.imageThurmb[nState]))
	{
		if(!DrawImageSection(pDC,rtDest,m_skinVScrollBar.imageThurmb[0]))
			return;
	}
}
void CScrollBarSkin::DrawVBackground(CDC *pDC,CRect rtDest)
{
	if(!DrawImageSection(pDC,rtDest,m_skinVScrollBar.imageScrollBar))
		return;
}
void CScrollBarSkin::DrawHScroll(CDC *pDC,CRect rtDest,int nState)
{
	int nHeight = rtDest.Height();

	CRect rtArrowLeft = CRect(rtDest.left,rtDest.top,rtDest.left + nHeight,rtDest.bottom);
	CRect rtArrowRight = CRect(rtDest.right - nHeight,rtDest.top,rtDest.right,rtDest.bottom);
	DrawHBackground(pDC,rtDest);
	DrawHArrowLeft(pDC,rtArrowLeft,nState);
	DrawHArrowRight(pDC,rtArrowRight,nState);

}
void CScrollBarSkin::DrawVScroll(CDC *pDC,CRect rtDest,int nState)
{
	int nWidth = rtDest.Width();

	CRect rtArrowTop = CRect(rtDest.left, rtDest.top,rtDest.right,rtDest.top + nWidth);
	CRect rtArrowBottom = CRect(rtDest.left,rtDest.bottom - nWidth,rtDest.right,rtDest.bottom);
	DrawVBackground(pDC,rtDest);
	DrawVArrowTop(pDC,rtArrowTop,nState);
	DrawVArrowBottom(pDC,rtArrowBottom,nState);
}
void CScrollBarSkin::DrawSizing(CDC *pDC,CRect rtDest)
{
	if(!DrawImageSection(pDC,rtDest,m_imageSizing))
	{
		CBrush brush(::GetSysColor(COLOR_BTNFACE));
		pDC->FillRect(rtDest,&brush);
	}
}