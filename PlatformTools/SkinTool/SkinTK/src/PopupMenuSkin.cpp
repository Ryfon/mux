﻿#include "StdAfx.h"
#include "..\include\popupmenuskin.h"

IMPLEMENT_SERIAL(CPopupMenuSkin,CObjectSkin,1)
CPopupMenuSkin::CPopupMenuSkin(void)
{
	m_nSkinType = kePopMenuSkin;
}
CPopupMenuSkin::CPopupMenuSkin(CString strName)
:CObjectSkin(strName,kePopMenuSkin)
{
}
CPopupMenuSkin::~CPopupMenuSkin(void)
{
}
void CPopupMenuSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_imageFrame,sizeof(m_imageFrame));
		ar.Write(&m_itemPopup,sizeof(m_itemPopup));
		ar.Write(&m_colorBackground,sizeof(m_colorBackground));
	}
	else
	{
		ar.Read(&m_imageFrame,sizeof(m_imageFrame));
		ar.Read(&m_itemPopup,sizeof(m_itemPopup));
		ar.Read(&m_colorBackground,sizeof(m_colorBackground));
	}
}