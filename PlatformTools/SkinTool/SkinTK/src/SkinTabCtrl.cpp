﻿#include "StdAfx.h"
#include "..\include\skintabctrl.h"
#include "..\include\TabSkin.h"

CSkinTabCtrl::CSkinTabCtrl(void)
{
	m_pTabSkin = GetSkin().GetTabSkin();
	m_nHotItem = -1;
	m_nPressedItem = -1;
}

CSkinTabCtrl::~CSkinTabCtrl(void)
{
	HookWindow( (HWND)NULL);
}
void CSkinTabCtrl::InstallSkin(HWND hWnd)
{
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );


	m_pTabSkin = GetSkin().GetTabSkin();
	if(m_pTabSkin == NULL)
		m_bEnableSkin = FALSE;

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);


	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0,  SWP_FRAMECHANGED|SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
void CSkinTabCtrl::LoadSkin()
{
	m_bEnableSkin = FALSE;
	m_pTabSkin = GetSkin().GetTabSkin();
	if(m_pTabSkin == NULL)
		m_bEnableSkin = FALSE;
	else
		m_bEnableSkin = TRUE;
	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
LRESULT CSkinTabCtrl::OnWndMsg(UINT msg,WPARAM wp,LPARAM lp)
{
	switch(msg)
	{
	case WM_MOUSELEAVE:
		OnMouseLeave();
		return 0;
	}
	return CSkinWnd::OnWndMsg(msg,wp,lp);
}
void CSkinTabCtrl::OnPaint()
{
	//	Default();
	CTabCtrl *pTab = (CTabCtrl*)CWnd::FromHandle(m_hWnd);
	CPaintDC dc(pTab);
	DrawTab(&dc);
	//	Default(WM_PAINT,(WPARAM)dc.m_hDC,0);
}
void CSkinTabCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	Default();
	if((nFlags & MK_LBUTTON) == 0)
	{
		TRACKMOUSEEVENT tme;
		m_nHotItem = HitTest(point);
		m_nPressedItem = -1;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = m_hWnd;
		TrackMouseEvent(&tme);
	}
	else if(nFlags & MK_LBUTTON)
	{
		TRACKMOUSEEVENT tme;
		m_nPressedItem = HitTest(point);
		m_nHotItem = -1;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = m_hWnd;
		TrackMouseEvent(&tme);
	}
	CTabCtrl *pTab = (CTabCtrl*)CWnd::FromHandle(m_hWnd);
	CDC *pDC =  pTab->GetDC();
	CRect rtClient;
	GetClientRect(rtClient);
	CMemDC dc(pDC,rtClient);
	DrawTab(&dc);
}
void CSkinTabCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	Default();
	m_nPressedItem = HitTest(point);
	m_nHotItem = -1;
	CTabCtrl *pTab = (CTabCtrl*)CWnd::FromHandle(m_hWnd);
	CDC *pDC =  pTab->GetDC();
	CRect rtClient;
	GetClientRect(rtClient);
	CMemDC dc(pDC,rtClient);
	DrawTab(&dc);
}
void CSkinTabCtrl::OnMouseLeave()
{
	m_nHotItem = -1;
	m_nPressedItem = -1;
	CTabCtrl *pTab = (CTabCtrl*)CWnd::FromHandle(m_hWnd);
	CDC *pDC =  pTab->GetDC();
	CRect rtClient;
	GetClientRect(rtClient);
	CMemDC dc(pDC,rtClient);
	DrawTab(&dc);
}
void CSkinTabCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
	Default();
	m_nPressedItem = -1;
	m_nHotItem = HitTest(point);
	CTabCtrl *pTab = (CTabCtrl*)CWnd::FromHandle(m_hWnd);
	CDC *pDC =  pTab->GetDC();
	CRect rtClient;
	GetClientRect(rtClient);
	CMemDC dc(pDC,rtClient);
	DrawTab(&dc);
}
UINT CSkinTabCtrl::HitTest(CPoint point)
{
	CTabCtrl *pTab = (CTabCtrl*)CWnd::FromHandle(m_hWnd);
	TCHITTESTINFO infoHitTest;
	infoHitTest.pt = point;
	return pTab->HitTest(&infoHitTest);
}
void CSkinTabCtrl::DrawTab(CDC *pDC)
{
	CRect rtClient;
	GetClientRect(rtClient);
	CTabCtrl *pTab = (CTabCtrl*)CWnd::FromHandle(m_hWnd);

	int nItemCount = pTab->GetItemCount();
	if(nItemCount <= 0)return;
	m_pTabSkin->DrawImageSection(pDC,rtClient,m_pTabSkin->m_imageBackground);
	/*CRgn rgn;
	rgn.CreateRectRgn(2,2,rt.right - 2,rt.bottom - 2);
	dc.SelectClipRgn(&rgn);*/

	CRect rcItem;
	DWORD dwStyle = GetWindowLong( m_hWnd, GWL_STYLE );
	int OldModule = pDC->SetBkMode(TRANSPARENT);
	CFont *pFont  = pTab->GetFont();
	CFont* pOldFont = pDC->SelectObject(pFont);

	for(int i = 0 ; i < nItemCount ; i++)
	{
		pTab->GetItemRect(i,rcItem);
		if(m_nPressedItem == i)
			DrawItemEntry(pDC,i,rcItem,PRESSED);
		else if(m_nHotItem == i)
			DrawItemEntry(pDC,i,rcItem,HOVER);
		else
			DrawItemEntry(pDC,i,rcItem,NORMAL);

		char ItemText[1025] = "";
		TCITEM tim;
		tim.mask = TCIF_IMAGE | TCIF_STATE | TCIF_TEXT;
		tim.pszText = ItemText;
		tim.cchTextMax = 1024;
		if(!pTab->GetItem(i,&tim))continue;
		CRect rtText = rcItem;
		rtText.left = rtText.left +3;
		pDC->DrawText(ItemText,(int)strlen(ItemText),&rtText,DT_LEFT | DT_SINGLELINE |DT_VCENTER| DT_END_ELLIPSIS);
	}

}
void CSkinTabCtrl::DrawItemEntry(CDC* pDC, int nIndex, CRect rcItem, int nState)
{
	if(nState == PRESSED)
	{
		if(!m_pTabSkin->DrawImageSection(pDC,rcItem,m_pTabSkin->m_imageTabPressed))
			m_pTabSkin->DrawImageSection(pDC,rcItem,m_pTabSkin->m_imageTabNormal);
	}
	else if(nState == HOVER)
	{
		if(!m_pTabSkin->DrawImageSection(pDC,rcItem,m_pTabSkin->m_imageTabHover))
			m_pTabSkin->DrawImageSection(pDC,rcItem,m_pTabSkin->m_imageTabNormal);
	}
	else
	{
		m_pTabSkin->DrawImageSection(pDC,rcItem,m_pTabSkin->m_imageTabNormal);
	}
}