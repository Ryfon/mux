﻿#include "StdAfx.h"
#include "..\include\editskin.h"

IMPLEMENT_SERIAL(CEditSkin,CObjectSkin,1)
CEditSkin::CEditSkin(void)
{
	m_nSkinType = keEditSkin;
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontEdit = nif.lfCaptionFont;
	m_bAutoFont = TRUE;
	m_colorBack = GetSysColor(COLOR_BTNFACE);
}
CEditSkin::CEditSkin(CString strName)
:CObjectSkin(strName,keEditSkin)
{
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontEdit = nif.lfCaptionFont;
	m_bAutoFont = TRUE;
	m_colorBack = GetSysColor(COLOR_BTNFACE);
}
CEditSkin::~CEditSkin(void)
{
}
void CEditSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_fontEdit,sizeof(m_fontEdit));
		ar.Write(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Write(&m_imageBorder,sizeof(m_imageBorder));
		ar.Write(&m_textEffect,sizeof(m_textEffect));
		ar.Write(&m_colorBack,sizeof(m_colorBack));
	}
	else
	{
		ar.Read(&m_fontEdit,sizeof(m_fontEdit));
		ar.Read(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Read(&m_imageBorder,sizeof(m_imageBorder));
		ar.Read(&m_textEffect,sizeof(m_textEffect));
		ar.Read(&m_colorBack,sizeof(m_colorBack));
	}
}
