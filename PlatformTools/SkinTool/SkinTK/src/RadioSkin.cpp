﻿#include "StdAfx.h"
#include "..\include\radioskin.h"


IMPLEMENT_SERIAL(CRadioSkin,CObjectSkin,1)
CRadioSkin::CRadioSkin(void)
{
	m_nSkinType = keRadioSkin;
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontRadio = nif.lfCaptionFont;

	m_colorBackground = ::GetSysColor(COLOR_BTNFACE);
}
CRadioSkin::CRadioSkin(CString strName)
:CObjectSkin(strName,keRadioSkin)
{
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontRadio = nif.lfCaptionFont;
	m_colorBackground = ::GetSysColor(COLOR_BTNFACE);
}
CRadioSkin::~CRadioSkin(void)
{
}
void CRadioSkin::DrawBackground(CDC *pDC,CRect rtDest)
{
	if(!DrawImageSection(pDC,rtDest,m_imageBackground))
	{
		CBrush brush(m_colorBackground);
		pDC->FillRect(rtDest,&brush);
	}
}
void CRadioSkin::DrawBox(CDC *pDC,CRect rtDest,int nState,int nCheck)
{
	if(nCheck == 0)
	{
		if(!DrawImageRect(pDC,rtDest,m_stateRadio[nState].imageChecked))
			DrawImageRect(pDC,rtDest,m_stateRadio[keRadioNormal].imageChecked);
	}
	else if(nCheck == 1)
	{
		if(!DrawImageRect(pDC,rtDest,m_stateRadio[nState].imageUnchecked))
			DrawImageRect(pDC,rtDest,m_stateRadio[keRadioNormal].imageUnchecked);
	}
	else
	{
		DrawImageRect(pDC,rtDest,m_stateRadio[keRadioNormal].imageChecked);
	}
}
void CRadioSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_fontRadio,sizeof(m_fontRadio));
		ar.Write(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Write(&m_bTransparent,sizeof(m_bTransparent));
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar.Write(&m_colorBackground,sizeof(m_colorBackground));
		ar.Write(&m_nBackMode,sizeof(m_nBackMode));
		ar.Write(&m_stateRadio,sizeof(m_stateRadio));
	}
	else
	{
		ar.Read(&m_fontRadio,sizeof(m_fontRadio));
		ar.Read(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Read(&m_bTransparent,sizeof(m_bTransparent));
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar.Read(&m_colorBackground,sizeof(m_colorBackground));
		ar.Read(&m_nBackMode,sizeof(m_nBackMode));
		ar.Read(&m_stateRadio,sizeof(m_stateRadio));
	}
}