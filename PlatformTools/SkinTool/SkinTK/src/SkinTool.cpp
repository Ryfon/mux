﻿#include "StdAfx.h"
#include "../include/SkinTool.h"

#include "../include/InitSKinTK.h"

CInitSKinTK m_skinInit;

BOOL  __stdcall SkinInit()
{
	return TRUE;
}
BOOL  __stdcall SkinExit()
{
	m_skinInit.UnInstallSkin();
	return TRUE;
}

BOOL  __stdcall SkinLoadSkin(LPCTSTR lpszFileName)
{
	m_skinInit.InstallSkin(lpszFileName);
	return TRUE;
}
void  __stdcall SkinModifyHue(int nPercent)
{
	m_skinInit.ModifyHue(nPercent);
}
void  __stdcall SetWindowSkin( HWND hWnd)
{
	m_skinInit.HookWindow(hWnd);
}
void  __stdcall RemoveWindowSkin( HWND hWnd )
{
	m_skinInit.RemoveWnd(hWnd);
}