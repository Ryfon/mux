﻿#include "StdAfx.h"
#include "..\include\skindrawhelper.h"
#include "..\include\Skin.h"


CSkinDrawHelper skinDrawHelper;
CSkinDrawHelper* GetSkinDrawHelper()
{
	return &skinDrawHelper;
}
CSkinDrawHelper::CSkinDrawHelper(void)
{
}

CSkinDrawHelper::~CSkinDrawHelper(void)
{
}

bool CSkinDrawHelper::DrawImageRect(CDC *pDC,CRect rtDest,SkinImageRect & imageRect)
{
	CSkinBitmap *pImage = GetImages()->FindImage(imageRect.strImageName);
	if(!pImage)
		return false;

	pImage->Draw(pDC,&rtDest,imageRect.rtImagePos);
	return true;
}

//*********************
//* 1	*	  2		* 3	*
//*********************
//* 4	*	  5		* 6	* 
//*	*			*	*
//*********************
//* 7	*	  8		* 9	*
//*********************
bool CSkinDrawHelper::DrawImageSection(CDC *pDC,CRect rtDest,SkinImageSection &imageSection)
{
	CSkinBitmap *pImage = GetImages()->FindImage(imageSection.strImageName);
	if(!pImage)
		return false;

	SkinRect src,dst;

	if(pImage)
	{
		// draw part one
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 || src.Height() != 0)
			pImage->Draw(pDC,dst,src);
		//draw part two
		src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.left + imageSection.marginImage.left;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.right - imageSection.marginImage.right;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(imageSection.stretchImage == keStretchAll ||
				imageSection.stretchImage == keStretchTop)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);

		}
		//draw part three
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.top ;
		src.right	= imageSection.rtImagePos.right ;
		src.bottom	= imageSection.rtImagePos.top + imageSection.marginImage.top ;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.top ;
		dst.right	= rtDest.right ;
		dst.bottom	= rtDest.top + imageSection.marginImage.top ;
		if(src.Width() != 0 || src.Height() != 0)
			pImage->Draw(pDC,dst,src);
		//draw part four
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.top + imageSection.marginImage.top ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(imageSection.stretchImage == keStretchAll ||
				imageSection.stretchImage == keStretchLeft)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);

		}
		//draw part five
		src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top;
		src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom;

		dst.left	= rtDest.left + imageSection.marginImage.left;
		dst.top		= rtDest.top + imageSection.marginImage.top;
		dst.right	= rtDest.right - imageSection.marginImage.right;
		dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(imageSection.stretchImage == keStretchAll ||
				imageSection.stretchImage == keStretchClient)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);

		}
		//draw part six
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.top + imageSection.marginImage.top;
		src.right	= imageSection.rtImagePos.right;
		src.bottom	= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.top + imageSection.marginImage.top;
		dst.right	= rtDest.right;
		dst.bottom	= rtDest.bottom - imageSection.marginImage.bottom;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(imageSection.stretchImage == keStretchAll ||
				imageSection.stretchImage == keStretchRight)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);

		}

		//draw part seven
		src.left	= imageSection.rtImagePos.left;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.left;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.left + imageSection.marginImage.left;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 || src.Height() != 0)
			pImage->Draw(pDC,dst,src);
		//draw part eight
		src.left	= imageSection.rtImagePos.left + imageSection.marginImage.left;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.left + imageSection.marginImage.left;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.right - imageSection.marginImage.right;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 || src.Height() != 0)
		{
			if(imageSection.stretchImage == keStretchAll ||
				imageSection.stretchImage == keStretchBottom)
				pImage->Draw(pDC,dst,src);
			else
				pImage->TitleDraw(pDC,dst,src);
		}
		//draw part nine
		src.left	= imageSection.rtImagePos.right - imageSection.marginImage.right;
		src.top		= imageSection.rtImagePos.bottom - imageSection.marginImage.bottom ;
		src.right	= imageSection.rtImagePos.right;
		src.bottom	= imageSection.rtImagePos.bottom ;

		dst.left	= rtDest.right - imageSection.marginImage.right;
		dst.top		= rtDest.bottom - imageSection.marginImage.bottom ;
		dst.right	= rtDest.right;
		dst.bottom	= rtDest.bottom ;
		if(src.Width() != 0 || src.Height() != 0)
			pImage->Draw(pDC,dst,src);

	}

	return true;
}
//bool CSkinDrawHelper::DrawFrameSkin(CDC *pDC,CRect rtDest,WindowSkin &windowSkin,bool bActive)
//{
//	CRect			rtMargins = windowSkin.rtMargins;
//	CRect			rtParts[4];
//	rtParts[0].left		= rtDest.left;
//	rtParts[0].top		= rtDest.top;
//	rtParts[0].right	= rtDest.right;
//	rtParts[0].bottom	= rtDest.top + rtMargins.top;
//
//	rtParts[1].left		= rtDest.left;
//	rtParts[1].top		= rtDest.top + rtMargins.top;
//	rtParts[1].right	= rtDest.left + rtMargins.left;
//	rtParts[1].bottom	= rtDest.bottom - rtMargins.bottom;
//
//	rtParts[2].left		= rtDest.right - rtMargins.right;
//	rtParts[2].top		= rtDest.top + rtMargins.top;
//	rtParts[2].right	= rtDest.right;
//	rtParts[2].bottom	= rtDest.bottom - rtMargins.bottom;
//
//	rtParts[3].left		= rtDest.left;
//	rtParts[3].top		= rtDest.bottom - rtMargins.bottom;
//	rtParts[3].right	= rtDest.right;
//	rtParts[3].bottom	= rtDest.bottom;
//
//
//	if(bActive)
//	{
//		DrawImageSection(pDC,rtParts[0],windowSkin.frameActive.imageTop);
//		DrawImageSection(pDC,rtParts[1],windowSkin.frameActive.imageLeft);
//		DrawImageSection(pDC,rtParts[2],windowSkin.frameActive.imageRight);
//		DrawImageSection(pDC,rtParts[3],windowSkin.frameActive.imageBottom);
//	}
//	else
//	{
//		DrawImageSection(pDC,rtParts[0],windowSkin.frameInactive.imageTop);
//		DrawImageSection(pDC,rtParts[1],windowSkin.frameInactive.imageLeft);
//		DrawImageSection(pDC,rtParts[2],windowSkin.frameInactive.imageRight);
//		DrawImageSection(pDC,rtParts[3],windowSkin.frameInactive.imageBottom);
//	}
//
//	return true;
//}
//bool CSkinDrawHelper::DrawButton(CDC *pDC,CRect rtDest,ButtonSkin &buttonSkin,ButtonState buttonState)
//{
//	CString strImage = buttonSkin.imageState[buttonState].strImageName;
//	if(strImage.IsEmpty())
//		return false;
//	DrawImageSection(pDC,rtDest,buttonSkin.imageState[buttonState]);
//	return true;
//}
//bool CSkinDrawHelper::DrawButton(CDC *pDC,CRect rtDest,WinButtonSkin &buttonSkin,WinButtonState buttonState)
//{
//	CString strImage = buttonSkin.iamgeButton[buttonState].strImageName;
//	if(strImage.IsEmpty())
//		return false;
//	DrawImageRect(pDC,rtDest,buttonSkin.iamgeButton[buttonState]);
//	return true;
//}