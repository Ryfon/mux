﻿#include "StdAfx.h"
#include "..\include\staticskin.h"


IMPLEMENT_SERIAL(CStaticSkin,CObjectSkin,1)
CStaticSkin::CStaticSkin(void)
{
	m_nSkinType = keStaticSkin;
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontStatic = nif.lfCaptionFont;
	m_bTransparent = TRUE;
	m_colorBack = GetSysColor(COLOR_BTNFACE);
}
CStaticSkin::CStaticSkin(CString strName)
:CObjectSkin(strName,keStaticSkin)
{
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontStatic = nif.lfCaptionFont;
	m_bTransparent = TRUE; 

	m_colorBack = GetSysColor(COLOR_BTNFACE);
}

CStaticSkin::~CStaticSkin(void)
{
}
void CStaticSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_fontStatic,sizeof(m_fontStatic));
		ar.Write(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Write(&m_bTransparent,sizeof(m_bTransparent));
		ar.Write(&m_nBackMode,sizeof(m_nBackMode));
		ar.Write(&m_imageBack,sizeof(m_imageBack));
		ar.Write(&m_colorBack,sizeof(m_colorBack));
		ar.Write(&m_textEffect,sizeof(m_textEffect));
	}
	else
	{
		ar.Read(&m_fontStatic,sizeof(m_fontStatic));
		ar.Read(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Read(&m_bTransparent,sizeof(m_bTransparent));
		ar.Read(&m_nBackMode,sizeof(m_nBackMode));
		ar.Read(&m_imageBack,sizeof(m_imageBack));
		ar.Read(&m_colorBack,sizeof(m_colorBack));
		ar.Read(&m_textEffect,sizeof(m_textEffect));
	}
}