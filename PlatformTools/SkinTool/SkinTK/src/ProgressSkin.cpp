﻿#include "StdAfx.h"
#include "..\include\progressskin.h"

IMPLEMENT_SERIAL(CProgressSkin,CObjectSkin,1)
CProgressSkin::CProgressSkin(void)
{
	m_nSkinType = keProgressSkin;
	m_colorBackground = GetSysColor(COLOR_ACTIVECAPTION);
	m_colorProgress = RGB(255,0,0);
}

CProgressSkin::~CProgressSkin(void)
{
}
CProgressSkin::CProgressSkin(CString strName)
:CObjectSkin(strName,keProgressSkin)
{
	m_colorBackground = GetSysColor(COLOR_ACTIVECAPTION);
	m_colorProgress = RGB(255,0,0);
}
void CProgressSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar.Write(&m_colorBackground,sizeof(m_colorBackground));
		ar.Write(&m_imageProgress,sizeof(m_imageProgress));
		ar.Write(&m_colorProgress,sizeof(m_colorProgress));
	}
	else
	{
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar.Read(&m_colorBackground,sizeof(m_colorBackground));
		ar.Read(&m_imageProgress,sizeof(m_imageProgress));
		ar.Read(&m_colorProgress,sizeof(m_colorProgress));
	}
}