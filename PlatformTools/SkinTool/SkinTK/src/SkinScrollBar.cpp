﻿#include "StdAfx.h"
#include "..\include\skinscrollbar.h"

CSkinScrollBar::CSkinScrollBar(void)
{
	m_pSkinScrollBar = GetSkin().GetScrollBarSkin();
	m_bHScroll = FALSE;
	m_bVScroll = FALSE;
}

CSkinScrollBar::~CSkinScrollBar(void)
{
	HookWindow((HWND)NULL);
}
/******************************************
* 函数名 : InstallSkin
* 功能	 : 
*******************************************/
void CSkinScrollBar::InstallSkin(HWND hWnd)
{
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );

	if(!m_pSkinScrollBar)
		m_pSkinScrollBar = GetSkin().GetScrollBarSkin();

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);

	
	if(style&SBS_VERT == SBS_VERT)
		m_bVScroll = TRUE;

	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 

}
/******************************************
* 函数名 : LoadSkin
* 功能	 : 
*******************************************/
void CSkinScrollBar::LoadSkin()
{
	m_bEnableSkin = FALSE;
	m_pSkinScrollBar = GetSkin().GetScrollBarSkin();
	m_bEnableSkin = TRUE;
}
LRESULT CSkinScrollBar::OnWndMsg(UINT msg,WPARAM wp,LPARAM lp)
{
	switch(msg)
	{
	case WM_MOUSELEAVE:
		OnMouseLeave();
	}
	return CSkinWnd::OnWndMsg(msg,wp,lp);
}
/******************************************
* 函数名 : OnMouseMove
* 功能	 : 鼠标移动时调用 
*******************************************/
void CSkinScrollBar::OnMouseMove(UINT nFlags, CPoint point)
{
	Default();
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);

	CRect rtClient;
	pWnd->GetClientRect(rtClient);

	

	CDC *pDC = pWnd->GetDC();
	if(m_bVScroll)
	{
		SCROLL_HITTEST test = ScrollHitTest(SB_VERT,point,FALSE);

		if(test == SCROLL_TOP_ARROW)
		{
			m_nVArrowTopState = keScrollHover;
			m_nVArrowBottomState = keScrollNormal;
			m_nVThumbState = keScrollNormal;
			DrawVScrollBar(pDC);
		}
		else if(test == SCROLL_BOTTOM_ARROW)
		{
			m_nVArrowTopState = keScrollNormal;
			m_nVArrowBottomState = keScrollHover;
			m_nVThumbState = keScrollNormal;
			DrawVScrollBar(pDC);
		}
		else if(test == SCROLL_THUMB)
		{
			m_nVArrowTopState = keScrollNormal;
			m_nVArrowBottomState = keScrollNormal;
			m_nVThumbState = keScrollHover;
			DrawVScrollBar(pDC);
		}
		else
		{
			m_nVArrowTopState = keScrollNormal;
			m_nVArrowBottomState = keScrollNormal;
			m_nVThumbState = keScrollNormal;
			DrawVScrollBar(pDC);
		}
	}
	else
	{
		SCROLL_HITTEST test = ScrollHitTest(SB_CTL,point,FALSE);

		if(test == SCROLL_TOP_ARROW)
		{
			m_nHArrowLeftState = keScrollHover;
			m_nHArrowRightState = keScrollNormal;
			m_nHThumbState = keScrollNormal;
			DrawHScrollBar(pDC);
		}
		else if(test == SCROLL_BOTTOM_ARROW)
		{
			m_nHArrowLeftState = keScrollNormal;
			m_nHArrowRightState = keScrollHover;
			m_nHThumbState = keScrollNormal;
			DrawHScrollBar(pDC);
		}
		else if(test == SCROLL_THUMB)
		{
			m_nHArrowLeftState = keScrollNormal;
			m_nHArrowRightState = keScrollNormal;
			m_nHThumbState = keScrollHover;
			DrawHScrollBar(pDC);
		}
		else
		{
			m_nHArrowLeftState = keScrollNormal;
			m_nHArrowRightState = keScrollNormal;
			m_nHThumbState = keScrollNormal;
			DrawHScrollBar(pDC);
		}
	}
	if(rtClient.PtInRect(point) && ((nFlags & MK_LBUTTON) == 0) )
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = m_hWnd;
		TrackMouseEvent(&tme);
	}
}
void CSkinScrollBar::OnMouseLeave()
{
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC *pDC = pWnd->GetDC();
	if(m_bVScroll)
	{
		m_nVArrowTopState = keScrollNormal;
		m_nVArrowBottomState = keScrollNormal;
		m_nVThumbState = keScrollNormal;
		DrawVScrollBar(pDC);
	}
	else
	{
		m_nHArrowLeftState = keScrollNormal;
		m_nHArrowRightState = keScrollNormal;
		m_nHThumbState = keScrollNormal;
		DrawHScrollBar(pDC);
	}
	
}
/******************************************
* 函数名 : OnLButtonDown
* 功能	 : 鼠标左键按下时调用 
*******************************************/
void CSkinScrollBar::OnLButtonDown(UINT nFlags, CPoint point)
{
	
	TrackScrollBar(SB_CTL,point);

//	Default();
}
/******************************************
* 函数名 : OnLButtonUp
* 功能	 : 鼠标左键放开时调用 
*******************************************/
void CSkinScrollBar::OnLButtonUp(UINT nFlags, CPoint point)
{
	Default();
}
/******************************************
* 函数名 : OnLButtonDblClk
* 功能	 : 鼠标左键双击时调用 
*******************************************/
void CSkinScrollBar::OnLButtonDblClk(UINT nFlags, CPoint point)
{	
	OnLButtonDown(nFlags,point);
	//Default();

}
/******************************************
* 函数名 : OnPaint
* 功能	 : 当客户需要重画时调用
*******************************************/
void CSkinScrollBar::OnPaint()
{
//	Default();
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CPaintDC dc(pWnd);
//	CDC *pDC = pWnd->GetDC();
	if(!m_bVScroll)
		DrawHScrollBar(&dc);
	else
		DrawVScrollBar(&dc);
}
void CSkinScrollBar::DrawHScrollBar(CDC *pDC)
{
	CRect rtWindow;
	GetWindowRect(&rtWindow);
	CRect rtPos;
	int nArrowSize;
	int nThumbSize;
	int nThumbPos;

	GetScrollRect(SB_CTL,&rtPos,&nArrowSize,&nThumbSize,&nThumbPos);
	CMemDC memDC(pDC,rtPos);

	m_pSkinScrollBar->DrawHBackground(&memDC,rtPos);
	m_pSkinScrollBar->DrawHArrowLeft(&memDC,CRect(rtPos.left,rtPos.top,rtPos.left + nArrowSize,rtPos.bottom),m_nHArrowLeftState);
	m_pSkinScrollBar->DrawHArrowRight(&memDC,CRect(rtPos.right - nArrowSize,rtPos.top,rtPos.right,rtPos.bottom),m_nHArrowRightState);
	if(nThumbPos && nThumbSize)
		m_pSkinScrollBar->DrawHThumb(&memDC,CRect(rtPos.left + nThumbPos,rtPos.top,rtPos.left +nThumbPos + nThumbSize,rtPos.bottom),m_nHThumbState);
}
void CSkinScrollBar::DrawVScrollBar(CDC *pDC)
{
	CRect rtWindow;
	GetWindowRect(&rtWindow);
	CRect rtPos;
	int nArrowSize;
	int nThumbSize;
	int nThumbPos;

	GetScrollRect(SB_CTL,&rtPos,&nArrowSize,&nThumbSize,&nThumbPos);
	CMemDC memDC(pDC,rtPos);
	m_pSkinScrollBar->DrawVBackground(&memDC,rtPos);
	m_pSkinScrollBar->DrawVArrowTop(&memDC,CRect(rtPos.left,rtPos.top,rtPos.right ,rtPos.top + nArrowSize),m_nVArrowTopState);
	m_pSkinScrollBar->DrawVArrowBottom(&memDC,CRect(rtPos.left,rtPos.bottom - nArrowSize,rtPos.right,rtPos.bottom),m_nVArrowBottomState);
	if(nThumbPos && nThumbSize)
		m_pSkinScrollBar->DrawVThumb(&memDC,CRect(rtPos.left,rtPos.top +nThumbPos,rtPos.right ,rtPos.top + +nThumbPos + nThumbSize),m_nVThumbState);
}
