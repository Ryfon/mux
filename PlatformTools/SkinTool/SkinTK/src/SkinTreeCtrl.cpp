﻿#include "StdAfx.h"
#include "..\include\skintreectrl.h"
#include "..\include\ListBoxSkin.h"

CSkinTreeCtrl::CSkinTreeCtrl(void)
{
	m_pListBoxSkin = GetSkin().GetListBoxSkin();
}

CSkinTreeCtrl::~CSkinTreeCtrl(void)
{
	HookWindow( (HWND)NULL);
	UninitializeSB(m_hWnd);
}
void CSkinTreeCtrl::LoadSkin()
{
	m_bEnableSkin = FALSE;
	m_pSkinScrollBar = GetSkin().GetScrollBarSkin();
	m_pListBoxSkin = GetSkin().GetListBoxSkin();

	m_bEnableSkin = TRUE;
	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
void CSkinTreeCtrl::InstallSkin(HWND hWnd)
{
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );

	if(m_pSkinScrollBar == NULL)
		m_pSkinScrollBar = GetSkin().GetScrollBarSkin();
	if (!m_pListBoxSkin)
	{
		m_pListBoxSkin = GetSkin().GetListBoxSkin();
	}

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);
	m_bHScroll		= style & WS_HSCROLL;
	m_bVScroll		= style & WS_VSCROLL;
	m_bLeftScroll	= exstyle & WS_EX_LEFTSCROLLBAR;
	m_bBorder = TRUE;

	InitializeSB(m_hWnd);
	m_pScrollWnd = GetScrollWndFromHwnd(m_hWnd);

	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0,  SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 

}
/******************************************
* 函数名 : OnDestroy
* 功能	 :  
*******************************************/
void CSkinTreeCtrl::OnDestroy()
{
	Default();
	
}
/******************************************
* 函数名 : OnNcHitTest
* 功能	 : 当鼠标移动时调用此函数 
*******************************************/
UINT CSkinTreeCtrl::OnNcHitTest( CPoint point )
{
	CRect rtWindow;
	CRect rtClient;
	SCROLLBAR *sb;

	int nHHeight = GetSystemMetrics(SM_CYHSCROLL);
	int nVWidth = GetSystemMetrics(SM_CXVSCROLL);
	GetWindowRect(&rtWindow);
	GetClientRect(&rtClient);
	ClientToScreen(&rtClient);

	if(rtClient.PtInRect(point))
		return HTCLIENT;

	sb = GetScrollBarFromHwnd(m_hWnd, SB_VERT);
	if(sb->fScrollVisible)
	{
		if(m_bLeftScroll)
			m_rtVScroll = CRect(rtClient.left - nVWidth,rtClient.top,rtClient.left,rtClient.bottom);
		else
			m_rtVScroll = CRect(rtClient.right,rtClient.top,rtClient.right + nVWidth,rtClient.bottom);

		if(m_rtVScroll.PtInRect(point))
			return HTVSCROLL;
	}
	sb = GetScrollBarFromHwnd(m_hWnd, SB_HORZ);
	if(sb->fScrollVisible)
	{
		m_rtHScroll = CRect(rtClient.left,rtClient.bottom,rtClient.right,rtClient.bottom + nHHeight);

		if(m_rtHScroll.PtInRect(point))
			return HTHSCROLL;
	}
	return HTNOWHERE;
}

/******************************************
* 函数名 : OnNcPaint
* 功能	 : 当非客户需要重画时调用 
*******************************************/
void CSkinTreeCtrl::OnNcPaint(HRGN rgn1)
{
	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);
	m_bHScroll		= style & WS_HSCROLL;
	m_bVScroll		= style & WS_VSCROLL;
	m_bLeftScroll	= exstyle & WS_EX_LEFTSCROLLBAR;

	SCROLLBAR *sb;

	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC * pDC = pWnd->GetWindowDC();
	CRect rtWindow;
	pWnd->GetWindowRect( rtWindow );

	CRect rtClient;
	GetClientRect(&rtClient);
	ClientToScreen(&rtClient);
	rtClient.OffsetRect(-rtWindow.left,-rtWindow.top);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);

	sb = GetScrollBarFromHwnd(m_hWnd, SB_HORZ);
	if(sb->fScrollVisible)
		DrawHScrollBar(pDC);

	sb = GetScrollBarFromHwnd(m_hWnd, SB_VERT);
	if(sb->fScrollVisible)
		DrawVScrollBar(pDC);

	if(m_bBorder)
		m_pListBoxSkin->DrawImageBorder(pDC,rtWindow,m_pListBoxSkin->m_imageBorder);

	if(m_bHScroll && m_bVScroll)
	{
		CRect rtSizing;
		if(m_bLeftScroll)
		{
			rtSizing = CRect(rtClient.right - GetSystemMetrics(SM_CXVSCROLL), rtClient.bottom,rtClient.right,rtClient.bottom + GetSystemMetrics(SM_CYHSCROLL));
		}
		else
		{
			rtSizing = CRect(rtClient.right, rtClient.bottom,rtClient.right + GetSystemMetrics(SM_CXVSCROLL),
				rtClient.bottom + GetSystemMetrics(SM_CYHSCROLL));
		}
		m_pSkinScrollBar->DrawSizing(pDC,rtSizing);
	}
}
/******************************************
* 函数名 : OnNcLButtonDown
* 功能	 : 非客户区按下鼠标左键 
*******************************************/
void CSkinTreeCtrl::OnNcLButtonDown( UINT nHitTest, CPoint point )
{
	switch(nHitTest)
	{
	case HTHSCROLL:
		TrackScrollBar(SB_HORZ,point);
		break;
	case HTVSCROLL:
		TrackScrollBar(SB_VERT,point);
		break;
	default:
		Default();
		break;
	}
}
/******************************************
* 函数名 : OnNcLButtonDblClk
* 功能	 : 非客户区双击鼠标左键 
*******************************************/
void CSkinTreeCtrl::OnNcLButtonDblClk( UINT nHitTest,  CPoint point )
{
	if(nHitTest == HTHSCROLL)
	{
		TrackScrollBar(SB_HORZ,point);
	}
	else if(nHitTest == HTVSCROLL)
	{
		TrackScrollBar(SB_VERT,point);
		//	SendMessage( m_hWnd, WM_SYSCOMMAND, (WPARAM)SC_VSCROLL, MAKELPARAM(point.x,point.y) );
	}
	else
		Default();
}
/******************************************
* 函数名 : OnNcCalcSize
* 功能	 : 根据窗口大小计算客户区大小 
*******************************************/
void CSkinTreeCtrl::OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	SCROLLBAR *sb;
	
	m_bHScroll		= style & WS_HSCROLL;
	m_bVScroll		= style & WS_VSCROLL;

	LPRECT lpRect = &(lpncsp->rgrc[0]);
	if(FALSE)
	{
		lpncsp->rgrc[0].left += 1;
		lpncsp->rgrc[0].top += 1;
		lpncsp->rgrc[0].right -= 1;
		lpncsp->rgrc[0].bottom -= 1;

		lpncsp->rgrc[1] = lpncsp->rgrc[0];
	}

	sb = GetScrollBarFromHwnd(m_hWnd, SB_HORZ);

	if(sb->fScrollFlags & CSBS_VISIBLE)
	{
		if( lpRect->bottom - lpRect->top > GetSystemMetrics(SM_CYHSCROLL))
			lpRect->bottom -= GetSystemMetrics(SM_CYHSCROLL);
		sb->fScrollVisible = TRUE;
	}
	else
	{
		sb->fScrollVisible = FALSE;
	}


	sb = GetScrollBarFromHwnd(m_hWnd, SB_VERT);

	if(sb->fScrollFlags & CSBS_VISIBLE)
	{
		if( lpRect->right - lpRect->left >= GetSystemMetrics(SM_CXVSCROLL)){
			if(m_bLeftScroll)
				lpRect->left  += GetSystemMetrics(SM_CXVSCROLL);
			else
				lpRect->right -= GetSystemMetrics(SM_CXVSCROLL);
		}
		sb->fScrollVisible = TRUE;
	}
	else
	{
		sb->fScrollVisible = FALSE;
	}
}

void CSkinTreeCtrl::OnMouseMove(UINT nFlags,CPoint point)
{
	Default();

	if(m_nHArrowLeftState != keScrollNormal ||m_nHArrowRightState != keScrollNormal|| m_nHThumbState != keScrollNormal 
		|| m_nVArrowTopState != keScrollNormal || m_nVArrowBottomState != keScrollNormal|| m_nVThumbState != keScrollNormal)
	{
		CWindowDC dc(CWnd::FromHandle(m_hWnd));

		m_nVArrowTopState = keScrollNormal;
		m_nVArrowBottomState = keScrollNormal;
		m_nVThumbState = keScrollNormal;
		if(m_bVScroll)		
			DrawVScrollBar(&dc);

		m_nHArrowLeftState = keScrollNormal;
		m_nHArrowRightState = keScrollNormal;
		m_nHThumbState = keScrollNormal;
		if(m_bHScroll)
			DrawHScrollBar(&dc);
	}
}
/******************************************
* 函数名 : OnNcMouseMove
* 功能	 : 非客户区鼠标移动 
*******************************************/
void CSkinTreeCtrl::OnNcMouseMove( UINT nHitTest, CPoint point )
{
	CWindowDC dc(CWnd::FromHandle(m_hWnd));
	switch(nHitTest)
	{
	case HTHSCROLL:
		{
			int xoffset = 0, yoffset = 0;
			CRect rtClient,rtWindow;
			GetClientRect(&rtClient);
			ClientToScreen(&rtClient);
			GetWindowRect(&rtWindow);

			xoffset = rtClient.left - rtWindow.left;
			yoffset = rtClient.top - rtWindow.top;
			::ScreenToClient(m_hWnd, &point );
			point.x += xoffset;
			point.y += yoffset;
			SCROLL_HITTEST test = ScrollHitTest(SB_HORZ,point,FALSE);

			if(test == SCROLL_TOP_ARROW)
			{
				m_nHArrowLeftState = keScrollHover;
				m_nHArrowRightState = keScrollNormal;
				m_nHThumbState = keScrollNormal;
				DrawHScrollBar(&dc);
			}
			else if(test == SCROLL_BOTTOM_ARROW)
			{
				m_nHArrowLeftState = keScrollNormal;
				m_nHArrowRightState = keScrollHover;
				m_nHThumbState = keScrollNormal;
				DrawHScrollBar(&dc);
			}
			else if(test == SCROLL_THUMB)
			{
				m_nHArrowLeftState = keScrollNormal;
				m_nHArrowRightState = keScrollNormal;
				m_nHThumbState = keScrollHover;
				DrawHScrollBar(&dc);
			}
			else
			{
				m_nHArrowLeftState = keScrollNormal;
				m_nHArrowRightState = keScrollNormal;
				m_nHThumbState = keScrollNormal;
				DrawHScrollBar(&dc);
			}

			m_nVArrowTopState = keScrollNormal;
			m_nVArrowBottomState = keScrollNormal;
			m_nVThumbState = keScrollNormal;
			if(m_bVScroll)		
				DrawVScrollBar(&dc);
		}
		break;
	case HTVSCROLL:
		{
			int xoffset = 0, yoffset = 0;
			CRect rtClient,rtWindow;
			GetClientRect(&rtClient);
			ClientToScreen(&rtClient);
			GetWindowRect(&rtWindow);

			xoffset = rtClient.left - rtWindow.left;
			yoffset = rtClient.top - rtWindow.top;
			::ScreenToClient(m_hWnd, &point );
			point.x += xoffset;
			point.y += yoffset;
			SCROLL_HITTEST test = ScrollHitTest(SB_VERT,point,FALSE);

			if(test == SCROLL_TOP_ARROW)
			{
				m_nVArrowTopState = keScrollHover;
				m_nVArrowBottomState = keScrollNormal;
				m_nVThumbState = keScrollNormal;
				DrawVScrollBar(&dc);
			}
			else if(test == SCROLL_BOTTOM_ARROW)
			{
				m_nVArrowTopState = keScrollNormal;
				m_nVArrowBottomState = keScrollHover;
				m_nVThumbState = keScrollNormal;
				DrawVScrollBar(&dc);
			}
			else if(test == SCROLL_THUMB)
			{
				m_nVArrowTopState = keScrollNormal;
				m_nVArrowBottomState = keScrollNormal;
				m_nVThumbState = keScrollHover;
				DrawVScrollBar(&dc);
			}
			else
			{
				m_nVArrowTopState = keScrollNormal;
				m_nVArrowBottomState = keScrollNormal;
				m_nVThumbState = keScrollNormal;
				DrawVScrollBar(&dc);
			}
			m_nHArrowLeftState = keScrollNormal;
			m_nHArrowRightState = keScrollNormal;
			m_nHThumbState = keScrollNormal;
			if(m_bHScroll)
				DrawHScrollBar(&dc);
		}
		break;
	default:
		Default();
	}
}