﻿#include "StdAfx.h"
#include "..\include\skinimages.h"

IMPLEMENT_SERIAL(CSkinImages,CObject,1)
CSkinImages::CSkinImages(void)
: m_nImageCount(0)
, m_pTemp(NULL)
{
}

CSkinImages::~CSkinImages(void)
{
	for(int i = 0 ;i < m_arrayImages.GetCount() ; i++)
	{
		CSkinBitmap *pImage = m_arrayImages.GetAt(i);
		
		if(pImage)
			delete pImage;
	}
	m_arrayImages.RemoveAll();
}


void CSkinImages::Serialize(CArchive& ar)
{
	if(ar.IsStoring())
	{
		m_nImageCount = m_arrayImages.GetCount();
		ar.Write(&m_nImageCount,sizeof(m_nImageCount));
		for(int i = 0 ; i < m_arrayImages.GetCount() ; i++)
		{
			CSkinBitmap *pImage = m_arrayImages.GetAt(i);
			pImage->Serialize(ar);
		}
	}
	else
	{
		for(int i = 0 ;i < m_arrayImages.GetCount() ; i++)
		{
			CSkinBitmap *pImage = m_arrayImages.GetAt(i);

			if(pImage)
				delete pImage;
		}
		m_arrayImages.RemoveAll();

		ar.Read(&m_nImageCount,sizeof(m_nImageCount));
		for(int i = 0 ;i < m_nImageCount ;i ++)
		{
			CSkinBitmap *pImage = new CSkinBitmap;
			pImage->Serialize(ar);
			m_arrayImages.Add(pImage);
		}
	}
}
BOOL CSkinImages::LoadImage(CString in_strImagePath)
{
	CSkinBitmap		*pImage = NULL;
	pImage = FindImage(in_strImagePath);
	if(pImage)
		return FALSE;
	CFile			cFile;
	CFileException	e;
	
	pImage = new CSkinBitmap;
	pImage->m_strName = in_strImagePath;
	if(!pImage->LoadImage(in_strImagePath))
		return FALSE;
	m_arrayImages.Add(pImage);
	return TRUE;
}
void CSkinImages::AddImage(CSkinBitmap &in_image)
{
	
}
void CSkinImages::ModifyHue(int nPercent)
{
	for(int i = 0 ; i < m_arrayImages.GetCount() ; i++)
	{
		CSkinBitmap *pImage = m_arrayImages[i];
		pImage->ModifyHue(nPercent);
	}
}
CSkinBitmap* CSkinImages::FindImage(CString in_strImageName)
{
	if(in_strImageName.IsEmpty())
		return NULL;
	for(int i = 0 ; i < m_arrayImages.GetCount() ; i++)
	{
		CSkinBitmap *pImage = m_arrayImages[i];
		CString strName = pImage->m_strName;
		if(in_strImageName == strName)
		{
			return pImage;
		}
	}
	return NULL;
}
BOOL CSkinImages::DeleteImage(CString in_strImageName)
{
	for(int i = 0 ; i < m_arrayImages.GetCount() ; i++)
	{
		CSkinBitmap *pImage = m_arrayImages[i];
		CString strName = pImage->m_strName;
		if(in_strImageName == strName)
		{
			delete pImage;
			m_arrayImages.RemoveAt(i);
			return TRUE;
		}
	}
	return FALSE;
}
BOOL CSkinImages::CopyImage(CString in_strImageName)
{
	if(m_pTemp)
		delete m_pTemp;
	for(int i = 0 ; i < m_arrayImages.GetCount() ; i++)
	{
		CSkinBitmap *pImage = m_arrayImages[i];
		CString strName = pImage->m_strName;
		if(in_strImageName == strName)
		{
			m_pTemp = new CSkinBitmap;
			*m_pTemp = *pImage;
			return TRUE;
		}
	}
	return FALSE;
}
BOOL CSkinImages::CutImage(CString in_strImageName)
{
	if(m_pTemp)
		delete m_pTemp;
	m_pTemp = NULL;
	for(int i = 0 ; i < m_arrayImages.GetCount() ; i++)
	{
		CSkinBitmap *pImage = m_arrayImages[i];
		CString strName = pImage->m_strName;
		if(in_strImageName == strName)
		{
			m_pTemp = pImage;
			m_arrayImages.RemoveAt(i);
			return TRUE;
		}
	}
	return FALSE;
}
BOOL CSkinImages::PasteImage()
{
	if(m_pTemp)
	{
		CSkinBitmap *pImage = new CSkinBitmap;
		*pImage = *m_pTemp;
		m_arrayImages.Add(pImage);
		return TRUE;
	}
	else
		return FALSE;
}