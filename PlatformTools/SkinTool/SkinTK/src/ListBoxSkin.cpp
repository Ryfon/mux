﻿#include "StdAfx.h"
#include "..\include\listboxskin.h"

IMPLEMENT_SERIAL(CListBoxSkin,CObjectSkin,1)
CListBoxSkin::CListBoxSkin(void)
{
	m_nSkinType = keListBoxSkin;
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontListBox = nif.lfCaptionFont;
	m_bAutoFont = TRUE;
}
CListBoxSkin::CListBoxSkin(CString strName)
: CObjectSkin(strName,keListBoxSkin)
{
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontListBox = nif.lfCaptionFont;
	m_bAutoFont = TRUE;
}
CListBoxSkin::~CListBoxSkin(void)
{
}
void CListBoxSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_fontListBox,sizeof(m_fontListBox));
		ar.Write(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Write(&m_imageBorder,sizeof(m_imageBorder));
		ar.Write(&m_colorBorder,sizeof(m_colorBorder));
	}
	else
	{
		ar.Read(&m_fontListBox,sizeof(m_fontListBox));
		ar.Read(&m_bAutoFont,sizeof(m_bAutoFont));
		ar.Read(&m_imageBorder,sizeof(m_imageBorder));
		ar.Read(&m_colorBorder,sizeof(m_colorBorder));
	}
}