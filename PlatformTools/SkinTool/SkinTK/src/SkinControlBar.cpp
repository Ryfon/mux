﻿#include "StdAfx.h"
#include "..\include\skincontrolbar.h"

CSkinControlBar::CSkinControlBar(void)
{
	m_pWinSkin = GetSkin().GetWindowSkin();
}

CSkinControlBar::~CSkinControlBar(void)
{
	HookWindow((HWND)NULL);
}
void CSkinControlBar::InstallSkin(HWND hWnd)
{
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );

	if(m_pWinSkin == NULL)
		m_pWinSkin = GetSkin().GetWindowSkin();
	if(m_pWinSkin == NULL)
		m_bEnableSkin = FALSE;
	else
		m_bEnableSkin = TRUE;

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);

	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 

}
void CSkinControlBar::LoadSkin()
{
	m_bEnableSkin = FALSE;
	m_pWinSkin = GetSkin().GetWindowSkin();
	if(m_pWinSkin == NULL)
		m_bEnableSkin = FALSE;
	else
		m_bEnableSkin = TRUE;
	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}

void CSkinControlBar::OnPaint()
{
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CPaintDC dc(pWnd);
	DrawControlBar(&dc);
	Default();
}
BOOL CSkinControlBar::OnEraseBkgnd(CDC *pDC)
{
	DrawControlBar(pDC);
	return TRUE;
}
void CSkinControlBar::OnNcPaint(HRGN rgn1)
{
	Default();
	/*CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CDC *pDC = pWnd->GetWindowDC();
	DrawControlBar(pDC);*/
}

void CSkinControlBar::DrawControlBar(CDC *pDC)
{
	if(m_pWinSkin == NULL)
		return;
	CRect rtWindow;
	CWnd *pWnd = (CWnd*)CWnd::FromHandle(m_hWnd);
	pWnd->GetWindowRect(rtWindow);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);
	if(!m_pWinSkin->DrawImageRect(pDC,rtWindow,m_pWinSkin->m_imageBackground))
	{
		COLORREF color = m_pWinSkin->m_colorBack;
		CBrush brush(color);
		pDC->FillRect(rtWindow,&brush);
	}
}