﻿#include "StdAfx.h"
#include "..\include\skinslider.h"
#include "..\include\SlideSkin.h"

CSkinSlider::CSkinSlider(void)
{
	m_pSliderSkin = GetSkin().GetSliderSkin();
}

CSkinSlider::~CSkinSlider(void)
{
	HookWindow( (HWND)NULL);
}
void CSkinSlider::InstallSkin(HWND hWnd)
{
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );


	m_pSliderSkin = GetSkin().GetSliderSkin();
	if(m_pSliderSkin == NULL)
		m_bEnableSkin = FALSE;

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);
	if(style & TBS_VERT == TBS_VERT)
		m_bVert = TRUE;
	else
		m_bVert = FALSE;


	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0,  SWP_FRAMECHANGED|SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
void CSkinSlider::LoadSkin()
{
	m_bEnableSkin = FALSE;
	m_pSliderSkin = GetSkin().GetSliderSkin();
	if(m_pSliderSkin == NULL)
		m_bEnableSkin = FALSE;
	else
		m_bEnableSkin = TRUE;
	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
LRESULT CSkinSlider::OnWndMsg(UINT msg,WPARAM wp,LPARAM lp)
{
	switch(msg)
	{
	case WM_MOUSELEAVE:
		OnMouseLeave();
		return 0;
	}
	return CSkinWnd::OnWndMsg(msg,wp,lp);
}
void CSkinSlider::OnPaint()
{
	CSliderCtrl *pSlider = (CSliderCtrl*)CWnd::FromHandle(m_hWnd);
	CPaintDC dc(pSlider);
	DrawSlider(&dc);
}
BOOL CSkinSlider::OnEraseBkgnd(CDC *pDC)
{
	//	CSpinButtonCtrl
	return TRUE;
}

void CSkinSlider::OnMouseMove(UINT nFlags, CPoint point)
{
	Default();
}
void CSkinSlider::OnLButtonDown(UINT nFlags, CPoint point)
{
	Default();
}
void CSkinSlider::OnMouseLeave()
{

}
void CSkinSlider::OnLButtonUp(UINT nFlags, CPoint point)
{
	Default();
}
UINT CSkinSlider::HitTest(CPoint point)
{
	return 1;
}
void CSkinSlider::DrawSlider(CDC *pDC)
{
	if(m_pSliderSkin == NULL)
		return;
	CSliderCtrl *pSlider = (CSliderCtrl*)CWnd::FromHandle(m_hWnd);
	CRect rtClient,rtChannel,rtThumb;
	GetClientRect(rtClient);
	pSlider->GetChannelRect(rtChannel);
	pSlider->GetThumbRect(rtThumb);

	m_pSliderSkin->DrawImageSection(pDC,rtClient,m_pSliderSkin->m_imageBackground);
	m_pSliderSkin->DrawImageSection(pDC,rtChannel,m_pSliderSkin->m_imageChannel);
	CMemDC dc(pDC,rtThumb);
	if(m_bVert)
		m_pSliderSkin->DrawImageSection(&dc,rtThumb,m_pSliderSkin->m_imageThumbVert);
	else
		m_pSliderSkin->DrawImageSection(&dc,rtThumb,m_pSliderSkin->m_imageThumbHorz);

}