﻿#include "StdAfx.h"
#include "..\include\checkboxskin.h"

IMPLEMENT_SERIAL(CCheckBoxSkin,CObjectSkin,1)
CCheckBoxSkin::CCheckBoxSkin(void)
{
	m_nSkinType = keCheckBoxSkin;
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontCheckBox = nif.lfCaptionFont;
	m_nBackMode = keBackColor;
	m_bAutoFont = TRUE;
	m_colorBackground = GetSysColor(COLOR_BTNFACE);
}
CCheckBoxSkin::CCheckBoxSkin(CString strName)
:CObjectSkin(strName,keCheckBoxSkin)
{
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	m_fontCheckBox = nif.lfCaptionFont;
	m_nBackMode = keBackColor;
	m_bAutoFont = TRUE;
	m_colorBackground = GetSysColor(COLOR_BTNFACE);
}
CCheckBoxSkin::~CCheckBoxSkin(void)
{
}
void CCheckBoxSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_fontCheckBox,sizeof(m_fontCheckBox));
		ar << m_bAutoFont;
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar.Write(&m_colorBackground,sizeof(m_colorBackground));
		ar.Write(&m_nBackMode,sizeof(m_nBackMode));
		ar.Write(m_stateCheckBox,sizeof(m_stateCheckBox));
	}
	else
	{
		ar.Read(&m_fontCheckBox,sizeof(m_fontCheckBox));
		ar >> m_bAutoFont;
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar.Read(&m_colorBackground,sizeof(m_colorBackground));
		ar.Read(&m_nBackMode,sizeof(m_nBackMode));
		ar.Read(m_stateCheckBox,sizeof(m_stateCheckBox));
	}
}

void CCheckBoxSkin::DrawBackground(CDC *pDC,CRect rtDest)
{
	
	if(!DrawImageSection(pDC,rtDest,m_imageBackground))
	{
		CBrush brush(m_colorBackground);
		pDC->FillRect(rtDest,&brush);
	}
}

void CCheckBoxSkin::DrawBox(CDC *pDC,CRect rtDest,int nState,int nCheck)
{
	if(nCheck == 0)
	{
		if(!DrawImageRect(pDC,rtDest,m_stateCheckBox[nState].imageChecked))
			DrawImageRect(pDC,rtDest,m_stateCheckBox[keCheckNormal].imageChecked);
	}
	else if(nCheck == 1)
	{
		if(!DrawImageRect(pDC,rtDest,m_stateCheckBox[nState].imageUnchecked))
			DrawImageRect(pDC,rtDest,m_stateCheckBox[keCheckNormal].imageUnchecked);
	}
	else if(nCheck == 2)
	{
		if(!DrawImageRect(pDC,rtDest,m_stateCheckBox[nState].imageIndeterminate))
			DrawImageRect(pDC,rtDest,m_stateCheckBox[keCheckNormal].imageIndeterminate);
	}
	else
	{
		DrawImageRect(pDC,rtDest,m_stateCheckBox[keCheckNormal].imageChecked);
	}

}