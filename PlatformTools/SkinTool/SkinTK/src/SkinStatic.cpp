﻿#include "StdAfx.h"
#include "..\include\skinstatic.h"
#include "..\include\StaticSkin.h"

CSkinStatic::CSkinStatic(void)
{
	m_pStaticSkin = GetSkin().GetStaticSkin();
	m_uTextFormat = 0;
}

CSkinStatic::~CSkinStatic(void)
{
	HookWindow((HWND)NULL);
}
void CSkinStatic::LoadSkin()
{
	m_bEnableSkin = FALSE;

	m_pStaticSkin = GetSkin().GetStaticSkin();

	if(m_pStaticSkin == NULL)
		m_bEnableSkin = FALSE;
	else 
		m_bEnableSkin = TRUE;
	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
void CSkinStatic::InstallSkin(HWND hWnd)
{
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );

	if(m_pStaticSkin == NULL)
		m_pStaticSkin = GetSkin().GetStaticSkin();

	if(m_pStaticSkin == NULL)
		m_bEnableSkin = FALSE;
	else 
		m_bEnableSkin = TRUE;

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);

	
	if((style & SS_RIGHT) == SS_RIGHT)
		m_uTextFormat |= DT_RIGHT;
	else if((style & SS_CENTER) == SS_CENTER)
		m_uTextFormat |= DT_CENTER;
	else
		m_uTextFormat |= DT_LEFT;
//	m_uTextFormat |= DT_END_ELLIPSIS;

	switch (style & SS_TYPEMASK)
	{
	case SS_LEFT:
		m_uTextFormat = DT_LEFT | DT_EXPANDTABS | DT_WORDBREAK;
		break;

	case SS_CENTER:
		m_uTextFormat = DT_CENTER | DT_EXPANDTABS | DT_WORDBREAK;
		break;

	case SS_RIGHT:
		m_uTextFormat = DT_RIGHT | DT_EXPANDTABS | DT_WORDBREAK;
		break;

	case SS_SIMPLE:
		m_uTextFormat = DT_LEFT | DT_SINGLELINE;
		break;

	case SS_LEFTNOWORDWRAP:
		m_uTextFormat = DT_LEFT | DT_EXPANDTABS;
		break;

	default:
		return;
	}

	if (style & SS_NOPREFIX)
		m_uTextFormat |= DT_NOPREFIX;

	if ((style & SS_TYPEMASK) != SS_SIMPLE)
	{
		if (style & SS_CENTERIMAGE)
			m_uTextFormat |= DT_SINGLELINE | DT_VCENTER;
		if (style & SS_EDITCONTROL)
			m_uTextFormat |= DT_EDITCONTROL;
		if (style & SS_ENDELLIPSIS)
			m_uTextFormat |= DT_SINGLELINE | DT_END_ELLIPSIS;
		if (style & SS_PATHELLIPSIS)
			m_uTextFormat |= DT_SINGLELINE | DT_PATH_ELLIPSIS;
		if (style & SS_WORDELLIPSIS)
			m_uTextFormat |= DT_SINGLELINE | DT_WORD_ELLIPSIS;
	}

	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 

}
void CSkinStatic::OnPaint()
{
	CPaintDC dc(CWnd::FromHandle(m_hWnd));
	DrawStatic(&dc);
}
void CSkinStatic::OnSetText(WPARAM wp,LPARAM lp)
{
	Default();
	CDC *pDC = CWnd::FromHandle(m_hWnd)->GetDC();
	DrawStatic(pDC);
}
void CSkinStatic::OnNcPaint(HRGN rgn1)
{
	CDC *pDC = CWnd::FromHandle(m_hWnd)->GetDC();
	DrawStatic(pDC);
}
BOOL CSkinStatic::OnEraseBkgnd(CDC *pDC)
{
	CRect rtWindow;
	CStatic *pStatic = (CStatic*)CWnd::FromHandle(m_hWnd);
	pStatic->GetWindowRect(rtWindow);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);
	if(m_pStaticSkin->m_bTransparent)
	{
		CBrush brushBack(m_pStaticSkin->m_colorBack);
		pDC->FillRect(rtWindow,&brushBack);
	}
	else
	{
		m_pStaticSkin->DrawImageSection(pDC,rtWindow,m_pStaticSkin->m_imageBack);
	}
	return TRUE;
}
void CSkinStatic::DrawStatic(CDC *pDC)
{
	
	if(m_pStaticSkin == NULL)
		return;
	CRect rtWindow;
	CStatic *pStatic = (CStatic*)CWnd::FromHandle(m_hWnd);
	pStatic->GetWindowRect(rtWindow);
	rtWindow.OffsetRect(-rtWindow.left,-rtWindow.top);

	int nOldMode = pDC->SetBkMode(TRANSPARENT);
	if(m_pStaticSkin->m_bTransparent)
	{
		CBrush brushBack(m_pStaticSkin->m_colorBack);
		pDC->FillRect(rtWindow,&brushBack);
	}
	else
	{
		m_pStaticSkin->DrawImageSection(pDC,rtWindow,m_pStaticSkin->m_imageBack);
	}
	CString strCaption;
	pStatic->GetWindowText(strCaption);
	CFont *pFont = NULL;
	CFont *pOld = NULL;
	if(m_pStaticSkin->m_bAutoFont)
	{
		pFont = pStatic->GetFont(); 
		pOld = (CFont*)pDC->SelectObject( pFont );
	}
	else
	{
		pFont = new CFont;
		pFont->CreateFontIndirect(&(m_pStaticSkin->m_fontStatic));
		pOld = (CFont*)pDC->SelectObject( pFont );
	}
	pDC->DrawText(strCaption,rtWindow,m_uTextFormat);

	HICON hIcon = pStatic->GetIcon();
	ICONINFO piconinfo;
	CRect rtIcon;
	if(hIcon)
	{
		GetIconInfo(hIcon,&piconinfo); //取图标信息
		rtIcon = rtWindow;

		DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );

		if (style & SS_CENTERIMAGE)
		{
			rtIcon.left = (rtWindow.right - rtWindow.left) / 2 - piconinfo.xHotspot;
			rtIcon.top = (rtWindow.bottom - rtWindow.top) / 2 - piconinfo.yHotspot;
			rtIcon.right = rtIcon.left + piconinfo.xHotspot*2;
			rtIcon.bottom = rtIcon.top + piconinfo.yHotspot*2;
		}

			
		
		DrawIconEx(pDC->GetSafeHdc(), rtIcon.left,rtIcon.top, hIcon,rtIcon.Width(),rtIcon.Height(),0, NULL, DI_NORMAL);
	}

	pDC->SetBkMode(nOldMode);
	pDC->SelectObject(pOld);
	if(!m_pStaticSkin->m_bAutoFont)
		delete pFont;
}