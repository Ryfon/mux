﻿#include "StdAfx.h"
#include "..\include\skinspin.h"
#include "..\include\SpinSkin.h"

CSkinSpin::CSkinSpin(void)
{
	m_pSpinSkin = GetSkin().GetSpinSkin();
	m_nHotButton = UNKNOW;
	m_nPressedButton = UNKNOW;
}

CSkinSpin::~CSkinSpin(void)
{
	HookWindow( (HWND)NULL);
}
void CSkinSpin::InstallSkin(HWND hWnd)
{
	m_bEnableSkin = TRUE;
	HookWindow( (HWND)NULL);

	int r = HookWindow( hWnd );


	m_pSpinSkin = GetSkin().GetSpinSkin();
	if(m_pSpinSkin == NULL)
		m_bEnableSkin = FALSE;

	DWORD style		= GetWindowLong( m_hWnd, GWL_STYLE );
	DWORD exstyle	=GetWindowLong(m_hWnd,GWL_EXSTYLE);
	if(style &UDS_HORZ  == UDS_HORZ)
		m_bVert = FALSE;
	else
		m_bVert = TRUE;

	m_nHotButton = UNKNOW;
	m_nPressedButton = UNKNOW;
	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0,  SWP_FRAMECHANGED|SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
void CSkinSpin::LoadSkin()
{
	m_bEnableSkin = FALSE;
	m_pSpinSkin = GetSkin().GetSpinSkin();
	if(m_pSpinSkin == NULL)
		m_bEnableSkin = FALSE;
	else
		m_bEnableSkin = TRUE;
	SetWindowPos(m_hWnd,NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 
}
LRESULT CSkinSpin::OnWndMsg(UINT msg,WPARAM wp,LPARAM lp)
{
	switch(msg)
	{
	case WM_MOUSELEAVE:
		OnMouseLeave();
		return 0;
	}
	return CSkinWnd::OnWndMsg(msg,wp,lp);
}
void CSkinSpin::OnPaint()
{
	CSpinButtonCtrl *pSpin = (CSpinButtonCtrl*)CWnd::FromHandle(m_hWnd);
	CPaintDC dc(pSpin);
	DrawSpin(&dc);

}
BOOL CSkinSpin::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}

void CSkinSpin::OnMouseMove(UINT nFlags, CPoint point)
{
	//Default();
	if((m_nHotButton == UNKNOW) && ((nFlags & MK_LBUTTON) == 0) )
	{
		TRACKMOUSEEVENT tme;
		m_nHotButton = HitTest(point);
		m_nPressedButton = UNKNOW;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = m_hWnd;
		TrackMouseEvent(&tme);
	}
	else if(nFlags & MK_LBUTTON)
	{
		TRACKMOUSEEVENT tme;
		m_nPressedButton = HitTest(point);
		m_nHotButton = UNKNOW;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = m_hWnd;
		TrackMouseEvent(&tme);
	}
	CSpinButtonCtrl *pSpin = (CSpinButtonCtrl*)CWnd::FromHandle(m_hWnd);
	CDC *pDC = pSpin->GetDC();
	DrawSpin(pDC);
	
}
void CSkinSpin::OnLButtonDown(UINT nFlags, CPoint point)
{
	Default();
	m_nPressedButton = HitTest(point);
	m_nHotButton = UNKNOW;
	CSpinButtonCtrl *pSpin = (CSpinButtonCtrl*)CWnd::FromHandle(m_hWnd);
	CDC *pDC = pSpin->GetDC();
	DrawSpin(pDC);
}
void CSkinSpin::OnMouseLeave()
{
	if(m_nHotButton != UNKNOW)
		m_nHotButton = UNKNOW;
	if(m_nPressedButton != UNKNOW)
		m_nPressedButton = UNKNOW;
	CSpinButtonCtrl *pSpin = (CSpinButtonCtrl*)CWnd::FromHandle(m_hWnd);
	CDC *pDC = pSpin->GetDC();
	DrawSpin(pDC);
}
void CSkinSpin::OnLButtonUp(UINT nFlags, CPoint point)
{
	Default();
	m_nPressedButton = UNKNOW;
	CSpinButtonCtrl *pSpin = (CSpinButtonCtrl*)CWnd::FromHandle(m_hWnd);
	CDC *pDC = pSpin->GetDC();
	DrawSpin(pDC);
	
}
UINT CSkinSpin::HitTest(CPoint point)
{
	CRect rtClient;
	GetClientRect(rtClient);
	CRect rtUp,rtDown;
	rtUp = rtClient;
	rtDown = rtClient;
	if(m_bVert)
	{
		rtUp.bottom = (rtClient.top + rtClient.bottom)/2;
		rtDown.top = (rtClient.top + rtClient.bottom)/2;
	}
	else
	{
		rtUp.right = (rtClient.left + rtClient.right)/2;
		rtDown.left = (rtClient.left + rtClient.right)/2;
	}
	
	if(rtUp.PtInRect(point))
		return UP;
	if(rtDown.PtInRect(point))
		return DOWN;
	return UNKNOW;
}
void CSkinSpin::DrawSpin(CDC *pDC)
{
	CSpinButtonCtrl *pSpin = (CSpinButtonCtrl*)CWnd::FromHandle(m_hWnd);
	CRect rtClient;
	GetClientRect(rtClient);
	rtClient.DeflateRect(2,0,1,0);

	CRect rtUp,rtDown;
	rtUp = rtClient;
	rtDown = rtClient;
	if(m_bVert)
	{
		rtUp.bottom = (rtClient.top + rtClient.bottom)/2;
		rtDown.top = (rtClient.top + rtClient.bottom)/2;

		if(m_nPressedButton == UP)
		{
			if(!m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbVert[keSpinPressed].imageUp))
				m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbVert[keSpinNormal].imageUp);
			m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbVert[keSpinNormal].imageDown);
		}
		else if(m_nPressedButton == DOWN)
		{
			m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbVert[keSpinNormal].imageUp);
			if(!m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbVert[keSpinPressed].imageDown))
				m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbVert[keSpinNormal].imageDown);
		}
		else if(m_nHotButton == UP)
		{
			if(!m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbVert[keSpinHover].imageUp))
				m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbVert[keSpinNormal].imageUp);
			m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbVert[keSpinNormal].imageDown);
		}
		else if(m_nHotButton == DOWN)
		{
			m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbVert[keSpinNormal].imageUp);
			if(!m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbVert[keSpinHover].imageDown))
				m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbVert[keSpinNormal].imageDown);
		}
		else
		{
			m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbVert[keSpinNormal].imageUp);
			m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbVert[keSpinNormal].imageDown);
		}
	}
	else
	{
		rtUp.right = (rtClient.left + rtClient.right)/2;
		rtDown.left = (rtClient.left + rtClient.right)/2;

		if(m_nPressedButton == UP)
		{
			if(!m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbHort[keSpinPressed].imageUp))
				m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbHort[keSpinNormal].imageUp);
			m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbHort[keSpinNormal].imageDown);
		}
		else if(m_nPressedButton == DOWN)
		{
			m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbHort[keSpinNormal].imageUp);
			if(!m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbHort[keSpinPressed].imageDown))
				m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbHort[keSpinNormal].imageDown);
		}
		else if(m_nHotButton == UP)
		{
			if(!m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbHort[keSpinHover].imageUp))
				m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbHort[keSpinNormal].imageUp);
			m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbHort[keSpinNormal].imageDown);
		}
		else if(m_nHotButton == DOWN)
		{
			m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbHort[keSpinNormal].imageUp);
			if(m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbHort[keSpinHover].imageDown))
				m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbHort[keSpinNormal].imageDown);
		}
		else
		{
			m_pSpinSkin->DrawImageSection(pDC,rtUp,m_pSpinSkin->m_thumbHort[keSpinNormal].imageUp);
			m_pSpinSkin->DrawImageSection(pDC,rtDown,m_pSpinSkin->m_thumbHort[keSpinNormal].imageDown);
		}
	}
}