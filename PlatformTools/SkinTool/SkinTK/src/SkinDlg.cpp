﻿#include "StdAfx.h"
#include "..\include\skindlg.h"

CSkinDlg::CSkinDlg(void)
{
}

CSkinDlg::~CSkinDlg(void)
{
}
BOOL CSkinDlg::OnEraseBkgnd(CDC *pDC)
{
	CWnd *pWnd = CWnd::FromHandle(m_hWnd);
	CRect rtClient;
	GetClientRect(&rtClient);
	CMemDC memDC(pDC,rtClient);
	m_pSkinWindow->DrawBackground(&memDC,rtClient);
	return TRUE;
}