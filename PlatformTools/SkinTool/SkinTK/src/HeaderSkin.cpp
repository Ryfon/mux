﻿#include "StdAfx.h"
#include "..\include\headerskin.h"

IMPLEMENT_SERIAL(CHeaderSkin,CObjectSkin,1)
CHeaderSkin::CHeaderSkin(void)
{
	m_nSkinType = keHeaderSkin;
}
CHeaderSkin::CHeaderSkin(CString strName)
: CObjectSkin(strName,keHeaderSkin)
{
	
}
CHeaderSkin::~CHeaderSkin(void)
{
}
void CHeaderSkin::Serialize(CArchive &ar)
{
	CObjectSkin::Serialize(ar);
	if(ar.IsStoring())
	{
		ar.Write(&m_imageBackground,sizeof(m_imageBackground));
		ar.Write(&m_imageItemNormal,sizeof(m_imageItemNormal));
		ar.Write(&m_imageItemHover,sizeof(m_imageItemHover));
		ar.Write(&m_imageItemPressed,sizeof(m_imageItemPressed));
	}
	else
	{
		ar.Read(&m_imageBackground,sizeof(m_imageBackground));
		ar.Read(&m_imageItemNormal,sizeof(m_imageItemNormal));
		ar.Read(&m_imageItemHover,sizeof(m_imageItemHover));
		ar.Read(&m_imageItemPressed,sizeof(m_imageItemPressed));
	}
}