﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINDLG_H
#define _SKINDLG_H

#if _MSC_VER > 1000
#pragma once
#endif
#include "SkinMainWnd.h"

class SKINTKDLL CSkinDlg :public CSkinFrame
{
public:
	CSkinDlg(void);
	virtual ~CSkinDlg(void);
	virtual BOOL OnEraseBkgnd(CDC *pDC);
};

#endif
