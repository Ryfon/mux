﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SCROLLBARSKIN_H
#define _SCROLLBARSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "ObjectSkin.h"


class SKINTKDLL CScrollBarSkin : public CObjectSkin
{
	DECLARE_SERIAL(CScrollBarSkin);
public:
	CScrollBarSkin(void);
	CScrollBarSkin(CString strName);
	virtual ~CScrollBarSkin(void);

	void Serialize(CArchive &ar);

	void DrawHArrowLeft(CDC *pDC,CRect rtDest,int nState);
	void DrawHArrowRight(CDC *pDC,CRect rtDest,int nState);
	void DrawHThumb(CDC *pDC,CRect rtDest,int nState);
	void DrawHBackground(CDC *pDC,CRect rtDest);

	void DrawVArrowTop(CDC *pDC,CRect rtDest,int nState);
	void DrawVArrowBottom(CDC *pDC,CRect rtDest,int nState);
	void DrawVThumb(CDC *pDC,CRect rtDest,int nState);
	void DrawVBackground(CDC *pDC,CRect rtDest);
	
	void DrawHScroll(CDC *pDC,CRect rtDest,int nState);
	void DrawVScroll(CDC *pDC,CRect rtDest,int nState);

	void DrawSizing(CDC *pDC,CRect rtDest);

public:
	ScrollBarStateSkin m_skinHScrollBar;
	ScrollBarStateSkin m_skinVScrollBar;
	SkinImageSection   m_imageSizing;
};

#endif