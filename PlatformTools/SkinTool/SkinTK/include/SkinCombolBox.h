﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINCOMBOLBOX_H
#define _SKINCOMBOLBOX_H

#if _MSC_VER > 1000
#pragma once
#endif
#include "..\include\SkinWnd.h"
class CSkinCombolBox : public CSkinWnd
{
public:
	CSkinCombolBox(void);
	virtual ~CSkinCombolBox(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();
	virtual void OnNcMouseMove( UINT nHitTest, CPoint point );
	virtual void OnMouseMove(UINT nFlags,CPoint point);
	virtual void OnNcLButtonDown( UINT nHitTest, CPoint point );
	virtual void OnNcLButtonDblClk( UINT nHitTest,  CPoint point );
	virtual void OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	virtual UINT OnNcHitTest( CPoint point );
	virtual void OnNcPaint(HRGN rgn1);
	virtual void OnCaptureChanged(WPARAM wp,LPARAM lp);

	virtual LRESULT OnWndMsg(UINT msg, WPARAM wp, LPARAM lp);
protected:
	CListBoxSkin *	m_pListBoxSkin;
};
#endif
