﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _COMBOBOXSKIN_H
#define _COMBOBOXSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif
enum ThumbState
{
	keThumbNormal = 0,
	keThumbHovered,
	keThumbPressed,
	keThumbDisabled,
	keThumbSize
};
class CComboBoxSkin : public CObjectSkin
{
	DECLARE_SERIAL(CComboBoxSkin);
public:
	CComboBoxSkin(void);
	CComboBoxSkin(CString strName);
	virtual ~CComboBoxSkin(void);

	void Serialize(CArchive &ar);
public:
	SkinLogFont		m_fontComboBox;
	int				m_bAutoFont;
	SkinImageSection m_imageBorder;
	SkinImageRect	 m_iamgeThumb[keThumbSize];
};

#endif