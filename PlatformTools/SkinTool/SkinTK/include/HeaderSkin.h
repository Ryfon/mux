﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _HEADERSKIN_H
#define _HEADERSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class CHeaderSkin : public CObjectSkin
{
	DECLARE_SERIAL(CHeaderSkin);
public:
	CHeaderSkin(void);
	CHeaderSkin(CString strName);
	virtual ~CHeaderSkin(void);
	void Serialize(CArchive &ar);
public:
	SkinImageSection	m_imageBackground;
	SkinImageSection	m_imageItemNormal;
	SkinImageSection	m_imageItemHover;
	SkinImageSection	m_imageItemPressed;
};
#endif
