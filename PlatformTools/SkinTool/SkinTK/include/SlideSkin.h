﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SLIDERSKIN_H
#define _SLIDERSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif


class SKINTKDLL CSliderSkin  : public CObjectSkin
{
	DECLARE_SERIAL(CSliderSkin);
public:
	CSliderSkin(void);
	CSliderSkin(CString strName);
	virtual ~CSliderSkin(void);
	
	void Serialize(CArchive &ar);
public:
	SkinImageSection	m_imageBackground;
	SkinImageSection	m_imageChannel;
	SkinImageSection	m_imageThumbHorz;
	SkinImageSection	m_imageThumbVert;
};

#endif
