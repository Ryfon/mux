﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/

#ifndef _SKINTOOL_
#define _SKINTOOL_

#if _MSC_VER > 1000
#pragma	once
#endif
#include <windows.h>

#ifdef SKINTK_EXPORTS 
#define SKINTKDLL  __declspec(dllexport)
#else
#define SKINTKDLL  __declspec(dllimport)
#endif

SKINTKDLL BOOL  __stdcall SkinInit();
SKINTKDLL BOOL  __stdcall SkinExit();

SKINTKDLL BOOL  __stdcall SkinLoadSkin(LPCTSTR lpszFileName);
SKINTKDLL void  __stdcall SkinModifyHue(int nPercent);
SKINTKDLL void  __stdcall SetWindowSkin( HWND hWnd);
SKINTKDLL void  __stdcall RemoveWindowSkin( HWND hWnd );


#endif