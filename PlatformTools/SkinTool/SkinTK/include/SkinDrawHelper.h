﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINDRAWHELPER_H
#define _SKINDRAWHELPER_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CSkinDrawHelper
{
public:
	CSkinDrawHelper(void);
	~CSkinDrawHelper(void);

	bool	DrawImageRect(CDC *pDC,CRect rtDest,SkinImageRect & imageRect);
	bool	DrawImageSection(CDC *pDC,CRect rtDest,SkinImageSection &imageSection);
//	bool	DrawFrameSkin(CDC *pDC,CRect rtDest,WindowSkin &windowSkin,bool bActive);
//	bool	DrawButton(CDC *pDC,CRect rtDest,ButtonSkin &buttonSkin,ButtonState buttonState);
//	bool	DrawButton(CDC *pDC,CRect rtDest,WinButtonSkin &buttonSkin,WinButtonState buttonState);
};
SKINTKDLL CSkinDrawHelper* GetSkinDrawHelper();

#endif