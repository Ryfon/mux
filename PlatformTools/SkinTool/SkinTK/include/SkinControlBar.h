﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINCONTROLBAR_H
#define _SKINCONTROLBAR_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"

class CSkinControlBar : public CSkinWnd
{
public:
	CSkinControlBar(void);
	~CSkinControlBar(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();

	void OnPaint();
	BOOL OnEraseBkgnd(CDC *pDC);
	void OnNcPaint(HRGN rgn1);

	void DrawControlBar(CDC *pDC);

protected:
	CWindowSkin *m_pWinSkin;
};
#endif