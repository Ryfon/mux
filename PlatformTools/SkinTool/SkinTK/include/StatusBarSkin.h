﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _STATUSBARSKIN_H
#define _STATUSBARSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class CStatusBarSkin : public CObjectSkin
{
	DECLARE_SERIAL(CStatusBarSkin);
public:
	CStatusBarSkin(void);
	CStatusBarSkin(CString strName);
	virtual ~CStatusBarSkin(void);

	void Serialize(CArchive &ar);

public:
	SkinImageSection m_imageBackground;
	SkinImageSection m_imageItem;
	COLORREF	m_colorBack;
};

#endif