﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 定义工程中用到的枚举类型
*************************************************/
#ifndef _SKINENUM_H
#define _SKINENUM_H 

#if _MSC_VER > 1000
#pragma once
#endif
//定义如何使用SkinImageMargins进行图形绘制
enum SkinSectionStretch
{
	keNoStretch = 0,
	keStretchAll,
	keStretchBottom,
	keStretchClient,
	keStretchLeft,
	keStretchRight,
	keStretchTop,
	keStretchCount
};
const CString stretch[keStretchCount] = 
{  _T("keNoStretch"),_T("keStretchAll"),
   _T("keStretchBottom"),_T("keStretchClient"),
   _T("keStretchLeft"),_T("keStretchRight"),
   _T("keStretchTop") };
enum SkinBackMode
{
	keBackImage = 0,
	keBackColor,
	keBackFade,
	keBackHalfFade,
	keBackFrameOnly,
	keBackFrameArea,
	keBackModeCount,
};
const CString backMode[keBackModeCount] = 
{  _T("keBackImage"),_T("keBackColor"),
_T("keBackFade"),_T("keBackHalfFade"),
_T("keBackFrameOnly"),_T("keBackFrameArea") };
//定义文本位置
enum SkinTextFlags
{
	keEllipsis = 0,
	keBottomCenter,
	keBottomLeft,
	keBottomRight,
	keCenter,
	keCenterLeft,
	keCenterRight,
	keTopCenter,
	keTopLeft,
	keTopRight,
	keRightToLeft,
	keTextCount
};
const CString TextFlags[keTextCount] = 
{  _T("keEllipsis"),_T("keBottomCenter"),
_T("keBottomLeft"),_T("keBottomRight"),
_T("keCenter"),_T("keCenterLeft"),
_T("keCenterRight"),_T("keTopCenter"),
_T("keTopLeft"),_T("keTopRight")
_T("keRightToLeft")};
//定义绘制方式
enum SkinBlend
{
	keOverwrite = 0,
	keAlphaBlend,
	keBlendCount
};
const CString Blend[keBlendCount] = 
{  _T("keOverwrite"),_T("keAlphaBlend")};
enum SkinType
{
	keUnknowSkin = -1,
	keWindowSkin =0,
	keWinButtonSkin,
	keWinStaticSkin,
	keScrollBarSkin,
	keDialogSkin,
	keStaticSkin,
	keButtonSkin,
	keCheckBoxSkin,
	keGroupBoxSkin,
	keRadioSkin,
	keComboxSkin,
	keEditSkin,
	keListBoxSkin,
	keHeaderSkin,
	kePopMenuSkin,
	keProgressSkin,
	keSliderSkin,
	keSpinSkin,
	keTabSkin,
	keToolbarSkin,
	keStatusbarSkin,
	keShapeWndSkin,
	keSkinCount
};
const CString SkinType[keSkinCount] = 
{  _T("WindiowSkin"),_T("DialogSkin"),
_T("ButtonSkin"),_T("StaticSkin"),
_T("ButtonCheckSkin"),_T("ButtonPushSkin")};
//定义静态的类型
enum SkinStaticType
{
	keStatic = 0,
	keDragArea,
	keWindowTitle,
	keResizeBottom,
	keResizeBottomLeft,
	keResizeBottomRight,
	keResizeLeft,
	keResizeRight,
	keResizeTopLeft,
	keReisizeTopRight,
	keStaticCount
};
const CString StaticType[keStaticCount] = 
{  _T("Static"),_T("DragArea"),
_T("WindowTitle"),_T("ResizeBottom"),
_T("ResizeBottomLeft"),_T("ResizeBottomRight"),
_T("ResizeLeft"),_T("ResizeRight"),
_T("ResizeTopLeft"),_T("ReisizeTopRight")
};
//定义动画中用到的方向
enum SkinDirection
{
	keNoDirection = 0,
	keDirectionBottom,
	keDirectionBottomLeft,
	keDirectionBottomRight,
	keDirectionHorizontalIn,
	keDirectionHorizontalOut,
	keDirectionInside,
	keDirectionLeft,
	keDirectionOutSide,
	keDirectionRight,
	keDirectionTop,
	keDirectionTopLeft,
	keDirectionTopRight,
	keDirectionVerticalIn,
	keDirectionVerticalOut,
	keDirectionCount
};
//定义鼠标键的类型
enum SkinMouse
{
	keLeftMouse = 0,
	keRightMouse,
	keMidMouse,
	keMouseCount
};
//定义滚动条的类型
enum SkinScrollType
{
	keHorizontal,
	keVertical,
	keScrollCount
};
//
enum SkinAnchorType
{
	keAnchorTopLeft = 0,
	keAnchorTopRight,
	keAnchorBottomLeft,
	keAnchorBottomRight,
	keAnchorLeft,
	keAnchorRight,
	keAnchorTop,
	keAnchorBottom,
	keAnchorClient,
	keAnchorCenter,
	keAnchorSize
};
const CString AnchorType[keAnchorSize] = 
{  _T("TopLeft"),_T("TopRight"),
_T("BottomLeft"),_T("BottomRight"),
_T("Left"),_T("Right"),
_T("Top"),_T("Bottom"),
_T("Client"),_T("Center")
};
const SkinRect anchorsType[keAnchorSize] = 
{	
	SkinRect(0,0,0,0),
	SkinRect(100,0,100,0),
	SkinRect(0,100,0,100),
	SkinRect(100,100,100,100),
	SkinRect(0,0,0,100),
	SkinRect(100,0,100,100),
	SkinRect(0,0,100,0),
	SkinRect(0,100,100,100),
	SkinRect(0,0,100,100),
	SkinRect(50,50,50,50)
};

enum ButtonState
{
	keNormal = 0,
	kePressed,
	keFocused,
	keDefault,
	keHighlighted,
	keDisabled,
	keButtonStateSize
};
enum CheckBoxState
{
	keCheckNormal = 0,
	keCheckHover,
	keCheckPressed,
	keCheckFocused,
	keCheckDisabled,
	keCheckBoxStateSize,
};
enum RadioState
{
	keRadioNormal = 0,
	keRadioHover,
	keRadioPressed,
	keRadioFocused,
	keRadioDisabled,
	keRadioStateSize
};
enum TextAlign
{
	keAlignTopLeft = 0,
	keAlignTopCenter,
	keAlgnTopRight,
	keAlignCenterLeft,
	keAlignCenter,
	keAlignCenterRight,
	keAlignBottomLeft,
	keAlignBottomCenter,
	keAlignBottomRight,

};

enum WindowState
{
	keActivate =0,
	keInActivate,
	keWinStateSize
};

enum WindowPart
{
	SKINPART_TOP_LEFT = 0, SKINPART_TOP,SKINPART_TOP_BORDER, SKINPART_TOP_RIGHT,
	SKINPART_LEFT,SKINPART_RIGHT,
	SKINPART_BOTTOM_LEFT, SKINPART_BOTTOM, SKINPART_BOTTOM_RIGHT,
	SKINPART_COUNT
};

enum WindowButton
{
	SKINBUTTON_SYSTEM = 0, SKINBUTTON_HELP,
	SKINBUTTON_MIN, SKINBUTTON_MAX, SKINBUTTON_CLOSE,
	SKINBUTTON_COUNT
};
enum WinButtonType
{
	keNone = -1,
	keClose = 0,
	keHelp,
	keMax,
	keMin,
	keRestore,
	keSysMenu,
	keButtonTypeSize
};
const CString winButtonType[keButtonTypeSize] = 
{  _T("Close"),_T("Help"),
_T("Max"),_T("Min"),
_T("Restore"),_T("SysMenu")
};

enum WinButtonState
{
	keWinButtonNormal = 0,
	keWinButtonHover,
	keWinButtonPressed,
	keWinButtonDisabled,
	keWinButtonStateSize
};
enum MouseState
{
	keMouseNormal = 0,
	keMouseDown,
	keMouseUp,
	keMouseSize
};
enum ScroolState
{
	keScrollNormal = 0,
	keScrollHover,
	keScrollPressed,
	keScrollDisabled,
	keScrollState
};
/* Minimum size of the rectangle between the arrows */
#define SCROLL_MIN_RECT  4
/* Minimum size of the thumb in pixels */
#define SCROLL_MIN_THUMB 6
/* Overlap between arrows and thumb */
#define SCROLL_ARROW_THUMB_OVERLAP 0
/* Delay (in ms) before first repetition when holding the button down */
#define SCROLL_FIRST_DELAY   100
/* Delay (in ms) between scroll repetitions */
#define SCROLL_REPEAT_DELAY  50
/* Scroll timer id */
#define SCROLL_TIMER   344
/* Scroll-bar hit testing */
enum SCROLL_HITTEST
{
	SCROLL_NOWHERE,      /* Outside the scroll bar */
	SCROLL_TOP_ARROW,    /* Top or left arrow */
	SCROLL_TOP_RECT,     /* Rectangle between the top arrow and the thumb */
	SCROLL_THUMB,        /* Thumb rectangle */
	SCROLL_BOTTOM_RECT,  /* Rectangle between the thumb and the bottom arrow */
	SCROLL_BOTTOM_ARROW  /* Bottom or right arrow */
};

#endif