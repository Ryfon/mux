﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINBUTTON_H
#define _SKINBUTTON_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"

#define BUTTON_NORMAL			0x00000080L
#define BUTTON_DISABLED			0x00000001L		// 禁用状态
#define BUTTON_PRESSED			0x00000002L		// 按下状态
#define BUTTON_HOVER			0x00000004L		// 高亮状态 (鼠标在该窗口上)
#define BUTTON_FOCUS			0x00000008L		// 具有键盘输入焦点
#define BUTTON_DEFAULT			0x00000010L		// 默认状态 (用于按钮)
#define BUTTON_CHECKED			0x00000020L		// 选中状态 (用于复选框)
#define BUTTON_INDETERMINATE	0x00000040L		// 未确定状态 (用于复选框)

#define BTNTYPE_PUSHBUTTON		0	// 按钮
#define BTNTYPE_CHECKBOX		1	// 复选框
#define BTNTYPE_RADIOBOX		2	// 单选框
#define BTNTYPE_GROUPBOX		3	// 组窗口
#define BTNTYPE_UNKNOWN			-1

#define SKIN_SETSTATE(Data, Mask, bSet)	((bSet) ? (Data |= Mask) : (Data &= ~Mask))

class SKINTKDLL CSkinButton :public CSkinWnd
{
public:
	CSkinButton(void);
	virtual ~CSkinButton(void);
	void InstallSkin(HWND hWnd);
	void InstallSkin(HWND hWnd,CString strSkinName);
	void LoadSkin();

	UINT ButtonStyle2Format(DWORD style);
	void OnMouseMove(UINT nFlags, CPoint point);
	void OnLButtonDown(UINT nFlags, CPoint point);
	void OnLButtonUp(UINT nFlags, CPoint point);
	void OnMouseLeave();
	void OnPaint();
	void OnSetFocus(HWND hWnd);
	void OnKillFocus(HWND hWnd);
	void OnEnable(BOOL bEnable);
	void OnBMSetState(WPARAM wp,LPARAM lp);
	void OnBMSetStyle(WPARAM wp,LPARAM lp);
	void OnBMSetCheck(WPARAM wp,LPARAM lp);

	virtual LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);

	void	DrawButton(CDC *pDC);

	void	DrawPushButton(CDC *pDC);
	void	DrawPushButtonText(CDC *pDC,CRect rtDest);
	void	DrawCheckBoxButton(CDC *pDC);
	void	DrawRadioButton(CDC *pDC);
	void	DrawGroupBox(CDC *pDC);
protected:
	CButtonSkin	*m_pButtonSkin;
	CCheckBoxSkin *m_pCheckSkin;
	CRadioSkin	*m_pRadioSkin;
	CGroupBoxSkin *m_pGroupSkin;
	int	m_nButtonType;
	int m_nButtonState;
	BOOL m_bIcon;
};

#endif
