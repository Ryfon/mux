﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINTOOLBAR_H
#define _SKINTOOLBAR_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"
#define  NORMAL 1
#define  HOVER 2
#define  PRESSED 3
#define  DISABLED 4

class SKINTKDLL CSkinToolBar : public CSkinWnd
{
public:
	CSkinToolBar(void);
	~CSkinToolBar(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();

	virtual void OnPaint();
	virtual void OnNcPaint(HRGN rgn1);
	virtual void OnMouseMove(UINT nFlags, CPoint point);
	virtual void OnLButtonDown(UINT nFlags, CPoint point);
	virtual void OnLButtonUp(UINT nFlags, CPoint point);
	void OnMouseLeave();

	virtual LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);

	UINT HitTest(CPoint point);
	void DrawToolBar(CDC *pDC);
	void DrawItemEntry(CDC* pDC, int nIndex, CRect rcItem, int nState);
public:
	CToolBarSkin	*m_pToolBarSkin;
	UINT m_nHotItem;
	UINT m_nPressedItem;
};

#endif
