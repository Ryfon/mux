﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _RADIOSKIN_H
#define _RADIOSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CRadioSkin :public CObjectSkin
{
	DECLARE_SERIAL(CRadioSkin);
public:
	CRadioSkin(void);
	CRadioSkin(CString strName);
	virtual ~CRadioSkin(void);
	void Serialize(CArchive &ar);

	void DrawBackground(CDC *pDC,CRect rtDest);
	void DrawBox(CDC *pDC,CRect rtDest,int nState,int nCheck);
public:
	SkinLogFont			m_fontRadio;
	int					m_bAutoFont;
	int					m_bTransparent;
	SkinImageSection	m_imageBackground;
	COLORREF			m_colorBackground;
	int					m_nBackMode;
	SkinRadioState		m_stateRadio[keRadioStateSize];
};

#endif