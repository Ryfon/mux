﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINMENUITEM_H
#define _SKINMENUITEM_H

#if _MSC_VER > 1000
#pragma once
#endif

class CSkinMenuBar;
BOOL _InitCommonResources(BOOL bForce = FALSE);

// CMenuItem interface
class CMenuItem
{
	// Construction
public:
	CMenuItem();

	// Attributes
	BYTE GetStyle() const { return m_fsStyle; }
	void ModifyState(BYTE fsRemove, BYTE fsAdd);
	BYTE GetState() const { return m_fsState; }
	CSize GetHorizontalSize() const;
	CRect GetRect() const { return m_rcItem; }
	TCHAR GetAccessKey() const { return m_cAccessKey; }

	// Overidables
	virtual void Update(CDC*) = 0;
	virtual void Layout(CPoint, BOOL bHorz) = 0;
	virtual void TrackPopup(CWnd* pBar, CWnd* pWndSentCmd) = 0;
	virtual void UpdateButtons()=0;
	// Implementation
public:
	virtual ~CMenuItem() { }

public:
	BYTE m_fsStyle;    // menu item style
	BYTE m_fsState;    // menu item state
	CRect m_rcItem;    // current rect
	CSize m_sizeHorz;  // ordinary horizontal size
	TCHAR m_cAccessKey;// access key (Alt key + X)
	
};
// CMenuItem style flags
#define MISTYLE_TRACKABLE		0x01
#define MISTYLE_WRAPPABLE		0x02
// CMenuItem state flags
#define MISTATE_HOT             0x01
#define MISTATE_PRESSED         0x02
#define MISTATE_HORZ            0x04
#define MISTATE_HIDDEN          0x08
#define MISTATE_WRAP            0x10
#define MISTATE_INACTIVE		0x20


class CMenuButton : public CMenuItem  
{
public:
	CMenuButton(HMENU hMenu, int nIndex);

	virtual void Update(CDC*);
	virtual void Layout(CPoint, BOOL bHorz);
	virtual void TrackPopup(CWnd* pBar, CWnd* pWndSentCmd);
	virtual void UpdateButtons();

	virtual ~CMenuButton() { }
	CString m_strBtn;
	void	SetText(TCHAR *szText);
public:
	// string on button
	CPoint  m_ptLineFrom, m_ptLineTo;// vertical line point on button
	HMENU   m_hSubMenu;              // handle to sub menu
	UINT    m_nID;                   // Item ID (if no sub menu)
	CSkinMenuBar* pMenuBar;

	void DrawHot(CDC*);
	void DrawPressed(CDC*);
	void DrawNone(CDC*);
	void DrawHorzText(CDC*, CPoint ptOffset = CPoint(0, 0));
	void DrawVertText(CDC*, CPoint ptOffset = CPoint(0, 0));
	void DrawButton(CDC* pDC,WORD wState);
	// used on constructing
	void InitButtonStringAndSubMenuHandle(HMENU hMenu, int nIndex);
	void InitHorizontalButtonSize();
	void InitAccessKeyAndVerticalLinePoint();
};


// CMenuIcon class
class CMenuIcon : public CMenuItem  
{
	// Construction
public:
	CMenuIcon(CSkinMenuBar* pMenuBar);

	// Operations
	void OnActivateChildWnd();
	// Overidables
	virtual void Update(CDC*);
	virtual void Layout(CPoint, BOOL bHorz);
	virtual void TrackPopup(CWnd* pBar, CWnd* pWndSentCmd);
	virtual void UpdateButtons();
	// Implementation
public:
	virtual ~CMenuIcon();

private:
	CSkinMenuBar* m_pMenuBar;
	HICON m_hDocIcon;
	HICON m_hIconWinLogo;// used on View which has no own Doc icon
};

// CMenuControl class
class CMenuControl : public CMenuItem  
{
	// Construction
public:
	CMenuControl(CWnd* pMenuBar);

	// Operations
	void OnActivateChildWnd();
	void DelayLayoutAndDraw(CDC* pDC, CSize sizeBar,BOOL bFlota);
	void DrawControl();
	// Overidables
	virtual void Update(CDC*);
	virtual void Layout(CPoint, BOOL bHorz);
	virtual void TrackPopup(CWnd* pBar, CWnd* pWndSentCmd) { ASSERT(TRUE); }
	void SetColorButton(COLORREF clrBtn);
	virtual void UpdateButtons();
	
	CImageList m_img;
	virtual ~CMenuControl() { }
private:
	CSkinMenuBar* m_pMenuBar;
	CRect m_arrCaption[3];
	BOOL  m_bDown;
	int	  m_nTracking;
	// Implementation helper

	int HitTest(CPoint point);
	CSize GetCaptionSize();
};





#endif
