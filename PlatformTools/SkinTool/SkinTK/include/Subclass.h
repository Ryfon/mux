﻿#ifndef _SUBCLASSW_H
#define _SUBCLASSW_H

#if _MSC_VER > 1000
#pragma once
#endif

class  SKINTKDLL CSubclassWnd : public CObject {
public:
	DECLARE_DYNAMIC(CSubclassWnd);
	CSubclassWnd();
	virtual ~CSubclassWnd();

	BOOL			GetAutoDestroy(){ return m_bAutoDestroy; }
	void			SetAutoDestroy(BOOL bAutoDestroy){m_bAutoDestroy = bAutoDestroy;}
	virtual BOOL	HookWindow(HWND  hwnd);
	virtual BOOL	HookWindow(CWnd* pWnd)	{ return HookWindow(pWnd->GetSafeHwnd()); }
	virtual BOOL	IsHooked() const		{ return m_hWnd!=NULL; }
	virtual BOOL	IsValidHook() const { return ::IsWindow(m_hWnd); }

	HWND			GetHwnd() const { return m_hWnd; }
	CWnd*			GetCWnd() const { return CWnd::FromHandle(m_hWnd); }

	DWORD			GetExStyle() const { return ::GetWindowLong(m_hWnd, GWL_EXSTYLE); }
	DWORD			GetStyle() const { return ::GetWindowLong(m_hWnd, GWL_STYLE); }
	HWND			GetParent() const { return ::GetParent(m_hWnd); }
	void			GetClientRect(LPRECT pRect) const { ::GetClientRect(m_hWnd, pRect); }
	void			GetWindowRect(LPRECT pRect) const { ::GetWindowRect(m_hWnd, pRect); }
	void			Invalidate(BOOL bErase = TRUE) const { ::InvalidateRect(m_hWnd, NULL, bErase); }
	BOOL			IsWindowEnabled() const { return ::IsWindowEnabled(m_hWnd); }
	BOOL			IsWindowVisible() const { return ::IsWindowVisible(m_hWnd); }

	void			ClientToWindow(LPRECT pRect);
	void			ScreenToClient(LPRECT pRect);
	void			ClientToScreen(LPRECT pRect);
	void			ClientToScreen(LPPOINT pPoint);
	LRESULT			Default(UINT msg,WPARAM wp, LPARAM lp);
	friend LRESULT CALLBACK HookWndProc(HWND, UINT, WPARAM, LPARAM);
	friend class CSubclassWndMap;

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	BOOL				m_bAutoDestroy;		// auto destroy 
	HWND				m_hWnd;				// the window hooked
	WNDPROC				m_pOldWndProc;		// ..and original window proc
	CSubclassWnd*		m_pNext;			// next in chain of hooks for this window

	virtual LRESULT WindowProc(UINT msg, WPARAM wp, LPARAM lp);
	virtual LRESULT Default();
	
};

class CSubclassWndMap : private CMapPtrToPtr {
public:
	CSubclassWndMap();
	~CSubclassWndMap();
	static CSubclassWndMap& GetHookMap();
	void Add(HWND hwnd, CSubclassWnd* pSubclassWnd);
	void Remove(CSubclassWnd* pSubclassWnd);
	void RemoveAll(HWND hwnd);
	CSubclassWnd* Lookup(HWND hwnd);
};

#define	theHookMap	(CSubclassWndMap::GetHookMap())


#endif

