﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINSCROLLBAR_H
#define _SKINSCROLLBAR_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"

class SKINTKDLL CSkinScrollBar :public CSkinWnd
{
public:
	CSkinScrollBar(void);
	virtual ~CSkinScrollBar(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();

	void OnMouseMove(UINT nFlags, CPoint point);
	void OnLButtonDown(UINT nFlags, CPoint point);
	void OnLButtonUp(UINT nFlags, CPoint point);
	void OnLButtonDblClk(UINT nFlags, CPoint point);
	void OnMouseLeave();
	virtual	void OnPaint();

	LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);

	void DrawHScrollBar(CDC *pDC);
	void DrawVScrollBar(CDC *pDC);
};
#endif
