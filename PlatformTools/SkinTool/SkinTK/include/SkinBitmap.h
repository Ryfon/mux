﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 操作位图
*************************************************/
#ifndef _SKINBITMAP_H
#define _SKINBITMAP_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CSkinBitmap : public CBitmap
{
public:
	CSkinBitmap(void);
	virtual ~CSkinBitmap(void);
	//获取位图的宽度
	int	 GetWidth()
	{
		BITMAP bm;
		memset( &bm, 0, sizeof(bm) );
		if(GetBitmap(&bm))
			return bm.bmWidth;
		else 
			return 0;
	}
	//获取位图的高度
	int	 GetHeight()
	{
		BITMAP bm;
		memset( &bm, 0, sizeof(bm) );
		if(GetBitmap(&bm))
			return bm.bmHeight;
		else
			return 0;
	}
	BOOL Attach( HBITMAP hbmp )
	{
		return CBitmap::Attach( hbmp );
	}
	CSkinBitmap& operator=(CSkinBitmap& src)
	{
		Copy(&src);
		return *this;
	}	
	COLORREF GetTransparentColor() const;
	BOOL Copy(CSkinBitmap *pSrc) ;
	void ModifyHue(CDC *pDC,int nWidth,int nHeight,int nPercent);
	void ModifyHue(int nPercent);
	//位图加载函数
	BOOL LoadImage(LPCTSTR szImagePath, COLORREF crBack = 0);
	BOOL LoadImage(UINT uIDRes, LPCTSTR szResourceType, HMODULE hInst = NULL, COLORREF crBack = 0); 
	BOOL LoadImage(LPBYTE lpBuffer,int nSize,COLORREF crBack = 0);
	//save image
	BOOL Save(CFile &file);
	BOOL Load(CFile &file);
	void Serialize(CArchive& ar);
	//位图绘制函数
	BOOL Draw(CDC *pDC, LPRECT lpRectDst,LPRECT lpRectSrc = NULL);
	BOOL TitleDraw(CDC *pDC, LPRECT lpRectDst,LPRECT lpRectSrc = NULL);
	BOOL CenterDraw(CDC *pDC, LPRECT lpRectDst,LPRECT lpRectSrc = NULL);
	BOOL WidthStretchDraw(CDC *pDC, LPRECT lpRectDst,LPRECT lpRectSrc = NULL);
	BOOL HeightStretchDraw(CDC *pDC, LPRECT lpRectDst,LPRECT lpRectSrc = NULL);
	BOOL AllStretchDraw(CDC *pDC, LPRECT lpRectDst,LPRECT lpRectSrc = NULL);
	//使用透明色绘制函数
	BOOL TransparentDraw( CDC *pDC, COLORREF crColour, LPRECT lpRectDst, 
						LPRECT lpRectSrc = NULL, int mode = 0);
	BOOL TitleTransDraw(CDC *pDC, COLORREF crColour, LPRECT lpRectDst, 
						LPRECT lpRectSrc = NULL);
	BOOL CenterTransDraw(CDC *pDC, COLORREF crColour, LPRECT lpRectDst, 
						LPRECT lpRectSrc = NULL);
	BOOL WidthStretchTransDraw(CDC *pDC, COLORREF crColour, LPRECT lpRectDst, 
								LPRECT lpRectSrc = NULL);
	BOOL HeightStretchTransDraw(CDC *pDC, COLORREF crColour, LPRECT lpRectDst, 
								LPRECT lpRectSrc = NULL);
	BOOL AllStretchTransDraw(CDC *pDC, COLORREF crColour, LPRECT lpRectDst, 
							LPRECT lpRectSrc = NULL);


	HRGN CreateRgnFromFile( COLORREF color );
	static BOOL GetResource(LPCTSTR lpName, LPCTSTR lpType, HMODULE hInst, void* pResource, int& nBufSize);
	static IPicture* LoadFromBuffer(BYTE* pBuff, int nSize);
protected:
	BOOL Draw(CDC *pDstDC,CDC *pSrcDC,LPRECT lpRectDst,LPRECT lpRectSrc = NULL);
	BOOL Attach(IPicture* pPicture, COLORREF crBack);
	
public:
	LPBYTE	m_lpBuffer;
	int		m_nSize;
	CString m_strName;
};
#endif