﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/

#ifndef _SKINTK_
#define _SKINTK_

#if _MSC_VER > 1000
#pragma	once
#endif


#ifdef SKINTK_EXPORTS
#define SKINTKDLL __declspec( dllexport  ) 
#else
#define SKINTKDLL  __declspec( dllimport  ) 
#endif
//link SkinTk lib

#if (defined _UNICODE) || (defined UNICODE)
	#ifdef _DEBUG
	#pragma comment(lib, "SkinTKUD.lib")
	#else
	#pragma comment(lib, "SkinTKU.lib")
	#endif
#else
	#ifdef _DEBUG
	#pragma comment(lib, "SkinTKD.lib")
	#else
	#pragma comment(lib, "SkinTK.lib")
	#endif
#endif

#include "..\include\SkinBase.h"
#include "..\include\SkinEnums.h"
#include "..\include\SkinStruct.h"

#include "..\include\SkinTools.h"
#include "..\include\SkinBitmap.h"
#include "..\include\SkinTextType.h"
#include "..\include\MenDC.h"
#include "..\include\SkinImages.h"
#include "..\include\SkinDrawHelper.h"

#include "..\include\Skin.h"
#include "..\include\ObjectSkin.h"
#include "..\include\WindowSkin.h"
#include "..\include\ButtonSkin.h"
#include "..\include\CheckBoxSkin.h"
#include "..\include\RadioSkin.h"
#include "..\include\GroupBoxSkin.h"
#include "..\include\EditSkin.h"
#include "..\include\StaticSkin.h"
#include "..\include\ListBoxSkin.h"
#include "..\include\ComboBoxSkin.h"
#include "..\include\HeaderSkin.h"
#include "..\include\ScrollBarSkin.h"
#include "..\include\PopupMenuSkin.h"
#include "..\include\TabSkin.h"
#include "..\include\StatusBarSkin.h"
#include "..\include\SkinWnd.h"
#include "..\include\SkinButton.h"
#include "..\include\SkinButtonCheck.h"
#include "..\include\SkinButtonGroup.h"
#include "..\include\SkinButtonPush.h"
#include "..\include\SkinButtonRadio.h"
#include "..\include\SkinCombox.h"
#include "..\include\SkinDlg.h"
#include "..\include\SkinEdit.h"
#include "..\include\SkinSpin.h"
#include "..\include\SkinProgress.h"
#include "..\include\SkinHeader.h"
#include "..\include\SkinScrollBar.h"
#include "..\include\SkinListBox.h"
#include "..\include\SkinSlider.h"
#include "..\include\SkinStatusBar.h"
#include "..\include\SkinToolBar.h"
#include "..\include\HeaderSkin.h"
#include "..\include\SpinSkin.h"
#include "..\include\ProgressSkin.h"
#include "..\include\SlideSkin.h"
#include "..\include\SkinTabCtrl.h"
#include "..\include\ToolBarSkin.h"

#include "..\include\SkinMainWnd.h"
#include "..\include\SkinSound.h"
#include "..\include\SkinStatic.h"

#include "..\include\Subclass.h"
#include "..\include\InitSKinTK.h"
#endif