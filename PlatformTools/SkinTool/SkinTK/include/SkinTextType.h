﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINTEXTTYPE_H
#define _SKINTEXTTYPE_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CSkinTextType : public CObject
{
	DECLARE_SERIAL(CSkinTextType);
public:
	CSkinTextType(void);
	virtual ~CSkinTextType(void);
	void Serialize(CArchive &ar);
	COLORREF GetTextColor()
	{
		return m_colorText;
	}
public:
	COLORREF m_colorText;
	COLORREF m_colorShadow;
	SkinSize m_sizeOffset;
};
#endif
