﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 各种皮肤的基类，（各种基本属性位置类型等）
*************************************************/
#ifndef _OBJECTSKIN_H
#define _OBJECTSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CObjectSkin : public CObject
{
	DECLARE_SERIAL(CObjectSkin);
public:
	CObjectSkin(void);
	CObjectSkin(CString &strName,int nType = 0);
	virtual ~CObjectSkin(void);

	virtual void	DrawSkin(CDC *pDC);
	virtual void	Serialize(CArchive &ar);

	BOOL			DrawImageRect(CDC *pDC,CRect rtDest,SkinImageRect &imageRect);
	BOOL			TransDrawImageRect(CDC *pDC,CRect rtDest,SkinImageRect & imageRect,COLORREF crColour);	
	BOOL			DrawImageSection(CDC *pDC,CRect rtDest,SkinImageSection &imageSection);
	BOOL			TransDrawImageSection(CDC *pDC,CRect rtDest,SkinImageSection &imageSection,COLORREF crColour);
	BOOL			DrawImageBorder(CDC *pDC,CRect rtDest,SkinImageSection &imageSection);
	BOOL			TransDrawImageBorder(CDC *pDC,CRect rtDest,SkinImageSection &imageSection,COLORREF crColour);


	void			CalcOffset();
	void			CalcPosition();
	CRect			CalcPosition(CRect rtParent);
	void			AddChild(CObjectSkin *pObjectSkin);
	void			DeleteChild(CString strName);
	int				GetChildrenCount();
	CObjectSkin*	FindChild(CString strName);
	CObjectSkin*	GetAt(int nIndex);

	CObjectSkin*	GetParent();
	void			SetParent(CObjectSkin *pObjectSkin);
	int				GetWidth();
	int				GetHeight();
	void			SetName(const CString strName);
	CString			GetName();
	void			SetTag(const CString strTag);
	CString			GetTag();
	void			SetSkinType(int nSkinType);
	int				GetSkinType();
	void			SetPosition(const SkinRect &rect);
	SkinRect		GetPosition();
	void			SetOffset(const SkinRect &offset);
	SkinRect		GetOffset();
	void			SetAnchors(int anchorType);
	int				GetAnchors();
	void			SetID(int nID);
	int				GetID();
public:
	CString			m_strName;
	int				m_nAnchorType;	//实际位置
	SkinRect		m_rtPosition;
	SkinRect		m_rtOffset;
	SkinRect		m_rtAnchors;
	SkinSize		m_szMin;
	SkinSize		m_szMax;
	int				m_nSkinType;
	CString			m_strTag;
	CObjectSkin*	m_pParent;
	int				m_bAutoPlace;
	int				m_nCtlID;

	CArray<CObjectSkin*,CObjectSkin*> m_arrayChildrens;
};

#endif
