﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _INITSKINTK_H
#define _INITSKINTK_H

#if _MSC_VER > 1000
#pragma once
#endif
#include "..\include\SkinMenu.h"
enum WindowType{
	wtUnknow,
	wtFrameWnd,
	wtDlg,
	wtEdit,
	wtComboBox,
	wtCombolBox,
	wtButton,
	wtStatic,
	wtListBox,
	wtScrollBar,
	wtMenu,
	wtHotKey,
	wtIPAddress,
	wtListCtrl,
	wtTreeCtrl,
	wtTimePick,
	wtUpDown,
	wtTabCtrl,
	wtProgress,
	wtTrackBar,
	wtHeader,
	wttStatusBar,
	wtToolBar,
	wtMDIClient,

	wtFMCView,
	wtFMCMDIFrame,
	wtFMCMDIChild,
	wtFMCMiniDockFrame,
	wtFMCMiniFrame,
	wtFMCFrame,
	wtFMCSplitter,
	wtFMCDialogBar,
	wtFMCControlBar,
	wtFMCWnd,
	wtRichEditCtrl,
};
class CSkinWnd;
class SKINTKDLL CInitSKinTK
{
public:
	CInitSKinTK(void);
	virtual ~CInitSKinTK(void);

	void		InstallSkin(CString strFileName);
	void		ChangeSkin(CString strFileName);
	void		UnInstallSkin();
	void		ModifyHue(int nPercent);
	CSkinWnd*	FindSkinWnd(HWND hWnd);
	void		RemoveWnd(CSkinWnd *pWnd);
	void		RemoveWnd(HWND hWnd);


	static CInitSKinTK& GetInitskin();
	static WindowType	GetWindowType(HWND hWnd);
	static WindowType	GetWindowTypeEx(HWND hWnd);
	static void			HookWindow(HWND hWnd);
protected:
	static BOOL CALLBACK SkinEnumChildProc(HWND hwnd, LPARAM lParam);
	static BOOL CALLBACK SkinEnumThreadWndProc(HWND hwnd, LPARAM lParam);

	static BOOL CALLBACK ModifyEnumChildProc(HWND hwnd, LPARAM lParam);
	static BOOL CALLBACK ModifyEnumThreadWndProc(HWND hwnd, LPARAM lParam);


	static LRESULT CALLBACK SkinHookProc(int iCode, WPARAM wParam, LPARAM lParam);
	static HHOOK m_hPrevSkinHook;

	

	static CArray<CSkinWnd*,CSkinWnd*> m_arrayWnd;
	CSkinMenu m_skinMenu;
};

#endif