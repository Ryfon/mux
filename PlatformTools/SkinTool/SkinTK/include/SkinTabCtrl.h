﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINTABCTRL_H
#define _SKINTABCTRL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"
#define  NORMAL 1
#define  HOVER 2
#define  PRESSED 3
class SKINTKDLL CSkinTabCtrl : public CSkinWnd
{
public:
	CSkinTabCtrl(void);
	~CSkinTabCtrl(void);

	virtual void InstallSkin(HWND hWnd);
	void		 LoadSkin();

	virtual void OnPaint();
	virtual void OnMouseMove(UINT nFlags, CPoint point);
	virtual void OnLButtonDown(UINT nFlags, CPoint point);
	virtual void OnLButtonUp(UINT nFlags, CPoint point);
	void		 OnMouseLeave();

	virtual LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);

	UINT HitTest(CPoint point);
	void DrawTab(CDC *pDC);
	void DrawItemEntry(CDC* pDC, int nIndex, CRect rcItem, int nState);
public:
	CTabSkin	*m_pTabSkin;
	UINT		m_nHotItem;
	UINT		m_nPressedItem;
};

#endif