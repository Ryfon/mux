﻿#ifndef SKINRICHEDITCTRL_H
#define SKINRICHEDITCTRL_H
#include "..\include\SkinListCtrl.h"

class CSkinRichEditCtrl: public CSkinListCtrl
{
public:
	CSkinRichEditCtrl();
	virtual ~CSkinRichEditCtrl();
public:
	virtual void OnNcPaint(HRGN rgn1);
protected:
private:
};
#endif