﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINSPIN_H
#define _SKINSPIN_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"
#define UNKNOW 0
#define UP 1
#define DOWN 2
class SKINTKDLL CSkinSpin : public CSkinWnd
{
public:
	CSkinSpin(void);
	virtual ~CSkinSpin(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();

	virtual void OnPaint();
	virtual BOOL OnEraseBkgnd(CDC *pDC);
	virtual void OnMouseMove(UINT nFlags, CPoint point);
	virtual void OnLButtonDown(UINT nFlags, CPoint point);
	virtual void OnLButtonUp(UINT nFlags, CPoint point);
	void OnMouseLeave();

	virtual LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);

	UINT HitTest(CPoint point);
	void DrawSpin(CDC *pDC);
public:
	CSpinSkin	*m_pSpinSkin;
	BOOL		 m_bVert;

	UINT m_nHotButton;
	UINT m_nPressedButton;
};

#endif