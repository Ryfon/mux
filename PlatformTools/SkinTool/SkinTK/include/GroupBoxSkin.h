﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _GROUPBOXSKIN_H
#define _GROUPBOXSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class CGroupBoxSkin : public CObjectSkin
{
	DECLARE_SERIAL(CGroupBoxSkin);
public:
	CGroupBoxSkin(void);
	CGroupBoxSkin(CString strNmae);
	virtual ~CGroupBoxSkin(void);

	void Serialize(CArchive &ar);

	BOOL DrawFrame(CDC *pDC,CRect rtDest);
	BOOL DrawCaption(CDC *pDC,CRect rtDest);

public:
	SkinLogFont			m_fontGroupBox;
	int					m_bAutoFont;
	SkinImageSection	m_imageFrame;
	SkinImageSection	m_imageCaption;
	COLORREF			m_colorCaption;
	SkinTextType		m_textEffect;
};

#endif
