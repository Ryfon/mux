﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 定义工程中用到的结构数据类型
*************************************************/
#ifndef _SKINBASE_H
#define _SKINBASE_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxtempl.h>
#include <afxcmn.h>

#define MAX_STRINGNAME  128

typedef CPoint	SkinPoint ;
typedef CSize	SkinSize ;
typedef CRect	SkinRect ;
typedef CRect	SkinMargins ;
typedef LOGFONT	SkinLogFont;

#ifndef _COLORREF_DEFINED
#define _COLORREF_DEFINED
typedef DWORD COLORREF;
#endif
#define RGB(r,g,b)          ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))

#define GetRValue(rgb)      ((BYTE)(rgb))
#define GetGValue(rgb)      ((BYTE)(((WORD)(rgb)) >> 8))
#define GetBValue(rgb)      ((BYTE)((rgb)>>16))


typedef DWORD	SKINCOLOR;

#define HAS_ANYFRAME(style,exStyle) \
	(((style) & (WS_THICKFRAME | WS_DLGFRAME | WS_BORDER)) || \
	((exStyle) & WS_EX_DLGMODALFRAME) || \
	!((style) & (WS_CHILD | WS_POPUP)))

#define HAS_THICKFRAME(style,exStyle) \
	(((style) & WS_THICKFRAME) && \
	!(((style) & (WS_DLGFRAME|WS_BORDER)) == WS_DLGFRAME))

#define HAS_MENU(w)  ((((w)->dwStyle & (WS_CHILD | WS_POPUP)) != WS_CHILD) && ((w)->wIDmenu != 0))

#ifndef __MENUITEMINFO_LOCAL_DEFINED
#define __MENUITEMINFO_LOCAL_DEFINED
typedef struct tagMENUITEMINFO_LOCAL
{
	UINT cbSize;
	UINT fMask;
	UINT fType; 
	UINT fState;
	UINT wID; 
	HMENU hSubMenu; 
	HBITMAP hbmpChecked; 
	HBITMAP hbmpUnchecked; 
	DWORD dwItemData; 
	LPTSTR dwTypeData;
	UINT cch; 
} MENUITEMINFO_LOCAL;
#endif


struct CMenuItemInfo : public MENUITEMINFO_LOCAL 
{
	CMenuItemInfo()
	{
		memset(this, 0, sizeof(MENUITEMINFO_LOCAL));
		cbSize = sizeof(MENUITEMINFO_LOCAL);
	}
};

#endif