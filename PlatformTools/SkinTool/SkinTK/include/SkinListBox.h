﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINLISTBOX_H
#define _SKINLISTBOX_H

#if _MSC_VER > 1000
#pragma once
#endif
#include "..\include\SkinWnd.h"

class SKINTKDLL CSkinListBox : public CSkinWnd
{
public:
	CSkinListBox(void);
	virtual ~CSkinListBox(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();
	virtual void OnNcMouseMove( UINT nHitTest, CPoint point );
	virtual void OnMouseMove(UINT nFlags,CPoint point);
	virtual void OnNcLButtonDown( UINT nHitTest, CPoint point );
	virtual void OnNcLButtonDblClk( UINT nHitTest,  CPoint point );
	virtual void OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	virtual UINT OnNcHitTest( CPoint point );
	virtual void OnNcPaint(HRGN rgn1);
	virtual void OnPaint();

	
protected:
	CListBoxSkin *	m_pListBoxSkin;
	BOOL		m_bBorder;
};

#endif