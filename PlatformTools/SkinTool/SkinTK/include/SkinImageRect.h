﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#include "Skin.h"
#pragma once
//////////////////////////////////////////////////
struct SkinImage
{
	char		strImageName[MAX_NAME];
	SkinRect	rtImagePos;
};

struct SkinSection 
{
	char		strImageName[MAX_NAME];
	SkinRect	rtImagePos;
	SkinMargins marginImage;
	int			stretchImage;
};
