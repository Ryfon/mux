﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINPROGRESS_H
#define _SKINPROGRESS_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"

class SKINTKDLL CSkinProgress : public CSkinWnd
{
public:
	CSkinProgress(void);
	~CSkinProgress(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();

	virtual void OnPaint();
	virtual BOOL OnEraseBkgnd(CDC *pDC);

	BOOL	GetPosRect(CRect &rtPos);

public:
	CProgressSkin *m_pProgressSkin;
	BOOL		   m_bVert;
};

#endif