﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINSTATUSBAR_H
#define _SKINSTATUSBAR_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"

class SKINTKDLL CSkinStatusBar : public CSkinWnd
{
public:
	CSkinStatusBar(void);
	~CSkinStatusBar(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();

	void OnPaint();
	BOOL OnEraseBkgnd(CDC *pDC);
	void OnSetText(WPARAM wp,LPARAM lp);

	void DrawStatusBar(CDC *pDC);
protected:
	CStatusBarSkin *m_pStatusBarSkin;
};
#endif
