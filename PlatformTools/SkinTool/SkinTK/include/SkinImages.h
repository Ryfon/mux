﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINIMAGES_H
#define _SKINIMAGES_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CSkinImages : public CObject
{
public:
	DECLARE_SERIAL(CSkinImages);
public:
	CSkinImages(void);
	virtual ~CSkinImages(void);
	
	void Serialize(CArchive& ar);

	BOOL LoadImage(CString in_strImagePath);
	void AddImage(CSkinBitmap &in_image);
	CSkinBitmap* FindImage(CString in_strImageName);
	BOOL DeleteImage(CString in_strImageName);
	BOOL CopyImage(CString in_strImageName);
	BOOL CutImage(CString in_strImageName);
	BOOL PasteImage();
	void ModifyHue(int nPercent);
public:
	CSkinBitmap	*m_pTemp;
	int			m_nImageCount;
	CArray<CSkinBitmap*,CSkinBitmap*> m_arrayImages;
};
#endif
