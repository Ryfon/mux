﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINWND_H
#define _SKINWND_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "SubClass.h"
#include "Skin.h"
#include "WindowSkin.h"
#include "ScrollBarSkin.h"
#include "SkinMenuItem.h"

class SKINTKDLL CSkinWnd :public CSubclassWnd
{
public:
	CSkinWnd(void);
	virtual ~CSkinWnd(void);
	
	virtual void	InstallSkin(HWND hWnd);
	virtual void	InstallSkin(HWND hWnd,CString strSkinName);
	virtual void	LoadSkin();
	//windows非客户区消息处理
	virtual void	OnNcRButtonUp( UINT nHitTest,   CPoint point);
	virtual void	OnNcRButtonDown( UINT nHitTest, CPoint point );
	virtual void	OnNcRButtonDblClk(UINT nHitTest, CPoint point);
	virtual void	OnNcMouseMove( UINT nHitTest, CPoint point );
	virtual void	OnNcLButtonUp( UINT nHitTest, CPoint point );
	virtual void	OnNcLButtonDown( UINT nHitTest, CPoint point );
	virtual void	OnNcLButtonDblClk( UINT nHitTest,  CPoint point );
	virtual UINT	OnNcHitTest( CPoint point );
	virtual BOOL	OnNcActivate( BOOL bActive );
	virtual void	OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	virtual void	OnNcPaint(HRGN rgn1);
	
	//windows客户区消息处理
	virtual LRESULT	OnSetIcon(UINT nFlag,HICON hIcon);
	virtual void	OnTimter(UINT_PTR nIDEvent);
	virtual void	OnSetText(WPARAM wp,LPARAM lp);
	virtual void	OnEnable(BOOL bEnable);
	virtual	void	OnActivate(UINT nState, HWND hWnd, BOOL bMinimized);
	virtual void	OnShowWindow( BOOL bShow, UINT nStatus);
	virtual	void	OnPaint();
	virtual void	OnSize(UINT nType, int cx, int cy );
	virtual BOOL	OnSizing(UINT fwSide, LPRECT pRect);
	virtual BOOL	OnEraseBkgnd(CDC* pDC);
	virtual BOOL	OnSetCursor(HWND hWnd, UINT nHitTest, UINT message);
	virtual void	OnSetFocus(HWND hWnd);
	virtual void	OnKillFocus(HWND hWnd);
	virtual void	OnActivateApp(BOOL bActive, HTASK hTask);
	virtual void	OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	virtual void	OnMouseMove(UINT nFlags, CPoint point);
	virtual void	OnLButtonDown(UINT nFlags, CPoint point);
	virtual void	OnLButtonUp(UINT nFlags, CPoint point);
	virtual void	OnLButtonDblClk(UINT nFlags, CPoint point);
	virtual void	OnVScroll(WPARAM wParam, LPARAM lParam);
	virtual void	OnHScroll(WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnNotify(WPARAM wParam, LPARAM lParam);
	virtual BOOL	OnDrawItem(int nIDCtrl,LPDRAWITEMSTRUCT lpDIS);
	virtual BOOL	OnMeasureItem(LPMEASUREITEMSTRUCT lpDIS);
	virtual void	OnNotify(UINT uNotify, UINT uID, CWnd* pCtrl) ;
	virtual LRESULT OnNotifyReflect(NMHDR* pNMHDR, LRESULT lrParent);
	virtual void	OnNotifyReflect(UINT uNotify, LRESULT lrParent);
	virtual void	OnStyleChanged(int nStyleType,LPSTYLESTRUCT lpStyleStruct);
	virtual void	OnSysCommand(UINT nID, LPARAM lParam );
	virtual void	OnInitMenuPopup(CMenu* pMenu, UINT nIndex, BOOL bSysMenu);
	virtual LRESULT	OnMenuChar(UINT nChar, UINT nFlags, CMenu* pMenu);
	virtual void	OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);
	virtual void	OnCaptureChanged(WPARAM wp,LPARAM lp);
	virtual void	OnDestroy();
	//消息处理函数
	virtual LRESULT WindowProc(UINT msg, WPARAM wp, LPARAM lp);
	virtual LRESULT OnWndMsg(UINT msg, WPARAM wp, LPARAM lp);

	//menubar function
	void			DrawMenuBar(CDC *pDC);
	//scrollbar function
	virtual void	DrawHScrollBar(CDC *pDC);
	virtual void	DrawVScrollBar(CDC *pDC);
	virtual void	TrackScrollBar(int nBar,CPoint pt);
	virtual void	HandleScrollEvent(int nBar,UINT msg,CPoint pt);
	BOOL			InitializeSB(HWND hwnd);
	HRESULT			UninitializeSB(HWND hwnd);
	BOOL			GetScrollRect(int nBar,LPRECT lpRect,int *nArrowSize,int *nThumbSize,int *nThumbPos);
	SCROLL_HITTEST	ScrollHitTest(int nBar,CPoint pt, BOOL bDragging );
	UINT			GetScroolThumbVal( SCROLLINFO *infoPtr, RECT *rect,BOOL vertical, INT pos );
	void			DrawMovingThumb(CDC *pDC,BOOL bVertical,CRect rtDest,int nArrowSize,int nThumbSize);
	BOOL			IsScrollInfoActive(SCROLLINFO *si);
	
	//menu function
	void	DrawMenuText(CDC& dc, CRect rc, CString text, COLORREF color);
	BOOL	Draw3DCheckmark(CDC& dc, const CRect& rc, BOOL bSelected,HBITMAP hbmCheck = NULL);
	void	ConvertMenu(CMenu* pMenu, UINT nIndex, BOOL bSysMenu, BOOL bShowButtons);
	void	PLSelectRect(CDC& dc, const CRect& rc);
	void	DrawSeparator(CRect& rc, CDC& dc);
	void	PLNormalRect(CDC& dc, const CRect& rc);
protected :
	BOOL			m_bHasMenu;
	CPopupMenuSkin	*m_pPopupMenu;
	CPtrList		m_menuList;		// list of HMENU's initialized


	CScrollBarSkin *m_pSkinScrollBar;
	BOOL			m_bEnableSkin;

	SCROLLWND		*m_pScrollWnd;

	BOOL			m_bVScroll;
	BOOL			m_bHScroll;
	BOOL			m_bLeftScroll;

	int				m_nHScrollPos;
	int				m_nVScrollPos;

	BOOL			m_bTrans;
	COLORREF		m_colorTrans;
	DWORD			m_dwStyle;
	DWORD			m_dwExStyle;

	UINT			m_nScrollHitTest;
	BOOL			m_bScrollVertical;
	BOOL			m_bMovingThumb;
	
	int				m_nHArrowLeftState;
	int				m_nHArrowRightState;
	int				m_nHThumbState;
	
	int				m_nVArrowTopState;
	int				m_nVArrowBottomState;
	int				m_nVThumbState;

	HWND			m_hScrollTracking ;
	int				m_nTrackingBar ;
	int				m_nTrackingPos ;
	int				m_nTrackingVal ;

	CRect			m_rtHScroll;
	CRect			m_rtVScroll;
	CRect			m_rtMenu;
};

#endif