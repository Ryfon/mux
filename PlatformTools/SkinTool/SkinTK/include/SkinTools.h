﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINTOOLS_H
#define _SKINTOOLS_H

#if _MSC_VER > 1000
#pragma once
#endif
//定义绘制动画的类型
//无效果
#define		ANIMATE_NONE				0
//活动从上至下
#define		ANIMATE_SLIDE_TTB			1
//滑动从左到右
#define		ANIMATE_SLIDE_LTR			2
//打开从中间到上下
#define		ANIMATE_UNFOLD_MTTB			3
//打开从中间到左右
#define		ANIMATE_UNFOLD_MTLR			4
//百叶窗效果从上到下
#define		ANIMATE_SHUTTER_TTB			5
//百叶窗效果左到右
#define		ANIMATE_SHUTTER_LTR			6
//打开左上到右下
#define		ANIMATE_UNFOLD_LTTRB		7
//滑动打开
#define		ANIMATE_UNFOLDSLIDE_LTTRB	8
//伸展从上到下
#define		ANIMATE_STRETCH_TTB			9
//伸展从左到右
#define		ANIMATE_STRETCH_LTR			10
//雪花效果
#define		ANIMATE_NOISE				11
//淡出效果
#define		ANIMATE_FADE				12
//随机效果
#define		ANIMATE_RAND				100

//窗口客户区位置
class SKINTKDLL CClientRect : public CRect
{
public:
	CClientRect (HWND hWnd)
	{
		::GetClientRect (hWnd, this);
	};
	CClientRect (const CWnd* pWnd)
	{
		::GetClientRect (pWnd->GetSafeHwnd(), this);
	};
};
//窗口位置
class CWindowRect : public CRect
{
public:
	CWindowRect (HWND hWnd)
	{
		::GetWindowRect (hWnd, this);
	};
	CWindowRect (const CWnd* pWnd)
	{
		::GetWindowRect (pWnd->GetSafeHwnd(), this);
	};
};
//窗口文本类
class SKINTKDLL CWindowText : public CString
{
public:
	CWindowText (HWND hWnd)
	{
		CWnd::FromHandle (hWnd)->GetWindowText (*this);
	};
	CWindowText (const CWnd* pWnd)
	{
		pWnd->GetWindowText (*this);
	};
};

/*******************************************************************************
**
**scrollbar fuction
**
**********************************************************************************/
#define CSBS_VISIBLE			8
BOOL WINAPI Skin_IsThumbTracking(HWND hwnd);
SCROLLWND *GetScrollWndFromHwnd(HWND hwnd);
SCROLLBAR *GetScrollBarFromHwnd(HWND hwnd, UINT nBar);
BOOL WINAPI Skin_IsCoolScrollEnabled(HWND hwnd);
static SCROLLINFO *GetScrollInfoFromHwnd(HWND hwnd, int fnBar);
BOOL WINAPI Skin_EnableScrollBar	(HWND hwnd, int wSBflags, UINT wArrows);
BOOL WINAPI Skin_GetScrollBarInfo(HWND hwnd);
BOOL WINAPI Skin_GetScrollInfo (HWND hwnd, int fnBar, LPSCROLLINFO lpsi);
int	WINAPI Skin_GetScrollPos (HWND hwnd, int nBar);
BOOL WINAPI Skin_GetScrollRange (HWND hwnd, int nBar, LPINT lpMinPos, LPINT lpMaxPos);
int	WINAPI Skin_SetScrollInfo (HWND hwnd, int fnBar, LPSCROLLINFO lpsi, BOOL fRedraw);
int WINAPI Skin_SetScrollPos(HWND hwnd, int nBar, int nPos, BOOL fRedraw);
int WINAPI Skin_SetScrollRange (HWND hwnd, int nBar, int nMinPos, int nMaxPos, BOOL fRedraw);
BOOL WINAPI Skin_ShowScrollBar (HWND hwnd, int wBar, BOOL fShow);

BOOL WINAPI SB_InitializeApp(void);
BOOL WINAPI SB_UninitializeApp(void);

#endif