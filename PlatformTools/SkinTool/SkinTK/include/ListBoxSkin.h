﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _LISTBOXSKIN_H
#define _LISTBOXSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif
class SKINTKDLL CListBoxSkin : public CObjectSkin
{
	DECLARE_SERIAL(CListBoxSkin);
public:
	CListBoxSkin(void);
	CListBoxSkin(CString strName);
	virtual ~CListBoxSkin(void);

	void Serialize(CArchive &ar);
public:
	SkinLogFont			m_fontListBox;
	int					m_bAutoFont;
	SkinImageSection	m_imageBorder;
	COLORREF			m_colorBorder;
};
#endif
