﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _POPUPMENUSKIN_H
#define _POPUPMENUSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif


class CPopupMenuSkin : public CObjectSkin
{
	DECLARE_SERIAL(CPopupMenuSkin);
public:
	CPopupMenuSkin(void);
	CPopupMenuSkin(CString strName);
	virtual ~CPopupMenuSkin(void);
	void	Serialize(CArchive &ar);

public:
	SkinImageSection m_imageFrame;
	COLORREF		 m_colorBackground;
	PopupMenuItem	 m_itemPopup;
};

#endif