﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINSLIDER_H
#define _SKINSLIDER_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"
class SKINTKDLL CSkinSlider : public CSkinWnd
{
public:
	CSkinSlider(void);
	~CSkinSlider(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();

	virtual void OnPaint();
	virtual BOOL OnEraseBkgnd(CDC *pDC);
	virtual void OnMouseMove(UINT nFlags, CPoint point);
	virtual void OnLButtonDown(UINT nFlags, CPoint point);
	virtual void OnLButtonUp(UINT nFlags, CPoint point);
	void OnMouseLeave();

	virtual LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);

	void DrawSlider(CDC *pDC);
	UINT HitTest(CPoint point);
public:
	CSliderSkin	*m_pSliderSkin;
	BOOL		 m_bVert;

};
#endif
