﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKIN_H
#define _SKIN_H

#if _MSC_VER > 1000
#pragma once
#endif
#include "SkinImages.h"
#include "ObjectSkin.h"

class CWindowSkin;
class CScrollBarSkin;
class CStaticSkin;
class CButtonSkin;
class CRadioSkin;
class CCheckBoxSkin;
class CGroupBoxSkin;
class CEditSkin;
class CComboBoxSkin;
class CListBoxSkin;
class CPopupMenuSkin;
class CProgressSkin;
class CSpinSkin;
class CHeaderSkin;
class CSliderSkin;
class CTabSkin;
class CStatusBarSkin;
class CToolBarSkin;

class SKINTKDLL CSkin
{
public:
	CSkin(void);
	virtual ~CSkin(void);

	inline CSkinImages* GetImages()
	{
		return m_pImages;
	}
	void			LoadSkin(CString strSkinFile);
	void			SaveSkin(CString strSkinFile);

	BOOL			LoadImage(CString strImageFile);

	// delete Image [5/20/2009 hemeng]
	BOOL			DeleteImage(CString strImageFile);

	CSkinBitmap*	FindIamge(CString strImageFile);
	void			SaveImage(CString strImageFile);
	void			Empty();
	void			ClearSkin();
	
	CObjectSkin*	AddWindowSkin(CString strName);
	CWindowSkin*	GetWindowSkin();

	CObjectSkin*	AddScrollBarSkin(CString strName);
	CScrollBarSkin* GetScrollBarSkin();

	CObjectSkin*	AddStaticSkin(CString strName);
	CStaticSkin*	GetStaticSkin();

	CObjectSkin*	AddButtonSkin(CString strName);
	CButtonSkin*	GetButtonSkin();

	CObjectSkin*	AddRadioSkin(CString strName);
	CRadioSkin*		GetRadioSkin();

	CObjectSkin*	AddCheckBoxSkin(CString strName);
	CCheckBoxSkin*	GetCheckBoxSkin();

	CObjectSkin*	AddGroupBoxSkin(CString strName);
	CGroupBoxSkin*	GetGroupBoxSkin();

	CObjectSkin*	AddEditSkin(CString strName);
	CEditSkin*		GetEditSkin();

	CObjectSkin*	AddComboxSkin(CString strName);
	CComboBoxSkin*	GetComboxSkin();

	CObjectSkin*	AddListBoxSkin(CString strName);
	CListBoxSkin*	GetListBoxSkin();

	CObjectSkin*	AddPopupMenuSkin(CString strName);
	CPopupMenuSkin*	GetPopupMenuSkin();

	CObjectSkin*	AddProgressSkin(CString strName);
	CProgressSkin*	GetProgressSkin();

	CObjectSkin*	AddSpinSkin(CString strName);
	CSpinSkin*		GetSpinSkin();

	CObjectSkin*	AddHeaderSkin(CString strName);
	CHeaderSkin*	GetHeaderSkin();

	CObjectSkin*	AddSliderSkin(CString strName);
	CSliderSkin*	GetSliderSkin();

	CObjectSkin*	AddTabSkin(CString strName);
	CTabSkin*		GetTabSkin();

	CObjectSkin*	AddStatusBarSkin(CString strName);
	CStatusBarSkin*	GetStatusBarSkin();

	CObjectSkin*	AddToolBarSkin(CString strName);
	CToolBarSkin*	GetToolBarSkin();

	CObjectSkin*	FindObjectSkin(CString strName);
	void			DeleteObjectSkin(CString strName);


	void DeleteChild(CString strName);
	int	 GetChildrenCount();

public:
	int					m_nSkinVersion;
	int					m_nNumOfSkin;
	ApplicationSkin		m_Application;
	CArray<CObjectSkin*,CObjectSkin*> m_arraySkinObjects;
	CSkinImages			*m_pImages;
};
SKINTKDLL CSkin&  GetSkin();
SKINTKDLL CSkinImages*  GetImages();
SKINTKDLL CSkinBitmap*   GetImage(CString in_strName);

#endif
