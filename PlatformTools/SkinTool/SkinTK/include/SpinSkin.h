﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SPINSKIN_H
#define _SPINSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif
struct SpinThumb
{
	SkinImageSection imageUp;
	SkinImageSection imageDown;
};
enum SpinState
{
	keSpinNormal = 0,
	keSpinHover,
	keSpinPressed,
	keSpinDisabled,
	keSpinCount,
};
class SKINTKDLL CSpinSkin : public CObjectSkin
{
	DECLARE_SERIAL(CSpinSkin);
public:
	CSpinSkin(void);
	CSpinSkin(CString strName);
	virtual ~CSpinSkin(void);

	void Serialize(CArchive &ar);
public:
	SpinThumb m_thumbHort[keSpinCount];
	SpinThumb m_thumbVert[keSpinCount];
};

#endif