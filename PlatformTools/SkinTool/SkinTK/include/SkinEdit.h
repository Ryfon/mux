﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINEDIT_H
#define _SKINEDIT_H

#if _MSC_VER > 1000
#pragma once
#endif
#include "..\include\SkinWnd.h"


class SKINTKDLL CSkinEdit : public CSkinWnd
{
	
public:
	CSkinEdit(void);
	virtual ~CSkinEdit(void);

	virtual void InstallSkin(HWND hWnd);
	void		 LoadSkin();
	virtual void OnNcMouseMove( UINT nHitTest, CPoint point );
	virtual void OnMouseMove(UINT nFlags,CPoint point);
	virtual void OnNcLButtonDown( UINT nHitTest, CPoint point );
	virtual void OnNcLButtonDblClk( UINT nHitTest,  CPoint point );
	virtual void OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	virtual UINT OnNcHitTest( CPoint point );
	virtual void OnNcPaint(HRGN rgn1);
protected:
	CEditSkin *	m_pEditSkin;
	BOOL		m_bBorder;
	
//	UINT		m_uTextFormat;
};
#endif