﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 窗口皮肤类以及其子控件相关类
*************************************************/
#ifndef _WINDOWSKIN_H
#define _WINDOWSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "ObjectSkin.h"

class SKINTKDLL CWinButtonSkin :public CObjectSkin
{
	DECLARE_SERIAL(CWinButtonSkin);
public:
	CWinButtonSkin();
	CWinButtonSkin(CString strName,int nWinButtonType);
	virtual ~CWinButtonSkin();
	void SetWinButtonType(int nType)
	{
		m_nWinButtonType = nType;
	}
	int	GetWinButtonType()
	{
		return m_nWinButtonType;
	}
	void Serialize(CArchive &ar);
	void DrawButton(CDC *pDC,CRect rtDst,int nStateButton);
public:
	int				m_nWinButtonType;
	SkinImageRect	m_iamgeButton[keWinButtonStateSize];
};

class SKINTKDLL CWinStaticSkin :public CObjectSkin
{
	DECLARE_SERIAL(CWinStaticSkin);
public:
	CWinStaticSkin();
	CWinStaticSkin(CString strName,int nWinButtonType);
	virtual ~CWinStaticSkin();
	void Serialize(CArchive &ar);
public:
};
class SKINTKDLL CWindowSkin :public CObjectSkin
{
	DECLARE_SERIAL(CWindowSkin);
public:
	CWindowSkin(void);
	CWindowSkin(CString strName);
	virtual ~CWindowSkin(void);
	void Serialize(CArchive &ar);
	//WinButton相关操作函数
	void AddWinButton(CWinButtonSkin *pSkinButton);
	BOOL DeleteWinButton(CString strName);
	int	 GetButtonCount();
	CWinButtonSkin* FindButton(CString strName);

	UINT HitTest(CPoint pt);
	void CalcPartsRect(CRect rtWindow);
	void CalcButtonRect(CRect rtWindow);
	void SetMargins(const SkinMargins &margins);
	void SetMargins(int left,int top,int right,int bottom);
	SkinMargins GetMargins();
	
	BOOL DrawFrame(CDC *pDC,CRect rtWindow,BOOL bActive);
	BOOL DrawLeft(CDC *pDC,CRect rtWindow,BOOL bActive);
	BOOL DrawRight(CDC *pDC,CRect rtWindow,BOOL bActive);
	BOOL DrawTop(CDC *pDC,CRect rtWindow,BOOL bActive);
	BOOL DrawBottom(CDC *pDC,CRect rtWindow,BOOL bActive);

	void DrawCaption(CDC *pDC,CRect rtDest,int nStateButton);

	void DrawButton(CDC *pDC,CRect rtParent,int nStateButton);
	void DrawMaxButton(CDC *pDC,int nStateButton);
	void DrawMinButton(CDC *pDC,int nStateButton);
	void DrawCloseButton(CDC *pDC,int nStateButton);
	void DrawRestoreButton(CDC *pDC,int nStateButton);

	void DrawMaxButton(CDC *pDC,CRect rtParent,int nStateButton);
	void DrawMinButton(CDC *pDC,CRect rtParent,int nStateButton);
	void DrawCloseButton(CDC *pDC,CRect rtParent,int nStateButton);
	void DrawRestoreButton(CDC *pDC,CRect rtParent,int nStateButton);

	void DrawBackground(CDC *pDC,CRect rtDest);

	HRGN GetRgn(CDC *pDC,CRect rtDest);
public:
	WindowFrameSkin		m_frameActive;
	WindowFrameSkin		m_frameInactive;
	MenuBarSkin			m_skinMenuBar;
	int					m_bSkinClient;
	SkinImageRect		m_imageBackground;
	COLORREF			m_colorBack;
	SkinMargins			m_rtMargins;
public:
	CRect				m_rectParts[SKINPART_COUNT];
	CRect				m_rtButtons[SKINBUTTON_COUNT];
	CRect				m_rtWindow;
};
class SKINTKDLL CShapeWndSkin :public CObjectSkin
{
	DECLARE_SERIAL(CShapeWndSkin);
public:
	CShapeWndSkin(void);
	CShapeWndSkin(CString strName);
	virtual ~CShapeWndSkin(void);
	void Serialize(CArchive &ar);
	//WinButton相关操作函数
	void AddWinButton(CWinButtonSkin *pSkinButton);
	BOOL DeleteWinButton(CString strName);
	int	 GetButtonCount();
	CWinButtonSkin* FindButton(CString strName);
};

#endif
