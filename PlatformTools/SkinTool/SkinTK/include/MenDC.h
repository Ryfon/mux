﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _MEMDC_H
#define _MEMDC_H

#if _MSC_VER > 1000
#pragma once
#endif

void WINAPI AlphaBlend			( HDC hdcResult, int nX, int nY, int nWidth, int nHeight, HDC hdcDest, int nXDest, int nYDest, HDC hdcSrc, int nXSrc, int nYSrc, BYTE bAlpha );
void WINAPI DrawAnimation		( RECT *prect, HDC hDCSrc, HDC hDCDest, int nType, int nStep, int nTimeDelay );

class SKINTKDLL CMemDC : public CDC
{
protected:
	CBitmap*	m_bitmap;
	CBitmap*	m_oldBitmap;
	CDC*		m_pDC;
	CRect		m_rect;
public:
	CMemDC(CDC* pDC, const CRect& rcBounds) : CDC()
	{
		CreateCompatibleDC(pDC);
		m_bitmap = new CBitmap;
		m_bitmap->CreateCompatibleBitmap(pDC, rcBounds.right, rcBounds.bottom);
		m_oldBitmap = SelectObject(m_bitmap);
		m_pDC = pDC;
		m_rect = rcBounds;
		SetBkMode(TRANSPARENT);
	}
	~CMemDC() 
	{
		m_pDC->BitBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(), 
			this, m_rect.left, m_rect.top, SRCCOPY);
		SelectObject(m_oldBitmap);
		if (m_bitmap != NULL) 
			delete m_bitmap;
	}
};
//===========================================================================
// 功能:
//    辅助类，创建同制定设备相兼容的内存设备。
//===========================================================================
class SKINTKDLL CCompatibleDC : public CDC
{
public:

	CCompatibleDC(CDC* pDC, CBitmap* pBitmap);

	virtual ~CCompatibleDC();

protected:

	CBitmap* m_pBmp;
};

class SKINTKDLL CAnimationMemDC : public CMemDC  
{
public:
	void Animation( int nType, int nStep, int nTimeDelay );
	CAnimationMemDC( CDC *pDC, CRect *prect );
	CAnimationMemDC( HDC hDC, RECT *prect );
	virtual ~CAnimationMemDC();

};
#endif
