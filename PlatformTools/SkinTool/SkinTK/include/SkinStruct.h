﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 定义工程中用到的结构数据类型
*************************************************/
#ifndef _SKINSTRUCT_H
#define _SKINSTRUCT_H

#if _MSC_VER > 1000
#pragma once
#endif

typedef struct SCROLLBAR
{
	UINT		fScrollFlags;		//flags
	BOOL		fScrollVisible;		//if this scrollbar visible?
	SCROLLINFO	scrollInfo;			//positional data (range, position, page size etc)

	int			nArrowLength;		//perpendicular size (height of a horizontal, width of a vertical)
	int			nArrowWidth;		//parallel size (width of horz, height of vert)

	int			nBarType;			//SB_HORZ / SB_VERT
	int			nMinThumbSize;

} SCROLLBAR;

typedef struct SCROLLWND
{
	UINT bars;				//which of the scrollbars do we handle? SB_VERT / SB_HORZ / SB_BOTH
	WNDPROC oldproc;		//old window procedure to call for every message

	SCROLLBAR sbarHorz;		//one scrollbar structure each for 
	SCROLLBAR sbarVert;		//the horizontal and vertical scrollbars

	BOOL fThumbTracking;	// are we currently thumb-tracking??
	BOOL fLeftScrollbar;	// support the WS_EX_LEFTSCROLLBAR style

	HWND hwndToolTip;		// tooltip support!!!

	//size of the window borders
	int cxLeftEdge, cxRightEdge;
	int cyTopEdge,  cyBottomEdge;

	// To prevent calling original WindowProc in response
	// to our own temporary style change (fixes TreeView problem)
	BOOL bPreventStyleChange;

} SCROLLWND;


struct SkinEvent
{
	char strAnimation[MAX_STRINGNAME];
	char strSound[MAX_STRINGNAME];
};
struct SkinImageInfo
{
	int		nWidth;
	int		nHeight;
	int		nPitch;
	LPBYTE	lpImageData;
	CRgn*	pRgn;
};
struct SkinTextType
{
	COLORREF m_colorText;
	COLORREF m_colorShadow;
	SkinSize m_sizeOffset;
};
struct SkinImageRect
{
	SkinImageRect()
	{
		rtImagePos = CRect(0,0,12,12);
		::ZeroMemory(strImageName,MAX_STRINGNAME);
		stretchDrawMode = keNoStretch;
	}
	char		strImageName[MAX_STRINGNAME];
	SkinRect	rtImagePos;
	int			stretchDrawMode;
};

struct SkinImageSection 
{
	SkinImageSection()
	{
		rtImagePos = CRect(0,0,12,12);
		marginImage = CRect(0,0,0,0);
		::ZeroMemory(strImageName,MAX_STRINGNAME);
		stretchDrawMode = keStretchAll;
	}
	char		strImageName[MAX_STRINGNAME];
	SkinRect	rtImagePos;
	SkinMargins marginImage;
	int			stretchDrawMode;
};
struct ScrollBarStateSkin
{
	SkinImageRect		imageArrow1[keScrollState];
	SkinImageRect		imageArrow2[keScrollState];
	SkinImageSection	imageScrollBar;
	SkinImageSection	imageThurmb[keScrollState];
};
struct WindowFrameSkin
{
	SkinImageSection  imageTop;
	SkinImageSection  imageBottom;
	SkinImageSection  imageLeft;
	SkinImageSection  imageRight;
};

struct MenuBarSkin
{
	MenuBarSkin()
	{
		NONCLIENTMETRICS nif;
		nif.cbSize = sizeof(nif);
		SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
		fontMenu = nif.lfCaptionFont;
	}
	SkinImageSection	imageBackground;
	SkinImageSection	imageActive;
	SkinImageSection	imagePressed;
	SkinLogFont			fontMenu;

	COLORREF			colorActive;
	COLORREF			colorPressed;

	COLORREF			colorGrayText;
	COLORREF			colorNormalText;
	COLORREF			colorPressedText;
	COLORREF			colorActiveText;
};

struct PopupMenuItem
{
	SkinImageSection	imageActive;
	SkinImageSection	imageSeparator;

	SkinLogFont			fontMenu;

	COLORREF			textNormal;
	COLORREF			textGrayed;
	COLORREF			textActive;

	COLORREF			colorSeparator;
	COLORREF			colorActive;
	
};	
struct ApplicationSkin
{
	char			strApplication[MAX_STRINGNAME];
	char			strAuthor[MAX_STRINGNAME];
	char			strDescription[MAX_STRINGNAME];
	char			strEmail[MAX_STRINGNAME];
	char			strHomepage[MAX_STRINGNAME];
	BOOL			bUseTrans;
	COLORREF		colorTransparent;
};

struct SkinButtonState
{
	SkinImageSection imageSkin;
	SkinTextType	 textSkin;
};

struct SkinCheckBoxState
{
	SkinImageRect	imageChecked;
	SkinImageRect	imageUnchecked;
	SkinImageRect	imageIndeterminate;
	SkinTextType	textEffect;
};
struct SkinRadioState
{
	SkinImageRect	imageChecked;
	SkinImageRect	imageUnchecked;
	SkinTextType	textEffect;
};
#endif
