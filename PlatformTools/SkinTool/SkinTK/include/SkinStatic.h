﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINSTATIC_H
#define _SKINSTATIC_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"

class SKINTKDLL CSkinStatic :public CSkinWnd
{
public:
	CSkinStatic(void);
	virtual ~CSkinStatic(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();

	void OnPaint();
	BOOL OnEraseBkgnd(CDC *pDC);
	void OnNcPaint(HRGN rgn1);
	void OnSetText(WPARAM wp,LPARAM lp);

	void DrawStatic(CDC *pDC);

protected:
	CStaticSkin *m_pStaticSkin;
	UINT		m_uTextFormat;

};
#endif
