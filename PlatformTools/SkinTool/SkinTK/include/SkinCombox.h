﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINCOMBOX_H
#define _SKINCOMBOX_H

#if _MSC_VER > 1000
#pragma once
#endif
#include "skinwnd.h"

#define COMBOX_NORMAL			0x00000080L		// 正常状态
#define COMBOX_DISABLED			0x00000001L		// 禁用状态
#define COMBOX_PRESSED			0x00000002L		// 按下状态
#define COMBOX_HOVER			0x00000004L		// 高亮状态 (鼠标在该窗口上)
#define COMBOX_FOCUS			0x00000008L		// 具有键盘输入焦点

class CSkinCombolBox;
class SKINTKDLL CSkinCombox :public CSkinWnd
{
public:
	CSkinCombox(void);
	virtual ~CSkinCombox(void);

	virtual void InstallSkin(HWND hWnd);
	void	LoadSkin();
	void	HookCombolBox(HWND hWnd);

	virtual void OnMouseMove(UINT nFlags,CPoint point);
	virtual void OnLButtonDown(UINT nFlags,CPoint point);
	virtual void OnLButtonUp(UINT nFlags, CPoint point);
	virtual void OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	virtual void OnNcPaint(HRGN rgn1);
	virtual void OnPaint();
	virtual void OnSetFocus(HWND hWnd);
	virtual void OnKillFocus(HWND hWnd);
	virtual void OnSetText(WPARAM wp,LPARAM lp);
	void OnMouseLeave();

	virtual LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);
	
	BOOL	HitTestThumb(CPoint point);
	void	DrawComboBox(CDC *pDC);
protected:
	CComboBoxSkin *	m_pComboxSkin;
	int				m_nComboxState;
	BOOL			m_bHistThumb;
	CSkinCombolBox	*m_pSubListBox;
};

#endif
