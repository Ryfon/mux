﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _TABSKIN_H
#define _TABSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class CTabSkin : public CObjectSkin
{
	DECLARE_SERIAL(CTabSkin);
public:
	CTabSkin(void);
	CTabSkin(CString strName);
	virtual ~CTabSkin(void);
	void Serialize(CArchive &ar);
public:
	SkinImageSection	m_imageBackground;
	SkinImageSection	m_imageBorder;
	SkinImageSection	m_imageTabNormal;
	SkinImageSection	m_imageTabHover;
	SkinImageSection	m_imageTabPressed;
};

#endif