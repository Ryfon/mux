﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _LISTVIEWSKIN_H
#define _LISTVIEWSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class CListViewSkin
{
public:
	CListViewSkin(void);
	virtual ~CListViewSkin(void);

	void Serialize(CArchive &ar);
public:
	SkinLogFont			m_fontEdit;
	int					m_bAutoFont;
	SkinImageSection	m_imageBorder;
	COLORREF			m_colorBorder;
};
#endif
