﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _TOOLBARSKIN_H
#define _TOOLBARSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CToolBarSkin : public CObjectSkin
{
	DECLARE_SERIAL(CToolBarSkin);
public:
	CToolBarSkin(void);
	CToolBarSkin(CString strName);
	~CToolBarSkin(void);
	void Serialize(CArchive &ar);
public:
	SkinImageSection	m_imageBackground;
	SkinImageSection	m_imageItemNormal;
	SkinImageSection	m_imageItemHover;
	SkinImageSection	m_imageItemPressed;
	SkinImageSection	m_imageItemDisabled;
};

#endif