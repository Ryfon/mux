﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _BUTTONSKIN_H
#define _BUTTONSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CButtonSkin : public CObjectSkin
{
	DECLARE_SERIAL(CButtonSkin);
public:
	CButtonSkin(void);
	CButtonSkin(CString strName);
	virtual ~CButtonSkin(void);

	void Serialize(CArchive &ar);
	
	void DrawButton(CDC *pDC,CRect rtDest,int nState);
public:
	SkinLogFont		m_fontButton;
	int				m_bAutoFont;
	SkinButtonState	m_stateButton[keButtonStateSize];
};

#endif
