﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _EDITSKIN_H
#define _EDITSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif


class CEditSkin : public CObjectSkin
{
	DECLARE_SERIAL(CEditSkin);
public:
	CEditSkin(void);
	CEditSkin(CString strName);
	virtual ~CEditSkin(void);

	void Serialize(CArchive &ar);


public:
	SkinLogFont			m_fontEdit;
	int					m_bAutoFont;
	SkinImageSection	m_imageBorder;
	COLORREF			m_colorBack;
	SkinTextType		m_textEffect;
};

#endif
