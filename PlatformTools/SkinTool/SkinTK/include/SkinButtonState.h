﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#pragma once

//////////////////////////////////////////////
class SKINTKDLL CSkinScrollBarState : public CObject
{
	DECLARE_SERIAL(CSkinScrollBarState);
public:
	CSkinScrollBarState(void);
	virtual ~CSkinScrollBarState(void);
	void Serialize(CArchive &ar);
public:
	BOOL			m_bArrow1;
	BOOL			m_bArrow2;
	BOOL			m_bScrollBar;
	BOOL			m_bThurmb;
	CSkinImageRect*	m_imageArrow1;
	CSkinImageRect*	m_imageArrow2;
	CSkinImageSection* m_imageScrollBar;
	CSkinImageSection* m_imageThurmb;
};

//////////////////////////////////////////////
class SKINTKDLL CSkinWindowFrame : public CObject
{
	DECLARE_SERIAL(CSkinWindowFrame);
public:
	CSkinWindowFrame(void);
	virtual ~CSkinWindowFrame(void);
	void Serialize(CArchive &ar);
	inline bool DrawTop(CDC *pDC,SkinRect rectDst);
	inline bool DrawBottom(CDC *pDC,SkinRect rectDst);
	inline bool DrawLeft(CDC *pDC,SkinRect rectDst);
	inline bool DrawRight(CDC *pDC,SkinRect rectDst);
public:
	CSkinImageSection  m_imageTop;
	CSkinImageSection  m_imageBottom;
	CSkinImageSection  m_imageLeft;
	CSkinImageSection  m_imageRight;
};

//////////////////////////////////////////////
class SKINTKDLL CSkinMenuBar : public CObject
{
	DECLARE_SERIAL(CSkinMenuBar);
public:
	CSkinMenuBar(void);
	virtual ~CSkinMenuBar(void);
	void Serialize(CArchive &ar);
public:
	CSkinImageSection m_imageNormal;
	CSkinImageSection m_imageActive;
	CSkinImageSection m_imagePressed;
	SkinLogFont m_fontMenu;

	CSkinTextType m_textNormal;
	CSkinTextType m_textGrayed;
	CSkinTextType m_textActive;
	CSkinTextType m_textPressed;

	COLORREF	m_colorActive;
	COLORREF	m_colorPressed;
};

//////////////////////////////////////////////
class SKINTKDLL CSkinPopupMenu : public CObject
{
	DECLARE_SERIAL(CSkinPopupMenu);
public:
	CSkinPopupMenu(void);
	virtual ~CSkinPopupMenu(void);
	void Serialize(CArchive &ar);
public:
	CSkinImageSection m_imageFrame;
	CSkinImageSection m_imageSeparator;

	SkinLogFont m_fontMenu;

	CSkinTextType m_textNormal;
	CSkinTextType m_textGrayed;
	CSkinTextType m_textActive;

	COLORREF	m_colorSeparator;
	COLORREF	m_colorActive;
	CSkinImageSection m_imageActive;
};