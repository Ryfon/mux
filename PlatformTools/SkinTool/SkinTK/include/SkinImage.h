﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 用于对DIB位图的处理
*************************************************/
#pragma once

class SKINTKDLL CSkinImage
{
public:
	CSkinImage(void);
	virtual ~CSkinImage(void);

	CSkinImage& operator=(const CSkinImage& src)
	{
		src.CopyTo(this);
		return *this;
	}
	void DeleteObject()
	{
		ATLASSERT(!IsEmpty());
		free(m_hDib);
		m_hDib = NULL;
	}
	BOOL IsEmpty() const
	{
		return m_hDib==NULL;
	}
	DWORD GetByteSize() const
	{
		return m_bi.biSize + m_bi.biSizeImage + GetPaletteSize();
	}
	inline LPBYTE GetBits() const
	{
		ATLASSERT(!IsEmpty());
		return( (BYTE*) m_hDib + ( *(LPDWORD) m_hDib ) + GetPaletteSize() ); 
	}
	inline DWORD GetWidth() const
	{
		ATLASSERT(!IsEmpty());
		return m_bi.biWidth;
	}
	inline DWORD GetHeight() const
	{
		ATLASSERT(!IsEmpty());
		return m_bi.biHeight;
	}
	inline DWORD GetLineWidth() const
	{
		ATLASSERT(!IsEmpty());
		return m_dwLineWidth;
	}
	inline DWORD GetPixelWidth() const
	{
		ATLASSERT(!IsEmpty());
		return m_dwPixelWidth;
	}
	WORD GetColorCount() const
	{
		return m_nColors;
	}
	WORD GetPaletteSize() const
	{
		WORD wNumColors = GetColorCount();
		if( m_bi.biSize==sizeof(BITMAPCOREHEADER) )
			return (WORD) (wNumColors * sizeof(RGBTRIPLE));
		else
			return (WORD) (wNumColors * sizeof(RGBQUAD));
	}
	WORD GetBitCount() const
	{
		ATLASSERT(!IsEmpty());
		return m_bi.biBitCount;
	}

	COLORREF GetPixel(int x, int y) const
	{
		ATLASSERT(!IsEmpty());
		if( x<0 || x>=m_bi.biWidth || y<0 || y>=m_bi.biHeight ) return RGB(0,0,0);
		LPBYTE lp = GetBits() + (y * m_dwLineWidth) + (x * m_dwPixelWidth);
		return RGB(lp[0], lp[1], lp[2]);
	}
	void SetPixel(int x, int y, COLORREF rgb)
	{
		ATLASSERT(!IsEmpty());
		if( x<0 || x>=m_bi.biWidth || y<0 || y>=m_bi.biHeight ) return;
		LPBYTE lp = GetBits() + (y * m_dwLineWidth) + (x * m_dwPixelWidth);
		*lp++ = GetRValue(rgb);
		*lp++ = GetGValue(rgb);
		*lp++ = GetBValue(rgb);
	}

	BOOL Draw(HDC hDC, long xoffset, long yoffset) const
	{
		if( (m_hDib==NULL) || (hDC==NULL) ) return FALSE;
			(hDC,COLORONCOLOR);   
		::SetDIBitsToDevice(hDC, xoffset, yoffset,
			m_bi.biWidth, m_bi.biHeight, 0, 0, 
			0, m_bi.biHeight, GetBits(),
			(BITMAPINFO*) m_hDib, DIB_RGB_COLORS);
		return TRUE;
	}
	BOOL Stretch(HDC hDC, long xoffset, long yoffset, long xsize, long ysize) const
	{
		if( (m_hDib==NULL) || (hDC==NULL) ) return FALSE;
		::SetStretchBltMode(hDC,COLORONCOLOR);   
		::StretchDIBits(hDC, xoffset, yoffset,
			xsize, ysize, 0, 0, m_bi.biWidth, m_bi.biHeight,
			GetBits(), (BITMAPINFO*) m_hDib, DIB_RGB_COLORS, SRCCOPY);
		return TRUE;
	}
	BOOL Stretch(HDC hDC, RECT rc) const
	{
		return Stretch(hDC, rc.left, rc.top, rc.right-rc.left, rc.bottom-rc.top);
	}
	BOOL CopyTo(CSkinImage *pDst) const;
	BOOL LoadBitmap(LPCTSTR lpszPathName);
	BOOL LoadBitmap(UINT nRes);
	BOOL Create(HBITMAP hBitmap);
	BOOL Create(DWORD dwWidth, DWORD dwHeight, WORD wBitCount);
public:
	LPVOID m_hDib;
	BITMAPINFOHEADER m_bi;
	DWORD m_dwLineWidth;
	DWORD m_dwPixelWidth;
	WORD m_nColors;
};
