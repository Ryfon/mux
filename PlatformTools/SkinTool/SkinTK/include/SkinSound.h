﻿#pragma once

class CSkinSound
{
public:
	CSkinSound(void);
	virtual ~CSkinSound(void);

	void	SetSoundName(CString &strSoundName)
	{
		m_strSoundName = strSoundName;
	}
	CString	GetSoundName()
	{
		return m_strSoundName;
	}
	void	EnableSound(bool bEnablePlay)
	{
		m_bEnablePlay = bEnablePlay;
	}
protected:
	CString m_strSoundName;
	BOOL	m_bEnablePlay;
};
