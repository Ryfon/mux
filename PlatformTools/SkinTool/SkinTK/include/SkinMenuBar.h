﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINMENUBAR_H
#define _SKINMENUBAR_H

#if _MSC_VER > 1000
#pragma once
#endif
class CWindowSkin;
class CSkinMenuItem;
class CSkinMenuBar;
class CMenuItem;

//menu bar use window coordinate
class CSkinMenuBar
{
public:
	friend class CMDIMenuMgr;
	CSkinMenuBar(void);
	virtual ~CSkinMenuBar(void);

	enum TRACKINGSTATE {							// menubar has three states:
		TRACK_NONE = 0,								// normal, not tracking anything
		TRACK_BUTTON,								// tracking buttons (F10/Alt mode)
		TRACK_POPUP									// tracking popups
	};

	BOOL			InitItems();
	void			DeleteItems();
	int				GetItemCount() const { return (int)m_arrItem.GetSize(); }
	BOOL			IsValidIndex(int nIndex) const { return 0 <= nIndex && nIndex < GetItemCount(); }
	int				HitTestOnTrack(CPoint point);
	void			SetMenu(HMENU hMenu,HWND hWnd);
	TRACKINGSTATE	GetTrackingState(int& iPopup) {
		iPopup = m_iPopupTracking; return m_iTrackingState;}
	void			CalcMenuBarPos();
	void			TrackPopup(int iButton);
	int				GetNextOrPrevButton(int iButton, BOOL bPrev);
	void			SetTrackingState(TRACKINGSTATE iState, int iButton=-1);
	void			ToggleTrackButtonMode();
	void			CancelMenuAndTrackNewOne(int iButton);
	virtual BOOL	OnMenuInput(MSG& m);	 // handle popup menu input
	CMenuItem*		GetIndex(int nIndex) const;
	void			OnLButtonDown(UINT nFlags, CPoint point);
	void			OnMouseMove(UINT nFlags, CPoint point);
	void			DrawMenuBar();
	static LRESULT CALLBACK MenuInputFilter(int code, WPARAM wp, LPARAM lp);
	// Attributes
	void OnSetMenu(HMENU hNewMenu, HMENU hWindowMenu);
public:
	CWindowSkin		*m_pSkinWindow;
	CArray<CMenuItem*, CMenuItem*> m_arrItem;
	HWND			m_hWnd;
	CRect			m_rtPosition;
	BOOL			m_bMDI;

	CMenuItem*		m_pPressed;
	CMenuItem*		m_pHot;
	HMENU			m_hMenu;
	// menu tracking stuff:
	int				m_iPopupTracking;				 // which popup I'm tracking if any
	int				m_iNewPopup;						 // next menu to track
	BOOL			m_bProcessRightArrow;			 // process l/r arrow keys?
	BOOL			m_bProcessLeftArrow;			 // ...
	BOOL			m_bEscapeWasPressed;			 // user pressed escape to exit menu
	CPoint			m_ptMouse;							 // mouse location when tracking popup
	HMENU			m_hMenuTracking;					 // current popup I'm tracking
	TRACKINGSTATE	m_iTrackingState;		 // current tracking state

};
#endif
