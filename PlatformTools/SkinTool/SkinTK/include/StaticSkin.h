﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _STATICSKIN_H
#define _STATICSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif


class CStaticSkin : public CObjectSkin
{
	DECLARE_SERIAL(CStaticSkin);
public:
	CStaticSkin(void);
	CStaticSkin(CString strName);
	virtual ~CStaticSkin(void);
	void Serialize(CArchive &ar);

public:
	SkinLogFont m_fontStatic;
	int			m_bAutoFont;
	int			m_bTransparent;
	int			m_nBackMode;
	SkinImageSection m_imageBack;
	COLORREF		 m_colorBack;
	SkinTextType m_textEffect;
};

#endif