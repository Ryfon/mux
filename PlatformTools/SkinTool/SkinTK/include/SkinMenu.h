﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINIMENU_H
#define _SKINIMENU_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "..\include\SkinWnd.h"

class CPopupMenuSkin;
class CSkinMenu 
{
public:
	CSkinMenu(void);
	~CSkinMenu(void);
	// Border Hook
	void			EnableHook();
	static void		EnableHook(BOOL bEnable);
	static void		RegisterEdge(int nLeft, int nTop, int nLength);
	static BOOL		IsModernVersion();
protected:
	static LRESULT	CALLBACK MsgHook(int nCode, WPARAM wParam, LPARAM lParam);
	static LRESULT	CALLBACK MenuProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
protected:
	static HHOOK	m_hMsgHook;
	static LPCTSTR	wpnOldProc;
	static BOOL		m_bPrinted;
	static int		m_nEdgeLeft;
	static int		m_nEdgeTop;
	static int		m_nEdgeSize;

	BOOL		m_bEnable;
	BOOL		m_bUnhook;

	static CPopupMenuSkin *m_pPopupMenuSkin;

};

#endif
