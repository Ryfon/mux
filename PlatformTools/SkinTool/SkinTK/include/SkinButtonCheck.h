﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _SKINBUTTONCHECK_H
#define _SKINBUTTONCHECK_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinbutton.h"

class SKINTKDLL CSkinButtonCheck :
	public CSkinButton
{
public:
	CSkinButtonCheck(void);
	virtual ~CSkinButtonCheck(void);
};

#endif