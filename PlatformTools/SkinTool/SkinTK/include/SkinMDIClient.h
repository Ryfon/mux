﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 操作位图
*************************************************/
#ifndef _SKINMDICLIENT_H
#define _SKINMDICLIENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "..\include\SkinMainWnd.h"

class CSkinMDIClient :public CSkinFrame
{
public:
	CSkinMDIClient(void);
	~CSkinMDIClient(void);

	virtual LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);
};

#endif