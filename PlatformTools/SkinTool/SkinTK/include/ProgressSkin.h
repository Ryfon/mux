﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _PROGRESSSKIN_H
#define _PROGRESSSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CProgressSkin : public CObjectSkin
{
	DECLARE_SERIAL(CProgressSkin);
public:
	CProgressSkin(void);
	CProgressSkin(CString strName);
	~CProgressSkin(void);

	void Serialize(CArchive &ar);
public:
	SkinImageSection	m_imageBackground;
	COLORREF			m_colorBackground;
	SkinImageSection	m_imageProgress;
	COLORREF			m_colorProgress;
};
#endif
