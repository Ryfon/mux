﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 操作位图
*************************************************/
#ifndef _SKINFRAME_H
#define _SKINFRAME_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "skinwnd.h"
#include "Skin.h"
#include "WindowSkin.h"
#include "SkinMenuBar.h"
#include "SkinMenu.h"

class  SKINTKDLL CSkinFrame :public CSkinWnd
{
public:
	CSkinFrame(void);
	virtual ~CSkinFrame(void);
	virtual void	InstallSkin(HWND hWnd);
	virtual void	InstallSkin(HWND hWnd,CString strSkinName);
	virtual void	OnNcMouseMove( UINT nHitTest, CPoint point );
	virtual void	OnMouseMove(UINT nFlags,CPoint point);
	virtual void	OnNcLButtonUp( UINT nHitTest, CPoint point );
	virtual void	OnNcLButtonDown( UINT nHitTest, CPoint point );
	virtual void	OnNcLButtonDblClk( UINT nHitTest,  CPoint point );
	virtual UINT	OnNcHitTest( CPoint point );
	virtual BOOL	OnNcActivate( BOOL bActive );
	virtual void	OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	virtual void	OnNcPaint(HRGN rgn1);
	virtual void	OnSysCommand(UINT nID,LPARAM lParem);
	virtual BOOL	OnEraseBkgnd(CDC *pDC);
	virtual void	OnSize(UINT nType,int cx,int cy);
	virtual void	OnSetText(WPARAM wp,LPARAM lp);
	virtual LRESULT OnSetIcon(UINT nFlag,HICON hIcon);
	virtual void	OnEnable(BOOL bEnable);
	virtual void	OnTimter(UINT_PTR nIDEvent);

	virtual LRESULT OnWndMsg(UINT msg,WPARAM wp,LPARAM lp);

	void	HandleMenuBarEvent(UINT msg,CPoint pt);
	void	TrackMenuBar(CPoint point);
	void	DrawFrame(CDC *pDC);
	void	DrawCaption(CDC *pDC);
	void	DrawWindowButton(CDC *pDC);
	void	DrawScrollBar(CDC *pDC,int nBar,BOOL arrors);
	void	DrawMenuBar(CDC *pDC);
	CSkinMenuBar m_menuBar;
protected:
	CWindowSkin	*m_pSkinWindow;

	BOOL	m_bActive;
	BOOL	m_bSizable;

	BOOL	m_bSysMenu;
	BOOL	m_bTitle;
	BOOL	m_bBorder;
	BOOL	m_bCloseBtn;
	BOOL	m_bMaxBtn;
	BOOL	m_bMinBtn;
	BOOL	m_bHelpBtn;
	BOOL	m_bMenu;
	BOOL	m_bMDI;
	BOOL	m_bInMenu;

	int		m_nCloseBtnState;
	int		m_nMaxBtnState;
	int		m_nMinBtnState;
	int		m_nHelpBtnState;
	int		m_nNcMouseState;
	int		m_nSelBtnType;

};
#endif
