﻿/***********************************************
*	工作室 : 天光工作室
*	作者   : 张东斌	
*	用途   : 
*************************************************/
#ifndef _CHECKBOXSKIN_H
#define _CHECKBOXSKIN_H

#if _MSC_VER > 1000
#pragma once
#endif

class SKINTKDLL CCheckBoxSkin : public CObjectSkin
{
	DECLARE_SERIAL(CCheckBoxSkin);
public:
	CCheckBoxSkin(void);
	CCheckBoxSkin(CString strName);
	virtual ~CCheckBoxSkin(void);

	void Serialize(CArchive &ar);

	void DrawBackground(CDC *pDC,CRect rtDest);
	void DrawBox(CDC *pDC,CRect rtDest,int nState,int nCheck);
public:
	SkinLogFont			m_fontCheckBox;
	int					m_bAutoFont;
	SkinImageSection	m_imageBackground;
	COLORREF			m_colorBackground;
	int					m_nBackMode;
	SkinCheckBoxState	m_stateCheckBox[keCheckBoxStateSize];
};


#endif