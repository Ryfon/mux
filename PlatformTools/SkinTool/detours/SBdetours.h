﻿#include <windows.h>
#ifndef COOLSB_DETOURS_INCLUDED
#define COOLSB_DETOURS_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

	//
//	SCROLLBAR datatype. There are two of these structures per window
//
typedef struct 
{
	UINT		fScrollFlags;		//flags
	BOOL		fScrollVisible;		//if this scrollbar visible?
	SCROLLINFO	scrollInfo;			//positional data (range, position, page size etc)

	int			nArrowLength;		//perpendicular size (height of a horizontal, width of a vertical)
	int			nArrowWidth;		//parallel size (width of horz, height of vert)

	int			nBarType;			//SB_HORZ / SB_VERT
	int			nMinThumbSize;

} SCROLLBAR;

//
//	Container structure for a cool scrollbar window.
//
typedef struct
{
	UINT bars;				//which of the scrollbars do we handle? SB_VERT / SB_HORZ / SB_BOTH
	WNDPROC oldproc;		//old window procedure to call for every message

	SCROLLBAR sbarHorz;		//one scrollbar structure each for 
	SCROLLBAR sbarVert;		//the horizontal and vertical scrollbars

	BOOL fThumbTracking;	// are we currently thumb-tracking??
	BOOL fLeftScrollbar;	// support the WS_EX_LEFTSCROLLBAR style

	HWND hwndToolTip;		// tooltip support!!!

	//size of the window borders
	int cxLeftEdge, cxRightEdge;
	int cyTopEdge,  cyBottomEdge;

	// To prevent calling original WindowProc in response
	// to our own temporary style change (fixes TreeView problem)
	BOOL bPreventStyleChange;

} SCROLLWND;

#define  CSBS_VISIBLE 0
BOOL WINAPI CoolSB_InitializeApp(void);
BOOL WINAPI CoolSB_UninitializeApp(void);

#ifdef __cplusplus
}
#endif

#endif