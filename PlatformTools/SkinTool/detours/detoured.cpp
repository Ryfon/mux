﻿#include <windows.h>
#include "detoured.h"

static HMODULE s_hDll;

HMODULE WINAPI Detoured()
{
    return s_hDll;
}

//BOOL WINAPI DllMain(HINSTANCE hinst, DWORD dwReason, LPVOID reserved)
//{
//    (void)reserved;
//
//    if (dwReason == DLL_PROCESS_ATTACH) {
//        s_hDll = hinst;
//        DisableThreadLibraryCalls(hinst);
//    }
//    return TRUE;
//}