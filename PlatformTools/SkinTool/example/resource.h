﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Sample1.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SAMPLE1_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDC_BUTTON1                     1000
#define IDC_CHECK4                      1005
#define IDC_CHECK5                      1006
#define IDC_CHECK6                      1007
#define IDC_RADIO1                      1008
#define IDC_RADIO2                      1009
#define IDC_COMBO1                      1010
#define IDC_EDIT1                       1011
#define IDC_CUSTOM1                     1012
#define IDC_SCROLLBAR1                  1013
#define IDC_SCROLLBAR2                  1014
#define IDC_LIST1                       1015
#define IDC_LIST2                       1016
#define IDC_TREE1                       1017
#define IDC_PROGRESS1                   1018
#define IDC_SPIN1                       1021
#define IDC_TAB1                        1022
#define IDC_SLIDER1                     1023
#define ID_FILE_NEW32771                32771
#define ID_FILE_OPENT                   32772
#define ID_EDIT_COPY32773               32773
#define ID_EDIT_PASTE32774              32774
#define ID_HELP32775                    32775
#define ID_HELP_ABOUT                   32776
#define ID_EDIT_CUT32777                32777
#define ID_EDIT_DELETE                  32778
#define ID_FILE_SAVE32779               32779
#define ID_FILE_SAVEAS                  32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
