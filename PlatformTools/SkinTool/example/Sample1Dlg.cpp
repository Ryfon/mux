﻿// Sample1Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Sample1.h"
#include "Sample1Dlg.h"
#include ".\sample1dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CSample1Dlg 对话框



CSample1Dlg::CSample1Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample1Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON1, m_button1);
	DDX_Control(pDX, IDC_LIST2, m_list2);
	DDX_Control(pDX, IDC_COMBO1, m_comboBox);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
	DDX_Control(pDX, IDC_TAB1, m_tab);
}

BEGIN_MESSAGE_MAP(CSample1Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_WM_HSCROLL()
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CSample1Dlg 消息处理程序

BOOL CSample1Dlg::OnInitDialog()
{
	

	CDialog::OnInitDialog();
	
	SCROLLINFO info;
	info.cbSize = sizeof(SCROLLINFO);     
	info.fMask = SIF_ALL;     
	info.nMin = 0;     
	info.nMax = 100; 
	info.nPage = 5;     
	info.nPos = 5;    
	//info.nTrackPos = 2; 
	SetScrollInfo(SB_HORZ,&info);

	// 将\“关于...\”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
//	m_skinButton.HookWindow(m_button1.GetSafeHwnd());
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));
	m_list2.InsertItem(0,_T("dfdfsdf"));

	m_comboBox.AddString(_T("Item0"));
	m_comboBox.AddString(_T("Item1"));
	m_comboBox.AddString(_T("Item2"));
	m_comboBox.AddString(_T("Item3"));
	m_comboBox.AddString(_T("Item4"));
	m_comboBox.AddString(_T("Item5"));
	m_comboBox.AddString(_T("Item6"));
	m_comboBox.AddString(_T("Item7"));
	m_comboBox.AddString(_T("Item8"));
	m_comboBox.AddString(_T("Item9"));
	m_comboBox.AddString(_T("Item10"));

	m_progress.SetRange(0,100);
	m_progress.SetPos(23);

	m_tab.InsertItem(0,_T("Item0"));
	m_tab.InsertItem(1,_T("Item1"));
	m_tab.InsertItem(2,_T("Item2"));
	return TRUE;  // 除非设置了控件的焦点，否则返回 TRUE
}

void CSample1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CSample1Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
	
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
HCURSOR CSample1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSample1Dlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CSample1Dlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	static int a = 0;
	SCROLLINFO info;
	info.cbSize = sizeof(SCROLLINFO);     
	info.fMask = SIF_POS;     
	GetScrollInfo(SB_HORZ,&info);
	int nDelta  ;

	switch (nSBCode) {

	case SB_LINELEFT:
		nDelta = -1;
		break;

	case SB_PAGELEFT:
		nDelta = -5;
		break;

	case SB_THUMBTRACK:
		nDelta = (int) nPos - info.nPos;
		break;

	case SB_PAGERIGHT:
		nDelta = 5;
		break;

	case SB_LINERIGHT:
		nDelta = 1;
		break;
	default: // Ignore other scroll bar messages
		return;
	}
	info.nPos += nDelta;
	SetScrollInfo(SB_HORZ,&info,TRUE);
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

int CSample1Dlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	return 0;
}

BOOL CSample1Dlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::PreCreateWindow(cs);
}

void CSample1Dlg::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	CDialog::OnOK();
}
