﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDITEM_H__)
#define __SPPROPERTYGRIDITEM_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// class forwards.

class CSPPropertyGridView;
class CSPPropertyGridItem;

//-----------------------------------------------------------------------
// Summary:
//     This constant is used to determine the "gutter" width of the property
//     grid.  This is the area on the left of the grid that the
//     expand buttons are drawn in.
// Remarks:
//     This is useful when drawing owner drawn controls such as buttons
//     and drop-down menus.
// Example:
//     The following example illustrates using SP_PGI_EXPAND_BORDER:
// <code>
// void CDelphiGridPage::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
// {
//     // code snip ...
//
//     CRect rcText(rc);
//     rcText.left = max(1, pItem->GetIndent()) * SP_PGI_EXPAND_BORDER + 3;
//     rcText.right = rcCaption.right - 1;
//     rcText.bottom -= 1;
//     dc.DrawText( pItem->GetCaption(), rcText, DT_SINGLELINE|DT_VCENTER);
// }
// </code>
//-----------------------------------------------------------------------
const int SP_PGI_EXPAND_BORDER = 14;

//-----------------------------------------------------------------------
// Summary:
//     Flags that indicate what styles to set for the grid item.
// Example:
//     <code>pItem->SetFlags(SPGridItemHasEdit|SPGridItemHasComboButton);</code>
// See Also: CSPPropertyGridItem::SetFlags, OnInplaceButtonDown
//
// <KEYWORDS SPGridItemHasEdit, SPGridItemHasExpandButton, SPGridItemHasComboButton>
//-----------------------------------------------------------------------
enum SPPropertyGridItemFlags
{
	SPGridItemHasEdit         = 1, // Item has an edit control.
	SPGridItemHasExpandButton = 2, // Item has an expand button.
	SPGridItemHasComboButton  = 4  // Item has a combo button.
};

//===========================================================================
// Summary:
//     CSPPropertyGridItems is a CList derived class. It represents the items
//     collection with some internal functionality.
//===========================================================================
class  CSPPropertyGridItems : public CCmdTarget
{

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member function to find an item.
	// Parameters:
	//     strCaption - Caption for the item to find.
	//     nID - Identifier for the item to find.
	// Returns:
	//     A pointer to a CSPPropertyGridItem object.
	//-----------------------------------------------------------------------
	CSPPropertyGridItem* FindItem(UINT nID);
	CSPPropertyGridItem* FindItem(CString strCaption); // <COMBINE CSPPropertyGridItems::FindItem@UINT>

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member to determine the index of a CSPPropertyGridItem.
	// Parameters:
	//     pItem - Pointer to the CSPPropertyGridItem you want to know
	//             the index of.
	// Returns:
	//     The index of the item within the collection of items, -1 if
	//     the item is not found.
	//-----------------------------------------------------------------------
	int Find(CSPPropertyGridItem* pItem);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member to clear all the items in a list.
	//-----------------------------------------------------------------------
	void Clear();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member to get count of the items in a list.
	// Returns:
	//     Count of the items in a list.
	//-----------------------------------------------------------------------
	long GetCount();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member to determine if any items have been added
	//     to the property grid.
	// Returns:
	//     TRUE if there are no items in the grid, FALSE if at least
	//     one item has been added to the grid.
	//-----------------------------------------------------------------------
	BOOL IsEmpty();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member to get an item by its index.
	// Parameters:
	//     nIndex - Item's index.
	// Returns:
	//     A pointer to a CSPPropertyGridItem object.
	//-----------------------------------------------------------------------
	CSPPropertyGridItem* GetAt(int nIndex);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member to remove an item by its index.
	// Parameters:
	//     nIndex - Item's index.
	//-----------------------------------------------------------------------
	void RemoveAt (int nIndex);


protected:

	//-------------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridItems object.
	//-------------------------------------------------------------------------
	CSPPropertyGridItems();

	//-----------------------------------------------------------------------
	// Summary:
	//     This member adds a new item to the end (tail) of the
	//     Property Grid's collection of items. The collection can be empty
	//     before the operation.
	// Parameters:
	//     pItem - Pointer to the CSPPropertyGridItem to add to the collection.
	//-----------------------------------------------------------------------
	void AddTail(CSPPropertyGridItem* pItem);

	//-----------------------------------------------------------------------
	// Summary:
	//     This member is called to insert a CSPPropertyGridItem object
	//     into the collections of items at a specific location specified
	//     by nIndex.
	// Parameters:
	//     nIndex - Position in the collection of items to insert pItem.
	//     pItem  - Pointer to the CSPPropertyGridItem to add to the collection.
	//-----------------------------------------------------------------------
	void InsertAt(int nIndex, CSPPropertyGridItem* pItem);

	//-----------------------------------------------------------------------
	// Summary:
	//     This member adds a collection of items to the end (tail) of the
	//     Property Grid's collection of items. The collection can be empty
	//     before the operation.
	// Parameters:
	//     pItems - Pointer to the collection of CSPPropertyGridItems to
	//              add to the collection.
	//-----------------------------------------------------------------------
	void AddTail(CSPPropertyGridItems* pItems);

	//-------------------------------------------------------------------------
	// Summary:
	//     This member sorts the collection of items in alphabetical order.
	// Remarks:
	//     This method uses Visual C++ run-time library (MSVCRT)
	//     implementation of the quick-sort function, qsort, for sorting
	//     stored CSPPropertyGridItem objects.
	//-------------------------------------------------------------------------
	void Sort();

private:
	static int _cdecl CompareFunc(const CSPPropertyGridItem** ppItem1, const CSPPropertyGridItem** ppItem2);

private:
	CArray<CSPPropertyGridItem*, CSPPropertyGridItem*> m_arrItems;
	CSPPropertyGridView* m_pGrid;

	friend class CSPPropertyGridItem;
	friend class CSPPropertyGridView;
};


class  CSPPropertyGridItemConstraint : public CCmdTarget
{
public:
	CSPPropertyGridItemConstraint();
	int GetIndex();

public:
	CString m_strConstraint;    // Caption text of constraint.  This is the
	                            // text displayed for this constraint.
	DWORD_PTR   m_dwData;       // The 32-bit value associated with the item.

protected:
	int m_nIndex;               // Index of constraint.

	friend class CSPPropertyGridItemConstraints;

};

class  CSPPropertyGridItemConstraints : public CCmdTarget
{
protected:

	
	CSPPropertyGridItemConstraints(CSPPropertyGridItem* pItem);
	~CSPPropertyGridItemConstraints();

public:

	CSPPropertyGridItemConstraint* AddConstraint(CString str, DWORD dwData = 0);

	int GetCount();
	BOOL IsEmpty();
	void RemoveAll();
	void RemoveAt (int nIndex);
	void SetCurrent(int nIndex);
	int GetCurrent();
	int FindConstraint(CString str);
	int FindConstraint(DWORD dwData); // <COMBINE CSPPropertyGridItemConstraints::FindConstraint@CString>
	CString GetAt(int nIndex);
	CSPPropertyGridItemConstraint* GetConstraintAt(int nIndex);
	void Sort();

private:
	static int _cdecl CompareFunc(const CSPPropertyGridItemConstraint** ppConstraint1, const CSPPropertyGridItemConstraint** ppConstraint2);

private:
	CArray<CSPPropertyGridItemConstraint*, CSPPropertyGridItemConstraint*>  m_arrConstraints;
	int m_nCurrent;
	CSPPropertyGridItem* m_pItem;


	friend class CSPPropertyGridItem;

};


class  CSPPropertyGridItem : public CCmdTarget
{
public:
	CSPPropertyGridItem(CString strCaption, LPCTSTR strValue = NULL, CString* pBindString = NULL);
	CSPPropertyGridItem(UINT nID, LPCTSTR strValue = NULL, CString* pBindString = NULL);  // <COMBINE CSPPropertyGridItem::CSPPropertyGridItem@CString@LPCTSTR@CString*>

	virtual ~CSPPropertyGridItem();

public:
	void SetCaption(CString strCaption);
	void SetPrompt(CString strCaption);
	void SetDescription(LPCTSTR lpszDescription);
	void SetReadOnly(BOOL bReadOnly);
	void SetID(UINT nID);
	void SetFlags(UINT nFlags);
	CString GetCaption() const;
	CString GetDescription();
	BOOL GetReadOnly();
	UINT GetID();
	CString GetValue();
	UINT GetFlags();
	BOOL IsCategory();
	CSPPropertyGridItem* AddChildItem(CSPPropertyGridItem* pItem);
	CSPPropertyGridItem* InsertChildItem(int nIndex, CSPPropertyGridItem* pItem);
	BOOL HasChilds();
	CSPPropertyGridItems* GetChilds();
	BOOL HasParent(CSPPropertyGridItem* pParent);
	CSPPropertyGridItemConstraints* GetConstraints();
	void SetConstraintEdit(BOOL bConstraintEdit);
	BOOL GetConstraintEdit();
	void Expand();
	void Collapse();
	void Select();
	CRect GetItemRect();
	virtual CRect GetValueRect();
	virtual void SetValue(CString strValue);
	virtual void OnValueChanged(CString strValue);
	CSPPropertyGridView* GetGrid();
	BOOL IsVisible() const;
	BOOL IsExpanded() const;
	CSPPropertyGridItem* GetParentItem();
	int GetIndent();
	virtual BOOL OnDrawItemValue(CDC& dc, CRect rcValue);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	BOOL IsSelected();
	void BindToString(CString* pBindString);
	void Remove();
	void SetMask(LPCTSTR strMask, LPCTSTR strLiteral, TCHAR chPrompt = _T('_'));
	void SetPasswordMask(TCHAR chMask = _T('*'));
	CString GetMaskedText();
	void SetMaskedText(LPCTSTR lpszMaskedText);
	virtual CString GetViewValue();
	void SetItemData(DWORD_PTR dwData);
	DWORD_PTR GetItemData();
	virtual CSPPropertyGridInplaceEdit& GetInplaceEdit();
protected:
	virtual void SetVisible(BOOL bVisible);
	virtual void OnSelect();
	virtual void OnDeselect();
	virtual BOOL OnChar(UINT nChar);
	virtual void OnLButtonDblClk();
	virtual void OnRButtonDown();
	virtual BOOL OnLButtonDown(UINT nFlags, CPoint point);
	virtual void OnAddChildItem();
	virtual void OnInplaceButtonDown();
	virtual CSPPropertyGridInplaceButton& GetInplaceButton();
	virtual CSPPropertyGridInplaceList& GetInplaceList();
	virtual void OnValidateEdit();
	virtual void SetEditText(CString str);
	virtual void OnBeforeInsert();
	virtual void OnConstraintsChanged();

protected:
	BOOL                    m_bConstraintEdit;  // TRUE to constraint edit.

	int                     m_nIndex;           // Index of the item.
	int                     m_nIndent;          // Indent of the item.
	UINT                    m_nID;              // Identifier of the item.
	UINT                    m_nFlags;           // Item's flags.
	BOOL                    m_bReadOnly;        // TRUE to disable item's edition.
	BOOL                    m_bVisible;         // Visibility of the item.
	BOOL                    m_bCategory;        // TRUE if the item is category.
	BOOL                    m_bExpanded;        // TRUE if item is expanded.
	CString                 m_strValue;         // Value of the item.
	CString                 m_strCaption;       // Caption of the item.
	CString                 m_strDescription;   // Description of the item.
	CSPPropertyGridItem*   m_pParent;          // Parent item.
	CSPPropertyGridView*   m_pGrid;            // Parent grid class.
	CSPPropertyGridItems*  m_pChilds;          // Child items.
	CSPPropertyGridItemConstraints*    m_pConstraints;     // Item's constraints.

	CString*                m_pBindString;      // Binded string.
	CString m_strFormat;                        // Format of the double value.

	CString m_strMask;                          // String to use as edit mask.
	CString m_strLiteral;                       // String to use as literal.  This is the same as the edit mask, but all mask characters are replaced with m_chPrompt.
	TCHAR m_chPrompt;                           // Character used as a space holder for a character.  This is used in m_strLiteral.
	bool m_bUseMask;                            // TRUE to use and edit mask to display item data.

	BOOL m_bPassword;                           // TRUE to use a password mask.  If TRUE, then each character will be replaced with an asterisk (*).
	DWORD_PTR m_dwData;                         // The 32-bit value associated with the item

private:
	void Init();
	void Refresh(BOOL bInvalidate = TRUE);


	friend class CSPPropertyGridItems;
	friend class CSPPropertyGridView;
	friend class CSPPropertyGridInplaceEdit;
	friend class CSPPropertyGridInplaceButton;

	DECLARE_DYNAMIC(CSPPropertyGridItem)


	friend class CSPPropertyGridItemConstraints;
	friend class CSPPropertyGrid;
};


class  CSPPropertyGridItemCategory : public CSPPropertyGridItem
{
public:
	
	CSPPropertyGridItemCategory(CString strCaption);
	CSPPropertyGridItemCategory(UINT nID); // <COMBINE CSPPropertyGridItemCategory::CSPPropertyGridItemCategory@CString>
};

//////////////////////////////////////////////////////////////////////
AFX_INLINE long CSPPropertyGridItems::GetCount() {
	return (long)m_arrItems.GetSize();
}
AFX_INLINE BOOL CSPPropertyGridItems::IsEmpty() {
	return GetCount() == 0;
}
AFX_INLINE void CSPPropertyGridItems::AddTail(CSPPropertyGridItem* pItem) {
	m_arrItems.Add(pItem);
}
AFX_INLINE void CSPPropertyGridItems::InsertAt(int nIndex, CSPPropertyGridItem* pItem) {
	m_arrItems.InsertAt(nIndex, pItem);
}
AFX_INLINE BOOL CSPPropertyGridItem::HasChilds() {
	return !m_pChilds->IsEmpty();
}
AFX_INLINE void CSPPropertyGridItem::SetCaption(CString strCaption) {
	m_strCaption = strCaption;
}
AFX_INLINE CString CSPPropertyGridItem::GetCaption() const {
	return m_strCaption;
}
AFX_INLINE CString CSPPropertyGridItem::GetDescription() {
	return m_strDescription;
}
AFX_INLINE BOOL CSPPropertyGridItem::GetReadOnly() {
	return m_bReadOnly;
}
AFX_INLINE void CSPPropertyGridItem::SetID(UINT nID) {
	m_nID = nID;
}
AFX_INLINE UINT CSPPropertyGridItem::GetID() {
	return m_nID;
}
AFX_INLINE CString CSPPropertyGridItem::GetValue() {
	return m_strValue;
}
AFX_INLINE BOOL CSPPropertyGridItem::IsCategory() {
	return m_bCategory;
}
AFX_INLINE CSPPropertyGridView* CSPPropertyGridItem::GetGrid() {
	return m_pGrid;
}
AFX_INLINE BOOL CSPPropertyGridItem::OnDrawItemValue(CDC&, CRect) {
	return FALSE;
}
AFX_INLINE void CSPPropertyGridItem::OnAddChildItem() {

}
AFX_INLINE void CSPPropertyGridItem::SetFlags(UINT nFlags) {
	ASSERT(!m_bCategory);
	ASSERT((nFlags & (SPGridItemHasComboButton | SPGridItemHasExpandButton)) != (SPGridItemHasComboButton | SPGridItemHasExpandButton));
	m_nFlags = nFlags;
}
AFX_INLINE UINT CSPPropertyGridItem::GetFlags() {
	return m_nFlags;
}
AFX_INLINE BOOL CSPPropertyGridItem::IsVisible() const {
	return m_bVisible;
}
AFX_INLINE BOOL CSPPropertyGridItem::IsExpanded() const {
	return m_bExpanded;
}
AFX_INLINE CSPPropertyGridItem* CSPPropertyGridItem::GetParentItem() {
	return m_pParent;
}
AFX_INLINE CSPPropertyGridItems* CSPPropertyGridItem::GetChilds() {
	return m_pChilds;
}
AFX_INLINE int CSPPropertyGridItem::GetIndent() {
	return m_nIndent;
}
AFX_INLINE void CSPPropertyGridItem::SetConstraintEdit(BOOL bConstraintEdit) {
	m_bConstraintEdit = bConstraintEdit;
}
AFX_INLINE BOOL CSPPropertyGridItem::GetConstraintEdit() {
	return m_bConstraintEdit;
}
AFX_INLINE void CSPPropertyGridItem::SetPasswordMask(TCHAR chMask ) {
	m_bPassword = TRUE;
	m_chPrompt = chMask;
}
AFX_INLINE void CSPPropertyGridItem::OnRButtonDown() {

}
AFX_INLINE void CSPPropertyGridItem::OnConstraintsChanged() {

}

AFX_INLINE void CSPPropertyGridItem::MeasureItem(LPMEASUREITEMSTRUCT /*lpMeasureItemStruct*/) {

}
AFX_INLINE void CSPPropertyGridItem::SetItemData(DWORD_PTR dwData) {
	m_dwData = dwData;
}
AFX_INLINE DWORD_PTR CSPPropertyGridItem::GetItemData() {
	return m_dwData;
}
AFX_INLINE void CSPPropertyGridItem::SetVisible(BOOL bVisible) {
	m_bVisible = bVisible;
}
AFX_INLINE CSPPropertyGridItemConstraints* CSPPropertyGridItem::GetConstraints(){
	return m_pConstraints;
}
AFX_INLINE int CSPPropertyGridItemConstraints::GetCount() {
	return (int)m_arrConstraints.GetSize();
}
AFX_INLINE BOOL CSPPropertyGridItemConstraints::IsEmpty() {
	return GetCount() == 0;
}
AFX_INLINE void CSPPropertyGridItemConstraints::SetCurrent(int nIndex){
	m_nCurrent = nIndex;
}
AFX_INLINE int CSPPropertyGridItemConstraints::GetCurrent(){
	return m_nCurrent;
}
AFX_INLINE int CSPPropertyGridItemConstraint::GetIndex() {
	return m_nIndex;
}


#endif
