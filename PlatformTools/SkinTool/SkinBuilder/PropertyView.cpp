﻿// PropertyView.cpp : implementation file
//

#include "stdafx.h"
#include "SkinBuilder.h"
#include "PropertyView.h"
#include ".\propertyview.h"
#include "SPPropertyGridItemImage.h"


// CPropertyView

IMPLEMENT_DYNCREATE(CPropertyView, CView)

CPropertyView::CPropertyView()
{
}

CPropertyView::~CPropertyView()
{
}

BEGIN_MESSAGE_MAP(CPropertyView, CView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_MESSAGE(SPWM_PROPERTYGRID_NOTIFY, OnGridNotify)
END_MESSAGE_MAP()


void CPropertyView::CreatePropertyApplication(ApplicationSkin *pAllicationSkin)
{
	m_wndPropertyApplication.Create(CRect(0,0,0,0),this,IDC_PROPERTY_APPLICATION);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyApplication.AddCategory(_T("Application"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_APPLICATION, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_APP_AUTHOR, NULL));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_APP_DESCRIPTION, NULL));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_APP_EMAIL, NULL));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_APP_HOMEPAGE, NULL));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_APP_USETRANS, NULL));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_APP_TRANSCOLOR, NULL));

	pCategoryApplication->Expand();
}
void CPropertyView::SetApplication(ApplicationSkin *pApp)
{
	CSPPropertyGridItem *pItem = NULL;
	pItem = m_wndPropertyApplication.FindItem(IDS_STRING_APPLICATION);
	if(pItem)
		pItem->SetValue(pApp->strApplication);

	pItem = m_wndPropertyApplication.FindItem(IDS_STRING_APP_AUTHOR);
	if(pItem)
		pItem->SetValue(pApp->strAuthor);
	pItem = m_wndPropertyApplication.FindItem(IDS_STRING_APP_DESCRIPTION);
	if(pItem)
		pItem->SetValue(pApp->strDescription);

	pItem = m_wndPropertyApplication.FindItem(IDS_STRING_APP_EMAIL);
	if(pItem)
		pItem->SetValue(pApp->strEmail);

	pItem = m_wndPropertyApplication.FindItem(IDS_STRING_APP_HOMEPAGE);
	if(pItem)
		pItem->SetValue(pApp->strHomepage);

	pItem = m_wndPropertyApplication.FindItem(IDS_STRING_APP_USETRANS);
	((CSPPropertyGridItemBool*)pItem)->SetBool(pApp->bUseTrans);

	pItem = m_wndPropertyApplication.FindItem(IDS_STRING_APP_TRANSCOLOR);
	((CSPPropertyGridItemColor*)pItem)->SetColor(pApp->colorTransparent);



}
void CPropertyView::CreatePropertyButton()
{
	m_wndPropertyButton.Create(CRect(0,0,0,0),this,IDC_PROPERTY_BUTTONSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyButton.AddCategory(_T("Button"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_BUTTON_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_BUTTON_ID, NULL ));
	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	LOGFONT font= nif.lfCaptionFont;
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_BUTTON_FONT, font));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_BUTTON_AUTOFONT, NULL ));

	CSPPropertyGridItem* pCategoryNormal;
	pCategoryNormal = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_BUTTON_NORMAL, NULL));
	pCategoryNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_BUTTON_NORMAL_IMAGE));
	pCategoryNormal->Expand();
	
	CSPPropertyGridItem* pCategoryHighlighted;
	pCategoryHighlighted = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_BUTTON_HIGHTLIGHTED, NULL));
	pCategoryHighlighted->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_BUTTON_HIGHTLIGHTED_IMAGE));
	pCategoryHighlighted->Expand();

	CSPPropertyGridItem* pCategoryPressed;
	pCategoryPressed = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_BUTTON_PRESSED, NULL));
	pCategoryPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_BUTTON_PRESSED_IMAGE));
	pCategoryPressed->Expand();

	CSPPropertyGridItem* pCategoryFocused;
	pCategoryFocused = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_BUTTON_FOCUSED, NULL));
	pCategoryFocused->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_BUTTON_FOCUSED_IMAGE));
	pCategoryFocused->Expand();

	CSPPropertyGridItem* pCategoryDefault;
	pCategoryDefault = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_BUTTON_DEFAULT, NULL));
	pCategoryDefault->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_BUTTON_DEFAULT_IMAGE));
	pCategoryDefault->Expand();
	
	CSPPropertyGridItem* pCategoryDisabled;
	pCategoryDisabled = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_BUTTON_DISABLED, NULL));
	pCategoryDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_BUTTON_DISABLED_IMAGE));
	pCategoryDisabled->Expand();

	pCategoryApplication->Expand();

}
void CPropertyView::SetButtonSkin(CButtonSkin *pButtonSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_NAME);
	pItem->SetValue(pButtonSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_ID);
	pNumber->SetNumber(pButtonSkin->m_nCtlID);

	pFont = (CSPPropertyGridItemFont*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_FONT);
	pFont->SetFont(pButtonSkin->m_fontButton);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_AUTOFONT);
	pBool->SetBool(pButtonSkin->m_bAutoFont);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_NORMAL_IMAGE);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pButtonSkin->m_stateButton[keNormal].imageSkin);
	pImage->SetValue(pButtonSkin->m_stateButton[keNormal].imageSkin.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_FOCUSED_IMAGE);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pButtonSkin->m_stateButton[keFocused].imageSkin);
	pImage->SetValue(pButtonSkin->m_stateButton[keFocused].imageSkin.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_DEFAULT_IMAGE);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pButtonSkin->m_stateButton[keDefault].imageSkin);
	pImage->SetValue(pButtonSkin->m_stateButton[keDefault].imageSkin.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_PRESSED_IMAGE);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pButtonSkin->m_stateButton[kePressed].imageSkin);
	pImage->SetValue(pButtonSkin->m_stateButton[kePressed].imageSkin.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_HIGHTLIGHTED_IMAGE);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pButtonSkin->m_stateButton[keHighlighted].imageSkin);
	pImage->SetValue(pButtonSkin->m_stateButton[keHighlighted].imageSkin.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyButton.FindItem(IDS_STRING_BUTTON_DISABLED_IMAGE);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pButtonSkin->m_stateButton[keDisabled].imageSkin);
	pImage->SetValue(pButtonSkin->m_stateButton[keDisabled].imageSkin.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;


}
void CPropertyView::CreatePropertyRadio()
{
	m_wndPropertyRadio.Create(CRect(0,0,0,0),this,IDC_PROPERTY_RADIOSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyRadio.AddCategory(_T("Radio"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_RADIO_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_RADIO_ID, NULL ));

	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);

	LOGFONT font= nif.lfCaptionFont;
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_RADIO_FONT, font));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_RADIO_AUTOFONT, NULL ));

	CSPPropertyGridItem* pCategoryNormal;
	pCategoryNormal = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_RADIO_NORMAL, NULL));
	pCategoryNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_NORMAL1));
	pCategoryNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_NORMAL2));
	pCategoryNormal->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_RADIO_NORMAL3));
	pCategoryNormal->Expand();

	CSPPropertyGridItem* pCategoryHover;
	pCategoryHover = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_RADIO_HOVER, NULL));
	pCategoryHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_HOVER1));
	pCategoryHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_HOVER2));
	pCategoryHover->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_RADIO_HOVER3));
	pCategoryHover->Expand();

	CSPPropertyGridItem* pCategoryFocused;
	pCategoryFocused = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_RADIO_FOCUSED, NULL));
	pCategoryFocused->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_FOCUSED1));
	pCategoryFocused->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_FOCUSED2));
	pCategoryFocused->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_RADIO_FOCUSED3));
	pCategoryFocused->Expand();


	CSPPropertyGridItem* pCategoryPressed;
	pCategoryPressed = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_RADIO_PRESSED, NULL));
	pCategoryPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_PRESSED1));
	pCategoryPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_PRESSED2));
	pCategoryPressed->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_RADIO_PRESSED3));
	pCategoryPressed->Expand();


	CSPPropertyGridItem* pCategoryDisabled;
	pCategoryDisabled = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_RADIO_DISABLED, NULL));
	pCategoryDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_DISABLED1));
	pCategoryDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_DISABLED2));
	pCategoryDisabled->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_RADIO_DISABLED3));
	pCategoryDisabled->Expand();


	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_RADIO_IMAGEBACK));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_RADIO_COLORBACK));


	pCategoryApplication->Expand();

}
void CPropertyView::SetRadioSkin(CRadioSkin *pRadioSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_NAME);
	pItem->SetValue(pRadioSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_ID);
	pNumber->SetNumber(pRadioSkin->m_nCtlID);

	pFont = (CSPPropertyGridItemFont*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_FONT);
	pFont->SetFont(pRadioSkin->m_fontRadio);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_AUTOFONT);
	pBool->SetBool(pRadioSkin->m_bAutoFont);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_NORMAL1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioNormal].imageChecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioNormal].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_NORMAL2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioNormal].imageUnchecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioNormal].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_HOVER1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioHover].imageChecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioHover].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_HOVER2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioHover].imageUnchecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioHover].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_PRESSED1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioPressed].imageChecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioPressed].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_PRESSED2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioPressed].imageUnchecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioPressed].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_FOCUSED1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioFocused].imageChecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioFocused].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_FOCUSED2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioFocused].imageUnchecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioFocused].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_DISABLED1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioDisabled].imageChecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioDisabled].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_DISABLED2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pRadioSkin->m_stateRadio[keRadioDisabled].imageUnchecked);
	pImage->SetValue(pRadioSkin->m_stateRadio[keRadioDisabled].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pRadioSkin->m_imageBackground);
	pImage->SetValue(pRadioSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyRadio.FindItem(IDS_STRING_RADIO_COLORBACK);
	pColor->SetColor(pRadioSkin->m_colorBackground);



}
void CPropertyView::CreatePropertyCheckBox()
{
	m_wndPropertyCheckBox.Create(CRect(0,0,0,0),this,IDC_PROPERTY_CHECKBOXSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyCheckBox.AddCategory(_T("CheckBox"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_CHECKBOX_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_CHECKBOX_ID, NULL ));


	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	LOGFONT font= nif.lfCaptionFont;
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_CHECKBOX_FONT, font));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_CHECKBOX_AUTOFONT, NULL ));

	CSPPropertyGridItem* pCategoryNormal;
	pCategoryNormal = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_CHECKBOX_NORMAL, NULL));
	pCategoryNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_NORMAL1));
	pCategoryNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_NORMAL2));
	pCategoryNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_NORMAL3));
	pCategoryNormal->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_CHECKBOX_NORMAL4));
	pCategoryNormal->Expand();

	CSPPropertyGridItem* pCategoryHover;
	pCategoryHover = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_CHECKBOX_HOVER, NULL));
	pCategoryHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_HOVER1));
	pCategoryHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_HOVER2));
	pCategoryHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_HOVER3));
	pCategoryHover->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_CHECKBOX_HOVER4));
	pCategoryHover->Expand();

	CSPPropertyGridItem* pCategoryFocused;
	pCategoryFocused = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_CHECKBOX_FOCUSED, NULL));
	pCategoryFocused->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_FOCUSED1));
	pCategoryFocused->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_FOCUSED2));
	pCategoryFocused->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_FOCUSED3));
	pCategoryFocused->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_CHECKBOX_FOCUSED4));
	pCategoryFocused->Expand();


	CSPPropertyGridItem* pCategoryPressed;
	pCategoryPressed = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_CHECKBOX_PRESSED, NULL));
	pCategoryPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_PRESSED1));
	pCategoryPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_PRESSED2));
	pCategoryPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_PRESSED3));
	pCategoryPressed->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_CHECKBOX_PRESSED4));
	pCategoryPressed->Expand();


	CSPPropertyGridItem* pCategoryDisabled;
	pCategoryDisabled = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_CHECKBOX_DISABLED, NULL));
	pCategoryDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_DISABLED1));
	pCategoryDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_DISABLED2));
	pCategoryDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_DISABLED3));
	pCategoryDisabled->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_CHECKBOX_DISABLED4));
	pCategoryDisabled->Expand();

	
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_CHECKBOX_IMAGEBACK));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_CHECKBOX_COLORBACK));


	pCategoryApplication->Expand();

}
void CPropertyView::SetCheckBoxSkin(CCheckBoxSkin *pCheckBoxSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_NAME);
	pItem->SetValue(pCheckBoxSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_ID);
	pNumber->SetNumber(pCheckBoxSkin->m_nCtlID);

	pFont = (CSPPropertyGridItemFont*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_FONT);
	pFont->SetFont(pCheckBoxSkin->m_fontCheckBox);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_AUTOFONT);
	pBool->SetBool(pCheckBoxSkin->m_bAutoFont);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_NORMAL1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckNormal].imageChecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckNormal].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_NORMAL2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckNormal].imageUnchecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckNormal].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_NORMAL3);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckNormal].imageIndeterminate);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckNormal].imageIndeterminate.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_HOVER1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckHover].imageChecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckHover].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_HOVER2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckHover].imageUnchecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckHover].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_HOVER3);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckHover].imageIndeterminate);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckHover].imageIndeterminate.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_PRESSED1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckPressed].imageChecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckPressed].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_PRESSED2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckPressed].imageUnchecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckPressed].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_PRESSED3);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckPressed].imageIndeterminate);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckPressed].imageIndeterminate.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;



	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_FOCUSED1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckFocused].imageChecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckFocused].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_FOCUSED2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckFocused].imageUnchecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckFocused].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_FOCUSED3);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckFocused].imageIndeterminate);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckFocused].imageIndeterminate.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_DISABLED1);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckDisabled].imageChecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckDisabled].imageChecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_DISABLED2);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckDisabled].imageUnchecked);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckDisabled].imageUnchecked.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_DISABLED3);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pCheckBoxSkin->m_stateCheckBox[keCheckDisabled].imageIndeterminate);
	pImage->SetValue(pCheckBoxSkin->m_stateCheckBox[keCheckDisabled].imageIndeterminate.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertyCheckBox.FindItem(IDS_STRING_CHECKBOX_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pCheckBoxSkin->m_imageBackground);
	pImage->SetValue(pCheckBoxSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;



}
void CPropertyView::CreatePropertyGroupBox()
{
	m_wndPropertyGroupBox.Create(CRect(0,0,0,0),this,IDC_PROPERTY_GROUPBOXSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyGroupBox.AddCategory(_T("GroupBox"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_GROUPBOX_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_GROUPBOX_ID, NULL ));


	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	LOGFONT font= nif.lfCaptionFont;
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_GROUPBOX_FONT, font));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_GROUPBOX_AUTOFONT, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_GROUPBOX_TRANSPARENT, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_GROUPBOX_IMAGEFRAME));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_GROUPBOX_IMAGECAPTION));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_GROUPBOX_COLORCAPTION));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemText(IDS_STRING_GROUPBOX_TEXT));

	pCategoryApplication->Expand();
}

void CPropertyView::SetGroupBoxSkin(CGroupBoxSkin *pGroupBoxSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyGroupBox.FindItem(IDS_STRING_GROUPBOX_NAME);
	pItem->SetValue(pGroupBoxSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyGroupBox.FindItem(IDS_STRING_GROUPBOX_ID);
	pNumber->SetNumber(pGroupBoxSkin->m_nCtlID);

	pFont = (CSPPropertyGridItemFont*)m_wndPropertyGroupBox.FindItem(IDS_STRING_GROUPBOX_FONT);
	pFont->SetFont(pGroupBoxSkin->m_fontGroupBox);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyGroupBox.FindItem(IDS_STRING_GROUPBOX_AUTOFONT);
	pBool->SetBool(pGroupBoxSkin->m_bAutoFont);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyGroupBox.FindItem(IDS_STRING_GROUPBOX_IMAGEFRAME);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pGroupBoxSkin->m_imageFrame);
	pImage->SetValue(pGroupBoxSkin->m_imageFrame.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyGroupBox.FindItem(IDS_STRING_GROUPBOX_IMAGECAPTION);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pGroupBoxSkin->m_imageCaption);
	pImage->SetValue(pGroupBoxSkin->m_imageCaption.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	
}
void CPropertyView::CreatePropertyEdit()
{
	m_wndPropertyEdit.Create(CRect(0,0,0,0),this,IDC_PROPERTY_EDITSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyEdit.AddCategory(_T("Edit"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_EDIT_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_EDIT_ID, NULL ));


	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	LOGFONT font= nif.lfCaptionFont;
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_EDIT_FONT, font));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_EDIT_AUTOFONT, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_EDIT_IMAGEBORDER));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_EDIT_COLORBACK));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_EDIT_COLORTEXT));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_TEDIT_DISABLEDCOLOR));

	pCategoryApplication->Expand();
}
void CPropertyView::SetEditSkin(CEditSkin *pEditSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyEdit.FindItem(IDS_STRING_EDIT_NAME);
	pItem->SetValue(pEditSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyEdit.FindItem(IDS_STRING_EDIT_ID);
	pNumber->SetNumber(pEditSkin->m_nCtlID);

	pFont = (CSPPropertyGridItemFont*)m_wndPropertyEdit.FindItem(IDS_STRING_EDIT_FONT);
	pFont->SetFont(pEditSkin->m_fontEdit);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyEdit.FindItem(IDS_STRING_EDIT_AUTOFONT);
	pBool->SetBool(pEditSkin->m_bAutoFont);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyEdit.FindItem(IDS_STRING_EDIT_IMAGEBORDER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pEditSkin->m_imageBorder);
	pImage->SetValue(pEditSkin->m_imageBorder.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyStatic.FindItem(IDS_STRING_STATIC_COLORBACK);
	pColor->SetColor(pEditSkin->m_colorBack);
}
void CPropertyView::CreatePropertyListBox()
{
	m_wndPropertyListBox.Create(CRect(0,0,0,0),this,IDC_PROPERTY_COMBOBOXSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyListBox.AddCategory(_T("ListBox"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_LISTBOX_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_LISTBOX_ID, NULL ));


	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	LOGFONT font= nif.lfCaptionFont;
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_LISTBOX_FONT, font));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_LISTBOX_AUTOFONT, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_LISTBOX_IMAGEBOREDER));

	pCategoryApplication->Expand();
}
void CPropertyView::SetListBoxSkin(CListBoxSkin *pListBoxSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyListBox.FindItem(IDS_STRING_LISTBOX_NAME);
	pItem->SetValue(pListBoxSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyListBox.FindItem(IDS_STRING_LISTBOX_ID);
	pNumber->SetNumber(pListBoxSkin->m_nCtlID);

	pFont = (CSPPropertyGridItemFont*)m_wndPropertyListBox.FindItem(IDS_STRING_LISTBOX_FONT);
	pFont->SetFont(pListBoxSkin->m_fontListBox);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyListBox.FindItem(IDS_STRING_LISTBOX_AUTOFONT);
	pBool->SetBool(pListBoxSkin->m_bAutoFont);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyListBox.FindItem(IDS_STRING_LISTBOX_IMAGEBOREDER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pListBoxSkin->m_imageBorder);
	pImage->SetValue(pListBoxSkin->m_imageBorder.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
}
void CPropertyView::CreatePropertyPopupMenu()
{
	m_wndPropertyPopupMenu.Create(CRect(0,0,0,0),this,IDC_PROPERTY_POPUPMENUSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyPopupMenu.AddCategory(_T("PopupMenu"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_POPUPMENU_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_POPUPMENU_ID, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_POPUPMENU_IMAGEFRAME));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_POPUPMENU_COLORBACKGROUND));
	CSPPropertyGridItem *pMenuItem = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_POPUPMENU_IMAGEITEM));
	pMenuItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_POPUPMENU_IMAGEACTIVE));
	pMenuItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_POPUPMENU_IMAGESEPARATOR));

	pMenuItem->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_POPUPMENU_TEXTNORMAL));
	pMenuItem->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_POPUPMENU_TEXTGRAYED));
	pMenuItem->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_POPUPMENU_TEXTACTIVE));
	pMenuItem->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_POPUPMENU_COLORACTIVE));
	pMenuItem->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_POPUPMENU_COLORSEPERATOR));

	pMenuItem->Expand();
	pCategoryApplication->Expand();
}
void CPropertyView::SetPopupMenuSkin(CPopupMenuSkin *pPopupMenuSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_NAME);
	pItem->SetValue(pPopupMenuSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_ID);
	pNumber->SetNumber(pPopupMenuSkin->m_nCtlID);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_IMAGEFRAME);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pPopupMenuSkin->m_imageFrame);
	pImage->SetValue(pPopupMenuSkin->m_imageFrame.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_IMAGESEPARATOR);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pPopupMenuSkin->m_itemPopup.imageSeparator);
	pImage->SetValue(pPopupMenuSkin->m_itemPopup.imageSeparator.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_IMAGEACTIVE);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pPopupMenuSkin->m_itemPopup.imageActive);
	pImage->SetValue(pPopupMenuSkin->m_itemPopup.imageSeparator.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_COLORBACKGROUND);
	pColor->SetColor(pPopupMenuSkin->m_colorBackground);


	pColor = (CSPPropertyGridItemColor*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_TEXTNORMAL);
	pColor->SetColor(pPopupMenuSkin->m_itemPopup.textNormal);

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_TEXTGRAYED);
	pColor->SetColor(pPopupMenuSkin->m_itemPopup.textGrayed);

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_TEXTACTIVE);
	pColor->SetColor(pPopupMenuSkin->m_itemPopup.textActive);

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_COLORACTIVE);
	pColor->SetColor(pPopupMenuSkin->m_itemPopup.colorActive);

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyPopupMenu.FindItem(IDS_STRING_POPUPMENU_COLORSEPERATOR);
	pColor->SetColor(pPopupMenuSkin->m_itemPopup.colorSeparator);
}
void CPropertyView::CreatePropertyComboBox()
{
	m_wndPropertyComboBox.Create(CRect(0,0,0,0),this,IDC_PROPERTY_COMBOBOXSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyComboBox.AddCategory(_T("Combox"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_COMBOX_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_COMBOX_ID, NULL ));


	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	LOGFONT font= nif.lfCaptionFont;
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_COMBOX_FONT, font));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_COMBOX_AUTOFONT, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_COMBOX_IMAGEBORDER));
	
	CSPPropertyGridItem* pThumb = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_COMBOX_IMAGETHUMB));
	pThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_COMBOX_IMAGETHUMB_NORMAL));
	pThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_COMBOX_IMAGETHUMB_HOVERED));
	pThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_COMBOX_IMAGETHUMB_PRESSED));
	pThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_COMBOX_IMAGETHUMB_DISABLED));

	pThumb->Expand();
	pCategoryApplication->Expand();
}
void CPropertyView::SetComboxSkin(CComboBoxSkin *pComboBoxSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_NAME);
	pItem->SetValue(pComboBoxSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_ID);
	pNumber->SetNumber(pComboBoxSkin->m_nCtlID);

	pFont = (CSPPropertyGridItemFont*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_FONT);
	pFont->SetFont(pComboBoxSkin->m_fontComboBox);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_AUTOFONT);
	pBool->SetBool(pComboBoxSkin->m_bAutoFont);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_IMAGEBORDER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pComboBoxSkin->m_imageBorder);
	pImage->SetValue(pComboBoxSkin->m_imageBorder.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_IMAGETHUMB_NORMAL);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pComboBoxSkin->m_iamgeThumb[keThumbNormal]);
	pImage->SetValue(pComboBoxSkin->m_iamgeThumb[keThumbNormal].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_IMAGETHUMB_HOVERED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pComboBoxSkin->m_iamgeThumb[keThumbHovered]);
	pImage->SetValue(pComboBoxSkin->m_iamgeThumb[keThumbHovered].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_IMAGETHUMB_PRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pComboBoxSkin->m_iamgeThumb[keThumbPressed]);
	pImage->SetValue(pComboBoxSkin->m_iamgeThumb[keThumbPressed].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyComboBox.FindItem(IDS_STRING_COMBOX_IMAGETHUMB_PRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pComboBoxSkin->m_iamgeThumb[keThumbDisabled]);
	pImage->SetValue(pComboBoxSkin->m_iamgeThumb[keThumbDisabled].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;
}
void CPropertyView::CreatePropertyStatic()
{
	m_wndPropertyStatic.Create(CRect(0,0,0,0),this,IDC_PROPERTY_STATICSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyStatic.AddCategory(_T("Static"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_STATIC_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_STATIC_ID, NULL ));


	NONCLIENTMETRICS nif;
	nif.cbSize = sizeof(nif);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS,sizeof(NONCLIENTMETRICS),&nif,0);
	LOGFONT font= nif.lfCaptionFont;
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_STATIC_FONT, font));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_STATIC_AUTOFONT, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_STATIC_TRANSPARENT, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_STATIC_IMAGEBACK));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_STATIC_COLORBACK));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_STATIC_TEXT));


	pCategoryApplication->Expand();
}
void CPropertyView::SetStaticSkin(CStaticSkin *pStaticSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyStatic.FindItem(IDS_STRING_STATIC_NAME);
	pItem->SetValue(pStaticSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyStatic.FindItem(IDS_STRING_STATIC_ID);
	pNumber->SetNumber(pStaticSkin->m_nCtlID);

	pFont = (CSPPropertyGridItemFont*)m_wndPropertyStatic.FindItem(IDS_STRING_STATIC_FONT);
	pFont->SetFont(pStaticSkin->m_fontStatic);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyStatic.FindItem(IDS_STRING_STATIC_AUTOFONT);
	pBool->SetBool(pStaticSkin->m_bAutoFont);

	pBool = (CSPPropertyGridItemBool*)m_wndPropertyStatic.FindItem(IDS_STRING_STATIC_TRANSPARENT);
	pBool->SetBool(pStaticSkin->m_bTransparent);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyStatic.FindItem(IDS_STRING_STATIC_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pStaticSkin->m_imageBack);
	pImage->SetValue(pStaticSkin->m_imageBack.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyStatic.FindItem(IDS_STRING_STATIC_COLORBACK);
	pColor->SetColor(pStaticSkin->m_colorBack);



}
void CPropertyView::CreatePropertyHeader()
{
	m_wndPropertyHeader.Create(CRect(0,0,0,0),this,IDC_PROPERTY_HEADERSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyHeader.AddCategory(_T("HeaderSkin"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_HEADER_NAME, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_HEADER_ID, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_HEADER_IMAGEBACK ));
	
	CSPPropertyGridItem* pItem = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_HEADER_ITEM, NULL ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_HEADER_ITEMNORMAL ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_HEADER_ITEMHOVER ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_HEADER_ITEMPRESSED ));

	pItem->Expand();
	pCategoryApplication->Expand();
}
void CPropertyView::SetHeaderSkin(CHeaderSkin *pHeaderSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyHeader.FindItem(IDS_STRING_HEADER_NAME);
	pItem->SetValue(pHeaderSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyHeader.FindItem(IDS_STRING_HEADER_ID);
	pNumber->SetNumber(pHeaderSkin->m_nCtlID);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyHeader.FindItem(IDS_STRING_HEADER_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pHeaderSkin->m_imageBackground);
	pImage->SetValue(pHeaderSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyHeader.FindItem(IDS_STRING_HEADER_ITEMNORMAL);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pHeaderSkin->m_imageItemNormal);
	pImage->SetValue(pHeaderSkin->m_imageItemNormal.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyHeader.FindItem(IDS_STRING_HEADER_ITEMHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pHeaderSkin->m_imageItemHover);
	pImage->SetValue(pHeaderSkin->m_imageItemHover.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyHeader.FindItem(IDS_STRING_HEADER_ITEMPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pHeaderSkin->m_imageItemPressed);
	pImage->SetValue(pHeaderSkin->m_imageItemPressed.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
}
void CPropertyView::CreatePropertySpin()
{
	m_wndPropertySpin.Create(CRect(0,0,0,0),this,IDC_PROPERTY_SPINSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertySpin.AddCategory(_T("SpinSkin"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_SPIN_ID, NULL ));

	CSPPropertyGridItem* pHorz = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_VERT, NULL ));
	CSPPropertyGridItem* pNormal = pHorz->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_NORMAL, NULL ));
	pNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_NORMALUP ));
	pNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_NORMALDOWN ));
	pNormal->Expand();
	CSPPropertyGridItem* pHover = pHorz->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_HOVER, NULL ));
	pHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_HOVERUP ));
	pHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_HOVERDOWN ));
	pHover->Expand();
	CSPPropertyGridItem* pPressed = pHorz->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_PRESSED, NULL ));
	pPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_PRESSEDUP ));
	pPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_PRESSEDDOWN ));
	pPressed->Expand();
	CSPPropertyGridItem* pDisabled = pHorz->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_DISABLED, NULL ));
	pDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_DISABLEDUP ));
	pDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_DISABLEDDOWN ));
	pDisabled->Expand();
	pHorz->Expand();

	CSPPropertyGridItem* pVert = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_HORZ, NULL ));
	pNormal = pVert->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_NORMAL, NULL ));
	pNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_NORMALLEFT ));
	pNormal->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_NORMALRIGHT ));
	pNormal->Expand();
	pHover = pVert->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_HOVER, NULL ));
	pHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_HOVERLEFT ));
	pHover->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_HOVERRIGHT ));
	pHover->Expand();
	pPressed = pVert->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_PRESSED, NULL ));
	pPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_PRESSEDLEFT ));
	pPressed->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_PRESSEDRIGHT ));
	pPressed->Expand();
	pDisabled = pVert->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SPIN_DISABLED, NULL ));
	pDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_DISABLEDLEFT ));
	pDisabled->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SPIN_DISABLEDRIGHT ));
	pDisabled->Expand();

	pVert->Expand();
	pCategoryApplication->Expand();

}
void CPropertyView::SetSpinSkin(CSpinSkin *pSpinSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_NAME);
	pItem->SetValue(pSpinSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_ID);
	pNumber->SetNumber(pSpinSkin->m_nCtlID);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_NORMALUP);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbVert[keSpinNormal].imageUp);
	pImage->SetValue(pSpinSkin->m_thumbVert[keSpinNormal].imageUp.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_NORMALDOWN);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbVert[keSpinNormal].imageDown);
	pImage->SetValue(pSpinSkin->m_thumbVert[keSpinNormal].imageDown.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_HOVERUP);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbVert[keSpinHover].imageUp);
	pImage->SetValue(pSpinSkin->m_thumbVert[keSpinHover].imageUp.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_HOVERDOWN);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbVert[keSpinHover].imageDown);
	pImage->SetValue(pSpinSkin->m_thumbVert[keSpinHover].imageDown.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_PRESSEDUP);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbVert[keSpinPressed].imageUp);
	pImage->SetValue(pSpinSkin->m_thumbVert[keSpinPressed].imageUp.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_PRESSEDDOWN);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbVert[keSpinPressed].imageDown);
	pImage->SetValue(pSpinSkin->m_thumbVert[keSpinPressed].imageDown.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_DISABLEDUP);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbVert[keSpinDisabled].imageUp);
	pImage->SetValue(pSpinSkin->m_thumbVert[keSpinDisabled].imageUp.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_DISABLEDDOWN);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbVert[keSpinDisabled].imageDown);
	pImage->SetValue(pSpinSkin->m_thumbVert[keSpinDisabled].imageDown.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;


	//////////////////////////////////////


	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_NORMALLEFT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbHort[keSpinNormal].imageUp);
	pImage->SetValue(pSpinSkin->m_thumbHort[keSpinNormal].imageUp.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_NORMALRIGHT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbHort[keSpinNormal].imageDown);
	pImage->SetValue(pSpinSkin->m_thumbHort[keSpinNormal].imageDown.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_HOVERLEFT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbHort[keSpinHover].imageUp);
	pImage->SetValue(pSpinSkin->m_thumbHort[keSpinHover].imageUp.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_HOVERRIGHT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbHort[keSpinHover].imageDown);
	pImage->SetValue(pSpinSkin->m_thumbHort[keSpinHover].imageDown.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_PRESSEDLEFT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbHort[keSpinPressed].imageUp);
	pImage->SetValue(pSpinSkin->m_thumbHort[keSpinPressed].imageUp.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_PRESSEDRIGHT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbHort[keSpinPressed].imageDown);
	pImage->SetValue(pSpinSkin->m_thumbHort[keSpinPressed].imageDown.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_DISABLEDLEFT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbHort[keSpinDisabled].imageUp);
	pImage->SetValue(pSpinSkin->m_thumbHort[keSpinDisabled].imageUp.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySpin.FindItem(IDS_STRING_SPIN_DISABLEDRIGHT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSpinSkin->m_thumbHort[keSpinDisabled].imageDown);
	pImage->SetValue(pSpinSkin->m_thumbHort[keSpinDisabled].imageDown.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;




}
void CPropertyView::CreatePropertyProgress()
{
	m_wndPropertyProgress.Create(CRect(0,0,0,0),this,IDC_PROPERTY_PROGRESSSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyProgress.AddCategory(_T("ProgressSkin"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_PROGRESS_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_PROGRESS_ID, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_PROGRESS_IMAGEBACK ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_PROGRESS_IMAGEPROGRESS ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_PROGRESS_COLORBACK ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_PROGRESS_COLORPROGRESS ));

	pCategoryApplication->Expand();
}
void CPropertyView::SetProgressSkin(CProgressSkin *pProgressSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyProgress.FindItem(IDS_STRING_PROGRESS_NAME);
	pItem->SetValue(pProgressSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyProgress.FindItem(IDS_STRING_PROGRESS_ID);
	pNumber->SetNumber(pProgressSkin->m_nCtlID);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyProgress.FindItem(IDS_STRING_PROGRESS_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pProgressSkin->m_imageBackground);
	pImage->SetValue(pProgressSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyProgress.FindItem(IDS_STRING_PROGRESS_IMAGEPROGRESS);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pProgressSkin->m_imageProgress);
	pImage->SetValue(pProgressSkin->m_imageProgress.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyProgress.FindItem(IDS_STRING_PROGRESS_COLORBACK);
	pColor->SetColor(pProgressSkin->m_colorBackground);

	pColor = (CSPPropertyGridItemColor*)m_wndPropertyProgress.FindItem(IDS_STRING_PROGRESS_COLORPROGRESS);
	pColor->SetColor(pProgressSkin->m_colorProgress);
}
void CPropertyView::CreatePropertyToolBar()
{
	m_wndPropertyToolBar.Create(CRect(0,0,0,0),this,IDC_PROPERTY_TOOLBARSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyToolBar.AddCategory(_T("ToolBarSkin"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_TOOLBAR_NAME, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_TOOLBAR_ID, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TOOLBAR_IMAGEBACK ));


	CSPPropertyGridItem* pItem = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_TOOLBAR_ITEM, NULL ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TOOLBAR_NORMAL ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TOOLBAR_HOVER ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TOOLBAR_PRESSED ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TOOLBAR_DISABLED ));

	pItem->Expand();
	pCategoryApplication->Expand();
}
void CPropertyView::SetToolBarSkin(CToolBarSkin *pToolbarSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyToolBar.FindItem(IDS_STRING_TOOLBAR_NAME);
	pItem->SetValue(pToolbarSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyToolBar.FindItem(IDS_STRING_TOOLBAR_ID);
	pNumber->SetNumber(pToolbarSkin->m_nCtlID);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyToolBar.FindItem(IDS_STRING_TOOLBAR_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pToolbarSkin->m_imageBackground);
	pImage->SetValue(pToolbarSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyToolBar.FindItem(IDS_STRING_TOOLBAR_NORMAL);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pToolbarSkin->m_imageItemNormal);
	pImage->SetValue(pToolbarSkin->m_imageItemNormal.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyToolBar.FindItem(IDS_STRING_TOOLBAR_HOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pToolbarSkin->m_imageItemHover);
	pImage->SetValue(pToolbarSkin->m_imageItemHover.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyToolBar.FindItem(IDS_STRING_TOOLBAR_PRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pToolbarSkin->m_imageItemPressed);
	pImage->SetValue(pToolbarSkin->m_imageItemPressed.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyToolBar.FindItem(IDS_STRING_TOOLBAR_DISABLED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pToolbarSkin->m_imageItemDisabled);
	pImage->SetValue(pToolbarSkin->m_imageItemDisabled.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

}
void CPropertyView::CreatePropertyStatusBar()
{
	m_wndPropertyStatusBar.Create(CRect(0,0,0,0),this,IDC_PROPERTY_STATUSBARSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyStatusBar.AddCategory(_T("StatusBarSkin"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_STATUSBAR_NAME, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_STATUSBAR_ID, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_STATUSBAR_IMAGEBACK ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_STATUSBAR_IMAGEITEM ));

	pCategoryApplication->Expand();
}
void CPropertyView::SetStatusSkin(CStatusBarSkin *pStatusbarSkin)
{

	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyStatusBar.FindItem(IDS_STRING_STATUSBAR_NAME);
	pItem->SetValue(pStatusbarSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyStatusBar.FindItem(IDS_STRING_STATUSBAR_ID);
	pNumber->SetNumber(pStatusbarSkin->m_nCtlID);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyStatusBar.FindItem(IDS_STRING_STATUSBAR_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pStatusbarSkin->m_imageBackground);
	pImage->SetValue(pStatusbarSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyStatusBar.FindItem(IDS_STRING_STATUSBAR_IMAGEITEM);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pStatusbarSkin->m_imageItem);
	pImage->SetValue(pStatusbarSkin->m_imageItem.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
}
void CPropertyView::CreatePropertyTab()
{
	m_wndPropertyTab.Create(CRect(0,0,0,0),this,IDC_PROPERTY_TABSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertyTab.AddCategory(_T("TabSkin"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_TAB_NAME, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_TAB_ID, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TAB_IMAGEBACK ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TAB_BORDER ));

	CSPPropertyGridItem* pItem = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_TAB, NULL ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TABNORMAL ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TABHOVER ));
	pItem->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_TABPRESSED ));

	pItem->Expand();
	pCategoryApplication->Expand();
}
void CPropertyView::SetTabSkin(CTabSkin *pTabSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertyTab.FindItem(IDS_STRING_TAB_NAME);
	pItem->SetValue(pTabSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertyTab.FindItem(IDS_STRING_TAB_ID);
	pNumber->SetNumber(pTabSkin->m_nCtlID);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyTab.FindItem(IDS_STRING_TAB_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pTabSkin->m_imageBackground);
	pImage->SetValue(pTabSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyTab.FindItem(IDS_STRING_TAB_BORDER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pTabSkin->m_imageBorder);
	pImage->SetValue(pTabSkin->m_imageBorder.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyTab.FindItem(IDS_STRING_TABNORMAL);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pTabSkin->m_imageTabNormal);
	pImage->SetValue(pTabSkin->m_imageTabNormal.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyTab.FindItem(IDS_STRING_TABHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pTabSkin->m_imageTabHover);
	pImage->SetValue(pTabSkin->m_imageTabHover.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyTab.FindItem(IDS_STRING_TABPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pTabSkin->m_imageTabPressed);
	pImage->SetValue(pTabSkin->m_imageTabPressed.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
}
void CPropertyView::CreatePropertySlider()
{
	m_wndPropertySlider.Create(CRect(0,0,0,0),this,IDC_PROPERTY_SLIDERSKIN);
	CSPPropertyGridItem* pCategoryApplication = m_wndPropertySlider.AddCategory(_T("SliderSkin"));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SLIDER_NAME, NULL ));

	pCategoryApplication->AddChildItem(new CSPPropertyGridItemNumber(IDS_STRING_SLIDER_ID, NULL ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SLIDER_IMAGEBACK ));
	pCategoryApplication->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SLIDER_IMAGECHANNEL ));
	
	CSPPropertyGridItem* pThumb = pCategoryApplication->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SLIDER_THUMB, NULL ));
	pThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SLIDER_THUMBVERT ));
	pThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SLIDER_THUMBHORZ ));

	pThumb->Expand();
	pCategoryApplication->Expand();
}
void CPropertyView::SetSliderSkin(CSliderSkin *pSliderSkin)
{
	CSPPropertyGridItem *pItem = NULL;
	CSPPropertyGridItemNumber *pNumber = NULL;
	CSPPropertyGridItemImage * pImage = NULL;
	CSPPropertyGridItemText	 *pText = NULL;
	CSPPropertyGridItemFont *pFont = NULL;
	CSPPropertyGridItemBool *pBool = NULL;
	CSPPropertyGridItemColor *pColor = NULL;

	pItem = (CSPPropertyGridItem*)m_wndPropertySlider.FindItem(IDS_STRING_SLIDER_NAME);
	pItem->SetValue(pSliderSkin->m_strName);

	pNumber = (CSPPropertyGridItemNumber*)m_wndPropertySlider.FindItem(IDS_STRING_SLIDER_ID);
	pNumber->SetNumber(pSliderSkin->m_nCtlID);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySlider.FindItem(IDS_STRING_SLIDER_IMAGEBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSliderSkin->m_imageBackground);
	pImage->SetValue(pSliderSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySlider.FindItem(IDS_STRING_SLIDER_IMAGECHANNEL);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSliderSkin->m_imageChannel);
	pImage->SetValue(pSliderSkin->m_imageChannel.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySlider.FindItem(IDS_STRING_SLIDER_THUMBVERT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSliderSkin->m_imageThumbVert);
	pImage->SetValue(pSliderSkin->m_imageThumbVert.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertySlider.FindItem(IDS_STRING_SLIDER_THUMBHORZ);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pSliderSkin->m_imageThumbHorz);
	pImage->SetValue(pSliderSkin->m_imageThumbHorz.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
}
void CPropertyView::CreatePropertyWindow()
{
	m_wndPropertyWindow.Create(CRect(0,0,0,0),this,IDC_PROPERTY_WINDOWSKIN);

	CSPPropertyGridItem* pCategoryMainFrame = m_wndPropertyWindow.AddCategory(_T("MainFrame"));
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemRect(IDS_STRING_WINDOW_POSITION, CRect(0,0,0,0) ));
	CSPPropertyGridItem* pCategoryActiveFrame;
	pCategoryActiveFrame = pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_WINDOW_ACTIVEFRAME));
	pCategoryActiveFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_ACTIVEFRAMELEFT));
	pCategoryActiveFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_ACTIVEFRAMETOP));
	pCategoryActiveFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_ACTIVEFRAMERIGHT));
	pCategoryActiveFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_ACTIVEFRAMEBOTTOM));
	pCategoryActiveFrame->Expand();

	CSPPropertyGridItem* pCategoryInactiveFrame;
	pCategoryInactiveFrame = pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_WINDOW_INACTIVEFRAME));
	pCategoryInactiveFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_INACTIVEFRAMELEFT));
	pCategoryInactiveFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_INACTIVEFRAMETOP));
	pCategoryInactiveFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_INACTIVEFRAMERIGHT));
	pCategoryInactiveFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_INACTIVEFRAMEBOTTOM));
	pCategoryInactiveFrame->Expand();

	CSPPropertyGridItem* pCategoryMenubar;
	LOGFONT font;
	::ZeroMemory(&font,sizeof(font));
	pCategoryMenubar = pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_WINDOW_MENUBAR));
	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemFont(IDS_STRING_WINDOW_MENUBARFONT,font));
	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_MENUBARBACK));

	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_WINDOW_MENUBARACTIVECOLOR));
	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_WINDOW_MENUBARPRESSEDCOLOR));

	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_MENUBARIMAGEACITVE));
	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_MENUBARIMAGEPRESSED));

	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_WINDOW_MENUBARGRAYTEXT));
	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_WINDOW_MENUBARNORMALTEXT));
	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_WINDOW_MENUBARPRESSEDTEXT));
	pCategoryMenubar->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_WINDOW_MENUBARACTIVETEXTCOLOR));
	pCategoryMenubar->Expand();
	
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemBool(IDS_STRING_WINDOW_SKINCLIENT));
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINDOW_BACKGROUND));
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemColor(IDS_STRING_WINDOW_BACKCOLOR));
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemRect(IDS_STRING_WINDOW_MARGIN,CRect(0,0,0,0)));
	pCategoryMainFrame->Expand();
}
void CPropertyView::SetWindowSkin(CWindowSkin *pWinSkin)
{
	if(!pWinSkin)
		return;
	CSPPropertyGridItemRect* pItemRect = (CSPPropertyGridItemRect*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_POSITION);
	pItemRect->SetRect(pWinSkin->GetPosition());
	pItemRect = (CSPPropertyGridItemRect*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MARGIN);
	pItemRect->SetRect(pWinSkin->GetMargins());


	CSPPropertyGridItemImage * pImage = NULL;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_ACTIVEFRAMELEFT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_frameActive.imageLeft);
	pImage->SetValue(pWinSkin->m_frameActive.imageLeft.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_ACTIVEFRAMETOP);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_frameActive.imageTop);
	pImage->SetValue(pWinSkin->m_frameActive.imageTop.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_ACTIVEFRAMERIGHT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_frameActive.imageRight);
	pImage->SetValue(pWinSkin->m_frameActive.imageRight.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_ACTIVEFRAMEBOTTOM);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_frameActive.imageBottom);
	pImage->SetValue(pWinSkin->m_frameActive.imageBottom.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;


	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_INACTIVEFRAMELEFT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_frameInactive.imageLeft);
	pImage->SetValue(pWinSkin->m_frameInactive.imageLeft.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_INACTIVEFRAMETOP);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_frameInactive.imageTop);
	pImage->SetValue(pWinSkin->m_frameInactive.imageTop.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_INACTIVEFRAMERIGHT);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_frameInactive.imageRight);
	pImage->SetValue(pWinSkin->m_frameInactive.imageRight.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_INACTIVEFRAMEBOTTOM);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_frameInactive.imageBottom);
	pImage->SetValue(pWinSkin->m_frameInactive.imageBottom.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	CSPPropertyGridItemFont *pFont = NULL;
	pFont = (CSPPropertyGridItemFont*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARFONT);
	pFont->SetFont(pWinSkin->m_skinMenuBar.fontMenu);

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARBACK);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_skinMenuBar.imageBackground);
	pImage->SetValue(pWinSkin->m_skinMenuBar.imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARIMAGEACITVE);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_skinMenuBar.imageActive);
	pImage->SetValue(pWinSkin->m_skinMenuBar.imageActive.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARIMAGEPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pWinSkin->m_skinMenuBar.imagePressed);
	pImage->SetValue(pWinSkin->m_skinMenuBar.imagePressed.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	CSPPropertyGridItemColor *pColor = NULL;
	pColor = (CSPPropertyGridItemColor*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARACTIVECOLOR);
	pColor->SetColor(pWinSkin->m_skinMenuBar.colorActive);
	pColor = (CSPPropertyGridItemColor*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARPRESSEDCOLOR);
	pColor->SetColor(pWinSkin->m_skinMenuBar.colorPressed);
	pColor = (CSPPropertyGridItemColor*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARGRAYTEXT);
	pColor->SetColor(pWinSkin->m_skinMenuBar.colorGrayText);
	pColor = (CSPPropertyGridItemColor*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARNORMALTEXT);
	pColor->SetColor(pWinSkin->m_skinMenuBar.colorNormalText);
	pColor = (CSPPropertyGridItemColor*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARPRESSEDTEXT);
	pColor->SetColor(pWinSkin->m_skinMenuBar.colorPressedText);
	pColor = (CSPPropertyGridItemColor*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_MENUBARACTIVETEXTCOLOR);
	pColor->SetColor(pWinSkin->m_skinMenuBar.colorActiveText);



	
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_BACKGROUND);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pWinSkin->m_imageBackground);
	pImage->SetValue(pWinSkin->m_imageBackground.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	
	pColor = (CSPPropertyGridItemColor*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_BACKCOLOR);
	pColor->SetColor(pWinSkin->m_colorBack);
	CSPPropertyGridItemBool *pBool = NULL;
	pBool = (CSPPropertyGridItemBool*)m_wndPropertyWindow.FindItem(IDS_STRING_WINDOW_SKINCLIENT);
	pBool->SetBool(pWinSkin->m_bSkinClient);


}
void CPropertyView::CreatePropertyWindButton()
{
	m_wndPropertyWinButton.Create(CRect(0,0,0,0),this,IDC_PROPERTY_WINBUTTONSKIN);
	CSPPropertyGridItem* pCategoryMainFrame = m_wndPropertyWinButton.AddCategory(_T("WinButton"));

	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_WINBUTTON_NAME));
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemRect(IDS_STRING_WINBUTTON_VISUAL, CRect(0,0,0,0) ));
	CSPPropertyGridItem* pButtonType = pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_WINBUTTON_BUTTONTYPE));
	for(int i = 0 ; i < keButtonTypeSize ; i++)
		pButtonType->GetConstraints()->AddConstraint(winButtonType[i]);
	pButtonType->GetConstraints()->SetCurrent(0);

	pButtonType->SetFlags(SPGridItemHasComboButton|SPGridItemHasEdit);
	pButtonType->SetConstraintEdit(FALSE);


	pButtonType = pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_WINBUTTON_ANCHOR));
	for(int i = 0 ; i < keAnchorSize ; i++)
		pButtonType->GetConstraints()->AddConstraint(AnchorType[i]);
	pButtonType->GetConstraints()->SetCurrent(0);

	pButtonType->SetFlags(SPGridItemHasComboButton|SPGridItemHasEdit);
	pButtonType->SetConstraintEdit(FALSE);

	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINBUTTON_STATENORMAL));
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINBUTTON_STATEHOVER));
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINBUTTON_STATEPRESSED));
	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_WINBUTTON_STATEDIABLED));

	pCategoryMainFrame->Expand();
}

void CPropertyView::SetWinButtonSkin(CWinButtonSkin *pWinButton)
{
	CSPPropertyGridItem *pItem = NULL;
	pItem = m_wndPropertyWinButton.FindItem(IDS_STRING_WINBUTTON_NAME);
	if(pItem)
		pItem->SetValue(pWinButton->GetName());

	pItem = m_wndPropertyWinButton.FindItem(IDS_STRING_WINBUTTON_VISUAL);
	if(pItem)
		((CSPPropertyGridItemRect*)pItem)->SetRect(pWinButton->GetPosition());

	pItem = m_wndPropertyWinButton.FindItem(IDS_STRING_WINBUTTON_BUTTONTYPE);
	if(pItem)
	{
		pItem->SetValue(winButtonType[pWinButton->GetWinButtonType()]);
		pItem->GetConstraints()->SetCurrent(pWinButton->GetWinButtonType());
	}
	pItem = m_wndPropertyWinButton.FindItem(IDS_STRING_WINBUTTON_ANCHOR);
	if(pItem)
	{
		pItem->SetValue(AnchorType[pWinButton->GetAnchors()]);
		pItem->GetConstraints()->SetCurrent(pWinButton->GetAnchors());
	}

	CSPPropertyGridItemImage * pImage = NULL;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWinButton.FindItem(IDS_STRING_WINBUTTON_STATENORMAL);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pWinButton->m_iamgeButton[0]);
	pImage->SetValue(pWinButton->m_iamgeButton[0].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWinButton.FindItem(IDS_STRING_WINBUTTON_STATEHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pWinButton->m_iamgeButton[1]);
	pImage->SetValue(pWinButton->m_iamgeButton[1].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWinButton.FindItem(IDS_STRING_WINBUTTON_STATEPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pWinButton->m_iamgeButton[2]);
	pImage->SetValue(pWinButton->m_iamgeButton[2].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyWinButton.FindItem(IDS_STRING_WINBUTTON_STATEDIABLED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pWinButton->m_iamgeButton[3]);
	pImage->SetValue(pWinButton->m_iamgeButton[3].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

}
void CPropertyView::CreatePropertyScrollBar()
{
	m_wndPropertyScrollBar.Create(CRect(0,0,0,0),this,IDC_PROPERTY_SCROLLBARSKIN);
	CSPPropertyGridItem* pCategoryMainFrame = m_wndPropertyScrollBar.AddCategory(_T("ScrollBar"));

	pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLL_NAME));

	CSPPropertyGridItem* pHScrollBar = pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLLBAR_HOR));

	CSPPropertyGridItem *pHArrowLeft = pHScrollBar->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLLBAR_HARROWLEFT));
	pHArrowLeft->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HARROWLEFTNOR));
	pHArrowLeft->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HARROWLEFTHOVER));
	pHArrowLeft->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HARROWLEFTPRESSED));
	pHArrowLeft->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HARROWLEFTDISABLED));
	pHArrowLeft->Expand();

	CSPPropertyGridItem *pHArrowRight = pHScrollBar->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLLBAR_HARROWRight));
	pHArrowRight->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HARROWRIGHTNOR));
	pHArrowRight->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HARROWRIGHTHOVER));
	pHArrowRight->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HARROWRIGHTPRESSED));
	pHArrowRight->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HARROWRIGHTDISABLED));
	pHArrowRight->Expand();

	CSPPropertyGridItem *pHArrowThumb =  pHScrollBar->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLLBAR_HTHUMB));
	pHArrowThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HTHUMBNOR));
	pHArrowThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HTHUMBHOVER));
	pHArrowThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HTHUMBPRESSED));
	pHArrowThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HTHUMBDISABLED));
	pHArrowThumb->Expand();


	pHScrollBar->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_HBACKGROUND));

	pHScrollBar->Expand();


	CSPPropertyGridItem* pVScrollBar = pCategoryMainFrame->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLLBAR_VER));
	CSPPropertyGridItem *pVArrowTop = pVScrollBar->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLLBAR_VARROWTOP));
	pVArrowTop->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VARROWTOPNORMAL));
	pVArrowTop->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VARROWTOPHOVER));
	pVArrowTop->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VARROWTOPPRESSED));
	pVArrowTop->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VARROWTOPDISABLED));
	pVArrowTop->Expand();

	CSPPropertyGridItem *pVArrowBottom = pVScrollBar->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLLBAR_VARROWBOTTOM));
	pVArrowBottom->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VARROWBOOTOMNORMAL));
	pVArrowBottom->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VARROWBOOTOMHOVER));
	pVArrowBottom->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VARROWBOOTOMPRESSED));
	pVArrowBottom->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VARROWBOOTOMDISABLED));
	pVArrowBottom->Expand();

	CSPPropertyGridItem *pVArrowThumb =  pVScrollBar->AddChildItem(new CSPPropertyGridItem(IDS_STRING_SCROLLBAR_VTHUMB));
	pVArrowThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VTHUMBNORMAL));
	pVArrowThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VTHUMBHOVER));
	pVArrowThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VTHUMBPRESSED));
	pVArrowThumb->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VTHUMBDISABLED));
	pVArrowThumb->Expand();


	pVScrollBar->AddChildItem(new CSPPropertyGridItemImage(IDS_STRING_SCROLLBAR_VBACKGROUND));

	pVScrollBar->Expand();


	pCategoryMainFrame->Expand();

}

void CPropertyView::SetScroolBarSkin(CScrollBarSkin *pScrollBar)
{
	CSPPropertyGridItem *pItem = NULL;
	pItem = m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLL_NAME);
	if(pItem)
		pItem->SetValue(pScrollBar->GetName());


	CSPPropertyGridItemImage * pImage = NULL;

	//hor left
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HARROWLEFTNOR);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinHScrollBar.imageArrow1[0]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageArrow1[0].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HARROWLEFTHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinHScrollBar.imageArrow1[1]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageArrow1[1].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HARROWLEFTPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinHScrollBar.imageArrow1[2]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageArrow1[2].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HARROWLEFTDISABLED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinHScrollBar.imageArrow1[3]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageArrow1[3].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	//hor right
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HARROWRIGHTNOR);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinHScrollBar.imageArrow2[0]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageArrow2[0].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HARROWRIGHTHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinHScrollBar.imageArrow2[1]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageArrow2[1].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HARROWRIGHTPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinHScrollBar.imageArrow2[2]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageArrow2[2].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HARROWRIGHTDISABLED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinHScrollBar.imageArrow2[3]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageArrow2[3].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	//hor thumb
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HTHUMBNOR);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinHScrollBar.imageThurmb[0]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageThurmb[0].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HTHUMBHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinHScrollBar.imageThurmb[1]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageThurmb[1].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HTHUMBPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinHScrollBar.imageThurmb[2]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageThurmb[2].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HTHUMBDISABLED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinHScrollBar.imageThurmb[3]);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageThurmb[3].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	//hor background
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_HBACKGROUND);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinHScrollBar.imageScrollBar);
	pImage->SetValue(pScrollBar->m_skinHScrollBar.imageScrollBar.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;


	//ver top
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VARROWTOPNORMAL);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinVScrollBar.imageArrow1[0]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageArrow1[0].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VARROWTOPHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinVScrollBar.imageArrow1[1]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageArrow1[1].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VARROWTOPPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinVScrollBar.imageArrow1[2]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageArrow1[2].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VARROWTOPDISABLED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinVScrollBar.imageArrow1[3]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageArrow1[3].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	//ver bottom
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VARROWBOOTOMNORMAL);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinVScrollBar.imageArrow2[0]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageArrow2[0].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VARROWBOOTOMHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinVScrollBar.imageArrow2[1]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageArrow2[1].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VARROWBOOTOMPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinVScrollBar.imageArrow2[2]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageArrow2[2].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VARROWBOOTOMDISABLED);
	pImage->m_dlgImage.m_wndImage.m_pImageRect = &(pScrollBar->m_skinVScrollBar.imageArrow2[3]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageArrow2[3].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = FALSE;


	//ver thumb
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VTHUMBNORMAL);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinVScrollBar.imageThurmb[0]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageThurmb[0].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VTHUMBHOVER);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinVScrollBar.imageThurmb[1]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageThurmb[1].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VTHUMBPRESSED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinVScrollBar.imageThurmb[2]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageThurmb[2].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VTHUMBDISABLED);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinVScrollBar.imageThurmb[3]);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageThurmb[3].strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

	//hor background
	pImage = (CSPPropertyGridItemImage*)m_wndPropertyScrollBar.FindItem(IDS_STRING_SCROLLBAR_VBACKGROUND);
	pImage->m_dlgImage.m_wndImage.m_pImageSection = &(pScrollBar->m_skinVScrollBar.imageScrollBar);
	pImage->SetValue(pScrollBar->m_skinVScrollBar.imageScrollBar.strImageName);
	pImage->m_dlgImage.m_wndImage.m_bSection = TRUE;

}
// CPropertyView drawing

void CPropertyView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
	

	m_wndPropertyWindow.Refresh();
	m_wndPropertyApplication.Refresh();
	m_wndPropertyWinButton.Refresh();
	m_wndPropertyScrollBar.Refresh();
	m_wndPropertyButton.Refresh();
	m_wndPropertyRadio.Refresh();
	m_wndPropertyCheckBox.Refresh();
	m_wndPropertyGroupBox.Refresh();
	m_wndPropertyEdit.Refresh();
	m_wndPropertyStatic.Refresh();
	m_wndPropertyCheckBox.Refresh();
}
	

// CPropertyView message handlers

int CPropertyView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	CRect rtClient;
	GetClientRect(&rtClient);
	//CreatePropertyApplication(NULL);

	
	CreatePropertyWindow();
	CreatePropertyApplication(NULL);
	CreatePropertyWindButton();
	CreatePropertyScrollBar();
	CreatePropertyButton();
	CreatePropertyRadio();
	CreatePropertyCheckBox();
	CreatePropertyGroupBox();
	CreatePropertyEdit();
	CreatePropertyComboBox();
	CreatePropertyStatic();
	CreatePropertyHeader();
	CreatePropertySpin();
	CreatePropertyToolBar();
	CreatePropertyStatusBar();
	CreatePropertySlider();
	CreatePropertyListBox();
	CreatePropertyPopupMenu();
	CreatePropertyProgress();
	CreatePropertyTab();
	return 0;
}

void CPropertyView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
	

	m_wndPropertyWindow.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyApplication.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyWinButton.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyScrollBar.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyButton.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyRadio.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyCheckBox.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyGroupBox.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyEdit.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyStatic.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyComboBox.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyListBox.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyPopupMenu.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertySpin.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyProgress.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyHeader.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertySlider.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyTab.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyStatusBar.MoveWindow(0,0,cx,cy,TRUE);
	m_wndPropertyToolBar.MoveWindow(0,0,cx,cy,TRUE);
//	OnUpdate(NULL,0,NULL);

}

void CPropertyView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{

	m_wndPropertyWindow.ShowWindow(SW_HIDE);
	m_wndPropertyApplication.ShowWindow(SW_HIDE);
	m_wndPropertyWinButton.ShowWindow(SW_HIDE);
	m_wndPropertyScrollBar.ShowWindow(SW_HIDE);
	m_wndPropertyButton.ShowWindow(SW_HIDE);
	m_wndPropertyRadio.ShowWindow(SW_HIDE);
	m_wndPropertyCheckBox.ShowWindow(SW_HIDE);
	m_wndPropertyGroupBox.ShowWindow(SW_HIDE);
	m_wndPropertyEdit.ShowWindow(SW_HIDE);
	m_wndPropertyStatic.ShowWindow(SW_HIDE);
	m_wndPropertyComboBox.ShowWindow(SW_HIDE);
	m_wndPropertyListBox.ShowWindow(SW_HIDE);
	m_wndPropertyPopupMenu.ShowWindow(SW_HIDE);
	m_wndPropertySpin.ShowWindow(SW_HIDE);
	m_wndPropertyProgress.ShowWindow(SW_HIDE);
	m_wndPropertySlider.ShowWindow(SW_HIDE);
	m_wndPropertyHeader.ShowWindow(SW_HIDE);
	m_wndPropertyTab.ShowWindow(SW_HIDE);
	m_wndPropertyStatusBar.ShowWindow(SW_HIDE);
	m_wndPropertyToolBar.ShowWindow(SW_HIDE);
	if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINS)
	{
		m_wndPropertyApplication.ShowWindow(SW_SHOW);
		SetApplication(&(GetDocument()->m_pSkin->m_Application));
		m_wndPropertyApplication.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINDOW)
	{
		m_wndPropertyWindow.ShowWindow(SW_SHOW);
		SetWindowSkin((CWindowSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyWindow.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINBUTTON)
	{
		m_wndPropertyWinButton.ShowWindow(SW_SHOW);
		SetWinButtonSkin(GetDocument()->m_pCurrentWinButton);
		m_wndPropertyWinButton.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SCROLLBAR)
	{
		m_wndPropertyScrollBar.ShowWindow(SW_SHOW);
		SetScroolBarSkin((CScrollBarSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyScrollBar.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_BUTTON)
	{
		m_wndPropertyButton.ShowWindow(SW_SHOW);
		SetButtonSkin((CButtonSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyButton.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_STATIC)
	{
		m_wndPropertyStatic.ShowWindow(SW_SHOW);
		SetStaticSkin((CStaticSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyStatic.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_RADIO)
	{
		m_wndPropertyRadio.ShowWindow(SW_SHOW);
		
		SetRadioSkin((CRadioSkin*)GetDocument()->m_pCurrentObject);

		m_wndPropertyRadio.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_CHECK)
	{
		m_wndPropertyCheckBox.ShowWindow(SW_SHOW);

		SetCheckBoxSkin((CCheckBoxSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyCheckBox.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_GROUP)
	{
		m_wndPropertyGroupBox.ShowWindow(SW_SHOW);

		SetGroupBoxSkin((CGroupBoxSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyGroupBox.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_EDIT)
	{
		m_wndPropertyEdit.ShowWindow(SW_SHOW);
		SetEditSkin((CEditSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyEdit.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_COMBOX)
	{
		m_wndPropertyComboBox.ShowWindow(SW_SHOW);
		SetComboxSkin((CComboBoxSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyComboBox.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_LISTBOX)
	{
		m_wndPropertyListBox.ShowWindow(SW_SHOW);
		SetListBoxSkin((CListBoxSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyListBox.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_POPUPMENU)
	{
		m_wndPropertyPopupMenu.ShowWindow(SW_SHOW);
		SetPopupMenuSkin((CPopupMenuSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyPopupMenu.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SPIN)
	{
		m_wndPropertySpin.ShowWindow(SW_SHOW);
		SetSpinSkin((CSpinSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertySpin.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_PROGRESS)
	{
		m_wndPropertyProgress.ShowWindow(SW_SHOW);
		SetProgressSkin((CProgressSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyProgress.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_HEADER)
	{
		m_wndPropertyHeader.ShowWindow(SW_SHOW);
		SetHeaderSkin((CHeaderSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyProgress.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SLIDER)
	{
		m_wndPropertySlider.ShowWindow(SW_SHOW);
		SetSliderSkin((CSliderSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyProgress.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_TAB)
	{
		m_wndPropertyTab.ShowWindow(SW_SHOW);
		SetTabSkin((CTabSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyTab.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_STATUSBAR)
	{
		m_wndPropertyStatusBar.ShowWindow(SW_SHOW);
		SetStatusSkin((CStatusBarSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyStatusBar.Refresh();
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_TOOLBAR)
	{
		m_wndPropertyToolBar.ShowWindow(SW_SHOW);
		SetToolBarSkin((CToolBarSkin*)GetDocument()->m_pCurrentObject);
		m_wndPropertyToolBar.Refresh();
	}
}
LRESULT CPropertyView::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == SP_PGN_ITEMVALUE_CHANGED)
	{
		CSPPropertyGridItem* pItem = (CSPPropertyGridItem*)lParam;
		if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINS)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_APPLICATION: 
				strcpy(GetDocument()->m_pSkin->m_Application.strApplication  ,pItem->GetValue()); break;
			case IDS_STRING_APP_AUTHOR: 
				strcpy(GetDocument()->m_pSkin->m_Application.strAuthor  ,pItem->GetValue());break;
			case IDS_STRING_APP_EMAIL:
				strcpy(GetDocument()->m_pSkin->m_Application.strEmail  ,pItem->GetValue());break;
			case IDS_STRING_APP_DESCRIPTION:
				strcpy(GetDocument()->m_pSkin->m_Application.strDescription  ,pItem->GetValue());break;
			case IDS_STRING_APP_HOMEPAGE:
				strcpy(GetDocument()->m_pSkin->m_Application.strHomepage  ,pItem->GetValue());break;
			case IDS_STRING_APP_USETRANS:
				GetDocument()->m_pSkin->m_Application.bUseTrans = ((CSPPropertyGridItemBool*)pItem)->GetBool();break;
			case IDS_STRING_APP_TRANSCOLOR:
				GetDocument()->m_pSkin->m_Application.colorTransparent = ((CSPPropertyGridItemColor*)pItem)->GetColor();break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINDOW)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_WINDOW_ACTIVEFRAMELEFT: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_rtMargins.left = pWin->m_frameActive.imageLeft.rtImagePos.Width();
					SetWindowSkin(pWin);
				}				
				break;
			case IDS_STRING_WINDOW_ACTIVEFRAMERIGHT: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_rtMargins.right = pWin->m_frameActive.imageRight.rtImagePos.Width();
					SetWindowSkin(pWin);
				}				
				break;
			case IDS_STRING_WINDOW_ACTIVEFRAMETOP: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_rtMargins.top = pWin->m_frameActive.imageTop.rtImagePos.Height();
					SetWindowSkin(pWin);
				}				
				break;
			case IDS_STRING_WINDOW_ACTIVEFRAMEBOTTOM: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_rtMargins.bottom = pWin->m_frameActive.imageBottom.rtImagePos.Height();
					SetWindowSkin(pWin);
				}				
				break;

			case IDS_STRING_WINDOW_POSITION: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->SetPosition(((CSPPropertyGridItemRect*)pItem)->GetRect());
				}				
				break;
			case IDS_STRING_WINDOW_MARGIN: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->SetMargins(((CSPPropertyGridItemRect*)pItem)->GetRect());
				}				
				break;
			case IDS_STRING_WINDOW_SKINCLIENT: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_bSkinClient = (((CSPPropertyGridItemBool*)pItem)->GetBool());
				}				
				break;
			case IDS_STRING_WINDOW_MENUBARFONT: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					 ((CSPPropertyGridItemFont*)pItem)->GetFont(&(pWin->m_skinMenuBar.fontMenu));
				}				
				break;
			case IDS_STRING_WINDOW_MENUBARACTIVECOLOR: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_skinMenuBar.colorActive = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}				
				break;
			case IDS_STRING_WINDOW_MENUBARPRESSEDCOLOR: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_skinMenuBar.colorPressed = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			case IDS_STRING_WINDOW_MENUBARGRAYTEXT: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_skinMenuBar.colorGrayText = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			case IDS_STRING_WINDOW_MENUBARNORMALTEXT: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_skinMenuBar.colorNormalText = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			case IDS_STRING_WINDOW_MENUBARPRESSEDTEXT: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_skinMenuBar.colorPressedText = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			case IDS_STRING_WINDOW_MENUBARACTIVETEXTCOLOR: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_skinMenuBar.colorActiveText = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			case IDS_STRING_WINDOW_BACKCOLOR: 
				{
					CWindowSkin *pWin = (CWindowSkin*)GetDocument()->m_pCurrentObject;
					pWin->m_colorBack = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINBUTTON)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_WINBUTTON_NAME: 
				GetDocument()->m_pCurrentWinButton->SetName(pItem->GetValue()); break;
			case IDS_STRING_WINBUTTON_VISUAL: 
				GetDocument()->m_pCurrentWinButton->SetPosition(((CSPPropertyGridItemRect*)pItem)->GetRect()); break;
			case IDS_STRING_WINBUTTON_BUTTONTYPE: 
				GetDocument()->m_pCurrentWinButton->SetWinButtonType(pItem->GetConstraints()->GetCurrent()); break;
			case IDS_STRING_WINBUTTON_ANCHOR: 
				GetDocument()->m_pCurrentWinButton->SetAnchors(pItem->GetConstraints()->GetCurrent()); break;
			case IDS_STRING_WINBUTTON_STATENORMAL:
			/*case IDS_STRING_WINBUTTON_STATEHOVER:
			case IDS_STRING_WINBUTTON_STATEPRESSED:
			case IDS_STRING_WINBUTTON_STATEDIABLED:*/
				{
					CWinButtonSkin *pWinButton = GetDocument()->m_pCurrentWinButton;
					CRect rt = pWinButton->m_iamgeButton[keWinButtonNormal].rtImagePos;
					pWinButton->SetPosition(CRect(0,0,rt.Width(),rt.Height()));
				}
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_BUTTON)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_BUTTON_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_BUTTON_ID: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			case IDS_STRING_BUTTON_FONT: 
				{
					CButtonSkin *pButton = (CButtonSkin*)GetDocument()->m_pCurrentObject;
					((CSPPropertyGridItemFont*)pItem)->GetFont(&(pButton->m_fontButton));
				}
				break;
			case IDS_STRING_BUTTON_AUTOFONT:
				{
					CButtonSkin *pButton = (CButtonSkin*)GetDocument()->m_pCurrentObject;
					pButton->m_bAutoFont =  ((CSPPropertyGridItemBool*)pItem)->GetBool();

				}
				break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SCROLLBAR)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_SCROLL_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_CHECK)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_CHECKBOX_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_CHECKBOX_ID: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			case IDS_STRING_CHECKBOX_FONT: 
				{
					CCheckBoxSkin *pCheck = (CCheckBoxSkin*)GetDocument()->m_pCurrentObject;
					((CSPPropertyGridItemFont*)pItem)->GetFont(&(pCheck->m_fontCheckBox));
				}
				break;
			case IDS_STRING_CHECKBOX_AUTOFONT:
				{
					CCheckBoxSkin *pCheck = (CCheckBoxSkin*)GetDocument()->m_pCurrentObject;
					pCheck->m_bAutoFont =  ((CSPPropertyGridItemBool*)pItem)->GetBool();
					
				}
				break;
			case IDS_STRING_CHECKBOX_COLORBACK: 
				{
					CCheckBoxSkin *pCheck = (CCheckBoxSkin*)GetDocument()->m_pCurrentObject;
					pCheck->m_colorBackground = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_RADIO)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_RADIO_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_RADIO_ID: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			case IDS_STRING_RADIO_FONT: 
				{
					CCheckBoxSkin *pCheck = (CCheckBoxSkin*)GetDocument()->m_pCurrentObject;
					((CSPPropertyGridItemFont*)pItem)->GetFont(&(pCheck->m_fontCheckBox));
				}
				break;
			case IDS_STRING_RADIO_AUTOFONT:
				{
					CCheckBoxSkin *pCheck = (CCheckBoxSkin*)GetDocument()->m_pCurrentObject;
					pCheck->m_bAutoFont =  ((CSPPropertyGridItemBool*)pItem)->GetBool();

				}
				break;
			case IDS_STRING_RADIO_COLORBACK: 
				{
					CRadioSkin *pRadio = (CRadioSkin*)GetDocument()->m_pCurrentObject;
					pRadio->m_colorBackground = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_GROUP)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_GROUPBOX_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_GROUPBOX_ID: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			case IDS_STRING_GROUPBOX_FONT: 
				{
					CGroupBoxSkin *pGroup = (CGroupBoxSkin*)GetDocument()->m_pCurrentObject;
					((CSPPropertyGridItemFont*)pItem)->GetFont(&(pGroup->m_fontGroupBox));
				}
				break;
			case IDS_STRING_GROUPBOX_AUTOFONT:
				{
					CGroupBoxSkin *pGroup = (CGroupBoxSkin*)GetDocument()->m_pCurrentObject;
					pGroup->m_bAutoFont =  ((CSPPropertyGridItemBool*)pItem)->GetBool();

				}
				break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_STATIC)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_STATIC_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_STATIC_ID: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			case IDS_STRING_STATIC_FONT: 
				{
					CStaticSkin *pStatic = (CStaticSkin*)GetDocument()->m_pCurrentObject;
					((CSPPropertyGridItemFont*)pItem)->GetFont(&(pStatic->m_fontStatic));
				}
				break;
			case IDS_STRING_STATIC_AUTOFONT:
				{
					CStaticSkin *pStatic = (CStaticSkin*)GetDocument()->m_pCurrentObject;
					pStatic->m_bAutoFont =  ((CSPPropertyGridItemBool*)pItem)->GetBool();

				}
				break;
			case IDS_STRING_STATIC_TRANSPARENT:
				{
					CStaticSkin *pStatic = (CStaticSkin*)GetDocument()->m_pCurrentObject;
					pStatic->m_bTransparent =  ((CSPPropertyGridItemBool*)pItem)->GetBool();

				}
				break;
			case IDS_STRING_STATIC_COLORBACK: 
				{
					CStaticSkin *pStatic = (CStaticSkin*)GetDocument()->m_pCurrentObject;
					pStatic->m_colorBack = (((CSPPropertyGridItemColor*)pItem)->GetColor());
				}					
				break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_COMBOX)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_COMBOX_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_COMBOX_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			case IDS_STRING_COMBOX_FONT: 
				{
					CComboBoxSkin *pCombox = (CComboBoxSkin*)GetDocument()->m_pCurrentObject;
					((CSPPropertyGridItemFont*)pItem)->GetFont(&(pCombox->m_fontComboBox));
				}
				break;
			case IDS_STRING_COMBOX_AUTOFONT:
				{
					CComboBoxSkin *pButton = (CComboBoxSkin*)GetDocument()->m_pCurrentObject;
					pButton->m_bAutoFont =  ((CSPPropertyGridItemBool*)pItem)->GetBool();

				}
				break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_LISTBOX)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_LISTBOX_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_LISTBOX_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			case IDS_STRING_LISTBOX_FONT: 
				{
					CListBoxSkin *pListBox = (CListBoxSkin*)GetDocument()->m_pCurrentObject;
					((CSPPropertyGridItemFont*)pItem)->GetFont(&(pListBox->m_fontListBox));
				}
				break;
			case IDS_STRING_LISTBOX_AUTOFONT:
				{
					CListBoxSkin *pListBox = (CListBoxSkin*)GetDocument()->m_pCurrentObject;
					pListBox->m_bAutoFont =  ((CSPPropertyGridItemBool*)pItem)->GetBool();

				}
				break;
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_POPUPMENU)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_POPUPMENU_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_POPUPMENU_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			
			case IDS_STRING_POPUPMENU_COLORBACKGROUND:
				{
					CPopupMenuSkin *pPopupMenu = (CPopupMenuSkin*)GetDocument()->m_pCurrentObject;
					pPopupMenu->m_colorBackground =  ((CSPPropertyGridItemColor*)pItem)->GetColor();

				}
				break;

			case IDS_STRING_POPUPMENU_TEXTNORMAL:
				{
					CPopupMenuSkin *pPopupMenu = (CPopupMenuSkin*)GetDocument()->m_pCurrentObject;
					pPopupMenu->m_itemPopup.textNormal =  ((CSPPropertyGridItemColor*)pItem)->GetColor();

				}
				break;
			case IDS_STRING_POPUPMENU_TEXTGRAYED:
				{
					CPopupMenuSkin *pPopupMenu = (CPopupMenuSkin*)GetDocument()->m_pCurrentObject;
					pPopupMenu->m_itemPopup.textGrayed =  ((CSPPropertyGridItemColor*)pItem)->GetColor();

				}
				break;
			case IDS_STRING_POPUPMENU_TEXTACTIVE:
				{
					CPopupMenuSkin *pPopupMenu = (CPopupMenuSkin*)GetDocument()->m_pCurrentObject;
					pPopupMenu->m_itemPopup.textActive =  ((CSPPropertyGridItemColor*)pItem)->GetColor();

				}
				break;

			case IDS_STRING_POPUPMENU_COLORACTIVE:
				{
					CPopupMenuSkin *pPopupMenu = (CPopupMenuSkin*)GetDocument()->m_pCurrentObject;
					pPopupMenu->m_itemPopup.colorActive =  ((CSPPropertyGridItemColor*)pItem)->GetColor();

				}
				break;

			case IDS_STRING_POPUPMENU_COLORSEPERATOR:
				{
					CPopupMenuSkin *pPopupMenu = (CPopupMenuSkin*)GetDocument()->m_pCurrentObject;
					pPopupMenu->m_itemPopup.colorSeparator =  ((CSPPropertyGridItemColor*)pItem)->GetColor();

				}
				break;
			}

		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SPIN)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_SPIN_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_SPIN_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;

			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_PROGRESS)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_PROGRESS_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_PROGRESS_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;
			case IDS_STRING_PROGRESS_COLORBACK:
				{
					CProgressSkin *pProgress = (CProgressSkin*)GetDocument()->m_pCurrentObject;
					pProgress->m_colorBackground =  ((CSPPropertyGridItemColor*)pItem)->GetColor();

				}
			case IDS_STRING_PROGRESS_COLORPROGRESS:
				{
					CProgressSkin *pProgress = (CProgressSkin*)GetDocument()->m_pCurrentObject;
					pProgress->m_colorProgress =  ((CSPPropertyGridItemColor*)pItem)->GetColor();

				}
			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SLIDER)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_SLIDER_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_SLIDER_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;

			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_HEADER)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_HEADER_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_HEADER_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;

			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_TAB)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_TAB_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_TAB_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;

			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_STATUSBAR)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_TAB_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_TAB_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;

			}
		}
		else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_TOOLBAR)
		{
			switch (pItem->GetID())
			{
			case IDS_STRING_TAB_NAME: 
				GetDocument()->m_pCurrentObject->SetName(pItem->GetValue()); break;
			case IDS_STRING_TAB_ID	: 
				GetDocument()->m_pCurrentObject->SetID(((CSPPropertyGridItemNumber*)pItem)->GetNumber()); break;

			}
		}
		GetDocument()->UpdateAllViews(this);
	}
	return 0;
}

