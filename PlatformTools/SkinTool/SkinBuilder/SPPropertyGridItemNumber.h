﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDITEMNUMBER_H__)
#define __SPPROPERTYGRIDITEMNUMBER_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//===========================================================================
// Summary:
//     CSPPropertyGridItemNumber is a CSPPropertyGridItem derived class.
//     It is used to create an integer value item in a Property Grid control.
//===========================================================================
class  CSPPropertyGridItemNumber : public CSPPropertyGridItem
{
public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridItemNumber object
	// Parameters:
	//     strCaption  - Caption of the item.
	//     nID         - Identifier of the item.
	//     nValue      - Initial value of item.
	//     pBindNumber - If not NULL, then the value of this item
	//                   is bound the value of this variable.
	// Remarks:
	//     Class CSPPropertyGridItemNumber has no default constructor.
	//
	//          When using the second constructor, the Identifier (nID) of the
	//          second constructor can be linked with a STRINGTABLE resource
	//          with the same id in such form "Caption\\nDescription".
	//
	//          BINDING:
	//            Variables can be bound to an item in two ways, the first is
	//            to pass in a variable at the time of creation, the second allows
	//            variables to be bound to an item after creation with the
	//            BindToNumber member.
	//
	//            Bound variables store the values of the property grid items
	//            and can be accessed without using the property grid methods
	//            and properties.  Bound variables allow the property grid to
	//            store data in variables.  When the value of a PropertyGridItem
	//            is changed, the value of the bound variable will be changed to
	//            the PropertyGridItem value.  The advantage of binding is that
	//            the variable can be used and manipulated without using
	//            PropertyGridItem methods and properties.
	//
	//            NOTE:  If the value of the variable is changed without using
	//            the PropertyGrid, the PropertyGridItem value will not be
	//            updated until you call CSPPropertyGrid::Refresh.
	// See Also: BindToNumber
	//-----------------------------------------------------------------------
	CSPPropertyGridItemNumber(CString strCaption, long nValue = 0, long* pBindNumber = NULL);
	CSPPropertyGridItemNumber(UINT nID, long nValue = 0, long* pBindNumber = NULL);  // <COMBINE CSPPropertyGridItemNumber::CSPPropertyGridItemNumber@CString@long@long*>

	//-----------------------------------------------------------------------
	// Summary:
	//     Destroys a CSPPropertyGridItemNumber object
	//-----------------------------------------------------------------------
	virtual ~CSPPropertyGridItemNumber();

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to change the item's value.
	// Parameters:
	//     nValue - The new integer value of the item.
	//-----------------------------------------------------------------------
	void SetNumber(long nValue);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to get the integer value of the item.
	// Returns:
	//     The integer value of the item.
	//-----------------------------------------------------------------------
	long GetNumber();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to bind an item to a long value.
	// Parameters:
	//     pBindNumber - Long value to bind to item.
	// Remarks:
	//     Variables can be bound to an item in two ways, the first is
	//     to pass in a variable at the time of creation, the second allows
	//     variables to be bound to an item after creation with the
	//     BindToNumber member.
	//
	//     Bound variables store the values of the property grid items
	//     and can be accessed without using the property grid methods
	//     and properties.  Bound variables allow the property grid to
	//     store data in variables.  When the value of a PropertyGridItem
	//     is changed, the value of the bound variable will be changed to
	//     the PropertyGridItem value.  The advantage of binding is that
	//     the variable can be used and manipulated without using
	//     PropertyGridItem methods and properties.
	//
	//     NOTE:  If the value of the variable is changed without using
	//     the PropertyGrid, the PropertyGridItem value will not be
	//     updated until you call CSPPropertyGrid::Refresh.
	//-----------------------------------------------------------------------
	void BindToNumber(long* pBindNumber);


protected:

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to change an item's value.
	//     Override this method to add new functionality.
	//     You should call the base class version of this function from your
	//     override.
	// Parameters:
	//     strValue - New value of the item.
	//-----------------------------------------------------------------------
	virtual void SetValue(CString strValue);

	//-------------------------------------------------------------------------
	// Summary:
	//     This member is called before the item becomes visible in the
	//     property grid.
	// Remarks:
	//     Before the item is inserted, it is first check to see if it
	//     is bound to a variable, if it is, then the value of the item
	//     is updated with the value stored in the bound variable.
	//
	//          OnBeforeInsert is called when an item is inserted,
	//          when a category is inserted, when a category or item is
	//          expanded, and when the sort property has changed.
	//-------------------------------------------------------------------------
	virtual void OnBeforeInsert();

protected:

	long m_nValue;          // Value of the item.
	long* m_pBindNumber;    // Binded object.  This is a pointer to the variable bound to this item.

private:

	DECLARE_DYNAMIC(CSPPropertyGridItemNumber)
};


//===========================================================================
// Summary:
//     CSPPropertyGridItemDouble is a CSPPropertyGridItem derived class.
//     It is used to create an double value item in a Property Grid control.
//===========================================================================
class  CSPPropertyGridItemDouble : public CSPPropertyGridItem
{
public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridItemDouble object.
	// Parameters:
	//     strCaption  - Caption of the item.
	//     nID         - Identifier of the item.
	//     fValue      - Initial value.
	//     strFormat   - The number of digits after the decimal point.
	//                   For example "%0.5f" would display 5 digits
	//                   past the decimal place.
	//     pBindNumber - If not NULL, then the value of this item
	//                   is bound the value of this variable.
	// Remarks:
	//     Class CSPPropertyGridItemDouble has no default constructor.
	//
	//          When using the second constructor, the Identifier (nID) of the
	//          second constructor can be linked with a STRINGTABLE resource
	//          with the same id in such form "Caption\\nDescription".
	//
	//          BINDING:
	//            Variables can be bound to an item in two ways, the first is
	//            to pass in a variable at the time of creation, the second allows
	//            variables to be bound to an item after creation with the
	//            BindToDouble member.
	//
	//            Bound variables store the values of the property grid items
	//            and can be accessed without using the property grid methods
	//            and properties.  Bound variables allow the property grid to
	//            store data in variables.  When the value of a PropertyGridItem
	//            is changed, the value of the bound variable will be changed to
	//            the PropertyGridItem value.  The advantage of binding is that
	//            the variable can be used and manipulated without using
	//            PropertyGridItem methods and properties.
	//
	//            NOTE:  If the value of the variable is changed without using
	//            the PropertyGrid, the PropertyGridItem value will not be
	//            updated until you call CSPPropertyGrid::Refresh.
	// See Also: BindToDouble
	//-----------------------------------------------------------------------
	CSPPropertyGridItemDouble(CString strCaption, double fValue = 0, LPCTSTR strFormat = _T("%0.2f"), double* pBindNumber = NULL);
	CSPPropertyGridItemDouble(UINT nID, double fValue = 0, LPCTSTR strFormat = _T("%0.2f"), double* pBindNumber = NULL); // <COMBINE CSPPropertyGridItemDouble::CSPPropertyGridItemDouble@CString@double@LPCTSTR@double*>

	//-----------------------------------------------------------------------
	// Summary:
	//     Destroys a CSPPropertyGridItemDouble object.
	//-----------------------------------------------------------------------
	virtual ~CSPPropertyGridItemDouble();

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to change the item's value.
	// Parameters:
	//     fValue - The new double value of the item.
	//-----------------------------------------------------------------------
	void SetDouble(double fValue);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to get the double value of the item.
	// Returns:
	//     The double value of the item.
	//-----------------------------------------------------------------------
	double GetDouble();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to bind the item to a double value.
	// Parameters:
	//     pBindDouble - Double value to bind to item.
	// Remarks:
	//     Variables can be bound to an item in two ways, the first is
	//     to pass in a variable at the time of creation, the second allows
	//     variables to be bound to an item after creation with the
	//     BindToDouble member.
	//
	//     Bound variables store the values of the property grid items
	//     and can be accessed without using the property grid methods
	//     and properties.  Bound variables allow the property grid to
	//     store data in variables.  When the value of a PropertyGridItem
	//     is changed, the value of the bound variable will be changed to
	//     the PropertyGridItem value.  The advantage of binding is that
	//     the variable can be used and manipulated without using
	//     PropertyGridItem methods and properties.
	//
	//     NOTE:  If the value of the variable is changed without using
	//     the PropertyGrid, the PropertyGridItem value will not be
	//     updated until you call CSPPropertyGrid::Refresh.
	//-----------------------------------------------------------------------
	void BindToDouble(double* pBindDouble);


protected:

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to change an item's value.
	//     Override this method to add new functionality.
	//     You should call the base class version of this function from your
	//     override.
	// Parameters:
	//     strValue - New value of the item.
	//-----------------------------------------------------------------------
	virtual void SetValue(CString strValue);

	//-------------------------------------------------------------------------
	// Summary:
	//     This member is called before the item becomes visible in the
	//     property grid.
	// Remarks:
	//     Before the item is inserted, it is first check to see if it
	//     is bound to a variable, if it is, then the value of the item
	//     is updated with the value stored in the bound variable.
	//
	//          OnBeforeInsert is called when an item is inserted,
	//          when a category is inserted, when a category or item is
	//          expanded, and when the sort property has changed.
	//-------------------------------------------------------------------------
	void OnBeforeInsert();


protected:

	double m_fValue;        // Double value of item.
	double* m_pBindDouble;  // Binded value.  This is a pointer to the variable bound to this item.

private:
	DECLARE_DYNAMIC(CSPPropertyGridItemDouble)

};

//////////////////////////////////////////////////////////////////////

AFX_INLINE long CSPPropertyGridItemNumber::GetNumber() {
	return m_nValue;
}

//////////////////////////////////////////////////////////////////////

AFX_INLINE double CSPPropertyGridItemDouble::GetDouble() {
	return m_fValue;
}

#endif // #if !defined(__SPPROPERTYGRIDITEMNUMBER_H__)
