﻿#include "StdAfx.h"
#include ".\sppropertygriditemimage.h"


CSPPropertyGridItemImage::CSPPropertyGridItemImage(CString strCaption, LPCTSTR strValue , CString* pBindString )
:CSPPropertyGridItem(strCaption)
{
	m_nFlags = SPGridItemHasEdit | SPGridItemHasExpandButton;
}
CSPPropertyGridItemImage::CSPPropertyGridItemImage(UINT nID)
:CSPPropertyGridItem(nID,NULL,NULL)
{
	m_nFlags = SPGridItemHasEdit | SPGridItemHasExpandButton;
}

CSPPropertyGridItemImage::~CSPPropertyGridItemImage(void)
{
}
void CSPPropertyGridItemImage::OnInplaceButtonDown()
{
	if ( m_dlgImage.DoModal( ) == IDOK )
	{
		if(m_dlgImage.m_wndImage.m_bSection)
		{
			OnValueChanged(m_dlgImage.m_wndImage.m_pImageSection->strImageName);
		}
		else
		{
			OnValueChanged(m_dlgImage.m_wndImage.m_pImageRect->strImageName);
		}
		m_pGrid->Invalidate( TRUE );
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////
CSPPropertyGridItemText::CSPPropertyGridItemText(CString strCaption, LPCTSTR strValue , CString* pBindString )
:CSPPropertyGridItem(strCaption)
{
	m_nFlags = SPGridItemHasEdit | SPGridItemHasExpandButton;
}
CSPPropertyGridItemText::CSPPropertyGridItemText(UINT nID)
:CSPPropertyGridItem(nID,NULL,NULL)
{
	m_nFlags = SPGridItemHasEdit | SPGridItemHasExpandButton;
}

CSPPropertyGridItemText::~CSPPropertyGridItemText(void)
{
}
void CSPPropertyGridItemText::OnInplaceButtonDown()
{
	if ( m_dlgText.DoModal( ) == IDOK )
	{
		/*if(m_dlgImage.m_wndImage.m_bSection)
		{
			OnValueChanged(m_dlgImage.m_wndImage.m_pImageSection->strImageName);
		}
		else
		{
			OnValueChanged(m_dlgImage.m_wndImage.m_pImageRect->strImageName);
		}
		m_pGrid->Invalidate( TRUE );*/
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////

class CSPPropertyGridItemRect::CCustomItemChildsAll : public CSPPropertyGridItemNumber
{
public:
	CCustomItemChildsAll(CString strCaption) : CSPPropertyGridItemNumber(strCaption) {}
	virtual void OnValueChanged(CString strValue)
	{
		SetValue(strValue);

		CSPPropertyGridItemRect* pParent = ((CSPPropertyGridItemRect*)m_pParent);
		CRect& rc = pParent->m_rcValue;
		rc.left = rc.right = rc.top = rc.bottom = GetNumber();
		pParent->OnValueChanged(pParent->RectToString(rc));
	}
};

class CSPPropertyGridItemRect::CCustomItemChildsPad : public CSPPropertyGridItemNumber
{
public:
	CCustomItemChildsPad(CString strCaption, LONG& nPad) : CSPPropertyGridItemNumber(strCaption), m_nPad(nPad) {}
	virtual void OnValueChanged(CString strValue)
	{
		SetValue(strValue);

		CSPPropertyGridItemRect* pParent = ((CSPPropertyGridItemRect*)m_pParent);
		m_nPad = GetNumber();
		pParent->OnValueChanged(pParent->RectToString(pParent->m_rcValue));

	}
	LONG& m_nPad;
};

CSPPropertyGridItemRect::CSPPropertyGridItemRect(CString strCaption, CRect rcValue)
: CSPPropertyGridItem(strCaption)
{
	m_rcValue = rcValue;
	m_strValue = RectToString(rcValue);
	m_nFlags = 0;
}

CSPPropertyGridItemRect::CSPPropertyGridItemRect(UINT nID, CRect rcValue)
: CSPPropertyGridItem(nID,NULL,NULL)
{
	m_rcValue = rcValue;
	m_strValue = RectToString(rcValue);
	m_nFlags = 0;
}

void CSPPropertyGridItemRect::OnAddChildItem()
{
	m_itemLeft = (CCustomItemChildsPad*)AddChildItem(new CCustomItemChildsPad(_T("Left"), m_rcValue.left));
	m_itemTop = (CCustomItemChildsPad*)AddChildItem(new CCustomItemChildsPad(_T("Top"), m_rcValue.top));
	m_itemRight = (CCustomItemChildsPad*)AddChildItem(new CCustomItemChildsPad(_T("Right"), m_rcValue.right));
	m_itemBottom = (CCustomItemChildsPad*)AddChildItem(new CCustomItemChildsPad(_T("Bottom"), m_rcValue.bottom));

	Expand();
	UpdateChilds();
}

void CSPPropertyGridItemRect::UpdateChilds()
{
	m_itemLeft->SetNumber(m_rcValue.left);
	m_itemRight->SetNumber(m_rcValue.right);
	m_itemTop->SetNumber(m_rcValue.top);
	m_itemBottom->SetNumber(m_rcValue.bottom);
}

void CSPPropertyGridItemRect::SetValue(CString strValue)
{
	CSPPropertyGridItem::SetValue(strValue);
	UpdateChilds();
}

CString CSPPropertyGridItemRect::RectToString(CRect rc)
{
	CString str;
	str.Format(_T("%i; %i; %i; %i"), rc.left, rc.top, rc.right, rc.bottom);
	return str;
}
void CSPPropertyGridItemRect::SetRect(CRect rc)
{
	m_rcValue = rc;
	SetValue(RectToString(m_rcValue));
	UpdateChilds();
}
CRect CSPPropertyGridItemRect::GetRect()
{
	return m_rcValue;
}
