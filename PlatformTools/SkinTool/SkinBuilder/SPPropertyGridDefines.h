﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDDEFINES_H__)
#define __SPPROPERTYGRIDDEFINES_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//-----------------------------------------------------------------------
// Summary:
//     The XTWM_PROPERTYGRID_NOTIFY message is sent to the CSPPropertyGrid owner window
//     whenever an action occurs within the CSPPropertyGrid
//
// <code>XTWM_PROPERTYGRID_NOTIFY
// nGridAction = (int) wParam;              // Property grid action
// pItem = (CSPPropertyGridItem*) lParam;  // pointer to a CSPPropertyGridItem object</code>
//
//     When the user performs an action in the property grid, the XTWM_PROPERTYGRID_NOTIFY message is
//     sent to the property grid's owner window.
// Parameters:
//     nGridAction -  Value of wParam specifies an value that indicates the user's
//                    request .
//     pItem - The value of lParam points to a CSPPropertyGridItem object that contains information for the
//             specified item. This pointer should <b>never</b> be NULL.
// Returns:
//     If the application is to process this message, the return value should be TRUE, otherwise the
//     return value is FALSE.
// Remarks:
//     nGridAction parameter can be one of the following values:
//          * <b>SP_PGN_SORTORDER_CHANGED</b> The sort order has changed in the property grid.
//          * <b>SP_PGN_SELECTION_CHANGED</b> The selection has changed in the property grid.
//          * <b>SP_PGN_ITEMVALUE_CHANGED</b> The value has changed for pItem in the property grid.
//          * <b>SP_PGN_EDIT_CHANGED</b> The edit value has changed in the property grid.
//          * <b>SP_PGN_INPLACEBUTTONDOWN</b> Item's in-place button (combo or expand) down.
//          * <b>SP_PGN_ITEMEXPANDCHANGED</b> User expand or collapse item.
//          * <b>SP_PGN_DBLCLICK</b> The user double-clicks the left mouse button in the property grid.
//          * <b>SP_PGN_RCLICK</b> The user pressed the right mouse button in the property grid.
//          * <b>SP_PGN_VERB_CLICK</b> The user click verb in the property grid.
//
// Example:
//    Here is an example of how an application would process the XTWM_PROPERTYGRID_NOTIFY
//    message.
//
// <code>
// int nGridAction = (int)wParam;
//
// // Cast the lParam to an CSPPropertyGridItem* object pointer.
// CSPPropertyGridItem* pItem = (CSPPropertyGridItem*)lParam;
// ASSERT(pItem);
//
// switch (nGridAction)
// {
//     case SP_PGN_SORTORDER_CHANGED:
//           {
//              m_nSort = m_wndSPPropertyGrid.GetPropertySort();
//              UpdateData(FALSE);
//           }
//         break;
//     case SP_PGN_ITEMVALUE_CHANGED:
//           {
//              TRACE(_T("Value Changed. Caption = %s, ID = %i, Value = %s\n"),
//                  pItem->GetCaption(), pItem->GetID(), pItem->GetValue());
//           }
//         break;
//     case SP_PGN_SELECTION_CHANGED:
//           {
//              TRACE(_T("Selection Changed. Item = %s\n"), pItem->GetCaption());
//           }
//         break;
// }
// return FALSE;
// </code>
//-----------------------------------------------------------------------
const UINT SPWM_PROPERTYGRID_NOTIFY = (WM_APP + 2533);


const UINT SP_PGN_SORTORDER_CHANGED  = 1;  //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_SELECTION_CHANGED  = 2;  //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_ITEMVALUE_CHANGED  = 3;  //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_EDIT_CHANGED      = 4;   //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_INPLACEBUTTONDOWN = 5;   //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_DRAWITEM = 6;            //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_ITEMEXPANDCHANGED = 7;   //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_DBLCLICK = 8;            //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_RCLICK = 9;              //<ALIAS SPWM_PROPERTYGRID_NOTIFY>
const UINT SP_PGN_VERB_CLICK = 10;         //<ALIAS SPWM_PROPERTYGRID_NOTIFY>

//-----------------------------------------------------------------------
// Summary:
//     Apply this style to the property grid to use the owner draw feature.
// See Also:
//     CSPPropertyGrid::Create
// Example:
//     The following example illustrates using SP_PGS_OWNERDRAW:
// <code>
// m_wndPropertyGrid.ModifyStyle(0, SP_PGS_OWNERDRAW);
// </code>
//-----------------------------------------------------------------------
const UINT SP_PGS_OWNERDRAW = 0x0010L;


#endif //#define __SPPROPERTYGRIDDEFINES_H__
