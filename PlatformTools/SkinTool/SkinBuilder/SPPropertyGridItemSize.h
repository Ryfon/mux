﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDITEMSIZE_H__)
#define __SPPROPERTYGRIDITEMSIZE_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//===========================================================================
// Summary:
//     CSPPropertyGridItemSize is a CSPPropertyGridItem derived class.
//     It is used to create a size value item in a Property Grid control.
//===========================================================================
class  CSPPropertyGridItemSize : public CSPPropertyGridItem
{
public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridItemSize object.
	// Parameters:
	//     strCaption - Caption of the item.
	//     nID        - Identifier of the item.
	//     size       - Initial value of item.
	//     pBindSize  - If not NULL, then the value of this item
	//                   is bound the value of this variable.
	// Remarks:
	//     Class CSPPropertyGridItemSize has no default constructor.
	//
	//          When using the second constructor, the Identifier (nID) of the
	//          second constructor can be linked with a STRINGTABLE resource
	//          with the same id in such form "Caption\\nDescription".
	//
	//          BINDING:
	//            Variables can be bound to an item in two ways, the first is
	//            to pass in a variable at the time of creation, the second allows
	//            variables to be bound to an item after creation with the
	//            BindToSize member.
	//
	//            Bound variables store the values of the property grid items
	//            and can be accessed without using the property grid methods
	//            and properties.  Bound variables allow the property grid to
	//            store data in variables.  When the value of a PropertyGridItem
	//            is changed, the value of the bound variable will be changed to
	//            the PropertyGridItem value.  The advantage of binding is that
	//            the variable can be used and manipulated without using
	//            PropertyGridItem methods and properties.
	//
	//            NOTE:  If the value of the variable is changed without using
	//            the PropertyGrid, the PropertyGridItem value will not be
	//            updated until you call CSPPropertyGrid::Refresh.
	// See Also: BindToSize
	//-----------------------------------------------------------------------
	CSPPropertyGridItemSize(CString strCaption, CSize size = 0, CSize* pBindSize = NULL);
	CSPPropertyGridItemSize(UINT nID, CSize size = 0, CSize* pBindSize = NULL); //<COMBINE CSPPropertyGridItemSize::CSPPropertyGridItemSize@CString@CSize@CSize*>

	//-----------------------------------------------------------------------
	// Summary:
	//     Destroys a CSPPropertyGridItemSize object.
	//-----------------------------------------------------------------------
	virtual ~CSPPropertyGridItemSize();

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to change the item's value.
	// Parameters:
	//     size - The new size value of the item.
	//-----------------------------------------------------------------------
	void SetSize(CSize size);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to get the size value of the item.
	// Returns:
	//     A CSize object representing the size value of the item.
	//-----------------------------------------------------------------------
	CSize GetSize();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to bind an item to a CSize object.
	// Parameters:
	//     pBindSize - CSize object to bind to item.
	// Remarks:
	//     Variables can be bound to an item in two ways, the first is
	//     to pass in a variable at the time of creation, the second allows
	//     variables to be bound to an item after creation with the
	//     BindToSize member.
	//
	//     Bound variables store the values of the property grid items
	//     and can be accessed without using the property grid methods
	//     and properties.  Bound variables allow the property grid to
	//     store data in variables.  When the value of a PropertyGridItem
	//     is changed, the value of the bound variable will be changed to
	//     the PropertyGridItem value.  The advantage of binding is that
	//     the variable can be used and manipulated without using
	//     PropertyGridItem methods and properties.
	//
	//     NOTE:  If the value of the variable is changed without using
	//     the PropertyGrid, the PropertyGridItem value will not be
	//     updated until you call CSPPropertyGrid::Refresh.
	//-----------------------------------------------------------------------
	void BindToSize(CSize* pBindSize);

protected:

	//-------------------------------------------------------------------------
	// Summary:
	//     This member is called before the item becomes visible in the
	//     property grid.
	// Remarks:
	//     Before the item is inserted, it is first check to see if it
	//     is bound to a variable, if it is, then the value of the item
	//     is updated with the value stored in the bound variable.
	//
	//          OnBeforeInsert is called when an item is inserted,
	//          when a category is inserted, when a category or item is
	//          expanded, and when the sort property has changed.
	//-------------------------------------------------------------------------
	void OnBeforeInsert();

private:

	void SetWidth(CString strWidth);
	void SetHeight(CString strHeight);

	CString SizeToString(CSize size);
	CSize StringToSize(CString str);

	virtual void OnAddChildItem();
	virtual void SetValue(CString strValue);

	void UpdateChilds();

private:

	class CSPPropertyGridItemSizeWidth;
	class CSPPropertyGridItemSizeHeight;

	CSize                          m_szValue;
	CSPPropertyGridItemSizeWidth*  m_itemWidth;
	CSPPropertyGridItemSizeHeight* m_itemHeight;

	CSize* m_pBindSize;

private:

	DECLARE_DYNAMIC(CSPPropertyGridItemSize)

	friend class CSPPropertyGridItemSizeWidth;
	friend class CSPPropertyGridItemSizeHeight;

};

//////////////////////////////////////////////////////////////////////

AFX_INLINE CSize CSPPropertyGridItemSize::GetSize() {
	return m_szValue;
}


#endif // #if !defined(__SPPROPERTYGRIDITEMSIZE_H__)
