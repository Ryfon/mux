﻿#pragma once
#include "afxcmn.h"

#include "resource.h"
#include "ImageEditWnd.h"
#include "afxwin.h"
// CImageDlg dialog

class CImageDlg : public CDialog
{
	DECLARE_DYNAMIC(CImageDlg)

public:
	CImageDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CImageDlg();
	void	UpdateRect(CRect rect);

	CListBox m_listImage;
	CImageEditWnd m_wndImage;
	CSkinBitmap	*m_pCurrentBitmap;
	
	int m_nSectionLeft;
	int m_nSectionTop;
	int m_nSectionRight;
	int m_nSectionBottom;
	int m_nMarginsLeft;
	int m_nMarginsTop;
	int m_nMarginsRight;
	int m_nMarginsBottom;
	int m_nDrawMode;
	
	CComboBox m_combDrawMode;
// Dialog Data
	enum { IDD = IDD_DIALOG_IMAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	

	afx_msg void OnLvnItemchangedListImage(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEditSelectionleft();
	afx_msg void OnEnChangeEditSelectiontop();
	afx_msg void OnEnChangeEditSelectionright();
	afx_msg void OnEnChangeEditSelectionbottom();
	afx_msg void OnEnChangeEditMarginsleft();
	afx_msg void OnEnChangeEditMarginstop();
	afx_msg void OnEnChangeEditMarginsright();
	afx_msg void OnEnChangeEditMarginsbottom();
	afx_msg void OnDeltaposSpinMarginsbottom(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinMarginsleft(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinMarginsright(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinMarginstop(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinSelectionbottom(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinSelectionleft(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinSelectionright(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinSelectiontop(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLbnSelchangeListImage();
	
	
	afx_msg void OnBnClickedButtonZoom1();
	afx_msg void OnBnClickedButtonZoom2();
	afx_msg void OnBnClickedButtonZoom3();
	afx_msg void OnBnClickedButtonZoom4();
	afx_msg void OnBnClickedButtonZoom5();
	afx_msg void OnCbnSelchangeComboDrawmode();
};
