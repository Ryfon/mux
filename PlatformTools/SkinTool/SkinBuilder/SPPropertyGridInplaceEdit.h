﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDINPLACEEDIT_H__)
#define __SPPROPERTYGRIDINPLACEEDIT_H__
//}}AFX_CODEJOCK_PRIVATE

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// class forwards.

#include "SPMaskEditT.h"

class CSPPropertyGridView;
class CSPPropertyGridItem;

//===========================================================================
// Summary:
//     CSPPropertyGridInplaceEdit is a CEdit derived class.
//     It is an internal class used by Property Grid control.
//===========================================================================
class  CSPPropertyGridInplaceEdit : public CXTMaskEditT<CEdit>
{
	DECLARE_DYNAMIC(CSPPropertyGridInplaceEdit)

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridInplaceEdit object
	//-----------------------------------------------------------------------
	CSPPropertyGridInplaceEdit();

	//-----------------------------------------------------------------------
	// Summary:
	//     Destroys a CSPPropertyGridInplaceEdit object, handles cleanup
	//     and deallocation.
	//-----------------------------------------------------------------------
	virtual ~CSPPropertyGridInplaceEdit();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to set edit text value.
	// Parameters:
	//     strValue - String value.
	//-----------------------------------------------------------------------
	virtual void SetValue(CString strValue);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to hide in-place edit control.
	//-----------------------------------------------------------------------
	virtual void HideWindow();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to create in-place edit control.
	// Parameters:
	//     pItem - Points to a CSPPropertyGridItem object
	//     rect -  Specifies the size and position of the item.
	//-----------------------------------------------------------------------
	virtual void Create(CSPPropertyGridItem* pItem, CRect rect);

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called when a parent item is destroyed.
	//-----------------------------------------------------------------------
	virtual void DestroyItem();

	//-----------------------------------------------------------------------
	// Summary:
	//     This member function retrieves a child property grid item
	// Returns:
	//     A pointer to a CSPPropertyGridItem object
	//-----------------------------------------------------------------------
	CSPPropertyGridItem* GetItem();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to select a constraint in child item constraint list.
	// Parameters:
	//     nDirection - Direction of constraint search.
	//     bCircle - TRUE to find by cycle.
	//-----------------------------------------------------------------------
	virtual BOOL SelectConstraint(int nDirection, BOOL bCircle);

private:

	CString              m_strValue;
	CBrush               m_brBack;
	COLORREF             m_clrBack;
	CWnd*                m_pGrid;
	CSPPropertyGridItem* m_pItem;

protected:
//{{AFX_CODEJOCK_PRIVATE
	//{{AFX_VIRTUAL(CSPPropertyGridInplaceEdit)
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL


	//{{AFX_MSG(CSPPropertyGridInplaceEdit)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnEnKillfocus();
	afx_msg void OnEnSetfocus();
	afx_msg void OnEnChange();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg UINT OnGetDlgCode();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
//}}AFX_CODEJOCK_PRIVATE

private:
	BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);

	DECLARE_MESSAGE_MAP()
	friend class CSPPropertyGridItem;
};

AFX_INLINE CSPPropertyGridItem* CSPPropertyGridInplaceEdit::GetItem() {
	return m_pItem;
}
AFX_INLINE BOOL CSPPropertyGridInplaceEdit::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) {
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}
AFX_INLINE BOOL CSPPropertyGridInplaceEdit::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID) {
	return CEdit::Create(dwStyle, rect, pParentWnd, nID);
}

#endif // #if !defined(__SPPROPERTYGRIDINPLACEEDIT_H__)
