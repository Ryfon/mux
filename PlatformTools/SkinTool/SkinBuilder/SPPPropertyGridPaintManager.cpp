﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/

#include "stdafx.h"
#include "Tmschema.h"
#include "SPVC80Helpers.h"
#include "SPDrawHelpers.h"

#include "SPPropertyGridPaintManager.h"
#include "SPPropertyGridInplaceEdit.h"
#include "SPPropertyGridInplaceButton.h"
#include "SPPropertyGridInplaceList.h"
#include "SPPropertyGridItem.h"
#include "SPPropertyGrid.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSPPropertyGridPaintManager::CSPPropertyGridPaintManager(CSPPropertyGrid* pGrid)
	: m_pGrid(pGrid)
{
	m_bThemed = FALSE;

	LOGFONT lfIcon;
	VERIFY( ::SystemParametersInfo( SPI_GETICONTITLELOGFONT, sizeof( lfIcon ), &lfIcon, 0 ) );
	lfIcon.lfWeight = FW_NORMAL;
	lfIcon.lfItalic = FALSE;

	VERIFY( m_fntVerbNormal.CreateFontIndirect(&lfIcon  ) );
	lfIcon.lfUnderline = TRUE;
	VERIFY( m_fntVerbUnderline.CreateFontIndirect(&lfIcon  ) );


	RefreshMetrics();

}

CSPPropertyGridPaintManager::~CSPPropertyGridPaintManager()
{

}

void CSPPropertyGridPaintManager::RefreshMetrics()
{
	SPColorManager()->RefreshColors();

	m_clrFace = GetXtremeColor(COLOR_3DFACE);
	m_clrShadow = GetXtremeColor(COLOR_3DSHADOW);
	m_clrVerbFace = GetXtremeColor(COLOR_ACTIVECAPTION);

	CSPPropertyGridView* pView = &m_pGrid->GetGridView();

	m_pGrid->m_clrHelpBack.SetStandardValue(m_clrFace);
	m_pGrid->m_clrHelpFore.SetStandardValue(GetXtremeColor(COLOR_BTNTEXT));
	pView->m_clrLine.SetStandardValue(GetSysColor(COLOR_SCROLLBAR));
	pView->m_clrCategoryFore.SetStandardValue(GetXtremeColor(COLOR_GRAYTEXT));
	pView->m_clrBack.SetStandardValue(GetXtremeColor(COLOR_WINDOW));
	pView->m_clrFore.SetStandardValue(GetXtremeColor(COLOR_WINDOWTEXT));
	pView->m_clrReadOnlyFore.SetStandardValue(GetXtremeColor(COLOR_GRAYTEXT));

	HWND hWnd = AfxGetMainWnd()? AfxGetMainWnd()->GetSafeHwnd(): 0;
	m_themeTree.OpenThemeData(hWnd, L"TREEVIEW");
	m_themeButton.OpenThemeData(hWnd, L"BUTTON");
	m_themeCombo.OpenThemeData(hWnd, L"COMBOBOX");


}

void CSPPropertyGridPaintManager::FillPropertyGrid(CDC* pDC)
{
	CSPClientRect rc(m_pGrid);
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();
	pDC->FillSolidRect(rc, m_clrFace);
	pDC->SetBkMode(TRANSPARENT);

	if (m_pGrid->IsHelpVisible())
	{
		CSPPenDC pen(*pDC, m_clrShadow);
		CSPBrushDC brush(*pDC, m_pGrid->m_clrHelpBack);

		CRect rcHelp(rc);
		rcHelp.top = rc.bottom - m_pGrid->m_nHelpHeight;
		pDC->Rectangle(rcHelp);


		CSPFontDC font(pDC, &pView->m_fontBold);
		CSPPropertyGridItem* pItem = m_pGrid->GetSelectedItem();
		if (pItem)
		{
			pDC->SetTextColor(m_pGrid->m_clrHelpFore);
			pDC->SetBkColor(m_pGrid->m_clrHelpBack);
			CRect rcCaption(rcHelp);
			rcCaption.DeflateRect(6, 3);
			pDC->DrawText(pItem->GetCaption(), rcCaption, DT_SINGLELINE|DT_NOPREFIX);

			CString strDesc = pItem->GetDescription();
			if (!strDesc.IsEmpty())
			{
				CSPFontDC font(pDC, &pView->m_fontNormal);
				CSPEmptyRect rcCaption;
				pDC->DrawText(pItem->GetCaption(), rcCaption, DT_SINGLELINE|DT_NOPREFIX|DT_CALCRECT);
				CRect rcDesc(rcHelp);
				rcDesc.DeflateRect(6, 3 + rcCaption.Height() + 3, 6, 3);
				pDC->DrawText(strDesc, rcDesc, DT_WORDBREAK|DT_NOPREFIX);
			}
		}

		rc.bottom -= m_pGrid->m_nHelpHeight + 3;
	}

	if (m_pGrid->IsVerbsVisible())
	{
		CSPPenDC pen(*pDC, m_clrShadow);
		CSPBrushDC brush(*pDC, m_pGrid->m_clrHelpBack);

		CRect rcVerbs(rc);
		rcVerbs.top = rc.bottom - m_pGrid->m_nVerbsHeight;

		HitTestVerbs(pDC, rcVerbs, CPoint(0));
	}
}

int CSPPropertyGridPaintManager::HitTestVerbs(CDC* pDC, CRect rcVerbs, CPoint pt)
{
	#define DRAWTESPART(strCaption)\
		CRect rcText(CRect(x, y, min(x + nTextWidth, rcVerbs.right), min(y + nTextHeight, rcVerbs.bottom)));\
		if (bDraw) {\
			pVerb->SetPart(rcText);\
			pDC->SetTextColor(m_clrVerbFace);\
			pDC->DrawText(strCaption, rcText, DT_SINGLELINE|DT_NOPREFIX); \
			if (pVerb->IsFocused()) {\
				pDC->SetTextColor(m_pGrid->m_clrHelpBack); pDC->SetBkColor(0); rcText.bottom ++;\
				pDC->DrawFocusRect(rcText);\
			}\
		}\
		else if (rcText.PtInRect(pt)) return i;

	BOOL bDraw = (pt == CPoint(0));

	if (bDraw) pDC->Rectangle(rcVerbs);

	CSPFontDC font(pDC, &m_fntVerbUnderline);

	rcVerbs.DeflateRect(6, 4);

	int nCount = m_pGrid->GetVerbs()->GetCount();

	int x = rcVerbs.left, y = rcVerbs.top, nWidth = rcVerbs.Width();
	int nTextHeight = pDC->GetTextExtent(_T("XXX")).cy;

	for (int i = 0; i < nCount; i++)
	{
		CSPPropertyGridVerb* pVerb = m_pGrid->GetVerbs()->GetAt(i);
		CString strCaption = pVerb->GetCaption();

		while(TRUE)
		{
			if (strCaption.IsEmpty())
				break;

			int nTextWidth = pDC->GetTextExtent(strCaption).cx;

			if (x + nTextWidth < nWidth)
			{
				DRAWTESPART(strCaption);
				x += nTextWidth;
				break;
			}
			else
			{
				int nStartIndex = (strCaption.GetLength() - 1) * (nWidth - x) / nTextWidth;
				int nIndex = nStartIndex;

				if ((nStartIndex < 1) && (x == rcVerbs.left))
				{
					i = nCount - 1;
					break;
				}

				for (;nIndex >= 0; nIndex--)
				{
					if (strCaption[nIndex] == _T(' ') || strCaption[nIndex] == _T(',') || strCaption[nIndex] == _T('-'))
						break;
				}

				if (nIndex > 0 || (x == rcVerbs.left))
				{
					if (nIndex <= 0) nIndex = nStartIndex;

					CString strLeft = strCaption.Left(nIndex + 1);
					CString strRight = strCaption.Mid(nIndex + 1);

					int nTextWidth = pDC->GetTextExtent(strLeft).cx;
					DRAWTESPART(strLeft);

					x = rcVerbs.left;
					y += nTextHeight;
					strCaption = strRight;
				}
				else
				{
					y += nTextHeight;
					x = rcVerbs.left;
				}
			}
			if (y >= rcVerbs.bottom)
				break;
		}

		if ((y >= rcVerbs.bottom) || (i == nCount - 1))
			break;

		if (bDraw)
		{
			font.SetFont(&m_fntVerbNormal);
			pDC->SetTextColor(m_pGrid->m_clrHelpFore);
			pDC->DrawText(_T(","), 1, CRect(x, y, x + 8, min(y + nTextHeight, rcVerbs.bottom)), DT_SINGLELINE|DT_NOPREFIX);
			font.SetFont(&m_fntVerbUnderline);
		}
		x += 8;
	}
	return -1;
}

void CSPPropertyGridPaintManager::FillPropertyGridView(CDC* pDC, BOOL bClient)
{

	CSPPropertyGridView* pView = &m_pGrid->GetGridView();


	if (bClient)
	{
		CSPClientRect rc(pView);
		pDC->FillSolidRect(rc, pView->m_clrBack);
	} else
	{
		CSPWindowRect rc(pView);
		rc.OffsetRect(-rc.TopLeft());
		pDC->Draw3dRect(rc, m_clrShadow, m_clrShadow);
	}

}

void CSPPropertyGridPaintManager::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();
	if (pView->GetStyle() & LBS_OWNERDRAWVARIABLE)
	{
		CSPPropertyGridItem* pItem = (CSPPropertyGridItem*)lpMeasureItemStruct->itemData;
		pItem->MeasureItem(lpMeasureItemStruct);
	}
}

void CSPPropertyGridPaintManager::DrawCategoryCaptionBackground(CDC* pDC, CRect rc)
{
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();
	pDC->FillSolidRect(rc, pView->m_clrLine);
}

void CSPPropertyGridPaintManager::DrawItem(PDRAWITEMSTRUCT lpDrawItemStruct)
{
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();
	CSPPropertyGridItem* pItem = (CSPPropertyGridItem*)lpDrawItemStruct->itemData;

	CSPWindowRect rcWindow(pView);
	CRect rc = lpDrawItemStruct->rcItem;

	CSPBufferDC dc(lpDrawItemStruct->hDC, rc);
	CSPFontDC font(&dc, &pView->m_fontNormal);

	COLORREF clrWindow = pView->m_clrBack;

	dc.FillSolidRect(rc, clrWindow);
	dc.SetBkMode(TRANSPARENT);


	CRect rcCaption(rc.left, rc.top, rc.left + pView->GetDividerPos(), rc.bottom);

	if (pItem->IsCategory())
	{
		DrawCategoryCaptionBackground(&dc, rc);

		CSPFontDC font(&dc, &pView->m_fontBold);

		dc.SetTextColor(pView->m_clrCategoryFore);
		CSPEmptyRect rcDrawText;
		dc.DrawText( pItem->GetCaption(), rcDrawText,  DT_SINGLELINE|DT_NOPREFIX|DT_VCENTER|DT_CALCRECT);

		CRect rcText(CPoint(((pItem->GetIndent() + 1) * SP_PGI_EXPAND_BORDER) - 1, rc.top + 1) , CSize(rcDrawText.Width() + 4, rc.Height() - 3));

		dc.DrawText( pItem->GetCaption(), rcText,  DT_SINGLELINE|DT_NOPREFIX|DT_VCENTER|DT_RIGHT);

		if ((lpDrawItemStruct->itemAction | ODA_FOCUS) && (lpDrawItemStruct->itemState & ODS_FOCUS))
		{
			dc.SetTextColor(0);
			dc.DrawFocusRect(rcText);
		}
	}
	else
	{
		dc.SetTextColor(pItem->GetReadOnly()? pView->m_clrReadOnlyFore: pView->m_clrFore);

		CRect rcValue(rc);
		rcValue.left = rcCaption.right + 4;
		rcValue.bottom -= 1;

		if (!pItem->OnDrawItemValue(dc, rcValue))
		{
			dc.DrawText( pItem->GetViewValue(), rcValue,  DT_SINGLELINE|DT_NOPREFIX|DT_VCENTER);
		}

		BOOL bSelected = ((lpDrawItemStruct->itemAction | ODA_SELECT) && (lpDrawItemStruct->itemState & ODS_SELECTED));
		BOOL bFocused = bSelected && CWnd::GetFocus() && ((CWnd::GetFocus() == pView) || (CWnd::GetFocus()->GetParent() == pView) || (CWnd::GetFocus()->GetOwner() == pView));
		if (bFocused)
		{
			dc.SetTextColor(::GetXtremeColor(COLOR_HIGHLIGHTTEXT));
			dc.FillSolidRect(rcCaption, ::GetXtremeColor(COLOR_HIGHLIGHT));
		}
		else if (bSelected)
		{
			dc.FillSolidRect(rcCaption, pView->m_clrLine);
		}

		CSPPropertyGridItem* pCategory = pItem->GetParentItem();
		while (pCategory != NULL)
		{
			if (pCategory->IsCategory())
				dc.FillSolidRect(rc.left + pCategory->GetIndent() * SP_PGI_EXPAND_BORDER, rc.top, SP_PGI_EXPAND_BORDER, rc.Height(), pView->m_clrLine);

			pCategory = pCategory->GetParentItem();
		}

		CSPPenDC pen(dc, pView->m_clrLine);
		dc.MoveTo(0, rc.bottom - 1); dc.LineTo(rc.right, rc.bottom - 1);
		dc.MoveTo(rcCaption.right, rc.top); dc.LineTo(rcCaption.right, rc.bottom - 1);

		CRect rcText(rc);
		rcText.left = pItem->GetIndent() * SP_PGI_EXPAND_BORDER + 3;
		rcText.right = rcCaption.right - 1;
		rcText.bottom -= 1;
		dc.DrawText( pItem->GetCaption(),   rcText,  DT_SINGLELINE|DT_NOPREFIX|DT_VCENTER);
	}

	CSPPropertyGridItem* pNext = pView->GetItem(lpDrawItemStruct->itemID + 1);
	if (pNext && pNext->IsCategory() && pNext->IsVisible())
	{
		dc.FillSolidRect(pNext->GetIndent() * SP_PGI_EXPAND_BORDER, rc.bottom - 1, rc.Width(), 1, m_clrFace);
	}



	if (pItem->HasChilds())
	{
		CRgn rgn;
		if (!pItem->IsCategory())
		{
			rgn.CreateRectRgnIndirect(&rcCaption);
			dc.SelectClipRgn(&rgn);
		}

		DrawExpandButton(dc, pItem, rcCaption);

		dc.SelectClipRgn(NULL);
	}
}

void CSPPropertyGridPaintManager::DrawExpandButton(CDC& dc, CSPPropertyGridItem* pItem, CRect rcCaption)
{
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();


	CRect rcSign(CPoint(SP_PGI_EXPAND_BORDER / 2 - 5, rcCaption.CenterPoint().y - 4), CSize(9, 9));

	if (pItem->GetIndent() > 0)
		rcSign.OffsetRect((pItem->GetIndent() - (pItem->IsCategory()? 0: 1)) * SP_PGI_EXPAND_BORDER, 0);


	if (m_bThemed && m_themeTree.IsAppThemed())
	{
		m_themeTree.DrawThemeBackground(dc, TVP_GLYPH, pItem->IsExpanded()? GLPS_OPENED: GLPS_CLOSED, &rcSign, 0);

	} else
	{

		CPoint pt = rcSign.CenterPoint();

		if (!pItem->IsCategory()) dc.FillSolidRect(rcSign, pView->m_clrBack);
		dc.Draw3dRect(rcSign, pView->m_clrFore, pView->m_clrFore);
		CSPPenDC pen (dc,  pView->m_clrFore);

		dc.MoveTo(pt.x - 2, pt.y);
		dc.LineTo(pt.x + 3, pt.y);

		if(!pItem->IsExpanded())
		{
			dc.MoveTo(pt.x, pt.y - 2);
			dc.LineTo(pt.x, pt.y + 3);
		}
	}


}

void CSPPropertyGridPaintManager::FillInplaceButton(CDC* pDC, CSPPropertyGridInplaceButton* pButton)
{
	CSPClientRect rect(pButton);
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();
	CPoint ptDots(rect.CenterPoint().x , rect.CenterPoint().y + 3);

	if (m_bThemed && m_themeButton.IsAppThemed() && m_themeCombo.IsAppThemed())
	{
		pDC->FillSolidRect(rect, pView->m_clrBack);
		pDC->FillSolidRect(rect.left, rect.top, rect.Width(), 1, pView->m_clrLine);
		pDC->FillSolidRect(rect.left, rect.bottom - 1, rect.Width(), 1, pView->m_clrLine);
		
		if (pButton->GetItem()->GetFlags() & SPGridItemHasExpandButton)
		{
			m_themeButton.DrawThemeBackground(*pDC, BP_PUSHBUTTON, pButton->m_bPressed && pButton->m_bOver? PBS_PRESSED: 
				pButton->m_bOver? PBS_HOT: CWnd::GetFocus() == pButton? PBS_DEFAULTED: PBS_NORMAL, rect, 0);		
		}
		
		if (pButton->GetItem()->GetFlags() & SPGridItemHasComboButton)
		{
			m_themeCombo.DrawThemeBackground(*pDC, CP_DROPDOWNBUTTON, 
				pButton->m_bPressed && pButton->m_bOver? CBXS_PRESSED: pButton->m_bOver? CBXS_HOT: CBXS_NORMAL, rect, 0);		
		} 
	}
	else
	{
		pDC->FillSolidRect(rect, GetXtremeColor(COLOR_3DFACE));

		if (pButton->m_bPressed && pButton->m_bOver) 
			pDC->Draw3dRect(rect, GetXtremeColor(COLOR_3DSHADOW), GetXtremeColor(COLOR_3DSHADOW));
		else
		{
			pDC->Draw3dRect(rect, GetXtremeColor(COLOR_3DFACE), GetXtremeColor(COLOR_3DDKSHADOW));
			rect.DeflateRect(1, 1);
			pDC->Draw3dRect(rect, GetXtremeColor(COLOR_WINDOW), GetXtremeColor(COLOR_3DSHADOW));
		}
		
		if (pButton->GetItem()->GetFlags() & SPGridItemHasComboButton)
		{
			CPoint pt = rect.CenterPoint();
			CSPDrawHelpers::Triangle(pDC, CPoint(pt.x -3, pt.y -1), CPoint(pt.x + 3, pt.y -1), CPoint(pt.x, pt.y +2), GetXtremeColor(COLOR_BTNTEXT));
		} 
	}

	if (pButton->GetItem()->GetFlags() & SPGridItemHasExpandButton)
	{		
		pDC->FillSolidRect(ptDots.x - 4, ptDots.y, 2, 2, GetXtremeColor(COLOR_BTNTEXT));
		pDC->FillSolidRect(ptDots.x,  ptDots.y, 2, 2, GetXtremeColor(COLOR_BTNTEXT));
		pDC->FillSolidRect(ptDots.x + 4, ptDots.y, 2, 2, GetXtremeColor(COLOR_BTNTEXT));
	}

	if (CWnd::GetFocus() == pButton)
	{
		pDC->SetTextColor(GetXtremeColor(COLOR_BTNTEXT));
		pDC->SetBkColor(GetXtremeColor(COLOR_3DFACE));
		rect.DeflateRect(2, 2);
		pDC->DrawFocusRect(rect);

	}
}

//////////////////////////////////////////////////////////////////////
//

CSPPropertyGridNativeXPTheme::CSPPropertyGridNativeXPTheme(CSPPropertyGrid* pGrid)
	: CSPPropertyGridPaintManager(pGrid)
{
	m_bThemed = TRUE;

	RefreshMetrics();
}

void CSPPropertyGridNativeXPTheme::RefreshMetrics()
{
	CSPPropertyGridPaintManager::RefreshMetrics();

	SPCurrentSystemTheme systemTheme = SPDrawHelpers()->GetCurrentSystemTheme();

	switch (systemTheme)
	{
		case SPSystemThemeSilver:
		case SPSystemThemeBlue: m_clrShadow = RGB(127, 157, 185); break;
		case SPSystemThemeOlive: m_clrShadow = RGB(164, 185, 127); break;
	}
}

//////////////////////////////////////////////////////////////////////
//

CSPPropertyGridOffice2003Theme::CSPPropertyGridOffice2003Theme(CSPPropertyGrid* pGrid)
	: CSPPropertyGridPaintManager(pGrid)
{
	RefreshMetrics();

}



void CSPPropertyGridOffice2003Theme::RefreshMetrics()
{
	CSPPropertyGridPaintManager::RefreshMetrics();

	m_bThemed = TRUE;
	m_bLunaTheme = FALSE;

	if (!SPColorManager()->IsLunaColorsDisabled())
	{

		CSPPropertyGridView* pView = &m_pGrid->GetGridView();

		SPCurrentSystemTheme systemTheme = SPDrawHelpers()->GetCurrentSystemTheme();

		switch (systemTheme)
		{
		case SPSystemThemeBlue:
			m_clrFace = RGB(221, 236, 254);
			m_clrShadow = RGB(123, 164, 224);
			m_pGrid->m_clrHelpBack.SetStandardValue(m_clrFace);
			pView->m_clrLine.SetStandardValue(RGB(169, 199, 240));
			pView->m_clrCategoryFore.SetStandardValue(0);

			m_bLunaTheme = TRUE;
			break;

		case SPSystemThemeOlive:
			m_clrFace = RGB(243, 242, 231);
			m_clrShadow = RGB(188, 187, 177);
			m_pGrid->m_clrHelpBack.SetStandardValue(m_clrFace);
			pView->m_clrLine.SetStandardValue(RGB(197, 212, 159));
			pView->m_clrCategoryFore.SetStandardValue(0);

			m_bLunaTheme = TRUE;
			break;

		case SPSystemThemeSilver:
			m_clrFace = RGB(238, 238, 244);
			m_clrShadow = RGB(161, 160, 187);
			m_pGrid->m_clrHelpBack.SetStandardValue(m_clrFace);
			pView->m_clrLine.SetStandardValue(RGB(192, 192, 211));
			pView->m_clrCategoryFore.SetStandardValue(0);

			m_bLunaTheme = TRUE;
			break;
		}
	}
}


//////////////////////////////////////////////////////////////////////
// CSPPropertyGridCoolTheme

CSPPropertyGridCoolTheme::CSPPropertyGridCoolTheme(CSPPropertyGrid* pGrid)
	: CSPPropertyGridPaintManager(pGrid)
{
	RefreshMetrics();
}

void CSPPropertyGridCoolTheme::RefreshMetrics()
{
	CSPPropertyGridPaintManager::RefreshMetrics();

	CSPPropertyGridView* pView = &m_pGrid->GetGridView();

	pView->m_clrLine.SetStandardValue(GetXtremeColor(COLOR_3DFACE));

}


void CSPPropertyGridCoolTheme::DrawItem(PDRAWITEMSTRUCT lpDrawItemStruct)
{
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();
	CSPPropertyGridItem* pItem = (CSPPropertyGridItem*)lpDrawItemStruct->itemData;

	COLORREF clrWindow = pView->m_clrBack;

	ASSERT(pItem != NULL);

	CSPWindowRect rcWindow(pView);
	CRect rc = lpDrawItemStruct->rcItem;

	CSPBufferDC dc(lpDrawItemStruct->hDC, rc);
	CSPFontDC font(&dc, &pView->m_fontNormal);

	dc.FillSolidRect(rc, clrWindow);
	dc.SetBkMode(TRANSPARENT);


	CRect rcCaption(rc.left, rc.top, rc.left + pView->GetDividerPos(), rc.bottom);

	COLORREF clrLine = pView->m_clrLine;
	COLORREF clrFore = pView->m_clrFore;
	COLORREF clrShadow = GetXtremeColor(COLOR_3DSHADOW);

	if (pItem->IsCategory())
	{
		dc.FillSolidRect(rc, clrLine);
		CSPFontDC font(&dc, &pView->m_fontBold);
		dc.SetTextColor(pView->m_clrCategoryFore);

		int nIndent = SP_PGI_EXPAND_BORDER * (pItem->GetIndent() + 1);

		if ((lpDrawItemStruct->itemAction | ODA_FOCUS) && (lpDrawItemStruct->itemState & ODS_FOCUS))
		{
			dc.SetTextColor(::GetXtremeColor(COLOR_HIGHLIGHTTEXT));
			dc.FillSolidRect(CRect(nIndent, rc.top , rc.right, rc.bottom), ::GetXtremeColor(COLOR_HIGHLIGHT));
		}

		CRect rcText(nIndent + 3, rc.top , rc.right, rc.bottom - 1);
		dc.DrawText( pItem->GetCaption(),   rcText,  DT_SINGLELINE|DT_NOPREFIX|DT_VCENTER);

		dc.FillSolidRect((pItem->GetIndent() + (pItem->IsExpanded()? 1: 0)) * SP_PGI_EXPAND_BORDER - 1, rc.bottom - 1, rc.Width(), 1, clrShadow);

		CSPPropertyGridItem* pCategory = pItem->GetParentItem();
		while (pCategory != NULL)
		{
			if (pCategory->IsCategory())
			{
				dc.FillSolidRect(rc.left + pCategory->GetIndent() * SP_PGI_EXPAND_BORDER, rc.top, SP_PGI_EXPAND_BORDER, rc.Height(), clrLine);
				dc.FillSolidRect(rc.left + (pCategory->GetIndent() + 1) * SP_PGI_EXPAND_BORDER - 1, rc.top, 1, rc.Height() + 2, clrShadow);
			}
			pCategory = pCategory->GetParentItem();
		}
	}
	else
	{
		dc.SetTextColor(pItem->GetReadOnly()? pView->m_clrReadOnlyFore: clrFore);

		CRect rcValue(rc);
		rcValue.left = rcCaption.right + 4;
		rcValue.bottom -= 1;

		if (!pItem->OnDrawItemValue(dc, rcValue))
		{
			dc.DrawText( pItem->GetViewValue(), rcValue,  DT_SINGLELINE|DT_VCENTER|DT_NOPREFIX);
		}

		BOOL bSelected = ((lpDrawItemStruct->itemAction | ODA_SELECT) && (lpDrawItemStruct->itemState & ODS_SELECTED));
		BOOL bFocused = bSelected && CWnd::GetFocus() && ((CWnd::GetFocus() == pView) || (CWnd::GetFocus()->GetParent() == pView) || (CWnd::GetFocus()->GetOwner() == pView));
		if (bFocused)
		{
			dc.SetTextColor(::GetXtremeColor(COLOR_HIGHLIGHTTEXT));
			dc.FillSolidRect(rcCaption, ::GetXtremeColor(COLOR_HIGHLIGHT));
		}
		else if (bSelected)
		{
			dc.FillSolidRect(rcCaption, clrLine);
		}

		CSPPenDC pen(dc, clrLine);
		dc.MoveTo(0, rc.bottom - 1); dc.LineTo(rc.right, rc.bottom - 1);
		dc.MoveTo(rcCaption.right, rc.top); dc.LineTo(rcCaption.right, rc.bottom - 1);

		CSPPropertyGridItem* pCategory = pItem->GetParentItem();
		while (pCategory != NULL)
		{
			if (pCategory->IsCategory())
			{
				dc.FillSolidRect(rc.left + pCategory->GetIndent() * SP_PGI_EXPAND_BORDER, rc.top, SP_PGI_EXPAND_BORDER, rc.Height(), clrLine);
				dc.FillSolidRect(rc.left + (pCategory->GetIndent() + 1) * SP_PGI_EXPAND_BORDER - 1, rc.top, 1, rc.Height() + 2, clrShadow);
			}
			pCategory = pCategory->GetParentItem();
		}

		CRect rcText(rc);
		rcText.left = pItem->GetIndent() * SP_PGI_EXPAND_BORDER + 3;
		rcText.right = rcCaption.right - 1;
		rcText.bottom -= 1;
		dc.DrawText( pItem->GetCaption(),   rcText,  DT_SINGLELINE|DT_NOPREFIX|DT_VCENTER);
	}

	CSPPropertyGridItem* pNext = (CSPPropertyGridItem*)pView->GetItemDataPtr(lpDrawItemStruct->itemID + 1);
	if (((ULONG_PTR)pNext == (ULONG_PTR)(-1)))
		dc.FillSolidRect(0, rc.bottom - 1, rc.Width(), 1, clrShadow);
	else if (pNext && pNext->IsCategory() && pNext->IsVisible())
	{
		dc.FillSolidRect(pNext->GetIndent() * SP_PGI_EXPAND_BORDER, rc.bottom - 1, rc.Width(), 1, clrShadow);
	}

	CSPPropertyGridItem* pPrev = pItem;
	CSPPropertyGridItem* pCategory = pPrev->GetParentItem();
	while (pCategory != NULL)
	{
		if (pItem->HasChilds() && pItem->IsExpanded())
			break;

		if (pCategory->IsCategory() && pCategory->IsVisible() && pCategory->GetChilds()->GetAt(pCategory->GetChilds()->GetCount() - 1) == pPrev)
		{
			dc.FillSolidRect(pCategory->GetIndent() * SP_PGI_EXPAND_BORDER, rc.bottom - 1, rc.Width(), 1, clrShadow);
		} else
			break;
		pPrev = pCategory;
		pCategory = pPrev->GetParentItem();
	}


	if (pItem->HasChilds())
	{
		CRgn rgn;
		if (!pItem->IsCategory())
		{
			rgn.CreateRectRgnIndirect(&rcCaption);
			dc.SelectClipRgn(&rgn);
		}

		DrawExpandButton(dc, pItem, rcCaption);

		dc.SelectClipRgn(NULL);
	}
}



//////////////////////////////////////////////////////////////////////
// CSPPropertyGridSimpleTheme

CSPPropertyGridSimpleTheme::CSPPropertyGridSimpleTheme(CSPPropertyGrid* pGrid)
	: CSPPropertyGridPaintManager(pGrid)
{
	RefreshMetrics();
}

void CSPPropertyGridSimpleTheme::RefreshMetrics()
{
	CSPPropertyGridPaintManager::RefreshMetrics();

	CSPPropertyGridView* pView = &m_pGrid->GetGridView();

	pView->m_clrLine.SetStandardValue(GetXtremeColor(COLOR_3DFACE));

}



void CSPPropertyGridSimpleTheme::DrawItem(PDRAWITEMSTRUCT lpDrawItemStruct)
{
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();
	CSPPropertyGridItem* pItem = (CSPPropertyGridItem*)lpDrawItemStruct->itemData;

	COLORREF clrWindow = pView->m_clrBack;

	ASSERT(pItem != NULL);


	CSPWindowRect rcWindow(pView);
	CRect rc = lpDrawItemStruct->rcItem;

	CSPBufferDC dc(lpDrawItemStruct->hDC, rc);
	CSPFontDC font(&dc, pItem->IsCategory()? &pView->m_fontBold: &pView->m_fontNormal);

	dc.FillSolidRect(rc, clrWindow);
	dc.SetBkMode(TRANSPARENT);


	CRect rcCaption(rc.left, rc.top, rc.left + pView->GetDividerPos(), rc.bottom);

	COLORREF clrLine = pView->m_clrLine;
	COLORREF clrFore = pView->m_clrFore;

	{
		if (!pItem->IsCategory())
		{

			dc.SetTextColor(pItem->GetReadOnly()? pView->m_clrReadOnlyFore: clrFore);

			CRect rcValue(rc);
			rcValue.left = rcCaption.right + 4;
			rcValue.bottom -= 1;

			if (!pItem->OnDrawItemValue(dc, rcValue))
			{
				dc.DrawText( pItem->GetViewValue(), rcValue,  DT_SINGLELINE|DT_VCENTER|DT_NOPREFIX);
			}
		}

		dc.SetTextColor(clrFore);

		BOOL bSelected = ((lpDrawItemStruct->itemAction | ODA_SELECT) && (lpDrawItemStruct->itemState & ODS_SELECTED));
		BOOL bFocused = bSelected && CWnd::GetFocus() && ((CWnd::GetFocus() == pView) || (CWnd::GetFocus()->GetParent() == pView) || (CWnd::GetFocus()->GetOwner() == pView));
		if (bFocused)
		{
			dc.SetTextColor(::GetXtremeColor(COLOR_HIGHLIGHTTEXT));
			dc.FillSolidRect(CRect(SP_PGI_EXPAND_BORDER, rcCaption.top, rcCaption.right, rcCaption.bottom), ::GetXtremeColor(COLOR_HIGHLIGHT));
		}
		else if (bSelected)
		{
			dc.FillSolidRect(CRect(SP_PGI_EXPAND_BORDER, rcCaption.top, rcCaption.right, rcCaption.bottom), clrLine);
		}
		dc.FillSolidRect(SP_PGI_EXPAND_BORDER - 1, rc.top, 1, rc.Height(), clrLine);

		CSPPenDC pen(dc, clrLine);
		dc.MoveTo(SP_PGI_EXPAND_BORDER, rc.bottom - 1); dc.LineTo(rc.right, rc.bottom - 1);
		dc.MoveTo(rcCaption.right, rc.top); dc.LineTo(rcCaption.right, rc.bottom - 1);


		CRect rcText(rc);
		rcText.left = (pItem->GetIndent() + (pItem->IsCategory()? 1: 0)) * SP_PGI_EXPAND_BORDER + 3;
		rcText.right = rcCaption.right - 1;
		rcText.bottom -= 1;
		dc.DrawText( pItem->GetCaption(),   rcText,  DT_SINGLELINE|DT_NOPREFIX|DT_VCENTER);
	}

	if (pView->GetCount() == (int)lpDrawItemStruct->itemID + 1)
		dc.FillSolidRect(0, rc.bottom - 1, rc.Width(), 1, clrLine);

	if (pItem->HasChilds())
	{
		CRgn rgn;
		rgn.CreateRectRgnIndirect(&rcCaption);
		dc.SelectClipRgn(&rgn);

		DrawExpandButton(dc, pItem, rcCaption);

		dc.SelectClipRgn(NULL);
	}
}



//////////////////////////////////////////////////////////////////////
// CSPPropertyGridDelphiTheme

CSPPropertyGridDelphiTheme::CSPPropertyGridDelphiTheme(CSPPropertyGrid* pGrid)
	: CSPPropertyGridPaintManager(pGrid)
{
	RefreshMetrics();
}


void CSPPropertyGridDelphiTheme::DrawItem(PDRAWITEMSTRUCT lpDrawItemStruct)
{
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();
	CSPPropertyGridItem* pItem = (CSPPropertyGridItem*)lpDrawItemStruct->itemData;

	COLORREF clrWindow = pView->m_clrBack;

	ASSERT(pItem != NULL);


	CSPWindowRect rcWindow(pView);
	CRect rc = lpDrawItemStruct->rcItem;

	CSPBufferDC dc(lpDrawItemStruct->hDC, rc);
	CSPFontDC font(&dc, pItem->IsCategory()? &pView->m_fontBold: &pView->m_fontNormal);

	dc.FillSolidRect(rc, clrWindow);
	dc.SetBkMode(TRANSPARENT);


	CRect rcCaption(rc.left, rc.top, rc.left + pView->GetDividerPos(), rc.bottom);

	COLORREF clrFore = pView->m_clrFore;
	COLORREF clrShadow = GetXtremeColor(COLOR_3DSHADOW);
	COLORREF clrLight = GetXtremeColor(COLOR_BTNHIGHLIGHT);

	{
		if (!pItem->IsCategory())
		{

			dc.SetTextColor(pItem->GetReadOnly()? pView->m_clrReadOnlyFore: clrFore);

			CRect rcValue(rc);
			rcValue.left = rcCaption.right + 4;
			rcValue.bottom -= 1;

			if (!pItem->OnDrawItemValue(dc, rcValue))
			{
				dc.DrawText( pItem->GetViewValue(), rcValue,  DT_SINGLELINE|DT_VCENTER|DT_NOPREFIX);
			}
		}

		dc.SetTextColor(clrFore);

		dc.Draw3dRect(rcCaption.right - 1, rc.top, 1, rc.Height(), clrShadow, clrShadow);
		dc.Draw3dRect(rcCaption.right, rc.top, 1, rc.Height(), clrLight, clrLight);

		BOOL bSelected = ((lpDrawItemStruct->itemAction | ODA_SELECT) && (lpDrawItemStruct->itemState & ODS_SELECTED));
		BOOL bFocused = bSelected && CWnd::GetFocus() && ((CWnd::GetFocus() == pView) || (CWnd::GetFocus()->GetParent() == pView) || (CWnd::GetFocus()->GetOwner() == pView));
		if (bFocused || bSelected)
		{
			dc.Draw3dRect(rc, clrFore, clrLight);
		}
		else
		{
			for (int i = 0; i < rc.right; i += 2)
				dc.SetPixel(i, rc.bottom - 1, clrShadow);
		}


		CRect rcText(rc);
		rcText.left = (pItem->GetIndent() + (pItem->IsCategory()? 1: 0)) * SP_PGI_EXPAND_BORDER + 3;
		rcText.right = rcCaption.right - 1;
		rcText.bottom -= 1;
		dc.DrawText( pItem->GetCaption(),   rcText,  DT_SINGLELINE|DT_NOPREFIX|DT_VCENTER);
	}

	if (pItem->HasChilds())
	{
		CRgn rgn;
		rgn.CreateRectRgnIndirect(&rcCaption);
		dc.SelectClipRgn(&rgn);

		DrawExpandButton(dc, pItem, rcCaption);

		dc.SelectClipRgn(NULL);
	}
}

void CSPPropertyGridDelphiTheme::RefreshMetrics()
{
	CSPPropertyGridPaintManager::RefreshMetrics();

	CSPPropertyGridView* pView = &m_pGrid->GetGridView();

	pView->m_clrBack.SetStandardValue(GetXtremeColor(COLOR_3DFACE));
	pView->m_clrFore.SetStandardValue(RGB(0, 0, 128));

}

//////////////////////////////////////////////////////////////////////////
// CSPPropertyGridWhidbeyTheme

CSPPropertyGridWhidbeyTheme::CSPPropertyGridWhidbeyTheme(CSPPropertyGrid* pGrid)
	: CSPPropertyGridPaintManager(pGrid)
{
	m_bThemed = TRUE;
	RefreshMetrics();
}

void CSPPropertyGridWhidbeyTheme::DrawCategoryCaptionBackground(CDC* pDC, CRect rc)
{
	CSPPropertyGridView* pView = &m_pGrid->GetGridView();

	pDC->FillSolidRect(rc, pView->m_clrLine);

	SPDrawHelpers()->GradientFill(pDC,
		CRect(rc.left, rc.bottom - 2, rc.right, rc.bottom - 1), pView->m_clrLine, GetXtremeColor(COLOR_3DHIGHLIGHT), TRUE);

}

void CSPPropertyGridWhidbeyTheme::RefreshMetrics()
{
	CSPPropertyGridPaintManager::RefreshMetrics();

	CSPPropertyGridView* pView = &m_pGrid->GetGridView();

	pView->m_clrLine.SetStandardValue(GetXtremeColor(COLOR_3DFACE));
	pView->m_clrCategoryFore.SetStandardValue(GetXtremeColor(COLOR_BTNTEXT));
	m_clrShadow = GetXtremeColor(XPCOLOR_STATICFRAME);
}

void CSPPropertyGridWhidbeyTheme::DrawExpandButton(CDC& dc, CSPPropertyGridItem* pItem, CRect rcCaption)
{

	CRect rcButton(CPoint(SP_PGI_EXPAND_BORDER / 2 - 5, rcCaption.CenterPoint().y - 4), CSize(9, 9));

	if (pItem->GetIndent() > 0)
		rcButton.OffsetRect((pItem->GetIndent() - (pItem->IsCategory()? 0: 1)) * SP_PGI_EXPAND_BORDER, 0);

	CDC* pDC = &dc;

	COLORREF clrButton = GetXtremeColor(COLOR_3DFACE);
	COLORREF clrFrame = GetXtremeColor(COLOR_3DSHADOW);
	pDC->FillSolidRect(rcButton.left + 1, rcButton.top + 1, 7, 7, clrButton);
	pDC->FillSolidRect(rcButton.left + 1, rcButton.top, 9 - 2, 1, clrFrame);
	pDC->FillSolidRect(rcButton.left + 1, rcButton.bottom - 1, 9 - 2, 1, clrFrame);
	pDC->FillSolidRect(rcButton.left, rcButton.top + 1, 1, 9 - 2, clrFrame);
	pDC->FillSolidRect(rcButton.right - 1, rcButton.top + 1, 1, 9 - 2, clrFrame);
	pDC->FillSolidRect(rcButton.left + 1, rcButton.top + 1, 9 - 2, 3, 0xFFFFFF);
	pDC->FillSolidRect(rcButton.left + 1, rcButton.top + 4, 9 - 4, 2, 0xFFFFFF);

	pDC->FillSolidRect(rcButton.left + 2, rcButton.top + 4, 9 - 4, 1, 0);
	if (!pItem->IsExpanded())
		pDC->FillSolidRect(rcButton.left + 4, rcButton.top + 2, 1, 9 - 4, 0);
}
