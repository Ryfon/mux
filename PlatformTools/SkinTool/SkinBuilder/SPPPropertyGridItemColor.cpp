﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/

#include "StdAfx.h"
#include "SPVC80Helpers.h"
#include "SPDrawHelpers.h"

#include "SPPropertyGridInplaceEdit.h"
#include "SPPropertyGridInplaceButton.h"
#include "SPPropertyGridInplaceList.h"
#include "SPPropertyGridItem.h"
#include "SPPropertyGridItemColor.h"
#include "SPPropertyGrid.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemColor

IMPLEMENT_DYNAMIC(CSPPropertyGridItemColor, CSPPropertyGridItem)

CSPPropertyGridItemColor::CSPPropertyGridItemColor(CString strCaption, COLORREF clr, COLORREF* pBindColor)
	: CSPPropertyGridItem(strCaption)
{
	m_pBindColor = pBindColor;
	SetColor(clr);
	m_nFlags = SPGridItemHasEdit | SPGridItemHasExpandButton;
}

CSPPropertyGridItemColor::CSPPropertyGridItemColor(UINT nID, COLORREF clr, COLORREF* pBindColor)
	: CSPPropertyGridItem(nID)
{
	m_pBindColor = pBindColor;
	SetColor(clr);
	m_nFlags = SPGridItemHasEdit | SPGridItemHasExpandButton;
}

CSPPropertyGridItemColor::~CSPPropertyGridItemColor()
{
}

/////////////////////////////////////////////////////////////////////////////
//

BOOL CSPPropertyGridItemColor::OnDrawItemValue(CDC& dc, CRect rcValue)
{
	COLORREF clr = dc.GetTextColor();
	CRect rcSample(rcValue.left - 2, rcValue.top + 1, rcValue.left + 18, rcValue.bottom - 1);
	CSPPenDC pen(dc, clr);
	CSPBrushDC brush(dc, m_clrValue);
	dc.Rectangle(rcSample);

	CRect rcText(rcValue);
	rcText.left += 25;

	dc.DrawText( m_strValue, rcText,  DT_SINGLELINE|DT_VCENTER);

	return TRUE;
}

CRect CSPPropertyGridItemColor::GetValueRect()
{
	CRect rcValue(CSPPropertyGridItem::GetValueRect());
	rcValue.left += 25;
	return rcValue;
}

COLORREF AFX_CDECL CSPPropertyGridItemColor::StringToRGB(CString str)
{
	CString strRed, strGreen, strBlue;

	AfxExtractSubString(strRed, str, 0, ';');
	AfxExtractSubString(strGreen, str, 1, ';');
	AfxExtractSubString(strBlue, str, 2, ';');

	return RGB(__min(_ttoi(strRed), 255), __min(_ttoi(strGreen), 255), __min(_ttoi(strBlue), 255));
}

CString AFX_CDECL CSPPropertyGridItemColor::RGBToString(COLORREF clr)
{
	CString str;
	str.Format(_T("%i; %i; %i"), GetRValue(clr), GetGValue(clr), GetBValue(clr));
	return str;
}

void CSPPropertyGridItemColor::SetValue(CString strValue)
{
	SetColor(StringToRGB(strValue));
}

void CSPPropertyGridItemColor::SetColor(COLORREF clr)
{
	m_clrValue = clr;

	if (m_pBindColor)
	{
		*m_pBindColor = clr;
	}

	CSPPropertyGridItem::SetValue(RGBToString(clr));
}

void CSPPropertyGridItemColor::BindToColor(COLORREF* pBindColor)
{
	m_pBindColor = pBindColor;
	if (m_pBindColor)
	{
		*m_pBindColor = m_clrValue;
	}
}


void CSPPropertyGridItemColor::OnBeforeInsert()
{
	if( m_pBindColor && *m_pBindColor != m_clrValue)
	{
		SetColor(*m_pBindColor);
	}
}


void CSPPropertyGridItemColor::OnInplaceButtonDown()
{
#ifdef _INCLUDE_CONTROLS
	CColorDialog dlg( m_clrValue, m_clrValue,
		CPS_SP_SHOW3DSELECTION|CPS_SP_SHOWHEXVALUE, m_pGrid );
#else
	CColorDialog dlg( m_clrValue, 0, m_pGrid );
#endif

	if ( dlg.DoModal( ) == IDOK )
	{
		OnValueChanged( RGBToString( dlg.GetColor( ) ) );
		m_pGrid->Invalidate( FALSE );
	}
}



