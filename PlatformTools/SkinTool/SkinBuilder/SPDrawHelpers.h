﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2006年3月
*********************************************/

#if !defined(__SPDRAWHELPERS_H__)
#define __SPDRAWHELPERS_H__
#if _MSC_VER > 1000
#pragma once
#endif

#include "SPColorManager.h"

//===========================================================================
// 功能:
//     辅助类，用来获取位图中的透明色。
//===========================================================================
class  CSPTransparentBitmap
{
public:
	CSPTransparentBitmap(HBITMAP hBitmap);

	//-----------------------------------------------------------------------
	// 功能:
	//    获取位图的透明色。
	//-----------------------------------------------------------------------
	COLORREF GetTransparentColor() const;

	//-----------------------------------------------------------------------
	// 功能:
	//     基于位图创建图标。
	//-----------------------------------------------------------------------
	HICON ConvertToIcon() const;

protected:

	HBITMAP m_hBitmap; 
};

//===========================================================================
// 功能:
//     辅助类，用来获取光标在客户区中的位置。
//===========================================================================
class  CSPClientCursorPos : public CPoint
{
public:

	CSPClientCursorPos(CWnd* pWnd);
};

//===========================================================================
// 功能:
//     辅助类，用来创建空的CRect对象。
//===========================================================================
class  CSPEmptyRect : public CRect
{
public:

	CSPEmptyRect();
};

//===========================================================================
// 功能:
//     辅助类，用来获取制定窗口的大小。
//===========================================================================
class  CSPWindowRect : public CRect
{
public:

	CSPWindowRect(HWND hWnd);

	CSPWindowRect(const CWnd* pWnd);
};

//===========================================================================
// 功能：
//     辅助类，用来获取制定窗口的客户区位置。
//===========================================================================
class  CSPClientRect : public CRect
{
public:

	CSPClientRect(HWND hWnd);

	CSPClientRect(const CWnd* pWnd);
};

//===========================================================================
// 功能:
//     辅助类，用来创建缓存设备内容。
//===========================================================================
class  CSPBufferDC : public CDC
{
public:

	CSPBufferDC(HDC hDestDC, const CRect rcPaint);
	CSPBufferDC(CDC *pDC, const CRect rcPaint);

	virtual ~CSPBufferDC();

	CDC* GetDestDC();

protected:

	HDC     m_hDestDC;    // 指向目标的设备内容句柄
	CBitmap m_bitmap;     // 内存设备内容中的位图
	CRect   m_rect;       // 绘制区域
	HGDIOBJ m_hOldBitmap; // 先前被选进设备内容的位图
};

//===========================================================================
// 功能:
//     辅助类，用来选择字体到指定的设备内容。
//===========================================================================
class  CSPFontDC
{
public:

	CSPFontDC(CDC* pDC, CFont* pFont);

	virtual ~CSPFontDC();

	//-----------------------------------------------------------------------
	// 功能：
	//     更改设备内容的字体。
	void SetFont(CFont* pFont);

protected:

	HDC     m_hDC;     
	HGDIOBJ m_hOldFont; 
};

//===========================================================================
// 功能:
//    辅助类，创建指定颜色的笔并选进设备内容。
//===========================================================================
class  CSPPenDC
{
public:

	CSPPenDC(HDC hDC, COLORREF crColor);

	CSPPenDC(CDC *pDC, COLORREF crColor);

	virtual ~CSPPenDC();

	void Color(COLORREF crColor);

	COLORREF Color();

protected:

	CPen m_pen;    
	HDC  m_hDC;   
	HPEN m_hOldPen;
};

//===========================================================================
// 功能:
//    辅助类，创建指定颜色的画刷并选进设备内容。
//===========================================================================
class  CSPBrushDC
{
public:

	CSPBrushDC(HDC hDC, COLORREF crColor);

	CSPBrushDC(CDC *pDC, COLORREF crColor);

	virtual ~CSPBrushDC();

	void Color(COLORREF crColor);

protected:

	CBrush m_brush;     
	HDC    m_hDC;       
	HBRUSH m_hOldBrush; 
};

//===========================================================================
// 功能:
//    辅助类，创建同制定设备相兼容的内存设备。
//===========================================================================
class  CSPCompatibleDC : public CDC
{
public:

	CSPCompatibleDC(CDC* pDC, CBitmap* pBitmap);

	virtual ~CSPCompatibleDC();

protected:

	CBitmap* m_pBmp;
};

//===========================================================================
// 功能:
//    辅助类，用来跟踪Splitter区域。
//===========================================================================
class  CSPSplitterTracker
{
public:

	CSPSplitterTracker(BOOL bSolid = FALSE);

public:
	//-----------------------------------------------------------------------
	// 功能：
	//     用来跟踪splitter的大小。
	//-----------------------------------------------------------------------
	BOOL Track(CWnd* pTrackWnd, CRect rcAvail, CRect& rectTracker, CPoint point, BOOL bHoriz);

	//-----------------------------------------------------------------------
	// 功能:
	//     设置控件的角框。
	//-----------------------------------------------------------------------
	void SetBoundRect(CRect rcBoundRect);

private:
	//-----------------------------------------------------------------------
	// 功能:
	//     翻转指定位置的象素，对设备上下文的每个象素进行非操作。
	//-----------------------------------------------------------------------
	void OnInvertTracker(CRect rc);

private:
	CDC* m_pDC;
	BOOL m_bSolid;
	CRect m_rcBoundRect;
};

//===========================================================================
// 功能:
//     辅助类，用来监视那嵌控件的鼠标消息。
//===========================================================================
class  CSPMouseMonitor
{
public:
	//-----------------------------------------------------------------------
	// 功能:
	//    调用此函数来监视鼠标消息。
	static void SetupHook(CWnd* pWndMonitor);

protected:
	
	static LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam);

private:
	static HHOOK m_hHookMouse;      // A handle to a hook.
	static CWnd* m_pWndMonitor;     // A pointer to the in-place control.
};


//===========================================================================
// 功能：
//     辅助类，用来进行各种绘制任务。
//===========================================================================
class  CSPDrawHelpers
{
protected:
	//===========================================================================
	// 功能:
	//     指向 Windows 的 GradientFill 函数的函数指针。
	//===========================================================================
	typedef BOOL (__stdcall *PFNGRADIENTFILL)(HDC, PTRIVERTEX, ULONG, PVOID, ULONG, ULONG);

public:
	
	CSPDrawHelpers();

	
	virtual ~CSPDrawHelpers();

private:
	HMODULE m_hMsImgDll; // Handle to MsImg32.dll.

public:
	//-----------------------------------------------------------------------
	// 功能:
	//     绘制渐变颜色。
	//-----------------------------------------------------------------------
	void GradientFill(CDC* pDC, LPRECT lpRect, COLORREF crFrom, COLORREF crTo, BOOL bHorz);

	//-----------------------------------------------------------------------
	// 功能:
	//     绘制渐变颜色。
	//-----------------------------------------------------------------------
	void GradientFill(CDC* pDC, LPRECT lpRect, CSPPaintManagerColorGradient& grc, BOOL bHorz);

	//-----------------------------------------------------------------------
	// 功能:
	//    剪切矩形四个顶角。
	//-----------------------------------------------------------------------
	void ExcludeCorners(CDC* pDC, CRect rc);

	//-----------------------------------------------------------------------
	// 功能:
	//     混合指定点象素。
	//-----------------------------------------------------------------------
	void BlurPoints(CDC* pDC, LPPOINT pts, int nCount);

	//-----------------------------------------------------------------------
	// 功能:
	//    混合指定的颜色。
	//-----------------------------------------------------------------------
	COLORREF BlendColors(COLORREF crA, COLORREF crB, float fAmountA);

	//-----------------------------------------------------------------------
	// 功能:
	//     判断指定的字体是否存在。
	//-----------------------------------------------------------------------
	static BOOL AFX_CDECL FontExists(CString& strFaceName);

	//-----------------------------------------------------------------------
	// 功能:
	//    获取默认字体。
	//-----------------------------------------------------------------------
	static CString AFX_CDECL GetDefaultFontName();

	//-----------------------------------------------------------------------
	// 功能:
	//    获取垂直字体名。
	//-----------------------------------------------------------------------
	CString GetVerticalFontName(BOOL bUseOfficeFont);

	//-----------------------------------------------------------------------
	// 功能:
	//     绘制三角形。
	//-----------------------------------------------------------------------
	static void AFX_CDECL Triangle(CDC* pDC, CPoint pt0, CPoint pt1, CPoint pt2, COLORREF clr);
	static void AFX_CDECL Triangle(CDC* pDC, CPoint pt0, CPoint pt1, CPoint pt2); // <combine CSPDrawHelpers::Triangle@CDC*@CPoint@CPoint@CPoint@COLORREF>

	//-----------------------------------------------------------------------
	// 功能:
	//     判断当前系统是否低分辨率。
	//-----------------------------------------------------------------------
	static BOOL AFX_CDECL IsLowResolution(HDC hDC = 0);

	//-----------------------------------------------------------------------
	// 功能:
	//    获取当前XP风格。
	//-----------------------------------------------------------------------
	SPCurrentSystemTheme GetCurrentSystemTheme();

	//-----------------------------------------------------------------------
	// 功能:
	//     判断当前渐变色是否快速绘制。
	//-----------------------------------------------------------------------
	BOOL IsFastFillAvailable();

	//-----------------------------------------------------------------------
	// 功能:
	//     使用渐变色填充矩形或三角形。
	//-----------------------------------------------------------------------
	BOOL GradientFill(HDC hdc, PTRIVERTEX pVertex, ULONG dwNumVertex, PVOID pMesh, ULONG dwNumMesh, ULONG dwMode);

	//-----------------------------------------------------------------------
	// 功能:
	//     获取当前的工作区域。
	//-----------------------------------------------------------------------
	static CRect AFX_CDECL GetWorkArea();
	static CRect AFX_CDECL GetWorkArea(const CRect& rect); //<COMBINE CSPDrawHelpers::GetWorkArea>
	static CRect AFX_CDECL GetWorkArea(const CWnd* pWnd);  //<COMBINE CSPDrawHelpers::GetWorkArea>

private:

	PFNGRADIENTFILL m_pfnFastGradientFill;

	void GradientFillFast(CDC* pDC, LPRECT lpRect, COLORREF crFrom, COLORREF crTo, BOOL bHorz);
	void GradientFillSlow(CDC* pDC, LPRECT lpRect, COLORREF crFrom, COLORREF crTo, BOOL bHorz);

	static CSPDrawHelpers m_instance;         
	friend CSPDrawHelpers* SPDrawHelpers();  
};


CSPDrawHelpers* SPDrawHelpers();


AFX_INLINE CSPDrawHelpers* SPDrawHelpers() {
	return &CSPDrawHelpers::m_instance;
}
AFX_INLINE BOOL CSPDrawHelpers::IsFastFillAvailable() {
	return m_pfnFastGradientFill != NULL;
}
AFX_INLINE void CSPDrawHelpers::Triangle(CDC* pDC, CPoint pt0, CPoint pt1, CPoint pt2) {
	CPoint pts[] = {pt0, pt1, pt2};
	pDC->Polygon(pts, 3);
}
AFX_INLINE void CSPSplitterTracker::SetBoundRect(CRect rcBoundRect) {
	m_rcBoundRect = rcBoundRect;
}

/////////////////////////////////////////////////////////////////////////////

#endif 
