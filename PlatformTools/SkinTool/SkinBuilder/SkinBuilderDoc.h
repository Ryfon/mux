﻿// SkinBuilderDoc.h :  CSkinBuilderDoc 类的接口
//


#pragma once



class CSkinBuilderDoc : public CDocument
{
protected: // 仅从序列化创建
	CSkinBuilderDoc();
	DECLARE_DYNCREATE(CSkinBuilderDoc)

public:
	CSkin *m_pSkin;
	CSkinBitmap *m_pCurrentImage;
	CObjectSkin  *m_pCurrentObject;
	CWinButtonSkin *m_pCurrentWinButton;
	int			 m_nSelectedType;
	float		 m_fZoom;

public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

public:
	virtual ~CSkinBuilderDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	afx_msg void OnObjectsLoadimage();
	afx_msg void OnWindowFramewindow();
	afx_msg void OnWindowlessWinbuttonskin();
	afx_msg void OnUpdateWindowlessWinbuttonskin(CCmdUI *pCmdUI);
	afx_msg void OnStandardcontrolsSkintoolbar();
	afx_msg void OnStandardcontrolsSkinscrollbar();
	afx_msg void OnStandardcontrolsSkinbutton();
	afx_msg void OnStandardcontrolsSkinradio();
	afx_msg void OnStandardcontrolsSkinedit();
	afx_msg void OnStandardcontrolsSkincheckbox();
	afx_msg void OnStandardcontrolsSkingroupbox();
	afx_msg void OnStandardcontrolsSkinstatic();
	afx_msg void OnStandardcontrolsSkincombobox();
	afx_msg void OnStandardcontrolsSkinlistbox();
	afx_msg void OnStandardcontrolsSkinpopupmenu();
	afx_msg void OnStandardcontrolsSkinspin();
	afx_msg void OnStandardcontrolsSkinheader();
	afx_msg void OnStandardcontrolsSkinprogress();
	afx_msg void OnStandardcontrolsSkinslider();
	afx_msg void OnStandardcontrolsSkintab();
	afx_msg void OnStandardcontrolsSkinstatusbar();
	afx_msg void OnEditDelete();
	afx_msg void OnObjectsUnloadimage();
};


