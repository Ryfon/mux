﻿#pragma once


// CPropertyView view
#include "SkinBuilderDoc.h"

class CPropertyView : public CView
{
	DECLARE_DYNCREATE(CPropertyView)

protected:
	CPropertyView();           // protected constructor used by dynamic creation
	virtual ~CPropertyView();

public:
	CSPPropertyGrid m_wndPropertyGrid;
	CSPPropertyGrid m_wndPropertyApplication;
	CSPPropertyGrid m_wndPropertyWindow;
	CSPPropertyGrid m_wndPropertyWinButton;
	CSPPropertyGrid m_wndPropertyScrollBar;

	CSPPropertyGrid m_wndPropertyButton;
	CSPPropertyGrid m_wndPropertyRadio;
	CSPPropertyGrid m_wndPropertyCheckBox;
	CSPPropertyGrid m_wndPropertyGroupBox;
	CSPPropertyGrid m_wndPropertyStatic;
	CSPPropertyGrid m_wndPropertyEdit;
	CSPPropertyGrid m_wndPropertyComboBox;
	CSPPropertyGrid m_wndPropertyListBox;
	CSPPropertyGrid m_wndPropertyHeader;
	CSPPropertyGrid m_wndPropertySpin;
	CSPPropertyGrid m_wndPropertyToolBar;
	CSPPropertyGrid m_wndPropertyStatusBar;
	CSPPropertyGrid m_wndPropertySlider;
	CSPPropertyGrid m_wndPropertyPopupMenu;
	CSPPropertyGrid m_wndPropertyProgress;
	CSPPropertyGrid m_wndPropertyTab;


	CString m_strAssemblyName;
	CString m_strOutputType;

	
public:
	void	CreatePropertyApplication(ApplicationSkin *pAllicationSkin);
	void	CreatePropertyWindow();
	void	CreatePropertyWindButton();
	void	CreatePropertyScrollBar();
	void	CreatePropertyButton();
	void	CreatePropertyRadio();
	void	CreatePropertyCheckBox();
	void	CreatePropertyGroupBox();
	void	CreatePropertyEdit();
	void	CreatePropertyListBox();
	void	CreatePropertyComboBox();
	void	CreatePropertyStatic();
	void	CreatePropertyHeader();
	void	CreatePropertySpin();
	void	CreatePropertyToolBar();
	void	CreatePropertyStatusBar();
	void	CreatePropertySlider();
	void	CreatePropertyPopupMenu();
	void	CreatePropertyProgress();
	void	CreatePropertyTab();



	void	SetWindowSkin(CWindowSkin *pwinSkin);
	void	SetApplication(ApplicationSkin *pApp);
	void	SetWinButtonSkin(CWinButtonSkin *pWinButton);
	void	SetScroolBarSkin(CScrollBarSkin *pScrollBar);

	void	SetButtonSkin(CButtonSkin *pButtonSkin);
	void	SetCheckBoxSkin(CCheckBoxSkin *pCheckBoxSkin);
	void	SetRadioSkin(CRadioSkin *pRadioSkin);
	void	SetGroupBoxSkin(CGroupBoxSkin *pGroupBoxSkin);
	void	SetStaticSkin(CStaticSkin *pStaticSkin);
	void	SetEditSkin(CEditSkin *pEditSkin);
	void	SetComboxSkin(CComboBoxSkin *pComboBoxSkin);
	void	SetListBoxSkin(CListBoxSkin *pListBoxSkin);
	void	SetPopupMenuSkin(CPopupMenuSkin *pPopupMenuSkin);
	void	SetSpinSkin(CSpinSkin *pSpinSkin);
	void	SetProgressSkin(CProgressSkin *pProgressSkin);
	void	SetSliderSkin(CSliderSkin *pSliderSkin);
	void	SetHeaderSkin(CHeaderSkin *pHeaderSkin);
	void	SetTabSkin(CTabSkin *pTabSkin);
	void	SetStatusSkin(CStatusBarSkin *pStatusbarSkin);
	void	SetToolBarSkin(CToolBarSkin *pToolbarSkin);
//	void	SetWindowSkin(CWindowSkin *pwinSkin);

	CSkinBuilderDoc* GetDocument() const;

	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnGridNotify(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

inline CSkinBuilderDoc* CPropertyView::GetDocument() const
{ return reinterpret_cast<CSkinBuilderDoc*>(m_pDocument); }
