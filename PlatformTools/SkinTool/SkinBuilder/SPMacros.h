﻿
#if !defined(__SPMACROS_H__)
#define __SPMACROS_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define AFX_WNDCOMMCTL_DATE_REG         0x20000
#define AfxDeferRegisterClass(fClass) AfxEndDeferRegisterClass(fClass)

#define SAFE_DELETE(ptr)
#undef SAFE_DELETE
#define SAFE_DELETE(ptr) \
	if(ptr) { delete ptr; ptr = NULL; }


#define SAFE_DELETE_AR(ptr)
#undef SAFE_DELETE_AR
#define SAFE_DELETE_AR(ptr) \
	if(ptr) { delete [] ptr; ptr = NULL; }


#define SAFE_RELEASE(comPointer)
#undef SAFE_RELEASE
#define SAFE_RELEASE(comPointer) \
	if(comPointer) { (comPointer)->Release(); (comPointer)=NULL; }


#define SAFE_CALLPTR(classPointer, functionName)
#undef SAFE_CALLPTR
#define SAFE_CALLPTR(classPointer, functionName) \
	if (classPointer) classPointer->functionName



#ifdef _DEBUG
#ifndef _AFX_NO_DEBUG_CRT
#define XT_ASSERT_MSG(exp, msg)

#undef XT_ASSERT_MSG
#define XT_ASSERT_MSG(exp, msg) \
{ \
	if ( !(exp) && (_CrtDbgReport( _CRT_ASSERT, __FILE__, __LINE__, NULL, "\n-----------------------\n" msg "\n-----------------------" ) ) ) \
		AfxDebugBreak(); \
} \

#else
#define XT_ASSERT_MSG(exp, msg) (void)( (exp) || (_assert("\n-----------------------\n" msg "\n-----------------------", __FILE__, __LINE__), 0) )
#endif//_AFX_NO_DEBUG_CRT

#else
#define XT_ASSERT_MSG(exp, msg) ((void)0)
#endif//_DEBUG

#ifdef _UNICODE
#ifndef UNICODE
#define UNICODE
#endif
#endif

#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define __LOC__ __FILE__ "("__STR1__(__LINE__)"): warning : "

#ifdef _AFXDLL
	#define SAFE_MANAGE_STATE(s) AFX_MANAGE_STATE(s)
#else
	#define SAFE_MANAGE_STATE(s)
#endif

#ifndef AFX_INLINE
#define AFX_INLINE inline
#endif

#ifndef CBRS_GRIPPER
#define CBRS_GRIPPER                    0x00400000L
#endif

#ifndef WS_EX_LAYOUTRTL
#define WS_EX_LAYOUTRTL                 0x00400000L
#endif

#ifndef _countof
#define _countof(array) (sizeof(array)/sizeof(array[0]))
#endif

#if (_MSC_VER <= 1200) && !defined(_WIN64)

// IA64 Macros:
#ifndef DWORD_PTR
#define DWORD_PTR DWORD
#endif

#ifndef UINT_PTR
#define UINT_PTR UINT
#endif

#ifndef INT_PTR
#define INT_PTR int
#endif

#ifndef ULONG_PTR
#define ULONG_PTR ULONG
#endif

#ifndef LONG_PTR
#define LONG_PTR long
#endif

#ifndef SetWindowLongPtr
#define SetWindowLongPtr SetWindowLong
#endif

#ifndef GetWindowLongPtr
#define GetWindowLongPtr GetWindowLong
#endif

#ifndef GetClassLongPtr
#define GetClassLongPtr GetClassLong
#endif

#endif

#endif 
