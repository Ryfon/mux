﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2006年3月
*********************************************/
#if !defined(__SPCOLORMANAGER_H__)
#define __SPCOLORMANAGER_H__

#if _MSC_VER > 1000
#pragma once
#endif 

//-----------------------------------------------------------------------
// 功能:
//     当前XP风格的类型。
//-----------------------------------------------------------------------
enum SPCurrentSystemTheme
{
	SPSystemThemeUnknown, 
	SPSystemThemeBlue,    
	SPSystemThemeOlive,   
	SPSystemThemeSilver,  
	SPSystemThemeAuto,    
	SPSystemThemeDefault  
};


#ifndef COLOR_GRADIENTACTIVECAPTION
#define COLOR_GRADIENTACTIVECAPTION           27
#endif

#ifndef COLOR_GRADIENTINACTIVECAPTION
#define COLOR_GRADIENTINACTIVECAPTION         28
#endif

const UINT XPCOLOR_BASE                     = 30; 
const UINT XPCOLOR_TOOLBAR_FACE             = XPCOLOR_BASE;
const UINT XPCOLOR_HIGHLIGHT                = 31; 
const UINT XPCOLOR_HIGHLIGHT_BORDER         = 32; 
const UINT XPCOLOR_HIGHLIGHT_PUSHED         = 33; 
const UINT XPCOLOR_HIGHLIGHT_CHECKED        = 36; 
const UINT XPCOLOR_HIGHLIGHT_CHECKED_BORDER = 37; 
const UINT XPCOLOR_ICONSHADDOW              = 34; 
const UINT XPCOLOR_GRAYTEXT                 = 35; 
const UINT XPCOLOR_TOOLBAR_GRIPPER          = 38; 
const UINT XPCOLOR_SEPARATOR                = 39; 
const UINT XPCOLOR_DISABLED                 = 40; 
const UINT XPCOLOR_MENUBAR_FACE             = 41; 
const UINT XPCOLOR_MENUBAR_EXPANDED         = 42; 
const UINT XPCOLOR_MENUBAR_BORDER           = 43; 
const UINT XPCOLOR_MENUBAR_TEXT             = 44; 
const UINT XPCOLOR_HIGHLIGHT_TEXT           = 45; 
const UINT XPCOLOR_TOOLBAR_TEXT             = 46; 
const UINT XPCOLOR_PUSHED_TEXT              = 47; 
const UINT XPCOLOR_TAB_INACTIVE_BACK        = 48; 
const UINT XPCOLOR_TAB_INACTIVE_TEXT        = 49; 
const UINT XPCOLOR_HIGHLIGHT_PUSHED_BORDER  = 50; 
const UINT XPCOLOR_CHECKED_TEXT             = XPCOLOR_HIGHLIGHT_TEXT; 
const UINT XPCOLOR_3DFACE                   = 51; 
const UINT XPCOLOR_3DSHADOW                 = 52; 
const UINT XPCOLOR_EDITCTRLBORDER           = 53; 
const UINT XPCOLOR_FRAME                    = 54; 
const UINT XPCOLOR_SPLITTER_FACE            = 55; 
const UINT XPCOLOR_LABEL                    = 56; 
const UINT XPCOLOR_STATICFRAME              = 57; 
const UINT XPCOLOR_LAST                     = 57; 


//===========================================================================
// 功能:
//     绘制颜色封装。
//===========================================================================
class  CSPPaintManagerColor
{
public:

	CSPPaintManagerColor()
	{
		m_clrStandardValue = m_clrCustomValue = (COLORREF)-1;
	}

	BOOL IsDefaultValue() {
		return (m_clrCustomValue == (COLORREF)-1) && (m_clrStandardValue == (COLORREF)-1);
	}

	COLORREF GetStandardColor() {
		return m_clrStandardValue;
	}

	void SetCustomValue(COLORREF clr) {
		m_clrCustomValue = clr;
	}

	void SetStandardValue(COLORREF clr) {
		m_clrStandardValue = clr;
	}

	void SetDefaultValue() {
		m_clrCustomValue = (COLORREF)-1;
	}

	operator COLORREF() {
		return  (m_clrCustomValue == (COLORREF)-1)? m_clrStandardValue: m_clrCustomValue;
	}

	const CSPPaintManagerColor& operator= (COLORREF clr) {
		SetCustomValue(clr);
		return *this;
	}

protected:
	COLORREF m_clrStandardValue;   
	COLORREF m_clrCustomValue;   

};


//===========================================================================
// 功能:
//    辅助类，用来绘制渐变色。
//===========================================================================
class  CSPPaintManagerColorGradient
{
public:

	void SetStandardValue(COLORREF clrLight, COLORREF clrDark);

	void SetCustomValue(COLORREF clrLight, COLORREF clrDark);

	void SetStandardValue(const COLORREF& clr);

public:
	CSPPaintManagerColor clrLight;     // 亮色
	CSPPaintManagerColor clrDark;      // 暗色
};

//===========================================================================
// 功能:
//    用来管理系统颜色。
//===========================================================================
class  CSPColorManager : public CCmdTarget
{
public:
	typedef DWORD (__stdcall *PFNGETSYSCOLOR)(int nIndex);

public:

	//-----------------------------------------------------------------------
	// 功能:
	//     用来更新颜色值。
	//-----------------------------------------------------------------------
	void RefreshColors(BOOL bInit = FALSE);

	//-----------------------------------------------------------------------
	// 功能:
	//    获取窗口指定部分的颜色
	// 参数:
	//     nIndex - 指定要获取颜色的部分
	// 
	//  <b>常量</b>						 <b>数值</b>	<b>描述</b>
	//  --------------------------------  ============  ------------------------------------------------------------
	//  COLOR_SCROLLBAR                   0             滚动条颜色
	//  COLOR_BACKGROUND                  1             桌面颜色
	//  COLOR_ACTIVECAPTION               2             活动标题栏颜色
	//  COLOR_INACTIVECAPTION             3             非活动标题栏颜色
	//  COLOR_MENU                        4             菜单背景色
	//  COLOR_WINDOW                      5             窗口背景色
	//  COLOR_WINDOWFRAME                 6             窗口框架色
	//  COLOR_MENUTEXT                    7             菜单文本颜色
	//  COLOR_WINDOWTEXT                  8             窗口文本颜色
	//  COLOR_CAPTIONTEXT                 9             标题栏文本颜色
	//  COLOR_ACTIVEBORDER                10            活动窗口边框颜色
	//  COLOR_INACTIVEBORDER              11            非活动窗口边框颜色
	//  COLOR_APPWORKSPACE                12            背景色MDI程序
	//  COLOR_HIGHLIGHT                   13            被选项颜色
	//  COLOR_HIGHLIGHTTEXT               14            被选项文本颜色
	//  COLOR_BTNFACE                     15            三维显示色活对话框背景色
	//  COLOR_BTNSHADOW                   16            命令按钮边阴影颜色
	//  COLOR_GRAYTEXT                    17            灰色文本色
	//  COLOR_BTNTEXT                     18            按钮文本色
	//  COLOR_INACTIVECAPTIONTEXT         19            非活动标题栏颜色
	//  COLOR_BTNHIGHLIGHT                20            三维显示高亮颜色
	//  COLOR_3DDKSHADOW                  21            三维显示暗阴影色
	//  COLOR_3DLIGHT                     22            三位亮色
	//  COLOR_INFOTEXT                    23            提示栏文本色
	//  COLOR_INFOBK                      24            提示栏背景色
	//  COLOR_HOTLIGHT                    26            热点颜色
	//  COLOR_GRADIENTACTIVECAPTION       27            窗口标题栏右边渐变颜色
	//  COLOR_GRADIENTINACTIVECAPTION     28            非窗口标题栏右边渐变颜色
	//  XPCOLOR_TOOLBAR_FACE              30            XP工具栏背景色
	//  XPCOLOR_HIGHLIGHT                 31            XP被选菜单项颜色
	//  XPCOLOR_HIGHLIGHT_BORDER          32            XP被选菜单项边框色
	//  XPCOLOR_HIGHLIGHT_PUSHED          33            XP菜单项弹出色
	//  XPCOLOR_HIGHLIGHT_CHECKED         36            XP菜单项被选色
	//  XPCOLOR_HIGHLIGHT_CHECKED_BORDER  37            被选菜单项边框色
	//  XPCOLOR_ICONSHADDOW               34            XP菜单项图标阴影色
	//  XPCOLOR_GRAYTEXT                  35            XP禁止菜单项文本色
	//  XPCOLOR_TOOLBAR_GRIPPER           38            XP工具栏gripper色
	//  XPCOLOR_SEPARATOR                 39            XP工具栏分隔符颜色
	//  XPCOLOR_DISABLED                  40            XP菜单图标禁止颜色
	//  XPCOLOR_MENUBAR_FACE              41            XP菜单项文本背景颜色
	//  XPCOLOR_MENUBAR_EXPANDED          42            XP隐藏菜单背景色
	//  XPCOLOR_MENUBAR_BORDER            43            XP菜单边框色
	//  XPCOLOR_MENUBAR_TEXT              44            XP菜单项文本色
	//  XPCOLOR_HIGHLIGHT_TEXT            45            XP选择菜单项文本色
	//  XPCOLOR_TOOLBAR_TEXT              46            XP工具栏文本色
	//  XPCOLOR_PUSHED_TEXT               47            XP工具栏选择文本色
	//  XPCOLOR_TAB_INACTIVE_BACK         48            XP 非活动tab背景色
	//  XPCOLOR_TAB_INACTIVE_TEXT         49            XP 非活动tab文本颜色
	//  XPCOLOR_HIGHLIGHT_PUSHED_BORDER   50            三维项边框颜色
	//  XPCOLOR_CHECKED_TEXT              45            被选按钮文本颜色
	//  XPCOLOR_3DFACE                    51            三维外观颜色
	//  XPCOLOR_3DSHADOW                  52            三维外观阴影颜色
	//  XPCOLOR_EDITCTRLBORDER            53            EDIT控件边框颜色
	//  XPCOLOR_FRAME                     54            Office 2003 框架颜色
	//  XPCOLOR_SPLITTER_FACE             55            XP splitter颜色
	//  XPCOLOR_LABEL                     56            标签颜色
	//  XPCOLOR_STATICFRAME               57            WinXP静态控件颜色
	//</TABLE>
	//-----------------------------------------------------------------------
	COLORREF GetColor(int nIndex);

	//-----------------------------------------------------------------------
	// 功能:
	//     渲染亮色和暗色。
	//-----------------------------------------------------------------------
	COLORREF LightColor(COLORREF clrLight, COLORREF clrDark, int nDelta);

	//-----------------------------------------------------------------------
	// 功能:
	//     禁用luna (Blue, Olive and Green)颜色
	//-----------------------------------------------------------------------
	void DisableLunaColors(BOOL bDisable = TRUE);

	//-----------------------------------------------------------------------
	// 功能:
	//    是否禁用luna (Blue, Olive and Green)颜色
	//-----------------------------------------------------------------------
	BOOL IsLunaColorsDisabled();

	//-----------------------------------------------------------------------
	// 功能:
	//     自定义指定的颜色。
	//-----------------------------------------------------------------------
	void SetColor(int nIndex, COLORREF clrValue);

	//-----------------------------------------------------------------------
	// 功能:
	//     自定义颜色。
	//-----------------------------------------------------------------------
	void SetColors(int cElements, const int* lpaElements, const COLORREF* lpaRgbValues);

	//-----------------------------------------------------------------------
	// 功能:
	//     判断当前使用的风格。
	//-----------------------------------------------------------------------
	SPCurrentSystemTheme GetCurrentSystemTheme();

	//-----------------------------------------------------------------------
	// 功能:
	//     获取当前在使用的Windows XP风格。
	//-----------------------------------------------------------------------
	SPCurrentSystemTheme GetWinThemeWrapperTheme();

	//-----------------------------------------------------------------------
	// 功能:
	//     设置当前风格。
	//-----------------------------------------------------------------------
	void SetLunaTheme(SPCurrentSystemTheme systemTheme);

	//-----------------------------------------------------------------------
	// 功能:
	//     替换系统定义的颜色获取函数。
	//-----------------------------------------------------------------------
	void SetGetSysColorPtr(PFNGETSYSCOLOR pfnGetSysColor);

private:
	CSPColorManager();
	void RefreshSysColors();
	void RefreshXPColors();
	void RefreshGradientColors();
	float ColorWidth(int nLength, int nWidth);
	float ColorDelta(COLORREF clrA, COLORREF clrB);
	float Length(COLORREF clrA, COLORREF clrB);
	BOOL LongColor(COLORREF clrMain, COLORREF clrSub, BOOL bCalcLength, float fDistance);
	COLORREF MixColor(COLORREF clrMain, COLORREF clrSub, float fDistance);
	COLORREF AdjustColor(COLORREF clrMain, COLORREF clrSub, float fDistance);
	double GetRDelta(COLORREF clr);
	double GetGDelta(COLORREF clr);
	double GetBDelta(COLORREF clr);

	// singleton, instantiate on demand.
	static CSPColorManager& AFX_CDECL Instance();

public:
	CSPPaintManagerColorGradient grcCaption;               // Caption gradient color.
	CSPPaintManagerColorGradient grcDockBar;               // Dockbar gradient color.
	CSPPaintManagerColorGradient grcShortcutBarGripper;    // Shortcut bar gripper.
	CSPPaintManagerColorGradient grcToolBar;               // ToolBar gradient color.

private:
	BOOL m_bInit;
	BOOL m_bDisableLunaColors;
	COLORREF m_arrStandardColor[XPCOLOR_LAST + 1];
	COLORREF m_arrCustomColor[XPCOLOR_LAST + 1];
	PFNGETSYSCOLOR m_pfnGetSysColor;
	SPCurrentSystemTheme m_systemTheme;


	friend CSPColorManager* SPColorManager();
};



CSPColorManager* SPColorManager();

//---------------------------------------------------------------------------
// 功能:
//     获取指定的颜色。
//---------------------------------------------------------------------------
COLORREF GetXtremeColor(UINT nIndex);

/////////////////////////////////////////////////////////////////////////////

AFX_INLINE COLORREF GetXtremeColor(UINT nIndex) {
	return SPColorManager()->GetColor(nIndex);
}
AFX_INLINE CSPColorManager* SPColorManager() {
	return &CSPColorManager::Instance();
}

#endif 
