﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/

#include "StdAfx.h"
#include "SPVC80Helpers.h"

#include "SPPropertyGridInplaceEdit.h"
#include "SPPropertyGridInplaceButton.h"
#include "SPPropertyGridInplaceList.h"
#include "SPPropertyGridItem.h"
#include "SPPropertyGridItemNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemNumber
IMPLEMENT_DYNAMIC(CSPPropertyGridItemNumber, CSPPropertyGridItem)


CSPPropertyGridItemNumber::CSPPropertyGridItemNumber(CString strCaption, long nValue, long* pBindNumber)
	: CSPPropertyGridItem(strCaption)
{
	m_pBindNumber = pBindNumber;
	SetNumber(nValue);
}
CSPPropertyGridItemNumber::CSPPropertyGridItemNumber(UINT nID, long nValue, long* pBindNumber)
	: CSPPropertyGridItem(nID)
{
	m_pBindNumber = pBindNumber;
	SetNumber(nValue);
}

CSPPropertyGridItemNumber::~CSPPropertyGridItemNumber()
{
}

/////////////////////////////////////////////////////////////////////////////
//

void CSPPropertyGridItemNumber::SetValue(CString strValue)
{
	SetNumber(_ttol(strValue));
}

void CSPPropertyGridItemNumber::SetNumber(long nValue)
{
	m_nValue = nValue;

	if (m_pBindNumber)
	{
		*m_pBindNumber = m_nValue;
	}

	CString strValue;
	strValue.Format(_T("%i"), nValue);
	CSPPropertyGridItem::SetValue(strValue);
}

void CSPPropertyGridItemNumber::BindToNumber(long* pBindNumber)
{
	m_pBindNumber = pBindNumber;
	if (m_pBindNumber)
	{
		*m_pBindNumber = m_nValue;
	}
}

void CSPPropertyGridItemNumber::OnBeforeInsert()
{
	if( m_pBindNumber && *m_pBindNumber != m_nValue)
	{
		SetNumber(*m_pBindNumber);
	}
}





/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemDouble
IMPLEMENT_DYNAMIC(CSPPropertyGridItemDouble, CSPPropertyGridItem)


CSPPropertyGridItemDouble::CSPPropertyGridItemDouble(CString strCaption, double fValue, LPCTSTR strFormat, double* pBindDouble)
	: CSPPropertyGridItem(strCaption)
{
	m_pBindDouble = pBindDouble;
	m_strFormat = strFormat;
	SetDouble(fValue);
}
CSPPropertyGridItemDouble::CSPPropertyGridItemDouble(UINT nID, double fValue, LPCTSTR strFormat, double* pBindDouble)
	: CSPPropertyGridItem(nID)
{
	m_pBindDouble = pBindDouble;
	m_strFormat = strFormat;
	SetDouble(fValue);
}

CSPPropertyGridItemDouble::~CSPPropertyGridItemDouble()
{
}

/////////////////////////////////////////////////////////////////////////////
//

void CSPPropertyGridItemDouble::SetValue(CString strValue)
{
#ifdef _UNICODE
	char astring[20];
	WideCharToMultiByte (CP_ACP, 0, strValue, -1, astring, 20, NULL, NULL);
	SetDouble((double)atof(astring));
#else
	SetDouble((double)atof(strValue));
#endif
}

void CSPPropertyGridItemDouble::SetDouble(double fValue)
{
	m_fValue = fValue;

	if (m_pBindDouble)
	{
		*m_pBindDouble = m_fValue;
	}

	CString strValue;
	strValue.Format(m_strFormat, fValue);
	CSPPropertyGridItem::SetValue(strValue);
}

void CSPPropertyGridItemDouble::BindToDouble(double* pBindNumber)
{
	m_pBindDouble = pBindNumber;
	if (m_pBindDouble)
	{
		*m_pBindDouble = m_fValue;
	}
}

void CSPPropertyGridItemDouble::OnBeforeInsert()
{
	if( m_pBindDouble && *m_pBindDouble != m_fValue)
	{
		SetDouble(*m_pBindDouble);
	}
}

