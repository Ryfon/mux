﻿// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "SkinBuilder.h"

#include "MainFrm.h"
#include ".\mainfrm.h"
#include "ImageView.h"
#include "SkinBuilderView.h"
#include "PropertyView.h"
#include "ImageDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 状态行指示器
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame 构造/析构

CMainFrame::CMainFrame()
{
	// TODO: 在此添加成员初始化代码
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("未能创建工具栏\n");
		return -1;      // 未能创建
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: 在此处通过修改 CREATESTRUCT cs 来修改窗口类或
	// 样式
	cs.lpszClass = AfxRegisterWndClass( 0, NULL, NULL,
		AfxGetApp()->LoadIcon(IDR_MAINFRAME));
	return TRUE;
}

// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

void CMainFrame::OnClose()
{
	/*SaveCommandBars(_T("命令栏"));

	CXTPDockingPaneLayout layoutNormal(GetDockingPaneManager());
	GetDockingPaneManager()->GetLayout(&layoutNormal);
	layoutNormal.Save(_T("布局"));*/

	CFrameWnd::OnClose();
}
BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// TODO: Add your specialized code here and/or call the base class
	if(!m_wndSplitter.CreateStatic(this,1,3))
	{
		TRACE0("Failed to create splitter window\n");
		return FALSE;
	}
	
	if (!m_wndSplitter.CreateView(0,0, RUNTIME_CLASS(CImageView),
		CSize(0, 0), pContext))
	{
		TRACE0("Failed to create imageview\n");
		return FALSE;
	}

	m_pViewImage = (CImageView*)m_wndSplitter.GetPane(0,0);

	if (!m_wndSplitter.CreateView(0,1, RUNTIME_CLASS(CSkinBuilderView),
		CSize(0, 0), pContext))
	{
		TRACE0("Failed to create main\n");
		return FALSE;
	}

	m_pViewMain = (CSkinBuilderView*)m_wndSplitter.GetPane(0,1);

	if (!m_wndSplitter.CreateView(0,2, RUNTIME_CLASS(CPropertyView),
		CSize(0, 0), pContext))
	{
		TRACE0("Failed to create propertyview\n");
		return FALSE;
	}
	m_pViewProperty = (CPropertyView*)m_wndSplitter.GetPane(0,1);
	//m_pViewProperty->SetParent(this);

	
	CRect r;
	GetClientRect(&r);

	m_wndSplitter.SetRowInfo( 0, r.Height(), 0 );
	m_wndSplitter.SetColumnInfo( 0, r.Width()/4, 0 );
	m_wndSplitter.SetColumnInfo( 1, r.Width()/2, 0 );


	return TRUE;
}

