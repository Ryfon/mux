﻿#pragma once


// CTextEffectDlg dialog

class CTextEffectDlg : public CDialog
{
	DECLARE_DYNAMIC(CTextEffectDlg)

public:
	CTextEffectDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTextEffectDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_TEXT };

	void	SetTextEffect(SkinTextType *pTextEffect);

	SkinTextType	m_textEffect;
	SkinTextType	*m_pTextEffect;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	
	DECLARE_MESSAGE_MAP()
	virtual void OnOK();
public:
	afx_msg void OnEnChangeEditOffsetx();
};
