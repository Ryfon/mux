﻿#pragma once
#include "ImageDlg.h"
#include "TextEffectDlg.h"


class CSPPropertyGridItemImage :public CSPPropertyGridItem
{
public:
	CSPPropertyGridItemImage(CString strCaption, LPCTSTR strValue = NULL, CString* pBindString = NULL);
	CSPPropertyGridItemImage(UINT nID);
	virtual ~CSPPropertyGridItemImage(void);

	void OnInplaceButtonDown();
	CImageDlg m_dlgImage;
};

////////////////////////////////////////
class CSPPropertyGridItemText :public CSPPropertyGridItem
{
public:
	CSPPropertyGridItemText(CString strCaption, LPCTSTR strValue = NULL, CString* pBindString = NULL);
	CSPPropertyGridItemText(UINT nID);
	virtual ~CSPPropertyGridItemText(void);

	void OnInplaceButtonDown();
	CTextEffectDlg m_dlgText;
};

class CSPPropertyGridItemRect : public CSPPropertyGridItem
{
	class CCustomItemChildsAll;
	class CCustomItemChildsPad;

	friend class CCustomItemChildsAll;
	friend class CCustomItemChildsPad;

public:
	CSPPropertyGridItemRect(CString strCaption, CRect rcValue);
	CSPPropertyGridItemRect(UINT nID,  CRect rcValue);

	void		SetRect(CRect rc);
	CRect		GetRect();

protected:
	virtual void OnAddChildItem();
	virtual void SetValue(CString strValue);

private:
	void UpdateChilds();
	CString RectToString(CRect rc);


private:
	CCustomItemChildsAll* m_itemAll;
	CCustomItemChildsPad* m_itemLeft;
	CCustomItemChildsPad* m_itemTop;
	CCustomItemChildsPad* m_itemRight;
	CCustomItemChildsPad* m_itemBottom;

	CRect m_rcValue;
};
