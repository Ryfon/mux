﻿// TextEffectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SkinBuilder.h"
#include "TextEffectDlg.h"
#include ".\texteffectdlg.h"


// CTextEffectDlg dialog

IMPLEMENT_DYNAMIC(CTextEffectDlg, CDialog)
CTextEffectDlg::CTextEffectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTextEffectDlg::IDD, pParent)
{
	m_pTextEffect = NULL;
}

CTextEffectDlg::~CTextEffectDlg()
{
}

void CTextEffectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTextEffectDlg, CDialog)
	ON_EN_CHANGE(IDC_EDIT_OFFSETX, OnEnChangeEditOffsetx)
END_MESSAGE_MAP()

void CTextEffectDlg::SetTextEffect(SkinTextType *pText)
{
	m_pTextEffect = pText;
	memcpy(&m_textEffect,m_pTextEffect,sizeof(m_textEffect));
}
// CTextEffectDlg message handlers

void CTextEffectDlg::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class
	if(m_pTextEffect)
		memcpy(m_pTextEffect,&m_textEffect,sizeof(m_textEffect));
	CDialog::OnOK();
}

void CTextEffectDlg::OnEnChangeEditOffsetx()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
