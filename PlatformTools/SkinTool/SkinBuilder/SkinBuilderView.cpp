﻿// SkinBuilderView.cpp : CSkinBuilderView 类的实现
//

#include "stdafx.h"
#include "SkinBuilder.h"

#include "SkinBuilderDoc.h"
#include "SkinBuilderView.h"
#include ".\skinbuilderview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
static UINT indicators[] =
{
		ID_SEPARATOR,           // 状态行指示器
		ID_INDICATOR_CAPS,
		ID_INDICATOR_NUM,
		ID_INDICATOR_SCRL,
};

// CSkinBuilderView

IMPLEMENT_DYNCREATE(CSkinBuilderView, CView)

BEGIN_MESSAGE_MAP(CSkinBuilderView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_CREATE()
END_MESSAGE_MAP()

// CSkinBuilderView 构造/析构

CSkinBuilderView::CSkinBuilderView()
{
	// TODO: 在此处添加构造代码
	m_trackerPos.m_nStyle = CRectTracker::resizeOutside;

}

CSkinBuilderView::~CSkinBuilderView()
{
}

BOOL CSkinBuilderView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改 CREATESTRUCT cs 来修改窗口类或
	// 样式

	return CView::PreCreateWindow(cs);
}

// CSkinBuilderView 绘制

void CSkinBuilderView::OnDraw(CDC* pDC)
{
	CSkinBuilderDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if(pDoc->m_nSelectedType == OBJECT_TYPE_IMAGE)
	{
		if (!pDoc->m_pCurrentImage)
			return;

		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = GetDocument()->m_pCurrentImage->GetHeight();
		nWidth = GetDocument()->m_pCurrentImage->GetWidth();

		int nLeft,nTop,nRight,nBottom;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;

		nRight = nLeft + nWidth;
		nBottom = nTop + nHeight;
		m_check.ShowWindow(SW_HIDE);
		m_push.ShowWindow(SW_HIDE);
		m_radio.ShowWindow(SW_HIDE);
		m_group.ShowWindow(SW_HIDE);
		m_static.ShowWindow(SW_HIDE);
		m_edit.ShowWindow(SW_HIDE);
		GetDocument()->m_pCurrentImage->TransparentDraw(pDC,RGB(255,0,255),CRect(nLeft,nTop,nRight,nBottom));
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINDOW||
		pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINBUTTON)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = pDoc->m_pCurrentObject->GetHeight();
		nWidth = pDoc->m_pCurrentObject->GetWidth();

		int nLeft,nTop,nRight,nBottom;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);

		nRight = nLeft + nWidth;
		nBottom = nTop + nHeight;
		CWindowSkin *pWin = (CWindowSkin*)pDoc->m_pCurrentObject;
		pWin->DrawFrame(pDC,CRect(nLeft,nTop,nRight,nBottom),true);
		pWin->DrawButton(pDC,CRect(nLeft,nTop,nRight,nBottom),0);
		if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINBUTTON)
		{
			CRect rt = pDoc->m_pCurrentWinButton->GetPosition();
			rt.OffsetRect(nLeft,nTop);
			pDoc->m_pCurrentWinButton->DrawButton(pDC,rt,0);
			m_trackerPos.m_rect = rt;
			m_trackerPos.Draw(pDC);
		}
		m_check.ShowWindow(SW_HIDE);
		m_push.ShowWindow(SW_HIDE);
		m_radio.ShowWindow(SW_HIDE);
		m_group.ShowWindow(SW_HIDE);
		m_static.ShowWindow(SW_HIDE);
		m_edit.ShowWindow(SW_HIDE);
		//		GetSkinDrawHelper()->DrawButton(pDC,CRect(12,12,33,33),pWin->winButtonSkin[0],keWinButtonHover);

	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SCROLLBAR)
	{
	//	if(pDoc->m_pCurrentObject == NULL)
	//		return;
	//	CRect rtClient;
	//	GetClientRect(&rtClient);

	//	int nWidth;
	//	int nHeight;

	//	CPoint pt;
	//	nHeight = 17;
	//	nWidth = 150;

	//	int nLeft,nTop;
	//	nLeft = (rtClient.Width() - nWidth)/2;
	//	nTop = (rtClient.Height()- nHeight)/2;
	//	m_ptTopLeft = CPoint(nLeft,nTop);

	//	CScrollBarSkin *pScrollBar = (CScrollBarSkin*)pDoc->m_pCurrentObject;

	////	pScrollBar->DrawHScroll(pDC,CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),0);
	////	pScrollBar->DrawHThumb(pDC,CRect(nLeft+ nHeight,nTop,nLeft + 2*nHeight,nTop + nHeight),0);

	//	nLeft +=  160;
	//	nTop -= 67;
	//	nHeight = 150;
	//	nWidth = 17;
	//	//m_ptTopLeft += CPoint(155,-67);

	//	pScrollBar->DrawVScroll(pDC,CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),0);
	//	pScrollBar->DrawVThumb(pDC,CRect(nLeft,nTop + nWidth,nLeft  + nWidth,nTop + 2*nWidth ),0);
	//	m_check.ShowWindow(SW_HIDE);
	//	m_push.ShowWindow(SW_HIDE);
	//	m_radio.ShowWindow(SW_HIDE);
	//	m_group.ShowWindow(SW_HIDE);
	//	m_static.ShowWindow(SW_HIDE);
	//	m_edit.ShowWindow(SW_HIDE);
	}
	

	// TODO: 在此处为本机数据添加绘制代码
}

#ifdef _DEBUG
void CSkinBuilderView::AssertValid() const
{
	CView::AssertValid();
}

void CSkinBuilderView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSkinBuilderDoc* CSkinBuilderView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSkinBuilderDoc)));
	return (CSkinBuilderDoc*)m_pDocument;
}
#endif //_DEBUG


// CSkinBuilderView 消息处理程序

void CSkinBuilderView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{

	// TODO: Add your specialized code here and/or call the base class
	m_check.ShowWindow(SW_HIDE);
	m_push.ShowWindow(SW_HIDE);
	m_radio.ShowWindow(SW_HIDE);
	m_group.ShowWindow(SW_HIDE);
	m_static.ShowWindow(SW_HIDE);
	m_edit.ShowWindow(SW_HIDE);
	m_scrollbar.ShowWindow(SW_HIDE);
	m_combox.ShowWindow(SW_HIDE);
	m_listbox.ShowWindow(SW_HIDE);
	m_spin.ShowWindow(SW_HIDE);
	m_header.ShowWindow(SW_HIDE);
	m_slider.ShowWindow(SW_HIDE);
	m_tab.ShowWindow(SW_HIDE);
	m_statusbar.ShowWindow(SW_HIDE);
	m_toolbar.ShowWindow(SW_HIDE);
	CSkinBuilderDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	
	if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_BUTTON)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 23;
		nWidth = 80;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);

		m_skinPush.LoadSkin();
		m_push.ShowWindow(SW_SHOW);
		m_push.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);

		/*	CButtonSkin *pButton = (CButtonSkin*)pDoc->m_pCurrentObject;
		pButton->DrawButton(pDC,CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),0);*/
	}

	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_CHECK)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 23;
		nWidth = 80;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinCheck.LoadSkin();
		m_check.ShowWindow(SW_SHOW);
		m_check.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_RADIO)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 23;
		nWidth = 80;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinRadio.LoadSkin();
		m_radio.ShowWindow(SW_SHOW);
		m_radio.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_GROUP)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 200;
		nWidth = 150;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinGroup.LoadSkin();
		m_group.ShowWindow(SW_SHOW);
		m_group.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_STATIC)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 30;
		nWidth = 80;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinStatic.LoadSkin();
		m_static.ShowWindow(SW_SHOW);
		m_static.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_EDIT)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 100;
		nWidth = 100;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinEdit.LoadSkin();
		m_edit.ShowWindow(SW_SHOW);
		m_edit.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SCROLLBAR)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 17;
		nWidth = 150;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2;
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinScrollBar.LoadSkin();
		m_scrollbar.ShowWindow(SW_SHOW);
		m_scrollbar.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_COMBOX)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 100;
		nWidth = 100;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2; 
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinCombox.LoadSkin();
		m_combox.ShowWindow(SW_SHOW);
		m_combox.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_LISTBOX)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 300;
		nWidth = 120;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2; 
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinListBox.LoadSkin();
		m_listbox.ShowWindow(SW_SHOW);
		m_listbox.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SPIN)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 40;
		nWidth = 20;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2; 
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinSpin.LoadSkin();
		m_spin.ShowWindow(SW_SHOW);
		m_spin.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_HEADER)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 15;
		nWidth = 200;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2; 
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinHeader.LoadSkin();
		m_header.ShowWindow(SW_SHOW);
		m_header.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_SLIDER)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 20;
		nWidth = 200;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2; 
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinSlider.LoadSkin();
		m_slider.ShowWindow(SW_SHOW);
		m_slider.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_TAB)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 200;
		nWidth = 300;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2; 
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinTab.LoadSkin();
		m_tab.ShowWindow(SW_SHOW);
		m_tab.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_STATUSBAR)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 20;
		nWidth = 300;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2; 
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinStatusbar.LoadSkin();
		m_statusbar.ShowWindow(SW_SHOW);
		m_statusbar.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	else if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_TOOLBAR)
	{
		if(pDoc->m_pCurrentObject == NULL)
			return;
		CRect rtClient;
		GetClientRect(&rtClient);

		int nWidth;
		int nHeight;

		CPoint pt;
		nHeight = 30;
		nWidth = 300;

		int nLeft,nTop;
		nLeft = (rtClient.Width() - nWidth)/2; 
		nTop = (rtClient.Height()- nHeight)/2;
		m_ptTopLeft = CPoint(nLeft,nTop);
		m_skinToolbar.LoadSkin();
		m_toolbar.ShowWindow(SW_SHOW);
		m_toolbar.MoveWindow(CRect(nLeft,nTop,nLeft + nWidth,nTop + nHeight),TRUE);
	}
	Invalidate();
	UpdateWindow();
}

void CSkinBuilderView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CSkinBuilderDoc* pDoc = GetDocument();
	if(m_trackerPos.Track(this,point,FALSE,NULL))
	{	
		
		if(pDoc->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINBUTTON)
		{
			m_rtPos = m_trackerPos.m_rect ;
			m_rtPos -= m_ptTopLeft;
			pDoc->m_pCurrentWinButton->SetPosition(m_rtPos);
		}
		
		InvalidateRect(NULL);
		pDoc->UpdateAllViews(this);
	}

	CView::OnLButtonDown(nFlags, point);
}

int CSkinBuilderView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	m_check.Create(_T("Check1"),WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX,CRect(0,0,12,12),this,1);
	m_check.ShowWindow(SW_HIDE);
	m_skinCheck.InstallSkin(m_check.GetSafeHwnd());

	m_push.Create(_T("Button"),WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON,CRect(0,0,12,12),this,2);
	m_push.ShowWindow(SW_HIDE);
	m_skinPush.InstallSkin(m_push.GetSafeHwnd());

	m_radio.Create(_T("Radio"),WS_CHILD|WS_VISIBLE|BS_RADIOBUTTON,CRect(0,0,12,12),this,3);
	m_radio.ShowWindow(SW_HIDE);
	m_skinRadio.InstallSkin(m_radio.GetSafeHwnd());

	m_group.Create(_T("Group"),WS_CHILD|WS_VISIBLE|BS_GROUPBOX,CRect(0,0,12,12),this,4);
	m_group.ShowWindow(SW_HIDE);
	m_skinGroup.InstallSkin(m_group.GetSafeHwnd());

	m_static.Create(_T("Static"),WS_CHILD|WS_VISIBLE|SS_CENTER,CRect(0,0,12,12),this,5);
	m_static.ShowWindow(SW_HIDE);
	m_skinStatic.InstallSkin(m_static.GetSafeHwnd());

	m_edit.Create(WS_CHILD|WS_VISIBLE|WS_HSCROLL|ES_AUTOHSCROLL,CRect(0,0,12,12),this,6);
	m_edit.ShowWindow(SW_HIDE);
	m_skinEdit.InstallSkin(m_edit.GetSafeHwnd());

	m_scrollbar.Create(WS_CHILD|WS_VISIBLE|SBS_HORZ,CRect(0,0,12,12),this,7);
	m_scrollbar.ShowWindow(SW_HIDE);
	m_skinScrollBar.InstallSkin(m_scrollbar.GetSafeHwnd());

	m_combox.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWN,CRect(0,0,30,100),this,8);
	m_combox.ShowWindow(SW_HIDE);
	m_skinCombox.InstallSkin(m_combox.GetSafeHwnd());

	m_listbox.Create(WS_CHILD|WS_VISIBLE|LBS_STANDARD,CRect(0,0,30,100),this,9);
	m_listbox.ShowWindow(SW_HIDE);
	m_skinListBox.InstallSkin(m_listbox.GetSafeHwnd());

	m_spin.Create(WS_CHILD|WS_VISIBLE|PBS_VERTICAL,CRect(0,0,12,12),this,10);
	m_spin.ShowWindow(SW_HIDE);
	m_skinSpin.InstallSkin(m_spin.GetSafeHwnd());

	m_header.Create(WS_CHILD|WS_VISIBLE|HDS_BUTTONS,CRect(0,0,12,12),this,11);
	m_header.ShowWindow(SW_HIDE);

	m_slider.Create(WS_CHILD|WS_VISIBLE|TBS_HORZ,CRect(0,0,12,12),this,12);
	m_slider.ShowWindow(SW_HIDE);
	m_skinSlider.InstallSkin(m_slider.GetSafeHwnd());

	m_tab.Create(WS_CHILD|WS_VISIBLE|TCS_SINGLELINE,CRect(0,0,12,12),this,13);
	m_tab.ShowWindow(SW_HIDE);
	m_skinTab.InstallSkin(m_tab.GetSafeHwnd());

	m_statusbar.Create(this,WS_CHILD|WS_VISIBLE|CBRS_ALIGN_ANY);
	m_statusbar.ShowWindow(SW_HIDE);
	m_statusbar.SetIndicators(indicators,sizeof(indicators)/sizeof(UINT));
	m_skinStatusbar.InstallSkin(m_statusbar.GetSafeHwnd());

	m_toolbar.Create(this,WS_CHILD|WS_VISIBLE|CBRS_ALIGN_ANY);
	m_toolbar.ShowWindow(SW_HIDE);
	m_skinToolbar.InstallSkin(m_toolbar.GetSafeHwnd());
	m_toolbar.LoadToolBar(IDR_MAINFRAME);

	m_tab.InsertItem(0,_T("Item0"));
	m_tab.InsertItem(1,_T("Item1"));
	m_tab.InsertItem(2,_T("Item2"));

	HDITEM hItem;
	hItem.mask = HDI_WIDTH|HDI_TEXT|HDI_FORMAT;

	hItem.fmt =HDF_STRING;
	hItem.cxy = 30;
	hItem.pszText = _T("Item1");
	hItem.cchTextMax = 255;
	m_header.InsertItem(0,&hItem);
	m_header.InsertItem(1,&hItem);
	m_header.InsertItem(2,&hItem);
	m_skinHeader.InstallSkin(m_header.GetSafeHwnd());

	SCROLLINFO info;
	::ZeroMemory((void*)&info,sizeof(info));
	info.cbSize = sizeof(SCROLLINFO);
	info.fMask = SIF_ALL;
	info.nMax = 100;
	info.nMin = 0;
	info.nPage = 20;
	info.nPos = 10;
	m_scrollbar.SetScrollInfo(&info,FALSE);
	return 0;
}
