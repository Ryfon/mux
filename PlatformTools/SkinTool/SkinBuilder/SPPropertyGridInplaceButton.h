﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDINPLACEBUTTON_H__)
#define __SPPROPERTYGRIDINPLACEBUTTON_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// class forwards

class CSPPropertyGridView;
class CSPPropertyGridItem;

//===========================================================================
// Summary:
//     CSPPropertyGridInplaceButton is a CStatic derived class.
//     It is an internal class used by Property Grid control.
//===========================================================================
class  CSPPropertyGridInplaceButton : public CStatic
{
	DECLARE_DYNAMIC(CSPPropertyGridInplaceButton)

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridInplaceButton object
	//-----------------------------------------------------------------------
	CSPPropertyGridInplaceButton();

	//-----------------------------------------------------------------------
	// Summary:
	//     Destroys a CSPPropertyGridInplaceButton object, handles cleanup
	//     and deallocation.
	//-----------------------------------------------------------------------
	virtual ~CSPPropertyGridInplaceButton();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to hide the in-place button control.
	//-----------------------------------------------------------------------
	virtual void HideWindow();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to create the in-place button control.
	// Parameters:
	//     pItem - Points to a CSPPropertyGridItem object
	//     rect  -  Specifies the size and position of the item.
	//-----------------------------------------------------------------------
	virtual void Create(CSPPropertyGridItem* pItem, CRect rect);

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called when the parent item is destroyed.
	//-----------------------------------------------------------------------
	virtual void DestroyItem();

	//-----------------------------------------------------------------------
	// Summary:
	//     This member function retrieves the child property grid item
	// Returns:
	//     A pointer to a CSPPropertyGridItem object
	//-----------------------------------------------------------------------
	CSPPropertyGridItem* GetItem();


public:
	BOOL                 m_bPressed;    // TRUE if the button is pressed.
	BOOL                 m_bOver;       // TRUE if the mouse cursor is over the button.

private:

	CWnd*                m_pGrid;
	CSPPropertyGridItem* m_pItem;

protected:

//{{AFX_CODEJOCK_PRIVATE

	//{{AFX_MSG(CSPPropertyGridInplaceButton)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg UINT OnGetDlgCode();
	afx_msg void OnMouseLeave();
	//}}AFX_MSG

private:
	BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	BOOL Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID = 0xffff);
	DECLARE_MESSAGE_MAP()

	friend class CSPPropertyGridItem;
//}}AFX_CODEJOCK_PRIVATE
};


AFX_INLINE CSPPropertyGridItem* CSPPropertyGridInplaceButton::GetItem() {
	return m_pItem;
}
AFX_INLINE BOOL CSPPropertyGridInplaceButton::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) {
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}
AFX_INLINE BOOL CSPPropertyGridInplaceButton::Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID) {
	return CStatic::Create(lpszText, dwStyle, rect, pParentWnd, nID);
}

#endif //#if !defined(__SPPROPERTYGRIDINPLACEBUTTON_H__)
