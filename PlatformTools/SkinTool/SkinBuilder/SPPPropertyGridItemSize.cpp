﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/

#include "StdAfx.h"
#include "SPVC80Helpers.h"

#include "SPPropertyGridInplaceEdit.h"
#include "SPPropertyGridInplaceButton.h"
#include "SPPropertyGridInplaceList.h"
#include "SPPropertyGridItem.h"
#include "SPPropertyGridItemSize.h"
#include "SPPropertyGridItemNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemSize::CSPPropertyGridItemSizeWidth

class CSPPropertyGridItemSize::CSPPropertyGridItemSizeWidth : public CSPPropertyGridItemNumber
{
public:
	CSPPropertyGridItemSizeWidth(CString strCaption)
		: CSPPropertyGridItemNumber(strCaption) {}

	virtual void OnValueChanged(CString strValue)
	{
		((CSPPropertyGridItemSize*)m_pParent)->SetWidth(strValue);
	}
};

/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemSize::CSPPropertyGridItemSizeHeight

class CSPPropertyGridItemSize::CSPPropertyGridItemSizeHeight : public CSPPropertyGridItemNumber
{
public:
	CSPPropertyGridItemSizeHeight(CString strCaption)
		: CSPPropertyGridItemNumber(strCaption) {}

	virtual void OnValueChanged(CString strValue)
	{
		((CSPPropertyGridItemSize*)m_pParent)->SetHeight(strValue);
	}
};

/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemSize
IMPLEMENT_DYNAMIC(CSPPropertyGridItemSize, CSPPropertyGridItem)

CSPPropertyGridItemSize::CSPPropertyGridItemSize(CString strCaption, CSize size, CSize* pBindSize)
	: CSPPropertyGridItem(strCaption)
{
	m_szValue = size;
	BindToSize(pBindSize);
	m_strValue = SizeToString(size);
}

CSPPropertyGridItemSize::CSPPropertyGridItemSize(UINT nID,  CSize size, CSize* pBindSize)
	: CSPPropertyGridItem(nID)
{
	m_szValue = size;
	BindToSize(pBindSize);
	m_strValue = SizeToString(size);

}

CSPPropertyGridItemSize::~CSPPropertyGridItemSize()
{

}

/////////////////////////////////////////////////////////////////////////////
//

void CSPPropertyGridItemSize::OnAddChildItem()
{
	m_itemWidth = (CSPPropertyGridItemSizeWidth*)AddChildItem(new CSPPropertyGridItemSizeWidth(_T("Width")));
	m_itemHeight = (CSPPropertyGridItemSizeHeight*)AddChildItem(new CSPPropertyGridItemSizeHeight(_T("Height")));
	UpdateChilds();
}

CString CSPPropertyGridItemSize::SizeToString(CSize size)
{
	CString str;
	str.Format(_T("%i; %i"), size.cx, size.cy);
	return str;
}

CSize CSPPropertyGridItemSize::StringToSize(CString str)
{
	CString strWidth, strHeight;

	AfxExtractSubString(strWidth, str, 0, ';');
	AfxExtractSubString(strHeight, str, 1, ';');

	return CSize(_ttoi(strWidth), _ttoi(strHeight));
}

void CSPPropertyGridItemSize::SetValue(CString strValue)
{
	SetSize(StringToSize(strValue));
}

void CSPPropertyGridItemSize::SetSize(CSize size)
{
	m_szValue = size;

	if (m_pBindSize)
	{
		*m_pBindSize = m_szValue;
	}

	CSPPropertyGridItem::SetValue(SizeToString(m_szValue));
	UpdateChilds();
}

void CSPPropertyGridItemSize::BindToSize(CSize* pBindSize)
{
	m_pBindSize = pBindSize;
	if (m_pBindSize)
	{
		*m_pBindSize = m_szValue;
	}
}

void CSPPropertyGridItemSize::OnBeforeInsert()
{
	if( m_pBindSize && *m_pBindSize != m_szValue)
	{
		SetSize(*m_pBindSize);
	}
}

void CSPPropertyGridItemSize::UpdateChilds()
{
	m_itemWidth->SetNumber(m_szValue.cx);
	m_itemHeight->SetNumber(m_szValue.cy);
}

void CSPPropertyGridItemSize::SetWidth(CString strWidth)
{
	OnValueChanged(SizeToString(CSize(_ttoi(strWidth), m_szValue.cy)));

}

void CSPPropertyGridItemSize::SetHeight(CString strHeight)
{
	OnValueChanged(SizeToString(CSize(m_szValue.cx, _ttoi(strHeight))));
}
