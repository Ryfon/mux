﻿// MainFrm.h : CMainFrame 类的接口
//


#pragma once

class CSkinBuilderView;
class CImageView;
class CPropertyView;
class CMainFrame : public CFrameWnd
{
	
protected: // 仅从序列化创建
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

//
public:
	CTreeCtrl m_wndObjectView;
//
public:
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

//
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:  // 控件条嵌入成员
	CToolBar		m_wndToolBar;	
	CStatusBar		m_wndStatusBar;
	CSplitterWnd		m_wndSplitter;
	CSkinBuilderView	*m_pViewMain;
	CImageView			*m_pViewImage;
	CPropertyView		*m_pViewProperty;
	
// 生成的消息映射函数
protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	DECLARE_MESSAGE_MAP()
};


