﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#include "stdafx.h"
#include "SPVC80Helpers.h"
#include "SPDrawHelpers.h"

#include "SPPropertyGridInplaceEdit.h"
#include "SPPropertyGridInplaceButton.h"
#include "SPPropertyGridInplaceList.h"
#include "SPPropertyGridItem.h"
#include "SPPropertyGridItemFont.h"
#include "SPPropertyGrid.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemFont
IMPLEMENT_DYNAMIC(CSPPropertyGridItemFont, CSPPropertyGridItem)


CSPPropertyGridItemFont::CSPPropertyGridItemFont(CString strCaption, LOGFONT& font)
	: CSPPropertyGridItem(strCaption)
{
	SetFont(font);
	m_nFlags = SPGridItemHasExpandButton;
	m_clrValue = (COLORREF)-1;
	EnableAutomation();
}

CSPPropertyGridItemFont::CSPPropertyGridItemFont(UINT nID, LOGFONT& font)
	: CSPPropertyGridItem(nID)
{
	SetFont(font);
	m_nFlags = SPGridItemHasExpandButton;
	m_clrValue = (COLORREF)-1;
	EnableAutomation();
}

CSPPropertyGridItemFont::~CSPPropertyGridItemFont()
{
}

/////////////////////////////////////////////////////////////////////////////
//

void CSPPropertyGridItemFont::SetFont(LOGFONT& font)
{
	MEMCPY_S(&m_lfValue, &font,  sizeof(LOGFONT));

	CWindowDC dc(CWnd::GetDesktopWindow());
	int nHeight = -MulDiv(m_lfValue.lfHeight, 72, dc.GetDeviceCaps(LOGPIXELSY));

	m_strValue.Format(_T("%s; %ipt"), m_lfValue.lfFaceName, nHeight);
}

BOOL CSPPropertyGridItemFont::OnDrawItemValue(CDC& dc, CRect rcValue)
{
	if (m_clrValue == (COLORREF)-1)
		return CSPPropertyGridItem::OnDrawItemValue(dc, rcValue);

	COLORREF clr = dc.GetTextColor();
	CRect rcSample(rcValue.left - 2, rcValue.top + 1, rcValue.left + 18, rcValue.bottom - 1);
	CSPPenDC pen(dc, clr);
	CSPBrushDC brush(dc, m_clrValue);
	dc.Rectangle(rcSample);

	CRect rcText(rcValue);
	rcText.left += 25;

	dc.DrawText( m_strValue, rcText,  DT_SINGLELINE|DT_VCENTER);

	return TRUE;
}

CRect CSPPropertyGridItemFont::GetValueRect()
{
	CRect rcValue(CSPPropertyGridItem::GetValueRect());
	if (m_clrValue != (COLORREF)-1) rcValue.left += 25;
	return rcValue;
}


UINT CALLBACK CSPPropertyGridItemFont::FontDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_INITDIALOG)
	{
		HWND hWndCombo = GetDlgItem(hWnd, 1139);
		if (hWndCombo)
			EnableWindow(hWndCombo, FALSE);

		return (UINT)AfxDlgProc(hWnd, message, wParam, lParam);
	}

	return 0;

}

void CSPPropertyGridItemFont::OnInplaceButtonDown()
{
	CFontDialog dlg(&m_lfValue, CF_EFFECTS | CF_SCREENFONTS, NULL, m_pGrid);

	if (m_clrValue == (COLORREF)-1)
	{
		dlg.m_cf.lpfnHook = FontDlgProc;
	}
	else
	{
		dlg.m_cf.rgbColors = m_clrValue;
	}

	if (dlg.DoModal() == IDOK)
	{
		LOGFONT lf;
		dlg.GetCurrentFont(&lf);
		SetFont(lf);

		if (m_clrValue != (COLORREF)-1)
			m_clrValue = dlg.GetColor();

		OnValueChanged(m_strValue);
		m_pGrid->Invalidate(FALSE);
	}
}

void CSPPropertyGridItemFont::SetColor(COLORREF clr)
{
	m_clrValue = clr;
	if (m_pGrid && m_pGrid->GetSafeHwnd())
		m_pGrid->Invalidate(FALSE);
}

COLORREF CSPPropertyGridItemFont::GetColor()
{
	return m_clrValue;
}

void CSPPropertyGridItemFont::GetFont(LOGFONT* lf)
{
	ASSERT(lf != NULL);
	MEMCPY_S(lf, &m_lfValue, sizeof(LOGFONT));
}

