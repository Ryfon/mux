﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDITEMBOOL_H__)
#define __SPPROPERTYGRIDITEMBOOL_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class  CSPPropertyGridItemBool : public CSPPropertyGridItem
{
public:
	CSPPropertyGridItemBool(CString strCaption, BOOL bValue = FALSE, BOOL* pBindBool = NULL);
	CSPPropertyGridItemBool(UINT nID, BOOL bValue = FALSE, BOOL* pBindBool = NULL); // <COMBINE CSPPropertyGridItemBool::CSPPropertyGridItemBool@CString@BOOL@BOOL*>

	virtual ~CSPPropertyGridItemBool();

	
	void SetBool(BOOL bValue);
	BOOL GetBool();
	void BindToBool(BOOL* pBindBool);
	BOOL SetTrueFalseText(CString strTrueText, CString strFalseText);

protected:

	void SetValue(CString strValue);
	BOOL SetValueText(CString& strValueText, CString strNewText);
	virtual void OnBeforeInsert();

private:

	void _Init(BOOL bValue);

protected:

	BOOL    m_bValue;       // Value of the item.
	BOOL*   m_pBindBool;    // Binded value.  This is a pointer to the variable bound to this item.
	CString m_strTrueText;  // Text of the TRUE label.
	CString m_strFalseText; // Text of the FALSE label.

private:

	DECLARE_DYNAMIC(CSPPropertyGridItemBool)

};


//===========================================================================
// Summary:
//     CSPPropertyGridItemEnum is a CSPPropertyGridItem derived class.
//     It is used to create an Enum value item in a Property Grid control.
// Remarks:
//     A Enum item is used to display a list of constraints.  This is
//     very similar to a normal constraint list.  The difference is that
//     an Enum constraint list will return the Value of the constraint when
//     selected, where a normal constraint wold return the text of the
//     constraint.
//
// Example:
//     This code illustrates how to add an item of type Enum to your grid.
// <code>
// //Adds a category to the grid called "Software"
// CSPPropertyGridItem* pStandard   = m_wndPropertyGrid.AddCategory(_T("Software"));
//
// //Adds a CSPPropertyGridItemFlags item with a caption of "Current OS" and an initial value of 2
// //This will cause the constraint with a value of 2 to be selected
// CSPPropertyGridItemEnum* pItem = (CSPPropertyGridItemEnum*)(pStandard->AddChildItem(new CSPPropertyGridItemEnum(_T("Current OS"), 2)));
//
// //Adds some constraints along with a value
// pItem->GetConstraints()->AddConstraint(_T("Windows 98"), 1);
// pItem->GetConstraints()->AddConstraint(_T("Windows 2000"), 2);
// pItem->GetConstraints()->AddConstraint(_T("Windows XP"), 3);
//
// //GetEnum will return the Data value of the constraint selected
// TRACE(_T("Enum Item Value= %d\n"), pItem->GetEnum());
//
// //GetValue will return the String of the constraint selected
// TRACE(_T("Enum Item String= %s\n"), pItem->GetValue());
// </code>
//
// See Also: CSPPropertyGridItemEnum::SetEnum, CSPPropertyGridItemEnum::GetEnum, CSPPropertyGridItem::GetValue
//===========================================================================
class  CSPPropertyGridItemEnum : public CSPPropertyGridItem
{
public:

	CSPPropertyGridItemEnum(CString strCaption, int nValue = 0, int* pBindEnum = NULL);
	CSPPropertyGridItemEnum(UINT nID, int nValue = 0, int* pBindEnum = NULL); // <COMBINE CSPPropertyGridItemEnum::CSPPropertyGridItemEnum@CString@int@int*>


	virtual ~CSPPropertyGridItemEnum();

	void SetEnum(int nValue);
	void SetEnum(CSPPropertyGridItemConstraint* pContraint); // <COMBINE CSPPropertyGridItemEnum::SetEnum@int>
	int GetEnum();
	void BindToEnum(int* pBindEnum);

protected:
	void SetValue(CString strValue);
	virtual void OnBeforeInsert();
	virtual void OnConstraintsChanged();
private:
	void _Init(int nValue);
protected:

	int    m_nValue;       // Value of the item.  This will contain the value of the selected constraint, not the text
	int*   m_pBindEnum;    // Binded value.  This is a pointer to the variable bound to this item.

private:

	DECLARE_DYNAMIC(CSPPropertyGridItemEnum)

};


//===========================================================================
// Summary:
//     CSPPropertyGridItemFlags is a CSPPropertyGridItem derived class.
//     It is used to create a Flags value item in a Property Grid control.
// Remarks:
//     A flag item is used to combine multiple boolean values into a
//     single item.  When using a flag item, you must add constraints
//     to the flag item.  Each constraint will have a value of TRUE or
//     FALSE.  The flag item will have an expand button that when clicked
//     will display all boolean constraints for that item.  The value of
//     the flag item will be the sum of all the boolean constraints with
//     a value of TRUE.
//
//          NOTE: Constraint values must be in powers of 2 if you will have an
//                "All" constraint that indicates that the value of all boolean
//                 constraints is TRUE or FALSE.
//
//          In the example below, the flag item's initial value will be 9, this
//          value is stored in CSPPropertyGridItemFlags::m_nValue and can be
//          set with SetFlags and retrieved with GetFlags.  The string value will
//          be "[Windows 98; Windows 95]" and this value is stored in
//          CSPPropertyGridItem::m_strValue, which can be retrieved
//          with CSPPropertyGridItem::GetValue.  You must use the SetFlags method
//          to change the flag items value by code.  The string value is what is
//          displayed to the user.
//
// Example:
// This sample illustrates how to add an item of type Flags to your grid.
// <code>
// //Adds a category to the grid called "Software"
// CSPPropertyGridItem* pStandard   = m_wndPropertyGrid.AddCategory(_T("Software"));
//
// //Adds a CSPPropertyGridItemFlags item with a caption of "Supported OS" and an initial value of 9
// CSPPropertyGridItemFlags* pItem = (CSPPropertyGridItemFlags*)(pStandard->AddChildItem(new CSPPropertyGridItemFlags(_T("Supported OS"), 1 + 8)));
//
// //Adds a constraint that will set all items TRUE or FALSE, note the value of the constraint is the sum
// //of all the other constraints.  This item will also be updated when the values of the other constraints
// //have changed.
// pItem->GetConstraints()->AddConstraint(_T("All Windows"), 1 + 2 + 4 + 8 + 16 + 32);
//
// //Adds a constraint that can have a value of TRUE or FALSE
// pItem->GetConstraints()->AddConstraint(_T("Windows 98"), 1);
// pItem->GetConstraints()->AddConstraint(_T("Windows 2000"), 2);
// pItem->GetConstraints()->AddConstraint(_T("Windows XP"), 4);
// pItem->GetConstraints()->AddConstraint(_T("Windows 95"), 8);
// pItem->GetConstraints()->AddConstraint(_T("Windows NT"), 16);
// pItem->GetConstraints()->AddConstraint(_T("Windows 2003"), 32);
//
// //This changes the value of the flag item to 21 and the string
// //caption to [Windows 98; Windows XP; Windows NT]
// pItem->SetFlags(21);
//
// //GetFlags will return the sum of all true constraints
// TRACE(_T("Flags Item Value= %d\n"), pItem->GetFlags());
//
// //GetValue will return a string containing the text of all true constraints
// TRACE(_T("Flags Item String= %s\n"), pItem->GetValue());
// </code>
//
// See Also: CSPPropertyGridItemFlags::SetFlags, CSPPropertyGridItemFlags::GetFlags, CSPPropertyGridItem::GetValue
//===========================================================================
class  CSPPropertyGridItemFlags : public CSPPropertyGridItem
{
public:

	
	CSPPropertyGridItemFlags(CString strCaption, int nValue = 0, int* pBindFlags = NULL);
	CSPPropertyGridItemFlags(UINT nID, int nValue = 0, int* pBindFlags = NULL);  // <COMBINE CSPPropertyGridItemFlags::CSPPropertyGridItemFlags@CString@int@int*>

	virtual ~CSPPropertyGridItemFlags();
	void SetFlags(int nValue);
	int GetFlags();
	void BindToFlags(int* pBindFlags);

protected:
	void SetValue(CString strValue);
	virtual void OnBeforeInsert();
	virtual void OnConstraintsChanged();
	virtual CString GetFlagsString();
	void UpdateChilds();

	class CSPPropertyGridItemFlag;

private:
	void _Init(int nValue);


protected:
	int    m_nValue;       // Value of the item.  This is the sum of the constraint values that have a value of TRUE.
	int*   m_pBindFlags;   // Binded value. This is a pointer to the variable bound to this item.

private:
	DECLARE_DYNAMIC(CSPPropertyGridItemFlags)


	friend class CSPPropertyGridItemFlag;
};


//////////////////////////////////////////////////////////////////////

AFX_INLINE BOOL CSPPropertyGridItemBool::GetBool() {
	return m_bValue;
}
AFX_INLINE BOOL CSPPropertyGridItemEnum::GetEnum() {
	return m_nValue;
}
AFX_INLINE BOOL CSPPropertyGridItemFlags::GetFlags() {
	return m_nValue;
}



#endif // #if !defined(__SPPROPERTYGRIDITEMBOOL_H__)
