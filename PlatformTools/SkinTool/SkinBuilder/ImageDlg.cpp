﻿// ImageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SkinBuilder.h"
#include "ImageDlg.h"
#include ".\imagedlg.h"


// CImageDlg dialog

IMPLEMENT_DYNAMIC(CImageDlg, CDialog)
CImageDlg::CImageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImageDlg::IDD, pParent)
	, m_nSectionTop(0)
	, m_nSectionLeft(0)
	, m_nSectionRight(10)
	, m_nSectionBottom(10)
	, m_nMarginsLeft(0)
	, m_nMarginsTop(0)
	, m_nMarginsRight(0)
	, m_nMarginsBottom(0)
	, m_nDrawMode(0)
{
	m_pCurrentBitmap = NULL;
}

CImageDlg::~CImageDlg()
{
}

void CImageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_IMAGE, m_listImage);
	DDX_Text(pDX, IDC_EDIT_SELECTIONTOP, m_nSectionTop);
	DDX_Text(pDX, IDC_EDIT_SELECTIONLEFT, m_nSectionLeft);
	DDX_Text(pDX, IDC_EDIT_SELECTIONRIGHT, m_nSectionRight);
	DDX_Text(pDX, IDC_EDIT_SELECTIONBOTTOM, m_nSectionBottom);
	DDX_Text(pDX, IDC_EDIT_MARGINSLEFT, m_nMarginsLeft);
	DDX_Text(pDX, IDC_EDIT_MARGINSTOP, m_nMarginsTop);
	DDX_Text(pDX, IDC_EDIT_MARGINSRIGHT, m_nMarginsRight);
	DDX_Text(pDX, IDC_EDIT_MARGINSBOTTOM, m_nMarginsBottom);
	DDX_CBIndex(pDX, IDC_COMBO_DRAWMODE, m_nDrawMode);
	DDX_Control(pDX, IDC_COMBO_DRAWMODE, m_combDrawMode);
}


BEGIN_MESSAGE_MAP(CImageDlg, CDialog)
	ON_EN_CHANGE(IDC_EDIT_SELECTIONLEFT, OnEnChangeEditSelectionleft)
	ON_EN_CHANGE(IDC_EDIT_SELECTIONTOP, OnEnChangeEditSelectiontop)
	ON_EN_CHANGE(IDC_EDIT_SELECTIONRIGHT, OnEnChangeEditSelectionright)
	ON_EN_CHANGE(IDC_EDIT_SELECTIONBOTTOM, OnEnChangeEditSelectionbottom)
	ON_EN_CHANGE(IDC_EDIT_MARGINSLEFT, OnEnChangeEditMarginsleft)
	ON_EN_CHANGE(IDC_EDIT_MARGINSTOP, OnEnChangeEditMarginstop)
	ON_EN_CHANGE(IDC_EDIT_MARGINSRIGHT, OnEnChangeEditMarginsright)
	ON_EN_CHANGE(IDC_EDIT_MARGINSBOTTOM, OnEnChangeEditMarginsbottom)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_MARGINSBOTTOM, OnDeltaposSpinMarginsbottom)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_MARGINSLEFT, OnDeltaposSpinMarginsleft)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_MARGINSRIGHT, OnDeltaposSpinMarginsright)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_MARGINSTOP, OnDeltaposSpinMarginstop)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SELECTIONBOTTOM, OnDeltaposSpinSelectionbottom)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SELECTIONLEFT, OnDeltaposSpinSelectionleft)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SELECTIONRIGHT, OnDeltaposSpinSelectionright)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SELECTIONTOP, OnDeltaposSpinSelectiontop)
	ON_LBN_SELCHANGE(IDC_LIST_IMAGE, OnLbnSelchangeListImage)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM1, OnBnClickedButtonZoom1)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM2, OnBnClickedButtonZoom2)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM3, OnBnClickedButtonZoom3)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM4, OnBnClickedButtonZoom4)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM5, OnBnClickedButtonZoom5)
	ON_CBN_SELCHANGE(IDC_COMBO_DRAWMODE, OnCbnSelchangeComboDrawmode)
END_MESSAGE_MAP()


// CImageDlg message handlers

BOOL CImageDlg::OnInitDialog()
{
	
	CDialog::OnInitDialog();
	// TODO:  Add extra initialization here
	CSkinImages *pImages = GetImages();

	for(int i = 0 ; i < keStretchCount ; i++)
	{
		m_combDrawMode.AddString(stretch[i]);
	}
	
	
	if(m_wndImage.m_bSection)
	{
		if(m_wndImage.m_pImageSection != NULL)
		{
			m_nSectionLeft = m_wndImage.m_pImageSection->rtImagePos.left;
			m_nSectionRight = m_wndImage.m_pImageSection->rtImagePos.right;
			m_nSectionTop = m_wndImage.m_pImageSection->rtImagePos.top;
			m_nSectionBottom = m_wndImage.m_pImageSection->rtImagePos.bottom;
			m_wndImage.m_rtTracker = m_wndImage.m_pImageSection->rtImagePos;

			m_nMarginsLeft = m_wndImage.m_pImageSection->marginImage.left;
			m_nMarginsRight = m_wndImage.m_pImageSection->marginImage.right;
			m_nMarginsTop = m_wndImage.m_pImageSection->marginImage.top;
			m_nMarginsBottom = m_wndImage.m_pImageSection->marginImage.bottom;
			m_wndImage.m_rtMargins = m_wndImage.m_pImageSection->marginImage;

			m_combDrawMode.SetCurSel(m_wndImage.m_pImageSection->stretchDrawMode);
			m_nDrawMode = m_wndImage.m_pImageSection->stretchDrawMode;
			UpdateData(FALSE);
		}
		CString strCaption;
		strCaption = _T("ImageSection   ") + (CString)m_wndImage.m_pImageSection->strImageName;
		SetWindowText(strCaption);
	}
	else
	{
		if(m_wndImage.m_pImageRect != NULL)
		{
			m_nSectionLeft = m_wndImage.m_pImageRect->rtImagePos.left;
			m_nSectionRight = m_wndImage.m_pImageRect->rtImagePos.right;
			m_nSectionTop = m_wndImage.m_pImageRect->rtImagePos.top;
			m_nSectionBottom = m_wndImage.m_pImageRect->rtImagePos.bottom;
			m_wndImage.m_rtTracker = m_wndImage.m_pImageRect->rtImagePos;
			UpdateData(FALSE);
		}
		CString strCaption;
		strCaption = _T("ImageRect   ") + (CString)m_wndImage.m_pImageRect->strImageName;
		SetWindowText(strCaption);
	}


	//	m_listImage.SetItemState(nSelected,LVIS_SELECTED ,LVIF_STATE);
	CRect rtWindow;
	GetDlgItem(IDC_STATIC_EDITIMAGE)->GetWindowRect(&rtWindow);
	ScreenToClient(&rtWindow);
	rtWindow.InflateRect(1,1);
	m_wndImage.Create(rtWindow,this);

	int nItem,nSelected = 0;
	CString strName ;
	if(m_wndImage.m_bSection)
		strName= m_wndImage.m_pImageSection->strImageName;
	else
		strName = m_wndImage.m_pImageRect->strImageName;
	for(int i = 0 ;i < pImages->m_arrayImages.GetCount(); i++)
	{
		CSkinBitmap *pBitmap = pImages->m_arrayImages.GetAt(i);
		nItem = m_listImage.AddString(pBitmap->m_strName);
		if(strName == pBitmap->m_strName)
		{
			m_pCurrentBitmap = pBitmap;
			m_wndImage.m_pCurrentBitmap = pBitmap;
			nSelected = nItem;
			m_listImage.SetCurSel(nSelected);
		}
	}
	
	
	m_wndImage.Invalidate(NULL);
	m_wndImage.UpdateWindow();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CImageDlg::OnLvnItemchangedListImage(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	/*POSITION pos =  m_listImage.GetFirstSelectedItemPosition();
	
	if(pos)
	{
		int nCurrent = m_listImage.GetNextSelectedItem(pos);
		CString strFile = m_listImage.GetItemText(nCurrent,0);
		m_pCurrentBitmap = GetSkin().FindIamge(strFile);
		m_wndImage.m_pCurrentBitmap = m_pCurrentBitmap;
		if(m_wndImage.m_bSection)
		{
			strcpy(m_wndImage.m_pImageSection->strImageName,m_pCurrentBitmap->m_strName);
		}
		else
		{
			strcpy(m_wndImage.m_pImageRect->strImageName,m_pCurrentBitmap->m_strName);
		}
		m_wndImage.InvalidateRect(NULL,TRUE);
	}
	*/
	*pResult = 0;
}
void CImageDlg::UpdateRect(CRect rect)
{
	m_nSectionTop = rect.top;
	m_nSectionLeft = rect.left;
	m_nSectionRight = rect.right;
	m_nSectionBottom = rect.bottom;
	UpdateData(FALSE);
	
}
void CImageDlg::OnEnChangeEditSelectionleft()
{
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtTracker.left = m_nSectionLeft;
		m_wndImage.m_pImageSection->rtImagePos.left = m_nSectionLeft;
	}
	else
	{
		m_wndImage.m_rtTracker.left = m_nSectionLeft;
		m_wndImage.m_pImageRect->rtImagePos.left = m_nSectionLeft;
	}
	
	m_wndImage.Invalidate();
}

void CImageDlg::OnEnChangeEditSelectiontop()
{
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtTracker.top = m_nSectionTop;
		m_wndImage.m_pImageSection->rtImagePos.top = m_nSectionTop;
	}
	else
	{
		m_wndImage.m_rtTracker.top = m_nSectionTop;
		m_wndImage.m_pImageRect->rtImagePos.top = m_nSectionTop;
	}
	m_wndImage.Invalidate();
}

void CImageDlg::OnEnChangeEditSelectionright()
{
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtTracker.right = m_nSectionRight;
		m_wndImage.m_pImageSection->rtImagePos.right = m_nSectionRight;
	}
	else
	{
		m_wndImage.m_rtTracker.right = m_nSectionRight;
		m_wndImage.m_pImageRect->rtImagePos.right = m_nSectionRight;
	}
	m_wndImage.Invalidate();
}

void CImageDlg::OnEnChangeEditSelectionbottom()
{
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtTracker.bottom = m_nSectionBottom;
		m_wndImage.m_pImageSection->rtImagePos.bottom = m_nSectionBottom;
	}
	else
	{
		m_wndImage.m_rtTracker.bottom = m_nSectionBottom;
		m_wndImage.m_pImageRect->rtImagePos.bottom = m_nSectionBottom;
	}
	m_wndImage.Invalidate();
}

void CImageDlg::OnEnChangeEditMarginsleft()
{
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtMargins.left = m_nMarginsLeft;
		m_wndImage.m_pImageSection->marginImage.left = m_nMarginsLeft;
	}
	m_wndImage.Invalidate();
}

void CImageDlg::OnEnChangeEditMarginstop()
{
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtMargins.top = m_nMarginsTop;
		m_wndImage.m_pImageSection->marginImage.top = m_nMarginsTop;
	}
	m_wndImage.Invalidate();
}

void CImageDlg::OnEnChangeEditMarginsright()
{
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtMargins.right = m_nMarginsRight;
		m_wndImage.m_pImageSection->marginImage.right = m_nMarginsRight;
	}
	m_wndImage.Invalidate();
}

void CImageDlg::OnEnChangeEditMarginsbottom()
{
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtMargins.bottom = m_nMarginsBottom;
		m_wndImage.m_pImageSection->marginImage.bottom = m_nMarginsBottom;
	}
	m_wndImage.Invalidate();
}

void CImageDlg::OnDeltaposSpinMarginsbottom(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_nMarginsBottom += pNMUpDown->iDelta;
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtMargins.bottom = m_nMarginsBottom;
		m_wndImage.m_pImageSection->marginImage.bottom = m_nMarginsBottom;
	}
	m_wndImage.Invalidate();
	UpdateData(FALSE);
	*pResult = 0;
}

void CImageDlg::OnDeltaposSpinMarginsleft(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_nMarginsLeft += pNMUpDown->iDelta;
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtMargins.left = m_nMarginsLeft;
		m_wndImage.m_pImageSection->marginImage.left = m_nMarginsLeft;
	}
	m_wndImage.Invalidate();
	UpdateData(FALSE);
	*pResult = 0;
}

void CImageDlg::OnDeltaposSpinMarginsright(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_nMarginsRight += pNMUpDown->iDelta;
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtMargins.right = m_nMarginsRight;
		m_wndImage.m_pImageSection->marginImage.right = m_nMarginsRight;
	}
	m_wndImage.Invalidate();
	UpdateData(FALSE);
	*pResult = 0;
}

void CImageDlg::OnDeltaposSpinMarginstop(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_nMarginsTop += pNMUpDown->iDelta;
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtMargins.top = m_nMarginsTop;
		m_wndImage.m_pImageSection->marginImage.top = m_nMarginsTop;
	}
	m_wndImage.Invalidate();
	UpdateData(FALSE);
	*pResult = 0;
}

void CImageDlg::OnDeltaposSpinSelectionbottom(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_nSectionBottom +=pNMUpDown->iDelta;
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtTracker.bottom = m_nSectionBottom;
		m_wndImage.m_pImageSection->rtImagePos.bottom = m_nSectionBottom;
	}
	else
	{
		m_wndImage.m_rtTracker.bottom = m_nSectionBottom;
		m_wndImage.m_pImageRect->rtImagePos.bottom = m_nSectionBottom;
	}
	m_wndImage.Invalidate();
	UpdateData(FALSE);
	*pResult = 0;
}

void CImageDlg::OnDeltaposSpinSelectionleft(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_nSectionLeft += pNMUpDown->iDelta;
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtTracker.left = m_nSectionLeft;
		m_wndImage.m_pImageSection->rtImagePos.left = m_nSectionLeft;
	}
	else
	{
		m_wndImage.m_rtTracker.left = m_nSectionLeft;
		m_wndImage.m_pImageRect->rtImagePos.left = m_nSectionLeft;
	}
	
	m_wndImage.Invalidate();
	UpdateData(FALSE);
	*pResult = 0;
}

void CImageDlg::OnDeltaposSpinSelectionright(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_nSectionRight += pNMUpDown->iDelta;

	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtTracker.right = m_nSectionRight;
		m_wndImage.m_pImageSection->rtImagePos.right = m_nSectionRight;
	}
	else
	{
		m_wndImage.m_rtTracker.right = m_nSectionRight;
		m_wndImage.m_pImageRect->rtImagePos.right = m_nSectionRight;
	}
	
	m_wndImage.Invalidate();
	UpdateData(FALSE);
	*pResult = 0;
}

void CImageDlg::OnDeltaposSpinSelectiontop(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_nSectionTop += pNMUpDown->iDelta;
	if(m_wndImage.m_bSection)
	{
		m_wndImage.m_rtTracker.top = m_nSectionTop;
		m_wndImage.m_pImageSection->rtImagePos.top = m_nSectionTop;
	}
	else
	{
		m_wndImage.m_rtTracker.top = m_nSectionTop;
		m_wndImage.m_pImageRect->rtImagePos.top = m_nSectionTop;
	}
	m_wndImage.Invalidate();
	UpdateData(FALSE);
	*pResult = 0;
}

void CImageDlg::OnLbnSelchangeListImage()
{
	// TODO: Add your control notification handler code here
	int nSel = m_listImage.GetCurSel();
	
	CString strFile;
	m_listImage.GetText(nSel,strFile);
	m_pCurrentBitmap = GetSkin().FindIamge(strFile);
	m_wndImage.m_pCurrentBitmap = m_pCurrentBitmap;
	if(m_wndImage.m_bSection)
	{
		strcpy(m_wndImage.m_pImageSection->strImageName,m_pCurrentBitmap->m_strName);
	}
	else
	{
		strcpy(m_wndImage.m_pImageRect->strImageName,m_pCurrentBitmap->m_strName);
	}
	m_wndImage.InvalidateRect(NULL,TRUE);
}

void CImageDlg::OnBnClickedButtonZoom1()
{
	// TODO: Add your control notification handler code here
	m_wndImage.m_fZoom = 1.0f;
	m_wndImage.SetScrool();
	m_wndImage.Invalidate();
}

void CImageDlg::OnBnClickedButtonZoom2()
{
	// TODO: Add your control notification handler code here
	m_wndImage.m_fZoom = 1.5f;
	m_wndImage.SetScrool();
	m_wndImage.Invalidate();
}

void CImageDlg::OnBnClickedButtonZoom3()
{
	// TODO: Add your control notification handler code here
	m_wndImage.m_fZoom = 2.0f;
	m_wndImage.SetScrool();
	m_wndImage.Invalidate();
}

void CImageDlg::OnBnClickedButtonZoom4()
{
	// TODO: Add your control notification handler code here
	m_wndImage.m_fZoom = 3.0f;
	m_wndImage.SetScrool();
	m_wndImage.Invalidate();
}

void CImageDlg::OnBnClickedButtonZoom5()
{
	// TODO: Add your control notification handler code here
	m_wndImage.m_fZoom = 4.0f;
	m_wndImage.SetScrool();
	m_wndImage.Invalidate();
}

void CImageDlg::OnCbnSelchangeComboDrawmode()
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if(m_wndImage.m_bSection)
	{
		if(m_wndImage.m_pImageSection != NULL)
		{
			m_wndImage.m_pImageSection->stretchDrawMode = m_nDrawMode;
		}
	}
	else
	{
		if(m_wndImage.m_pImageRect != NULL)
		{
			m_wndImage.m_pImageRect->stretchDrawMode = m_nDrawMode;
		}
	}
}
