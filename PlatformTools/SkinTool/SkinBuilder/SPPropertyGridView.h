﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDVIEW_H__)
#define __SPPROPERTYGRIDVIEW_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SPPropertyGridPaintManager.h"
#include "SPDrawHelpers.h"

class CSPPropertyGridView;
class CSPPropertyGridPaintManager;
class CSPPropertyGrid;

//===========================================================================
// Summary:
//     CSPPropertyGridToolTip is a CWnd derived class.
//     It is an internal class used by Property Grid control
//===========================================================================
class  CSPPropertyGridToolTip : public CWnd
{
public:
	//-----------------------------------------------------------------------
	// Summary:
	//     Activate Tooltip control.
	// Parameters:
	//     bActive - TRUE to activate.
	//-----------------------------------------------------------------------
	void Activate(BOOL bActive);

	//-----------------------------------------------------------------------
	// Summary:
	//     Creates Tooltip control.
	// Parameters:
	//     pParentWnd - Points to a CSPPropertyGridView object.
	//-----------------------------------------------------------------------
	void Create(CSPPropertyGridView* pParentWnd);

protected:
	CSPPropertyGridView* m_pGrid;      // Parent window.

protected:

//{{AFX_CODEJOCK_PRIVATE
	DECLARE_MESSAGE_MAP()
	//{{AFX_MSG(CSPPropertyGridView)
	afx_msg BOOL OnEraseBkgnd(CDC*);
	afx_msg void OnPaint();
	afx_msg LRESULT OnNcHitTest(CPoint point);
	//}}AFX_MSG
//}}AFX_CODEJOCK_PRIVATE

private:
	BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
};

AFX_INLINE BOOL CSPPropertyGridToolTip::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) {
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}


//===========================================================================
// Summary:
//     CSPPropertyGridView is a CListBox derived class.
//     It is an internal class used by Property Grid control
//===========================================================================
class  CSPPropertyGridView : public CListBox
{
	DECLARE_DYNAMIC(CSPPropertyGridView)

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridView object
	//-----------------------------------------------------------------------
	CSPPropertyGridView();

	//-----------------------------------------------------------------------
	// Summary:
	//     Destroys a CSPPropertyGridView object, handles cleanup and
	//     deallocation.
	//-----------------------------------------------------------------------
	virtual ~CSPPropertyGridView();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to get the position of the divider.
	// Returns:
	//     Position of the divider
	//-----------------------------------------------------------------------
	int GetDividerPos();

	//-----------------------------------------------------------------------
	// Summary:
	//     This method determines where a point lies in a specified item.
	// Parameters:
	//     point - Specifies the point to be tested.
	// Returns:
	//     A pointer to a CSPPropertyGridItem object
	//-----------------------------------------------------------------------
	CSPPropertyGridItem* ItemFromPoint(CPoint point);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to refresh items in the list.
	//-----------------------------------------------------------------------
	void Refresh();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to remove all items in the list.
	//-----------------------------------------------------------------------
	void ResetContent();

	//-----------------------------------------------------------------------
	// Summary:
	//     This method retrieves an item with the specified index.
	// Parameters:
	//     nIndex - Specifies the zero-based index of the item to retrieve.
	// Returns:
	//     A pointer to a CSPPropertyGridItem object
	//-----------------------------------------------------------------------
	CSPPropertyGridItem* GetItem(int nIndex);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member function to add a new category to a Property
	//     Grid Control.
	// Parameters:
	//     strCaption - Name of the category to add.
	//     pCategory  - Points to a CSPPropertyGridItem object.
	// Returns:
	//     A pointer to a CSPPropertyGridItem object.
	//-----------------------------------------------------------------------
	CSPPropertyGridItem* AddCategory(CString strCaption, CSPPropertyGridItem* pCategory = NULL);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member function to insert a new category to the Property Grid control.
	// Parameters:
	//     strCaption - Name of the category to add.
	//     nIndex     - Index of category to be inserted
	//     pCategory  - Points to a CSPPropertyGridItem object.
	// Returns:
	//     The pointer to the item object of the newly inserted category.
	//-----------------------------------------------------------------------
	CSPPropertyGridItem* InsertCategory(int nIndex, CString strCaption, CSPPropertyGridItem* pCategory = NULL);

private:

	CSPPropertyGridItem* GetSelectedItem();
	void SetPropertySort(SPPropertyGridSortOrder sort, BOOL bRrefresh = FALSE);
	int InsertItem(CSPPropertyGridItem* pItem, int nIndex);
	int _DoExpand(CSPPropertyGridItem* pItem, int nIndex);
	void _DoCollapse(CSPPropertyGridItem* pItem);
	void _RefreshIndexes();
	void SwitchExpandState(int nItem);
	int HitTest(CPoint point);

	void ShowToolTip(CPoint pt);
	void _ShowToolTip(CRect rcBound, CRect rcText);
	CSize _GetTextExtent(CString str);
	void RelayToolTipEvent(UINT message);


public:
	CFont                         m_fontNormal;         // Normal font.
	CFont                         m_fontBold;           // Bold font.

	CSPPaintManagerColor         m_clrLine;            // Line color.
	CSPPaintManagerColor         m_clrFore;            // Color of the item's text.
	CSPPaintManagerColor         m_clrCategoryFore;    // Color of the category text.
	CSPPaintManagerColor         m_clrBack;            // Background color.
	CSPPaintManagerColor         m_clrReadOnlyFore;    // Color of read-only text.
	CSPPropertyGrid*             m_pGrid;              // Parent CSPPropertyGrid class.

protected:
	BOOL                          m_bTracking;          // TRUE if grid in tracking mode.
	CRect                         m_rcToolTip;          // Position of the Tooltip.
	double                        m_dDivider;           // Divider position.
	HCURSOR                       m_hCursor;            // Handle of the cursor.
	CString                       m_strTipText;         // Tooltip text.
	SPPropertyGridSortOrder      m_properetySort;      // Current sort order.
	CSPPropertyGridItem*         m_pSelected;          // Current selected item.
	CSPPropertyGridItems*        m_pCategories;        // Collection of the categories.
	CSPPropertyGridToolTip       m_wndTip;             // Tooltip control.
	CSPPropertyGridInplaceEdit   m_wndEdit;            // In-place edit control.
	CSPPropertyGridInplaceButton m_wndButton;          // In-place button control.
	CSPPropertyGridInplaceList   m_wndListBox;         // In-place list control.
	int                           m_nLockUpdate;        // TRUE to lock update.


protected:
//{{AFX_CODEJOCK_PRIVATE
	//{{AFX_VIRTUAL(CSPPropertyGridView)
	virtual void DrawItem(LPDRAWITEMSTRUCT);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT);

	virtual int CompareItem(LPCOMPAREITEMSTRUCT) { return 0;}
	virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSPPropertyGridView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnNcPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSelectionChanged();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnChar(UINT nChar, UINT nRepCntr, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg UINT OnGetDlgCode();
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus (CWnd* pNewWnd);
	//}}AFX_MSG
//}}AFX_CODEJOCK_PRIVATE

private:
	DECLARE_MESSAGE_MAP()

	friend class CSPPropertyGridItem;
	friend class CSPPropertyGridItems;
	friend class CSPPropertyGrid;
	friend class CSPPropertyGridInplaceEdit;
	friend class CPropertyGridCtrl;
	friend class CSPPropertyGridToolTip;
};


#endif // #if !defined(__SPPROPERTYGRIDVIEW_H__)
