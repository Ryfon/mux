﻿// ImageEditWnd.cpp : implementation file
//

#include "stdafx.h"
#include "SkinBuilder.h"
#include "ImageEditWnd.h"
#include ".\imageeditwnd.h"
#include "ImageDlg.h"


// CImageEditWnd

IMPLEMENT_DYNAMIC(CImageEditWnd, CWnd)
CImageEditWnd::CImageEditWnd()
{
	m_pCurrentBitmap = NULL;
	m_pImageRect = NULL;
	m_pImageSection = NULL;
	//m_tracker.m_rect = CRect(23,23,123,123);
	m_rtTracker = CRect(0,0,100,100);
	m_rtMargins = CRect(0,0,0,0);
	m_tracker.m_nStyle = CRectTracker::resizeOutside;
	m_fZoom = 1.0f;
	m_nHScrollPos = 0;
	m_nVScrollPos = 0;
}

CImageEditWnd::~CImageEditWnd()
{
}


BEGIN_MESSAGE_MAP(CImageEditWnd, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// CImageEditWnd message handlers


BOOL CImageEditWnd::Create(  const RECT& rect, CWnd* pParentWnd)
{
	// TODO: Add your specialized code here and/or call the base class

	return CWnd::Create(NULL, NULL, WS_CHILD|WS_VISIBLE|WS_EX_CLIENTEDGE|WS_HSCROLL|WS_VSCROLL, rect, pParentWnd, NULL, NULL);
}
void CImageEditWnd::SetScrool()
{
	int nWidth;
	int nHeight;

	nHeight = m_pCurrentBitmap->GetHeight();
	nWidth = m_pCurrentBitmap->GetWidth();

	int nDstHeight,nDstWidht;
	nDstHeight = (int)(m_fZoom * nHeight) + 60;
	nDstWidht = (int)(m_fZoom * nWidth) + 60;

	SCROLLINFO si;
	si.cbSize = sizeof(si);
	if(nDstWidht > m_nViewWidth )
	{
		
		si.fMask = SIF_PAGE | SIF_RANGE ;
		si.nMin = 0;
		si.nMax = nDstWidht + 60;
		//	si.nPos = 30;
		si.nPage = m_nViewWidth;

		SetScrollInfo (SB_HORZ, &si, TRUE);
	}

	if( nDstHeight > m_nViewHeight)
	{
		si.fMask = SIF_PAGE | SIF_RANGE ;
		si.nMin = 0;
		si.nMax = nDstHeight + 60;
		//	si.nPos = 30;
		si.nPage = m_nViewHeight;

		SetScrollInfo (SB_VERT, &si, TRUE);

	}
}
void CImageEditWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CWnd::OnPaint() for painting messages
	if(!m_pCurrentBitmap)
		return;
	/*CRect rtClient;
	GetClientRect(&rtClient);*/

	int nWidth;
	int nHeight;

	nHeight = m_pCurrentBitmap->GetHeight();
	nWidth = m_pCurrentBitmap->GetWidth();
	
	int nDstHeight,nDstWidht;
	nDstHeight = (int)(m_fZoom * nHeight);
	nDstWidht = (int)(m_fZoom * nWidth);

	

	int nLeft,nTop,nRight,nBottom;

	//if(nDstWidht > m_nViewWidth || nDstHeight > m_nViewHeight)
	//{
	//	SCROLLINFO si;
	//	si.fMask = SIF_PAGE | SIF_RANGE ;
	//	si.nMin = 0;
	//	si.nMax = nDstWidht + 60;
	//	//	si.nPos = 30;
	//	si.nPage = m_nViewWidth;

	//	SetScrollInfo (SB_HORZ, &si, TRUE);

	//	si.fMask = SIF_PAGE | SIF_RANGE ;
	//	si.nMin = 0;
	//	si.nMax = nDstHeight + 60;
	//	//	si.nPos = 30;
	//	si.nPage = m_nViewHeight;

	//	SetScrollInfo (SB_VERT, &si, TRUE);

	//}
	
	nLeft = 40 - m_nHScrollPos;
	nTop = 40 - m_nVScrollPos;
	m_ptLeftTop = CPoint(nLeft,nTop);
	nRight = nLeft + nDstWidht;
	nBottom = nTop + nDstHeight;
	
//	CMemDC memDC(&dc,CRect(nLeft,nTop,nRight,nBottom));
	m_pCurrentBitmap->Draw(&dc,CRect(nLeft,nTop,nRight,nBottom),CRect(0,0,nWidth,nHeight));
	

	CRect rtTemp = m_rtTracker;
	rtTemp.bottom *= m_fZoom;
	rtTemp.right *= m_fZoom;
	rtTemp.left *= m_fZoom;
	rtTemp.top *= m_fZoom;
	
	m_tracker.m_rect = rtTemp + m_ptLeftTop;
	CPen pen(PS_SOLID,1,RGB(255,0,0));
	CBrush *pOldBrush = (CBrush*)dc.SelectStockObject(NULL_BRUSH);
	CPen *pOld = dc.SelectObject(&pen);
	int nOldROP = dc.SetROP2(R2_COPYPEN);
	CRect rtTracker = rtTemp + m_ptLeftTop;
	CPoint ptStart,ptEnd;

	CRect rtTempMargins = m_rtMargins;
	rtTempMargins.left *= m_fZoom;
	rtTempMargins.right *= m_fZoom;
	rtTempMargins.bottom *= m_fZoom;
	rtTempMargins.top *= m_fZoom;

	if(rtTempMargins.left)
	{
		ptStart = CPoint(rtTracker.left + rtTempMargins.left,rtTracker.top);
		ptEnd = CPoint(rtTracker.left + rtTempMargins.left,rtTracker.bottom);
		dc.MoveTo(ptStart);
		dc.LineTo(ptEnd);
	}
	
	if(rtTempMargins.top)
	{
		ptStart = CPoint(rtTracker.left,rtTracker.top + rtTempMargins.top);
		ptEnd = CPoint(rtTracker.right,rtTracker.top + rtTempMargins.top);
		dc.MoveTo(ptStart);
		dc.LineTo(ptEnd);
	}

	if(rtTempMargins.right)
	{
		ptStart = CPoint(rtTracker.right - rtTempMargins.right,rtTracker.top);
		ptEnd = CPoint(rtTracker.right - rtTempMargins.right,rtTracker.bottom);
		dc.MoveTo(ptStart);
		dc.LineTo(ptEnd);
	}
	
	if(rtTempMargins.bottom)
	{
		ptStart = CPoint(rtTracker.left,rtTracker.bottom - rtTempMargins.bottom);
		ptEnd = CPoint(rtTracker.right,rtTracker.bottom - rtTempMargins.bottom);
		dc.MoveTo(ptStart);
		dc.LineTo(ptEnd);
	}
	
	m_tracker.Draw(&dc);
	dc.Rectangle(rtTracker);
	dc.SelectObject(pOld);
	dc.SelectObject(pOldBrush);
	dc.SetROP2(nOldROP);
}

int CImageEditWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rtClient;
	GetClientRect(&rtClient);
	
	m_nViewWidth = rtClient.Width();
	m_nViewHeight = rtClient.Height();

	int cx = m_nViewWidth;
	int cy = m_nViewHeight;

	int nHScrollMax = 0;
	m_nHScrollPos = m_nHPageSize = 0;

	if (cx < m_nViewWidth) {
		nHScrollMax = m_nViewWidth - 1;
		m_nHPageSize = cx;
		m_nHScrollPos = min (m_nHScrollPos, m_nViewWidth -
			m_nHPageSize - 1);
	}


	SCROLLINFO si;
	si.fMask = SIF_PAGE | SIF_RANGE | SIF_POS;
	si.nMin = 0;
	si.nMax = nHScrollMax;
	si.nPos = m_nHScrollPos;
	si.nPage = m_nHPageSize;

	SetScrollInfo (SB_HORZ, &si, TRUE);

	//
	// Set the vertical scrolling parameters.
	//
	int nVScrollMax = 0;
	m_nVScrollPos = m_nVPageSize = 0;

	if (cy < m_nViewHeight) {
		nVScrollMax = m_nViewHeight - 1;
		m_nVPageSize = cy;
		m_nVScrollPos = min (m_nVScrollPos, m_nViewHeight -
			m_nVPageSize - 1);
	}

	si.fMask = SIF_PAGE | SIF_RANGE | SIF_POS;
	si.nMin = 0;
	si.nMax = nVScrollMax;
	si.nPos = m_nVScrollPos;
	si.nPage = m_nVPageSize;

	SetScrollInfo (SB_VERT, &si, TRUE);

	return 0;
}

void CImageEditWnd::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

}

void CImageEditWnd::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	int nDelta;

	switch (nSBCode) {

	case SB_LINELEFT:
		nDelta = -20;
		break;

	case SB_PAGELEFT:
		nDelta = -m_nVPageSize;
		break;

	case SB_THUMBTRACK:
		nDelta = (int) nPos - m_nVScrollPos;
		break;

	case SB_PAGERIGHT:
		nDelta = m_nVPageSize;
		break;

	case SB_LINERIGHT:
		nDelta = 20;
		break;
	default: // Ignore other scroll bar messages
		return;
	}

	int nScrollPos = m_nVScrollPos + nDelta;
	int nMaxPos = m_nViewHeight - m_nVPageSize;

	if (nScrollPos < 0)
		nDelta = -m_nVScrollPos;
	else if (nScrollPos > nMaxPos)
		nDelta = nMaxPos - m_nVScrollPos;

	if (nDelta != 0) {
		m_nVScrollPos += nDelta;
		SetScrollPos (SB_VERT, m_nVScrollPos, TRUE);
		ScrollWindow (0, -nDelta);
	}



//	CWnd::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CImageEditWnd::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default

	int nDelta;

	switch (nSBCode) {

	case SB_LINELEFT:
		nDelta = -20;
		break;

	case SB_PAGELEFT:
		nDelta = -m_nHPageSize;
		break;

	case SB_THUMBTRACK:
		nDelta = (int) nPos - m_nHScrollPos;
		break;

	case SB_PAGERIGHT:
		nDelta = m_nHPageSize;
		break;

	case SB_LINERIGHT:
		nDelta = 20;
		break;
	default: // Ignore other scroll bar messages
		return;
	}

	int nScrollPos = m_nHScrollPos + nDelta;
	int nMaxPos = m_nViewWidth - m_nHPageSize;

	if (nScrollPos < 0)
		nDelta = -m_nHScrollPos;
	else if (nScrollPos > nMaxPos)
		nDelta = nMaxPos - m_nHScrollPos;

	if (nDelta != 0) {
		m_nHScrollPos += nDelta;
		SetScrollPos (SB_HORZ, m_nHScrollPos, TRUE);
		ScrollWindow (-nDelta, 0);	
	//	Invalidate();
	}


	//CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

BOOL CImageEditWnd::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default
	CRect rtClient;
	GetClientRect(&rtClient);
	
	CBrush brush(::GetSysColor(COLOR_WINDOW));
	pDC->FillRect(rtClient,&brush);

	return TRUE;
}

void CImageEditWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CRect rtClient;
	GetClientRect(&rtClient);
	if(m_tracker.Track(this,point,FALSE,NULL))
	{	

		m_rtTracker = m_tracker.m_rect ;

		
		CImageDlg *pDlg = (CImageDlg*)GetParent();
		m_rtTracker = m_rtTracker - m_ptLeftTop ;
		m_rtTracker.left = (float)m_rtTracker.left/m_fZoom;
		m_rtTracker.top = (float)m_rtTracker.top/m_fZoom;
		m_rtTracker.bottom = (float)m_rtTracker.bottom/m_fZoom;
		m_rtTracker.right = (float)m_rtTracker.right/m_fZoom;
		if(m_bSection)
			m_pImageSection->rtImagePos = m_rtTracker;
		else
			m_pImageRect->rtImagePos = m_rtTracker;
		pDlg->UpdateRect(m_rtTracker);

		InvalidateRect(&rtClient);
	}


	CWnd::OnLButtonDown(nFlags, point);
}

void CImageEditWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	UINT uHit = m_tracker.HitTest(point);
	m_tracker.SetCursor(this,uHit);
	CWnd::OnMouseMove(nFlags, point);
}
