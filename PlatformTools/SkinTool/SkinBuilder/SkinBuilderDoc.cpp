﻿// SkinBuilderDoc.cpp :  CSkinBuilderDoc 类的实现
//

#include "stdafx.h"
#include "SkinBuilder.h"

#include "SkinBuilderDoc.h"
#include "MainFrm.h"
#include ".\skinbuilderdoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSkinBuilderDoc
IMPLEMENT_DYNCREATE(CSkinBuilderDoc, CDocument)

BEGIN_MESSAGE_MAP(CSkinBuilderDoc, CDocument)
	ON_COMMAND(ID_OBJECTS_LOADIMAGE, OnObjectsLoadimage)
	ON_COMMAND(ID_WINDOW_FRAMEWINDOW, OnWindowFramewindow)
	ON_COMMAND(ID_WINDOWLESS_WINBUTTONSKIN, OnWindowlessWinbuttonskin)
	ON_UPDATE_COMMAND_UI(ID_WINDOWLESS_WINBUTTONSKIN, OnUpdateWindowlessWinbuttonskin)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINTOOLBAR, OnStandardcontrolsSkintoolbar)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINSCROLLBAR, OnStandardcontrolsSkinscrollbar)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINBUTTON, OnStandardcontrolsSkinbutton)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINRADIO, OnStandardcontrolsSkinradio)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINEDIT, OnStandardcontrolsSkinedit)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINCHECKBOX, OnStandardcontrolsSkincheckbox)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINGROUPBOX, OnStandardcontrolsSkingroupbox)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINSTATIC, OnStandardcontrolsSkinstatic)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINCOMBOBOX, OnStandardcontrolsSkincombobox)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINLISTBOX, OnStandardcontrolsSkinlistbox)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINPOPUPMENU, OnStandardcontrolsSkinpopupmenu)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINSPIN, OnStandardcontrolsSkinspin)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINHEADER, OnStandardcontrolsSkinheader)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINPROGRESS, OnStandardcontrolsSkinprogress)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINSLIDER, OnStandardcontrolsSkinslider)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINTAB, OnStandardcontrolsSkintab)
	ON_COMMAND(ID_STANDARDCONTROLS_SKINSTATUSBAR, OnStandardcontrolsSkinstatusbar)
	ON_COMMAND(ID_EDIT_DELETE, OnEditDelete)
	ON_COMMAND(ID_OBJECTS_UNLOADIMAGE, &CSkinBuilderDoc::OnObjectsUnloadimage)
END_MESSAGE_MAP()


// CSkinBuilderDoc 构造/析构

CSkinBuilderDoc::CSkinBuilderDoc()
{
	// TODO: 在此添加一次性构造代码
	m_pCurrentImage = NULL;
	m_pCurrentObject = NULL;
	m_pSkin = NULL;
	m_pCurrentWinButton = NULL;
	m_nSelectedType = OBJECT_TYPE_SKINS;
	m_fZoom = 2;
	
	
}

CSkinBuilderDoc::~CSkinBuilderDoc()
{
}

BOOL CSkinBuilderDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	m_pSkin = &GetSkin();
	m_pCurrentObject = NULL;
	m_pCurrentImage = NULL;

	/*CMainFrame *pMain = (CMainFrame*)AfxGetMainWnd();
	m_pViewImage = pMain->m_pViewImage;
	m_pViewProperty = pMain->m_pViewProperty;
	m_pViewMain = pMain->m_pViewMain;*/
	// TODO: 在此添加重新初始化代码
	// (SDI 文档将重用该文档)

	return TRUE;
}

// CSknBuilderDoc 序列化

void CSkinBuilderDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}

// CSkinBuilderDoc 诊断

#ifdef _DEBUG
void CSkinBuilderDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSkinBuilderDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CSkinBuilderDoc 命令

BOOL CSkinBuilderDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	/*if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;*/

	// TODO:  Add your specialized creation code here
	GetSkin().LoadSkin(lpszPathName);
	m_nSelectedType = OBJECT_TYPE_SKINS;
	if( m_pSkin->m_pImages->m_arrayImages.GetCount() > 0)
		m_pCurrentImage = m_pSkin->m_pImages->m_arrayImages.GetAt(0);
	else
		m_pCurrentImage = NULL;

	if(m_pSkin->m_arraySkinObjects.GetCount()> 0)
		m_pCurrentObject = m_pSkin->m_arraySkinObjects.GetAt(0);
	else
		m_pCurrentObject = NULL;

	UpdateAllViews(NULL);
	return TRUE;
}

BOOL CSkinBuilderDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	GetSkin().SaveSkin(lpszPathName);
	UpdateAllViews(NULL);
	return TRUE;
	return CDocument::OnSaveDocument(lpszPathName);
}

void CSkinBuilderDoc::OnObjectsLoadimage()
{
	// TODO: Add your command handler code here
	CFileDialog dlg(TRUE,_T ("bmp"), _T ("*.bmp"),OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,_T("bitmap files (*.bmp)|*.bmp|All files (*.*)|*.*||"));
	dlg.DoModal();
	CString path = dlg.GetFileName();
	if(path.IsEmpty())
		return;

	m_pSkin->LoadImage(path);
	m_pCurrentImage = m_pSkin->FindIamge(path);
	m_nSelectedType = OBJECT_TYPE_IMAGE;
	UpdateAllViews(NULL);
	
}

void CSkinBuilderDoc::OnWindowFramewindow()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddWindowSkin(_T("MainFrame"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_WINDOW;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnWindowlessWinbuttonskin()
{
	// TODO: Add your command handler code here
	if(m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINDOW)
	{
		if(m_pCurrentObject->GetSkinType() == keWindowSkin)
		{
			CWindowSkin *pWin = (CWindowSkin*)m_pCurrentObject;
			int nCount = pWin->GetButtonCount();
			CString strName;
			strName.Format(_T("Button%d"),nCount);
			CWinButtonSkin *pWinButton = new CWinButtonSkin(strName,keClose);
			pWin->AddWinButton(pWinButton);
			UpdateAllViews(NULL);
		}
	}
}

void CSkinBuilderDoc::OnUpdateWindowlessWinbuttonskin(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	if(m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINDOW)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

void CSkinBuilderDoc::OnStandardcontrolsSkintoolbar()
{
	m_pCurrentObject = m_pSkin->AddToolBarSkin(_T("ToolBar"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_TOOLBAR;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinscrollbar()
{
	m_pCurrentObject = m_pSkin->AddScrollBarSkin(_T("ScrollBar"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_SCROLLBAR;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinbutton()
{
	m_pCurrentObject = m_pSkin->AddButtonSkin(_T("Button"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_BUTTON;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinradio()
{
	m_pCurrentObject = m_pSkin->AddRadioSkin(_T("Radio"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_RADIO;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinedit()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddEditSkin(_T("Edit"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_EDIT;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkincheckbox()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddCheckBoxSkin(_T("CheckBox"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_CHECK;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkingroupbox()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddGroupBoxSkin(_T("GroupBox"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_GROUP;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinstatic()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddStaticSkin(_T("Static"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_STATIC;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkincombobox()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddComboxSkin(_T("Combox"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_COMBOX;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinlistbox()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddListBoxSkin(_T("ListBox"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_LISTBOX;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinpopupmenu()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddPopupMenuSkin(_T("PopupMenu"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_POPUPMENU;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinspin()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddSpinSkin(_T("SpinSkin"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_SPIN;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinheader()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddHeaderSkin(_T("HeaderSkin"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_HEADER;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinprogress()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddProgressSkin(_T("ProgressSkin"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_PROGRESS;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinslider()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddSliderSkin(_T("SliderSkin"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_SLIDER;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkintab()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddTabSkin(_T("TabSkin"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_TAB;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnStandardcontrolsSkinstatusbar()
{
	// TODO: Add your command handler code here
	m_pCurrentObject = m_pSkin->AddStatusBarSkin(_T("StatusbarSkin"));
	m_nSelectedType = OBJECT_TYPE_SKINOBJECT_STATUSBAR;
	UpdateAllViews(NULL);
}

void CSkinBuilderDoc::OnEditDelete()
{
	// TODO: Add your command handler code here
	if(m_nSelectedType > OBJECT_TPPE_SKINOBJECTS && m_nSelectedType < OBJECT_TYPE_SKINOBJECT_MAX)
	{
		GetSkin().DeleteObjectSkin(m_pCurrentObject->GetName());
		m_nSelectedType = OBJECT_TPPE_SKINOBJECTS;
		m_pCurrentObject = NULL;
	}

	UpdateAllViews(NULL); 
}

void CSkinBuilderDoc::OnObjectsUnloadimage()
{
	// TODO: 在此添加命令处理程序代码
	CString strImagePath = m_pCurrentImage->m_strName;

	if(strImagePath.IsEmpty())
		return;

	m_pSkin->DeleteImage(strImagePath);
}
