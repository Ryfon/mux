﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(__SPPROPERTYGRIDINPLACELIST_H__)
#define __SPPROPERTYGRIDINPLACELIST_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// class forwards.

class CSPPropertyGridView;
class CSPPropertyGridItem;

//===========================================================================
// Summary:
//     CSPPropertyGridInplaceList is a CListBox derived class.
//     It is an internal class used by Property Grid control.
//===========================================================================
class  CSPPropertyGridInplaceList : public CListBox
{
	DECLARE_DYNAMIC(CSPPropertyGridInplaceList)

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridInplaceList object
	//-----------------------------------------------------------------------
	CSPPropertyGridInplaceList();

	//-----------------------------------------------------------------------
	// Summary:
	//     Destroys a CSPPropertyGridInplaceList object, handles cleanup
	//     and deallocation.
	//-----------------------------------------------------------------------
	virtual ~CSPPropertyGridInplaceList();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to create in-place list control.
	// Parameters:
	//     pItem - Parent item of the list.
	//     rect  -  Specifies the size and position of the item.
	//-----------------------------------------------------------------------
	virtual void Create(CSPPropertyGridItem* pItem, CRect rect);

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called when the parent item is destroyed.
	//-----------------------------------------------------------------------
	virtual void DestroyItem();

private:

	CSPPropertyGridItem* m_pItem;

protected:

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to cancel user changes.
	//-----------------------------------------------------------------------
	virtual void Cancel(void);

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this method to apply user changes.
	//-----------------------------------------------------------------------
	virtual void Apply(void);

protected:
//{{AFX_CODEJOCK_PRIVATE
	DECLARE_MESSAGE_MAP()
	//{{AFX_VIRTUAL(CSPPropertyGridInplaceList)
	void PostNcDestroy();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSPPropertyGridInplaceList)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
//}}AFX_CODEJOCK_PRIVATE

private:
	BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);

	friend class CSPPropertyGridItem;
};

//////////////////////////////////////////////////////////////////////////

AFX_INLINE BOOL CSPPropertyGridInplaceList::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) {
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}
AFX_INLINE BOOL CSPPropertyGridInplaceList::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID) {
	return CListBox::Create(dwStyle, rect, pParentWnd, nID);
}

#endif // #if !defined(__SPPROPERTYGRIDINPLACELIST_H__)
