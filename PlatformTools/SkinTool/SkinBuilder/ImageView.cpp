﻿// ImageView.cpp : implementation file
//

#include "stdafx.h"
#include "SkinBuilder.h"
#include "ImageView.h"
#include ".\imageview.h"

#define SKIN 0
#define IMAGES 1
#define WNDSKIN 2

// CImageView

IMPLEMENT_DYNCREATE(CImageView, CTreeView)

CImageView::CImageView()
{
	m_pImages = GetImages();
}

CImageView::~CImageView()
{
}

BEGIN_MESSAGE_MAP(CImageView, CTreeView)
	ON_WM_CREATE()
	ON_WM_CONTEXTMENU()
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnTvnSelchanged)
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()


// CImageView diagnostics

#ifdef _DEBUG
void CImageView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CImageView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif //_DEBUG


// CImageView message handlers

int CImageView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	lpCreateStruct->style |= TVS_HASLINES | TVS_SHOWSELALWAYS|TVS_HASBUTTONS|TVS_LINESATROOT;
	if (CTreeView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	m_imageList.Create(IDB_BITMAP_MAIN,16,42,RGB(255,0,255));
	GetTreeCtrl().SetImageList(&m_imageList,TVSIL_NORMAL );
	m_itemSkins = GetTreeCtrl().InsertItem(_T("skin"),0,0);

	m_itemImages = GetTreeCtrl().InsertItem(_T("images"),1,1,m_itemSkins);

	m_itemSkinObject = GetTreeCtrl().InsertItem(_T("SkinObject"),3,3,m_itemSkins);

	GetTreeCtrl().Expand(m_itemSkins,TVE_EXPAND );

	return 0;
}

void CImageView::OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/)
{
	// TODO: Add your message handler code here
}

void CImageView::OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	HTREEITEM hItem = GetTreeCtrl().GetSelectedItem();
	
	
	GetDocument()->m_nSelectedType = GetTreeCtrl().GetItemData(hItem);
	if(GetDocument()->m_nSelectedType == OBJECT_TYPE_IMAGE)
	{
		CString strName;
		strName = GetTreeCtrl().GetItemText(hItem);

		GetDocument()->m_pCurrentImage = GetDocument()->m_pSkin->FindIamge(strName);
	}

	if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINDOW)
	{
		CString strName;
		strName = GetTreeCtrl().GetItemText(hItem);

		GetDocument()->m_pCurrentObject = GetDocument()->m_pSkin->FindObjectSkin(strName);
	}
	else if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINBUTTON)
	{
		CString strName;
		strName = GetTreeCtrl().GetItemText(hItem);
		CWindowSkin *pWin = GetSkin().GetWindowSkin();
		GetDocument()->m_pCurrentObject = pWin;
		GetDocument()->m_pCurrentWinButton =pWin->FindButton(strName);
	}
	else
	{
		CString strName;
		strName = GetTreeCtrl().GetItemText(hItem);
		GetDocument()->m_pCurrentObject = GetDocument()->m_pSkin->FindObjectSkin(strName);
	}




	

	GetDocument()->UpdateAllViews(this);
	
	*pResult = 0;
}

void CImageView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	// TODO: Add your specialized code here and/or call the base class
	GetTreeCtrl().DeleteAllItems();
	m_itemSkins = GetTreeCtrl().InsertItem(_T("skin"),0,0);
	GetTreeCtrl().SetItemData(m_itemSkins,OBJECT_TYPE_SKINS);

	m_itemImages = GetTreeCtrl().InsertItem(_T("images"),1,1,m_itemSkins);
	GetTreeCtrl().SetItemData(m_itemImages,OBJECT_TYPE_IMAGES);

	m_itemSkinObject = GetTreeCtrl().InsertItem(_T("SkinObject"),3,3,m_itemSkins);
	GetTreeCtrl().SetItemData(m_itemSkinObject,OBJECT_TPPE_SKINOBJECTS);
	GetTreeCtrl().Expand(m_itemSkins,TVE_EXPAND );


	HTREEITEM hItem;
	for(int i = 0 ; i < m_pImages->m_arrayImages.GetCount();i++)
	{
		CSkinBitmap *pImage = m_pImages->m_arrayImages.GetAt(i);
		hItem = GetTreeCtrl().InsertItem(pImage->m_strName,9,9,m_itemImages);
		GetTreeCtrl().SetItemData(hItem,OBJECT_TYPE_IMAGE);
		GetTreeCtrl().Expand(m_itemImages,TVE_EXPAND );
		
	}
	for(int i = 0 ; i < GetSkin().m_arraySkinObjects.GetCount();i++)
	{
		CObjectSkin *pObject = GetSkin().m_arraySkinObjects.GetAt(i);
		if(pObject->GetSkinType() == keWindowSkin)
		{
			CWindowSkin *pWin = (CWindowSkin*)pObject;
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);

			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_WINDOW);

			for(int i = 0 ; i < pWin->GetChildrenCount() ;i++)
			{
				hItem = GetTreeCtrl().InsertItem(pWin->GetAt(i)->GetName(),3,3,hParent);
				GetTreeCtrl().SetItemData(hItem,OBJECT_TYPE_SKINOBJECT_WINBUTTON);
			}

			GetTreeCtrl().Expand(hParent,TVE_EXPAND);
		}
		else if(pObject->GetSkinType() == keScrollBarSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_SCROLLBAR);
		}
		else if(pObject->GetSkinType() == keButtonSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_BUTTON);
		}
		else if(pObject->GetSkinType() == keRadioSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_RADIO);
		}
		else if(pObject->GetSkinType() == keCheckBoxSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_CHECK);
		}
		else if(pObject->GetSkinType() == keGroupBoxSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_GROUP);
		}
		else if(pObject->GetSkinType() == keEditSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_EDIT);
		}
		else if(pObject->GetSkinType() == keStaticSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_STATIC);
		}
		else if(pObject->GetSkinType() == keComboxSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_COMBOX);
		}
		else if(pObject->GetSkinType() == keListBoxSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_LISTBOX);
		}
		else if(pObject->GetSkinType() == kePopMenuSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_POPUPMENU);
		}
		else if(pObject->GetSkinType() == keSpinSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_SPIN);
		}
		else if(pObject->GetSkinType() == keSliderSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_SLIDER);
		}
		else if(pObject->GetSkinType() == keProgressSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_PROGRESS);
		}
		else if(pObject->GetSkinType() == keHeaderSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_HEADER);
		}
		else if(pObject->GetSkinType() == keTabSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_TAB);
		}
		else if(pObject->GetSkinType() == keStatusbarSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_STATUSBAR);
		}
		else if(pObject->GetSkinType() == keToolbarSkin)
		{
			HTREEITEM hParent;
			hParent = GetTreeCtrl().InsertItem(pObject->GetName(),3,3,m_itemSkinObject);
			GetTreeCtrl().SetItemData(hParent,OBJECT_TYPE_SKINOBJECT_TOOLBAR);
		}
	}
	GetTreeCtrl().Expand(m_itemSkins,TVE_EXPAND);
	GetTreeCtrl().Expand(m_itemImages,TVE_EXPAND);
	GetTreeCtrl().Expand(m_itemSkinObject,TVE_EXPAND);
	Invalidate(TRUE);

}

void CImageView::OnInitialUpdate()
{
	CTreeView::OnInitialUpdate();

	m_pImages = GetImages();
}

void CImageView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CMenu menu;
	menu.LoadMenu(IDR_MAINFRAME);
	CMenu *pSub = menu.GetSubMenu(3);
	if(GetDocument()->m_nSelectedType == OBJECT_TYPE_SKINOBJECT_WINDOW)
	{
		CMenu *pMenu = pSub->GetSubMenu(1);
		pMenu->EnableMenuItem(ID_WINDOWLESS_WINBUTTONSKIN,MF_BYCOMMAND | MF_ENABLED );
	}
	else
	{
		CMenu *pMenu = pSub->GetSubMenu(1);
		pMenu->EnableMenuItem(ID_WINDOWLESS_WINBUTTONSKIN,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
	}

	CMenu *pMenu = pSub->GetSubMenu(1);
	pMenu->EnableMenuItem(ID_WINDOWLESS_WINSTATICSKIN,MF_BYCOMMAND | MF_DISABLED |MF_GRAYED);

	CPoint pt;
	GetCursorPos(&pt);

	pSub->TrackPopupMenuEx(TPM_RIGHTBUTTON|TPM_LEFTALIGN,pt.x,pt.y,this,NULL);

	CTreeView::OnRButtonDown(nFlags, point);
}
