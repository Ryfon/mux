﻿// SkinBuilderView.h : CSkinBuilderView 类的接口
//


#pragma once
#include "SkinBuilderDoc.h"

class CSkinBuilderView : public CView
{
protected: // 仅从序列化创建
	CSkinBuilderView();
	DECLARE_DYNCREATE(CSkinBuilderView)
	virtual ~CSkinBuilderView();

// 属性
public:
	CSkinButton m_skinCheck;
	CSkinButton m_skinPush;
	CSkinButton m_skinRadio;
	CSkinButton m_skinGroup;
	CSkinStatic m_skinStatic;
	CSkinEdit	m_skinEdit;
	CSkinScrollBar m_skinScrollBar;
	CSkinCombox	m_skinCombox;
	CSkinSpin m_skinSpin;
	CSkinListBox m_skinListBox; 
	CSkinHeader m_skinHeader; 
	CSkinSlider m_skinSlider;
	CSkinTabCtrl m_skinTab;
	CSkinStatusBar m_skinStatusbar;
	CSkinToolBar	m_skinToolbar;
	
	CButton m_push;
	CButton m_radio;
	CButton m_group;
	CButton m_check;
	CStatic m_static;
	CEdit	m_edit;
	CScrollBar	m_scrollbar;
	CComboBox	m_combox;
	CListBox	m_listbox;
	CSpinButtonCtrl m_spin;
	CSliderCtrl m_slider;
	CProgressCtrl m_progress;
	CHeaderCtrl m_header;
	CTabCtrl	m_tab;
	CStatusBar  m_statusbar;

	CToolBar m_toolbar;

	CRectTracker m_trackerPos;
	CRect m_rtPos;
	CPoint m_ptTopLeft;
	CSkinBuilderDoc* GetDocument() const;

public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图

protected:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);

// 实现
public:
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};

#ifndef _DEBUG  // SkinBuilderView.cpp 的调试版本
inline CSkinBuilderDoc* CSkinBuilderView::GetDocument() const
   { return reinterpret_cast<CSkinBuilderDoc*>(m_pDocument); }
#endif

