﻿#pragma once


// CImageView view
#include "SkinBuilderDoc.h"

class CImageView : public CTreeView
{
	DECLARE_DYNCREATE(CImageView)

protected:
	CImageView();           // protected constructor used by dynamic creation
	virtual ~CImageView();

public:
	CImageList	m_imageList;
	HTREEITEM	m_itemSkins;
	HTREEITEM	m_itemImages;
	HTREEITEM   m_itemSkinObject;

	CSkinImages *m_pImages;
	CSkinBuilderDoc* GetDocument() const;

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
	afx_msg void OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void OnInitialUpdate();
public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};
inline CSkinBuilderDoc* CImageView::GetDocument() const
{ return reinterpret_cast<CSkinBuilderDoc*>(m_pDocument); }

