﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#include "StdAfx.h"
#include "SPVC80Helpers.h"

#include "SPPropertyGridInplaceEdit.h"
#include "SPPropertyGridInplaceButton.h"
#include "SPPropertyGridInplaceList.h"
#include "SPPropertyGridItem.h"
#include "SPPropertyGriditembool.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemBool

IMPLEMENT_DYNAMIC(CSPPropertyGridItemBool, CSPPropertyGridItem)

CSPPropertyGridItemBool::CSPPropertyGridItemBool(CString strCaption, BOOL bValue, BOOL* pBindBool)
: CSPPropertyGridItem(strCaption)
, m_strTrueText(_T("True"))
, m_strFalseText(_T("False"))
{
	m_pBindBool = pBindBool;
	_Init(bValue);

}

CSPPropertyGridItemBool::CSPPropertyGridItemBool(UINT nID, BOOL bValue, BOOL* pBindBool)
: CSPPropertyGridItem(nID)
, m_strTrueText(_T("True"))
, m_strFalseText(_T("False"))
{
	m_pBindBool = pBindBool;
	_Init(bValue);
}

CSPPropertyGridItemBool::~CSPPropertyGridItemBool()
{
}

/////////////////////////////////////////////////////////////////////////////
//

void CSPPropertyGridItemBool::_Init(BOOL bValue)
{
	SetBool(bValue);
	m_nFlags = SPGridItemHasComboButton | SPGridItemHasEdit;

	m_pConstraints->AddConstraint(m_strTrueText);
	m_pConstraints->AddConstraint(m_strFalseText);

	SetConstraintEdit(TRUE);
}

void CSPPropertyGridItemBool::SetValue(CString strValue)
{
	SetBool(strValue.CompareNoCase(m_strTrueText) == 0);
}

void CSPPropertyGridItemBool::SetBool(BOOL bValue)
{
	m_bValue = bValue;

	if (m_pBindBool)
	{
		*m_pBindBool = bValue;
	}

	CSPPropertyGridItem::SetValue(bValue ? m_strTrueText: m_strFalseText);
}

void CSPPropertyGridItemBool::BindToBool(BOOL* pBindBool)
{
	m_pBindBool = pBindBool;
	if (m_pBindBool)
	{
		*m_pBindBool = m_bValue;
	}
}

void CSPPropertyGridItemBool::OnBeforeInsert()
{
	if( m_pBindBool && *m_pBindBool != m_bValue)
	{
		SetBool(*m_pBindBool);
	}
}


BOOL CSPPropertyGridItemBool::SetValueText(CString& strValueText, CString strNewText)
{
	// see if the value exists.
	int iIndex = m_pConstraints->FindConstraint(strValueText);
	if (iIndex != -1)
	{
		// if this is the current value change it as well.
		if (GetValue() == strValueText) {
			CSPPropertyGridItem::SetValue(strNewText);
		}

		// update the value.
		strValueText  = strNewText;
		m_pConstraints->GetConstraintAt(iIndex)->m_strConstraint = strValueText;
		return TRUE;
	}
	return FALSE;
}

BOOL CSPPropertyGridItemBool::SetTrueFalseText(CString strTrueText, CString strFalseText)
{
	// update the "True" value text
	if (!SetValueText(m_strTrueText, strTrueText))
		return FALSE;

	// update the "False" value text
	if (!SetValueText(m_strFalseText, strFalseText))
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemEnum

IMPLEMENT_DYNAMIC(CSPPropertyGridItemEnum, CSPPropertyGridItem)

CSPPropertyGridItemEnum::CSPPropertyGridItemEnum(CString strCaption, int nValue, int* pBindEnum)
: CSPPropertyGridItem(strCaption)
{
	m_pBindEnum = pBindEnum;
	_Init(nValue);

}

CSPPropertyGridItemEnum::CSPPropertyGridItemEnum(UINT nID, int nValue, int* pBindEnum)
: CSPPropertyGridItem(nID)
{
	m_pBindEnum = pBindEnum;
	_Init(nValue);
}

CSPPropertyGridItemEnum::~CSPPropertyGridItemEnum()
{
}

void CSPPropertyGridItemEnum::_Init(int nValue)
{
	SetEnum(nValue);
	m_nFlags = SPGridItemHasComboButton | SPGridItemHasEdit;

	SetConstraintEdit(TRUE);
}

void CSPPropertyGridItemEnum::SetValue(CString strValue)
{
	int nIndex = m_pConstraints->FindConstraint(strValue);
	ASSERT(nIndex >= 0);
	if (nIndex >= 0)
	{
		SetEnum(m_pConstraints->GetConstraintAt(nIndex));
	}
}

void CSPPropertyGridItemEnum::SetEnum(int nValue)
{
	m_nValue = nValue;

	if (m_pBindEnum)
	{
		*m_pBindEnum = nValue;
	}

	int nIndex = m_pConstraints->FindConstraint(nValue);
	CSPPropertyGridItem::SetValue(m_pConstraints->GetAt(nIndex));
}

void CSPPropertyGridItemEnum::SetEnum(CSPPropertyGridItemConstraint* pContraint)
{
	m_nValue = (int)pContraint->m_dwData;

	if (m_pBindEnum)
	{
		*m_pBindEnum = m_nValue;
	}

	CSPPropertyGridItem::SetValue(pContraint->m_strConstraint);
}

void CSPPropertyGridItemEnum::BindToEnum(int* pBindEnum)
{
	m_pBindEnum = pBindEnum;
	if (m_pBindEnum)
	{
		*m_pBindEnum = m_nValue;
	}
}

void CSPPropertyGridItemEnum::OnBeforeInsert()
{
	if( m_pBindEnum && *m_pBindEnum != m_nValue)
	{
		SetEnum(*m_pBindEnum);
	}
}

void CSPPropertyGridItemEnum::OnConstraintsChanged()
{
	if (m_strValue.IsEmpty())
	{
		int nIndex = m_pConstraints->FindConstraint(m_nValue);
		if (nIndex != -1)
			m_strValue = m_pConstraints->GetAt(nIndex);
	}

}



/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridItemFlags

class CSPPropertyGridItemFlags::CSPPropertyGridItemFlag : public CSPPropertyGridItemBool
{
public:

	CSPPropertyGridItemFlag(CString strCaption, DWORD dwFlag)
		:  CSPPropertyGridItemBool(strCaption), m_dwFlag(dwFlag)
	{
	}

	void OnValueChanged(CString strValue);

	DWORD m_dwFlag;
};

void CSPPropertyGridItemFlags::CSPPropertyGridItemFlag::OnValueChanged(CString strValue)
{
	SetValue(strValue);

	CSPPropertyGridItemFlags* pParent = DYNAMIC_DOWNCAST(CSPPropertyGridItemFlags, m_pParent);
	ASSERT(pParent);

	if (GetBool())
		pParent->m_nValue |= m_dwFlag; else pParent->m_nValue &= ~m_dwFlag;

	pParent->OnValueChanged(pParent->GetFlagsString());

}

IMPLEMENT_DYNAMIC(CSPPropertyGridItemFlags, CSPPropertyGridItem)

CSPPropertyGridItemFlags::CSPPropertyGridItemFlags(CString strCaption, int nValue, int* pBindFlags)
: CSPPropertyGridItem(strCaption)
{
	m_pBindFlags = pBindFlags;
	_Init(nValue);

}

CSPPropertyGridItemFlags::CSPPropertyGridItemFlags(UINT nID, int nValue, int* pBindFlags)
: CSPPropertyGridItem(nID)
{
	m_pBindFlags = pBindFlags;
	_Init(nValue);
}

CSPPropertyGridItemFlags::~CSPPropertyGridItemFlags()
{
}

void CSPPropertyGridItemFlags::_Init(int nValue)
{
	SetFlags(nValue);
	m_nFlags = SPGridItemHasEdit;
}

AFX_INLINE BOOL HasFlag(CString strValue, CString strFlag)
{
	strFlag.MakeLower();
	int nIndex = strValue.Find(strFlag);
	if (nIndex == -1)
		return FALSE;

	TCHAR chLast = nIndex + strFlag.GetLength() == strValue.GetLength()? _T(' '): strValue[strFlag.GetLength() + nIndex];
	TCHAR chFirst = nIndex == 0? _T(' '): strValue[nIndex - 1];

	return (chLast == ' ' || chLast == ',' || chLast == ';' || chLast == ']')
		&& (chFirst == ' ' || chFirst == ',' || chFirst == ';' || chFirst == '[');
}

void CSPPropertyGridItemFlags::SetValue(CString strValue)
{
	int nValue = 0;
	strValue.MakeLower();

	CSPPropertyGridItemConstraints* pConstraints = GetConstraints();
	for (int i = 0; i < pConstraints->GetCount(); i++)
	{
		if (HasFlag(strValue, pConstraints->GetAt(i)))
			nValue |= pConstraints->GetConstraintAt(i)->m_dwData;
	}

	SetFlags(nValue);
}

void CSPPropertyGridItemFlags::SetFlags(int nValue)
{
	m_nValue = nValue;

	if (m_pBindFlags)
	{
		*m_pBindFlags = nValue;
	}

	UpdateChilds();

	CSPPropertyGridItem::SetValue(GetFlagsString());
}

void CSPPropertyGridItemFlags::BindToFlags(int* pBindFlags)
{
	m_pBindFlags = pBindFlags;
	if (m_pBindFlags)
	{
		*m_pBindFlags = m_nValue;
	}
}

CString CSPPropertyGridItemFlags::GetFlagsString()
{
	CString str;

	CSPPropertyGridItemConstraints* pConstraints = GetConstraints();
	int nValue = 0;

	for (int i = 0; i < pConstraints->GetCount(); i++)
	{
		CSPPropertyGridItemConstraint* pConstraint =  pConstraints->GetConstraintAt(i);

		if ((nValue & pConstraint->m_dwData) == pConstraint->m_dwData)
			continue;

		if ((m_nValue & pConstraint->m_dwData) == pConstraint->m_dwData)
		{
			str += (str.IsEmpty()? _T(""): _T(";")) + pConstraint->m_strConstraint;
			nValue |= pConstraint->m_dwData;
		}
	}

	return  _T("[") + str + _T("]");
}

void CSPPropertyGridItemFlags::UpdateChilds()
{
	CSPPropertyGridItems* pItems = GetChilds();
	for (int i = 0; i < pItems->GetCount(); i++)
	{
		CSPPropertyGridItemFlag* pItem = (CSPPropertyGridItemFlag*)pItems->GetAt(i);
		pItem->SetBool((m_nValue & pItem->m_dwFlag) == pItem->m_dwFlag);
	}
}


void CSPPropertyGridItemFlags::OnBeforeInsert()
{
	if( m_pBindFlags && *m_pBindFlags != m_nValue)
	{
		SetFlags(*m_pBindFlags);
	}
}

void CSPPropertyGridItemFlags::OnConstraintsChanged()
{
	GetChilds()->Clear();

	CSPPropertyGridItemConstraints* pConstraints = GetConstraints();

	for (int i = 0; i < pConstraints->GetCount(); i++)
	{
		AddChildItem(new CSPPropertyGridItemFlag(pConstraints->GetAt(i), (int)pConstraints->GetConstraintAt(i)->m_dwData));
	}
	UpdateChilds();
	m_strValue = GetFlagsString();
}

