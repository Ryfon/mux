﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#if !defined(_SPPROPERTYGRIDPAINTMANAGER_H__)
#define _SPPROPERTYGRIDPAINTMANAGER_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SPWinThemeWrapper.h"

class CSPPropertyGrid;
class CSPPropertyGridInplaceButton;
class CSPPropertyGridItem;

//-----------------------------------------------------------------------
// Summary:
//     Visual theme enumeration
// Example:
//     <code> m_wndPropertyGrid.SetTheme(SPGridThemeWhidbey); </code>
// See Also: CSPPropertyGrid::SetTheme, CSPPropertyGrid::GetCurrentTheme
//
// <KEYWORDS SPGridThemeDefault, SPGridThemeNativeWinXP, SPGridThemeOffice2003, SPGridThemeCool, SPGridThemeSimple, SPGridThemeDelphi, SPGridThemeWhidbey>
//-----------------------------------------------------------------------
enum SPPropertyGridPaintTheme
{
	SPGridThemeDefault,     // Default theme.
	SPGridThemeNativeWinXP, // WinXP style theme.
	SPGridThemeOffice2003,  // Office 2003 style theme.
	SPGridThemeCool,        // Cool theme.
	SPGridThemeSimple,      // Visual Basic style theme.
	SPGridThemeDelphi,      // Delphi style theme.
	SPGridThemeWhidbey      // Visual Studio 2005 "Whidbey" style theme.
};


//===========================================================================
// Summary:
//     Standalone class used by CSPPropertyGrid as the paint manager.
//===========================================================================
class  CSPPropertyGridPaintManager
{
public:

	//-----------------------------------------------------------------------
	// Summary:
	//     Constructs a CSPPropertyGridPaintManager object.
	// Parameters:
	//     pGrid - Points to a CSPPropertyGrid object
	//-----------------------------------------------------------------------
	CSPPropertyGridPaintManager(CSPPropertyGrid* pGrid);

	//-----------------------------------------------------------------------
	// Summary:
	//     Destroys a CSPPropertyGridPaintManager object, handles cleanup
	//     and deallocation.
	//-----------------------------------------------------------------------
	virtual ~CSPPropertyGridPaintManager();

public:

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called to draw property grid background.
	// Parameters:
	//     pDC - Pointer to a valid device context
	//-----------------------------------------------------------------------
	virtual void FillPropertyGrid(CDC* pDC);

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called to draw view background.
	// Parameters:
	//     pDC - Pointer to a valid device context
	//     bClient - TRUE to draw client area; FALSE to draw frame.
	//-----------------------------------------------------------------------
	virtual void FillPropertyGridView(CDC* pDC, BOOL bClient);

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called to draw single item of the grid.
	// Parameters:
	//     lpDrawItemStruct - A long pointer to a DRAWITEMSTRUCT structure
	//     that contains information about the type of drawing required.
	//-----------------------------------------------------------------------
	virtual void DrawItem(PDRAWITEMSTRUCT lpDrawItemStruct);

	//-----------------------------------------------------------------------
	// Summary:
	//     Override this method and fill in the MEASUREITEMSTRUCT structure
	//     to inform Windows of the list-box dimensions.
	// Parameters:
	//     lpMeasureItemStruct - Specifies a long pointer to a MEASUREITEMSTRUCT
	//     structure.
	//-----------------------------------------------------------------------
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called to draw in-place button of the grid.
	// Parameters:
	//     pDC - Pointer to a valid device context
	//     pButton - Points to a CSPPropertyGridInplaceButton object
	//-----------------------------------------------------------------------
	virtual void FillInplaceButton(CDC* pDC, CSPPropertyGridInplaceButton* pButton);

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called to refresh the visual metrics of manager.
	//-----------------------------------------------------------------------
	virtual void RefreshMetrics();

	//-----------------------------------------------------------------------
	// Summary:
	//     Call this member to determine if a verb is at a specific point.
	// Parameters:
	//     pDC     - Reference to a valid device context
	//     rcVerbs - Bounding rectangle of all verbs.
	//     pt      - Point to test.
	// Returns:
	//     Index of verb within collection if the point was on a verb,
	//     otherwise -1.
	//-----------------------------------------------------------------------
	int HitTestVerbs(CDC* pDC, CRect rcVerbs, CPoint pt);

protected:

	//-----------------------------------------------------------------------
	// Summary:
	//     This method is called to draw expand buttons of the grid.
	// Parameters:
	//     dc - Reference to a valid device context
	//     pItem - Points to a CSPPropertyGridItem object
	//     rcCaption - Caption button rectangle.
	//-----------------------------------------------------------------------
	virtual void DrawExpandButton(CDC& dc, CSPPropertyGridItem* pItem, CRect rcCaption);

	//-----------------------------------------------------------------------
	// Summary:
	//     This member is called to draw the bounding rectangle for a
	//     category caption.
	// Parameters:
	//     pDC - Reference to a valid device context
	//     rc  - Bounding rectangle of Categorys caption.
	//-----------------------------------------------------------------------
	virtual void DrawCategoryCaptionBackground(CDC* pDC, CRect rc);

protected:
	CSPPropertyGrid* m_pGrid;                      // Parent grid class.
	BOOL m_bThemed;                                 // TRUE to use WinXP themes.
	CFont m_fntVerbUnderline;                       // Font used to display the caption of verbs with underline
	CFont m_fntVerbNormal;                          // Font used to display the caption of verbs without underline


	CSPWinThemeWrapper m_themeTree;                // Themes for Tree controls.
	CSPWinThemeWrapper m_themeButton;              // Themes for button controls
	CSPWinThemeWrapper m_themeCombo;               // Themes for combo button.

	COLORREF m_clrFace;                             // Face color.
	COLORREF m_clrShadow;                           // Shadow color.
	COLORREF m_clrVerbFace;                         // Text color of verbs.
};

namespace SPPropertyGridPaintThemes
{

	//===========================================================================
	// Summary:
	//     CSPPropertyGridOffice2003Theme is a CSPPropertyGridPaintManager derived
	//     class.  This represents an Office 2003 style theme.
	//===========================================================================
	class  CSPPropertyGridOffice2003Theme : public CSPPropertyGridPaintManager
	{
	public:

		//-----------------------------------------------------------------------
		// Summary:
		//     Constructs a CSPPropertyGridOffice2003Theme object.
		// Parameters:
		//     pGrid - Points to a CSPPropertyGrid object
		//-----------------------------------------------------------------------
		CSPPropertyGridOffice2003Theme(CSPPropertyGrid* pGrid);

		//-------------------------------------------------------------------------
		// Summary:
		//     This method is called to refresh the visual metrics of the
		//     property grid.
		//-------------------------------------------------------------------------
		virtual void RefreshMetrics();


	protected:
		BOOL m_bLunaTheme;  // TRUE to use luna colors, FALSE otherwise
	};

	//===========================================================================
	// Summary:
	//     CSPPropertyGridNativeXPTheme is a CSPPropertyGridPaintManager derived
	//     class.  This represents a Native Windows XP style theme.
	//===========================================================================
	class  CSPPropertyGridNativeXPTheme : public CSPPropertyGridPaintManager
	{
	public:

		//-----------------------------------------------------------------------
		// Summary:
		//     Constructs a CSPPropertyGridNativeXPTheme object.
		// Parameters:
		//     pGrid - Points to a CSPPropertyGrid object
		//-----------------------------------------------------------------------
		CSPPropertyGridNativeXPTheme(CSPPropertyGrid* pGrid);

		//-------------------------------------------------------------------------
		// Summary:
		//     This method is called to refresh the visual metrics of the
		//     property grid.
		//-------------------------------------------------------------------------
		virtual void RefreshMetrics();
	};


	//===========================================================================
	// Summary:
	//     CSPPropertyGridCoolTheme is a CSPPropertyGridPaintManager derived
	//     class.  This represents a Cool style theme.
	//===========================================================================
	class  CSPPropertyGridCoolTheme : public CSPPropertyGridPaintManager
	{
	public:
		//-----------------------------------------------------------------------
		// Summary:
		//     Constructs a CSPPropertyGridCoolTheme object.
		// Parameters:
		//     pGrid - Points to a CSPPropertyGrid object
		//-----------------------------------------------------------------------
		CSPPropertyGridCoolTheme(CSPPropertyGrid* pGrid);

		//-----------------------------------------------------------------------
		// Summary:
		//     This method is called to draw single item of the grid.
		// Parameters:
		//     lpDrawItemStruct - A long pointer to a DRAWITEMSTRUCT structure
		//     that contains information about the type of drawing required.
		//-----------------------------------------------------------------------
		virtual void DrawItem(PDRAWITEMSTRUCT lpDrawItemStruct);

		//-------------------------------------------------------------------------
		// Summary:
		//     This method is called to refresh the visual metrics of the
		//     property grid.
		//-------------------------------------------------------------------------
		void RefreshMetrics();
	};


	//===========================================================================
	// Summary:
	//     CSPPropertyGridSimpleTheme is a CSPPropertyGridPaintManager derived
	//     class.  This represents a Simple style theme.
	//===========================================================================
	class  CSPPropertyGridSimpleTheme : public CSPPropertyGridPaintManager
	{
	public:
		//-----------------------------------------------------------------------
		// Summary:
		//     Constructs a CSPPropertyGridSimpleTheme object.
		// Parameters:
		//     pGrid - Points to a CSPPropertyGrid object
		//-----------------------------------------------------------------------
		CSPPropertyGridSimpleTheme(CSPPropertyGrid* pGrid);

		//-----------------------------------------------------------------------
		// Summary:
		//     This method is called to draw single item of the grid.
		// Parameters:
		//     lpDrawItemStruct - A long pointer to a DRAWITEMSTRUCT structure
		//     that contains information about the type of drawing required.
		//-----------------------------------------------------------------------
		virtual void DrawItem(PDRAWITEMSTRUCT lpDrawItemStruct);

		//-------------------------------------------------------------------------
		// Summary:
		//     This method is called to refresh the visual metrics of the
		//     property grid.
		//-------------------------------------------------------------------------
		void RefreshMetrics();
	};


	//===========================================================================
	// Summary:
	//     CSPPropertyGridDelphiTheme is a CSPPropertyGridPaintManager derived
	//     class.  This represents a Delphi style theme.
	//===========================================================================
	class  CSPPropertyGridDelphiTheme : public CSPPropertyGridPaintManager
	{
	public:
		//-----------------------------------------------------------------------
		// Summary:
		//     Constructs a CSPPropertyGridDelphiTheme object.
		// Parameters:
		//     pGrid - Points to a CSPPropertyGrid object
		//-----------------------------------------------------------------------
		CSPPropertyGridDelphiTheme(CSPPropertyGrid* pGrid);

		//-----------------------------------------------------------------------
		// Summary:
		//     This method is called to draw single item of the grid.
		// Parameters:
		//     lpDrawItemStruct - A long pointer to a DRAWITEMSTRUCT structure
		//     that contains information about the type of drawing required.
		//-----------------------------------------------------------------------
		virtual void DrawItem(PDRAWITEMSTRUCT lpDrawItemStruct);

		//-------------------------------------------------------------------------
		// Summary:
		//     This method is called to refresh the visual metrics of the
		//     property grid.
		//-------------------------------------------------------------------------
		virtual void RefreshMetrics();

	};


	//===========================================================================
	// Summary:
	//     CSPPropertyGridWhidbeyTheme is a CSPPropertyGridPaintManager derived
	//     class.  This represents a Visual Studio 2005 "Whidbey" style theme.
	//===========================================================================
	class  CSPPropertyGridWhidbeyTheme : public CSPPropertyGridPaintManager
	{
	public:

		//-----------------------------------------------------------------------
		// Summary:
		//     Constructs a CSPPropertyGridWhidbeyTheme object.
		// Parameters:
		//     pGrid - Points to a CSPPropertyGrid object
		//-----------------------------------------------------------------------
		CSPPropertyGridWhidbeyTheme(CSPPropertyGrid* pGrid);

	protected:

		//-----------------------------------------------------------------------
		// Summary:
		//     This member is called to draw the bounding rectangle for a
		//     category caption.
		// Parameters:
		//     pDC - Reference to a valid device context
		//     rc  - Bounding rectangle of Categorys caption.
		//-----------------------------------------------------------------------
		void DrawCategoryCaptionBackground(CDC* pDC, CRect rc);

		//-------------------------------------------------------------------------
		// Summary:
		//     This method is called to refresh the visual metrics of the
		//     property grid.
		//-------------------------------------------------------------------------
		void RefreshMetrics();

		//-----------------------------------------------------------------------
		// Summary:
		//     This method is called to draw expand buttons of the grid.
		// Parameters:
		//     dc        - Reference to a valid device context
		//     pItem     - Points to a CSPPropertyGridItem object
		//     rcCaption - Caption button rectangle.
		//-----------------------------------------------------------------------
		void DrawExpandButton(CDC& dc, CSPPropertyGridItem* pItem, CRect rcCaption);
	};

}
using namespace SPPropertyGridPaintThemes;


#endif // !defined(_SPPROPERTYGRIDPAINTMANAGER_H__)
