﻿/********************************************
** 工作室：S&P工作室
** 作者	 ：张东斌
** 日期  ：2007年6月
*********************************************/
#include "stdafx.h"
#include "SPVC80Helpers.h"
#include "SPDrawHelpers.h"

#include "SPPropertyGridInplaceEdit.h"
#include "SPPropertyGridInplaceButton.h"
#include "SPPropertyGridInplaceList.h"
#include "SPPropertyGridItem.h"
#include "SPPropertyGridInplaceButton.h"
#include "SPPropertyGridInplaceEdit.h"
#include "SPPropertyGrid.h"
#include "SPPropertyGridPaintManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CXTColorPicker

CSPPropertyGridInplaceButton::CSPPropertyGridInplaceButton()
: m_bPressed(FALSE)
, m_bOver(FALSE)
, m_pGrid(0)
, m_pItem(0)
{

}

CSPPropertyGridInplaceButton::~CSPPropertyGridInplaceButton()
{

}

IMPLEMENT_DYNAMIC(CSPPropertyGridInplaceButton, CStatic)

BEGIN_MESSAGE_MAP(CSPPropertyGridInplaceButton, CStatic)
	//{{AFX_MSG_MAP(CSPPropertyGridInplaceButton)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_CAPTURECHANGED()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()
	ON_WM_GETDLGCODE()
	ON_MESSAGE_VOID(WM_MOUSELEAVE, OnMouseLeave)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

UINT CSPPropertyGridInplaceButton::OnGetDlgCode()
{
	return DLGC_WANTALLKEYS;
}


void CSPPropertyGridInplaceButton::HideWindow()
{
	if (m_hWnd)
	{
		ShowWindow(SW_HIDE);
	}
}

void CSPPropertyGridInplaceButton::Create(CSPPropertyGridItem* pItem, CRect rect)
{
	ASSERT(pItem && pItem->GetGrid());
	
	int nWidth = GetSystemMetrics(SM_CXHTHUMB);

	m_pGrid      = pItem->GetGrid();
	rect.left    = rect.right - nWidth;
	rect.top -= 1;

	

	if (!m_hWnd)
	{
		CStatic::Create(_T(""), SS_NOTIFY|WS_CHILD, rect, m_pGrid);
	}

	SetWindowPos(0, rect.left, rect.top, rect.Width(), rect.Height(), SWP_NOZORDER|SWP_SHOWWINDOW);
	m_pItem = pItem;
}

/////////////////////////////////////////////////////////////////////////////
// CSPPropertyGridInplaceButton message handlers

void CSPPropertyGridInplaceButton::OnPaint()
{
	CPaintDC dc(this);

	CSPPropertyGridView* pView = (CSPPropertyGridView*)m_pGrid;
	if (pView)
	{
		pView->m_pGrid->GetPaintManager()->FillInplaceButton(&dc, this);
	}
}

void CSPPropertyGridInplaceButton::OnLButtonDown(UINT, CPoint)
{
	m_bOver = m_bPressed = TRUE;
	Invalidate(FALSE);
	SetCapture();
}

void CSPPropertyGridInplaceButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (m_bPressed)
	{
		m_bPressed = FALSE;
		Invalidate(FALSE);
		ReleaseCapture();
		if (m_bOver)
			m_pItem->OnInplaceButtonDown();
	}
	CStatic::OnLButtonUp(nFlags, point);
}

void CSPPropertyGridInplaceButton::OnMouseMove(UINT nFlags, CPoint point)
{
	CSPClientRect rect(this);
	if ((rect.PtInRect(point) && !m_bOver) ||
		(!rect.PtInRect(point) && m_bOver))
	{
		m_bOver = rect.PtInRect(point);
		Invalidate(FALSE);

		if (m_bOver)
		{
			TRACKMOUSEEVENT tme = { sizeof(TRACKMOUSEEVENT), TME_LEAVE, m_hWnd};
			_TrackMouseEvent(&tme);
		}
	}

	CStatic::OnMouseMove(nFlags, point);
}

void CSPPropertyGridInplaceButton::OnMouseLeave()
{
	//reset mouse vars
	OnMouseMove(0,CPoint(-1, -1));
}


void CSPPropertyGridInplaceButton::OnCaptureChanged(CWnd *pWnd)
{
	m_bPressed = FALSE;
	Invalidate(FALSE);

	CStatic::OnCaptureChanged(pWnd);
}

void CSPPropertyGridInplaceButton::DestroyItem()
{
	// reset variables to defaults.
	m_bPressed = FALSE;
	m_bOver    = FALSE;
	m_pGrid    = NULL;
	m_pItem    = NULL;

	// destroy the window.
	//DestroyWindow( );
	if (::IsWindow(m_hWnd)) ShowWindow(SW_HIDE);
}

void CSPPropertyGridInplaceButton::OnSetFocus(CWnd* pOldWnd)
{
	CStatic::OnSetFocus(pOldWnd);
	Invalidate(FALSE);

#ifdef SP_SITENOTIFY_ONFOCUS
	SP_SITENOTIFY_ONFOCUS(this, m_pGrid, TRUE)
#endif

}

void CSPPropertyGridInplaceButton::OnKillFocus(CWnd* pNewWnd)
{
	CStatic::OnKillFocus(pNewWnd);
	if (m_pGrid)
		m_pGrid->Invalidate(FALSE);
	Invalidate(FALSE);

#ifdef SP_SITENOTIFY_ONFOCUS
	SP_SITENOTIFY_ONFOCUS(this, m_pGrid, FALSE)
#endif
}

void CSPPropertyGridInplaceButton::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (m_pItem && (nChar == VK_SPACE || nChar == VK_DOWN || nChar == VK_RETURN))
	{
		m_pItem->OnInplaceButtonDown();
		return;
	}
	if (nChar == VK_TAB && m_pGrid)
	{
		((CSPPropertyGrid*)m_pGrid->GetParent())->OnNavigate(
			SPGridUIInplaceButton, GetKeyState(VK_SHIFT) >= 0, m_pItem);
		return;
	}

	CStatic::OnKeyDown(nChar, nRepCnt, nFlags);
}
