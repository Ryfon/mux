﻿#pragma once


// CImageEditWnd

class CImageEditWnd : public CWnd
{
	DECLARE_DYNAMIC(CImageEditWnd)

public:
	CImageEditWnd();
	virtual ~CImageEditWnd();
	virtual BOOL Create(const RECT& rect, CWnd* pParentWnd);
	void	SetScrool();

	int m_nViewWidth;   // Workspace width in pixels
	int m_nViewHeight;  // Workspace height in pixels
	int m_nHScrollPos;  // Horizontal scroll position
	int m_nVScrollPos;  // Vertical scroll position
	int m_nHPageSize;   // Horizontal page size
	int m_nVPageSize;   // Vertical page size

	CPoint m_ptLeftTop;

	BOOL m_bSection;

	SkinImageRect *m_pImageRect;
	SkinImageSection *m_pImageSection;
	CSkinBitmap	*m_pCurrentBitmap;
	float	m_fZoom;
	CRectTracker m_tracker;
	CRect m_rtTracker;
	CRect m_rtMargins;
protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};


