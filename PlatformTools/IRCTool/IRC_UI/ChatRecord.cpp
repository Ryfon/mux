﻿// ChatRecord.cpp : 实现文件
//

#include "stdafx.h"
#include <afxole.h>
#include <afxodlgs.h>
#include <afxpriv.h>
#include <Richole.h>
#include <atltime.h>

#include "IRC_UI.h"
#include "ChatRecord.h"
#include "IRC_UIDlg.h"
#include "UserRelation.h"
#include "FriendDlg.h"
#include "FoeDlg.h"
#include "BlackItemDlg.h"
#include "Util.h"
#include "Helper.h"

//#define PERPAGE_RECORDS 20
// CChatRecord 对话框

IMPLEMENT_DYNAMIC(CChatRecord, CDialog)

CChatRecord::CChatRecord(CWnd* pParent /*=NULL*/)
	: CDialog(CChatRecord::IDD, pParent)
	, m_strSearche(_T("")), m_InSearch(false)
{

}

CChatRecord::~CChatRecord()
{
	for (map<HTREEITEM, VECTCHATLOG>::iterator IteVect = m_PrivateChats.begin(); IteVect != m_PrivateChats.end(); IteVect++)
	{
		for (int nIndex = 0; nIndex < (int)IteVect->second.size(); nIndex++)
		{
			delete IteVect->second[nIndex];
		}
		IteVect->second.clear();
	}
	m_PrivateChats.clear();

	for (int nIndex = 0; nIndex < (int)m_WorldChats.size(); nIndex++)
	{
		delete m_WorldChats[nIndex];
	}
	m_WorldChats.clear();

	for (int nIndex = 0; nIndex < (int)m_SameChats.size(); nIndex++)
	{
		delete m_SameChats[nIndex];
	}
	m_SameChats.clear();

	for (int nIndex = 0; nIndex < (int)m_SystemChats.size(); nIndex++)
	{
		delete m_SystemChats[nIndex];
	}
	m_SystemChats.clear();

	for (int nIndex = 0; nIndex < (int)m_ChatGroupChats.size(); nIndex++)
	{
		delete m_ChatGroupChats[nIndex];
	}
	m_ChatGroupChats.clear();

	for (int nIndex = 0; nIndex < (int)m_GroupChats.size(); nIndex++)
	{
		delete m_GroupChats[nIndex];
	}
	m_GroupChats.clear();

	for (int nIndex = 0; nIndex < (int)m_CommonChats.size(); nIndex++)
	{
		delete m_CommonChats[nIndex];
	}
	m_CommonChats.clear();

	m_PriItems.clear();
}

void CChatRecord::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TV_GROUPS, m_tvGroups);
	DDX_Control(pDX, IDC_LV_CHAT, m_lvChat);
	DDX_Control(pDX, IDC_EDT_CHATDESC, m_edtChatDesc);
}


BEGIN_MESSAGE_MAP(CChatRecord, CDialog)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TV_GROUPS, &CChatRecord::OnTvnSelchangedTvGroups)
	ON_BN_CLICKED(IDC_BTN_SEARCH, &CChatRecord::OnBnClickedBtnSearch)
	ON_NOTIFY(NM_CLICK, IDC_TV_GROUPS, &CChatRecord::OnNMClickTvGroups)
	ON_NOTIFY(NM_CLICK, IDC_LV_CHAT, &CChatRecord::OnNMClickLvChat)
	ON_BN_CLICKED(IDC_BTN_LASTPAGE, &CChatRecord::OnBnClickedBtnLastpage)
	ON_BN_CLICKED(IDC_BTN_NEXTPAGE, &CChatRecord::OnBnClickedBtnNextpage)
	ON_BN_CLICKED(IDC_BTN_PREPAGE, &CChatRecord::OnBnClickedBtnPrepage)
	ON_BN_CLICKED(IDC_BTN_FIRSTPAGE, &CChatRecord::OnBnClickedBtnFirstpage)
	ON_CBN_SELCHANGE(IDC_COMBO_PAGES, &CChatRecord::OnCbnSelchangeComboPages)
	ON_NOTIFY(NM_DBLCLK, IDC_LV_CHAT, &CChatRecord::OnNMDblclkLvChat)
	ON_MESSAGE(WM_DOLINK, OnLink)
	ON_MESSAGE(WM_GETFACE, OnGetFace)
	ON_WM_SYSCOMMAND()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CChatRecord 消息处理程序

void CChatRecord::OnTvnSelchangedTvGroups(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}

void CChatRecord::PostNcDestroy()
{
	CDialog::PostNcDestroy();
	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	pIrcMain->m_ChatRecord = NULL;
	delete this;
}

void CChatRecord::OnCancel()
{
	DestroyWindow();
	//CDialog::OnCancel();
}

void CChatRecord::OnOK()
{
	//CDialog::OnOK();
}

BOOL CChatRecord::OnInitDialog()
{
	CDialog::OnInitDialog();
	CreateFaceIcons();	
	DWORD dwStyle = GetWindowLong(m_tvGroups.m_hWnd, GWL_STYLE);
	dwStyle |= TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT;
	SetWindowLong(m_tvGroups.m_hWnd, GWL_STYLE, dwStyle);

	m_edtChatDesc.SetEventMask(m_edtChatDesc.GetEventMask() | ENM_LINK | ENM_MOUSEEVENTS | ENM_KEYEVENTS);
	m_LinkInfoDlg = NULL;

	m_CurChats = NULL;

	HTREEITEM hGroup = m_tvGroups.InsertItem(_T("所有分组"));

	HTREEITEM hFriend = m_tvGroups.InsertItem(_T("好友"), hGroup, NULL);
	m_PriItems.clear();
	//好友分组信息
	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	CFriendDlg* pFriendDlg = pIrcMain->m_UserRelation.GetFriendDlg();
	for (map<unsigned long, DispGroupInfo*>::iterator Ite = pFriendDlg->GetGroupBegin(); Ite != pFriendDlg->GetGroupEnd(); Ite++)
	{
		HTREEITEM hFriendGroup;
		hFriendGroup = m_tvGroups.InsertItem(Ite->second->GroupInfo.m_GroupName, hFriend, NULL);
		HTREEITEM hItem = NULL;
		for (map<unsigned long, FriendInfo_I*>::iterator IteFriend = pFriendDlg->GetFriendBegin(); IteFriend != pFriendDlg->GetFriendEnd(); IteFriend++)
		{
			if (IteFriend->second->groupID == Ite->second->GroupInfo.m_GroupID)
			{
				hItem = m_tvGroups.InsertItem(IteFriend->second->nickName, hFriendGroup, NULL);
				m_PriItems[IteFriend->second->nickName] = hItem;
			}
		}
	}

	HTREEITEM hFoeRoot = m_tvGroups.InsertItem(_T("仇人"), hGroup, hFriend);
	CFoeDlg* pFoeDlg = pIrcMain->m_UserRelation.GetFoeDlg();
	for (map<unsigned long, FoeInfo_I*>::iterator IteFoe = pFoeDlg->GetFoeBegin(); IteFoe != pFoeDlg->GetFoeEnd(); IteFoe++)
	{
		HTREEITEM hItem = m_tvGroups.InsertItem(IteFoe->second->nickName, hFoeRoot, NULL);
		m_PriItems[IteFoe->second->nickName] = hItem;
	}

	HTREEITEM hBlackItem = m_tvGroups.InsertItem(_T("黑名单"), hGroup, hFoeRoot);
	CBlackItemDlg* pBlackItemDlg = pIrcMain->m_UserRelation.GetBlackItemDlg();
	vector<string> vectBlackItem;
	pBlackItemDlg->GetBlackItem(vectBlackItem);
	for (int nIndex = 0; nIndex < (int)vectBlackItem.size(); nIndex++)
	{
		HTREEITEM hItem = m_tvGroups.InsertItem(vectBlackItem[nIndex].c_str(), hBlackItem, NULL);
		m_PriItems[vectBlackItem[nIndex].c_str()] = hItem;
	}

	HTREEITEM hStranger = m_tvGroups.InsertItem(_T("陌生人"), hGroup, hBlackItem);
	m_StrangerItem = hStranger;

	HTREEITEM hChannel = m_tvGroups.InsertItem(_T("频道"), NULL, hGroup);

	HTREEITEM hWorld = m_tvGroups.InsertItem(_T("世界"), hChannel, NULL);
	m_tvGroups.SetItemData(hWorld, (DWORD_PTR)&m_WorldChats);

	HTREEITEM hOrg = m_tvGroups.InsertItem(_T("战盟"), hChannel, hWorld);
	m_tvGroups.SetItemData(hOrg, (DWORD_PTR)&m_GuildChats);

	HTREEITEM hSameCity = m_tvGroups.InsertItem(_T("同城"), hChannel, hOrg);
	m_tvGroups.SetItemData(hSameCity, (DWORD_PTR)&m_SameChats);

	HTREEITEM hGM = m_tvGroups.InsertItem(_T("GM"), hChannel, hSameCity);
	m_tvGroups.SetItemData(hGM, (DWORD_PTR)&m_GMChats);

	HTREEITEM hGroupChannel = m_tvGroups.InsertItem(_T("组队"), hChannel, hGM);
	m_tvGroups.SetItemData(hGroupChannel, (DWORD_PTR)&m_GroupChats);

	HTREEITEM hCommon = m_tvGroups.InsertItem(_T("普通"), hChannel, hGroupChannel);
	m_tvGroups.SetItemData(hCommon, (DWORD_PTR)&m_CommonChats);

	HTREEITEM hChatGroup = m_tvGroups.InsertItem(_T("聊天群"), hChannel, hCommon);
	m_tvGroups.SetItemData(hChatGroup, (DWORD_PTR)&m_ChatGroupChats);

	HTREEITEM hSystem = m_tvGroups.InsertItem(_T("系统"), hChannel, hChatGroup);
	m_tvGroups.SetItemData(hSystem, (DWORD_PTR)&m_SystemChats);

	CRect rectlvChat;
	m_lvChat.GetClientRect(&rectlvChat);
	int nColWidth = rectlvChat.Width() / 4;
	m_lvChat.InsertColumn(0, _T("发信人"), LVCFMT_CENTER, nColWidth);
	m_lvChat.InsertColumn(1, _T("日期"), LVCFMT_CENTER, nColWidth);
	m_lvChat.InsertColumn(2, _T("发信内容"), LVCFMT_CENTER, nColWidth * 2);
	ListView_SetExtendedListViewStyle(m_lvChat.m_hWnd, LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP | LVS_EX_GRIDLINES);

	
	pIrcMain->GetIrcInfo()->ReadPrivateMsg();
	pIrcMain->GetIrcInfo()->ReadWorldMsg();
	pIrcMain->GetIrcInfo()->ReadGuildMsg();
	pIrcMain->GetIrcInfo()->ReadCommonMsg();
	pIrcMain->GetIrcInfo()->ReadSystermMsg();
	pIrcMain->GetIrcInfo()->ReadGroupMsg();
	pIrcMain->GetIrcInfo()->ReadChatGroupMsg();


	m_tvGroups.SelectItem(hGroup);
	CComboBox* pComBox = (CComboBox*)(GetDlgItem(IDC_COMBO_SPAN));
	pComBox->AddString(_T("全部"));
	pComBox->SetItemData(0, 0);
	pComBox->AddString(_T("所有分组"));
	pComBox->SetItemData(1, (DWORD_PTR)hGroup);
	pComBox->AddString(_T("频道"));
	pComBox->SetItemData(2, (DWORD_PTR)hChannel);
	pComBox->SetCurSel(1);
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_TV_GROUPS), this, CControlSizer::TOPLEFT, CControlSizer::FIXWIDTH));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_LV_CHAT), this, CControlSizer::TOPLEFT, CControlSizer::FIXNO));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_EDT_CHATDESC), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXHEIGHT));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_BTN_FIRSTPAGE), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXALL));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_BTN_PREPAGE), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXALL));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_BTN_NEXTPAGE), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXALL));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_BTN_LASTPAGE), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXALL));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_STATIC_PAGES), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXALL));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_STATIC_GO), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXALL));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_COMBO_PAGES), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXALL));
	m_ControlSizer.push_back(CControlSizer(GetDlgItem(IDC_STATIC_PAGE), this, CControlSizer::BOTTOMLEFT, CControlSizer::FIXALL));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CChatRecord::OnBnClickedBtnSearch()
{
	CString strKey;
	GetDlgItem(IDC_EDT_SEARCH)->GetWindowText(strKey);
	if (strKey.Compare(_T("")) == 0)
	{
		m_tvGroups.SelectItem(NULL);
		DisplayChatRec(NULL);
		return;
	}
	m_InSearch = true;
	CComboBox* pComBox = (CComboBox*)(GetDlgItem(IDC_COMBO_SPAN));
	int nSelIndex = pComBox->GetCurSel();
	HTREEITEM hItem = (HTREEITEM)pComBox->GetItemData(nSelIndex);
	m_SearchChats.clear();
	//在全部中搜索
	if (hItem == NULL)
	{
		hItem = m_tvGroups.GetRootItem();
		while (hItem != NULL)
		{
			SearchRecords(hItem, strKey);
			hItem = m_tvGroups.GetNextItem(hItem, TVGN_NEXT);
		}
	}
	else
	{
		SearchRecords(hItem, strKey);
	}

	m_CurChats = &m_SearchChats;
	DisplayChatRec(m_CurChats);
}

void CChatRecord::OnNMClickTvGroups(NMHDR *pNMHDR, LRESULT *pResult)
{
	m_InSearch = false;
	CPoint point;
	GetCursorPos(&point);

	m_tvGroups.ScreenToClient(&point);
	TVHITTESTINFO tvHitInfo;
	tvHitInfo.flags = TVHT_ONITEM | TVHT_ONITEMBUTTON | TVHT_ONITEMLABEL;
	tvHitInfo.pt = point;

	HTREEITEM hItem = m_tvGroups.HitTest(&tvHitInfo);
	if (hItem != NULL)
	{
		m_CurChats = (VECTCHATLOG*)(m_tvGroups.GetItemData(hItem));
		DisplayChatRec(m_CurChats);
	}
	if (m_tvGroups.GetParentItem(hItem) != NULL)
	{
		CComboBox* pComBox = (CComboBox*)(GetDlgItem(IDC_COMBO_SPAN));
		int nSize = pComBox->GetCount();
		if (nSize > 3)
		{
			pComBox->DeleteString(nSize - 1);
		}
		pComBox->AddString(m_tvGroups.GetItemText(hItem));
		nSize = pComBox->GetCount();
		pComBox->SetCurSel(nSize - 1);
		pComBox->SetItemData(nSize - 1, (DWORD_PTR)hItem);
	}
	else
	{
		CComboBox* pComBox = (CComboBox*)(GetDlgItem(IDC_COMBO_SPAN));
		int nSize = pComBox->GetCount();
		if (nSize > 3)
		{
			pComBox->DeleteString(nSize - 1);
		}
		for (int nIndex = 0; nIndex < pComBox->GetCount(); nIndex++)
		{
			if (pComBox->GetItemData(nIndex) == (DWORD_PTR)hItem)
			{
				pComBox->SetCurSel(nIndex);
				break;
			}
		}
	}

	*pResult = 0;
}

void CChatRecord::AddWorldChat(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_WorldChats.push_back(pNewChatLog);
}

void CChatRecord::AddGuildChat(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_GuildChats.push_back(pNewChatLog);
}

void CChatRecord::AddPrivateChat(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	map<string, HTREEITEM>::iterator IteItem = m_PriItems.find(pNewChatLog->PeerName);
	HTREEITEM hPriItem = NULL; 
	if (IteItem == m_PriItems.end())
	{
		bool bFind = false;
		HTREEITEM hItem = m_tvGroups.GetNextItem(m_StrangerItem, TVGN_CHILD);
		while (hItem)
		{
			if (StrCmp(pNewChatLog->PeerName, m_tvGroups.GetItemText(hItem)) == 0)
			{
				bFind = true;
				break;
			}
			hItem = m_tvGroups.GetNextItem(hItem, TVGN_NEXT);
		}
		if (!bFind)
		{
			hItem = m_tvGroups.InsertItem(pNewChatLog->PeerName, m_StrangerItem, NULL);
		}
		hPriItem = hItem;
	}
	else
	{
		hPriItem = IteItem->second;
	}

	map<HTREEITEM, VECTCHATLOG>::iterator Ite = m_PrivateChats.find(hPriItem);
	if (Ite == m_PrivateChats.end())
	{
		VECTCHATLOG vectChatLog;
		vectChatLog.push_back(pNewChatLog);
		Ite = m_PrivateChats.insert(m_PrivateChats.begin(), make_pair(hPriItem, vectChatLog));		
	}
	else
	{
		Ite->second.push_back(pNewChatLog);
	}

	if (m_tvGroups.GetItemData(hPriItem) == 0)
	{
		m_tvGroups.SetItemData(hPriItem, (DWORD_PTR)&(Ite->second));
	}
}

void CChatRecord::DisplayChatRec(VECTCHATLOG* pvectLog)
{
	CComboBox* pComboBox = static_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PAGES));
	pComboBox->SetCurSel(-1);
	for (int nIndex = pComboBox->GetCount() - 1; nIndex >= 0; nIndex--)
	{
		pComboBox->DeleteString(nIndex);
	}
	m_PageCount = 0;
	m_PageIndex = 0;

	CRect rtItem;
	m_lvChat.SetRedraw(FALSE);
	if (m_lvChat.GetItemCount() == 0)
	{
		m_lvChat.InsertItem(0, _T(""));
	}
	m_lvChat.GetItemRect(0, &rtItem, LVIR_BOUNDS);
	m_lvChat.DeleteAllItems();
	m_lvChat.SetRedraw(TRUE);
	m_lvChat.Invalidate();
	m_lvChat.UpdateWindow();
	
	CString strPages;
	strPages.Format(_T("当前第%d/%d页"), m_PageIndex, m_PageCount);
	GetDlgItem(IDC_STATIC_PAGES)->SetWindowText(strPages);

	GetDlgItem(IDC_BTN_FIRSTPAGE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_PREPAGE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_NEXTPAGE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_LASTPAGE)->EnableWindow(FALSE);
	
	m_edtChatDesc.SetSel(0, -1);
	m_edtChatDesc.ReplaceSel(_T(""));

	if (pvectLog == NULL)
	{
		return;
	}

	CRect rtListCtrl;
	m_lvChat.GetClientRect(rtListCtrl);
	m_ItemCount = (int)floor((double)rtListCtrl.Height() / rtItem.Height());
	//计算总页数
	m_PageCount = (int)ceill((double)pvectLog->size() / m_ItemCount);
	if (m_PageCount > 0)
	{
		m_PageIndex = 1;
	}

	for (int nIndex = 0; nIndex < m_PageCount; nIndex++)
	{
		CString strIndex;
		strIndex.Format(_T("%d"), nIndex + 1);
		pComboBox->InsertString(nIndex, strIndex);
	}

	if (m_PageIndex > 0)
	{
		DisplayPage(m_PageIndex);
	}
}
void CChatRecord::OnNMClickLvChat(NMHDR *pNMHDR, LRESULT *pResult)
{
	CPoint point;
	GetCursorPos(&point);
	m_lvChat.ScreenToClient(&point);

	LVHITTESTINFO lvHitInfo;
	lvHitInfo.flags = LVHT_ABOVE;
	lvHitInfo.pt = point;

	int nIndex = m_lvChat.SubItemHitTest(&lvHitInfo);
	if (nIndex != -1)
	{
		ChatLog* pChatLog = (ChatLog*)m_lvChat.GetItemData(nIndex);
		if (pChatLog)
		{
			CHATMSG chatMsg;
			sprintf(chatMsg.szRoleName, "%s", pChatLog->ChatDirection == ChatLog::LOCAL_SEND ? pChatLog->LocalName : pChatLog->PeerName);
			sprintf(chatMsg.szChat, "%s", pChatLog->Chat);
			m_edtChatDesc.SetWindowText(_T(""));
			m_edtChatDesc.AddChat(pChatLog->ClientType, &chatMsg, pChatLog->ChatDirection == ChatLog::LOCAL_SEND, false);
		}
	}
	*pResult = 0;
}

void CChatRecord::CreateFaceIcons()
{
	CIRC_UIDlg* pMainDlg = (CIRC_UIDlg*)(AfxGetApp()->GetMainWnd());
	for (int nIndex = 0; nIndex < pMainDlg->m_Face.GetFaceCount(); nIndex++)
	{
		CGif* pGif = pMainDlg->m_Face.GetGifAt(pMainDlg->m_Face.GetFaceNoAt(nIndex));
		m_Face.AddGif(pGif, pMainDlg->m_Face.GetFaceNoAt(nIndex));
	}
}

void CChatRecord::DisplayPage(int nIndex)
{
	GetDlgItem(IDC_BTN_FIRSTPAGE)->EnableWindow(nIndex != 1);
	GetDlgItem(IDC_BTN_PREPAGE)->EnableWindow(nIndex != 1);
	GetDlgItem(IDC_BTN_NEXTPAGE)->EnableWindow(nIndex != m_PageCount);
	GetDlgItem(IDC_BTN_LASTPAGE)->EnableWindow(nIndex != m_PageCount);

	CComboBox* pComboBox = static_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PAGES));
	int nCurSel = pComboBox->GetCurSel();
	if (nCurSel != nIndex - 1 && nIndex - 1 < pComboBox->GetCount())
	{
		m_PageIndex = nIndex;
		pComboBox->SetCurSel(nIndex - 1);
		OnCbnSelchangeComboPages();
	}
}
void CChatRecord::OnBnClickedBtnLastpage()
{
	if (m_PageCount > 1)
	{
		m_PageIndex = m_PageCount;
		DisplayPage(m_PageIndex);
	}
}

void CChatRecord::OnBnClickedBtnNextpage()
{
	if (m_PageCount > 1 && m_PageIndex < m_PageCount)
	{
		m_PageIndex++;
		DisplayPage(m_PageIndex);
	}
}

void CChatRecord::OnBnClickedBtnPrepage()
{
	if (m_PageCount > 1 && m_PageIndex > 1)
	{
		m_PageIndex--;
		DisplayPage(m_PageIndex);
	}
}

void CChatRecord::OnBnClickedBtnFirstpage()
{
	m_PageIndex = 1;
	if (m_PageIndex < m_PageCount)
	{
		DisplayPage(m_PageIndex);
	}
}

void CChatRecord::OnCbnSelchangeComboPages()
{
	CComboBox* pComboBox = static_cast<CComboBox*>(GetDlgItem(IDC_COMBO_PAGES));
	int nIndex = pComboBox->GetCurSel();
	m_PageIndex = nIndex + 1;
	CString strPages;
	strPages.Format(_T("当前第%d/%d页"), m_PageIndex, m_PageCount);
	GetDlgItem(IDC_STATIC_PAGES)->SetWindowText(strPages);

	GetDlgItem(IDC_BTN_FIRSTPAGE)->EnableWindow(m_PageIndex != 1);
	GetDlgItem(IDC_BTN_PREPAGE)->EnableWindow(m_PageIndex != 1);
	GetDlgItem(IDC_BTN_NEXTPAGE)->EnableWindow(m_PageIndex != m_PageCount);
	GetDlgItem(IDC_BTN_LASTPAGE)->EnableWindow(m_PageIndex != m_PageCount);

	m_lvChat.SetRedraw(FALSE);
	m_lvChat.DeleteAllItems();

	int nStartIndex = (m_PageIndex - 1) * m_ItemCount;
	for (int nIndex = 0; nIndex < m_ItemCount; nIndex++)
	{
		if ((nStartIndex + nIndex) > (int)m_CurChats->size() - 1)
		{
			break;
		}

		ChatLog* pChatLog = (*m_CurChats)[nStartIndex + nIndex];
		int nItemCount = m_lvChat.GetItemCount();
		CString strSender;
		if (pChatLog->ChatDirection == ChatLog::LOCAL_SEND)
		{
			strSender = pChatLog->LocalName;
		}
		else
		{
			strSender = pChatLog->PeerName;
		}

		int nItemIndex = m_lvChat.InsertItem(nItemCount, strSender);
		m_lvChat.SetItemData(nItemIndex, (DWORD_PTR)pChatLog);
		CString strSendTime;
		strSendTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"), pChatLog->LogTime.wYear, pChatLog->LogTime.wMonth, pChatLog->LogTime.wDay
			, pChatLog->LogTime.wHour, pChatLog->LogTime.wMinute, pChatLog->LogTime.wSecond);
		m_lvChat.SetItemText(nItemIndex, 1, strSendTime);

		CString strChat(pChatLog->Chat);

		int nExtraDataPos = strChat.ReverseFind(_T('['));
		strChat = strChat.Left(nExtraDataPos);
		m_lvChat.SetItemText(nItemIndex, 2, strChat);
	}
	m_lvChat.SetRedraw(TRUE);
	m_lvChat.Invalidate();
	m_lvChat.UpdateWindow();
	if (m_lvChat.GetItemCount() > 0)
	{
		m_lvChat.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
		ChatLog* pChatLog = (ChatLog*)m_lvChat.GetItemData(0);
		if (pChatLog)
		{
			CHATMSG chatMsg;
			sprintf(chatMsg.szRoleName, "%s", pChatLog->ChatDirection == ChatLog::LOCAL_SEND ? pChatLog->LocalName : pChatLog->PeerName);
			sprintf(chatMsg.szChat, "%s", pChatLog->Chat);
			m_edtChatDesc.SetWindowText(_T(""));
			m_edtChatDesc.AddChat(pChatLog->ClientType, &chatMsg, pChatLog->ChatDirection == ChatLog::LOCAL_SEND, false);
		}
	}
}

void CChatRecord::DoLink(LINKINFO &LinkInfo)
{
	if (m_LinkInfoDlg != NULL)
	{
		m_LinkInfoDlg->DestroyWindow();
		m_LinkInfoDlg = NULL;
	}

	if (m_LinkInfoDlg == NULL)
	{
		m_LinkInfoDlg = new CLinkInfoDlg(this);
		m_LinkInfoDlg->SetParentInfo(3, this);
		m_LinkInfoDlg->Create(CLinkInfoDlg::IDD, this);
	}

	CRect rectTips;
	m_LinkInfoDlg->GetWindowRect(&rectTips);

	CPoint pnt;
	pnt = m_edtChatDesc.PosFromChar(LinkInfo.nEndIndex);
	m_edtChatDesc.ClientToScreen(&pnt);
	m_LinkInfoDlg->MoveWindow(pnt.x, pnt.y, rectTips.Width(), rectTips.Height());

	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	switch(LinkInfo.nType)
	{
		//道具
	case 0:
		{
			vector<string> vectItemInfo;
			CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
			int nItemType = pIrcMain->GetIrcInfo()->GetItemInfo(LinkInfo.nID, vectItemInfo);
			m_LinkInfoDlg->UpdateTips(nItemType, LinkInfo.LevelUp, LinkInfo.AddLevel, LinkInfo.Wear, LinkInfo.MassLevel, vectItemInfo);
		}
		break;
		//技能
	case 1:
		{
			vector<string> vectSkillProp;
			pIrcMain->GetIrcInfo()->GetSkillInfo(LinkInfo.nID, vectSkillProp);
			m_LinkInfoDlg->UpdateSkillInfo(vectSkillProp);
		}
		break;
		//任务
	case 2:
		{
			vector<string> vectTaskInfo;
			CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
			pIrcMain->GetIrcInfo()->GetTaskInfo(LinkInfo.nID, vectTaskInfo);
			m_LinkInfoDlg->UpdateTips(vectTaskInfo);
		}
		break;
	default:
		break;
	}
	m_LinkInfoDlg->ShowWindow(SW_SHOW);
}
BOOL CChatRecord::PreTranslateMessage(MSG* pMsg)
{
	return CDialog::PreTranslateMessage(pMsg);
}

void CChatRecord::OnNMDblclkLvChat(NMHDR *pNMHDR, LRESULT *pResult)
{
	if (!m_InSearch)
	{
		return;
	}
	CPoint point;
	GetCursorPos(&point);

	m_lvChat.ScreenToClient(&point);

	LVHITTESTINFO lvHitInfo;
	lvHitInfo.flags = LVHT_ABOVE;
	lvHitInfo.pt = point;

	int nIndex = m_lvChat.SubItemHitTest(&lvHitInfo);
	if (nIndex != -1)
	{
		ChatLog* pChatLog = (ChatLog*)m_lvChat.GetItemData(nIndex);
		if (pChatLog)
		{
			//私聊
			if (pChatLog->ClientType == CT_PRIVATE_CHAT || pChatLog->ClientType == CT_PRIVATE_IRC_CHAT)
			{
				HTREEITEM hr = NULL;
				map<string, HTREEITEM>::iterator IteItem = m_PriItems.find(pChatLog->PeerName);
				if (IteItem == m_PriItems.end())
				{
					HTREEITEM hChild = m_tvGroups.GetChildItem(m_StrangerItem);
					while (hChild)
					{
						CString strText;
						strText = m_tvGroups.GetItemText(hChild);
						if (strText == pChatLog->PeerName)
						{
							hr = hChild;
							break;
						}
						hChild = m_tvGroups.GetNextItem(hChild, TVGN_NEXT);
					}
				}
				else
				{
					hr = IteItem->second;
				}
				map<HTREEITEM, VECTCHATLOG>::iterator IteChatLog = m_PrivateChats.find(hr);
				if (IteChatLog != m_PrivateChats.end())
				{
					m_CurChats = &(IteChatLog->second);
					DisplayChatRec(m_CurChats);
					HTREEITEM hrParent = m_tvGroups.GetParentItem(hr);
					if (hrParent)
					{
						m_tvGroups.Expand(hrParent, TVE_EXPAND);
					}
					m_tvGroups.EnsureVisible(hr);
					m_tvGroups.SetItemState(hr, TVIS_SELECTED, TVIS_SELECTED);
					Locate(pChatLog);
				}
			}
			else
			{
				CString strKey;
				//世界频道聊天
				if (pChatLog->ClientType == CT_WORLD_CHAT || pChatLog->ClientType == CT_WORLD_IRC_CHAT)
				{
					m_CurChats = &m_WorldChats;
					strKey = _T("世界");
				}
				if (pChatLog->ClientType == CT_UNION_CHAT || pChatLog->ClientType == CT_UNION_IRC_CHAT)
				{
					m_CurChats = &m_GuildChats;
					strKey = _T("战盟");
				}
				if (pChatLog->ClientType == CT_TEAM_CHAT)
				{
					m_CurChats = &m_GroupChats;
					strKey = _T("组队");
				}
				if (pChatLog->ClientType == CT_SYS_EVENT_NOTI || pChatLog->ClientType == CT_SYS_WARNING)
				{
					m_CurChats = &m_SystemChats;
					strKey = _T("系统");
				}
				if (pChatLog->ClientType == CT_SCOPE_CHAT)
				{
					m_CurChats = &m_CommonChats;
					strKey = _T("普通");
				}
				if (pChatLog->ClientType == CT_CUST_GROUP)
				{
					m_CurChats = &m_ChatGroupChats;
					strKey = _T("群聊");
				}

				DisplayChatRec(m_CurChats);
				HTREEITEM hr = m_tvGroups.GetRootItem();
				hr = m_tvGroups.GetNextItem(hr, TVGN_NEXT);
				m_tvGroups.Expand(hr, TVE_EXPAND);
				hr = m_tvGroups.GetNextItem(hr, TVGN_CHILD);
				while (hr)
				{
					if (m_tvGroups.GetItemText(hr) == strKey)
					{
						break;
					}
					hr = m_tvGroups.GetNextItem(hr, TVGN_NEXT);
				}
				m_tvGroups.EnsureVisible(hr);
				m_tvGroups.SelectItem(hr);
				Locate(pChatLog);

			}
		}
	}

	*pResult = 0;
}

void CChatRecord::SearchRecords(HTREEITEM ht, CString strKey)
{
	if (NULL == ht)
	{
		return;
	}

	VECTCHATLOG* pVectChatLog = (VECTCHATLOG*)(m_tvGroups.GetItemData(ht));
	if (pVectChatLog)
	{
		for (int nIndex = 0; nIndex < (int)pVectChatLog->size(); nIndex++)
		{
			ChatLog* pChatLog = (*pVectChatLog)[nIndex];
			CString strChat(pChatLog->Chat);
			//去掉附加数据
			int nExtraDataPos = strChat.ReverseFind(_T('['));
			strChat = strChat.Left(nExtraDataPos);
			//去掉表情数据
			int nKeyPos = strChat.Find(strKey);
			if (nKeyPos != -1)
			{
				m_SearchChats.push_back(pChatLog);
			}
		}
	}

	if (m_tvGroups.ItemHasChildren(ht))
	{
		HTREEITEM hrChildNode = m_tvGroups.GetChildItem(ht);
		while (hrChildNode)
		{
			SearchRecords(hrChildNode, strKey);
			hrChildNode = m_tvGroups.GetNextItem(hrChildNode, TVGN_NEXT);
		}
	}
}

int CChatRecord::Locate(ChatLog *pChatLog)
{
	size_t nIndex = 0;
	for (nIndex = 0; nIndex < m_CurChats->size(); nIndex++)
	{
		if (stricmp(pChatLog->PeerName, (*m_CurChats)[nIndex]->PeerName) == 0)
		{
			CTime tm1(pChatLog->LogTime);
			CTime tm2((*m_CurChats)[nIndex]->LogTime);
			if (tm1 == tm2)
			{
				break;
			}
		}
	}

	int nPageCount = (int)floor((double)nIndex / m_ItemCount);
	m_PageIndex = nPageCount + 1;
	DisplayPage(m_PageIndex);
	nIndex -= nPageCount * m_ItemCount;
	m_lvChat.SetItemState((int)nIndex, LVIS_SELECTED, LVIS_SELECTED);
	CHATMSG chatMsg;
	sprintf(chatMsg.szRoleName, "%s", pChatLog->ChatDirection == ChatLog::LOCAL_SEND ? pChatLog->LocalName : pChatLog->PeerName);
	sprintf(chatMsg.szChat, "%s", pChatLog->Chat);
	m_edtChatDesc.SetWindowText(_T(""));
	m_edtChatDesc.AddChat(pChatLog->ClientType, &chatMsg, pChatLog->ChatDirection == ChatLog::LOCAL_SEND, false);
	return 0;
}

LRESULT CChatRecord::OnLink(WPARAM wParam, LPARAM lParam)
{
	CIrcEdit* pIrcEdit = reinterpret_cast<CIrcEdit*>(wParam);
	LINKINFO* pLinkInfo = reinterpret_cast<LINKINFO*>(lParam);
	DoLink(*pLinkInfo);
	return 0;
}

LRESULT CChatRecord::OnGetFace(WPARAM wParam, LPARAM lParam)
{
	int nFaceIndex = *reinterpret_cast<int*>(&lParam);
	CGif* pFace = NULL;
	pFace = m_Face.GetGifAt(nFaceIndex);

	return reinterpret_cast<LRESULT>(pFace);
}
void CChatRecord::OnSysCommand(UINT nID, LPARAM lParam)
{
	if (nID == SC_MINIMIZE)
	{
		ShowWindow(SW_MINIMIZE);
	}
	else if (nID == SC_MAXIMIZE)
	{
		ShowWindow(SW_MAXIMIZE);
	}
	else if (nID = SW_RESTORE)
	{
		ShowWindow(SW_RESTORE);
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CChatRecord::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	LockWindowUpdate();
	for (size_t nIndex = 0; nIndex < m_ControlSizer.size(); nIndex++)
	{
		m_ControlSizer[nIndex].Update();
	}
	UnlockWindowUpdate();
}

void CChatRecord::AddCommonChat(ChatLog *pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_CommonChats.push_back(pNewChatLog);
}

void CChatRecord::AddSystemChat(ChatLog *pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_SystemChats.push_back(pNewChatLog);

}

void CChatRecord::AddTeamChat(ChatLog *pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_GroupChats.push_back(pNewChatLog);
}

void CChatRecord::AddChatGroupChat(ChatLog *pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_ChatGroupChats.push_back(pNewChatLog);
}