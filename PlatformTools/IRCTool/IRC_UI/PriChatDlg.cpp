﻿// PriChatDlg.cpp : 实现文件
//

#include "stdafx.h"
#include <afxole.h>
#include <afxodlgs.h>
#include <afxpriv.h>
#include <Richole.h>

#include "IRC_UI.h"
#include "PriChatDlg.h"
#include "IRC_UIDlg.h"
#include "Util.h"
#include "Helper.h"

// CPriChatDlg 对话框

IMPLEMENT_DYNCREATE(CPriChatDlg, CDialog)

CPriChatDlg::CPriChatDlg(CWnd* pParent /*=NULL*/, CIRC_UIDlg* pIrcMain)
	: CDialog(CPriChatDlg::IDD, pParent), m_IrcMain(pIrcMain)
	, m_Title(_T(""))
{
	m_bIsDragging = false;
}

CPriChatDlg::~CPriChatDlg()
{
	
}

void CPriChatDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SD_MINIMIZE, m_BtnMinimize);
	DDX_Control(pDX, IDC_SD_FILTER, m_BtnFilter);
	DDX_Control(pDX, IDC_SD_CANCEL, m_BtnClose);
	DDX_Control(pDX, IDC_FACE, m_Face);
	DDX_Control(pDX, IDC_TAB_USERINFO, m_UserInfoTab);
	DDX_Control(pDX, IDC_PRIRECVMSG, m_MsgEdit);
	DDX_Control(pDX, IDC_PRISENDMSG, m_LocalMsg);
	DDX_Control(pDX, IDC_EDT_USRINFO, m_UsrInfo);
	DDX_Control(pDX, IDC_UserInfo, m_BtnUsrInfo);
	DDX_Text(pDX, IDC_LBL_TITLE, m_Title);
}

BOOL CPriChatDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_LinkInfoDlg = NULL;
	m_BtnMinimize.LoadStdImage(IDR_MINIMIZE, _T("PNG"));
	m_BtnFilter.LoadStdImage(IDR_FILTER, _T("PNG"));
	m_BtnClose.LoadStdImage(IDR_CANCEL, _T("PNG"));

	m_UserInfoTab.InsertItem(0, _T("游戏"));
	m_UserInfoTab.InsertItem(1, _T("交友"));

	InitSource();
	
	//设置背景图片
	CreateFaceIcon();
	m_MsgEdit.SetEventMask(m_MsgEdit.GetEventMask() | EN_LINK | ENM_MOUSEEVENTS | ENM_SCROLL | ENM_KEYEVENTS);

	m_LocalMsg.SetEventMask(m_LocalMsg.GetEventMask() | ENM_CHANGE | ENM_KEYEVENTS);
	m_LocalMsg.LimitText(MAX_TEXT_MSG);

	m_bFilter = false;
	m_bUserInfoDisp = false;
	
	OnBnClickedUserinfo();
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

BEGIN_MESSAGE_MAP(CPriChatDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_SD_MINIMIZE, &CPriChatDlg::OnBnClickedSdMinimize)
	ON_BN_CLICKED(IDC_SD_FILTER, &CPriChatDlg::OnBnClickedSdFilter)
	ON_BN_CLICKED(IDC_SD_CANCEL, &CPriChatDlg::OnBnClickedSdCancel)
	ON_BN_CLICKED(IDC_UserInfo, &CPriChatDlg::OnBnClickedUserinfo)
	ON_BN_CLICKED(IDC_FACE, &CPriChatDlg::OnBnClickedFace)
	ON_WM_SIZE()
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_DOLINK, OnLink)
	ON_MESSAGE(WM_GETFACE, OnGetFace)
	ON_MESSAGE(WM_SENDMSG, OnSendMsg)
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()

void CPriChatDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		//dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

BOOL CPriChatDlg::OnEraseBkgnd(CDC* pDC)
{
	return /*CDialog::OnEraseBkgnd(pDC)*/TRUE;
}

void CPriChatDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	SetCapture();
	m_ptCurPoint = point;
	m_bIsDragging = true;
	CDialog::OnLButtonDown(nFlags, point);
}

void CPriChatDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(m_bIsDragging)
	{
		ReleaseCapture();
		m_bIsDragging = FALSE;
	}

	CDialog::OnLButtonUp(nFlags, point);
}


void CPriChatDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if(m_bIsDragging)
	{
		ClientToScreen(&point);
		SetWindowPos(	NULL, point.x - m_ptCurPoint.x, point.y - m_ptCurPoint.y, 0, 0, 
			SWP_NOZORDER | SWP_NOSIZE);

	}	
	CDialog::OnMouseMove(nFlags, point);
}

void CPriChatDlg::OnBnClickedSdMinimize()
{
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}

void CPriChatDlg::OnBnClickedSdFilter()
{
	m_bFilter = !m_bFilter;
}

void CPriChatDlg::OnBnClickedSdCancel()
{
	OnCancel();
}

void CPriChatDlg::CreateFaceIcon()
{
	CIRC_UIDlg* pMainDlg = (CIRC_UIDlg*)(AfxGetApp()->GetMainWnd());
	for (int nIndex = 0; nIndex < pMainDlg->m_Face.GetFaceCount(); nIndex++)
	{
		CGif* pGif = pMainDlg->m_Face.GetGifAt(pMainDlg->m_Face.GetFaceNoAt(nIndex));
		m_Face.AddGif(pGif, pMainDlg->m_Face.GetFaceNoAt(nIndex));
	}
}

void CPriChatDlg::OnBnClickedUserinfo()
{
	DisplayUsrInfo(m_bUserInfoDisp);
	m_bUserInfoDisp = !m_bUserInfoDisp;
	m_BtnUsrInfo.SetWindowText(m_bUserInfoDisp ? _T(">") : _T("<"));
}

void CPriChatDlg::OnBnClickedFace()
{
	LPRICHEDITOLE lpRichEditOle = m_LocalMsg.GetIRichEditOle();
	long nObjCount = lpRichEditOle->GetObjectCount();
	if (nObjCount + 1 > MAX_ICON_MSG || nObjCount + m_LocalMsg.GetCharCount() + 2 > MAX_TEXT_MSG)
	{
		lpRichEditOle->Release();
		return;
	}
	lpRichEditOle->Release();
	m_LocalMsg.InsertFaceToEdit(m_Face.GetGifAt(m_Face.GetCurFaceNo()), m_Face.GetCurFaceNo());
	m_LocalMsg.SetFocus();
}

BOOL CPriChatDlg::PreTranslateMessage(MSG* pMsg)
{
	return CDialog::PreTranslateMessage(pMsg);
}

void CPriChatDlg::OnOK()
{

	//CDialog::OnOK();
}

void CPriChatDlg::OnCancel()
{
	DestroyWindow();
	//CDialog::OnCancel();
}

void CPriChatDlg::PostNcDestroy()
{
	m_IrcMain->RemovePriChat(m_PeerRoleName);
	CDialog::PostNcDestroy();
	delete this;
}

void CPriChatDlg::DisplayUsrInfo(bool bDisplay)
{
	m_UsrInfo.ShowWindow(bDisplay ? SW_SHOW : SW_HIDE);
	m_UserInfoTab.ShowWindow(bDisplay ? SW_SHOW : SW_HIDE);

	CRect rectUsrInfo;
	m_UsrInfo.GetWindowRect(&rectUsrInfo);

	CRect rectDlg;
	GetWindowRect(&rectDlg);

	CRect rectMin;
	m_BtnMinimize.GetWindowRect(&rectMin);

	CRect rectFilter;
	m_BtnFilter.GetWindowRect(&rectFilter);

	CRect rectClose;
	m_BtnClose.GetWindowRect(&rectClose);
	
	if (bDisplay)
	{
		rectDlg.right += rectUsrInfo.Width();
		rectMin.left += rectUsrInfo.Width();
		rectMin.right += rectUsrInfo.Width();
		rectFilter.left += rectUsrInfo.Width();
		rectFilter.right += rectUsrInfo.Width();
		rectClose.left += rectUsrInfo.Width();
		rectClose.right += rectUsrInfo.Width();
	}
	else
	{
		rectDlg.right -= rectUsrInfo.Width();
		rectMin.left -= rectUsrInfo.Width();
		rectMin.right -= rectUsrInfo.Width();
		rectFilter.left -= rectUsrInfo.Width();
		rectFilter.right -= rectUsrInfo.Width();
		rectClose.left -= rectUsrInfo.Width();
		rectClose.right -= rectUsrInfo.Width();
	}

	ScreenToClient(&rectMin);
	m_BtnMinimize.MoveWindow(rectMin);

	ScreenToClient(&rectFilter);
	m_BtnFilter.MoveWindow(rectFilter);

	ScreenToClient(&rectClose);
	m_BtnClose.MoveWindow(rectClose);

	MoveWindow(rectDlg);
}
void CPriChatDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	Invalidate();
}

void CPriChatDlg::FormatUsrInfo(PEERUSRINFO *pPeerUsrInfo)
{
	CString strName;
	strName.Format(_T("姓名:%s\r\n"), pPeerUsrInfo->playerName);
	m_UsrInfo.SetSel(-1, -1);	
	m_UsrInfo.ReplaceSel(strName);


	CString strGenger;
	strGenger.Format(_T("性别:%s\r\n"), m_IrcMain->GetIrcInfo()->GetGender(pPeerUsrInfo->playerGender).c_str());
	m_UsrInfo.SetSel(-1, -1);
	m_UsrInfo.ReplaceSel(strGenger);

	CString strJob;
	strJob.Format(_T("职业:%s\r\n"), m_IrcMain->GetIrcInfo()->GetClassByClassID(pPeerUsrInfo->playerClass).c_str());
	m_UsrInfo.SetSel(-1, -1);
	m_UsrInfo.ReplaceSel(strJob);

	CString strLevel;
	strLevel.Format(_T("等级:%d\r\n"), pPeerUsrInfo->playerLevel);
	m_UsrInfo.SetSel(-1, -1);
	m_UsrInfo.ReplaceSel(strLevel);

	CString strFereName;
	strFereName.Format(_T("伴侣:%s\r\n"),  pPeerUsrInfo->FereName);
	m_UsrInfo.SetSel(-1, -1);
	m_UsrInfo.ReplaceSel(strFereName);

	CString strGuildName;
	strGuildName.Format(_T("战盟:%s\r\n"),  pPeerUsrInfo->GuildName);
	m_UsrInfo.SetSel(-1, -1);
	m_UsrInfo.ReplaceSel(strGuildName);

	CHARFORMAT2 cf2;
	ZeroMemory(&cf2, sizeof(CHARFORMAT2));

	cf2.cbSize = sizeof(CHARFORMAT2);
	COLORREF ChatClr;
	ChatClr = RGB(0, 0, 255);

	cf2.dwMask |= CFM_COLOR;
	cf2.crTextColor = ChatClr;

	cf2.dwMask |= CFM_SIZE;
	cf2.yHeight = 200;

	cf2.dwMask |= CFM_FACE;
	strcpy(cf2.szFaceName, "宋体");

	m_UsrInfo.SetSel(0, -1);
	m_UsrInfo.SetSelectionCharFormat(cf2);


	PARAFORMAT pf;
	pf.cbSize = sizeof(pf);
	pf.dwMask = PFM_ALIGNMENT;
	pf.wAlignment = PFA_LEFT;
	m_UsrInfo.SetParaFormat(pf);

	m_UsrInfo.SetSel(-1, -1);

}

void CPriChatDlg::DoLink(LINKINFO &LinkInfo)
{
	if (m_LinkInfoDlg != NULL)
	{
		m_LinkInfoDlg->DestroyWindow();
		m_LinkInfoDlg = NULL;
	}

	if (m_LinkInfoDlg == NULL)
	{
		m_LinkInfoDlg = new CLinkInfoDlg(this);
		m_LinkInfoDlg->SetParentInfo(2, this);
		m_LinkInfoDlg->Create(CLinkInfoDlg::IDD, this);
	}

	CRect rectTips;
	m_LinkInfoDlg->GetWindowRect(&rectTips);

	CPoint pnt;
	pnt = m_MsgEdit.PosFromChar(LinkInfo.nEndIndex);
	m_MsgEdit.ClientToScreen(&pnt);
	m_LinkInfoDlg->MoveWindow(pnt.x, pnt.y, rectTips.Width(), rectTips.Height());

	switch(LinkInfo.nType)
	{
		//道具
	case 0:
		{
			vector<string> vectItemInfo;
			CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
			int nItemType = pIrcMain->GetIrcInfo()->GetItemInfo(LinkInfo.nID, vectItemInfo);
			m_LinkInfoDlg->UpdateTips(nItemType, LinkInfo.LevelUp, LinkInfo.AddLevel, LinkInfo.Wear, LinkInfo.MassLevel, vectItemInfo);
		}
		break;
		//技能
	case 1:
		{
			vector<string> vectSkillProp;
			m_IrcMain->GetIrcInfo()->GetSkillInfo(LinkInfo.nID, vectSkillProp);
			m_LinkInfoDlg->UpdateSkillInfo(vectSkillProp);
		}
		break;
		//任务
	case 2:
		{
			vector<string> vectTaskInfo;
			CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
			pIrcMain->GetIrcInfo()->GetTaskInfo(LinkInfo.nID, vectTaskInfo);
			m_LinkInfoDlg->UpdateTips(vectTaskInfo);
		}
		break;
	default:
		break;
	}
	m_LinkInfoDlg->ShowWindow(SW_SHOW);
}
void CPriChatDlg::ShowPriChatError(int nErrType, CString strRoleName)
{
	CString strErrInfo("私聊错误：");
	strErrInfo += strRoleName;
	switch(nErrType)
	{
	case 0:
		{
			strErrInfo += _T("玩家不存在或者不在线");
		}
		break;
	case 1:
		{
			strErrInfo += _T("没有世界聊天权限");
		}
		break;
	case 2:
		{
			strErrInfo += _T("没有私聊权限");
		}
	    break;
	case 3:
		{
			strErrInfo += _T("");
		}
	    break;
	default:
	    break;
	}

	m_MsgEdit.SetSel(-1, -1);
	m_MsgEdit.ReplaceSel(_T("\r\n"));

	m_MsgEdit.SetSel(-1, -1);
	m_MsgEdit.ReplaceSel(strErrInfo);

	m_LocalMsg.SetReadOnly();
}

HBRUSH CPriChatDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	if (pWnd->GetDlgCtrlID() == IDC_LBL_TITLE)
	{
		pDC->SetBkMode(TRANSPARENT);
		hbr = (HBRUSH)GetStockObject(NULL_BRUSH);
	}
	return hbr;
}

LRESULT CPriChatDlg::OnLink(WPARAM wParam, LPARAM lParam)
{
	CIrcEdit* pIrcEdit = reinterpret_cast<CIrcEdit*>(wParam);
	LINKINFO* pLinkInfo = reinterpret_cast<LINKINFO*>(lParam);
	DoLink(*pLinkInfo);
	return 0;
}

LRESULT CPriChatDlg::OnGetFace(WPARAM wParam, LPARAM lParam)
{
	int nFaceIndex = *reinterpret_cast<int*>(&lParam);
	CGif* pFace = NULL;
	pFace = m_Face.GetGifAt(nFaceIndex);

	return reinterpret_cast<LRESULT>(pFace);
}

LRESULT CPriChatDlg::OnSendMsg(WPARAM wParam, LPARAM lParam)
{
	CIrcEdit* pIrcEidt = reinterpret_cast<CIrcEdit*>(wParam);
	CString* pStrMsg = reinterpret_cast<CString*>(lParam);
	m_IrcMain->GetIrcInfo()->SendPrivateMsg(LPCTSTR(m_PeerRoleName), *pStrMsg);
	CHATMSG chatMsg;
	sprintf(chatMsg.szRoleName, "%s", m_IrcMain->m_RoleName.c_str());
	sprintf(chatMsg.szChat, "%s", *pStrMsg);
	m_MsgEdit.AddChat(CT_PRIVATE_CHAT, &chatMsg, true);
	m_LocalMsg.SetWindowText(_T(""));
	m_LocalMsg.Invalidate();
	return 0;
}

bool CPriChatDlg::InitSource()
{
	HBITMAP hBitMap = NULL;
	HBITMAP hd = NULL;
	if (CUISourceManager::GetInstance()->GetSourceHandle("FaceIcon0", hBitMap))
	{
		if (CUISourceManager::GetInstance()->GetSourceHandle("FaceIcon1",hd))
		{
			m_Face.SetBitmaps(hd,RGB(255,0,255),hBitMap,RGB(255,0,255));
		}
		else
		{
			m_Face.SetBitmaps(hBitMap,RGB(255,0,255));
		}
		m_Face.DrawTransparent(TRUE);
		m_Face.DrawBorder(FALSE);
	}

	return true;

}

void CPriChatDlg::ShowOffLineTip(BOOL bShow)
{
	CStatic* pLabel = (CStatic*)GetDlgItem(IDC_LBL_OFFLINETIPS);
	pLabel->ShowWindow(bShow ? SW_SHOW : SW_HIDE);
}
void CPriChatDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if (nID == SC_MINIMIZE)
	{
		ShowWindow(SW_MINIMIZE);
	}
	else if (nID == SC_MAXIMIZE)
	{
		m_bFilter = !m_bFilter;
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
	
}
