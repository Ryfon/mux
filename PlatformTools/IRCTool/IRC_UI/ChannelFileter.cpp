﻿// ChannelFileter.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "ChannelFileter.h"
#include "IRC_UIDlg.h"

// CChannelFileter 对话框

IMPLEMENT_DYNAMIC(CChannelFileter, CDialog)

CChannelFileter::CChannelFileter(CWnd* pParent /*=NULL*/)
	: CDialog(CChannelFileter::IDD, pParent)
	, m_WorldFilter(false)
	, m_OrgFilter(false)
	, m_SameCityFilter(false)
	, m_GmFilter(false)
{

}

CChannelFileter::~CChannelFileter()
{

}

void CChannelFileter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_WORLD, (int&)m_WorldFilter);
	DDX_Check(pDX, IDC_CHECK_ORG, (int&)m_OrgFilter);
	DDX_Check(pDX, IDC_CHECK_SAMECITY, (int&)m_SameCityFilter);
	DDX_Check(pDX, IDC_CHECK_GM, (int&)m_GmFilter);

}


BEGIN_MESSAGE_MAP(CChannelFileter, CDialog)
END_MESSAGE_MAP()


// CChannelFileter 消息处理程序

void CChannelFileter::OnOK()
{
	//CDialog::OnOK();
}

void CChannelFileter::OnCancel()
{
	UpdateData();
	CIRC_UIDlg* pMainDlg = dynamic_cast<CIRC_UIDlg*>(::AfxGetApp()->GetMainWnd());
	if (pMainDlg->m_TabMsg.GetCurSel() == 1)
	{
		pMainDlg->FilterChannel();
	}
	CDialog::OnCancel();
}

BOOL CChannelFileter::OnInitDialog()
{
	CDialog::OnInitDialog();

	((CButton*)GetDlgItem(IDC_CHECK_WORLD))->SetCheck(BST_CHECKED);
	((CButton*)GetDlgItem(IDC_CHECK_ORG))->SetCheck(BST_CHECKED);
	((CButton*)GetDlgItem(IDC_CHECK_SAMECITY))->SetCheck(BST_CHECKED);
	((CButton*)GetDlgItem(IDC_CHECK_GM))->SetCheck(BST_CHECKED);

	UpdateData();
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CChannelFileter::ResetControl()
{
	((CButton*)GetDlgItem(IDC_CHECK_WORLD))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_CHECK_ORG))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_CHECK_SAMECITY))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_CHECK_GM))->SetCheck(BST_UNCHECKED);

	UpdateData();
}