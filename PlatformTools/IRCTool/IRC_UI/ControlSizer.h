﻿#ifndef CONTROLSIZER_H
#define CONTROLSIZER_H

class CControlSizer
{
public:
	enum FIXPOINT {TOPLEFT = 0, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, ALLFIXED};
	enum FIXDIM {FIXHEIGHT = 0, FIXWIDTH, FIXALL, FIXNO};
	CControlSizer(CWnd* pControl, CWnd* pParent, FIXPOINT fp, FIXDIM fd);
	virtual ~CControlSizer(void);
public:
	void Update();
private:
	CWnd* m_Control; //被调整的控件
	CWnd* m_Owner; //被调整控件的Parent
	CRect m_ControlRect;
	CRect m_OwnerRect;
	FIXPOINT m_fp;
	FIXDIM m_fd;
};

#endif