﻿#pragma once

#ifdef _WIN32_WCE
#error "Windows CE 不支持 CDHtmlDialog。"
#endif 

#include "IRCIntf.h"
#include "TabSheet.h"
#include "FoeDlg.h"
#include "FriendDlg.h"
#include "BlackItemDlg.h"
#include "UnionDlg.h"
// CUserRelation 对话框

class CUserRelation : public CDialog
{
	DECLARE_DYNCREATE(CUserRelation)

public:
	CUserRelation(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CUserRelation();

// 对话框数据
	enum { IDD = IDD_USERRELATION};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	virtual void OnOK();
	virtual void OnCancel();
private:
	CTabSheet m_UserTab;
	CFriendDlg m_FriendDlg;
	CBlackItemDlg m_BlackItemDlg;
	CFoeDlg m_FoeDlg;
	CUnionDlg m_UnionDlg;
public:
	void AddUnionDlg();
	void AddGuildMem(UnionMember_I* pGuildMem);
	void GuildMemOnOffline(UINT nMemID, bool bOnline);
	void GuildMemBanChat(const char* szRoleName);
	void RemoveUnionDlg();
	void ClearRelation();
	void UserOffLine();
	void AddGroupInfo(FriendGroupInfo_I* pFriendGroupInfo);
	void AddFriendInfo(FriendInfo_I* pFriendInfo);
	void AddBlackItem(CString strBlackName);
	void AddFoeInfo(FoeInfo_I* pFoeInfo);
	void FriendInfoUpdate(FriendInfo_I* pFriendInfo);
	void FoeInfoUpdate(FoeInfo_I* pFoeInfo);
	int GetFriendOnLineType(unsigned long ulFriendID);
	int GetFoeOnLineType(unsigned long ulFoeID);
	int GetUserType(const string& strRoleName);
	bool IsFoeBanned(const string& strRoleName)
	{
		return m_FoeDlg.IsFoeBanned(strRoleName);
	}
	
	bool IsGuildMemBanned(const string& strRoleName)
	{
		return m_UnionDlg.IsBanned(strRoleName);		 
	}

	bool IsBanned();

	CString GetFriendName(unsigned long ulFriendID)
	{
		return m_FriendDlg.GetFriendName(ulFriendID);
	}
	CString GetFoeName(unsigned long ulFoeID)
	{
		return m_FoeDlg.GetFoeName(ulFoeID);
	}

	CFoeDlg* GetFoeDlg()
	{
		return &m_FoeDlg;
	}

	CFriendDlg* GetFriendDlg()
	{
		return &m_FriendDlg;
	}

	CBlackItemDlg* GetBlackItemDlg()
	{
		return &m_BlackItemDlg;
	}
	CUnionDlg* GetUnionDlg()
	{
		return &m_UnionDlg;
	}

	void AddGuildMemInfo(UnionMember_I* pUnionMemInfo);
	void DelGuildMem(UINT nMemID);
	void GuildMemberTitleChange(const char* szMemberName, const char* szTitleName);
	void UpdateGuildPos();
	void UpdateGuildMemPos(const char* szMemberName, USHORT nPos);
};
