﻿#pragma once
#include "afxcmn.h"
#include "afxwin.h"

// CSysConfig 对话框

class CSysConfig : public CDialog
{
	DECLARE_DYNAMIC(CSysConfig)

public:
	CSysConfig(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSysConfig();

// 对话框数据
	enum { IDD = IDD_SYSCONFIG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnOk();
	afx_msg void OnBnClickedBtnCancel();
	afx_msg void OnBnClickedBtnApply();
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();
private:
	void InitControls();
	void DisplayGroup(int nIndex);
	void UpdateConfig();
	static int CALLBACK SetDefaultDir(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData);
public:
	CListCtrl m_lvConfigSection;
	CButton m_btnOk;
	CButton m_btnCancel;
	CButton m_btnApply;
	afx_msg void OnNMClickLstSection(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnOpenfile();
	afx_msg void OnBnClickedBtbClearmsg();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
};
