﻿#include "StdAfx.h"
#include "Helper.h"
#include "shlwapi.h"

#pragma comment( lib, "Msimg32.lib" )

CHelper::CHelper(void)
{
}

CHelper::~CHelper(void)
{
}

BOOL CHelper::LoadBitmapFromBMPFile(LPCTSTR szFileName, HANDLE & phBitmap, bool bSpeical)
{
	phBitmap = NULL;	

	CFileFind finder;
	if(!finder.FindFile(szFileName))
	{
		return FALSE;
	}

	if (!bSpeical)
	{
		// Use LoadImage() to get the image loaded in to a DIBSection
		phBitmap = LoadImage(NULL, szFileName, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_DEFAULTSIZE | LR_LOADFROMFILE);
	}
	else
	{
		// Use LoadImage() to get the image loaded in to a DIBSection
		phBitmap = LoadImage(NULL, szFileName, IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_LOADFROMFILE);
	}

	if(phBitmap == NULL)
	{
		return FALSE;
	}

	
	return TRUE;
}

BOOL CHelper::LoadIconFromFile(LPCTSTR szFileName, HANDLE & phIcon)
{
	phIcon = NULL;	

	CFileFind finder;
	if(!finder.FindFile(szFileName))
	{
		return FALSE;
	}

	// Use LoadImage() to get the image loaded in to a DIBSection
	phIcon = (HBITMAP)LoadImage(NULL, szFileName, IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_LOADFROMFILE);

	if(phIcon == NULL)
	{
		return FALSE;
	}

	return TRUE;
}

void CHelper::DrawBitmap(CPaintDC * pDC, CRect *pRect, HBITMAP phBitMap,int iColor)
{
	if (pDC == NULL || pRect == NULL)
	{
		return;
	}

	BITMAP bm;

	GetObject(phBitMap, sizeof(BITMAP), &bm);

	CDC MemDC;
	MemDC.CreateCompatibleDC(pDC);

	HBITMAP hOldBitmap = (HBITMAP)SelectObject(MemDC, phBitMap);
	if (bm.bmBitsPixel * bm.bmPlanes < 32)
	{
		TransparentBlt(pDC->GetSafeHdc(),0,0,pRect->right,pRect->bottom,
			MemDC.GetSafeHdc(),0,0,bm.bmWidth,bm.bmHeight,RGB(0xff,0,0xff));
	}
	else if(iColor > 0)
	{
		BLENDFUNCTION blendFun ;
		blendFun.BlendOp = AC_SRC_OVER;
		blendFun.BlendFlags = 0;
		blendFun.SourceConstantAlpha = iColor;
		blendFun.AlphaFormat = AC_SRC_OVER;
		AlphaBlend(pDC->GetSafeHdc(),0,0,pRect->right,pRect->bottom,
			MemDC.GetSafeHdc(),0,0,bm.bmWidth,bm.bmHeight,blendFun);
	}
	
	MemDC.SelectObject(hOldBitmap);
	MemDC.DeleteDC();
}

string CHelper::GetAppWorkDir()
{
	TCHAR tszCurPath[MAX_PATH];
	::GetModuleFileName(NULL, tszCurPath, MAX_PATH - 1);
	LPTSTR lpPos = StrRChrI(tszCurPath, NULL, '\\');
	tszCurPath[lpPos - tszCurPath] = 0;
	string strUIPath(tszCurPath);
	strUIPath += "\\";
	return strUIPath;
}

bool CHelper::IsRoot(string strFilePath)
{
	
	if (strFilePath.size() == 0)
	{
		return false;
	}
	string Root;
	Root = strFilePath.at(0);
	Root += ":\\";

	if ( Root== strFilePath )
	{
		return true;
	}

	else
	{
		return false;
	}
	
}

string CHelper::RepleaceString(char chToken, char chDesToken, const std::string str)
{
	vector<string> vSeg;
	
	_SplitString(str.c_str(),vSeg, chToken);

	string strResult;

	for (unsigned int i = 0; i < vSeg.size(); i++)
	{
		strResult += vSeg[i];
		strResult += chDesToken;
	}

	return strResult;
}

void CHelper::_SplitString( const char * szStr, vector< string >& vStrs, char chToken )
{
	if (NULL == szStr)
	{
		return;
	}

	string strCur;
	for ( UINT nIndex = 0; nIndex < strlen( szStr ); nIndex ++ )
	{
		if ( chToken == szStr[nIndex] )
		{
			vStrs.push_back( strCur );
			strCur = "";
		}
		else
		{
			strCur.push_back( szStr[nIndex] );
		}
	}

	if ( strCur.length() >= 1 )
		vStrs.push_back( strCur );
}
