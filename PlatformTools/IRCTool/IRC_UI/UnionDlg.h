﻿#pragma once
#include "afxcmn.h"
#include "IRCIntf.h"
#include "UsrInfoDlg.h"

// CUnionDlg 对话框

class CUnionDlg : public CDialog
{
	DECLARE_DYNAMIC(CUnionDlg)

public:
	CUnionDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CUnionDlg();

// 对话框数据
	enum { IDD = IDD_DLG_UNION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	void AddGuildMemInfo(UnionMember_I* pUnionMemInfo);
	void DelGuildMem(UINT nMemID);
	void ClearGuildMemInfo();
	void GuildMemOnOffline(UINT nMemID, bool bOnline);
	void Offline();
	bool IsBanned(const string& strRoleName);
	bool IsBanned();
	void GuildBanChat(const char* szRoleName);
	void GuildMemberTitleChange(const char* szMemberName, const char* szTitleName);
	void UpdateGuildPos();
	void UpdateGuildMemPos(const char* szMemberName, USHORT nPos);
public:
	afx_msg void OnNMRclickUnionlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawUnionlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkUnionlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnGuildCopyname();
	afx_msg void OnGuildBan();
	afx_msg void OnGuildPrivateChat();
private:
	CMenu m_GuildMenu;
	CListCtrl m_UnionMemberList;
	map<unsigned long, UnionMember_I*> m_GuildMemMap;
	UnionMember_I* m_CurGuildMem;
	CUsrInfoDlg* m_UsrInfoDlg;
	map<string, UnionMember_I*> m_BanGuildMemMap;

};
