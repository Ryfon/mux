﻿/** @file UIIconSource.h 
@brief icn资源
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2009-06-23
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#pragma once
#include "uisource.h"

class CUIIconSource :
	public CUISource
{
public:
	CUIIconSource(void);
	~CUIIconSource(void);

	virtual bool Initial(string strPath);
};
