﻿#pragma once
#include "afxcmn.h"
#include "IRCIntf.h"
#include "UsrInfoDlg.h"

// CFoeDlg 对话框

class CFoeDlg : public CDialog
{
	DECLARE_DYNAMIC(CFoeDlg)

public:
	CFoeDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CFoeDlg();

// 对话框数据
	enum { IDD = IDD_DLG_FOE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	void AddFoeInfo(FoeInfo_I* pFoeInfo);
	void FoeInfoUpdate(FoeInfo_I* pFoeInfo);
	void ClearFoeInfo();
	void OffLine();
	int GetFoeOnLineType(unsigned long ulFoeID);
	CString GetFoeName(unsigned long ulFoeID);
	bool IsFoe(const string& strRoleName);
	bool IsFoeBanned(const string& strRoleName);
	map<unsigned long, FoeInfo_I*>::iterator GetFoeBegin()
	{
		return m_FoeMap.begin();
	}
	map<unsigned long, FoeInfo_I*>::iterator GetFoeEnd()
	{
		return m_FoeMap.end();
	}
private:
	CListCtrl m_FoeList;
	map<unsigned long, FoeInfo_I*> m_FoeMap;
	CMenu m_FoeMenu;
	FoeInfo_I* m_CurPosFoe;
	CImageList m_ImgList;

	CBitmap m_BmpSortUp;
	CBitmap m_BmpSortDown;
	CImageList m_SortImgList;
	int m_SortType; //-1:默认排序 0 第一列升序 1第一列降序 2 第二列升序 3 第二列降序
	map<string, FoeInfo_I*> m_BanFoeMap;
public:
	CUsrInfoDlg* m_UsrInfoDlg;

protected:
	virtual BOOL OnInitDialog();
public:
	afx_msg void OnNMRclickFoelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnFoesDetail();
	afx_msg void OnUpdateFoesDetail(CCmdUI *pCmdUI);
	afx_msg void OnFoesPrichat();
	afx_msg void OnUpdateFoesPrichat(CCmdUI *pCmdUI);
	afx_msg void OnFoesCopyname();
	afx_msg void OnUpdateFoesCopyname(CCmdUI *pCmdUI);
	afx_msg void OnFoesBan();
	afx_msg void OnUpdateFoesBan(CCmdUI *pCmdUI);
protected:
	static int CALLBACK FoeInfoSort(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

public:
	afx_msg void OnHdnItemclickFoelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawFoelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkFoelist(NMHDR *pNMHDR, LRESULT *pResult);
};
