﻿// UsrInfoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "UsrInfoDlg.h"
#include "FriendDlg.h"
#include "IRC_UIDlg.h"

// CUsrInfoDlg 对话框

IMPLEMENT_DYNAMIC(CUsrInfoDlg, CDialog)

CUsrInfoDlg::CUsrInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUsrInfoDlg::IDD, pParent)
{

}

CUsrInfoDlg::~CUsrInfoDlg()
{

}

void CUsrInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDT_USRRELATION, m_EdtUsrInfo);
}


BEGIN_MESSAGE_MAP(CUsrInfoDlg, CDialog)
END_MESSAGE_MAP()


// CUsrInfoDlg 消息处理程序

BOOL CUsrInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CUsrInfoDlg::OnOK()
{
	//CDialog::OnOK();
}

void CUsrInfoDlg::OnCancel()
{
	DestroyWindow();
	//CDialog::OnCancel();
}

void CUsrInfoDlg::PostNcDestroy()
{
	CDialog::PostNcDestroy();
	if (m_ParentType == 1)
	{
		CFriendDlg* pFriendDlg = static_cast<CFriendDlg*>(m_Parent);
		pFriendDlg->m_UsrInfoDlg = NULL;
	}
	if (m_ParentType == 2)
	{
		CFoeDlg* pFoeDlg = static_cast<CFoeDlg*>(m_Parent);
		pFoeDlg->m_UsrInfoDlg = NULL;
	}
	delete this;
}

void CUsrInfoDlg::UpdateUsrInfo(FriendInfo_I* pFriendInfo)
{
	CString strName;
	strName.Format(_T("姓名:%s\r\n"), pFriendInfo->nickName);
	m_EdtUsrInfo.SetSel(-1, -1);	
	m_EdtUsrInfo.ReplaceSel(strName);

	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	CString strGenger;
	strGenger.Format(_T("性别:%s\r\n"), pIrcMain->GetIrcInfo()->GetGender(pFriendInfo->playerSex).c_str());
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strGenger);

	CString strJob;
	strJob.Format(_T("职业:%s\r\n"), pIrcMain->GetIrcInfo()->GetClassByClassID(pFriendInfo->playerClass).c_str());
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strJob);

	/*
	CString strRace;
	strRace.Format(_T("种族:%s\r\n"),  m_IrcMain->GetIrcInfo()->GetRaceByRaceID(pPeerUsrInfo->playerRace));
	m_UsrInfo.SetSel(-1, -1);
	m_UsrInfo.ReplaceSel(strRace);
	*/

	CString strLevel;
	strLevel.Format(_T("等级:%d\r\n"), pFriendInfo->playerLevel);
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strLevel);

	CString strFereName;
	strFereName.Format(_T("伴侣:%s\r\n"),  /*pFriendInfo->FereName*/_T("无"));
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strFereName);

	CString strGuildName;
	strGuildName.Format(_T("战盟:%s\r\n"),  pFriendInfo->GuildName);
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strGuildName);

	CHARFORMAT2 cf2;
	ZeroMemory(&cf2, sizeof(CHARFORMAT2));

	cf2.cbSize = sizeof(CHARFORMAT2);
	COLORREF ChatClr;
	ChatClr = RGB(0, 0, 255);

	cf2.dwMask |= CFM_COLOR;
	cf2.crTextColor = ChatClr;

	cf2.dwMask |= CFM_SIZE;
	cf2.yHeight = 200;

	cf2.dwMask |= CFM_FACE;
	strcpy(cf2.szFaceName, "宋体");

	m_EdtUsrInfo.SetSel(0, -1);
	m_EdtUsrInfo.SetSelectionCharFormat(cf2);


	PARAFORMAT pf;
	pf.cbSize = sizeof(pf);
	pf.dwMask = PFM_ALIGNMENT;
	pf.wAlignment = PFA_CENTER;
	m_EdtUsrInfo.SetParaFormat(pf);

	m_EdtUsrInfo.SetSel(-1, -1);	
}

void CUsrInfoDlg::UpdateUsrInfo(FoeInfo_I* pFoeInfo)
{
	CString strName;
	strName.Format(_T("姓名:%s\r\n"), pFoeInfo->nickName);
	m_EdtUsrInfo.SetSel(-1, -1);	
	m_EdtUsrInfo.ReplaceSel(strName);

	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	CString strGenger;
	strGenger.Format(_T("性别:%s\r\n"), pIrcMain->GetIrcInfo()->GetGender(pFoeInfo->playerSex).c_str());
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strGenger);

	CString strJob;
	strJob.Format(_T("职业:%s\r\n"), pIrcMain->GetIrcInfo()->GetClassByClassID(pFoeInfo->playerClass).c_str());
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strJob);

	/*
	CString strRace;
	strRace.Format(_T("种族:%s\r\n"),  m_IrcMain->GetIrcInfo()->GetRaceByRaceID(pPeerUsrInfo->playerRace));
	m_UsrInfo.SetSel(-1, -1);
	m_UsrInfo.ReplaceSel(strRace);
	*/

	CString strLevel;
	strLevel.Format(_T("等级:%d\r\n"), pFoeInfo->playerLevel);
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strLevel);

	CString strFereName;
	strFereName.Format(_T("伴侣:%s\r\n"),  /*pFriendInfo->FereName*/_T("无"));
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strFereName);

	CString strGuildName;
	strGuildName.Format(_T("战盟:%s\r\n"),  pFoeInfo->GuildName);
	m_EdtUsrInfo.SetSel(-1, -1);
	m_EdtUsrInfo.ReplaceSel(strGuildName);

	CHARFORMAT2 cf2;
	ZeroMemory(&cf2, sizeof(CHARFORMAT2));

	cf2.cbSize = sizeof(CHARFORMAT2);
	COLORREF ChatClr;
	ChatClr = RGB(0, 0, 255);

	cf2.dwMask |= CFM_COLOR;
	cf2.crTextColor = ChatClr;

	cf2.dwMask |= CFM_SIZE;
	cf2.yHeight = 200;

	cf2.dwMask |= CFM_FACE;
	strcpy(cf2.szFaceName, "宋体");

	m_EdtUsrInfo.SetSel(0, -1);
	m_EdtUsrInfo.SetSelectionCharFormat(cf2);


	PARAFORMAT pf;
	pf.cbSize = sizeof(pf);
	pf.dwMask = PFM_ALIGNMENT;
	pf.wAlignment = PFA_CENTER;
	m_EdtUsrInfo.SetParaFormat(pf);

	m_EdtUsrInfo.SetSel(-1, -1);
}