﻿#pragma once
#include <Richole.h>
#include "IrcEventSinkImp.h"
#include "WordFilter.h"
#include "Gif.h"
// CIrcEdit
#define WM_NEWPRICHAT WM_APP + 5000
#define WM_DOLINK WM_APP + 5001
#define WM_GETFACE WM_APP + 6000
#define WM_SENDMSG WM_APP + 7000

class CIrcEditCallback: public IRichEditOleCallback
{
public:
	CIrcEditCallback(){m_dwRef = 0;};
	virtual ~CIrcEditCallback(){};
public:
	virtual ULONG STDMETHODCALLTYPE AddRef();
	virtual ULONG STDMETHODCALLTYPE Release();
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, void ** ppvObject);


	 virtual HRESULT STDMETHODCALLTYPE DeleteObject(LPOLEOBJECT lpoleobj);
	 virtual HRESULT  STDMETHODCALLTYPE ContextSensitiveHelp(BOOL fEnterMode);
	 virtual HRESULT STDMETHODCALLTYPE GetClipboardData(CHARRANGE *lpchrg, DWORD reco,  LPDATAOBJECT *lplpdataobj);
	 virtual HRESULT STDMETHODCALLTYPE GetContextMenu(WORD seltype, LPOLEOBJECT lpoleobj, CHARRANGE *lpchrg, HMENU *lphmenu);
	 virtual HRESULT STDMETHODCALLTYPE GetDragDropEffect(BOOL fDrag, DWORD grfKeyState, LPDWORD pdwEffect);
	 virtual HRESULT STDMETHODCALLTYPE GetInPlaceContext(LPOLEINPLACEFRAME *lplpFrame, LPOLEINPLACEUIWINDOW *lplpDoc, LPOLEINPLACEFRAMEINFO lpFrameInfo);
	 virtual HRESULT STDMETHODCALLTYPE GetNewStorage(LPSTORAGE *lplpstg);
	 virtual HRESULT STDMETHODCALLTYPE QueryAcceptData(LPDATAOBJECT lpdataobj, CLIPFORMAT *lpcfFormat, DWORD reco, BOOL fReally, HGLOBAL hMetaPict);
	 virtual HRESULT STDMETHODCALLTYPE QueryInsertObject(LPCLSID lpclsid, LPSTORAGE lpstg, LONG cp);
	 virtual HRESULT STDMETHODCALLTYPE ShowContainerUI(BOOL fShow);
private:
	DWORD m_dwRef;
};

class CIrcEdit : public CRichEditCtrl
{
	DECLARE_DYNAMIC(CIrcEdit)

public:
	CIrcEdit();
	virtual ~CIrcEdit();

	virtual void OnFinalRelease();
public:
	void InsertBmpToEdit(HBITMAP hBitmap, int nIndex, int nCharIndex = -1);
	void InsertFaceToEdit(CGif* pGif, int nIndex, int nCharIndex = -1);
	void AddChat(int nChannelType, CHATMSG* pChatMsg, bool bMe = false, bool bShowRole = true, CBitmap* UserPhoto = NULL);
	void FlushMsg();
	void LoadFilterWord(CWordFilter* pWorldFilter);
	int GetCharCount();
	void AppendMsg(LPCTSTR lpMsg);
	void ClearMsg();
protected:
	DECLARE_MESSAGE_MAP()
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	map<int, LINKINFO> m_LinkInfo;
	CPoint m_msgClickPos;
	vector<MsgInfo> m_DispMsg;
	CWordFilter* m_WorldFilter;
	CIrcEditCallback* m_pCallback;
private:
	void CheckMaxLine();
	void GetLinks(wstring& wstrChat, int nStartIndex, map<int, LINKINFO>& NewLinkInfo);
	void SetChatStyle(int nChannelType, bool bMe, int nStartIndex, int nEndIndex = -1); //0:世界频道消息 1：公会频道消息 2：同城频道消息 3：GM频道消息 4:私聊
	void SetLinkFmt(int nStartIndex, int nEndIndex);
	bool IsReachBottom();
	CString GetMsg();
public:
	afx_msg void OnEnLink(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnMsgfilter(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChange();
	afx_msg void OnEnVscroll();
protected:
	virtual void PreSubclassWindow();
};


