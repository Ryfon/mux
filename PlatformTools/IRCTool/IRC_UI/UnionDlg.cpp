﻿// UnionDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "UnionDlg.h"
#include "IRC_UIDlg.h"

// CUnionDlg 对话框

IMPLEMENT_DYNAMIC(CUnionDlg, CDialog)

CUnionDlg::CUnionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUnionDlg::IDD, pParent)
{
	m_GuildMemMap.clear();
}

CUnionDlg::~CUnionDlg()
{
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		delete Ite->second;
	}
	m_GuildMemMap.clear();
}

void CUnionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_UNIONMEMBERLIST, m_UnionMemberList);
}


BEGIN_MESSAGE_MAP(CUnionDlg, CDialog)
	ON_NOTIFY(NM_RCLICK, IDC_UNIONMEMBERLIST, &CUnionDlg::OnNMRclickUnionlist)
	ON_NOTIFY(NM_DBLCLK, IDC_UNIONMEMBERLIST, &CUnionDlg::OnNMDblclkUnionlist)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_UNIONMEMBERLIST, &CUnionDlg::OnNMCustomdrawUnionlist)
	ON_COMMAND(ID_GUILDS_COPYNAME, &CUnionDlg::OnGuildCopyname)
	ON_COMMAND(ID_GUILDS_BAN, &CUnionDlg::OnGuildBan)
	ON_COMMAND(ID_GUILDS_PRIVATECHAT, &CUnionDlg::OnGuildPrivateChat)
END_MESSAGE_MAP()


// CUnionDlg 消息处理程序

BOOL CUnionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CRect rect;
	m_UnionMemberList.GetClientRect(&rect);
	int nColWidth = rect.Width() / 5;

	m_UnionMemberList.InsertColumn(0, _T("姓名"), LVCFMT_CENTER, nColWidth);
	m_UnionMemberList.InsertColumn(1, _T("职业"), LVCFMT_CENTER, nColWidth);
	m_UnionMemberList.InsertColumn(2, _T("职位"), LVCFMT_CENTER, nColWidth);
	m_UnionMemberList.InsertColumn(3, _T("称号"), LVCFMT_CENTER, nColWidth);
	m_UnionMemberList.InsertColumn(4, _T("等级"), LVCFMT_CENTER, nColWidth);

	ListView_SetExtendedListViewStyle(m_UnionMemberList.m_hWnd, LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);

	m_CurGuildMem = NULL;
	m_UsrInfoDlg = NULL;

	m_GuildMenu.LoadMenu(IDR_GUILDS);


	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CUnionDlg::AddGuildMemInfo(UnionMember_I *pUnionMemInfo)
{
	map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.find(pUnionMemInfo->uiid);
	if (Ite == m_GuildMemMap.end())
	{
		UnionMember_I* pNewGuildMemInfo = new UnionMember_I;
		memcpy(pNewGuildMemInfo, pUnionMemInfo, sizeof(UnionMember_I));
		m_GuildMemMap[pNewGuildMemInfo->uiid] = pNewGuildMemInfo;

		int nIndex;
		int nItemCount = m_UnionMemberList.GetItemCount();
		if (pNewGuildMemInfo->onLine == 2)
		{
			nIndex = m_UnionMemberList.InsertItem(nItemCount, pNewGuildMemInfo->name, 0);
		}
		else
		{
			nIndex = m_UnionMemberList.InsertItem(nItemCount, pNewGuildMemInfo->name, -1);
		}

		CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);

		m_UnionMemberList.SetItemText(nIndex, 1, pIrcMain->GetIrcInfo()->GetClassByClassID(pNewGuildMemInfo->job).c_str()); //职业
		map<int, string>::iterator ItePos = pIrcMain->m_GuildPosList.find(pNewGuildMemInfo->posSeq);
		m_UnionMemberList.SetItemText(nIndex, 2, ItePos != pIrcMain->m_GuildPosList.end() ? ItePos->second.c_str() : _T("")); //职位
		m_UnionMemberList.SetItemText(nIndex, 3, pNewGuildMemInfo->title); //称号
		CString strLevel;
		strLevel.Format(_T("%d"), pNewGuildMemInfo->level);
		m_UnionMemberList.SetItemText(nIndex, 4, strLevel);//等级 	
		m_UnionMemberList.SetItemData(nIndex, (DWORD_PTR)pNewGuildMemInfo); 
		//m_UnionMemberList.SortItems(FoeInfoSort, -1);
	}
	else
	{
		((UnionMember_I*)Ite->second)->onLine = pUnionMemInfo->onLine;
	}
}

void CUnionDlg::DelGuildMem(UINT nMemID)
{
	UnionMember_I* pGuildMemInfo = NULL;
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		if (pGuildMemInfo->uiid == nMemID)
		{
			pGuildMemInfo = Ite->second;
			m_GuildMemMap.erase(Ite);
			break;
		}
	}
	if (pGuildMemInfo == NULL)
	{
		return;
	}
	map<string, UnionMember_I*>::iterator Ite = m_BanGuildMemMap.find(pGuildMemInfo->name);
	if (Ite != m_BanGuildMemMap.end())
	{
		m_BanGuildMemMap.erase(Ite);
	}

	for (int nIndex = 0; nIndex < m_UnionMemberList.GetItemCount(); nIndex++)
	{
		if (pGuildMemInfo->uiid == ((UnionMember_I*)m_UnionMemberList.GetItemData(nIndex))->uiid)
		{
			m_UnionMemberList.DeleteItem(nIndex);
			break;
		}
	}
	delete pGuildMemInfo;
}

void CUnionDlg::OnNMRclickUnionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	CMenu *psub = (CMenu *)m_GuildMenu.GetSubMenu(0);
	CPoint point;
	GetCursorPos(&point);

	m_UnionMemberList.ScreenToClient(&point);
	LVHITTESTINFO lvinfo;
	lvinfo.pt = point;
	lvinfo.flags = LVHT_ABOVE;

	int nIndex = m_UnionMemberList.SubItemHitTest(&lvinfo);
	if (nIndex != -1)
	{
		m_CurGuildMem = reinterpret_cast<UnionMember_I*>(m_UnionMemberList.GetItemData(nIndex));
		m_UnionMemberList.ClientToScreen(&point);
		map<string, UnionMember_I*>::iterator Ite = m_BanGuildMemMap.find(m_CurGuildMem->name);
		psub->ModifyMenu(ID_GUILDS_BAN, MF_BYCOMMAND, ID_GUILDS_BAN, Ite != m_BanGuildMemMap.end() ? _T("解除屏蔽") : _T("屏蔽"));
		if (m_CurGuildMem->onLine == 0)
		{
			psub->EnableMenuItem(ID_GUILDS_PRIVATECHAT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);

		}
		else
		{
			psub->EnableMenuItem(ID_GUILDS_PRIVATECHAT, MF_BYCOMMAND | MF_ENABLED);
		}
		psub->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}
	else
	{
		m_CurGuildMem = NULL;
	}

	*pResult = 0;
}

void CUnionDlg::OnNMDblclkUnionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	CPoint point;
	GetCursorPos(&point);

	m_UnionMemberList.ScreenToClient(&point);
	LVHITTESTINFO lvinfo;
	lvinfo.pt = point;
	lvinfo.flags = LVHT_ABOVE;

	int nIndex = m_UnionMemberList.SubItemHitTest(&lvinfo);
	if (nIndex != -1)
	{
		m_CurGuildMem = reinterpret_cast<UnionMember_I*>(m_UnionMemberList.GetItemData(nIndex));
		m_UnionMemberList.ClientToScreen(&point);
		if (m_CurGuildMem->onLine != 0)
		{
			OnGuildPrivateChat();
		}
	}
	else
	{
		m_CurGuildMem = NULL;
	}
	*pResult = 0;
}

void CUnionDlg::OnNMCustomdrawUnionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = CDRF_DODEFAULT;

	LPNMLVCUSTOMDRAW pLvNMCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);
	switch(pLvNMCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		{
			*pResult = CDRF_NOTIFYITEMDRAW;
		}
		break;
	case CDDS_ITEMPREPAINT:
		{
			*pResult = CDRF_NOTIFYSUBITEMDRAW;
		}
		break;
	case CDDS_ITEMPREPAINT | CDDS_SUBITEM:
		{
			int nItem = static_cast<int>(pLvNMCD->nmcd.dwItemSpec);
			COLORREF clrTextColr;
			UnionMember_I* pGuildMemInfo = reinterpret_cast<UnionMember_I*>(m_UnionMemberList.GetItemData(nItem));
			if (pGuildMemInfo->onLine == 0)
			{
				clrTextColr = RGB(222,222,222);
			}
			else
			{
				clrTextColr = RGB(0, 0, 0);
			}
			pLvNMCD->clrText = clrTextColr;
		}
		break;
	default:
		break;
	}
}

void CUnionDlg::OnGuildCopyname()
{
	if (OpenClipboard())
	{
		HGLOBAL hClipBuffer;
		char* pBuffer;
		EmptyClipboard();
		hClipBuffer = GlobalAlloc(GMEM_DDESHARE, strlen(m_CurGuildMem->name) + 1);
		pBuffer = (char*)GlobalLock(hClipBuffer);
		strcpy(pBuffer, m_CurGuildMem->name);
		GlobalUnlock(hClipBuffer);
		SetClipboardData(CF_TEXT, hClipBuffer);
		CloseClipboard();
	}
}

void CUnionDlg::OnGuildBan()
{
	map<string, UnionMember_I*>::iterator Ite = m_BanGuildMemMap.find(m_CurGuildMem->name);
	if (Ite == m_BanGuildMemMap.end())
	{
		m_BanGuildMemMap[m_CurGuildMem->name] = m_CurGuildMem;
		m_GuildMenu.ModifyMenu(ID_GUILDS_BAN, MF_BYCOMMAND, ID_GUILDS_BAN, _T("解除屏蔽"));
	}
	else
	{
		m_BanGuildMemMap.erase(Ite);
		m_GuildMenu.ModifyMenu(ID_GUILDS_BAN, MF_BYCOMMAND, ID_GUILDS_BAN, _T("屏蔽"));
	}
}

void CUnionDlg::OnGuildPrivateChat()
{
	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	if (m_CurGuildMem && strcmp(m_CurGuildMem->name, pIrcMain->GetRoleName().c_str()))
	{
		pIrcMain->NewPrivateChat(m_CurGuildMem->name);
	}
}

void CUnionDlg::ClearGuildMemInfo()
{
	m_UnionMemberList.DeleteAllItems();
	m_CurGuildMem = NULL;
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		delete Ite->second;
	}
	m_GuildMemMap.clear();
	m_BanGuildMemMap.clear();
}

void CUnionDlg::Offline()
{
	//把所有战盟成员都修改成离线状态
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		UnionMember_I* pGuildMem = Ite->second;
		GuildMemOnOffline(pGuildMem->uiid, false);
	}
}

void CUnionDlg::GuildMemOnOffline(UINT nMemID, bool bOnline)
{
	UnionMember_I* pGuildMemInfo = NULL;
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		if ((Ite->second)->uiid == nMemID)
		{
			pGuildMemInfo = Ite->second;
			pGuildMemInfo->onLine = bOnline;
			break;
		}
	}
}

bool CUnionDlg::IsBanned(const string& strRoleName)
{
	map<string, UnionMember_I*>::iterator Ite = m_BanGuildMemMap.find(strRoleName);
	return Ite != m_BanGuildMemMap.end();
}

bool CUnionDlg::IsBanned()
{
	UnionMember_I* pGuildMemInfo = NULL;
	CIRC_UIDlg* pMainDlg = (CIRC_UIDlg*)AfxGetApp()->GetMainWnd();
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		if (!strcmp(pMainDlg->GetRoleName().c_str(), Ite->second->name))
		{
			return Ite->second->forbid == 1; 
		}
	}
	return false;
}

void CUnionDlg::GuildBanChat(const char* szRoleName)
{
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		if (!strcmp(Ite->second->name, szRoleName))
		{
			Ite->second->forbid = Ite->second->forbid == 0 ? 1 : 0;
			break;
		}
	}
}

void CUnionDlg::GuildMemberTitleChange(const char *szMemberName, const char *szTitleName)
{
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		if (!strcmp(Ite->second->name, szMemberName))
		{
			strcpy(Ite->second->title, szTitleName);
			break;
		}
	}
	
	for (int nIndex = 0; nIndex < m_UnionMemberList.GetItemCount(); nIndex++)
	{
		if (!strcmp(szMemberName, ((UnionMember_I*)m_UnionMemberList.GetItemData(nIndex))->name))
		{
			m_UnionMemberList.SetItemText(nIndex, 3, szTitleName);
			break;
		}
	}
	
}

void CUnionDlg::UpdateGuildPos()
{
	for (int nIndex = 0; nIndex < m_UnionMemberList.GetItemCount(); nIndex++)
	{
		UnionMember_I* pGuidMemInfo = (UnionMember_I*)(m_UnionMemberList.GetItemData(nIndex));
		CIRC_UIDlg* pIrcMain = (CIRC_UIDlg*)AfxGetApp()->GetMainWnd();
		map<int, string>::iterator ItePos = pIrcMain->m_GuildPosList.find(pGuidMemInfo->posSeq);
		m_UnionMemberList.SetItemText(nIndex, 2, ItePos != pIrcMain->m_GuildPosList.end() ? ItePos->second.c_str() : _T("")); //职位
	}
}

void CUnionDlg::UpdateGuildMemPos(const char *szMemberName, USHORT nPos)
{
	for (map<unsigned long, UnionMember_I*>::iterator Ite = m_GuildMemMap.begin(); Ite != m_GuildMemMap.end(); Ite++)
	{
		if (!strcmp(Ite->second->name, szMemberName))
		{
			Ite->second->posSeq = nPos;
			break;
		}
	}

	for (int nIndex = 0; nIndex < m_UnionMemberList.GetItemCount(); nIndex++)
	{
		if (!strcmp(szMemberName, ((UnionMember_I*)m_UnionMemberList.GetItemData(nIndex))->name))
		{
			CIRC_UIDlg* pIrcMain = (CIRC_UIDlg*)AfxGetApp()->GetMainWnd();
			map<int, string>::iterator ItePos = pIrcMain->m_GuildPosList.find(nPos);
			m_UnionMemberList.SetItemText(nIndex, 2, ItePos != pIrcMain->m_GuildPosList.end() ? ItePos->second.c_str() : _T("")); //职位
			break;
		}
	}

}


