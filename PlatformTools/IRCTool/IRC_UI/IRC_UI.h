﻿// IRC_UI.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "Config.h"
#include "SystemTray.h"
// CIRC_UIApp:
// 有关此类的实现，请参阅 IRC_UI.cpp
//
//  [6/24/2009 hemeng]
#include "UISourceManager.h"

// begin [7/23/2009 hemeng]
#include "PackageSourceManager.h"
// end [7/23/2009 hemeng]

class CIrcCmdInfo: public CCommandLineInfo
{
public:
	CIrcCmdInfo();
	virtual ~CIrcCmdInfo();
public:
	virtual void ParseParam(const char* pszParam, BOOL bFlag, BOOL bLast);
protected:

public:
	CString m_IpAddr;
	USHORT m_Port;
	BYTE m_ServerType;
	BYTE m_ParamCount;
};

class CIRC_UIApp : public CWinApp
{
public:
	CIRC_UIApp();

// 重写
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
// 实现

	DECLARE_MESSAGE_MAP()
private:
	ULONG_PTR m_gdiplusToken;
	HICON m_hIcon;

public:
	CConfig* m_Config;
	CIrcCmdInfo m_CmdInfo;
};

extern CIRC_UIApp theApp;