﻿#include "StdAfx.h"
#include "UIIconSource.h"
#include "Helper.h"

CUIIconSource::CUIIconSource(void)
{
	m_strSourcType = "ico";
}

CUIIconSource::~CUIIconSource(void)
{
}

bool CUIIconSource::Initial(string strPath)
{
	if (strPath == "")
	{
		return false;
	}

	if(!CHelper::LoadIconFromFile(strPath.c_str(),m_hdSource))
	{
		return false;
	}

	return true;
}
