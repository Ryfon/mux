﻿// LoginDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "LoginDlg.h"
#include "IRC_UIDlg.h"
#include "Helper.h"
#include "PackageSourceManager.h"

IMPLEMENT_DYNAMIC(CLoginDlg, CDialog)

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoginDlg::IDD, pParent)
	, m_Account(_T(""))
	, m_Pwd(_T(""))
	, m_RoleName(_T(""))
	, m_ServerIp(_T(""))
	, m_Port(0)
	, m_GateType(1)
{

}

CLoginDlg::~CLoginDlg()
{
		
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDT_ACCOUNT, m_Account);
	DDX_Text(pDX, IDC_EDT_PWD, m_Pwd);
	DDX_Control(pDX, IDC_LST_ROLEINFO, m_RoleList);
}


BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_OK, &CLoginDlg::OnBnClickedBtnOk)
END_MESSAGE_MAP()


// CLoginDlg 消息处理程序

void CLoginDlg::OnBnClickedBtnOk()
{
	UpdateData(TRUE);
	//调用接口进行登录，同时启动一个定时器，如果在给定的时间内登陆没有成功，报告说登录超时调用
	CComboBox* pComboBox = (CComboBox*)(GetDlgItem(IDC_COMBO_SERVER));
	int nIndex = pComboBox->GetCurSel();
	if (nIndex == -1)
	{
		nIndex = 0;
	}
	CString strServer;
	pComboBox->GetWindowText(strServer);
	m_GateType = 1;
	int nPos = strServer.Find(_T(" "));
	m_ServerIp = strServer.Left(nPos);
	strServer.Delete(0, nPos);
	m_Port = atoi(strServer);
	m_GateType = (int)(pComboBox->GetItemData(nIndex));

	CIRC_UIDlg* pMainDlg = (CIRC_UIDlg*)(AfxGetApp()->m_pMainWnd);
	if (m_Stage == 0)
	{
		pMainDlg->LogIn();
	}
	if (m_Stage == 1)
	{
		POSITION pos = m_RoleList.GetFirstSelectedItemPosition();
		if (pos)
		{
			int nIndex = m_RoleList.GetNextSelectedItem(pos);
			pMainDlg->GetIrcInfo()->EnterWorld(m_RoleList.GetItemData(nIndex));
			pMainDlg->m_RoleName = m_RoleList.GetItemText(nIndex, 0);
		}
	}
	GetDlgItem(IDC_BTN_OK)->EnableWindow(FALSE);
}

void CLoginDlg::OnLogin()
{
	CDialog::OnOK();
}
void CLoginDlg::OnOK()
{
	// TODO: 在此添加专用代码和/或调用基类

	//CDialog::OnOK();
}

BOOL CLoginDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// TODO:  在此添加额外的初始化
	m_Stage = 0;
	if(!InitSource())
	{
		OutputDebugString("Fail to load source");
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


bool CLoginDlg::InitSource()
{
	string strPackPath = CHelper::GetAppWorkDir() + ".Data\\Config\\SysConfig.xml";
	
	char* pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath);

	if (pBuf == NULL)
	{
		return false;
	}

	TiXmlDocument xmlDoc;
	xmlDoc.Parse(pBuf);
	TiXmlElement* pXmlRoot = xmlDoc.RootElement();
	CComboBox* pComboBox = (CComboBox*)(GetDlgItem(IDC_COMBO_SERVER));
	for (const TiXmlElement* pXmlServer = pXmlRoot->FirstChildElement(); pXmlServer; pXmlServer = pXmlServer->NextSiblingElement())
	{
		CString strItem;
		strItem.Format(_T("%s %s"), pXmlServer->Attribute("IP"), pXmlServer->Attribute("Port"));
		int nIndex = pComboBox->AddString(strItem);
		pComboBox->SetItemData(nIndex, (DWORD_PTR)1);

	}
	delete [] pBuf;
	CIRC_UIApp* pApp = static_cast<CIRC_UIApp*>(AfxGetApp());		
	if (pApp->m_CmdInfo.m_ParamCount == 3)
	{
		CString strItem;
		strItem.Format(_T("%s %d"), pApp->m_CmdInfo.m_IpAddr, pApp->m_CmdInfo.m_Port);
		int nIndex = pComboBox->AddString(strItem);
		pComboBox->SetItemData(nIndex, (DWORD_PTR)pApp->m_CmdInfo.m_ServerType);
	}
	if (pComboBox->GetCount() > 0)
	{
		pComboBox->SetCurSel(0);
	}
	return true;
}

void CLoginDlg::ProcessRole(unsigned int uiRoleID, const char *strRoleName)
{
	if (!m_RoleList.IsWindowVisible())
	{
		GetDlgItem(IDC_COMBO_SERVER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDT_PWD)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDT_ACCOUNT)->ShowWindow(SW_HIDE);
		m_RoleList.ShowWindow(SW_SHOW);
		CRect rect;
		m_RoleList.GetClientRect(&rect);
		int nColWidth = rect.Width();
		m_RoleList.InsertColumn(0, _T("角色名"), LVCFMT_CENTER, nColWidth);
		CButton* pBnt = (CButton*)(GetDlgItem(IDC_BTN_OK));
		m_Stage = 1;
		pBnt->SetWindowText(_T("进入游戏"));
	}
	if (uiRoleID != 0)
	{
		int nIndex = m_RoleList.InsertItem(m_RoleList.GetItemCount(), strRoleName);
		m_RoleList.SetItemData(nIndex, (DWORD_PTR)uiRoleID);
	}
	else
	{
		if (m_RoleList.GetItemCount() == 0)
		{
			GetDlgItem(IDC_BTN_OK)->EnableWindow(FALSE);
		}
		else
		{
			m_RoleList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
			GetDlgItem(IDC_BTN_OK)->EnableWindow();
		}
	}
}