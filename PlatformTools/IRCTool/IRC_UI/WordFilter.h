﻿#ifndef WORDFILTER_H
#define WORDFILTER_H

class CWordFilter
{
public:
	CWordFilter(void);
	virtual ~CWordFilter(void);
public:
	int Parse(const char* szBuf);
	int FilterStr(wstring& wstr, wstring wstrReplace);

private:
	vector<wstring> m_KeyWords;
};

#endif






