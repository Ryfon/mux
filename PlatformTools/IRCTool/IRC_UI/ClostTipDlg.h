﻿#pragma once

// CClostTipDlg 对话框

class CClostTipDlg : public CDialog
{
	DECLARE_DYNAMIC(CClostTipDlg)

public:
	CClostTipDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CClostTipDlg();

// 对话框数据
	enum { IDD = IDD_DLG_CLOSE_TIP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	int m_ClostType;
	bool m_bIsDispDlg;
	afx_msg void OnBnClickedBtnOk();
	afx_msg void OnBnClickedBtnCancel();
	virtual BOOL OnInitDialog();
};
