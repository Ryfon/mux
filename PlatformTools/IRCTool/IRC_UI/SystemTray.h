﻿/** @file SystemTray.h 
@brief 系统托盘类
<pre>
*	Copyright (c) 2008，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-12-01
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef SYSTEMTRAY_H
#define SYSTEMTRAY_H

#ifdef NOTIFYICONDATA_V1_SIZE   // If NOTIFYICONDATA_V1_SIZE, then we can use fun stuff
#define SYSTEMTRAY_USEW2K
#else
#define NIIF_NONE 0
#endif

// #include <afxwin.h>
#include <afxtempl.h>
#include <afxdisp.h>    // COleDateTime
//托盘通知消息
#define	WM_ICON_NOTIFY			WM_APP + 1

class CSystemTray: public CWnd
{
public:
	CSystemTray(void);
	CSystemTray(CWnd* pWnd, UINT uCallbackMessage, LPCTSTR szTip, HICON icon, UINT uID, 
		BOOL bhidden = FALSE,
		LPCTSTR szBalloonTip = NULL, LPCTSTR szBalloonTitle = NULL, 
		DWORD dwBalloonIcon = NIIF_NONE, UINT uBalloonTimeout = 10);  
	virtual ~CSystemTray(void);
	DECLARE_DYNAMIC(CSystemTray);
public:
	BOOL Enabled() { return TRUE;}
	BOOL Visible() {return TRUE;}

	// Create the tray icon
	BOOL Create(CWnd* pParent, UINT uCallbackMessage, LPCTSTR szTip, HICON icon, UINT uID,
		BOOL bHidden = FALSE,
		LPCTSTR szBalloonTip = NULL, LPCTSTR szBalloonTitle = NULL, 
		DWORD dwBalloonIcon = NIIF_NONE, UINT uBalloonTimeout = 10);

	// Change or retrieve the Tooltip text
	BOOL    SetTooltipText(LPCTSTR pszTooltipText);
	BOOL    SetTooltipText(UINT nID);
	CString GetTooltipText() const;

	// Change or retrieve the icon displayed
	BOOL  SetIcon(HICON hIcon);
	BOOL  SetIcon(LPCTSTR lpszIconName);
	BOOL  SetIcon(UINT nIDResource);
	BOOL  SetStandardIcon(LPCTSTR lpIconName);
	BOOL  SetStandardIcon(UINT nIDResource);
	HICON GetIcon() const;

	void  SetFocus();
	BOOL  HideIcon();
	BOOL  ShowIcon();
	BOOL  AddIcon();
	BOOL  RemoveIcon();
	BOOL  MoveToRight();

	BOOL ShowBalloon(LPCTSTR szText, LPCTSTR szTitle = NULL,
		DWORD dwIcon = NIIF_NONE, UINT uTimeout = 10);

	// For icon animation
	BOOL  SetIconList(UINT uFirstIconID, UINT uLastIconID); 
	BOOL  SetIconList(HICON* pHIconList, UINT nNumIcons); 
	BOOL  Animate(UINT nDelayMilliSeconds, int nNumSeconds = -1);
	BOOL  StepAnimation();
	BOOL  StopAnimation();

	// Change menu default item
	void GetMenuDefaultItem(UINT& uItem, BOOL& bByPos);
	BOOL SetMenuDefaultItem(UINT uItem, BOOL bByPos);

	// Change or retrieve the window to send notification messages to
	BOOL  SetNotificationWnd(CWnd* pNotifyWnd);
	CWnd* GetNotificationWnd() const;

	// Change or retrieve the window to send menu commands to
	BOOL  SetTargetWnd(CWnd* pTargetWnd);
	CWnd* GetTargetWnd() const;

	// Change or retrieve  notification messages sent to the window
	BOOL  SetCallbackMessage(UINT uCallbackMessage);
	UINT  GetCallbackMessage() const;

	UINT_PTR GetTimerID() const   { return m_nTimerID; }

	// Static functions
public:
	static void MinimiseToTray(CWnd* pWnd, BOOL bForceAnimation = FALSE);
	static void MaximiseFromTray(CWnd* pWnd, BOOL bForceAnimation = FALSE);

public:
	// Default handler for tray notification message
	virtual LRESULT OnTrayNotification(WPARAM uID, LPARAM lEvent);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSystemTray)
protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void Initialise();
	void InstallIconPending();

	virtual void CustomizeMenu(CMenu*) {}	// Used for customizing the menu

	// Implementation
protected:
	NOTIFYICONDATA  m_tnd;
	BOOL            m_bEnabled;         // does O/S support tray icon?
	BOOL            m_bHidden;          // Has the icon been hidden?
	BOOL            m_bRemoved;         // Has the icon been removed?
	BOOL            m_bShowIconPending; // Show the icon once tha taskbar has been created
	BOOL            m_bWin2K;           // Use new W2K features?
	CWnd*           m_pTargetWnd;       // Window that menu commands are sent

	CArray<HICON, HICON> m_IconList;
	UINT_PTR     m_uIDTimer;
	INT_PTR      m_nCurrentIcon;
	COleDateTime m_StartTime;
	UINT         m_nAnimationPeriod;
	HICON        m_hSavedIcon;
	UINT         m_DefaultMenuItemID;
	BOOL         m_DefaultMenuItemByPos;
	UINT         m_uCreationFlags;

	// Static data
protected:
	static BOOL RemoveTaskbarIcon(CWnd* pWnd);

	static const UINT m_nTimerID;
	static UINT  m_nMaxTooltipLength;
	static const UINT m_nTaskbarCreatedMsg;
	static CWnd  m_wndInvisible;

	static BOOL GetW2K();
	static void GetTrayWndRect(LPRECT lprect);
	static BOOL GetDoWndAnimation();
	// Generated message map functions
protected:
	//{{AFX_MSG(CSystemTray)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	//}}AFX_MSG
	LRESULT OnTaskbarCreated(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

#endif