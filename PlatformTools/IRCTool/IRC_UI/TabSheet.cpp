﻿// TabSheet.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "TabSheet.h"


// CTabSheet

IMPLEMENT_DYNAMIC(CTabSheet, CTabCtrl)

CTabSheet::CTabSheet()
{
	m_CurPageIndex = 0;
}

CTabSheet::~CTabSheet()
{

}


BEGIN_MESSAGE_MAP(CTabSheet, CTabCtrl)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

BOOL CTabSheet::AddPage(LPCTSTR strTitle, CDialog* pDialog, UINT ID)
{
	DLGINFO dlgInfo;
	dlgInfo.strTitle = strTitle;
	dlgInfo.DlgID = ID;
	dlgInfo.pDlg = pDialog;
	m_pPages.push_back(dlgInfo);
	pDialog->Create(ID, this->GetParent());
	InsertItem(m_pPages.size() - 1, strTitle);
	SetRect();
	return TRUE;
}

void CTabSheet::RemovePage(LPCTSTR strTitle)
{
	for (size_t nIndex = 0; nIndex < m_pPages.size(); nIndex++)
	{
		if (stricmp(strTitle, m_pPages[nIndex].strTitle.c_str()) == 0)
		{
			if (m_CurPageIndex == nIndex)
			{
				SetCurSel(0);
			}
			DeleteItem(nIndex);
			m_pPages[nIndex].pDlg->DestroyWindow();
			m_pPages.erase(m_pPages.begin() + nIndex);
			break;
		}
	}
}
void CTabSheet::SetRect()
{
	CRect tabRect, ItemRect;
	int nX, nY, nXc, nYc;

	GetClientRect(&tabRect);
	GetItemRect(0, &ItemRect);

	nX = ItemRect.left;
	nY = ItemRect.bottom + 1;
	nXc = tabRect.right - ItemRect.left + 2;
	CRect DlgRect;
	m_pPages[0].pDlg->GetClientRect(&DlgRect);
	nYc = DlgRect.bottom - nY - 2;

	m_pPages[0].pDlg->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);
	for (int nIndex = 1; nIndex < m_pPages.size(); nIndex++)
	{
		m_pPages[nIndex].pDlg->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);
	}
}

void CTabSheet::Show()
{
	m_pPages[0].pDlg->ShowWindow(SW_SHOW);
	for (int nIndex = 1; nIndex < m_pPages.size(); nIndex++)
	{
		m_pPages[nIndex].pDlg->ShowWindow(SW_HIDE);
	}
}

int CTabSheet::GetCurSel()
{
	return CTabCtrl::GetCurSel();
}

int CTabSheet::SetCurSel(int nItem)
{
	if (nItem < 0 || nItem >= m_pPages.size())
	{
		return -1;
	}

	int nRet = m_CurPageIndex;
	if (m_CurPageIndex != nItem)
	{
		m_pPages[m_CurPageIndex].pDlg->ShowWindow(SW_HIDE);
		m_CurPageIndex = nItem;
		m_pPages[m_CurPageIndex].pDlg->ShowWindow(SW_SHOW);
		CTabCtrl::SetCurSel(m_CurPageIndex);
	}
	return nRet;
}
// CTabSheet 消息处理程序

void CTabSheet::OnLButtonDown(UINT nFlags, CPoint point)
{
	CTabCtrl::OnLButtonDown(nFlags, point);
	int nClickPageIndex = GetCurFocus();
	if (m_CurPageIndex != nClickPageIndex)
	{
		SetCurSel(nClickPageIndex);
	}
}

