﻿#pragma once

class CHelper
{
public:
	CHelper(void);
	~CHelper(void);

	// 某些特殊bmp在载入时不需要创建DIBSECTION，则设置bSpecial为false
	static BOOL LoadBitmapFromBMPFile(LPCTSTR szFileName, HANDLE & phBitmap, bool bSpecila = false);
	static BOOL	LoadIconFromFile(LPCTSTR szFileName, HANDLE & phIcon);
	// icolor : 如果图像为32位，则代表alpah值，icolor为-1
	static void DrawBitmap(CPaintDC * pDC, CRect *pRect, HBITMAP phBitMap, int iColor);
	static string GetAppWorkDir();
	static bool IsRoot(string strFilePath);
	static string RepleaceString(char chToken, char chDesToken,const string strSrc);

private:
	static void _SplitString( const char * szStr, vector< string >& vStrs, char chToken );
};
