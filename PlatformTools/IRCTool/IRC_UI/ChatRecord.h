﻿#pragma once
#include "afxcmn.h"
#include "IRCIntf.h"
#include "IRC_UIDlg.h"
#include "LinkInfoDlg.h"
#include "IconPicker.h"
#include "IrcEdit.h"
#include "ControlSizer.h"

// CChatRecord 对话框

typedef vector<ChatLog*> VECTCHATLOG;
class CChatRecord : public CDialog
{
	DECLARE_DYNAMIC(CChatRecord)

public:
	CChatRecord(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CChatRecord();

// 对话框数据
	enum { IDD = IDD_DLGCHATREC };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
private:
	CString m_strSearche;
public:
	afx_msg void OnTvnSelchangedTvGroups(NMHDR *pNMHDR, LRESULT *pResult);
private:
	CIconPicker m_Face;
	CTreeCtrl m_tvGroups;
	CListCtrl m_lvChat;
	CIrcEdit m_edtChatDesc;

	map<string, HTREEITEM> m_PriItems; //好友、仇人、黑名单对应的tv项
	HTREEITEM m_StrangerItem;
	
	VECTCHATLOG m_WorldChats;
	VECTCHATLOG m_GuildChats;
	VECTCHATLOG m_GMChats;
	VECTCHATLOG m_SameChats;
	VECTCHATLOG m_GroupChats;
	VECTCHATLOG m_ChatGroupChats;
	VECTCHATLOG m_SystemChats;
	VECTCHATLOG m_CommonChats;

	VECTCHATLOG m_SearchChats;

	VECTCHATLOG* m_CurChats;

	map<HTREEITEM, VECTCHATLOG> m_PrivateChats; //私聊用户－－聊天信息影射
	vector<CControlSizer> m_ControlSizer;
	int m_PageIndex;
	int m_PageCount;
	int m_ItemCount;
	bool m_InSearch;
protected:
	virtual void PostNcDestroy();
	virtual void OnCancel();
	virtual void OnOK();
private:
	void DisplayChatRec(VECTCHATLOG* pvectLog);
	void DisplayPage(int nIndex);
	void CreateFaceIcons();
	void DoLink(LINKINFO& LinkInfo);
	void SearchRecords(HTREEITEM ht, CString strKey);
	int Locate(ChatLog* pChatLog);
public:
	void AddPrivateChat(ChatLog* pChatLog);
	void AddWorldChat(ChatLog* pChatLog);
	void AddGuildChat(ChatLog* pChatLog);
	void AddCommonChat(ChatLog* pChatLog);
	void AddSystemChat(ChatLog* pChatLog);
	void AddTeamChat(ChatLog* pChatLog);
	void AddChatGroupChat(ChatLog* pChatLog);
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnSearch();
	afx_msg void OnNMClickTvGroups(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickLvChat(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnLastpage();
	afx_msg void OnBnClickedBtnNextpage();
	afx_msg void OnBnClickedBtnPrepage();
	afx_msg void OnBnClickedBtnFirstpage();
	afx_msg void OnCbnSelchangeComboPages();

	afx_msg LRESULT OnLink(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetFace(WPARAM wParam, LPARAM lParam);

public:
	CLinkInfoDlg* m_LinkInfoDlg;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnNMDblclkLvChat(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
