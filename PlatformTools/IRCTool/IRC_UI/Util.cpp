﻿#include "StdAfx.h"
#include <afxole.h>
#include <afxodlgs.h>
#include <afxpriv.h>
#include <Richole.h>
#include "Util.h"

BOOL CUtil::CopyToClipBoard(CString& strData, CWnd& pWndOwner)
{
	BOOL bRet = FALSE;
	if (OpenClipboard(pWndOwner.GetSafeHwnd()))
	{
		if (!EmptyClipboard())
		{
			CloseClipboard();
			return bRet;
		}

		HGLOBAL hClipBuffer;
		hClipBuffer = GlobalAlloc(GMEM_MOVEABLE, (strData.GetLength() + 1) * sizeof(TCHAR));
		char* pBuf;
		pBuf = (char*)GlobalLock(hClipBuffer);
		ZeroMemory(pBuf, (strData.GetLength() + 1) * sizeof(TCHAR));
		memcpy(pBuf, strData, strData.GetLength());
		GlobalUnlock(hClipBuffer);
		SetClipboardData(CF_TEXT, hClipBuffer);
		CloseClipboard();
	}
	return bRet;
}

CString CUtil::GetSel(CRichEditCtrl &rch)
{
	CString strSelData(_T(""));
	long nSelStart, nSelEnd;
	rch.GetSel(nSelStart, nSelEnd);
	strSelData = rch.GetSelText();

	USES_CONVERSION;
	LPWSTR lpwstrMsg = A2W(strSelData);
	wstring wstrMsg(lpwstrMsg);
	wstring wstrNewMsg;

	int nPos = 0;
	for (int nIndex = 0, nMsgPos = 0; nIndex < nSelEnd - nSelStart; nIndex++)
	{
		int nCp = nSelStart + nIndex;
		REOBJECT reObject;
		ZeroMemory(&reObject, sizeof(reObject));
		reObject.cbStruct = sizeof(REOBJECT);
		reObject.cp = nCp;
		LPRICHEDITOLE lpRichEditOle = rch.GetIRichEditOle();
		HRESULT hResult = lpRichEditOle->GetObject(REO_IOB_USE_CP, &reObject, REO_GETOBJ_ALL_INTERFACES);
		if (hResult == S_OK) 
		{
			//-1表示是IRC表示图标
			if (reObject.dwUser != -1)
			{
				CString strFace;
				strFace.Format(_T("%s%03d"), _T("&"), reObject.dwUser);
				wstring wstrFace = A2W(strFace);
				wstrNewMsg += wstrMsg.substr(nPos, nIndex - nPos);
				wstrNewMsg += wstrFace;
			}
			nPos = nIndex + 1;
		}
		else if(nIndex == nSelEnd - nSelStart - 1 && nPos != nIndex + 1)
		{
			wstrNewMsg += wstrMsg.substr(nPos);
		}
		lpRichEditOle->Release();
	}

	//去掉换行符
	strSelData = CW2A(wstrNewMsg.c_str());
	strSelData.Replace(_T("\r\n"), _T(" "));
	strSelData.Replace(VK_RETURN, ' ');
	return strSelData;
}

BOOL CUtil::PasteFromClipBoard(CRichEditCtrl &rchTo, CWnd &pWndOw)
{
	BOOL bRet = FALSE;
	CString strClipBoardData(_T(""));
	if (OpenClipboard(pWndOw.GetSafeHwnd()))
	{
		HANDLE hData = GetClipboardData(CF_TEXT);
		if (hData == NULL)
		{
			CloseClipboard();
			return bRet;
		}
		char* pBuf = NULL;
		pBuf = (char*)GlobalLock(hData);
		strClipBoardData = pBuf;
		GlobalUnlock(hData);
		CloseClipboard();
	}
	if (strClipBoardData.IsEmpty())
	{
		return bRet;
	}
	//去掉换行符
	strClipBoardData.Replace(_T("\r\n"), _T(" "));
	strClipBoardData.Replace(VK_RETURN, ' ');
	//判断是否超过限制的范围
	CString strMsg;
	rchTo.GetWindowText(strMsg);

	USES_CONVERSION;
	LPWSTR lpwstrMsg = A2W(strMsg);
	wstring wstrMsg(lpwstrMsg);
	LPRICHEDITOLE lpRichEditOle = rchTo.GetIRichEditOle();
	int nAvaLen = rchTo.GetLimitText() - (int)wstrMsg.length() - lpRichEditOle->GetObjectCount();
	lpRichEditOle->Release();
	LPWSTR lpwstrClipBoardData = A2W(strClipBoardData);
	wstring wstrClipBoardData(lpwstrClipBoardData);
	strMsg = CW2A(wstrClipBoardData.substr(0, nAvaLen).c_str());
	rchTo.ReplaceSel(strMsg);
	//显示截断窗口
	return bRet;
}

CString CUtil::GetCurTime()
{
	SYSTEMTIME sysTime;
	GetLocalTime(&sysTime);
	CString strCurTime;
	strCurTime.Format("%02d:%02d:%02d", sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
	return strCurTime;
}
