﻿// IRC_UIDlg.h : 头文件
//

#pragma once


#include "SystemTray.h"
#include "IrcEventSinkImp.h"
#include "UserRelation.h"
#include "PriChatDlg.h"
#include "stylebutton.h"
#include "TaskbarNotifier.h"
#include "IconPicker.h"
#include "afxwin.h"
#include "ChannelFileter.h"
#include "SysConfig.h"
#include "SysConfig.h"
#include "LinkInfoDlg.h"
#include "btnst.h"
#include "IrcEdit.h"
#include "WordFilter.h"

class CChatRecord;
class CPriChatDlg;
class CLoginDlg;

struct MSGTIPSITEM
{
	int nType; //0 频道聊天 1私聊
	string strRoleName;
	vector<CHATMSG*> Msgs;
};

struct CHANNELMSG
{
	int nType; //频道标志
	string strRoleName;
	string strChat;
};

typedef vector<MSGTIPSITEM*> MSGTIPSLIST;
class CPackage;

enum IRCSTATUS {IRC_UNLOGIN = 0, IRC_ONLINE, IRC_OFFLINE};

struct TRAYTIPSINFO 
{
	unsigned int ID;
	char                	nickName[MAX_NAME_SIZE];        	//好友伲称
	char                	isOnline;                     	//是否在线
	unsigned char TipType; //0:好友 1：仇人
	unsigned char ClientType; // 0:client 1:irc
};

// CIRC_UIDlg 对话框
// modify [6/25/2009 hemeng]
class CIRC_UIDlg : public CDialog
{
// 构造
public:
	CIRC_UIDlg(CWnd* pParent = NULL);	// 标准构造函数
// 对话框数据
	enum { IDD = IDD_IRC_UI_DIALOG};
	enum {TRAYTIPSNUM = 4};
	// 频道选择 [6/26/2009 hemeng]
	enum CHANNEL_SEL 
	{
		CS_WORLD = 0,	// 世界频道
		CS_UNION,		// 工会频道
		CS_CITY,		// 同城频道
		CS_GM,			// gm频道		

		CS_COUNT
	};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
	bool SetButtonSTSource(CButtonST& kButton,string strDes0, string strDes1 = "" );

// 实现
protected:
	HICON m_hIcon; //频道聊天界面图标
	HICON m_OffLineIcon;
	HICON m_OnLineIcon;
	HICON m_ChannelIcon[2]; //频道闪动
	HICON m_PrivateIcon[2]; //私聊闪动　
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	CIRCIntf* m_IRCIntf;
	CIRCEventSink* m_IrcEventSinker;
	CSystemTray m_SysTray;
	bool m_isAtTray;
	// begin [7/23/2009 hemeng]
	bool m_bMinMode;
	int  m_iOrgDlgHeight;
	int	 m_iOrgDlgWidth;
	// end [7/23/2009 hemeng]
protected:
	afx_msg LRESULT OnLogin(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPrivateChat(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnWorldChat(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFriendGroupInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFriendInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFriendInfoUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFoeInfoUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFoeInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnBlackItemInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReadPriChat(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReadWorldChat(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPeerUsrInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnIRCError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChatError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRoleInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLink(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNewPriChat(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetFace(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSendMsg(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnGuild(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildDismiss(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildDelMem(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildMember(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildMsg(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildAddMem(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildMemOffOnline(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildBanChat(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildMemberTitleChange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildPosUpdateNty(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGuildMemPosChange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReadGuildChat(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTaskNotifierClick(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTrayMenuPop(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReadCommomMsg(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReadSystemMsg(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReadTeamMsg(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReadChatGroupMsg(WPARAM wParam, LPARAM lParam);

public:
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnBnClickedSdTray();
	/*StyleButton m_btnTray;
	StyleButton m_btnMin;
	StyleButton m_btnClose;*/

public:
	void CreateButtons();
	afx_msg void OnBnClickedUserlist();
	afx_msg void OnBnClickedFilter();
	afx_msg void OnBnClickedSearch();
	afx_msg void OnBnClickedGotogame();
	CButton m_btnList;
	CButton m_btnFilter;
	CButton m_btnSearch;
	CButton m_btnGoToGame;
public:
	CIRCIntf* GetIrcInfo()
	{
		return m_IRCIntf;
	}
	string GetRoleName() const
	{
		return m_RoleName;
	}
	void RemovePriChat(CString strPeerName);
	void FilterChannel();
private:
	void SetButtonStyle(StyleButton& aButton, LPCTSTR szButtonCaption);
	void SplashTray();
public:
	CIrcEdit m_Advisement;

	afx_msg void OnSysconfig();
	afx_msg void OnOpenirc();
protected:
	bool m_bIsDragging;
	POINT m_ptCurPoint;
	CRect m_rcCurRect;
	CImageList m_ImgList;
public:
	CBitmap m_IrcFlag;
private:
	// delete [6/24/2009 hemeng]
	//void CreateBkImgs();

	void CreateFaceIcon();
	void DoLink(LINKINFO& LinkInfo);
	void HideChildWindow();


	bool ShowLogIn();

	void FlushUserInfo();

	// add [6/24/2009 hemeng]
	virtual bool InitSource();

	// begin [7/23/2009 hemeng]
	void DisplayMinMode(bool bMinMode);
	// end [7/23/2009 hemeng]
	bool ChangeChatChannel(CHANNEL_SEL eChannel);
	void OnChannelChat(int nChatType, CHATMSG* pChatMsg);
	bool IsIrc(int nChatType);
	bool IsFilter(int nChatType);
	void ReadTipsInfo();
	CTaskbarNotifier* GetFreeNotifyer();
public:
	void NewPrivateChat(CString strRoleName, CString strChat = _T(""));
	int UserType();
	int LogIn();
	bool IsGuild() const
	{
		return m_IsGuild;
	}
	COLORREF GetItemColor(int nMassLevel);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedSdCancel();
	afx_msg void OnBnClickedSdMinimize();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	// modify [6/4/2009 hemeng]
	CButtonST m_ChannelSel[CS_COUNT];	
	//  [6/4/2009 hemeng]
	CIconPicker m_Face;
	CIrcEdit m_MsgEdit;
	CIrcEdit m_LocalMsg;
	CTabCtrl m_TabMsg;
	CMenu m_ChannelMenu;
	afx_msg void OnBnClickedChannel();
	afx_msg void OnBnClickedFace();
	afx_msg void OnOrganize();
	afx_msg void OnSamecity();
	afx_msg void OnGmchannel();
private:
	int m_CurChannel;
	map<CString, CPriChatDlg*> m_PrivateChats;
	CChannelFileter m_ChannelFilter;
	MSGTIPSLIST m_MsgList; //当窗口在托盘时系统接收到的消息
	vector<CHANNELMSG*> m_CacheMsg; //频道消息的缓存，用于综合和个人之间切换时更新界面用
	CLoginDlg* m_LoginDlg;
	IRCSTATUS m_IrcStatus;
	UINT m_GuidID;
	bool m_IsGuild;
	//// add [6/5/2009 hemeng]
	//HBITMAP	  m_bmpBackground;
	//// end add [6/5/2009 hemeng]

	// add [6/24/2009 hemeng]
	CTaskbarNotifier m_UsrNotifier[TRAYTIPSNUM];
	vector<TRAYTIPSINFO> m_TrayTipsInfo;
public:
	CUserRelation m_UserRelation;
public:
	string m_Account;
	string m_Pwd;
	string m_RoleName;
	string m_ServerIp;
	unsigned short m_ServerPort;
	bool m_ServerType;
	map<int, string> m_GuildPosList; //公会职位列表
public:
	CChatRecord* m_ChatRecord;
	CSysConfig* m_SysConfig;
	CLinkInfoDlg* m_LinkInfoDlg;
	CWordFilter m_WorldFilter;
public:
	afx_msg void OnChannelworld();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	virtual void OnOK();
public:
	afx_msg void OnChatrec();
	afx_msg void OnExit();
	afx_msg void OnUpdateMainfrm(CCmdUI *pCmdUI);
	afx_msg void OnUpdateOffline(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSysconfig(CCmdUI *pCmdUI);
	afx_msg void OnUpdateChatrec(CCmdUI *pCmdUI);
	afx_msg void OnOffline();
	afx_msg void OnRelogin();
	afx_msg void OnUpdateRelogin(CCmdUI *pCmdUI);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnNMClickTabmsg(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
