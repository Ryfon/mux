﻿#pragma once


// CChannelFileter 对话框

class CChannelFileter : public CDialog
{
	DECLARE_DYNAMIC(CChannelFileter)

public:
	CChannelFileter(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CChannelFileter();

// 对话框数据
	enum { IDD = IDD_DLG_FILTER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	bool m_WorldFilter;
	bool m_OrgFilter;
	bool m_SameCityFilter;
	bool m_GmFilter;
public:
	void ResetControl();
protected:
	virtual void OnOK();
	virtual void OnCancel();
public:
	virtual BOOL OnInitDialog();
};
