﻿// IconPicker.cpp : implementation file
//

#include "stdafx.h"
#include "IconPicker.h"
#include "EnBitmap.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIconPicker
CIconPicker::CIconPicker(bool bDel)
: m_bDel(bDel)
{
	m_iCurIndex=0;
	m_pIconContainer=NULL;
	m_bState=FALSE;
}

CIconPicker::~CIconPicker()
{
	if (!m_bDel)
	{
		return;
	}

	m_FaceNoList.clear();

	for (int nIndex = 0; nIndex < m_GifArray.GetSize(); nIndex++)
	{
		delete m_GifArray.GetAt(nIndex);
	}
}


BEGIN_MESSAGE_MAP(CIconPicker, CButtonST)
	//{{AFX_MSG_MAP(CIconPicker)
	ON_WM_LBUTTONDOWN()
	ON_MESSAGE(WM_USER + 101, OnSelectICON)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIconPicker message handlers

////左键按下的时候在本按钮下方创建一个CIconContainer
////在CIconContainer中显示所有的小位图
void CIconPicker::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(m_bState || m_GifArray.IsEmpty()) 
		return ;
	m_bState=TRUE;
	this->SetState(TRUE);
	

	RECT rect;
	this->GetWindowRect(&rect);
	
	POINT pt;
	pt.x=rect.left;pt.y=rect.bottom;

	m_pIconContainer = new CIconContainer;
	if(m_pIconContainer->Create(pt, this, &m_GifArray))
	{
		m_pIconContainer->ShowWindow(SW_SHOW);
		m_pIconContainer->UpdateWindow();
		m_pIconContainer->SetFocus();
	}
}


///收到来自CIconContainer的消息,返回用户选中的图片标号
///-3表示用户没有选中图片
LRESULT CIconPicker::OnSelectICON(WPARAM wParam, LPARAM lParam)
{
	if(wParam >= 0 && wParam < (WPARAM)m_GifArray.GetSize())
	{
		///如果用户选择了图片,则发送WM_COMMAND给父窗口,表示本按钮被单击
		m_iCurIndex = (int)wParam;

		::SendMessage(GetParent()->m_hWnd,WM_COMMAND,GetDlgCtrlID(),0);
	}
	///否则的话,不发送WM_COMMAND消息
	this->SetState(FALSE);
	this->Invalidate();
	m_bState = FALSE;
	delete m_pIconContainer;
	return 0;
}


CGif* CIconPicker::GetGifAt(int nFaceNo)
{
	vector<int>::iterator Ite = find(m_FaceNoList.begin(), m_FaceNoList.end(), nFaceNo);
	if (Ite == m_FaceNoList.end())
	{
		return NULL;
	}

	return m_GifArray.GetAt(Ite - m_FaceNoList.begin());
}

int CIconPicker::GetFaceNoAt(int index)
{
	return m_FaceNoList[index];
}
///返回图片列表中图片的个数
int CIconPicker::GetFaceCount()
{
	return (int)m_GifArray.GetSize();
}

int CIconPicker::GetCurFaceNo()
{
	return m_FaceNoList[m_iCurIndex];
}

void CIconPicker::CreateFace(CString strFilePath)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(strFilePath, &FindFileData);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		while (FindNextFile(hFind, &FindFileData))
		{
			if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				continue;
			}
			CString strFileName = strFilePath.Left(strFilePath.GetLength() - 1);
			strFileName += FindFileData.cFileName;
			char* pFaceNo = strchr(FindFileData.cFileName, '.');
			FindFileData.cFileName[pFaceNo - FindFileData.cFileName] = 0;
			int nFaceNo = atoi(FindFileData.cFileName);
			//AddBitmap(strFileName, nFaceNo);
		}
	}
	FindClose(hFind);
}

void CIconPicker::AddGif(void* pBuf, UINT nBufSize, int nFace)
{
	CGif* pGif = new CGif((BYTE*)pBuf, nBufSize);
	AddGif(pGif, nFace);
}

void CIconPicker::AddGif(CGif* pGif, int nFace)
{
	m_GifArray.Add(pGif);
	m_FaceNoList.push_back(nFace);
}