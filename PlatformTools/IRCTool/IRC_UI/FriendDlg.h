﻿#pragma once
#include "afxcmn.h"
#include "IRCIntf.h"
#include "UsrInfoDlg.h"

// CFriendDlg 对话框

struct DispGroupInfo
{
	FriendGroupInfo_I GroupInfo;
	int FriendsCount;
	int FriendCountOnLine;
};

class CFriendDlg : public CDialog
{
	DECLARE_DYNAMIC(CFriendDlg)

public:
	CFriendDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CFriendDlg();

// 对话框数据
	enum { IDD = IDD_DLG_FRIEND };
private:
	void InitFriendInfo();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	void AddFriendInfo(FriendInfo_I* pFriendInfo);
	void AddFriendGroupInfo(FriendGroupInfo_I* pFriendGroupInfo);
	void ClearFriendInfo();
	void OffLine();
	void FriendInfoUpdate(FriendInfo_I* pFriendInfo);
	int GetFriendOnLineType(unsigned long ulFriendID);
	bool IsFriend(const string& strRoleName);
	CString GetFriendName(unsigned long ulFriendID);
	map<unsigned long, DispGroupInfo*>::iterator GetGroupBegin()
	{
		return m_GroupInfoMap.begin();
	}
	map<unsigned long, DispGroupInfo*>::iterator GetGroupEnd()
	{
		return m_GroupInfoMap.end();
	}

	map<unsigned long, FriendInfo_I*>::iterator GetFriendBegin()
	{
		return m_FriendInfoMap.begin();
	}

	map<unsigned long, FriendInfo_I*>::iterator GetFriendEnd()
	{
		return m_FriendInfoMap.end();
	}

private:
	CListCtrl m_GroupInfo;
	CListCtrl m_FriendInfo;
	CMenu m_FriendMenu;
	FriendInfo_I* m_CurPosFriend;
	map<unsigned long, DispGroupInfo*> m_GroupInfoMap;
	map<unsigned long, FriendInfo_I*> m_FriendInfoMap;
	CBitmap m_BmpSortUp;
	CBitmap m_BmpSortDown;
	CImageList m_ImgList;
	CImageList m_SortImgList;
	int m_SortType; //-1:默认排序 0 第一列升序 1第一列降序 2 第二列升序 3 第二列降序
public:
	CUsrInfoDlg* m_UsrInfoDlg;
private:
	void DisplayFriendInfo(unsigned long ulGroupID);

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnFriendsDetails();
	afx_msg void OnUpdateFriendsDetails(CCmdUI *pCmdUI);
	afx_msg void OnFriendsPrichat();
	afx_msg void OnUpdateFriendsPrichat(CCmdUI *pCmdUI);
	afx_msg void OnFriendsCopyname();
	afx_msg void OnUpdateFriendsCopyname(CCmdUI *pCmdUI);
	afx_msg void OnNMRclickFriendinfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickGroupinfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawFriendinfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnHdnItemclickFriendinfo(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	static int CALLBACK FriendInfoSort(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
public:
	afx_msg void OnNMDblclkFriendinfo(NMHDR *pNMHDR, LRESULT *pResult);
};
