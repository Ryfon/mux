﻿#pragma once

#define MSG_TIP_TYPE_POPUP 1
#define MSG_TIP_TYPE_FLASH 2

#define ONLINE_TIP_FRIEND 1
#define ONLINE_TIP_FOE 2
#define ONLINE_TIP_GAME 3
#define ONLINE_TIP_IRC 4

#define MSG_CHANNEL_WORLD 1
#define MSG_CHANNEL_SAMECITY 2
#define MSG_CHANNEL_ORG 3
#define MSG_CHANNEL_GM 4

#define MSG_PRICHAT_TYPE_FRIEND 1
#define MSG_PRICHAT_TYPE_FOE 2
#define MSG_PRICHAT_TYPE_STRANGER 3

#define  IRC_CLOSE_TIP_TRAY 1
#define  IRC_CLOSE_TIP_CLOSE 2

class CConfig
{
public:
	string m_ConfigFileName;
	int m_MsgTipType;
	string m_strMsgPath;
	vector<int> m_OnLineTip;
	vector<int> m_MsgChannel;
	vector<int> m_MsgPriChatType;
	int m_CloseType;
	bool m_DispCloseTip;

	string m_Account;
	string m_Pwd;
	string m_RoleName;
	string m_SrvIp;
	unsigned short m_SrvPort;
	int m_SrvType;
public:
	CConfig(void);
	virtual ~CConfig(void);
public:
	int LoadConfig(const char* szXmlFile); 
	int SaveConfig(const char* szXmlFile);
	int SaveConfig();
	static void Split(const string& strSrc, vector<int>& vectInt, char ch);
public:
	bool OnLineTip(int nTip);
	bool MsgChannel(int nChannel);
	bool MsgPriChatType(int nType);
};
