﻿#pragma once
#include "btnst.h"
#include "afxcmn.h"

// CLoginDlg 对话框
class CLoginDlg : public CDialog
{
	DECLARE_DYNAMIC(CLoginDlg)

public:
	CLoginDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CLoginDlg();

// 对话框数据
	enum { IDD = IDD_DLG_LOGIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnOk();
	void OnLogin();
	CString m_Account;
	CString m_Pwd;
	CString m_RoleName;
	CString m_ServerIp;
	int m_Port;
	int m_GateType;
	int m_Stage; //0:登录　１：选角色
protected:
	virtual void OnOK();
	
		// add [6/24/2009 hemeng]
	virtual bool InitSource();
	//virtual bool SetButtonSTSource(CButtonST& kButton,string strDes0, string strDes1 = "");
public:
	virtual BOOL OnInitDialog();
public:
	void ProcessRole(unsigned int uiRoleID, const char* strRoleName);
	CListCtrl m_RoleList;
};
