﻿#include "StdAfx.h"
#include "IrcEventSinkImp.h"

CIrcEventSinkImp::CIrcEventSinkImp(CWnd* pWnd)
: m_Wnd(pWnd)
{

}

CIrcEventSinkImp::~CIrcEventSinkImp(void)
{
	
}

int CIrcEventSinkImp::OnIRCError(unsigned short usErrCode)
{
	m_Wnd->PostMessage(WM_IRC_ERROR, 0, (LPARAM)usErrCode);
	return 0;
}

int CIrcEventSinkImp::OnFriendsGroup(FriendGroupInfo_I* pFirendGroupInfo, unsigned short usGroupNum)
{
	FriendGroupInfo_I* pFriendGrpInfo = new FriendGroupInfo_I[usGroupNum];
	memcpy(pFriendGrpInfo, pFirendGroupInfo, usGroupNum * sizeof(FriendGroupInfo_I));
	m_Wnd->PostMessage(WM_IRC_FRIENDGROUP, usGroupNum, (LPARAM)pFriendGrpInfo);
	return 0;
}

int CIrcEventSinkImp::OnFriendInfo(FriendInfo_I* pFriendInfo, unsigned short usFriendCount)
{
	FriendInfo_I* pNewFriendInfo = new FriendInfo_I[usFriendCount];
	memcpy(pNewFriendInfo, pFriendInfo, usFriendCount * sizeof(FriendInfo_I));
	m_Wnd->PostMessage(WM_IRC_FRIENDINFO, usFriendCount, (LPARAM)pNewFriendInfo);
	return 0;
}

int CIrcEventSinkImp::OnFriendInfoUpdate(FriendInfo_I* pFriendInfo)
{
	FriendInfo_I* pLocalFriendInfo = new FriendInfo_I;
	memcpy(pLocalFriendInfo, pFriendInfo, sizeof(FriendInfo_I));
	m_Wnd->PostMessage(WM_IRC_FRIENDINFOUPDATE, 0, (LPARAM)pLocalFriendInfo);
	return 0;
}

int CIrcEventSinkImp::OnFoeInfo(FoeInfo_I* pFoeInfo, unsigned short usFoeCount)
{
	FoeInfo_I* pNewFoeInfo = new FoeInfo_I[usFoeCount];
	memcpy(pNewFoeInfo, pFoeInfo, usFoeCount * sizeof(FoeInfo_I));
	m_Wnd->PostMessage(WM_IRC_FEOINFO, usFoeCount, (LPARAM)pNewFoeInfo);
	return 0;
}

int CIrcEventSinkImp::OnFoeUpdate(FoeInfo_I* pFoeInfo)
{
	FoeInfo_I* pLocalFoeInfo = new FoeInfo_I;
	memcpy(pLocalFoeInfo, pFoeInfo, sizeof(FoeInfo_I));
	m_Wnd->PostMessage(WM_IRC_FEOINFOUPDATE, 0, (LPARAM)pLocalFoeInfo);
	return 0;
}

int CIrcEventSinkImp::OnBlackItem(BlackListItemInfo_I* pBlackItemInfo)
{
	BlackListItemInfo_I* pNewBlackItemInfo = new BlackListItemInfo_I;
	memcpy(pNewBlackItemInfo, pBlackItemInfo, sizeof(BlackListItemInfo_I));
	m_Wnd->PostMessage(WM_IRC_BLACKITEM, 0, (LPARAM)pNewBlackItemInfo);
	return 0;
}

int CIrcEventSinkImp::OnRoleInfo(unsigned int uiRoleID, const char* szRoleName)
{
	char* pName = new char[MAX_NAME_SIZE + 1];
	pName[0] = 0;
	sprintf(pName, "%s", szRoleName);
	m_Wnd->PostMessage(WM_IRC_ROLEINFO, (WPARAM)uiRoleID, (LPARAM)pName);
	return 0;
}
int CIrcEventSinkImp::OnLoginGame()
{
	m_Wnd->PostMessage(WM_IRC_LOGIN);
	return 0;
}

int CIrcEventSinkImp::OnWorldMsg(const char* szSrc, const char* szChat, int nClientType)
{
	CHATMSG* pChatMsg = new CHATMSG;
	memset(pChatMsg, 0, sizeof(CHATMSG));
	strcpy(pChatMsg->szRoleName, szSrc);
	strcpy(pChatMsg->szChat, szChat);
	m_Wnd->PostMessage(WM__IRC_WORLDCHAT, (WPARAM)nClientType, (LPARAM)pChatMsg);
	return 0;
}

int CIrcEventSinkImp::OnGuildMsg(const char* szSrc, const char* szChat, int nClientType)
{
	CHATMSG* pChatMsg = new CHATMSG;
	memset(pChatMsg, 0, sizeof(CHATMSG));
	strcpy(pChatMsg->szRoleName, szSrc);
	strcpy(pChatMsg->szChat, szChat);
	m_Wnd->PostMessage(WM_IRC_GUILDCHAT, (WPARAM)nClientType, (LPARAM)pChatMsg);
	return 0;
}

int CIrcEventSinkImp::OnPrivateMsg(const char* szSrc, const char* szChat, int nClientType)
{
	CHATMSG* pChatMsg = new CHATMSG;
	memset(pChatMsg, 0, sizeof(CHATMSG));
	strcpy(pChatMsg->szRoleName, szSrc);
	strcpy(pChatMsg->szChat, szChat);
	m_Wnd->PostMessage(WM_IRC_PRIVATECHAT, (WPARAM)nClientType, (LPARAM)pChatMsg);
	return 0;
}

int CIrcEventSinkImp::OnChatError(const char* szSrc, int nErrorType)
{
	//聊天时发生错误，这时候应该不让玩家再发送消息
	CHATMSG* pChatMsg = new CHATMSG;
	memset(pChatMsg, 0, sizeof(CHATMSG));
	strcpy(pChatMsg->szRoleName, szSrc);
	m_Wnd->PostMessage(WM_IRC_CHATERROR, nErrorType, (LPARAM)pChatMsg);
	return 0;
}

int CIrcEventSinkImp::OnReadPrivateMsg(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_Wnd->PostMessage(WM_IRC_READPRIVATEMSG, 0, (LPARAM)pNewChatLog);
	return 0;
}

int CIrcEventSinkImp::OnReadWorldMsg(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_Wnd->PostMessage(WM_IRC_READWORLDMSG, 0, (LPARAM)pNewChatLog);
	return 0;
}

int CIrcEventSinkImp::OnReadGuildMsg(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_Wnd->PostMessage(WM_IRC_READGUILDMSG, 0, (LPARAM)pNewChatLog);
	return 0;
}

int CIrcEventSinkImp::OnReadCommonMsg(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_Wnd->PostMessage(WM_IRC_READCOMMONMSG, 0, (LPARAM)pNewChatLog);
	return 0;
}

int CIrcEventSinkImp::OnReadSystemMsg(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_Wnd->PostMessage(WM_IRC_READSYSTEMMSG, 0, (LPARAM)pNewChatLog);
	return 0;
}

int CIrcEventSinkImp::OnReadTeamMsg(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_Wnd->PostMessage(WM_IRC_READTEAMMSG, 0, (LPARAM)pNewChatLog);

	return 0;
}

int CIrcEventSinkImp::OnReadChatGroupMsg(ChatLog* pChatLog)
{
	ChatLog* pNewChatLog = new ChatLog;
	memcpy(pNewChatLog, pChatLog, sizeof(ChatLog));
	m_Wnd->PostMessage(WM_IRC_READCHATGROUPMSG, 0, (LPARAM)pNewChatLog);

	return 0;
}


int CIrcEventSinkImp::OnPeerInfo(PEERUSRINFO* pPeerUsrInfo)
{
	PEERUSRINFO* pNewPeerUsrInfo = new PEERUSRINFO;
	memcpy(pNewPeerUsrInfo, pPeerUsrInfo, sizeof(PEERUSRINFO));
	m_Wnd->PostMessage(WM_IRC_PEERUSRINF, 0, (LPARAM)pNewPeerUsrInfo);
	return 0;
}

int CIrcEventSinkImp::OnGuild(Union_I* pUnion)
{
	Union_I* pNewUnion = new Union_I;
	memcpy(pNewUnion, pUnion, sizeof(Union_I));
	m_Wnd->PostMessage(WM_IRC_GUILD, 0, (LPARAM)pNewUnion);
	return 0;
}

int CIrcEventSinkImp::OnGuildDismiss()
{
	m_Wnd->PostMessage(WM_IRC_GUILD_DISMISS, 0, 0);
	return 0;
}

int CIrcEventSinkImp::OnGuildMember(UnionMember_I* pUnionMember)
{
	UnionMember_I* pNewUnionMember = new UnionMember_I;
	memcpy(pNewUnionMember, pUnionMember, sizeof(UnionMember_I));
	m_Wnd->PostMessage(WM_IRC_GUILD_MEMBER, 0, (LPARAM)pNewUnionMember);
	return 0;
}

int CIrcEventSinkImp::OnGuildBanChat(const char* szRoleName)
{
	char* pszName = new char[strlen(szRoleName) + 1];
	memset(pszName, 0, strlen(szRoleName) + 1);
	strcpy(pszName, szRoleName);
	m_Wnd->PostMessage(WM_IRC_GUILD_BANCHAT, 0, (LPARAM)pszName);
	return 0;
}
int CIrcEventSinkImp::OnGuildDelMember(unsigned short usFlag, unsigned int nMemID)
{
	m_Wnd->PostMessage(WM_IRC_GUILD_DELMEM, (WPARAM)usFlag, (LPARAM)nMemID);
	return 0;
}

int CIrcEventSinkImp::OnGuildAddMem(UnionMember_I* pGuildMem)
{
	UnionMember_I* pNewGuildMem = new UnionMember_I;
	memcpy(pNewGuildMem, pGuildMem, sizeof(UnionMember_I));
	m_Wnd->PostMessage(WM_IRC_GUILD_ADDMEM, 0, (LPARAM)pNewGuildMem);
	return 0;
}

int CIrcEventSinkImp::OnGuildMemOffOnline(UINT nMemID, bool bIsOnLine)
{
	m_Wnd->PostMessage(WM_IRC_GUILD_ONOFFLINE, (WPARAM)nMemID, (LPARAM)bIsOnLine);
	return 0;
}

int CIrcEventSinkImp::OnGuildMemberTitleChange(const char* szMemberName, const char* szTitleName)
{
	char* pMemberName = new char[strlen(szMemberName) + 1];
	memset(pMemberName, 0, strlen(szMemberName) + 1);
	strcpy(pMemberName, szMemberName);
	char* pTitleName = new char[strlen(szTitleName) + 1];
	memset(pTitleName, 0, strlen(szTitleName) + 1);
	strcpy(pTitleName, szTitleName);
	m_Wnd->PostMessage(WM_IRC_GUILD_MEMTITLECHANGE, (WPARAM)pMemberName, (LPARAM)pTitleName);
	return 0;
}

int CIrcEventSinkImp::OnGuildPosUpdateNty(int nPosLen, UnionPosRight_I *pUnionPos)
{
	UnionPosRight_I* pNewUnionPos = new UnionPosRight_I[nPosLen];
	memcpy(pNewUnionPos, pUnionPos, sizeof(UnionPosRight_I) * nPosLen);
	m_Wnd->PostMessage(WM_IRC_GUILD_POS_UPDATE, (WPARAM)nPosLen, (LPARAM)pNewUnionPos);
	return 0;
}

int CIrcEventSinkImp::OnGuildMemPosChange(const char* szMemberName, USHORT nPos)
{
	m_Wnd->SendMessage(WM_IRC_GUILD_MEMPOS_NTY, (WPARAM)szMemberName, (LPARAM)nPos);
	return 0;
}
