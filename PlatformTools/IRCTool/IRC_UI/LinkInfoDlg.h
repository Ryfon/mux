﻿#pragma once
#include "afxcmn.h"
#include "UISourceManager.h"
#include "IrcEdit.h"
// CLinkInfoDlg 对话框

class CLinkInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CLinkInfoDlg)

public:
	CLinkInfoDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CLinkInfoDlg();

// 对话框数据
	enum { IDD = IDD_DLG_LINKINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CIrcEdit m_Tips;
	int m_ParentType;
	CDialog* m_Parent;
protected:
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();
public:
	void UpdateTips(int nItemType, int nLevelUp, int nAddID, int nWear, int nMassLevel, vector<string>& vectItemInfo);
	void UpdateTips(vector<string>& vectTaskInfo);
	void UpdateSkillInfo(vector<string>& vectSkillProp);
	virtual BOOL OnInitDialog();
	void SetParentInfo(int nType, CDialog* pDlg)
	{
		m_ParentType = nType;
		m_Parent = pDlg;
	}
private:
	void UpdateItemTips(int nLevelUp, int nAddID, int nWear, int nMassLevel, vector<string>& vectItemInfo);
	void UpdateEquipItemTips(int nLevelUp, int nAddID, int nWear, int nMassLevel, vector<string>& vectItemInfo);
	void InitCharFormat(int nFontSize, COLORREF crColor, const char* szFontName, CHARFORMAT2& cf);
};
