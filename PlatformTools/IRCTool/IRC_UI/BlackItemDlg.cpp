﻿// BlackItemDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "BlackItemDlg.h"


// CBlackItemDlg 对话框

IMPLEMENT_DYNAMIC(CBlackItemDlg, CDialog)

CBlackItemDlg::CBlackItemDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBlackItemDlg::IDD, pParent)
{

}

CBlackItemDlg::~CBlackItemDlg()
{
}

void CBlackItemDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BLACKLIST, m_BlackList);
}


BEGIN_MESSAGE_MAP(CBlackItemDlg, CDialog)

END_MESSAGE_MAP()


// CBlackItemDlg 消息处理程序

BOOL CBlackItemDlg::OnInitDialog()
{
	CDialog::OnInitDialog();	

	CRect rectList;
	m_BlackList.GetClientRect(&rectList);
	m_BlackList.InsertColumn(0, _T("姓名"), LVCFMT_CENTER, rectList.Width());

	ListView_SetExtendedListViewStyle(m_BlackList.m_hWnd, LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CBlackItemDlg::GetBlackItem(vector<string>& vectBlackItem)
{
	vectBlackItem.clear();
	for (int nIndex = 0; nIndex < m_BlackList.GetItemCount(); nIndex++)
	{
		string strTmp = m_BlackList.GetItemText(nIndex, 0);
		vectBlackItem.push_back(strTmp);
	}
}

bool CBlackItemDlg::IsBlackItem(const string& strRoleName)
{
	bool bRet = false;
	for (int nIndex = 0; nIndex < m_BlackList.GetItemCount(); nIndex++)
	{
		string strTmp = m_BlackList.GetItemText(nIndex, 0);
		if (strTmp == strRoleName)
		{
			bRet = true;
			break;
		}
	}

	return bRet;
}

