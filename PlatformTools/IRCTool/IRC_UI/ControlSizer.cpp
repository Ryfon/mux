﻿#include "StdAfx.h"
#include "ControlSizer.h"

CControlSizer::CControlSizer(CWnd* pControl, CWnd* pParent, FIXPOINT fp, FIXDIM fd)
: m_Control(pControl), m_Owner(pParent), m_fp(fp), m_fd(fd)
{
	m_Owner->GetWindowRect(&m_OwnerRect);
	m_Control->GetWindowRect(&m_ControlRect);
	m_Owner->ScreenToClient(&m_ControlRect);
}

CControlSizer::~CControlSizer(void)
{

}

void CControlSizer::Update()
{
	CRect rectOwner;
	m_Owner->GetWindowRect(&rectOwner);
	if (rectOwner.Height() == m_OwnerRect.Height() && rectOwner.Width() == m_OwnerRect.Width())
	{
		return;
	}
	int nHeightOff = rectOwner.Height() - m_OwnerRect.Height();
	int nWidthOff = rectOwner.Width() - m_OwnerRect.Width();
	CRect rectControl(m_ControlRect);

	switch(m_fp)
	{
	case TOPLEFT:
		{
			switch(m_fd)
			{
			case FIXNO:
				{
					rectControl.bottom += nHeightOff;
					rectControl.right += nWidthOff;
				}
				break;
			case FIXHEIGHT:
				{
					rectControl.right += nWidthOff;
				}
				break;
			case FIXALL:
			    break;
			case FIXWIDTH:
				{
					rectControl.bottom += nHeightOff;
				}
			    break;
			default:
			    break;
			}
		}
		break;
	case TOPRIGHT:
		{
			switch(m_fd)
			{
			case FIXNO:
				{
					rectControl.bottom += nHeightOff;
					rectControl.left -= nWidthOff;
				}
				break;
			case FIXHEIGHT:
				{
					rectControl.left -= nWidthOff;
				}
				break;
			case FIXALL:
				break;
			case FIXWIDTH:
				{
					rectControl.bottom += nHeightOff;
					rectControl.left -= nWidthOff;
				}
				break;
			default:
				break;
			}

		}
		break;
	case BOTTOMLEFT:
		{
			switch(m_fd)
			{
			case FIXNO:
				{
					rectControl.top -= nHeightOff;
					rectControl.right += nWidthOff;
				}
				break;
			case FIXHEIGHT:
				{
					rectControl.top += nHeightOff;
					rectControl.bottom += nHeightOff;
					rectControl.right += nWidthOff;
				}
				break;
			case FIXALL:
				{
					rectControl.top += nHeightOff;
					rectControl.bottom += nHeightOff;
				}
				break;
			case FIXWIDTH:
				{
					rectControl.top -= nHeightOff;
				}
				break;
			default:
				break;
			}
		}
	    break;
	case BOTTOMRIGHT:
		{
			switch(m_fd)
			{
			case FIXNO:
				{
					rectControl.top -= nHeightOff;
					rectControl.left -= nWidthOff;
				}
				break;
			case FIXHEIGHT:
				{
					rectControl.top += nHeightOff;
					rectControl.bottom += nHeightOff;
					rectControl.left -= nWidthOff;
				}
				break;
			case FIXALL:
				{
					rectControl.top += nHeightOff;
					rectControl.bottom += nHeightOff;
					rectControl.left += nWidthOff;
					rectControl.right += nWidthOff;
				}
				break;
			case FIXWIDTH:
				{
					rectControl.top -= nHeightOff;
					rectControl.left += nWidthOff;
					rectControl.right += nWidthOff;
				}
				break;
			default:
				break;
			}
		}
	    break;
	default:
	    break;
	}
	m_Control->MoveWindow(&rectControl);
	m_ControlRect = rectControl;
	m_OwnerRect = rectOwner;
}