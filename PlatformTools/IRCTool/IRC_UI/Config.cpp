﻿#include "StdAfx.h"
#include "Config.h"
#define CONFIGFILENAME _T(".\\IRCConfig.xml")
CConfig::CConfig(void)
{
	m_ConfigFileName = CONFIGFILENAME;
	LoadConfig(m_ConfigFileName.c_str());
}

CConfig::~CConfig(void)
{

}

int CConfig::LoadConfig(const char* szXmlFile)
{
	TiXmlDocument XmlDoc(szXmlFile);
	if (!XmlDoc.LoadFile())
	{
		OutputDebugString("打开Xml文件失败。/n");
		return -1000;
	}

	TiXmlElement* pRootElement = XmlDoc.RootElement();
	
	TiXmlElement* pXmlSection = pRootElement->FirstChildElement();
	if (!pXmlSection)
	{
		return -1;
	}
	pXmlSection->Attribute("TipType", &m_MsgTipType);	

	pXmlSection = pXmlSection->NextSiblingElement();
	if (!pXmlSection)
	{
		return -1;
	}
	m_strMsgPath = pXmlSection->Attribute("FilePath");
	pXmlSection = pXmlSection->NextSiblingElement();
	if (!pXmlSection)
	{
		return -1;
	}
	string strTipList = pXmlSection->Attribute("TipList");
	strTipList += ",";
	Split(strTipList, m_OnLineTip, ',');

	pXmlSection = pXmlSection->NextSiblingElement();
	if (!pXmlSection)
	{
		return -1;
	}
	string strChannelList = pXmlSection->Attribute("ChannelList");
	strChannelList += ",";
	Split(strChannelList, m_MsgChannel, ',');

	pXmlSection = pXmlSection->NextSiblingElement();
	if (!pXmlSection)
	{
		return -1;
	}
	string strTypeList = pXmlSection->Attribute("TypeList");
	strTypeList += ",";
	Split(strTypeList, m_MsgPriChatType, ',');

	pXmlSection = pXmlSection->NextSiblingElement();
	if (!pXmlSection)
	{
		return -1;
	}
	pXmlSection->Attribute("Type", &m_CloseType);
	m_DispCloseTip = atoi(pXmlSection->Attribute("DispTips")) != 0;
	
	pXmlSection = pXmlSection->NextSiblingElement();
	if (!pXmlSection)
	{
		return -1;
	}

	m_Account = pXmlSection->Attribute("Account");
	m_Pwd = pXmlSection->Attribute("Pwd");
	m_SrvIp = pXmlSection->Attribute("SrvIp") ;
	m_RoleName = pXmlSection->Attribute("RoleName");
	pXmlSection->Attribute("SrvPort", (int*)&m_SrvPort);
	pXmlSection->Attribute("SrvType", &m_SrvType);
	m_ConfigFileName = szXmlFile;
	return 0;
}

int CConfig::SaveConfig(const char* szXmlFile)
{
	//插入Xml声明
	TiXmlDocument* pXmlDoc = new TiXmlDocument(szXmlFile);
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	pXmlDoc->InsertEndChild(*pXmlDec);	
	TiXmlElement* pXmlRootElement = new TiXmlElement("IRCSysConfig");
	pXmlDoc->LinkEndChild(pXmlRootElement);

	TiXmlElement* pXmlMsgTipType = new TiXmlElement("MsgTipType");
	pXmlMsgTipType->SetAttribute("TipType", m_MsgTipType);
	pXmlRootElement->LinkEndChild(pXmlMsgTipType);

	TiXmlElement* pXmlMsgPath = new TiXmlElement("MsgPath");
	pXmlMsgPath->SetAttribute("FilePath", m_strMsgPath.c_str());
	pXmlRootElement->LinkEndChild(pXmlMsgPath);

	TiXmlElement* pXmlOnLineTip = new TiXmlElement("OnLineTip");
	string strOnLineTip;
	for (int nIndex = 0; nIndex < (int)m_OnLineTip.size(); nIndex++)
	{
		char szTmp[MAX_PATH];
		itoa(m_OnLineTip[nIndex], szTmp, 10);
		strOnLineTip.append(szTmp);
		if (nIndex != (int)m_OnLineTip.size() - 1)
		{
			strOnLineTip.append(",");
		}
	}
	pXmlOnLineTip->SetAttribute("TipList", strOnLineTip.c_str());
	pXmlRootElement->LinkEndChild(pXmlOnLineTip);

	TiXmlElement* pXmlMsgChannel = new TiXmlElement("MsgChannel");
	string strChannelList;
	for (int nIndex = 0; nIndex < (int)m_MsgChannel.size(); nIndex++)
	{
		char szTmp[MAX_PATH];
		itoa(m_MsgChannel[nIndex], szTmp, 10);
		strChannelList.append(szTmp);
		if (nIndex != (int)m_MsgChannel.size() - 1)
		{
			strChannelList.append(",");
		}
	}
	pXmlMsgChannel->SetAttribute("ChannelList", strChannelList.c_str());
	pXmlRootElement->LinkEndChild(pXmlMsgChannel);

	TiXmlElement* pXmlMsgPriChatType = new TiXmlElement("MsgPriChatType");
	string strMsgPriChatType;
	for (int nIndex = 0; nIndex < (int)m_MsgPriChatType.size(); nIndex++)
	{
		char szTmp[MAX_PATH];
		itoa(m_MsgPriChatType[nIndex], szTmp, 10);
		strMsgPriChatType.append(szTmp);
		if (nIndex != (int)m_MsgPriChatType.size() - 1)
		{
			strMsgPriChatType.append(",");
		}
	}
	pXmlMsgPriChatType->SetAttribute("TypeList", strMsgPriChatType.c_str());
	pXmlRootElement->LinkEndChild(pXmlMsgPriChatType);

	TiXmlElement* pXmlIRCCloseType = new TiXmlElement("IRCCloseType");
	pXmlIRCCloseType->SetAttribute("Type", m_CloseType);
	pXmlIRCCloseType->SetAttribute("DispTips", m_DispCloseTip ? 1 : 0);
	pXmlRootElement->LinkEndChild(pXmlIRCCloseType);
	
	TiXmlElement* pXmlAccountInfo = new TiXmlElement("AccountInfo");
	pXmlAccountInfo->SetAttribute("Account", m_Account.c_str());
	pXmlAccountInfo->SetAttribute("Pwd", m_Pwd.c_str());
	pXmlAccountInfo->SetAttribute("RoleName", m_RoleName.c_str());
	pXmlAccountInfo->SetAttribute("SrvIp", m_SrvIp.c_str());
	pXmlAccountInfo->SetAttribute("SrvPort", m_SrvPort);
	pXmlAccountInfo->SetAttribute("SrvType", m_SrvType);
	pXmlRootElement->LinkEndChild(pXmlAccountInfo);

	pXmlDoc->SaveFile(szXmlFile);

	delete pXmlDec;
	delete pXmlDoc;
	return 0;
}

void CConfig::Split(const string &strSrc, vector<int> &vectInt, char ch)
{
	vectInt.clear();
	string::size_type stPos = 0;
	string strTmp = strSrc;
	while (strTmp != "")
	{
		stPos = strTmp.find(ch);
		if (stPos == string::npos)
		{
			break;
		}
		string strItem = strTmp.substr(0, stPos);
		vectInt.push_back(atoi(strItem.c_str()));
		strTmp = strTmp.substr(stPos + 1);
	}
}

int CConfig::SaveConfig()
{
	return SaveConfig(m_ConfigFileName.c_str());
}

bool CConfig::OnLineTip(int nTip)
{
	vector<int>::iterator Ite = find(m_OnLineTip.begin(), m_OnLineTip.end(), nTip);
	return Ite != m_OnLineTip.end();
}

bool CConfig::MsgChannel(int nChannel)
{
	vector<int>::iterator Ite = find(m_MsgChannel.begin(), m_MsgChannel.end(), nChannel);
	return Ite != m_MsgChannel.end();
}

bool CConfig::MsgPriChatType(int nType)
{
	vector<int>::iterator Ite = find(m_MsgPriChatType.begin(), m_MsgPriChatType.end(), nType);
	return Ite != m_MsgPriChatType.end();
}