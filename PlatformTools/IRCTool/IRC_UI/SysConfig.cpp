﻿// SysConfig.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "SysConfig.h"
#include "IRC_UIDlg.h"

// CSysConfig 对话框

IMPLEMENT_DYNAMIC(CSysConfig, CDialog)

CSysConfig::CSysConfig(CWnd* pParent /*=NULL*/)
	: CDialog(CSysConfig::IDD, pParent)
{
	
}

CSysConfig::~CSysConfig()
{

}

void CSysConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LST_SECTION, m_lvConfigSection);
	DDX_Control(pDX, IDC_BTN_OK, m_btnOk);
	DDX_Control(pDX, IDC_BTN_CANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BTN_APPLY, m_btnApply);
}


BEGIN_MESSAGE_MAP(CSysConfig, CDialog)
	ON_BN_CLICKED(IDC_BTN_OK, &CSysConfig::OnBnClickedBtnOk)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CSysConfig::OnBnClickedBtnCancel)
	ON_BN_CLICKED(IDC_BTN_APPLY, &CSysConfig::OnBnClickedBtnApply)
	ON_NOTIFY(NM_CLICK, IDC_LST_SECTION, &CSysConfig::OnNMClickLstSection)
	ON_BN_CLICKED(IDC_BTN_OPENFILE, &CSysConfig::OnBnClickedBtnOpenfile)
	ON_BN_CLICKED(IDC_BTB_CLEARMSG, &CSysConfig::OnBnClickedBtbClearmsg)
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()


// CSysConfig 消息处理程序

void CSysConfig::OnBnClickedBtnOk()
{
	UpdateConfig();
	OnCancel();
}

void CSysConfig::OnBnClickedBtnCancel()
{
	OnCancel();
}

void CSysConfig::OnBnClickedBtnApply()
{
	UpdateConfig();
}

BOOL CSysConfig::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_lvConfigSection.InsertColumn(0, _T(""), LVCFMT_CENTER, 2);
	CRect rectlv;
	m_lvConfigSection.GetClientRect(&rectlv);
	m_lvConfigSection.InsertColumn(1, _T("系统设置"), LVCFMT_LEFT, rectlv.Width() - 2);
	ListView_SetExtendedListViewStyle(m_lvConfigSection.m_hWnd, LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);

	int nIndex = m_lvConfigSection.InsertItem(m_lvConfigSection.GetItemCount(), _T(""));
	m_lvConfigSection.SetItemText(nIndex, 1, _T("防打扰设置"));
	m_lvConfigSection.SetItemData(nIndex, (DWORD_PTR)nIndex);

	nIndex = m_lvConfigSection.InsertItem(m_lvConfigSection.GetItemCount(), _T(""));
	m_lvConfigSection.SetItemText(nIndex, 1, _T("消息保存设置"));
	m_lvConfigSection.SetItemData(nIndex, (DWORD_PTR)nIndex);

	nIndex = m_lvConfigSection.InsertItem(m_lvConfigSection.GetItemCount(), _T(""));
	m_lvConfigSection.SetItemText(nIndex, 1, _T("上下线提示设置"));
	m_lvConfigSection.SetItemData(nIndex, (DWORD_PTR)nIndex);

	nIndex = m_lvConfigSection.InsertItem(m_lvConfigSection.GetItemCount(), _T(""));
	m_lvConfigSection.SetItemText(nIndex, 1, _T("保存记录筛选"));
	m_lvConfigSection.SetItemData(nIndex, (DWORD_PTR)nIndex);

	nIndex = m_lvConfigSection.InsertItem(m_lvConfigSection.GetItemCount(), _T(""));
	m_lvConfigSection.SetItemText(nIndex, 1, _T("常规设置"));
	m_lvConfigSection.SetItemData(nIndex, (DWORD_PTR)nIndex);
	InitControls();
	m_lvConfigSection.SetItemState(0, LVNI_SELECTED, LVNI_SELECTED);
	DisplayGroup(0);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CSysConfig::OnOK()
{

	//CDialog::OnOK();
}

void CSysConfig::OnCancel()
{
	DestroyWindow();
	//CDialog::OnCancel();
}

void CSysConfig::PostNcDestroy()
{
	CDialog::PostNcDestroy();
	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	pIrcMain->m_SysConfig = NULL;
	delete this;
}

void CSysConfig::InitControls()
{
	CIRC_UIApp* pApp = static_cast<CIRC_UIApp*>(AfxGetApp());
	CConfig* pConfig = pApp->m_Config;
	((CButton*)GetDlgItem(IDC_RD_DIRECT))->SetCheck(pConfig->m_MsgTipType == MSG_TIP_TYPE_POPUP);
	((CButton*)GetDlgItem(IDC_RD_FLASH))->SetCheck(pConfig->m_MsgTipType == MSG_TIP_TYPE_FLASH);

	((CEdit*)GetDlgItem(IDC_EDT_MSGPATH))->SetWindowText(pConfig->m_strMsgPath.c_str());

	((CButton*)GetDlgItem(IDC_CB_FRIENDTIP))->SetCheck(!pConfig->OnLineTip(ONLINE_TIP_FRIEND) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_FOETIP))->SetCheck(!pConfig->OnLineTip(ONLINE_TIP_FOE) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_GAME))->SetCheck(!pConfig->OnLineTip(ONLINE_TIP_GAME) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_IRC))->SetCheck(!pConfig->OnLineTip(ONLINE_TIP_IRC) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_WORLD))->SetCheck(!pConfig->MsgChannel(MSG_CHANNEL_WORLD) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_SAMECITY))->SetCheck(!pConfig->MsgChannel(MSG_CHANNEL_SAMECITY) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_ORG))->SetCheck(!pConfig->MsgChannel(MSG_CHANNEL_ORG) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_GM))->SetCheck(!pConfig->MsgChannel(MSG_CHANNEL_GM) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_FRIEND))->SetCheck(!pConfig->MsgPriChatType(MSG_PRICHAT_TYPE_FRIEND) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_FOE))->SetCheck(!pConfig->MsgPriChatType(MSG_PRICHAT_TYPE_FOE) ? BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_CB_STRANGER))->SetCheck(!pConfig->MsgPriChatType(MSG_PRICHAT_TYPE_STRANGER) ?  BST_UNCHECKED : BST_CHECKED);

	((CButton*)GetDlgItem(IDC_RD_TRAY))->SetCheck(pConfig->m_CloseType == IRC_CLOSE_TIP_TRAY);
	((CButton*)GetDlgItem(IDC_RD_CLOSE))->SetCheck(pConfig->m_CloseType == IRC_CLOSE_TIP_CLOSE);

	((CButton*)GetDlgItem(IDC_CB_CLOSENOTIP))->SetCheck(pConfig->m_DispCloseTip ?  BST_UNCHECKED : BST_CHECKED);
}
void CSysConfig::OnNMClickLstSection(NMHDR *pNMHDR, LRESULT *pResult)
{
	CListCtrl* pListCtrl = static_cast<CListCtrl*>(GetDlgItem(IDC_LST_SECTION));

	CPoint point;
	GetCursorPos(&point);


	pListCtrl->ScreenToClient(&point);
	LVHITTESTINFO lvinfo;
	lvinfo.pt = point;
	lvinfo.flags = LVHT_ABOVE;

	int nIndex = pListCtrl->SubItemHitTest(&lvinfo);
	if (nIndex != -1)
	{
		int nItemData = (int)m_lvConfigSection.GetItemData(nIndex);
		DisplayGroup(nItemData);
	}
	*pResult = 0;
}

void CSysConfig::DisplayGroup(int nIndex)
{
	//1
	GetDlgItem(IDC_RD_DIRECT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RD_FLASH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GB_MSGTIPTYPE)->ShowWindow(SW_HIDE);

	//2
	GetDlgItem(IDC_EDT_MSGPATH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTN_OPENFILE)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BTB_CLEARMSG)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GROUP_MSGPATH)->ShowWindow(SW_HIDE);

	//3
	GetDlgItem(IDC_CB_FRIENDTIP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_FOETIP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_GAME)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_IRC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GROUP_ONOFFTIPS)->ShowWindow(SW_HIDE);

	//4
	GetDlgItem(IDC_CB_WORLD)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_SAMECITY)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_ORG)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_GM)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GROUP_CHANNEL)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_CB_FRIEND)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_FOE)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_STRANGER)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GROUP_PRIVATECHAT)->ShowWindow(SW_HIDE);

	//5
	GetDlgItem(IDC_RD_TRAY)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RD_CLOSE)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CB_CLOSENOTIP)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_GROUP_CLOSE)->ShowWindow(SW_HIDE);

	switch(nIndex)
	{
	case 0:
		{
			GetDlgItem(IDC_RD_DIRECT)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RD_FLASH)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_GB_MSGTIPTYPE)->ShowWindow(SW_SHOW);
		}
		break;
	case 1:
		{
			CRect rectMsgType;
			GetDlgItem(IDC_GB_MSGTIPTYPE)->GetWindowRect(&rectMsgType);
			CRect rectMsgPath;
			GetDlgItem(IDC_GROUP_MSGPATH)->GetWindowRect(&rectMsgPath);
			int nHeight = rectMsgPath.Height();
			int nDiff = rectMsgPath.top - rectMsgType.top;
			rectMsgPath.top = rectMsgType.top;
			rectMsgPath.bottom = rectMsgPath.top + nHeight;
			ScreenToClient(&rectMsgPath);
			GetDlgItem(IDC_GROUP_MSGPATH)->MoveWindow(&rectMsgPath);
			
			CRect rect;
			GetDlgItem(IDC_EDT_MSGPATH)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_EDT_MSGPATH)->MoveWindow(&rect);

			GetDlgItem(IDC_BTN_OPENFILE)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_BTN_OPENFILE)->MoveWindow(&rect);

			GetDlgItem(IDC_BTB_CLEARMSG)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_BTB_CLEARMSG)->MoveWindow(&rect);

			GetDlgItem(IDC_EDT_MSGPATH)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_BTN_OPENFILE)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_BTB_CLEARMSG)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_GROUP_MSGPATH)->ShowWindow(SW_SHOW);

		}
		break;
	case 2:
		{
			CRect rectMsgType;
			GetDlgItem(IDC_GB_MSGTIPTYPE)->GetWindowRect(&rectMsgType);
			CRect rectTips;
			GetDlgItem(IDC_GROUP_ONOFFTIPS)->GetWindowRect(&rectTips);
			int nHeight = rectTips.Height();
			int nDiff = rectTips.top - rectMsgType.top;
			rectTips.top = rectMsgType.top;
			rectTips.bottom = rectTips.top + nHeight;
			ScreenToClient(&rectTips);
			GetDlgItem(IDC_GROUP_ONOFFTIPS)->MoveWindow(&rectTips);

			CRect rect;
			GetDlgItem(IDC_CB_FRIENDTIP)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_FRIENDTIP)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_FOETIP)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient((&rect));
			GetDlgItem(IDC_CB_FOETIP)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_GAME)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_GAME)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_IRC)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_IRC)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_FRIENDTIP)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_FOETIP)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_GAME)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_IRC)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_GROUP_ONOFFTIPS)->ShowWindow(SW_SHOW);
		}
	    break;
	case 3:
		{
			CRect rectMsgType;
			GetDlgItem(IDC_GB_MSGTIPTYPE)->GetWindowRect(&rectMsgType);
			CRect rectChannel;
			GetDlgItem(IDC_GROUP_CHANNEL)->GetWindowRect(&rectChannel);
			int nDiffChannel = rectChannel.top - rectMsgType.top;
			int nHeight = rectChannel.Height();
			rectChannel.top = rectMsgType.top;
			rectChannel.bottom = rectChannel.top + nHeight;
			ScreenToClient(&rectChannel);
			GetDlgItem(IDC_GROUP_CHANNEL)->MoveWindow(&rectChannel);

			CRect rect;
			GetDlgItem(IDC_CB_WORLD)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top -= nDiffChannel;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_WORLD)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_SAMECITY)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiffChannel;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_SAMECITY)->MoveWindow(&rect);
			
			GetDlgItem(IDC_CB_ORG)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top -= nDiffChannel;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_ORG)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_GM)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top -= nDiffChannel;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_GM)->MoveWindow(&rect);


			GetDlgItem(IDC_CB_WORLD)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_SAMECITY)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_ORG)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_GM)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_GROUP_CHANNEL)->ShowWindow(SW_SHOW);


			CRect rectPri;
			GetDlgItem(IDC_GROUP_PRIVATECHAT)->GetWindowRect(&rectPri);
			nHeight = rectPri.Height();
			rectPri.top -= nDiffChannel;
			rectPri.bottom = rectPri.top + nHeight;
			ScreenToClient(&rectPri);
			GetDlgItem(IDC_GROUP_PRIVATECHAT)->MoveWindow(&rectPri);

			GetDlgItem(IDC_CB_FRIEND)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top -= nDiffChannel;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_FRIEND)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_FOE)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top -= nDiffChannel;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_FOE)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_STRANGER)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top -= nDiffChannel;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_STRANGER)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_FRIEND)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_FOE)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_STRANGER)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_GROUP_PRIVATECHAT)->ShowWindow(SW_SHOW);

		}
	    break;
	case 4:
		{
			CRect rectMsgType;
			GetDlgItem(IDC_GB_MSGTIPTYPE)->GetWindowRect(&rectMsgType);
			CRect rectClose;
			GetDlgItem(IDC_GROUP_CLOSE)->GetWindowRect(&rectClose);
			int nDiff = rectClose.top - rectMsgType.top;
			int nHeight = rectClose.Height();
			rectClose.top = rectMsgType.top;
			rectClose.bottom = rectClose.top + nHeight;
			ScreenToClient(&rectClose);
			GetDlgItem(IDC_GROUP_CLOSE)->MoveWindow(&rectClose);

			CRect rect;
			GetDlgItem(IDC_RD_TRAY)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_RD_TRAY)->MoveWindow(&rect);

			GetDlgItem(IDC_RD_CLOSE)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_RD_CLOSE)->MoveWindow(&rect);

			GetDlgItem(IDC_CB_CLOSENOTIP)->GetWindowRect(&rect);
			nHeight = rect.Height();
			rect.top = rect.top - nDiff;
			rect.bottom = rect.top + nHeight;
			ScreenToClient(&rect);
			GetDlgItem(IDC_CB_CLOSENOTIP)->MoveWindow(&rect);

			GetDlgItem(IDC_RD_TRAY)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_RD_CLOSE)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CB_CLOSENOTIP)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_GROUP_CLOSE)->ShowWindow(SW_SHOW);
		}
		break;
	default:
	    break;
	}
}
void CSysConfig::UpdateConfig()
{
	CIRC_UIApp* pApp = static_cast<CIRC_UIApp*>(AfxGetApp());
	CConfig* pConfig = pApp->m_Config;
	pConfig->m_MsgTipType = ((CButton*)GetDlgItem(IDC_RD_DIRECT))->GetCheck() == BST_CHECKED ? MSG_TIP_TYPE_POPUP : MSG_TIP_TYPE_FLASH;
	pConfig->m_MsgTipType = ((CButton*)GetDlgItem(IDC_RD_FLASH))->GetCheck() == BST_CHECKED ? MSG_TIP_TYPE_FLASH: MSG_TIP_TYPE_POPUP;


	CString strTmp;
	((CEdit*)GetDlgItem(IDC_EDT_MSGPATH))->GetWindowText(strTmp);
	pConfig->m_strMsgPath = (LPCTSTR)strTmp;

	if (((CButton*)GetDlgItem(IDC_CB_FRIENDTIP))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_OnLineTip.begin(), pConfig->m_OnLineTip.end(), ONLINE_TIP_FRIEND);
		if (Ite == pConfig->m_OnLineTip.end())
		{
			pConfig->m_OnLineTip.push_back(ONLINE_TIP_FRIEND);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_OnLineTip.begin(), pConfig->m_OnLineTip.end(), ONLINE_TIP_FRIEND);
		if (Ite != pConfig->m_OnLineTip.end())
		{
			pConfig->m_OnLineTip.erase(Ite);
		}
	}

	if (((CButton*)GetDlgItem(IDC_CB_FOETIP))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_OnLineTip.begin(), pConfig->m_OnLineTip.end(), ONLINE_TIP_FOE);
		if (Ite == pConfig->m_OnLineTip.end())
		{
			pConfig->m_OnLineTip.push_back(ONLINE_TIP_FOE);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_OnLineTip.begin(), pConfig->m_OnLineTip.end(), ONLINE_TIP_FOE);
		if (Ite != pConfig->m_OnLineTip.end())
		{
			pConfig->m_OnLineTip.erase(Ite);
		}
	}

	if (((CButton*)GetDlgItem(IDC_CB_GAME))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_OnLineTip.begin(), pConfig->m_OnLineTip.end(), ONLINE_TIP_GAME);
		if (Ite == pConfig->m_OnLineTip.end())
		{
			pConfig->m_OnLineTip.push_back(ONLINE_TIP_GAME);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_OnLineTip.begin(), pConfig->m_OnLineTip.end(), ONLINE_TIP_GAME);
		if (Ite != pConfig->m_OnLineTip.end())
		{
			pConfig->m_OnLineTip.erase(Ite);
		}
	}

	if (((CButton*)GetDlgItem(IDC_CB_IRC))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_OnLineTip.begin(), pConfig->m_OnLineTip.end(), ONLINE_TIP_IRC);
		if (Ite == pConfig->m_OnLineTip.end())
		{
			pConfig->m_OnLineTip.push_back(ONLINE_TIP_IRC);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_OnLineTip.begin(), pConfig->m_OnLineTip.end(), ONLINE_TIP_IRC);
		if (Ite != pConfig->m_OnLineTip.end())
		{
			pConfig->m_OnLineTip.erase(Ite);
		}
	}

	if (((CButton*)GetDlgItem(IDC_CB_WORLD))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgChannel.begin(), pConfig->m_MsgChannel.end(), MSG_CHANNEL_WORLD);
		if (Ite == pConfig->m_MsgChannel.end())
		{
			pConfig->m_MsgChannel.push_back(MSG_CHANNEL_WORLD);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgChannel.begin(), pConfig->m_MsgChannel.end(), MSG_CHANNEL_WORLD);
		if (Ite != pConfig->m_MsgChannel.end())
		{
			pConfig->m_MsgChannel.erase(Ite);
		}
	}


	if (((CButton*)GetDlgItem(IDC_CB_SAMECITY))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgChannel.begin(), pConfig->m_MsgChannel.end(), MSG_CHANNEL_SAMECITY);
		if (Ite == pConfig->m_MsgChannel.end())
		{
			pConfig->m_MsgChannel.push_back(MSG_CHANNEL_SAMECITY);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgChannel.begin(), pConfig->m_MsgChannel.end(), MSG_CHANNEL_SAMECITY);
		if (Ite != pConfig->m_MsgChannel.end())
		{
			pConfig->m_MsgChannel.erase(Ite);
		}
	}

	if (((CButton*)GetDlgItem(IDC_CB_ORG))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgChannel.begin(), pConfig->m_MsgChannel.end(), MSG_CHANNEL_ORG);
		if (Ite == pConfig->m_MsgChannel.end())
		{
			pConfig->m_MsgChannel.push_back(MSG_CHANNEL_ORG);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgChannel.begin(), pConfig->m_MsgChannel.end(), MSG_CHANNEL_ORG);
		if (Ite != pConfig->m_MsgChannel.end())
		{
			pConfig->m_MsgChannel.erase(Ite);
		}
	}

	if (((CButton*)GetDlgItem(IDC_CB_GM))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgChannel.begin(), pConfig->m_MsgChannel.end(), MSG_CHANNEL_GM);
		if (Ite == pConfig->m_MsgChannel.end())
		{
			pConfig->m_MsgChannel.push_back(MSG_CHANNEL_GM);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgChannel.begin(), pConfig->m_MsgChannel.end(), MSG_CHANNEL_GM);
		if (Ite != pConfig->m_MsgChannel.end())
		{
			pConfig->m_MsgChannel.erase(Ite);
		}
	}

	if (((CButton*)GetDlgItem(IDC_CB_FRIEND))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgPriChatType.begin(), pConfig->m_MsgPriChatType.end(), MSG_PRICHAT_TYPE_FRIEND);
		if (Ite == pConfig->m_MsgPriChatType.end())
		{
			pConfig->m_MsgPriChatType.push_back(MSG_PRICHAT_TYPE_FRIEND);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgPriChatType.begin(), pConfig->m_MsgPriChatType.end(), MSG_PRICHAT_TYPE_FRIEND);
		if (Ite != pConfig->m_MsgPriChatType.end())
		{
			pConfig->m_MsgPriChatType.erase(Ite);
		}
	}

	if (((CButton*)GetDlgItem(IDC_CB_FOE))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgPriChatType.begin(), pConfig->m_MsgPriChatType.end(), MSG_PRICHAT_TYPE_FOE);
		if (Ite == pConfig->m_MsgPriChatType.end())
		{
			pConfig->m_MsgPriChatType.push_back(MSG_PRICHAT_TYPE_FOE);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgPriChatType.begin(), pConfig->m_MsgPriChatType.end(), MSG_PRICHAT_TYPE_FOE);
		if (Ite != pConfig->m_MsgPriChatType.end())
		{
			pConfig->m_MsgPriChatType.erase(Ite);
		}
	}
	
	if (((CButton*)GetDlgItem(IDC_CB_FOE))->GetCheck())
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgPriChatType.begin(), pConfig->m_MsgPriChatType.end(), MSG_PRICHAT_TYPE_STRANGER);
		if (Ite == pConfig->m_MsgPriChatType.end())
		{
			pConfig->m_MsgPriChatType.push_back(MSG_PRICHAT_TYPE_STRANGER);
		}
	}
	else
	{
		vector<int>::iterator Ite = find(pConfig->m_MsgPriChatType.begin(), pConfig->m_MsgPriChatType.end(), MSG_PRICHAT_TYPE_STRANGER);
		if (Ite != pConfig->m_MsgPriChatType.end())
		{
			pConfig->m_MsgPriChatType.erase(Ite);
		}
	}

	pConfig->m_CloseType = ((CButton*)GetDlgItem(IDC_RD_TRAY))->GetCheck() ? IRC_CLOSE_TIP_TRAY : IRC_CLOSE_TIP_CLOSE;
	pConfig->m_CloseType = ((CButton*)GetDlgItem(IDC_RD_CLOSE))->GetCheck() ? IRC_CLOSE_TIP_CLOSE : IRC_CLOSE_TIP_TRAY;

	pConfig->m_DispCloseTip = ((CButton*)GetDlgItem(IDC_CB_CLOSENOTIP))->GetCheck() == BST_UNCHECKED;
	pConfig->SaveConfig();
	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	pIrcMain->GetIrcInfo()->ChangeChatLogDir(pConfig->m_strMsgPath.c_str());

	unsigned short usFilter = 0;
	if (pConfig->MsgChannel(MSG_CHANNEL_WORLD))
	{
		usFilter |= 1;
	}
	if (pConfig->MsgChannel(MSG_CHANNEL_SAMECITY))
	{
		usFilter |= 2;
	}
	if (pConfig->MsgChannel(MSG_CHANNEL_ORG))
	{
		usFilter |= 4;
	}
	if (pConfig->MsgChannel(MSG_CHANNEL_GM))
	{
		usFilter |= 8;
	}

	if (pConfig->MsgPriChatType(MSG_PRICHAT_TYPE_FRIEND))
	{
		usFilter |= 16;
	}
	if (pConfig->MsgPriChatType(MSG_PRICHAT_TYPE_FOE))
	{
		usFilter |= 32;
	}
	if (pConfig->MsgPriChatType(MSG_PRICHAT_TYPE_STRANGER))
	{
		usFilter |=64;
	}
	pIrcMain->GetIrcInfo()->SetFilter(usFilter);
}

void CSysConfig::OnBnClickedBtnOpenfile()
{
	TCHAR DirPath[MAX_PATH];
	BROWSEINFO bi;
	ZeroMemory(&bi, sizeof(BROWSEINFO));
	ITEMIDLIST* pidl;

	bi.hwndOwner = ::GetActiveWindow();
	bi.pszDisplayName = DirPath;
	bi.lpszTitle = _T("请选择聊天记录的存储位置：");
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_USENEWUI | BIF_DONTGOBELOWDOMAIN | BIF_VALIDATE;
	bi.lpfn = SetDefaultDir;
	CIRC_UIApp* pIrcApp = static_cast<CIRC_UIApp*>(AfxGetApp());
	bi.lParam = (LPARAM)pIrcApp->m_Config->m_strMsgPath.c_str();
	bi.iImage = 0;
	pidl = SHBrowseForFolder(&bi);
	if (pidl == NULL)
	{
		DirPath[0] = 0;
	}
	if (!SHGetPathFromIDList(pidl, DirPath))
	{
		DirPath[0] = 0;
	}
	LPMALLOC pMalloc;
	if (SUCCEEDED(SHGetMalloc(&pMalloc)))
	{
		pMalloc->Free(pidl);
		(void)pMalloc->Release();
	}
	if (lstrlen(DirPath) != 0)
	{
		((CEdit*)GetDlgItem(IDC_EDT_MSGPATH))->SetWindowText(DirPath);
	}
}

void CSysConfig::OnBnClickedBtbClearmsg()
{
	int nRet = MessageBox(_T("你确定要清空聊天记录吗？"), _T("清空确认"), MB_OKCANCEL | MB_ICONQUESTION | MB_APPLMODAL);
	if (nRet != IDOK)
	{
		return;
	}
	CIRC_UIApp* pApp = static_cast<CIRC_UIApp*>(AfxGetApp());
	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(pApp->m_pMainWnd);
	pIrcMain->GetIrcInfo()->ClearLog(-1);	
}

int CALLBACK CSysConfig::SetDefaultDir(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	if (uMsg == BFFM_INITIALIZED)
	{
		::SendMessage(hWnd, BFFM_SETSELECTION | BFFM_SETEXPANDED, TRUE, (LPARAM)lpData);
	}
	return 0;
}
BOOL CSysConfig::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CSysConfig::OnSysCommand(UINT nID, LPARAM lParam)
{
	if (nID == SC_MINIMIZE)
	{
		ShowWindow(SW_MINIMIZE);
	}
	else if (nID == SC_MAXIMIZE)
	{

	}
	else
		CDialog::OnSysCommand(nID, lParam);
}