﻿// IRC_UI.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "IRC_UIDlg.h"
#include "LoginDlg.h"
#include "Helper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CIrcCmdInfo::CIrcCmdInfo()
: m_IpAddr(""), m_Port(0), m_ServerType(0), m_ParamCount(0)
{

}

CIrcCmdInfo::~CIrcCmdInfo()
{

}

void CIrcCmdInfo::ParseParam(const char *pszParam, BOOL bFlag, BOOL bLast)
{
	if (m_ParamCount == 0)
	{
		m_IpAddr = pszParam;
	}
	else if (m_ParamCount == 1)
	{
		m_Port = atoi(pszParam);
	}
	else if (m_ParamCount == 3)
	{
		m_ServerType = atoi(pszParam);
	}
	m_ParamCount++;
}

// CIRC_UIApp

BEGIN_MESSAGE_MAP(CIRC_UIApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CIRC_UIApp 构造

CIRC_UIApp::CIRC_UIApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CIRC_UIApp 对象

CIRC_UIApp theApp;


// CIRC_UIApp 初始化

BOOL CIRC_UIApp::InitInstance()
{
	//解析命令行参数
	ParseCommandLine(m_CmdInfo);

	::AfxInitRichEdit2();
	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);

	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_Config = new CConfig;

	//  [6/24/2009 hemeng]
	string strUIConfigFile = CHelper::GetAppWorkDir();
	strUIConfigFile += "IRCUIConfig.xml";

	if(!CUISourceManager::GetInstance()->Initial(strUIConfigFile))
	{
		OutputDebugString("Fail to initial source manager!");
	}

	CIRC_UIDlg dlg;

	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此处放置处理何时用“确定”来关闭
		//  对话框的代码
		m_Config->m_Account = dlg.m_Account;
		m_Config->m_Pwd = dlg.m_Pwd;
		m_Config->m_RoleName = dlg.m_RoleName;
		m_Config->m_SrvIp = dlg.m_ServerIp;
		m_Config->m_SrvPort = dlg.m_ServerPort;
		m_Config->m_SrvType = dlg.m_ServerType;
		m_Config->SaveConfig();
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用“取消”来关闭
		//  对话框的代码
	}


	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

int CIRC_UIApp::ExitInstance()
{
	GdiplusShutdown(m_gdiplusToken);
	delete m_Config;

	// release source [6/24/2009 hemeng]
	CUISourceManager::GetInstance()->Release();

	// begin [7/23/2009 hemeng]
	CPackageSourceManager::GetInstance()->Release();
	// end [7/23/2009 hemeng]

	return CWinApp::ExitInstance();
}