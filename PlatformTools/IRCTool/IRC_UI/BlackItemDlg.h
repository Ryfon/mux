﻿#pragma once
#include "afxcmn.h"

// CBlackItemDlg 对话框

class CBlackItemDlg : public CDialog
{
	DECLARE_DYNAMIC(CBlackItemDlg)

public:
	CBlackItemDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CBlackItemDlg();

// 对话框数据
	enum { IDD = IDD_DLG_BLACKITEM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_BlackList;
	virtual BOOL OnInitDialog();
public:
	void GetBlackItem(vector<string>& vectBlackItem);
	bool IsBlackItem(const string& strRoleName);

};
