﻿#include "StdAfx.h"
#include "UIBmpSource.h"
#include "Helper.h"

CUIBmpSource::CUIBmpSource(void)
{
	m_strSourcType = "bmp";
}

CUIBmpSource::~CUIBmpSource(void)
{
}

bool	CUIBmpSource::Initial(string strPath,bool bSpeical)
{
	if (strPath == "")
	{
		return false;
	}

	if(!CHelper::LoadBitmapFromBMPFile(strPath.c_str(),m_hdSource,bSpeical))
	{
		return false;
	}

	return true;
}
