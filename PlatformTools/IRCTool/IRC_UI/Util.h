﻿#pragma once

class CUtil
{
public:
	CUtil(void){};
	virtual ~CUtil(void){};
public:
	static BOOL CopyToClipBoard(CString& strData, CWnd& pWndOwner);
	static BOOL PasteFromClipBoard(CRichEditCtrl& rchTo, CWnd& pWndOwner);
	static CString GetSel(CRichEditCtrl& rch);
	static CString GetCurTime();
};
