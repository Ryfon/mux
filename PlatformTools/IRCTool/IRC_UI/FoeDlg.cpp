﻿// FoeDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "FoeDlg.h"
#include "IRC_UIDlg.h"

// CFoeDlg 对话框

IMPLEMENT_DYNAMIC(CFoeDlg, CDialog)

CFoeDlg::CFoeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFoeDlg::IDD, pParent)
{
	m_FoeMap.clear();
}

CFoeDlg::~CFoeDlg()
{
	for (map<unsigned long, FoeInfo_I*>::iterator Ite = m_FoeMap.begin(); Ite != m_FoeMap.end(); Ite++)
	{
		delete Ite->second;
	}

	m_FoeMap.clear();
}

void CFoeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FOELIST, m_FoeList);
}


BEGIN_MESSAGE_MAP(CFoeDlg, CDialog)
	ON_NOTIFY(NM_RCLICK, IDC_FOELIST, &CFoeDlg::OnNMRclickFoelist)
	ON_COMMAND(ID_FOES_DETAIL, &CFoeDlg::OnFoesDetail)
	ON_UPDATE_COMMAND_UI(ID_FOES_DETAIL, &CFoeDlg::OnUpdateFoesDetail)
	ON_COMMAND(ID_FOES_PRICHAT, &CFoeDlg::OnFoesPrichat)
	ON_UPDATE_COMMAND_UI(ID_FOES_PRICHAT, &CFoeDlg::OnUpdateFoesPrichat)
	ON_COMMAND(ID_FOES_COPYNAME, &CFoeDlg::OnFoesCopyname)
	ON_UPDATE_COMMAND_UI(ID_FOES_COPYNAME, &CFoeDlg::OnUpdateFoesCopyname)
	ON_COMMAND(ID_FOES_BAN, &CFoeDlg::OnFoesBan)
	ON_UPDATE_COMMAND_UI(ID_FOES_BAN, &CFoeDlg::OnUpdateFoesBan)
	ON_NOTIFY(HDN_ITEMCLICK, 0, &CFoeDlg::OnHdnItemclickFoelist)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_FOELIST, &CFoeDlg::OnNMCustomdrawFoelist)
	ON_NOTIFY(NM_DBLCLK, IDC_FOELIST, &CFoeDlg::OnNMDblclkFoelist)
END_MESSAGE_MAP()


// CFoeDlg 消息处理程序

BOOL CFoeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	BITMAP BitMap;
	pIrcMain->m_IrcFlag.GetBitmap(&BitMap);
	m_ImgList.Create(BitMap.bmWidth, BitMap.bmHeight, ILC_COLORDDB | ILC_MASK, 1, 1);
	m_ImgList.Add(&pIrcMain->m_IrcFlag, (CBitmap*)NULL);

	m_UsrInfoDlg = NULL;
	m_CurPosFoe = NULL;
	m_FoeMenu.LoadMenu(IDR_FOES);
	CRect rectFoeInfo;
	m_FoeList.GetClientRect(&rectFoeInfo);
	int nColWidth = rectFoeInfo.Width() / 4;

	m_FoeList.InsertColumn(0, _T("姓名"), LVCFMT_CENTER, nColWidth);
	m_FoeList.InsertColumn(1, _T("职业"), LVCFMT_CENTER, nColWidth);
	m_FoeList.InsertColumn(2, _T("种族"), LVCFMT_CENTER, nColWidth);
	m_FoeList.InsertColumn(3, _T("性别"), LVCFMT_CENTER, nColWidth);

	m_FoeList.SetImageList(&m_ImgList, LVSIL_SMALL);
	ListView_SetExtendedListViewStyle(m_FoeList.m_hWnd, LVS_EX_FULLROWSELECT  | LVS_EX_HEADERDRAGDROP);

	m_BmpSortDown.LoadBitmap(IDB_BMP_SORTDOWN);
	m_BmpSortUp.LoadBitmap(IDB_BMP_SORTUP);

	m_SortImgList.Create(44, 44, ILC_COLORDDB | ILC_MASK, 2, 1);
	m_SortImgList.Add(&m_BmpSortUp, (CBitmap*)NULL);
	m_SortImgList.Add(&m_BmpSortDown, (CBitmap*)NULL);

	m_SortType = -1;

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CFoeDlg::AddFoeInfo(FoeInfo_I* pFoeInfo)
{
	map<unsigned long, FoeInfo_I*>::iterator Ite = m_FoeMap.find(pFoeInfo->foeID);
	if (Ite == m_FoeMap.end())
	{
		FoeInfo_I* pNewFoeInfo = new FoeInfo_I;
		memcpy(pNewFoeInfo, pFoeInfo, sizeof(FoeInfo_I));
		m_FoeMap[pNewFoeInfo->foeID] = pNewFoeInfo;

		int nIndex;
		int nItemCount = m_FoeList.GetItemCount();
		if (pNewFoeInfo->isOnline == 2)
		{
			nIndex = m_FoeList.InsertItem(nItemCount, pNewFoeInfo->nickName, 0);
		}
		else
		{
			nIndex = m_FoeList.InsertItem(nItemCount, pNewFoeInfo->nickName, -1);
		}
		
		CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);

		m_FoeList.SetItemText(nIndex, 1, pIrcMain->GetIrcInfo()->GetClassByClassID(pNewFoeInfo->playerClass).c_str());
		m_FoeList.SetItemText(nIndex, 2, pIrcMain->GetIrcInfo()->GetRaceByRaceID(pNewFoeInfo->playerRace).c_str());
		m_FoeList.SetItemText(nIndex, 3, pIrcMain->GetIrcInfo()->GetGender(pNewFoeInfo->playerSex).c_str());
		m_FoeList.SetItemData(nIndex, (DWORD_PTR)pNewFoeInfo);
		m_FoeList.SortItems(FoeInfoSort, -1);
	}
	else
	{
		((FoeInfo_I*)Ite->second)->isOnline = pFoeInfo->isOnline;
	}
}
void CFoeDlg::OnNMRclickFoelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	CMenu *psub = (CMenu *)m_FoeMenu.GetSubMenu(0);
	CPoint point;
	GetCursorPos(&point);

	m_FoeList.ScreenToClient(&point);
	LVHITTESTINFO lvinfo;
	lvinfo.pt = point;
	lvinfo.flags = LVHT_ABOVE;

	int nIndex = m_FoeList.SubItemHitTest(&lvinfo);
	if (nIndex != -1)
	{
		m_CurPosFoe = reinterpret_cast<FoeInfo_I*>(m_FoeList.GetItemData(nIndex));
		m_FoeList.ClientToScreen(&point);
		map<string, FoeInfo_I*>::iterator Ite = m_BanFoeMap.find(m_CurPosFoe->nickName);
		psub->ModifyMenu(ID_FOES_BAN, MF_BYCOMMAND, ID_FOES_BAN, Ite != m_BanFoeMap.end() ? _T("解除屏蔽") : _T("屏蔽"));
		if (m_CurPosFoe->isOnline == 0)
		{
			psub->EnableMenuItem(ID_FOES_PRICHAT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		}
		else
		{
			psub->EnableMenuItem(ID_FOES_PRICHAT, MF_BYCOMMAND | MF_ENABLED);
		}
		psub->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}
	else
	{
		m_CurPosFoe = NULL;
	}

	*pResult = 0;
}

void CFoeDlg::OnFoesDetail()
{
	if (m_UsrInfoDlg != NULL)
	{
		m_UsrInfoDlg->DestroyWindow();
	}

	m_UsrInfoDlg = new CUsrInfoDlg(this);
	m_UsrInfoDlg->Create(CUsrInfoDlg::IDD, this);
	m_UsrInfoDlg->SetParentInfo(2, this);
	CRect rectUsrInfo;
	m_UsrInfoDlg->GetWindowRect(&rectUsrInfo);

	CPoint pnt;
	GetCursorPos(&pnt);

	m_UsrInfoDlg->MoveWindow(pnt.x, pnt.y, rectUsrInfo.Width(), rectUsrInfo.Height());
	m_UsrInfoDlg->UpdateUsrInfo(m_CurPosFoe);
	m_UsrInfoDlg->ShowWindow(SW_SHOW);
}

void CFoeDlg::OnUpdateFoesDetail(CCmdUI *pCmdUI)
{
	
}

void CFoeDlg::OnFoesPrichat()
{
	CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	if (m_CurPosFoe)
	{
		pIrcMain->NewPrivateChat(m_CurPosFoe->nickName);
	}
}

void CFoeDlg::OnUpdateFoesPrichat(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_CurPosFoe->isOnline != 0);	
}

void CFoeDlg::OnFoesCopyname()
{
	if (OpenClipboard())
	{
		HGLOBAL hClipBuffer;
		char* pBuffer;
		EmptyClipboard();
		hClipBuffer = GlobalAlloc(GMEM_DDESHARE, strlen(m_CurPosFoe->nickName) + 1);
		pBuffer = (char*)GlobalLock(hClipBuffer);
		strcpy(pBuffer, m_CurPosFoe->nickName);
		GlobalUnlock(hClipBuffer);
		SetClipboardData(CF_TEXT, hClipBuffer);
		CloseClipboard();
	}
}

void CFoeDlg::OnUpdateFoesCopyname(CCmdUI *pCmdUI)
{

}

void CFoeDlg::OnFoesBan()
{
	map<string, FoeInfo_I*>::iterator Ite = m_BanFoeMap.find(m_CurPosFoe->nickName);
	if (Ite == m_BanFoeMap.end())
	{
		m_BanFoeMap[m_CurPosFoe->nickName] = m_CurPosFoe;
		m_FoeMenu.ModifyMenu(ID_FOES_BAN, MF_BYCOMMAND, ID_FOES_BAN, _T("解除屏蔽"));
	}
	else
	{
		m_BanFoeMap.erase(Ite);
		m_FoeMenu.ModifyMenu(ID_FOES_BAN, MF_BYCOMMAND, ID_FOES_BAN, _T("屏蔽"));
	}
}

void CFoeDlg::OnUpdateFoesBan(CCmdUI *pCmdUI)
{
	
}

void CFoeDlg::FoeInfoUpdate(FoeInfo_I* pFoeInfo)
{
	map<unsigned long, FoeInfo_I*>::iterator Ite = m_FoeMap.find(pFoeInfo->foeID);
	if (Ite != m_FoeMap.end())
	{
		Ite->second->isOnline = pFoeInfo->isOnline;
		Ite->second->playerLevel = pFoeInfo->playerLevel;
		strncpy(Ite->second->GuildName, pFoeInfo->GuildName, strlen(pFoeInfo->GuildName));
		strncpy(Ite->second->FereName, pFoeInfo->FereName, strlen(pFoeInfo->FereName));		
		LVFINDINFO lvInfo;
		lvInfo.flags = LVFI_STRING | LVFI_PARTIAL;
		lvInfo.psz = Ite->second->nickName;
		int nIndex = m_FoeList.FindItem(&lvInfo);
		if (nIndex != -1)
		{
			LVITEM lvItem;
			ZeroMemory(&lvItem, sizeof(LVITEM));
			lvItem.iItem = nIndex;
			lvItem.mask = LVIF_IMAGE;
			lvItem.iImage = Ite->second->isOnline == 2 ? 0 : -1;
			m_FoeList.SetItem(&lvItem);
			if (m_SortType == -1)
			{
				m_FoeList.SortItems(FoeInfoSort, -1);
			}
		}

	}
}

int CFoeDlg::GetFoeOnLineType(unsigned long ulFoeID)
{
	int nFoeOnLineType = -1;
	map<unsigned long ,FoeInfo_I*>::iterator Ite = m_FoeMap.find(ulFoeID);
	if (Ite != m_FoeMap.end())
	{
		nFoeOnLineType = Ite->second->isOnline;
	}
	return nFoeOnLineType;
}

CString CFoeDlg::GetFoeName(unsigned long ulFoeID)
{
	map<unsigned long ,FoeInfo_I*>::iterator Ite = m_FoeMap.find(ulFoeID);
	if (Ite != m_FoeMap.end())
	{
		return Ite->second->nickName;
	}
	return _T("");
}

bool CFoeDlg::IsFoe(const string& strRoleName)
{
	bool bRet = false;
	for (map<unsigned long ,FoeInfo_I*>::iterator Ite = m_FoeMap.begin(); Ite != m_FoeMap.end(); Ite++)
	{
		if (strRoleName == Ite->second->nickName)
		{
			bRet = true;
			break;
		}
	}
	return bRet;
}


int CALLBACK CFoeDlg::FoeInfoSort(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	int nSortType = (int)lParamSort;
	FoeInfo_I* pFoeInfo1 = reinterpret_cast<FoeInfo_I*>(lParam1);
	FoeInfo_I* pFoeInfo2 = reinterpret_cast<FoeInfo_I*>(lParam2);
	int nResult = 0;
	CIRC_UIDlg* pIrcMain = reinterpret_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
	switch(nSortType)
	{
	case -1:
		{
			nResult = pFoeInfo2->isOnline - pFoeInfo1->isOnline;
			if (nResult == 0)
			{
				nResult = pFoeInfo1->joinID - pFoeInfo2->joinID;
			}
		}
		break;
	case 0:
		{
			nResult = strcmp(pFoeInfo1->nickName, pFoeInfo2->nickName);
		}
		break;
	case 1:
		{
			nResult = strcmp(pFoeInfo2->nickName, pFoeInfo1->nickName);
		}
		break;
	case 2:
		{
			nResult = strcmp(pIrcMain->GetIrcInfo()->GetClassByClassID(pFoeInfo1->playerClass).c_str(), pIrcMain->GetIrcInfo()->GetClassByClassID(pFoeInfo2->playerClass).c_str());
		}
		break;
	case 3:
		{
			nResult = -(strcmp(pIrcMain->GetIrcInfo()->GetClassByClassID(pFoeInfo1->playerClass).c_str(), pIrcMain->GetIrcInfo()->GetClassByClassID(pFoeInfo2->playerClass).c_str()));
		}
		break;
	case 4:
		{
			nResult = strcmp(pIrcMain->GetIrcInfo()->GetRaceByRaceID(pFoeInfo1->playerRace).c_str(), pIrcMain->GetIrcInfo()->GetRaceByRaceID(pFoeInfo2->playerRace).c_str());
		}
		break;
	case 5:
		{
			nResult = -(strcmp(pIrcMain->GetIrcInfo()->GetRaceByRaceID(pFoeInfo1->playerRace).c_str(), pIrcMain->GetIrcInfo()->GetRaceByRaceID(pFoeInfo2->playerRace).c_str()));
		}
		break;
	case 6:
		{
			nResult = pFoeInfo1->playerSex - pFoeInfo2->playerSex;
		}
		break;
	case 7:
		{
			nResult = pFoeInfo2->playerSex - pFoeInfo1->playerSex;
		}
	default:
		break;
	}
	return nResult;
}
void CFoeDlg::OnHdnItemclickFoelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	//排序图标
	int nColIndex = m_SortType / 2;
	if (nColIndex != phdr->iItem)
	{
		m_SortType = phdr->iItem * 2;
	}
	else
	{
		m_SortType = m_SortType == nColIndex * 2 ? ++m_SortType : --m_SortType;
	}
	m_FoeList.SortItems(FoeInfoSort, (DWORD_PTR)m_SortType);
	*pResult = 0;
}

bool CFoeDlg::IsFoeBanned(const string& strRoleName)
{
	map<string, FoeInfo_I*>::iterator Ite = m_BanFoeMap.find(strRoleName);
	return Ite != m_BanFoeMap.end();
}

void CFoeDlg::OnNMCustomdrawFoelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = CDRF_DODEFAULT;

	LPNMLVCUSTOMDRAW pLvNMCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);
	switch(pLvNMCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		{
			*pResult = CDRF_NOTIFYITEMDRAW;
		}
		break;
	case CDDS_ITEMPREPAINT:
		{
			*pResult = CDRF_NOTIFYSUBITEMDRAW;
		}
		break;
	case CDDS_ITEMPREPAINT | CDDS_SUBITEM:
		{
			int nItem = static_cast<int>(pLvNMCD->nmcd.dwItemSpec);
			COLORREF clrTextColr;
			FoeInfo_I* pFoeInfo = reinterpret_cast<FoeInfo_I*>(m_FoeList.GetItemData(nItem));
			if (pFoeInfo->isOnline == 0)
			{
				clrTextColr = RGB(222,222,222);
			}
			else
			{
				clrTextColr = RGB(0, 0, 0);
			}
			pLvNMCD->clrText = clrTextColr;
		}
		break;
	default:
		break;
	}
}

void CFoeDlg::OnNMDblclkFoelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	CPoint point;
	GetCursorPos(&point);

	m_FoeList.ScreenToClient(&point);
	LVHITTESTINFO lvinfo;
	lvinfo.pt = point;
	lvinfo.flags = LVHT_ABOVE;

	int nIndex = m_FoeList.SubItemHitTest(&lvinfo);
	if (nIndex != -1)
	{
		m_CurPosFoe = reinterpret_cast<FoeInfo_I*>(m_FoeList.GetItemData(nIndex));
		m_FoeList.ClientToScreen(&point);
		if (m_CurPosFoe->isOnline != 0)
		{
			OnFoesPrichat();
		}
	}
	else
	{
		m_CurPosFoe = NULL;
	}
	*pResult = 0;
}

void CFoeDlg::ClearFoeInfo()
{
	m_FoeList.DeleteAllItems();
	m_CurPosFoe = NULL;
	m_SortType = -1;
	for (map<unsigned long, FoeInfo_I*>::iterator Ite = m_FoeMap.begin(); Ite != m_FoeMap.end(); Ite++)
	{
		delete Ite->second;
	}
	m_FoeMap.clear();
	
	m_BanFoeMap.clear();
}

void CFoeDlg::OffLine()
{
	for (map<unsigned long, FoeInfo_I*>::iterator Ite = m_FoeMap.begin(); Ite != m_FoeMap.end(); Ite++)
	{
		FoeInfo_I* pFofInfo = Ite->second;
		pFofInfo->isOnline = 0;
		FoeInfoUpdate(pFofInfo);
	}
}