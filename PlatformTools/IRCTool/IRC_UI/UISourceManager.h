﻿/** @file UISourceManager.h 
@brief UI资源管理
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2009-06-23
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#pragma once
#include "UIBmpSource.h"
#include "UIIconSource.h"

class CUISourceManager
{
public:
	CUISourceManager(void);
	~CUISourceManager(void);

	static	CUISourceManager*	GetInstance();
	void Release();

	bool	Initial(string strConfigFilePath);
	
	bool			GetSourceHandle(string strDesce,HANDLE& hd0);
	bool			GetSourceHandle(string strDesce,HBITMAP& hd);
	bool			GetSourceHandle(string strDesce,HICON&hd);
	string			GetSourceType(string strDesce);
	size_t			GetSourceCount()	{return m_mapSource.size();}; 
	int			GetTransAlpha()		{return m_sTransparentAlpha;};

protected:
	struct stConfigInfo 
	{
		string	strDes;
		string	strSourcePath;
		bool	bSpecial;
	};

	bool			_GetSource(string strDesc, CUISource& kSource);

	bool			_LoadConfigFile(string strFilePath);
	bool			_LoadXML(string strFilePath, vector<stConfigInfo>& vConfigInfos);
	void			_LoadSoure(vector<stConfigInfo> vConfigInfos);
	void			_AddSource(string strDes, string strSourcePath, bool bSpeicial = false);
	

	// strDesc 资源描述，即关联名称
	// kSource 关联资源
	map<string ,CUISource> m_mapSource;
	int	m_sTransparentAlpha;
};
