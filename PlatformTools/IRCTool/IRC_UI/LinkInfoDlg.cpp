﻿// LinkInfoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "LinkInfoDlg.h"
#include "IRC_UIDlg.h"
#include "PriChatDlg.h"
#include "ChatRecord.h"

// CLinkInfoDlg 对话框

IMPLEMENT_DYNAMIC(CLinkInfoDlg, CDialog)
const char* ClassDesc[] = 
{
	"",
	"剑士",
	"刺客",
	"圣骑士",
	"魔剑士",
	"弓箭手",
	"魔法师"
};

const char* RaceDesc[] =
{
	"",
	"人类",
	"艾卡",
	"精灵"
};

const char* GearTypeDesc[] = 
{
	"头盔",
	"护胸",
	"手套",
	"鞋子",
	"护肩",
	"披风",
	"耳环",
	"项链",
	"手镯",
	"戒指",
	"主手",
	"副手",
	"主手",
	"坐骑"
};

const char* ItemTypeDesc[] = 
{
	"装备",
	"消耗品",
	"普通物品",
	"技能书",
	"任务道具",
	"功能道具",
	"价值物道具",
	"蓝图道具",
	"时装道具"

};
CLinkInfoDlg::CLinkInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLinkInfoDlg::IDD, pParent)
{

}

CLinkInfoDlg::~CLinkInfoDlg()
{

}

void CLinkInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_REDT_TIPINFO, m_Tips);
}


BEGIN_MESSAGE_MAP(CLinkInfoDlg, CDialog)
END_MESSAGE_MAP()


// CLinkInfoDlg 消息处理程序

void CLinkInfoDlg::OnOK()
{

	//CDialog::OnOK();
}

void CLinkInfoDlg::OnCancel()
{
	DestroyWindow();
	//CDialog::OnCancel();
}

void CLinkInfoDlg::PostNcDestroy()
{
	CDialog::PostNcDestroy();
	if (m_ParentType == 1)
	{
		CIRC_UIDlg* pIrcMain = static_cast<CIRC_UIDlg*>(AfxGetApp()->m_pMainWnd);
		pIrcMain->m_LinkInfoDlg = NULL;
	}
	if (m_ParentType == 2)
	{
		CPriChatDlg* pPriChatDlg = static_cast<CPriChatDlg*>(m_Parent);
		pPriChatDlg->m_LinkInfoDlg = NULL;
	}
	if (m_ParentType == 3)
	{
		CChatRecord* pChatRecod = static_cast<CChatRecord*>(m_Parent);
		pChatRecod->m_LinkInfoDlg = NULL;
	}
	delete this;
}

BOOL CLinkInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowLong(GetSafeHwnd(), GWL_EXSTYLE, GetWindowLong(GetSafeHwnd(), GWL_EXSTYLE) ^ WS_EX_LAYERED);
	int iTransAlpha = CUISourceManager::GetInstance()->GetTransAlpha();
	SetLayeredWindowAttributes(0, iTransAlpha, LWA_ALPHA);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CLinkInfoDlg::UpdateTips(int nItemType, int nLevelUp, int nAddID, int nWear, int nMassLevel, vector<string>& vectItemInfo)
{
	if (nItemType == 0)
	{
		UpdateEquipItemTips(nLevelUp, nAddID, nWear, nMassLevel, vectItemInfo);
	}
	if (nItemType == 1)
	{
		UpdateItemTips(nLevelUp, nAddID, nWear, nMassLevel, vectItemInfo);
	}
}

void CLinkInfoDlg::UpdateItemTips(int nLevelUp, int nAddID, int nWear, int nMassLevel, vector<string>& vectItemInfo)
{
	SetWindowText(_T("道具提示"));
	m_Tips.SetSel(0, -1);
	m_Tips.ReplaceSel(_T(""));
	if (vectItemInfo.size() == 0)
	{
		m_Tips.SetWindowText(_T("找不到道具链接信息"));
		return;
	}
	int nTextLen = 0;
	//道具名字
	CIRC_UIDlg* pMainDlg = (CIRC_UIDlg*)(AfxGetApp()->m_pMainWnd);
	CHARFORMAT2 cf2;
	if (nMassLevel == -1)
	{
		nMassLevel = atoi(vectItemInfo[8].c_str());
	}
	InitCharFormat(12, pMainDlg->GetItemColor(nMassLevel), "宋体", cf2);
	m_Tips.SetDefaultCharFormat(cf2);
	CString strItemTip(vectItemInfo[0].c_str());
	m_Tips.AppendMsg(strItemTip);
	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();
	//绑定状态
	strItemTip = _T("")	;
	if (atoi(vectItemInfo[10].c_str()) == 1)
	{
		strItemTip += _T("已绑定");
	}
	else if (atoi(vectItemInfo[10].c_str()) == 3)
	{
		strItemTip += _T("拾取绑定");
	}
	
	vector<int> vectInt;
	CConfig::Split(vectItemInfo[11], vectInt, ',');
	vector<int>::iterator Ite = find(vectInt.begin(), vectInt.end(), 0);
	if (Ite != vectInt.end())
	{
		strItemTip += _T(" 不可丢弃");
	}
	Ite = find(vectInt.begin(), vectInt.end(), 1);
	if (Ite != vectInt.end())
	{
		strItemTip += _T(" 不可交易");
	}
	else
	{
		Ite = find(vectInt.begin(), vectInt.end(), 2);
		if (Ite != vectInt.end())
		{
			strItemTip += _T(" 不可交易");
		}
	}

	Ite = find(vectInt.begin(), vectInt.end(), 3);
	if (Ite != vectInt.end())
	{
		strItemTip += _T(" 死亡后不掉落");
	}

	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);

	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//使用等级 道具类型
	strItemTip.Format(_T("使用等级%s"), vectItemInfo[6].c_str());
	m_Tips.AppendMsg(strItemTip);
	strItemTip.Format(_T("\t%s"), ItemTypeDesc[atoi(vectItemInfo[1].c_str())]);
	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);

	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//道具说明
	strItemTip.Format(_T("%s"), vectItemInfo[13].c_str());
	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(0, 255, 0), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);

	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//剩余时间
	vectInt.clear();
	CConfig::Split(vectItemInfo[12], vectInt, ',');	
	Ite = find(vectInt.begin(), vectInt.end(), 4);
	if (Ite != vectInt.end())
	{
		strItemTip.Format(_T("剩余时间: %d"), nWear);
		m_Tips.AppendMsg(strItemTip);
		m_Tips.SetSel(nTextLen - 1, -1);
		InitCharFormat(10, RGB(255, 255, 0), "宋体", cf2);
		m_Tips.SetSelectionCharFormat(cf2);

		m_Tips.AppendMsg(_T("\r\n"));
		nTextLen = m_Tips.GetCharCount();
	}

	//每层堆叠
	strItemTip.Format(_T("每层堆叠 %s"), vectItemInfo[7].c_str());
	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 0), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);

	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//出售价格
	strItemTip.Format(_T("出售价格: %s金"), vectItemInfo[5].c_str());
	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);

}

void CLinkInfoDlg::UpdateEquipItemTips(int nLevelUp, int nAddID, int nWear, int nMassLevel, vector<string>& vectItemInfo)
{
	SetWindowText(_T("道具提示"));
	m_Tips.SetSel(0, -1);
	m_Tips.ReplaceSel(_T(""));

	//道具名字
	m_Tips.SetSel(-1, -1);
	if (vectItemInfo.size() == 0)
	{
		m_Tips.SetWindowText(_T("找不到道具链接信息"));
		return;
	}

	int nTextLen = 0;
	//道具名字
	CIRC_UIDlg* pMainDlg = (CIRC_UIDlg*)(AfxGetApp()->m_pMainWnd);
	CHARFORMAT2 cf2;
	if (nMassLevel == -1)
	{
		nMassLevel = atoi(vectItemInfo[8].c_str());
	}
	InitCharFormat(12, pMainDlg->GetItemColor(nMassLevel), "宋体", cf2);
	m_Tips.SetDefaultCharFormat(cf2);
	CString strItemTip;
	if (nLevelUp != 0)
	{
		strItemTip.Format(_T("+%d %s"), nLevelUp, vectItemInfo[0].c_str());
	}
	else
	{
		strItemTip.Format(_T("%s"), vectItemInfo[0].c_str());
	}
	m_Tips.AppendMsg(strItemTip);
	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//绑定状态
	strItemTip = _T("")	;
	if (atoi(vectItemInfo[10].c_str()) == 1)
	{
		strItemTip += _T("已绑定");
	}
	else if (atoi(vectItemInfo[10].c_str()) == 3)
	{
		strItemTip += _T("拾取绑定");
	}

	vector<int> vectInt;
	CConfig::Split(vectItemInfo[11], vectInt, ',');
	vector<int>::iterator Ite = find(vectInt.begin(), vectInt.end(), 0);
	if (Ite != vectInt.end())
	{
		strItemTip += _T(" 不可丢弃");
	}
	Ite = find(vectInt.begin(), vectInt.end(), 1);
	if (Ite != vectInt.end())
	{
		strItemTip += _T(" 不可交易");
	}
	else
	{
		Ite = find(vectInt.begin(), vectInt.end(), 2);
		if (Ite != vectInt.end())
		{
			strItemTip += _T(" 不可交易");
		}
	}

	Ite = find(vectInt.begin(), vectInt.end(), 3);
	if (Ite != vectInt.end())
	{
		strItemTip += _T(" 死亡后不掉落");
	}

	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);

	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//职业
	vectInt.clear();
	CConfig::Split(vectItemInfo[3], vectInt, ',');
	if (vectInt.size() == 6)
	{
		strItemTip = _T("通用");
	}
	else
	{
		strItemTip = _T("");
		for (int nIndex = 0; nIndex < vectInt.size(); nIndex++)
		{
			strItemTip += _T(" ");
			strItemTip +=  ClassDesc[vectInt[nIndex]];
		}
	}
	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);

	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//种族
	vectInt.clear();
	CConfig::Split(vectItemInfo[2], vectInt, ',');
	if (vectInt.size() == 3)
	{
		strItemTip = _T("通用");
	}
	else
	{
		strItemTip = _T("");
		for (int nIndex = 0; nIndex < vectInt.size(); nIndex++)
		{
			strItemTip += _T(" ");
			strItemTip += RaceDesc[vectInt[nIndex]];
		}
	}

	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);

	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();
		
	//部位
	strItemTip.Format(_T("%s"), GearTypeDesc[atoi(vectItemInfo[14].c_str())]);
	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);
	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//装备属性
	vector<string> vectLevelUpInfo;
	strItemTip = _T("");
	if (nLevelUp != 0)
	{
		int nBrand = atoi(vectItemInfo[22].c_str());
		pMainDlg->GetIrcInfo()->GetItemLevelUpInfo(nBrand * 100 + nLevelUp, vectLevelUpInfo);
	}
	if (atoi(vectItemInfo[23].c_str()) > 0)
	{
		if (vectLevelUpInfo.size() > 0 && atoi(vectLevelUpInfo[5].c_str()) > 0)
		{
			strItemTip.Format(_T("物理攻击 %s(+%s)"), vectItemInfo[23].c_str(), vectLevelUpInfo[5].c_str());
		}
		else
		{
			strItemTip.Format(_T("物理攻击 %s"), vectItemInfo[23].c_str());
		}
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[24].c_str()) > 0)
	{
		if (vectLevelUpInfo.size() > 0 && atoi(vectLevelUpInfo[6].c_str()) > 0)
		{
			strItemTip.Format(_T("魔法攻击 %s(+%s)"), vectItemInfo[24].c_str(), vectLevelUpInfo[6].c_str());
		}
		else
		{
			strItemTip.Format(_T("魔法攻击 %s"), vectItemInfo[24].c_str());
		}
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[25].c_str()) > 0)
	{
		if (vectLevelUpInfo.size() > 0 && atoi(vectLevelUpInfo[7].c_str()) > 0)
		{
			strItemTip.Format(_T("物理防御 %s(+%s)"), vectItemInfo[25].c_str(), vectLevelUpInfo[7].c_str());
		}
		else
		{
			strItemTip.Format(_T("物理防御 %s"), vectItemInfo[25].c_str());
		}
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[26].c_str()) > 0)
	{
		if (vectLevelUpInfo.size() > 0 && atoi(vectLevelUpInfo[8].c_str()) > 0)
		{
			strItemTip.Format(_T("魔法防御 %s(+%s)"), vectItemInfo[26].c_str(), vectLevelUpInfo[8].c_str());
		}
		else
		{
			strItemTip.Format(_T("魔法防御 %s"), vectItemInfo[26].c_str());
		}
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (strItemTip != _T(""))
	{
		m_Tips.SetSel(nTextLen - 1, -1);
		InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
		m_Tips.SetSelectionCharFormat(cf2);
		nTextLen = m_Tips.GetCharCount();
	}

	//耐久度
	strItemTip.Format(_T("耐久 %d/%s"), nWear, vectItemInfo[4].c_str());
	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);
	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//需要等级
	strItemTip.Format(_T("需要等级 %s"), vectItemInfo[6].c_str());
	m_Tips.AppendMsg(strItemTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);
	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//附加属性
	if (atoi(vectItemInfo[20].c_str()) > 0)
	{
		strItemTip.Format(_T("生命值+%s"), vectItemInfo[20].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[21].c_str()) > 0)
	{
		strItemTip.Format(_T("魔法值+%s"), vectItemInfo[21].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[27].c_str()) > 0)
	{
		strItemTip.Format(_T("命中+%s"), vectItemInfo[27].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[28].c_str()) > 0)
	{
		strItemTip.Format(_T("精准+%s"), vectItemInfo[28].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[41].c_str()) > 0)
	{
		strItemTip.Format(_T("格档+%s"), vectItemInfo[41].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[42].c_str()) > 0)
	{
		strItemTip.Format(_T("抵抗+%s"), vectItemInfo[42].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[29].c_str()) > 0)
	{
		strItemTip.Format(_T("致命一击等级+%s"), vectItemInfo[29].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[30].c_str()) > 0)
	{
		strItemTip.Format(_T("会心一击等级+%s"), vectItemInfo[30].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[37].c_str()) > 0)
	{
		strItemTip.Format(_T("降低敌方致命一击等级%s"), vectItemInfo[37].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[38].c_str()) > 0)
	{
		strItemTip.Format(_T("降低敌方会心一击等级%s"), vectItemInfo[38].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[31].c_str()) > 0)
	{
		strItemTip.Format(_T("物理攻击+%s\%"), vectItemInfo[31].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[32].c_str()) > 0)
	{
		strItemTip.Format(_T("魔法攻击+%s\%"), vectItemInfo[32].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[35].c_str()) > 0)
	{
		strItemTip.Format(_T("物理防御+%s\%"), vectItemInfo[35].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[36].c_str()) > 0)
	{
		strItemTip.Format(_T("魔法防御+%s\%"), vectItemInfo[36].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[33].c_str()) > 0)
	{
		strItemTip.Format(_T("致命一击伤害+%s"), vectItemInfo[33].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[34].c_str()) > 0)
	{
		strItemTip.Format(_T("会心一击伤害+%s"), vectItemInfo[34].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[39].c_str()) > 0)
	{
		strItemTip.Format(_T("减免致命一击伤害+%s"), vectItemInfo[39].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[40].c_str()) > 0)
	{
		strItemTip.Format(_T("减免会心一击伤害+%s"), vectItemInfo[40].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[49].c_str()) > 0)
	{
		strItemTip.Format(_T("20%几率造成额外物理伤害%s"), vectItemInfo[49].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[50].c_str()) > 0)
	{
		strItemTip.Format(_T("20%几率造成额外魔法伤害%s"), vectItemInfo[50].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[43].c_str()) > 0)
	{
		strItemTip.Format(_T("物理伤害减免%s\%"), vectItemInfo[43].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}
	if (atoi(vectItemInfo[44].c_str()) > 0)
	{
		strItemTip.Format(_T("魔法伤害减免%s\%"), vectItemInfo[44].c_str());
		m_Tips.AppendMsg(strItemTip);
		m_Tips.AppendMsg(_T("\r\n"));
	}

}

void CLinkInfoDlg::UpdateTips(vector<string>& vectTaskInfo)
{
	m_Tips.SetSel(0, -1);
	if (vectTaskInfo.size() == 0)
	{
		m_Tips.ReplaceSel(_T(""));
		SetWindowText(_T("无效的任务链接"));
		return;
	}
	SetWindowText(vectTaskInfo[0].c_str());
	m_Tips.ReplaceSel(vectTaskInfo[1].c_str());

	COLORREF clrRef;
	clrRef = 0x666666;

	CHARFORMAT2 cf2;
	ZeroMemory(&cf2, sizeof(CHARFORMAT2));
	cf2.dwMask = CFM_COLOR;
	cf2.dwEffects = clrRef;
	m_Tips.SetSelectionCharFormat(cf2);
	m_Tips.SetSel(-1, -1);
}

void CLinkInfoDlg::UpdateSkillInfo(vector<string>& vectSkillProp)
{
	SetWindowText(_T("技能提示"));
	m_Tips.ClearMsg();
	if (vectSkillProp.size() == 0)
	{
		m_Tips.AppendMsg(_T("找不到技能链接信息"));
		return;
	}

	int nTextLen = 0;
	//显示技能名字
	CHARFORMAT2 cf2;
	InitCharFormat(12, RGB(0, 255, 0), "宋体", cf2);
	m_Tips.SetDefaultCharFormat(cf2);
	CString strSkillTip(vectSkillProp[0].c_str());
	m_Tips.AppendMsg(strSkillTip);
	nTextLen = m_Tips.GetCharCount();

	//显示技能等级
	strSkillTip.Format(_T("(等级%s)"), vectSkillProp[1].c_str());
	m_Tips.AppendMsg(strSkillTip);
	InitCharFormat(10, RGB(0, 255, 0), "宋体", cf2);
	m_Tips.SetSel(nTextLen - 1, -1);
	m_Tips.SetSelectionCharFormat(cf2);

	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//显示属性
	int nType = atoi(vectSkillProp[2].c_str());
	int nSpLevel = atoi(vectSkillProp[3].c_str());
	if (nSpLevel > 0)
	{
		strSkillTip.Format(_T("属性:%s\t"), _T("幸运"));
	}
	else
	{
		strSkillTip.Format(_T("属性:%s\t"), nType == 0 ? _T("物理") : _T("魔法"));    
	}
	m_Tips.AppendMsg(strSkillTip);

	//显示消耗
	CString strCost;
	strSkillTip = _T("消耗:");
	if(atoi(vectSkillProp[10].c_str()) != 0)
	{
		strCost.Format(_T(" %d生命点"), atoi(vectSkillProp[10].c_str()));
		strSkillTip += strCost;
	}
	else if (atoi(vectSkillProp[11].c_str()) != 0)
	{
		strCost.Format(_T(" %d魔法点"), atoi(vectSkillProp[11].c_str()));
		strSkillTip += strCost;
	}
	else if (atoi(vectSkillProp[14].c_str()) != 0)
	{
		strCost.Format(_T(" %d金钱"), atoi(vectSkillProp[14].c_str()));
		strSkillTip += strCost;
	}
	else if (atoi(vectSkillProp[15].c_str()) != 0)
	{
		strCost.Format(_T(" %d能量点"), atoi(vectSkillProp[15].c_str()));
		strSkillTip += strCost;
	}
	m_Tips.AppendMsg(strSkillTip);

	m_Tips.AppendMsg(_T("\r\n"));

	//施展时间
	int nCurseTime = atoi(vectSkillProp[8].c_str());
	if ((nCurseTime % 1000) == 0)
	{
		strSkillTip.Format(_T("%d秒施展时间\t"), nCurseTime / 1000);
	}
	else
	{
		strSkillTip.Format(_T("%.1f秒施展时间\t"), nCurseTime / 1000.0f);
	}
	m_Tips.AppendMsg(strSkillTip);
	//射程
	strSkillTip.Format(_T("射程:%s-%s码\t"), vectSkillProp[4].c_str(), vectSkillProp[5].c_str());
	m_Tips.AppendMsg(strSkillTip);

	m_Tips.AppendMsg(_T("\r\n"));

	//冷却时间
	strSkillTip.Format(_T("%d秒冷却时间\t"), atoi(vectSkillProp[7].c_str()) / 1000);
	m_Tips.AppendMsg(strSkillTip);

	//作用范围
	int nRange = atoi(vectSkillProp[6].c_str());
	if (nRange == 0)
	{
		strSkillTip.Format(_T("%s"), _T("单体目标"));
	}
	else
	{
		strSkillTip.Format(_T("作用范围:%d"), nRange);
	}
	m_Tips.AppendMsg(strSkillTip);

	InitCharFormat(10, RGB(255, 255, 255), "宋体", cf2);
	m_Tips.SetSel(nTextLen - 1, -1);
	m_Tips.SetSelectionCharFormat(cf2);
	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	//显示材料
	UINT nItemID = atol(vectSkillProp[12].c_str());
	int nItemNum = atoi(vectSkillProp[13].c_str());
	if (nItemID > 0 && nItemNum > 0)
	{
		//根据道具Id获得道具名称
		strSkillTip.Format(_T("材料:%s"), _T("未知"));
		m_Tips.AppendMsg(strSkillTip);
		InitCharFormat(10, RGB(255, 0, 0), "宋体", cf2);
		m_Tips.SetSel(nTextLen, -1);
		m_Tips.SetSelectionCharFormat(cf2);
		m_Tips.AppendMsg(_T("\r\n"));
		nTextLen = m_Tips.GetCharCount();
	}
	
	//显示描述
	strSkillTip.Format(_T("%s"), vectSkillProp[9].c_str());
	m_Tips.AppendMsg(strSkillTip);
	m_Tips.SetSel(nTextLen - 1, -1);
	InitCharFormat(10, RGB(255, 255, 0), "宋体", cf2);
	m_Tips.SetSelectionCharFormat(cf2);
	
	m_Tips.AppendMsg(_T("\r\n"));
	nTextLen = m_Tips.GetCharCount();

	CString strText;
	m_Tips.GetWindowText(strText);
	//显示附加信息
	strSkillTip = _T("");
	if (nSpLevel > 0)
	{
		strSkillTip = _T("使用后可获得幸运熟练度");
	}
	else if (nType == 0 || nType == 1)
	{
		strSkillTip.Format(_T("使用后可获得%s熟练度,并可获得幸运能量"), nType == 0 ? _T("物理") : _T("魔法"));    
	}
	if (strSkillTip != _T(""))
	{
		m_Tips.AppendMsg(strSkillTip);
		m_Tips.SetSel(nTextLen - 1, -1);
		InitCharFormat(10, RGB(0, 0, 255), "宋体", cf2);
		m_Tips.SetSelectionCharFormat(cf2);
	}
	
/*	
	m_Tips.SetSel(0, -1);
	PARAFORMAT pf;
	pf.cbSize = sizeof(pf);
	pf.dwMask = PFM_ALIGNMENT;
	pf.wAlignment = PFA_CENTER;
	m_Tips.SetParaFormat(pf);
*/
	m_Tips.SetSel(-1, -1);
}

void CLinkInfoDlg::InitCharFormat(int nPointSize, COLORREF crColor, const char *szFontName,  CHARFORMAT2& cf)
{
	ZeroMemory(&cf, sizeof(CHARFORMAT2));
	cf.cbSize = sizeof(CHARFORMAT2);

	cf.dwMask |= CFM_COLOR;
	cf.crTextColor = crColor;

	cf.dwMask |= CFM_SIZE;
	cf.yHeight = nPointSize * 20;

	cf.dwMask |= CFM_FACE;
	strcpy(cf.szFaceName, szFontName);
}