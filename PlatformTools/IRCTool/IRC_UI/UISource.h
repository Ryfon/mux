﻿/** @file UISource.h 
@brief IRC工具资源类
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
*
*  备    注：
</pre>*/
#pragma once

class CUISource
{
public:
	CUISource(void);
	~CUISource(void);

	virtual bool	Initial(string strPath);	
	virtual HANDLE	GetSource()	{return m_hdSource;};
	virtual	string	GetSourceType()		{return m_strSourcType;};

protected:	
	// 如果是CBUTTONST资源则应有一对资源
	HANDLE			m_hdSource;	// 资源句柄
	string			m_strSourcType;
};
