﻿#pragma once
#include "afxcmn.h"
#include "IRCIntf.h"

// CUsrInfoDlg 对话框

class CUsrInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CUsrInfoDlg)

public:
	CUsrInfoDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CUsrInfoDlg();

// 对话框数据
	enum { IDD = IDD_DLG_USRINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CRichEditCtrl m_EdtUsrInfo;
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();
private:
	int m_ParentType;
	CDialog* m_Parent;
public:
	void SetParentInfo(int nType, CDialog* pDlg)
	{
		m_ParentType = nType;
		m_Parent = pDlg;
	}

	void UpdateUsrInfo(FriendInfo_I* pFriendInfo);
	void UpdateUsrInfo(FoeInfo_I* pFoeInfo);
};
