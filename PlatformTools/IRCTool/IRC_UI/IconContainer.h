﻿#if !defined(AFX_ICONCONTAINER_H__D2D2811F_F1E5_4E2E_8DF9_632BD149F3F4__INCLUDED_)
#define AFX_ICONCONTAINER_H__D2D2811F_F1E5_4E2E_8DF9_632BD149F3F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IconContainer.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIconContainer window
#include "InnerButton.h"
#include "afxtempl.h"
#include "Gif.h"

class CIconContainer : public CWnd
{
// Construction
public:
	CIconContainer();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIconContainer)
public:
	virtual BOOL Create(POINT pt,CButton* pParentButton,CArray<CGif*, CGif*> *pGifArray);
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIconContainer();

	// Generated message map functions
protected:
	int m_nCellHeight;
	int m_nCellWidth;
	int m_nCol;
	int m_nRow;
	BOOL m_bClosed;
	void Close(int index = -3);
	CButton* m_pParentButton;
	CArray<CInnerButton*,CInnerButton*> m_InnerButtonArray;
	//{{AFX_MSG(CIconContainer)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICONCONTAINER_H__D2D2811F_F1E5_4E2E_8DF9_632BD149F3F4__INCLUDED_)
