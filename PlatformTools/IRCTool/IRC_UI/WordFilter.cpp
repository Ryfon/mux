﻿#include "StdAfx.h"
#include "WordFilter.h"

CWordFilter::CWordFilter(void)
{

}

CWordFilter::~CWordFilter(void)
{

}

int CWordFilter::Parse(const char *szBuf)
{
	istrstream iBuf(szBuf, (streamsize)strlen(szBuf) + 1);
	string strWorld;
	while (!iBuf.eof())
	{
		getline(iBuf, strWorld, '\r');
		USES_CONVERSION;
		LPWSTR lpwstrMsg = A2W(strWorld.c_str());
		m_KeyWords.push_back(lpwstrMsg);
		strWorld = "";
		getline(iBuf, strWorld, '\n');
	}
	return 0;
}

int CWordFilter::FilterStr(std::wstring &wstr, std::wstring wstrReplace)
{
	for (size_t nIndex = 0; nIndex < m_KeyWords.size(); nIndex++)
	{
		wstring::size_type nPos = wstr.find(m_KeyWords[nIndex]);
		if (nPos != wstring::npos)
		{
			wstr.replace(nPos, m_KeyWords[nIndex].length(), wstrReplace);
		}
	}
	return 0;
}