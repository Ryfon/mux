﻿#include "StdAfx.h"
#include "Gif.h"
//#include "Util.h"

CGif::CGif(LPCTSTR szFileName)
{
	m_pBuf = NULL;
	m_BufSize = 0;
	USES_CONVERSION;
	LPWSTR lpwstrMsg = A2W(szFileName);

	m_pImg = new Image(lpwstrMsg);
	UINT count = m_pImg->GetFrameDimensionsCount();
	m_pDimensionIDs = new GUID[count];
	m_pImg->GetFrameDimensionsList(m_pDimensionIDs, count);

	m_FrameCount = m_pImg->GetFrameCount(&m_pDimensionIDs[0]);

	UINT nBufSize = m_pImg->GetPropertyItemSize(PropertyTagFrameDelay);
	m_pItem = (PropertyItem*)malloc(nBufSize);
	m_pImg->GetPropertyItem(PropertyTagFrameDelay, nBufSize, m_pItem);
	m_CurFrame = 0;
}

CGif::CGif(BYTE* pBuf, UINT nSize)
{
	m_BufSize = nSize;
	m_pBuf = new BYTE[m_BufSize];
	memcpy(m_pBuf, pBuf, m_BufSize);
	HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, nSize);
	if (hGlobal != NULL)
	{
		void* pData = GlobalLock(hGlobal);
		if (pData != NULL)
		{
			memcpy(pData, m_pBuf, m_BufSize);
			GlobalUnlock(hGlobal);

			IStream* pStream = NULL;
			if (CreateStreamOnHGlobal(hGlobal, TRUE, &pStream) == S_OK)
			{
				CGif::CGif(pStream);
				LoadFromStream(pStream);
				pStream->Release();

			}
			else
				GlobalFree(hGlobal);
		}
		else
			GlobalFree(hGlobal);
	}
}

CGif::CGif(IStream* pStream)
{
	m_BufSize = 0;
	m_pBuf = NULL;
	LoadFromStream(pStream);
}

CGif::~CGif(void)
{
	delete [] m_pDimensionIDs;
	delete m_pItem;
}

Image* CGif::GetImg()
{
	return m_pImg;
}

void CGif::SelectFrame(UINT nFrameIndex)
{
	GUID guid = FrameDimensionTime;
	m_pImg->SelectActiveFrame(&guid, nFrameIndex);
	m_CurFrame = nFrameIndex;
}

void CGif::NextFrame()
{
	if (IsLastFrame())
	{
		return;
	}
	m_CurFrame++;
	SelectFrame(m_CurFrame);
}

UINT CGif::GetFrameTime()
{
	return ((UINT*)m_pItem[0].value)[m_CurFrame];
}

int CGif::LoadFromStream(IStream* pStream)
{
	m_pImg = Image::FromStream(pStream);
	UINT count = m_pImg->GetFrameDimensionsCount();
	m_pDimensionIDs = new GUID[count];
	m_pImg->GetFrameDimensionsList(m_pDimensionIDs, count);

	m_FrameCount = m_pImg->GetFrameCount(&m_pDimensionIDs[0]);

	UINT nBufSize = m_pImg->GetPropertyItemSize(PropertyTagFrameDelay);
	m_pItem = (PropertyItem*)malloc(nBufSize);
	m_pImg->GetPropertyItem(PropertyTagFrameDelay, nBufSize, m_pItem);
	m_CurFrame = 0;
	return 0;
}

int CGif::SaveToFile(const char* szFileName)
{
	if (m_pBuf == NULL)
	{
		return 0;
	}
	FILE* pFile = NULL;
	pFile = fopen(szFileName, "w+b");
	if (pFile == NULL)
	{
		return 0;
	}
	fwrite(m_pBuf, sizeof(BYTE), m_BufSize, pFile);
	fclose(pFile);
	return 0;
}
