﻿#pragma once

#ifdef _WIN32_WCE
#error "Windows CE 不支持 CDHtmlDialog。"
#endif 
#include "StyleButton.h"
#include "IconPicker.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "IrcEventSinkImp.h"
#include "LinkInfoDlg.h"
#include "IrcEdit.h"


// CPriChatDlg 对话框
class CIRC_UIDlg;
class CPriChatDlg : public CDialog
{
	DECLARE_DYNCREATE(CPriChatDlg)

public:
	CPriChatDlg(CWnd* pParent = NULL, CIRC_UIDlg* pIrcMain = NULL);   // 标准构造函数
	virtual ~CPriChatDlg();

// 对话框数据
	enum { IDD = IDD_PRI_CHAT};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
protected:
	CBitmap m_BitmapTopLeft;
	CSize	m_szBitmapTopLeft;
	CBitmap m_BitmapTopMid;
	CSize	m_szBitmapTopMid;
	CBitmap m_BitmapTopRight;
	CSize	m_szBitmapTopRight;

	CBitmap m_BitmapCenterLeft;
	CSize	m_szBitmapCenterLeft;
	CBitmap m_BitmapCenterMid;
	CSize	m_szBitmapCenterMid;
	CBitmap m_BitmapCenterRight;
	CSize	m_szBitmapCenterRight;

	CBitmap m_BitmapBottomLeft;
	CSize	m_szBitmapBottomLeft;
	CBitmap m_BitmapBottomMid;
	CSize	m_szBitmapBottomMid;
	CBitmap m_BitmapBottomRight;
	CSize	m_szBitmapBottomRight;

	bool m_bIsDragging;
	POINT m_ptCurPoint;
	CRect m_rcCurRect;
private:
	void CreateFaceIcon();
	void DisplayUsrInfo(bool bDisplay);

	// add [6/25/2009 hemeng]
	bool InitSource();

private:
	bool m_bFilter;
	CIRC_UIDlg* m_IrcMain;
	CString m_PeerRoleName;
	bool m_bUserInfoDisp; //是否显示玩家信息面板
public:
	void SetPeerRoleName(CString& strRoleName)
	{
		m_PeerRoleName = strRoleName;
	}
	bool CanAppendMsg()
	{
		return !m_bFilter;
	}
	void FormatUsrInfo(PEERUSRINFO* pPeerUsrInfo);
	void ShowPriChatError(int nErrType, CString strRoleName);
	void ShowOffLineTip(BOOL bShow);
public:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

	afx_msg LRESULT OnLink(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetFace(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSendMsg(WPARAM wParam, LPARAM lParam);

	StyleButton m_BtnMinimize;
	StyleButton m_BtnFilter;
	StyleButton m_BtnClose;
	afx_msg void OnBnClickedSdMinimize();
	afx_msg void OnBnClickedSdFilter();
	afx_msg void OnBnClickedSdCancel();
	CIconPicker m_Face;
	CTabCtrl m_UserInfoTab;
	afx_msg void OnBnClickedUserinfo();
	CIrcEdit m_MsgEdit;
	CIrcEdit m_LocalMsg;
	afx_msg void OnBnClickedFace();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CRichEditCtrl m_UsrInfo;
protected:
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();
	void DoLink(LINKINFO& LinkInfo);
public:
	CButton m_BtnUsrInfo;
	CLinkInfoDlg* m_LinkInfoDlg;
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CString m_Title;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
};
