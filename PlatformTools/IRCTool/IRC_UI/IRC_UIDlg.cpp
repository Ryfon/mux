﻿// IRC_UIDlg.cpp : 实现文件
//

#include "stdafx.h"
#include <afxole.h>
#include <afxodlgs.h>
#include <afxpriv.h>
#include <Richole.h>
#include <PackageFileSystem.h>
#include <Package.h>

#include "IRC_UI.h"
#include "IRC_UIDlg.h"
#include "Templates.h"
#include "ChatRecord.h"
#include "ClostTipDlg.h"
#include "Util.h"
#include "LoginDlg.h"
#include "Helper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define FLASH_INTERNAL 500	

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{

}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CIRC_UIDlg 对话框

CIRC_UIDlg::CIRC_UIDlg(CWnd* pParent /*=NULL*/)
: CDialog(CIRC_UIDlg::IDD, pParent), m_Face(true)
{
	m_IRCIntf = NULL;
	m_LoginDlg = NULL;
	m_IrcEventSinker = new CIrcEventSinkImp(this);
	m_bIsDragging = false;
	m_IrcStatus = IRC_UNLOGIN;
	m_bMinMode = false;
}

void CIRC_UIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_USERLIST, m_btnList);
	DDX_Control(pDX, IDC_FILTER, m_btnFilter);
	DDX_Control(pDX, IDC_Search, m_btnSearch);
	DDX_Control(pDX, IDC_GOTOGAME, m_btnGoToGame);
	DDX_Control(pDX, IDC_ADVISEMENT, m_Advisement);
	DDX_Control(pDX, IDC_CHANNEL0, m_ChannelSel[0]);
	DDX_Control(pDX, IDC_CHANNEL1, m_ChannelSel[1]);
	DDX_Control(pDX, IDC_CHANNEL2, m_ChannelSel[2]);
	DDX_Control(pDX, IDC_CHANNEL3, m_ChannelSel[3]);
	DDX_Control(pDX, IDC_FACE, m_Face);
	DDX_Control(pDX, IDC_RECVMSG, m_MsgEdit);
	DDX_Control(pDX, IDC_SENDMSG, m_LocalMsg);
	DDX_Control(pDX, IDC_TABMSG, m_TabMsg);
}

BEGIN_MESSAGE_MAP(CIRC_UIDlg, CDialog)
	ON_WM_SYSCOMMAND()
	//}}AFX_MSG_MAP
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_IRC_LOGIN, OnLogin)
	ON_MESSAGE(WM_IRC_PRIVATECHAT, OnPrivateChat)
	ON_MESSAGE(WM__IRC_WORLDCHAT, OnWorldChat)
	ON_MESSAGE(WM_IRC_FRIENDGROUP, OnFriendGroupInfo)
	ON_MESSAGE(WM_IRC_FRIENDINFO, OnFriendInfo)
	ON_MESSAGE(WM_IRC_FEOINFO, OnFoeInfo)
	ON_MESSAGE(WM_IRC_BLACKITEM, OnBlackItemInfo)
	ON_MESSAGE(WM_IRC_FRIENDINFOUPDATE, OnFriendInfoUpdate)
	ON_MESSAGE(WM_IRC_FEOINFOUPDATE, OnFoeInfoUpdate)
	ON_MESSAGE(WM_IRC_READPRIVATEMSG, OnReadPriChat)
	ON_MESSAGE(WM_IRC_READWORLDMSG, OnReadWorldChat)
	ON_MESSAGE(WM_IRC_PEERUSRINF, OnPeerUsrInfo)
	ON_MESSAGE(WM_IRC_ERROR, OnIRCError)
	ON_MESSAGE(WM_IRC_CHATERROR, OnChatError)
	ON_MESSAGE(WM_IRC_ROLEINFO, OnRoleInfo)
	ON_MESSAGE(WM_DOLINK, OnLink)
	ON_MESSAGE(WM_NEWPRICHAT, OnNewPriChat)
	ON_MESSAGE(WM_GETFACE, OnGetFace)
	ON_MESSAGE(WM_SENDMSG, OnSendMsg)
	ON_MESSAGE(WM_IRC_GUILD, OnGuild)
	ON_MESSAGE(WM_IRC_GUILDCHAT, OnGuildMsg)
	ON_MESSAGE(WM_IRC_GUILD_DISMISS, OnGuildDismiss)
	ON_MESSAGE(WM_IRC_GUILD_DELMEM, OnGuildDelMem)
	ON_MESSAGE(WM_IRC_READGUILDMSG, OnReadGuildChat)
	ON_MESSAGE(WM_IRC_GUILD_MEMBER, OnGuildMember)
	ON_MESSAGE(WM_IRC_GUILD_ADDMEM, OnGuildAddMem)
	ON_MESSAGE(WM_IRC_GUILD_ONOFFLINE, OnGuildMemOffOnline)
	ON_MESSAGE(WM_IRC_GUILD_BANCHAT, OnGuildBanChat)
	ON_MESSAGE(WM_IRC_GUILD_MEMTITLECHANGE, OnGuildMemberTitleChange)
	ON_MESSAGE(WM_IRC_GUILD_POS_UPDATE, OnGuildPosUpdateNty)
	ON_MESSAGE(WM_IRC_GUILD_MEMPOS_NTY, OnGuildMemPosChange)

	ON_MESSAGE(WM_IRC_READCOMMONMSG, OnReadCommomMsg)
	ON_MESSAGE(WM_IRC_READSYSTEMMSG, OnReadSystemMsg)
	ON_MESSAGE(WM_IRC_READTEAMMSG, OnReadTeamMsg)
	ON_MESSAGE(WM_IRC_READCHATGROUPMSG, OnReadChatGroupMsg)

	ON_MESSAGE(WM_TASKBARNOTIFIERCLICKED, OnTaskNotifierClick)
	ON_MESSAGE(WM_ICON_NOTIFY, OnTrayMenuPop)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_SD_TRAY, &CIRC_UIDlg::OnBnClickedSdTray)
	ON_BN_CLICKED(IDC_USERLIST, &CIRC_UIDlg::OnBnClickedUserlist)
	ON_BN_CLICKED(IDC_FILTER, &CIRC_UIDlg::OnBnClickedFilter)
	ON_BN_CLICKED(IDC_Search, &CIRC_UIDlg::OnBnClickedSearch)
	ON_BN_CLICKED(IDC_GOTOGAME, &CIRC_UIDlg::OnBnClickedGotogame)
	ON_BN_CLICKED(IDC_CHANNEL0, &CIRC_UIDlg::OnBnClickedChannel)
	ON_BN_CLICKED(IDC_CHANNEL1, &CIRC_UIDlg::OnBnClickedChannel)
	ON_BN_CLICKED(IDC_CHANNEL2, &CIRC_UIDlg::OnBnClickedChannel)
	ON_BN_CLICKED(IDC_CHANNEL3, &CIRC_UIDlg::OnBnClickedChannel)
	ON_COMMAND(ID_SYSCONFIG, &CIRC_UIDlg::OnSysconfig)
	ON_COMMAND(ID_MAINFRM, &CIRC_UIDlg::OnOpenirc)
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_SD_CANCEL, &CIRC_UIDlg::OnBnClickedSdCancel)
	ON_BN_CLICKED(IDC_SD_MINIMIZE, &CIRC_UIDlg::OnBnClickedSdMinimize)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_CHANNEL, &CIRC_UIDlg::OnBnClickedChannel)
	ON_BN_CLICKED(IDC_FACE, &CIRC_UIDlg::OnBnClickedFace)
	ON_COMMAND(ID_ORGANIZE, &CIRC_UIDlg::OnOrganize)
	ON_COMMAND(ID_SAMECITY, &CIRC_UIDlg::OnSamecity)
	ON_COMMAND(ID_GMCHANNEL, &CIRC_UIDlg::OnGmchannel)
	ON_COMMAND(ID_CHANNELWORLD, &CIRC_UIDlg::OnChannelworld)
	ON_WM_VSCROLL()
	ON_WM_CREATE()
	ON_COMMAND(ID_CHATREC, &CIRC_UIDlg::OnChatrec)
	ON_COMMAND(ID_EXIT, &CIRC_UIDlg::OnExit)
	ON_UPDATE_COMMAND_UI(ID_MAINFRM, &CIRC_UIDlg::OnUpdateMainfrm)
	ON_UPDATE_COMMAND_UI(ID_OFFLINE, &CIRC_UIDlg::OnUpdateOffline)
	ON_UPDATE_COMMAND_UI(ID_SYSCONFIG, &CIRC_UIDlg::OnUpdateSysconfig)
	ON_UPDATE_COMMAND_UI(ID_CHATREC, &CIRC_UIDlg::OnUpdateChatrec)
	ON_COMMAND(ID_OFFLINE, &CIRC_UIDlg::OnOffline)
	ON_COMMAND(ID_RELOGIN, &CIRC_UIDlg::OnRelogin)
	ON_UPDATE_COMMAND_UI(ID_RELOGIN, &CIRC_UIDlg::OnUpdateRelogin)
	ON_WM_SETTINGCHANGE()
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_TABMSG, &CIRC_UIDlg::OnNMClickTabmsg)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CIRC_UIDlg 消息处理程序

BOOL CIRC_UIDlg::OnInitDialog()
{
	//
	CDialog::OnInitDialog();

	SkinLoadSkin(_T("12.skn"));

	m_MsgList.clear();

	ModifyStyleEx(0, WS_EX_APPWINDOW);
	SetTimer(1, 500, NULL);
	if(!InitSource())
	{
		MessageBox("Fail to initial source");
	}	

	for (int nIndex = 0; nIndex < TRAYTIPSNUM; nIndex++)
	{
		m_UsrNotifier[nIndex].Create(this, nIndex);
		m_UsrNotifier[nIndex].SetTextFont("Arial", 90, TN_TEXT_NORMAL, TN_TEXT_UNDERLINE);
		m_UsrNotifier[nIndex].SetTextColor(RGB(0, 0, 0), RGB(0, 0, 200));
		m_UsrNotifier[nIndex].SetTextRect(CRect(10, 40, m_UsrNotifier->m_nSkinWidth - 10, m_UsrNotifier->m_nSkinHeight - 25));
	}

	//登录界面
	if (!ShowLogIn())
	{
		CDialog::OnCancel();
	}

	CRect dlgRec;
	GetWindowRect(&dlgRec);

	m_iOrgDlgHeight = dlgRec.Height();
	m_iOrgDlgWidth = dlgRec.Width();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CIRC_UIDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else if (nID == SC_MINIMIZE)
	{
		HideChildWindow();
		m_SysTray.MinimiseToTray(this);
		m_isAtTray = true;
	}
	else if (nID == SC_MAXIMIZE)
	{
		//改变窗口大小
		HideChildWindow();
		m_bMinMode = !m_bMinMode;
		DisplayMinMode(m_bMinMode);
	}
	else 
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CIRC_UIDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CIRC_UIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CIRC_UIDlg::OnDestroy()
{
	CDialog::OnDestroy();
	if (m_IRCIntf)
	{
		m_IRCIntf->CleanUp();
		DeleteIRCIntf(m_IRCIntf);
	}
	if (m_IrcEventSinker)
	{
		delete m_IrcEventSinker;
	}
	for (size_t nIndex = 0; nIndex < m_MsgList.size(); nIndex++)
	{
		delete m_MsgList[nIndex];
	}
	m_MsgList.clear();
	for (size_t nIndex = 0; nIndex < m_CacheMsg.size(); nIndex++)
	{
		delete m_CacheMsg[nIndex];
	}
	m_CacheMsg.clear();
	KillTimer(1);
}

LRESULT CIRC_UIDlg::OnLogin(WPARAM wParam, LPARAM lParam)
{
	m_GuidID = -1;
	m_IsGuild = false;
	SetWindowText(_T(m_RoleName.c_str()));
	m_SysTray.SetTooltipText(m_RoleName.c_str());
	((CIRC_UIApp*)AfxGetApp())->m_Config->SaveConfig();
	m_IrcStatus = IRC_ONLINE;
	char szRoleID[MAX_PATH];
	itoa(m_IRCIntf->GetCurRoleNo(), szRoleID, 10);
	CConfig* pConfig = static_cast<CIRC_UIApp*>(AfxGetApp())->m_Config;

	m_SysTray.StopAnimation();
	m_SysTray.SetIcon(m_OnLineIcon);

	if (m_LoginDlg)
	{
		m_LoginDlg->OnLogin();
	}

	string strRoleDir;
	strRoleDir = CHelper::GetAppWorkDir() + "UserData";
	CreateDirectory(strRoleDir.c_str(), NULL);
	strRoleDir = strRoleDir + "\\" + m_Account;
	CreateDirectory(strRoleDir.c_str(), NULL);
	strRoleDir = strRoleDir + "\\" + m_RoleName;
	string strConfigName = strRoleDir + ".xml";
	int nSuccess = pConfig->LoadConfig(strConfigName.c_str());
	if (nSuccess == -1000)
	{
		pConfig->m_ConfigFileName = strConfigName;
		::CreateDirectory(strRoleDir.c_str(), NULL);
		pConfig->m_strMsgPath = strRoleDir;
	}

	m_IRCIntf->ChangeChatLogDir(pConfig->m_strMsgPath.c_str());
	unsigned short usFilter = 0;
	CIRC_UIApp* pIrcApp = static_cast<CIRC_UIApp*>(AfxGetApp());
	if (pIrcApp->m_Config->MsgChannel(MSG_CHANNEL_WORLD))
	{
		usFilter |= 1;
	}
	if (pIrcApp->m_Config->MsgChannel(MSG_CHANNEL_SAMECITY))
	{
		usFilter |= 2;
	}
	if (pIrcApp->m_Config->MsgChannel(MSG_CHANNEL_ORG))
	{
		usFilter |= 4;
	}
	if (pIrcApp->m_Config->MsgChannel(MSG_CHANNEL_GM))
	{
		usFilter |= 8;
	}

	if (pIrcApp->m_Config->MsgPriChatType(MSG_PRICHAT_TYPE_FRIEND))
	{
		usFilter |= 16;
	}
	if (pIrcApp->m_Config->MsgPriChatType(MSG_PRICHAT_TYPE_FOE))
	{
		usFilter |= 32;
	}
	if (pIrcApp->m_Config->MsgPriChatType(MSG_PRICHAT_TYPE_STRANGER))
	{
		usFilter |=64;
	}
	m_IRCIntf->SetFilter(usFilter);
	m_LocalMsg.SetReadOnly(FALSE);
	//在这个事件发生后才能发送消息
	CStatic* pLabel = (CStatic*)(GetDlgItem(IDC_LBL_OFFLINETIPS));
	pLabel->ShowWindow(SW_HIDE);

	//遍历私聊窗口，也按照上面的操作进行设置
	for (map<CString, CPriChatDlg*>::iterator Ite = m_PrivateChats.begin(); Ite != m_PrivateChats.end(); Ite++)
	{
		CPriChatDlg* pPriChatDlg = Ite->second;
		pPriChatDlg->m_LocalMsg.SetReadOnly(FALSE);
		//在接收消息的上方显示浮动窗口，浮动窗口上显示玩家已离线消息
		pPriChatDlg->ShowOffLineTip(FALSE);
	}
	return 0;
}

LRESULT CIRC_UIDlg::OnPrivateChat(WPARAM wParam, LPARAM lParam)
{
	CHATMSG* pChatMsg = reinterpret_cast<CHATMSG*>(lParam);
	CString strRoleName(pChatMsg->szRoleName);
	CString strChat(pChatMsg->szChat);
	if (!m_UserRelation.IsFoeBanned(pChatMsg->szRoleName) && !m_UserRelation.IsGuildMemBanned(pChatMsg->szRoleName))
	{
		NewPrivateChat(strRoleName, strChat);
	}
	delete pChatMsg;
	return 0;
}

LRESULT CIRC_UIDlg::OnWorldChat(WPARAM wParam, LPARAM lParam)
{
	CHATMSG* pChatMsg = reinterpret_cast<CHATMSG*>(lParam);
	int nChatType = *reinterpret_cast<int*>(&wParam);

	OnChannelChat(nChatType, pChatMsg);
	return 0;
}

void CIRC_UIDlg::OnClose()
{
	CIRC_UIApp* pIrcApp = static_cast<CIRC_UIApp*>(AfxGetApp());
	CConfig* pConfig = pIrcApp->m_Config;
	if (pConfig->m_DispCloseTip)
	{
		CClostTipDlg closeDlg;
		closeDlg.m_ClostType = pConfig->m_CloseType - 1;
		closeDlg.m_bIsDispDlg = pConfig->m_DispCloseTip;
		INT_PTR nRet = closeDlg.DoModal();
		if (nRet == IDOK)
		{
			pConfig->m_CloseType = closeDlg.m_ClostType + 1;
			pConfig->m_DispCloseTip = !closeDlg.m_bIsDispDlg;
			pConfig->SaveConfig();
		}
		else
		{
			return;
		}
	}
	if (pConfig->m_CloseType == 1)
	{
		m_isAtTray = true;
		OnBnClickedSdTray();
		return;
	}

	CDialog::OnClose();
}
void CIRC_UIDlg::OnBnClickedSdTray()
{
	HideChildWindow();
	m_SysTray.MinimiseToTray(this);
	m_isAtTray = true;
}

void CIRC_UIDlg::CreateButtons()
{
//	SetButtonStyle(m_btnList, _T("列表"));
//	SetButtonStyle(m_btnFilter, _T("筛选"));
//	SetButtonStyle(m_btnSearch, _T("搜索"));
//	SetButtonStyle(m_btnGoToGame, _T("游戏"));
}

void CIRC_UIDlg::OnBnClickedUserlist()
{
	int nCmd;
	nCmd = m_UserRelation.IsWindowVisible() ? SW_HIDE : SW_SHOW;
	if (nCmd == SW_SHOW)
	{
		CRect rcUserRelation;
		m_UserRelation.GetWindowRect(&rcUserRelation);
		CRect rcBtn;
		m_btnList.GetWindowRect(&rcBtn);
		m_UserRelation.MoveWindow(rcBtn.left, rcBtn.bottom, rcUserRelation.Width(), rcUserRelation.Height());
	}
	m_UserRelation.ShowWindow(nCmd);
}

void CIRC_UIDlg::OnBnClickedFilter()
{
	int nCmd;
	nCmd = m_ChannelFilter.IsWindowVisible() ? SW_HIDE : SW_SHOW;
	if (nCmd == SW_SHOW)
	{
		CRect rcChannelFilter;
		m_ChannelFilter.GetWindowRect(&rcChannelFilter);
		CRect rcBtn;
		m_btnFilter.GetWindowRect(&rcBtn);
		m_ChannelFilter.MoveWindow(rcBtn.left, rcBtn.bottom, rcChannelFilter.Width(), rcChannelFilter.Height());
	}
	m_ChannelFilter.ShowWindow(nCmd);
}

void CIRC_UIDlg::OnBnClickedSearch()
{

}

void CIRC_UIDlg::OnBnClickedGotogame()
{
	if (AfxMessageBox(_T("是否确定退出IRCTool进入游戏？"), MB_YESNO) == IDYES)
	{
		m_IRCIntf->Close();
		string strClientFileName = CHelper::GetAppWorkDir() + "MuxS.exe";
		if (32 < (int)ShellExecute(NULL, "open", strClientFileName.c_str(), NULL, NULL, SW_SHOWNORMAL))
		{
			PostQuitMessage(0);
		}
		else
		{
			AfxMessageBox(_T("启动游戏失败"));
		}
	}
}

void CIRC_UIDlg::SetButtonStyle(StyleButton& aButton, LPCTSTR szButtonCaption)
{
	COLORREF bkgnd = RGB(14, 58, 145); 
	COLORREF hlt = RGB(0x1f, 0xb8, 0xcf);
	COLORREF txt = RGB(36, 225, 228);

	CRect crbtnList; 
	aButton.GetClientRect(crbtnList);
	crbtnList.right--; 
	crbtnList.bottom--;  // allow room for pressed state

	Stack sBtnList(crbtnList);
	sBtnList.FillSolid(bkgnd);
	sBtnList.SetOuterBorder(1, bkgnd);
	sBtnList.SetShape(ROUNDRECT, 5);

	sBtnList.FillBar(TOP_EDGE, 26, Clr(160, hlt), CLEAR);

	CString str1(szButtonCaption);
	sBtnList.AddAlignString(str1, ALIGN_CENTER, CPoint(0, 0), txt, 16, FONT_REG, L"Swiss");

	Style styleList;
	styleList.AddStack(sBtnList);
	aButton.LoadStdStyle(styleList);
}

void CIRC_UIDlg::OnSysconfig()
{
	if (m_SysConfig == NULL)
	{
		m_SysConfig = new CSysConfig(this);
		m_SysConfig->Create(CSysConfig::IDD, GetDesktopWindow());
		m_SysConfig->ShowWindow(SW_SHOW);
	}
}

void CIRC_UIDlg::OnOpenirc()
{
	//判断消息队列中是否还有频道聊天的记录，如果有，更新到UI上，停止当前的闪烁
	size_t nSize = m_MsgList.size();
	if (nSize > 0)
	{
		MSGTIPSITEM* pMsgTipsItem = m_MsgList[nSize - 1];
		if (pMsgTipsItem->nType != 0)
		{
			NewPrivateChat(pMsgTipsItem->strRoleName.c_str());
		}
		else
		{
			m_MsgList.pop_back();
			if (m_isAtTray)
			{
				m_SysTray.MaximiseFromTray(this);
			}
			m_isAtTray = false;
		}
		m_SysTray.StopAnimation();
	}
	else
	{
		if (m_isAtTray)
		{
			m_SysTray.MaximiseFromTray(this);
		}
		m_isAtTray = false;
	}
	if (m_MsgList.size() > 0)
	{
		SplashTray();
	}
	else
	{
		if (m_IrcStatus == IRC_OFFLINE)
		{
			m_SysTray.SetIcon(m_OffLineIcon);
		}
	}
}

BOOL CIRC_UIDlg::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}
void CIRC_UIDlg::OnBnClickedSdCancel()
{
	this->SendMessage(WM_CLOSE);
}

void CIRC_UIDlg::OnBnClickedSdMinimize()
{
	HideChildWindow();
	PostMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}

void CIRC_UIDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	SetCapture();
	m_ptCurPoint = point;
	m_bIsDragging = true;
	CDialog::OnLButtonDown(nFlags, point);
}

void CIRC_UIDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(m_bIsDragging)
	{
		ReleaseCapture();
		m_bIsDragging = FALSE;
	}
	
	CDialog::OnLButtonUp(nFlags, point);
}

void CIRC_UIDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if(m_bIsDragging)
	{
		ClientToScreen(&point);
		SetWindowPos(	NULL, point.x - m_ptCurPoint.x, point.y - m_ptCurPoint.y, 0, 0, 
			SWP_NOZORDER | SWP_NOSIZE);
	}	
	CDialog::OnMouseMove(nFlags, point);
}

void CIRC_UIDlg::OnBnClickedChannel()
{
	CMenu *psub = (CMenu *)m_ChannelMenu.GetSubMenu(0);
	CRect rect;
	m_ChannelSel[m_CurChannel].GetWindowRect(rect);
	psub->EnableMenuItem(ID_ORGANIZE, m_IsGuild && !m_UserRelation.IsBanned() ? MF_BYCOMMAND | MF_ENABLED : MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
	DWORD dwID =psub->TrackPopupMenu((TPM_LEFTALIGN | TPM_RIGHTBUTTON),
		rect.right,rect.top - 27, this);
	m_LocalMsg.SetFocus();
}

void CIRC_UIDlg::OnBnClickedFace()
{
	LPRICHEDITOLE lpRichEditOle = m_LocalMsg.GetIRichEditOle();
	long nObjCount = lpRichEditOle->GetObjectCount();
	int nTextLen = m_LocalMsg.GetTextLength();
	if (nObjCount + 1 > MAX_ICON_MSG || nObjCount + nTextLen + 2 > MAX_TEXT_MSG)
	{
		lpRichEditOle->Release();
		return;
	}
	DWORD dwRef = lpRichEditOle->Release();
	m_LocalMsg.InsertFaceToEdit(m_Face.GetGifAt(m_Face.GetCurFaceNo()), m_Face.GetCurFaceNo());
	m_LocalMsg.SetFocus();
}

void CIRC_UIDlg::OnOrganize()
{
	ChangeChatChannel(CS_UNION);
}

void CIRC_UIDlg::OnSamecity()
{
	ChangeChatChannel(CS_CITY);
}

void CIRC_UIDlg::OnGmchannel()
{
	ChangeChatChannel(CS_GM);
}

void CIRC_UIDlg::CreateFaceIcon()
{
	string strPackPath = CHelper::GetAppWorkDir();
	CPackage pkg;
	if (!pkg.OpenPackage(strPackPath + _T(".\\Data\\UI.pkg")))
	{
		return;
	}
	CPackageFileSystemPtr pPkgFileStream = pkg.GetPackageFileSystem();
	SPackageDirInfo* pPkgDirInfo = pPkgFileStream->GetPackDirInfo(_T("anime\\chat"));
	CPackageSourceManager pkgMng;
	for (map< string, SPackageDirInfo >::iterator Ite = pPkgDirInfo->vPackageDirInfos.begin(); Ite != pPkgDirInfo->vPackageDirInfos.end(); Ite++)
	{
		if (Ite->second.vPackageFileInfos.size() == 0)
		{
			continue;
		}
		int nFaceNo = atoi(Ite->second.szDirName.c_str());
		int nDataLen = 0;
		char* pBuf = pkgMng.GetFileDataEx(strPackPath + ".\\Data\\UI\\anime\\chat\\" + Ite->second.szDirName + _T("\\ani.gif"), nDataLen);
		if (pBuf)
		{
			m_Face.AddGif(pBuf, nDataLen, nFaceNo);
			delete [] pBuf;
			pBuf = NULL;
		}
		
	}
	pkg.ClosePackage();
}

void CIRC_UIDlg::OnChannelworld()
{
	ChangeChatChannel(CS_WORLD);
}

void CIRC_UIDlg::DoLink(LINKINFO& LinkInfo)
{
	if (m_LinkInfoDlg != NULL)
	{
		m_LinkInfoDlg->DestroyWindow();
		m_LinkInfoDlg = NULL;
	}

	if (m_LinkInfoDlg == NULL)
	{
		m_LinkInfoDlg = new CLinkInfoDlg(this);
		m_LinkInfoDlg->SetParentInfo(1, this);
		m_LinkInfoDlg->Create(CLinkInfoDlg::IDD, this);
	}

	CRect rectTips;
	m_LinkInfoDlg->GetWindowRect(&rectTips);

	CPoint pnt;
	pnt = m_MsgEdit.PosFromChar(LinkInfo.nEndIndex);
	m_MsgEdit.ClientToScreen(&pnt);
	m_LinkInfoDlg->MoveWindow(pnt.x, pnt.y, rectTips.Width(), rectTips.Height());

	switch(LinkInfo.nType)
	{
		//道具
	case 0:
		{
			vector<string> vectItemInfo;
			int nItemType = m_IRCIntf->GetItemInfo(LinkInfo.nID, vectItemInfo);
			m_LinkInfoDlg->UpdateTips(nItemType, LinkInfo.LevelUp, LinkInfo.AddLevel, LinkInfo.Wear, LinkInfo.MassLevel, vectItemInfo);
		}
		break;
		//技能
	case 1:
		{
			vector<string> vectSkillProp;
			m_IRCIntf->GetSkillInfo(LinkInfo.nID, vectSkillProp);
			m_LinkInfoDlg->UpdateSkillInfo(vectSkillProp);
		}
		break;
		//任务
	case 2:
		{
			vector<string> vectTaskInfo;
			m_IRCIntf->GetTaskInfo(LinkInfo.nID, vectTaskInfo);
			m_LinkInfoDlg->UpdateTips(vectTaskInfo);
		}
	    break;
	default:
	    break;
	}
	m_LinkInfoDlg->ShowWindow(SW_SHOW);
}

void CIRC_UIDlg::NewPrivateChat(CString strRoleName, CString strChat)
{
	//检查该玩家对应的私聊窗口是否存在，如果不存在，则创建对应的私聊窗口
	map<CString, CPriChatDlg*>::iterator IteChat = m_PrivateChats.find(strRoleName);
	if (IteChat != m_PrivateChats.end())
	{
		CPriChatDlg* pPriChatDlg = IteChat->second;
		if (pPriChatDlg != NULL)
		{
			if (!pPriChatDlg->CanAppendMsg())
			{
				return;
			}
			if (strChat.Compare(_T("")) != 0)
			{
				//为了控制滚动条的位置和光标的关系，需要把焦点进行转移
				pPriChatDlg->m_LocalMsg.SetFocus();
				CHATMSG chatMsg;
				sprintf(chatMsg.szRoleName, "%s", (LPCTSTR)strRoleName);
				sprintf(chatMsg.szChat, "%s", (LPCTSTR)strChat);
				pPriChatDlg->m_MsgEdit.AddChat(CT_PRIVATE_CHAT, &chatMsg);
				if (pPriChatDlg->IsIconic())
				{
					pPriChatDlg->FlashWindowEx(FLASHW_ALL | FLASHW_TIMERNOFG, -1, 0);
				}
			}
			else
			{
				pPriChatDlg->SetActiveWindow();
				pPriChatDlg->SetForegroundWindow();
				if (pPriChatDlg->IsIconic())
				{
					pPriChatDlg->SendMessage(WM_SYSCOMMAND, SC_RESTORE, 0);
				}
			}
		}
	}
	else
	{
		//对端发起私聊
		if (strChat.Compare(_T("")) != 0)
		{
			CIRC_UIApp* pIrcApp = static_cast<CIRC_UIApp*>(AfxGetApp());
			CConfig* pConfig = pIrcApp->m_Config;
			bool bExist = false;
			if (pConfig->m_MsgTipType == 2)
			{
				for (int nIndex = 0; nIndex < (int)m_MsgList.size(); nIndex++)
				{
					MSGTIPSITEM* pMsgTipsItem = m_MsgList[nIndex];
					if (pMsgTipsItem->nType != 0)
					{
						if (strRoleName.Compare(pMsgTipsItem->strRoleName.c_str()) == 0)
						{
							CHATMSG* pChatMsg = new CHATMSG;
							ZeroMemory(pChatMsg, sizeof(CHATMSG));
							strncpy(pChatMsg->szChat, strChat, strChat.GetLength());
							strncpy(pChatMsg->szRoleName, strRoleName, strRoleName.GetLength());
							pMsgTipsItem->Msgs.push_back(pChatMsg);
							bExist = true;
						}
					}
				}

				if (!bExist)
				{
					MSGTIPSITEM* pMsgTipsItem = new MSGTIPSITEM;
					pMsgTipsItem->nType = 1;
					pMsgTipsItem->strRoleName = strRoleName;
					CHATMSG* pChatMsg = new CHATMSG;
					ZeroMemory(pChatMsg, sizeof(CHATMSG));
					strncpy(pChatMsg->szChat, strChat, strChat.GetLength());
					strncpy(pChatMsg->szRoleName, strRoleName, strRoleName.GetLength());
					pMsgTipsItem->Msgs.push_back(pChatMsg);
					m_MsgList.push_back(pMsgTipsItem);
				}
				if (m_MsgList.size() > 0)
				{
					SplashTray();
				}
				return;
			}
		}

		CPriChatDlg* pPriChatDlg = new CPriChatDlg(NULL, this);
		m_IRCIntf->ReqUserInfo(strRoleName);
		pPriChatDlg->SetPeerRoleName(strRoleName);
		pPriChatDlg->m_LocalMsg.LoadFilterWord(&m_WorldFilter);
		if (pPriChatDlg != NULL)
		{
			pPriChatDlg->Create(CPriChatDlg::IDD, GetDesktopWindow());
			CString strTitle;
			strTitle.Format(_T("与玩家%s的对话"), strRoleName);
			pPriChatDlg->SetWindowText(strTitle);
			pPriChatDlg->m_Title = strTitle;
			pPriChatDlg->UpdateData(FALSE);
			pPriChatDlg->ModifyStyleEx(0, WS_EX_APPWINDOW);
			m_PrivateChats[strRoleName] = pPriChatDlg;

			for (int nIndex = 0; nIndex < (int)m_MsgList.size(); nIndex++)
			{
				MSGTIPSITEM* pMsgTipsItem = m_MsgList[nIndex];
				if (strRoleName.Compare(pMsgTipsItem->strRoleName.c_str()) == 0)
				{
					for (int nChatIndex = 0; nChatIndex < (int)pMsgTipsItem->Msgs.size(); nChatIndex++)
					{
						pPriChatDlg->m_MsgEdit.AddChat(CT_PRIVATE_CHAT, pMsgTipsItem->Msgs[nChatIndex]);
						delete pMsgTipsItem->Msgs[nChatIndex];
					}
					pMsgTipsItem->Msgs.clear();
					delete pMsgTipsItem;
					m_MsgList.erase(m_MsgList.begin() + nIndex);
					break;
				}
			}

			if (strChat.Compare(_T("")) != 0)
			{
				CHATMSG chatMsg;
				sprintf(chatMsg.szRoleName, "%s", (LPCTSTR)strRoleName);
				sprintf(chatMsg.szChat, "%s", (LPCTSTR)strChat);
				pPriChatDlg->m_MsgEdit.AddChat(CT_PRIVATE_CHAT, &chatMsg);
			}
			pPriChatDlg->ShowWindow(SW_SHOW);
			if (m_IrcStatus == IRC_OFFLINE)
			{
				pPriChatDlg->ShowOffLineTip(TRUE);
				pPriChatDlg->m_LocalMsg.SetReadOnly();
			}
		}
	}
}
int CIRC_UIDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

void CIRC_UIDlg::RemovePriChat(CString strPeerName)
{
	map<CString, CPriChatDlg*>::iterator IteChat = m_PrivateChats.find(strPeerName);
	if (IteChat != m_PrivateChats.end())
	{
		m_PrivateChats.erase(IteChat);
	}
}
void CIRC_UIDlg::OnOK()
{
	//CDialog::OnOK();
}

LRESULT CIRC_UIDlg::OnFriendGroupInfo(WPARAM wParam, LPARAM lParam)
{
	FriendGroupInfo_I* pFriendGroupInfo = reinterpret_cast<FriendGroupInfo_I*>(lParam);
	for (int nIndex = 0; nIndex < (int)wParam; nIndex++)
	{
		m_UserRelation.AddGroupInfo(&pFriendGroupInfo[nIndex]);
	}
	delete [] pFriendGroupInfo;
	return 0;
}

LRESULT CIRC_UIDlg::OnFriendInfo(WPARAM wParam, LPARAM lParam)
{
	FriendInfo_I* pFriendInfo = reinterpret_cast<FriendInfo_I*>(lParam);
	for (int nIndex = 0; nIndex < (int)wParam; nIndex++)
	{
		m_UserRelation.AddFriendInfo(&pFriendInfo[nIndex]);
	}
	delete [] pFriendInfo;
	return 0;
}

LRESULT CIRC_UIDlg::OnFoeInfo(WPARAM wParam, LPARAM lParam)
{
	FoeInfo_I* pFoeInfo = reinterpret_cast<FoeInfo_I*>(lParam);
	for (int nIndex = 0; nIndex < (int)wParam; nIndex++)
	{
		m_UserRelation.AddFoeInfo(&pFoeInfo[nIndex]);
	}
	delete [] pFoeInfo;
	return 0;
}
LRESULT CIRC_UIDlg::OnBlackItemInfo(WPARAM wParam, LPARAM lParam)
{
	BlackListItemInfo_I* pBlackItemInfo = reinterpret_cast<BlackListItemInfo_I*>(lParam);
	m_UserRelation.AddBlackItem(pBlackItemInfo->nickName);
	delete pBlackItemInfo;
	return 0;
}
LRESULT CIRC_UIDlg::OnFoeInfoUpdate(WPARAM wParam, LPARAM lParam)
{
	FoeInfo_I* pFoeInfo = reinterpret_cast<FoeInfo_I*>(lParam);
	int nOnLineType = m_UserRelation.GetFoeOnLineType(pFoeInfo->foeID);
	if (nOnLineType == pFoeInfo->isOnline)
	{
		m_UserRelation.FoeInfoUpdate(pFoeInfo);
		delete pFoeInfo;
		return 0;
	}
	//上下线提示
	CIRC_UIApp* pIrcApp = static_cast<CIRC_UIApp*>(AfxGetApp());
	if (!pIrcApp->m_Config->OnLineTip(ONLINE_TIP_FOE))
	{
		m_UserRelation.FoeInfoUpdate(pFoeInfo);
		delete pFoeInfo;
		return 0;
	}

	if ((pFoeInfo->isOnline == 1) && !(pIrcApp->m_Config->OnLineTip(ONLINE_TIP_GAME)))
	{
		m_UserRelation.FoeInfoUpdate(pFoeInfo);
		delete pFoeInfo;
		return 0;
	}

	if ((pFoeInfo->isOnline == 2) && !(pIrcApp->m_Config->OnLineTip(ONLINE_TIP_IRC)))
	{
		m_UserRelation.FoeInfoUpdate(pFoeInfo);
		delete pFoeInfo;
		return 0;
	}

	if (pFoeInfo->isOnline == 0)
	{
		if ((m_UserRelation.GetFoeOnLineType(pFoeInfo->foeID) == 2) && !pIrcApp->m_Config->OnLineTip(ONLINE_TIP_IRC))
		{
			m_UserRelation.FoeInfoUpdate(pFoeInfo);
			delete pFoeInfo;
			return 0;
		}

		if ((m_UserRelation.GetFoeOnLineType(pFoeInfo->foeID) == 1) && !pIrcApp->m_Config->OnLineTip(ONLINE_TIP_GAME))
		{
			m_UserRelation.FoeInfoUpdate(pFoeInfo);
			delete pFoeInfo;
			return 0;
		}
	}

	TRAYTIPSINFO trayTipsInfo;
	trayTipsInfo.isOnline = pFoeInfo->isOnline;
	trayTipsInfo.TipType = 1;
	trayTipsInfo.ClientType = m_UserRelation.GetFoeOnLineType(pFoeInfo->foeID) == 2 ? 1 : 0;
	trayTipsInfo.ID = pFoeInfo->foeID;
	sprintf(trayTipsInfo.nickName, "%s", m_UserRelation.GetFoeName(pFoeInfo->foeID));
	m_TrayTipsInfo.push_back(trayTipsInfo);

	m_UserRelation.FoeInfoUpdate(pFoeInfo);

	delete pFoeInfo;
	return 0;
}
LRESULT CIRC_UIDlg::OnFriendInfoUpdate(WPARAM wParam, LPARAM lParam)
{
	FriendInfo_I* pFriendInfo = reinterpret_cast<FriendInfo_I*>(lParam);
	int nOnLineType = m_UserRelation.GetFriendOnLineType(pFriendInfo->friendID);
	if (nOnLineType == pFriendInfo->isOnline)
	{
		m_UserRelation.FriendInfoUpdate(pFriendInfo);
		delete pFriendInfo;
		return 0;
	}

	//上下线提示
	CIRC_UIApp* pIrcApp = static_cast<CIRC_UIApp*>(AfxGetApp());
	if (!pIrcApp->m_Config->OnLineTip(ONLINE_TIP_FRIEND))
	{
		m_UserRelation.FriendInfoUpdate(pFriendInfo);
		delete pFriendInfo;
		return 0;
	}

	if ((pFriendInfo->isOnline == 1) && (!pIrcApp->m_Config->OnLineTip(ONLINE_TIP_GAME)))
	{
		m_UserRelation.FriendInfoUpdate(pFriendInfo);
		delete pFriendInfo;
		return 0;
	}

	if ((pFriendInfo->isOnline == 2) && (!pIrcApp->m_Config->OnLineTip(ONLINE_TIP_IRC)))
	{
		m_UserRelation.FriendInfoUpdate(pFriendInfo);
		delete pFriendInfo;
		return 0;
	}

	if (pFriendInfo->isOnline == 0 )
	{
		if ((m_UserRelation.GetFriendOnLineType(pFriendInfo->friendID) == 2) && !pIrcApp->m_Config->OnLineTip(ONLINE_TIP_IRC))
		{
			m_UserRelation.FriendInfoUpdate(pFriendInfo);
			delete pFriendInfo;
			return 0;
		}

		if ((m_UserRelation.GetFriendOnLineType(pFriendInfo->friendID) == 1) && !pIrcApp->m_Config->OnLineTip(ONLINE_TIP_GAME))
		{
			m_UserRelation.FriendInfoUpdate(pFriendInfo);
			delete pFriendInfo;
			return 0;
		}
	}

	TRAYTIPSINFO trayTipsInfo;
	trayTipsInfo.isOnline = pFriendInfo->isOnline;
	trayTipsInfo.TipType = 0;
	trayTipsInfo.ID = pFriendInfo->friendID;
	trayTipsInfo.ClientType = m_UserRelation.GetFriendOnLineType(pFriendInfo->friendID) == 2 ? 1: 0;
	sprintf(trayTipsInfo.nickName, "%s", m_UserRelation.GetFriendName(pFriendInfo->friendID));
	m_TrayTipsInfo.push_back(trayTipsInfo);

	m_UserRelation.FriendInfoUpdate(pFriendInfo);
	delete pFriendInfo;
	return 0;
}

void CIRC_UIDlg::OnChatrec()
{
	if (m_ChatRecord == NULL)
	{
		m_ChatRecord = new CChatRecord(this);
		m_ChatRecord->Create(CChatRecord::IDD, GetDesktopWindow());
		m_ChatRecord->ShowWindow(SW_SHOW);
	}
}

LRESULT CIRC_UIDlg::OnReadPriChat(WPARAM wParam, LPARAM lParam)
{
	ChatLog* pChatLog = reinterpret_cast<ChatLog*>(lParam);
	if (m_ChatRecord)
	{
		m_ChatRecord->AddPrivateChat(pChatLog);
	}
	delete pChatLog;
	return 0;
}

LRESULT CIRC_UIDlg::OnReadWorldChat(WPARAM wParam, LPARAM lParam)
{
	ChatLog* pChatLog = reinterpret_cast<ChatLog*>(lParam);
	if (m_ChatRecord)
	{
		m_ChatRecord->AddWorldChat(pChatLog);
	}
	delete pChatLog;
	return 0;
}

LRESULT CIRC_UIDlg::OnReadGuildChat(WPARAM wParam, LPARAM lParam)
{
	ChatLog* pChatLog = reinterpret_cast<ChatLog*>(lParam);
	if (m_ChatRecord)
	{
		m_ChatRecord->AddGuildChat(pChatLog);
	}
	delete pChatLog;
	return 0;
}

LRESULT CIRC_UIDlg::OnPeerUsrInfo(WPARAM wParam, LPARAM lParam)
{
	PEERUSRINFO* pPeerUsrInfo = reinterpret_cast<PEERUSRINFO*>(lParam);
	//检查该玩家对应的私聊窗口是否存在，如果不存在，则创建对应的私聊窗口
	map<CString, CPriChatDlg*>::iterator IteChat = m_PrivateChats.find(pPeerUsrInfo->playerName);
	if (IteChat != m_PrivateChats.end())
	{
		CPriChatDlg* pPriChatDlg = IteChat->second;
		pPriChatDlg->FormatUsrInfo(pPeerUsrInfo);
	}
	delete pPeerUsrInfo;
	return 0;
}


void CIRC_UIDlg::HideChildWindow()
{
	if (m_UserRelation.IsWindowVisible())
	{
		m_UserRelation.ShowWindow(SW_HIDE);
	}
	if (m_ChannelFilter.IsWindowVisible())
	{
		m_ChannelFilter.ShowWindow(SW_HIDE);
	}
}

LRESULT CIRC_UIDlg::OnIRCError(WPARAM wParam, LPARAM lParam)
{
	unsigned long ulErrCode = (unsigned long)lParam;
	switch(ulErrCode)
	{
	case ERR_INVALID_USER:
		{
			AfxMessageBox(_T("用户名或者密码错误"));
			m_IRCIntf->Close();
			if (m_LoginDlg)
			{
				m_LoginDlg->GetDlgItem(IDC_BTN_OK)->EnableWindow();
			}
		}
		break;
	case ERR_INVALID_ROLE:
		{
			AfxMessageBox(_T("角色名无效"));
			m_IRCIntf->Close();
			if (m_LoginDlg)
			{
				m_LoginDlg->GetDlgItem(IDC_BTN_OK)->EnableWindow();
			}
		}
		break;
	case ERR_LOGIN_TIMEOUT:
		{
			AfxMessageBox(_T("登录超时"));
			m_IRCIntf->Close();
			if (m_LoginDlg)
			{
				m_LoginDlg->GetDlgItem(IDC_BTN_OK)->EnableWindow();
			}
		}
	    break;
	case ERR_USER_ONLINE:
		{
			AfxMessageBox(_T("玩家已在线！"));
			m_IRCIntf->Close();
			if (m_LoginDlg)
			{
				m_LoginDlg->GetDlgItem(IDC_BTN_OK)->EnableWindow();
			}
		}
		break;
	case ERR_CONNECTION_BREAKDOWN:
		{
			if (m_IrcStatus == IRC_ONLINE)
			{
				OnOffline();
			}
			if (m_LoginDlg)
			{
				m_LoginDlg->GetDlgItem(IDC_BTN_OK)->EnableWindow();
			}
		}
	    break;
	default:
	    break;
	}
	return 0;
}

LRESULT CIRC_UIDlg::OnChatError(WPARAM wParam, LPARAM lParam)
{
	CHATMSG* pChatMsg = reinterpret_cast<CHATMSG*>(lParam);
	int nErrType = (int)wParam;

	map<CString, CPriChatDlg*>::iterator IteChat = m_PrivateChats.find(pChatMsg->szRoleName);
	if (IteChat != m_PrivateChats.end())
	{
		CPriChatDlg* pPrivateChatDlg = IteChat->second;
		pPrivateChatDlg->ShowPriChatError(nErrType, pChatMsg->szRoleName);
	}

	delete pChatMsg;
	return 0;
}

void CIRC_UIDlg::OnExit()
{
	PostQuitMessage(0);
}

bool CIRC_UIDlg::ShowLogIn()
{
	bool bRet = false;
	CLoginDlg LogDlg;
	LogDlg.m_Account = ((CIRC_UIApp*)AfxGetApp())->m_Config->m_Account.c_str();
	LogDlg.m_Pwd = ((CIRC_UIApp*)AfxGetApp())->m_Config->m_Pwd.c_str();
	LogDlg.m_RoleName = ((CIRC_UIApp*)AfxGetApp())->m_Config->m_RoleName.c_str();
	LogDlg.m_ServerIp = ((CIRC_UIApp*)AfxGetApp())->m_Config->m_SrvIp.c_str();
	LogDlg.m_Port = ((CIRC_UIApp*)AfxGetApp())->m_Config->m_SrvPort;
	LogDlg.m_GateType = ((CIRC_UIApp*)AfxGetApp())->m_Config->m_SrvType;
	m_LoginDlg = &LogDlg;
	INT_PTR nLogIn = LogDlg.DoModal();
	if (nLogIn != IDOK)
	{
		bRet = false;
	}
	else
	{
		bRet = true;
	}
	m_LoginDlg = NULL;
	if (bRet && m_isAtTray)
	{
		m_SysTray.MaximiseFromTray(this);
		m_isAtTray = false;
	}
	return bRet;
}

int CIRC_UIDlg::LogIn()
{
	if (m_LoginDlg)
	{
		m_Account = (LPCTSTR)m_LoginDlg->m_Account;
		m_Pwd = (LPCTSTR)m_LoginDlg->m_Pwd;
		m_RoleName = (LPCTSTR)m_LoginDlg->m_RoleName;
		m_ServerIp = (LPCTSTR)m_LoginDlg->m_ServerIp;
		m_ServerPort = m_LoginDlg->m_Port;
		m_ServerType = m_LoginDlg->m_GateType != 0;
	}

	m_IRCIntf->SetServerInfo(m_ServerType ? m_ServerIp.c_str() : "", m_ServerType ? m_ServerPort : 0, !m_ServerType ? m_ServerIp.c_str() : "", 
		!m_ServerType? m_ServerPort : 0);

	m_IRCIntf->SetPlayerInfo(m_Account.c_str(), m_Pwd.c_str(), m_RoleName.c_str());
	m_IRCIntf->Open();

	//启动登录托盘显示
	HICON pHicon[2];
	pHicon[0] = m_OffLineIcon;
	pHicon[1] = m_OnLineIcon;
	m_SysTray.SetIconList(pHicon, 2);

	//设定消息提示图标
	m_SysTray.Animate(FLASH_INTERNAL);
	return 0;
}
void CIRC_UIDlg::OnUpdateMainfrm(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_IrcStatus == IRC_ONLINE || m_IrcStatus == IRC_OFFLINE);
}

void CIRC_UIDlg::OnUpdateOffline(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_IrcStatus == IRC_ONLINE || m_IrcStatus == IRC_OFFLINE);
}

void CIRC_UIDlg::OnUpdateSysconfig(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_IrcStatus == IRC_ONLINE);
}

void CIRC_UIDlg::OnUpdateChatrec(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_IrcStatus == IRC_ONLINE);
}

void CIRC_UIDlg::OnOffline()
{
	//判断玩家的输入和私聊窗口，提醒玩家并且关闭
	if (m_IrcStatus == IRC_ONLINE)
	{
		m_IRCIntf->Close();
		m_IrcStatus = IRC_OFFLINE;
		if (!m_isAtTray || m_MsgList.size() == 0)
		{
			m_SysTray.SetIcon(m_OffLineIcon);
		}
		
		CStatic* pLabel = (CStatic*)GetDlgItem(IDC_LBL_OFFLINETIPS);
		if (!pLabel->IsWindowVisible())
		{
			pLabel->ShowWindow(SW_SHOW);
		}
		//把发送消息的窗口属性设置为只读
		m_LocalMsg.SetReadOnly();
		//遍历私聊窗口，也按照上面的操作进行设置
		for (map<CString, CPriChatDlg*>::iterator Ite = m_PrivateChats.begin(); Ite != m_PrivateChats.end(); Ite++)
		{
			CPriChatDlg* pPriChatDlg = Ite->second;
			pPriChatDlg->m_LocalMsg.SetReadOnly();
			//在接收消息的上方显示浮动窗口，浮动窗口上显示玩家已离线消息
			pPriChatDlg->ShowOffLineTip(TRUE);
		}
		
		//更新好友和仇人的状态
		m_UserRelation.UserOffLine();
	}
	else
	{
		LogIn();
	}
}

void CIRC_UIDlg::OnRelogin()
{
	int nRet = 0;
	if (m_PrivateChats.size() > 0)
	{
		nRet = AfxMessageBox(_T("你确定要切换用户吗？\n这会关闭所有已打开的聊天窗口"), MB_YESNO|MB_ICONSTOP);
	}
	else
	{
		nRet = AfxMessageBox(_T("你确定要退出当前用户并切换用户吗？"), MB_YESNO|MB_ICONSTOP);
	}
	if (nRet == IDYES)
	{
		if (m_IrcStatus == IRC_ONLINE)
		{
			OnOffline();
		}
		if (!m_isAtTray)
		{
			m_SysTray.MinimiseToTray(this);
			m_isAtTray = true;
		}
		m_IrcStatus = IRC_UNLOGIN;
		FlushUserInfo();
		//显示登录界面
		ShowLogIn();
	}
}

void CIRC_UIDlg::OnUpdateRelogin(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_IrcStatus != IRC_UNLOGIN);
}

void CIRC_UIDlg::FlushUserInfo()
{
	for (map<CString, CPriChatDlg*>::iterator Ite = m_PrivateChats.begin(); Ite != m_PrivateChats.end(); Ite++)
	{
		CPriChatDlg* pDlg = Ite->second;
		pDlg->SendMessage(WM_CLOSE);
		delete pDlg;
	}
	m_PrivateChats.clear();
	m_MsgEdit.FlushMsg();
	m_LocalMsg.FlushMsg();
	m_Advisement.FlushMsg();
	m_UserRelation.ClearRelation();
	m_ChannelFilter.ResetControl();
	for (size_t nIndex = 0; nIndex < m_CacheMsg.size(); nIndex++)
	{
		delete m_CacheMsg[nIndex];
	}
	m_CacheMsg.clear();
	for (size_t nIndex = 0; nIndex < m_MsgList.size(); nIndex++)
	{
		delete m_MsgList[nIndex];
	}
	m_MsgList.clear();
}

void CIRC_UIDlg::SplashTray()
{
	size_t nSize = m_MsgList.size();
	if (nSize > 0)
	{
		if (m_MsgList[nSize - 1]->nType == 0)
		{
			m_SysTray.SetIconList(m_ChannelIcon, 2);
		}
		else
		{
			m_SysTray.SetIconList(m_PrivateIcon, 2);			
		}
		m_SysTray.Animate(FLASH_INTERNAL);
	}
}
void CIRC_UIDlg::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDialog::OnSettingChange(uFlags, lpszSection);
	if (uFlags == SPI_SETWORKAREA)
	{
		Invalidate(FALSE);
	}
}

void CIRC_UIDlg::OnSize(UINT nType, int cx, int cy)
{
/*	if (nType == SIZE_MAXIMIZED)
	{
		HideChildWindow();
		// moved [7/22/2009 hemeng]
		
	}
	else if (nType == SIZE_MINIMIZED)
	{
		HideChildWindow();
		m_SysTray.MinimiseToTray(this);
		m_isAtTray = true;

	}
	else
	{
		CDialog::OnSize(nType, cx, cy);
	}
*/
	CDialog::OnSize(nType, cx, cy);
}

LRESULT CIRC_UIDlg::OnLink(WPARAM wParam, LPARAM lParam)
{
	CIrcEdit* pIrcEdit = reinterpret_cast<CIrcEdit*>(wParam);
	LINKINFO* pLinkInfo = reinterpret_cast<LINKINFO*>(lParam);
	DoLink(*pLinkInfo);
	return 0;
}

LRESULT CIRC_UIDlg::OnNewPriChat(WPARAM wParam, LPARAM lParam)
{
	CIrcEdit* pIrcEdit = reinterpret_cast<CIrcEdit*>(wParam);
	CHARRANGE* pCharRange = reinterpret_cast<CHARRANGE*>(lParam);
	pIrcEdit->SetSel(pCharRange->cpMin, pCharRange->cpMax);
	CString strRoleName = pIrcEdit->GetSelText();
	CString strLocalRole(m_RoleName.c_str());
	pIrcEdit->SetSel(-1, -1);
	if (strLocalRole != strRoleName)
	{
		NewPrivateChat(strRoleName);
	}
	return 0;
}

LRESULT CIRC_UIDlg::OnGetFace(WPARAM wParam, LPARAM lParam)
{
	int nFaceIndex = *reinterpret_cast<int*>(&lParam);
	
	CGif* pFace = NULL;
	pFace = m_Face.GetGifAt(nFaceIndex);

	
	return reinterpret_cast<LRESULT>(pFace);
}

LRESULT CIRC_UIDlg::OnSendMsg(WPARAM wParam, LPARAM lParam)
{
	CIrcEdit* pIrcEidt = reinterpret_cast<CIrcEdit*>(wParam);
	CString* pStrMsg = reinterpret_cast<CString*>(lParam);
	int nChannelType = 0;
	CHATMSG ChatMsg;
	ZeroMemory(&ChatMsg, sizeof(CHATMSG));
	strncpy(ChatMsg.szRoleName, m_RoleName.c_str(), m_RoleName.size());
	strncpy(ChatMsg.szChat, (LPCTSTR)(*pStrMsg), pStrMsg->GetLength());
	switch(m_CurChannel)
	{
	case CS_WORLD:
		{
			m_IRCIntf->SendMsgToWorld((LPCTSTR)(*pStrMsg));
			nChannelType = CT_WORLD_IRC_CHAT;
		}
		break;
	case CS_UNION:
		{
			m_IRCIntf->SendMsgToGuild((LPCTSTR)(*pStrMsg));
			nChannelType = CT_UNION_IRC_CHAT;
		}
		break;
	case CS_CITY:
		break;
	case CS_GM:
		break;
	default:

		break;
	}

	//判断
	CHANNELMSG* pChannelMsg = new CHANNELMSG;
	pChannelMsg->nType = nChannelType;
	pChannelMsg->strRoleName = m_RoleName;
	pChannelMsg->strChat = ChatMsg.szChat;
	m_CacheMsg.push_back(pChannelMsg);
	if (m_CacheMsg.size() > MAX_DISP_LINE)
	{
		delete m_CacheMsg[0];
		m_CacheMsg.erase(m_CacheMsg.begin());
	}

	if ((m_TabMsg.GetCurSel() == 1) && !IsFilter(nChannelType))
	{
		pIrcEidt->SetWindowText(_T(""));
		return 0;
	}
	m_MsgEdit.AddChat(nChannelType, &ChatMsg, true, true, &m_IrcFlag);
	pIrcEidt->SetWindowText(_T(""));
	return 0;
}

bool	CIRC_UIDlg::InitSource()
{
	//CUISourceManager::GetInstance()->GetSourceHandle("Background Picture",m_bmpBackground);
	CUISourceManager::GetInstance()->GetSourceHandle("OffLineIcon",m_OffLineIcon);
	CUISourceManager::GetInstance()->GetSourceHandle("OnLineIcon",m_OnLineIcon);
	CUISourceManager::GetInstance()->GetSourceHandle("ChannelIcon0",m_ChannelIcon[0]);
	CUISourceManager::GetInstance()->GetSourceHandle("ChannelIcon1",m_ChannelIcon[1]);
	CUISourceManager::GetInstance()->GetSourceHandle("PrivateIcon0",m_PrivateIcon[0]);
	CUISourceManager::GetInstance()->GetSourceHandle("PrivateIcon1",m_PrivateIcon[1]);
	
	HBITMAP hBitMap = NULL;
	if (CUISourceManager::GetInstance()->GetSourceHandle("IRC_Icon", hBitMap))
	{
		m_IrcFlag.Attach(hBitMap);
	}		

	hBitMap = NULL;
	if (CUISourceManager::GetInstance()->GetSourceHandle("OnOffLineTips", hBitMap))
	{
		for (int nIndex = 0; nIndex < TRAYTIPSNUM; nIndex++)
		{
			m_UsrNotifier[nIndex].SetSkin(hBitMap);	
		}
		
	}	

	SetButtonSTSource(m_Face,"FaceIcon0","FaceIcon1");

	string strDes = "ChannelSel";
	for (int i = 0; i < CS_COUNT; i++)
	{
		// int to string
		char temp0[64];
		string strTemp0,strTemp1;

		sprintf(temp0, "%d", i);
		strTemp0 = strDes + string(temp0) + "_1";
		strTemp1 = strDes + string(temp0) + "_2";
		
		if(!SetButtonSTSource(m_ChannelSel[i],strTemp0,strTemp1))
		{
			OutputDebugString("Error");
		}
		m_ChannelSel[i].DrawTransparent(TRUE);
		m_ChannelSel[i].DrawBorder(FALSE);
		m_ChannelSel[i].SetTooltipText(_T("聊天频道选择"));
	}

	if (!m_SysTray.Create(
		NULL,                            // Let icon deal with its own messages
		WM_ICON_NOTIFY,                  // Icon notify message to use
		m_Account.c_str(),  // tooltip
		m_OffLineIcon,
		IDR_TRAY_MENU,                  // ID of tray icon
		FALSE,
		NULL,
		NULL,
		NIIF_INFO,                    // balloon icon
		20 ))                            // balloon timeout
	{
		return FALSE;
	}
	m_SysTray.MinimiseToTray(this);
	m_isAtTray = true;

	m_UserRelation.Create(CUserRelation::IDD, this);
	m_ChannelFilter.Create(CChannelFileter::IDD, this);

	m_ChatRecord = NULL;
	m_SysConfig = NULL;
	m_LinkInfoDlg = NULL;

	m_ChannelMenu.LoadMenu(IDR_CHANNELOPT);

	//  [6/26/2009 hemeng]
	// 设置初始频道为同城
	m_CurChannel = CS_CITY;
	for (int i = 0; i < CS_COUNT; i++)
	{
		m_ChannelSel[i].ShowWindow(SW_HIDE);
		//m_ChannelSel[i].SetMenu(IDR_CHANNELOPT,m_hWnd);
	}
	m_ChannelSel[m_CurChannel].ShowWindow(SW_SHOW);

	m_TabMsg.InsertItem(0, _T("综合"));
	m_TabMsg.InsertItem(1, _T("个人"));

	CreateFaceIcon();

	/*m_btnTray.LoadStdImage(IDR_TRAY, _T("PNG"));
	m_SysTray.SetMenuDefaultItem(ID_MAINFRM, FALSE);

	m_btnMin.LoadStdImage(IDR_MINIMIZE, _T("PNG"));
	m_btnClose.LoadStdImage(IDR_CANCEL, _T("PNG"));*/


	CreateButtons();	

	m_Advisement.SetWindowText(_T("公告信息"));
	m_Advisement.SetEventMask(m_Advisement.GetEventMask() | ENM_LINK | ENM_MOUSEEVENTS);

	m_MsgEdit.SetEventMask(m_MsgEdit.GetEventMask() | ENM_LINK | ENM_SCROLL | ENM_MOUSEEVENTS | ENM_KEYEVENTS);

	m_LocalMsg.SetEventMask(m_LocalMsg.GetEventMask() | ENM_CHANGE | ENM_KEYEVENTS);
	m_LocalMsg.LimitText(MAX_TEXT_MSG);
	CenterWindow();
	m_LocalMsg.SetReadOnly();

	m_IRCIntf = CreateIRCIntf();
	string strPackPath = CHelper::GetAppWorkDir();
	int nFileSize = 0;
	//char* pBuf = CPackageSourceManager::GetInstance()->GetFileDataEx(strPackPath + ".\\Data\\Config\\MuxCliMod.dll", nFileSize);
	char* pBuf = CPackageSourceManager::GetInstance()->GetFileDataEx(strPackPath + ".\\Data\\Config\\MuxCliMod_win.dll", nFileSize);
	if (pBuf)
	{
		m_IRCIntf->StartUp(*m_IrcEventSinker, pBuf);
		delete [] pBuf;
	}
	ReadTipsInfo();
	return true;
}

void CIRC_UIDlg::ReadTipsInfo()
{
	string strPackPath = CHelper::GetAppWorkDir();
	//技能配置表
	char* pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\skill.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadSkillInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//套装配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\suitattribute.xml");
	if (pBuf)
	{
		//道具信息
		m_IRCIntf->ReadSuitInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//装备配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\equip.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//武器配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\weapon.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//消耗品配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\expendable.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//功能性道具配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\functionitem.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//任务道具配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\taskitem.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//价值物配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath +"Data\\Config\\Item\\axiology.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//图纸配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\buleprint.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//技能书配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\skillbook.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//骑乘道具配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\mount.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//时装配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\clothes.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}

	//追加属性表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\addattribute.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadAdditionAttr(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	//升级属性配置表
	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\Item\\itemlevelupattribute.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadItemLevelUpInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}

	pBuf = CPackageSourceManager::GetInstance()->GetFileData(strPackPath + "Data\\Config\\task\\task.xml");
	if (pBuf)
	{
		m_IRCIntf->ReadTaskInfo(pBuf);
		delete [] pBuf;
		pBuf = NULL;
	}
	int nDataLen = 0;
	pBuf = CPackageSourceManager::GetInstance()->GetFileDataEx(strPackPath + "Data\\Config\\UnhealthyPhrase.txt", nDataLen);
	if (pBuf)
	{
		string strWorldFilter;
		strWorldFilter.append(pBuf, nDataLen);
		m_WorldFilter.Parse(strWorldFilter.c_str());
		m_LocalMsg.LoadFilterWord(&m_WorldFilter);
		delete [] pBuf;
		pBuf = NULL;
	}
}

bool CIRC_UIDlg::ChangeChatChannel(CHANNEL_SEL eChannel)
{
	m_CurChannel = eChannel;
	if (eChannel >= CS_COUNT)
	{
		return false;
	}

	for (int i = 0; i < CS_COUNT; i++)
	{
		m_ChannelSel[i].ShowWindow(SW_HIDE);		
	}

	m_ChannelSel[eChannel].ShowWindow(SW_SHOW);
	return true;
}

LRESULT CIRC_UIDlg::OnGuild(WPARAM wParam, LPARAM lParam)
{
	m_GuildPosList.clear();
	m_IsGuild = true;
	m_UserRelation.AddUnionDlg();
	Union_I* pUnionI = (Union_I*)lParam;
	for (int nIndex = 0; nIndex < pUnionI->posListLen; nIndex++)
	{
		m_GuildPosList[pUnionI->posList[nIndex].seq] = pUnionI->posList[nIndex].pos;
	}
	m_GuidID = pUnionI->id;
	m_IRCIntf->QryUnionMember();
	delete pUnionI;
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildMsg(WPARAM wParam, LPARAM lParam)
{
	CHATMSG* pChatMsg = reinterpret_cast<CHATMSG*>(lParam);
	int nChatType = *reinterpret_cast<int*>(&wParam);

	OnChannelChat(nChatType, pChatMsg);

	return 0;
}

void CIRC_UIDlg::OnChannelChat(int nChatType, CHATMSG* pChatMsg)
{
	if (m_UserRelation.IsFoeBanned(pChatMsg->szRoleName))
	{
		return;
	}

	CHANNELMSG* pChannelMsg = new CHANNELMSG;
	pChannelMsg->nType = nChatType;
	pChannelMsg->strRoleName = pChatMsg->szRoleName;
	pChannelMsg->strChat = pChatMsg->szChat;
	m_CacheMsg.push_back(pChannelMsg);
	if (m_CacheMsg.size() > MAX_DISP_LINE)
	{
		delete m_CacheMsg[0];
		m_CacheMsg.erase(m_CacheMsg.begin());
	}

	if ((m_TabMsg.GetCurSel() == 1) && !IsFilter(nChatType))
	{
		return;
	}
	
	//为了控制滚动条的位置和光标的关系，需要把焦点进行转移
	//m_LocalMsg.SetFocus();
	m_MsgEdit.AddChat(nChatType, pChatMsg, false, true, IsIrc(nChatType) ? &m_IrcFlag : NULL);
	delete pChatMsg;
	if (m_isAtTray)
	{
		CIRC_UIApp* pIrcApp = static_cast<CIRC_UIApp*>(AfxGetApp());
		CConfig* pConfig = pIrcApp->m_Config;
		if (pConfig->m_MsgTipType == 1)
		{
			//判断消息队列中是否还有频道聊天的记录，如果有，更新到UI上，停止当前的闪烁
			for (int nIndex = 0; nIndex < (int)m_MsgList.size(); nIndex++)
			{
				MSGTIPSITEM* pMsgTipsItem = m_MsgList[nIndex];
				if (pMsgTipsItem->nType == 0)
				{
					m_MsgList.erase(m_MsgList.begin() + nIndex);
					m_SysTray.StopAnimation();
					break;
				}
			}
			if (m_MsgList.size() > 0)
			{
				SplashTray();
			}
			m_SysTray.MaximiseFromTray(this);

		}
		else
		{
			bool bExist = false;
			for (int nIndex = 0; nIndex < (int)m_MsgList.size(); nIndex++)
			{
				MSGTIPSITEM* pMsgTipsItem = m_MsgList[nIndex];
				if (pMsgTipsItem->nType == 0)
				{
					bExist = true;
					break;
				}
			}
			if (!bExist)
			{
				MSGTIPSITEM* pMsgTipsItem = new MSGTIPSITEM;
				pMsgTipsItem->nType = 0;
				m_MsgList.push_back(pMsgTipsItem);
			}
			SplashTray();
		}
	}
	else
	{
		if (IsIconic())
		{
			FlashWindowEx(FLASHW_ALL | FLASHW_TIMERNOFG, -1, 0);
		}
	}
}

bool CIRC_UIDlg::IsIrc(int nChatType)
{
	bool bIsIrc = false;
	switch(nChatType)
	{
	case CT_PRIVATE_IRC_CHAT:
	case CT_WORLD_IRC_CHAT:
	case CT_UNION_IRC_CHAT:
		{
			bIsIrc = true;
		}
	    break;
	default:
	    break;
	}
	return bIsIrc;
}

bool CIRC_UIDlg::IsFilter(int nChatType)
{
	bool bIsFilter = false;
	switch(nChatType)
	{
	case CT_WORLD_IRC_CHAT:
	case CT_WORLD_CHAT:
		{
			bIsFilter = m_ChannelFilter.m_WorldFilter;
		}
		break;
	case CT_UNION_IRC_CHAT:
	case CT_UNION_CHAT:
		{
			bIsFilter = m_ChannelFilter.m_OrgFilter;
		}
		break;
	default:
		break;
	}
	return bIsFilter;
}

bool CIRC_UIDlg::SetButtonSTSource(CButtonST& kButton,string strDes0, string strDes1 /* =  */)
{
	HANDLE hd0 = NULL;
	HANDLE hd1 = NULL;

	if (strDes0 == "")
	{
		return false;
	}

	if (CUISourceManager::GetInstance()->GetSourceHandle(strDes0, hd0))
	{
		if (strDes1 != "" )
		{
			if (CUISourceManager::GetInstance()->GetSourceHandle(strDes1,hd1))
			{
				if (CUISourceManager::GetInstance()->GetSourceType(strDes0) == 
					CUISourceManager::GetInstance()->GetSourceType(strDes1))
				{
					if (CUISourceManager::GetInstance()->GetSourceType(strDes0) == "bmp")
					{
						kButton.SetBitmaps((HBITMAP)hd1,RGB(255,0,255),(HBITMAP)hd0,RGB(255,0,255));
					}
					else
					{
						kButton.SetIcon((HICON)hd0,(HICON)hd1);
					}
				}
				else
				{
					return false;
				}
			}

		}
		else
		{
			if (CUISourceManager::GetInstance()->GetSourceType(strDes0) == "bmp")
			{
				kButton.SetBitmaps((HBITMAP)hd0,RGB(255,0,255));
			}
			else
			{
				kButton.SetIcon((HICON)hd0);
			}			
		}
		kButton.DrawTransparent(TRUE);
		kButton.DrawBorder(FALSE);
	}
	else
	{
		return false;
	}

	return true;
}


void CIRC_UIDlg::OnNMClickTabmsg(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	int nCurSel;
	nCurSel = m_TabMsg.GetCurSel();
	/*
	在用户选择了综合频道时，需要把原来的消息都清空，然后把缓存的消息都显示在界面上
	在用户选择了个人频道时，需要把原来的消息都清空，然后遍历缓存的消息，把符合条件的消息
	显示在界面上。
	*/
	m_ChannelFilter.UpdateData();
	switch(nCurSel)
	{
	case 0:
		{
			m_MsgEdit.FlushMsg();
			for (int nIndex = 0; nIndex < (int)m_CacheMsg.size(); nIndex++)
			{
				CHATMSG chatMsg;
				sprintf(chatMsg.szRoleName, "%s", m_CacheMsg[nIndex]->strRoleName.c_str());
				sprintf(chatMsg.szChat, "%s", m_CacheMsg[nIndex]->strChat.c_str());
				m_MsgEdit.AddChat(m_CacheMsg[nIndex]->nType, &chatMsg, m_CacheMsg[nIndex]->strRoleName == m_RoleName, true, IsIrc(m_CacheMsg[nIndex]->nType) ? &m_IrcFlag : NULL);
			}
		}
		break;
	case 1:
		{
			FilterChannel();
		}
		break;
	default:
		;
	}
	*pResult = 0;
}

void CIRC_UIDlg::FilterChannel()
{
	bool bHaveFilter = !(m_ChannelFilter.m_GmFilter && m_ChannelFilter.m_WorldFilter && m_ChannelFilter.m_SameCityFilter && m_ChannelFilter.m_OrgFilter);
	if (/*bHaveFilter*/1)
	{
		m_MsgEdit.FlushMsg();
		for (int nIndex = 0; nIndex < (int)m_CacheMsg.size(); nIndex++)
		{
			if (!IsFilter(m_CacheMsg[nIndex]->nType))
			{
				continue;
			}
			CHATMSG chatMsg;
			sprintf(chatMsg.szRoleName, "%s", m_CacheMsg[nIndex]->strRoleName.c_str());
			sprintf(chatMsg.szChat, "%s", m_CacheMsg[nIndex]->strChat.c_str());
			m_MsgEdit.AddChat(m_CacheMsg[nIndex]->nType, &chatMsg, m_CacheMsg[nIndex]->strRoleName == m_RoleName, true, IsIrc(m_CacheMsg[nIndex]->nType) ? &m_IrcFlag : NULL);
		}

	}
}

LRESULT CIRC_UIDlg::OnTaskNotifierClick(WPARAM wParam, LPARAM lParam)
{
	int nTipType = (int)wParam;
	unsigned int nID = (unsigned int)lParam;
	//好友
	if (nTipType == 0)
	{
		if (m_UserRelation.GetFriendOnLineType(nID) != 0)
		{
			NewPrivateChat(m_UserRelation.GetFriendName(nID), _T(""));
		}
	}
	//仇人
	if (nTipType == 1)
	{
		if (m_UserRelation.GetFoeOnLineType(nID) != 0)
		{
			NewPrivateChat(m_UserRelation.GetFoeName(nID), _T(""));
		}
	}
	return 0;
}
LRESULT CIRC_UIDlg::OnTrayMenuPop(WPARAM wParam, LPARAM lParam)
{
	CMenu* pSubMenu = reinterpret_cast<CMenu*>(wParam);
	pSubMenu->EnableMenuItem(ID_CHATREC, m_IrcStatus == IRC_ONLINE ? MF_BYCOMMAND | MF_ENABLED : MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
	pSubMenu->EnableMenuItem(ID_SYSCONFIG, m_IrcStatus == IRC_ONLINE ? MF_BYCOMMAND | MF_ENABLED : MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
	pSubMenu->ModifyMenu(ID_OFFLINE, MF_BYCOMMAND, ID_OFFLINE, m_IrcStatus == IRC_ONLINE ? _T("离线") : _T("登录"));
	pSubMenu->EnableMenuItem(ID_OFFLINE, m_IrcStatus == IRC_UNLOGIN ? MF_BYCOMMAND | MF_DISABLED | MF_GRAYED : MF_BYCOMMAND | MF_ENABLED);
	pSubMenu->EnableMenuItem(ID_RELOGIN, m_IrcStatus == IRC_UNLOGIN ? MF_BYCOMMAND | MF_DISABLED | MF_GRAYED : MF_BYCOMMAND | MF_ENABLED);
	pSubMenu->EnableMenuItem(ID_MAINFRM, m_IrcStatus == IRC_UNLOGIN ? MF_BYCOMMAND | MF_DISABLED | MF_GRAYED : MF_BYCOMMAND | MF_ENABLED);
	return 0;
}

void CIRC_UIDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (m_TrayTipsInfo.size() != 0)
	{
		size_t nIndex = 0;

		for (nIndex = 0; nIndex < m_TrayTipsInfo.size() && nIndex < TRAYTIPSNUM; nIndex++)
		{
			CTaskbarNotifier* pNotifyer = GetFreeNotifyer();
			if (pNotifyer == NULL)
			{
				return;
			}
			
			CString strTip;
			CString strClient;
			if (m_TrayTipsInfo[nIndex].isOnline == 1)
			{
				strClient = _T("游戏");
			}
			if (m_TrayTipsInfo[nIndex].isOnline == 2)
			{
				strClient = _T("IRC工具");
			}
			if (m_TrayTipsInfo[nIndex].isOnline == 0)
			{
				strClient = m_TrayTipsInfo[nIndex].ClientType == 1 ? _T("IRC工具") : _T("游戏");
			}
			strTip.Format(_T("%s%s%s%s了"), m_TrayTipsInfo[nIndex].TipType == 0 ? _T("好友") : _T("仇人"), m_TrayTipsInfo[nIndex].nickName, m_TrayTipsInfo[nIndex].isOnline != 0 ? _T("登录") : _T("离开"), strClient);

			pNotifyer->SetAppValue(m_TrayTipsInfo[nIndex].TipType, m_TrayTipsInfo[nIndex].ID);
			pNotifyer->Show(strTip);
			pNotifyer->UpdateWindow();
			m_TrayTipsInfo.erase(m_TrayTipsInfo.begin());
		}
	}
	CDialog::OnTimer(nIDEvent);
}

CTaskbarNotifier* CIRC_UIDlg::GetFreeNotifyer()
{
	CTaskbarNotifier* pNotifyer = NULL;
	for (int nIndex = 0; nIndex < TRAYTIPSNUM; nIndex++)
	{
		if (!m_UsrNotifier[nIndex].IsWindowVisible())
		{
			pNotifyer = &(m_UsrNotifier[nIndex]);
			break;
		}
	}
	return pNotifyer;
}

LRESULT CIRC_UIDlg::OnRoleInfo(WPARAM wParam, LPARAM lParam)
{
	unsigned int uiRoleID = (unsigned int)(wParam);
	char* pRoleName = (char*)(lParam);
	if (m_LoginDlg)
	{
		m_LoginDlg->ProcessRole(uiRoleID, pRoleName);
	}
	else
	{
		//离线后再次上线
		if (strcmp(m_RoleName.c_str(), pRoleName) == 0)
		{
			m_IRCIntf->EnterWorld(uiRoleID);
		}
	}
	delete pRoleName;
	return 0;
}

// begin [7/23/2009 hemeng]
void CIRC_UIDlg::DisplayMinMode(bool bMinMode)
{
	m_btnList.ShowWindow( bMinMode ? SW_HIDE : SW_SHOW);
	m_btnFilter.ShowWindow( bMinMode ? SW_HIDE : SW_SHOW);
	m_btnSearch.ShowWindow( bMinMode ? SW_HIDE : SW_SHOW);
	m_btnGoToGame.ShowWindow( bMinMode ? SW_HIDE : SW_SHOW);
	m_Advisement.ShowWindow(bMinMode ? SW_HIDE : SW_SHOW);
	m_LocalMsg.ShowWindow(bMinMode ? SW_HIDE : SW_SHOW);

	m_ChannelSel[m_CurChannel].ShowWindow(bMinMode ? SW_HIDE : SW_SHOW);
	m_Face.ShowWindow(bMinMode ? SW_HIDE : SW_SHOW);
	//m_TabMsg.ShowWindow(bMinMode ? SW_HIDE : SW_SHOW);

	// 获取当前对话框的大小 [7/23/2009 hemeng]
	CRect dlgRec;
	GetWindowRect(&dlgRec);

	// 获取聊天窗口的大小 [7/23/2009 hemeng]
	CRect msgRec;
	m_MsgEdit.GetWindowRect(&msgRec);
	ScreenToClient(msgRec);

	CRect tabRec;
	m_TabMsg.GetWindowRect(&tabRec);
	ScreenToClient(tabRec);

	int imsgHeight = msgRec.Height();
	int itabHeight = tabRec.Height();
	int imsgWidth = msgRec.Width();
	int itabWidth = tabRec.Width();

	if (bMinMode)
	{
		CRect tmpRec;

		m_Face.GetWindowRect(&tmpRec);
		ScreenToClient(tmpRec);
		int iRightDes = tmpRec.right;

		m_btnList.GetWindowRect(&tmpRec);	
		ScreenToClient(tmpRec);

		int iTopDes = tabRec.top - tmpRec.top;

		m_LocalMsg.GetWindowRect(&tmpRec);
		ScreenToClient(tmpRec);
		int iBottomDes = tmpRec.bottom - msgRec.bottom;		

		tabRec.top -= iTopDes;
		tabRec.bottom = tabRec.top + itabHeight;
		tabRec.left -= iRightDes;
		tabRec.right = tabRec.left + itabWidth;

		msgRec.top = tabRec.bottom;
		msgRec.bottom = msgRec.top + imsgHeight;
		msgRec.left -= iRightDes;
		msgRec.right = msgRec.left + imsgWidth;

		dlgRec.top = dlgRec.top + iTopDes + iBottomDes;
		dlgRec.right -= iRightDes;
	}
	else
	{
		int iTopInc = m_iOrgDlgHeight - dlgRec.Height();
		int iRightInc = m_iOrgDlgWidth - dlgRec.Width(); 
		dlgRec.top -= iTopInc;
		
		// begin [7/24/2009 hemeng]
		// 如果超出屏幕范围，进行响应处理 [7/24/2009 hemeng]
		if (dlgRec.top < 0)
		{
			int iTmp = 0 - dlgRec.top;

			dlgRec.top = 0;
			dlgRec.bottom += iTmp;
		}

		dlgRec.right += iRightInc;
		int iWinWidth = GetSystemMetrics(SM_CXSCREEN);
		if (dlgRec.right > iWinWidth)
		{
			int iTmp = dlgRec.right - iWinWidth;

			dlgRec.right -= iTmp;
			dlgRec.left -= iTmp;
		}
		// end [7/24/2009 hemeng]

		CRect tmpRec;
				
		m_Advisement.GetWindowRect(&tmpRec);
		ScreenToClient(tmpRec);

		tabRec.top = tmpRec.bottom;
		tabRec.bottom = tabRec.top + itabHeight;
		tabRec.left += iRightInc;
		tabRec.right = tabRec.left + itabWidth;
		
		msgRec.top = tabRec.bottom;
		msgRec.bottom = msgRec.top + imsgHeight;		
		msgRec.left += iRightInc;
		msgRec.right = msgRec.left + imsgWidth;
	}

	m_TabMsg.MoveWindow(tabRec);	
	m_MsgEdit.MoveWindow(msgRec);

	MoveWindow(dlgRec);
}

// end [7/23/2009 hemeng]
LRESULT CIRC_UIDlg::OnGuildDelMem(WPARAM wParam, LPARAM lParam)
{
	unsigned short usFlag = (unsigned short)wParam;
	unsigned int nMemID = (unsigned int)lParam;
	//自己被踢出战盟
	if (usFlag == 1)
	{
		m_IsGuild = false;
		m_UserRelation.RemoveUnionDlg();
		m_GuidID = -1;
		if (m_CurChannel == CS_UNION)
		{
			m_LocalMsg.SetReadOnly();
		}
	}
	else
	{
		m_UserRelation.DelGuildMem(nMemID);
	}
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildDismiss(WPARAM wParam, LPARAM lParam)
{
	m_IsGuild = false;
	m_UserRelation.RemoveUnionDlg();
	m_GuidID = -1;
	if (m_CurChannel == CS_UNION)
	{
		m_LocalMsg.SetReadOnly();
	}
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildMember(WPARAM wParam, LPARAM lParam)
{
	UnionMember_I* pUnionMember = reinterpret_cast<UnionMember_I*>(lParam);
	m_UserRelation.AddGuildMemInfo(pUnionMember);
	delete pUnionMember;
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildAddMem(WPARAM wParam, LPARAM lParam)
{
	UnionMember_I* pGuildMem = reinterpret_cast<UnionMember_I*>(lParam);
	m_UserRelation.AddGuildMem(pGuildMem);
	delete pGuildMem;
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildMemOffOnline(WPARAM wParam, LPARAM lParam)
{
	m_UserRelation.GuildMemOnOffline((UINT)wParam, (bool)lParam);
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildBanChat(WPARAM wParam, LPARAM lParam)
{
	char* pGuildRoleName = (char*)lParam;
	m_UserRelation.GuildMemBanChat(pGuildRoleName);
	delete [] pGuildRoleName;
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildMemberTitleChange(WPARAM wParam, LPARAM lParam)
{
	char* szMemberName = (char*)wParam;
	char* szTitleName = (char*)lParam;
	m_UserRelation.GuildMemberTitleChange(szMemberName, szTitleName);
	delete [] szMemberName;
	delete [] szTitleName;
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildPosUpdateNty(WPARAM wParam, LPARAM lParam)
{
	int nPosLen = (int)wParam;
	UnionPosRight_I* pUnionPos = (UnionPosRight_I*)lParam;
	m_GuildPosList.clear();
	for (int nIndex = 0; nIndex < nPosLen; nIndex++)
	{
		m_GuildPosList[pUnionPos[nIndex].seq] = pUnionPos[nIndex].pos;
	}
	m_UserRelation.UpdateGuildPos();
	delete [] pUnionPos;
	return 0;
}

LRESULT CIRC_UIDlg::OnGuildMemPosChange(WPARAM wParam, LPARAM lParam)
{
	char* szMemberName = (char*)wParam;
	USHORT nPos = (USHORT)lParam;
	m_UserRelation.UpdateGuildMemPos(szMemberName, nPos);
	return 0;
}

COLORREF CIRC_UIDlg::GetItemColor(int nMassLevel)
{
	COLORREF clrRef;
	switch(nMassLevel)
	{
	case 0:
		{
			clrRef = 0x666666;
		}
		break;
	case 1:
		{
			clrRef = 0xFFFFFF;
		}
		break;
	case 2:
		{
			clrRef = 0x00FF00;
		}
		break;
	case 3:
		{
			clrRef = 0x0000FF;
		}
		break;
	case 4:
		{
			clrRef = 0x8B3EFF;
		}
		break;
	case 5:
		{
			clrRef = 0xFF6600;
		}
		break;
	default:
		{
			clrRef = 0;
		}
		break;
	}
	return clrRef;
}

LRESULT CIRC_UIDlg::OnReadCommomMsg(WPARAM wParam, LPARAM lParam)
{
	ChatLog* pChatLog = reinterpret_cast<ChatLog*>(lParam);
	if (m_ChatRecord)
	{
		m_ChatRecord->AddCommonChat(pChatLog);
	}
	delete pChatLog;
	return 0;
}

LRESULT CIRC_UIDlg::OnReadSystemMsg(WPARAM wParam, LPARAM lParam)
{
	ChatLog* pChatLog = reinterpret_cast<ChatLog*>(lParam);
	if (m_ChatRecord)
	{
		m_ChatRecord->AddSystemChat(pChatLog);
	}
	delete pChatLog;
	return 0;
}

LRESULT CIRC_UIDlg::OnReadTeamMsg(WPARAM wParam, LPARAM lParam)
{
	ChatLog* pChatLog = reinterpret_cast<ChatLog*>(lParam);
	if (m_ChatRecord)
	{
		m_ChatRecord->AddTeamChat(pChatLog);
	}
	delete pChatLog;

	return 0;
}

LRESULT CIRC_UIDlg::OnReadChatGroupMsg(WPARAM wParam, LPARAM lParam)
{
	ChatLog* pChatLog = reinterpret_cast<ChatLog*>(lParam);
	if (m_ChatRecord)
	{
		m_ChatRecord->AddChatGroupChat(pChatLog);
	}
	delete pChatLog;

	return 0;
}