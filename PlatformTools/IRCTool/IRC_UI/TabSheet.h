﻿#pragma once

// CTabSheet

class CTabSheet : public CTabCtrl
{
	DECLARE_DYNAMIC(CTabSheet)

public:
	CTabSheet();
	virtual ~CTabSheet();

public:
	int GetCurSel();
	int SetCurSel(int nItem);
	void Show();
	void SetRect();
	BOOL AddPage(LPCTSTR strTitle, CDialog* pDialog, UINT ID);
	void RemovePage(LPCTSTR strTitle);
protected:
	struct DLGINFO		
	{
		string strTitle;
		UINT DlgID;
		CDialog* pDlg;
	};
	vector<DLGINFO> m_pPages;
	int m_CurPageIndex;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};


