﻿#if !defined(AFX_INNERBUTTON_H__BBD43C5E_218B_4624_9B0E_A0A7B543F1CD__INCLUDED_)
#define AFX_INNERBUTTON_H__BBD43C5E_218B_4624_9B0E_A0A7B543F1CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InnerButton.h : header file
//
#include "Gif.h"
/////////////////////////////////////////////////////////////////////////////
// CInnerButton window

class CInnerButton : public CButton
{
// Construction
public:
	CInnerButton();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInnerButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetGif(CGif* pGif);
	virtual ~CInnerButton();

	// Generated message map functions
	void PlayGif();
protected:
	BOOL m_bTracking;
	BOOL m_bOver;
	CGif* m_pGif;
	//{{AFX_MSG(CInnerButton)
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMouseHover(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INNERBUTTON_H__BBD43C5E_218B_4624_9B0E_A0A7B543F1CD__INCLUDED_)
