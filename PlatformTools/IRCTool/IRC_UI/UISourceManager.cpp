﻿#include "StdAfx.h"
#include "UISourceManager.h"
#include "Helper.h"

CUISourceManager::CUISourceManager(void)
:m_sTransparentAlpha(255)
{
}

CUISourceManager::~CUISourceManager(void)
{
}

CUISourceManager* CUISourceManager::GetInstance()
{
	static CUISourceManager pkThis;
	return &pkThis;
}

void CUISourceManager::Release()
{
	m_mapSource.clear();
}

void CUISourceManager::_AddSource(string strDesce, string strSource, bool bSpeical)
{
	if ( strDesce == "" || strSource == "")
	{
		return;
	}

	// get extension
	string strExtension = strSource.substr(strSource.find_last_of(".") + 1);
	// to lower
	std::transform(strExtension.begin(), strExtension.end(), strExtension.begin(), tolower);
	
	if (strExtension == "bmp")
	{
		CUIBmpSource kBmpSource;
		if(!kBmpSource.Initial(strSource, bSpeical))
		{
			return ;
		}
		m_mapSource.insert(pair<string,CUISource>(strDesce,kBmpSource));
	}
	else if(strExtension == "ico")
	{
		CUIIconSource kIconSource;
		if(!kIconSource.Initial(strSource))
		{
			return ;
		}

		m_mapSource.insert(pair<string,CUISource>(strDesce,kIconSource));
	}

	
}

bool	CUISourceManager::GetSourceHandle(string strDesce,HANDLE& hd0)
{
	if (strDesce == "")
	{
		return false;
	}

	CUISource kSource;
	
	if(!_GetSource(strDesce,kSource))
	{
		return false;
	}

	hd0 = kSource.GetSource();	

	return true;
}

bool	CUISourceManager::GetSourceHandle(std::string strDesce, HBITMAP &hd)
{
	string strSourceType = GetSourceType(strDesce);
	if (strSourceType != "bmp")
	{
		// wrong source type
		OutputDebugString("Wrong file type");
		return false;
	}

	CUIBmpSource kSource;

	if (!_GetSource(strDesce,kSource))
	{
		OutputDebugString("Fail to get source");
		return false;
	}

	hd = (HBITMAP)kSource.GetSource();

	return true;
}

bool CUISourceManager::GetSourceHandle(string strDesce,HICON&hd)
{
	string strSourceType = GetSourceType(strDesce);
	if (strSourceType != "ico")
	{
		// wrong source type
		return false;
	}

	CUIIconSource kSource;

	if (!_GetSource(strDesce,kSource))
	{
		return false;
	}

	hd = (HICON)kSource.GetSource();

	return true;
}

string CUISourceManager::GetSourceType(string strDesce)
{
	CUISource kSource;
	
	if(!_GetSource(strDesce,kSource))
	{
		return "";
	}
	
	return kSource.GetSourceType();
}

bool	CUISourceManager::_GetSource(string strDesc, CUISource& kSource)
{
	if (strDesc == "")
	{
		return false;
	}
	
	map<string, CUISource>::iterator it;

	it = m_mapSource.find(strDesc);

	if (it == m_mapSource.end())
	{
		return	false;
	}

	kSource = it->second;

	return true;
}

bool CUISourceManager::Initial(string strConfigFilePath)
{
	return _LoadConfigFile(strConfigFilePath);
}

bool CUISourceManager::_LoadConfigFile(std::string strFilePath)
{
	vector<stConfigInfo> vConfigInfos;

	if (!_LoadXML(strFilePath,vConfigInfos))
	{
		return false;
	}

	_LoadSoure(vConfigInfos);

	return true;
}

bool CUISourceManager::_LoadXML(std::string strFilePath, std::vector<stConfigInfo>& vConfigInfos)
{
	if (strFilePath == "")
	{
		OutputDebugString("错误的资源配置文件路径！");
		return false;
	}

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	if(!pXmlDoc->LoadFile(strFilePath.c_str()))
	{
		OutputDebugString("Fail to open uiconfil.xml");
		return false;
	}

	TiXmlElement* pElmtRoot = pXmlDoc->RootElement();
	TiXmlElement* pElmt = pElmtRoot->FirstChildElement();

	while (pElmt != NULL)
	{
		const char* pszName = pElmt->Value();
		if (strcmp(pszName, "Source") == 0)
		{
			stConfigInfo stConfig;
			const char* szDes = pElmt->Attribute("Description");
			const char* szFilePath = pElmt->Attribute("FilePath");
			int iFlag = 0;
			pElmt->Attribute("Flag",&iFlag);

			stConfig.bSpecial = (iFlag == 1);
			stConfig.strDes = string(szDes);
			stConfig.strSourcePath = string(szFilePath);

			szDes = NULL;
			szFilePath = NULL;
			
			vConfigInfos.push_back(stConfig);
		}
		else if (strcmp(pszName, "Alpha") == 0)
		{
			pElmt->Attribute("Value",&m_sTransparentAlpha);
		}

		pElmt = pElmt->NextSiblingElement();
	}

	if (pXmlDoc)
	{
		delete pXmlDoc;
		pXmlDoc = NULL;
	}

	return true;
}

void CUISourceManager::_LoadSoure(std::vector<stConfigInfo> vConfigInfos)
{
	string strCurrWorkDir = CHelper::GetAppWorkDir();

	for (unsigned int uiIndex = 0; uiIndex < vConfigInfos.size(); uiIndex++)
	{
		stConfigInfo stConfig = vConfigInfos[uiIndex];

		_AddSource(stConfig.strDes,strCurrWorkDir + stConfig.strSourcePath, stConfig.bSpecial);
	}
}
