﻿// UserRelation.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "UserRelation.h"
#include "IRC_UIDlg.h"

// CUserRelation 对话框

IMPLEMENT_DYNCREATE(CUserRelation, CDialog)

CUserRelation::CUserRelation(CWnd* pParent /*=NULL*/)
	: CDialog(CUserRelation::IDD, pParent)
{
			
}

CUserRelation::~CUserRelation()
{
	
}

void CUserRelation::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_USER, m_UserTab);
}

BOOL CUserRelation::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_UserTab.AddPage(_T("好友"), &m_FriendDlg , CFriendDlg::IDD);
	m_UserTab.AddPage(_T("仇人"), &m_FoeDlg, CFoeDlg::IDD);
	m_UserTab.AddPage(_T("黑名单"), &m_BlackItemDlg, CBlackItemDlg::IDD);
	m_UserTab.Show();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

BEGIN_MESSAGE_MAP(CUserRelation, CDialog)
END_MESSAGE_MAP()

void CUserRelation::OnOK()
{

	//CDialog::OnOK();
}

void CUserRelation::OnCancel()
{
	//DestroyWindow();
	CDialog::OnCancel();
}

void CUserRelation::AddGroupInfo(FriendGroupInfo_I* pFriendGroupInfo)
{
	m_FriendDlg.AddFriendGroupInfo(pFriendGroupInfo);
}

void CUserRelation::AddFriendInfo(FriendInfo_I* pFriendInfo)
{
	m_FriendDlg.AddFriendInfo(pFriendInfo);
}

void CUserRelation::AddBlackItem(CString strBlackName)
{
	int nItemCount = m_BlackItemDlg.m_BlackList.GetItemCount();
	m_BlackItemDlg.m_BlackList.InsertItem(nItemCount, strBlackName);
}

void CUserRelation::AddFoeInfo(FoeInfo_I* pFoeInfo)
{
	m_FoeDlg.AddFoeInfo(pFoeInfo);
}

void CUserRelation::FriendInfoUpdate(FriendInfo_I* pFriendInfo)
{
	m_FriendDlg.FriendInfoUpdate(pFriendInfo);
}

void CUserRelation::FoeInfoUpdate(FoeInfo_I* pFoeInfo)
{
	m_FoeDlg.FoeInfoUpdate(pFoeInfo);
}

int CUserRelation::GetFriendOnLineType(unsigned long ulFriendID)
{
	return m_FriendDlg.GetFriendOnLineType(ulFriendID);
}

int CUserRelation::GetFoeOnLineType(unsigned long ulFoeID)
{
	return m_FoeDlg.GetFoeOnLineType(ulFoeID);
}

int CUserRelation::GetUserType(const string& strRoleName)
{
	if (m_FriendDlg.IsFriend(strRoleName))
	{
		return 0;
	}
	else if (m_FoeDlg.IsFoe(strRoleName))
	{
		return 1;
	}

	else if (m_BlackItemDlg.IsBlackItem(strRoleName))
	{
		return 2;
	}
	else
	{
		return 3;
	}
}

void CUserRelation::ClearRelation()
{
	m_FriendDlg.ClearFriendInfo();
	m_FoeDlg.ClearFoeInfo();
	m_BlackItemDlg.m_BlackList.DeleteAllItems();
	m_UnionDlg.ClearGuildMemInfo();
}

void CUserRelation::UserOffLine()
{
	m_FoeDlg.OffLine();
	m_FriendDlg.OffLine();
	m_UnionDlg.Offline();
}

void CUserRelation::AddUnionDlg()
{
	if (m_UserTab.GetItemCount() < 4)
	{
		m_UserTab.AddPage(_T("战盟"), &m_UnionDlg, CUnionDlg::IDD);
	}
}

void CUserRelation::RemoveUnionDlg()
{
	m_UserTab.RemovePage(_T("战盟"));
}

void CUserRelation::AddGuildMemInfo(UnionMember_I *pUnionMemInfo)
{
	m_UnionDlg.AddGuildMemInfo(pUnionMemInfo);
}

void CUserRelation::DelGuildMem(UINT nMemID)
{
	m_UnionDlg.DelGuildMem(nMemID);
}

void CUserRelation::AddGuildMem(UnionMember_I *pGuildMem)
{
	m_UnionDlg.AddGuildMemInfo(pGuildMem);
}

void CUserRelation::GuildMemOnOffline(UINT nMemID, bool bOnline)
{
	m_UnionDlg.GuildMemOnOffline(nMemID, bOnline);
}

void CUserRelation::GuildMemBanChat(const char *szRoleName)
{
	m_UnionDlg.GuildBanChat(szRoleName);
}

bool CUserRelation::IsBanned()
{
	return m_UnionDlg.IsBanned();
}

void CUserRelation::GuildMemberTitleChange(const char* szMemberName, const char* szTitleName)
{
	m_UnionDlg.GuildMemberTitleChange(szMemberName, szTitleName);
}

void CUserRelation::UpdateGuildPos()
{
	m_UnionDlg.UpdateGuildPos();
}

void CUserRelation::UpdateGuildMemPos(const char* szMemberName, USHORT nPos)
{
	m_UnionDlg.UpdateGuildMemPos(szMemberName, nPos);
}
