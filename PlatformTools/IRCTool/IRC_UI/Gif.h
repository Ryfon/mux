﻿#pragma once

class CGif
{
public:
	CGif(LPCTSTR szFileName);
	CGif(BYTE* pBuf, UINT nSize);
	CGif(IStream* pStream);
	virtual ~CGif();
public:
	void SelectFrame(UINT nFrameIndex);
	void NextFrame();

	BOOL IsAnimated() const
	{
		return m_FrameCount > 1;
	}

	BOOL IsLastFrame() const
	{
		return m_CurFrame == m_FrameCount - 1;
	}

	UINT GetFrameTime();
	Image* GetImg();
	int SaveToFile(const char* szFileName);
protected:
	int LoadFromStream(IStream* pStream);
private:
	Image* m_pImg;
	GUID* m_pDimensionIDs;
	UINT m_FrameCount;
	PropertyItem* m_pItem;
	UINT m_CurFrame;
	BYTE* m_pBuf;
	UINT m_BufSize;
};
