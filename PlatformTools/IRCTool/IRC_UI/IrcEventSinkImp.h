﻿#ifndef IRCEVENTSINKIMP_H
#define IRCEVENTSINKIMP_H
#include "..\include\IRCIntf.h"

//进入游戏世界消息
#define WM_IRC_LOGIN	        WM_APP + 1000
//收到好友组信息消息
#define WM_IRC_FRIENDGROUP		WM_APP + 1001
//收到好友信息消息
#define WM_IRC_FRIENDINFO		WM_APP + 1002
//收到好友更新信息消息
#define WM_IRC_FRIENDINFOUPDATE	WM_APP + 1003
//收到仇人信息消息
#define WM_IRC_FEOINFO			WM_APP + 1004
//收到仇人更新信息消息
#define WM_IRC_FEOINFOUPDATE    WM_APP + 1005
//收到黑名单信息消息			
#define WM_IRC_BLACKITEM        WM_APP + 1006
//收到私聊信息消息
#define WM_IRC_PRIVATECHAT      WM_APP + 1007
//收到世界频道聊天信息消息
#define WM__IRC_WORLDCHAT       WM_APP + 1008
//收到私聊错误消息
#define WM_IRC_CHATERROR		WM_APP + 1009
//读取私聊记录消息
#define WM_IRC_READPRIVATEMSG   WM_APP + 1010
//读取世界频道聊天记录消息
#define WM_IRC_READWORLDMSG     WM_APP + 1011
//
#define	WM_IRC_PEERUSRINF      WM_APP + 1012
//
#define WM_IRC_ERROR			WM_APP + 1013
//公会信息
#define WM_IRC_GUILD WM_APP + 1014
//公会聊天
#define WM_IRC_GUILDCHAT WM_APP + 1015
//读取公会消息
#define WM_IRC_READGUILDMSG WM_APP + 1016
//玩家角色信息
#define WM_IRC_ROLEINFO WM_APP + 1017
//公会解散
#define WM_IRC_GUILD_DISMISS WM_APP + 1018
//公会删除会员
#define WM_IRC_GUILD_DELMEM WM_APP + 1019
//公会会员信息
#define WM_IRC_GUILD_MEMBER WM_APP + 1020
//公会增加会员
#define WM_IRC_GUILD_ADDMEM WM_APP + 1021
//公会会员上线下线
#define WM_IRC_GUILD_ONOFFLINE WM_APP + 1022
//公会禁言通知
#define WM_IRC_GUILD_BANCHAT WM_APP + 1023
//公会会员称号变化通知
#define WM_IRC_GUILD_MEMTITLECHANGE WM_APP + 1024
//公会职位更新通知
#define WM_IRC_GUILD_POS_UPDATE WM_APP + 1025
//公会会员职位变化通知
#define WM_IRC_GUILD_MEMPOS_NTY WM_APP + 1026
//读取普通频道消息
#define WM_IRC_READCOMMONMSG WM_APP + 1027
//读取系统频道消息
#define WM_IRC_READSYSTEMMSG WM_APP + 1028
//读取组队频道消息
#define WM_IRC_READTEAMMSG WM_APP + 1029
//读取聊天群频道消息
#define WM_IRC_READCHATGROUPMSG WM_APP + 1030

#define FACECHARLEN		2
#define MAX_TEXT_MSG    60
#define MAX_ICON_MSG    3
#define MAX_DISP_LINE 100

struct CHATMSG 
{
	char szRoleName[MAX_NAME_SIZE];
	char szChat[MAX_CHAT_SIZE];
};

struct LINKINFO 
{
	int nStartIndex; //开始位置
	int nEndIndex; //结束位置
	int nType; //链接类型
	int nID; //ID
	int LevelUp;  //升级次数
	int AddLevel; //追加次数
	int MassLevel; //质量等级
	int Wear; //耐久度
};

struct MsgInfo
{
	int nStartIndex;
	int nEndIndex;
};

typedef	enum
{
	CT_SCOPE_CHAT                        	=	0,           	//周边范围内群聊
	CT_PRIVATE_CHAT                      	=	1,           	//私聊
	CT_MAP_BRO_CHAT                      	=	2,           	//地图内喊话
	CT_SYS_GM_NOTI_MAP                   	=	3,           	//GM发布的通知(通知地图内所有人)
	CT_SYS_EVENT_NOTI                    	=	4,           	//系统事件通知（例如操作结果）
	CT_SYS_WARNING                       	=	5,           	//系统警告
	CT_NPC_UIINFO                        	=	6,           	//NPC对话界面信息；
	CT_ITEM_UIINFO                       	=	7,           	//Item对话界面信息；
	CT_WORLD_CHAT                        	=	8,           	//全世界广播
	CT_TEAM_CHAT                         	=	9,           	//组队广播
	CT_AREA_UIINFO                       	=	10,          	//区域脚本界面信息
	CT_PRIVATE_IRC_CHAT                  	=	11,          	//IRC私聊
	CT_WORLD_IRC_CHAT                    	=	12,          	//IRC世界聊天
	CT_UNION_CHAT                        	=	13,          	//工会聊天
	CT_UNION_IRC_CHAT                    	=	14,          	//工会IRC聊天
	CT_SCRIPT_MSG                        	=	15,          	//脚本调用的消息
	CT_RACE_SYS_EVENT                    	=	16,          	//只给特定种族发的系统消息
	CT_RACE_BRO_CHAT                     	=	17,          	//种族喊话

	//自定义的聊天类型，屏蔽服务器聊天协议的不同
	CT_CUST_GROUP							=   20000,          //群聊

}CHAT_TYPE;

class CIrcEventSinkImp: public CIRCEventSink
{
public:
	CIrcEventSinkImp(CWnd* pWnd);
	virtual ~CIrcEventSinkImp(void);
public:
	virtual int OnRoleInfo(unsigned int uiRoleID, const char* szRoleName);
	virtual int OnLoginGame();
	virtual int OnFriendsGroup(FriendGroupInfo_I* pFirendGroupInfo, unsigned short usGroupNum);
	virtual int OnFriendInfo(FriendInfo_I* pFriendInfo, unsigned short usFriendCount);
	virtual int OnFriendInfoUpdate(FriendInfo_I* pFriendInfo);
	virtual int OnFoeInfo(FoeInfo_I* pFoeInfo, unsigned short usFoeCount);
	virtual int OnFoeUpdate(FoeInfo_I* pFoeInfo);
	virtual int OnGuild(Union_I* pUnion);
	virtual int OnGuildDismiss();
	virtual int OnGuildDelMember(unsigned short usFlag, unsigned int nMemID);
	virtual int OnGuildMember(UnionMember_I* pUnionMember);
	virtual int OnGuildAddMem(UnionMember_I* pGuildMem);
	virtual int OnGuildMemOffOnline(UINT nMemID, bool bIsOnLine);
	virtual int OnGuildBanChat(const char* szRoleName);
	virtual int OnGuildMemberTitleChange(const char* szMemberName, const char* szTitleName);
	virtual int OnGuildPosUpdateNty(int nPosLen, UnionPosRight_I* pUnionPos);
	virtual int OnGuildMemPosChange(const char* szMemberName, USHORT nPos);

	virtual int OnBlackItem(BlackListItemInfo_I* pBlackItemInfo);
	virtual int OnPrivateMsg(const char* szSrc, const char* szChat, int nClientType);
	virtual int OnWorldMsg(const char* szSrc, const char* szChat, int nClientType);
	virtual int OnGuildMsg(const char* szSrc, const char* szChat, int nClientType);
	virtual int OnChatError(const char* szSrc, int nErrorType);

	virtual int OnReadPrivateMsg(ChatLog* pChatLog);
	virtual int OnReadWorldMsg(ChatLog* pChatLog);
	virtual int OnReadGuildMsg(ChatLog* pChatLog);
	virtual int OnReadCommonMsg(ChatLog* pChatLog);
	virtual int OnReadSystemMsg(ChatLog* pChatLog);
	virtual int OnReadTeamMsg(ChatLog* pChatLog);
	virtual int OnReadChatGroupMsg(ChatLog* pChatLog);

	virtual int OnIRCError(unsigned short usErrCode);
	virtual int OnPeerInfo(PEERUSRINFO* pPeerUsrInfo);
private:
	CWnd* m_Wnd;
};

int GetItemTypeByFile(const string& strFileName);
#endif