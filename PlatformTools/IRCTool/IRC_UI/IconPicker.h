﻿#if !defined(AFX_ICONPICKER_H__D8AADA84_2CAC_4D27_9A8A_ABB0F2F3F2E6__INCLUDED_)
#define AFX_ICONPICKER_H__D8AADA84_2CAC_4D27_9A8A_ABB0F2F3F2E6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IconPicker.h : header file
//
#include <afxctl.h>
/////////////////////////////////////////////////////////////////////////////
// CIconPicker window
#include "afxtempl.h"
#include "IconContainer.h"
#include "BtnST.h"
#include "Gif.h"

// modify [6/5/2009 hemeng]
class CIconPicker : public CButtonST
{
// Construction
public:
	CIconPicker(bool bDel = false);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIconPicker)
	//}}AFX_VIRTUAL

// Implementation
public:
	void CreateFace(CString strFilePath);
	int GetCurFaceNo();
	int GetFaceNoAt(int nindex);
	int GetFaceCount();
	CGif* GetGifAt(int nFaceNo);
	int GetBitmapNoAt(int index);
	void AddGif(void* pBuf, UINT nBufSize, int nFace);
	void AddGif(CGif* pGif, int nFace);
	virtual ~CIconPicker();
	int GetIndex(int nFaceNO)
	{
		int nRet = -1;
		for (int nIndex = 0; nIndex < (int)m_FaceNoList.size(); nIndex++)
		{
			if (m_FaceNoList[nIndex] == nFaceNO)
			{
				nRet = nIndex;
			}
		}
		return nRet;
	}

	CGif* GetGif(int nIndex )
	{
		return m_GifArray[nIndex];
	}
	// Generated message map functions
protected:
	int m_iCurIndex;
	CArray<CGif*, CGif*> m_GifArray;
	vector<int> m_FaceNoList;
	BOOL m_bState;
	CIconContainer *m_pIconContainer;
	bool m_bDel;
	//{{AFX_MSG(CIconPicker)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg LRESULT OnSelectICON(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICONPICKER_H__D8AADA84_2CAC_4D27_9A8A_ABB0F2F3F2E6__INCLUDED_)
