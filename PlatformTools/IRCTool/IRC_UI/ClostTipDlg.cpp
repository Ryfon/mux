﻿// ClostTipDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IRC_UI.h"
#include "ClostTipDlg.h"


// CClostTipDlg 对话框

IMPLEMENT_DYNAMIC(CClostTipDlg, CDialog)

CClostTipDlg::CClostTipDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CClostTipDlg::IDD, pParent)
	, m_ClostType(0)
	, m_bIsDispDlg(false)
{

}

CClostTipDlg::~CClostTipDlg()
{
}

void CClostTipDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CB__CLOSE_CLOSENOTIP, (int&)m_bIsDispDlg);
	DDX_Radio(pDX, IDC_RD__CLOSE_TRAY, m_ClostType);
}


BEGIN_MESSAGE_MAP(CClostTipDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_OK, &CClostTipDlg::OnBnClickedBtnOk)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CClostTipDlg::OnBnClickedBtnCancel)
END_MESSAGE_MAP()


// CClostTipDlg 消息处理程序

void CClostTipDlg::OnBnClickedBtnOk()
{
	UpdateData(TRUE);
	CDialog::OnOK();
}

void CClostTipDlg::OnBnClickedBtnCancel()
{
	CDialog::OnCancel();
}

BOOL CClostTipDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

