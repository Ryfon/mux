﻿// GifAnimator.h : CGifAnimator 的声明
#pragma once
#include "resource.h"       // 主符号
#include <atlctl.h>
#include "GifAnimatePlayer.h"
#include "_IGifAnimatorEvents_CP.h"
#include "../IRC_UI/Gif.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif

class CGifAnimator;

class CWinHidden: public CWindowImpl<CWinHidden, CWindow, CNullTraits>
{
	BEGIN_MSG_MAP(CWinHidden)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
	END_MSG_MAP()
private:
	CGifAnimator* m_pFullCtrl;
	UINT m_nTimer;
public:
	CWinHidden(): m_nTimer(0), m_pFullCtrl(NULL){};
	~CWinHidden()
	{
		if (m_nTimer != 0)
		{
			KillTimer(m_nTimer);
			m_nTimer = 0;
		}
		if (m_hWnd != NULL)
		{
			::DestroyWindow(m_hWnd);
			m_hWnd = NULL;

		}
	}

	void Attach(CGifAnimator* pFullCtrl)
	{
		m_pFullCtrl = pFullCtrl;
	}

	BOOL SetThisTimer(UINT nIDEvent, UINT nElapse, TIMERPROC lpTimeProc)
	{
		if (m_hWnd == NULL)
		{
			RECT rt = {0, 0, 0, 0};
			Create(NULL, rt);
		}
		m_nTimer = (UINT)::SetTimer(m_hWnd, nIDEvent, nElapse, lpTimeProc);
		return m_nTimer == 0;
	}

	void KillThisTimer()
	{
		if (m_nTimer != 0)
		{
			KillTimer(m_nTimer);
			m_nTimer = 0;
		}
	}
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL bHandled);
};


// CGifAnimator
class ATL_NO_VTABLE CGifAnimator :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CStockPropImpl<CGifAnimator, IGifAnimator>,
	public IPersistStreamInitImpl<CGifAnimator>,
	public IOleControlImpl<CGifAnimator>,
	public IOleObjectImpl<CGifAnimator>,
	public IOleInPlaceActiveObjectImpl<CGifAnimator>,
	public IViewObjectExImpl<CGifAnimator>,
	public IOleInPlaceObjectWindowlessImpl<CGifAnimator>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CGifAnimator>,
	public CProxy_IGifAnimatorEvents<CGifAnimator>,
	public IPersistStorageImpl<CGifAnimator>,
	public ISpecifyPropertyPagesImpl<CGifAnimator>,
	public IQuickActivateImpl<CGifAnimator>,
#ifndef _WIN32_WCE
	public IDataObjectImpl<CGifAnimator>,
#endif
	public IProvideClassInfo2Impl<&CLSID_GifAnimator, &__uuidof(_IGifAnimatorEvents), &LIBID_GifAnimatePlayerLib>,
#ifdef _WIN32_WCE // 要在 Windows CE 上正确加载该控件，要求 IObjectSafety
	public IObjectSafetyImpl<CGifAnimator, INTERFACESAFE_FOR_UNTRUSTED_CALLER>,
#endif
	public CComCoClass<CGifAnimator, &CLSID_GifAnimator>,
	public CComControl<CGifAnimator>
{
public:
	CContainedWindow m_ctlStatic;
	CGif* m_pGif;
	CWinHidden m_wndHidden;
#pragma warning(push)
#pragma warning(disable: 4355) // “this”: 用于基成员初始值设定项列表


	CGifAnimator()
		: m_ctlStatic(_T("Static"), this, 1), m_pGif(NULL)
	{
		m_bWindowOnly = TRUE;
		m_pGif = NULL;
	}

#pragma warning(pop)

DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_INSIDEOUT |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST
)

DECLARE_REGISTRY_RESOURCEID(IDR_GIFANIMATOR)


BEGIN_COM_MAP(CGifAnimator)
	COM_INTERFACE_ENTRY(IGifAnimator)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IViewObjectEx)
	COM_INTERFACE_ENTRY(IViewObject2)
	COM_INTERFACE_ENTRY(IViewObject)
	COM_INTERFACE_ENTRY(IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceObject)
	COM_INTERFACE_ENTRY2(IOleWindow, IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceActiveObject)
	COM_INTERFACE_ENTRY(IOleControl)
	COM_INTERFACE_ENTRY(IOleObject)
	COM_INTERFACE_ENTRY(IPersistStreamInit)
	COM_INTERFACE_ENTRY2(IPersist, IPersistStreamInit)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY(ISpecifyPropertyPages)
	COM_INTERFACE_ENTRY(IQuickActivate)
	COM_INTERFACE_ENTRY(IPersistStorage)
#ifndef _WIN32_WCE
	COM_INTERFACE_ENTRY(IDataObject)
#endif
	COM_INTERFACE_ENTRY(IProvideClassInfo)
	COM_INTERFACE_ENTRY(IProvideClassInfo2)
#ifdef _WIN32_WCE // 要在 Windows CE 上正确加载该控件，要求 IObjectSafety
	COM_INTERFACE_ENTRY_IID(IID_IObjectSafety, IObjectSafety)
#endif
END_COM_MAP()

BEGIN_PROP_MAP(CGifAnimator)
	PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
	PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
#ifndef _WIN32_WCE
	PROP_ENTRY("BackColor", DISPID_BACKCOLOR, CLSID_StockColorPage)
#endif
	PROP_ENTRY("BackStyle", DISPID_BACKSTYLE, CLSID_NULL)
#ifndef _WIN32_WCE
	PROP_ENTRY("BorderColor", DISPID_BORDERCOLOR, CLSID_StockColorPage)
#endif
	PROP_ENTRY("BorderVisible", DISPID_BORDERVISIBLE, CLSID_NULL)
	// 示例项
	// PROP_ENTRY("Property Description", dispid, clsid)
	// PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()

BEGIN_CONNECTION_POINT_MAP(CGifAnimator)
	CONNECTION_POINT_ENTRY(__uuidof(_IGifAnimatorEvents))
END_CONNECTION_POINT_MAP()

BEGIN_MSG_MAP(CGifAnimator)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_SETFOCUS, OnSetFocus)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)
	CHAIN_MSG_MAP(CComControl<CGifAnimator>)
ALT_MSG_MAP(1)
	// 将此替换为超类 Static 的消息映射项
END_MSG_MAP()
// 处理程序原型:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnSetFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		LRESULT lRes = CComControl<CGifAnimator>::OnSetFocus(uMsg, wParam, lParam, bHandled);
		if (m_bInPlaceActive)
		{
/*			if(!IsChild(::GetFocus()))
				m_ctlStatic.SetFocus();
*/
		}
		return lRes;
	}

	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
/*		RECT rc;
		GetWindowRect(&rc);
		rc.right -= rc.left;
		rc.bottom -= rc.top;
		rc.top = rc.left = 0;
		m_ctlStatic.Create(m_hWnd, rc);
*/
		return 0;
	}

	STDMETHOD(SetObjectRects)(LPCRECT prcPos,LPCRECT prcClip)
	{
		IOleInPlaceObjectWindowlessImpl<CGifAnimator>::SetObjectRects(prcPos, prcClip);
		int cx, cy;
		cx = prcPos->right - prcPos->left;
		cy = prcPos->bottom - prcPos->top;
		::SetWindowPos(m_ctlStatic.m_hWnd, NULL, 0,
			0, cx, cy, SWP_NOZORDER | SWP_NOACTIVATE);
		return S_OK;
	}

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
	{
		static const IID* arr[] =
		{
			&IID_IGifAnimator,
		};

		for (int i=0; i<sizeof(arr)/sizeof(arr[0]); i++)
		{
			if (InlineIsEqualGUID(*arr[i], riid))
				return S_OK;
		}
		return S_FALSE;
	}

// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

// IGifAnimator
	OLE_COLOR m_clrBackColor;
	void OnBackColorChanged()
	{
		ATLTRACE(_T("OnBackColorChanged\n"));
	}
	LONG m_nBackStyle;
	void OnBackStyleChanged()
	{
		ATLTRACE(_T("OnBackStyleChanged\n"));
	}
	OLE_COLOR m_clrBorderColor;
	void OnBorderColorChanged()
	{
		ATLTRACE(_T("OnBorderColorChanged\n"));
	}
	BOOL m_bBorderVisible;
	void OnBorderVisibleChanged()
	{
		ATLTRACE(_T("OnBorderVisibleChanged\n"));
	}

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
		m_wndHidden.KillThisTimer();
		if (m_pGif)
		{
			delete m_pGif;
			m_pGif = NULL;
		}

	}
	virtual HRESULT OnDraw(ATL_DRAWINFO& di);
	LRESULT OnTimer(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
private:
	int m_Width;
	int m_Height;

public:
	STDMETHOD(Play)(void);
	STDMETHOD(LoadFromFile)(CHAR* szFileName);
};

OBJECT_ENTRY_AUTO(__uuidof(GifAnimator), CGifAnimator)
