﻿// GifAnimator.cpp : CGifAnimator 的实现
#include "stdafx.h"
#include "GifAnimator.h"

LRESULT CWinHidden::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL bHandled)
{
	if ((UINT)m_nTimer != wParam)
	{
		return -1;
	}
	KillTimer(m_nTimer);
	if (m_pFullCtrl != NULL)
	{
		m_pFullCtrl->OnTimer(uMsg, wParam, lParam, bHandled);
	}
	return 0;
};

// CGifAnimator

HRESULT CGifAnimator::OnDraw(ATL_DRAWINFO& di)
{
	Graphics g(di.hdcDraw);
	Rect rc(di.prcBounds->left, di.prcBounds->top, m_Width, m_Height);
	g.DrawImage(m_pGif->GetImg(), rc);
	return S_OK;
}

LRESULT CGifAnimator::OnTimer(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if (m_pGif->IsLastFrame())
	{
		m_pGif->SelectFrame(0);
		m_wndHidden.SetThisTimer(1, 1000, NULL);
	}
	else
	{
		m_pGif->NextFrame();
		m_wndHidden.SetThisTimer(1, m_pGif->GetFrameTime() * 10, NULL);
	}

	FireViewChange();
	return 0;
}

STDMETHODIMP CGifAnimator::Play(void)
{
	m_pGif->SelectFrame(0);
	if (m_pGif->IsAnimated())
	{
		m_wndHidden.Attach(this);
		m_wndHidden.SetThisTimer(1, m_pGif->GetFrameTime() * 10, NULL);
	}
	FireViewChange();
	return S_OK;
}

STDMETHODIMP CGifAnimator::LoadFromFile(CHAR* szFileName)
{
	FILE* pFile = NULL;
	pFile = fopen(szFileName, "rb");
	if (pFile == NULL)
	{
		return S_FALSE;
	}
	fseek(pFile, 0, SEEK_SET);
	UINT nPos = ftell(pFile);
	fseek(pFile, 0, SEEK_END);
	UINT nFileSize = ftell(pFile) - nPos;
	BYTE* pBuf = new BYTE[nFileSize];
	fseek(pFile, 0, SEEK_SET);
	fread(pBuf, sizeof(BYTE), nFileSize, pFile);
	fclose(pFile);

	m_pGif = new CGif(pBuf, nFileSize);
	m_Width = m_pGif->GetImg()->GetWidth();
	m_Height = m_pGif->GetImg()->GetHeight();
	SIZEL sl;
	sl.cx = m_Width;
	sl.cy = m_Height;
	AtlPixelToHiMetric(&sl, &m_sizeExtent);
	return S_OK;
}
