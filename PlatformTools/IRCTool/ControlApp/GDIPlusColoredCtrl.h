﻿/** @file CGDIPlusColoredCtrl.h 
@brief 自定义控件
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-12-07
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#pragma once
#include "afxwin.h"
#include "ControlDef.h"
#include "GloableConfig.h"

#ifndef _GDIPLUSCOLOREDCTRL_
#define _GDIPLUSCOLOREDCTRL_

class CONTROLAPP_ENTRY CGDIPlusColoredCtrl
{
public:
	CGDIPlusColoredCtrl(void);
	~CGDIPlusColoredCtrl(void);

	// 控件状态 [12/7/2009 hemeng]
	enum CONTROL_STATE
	{
		ST_NORMAL = 0,
		ST_HOVER,
		ST_PRESS,
		ST_FOCUSE,
		ST_DISABLE,

		ST_COUNT
	};

	struct stColoredText
	{
		stColoredText()
			:	m_colText(Color::Black),
				m_colDisable(Color::Gray),
				m_fstText(FontStyleRegular)
		{
			m_strText = _T("控件");
			m_ftText = _T("宋体");
			m_fTextSize = 10;	
		}

		~stColoredText()
		{
		
		}

		void operator = (stColoredText& src)
		{
			m_colText = src.m_colText;
			m_strText = src.m_strText;
			m_fTextSize = src.m_fTextSize;
			m_fstText = src.m_fstText;
			m_ftText = src.m_ftText;
			m_colDisable = src.m_colDisable;
		}

		Color	m_colText;
		Color	m_colDisable;
		CString	m_strText;				// text message [12/7/2009 hemeng]
		CString m_ftText;				// text font [12/7/2009 hemeng]
		float	m_fTextSize;			// text size [12/7/2009 hemeng]
		FontStyle	m_fstText;			// text style [12/7/2009 hemeng]
	};

public:
	// functions [12/7/2009 hemeng]
	DWORD	SetCtrlCursor(CWnd* pWnd,int nCursorId = NULL, BOOL bRepaint = TRUE);

	void	SetToolTipText(int nText,CWnd* pWnd, BOOL bActive = TRUE);
	void	SetToolTipText(LPCTSTR lpszText, CWnd* pWnd, BOOL bActive = TRUE);
	void	ActivateTooltip(BOOL bEnable = TRUE);

	void	SetCtrlBKColor(Color col)	{m_colBK = col;};
	void	SetCtrlBorderColor(Color col)	{m_colBorder = col;};

	void	DrawTransparent(bool bTransDraw)	{m_bDrawTransparent = bTransDraw;};
	void	SetCtrlAlphaBlend(int iAlpha, bool bTransDraw = true);

	void	SetCtrlText(CWnd* pWnd,stColoredText stText);
	stColoredText GetCtrlText() {return m_stText;};

protected:
	// variables [12/7/2009 hemeng]
	Color	m_colBK;
	Color	m_colBorder;
	int		m_iAlpha;
	int		m_iArcPercent;

	bool	m_bDrawTransparent;

	CONTROL_STATE m_curState;		// 当前控件状态 [12/7/2009 hemeng]

	CToolTipCtrl	m_ToolTip;
	stColoredText	m_stText;
	
	HCURSOR	m_hCursor;

protected:
	// functions [12/7/2009 hemeng]
	virtual void InitToolTip(CWnd* pWnd);
	virtual void DrawCtrlRect(CDC* pDC, Rect& rtDraw,Color colRes, Color colDes);
	virtual void DrawCtrlText(CDC* pDC, stColoredText& stText, Rect& rtDraw);
	virtual void DrawCtrlBorder(CDC* pDC, Rect& rtDraw,float fBorderWidth,Color colBorder);
};

#endif
