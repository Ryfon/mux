﻿#include "stdafx.h"
#include "GDIPlusColoredButton.h"

CGDIPlusColoredButton::CGDIPlusColoredButton(void)
:	m_bMouseOnButton(FALSE)
{
	m_nTypeStyle = BS_TYPEMASK;
	m_bMouseOnButton = FALSE;
	m_nState = ST_NORMAL;
	m_colBK = Color(150,150,150);

	m_pBps[0] = 0;
	m_pBps[1] = 0.4;
	m_pBps[2] = 0.6;
	m_pBps[3] = 1.0;
}

CGDIPlusColoredButton::~CGDIPlusColoredButton(void)
{
}

BEGIN_MESSAGE_MAP(CGDIPlusColoredButton, CButton)
	ON_WM_MOUSEMOVE()
	ON_WM_CANCELMODE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
//	ON_WM_ACTIVATE()
END_MESSAGE_MAP()

void CGDIPlusColoredButton::PreSubclassWindow()
{
	// TODO: 在此添加专用代码和/或调用基类
	UINT nBS;

	nBS = GetButtonStyle();

	// Set initial control type
	m_nTypeStyle = nBS & BS_TYPEMASK;

	// Set initial default state flag
	if (m_nTypeStyle == BS_DEFPUSHBUTTON)
	{
		// Adjust style for default button
		m_nTypeStyle = BS_PUSHBUTTON;
	} // If


	// You should not set the Owner Draw before this call
	// (don't use the resource editor "Owner Draw" or
	// ModifyStyle(0, BS_OWNERDRAW) before calling PreSubclassWindow() )
	ASSERT(m_nTypeStyle != BS_OWNERDRAW);

	// 设置为ownerdraw [12/8/2009 hemeng]
	ModifyStyle(BS_TYPEMASK , BS_OWNERDRAW, SWP_FRAMECHANGED);

	__super::PreSubclassWindow();	

	CString strTmp;
	GetWindowText(strTmp);
	m_stText.m_strText = strTmp;

}

BOOL CGDIPlusColoredButton::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	InitToolTip(this);
	m_ToolTip.RelayEvent(pMsg);

	return __super::PreTranslateMessage(pMsg);
}

void CGDIPlusColoredButton::DrawCtrlRect(CDC *pDC, Gdiplus::Rect &rtDraw,Color colTop, Color colBut)
{
	if (NULL == pDC )
	{
		return;
	}

	Graphics grahp(pDC->m_hDC);


	LinearGradientBrush br(rtDraw,colTop,colBut,LinearGradientModeVertical);
		
	GraphicsPath* pRoundPath = CGDIPlusHelper::MakeRoundRect(rtDraw,m_iArcPercent);
	
	Region rgDraw(pRoundPath);

	if (m_bDrawTransparent)
	{
		BYTE r,g,b;
		r = colTop.GetR();
		g = colTop.GetG();
		b = colTop.GetB();

		colTop = Color::MakeARGB(m_iAlpha,r,g,b);

		r = colBut.GetR();
		g = colBut.GetG();
		b = colBut.GetB();

		colBut = Color::MakeARGB(m_iAlpha,r,g,b);
	}
	
	if (m_nState == ST_PRESS)
	{
		br.SetLinearColors(colBut, colTop);
		
	}
	else
	{
		Color	pColor[4] = {colTop,colTop,colBut,colBut};
		br.SetInterpolationColors(pColor,m_pBps,4);
	}	
	
	grahp.FillRegion(&br,&rgDraw);
}

void CGDIPlusColoredButton::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	// TODO:  添加您的代码以绘制指定项
	UINT nCtrlType = lpDIS->CtlType;

	// 非ownerdraw button [12/8/2009 hemeng]
	if (nCtrlType != ODT_BUTTON )
	{
		__super::DrawItem(lpDIS);
		return;
	}

	// 判定状态 [12/8/2009 hemeng]
	m_nState = ST_NORMAL;
	bool bPressed = lpDIS->itemState & ODS_SELECTED;

	if (lpDIS->itemState == ODS_DISABLED)
	{
		m_nState = ST_DISABLE;
	}
	else if (lpDIS->itemState & ODS_FOCUS && !m_bMouseOnButton)
	{
		m_nState = ST_FOCUSE;
	}
	else if(bPressed && m_bMouseOnButton)
	{
		m_nState = ST_PRESS;
	}
	else if (m_bMouseOnButton)
	{
		m_nState = ST_HOVER;
	}

	CRect itemRect = lpDIS->rcItem;
	Rect rtDraw(itemRect.top,itemRect.left,itemRect.Width(),itemRect.Height());
	Rect rtBorder(rtDraw);
	float	fBorderWidth = 1.0f;;
	rtBorder.Width -= fBorderWidth;
	rtBorder.Height -= fBorderWidth;

	CDC*	pDC = CDC::FromHandle(lpDIS->hDC);
	pDC->SetBkMode(TRANSPARENT);
	
	Graphics graph(pDC->m_hDC);
	graph.Clear(Color::White);
	DrawCtrlRect(pDC,rtDraw,Color::White,m_colBK);
	DrawCtrlBorder(pDC,rtBorder,fBorderWidth,m_colBorder);
	DrawCtrlText(pDC,m_stText,rtDraw);

	if (m_nState == ST_FOCUSE)
	{
		/*int iOffset  = 2;
		rtDraw.Y += iOffset;
		rtDraw.X += iOffset;
		rtDraw.Width -= iOffset * 3;
		rtDraw.Height -= iOffset * 3;	
		Color colFoc = Color::SteelBlue;
		DrawCtrlBorder(pDC,rtDraw,fBorderWidth,colFoc);*/
	}
	else if (m_nState == ST_HOVER)
	{
		int iOffset  = 1;
		rtDraw.Y += iOffset;
		rtDraw.X += iOffset;
		rtDraw.Width -= iOffset * 3;
		rtDraw.Height -= iOffset * 3;
		DrawCtrlBorder(pDC,rtDraw,fBorderWidth,m_colBorder);		
	}	
}

void CGDIPlusColoredButton::DrawCtrlBorder(CDC *pDC, Gdiplus::Rect &rtDraw, float fBorderWidth,Color col)
{
	//__super::DrawCtrlBorder(pDC,rtDraw,fBorderWidth,col);\
	
	Graphics gragh(pDC->m_hDC);

	Pen pBorder(col, fBorderWidth);


	GraphicsPath* pPath = CGDIPlusHelper::MakeRoundRect(rtDraw,m_iArcPercent);

	gragh.DrawPath(&pBorder,pPath);
}

void CGDIPlusColoredButton::DrawCtrlText(CDC* pDC, stColoredText& stText, Rect& rtDraw)
{
	Graphics graph(pDC->m_hDC);

	SolidBrush brText(m_stText.m_colText);

	if (m_nState == ST_DISABLE)
	{
		brText.SetColor(m_stText.m_colDisable);
	}
	else if (m_nState == ST_PRESS)
	{
		rtDraw.X += 2;
		rtDraw.Y += 2;
	}

	USES_CONVERSION;
	LPWSTR lpwstrMsg = A2W(m_stText.m_ftText);
	Font ftText(lpwstrMsg,m_stText.m_fTextSize,m_stText.m_fstText);
	RectF frt(rtDraw.X,rtDraw.Y,rtDraw.Width,rtDraw.Height);

	StringFormat* pFormat = new StringFormat();
	pFormat->SetAlignment(StringAlignmentCenter);
	pFormat->SetLineAlignment(StringAlignmentCenter);
	
	lpwstrMsg = A2W(m_stText.m_strText);
	int iStrLen = wcslen(lpwstrMsg);
	graph.DrawString(lpwstrMsg,iStrLen,
		&ftText,frt,pFormat,&brText);
}


void CGDIPlusColoredButton::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CWnd*			wndUnderMouse = NULL;
	CWnd*			wndActive = this;
	TRACKMOUSEEVENT	csTME;

	CButton::OnMouseMove(nFlags, point);
	ClientToScreen(&point);
	wndUnderMouse = WindowFromPoint(point);

	if (wndUnderMouse && wndUnderMouse->m_hWnd == m_hWnd &&
		wndActive)
	{
		if (!m_bMouseOnButton)
		{
			m_bMouseOnButton = TRUE;

			Invalidate();

			csTME.cbSize = sizeof(csTME);
			csTME.dwFlags = TME_LEAVE;
			csTME.hwndTrack = m_hWnd;
			::_TrackMouseEvent(&csTME);
		}
	}
	else
	{
		CancelHover();
	}
	//__super::OnMouseMove(nFlags, point);
}

void CGDIPlusColoredButton::OnCancelMode()
{
	__super::OnCancelMode();

	// TODO: 在此处添加消息处理程序代码
	CancelHover();
	
}

void CGDIPlusColoredButton::CancelHover()
{
	if (m_bMouseOnButton)
	{
		m_bMouseOnButton = FALSE;
		Invalidate();
	}
}

LRESULT CGDIPlusColoredButton::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	CancelHover();
	return 0;
}
