﻿#pragma once
#include "afxwin.h"
#include "GDIPlusColoredCtrl.h"

class CONTROLAPP_ENTRY CGDIPlusColoredButton :
	public CButton ,public CGDIPlusColoredCtrl
{
public:
	CGDIPlusColoredButton(void);
	~CGDIPlusColoredButton(void);
protected:
	virtual void PreSubclassWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	BOOL	m_bMouseOnButton;
	UINT	m_nTypeStyle;		// Button style
	CONTROL_STATE m_nState;

	REAL	m_pBps[4];		// 渐变控制 [12/17/2009 hemeng]

protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnCancelMode();
	void	CancelHover();
	LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);

	virtual void DrawCtrlRect(CDC* pDC, Rect& rtDraw,Color colTop, Color colBot);
	virtual void DrawCtrlBorder(CDC* pDC, Rect& rtDraw,float fBorderWidth,Color col);
	virtual void DrawCtrlText(CDC* pDC, stColoredText& stText, Rect& rtDraw);
};


