﻿// GDIPlusColoredStatic.cpp : 实现文件
//

#include "stdafx.h"
#include "ControlApp.h"
#include "GDIPlusColoredStatic.h"


// CGDIPlusColoredStatic

IMPLEMENT_DYNAMIC(CGDIPlusColoredStatic, CStatic)

CGDIPlusColoredStatic::CGDIPlusColoredStatic()
{
	m_bDrawBorder = false;
	m_bDrawTransparent = true;
	m_iAlpha = 0;
}

CGDIPlusColoredStatic::~CGDIPlusColoredStatic()
{
}


BEGIN_MESSAGE_MAP(CGDIPlusColoredStatic, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CGDIPlusColoredStatic 消息处理程序



void CGDIPlusColoredStatic::PreSubclassWindow()
{
	// TODO: 在此添加专用代码和/或调用基类
	ModifyStyle(SS_TYPEMASK,SS_OWNERDRAW,SWP_FRAMECHANGED);

	__super::PreSubclassWindow();

	CString strTmp;
	GetWindowText(strTmp);
	m_stText.m_strText = strTmp;
}

void CGDIPlusColoredStatic::SetDrawBorder(bool bDraw)
{
	m_bDrawBorder = bDraw;
}

void CGDIPlusColoredStatic::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 __super::OnPaint()

	// 绘制窗体 [12/22/2009 hemeng]
	CRect rtWnd;
	GetClientRect(&rtWnd);
	Rect rcWnd;
	rcWnd.X = rtWnd.left;
	rcWnd.Y = rtWnd.top;
	rcWnd.Width = rtWnd.Width();
	rcWnd.Height = rtWnd.Height();

	DrawCtrlText(&dc,m_stText,rcWnd);

	if (m_bDrawBorder)
	{
		float fBorderWidth = 1.0f;
		DrawCtrlBorder(&dc,rcWnd,fBorderWidth,m_colBorder);
	}
}
