﻿#pragma once

#include"GDIPlusColoredCtrl.h"
// CGDIPlusColoredStatic

class CGDIPlusColoredStatic : public CStatic,public CGDIPlusColoredCtrl
{
	DECLARE_DYNAMIC(CGDIPlusColoredStatic)

public:
	CGDIPlusColoredStatic();
	virtual ~CGDIPlusColoredStatic();

	void SetDrawBorder(bool bDraw);

protected:
	DECLARE_MESSAGE_MAP()
	virtual void PreSubclassWindow();

protected:
	bool	m_bDrawBorder;
public:
	afx_msg void OnPaint();
};


