﻿// GDIPlusColoredDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "ControlApp.h"
#include "GDIPlusColoredDialog.h"


// CGDIPlusColoredDialog 对话框

IMPLEMENT_DYNAMIC(CGDIPlusColoredDialog, CDialog)

CGDIPlusColoredDialog::CGDIPlusColoredDialog(UINT nIDTemplate,CWnd* pParent /*=NULL*/)
	: CDialog(nIDTemplate, pParent)
{
	m_pDlgWndPath = NULL;

	m_iTitleOffset = 2;
	m_iTitleHeight = 25;

	m_hIcon = NULL;

	m_iArcPercent = 4;

	m_colBorder = Color::Gray;

	m_iOldX = m_iOldY = 0;
	m_bResize = false;

	m_colTitle = Color(100,100,100);

	m_pBps[0] = 0;
	m_pBps[1] = 0.1;
	m_pBps[2] = 0.3;
	m_pBps[3] = 1.0;

}

CGDIPlusColoredDialog::~CGDIPlusColoredDialog()
{
	if (m_pDlgWndPath)
	{
		delete m_pDlgWndPath;
		m_pDlgWndPath = NULL;
	}
}

void CGDIPlusColoredDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CGDIPlusColoredDialog, CDialog)
	ON_BN_CLICKED(IDC_GDIDIG_SYSBTN_CLOSE, &CGDIPlusColoredDialog::OnBnClickedCloseBtn)
	ON_BN_CLICKED(IDC_GDIDIG_SYSBTN_MIN, &CGDIPlusColoredDialog::OnBnClickedMinBtn)
	ON_BN_CLICKED(IDC_GDIDIG_SYSBTN_MAX, &CGDIPlusColoredDialog::OnBnClickedMaxBtn)
	ON_WM_PAINT()
	ON_WM_SIZE()
//	ON_WM_SIZING()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


// CGDIPlusColoredDialog 消息处理程序

void CGDIPlusColoredDialog::PreSubclassWindow()
{
	// TODO: 在此添加专用代码和/或调用基类
	// 取消对话框的所有外边框和标题栏，采用自绘方式 [12/15/2009 hemeng]
	ModifyStyle(GetSafeHwnd(),WS_CAPTION ,0,SWP_FRAMECHANGED);

	__super::PreSubclassWindow();
}


void CGDIPlusColoredDialog::InitDlgRegion(Rect rtWnd)
{
	if (m_pDlgWndPath)
	{
		delete m_pDlgWndPath;
		m_pDlgWndPath = NULL;
	}
	m_pDlgWndPath = CGDIPlusHelper::MakeRoundRect(rtWnd,m_iArcPercent);
	
	ChangeWindowRgn();
}

void CGDIPlusColoredDialog::InitSysCtrls(Rect rcWnd)
{
	int cxIcon = SIZE_SYS_ICON;
	int cyIcon = SIZE_SYS_ICON;

	CRect rtBtn;

	rtBtn.right = rcWnd.X + rcWnd.Width - OFFSET_SYS_BTN;
	rtBtn.left = rtBtn.right - cxIcon;
	rtBtn.top = rcWnd.Y + OFFSET_SYS_BTN;
	rtBtn.bottom = rtBtn.top + cyIcon;

	m_btnClose.Create(NULL,WS_CHILD | WS_VISIBLE,rtBtn,this,IDC_GDIDIG_SYSBTN_CLOSE);
	m_btnClose.SetToolTipText(_T("关闭"),&m_btnClose);

	rtBtn.right -= (OFFSET_SYS_BTN + cxIcon);
	rtBtn.left -= (OFFSET_SYS_BTN + cxIcon);
	m_btnMax.Create(NULL,WS_CHILD | WS_VISIBLE,rtBtn,this,IDC_GDIDIG_SYSBTN_MAX);
	m_btnMax.SetToolTipText(_T("最大化"),&m_btnMax);

	rtBtn.right -= (OFFSET_SYS_BTN + cxIcon);
	rtBtn.left -= (OFFSET_SYS_BTN + cxIcon);
	m_btnMin.Create(NULL,WS_CHILD | WS_VISIBLE,rtBtn,this,IDC_GDIDIG_SYSBTN_MIN);
	m_btnMin.SetToolTipText(_T("最小化"),&m_btnMin);

}

BOOL CGDIPlusColoredDialog::OnInitDialog()
{
	__super::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CRect rcWnd;
	GetClientRect(&rcWnd);

	m_iOldX = rcWnd.left;
	m_iOldY = rcWnd.top;

	CString strTmp;
	GetWindowText(strTmp);
	m_stText.m_strText = strTmp;

	CFont* pFont = new CFont();
	pFont->CreatePointFont((int)m_stText.m_fTextSize,m_stText.m_ftText);
	SetFont(pFont);
	delete pFont;
	pFont = NULL;

	Rect rtWnd;
	rtWnd.X = rcWnd.left;
	rtWnd.Y = rcWnd.top;
	rtWnd.Width = rcWnd.Width();
	rtWnd.Height = rcWnd.Height();
	InitDlgRegion(rtWnd);
	InitSysCtrls(rtWnd);

	SetWindowLong(GetSafeHwnd(),GWL_EXSTYLE,GetWindowLong(GetSafeHwnd(),GWL_EXSTYLE)^WS_EX_LAYERED);
	SetLayeredWindowAttributes(0,m_iAlpha,LWA_ALPHA);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CGDIPlusColoredDialog::SetTitleHeight(int iHeight)
{
	m_iTitleHeight = iHeight;
}	

void CGDIPlusColoredDialog::ReDrawSysCtrls()
{
	CRect rtClient;
	GetClientRect(&rtClient);

	CRect rtBtn;
	int cxIcon = SIZE_SYS_ICON;
	int cyIcon = SIZE_SYS_ICON;

	// close btn [10/3/2009 hemeng]
	rtBtn.right = rtClient.Width() -  SIZE_SYS_ICON;
	rtBtn.top = rtClient.top + OFFSET_SYS_BTN;
	rtBtn.left = rtBtn.right - cxIcon;
	rtBtn.bottom = rtBtn.top + cyIcon;

	m_btnClose.MoveWindow(rtBtn);

	rtBtn.right -= (OFFSET_SYS_BTN + cxIcon);
	rtBtn.left -= (OFFSET_SYS_BTN + cxIcon);

	m_btnMax.MoveWindow(rtBtn);

	rtBtn.right -= (OFFSET_SYS_BTN + cxIcon);
	rtBtn.left -= (OFFSET_SYS_BTN + cxIcon);

	m_btnMin.MoveWindow(rtBtn);
}

void CGDIPlusColoredDialog::UpdateDlgParams()
{
	CRect rcWnd;
	GetClientRect(&rcWnd);	

	Rect rtWnd;
	rtWnd.X = rtWnd.Y = 0;
	rtWnd.Width = rcWnd.Width();
	rtWnd.Height = rcWnd.Height();

	// 如果是从最大化恢复，则使用旧的起点坐标绘制对话框 [12/17/2009 hemeng]
	if (m_bResize)
	{
		rcWnd.left = m_iOldX;
		rcWnd.top = m_iOldY;
		rcWnd.right = rcWnd.left + rtWnd.Width;
		rcWnd.bottom = rcWnd.top + rtWnd.Height;
	}	

	rtWnd.Width = rcWnd.Width();
	rtWnd.Height = rcWnd.Height();

	MoveWindow(rcWnd,TRUE);

	InitDlgRegion(rtWnd);
	
}

void CGDIPlusColoredDialog::OnBnClickedCloseBtn()
{
	SendMessage(WM_CLOSE);
}

void CGDIPlusColoredDialog::OnBnClickedMaxBtn()
{
	if (IsZoomed())
	{
		// 需要使用原位置绘制 [12/17/2009 hemeng]
		m_bResize = true;
		SendMessage(WM_SYSCOMMAND,SC_RESTORE/*,MAKELPARAM(point.x,point.y)*/);	
	}
	else
	{
		// 最大化，记录原位置，并设置不使用原位置绘制 [12/17/2009 hemeng]
		CRect rtWnd;
		GetWindowRect(&rtWnd);
		m_iOldX = rtWnd.left;
		m_iOldY = rtWnd.top;
		m_bResize = false;
		SendMessage(WM_SYSCOMMAND,SC_MAXIMIZE/*,MAKELPARAM(point.x,point.y)*/);		
	}
}

void CGDIPlusColoredDialog::OnBnClickedMinBtn()
{
	SendMessage(WM_SYSCOMMAND,SC_MINIMIZE);
}

void CGDIPlusColoredDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 __super::OnPaint()
	SetLayeredWindowAttributes(0,m_iAlpha,LWA_ALPHA);

	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		Graphics graph(dc.m_hDC);
		DrawDlg(graph);
		ReDrawSysCtrls();
		DrawTitle(graph);
	}
}

void CGDIPlusColoredDialog::DrawDlg(Graphics& graph)
{
	if (m_pDlgWndPath)
	{
		CRect rtWnd;
		GetClientRect(&rtWnd);

		Rect rcWnd;
		rcWnd.X = rtWnd.left;
		rcWnd.Y = rtWnd.top;
		rcWnd.Width = rtWnd.Width();
		rcWnd.Height = rtWnd.Height();

		Region rgDraw(m_pDlgWndPath);
		LinearGradientBrush br(rcWnd,Color::White,m_colBK,LinearGradientModeVertical);
		float fBorderWidth = 2.0f;
		Pen pBorder(m_colBorder, fBorderWidth);

		graph.FillRegion(&br,&rgDraw);
		graph.DrawPath(&pBorder,m_pDlgWndPath);

	}
}

void CGDIPlusColoredDialog::DrawTitle(Graphics& graph)
{
	if (m_stText.m_strText.GetLength() > 0)
	{	
		CRect rtWnd;
		GetClientRect(&rtWnd);

		Rect rcWnd;
		rcWnd.X = rtWnd.left;
		rcWnd.Y = rtWnd.top;
		rcWnd.Width = rtWnd.Width();
		rcWnd.Height = rtWnd.Height();

		rcWnd.X += m_iTitleOffset;
		rcWnd.Width -= m_iTitleOffset * 2;
		rcWnd.Y += 1;
		rcWnd.Height = m_iTitleHeight;

		m_rtCaption.X = rcWnd.X + OFFSET_SYS_BTN;
		m_rtCaption.Y = rcWnd.Y + m_iTitleHeight - SIZE_SYS_ICON - OFFSET_SYS_BTN;
		m_rtCaption.Width = m_stText.m_fTextSize * m_stText.m_strText.GetLength();
		m_rtCaption.Height = SIZE_SYS_ICON;


		Color colTop = Color::White;
		LinearGradientBrush br(rcWnd,colTop,m_colTitle,LinearGradientModeVertical);
		Color	pColor[4] = {colTop,colTop,m_colTitle,m_colTitle};
		br.SetInterpolationColors(pColor,m_pBps,4);
		
		graph.FillRectangle(&br,rcWnd);

		USES_CONVERSION;
		LPWSTR lpwstrMsg = A2W(m_stText.m_ftText);
		Font ftText(lpwstrMsg,m_stText.m_fTextSize,m_stText.m_fstText);
		RectF frt(m_rtCaption.X,m_rtCaption.Y,m_rtCaption.Width,m_rtCaption.Height);

		StringFormat* pFormat = new StringFormat();
		pFormat->SetAlignment(StringAlignmentCenter);
		pFormat->SetLineAlignment(StringAlignmentCenter);

		lpwstrMsg = A2W(m_stText.m_strText);
		int iStrLen = wcslen(lpwstrMsg);
		SolidBrush brText(m_stText.m_colText);
		graph.DrawString(lpwstrMsg,iStrLen,
			&ftText,frt,pFormat,&brText);
	}
}

void CGDIPlusColoredDialog::ChangeWindowRgn()
{
	if (m_pDlgWndPath)
	{
		Region rg(m_pDlgWndPath);

		Graphics graph(GetSafeHwnd());
		if (rg.IsEmpty(&graph))
		{
			return;
		}

		HRGN hRgn = rg.GetHRGN(&graph);
		SetWindowRgn(hRgn,TRUE);
	}
}



void CGDIPlusColoredDialog::OnSize(UINT nType, int cx, int cy)
{
	__super::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	if (nType != SIZE_MINIMIZED)
	{
		UpdateDlgParams();	
	}
	
	
}

void CGDIPlusColoredDialog::OnSizing(UINT fwSide, LPRECT pRect)
{
	__super::OnSizing(fwSide, pRect);

	// TODO: 在此处添加消息处理程序代码

	UpdateDlgParams();	
}

void CGDIPlusColoredDialog::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	//禁止显示移动矩形窗体框
	::SystemParametersInfo(SPI_SETDRAGFULLWINDOWS,TRUE,NULL,0);
	//非标题栏移动整个窗口
	SendMessage(WM_SYSCOMMAND,0xF012,0);  

	__super::OnLButtonDown(nFlags, point);
}
