﻿#include "StdAfx.h"
#include "GDIPlusHelper.h"

CGDIPlusHelper::CGDIPlusHelper(void)
{
}

CGDIPlusHelper::~CGDIPlusHelper(void)
{
}

GraphicsPath* CGDIPlusHelper::MakeRoundRect(Rect rect, INT percentageRounded)
{
	ASSERT(percentageRounded >=1 && percentageRounded <= 100);

	INT left = rect.X;
	INT right = rect.X + rect.Width;
	INT top = rect.Y;
	INT bottom = rect.Y + rect.Height;

	INT offsetX = (right - left) * percentageRounded / 100;
	INT offsetY = (bottom - top) * percentageRounded / 100;

	GraphicsPath pt;
	GraphicsPath* path = pt.Clone();

	path->AddArc(right - offsetX, top, offsetX, offsetY, 270, 90);
	path->AddArc(right - offsetX, bottom - offsetY, offsetX, offsetY, 0, 90);
	path->AddArc(left, bottom - offsetY, offsetX, offsetY, 90, 90);
	path->AddArc(left, top, offsetX, offsetY, 180, 90);
	path->AddLine(left + offsetX, top, right - offsetX / 2, top);

	return path;

}
