﻿#include "stdafx.h"
#include "GDIPlusColoredCtrl.h"

CGDIPlusColoredCtrl::CGDIPlusColoredCtrl(void)
:	m_colBK(Color(200,200,200)),	
	m_colBorder(Color::Gray),
	m_bDrawTransparent(false)
{
	m_ToolTip.m_hWnd = NULL;

	m_hCursor = NULL;

	m_iAlpha = 255;	

	m_iArcPercent = 20;
}

CGDIPlusColoredCtrl::~CGDIPlusColoredCtrl(void)
{
	if (m_hCursor)
	{
		::DestroyCursor(m_hCursor);
	}	
}

void CGDIPlusColoredCtrl::InitToolTip(CWnd* pWnd)
{
	if (NULL == m_ToolTip.m_hWnd)
	{
		// Create ToolTip control [12/07/2009 hemeng]
		m_ToolTip.Create(pWnd);
		// Create inactive [12/07/2009 hemeng]
		m_ToolTip.Activate(FALSE);
		// Enable multiline [12/07/2009 hemeng]
		m_ToolTip.SendMessage(TTM_SETMAXTIPWIDTH,0,400);
	}
}

DWORD CGDIPlusColoredCtrl::SetCtrlCursor(CWnd* pWnd,int nCursorId /* = NULL */, BOOL bRepaint /* = TRUE */)
{
	HINSTANCE	hInstResource = NULL;
	// Destroy any previous cursor
	if (m_hCursor)
	{
		::DestroyCursor(m_hCursor);
		m_hCursor = NULL;
	} // if

	// Load cursor
	if (nCursorId)
	{
		hInstResource = AfxFindResourceHandle(MAKEINTRESOURCE(nCursorId), RT_GROUP_CURSOR);
		// Load cursor resource
		m_hCursor = (HCURSOR)::LoadImage(hInstResource, MAKEINTRESOURCE(nCursorId), IMAGE_CURSOR, 0, 0, 0);
		// Repaint the button
		if (bRepaint) 
			pWnd->Invalidate();
		// If something wrong
		if (m_hCursor == NULL) return -1;
	} // if

	return 0;
}

void CGDIPlusColoredCtrl::SetToolTipText(LPCTSTR lpszText,CWnd* pWnd, BOOL bActive /* = TRUE */)
{
	if (lpszText == NULL) return;

	// Initialize ToolTip
	InitToolTip(pWnd);

	// If there is no tooltip defined then add it
	if (m_ToolTip.GetToolCount() == 0)
	{
		CRect rectBtn; 
		GetClientRect(pWnd->m_hWnd,rectBtn);
		m_ToolTip.AddTool(pWnd, lpszText, rectBtn, 1);
	} // if

	// Set text for tooltip
	m_ToolTip.UpdateTipText(lpszText, pWnd, 1);
	m_ToolTip.Activate(bActive);
}

void CGDIPlusColoredCtrl::SetToolTipText(int nText, CWnd* pWnd,BOOL bActive /* = TRUE */)
{
	CString sText;

	// Load string resource
	sText.LoadString(nText);
	// If string resource is not empty
	if (sText.IsEmpty() == FALSE) 
		SetToolTipText((LPCTSTR)sText,pWnd, bActive);
}

void CGDIPlusColoredCtrl::ActivateTooltip(BOOL bEnable)
{
	// If there is no tooltip then do nothing
	if (m_ToolTip.GetToolCount() == 0) return;

	// Activate tooltip
	m_ToolTip.Activate(bEnable);
}

void CGDIPlusColoredCtrl::SetCtrlAlphaBlend(int iAlpha,bool bTransDraw)
{
	if (iAlpha < 0)
	{
		iAlpha = 0;
	}
	else if (iAlpha > 255)
	{
		iAlpha = 255;
	}

	m_iAlpha = iAlpha;
	m_bDrawTransparent = bTransDraw;
}

void CGDIPlusColoredCtrl::SetCtrlText(CWnd* pWnd,stColoredText stText)
{
	m_stText = stText;

	pWnd->SetWindowText(m_stText.m_strText);
}

void CGDIPlusColoredCtrl::DrawCtrlRect(CDC* pDC, Rect& rtDraw,Color colRes, Color colDes)
{
	if (NULL == pDC)
	{
		return;
	}

	Graphics grahp(pDC->m_hDC);

	Color colTop,colBottom;

	if (m_bDrawTransparent)
	{
		colTop = Color(m_iAlpha,colRes.GetR(),colRes.GetG(),colRes.GetB());
		colBottom = Color(m_iAlpha,colDes.GetR(),colDes.GetG(),colDes.GetB());
	}
	else
	{
		colTop = colRes;
		colBottom = colDes;
	}

	LinearGradientBrush lgBrush(rtDraw,colBottom, colTop, LinearGradientModeVertical);

	GraphicsPath* pRoundPath = CGDIPlusHelper::MakeRoundRect(rtDraw,m_iArcPercent);

	Region rgDraw(pRoundPath);

	grahp.FillRegion(&lgBrush,&rgDraw);

}

void CGDIPlusColoredCtrl::DrawCtrlText(CDC* pDC, stColoredText& stText, Rect& rtDraw)
{
	Graphics graph(pDC->m_hDC);

	SolidBrush brText(m_stText.m_colText);
	if (m_curState == ST_DISABLE)
	{
		brText.SetColor(m_stText.m_colDisable);
	}

	USES_CONVERSION;
	LPWSTR lpwstrMsg = A2W(m_stText.m_ftText);
	Font ftText(lpwstrMsg,m_stText.m_fTextSize,m_stText.m_fstText);
	RectF frt(rtDraw.X,rtDraw.Y,rtDraw.Width,rtDraw.Height);

	StringFormat* pFormat = new StringFormat();
	pFormat->SetAlignment(StringAlignmentCenter);
	pFormat->SetLineAlignment(StringAlignmentCenter);
	
	lpwstrMsg = A2W(m_stText.m_strText);
	int iStrLen = wcslen(lpwstrMsg);
	graph.DrawString(lpwstrMsg,iStrLen,
		&ftText,frt,pFormat,&brText);
}

void CGDIPlusColoredCtrl::DrawCtrlBorder(CDC* pDC, Rect& rtDraw,float fBorderWidth,Color col)
{
	Graphics gragh(pDC->m_hDC);
	
	Pen pBorder(col, fBorderWidth);
	
	rtDraw.X += fBorderWidth / 2;
	rtDraw.Y += fBorderWidth / 2;

	rtDraw.Width -= fBorderWidth;
	rtDraw.Height -= fBorderWidth;

	GraphicsPath* pRoundPath = CGDIPlusHelper::MakeRoundRect(rtDraw,m_iArcPercent);

	gragh.DrawPath(&pBorder,pRoundPath);

}