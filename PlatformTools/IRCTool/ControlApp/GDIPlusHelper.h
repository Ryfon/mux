﻿#pragma once

class CGDIPlusHelper
{
public:
	CGDIPlusHelper(void);
	~CGDIPlusHelper(void);

	// 创建圆角矩形 [12/14/2009 hemeng]
	static GraphicsPath* MakeRoundRect(Rect rect, INT percentageRounded);
};
