﻿#pragma once
#include "GDIPlusColoredCtrl.h"
#include "GDIPlusColoredButton.h"
#include "GDIPlusColoredStatic.h"
#include "GDIPlusColoredButton.h"

// CGDIPlusColoredDialog 对话框
#define IDC_GDIDIG_SYSBTN_CLOSE	 10201	// 关闭按钮 [12/14/2009 hemeng]
#define IDC_GDIDIG_SYSBTN_MIN	 10202	// 最小化按钮 [12/14/2009 hemeng]
#define IDC_GDIDIG_SYSBTN_MAX	 10203	// 最大化按钮 [12/14/2009 hemeng]

class CONTROLAPP_ENTRY CGDIPlusColoredDialog : public CDialog, public CGDIPlusColoredCtrl
{
	DECLARE_DYNAMIC(CGDIPlusColoredDialog)

public:
	CGDIPlusColoredDialog(UINT nIDTemplate,CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CGDIPlusColoredDialog();

	int	GetTitleHeight()	{return m_iTitleHeight;};
	void SetTitleHeight(int iHeight);

// 对话框数据
	//enum { IDD = IDD_GDIPLUSCOLOREDDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual void InitDlgRegion(Rect rtWnd);
	virtual void InitSysCtrls(Rect rtWnd);

	virtual void DrawDlg(Graphics& graph);
	virtual void DrawTitle(Graphics& graph);
	virtual void ReDrawSysCtrls();
	void	UpdateDlgParams();

	void	ChangeWindowRgn();

	DECLARE_MESSAGE_MAP()
	virtual void PreSubclassWindow();
	afx_msg void OnBnClickedCloseBtn();
	afx_msg void OnBnClickedMinBtn();
	afx_msg void OnBnClickedMaxBtn();


protected:
	// 成员变量 [12/15/2009 hemeng]
	GraphicsPath*	m_pDlgWndPath;		// 对话框区域 [12/15/2009 hemeng]
	Rect			m_rtCaption;		// caption位置 [12/15/2009 hemeng]

	int	m_iTitleOffset;			// 标题栏向内的偏移值 [12/15/2009 hemeng]
	int m_iTitleHeight;			// 标题栏高度 [12/15/2009 hemeng]

	HICON	m_hIcon;			// 系统图标 [10/3/2009 hemeng]
	CGDIPlusColoredButton m_btnMin;
	CGDIPlusColoredButton m_btnMax;
	CGDIPlusColoredButton m_btnClose;
	
	int		m_iOldX;		// 旧的起点坐标X [12/17/2009 hemeng]
	int		m_iOldY;		// 旧的起点坐标Y [12/17/2009 hemeng]
	bool	m_bResize;		// 是否采用旧坐标起点绘制 [12/17/2009 hemeng]

	bool	m_bShadow;		// 是否绘制阴影 [12/17/2009 hemeng]

	Color	m_colTitle;
	REAL	m_pBps[4];		// 渐变控制 [12/17/2009 hemeng]

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};
