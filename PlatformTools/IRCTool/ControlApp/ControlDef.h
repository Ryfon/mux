﻿#ifndef _CONTROLDEF_H_
#define _CONTROLDEF_H_

#ifdef CONTROLAPP_EXPORT
// DLL library project uses this
#define CONTROLAPP_ENTRY __declspec(dllexport)
#else
#ifdef CONTROLAPP_IMPORT
// client of DLL uses this
#define CONTROLAPP_ENTRY __declspec(dllimport)
#else
// static library project uses this
#define CONTROLAPP_ENTRY
#endif
#endif



#endif