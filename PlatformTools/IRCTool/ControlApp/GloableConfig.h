﻿#include "GDIPlusHelper.h"

static ColorMatrix g_szColorMatrixIdentity = {	1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 
0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 
0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 
};

#define SIZE_SYS_ICON	16	// 系统按钮大小 [12/16/2009 hemeng]
#define OFFSET_SYS_BTN	4	// 系统按钮据边框的距离 [12/15/2009 hemeng]