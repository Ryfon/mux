﻿/** @file ClientManager.h 
@brief 负责网络通讯功能的封装
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-01-17
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef CLIENTMANAGER_H
#define CLIENTMANAGER_H

#define WIN32_LEAN_AND_MEAN		// 从 Windows 头中排除极少使用的资料
#include <Windows.h>

#include <map>
using namespace std;

typedef void* HANDLE;
 
class CClientSession;
typedef map<string, CClientSession*> CLISESSIONMAP;

class CMemPool;
class CClientManager
{
public:
	CClientManager(void);
public:
	virtual ~CClientManager(void);
public:
	//启动Client管理器
	int StartCliMng();
	//停止Client管理器
	int StopCliMng();
	//增加一个连接
	CClientSession* AddClientSession(const string& strClientID);
	//删除一个连接
	int DeleteClientSession(const string& strClientID);
	//获得给定标示的客户端
	CClientSession* GetClientSession(const string& strClientID);
	size_t GetClientCount() const {return m_ClientSessions.size();}
	void CloseAll();
	//清除所有ClientSession
	void ClearAll();

	void AddToIocp(CClientSession* pClientSession);
	CMemPool* GetMemPool()
	{
		return m_MemPool;
	}
	HANDLE GetIocp() const
	{
		return m_Iocp;
	}
private:
	HANDLE m_Iocp; //Io完成端口，处理所有客户端连接的网络事件
	int m_ThreadNum; //工作线程数
	HANDLE* m_pThread; //工作线程数组
	CLISESSIONMAP m_ClientSessions; //维护客户端列表
	CRITICAL_SECTION m_CliSection; //保护客户端列表线程安全
	//是否成功加载Win库
	bool m_SockInitSuccess;
	CMemPool* m_MemPool; //内存池对象
	//初始化Winsock库
	void InitWinSock();
	//卸载Winsock库
	void UnInitWinSock();

private:
	//线程函数
	static unsigned __stdcall WorkThread(void* pThreadParams);
private:
	CClientManager(const CClientManager&);
	CClientManager& operator=(const CClientManager&);
};


#endif