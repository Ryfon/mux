﻿/** @file MemNode.h 
@brief 封装内存池的内存节点
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-01-20
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef MEMNODE_H
#define MEMNODE_H

class CMemPool;
class CMemNode: public OVERLAPPED
{
public:
	friend class CMemPool;
public:
	virtual ~CMemNode(void);
public:
	//把给定的数据放进缓冲区
	void AddData(const char* const pData, size_t DataLen);
	//获取缓冲区的已用大小
	size_t GetUsedSize() const;
	//获得缓冲区大小
	size_t GetSize() const;
	//获取数据指针
	char* GetBufferPrt();
	//设定要用的大小,一般和GetBufferPtr一起使用
	void SetUseSize(size_t UseSize);

	//设置、获取附加数据
	size_t GetBufType() const {return m_BufType;}
	void SetBufType(size_t BufType);
	//分割Buf
	CMemNode* SplitBuf(size_t RemoveDataLen);
	//获取新的MemNode
	CMemNode* NewMemNode();	
	//移除缓冲区中的部分数据
	void RemoveData(size_t RomoveDataLen);
	//增加一定数目的数据到给定的MemNode
	void AddToMemNode(CMemNode* pToMemNode, size_t nAddDataLen);
	//清空数据
	void Empty();

	//操作序列号
	unsigned long GetSeqNum() const {return m_SeqNum;}
	void SetSeqNum(unsigned long ulSeqNo);
public:
	CMemNode* m_NextNode;
private:
	//属于附加数据，标示这个数据节点的含义
	size_t m_BufType;
	const size_t m_BufSize; //缓冲区大小
	size_t m_UsedSize; //已使用的大小

	unsigned long m_SeqNum; //序列号

	//long m_RefCount; //引用计数
	CMemPool& m_MemPool; //内存池对象
#pragma warning(disable:4200) //禁掉0长度数组警告
	char m_Data[0]; //数据指针, 必须是最后一个数据成员
private:
	CMemNode(CMemPool& MemPool, size_t MemSize);
	//此处也应该提供数据版本的new与delete
	static void* operator new(size_t ObjSize, size_t BufSize);
	static void operator delete(void* pObj, size_t BufSize);	
	//禁用Copy构造函数、赋值运算符
	CMemNode(const CMemNode& );
	CMemNode& operator=(const CMemNode&);
};

#endif