﻿/**
* @file clinet.h
* @brief 客户端通信模块接口定义
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:clinet.h
* 摘    要:定义客户端通信模块接口
* 作    者:dzj
* 完成日期:2007.11.09
*/

#ifndef MUX_CLIMOD_INTERFACE
#define MUX_CLIMOD_INTERFACE

#ifndef CLIMOD_DLL //如果不是DLL，则定义此宏
#define MUXDLLFUN extern "C" __declspec(dllimport)
#endif //CLIMOD_DLL

namespace MUX_CLIMOD
{
	///版本号0.1；
	const int MUX_CLIMOD_VERSION[] = { 0, 1 };

	///成功，MUX_CLI_SUCESS 函数成功返回；
    const int MUX_CLI_SUCESS = 0;
	///错误，MUX_CLI_NORMALL_ERR，一般错误；
	const int MUX_CLI_NORMALL_ERR = 1000;
	///错误，MUX_CLI_CONNECT_ERR 连接失败；
	const int MUX_CLI_CONNECT_ERR = 1001;
	///错误，工作进行中
	const int MUX_CLI_INPROCESS = 1002;
	///错误，重复操作；
	const int MUX_CLI_REOPERA = 1003;
	///错误，尚未登录；
	const int MUX_CLI_NOTLOGIN = 1004;
	///错误，连接断开；
	const int MUX_CLI_DISCONNECT = 1005;

	///错误：未初始化;
	const int MUX_CLI_NOINIT = 1006;///错误：未初始化;
	///正常：操作正在进行中
	const int MUX_CLI_INPROGRESS   = 1007;//操作正在进行中

	///错误，连接断开；
	#define ERR_DISCONNECTED 2001
	///错误，接收失败；
    #define ERR_RCV_ERROR   2002
	///错误，发送失败；
    #define ERR_SEND_ERROR  2003

	///网络消息接收处理者
	class IMuxCliNetSinker;

	///网络通信主模块接口；
	class IMuxCliNetMod
	{
	public:
		///初始化客户端网络模块，输入参数为回调对象、服务器IP地址与端口；
		virtual int InitMuxCliNetMod( IMuxCliNetSinker* pMsgRcver, const char* assignSrvIp, WORD assignSrvPort ) = 0;
		///对应InitMuxCliNetMod的反初始化；
		virtual void UninitMuxCliNetMod() = 0;
		///向服务器请求区服务器列表；
		virtual int GetSrvList( unsigned short wRegion ) = 0;
		///客户端选择服务器；
		virtual int SelectSrv( unsigned short wSrvNo ) = 0;

		///异步，发登录请求，登录结果在OnLoginRes中返回；
		virtual int LoginReq( const char* userId, const char* userPassword ) = 0;
		///客户端选角色；
		virtual int SelectRole( unsigned int uiRoleNo ) = 0;
        ///客户端发送进入世界消息（游戏开始，进入场景）；
		virtual int EnterWorld() = 0;

		///查询网络状况接口，08.03.03新加;
		virtual int GetNetStat() = 0;//取网络繁忙状况，返回值[0-100],0：最空闲,100:最繁忙；

		//客户端必须在网络模块初始化成功后循环调用本函数以处理网络事件;
		virtual int ProcessNetEvent() = 0;
		///释放客户端网络模块；
		virtual void ReleaseMuxCliNetMod() = 0;

	//以下为发消息接口;
	public:
		///异步，查询帐号是否有人使用；
		virtual int TestAccountsReq( const char* accountsTest ) = 0;
		///异步，创建帐号请求；
		virtual int CreateAccountsReq( const char* userId, const char* userPassword ) = 0;
		///创建角色；
		virtual int CreateRole() = 0;
		///删除角色；
		virtual int DeleteRole() = 0;
		///异步，向服务器发消息；
		virtual int SendPkgReq( const DWORD dwPkgID, const char* pPkg, const DWORD dwLen ) = 0; 
		///返回距离商定时刻过去了多久(单位毫秒)，如果当前还未商定，则返回0；
		virtual unsigned long GetConnPassedMsec() = 0;
		///类似于底层函数time()
		virtual time_t Time() = 0;
		///获取日历时间
		virtual void GetCurDateTime( unsigned int& year, unsigned int& month, unsigned int& day, unsigned int& hour, unsigned int& minute, unsigned int& second ) = 0;

	public:
		///开启或关闭调试控制台窗口，第一次开启，第二次关闭；
		virtual void DbgConsole( const char* consoleLogFile=NULL/*是否同时将调试窗口信息输送至文件*/ ) = 0;		
		///发给调试控制台窗口的命令；
		/*
		   /d monsend msgID [-c]//将指定类型消息添加到发送监视列表,第一次执行开启监视，第二次执行关闭， -c打出消息各字段内容；
		   /d monrcv  msgID [-c]//将指定类型消息添加到接收监视列表,第一次执行开启监视，第二次执行关闭， -c打出消息各字段内容；
		   /d simsend msgID ... //模拟客户端向服务器发送指定类型消息,...为消息各字段内容。
		   /d simrcv  msgID ... //模拟服务器向客户端发送指定类型消息,...为消息各字段内容。
		*/
		virtual void DbgConsoleCmd( const char* ctrlCmd, ... ) = 0;
	};

	///网络事件回调者接口；
	class IMuxCliNetSinker
	{
	public:
		//虚析构；
		virtual ~IMuxCliNetSinker() {};

	public:		
		///回调，在此函数中返回登录结果；
		virtual int OnLoginRes( int errNo ) = 0;

		///回调，返回之前的测试帐号结果；
		virtual int OnTestAccountsRes( int errNo ) = 0;

		///回调，返回之前创建帐号请求的结果；
		virtual int OnCreateAccountsRes( int errNo ) = 0;

		///回调，收到服务器发来的消息；
		virtual int OnPkgRcved( unsigned short wCmd, const char* pPkg, const DWORD dwLen ) = 0;

		///回调，出错时回调；
		virtual int OnError( const int& errNo, const DWORD dwPkgID ) = 0;		
	};

	///网络模块获得；
	MUXDLLFUN int GetMuxCliNetMod( IMuxCliNetMod** pNetMod );

	///网络模块获得函数定义；
	typedef int ( * PFN_GetMuxCliNetMod )( IMuxCliNetMod** pNetMod );
}
#endif //MUX_CLIMOD_INTERFACE