﻿/** @file AppException.h 
@brief 写异常类封装
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-01-16
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef APPEXCEPTION_H
#define APPEXCEPTION_H

class CAppException: public exception
{
public:
	CAppException(const string& strWhere, const string& strMsg);
	virtual string GetWhere() const;
	virtual string GetMsg() const;
public:
	virtual ~CAppException(void);
protected:
	const string m_Where;
	const string m_Msg;
};

#endif