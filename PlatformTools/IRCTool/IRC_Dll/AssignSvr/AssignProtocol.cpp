﻿#include "AssignProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

Assign_namespace ::AssignProtocol::AssignProtocol()
{
	m_mapEnCodeFunc[G_A_SRV_MSG_ID             ]	=	&AssignProtocol::EnCode__GAGateSrvMsg;
	m_mapDeCodeFunc[G_A_SRV_MSG_ID             ]	=	&AssignProtocol::DeCode__GAGateSrvMsg;

	m_mapEnCodeFunc[C_A_QUERY_SRV_STATE_ID     ]	=	&AssignProtocol::EnCode__CAQuerySrvState;
	m_mapDeCodeFunc[C_A_QUERY_SRV_STATE_ID     ]	=	&AssignProtocol::DeCode__CAQuerySrvState;

	m_mapEnCodeFunc[A_C_NOTIFYSRVSTATEID       ]	=	&AssignProtocol::EnCode__ACNotifyASrvState;
	m_mapDeCodeFunc[A_C_NOTIFYSRVSTATEID       ]	=	&AssignProtocol::DeCode__ACNotifyASrvState;

	m_mapEnCodeFunc[C_A_REQUEST_GATESRV_ADDR_ID]	=	&AssignProtocol::EnCode__CARequestGateSrvAddr;
	m_mapDeCodeFunc[C_A_REQUEST_GATESRV_ADDR_ID]	=	&AssignProtocol::DeCode__CARequestGateSrvAddr;

	m_mapEnCodeFunc[A_C_NOTIFYGATESRVID        ]	=	&AssignProtocol::EnCode__ACNotifyGateSrvAddr;
	m_mapDeCodeFunc[A_C_NOTIFYGATESRVID        ]	=	&AssignProtocol::DeCode__ACNotifyGateSrvAddr;

}

Assign_namespace ::AssignProtocol::~AssignProtocol()
{
}

size_t	Assign_namespace ::AssignProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	Assign_namespace ::AssignProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

Assign_namespace ::EnCodeFunc	Assign_namespace ::AssignProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

Assign_namespace ::DeCodeFunc	Assign_namespace ::AssignProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	Assign_namespace ::AssignProtocol::EnCode__GAGateSrvMsg(void* pData)
{
	GAGateSrvMsg* pkGAGateSrvMsg = (GAGateSrvMsg*)(pData);

	//EnCode IP
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGAGateSrvMsg->IP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Port
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGAGateSrvMsg->Port), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Num
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGAGateSrvMsg->Num), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::DeCode__GAGateSrvMsg(void* pData)
{
	GAGateSrvMsg* pkGAGateSrvMsg = (GAGateSrvMsg*)(pData);

	//DeCode IP
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGAGateSrvMsg->IP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Port
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGAGateSrvMsg->Port), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Num
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGAGateSrvMsg->Num), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::EnCode__CAQuerySrvState(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::DeCode__CAQuerySrvState(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::EnCode__ACNotifyASrvState(void* pData)
{
	ACNotifyASrvState* pkACNotifyASrvState = (ACNotifyASrvState*)(pData);

	//EnCode State
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkACNotifyASrvState->State), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::DeCode__ACNotifyASrvState(void* pData)
{
	ACNotifyASrvState* pkACNotifyASrvState = (ACNotifyASrvState*)(pData);

	//DeCode State
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkACNotifyASrvState->State), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::EnCode__CARequestGateSrvAddr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::DeCode__CARequestGateSrvAddr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::EnCode__ACNotifyGateSrvAddr(void* pData)
{
	ACNotifyGateSrvAddr* pkACNotifyGateSrvAddr = (ACNotifyGateSrvAddr*)(pData);

	//EnCode IP
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkACNotifyGateSrvAddr->IP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Port
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkACNotifyGateSrvAddr->Port), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	Assign_namespace ::AssignProtocol::DeCode__ACNotifyGateSrvAddr(void* pData)
{
	ACNotifyGateSrvAddr* pkACNotifyGateSrvAddr = (ACNotifyGateSrvAddr*)(pData);

	//DeCode IP
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkACNotifyGateSrvAddr->IP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Port
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkACNotifyGateSrvAddr->Port), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

