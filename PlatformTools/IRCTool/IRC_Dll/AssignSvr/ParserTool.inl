inline char* Assign_namespace ::ParserTool::GetBufData()
{
	return m_pcBuf;
}

inline size_t Assign_namespace ::ParserTool::GetBufDataLen()
{
	return m_unOffset;
}

inline void	Assign_namespace ::ParserTool::SetBufDataLen(size_t unOffset)
{
	m_unOffset = unOffset;
}

inline size_t Assign_namespace ::ParserTool::GetBufSize()
{
	return m_unSize;
}

inline size_t Assign_namespace ::ParserTool::GetOutSize()
{
	return m_unOutSize;
}

