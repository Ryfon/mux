﻿#include "..\stdafx.h"
#include "ParserTool.h"
#include "cassert"
#ifdef	WIN32
#include <Winsock2.h>
#else
#include <memory.h>
#include <sys/socket.h>
#include <netinet/in.h>
#endif

namespace Assign_namespace 
{
	template <class T> bool PackData(T* t, size_t unCount, ParserTool* pkPackage)
	{
		size_t	nOffset   = pkPackage->GetBufDataLen();
		size_t	nDataSize = sizeof(T) * unCount;

		if((nOffset + nDataSize) > pkPackage->GetBufSize())
		{
			return false;
		}

		memcpy(pkPackage->GetBufData() + nOffset, t, nDataSize);
		pkPackage->SetBufDataLen(nOffset + nDataSize);
		return true;
	}

	template <class T> bool UnpackData(T* t, size_t unCount, ParserTool* pkPackage)
	{
		size_t nOffset   = pkPackage->GetBufDataLen();
		size_t nDataSize = sizeof(T) * unCount;

		if((nOffset + nDataSize) > pkPackage->GetOutSize())
		{
			return false;
		}

		if((nOffset + nDataSize) > pkPackage->GetBufSize())
		{
			return false;
		}

		memcpy(t, pkPackage->GetBufData() + nOffset,  nDataSize);
		pkPackage->SetBufDataLen(nOffset + nDataSize);
		return true;
	}
}

Assign_namespace ::ParserTool::ParserTool(void)
{
	m_pcBuf                =   NULL;
	m_unSize               =   0;
	m_unOffset             =   0;
	m_unOutSize            =   0;

	m_pfnPack[eBool]       =   &ParserTool::PackBool;
	m_pfnPack[eCHAR]       =   &ParserTool::PackCHAR;
	m_pfnPack[eBYTE]       =   &ParserTool::PackBYTE;
	m_pfnPack[eSHORT]      =   &ParserTool::PackSHORT;
	m_pfnPack[eUSHORT]     =   &ParserTool::PackUSHORT;
	m_pfnPack[eINT]        =   &ParserTool::PackINT;
	m_pfnPack[eUINT]       =   &ParserTool::PackUINT;
	m_pfnPack[eFLOAT]      =   &ParserTool::PackFLOAT;
	m_pfnPack[eDOUBLE]     =   &ParserTool::PackDOUBLE;

	m_pfnUnPack[eBool]     =   &ParserTool::UnPackBool;
	m_pfnUnPack[eCHAR]     =   &ParserTool::UnPackCHAR;
	m_pfnUnPack[eBYTE]     =   &ParserTool::UnPackBYTE;
	m_pfnUnPack[eSHORT]    =   &ParserTool::UnPackSHORT;
	m_pfnUnPack[eUSHORT]   =   &ParserTool::UnPackUSHORT;
	m_pfnUnPack[eINT]      =   &ParserTool::UnPackINT;
	m_pfnUnPack[eUINT]     =   &ParserTool::UnPackUINT;
	m_pfnUnPack[eFLOAT]    =   &ParserTool::UnPackFLOAT;
	m_pfnUnPack[eDOUBLE]   =   &ParserTool::UnPackDOUBLE;
}

Assign_namespace ::ParserTool::~ParserTool(void)
{
}

void Assign_namespace ::ParserTool::Init(char* pcBuf, size_t unSize, size_t unOutSize)
{
	m_pcBuf     =   pcBuf;
	m_unSize    =   unSize;
	m_unOffset  =   0;
	m_unOutSize =   unOutSize;
}

void Assign_namespace ::ParserTool::Reset()
{
	m_unOffset	=	0;
}

bool Assign_namespace ::ParserTool::Pack(const char* pcTypeName, void* pData, size_t unCount)
{
	BASETYPE eBaseType = GetBaseType(pcTypeName);
	assert((eBaseType >= eBool) && (eBaseType <= eDOUBLE));
	return	(this->*(m_pfnPack[eBaseType]))(pData, unCount);
}

bool Assign_namespace ::ParserTool::UnPack(const char* pcTypeName, void* pData, size_t unCount)
{
	BASETYPE eBaseType = GetBaseType(pcTypeName);
	assert((eBaseType >= eBool) && (eBaseType <= eDOUBLE));
	return	(this->*(m_pfnUnPack[eBaseType]))(pData, unCount);
}

Assign_namespace ::BASETYPE Assign_namespace ::ParserTool::GetBaseType(const char* pcTypeName)
{
	if(!strcmp("bool", pcTypeName))
	{
		return eBool;
	}
	else if(!strcmp("CHAR" , pcTypeName))
	{
		return eCHAR;
	}
	else if(!strcmp("BYTE", pcTypeName))
	{
		return eBYTE;
	}
	else if(!strcmp("SHORT", pcTypeName))
	{
		return eSHORT;
	}
	else if(!strcmp("USHORT", pcTypeName))
	{
		return eUSHORT;
	}
	else if(!strcmp("INT", pcTypeName))
	{
		return eINT;
	}
	else if(!strcmp("UINT", pcTypeName))
	{
		return eUINT;
	}
	else if(!strcmp("FLOAT", pcTypeName))
	{
		return eFLOAT;
	}
	else if(!strcmp("DOUBLE", pcTypeName))
	{
		return eDOUBLE;
	}
	else
	{
		return eMaxType;
	}
}

bool Assign_namespace ::ParserTool::CheckIntegeType(const char* pcTypeName)
{
	if(pcTypeName == NULL)
	{
		return false;
	}

	BASETYPE eBaseType = GetBaseType(pcTypeName);
	if((eBaseType >= eCHAR) && (eBaseType <= eUINT))
	{
		return true;
	}

	return false;
}

bool Assign_namespace ::ParserTool::CheckBaseType(const char* pcTypeName)
{
	if(pcTypeName == NULL)
	{
		return false;
	}

	BASETYPE eBaseType = GetBaseType(pcTypeName);
	if((eBaseType >= eBool) && (eBaseType <= eDOUBLE))
	{
		return true;
	}

	return false;
}

bool Assign_namespace ::ParserTool::PackBool(void* pData, size_t unCount)
{
	return PackData((bool*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::UnPackBool(void* pData, size_t unCount)
{
	return UnpackData((bool*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::PackCHAR(void* pData, size_t unCount)
{
	return PackData((CHAR*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::UnPackCHAR(void* pData, size_t unCount)
{
	return UnpackData((CHAR*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::PackBYTE(void* pData, size_t unCount)
{
	return PackData((BYTE*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::UnPackBYTE(void* pData, size_t unCount)
{
	return UnpackData((BYTE*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::PackSHORT(void* pData, size_t unCount)
{
	SHORT	nSrcShort, nDesShort;
	for(size_t i = 0; i < unCount; ++i)
	{
		nSrcShort = *(SHORT*)((char*)pData + sizeof(SHORT) * i);
		nDesShort =	htons(nSrcShort);
		if(!PackData(&nDesShort, 1, this))
		{
			return false;
		}
	}

	return true;
}

bool Assign_namespace ::ParserTool::UnPackSHORT(void* pData, size_t unCount)
{
	if(!UnpackData((SHORT*)pData, unCount, this))
	{
		return false;
	}

	SHORT	nDesShort, nSrcShort;
	size_t	nOffset	=	0;
	for(size_t i = 0; i < unCount; ++i)
	{
		nDesShort	=	*(SHORT*)((char*)pData + nOffset);
		nSrcShort	=	ntohs(nDesShort);

		memcpy((char*)pData + nOffset, &nSrcShort, sizeof(SHORT));
		nOffset	+=	sizeof(SHORT);
	}

	return true;
}

bool Assign_namespace ::ParserTool::PackUSHORT(void* pData, size_t unCount)
{
	USHORT	nSrcShort, nDesShort;
	for(size_t i = 0; i < unCount; ++i)
	{
		nSrcShort = *(USHORT*)((char*)pData + sizeof(USHORT) * i);
		nDesShort =	htons(nSrcShort);
		if(!PackData(&nDesShort, 1, this))
		{
			return false;
		}
	}

	return true;
}

bool Assign_namespace ::ParserTool::UnPackUSHORT(void* pData, size_t unCount)
{
	if(!UnpackData((USHORT*)pData, unCount, this))
	{
		return false;
	}

	SHORT	nDesShort, nSrcShort;
	size_t	nOffset	=	0;
	for(size_t i = 0; i < unCount; ++i)
	{
		nDesShort	=	*(USHORT*)((char*)pData + nOffset);
		nSrcShort	=	ntohs(nDesShort);

		memcpy((char*)pData + nOffset, &nSrcShort, sizeof(USHORT));
		nOffset	+=	sizeof(USHORT);
	}

	return true;
}

bool Assign_namespace ::ParserTool::PackINT(void* pData, size_t unCount)
{
	INT	nSrcInt, nDesInt;
	for(size_t i = 0; i < unCount; ++i)
	{
		nSrcInt =	*(INT*)((char*)pData + sizeof(INT) * i);
		nDesInt =	htonl(nSrcInt);
		if(!PackData(&nDesInt, 1, this))
		{
			return false;
		}
	}

	return true;
}

bool Assign_namespace ::ParserTool::UnPackINT(void* pData, size_t unCount)
{
	if(!UnpackData((INT*)pData, unCount, this))
	{
		return false;
	}

	INT	nDesInt, nSrcInt;
	size_t	nOffset	=	0;
	for(size_t i = 0; i < unCount; ++i)
	{
		nDesInt		=	*(INT*)((char*)pData + nOffset);
		nSrcInt		=	ntohl(nDesInt);

		memcpy((char*)pData + nOffset, &nSrcInt, sizeof(INT));
		nOffset	+=	sizeof(INT);
	}

	return true;
}

bool Assign_namespace ::ParserTool::PackUINT(void* pData, size_t unCount)
{
	UINT	nSrcInt, nDesInt;
	for(size_t i = 0; i < unCount; ++i)
	{
		nSrcInt =	*(UINT*)((char*)pData + sizeof(UINT) * i);
		nDesInt =	htonl(nSrcInt);
		if(!PackData(&nDesInt, 1, this))
		{
			return false;
		}
	}

	return true;
}

bool Assign_namespace ::ParserTool::UnPackUINT(void* pData, size_t unCount)
{
	if(!UnpackData((UINT*)pData, unCount, this))
	{
		return false;
	}

	UINT	nDesInt, nSrcInt;
	size_t	nOffset	=	0;
	for(size_t i = 0; i < unCount; ++i)
	{
		nDesInt		=	*(UINT*)((char*)pData + nOffset);
		nSrcInt		=	ntohl(nDesInt);

		memcpy((char*)pData + nOffset, &nSrcInt, sizeof(UINT));
		nOffset	+=	sizeof(UINT);
	}

	return true;
}

bool Assign_namespace ::ParserTool::PackFLOAT(void* pData, size_t unCount)
{
	return PackData((FLOAT*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::UnPackFLOAT(void* pData, size_t unCount)
{
	return UnpackData((FLOAT*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::PackDOUBLE(void* pData, size_t unCount)
{
	return PackData((DOUBLE*)pData, unCount, this);
}

bool Assign_namespace ::ParserTool::UnPackDOUBLE(void* pData, size_t unCount)
{
	return UnpackData((DOUBLE*)pData, unCount, this);
}

