﻿#ifndef	__PARSERTOOL__H___
#define	__PARSERTOOL__H___
#include <stdio.h>

namespace Assign_namespace 
{
	typedef char             CHAR;
	typedef unsigned char    BYTE;
	typedef short            SHORT;
	typedef unsigned short   USHORT;
	typedef int              INT;
	typedef unsigned int     UINT;
	typedef float            FLOAT;
	typedef double           DOUBLE;

	typedef enum
	{
		eBool,
		eCHAR,
		eBYTE,
		eSHORT,
		eUSHORT,
		eINT,
		eUINT,
		eFLOAT,
		eDOUBLE,
		eMaxType
	}BASETYPE;

	class	ParserTool;
	typedef	bool (ParserTool::*PackFunc)(void* pData, size_t unCount);
	typedef	bool (ParserTool::*UnPackFunc)(void* pData, size_t unCount);
	class 
	#ifdef	WIN32
	_declspec(dllexport) 
	#endif
	ParserTool
	{
	public:
		ParserTool(void);
		~ParserTool(void);

		char*           GetBufData();
		size_t          GetBufDataLen();
		void            SetBufDataLen(size_t unOffset);
		size_t          GetBufSize();
		size_t          GetOutSize();

	public:
		void            Init(char* pcBuf, size_t unSize, size_t unOutSize = 0);
		void            Reset();
		bool            Pack(const char* pcTypeName, void* pData, size_t unCount);
		bool            UnPack(const char* pcTypeName, void* pData, size_t unCount);
		static BASETYPE GetBaseType(const char* pcTypeName);
		static bool     CheckIntegeType(const char* pcTypeName);
		static bool     CheckBaseType(const char* pcTypeName);

	protected:
		bool            PackBool(void* pData, size_t unCount);
		bool            UnPackBool(void* pData, size_t unCount);
		bool            PackCHAR(void* pData, size_t unCount);
		bool            UnPackCHAR(void* pData, size_t unCount);
		bool            PackBYTE(void* pData, size_t unCount);
		bool            UnPackBYTE(void* pData, size_t unCount);

		bool            PackSHORT(void* pData, size_t unCount);
		bool            UnPackSHORT(void* pData, size_t unCount);
		bool            PackUSHORT(void* pData, size_t unCount);
		bool            UnPackUSHORT(void* pData, size_t unCount);

		bool            PackINT(void* pData, size_t unCount);
		bool            UnPackINT(void* pData, size_t unCount);
		bool            PackUINT(void* pData, size_t unCount);
		bool            UnPackUINT(void* pData, size_t unCount);

		bool            PackFLOAT(void* pData, size_t unCount);
		bool            UnPackFLOAT(void* pData, size_t unCount);
		bool            PackDOUBLE(void* pData, size_t unCount);
		bool            UnPackDOUBLE(void* pData, size_t unCount);

	protected:
		char*           m_pcBuf;
		size_t          m_unSize;
		size_t          m_unOffset;
		size_t          m_unOutSize;
		PackFunc        m_pfnPack[eMaxType];
		UnPackFunc      m_pfnUnPack[eMaxType];
	};

#include "ParserTool.inl"
}
#endif

