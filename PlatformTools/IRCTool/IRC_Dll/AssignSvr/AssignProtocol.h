﻿#ifndef			__AssignProtocol__
#define			__AssignProtocol__
#include "ParserTool.h"
#include "map"

namespace Assign_namespace 
{
	//Enums
	typedef	enum
	{
		G_A_SRV_MSG_ID             	=	0x1200,        	//
		C_A_QUERY_SRV_STATE_ID     	=	0x0201,        	//
		A_C_NOTIFYSRVSTATEID       	=	0x2001,        	//
		C_A_REQUEST_GATESRV_ADDR_ID	=	0x0202,        	//
		A_C_NOTIFYGATESRVID        	=	0x2002,        	//
	}MessageId;

	typedef	enum
	{
		IDLESSE                    	=	0,             	//游戏服务器组 空闲
		NATURAL                    	=	1,             	//游戏服务器组 正常
		FULL                       	=	2,             	//游戏服务器组 忙
	}GameServerState;


	//Defines

	//Typedefs

	//Types
	template<bool> struct TypeMaxBytes;
	template<> struct TypeMaxBytes<true> {};
	#define	TYPE_MAXBYTES_CHECK(typename) \
	struct type##typename \
	{\
		TypeMaxBytes< sizeof(typename) < 300 > struct_larger_##typename;\
	}

	struct GAGateSrvMsg
	{
		static const USHORT	wCmd = G_A_SRV_MSG_ID;

		UINT  	IP;   	//IP地址
		USHORT	Port; 	//
		USHORT	Num;  	//
	};
	TYPE_MAXBYTES_CHECK(GAGateSrvMsg);

	struct CAQuerySrvState
	{
		static const USHORT	wCmd = C_A_QUERY_SRV_STATE_ID;
	};
	TYPE_MAXBYTES_CHECK(CAQuerySrvState);

	struct ACNotifyASrvState
	{
		static const USHORT	wCmd = A_C_NOTIFYSRVSTATEID;

		BYTE  	State;	//服务器当前状态 参见 GameServerState 
	};
	TYPE_MAXBYTES_CHECK(ACNotifyASrvState);

	struct CARequestGateSrvAddr
	{
		static const USHORT	wCmd = C_A_REQUEST_GATESRV_ADDR_ID;
	};
	TYPE_MAXBYTES_CHECK(CARequestGateSrvAddr);

	struct ACNotifyGateSrvAddr
	{
		static const USHORT	wCmd = A_C_NOTIFYGATESRVID;

		UINT  	IP;   	//IP地址
		USHORT	Port; 	//
	};
	TYPE_MAXBYTES_CHECK(ACNotifyGateSrvAddr);


	//Messages
	typedef enum Message_id_type
	{
		G_A_SrvMsg        	=	G_A_SRV_MSG_ID,             	//GAGateSrvMsg
		C_A_QuerySrv      	=	C_A_QUERY_SRV_STATE_ID,     	//CAQuerySrvState
		A_C_NotifySrvState	=	A_C_NOTIFYSRVSTATEID,       	//ACNotifyASrvState
		C_A_RequestGateSrv	=	C_A_REQUEST_GATESRV_ADDR_ID,	//CARequestGateSrvAddr
		A_C_NotifyGateSrv 	=	A_C_NOTIFYGATESRVID,        	//ACNotifyGateSrvAddr
	};

	//Class Data
	class AssignProtocol;
	typedef size_t (AssignProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (AssignProtocol::*DeCodeFunc)(void* pData);

	class 
	AssignProtocol
	{
	public:
		AssignProtocol();
		~AssignProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__GAGateSrvMsg(void* pData);
		size_t	DeCode__GAGateSrvMsg(void* pData);

		size_t	EnCode__CAQuerySrvState(void* pData);
		size_t	DeCode__CAQuerySrvState(void* pData);

		size_t	EnCode__ACNotifyASrvState(void* pData);
		size_t	DeCode__ACNotifyASrvState(void* pData);

		size_t	EnCode__CARequestGateSrvAddr(void* pData);
		size_t	DeCode__CARequestGateSrvAddr(void* pData);

		size_t	EnCode__ACNotifyGateSrvAddr(void* pData);
		size_t	DeCode__ACNotifyGateSrvAddr(void* pData);

	protected:
		std::map<int, EnCodeFunc>		m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>		m_mapDeCodeFunc;
		ParserTool		                m_kPackage;
	};
}
#endif			//__AssignProtocol__
