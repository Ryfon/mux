﻿/** @file SystemInfo.h 
@brief 系统信息封装
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-01-16
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H

class CSystemInfo: public SYSTEM_INFO
{
public:
	CSystemInfo(void);
public:
	virtual ~CSystemInfo(void);
};

#endif