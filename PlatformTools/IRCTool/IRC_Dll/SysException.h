﻿/** @file SysException.h 
@brief 封装系统错误异常类
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-01-17
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef SYSEXCEPTION_H
#define SYSEXCEPTION_H

#include "AppException.h"

class CSysException: public CAppException
{
public:
	CSysException(const string& strWhere, DWORD dwErrorCode);
public:
	virtual ~CSysException(void);
private:
	//根据错误码返回系统定义的错误信息
	string GetErrorMessage(DWORD dwErrorCode);
};

#endif