﻿#include "stdafx.h"
#include "SkillManager.h"

CSkillManager::CSkillManager(void)
{
	
}

CSkillManager::~CSkillManager(void)
{
	for (map<unsigned long, PPLAYERSKILL>::iterator Ite = m_SkillInfos.begin(); Ite != m_SkillInfos.end(); Ite++)
	{
		PPLAYERSKILL pPlayerSkill = Ite->second;
		delete pPlayerSkill;
	}
	m_SkillInfos.clear();
}

int CSkillManager::LoadSkillInfo(const char* szXmlFile)
{
	TiXmlDocument XmlDoc;
	XmlDoc.Parse(szXmlFile);

	TiXmlElement* pRootElement = XmlDoc.RootElement();

	for (const TiXmlElement* pXmlSkill = pRootElement->FirstChildElement(); pXmlSkill; pXmlSkill = pXmlSkill->NextSiblingElement() )
	{
		PPLAYERSKILL pPlayerSkill = new PLAYERSKILL;
		pXmlSkill->Attribute("nID", (int*)&(pPlayerSkill->nID));
		pXmlSkill->Attribute("nType", &(pPlayerSkill->nType));
		pPlayerSkill->strName = pXmlSkill->Attribute("nName");
		pXmlSkill->Attribute("nLevel", &(pPlayerSkill->nLevel));
		pXmlSkill->Attribute("SpLevel", &(pPlayerSkill->nSpLevel));
		pXmlSkill->Attribute("nAttDistMin", &(pPlayerSkill->nAttDistMin));
		pXmlSkill->Attribute("nAttDistMax", &(pPlayerSkill->nAttDistMax));
		pXmlSkill->Attribute("nCoolDown", &(pPlayerSkill->nCDTime));
		pXmlSkill->Attribute("nCurseTime", &(pPlayerSkill->nCurseTime));
		pXmlSkill->Attribute("nRange", &(pPlayerSkill->nRange));
		pPlayerSkill->strNote = pXmlSkill->Attribute("nSkillNote");

		pXmlSkill->Attribute("nUllageHp", &(pPlayerSkill->nUllageHp));		
		pXmlSkill->Attribute("nUllageMp", &(pPlayerSkill->nUllageMp));		
		pXmlSkill->Attribute("nUllageItem", &(pPlayerSkill->nUllageItem));
		pXmlSkill->Attribute("nUllageItemNum", &(pPlayerSkill->nUllageItemNum));
		pXmlSkill->Attribute("nUllageMoney", &(pPlayerSkill->nUllageMoney));
		pXmlSkill->Attribute("nUllageSp", &(pPlayerSkill->nUllageSp));

		m_SkillInfos[pPlayerSkill->nID] = pPlayerSkill;
	}
	return 0;
}

PPLAYERSKILL CSkillManager::GetSkillInfo(unsigned long ulSkillID)
{
	map<unsigned long, PPLAYERSKILL>::iterator Ite = m_SkillInfos.find(ulSkillID);
	if (Ite != m_SkillInfos.end())
	{
		return Ite->second;
	}
	else
		return NULL;
}