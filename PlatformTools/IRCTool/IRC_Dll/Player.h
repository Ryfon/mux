﻿/** @file Player.h 
@brief 封装玩家信息
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-01-23
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef PLAYER_H
#define PLAYER_H

#include "CliProtocol_T.h"
#include "..\include\IRCIntf.h"
#include "SkillManager.h"
#include "ItemManager.h"
#include "ClientManager.h"
#include "TaskManager.h"
#include "CliNet.h"
#include "PacketBuild.h"
#include "../ChatRecorder/ChatRecordMng.h"
#include "MemoryModule.h"

#define DealPlayerPkgPre( OUTTYPE, outPtr, inputPkg, inputLen, ProtolTool ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	memset( outPtr, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)ProtolTool.DeCode( OUTTYPE::wCmd, &tmpStruct, outBufLen, (char*)inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
	{\
		return false;\
	}







#define MAX_MSG_SIZE 512

using namespace MUX_PROTO;

inline bool operator < ( const PlayerID& first,const PlayerID& otherPlayerID )
{
	if ( first.wGID < otherPlayerID.wGID )
		return true;
	else if ( first.wGID > otherPlayerID.wGID )
		return false;
	else 
		return first.dwPID < otherPlayerID.dwPID? true: false;
}

inline bool operator == ( const PlayerID& firstPlayerID,const PlayerID &secPlayerID )
{
	return ( ( firstPlayerID.wGID == secPlayerID.wGID ) && ( firstPlayerID.dwPID == secPlayerID.dwPID ) );
}

inline bool operator != ( const PlayerID& firstPlayerID,const PlayerID & otherPlayerID )
{
	return ( ( firstPlayerID.wGID != otherPlayerID.wGID ) || ( firstPlayerID.dwPID != otherPlayerID.dwPID ) );
}


//提供PlayerID的比较Function
struct PlayerIDComp
{
	bool operator()(const PlayerID& Src, const PlayerID& Dest) const
	{
		return Src.wGID > Dest.wGID || (Src.wGID == Dest.wGID && Src.dwPID > Dest.dwPID);
	}
};

//角色定义
typedef struct _tagRole 
{
	PlayerInfoLogin playerInfo;
} ROLE, *PROLE;

class CPlayer;

#pragma pack(push, 1)
typedef struct tag_msgHeader
{
	unsigned short m_DataLen;
	unsigned char m_DataType;
	unsigned short m_Cmd;

} MSGHEADER, *PMSGHEADER;
//连接成功消息
struct CONNMSG 
{
	unsigned char ConnType;
};

struct REQCONNMSG
{
	unsigned char ConnType;
	char IpAddr[32];
	unsigned short Port;
};

//
#define LOCAL_TYPE 0xFF

//发起连接
#define REQ_CONN 0xFFFF
//连接成功
#define CONN_ID 0xFFFE
//关闭连接
#define REQ_CONN_CLOSE 0xFFFD
//连接关闭
#define CONN_CLOSE 0xFFFC
//私聊记录
#define PRI_MSG 0xFFFB
//世界频道记录
#define WORLD_MSG 0xFFFA
//清空记录命令
#define CLEAR_LOG 0xFFF9
//工会频道记录
#define GUILD_MSG 0xFFF8

#pragma pack(pop)

class CMemNode;
///玩家当前所处的状态(阶段)
enum PLAYER_STAT
{
	PS_INVALID = 0, //初始(无效状态)
	PS_QUERYASSIGNSVR, //查询AssignServer状态
	PS_QUERYGATESVR, //查询GateServer端口
	PS_OPENCONN, 
	PS_LOGINING, //登录中
	PS_LOGINED, //已登录
	PS_ROLESELING, //选角色中
	PS_ROLESELED, //角色已选
	PS_LOGINGGAME, //登录游戏中
	PS_LOGGAME//已登录游戏
};

//玩家的接收消息队列，存贮利用IOCP机制从网络上收到的消息
typedef list<CMemNode*> IN_BOX;

class CClientSession;

//消息处理结构
struct MSGHANDLER
{
	MSGHANDLER(): m_Status(PS_INVALID), handler(NULL){}
	MSGHANDLER(int status, int (CPlayer::*_handler)(void*, unsigned short)): m_Status(status), handler(_handler){}
	int m_Status;
	int (CPlayer::*handler)(void*, unsigned short);
};

typedef map<TYPE_ID, string> FRIENDGROUPMAP;
typedef map<TYPE_ID, FriendInfo> FRIENDINFOMAP;
typedef map<TYPE_ID, FoeInfo> FOEINFOMAP;
typedef map<TYPE_ID, BlackListItemInfo> BLACKITEMMAP;

typedef map<unsigned short, MSGHANDLER> HANDLERMAP;

typedef struct _tagReqRespInfo 
{
	_tagReqRespInfo(): m_ReqID(0), m_RespID(0), m_ReqTime(0){}
	_tagReqRespInfo(unsigned short usReq, unsigned short usResp, DWORD dwTime): m_ReqID(usReq), m_RespID(usResp), m_ReqTime(dwTime){}
	unsigned short m_ReqID;
	unsigned short m_RespID;
	int m_TimeOut;
	DWORD m_ReqTime;
}REQRESPINFO, *PREQRESPINFO;

class CPlayer: public CIRCIntf, MUX_CLIMOD::IMuxCliNetSinker, IChatRecEvent  
{
public:
	CPlayer();
public:
	virtual ~CPlayer(void);

public:
	int virtual OnChatRecord(CChatRecordMng::ChatLog* pChatLog);
public: //接口实现
	//设置服务器信息
	virtual int SetServerInfo(const char* szAssignSvrIp, unsigned short usPort, const char* szGateSvrIp, unsigned short usGateSvrPort);
	//设置玩家信息
	virtual int SetPlayerInfo(const char* szAccount, const char* szPwd, const char* szRoleName);
	//读取技能信息
	virtual int ReadSkillInfo(const char* szSkillFileName);
	//读取任务信息
	virtual int ReadTaskInfo(const char* szTaskFileName);
	//读取道具信息
	virtual int ReadItemInfo(const char* szItemFileName);
	virtual int ReadSuitInfo(const char* szSuitFileName);
	virtual int ReadAdditionAttr(const char* szFileName);
	virtual int ReadItemLevelUpInfo(const char* szFileName);
	virtual void ReqUserInfo(const char* szRoleName);

	virtual void ClearLog(int nType);

	virtual void SetFilter(unsigned short usFilter);

	virtual void ChangeChatLogDir(const char* szDir);

	virtual void GetSkillInfo(unsigned long ulSkillID, vector<string>& vectSkillProp);

	virtual int GetItemInfo(unsigned long ulItemID, vector<string>& vectItemInfo);

	virtual int GetSuitInfo(unsigned long ulSuitID, int& nSuitCount, vector<string>& vectSuitName, vector<string>& vectSuitAttr);
	
	virtual int GetItemAdditionInfo(unsigned long ulItemAddID, vector<string>& vectItemAdditionAttr);

	virtual int GetItemLevelUpInfo(unsigned long ulLevelUpID, vector<string>& vectLevelUpInfo);

	virtual int GetTaskInfo(unsigned long ulTaskID, vector<string>& vectTaskInfo);

	virtual string GetClassByClassID(unsigned long ulClassID);

	virtual string GetRaceByRaceID(unsigned long ulRaceID);

	virtual string GetGender(unsigned long ulGenderID);

	virtual unsigned short GetCurRoleNo();
	virtual int QryUnionMember();
	//启动
	virtual int StartUp(CIRCEventSink& ircEvent, char* pDllData);
	//停止
	virtual int CleanUp();

	virtual int Open();
	virtual int Close();
	virtual int EnterWorld(unsigned int uiRoleID);
	virtual int SendPrivateMsg(const char* szDest, const char* szChat);
	virtual int SendMsgToWorld(const char* szChat);
	virtual int SendMsgToGuild(const char* szChat);

	virtual int ReadPrivateMsg();
	virtual int ReadWorldMsg();
	virtual int ReadGuildMsg();
	virtual int ReadCommonMsg();
	virtual int ReadSystermMsg();
	virtual int ReadChatGroupMsg();
	virtual int ReadGroupMsg(); 

public:
	virtual int OnLoginRes( int errNo );
	virtual int OnTestAccountsRes( int errNo );
	virtual int OnCreateAccountsRes( int errNo );
	virtual int OnPkgRcved( unsigned short wCmd, const char* pPkg, const DWORD dwLen );
	virtual int OnError( const int& errNo, const DWORD dwPkgID );
public:
	//网络数据到来
	int OnConnectionOpen();
	int OnConnectionClose();
	int OnDataRecv(CMemNode* pMemNode);
	int LeaveWorld();
	//处理网络消息
	int HandleNetMsg();
	//增加网络消息到缓冲队列
	int AddNetMsg(CMemNode* pMemNode);
public:
	int QueryAssignSrvState();
	int QueryGateSrvAddr();
private:
	int RegisterHandler();
public:
	//打开连接
	int OpenConnection();
	//心跳
	int HeartBeat(unsigned long ulPkgID = 0);
	//获取加密Key
	int ReqClientKey();
	//验证密码
	int ReqCheckPwd();
	//登录
	int Login();
	//选择角色
	int SelectRole(unsigned int uiRoleNO);
	//登录游戏
	int StartGame();
	//
	int ChatWith(CHAT_TYPE ChatType, const char* szDest, const char* strChat);
public:
	//处理接收的数据信息
	int ProcRecvData(CMemNode* pMemNode);
	//获得帐号、密码
	void GetAccountAndPwd(string& strAccount, string& strPwd);
	const char* GetAccount();
	void SetCliSession(CClientSession* pClientSession);
private:
	int SaveChat(CHAT_TYPE ct, ChatLog::CHATDIRECT cd, const char* szChatObj, const char* szChat);
	int LoadChat(char* pData, unsigned short usDataLen, ChatLog& cl);
	bool IsBlackItem(const char* szRoleName);
	void CheckTimeOut();
	void SetTimeOut(USHORT usReqID, USHORT usRespID, DWORD dwTimeOut);
	void CalcTimeOut(unsigned short usCmdID);

	BOOL GetCreateCliNetModFun(char* pDllData);
private:
	int NetBias(void* pRawData, unsigned short usDataLen);
	int ProcNotifySvrState(void* pRawData, unsigned short usDataLen);
	int ProcNotifyGateSrvAddr(void* pRawData, unsigned short usDataLen);

	int ProcRandomKey(void* pRawData, unsigned short usDataLen);

	//登录的回应
	int LoginResp(void* pRawData, unsigned short usDataLen);
	//玩家标示信息
	int UpdatePlayerInfo(void* pRawData, unsigned short usDataLen);
	//更新玩家角色信息
	int UpdateRoleInfo(void* pRawData, unsigned short usDataLen);
	//更新玩家所选角色
	int UpdateSelRole(void* pRawData, unsigned short usDataLen);
	//玩家登录游戏世界
	int LoginGame(void* pRawData, unsigned short usDataLen);
	//处理聊天消息
	int PlayerChat(void* pRawData, unsigned short usDataLen);
	//处理私聊时业务错误消息
	int ProcPrivateChatErro(void* pRawData, unsigned short usDataLen);
	//心跳协议
	int PlayerHeartBeat(void* pRawData, unsigned short usDataLen);
	//对时消息
	int NetDetect(void* pRawData, unsigned short usDataLen);
	//处理连接成功消息
	int  ProcConn(void* pRawData, unsigned short usDataLen);
	//处理连接请求
	int ProcConnReq(void* pRawData, unsigned short usDataLen);
	//处理关闭连接请求
	int ProcConnCloseReq(void* pRawData, unsigned short usDataLen);
	//处理连接关闭消息
	int ProcConnClose(void* pRawData, unsigned short usDataLen);
	//处理服务器端错误
	int ProcServerError(void * pRawData, USHORT usDataLen);

	//处理玩家好友分组信息
	int ProcFriendGroups(void* pRawData, unsigned short usDataLen);
	//处理好友信息
	int ProcFriends(void* pRawData, unsigned short usDataLen);
	//处理好友信息更新
	int ProcFriendInfoUpdate(void* pRawData, unsigned short usDataLen);
	//处理仇人列表
	int ProcFoeList(void* pRawData, unsigned short usDataLen);
	//处理仇人
	int ProcNewFoe(void* pRawData, unsigned short usDataLen);
	//处理仇人信息更新
	int ProFoeInfoUpdate(void* pRawData, unsigned short usDataLen);
	//黑名单列表
	int ProcBlackList(void* pRawData, unsigned short usDataLen);
/*
	int ProcPrivateMsgRec(void* pRawData, unsigned short usDataLen);

	int ProcWorldMsgRec(void* pRawData, unsigned short usDataLen);

	int ProcGuildMsgRec(void* pRawData, unsigned short usDataLen);
*/
	int ProcClearLog(void* pRawData, unsigned short usDataLen);

	int ProcIrcQryUserInfo(void* pRawData, unsigned short usDataLen);

	int ProcUnionInfo(void* pRawData, unsigned short usDataLen);

	int ProcDelUnionMember(void*, unsigned short);

	int ProcUnionDestroy(void*, unsigned short);

	int ProcForbidUnionSpeek(void*, unsigned short);

	int ProcUnionMembers(void*, unsigned short);

	int ProcAddUnionMember(void*, unsigned short);

	int ProcUnionMemOnOffline(void*, unsigned short);

	int ProcUnionMemTitleNty(void*, unsigned short);

	int ProcUnionPosUdpNty(void*, unsigned short);

	int ProcUnionMemAdvPos(void*, unsigned short);

	int ProcUnionMemRedPos(void*, unsigned short);

private:
	string m_AssignSrvIp;
	int m_AssignSrvPort;
	string m_GateSrvIp;
	int m_GatePort;
	int m_TrySendTimes;
	DWORD m_dwLastCheckTime;
	DWORD m_dwLastCheckID;
	unsigned short m_Filter;
	typedef map<unsigned short, PREQRESPINFO> REQRESPINFOMAP;
	REQRESPINFOMAP m_ReqRespInfos;
	CChatRecordMng m_RecordMng;
private:
	HANDLE m_hEvent;
	HANDLE m_hFrameUpdate;
	static unsigned __stdcall FrameUpdate(void* pThreadParams);
private:
	CIRCEventSink* m_IRCEvent;
	string m_Account; //帐号
	string m_Pwd; //密码
	string m_CliKey;
	PlayerID m_UserID; //玩家唯一标示
	PLAYER_STAT m_UserStage; //玩家阶段
	CClientManager m_CliManager;
	map<unsigned short, PROLE> m_RoleInfo; //玩家可用角色
	string m_RoleName;
	unsigned short m_RoleNO; //玩家当前角色序号
	FRIENDGROUPMAP m_FriendGroupMap; //好友分组信息
	FRIENDINFOMAP m_FriendInfoMap; //好友信息
	FOEINFOMAP m_FoeInfoMap; 
	BLACKITEMMAP m_BlackItemMap;
private:
	unsigned long m_NetBias;
	vector<u_long> m_vecDetectPkgRcvTime;
	bool m_IsNetBiasAquired;
	CClientSession* m_CliSession;
private:
	IN_BOX m_ProNodes;
	IN_BOX m_NetNodes;

	IN_BOX* m_ReadPtr;
	IN_BOX* m_WritePtr;
	CRITICAL_SECTION m_CriticalSection;
	HANDLERMAP m_Handler;
private:
	CliProtocol m_ProtolTool;
	CSkillManager m_SkillManager;
	CItemManager m_ItemManager;
	CTaskManager m_TaskManager;
private:
	///取模块函数指针；
	MUX_CLIMOD::PFN_GetMuxCliNetMod m_pfnGetMuxCliNetMod;
	///网络模块；
	MUX_CLIMOD::IMuxCliNetMod* m_pNetMod;
	///通信库句柄；
	HMEMORYMODULE m_hMemoryModule;
private:
	CPlayer(const CPlayer&);
	CPlayer& operator=(const CPlayer&);
};

#endif