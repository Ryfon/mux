﻿#include "stdafx.h"
#include "SysException.h"

CSysException::CSysException(const string& strWhere, DWORD dwErrorCode)
: CAppException(strWhere, GetErrorMessage(dwErrorCode))
{

}

CSysException::~CSysException(void)
{

}

string CSysException::GetErrorMessage(DWORD dwErrorCode)
{
	const int ERRMSGBUFFERSIZE = 256;
	string strMsg;
	HLOCAL pBuf;
	DWORD dwRet;
	//从系统消息表中搜索消息
	dwRet = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, 0, dwErrorCode, 
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&pBuf, 0, NULL);
	if (dwRet == 0)
	{
		//从Net库中搜索消息
		HINSTANCE hInst;
		hInst = LoadLibraryEx("NETMSG.DLL", NULL, DONT_RESOLVE_DLL_REFERENCES);
		if (hInst != 0)
		{
			dwRet = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | // The function will allocate memory for the message.
				FORMAT_MESSAGE_FROM_HMODULE | // Message definition is in a module.
				FORMAT_MESSAGE_IGNORE_INSERTS,  // No inserts used.
				hInst,
				dwErrorCode,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				(LPSTR)&pBuf,
				0,
				NULL
				);
			FreeLibrary(hInst);
		}
	}
	if (dwRet != 0)
	{
		strMsg = (LPSTR)pBuf;
		LocalFree(pBuf);
	}
	return strMsg;
}
