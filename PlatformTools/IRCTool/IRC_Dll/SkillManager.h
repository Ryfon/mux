﻿/** @file SkillManager.h 
@brief 玩家技能管理器
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-05-03
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef SKILLMANAGER_H
#define SKILLMANAGER_H
#include "CliProtocol_T.h"

using namespace MUX_PROTO;
//技能定义
typedef struct  _tagPlayerSkill
{
	int nID; //技能ID
	int nType; //技能类型 0 物理技能 1 魔法技能
	string strName; //技能名称
	int nLevel; //技能等级
	int nSpLevel;  //大于0 表示是幸运技能
	int nAttDistMin; //最小施放距离
	int nAttDistMax; //最大施放距离
	int nRange; //范围
	int nCDTime; //冷却时间
	int nCurseTime; //施展时间
	string strNote; //技能描述
	int nUllageHp; //消耗的hp
	int nUllageMp; //消耗的mp
	int nUllageItem; //消耗的道具id
	int nUllageItemNum; //消耗的道具数量
	int nUllageMoney; //消耗的金钱
	int nUllageSp; //消耗的幸运点

	string GetSkillTypeString(int iSkillType)
	{
		if (nSpLevel > 0)
		{
			return "幸运技能";
		}

		switch (iSkillType)
		{
		case TYPE_PHYSICS:
			{
				return "物理技能";
			}
			break;

		case TYPE_MAGIC:
			{
				return "魔法技能";
			}
			break;

		case TYPE_ASSIST:
			{
				return "附助技能";
			}
			break;

		case TYPE_SPECIAL:
			{
				return "SP技能";
			}
			break;
		};

		return "未定义技能";
	}

} PLAYERSKILL, *PPLAYERSKILL;

//玩家技能管理器
class CSkillManager
{
public:
	CSkillManager(void);
	virtual ~CSkillManager(void);
public:
	PPLAYERSKILL GetSkillInfo(unsigned long ulSkillID);
	int LoadSkillInfo(const char* szXmlFile);
private:
	map<unsigned long, PPLAYERSKILL> m_SkillInfos;
};

#endif