﻿/** @file ClientSession.h 
@brief 负责客户端连接的封装
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-01-18
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef CLIENTSESSION_H
#define CLIENTSESSION_H

class CMemNode;
class CClientManager;
class CMemPool;
class CPlayer;

//接收缓冲区
#define NETBUF 8192
#define MAXIMUMSEQUENSENUMBER 5121

enum IO_OPERATION{IO_CONN, IO_SEND_REQ, IO_SEND, IO_RECV};

class CClientSession
{
public:
	explicit CClientSession(CClientManager& ClientManager);
public:
	virtual ~CClientSession(void);
public:
	int OpenAssignSrv(const string& strAssignSvrIp, int nAssignSvrPort);
public:

	//打开连接
	int OpenSession(const string& strServerIp, int nPort);
	//关闭连接
	int CloseSession();
	//获得内部的Socket句柄
	SOCKET GetSocket() const {return m_ConnType == 1 ? m_CliSocket : m_UdpSocket;}
	//发出读数据请求
	int ReadData(CMemNode* pMemNode);
	//发出发送数据请求
	int SendData(CMemNode* pMemNode);
	
	int PostData(CMemNode* pMemNode);
	string GetAccount() const {return m_Account;}
	void SetAccount(const string& strAccount);
	bool IsConnected() const {return m_Connected;}
	
	//获得当前序号的内存节点
	CMemNode* GetCurData(CMemNode* pMemNode);
	CMemNode* GetPartialMem() const {return m_PartialMem;}

	void ProcMemNode(CMemNode* pMemNode);

	CMemNode* m_PartialMem;
	CClientManager& m_CliManager; //客户端管理模块

	//预分配一块内存
	CMemNode* PrepareMem();
	//释放一块内存
	void ReleaseMem(CMemNode* pMemNode);
	void SetPlayer(CPlayer* pPlayer);

	unsigned short GetLocalPort();
public:
	int OnConnection();
	int OnClose();
public:
	void GetSockDataLen(u_long& ulDataLen);
private:
	SOCKET m_CliSocket; //socket句柄
	SOCKET m_UdpSocket;
	SOCKADDR_IN m_LocalAddr; 
	string m_Account; //用户帐号
	CRITICAL_SECTION m_csSession;
	
	//序列化读操作
	unsigned long m_ReadSeqNo; //读序列号
	unsigned long m_CurReadSeqNo; //下一个要读的序列号
	map<unsigned long, CMemNode*> m_ReadMap; //需要进行排序的数据包
	//序列化写操作
	unsigned long m_SendSeqNo;
	unsigned long m_CurSendSeqNo;
	map<unsigned long, CMemNode*> m_SendMap;
private:
	CMemPool* m_MemPool; //内存池对象
	CPlayer* m_Player; //对应的玩家类
	string m_GateSrvIp;
	unsigned short m_GateSrvPort;
public:
	bool m_Connected;
	int m_ConnType;
private:
	CClientSession(const CClientSession&);
	CClientSession& operator=(const CClientSession&);
};

#endif