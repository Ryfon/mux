﻿#include "stdafx.h"
#include "TaskManager.h"

void SplitString( const char * szStr, vector< string >& vStrs, char chToken );

CTaskManager::CTaskManager(void)
{

}

CTaskManager::~CTaskManager(void)
{
	
}

void CTaskManager::LoadFromXml(const char* pszXmlFile)
{
	TiXmlDocument xmlDoc;
	xmlDoc.Parse(pszXmlFile);
	TiXmlElement * pRoot = xmlDoc.FirstChildElement();
	if ( !pRoot )
	{
		OutputDebugString(_T("打开任务xml为空！\n"));
		return;
	}

	TiXmlElement * pTaskElement = pRoot->FirstChildElement( "Task" );
	while ( pTaskElement )
	{
		CTask Task;
		pTaskElement->Attribute("ID", (int*)&Task.iID);
		Task.strName = pTaskElement->Attribute("Name");
		TiXmlNode* pSimpleDesc = pTaskElement->LastChild();
		TiXmlNode* pProviderDesc = pSimpleDesc->PreviousSibling();
		const char* szProviderDesc = pProviderDesc->Value();
		const char* szSimpleDesc = pSimpleDesc->Value();
		Task.strProviderDesc = szProviderDesc;
		Task.strSimpleDesc = szSimpleDesc;
		m_TaskMap[Task.iID] = Task;
		pTaskElement = pTaskElement->NextSiblingElement( "Task" );
	}

}

CTask* CTaskManager::GetTask(unsigned long ulTaskID)
{
	map<int, CTask>::iterator Ite = m_TaskMap.find(ulTaskID);
	return Ite == m_TaskMap.end() ? NULL : &Ite->second;
}

void SplitString( const char * szStr, vector< string >& vStrs, char chToken )
{
	string strCur;
	for ( UINT nIndex = 0; nIndex < strlen( szStr ); nIndex ++ )
	{
		if ( chToken == szStr[nIndex] )
		{
			vStrs.push_back( strCur );
			strCur = "";
		}
		else
		{
			strCur.push_back( szStr[nIndex] );
		}
	}

	if ( strCur.length() >= 1 )
		vStrs.push_back( strCur );
}