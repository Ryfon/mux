﻿#include "stdafx.h"
#include "MemPool.h"
#include "MemNode.h"
#include "AppException.h"

CMemPool::CMemPool(size_t BufSize, size_t FreeBufNum)
:m_BufSize(BufSize), m_BufNum(FreeBufNum)
{
	InitializeCriticalSection(&m_cs);
}

CMemPool::~CMemPool(void)
{
	Clear();
	DeleteCriticalSection(&m_cs);
}

CMemNode* CMemPool::NewMemNode(size_t BufSize)
{
	//m_FreeMemNodes中查找
	CMemNode* pMemNode = NULL;

	//EnterCriticalSection(&m_cs);
	if (/*!m_FreeMemNodes.empty()*/0)
	{
		pMemNode = m_FreeMemNodes.front();
		m_FreeMemNodes.pop_front();
		pMemNode->Empty();
	}
	else
	{
		if (BufSize > m_BufSize)
		{
			pMemNode = new(BufSize)CMemNode(*this, BufSize);
		}
		else
		{
			pMemNode = new(m_BufSize)CMemNode(*this, m_BufSize);
		}

	}
	//m_ActiceMemNodes.push_front(pMemNode);
	//LeaveCriticalSection(&m_cs);

	return pMemNode;
}

void CMemPool::DestroyMem(CMemNode* pMemNode)
{
	delete pMemNode;
}

void CMemPool::Clear()
{
	EnterCriticalSection(&m_cs);
	while (!m_ActiceMemNodes.empty())
	{
		DestroyMem(m_ActiceMemNodes.front());
		m_ActiceMemNodes.pop_front();
	}
	while (!m_FreeMemNodes.empty())
	{
		DestroyMem(m_FreeMemNodes.front());
		m_FreeMemNodes.pop_front();
	}
	LeaveCriticalSection(&m_cs);
}

void CMemPool::ReleaseMemNode(CMemNode* pMemNode)
{
	if (NULL == pMemNode)
	{
		throw CAppException("pMemNode is NULL in CMemPoo-ReleaseMemNode", "param is null");
	}

	//EnterCriticalSection(&m_cs);
	//m_ActiceMemNodes.remove(pMemNode);
	if (/*m_BufNum == 0 || m_FreeMemNodes.size() < m_BufNum*/0)
	{
		m_FreeMemNodes.push_front(pMemNode);
	}
	else
	{
		DestroyMem(pMemNode);
	}
	//LeaveCriticalSection(&m_cs);
}

void CMemNode::Empty()
{
	m_UsedSize = 0;
}