﻿#include "stdafx.h"
#include "Player.h"
#include "MemNode.h"
#include "ClientSession.h"
#include "SkillManager.h"
#include "ItemManager.h"
#include ".\AssignSvr\AssignProtocol.h"
#include "md5.h"
#include "MemPool.h"

using namespace Assign_namespace;
AssignProtocol g_AssignProtocol;

string HexToString(const char *pBuffer, size_t iBytes);
void StringToHex(const string &ts, char *pBuffer, size_t nBytes);

char CPacketBuild::m_InnerBuf[PKGBUILD_BUF_SIZE];
CliProtocol CPacketBuild::m_clipro;

#define SendPkg( PKG_TYPE, PKG_CONTENT ) \
{\
	char* pPkg = NULL;\
	int nPkgLen = 0;\
	if ( CPacketBuild::CreatePkg<PKG_TYPE>( &PKG_CONTENT, &pPkg, nPkgLen ) )/*//组包之后必须立即调用发包函数，在tmpRunMsg释放之前；*/\
	{\
		int rst = m_pNetMod->SendPkgReq( 0/*包编号，目前暂时不用*/, pPkg/*发送缓冲区*/, nPkgLen/*发送数据长度*/ );\
		if ( MUX_CLIMOD::MUX_CLI_SUCESS != rst )\
		{\
			char szMsg[128];\
		}\
	}\
}

CPlayer::CPlayer()
: m_UserStage(PS_INVALID), m_RoleNO(-1)
,  m_IsNetBiasAquired(false), m_NetBias(0), m_CliSession(NULL)
{
	InitializeCriticalSection(&m_CriticalSection);
	memset(&m_UserID, 0, sizeof(PlayerID));
	m_Handler.clear();
	RegisterHandler();
	m_CliKey = "";
	m_TrySendTimes = 10;
	m_ReadPtr = &m_ProNodes;
	m_WritePtr = &m_NetNodes;
	m_dwLastCheckID = 0;
	m_dwLastCheckTime = 0;
	m_Account = "";
	m_Pwd = "";
	m_hMemoryModule = NULL;
	m_hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}

CPlayer::~CPlayer(void)
{
	for (IN_BOX::iterator Ite = m_NetNodes.begin(); Ite != m_NetNodes.end(); Ite++)
	{
		m_CliSession->ReleaseMem(*Ite);
	}
	m_NetNodes.clear();

	for (IN_BOX::iterator Ite = m_ProNodes.begin(); Ite != m_ProNodes.end(); Ite++)
	{
		m_CliSession->ReleaseMem(*Ite);
	}
	m_ProNodes.clear();

	for (map<unsigned short, PROLE>::iterator Ite = m_RoleInfo.begin(); Ite != m_RoleInfo.end(); Ite++)
	{
		delete Ite->second;
	}
	for (REQRESPINFOMAP::iterator Ite = m_ReqRespInfos.begin(); Ite != m_ReqRespInfos.end(); Ite++)
	{
		delete Ite->second;
	}
	m_ReqRespInfos.clear();

	m_RoleInfo.clear();
	CloseHandle(m_hEvent);
	DeleteCriticalSection(&m_CriticalSection);
}

void CPlayer::GetAccountAndPwd(string& strAccount, string& strPwd)
{
	strAccount = m_Account;
	strPwd = m_Pwd;
}

const char* CPlayer::GetAccount()
{
	return m_Account.c_str();
}

int CPlayer::Login()
{
	//保留内存块的空间
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	//设置协议头
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_DataType = 0;
	pMsgHeader->m_Cmd = CGLogin::wCmd;

	//设置协议体
	CGLogin cgLogin;
	memset(&cgLogin, 0, sizeof(cgLogin));
	//字符串长度都需要包含/0
	cgLogin.accountSize = m_Account.length() + 1;
	sprintf(cgLogin.strUserAccount, "%s", m_Account.c_str());
	cgLogin.passwdSize = m_Pwd.length() + 1;
	sprintf(cgLogin.strUserPassword, "%s", m_Pwd.c_str());

	CGLogin* pCGLogIn = reinterpret_cast<CGLogin*>(pMemNode->GetBufferPrt() + sizeof(MSGHEADER));
	int nEnCodeLen = m_ProtolTool.EnCode(pMsgHeader->m_Cmd, &cgLogin, (char*)pCGLogIn, sizeof(CGLogin));
	pMsgHeader->m_DataLen = nEnCodeLen + sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	m_UserStage = PS_LOGINING;
	//发送消息
	m_CliSession->SendData(pMemNode);
	return 0;
}

int CPlayer::LoginResp(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCPlayerLogin, pPlayerLogin, pRawData, usDataLen, m_ProtolTool);
	//正常登录
	if (pPlayerLogin->byIsLoginOK == GCPlayerLogin::ISLOGINOK_SUCCESS)
	{
		m_UserStage = PS_LOGINED;
	}
	//登录失败
	else
	{
		switch(pPlayerLogin->byIsLoginOK)
		{
		case GCPlayerLogin::ISLOGINOK_FAILED_ACCOUNTNOTEXIST:
		case GCPlayerLogin::ISLOGINOK_FAILED_PWDWRONG:
			{
				m_IRCEvent->OnIRCError(ERR_INVALID_USER);
			}
			break;
		case GCPlayerLogin::ISLOGINOK_FAILED_ONLINE:
			{
				m_IRCEvent->OnIRCError(ERR_USER_ONLINE);
			}
		    break;
		default:
		    break;
		}
		
	}
	return 0;
}

int CPlayer::ProcRecvData(CMemNode* pMemNode)
{
	char* pData = pMemNode->GetBufferPrt();
	unsigned short pDataLen = *(unsigned short*)pData;
	if (pDataLen != pMemNode->GetUsedSize())
	{
		//CRobotManager::m_AppLogger.WriteLog("GameRobot.log", "Account:%s, Header data len:%d, Mem Node len:%d.\n", m_Account.c_str(), pDataLen, pMemNode->GetUsedSize());
		return 0;
	}
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pData);
	//处理具体的协议
	HANDLERMAP::iterator Ite = m_Handler.find(pMsgHeader->m_Cmd);
	if (Ite == m_Handler.end())
	{
		//CRobotManager::m_AppLogger.WriteLog("GameRobot.log", "Account:%s, msg id %d can not found handler.\n", m_Account.c_str(), pMsgHeader->m_Cmd);
	}
	else
	{
		(this->*Ite->second.handler)(pData + sizeof(MSGHEADER), pMsgHeader->m_DataLen - sizeof(MSGHEADER));
	}

	return 0;
}

int CPlayer::UpdatePlayerInfo(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCPlayerSelfInfo, pPlayerSelfInfo, pRawData, usDataLen, m_ProtolTool);
	m_UserID = pPlayerSelfInfo->lPlayerID;
	return 0;
}

int CPlayer::UpdateRoleInfo(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCPlayerRole, pPlayerRole, pRawData, usDataLen, m_ProtolTool);

	//有错误发生
	if (pPlayerRole->nErrCode != GCPlayerRole::ISERR_OK)
	{
		//报告错误
		m_IRCEvent->OnIRCError(ERR_INVALID_ROLE);
		return 0;
	}

	if (pPlayerRole->byFlag == GCPlayerRole::FLAG_END)
	{
		m_IRCEvent->OnRoleInfo(0, "");
	}
	else
	{
		map<unsigned short, PROLE>::iterator Ite = m_RoleInfo.find((pPlayerRole->playerInfo).uiID);
		PROLE pRole;
		if (Ite == m_RoleInfo.end())
		{
			pRole = new ROLE;
			m_RoleInfo[(pPlayerRole->playerInfo).uiID] = pRole;
		}
		else
		{
			pRole = Ite->second;
		}
		memcpy(pRole, &(pPlayerRole->playerInfo), sizeof(ROLE));
		m_IRCEvent->OnRoleInfo(pRole->playerInfo.uiID, pRole->playerInfo.szNickName);
	}
	return 0;
}

int CPlayer::SelectRole(unsigned int uiRoleNO)
{
	map<unsigned short, PROLE>::iterator Ite = m_RoleInfo.find(uiRoleNO);
	if (Ite == m_RoleInfo.end())
	{
		//给定的角色不存在
		//CRobotManager::m_AppLogger.WriteLog("GameRobot.log", "Account:%s, 给定的角色不存在.\n", m_Account.c_str());
		return -1;
	}
	//保留内存块的空间
	//设置协议体
	CGSelectRole cgSelRole;
	cgSelRole.uiRoleNo = uiRoleNO;
	SendPkg(CGSelectRole, cgSelRole);
	m_UserStage = PS_ROLESELING;

	return 0;		 
}

int CPlayer::UpdateSelRole(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCSelectRole, pSelRole, pRawData, usDataLen, m_ProtolTool);
	if (pSelRole->byIsSelectOK == GCSelectRole::ISSELECTOK_SUCCESS)
	{
		m_RoleNO = pSelRole->uiRoleNo;
		m_UserStage = PS_ROLESELED;
		StartGame();
	}
	//选角色失败
	else
	{
		m_IRCEvent->OnIRCError(ERR_INVALID_ROLE);
	}

	return 0;
}

int CPlayer::StartGame()
{
	//设置消息体
	CGIrcEnterWorld cgEnterWorld;
	//发送消息
	m_UserStage = PS_LOGINGGAME;
	SendPkg(CGIrcEnterWorld, cgEnterWorld);
	return 0;
}

int CPlayer::LoginGame(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCIrcEnterWorldRst, pPlayerAppear, pRawData, usDataLen, m_ProtolTool);
	if (pPlayerAppear->bSuccess)
	{
		m_UserStage = PS_LOGGAME;
		m_IRCEvent->OnLoginGame();
	}
	else
	{
		m_IRCEvent->OnIRCError(ERR_INVALID_ROLE);
	}
	return 0;
}

int CPlayer::ChatWith(CHAT_TYPE ChatType, const char* szDest, const char* strChat)
{
	//设置协议体
	CGChat cgChat;
	memset(&cgChat, 0, sizeof(CGChat));
	strncpy(cgChat.tgtName, szDest, strlen(szDest));
	cgChat.tgtNameSize = strlen(szDest) + 1;

	cgChat.chatSize = strlen(strChat) + 1;
	cgChat.nType = ChatType;
	sprintf(cgChat.strChat, "%s", strChat);
	SendPkg(CGChat, cgChat);
	return 0;
}


int CPlayer::HeartBeat(unsigned long ulPkgID)
{
	if (m_dwLastCheckTime == 0)
	{
		return 0;
	}

	DWORD dwNow = timeGetTime();
	if (dwNow - m_dwLastCheckTime < 3000 )
	{
		return 0;
	}

	m_dwLastCheckID++;
	m_dwLastCheckTime = dwNow;

	//保留内存块的空间
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	//设置协议头
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_DataType = 0;
	pMsgHeader->m_Cmd = CGTimeCheck::wCmd;
	//设置协议体
	CGTimeCheck cgTimeCheck;
	cgTimeCheck.checkID = m_dwLastCheckID;

	CGTimeCheck* pCGTimeCheck = reinterpret_cast<CGTimeCheck*>(pMemNode->GetBufferPrt() + sizeof(MSGHEADER));
	int nEncodeLen = m_ProtolTool.EnCode(CGTimeCheck::wCmd, &cgTimeCheck, (char*)pCGTimeCheck, sizeof(CGTimeCheck));
	pMsgHeader->m_DataLen = nEncodeLen + sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	//发送消息
	m_CliSession->SendData(pMemNode);
	return 0;
}

int CPlayer::PlayerChat(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCChat, pGCChat, pRawData, usDataLen, m_ProtolTool);
	if (IsBlackItem(pGCChat->srcName))
	{
		return 0;
	}
	switch(pGCChat->chatType)
	{
		//私聊
	case CT_PRIVATE_CHAT:
		{
			m_IRCEvent->OnPrivateMsg(pGCChat->srcName, pGCChat->strChat, pGCChat->chatType);
		}
		break;
	case CT_PRIVATE_IRC_CHAT:
		{
			m_IRCEvent->OnPrivateMsg(pGCChat->srcName, pGCChat->strChat, pGCChat->chatType);
		}
		break;
		//世界频道
	case CT_WORLD_CHAT:
		{
			if (stricmp(m_RoleName.c_str(), pGCChat->srcName) == 0)
			{
				return 0;
			}
			if (pGCChat->extInfo != 0)
			{
				return 0;
			}
			m_IRCEvent->OnWorldMsg(pGCChat->srcName, pGCChat->strChat, pGCChat->chatType);
		}
		break;
	case CT_WORLD_IRC_CHAT:
		{
			if (stricmp(m_RoleName.c_str(), pGCChat->srcName) == 0)
			{
				return 0;
			}
			m_IRCEvent->OnWorldMsg(pGCChat->srcName, pGCChat->strChat, pGCChat->chatType);
		}
		break;
	case CT_UNION_CHAT:
	case CT_UNION_IRC_CHAT:
		{
			if (stricmp(m_RoleName.c_str(), pGCChat->srcName) == 0)
			{
				return 0;
			}
			m_IRCEvent->OnGuildMsg(pGCChat->srcName, pGCChat->strChat, pGCChat->chatType);
		}
		break;
	default:
		{
			return 0;
		}
	    break;
	}
	SaveChat(pGCChat->chatType, ChatLog::PEER_SEND, pGCChat->srcName, pGCChat->strChat);
	return 0;
}

int CPlayer::PlayerHeartBeat(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCTimeCheck, pGCTimeCheck, pRawData, usDataLen, m_ProtolTool);
	return 0;
}

int CPlayer::NetDetect(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCNetDetect, pNetDetect, pRawData, usDataLen, m_ProtolTool);
	//服务器端发来的GCNetDetect包编号顺序错误
	if ( pNetDetect->pkgID != m_vecDetectPkgRcvTime.size() )
	{
		return 0;
	}

	m_vecDetectPkgRcvTime.push_back( timeGetTime());

	//设置协议体
	CGNetDetect cgNetDectect;
	cgNetDectect.pkgID = pNetDetect->pkgID;
	SendPkg(CGNetDetect, cgNetDectect);
	return 0;
}

int CPlayer::NetBias(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCNetBias, pGCNetBias, pRawData, usDataLen, m_ProtolTool);
	//服务器端发来的netBias包编号错误
	if (pGCNetBias->pkgID >= (int)m_vecDetectPkgRcvTime.size())
	{
		return 0;
	}

	m_NetBias = m_vecDetectPkgRcvTime[pGCNetBias->pkgID];//与服务器商定的基准时刻；
	m_IsNetBiasAquired = true;//是否已与服务器商定基准时刻；
	return 0;
}

void CPlayer::SetCliSession(CClientSession* pClientSession)
{
	m_CliSession = pClientSession;
}

int CPlayer::OnDataRecv(CMemNode* pMemNode)
{
	//先处理上次没有处理完的部分数据
	if (m_CliSession->m_PartialMem != NULL)
	{//上次剩下的数据不到一个包头
		if (m_CliSession->m_PartialMem->GetUsedSize() < sizeof(MSGHEADER))
		{
			pMemNode->AddToMemNode(m_CliSession->m_PartialMem, sizeof(MSGHEADER) - m_CliSession->m_PartialMem->GetUsedSize());
		}
		//合并后仍然不到一个包头
		if (m_CliSession->m_PartialMem->GetUsedSize() < sizeof(MSGHEADER))
		{
			if (pMemNode->GetUsedSize() != 0)
			{
				OutputDebugString("解包错误.\n");
			}

			m_CliSession->ReleaseMem(pMemNode);
			return 0;

		}

		PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(m_CliSession->m_PartialMem->GetBufferPrt());
		//请求的数据大于部分包中的数据量，把新的MemNode中的数据拷贝过去
		if (pMsgHeader->m_DataLen >= m_CliSession->m_PartialMem->GetUsedSize())
		{
			int nCopyBytes;
			if ((pMsgHeader->m_DataLen - m_CliSession->m_PartialMem->GetUsedSize()) > pMemNode->GetUsedSize())
			{
				nCopyBytes = pMemNode->GetUsedSize();
			}
			else
			{
				nCopyBytes = pMsgHeader->m_DataLen - m_CliSession->m_PartialMem->GetUsedSize();
			}
			pMemNode->AddToMemNode(m_CliSession->m_PartialMem, nCopyBytes);
		}
		else
		{
			m_CliSession->ReleaseMem(pMemNode);
			return 0;
		}
		if (pMsgHeader->m_DataLen == m_CliSession->m_PartialMem->GetUsedSize())
		{
			m_CliSession->m_PartialMem->SetBufType(IO_RECV);
			AddNetMsg(m_CliSession->m_PartialMem);
			m_CliSession->m_PartialMem = NULL;
		}
	}
	
	//当前包并到半包中去
	if (pMemNode->GetUsedSize() == 0)
	{
		m_CliSession->ReleaseMem(pMemNode);
		return 0;
	}
	//获得接收的消息长度和地址
	//pData指向MemNode中的缓冲区首地址

	char* pData = pMemNode->GetBufferPrt();
	//nDataLen表示MemNode中有用信息的字节数
	size_t nDataLen= pMemNode->GetUsedSize();
	//数据包没有包头大，作为数据不全的包处理
	if (nDataLen < sizeof(MSGHEADER))
	{
		m_CliSession->m_PartialMem = pMemNode;
		return 0;
	}

	//一个CMemNode中可能存在多个消息包，nMsgLen记录每个消息包的长度
	PMSGHEADER pmsgHeader;
	unsigned short nMsgLen = 0;
	while (nDataLen > 0)
	{
		pmsgHeader = reinterpret_cast<PMSGHEADER>(pData);
		if (pmsgHeader == NULL)
		{
			OutputDebugString("数据包错误， 不能获取包头！\n");
			m_CliSession->ReleaseMem(pMemNode);
			break;
		}

		nMsgLen = pmsgHeader->m_DataLen;
		if (nMsgLen == 0 || nMsgLen > MAX_MSG_SIZE)
		{
			//CRobotManager::m_AppLogger.WriteLog("GameRobot.log", "OnDataRecv: Account:%s, DataSize:%d, MemSize:%d, Cmd:%h\n", m_Account.c_str(), nMsgLen, nDataLen, pmsgHeader->m_Cmd);
			m_CliSession->ReleaseMem(pMemNode);
			m_CliSession->CloseSession();
			break;
		}

		if (nMsgLen == nDataLen) //MemNode中仅有一个消息包
		{
			AddNetMsg(pMemNode);
			break; //直接跳出while循环
		}
		else if (nMsgLen < nDataLen) //MemNode中包含多个消息包
		{
			CMemNode* pNewMemNode = pMemNode->SplitBuf(nMsgLen);
			pNewMemNode->SetBufType(IO_RECV);
			AddNetMsg(pNewMemNode);
		}
		else if(nMsgLen > nDataLen) //一个完整的消息包没有取完整
		{
			m_CliSession->m_PartialMem = pMemNode;
			break;
		}
		//继续处理余下的消息包
		nDataLen = pMemNode->GetUsedSize();
	}

	return 0;
}

int CPlayer::AddNetMsg(CMemNode* pMemNode)
{
	EnterCriticalSection(&m_CriticalSection);
	m_WritePtr->push_back(pMemNode);
	LeaveCriticalSection(&m_CriticalSection);
	return 0;
}

int CPlayer::HandleNetMsg()
{
	EnterCriticalSection(&m_CriticalSection);
	if (m_WritePtr->size() != 0)
	{
		IN_BOX* pTempPtr = m_ReadPtr;
		m_ReadPtr = m_WritePtr;
		m_WritePtr = pTempPtr;
		LeaveCriticalSection(&m_CriticalSection);
		IN_BOX::iterator Ite = m_ReadPtr->begin();
		while (Ite != m_ReadPtr->end())
		{
			CMemNode* pMemNode = *Ite;
			ProcRecvData(pMemNode);
			m_CliSession->ReleaseMem(pMemNode);
			m_ReadPtr->erase(Ite);
			//HeartBeat();
			Ite = m_ReadPtr->begin();
		}
	}
	else
	{
		LeaveCriticalSection(&m_CriticalSection);
	}
	//HeartBeat();
	m_pNetMod->ProcessNetEvent();
	return 0;
}

int CPlayer::LeaveWorld()
{
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_Cmd = REQ_CONN_CLOSE;
	pMsgHeader->m_DataType = LOCAL_TYPE;
	pMsgHeader->m_DataLen = sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	AddNetMsg(pMemNode);
	return 0;
}

int CPlayer::QueryAssignSrvState()
{
	//保留内存块的空间
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	//设置协议头
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_DataType = 0;
	pMsgHeader->m_Cmd = CAQuerySrvState::wCmd;
	//设置协议体
	pMsgHeader->m_DataLen = sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	SetTimeOut(CAQuerySrvState::wCmd, ACNotifyASrvState::wCmd, 1000);
	m_UserStage = PS_QUERYASSIGNSVR;
	//发送消息
	m_CliSession->SendData(pMemNode);
	return 0;
}

int CPlayer::QueryGateSrvAddr()
{
	//保留内存块的空间
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	//设置协议头
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_DataType = 0;
	pMsgHeader->m_Cmd = CARequestGateSrvAddr::wCmd;
	//设置协议体
	pMsgHeader->m_DataLen = sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	SetTimeOut(CARequestGateSrvAddr::wCmd, ACNotifyGateSrvAddr::wCmd, 1000);
	m_UserStage = PS_QUERYGATESVR;
	//发送消息
	m_CliSession->SendData(pMemNode);
	return 0;
}

int CPlayer::OnConnectionClose()
{
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_Cmd = CONN_CLOSE;
	pMsgHeader->m_DataType = LOCAL_TYPE;
	pMsgHeader->m_DataLen = sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	AddNetMsg(pMemNode);
	return 0;
}

int CPlayer::OnConnectionOpen()
{
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_Cmd = CONN_ID;
	pMsgHeader->m_DataType = LOCAL_TYPE;
	
	CONNMSG* pConnMsg = reinterpret_cast<CONNMSG*>(pMemNode->GetBufferPrt() + sizeof(MSGHEADER));
	pConnMsg->ConnType = m_CliSession->m_ConnType;
	pMsgHeader->m_DataLen = sizeof(CONNMSG) + sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	AddNetMsg(pMemNode);
	return 0;
}

int CPlayer::ProcNotifySvrState(void* pRawData, unsigned short usDataLen)
{
	if (m_UserStage != PS_QUERYASSIGNSVR)
	{
		return 0;
	}
	DealPlayerPkgPre(ACNotifyASrvState, pACNotifyASrvState, pRawData, usDataLen, g_AssignProtocol);
	CalcTimeOut(ACNotifyASrvState::wCmd);
	//该AssignServer忙碌，不能返回GateServer Ip
	if (pACNotifyASrvState->State == FULL)
	{
		char szBuf[MAX_PATH];
		sprintf(szBuf, "Account:%s, Assign server is busy.\n", m_Account.c_str());
		OutputDebugString(szBuf);
		m_CliSession->CloseSession();
		return 0;
	}
	QueryGateSrvAddr();
	m_CliSession->ReadData(NULL);

	return 0;
}

int CPlayer::ProcNotifyGateSrvAddr(void* pRawData, unsigned short usDataLen)
{
	if (m_UserStage != PS_QUERYGATESVR)
	{
		return 0;
	}
	DealPlayerPkgPre(ACNotifyGateSrvAddr, pACNotifyGateSrvAddr, pRawData, usDataLen, g_AssignProtocol);
	CalcTimeOut(ACNotifyGateSrvAddr::wCmd);
	in_addr ss ;
	ss.S_un.S_addr = htonl(pACNotifyGateSrvAddr->IP) ;
	m_UserStage = PS_OPENCONN;
	//m_CliSession->OpenSession(inet_ntoa(ss), pACNotifyGateSrvAddr->Port);

	int isok = m_pNetMod->InitMuxCliNetMod(this, inet_ntoa(ss), pACNotifyGateSrvAddr->Port);
	if (  MUX_CLIMOD::MUX_CLI_SUCESS != isok )
	{
		if ( MUX_CLIMOD::MUX_CLI_INPROCESS == isok )
		{
			//MessageBox( TEXT("重复初始化网络模块失败, in OnNetmodtestConnect") );
		} else if ( MUX_CLIMOD::MUX_CLI_REOPERA == isok ) {
			//MessageBox( TEXT("重复操作, in OnNetmodtestConnect") );
		} else {
			//MessageBox( TEXT("初始化网络模块失败, in OnNetmodtestConnect") );
		}
		return -1;
	}
	m_CliSession->OnConnection();
	return 0;
}

//注册消息处理句柄
int CPlayer::RegisterHandler()
{
	m_Handler[A_C_NOTIFYSRVSTATEID] = MSGHANDLER(PS_QUERYASSIGNSVR, &CPlayer::ProcNotifySvrState);
	m_Handler[A_C_NOTIFYGATESRVID] = MSGHANDLER(PS_QUERYASSIGNSVR, &CPlayer::ProcNotifyGateSrvAddr);
	m_Handler[G_C_RANDOM_STRING] = MSGHANDLER(PS_LOGINING, &CPlayer::ProcRandomKey);

	m_Handler[G_C_PLAYER_LOGIN] = MSGHANDLER(PS_LOGINING, &CPlayer::LoginResp);
	m_Handler[G_C_PLAYER_SELF_INFO] = MSGHANDLER(PS_LOGINING, &CPlayer::UpdatePlayerInfo);
	m_Handler[G_C_PLAYER_ROLE] = MSGHANDLER(PS_LOGINED, &CPlayer::UpdateRoleInfo);
	m_Handler[G_C_SELECT_ROLE] = MSGHANDLER(PS_LOGINED, &CPlayer::UpdateSelRole);
	m_Handler[G_C_NET_DETECT] = MSGHANDLER(PS_LOGGAME, &CPlayer::NetDetect);
	m_Handler[G_C_IRC_ENTER_WORLD_RST] = MSGHANDLER(PS_LOGINGGAME, &CPlayer::LoginGame);
	m_Handler[G_C_CHAT] = MSGHANDLER(PS_LOGGAME, &CPlayer::PlayerChat);
	m_Handler[G_C_NET_BIAS] = MSGHANDLER(PS_LOGGAME, &CPlayer::NetBias);
	m_Handler[G_C_TIME_CHECK] = MSGHANDLER(PS_LOGGAME, &CPlayer::PlayerHeartBeat);
	m_Handler[CONN_ID] = MSGHANDLER(PS_INVALID, &CPlayer::ProcConn);
	m_Handler[REQ_CONN] = MSGHANDLER(PS_INVALID, &CPlayer::ProcConnReq);
	m_Handler[CONN_CLOSE] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcConnClose);
	m_Handler[REQ_CONN_CLOSE] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcConnCloseReq);
	m_Handler[G_C_FRIEND_GROUPS] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcFriendGroups);
	m_Handler[G_C_SRV_ERROR] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcServerError);
	m_Handler[G_C_FRIENDS] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcFriends);	
	m_Handler[G_C_FOE_LIST] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcFoeList);	
	m_Handler[G_C_NEW_FOE] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcNewFoe);	
	m_Handler[G_C_BLACKLIST] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcBlackList);	
	m_Handler[G_C_FRIEND_INFO_UPDATE] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcFriendInfoUpdate);	
	m_Handler[G_C_FOEINFO_UPDATE] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProFoeInfoUpdate);	
	m_Handler[G_C_CHAT_ERROR] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcPrivateChatErro);	
	m_Handler[G_C_IRC_QUERY_PLAYERINFO_RST] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcIrcQryUserInfo);	
	m_Handler[G_C_QUERY_UNION_BASIC_RST] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcUnionInfo);	
	m_Handler[G_C_DESTROY_UNION_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcUnionDestroy);	
	m_Handler[G_C_REMOVE_UNION_MEM_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcDelUnionMember);	
	m_Handler[G_C_FOR_UNION_SPEEK_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcForbidUnionSpeek);	
	m_Handler[G_C_QUERY_UNION_MEM_RST] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcUnionMembers);		
	m_Handler[G_C_ADD_UNION_MEM_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcAddUnionMember);		
	m_Handler[G_C_UNION_MEM_ONOFF_LINE_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcUnionMemOnOffline);		
	m_Handler[G_C_MOD_UNION_MBR_TTL_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcUnionMemTitleNty);		
	m_Handler[G_C_UPD_UNION_POS_CON_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcUnionPosUdpNty);		
	m_Handler[G_C_ADV_UNION_MBR_POS_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcUnionMemAdvPos);		
	m_Handler[G_C_RED_UNION_MBR_POS_NTF] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcUnionMemRedPos);		


/*
	m_Handler[PRI_MSG] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcPrivateMsgRec);	
	m_Handler[WORLD_MSG] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcWorldMsgRec);	
	m_Handler[GUILD_MSG] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcGuildMsgRec);	
*/
	m_Handler[CLEAR_LOG] = MSGHANDLER(PS_LOGGAME, &CPlayer::ProcClearLog);	
	return 0;
}

int CPlayer::ProcServerError(void * pRawData, USHORT usDataLen)
{
	DealPlayerPkgPre(GCSrvError, pGCSrvError, pRawData, usDataLen, m_ProtolTool);
	return 0;
}

int CPlayer::ProcConn(void* pRawData, unsigned short usDataLen)
{
	CONNMSG* pConnMsg = reinterpret_cast<CONNMSG*>(pRawData);
	//建立udp伪连接
	if (pConnMsg->ConnType == 0 && m_UserStage == PS_INVALID)
	{
		m_CliSession->ReadData(NULL);
		QueryAssignSrvState();
		return 0;
	}
	//建立到GateServer连接
	m_dwLastCheckTime = timeGetTime();
	//HeartBeat(m_dwLastCheckID);
	//ReqClientKey();
	int nRet = m_pNetMod->LoginReq(m_Account.c_str(), m_Pwd.c_str());
	return 0;
}

int CPlayer::ReqClientKey()
{
	//保留内存块的空间
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	//设置协议头
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_DataType = 0;
	pMsgHeader->m_Cmd = CGRequestRandomString::wCmd;

	//设置协议体
	CGRequestRandomString cgRandomString;
	memset(&cgRandomString, 0, sizeof(cgRandomString));
	//字符串长度都需要包含/0
	cgRandomString.accountSize = m_Account.length() + 1;
	sprintf(cgRandomString.strUserAccount, "%s", m_Account.c_str());

	CGRequestRandomString* pCGRandomString = reinterpret_cast<CGRequestRandomString*>(pMemNode->GetBufferPrt() + sizeof(MSGHEADER));
	int nEnCodeLen = m_ProtolTool.EnCode(pMsgHeader->m_Cmd, &cgRandomString, (char*)pCGRandomString, sizeof(CGRequestRandomString));
	pMsgHeader->m_DataLen = nEnCodeLen + sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	m_UserStage = PS_LOGINING;
	//发送消息
	m_CliSession->SendData(pMemNode);

	return 0;
}

int CPlayer::ProcRandomKey(void* pRawData, unsigned short usDataLen)
{
	//DealPlayerPkgPre(GCRandomString, pGCRandomString, pRawData, usDataLen, m_ProtolTool);
	GCRandomString* pGCRandomString = reinterpret_cast<GCRandomString*>(pRawData);
	m_CliKey.append(pGCRandomString->randomString, pGCRandomString->rdStrSize);
	ReqCheckPwd();
	return 0;
}

int CPlayer::ReqCheckPwd()
{
	//生成MD5串
	char md5val[128];
	memset( md5val, 0, sizeof(md5val) );
	build_md5(m_Account, m_Pwd, m_CliKey.c_str(), m_CliKey.length(), md5val, sizeof(md5val));//计算md5值;

	//保留内存块的空间
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	//设置协议头
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_DataType = 0;
	pMsgHeader->m_Cmd = CGUserMD5Value::wCmd;

	//设置协议体
	CGUserMD5Value cgUserMD5Value;
	memset(&cgUserMD5Value, 0, sizeof(cgUserMD5Value));
	cgUserMD5Value.md5ValueSize = MD5_VALUE_CHARLEN;
	memcpy_s(cgUserMD5Value.md5Value, sizeof(cgUserMD5Value.md5Value), md5val, cgUserMD5Value.md5ValueSize);

	CGUserMD5Value* pCGUserMD5Value = reinterpret_cast<CGUserMD5Value*>(pMemNode->GetBufferPrt() + sizeof(MSGHEADER));
	int nEnCodeLen = m_ProtolTool.EnCode(pMsgHeader->m_Cmd, &cgUserMD5Value, (char*)pCGUserMD5Value, sizeof(CGUserMD5Value));
	pMsgHeader->m_DataLen = nEnCodeLen + sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	m_UserStage = PS_LOGINING;
	//发送消息
	m_CliSession->SendData(pMemNode);
	return 0;
}

int CPlayer::ProcFriendGroups(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCFriendGroups, pGCFriendGroups, pRawData, usDataLen, m_ProtolTool);
	for (UINT nIndex = 0; nIndex < pGCFriendGroups->friendGroupsSize; nIndex++)
	{
		m_FriendGroupMap[pGCFriendGroups->friendGroups[nIndex].groupID] = pGCFriendGroups->friendGroups[nIndex].groupName;
	}

	FriendGroupInfo_I *pFriendGroupInfo = new FriendGroupInfo_I[pGCFriendGroups->friendGroupsSize];
	for (UINT nIndex = 0; nIndex < pGCFriendGroups->friendGroupsSize; nIndex++)
	{
		pFriendGroupInfo[nIndex].m_GroupID = pGCFriendGroups->friendGroups[nIndex].groupID;
		memset(pFriendGroupInfo[nIndex].m_GroupName, 0, sizeof(pFriendGroupInfo[nIndex].m_GroupName));
		memcpy_s(pFriendGroupInfo[nIndex].m_GroupName, sizeof(pFriendGroupInfo[nIndex].m_GroupName), pGCFriendGroups->friendGroups[nIndex].groupName, pGCFriendGroups->friendGroups[nIndex].groupNameSize);
	}
	m_IRCEvent->OnFriendsGroup(pFriendGroupInfo, pGCFriendGroups->friendGroupsSize);
	delete [] pFriendGroupInfo;
	return 0;
}

int CPlayer::OpenConnection()
{
	m_UserStage = PS_INVALID;
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_Cmd = REQ_CONN;
	pMsgHeader->m_DataType = LOCAL_TYPE;

	REQCONNMSG* pReqConnMsg = reinterpret_cast<REQCONNMSG*>(pMemNode->GetBufferPrt() + sizeof(MSGHEADER));
	int nServerType = m_AssignSrvIp != "" ? 0 : 1;
	//pReqConnMsg->ConnType = nServerType;
	pReqConnMsg->ConnType = 1;
	if (nServerType == 0)
	{
		memset(pReqConnMsg->IpAddr, 0, sizeof(pReqConnMsg->IpAddr));
		strcpy(pReqConnMsg->IpAddr, m_AssignSrvIp.c_str());
		pReqConnMsg->Port = m_AssignSrvPort;

	}
	else
	{
		memset(pReqConnMsg->IpAddr, 0, sizeof(pReqConnMsg->IpAddr));
		strcpy(pReqConnMsg->IpAddr, m_GateSrvIp.c_str());
		pReqConnMsg->Port = m_GatePort;
	}
	pMsgHeader->m_DataLen = sizeof(REQCONNMSG) + sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	AddNetMsg(pMemNode);
	return 0;
}

int CPlayer::ProcConnReq(void* pRawData, unsigned short usDataLen)
{
	REQCONNMSG* pReqConnMsg = reinterpret_cast<REQCONNMSG*>(pRawData);
	if (pReqConnMsg->ConnType == 0)
	{
		m_CliSession->OpenAssignSrv(pReqConnMsg->IpAddr, pReqConnMsg->Port);
	}
	else
	{
		//m_CliSession->OpenSession(pReqConnMsg->IpAddr, pReqConnMsg->Port);
		m_CliSession->m_ConnType = 1;
		int isok = m_pNetMod->InitMuxCliNetMod(this, pReqConnMsg->IpAddr, pReqConnMsg->Port);
		if (  MUX_CLIMOD::MUX_CLI_SUCESS != isok )
		{
			if ( MUX_CLIMOD::MUX_CLI_INPROCESS == isok )
			{
				//MessageBox( TEXT("重复初始化网络模块失败, in OnNetmodtestConnect") );
			} else if ( MUX_CLIMOD::MUX_CLI_REOPERA == isok ) {
				//MessageBox( TEXT("重复操作, in OnNetmodtestConnect") );
			} else {
				//MessageBox( TEXT("初始化网络模块失败, in OnNetmodtestConnect") );
			}
			return -1;
		}
		m_CliSession->OnConnection();
	}
	return 0;
}

int CPlayer::ProcConnClose(void* pRawData, unsigned short usDataLen)
{
	m_CliKey.clear();
	m_FriendGroupMap.clear();
	m_FriendInfoMap.clear();
	m_FoeInfoMap.clear();
	m_BlackItemMap.clear();
	//此处不能区分自己主动离线还是被动离线，需要在UI上判断是否被动离线
	m_IRCEvent->OnIRCError(ERR_CONNECTION_BREAKDOWN);
	m_UserStage = PS_INVALID;

	return 0;
}

int CPlayer::ProcConnCloseReq(void* pRawData, unsigned short usDataLen)
{
	//m_CliSession->CloseSession();
	m_pNetMod->UninitMuxCliNetMod();
	m_CliSession->OnClose();
	return 0;
}

int CPlayer::ProcFriends(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCPlayerFriends, pGCPlayerFriends, pRawData, usDataLen, m_ProtolTool);
	for (UINT nIndex = 0; nIndex < pGCPlayerFriends->playerFriendsSize; nIndex++)
	{
		pGCPlayerFriends->playerFriends[nIndex].joinID = m_FriendInfoMap.size();
		m_FriendInfoMap[pGCPlayerFriends->playerFriends[nIndex].friendID] = pGCPlayerFriends->playerFriends[nIndex];
	}
	//好友信息已经发送完毕
	if (pGCPlayerFriends->pageID == 0)
	{
		FriendInfo_I* pFriendInfo = new FriendInfo_I[m_FriendInfoMap.size()];
		int nIndex = 0;
		for (FRIENDINFOMAP::iterator Ite = m_FriendInfoMap.begin(); Ite != m_FriendInfoMap.end(); Ite++)
		{
			pFriendInfo[nIndex].friendID = Ite->second.friendID;
			pFriendInfo[nIndex].groupID = Ite->second.groupID;
			pFriendInfo[nIndex].isHateMe = Ite->second.isHateMe;
			pFriendInfo[nIndex].isOnline = Ite->second.isOnline;
			pFriendInfo[nIndex].joinID = Ite->second.joinID;
			pFriendInfo[nIndex].mapID = Ite->second.mapID;
			memset(pFriendInfo[nIndex].nickName, 0, sizeof(pFriendInfo[nIndex].nickName));
			strcpy(pFriendInfo[nIndex].nickName, Ite->second.nickName);
			pFriendInfo[nIndex].playerClass = Ite->second.playerClass;
			pFriendInfo[nIndex].playerLevel = Ite->second.playerLevel;
			pFriendInfo[nIndex].playerSex = Ite->second.playerSex;
			pFriendInfo[nIndex].playerRace = Ite->second.playerRace;
			strncpy(pFriendInfo[nIndex].GuildName, Ite->second.GuildName, Ite->second.GuildNameLen);
			strncpy(pFriendInfo[nIndex].FereName, Ite->second.FereName, Ite->second.FereNameLen);
			nIndex++;
		}
		m_IRCEvent->OnFriendInfo(pFriendInfo, nIndex);
		delete [] pFriendInfo;
	}
	return 0;
}

int CPlayer::ProcFoeList(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCFoeList, pGCFoeList, pRawData, usDataLen, m_ProtolTool);

	for (UINT nIndex = 0; nIndex < pGCFoeList->playerFoeListSize; nIndex++)
	{
		pGCFoeList->playerFoeList[nIndex].joinID = m_FoeInfoMap.size();
		m_FoeInfoMap[pGCFoeList->playerFoeList[nIndex].foeID] = pGCFoeList->playerFoeList[nIndex];
	}

	FoeInfo_I* pFoeInfo = new FoeInfo_I[pGCFoeList->playerFoeListSize];
	for (UINT nIndex = 0; nIndex < pGCFoeList->playerFoeListSize; nIndex++)
	{
		pFoeInfo[nIndex].foeID = pGCFoeList->playerFoeList[nIndex].foeID;
		pFoeInfo[nIndex].isHateMe = pGCFoeList->playerFoeList[nIndex].isHateMe;
		pFoeInfo[nIndex].isLocked = pGCFoeList->playerFoeList[nIndex].isLocked;
		pFoeInfo[nIndex].isOnline = pGCFoeList->playerFoeList[nIndex].isOnline;
		pFoeInfo[nIndex].joinID = pGCFoeList->playerFoeList[nIndex].joinID;
		pFoeInfo[nIndex].mapID = pGCFoeList->playerFoeList[nIndex].mapID;
		memset(pFoeInfo[nIndex].nickName, 0, sizeof(pFoeInfo[nIndex].nickName));
		strcpy(pFoeInfo[nIndex].nickName, pGCFoeList->playerFoeList[nIndex].nickName);
		pFoeInfo[nIndex].playerClass = pGCFoeList->playerFoeList[nIndex].playerClass;
		pFoeInfo[nIndex].playerLevel = pGCFoeList->playerFoeList[nIndex].playerLevel;
		pFoeInfo[nIndex].playerSex = pGCFoeList->playerFoeList[nIndex].playerSex;
		pFoeInfo[nIndex].playerRace = pGCFoeList->playerFoeList[nIndex].playerRace;
		strncpy(pFoeInfo[nIndex].GuildName, pGCFoeList->playerFoeList[nIndex].GuildName, pGCFoeList->playerFoeList[nIndex].GuildNameLen);
		strncpy(pFoeInfo[nIndex].FereName, pGCFoeList->playerFoeList[nIndex].FereName, pGCFoeList->playerFoeList[nIndex].FereNameLen);
	}
	m_IRCEvent->OnFoeInfo(pFoeInfo, pGCFoeList->playerFoeListSize);
	delete [] pFoeInfo;
	return 0;
}

int CPlayer::ProcNewFoe(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCNewFoe, pGCNewFoe, pRawData, usDataLen, m_ProtolTool);

	return 0;
}

int CPlayer::SetServerInfo(const char* szAssignSvrIp, unsigned short usAssignSvrPort, const char* szGateSvrIp, unsigned short usGateSvrPort)
{
	m_AssignSrvIp = szAssignSvrIp;
	m_AssignSrvPort = usAssignSvrPort;
	m_GateSrvIp = szGateSvrIp;
	m_GatePort = usGateSvrPort;
	return 0;
}

int CPlayer::SetPlayerInfo(const char *szAccount, const char *szPwd, const char* szRoleName)
{
	m_Account = szAccount;
	m_Pwd = szPwd;
	m_RoleName = szRoleName;
	return 0;
}

int CPlayer::ReadSkillInfo(const char* szSkillFileName)
{
	m_SkillManager.LoadSkillInfo(szSkillFileName);
	return 0;
}

int CPlayer::ReadItemInfo(const char* szItemFileName)
{
	m_ItemManager.LoadItemData(szItemFileName);
	return 0;
}

int CPlayer::ReadTaskInfo(const char *szTaskFileName)
{
	m_TaskManager.LoadFromXml(szTaskFileName);
	return 0;
}

int CPlayer::StartUp(CIRCEventSink &ircEvent, char* pDllData)
{
	if (!GetCreateCliNetModFun(pDllData))
	{
		return -1;
	}
	
	if ((*m_pfnGetMuxCliNetMod)( &m_pNetMod ) != MUX_CLIMOD::MUX_CLI_SUCESS)
	{
		return -1;
	}

	md5::init();
	m_IRCEvent = &ircEvent;

	m_CliManager.StartCliMng();
	ResetEvent(m_hEvent);
	m_hFrameUpdate = reinterpret_cast<HANDLE>(_beginthreadex(0, 0, &FrameUpdate, this, CREATE_SUSPENDED, NULL));

	CClientSession* pClientSession = m_CliManager.AddClientSession(GetAccount());
	pClientSession->SetAccount(GetAccount());
	pClientSession->SetPlayer(this);
	SetCliSession(pClientSession);
	ResumeThread(m_hFrameUpdate);

	return 0;
}

int CPlayer::CleanUp()
{
	m_CliManager.StopCliMng();
	//LeaveWorld();
	SetEvent(m_hEvent);
	WaitForSingleObject(m_hFrameUpdate, INFINITE);
	//清除资源
	m_pNetMod->UninitMuxCliNetMod();
	m_pNetMod = NULL;
	MemoryFreeLibrary(m_hMemoryModule);
	m_hMemoryModule = NULL;
	return 0;
}

unsigned __stdcall CPlayer::FrameUpdate(void *pThreadParams)
{
	CPlayer* pPlayer = reinterpret_cast<CPlayer*>(pThreadParams);
	while(WaitForSingleObject(pPlayer->m_hEvent, 1) == WAIT_TIMEOUT)
	{
		pPlayer->HandleNetMsg();
	}
	return 0;
}

int CPlayer::ProcBlackList(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCPlayerBlacklist, pGCPlayerBlacklist, pRawData, usDataLen, m_ProtolTool);
	pGCPlayerBlacklist->playerBlacklist.joinID = m_BlackItemMap.size();
	m_BlackItemMap[pGCPlayerBlacklist->playerBlacklist.itemID] = pGCPlayerBlacklist->playerBlacklist;
	BlackListItemInfo_I* pBlackItem = new BlackListItemInfo_I;
	pBlackItem->itemID = pGCPlayerBlacklist->playerBlacklist.itemID;
	pBlackItem->joinID = pGCPlayerBlacklist->playerBlacklist.joinID;
	memset(pBlackItem->nickName, 0, sizeof(pBlackItem->nickName));
	strcpy(pBlackItem->nickName, pGCPlayerBlacklist->playerBlacklist.nickName);
	m_IRCEvent->OnBlackItem(pBlackItem);
	delete pBlackItem;
	return 0;
}

int CPlayer::ProcFriendInfoUpdate(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCFriendInfoUpdate, pGCFriendInfoUpdate, pRawData, usDataLen, m_ProtolTool);
	FriendInfo_I* pFriendInfo = new FriendInfo_I;
	pFriendInfo->friendID = pGCFriendInfoUpdate->friendInfo.friendID;
	pFriendInfo->groupID = pGCFriendInfoUpdate->friendInfo.groupID;
	pFriendInfo->isHateMe = pGCFriendInfoUpdate->friendInfo.isHateMe;
	pFriendInfo->isOnline = pGCFriendInfoUpdate->friendInfo.isOnline;
	pFriendInfo->joinID = pGCFriendInfoUpdate->friendInfo.joinID;
	pFriendInfo->mapID = pGCFriendInfoUpdate->friendInfo.mapID;
	memset(pFriendInfo->nickName, 0, sizeof(pGCFriendInfoUpdate->friendInfo.nickName));
	strcpy(pFriendInfo->nickName, pGCFriendInfoUpdate->friendInfo.nickName);
	pFriendInfo->playerClass = pGCFriendInfoUpdate->friendInfo.playerClass;
	pFriendInfo->playerLevel = pGCFriendInfoUpdate->friendInfo.playerLevel;
	pFriendInfo->playerSex = pGCFriendInfoUpdate->friendInfo.playerSex;
	strncpy(pFriendInfo->GuildName, pGCFriendInfoUpdate->friendInfo.GuildName, pGCFriendInfoUpdate->friendInfo.GuildNameLen);
	strncpy(pFriendInfo->FereName, pGCFriendInfoUpdate->friendInfo.FereName, pGCFriendInfoUpdate->friendInfo.FereNameLen);
	m_IRCEvent->OnFriendInfoUpdate(pFriendInfo);
	delete pFriendInfo;
	return 0;
}

int CPlayer::ProFoeInfoUpdate(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCFoeInfoUpdate, pGCFoeInfoUpdate, pRawData, usDataLen, m_ProtolTool);
	FoeInfo_I* pFoeInfo = new FoeInfo_I;
	pFoeInfo->foeID = pGCFoeInfoUpdate->updateFoe.foeID;
	pFoeInfo->isHateMe = pGCFoeInfoUpdate->updateFoe.isHateMe;
	pFoeInfo->isLocked = pGCFoeInfoUpdate->updateFoe.isLocked;
	pFoeInfo->isOnline = pGCFoeInfoUpdate->updateFoe.isOnline;
	pFoeInfo->joinID = pGCFoeInfoUpdate->updateFoe.joinID;
	pFoeInfo->mapID = pGCFoeInfoUpdate->updateFoe.mapID;
	memset(pFoeInfo->nickName, 0, sizeof(pFoeInfo->nickName));
	strcpy(pFoeInfo->nickName, pGCFoeInfoUpdate->updateFoe.nickName);
	pFoeInfo->playerClass = pGCFoeInfoUpdate->updateFoe.playerClass;
	pFoeInfo->playerLevel = pGCFoeInfoUpdate->updateFoe.playerLevel;
	pFoeInfo->playerSex = pGCFoeInfoUpdate->updateFoe.playerSex;
	strncpy(pFoeInfo->GuildName, pGCFoeInfoUpdate->updateFoe.GuildName, pGCFoeInfoUpdate->updateFoe.GuildNameLen);
	strncpy(pFoeInfo->FereName, pGCFoeInfoUpdate->updateFoe.FereName, pGCFoeInfoUpdate->updateFoe.FereNameLen); 
	m_IRCEvent->OnFoeUpdate(pFoeInfo);
	delete pFoeInfo;
	return 0;
}

int CPlayer::ProcPrivateChatErro(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCChatError, pGCChatError, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnChatError(pGCChatError->tgtName, pGCChatError->nErrorType);
	return 0;
}

int CPlayer::SendMsgToWorld(const char* szChat)
{
	SaveChat(CT_WORLD_IRC_CHAT, ChatLog::LOCAL_SEND, "", szChat);
	ChatWith(CT_WORLD_IRC_CHAT, "", szChat);
	return 0;
}

int CPlayer::SendMsgToGuild(const char* szChat)
{
	SaveChat(CT_UNION_IRC_CHAT, ChatLog::LOCAL_SEND, "", szChat);
	ChatWith(CT_UNION_IRC_CHAT, "", szChat);
	return 0;
}
int CPlayer::SendPrivateMsg(const char* szDest, const char* szChat)
{
	SaveChat(CT_PRIVATE_IRC_CHAT, ChatLog::LOCAL_SEND, szDest, szChat);
	ChatWith(CT_PRIVATE_IRC_CHAT, szDest, szChat);
	return 0;
}


string HexToString(const char *pBuffer, size_t iBytes)
{
	string result;

	for (size_t i = 0; i < iBytes; i++)
	{
		BYTE c ;

		BYTE b = pBuffer[i];
		b = b >> 4;

		if (9 >= b)
		{
			c = b + '0';
		}
		else
		{
			c = (b - 10) + 'A';
		}

		result += (BYTE)c;

		b = pBuffer[i] & 0x0f;

		if (9 >= b)
		{
			c = b + '0';
		}
		else
		{
			c = (b - 10) + 'A';
		}

		result += (BYTE)c;
	}

	return result;
}

void StringToHex(const string &ts, char *pBuffer, size_t nBytes)
{
	for (size_t i = 0; i < nBytes; i++)
	{
		const size_t stringOffset = i * 2;

		BYTE val = 0;

		const BYTE b = ts[stringOffset];

		if (isdigit(b)) 
		{
			val = (BYTE)((b - '0') * 16); 
		}
		else 
		{
			val = (BYTE)(((toupper(b) - 'A') + 10) * 16); 
		}

		const BYTE b1 = ts[stringOffset + 1];

		if (isdigit(b1)) 
		{
			val += b1 - '0' ; 
		}
		else 
		{
			val += (BYTE)((toupper(b1) - 'A') + 10); 
		}

		pBuffer[i] = val;
	}
}

int CPlayer::ReadWorldMsg()
{
	m_RecordMng.ReadChatLog((CChatRecordMng::CHAT_TYPE)CT_WORLD_CHAT);
	return 0;
}

int CPlayer::ReadGuildMsg()
{
	m_RecordMng.ReadChatLog((CChatRecordMng::CHAT_TYPE)CT_UNION_CHAT);
	return 0;
}
int CPlayer::ReadPrivateMsg()
{
	m_RecordMng.ReadChatLog((CChatRecordMng::CHAT_TYPE)CT_PRIVATE_CHAT);
	return 0;
}

int CPlayer::SaveChat(CHAT_TYPE ct, ChatLog::CHATDIRECT cd, const char* szChatObj, const char* szChat)
{
	switch(ct)
	{
	case CT_PRIVATE_CHAT:
	case CT_PRIVATE_IRC_CHAT:
		{
			//看看私聊对象是好友、仇人、陌生人
			int bFindResult = 0;
			
			for (FRIENDINFOMAP::iterator Ite = m_FriendInfoMap.begin(); Ite != m_FriendInfoMap.end(); Ite++)
			{
				if (strcmp(Ite->second.nickName, szChatObj) == 0)
				{
					bFindResult = 1;
					break;
				}
			}
			
			if (bFindResult == 0)
			{
				for (FOEINFOMAP::iterator Ite = m_FoeInfoMap.begin(); Ite != m_FoeInfoMap.end(); Ite++)
				{
					if (strcmp(Ite->second.nickName, szChatObj) == 0)
					{
						bFindResult = 2;
						break;
					}
				}
			}

			switch(bFindResult)
			{
			case 0:
				{
					if (!(m_Filter & 64))
					{
						return 0;
					}
				}
				break;
			case 1:
				{
					if (!(m_Filter & 16))
					{
						return 0;
					}
				}
				break;
			case 2:
				{
					if (!(m_Filter & 32))
					{
						return 0;
					}
				}
			    break;
			default:
			    break;
			}
		}
		break;
	case CT_WORLD_CHAT:
	case CT_WORLD_IRC_CHAT:
		{
			if (!(m_Filter & 1))
			{
				return 0;
			}
		}
	    break;
	case CT_UNION_IRC_CHAT:
	case CT_UNION_CHAT:
		{
			if (!(m_Filter & 4))
			{
				return 0;
			}
		}
		break;
	default:
		{
			return 0;
		}
	    break;
	}
	m_RecordMng.WriteChatLog((CChatRecordMng::CHAT_TYPE)ct, cd == ChatLog::LOCAL_SEND, m_RoleName.c_str(), szChatObj, szChat);
	return 0;
}
/*
int CPlayer::ProcPrivateMsgRec(void* pRawData, unsigned short usDataLen)
{
	ChatLog cl;
	LoadChat((char*)pRawData, usDataLen, cl);
	m_IRCEvent->OnReadPrivateMsg(&cl);
	return 0;
}

int CPlayer::ProcWorldMsgRec(void* pRawData, unsigned short usDataLen)
{
	ChatLog cl;
	LoadChat((char*)pRawData, usDataLen, cl);
	m_IRCEvent->OnReadWorldMsg(&cl);
	return 0;
}

int CPlayer::ProcGuildMsgRec(void *pRawData, unsigned short usDataLen)
{
	ChatLog cl;
	LoadChat((char*)pRawData, usDataLen, cl);
	m_IRCEvent->OnReadGuildMsg(&cl);
	return 0;
}
*/
int CPlayer::LoadChat(char* pData, unsigned short usDataLen, ChatLog &cl)
{
	memset(&cl, 0, sizeof(cl));
	int nSpan = strlen(pData);
	strncpy(cl.PeerName, pData, nSpan);
	pData += nSpan + 1;
	
	nSpan = strlen(pData);
	strncpy(cl.LocalName, pData, nSpan);
	pData += nSpan + 1;

	cl.ChatDirection = *((ChatLog::CHATDIRECT*)pData);
	nSpan = sizeof(cl.ChatDirection);
	pData += nSpan;

	cl.ClientType = *((unsigned char*)pData);
	nSpan = sizeof(cl.ClientType);
	pData += nSpan;

	cl.LogTime = *(SYSTEMTIME*)pData;
	nSpan = sizeof(SYSTEMTIME);
	pData += nSpan;

	strncpy(cl.Chat, pData, strlen(pData));
	return 0;
}
//技能名称12绿色、技能等级技能类型10绿色
void CPlayer::GetSkillInfo(unsigned long ulSkillID, vector<string>& vectSkillProp)
{
	vectSkillProp.clear();
	PPLAYERSKILL pPlayerSkill = m_SkillManager.GetSkillInfo(ulSkillID);
	if (pPlayerSkill != NULL)
	{
		vectSkillProp.push_back(pPlayerSkill->strName); //0
		char szBuf[MAX_PATH];
		sprintf(szBuf, "%d", pPlayerSkill->nLevel);
		vectSkillProp.push_back(szBuf); //1
		sprintf(szBuf, "%d", pPlayerSkill->nType);
		vectSkillProp.push_back(szBuf); //2
		sprintf(szBuf, "%d", pPlayerSkill->nSpLevel);
		vectSkillProp.push_back(szBuf); //3
		sprintf(szBuf, "%d", pPlayerSkill->nAttDistMin);
		vectSkillProp.push_back(szBuf); //4
		sprintf(szBuf, "%d", pPlayerSkill->nAttDistMax);
		vectSkillProp.push_back(szBuf); //5
		sprintf(szBuf, "%d", pPlayerSkill->nRange);
		vectSkillProp.push_back(szBuf); //6
		sprintf(szBuf, "%d", pPlayerSkill->nCDTime);
		vectSkillProp.push_back(szBuf); //7
		sprintf(szBuf, "%d", pPlayerSkill->nCurseTime);
		vectSkillProp.push_back(szBuf); //8
		vectSkillProp.push_back(pPlayerSkill->strNote);//9
		sprintf(szBuf, "%d", pPlayerSkill->nUllageHp);
		vectSkillProp.push_back(szBuf); //10
		sprintf(szBuf, "%d", pPlayerSkill->nUllageMp);
		vectSkillProp.push_back(szBuf); //11
		sprintf(szBuf, "%d", pPlayerSkill->nUllageItem);
		vectSkillProp.push_back(szBuf); //12
		sprintf(szBuf, "%d", pPlayerSkill->nUllageItemNum);
		vectSkillProp.push_back(szBuf); //13
		sprintf(szBuf, "%d", pPlayerSkill->nUllageMoney);
		vectSkillProp.push_back(szBuf); //14
		sprintf(szBuf, "%d", pPlayerSkill->nUllageSp);
		vectSkillProp.push_back(szBuf); //15

	}
	else
	{

	}
}

string CPlayer::GetClassByClassID(unsigned long ulClassID)
{
	const char* ClassDesc[] = 
	{
		"",
		"剑士",
		"刺客",
		"圣骑士",
		"魔剑士",
		"弓箭手",
		"魔法师"
	};
	return ClassDesc[ulClassID];
}

string CPlayer::GetRaceByRaceID(unsigned long ulRaceID)
{
	const char* RaceDesc[] =
	{
		"",
		"人类",
		"艾卡",
		"精灵"
	};
	return RaceDesc[ulRaceID];
}

string CPlayer::GetGender(unsigned long ulGenderID)
{
	const char* GenderDesc[] =
	{
		"",
		"男性",
		"女性"
	};
	return GenderDesc[ulGenderID];
}

unsigned short CPlayer::GetCurRoleNo()
{
	return m_RoleNO;
}

void CPlayer::ChangeChatLogDir(const char *szDir)
{
	m_RecordMng.StopLogger();
	m_RecordMng.StartLogger(szDir, this);
}

void CPlayer::SetFilter(unsigned short usFilter)
{
	m_Filter = usFilter;
}
void CPlayer::ClearLog(int nType)
{
	CMemNode* pMemNode = m_CliSession->PrepareMem();
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pMemNode->GetBufferPrt());
	pMsgHeader->m_Cmd = CLEAR_LOG;
	pMsgHeader->m_DataType = LOCAL_TYPE;
	pMsgHeader->m_DataLen = sizeof(MSGHEADER);
	pMemNode->SetUseSize(pMsgHeader->m_DataLen);
	AddNetMsg(pMemNode);
}

int CPlayer::ProcClearLog(void* pRawData, unsigned short usDataLen)
{
	m_RecordMng.ClearChat();
	return 0;
}

void CPlayer::ReqUserInfo(const char *szRoleName)
{
	//设置消息体
	CGIrcQueryPlayerInfo cgIrcQueryPlayerInfo;
	ZeroMemory(&cgIrcQueryPlayerInfo, sizeof(CGIrcQueryPlayerInfo));
	cgIrcQueryPlayerInfo.nameSize = strlen(szRoleName) + 1;
	strncpy(cgIrcQueryPlayerInfo.playerName, szRoleName, strlen(szRoleName));
	SendPkg(CGIrcQueryPlayerInfo, cgIrcQueryPlayerInfo);
}
int CPlayer::ProcIrcQryUserInfo(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCIrcQueryPlayerInfoRst, pGCIrcQueryPlayerInfoRst, pRawData, usDataLen, m_ProtolTool);
	if (pGCIrcQueryPlayerInfoRst->errorCode == GCIrcQueryPlayerInfoRst::QUERY_SUCCESS)
	{
		PEERUSRINFO* pPeerUsrInfo = new PEERUSRINFO;
		ZeroMemory(pPeerUsrInfo, sizeof(PEERUSRINFO));
		strncpy(pPeerUsrInfo->playerName, pGCIrcQueryPlayerInfoRst->playerName, pGCIrcQueryPlayerInfoRst->nameSize);
		pPeerUsrInfo->playerGender = pGCIrcQueryPlayerInfoRst->playerSex;
		pPeerUsrInfo->mapID = pGCIrcQueryPlayerInfoRst->mapID;
		pPeerUsrInfo->playerLevel = pGCIrcQueryPlayerInfoRst->playerLevel;
		pPeerUsrInfo->playerClass = pGCIrcQueryPlayerInfoRst->playerClass;
		pPeerUsrInfo->playerRace = pGCIrcQueryPlayerInfoRst->playerRace;
		strncpy(pPeerUsrInfo->GuildName, pGCIrcQueryPlayerInfoRst->GuildName, pGCIrcQueryPlayerInfoRst->GuildNameLen);
		strncpy(pPeerUsrInfo->FereName, pGCIrcQueryPlayerInfoRst->FereName, pGCIrcQueryPlayerInfoRst->FereNameLen);
		m_IRCEvent->OnPeerInfo(pPeerUsrInfo);
		delete pPeerUsrInfo;
	}
	return 0;
}

int CPlayer::GetItemInfo(unsigned long ulItemID, vector<string>& vectItemInfo)
{
	CItem* pItem = m_ItemManager.GetItem(ulItemID);
	if (pItem == NULL)
	{
		return 0;
	}
	return pItem->ShowTip(vectItemInfo);
}

int CPlayer::GetSuitInfo(unsigned long ulSuitID, int& nSuitCount, vector<string> &vectSuitName, vector<string> &vectSuitAttr)
{
	CSuit* pSuit = m_ItemManager.GetSuit(ulSuitID);
	if (pSuit == NULL)
	{
		return 0;
	}
	nSuitCount = (int)pSuit->GetItems().size();
	for (int nIndex = 0; nIndex < (int)pSuit->GetItems().size(); nIndex++)
	{
		vectSuitName.push_back(pSuit->GetItems()[nIndex]->Name);
	}
	vectSuitAttr.insert(vectSuitAttr.begin(), pSuit->GetEffects().begin(), pSuit->GetEffects().end());
	return 0;
}

int CPlayer::GetTaskInfo(unsigned long ulTaskID, vector<string>& vectTaskInfo)
{
	CTask* pTask = m_TaskManager.GetTask(ulTaskID);
	if (pTask == NULL)
	{
		return 0;
	}
	vectTaskInfo.push_back(pTask->strName);
	vectTaskInfo.push_back(pTask->strSimpleDesc);
	return 0;
}
bool CPlayer::IsBlackItem(const char* szRoleName)
{
	bool bRet = false;
	for (BLACKITEMMAP::iterator Ite = m_BlackItemMap.begin(); Ite != m_BlackItemMap.end(); Ite++)
	{
		if (stricmp(szRoleName, Ite->second.nickName) == 0)
		{
			bRet = true;
			break;
		}
	}
	return bRet;
}

int CPlayer::Open()
{
	return OpenConnection();
}

int CPlayer::Close()
{
	return LeaveWorld();
}

void CPlayer::CheckTimeOut()
{
	//检查是否有请求超时发生
	REQRESPINFOMAP::iterator Ite = m_ReqRespInfos.begin();
	while (Ite != m_ReqRespInfos.end())
	{
		PREQRESPINFO pReqRespInfo = Ite->second;
		DWORD dwCurTime = timeGetTime();
		int nInternal = dwCurTime - pReqRespInfo->m_ReqTime;
		if (pReqRespInfo->m_TimeOut > 0 && nInternal > pReqRespInfo->m_TimeOut)
		{
			if (pReqRespInfo->m_ReqID == CAQuerySrvState::wCmd || pReqRespInfo->m_ReqID == CARequestGateSrvAddr::wCmd)
			{
				if (m_TrySendTimes > 0)
				{
					//重新发送消息
					m_TrySendTimes--;
					m_ReqRespInfos.erase(Ite++);
					if (pReqRespInfo->m_ReqID == CAQuerySrvState::wCmd)
					{
						QueryAssignSrvState();
					}
					if (pReqRespInfo->m_ReqID == CARequestGateSrvAddr::wCmd)
					{
						QueryGateSrvAddr();
					}
					continue;
				}
				else
				{
					m_IRCEvent->OnIRCError(ERR_LOGIN_TIMEOUT);
					m_ReqRespInfos.erase(Ite++);
				}
			}
			else
			{
				m_IRCEvent->OnIRCError(ERR_LOGIN_TIMEOUT);
				m_ReqRespInfos.erase(Ite++);
			}
		}
		else
		{
			Ite++;
		}
		
	}
}

void CPlayer::SetTimeOut(USHORT usReqID, USHORT usRespID, DWORD dwTimeOut)
{
	REQRESPINFO* pReqRespInfo = new REQRESPINFO(usReqID, usRespID, timeGetTime());
	pReqRespInfo->m_TimeOut = dwTimeOut;
	REQRESPINFOMAP::iterator IteMap = m_ReqRespInfos.find(usRespID);
	if (IteMap != m_ReqRespInfos.end())
	{
		delete IteMap->second;
		m_ReqRespInfos.erase(IteMap);
	}
	m_ReqRespInfos[usRespID] = pReqRespInfo;
}

void CPlayer::CalcTimeOut(unsigned short usCmdID)
{
	REQRESPINFOMAP::iterator Ite = m_ReqRespInfos.find(usCmdID);
	if (Ite != m_ReqRespInfos.end())
	{
		PREQRESPINFO pReqRespInfo = Ite->second;
		delete pReqRespInfo;
		m_ReqRespInfos.erase(Ite);
	}
}

int CPlayer::ProcUnionInfo(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCQueryUnionBasicResult, pGCQueryUnionBasicResult, pRawData, usDataLen, m_ProtolTool);
	Union_I* pUnion = new Union_I;
	memcpy(pUnion, &(pGCQueryUnionBasicResult->_union), sizeof(Union_I));
	m_IRCEvent->OnGuild(pUnion);
	delete pUnion;
	return 0;
}

int CPlayer::ProcUnionDestroy(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCUnionDestroyedNotify, pGCUnionDestroyedNotify, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnGuildDismiss();
	return 0;
}

int CPlayer::ProcDelUnionMember(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCRemoveUnionMemberNotify, pGCRemoveUnionMemberNotify, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnGuildDelMember(pGCRemoveUnionMemberNotify->flag, pGCRemoveUnionMemberNotify->memberRemoved);
	return 0;
}

int CPlayer::ProcForbidUnionSpeek(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCForbidUnionSpeekNtf, pGCForbidUnionSpeekNtf, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnGuildBanChat(pGCForbidUnionSpeekNtf->name);
	return 0;
}

int CPlayer::ProcUnionMembers(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCQueryUnionMembersResult, pGCQueryUnionMembersResult, pRawData, usDataLen, m_ProtolTool);
	for (int nIndex = 0; nIndex < pGCQueryUnionMembersResult->members.count; nIndex++)
	{
		UnionMember_I* pUnionMember = new UnionMember_I;
		memcpy(pUnionMember, &(pGCQueryUnionMembersResult->members.members[nIndex]), sizeof(UnionMember_I));
		m_IRCEvent->OnGuildMember(pUnionMember);
		delete pUnionMember;
	}
	return 0;
}
//增加战盟成员通知
int CPlayer::ProcAddUnionMember(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCAddUnionMemberNotify, pGCAddUnionMemberNotify, pRawData, usDataLen, m_ProtolTool);
	UnionMember_I* pGuildMem = new UnionMember_I;
	memcpy(pGuildMem, &(pGCAddUnionMemberNotify->memberAdded), sizeof(UnionMember_I));
	m_IRCEvent->OnGuildAddMem(pGuildMem);
	delete pGuildMem;
	return 0;
}

int CPlayer::EnterWorld(unsigned int uiRoleID)
{
	map<unsigned short, PROLE>::iterator Ite = m_RoleInfo.find(uiRoleID);
	if (Ite != m_RoleInfo.end())
	{
		PROLE pRole = Ite->second;
		m_RoleName = pRole->playerInfo.szNickName;
	}
	return SelectRole(uiRoleID);
}

int CPlayer::QryUnionMember()
{
	CGQueryUnionMembersRequest cgQueryUnionMembersRequest;
	SendPkg(CGQueryUnionMembersRequest, cgQueryUnionMembersRequest);
	return 0;
}

int CPlayer::OnLoginRes(int errNo )
{
	switch(errNo)
	{
	case GCPlayerLogin::ISLOGINOK_SUCCESS:
		{
			m_UserStage = PS_LOGINED;
		}
		break;
	case GCPlayerLogin::ISLOGINOK_FAILED_ACCOUNTNOTEXIST:
	case GCPlayerLogin::ISLOGINOK_FAILED_PWDWRONG:
		{
			m_IRCEvent->OnIRCError(ERR_INVALID_USER);
		}
		break;
	case GCPlayerLogin::ISLOGINOK_FAILED_ONLINE:
		{
			m_IRCEvent->OnIRCError(ERR_USER_ONLINE);
		}
		break;
	default:
		break;
	}
	return 0;
}

int CPlayer::OnTestAccountsRes(int errNo )
{

	return 0;
}

int CPlayer::OnCreateAccountsRes(int errNo )
{

	return 0;
}

int CPlayer::OnPkgRcved( unsigned short wCmd, const char* pPkg, const DWORD dwLen )
{
	//MessageBox(NULL,"收到服务器发来的消息",NULL,NULL);

	HANDLERMAP::iterator Ite = m_Handler.find(wCmd);
	if (Ite == m_Handler.end())
	{
		//CRobotManager::m_AppLogger.WriteLog("GameRobot.log", "Account:%s, msg id %d can not found handler.\n", m_Account.c_str(), pMsgHeader->m_Cmd);
	}
	else
	{
		(this->*Ite->second.handler)((void*)pPkg, dwLen);
	}
	
	return 0;
}

int CPlayer::OnError( const int& errNo, const DWORD dwPkgID )
{
	m_IRCEvent->OnIRCError(ERR_CONNECTION_BREAKDOWN);
	return 0;
}

BOOL CPlayer::GetCreateCliNetModFun(char* pDllData)
{
	if ( NULL != m_hMemoryModule )
	{
		MemoryFreeLibrary(m_hMemoryModule);
	}
	m_hMemoryModule  = MemoryLoadLibrary(pDllData);
	if( m_hMemoryModule == NULL)
	{
		return FALSE;
	}

	m_pfnGetMuxCliNetMod = (MUX_CLIMOD::PFN_GetMuxCliNetMod)MemoryGetProcAddress( m_hMemoryModule, "GetMuxCliNetMod");
	if ( NULL == m_pfnGetMuxCliNetMod )
	{
		return FALSE;
	}

	return TRUE;
}

int CPlayer::ProcUnionMemOnOffline(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCUnionMemberOnOffLineNtf, pGCUnionMemberOnOffLineNtf, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnGuildMemOffOnline(pGCUnionMemberOnOffLineNtf->uiid, pGCUnionMemberOnOffLineNtf->flag);
	return 0;
}

int CPlayer::ProcUnionMemTitleNty(void * pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCUnionMemberTitleNtf, pGCUnionMemberTitleNtf, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnGuildMemberTitleChange(pGCUnionMemberTitleNtf->memberName, pGCUnionMemberTitleNtf->title);
	return 0;
}

int CPlayer::ProcUnionPosUdpNty(void* pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCUpdateUnionPosCfgNtf, pGCUpdateUnionPosCfgNtf, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnGuildPosUpdateNty(pGCUpdateUnionPosCfgNtf->posListLen, (UnionPosRight_I*)pGCUpdateUnionPosCfgNtf->posList);
	return 0;
}

int CPlayer::ProcUnionMemAdvPos(void * pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCAdvanceUnionMemberPosNtf, pGCAdvanceUnionMemberPosNtf, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnGuildMemPosChange(pGCAdvanceUnionMemberPosNtf->memberName, pGCAdvanceUnionMemberPosNtf->posSeq);
	return 0;
}

int CPlayer::ProcUnionMemRedPos(void * pRawData, unsigned short usDataLen)
{
	DealPlayerPkgPre(GCReduceUnionMemberPosNtf, pGCReduceUnionMemberPosNtf, pRawData, usDataLen, m_ProtolTool);
	m_IRCEvent->OnGuildMemPosChange(pGCReduceUnionMemberPosNtf->memberName, pGCReduceUnionMemberPosNtf->posSeq);
	return 0;
}

int CPlayer::OnChatRecord(CChatRecordMng::ChatLog* pChatLog)
{
	switch(pChatLog->ClientType)
	{
	case CT_PRIVATE_CHAT:
	case CT_PRIVATE_IRC_CHAT:
		{
			m_IRCEvent->OnReadPrivateMsg((ChatLog*)pChatLog);
		}
		break;
	case CT_WORLD_CHAT:
	case CT_WORLD_IRC_CHAT:
		{
			m_IRCEvent->OnReadWorldMsg((ChatLog*)pChatLog);
		}
		break;
	case CT_UNION_CHAT:
	case CT_UNION_IRC_CHAT:
		{
			m_IRCEvent->OnReadGuildMsg((ChatLog*)pChatLog);
		}
		break;
	case CT_SCOPE_CHAT:
		{
			m_IRCEvent->OnReadCommonMsg((ChatLog*)pChatLog);
		}
		break;
	case CT_TEAM_CHAT:
		{
			m_IRCEvent->OnReadTeamMsg((ChatLog*)pChatLog);
		}
		break;
	case CChatRecordMng::CT_CUST_GROUP:
		{
			m_IRCEvent->OnReadChatGroupMsg((ChatLog*)pChatLog);
		}
		break;
	case CT_SYS_EVENT_NOTI:
	case CT_SYS_WARNING:
		{
			m_IRCEvent->OnReadSystemMsg((ChatLog*)pChatLog);
		}
		break;
	default:
		break;
	}
	
	return 0;
}

int CPlayer::ReadSuitInfo(const char *szSuitFileName)
{
	m_ItemManager.LoadSuitData(szSuitFileName);
	return 0;
}

int CPlayer::ReadAdditionAttr(const char* szFileName)
{
	m_ItemManager.LoadAddAttrData(szFileName);
	return 0;
}

int CPlayer::ReadItemLevelUpInfo(const char* szFileName)
{
	m_ItemManager.LoadItemLevelUpData(szFileName);
	return 0;
}

int CPlayer::GetItemAdditionInfo(unsigned long ulItemAddID, std::vector<string> &vectItemAdditionAttr)
{
	CEquipAddInfo* pEquipAddInfo = m_ItemManager.GetEquipAddInfo(ulItemAddID);
	if (pEquipAddInfo)
	{
		pEquipAddInfo->GetProp(vectItemAdditionAttr);
	}
	return 0;
}

int CPlayer::GetItemLevelUpInfo(unsigned long ulLevelUpID, std::vector<string> &vectLevelUpInfo)
{
	CItemLevelUp* pItemLevelUp = m_ItemManager.GetItemLevelUp(ulLevelUpID);
	if (pItemLevelUp)
	{
		pItemLevelUp->GetProp(vectLevelUpInfo);
	}
	return 0;
}

int CPlayer::ReadCommonMsg()
{
	m_RecordMng.ReadChatLog((CChatRecordMng::CHAT_TYPE)CT_SCOPE_CHAT);	
	return 0;
}

int CPlayer::ReadGroupMsg()
{
	m_RecordMng.ReadChatLog((CChatRecordMng::CHAT_TYPE)CT_TEAM_CHAT);	
	return 0;
}

int CPlayer::ReadChatGroupMsg()
{
	m_RecordMng.ReadChatLog(CChatRecordMng::CT_CUST_GROUP);	
	return 0;
}

int CPlayer::ReadSystermMsg()
{
	m_RecordMng.ReadChatLog((CChatRecordMng::CHAT_TYPE)CT_SYS_EVENT_NOTI);	
	return 0;
}



