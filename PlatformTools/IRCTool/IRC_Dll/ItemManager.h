﻿/** @file ItemManager.h 
@brief 管理器
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-07-09
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef ITEMMANAGER_H
#define ITEMMANAGER_H

enum ITEM_TYPE
{
	//可装备
	TYPE_EQUIP,
	//可消耗
	TYPE_CONSUME,
	//普通物品
	TYPE_NORMAL,
	//技能书
	TYPE_SKILLBOOK,
	//任务道具
	TYPE_TASK,
	//功能道具
	TYPE_FUNC,
	//价值物道具
	TYPE_VALUEOBJ,
	//蓝图道具
	TYPE_BLUEPRINT,
	//时装道具
	TYPE_CLOTHES,
};
//消耗类型
enum ECONSUME_TYPE
{
	ECONSUME_DEATH, //死亡
	ECONSUME_USE, //使用次数
	ECONSUME_DAMAGE, //受伤害次数
	ECONSUME_SKILL, //使用技能次数
	ECONSUME_TIME, //时间
};

enum MONEY_TYPE
{
	MONEY_GAME,
	MONEY_RMB,
	MONEY_ITEM,
};

enum ITEM_MASSLEVEL
{
	E_GRAY,//灰色
	E_WHITE,//白色
	E_GREEN,//绿色
	E_BLUE,//蓝色
	E_PURPLE,//紫色
	E_ORANGE,//橙色
	E_GOLD = 9, //暗金
};

//装备类型
enum ITEM_GEARARM_TYPE
{
	E_HEAD = 0x0,//头部装备
	E_BODY,//身体装备
	E_HAND,//手部装备
	E_FOOT,//脚部装备
	E_SHOULDER,//肩部装备
	E_BACK,//背部装备
	E_EAR,//耳环装备
	E_NECKLACE,//项链装备
	E_BRACELET,//手镯装备
	E_FINGER,//手指装备
	E_FRISTHAND,//主手装备
	E_SECONDHAND,//副手装备
	E_DOUBLEHAND,//双手武器
	E_DRIVE,//坐骑装备
};

//所属种族
enum ITEM_NATION
{
	E_HUMAN = 1,//人类
	E_AIKA,//艾卡
	E_FAIRY,//精灵 
};

//所属职业
enum ITEM_CLASS
{
	E_SWORDMAN = 1,//剑士
	E_ASSASSIN,//刺客
	E_GOLDRIDER,//圣骑士
	E_DEVILSWORDMAN,//魔剑士
	E_ARCHER,//弓箭手
	E_ENCHANTER,//法师
};

enum ITEM_BIND_TYPE
{
	E_EQUIP_BIND, //装备绑定已绑定
	E_EQUIP_UNBIND, //装备绑定未绑定
	E_PICK_BIND, //拾取绑定
	E_CANNOT_BIND, //不可被绑定
};

enum ITEM_BIND_LEVEL
{
	E_CANNOT_DELETE, //不可被删除
	E_CANNOT_TRADEWITHPLAYER, //不可与玩家交易
	E_CANNOT_TRADEWITHNPC, //不可与NPC交易
	E_CANNOT_DROPONDIE, //死亡后不掉落
};


///所有道具的公有属性
class CIntList
{
public:
	void Set(string strString);
	vector<int>& GetInts();
	bool IsIn(int iData) const;
	string GetRawValue()
	{
		return m_RawValue;
	}
private:
	vector<int> m_vecInts;
	string m_RawValue;
};

class CItemManager;

class CItem
{
public:
	explicit CItem(CItemManager* pItemManager, const TiXmlElement* pItemXml);
public:
	CItemManager* m_ItemManager;
public:
	int			ItemID; //道具ID
	ITEM_TYPE   ItemType; //道具类型
	CIntList	Race; //种族
	CIntList	Class; //职业
	string		Name; //道具名字
	int			MaxWear; //最大耐久度
	int			Price; //价格
	int			Level; //等级
	int			MaxAmount; //最大数量
	ITEM_MASSLEVEL	MassLevel; //质量等级
	int			CannotDepot; //是否可存仓库 0 可存 1 否
	ITEM_BIND_TYPE	BindEquip; //绑定方式
	CIntList	BindEquipList; //绑定内容
	int			Uniquely; //是否装备唯一
	CIntList	ConsumeWear; //消耗类型
	string		Text; //描述
protected:
	virtual int LoadFromXml(const TiXmlElement* pItemXml);
public:
	virtual int ShowTip(vector<string>& vectItemInfo);
	string GetClassByClassID(unsigned long ulClassID);
	string GetRaceByRaceID(unsigned long ulRaceID);
	string GetGearArmTypeString(int nGearArmType);
};

//附加属性
class CEquipAddInfo
{
public:
	void LoadFromXml(const TiXmlElement* pElem);
public:
	void GetProp(vector<string>& vectProp);
public:
	int nAttackPhyMin; //最小物理攻击力
	int nAttackPhyMax; //最大物理攻击力
	int nAttackMagMin; //最小魔法攻击力
	int nAttackMagMax; //最大魔法攻击力
	int nAddHP; 
	int nAddMP;
	int nAddPhysicsAttack; //物理攻击
	int nAddMagicAttack; //魔法攻击
	int nAddPhysicsDef;  //物理防御
	int nAddMagicDef; //魔法防御
	int nAddPhysicsHit; //物理命中
	int nAddMagicHit; //魔法命中
	int nAddPhysicsCritical; //物理暴击（几率）等级
	int nAddMagicCritical; //魔法暴击（几率）等级
	int nAddPhysicsAttackPercent; //物理攻击百分比
	int nAddMagicAttackPercent; //魔法攻击百分比
	int nAddPhysicsCriticalDamage; //附加物理暴击伤害
	int nAddMagicCriticalDamage; //附加魔法暴击伤害
	int nAddPhysicsDefendPercent; //物理防御百分比
	int nAddMagicDefendPercent; //魔法防御百分比
	int nAddPhysicsCriticalDerate; //减免物理暴击（几率）等级
	int nAddMagicCriticalDerate; //减免魔法暴击（几率）等级
	int nAddPhysicsCriticalDamageDerate; //减免物理暴击伤害
	int nAddMagicCriticalDamageDerate; //减免魔法暴击伤害
	int nAddPhysicsJouk; //物理闪避等级
	int nAddMagicJouk; //魔法闪避等级
	int nAddPhysicsDeratePercent; //物理伤害减免百分比
	int nAddMagicDeratePercent; //魔法伤害减免百分比
	int nAddStun; //击晕（几率）等级
	int nAddTie; //束缚（几率）等级
	int nAddAntistun; //抗晕（几率）等级
	int nAddAntitie; //抗束缚（几率）等级
	int nAddPhysicsAttackAdd; //20%产生额外物理伤害
	int nAddMagicAttackAdd; //20%产生额外魔法伤害
	int nAddPhysicsRift; //物理穿透等级
	int nAddMagicRift; //魔法穿透等级
	int nAddSpeed; //移动速度
};

//装备升级属性表
class CItemLevelUp
{
public:
	void LoadFromXml(const TiXmlElement* pItemLevelXml);
public:
	void GetProp(vector<string>& vectProp);
public:
	int ID;
	int AttackPhyMin; //物理攻击下限
	int AttackPhyMax; //物理攻击上限
	int AttackMagMin; //魔法攻击下限
	int AttackMagMax; //魔法攻击上限
	int PhysicsAttack; //物理攻击
	int MagicAttack;   //魔法攻击
	int PhysicsDefend; //物理防御
	int MagicDefend; //魔法防御
};
//装备
class CEquipItem: public CItem
{
public:
	explicit CEquipItem(CItemManager* pItemManager, const TiXmlElement* pItemXml);
protected:
	virtual int LoadFromXml(const TiXmlElement* pItemXml);
public:
	ITEM_GEARARM_TYPE iGearArmType; //装备类型
	int iSuitID; //套装编号
	int nAttackPhyMin; //最小物理攻击力
	int nAttackPhyMax; //最大物理攻击力
	int nAttackMagMin; //最小魔法攻击力
	int nAttackMagMax; //最大魔法攻击力
	int nAddHP;
	int nAddMP;
	int nBrand; //品牌号，在显示装备升级时需要 需要和升级的等级合并产生一个id，用这个id在道具升级属性表中查找
	int nAddPhysicsAttack; //物攻
	int nAddMagicAttack; //魔攻
	int nAddPhysicsDef;  //物防
	int nAddMagicDef; //魔防
	int nAddPhysicsHit; //物理命中
	int nAddMagicHit; //魔法命中
	int nAddPhysicsCritical; //物理会心一击等级
	int nAddMagicCritical; //魔法会心一击等级
	int nAddPhysicsAttackPercent; //物理攻击百分比
	int nAddMagicAttackPercent; //魔法攻击百分比
	int nAddPhysicsCriticalDamage; //物理会心一击伤害
	int nAddMagicCriticalDamage; //魔法会心一击伤害
	int nAddPhysicsDefendPercent; //物理防御百分比
	int nAddMagicDefendPercent; //魔法防御百分比
	int nAddPhysicsCriticalDerate; //减免物理会心一击等级
	int nAddMagicCriticalDerate; //减免魔法会心一击等级
	int nAddPhysicsCriticalDamageDerate; //减免物理会心一击伤害
	int nAddMagicCriticalDamageDerate; //减免魔法会心一击伤害
	int nAddPhysicsJouk; //物理闪避等级
	int nAddMagicJouk; //魔法闪避等级
	int nAddPhysicsDeratePercent; //物理伤害减免百分比
	int nAddMagicDeratePercent; //魔法伤害减免百分比
	int nAddStun; //击晕（几率）等级
	int nAddTie; //束缚（几率）等级
	int nAddAntistun; //抗晕（几率）等级
	int nAddAntitie; //抗束缚（几率）等级
	int nAddPhysicsAttackAdd; //20%几率造成额外物理伤害
	int nAddMagicAttackAdd; //20%几率造成额外魔法伤害
	int nAddPhysicsRift; //物理穿透等级
	int nAddMagicRift; //魔法穿透等级
	int nAddSpeed; //移动速度
	int AddID; //追加编号
public:
	virtual int ShowTip(vector<string>& vectItemInfo);
};

//套装
class CSuit
{
public:
	void LoadFromXml(const TiXmlElement* pSuitXml);
	//添加该套装所需的装备
	void Add(const CItem* pItem);
	//得到所需要的装备
	vector<const CItem*>& GetItems()
	{
		return Items;
	}
	void BuildEffect();
	vector<string>& GetEffects()
	{
		return Effects;
	}
private:
	vector<const CItem*> Items;
	vector<string> Effects;
public:
	int ID; 
	int Head; //头
	int Body; //身
	int Hand; //手
	int Foot; //脚
	int Shoulder; //肩
	int Back; //背
	int Torque; //项链
	int Ring1; //戒指1
	int Ring2; //戒指2
	int Earring1;  //耳环1
	int Earring2; //耳环2
	int Bangle1; //手镯1 
	int Bangle2; //手镯2
	int Weapon1; //主手
	int Weapon2; //副手
	int Weapon3; //双手
	int Mounts; //座骑
	int AttackPhyMin; //最小物理攻击力
	int AttackPhyMax; //最大物理攻击力
	int AttackMagMin; //最小魔法攻击力
	int AttackMagMax; //最大魔法攻击力
	int Hp; 
	int Mp; 
	int PhysicsAttack; //物理攻击
	int MagicAttack;   //魔法攻击
	int PhysicsDefend; //物理防御
	int MagicDefend;   //魔法防御
	int PhysicsHit;    //物理命中
	int MagicHit;      //魔法命中
	int PhysicsCritical; //物理暴击等级
	int MagicCritical;   //魔法暴击等级
	int PhysicsAttackPercent; //物理攻击百分比
	int MagicAttackPercent;   //魔法攻击百分比
	int PhysicsCriticalDamage; //附加物理暴击伤害
	int MagicCriticalDamage;   //附加魔法暴击伤害
	int PhysicsDefendPercent;  //物理防御百分比
	int MagicDefendPercent;    //魔法防御百分比
	int PhysicsCriticalDerate; //减免物理暴击等级
	int MagicCriticalDerate; //减免魔法暴击等级
	int PhysicsCriticalDamageDerate; //减免物理暴击伤害
	int MagicCriticalDamageDerate; //减免魔法暴击伤害
	int PhysicsJouk; //物理闪避等级
	int MagicJouk;  //魔法闪避等级
	int PhysicsDeratePercent; //物理伤害减免百分比
	int MagicDeratePercent; //魔法伤害减免百分比
	int Stun; //击晕等级
	int Tie; //束缚等级
	int Antistun; //抗晕等级
	int Antitie; //抗束缚等级
	int PhysicsAttackAdd; //20%产生额外物理伤害
	int MagicAttackAdd; //20%产生额外魔法伤害
	int PhysicsRift; //物理穿透等级
	int MagicRift; //魔法穿透等级
	int AddSpeed; //移动速度
};

class CItemManager
{
public:
	CItemManager(void);
	virtual ~CItemManager(void);
public:
	CItem* GetItem(unsigned long ulItemID);
public:
	int LoadItemData(const char* szXmlFile);
	int LoadSuitData(const char* szXmlFile);
	int LoadAddAttrData(const char* szXmlFile);
	int LoadItemLevelUpData(const char* szXmlFile);
	CEquipAddInfo* GetEquipAddInfo(int nID);
	CSuit* GetSuit(int nSuitID);
	CItemLevelUp* GetItemLevelUp(int nItemLevelUpID);
private:
	map<unsigned long, CItem*> m_Items;
	map<int, CEquipAddInfo> m_ItemAddInfoMap;
	map<int, CSuit> m_SuitMap;
	map<int, CItemLevelUp> m_ItemLevelUpMap;
};

#endif