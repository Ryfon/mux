﻿#include "stdafx.h"
#include "AppException.h"

CAppException::CAppException(const string& strWhere, const string& strMsg)
: m_Where(strWhere), m_Msg(strMsg)
{

}

CAppException::~CAppException(void)
{

}

string CAppException::GetMsg() const
{
	return m_Msg;
}

string CAppException::GetWhere() const
{
	return m_Where;
}


