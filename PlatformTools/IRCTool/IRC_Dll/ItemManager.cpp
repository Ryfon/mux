﻿#include "stdafx.h"
#include "ItemManager.h"

void CIntList::Set(string strString)
{
	m_RawValue = strString;
	m_vecInts.clear();
	for (int nIndex = 0, nCharPos = 0; nIndex < (int)strString.size(); nIndex++)
	{
		if (strString[nIndex] == ',')
		{
			string strNumber = strString.substr(nCharPos, nIndex);
			m_vecInts.push_back(atoi(strNumber.c_str()));
			nCharPos = nIndex + 1;
		}
		else
		{
			if (nIndex == (int)strString.size() - 1)
			{
				string strNumber = strString.substr(nCharPos, strString.size());
				m_vecInts.push_back(atoi((strNumber.c_str())));
			}
		}
	}
}

vector<int>& CIntList::GetInts()
{
	return m_vecInts;
}

bool CIntList::IsIn(int iData) const
{
	vector<int>::const_iterator Ite = find(m_vecInts.begin(), m_vecInts.end(), iData);
	return Ite != m_vecInts.end();
}

CItem::CItem(CItemManager* pItemManager, const TiXmlElement* pItemXml)
: m_ItemManager(pItemManager)
{
	LoadFromXml(pItemXml);
}

int CItem::LoadFromXml(const TiXmlElement* pItemXml)
{
	pItemXml->Attribute("ItemID", (int*)&ItemID);
	pItemXml->Attribute("ItemType", (int*)&ItemType);
	Name = pItemXml->Attribute("Name");
	pItemXml->Attribute("MaxWear", &MaxWear);
	pItemXml->Attribute("Price", &Price);
	pItemXml->Attribute("Level", &Level);
	pItemXml->Attribute("MaxAmount", &MaxAmount);
	pItemXml->Attribute("MassLevel", (int*)&MassLevel);
	pItemXml->Attribute("CannotDepot", &CannotDepot);
	pItemXml->Attribute("BindEquip", (int*)&BindEquip);
	pItemXml->Attribute("Uniquely", &Uniquely);
	Text = pItemXml->Attribute("Text");
	
	if (pItemXml->Attribute("Race"))
	{
		Race.Set(pItemXml->Attribute("Race"));
	}

	if (pItemXml->Attribute("Class"))
	{
		Class.Set(pItemXml->Attribute("Class"));
	}

	if (pItemXml->Attribute("BindEquipList"))
	{
		BindEquipList.Set(pItemXml->Attribute("BindEquipList"));
	}

	if (pItemXml->Attribute("ConsumeWear"))
	{
		ConsumeWear.Set(pItemXml->Attribute("ConsumeWear"));
	}

	return 0;
}


int CItem::ShowTip(vector<string>& vectItemInfo)
{
	vectItemInfo.push_back(Name); //0 道具名称
	char szTmp[MAX_PATH];
	sprintf(szTmp, "%d", ItemType);
	vectItemInfo.push_back(szTmp); // 1 道具类型
	vectItemInfo.push_back(Race.GetRawValue()); //2 种族
	vectItemInfo.push_back(Class.GetRawValue()); //3 职业
	sprintf(szTmp, "%d", MaxWear);
	vectItemInfo.push_back(szTmp); //4 最大耐久度
	sprintf(szTmp, "%d", Price);
	vectItemInfo.push_back(szTmp); // 5 价格
	sprintf(szTmp, "%d", Level);
	vectItemInfo.push_back(szTmp); // 6 需要等级
	sprintf(szTmp, "%d", MaxAmount);
	vectItemInfo.push_back(szTmp); // 7 最大堆叠数量
	sprintf(szTmp, "%d", MassLevel);
	vectItemInfo.push_back(szTmp); // 8 质量等级
	sprintf(szTmp, "%d", CannotDepot);
	vectItemInfo.push_back(szTmp); //9 是否可存仓库
	sprintf(szTmp, "%d", BindEquip);
	vectItemInfo.push_back(szTmp); // 10 绑定方式
	vectItemInfo.push_back(BindEquipList.GetRawValue()); //11绑定内容
	vectItemInfo.push_back(ConsumeWear.GetRawValue()); //12 消耗类型
	vectItemInfo.push_back(Text); //13 道具描述
	return ItemType;
}
string CItem::GetClassByClassID(unsigned long ulClassID)
{
	const char* ClassDesc[] = 
	{
		"",
		"剑士",
		"刺客",
		"圣骑士",
		"魔剑士",
		"弓箭手",
		"魔法师"
	};
	return ClassDesc[ulClassID];
}
string CItem::GetRaceByRaceID(unsigned long ulRaceID)
{
	const char* RaceDesc[] =
	{
		"",
		"人类",
		"艾卡",
		"精灵"
	};
	return RaceDesc[ulRaceID];
}

string CItem::GetGearArmTypeString(int nGearArmType)
{
	const char* GearTypeDesc[] = 
	{
		"头盔",
		"护胸",
		"手套",
		"鞋子",
		"护肩",
		"披风",
		"耳环",
		"项链",
		"手镯",
		"戒指",
		"主手",
		"副手",
		"主手",
		"坐骑"
	};

	return GearTypeDesc[nGearArmType];
}

void CEquipAddInfo::LoadFromXml(const TiXmlElement* pElem)
{
	pElem->Attribute("Hp", &nAddHP);
	pElem->Attribute("Mp", &nAddMP);
	pElem->Attribute("PhysicsAttack", &nAddPhysicsAttack);
	pElem->Attribute("MagicAttack", &nAddMagicAttack);
	pElem->Attribute("PhysicsDefend", &nAddPhysicsDef);
	pElem->Attribute("MagicDefend", &nAddMagicDef);
	pElem->Attribute("PhysicsHit", &nAddPhysicsHit);
	pElem->Attribute("MagicHit", &nAddMagicHit);
	pElem->Attribute("PhysicsCritical", &nAddPhysicsCritical);
	pElem->Attribute("MagicCritical", &nAddMagicCritical);
	pElem->Attribute("PhysicsAttackPercent", &nAddPhysicsAttackPercent);
	pElem->Attribute("MagicAttackPercent", &nAddMagicAttackPercent);
	pElem->Attribute("PhysicsCriticalDamage", &nAddPhysicsCriticalDamage);
	pElem->Attribute("MagicCriticalDamage", &nAddMagicCriticalDamage);
	pElem->Attribute("PhysicsDefendPercent", &nAddPhysicsDefendPercent);
	pElem->Attribute("MagicDefendPercent", &nAddMagicDefendPercent);
	pElem->Attribute("PhysicsCriticalDerate", &nAddPhysicsCriticalDerate);
	pElem->Attribute("MagicCriticalDerate", &nAddMagicCriticalDerate);
	pElem->Attribute("PhysicsCriticalDamageDerate", &nAddPhysicsCriticalDamageDerate);
	pElem->Attribute("MagicCriticalDamageDerate", &nAddMagicCriticalDamageDerate);
	pElem->Attribute("PhysicsJouk", &nAddPhysicsJouk);
	pElem->Attribute("MagicJouk", &nAddMagicJouk);
	pElem->Attribute("PhysicsDeratePercent", &nAddPhysicsDeratePercent);
	pElem->Attribute("MagicDeratePercent", &nAddMagicDeratePercent);
	pElem->Attribute("Stun", &nAddStun);
	pElem->Attribute("Tie", &nAddTie);
	pElem->Attribute("Antistun", &nAddAntistun);
	pElem->Attribute("Antitie", &nAddAntitie);
	pElem->Attribute("PhysicsAttackAdd", &nAddPhysicsAttackAdd);
	pElem->Attribute("MagicAttackAdd", &nAddMagicAttackAdd);
	pElem->Attribute("PhysicsRift", &nAddPhysicsRift);
	pElem->Attribute("MagicRift", &nAddMagicRift);
	pElem->Attribute("AttackPhyMin", &nAttackPhyMin);
	pElem->Attribute("AttackPhyMax", &nAttackPhyMax);
	pElem->Attribute("AttackMagMin", &nAttackMagMin);
	pElem->Attribute("AttackMagMax", &nAttackMagMax);
	pElem->Attribute("AddSpeed", &nAddSpeed);
}

void CEquipAddInfo::GetProp(vector<string> &vectProp)
{
	char szTmp[MAX_PATH];
	sprintf(szTmp, "%d", nAttackPhyMin);
	vectProp.push_back(szTmp); //0
	sprintf(szTmp, "%d", nAttackPhyMax);
	vectProp.push_back(szTmp); //1
	sprintf(szTmp, "%d", nAttackMagMin);
	vectProp.push_back(szTmp); //2
	sprintf(szTmp, "%d", nAttackMagMax);
	vectProp.push_back(szTmp); //3
	sprintf(szTmp, "%d", nAddHP);
	vectProp.push_back(szTmp); //4
	sprintf(szTmp, "%d", nAddMP);
	vectProp.push_back(szTmp); //5
	sprintf(szTmp, "%d", nAddPhysicsAttack);
	vectProp.push_back(szTmp); //6
	sprintf(szTmp, "%d", nAddMagicAttack);
	vectProp.push_back(szTmp); //7
	sprintf(szTmp, "%d", nAddPhysicsDef);
	vectProp.push_back(szTmp); //8
	sprintf(szTmp, "%d", nAddMagicDef);
	vectProp.push_back(szTmp); //9
	sprintf(szTmp, "%d", nAddPhysicsHit);
	vectProp.push_back(szTmp); //10
	sprintf(szTmp, "%d", nAddMagicHit);
	vectProp.push_back(szTmp); //11
	sprintf(szTmp, "%d", nAddPhysicsCritical);
	vectProp.push_back(szTmp); //12
	sprintf(szTmp, "%d", nAddMagicCritical);
	vectProp.push_back(szTmp); //13
	sprintf(szTmp, "%d", nAddPhysicsAttackPercent);
	vectProp.push_back(szTmp); //14
	sprintf(szTmp, "%d", nAddMagicAttackPercent);
	vectProp.push_back(szTmp); //15
	sprintf(szTmp, "%d", nAddPhysicsCriticalDamage);
	vectProp.push_back(szTmp); //16
	sprintf(szTmp, "%d", nAddMagicCriticalDamage);
	vectProp.push_back(szTmp); //17
	sprintf(szTmp, "%d", nAddPhysicsDefendPercent);
	vectProp.push_back(szTmp); //18
	sprintf(szTmp, "%d", nAddMagicDefendPercent);
	vectProp.push_back(szTmp); //19
	sprintf(szTmp, "%d", nAddPhysicsCriticalDerate);
	vectProp.push_back(szTmp); //20
	sprintf(szTmp, "%d", nAddMagicCriticalDerate);
	vectProp.push_back(szTmp); //21
	sprintf(szTmp, "%d", nAddPhysicsCriticalDamageDerate);
	vectProp.push_back(szTmp); //22
	sprintf(szTmp, "%d", nAddMagicCriticalDamageDerate);
	vectProp.push_back(szTmp); //23
	sprintf(szTmp, "%d", nAddPhysicsJouk);
	vectProp.push_back(szTmp); //24
	sprintf(szTmp, "%d", nAddMagicJouk);
	vectProp.push_back(szTmp); //25
	sprintf(szTmp, "%d", nAddPhysicsDeratePercent);
	vectProp.push_back(szTmp); //26
	sprintf(szTmp, "%d", nAddMagicDeratePercent);
	vectProp.push_back(szTmp); //27
	sprintf(szTmp, "%d", nAddStun);
	vectProp.push_back(szTmp); //28
	sprintf(szTmp, "%d", nAddTie);
	vectProp.push_back(szTmp); //29
	sprintf(szTmp, "%d", nAddAntistun);
	vectProp.push_back(szTmp); //30
	sprintf(szTmp, "%d", nAddAntitie);
	vectProp.push_back(szTmp); //31
	sprintf(szTmp, "%d", nAddPhysicsAttackAdd);
	vectProp.push_back(szTmp); //32
	sprintf(szTmp, "%d", nAddMagicAttackAdd);
	vectProp.push_back(szTmp); //33
	sprintf(szTmp, "%d", nAddPhysicsRift);
	vectProp.push_back(szTmp); //34
	sprintf(szTmp, "%d", nAddMagicRift);
	vectProp.push_back(szTmp); //35
	sprintf(szTmp, "%d", nAddSpeed);
	vectProp.push_back(szTmp); //36
}

CEquipItem::CEquipItem(CItemManager* pItemManager, const TiXmlElement* pItemXml)
: CItem(pItemManager, pItemXml)
{
	LoadFromXml(pItemXml);
}

int CEquipItem::LoadFromXml(const TiXmlElement* pItemXml)
{
	pItemXml->Attribute("GearArmType", (int*)&iGearArmType);
	pItemXml->Attribute("SuitId", &iSuitID);
	pItemXml->Attribute("AddId", &AddID);
	//加载附加属性
	pItemXml->Attribute("Hp", &nAddHP);
	pItemXml->Attribute("Mp", &nAddMP);
	pItemXml->Attribute("PhysicsAttack", &nAddPhysicsAttack);
	pItemXml->Attribute("MagicAttack", &nAddMagicAttack);
	pItemXml->Attribute("PhysicsDefend", &nAddPhysicsDef);
	pItemXml->Attribute("MagicDefend", &nAddMagicDef);
	pItemXml->Attribute("PhysicsHit", &nAddPhysicsHit);
	pItemXml->Attribute("MagicHit", &nAddMagicHit);
	pItemXml->Attribute("PhysicsCritical", &nAddPhysicsCritical);
	pItemXml->Attribute("MagicCritical", &nAddMagicCritical);
	pItemXml->Attribute("PhysicsAttackPercent", &nAddPhysicsAttackPercent);
	pItemXml->Attribute("MagicAttackPercent", &nAddMagicAttackPercent);
	pItemXml->Attribute("PhysicsCriticalDamage", &nAddPhysicsCriticalDamage);
	pItemXml->Attribute("MagicCriticalDamage", &nAddMagicCriticalDamage);
	pItemXml->Attribute("PhysicsDefendPercent", &nAddPhysicsDefendPercent);
	pItemXml->Attribute("MagicDefendPercent", &nAddMagicDefendPercent);
	pItemXml->Attribute("PhysicsCriticalDerate", &nAddPhysicsCriticalDerate);
	pItemXml->Attribute("MagicCriticalDerate", &nAddMagicCriticalDerate);
	pItemXml->Attribute("PhysicsCriticalDamageDerate", &nAddPhysicsCriticalDamageDerate);
	pItemXml->Attribute("MagicCriticalDamageDerate", &nAddMagicCriticalDamageDerate);
	pItemXml->Attribute("PhysicsJouk", &nAddPhysicsJouk);
	pItemXml->Attribute("MagicJouk", &nAddMagicJouk);
	pItemXml->Attribute("PhysicsDeratePercent", &nAddPhysicsDeratePercent);
	pItemXml->Attribute("MagicDeratePercent", &nAddMagicDeratePercent);
	pItemXml->Attribute("Stun", &nAddStun);
	pItemXml->Attribute("Tie", &nAddTie);
	pItemXml->Attribute("Antistun", &nAddAntistun);
	pItemXml->Attribute("Antitie", &nAddAntitie);
	pItemXml->Attribute("PhysicsAttackAdd", &nAddPhysicsAttackAdd);
	pItemXml->Attribute("MagicAttackAdd", &nAddMagicAttackAdd);
	pItemXml->Attribute("PhysicsRift", &nAddPhysicsRift);
	pItemXml->Attribute("MagicRift", &nAddMagicRift);
	pItemXml->Attribute("Brand", &nBrand);
	pItemXml->Attribute("AttackPhyMin", &nAttackPhyMin);
	pItemXml->Attribute("AttackPhyMax", &nAttackPhyMax);
	pItemXml->Attribute("AttackMagMin", &nAttackMagMin);
	pItemXml->Attribute("AttackMagMax", &nAttackMagMax);
	return 0;
}


int CEquipItem::ShowTip(vector<string>& vectItemInfo)
{
	CItem::ShowTip(vectItemInfo); //13

	char szBuf[MAX_PATH];
	sprintf(szBuf, "%d", iGearArmType);
	vectItemInfo.push_back(szBuf); //14
	sprintf(szBuf, "%d", iSuitID);
	vectItemInfo.push_back(szBuf); //15
	sprintf(szBuf, "%d", nAttackPhyMin);
	vectItemInfo.push_back(szBuf); //16
	sprintf(szBuf, "%d", nAttackPhyMax);
	vectItemInfo.push_back(szBuf); //17
	sprintf(szBuf, "%d", nAttackMagMin);
	vectItemInfo.push_back(szBuf); //18

	sprintf(szBuf, "%d", nAttackMagMax);
	vectItemInfo.push_back(szBuf); //19

	sprintf(szBuf, "%d", nAddHP);
	vectItemInfo.push_back(szBuf); //20

	sprintf(szBuf, "%d", nAddMP);
	vectItemInfo.push_back(szBuf); //21

	sprintf(szBuf, "%d", nBrand);
	vectItemInfo.push_back(szBuf); //22

	sprintf(szBuf, "%d", nAddPhysicsAttack);
	vectItemInfo.push_back(szBuf); //23

	sprintf(szBuf, "%d", nAddMagicAttack);
	vectItemInfo.push_back(szBuf); //24  
	sprintf(szBuf, "%d", nAddPhysicsDef);
	vectItemInfo.push_back(szBuf); //25

	sprintf(szBuf, "%d", nAddMagicDef);
	vectItemInfo.push_back(szBuf); //26
	sprintf(szBuf, "%d", nAddPhysicsHit);
	vectItemInfo.push_back(szBuf); //27
	sprintf(szBuf, "%d", nAddMagicHit);
	vectItemInfo.push_back(szBuf); //28
	sprintf(szBuf, "%d",nAddPhysicsCritical);
	vectItemInfo.push_back(szBuf); //29
	sprintf(szBuf, "%d", nAddMagicCritical);
	vectItemInfo.push_back(szBuf); //30
	sprintf(szBuf, "%d", nAddPhysicsAttackPercent);
	vectItemInfo.push_back(szBuf); //31
	sprintf(szBuf, "%d", nAddMagicAttackPercent);
	vectItemInfo.push_back(szBuf); //32
	sprintf(szBuf, "%d", nAddPhysicsCriticalDamage);
	vectItemInfo.push_back(szBuf); //33
	sprintf(szBuf, "%d", nAddMagicCriticalDamage);
	vectItemInfo.push_back(szBuf); //34
	sprintf(szBuf, "%d", nAddPhysicsDefendPercent);
	vectItemInfo.push_back(szBuf); //35
	sprintf(szBuf, "%d", nAddMagicDefendPercent);
	vectItemInfo.push_back(szBuf); //36
	sprintf(szBuf, "%d", nAddPhysicsCriticalDerate);
	vectItemInfo.push_back(szBuf); //37
	sprintf(szBuf, "%d", nAddMagicCriticalDerate);
	vectItemInfo.push_back(szBuf); //38
	sprintf(szBuf, "%d", nAddPhysicsCriticalDamageDerate);
	vectItemInfo.push_back(szBuf); //39
	sprintf(szBuf, "%d", nAddMagicCriticalDamageDerate);
	vectItemInfo.push_back(szBuf); //40
	sprintf(szBuf, "%d", nAddPhysicsJouk);
	vectItemInfo.push_back(szBuf); //41
	sprintf(szBuf, "%d", nAddMagicJouk);
	vectItemInfo.push_back(szBuf); //42
	sprintf(szBuf, "%d", nAddPhysicsDeratePercent);
	vectItemInfo.push_back(szBuf); //43
	sprintf(szBuf, "%d", nAddMagicDeratePercent);
	vectItemInfo.push_back(szBuf); //44
	sprintf(szBuf, "%d", nAddStun);
	vectItemInfo.push_back(szBuf); //45
	sprintf(szBuf, "%d", nAddTie);
	vectItemInfo.push_back(szBuf); //46
	sprintf(szBuf, "%d", nAddAntistun);
	vectItemInfo.push_back(szBuf); //47
	sprintf(szBuf, "%d", nAddAntitie);
	vectItemInfo.push_back(szBuf); //48
	sprintf(szBuf, "%d", nAddPhysicsAttackAdd);
	vectItemInfo.push_back(szBuf); //49
	sprintf(szBuf, "%d", nAddMagicAttackAdd);
	vectItemInfo.push_back(szBuf); //50
	sprintf(szBuf, "%d", nAddPhysicsRift);
	vectItemInfo.push_back(szBuf); //51
	sprintf(szBuf, "%d", nAddMagicRift);
	vectItemInfo.push_back(szBuf); //52
	sprintf(szBuf, "%d", nAddSpeed);
	vectItemInfo.push_back(szBuf); //53
	sprintf(szBuf, "%d", AddID);
	vectItemInfo.push_back(szBuf); //54

	return ItemType;
}

void CItemLevelUp::LoadFromXml(const TiXmlElement* pItemLevelXml)
{
	pItemLevelXml->Attribute("LevelUpId", &ID);
	pItemLevelXml->Attribute("AttackPhyMin", &AttackPhyMin);
	pItemLevelXml->Attribute("AttackPhyMax", &AttackPhyMax);
	pItemLevelXml->Attribute("AttackMagMin", &AttackMagMin);
	pItemLevelXml->Attribute("AttackMagMax", &AttackMagMax);
	pItemLevelXml->Attribute("PhysicsAttack", &PhysicsAttack);
	pItemLevelXml->Attribute("MagicAttack", &MagicAttack);
	pItemLevelXml->Attribute("PhysicsDefend", &PhysicsDefend);
	pItemLevelXml->Attribute("MagicDefend", &MagicDefend);
}

void CItemLevelUp::GetProp(vector<string>& vectProp)
{
	char szTmp[MAX_PATH];
	sprintf(szTmp, "%d", ID);
	vectProp.push_back(szTmp); //0
	sprintf(szTmp, "%d", AttackPhyMin);
	vectProp.push_back(szTmp); //1
	sprintf(szTmp, "%d", AttackPhyMax);
	vectProp.push_back(szTmp); //2
	sprintf(szTmp, "%d", AttackMagMin);
	vectProp.push_back(szTmp); //3
	sprintf(szTmp, "%d", AttackMagMax);
	vectProp.push_back(szTmp); //4
	sprintf(szTmp, "%d", PhysicsAttack);
	vectProp.push_back(szTmp); //5
	sprintf(szTmp, "%d", MagicAttack);
	vectProp.push_back((szTmp)); //6
	sprintf(szTmp, "%d", PhysicsDefend);
	vectProp.push_back((szTmp)); //7
	sprintf(szTmp, "%d", MagicDefend);
	vectProp.push_back(szTmp); //8
}

void CSuit::LoadFromXml(const TiXmlElement* pSuitXml)
{
	pSuitXml->Attribute("ID", &ID);
	pSuitXml->Attribute("Head", &Head);
	pSuitXml->Attribute("Body", &Body);
	pSuitXml->Attribute("Hand", &Hand);
	pSuitXml->Attribute("Foot", &Foot);
	pSuitXml->Attribute("Shoulder", &Shoulder);
	pSuitXml->Attribute("Back", &Back);
	pSuitXml->Attribute("Torque", &Torque);
	pSuitXml->Attribute("Ring1", &Ring1);
	pSuitXml->Attribute("Ring2", &Ring2);
	pSuitXml->Attribute("Earring1", &Earring1);
	pSuitXml->Attribute("Earring2", &Earring2);
	pSuitXml->Attribute("Bangle1", &Bangle1);
	pSuitXml->Attribute("Bangle2", &Bangle2);
	pSuitXml->Attribute("Weapon1", &Weapon1);
	pSuitXml->Attribute("Weapon2", &Weapon2);
	pSuitXml->Attribute("Weapon3", &Weapon3);
	pSuitXml->Attribute("Mounts", &Mounts);
	pSuitXml->Attribute("AttackPhyMin", &AttackPhyMin);
	pSuitXml->Attribute("AttackPhyMax", &AttackPhyMax);
	pSuitXml->Attribute("AttackMagMin", &AttackMagMin);
	pSuitXml->Attribute("AttackMagMax", &AttackMagMax);
	pSuitXml->Attribute("Hp", &Hp);
	pSuitXml->Attribute("Mp", &Mp);
	pSuitXml->Attribute("PhysicsAttack", &PhysicsAttack);
	pSuitXml->Attribute("MagicAttack", &MagicAttack);
	pSuitXml->Attribute("PhysicsDefend", &PhysicsDefend);
	pSuitXml->Attribute("MagicDefend", &MagicDefend);
	pSuitXml->Attribute("PhysicsHit", &PhysicsHit);
	pSuitXml->Attribute("MagicHit", &MagicHit);
	pSuitXml->Attribute("PhysicsCritical", &PhysicsCritical);
	pSuitXml->Attribute("MagicCritical", &MagicCritical);
	pSuitXml->Attribute("PhysicsAttackPercent", &PhysicsAttackPercent);
	pSuitXml->Attribute("MagicAttackPercent", &MagicAttackPercent);
	pSuitXml->Attribute("PhysicsCriticalDamage", &PhysicsCriticalDamage);
	pSuitXml->Attribute("MagicCriticalDamage", &MagicCriticalDamage);
	pSuitXml->Attribute("PhysicsDefendPercent", &PhysicsDefendPercent);
	pSuitXml->Attribute("MagicDefendPercent", &MagicDefendPercent);
	pSuitXml->Attribute("PhysicsCriticalDerate", &PhysicsCriticalDerate);
	pSuitXml->Attribute("MagicCriticalDerate", &MagicCriticalDerate);
	pSuitXml->Attribute("PhysicsCriticalDamageDerate", &PhysicsCriticalDamageDerate);
	pSuitXml->Attribute("MagicCriticalDamageDerate", &MagicCriticalDamageDerate);
	pSuitXml->Attribute("PhysicsJouk", &PhysicsJouk);
	pSuitXml->Attribute("MagicJouk", &MagicJouk);
	pSuitXml->Attribute("PhysicsDeratePercent", &PhysicsDeratePercent);
	pSuitXml->Attribute("MagicDeratePercent", &MagicDeratePercent);
	pSuitXml->Attribute("Stun", &Stun);
	pSuitXml->Attribute("Tie", &Tie);
	pSuitXml->Attribute("Antistun", &Antistun);
	pSuitXml->Attribute("Antitie", &Antitie);
	pSuitXml->Attribute("PhysicsAttackAdd", &PhysicsAttackAdd);
	pSuitXml->Attribute("MagicAttackAdd", &MagicAttackAdd);
	pSuitXml->Attribute("PhysicsRift", &PhysicsRift);
	pSuitXml->Attribute("MagicRift", &MagicRift);
	pSuitXml->Attribute("AddSpeed", &AddSpeed);

}
void CSuit::BuildEffect()
{
	if (Hp > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"生命值＋%d",Hp);
		Effects.push_back(string(buf));
	}

	if (Mp > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"魔法值＋%d",Mp);
		Effects.push_back(string(buf));
	}

	if (PhysicsAttack > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"物理攻击＋%d",PhysicsAttack);
		Effects.push_back(string(buf));
	}

	if (MagicAttack > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"魔法攻击＋%d",MagicAttack);
		Effects.push_back(string(buf));
	}

	if (PhysicsDefend > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"物理防御＋%d",PhysicsDefend);
		Effects.push_back(string(buf));
	}

	if (MagicDefend > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"魔法防御＋%d",MagicDefend);
		Effects.push_back(string(buf));
	}

	if (PhysicsHit > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"物理命中＋%d",PhysicsHit);
		Effects.push_back(string(buf));
	}

	if (MagicHit > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"魔法命中＋%d",MagicHit);
		Effects.push_back(string(buf));
	}

	if (PhysicsCritical > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"物理攻击会心一击等级＋%d",PhysicsCritical);
		Effects.push_back(string(buf));
	}

	if (MagicCritical > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"魔法攻击会心一击等级＋%d",MagicCritical);
		Effects.push_back(string(buf));
	}

	if (PhysicsAttackPercent > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加物理攻击%d％",PhysicsAttackPercent);
		Effects.push_back(string(buf));
	}

	if (MagicAttackPercent > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加魔法攻击%d％",MagicAttackPercent);
		Effects.push_back(string(buf));
	}

	if (AttackPhyMin > 0)
	{
		string strProp = "最小物理攻击+";
		char buf[128];
		sprintf_s(buf,sizeof(buf),"%d",AttackPhyMin);
		strProp += buf;
		Effects.push_back(strProp);
	}

	if (AttackPhyMax > 0)
	{
		string strProp = "最大物理攻击+";
		char buf[128];
		sprintf_s(buf,sizeof(buf),"%d",AttackPhyMax);
		strProp += buf;
		Effects.push_back(strProp);
	}

	if (AttackMagMin > 0)
	{
		string strProp = "最小魔法攻击+";
		char buf[128];
		sprintf_s(buf,sizeof(buf),"%d",AttackMagMin);
		strProp += buf;
		Effects.push_back(strProp);
	}

	if (AttackMagMax > 0)
	{
		string strProp = "最大魔法攻击+";
		char buf[128];
		sprintf_s(buf,sizeof(buf),"%d",AttackMagMax);
		strProp += buf;
		Effects.push_back(strProp);
	}

	if (PhysicsCriticalDamage > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"物理攻击会心一击附加伤害%d",PhysicsCriticalDamage);
		Effects.push_back(string(buf));
	}

	if (MagicCriticalDamage > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"魔法攻击会心一击附加伤害%d",MagicCriticalDamage);
		Effects.push_back(string(buf));
	}

	if (PhysicsDefendPercent > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加物理防御%d％",PhysicsDefendPercent);
		Effects.push_back(string(buf));
	}

	if (MagicDefendPercent > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加魔法防御%d％",MagicDefendPercent);
		Effects.push_back(string(buf));
	}

	if (PhysicsCriticalDerate > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"防御物理攻击会心一击等级＋%d",PhysicsCriticalDerate);
		Effects.push_back(string(buf));
	}

	if (MagicCriticalDerate > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"防御魔法攻击会心一击等级＋%d",MagicCriticalDerate);
		Effects.push_back(string(buf));
	}

	if (PhysicsCriticalDamageDerate > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"减免物理攻击会心一击伤害%d",PhysicsCriticalDamageDerate);
		Effects.push_back(string(buf));
	}

	if (MagicCriticalDamageDerate > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"减免魔法攻击会心一击伤害%d",MagicCriticalDamageDerate);
		Effects.push_back(string(buf));
	}

	if (PhysicsJouk > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"物理闪避等级+%d",PhysicsJouk);
		Effects.push_back(string(buf));
	}

	if (MagicJouk > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"魔法闪避等级＋%d",MagicJouk);
		Effects.push_back(string(buf));
	}

	if (PhysicsDeratePercent > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"使你受到的物理攻击减少%d％",PhysicsDeratePercent);
		Effects.push_back(string(buf));
	}

	if (MagicDeratePercent > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"使你受到的魔法攻击减少%d％",MagicDeratePercent);
		Effects.push_back(string(buf));
	}

	if (Stun > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加技能附带的击晕几率%d％",Stun);
		Effects.push_back(string(buf));
	}

	if (Tie > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加技能附带的束缚几率%d％",Tie);
		Effects.push_back(string(buf));
	}

	if (Antistun > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加抵抗击晕几率%d",Antistun);
		Effects.push_back(string(buf));
	}

	if (Antitie > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加抵抗束缚几率%d",Antitie);
		Effects.push_back(string(buf));
	}

	if (PhysicsAttackAdd > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"在你发动物理攻击的时候有20％概率造成额外的物理伤害%d点",PhysicsAttackAdd);
		Effects.push_back(string(buf));
	}

	if (MagicAttackAdd > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"在你发动魔法攻击的时候有20％概率造成额外的魔法伤害%d点",MagicAttackAdd);
		Effects.push_back(string(buf));
	}

	if (PhysicsRift > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"使你的物理攻击有%d％概率忽视目标防御",PhysicsRift);
		Effects.push_back(string(buf));
	}

	if (MagicRift > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"使你的魔法攻击有%d％概率忽视目标防御",MagicRift);
		Effects.push_back(string(buf));
	}

	if (AddSpeed > 0)
	{
		char buf[128];
		sprintf_s(buf,sizeof(buf),"增加速度+%d",AddSpeed);
		Effects.push_back(string(buf));
	}
}
void CSuit::Add(const CItem* pItem)
{
	Items.push_back(pItem);
}
CItemManager::CItemManager(void)
{
	m_Items.clear();
}

CItemManager::~CItemManager(void)
{
	for (map<unsigned long, CItem*>::iterator Ite = m_Items.begin(); Ite != m_Items.end(); Ite++)
	{
		delete Ite->second;
	}

	m_Items.clear();
}


int CItemManager::LoadItemData(const char *szXmlFile)
{
	TiXmlDocument XmlDoc;
	XmlDoc.Parse(szXmlFile);

	int nItemType = 0;
	TiXmlElement* pRootElement = XmlDoc.RootElement();
	for (const TiXmlElement* pXmlSkillRoot = pRootElement->FirstChildElement(); pXmlSkillRoot; pXmlSkillRoot = pXmlSkillRoot->NextSiblingElement())
	{
		pXmlSkillRoot->Attribute("ItemType", &nItemType);
		switch(nItemType)
		{
		case TYPE_EQUIP:
			{
				CEquipItem* pItem = NULL;
				pItem = new CEquipItem(this, pXmlSkillRoot);
				map<int, CSuit>::iterator Ite = m_SuitMap.find(pItem->iSuitID);
				if (Ite != m_SuitMap.end())
				{
					Ite->second.Add(pItem);
				}
				m_Items[pItem->ItemID] = pItem;
			}
			break;
		case TYPE_CONSUME:
		case TYPE_NORMAL:
		case TYPE_SKILLBOOK:
		case TYPE_TASK:
		case TYPE_FUNC:
		case TYPE_VALUEOBJ:
		case TYPE_BLUEPRINT:
		case TYPE_CLOTHES:
			{
				CItem* pItem = NULL;
				pItem = new CItem(this, pXmlSkillRoot);
				m_Items[pItem->ItemID] = pItem;
			}
			break;
		default:

			break;
		}
	}
	return 0;
}

int CItemManager::LoadSuitData(const char* szXmlFile)
{
	TiXmlDocument XmlDoc;
	XmlDoc.Parse(szXmlFile);
	TiXmlElement* pRootElement = XmlDoc.RootElement();
	for (const TiXmlElement* pSuitxml = pRootElement->FirstChildElement(); pSuitxml; pSuitxml = pSuitxml->NextSiblingElement())
	{
		CSuit Suit;
		Suit.LoadFromXml(pSuitxml);
		Suit.BuildEffect();
		m_SuitMap[Suit.ID] = Suit;
	}
	return 0;
}

int CItemManager::LoadAddAttrData(const char* szXmlFile)
{
	TiXmlDocument XmlDoc;
	XmlDoc.Parse(szXmlFile);
	TiXmlElement* pRootElement = XmlDoc.RootElement();
	for (const TiXmlElement* pXmlAddAtttribute = pRootElement->FirstChildElement(); pXmlAddAtttribute; pXmlAddAtttribute = pXmlAddAtttribute->NextSiblingElement())
	{
		CEquipAddInfo EquitAddInfo;
		EquitAddInfo.LoadFromXml(pXmlAddAtttribute);
		const char* szID = pXmlAddAtttribute->Attribute("ID");
		m_ItemAddInfoMap[atoi(szID)] = EquitAddInfo;
	}
	return 0;
}


CItem* CItemManager::GetItem(unsigned long ulItemID)
{
	map<unsigned long, CItem*>::iterator Ite = m_Items.find(ulItemID);
	if (Ite == m_Items.end())
	{
		return NULL;
	}
	return reinterpret_cast<CItem*>(Ite->second);
}

CEquipAddInfo* CItemManager::GetEquipAddInfo(int nID)
{
	map<int, CEquipAddInfo>::iterator Ite = m_ItemAddInfoMap.find(nID);
	return Ite == m_ItemAddInfoMap.end() ? NULL : &(Ite->second);
}
CSuit* CItemManager::GetSuit(int nSuitID)
{
	map<int, CSuit>::iterator Ite = m_SuitMap.find(nSuitID);
	return Ite != m_SuitMap.end() ? &(Ite->second) : NULL;
}

int CItemManager::LoadItemLevelUpData(const char *szXmlFile)
{
	TiXmlDocument XmlDoc;
	XmlDoc.Parse(szXmlFile);
	TiXmlElement* pRootElement = XmlDoc.RootElement();
	for (const TiXmlElement* pXmlItemLevelUp = pRootElement->FirstChildElement(); pXmlItemLevelUp; pXmlItemLevelUp = pXmlItemLevelUp->NextSiblingElement())
	{
		CItemLevelUp ItemLevelUp;
		ItemLevelUp.LoadFromXml(pXmlItemLevelUp);
		m_ItemLevelUpMap[ItemLevelUp.ID] = ItemLevelUp;
	}
	
	return 0;
}

CItemLevelUp* CItemManager::GetItemLevelUp(int nItemLevelUpID)
{
	map<int, CItemLevelUp>::iterator Ite = m_ItemLevelUpMap.find(nItemLevelUpID);
	return Ite != m_ItemLevelUpMap.end() ? &(Ite->second) : NULL;
}