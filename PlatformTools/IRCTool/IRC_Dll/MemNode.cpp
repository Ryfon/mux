﻿#include "stdafx.h"
#include "MemNode.h"
#include "AppException.h"
#include "MemPool.h"

CMemNode::CMemNode(CMemPool& MemPool, size_t MemSize)
:m_BufSize(MemSize), m_MemPool(MemPool), m_BufType(0), m_UsedSize(0)
{
	memset((OVERLAPPED*)this, 0, sizeof(OVERLAPPED));
	m_SeqNum = 0;
	m_NextNode = NULL;
}

CMemNode::~CMemNode(void)
{

}

void CMemNode::AddData(const char* const pData, size_t DataLen)
{
	if (DataLen > m_BufSize - m_UsedSize)
	{
		throw CAppException("throw an exception in CMemNode-Add", "not enough space in buffer.");
	}

	memcpy(m_Data + m_UsedSize, pData, DataLen);
	m_UsedSize += DataLen;
}

size_t CMemNode::GetUsedSize() const
{
	return m_UsedSize;
}

size_t CMemNode::GetSize() const
{
	return m_BufSize;
}

char* CMemNode::GetBufferPrt()
{
	return &m_Data[0];
}

void CMemNode::SetUseSize(size_t UseSize)
{
	if (m_BufSize < m_UsedSize + UseSize)
	{
		throw CAppException("throw an exception in CMemNode-SetUseSize", "not enough space in buffer");
	}
	m_UsedSize += UseSize;
}

void CMemNode::SetBufType(size_t BufType)
{
	m_BufType = BufType;
}

void* CMemNode::operator new(size_t ObjSize, size_t BufSize)
{
	void* pMem = new char[ObjSize + BufSize];
	return pMem;
}

void CMemNode::operator delete(void *pObj, size_t BufSize)
{
	delete [] pObj;
}

CMemNode* CMemNode::SplitBuf(size_t RemoveDataLen)
{
	CMemNode* pMemNode = m_MemPool.NewMemNode();
	pMemNode->AddData(m_Data, RemoveDataLen);
	m_UsedSize -= RemoveDataLen;
	memmove(m_Data, m_Data + RemoveDataLen, m_UsedSize);
	return pMemNode;
}

CMemNode* CMemNode::NewMemNode()
{
	return m_MemPool.NewMemNode();
}

void CMemNode::RemoveData(size_t RemoveDataLen)
{
	m_UsedSize -= RemoveDataLen;
	memmove(m_Data, m_Data + RemoveDataLen, m_UsedSize);
}

void CMemNode::AddToMemNode(CMemNode* pToMemNode, size_t nAddDataLen)
{
	if (pToMemNode == NULL)
	{
		return;
	}
	if (nAddDataLen > m_UsedSize)
	{
		return;
	}
	pToMemNode->AddData(m_Data, nAddDataLen);
	RemoveData(nAddDataLen);
}

void CMemNode::SetSeqNum(unsigned long ulSeqNo)
{
	m_SeqNum = ulSeqNo;
}