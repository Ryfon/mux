﻿#pragma once

class CTask
{
public:
	enum ETaskStatType
	{
		NTS_NOACCEPT              	=	0,           	//未接受任务,对于任务实例而言不可能
		NTS_TGT_NOT_ACHIEVE       	=	1,           	//接受但还未达成目标
		NTS_TGT_ACHIEVE           	=	2,           	//接受且已达成目标
		NTS_FINISH                	=	3,           	//已完成任务
		NTS_INVALID               	=	4,           	//无效任务
	};

	enum TYPE
	{
		TYPE_MAINLINE,//主线任务
		TYPE_SUBLINE,//支线任务
		TYPE_EVERYDAY,//每日任务
		TYPE_INSTANCE,//副本任务
		TYPE_ACTIVITY,//活动任务
		TYPE_SYSTEM,//系统任务
		TYPE_FESTIVAL,//节日任务
		TYPE_SIZE
	};

	enum TARGET_TYPE
	{
		TT_NPC,
		TT_MONSTER,
		TT_ITEM,
		TT_PROPERTY,
		TT_SIZE
	};

	enum AWARD_TYPE
	{
		AT_ITEM,
		AT_SKILL,
		AT_SIZE
	};
public:
	struct STaskCondition
	{
		string strCondition;///前提
		int iProperty;
		int iValue;

		STaskCondition()
		{
			strCondition ="";
			iProperty = 0;
			iValue = 0;
		}
	};
	struct STaskTarget
	{
		TARGET_TYPE iType;
		int iID;
		string strName;
		int iPosX;
		int iPosY;

		STaskTarget()
		{
			iType = TT_NPC;
			iID = 0;
			strName = "";
			iPosX = -1;
			iPosY = -1;
		}
	};
	struct SAwards
	{
		string strAward;
		AWARD_TYPE iType;///奖励类型,物品0,技能1
		int iValue;///奖励的物品id
		string strPhoto;///图片名字
		string strName;///物品名字
		int iCount;///物品数量

		SAwards()
		{
			strAward = "";
			iType = AT_ITEM;
			iValue = 0;
			strName = "";
			iCount = 0;
		}
	};

public:
	UINT iID;					///任务id
	UINT iTypeID;				///任务类型id
	int iMapID;					///任务地图
	TYPE uiType;				///任务类型,主线,支线...
	int iStarLevel;				///任务星级
	string strName;				///任务名称
	string strSimpleDesc;		///任务简述
	string strProviderDesc;		///接任务描述
	string strSubmitToDesc;		///还任务描述
	string strType;				///任务类型,计数,对话...
	int iLevel;					///做任务最适合等级
	string strSubTaskType;		///任务子类型,杀怪,送礼...
	bool bItemProvider;			///是否是从物品获得的任务,true物品,falseNPC
	int iProvider;				///获取任务的npc或物品的id
	string strProviderName;
	int iProviderPosX;
	int iProviderPosY;
	int iSubmitTo;				///完成任务的npc的id
	string strSubmitToName;
	int iSubmitToPosX;
	int iSubmitToPosY;
	int iTargetNPC;				///目标npc,送礼类等有用
	string strTargetNPCName;
	int iTargetNPCPosX;
	int iTargetNPCPosY;
	int iTargetMonsterBegin;	///怪物目标的起始id
	int iTargetMonsterEnd;		///怪物目标的结束id,起始id~结束id为所要杀的怪
	int iTargetItemBegin;		///物品目标的起始id
	int iTargetItemEnd;			///物品目标的结束id
	int iTargetProperty;		///人物属性,锻炼类
	string strTargetName;		///目标名字,总的名称
	int iCompletedValue;		///杀怪已完成数量
	int iTargetValue;			///物品收集,杀怪,人物属性目标值
	int iAwardExp;				///奖励的经验值
	int iAwardMoney;			///奖励的金钱
	bool bShare;				///是否可共享
	bool bRepeat;				///是否可重复
	bool bGiveUp;				///是否可放弃
	bool bTrace;				///是否追踪任务
	vector<STaskCondition> vecTaskCondition;///接任务的前提
	vector<STaskTarget> vecTarget;///任务目标
	vector<SAwards> vecFixedAwards;///奖励
	vector<SAwards> vecOptionalAwards;///奖励
	ETaskStatType eTaskStatus;
};

class CTaskManager
{
public:
	CTaskManager(void);
	virtual ~CTaskManager(void);
public:
	CTask* GetTask(unsigned long ulTaskID);
	void LoadFromXml(const char* pszXmlFile);
private:
	map<int, CTask> m_TaskMap;
};
