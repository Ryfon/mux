﻿/** @file MemPool.h 
@brief 封装内存池
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-01-20
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef MEMPOOL_H
#define MEMPOOL_H

class CMemNode;
class CMemPool
{
public:
	friend class CMemNode;
	CMemPool(size_t BufSize, size_t FreeBufNum);
public:
	virtual ~CMemPool(void);
public:
	CMemNode* NewMemNode(size_t BufSize = 0);
	//删除内存节点
	void DestroyMem(CMemNode* pMemNode);
	//把内存节点放入池中
	void ReleaseMemNode(CMemNode* pMemNode);
private:
	//清楚所有内存节点
	void Clear();
private:
	const size_t m_BufNum; //内存块数
	const size_t m_BufSize; //每个内存块大小
	list<CMemNode*> m_FreeMemNodes; //可用的内存节点
	list<CMemNode*> m_ActiceMemNodes; //活动的内存节点
	CRITICAL_SECTION m_cs; //线程同步
};

#endif