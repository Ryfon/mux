﻿#ifndef CHAT_RECORDMNG_H
#define CHAT_RECORDMNG_H

#include <Windows.h>
#include <map>
#include <string>

#define MAX_NAME_SIZE 20
#define MAX_CHAT_SIZE 256



#pragma pack(push, 1)
typedef struct tag_FileHeader
{
	unsigned short m_DataLen;
	unsigned char m_DataType;
	unsigned short m_Cmd;

} FILEHEADER, *PFILEHEADER;
#pragma pack(pop)

using namespace std;

class CChatRecord;
class CMemPool;
class IChatRecEvent;

class CChatRecordMng
{
	friend CChatRecord;
public:
	#pragma pack(push, 1)

	typedef	enum
	{
		CT_SCOPE_CHAT                        	=	0,           	//周边范围内群聊
		CT_PRIVATE_CHAT                      	=	1,           	//私聊
		CT_MAP_BRO_CHAT                      	=	2,           	//地图内喊话
		CT_SYS_GM_NOTI_MAP                   	=	3,           	//GM发布的通知(通知地图内所有人)
		CT_SYS_EVENT_NOTI                    	=	4,           	//系统事件通知（例如操作结果）
		CT_SYS_WARNING                       	=	5,           	//系统警告
		CT_NPC_UIINFO                        	=	6,           	//NPC对话界面信息；
		CT_ITEM_UIINFO                       	=	7,           	//Item对话界面信息；
		CT_WORLD_CHAT                        	=	8,           	//全世界广播
		CT_TEAM_CHAT                         	=	9,           	//组队广播
		CT_AREA_UIINFO                       	=	10,          	//区域脚本界面信息
		CT_PRIVATE_IRC_CHAT                  	=	11,          	//IRC私聊
		CT_WORLD_IRC_CHAT                    	=	12,          	//IRC世界聊天
		CT_UNION_CHAT                        	=	13,          	//工会聊天
		CT_UNION_IRC_CHAT                    	=	14,          	//工会IRC聊天
		CT_SCRIPT_MSG                        	=	15,          	//脚本调用的消息
		CT_RACE_SYS_EVENT                    	=	16,          	//只给特定种族发的系统消息
		CT_RACE_BRO_CHAT                     	=	17,          	//种族喊话

		//自定义的聊天类型，屏蔽服务器聊天协议的不同
		CT_CUST_GROUP							=   20000,          //群聊
		CT_CUST_BULLETIN						=	20001,          //公告
	}CHAT_TYPE;

	enum CHATDIRECT 
	{
		LOCAL_SEND = 0, //发出消息
		PEER_SEND = 1 //接收消息
	};
	struct ChatLog
	{
		enum CHATDIRECT 
		{
			LOCAL_SEND = 0, //发出消息
			PEER_SEND = 1 //接收消息
		};
		char PeerName[MAX_NAME_SIZE];
		char LocalName[MAX_NAME_SIZE];
		CHATDIRECT ChatDirection;
		unsigned char ClientType; 
		SYSTEMTIME LogTime;
		char Chat[MAX_CHAT_SIZE];
		ChatLog()
		{
			memset(PeerName, 0, MAX_NAME_SIZE);
			memset(LocalName, 0, MAX_NAME_SIZE);
			memset(Chat, 0, MAX_NAME_SIZE);
		}
	};
	#pragma pack(pop)

public:
	enum FILE_IOCODE
	{
		IO_READ,
		IO_WRITE_REQ,
		IO_WRITE,
		IO_UNKNOWN
	};
public:
	CChatRecordMng(void);
	virtual ~CChatRecordMng(void);
public:
	int StartLogger(const char* szPath, IChatRecEvent* pEvent);
	int StopLogger();
	int WriteChatLog(CHAT_TYPE ct, bool bMe, const char* szRoleName, const char* szChatObj, const char* szChat);
	int ReadChatLog(CHAT_TYPE ct);
	int ClearChat();
private:
	int AddRecord(CChatRecord* pRecord);
	CChatRecord* GetRecord(CHAT_TYPE ct);
private:
	HANDLE m_Iocp; //完成端口
	HANDLE* m_pThread;
	CMemPool* m_MemPool; //内存池对象
	bool m_bIsStart;
	IChatRecEvent* m_pChatRecEvent;
private:
	CChatRecord* m_pWorldRecord; //世界
	CChatRecord* m_pRaceRecord; //种族
	CChatRecord* m_pGuildRecord; //战盟
	CChatRecord* m_pPrivateRecord; //私聊
	CChatRecord* m_pGroupRecord; //组队
	CChatRecord* m_pCommRecord; //普通
	CChatRecord* m_pAdvise; //公告
	CChatRecord* m_pSystem; //系统
	CChatRecord* m_pChatGroup; //聊天群
	CChatRecord* m_pSameCity; //同城
	CChatRecord* m_pLocal; //本地
private:
	//线程函数
	static unsigned __stdcall WorkThread(void* pThreadParams);

};

class IChatRecEvent
{
public:
	virtual ~IChatRecEvent(){};
public:
	virtual int OnChatRecord(CChatRecordMng::ChatLog* pChatLog) = 0;
};

#endif




