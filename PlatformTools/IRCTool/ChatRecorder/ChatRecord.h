﻿#ifndef CHAT_RECORD_H
#define CHAT_RECORD_H
#include <Windows.h>
#include <string>
#include "ChatRecordMng.h"
//换行符
#define NEWLINE_CHAR "\r\n"

class CMemNode;

using namespace std;
class CChatRecord
{
	friend CChatRecordMng;
public:
	CChatRecord(CChatRecordMng& ChatRecordMng);
	virtual ~CChatRecord(void);
private:
	int Open(const string& strFileName);
	int Write(const char* strFormat, ...); //写
	int Write(const char* pData, unsigned long ulDataLen); //写
	int Read(); //从当前位置异步读文件
	int Clear();
	int SetLogPath(const string& strLogPath);
	string GetDefaultDirectory(); //获得当前目录（后面加上/）
	int OnData(CMemNode* pMemNode);
	int LoadChatRec(char* pData, unsigned short usDataLen, struct CChatRecordMng::ChatLog &cl);
private:
	CChatRecordMng& m_ChatRecordMng;
	string m_FilePath; //保证多线程同步读写文件
	string m_FileName;
	HANDLE m_FileHandle;
	unsigned long m_CurPos;
	CMemNode* m_PartRec;
};

#endif