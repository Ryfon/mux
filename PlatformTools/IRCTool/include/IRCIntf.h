﻿/** @file IRCIntf.h 
@brief 接口定义
<pre>
*	Copyright (c) 2008，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2008-11-17
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef IRCINTF_H
#define IRCINTF_H

#ifdef IRC_DLL_EXPORTS
#define IRC_DLL_API __declspec(dllexport)
#else
#define IRC_DLL_API __declspec(dllimport)
#endif

enum CHAT_ERROR
{
	TARGET_NOT_EXIST         	=	0,               	//
	NO_WORLD_CHAT_PRIVILEGE  	=	1,               	//
	NO_PRIVATE_CHAT_PRIVILEGE	=	2,               	//
	NO_TEAM_CHAT_PRIVILEGE   	=	3,               	//
};

#define MAX_NAME_SIZE 20
#define MAX_CHAT_SIZE 256

#pragma pack(push, 1)
struct FriendGroupInfo_I 
{
	unsigned long m_GroupID;
	char m_GroupName[MAX_NAME_SIZE];
	FriendGroupInfo_I()
	{
		memset(m_GroupName, 0, MAX_NAME_SIZE);
	}
};

struct FriendInfo_I
{
	unsigned long             	friendID;                     	//好友编号,用于与C--S间通信
	unsigned long             	joinID;                       	//加入序号，按加入的先后顺序编号，用于客户端排序显示；
	unsigned long             	groupID;                      	//该好友所属组的组号
	char                	nickName[MAX_NAME_SIZE];        	//好友伲称
	bool                	isHateMe;                     	//该好友是否将自己作为仇人，用于客户端显示
	char                	isOnline;                     	//是否在线
	unsigned short              	mapID;                        	//地图号
	unsigned short              	playerLevel;                  	//等级
	unsigned short              	playerClass;                  	//职业
	char                			playerSex;                    	//性别
	unsigned short              	playerRace;                     //种族
	char GuildName[MAX_NAME_SIZE];
	char FereName[MAX_NAME_SIZE];
	FriendInfo_I()
	{
		memset(nickName, 0, MAX_NAME_SIZE);
		memset(GuildName, 0, MAX_NAME_SIZE);
		memset(FereName, 0, MAX_NAME_SIZE);
	}
};

struct FoeInfo_I
{
	unsigned long             	foeID;                        	//仇人编号,用于与C--S间通信
	unsigned long             	joinID;                       	//加入仇人名单的历史序号，用于客户端排序显示
	char                	nickName[MAX_NAME_SIZE];        	//好友伲称
	bool                	isHateMe;                     	//该好友是否将自己作为仇人，用于客户端显示
	char                	isOnline;                     	//是否在线
	unsigned short              	mapID;                        	//是否在线
	unsigned short              	playerLevel;                  	//等级
	unsigned short              	playerClass;                  	//职业
	char                	playerSex;                    	//性别
	bool                	isLocked;                     	//是否被锁定(不可被新仇人覆盖)
	unsigned short         	playerRace;                                 	//种族
	char GuildName[MAX_NAME_SIZE];
	char FereName[MAX_NAME_SIZE];
	FoeInfo_I()
	{
		memset(nickName, 0, MAX_NAME_SIZE);
		memset(GuildName, 0, MAX_NAME_SIZE);
		memset(FereName, 0, MAX_NAME_SIZE);
	}
};

struct BlackListItemInfo_I 
{
	unsigned long             	itemID;                       	//黑名单中的玩家编号,用于与C--S间通信
	unsigned long             	joinID;                       	//加入黑名单的历史序号，用于客户端排序显示
	char                	nickName[MAX_NAME_SIZE];        	//好友伲称
	BlackListItemInfo_I()
	{
		memset(nickName, 0, MAX_NAME_SIZE);
	}
};

struct UnionPosRight_I
{
	USHORT                 	seq;                                        	//
	USHORT                 	posLen;                                     	//
	CHAR                   	pos[8];                                       	//
	UINT                   	right;                                      	//
};

struct Union_I
{
	unsigned int                 	id;                                         	//
	unsigned short                 	nameLen;                                    	//
	char                   	name[MAX_NAME_SIZE];                      	//
	unsigned short                 	level;                                      	//
	unsigned short                 	num;                                        	//
	unsigned int                   	ownerUiid;                                  	//
	unsigned short                 	titleLen;                                   	//
	char                   	title[MAX_NAME_SIZE];                    	//
	unsigned int                   	active;                                     	//
	unsigned short                 	posListLen;                                 	//
	UnionPosRight_I          	posList[10];                                  	//
};

struct UnionMember_I
{
	unsigned int                   	gateId;                                     	//
	unsigned int                   	playerId;                                   	//
	unsigned int                   	uiid;                                       	//
	unsigned short                 	nameLen;                                    	//
	char                   	name[MAX_NAME_SIZE];                     	//
	unsigned short                 	job;                                        	//
	unsigned short                 	posSeq;                                     	//
	unsigned short                 	titleLen;                                   	//
	char                   	title[MAX_NAME_SIZE];                    	//
	unsigned short                 	level;                                      	//
	unsigned int                   	lastQuit;                                   	//
	unsigned short                 	forbid;                                     	//
	unsigned short                 	onLine;                                     	//
};

struct ChatLog
{
	enum CHATDIRECT 
	{
		LOCAL_SEND = 0, //发出消息
		PEER_SEND = 1 //接收消息
	};
	char PeerName[MAX_NAME_SIZE];
	char LocalName[MAX_NAME_SIZE];
	CHATDIRECT ChatDirection;
	unsigned char ClientType; 
	SYSTEMTIME LogTime;
	char Chat[MAX_CHAT_SIZE];
	ChatLog()
	{
		memset(PeerName, 0, MAX_NAME_SIZE);
		memset(LocalName, 0, MAX_NAME_SIZE);
		memset(Chat, 0, MAX_NAME_SIZE);
	}
};

struct PEERUSRINFO 
{
	char playerName[MAX_NAME_SIZE];
	unsigned char playerGender;
	unsigned short mapID;                                      	//玩家所在的地图
	unsigned short playerLevel;                                	//等级
	unsigned short playerClass;                                	//职业
	unsigned short playerRace;                                 	//种族
	char GuildName[MAX_NAME_SIZE];                     	//公会伲称
	char FereName[MAX_NAME_SIZE];                      	//伴侣伲称
	PEERUSRINFO()
	{
		memset(playerName, 0, MAX_NAME_SIZE);
		memset(GuildName, 0, MAX_NAME_SIZE);
		memset(FereName, 0, MAX_NAME_SIZE);
	}

};

#pragma pack(pop)

class CIRCEventSink;
//接口定义
class IRC_DLL_API CIRCIntf
{
public:
	virtual ~CIRCIntf(){};
public:
	//设置服务器信息
	virtual int SetServerInfo(const char* szAssignSvrIp, unsigned short usAssignSvrPort, const char* szGateSvrIp, unsigned short usGateSvrPort) = 0;
	//设置玩家信息
	virtual int SetPlayerInfo(const char* szAccount, const char* szPwd, const char* szRoleName) = 0;
	//读取技能信息
	virtual int ReadSkillInfo(const char* szSkillFileName) = 0;
	//读取任务信息
	virtual int ReadTaskInfo(const char* szTaskFileName) = 0;
	//读取道具信息
	virtual int ReadItemInfo(const char* szItemFileName) = 0;
	//读取套装信息
	virtual int ReadSuitInfo(const char* szSuitFileName) = 0;
	//读取附加属性信息
	virtual int ReadAdditionAttr(const char* szFileName) = 0;
	//读取升级属性信息
	virtual int ReadItemLevelUpInfo(const char* szFileName) = 0;
	//获得技能简明信息
	virtual void GetSkillInfo(unsigned long ulSkillID, vector<string>& vectSkillProp) = 0;
	//获得道具信息
	virtual int GetItemInfo(unsigned long ulItemID, vector<string>& vectItemInfo) = 0;
	//获取套装信息
	virtual int GetSuitInfo(unsigned long ulSuitID, int& nSuitCount, vector<string>& vectSuitName, vector<string>& vectSuitAttr) = 0;
	//获取追加属性信息
	virtual int GetItemAdditionInfo(unsigned long ulItemAddID, vector<string>& vectItemAdditionAttr) = 0;
	//获得升级属性信息
	virtual int GetItemLevelUpInfo(unsigned long ulLevelUpID, vector<string>& vectLevelUpInfo) = 0;
	//获得任务信息
	virtual int GetTaskInfo(unsigned long ulTaskID, vector<string>& vectTaskInfo) = 0;

	virtual void ReqUserInfo(const char* szRoleName) = 0;

	virtual void ClearLog(int nType) = 0;

	virtual void SetFilter(unsigned short usFilter ) = 0;

	virtual void ChangeChatLogDir(const char* szDir) = 0;

	virtual string GetClassByClassID(unsigned long ulClassID) = 0;

	virtual string GetRaceByRaceID(unsigned long ulRaceID) = 0;

	virtual string GetGender(unsigned long ulGenderID) = 0;

	virtual unsigned short GetCurRoleNo() = 0;


	//启动
	virtual int StartUp(CIRCEventSink& ircEvent, char* pDllData) = 0;
	//停止
	virtual int CleanUp() = 0;
	//打开连接，并且用用户名进行登录
	virtual int Open() = 0;
	//进入游戏
	virtual int EnterWorld(unsigned int uiRoleID) = 0;
	//离开游戏
	virtual int Close() = 0;
	//
	virtual int QryUnionMember() = 0;

	//向世界频道发送消息 注：不允许向世界频道发送消息
	virtual int SendMsgToWorld(const char* szChat) = 0;
	//向公会频道发送消息
	virtual int SendMsgToGuild(const char* szChat) = 0;
	//向同城频道发送消息

	//向GM频道发送消息 注：不允许向GM频道发送消息

	//发送私聊消息
	virtual int SendPrivateMsg(const char* szDest, const char* szChat) = 0;
	//同城搜索

	//游戏元素

	//读取私聊记录
	virtual int ReadPrivateMsg() = 0;
	//读取世界聊天记录
	virtual int ReadWorldMsg() = 0;
	//读取战盟记录
	virtual int ReadGuildMsg() = 0;
	//读取普通聊天记录
	virtual int ReadCommonMsg() = 0;
	//读取种族聊天记录
	//virtual int ReadRaceMsg() = 0;
	//读取组队聊天记录
	virtual int ReadGroupMsg() = 0;
	//读取聊天群
	virtual int ReadChatGroupMsg() = 0;
	//读取系统聊天群
	virtual int ReadSystermMsg() = 0;
};

extern "C" IRC_DLL_API CIRCIntf* CreateIRCIntf();
extern "C" IRC_DLL_API void DeleteIRCIntf(CIRCIntf* pIRCIntf);

//用户名或密码错误
#define ERR_INVALID_USER 1001
//角色名无效
#define ERR_INVALID_ROLE 1002
//登录超时
#define ERR_LOGIN_TIMEOUT 1003
//玩家已在线
#define ERR_USER_ONLINE 1004
//连接被中断
#define ERR_CONNECTION_BREAKDOWN 2001
//对端用户不在线或者角色名不存在
#define ERR_PEERUSER_OFFLINE 3001

//回调接口定义
class CIRCEventSink
{
public:
	//玩家角色列表通知
	virtual int OnRoleInfo(unsigned int uiRoleID, const char* szRoleName) = 0;
	//进入游戏世界成功
	virtual int OnLoginGame() = 0;
	//好友分组信息
	virtual int OnFriendsGroup(FriendGroupInfo_I* pFirendGroupInfo, unsigned short usGroupNum) = 0;
	//好友信息
	virtual int OnFriendInfo(FriendInfo_I* pFriendInfo, unsigned short usFriendCount) = 0;
	//好友信息更新（上线、下线）
	virtual int OnFriendInfoUpdate(FriendInfo_I* pFriendInfo) = 0;
	//仇人信息
	virtual int OnFoeInfo(FoeInfo_I* pFoeInfo, unsigned short usFoeCount) = 0;
	//仇人信息更新（上线、下线）
	virtual int OnFoeUpdate(FoeInfo_I* pFoeInfo) = 0;
	//黑名单信息
	virtual int OnBlackItem(BlackListItemInfo_I* pBlackItemInfo) = 0;
	//公会消息
	virtual int OnGuild(Union_I* pUnion) = 0;
	//公会解散消息
	virtual int OnGuildDismiss() = 0;
	//删除公会会员通知
	virtual int OnGuildDelMember(unsigned short usFlag, unsigned int nMemID) = 0;
	//增加公会会员通知
	virtual int OnGuildAddMem(UnionMember_I* pGuildMem) = 0;
	//公会会员上下线通知
	virtual int OnGuildMemOffOnline(UINT nMemID, bool bIsOnLine) = 0;
	//公会禁言通知
	virtual int OnGuildBanChat(const char* szRoleName) = 0;
	//公会会员信息
	virtual int OnGuildMember(UnionMember_I* pUnionMember) = 0;
	//公会会员称号改变通知
	virtual int OnGuildMemberTitleChange(const char* szMemberName, const char* szTitleName) = 0;
	//公会职位信息更新通知
	virtual int OnGuildPosUpdateNty(int nPosLen, UnionPosRight_I* pUnionPos) = 0;
	//公会会员职位变化通知
	virtual int OnGuildMemPosChange(const char* szMemberName, USHORT nPos) = 0;

	//世界频道消息
	virtual int OnWorldMsg(const char* szSrc, const char* szChat, int nClientType) = 0;

	virtual int OnPeerInfo(PEERUSRINFO* pPeerUsrInfo) = 0;
	//公会频道消息
	virtual int OnGuildMsg(const char* szSrc, const char* szChat, int nClientType) = 0;
	//同城频道消息

	//GM频道消息

	//私聊消息
	virtual int OnPrivateMsg(const char* szSrc, const char* szChat, int nClientType) = 0;
	//同城搜索消息

	//聊天错误消息
	virtual int OnChatError(const char* szSrc, int nErrorType) = 0;

	//私聊记录读取结果
	virtual int OnReadPrivateMsg(ChatLog* pChatLog) = 0;
	//世界消息读取结果
	virtual int OnReadWorldMsg(ChatLog* pChatLog) = 0;
	//公会消息读取结果
	virtual int OnReadGuildMsg(ChatLog* pChatLog) = 0;
	//普通消息读取结果
	virtual int OnReadCommonMsg(ChatLog* pChatLog) = 0;
	//系统消息读取结果
	virtual int OnReadSystemMsg(ChatLog* pChatLog) = 0;
	//组队消息读取结果
	virtual int OnReadTeamMsg(ChatLog* pChatLog) = 0;
	//聊天群消息读取结果
	virtual int OnReadChatGroupMsg(ChatLog* pChatLog) = 0;
	//错误信息
	virtual int OnIRCError(unsigned short usErrCode) = 0;
};
#endif