﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TextureTools;

namespace TestTextureMerger
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            TextureMerger.SetShaderPath(@"E:\mux\Developer\PlatformTools\TextureTools\TextureMerger\shader.fx");
           bitmaps[0] = new Bitmap(@"..\..\Texture0.bmp");
           bitmaps[1] = new Bitmap(@"..\..\Texture1.bmp");
           bitmaps[2] = new Bitmap(@"..\..\Texture2.bmp");
           bitmaps[3] = new Bitmap(@"..\..\Texture3.bmp");
           bitmaps[4] = new Bitmap(@"..\..\Texture4.bmp");
           bitmaps[5] = new Bitmap(@"..\..\Texture5.bmp");
           bitmaps[6] = new Bitmap(@"..\..\Texture6.bmp");
           bitmaps[7] = new Bitmap(@"..\..\scene.jpg");          

        }

        private void TestTextureMerger()
        {
            //Bitmap bitmap = new Bitmap(1024, 1024);
            //Bitmap[] bitmaps = new Bitmap[7];

            //bitmaps[0] = new Bitmap(@"..\..\Texture0.bmp");
            //bitmaps[1] = new Bitmap(@"..\..\Texture1.bmp");
            //bitmaps[2] = new Bitmap(@"..\..\Texture2.bmp");
            //bitmaps[3] = new Bitmap(@"..\..\Texture3.bmp");
            //bitmaps[4] = new Bitmap(@"..\..\Texture4.bmp");
            //bitmaps[5] = new Bitmap(@"..\..\Texture5.bmp");
            //bitmaps[6] = new Bitmap(@"..\..\Texture6.bmp");

            //TextureMerger.Instance.AddTimeStamp("Enter Render");

            for (int i = 0; i < 8; i++ )
            {
                TextureMerger.Instance.AddBitMap(bitmaps[i],0.5f);
            }

            TextureMerger.Instance.Render(bitmap);

            //TextureMerger.Instance.AddTimeStamp("Quit Render");

            pictureBox1.BackgroundImage = bitmap;

            TextureMerger.Instance.ClearBitMaps();
            //bitmap.Save("output.bmp"); 
  
            //for(int i = 0; i < 7; i++)
            //{
            //    bitmaps[i].Dispose();
            //    bitmaps[i] = null;
            //}

            //TextureMerger.Instance.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TestTextureMerger();
        }

        //protected TextureMerger m_merger = null;

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            TextureMerger.Instance.Dispose();
        }

         Bitmap bitmap = new Bitmap(1024, 1024);
         Bitmap[] bitmaps = new Bitmap[8];
            
    }
}