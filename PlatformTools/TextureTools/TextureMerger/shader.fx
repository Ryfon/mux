float3 fColorKey
<
   string UIName = "fColorKey";
   string UIWidget = "Numeric";
   bool UIVisible =  false;
   float UIMin = 0.00;
   float UIMax = 1.00;
> = float3( 0.00, 0.00, 0.00 );

Texture Texture0;
Texture Texture1;
Texture Texture2;
Texture Texture3;
Texture Texture4;
Texture Texture5;
Texture Texture6;
Texture Texture7;

float fAlpha0 = 1.0f;
float fAlpha1 = 1.0f;
float fAlpha2 = 1.0f;
float fAlpha3 = 1.0f;
float fAlpha4 = 1.0f;
float fAlpha5 = 1.0f;
float fAlpha6 = 1.0f;
float fAlpha7 = 1.0f;

float fTextureWidth;

sampler samp0 = sampler_state { texture = <Texture0>;
																minfilter = LINEAR; mipfilter = LINEAR; magfilter = LINEAR;};
sampler samp1 = sampler_state { texture = <Texture1>;
																minfilter = LINEAR; mipfilter = LINEAR; magfilter = LINEAR;};
sampler samp2 = sampler_state { texture = <Texture2>;
																minfilter = LINEAR; mipfilter = LINEAR; magfilter = LINEAR;};
sampler samp3 = sampler_state { texture = <Texture3>;
																minfilter = LINEAR; mipfilter = LINEAR; magfilter = LINEAR;};	
sampler samp4 = sampler_state { texture = <Texture4>;
																minfilter = LINEAR; mipfilter = LINEAR; magfilter = LINEAR;};
sampler samp5 = sampler_state { texture = <Texture5>;
																minfilter = LINEAR; mipfilter = LINEAR; magfilter = LINEAR;};
sampler samp6 = sampler_state { texture = <Texture6>;
																minfilter = LINEAR; mipfilter = LINEAR; magfilter = LINEAR;};
sampler samp7 = sampler_state { texture = <Texture7>;
																minfilter = LINEAR; mipfilter = LINEAR; magfilter = LINEAR;};																
	
float4 ComputeColor(float4 srcColor, float4 desColor,float3 colorKey, float fAlpha)
{
		float4 color;
		if( srcColor.r == colorKey.r &&
		 	srcColor.g == colorKey.g &&
		 	srcColor.b == colorKey.b
		 )
     	{
     		color = desColor;
     	}
     	else
     	{
     		color = desColor * (1-fAlpha) + srcColor * fAlpha;
     	}
     	return color;
}
															
float4 BlendWithColorKey(float4x4 matColor, float3 colorKey, float4 matAlpha)
{
	for(int i = 1; i < 4; i++)
	{
		matColor[i].a = matAlpha[i];
		matColor[i] = ComputeColor(matColor[i], matColor[i-1],colorKey, matAlpha[i]);
  	}
  	
  	return matColor[3];
}
																
float4 ps_main(float2 texCoord : TEXCOORD0) : COLOR
{
	float fInverseViewportWidth = 1 / fTextureWidth;
	fInverseViewportWidth *= 0.5f;

	  float4x4 matColor0;
	  matColor0[0] = tex2D( samp0, texCoord + fInverseViewportWidth);
	  matColor0[1] = tex2D( samp1, texCoord + fInverseViewportWidth);
	  matColor0[2] = tex2D( samp2, texCoord + fInverseViewportWidth);
	  matColor0[3] = tex2D( samp3, texCoord + fInverseViewportWidth);
	  float4x4 matColor1;
	  matColor1[0] = tex2D( samp4, texCoord + fInverseViewportWidth);
	  matColor1[1] = tex2D( samp5, texCoord + fInverseViewportWidth);
	  matColor1[2] = tex2D( samp6, texCoord + fInverseViewportWidth);
	  matColor1[3] = tex2D( samp7, texCoord + fInverseViewportWidth);
  
	float4 matAlpha0;
	matAlpha0[0] = fAlpha0;
	matAlpha0[1] = fAlpha1;
	matAlpha0[2] = fAlpha2;
	matAlpha0[3] = fAlpha3;
	
	float4 matAlpha1;
	matAlpha1[0] = fAlpha4;
	matAlpha1[1] = fAlpha5;
	matAlpha1[2] = fAlpha6;
	matAlpha1[3] = fAlpha7;

	float4 color = BlendWithColorKey(matColor0, fColorKey,matAlpha0);
	
	matColor1[0] = ComputeColor(matColor1[0],color, fColorKey, matAlpha1[0]);
	
	color = BlendWithColorKey(matColor1, fColorKey, matAlpha1);
	
	return color;
}

technique TestShader
{
	pass P0
	{
		CullMode = None;	
		
		PixelShader = compile ps_3_0 ps_main();
	}
}