﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;

namespace TextureTools
{
    // D3D 辅助类
    class D3DHelper : Form
    {
        // Default Constructor
        protected D3DHelper()
        {
            presentParams.Windowed = true;
            presentParams.SwapEffect = SwapEffect.Discard;
        }

        static public bool InitGraphics()
        {
            if (m_d3dDevice != null)
            {
                return true;
            }

            try
            {
                // delete [5/31/2009 hemeng]
                //PresentParameters presentParams = new PresentParameters();  //
                //presentParams.Windowed = true;
                //presentParams.SwapEffect = SwapEffect.Discard;
                // Create Device
                m_d3dDevice = new Device(0, DeviceType.Hardware, new D3DHelper(),
                    CreateFlags.HardwareVertexProcessing, presentParams);

                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }
        }

        static public Device D3DDevice
        {
            get { return m_d3dDevice; }
        }

        // add [5/31/2009 hemeng]
        // 检查设备状态
        static public bool CheckDeviceState()
        {
            if(m_d3dDevice.CheckCooperativeLevel() == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static public bool ResetDevice()
        {
            if (m_d3dDevice.CheckCooperativeLevel() == true)
            {
                return true;
            }

            m_d3dDevice.Reset(presentParams);

            try
            {
                m_d3dDevice.TestCooperativeLevel();
            }
            catch (DeviceLostException e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }

            return true;
        }

        static protected Device m_d3dDevice = null;    // The D3D Device
        static protected PresentParameters presentParams = new PresentParameters(); // D3D Device paramters
    }
}
