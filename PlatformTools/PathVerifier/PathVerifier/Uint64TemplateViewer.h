﻿#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Collections::Generic;
using namespace System::Drawing::Drawing2D;
using namespace System::Text;
using namespace System::Windows::Forms;
using namespace System::IO;

#include "PathVerifier.h"

namespace PathVerifier {

	/// <summary>
	/// Uint64TemplateViewer 摘要
	/// </summary>
	public ref class Uint64TemplateViewer : public System::Windows::Forms::UserControl
	{
	public:
		Uint64TemplateViewer(void)
		{
			InitializeComponent();
			//
			//TODO: 在此处添加构造函数代码
			//
		}

		void SetTagTemplate(uint64 uiTemp)
		{
			m_uiTagTemplate = uiTemp;
			this->Invalidate();
		}

	protected: uint64 m_uiTagTemplate;
	protected:
		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		~Uint64TemplateViewer()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		void InitializeComponent(void)
		{
			this->SuspendLayout();
			// 
			// Uint64TemplateViewer
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Name = L"Uint64TemplateViewer";
			this->Size = System::Drawing::Size(64, 64);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Uint64TemplateViewer::Uint64TemplateViewer_Paint);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Uint64TemplateViewer_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {

				 // 重写 paint
				 // 清除原画面
				 Graphics^ g = this->CreateGraphics();
				 g->FillRectangle(gcnew SolidBrush(this->BackColor), 0, 0, Width, Height);

				 GraphicsPath^ path = gcnew GraphicsPath(System::Drawing::Drawing2D::FillMode::Winding);

				 // 根据 template 每一位的值绘制 Grid

				 int iGridIdx = 0;
				 int iGridSize = 8;
				 // 从左下第一个 Grid 开始绘制
				 for (int i = 0; i < 8;  i++)  // 遍历行
				 {
					 for (int j=0; j<8; j++, iGridIdx++)  // 遍历列
					 {
						 if ((m_uiTagTemplate & (1ll << iGridIdx)) == 0 )// block
						 {
							 Rectangle rect = Rectangle(j*iGridSize, (8-i-1)*iGridSize, iGridSize, iGridSize);
							 path->AddRectangle(rect);
						 }
					 }
				 }
				 SolidBrush^ brush = gcnew SolidBrush(Color::Red);
				 g->FillPath(brush, path);
			 }
	};
}
