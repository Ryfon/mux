﻿#pragma once

#pragma unmanaged
#include "PathVerifier.h"
#include "MapAccessTemplateGenerator.h"
#pragma managed

#include "MapViewer.h"
#include "MapManager.h"
#include "UInt64TemplateViewer.h"

namespace PathVerifier {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Form1 摘要
	///
	/// 警告: 如果更改此类的名称，则需要更改
	///          与此类所依赖的所有 .resx 文件关联的托管资源编译器工具的
	///          “资源文件名”属性。否则，
	///          设计器将不能与此窗体的关联
	///          本地化资源正确交互。
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		static Form1^ GetInstance()
		{
			if (m_Instance == nullptr)
			{
				m_Instance = gcnew Form1();
			}

			return m_Instance;
		}

		void AddLog(String^ strLog, bool bTime)
		{
			String^ strInfo = "";
			if (bTime)
			{
				strInfo = System::DateTime::Now.ToString() 
					+ ": " 
					+ strLog
					+ "\r\n";
			}
			else
			{
				strInfo = strLog + "\r\n";
			}

			m_tbInfo->Text = strInfo + m_tbInfo->Text;
		}

	private: System::Windows::Forms::Label^  label2;
	public: 
	private: System::Windows::Forms::TextBox^  m_tbTestTimes;
	private: System::Windows::Forms::Panel^  m_panelTemplateView;
	private: System::Windows::Forms::TextBox^  m_tbTagTemplateID;
	private: System::Windows::Forms::TextBox^  m_tbEndLocalID;

	private: System::Windows::Forms::TextBox^  m_tbStartLocalID;

	private: System::Windows::Forms::TextBox^  m_tbTempDirection;
	private: System::Windows::Forms::Button^  m_btnViewTemplate;
	private: System::Windows::Forms::CheckBox^  m_cbDDATest;
	private: System::Windows::Forms::RadioButton^  m_rbGridSize2x2;
	private: System::Windows::Forms::RadioButton^  m_rbGridSize1x1;
	private: System::Windows::Forms::Button^  m_btnLoadAccessRegion;
	private: System::Windows::Forms::Button^  m_btnTest;
	private: System::Windows::Forms::TextBox^  m_tbEndY;

	private: System::Windows::Forms::TextBox^  m_tbEndX;

	private: System::Windows::Forms::TextBox^  m_tbStartY;

	private: System::Windows::Forms::TextBox^  m_tbStartX;
	private: System::Windows::Forms::Button^  m_btnVerifyPath;







	protected:
		static Form1^ m_Instance = nullptr;

		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: 在此处添加构造函数代码
			//
			
			m_MapViewer = gcnew MapViewer();
			m_panelMap->Controls->Add(m_MapViewer);
			m_MapViewer->Location = Drawing::Point(0, 0);

			m_TemplateViewer0 = gcnew Uint64TemplateViewer();
			m_panelTemplateView->Controls->Add(m_TemplateViewer0);
			m_TemplateViewer0->Location = Drawing::Point(0, 0);

			m_TemplateViewer1 = gcnew Uint64TemplateViewer();
			m_panelTemplateView->Controls->Add(m_TemplateViewer1);
			m_TemplateViewer1->Location = Drawing::Point(65, 0);

			m_TemplateViewer2 = gcnew Uint64TemplateViewer();
			m_panelTemplateView->Controls->Add(m_TemplateViewer2);
			m_TemplateViewer2->Location = Drawing::Point(0, 65);

			m_TemplateViewer3 = gcnew Uint64TemplateViewer();
			m_panelTemplateView->Controls->Add(m_TemplateViewer3);
			m_TemplateViewer3->Location = Drawing::Point(65, 65);


			m_MapManager = gcnew CMapManager();
		}

	protected:
		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  m_btnLoadMap;
	protected: 

	private: System::Windows::Forms::Panel^  m_panelMap;

	private: System::Windows::Forms::RadioButton^  m_rbGridSize4x4;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::RadioButton^  m_rbGridSize6x6;
	private: System::Windows::Forms::RadioButton^  m_rbGridSize8x8;



	protected: 
		// 自定义成员
		MapViewer^	m_MapViewer;

		// 4 个template 
		Uint64TemplateViewer^ m_TemplateViewer0;
		Uint64TemplateViewer^ m_TemplateViewer1;
		Uint64TemplateViewer^ m_TemplateViewer2;
		Uint64TemplateViewer^ m_TemplateViewer3;


	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	private: System::Windows::Forms::TextBox^  m_tbInfo;
	protected: 
	private:
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		void InitializeComponent(void)
		{
			this->m_btnLoadMap = (gcnew System::Windows::Forms::Button());
			this->m_panelMap = (gcnew System::Windows::Forms::Panel());
			this->m_rbGridSize4x4 = (gcnew System::Windows::Forms::RadioButton());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->m_rbGridSize6x6 = (gcnew System::Windows::Forms::RadioButton());
			this->m_rbGridSize8x8 = (gcnew System::Windows::Forms::RadioButton());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->m_btnTest = (gcnew System::Windows::Forms::Button());
			this->m_btnLoadAccessRegion = (gcnew System::Windows::Forms::Button());
			this->m_rbGridSize1x1 = (gcnew System::Windows::Forms::RadioButton());
			this->m_rbGridSize2x2 = (gcnew System::Windows::Forms::RadioButton());
			this->m_cbDDATest = (gcnew System::Windows::Forms::CheckBox());
			this->m_btnViewTemplate = (gcnew System::Windows::Forms::Button());
			this->m_tbEndLocalID = (gcnew System::Windows::Forms::TextBox());
			this->m_tbStartLocalID = (gcnew System::Windows::Forms::TextBox());
			this->m_tbTempDirection = (gcnew System::Windows::Forms::TextBox());
			this->m_tbTagTemplateID = (gcnew System::Windows::Forms::TextBox());
			this->m_panelTemplateView = (gcnew System::Windows::Forms::Panel());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->m_tbTestTimes = (gcnew System::Windows::Forms::TextBox());
			this->m_tbInfo = (gcnew System::Windows::Forms::TextBox());
			this->m_tbStartY = (gcnew System::Windows::Forms::TextBox());
			this->m_tbStartX = (gcnew System::Windows::Forms::TextBox());
			this->m_tbEndY = (gcnew System::Windows::Forms::TextBox());
			this->m_tbEndX = (gcnew System::Windows::Forms::TextBox());
			this->m_btnVerifyPath = (gcnew System::Windows::Forms::Button());
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->SuspendLayout();
			// 
			// m_btnLoadMap
			// 
			this->m_btnLoadMap->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->m_btnLoadMap->Location = System::Drawing::Point(99, 540);
			this->m_btnLoadMap->Name = L"m_btnLoadMap";
			this->m_btnLoadMap->Size = System::Drawing::Size(40, 40);
			this->m_btnLoadMap->TabIndex = 0;
			this->m_btnLoadMap->Text = L"载入地图";
			this->m_btnLoadMap->UseVisualStyleBackColor = true;
			this->m_btnLoadMap->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// m_panelMap
			// 
			this->m_panelMap->AutoScroll = true;
			this->m_panelMap->Dock = System::Windows::Forms::DockStyle::Fill;
			this->m_panelMap->Location = System::Drawing::Point(0, 0);
			this->m_panelMap->Name = L"m_panelMap";
			this->m_panelMap->Size = System::Drawing::Size(574, 583);
			this->m_panelMap->TabIndex = 1;
			// 
			// m_rbGridSize4x4
			// 
			this->m_rbGridSize4x4->AutoSize = true;
			this->m_rbGridSize4x4->Location = System::Drawing::Point(55, 48);
			this->m_rbGridSize4x4->Name = L"m_rbGridSize4x4";
			this->m_rbGridSize4x4->Size = System::Drawing::Size(41, 16);
			this->m_rbGridSize4x4->TabIndex = 3;
			this->m_rbGridSize4x4->TabStop = true;
			this->m_rbGridSize4x4->Text = L"4x4";
			this->m_rbGridSize4x4->UseVisualStyleBackColor = true;
			this->m_rbGridSize4x4->CheckedChanged += gcnew System::EventHandler(this, &Form1::m_rbGridSize4x4_CheckedChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(8, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(53, 12);
			this->label1->TabIndex = 4;
			this->label1->Text = L"格子尺寸";
			// 
			// m_rbGridSize6x6
			// 
			this->m_rbGridSize6x6->AutoSize = true;
			this->m_rbGridSize6x6->Location = System::Drawing::Point(8, 68);
			this->m_rbGridSize6x6->Name = L"m_rbGridSize6x6";
			this->m_rbGridSize6x6->Size = System::Drawing::Size(41, 16);
			this->m_rbGridSize6x6->TabIndex = 5;
			this->m_rbGridSize6x6->TabStop = true;
			this->m_rbGridSize6x6->Text = L"6x6";
			this->m_rbGridSize6x6->UseVisualStyleBackColor = true;
			this->m_rbGridSize6x6->CheckedChanged += gcnew System::EventHandler(this, &Form1::m_rbGridSize6x6_CheckedChanged);
			// 
			// m_rbGridSize8x8
			// 
			this->m_rbGridSize8x8->AutoSize = true;
			this->m_rbGridSize8x8->Location = System::Drawing::Point(55, 68);
			this->m_rbGridSize8x8->Name = L"m_rbGridSize8x8";
			this->m_rbGridSize8x8->Size = System::Drawing::Size(41, 16);
			this->m_rbGridSize8x8->TabIndex = 6;
			this->m_rbGridSize8x8->TabStop = true;
			this->m_rbGridSize8x8->Text = L"8x8";
			this->m_rbGridSize8x8->UseVisualStyleBackColor = true;
			this->m_rbGridSize8x8->CheckedChanged += gcnew System::EventHandler(this, &Form1::m_rbGridSize8x8_CheckedChanged);
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->Location = System::Drawing::Point(0, 0);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->m_panelMap);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->m_btnVerifyPath);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbEndY);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbEndX);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbStartY);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbStartX);
			this->splitContainer1->Panel2->Controls->Add(this->m_btnTest);
			this->splitContainer1->Panel2->Controls->Add(this->m_btnLoadAccessRegion);
			this->splitContainer1->Panel2->Controls->Add(this->m_rbGridSize1x1);
			this->splitContainer1->Panel2->Controls->Add(this->m_rbGridSize2x2);
			this->splitContainer1->Panel2->Controls->Add(this->m_cbDDATest);
			this->splitContainer1->Panel2->Controls->Add(this->m_btnViewTemplate);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbEndLocalID);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbStartLocalID);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbTempDirection);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbTagTemplateID);
			this->splitContainer1->Panel2->Controls->Add(this->m_panelTemplateView);
			this->splitContainer1->Panel2->Controls->Add(this->label2);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbTestTimes);
			this->splitContainer1->Panel2->Controls->Add(this->m_tbInfo);
			this->splitContainer1->Panel2->Controls->Add(this->m_rbGridSize8x8);
			this->splitContainer1->Panel2->Controls->Add(this->m_btnLoadMap);
			this->splitContainer1->Panel2->Controls->Add(this->m_rbGridSize6x6);
			this->splitContainer1->Panel2->Controls->Add(this->m_rbGridSize4x4);
			this->splitContainer1->Panel2->Controls->Add(this->label1);
			this->splitContainer1->Size = System::Drawing::Size(722, 583);
			this->splitContainer1->SplitterDistance = 574;
			this->splitContainer1->TabIndex = 7;
			// 
			// m_btnTest
			// 
			this->m_btnTest->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->m_btnTest->Location = System::Drawing::Point(9, 549);
			this->m_btnTest->Name = L"m_btnTest";
			this->m_btnTest->Size = System::Drawing::Size(21, 23);
			this->m_btnTest->TabIndex = 20;
			this->m_btnTest->Text = L"button1";
			this->m_btnTest->UseVisualStyleBackColor = true;
			this->m_btnTest->Click += gcnew System::EventHandler(this, &Form1::m_btnTest_Click);
			// 
			// m_btnLoadAccessRegion
			// 
			this->m_btnLoadAccessRegion->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->m_btnLoadAccessRegion->Location = System::Drawing::Point(34, 540);
			this->m_btnLoadAccessRegion->Name = L"m_btnLoadAccessRegion";
			this->m_btnLoadAccessRegion->Size = System::Drawing::Size(59, 40);
			this->m_btnLoadAccessRegion->TabIndex = 19;
			this->m_btnLoadAccessRegion->Text = L"载入连通区域";
			this->m_btnLoadAccessRegion->UseVisualStyleBackColor = true;
			this->m_btnLoadAccessRegion->Click += gcnew System::EventHandler(this, &Form1::m_btnLoadAccessRegion_Click);
			// 
			// m_rbGridSize1x1
			// 
			this->m_rbGridSize1x1->AutoSize = true;
			this->m_rbGridSize1x1->Location = System::Drawing::Point(8, 28);
			this->m_rbGridSize1x1->Name = L"m_rbGridSize1x1";
			this->m_rbGridSize1x1->Size = System::Drawing::Size(41, 16);
			this->m_rbGridSize1x1->TabIndex = 18;
			this->m_rbGridSize1x1->TabStop = true;
			this->m_rbGridSize1x1->Text = L"1x1";
			this->m_rbGridSize1x1->UseVisualStyleBackColor = true;
			this->m_rbGridSize1x1->CheckedChanged += gcnew System::EventHandler(this, &Form1::m_rbGridSize1x1_CheckedChanged);
			// 
			// m_rbGridSize2x2
			// 
			this->m_rbGridSize2x2->AutoSize = true;
			this->m_rbGridSize2x2->Location = System::Drawing::Point(8, 48);
			this->m_rbGridSize2x2->Name = L"m_rbGridSize2x2";
			this->m_rbGridSize2x2->Size = System::Drawing::Size(41, 16);
			this->m_rbGridSize2x2->TabIndex = 17;
			this->m_rbGridSize2x2->TabStop = true;
			this->m_rbGridSize2x2->Text = L"2x2";
			this->m_rbGridSize2x2->UseVisualStyleBackColor = true;
			this->m_rbGridSize2x2->CheckedChanged += gcnew System::EventHandler(this, &Form1::m_rbGridSize2x2_CheckedChanged);
			// 
			// m_cbDDATest
			// 
			this->m_cbDDATest->AutoSize = true;
			this->m_cbDDATest->Checked = true;
			this->m_cbDDATest->CheckState = System::Windows::Forms::CheckState::Checked;
			this->m_cbDDATest->Location = System::Drawing::Point(9, 136);
			this->m_cbDDATest->Name = L"m_cbDDATest";
			this->m_cbDDATest->Size = System::Drawing::Size(42, 16);
			this->m_cbDDATest->TabIndex = 16;
			this->m_cbDDATest->Text = L"DDA";
			this->m_cbDDATest->UseVisualStyleBackColor = true;
			this->m_cbDDATest->CheckedChanged += gcnew System::EventHandler(this, &Form1::m_cbDDATest_CheckedChanged);
			// 
			// m_btnViewTemplate
			// 
			this->m_btnViewTemplate->Location = System::Drawing::Point(104, 393);
			this->m_btnViewTemplate->Name = L"m_btnViewTemplate";
			this->m_btnViewTemplate->Size = System::Drawing::Size(27, 23);
			this->m_btnViewTemplate->TabIndex = 15;
			this->m_btnViewTemplate->Text = L"check";
			this->m_btnViewTemplate->UseVisualStyleBackColor = true;
			this->m_btnViewTemplate->Click += gcnew System::EventHandler(this, &Form1::m_btnViewTemplate_Click);
			// 
			// m_tbEndLocalID
			// 
			this->m_tbEndLocalID->Location = System::Drawing::Point(55, 393);
			this->m_tbEndLocalID->Name = L"m_tbEndLocalID";
			this->m_tbEndLocalID->Size = System::Drawing::Size(40, 21);
			this->m_tbEndLocalID->TabIndex = 14;
			this->m_tbEndLocalID->Text = L"0";
			// 
			// m_tbStartLocalID
			// 
			this->m_tbStartLocalID->Location = System::Drawing::Point(10, 393);
			this->m_tbStartLocalID->Name = L"m_tbStartLocalID";
			this->m_tbStartLocalID->Size = System::Drawing::Size(39, 21);
			this->m_tbStartLocalID->TabIndex = 13;
			this->m_tbStartLocalID->Text = L"0";
			// 
			// m_tbTempDirection
			// 
			this->m_tbTempDirection->Location = System::Drawing::Point(8, 366);
			this->m_tbTempDirection->Name = L"m_tbTempDirection";
			this->m_tbTempDirection->Size = System::Drawing::Size(100, 21);
			this->m_tbTempDirection->TabIndex = 12;
			this->m_tbTempDirection->Text = L"0";
			// 
			// m_tbTagTemplateID
			// 
			this->m_tbTagTemplateID->Location = System::Drawing::Point(8, 202);
			this->m_tbTagTemplateID->Name = L"m_tbTagTemplateID";
			this->m_tbTagTemplateID->Size = System::Drawing::Size(67, 21);
			this->m_tbTagTemplateID->TabIndex = 11;
			this->m_tbTagTemplateID->TextChanged += gcnew System::EventHandler(this, &Form1::m_tbTagTemplateID_TextChanged);
			// 
			// m_panelTemplateView
			// 
			this->m_panelTemplateView->AutoScroll = true;
			this->m_panelTemplateView->Location = System::Drawing::Point(6, 228);
			this->m_panelTemplateView->MaximumSize = System::Drawing::Size(130, 130);
			this->m_panelTemplateView->MinimumSize = System::Drawing::Size(130, 130);
			this->m_panelTemplateView->Name = L"m_panelTemplateView";
			this->m_panelTemplateView->Size = System::Drawing::Size(130, 130);
			this->m_panelTemplateView->TabIndex = 10;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(10, 157);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(53, 12);
			this->label2->TabIndex = 9;
			this->label2->Text = L"测试次数";
			// 
			// m_tbTestTimes
			// 
			this->m_tbTestTimes->Location = System::Drawing::Point(10, 175);
			this->m_tbTestTimes->Name = L"m_tbTestTimes";
			this->m_tbTestTimes->Size = System::Drawing::Size(100, 21);
			this->m_tbTestTimes->TabIndex = 8;
			this->m_tbTestTimes->Text = L"50000";
			this->m_tbTestTimes->TextChanged += gcnew System::EventHandler(this, &Form1::m_tbTestTimes_TextChanged);
			// 
			// m_tbInfo
			// 
			this->m_tbInfo->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_tbInfo->Location = System::Drawing::Point(8, 423);
			this->m_tbInfo->Multiline = true;
			this->m_tbInfo->Name = L"m_tbInfo";
			this->m_tbInfo->Size = System::Drawing::Size(128, 111);
			this->m_tbInfo->TabIndex = 7;
			// 
			// m_tbStartY
			// 
			this->m_tbStartY->Location = System::Drawing::Point(54, 85);
			this->m_tbStartY->Name = L"m_tbStartY";
			this->m_tbStartY->Size = System::Drawing::Size(40, 21);
			this->m_tbStartY->TabIndex = 22;
			this->m_tbStartY->Text = L"0";
			// 
			// m_tbStartX
			// 
			this->m_tbStartX->Location = System::Drawing::Point(9, 85);
			this->m_tbStartX->Name = L"m_tbStartX";
			this->m_tbStartX->Size = System::Drawing::Size(39, 21);
			this->m_tbStartX->TabIndex = 21;
			this->m_tbStartX->Text = L"0";
			// 
			// m_tbEndY
			// 
			this->m_tbEndY->Location = System::Drawing::Point(54, 108);
			this->m_tbEndY->Name = L"m_tbEndY";
			this->m_tbEndY->Size = System::Drawing::Size(40, 21);
			this->m_tbEndY->TabIndex = 24;
			this->m_tbEndY->Text = L"0";
			// 
			// m_tbEndX
			// 
			this->m_tbEndX->Location = System::Drawing::Point(9, 108);
			this->m_tbEndX->Name = L"m_tbEndX";
			this->m_tbEndX->Size = System::Drawing::Size(39, 21);
			this->m_tbEndX->TabIndex = 23;
			this->m_tbEndX->Text = L"0";
			// 
			// m_btnVerifyPath
			// 
			this->m_btnVerifyPath->Location = System::Drawing::Point(104, 108);
			this->m_btnVerifyPath->Name = L"m_btnVerifyPath";
			this->m_btnVerifyPath->Size = System::Drawing::Size(32, 23);
			this->m_btnVerifyPath->TabIndex = 25;
			this->m_btnVerifyPath->Text = L"验证";
			this->m_btnVerifyPath->UseVisualStyleBackColor = true;
			this->m_btnVerifyPath->Click += gcnew System::EventHandler(this, &Form1::m_btnVerifyPath_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(722, 583);
			this->Controls->Add(this->splitContainer1);
			this->Name = L"Form1";
			this->Text = L"路径检查测试";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			this->splitContainer1->Panel2->PerformLayout();
			this->splitContainer1->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
	private:
		CMapManager^ m_MapManager;

		System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
				// 载入地图 .map 文件
				 OpenFileDialog^ dlg = gcnew OpenFileDialog();
				 dlg->Filter = "地图信息 (*.map)|*.map|All files(*.*)|*.*";
				 dlg->FilterIndex = 1;

				 if ( dlg->ShowDialog() == Windows::Forms::DialogResult::OK )
				 {
					CMapInfo^ mapInfo = m_MapManager->LoadMap(dlg->FileName);
					if (mapInfo != nullptr)
					{
						m_MapViewer->TagMapInfo = mapInfo;
					}
					else
					{
						// 文件载入失败
					}
				 }

			 }
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			 }
private: System::Void m_rbGridSize2x2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 m_MapViewer->GridSize = 2;
					  }
private: System::Void m_rbGridSize1x1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 m_MapViewer->GridSize = 1;
		 }
private: System::Void m_rbGridSize4x4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 m_MapViewer->GridSize = 4;
		 }
private: System::Void m_rbGridSize6x6_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 m_MapViewer->GridSize = 6;
		 }
private: System::Void m_rbGridSize8x8_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			 m_MapViewer->GridSize = 8;
		 }
private: System::Void m_tbTestTimes_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			 int iTimes = 50000;
			 try
			 {
				 iTimes = Int32::Parse(m_tbTestTimes->Text);
				 m_MapViewer->TestTimes = iTimes;
			 }
			 catch(Exception^)
			 {
				 m_tbTestTimes->Text = "50000";
			 }
			 

		 }
private: System::Void m_tbTagTemplateID_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 try
			 {
				 int iTagTmpID = Int32::Parse(m_tbTagTemplateID->Text);
				 uint64 iTemp = m_MapViewer->GetMapPiece(iTagTmpID);
				 m_TemplateViewer0->SetTagTemplate(iTemp);
			 }
			 catch (Exception^)
			 {
			 }
		 }
private: System::Void m_btnViewTemplate_Click(System::Object^  sender, System::EventArgs^  e) {
			 CMapAccessTemplateGenerator* pTempGen = CMapAccessTemplateGenerator::GetInstance();
			 
			 try
			 {
				 int iDir = Int32::Parse(m_tbTempDirection->Text);
				 int iStart = Int32::Parse(m_tbStartLocalID->Text);
				 int iEnd = Int32::Parse(m_tbEndLocalID->Text);

				 if (iDir == CMapAccessTemplateGenerator::AD_SELF)
				 {
					 uint64 uiTmp = pTempGen->m_arrAccTmpSelf[iStart][iEnd];
					 m_TemplateViewer0->SetTagTemplate(uiTmp);
				 }
				 else if (iDir == CMapAccessTemplateGenerator::AD_RIGHT)
				 {
					 uint64 uiTmp0 = pTempGen->m_arrAccTmpRight[iStart][iEnd][0];
					 uint64 uiTmp1 = pTempGen->m_arrAccTmpRight[iStart][iEnd][1];
					 m_TemplateViewer0->SetTagTemplate(uiTmp0);
					 m_TemplateViewer1->SetTagTemplate(uiTmp1);
				 }
				 else if (iDir == CMapAccessTemplateGenerator::AD_UP)
				 {
					 uint64 uiTmp0 = pTempGen->m_arrAccTmpUp[iStart][iEnd][0];
					 uint64 uiTmp1 = pTempGen->m_arrAccTmpUp[iStart][iEnd][1];
					 m_TemplateViewer2->SetTagTemplate(uiTmp0);
					 m_TemplateViewer0->SetTagTemplate(uiTmp1);
				 }
				 else if (iDir == CMapAccessTemplateGenerator::AD_UPLEFT)
				 {
					 uint64 uiTmp0 = pTempGen->m_arrAccTmpUpLeft[iStart][iEnd][0];
					 uint64 uiTmp1 = pTempGen->m_arrAccTmpUpLeft[iStart][iEnd][1];
					 uint64 uiTmp2 = pTempGen->m_arrAccTmpUpLeft[iStart][iEnd][2];
					 uint64 uiTmp3 = pTempGen->m_arrAccTmpUpLeft[iStart][iEnd][3];

					 m_TemplateViewer3->SetTagTemplate(uiTmp0);
					 m_TemplateViewer2->SetTagTemplate(uiTmp1);
					 m_TemplateViewer0->SetTagTemplate(uiTmp2);
					 m_TemplateViewer1->SetTagTemplate(uiTmp3);
				 }
				 else if (iDir == CMapAccessTemplateGenerator::AD_UPRIGHT)
				 {
					 uint64 uiTmp0 = pTempGen->m_arrAccTmpUpRight[iStart][iEnd][0];
					 uint64 uiTmp1 = pTempGen->m_arrAccTmpUpRight[iStart][iEnd][1];
					 uint64 uiTmp2 = pTempGen->m_arrAccTmpUpRight[iStart][iEnd][2];
					 uint64 uiTmp3 = pTempGen->m_arrAccTmpUpRight[iStart][iEnd][3];

					 m_TemplateViewer2->SetTagTemplate(uiTmp0);
					 m_TemplateViewer0->SetTagTemplate(uiTmp1);
					 m_TemplateViewer1->SetTagTemplate(uiTmp2);
					 m_TemplateViewer3->SetTagTemplate(uiTmp3);
				 }
			 }
			 catch(Exception^)
			 {

			 }

		 }
private: System::Void m_cbDDATest_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
			m_MapViewer->SetDDATest(m_cbDDATest->Checked);
		 }

private: System::Void m_btnLoadAccessRegion_Click(System::Object^  sender, System::EventArgs^  e) {
			 // 载入 AccessRegion .map 文件
			 OpenFileDialog^ dlg = gcnew OpenFileDialog();
			 dlg->Filter = "地图信息 (*.map)|*.map|All files(*.*)|*.*";
			 dlg->FilterIndex = 1;

			 if ( dlg->ShowDialog() == Windows::Forms::DialogResult::OK )
			 {
				 CMapInfo^ mapInfo = m_MapManager->LoadMap(dlg->FileName);
				 if (mapInfo != nullptr)
				 {
					m_MapViewer->SetAccessRegion(mapInfo);

				 }
				 else
				 {
					// 文件载入失败
				 }
				
			 }
		 }
private: System::Void m_btnTest_Click(System::Object^  sender, System::EventArgs^  e) {
			m_MapViewer->TestBlockQuad();
		 }
private: System::Void m_btnVerifyPath_Click(System::Object^  sender, System::EventArgs^  e) {
			 int iStartX = Int32::Parse(m_tbStartX->Text);
			 int iStartY = Int32::Parse(m_tbStartY->Text);

			 int iEndX = Int32::Parse(m_tbEndX->Text);
			 int iEndY = Int32::Parse(m_tbEndY->Text);

			 m_MapViewer->VerifyWalkable(Point(iStartX, iStartY), Point(iEndX, iEndY));

		 }
};
}

