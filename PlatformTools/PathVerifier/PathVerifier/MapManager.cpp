﻿#include "stdafx.h"
#include "MapManager.h"

namespace PathVerifier {

//------------------------------------------------------------------------------
CMapInfo^ CMapManager::LoadMap(String^ strFile)
{
	if (!File::Exists(strFile))
	{
		MessageBox::Show("文件 " + strFile + " 不存在.");
		return nullptr;
	}
	else if (!Path::GetExtension(strFile)->Equals(".map"))
	{
		MessageBox::Show("打开的不是 .map 文件. 加载失败.");
		return nullptr;
	}

	try
	{
		FileStream^ fs = File::Open(strFile, FileMode::Open, FileAccess::Read, FileShare::Read);
		int iBufLen = (int)(fs->Length);    // 地图最大为 512 * 512 grid, 不会超过 int 的表示范围
		array<unsigned char>^ buf = gcnew array<unsigned char>(iBufLen);

		// 读入地图所有信息
		fs->Read(buf, 0, iBufLen);
		fs->Close();

		// 解析 buf 中的数据
		CMapInfo^ pMapInfo = gcnew CMapInfo();

		int iCurPos = 4;	// 忽略前4位版本号
		pMapInfo->m_iMapID = BitConverter::ToUInt32(buf, iCurPos); iCurPos += 4;
		pMapInfo->m_iNumGridX = BitConverter::ToInt32(buf, iCurPos); iCurPos += 4;
		pMapInfo->m_iNumGridY = BitConverter::ToInt32(buf, iCurPos); iCurPos += 4;    
		iCurPos += 8; // 忽略后边两个整数

		int iGridNum = pMapInfo->m_iNumGridX * pMapInfo->m_iNumGridY;
		pMapInfo->m_arrMapInfo = new unsigned int[iGridNum];

		int iIdx = 0;
		for (int i=0; i<pMapInfo->m_iNumGridX; i++)  // 遍历行
		{
			for (int j=0; j<pMapInfo->m_iNumGridY; j++) // 遍历列
			{
				pMapInfo->m_arrMapInfo[iIdx++] = BitConverter::ToUInt32(buf, iCurPos);
				iCurPos += 4;
			}
		}

		// 将地图信息存入 map
		//m_htMaps[pMapInfo->m_iMapID] = pMapInfo;

		return pMapInfo;

	}
	catch (Exception^ e)
	{
		MessageBox::Show(e->ToString());
	}

	return nullptr;

}
//------------------------------------------------------------------------------
CMapInfo^ CMapManager::GetMapInfo(int iMapID)
{
	if (m_htMaps->ContainsKey(iMapID))
	{
		return dynamic_cast<CMapInfo^>(m_htMaps[iMapID]);
	}
	else
	{
		return nullptr;
	}
}
}