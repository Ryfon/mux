﻿#include "StdAfx.h"
#include "MapViewer.h"
#include "MainForm.h"

namespace PathVerifier 
{
	MapViewer::MapViewer(void)
	{
		InitializeComponent();
		//
		//TODO: 在此处添加构造函数代码
		//
		m_iGridSize = 4;	// 默认 grid 大小
		m_bFirstClick = true;
		m_bRightFirstClick = true;
		m_arrDDAPath = gcnew ArrayList();
		m_iTestTimes = 50000;
		m_pPathVerifier = NULL;
		m_pDistPathVerifier = NULL;
		m_AcceRegionPathVerifier = NULL;
		m_pAccessRegionMap = nullptr;
		m_puiPieceBlocks = NULL;
		m_bTestDDA = true;

		m_arrBlockQuads = new std::vector<CPathVerifier::CBlockQuad*>;
	}

	void MapViewer::VerifyWalkable(Point ptPieceStart, Point ptPieceEnd)
	{
		m_ptStartPoint = ptPieceStart;
		m_ptEndPoint = ptPieceEnd;

		m_arrDDAPath->Clear();

		/////////////////
		// 使用 dda 检查
		/////////////////

		bool bPass = _DDAVerify(m_arrDDAPath, ptPieceStart, ptPieceEnd);

		bool bFoo = false;
		if (m_bTestDDA)
		{
			DateTime timeStart = System::DateTime::Now;

			for (int i=0; i<m_iTestTimes; i++)
			{
				//_DDAVerify(nullptr, ptPieceStart, ptPieceEnd);
				bFoo = m_AcceRegionPathVerifier->DDAVerify(ptPieceStart.X, ptPieceStart.Y, ptPieceEnd.X, ptPieceEnd.Y);
			}

			//Console::Write(k.ToString());

			DateTime timeEnd = System::DateTime::Now;
			TimeSpan timeSpan = timeEnd - timeStart;

			if (bPass)
			{
				Form1::GetInstance()->AddLog("Pass DDA test, take time " + timeSpan.TotalMilliseconds.ToString(), true);
			}
			else
			{
				Form1::GetInstance()->AddLog("Deny by DDA test, take time " + timeSpan.TotalMilliseconds.ToString(), true);
			}
		}

		///////////////////
		// 使用 uint64 piece 检查
		///////////////////

		//bool bTempPass = m_pPathVerifier->VerifiyWalkable(ptPieceStart.X, ptPieceStart.Y, ptPieceEnd.X, ptPieceEnd.Y);

		//DateTime timeStartPiece = System::DateTime::Now;
		//for (int i=0; i<m_iTestTimes; i++)
		//{
		//	bFoo = m_pPathVerifier->VerifiyWalkable(ptPieceStart.X, ptPieceStart.Y, ptPieceEnd.X, ptPieceEnd.Y);
		//}

		//DateTime timeEndPiece = System::DateTime::Now;
		//TimeSpan timeSpanPiece = timeEndPiece - timeStartPiece;

		//Console::Write(bFoo.ToString());

		//if (bTempPass)
		//{
		//	Form1::GetInstance()->AddLog("Pass piece template test, take time " + timeSpanPiece.TotalMilliseconds.ToString(), true);
		//}
		//else
		//{
		//	Form1::GetInstance()->AddLog("Deny by piece template test, take time " + timeSpanPiece.TotalMilliseconds.ToString(), true);
		//}
		//Form1::GetInstance()->AddLog("===========================", false);

		//if (bPass != bTempPass)
		//{
		//	MessageBox::Show("测试结果不同,请检查.");
		//}

		///////////////////
		// 使用 access distance 检查
		///////////////////
		if (m_pDistPathVerifier != NULL)
		{
			bool bDistPass = m_pDistPathVerifier->VerifiyWalkable(ptPieceStart.X, ptPieceStart.Y, ptPieceEnd.X, ptPieceEnd.Y);

			DateTime timeStartDist = System::DateTime::Now;
			for (int i=0; i<m_iTestTimes; i++)
			{
				bFoo = m_pDistPathVerifier->VerifiyWalkable(ptPieceStart.X, ptPieceStart.Y, ptPieceEnd.X, ptPieceEnd.Y);
			}

			DateTime timeEndDist = System::DateTime::Now;
			TimeSpan timeSpanDist = timeEndDist - timeStartDist;

			Console::Write(bFoo.ToString());

			if (bDistPass)
			{
				Form1::GetInstance()->AddLog("Pass access distance test, take time " + timeSpanDist.TotalMilliseconds.ToString(), true);
			}
			else
			{
				Form1::GetInstance()->AddLog("Deny by access distance test, take time " + timeSpanDist.TotalMilliseconds.ToString(), true);
			}
			Form1::GetInstance()->AddLog("===========================", false);

			if (bPass != bDistPass)
			{
				MessageBox::Show("测试结果不同,请检查.");
			}
		}

		///////////////////
		// 使用 access region 检查
		///////////////////
		if (m_AcceRegionPathVerifier != NULL)
		{
			bool bRegionPass = m_AcceRegionPathVerifier->VerifiyWalkable(ptPieceStart.X, ptPieceStart.Y, ptPieceEnd.X, ptPieceEnd.Y);
			DateTime timeStart = System::DateTime::Now;
			for (int i=0; i<m_iTestTimes; i++)
			{
				bFoo = m_AcceRegionPathVerifier->VerifiyWalkable(ptPieceStart.X, ptPieceStart.Y, ptPieceEnd.X, ptPieceEnd.Y);
			}

			DateTime timeEnd = System::DateTime::Now;
			TimeSpan timeSpan = timeEnd - timeStart;

			Console::Write(bFoo.ToString());

			if (bRegionPass)
			{
				Form1::GetInstance()->AddLog("Pass access region test, take time " + timeSpan.TotalMilliseconds.ToString(), true);
			}
			else
			{
				Form1::GetInstance()->AddLog("Deny by access distance test, take time " + timeSpan.TotalMilliseconds.ToString(), true);
			}
			Form1::GetInstance()->AddLog("===========================", false);

			if (bPass != bRegionPass)
			{
				MessageBox::Show("测试结果不同,请检查.");
			}

		}

		///////////////////
		// BlockQuad 检查
		///////////////////
		if (m_bRightFirstClick)
		{
			// 已经存在 block quad 了
			int iMinX = Math::Min(m_ptBQStartPoint.X, m_ptBQEndPoint.X);
			int iMaxX = Math::Max(m_ptBQStartPoint.X, m_ptBQEndPoint.X);
			int iMinY = Math::Min(m_ptBQStartPoint.Y, m_ptBQEndPoint.Y);
			int iMaxY = Math::Max(m_ptBQStartPoint.Y, m_ptBQEndPoint.Y);
			CPathVerifier::CBlockQuad bq(0, iMinX, iMinY, iMaxX, iMaxY);
			m_AcceRegionPathVerifier->SetBlockQuads(1, &bq);
			m_AcceRegionPathVerifier->SetBlockQuadEnable(0, true);

			bool bResult = 
				m_AcceRegionPathVerifier->BlockQuadTest(ptPieceStart.X, ptPieceStart.Y, ptPieceEnd.X, ptPieceEnd.Y);
			if (!bResult)
			{
				MessageBox::Show("BlockQuad 测试失败");
			}

		}

		m_bFirstClick = true;
		this->Invalidate();
	}

	System::Void MapViewer::MapViewer_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

		// 左键设置检测路径
		if (e->Button == Windows::Forms::MouseButtons::Left)
		{
			if (m_bFirstClick)
			{
				m_ptStartPoint = _WndPointToPiecePoint(Point(e->X, e->Y));

				m_ptEndPoint.X = -1;
				m_ptEndPoint.Y = -1;

				m_bFirstClick = false;
				this->Invalidate();
			}
			else
			{
				m_ptEndPoint = _WndPointToPiecePoint(Point(e->X, e->Y));

				VerifyWalkable(m_ptStartPoint, m_ptEndPoint);
				//m_bFirstClick = !m_bFirstClick;		
			}
		}
		else if (e->Button == Windows::Forms::MouseButtons::Right)
		{
			// 右键设置 BlockQuad
			if (m_bRightFirstClick)
			{
				m_ptBQStartPoint = _WndPointToPiecePoint(Point(e->X, e->Y));

				m_ptBQEndPoint.X = -1;
				m_ptBQEndPoint.Y = -1;

				m_bRightFirstClick = false;
				//this->Invalidate();
			}
			else
			{
				m_ptBQEndPoint = _WndPointToPiecePoint(Point(e->X, e->Y));
				m_bRightFirstClick = true;
				this->Invalidate();
			}
		}

	}
	System::Void MapViewer::MapViewer_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 Point pt = _WndPointToPiecePoint(Point(e->X, e->Y));
				 Form1::GetInstance()->Text = "" + pt.X + ", " + pt.Y;
			 }
}