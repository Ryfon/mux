﻿#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::IO;

namespace PathVerifier {

	public ref class CMapInfo
	{
		//~CMapInfo() {delete[] m_arrMapInfo;}
	public:
		int m_iMapID;
		int m_iNumGridX;
		int m_iNumGridY;
		unsigned int* m_arrMapInfo;
		//array<unsigned int>^ m_arrMapInfo;
	};

	// 地图管理
	public ref class CMapManager
	{
	public:
		CMapManager(void)
		{
			m_htMaps = gcnew Hashtable();
		}

		// 加载地图文件, 返回 map idi
		CMapInfo^ LoadMap(String^ strFile);

		// 根据 map id 获取 map info
		CMapInfo^ GetMapInfo(int iMapID);
	private:

		Hashtable^ m_htMaps;	// map id(int) - CMapInfo 
	};


}
