﻿#pragma once
#include "MapManager.h"

#pragma unmanaged
#include "PathVerifier.h"
#include "PieceTemplatePathVerifier.h"
#include "AccessDistancePathVerifier.h"
#include "AccessRegionPathVerifier.h"
#include "math.h"
#include "stdlib.h"
#pragma managed

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Collections::Generic;
using namespace System::Drawing::Drawing2D;
using namespace System::Text;
using namespace System::Windows::Forms;
using namespace System::IO;

namespace PathVerifier {

	/// <summary>
	/// MapViewer 摘要
	/// </summary>
	public ref class MapViewer : public System::Windows::Forms::UserControl
	{
	public:
		MapViewer(void);

	public: 

		CMapInfo^ m_pAccessRegionMap;

		// 载入连通区域，创建 AccessRegion 路径验证
		void SetAccessRegion(CMapInfo^ pAcceRegionMap)
		{
			if (pAcceRegionMap == nullptr) return;

			if (m_AcceRegionPathVerifier != NULL) {delete m_AcceRegionPathVerifier;}
			if (m_pAccessRegionMap != nullptr) {delete[] m_pAccessRegionMap;}

			m_pAccessRegionMap = pAcceRegionMap;

			// 将地图的一维数组转为二维
			unsigned int** ppuiAccessMap = 
				ArrayTo2DimArray(
				pAcceRegionMap->m_arrMapInfo, 
				pAcceRegionMap->m_iNumGridX, 
				pAcceRegionMap->m_iNumGridY);

			unsigned int** ppuiMapInfo = ArrayTo2DimArray(
				m_TagMapInfo->m_arrMapInfo, m_TagMapInfo->m_iNumGridX, m_TagMapInfo->m_iNumGridY);
				
			m_AcceRegionPathVerifier = new CAccessRegionPathVerifier(
				pAcceRegionMap->m_iMapID,
				pAcceRegionMap->m_iNumGridX, 
				pAcceRegionMap->m_iNumGridY, 
				ppuiMapInfo,
				ppuiAccessMap);

		}

		void VerifyWalkable(Point ptPieceStart, Point ptPieceEnd);

		void TestBlockQuad()
		{
			CPathVerifier::CBlockQuad* arrBlockQuad = new CPathVerifier::CBlockQuad[2];
			arrBlockQuad[0] = CPathVerifier::CBlockQuad(0, 100, 100, 200, 200);
			arrBlockQuad[1] = CPathVerifier::CBlockQuad(1, 300, 300, 500, 500);

			m_AcceRegionPathVerifier->SetBlockQuads(2, arrBlockQuad);
			m_AcceRegionPathVerifier->SetBlockQuadEnable(0, true);
			m_AcceRegionPathVerifier->SetBlockQuadEnable(1, true);

			bool bResult = m_AcceRegionPathVerifier->BlockQuadTest(150, 0, 300, 300);

			bResult = m_AcceRegionPathVerifier->BlockQuadTest(150, 0, 0, 150);
			bResult = m_AcceRegionPathVerifier->BlockQuadTest(0, 400, 300, 0);
			bResult = m_AcceRegionPathVerifier->BlockQuadTest(0, 400, 450, 0);
			bResult = m_AcceRegionPathVerifier->BlockQuadTest(100, 0, 400, 400);
		}

		unsigned int** ArrayTo2DimArray(unsigned int* puiOneDim, int iX, int iY)
		{
			unsigned int** ppuiTwoDim = new unsigned int*[iY];
			for (int i=0; i<iY; i++)
			{
				ppuiTwoDim[i] = puiOneDim +(i*iX);
			}

			return ppuiTwoDim;
		}

		property CMapInfo^ TagMapInfo
		{
			CMapInfo^ get()
			{
				return m_TagMapInfo;
			}

			void set(CMapInfo^ pMapInfo)
			{
				if (pMapInfo != nullptr)
				{
					m_TagMapInfo = pMapInfo;

					/////////////////////////////////
					// 模板方法验证器初始化
					if (m_puiPieceBlocks != 0)
					{
						delete[] m_puiPieceBlocks;
						m_puiPieceBlocks = 0;
					}

					int iNumPieces = 
						(pMapInfo->m_iNumGridX/MAP_PIECE_DIM) * (pMapInfo->m_iNumGridY/MAP_PIECE_DIM);

					if (m_puiPieceBlocks != NULL)
					{
						delete[] m_puiPieceBlocks;
						m_puiPieceBlocks = NULL;
					}
					
					m_puiPieceBlocks = new uint64[iNumPieces];

					// 将 map info 转换为 piece block 方式的地图信息
					CPieceTemplatePathVerifier::MapToPieceBlock(
						pMapInfo->m_iNumGridX, 
						pMapInfo->m_iNumGridY, 
						pMapInfo->m_arrMapInfo,
						m_puiPieceBlocks);

					if (m_pPathVerifier != NULL)
					{
						delete m_pPathVerifier;
						m_pPathVerifier = NULL;
					}

					unsigned int** ppuiMapInfo =
						ArrayTo2DimArray(m_TagMapInfo->m_arrMapInfo, 
						m_TagMapInfo->m_iNumGridX, 
						m_TagMapInfo->m_iNumGridY);

					m_pPathVerifier 
						= new CPieceTemplatePathVerifier(
						m_TagMapInfo->m_iMapID, 
						m_TagMapInfo->m_iNumGridX, 
						m_TagMapInfo->m_iNumGridY, 
						ppuiMapInfo,
						m_puiPieceBlocks);

					////////////////////////////
					// 距离查询方法初始化
					DateTime dtStart = DateTime::Now;

					//m_pDistPathVerifier 
					//	= new CAccessDistancePathVerifier(
					//	m_TagMapInfo->m_iMapID, 
					//	m_TagMapInfo->m_iNumGridX,
					//	m_TagMapInfo->m_iNumGridY,
					//	m_TagMapInfo->m_arrMapInfo);

					TimeSpan tsUsed = DateTime::Now - dtStart;
	
					double dMS = tsUsed.TotalMilliseconds;

					// 重绘
					this->Width = m_TagMapInfo->m_iNumGridX * m_iGridSize;
					this->Height = m_TagMapInfo->m_iNumGridY * m_iGridSize;
					this->Invalidate();
				}
			}
		}
		property int GridSize
		{
			void set(int iSize) 
			{
				m_iGridSize = Math::Max(iSize, 1);// 最小为 2
				if (m_TagMapInfo != nullptr)
				{
					this->Width = m_TagMapInfo->m_iNumGridX * m_iGridSize;
					this->Height = m_TagMapInfo->m_iNumGridY * m_iGridSize;
				}

				this->Invalidate();
			}	

			int get() {return m_iGridSize;}
		}



		property int TestTimes
		{
			void set(int iTimes) {m_iTestTimes = iTimes;}
		}
	protected:
		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		~MapViewer()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		void InitializeComponent(void)
		{
			this->SuspendLayout();
			// 
			// MapViewer
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Name = L"MapViewer";
			this->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MapViewer::MapViewer_MouseDown);
			this->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MapViewer::MapViewer_MouseMove);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MapViewer::MapViewer_Paint);
			this->ResumeLayout(false);

		}
#pragma endregion

	public:
		uint64 GetMapPiece(int iPieceID)
		{
			return m_puiPieceBlocks[iPieceID];
		}

		void SetDDATest(bool bTest)
		{
			m_bTestDDA = bTest;
		}
	private:
		CMapInfo^	m_TagMapInfo;		// 目标地图
		uint64*		m_puiPieceBlocks;	// 地图碰撞信息	  
		int			m_iGridSize;		// 地图上每个 grid 占的象素数
		CPieceTemplatePathVerifier* m_pPathVerifier;			// uint64 piece 方法的路径验证
		CAccessDistancePathVerifier* m_pDistPathVerifier;		// grid 所能到达距离 方法的路径验证
		CAccessRegionPathVerifier* m_AcceRegionPathVerifier;	// 连通区域路径验证

		// 寻路相关
		bool		m_bFirstClick;		// 鼠标是否标记点起始点
		Point		m_ptStartPoint;		// 寻路的起始点	, 屏幕坐标
		Point		m_ptEndPoint;

		int			m_iTestTimes;		// 测试次数, 使用较大的测试次数来取得精确时间
		bool		m_bTestDDA;	// 是否使用 dda 测试比较
		ArrayList^	m_arrDDAPath;		// DDA 方法搜索出来的路径

		// BlockQuad 相关
		bool		m_bRightFirstClick;
		Point		m_ptBQStartPoint;		// BlockQuad 第一点
		Point		m_ptBQEndPoint;			// BlockQuad 第二点
		std::vector<CPathVerifier::CBlockQuad*>*	m_arrBlockQuads;

	private:
		// 判断一个 grid 是否是阻挡点
		bool _IsBlock(unsigned int uiMapInfo)
		{
			return  ( uiMapInfo & (1<<20) ) && ( uiMapInfo & (1<<21) );
		}

		// 屏幕坐标转换到 grid 坐标
		Point _WndPointToPiecePoint(Point ptWnd)
		{
			Point ptPiece;
			ptPiece.X = ptWnd.X / m_iGridSize;
			ptPiece.Y = (this->Height - ptWnd.Y - 1) / m_iGridSize;

			return ptPiece;
		}

		// DDA 方法验证
		bool _DDAVerify(ArrayList^ arrPath, Point ptStart, Point ptEnd)
		{
			// 两点距离
			int iDX = ptEnd.X - ptStart.X;
			int iDY = ptEnd.Y - ptStart.Y;

			// 步数
			int iStpes = __max(abs(iDX), abs(iDY));

			// 步进
			float fdx = ((float)iDX) / iStpes;
			float fdy = (float(iDY)) / iStpes;

			// 遍历每一步
			float fCurrX = ptStart.X;
			float fCurrY = ptStart.Y;
			for (int i=0; i<=iStpes; i++, fCurrX+=fdx, fCurrY+=fdy)
			{
				if (i == iStpes)
				{
					fCurrX = ptEnd.X;
					fCurrY = ptEnd.Y;
				}

				int iGridID = ((int)fCurrY) * m_TagMapInfo->m_iNumGridX + (int)fCurrX;

				unsigned int uiGridInfo = m_TagMapInfo->m_arrMapInfo[iGridID];
				if (!(( uiGridInfo & (1<<20) ) && ( uiGridInfo & (1<<21) )))//_IsBlock()
				{
					if (arrPath != nullptr)
					{
						arrPath->Add(Point(fCurrX, fCurrY));
					}
				}
				else
				{
					return false;
				}
			}
			return true;
		}

		// 重写控件绘制函数
		System::Void MapViewer_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
				// 还没有 Tag MapInfo
				if (m_TagMapInfo == nullptr) return;

				Point ptStart = (m_ptStartPoint);
				Point ptEnd = (m_ptEndPoint);

				 // 重写 paint
				 // 清除原画面
				 Graphics^ g = this->CreateGraphics();
				 g->FillRectangle(gcnew SolidBrush(this->BackColor), 0, 0, Width, Height);

				 GraphicsPath^ path = gcnew GraphicsPath(System::Drawing::Drawing2D::FillMode::Winding);
				 GraphicsPath^ pathStart = gcnew GraphicsPath(System::Drawing::Drawing2D::FillMode::Winding);
				 GraphicsPath^ pathEnd = gcnew GraphicsPath(System::Drawing::Drawing2D::FillMode::Winding);
				 GraphicsPath^ pathDDA = gcnew GraphicsPath(System::Drawing::Drawing2D::FillMode::Winding);
				 GraphicsPath^ pathBlockQuad = gcnew GraphicsPath(System::Drawing::Drawing2D::FillMode::Winding);

				 // 绘制 Grid
				 const int BLOCK_MARK = 1;   // 如果 grid 值为 1 就是碰撞
				 int iGridIdx = 0;
				 // 从左下第一个 Grid 开始绘制
				 for (int i = 0; i < m_TagMapInfo->m_iNumGridX; i++)  // 遍历行
				 {
					 for (int j=0; j<m_TagMapInfo->m_iNumGridY; j++)  // 遍历列
					 {

						 if (_IsBlock(m_TagMapInfo->m_arrMapInfo[iGridIdx++]))//
						 {
							 // Grid 左上角的坐标
							 int iX = j * m_iGridSize;
							 int iY = (m_TagMapInfo->m_iNumGridY - i - 1) * m_iGridSize;
							 Rectangle rect = Rectangle(iX, iY, m_iGridSize, m_iGridSize);
							 path->AddRectangle(rect);
						 }

						 if (j==ptStart.X && i==ptStart.Y)
						 {
							 // 绘制起始点
							 int iX = j * m_iGridSize;
							 int iY = (m_TagMapInfo->m_iNumGridY - i - 1) * m_iGridSize;
							 Rectangle rect = Rectangle(iX, iY, m_iGridSize, m_iGridSize);
							 //path->AddRectangle(rect);
							 pathStart->AddRectangle(rect);
						 }
						 else if (j==ptEnd.X && i==ptEnd.Y)
						 {
							 // 绘制终点
							 int iX = j * m_iGridSize;
							 int iY = (m_TagMapInfo->m_iNumGridY - i - 1) * m_iGridSize;
							 Rectangle rect = Rectangle(iX, iY, m_iGridSize, m_iGridSize);
							// path->AddRectangle(rect);
							 pathEnd->AddRectangle(rect);
						 }



					 }
				 }

				 // 绘制 DDA 路径
				 for (int i=0; i<m_arrDDAPath->Count; i++)
				 {
					 Point ptGrid = (Point)m_arrDDAPath[i];
					 int iX = ptGrid.X * m_iGridSize;
					 int iY = (m_TagMapInfo->m_iNumGridY - ptGrid.Y - 1) * m_iGridSize;
					 Rectangle rect = Rectangle(iX, iY, m_iGridSize, m_iGridSize);
					 // path->AddRectangle(rect);
					 pathDDA->AddRectangle(rect);
				 }

				 // 绘制 BlockQuad
				 int iBQLeft = Math::Min(m_ptBQStartPoint.X, m_ptBQEndPoint.X);
				 int iBQRight = Math::Max(m_ptBQStartPoint.X, m_ptBQEndPoint.X);
				 int iBQTop = Math::Max(m_ptBQStartPoint.Y, m_ptBQEndPoint.Y);
				 int iBQBottom = Math::Min(m_ptBQStartPoint.Y, m_ptBQEndPoint.Y);
				 // x, y, width, height
				 Rectangle rectBQ = Rectangle(
					 iBQLeft*m_iGridSize, 
					 (m_TagMapInfo->m_iNumGridY - iBQTop - 1)*m_iGridSize,
					 (iBQRight-iBQLeft+1)*m_iGridSize, (iBQTop-iBQBottom+1)*m_iGridSize);
				 pathBlockQuad->AddRectangle(rectBQ);

				 SolidBrush^ brush = gcnew SolidBrush(Color::Red);
				 g->FillPath(brush, path);

				 brush = gcnew SolidBrush(Color::White);
				 g->FillPath(brush, pathStart);

				 brush = gcnew SolidBrush(Color::Black);
				 g->FillPath(brush, pathEnd);

				 brush = gcnew SolidBrush(Color::Green);
				 g->FillPath(brush, pathDDA);

				 brush = gcnew SolidBrush(Color::DarkKhaki);
				 g->FillPath(brush, pathBlockQuad);

				 // 绘制 piece 分割线
				 Pen^ penLine = gcnew Pen(Color::LightGreen);

				for (int i=0; i<m_TagMapInfo->m_iNumGridY; i++)
				{
					if (i % 8 == 0)
					{
						g->DrawLine(penLine, 0, i*m_iGridSize, m_TagMapInfo->m_iNumGridX*m_iGridSize, i*m_iGridSize);
					}
				}					
				
				for (int i=0; i<m_TagMapInfo->m_iNumGridX; i++)
				{
					if (i % 8 == 0)
					{
						g->DrawLine(penLine, i*m_iGridSize, 0, i*m_iGridSize, m_TagMapInfo->m_iNumGridY*m_iGridSize);
					}
				}
			 }

private: System::Void MapViewer_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);

private: System::Void MapViewer_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
};
}
