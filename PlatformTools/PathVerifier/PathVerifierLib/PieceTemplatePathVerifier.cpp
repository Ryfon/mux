﻿
#include "PieceTemplatePathVerifier.h"
#include "memory.h"

//------------------------------------------------------------------------------
CPieceTemplatePathVerifier::CPieceTemplatePathVerifier(
	int iMapID, int iSizeX, int iSizeY, 
	unsigned int** ppuiMapInfo, const uint64* pMap)
: CPathVerifier(iMapID, iSizeX, iSizeY, ppuiMapInfo), m_pMap(pMap)
{
	m_iNumPiecesX = m_iSizeX / MAP_PIECE_DIM;	// 除8
	m_iNumPiecesY = m_iSizeY / MAP_PIECE_DIM;
	m_pAccTempGen = CMapAccessTemplateGenerator::GetInstance();
}
//------------------------------------------------------------------------------
CPieceTemplatePathVerifier::~CPieceTemplatePathVerifier(void)
{
}
//------------------------------------------------------------------------------
void CPieceTemplatePathVerifier::MapToPieceBlock(int iSizeX, int iSizeY, const unsigned int* piOrigMap, uint64* pMapInPieceBlock)
{
	assert (piOrigMap != 0 && pMapInPieceBlock != 0);

	int iNumPieceX = iSizeX / MAP_PIECE_DIM;
	int iNumPieceY = iSizeY / MAP_PIECE_DIM;

	memset(pMapInPieceBlock, 0, iNumPieceX*iNumPieceY*sizeof(uint64));

	// 按顺序遍历每个 piece
	int iOffsetX = 0;	// 每个 piece 首 grid 相对于原点的偏移
	int iOffsetY = 0;
	int iPieceID = 0;	// piece 在整个地图中的 ID
	for (int i=0; i<iNumPieceY; i++, iOffsetY+=MAP_PIECE_DIM)	// 行
	{
		iOffsetX = 0;
		for (int j=0; j<iNumPieceX; j++, iPieceID++, iOffsetX+=MAP_PIECE_DIM)// 列
		{
			int iLocalID = 0;	// piece 中的 grid id
			// 遍历每个 piece 中的 64 个 grid
			for (int m=0; m<MAP_PIECE_DIM; m++)	// 行
			{
				for (int n=0; n<MAP_PIECE_DIM; n++, iLocalID++) // 列
				{
					int iGridID = (iOffsetY+m)*iSizeX + (iOffsetX+n);	// grid 在整个地图中的 id
					// 判断该点是否可通行
					//21 22位同时为1代表不可通行
					bool bBlock = ( piOrigMap[iGridID] & (1<<20) ) 
						&& ( piOrigMap[iGridID] & (1<<21) );
					if (!bBlock)
					{
						pMapInPieceBlock[iPieceID] |= (0x1ll << iLocalID);
					}

				}
			} // End for each grin in piece
		}
	} // End for each piece
}
