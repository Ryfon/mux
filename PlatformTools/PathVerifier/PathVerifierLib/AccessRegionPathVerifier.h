﻿/** @file AccessRegionPathVerifier.h 
@brief 使用连通区域进行地图可行走验证
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2009-09-27
*
*	取代版本：
*	作    者：
*	完成日期：
*/

#ifndef ACCESSREGIONPATHVERIFIER_H
#define ACCESSREGIONPATHVERIFIER_H

#include "PathVerifier.h"
class CAccessRegionPathVerifier : public CPathVerifier
{
public:
	// 参数说明
	CAccessRegionPathVerifier(int iMapID, int iSizeX, int iSizeY, 
			unsigned int** ppuiMapInfo, unsigned int** ppuiAccessRegion);
	~CAccessRegionPathVerifier(void);

	// 实现父类的验证函数
	virtual inline bool VerifiyWalkable(
		int iStartX, int iStartY, int iEndX, int iEndY, bool bUseExtraDDATest = true);

protected:
	unsigned int** m_ppuiAccessRegion;		// 二维数组, 第一维表示行数，第二维表示列数。
											// 地图每个 grid 对应一个 32 bit 整数，其中每一位对应是否属于某一连通区域。整张地图最多 32 个区域

};

#include "AccessRegionPathVerifier.inl"

#endif