inline bool CAccessRegionPathVerifier::VerifiyWalkable(
			int iStartX, int iStartY, int iEndX, int iEndY, bool bUseExtraDDATest)
{
	bool bResult
		= ((m_ppuiAccessRegion[iStartY][iStartX] & m_ppuiAccessRegion[iEndY][iEndX]) > 0);

	// 如果没有通过测试并且使用额外 dda 测试
	if (!bResult && bUseExtraDDATest)
	{
		bResult = DDAVerify(iStartX, iStartY, iEndX, iEndY);
	}

	return bResult;
}