﻿/** @file PathVerifier.h 
@brief 地图可行走验证, 记录每个 grid 在周围方向所能到达的最大距离
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2009-09-27
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef ACCESSDISTANCEPATHVERIFIER_H
#define ACCESSDISTANCEPATHVERIFIER_H

#include "pathverifier.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
#include <assert.h>

// 测试 0 ~ PI 范围内的多少个方向
#define NUM_AROUND_DIRECTIONS 16

class CAccessDistancePathVerifier :	public CPathVerifier
{
public:
	CAccessDistancePathVerifier(int iMapID, int iSizeX, int iSizeY, unsigned int** ppuiMapInfo);
	virtual ~CAccessDistancePathVerifier(void);

	// 实现虚函数
	virtual inline bool VerifiyWalkable(
		int iStartX, int iStartY, int iEndX, int iEndY);

protected:
	// 计算每个 grid 在周围几个方向的 access distance
	void _MapInfo2AccessDistance(unsigned int** ppuiMapInfo);

	// 初始化 dx, dy 数组
	void _Init();

	// grid 坐标转换为 grid id
	inline int _GridPos2ID(int iX, int iY);

	// 交换两个整数
	inline void _Swap(int& i, int& k);

	// 判断 uiMapInfo 表示的 grid 是否阻挡点
	inline bool _IsBlock(unsigned int uiGridInfo);

	// 用 dda 算法取得沿某方向所能到达的最远距离, 超过 256 时记为256
	// iStartX, iStartY 开始点
	// k 第几个方向
	// pMapInfo 地图信息数组
	inline float _DDAGetAccessDistance(int iStartX, int iStartY, int k, unsigned int** ppuiMapInfo);

	// 获取iX, iY 周围 grid 往 kDir 方向所能到达的最大值
	inline float _GetMaxAccessAround(int iX, int iY, float** ppRealAccDist, int iDir);

	// 获取iX ,iY 往 kDir 方向所能到达的最大值
	inline float _GetAccessDistance(int iX, int iY, float** ppRealAccDist, int iDir);

	// 记录地图每个 Grid 向周围 NUM_AROUNT_DIRECTIONS 方向所能到达的最大距离. 
	// m_ucAccessDistance[id] 为 0 表示该点本身不可行走 
	unsigned char** m_ucAccessDistance;		// [m_iSizeX/2*m_iSizeY/2][NUM_AROUND_DIRECTIONS+1]

	// 每个方向上 dda 算法的步进值
	float m_arrDx[NUM_AROUND_DIRECTIONS+1];
	float m_arrDy[NUM_AROUND_DIRECTIONS+1];

	float m_fDAngle;	// PI / NUM_AROUND_DIRECTIONS
	float m_fHalfDAngle; // m_fDAngle / 2
	int m_iHalfSizeX;	// m_iSizeX / 2
};

#include "AccessDistancePathVerifier.inl"

#endif