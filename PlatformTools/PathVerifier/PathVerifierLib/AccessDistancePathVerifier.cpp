﻿
#include "AccessDistancePathVerifier.h"

//------------------------------------------------------------------------------
CAccessDistancePathVerifier::CAccessDistancePathVerifier(
	int iMapID, int iSizeX, int iSizeY, unsigned int** ppuiMapInfo)
	:	CPathVerifier(iMapID, iSizeX, iSizeY, ppuiMapInfo), 
		m_ucAccessDistance(0), m_iHalfSizeX(iSizeX>>1)
{
	_Init();
	_MapInfo2AccessDistance(ppuiMapInfo);
}
//------------------------------------------------------------------------------
CAccessDistancePathVerifier::~CAccessDistancePathVerifier(void)
{
	if (m_ucAccessDistance == 0) return;
	
	for (int i=0; i<m_iSizeX*m_iSizeY; i++)
	{
		if (m_ucAccessDistance[i] != 0)
		{
			delete[] m_ucAccessDistance[i];
			m_ucAccessDistance[i] = 0;
		}
	}

	delete[] m_ucAccessDistance;
	m_ucAccessDistance = 0;
}
//------------------------------------------------------------------------------
void CAccessDistancePathVerifier::_Init()
{
	float dx = 0.0f, dy = 0.0f;

	// 每次角度的增加值. 弧度
	float fdAngle = M_PI / NUM_AROUND_DIRECTIONS;
	m_fDAngle = fdAngle;
	m_fHalfDAngle = m_fDAngle * 0.5f;

	float fAngle = 0.0f;
	for (int i=0; i<=NUM_AROUND_DIRECTIONS; i++, fAngle+=fdAngle)
	{
		// 根据 fAngle 所在的不同区间计算 dx, dy
		if (fAngle >= 1.75f*M_PI || fAngle < 0.25f*M_PI)
		{
			// 向右的方向, major x
			dx = 1.0f;
			dy = tan(fAngle);
		}
		else if (fAngle >= 0.25f*M_PI && fAngle< 0.75f*M_PI)
		{
			// 向上, major y
			dy = 1.0f;
			dx = 1.0f / tan(fAngle);
		}
		else if (fAngle >= 0.75f*M_PI && fAngle < 1.25f*M_PI)
		{
			// 向左, major x
			dx = -1.0f;
			dy = -tan(fAngle);
		}
		else // fAngle >= 1.25f && fAngle < 1.75f
		{
			// 向下, major y
			dy = -1.0f;
			dx = 1.0f / tan(fAngle);
		}

		m_arrDx[i] = dx;
		m_arrDy[i] = dy;
	}

}
//------------------------------------------------------------------------------
// 计算每个 grid 在周围几个方向的 access distance
void CAccessDistancePathVerifier::_MapInfo2AccessDistance(unsigned int** ppuiMapInfo)
{
	if (m_ucAccessDistance != 0) return; // 初始化过了

	int iNumGrid = m_iSizeY * m_iSizeX;

	// 临时数组, 用于放置每个 Grid 在各方向实际到达的距离
	float** ppRealAccessDistance = new float* [iNumGrid];
	float** ppSmoothAccessDistance = new float*[iNumGrid];

	// 遍历 Grids, 计算每个 grid 在各方向所能到达的最大距离
	for (int i=0, iGridID=0; i<m_iSizeY; i++)	// 行
	{
		for (int j=0; j<m_iSizeX; j++, iGridID++)	// 列
		{
			if (_IsBlock(ppuiMapInfo[i][j]))
			{
				// 该点不可行走
				ppRealAccessDistance[iGridID] = 0;
				continue;
			}

			ppRealAccessDistance[iGridID] = new float[NUM_AROUND_DIRECTIONS+1];

			// 遍历周围 NUM_AROUND_DIRECTIONS 个方向
			for (int k=0; k<=NUM_AROUND_DIRECTIONS; k++)
			{
				float fDist = _DDAGetAccessDistance(j, i, k, ppuiMapInfo);
				ppRealAccessDistance[iGridID][k] = fDist;
			}
		}
	}

	// 对 access distance 进行 smooth 操作. 
	// 每个 grid 都取上下左右4个方向邻居的最大通行距离为自己的最大通行距离
	for (int i=0, iGridID=0; i<m_iSizeY; i++)	// 行
	{
		for (int j=0; j<m_iSizeX; j++, iGridID++)	// 列
		{
			if (ppRealAccessDistance[iGridID] == 0)
			{
				// 该点不可行走
				ppSmoothAccessDistance[iGridID] = 0;
				continue;
			}

			ppSmoothAccessDistance[iGridID] = new float[NUM_AROUND_DIRECTIONS+1];

			// 遍历周围 NUM_AROUND_DIRECTIONS 个方向
			for (int k=0; k<NUM_AROUND_DIRECTIONS+1; k++)
			{
				ppSmoothAccessDistance[iGridID][k] = _GetMaxAccessAround(j, i, ppRealAccessDistance, k);
			}
		}
	}

	// ?? 将 distance 投影到 x 或者 y 轴 ?
	// 将 access distance x, y 方向各缩小为原来的 1/2,
	int iHalfX = m_iSizeX>>1, iHalfY = m_iSizeY>>1;
	m_ucAccessDistance = new unsigned char* [iHalfX * iHalfY];

	for (int i=0, iHalfGridID=0; i<iHalfY; i++)
	{
		for (int j=0; j<iHalfX; j++, iHalfGridID++)
		{
			// 判断周围4个点是否都是 block 点
			if (_IsBlock(ppuiMapInfo[(j<<1)][(i<<1)]) 
				&& _IsBlock(ppuiMapInfo[(j<<1)+1][(i<<1)])
				&& _IsBlock(ppuiMapInfo[(j<<1)][(i<<1)+1])
				&& _IsBlock(ppuiMapInfo[(j<<1)+1][(i<<1)+1]))
			{
				m_ucAccessDistance[iHalfGridID] = 0;
				continue;
			}

			m_ucAccessDistance[iHalfGridID] = new unsigned char[NUM_AROUND_DIRECTIONS+1];

			// 遍历周围 NUM_AROUND_DIRECTIONS 个方向
			for (int k=0; k<=NUM_AROUND_DIRECTIONS; k++)
			{
				float fMaxAccess = __max(
					_GetAccessDistance((j<<1), (i<<1), ppSmoothAccessDistance, k), 
					_GetAccessDistance((j<<1)+1, (i<<1), ppSmoothAccessDistance, k));
				fMaxAccess = __max(fMaxAccess,
					_GetAccessDistance((j<<1), (i<<1)+1, ppSmoothAccessDistance, k));
				fMaxAccess = __max(fMaxAccess,
					_GetAccessDistance((j<<1)+1, (i<<1)+1, ppSmoothAccessDistance, k));

				m_ucAccessDistance[iHalfGridID][k] = __min(255, (unsigned char)(fMaxAccess*0.5f));
			}
		}
	}

	// 释放临时资源
	for (int i=0; i<iNumGrid; i++)
	{
		if (ppRealAccessDistance[i] != 0)
		{
			delete[] ppRealAccessDistance[i];
			delete[] ppSmoothAccessDistance[i];
			ppRealAccessDistance[i] = 0;
			ppSmoothAccessDistance[i] = 0;

		}
	}
	delete[] ppRealAccessDistance;
	delete[] ppSmoothAccessDistance;

}
//------------------------------------------------------------------------------
