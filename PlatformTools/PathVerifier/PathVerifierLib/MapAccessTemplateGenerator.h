﻿/** @file MapAccessTemplateGenerator.h 
@brief 可行走摸板生成器, 可生成任意两个相临(8x8)小块任意两点间直线可行走需检查的以 int64 表示的模板
		函数均忽略参数合法性检查以提高效率,必须在上下文中保证参数正确
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2009-09-27
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef MAPACCESSTEMPLATEGENERATOR_H
#define MAPACCESSTEMPLATEGENERATOR_H

#include <stdlib.h>
#include <math.h>
#include <assert.h>

#ifndef uint64
typedef unsigned __int64 uint64;
#endif

//#ifndef max
//#define  max __max
//#endif

// 地图每个小块的尺寸
#define MAP_PIECE_DIM 8
// 每个小块中的 grid 数量
#define MAP_NUM_GRIDS_IN_PIECE 64

// 单件
class CMapAccessTemplateGenerator
{
public:

	// 获取唯一实例
	static CMapAccessTemplateGenerator* GetInstance();

	~CMapAccessTemplateGenerator(void);

	// 到相临 piece 所可能的方向
	enum eAccessDirection
	{
		AD_SELF = 0,
		AD_UPLEFT = 1,
		AD_UP = 2,
		AD_UPRIGHT = 3,
		AD_RIGHT = 4,
		AD_MAX = 5,
	};

	// 生成可通行模板, 在构造时候会自动调用,一般不用手动调用
	void GenAccessTemplate(eAccessDirection eDirection = AD_MAX);

protected:
	CMapAccessTemplateGenerator(void);
	static CMapAccessTemplateGenerator* ms_pInstance;

	/**
	*	<BR>功能说明：生成从一个 piece 的 Start 点 到一个相临 piece(偏移 Offset) 的 End 点的 DDA 测试模板
	*	<BR>可访问性：公共
	*	<BR>注    释：abs(iOffsetX) <= 1 && abs(iOffsetY) <= 1
	*	@param	[in]	iStartX	起始点在 start piece 中的坐标
	*	@param	[in]	iStartY
	*	@param	[in]	iEndX	终点在 end piece 中的坐标
	*	@param	[in]	iEndY
	*	@param	[in]	eDir	目标点所在 piece 相对起始点所在 piece 的位置
	*	@param	[out]	uiTemp0	左右,上下相临只有两个模板,斜角相临使用4个模板.从 start piece 开始按顺时针顺序
	*	@param	[out]	uiTemp1
	*	@param	[out]	uiTemp2	
	*	@param	[out]	uiTemp3
	*	@return true - 生成成功		false - 有非法参数,生成失败
	*/ 
	inline bool _DDASetTemplate(int iStartX, int iStartY, int iEndX, int iEndY, 
									eAccessDirection eDir, uint64& uiTemp0);
	inline bool _DDASetTemplate(int iStartX, int iStartY, int iEndX, int iEndY, 
									eAccessDirection eDir, uint64& uiTemp0, uint64& uiTemp1);
	inline bool _DDASetTemplate(int iStartX, int iStartY, int iEndX, int iEndY, 
				eAccessDirection eDir, uint64& uiTemp0, uint64& uiTemp1, uint64& uiTemp2, uint64& uiTemp3);

	// 初始化 DDA 系数
	inline void _InitDDAParams(const int iDX, const int iDY, float& dx, float& dy, int& iSteps);

	// 将 uiTag 的第 iWhere 位设置为 1.
	// iWhere = [0, 63], iValue = [0, 1]
	inline void _SetBit(uint64& uiTag, int iWhere);

public:
	// 从当前 piece 到周围任意相临 piece 的模板, 总共占用 416KB 内存
	uint64	m_arrAccTmpSelf[MAP_NUM_GRIDS_IN_PIECE][MAP_NUM_GRIDS_IN_PIECE];		// 相同 piece
	uint64	m_arrAccTmpUpLeft[MAP_NUM_GRIDS_IN_PIECE][MAP_NUM_GRIDS_IN_PIECE][4];	// 到左上, 到斜角可能会经过另外两个 相临 piece 中的一个, 所以有4个 piece 的模板
	uint64	m_arrAccTmpUp[MAP_NUM_GRIDS_IN_PIECE][MAP_NUM_GRIDS_IN_PIECE][2];		// 到上方
	uint64	m_arrAccTmpUpRight[MAP_NUM_GRIDS_IN_PIECE][MAP_NUM_GRIDS_IN_PIECE][4];	// 到右上
	uint64	m_arrAccTmpRight[MAP_NUM_GRIDS_IN_PIECE][MAP_NUM_GRIDS_IN_PIECE][2];	// 到右边
};

#include "MapAccessTemplateGenerator.inl"

#endif // MAPACCESSTEMPLATEGENERATOR_H