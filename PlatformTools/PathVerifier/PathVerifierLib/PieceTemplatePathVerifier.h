﻿/** @file PieceTemplatePathVerifier.h 
@brief 用地块模板方法的地图可行走验证
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2009-09-27
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef PIECETTEMPLATEPATHVERIFIER_H
#define PIECETTEMPLATEPATHVERIFIER_H

#include "PathVerifier.h"
#include "MapAccessTemplateGenerator.h"

/// 验证地图上任意两点是否可直线通行
class CPieceTemplatePathVerifier : public CPathVerifier
{
public:

	/**
	*	<BR>功能说明：构造函数
	*	<BR>可访问性：公共
	*	<BR>注    释：pMap 是用 64 位整数表示的地图上每个 8x8 小块的可行走信息数组.
	*	@param	[in] iMapID			地图编号
	*	@param	[in] iSizeX			地图尺寸, 必须是 8 的整倍数.(实际上是64的整倍数)
	*	@param	[in] iSizeY
	*	@param	[in] pMap			地图可行走信息.
	*	@return 
	*/ 
	CPieceTemplatePathVerifier(int iMapID, int iSizeX, int iSizeY, unsigned int** ppuiMapInfo, const uint64* pMap);
	virtual ~CPieceTemplatePathVerifier(void);

	/**
	*	<BR>功能说明：验证地图上两点间直线是否可行走
	*	<BR>可访问性：公共
	*	<BR>注    释：
	*	@param	[in]	iStartX	起始点
	*	@param	[in]	iStartY
	*	@param	[in]	iEndX	结束点
	*	@param	[in]	iEndY
	*	@param	[in]	bUseExtraDDATest 当测试失败后是否使用额外的 dda 测试
	*	@return	是否可行走 
	*/ 
	virtual inline bool VerifiyWalkable(int iStartX, int iStartY, int iEndX, int iEndY, bool bUseExtraDDATest = false);

	/**
	*	<BR>功能说明： 将地图转换成以 uint64 表示每个 piece 阻挡信息的模式. 1bit/grid
	*	<BR>可访问性：公共
	*	<BR>注    释：512x512 大小的地图占用 32K 内存
	*	@param	[in]	iSizeX	地图尺寸. 64 的整数次倍
	*	@param	[in]	iSizeY
	*	@param	[in]	piOrigMap 原 .map 文件载入的地图信息. 32bit/grid 
	*	@param	[out]	pMapInPieceBlock 转换结果, 事先必须分配好内存
	*	@return	 
	*/ 
	static void MapToPieceBlock(int iSizeX, int iSizeY, const unsigned int* piOrigMap, uint64* pMapInPieceBlock);

	//////////////
	//测试用函数

protected:
	static const int PIECE_SIZE = 8;	// 地图每个小块的尺寸

	// 将地图坐标转换为 piece id
	inline int _MapPosToPieceID(int iX, int iY);

	// 地图坐标转换为 piece 的 local id
	inline int _MapPosToPieceLocalID(int iX, int iY);

	// 判断两相临 piece 上两点是否可通行
	inline bool _CanAccess(int iLastPieceX, int iLastPieceY, int iLastPieceID, int iLastLocalID,
								int iCurrPieceX, int iCurrPieceY, int iCurrPieceID, int iCurrLocalID);

	// 判断两 piece 是否符合模板, 
	inline bool _IsFitTemplat(int iStartPiece, int iEndPiece, 
		CMapAccessTemplateGenerator::eAccessDirection eDir, const uint64* arrTemp);


	int				m_iNumPiecesX;	// x 方向 piece 数量
	int				m_iNumPiecesY;	// y 方向 piece 数量
	const uint64*	m_pMap;			// 地图可行走信息, 每个 uint64 整数记录对应小块(8x8)的可行走信息.
	CMapAccessTemplateGenerator* m_pAccTempGen;	// 可通行模板

};

#include "PieceTemplatePathVerifier.inl"

#endif	// PIECETTEMPLATEPATHVERIFIER_H