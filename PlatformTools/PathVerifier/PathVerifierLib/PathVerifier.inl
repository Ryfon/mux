//-----------------------------------------------------------------
bool CPathVerifier::DDAVerify(int iStartX, int iStartY, int iEndX, int iEndY)
{
	// 两点距离
	int iDX = iEndX - iStartX;
	int iDY = iEndY - iStartY;

	// 步数
	int iStpes = __max(abs(iDX), abs(iDY));

	// 步进
	float fdx = ((float)iDX) / iStpes;
	float fdy = (float(iDY)) / iStpes;

	// 遍历每一步
	float fCurrX = iStartX;
	float fCurrY = iStartY;
	for (int i=0; i<=iStpes; i++, fCurrX+=fdx, fCurrY+=fdy)
	{
		if (i == iStpes)
		{
			fCurrX = iEndX;
			fCurrY = iEndY;
		}

		//int iGridID = ((int)fCurrY) * iNumGridX + (int)fCurrX;

		unsigned int uiGridInfo = m_ppuiMapInfo[(int)(fCurrY)][(int)(fCurrX)];
		if (!(( uiGridInfo & (1<<20) ) && ( uiGridInfo & (1<<21) )))//_IsBlock()
		{
			// can access to here.
		}
		else
		{
			return false;
		}
	}
	return true;
}
//------------------------------------------------------------------------------
inline void CPathVerifier::SetBlockQuads(int iNum, CBlockQuad* pBlockRects)
{
	m_iNumBlockQuads = iNum;
	m_arrBlockQuads = pBlockRects;
	_RefreshNumBlockQuadEnabled();
}
//------------------------------------------------------------------------------
inline void CPathVerifier::SetBlockQuadEnable(int iID, bool bEnable)
{
	for (int i=0; i<m_iNumBlockQuads; i++)
	{
		if (m_arrBlockQuads[i].m_iID == iID)
		{
			m_arrBlockQuads[i].SetEnable(bEnable);
		}
	}

	_RefreshNumBlockQuadEnabled();
}
//------------------------------------------------------------------------------
inline void CPathVerifier::_RefreshNumBlockQuadEnabled()
{
	m_iNumBlockQuadEnabled = 0;
	for (int i=0; i<m_iNumBlockQuads; i++)
	{
		if (m_arrBlockQuads[i].m_bEnable)
		{
			m_iNumBlockQuadEnabled++;
		}
	}
}
//------------------------------------------------------------------------------
inline bool CPathVerifier::BlockQuadTest(int iStartX, int iStartY, int iEndX, int iEndY)
{

	if (m_iNumBlockQuadEnabled == 0) return true;	// 开启的 BlockQuad 数量为0

	// 遍历所有 BlockQuad, 将开启的与 线段测试
	for (int i=0; i<m_iNumBlockQuads; i++)
	{
		if (m_arrBlockQuads[i].m_bEnable)
		{
			if (!_BlockQuadTest(iStartX, iStartY, iEndX, iEndY, m_arrBlockQuads + i))
			{
				return false;
			}
		}
	}

	return true;
}
//------------------------------------------------------------------------------
inline bool CPathVerifier::_BlockQuadTest(
	int iStartX, int iStartY, int iEndX, int iEndY, CBlockQuad* pTagBlockQuad)
{
	// 2D 线段与包围盒 碰撞测试， 使用类似 Doom3 SDK 的算法

	// 直线的中心点
	int iLineCenterX = (iStartX+iEndX) >> 1;
	int iLineCenterY = (iStartY + iEndY) >> 1;

	// 直线的方向
	int iLineDirX = iLineCenterX - iStartX;
	int iLineDirY = iLineCenterY - iStartY;

	// 从 aabb center 到 line center 向量
	int iDirX = iLineCenterX - pTagBlockQuad->m_iCenterX;
	int iDirY = iLineCenterY - pTagBlockQuad->m_iCenterY;

	// 1. 将线段投影到坐标轴上
	int ld[2];	// 线段在坐标轴上的投影长度
	ld[0] = abs(iLineDirX);
	if (abs(iDirX) > pTagBlockQuad->m_iExtendX + ld[0])
	{
		return true;
	}

	ld[1] = abs(iLineDirY);
	if (abs(iDirY) > pTagBlockQuad->m_iExtendY + ld[1])
	{
		return true;
	}

	// 2. 将 aabb 投影到 与线段垂直的 separate axis
	int iSeparateAxisX = iLineDirY;
	int iSeparateAxisY = -iLineDirX;

	// iDir 投影到 sepatate axis 的长度
	int iDirProj = labs(iDirX*iSeparateAxisX + iDirY*iSeparateAxisY);
	int iQuadProj = labs(pTagBlockQuad->m_iExtendX*iSeparateAxisX) 
		+ abs(pTagBlockQuad->m_iExtendY*iSeparateAxisY);

	return (iDirProj > iQuadProj);

	
}
//------------------------------------------------------------------------------