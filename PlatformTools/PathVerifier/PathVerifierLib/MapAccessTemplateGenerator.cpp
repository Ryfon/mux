﻿
#include "MapAccessTemplateGenerator.h"
#include <memory>

CMapAccessTemplateGenerator* CMapAccessTemplateGenerator::ms_pInstance = 0;

CMapAccessTemplateGenerator::CMapAccessTemplateGenerator(void)
{
	// 清空模板数组
	memset(m_arrAccTmpSelf, 0, sizeof(m_arrAccTmpSelf));
	memset(m_arrAccTmpUpLeft, 0, sizeof(m_arrAccTmpUpLeft));
	memset(m_arrAccTmpUp, 0, sizeof(m_arrAccTmpUp));
	memset(m_arrAccTmpUpRight, 0, sizeof(m_arrAccTmpUpRight));
	memset(m_arrAccTmpRight, 0, sizeof(m_arrAccTmpRight));

	// 生成所有的模板
	GenAccessTemplate();

}
//------------------------------------------------------------------------------
CMapAccessTemplateGenerator::~CMapAccessTemplateGenerator(void)
{
}
//------------------------------------------------------------------------------
CMapAccessTemplateGenerator* CMapAccessTemplateGenerator::GetInstance()
{
	if (ms_pInstance == NULL)
	{
		ms_pInstance = new CMapAccessTemplateGenerator();
	}

	return ms_pInstance;
}
//------------------------------------------------------------------------------
void CMapAccessTemplateGenerator::GenAccessTemplate(eAccessDirection eDirection)
{
	if (eDirection == AD_MAX)
	{
		// 创建所有的模板
		for (int i=0; i<AD_MAX; i++)
		{
			GenAccessTemplate((eAccessDirection)i);
		}
	}
	else
	{
		// 创建某方向的特定摸板
		int iStartID = 0;	// 起始点 id, 在 self piece 中的序号
		// 遍历 piece 中起始点
		for (int iStartY=0; iStartY<MAP_PIECE_DIM; iStartY++)
		{
			for (int iStartX=0; iStartX<MAP_PIECE_DIM; iStartX++, iStartID++)
			{
				int iEndID = 0;
				// 遍历终点
				for (int iEndY=0; iEndY<MAP_PIECE_DIM; iEndY++)
				{
					for (int iEndX=0; iEndX<MAP_PIECE_DIM; iEndX++, iEndID++)
					{
						if (eDirection == AD_SELF)
						{
							_DDASetTemplate(iStartX, iStartY, iEndX, iEndY, eDirection, m_arrAccTmpSelf[iStartID][iEndID]);
						}
						else if (eDirection == AD_UP)
						{
							_DDASetTemplate(iStartX, iStartY, iEndX, iEndY, eDirection, m_arrAccTmpUp[iStartID][iEndID][0], m_arrAccTmpUp[iStartID][iEndID][1]);
						}
						else if (eDirection == AD_RIGHT)
						{
							_DDASetTemplate(iStartX, iStartY, iEndX, iEndY, eDirection, m_arrAccTmpRight[iStartID][iEndID][0], m_arrAccTmpRight[iStartID][iEndID][1]);
						}
						else if (eDirection == AD_UPLEFT )
						{
							_DDASetTemplate(iStartX, iStartY, iEndX, iEndY, eDirection, 
								m_arrAccTmpUpLeft[iStartID][iEndID][0], m_arrAccTmpUpLeft[iStartID][iEndID][1],
								m_arrAccTmpUpLeft[iStartID][iEndID][2], m_arrAccTmpUpLeft[iStartID][iEndID][3]);
						}
						else if (eDirection == AD_UPRIGHT)
						{
							_DDASetTemplate(iStartX, iStartY, iEndX, iEndY, eDirection, 
								m_arrAccTmpUpRight[iStartID][iEndID][0], m_arrAccTmpUpRight[iStartID][iEndID][1],
								m_arrAccTmpUpRight[iStartID][iEndID][2], m_arrAccTmpUpRight[iStartID][iEndID][3]);
						}
						
					}
				}// End for each end point
			}
		} // End for each start point
	}
}