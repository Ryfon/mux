﻿
#include "AccessRegionPathVerifier.h"

CAccessRegionPathVerifier::CAccessRegionPathVerifier(
	int iMapID, int iSizeX, int iSizeY, 
	unsigned int** ppuiMapInfo, unsigned int** ppuiAccessRegion)
	: CPathVerifier(iMapID, iSizeX, iSizeY, ppuiMapInfo),
		m_ppuiAccessRegion(ppuiAccessRegion)
{
}
//----------------------------------------------------------------
CAccessRegionPathVerifier::~CAccessRegionPathVerifier(void)
{
}
//----------------------------------------------------------------

