﻿#include "WinDecryptBuffer.h"

CWinDecryptBuffer::CWinDecryptBuffer(void)
{
	Create();
}

CWinDecryptBuffer::~CWinDecryptBuffer(void)
{
	Destroy();
}

CWinDecryptBuffer& CWinDecryptBuffer::GetInstance()
{
	static CWinDecryptBuffer inst;
	return inst;
}

bool CWinDecryptBuffer::Create()
{
	//定义固定密码
	const PCHAR szPSW = "password";	


	//获得CSP句柄
	if ( CryptAcquireContext(
		&m_hCryptProv, 
		NULL, 
		MS_ENHANCED_PROV, 
		PROV_RSA_FULL, 
		0
		) )
	{
		//MESSAGE_PRINT("获得CSP句柄OK！");
	}
	else
	{
		if ( CryptAcquireContext(
			&m_hCryptProv,
			NULL,
			NULL,
			PROV_RSA_FULL,
			CRYPT_NEWKEYSET			//创建密钥
			) )
		{
			//MESSAGE_PRINT("获得CSP句柄OK！");
		}
		else
		{
			MessageBox(NULL,"获得CSP句柄 FAILED","ERROR",MB_OK);
			return false;
		}
	}

	//创建hash对象
	if ( CryptCreateHash(
		m_hCryptProv,
		CALG_MD5,
		0,
		0,
		&m_hHash
		))
	{
		//MESSAGE_PRINT("创建hash句柄OK！");		
	}
	else
	{
		MessageBox(NULL,"创建hash句柄FAILED","ERROR",MB_OK);
		return false;
	}

	if ( CryptHashData(
		m_hHash,
		(BYTE*) szPSW,
		(DWORD)strlen( szPSW ),
		0
		) ) 
	{
		//MESSAGE_PRINT("获得HASH句柄OK！");
	}
	else
	{
		MessageBox(NULL,"获得HASH句柄FAILED","ERROR",MB_OK);
		return false;
	}

	// Derive a session key from the hash object. 
	if(CryptDeriveKey(
		m_hCryptProv, 
		ENCRYPT_ALGORITHM, 
		m_hHash, 
		KEYLENGTH, 
		&m_hKey))
	{
		//MESSAGE_PRINT("An encryption key is derived from the password hash.");
	}
	else
	{
		MessageBox(NULL,"CryptDeriveKey FAILED","ERROR",MB_OK);
		return false;
	}
	//-------------------------------------------------------------------
	// Destroy hash object. 
	if(m_hHash) 
	{
		if(!(CryptDestroyHash(m_hHash)))
		{
			MessageBox(NULL,"CryptDestroyHash FAILED","ERROR",MB_OK);
			return false;
		}
		m_hHash = 0;
	}
	return true;
}

void CWinDecryptBuffer::Destroy()
{
	//销毁临时数据
	if ( m_hKey )
	{
		CryptDestroyKey( m_hKey );
	}
	if ( m_hHash )
	{
		CryptDestroyHash( m_hHash );
	}
	if ( m_hCryptProv )
	{
		CryptReleaseContext( m_hCryptProv, 0 );
	}
}

bool CWinDecryptBuffer::DoConvertBuffer(const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize)
{
	if (NULL == szBuffer)
	{
		//ERROR_PRINT("解密源BUFFER为NULL:API.CPP-DecryptBuffer",NULL);
		return false;
	}
	if ( nDecryptBufferSize > nBufferSize )
	{
		//ERROR_PRINT("解密解密BUFFER大于源BUFFER:API.CPP-DecryptBuffer",NULL);
		return false;
	}

	if (NULL == pDestBuffer)
	{
		//ERROR_PRINT("解密目标BUFFER为NULL:API.CPP-DecryptBuffer",NULL);
		return false;
	}
	if ( memcpy_s( pDestBuffer,nBufferSize,szBuffer,nBufferSize ) )
	{
		//ERROR_PRINT("解密拷贝BUFFER出错:API.CPP-DecryptBuffer",NULL);
		return false;
	}
	
	//加密 解密nDestBufferSize/8 * 8 字节
	DWORD dwCount = (DWORD)nDecryptBufferSize / ENCRYPT_BLOCK_SIZE * ENCRYPT_BLOCK_SIZE;
	if ( !CryptDecrypt(
		m_hKey,
		0,
		true,
		0,
		pDestBuffer,
		&dwCount
		) )
	{
		//ERROR_PRINT("解密出错:API.CPP-DecryptBuffer",NULL);
		return false;
	}

	return true;
}