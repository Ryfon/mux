﻿/** @file ConvertBuffer.h
@brief 
<pre>
*	Copyright (c) 2009，北京金刚游科技有限公司
*	All rights reserved.
*
*	当前版本：
*	作    者：KingKong
*	完成日期：2009-04-02
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#include <windows.h>
#include "PackageEntryDef.h"
#pragma once

class CConvertBuffer
{
public:
	CConvertBuffer(void);
	virtual ~CConvertBuffer(void);

	virtual bool Create();
	virtual void Destroy();
	virtual bool DoConvertBuffer(const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize);

};
