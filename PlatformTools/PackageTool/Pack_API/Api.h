﻿/** @file Api.h 
@brief 文件打包：公用变量、函数定义
<pre>
*	Copyright (c) 2009，北京金刚游科技有限公司
*	All rights reserved.
*
*	当前版本：
*	作    者：KingKong
*	完成日期：2009
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#if	!defined  _API_H_
#define _API_H_

#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <windows.h>
#include <Wincrypt.h>
#include <io.h>
#include <conio.h>
#include "zlib.h"
#include "PackageEntryDef.h"
#include "tinyxml.h"
#include "SmartPointer.h"
//  [4/2/2009 hemeng]
#include "WinDecryptBuffer.h"
#include "WinEncryptBuffer.h"
using namespace std;

#pragma warning(push)
#pragma warning( disable: 4251 )

//包头信息的大小
#define SIZE_PACK_HEAD	sizeof(SPackageFileHeader)

//错误提示及信息提示
//////////////////////////////////////////////////////////////////////////
// 修改报错 [1/14/2010 hemeng]
inline void Debug_WriteLog_Package( const char * szOutputInfo, ... )
{

	char szBuffer[ 8 * 1024 ];
	va_list valist;
	va_start( valist, szOutputInfo );
	vsnprintf_s( szBuffer, sizeof( szBuffer ), _TRUNCATE, szOutputInfo, valist );
	va_end( valist );

#ifdef UNICODE
	USES_CONVERSION;
	OutputDebugStringW( A2W(szBuffer));
#else
	OutputDebugStringA( szBuffer );
#endif // !UNICODE
	//OutputDebugString( szBuffer );

}

typedef void (*Debug_WriteLog_Def)( const char * szOutputInfo, ... );

extern Debug_WriteLog_Def s_pfnDebugWriteLog;


#define ERROR_PRINT(msg,param1) if (s_pfnDebugWriteLog){(*s_pfnDebugWriteLog)("%s,%s", msg, param1);}

//#ifdef CODE_IN_PACKAPI
//#define ERROR_PRINT(msg,filePath) do {  char szInfo[MAX_PATH * 3];\
//	sprintf_s( szInfo, "%s %s\n", msg,filePath );\
//	MessageBox(NULL,szInfo,"Error",MB_OK);} while(0)
//#else
//#define ERROR_PRINT(msg,filePath)do {   char szInfo[MAX_PATH * 3];\
//	sprintf_s( szInfo, "%s %s\n", msg,filePath );\
//	OutputDebugString(szInfo); } while(0)
//#endif //#ifndef CODE_IN_PACKAPE

//////////////////////////////////////////////////////////////////////////
//
#define MESSAGE_PRINT(msg) do { /*cerr << msg << endl;*/ } while(0)

//配置表路径
#define CONFIG_FILE_NAME	"package_config.xml"

//---------------------------------------------------------------
//压缩API宏
//小于10K的文件不进行压缩
#define ZIP_BLOCK_SIZE		sizeof(gz_header) + 10

//文件类型
enum EFileType	
{
	FT_PNG,
	FT_DDS,
	FT_PFX,
	FT_KFM,
	FT_KF,
	FT_XML,
	FT_NIF,
	FT_IFL,
	FT_TXT,
	FT_TGA,
	FT_OTHER,
	//文件类型总数
	MAX_FT_COUNT
};

//压缩类型
enum EEncryptType
{
	ET_HEADER,					//仅头文件加密
	ET_ZIP,						//采用ZIP方式压缩
	ET_ZIP_HEADER,				//采用zip压缩，并且加密文件头
	ET_NONE,					//不压缩，不加密
	//压缩类型总数
	MAX_EE_COUNT
};

//文件头信息
struct SPackageFileHeader
{
	UINT nType;					//文件类型"PK"
	UINT nVersion;				//包格式版本号
	// 添加内容版本信息 [4/14/2009 hemeng]
	FILETIME nContentVersion; //内容版本号
	UINT nFileSystemInfoPos;	//文件信息描述存储头所在位置	
};

//文件信息描述存储描述头
struct SFileSystemInfo 
{
	string	strRootDir;
	UINT	nFileInfoPos;
	UINT	nFileInfoNum;
	UINT	nIdleInfoPos;
	UINT	nIdleInfoNum;
};

//碎片空间信息
struct PACKAGE_ENTRY SPackageIdleSpaceInfo
{
	UINT nPos;			//空间位置
	UINT nRegionSize;	//空间大小
};

//记录包文件描述信息
//写文件时统一每个文件描述的长度为MAX_PATH
struct PACKAGE_ENTRY SPackageFileInfo 
{
	string		 szFilename;					//文件名 最大 MAX_PATH
	string		 szFullPathName;				//全路径名 包含文件名（带.类型 i.e. a.nif） 均为相对路径
	EFileType	 eFileType;						//文件类型
	EEncryptType eEncryptType;					//文件加密类型
	UINT		 nPos;							//文件所在位置
	UINT		 nFileSize;						//文件大小 如果文件有压缩则是压缩后的大小
	UINT		 nFileOrgSize;					//文件压缩前的大小

	bool operator == (SPackageFileInfo& srcFileInfo)
	{
		if (szFilename != srcFileInfo.szFilename)
		{
			return false;
		}

		if (szFullPathName != srcFileInfo.szFullPathName)
		{
			return false;
		}

		if (eFileType != srcFileInfo.eFileType)
		{
			return false;
		}

		if (eEncryptType != srcFileInfo.eEncryptType)
		{
			return false;
		}

		if (nPos != srcFileInfo.nPos)
		{
			return false;
		}

		if (nFileSize != srcFileInfo.nFileSize)
		{
			return false;
		}

		if (nFileOrgSize != srcFileInfo.nFileOrgSize)
		{
			return false;
		}

		return true;
	}
};

//包目录信息
struct PACKAGE_ENTRY SPackageDirInfo
{
	string							szDirName;					//目录名
	map< string, SPackageFileInfo >	vPackageFileInfos;						//该目录下的文件
	map< string, SPackageDirInfo >	vPackageDirInfos;						//该目录下的目录
};

//公用函数
////////////////////////////////////////////////////////////////////////////////
//函数名:_GetNextPathSegment
//功能:字符串切割，依照给定的截断标志分解字符串
//入口参数:const char* szStr		需要分解的字符串
//			string& strValue	分解后字符段
//出口参数:const char *
////////////////////////////////////////////////////////////////////////////////

const char * _GetNextPathSegment( const char * szStr, string& strValue );
////////////////////////////////////////////////////////////////////////////////
//函数名:_GetPathSegment
//功能:字符串切割，依照给定的截断标志分解字符串
//入口参数:const char* szStr		需要分解的字符串
//			vector<string>& vStrs	分解后字符段
//			char chToken			标志字符，分解字符串的截断标志
//出口参数:无
////////////////////////////////////////////////////////////////////////////////
//void _SplitString( const char * szStr, vector< string >& vStrs, char chToken );
void _GetPathSegment( const char * szPathFile, vector< string >& vPathSegment );

////////////////////////////////////////////////////////////////////////////////
//函数名：StringLower
//功能：小写化输入字符串
//入口参数：string& str 转换的目标字符串
//出口参数：无
////////////////////////////////////////////////////////////////////////////////
PACKAGE_ENTRY void StringLower( string& str );

////////////////////////////////////////////////////////////////////////////////
//函数名：StringUpper
//功能：大写化输入字符串
//入口参数：string& str 转换的目标字符串
//出口参数：无
////////////////////////////////////////////////////////////////////////////////
PACKAGE_ENTRY void StringUpper( string& str );

////////////////////////////////////////////////////////////////////////////////
//函数名:EncryptBuffer
//功能:加密文件
//入口参数: const BYTE* szBuffer		源数据
//			UINT  nBufferSize			源数据大小
//			BYTE* pDestBuffer			目标数据，压缩后数据
//			UINT  nEncrypteBufferSize	目标数据大小	必须大于或等于ENCRYPT_BLOCK_SIZE（8byte）
//			const char* szPSW			加密密码，为8位字符 默认密码为“password”
//出口参数: bool  成功返回ture 失败返回 false
////////////////////////////////////////////////////////////////////////////////
bool EncryptBuffer( const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nEncryptBufferSize );

////////////////////////////////////////////////////////////////////////////////
//函数名:DecryptBuffer
//功能:解密数据
//入口参数:const BYTE* szBuffer			源数据
//			UINT  nBufferSize			源数据大小
//			BYTE* pDestBuffer			目标数据，解压后数据
//			UINT  nDecrypteBufferSize	目标数据大小
//			const char* szPSW			解密密码，为8位字符 默认密码为“password”
//出口参数: bool  成功返回ture 失败返回 false
////////////////////////////////////////////////////////////////////////////////
bool DecryptBuffer( const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize);

////////////////////////////////////////////////////////////////////////////////
//函数名:ZipBuffer
//功能:压缩数据
//入口参数: const BYTE * szBuffer		源数据
//			UINT nBufferSize			源数据大小
//			BYTE * pDestBuffer			目标数据
//			UINT nDestBufferSize		目标数据大小，不得小于源数据大小的1%
//出口参数: UINT 压缩后文件大小
////////////////////////////////////////////////////////////////////////////////
UINT ZipBuffer( const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDestBufferSize );

////////////////////////////////////////////////////////////////////////////////
//函数名:UnZipBuffer
//功能:解压数据
//入口参数: const BYTE * szBuffer		源数据
//			UINT nBufferSize			源数据大小
//			BYTE * pDestBuffer			目标数据
//			UINT nDestBufferSize		目标数据大小，不得小于源数据大小
//出口参数:UINT 解压后数据大小
////////////////////////////////////////////////////////////////////////////////
UINT UnZipBuffer( const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDestBufferSize );

////////////////////////////////////////////////////////////////////////////////
//函数名:GetFileType
//功能:获得文件类型，根据文件后缀获得文件类型
//入口参数:string strFilePath	文件名
//出口参数:EFileType 文件类型
////////////////////////////////////////////////////////////////////////////////
EFileType GetFileType(string strFilePath);

////////////////////////////////////////////////////////////////////////////////
//函数名:GetFileEncryptType
//功能:根据文件类型，配置文件压缩及加密类型
//入口参数: EFileType nType	文件类型
//			UINT nFileSize  文件大小
//出口参数:EEncryptType 文件压缩及加密类型
////////////////////////////////////////////////////////////////////////////////
//EEncryptType GetFileEncryptType( EFileType nType, UINT nFileSize );

////////////////////////////////////////////////////////////////////////////////
//函数名:GetBufferSize
//功能:获得指定文件实际大小
//入口参数:string strFileName	文件名
//出口参数:UINT	文件大小
////////////////////////////////////////////////////////////////////////////////
int GetBufferSize( string strFileName );

////////////////////////////////////////////////////////////////////////////////
//函数名:GetBufferSize
//功能:获得指定文件实际大小
//入口参数:FILE* pFile 文件流指针
//出口参数:UINT	文件大小
////////////////////////////////////////////////////////////////////////////////
int GetBufferSize( FILE* pFile );



////////////////////////////////////////////////////////////////////////////////
//函数名:CompareFileInfo
//功能: 文件信息结构比较函数，比较文件描述中的nPos值
//入口参数: SPackageFileInfo sFileInfoFir	源文件信息
//			SPackageFileInfo sFileInfoSec	比对文件信息
//出口参数:bool	源小于比对返回ture，反之返回false
////////////////////////////////////////////////////////////////////////////////
bool	CompareFileInfo( SPackageFileInfo sFileInfoFir,SPackageFileInfo sFileInfoSec );

////////////////////////////////////////////////////////////////////////////////
//函数名:CompareIdleSpace
//功能: 碎片空间结构比较函数，比较碎片空间描述中的nPos值
//入口参数: SPackageIdleSpaceInfo sIdleFir	源文件信息
//			SPackageIdleSpaceInfo sIdleSec	比对文件信息
//出口参数:bool	源小于比对返回ture，反之返回false
////////////////////////////////////////////////////////////////////////////////
bool	CompareIdleSpace( SPackageIdleSpaceInfo sIdleFir, SPackageIdleSpaceInfo sIdleSec );

////////////////////////////////////////////////////////////////////////////////
//函数名:ReadFileToBuffer
//功能: 读取文件数据到buffer
//入口参数: BYTE* pBuffer			目标buffer
//			UINT nBufferSize		目标buffer大小
//			const char* pFileName	文件绝对路径+文件名
//出口参数: bool	成功返回ture，反之返回false
////////////////////////////////////////////////////////////////////////////////
bool	ReadFileToBuffer( BYTE* pBuffer,UINT nBufferSize, const char* pFileName );

////////////////////////////////////////////////////////////////////////////////
//函数名:FindAllFiles
//功能: 将指定文件夹下的所有文件压入容器
//入口参数: string szPath				文件夹路径
//			vector<string>& vAllFiles	目标容器
//出口参数: bool	成功返回ture，反之返回false
////////////////////////////////////////////////////////////////////////////////
PACKAGE_ENTRY bool	FindAllFiles( string szPath, vector<string>& vAllFiles );


////////////////////////////////////////////////////////////////////////////////
//函数名:IsRoot
//功能: 判断当前路径是否为根目录	
//入口参数: string szPath	目录路径
//出口参数: bool	成功返回ture，反之返回false
//说明:
////////////////////////////////////////////////////////////////////////////////
bool	IsRoot( string szPath );

////////////////////////////////////////////////////////////////////////////////
//函数名:_GetRelativePath
//功能: 获取相对路径
//入口参数: const char * szBasePath	基路径
//			const char * szFullPath 目标路径
//出口参数: string	相对路径
//说明:
////////////////////////////////////////////////////////////////////////////////
string _GetRelativePath( const char * szBasePath, const char * szFullPath );

////////////////////////////////////////////////////////////////////////////////
//函数名:CompareFileTime
//功能: 获取相对路径
//入口参数: FILETIME fileTime1 被比较的时间
//			FILETIME fileTime2 比较的时间
//出口参数: int 大于返回1，小于返回-1，等于返回0
//说明:
////////////////////////////////////////////////////////////////////////////////
int CompareFileTime(FILETIME fileTime1,FILETIME fileTime2);

// 检查文件路径中的文件夹是否存在，如果不存在则创建 [8/10/2009 hemeng]
bool		_ValidateDirs(string strAbsFileName);


#pragma warning(pop)


#endif
//end of api.h