﻿#include "SimpleEncoderBuffer.h"

CSimpleEncoderBuffer::CSimpleEncoderBuffer(void)
{

}

CSimpleEncoderBuffer::~CSimpleEncoderBuffer(void)
{

}

bool CSimpleEncoderBuffer::Create()
{
	return true;
}

void CSimpleEncoderBuffer::Destroy()
{

}

CSimpleEncoderBuffer& CSimpleEncoderBuffer::GetInstance()
{
	static CSimpleEncoderBuffer inst;
	return inst;
}

bool CSimpleEncoderBuffer::DoConvertBuffer(const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize)
{
	return ConvertBuffer( szBuffer, nBufferSize, pDestBuffer, nDecryptBufferSize );
}
