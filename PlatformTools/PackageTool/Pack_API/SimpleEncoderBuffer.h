﻿/** @file SimpleEncoderBuffer.h 
@brief 文件打包：包文件系统 第一版测试
<pre>
*	Copyright (c) 2007，北京金刚游科技有限公司
*	All rights reserved.
*
*	当前版本：2009-04-03 
*	作    者：KingKong
*	完成日期：2009-04-03
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#include "ConvertBuffer.h"

#define SIMPLE_ENCODER_BUFFER( ch ) ( ( ( ch & 0x0f ) << 4 ) | ( ( ch & 0xf0 ) >> 4 ) )

class CSimpleEncoderBuffer :
	public CConvertBuffer
{
public:
	CSimpleEncoderBuffer(void);
	~CSimpleEncoderBuffer(void);

	virtual bool Create();
	virtual void Destroy();
	static CSimpleEncoderBuffer& GetInstance();
	virtual bool DoConvertBuffer(const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize);
	static inline bool ConvertBuffer(const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize)
	{

		if (NULL == szBuffer)
		{
			return false;
		}
		if ( nDecryptBufferSize > nBufferSize )
		{
			return false;
		}

		if (NULL == pDestBuffer)
		{
			return false;
		}
		for ( UINT nIndex = 0; nIndex < nDecryptBufferSize; nIndex ++ )
		{
			pDestBuffer[nIndex] = SIMPLE_ENCODER_BUFFER( szBuffer[nIndex] );
		}
		return true;
	}
private:
};
