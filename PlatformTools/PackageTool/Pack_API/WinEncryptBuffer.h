﻿#pragma once
#include "convertbuffer.h"
#include <Wincrypt.h>

//加密API宏
#define MY_ENCODING_TYPE (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define KEYLENGTH	0x00800000
#define ENCRYPT_ALGORITHM	CALG_RC4

//----------------------------------------------------------------
//加密大小必须是8字节的整倍数
#define ENCRYPT_BLOCK_SIZE	8
#define ENCRYPT_PACK_LENGTH 8 * ENCRYPT_BLOCK_SIZE

class CWinEncryptBuffer :
	public CConvertBuffer
{
public:
	CWinEncryptBuffer(void);
	~CWinEncryptBuffer(void);

	virtual bool Create();
	virtual void Destroy();
	static CWinEncryptBuffer& GetInstance();
	virtual bool DoConvertBuffer(const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize);

private:
	HCRYPTPROV	m_hCryptProv;		//CSP句柄
	HCRYPTKEY	m_hKey;				//key句柄
	HCRYPTHASH	m_hHash;			//hash对象句柄
};
