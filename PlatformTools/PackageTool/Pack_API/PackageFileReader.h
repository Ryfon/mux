﻿/** @file PackageFileReader.h 
@brief 文件打包：包文件读取类 第一版测试
<pre>
*	Copyright (c) 2007，北京金刚游科技有限公司
*	All rights reserved.
*
*	当前版本：2008-08-21 
*	作    者：KingKong
*	完成日期：2008-08-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#if !defined _PACKAGFILEREADER_H_
#define _PACKAGFILEREADER_H_

#include "Api.h"
#include "Package.h"
#include "PackageFileSystem.h"
#include "zlib.h"
#include "DiskFile.h"

class CPackage;

class CPackageFileReader
{

public:
	CPackageFileReader( CPackage* pPackage, SPackageFileInfo* pPackageFileInfo);
	~CPackageFileReader();	

	////////////////////////////////////////////////////////////////////////////////
	//函数名:ReadBuffer
	//功能:读取指定文件数据	
	//入口参数: BYTE* pBuffer						目标数据区指针
	//			SPackageFileInfo* pFileInfo		指定文件描述
	//出口参数: UINT	读取数据的真实大小
	//说明: pBuffer的空间必须大于等于sFileInfo.OrgSize，否则解压后数据会丢失
	////////////////////////////////////////////////////////////////////////////////
	UINT	ReadBuffer( BYTE* pBuffer );


	////////////////////////////////////////////////////////////////////////////////
	//函数名:ReadBuffer
	//功能:根据指定的大小，获取原始文件数据
	//入口参数: BYTE* pBuffer						目标数据区指针
	//			UINT uiBeginPos						要读取的开始位置
	//			UINT uiBytes						要读取的原始文件大小
	//出口参数: UINT	读取数据的真实大小
	//说明: -- by KingKong
	////////////////////////////////////////////////////////////////////////////////
	UINT	ReadBuffer( BYTE * pBuffer, UINT uiBeginPos, UINT uiBytes );
	
	////////////////////////////////////////////////////////////////////////////////
	//函数名:ReadUnZipBuffer
	//功能:读取包中数据
	//入口参数: BYTE* pBuffer	数据指针
	//			SPackageFileInfo* pFileInfo		指定文件描述
	//出口参数: bool	成功返回ture， 失败返回 false
	//说明:读取数据不进行解密及解压
	////////////////////////////////////////////////////////////////////////////////
	bool	ReadUnZipBuffer( BYTE* pBuffer);		

	////////////////////////////////////////////////////////////////////////////////
	//函数名:GetPackageFileInfo
	//功能: 返回当前读取的文件信息描述
	//入口参数: 无
	//出口参数: SPackageFileInfo*	文件信息描述指针
	//说明:
	////////////////////////////////////////////////////////////////////////////////
	SPackageFileInfo*	GetPackageFileInfo()	{ return m_pPackageFileInfo; };


protected:

private:
	CPackageFileReader();
	CPackageFileReader(CPackageFileReader&);
	////////////////////////////////////////////////////////////////////////////////
	//函数名:SetReaderFileInfo
	//功能: 设置当前读取文件的文件描述
	//入口参数: SPackageFileInfo* pFileInfo	源文件描述
	//出口参数: bool
	//说明:
	////////////////////////////////////////////////////////////////////////////////
	/*bool				_SetReaderFileInfo( SPackageFileInfo* pFileInfo );*/

	////////////////////////////////////////////////////////////////////////////////
	//函数名:_DecodeBuffer
	//功能: 解压解密数据
	//入口参数: BYTE* pBuffer				源数据
	//			SPackageFileInfo sFileInfo*	源数据描述
	//出口参数: UINT	解压解密后的数据大小
	//说明:如果失败，返回0
	////////////////////////////////////////////////////////////////////////////////
	UINT				_DecodeBuffer( BYTE* pBuffer, SPackageFileInfo* pFileInfo );

	//记录当前文件位置信息
	SPackageFileInfo*	m_pPackageFileInfo;
	//包文件
	CPackage*			m_pPackage;
	
	///文件缓冲区
	BYTE *				m_pFileBuffer;
	
	BYTE *				m_pDecoderBuffer;
	
	bool				m_bDecrypteBufferForHeaderReady;
	///为header模式指定的解密缓冲区
	BYTE *				m_pDecrypteBufferForHeader;
	///文件缓冲区的起始位置
	UINT				m_nCurBufferBegin;
	///文件缓冲区的大小
	UINT				m_nBufferSize;
	///文件缓冲区的max
	const UINT			m_nBufferMaxSize; 
	///当前文件的指针位置
	UINT				m_nCurFilePos;
	
	///因为只能顺序向下读，所以动态seek成本很大
	z_stream			m_zipStream;

	///从磁盘读取数据
	UINT				ReadBufferFromDisk( BYTE * pBuffer, UINT uiBeginPos, UINT uiBytes );
	///读取被编码过的数据
	UINT				ReadBufferForDecode( BYTE * pBuffer, UINT uiBeginPos, UINT uiBytes );
	
	bool				ReadyBufferDecrypteForHeader();
	///给header加密模式的进行解密
	bool				DoBufferDecrypteForHeader( BYTE * pBuffer, UINT uiBeginPos, UINT uiBytes );
	///从磁盘上将数据解码到内存中，主要处理zip等
	bool				DoDecoderToBuffer( UINT nNextBufferBeginPos );
	///将内存中的数据进行解密处理
	bool				DoBufferDecrypte();

	bool				InitZipStream();

	void				DestroyZipStream();

	int					ReadNextBlockForDecode();

	CDiskFileReader		m_diskFileReader;

};

#endif
//end of _PACKAGFILEREADER_H_