﻿#include "Api.h"
#include "SimpleEncoderBuffer.h"
// add [8/10/2009 hemeng]
#include <direct.h>

#define CODE_IN_PACKAPE

Debug_WriteLog_Def s_pfnDebugWriteLog = Debug_WriteLog_Package;

using namespace std;

////////////////////////////////////////////////////////////////////////////////
//函数名:EncryptBuffer
//功能:加密文件
//入口参数: const BYTE* szBuffer		源数据
//			UINT  nBufferSize			源数据大小
//			BYTE* pDestBuffer			目标数据，压缩后数据
//			UINT  nEncrypteBufferSize	目标数据大小	必须大于或等于ENCRYPT_BLOCK_SIZE（8byte）
//			const char* szPSW			加密密码，为8位字符
//出口参数: bool  成功返回ture 失败返回 false
////////////////////////////////////////////////////////////////////////////////
bool EncryptBuffer( const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nEncryptBufferSize )
{
	//  [4/2/2009 hemeng]
	//return CWinEncryptBuffer::GetInstance().DoConvertBuffer(szBuffer,nBufferSize,pDestBuffer,nEncryptBufferSize);
	return CSimpleEncoderBuffer::ConvertBuffer( szBuffer, nBufferSize, pDestBuffer, nEncryptBufferSize );
}

////////////////////////////////////////////////////////////////////////////////
//函数名:DecryptBuffer
//功能:解密数据
//入口参数:const BYTE* szBuffer			源数据
//			UINT  nBufferSize			源数据大小
//			BYTE* pDestBuffer			目标数据，解压后数据
//			UINT  nDecrypteBufferSize	目标数据大小
//			const char* szPSW			解密密码，为8位字符 默认密码为“password”
//出口参数: bool  成功返回ture 失败返回 false ,线程不安全，为此花费的代价太大
////////////////////////////////////////////////////////////////////////////////
bool DecryptBuffer( const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize )
{
	//  [4/2/2009 hemeng]
	//CWinDecryptBuffer inst;
	//return inst.DoConvertBuffer(szBuffer,nBufferSize,pDestBuffer,nDecryptBufferSize);
	//return CWinDecryptBuffer::GetInstance().DoConvertBuffer(szBuffer,nBufferSize,pDestBuffer,nDecryptBufferSize);
	return CSimpleEncoderBuffer::ConvertBuffer( szBuffer, nBufferSize, pDestBuffer, nDecryptBufferSize );
}

////////////////////////////////////////////////////////////////////////////////
//函数名:ZipBuffer
//功能:压缩数据
//入口参数: const BYTE * szBuffer		源数据
//			UINT nBufferSize			源数据大小
//			BYTE * pDestBuffer			目标数据
//			UINT nDestBufferSize		目标数据大小，不得小于源数据大小的1%
//出口参数: UINT 压缩后文件大小
////////////////////////////////////////////////////////////////////////////////
UINT	ZipBuffer( const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDestBufferSize )
{
	if (NULL == szBuffer)
	{
		ERROR_PRINT("压缩源BUFFER为NULL:API.CPP-ZipBuffer",NULL);
		return 0;
	}

	if (NULL == pDestBuffer)
	{
		ERROR_PRINT("压缩目标BUFFER为NULL:API.CPP-ZipBuffer",NULL);
		return 0;
	}
	
	UINT nZipSize = nBufferSize;
	int iError = compress( pDestBuffer,(uLongf*)&nZipSize,szBuffer,nBufferSize );
	if ( iError != Z_OK )
	{
		ERROR_PRINT("压缩BUFFER出错:API.CPP-ZipBuffer",NULL);
		return 0;
	}

	MESSAGE_PRINT("压缩OK！");

	return nZipSize;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:UnZipBuffer
//功能:解压数据
//入口参数: const BYTE * szBuffer		源数据
//			UINT nBufferSize			源数据大小
//			BYTE * pDestBuffer			目标数据
//			UINT nDestBufferSize		目标数据大小，不得小于源数据大小
//出口参数:UINT 解压后数据大小
UINT	UnZipBuffer( const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDestBufferSize )
{
	if (NULL == szBuffer)
	{
		ERROR_PRINT("解压缩源BUFFER为NULL:API.CPP-UnZipBuffer",NULL);
		return 0;
	}
	
	UINT nUnZipSize = nDestBufferSize;
	
	//指向真正压缩数据的位置
	if ( uncompress( pDestBuffer,(uLongf*)&nUnZipSize,szBuffer,nBufferSize) != Z_OK )
	{
		ERROR_PRINT("解压BUFFER出错:API.CPP-ZipBuffer",NULL);
		return 0;
	}
	
	MESSAGE_PRINT("解压OK！");
	return nUnZipSize;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:_GetPathSegment
//功能:字符串切割，依照给定的截断标志分解字符串
//入口参数:const char* szStr		需要分解的字符串
//			vector<string>& vStrs	分解后字符段
//			char chToken			标志字符，分解字符串的截断标志
//出口参数:无
////////////////////////////////////////////////////////////////////////////////
void _GetPathSegment( const char * szPathFile, vector< string >& vPathSegment )
{
	const char * szPathFilePos = szPathFile;
	while ( szPathFilePos )
	{
		string strPathSegment;
		szPathFilePos = _GetNextPathSegment( szPathFilePos, strPathSegment );
		if ( strPathSegment == "." )
			continue;
		vPathSegment.push_back( strPathSegment );
	}
}

////////////////////////////////////////////////////////////////////////////////
//函数名:_GetNextPathSegment
//功能:字符串切割，依照给定的截断标志分解字符串
//入口参数:const char* szStr		需要分解的字符串
//			string& strValue	分解后字符段
//出口参数:const char *
////////////////////////////////////////////////////////////////////////////////
const char * _GetNextPathSegment( const char * szStr, string& strValue )
{
	strValue = "";
	UINT nStrLen = (UINT)strlen( szStr );
	for ( UINT nIndex = 0; nIndex < nStrLen; nIndex ++ )
	{
		if ( '\\' == szStr[nIndex] || '/' == szStr[nIndex] )
		{
			if ( strValue != "." )
			{
				return szStr + nIndex + 1;
			}
			else
			{
				strValue = "";
			}
		}
		else
		{
			strValue.push_back( szStr[nIndex] );
		}
	}
	return NULL;
}
//void _SplitString( const char * szStr, vector< string >& vStrs, char chToken )
//{
//	if (NULL == szStr)
//	{
//		return;
//	}
//
//	string strCur;
//	for ( UINT nIndex = 0; nIndex < strlen( szStr ); nIndex ++ )
//	{
//		if ( chToken == szStr[nIndex] )
//		{
//			vStrs.push_back( strCur );
//			strCur = "";
//		}
//		else
//		{
//			strCur.push_back( szStr[nIndex] );
//		}
//	}
//
//	if ( strCur.length() >= 1 )
//		vStrs.push_back( strCur );
//}

////////////////////////////////////////////////////////////////////////////////
//函数名：StringLower
//功能：小写化输入字符串
//入口参数：string& str 转换的目标字符串
//出口参数：无
////////////////////////////////////////////////////////////////////////////////
void StringLower( string& str )
{
	if (str.size() == 0)
	{
		return;
	}
	for ( UINT uiIndex = 0; uiIndex < str.size(); uiIndex++ )
	{
		str[uiIndex] = tolower( str[uiIndex] );
	}	
}

////////////////////////////////////////////////////////////////////////////////
//函数名：StringUpper
//功能：大写化输入字符串
//入口参数：string& str 转换的目标字符串
//出口参数：无
////////////////////////////////////////////////////////////////////////////////
void StringUpper( string& str )
{
	if (str.size() == 0)
	{
		return;
	}

	for ( UINT uiIndex = 0; uiIndex < str.size(); uiIndex++ )
	{
		str[uiIndex] = toupper( str[uiIndex] );
	}	
}

////////////////////////////////////////////////////////////////////////////////
//函数名:GetFileType
//功能:获得文件类型，根据文件后缀获得文件类型
//入口参数:string strFilePath	文件名
//出口参数:EFileType 文件类型
////////////////////////////////////////////////////////////////////////////////
EFileType GetFileType( string strFilePath )
{
	//获得文件后缀
	string strFileSuff = strFilePath.substr( strFilePath.find_last_of(".") + 1);

	StringLower( strFileSuff );
	if ( strFileSuff == "png" )
	{
		return FT_PNG;
	}
	if ( strFileSuff == "pfx" )
	{
		return FT_PFX;
	}
	if ( strFileSuff == "ifl" )
	{
		return FT_IFL;
	}
	if ( strFileSuff == "kfm" )
	{
		return FT_KFM;
	}
	if ( strFileSuff == "dds" )
	{
		return FT_DDS;
	}
	if ( strFileSuff == "xml" )
	{
		return FT_XML;
	}
	if ( strFileSuff == "nif" )
	{
		return FT_NIF;
	}
	if ( strFileSuff == "txt" )
	{
		return FT_TXT;
	}
	if ( strFileSuff == "kf")
	{
		return FT_KF;
	}
	if ( strFileSuff == "tga")
	{
		return FT_TGA;
	}
	
	return FT_OTHER;
}

//////////////////////////////////////////////////////////////////////////////////
////函数名:GetFileEncryptType
////功能:根据文件类型，配置文件压缩及加密类型
////入口参数: EFileType nType	文件类型
////			UINT nFileSize  文件大小
////出口参数:EEncryptType 文件压缩及加密类型 默认为ET_HEADER
//////////////////////////////////////////////////////////////////////////////////
//EEncryptType	GetFileEncryptType( EFileType nType, UINT nFileSize )
//{
//	EEncryptType eEncrypType = ET_HEADER;
//	if ( nFileSize < ZIP_BLOCK_SIZE )
//	{
//		eEncrypType = ET_NONE;
//	}	
//
//	else
//	{
//		switch( nType )
//		{
//		case FT_PFX:
//			eEncrypType = ET_ZIP_HEADER;
//			break;
//		case FT_XML:
//			eEncrypType = ET_ZIP_HEADER;
//			break;
//		case FT_TXT:
//			eEncrypType = ET_ZIP_HEADER;
//			break;
//		case FT_IFL:
//			eEncrypType = ET_HEADER;
//			break;
//		case FT_PNG:
//			eEncrypType = ET_HEADER;
//			break;
//		case FT_NIF:
//			eEncrypType = ET_ZIP_HEADER;
//			break;
//		case FT_TGA:
//			eEncrypType = ET_ZIP_HEADER;
//			break;
//		default:
//			break;
//		}
//	}
//	
//	
//	return eEncrypType;
//}

////////////////////////////////////////////////////////////////////////////////
//函数名:GetBufferSize
//功能:获得指定文件实际大小
//入口参数:string strFileName	文件名
//出口参数:UINT	文件大小
////////////////////////////////////////////////////////////////////////////////
int	GetBufferSize( string strFileName )
{
	UINT nBufferSize = 0;
	FILE* pFile;

	//是否可读
	if ( fopen_s( &pFile, strFileName.c_str(), "rb" ) )
	{
		ERROR_PRINT("文件不可读或不存在:API.CPP-GetBufferSize",strFileName.c_str());
		return -1;
	}
	fseek( pFile, 0, SEEK_END );
	nBufferSize = ftell(pFile);
	fclose(pFile);

	return nBufferSize;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:GetBufferSize
//功能:获得指定文件实际大小
//入口参数:FILE* pFile 文件流指针
//出口参数:UINT	文件大小
////////////////////////////////////////////////////////////////////////////////
int	GetBufferSize( FILE* pFile )
{
	if ( !pFile )
	{
		return -1;
	}
	UINT nCurrPos = ftell(pFile);

	UINT nBufferSize = 0;

	fseek( pFile, 0, SEEK_END );
	nBufferSize = ftell(pFile);
	fseek( pFile, nCurrPos, SEEK_SET );
	
	return nBufferSize;
}


////////////////////////////////////////////////////////////////////////////////
//函数名:CompareFileInfo
//功能: 文件信息结构比较函数，比较文件描述中的nPos值
//入口参数: SPackageFileInfo sFileInfoFir	源文件信息
//			SPackageFileInfo sFileInfoSec	比对文件信息
//出口参数:bool	源小于比对返回ture，反之返回false
////////////////////////////////////////////////////////////////////////////////
bool	CompareFileInfo( SPackageFileInfo sFileInfoFir,SPackageFileInfo sFileInfoSec )
{
	return sFileInfoFir.nPos < sFileInfoSec.nPos;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:CompareIdleSpace
//功能: 碎片空间结构比较函数，比较碎片空间描述中的nPos值
//入口参数: SPackageIdleSpaceInfo sIdleFir	源文件信息
//			SPackageIdleSpaceInfo sIdleSec	比对文件信息
//出口参数:bool	源小于比对返回ture，反之返回false
////////////////////////////////////////////////////////////////////////////////
bool	CompareIdleSpace( SPackageIdleSpaceInfo sIdleFir, SPackageIdleSpaceInfo sIdleSec )
{
	return sIdleFir.nPos < sIdleSec.nPos;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:ReadFileToBuffer
//功能: 读取文件数据到buffer
//入口参数: BYTE* pBuffer			目标buffer
//			UINT nBufferSize		目标buffer大小
//			const char* pFileName	文件绝对路径+文件名
//出口参数:bool	成功返回ture，反之返回false
////////////////////////////////////////////////////////////////////////////////
bool	ReadFileToBuffer( BYTE* pBuffer,UINT nBufferSize, const char* pFileName )
{
	if (NULL == pBuffer || NULL == pFileName)
	{
		return false;
	}
	FILE* pFile;

	if ( fopen_s(&pFile, pFileName,"rb") )
	{
		ERROR_PRINT("文件打开失败！",pFileName);
		return false;
	}

	fread( pBuffer, nBufferSize, 1, pFile );
	if ( feof(pFile) )
	{
		return false;
	}
	fclose(pFile);

	return true;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:FindAllFiles
//功能: 将指定文件夹下的所有文件压入容器
//入口参数: string szPath				文件夹路径
//			vector<string>& vAllFiles	目标容器
//出口参数: bool	成功返回ture，反之返回false
////////////////////////////////////////////////////////////////////////////////
bool	FindAllFiles( string szPath, vector<string>& vAllFiles )
{
	if (szPath.size() == 0)
	{
		return false;
	}
	string szFind;
	szFind = szPath;

	if ( !IsRoot(szFind) )
	{
		szFind +="\\";
	}

	szFind += "*.*";

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = FindFirstFile( szFind.c_str(), &FindFileData );

	if ( hFind == INVALID_HANDLE_VALUE )
	{
		return false;
	}

	do 
	{
		//过滤本机目录和父级目录
		if ( FindFileData.cFileName[0] == '.' )
		{
			continue;
		}

		//找到的是目录，则进入目录进行递归
		if ( FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			string szFile;
			if ( IsRoot(szPath) )
			{
				szFile = szPath + FindFileData.cFileName;
			}
			else
			{
				szFile = szPath + "\\" + FindFileData.cFileName;
			}

			if (!FindAllFiles(szFile,vAllFiles))
			{
				return false;
			}
		}

		//找到的是文件
		else
		{
			string szFile;
			if ( IsRoot(szPath) )
			{
				szFile = szPath + FindFileData.cFileName;
			}
			else
			{
				szFile = szPath + "\\" + FindFileData.cFileName;
			}
			vAllFiles.push_back( szFile );

		}

	} while(FindNextFile(hFind, &FindFileData));

	return FindClose(hFind) == TRUE;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:IsRoot
//功能: 判断当前路径是否为根目录	
//入口参数: string szPath	目录路径
//出口参数: bool	成功返回ture，反之返回false
//说明:
////////////////////////////////////////////////////////////////////////////////
bool	IsRoot( string szPath )
{
	if (szPath.size() == 0)
	{
		return false;
	}
	string Root;
	Root = szPath.at(0);
	Root += ":\\";

	if ( Root== szPath )
	{
		return true;
	}

	else
	{
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////
//函数名:_GetRelativePath
//功能: 获取相对路径
//入口参数: const char * szBasePath	基路径
//			const char * szFullPath 目标路径
//出口参数: string	相对路径
//说明:
////////////////////////////////////////////////////////////////////////////////
string _GetRelativePath( const char * szBasePath, const char * szFullPath )
{
	if ( szFullPath == NULL )
		return "";
	if ( szBasePath == NULL )
		return szFullPath;

	const char * szRelativePath = szFullPath;

	UINT nIndex = 0;
	UINT nBasePathLen = (UINT)strlen( szBasePath );
	UINT nFullPathLen = (UINT)strlen( szFullPath );
	if ( nBasePathLen > nFullPathLen )
		return "";

	string strBaseSegment;
	string strFullSegment;
	const char * szBasePathCurPos = szBasePath;
	const char * szFullPathCurPos = szFullPath;

	while ( true )
	{
		szBasePathCurPos = _GetNextPathSegment( szBasePathCurPos, strBaseSegment );
		szFullPathCurPos = _GetNextPathSegment( szFullPathCurPos, strFullSegment );
		if ( _stricmp( strBaseSegment.c_str(), strFullSegment.c_str() ) == 0 )
		{
			if ( szBasePathCurPos && szFullPathCurPos )
			{
				continue;
			}
			else
			{
				if ( !szFullPathCurPos )
					return "";
				if ( !szBasePathCurPos )
					return szFullPathCurPos;
			}
		}
		else
		{
			if ( strBaseSegment.size() > 0 )
				return szFullPath;

			if ( !szFullPathCurPos )
				return strFullSegment;
			else
				return strFullSegment + "\\" + szFullPathCurPos;
		}
	}
	return szFullPathCurPos;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:CompareFileTime
//功能: 获取相对路径
//入口参数: FILETIME fileTime1 被比较的时间
//			FILETIME fileTime2 比较的时间
//出口参数: int 大于返回1，小于返回-1，等于返回0
//说明:
////////////////////////////////////////////////////////////////////////////////
int CompareFileTime(FILETIME fileTime1,FILETIME fileTime2)
{
	if (fileTime1.dwHighDateTime > fileTime2.dwHighDateTime)
	{
		return 1;
	}
	else if ( fileTime1.dwHighDateTime == fileTime2.dwHighDateTime)
	{
		if (fileTime1.dwLowDateTime > fileTime2.dwLowDateTime)
		{
			return 1;
		}
		else if (fileTime1.dwLowDateTime == fileTime2.dwLowDateTime)
		{
			return 0;
		}
		else
		{
			return -1;
		}
	}
	else
	{
		return -1;
	}
}
bool _ValidateDirs(string strAbsFileName)
{
	if (strAbsFileName == "")
	{
		return false;
	}

	vector<string> vDirs;

	_GetPathSegment(strAbsFileName.c_str(),vDirs);
	string strTmpDir = "";

	bool bNetWork = false;
	// 如果是网络路径，首先找到主机名 [8/26/2009 hemeng]
	if (vDirs[0] == "" && vDirs[1] == vDirs[0] && vDirs.size() >= 3)
	{
		bNetWork = true;
		strTmpDir += "\\";
		strTmpDir += "\\";
		strTmpDir += vDirs[2];
		strTmpDir += "\\";
	}

	// 遍历路径中所有的目录 [8/10/2009 hemeng]
	unsigned int uiIndex = 0; 
	if (bNetWork)
	{
		uiIndex = 3;
	}
	for (; uiIndex < vDirs.size() - 1; uiIndex ++)
	{
		strTmpDir += vDirs[uiIndex];	

		// 路径不存在 [8/10/2009 hemeng]
		if (_access_s(strTmpDir.c_str(), 0) != 0 )
		{
			if (_mkdir(strTmpDir.c_str()) != 0)
			{
				return false;
			}			
		}
		strTmpDir += "\\";
	}

	return true;
}

