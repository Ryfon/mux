﻿/** @file SmartPointer.h 
@brief 文件打包：包文件类 第一版测试
<pre>
*	Copyright (c) 2007，北京金刚游科技有限公司
*	All rights reserved.
*
*	当前版本：2009-3-
*	作    者：KingKong
*	完成日期：2009-3-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#pragma once
#include "Api.h"
#include <Windows.h>

class PACKAGE_ENTRY CRefObject
{
public:
    CRefObject();
    virtual ~CRefObject();
    void IncRefCount();
    void DecRefCount();
    unsigned int GetRefCount() const;
protected:
    virtual void DeleteThis();
private:
    unsigned int m_uiRefCount;
};

inline CRefObject::CRefObject()
{
    m_uiRefCount = 0;
}

inline CRefObject::~CRefObject()
{
}

inline void CRefObject::IncRefCount()
{
	InterlockedIncrement((long*)&m_uiRefCount);
}

inline void CRefObject::DecRefCount()
{
    if (InterlockedDecrement((long*)&m_uiRefCount) == 0)
        DeleteThis();
}

inline unsigned int CRefObject::GetRefCount() const
{
    return m_uiRefCount;
}


//------------------------------------------------------------------

template <class T> class PACKAGE_ENTRY CPointer
{
public:
    // construction and destruction
    CPointer(T* pObject = (T*) 0);
    CPointer(const CPointer& ptr);
    ~CPointer();

    // implicit conversions
    operator T*() const;
    T& operator*() const;
    T* operator->() const;

    // assignment
    CPointer& operator=(const CPointer& ptr);
    CPointer& operator=(T* pObject);

    // comparisons
    bool operator==(T* pObject) const;
    bool operator!=(T* pObject) const;
    bool operator==(const CPointer& ptr) const;
    bool operator!=(const CPointer& ptr) const;

protected:
    // the managed pointer
    T* m_pObject;
};

#define SmartPointerDef(classname) \
    class classname; \
    typedef CPointer<classname> classname##Ptr

#define CSmartPointerCast(type, smartptr) ((type*) (void*) (smartptr))

//---------------------------------------------------------------------------
template <class T>
inline CPointer<T>::CPointer(T* pObject)
{
    m_pObject = pObject;
    if (m_pObject)
        m_pObject->IncRefCount();
}
//---------------------------------------------------------------------------
template <class T>
inline CPointer<T>::CPointer(const CPointer& ptr)
{
    m_pObject = ptr.m_pObject;
    if (m_pObject)
        m_pObject->IncRefCount();
}
//---------------------------------------------------------------------------
template <class T>
inline CPointer<T>::~CPointer()
{
    if (m_pObject)
        m_pObject->DecRefCount();
}
//---------------------------------------------------------------------------
template <class T>
inline CPointer<T>::operator T*() const
{
    return m_pObject;
}
//---------------------------------------------------------------------------
template <class T>
inline T& CPointer<T>::operator*() const
{
    return *m_pObject;
}
//---------------------------------------------------------------------------
template <class T>
inline T* CPointer<T>::operator->() const
{
    return m_pObject;
}
//---------------------------------------------------------------------------
template <class T>
inline CPointer<T>& CPointer<T>::operator=(const CPointer& ptr)
{
    if (m_pObject != ptr.m_pObject)
    {
        if (m_pObject)
            m_pObject->DecRefCount();
        m_pObject = ptr.m_pObject;
        if (m_pObject)
            m_pObject->IncRefCount();
    }
    return *this;
}
//---------------------------------------------------------------------------
template <class T>
inline CPointer<T>& CPointer<T>::operator=(T* pObject)
{
    if (m_pObject != pObject)
    {
        if (m_pObject)
            m_pObject->DecRefCount();
        m_pObject = pObject;
        if (m_pObject)
            m_pObject->IncRefCount();
    }
    return *this;
}
//---------------------------------------------------------------------------
template <class T>
inline bool CPointer<T>::operator==(T* pObject) const
{
    return (m_pObject == pObject);
}
//---------------------------------------------------------------------------
template <class T>
inline bool CPointer<T>::operator!=(T* pObject) const
{
    return (m_pObject != pObject);
}
//---------------------------------------------------------------------------
template <class T>
inline bool CPointer<T>::operator==(const CPointer& ptr) const
{
    return (m_pObject == ptr.m_pObject);
}
//---------------------------------------------------------------------------
template <class T>
inline bool CPointer<T>::operator!=(const CPointer& ptr) const
{
    return (m_pObject != ptr.m_pObject);
}
//---------------------------------------------------------------------------

