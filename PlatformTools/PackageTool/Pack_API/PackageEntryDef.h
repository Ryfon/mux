﻿#pragma once


#ifdef PACKAGE_EXPORT
// DLL library project uses this
#define PACKAGE_ENTRY __declspec(dllexport)
#else
#ifdef PACKAGE_IMPORT
// client of DLL uses this
#define PACKAGE_ENTRY __declspec(dllimport)
#else
// static library project uses this
#define PACKAGE_ENTRY
#endif
#endif


