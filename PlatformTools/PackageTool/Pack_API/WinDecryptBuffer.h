﻿/** @file WinDecryptBuffer.h
@brief 
<pre>
*	Copyright (c) 2009，北京金刚游科技有限公司
*	All rights reserved.
*
*	当前版本：
*	作    者：KingKong
*	完成日期：2009-04-02
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
#include "convertbuffer.h"
#include <Wincrypt.h>

//加密API宏
#define MY_ENCODING_TYPE (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define KEYLENGTH	0x00800000
#define ENCRYPT_ALGORITHM	CALG_RC4

//----------------------------------------------------------------
//加密大小必须是8字节的整倍数
#define ENCRYPT_BLOCK_SIZE	8
#define ENCRYPT_PACK_LENGTH 8 * ENCRYPT_BLOCK_SIZE

class CWinDecryptBuffer :
	public CConvertBuffer
{
public:
	CWinDecryptBuffer(void);
	~CWinDecryptBuffer(void);

	virtual bool Create();
	virtual void Destroy();
	static CWinDecryptBuffer& GetInstance();
	virtual bool DoConvertBuffer(const BYTE * szBuffer, UINT nBufferSize, BYTE * pDestBuffer, UINT nDecryptBufferSize);

private:
	HCRYPTPROV	m_hCryptProv;		//CSP句柄
	HCRYPTKEY	m_hKey;				//key句柄
	HCRYPTHASH	m_hHash;			//hash对象句柄
};
