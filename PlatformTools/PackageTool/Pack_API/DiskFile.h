﻿/** @file DiskFile.h 
@brief 文件打包：包文件类 第一版测试
<pre>
*	Copyright (c) 2007，北京金刚游科技有限公司
*	All rights reserved.
*
*	当前版本：2009-3-25
*	作    者：KingKong
*	完成日期：2009-3-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#include <io.h>
#include <stdio.h>

#ifndef BYTE
#define BYTE unsigned char
#endif
/// disk file only read
class CDiskFileReader
{
public:
	CDiskFileReader();
	~CDiskFileReader();
	void SetFileInfo( FILE* hFile, unsigned int nFileSize, unsigned int nFileBeginPos );
	void Seek( int iOffset, int iWhence);
	void Seek( int iOffset );
	unsigned int Read( void* pvBuffer, unsigned int uiBytes );
private:
	unsigned int m_uiBufferAllocSize;
	unsigned int m_uiFilePos;
	unsigned int m_uiBufferBeginPos;
	unsigned int m_uiBufferReadSize;
	FILE * m_pFile;
	unsigned int m_nFileSize; ///文件的大小
	unsigned int m_nFileBeginPos; ///文件相对包的开始位置
	BYTE * m_pBuffer;
	bool m_bGood;

	void Flush();
	unsigned int DiskRead(void* pBuffer, unsigned int uiBytes);
};