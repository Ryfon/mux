﻿/** @file PackageFileSystem.h 
@brief 文件打包：包文件系统 第一版测试
<pre>
*	Copyright (c) 2007，北京金刚游科技有限公司
*	All rights reserved.
*
*	当前版本：2008-08-20 
*	作    者：KingKong
*	完成日期：2008-08-
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#if !defined _PACKAGFILESYSTEM_H_
#define _PACKAGFILESYSTEM_H_
#include "Api.h"

#pragma warning(push)
#pragma warning( disable: 4251 )


class PACKAGE_ENTRY CPackageFileSystem: public CRefObject
{
	public:
		CPackageFileSystem(bool bModified = false);
		~CPackageFileSystem();

		////////////////////////////////////////////////////////////////////////////////
		//函数名:CPackageFileSystem 拷贝构造函数
		//功能:拷贝构造一个文件系统信息
		//入口参数: CPackageFileSystem* kSrcSystem 源文件信息系统
		//出口参数: 无
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		CPackageFileSystem( CPackageFileSystem* kSrcSystem );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:CloneFrom函数
		//功能:拷贝构造一个文件系统信息
		//入口参数: CPackageFileSystem* kSrcSystem 源文件信息系统
		//出口参数: 无
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		void CloneFrom( CPackageFileSystem* kSrcSystem );


		////////////////////////////////////////////////////////////////////////////////
		//函数名:LoadPackFileSystem
		//功能:从包文件载入包系统信息
		//入口参数: FILE* pPackFile		包文件
		//			UINT nSystemInfoPos 文件系统信息存储位置
		//			bool bModified		当前系统模式
		//出口参数: bool	成功返回true, 失败返回 false
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool	LoadPackFileSystem( FILE* pPackFile, UINT nSystemInfoPos, bool bModified );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:SavePackFileSystemInfo
		//功能:向包文件写入包系统信息
		//入口参数: FILE* pPackFile		包文件
		//			UINT  nPos 文件系统信息存储位置
		//出口参数: UINT  文件系统信息在包文件中的最后写入位置
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		UINT	SavePackFileSystemInfo( FILE* pPackFile,UINT nPos,FILETIME nContentVersion );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:GetPackFileInfo
		//功能:获得指定文件的文件描述信息
		//入口参数: string szPathName 文件全路径（相对路径+文件名）
		//出口参数: SPackageFileInfo* 文件信息描述指针，如果未找到则返回NULL
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		SPackageFileInfo*	GetPackFileInfo( string szPathName );


		////////////////////////////////////////////////////////////////////////////////
		//函数名:GetPackDirInfo
		//功能:获得指定文件的文件描述信息
		//入口参数: string szPathName 文件全路径（相对路径+文件名）
		//出口参数: SPackageFileInfo* 文件信息描述指针，如果未找到则返回NULL
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		SPackageDirInfo*	GetPackDirInfo( string szPathName );


		////////////////////////////////////////////////////////////////////////////////
		//函数名:SetPackageRootDir
		//功能:设置包的根目录 
		//入口参数: string strRootDir 根目录
		//出口参数: bool
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool	SetPackageRootDir( string strRootDir) ;

		////////////////////////////////////////////////////////////////////////////////
		//函数名:GetPackageRootDir
		//功能:返回包根目录路径
		//入口参数: 无
		//出口参数: string 根目录路径
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		string	GetPackageRootDir() const
		{
			return m_rootDir.szDirName;
		};

		////////////////////////////////////////////////////////////////////////////////
		//函数名:GetPackageDataEndPos
		//功能: 获得包数据区最后写入位置
		//入口参数: 无
		//出口参数: UINT 位置
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		UINT	GetPackageDataEndPos() const
		{
			return m_sSystemInfo.nFileInfoPos;
		};

		////////////////////////////////////////////////////////////////////////////////
		//函数名:AddFileInfo
		//功能:添加指定文件描述到包文件描述系统
		//入口参数: string szPathName					文件全路径
		//			const SPackageFileInfo& fileInfo	源文件描述信息
		//出口参数: bool	成功返回true, 失败返回 false 
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool	AddFileInfo( string szPathName, const SPackageFileInfo& fileInfo );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:DeleteFileInfo
		//功能:删除指定文件描述
		//入口参数: string szPathName 文件全路径
		//出口参数: bool
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool	DeleteFileInfo( string szPathName );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:GetIdleSpace
		//功能:获得大于等于指定大小的碎片空间
		//入口参数: UINT nSize							空间大小
		//			SPackageIdleSpaceInfo& sIdleInfo	碎片空间信息的引用
		//			bool	bUse						是否使用改碎片空间，ture 则在返回碎片空间的同时删除该空间，反之仅返回该空间
		//出口参数: bool	找到返回 ture，反之返回 false
		//说明:找到则由sIfleInfo返回找到的碎片空间信息
		////////////////////////////////////////////////////////////////////////////////
		bool	GetIdleSpace( UINT nSize,SPackageIdleSpaceInfo& sIdleInfo, bool bUse );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:Deflate
		//功能:紧凑模式
		//入口参数: FILE* pPackFile		源包文件流指针
		//出口参数: 
		//说明:bool	成功返回true, 失败返回 false 
		////////////////////////////////////////////////////////////////////////////////
		bool	Deflate( FILE* pPackFile );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:FindAllFileInfos
		//功能:由当前文件系统和比对文件系统对比，查找是否相同
		//入口参数: vector<Sstring>& vFilesName	当前文件系统所有文件
		//出口参数: bool	成功返回true, 失败返回 false 
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool				FindAllFilesName(vector<string>& vFilesName);

		void				GetAllFilesInfo(vector<SPackageFileInfo>& vFileInfos);
		////////////////////////////////////////////////////////////////////////////////
		//函数名:GetRelPath
		//功能:获得相对路径
		//入口参数: string strAbsPath	源路径
		//出口参数: string	相对路径
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		string				GetRelPath( string strAbsPath );
		
	protected:
		
	private:
		CPackageFileSystem(CPackageFileSystem&);

		////////////////////////////////////////////////////////////////////////////////
		//函数名:WriteSystemInfoToPackage
		//功能:将包系统信息写入指定文件
		//入口参数: FILE* pFile					 目标文件流指针
		//			SFileSystemInfo& sSystemInfo 源包系统信息
		//			UINT nPos					 写入位置
		//出口参数:	UINT 写入文件的最后位置
		////////////////////////////////////////////////////////////////////////////////
		UINT _WriteSystemInfoToPackage( FILE* pFile, SFileSystemInfo& sSystemInfo, UINT nPos );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:ReadSystemInfoFromPackage
		//功能:从指定文件读取包系统信息
		//入口参数:	FILE* pFile					 源文件流指针
		//			SFileSystemInfo& sSystemInfo 目标包系统信息
		//			UINT nPos					 读入位置 返回读入最后位置
		//出口参数: bool 成功返回ture 失败返回 false
		////////////////////////////////////////////////////////////////////////////////
		bool _ReadSystemInfoFromPackage( FILE* pFile, SFileSystemInfo& sSystemInfo, UINT& nPos  );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:WriteFileInfoToPackage
		//功能:将文件信息写入指定文件
		//入口参数: FILE* pFile					目标文件流指针
		//			SPackageFileInfo& sFileInfo 源包文件信息
		//			UINT nPos					 写入位置
		//出口参数:	UINT 写入文件的最后位置
		////////////////////////////////////////////////////////////////////////////////
		UINT _WriteFileInfoToPackage( FILE* pFile, SPackageFileInfo& sFileInfo, UINT nPos  );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:ReadFileInfoFromPackage
		//功能:从指定文件读取文件信息
		//入口参数:	FILE* pFile					源文件流指针
		//			SPackageFileInfo& sFileInfo 目标文件信息
		//			UINT nPos					 读入位置 返回读入最后位置
		//出口参数: bool 成功返回ture 失败返回 false
		////////////////////////////////////////////////////////////////////////////////
		bool _ReadFileInfoFromPackage( FILE* pFile, SPackageFileInfo& sFileInfo, UINT& nPos  );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:_CreateDirInfo
		//功能:创建目录信息
		//入口参数: string strDirName	源目录路径
		//出口参数: SPackageDirInfo*	目录信息指针
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		SPackageDirInfo*	_CreateDirInfo( string strDirName );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:_SaveSystemInfoToPackage
		//功能:写入包系统信息到包文件
		//入口参数: FILE* pPackFile	包文件流指针
		//			UINT nPos		写入位置
		//出口参数: 最终写入位置
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		UINT	_SaveSystemInfoToPackage( FILE* pPackFile,UINT nPos );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:_SaveFileInfoToPackage
		//功能:写入文件描述信息到包文件
		//入口参数: FILE* pPackFile	包文件流指针
		//			UINT nPos		写入位置
		//出口参数: 最终写入位置
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		UINT	_SaveFileInfoToPackage( FILE* pPackFile,UINT nPos );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:_SaveFileIdleSpaceInfoToPackage
		//功能:写入碎片空间信息到包文件
		//入口参数: FILE* pPackFile	包文件流指针
		//			UINT nPos		写入位置
		//出口参数: 最终写入位置
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		UINT	_SaveFileIdleSpaceInfoToPackage( FILE* pPackFile,UINT nPos );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:_InsertAllFileInfoToVector
		//功能: 将包系统中的所有文件信息插入一个容器中
		//入口参数: vector<SPackageFileInfo>& vPackageFileInfos	目标容器
		//出口参数: 无
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		void	_InsertAllFileInfoToVector( vector<SPackageFileInfo>& vPackageFileInfos );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:_InsertDirInfoToVector
		//功能:将指定目录信息中的所有文件信息插入目标容器
		//入口参数: vector<SPackageFileInfo>& vPackageFileInfos 目标容器
		//			SPackageDirInfo& sDirInfo					源目录信息
		//出口参数: 无
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		void	_InsertDirInfoToVector( vector<SPackageFileInfo>& vPackageFileInfos, SPackageDirInfo& sDirInfo );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:_LoadSystemInfo
		//功能:载入包系统信息
		//入口参数: FILE* pPackFile	包文件流指针
		//			UINT nPos		系统信息在包中的存储位置
		//出口参数: 
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool	_LoadSystemInfo( FILE* pPackFile,UINT nPos );
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:_LoadFileInfos
		//功能:载入文件信息描述
		//入口参数: FILE* pPackFile	包文件流指针
		//			UINT nPos		信息在包中的存储位置
		//出口参数: bool			成功返回ture，失败返回false
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool	_LoadFileInfos( FILE* pPackFile,UINT nFileInfoPos);
		
		////////////////////////////////////////////////////////////////////////////////
		//函数名:_LoadIdleInfo
		//功能:载入碎片空间
		//入口参数: FILE* pPackFile	包文件流指针
		//			UINT nPos		信息在包中的存储位置
		//出口参数: bool			成功返回ture，失败返回false
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool	_LoadIdleInfo( FILE* pPackFile,UINT nPos );


		////////////////////////////////////////////////////////////////////////////////
		//函数名:_MoveFileData
		//功能: 移动包数据，用于紧缩模式
		//入口参数: FILE* pPackFile				包文件流
		//			SPackageFileInfo sFileInfo	当前移动数据的文件描述信息
		//			UINT nMoveOffset			向上移动的偏移量
		//出口参数: bool			成功返回ture，失败返回false 
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		bool	_MoveFileData( FILE* pPackFile, SPackageFileInfo sFileInfo, UINT nMoveOffset );

		////////////////////////////////////////////////////////////////////////////////
		//函数名:_GetPackDirInfo
		//功能:获得指定路径的目录描述信息
		//入口参数: string szPathName 相对于root的路径
		//出口参数: SPackageDirInfo* 目录信息指针，如果未找到则返回NULL
		//说明:
		////////////////////////////////////////////////////////////////////////////////
		SPackageDirInfo*	_GetPackDirInfo( string szPathName );

		size_t	_Encoderfwrite( const void *buffer, size_t size,	size_t count,FILE *stream );
		size_t	_Decoderfread( void *buffer, size_t size,	size_t count,FILE *stream );
		

		//----------------------------------------------------------------------------------------------------------------
		// 成员变量
		SPackageDirInfo		m_rootDir;		// 文件系统根
		bool				m_bModified;	// 当前系统是否可改写

		// 用于记录有空闲空间的文件结构位置
		vector<SPackageIdleSpaceInfo>		m_vIdleSpace;
		SFileSystemInfo						m_sSystemInfo;		// 包系统信息统计
		
};

//导出函数
PACKAGE_ENTRY string				GetPackageRootDir();

SmartPointerDef( CPackageFileSystem );

#pragma warning(pop)


#endif
//end of _PACKAGFILESYSTEM_H_