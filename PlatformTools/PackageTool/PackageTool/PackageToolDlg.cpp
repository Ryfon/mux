﻿// PackageToolDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "PackageTool.h"
#include "PackageToolDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CPackageToolDlg 对话框




CPackageToolDlg::CPackageToolDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPackageToolDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPackageToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_OPINFO, m_editPackingInfo);
	DDX_Control(pDX, IDC_EDIT_PROCESSINFO, m_editProcessInfo);
	DDX_Control(pDX, IDC_EDIT_OPEN_PACK_PATH, m_editDirPath);
	DDX_Control(pDX, IDC_EDIT_PACK_SAVE_PATH, m_editPackFilePath);
}

BEGIN_MESSAGE_MAP(CPackageToolDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_OPEN_FILE, &CPackageToolDlg::OnBnClickedOpenFile)
	ON_BN_CLICKED(IDC_BUTTON_PACK_PATH, &CPackageToolDlg::OnBnClickedButtonPackPath)
	ON_BN_CLICKED(IDOK, &CPackageToolDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_OPEN_PACKAGE, &CPackageToolDlg::OnBnClickedOpenPackage)
	ON_BN_CLICKED(IDC_OPEN_LATEST, &CPackageToolDlg::OnBnClickedOpenLatest)
	ON_BN_CLICKED(IDC_BUTTON_TEST, &CPackageToolDlg::OnBnClickedButtonTest)
	ON_BN_CLICKED(IDC_OPEN_OLD, &CPackageToolDlg::OnBnClickedOpenOld)
	ON_BN_CLICKED(IDC_BTN_EXPORT_PACK_INFO, &CPackageToolDlg::OnBnClickedBtnExportPackInfo)
	ON_BN_CLICKED(IDC_BTN_IMPORT_PACK_INFO, &CPackageToolDlg::OnBnClickedBtnImportPackInfo)
	ON_BN_CLICKED(IDC_DEFLATE, &CPackageToolDlg::OnBnClickedDeflate)
END_MESSAGE_MAP()


// CPackageToolDlg 消息处理程序

BOOL CPackageToolDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CPackageToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CPackageToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CPackageToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CPackageToolDlg::OnBnClickedOpenFile()
{
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));
	string strDirPath = GetDir();

	if (strDirPath == "")
	{
		ShowMessage("请选择打包目录！");
		return;
	}
	
	m_editDirPath.SetWindowText(strDirPath.c_str());
	/*GetDlgItem( IDC_EDIT_OPEN_PACK_PATH )->SetWindowText( m_szDirPath.c_str() );*/
}

void CPackageToolDlg::OnBnClickedButtonPackPath()
{
	// TODO: 在此添加控件通知处理程序代码
	string strPackPath = GetDir();

	if (strPackPath == "")
	{
		ShowMessage("请选择包存放位置！");
		return;
	}
	m_editPackFilePath.SetWindowText(strPackPath.c_str());
	/*GetDlgItem( IDC_EDIT_PACK_SAVE_PATH )->SetWindowText( m_szPackPath .c_str());*/
}

void CPackageToolDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	//OnOK();
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));
	char szDirPath[MAX_PATH];
	char szPackFilePath[MAX_PATH];

	m_editDirPath.GetWindowText(szDirPath,MAX_PATH);
	m_editPackFilePath.GetWindowText(szPackFilePath,MAX_PATH);

	if ( strlen(szDirPath) == 0 || strlen(szPackFilePath) == 0 )
	{
		ShowMessage( "目标文件夹或保存路径未填写！" );
		return;
	}
	
	string strDirPath = string(szDirPath);
	string strPathFilePath = string(szPackFilePath);
	
	if (strDirPath[strDirPath.size() - 1] == '\\')
	{
		strDirPath.erase(strDirPath.find_last_of("\\"),2);
		/*strDirPath.substr(0,(strDirPath.find_last_of("\\") - 1));*/
	}

	string strPackName = strDirPath.substr(strDirPath.find_last_of("\\") + 1) + ".pkg";;
	

	if (strPathFilePath[strPathFilePath.size() - 1] == '\\')
	{
		strPathFilePath += strPackName;
	}
	else
	{
		strPathFilePath += "\\";
		strPathFilePath += strPackName;
	}
	
	//创建包
	CPackage* pPackage = new CPackage();
	if ( !pPackage->OpenPackage( strPathFilePath, CPackage::EO_WRITE, true ) )
	{
		ShowMessage( "创建包失败！" );
		return;
	}
	CPackageFileSystem* pPackFileSystem = pPackage->GetPackageFileSystem();
	if ( pPackFileSystem == NULL )
	{
		ShowMessage( "获取包系统信息出错！" );
		return;
	}

	vector<string> vFiles;

	if(!FindAllFiles( strDirPath, vFiles ))
	{
		ShowMessage( "查找文件目录失败！" );

		delete pPackFileSystem;
		pPackFileSystem = NULL;
		
		delete pPackage;
		pPackage = NULL;
		return;
	}

	m_editProcessInfo.SetSel(0,-1);
	m_editProcessInfo.Clear();
	m_editProcessInfo.ReplaceSel("正在添加文件：");

	UINT nFileNum = (UINT)vFiles.size();

	for ( UINT uiIndex = 0; uiIndex < nFileNum; uiIndex++ )
	{
		string strPackRootDir = pPackFileSystem->GetPackageRootDir();
		string strPath = vFiles[uiIndex];
		StringLower(strPath);
		size_t it = strPath.find( strPackRootDir );
		if ( it != string::npos )
		{
			if ( !pPackage->AddFile(strPath,strDirPath) )
			{
				ShowMessage( "添加文件出错！" );
				continue;
			}
		}
		else
		{
			ShowMessage("文件不属于当前包根目录！不能添加！");
		}
		m_editPackingInfo.SetSel( 0, -1 );
		m_editPackingInfo.Clear();
		m_editPackingInfo.ReplaceSel( strPath.c_str() );
	}

	/*pPackage->SetPackVersion(2);*/

	pPackage->ClosePackage();

	delete pPackage;
	pPackage = NULL;

	if (pPackFileSystem)
	{
		pPackFileSystem = NULL;
	}

	ShowMessage( "文件添加成功!" );
}

void CPackageToolDlg::ShowMessage(const char* szMessage)
{
	CMessageDlg msgDlg;

	msgDlg.SetDlgInfo(szMessage);
	msgDlg.DoModal();
}

void CPackageToolDlg::OnBnClickedOpenPackage()
{
	// TODO: 在此添加控件通知处理程序代码
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));
	CFileDialog *openFileDlg = new CFileDialog(true,NULL,NULL,OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openFileDlg->m_ofn.lpstrFilter = "资源包文件(*.pkg)|*.pkg|All files(*.*)|*.*";
	char pFilePath[MAX_PATH];

	if ( openFileDlg->DoModal() == IDOK )
	{
		strncpy_s( pFilePath,MAX_PATH, (LPCTSTR)openFileDlg->GetPathName(),sizeof(pFilePath) );		
	}
	else
	{
		ShowMessage("物件信息读入失败");
		return;
	}

	if (openFileDlg)
	{
		delete openFileDlg;
		openFileDlg = NULL;
	}

	//查找文件夹
	string strReleasePath = GetDir();
	if (strReleasePath == "")
	{
		ShowMessage("释放目录不能为空！");
		return;
	}
	

	CPackage* pPackage = new CPackage;
	if ( NULL == pPackage)
	{
		ShowMessage("分配内存失败！");
		return;
	}

	if ( !pPackage->OpenPackage(pFilePath) )
	{
		ShowMessage("打开失败！");
		delete pPackage;
		pPackage = NULL;
		return;
	}

	m_editProcessInfo.SetSel(0,-1);
	m_editProcessInfo.Clear();
	m_editProcessInfo.ReplaceSel("正在释放文件：");

	vector<string> vFiles;
	pPackage->GetPackageFileSystem()->FindAllFilesName(vFiles);

	for ( UINT uiIndex = 0; uiIndex < vFiles.size(); uiIndex++ )
	{
		string strAbsFilePath = strReleasePath + "\\" + vFiles[uiIndex];
		if ( !pPackage->CreateNewFileFromPackage(vFiles[uiIndex],strAbsFilePath))
		{
			ShowMessage("创建新文件失败！");
			return;
		}
		m_editPackingInfo.SetSel( 0, -1 );
		m_editPackingInfo.Clear();
		m_editPackingInfo.ReplaceSel( vFiles[uiIndex].c_str() );
	}

	pPackage->ClosePackage();

	if (pPackage)
	{
		delete pPackage;
		pPackage = NULL;
	}	

	ShowMessage("释放完成！");
	
}

string CPackageToolDlg::GetDir()
{
	//查找文件夹
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));
	string strPath;
	BROWSEINFO	bi;

	LPITEMIDLIST pidl;
	LPMALLOC	pMalloc;

	TCHAR szDirPath[MAX_PATH];

	if ( SUCCEEDED( SHGetMalloc(&pMalloc) ) )
	{
		ZeroMemory( &bi,sizeof(bi) );

		bi.hwndOwner = this->m_hWnd;
		bi.pszDisplayName = 0;
		bi.pidlRoot = 0;
		bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_VALIDATE;

		pidl = SHBrowseForFolder( &bi );

		if ( pidl )
		{
			if ( !SHGetPathFromIDList( pidl, szDirPath ) )
			{
				ShowMessage("读取文件夹路径出错！");
				return "";
			}
			else
			{
				strPath = string( szDirPath );
			}
		}

		pMalloc->Free( pidl );
		pMalloc->Release();
	}

	return strPath;
}

void CPackageToolDlg::OnBnClickedOpenLatest()
{
	// TODO: 在此添加控件通知处理程序代码
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));
	CFileDialog *openFileDlg = new CFileDialog(true,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openFileDlg->m_ofn.lpstrFilter = "资源包文件(*.pkg)|*.pkg|All files(*.*)|*.*";

	if ( openFileDlg->DoModal() == IDOK )
	{
		strncpy_s( m_pLatestPack, MAX_PATH,(LPCTSTR)openFileDlg->GetPathName(),sizeof(m_pLatestPack) );		
	}
	else
	{
		ShowMessage("物件信息读入失败");
		return;
	}

	GetDlgItem( IDC_EDIT_OPEN_LATEST )->SetWindowText( m_pLatestPack );
}

void CPackageToolDlg::OnBnClickedButtonTest()
{
	// TODO: 在此添加控件通知处理程序代码
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));

	ShowMessage("请选择包文件");
	CFileDialog *openFileDlg = new CFileDialog(true,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openFileDlg->m_ofn.lpstrFilter = "资源包文件(*.pkg)|*.pkg|All files(*.*)|*.*";
	char pFilePath[MAX_PATH];

	if ( openFileDlg->DoModal() == IDOK )
	{
		strncpy_s( pFilePath, MAX_PATH,(LPCTSTR)openFileDlg->GetPathName(),sizeof(pFilePath) );		
	}
	else
	{
		ShowMessage("物件信息读入失败");
		return;
	}

	delete openFileDlg;
	openFileDlg = NULL;

	CPackage* pPackage = new CPackage;
	if ( NULL == pPackage)
	{
		ShowMessage("分配内存失败！");
		return;
	}

	if ( !pPackage->OpenPackage(pFilePath,pPackage->EO_READ) )
	{
		ShowMessage("打开失败！");
		pPackage = NULL;
		return;
	}

	/*pPackage->SetPackVersion(2);*/
	//查找文件夹
	ShowMessage("请选择信息文件存放路径");
	string strReleasePath = GetDir();
	if (strReleasePath == "")
	{
		ShowMessage("释放目录不能为空！");
		return;
	}
	if (strReleasePath[strReleasePath.size() -1 ] != '\\')
	{
		strReleasePath += "\\";
	}
	strReleasePath += pPackage->GetPackageRootDir();
	strReleasePath += "_PackInfo.txt";

	ofstream outFile;
	outFile.open(strReleasePath.c_str());

	outFile << "Pack Code Version" << endl;
	outFile << pPackage->GetPackCodeVersion() << endl;

	SYSTEMTIME nTime;
	FileTimeToSystemTime(&pPackage->GetPackContentVersion(),&nTime);
	outFile << "Pack Content Version:" << endl;
	outFile << nTime.wYear << "/" << nTime.wMonth << "/" << nTime.wDay << "/" << nTime.wHour << ":" << nTime.wMinute << endl;

	outFile << "Pack DirInfo:" << endl;
	outFile << pPackage->GetPackageFileSystem()->GetPackageRootDir().c_str() << endl;

	outFile << "Pack FileInfo:" << endl;
	vector<string> vFiles;
	pPackage->GetPackageFileSystem()->FindAllFilesName(vFiles);

	for ( UINT uiIndex = 0; uiIndex < vFiles.size(); uiIndex++ )
	{
		outFile << vFiles[uiIndex].c_str() << endl;
	}
	
	outFile.close();
	
	ShowMessage("信息文件输出完毕" );


	//for (UINT uiIndex = 0; uiIndex < vFiles.size() / 2; uiIndex++)
	//{
	//	if(pPackage->DeletePackFile(vFiles[uiIndex]) == false)
	//	{
	//		string strError = "删除文件:" + vFiles[uiIndex] + "出错";
	//		ShowMessage(strError.c_str());
	//		continue;
	//	}
	//}

	//pPackage->Deflate();

	pPackage->ClosePackage();

	delete pPackage;
	pPackage = NULL;
}

void CPackageToolDlg::OnBnClickedOpenOld()
{
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));

	// TODO: 在此添加控件通知处理程序代码
	CFileDialog *openFileDlg = new CFileDialog(true,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openFileDlg->m_ofn.lpstrFilter = "资源包文件(*.pkg)|*.pkg|All files(*.*)|*.*";

	if ( openFileDlg->DoModal() == IDOK )
	{
		strncpy_s( m_pOldPack, MAX_PATH, (LPCTSTR)openFileDlg->GetPathName(),sizeof(m_pOldPack) );		
	}
	else
	{
		ShowMessage("物件信息读入失败");
		return;
	}

	GetDlgItem( IDC_EDIT_OPEN_OLD )->SetWindowText( m_pOldPack );
}

void CPackageToolDlg::OnBnClickedBtnExportPackInfo()
{
	// TODO: 在此添加控件通知处理程序代码
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));

	ShowMessage("请选择包文件");
	CFileDialog *openFileDlg = new CFileDialog(true,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openFileDlg->m_ofn.lpstrFilter = "资源包文件(*.pkg)|*.pkg|All files(*.*)|*.*";
	char pFilePath[MAX_PATH];

	if ( openFileDlg->DoModal() == IDOK )
	{
		strncpy_s( pFilePath,MAX_PATH, (LPCTSTR)openFileDlg->GetPathName(),sizeof(pFilePath) );		
	}
	else
	{
		ShowMessage("物件信息读入失败");
		return;
	}

	delete openFileDlg;
	openFileDlg = NULL;

	CPackage* pPackage = new CPackage;
	if ( NULL == pPackage)
	{
		ShowMessage("分配内存失败！");
		return;
	}

	if ( !pPackage->OpenPackage(pFilePath,pPackage->EO_READ) )
	{
		ShowMessage("打开失败！");
		return;
	}

	CPackageFileSystem* pPackSys = pPackage->GetPackageFileSystem();
	if (NULL == pPackSys)
	{
		delete pPackage;
		pPackage = NULL;
	}

	UINT nDataEndPos = pPackSys->GetPackageDataEndPos();

	pPackSys = NULL;
	delete pPackage;
	pPackage = NULL;

	//查找文件夹
	ShowMessage("请选择信息文件存放路径");
	string strReleasePath = GetDir();
	if (strReleasePath == "")
	{
		ShowMessage("释放目录不能为空！");
		return;
	}
	if (strReleasePath[strReleasePath.size() -1 ] != '\\')
	{
		strReleasePath += "\\";
	}
	strReleasePath += "PackSystemInfo.dat";

	FILE* pPackFile;
	if (fopen_s(&pPackFile,pFilePath,"rb") != 0)
	{
		pPackFile = NULL;
		return;
	}

	SPackageFileHeader sHeader;
	fread(&sHeader,SIZE_PACK_HEAD,1,pPackFile);
	if (feof(pPackFile))
	{
		fclose(pPackFile);
		pPackFile = NULL;
		return;
	}
	
	fseek(pPackFile,0,SEEK_END);
	int iPackFileEnd = ftell(pPackFile);

	fseek(pPackFile,nDataEndPos,SEEK_SET);
	int nBufferSize = iPackFileEnd - nDataEndPos;
	BYTE* pBuffer = new BYTE[nBufferSize];

	fread(pBuffer,nBufferSize,1,pPackFile);
	if (feof(pPackFile))
	{
		delete [] pBuffer;
		pBuffer = NULL;
		fclose(pPackFile);
		pPackFile = NULL;
		return;
	}
	fclose(pPackFile);
	pPackFile = NULL;

	BYTE* pEnctyptBuffer = new BYTE[nBufferSize];
	if (!EncryptBuffer(pBuffer,nBufferSize,pEnctyptBuffer,nBufferSize))
	{
		delete [] pBuffer;
		pBuffer = NULL;

		delete [] pEnctyptBuffer;
		pEnctyptBuffer = NULL;
		return;
	}
	delete [] pBuffer;
	pBuffer = NULL;

	FILE* pExportFile;
	if (fopen_s(&pExportFile,strReleasePath.c_str(),"wb+") != 0)
	{
		delete [] pEnctyptBuffer;
		pEnctyptBuffer = NULL;
		pExportFile = NULL;
		return;
	}

	// 首先写入dataend位置 [9/7/2009 hemeng]
	fwrite(&nDataEndPos,sizeof(UINT),1,pExportFile);
	// 写入文件信息系统存储位置 [9/7/2009 hemeng]
	fwrite(&sHeader,SIZE_PACK_HEAD,1,pExportFile);

	fwrite(pEnctyptBuffer,nBufferSize,1,pExportFile);
	
	delete [] pEnctyptBuffer;
	pEnctyptBuffer = NULL;
	
	fclose(pExportFile);
	pExportFile = NULL;

	ShowMessage("释放完成");
}

void CPackageToolDlg::OnBnClickedBtnImportPackInfo()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog *openFileDlg = new CFileDialog(true,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openFileDlg->m_ofn.lpstrFilter = "资源包文件(*.dat)|*.dat";
	char pFilePath[MAX_PATH];

	if ( openFileDlg->DoModal() == IDOK )
	{
		strncpy_s( pFilePath,MAX_PATH, (LPCTSTR)openFileDlg->GetPathName(),sizeof(pFilePath) );		
	}
	else
	{
		ShowMessage("物件信息读入失败");
		return;
	}

	delete openFileDlg;
	openFileDlg = NULL;

	// 减去dataend位置存储的大小和文件信息存储位置数据的大小 [9/7/2009 hemeng]
	int nBufferSize = GetBufferSize(pFilePath) - sizeof(UINT) - SIZE_PACK_HEAD;

	if (nBufferSize <= 0)
	{
		return;
	}

	BYTE* pBuffer = new BYTE[nBufferSize];

	FILE* pFile;
	if (fopen_s(&pFile,pFilePath,"rb") != 0)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		pFile = NULL;

		return;
	}


	UINT nDataEndPos = 0;
	SPackageFileHeader sHeader;
	fread(&nDataEndPos,sizeof(UINT),1,pFile);	
	fread(&sHeader,SIZE_PACK_HEAD,1,pFile);
	fread(pBuffer,nBufferSize,1,pFile);
	if (feof(pFile))
	{
		delete [] pBuffer;
		pBuffer = NULL;
		fclose(pFile);
		pFile = NULL;
		return;
	}

	fclose(pFile);
	pFile = NULL;

	BYTE* pDecryptBuffer = new BYTE[nBufferSize];
	if (!DecryptBuffer(pBuffer,nBufferSize,pDecryptBuffer,nBufferSize))
	{
		delete [] pBuffer;
		pBuffer = NULL;
		delete [] pDecryptBuffer;
		pDecryptBuffer = NULL;
		return;
	}
	delete [] pBuffer;
	pBuffer = NULL;

	CFileDialog *openPackDlg = new CFileDialog(true,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openPackDlg->m_ofn.lpstrFilter = "资源包文件(*.pkg)|*.pkg|All files(*.*)|*.*";
	char pPackPath[MAX_PATH];

	if ( openPackDlg->DoModal() == IDOK )
	{
		strncpy_s( pPackPath,MAX_PATH, (LPCTSTR)openPackDlg->GetPathName(),sizeof(pPackPath) );		
	}
	else
	{
		ShowMessage("物件信息读入失败");
		delete [] pDecryptBuffer;
		pDecryptBuffer = NULL;
		delete openPackDlg;
		openPackDlg = NULL;	
		return;
	}

	delete openPackDlg;
	openPackDlg = NULL;	

	FILE* pPackFile;
	if (fopen_s(&pPackFile,pPackPath,"rb+") != 0)
	{
		delete [] pDecryptBuffer;
		pDecryptBuffer = NULL;
		pPackFile = NULL;
		return;
	}

	fseek(pPackFile,nDataEndPos,SEEK_SET);
	if (feof(pPackFile))
	{
		delete [] pDecryptBuffer;
		pDecryptBuffer = NULL;
		fclose(pPackFile);
		pPackFile = NULL;
		return;
	}
	
	fwrite(pDecryptBuffer,nBufferSize,1,pPackFile);
	if (feof(pPackFile))
	{
		ShowMessage("导入出错");
	}

	// 重写包头 [9/7/2009 hemeng]
	fseek(pPackFile,0,SEEK_SET);
	fwrite(&sHeader,SIZE_PACK_HEAD,1,pPackFile);

	fclose(pPackFile);
	pPackFile = NULL;
	delete [] pDecryptBuffer;
	pDecryptBuffer = NULL;
}

void CPackageToolDlg::OnBnClickedDeflate()
{
	// TODO: 在此添加控件通知处理程序代码
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));

	ShowMessage("请选择包文件");
	CFileDialog *openFileDlg = new CFileDialog(true,NULL,NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,NULL,NULL,0);
	openFileDlg->m_ofn.lpstrFilter = "资源包文件(*.pkg)|*.pkg|All files(*.*)|*.*";
	char pFilePath[MAX_PATH];

	if ( openFileDlg->DoModal() == IDOK )
	{
		strncpy_s( pFilePath, MAX_PATH,(LPCTSTR)openFileDlg->GetPathName(),sizeof(pFilePath) );		
	}
	else
	{
		ShowMessage("物件信息读入失败");
		return;
	}

	delete openFileDlg;
	openFileDlg = NULL;

	CPackage* pPackage = new CPackage;
	if ( NULL == pPackage)
	{
		ShowMessage("分配内存失败！");
		return;
	}

	if ( !pPackage->OpenPackage(pFilePath,pPackage->EO_WRITE) )
	{
		ShowMessage("打开失败！");
		pPackage = NULL;
		return;
	}

	if(pPackage->Deflate())
		ShowMessage("紧缩完毕");
	else
		ShowMessage("紧缩失败");

	delete pPackage;
	pPackage = NULL;
}
