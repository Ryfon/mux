﻿// PackageTool.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "PackageTool.h"
#include "PackageToolDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPackageToolApp

BEGIN_MESSAGE_MAP(CPackageToolApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CPackageToolApp 构造

CPackageToolApp::CPackageToolApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CPackageToolApp 对象

CPackageToolApp theApp;


// CPackageToolApp 初始化

BOOL CPackageToolApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	LPSTR strCommand = m_lpCmdLine;
	vector<string> vCommands;
	_SplitString(strCommand,vCommands,' ');

	if (vCommands.size() > 2)
	{
		if (vCommands[0] != "" && vCommands[1] != "" && vCommands[2] != "")
		{
			if (vCommands[0] == "+")
			{
				printf("Process packaging,please wait......");
				if(_ProcessPackaging(vCommands[1],vCommands[2]) == false)
				{
					ERROR_PRINT("Package failed!",vCommands[1].c_str());
					printf("Packaging failed!");
				}
				printf("Packaging success!");
			}
			else if (vCommands[0] == "-")
				{
					printf("Process detaching,please wait......");
					if(_ProcessDetaching(vCommands[1],vCommands[2]) == false)
					{
						ERROR_PRINT("Package failed!",vCommands[1].c_str());
						printf("Detaching failed!");
					}
					printf("Packaging success!");
				}
			/*else
			{
				ERROR_PRINT("")
			}*/

		}
	}
	
	else
	{
		CPackageToolDlg dlg;
		m_pMainWnd = &dlg;
		INT_PTR nResponse = dlg.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: 在此处放置处理何时用“确定”来关闭
			//  对话框的代码
		}
		else if (nResponse == IDCANCEL)
		{
			// TODO: 在此放置处理何时用“取消”来关闭
			//  对话框的代码
		}
	}

	

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

void CPackageToolApp::_SplitString(const char * szStr, vector< string >& vStrs, char chToken )
{
	if (NULL == szStr)
	{
		return;
	}

	string strCur;
	for ( UINT nIndex = 0; nIndex < strlen( szStr ); nIndex ++ )
	{
		if ( chToken == szStr[nIndex] )
		{
			vStrs.push_back( strCur );
			strCur = "";
		}
		else
		{
			strCur.push_back( szStr[nIndex] );
		}
	}

	if ( strCur.length() >= 1 )
		vStrs.push_back( strCur );
}

////////////////////////////////////////////////////////////////////////////////
//函数名:_ProcessPackaging
//功能:命令行打包
//入口参数:string strFileDir		打包目录
//			string strPackPath		包文件路径
//出口参数:bool
////////////////////////////////////////////////////////////////////////////////
bool CPackageToolApp::_ProcessPackaging(string strFileDir, string strPackPath)
{
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));

	if (strFileDir == "" || strPackPath == "")
	{
		return false;
	}

	string strTemp =strFileDir.substr(strFileDir.find_last_of("\\") + 1) + ".pkg";
	strPackPath += "\\";
	strPackPath += strTemp;

	//创建包
	CPackage* pPackage = new CPackage();
	if ( !pPackage->OpenPackage( strPackPath, CPackage::EO_WRITE, true ) )
	{
		ERROR_PRINT( "创建包失败！",strPackPath.c_str() );
		return false;
	}
	pPackage->AddDirFiles(strFileDir);

	pPackage->ClosePackage();

	delete pPackage;
	pPackage = NULL;

	printf( "文件添加成功!" );
	return true;
}

////////////////////////////////////////////////////////////////////////////////
//函数名:_ProcessDetaching
//功能:命令行解包
//入口参数:string strPackDir		包文件路径
//			string strDetachPath	文件释放路径
//出口参数:bool
////////////////////////////////////////////////////////////////////////////////
bool CPackageToolApp::_ProcessDetaching(string strPackDir, string strDetachPath)
{
	//本地化，防止路径为中文不能识别
	std::locale::global(std::locale(""));

	CPackage* pPackage = new CPackage();
	if ( NULL == pPackage)
	{
		ERROR_PRINT("分配内存失败！",strPackDir.c_str());
		return false;
	}

	if ( !pPackage->OpenPackage(strPackDir) )
	{
		ERROR_PRINT("打开失败！",strPackDir.c_str());
		delete pPackage;
		pPackage = NULL;
		return false;
	}

	vector<string> vFiles;
	pPackage->GetPackageFileSystem()->FindAllFilesName(vFiles);

	for ( UINT uiIndex = 0; uiIndex < vFiles.size(); uiIndex++ )
	{
		string strAbsFilePath = strDetachPath + "\\" + vFiles[uiIndex];

		if (_access_s(strAbsFilePath.c_str(),0) == 0)
		{
			// 如果文件已存在且不可写，则跳过 [8/26/2009 hemeng]
			if (!DeleteFile(strAbsFilePath.c_str()))
			{
				continue;
			}	
		}
		
		if ( !pPackage->CreateNewFileFromPackage(vFiles[uiIndex],strAbsFilePath))
		{
			ERROR_PRINT("创建新文件失败！",vFiles[uiIndex].c_str());
			continue;
		}

		
	}

	pPackage->ClosePackage();

	delete pPackage;
	pPackage = NULL;

	printf("释放完成！");
	return true;
}