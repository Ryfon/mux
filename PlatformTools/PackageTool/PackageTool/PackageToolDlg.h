﻿// PackageToolDlg.h : 头文件
//
#include "..\Pack_API\Package.h"
#pragma once#include "afxwin.h"

// CPackageToolDlg 对话框
class CPackageToolDlg : public CDialog
{
// 构造
public:
	CPackageToolDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_PACKAGETOOL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	void ShowMessage(const char* szMessage);
	string GetDir();

public:
	afx_msg void OnBnClickedOpenFile();

private:
	char m_pLatestPack[MAX_PATH];
	char m_pOldPack[MAX_PATH];

public:
	afx_msg void OnBnClickedButtonPackPath();
	afx_msg void OnBnClickedOk();
	CEdit m_editPackingInfo;
	CEdit m_editExistPackagePath;

private:
	//for test only
	afx_msg void OnBnClickedOpenPackage();
public:
	CEdit m_editProcessInfo;
	afx_msg void OnBnClickedOpenLatest();
	afx_msg void OnBnClickedButtonTest();
	afx_msg void OnBnClickedOpenOld();
	CEdit m_editDirPath;
	CEdit m_editPackFilePath;
	afx_msg void OnBnClickedBtnExportPackInfo();
	afx_msg void OnBnClickedBtnImportPackInfo();
	afx_msg void OnBnClickedDeflate();
};
