﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PackageTool.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PACKAGETOOL_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_MSG                  130
#define IDC_EDIT_OPEN_PACK_PATH         1002
#define IDC_OPEN_FILE                   1003
#define IDC_EDIT_PACK_SAVE_PATH         1004
#define IDC_BUTTON_PACK_PATH            1005
#define IDC_EDIT_OPEN_PACK_PATH2        1006
#define IDC_EDIT_OPEN_EXISTPACK_PATH    1006
#define IDC_EDIT_OPEN_LATEST            1006
#define IDC_OPEN_LATEST                 1007
#define IDC_EDIT_OPEN_OLD               1008
#define IDC_EDIT_OPINFO                 1009
#define IDC_OPEN_OLD                    1010
#define IDC_EDIT_MSG                    1011
#define IDC_OPEN_PACKAGE                1012
#define IDC_EDIT_PROCESSINFO            1014
#define IDC_BUTTON_TEST                 1018
#define IDC_BUTTON3                     1019
#define IDC_BUTTON_CREATEDIFFERPACK     1019
#define IDC_BUTTON1                     1020
#define IDC_BTN_CHANGECODEVERSION       1020
#define IDC_DEFLATE                     1020
#define IDC_BTN_EXPORT_PACK_INFO        1021
#define IDC_BUTTON4                     1022
#define IDC_BTN_IMPORT_PACK_INFO        1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
