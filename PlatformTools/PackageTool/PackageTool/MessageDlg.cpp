﻿// MessageDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "PackageTool.h"
#include "MessageDlg.h"


// CMessageDlg 对话框

IMPLEMENT_DYNAMIC(CMessageDlg, CDialog)

CMessageDlg::CMessageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMessageDlg::IDD, pParent)
{

}

CMessageDlg::~CMessageDlg()
{
}

void CMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_MSG, m_editMessage);

	m_editMessage.SetSel( 0, -1 );
	m_editMessage.Clear();
	m_editMessage.ReplaceSel( m_szMessage );
}

void CMessageDlg::SetDlgInfo(const char* szMessage)
{
	m_szMessage = szMessage;
}

BEGIN_MESSAGE_MAP(CMessageDlg, CDialog)
END_MESSAGE_MAP()


// CMessageDlg 消息处理程序
