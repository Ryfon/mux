﻿#pragma once
#include "afxwin.h"

#include <string>
// CMessageDlg 对话框

class CMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CMessageDlg)

public:
	CMessageDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CMessageDlg();

	void SetDlgInfo(const char* szMessage);

// 对话框数据
	enum { IDD = IDD_DIALOG_MSG };
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_editMessage;
	const char* m_szMessage;

};
