﻿// PackageTool.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "MessageDlg.h"


// CPackageToolApp:
// 有关此类的实现，请参阅 PackageTool.cpp
//
#include <vector>
#include <string>
// modify [8/3/2009 hemeng]
#include "..\Pack_API\Package.h"
// end [8/3/2009 hemeng]
using namespace std;

class CPackageToolApp : public CWinApp
{
public:
	CPackageToolApp();

// 重写
	public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
private:
	void _SplitString( const char * szStr, vector< string >& vStrs, char chToken );
	////////////////////////////////////////////////////////////////////////////////
	//函数名:_ProcessPackaging
	//功能:命令行打包
	//入口参数:string strFileDir		打包目录
	//			string strPackPath		包文件路径
	//出口参数:bool
	////////////////////////////////////////////////////////////////////////////////
	bool _ProcessPackaging(string strFileDir, string strPackPath);

	////////////////////////////////////////////////////////////////////////////////
	//函数名:_ProcessDetaching
	//功能:命令行解包
	//入口参数:string strPackDir		包文件路径
	//			string strDetachPath	文件释放路径
	//出口参数:bool
	////////////////////////////////////////////////////////////////////////////////
	bool _ProcessDetaching(string strPackDir, string strDetachPath);
};

extern CPackageToolApp theApp;