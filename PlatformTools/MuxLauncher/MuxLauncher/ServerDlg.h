﻿#pragma once

#include "PatchCliIntf.h"
#include "SelectDlg.h"

#define MAX_SERVER_NUMBER	4
// CServerDlg 对话框

class CServerDlg : public CDialog
{
	DECLARE_DYNAMIC(CServerDlg)

public:
	CServerDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CServerDlg();

// 对话框数据
	enum { IDD = IDD_SERVER_DIALOG };

// 成员函数
private:
	void PaintBk(CDC* pDC, CRect rect);
	void ShowText(CDC* dc, CString strTextValue, CRect rect, COLORREF crColor);
	void GetServerList(const char * czFileName);
	void GetServerList();
public:
	LRESULT OnServerDlgUpdate(WPARAM wParam, LPARAM lParam);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	void SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent);
	BOOL PreTranslateMessage(MSG* pMsg);
private:
	CBitmap*			m_pBkgndImage;
	CString				m_strBkgndImageName;
	CFourBitmapButton	m_ImageButtonApplyProtocol;
	CFourBitmapButton	m_ImageButtonRecommandServer;
	CFourBitmapButton	m_ImageButtonServer[4];
	COLORREF			m_crServerNameColor;

	CRect m_rectRecommandLabel;
	CString m_strRecommandLabel;
	COLORREF m_crRecommandLabel;
	
	CRect m_rectSelectLabel;
	CString m_strSelectLabel;
	COLORREF m_crSelectLabel;
	
	CRect m_rectRecommandName;
	CString m_strRecommandName;
	COLORREF m_crRecommandName;

	CRect m_rectSelectName;
	CString m_strSelectName;
	COLORREF m_crSelectName;

	CString m_ServerName[MAX_SERVER_NUMBER];
	int m_iMaxServerNum;

	CPatchCliIntf* m_PatchCliIntf;
	PCATINFO m_pServerInfo;

private:
	CSelectDlg*	m_pSelectDlg;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnServerButtonClicked(UINT uID);
	afx_msg void OnBnClickedButtonAppProtocol();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonRecommandServer();
};
