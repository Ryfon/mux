﻿#pragma once

#define MAXPAGE 16
// CTabSheet

class CTabSheet : public CTabCtrl
{
	DECLARE_DYNAMIC(CTabSheet)

public:
	CTabSheet();
	virtual ~CTabSheet();

public:
	int GetCurSel();
	int SetCurSel(int nItem);
	void Show();
	void SetRect();
	BOOL AddPage(LPCTSTR strTitle, CDialog* pDialog, UINT ID);
protected:
	LPCTSTR m_strTitles[MAXPAGE];
	UINT m_DlgIds[MAXPAGE];
	CDialog* m_pPages[MAXPAGE];
	int m_NumOfPages;
	int m_CurPageIndex;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};


