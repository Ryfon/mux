﻿#include "StdAfx.h"
#include "ColorText.h"

CColorText::CColorText(void)
{
	m_strText = _T("");
	m_rectLocation.SetRect(0, 0, 32, 20);
	m_crTextColor = RGB(0, 0, 0);
	m_Font.CreateFont(14, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_iBkMode = TRANSPARENT;
	m_nFormat = DT_CENTER | DT_VCENTER | DT_SINGLELINE;		
	m_crShadowColor = RGB(0, 0, 255);
	m_iShadowNum = 0;
}

CColorText::~CColorText(void)
{
}

CColorText::CColorText(CString text, CRect *location, COLORREF textColor/* = RGB(0,0,0)*/, CFont *font/* = NULL*/)
{
	m_strText = text;

	if (location != NULL)
	{
		m_rectLocation.CopyRect(location);
	}
	
	m_crTextColor = textColor;

	if (font != NULL)
	{
		LOGFONT tempFont;
		font->GetLogFont(&tempFont);
		m_Font.CreateFontIndirect(&tempFont);
	}
	else
	{
		m_Font.CreateFont(14, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	}

	// 默认值
	m_iBkMode = TRANSPARENT;
	m_nFormat = DT_CENTER | DT_VCENTER | DT_SINGLELINE;		
	m_crShadowColor = RGB(0, 0, 255);
	m_iShadowNum = 0;
}

void CColorText::Copy(CColorText *src)
{
	m_strText = src->m_strText;

	m_rectLocation.CopyRect(src->m_rectLocation);


	m_crTextColor = src->m_crTextColor;

	LOGFONT tempFont;
	src->m_Font.GetLogFont(&tempFont);
	m_Font.Detach();
	m_Font.CreateFontIndirect(&tempFont);

	m_iBkMode = src->m_iBkMode;
	m_nFormat = src->m_nFormat;
	m_crShadowColor = src->m_crShadowColor;
	m_iShadowNum = src->m_iShadowNum;
}

void CColorText::SetText(CString text)
{
	m_strText = text;
}

void CColorText::SetTextColor(COLORREF color)
{
	m_crTextColor = color;
}

void CColorText::SetBkMode(int mode)
{
	m_iBkMode = mode;
}

void CColorText::SetFormat(UINT format)
{
	m_nFormat = format;
}

void CColorText::SetShadow(COLORREF color, int num)
{
	m_crShadowColor = color;
	m_iShadowNum = num;
}

void CColorText::Draw(CDC *dc)
{
	// 先画阴影
	if (m_iShadowNum > 0)
	{
		DrawShadow(dc);
	}

	// 后写字
	DrawText(dc, m_strText, &m_rectLocation, m_crTextColor, &m_Font, m_iBkMode, m_nFormat);
}

void CColorText::DrawText(CDC* dc, CString text, CRect* rect, COLORREF color, CFont* font, int mode, UINT format)
{
	if (dc != NULL && text.GetLength() > 0)
	{
		// 字体
		CFont* def_font = dc->SelectObject(font);

		// 背景模式
		dc->SetBkMode(mode);

		// 字体颜色
		dc->SetTextColor(color);

		// 写字
		dc->DrawText(text, rect, format);

		// 清理工作
		dc->SelectObject(def_font);
	}
}

void CColorText::DrawShadow(CDC *dc)
{
	// 多层阴影
	for (int iIndex = m_iShadowNum;iIndex > 0;--iIndex)
	{
		// 计算位置
		CRect rect;
		rect.CopyRect(m_rectLocation);
		rect.OffsetRect(iIndex, iIndex);

		// 计算颜色
		int sR = GetRValue(m_crTextColor);
		int sG = GetGValue(m_crTextColor);
		int sB = GetBValue(m_crTextColor);
		int dR = GetRValue(m_crShadowColor);
		int dG = GetGValue(m_crShadowColor);
		int dB = GetBValue(m_crShadowColor);
		int cR = (sR*(m_iShadowNum - iIndex) + dR*iIndex) / m_iShadowNum;
		int cG = (sG*(m_iShadowNum - iIndex) + dG*iIndex) / m_iShadowNum;
		int cB = (sB*(m_iShadowNum - iIndex) + dB*iIndex) / m_iShadowNum;
		COLORREF color = RGB(cR, cG, cB);

		// 渲染
		DrawText(dc, m_strText, &rect, color, &m_Font, m_iBkMode, m_nFormat);
	}
}