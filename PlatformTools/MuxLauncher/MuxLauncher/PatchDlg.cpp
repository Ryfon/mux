﻿// PatchDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "PatchDlg.h"
#include "ConfigDlg.h"

// CPatchDlg 对话框

IMPLEMENT_DYNAMIC(CPatchDlg, CDialog)

CPatchDlg::CPatchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPatchDlg::IDD, pParent)
{
	m_pBkgndImage = NULL;
	m_PatchCliIntf = CreatePatchIntf(this);
	m_bPatchCliIntfStarted = FALSE;
	m_pSettingDlg = NULL;
	m_pServerDlg = NULL;
}

CPatchDlg::~CPatchDlg()
{
	if (m_pSettingDlg != NULL)
	{
		delete m_pSettingDlg;
		m_pSettingDlg = NULL;
	}

	if (m_bPatchCliIntfStarted)
	{
		m_PatchCliIntf->CleanUp();
	}
	DeletePatchIntf(m_PatchCliIntf);
}

void CPatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CPatchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: 在此添加额外的初始化代码

	// 获取UI配置
	CDialogConfig* pDialogConfig = ConfigManager::GetInstance()->GetDialogConfig(_T("PatchDlg"));
	TiXmlElement* pXmlDialogConfig = pDialogConfig->GetConfig();
	TiXmlElement* pXmlTextClientVersion = pDialogConfig->GetChildConfig(_T("ClientVersion"));
	TiXmlElement* pXmlTextServerVersion = pDialogConfig->GetChildConfig(_T("ServerVersion"));
	TiXmlElement* pXmlButtonConfig = pDialogConfig->GetChildConfig(_T("Config"));
	TiXmlElement* pXmlButtonStart = pDialogConfig->GetChildConfig(_T("Start"));
	TiXmlElement* pXmlButtonManual = pDialogConfig->GetChildConfig(_T("Manual"));
	TiXmlElement* pXmlProgressDownLoad = pDialogConfig->GetChildConfig(_T("DownLoad"));
	TiXmlElement* pXmlProgressInstall = pDialogConfig->GetChildConfig(_T("Install"));

	// 初始化自身大小
	m_strBkgndImageName = pXmlDialogConfig->Attribute("BGImage");
	this->MoveWindow(pXmlDialogConfig->IntAttribute("Left"), pXmlDialogConfig->IntAttribute("Top"), pXmlDialogConfig->IntAttribute("Width"), pXmlDialogConfig->IntAttribute("Height"));
	//this->MoveWindow(9, 415, 694, 88);
	SetupRegion(GetDC(), pDialogConfig->GetShapeName(), RGB(255, 0, 255));

	// 初始化控件
	this->GetDlgItem(IDC_BUTTON_CONFIG)->MoveWindow(pXmlButtonConfig->IntAttribute("Left"), pXmlButtonConfig->IntAttribute("Top"), pXmlButtonConfig->IntAttribute("Width"), pXmlButtonConfig->IntAttribute("Height"));
	//m_ImageButtonGameConfig.InitButton(IDC_BUTTON_CONFIG, this, L"display_set_1.bmp", L"display_set_2.bmp", L"display_set_3.bmp", L"display_set_4.bmp");
	//m_ImageButtonGameConfig.InitButton(IDC_BUTTON_CONFIG, this, CString(pXmlButtonConfig->Attribute("BGImage")), pXmlButtonConfig->IntAttribute("Width"), pXmlButtonConfig->IntAttribute("Height"));
	m_ImageButtonGameConfig.InitButton(IDC_BUTTON_CONFIG,this,CString(pXmlButtonConfig->Attribute("NormalImage")),CString(pXmlButtonConfig->Attribute("HoverImage")),CString(pXmlButtonConfig->Attribute("PressedImage")));

	this->GetDlgItem(IDC_BUTTON_START)->MoveWindow(pXmlButtonStart->IntAttribute("Left"), pXmlButtonStart->IntAttribute("Top"), pXmlButtonStart->IntAttribute("Width"), pXmlButtonStart->IntAttribute("Height"));
	//m_ImageButtonGameStart.InitButton(IDC_BUTTON_START, this, L"start_button_1.bmp", L"start_button_2.bmp", L"start_button_3.bmp", L"start_button_4.bmp");
	//m_ImageButtonGameStart.InitButton(IDC_BUTTON_START, this, CString(pXmlButtonStart->Attribute("BGImage")), pXmlButtonStart->IntAttribute("Width"), pXmlButtonStart->IntAttribute("Height"));
	m_ImageButtonGameStart.InitButton(IDC_BUTTON_START,this,CString(pXmlButtonStart->Attribute("NormalImage")),CString(pXmlButtonStart->Attribute("HoverImage")),CString(pXmlButtonStart->Attribute("PressedImage")),CString(pXmlButtonStart->Attribute("DisableImage")));
	//this->GetDlgItem(IDC_BUTTON_START)->EnableWindow(FALSE);

	this->GetDlgItem(IDC_BUTTON_DOWNLOAD)->MoveWindow(pXmlButtonManual->IntAttribute("Left"), pXmlButtonManual->IntAttribute("Top"), pXmlButtonManual->IntAttribute("Width"), pXmlButtonManual->IntAttribute("Height"));
	//m_ImageButtonManualDownload.InitButton(IDC_BUTTON_DOWNLOAD, this, CString(pXmlButtonManual->Attribute("BGImage")), pXmlButtonManual->IntAttribute("Width"), pXmlButtonManual->IntAttribute("Height"));
	m_ImageButtonManualDownload.InitButton(IDC_BUTTON_DOWNLOAD,this,CString(pXmlButtonManual->Attribute("NormalImage")),CString(pXmlButtonManual->Attribute("HoverImage")),CString(pXmlButtonManual->Attribute("PressedImage")));

	this->GetDlgItem(IDC_PROGRESS_DOWNLOAD_NEW)->MoveWindow(pXmlProgressDownLoad->IntAttribute("Left"), pXmlProgressDownLoad->IntAttribute("Top"), pXmlProgressDownLoad->IntAttribute("Width"), pXmlProgressDownLoad->IntAttribute("Height"));
	m_ImageProgressCtrlDownLoad.SubclassDlgItem(IDC_PROGRESS_DOWNLOAD_NEW, this);
	m_ImageProgressCtrlDownLoad.InitImages(CString(pXmlProgressDownLoad->Attribute("BGImage")), pXmlProgressDownLoad->IntAttribute("Width"), pXmlProgressDownLoad->IntAttribute("Height"));
	m_ImageProgressCtrlDownLoad.SetShowImage(TRUE);
	m_ImageProgressCtrlDownLoad.SetDisplayText(L"下载进度：");
	m_ImageProgressCtrlDownLoad.SetTextForeColour(RGB(255, 255, 255));
	m_ImageProgressCtrlDownLoad.SetBkColour(RGB(150, 150, 150));
	m_ImageProgressCtrlDownLoad.SetRange(0, 100);

	this->GetDlgItem(IDC_PROGRESS_INSTALL_NEW)->MoveWindow(pXmlProgressInstall->IntAttribute("Left"), pXmlProgressInstall->IntAttribute("Top"), pXmlProgressInstall->IntAttribute("Width"), pXmlProgressInstall->IntAttribute("Height"));
	m_ImageProgressCtrlInstall.SubclassDlgItem(IDC_PROGRESS_INSTALL_NEW, this);
	m_ImageProgressCtrlInstall.InitImages(CString(pXmlProgressInstall->Attribute("BGImage")), pXmlProgressInstall->IntAttribute("Width"), pXmlProgressInstall->IntAttribute("Height"));
	m_ImageProgressCtrlInstall.SetShowImage(TRUE);
	m_ImageProgressCtrlInstall.SetDisplayText(L"安装进度：");
	m_ImageProgressCtrlInstall.SetTextForeColour(RGB(255, 255, 255));
	m_ImageProgressCtrlInstall.SetBkColour(RGB(150, 150, 150));
	m_ImageProgressCtrlInstall.SetRange(0, 100);
	
	// 版本显示
	m_strClientVersion = pXmlTextClientVersion->Attribute("Text");
	m_strServerVersion = pXmlTextServerVersion->Attribute("Text");
	m_rectServer.SetRect(pXmlTextServerVersion->IntAttribute("Left"), pXmlTextServerVersion->IntAttribute("Top"), pXmlTextServerVersion->IntAttribute("Right"), pXmlTextServerVersion->IntAttribute("Bottom"));
	m_rectClient.SetRect(pXmlTextClientVersion->IntAttribute("Left"), pXmlTextClientVersion->IntAttribute("Top"), pXmlTextClientVersion->IntAttribute("Right"), pXmlTextClientVersion->IntAttribute("Bottom"));
	m_crServerVersion = RGB(pXmlTextServerVersion->IntAttribute("R"), pXmlTextServerVersion->IntAttribute("G"), pXmlTextServerVersion->IntAttribute("B"));
	m_crClientVersion = RGB(pXmlTextClientVersion->IntAttribute("R"), pXmlTextClientVersion->IntAttribute("G"), pXmlTextClientVersion->IntAttribute("B"));

	// 启动Patch
	//m_PatchCliIntf->StartUp();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CPatchDlg::SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent)
{
	// 获取配置信息
	CShapeConfig* pShapeConfig = ConfigManager::GetInstance()->GetShapeConfig(strBitmapFileName);
	vector<CRect*>& arrWorkRect = pShapeConfig->GetWorkRectList();

	// 定义工作区
	/*CRect arrWorkRect[3];
	int	iWorkRectNum = 3;
	arrWorkRect[0].SetRect(0, 0, 16, 87);
	arrWorkRect[1].SetRect(686, 0, 694, 87);
	arrWorkRect[2].SetRect(17, 84, 685, 87);*/
	
	// 定义变量
	CDC memDC;
	CBitmap* pBitmap;
	CBitmap* pOldMemBmp = NULL;
	COLORREF cl;
	CRect cRect;
	int x, y;
	CRgn wndRgn, rgnTemp;

	// 获得窗口大小
	GetWindowRect(&cRect);

	// 加载位图资源
	HBITMAP temp = (HBITMAP)LoadImage(NULL, pShapeConfig->GetImageName(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		pBitmap = CBitmap::FromHandle(temp);
		memDC.CreateCompatibleDC(pDC);
		pOldMemBmp = memDC.SelectObject(pBitmap);

		// 创建默认区域
		wndRgn.CreateRectRgn(0, 0, cRect.Width(), cRect.Height());

		// 建立窗口
		//for (x = 0;x <= cRect.Width();++x)
		//{
		//	for (y = 0;y <= cRect.Height();++y)
		//	{
		//		// 获取坐标处的像素值
		//		cl = memDC.GetPixel(x, y);

		//		// 扣除
		//		if (cl == crTransparent)
		//		{
		//			rgnTemp.CreateRectRgn(x, y, x+1, y+1);
		//			wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
		//			rgnTemp.DeleteObject();
		//		}
		//	}
		//}
		for(int nRectIndex = 0;nRectIndex < arrWorkRect.size();++nRectIndex)
		{
			CRect* rectWork = arrWorkRect[nRectIndex];

			for (x = rectWork->left;x <= rectWork->right;++x)
			{
				for (y = rectWork->top;y <= rectWork->bottom;++y)
				{
					// 获取坐标处的像素值
					cl = memDC.GetPixel(x, y);

					// 扣除
					if (cl == pShapeConfig->GetTransparentColor())
					{

						rgnTemp.CreateRectRgn(x, y, x+1, y+1);
						wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
						rgnTemp.DeleteObject();

					}
				}
			}
		}

		// 清除资源
		pBitmap->DeleteObject();
		memDC.SelectObject(pOldMemBmp);
		memDC.DeleteDC();

		// 指定窗口
		SetWindowRgn((HRGN)wndRgn, TRUE);
	}
}

BEGIN_MESSAGE_MAP(CPatchDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_START, &CPatchDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG, &CPatchDlg::OnBnClickedButtonConfig)
	ON_WM_ERASEBKGND()
//	ON_WM_PAINT()
ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// 逻辑代码
BOOL CPatchDlg::StartGame()
{
	// 启动参数初始化
	STARTUPINFO start;
	PROCESS_INFORMATION process;
	memset(&start, 0, sizeof(STARTUPINFO));
	start.cb = sizeof(start);
	
	// 先尝试从dat启动
	TCHAR szFileName[MAX_PATH] = L"MuxR.dat";
	if (CreateProcess(NULL, szFileName, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &start, &process))
	{
		return TRUE;
	}
	else
	{
		// 从exe启动
		if (ShellExecute(NULL, _T("open"), L"SSR.exe", NULL, NULL, SW_SHOWNORMAL) > HINSTANCE(32))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}

void CPatchDlg::PaintBk(CDC* pDC, CRect rect)
{
	CDC tempDC;
	CBitmap tempBmp;
	
	if (pDC != NULL)
	{
		tempDC.CreateCompatibleDC(pDC);
		tempBmp.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
		CBitmap* pOldBmp = tempDC.SelectObject(&tempBmp);

		tempDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
		
		pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &tempDC, 0, 0, SRCCOPY);

		tempBmp.DeleteObject();
		tempDC.SelectObject(pOldBmp);
		tempDC.DeleteDC();
	}
} // End of PaintBk

void CPatchDlg::ShowText(CDC* dc, CString strTextValue, CRect rect, COLORREF crColor)
{
	// 重绘背景
	PaintBk(dc, rect);

	// 字体输出
	CFont font;
	font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体") );
	CFont* def_font = dc->SelectObject(&font);
	dc->SetBkMode(TRANSPARENT);
	dc->SetTextColor(crColor);
	dc->TextOut(rect.left, rect.top, strTextValue);

	dc->SelectObject(def_font);
	font.DeleteObject();
}

void CPatchDlg::SetServerDlg(CDialog *pServerDlg)
{
	m_pServerDlg = pServerDlg;
}

// 事件
int CPatchDlg::OnCliVersion(PATCHVERSION &version)
{
	WCHAR szBuf[128];
	wsprintf(szBuf, _T("本地版本：%d.%u.%d"), version.MajorVersion, version.MinVersion, version.AdditionVersion);
	m_strClientVersion = szBuf;
	//ShowText(GetDC(), m_strClientVersion, m_rectClient, RGB(255, 255, 255));
	this->Invalidate();

	return 0;
}

int CPatchDlg::OnServerVersion(PATCHVERSION& version, unsigned char ucPatchNum)
{
	WCHAR szBuf[128];
	wsprintf(szBuf, L"服务器版本：%d.%u.%d", version.MajorVersion, version.MinVersion, version.AdditionVersion);
	m_strServerVersion = szBuf;
	//ShowText(GetDC(), m_strServerVersion, m_rectServer, RGB(255, 255, 255));
	this->Invalidate();

	m_iPatchNum = ucPatchNum;
	wsprintf(szBuf, L"下载进度：(%d/%d)", 0, m_iPatchNum);
	
	if (ucPatchNum)
	{
		m_PatchCliIntf->CheckVersion();
	}
	else
	{
		if (StartGame())
		{
			ConfigManager::GetInstance()->SaveGameConfigure("UserData/Configuration.xml");
			//m_PatchCliIntf->CleanUp();
			AfxGetApp()->GetMainWnd()->PostMessage(WM_CLOSE, 0, 0);
		}
		else
		{
			MessageBox(L"游戏启动失败！");
		}
		//this->GetDlgItem(IDC_BUTTON_START)->EnableWindow(TRUE);
	}

	return 0;
}

int CPatchDlg::OnDownloadProgress(PATCHVERSION& version, int nIndex, int nPos, int nDownloadSpeed, int nUploadSpeed, float fFileSize)
{
	//nFileSize 文件
	//WCHAR szBuf[128];
	//wsprintf(szBuf, L"下载进度：%d%s(%d/%d)(%fM)", nPos, _T("%"), nIndex, m_iPatchNum, fFileSize);
	CString strTemp;
	strTemp.Format(_T("下载进度：%d%s(%d/%d)(%2.1fM)"), nPos, _T("%"), nIndex, m_iPatchNum, fFileSize);
	m_ImageProgressCtrlDownLoad.SetDisplayText(strTemp);
	m_ImageProgressCtrlDownLoad.SetPos(nPos);

	return 0;
}

int CPatchDlg::OnInstallProgress(PATCHVERSION& version, int nStatus, int nPos)
{
	WCHAR szBuf[128];
	wsprintf(szBuf, L"安装进度：%d%s", nPos, L"%");
	m_ImageProgressCtrlInstall.SetDisplayText(szBuf);
	m_ImageProgressCtrlInstall.SetPos(nPos);


	wsprintf(szBuf, _T("本地版本：%d.%u.%d"), version.MajorVersion, version.MinVersion, version.AdditionVersion);
	m_strClientVersion = szBuf;
	//ShowText(GetDC(), m_strClientVersion, m_rectClient, RGB(255, 255, 255));
	this->Invalidate();

	if (nPos == 100)
	{
		if (StartGame())
		{
			ConfigManager::GetInstance()->SaveGameConfigure("UserData/Configuration.xml");
			//m_PatchCliIntf->CleanUp();
			AfxGetApp()->GetMainWnd()->PostMessage(WM_CLOSE, 0, 0);
		}
		else
		{
			MessageBox(L"游戏启动失败！");
		}
		//this->GetDlgItem(IDC_BUTTON_START)->EnableWindow(TRUE);
	}

	return 0;
}

int CPatchDlg::OnError(int nErrorCode)
{
	CString strErrorMsg;

	switch(nErrorCode)
	{
	case MUXPATCH_DOWNLOAD_SERVERNOTFOUND:
		strErrorMsg = L"更新服务器不可用";
		break;
	case MUXPATCH_DOWNLOAD_NETBREAKDOWN:
		strErrorMsg = L"网络连接已断开";
	    break;
	case MUXPATCH_ERROR_INIT:
		strErrorMsg = _T("初始化失败");
		break;
	case MUXPATCH_ERROR_CONTENTVERSION:
		strErrorMsg = _T("错误的内容版本（与当前更新包版本不符）");// 错误的内容版本（与当前更新包版本不符）  [8/24/2009 hemeng]
		break;
	case MUXPATCH_ERROR_UPDATEFAIL:
		strErrorMsg = _T("更新失败");// 更新失败 [8/24/2009 hemeng]
		break;
	case MUXPATCH_ERROR_FINISHING:
		strErrorMsg = _T("完成更新出错");// 完成更新是出错 [8/24/2009 hemeng]
		break;
	default:
	    return 0;
		break;
	}

	MessageBox(strErrorMsg);
	
	//this->GetDlgItem(IDC_BUTTON_START)->EnableWindow(TRUE);

	return 0;
}

// CPatchDlg 消息处理程序
BOOL CPatchDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
			return TRUE;
		case VK_ESCAPE:
		    return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CPatchDlg::OnBnClickedButtonStart()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!m_bIsApplyProtocol)
	{
		MessageBox(L"您还没有同意服务条款协议！");
		return;
	}

	// 保存配置
	//ConfigManager::GetInstance()->SaveGameConfigure("UserData/Configuration.xml");

	//if (StartGame())
	//{
	//	ConfigManager::GetInstance()->SaveGameConfigure("UserData/Configuration.xml");
	//	PostQuitMessage(0);
	//}
	//else
	//{
	//	MessageBox(L"游戏启动失败！");
	//}
	m_PatchCliIntf->StartUp(g_szPatchIp, g_PatchPort);
	m_bPatchCliIntfStarted = TRUE;
	this->GetDlgItem(IDC_BUTTON_START)->EnableWindow(FALSE);
	if(m_pServerDlg != NULL)
	{
		m_pServerDlg->SendMessage(WM_SERVER_DLG_UPDATE, 1, 0);
	}
}

void CPatchDlg::OnBnClickedButtonConfig()
{
	// TODO: 在此添加控件通知处理程序代码
	/*CConfigDlg dlg;
	dlg.DoModal();*/

	if (m_pSettingDlg == NULL)
	{
		m_pSettingDlg = new CSettingDlg(this);
		m_pSettingDlg->Create(IDD_SETTING_DIALOG, this);
	}

	::AfxGetMainWnd()->EnableWindow(FALSE);

	m_pSettingDlg->ShowConfigure();
	m_pSettingDlg->ShowWindow(SW_SHOW);
	m_pSettingDlg->SetFocus();
}

BOOL CPatchDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_pBkgndImage == NULL)
	{
		HBITMAP temp = (HBITMAP)LoadImage(NULL, m_strBkgndImageName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		if (temp != NULL)
		{
			m_pBkgndImage = CBitmap::FromHandle(temp);
		}
		else
		{
			m_pBkgndImage = NULL;
		}
	}
	
	if (m_pBkgndImage != NULL)
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		BITMAP hBitmap;
		m_pBkgndImage->GetBitmap(&hBitmap);

		CBitmap* pOldBmp = memDC.SelectObject(m_pBkgndImage);
		
		//pDC->BitBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, SRCCOPY);
		pDC->TransparentBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, hBitmap.bmWidth, hBitmap.bmHeight, RGB(255, 0, 255));

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();

		ShowText(GetDC(), m_strServerVersion, m_rectServer, m_crServerVersion);
		ShowText(GetDC(), m_strClientVersion, m_rectClient, m_crClientVersion);

		m_ImageButtonGameStart.Invalidate();
		m_ImageButtonGameConfig.Invalidate();
		m_ImageButtonManualDownload.Invalidate();
		m_ImageProgressCtrlDownLoad.Invalidate();
		m_ImageProgressCtrlInstall.Invalidate();

		return TRUE;
	}
	else
	{
		CDialog::OnEraseBkgnd(pDC);

		CRect rect;
		this->GetClientRect(&rect);
		pDC->FillSolidRect(&rect, RGB(50, 50, 50));

		ShowText(GetDC(), m_strServerVersion, m_rectServer, RGB(255, 255, 255));
		ShowText(GetDC(), m_strClientVersion, m_rectClient, RGB(255, 255, 255));

		return TRUE;
	}
}

void CPatchDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	//禁止显示移动矩形窗体框
	::SystemParametersInfo(SPI_SETDRAGFULLWINDOWS,TRUE,NULL,0);
	//非标题栏移动整个窗口
	m_pParentWnd->SendMessage(WM_SYSCOMMAND,0xF012,0); 

	CDialog::OnLButtonDown(nFlags, point);
}
