﻿// FourBitmapButton.cpp : 实现文件
//

#include "stdafx.h"
#include "FourBitmapButton.h"

//class CGetBitmaps : public CFourBitmapButton
//{
//	CFourBitmapButton * btn;
//public:
//	CGetBitmaps(CFourBitmapButton * button)
//	{
//		btn = button;
//	}
//	inline CBitmap* Nor()
//	{
//		return (CBitmap*)( PCHAR(btn) + (ULONG)(PCHAR (&m_bitmap) - PCHAR(this)) );
//	}
//};

// CFourBitmapButton

IMPLEMENT_DYNAMIC(CFourBitmapButton, CBitmapButton)

CFourBitmapButton::CFourBitmapButton()
{
	m_bMouseOnButton = FALSE;
	m_bButtonDown = FALSE;
	m_bitmapMouse = NULL;
	m_bIsCheckBox = FALSE;
	m_bIsChecked = FALSE;
	m_strDisplayText = "";
	m_crTextColor = RGB(0, 0, 0);
	m_bitmapCheckBoxDisableNormal = NULL;
	m_bitmapCheckBoxDisableChecked = NULL;
	m_bitmapCheckBoxChecked = NULL;
	m_bTransparent = FALSE;
	m_bitmapTransparent = NULL;
	m_bLongClick = FALSE;
}

CFourBitmapButton::~CFourBitmapButton()
{
}


BEGIN_MESSAGE_MAP(CFourBitmapButton, CBitmapButton)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_ERASEBKGND()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_WM_TIMER()
END_MESSAGE_MAP()



// CFourBitmapButton 消息处理程序
void CFourBitmapButton::OnMouseMove(UINT nFlags, CPoint point)
{
	CWnd*				wndUnderMouse = NULL;
	CWnd*				wndActive = this;
	TRACKMOUSEEVENT		csTME;

	if (nFlags & VK_LBUTTON)
	{
		int dx = point.x - m_cpDownLocation.x;
		int dy = point.y - m_cpDownLocation.y;

		DWORD x = MAKEWORD(dx >= 0 ? dx : -dx, dx > 0 ? 1 : 2);
		DWORD y = MAKEWORD(dy >= 0 ? dy : -dy, dy > 0 ? 1 : 2);

		this->GetParent()->SendMessage(FBN_DRAG, m_uiID, MAKELONG(x, y));
	}

	CButton::OnMouseMove(nFlags, point);

	ClientToScreen(&point);
	wndUnderMouse = WindowFromPoint(point);

	if (wndUnderMouse && wndUnderMouse->m_hWnd == m_hWnd && wndActive)
	{
		if (!m_bMouseOnButton)
		{
			m_bMouseOnButton = TRUE;

			Invalidate();

			csTME.cbSize = sizeof(csTME);
			csTME.dwFlags = TME_LEAVE;
			csTME.hwndTrack = m_hWnd;
			::_TrackMouseEvent(&csTME);
		} // if
	} else CancelHover();
} // End of OnMouseMove

void CFourBitmapButton::OnLButtonDown(UINT nFlags, CPoint point)
{	
	m_bLongClick = TRUE;
	SetTimer(1, 300, NULL);
	CBitmapButton::OnLButtonDown(nFlags, point);
	//m_bIsChecked = !m_bIsChecked;
	m_bButtonDown = TRUE;
	m_cpDownLocation.SetPoint(point.x, point.y);
}

void CFourBitmapButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_bLongClick = FALSE;
	if (m_bButtonDown && m_bMouseOnButton)
	{
		m_bIsChecked = !m_bIsChecked;
	}
	m_bButtonDown = FALSE;
	CBitmapButton::OnLButtonUp(nFlags, point);
	this->GetParent()->SendMessage(FBN_LBUTTONUP, m_uiID, MAKELONG(point.x, point.y));
}

// Handler for WM_MOUSELEAVE
LRESULT CFourBitmapButton::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	CancelHover();
	return 0;
} // End of OnMouseLeave

BOOL CFourBitmapButton::OnEraseBkgnd(CDC* pDC)
{
	// 现画父窗口的背景
	if (m_bitmapTransparent != NULL)
	{
		DrawMouseBitmap(pDC, m_bitmapTransparent);
	}

	if (&m_bitmap != NULL)
	{
		if (!m_bTransparent)
		{
			DrawMouseBitmap(pDC, &m_bitmap);
		}
		else
		{
			if (m_bitmapTransparent != NULL)
			{
				DrawMouseBitmap(pDC, m_bitmapTransparent);
			}
			else
			{
				return CBitmapButton::OnEraseBkgnd(pDC);
			}
		}

		return TRUE;
	}
	else
	{
		return CBitmapButton::OnEraseBkgnd(pDC);
	}
}

void CFourBitmapButton::CancelHover()
{
	// Only for flat buttons

	if (m_bMouseOnButton)
	{
		m_bMouseOnButton = FALSE;
		Invalidate();
	} // if

} // End of CancelHover

void CFourBitmapButton::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC*	pDC = CDC::FromHandle(lpDIS->hDC);

	BOOL m_bIsPressed = (lpDIS->itemState & ODS_SELECTED);
	BOOL m_bIsEnable = this->IsWindowEnabled();

	if (m_bIsCheckBox && !m_bIsEnable)
	{
		DrawCheckBoxDisableImage(pDC);
	}
	else
	{
		if (m_bIsCheckBox && m_bIsChecked)
		{
			DrawMouseBitmap(pDC, m_bitmapCheckBoxChecked);
		}
		else
		{
			if(m_bMouseOnButton && !m_bIsPressed && m_bitmapMouse)
			{
				DrawMouseBitmap(pDC, m_bitmapMouse);
			}
			else
			{
				//CBitmapButton::DrawItem(lpDIS);
				if (!m_bIsEnable)
				{
					DrawMouseBitmap(pDC, &m_bitmapDisabled);
				}
				else if (m_bIsPressed)
				{
					DrawMouseBitmap(pDC, &m_bitmapSel);
				}
				else
				{
					if (!m_bTransparent)
					{
						DrawMouseBitmap(pDC, &m_bitmap);
					}
				}
			}
		}
	}

	for (vector<CColorText*>::iterator it = m_setTextList.begin();it != m_setTextList.end();++it)
	{
		(*it)->Draw(pDC);
	}
	/*CRect rect;
	this->GetClientRect(&rect);
	DrawDisplayText(pDC, m_strDisplayText, rect, m_crTextColor);*/
}

BOOL CFourBitmapButton::LoadHoverBitmap(CString strFileName)
{
	BITMAP hBitmap;
	HBITMAP temp = (HBITMAP)LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		m_bitmapMouse = CBitmap::FromHandle(temp);
		m_bitmapMouse->GetBitmap(&hBitmap);

		m_iWidth = hBitmap.bmWidth;
		m_iHeight = hBitmap.bmHeight;

		return TRUE;
	}
	else
	{
		m_bitmapMouse = NULL;
		return FALSE;
	}
}

BOOL CFourBitmapButton::LoadNormalBitmap(CString strFileName)
{
	return LoadButtonBitmap(&m_bitmap, strFileName);
}

BOOL CFourBitmapButton::LoadPressedBitmap(CString strFileName)
{
	return LoadButtonBitmap(&m_bitmapSel, strFileName);
}

BOOL CFourBitmapButton::LoadDisableBitmap(CString strFileName)
{
	return LoadButtonBitmap(&m_bitmapDisabled, strFileName);
}

void CFourBitmapButton::DrawMouseBitmap(CDC* dc, CBitmap* pBitmap)
{
	if (m_bitmapMouse != NULL)
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		CBitmap* pOldBmp = memDC.SelectObject(pBitmap);

		//dc->BitBlt(0, 0, m_iWidth, m_iHeight, &memDC, 0, 0, SRCCOPY);
		dc->TransparentBlt(0, 0, m_iWidth, m_iHeight, &memDC, 0, 0, m_iWidth, m_iHeight, RGB(255, 0, 255));

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();
	}
}

void CFourBitmapButton::DrawCheckBoxDisableImage(CDC* dc)
{
	if (GetCheckBoxValue())
	{
		if (m_bitmapCheckBoxDisableChecked != NULL)
		{
			DrawMouseBitmap(dc, m_bitmapCheckBoxDisableChecked);
		}
		else
		{
			DrawMouseBitmap(dc, &m_bitmapSel);
		}
	}
	else
	{
		if (m_bitmapCheckBoxDisableNormal != NULL)
		{
			DrawMouseBitmap(dc, m_bitmapCheckBoxDisableNormal);
		}
		else if (&m_bitmapDisabled != NULL)
		{
			DrawMouseBitmap(dc, &m_bitmapDisabled);
		}
		else
		{
			DrawMouseBitmap(dc, &m_bitmap);
		}
	}
}

void CFourBitmapButton::DrawDisplayText(CDC* dc, CString strText, CRect rect, COLORREF crColor)
{
	if (strText.GetLength() > 0)
	{
		CFont font;
		font.CreateFont(18, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, GB2312_CHARSET, OUT_STROKE_PRECIS, CLIP_STROKE_PRECIS, DRAFT_QUALITY, VARIABLE_PITCH | FF_MODERN, _T("黑体") );
		CFont* def_font = dc->SelectObject(&font);

		dc->SetBkMode(TRANSPARENT);

		// 制作阴影
		CRect rect1;
		rect1.CopyRect(rect);

		dc->SetTextColor(RGB(GetRValue(crColor)/2, GetGValue(crColor)/2, GetBValue(crColor)/2));
		rect1.OffsetRect(1, 1);
		dc->DrawText(strText, rect1, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		dc->SetTextColor(RGB(0, 0, 0));
		rect1.OffsetRect(1, 1);
		dc->DrawText(strText, rect1, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		// 写字
		dc->SetTextColor(crColor);
		dc->DrawText(strText, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		dc->SelectObject(def_font);
		font.DeleteObject();
	}
}

BOOL CFourBitmapButton::LoadButtonBitmap(CBitmap* pChangeImage, CString strFileName)
{
	CDC srcDC;
	srcDC.CreateCompatibleDC(NULL);

	CDC memDC;
	memDC.CreateCompatibleDC(NULL);

	CBitmap src;

	HBITMAP hbm = (HBITMAP)LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE|LR_CREATEDIBSECTION);
	if (hbm == NULL)
	{
		return FALSE;
	}
	src.Attach(hbm);

	CBitmap* pOldBitmap = srcDC.SelectObject(&src);

	BITMAP bmpinfo;
	src.GetBitmap(&bmpinfo);
	int bmpWidth = bmpinfo.bmWidth;
	int bmpHeight = bmpinfo.bmHeight;

	BOOL Rz = TRUE;

	pChangeImage->DeleteObject();
	pChangeImage->CreateCompatibleBitmap(&srcDC, bmpWidth, bmpHeight);

	memDC.SelectObject(pChangeImage);
	if (!memDC.BitBlt(0, 0, bmpWidth, bmpHeight, &srcDC, 0, 0, SRCCOPY))
	{
		Rz = FALSE;
	}

	srcDC.SelectObject(pOldBitmap);

	srcDC.DeleteDC();
	memDC.DeleteDC();

	src.DeleteObject();

	return Rz;
}

BOOL CFourBitmapButton::LoadButtonBitmap(CBitmap* pChangeImage, CString strFileName, int x, int y, int width, int height)
{
	// 加载图片
	HBITMAP hbm = (HBITMAP)LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE|LR_CREATEDIBSECTION);
	if (hbm == NULL)
	{
		return FALSE;
	}

	// 转化为CBitmap
	CBitmap src;
	src.Attach(hbm);
	
	// 越界判断
	BITMAP bmpinfo;
	src.GetBitmap(&bmpinfo);

	if(x+width > bmpinfo.bmWidth || y+height > bmpinfo.bmHeight)
	{
		src.DeleteObject();
		return FALSE;
	}

	CDC srcDC;
	srcDC.CreateCompatibleDC(NULL);

	CDC memDC;
	memDC.CreateCompatibleDC(NULL);

	CBitmap* pOldBitmap = srcDC.SelectObject(&src);

	
	int bmpWidth = width;
	int bmpHeight = height;

	BOOL Rz = TRUE;

	pChangeImage->DeleteObject();
	pChangeImage->CreateCompatibleBitmap(&srcDC, bmpWidth, bmpHeight);

	memDC.SelectObject(pChangeImage);
	if (!memDC.BitBlt(0, 0, bmpWidth, bmpHeight, &srcDC, x, y, SRCCOPY))
	{
		Rz = FALSE;
	}

	srcDC.SelectObject(pOldBitmap);

	srcDC.DeleteDC();
	memDC.DeleteDC();

	src.DeleteObject();

	return Rz;
}

BOOL CFourBitmapButton::InitButton(UINT nID, CWnd* pParent, CString strNormalImage, CString strHoverImage /* = L */, CString strPressedImage /* = L */, CString strDisableImage /* = L */)
{
	m_uiID = nID;

	if (LoadNormalBitmap(strNormalImage))
	{
		LoadHoverBitmap(strHoverImage);
		LoadPressedBitmap(strPressedImage);
		LoadDisableBitmap(strDisableImage);

		m_bitmapCheckBoxChecked = new CBitmap();
		LoadButtonBitmap(m_bitmapCheckBoxChecked, strPressedImage);

		SubclassDlgItem(nID, pParent);
		SizeToContent();

		return TRUE;
	}
	else
	{
		pParent->GetDlgItem(nID)->ModifyStyle(BS_OWNERDRAW, NULL);
		SubclassDlgItem(nID, pParent);
		return FALSE;
	}
}

BOOL CFourBitmapButton::InitButton(UINT nID, CWnd* pParent, CString strImage, int width, int height, BOOL isCheckBox /* = FALSE */)
{
	m_uiID = nID;

	if (LoadButtonBitmap(&m_bitmap, strImage, 0, 0, width, height))
	{
		m_bitmapMouse = new CBitmap();
		m_bitmapCheckBoxChecked = new CBitmap();

		m_iWidth = width;
		m_iHeight = height;
		m_bIsCheckBox = isCheckBox;

		if (m_bIsCheckBox)
		{
			LoadButtonBitmap(m_bitmapMouse, strImage, 0, 0, width, height);

			LoadButtonBitmap(&m_bitmapSel, strImage, width, 0, width, height);

			LoadButtonBitmap(m_bitmapCheckBoxChecked, strImage, width, 0, width, height);
		}
		else
		{
			LoadButtonBitmap(m_bitmapMouse, strImage, width, 0, width, height);

			LoadButtonBitmap(&m_bitmapSel, strImage, 2*width, 0, width, height);
			
			LoadButtonBitmap(m_bitmapCheckBoxChecked, strImage, 2*width, 0, width, height);

			LoadButtonBitmap(&m_bitmapDisabled, strImage, 3*width, 0, width, height);
		}

		/*LoadHoverBitmap(strHoverImage);
		LoadPressedBitmap(strPressedImage);
		LoadDisableBitmap(strDisableImage);*/

		SubclassDlgItem(nID, pParent);
		SizeToContent();

		return TRUE;
	}
	else
	{
		pParent->GetDlgItem(nID)->ModifyStyle(BS_OWNERDRAW, NULL);
		if (isCheckBox)
		{
			pParent->GetDlgItem(nID)->ModifyStyle(BS_USERBUTTON, BS_AUTOCHECKBOX);
		}
		SubclassDlgItem(nID, pParent);
		return FALSE;
	}
}

BOOL CFourBitmapButton::SetCheckBoxDisableImage(CString strImage, int x, int y, int width, int height)
{
	if (m_bIsCheckBox)
	{
		m_bitmapCheckBoxDisableNormal = new CBitmap();
		if (LoadButtonBitmap(m_bitmapCheckBoxDisableNormal, strImage, x, y, width, height))
		{
			m_bitmapCheckBoxDisableChecked = new CBitmap();
			if (LoadButtonBitmap(m_bitmapCheckBoxDisableChecked, strImage, x+width, y, width, height))
			{
				return TRUE;
			}
			else
			{
				delete m_bitmapCheckBoxDisableChecked;
				m_bitmapCheckBoxDisableChecked = NULL;
				return FALSE;
			}
		}
		else
		{
			delete m_bitmapCheckBoxDisableNormal;
			m_bitmapCheckBoxDisableNormal = NULL;
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
}

BOOL CFourBitmapButton::SetCheckBoxCheckedImage(CString strImage, int x, int y, int width, int height)
{
	return LoadButtonBitmap(m_bitmapCheckBoxChecked, strImage, x, y, width, height);
}

void CFourBitmapButton::SetIsCheckBox(BOOL bIs)
{
	m_bIsCheckBox = bIs;
}

void CFourBitmapButton::SetCheckBoxValue(BOOL bChecked)
{
	//m_bIsChecked = bChecked;
	//this->Invalidate();
	if (GetButtonStyle() == BS_OWNERDRAW)
	{
		if (m_bIsChecked != bChecked)
		{
			m_bIsChecked = bChecked;
			this->Invalidate();
		}
	}
	else
	{
		if (bChecked)
		{
			this->SetCheck(BST_CHECKED);
		}
		else
		{
			this->SetCheck(BST_UNCHECKED);
		}
	}
}

BOOL CFourBitmapButton::GetCheckBoxValue()
{
	if (GetButtonStyle() == BS_OWNERDRAW)
	{
		return m_bIsChecked;
	}
	else
	{
		int value = this->GetCheck();
		return (value == BST_CHECKED ? TRUE : FALSE);

	}

}

void CFourBitmapButton::SetDisplayText(CString text)
{
	m_strDisplayText = text;
}

void CFourBitmapButton::SetDisplayTextColor(COLORREF color)
{
	m_crTextColor = color;
}

void CFourBitmapButton::AddDisplayText(CColorText* text)
{
	// 无资源情况下的处理
	if (m_setTextList.size() == 0)
	{
		this->SetWindowText(text->m_strText);
	}

	CColorText* newText = new CColorText();
	newText->Copy(text);
	m_setTextList.push_back(newText);
}

void CFourBitmapButton::ChangeDisplayText(int index, CString text, COLORREF color)
{
	if (index < (int)m_setTextList.size())
	{
		// 无资源情况下的处理
		if (index == 0)
		{
			this->SetWindowText(text);
		}

		m_setTextList[index]->SetText(text);
		m_setTextList[index]->SetTextColor(color);
	}
}

void CFourBitmapButton::SetTransparent(BOOL b)
{
	m_bTransparent = b;
}

BOOL CFourBitmapButton::SetTransparentImage(CString strImage, int x, int y, int width, int height)
{
	if (m_bitmapTransparent == NULL)
	{
		m_bitmapTransparent = new CBitmap();
	}

	if (!LoadButtonBitmap(m_bitmapTransparent, strImage, x, y, width, height))
	{
		delete m_bitmapTransparent;
		m_bitmapTransparent = NULL;
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}
void CFourBitmapButton::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	//CBitmapButton::OnTimer(nIDEvent);
	switch(nIDEvent)
	{
	case 1:
		KillTimer(1);
		if (m_bLongClick)
		{
			SetTimer(2, 100, NULL);
		}
		break;
	case 2:
		if (m_bLongClick)
		{
			this->GetParent()->SendMessage(FBN_LBLONGCLICK, m_uiID, 0);
		}
		else
		{
			KillTimer(2);
		}
		break;
	default:
		break;
	}

}
