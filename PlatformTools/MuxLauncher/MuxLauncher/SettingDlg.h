﻿#pragma once
#include "afxwin.h"


// CSettingDlg 对话框

class CSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CSettingDlg)

public:
	CSettingDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSettingDlg();
	virtual BOOL OnInitDialog();

private:
	void InitGameConfig();
	void ShowGameConfig(CDC * pDC);
	void VisableGameConfig(UINT visiable);

private:
	void InitAutoUpdate();
	void ShowAutoUpdate(CDC * pDC);
	void VisableAutoUpdate(UINT visiable);
	void EnableAutoUpdate(BOOL bEnable);
private:
	void InitHardwareConfig();
	void GetHardwareConfig();
	void ShowHardwareConfig(CDC* pDC);
public:
	void ShowConfigure();
private:
	void GetConfigure();
private:
	void SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent);
	BOOL PreTranslateMessage(MSG* pMsg);
	void PaintBk(CDC* pDC, CRect rect);
	BOOL MyShowText(CDC* dc, CString strText, CRect rect, COLORREF crColor, int iHeight, int iWeight, CString strFaceName);
	BOOL MyLoadImage(CString strFileName, CBitmap ** pImage, CRect* rect, int iPosX, int iPosY);
	BOOL MyShowImage(CDC* dc, CBitmap* pImage, CRect* rect, COLORREF crTransparent);

private:
	CBitmap*	m_pBkgndImage;
	CString		m_strBkgndImageName;
private:
	CFourBitmapButton	m_ImageButtonOK;
	CFourBitmapButton	m_ImageButtonCancel;
	CFourBitmapButton	m_ImageButtonClose;

	CFourBitmapButton	m_ImageButtonGameConfig;
	CFourBitmapButton	m_ImageButtonAutoUpdate;

	CFourBitmapButton	m_ImageButtonRecommand;

	CFourBitmapButton	m_ImageButtonWindow;
	CFourBitmapButton	m_ImageButtonVS;
	CFourBitmapButton	m_ImageButtonNoSound;

	CImageComboBox m_comboboxResolution;
	CImageComboBox m_comboboxTerrian;
	CImageComboBox m_comboboxEffect;
	CImageComboBox m_comboboxShadow;

	CBitmap*	m_pImageHardware;
	CRect		m_rectHardware;
	
	CBitmap*	m_pImageLine;
	CRect		m_rectLine;

	CBitmap*	m_pImageResolution;
	CRect		m_rectResolution;

	CBitmap*	m_pImageTerrian;
	CRect		m_rectTerrian;

	CBitmap*	m_pImageEffect;
	CRect		m_rectEffect;

	CBitmap*	m_pImageShadow;
	CRect		m_rectShadow;

private:
	CBitmap*	m_pImageAutoUpdateTitle;
	CRect		m_rectAutoUpdateTitle;

	CBitmap*	m_pImageAutoUpdateBK;
	CRect		m_rectAutoUpdateBK;
	
	CBitmap*	m_pImageStandBy;
	CRect		m_rectStandBy;

	CBitmap*	m_pImageCPU;
	CRect		m_rectCPU;

	CBitmap*	m_pImageNetWork;
	CRect		m_rectNetWork;

	CBitmap*	m_pImageShutDown;
	CRect		m_rectShutDown;

	CFourBitmapButton	m_ImageButtonDoAutoUpdate;
	CFourBitmapButton	m_ImageButtonNoAutoUpdate;
	CFourBitmapButton   m_ImageButtonRemind;

	CFourBitmapButton	m_ImageButtonStandBy;
	CFourBitmapButton	m_ImageButtonCPU;
	CFourBitmapButton	m_ImageButtonNetWork;
	CFourBitmapButton	m_ImageButtonShutDown;

private:
	CString		m_strHardwareTitle;
	CRect		m_rectHardwareTitle;
	
	CString		m_strCentralProcessor;
	CRect		m_rectCentralProcessor;
	
	CString		m_strMemory;
	CRect		m_rectMemory;

	CString		m_strVideoCard;
	CRect		m_rectVideoCard;

	CString		m_strOS;
	CRect		m_rectOS;

	// 对话框数据
	enum { IDD = IDD_SETTING_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonSettingOk();
	afx_msg void OnBnClickedButtonSettingCancel();
	afx_msg void OnBnClickedButtonSettingTabConfig();
	afx_msg void OnBnClickedButtonSettingTabUpdate();
	afx_msg void OnClose();
	afx_msg void OnBnClickedButtonSettingDoautoupdate();
	afx_msg void OnBnClickedButtonSettingNoautoupdate();
	afx_msg void OnBnClickedButtonSettingStandby();
	afx_msg void OnBnClickedButtonSettingCpu();
	afx_msg void OnBnClickedButtonSettingNetwork();
	afx_msg void OnBnClickedButtonSettingClose();
	afx_msg void OnBnClickedButtonSettingRecommand();
	int m_iStandByValue;
	afx_msg void OnEnChangeEditSettingStandby();
	int m_iCPUValue;
	int m_iNetWorkValue;
	afx_msg void OnEnChangeEditSettingCpu();
	afx_msg void OnEnChangeEditSettingNetwork();
	CEdit m_editStandBy;
	CEdit m_editCPU;
	CEdit m_editNetWork;
};
