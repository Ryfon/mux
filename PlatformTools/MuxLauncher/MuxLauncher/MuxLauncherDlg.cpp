﻿// MuxLauncherDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "MuxLauncherDlg.h"
#include "ConfigDlg.h"
#include <shlwapi.h>
#include "PackageSourceManager.h"
#include "ConfigManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMuxLauncherDlg 对话框




CMuxLauncherDlg::CMuxLauncherDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMuxLauncherDlg::IDD, pParent)
	, m_bAppProtocol(FALSE)
	, m_staticShowDefaultServer(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_DefFont.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体") );
	m_PatchCliIntf = CreatePatchIntf(this);
}

CMuxLauncherDlg::~CMuxLauncherDlg()
{
	m_PatchCliIntf->CleanUp();
	delete m_PatchCliIntf;
}

void CMuxLauncherDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPLORER_NEWS, m_ExplorerNews);
	DDX_Control(pDX, IDC_EXPLORER_NOTICE, m_ExplorerNotice);
	DDX_Check(pDX, IDC_CHECK_PROTOCOL, m_bAppProtocol);
	DDX_Text(pDX, IDC_TEXT_SHOWDEFAULT, m_staticShowDefaultServer);
	DDX_Control(pDX, IDC_BUTTONCONFIG, m_btnSysConfig);
	DDX_Control(pDX, IDC_BUTTONSTART, m_btnStartGame);
	DDX_Control(pDX, IDC_PROGRESS_DOWNLOAD, m_ProcessCtrlDownLoad);
	DDX_Control(pDX, IDC_PROGRESS_INSTALL, m_ProcessCtrlInstall);
}

BEGIN_MESSAGE_MAP(CMuxLauncherDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTONCONFIG, &CMuxLauncherDlg::OnBnClickedButtonconfig)
	ON_BN_CLICKED(IDC_BUTTONSTART, &CMuxLauncherDlg::OnBnClickedButtonstart)
	ON_BN_CLICKED(IDC_BUTTON_REGISTER, &CMuxLauncherDlg::OnBnClickedButtonRegister)
	ON_BN_CLICKED(IDC_BUTTON_ACCOUNT, &CMuxLauncherDlg::OnBnClickedButtonAccount)
	ON_BN_CLICKED(IDC_BUTTON_ROOKIE, &CMuxLauncherDlg::OnBnClickedButtonRookie)
	ON_BN_CLICKED(IDC_BUTTON_OFFICIAL, &CMuxLauncherDlg::OnBnClickedButtonOfficial)
	ON_BN_CLICKED(IDC_BUTTON_FORUM, &CMuxLauncherDlg::OnBnClickedButtonForum)
	ON_BN_CLICKED(IDC_BUTTON_SUPPORT, &CMuxLauncherDlg::OnBnClickedButtonSupport)
	ON_CONTROL_RANGE(BN_CLICKED, RADIO_ID_START, RADIO_ID_START+MAX_SERVER_NUM, &CMuxLauncherDlg::OnRangeRadioClicked)
	ON_CONTROL_RANGE(BN_CLICKED, BUTTON_ID_START, BUTTON_ID_END, &CMuxLauncherDlg::OnLinkButtonClicked)
	ON_BN_CLICKED(IDC_BUTTON_MANUAL_DOWNLOAD, &CMuxLauncherDlg::OnBnClickedButtonManualDownload)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL, &CMuxLauncherDlg::OnBnClickedButtonProtocol)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CMuxLauncherDlg, CDialog)
	//{{AFX_EVENTSINK_MAP(CMuxLauncherDlg)
	ON_EVENT(CMuxLauncherDlg, IDC_EXPLORER_NEWS, 250, OnBeforeNavigate2Explorer, VTS_DISPATCH VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PBOOL)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CMuxLauncherDlg::OnBeforeNavigate2Explorer(LPDISPATCH pDisp, 
												VARIANT FAR* URL, 
												VARIANT FAR* Flags, 
												VARIANT FAR* TargetFrameName, 
												VARIANT FAR* PostData, 
												VARIANT FAR* Headers, 
												BOOL FAR* Cancel)
{
	try
	{
		int id = 0;

		// 清空旧的数据
		m_WebPropMap.clear();

		// 获取数据流
		if ( ( (PostData->vt) & (VT_VARIANT | VT_BYREF) ) == 0)
		{
			return;
		}
		VARIANT * v = PostData->pvarVal;
		if ( ( (v->vt) & (VT_UI1 | VT_ARRAY) ) == 0)
		{
			return;
		}
		SAFEARRAY * pArr = v->parray;
		CString sData( (LPCSTR)pArr->pvData );

		// 分析数据流
		CStringArray arrPart;
		while (TRUE)
		{
			id = sData.Find('&');
			if (id == -1)
			{
				arrPart.Add(sData);
				break;
			}

			arrPart.Add(sData.Left(id));
			sData = sData.Mid(id + 1);
		}

		// 处理数据流
		for (int nPart = 0;nPart < arrPart.GetSize();nPart++)
		{
			CString sPart = arrPart.GetAt(nPart);
			id = sPart.Find('=');
			ASSERT(id != -1);
			CString sName = sPart.Left(id);
			CString sValue = sPart.Mid(id + 1);

			sName = WebStr2Str(sName);
			sValue = WebStr2Str(sValue);

			m_WebPropMap[sName] = sValue;
		}
	}
	catch (CException*)
	{
		*Cancel = FALSE;
	}
}

// CMuxLauncherDlg 消息处理程序

BOOL CMuxLauncherDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	// 加载UI配置
	bool bUIConfig = ConfigManager::GetInstance()->LoadConfig("MuxLauncherUI.xml");

	// 加载配置文件
	LoadConfigure("UserData/Configuration.xml");

	// 获取服务器列表
	m_iMaxServerNum = 0;
	GetServerList("sysconfig.xml");

	// 动态创建LinkButton
	vector<int>* linkbuttonList = ConfigManager::GetInstance()->GetLinkList(IDD_MUXLAUNCHER_DIALOG);
	if (linkbuttonList != NULL)
	{
		vector<int>::iterator it = linkbuttonList->begin();
		for (;it != linkbuttonList->end();++it)
		{
			int iLinkID = *it;
			int iLeft = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Left") );
			int iTop = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Top") );
			int iWidth = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Width") );
			int iHeight = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Height") );
			const char* czText = ConfigManager::GetInstance()->GetConfig(iLinkID, "Text");
			int iCr = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Cr") );
			int iCg = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Cg") );
			int iCb = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Cb") );
			COLORREF color = RGB(iCr, iCg, iCb);

			CButtonST* newButton = new CButtonST();
			m_LinkList.push_back(newButton);
			newButton->Create(CString(czText), WS_VISIBLE|BS_USERBUTTON|WS_CHILD, CRect(iLeft, iTop, iLeft+iWidth, iTop+iHeight), this, iLinkID);
			newButton->SetFont(&m_DefFont);
			newButton->SetColor(CButtonST::BTNST_COLOR_FG_FOCUS, color);
			newButton->SetColor(CButtonST::BTNST_COLOR_FG_IN, color);
			newButton->SetColor(CButtonST::BTNST_COLOR_FG_OUT, color);
			newButton->DrawTransparent();
		}
	}

	// 创建服务器列表
	for (int i = 0;i < m_iMaxServerNum;++i)
	{
		int mod = i % 2;
		int quo = i / 2;
		int iRadionHeight = 23;
		m_Servers[i].Create(m_ServerName[i], WS_VISIBLE|BS_AUTORADIOBUTTON|WS_CHILD, CRect(20+mod*180, 150+quo*iRadionHeight, 180+mod*180, 150+iRadionHeight+quo*iRadionHeight), this, RADIO_ID_START+i);
		m_Servers[i].SetFont(&m_DefFont);
		//m_Servers[i].SetColor(CButtonST::BTNST_COLOR_FG_FOCUS, RGB(255, 0, 0));
		m_Servers[i].SetColor(CButtonST::BTNST_COLOR_FG_IN, RGB(255, 0, 0));
		//m_Servers[i].SetColor(CButtonST::BTNST_COLOR_FG_OUT, RGB(255, 0, 0));
		m_Servers[i].DrawTransparent();
	}

	// 选定默认服务器
	if (m_iDefaultServer >= m_iMaxServerNum)
	{
		m_iDefaultServer = 0;
	}
	if (m_iMaxServerNum > 0)
	{
		m_Servers[m_iDefaultServer].SetCheck(TRUE);
		//m_Servers[m_iDefaultServer].SetFlat(FALSE);
		m_Servers[m_iDefaultServer].SetColor(CButtonST::BTNST_COLOR_FG_OUT, RGB(255, 0, 0));
	}
	
	m_staticShowDefaultServer = m_ServerName[m_iDefaultServer];
	
	m_bAppProtocol = TRUE; // 默认为同意服务条款

	// 显示浏览器中的网页
	//::ShowScrollBar(m_ExplorerNews.GetSafeHwnd(), SB_BOTH, FALSE);
	//m_ExplorerNews.Navigate2(&CComVariant(_T("http://mux.the9.com")), NULL, NULL, NULL, NULL);
	//m_ExplorerNotice.Navigate2(&CComVariant(_T("http://mux.the9.com")), NULL, NULL, NULL, NULL);
	const char* czNewsURL = ConfigManager::GetInstance()->GetConfig(IDC_EXPLORER_NEWS, "Text");
	if (czNewsURL != NULL)
	{
		m_ExplorerNews.Navigate2(&CComVariant( CString(czNewsURL) ), NULL, NULL, NULL, NULL);
	}
	else
	{
		m_ExplorerNews.Navigate2(&CComVariant(_T("http://mux.the9.com")), NULL, NULL, NULL, NULL);
	}
	const char* czNoticeURL = ConfigManager::GetInstance()->GetConfig(IDC_EXPLORER_NOTICE, "Text");
	if (czNoticeURL != NULL)
	{
		m_ExplorerNotice.Navigate2(&CComVariant( CString(czNoticeURL) ), NULL, NULL, NULL, NULL);
	}
	else
	{
		//m_ExplorerNotice.Navigate2(&CComVariant(_T("http://mux.the9.com")), NULL, NULL, NULL, NULL);
	}

	// 调整一些CButtonST的显示
	m_btnSysConfig.DrawTransparent();
	m_btnStartGame.DrawTransparent();
	m_btnSysConfig.SetFlat(FALSE);
	m_btnStartGame.SetFlat(FALSE);

	// 调整控件位置
	vector<int>* controllist = ConfigManager::GetInstance()->GetControlList();
	if (controllist != NULL)
	{
		vector<int>::iterator it = controllist->begin();
		for (;it != controllist->end();++it)
		{
			int iLinkID = *it;
			int iLeft = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Left") );
			int iTop = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Top") );
			int iWidth = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Width") );
			int iHeight = atoi( ConfigManager::GetInstance()->GetConfig(iLinkID, "Height") );
			GetDlgItem(iLinkID)->MoveWindow(iLeft, iTop, iWidth, iHeight);
		}
	}

	// 如果成功加载配置文件的话，隐藏一些不需要的控件
	if (bUIConfig)
	{
		GetDlgItem(IDC_BUTTON_REGISTER)->ShowWindow(0);
		GetDlgItem(IDC_BUTTON_ACCOUNT)->ShowWindow(0);
		GetDlgItem(IDC_BUTTON_SUPPORT)->ShowWindow(0);
		GetDlgItem(IDC_BUTTON_FORUM)->ShowWindow(0);
		GetDlgItem(IDC_BUTTON_ROOKIE)->ShowWindow(0);
		GetDlgItem(IDC_BUTTON_OFFICIAL)->ShowWindow(0);

		GetDlgItem(IDC_TEXT_HEALTH_NOTICE)->ShowWindow(0);
		GetDlgItem(IDC_TEXT_VERSION_INFO)->ShowWindow(0);
		GetDlgItem(IDC_TEXT_SETUP_PROCESS)->ShowWindow(0);
		GetDlgItem(IDC_TEXT_WALLOW_SYSTEM)->ShowWindow(0);
		GetDlgItem(IDC_TEXT_PK_SYSTEM)->ShowWindow(0);
		
		GetDlgItem(IDC_TEXT_LOCAL_CLIENT_VERSION)->ShowWindow(0);
		GetDlgItem(IDC_TEXT_SERVER_CLIENT_VERSION)->ShowWindow(0);

		GetDlgItem(IDC_BUTTON_PROTOCOL)->ShowWindow(0);
		GetDlgItem(IDC_BUTTON_MANUAL_DOWNLOAD)->ShowWindow(0);

		GetDlgItem(IDC_PANEL_GAME_NOTICE)->SetWindowText(_T(""));
		GetDlgItem(IDC_PANEL_GAME_NEWS)->SetWindowText(_T(""));
		GetDlgItem(IDC_PANEL_FIRST_SERVER)->SetWindowText(_T(""));
		GetDlgItem(IDC_PANEL_SERVER_LIST)->SetWindowText(_T(""));
		GetDlgItem(IDC_PANEL_SELECT_SERVER)->SetWindowText(_T(""));
	}
	
	// 设置进度条
	m_ProcessCtrlDownLoad.SetRange(0, 100);
	m_ProcessCtrlInstall.SetRange(0, 100);

	// 禁止启动游戏
	this->GetDlgItem(IDC_BUTTONSTART)->EnableWindow(FALSE);

	UpdateData(FALSE);

	//m_PatchCliIntf->StartUp();
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMuxLauncherDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMuxLauncherDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();

		CDC* dc = GetDC();

		//ShowImage(dc, _T("tdc.bmp"), 7, 505, RGB(255, 255, 255));
		//ShowImage(dc, _T("muxlogo.bmp"), 140, 504, RGB(0, 0, 0));

		// 显示图片或文字
		vector<TiXmlElement*>* staticlist = ConfigManager::GetInstance()->GetStaticList(IDD_MUXLAUNCHER_DIALOG);
		if (staticlist != NULL)
		{
			vector<TiXmlElement*>::iterator it = staticlist->begin();
			for (;it != staticlist->end();++it)
			{
				// 判断类型
				const char* czType = (*it)->Attribute("Type");
				const char* czText = (*it)->Attribute("Text");
				int iPosX = atoi( (*it)->Attribute("PosX") );
				int iPosY = atoi( (*it)->Attribute("PosY") );
				int iCr = atoi( (*it)->Attribute("Cr") );
				int iCg = atoi( (*it)->Attribute("Cg") );
				int iCb = atoi( (*it)->Attribute("Cb") );
				COLORREF color = RGB(iCr, iCg, iCb);

				if (strcmp(czType, "Image") == 0)
				{
					ShowImage(dc, CString(czText), iPosX, iPosY, color);
				}
				else if (strcmp(czType, "Text") == 0)
				{
					ShowText(dc, CString(czText), iPosX, iPosY, color);
				}
			}
		}

		ReleaseDC(dc);
	}
}

bool CMuxLauncherDlg::ShowText(CDC* dc, CString strTextValue, int iPosX, int iPosY, COLORREF crColor)
{
	// 字体输出
	CFont font;
	font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体") );
	CFont* def_font = dc->SelectObject(&font);
	dc->SetBkMode(TRANSPARENT);
	dc->SetTextColor(crColor);
	dc->TextOut(iPosX, iPosY, strTextValue);

	dc->SelectObject(def_font);
	font.DeleteObject();

	return true;
}
bool CMuxLauncherDlg::ShowImage(CDC* dc, CString strFileName, int iPosX, int iPosY, COLORREF crTransparent)
{
	CDC memDC;
	memDC.CreateCompatibleDC(NULL);

	BITMAP hBitmap;
	HBITMAP temp = (HBITMAP)LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		CBitmap* bmp = CBitmap::FromHandle(temp);
		CBitmap* pOldBmp = memDC.SelectObject(bmp);
		bmp->GetBitmap(&hBitmap);

		TransparentBlt(dc->m_hDC, iPosX, iPosY, hBitmap.bmWidth, hBitmap.bmHeight, memDC.m_hDC, 0, 0, hBitmap.bmWidth, hBitmap.bmHeight, crTransparent);

		bmp->DeleteObject();
		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();
		return true;
	}
	else
	{
		memDC.DeleteDC();
		return false;
	}
}

CString CMuxLauncherDlg::WebStr2Str(LPCTSTR lpBuf)
{
	int nLen;
	if (!lpBuf)
	{
		nLen = 0;
	}
	else
	{
		nLen = ::lstrlen(lpBuf);
	}

	CString s;
	int i = 0;

	while (i < nLen)
	{
		if (lpBuf[i] == '%')
		{
			TCHAR c1 = lpBuf[i+1];
			TCHAR c2 = lpBuf[i+2];
			i+=2;

			if (c1 >= '0' && c1 <= '9')
			{
				c1 = (c1 - '0') * 16;
			}
			else if (c1 >= 'A' && c1 <= 'Z')
			{
				c1 = (c1 - 'A' + 10) * 16;
			}
			else if (c1 >= 'a' && c1 <= 'z')
			{
				c1 = (c1 - 'a' + 10) * 16;
			}

			if (c2 >= '0' && c2 <= '9')
			{
				c2 = (c2 - '0') * 16;
			}
			else if (c2 >= 'A' && c2 <= 'Z')
			{
				c2 = (c2 - 'A' + 10) * 16;
			}
			else if (c2 >= 'a' && c2 <= 'z')
			{
				c2 = (c2 - 'a' + 10) * 16;
			}

			TCHAR szStr[2];
			szStr[0] = c1 + c2;
			szStr[1] = 0;

			s += szStr;
		}
		else if (lpBuf[i] == '+')
		{
			s += " ";
		}
		else
		{
			s += CString(&lpBuf[i], 1);
		}
		i++;
	}

	return s;
}


void CMuxLauncherDlg::GetServerList(const char * czFileName)
{
	// 尝试读取文件
	int iIndex = 0;
	bool bReadSucceed = false;
	
	// 获得完整的路径名
	TCHAR tcCurrDir[MAX_PATH];
	::GetCurrentDirectory(MAX_PATH, tcCurrDir);
	//CString csCurrDir(tcCurrDir);
	USES_CONVERSION;
	//string strCurrDir(T2A(csCurrDir));

	char fullName[1024] = "\0";
	strcat_s(fullName, T2A(tcCurrDir));
	strcat_s(fullName, "\\Data\\Config\\");
	strcat_s(fullName, czFileName);

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	if ( pXmlDoc->LoadFile(fullName) )	// 首先尝试直接读取
	{
		bReadSucceed = true;
	}
	else
	{
		// 尝试从pkg中读取
		//CPackage pkg;
		//if (pkg.OpenPackage("Data\\Config.pkg"))
		//{
		//	CPackageFileReader* pFileReader = pkg.GetPackageFileReader(czFileName);// 读取文件
		//	if (pFileReader != NULL)
		//	{
		//		SPackageFileInfo* pFileInfo = pFileReader->GetPackageFileInfo();	// 获取文件信息
		//		if (pFileInfo != NULL)
		//		{
		//			char* pBuf = new char[pFileInfo->nFileOrgSize];
		//			UINT nRet = pFileReader->ReadBuffer((unsigned char*)pBuf);
		//			if (nRet == pFileInfo->nFileOrgSize)// 读取大小一致
		//			{
		//				if (pXmlDoc->Parse(pBuf) != NULL)
		//				{
		//					bReadSucceed = true;
		//				}
		//			}
		//			delete [] pBuf;
		//		}		
		//	}

		//	// 关闭包
		//	pkg.ClosePackage();
		//}
		char* pBuf = CPackageSourceManager::GetInstance()->GetFileData(fullName);
		CPackageSourceManager::GetInstance()->Release();
		if (pBuf != NULL)
		{
			if (pXmlDoc->Parse(pBuf) != NULL)
								{
									bReadSucceed = true;
								}
			delete [] pBuf;
			pBuf = NULL;
		}
	}

	// 成功的话开始读取文件
	if (bReadSucceed)
	{
		TiXmlElement* pXmlRoot = pXmlDoc->RootElement(); // 获取根节点
		if (pXmlRoot != NULL)
		{
			TiXmlElement* pXmlLine = pXmlRoot;
			for (;pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
			{
				if (strcmp(pXmlLine->Value(), "Servers") == 0)	// 系统配置
				{
					for (TiXmlElement* pXmlConfigure = pXmlLine->FirstChildElement();pXmlConfigure;pXmlConfigure = pXmlConfigure->NextSiblingElement())
					{
						if (iIndex < MAX_SERVER_NUM)
						{
							// 如果有Name字段的话，显示名称
							const char* czName = pXmlConfigure->Attribute("Name");
							if (czName != NULL && strlen(czName) != 0)
							{
								m_ServerName[iIndex] += czName;
							}
							else
							{
								m_ServerName[iIndex] += pXmlConfigure->Attribute("IP");
								m_ServerName[iIndex] += " : ";
								m_ServerName[iIndex] += pXmlConfigure->Attribute("Port");
							}
							
							iIndex++;
						}
					}

					m_iMaxServerNum = iIndex;
				}
			}
		}
	}

	// 清除数据
	if (pXmlDoc != NULL)
	{
		pXmlDoc->Clear();
		delete pXmlDoc;
		pXmlDoc = NULL;
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CMuxLauncherDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMuxLauncherDlg::OnBnClickedButtonconfig()
{
	// TODO: 在此添加控件通知处理程序代码
	CConfigDlg dlg;
	dlg.DoModal();
}

void CMuxLauncherDlg::OnBnClickedButtonstart()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	if (!m_bAppProtocol)
	{
		AfxMessageBox(_T("您还没有同意MUX服务条款协议"));

		//CString strSelectServer = m_WebPropMap[CString("1111")];
		//if (strSelectServer.GetLength() > 0)
		//{
		//	AfxMessageBox(strSelectServer);
		//}	
		
		return;
	}

	CString strClientFileName("MuxS.dat");
	STARTUPINFO start;
	PROCESS_INFORMATION process;
	memset(&start, 0, sizeof(STARTUPINFO));
	start.cb = sizeof(start);
	//start.wShowWindow = SW_SHOW;
	//start.dwFlags = STARTF_USESHOWWINDOW;

	/*if (ShellExecute(NULL, _T("open"), strClientFileName, NULL, NULL, SW_SHOWNORMAL) > HINSTANCE(32))
	{
		SaveConfigure("UserData/Configuration.xml");
		PostQuitMessage(0);
	}*/
	TCHAR szFileName[MAX_PATH] = L"Muxs.dat";
	if (CreateProcess(NULL, szFileName, NULL, NULL, FALSE, HIGH_PRIORITY_CLASS, NULL, NULL, &start, &process))
	{
		SaveConfigure("UserData/Configuration.xml");
		PostQuitMessage(0);
	}
	else
	{
		strClientFileName = "MuxS.exe";
		if (ShellExecute(NULL, _T("open"), strClientFileName, NULL, NULL, SW_SHOWNORMAL) > HINSTANCE(32))
		{
			SaveConfigure("UserData/Configuration.xml");
			PostQuitMessage(0);
		}
		else
		{
			AfxMessageBox(_T("启动游戏失败"));
		}
	}
}

void CMuxLauncherDlg::LoadConfigure(const char * czFileName)
{
	// 先加载默认配置
	m_iDefaultServer = 0;
	for (int iIndex = 0;iIndex < CT_NUMBER;++iIndex)
	{
		m_Configure[iIndex] = aSystemConfigDefaultValue[iIndex];
	}

	// 尝试读取文件
	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	if (pXmlDoc->LoadFile(czFileName))
	{
		TiXmlElement* pXmlRoot = pXmlDoc->RootElement(); // 获取根节点
		if (pXmlRoot != NULL)
		{
			TiXmlElement* pXmlLine = pXmlRoot->FirstChildElement();
			for (;pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
			{
				if (strcmp(pXmlLine->Value(), "SystemConfig") == 0)	// 系统配置
				{
					for (TiXmlElement* pXmlConfigure = pXmlLine->FirstChildElement();pXmlConfigure;pXmlConfigure = pXmlConfigure->NextSiblingElement())
					{
						int index = atoi(pXmlConfigure->Attribute("index"));
						int value = atoi(pXmlConfigure->Attribute("value"));

						if (index < CT_NUMBER)
						{
							m_Configure[index] = value;
						}
					}

				}
				else if (strcmp(pXmlLine->Value(), "DefaultServer") == 0)	// 默认服务器
				{
					m_iDefaultServer = atoi(pXmlLine->Attribute("Index"));
				}
			}
		}
	}
	else
	{
		// 配置文件不存在或有问题，直接创建
		SaveConfigure(czFileName);
	}

	// 清除数据
	if (pXmlDoc != NULL)
	{
		pXmlDoc->Clear();
		delete pXmlDoc;
		pXmlDoc = NULL;
	}
}


void CMuxLauncherDlg::SaveConfigure(const char * czFileName)
{
	// 文件头
	TiXmlDocument* m_XmlDoc = new TiXmlDocument();
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	m_XmlDoc->InsertEndChild(*pXmlDec);
	TiXmlElement * pXmlRoot = new TiXmlElement("Root");
	m_XmlDoc->LinkEndChild(pXmlRoot);

	// 系统配置
	TiXmlElement* pXmlSystemConfig = new TiXmlElement("SystemConfig");
	pXmlRoot->LinkEndChild(pXmlSystemConfig);
	for (int iIndex = 0;iIndex < CT_NUMBER;++iIndex)
	{
		TiXmlElement* pXmlConfig = new TiXmlElement("Config");
		pXmlConfig->SetAttribute("index", iIndex);
		pXmlConfig->SetAttribute("value", m_Configure[iIndex]);
		pXmlSystemConfig->LinkEndChild(pXmlConfig);
	}

	// 默认服务器
	TiXmlElement* pXmlDefaultServer = new TiXmlElement("DefaultServer");
	pXmlDefaultServer->SetAttribute("Index", m_iDefaultServer);
	pXmlRoot->LinkEndChild(pXmlDefaultServer);

	// 判断UserData文件夹是否存在
	if (!PathIsDirectory(_T("UserData")))
	{
		CreateDirectory(_T("UserData"), NULL);
	}

	// 写入文件
	m_XmlDoc->SaveFile(czFileName);

	// 清空数据
	if (m_XmlDoc != NULL)
	{
		m_XmlDoc->Clear();
		delete m_XmlDoc;
		m_XmlDoc = NULL;
	}
}


void CMuxLauncherDlg::OnBnClickedButtonRegister()
{
	// TODO: 在此添加控件通知处理程序代码
	ShellExecute(NULL, _T("open"), _T("http://mux.the9.com"), NULL, NULL, SW_SHOWNORMAL);
}

void CMuxLauncherDlg::OnBnClickedButtonAccount()
{
	// TODO: 在此添加控件通知处理程序代码
	ShellExecute(NULL, _T("open"), _T("http://mux.the9.com"), NULL, NULL, SW_SHOWNORMAL);
}

void CMuxLauncherDlg::OnBnClickedButtonRookie()
{
	// TODO: 在此添加控件通知处理程序代码
	ShellExecute(NULL, _T("open"), _T("http://mux.the9.com"), NULL, NULL, SW_SHOWNORMAL);
}

void CMuxLauncherDlg::OnBnClickedButtonOfficial()
{
	// TODO: 在此添加控件通知处理程序代码
	ShellExecute(NULL, _T("open"), _T("http://mux.the9.com"), NULL, NULL, SW_SHOWNORMAL);
}

void CMuxLauncherDlg::OnBnClickedButtonForum()
{
	// TODO: 在此添加控件通知处理程序代码
	ShellExecute(NULL, _T("open"), _T("http://mux.the9.com"), NULL, NULL, SW_SHOWNORMAL);
}

void CMuxLauncherDlg::OnBnClickedButtonSupport()
{
	// TODO: 在此添加控件通知处理程序代码
	ShellExecute(NULL, _T("open"), _T("http://mux.the9.com"), NULL, NULL, SW_SHOWNORMAL);
}

void CMuxLauncherDlg::OnRangeRadioClicked(UINT uID)
{
	m_iDefaultServer = uID - RADIO_ID_START;
	m_staticShowDefaultServer = m_ServerName[m_iDefaultServer];
	
	for (int i = 0;i < m_iMaxServerNum;++i)
	{
		if (i != m_iDefaultServer)
		{
			//m_Servers[i].SetFlat(TRUE);
			m_Servers[i].SetColor(CButtonST::BTNST_COLOR_FG_OUT, RGB(0, 0, 0));
		}
	}

	//m_Servers[m_iDefaultServer].SetFlat(FALSE);
	m_Servers[m_iDefaultServer].SetColor(CButtonST::BTNST_COLOR_FG_OUT, RGB(255, 0, 0));

	UpdateData(FALSE);
}

void CMuxLauncherDlg::OnLinkButtonClicked(UINT uID)
{
	const char* czUrl = ConfigManager::GetInstance()->GetConfig(uID, "Url");
	if (czUrl != NULL)
	{
		ShellExecute(NULL, _T("open"), CString(czUrl), NULL, NULL, SW_SHOWNORMAL);
	}
}

void CMuxLauncherDlg::OnBnClickedButtonManualDownload()
{
	// TODO: 在此添加控件通知处理程序代码
	ShellExecute(NULL, _T("open"), _T("http://mux.the9.com"), NULL, NULL, SW_SHOWNORMAL);
}

void CMuxLauncherDlg::OnBnClickedButtonProtocol()
{
	// TODO: 在此添加控件通知处理程序代码
	ShellExecute(NULL, _T("open"), _T("http://mux.the9.com"), NULL, NULL, SW_SHOWNORMAL);
}

BOOL CMuxLauncherDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	// 显示背景图片
	const char* czBGImage = ConfigManager::GetInstance()->GetConfig(IDD_MUXLAUNCHER_DIALOG, "BGImage");
	CString strBGImageName(czBGImage);

	if (czBGImage != NULL && ShowImage(pDC, strBGImageName, 0, 0, RGB(0, 0, 0)))
	{
		return TRUE;
	}
	else
	{
		return CDialog::OnEraseBkgnd(pDC);
	}	
}

int CMuxLauncherDlg::OnCliVersion(PATCHVERSION &version)
{
	WCHAR szBuf[128];
	wsprintf(szBuf, _T("本地客户端版本号：%d.%u.%d"), version.MajorVersion, version.MinVersion, version.AdditionVersion);
	GetDlgItem(IDC_TEXT_LOCAL_CLIENT_VERSION)->SetWindowText(szBuf);
	//MessageBox(szBuf);
	return 0;
}

int CMuxLauncherDlg::OnServerVersion(PATCHVERSION& version, unsigned char ucPatchNum)
{
	WCHAR szBuf[128];
	wsprintf(szBuf, L"服务器客户端版本号：%d.%u.%d", version.MajorVersion, version.MinVersion, version.AdditionVersion);
	GetDlgItem(IDC_TEXT_SERVER_CLIENT_VERSION)->SetWindowText(szBuf);

	m_iPatchNum = ucPatchNum;
	wsprintf(szBuf, L"(%d/%d)", 0, m_iPatchNum);
	GetDlgItem(IDC_TEXT_DOWNLOAD_PROCESS)->SetWindowText(szBuf);

	if (ucPatchNum)
	{
		m_PatchCliIntf->CheckVersion();
	}
	else
	{
		this->GetDlgItem(IDC_BUTTONSTART)->EnableWindow(TRUE);
	}

	return 0;
}

int CMuxLauncherDlg::OnDownloadProgress(PATCHVERSION& version, int nIndex, int nPos, int nDownloadSpeed, int nUploadSpeed, float fFileSize)
{
	//nFileSize 文件
	m_ProcessCtrlDownLoad.SetPos(nPos);

	WCHAR szBuf[128];
	wsprintf(szBuf, L"(%d/%d)", nIndex, m_iPatchNum);
	GetDlgItem(IDC_TEXT_DOWNLOAD_PROCESS)->SetWindowText(szBuf);

	return 0;
}

int CMuxLauncherDlg::OnInstallProgress(PATCHVERSION& version, int nStatus, int nPos)
{
	m_ProcessCtrlInstall.SetPos(nPos);

	WCHAR szBuf[128];
	wsprintf(szBuf, _T("本地客户端版本号：%d.%u.%d"), version.MajorVersion, version.MinVersion, version.AdditionVersion);
	GetDlgItem(IDC_TEXT_LOCAL_CLIENT_VERSION)->SetWindowText(szBuf);

	if (nPos == 100)
	{
		this->GetDlgItem(IDC_BUTTONSTART)->EnableWindow(TRUE);
	}
	return 0;
}

int CMuxLauncherDlg::OnError(int nErrorCode)
{
	CString strErrorMsg;

	switch(nErrorCode)
	{
	case MUXPATCH_DOWNLOAD_SERVERNOTFOUND:
		strErrorMsg = L"更新服务器不可用";
		break;
	case MUXPATCH_DOWNLOAD_NETBREAKDOWN:
		strErrorMsg = L"网络连接已断开";
		break;
	default:
		return 0;
		break;
	}

	MessageBox(strErrorMsg);
	return 0;
}