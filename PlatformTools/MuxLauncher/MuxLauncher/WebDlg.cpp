﻿// WebDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "WebDlg.h"


// CWebDlg 对话框

IMPLEMENT_DYNCREATE(CWebDlg, CDHtmlDialog)

CWebDlg::CWebDlg(CWnd* pParent /*=NULL*/)
	: CDHtmlDialog(CWebDlg::IDD, CWebDlg::IDH, pParent)
{

}

CWebDlg::~CWebDlg()
{
}

void CWebDlg::DoDataExchange(CDataExchange* pDX)
{
	CDHtmlDialog::DoDataExchange(pDX);
}

BOOL CWebDlg::OnInitDialog()
{
	// 获取配置
	CDialogConfig* pDialogConfig = ConfigManager::GetInstance()->GetDialogConfig(_T("WebDlg"));
	TiXmlElement* pXmlDialogConfig = pDialogConfig->GetConfig();

	// 初始化设置
	CDHtmlDialog::OnInitDialog();
	this->MoveWindow(pXmlDialogConfig->IntAttribute("Left"), pXmlDialogConfig->IntAttribute("Top"), pXmlDialogConfig->IntAttribute("Width"), pXmlDialogConfig->IntAttribute("Height"));
	
	// 本地文件和URL
	if(pXmlDialogConfig->IntAttribute("ShapeName") == 1)
	{
		TCHAR tcCurrDir[MAX_PATH];
		::GetCurrentDirectory(MAX_PATH, tcCurrDir);
		CString strUrl(tcCurrDir+CString("\\"));
		this->Navigate(strUrl + CString(pXmlDialogConfig->Attribute("BGImage")));

	}
	else
	{
		this->Navigate(CString(pXmlDialogConfig->Attribute("BGImage")));
	}
	
	/*m_pPatchDlg = new CPatchDlg(this);
	m_pPatchDlg->Create(IDD_PATCH_DIALOG, this);
	m_pPatchDlg->ShowWindow(SW_SHOW);*/

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

BOOL CWebDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
			return TRUE;
		case VK_ESCAPE:
			return TRUE;
		case VK_F5:
			return TRUE;
		case VK_BACK:
			return TRUE;
		}
	}

	if (pMsg->message == WM_LBUTTONDBLCLK || pMsg->message == WM_RBUTTONDOWN || pMsg->message == WM_RBUTTONDBLCLK)
	{
		return TRUE;
	}

	return CDHtmlDialog::PreTranslateMessage(pMsg);
}

void CWebDlg::OnNavigateComplete(LPDISPATCH pDisp, LPCTSTR szUrl)
{
	CDHtmlDialog::OnNavigateComplete(pDisp, szUrl);

	// 获取真实路径
	IHTMLDocument2 *pphtmlDoc;
	HRESULT hr = GetDHtmlDocument(&pphtmlDoc);
	BSTR pUrl;
	pphtmlDoc->get_URL(&pUrl);

	// 获取相对路径
	CString strUrl;
	GetCurrentUrl(strUrl);

	// 比较
	if (strUrl.Compare(pUrl) != 0)
	{
		strUrl = L"file://" + strUrl;
		if (strUrl.Compare(pUrl) != 0)
		{
			this->ShowWindow(SW_HIDE);
		}
		else
		{
			this->ShowWindow(SW_SHOW);
		}
	}
	else
	{
		this->ShowWindow(SW_SHOW);
	}
}

BEGIN_MESSAGE_MAP(CWebDlg, CDHtmlDialog)
//	ON_WM_PAINT()
//	ON_WM_ERASEBKGND()
//ON_WM_ERASEBKGND()
//ON_WM_NCPAINT()
//ON_WM_UPDATEUISTATE()
END_MESSAGE_MAP()

BEGIN_DHTML_EVENT_MAP(CWebDlg)
	DHTML_EVENT_ONCLICK(_T("ButtonOK"), OnButtonOK)
	DHTML_EVENT_ONCLICK(_T("ButtonCancel"), OnButtonCancel)
END_DHTML_EVENT_MAP()



// CWebDlg 消息处理程序

HRESULT CWebDlg::OnButtonOK(IHTMLElement* /*pElement*/)
{
	OnOK();
	return S_OK;
}

HRESULT CWebDlg::OnButtonCancel(IHTMLElement* /*pElement*/)
{
	OnCancel();
	return S_OK;
}

