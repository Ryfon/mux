﻿#pragma once
#include "afxwin.h"



// CSystemConfigDlg 对话框

class CSystemConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CSystemConfigDlg)

public:
	CSystemConfigDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSystemConfigDlg();

// 对话框数据
	enum { IDD = IDD_DIALOGSYSTEMCONFIG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	
	
	DECLARE_MESSAGE_MAP()
	
};
