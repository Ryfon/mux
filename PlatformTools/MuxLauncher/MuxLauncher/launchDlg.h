﻿// launchDlg.h : 头文件
//

#pragma once
#include "PatchDlg.h"
#include "WebDlg.h"
#include "ServerDlg.h"

// ClaunchDlg 对话框
class ClaunchDlg : public CDialog
{
// 构造
public:
	ClaunchDlg(CWnd* pParent = NULL);	// 标准构造函数
	virtual ~ClaunchDlg();

// 对话框数据
	enum { IDD = IDD_LAUNCH_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
	void SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent);
	BOOL PreTranslateMessage(MSG* pMsg);

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	void PaintBk(CDC* pDC, CRect rect);
	void ShowText(CDC* dc, CString strTextValue, CRect rect, COLORREF crColor);
private:
	CBitmap*	m_pBkgndImage;
	CString		m_strBkgndImageName;
	CPatchDlg * m_pPatchDlg;
	CWebDlg*	m_pWebDlg;
	CServerDlg* m_pServerDlg;
	CFourBitmapButton m_ImageButtonClose;
	CFourBitmapButton m_ImageButtonMin;

	CRect		m_rectLoadRes;
	CString		m_strLoadRes;
	COLORREF	m_crLoadRes;
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnBnClickedButtonMin();
};
