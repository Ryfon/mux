﻿#include "StdAfx.h"
#include "resource.h"
#include "ConfigManager.h"
#include <ddraw.h>
#include <d3d9.h>
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "d3d9.lib")

ConfigManager* ConfigManager::m_Instance = NULL;

ConfigManager::ConfigManager(void)
{
	m_pXmlDoc = new TiXmlDocument();
	m_pXmlDocGameConfigure = NULL;
	m_PatchCliIntf = CreatePatchIntf(NULL);
	m_PatchCliIntf->GetServerInfo();
}

ConfigManager::~ConfigManager(void)
{
	m_pXmlDoc->Clear();
	delete m_pXmlDoc;
	m_pXmlDoc = NULL;

	DeleteGameConfigure();

	delete m_PatchCliIntf;

	if (m_Instance != NULL)
	{
		delete m_Instance;
		m_Instance = NULL;
	}
}

bool ConfigManager::LoadConfig(const char* czFileName)
{
	// 清空旧的数据
	m_ConfigMap.clear();
	m_StaticMap.clear();
	m_LinkMap.clear();
	m_ControlList.clear();
	if (m_pXmlDoc != NULL)
	{
		m_pXmlDoc->Clear();
	}
	else
	{
		m_pXmlDoc = new TiXmlDocument();
	}

	// 尝试读取文件
	if (m_pXmlDoc->LoadFile(czFileName))
	{
		TiXmlElement* pXmlRoot = m_pXmlDoc->RootElement(); // 获取根节点
		if (pXmlRoot != NULL)
		{
			// 根节点为MainForm,ID为key，节点本身为value，放入map中
			int iMainFormID = -1;
			if (pXmlRoot->Attribute("ID") != NULL)
			{
				iMainFormID = atoi(pXmlRoot->Attribute("ID"));
				m_ConfigMap[iMainFormID] = pXmlRoot;
			}

			// 遍历子节点，将config放入map中
			TiXmlElement* pXmlConfig = pXmlRoot->FirstChildElement();
			for (;pXmlConfig;pXmlConfig = pXmlConfig->NextSiblingElement())
			{
				// 有ID的子节点，都要放入总表中
				int iConfigID = -1;
				if (pXmlConfig->Attribute("ID") != NULL)
				{
					iConfigID = atoi(pXmlConfig->Attribute("ID"));
					m_ConfigMap[iConfigID] = pXmlConfig;
					m_ControlList.push_back(iConfigID);
				}

				if (strcmp(pXmlConfig->Value(), "Static") == 0)	// 固定节点，图片或文字
				{
					// 获取容器
					vector<TiXmlElement*>* father = m_StaticMap[iMainFormID];
					if (father == NULL)
					{
						father = new vector<TiXmlElement*>;
						m_StaticMap[iMainFormID] = father;
					}

					// 填充数据
					father->push_back(pXmlConfig);
				}
				else if (strcmp(pXmlConfig->Value(), "Link") == 0)	// link节点，弹出网页
				{
					if (iConfigID != -1)
					{
						// 获取容器
						vector<int>* father = m_LinkMap[iMainFormID];
						if (father == NULL)
						{
							father = new vector<int>;
							m_LinkMap[iMainFormID] = father;
						}

						// 填充数据
						father->push_back(iConfigID);
					}
				}

				
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	// 正确返回
	return true;
}

const char* ConfigManager::GetConfig(int iID, const char* czAttrName)
{
	// 获取节点
	TiXmlElement* pXmlConfig = m_ConfigMap[iID];
	if (pXmlConfig == NULL)
	{
		return NULL;
	}
	else
	{
		return pXmlConfig->Attribute(czAttrName);
	}
}

vector<TiXmlElement*>* ConfigManager::GetStaticList(int iID)
{
	return m_StaticMap[iID];
}

vector<int>* ConfigManager::GetLinkList(int iID)
{
	return m_LinkMap[iID];
}

vector<int>* ConfigManager::GetControlList()
{
	return &m_ControlList;
}

void ConfigManager::LoadGameConfigure(const char * czFileName)
{
	// 先加载默认配置
	m_iDefaultServer = 0;
	for (int iIndex = 0;iIndex < CT_NUMBER;++iIndex)
	{
		m_Configure[iIndex] = aSystemConfigDefaultValue[iIndex];
	}

	// 预下载的默认配置
	g_iEnableAutoUpdate = 1;
	g_iEnableStandBy = 1;
	g_iStandByValue = 20;
	g_iEnableCPU = 1;
	g_iCPUValue = 20;
	g_iEnableNetWork = 1;
	g_iNetWorkValue = 20;
	g_iEnableShutDown = 1;
	g_iEnableUserRemind = 1;

	// 清除旧的数据
	DeleteGameConfigure();

	// 尝试读取文件
	m_pXmlDocGameConfigure = new TiXmlDocument();
	if (m_pXmlDocGameConfigure->LoadFile(czFileName))
	{
		TiXmlElement* pXmlRoot = m_pXmlDocGameConfigure->RootElement(); // 获取根节点
		if (pXmlRoot != NULL)
		{
			TiXmlElement* pXmlLine = pXmlRoot->FirstChildElement();
			for (;pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
			{
				if (strcmp(pXmlLine->Value(), "SystemConfig") == 0)	// 系统配置
				{
					for (TiXmlElement* pXmlConfigure = pXmlLine->FirstChildElement();pXmlConfigure;pXmlConfigure = pXmlConfigure->NextSiblingElement())
					{
						int index = atoi(pXmlConfigure->Attribute("index"));
						int value = atoi(pXmlConfigure->Attribute("value"));

						if (index < CT_NUMBER)
						{
							m_Configure[index] = value;
						}
					}

				}
				else if (strcmp(pXmlLine->Value(), "DefaultServer") == 0)	// 默认服务器
				{
					m_iDefaultServer = atoi(pXmlLine->Attribute("Index"));
					g_uiCatID = pXmlLine->IntAttribute("CatID");
					g_uiAreaID = pXmlLine->IntAttribute("AreaID");
					g_uiServerID = pXmlLine->IntAttribute("ServerID");
				}
				else if(strcmp(pXmlLine->Value(), "AutoUpdate") == 0)
				{
					// 获取数据
					g_iEnableAutoUpdate = atoi(pXmlLine->Attribute("EnableAutoUpdate"));
					g_iEnableStandBy = atoi(pXmlLine->Attribute("EnableStandBy"));
					g_iStandByValue = atoi(pXmlLine->Attribute("StandByValue"));
					g_iEnableCPU = atoi(pXmlLine->Attribute("EnableCPU"));
					g_iCPUValue = atoi(pXmlLine->Attribute("CPUValue"));
					g_iEnableNetWork = atoi(pXmlLine->Attribute("EnableNetWork"));
					g_iNetWorkValue = atoi(pXmlLine->Attribute("NetWorkValue"));
					g_iEnableShutDown = atoi(pXmlLine->Attribute("EnableShutDown"));
					g_iEnableUserRemind = atoi(pXmlLine->Attribute("EnableUserRemind"));

					// 范围检测
					if(g_iStandByValue < 10 || g_iStandByValue > 60)
					{
						g_iStandByValue = 20;
					}
					if(g_iCPUValue < 10 || g_iCPUValue > 80)
					{
						g_iCPUValue = 20;
					}
					if(g_iNetWorkValue < 10 || g_iNetWorkValue > 80)
					{
						g_iNetWorkValue = 20;
					}
				}
			}
		}
	}
	else
	{
		// 配置文件不存在或有问题，直接创建
		CreateGameConfigure(czFileName);
	}

	// 清除数据
	/*if (pXmlDoc != NULL)
	{
		pXmlDoc->Clear();
		delete pXmlDoc;
		pXmlDoc = NULL;
	}*/
}


void ConfigManager::SaveGameConfigure(const char * czFileName)
{
	// 如果文件不存在则创建默认的文件
	if(m_pXmlDocGameConfigure == NULL)
	{
		CreateGameConfigure(czFileName);
	}

	// 获取根节点
	TiXmlElement * pXmlRoot = m_pXmlDocGameConfigure->RootElement();
	if(pXmlRoot != NULL)
	{
		// 系统配置	
		TiXmlElement* pXmlSystemConfig = pXmlRoot->FirstChildElement("SystemConfig");
		if(pXmlSystemConfig != NULL)
		{
			TiXmlElement* pXmlConfig = pXmlSystemConfig->FirstChildElement();
			for (int iIndex = 0;iIndex < CT_NUMBER && pXmlConfig;pXmlConfig = pXmlConfig->NextSiblingElement())
			{
				pXmlConfig->SetAttribute("index", iIndex);
				pXmlConfig->SetAttribute("value", m_Configure[iIndex]);
				++iIndex;
			}
		}

		// 默认服务器
		TiXmlElement* pXmlDefaultServer = pXmlRoot->FirstChildElement("DefaultServer");
		if(pXmlDefaultServer != NULL)
		{
			pXmlDefaultServer->SetAttribute("Index", m_iDefaultServer);
			pXmlDefaultServer->SetAttribute("CatID", g_uiCatID);
			pXmlDefaultServer->SetAttribute("AreaID", g_uiAreaID);
			pXmlDefaultServer->SetAttribute("ServerID", g_uiServerID);
			pXmlDefaultServer->SetAttribute("IP", g_szServerIp);
			pXmlDefaultServer->SetAttribute("Port", g_ServerPort);
		}


		// 预下载选项
		TiXmlElement* pXmlAutoUpdate = pXmlRoot->FirstChildElement("AutoUpdate");
		if(pXmlAutoUpdate != NULL)
		{
			pXmlAutoUpdate->SetAttribute("EnableAutoUpdate", g_iEnableAutoUpdate);
			pXmlAutoUpdate->SetAttribute("EnableStandBy", g_iEnableStandBy);
			pXmlAutoUpdate->SetAttribute("StandByValue", g_iStandByValue);
			pXmlAutoUpdate->SetAttribute("EnableCPU", g_iEnableCPU);
			pXmlAutoUpdate->SetAttribute("CPUValue", g_iCPUValue);
			pXmlAutoUpdate->SetAttribute("EnableNetWork", g_iEnableNetWork);
			pXmlAutoUpdate->SetAttribute("NetWorkValue", g_iNetWorkValue);
			pXmlAutoUpdate->SetAttribute("EnableShutDown", g_iEnableShutDown);
			pXmlAutoUpdate->SetAttribute("EnableUserRemind", g_iEnableUserRemind);
		}

	}


	// 判断UserData文件夹是否存在
	if (!PathIsDirectory(_T("UserData")))
	{
		CreateDirectory(_T("UserData"), NULL);
	}

	// 写入文件
	m_pXmlDocGameConfigure->SaveFile(czFileName);
}

void ConfigManager::CreateGameConfigure(const char* czFileName)
{
	// 清空旧的数据
	DeleteGameConfigure();

	// 文件头
	m_pXmlDocGameConfigure = new TiXmlDocument();
	TiXmlDeclaration* pXmlDec = new TiXmlDeclaration("1.0", "gb2312", "yes");
	m_pXmlDocGameConfigure->InsertEndChild(*pXmlDec);
	TiXmlElement * pXmlRoot = new TiXmlElement("Root");
	m_pXmlDocGameConfigure->LinkEndChild(pXmlRoot);

	// 系统配置
	TiXmlElement* pXmlSystemConfig = new TiXmlElement("SystemConfig");
	pXmlRoot->LinkEndChild(pXmlSystemConfig);
	for (int iIndex = 0;iIndex < CT_NUMBER;++iIndex)
	{
		TiXmlElement* pXmlConfig = new TiXmlElement("Config");
		pXmlConfig->SetAttribute("index", iIndex);
		pXmlConfig->SetAttribute("value", m_Configure[iIndex]);
		pXmlSystemConfig->LinkEndChild(pXmlConfig);
	}

	// 默认服务器
	TiXmlElement* pXmlDefaultServer = new TiXmlElement("DefaultServer");
	pXmlDefaultServer->SetAttribute("Index", m_iDefaultServer);
	pXmlDefaultServer->SetAttribute("CatID", g_uiCatID);
	pXmlDefaultServer->SetAttribute("AreaID", g_uiAreaID);
	pXmlDefaultServer->SetAttribute("ServerID", g_uiServerID);
	pXmlDefaultServer->SetAttribute("IP", g_szServerIp);
	pXmlDefaultServer->SetAttribute("Port", g_ServerPort);
	pXmlRoot->LinkEndChild(pXmlDefaultServer);

	// 预下载选项
	TiXmlElement* pXmlAutoUpdate = new TiXmlElement("AutoUpdate");
	pXmlAutoUpdate->SetAttribute("EnableAutoUpdate", g_iEnableAutoUpdate);
	pXmlAutoUpdate->SetAttribute("EnableStandBy", g_iEnableStandBy);
	pXmlAutoUpdate->SetAttribute("StandByValue", g_iStandByValue);
	pXmlAutoUpdate->SetAttribute("EnableCPU", g_iEnableCPU);
	pXmlAutoUpdate->SetAttribute("CPUValue", g_iCPUValue);
	pXmlAutoUpdate->SetAttribute("EnableNetWork", g_iEnableNetWork);
	pXmlAutoUpdate->SetAttribute("NetWorkValue", g_iNetWorkValue);
	pXmlAutoUpdate->SetAttribute("EnableShutDown", g_iEnableShutDown);
	pXmlAutoUpdate->SetAttribute("EnableUserRemind", g_iEnableUserRemind);
	pXmlRoot->LinkEndChild(pXmlAutoUpdate);

	// 默认帐号选项
	TiXmlElement* pXmlDefaultAccount = new TiXmlElement("DefaultAccount");
	pXmlDefaultAccount->SetAttribute("On", 0);
	pXmlDefaultAccount->SetAttribute("Name", "");
	pXmlRoot->LinkEndChild(pXmlDefaultAccount);

	// 判断UserData文件夹是否存在
	if (!PathIsDirectory(_T("UserData")))
	{
		CreateDirectory(_T("UserData"), NULL);
	}

	// 写入文件
	m_pXmlDocGameConfigure->SaveFile(czFileName);

	// 清空数据
	/*if (m_XmlDoc != NULL)
	{
		m_XmlDoc->Clear();
		delete m_XmlDoc;
		m_XmlDoc = NULL;
	}*/
};

void ConfigManager::DeleteGameConfigure()
{
	if(m_pXmlDocGameConfigure != NULL)
	{
		m_pXmlDocGameConfigure->Clear();
		delete m_pXmlDocGameConfigure;
		m_pXmlDocGameConfigure = NULL;
	}
}

BOOL ConfigManager::CheckResExist()
{
	if(!PathFileExists(L"launcherbmp"))
	{
		return FALSE;
	}
	
	if(!PathFileExists(L"launcherbmp\\main.bmp"))
	{
		return FALSE;
	}
	
	if(!PathFileExists(L"launcherbmp\\progress.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\servers.bmp"))
	{
		return FALSE;
	}
	
	if(!PathFileExists(L"launcherbmp\\barblue.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\barorange.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\checkbox_agree.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\closebtn.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\display_set.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\downloadbtn.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\minisizebtn.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\select.bmp"))
	{
		return FALSE;
	}

	if(!PathFileExists(L"launcherbmp\\start_button.bmp"))
	{
		return FALSE;
	}

	return TRUE;
}

void ConfigManager::GetAllResolution(vector<CString>& list)
{
	DWORD iMode = 0;
	DEVMODE devmode;
	set<stResolution*> temp;
	while (EnumDisplaySettings(NULL, iMode, &devmode))
	{	
		if (devmode.dmPelsWidth >= 1024 && devmode.dmPelsHeight >= 768 && devmode.dmBitsPerPel >= 16)	// 限定最小分辨率为1024*768*16
		{
			//CString str;

			//str.Format(_T("%d x %d x %d bpp"), devmode.dmPelsWidth, devmode.dmPelsHeight, devmode.dmBitsPerPel);
			temp.insert(new stResolution(devmode.dmPelsWidth, devmode.dmPelsHeight, devmode.dmBitsPerPel));
		}

		iMode++;
	}

	// 排序后插入
	while (temp.size() > 0)
	{
		set<stResolution*>::iterator smallest = temp.begin();
		for (set<stResolution*>::iterator it = temp.begin();it != temp.end();++it)
		{
			if((*it)->bit > (*smallest)->bit)
			{
				smallest = it;
			}
			else if ((*it)->bit == (*smallest)->bit)
			{
				if ((*it)->width < (*smallest)->width)
				{
					smallest = it;
				}
				else if ((*it)->width == (*smallest)->width)
				{
					if ((*it)->height < (*smallest)->height)
					{
						smallest = it;
					}
				}
			}
		}

		CString str;
		str.Format(_T("%d x %d x %d bpp"), (*smallest)->width, (*smallest)->height, (*smallest)->bit);
		bool bExist = false;
		for (int i = 0;i < (int)list.size();i++)
		{
			if (list[i].Compare(str) == 0)
			{
				bExist = true;
			}
		}
		if (!bExist)
		{
			list.push_back(str);
		}

		temp.erase(smallest);
	}

}

CString ConfigManager::GetRegQueryValue(HKEY hKey, LPCTSTR lpSubKey, REGSAM samDesired, LPCTSTR lpValueName)
{
	LONG result;
	HKEY myKey;
	TCHAR data[256];
	DWORD dataSize = 256;
	CString strResult;

	// 打开注册表
	result = ::RegOpenKeyEx(hKey, lpSubKey, 0, samDesired, &myKey);
	if (result != ERROR_SUCCESS)
	{
		SetLastError(result);
		return strResult;
	}

	// 获取相应的值
	memset(data, 0, sizeof(TCHAR) * 256);
	result = ::RegQueryValueEx(myKey, lpValueName, NULL, NULL, (LPBYTE)data, &dataSize);

	// 关闭注册表
	if (myKey)
	{
		RegCloseKey(myKey);
	}

	if (result != ERROR_SUCCESS)
	{
		SetLastError(result);
		return strResult;
	}
	else
	{
		strResult.Format(_T("%s"), data);
		return strResult;
	}
}

float ConfigManager::GetVideoMemoryTotal()
{
	LPDIRECTDRAW2 lpdd;
	HRESULT ddrval;

	CoInitialize(NULL);

	ddrval = CoCreateInstance(CLSID_DirectDraw, NULL, CLSCTX_ALL, IID_IDirectDraw2, (void**)&lpdd);

	if (!FAILED(ddrval))
	{
		ddrval = IDirectDraw2_Initialize(lpdd, NULL);
	}

	DDCAPS ddcaps;

	ddcaps.dwSize = sizeof(DDCAPS);
	lpdd->GetCaps(&ddcaps, NULL);

	lpdd->Release();

	DWORD dwMem = ddcaps.dwVidMemTotal;

	float iMem = dwMem / 1024.0f / 1024.0f;
	CoUninitialize();

	return iMem;
}

void ConfigManager::GetScrollBarPosList(int top, int bottom, int num, std::vector<int> &posList, std::vector<CPoint> &rangeList)
{
	// 清空旧的数据
	posList.clear();
	rangeList.clear();

	// 输入处理
	if (num < 1)
	{
		num = 1;
	}

	// 计算每个的间隔距离
	int iTotalRange = bottom - top + 1;
	int iNormalRange = iTotalRange / num; // 普通距离
	int iNormalNum = num - 1;	// 普通数量
	iNormalNum = iNormalNum >= 0 ? iNormalNum : 0;
	int iHeadRange = iTotalRange - iNormalNum * iNormalRange;// 头距离

	// 先做好rangelist
	CPoint lastPoint;
	lastPoint.SetPoint(top, top+iHeadRange-1);
	rangeList.push_back(lastPoint);
	for (int iIndex = 2;iIndex <= num;++iIndex)
	{
		int iStart = lastPoint.y+1;
		int iEnd = iStart + iNormalRange - 1;
		lastPoint.SetPoint(iStart, iEnd);
		rangeList.push_back(lastPoint);
	}

	// 通过rangelist计算poslist
	for (int iIndex = 1;iIndex <= num;++iIndex)
	{
		if (iIndex == 1)// 头要特殊处理
		{
			posList.push_back(top);
		}
		else if (iIndex == num)// 尾也要特殊处理
		{
			posList.push_back(bottom);
		}
		else
		{
			CPoint r = rangeList[iIndex-1];
			posList.push_back((r.x + r.y) / 2);
		}
	}

	// 测试输出
	//for (int iIndex = 0;iIndex < num;++iIndex)
	//{
	//	CString strOut;
	//	int pos = posList[iIndex];
	//	int x = rangeList[iIndex].x;
	//	int y = rangeList[iIndex].y;
	//	strOut.Format(L"%d     %d~%d\n", pos, x, y);
	//	OutputDebugString(strOut);
	//}
}

BOOL ConfigManager::SetOutPutData(unsigned int cat, unsigned int area, unsigned int server)
{
	// 定义变量
	unsigned char ucCatNum;
	unsigned char ucAreaNum;
	unsigned char ucServerNum;
	BOOL bFindCat = FALSE;
	BOOL bFindArea = FALSE;
	BOOL bFindServer = FALSE;
	int iServerIndex = 0;
	int iAreaIndex = 0;

	// 记录数据
	g_uiCatID = cat;
	g_uiAreaID = area;
	g_uiServerID = server;

	// 验证cat
	m_pCatInfo = m_PatchCliIntf->GetCatInfo(ucCatNum);
	if (ucCatNum > 0)
	{
		for (int i = 0;i < ucCatNum;++i)
		{
			if (m_pCatInfo[i].nCatID == g_uiCatID)
			{
				m_iDefaultServer = i;
				bFindCat = TRUE;
			}
		}
	}
	else
	{
		g_uiCatID = 0;
		g_uiAreaID = 0;
		g_uiServerID = 0;
		return FALSE;
	}

	if (bFindCat == FALSE)
	{
		m_iDefaultServer = 0;
		g_uiServerID = m_pCatInfo[0].nCatID;
	}

	// 验证area
	m_pAreaInfo = m_PatchCliIntf->GetAreaInfo(g_uiCatID, ucAreaNum);
	if (ucAreaNum > 0)
	{
		for (int i = 0;i < ucAreaNum;++i)
		{
			if (m_pAreaInfo[i].nAreaID == g_uiAreaID)
			{
				iAreaIndex = i;
				bFindArea = TRUE;
			}
		}
	}
	else
	{
		g_uiAreaID = 0;
		g_uiServerID = 0;
		return FALSE;
	}

	if (bFindArea == FALSE)
	{
		iAreaIndex = 0;
		g_uiAreaID = m_pAreaInfo[0].nAreaID;
	}

	// 验证server
	m_pServerInfo = m_PatchCliIntf->GetRegionInfo(g_uiAreaID, ucServerNum);
	if (ucServerNum > 0)
	{
		for (int i = 0;i < ucServerNum;++i)
		{
			if (m_pServerInfo[i].nServerID == g_uiServerID)
			{
				iServerIndex = i;
				bFindServer = TRUE;
			}
		}
	}
	else
	{
		g_uiServerID = 0;
		return FALSE;
	}

	if (bFindServer == FALSE)
	{
		iServerIndex = 0;
		g_uiServerID = m_pServerInfo[0].nServerID;
	}

	// 填充IP
	strcpy_s(g_szPatchIp, _countof(g_szPatchIp), m_pServerInfo[iServerIndex].szPatchIp);
	g_PatchPort = m_pServerInfo[iServerIndex].PatchPort;
	strcpy_s(g_szServerIp, _countof(g_szServerIp), m_pServerInfo[iServerIndex].szServerIp);
	g_ServerPort = m_pServerInfo[iServerIndex].ServerPort;
	
	g_szServerName = _T("");
	g_szServerName += m_pServerInfo[iServerIndex].szServerName;
	g_szServerName += _T("(");
	g_szServerName += m_pAreaInfo[iAreaIndex].szAreaName;
	g_szServerName += _T(")");

	return (bFindCat && bFindArea && bFindServer);
}

void ConfigManager::GetRecommendServer()
{
	m_PatchCliIntf->GetRecommendServer(g_uiRCCatID, g_uiRCAreaID, g_uiRCServerID);
	SetOutPutData(g_uiRCCatID, g_uiRCAreaID, g_uiRCServerID);
	g_szRCServerName = g_szServerName;
}

DWORD ConfigManager::GetPixelShaderVersion()
{
	// 初始化设备
	IDirect3D9* d3d9;
	d3d9 = Direct3DCreate9(D3D_SDK_VERSION);
	if(!d3d9)
	{
		return 0;
	}

	// 获取结构
	D3DCAPS9 caps;
	d3d9->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps);

	// 返回
	return caps.PixelShaderVersion;
}

DWORD ConfigManager::GetNumberOfProcessors()
{
	SYSTEM_INFO systemInfo;
	::GetSystemInfo(&systemInfo);
	return systemInfo.dwNumberOfProcessors;
}

DWORDLONG ConfigManager::GetTotalMemory()
{
	MEMORYSTATUSEX MemoryStatus;
	MemoryStatus.dwLength = sizeof(MEMORYSTATUSEX);
	::GlobalMemoryStatusEx(&MemoryStatus);
	return MemoryStatus.ullTotalPhys;
}

void ConfigManager::SetGameConfigureByHardware()
{
	// 默认的配置为低配
	int iConfigIndex = 0;

	// 检测CPU
	if(GetNumberOfProcessors() >= 2)
	{
		// 检测显卡
		if(GetPixelShaderVersion() < D3DPS_VERSION(3,0))
		{
			iConfigIndex = 0;
		}
		else
		{
			iConfigIndex = 1;
		}
	}
	else
	{
		iConfigIndex = 0;
	}
	
	// 内存容量检测
	float fTotalMemory = GetTotalMemory() / 1024.0f / 1024.0f;
	if(fTotalMemory < 512.0f)
	{
		iConfigIndex = 1;	
	}

	// 设置
	m_Configure[CT_TERRIAN] = iConfigIndex;// 地表细节
	m_Configure[CT_EFFECT] = iConfigIndex;// 特效细节
	m_Configure[CT_SHADOW] = 0;//	阴影细节
	m_Configure[CT_SKILL_EFFECT] = iConfigIndex;//	技能细节
	m_Configure[CT_VISIBLE_RANGE] = iConfigIndex;// 远景显示
	m_Configure[CT_VS] = iConfigIndex;// 垂直同步
	m_Configure[CT_COLOR] = iConfigIndex;// 后期调色
	m_Configure[CT_BLOOM] = 0;// 全屏泛光效果
	m_Configure[CT_AA] = 0;//全屏抗锯齿
	m_Configure[CT_LOW_MEMORY] = !iConfigIndex;// 低内存模式
	m_Configure[CT_EQUIP_EFFECT] = iConfigIndex;// 装备特效
}

bool ConfigManager::LoadUIConfig(const char *czFileName)
{
	// 清空旧的数据
	m_DialogList.clear();
	m_ShapeList.clear();
	if (m_pXmlDoc != NULL)
	{
		m_pXmlDoc->Clear();
	}
	else
	{
		m_pXmlDoc = new TiXmlDocument();
	}

	// 尝试读取文件
	if (!m_pXmlDoc->LoadFile(czFileName))
	{
		if(!LoadUIConfigRes(IDR_XML1, m_pXmlDoc))
		{
			return false;
		}
		else
		{
			m_pXmlDoc->SaveFile(czFileName);
		}
	}
	
	TiXmlElement* pXmlRoot = m_pXmlDoc->RootElement(); // 获取根节点
	if (pXmlRoot != NULL)
	{
		// 分别读取Dialog和Shape
		TiXmlElement* pXmlConfig = pXmlRoot->FirstChildElement();
		for(;pXmlConfig;pXmlConfig = pXmlConfig->NextSiblingElement())
		{
			if (strcmp(pXmlConfig->Value(), "DialogList") == 0)	// DialogList
			{
				LoadDialogUIConfig(pXmlConfig);
			}
			else if(strcmp(pXmlConfig->Value(), "ShapeList") == 0)// ShapeList
			{
				LoadShapeUIConfig(pXmlConfig);
			}
		}
	}
	else
	{
		return false;
	}

	// 正确返回
	return true;
}

CDialogConfig* ConfigManager::GetDialogConfig(CString name)
{
	return (m_DialogList[name] == NULL ? &m_DefaultDialogConfig : m_DialogList[name]);
}

CShapeConfig* ConfigManager::GetShapeConfig(CString name)
{
	return (m_ShapeList[name] == NULL ? &m_DefaultShapeConfig : m_ShapeList[name]);
}

bool ConfigManager::LoadUIConfigRes(WORD wResID, TiXmlDocument* pXmlDoc)
{
	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC	hRC = FindResource(hModule, MAKEINTRESOURCE(wResID), _T("XML"));

	if (NULL == hRC)
	{
		return false;
	}

	HGLOBAL hGlb = LoadResource(hModule,hRC);

	if (NULL == hGlb)
	{
		return false;
	}

	const char* pConfigFileBuffer = (const char*)LockResource(hGlb);

	if (NULL == pConfigFileBuffer)
	{
		return false;
	}

	if(NULL == pXmlDoc)
	{
		return false;
	}

	pXmlDoc->Parse(pConfigFileBuffer);
	
	FreeResource(hGlb);

	return true;
}


bool ConfigManager::LoadDialogUIConfig(TiXmlElement *pXmlDialogList)
{
	if(pXmlDialogList != NULL)
	{
		// 遍历所有的dialog
		TiXmlElement* pXmlDialog = pXmlDialogList->FirstChildElement();
		for(;pXmlDialog;pXmlDialog = pXmlDialog->NextSiblingElement())
		{
			const char* czDialogName = pXmlDialog->Attribute("Name");
			if(czDialogName != NULL)
			{
				if(strlen(czDialogName) == 0)
				{
					continue;
				}
				else
				{
					CDialogConfig* pDlgConf = new CDialogConfig();
					pDlgConf->SetConfig(pXmlDialog);

					CString strName(czDialogName);
					m_DialogList[strName] = pDlgConf;
				}
			}
			else
			{
				continue;// 名字不正确的dialog直接跳过
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}

bool ConfigManager::LoadShapeUIConfig(TiXmlElement *pXmlShapeList)
{
	if(pXmlShapeList != NULL)
	{
		// 遍历所有的Shape
		TiXmlElement* pXmlShape = pXmlShapeList->FirstChildElement();
		for(;pXmlShape;pXmlShape = pXmlShape->NextSiblingElement())
		{
			const char* czShapeName = pXmlShape->Attribute("Name");
			if(czShapeName != NULL)
			{
				if(strlen(czShapeName) == 0)
				{
					continue;
				}
				else
				{
					CShapeConfig* pDlgConf = new CShapeConfig();
					pDlgConf->SetConfig(pXmlShape);

					CString strName(czShapeName);
					m_ShapeList[strName] = pDlgConf;
				}
			}
			else
			{
				continue;// 名字不正确的dialog直接跳过
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}