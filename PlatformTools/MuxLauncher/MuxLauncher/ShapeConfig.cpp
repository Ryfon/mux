﻿#include "StdAfx.h"
#include "ShapeConfig.h"

CShapeConfig::CShapeConfig(void)
{
}

CShapeConfig::~CShapeConfig(void)
{
}

void CShapeConfig::SetConfig(TiXmlElement *config)
{
	// 设置属性
	m_Config = config;

	// 子节点属性
	if(m_Config != NULL)
	{
		// 获取名称
		m_strImageName = m_Config->Attribute("Image");

		// 获取屏蔽色
		m_crTransparent = RGB(m_Config->IntAttribute("R"), m_Config->IntAttribute("G"), m_Config->IntAttribute("B"));

		// 子节点
		TiXmlElement* pXmlChild = m_Config->FirstChildElement();
		for(;pXmlChild;pXmlChild = pXmlChild->NextSiblingElement())
		{
			// 获取属性
			int nLeft = pXmlChild->IntAttribute("Left");
			int nTop = pXmlChild->IntAttribute("Top");
			int nRight = pXmlChild->IntAttribute("Right");
			int nBottom = pXmlChild->IntAttribute("Bottom");
			
			CRect* workRect = new CRect(nLeft, nTop, nRight, nBottom);
			m_WorkRectList.push_back(workRect);
		}
	}
}

CString CShapeConfig::GetImageName()
{
	return m_strImageName;
}

vector<CRect*>& CShapeConfig::GetWorkRectList()
{
	return m_WorkRectList;
}

COLORREF CShapeConfig::GetTransparentColor()
{
	return m_crTransparent;
}