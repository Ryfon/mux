﻿#pragma once
#include <vector>
#include <set>
#include "afxwin.h"
using namespace std;


// CGameConfigDlg 对话框

class CGameConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CGameConfigDlg)

public:
	CGameConfigDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CGameConfigDlg();
	void ShowConfigure();
	void GetConfigure();
// 对话框数据
	enum { IDD = IDD_DIALOGGAMECONFIG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	void GetAllResolution(vector<CString>& list);
	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_comboResolution;
public:
	BOOL m_bWindow;
	BOOL m_bVS;
	BOOL m_bSoundOff;
	CComboBox m_comboTerrain;
	CComboBox m_comboEffect;
	CComboBox m_comboShadow;
};
