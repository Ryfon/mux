﻿// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 从 Windows 头中排除极少使用的资料
#endif

// 如果您必须使用下列所指定的平台之前的平台，则修改下面的定义。
// 有关不同平台的相应值的最新信息，请参考 MSDN。
#ifndef WINVER				// 允许使用特定于 Windows XP 或更高版本的功能。
#define WINVER 0x0501		// 将此值更改为相应的值，以适用于 Windows 的其他版本。
#endif

#ifndef _WIN32_WINNT		// 允许使用特定于 Windows XP 或更高版本的功能。
#define _WIN32_WINNT 0x0501	// 将此值更改为相应的值，以适用于 Windows 的其他版本。
#endif						

#ifndef _WIN32_WINDOWS		// 允许使用特定于 Windows 98 或更高版本的功能。
#define _WIN32_WINDOWS 0x0410 // 将它更改为适合 Windows Me 或更高版本的相应值。
#endif

#ifndef _WIN32_IE			// 允许使用特定于 IE 6.0 或更高版本的功能。
#define _WIN32_IE 0x0600	// 将此值更改为相应的值，以适用于 IE 的其他版本。值。
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 某些 CString 构造函数将是显式的

// 关闭 MFC 对某些常见但经常可放心忽略的警告消息的隐藏
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 核心组件和标准组件
#include <afxext.h>         // MFC 扩展


#include <afxdisp.h>        // MFC 自动化类



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC 对 Internet Explorer 4 公共控件的支持
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC 对 Windows 公共控件的支持
#endif // _AFX_NO_AFXCMN_SUPPORT









#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#include "tinyxml.h"
#include "Package.h"
#pragma comment(lib, "zlib.lib")
#pragma comment(lib, "PatchClient.lib")

#include "FourBitmapButton.h"
#include "ImageProgressCtrl.h"
#include "ImageComboBox.h"
#include <afxdhtml.h>

#include "DialogConfig.h"
#include "ShapeConfig.h"
#include "ConfigManager.h"
#include "..\..\..\Client\Source\Configuration\ConfigInitInfo.h"

//enum EConfigType
//{
//	CT_RESOLUTION_INDEX,	// 分辨率
//	CT_CPU_TYPE,			// CPU 类型
//	CT_WINDOW, 				// 窗口模式
//	CT_LOW_MEMORY,			// 低内存模式
//	CT_TERRIAN,				// 地表细节
//	CT_SHADOW,				// 阴影细节
//	CT_EFFECT,				// 特效细节
//	CT_BLOOM,				// 全屏泛光
//	CT_VS,					// 垂直同步
//	CT_AA,					// 抗锯齿
//	CT_MUSIC_SOUND,			// 音乐音量
//	CT_MUSIC_OFF,			//	音乐开关
//	CT_EFFECT_SOUND,		// 音效音量
//	CT_EFFECT_OFF,			// 音效开关
//	CT_PROTECTED_MASSLEVEL,	// 出售保护品质
//	CT_UI_STYLE,			// 界面风格
//	CT_CHATBUBBLE_WORLD,	// 世界频道
//	CT_CHATBUBBLE_GUILD,	// 战盟频道
//	CT_CHATBUBBLE_CITY,		// 同城频道
//	CT_CHATBUBBLE_TEAM,		// 组队频道
//	CT_CHATBUBBLE_NORMAL,	// 普通频道
//	CT_CHATBUBBLE_PRIVATE,	// 私聊频道
//	CT_REFUSE_TEAM,			// 拒绝组队
//	CT_REFUSE_TRADE,		// 拒绝交易
//	CT_REFUSE_FRIEND,		// 拒绝添加好友邀请
//	CT_REFUSE_GUILD,		// 拒绝战盟邀请
//	CT_REFUSE_MOUNT,		// 拒绝双人骑乘
//	CT_NAME_DISPLAY,		// 头顶文字显示
//	CT_HP_DISPLAY,			// 头顶血条显示
//	CT_FRAME_DISPLAY,		// 帧数显示
//	CT_AUTOATK,				// 自动攻击
//	CT_SKILL_EFFECT,		// 技能细节
//	CT_COLOR,				// 后期调色
//	CT_SOUND_OFF,			// 禁止声音
//	CT_LOCK_CAMERA,			// 镜头锁定
//	CT_VISIBLE_RANGE,		// 可视范围
//	CT_NUMBER//
//};

typedef struct stResolution 
{
	DWORD width;
	DWORD height;
	DWORD bit;

	stResolution(DWORD w, DWORD h, DWORD b)
	{
		width = w;
		height = h;
		bit = b;
	}
}stResolution;

//static int aDefaultValue[CT_NUMBER] = {0,0,1,1,1,1,1,0,0,0,30,0,30,0,0,1,1,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,1,1}; // 默认值

extern int m_Configure[CT_NUMBER];	
extern int m_iDefaultServer;
extern BOOL m_bIsApplyProtocol;

extern char g_szPatchIp[256];
extern unsigned short g_PatchPort;

extern char g_szServerIp[256];
extern unsigned short g_ServerPort;

extern CString g_szServerName;
extern unsigned int g_uiCatID;
extern unsigned int g_uiAreaID;
extern unsigned int g_uiServerID;

extern CString g_szRCServerName;
extern unsigned int g_uiRCCatID;
extern unsigned int g_uiRCAreaID;
extern unsigned int g_uiRCServerID;

// 预下载选项
extern int g_iEnableAutoUpdate;
extern int g_iEnableStandBy;
extern int g_iStandByValue;
extern int g_iEnableCPU;
extern int g_iCPUValue;
extern int g_iEnableNetWork;
extern int g_iNetWorkValue;
extern int g_iEnableShutDown;
extern int g_iEnableUserRemind;

// 自定义的消息
#define WM_SERVER_DLG_UPDATE		WM_USER+200