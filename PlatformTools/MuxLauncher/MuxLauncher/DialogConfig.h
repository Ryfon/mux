﻿#pragma once

class CDialogConfig
{
public:
	CDialogConfig(void);
	~CDialogConfig(void);

public:
	void SetConfig(TiXmlElement* config);
	TiXmlElement* GetConfig();
	TiXmlElement* GetChildConfig(CString name);
	CString GetShapeName();
protected:
	TiXmlElement* m_Config;						// 窗口自己的config
	map<CString, TiXmlElement*> m_ChildList;	// 窗口包含的子控件的config
	CString m_strShapeName;
	TiXmlElement* m_pXmlDefaultDialog;			// 默认的窗口配置
	TiXmlElement* m_pXmlDefaultChild;			// 默认的child配置
};
