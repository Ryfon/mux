﻿/** @file PackageSourceManager.h 
@brief 包资源管理
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：和萌
*	完成日期：2009-07-23
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#pragma once
#include "Package.h"

class CPackageSourceManager
{
public:
	CPackageSourceManager(void);
	~CPackageSourceManager(void);

	static	CPackageSourceManager* GetInstance();
	void	Release();

	// 给定路径读取文件数据 [7/23/2009 hemeng]
	char* GetFileData(const string strFilePath);

private:
	// 如果map中不存在指定package则查找指定package [7/23/2009 hemeng]
	CPackage*	_GetRootPackage(const string strFilePath);
	// 读取数据 [7/23/2009 hemeng]
	char*		_ReadFileData(CPackage* pkPack, const string strFilePath);
	// 是否存在父级包或本级包 [7/23/2009 hemeng]
	CPackage*	_GetExistPack(const string strFilePath);

private:
	map<string , CPackage* > m_arrPackSources;
};
