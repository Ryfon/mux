﻿#include "StdAfx.h"
#include "ImageProgressCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CImageProgressCtrl::CImageProgressCtrl(void)
{
	m_nPos = 0;
	m_nStepSize = 1;
	m_nMax = 100;
	m_nMin = 0;
	m_bShowText  = TRUE;
	m_bShowImage = FALSE;		
	m_strText.Empty();
	m_colFore = ::GetSysColor(COLOR_HIGHLIGHT);
	m_colBk = ::GetSysColor(COLOR_WINDOW);
	m_colTextFore = ::GetSysColor(COLOR_HIGHLIGHT);
	m_colTextBk = ::GetSysColor(COLOR_WINDOW);
	m_nBarWidth = -1;

	m_pBkImage = NULL;
	m_pForeImage =	NULL;
}

CImageProgressCtrl::~CImageProgressCtrl(void)
{
	if (m_pBkImage)
	{
		delete m_pBkImage;
		m_pBkImage = NULL;
	}

	if (m_pForeImage)
	{
		delete m_pForeImage;
		m_pForeImage = NULL;
	}
}

BOOL	CImageProgressCtrl::InitImages(CString pszForeImageFile, CString pszBkImageFile /* = NULL */)
{
	if (pszBkImageFile)
	{
		m_pBkImage = new CImage();
		HRESULT hr = m_pBkImage->Load(pszBkImageFile);
		if (hr != S_OK)
		{
			return false;
		}
	}

	if (pszForeImageFile)
	{
		m_pForeImage = new CImage();
		HRESULT hr = m_pForeImage->Load(pszForeImageFile);
		if (hr != S_OK)
		{
			return false;
		}
	}

	return true;
}

BOOL CImageProgressCtrl::InitImages(CString pszForeImageFile, int width, int height)
{
	CBitmap* temp = new CBitmap();

	if (LoadDynamicBitmap(temp, pszForeImageFile, 0, 0, width, height))
	{
		m_pBkImage = new CImage();
		m_pBkImage->Attach((HBITMAP)temp->Detach());
	}
	else
	{
		return FALSE;
	}
	
	if (LoadDynamicBitmap(temp, pszForeImageFile, 0, height, width, height))
	{
		m_pForeImage = new CImage();
		m_pForeImage->Attach((HBITMAP)temp->Detach());
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

BEGIN_MESSAGE_MAP(CImageProgressCtrl, CStatic)
	//{{AFX_MSG_MAP(CTextProgressCtrl)
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void	CImageProgressCtrl::SetDisplayText(CString strText)
{
	if (strText.GetLength() != 0)
	{
		m_strText = strText;
	}
}

void	CImageProgressCtrl::OnSize(UINT nType,int cx, int cy)
{
	CProgressCtrl::OnSize(nType,cx,cy);

	m_nBarWidth = -1;
}

void	CImageProgressCtrl::OnPaint()
{
	if (m_nMin >= m_nMax)
	{
		return;
	}

	CRect LeftRect, RightRect, ClientRect;
	GetClientRect(ClientRect);

	double Fraction = (double)(m_nPos - m_nMin) / ((double)(m_nMax - m_nMin));

	CPaintDC PaintDC(this); // device context for painting
	CMemDC dc(&PaintDC);
	//CPaintDC dc(this);    // device context for painting (if not double buffering)

	LeftRect = RightRect = ClientRect;

	LeftRect.right = LeftRect.left + (int)((LeftRect.right - LeftRect.left)*Fraction);
	dc.FillSolidRect(LeftRect, m_colFore);

	RightRect.left = LeftRect.right;
	dc.FillSolidRect(RightRect, m_colBk);

	if (m_bShowImage)
	{

		if (m_pBkImage)
		{
			CDC* pBkDC = CDC::FromHandle(m_pBkImage->GetDC());
			dc.StretchBlt(0,0,ClientRect.Width(),ClientRect.Height(),pBkDC,0,0,m_pBkImage->GetWidth(),m_pBkImage->GetHeight(),SRCCOPY);
			m_pBkImage->ReleaseDC();
		}

		if (m_pForeImage)
		{
			CDC* pForeDC = CDC::FromHandle(m_pForeImage->GetDC());
			//dc.StretchBlt(0,0,ClientRect.Width()*Fraction,ClientRect.Height(),pForeDC,0,0,m_pForeImage->GetWidth(),m_pForeImage->GetHeight(),SRCCOPY);
			dc.BitBlt(0,0,(int)(ClientRect.Width()*Fraction),ClientRect.Height(),pForeDC,0,0,SRCCOPY);
			m_pForeImage->ReleaseDC();
		}
	}

	if (m_bShowText)
	{
		// 设置字体
		CFont font;
		font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体") );
		CFont* def_font = dc->SelectObject(&font);
		// 设置字体

		CString str;
		if (m_strText.GetLength())
			str = m_strText;
		else
			str.Format(_T("%d%%"), (int)(Fraction*100.0));

		dc.SetBkMode(TRANSPARENT);

		CRgn rgn;
		rgn.CreateRectRgn(LeftRect.left, LeftRect.top, LeftRect.right, LeftRect.bottom);
		dc.SelectClipRgn(&rgn);
		dc.SetTextColor(m_colTextBk);

		dc.DrawText(str, ClientRect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		rgn.DeleteObject();
		rgn.CreateRectRgn(RightRect.left, RightRect.top, RightRect.right, RightRect.bottom);
		dc.SelectClipRgn(&rgn);
		dc.SetTextColor(m_colTextFore);

		dc.DrawText(str, ClientRect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		// 设置字体
		dc->SelectObject(def_font);
		font.DeleteObject();
		// 设置字体
	}

}

void	CImageProgressCtrl::SetForeColour(COLORREF col)
{
	m_colFore = col;
}

void	CImageProgressCtrl::SetBkColour(COLORREF col)
{
	 m_colBk = col;
}

void	CImageProgressCtrl::SetTextForeColour(COLORREF col)
{
	m_colTextFore = col;
}

void CImageProgressCtrl::SetTextBkColour(COLORREF col)
{
	m_colTextBk = col;
}

COLORREF CImageProgressCtrl::GetForeColour()
{
	return m_colFore;
}

COLORREF CImageProgressCtrl::GetBkColour()
{
	return m_colBk;
}

COLORREF CImageProgressCtrl::GetTextForeColour()
{
	return m_colTextFore;
}

COLORREF CImageProgressCtrl::GetTextBkColour()
{
	return m_colTextBk;
}

void CImageProgressCtrl::SetShowText(BOOL bShow)
{ 
	if (::IsWindow(m_hWnd) && m_bShowText != bShow)
		Invalidate();

	m_bShowText = bShow;
}


void CImageProgressCtrl::SetRange(int nLower, int nUpper)
{
	m_nMax = nUpper;
	m_nMin = nLower;
}

int CImageProgressCtrl::SetPos(int nPos) 
{    
	if (!::IsWindow(m_hWnd))
		return -1;

	int nOldPos = m_nPos;
	m_nPos = nPos;

	CRect rect;
	GetClientRect(rect);

	double Fraction = (double)(m_nPos - m_nMin) / ((double)(m_nMax - m_nMin));
	int nBarWidth = (int) (Fraction * rect.Width());

	if (nBarWidth != m_nBarWidth)
	{
		m_nBarWidth = nBarWidth;
		RedrawWindow();
	}

	this->Invalidate();
	return nOldPos;
}

int CImageProgressCtrl::StepIt() 
{    
	return SetPos(m_nPos + m_nStepSize);
}

int CImageProgressCtrl::OffsetPos(int nPos)
{
	return SetPos(m_nPos + nPos);
}

int CImageProgressCtrl::SetStep(int nStep)
{
	int nOldStep = nStep;
	m_nStepSize = nStep;
	return nOldStep;
}

void CImageProgressCtrl::SetShowImage(BOOL bShow)
{
	m_bShowImage = bShow;
}

BOOL CImageProgressCtrl::LoadDynamicBitmap(CBitmap* pChangeImage, CString strFileName, int x, int y, int width, int height)
{
	CDC srcDC;
	srcDC.CreateCompatibleDC(NULL);

	CDC memDC;
	memDC.CreateCompatibleDC(NULL);

	CBitmap src;

	HBITMAP hbm = (HBITMAP)LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE|LR_CREATEDIBSECTION);
	if (hbm == NULL)
	{
		return FALSE;
	}
	src.Attach(hbm);

	CBitmap* pOldBitmap = srcDC.SelectObject(&src);

	//BITMAP bmpinfo;
	//src.GetBitmap(&bmpinfo);
	int bmpWidth = width;
	int bmpHeight = height;

	BOOL Rz = TRUE;

	pChangeImage->DeleteObject();
	pChangeImage->CreateCompatibleBitmap(&srcDC, bmpWidth, bmpHeight);

	memDC.SelectObject(pChangeImage);
	if (!memDC.BitBlt(0, 0, bmpWidth, bmpHeight, &srcDC, x, y, SRCCOPY))
	{
		Rz = FALSE;
	}

	srcDC.SelectObject(pOldBitmap);

	srcDC.DeleteDC();
	memDC.DeleteDC();

	src.DeleteObject();

	return Rz;
}