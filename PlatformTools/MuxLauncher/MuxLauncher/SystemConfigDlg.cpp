﻿// SystemConfigDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "SystemConfigDlg.h"
#include "Windows.h"

// CSystemConfigDlg 对话框

IMPLEMENT_DYNAMIC(CSystemConfigDlg, CDialog)

CSystemConfigDlg::CSystemConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSystemConfigDlg::IDD, pParent)
{
	
}

CSystemConfigDlg::~CSystemConfigDlg()
{
}

void CSystemConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSystemConfigDlg, CDialog)
END_MESSAGE_MAP()


// CSystemConfigDlg 消息处理程序
