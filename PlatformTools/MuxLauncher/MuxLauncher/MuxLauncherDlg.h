﻿// MuxLauncherDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "explorer_news.h"
#include "afxcmn.h"
#include "BtnST.h"

#include "PatchCliIntf.h"

#define RADIO_ID_START		10000
#define BUTTON_ID_START		20000
#define BUTTON_ID_END		29999
#define MAX_SERVER_NUM		26

// CMuxLauncherDlg 对话框
class CMuxLauncherDlg : public CDialog, public CPatchCliEvent
{
// 构造
public:
	CMuxLauncherDlg(CWnd* pParent = NULL);	// 标准构造函数
	virtual ~CMuxLauncherDlg();

// 对话框数据
	enum { IDD = IDD_MUXLAUNCHER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 事件
public:
	virtual int OnCliVersion(PATCHVERSION& version);
	virtual int OnServerVersion(PATCHVERSION& version, unsigned char ucPatchNum);
	virtual int OnDownloadProgress(PATCHVERSION& version, int nIndex, int nPos, int nDownloadSpeed, int nUploadSpeed, float fFileSize);
	virtual int OnInstallProgress(PATCHVERSION& version, int nStatus, int nPos);
	virtual int OnError(int nErrorCode);
private:
	CPatchCliIntf* m_PatchCliIntf;

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	void GetServerList(const char * czFileName);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	bool ShowImage(CDC* dc, CString strFileName, int iPosX, int iPosY, COLORREF crTransparent);
	bool ShowText(CDC* dc, CString strTextValue, int iPosX, int iPosY, COLORREF crColor);
	CString WebStr2Str(LPCTSTR lpBuf);
private:
	map<CString, CString> m_WebPropMap;
	int m_iPatchNum;
public:
	void LoadConfigure(const char * czFileName);
	void SaveConfigure(const char * czFileName);

	afx_msg void OnBnClickedButtonconfig();
	virtual afx_msg void OnBnClickedButtonstart() sealed;

	CExplorer_news m_ExplorerNews;
	CExplorer_news m_ExplorerNotice;
	BOOL m_bAppProtocol;
	CButtonST m_Servers[MAX_SERVER_NUM];
	vector<CButtonST*> m_LinkList;
	CFont m_DefFont;
	CString m_ServerName[MAX_SERVER_NUM];
	int m_iMaxServerNum;
	afx_msg void OnBnClickedButtonRegister();
	afx_msg void OnBnClickedButtonAccount();
	afx_msg void OnBnClickedButtonRookie();
	afx_msg void OnBnClickedButtonOfficial();
	afx_msg void OnBnClickedButtonForum();
	afx_msg void OnBnClickedButtonSupport();
	afx_msg void OnRangeRadioClicked(UINT uID);
	afx_msg void OnLinkButtonClicked(UINT uID);
	CString m_staticShowDefaultServer;
	afx_msg void OnBnClickedButtonManualDownload();
	afx_msg void OnBnClickedButtonProtocol();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBeforeNavigate2Explorer(LPDISPATCH pDisp, VARIANT FAR* URL, VARIANT FAR* Flags, VARIANT FAR* TargetFrameName, VARIANT FAR* PostData, VARIANT FAR* Headers, BOOL FAR* Cancel);
	DECLARE_EVENTSINK_MAP()
	CButtonST m_btnSysConfig;
	CButtonST m_btnStartGame;
	CProgressCtrl m_ProcessCtrlDownLoad;
	CProgressCtrl m_ProcessCtrlInstall;
};
