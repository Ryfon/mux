﻿#pragma once

#include <set>
using namespace std;

#include "PatchCliIntf.h"

class ConfigManager
{
public:
	~ConfigManager(void);
public:
	static ConfigManager* GetInstance()
	{
		if (m_Instance == NULL)
		{
			m_Instance = new ConfigManager();
		}

		return m_Instance;
	}
public:
	bool LoadConfig(const char* czFileName);
	const char* GetConfig(int iID, const char* czAttrName);
	vector<TiXmlElement*>* GetStaticList(int iID);
	vector<int>* GetLinkList(int iID);
	vector<int>* GetControlList();
public:
	void LoadGameConfigure(const char* czFileName);
	void SaveGameConfigure(const char* czFileName);
	void CreateGameConfigure(const char* czFileName);
	void DeleteGameConfigure();
public:
	BOOL CheckResExist();
public:
	void GetAllResolution(vector<CString>& list);
public:
	CString GetRegQueryValue(HKEY hKey, LPCTSTR lpSubKey, REGSAM samDesired, LPCTSTR lpValueName);
public:
	float GetVideoMemoryTotal();
public:
	void GetScrollBarPosList(int top, int bottom, int num, vector<int>& posList, vector<CPoint>& rangeList);
public:
	BOOL SetOutPutData(unsigned int cat, unsigned int area, unsigned int server);
public:
	void GetRecommendServer();
public:
	void SetGameConfigureByHardware();
public:
	bool LoadUIConfig(const char* czFileName); 
	CDialogConfig* GetDialogConfig(CString name);
	CShapeConfig* GetShapeConfig(CString name);
private:
	bool LoadUIConfigRes(WORD wResID, TiXmlDocument* pXmlDoc);
	bool LoadDialogUIConfig(TiXmlElement* pXmlDialogList);
	bool LoadShapeUIConfig(TiXmlElement* pXmlShapeList);
private:
	DWORD GetPixelShaderVersion();
	DWORD GetNumberOfProcessors();
	DWORDLONG GetTotalMemory();
private:
	ConfigManager(void);
	static ConfigManager* m_Instance;
private:
	TiXmlDocument* m_pXmlDocGameConfigure;
private:
	TiXmlDocument* m_pXmlDoc;
	map<CString, CDialogConfig*>			m_DialogList;
	map<CString, CShapeConfig*>				m_ShapeList;
	CDialogConfig							m_DefaultDialogConfig;
	CShapeConfig							m_DefaultShapeConfig;
private:
	map<int, TiXmlElement*>					m_ConfigMap;
	map<int, vector<TiXmlElement*>* >		m_StaticMap;
	map<int, vector<int>* >					m_LinkMap;
	vector<int>								m_ControlList;
private:
	CPatchCliIntf*	m_PatchCliIntf;
	PCATINFO		m_pCatInfo;
	PAREAINFO		m_pAreaInfo;
	PSERVERINFO		m_pServerInfo;
};
