﻿#pragma once


// CImageComboBox

class CImageComboBox : public CComboBox
{
	DECLARE_DYNAMIC(CImageComboBox)

public:
	CImageComboBox();
	virtual ~CImageComboBox();
	virtual void PreSubclassWindow();
public:
	BOOL InitComboBox(UINT nID, CWnd* pParent, CString strImage = L"", COLORREF crListBKColor = RGB(255, 255, 255), COLORREF crFontColor = RGB(0, 0, 0), int iFontHeight = 14, CString strFontFaceName = L"黑体", COLORREF crListHiliteColor = RGB(49, 106, 197) );
	BOOL SetBKImage(CString strFileName);
	void SetFontHeight(int height);
	void SetFontFaceName(CString strFaceName);
	void SetFontColor(COLORREF color);
	void SetListBKColor(COLORREF color);
	void SetListHiliteColor(COLORREF color);
public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
private:
	BOOL LoadBKImage(CString strFileName);
	BOOL DrawBKImage(CDC* pDC);
	BOOL DrawFontText(CDC* pDC, CRect* rect, CString strText);
private:
	CBitmap*			m_pBkgndImage;
	CString				m_strBKImageName;

	int					m_iFontHeight;
	CString				m_strFontFaceName;

	COLORREF			m_crFontColor;
	COLORREF			m_crListBKColor;
	COLORREF			m_crListHiliteColor;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnPaint();
	afx_msg void OnCbnSetfocus();
	afx_msg void OnCbnSelchange();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
};


