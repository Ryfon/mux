﻿#pragma once

#include "PatchCliIntf.h"
#include "SettingDlg.h"

// CPatchDlg 对话框

class CPatchDlg : public CDialog, public CPatchCliEvent
{
	DECLARE_DYNAMIC(CPatchDlg)

public:
	CPatchDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CPatchDlg();

// 对话框数据
	enum { IDD = IDD_PATCH_DIALOG };

//	成员变量
private:

// 成员函数
private:
	BOOL StartGame();
	void PaintBk(CDC* pDC, CRect rect);
	void ShowText(CDC* dc, CString strTextValue, CRect rect, COLORREF crColor);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	void SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent);
	BOOL PreTranslateMessage(MSG* pMsg);
public:
	void SetServerDlg(CDialog* pServerDlg);
// 事件
public:
	virtual int OnCliVersion(PATCHVERSION& version);
	virtual int OnServerVersion(PATCHVERSION& version, unsigned char ucPatchNum);
	virtual int OnDownloadProgress(PATCHVERSION& version, int nIndex, int nPos, int nDownloadSpeed, int nUploadSpeed, float fFileSize);
	virtual int OnInstallProgress(PATCHVERSION& version, int nStatus, int nPos);
	virtual int OnError(int nErrorCode);
private:
	CPatchCliIntf* m_PatchCliIntf;
	BOOL m_bPatchCliIntfStarted;
	int m_iPatchNum;
// 事件

private:
	CBitmap*			m_pBkgndImage;
	CString				m_strBkgndImageName;
	CFourBitmapButton	m_ImageButtonGameStart;
	CFourBitmapButton	m_ImageButtonGameConfig;
	CFourBitmapButton	m_ImageButtonManualDownload;
	CImageProgressCtrl	m_ImageProgressCtrlDownLoad;
	CImageProgressCtrl	m_ImageProgressCtrlInstall;

	CRect m_rectServer;
	CRect m_rectClient;
	CString m_strServerVersion;
	CString m_strClientVersion;
	COLORREF m_crServerVersion;
	COLORREF m_crClientVersion;

	CSettingDlg* m_pSettingDlg;

	CDialog* m_pServerDlg;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonConfig();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};
