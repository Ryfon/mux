﻿// stdafx.cpp : 只包括标准包含文件的源文件
// MuxLauncher.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"


int m_Configure[CT_NUMBER];	
int m_iDefaultServer;
int m_bIsApplyProtocol;

char g_szPatchIp[256];
unsigned short g_PatchPort;

char g_szServerIp[256];
unsigned short g_ServerPort;

CString g_szServerName;
unsigned int g_uiCatID;
unsigned int g_uiAreaID;
unsigned int g_uiServerID;

CString g_szRCServerName;
unsigned int g_uiRCCatID;
unsigned int g_uiRCAreaID;
unsigned int g_uiRCServerID;

// 预下载选项
int	g_iEnableAutoUpdate;
int	g_iEnableStandBy;
int	g_iStandByValue;
int	g_iEnableCPU;
int	g_iCPUValue;
int	g_iEnableNetWork;
int	g_iNetWorkValue;
int g_iEnableShutDown;
int	g_iEnableUserRemind;