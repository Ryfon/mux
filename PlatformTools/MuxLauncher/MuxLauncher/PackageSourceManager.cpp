﻿#include "StdAfx.h"
#include "PackageSourceManager.h"
#include "Helper.h"

CPackageSourceManager::CPackageSourceManager(void)
{

}

CPackageSourceManager::~CPackageSourceManager(void)
{
	Release();
}

void CPackageSourceManager::Release()
{
	map<string, CPackage*>::iterator it = m_arrPackSources.begin();
	for (; it != m_arrPackSources.end(); it++)
	{
		CPackage* pTemp = it->second;
		if (pTemp != NULL)
		{
			pTemp->ClosePackage();
			pTemp = NULL;
		}
	}
}

CPackageSourceManager* CPackageSourceManager::GetInstance()
{
	static CPackageSourceManager* m_pkThis = new CPackageSourceManager();
	return m_pkThis;
}

CPackage* CPackageSourceManager::_GetRootPackage(const string strFilePath)
{
	string strPackName = strFilePath.substr(strFilePath.find_last_of("\\") + 1);

	if (strPackName.find(".") >= 0)
	{
		strPackName = strPackName.substr(strPackName.find(".") + 1);
	}

	// 当不是根目录时 [7/23/2009 hemeng]
	if( !CHelper::IsRoot(strFilePath) )
	{
		string strParentDir = strFilePath.substr(0,strFilePath.find_last_of("\\"));

		//  查找父级目录是否存在 [7/23/2009 hemeng]
		CFileFind finder;

		if (!finder.FindFile(CString(strParentDir.c_str())))
		{
			return _GetRootPackage(strParentDir);
		}
		else
		{
			// 在父级目录中查找该文件或文件夹命名的包 [7/23/2009 hemeng]
			string strFile = strParentDir + "\\" + strPackName + ".pkg";
			
			if ( !finder.FindFile(CString(strFile.c_str())) )
			{
				return _GetRootPackage(strParentDir);
			}
			else
			{
				CPackage* pkPack = new CPackage();
				
				if (!pkPack->OpenPackage(strFile))
				{
					return NULL;
				}
				else
				{
					return pkPack;
				}
			}
		}
	}
	else
	{
		return NULL;
	}

	return NULL;
}

char* CPackageSourceManager::_ReadFileData(CPackage* pkPack, const string strFilePath)
{
	CPackageFileReader* pFileReader = pkPack->GetPackageFileReader(strFilePath);
	if (pFileReader == NULL)
	{
		AfxMessageBox(_T("读取pkg data 失败！"));
		return NULL;
	}
	SPackageFileInfo* pFileInfo = pFileReader->GetPackageFileInfo();
	if (pFileInfo == NULL)
	{
		AfxMessageBox(_T("获取文件信息失败"));
		return NULL;
	}
	char* pBuf = new char[pFileInfo->nFileOrgSize];
	UINT nRet = pFileReader->ReadBuffer((unsigned char*)pBuf);
	return nRet != 0 ? pBuf : NULL;
	
	return NULL;
}

char* CPackageSourceManager::GetFileData(const string strFilePath)
{
	CPackage* pkPack = _GetExistPack(strFilePath);
	// 如果没有已打开的包对应，则查找创建 [7/23/2009 hemeng]
	if (pkPack == NULL)
	{
		pkPack = _GetRootPackage(strFilePath);

		if (pkPack == NULL)
		{
			return NULL;
		}
		else
		{
			string strPackName = pkPack->GetPackageRootDir();
			m_arrPackSources.insert(pair<string,CPackage*>(strPackName, pkPack));			
		}	
	}	

	string strRelPath = strFilePath;
	StringLower(strRelPath);
	// relPath 要去掉包名+ \\ [7/23/2009 hemeng]
	size_t uiPos = strRelPath.find(pkPack->GetPackageRootDir()) + pkPack->GetPackageRootDir().length() + 1;
	strRelPath = strRelPath.substr(uiPos);

	return _ReadFileData(pkPack,strRelPath);
}

CPackage* CPackageSourceManager::_GetExistPack(const string strFilePath)
{
	map<string, CPackage*>::iterator it = m_arrPackSources.begin();

	for (;it != m_arrPackSources.end(); it++)
	{
		string strPackName = it->first;

		if ( strFilePath.find(strPackName) >= 0)
		{
			// 找到包 [7/23/2009 hemeng]
			return it->second;
		}		
	}
	
	return NULL;
}
