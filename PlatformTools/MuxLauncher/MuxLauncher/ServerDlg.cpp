﻿// ServerDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "ServerDlg.h"
#include "PackageSourceManager.h"

// CServerDlg 对话框

IMPLEMENT_DYNAMIC(CServerDlg, CDialog)

CServerDlg::CServerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServerDlg::IDD, pParent)
{
	m_pBkgndImage = NULL;
	m_PatchCliIntf = CreatePatchIntf(NULL);
	m_iMaxServerNum = 0;
	m_pSelectDlg = NULL;
}

CServerDlg::~CServerDlg()
{
	DeletePatchIntf(m_PatchCliIntf);
	if (m_pSelectDlg != NULL)
	{
		delete m_pSelectDlg;
		m_pSelectDlg = NULL;
	}
}

void CServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CServerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 获取服务器列表
	m_iMaxServerNum = 0;
	//GetServerList("sysconfig.xml");
	GetServerList();

	// 获取UI配置
	CDialogConfig* pDialogConfig = ConfigManager::GetInstance()->GetDialogConfig(_T("ServerDlg"));
	TiXmlElement* pXmlDialogConfig = pDialogConfig->GetConfig();
	TiXmlElement* pXmlTextSelectLabel = pDialogConfig->GetChildConfig(_T("SelectLabel"));
	TiXmlElement* pXmlTextSelectName = pDialogConfig->GetChildConfig(_T("SelectName"));
	TiXmlElement* pXmlButtonRecommandServer = pDialogConfig->GetChildConfig(_T("RecommandServer"));
	TiXmlElement* pXmlButtonApplyProtocol = pDialogConfig->GetChildConfig(_T("ApplyProtocol"));
	TiXmlElement* pXmlButtonServer1 = pDialogConfig->GetChildConfig(_T("Server1"));
	TiXmlElement* pXmlButtonServer2 = pDialogConfig->GetChildConfig(_T("Server2"));
	TiXmlElement* pXmlButtonServer3 = pDialogConfig->GetChildConfig(_T("Server3"));
	TiXmlElement* pXmlButtonServer4 = pDialogConfig->GetChildConfig(_T("Server4"));

	// 初始化自身大小
	m_strBkgndImageName = pXmlDialogConfig->Attribute("BGImage");
	this->MoveWindow(pXmlDialogConfig->IntAttribute("Left"), pXmlDialogConfig->IntAttribute("Top"), pXmlDialogConfig->IntAttribute("Width"), pXmlDialogConfig->IntAttribute("Height"));
	SetupRegion(GetDC(), pDialogConfig->GetShapeName(), RGB(255, 0, 255));
	
	// 初始化推荐服务器按钮
	this->GetDlgItem(IDC_BUTTON_RECOMMAND_SERVER)->MoveWindow(pXmlButtonRecommandServer->IntAttribute("Left"), pXmlButtonRecommandServer->IntAttribute("Top"), pXmlButtonRecommandServer->IntAttribute("Width"), pXmlButtonRecommandServer->IntAttribute("Height"));
	//m_ImageButtonRecommandServer.InitButton(IDC_BUTTON_RECOMMAND_SERVER, this, CString(pXmlButtonRecommandServer->Attribute("BGImage")), pXmlButtonRecommandServer->IntAttribute("Width"), pXmlButtonRecommandServer->IntAttribute("Height"));
	m_ImageButtonRecommandServer.InitButton(IDC_BUTTON_RECOMMAND_SERVER,this,CString(pXmlButtonRecommandServer->Attribute("NormalImage")),CString(pXmlButtonRecommandServer->Attribute("HoverImage")),CString(pXmlButtonRecommandServer->Attribute("PressedImage")),CString(pXmlButtonRecommandServer->Attribute("DisableImage")));
	CFont RCnameFont;
	RCnameFont.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, GB2312_CHARSET, OUT_STROKE_PRECIS, CLIP_STROKE_PRECIS, DRAFT_QUALITY, VARIABLE_PITCH | FF_MODERN, _T("宋体") );
	CColorText RCtext(g_szRCServerName, new CRect(0, 0, 140, 17), RGB(236, 133, 0), &RCnameFont);
	m_ImageButtonRecommandServer.AddDisplayText(&RCtext);
	if(g_szRCServerName.GetLength() == 0)
	{
		m_ImageButtonRecommandServer.EnableWindow(FALSE);	
	}

	// 初始化服务协议条款
	m_bIsApplyProtocol = TRUE;
	this->GetDlgItem(IDC_BUTTON_APP_PROTOCOL)->MoveWindow(pXmlButtonApplyProtocol->IntAttribute("Left"), pXmlButtonApplyProtocol->IntAttribute("Top"), pXmlButtonApplyProtocol->IntAttribute("Width"), pXmlButtonApplyProtocol->IntAttribute("Height"));
	//m_ImageButtonApplyProtocol.InitButton(IDC_BUTTON_APP_PROTOCOL, this, CString(pXmlButtonApplyProtocol->Attribute("BGImage")), pXmlButtonApplyProtocol->IntAttribute("Width"), pXmlButtonApplyProtocol->IntAttribute("Height"), TRUE);
	m_ImageButtonApplyProtocol.InitButton(IDC_BUTTON_APP_PROTOCOL,this,CString(pXmlButtonApplyProtocol->Attribute("NormalImage")),CString(pXmlButtonApplyProtocol->Attribute("HoverImage")),CString(pXmlButtonApplyProtocol->Attribute("PressedImage")));
	m_ImageButtonApplyProtocol.SetIsCheckBox(TRUE);
	m_ImageButtonApplyProtocol.SetCheckBoxValue(TRUE);
	m_ImageButtonApplyProtocol.SetCheckBoxDisableImage(CString(pXmlButtonApplyProtocol->Attribute("PressedImage")),pXmlButtonApplyProtocol->IntAttribute("Left"), pXmlButtonApplyProtocol->IntAttribute("Top"), pXmlButtonApplyProtocol->IntAttribute("Width"), pXmlButtonApplyProtocol->IntAttribute("Height"));

	// 初始化服务器选择按钮
	m_crServerNameColor = RGB(255, 150, 0);

	this->GetDlgItem(IDC_BUTTON_SERVER_1)->MoveWindow(pXmlButtonServer1->IntAttribute("Left"), pXmlButtonServer1->IntAttribute("Top"), pXmlButtonServer1->IntAttribute("Width"), pXmlButtonServer1->IntAttribute("Height"));
	//m_ImageButtonServer[0].InitButton(IDC_BUTTON_SERVER_1, this, CString(pXmlButtonServer1->Attribute("BGImage")), pXmlButtonServer1->IntAttribute("Width"), pXmlButtonServer1->IntAttribute("Height"));
	m_ImageButtonServer[0].InitButton(IDC_BUTTON_SERVER_1,this,CString(pXmlButtonServer1->Attribute("NormalImage")),CString(pXmlButtonServer1->Attribute("HoverImage")),CString(pXmlButtonServer1->Attribute("PressedImage")));
	m_ImageButtonServer[0].SetIsCheckBox(TRUE);
	m_ImageButtonServer[0].SetCheckBoxDisableImage(CString(pXmlButtonServer1->Attribute("PressedImage")), pXmlButtonServer1->IntAttribute("Width")*3, 0, pXmlButtonServer1->IntAttribute("Width"), pXmlButtonServer1->IntAttribute("Height"));

	this->GetDlgItem(IDC_BUTTON_SERVER_2)->MoveWindow(pXmlButtonServer2->IntAttribute("Left"), pXmlButtonServer2->IntAttribute("Top"), pXmlButtonServer2->IntAttribute("Width"), pXmlButtonServer2->IntAttribute("Height"));
	//m_ImageButtonServer[1].InitButton(IDC_BUTTON_SERVER_2, this, CString(pXmlButtonServer2->Attribute("BGImage")), pXmlButtonServer2->IntAttribute("Width"), pXmlButtonServer2->IntAttribute("Height"));
	m_ImageButtonServer[1].InitButton(IDC_BUTTON_SERVER_2,this,CString(pXmlButtonServer2->Attribute("NormalImage")),CString(pXmlButtonServer2->Attribute("HoverImage")),CString(pXmlButtonServer2->Attribute("PressedImage")));
	m_ImageButtonServer[1].SetIsCheckBox(TRUE);
	m_ImageButtonServer[1].SetCheckBoxDisableImage(CString(pXmlButtonServer2->Attribute("PressedImage")), pXmlButtonServer2->IntAttribute("Width")*3, 0, pXmlButtonServer2->IntAttribute("Width"), pXmlButtonServer2->IntAttribute("Height"));

	this->GetDlgItem(IDC_BUTTON_SERVER_3)->MoveWindow(pXmlButtonServer3->IntAttribute("Left"), pXmlButtonServer3->IntAttribute("Top"), pXmlButtonServer3->IntAttribute("Width"), pXmlButtonServer3->IntAttribute("Height"));
	//m_ImageButtonServer[2].InitButton(IDC_BUTTON_SERVER_3, this, CString(pXmlButtonServer3->Attribute("BGImage")), pXmlButtonServer3->IntAttribute("Width"), pXmlButtonServer3->IntAttribute("Height"));
	m_ImageButtonServer[2].InitButton(IDC_BUTTON_SERVER_3,this,CString(pXmlButtonServer3->Attribute("NormalImage")),CString(pXmlButtonServer3->Attribute("HoverImage")),CString(pXmlButtonServer3->Attribute("PressedImage")));
	m_ImageButtonServer[2].SetIsCheckBox(TRUE);
	m_ImageButtonServer[2].SetCheckBoxDisableImage(CString(pXmlButtonServer3->Attribute("PressedImage")), pXmlButtonServer3->IntAttribute("Width")*3, 0, pXmlButtonServer3->IntAttribute("Width"), pXmlButtonServer3->IntAttribute("Height"));

	this->GetDlgItem(IDC_BUTTON_SERVER_4)->MoveWindow(pXmlButtonServer4->IntAttribute("Left"), pXmlButtonServer4->IntAttribute("Top"), pXmlButtonServer4->IntAttribute("Width"), pXmlButtonServer4->IntAttribute("Height"));
	//m_ImageButtonServer[3].InitButton(IDC_BUTTON_SERVER_4, this, CString(pXmlButtonServer4->Attribute("BGImage")), pXmlButtonServer4->IntAttribute("Width"), pXmlButtonServer4->IntAttribute("Height"));
	m_ImageButtonServer[3].InitButton(IDC_BUTTON_SERVER_4,this,CString(pXmlButtonServer4->Attribute("NormalImage")),CString(pXmlButtonServer4->Attribute("HoverImage")),CString(pXmlButtonServer4->Attribute("PressedImage")));
	m_ImageButtonServer[3].SetIsCheckBox(TRUE);
	m_ImageButtonServer[3].SetCheckBoxDisableImage(CString(pXmlButtonServer4->Attribute("PressedImage")), pXmlButtonServer4->IntAttribute("Width")*3, 0, pXmlButtonServer4->IntAttribute("Width"), pXmlButtonServer4->IntAttribute("Height"));

	/*this->GetDlgItem(IDC_BUTTON_SERVER_2)->MoveWindow(181, 76, 126, 53);
	m_ImageButtonServer[1].InitButton(IDC_BUTTON_SERVER_2, this, L"launcherbmp\\select.bmp", 126, 53);
	m_ImageButtonServer[1].SetIsCheckBox(TRUE);
	m_ImageButtonServer[1].SetCheckBoxDisableImage(L"launcherbmp\\select.bmp", 378, 0, 126, 53);

	this->GetDlgItem(IDC_BUTTON_SERVER_3)->MoveWindow(46, 134, 126, 53);
	m_ImageButtonServer[2].InitButton(IDC_BUTTON_SERVER_3, this, L"launcherbmp\\select.bmp", 126, 53);
	m_ImageButtonServer[2].SetIsCheckBox(TRUE);
	m_ImageButtonServer[2].SetCheckBoxDisableImage(L"launcherbmp\\select.bmp", 378, 0, 126, 53);

	this->GetDlgItem(IDC_BUTTON_SERVER_4)->MoveWindow(181, 134, 126, 53);
	m_ImageButtonServer[3].InitButton(IDC_BUTTON_SERVER_4, this, L"launcherbmp\\select.bmp", 126, 53);
	m_ImageButtonServer[3].SetIsCheckBox(TRUE);
	m_ImageButtonServer[3].SetCheckBoxDisableImage(L"launcherbmp\\select.bmp", 378, 0, 126, 53);*/

	// 设置服务器名字
	for (int i = 0;i < m_iMaxServerNum;++i)
	{
		m_ImageButtonServer[i].SetDisplayText(m_ServerName[i]);
		m_ImageButtonServer[i].SetDisplayTextColor(m_crServerNameColor);

		// 服务器名字字体
		CFont nameFont;
		nameFont.CreateFont(18, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, GB2312_CHARSET, OUT_STROKE_PRECIS, CLIP_STROKE_PRECIS, DRAFT_QUALITY, VARIABLE_PITCH | FF_MODERN, _T("黑体") );

		CColorText text(m_ServerName[i], new CRect(0, 0, 126, 53), m_crServerNameColor, &nameFont);
		text.SetShadow(RGB(0, 0, 0), 2);
		m_ImageButtonServer[i].AddDisplayText(&text);
	}

	// 初始化字体显示
	/*m_strRecommandLabel = L"推荐服务器:";
	m_rectRecommandLabel.SetRect(70, 45, 125, 57);
	m_crRecommandLabel = RGB(235, 130, 0);*/

	m_strSelectLabel = pXmlTextSelectLabel->Attribute("Text");
	m_rectSelectLabel.SetRect(pXmlTextSelectLabel->IntAttribute("Left"), pXmlTextSelectLabel->IntAttribute("Top"), pXmlTextSelectLabel->IntAttribute("Right"), pXmlTextSelectLabel->IntAttribute("Bottom"));
	m_crSelectLabel = RGB(pXmlTextSelectLabel->IntAttribute("R"), pXmlTextSelectLabel->IntAttribute("G"), pXmlTextSelectLabel->IntAttribute("B")); //RGB(240, 180 ,60);
	
	/*m_strRecommandName = L"";
	m_rectRecommandName.SetRect(160, 45, 310, 57);
	m_crRecommandName = RGB(255, 150, 0);*/

	m_strSelectName = pXmlTextSelectName->Attribute("Text");
	m_rectSelectName.SetRect(pXmlTextSelectName->IntAttribute("Left"), pXmlTextSelectName->IntAttribute("Top"), pXmlTextSelectName->IntAttribute("Right"), pXmlTextSelectName->IntAttribute("Bottom"));
	m_crSelectName = RGB(pXmlTextSelectName->IntAttribute("R"), pXmlTextSelectName->IntAttribute("G"), pXmlTextSelectName->IntAttribute("B")); //RGB(255, 150 ,0);

	// 设置默认服务器
	if (m_iDefaultServer >= m_iMaxServerNum)
	{
		m_iDefaultServer = 0;
	}
	if (m_iMaxServerNum > 0)
	{
		m_strRecommandName = m_ServerName[0];
		m_strSelectName = m_ServerName[m_iDefaultServer];
		m_ImageButtonServer[m_iDefaultServer].SetCheckBoxValue(TRUE);

		//strcpy(g_szPatchIp, m_pServerInfo[m_iDefaultServer].szPatchIp);
		//g_PatchPort = m_pServerInfo[m_iDefaultServer].PatchPort;
		//strcpy(g_szServerIp, m_pServerInfo[m_iDefaultServer].szServerIp);
		//g_ServerPort = m_pServerInfo[m_iDefaultServer].ServerPort;
	}
	
	// 屏蔽不能使用的服务器
	for (int index = 0;index < MAX_SERVER_NUMBER;++index)
	{
		if (index < m_iMaxServerNum)
		{
			m_ImageButtonServer[index].EnableWindow(TRUE);
		}
		else
		{
			m_ImageButtonServer[index].EnableWindow(FALSE);
		}
	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CServerDlg::SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent)
{
	// 获取配置信息
	CShapeConfig* pShapeConfig = ConfigManager::GetInstance()->GetShapeConfig(strBitmapFileName);
	vector<CRect*>& arrWorkRect = pShapeConfig->GetWorkRectList();

	// 定义工作区
	/*CRect arrWorkRect[5];
	int	iWorkRectNum = 5;
	arrWorkRect[0].SetRect(0, 0, 11, 292);
	arrWorkRect[1].SetRect(340, 0, 351, 292);
	arrWorkRect[2].SetRect(12, 0, 339, 14);
	arrWorkRect[3].SetRect(12, 232, 339, 239);
	arrWorkRect[4].SetRect(12, 287, 339, 292);*/


	// 定义变量
	CDC memDC;
	CBitmap* pBitmap;
	CBitmap* pOldMemBmp = NULL;
	COLORREF cl;
	CRect cRect;
	int x, y;
	CRgn wndRgn, rgnTemp;

	// 获得窗口大小
	GetWindowRect(&cRect);

	// 加载位图资源
	HBITMAP temp = (HBITMAP)LoadImage(NULL, pShapeConfig->GetImageName(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		pBitmap = CBitmap::FromHandle(temp);
		memDC.CreateCompatibleDC(pDC);
		pOldMemBmp = memDC.SelectObject(pBitmap);

		// 创建默认区域
		wndRgn.CreateRectRgn(0, 0, cRect.Width(), cRect.Height());

		// 建立窗口
		//for (x = 0;x <= cRect.Width();++x)
		//{
		//	for (y = 0;y <= cRect.Height();++y)
		//	{
		//		// 获取坐标处的像素值
		//		cl = memDC.GetPixel(x, y);

		//		// 扣除
		//		if (cl == crTransparent)
		//		{
		//			rgnTemp.CreateRectRgn(x, y, x+1, y+1);
		//			wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
		//			rgnTemp.DeleteObject();
		//		}
		//	}
		//}
		for(int nRectIndex = 0;nRectIndex < arrWorkRect.size();++nRectIndex)
		{
			CRect* rectWork = arrWorkRect[nRectIndex];

			for (x = rectWork->left;x <= rectWork->right;++x)
			{
				for (y = rectWork->top;y <= rectWork->bottom;++y)
				{
					// 获取坐标处的像素值
					cl = memDC.GetPixel(x, y);

					// 扣除
					if (cl == pShapeConfig->GetTransparentColor())
					{

						rgnTemp.CreateRectRgn(x, y, x+1, y+1);
						wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
						rgnTemp.DeleteObject();

					}
				}
			}
		}

		// 清除资源
		pBitmap->DeleteObject();
		memDC.SelectObject(pOldMemBmp);
		memDC.DeleteDC();

		// 指定窗口
		SetWindowRgn((HRGN)wndRgn, TRUE);
	}
}

BOOL CServerDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
			return TRUE;
		case VK_ESCAPE:
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

// 逻辑代码
void CServerDlg::PaintBk(CDC* pDC, CRect rect)
{
	CDC tempDC;
	CBitmap tempBmp;

	if (pDC != NULL)
	{
		tempDC.CreateCompatibleDC(pDC);
		tempBmp.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
		CBitmap* pOldBmp = tempDC.SelectObject(&tempBmp);

		tempDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);

		pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &tempDC, 0, 0, SRCCOPY);
		//pDC->FillSolidRect(&rect, RGB(255, 0, 0));

		tempBmp.DeleteObject();
		tempDC.SelectObject(pOldBmp);
		tempDC.DeleteDC();
	}
} // End of PaintBk

void CServerDlg::ShowText(CDC* dc, CString strTextValue, CRect rect, COLORREF crColor)
{
	// 重绘背景
	PaintBk(dc, rect);

	// 字体输出
	CFont font;
	font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体") );
	CFont* def_font = dc->SelectObject(&font);
	dc->SetBkMode(TRANSPARENT);
	dc->SetTextColor(crColor);
	dc->TextOut(rect.left, rect.top, strTextValue);

	dc->SelectObject(def_font);
	font.DeleteObject();
}

void CServerDlg::GetServerList(const char * czFileName)
{
	// 尝试读取文件
	int iIndex = 0;
	bool bReadSucceed = false;

	// 获得完整的路径名
	TCHAR tcCurrDir[MAX_PATH];
	::GetCurrentDirectory(MAX_PATH, tcCurrDir);
	//CString csCurrDir(tcCurrDir);
	USES_CONVERSION;
	//string strCurrDir(T2A(csCurrDir));

	char fullName[1024] = "\0";
	strcat_s(fullName, T2A(tcCurrDir));
	strcat_s(fullName, "\\Data\\Config\\");
	strcat_s(fullName, czFileName);

	TiXmlDocument* pXmlDoc = new TiXmlDocument();
	if ( pXmlDoc->LoadFile(fullName) )	// 首先尝试直接读取
	{
		bReadSucceed = true;
	}
	else
	{
		// 尝试从pkg中读取
		char* pBuf = CPackageSourceManager::GetInstance()->GetFileData(fullName);
		CPackageSourceManager::GetInstance()->Release();
		if (pBuf != NULL)
		{
			if (pXmlDoc->Parse(pBuf) != NULL)
			{
				bReadSucceed = true;
			}
			delete [] pBuf;
			pBuf = NULL;
		}
	}

	// 成功的话开始读取文件
	if (bReadSucceed)
	{
		TiXmlElement* pXmlRoot = pXmlDoc->RootElement(); // 获取根节点
		if (pXmlRoot != NULL)
		{
			TiXmlElement* pXmlLine = pXmlRoot;
			for (;pXmlLine;pXmlLine = pXmlLine->NextSiblingElement())
			{
				if (strcmp(pXmlLine->Value(), "Servers") == 0)	// 系统配置
				{
					for (TiXmlElement* pXmlConfigure = pXmlLine->FirstChildElement();pXmlConfigure;pXmlConfigure = pXmlConfigure->NextSiblingElement())
					{
						if (iIndex < MAX_SERVER_NUMBER)
						{
							// 如果有Name字段的话，显示名称
							const char* czName = pXmlConfigure->Attribute("Name");
							if (czName != NULL && strlen(czName) != 0)
							{
								m_ServerName[iIndex] += czName;
							}
							else
							{
								m_ServerName[iIndex] += pXmlConfigure->Attribute("IP");
								//m_ServerName[iIndex] += " : ";
								//m_ServerName[iIndex] += pXmlConfigure->Attribute("Port");
							}

							iIndex++;
						}
					}

					m_iMaxServerNum = iIndex;
				}
			}
		}
	}

	// 清除数据
	if (pXmlDoc != NULL)
	{
		pXmlDoc->Clear();
		delete pXmlDoc;
		pXmlDoc = NULL;
	}
}

void CServerDlg::GetServerList()
{
	unsigned char ucNum;
	m_pServerInfo = m_PatchCliIntf->GetCatInfo(ucNum);
	
	// 填写名称
	int iIndex;
	m_iMaxServerNum = -1;
	for (iIndex = 0;iIndex < ucNum;++iIndex)
	{
		if (iIndex < MAX_SERVER_NUMBER)
		{
			m_ServerName[iIndex] = m_pServerInfo[iIndex].szCatName;

			// 限定大小
			m_iMaxServerNum = iIndex;
		}
	}

	// 索引从0开始，所以数量为索引+1
	m_iMaxServerNum = m_iMaxServerNum + 1;
}

LRESULT CServerDlg::OnServerDlgUpdate(WPARAM wParam, LPARAM lParam)
{
	// 处于disable状态
	if(wParam == 1)
	{
		m_ImageButtonRecommandServer.EnableWindow(FALSE);
	}

	for (int i = 0;i < MAX_SERVER_NUMBER;++i)
	{
		if (i == m_iDefaultServer)
		{
			m_ImageButtonServer[i].SetCheckBoxValue(TRUE);
		}
		else
		{
			m_ImageButtonServer[i].SetCheckBoxValue(FALSE);
		}

		if(wParam == 1)
		{	
			m_ImageButtonServer[i].SetCheckBoxValue(FALSE);
			m_ImageButtonServer[i].EnableWindow(FALSE);
		}
	}

	this->InvalidateRect(m_rectSelectName);
	this->Invalidate();

	return 0;
}

BEGIN_MESSAGE_MAP(CServerDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_CONTROL_RANGE(BN_CLICKED, IDC_BUTTON_SERVER_1, IDC_BUTTON_SERVER_4, &CServerDlg::OnServerButtonClicked)
	ON_BN_CLICKED(IDC_BUTTON_APP_PROTOCOL, &CServerDlg::OnBnClickedButtonAppProtocol)
	ON_WM_LBUTTONDOWN()
	ON_MESSAGE(WM_SERVER_DLG_UPDATE, OnServerDlgUpdate)
	ON_BN_CLICKED(IDC_BUTTON_RECOMMAND_SERVER, &CServerDlg::OnBnClickedButtonRecommandServer)
END_MESSAGE_MAP()


// CServerDlg 消息处理程序

BOOL CServerDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_pBkgndImage == NULL)
	{
		HBITMAP temp = (HBITMAP)LoadImage(NULL, m_strBkgndImageName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		if (temp != NULL)
		{
			m_pBkgndImage = CBitmap::FromHandle(temp);
		}
		else
		{
			m_pBkgndImage = NULL;
		}
	}

	if (m_pBkgndImage != NULL)
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		BITMAP hBitmap;
		m_pBkgndImage->GetBitmap(&hBitmap);

		CBitmap* pOldBmp = memDC.SelectObject(m_pBkgndImage);

		//pDC->BitBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, SRCCOPY);
		pDC->TransparentBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, hBitmap.bmWidth, hBitmap.bmHeight, RGB(255, 0, 255));

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();


		//ShowText(pDC, m_strRecommandLabel, m_rectRecommandLabel, m_crRecommandLabel);
		ShowText(pDC, m_strSelectLabel, m_rectSelectLabel, m_crSelectLabel);
		//ShowText(pDC, m_strRecommandName, m_rectRecommandName, m_crRecommandName);
		ShowText(pDC, g_szServerName, m_rectSelectName, m_crSelectName);
		
		m_ImageButtonApplyProtocol.Invalidate();
		m_ImageButtonServer[0].Invalidate();
		m_ImageButtonServer[1].Invalidate();
		m_ImageButtonServer[2].Invalidate();
		m_ImageButtonServer[3].Invalidate();

		return TRUE;
	}
	else
	{
		CDialog::OnEraseBkgnd(pDC);
		
		CRect rect;
		this->GetClientRect(&rect);
		pDC->FillSolidRect(&rect, RGB(100, 100, 100));
		//ShowText(pDC, m_strRecommandLabel, m_rectRecommandLabel, m_crRecommandLabel);
		ShowText(pDC, m_strSelectLabel, m_rectSelectLabel, m_crSelectLabel);
		//ShowText(pDC, m_strRecommandName, m_rectRecommandName, m_crRecommandName);
		ShowText(pDC, g_szServerName, m_rectSelectName, m_crSelectName);

		return TRUE;
	}
}

void CServerDlg::OnServerButtonClicked(UINT uID)
{
	int iSelectIndex = uID - IDC_BUTTON_SERVER_1;

	// 如果名字为空，说明是无用的服务器，不做处理
	if (m_ServerName[iSelectIndex].GetLength() > 0)
	{
		//m_iDefaultServer = iSelectIndex;
		//strcpy(g_szPatchIp, m_pServerInfo[m_iDefaultServer].szPatchIp);
		//g_PatchPort = m_pServerInfo[m_iDefaultServer].PatchPort;
		//strcpy(g_szServerIp, m_pServerInfo[m_iDefaultServer].szServerIp);
		//g_ServerPort = m_pServerInfo[m_iDefaultServer].ServerPort;

		// 界面处理
		for (int i = 0;i < MAX_SERVER_NUMBER;++i)
		{
			if (i == iSelectIndex)
			{
				m_ImageButtonServer[i].SetCheckBoxValue(TRUE);
			}
			else
			{
				m_ImageButtonServer[i].SetCheckBoxValue(FALSE);
			}
		}
	}

	//// 界面处理
	//for (int i = 0;i < MAX_SERVER_NUMBER;++i)
	//{
	//	if (i == m_iDefaultServer)
	//	{
	//		m_ImageButtonServer[i].SetCheckBoxValue(TRUE);
	//	}
	//	else
	//	{
	//		m_ImageButtonServer[i].SetCheckBoxValue(FALSE);
	//	}
	//}

	//m_strSelectName = m_ServerName[m_iDefaultServer];
	//this->InvalidateRect(m_rectSelectName);

	// 弹出选择对话框
	if (m_pSelectDlg == NULL)
	{
		m_pSelectDlg = new CSelectDlg(this);
		m_pSelectDlg->Create(IDD_SELECT_DIALOG, this);
	}

	m_pSelectDlg->ShowSelect(iSelectIndex, m_pServerInfo[iSelectIndex].nCatID, g_uiAreaID, g_uiServerID);
}

void CServerDlg::OnBnClickedButtonAppProtocol()
{
	// TODO: 在此添加控件通知处理程序代码
	m_bIsApplyProtocol = m_ImageButtonApplyProtocol.GetCheckBoxValue();
}

void CServerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	//禁止显示移动矩形窗体框
	::SystemParametersInfo(SPI_SETDRAGFULLWINDOWS,TRUE,NULL,0);
	//非标题栏移动整个窗口
	m_pParentWnd->SendMessage(WM_SYSCOMMAND,0xF012,0); 

	CDialog::OnLButtonDown(nFlags, point);
}

void CServerDlg::OnBnClickedButtonRecommandServer()
{
	// TODO: 在此添加控件通知处理程序代码
	ConfigManager::GetInstance()->SetOutPutData(g_uiRCCatID, g_uiRCAreaID, g_uiRCServerID);
	OnServerDlgUpdate(0, 0);
}
