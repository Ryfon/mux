﻿#pragma once

#define MAX_SELECT_AREA_NUM		10
#define MAX_SELECT_SERVER_NUM	12

#define AREA_NAME_COLOR			RGB(166, 169, 151)
#define SERVER_NAME_COLOR		RGB(255, 255, 255)
#define SERVER_RED				RGB(209, 15, 15)
#define SERVER_YELLOW			RGB(248, 207, 41)
#define SERVER_GREE				RGB(107, 224, 76)
#define SERVER_GRAY				RGB(120, 122, 114)

#include "PatchCliIntf.h"

// CSelectDlg 对话框

class CSelectDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectDlg)

public:
	CSelectDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSelectDlg();

// 对话框数据
	enum { IDD = IDD_SELECT_DIALOG };

// 成员函数
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	void SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent);
	BOOL MyLoadImage(CString strFileName, CBitmap ** pImage, CRect* rect, int iPosX, int iPosY);
	BOOL MyShowImage(CDC* dc, CBitmap* pImage, CRect* rect, COLORREF crTransparent);
	int InitAreaList(int iCatId, int iAreaId);
	int InitServerList(int iAreaId, int iServerId);
	void SetServerButton(int index, int InfoIndex);
private:
	LRESULT OnButtonDrag(WPARAM wParam, LPARAM lParam);
	LRESULT OnBarLButtonUp(WPARAM wParam, LPARAM lParam);
	LRESULT OnBarLBLongClick(WPARAM wParam, LPARAM lParam);
	int GetTopIndexByBarPos(int pos);
	void MoveBarByTopIndex(int top);
	void MoveListByTopIndex(int top);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

public:
	void ShowSelect(int iCatIndex, int iCatId, int iAreaId, int iServerId);
// 成员变量
private:
	CWnd*		m_pRealParent;
	CBitmap*		m_pBkgndImage;
	CString			m_strBkgndImageName;
	int				m_barTop;
	int				m_barBottom;
	vector<int>		m_barPosList;
	vector<CPoint>	m_barRangeList;
	int				m_iTopIndex;
private:
	CFourBitmapButton	m_ImageButtonOK;
	CFourBitmapButton	m_ImageButtonCancel;
	CFourBitmapButton	m_ImageButtonClose;

	CFourBitmapButton	m_AreaButtonList[MAX_SELECT_AREA_NUM];
	UINT				m_AreaButtonIDList[MAX_SELECT_AREA_NUM];
	
	CFourBitmapButton	m_ServerButtonList[MAX_SELECT_SERVER_NUM];
	UINT				m_ServerButtonIDList[MAX_SELECT_SERVER_NUM];

	CBitmap*			m_pImageScrollBar;
	CRect				m_rectScrollBar;

	CFourBitmapButton	m_ImageButtonScrollUp;
	CFourBitmapButton	m_ImageButtonScrollBar;
	CFourBitmapButton	m_ImageButtonScrollDown;
private:
	CPatchCliIntf*	m_PatchCliIntf;
	PAREAINFO		m_pAreaInfo;
	PSERVERINFO		m_pServerInfo;
	unsigned char	m_ucServerNum;

	int				m_iSelectCatID;
	int				m_iSelectAreaID;
	int				m_iSelectServerID;
	CString			m_strSelectServerName;
	int				m_iSelectCatIndex;
	int				m_iSelectAreaIndex;
	int				m_iSelectServerIndex;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonSelectOk();
	afx_msg void OnBnClickedButtonSelectCancel();
	afx_msg void OnBnClickedButtonSelectScrollUp();
	afx_msg void OnBnClickedButtonSelectScrollDown();
	afx_msg void OnServerButtonClicked(UINT uID);
	afx_msg void OnAreaButtonClicked(UINT uID);
	afx_msg void OnClose();
	afx_msg void OnServerButtonDoubleclicked(UINT uID);
	afx_msg void OnBnClickedButtonSelectClose();
};
