﻿// ImageComboBox.cpp : 实现文件
//

#include "stdafx.h"
#include "ImageComboBox.h"


// CImageComboBox

IMPLEMENT_DYNAMIC(CImageComboBox, CComboBox)

CImageComboBox::CImageComboBox()
{
	m_pBkgndImage = NULL;
	m_strBKImageName = L"";

	m_iFontHeight = 14;
	m_strBKImageName = L"黑体";

	m_crFontColor = RGB(0, 0, 0);
	m_crListBKColor = RGB(255, 255, 255);
	m_crListHiliteColor = RGB(49, 106, 197);
}

CImageComboBox::~CImageComboBox()
{
}

void CImageComboBox::PreSubclassWindow()
{
	if (m_pBkgndImage == NULL)
	{
		ModifyStyle(CBS_OWNERDRAWFIXED, NULL);
	}

	if (GetWindowLong(m_hWnd, GWL_STYLE) & CBS_HASSTRINGS)
	{
		if (GetWindowLong(m_hWnd, GWL_STYLE) & CBS_OWNERDRAWFIXED)
		{
			::SendMessage(m_hWnd, CB_SETITEMHEIGHT, (WPARAM)-1, 12L);
		}
	}

	CComboBox::PreSubclassWindow();
}

BOOL CImageComboBox::InitComboBox(UINT nID, CWnd* pParent, CString strImage /* = L */, COLORREF crListBKColor /* = RGB */, COLORREF crFontColor /* = RGB */, int iFontHeight /* = 14 */, CString strFontFaceName /* = L */, COLORREF crListHiliteColor /* = RGB */)
{
	if (SetBKImage(strImage))
	{
		SetListBKColor(crListBKColor);
		SetFontColor(crFontColor);
		SetFontHeight(iFontHeight);
		SetFontFaceName(strFontFaceName);
		SetListHiliteColor(crListHiliteColor);
		
		SubclassDlgItem(nID, pParent);

		return TRUE;
	}
	else
	{
		SubclassDlgItem(nID, pParent);

		return FALSE;
	}
}

BOOL CImageComboBox::SetBKImage(CString strFileName)
{
	// 保存文件名
	m_strBKImageName = strFileName;

	// 加载文件
	return LoadBKImage(strFileName);
}

void CImageComboBox::SetFontHeight(int height)
{
	m_iFontHeight = height;
}

void CImageComboBox::SetFontFaceName(CString strFaceName)
{
	m_strFontFaceName = strFaceName;
}
void CImageComboBox::SetFontColor(COLORREF color)
{
	m_crFontColor = color;
}

void CImageComboBox::SetListBKColor(COLORREF color)
{
	m_crListBKColor = color;
}

void CImageComboBox::SetListHiliteColor(COLORREF color)
{
	m_crListHiliteColor = color;
}

BOOL CImageComboBox::LoadBKImage(CString strFileName)
{
	HBITMAP temp = (HBITMAP)LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		m_pBkgndImage = CBitmap::FromHandle(temp);
		return TRUE;
	}
	else
	{
		m_pBkgndImage = NULL;
		return FALSE;
	}
}

BOOL CImageComboBox::DrawBKImage(CDC* pDC)
{
	if (m_pBkgndImage != NULL)
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		BITMAP hBitmap;
		m_pBkgndImage->GetBitmap(&hBitmap);

		CBitmap* pOldBmp = memDC.SelectObject(m_pBkgndImage);

		pDC->BitBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, SRCCOPY);

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();

		return TRUE;
	}
	else
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		CRect rect;
		GetClientRect(&rect);
		pDC->FillSolidRect(&rect, m_crListBKColor);

		memDC.DeleteDC();

		return FALSE;
	}
}

void CImageComboBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType != ODT_COMBOBOX)
	{
		return;
	}

	CRect rect(lpDrawItemStruct->rcItem);
	
	CDC dc;
	dc.Attach(lpDrawItemStruct->hDC);

	int index = lpDrawItemStruct->itemID;
	CString strText;
	GetLBText(index, strText);

	if (lpDrawItemStruct->itemState & ODS_FOCUS)
	{
		dc.FillSolidRect(&rect, m_crListHiliteColor);
	}
	else
	{
		dc.FillSolidRect(&rect, m_crListBKColor);
	}

	DrawFontText(&dc, &rect, strText);

	dc.Detach();
}

BOOL CImageComboBox::DrawFontText(CDC* pDC, CRect* rect, CString strText)
{
	CFont font;
	font.CreateFont(m_iFontHeight, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, m_strFontFaceName);
	CFont* def_font = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(m_crFontColor);
	pDC->DrawText(strText, rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	pDC->SelectObject(def_font);
	font.DeleteObject();

	return TRUE;
}

BEGIN_MESSAGE_MAP(CImageComboBox, CComboBox)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
	ON_CONTROL_REFLECT(CBN_SETFOCUS, &CImageComboBox::OnCbnSetfocus)
	ON_CONTROL_REFLECT(CBN_SELCHANGE, &CImageComboBox::OnCbnSelchange)
	ON_WM_KILLFOCUS()
END_MESSAGE_MAP()



// CImageComboBox 消息处理程序


BOOL CImageComboBox::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (DrawBKImage(pDC))
	{
		return TRUE;
	}
	else
	{
		return CComboBox::OnEraseBkgnd(pDC);
	}
}

HBRUSH CImageComboBox::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CComboBox::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何属性

	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	switch(nCtlColor)
	{
	case CTLCOLOR_EDIT:
		pDC->SetTextColor(m_crFontColor);
		pDC->SetBkColor(m_crListBKColor);
		hbr = ::CreateSolidBrush(m_crListBKColor);
		break;
	case CTLCOLOR_LISTBOX:
		pDC->SetTextColor(m_crFontColor);
		pDC->SetBkColor(m_crListBKColor);
		hbr = ::CreateSolidBrush(m_crListBKColor);
	    break;
	default:
	    break;
	}
	

	return hbr;
}

void CImageComboBox::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 CComboBox::OnPaint()
	
	// 背景
	DrawBKImage(&dc);
	
	// 字体
	CRect rect;
	this->GetClientRect(&rect);
	CString strText;
	this->GetWindowText(strText);
	//strText = L" " + strText;

	DrawFontText(&dc, &rect, strText);
}

void CImageComboBox::OnCbnSetfocus()
{
	// TODO: 在此添加控件通知处理程序代码
	this->Invalidate();
}

void CImageComboBox::OnCbnSelchange()
{
	// TODO: 在此添加控件通知处理程序代码
	this->Invalidate();
}

void CImageComboBox::OnKillFocus(CWnd* pNewWnd)
{
	//CComboBox::OnKillFocus(pNewWnd);

	// TODO: 在此处添加消息处理程序代码
}
