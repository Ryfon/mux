﻿// SettingDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "SettingDlg.h"


// CSettingDlg 对话框

IMPLEMENT_DYNAMIC(CSettingDlg, CDialog)

CSettingDlg::CSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSettingDlg::IDD, pParent)
	, m_iStandByValue(0)
	, m_iCPUValue(0)
	, m_iNetWorkValue(0)
{
	m_pBkgndImage = NULL;
	m_pImageHardware = NULL;
	m_pImageLine = NULL;
	m_pImageResolution = NULL;
	m_pImageTerrian = NULL;
	m_pImageEffect = NULL;
	m_pImageShadow = NULL;
}

CSettingDlg::~CSettingDlg()
{
}

void CSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SETTING_STANDBY, m_iStandByValue);
	DDV_MinMaxInt(pDX, m_iStandByValue, 1, 60);
	DDX_Text(pDX, IDC_EDIT_SETTING_CPU, m_iCPUValue);
	DDV_MinMaxInt(pDX, m_iCPUValue, 1, 80);
	DDX_Text(pDX, IDC_EDIT_SETTING_NETWORK, m_iNetWorkValue);
	DDV_MinMaxInt(pDX, m_iNetWorkValue, 1, 80);
	DDX_Control(pDX, IDC_EDIT_SETTING_STANDBY, m_editStandBy);
	DDX_Control(pDX, IDC_EDIT_SETTING_CPU, m_editCPU);
	DDX_Control(pDX, IDC_EDIT_SETTING_NETWORK, m_editNetWork);
}

BOOL CSettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// 获取UI配置
	CDialogConfig* pDialogConfig = ConfigManager::GetInstance()->GetDialogConfig(_T("SettingDlg"));
	TiXmlElement* pXmlDialogConfig = pDialogConfig->GetConfig();
	TiXmlElement* pXmlButtonOk = pDialogConfig->GetChildConfig(_T("Ok"));
	TiXmlElement* pXmlButtonCancel = pDialogConfig->GetChildConfig(_T("Cancel"));
	TiXmlElement* pXmlButtonClose = pDialogConfig->GetChildConfig(_T("Close"));
	TiXmlElement* pXmlButtonTabConfig = pDialogConfig->GetChildConfig(_T("TabConfig"));
	TiXmlElement* pXmlButtonTabUpdate = pDialogConfig->GetChildConfig(_T("TabUpdate"));

	// 初始化自身
	//this->MoveWindow(0, 0, 471, 491);
	m_strBkgndImageName = pXmlDialogConfig->Attribute("BGImage");
	this->MoveWindow(pXmlDialogConfig->IntAttribute("Left"), pXmlDialogConfig->IntAttribute("Top"), pXmlDialogConfig->IntAttribute("Width"), pXmlDialogConfig->IntAttribute("Height"));
	this->CenterWindow();

	// 初始化硬件配置
	InitHardwareConfig();

	// 初始化按钮
	/*this->GetDlgItem(IDC_BUTTON_SETTING_OK)->MoveWindow(145, 451, 73, 23);
	m_ImageButtonOK.InitButton(IDC_BUTTON_SETTING_OK, this, L"launcherset\\btn_confirm.bmp", 73, 23);
	
	this->GetDlgItem(IDC_BUTTON_SETTING_CANCEL)->MoveWindow(251, 451, 73, 23);
	m_ImageButtonCancel.InitButton(IDC_BUTTON_SETTING_CANCEL, this, L"launcherset\\btn_cancel.bmp", 73, 23);
	
	this->GetDlgItem(IDC_BUTTON_SETTING_CLOSE)->MoveWindow(435, 10, 20, 19);
	m_ImageButtonClose.InitButton(IDC_BUTTON_SETTING_CLOSE, this, _T("launcherbmp\\btn_close.bmp"), 20, 19);

	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_CONFIG)->MoveWindow(27, 15, 86, 27);
	m_ImageButtonGameConfig.InitButton(IDC_BUTTON_SETTING_TAB_CONFIG, this, L"launcherset\\tagbtn_seting.bmp", 86, 27);
	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_CONFIG)->EnableWindow(FALSE);

	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_UPDATE)->MoveWindow(115, 15, 86, 27);
	m_ImageButtonAutoUpdate.InitButton(IDC_BUTTON_SETTING_TAB_UPDATE, this, L"launcherset\\tagbtn_updata.bmp", 86, 27);*/
	this->GetDlgItem(IDC_BUTTON_SETTING_OK)->MoveWindow(pXmlButtonOk->IntAttribute("Left"), pXmlButtonOk->IntAttribute("Top"), pXmlButtonOk->IntAttribute("Width"), pXmlButtonOk->IntAttribute("Height"));
	//m_ImageButtonOK.InitButton(IDC_BUTTON_SETTING_OK, this, CString(pXmlButtonOk->Attribute("BGImage")), pXmlButtonOk->IntAttribute("Width"), pXmlButtonOk->IntAttribute("Height"));
	m_ImageButtonOK.InitButton(IDC_BUTTON_SETTING_OK,this,CString(pXmlButtonOk->Attribute("NormalImage")),CString(pXmlButtonOk->Attribute("HoverImage")),CString(pXmlButtonOk->Attribute("PressedImage")));

	this->GetDlgItem(IDC_BUTTON_SETTING_CANCEL)->MoveWindow(pXmlButtonCancel->IntAttribute("Left"), pXmlButtonCancel->IntAttribute("Top"), pXmlButtonCancel->IntAttribute("Width"), pXmlButtonCancel->IntAttribute("Height"));
	//m_ImageButtonCancel.InitButton(IDC_BUTTON_SETTING_CANCEL, this, CString(pXmlButtonCancel->Attribute("BGImage")), pXmlButtonCancel->IntAttribute("Width"), pXmlButtonCancel->IntAttribute("Height"));
	m_ImageButtonCancel.InitButton(IDC_BUTTON_SETTING_CANCEL,this,CString(pXmlButtonCancel->Attribute("NormalImage")),CString(pXmlButtonCancel->Attribute("HoverImage")),CString(pXmlButtonCancel->Attribute("PressedImage")));

	this->GetDlgItem(IDC_BUTTON_SETTING_CLOSE)->MoveWindow(pXmlButtonClose->IntAttribute("Left"), pXmlButtonClose->IntAttribute("Top"), pXmlButtonClose->IntAttribute("Width"), pXmlButtonClose->IntAttribute("Height"));
	//m_ImageButtonClose.InitButton(IDC_BUTTON_SETTING_CLOSE, this, CString(pXmlButtonClose->Attribute("BGImage")), pXmlButtonClose->IntAttribute("Width"), pXmlButtonClose->IntAttribute("Height"));
	m_ImageButtonClose.InitButton(IDC_BUTTON_SETTING_CLOSE,this,CString(pXmlButtonClose->Attribute("NormalImage")),CString(pXmlButtonClose->Attribute("HoverImage")),CString(pXmlButtonClose->Attribute("PressedImage")));

	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_CONFIG)->MoveWindow(pXmlButtonTabConfig->IntAttribute("Left"), pXmlButtonTabConfig->IntAttribute("Top"), pXmlButtonTabConfig->IntAttribute("Width"), pXmlButtonTabConfig->IntAttribute("Height"));
	//m_ImageButtonGameConfig.InitButton(IDC_BUTTON_SETTING_TAB_CONFIG, this, CString(pXmlButtonTabConfig->Attribute("BGImage")), pXmlButtonTabConfig->IntAttribute("Width"), pXmlButtonTabConfig->IntAttribute("Height"));
	m_ImageButtonGameConfig.InitButton(IDC_BUTTON_SETTING_TAB_CONFIG,this,CString(pXmlButtonTabConfig->Attribute("NormalImage")),CString(pXmlButtonTabConfig->Attribute("HoverImage")),CString(pXmlButtonTabConfig->Attribute("PressedImage")),CString(pXmlButtonTabConfig->Attribute("DisableImage")));
	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_CONFIG)->EnableWindow(FALSE);

	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_UPDATE)->MoveWindow(pXmlButtonTabUpdate->IntAttribute("Left"), pXmlButtonTabUpdate->IntAttribute("Top"), pXmlButtonTabUpdate->IntAttribute("Width"), pXmlButtonTabUpdate->IntAttribute("Height"));
	//m_ImageButtonAutoUpdate.InitButton(IDC_BUTTON_SETTING_TAB_UPDATE, this, CString(pXmlButtonTabUpdate->Attribute("BGImage")), pXmlButtonTabUpdate->IntAttribute("Width"), pXmlButtonTabUpdate->IntAttribute("Height"));
	m_ImageButtonAutoUpdate.InitButton(IDC_BUTTON_SETTING_TAB_UPDATE,this,CString(pXmlButtonTabUpdate->Attribute("NormalImage")),CString(pXmlButtonTabUpdate->Attribute("HoverImage")),CString(pXmlButtonTabUpdate->Attribute("PressedImage")),CString(pXmlButtonTabUpdate->Attribute("DisableImage")));

	// 初始化GameConfig
	InitGameConfig();

	// 初始化AutoUpdate
	InitAutoUpdate();

	// 显示控制
	VisableGameConfig(SW_SHOW);
	VisableAutoUpdate(SW_HIDE);

	// 最后调整形状
	this->ShowWindow(SW_SHOW);
	this->UpdateWindow();
	//SetupRegion(GetDC(), L"launcherset\\setbg.bmp", RGB(255, 0, 255));
	SetupRegion(GetDC(), pDialogConfig->GetShapeName(), RGB(255, 0, 255));

	// 显示游戏配置
	ShowConfigure();

	// 显示硬件配置
	GetHardwareConfig();

	this->Invalidate();

	return TRUE;
}

void CSettingDlg::InitGameConfig()
{
	// 获取UI配置
	CDialogConfig* pDialogConfig = ConfigManager::GetInstance()->GetDialogConfig(_T("SettingDlg"));
	TiXmlElement* pXmlDialogConfig = pDialogConfig->GetConfig();
	
	TiXmlElement* pXmlButtonRecommend = pDialogConfig->GetChildConfig(_T("Recommend"));
	TiXmlElement* pXmlButtonWindow = pDialogConfig->GetChildConfig(_T("Window"));
	TiXmlElement* pXmlButtonVS = pDialogConfig->GetChildConfig(_T("VS"));
	TiXmlElement* pXmlButtonNoSound = pDialogConfig->GetChildConfig(_T("NoSound"));
	
	TiXmlElement* pXmlComboResolution = pDialogConfig->GetChildConfig(_T("ComboResolution"));
	TiXmlElement* pXmlComboTerrian = pDialogConfig->GetChildConfig(_T("ComboTerrian"));
	TiXmlElement* pXmlComboEffect = pDialogConfig->GetChildConfig(_T("ComboEffect"));
	TiXmlElement* pXmlComboShadow = pDialogConfig->GetChildConfig(_T("ComboShadow"));
	
	TiXmlElement* pXmlListColor = pDialogConfig->GetChildConfig(_T("ListColor"));
	TiXmlElement* pXmlFontColor = pDialogConfig->GetChildConfig(_T("FontColor"));
	TiXmlElement* pXmlListHiliteColor = pDialogConfig->GetChildConfig(_T("ListHiliteColor"));
	COLORREF crListColor = RGB(pXmlListColor->IntAttribute("R"), pXmlListColor->IntAttribute("G"), pXmlListColor->IntAttribute("B"));
	COLORREF crFontColor = RGB(pXmlFontColor->IntAttribute("R"), pXmlFontColor->IntAttribute("G"), pXmlFontColor->IntAttribute("B"));
	COLORREF crListHiliteColor = RGB(pXmlListHiliteColor->IntAttribute("R"), pXmlListHiliteColor->IntAttribute("G"), pXmlListHiliteColor->IntAttribute("B"));

	TiXmlElement* pXmlImageHardware = pDialogConfig->GetChildConfig(_T("ImageHardware"));
	TiXmlElement* pXmlImageLine = pDialogConfig->GetChildConfig(_T("ImageLine"));
	TiXmlElement* pXmlImageResolution = pDialogConfig->GetChildConfig(_T("ImageResolution"));
	TiXmlElement* pXmlImageTerrian = pDialogConfig->GetChildConfig(_T("ImageTerrian"));
	TiXmlElement* pXmlImageEffect = pDialogConfig->GetChildConfig(_T("ImageEffect"));
	TiXmlElement* pXmlImageShadow = pDialogConfig->GetChildConfig(_T("ImageShadow"));

	// 初始化按钮
	/*this->GetDlgItem(IDC_BUTTON_SETTING_RECOMMAND)->MoveWindow(44, 402, 93, 23);
	m_ImageButtonRecommand.InitButton(IDC_BUTTON_SETTING_RECOMMAND, this, L"launcherset\\btn_autoconfig.bmp", 93, 23);*/
	this->GetDlgItem(IDC_BUTTON_SETTING_RECOMMAND)->MoveWindow(pXmlButtonRecommend->IntAttribute("Left"), pXmlButtonRecommend->IntAttribute("Top"), pXmlButtonRecommend->IntAttribute("Width"), pXmlButtonRecommend->IntAttribute("Height"));
	//m_ImageButtonRecommand.InitButton(IDC_BUTTON_SETTING_RECOMMAND, this, CString(pXmlButtonRecommend->Attribute("BGImage")), pXmlButtonRecommend->IntAttribute("Width"), pXmlButtonRecommend->IntAttribute("Height"));
	m_ImageButtonRecommand.InitButton(IDC_BUTTON_SETTING_RECOMMAND,this,CString(pXmlButtonRecommend->Attribute("NormalImage")),CString(pXmlButtonRecommend->Attribute("HoverImage")),CString(pXmlButtonRecommend->Attribute("PressedImage")));

	// 初始化CheckBox
	/*this->GetDlgItem(IDC_BUTTON_SETTING_WINDOW)->MoveWindow(45, 279, 66, 14);
	m_ImageButtonWindow.InitButton(IDC_BUTTON_SETTING_WINDOW, this, L"launcherset\\checkbox_window.bmp", 66, 14, TRUE);

	this->GetDlgItem(IDC_BUTTON_SETTING_VS)->MoveWindow(165, 279, 66, 14);
	m_ImageButtonVS.InitButton(IDC_BUTTON_SETTING_VS, this, L"launcherset\\checkbox_rsync.bmp",66, 14, TRUE);

	this->GetDlgItem(IDC_BUTTON_SETTING_NO_SOUND)->MoveWindow(45, 373, 66, 14);
	m_ImageButtonNoSound.InitButton(IDC_BUTTON_SETTING_NO_SOUND, this, L"launcherset\\checkbox_nosound.bmp", 66, 14, TRUE);*/
	this->GetDlgItem(IDC_BUTTON_SETTING_WINDOW)->MoveWindow(pXmlButtonWindow->IntAttribute("Left"), pXmlButtonWindow->IntAttribute("Top"), pXmlButtonWindow->IntAttribute("Width"), pXmlButtonWindow->IntAttribute("Height"));
	//m_ImageButtonWindow.InitButton(IDC_BUTTON_SETTING_WINDOW, this, CString(pXmlButtonWindow->Attribute("BGImage")), pXmlButtonWindow->IntAttribute("Width"), pXmlButtonWindow->IntAttribute("Height"), TRUE);
	m_ImageButtonWindow.InitButton(IDC_BUTTON_SETTING_WINDOW,this,CString(pXmlButtonWindow->Attribute("NormalImage")),CString(pXmlButtonWindow->Attribute("HoverImage")),CString(pXmlButtonWindow->Attribute("PressedImage")));
	m_ImageButtonWindow.SetIsCheckBox(TRUE);
	m_ImageButtonWindow.SetCheckBoxDisableImage(CString(pXmlButtonWindow->Attribute("PressedImage")),pXmlButtonWindow->IntAttribute("Left"), pXmlButtonWindow->IntAttribute("Top"), pXmlButtonWindow->IntAttribute("Width"), pXmlButtonWindow->IntAttribute("Height"));

	this->GetDlgItem(IDC_BUTTON_SETTING_VS)->MoveWindow(pXmlButtonVS->IntAttribute("Left"), pXmlButtonVS->IntAttribute("Top"), pXmlButtonVS->IntAttribute("Width"), pXmlButtonVS->IntAttribute("Height"));
	//m_ImageButtonVS.InitButton(IDC_BUTTON_SETTING_VS, this, CString(pXmlButtonVS->Attribute("BGImage")), pXmlButtonVS->IntAttribute("Width"), pXmlButtonVS->IntAttribute("Height"), TRUE);
	m_ImageButtonVS.InitButton(IDC_BUTTON_SETTING_VS,this,CString(pXmlButtonVS->Attribute("NormalImage")),CString(pXmlButtonVS->Attribute("HoverImage")),CString(pXmlButtonVS->Attribute("PressedImage")));
	m_ImageButtonVS.SetIsCheckBox(TRUE);
	m_ImageButtonVS.SetCheckBoxDisableImage(CString(pXmlButtonVS->Attribute("PressedImage")),pXmlButtonVS->IntAttribute("Left"), pXmlButtonVS->IntAttribute("Top"), pXmlButtonVS->IntAttribute("Width"), pXmlButtonVS->IntAttribute("Height"));

	this->GetDlgItem(IDC_BUTTON_SETTING_NO_SOUND)->MoveWindow(pXmlButtonNoSound->IntAttribute("Left"), pXmlButtonNoSound->IntAttribute("Top"), pXmlButtonNoSound->IntAttribute("Width"), pXmlButtonNoSound->IntAttribute("Height"));
	//m_ImageButtonNoSound.InitButton(IDC_BUTTON_SETTING_NO_SOUND, this, CString(pXmlButtonNoSound->Attribute("BGImage")), pXmlButtonNoSound->IntAttribute("Width"), pXmlButtonNoSound->IntAttribute("Height"), TRUE);
	m_ImageButtonNoSound.InitButton(IDC_BUTTON_SETTING_NO_SOUND,this,CString(pXmlButtonNoSound->Attribute("NormalImage")),CString(pXmlButtonNoSound->Attribute("HoverImage")),CString(pXmlButtonNoSound->Attribute("PressedImage")));
	m_ImageButtonNoSound.SetIsCheckBox(TRUE);
	m_ImageButtonNoSound.SetCheckBoxDisableImage(CString(pXmlButtonNoSound->Attribute("PressedImage")),pXmlButtonNoSound->IntAttribute("Left"), pXmlButtonNoSound->IntAttribute("Top"), pXmlButtonNoSound->IntAttribute("Width"), pXmlButtonNoSound->IntAttribute("Height"));

	// 初始化下拉框
	//this->GetDlgItem(IDC_COMBO_SETTING_RESOLUTION)->MoveWindow(100, 246, 126, 18);
	//m_comboboxResolution.InitComboBox(IDC_COMBO_SETTING_RESOLUTION, this, L"launcherset\\select_btn.bmp", RGB(48, 49, 42), RGB(133, 134, 128), 14, L"黑体", RGB(34,35,29));
	this->GetDlgItem(IDC_COMBO_SETTING_RESOLUTION)->MoveWindow(pXmlComboResolution->IntAttribute("Left"), pXmlComboResolution->IntAttribute("Top"), pXmlComboResolution->IntAttribute("Width"), pXmlComboResolution->IntAttribute("Height"));
	m_comboboxResolution.InitComboBox(IDC_COMBO_SETTING_RESOLUTION, this, CString(pXmlComboResolution->Attribute("BGImage")), crListColor, crFontColor, 14, L"黑体", crListHiliteColor);
	vector<CString> resolutionSet;
	ConfigManager::GetInstance()->GetAllResolution(resolutionSet);
	m_comboboxResolution.Clear();
	for (vector<CString>::iterator it = resolutionSet.begin();it != resolutionSet.end();++it)
	{
		m_comboboxResolution.AddString(L" " + *it);
	}
	m_comboboxResolution.SetCurSel(0);

	/*this->GetDlgItem(IDC_COMBO_SETTING_TERRIAN)->MoveWindow(100, 307, 126, 18);
	m_comboboxTerrian.InitComboBox(IDC_COMBO_SETTING_TERRIAN, this, L"launcherset\\select_btn.bmp", RGB(48, 49, 43), RGB(133, 134, 128), 14, L"黑体", RGB(34,35,29));*/
	this->GetDlgItem(IDC_COMBO_SETTING_TERRIAN)->MoveWindow(pXmlComboTerrian->IntAttribute("Left"), pXmlComboTerrian->IntAttribute("Top"), pXmlComboTerrian->IntAttribute("Width"), pXmlComboTerrian->IntAttribute("Height"));
	m_comboboxTerrian.InitComboBox(IDC_COMBO_SETTING_TERRIAN, this, CString(pXmlComboTerrian->Attribute("BGImage")), crListColor, crFontColor, 14, L"黑体", crListHiliteColor);
	m_comboboxTerrian.AddString(_T("             低(Low)"));
	m_comboboxTerrian.AddString(_T("          中(Medium)"));
	m_comboboxTerrian.AddString(_T("             高(High)"));
	m_comboboxTerrian.SetCurSel(0);

	/*this->GetDlgItem(IDC_COMBO_SETTING_EFFECT)->MoveWindow(305, 307, 126, 18);
	m_comboboxEffect.InitComboBox(IDC_COMBO_SETTING_EFFECT, this, L"launcherset\\select_btn.bmp", RGB(48, 49, 43), RGB(133, 134, 128), 14, L"黑体", RGB(34,35,29));*/
	this->GetDlgItem(IDC_COMBO_SETTING_EFFECT)->MoveWindow(pXmlComboEffect->IntAttribute("Left"), pXmlComboEffect->IntAttribute("Top"), pXmlComboEffect->IntAttribute("Width"), pXmlComboEffect->IntAttribute("Height"));
	m_comboboxEffect.InitComboBox(IDC_COMBO_SETTING_EFFECT, this, CString(pXmlComboEffect->Attribute("BGImage")), crListColor, crFontColor, 14, L"黑体", crListHiliteColor);
	m_comboboxEffect.AddString(_T("             低(Low)"));
	m_comboboxEffect.AddString(_T("          中(Medium)"));
	m_comboboxEffect.AddString(_T("             高(High)"));
	m_comboboxEffect.SetCurSel(0);

	/*this->GetDlgItem(IDC_COMBO_SETTING_SHADOW)->MoveWindow(100, 341, 126, 18);
	m_comboboxShadow.InitComboBox(IDC_COMBO_SETTING_SHADOW, this, L"launcherset\\select_btn.bmp", RGB(48, 49, 43), RGB(133, 134, 128), 14, L"黑体", RGB(34,35,29));*/
	this->GetDlgItem(IDC_COMBO_SETTING_SHADOW)->MoveWindow(pXmlComboShadow->IntAttribute("Left"), pXmlComboShadow->IntAttribute("Top"), pXmlComboShadow->IntAttribute("Width"), pXmlComboShadow->IntAttribute("Height"));
	m_comboboxShadow.InitComboBox(IDC_COMBO_SETTING_SHADOW, this, CString(pXmlComboShadow->Attribute("BGImage")), crListColor, crFontColor, 14, L"黑体", crListHiliteColor);
	m_comboboxShadow.AddString(_T("             低(Low)"));
	m_comboboxShadow.AddString(_T("          中(Medium)"));
	m_comboboxShadow.AddString(_T("             高(High)"));
	m_comboboxShadow.SetCurSel(0);

	// 初始化图片
	/*MyLoadImage(L"launcherset\\infobg.bmp", &m_pImageHardware, &m_rectHardware, 28, 54);
	MyLoadImage(L"launcherset\\line.bmp", &m_pImageLine, &m_rectLine, 17, 228);
	MyLoadImage(L"launcherset\\txt_resolution.bmp", &m_pImageResolution, &m_rectResolution, 43, 249);
	MyLoadImage(L"launcherset\\txt_ground.bmp", &m_pImageTerrian, &m_rectTerrian, 43, 310);
	MyLoadImage(L"launcherset\\txt_spell.bmp", &m_pImageEffect, &m_rectEffect, 248, 310);
	MyLoadImage(L"launcherset\\txt_shadow.bmp", &m_pImageShadow, &m_rectShadow, 43, 344);*/
	MyLoadImage(CString(pXmlImageHardware->Attribute("BGImage")), &m_pImageHardware, &m_rectHardware, pXmlImageHardware->IntAttribute("Left"), pXmlImageHardware->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageLine->Attribute("BGImage")), &m_pImageLine, &m_rectLine, pXmlImageLine->IntAttribute("Left"), pXmlImageLine->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageResolution->Attribute("BGImage")), &m_pImageResolution, &m_rectResolution, pXmlImageResolution->IntAttribute("Left"), pXmlImageResolution->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageTerrian->Attribute("BGImage")), &m_pImageTerrian, &m_rectTerrian, pXmlImageTerrian->IntAttribute("Left"), pXmlImageTerrian->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageEffect->Attribute("BGImage")), &m_pImageEffect, &m_rectEffect, pXmlImageEffect->IntAttribute("Left"), pXmlImageEffect->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageShadow->Attribute("BGImage")), &m_pImageShadow, &m_rectShadow, pXmlImageShadow->IntAttribute("Left"), pXmlImageShadow->IntAttribute("Top"));
}

void CSettingDlg::ShowGameConfig(CDC * pDC)
{
	MyShowImage(pDC, m_pImageHardware, &m_rectHardware, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageLine, &m_rectLine, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageResolution, &m_rectResolution, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageTerrian, &m_rectTerrian, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageEffect, &m_rectEffect, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageShadow, &m_rectShadow, RGB(255, 0, 255));

	ShowHardwareConfig(pDC);
}

void CSettingDlg::VisableGameConfig(UINT visiable)
{
	this->GetDlgItem(IDC_BUTTON_SETTING_RECOMMAND)->ShowWindow(visiable);
	this->GetDlgItem(IDC_BUTTON_SETTING_WINDOW)->ShowWindow(visiable);
	this->GetDlgItem(IDC_BUTTON_SETTING_VS)->ShowWindow(visiable);
	this->GetDlgItem(IDC_BUTTON_SETTING_NO_SOUND)->ShowWindow(visiable);	
	this->GetDlgItem(IDC_COMBO_SETTING_RESOLUTION)->ShowWindow(visiable);	
	this->GetDlgItem(IDC_COMBO_SETTING_TERRIAN)->ShowWindow(visiable);	
	this->GetDlgItem(IDC_COMBO_SETTING_EFFECT)->ShowWindow(visiable);
	this->GetDlgItem(IDC_COMBO_SETTING_SHADOW)->ShowWindow(visiable);
}

void CSettingDlg::InitAutoUpdate()
{
	// 获取UI配置
	CDialogConfig* pDialogConfig = ConfigManager::GetInstance()->GetDialogConfig(_T("SettingDlg"));
	TiXmlElement* pXmlDialogConfig = pDialogConfig->GetConfig();
	
	TiXmlElement* pXmlDoAutoUpdate = pDialogConfig->GetChildConfig(_T("DoAutoUpdate"));
	TiXmlElement* pXmlNoAutoUpdate = pDialogConfig->GetChildConfig(_T("NoAutoUpdate"));
	TiXmlElement* pXmlRemind = pDialogConfig->GetChildConfig(_T("Remind"));
	TiXmlElement* pXmlStandBy = pDialogConfig->GetChildConfig(_T("StandBy"));
	TiXmlElement* pXmlCPU = pDialogConfig->GetChildConfig(_T("CPU"));
	TiXmlElement* pXmlNetwork = pDialogConfig->GetChildConfig(_T("Network"));
	TiXmlElement* pXmlShutDown = pDialogConfig->GetChildConfig(_T("ShutDown"));

	TiXmlElement* pXmlInputStandBy = pDialogConfig->GetChildConfig(_T("InputStandBy"));
	TiXmlElement* pXmlInputCPU = pDialogConfig->GetChildConfig(_T("InputCPU"));
	TiXmlElement* pXmlInputNetwork = pDialogConfig->GetChildConfig(_T("InputNetwork"));

	TiXmlElement* pXmlImageAutoUpdateTitle = pDialogConfig->GetChildConfig(_T("ImageAutoUpdateTitle"));
	TiXmlElement* pXmlImageAutoUpdateBK = pDialogConfig->GetChildConfig(_T("ImageAutoUpdateBK"));
	TiXmlElement* pXmlImageStandBy = pDialogConfig->GetChildConfig(_T("ImageStandBy"));
	TiXmlElement* pXmlImageCPU = pDialogConfig->GetChildConfig(_T("ImageCPU"));
	TiXmlElement* pXmlImageNetwork = pDialogConfig->GetChildConfig(_T("ImageNetwork"));
	TiXmlElement* pXmlImageShutDown = pDialogConfig->GetChildConfig(_T("ImageShutDown"));

	// 初始化CheckBox
	/*this->GetDlgItem(IDC_BUTTON_SETTING_DOAUTOUPDATE)->MoveWindow(44, 136, 107, 17);
	m_ImageButtonDoAutoUpdate.InitButton(IDC_BUTTON_SETTING_DOAUTOUPDATE, this, L"launcherset\\radio_doupdata.bmp", 107, 17, TRUE);
	m_ImageButtonDoAutoUpdate.SetCheckBoxValue(TRUE);

	this->GetDlgItem(IDC_BUTTON_SETTING_NOAUTOUPDATE)->MoveWindow(44, 367, 107, 17);
	m_ImageButtonNoAutoUpdate.InitButton(IDC_BUTTON_SETTING_NOAUTOUPDATE, this, L"launcherset\\radio_noupdata.bmp", 107, 17, TRUE);
	
	this->GetDlgItem(IDC_BUTTON_SETTING_REMIND)->MoveWindow(300, 324, 128, 14);
	m_ImageButtonRemind.InitButton(IDC_BUTTON_SETTING_REMIND, this, L"launcherset\\checkbox_alert.bmp", 128, 14, TRUE);*/
	this->GetDlgItem(IDC_BUTTON_SETTING_DOAUTOUPDATE)->MoveWindow(pXmlDoAutoUpdate->IntAttribute("Left"), pXmlDoAutoUpdate->IntAttribute("Top"), pXmlDoAutoUpdate->IntAttribute("Width"), pXmlDoAutoUpdate->IntAttribute("Height"));
	//m_ImageButtonDoAutoUpdate.InitButton(IDC_BUTTON_SETTING_DOAUTOUPDATE, this, CString(pXmlDoAutoUpdate->Attribute("BGImage")), pXmlDoAutoUpdate->IntAttribute("Width"), pXmlDoAutoUpdate->IntAttribute("Height"), TRUE);
	m_ImageButtonDoAutoUpdate.InitButton(IDC_BUTTON_SETTING_DOAUTOUPDATE,this,CString(pXmlDoAutoUpdate->Attribute("NormalImage")),CString(pXmlDoAutoUpdate->Attribute("HoverImage")),CString(pXmlDoAutoUpdate->Attribute("PressedImage")));
	m_ImageButtonDoAutoUpdate.SetIsCheckBox(TRUE);
	
	this->GetDlgItem(IDC_BUTTON_SETTING_NOAUTOUPDATE)->MoveWindow(pXmlNoAutoUpdate->IntAttribute("Left"), pXmlNoAutoUpdate->IntAttribute("Top"), pXmlNoAutoUpdate->IntAttribute("Width"), pXmlNoAutoUpdate->IntAttribute("Height"));
	//m_ImageButtonNoAutoUpdate.InitButton(IDC_BUTTON_SETTING_NOAUTOUPDATE, this, CString(pXmlNoAutoUpdate->Attribute("BGImage")), pXmlNoAutoUpdate->IntAttribute("Width"), pXmlNoAutoUpdate->IntAttribute("Height"), TRUE);
	m_ImageButtonNoAutoUpdate.InitButton(IDC_BUTTON_SETTING_NOAUTOUPDATE,this,CString(pXmlNoAutoUpdate->Attribute("NormalImage")),CString(pXmlNoAutoUpdate->Attribute("HoverImage")),CString(pXmlNoAutoUpdate->Attribute("PressedImage")));
	m_ImageButtonNoAutoUpdate.SetIsCheckBox(TRUE);

	this->GetDlgItem(IDC_BUTTON_SETTING_REMIND)->MoveWindow(pXmlRemind->IntAttribute("Left"), pXmlRemind->IntAttribute("Top"), pXmlRemind->IntAttribute("Width"), pXmlRemind->IntAttribute("Height"));
	//m_ImageButtonRemind.InitButton(IDC_BUTTON_SETTING_REMIND, this, CString(pXmlRemind->Attribute("BGImage")), pXmlRemind->IntAttribute("Width"), pXmlRemind->IntAttribute("Height"), TRUE);
	m_ImageButtonRemind.InitButton(IDC_BUTTON_SETTING_REMIND,this,CString(pXmlRemind->Attribute("NormalImage")),CString(pXmlRemind->Attribute("HoverImage")),CString(pXmlRemind->Attribute("PressedImage")));
	m_ImageButtonRemind.SetIsCheckBox(TRUE);

	/*this->GetDlgItem(IDC_BUTTON_SETTING_STANDBY)->MoveWindow(49, 178, 15, 15);
	m_ImageButtonStandBy.InitButton(IDC_BUTTON_SETTING_STANDBY, this, L"launcherset\\checkbox_enabled.bmp", 15, 15, TRUE);
	m_ImageButtonStandBy.SetCheckBoxDisableImage(L"launcherset\\checkbox_disabled.bmp", 0, 0, 15, 15);

	this->GetDlgItem(IDC_BUTTON_SETTING_CPU)->MoveWindow(49, 210, 15, 15);
	m_ImageButtonCPU.InitButton(IDC_BUTTON_SETTING_CPU, this, L"launcherset\\checkbox_enabled.bmp", 15, 15, TRUE);
	m_ImageButtonCPU.SetCheckBoxDisableImage(L"launcherset\\checkbox_disabled.bmp", 0, 0, 15, 15);

	this->GetDlgItem(IDC_BUTTON_SETTING_NETWORK)->MoveWindow(49, 242, 15, 15);
	m_ImageButtonNetWork.InitButton(IDC_BUTTON_SETTING_NETWORK, this, L"launcherset\\checkbox_enabled.bmp", 15, 15, TRUE);
	m_ImageButtonNetWork.SetCheckBoxDisableImage(L"launcherset\\checkbox_disabled.bmp", 0, 0, 15, 15);

	this->GetDlgItem(IDC_BUTTON_SETTING_SHUTDOWN)->MoveWindow(49, 280, 15, 15);
	m_ImageButtonShutDown.InitButton(IDC_BUTTON_SETTING_SHUTDOWN, this, L"launcherset\\checkbox_enabled.bmp", 15, 15, TRUE);
	m_ImageButtonShutDown.SetCheckBoxDisableImage(L"launcherset\\checkbox_disabled.bmp", 0, 0, 15, 15);*/
	this->GetDlgItem(IDC_BUTTON_SETTING_STANDBY)->MoveWindow(pXmlStandBy->IntAttribute("Left"), pXmlStandBy->IntAttribute("Top"), pXmlStandBy->IntAttribute("Width"), pXmlStandBy->IntAttribute("Height"));
	//m_ImageButtonStandBy.InitButton(IDC_BUTTON_SETTING_STANDBY, this, CString(pXmlStandBy->Attribute("BGImage")), pXmlStandBy->IntAttribute("Width"), pXmlStandBy->IntAttribute("Height"), TRUE);
	m_ImageButtonStandBy.InitButton(IDC_BUTTON_SETTING_STANDBY,this,CString(pXmlStandBy->Attribute("NormalImage")),CString(pXmlStandBy->Attribute("HoverImage")),CString(pXmlStandBy->Attribute("PressedImage")));
	m_ImageButtonStandBy.SetIsCheckBox(TRUE);
	m_ImageButtonStandBy.SetCheckBoxDisableImage(CString(pXmlStandBy->Attribute("DisableImage")), 0, 0, pXmlStandBy->IntAttribute("Width"), pXmlStandBy->IntAttribute("Height"));

	this->GetDlgItem(IDC_BUTTON_SETTING_CPU)->MoveWindow(pXmlCPU->IntAttribute("Left"), pXmlCPU->IntAttribute("Top"), pXmlCPU->IntAttribute("Width"), pXmlCPU->IntAttribute("Height"));
	//m_ImageButtonCPU.InitButton(IDC_BUTTON_SETTING_CPU, this, CString(pXmlCPU->Attribute("BGImage")), pXmlCPU->IntAttribute("Width"), pXmlCPU->IntAttribute("Height"), TRUE);
	m_ImageButtonCPU.InitButton(IDC_BUTTON_SETTING_CPU,this,CString(pXmlCPU->Attribute("NormalImage")),CString(pXmlCPU->Attribute("HoverImage")),CString(pXmlCPU->Attribute("PressedImage")));
	m_ImageButtonCPU.SetIsCheckBox(TRUE);
	m_ImageButtonCPU.SetCheckBoxDisableImage(CString(pXmlCPU->Attribute("DisableImage")), 0, 0, pXmlCPU->IntAttribute("Width"), pXmlCPU->IntAttribute("Height"));

	this->GetDlgItem(IDC_BUTTON_SETTING_NETWORK)->MoveWindow(pXmlNetwork->IntAttribute("Left"), pXmlNetwork->IntAttribute("Top"), pXmlNetwork->IntAttribute("Width"), pXmlNetwork->IntAttribute("Height"));
	//m_ImageButtonNetWork.InitButton(IDC_BUTTON_SETTING_NETWORK, this, CString(pXmlNetwork->Attribute("BGImage")), pXmlNetwork->IntAttribute("Width"), pXmlNetwork->IntAttribute("Height"), TRUE);
	m_ImageButtonNetWork.InitButton(IDC_BUTTON_SETTING_NETWORK,this,CString(pXmlNetwork->Attribute("NormalImage")),CString(pXmlNetwork->Attribute("HoverImage")),CString(pXmlNetwork->Attribute("PressedImage")));
	m_ImageButtonNetWork.SetIsCheckBox(TRUE);
	m_ImageButtonNetWork.SetCheckBoxDisableImage(CString(pXmlNetwork->Attribute("DisableImage")), 0, 0, pXmlNetwork->IntAttribute("Width"), pXmlNetwork->IntAttribute("Height"));

	this->GetDlgItem(IDC_BUTTON_SETTING_SHUTDOWN)->MoveWindow(pXmlShutDown->IntAttribute("Left"), pXmlShutDown->IntAttribute("Top"), pXmlShutDown->IntAttribute("Width"), pXmlShutDown->IntAttribute("Height"));
	//m_ImageButtonShutDown.InitButton(IDC_BUTTON_SETTING_SHUTDOWN, this, CString(pXmlShutDown->Attribute("BGImage")), pXmlShutDown->IntAttribute("Width"), pXmlShutDown->IntAttribute("Height"), TRUE);
	m_ImageButtonShutDown.InitButton(IDC_BUTTON_SETTING_SHUTDOWN,this,CString(pXmlShutDown->Attribute("NormalImage")),CString(pXmlShutDown->Attribute("HoverImage")),CString(pXmlShutDown->Attribute("PressedImage")));
	m_ImageButtonShutDown.SetIsCheckBox(TRUE);
	m_ImageButtonShutDown.SetCheckBoxDisableImage(CString(pXmlShutDown->Attribute("DisableImage")), 0, 0, pXmlShutDown->IntAttribute("Width"), pXmlShutDown->IntAttribute("Height"));

	// 初始化输入框
	/*this->GetDlgItem(IDC_EDIT_SETTING_STANDBY)->MoveWindow(70, 176, 40, 19);
	this->GetDlgItem(IDC_EDIT_SETTING_CPU)->MoveWindow(70, 208, 40, 19);
	this->GetDlgItem(IDC_EDIT_SETTING_NETWORK)->MoveWindow(70, 240, 40, 19);*/
	this->GetDlgItem(IDC_EDIT_SETTING_STANDBY)->MoveWindow(pXmlInputStandBy->IntAttribute("Left"), pXmlInputStandBy->IntAttribute("Top"), pXmlInputStandBy->IntAttribute("Width"), pXmlInputStandBy->IntAttribute("Height"));
	this->GetDlgItem(IDC_EDIT_SETTING_CPU)->MoveWindow(pXmlInputCPU->IntAttribute("Left"), pXmlInputCPU->IntAttribute("Top"), pXmlInputCPU->IntAttribute("Width"), pXmlInputCPU->IntAttribute("Height"));
	this->GetDlgItem(IDC_EDIT_SETTING_NETWORK)->MoveWindow(pXmlInputNetwork->IntAttribute("Left"), pXmlInputNetwork->IntAttribute("Top"), pXmlInputNetwork->IntAttribute("Width"), pXmlInputNetwork->IntAttribute("Height"));

	// 初始化图片
	/*MyLoadImage(L"launcherset\\txt_autoupdatatitle.bmp", &m_pImageAutoUpdateTitle, &m_rectAutoUpdateTitle, 27, 58);
	MyLoadImage(L"launcherset\\autoupdatesetbg.bmp", &m_pImageAutoUpdateBK, &m_rectAutoUpdateBK, 32, 158);

	MyLoadImage(L"launcherset\\txt_upset_01.bmp", &m_pImageStandBy, &m_rectStandBy, 116, 178);
	MyLoadImage(L"launcherset\\txt_upset_02.bmp", &m_pImageCPU, &m_rectCPU, 116, 210);
	MyLoadImage(L"launcherset\\txt_upset_03.bmp", &m_pImageNetWork, &m_rectNetWork, 116, 242);
	MyLoadImage(L"launcherset\\txt_upset_04.bmp", &m_pImageShutDown, &m_rectShutDown, 70, 280);*/
	MyLoadImage(CString(pXmlImageAutoUpdateTitle->Attribute("BGImage")), &m_pImageAutoUpdateTitle, &m_rectAutoUpdateTitle, pXmlImageAutoUpdateTitle->IntAttribute("Left"), pXmlImageAutoUpdateTitle->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageAutoUpdateBK->Attribute("BGImage")), &m_pImageAutoUpdateBK, &m_rectAutoUpdateBK, pXmlImageAutoUpdateBK->IntAttribute("Left"), pXmlImageAutoUpdateBK->IntAttribute("Top"));

	MyLoadImage(CString(pXmlImageStandBy->Attribute("BGImage")), &m_pImageStandBy, &m_rectStandBy, pXmlImageStandBy->IntAttribute("Left"), pXmlImageStandBy->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageCPU->Attribute("BGImage")), &m_pImageCPU, &m_rectCPU, pXmlImageCPU->IntAttribute("Left"), pXmlImageCPU->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageNetwork->Attribute("BGImage")), &m_pImageNetWork, &m_rectNetWork, pXmlImageNetwork->IntAttribute("Left"), pXmlImageNetwork->IntAttribute("Top"));
	MyLoadImage(CString(pXmlImageShutDown->Attribute("BGImage")), &m_pImageShutDown, &m_rectShutDown, pXmlImageShutDown->IntAttribute("Left"), pXmlImageShutDown->IntAttribute("Top"));
}

void CSettingDlg::ShowAutoUpdate(CDC * pDC)
{
	MyShowImage(pDC, m_pImageAutoUpdateTitle, &m_rectAutoUpdateTitle, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageAutoUpdateBK, &m_rectAutoUpdateBK, RGB(255, 0, 255));

	MyShowImage(pDC, m_pImageStandBy, &m_rectStandBy, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageCPU, &m_rectCPU, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageNetWork, &m_rectNetWork, RGB(255, 0, 255));
	MyShowImage(pDC, m_pImageShutDown, &m_rectShutDown, RGB(255, 0, 255));
}

void CSettingDlg::VisableAutoUpdate(UINT visiable)
{
	this->GetDlgItem(IDC_BUTTON_SETTING_DOAUTOUPDATE)->ShowWindow(visiable);
	this->GetDlgItem(IDC_BUTTON_SETTING_NOAUTOUPDATE)->ShowWindow(visiable);
	this->GetDlgItem(IDC_BUTTON_SETTING_REMIND)->ShowWindow(visiable);

	this->GetDlgItem(IDC_EDIT_SETTING_STANDBY)->ShowWindow(visiable);
	this->GetDlgItem(IDC_EDIT_SETTING_CPU)->ShowWindow(visiable);
	this->GetDlgItem(IDC_EDIT_SETTING_NETWORK)->ShowWindow(visiable);

	this->GetDlgItem(IDC_BUTTON_SETTING_STANDBY)->ShowWindow(visiable);
	this->GetDlgItem(IDC_BUTTON_SETTING_CPU)->ShowWindow(visiable);
	this->GetDlgItem(IDC_BUTTON_SETTING_NETWORK)->ShowWindow(visiable);
	this->GetDlgItem(IDC_BUTTON_SETTING_SHUTDOWN)->ShowWindow(visiable);
}

void CSettingDlg::EnableAutoUpdate(BOOL bEnable)
{
	this->GetDlgItem(IDC_EDIT_SETTING_STANDBY)->EnableWindow(bEnable);
	this->GetDlgItem(IDC_EDIT_SETTING_CPU)->EnableWindow(bEnable);
	this->GetDlgItem(IDC_EDIT_SETTING_NETWORK)->EnableWindow(bEnable);

	this->GetDlgItem(IDC_BUTTON_SETTING_STANDBY)->EnableWindow(bEnable);
	this->GetDlgItem(IDC_BUTTON_SETTING_CPU)->EnableWindow(bEnable);
	this->GetDlgItem(IDC_BUTTON_SETTING_NETWORK)->EnableWindow(bEnable);
	this->GetDlgItem(IDC_BUTTON_SETTING_SHUTDOWN)->EnableWindow(bEnable);
}

void CSettingDlg::SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent)
{
	// 获取配置信息
	CShapeConfig* pShapeConfig = ConfigManager::GetInstance()->GetShapeConfig(strBitmapFileName);
	vector<CRect*>& arrWorkRect = pShapeConfig->GetWorkRectList();

	// 定义工作区
	/*CRect arrWorkRect[4];
	int	iWorkRectNum = 4;
	arrWorkRect[0].SetRect(0, 0, 6, 6);
	arrWorkRect[1].SetRect(465, 0, 470, 6);
	arrWorkRect[2].SetRect(0, 485, 5, 490);
	arrWorkRect[3].SetRect(466, 486, 470, 490);*/

	// 定义变量
	CDC memDC;
	CBitmap* pBitmap;
	CBitmap* pOldMemBmp = NULL;
	COLORREF cl;
	CRect cRect;
	int x, y;
	CRgn wndRgn, rgnTemp;

	// 获得窗口大小
	GetWindowRect(&cRect);

	// 加载位图资源
	HBITMAP temp = (HBITMAP)LoadImage(NULL, pShapeConfig->GetImageName(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		pBitmap = CBitmap::FromHandle(temp);
		memDC.CreateCompatibleDC(pDC);
		pOldMemBmp = memDC.SelectObject(pBitmap);

		// 创建默认区域
		wndRgn.CreateRectRgn(0, 0, cRect.Width(), cRect.Height());

		// 建立窗口
		//for (x = 0;x <= cRect.Width();++x)
		//{
		//	for (y = 0;y <= cRect.Height();++y)
		//	{
		//		// 获取坐标处的像素值
		//		cl = memDC.GetPixel(x, y);

		//		// 扣除
		//		if (cl == crTransparent)
		//		{
		//			rgnTemp.CreateRectRgn(x, y, x+1, y+1);
		//			wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
		//			rgnTemp.DeleteObject();
		//		}
		//	}
		//}
		for(int nRectIndex = 0;nRectIndex < arrWorkRect.size();++nRectIndex)
		{
			CRect* rectWork = arrWorkRect[nRectIndex];

			for (x = rectWork->left;x <= rectWork->right;++x)
			{
				for (y = rectWork->top;y <= rectWork->bottom;++y)
				{
					// 获取坐标处的像素值
					cl = memDC.GetPixel(x, y);

					// 扣除
					if (cl == pShapeConfig->GetTransparentColor())
					{
						rgnTemp.CreateRectRgn(x, y, x+1, y+1);
						wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
						rgnTemp.DeleteObject();

					}
				}
			}
		}

		// 清除资源
		pBitmap->DeleteObject();
		memDC.SelectObject(pOldMemBmp);
		memDC.DeleteDC();

		// 指定窗口
		SetWindowRgn((HRGN)wndRgn, TRUE);
	}
}

BOOL CSettingDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
			return TRUE;
		case VK_ESCAPE:
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CSettingDlg::MyLoadImage(CString strFileName, CBitmap ** pImage, CRect* rect, int iPosX, int iPosY)
{
	BITMAP hBitmap;
	HBITMAP temp = (HBITMAP)LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		*pImage = CBitmap::FromHandle(temp);
		
		(*pImage)->GetBitmap(&hBitmap);
		
		rect->SetRect(iPosX, iPosY, iPosX+hBitmap.bmWidth, iPosY+hBitmap.bmHeight);

		return TRUE;
	}
	else
	{
		*pImage = NULL;
		rect->SetRect(0, 0, 0, 0);
		return FALSE;
	}
}

BOOL CSettingDlg::MyShowImage(CDC *dc, CBitmap *pImage, CRect *rect, COLORREF crTransparent)
{
	if (dc == NULL || pImage == NULL || rect == NULL)
	{
		return FALSE;
	}
	else
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		CBitmap* pOldBmp = memDC.SelectObject(pImage);

		TransparentBlt(dc->m_hDC, rect->left, rect->top, rect->Width(), rect->Height(), memDC.m_hDC, 0, 0, rect->Width(), rect->Height(), crTransparent);

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();

		return TRUE;
	}
}

void CSettingDlg::PaintBk(CDC* pDC, CRect rect)
{
	CDC tempDC;
	CBitmap tempBmp;

	if (pDC != NULL)
	{
		tempDC.CreateCompatibleDC(pDC);
		tempBmp.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
		CBitmap* pOldBmp = tempDC.SelectObject(&tempBmp);

		tempDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);

		pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &tempDC, 0, 0, SRCCOPY);

		tempBmp.DeleteObject();
		tempDC.SelectObject(pOldBmp);
		tempDC.DeleteDC();
	}
} // End of Pa

BOOL CSettingDlg::MyShowText(CDC *dc, CString strText, CRect rect, COLORREF crColor, int iHeight, int iWeight, CString strFaceName)
{
	if (dc == NULL)
	{
		return FALSE;
	}
	else
	{
		// 重绘背景
		PaintBk(dc, rect);

		// 字体输出
		CFont font;
		font.CreateFont(iHeight, 0, 0, 0, iWeight, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, strFaceName);
		CFont* def_font = dc->SelectObject(&font);
		dc->SetBkMode(TRANSPARENT);
		dc->SetTextColor(crColor);
		dc->TextOut(rect.left, rect.top, strText);

		dc->SelectObject(def_font);
		font.DeleteObject();

		return TRUE;
	}
}

void CSettingDlg::ShowConfigure()
{
	m_comboboxResolution.SetCurSel(m_Configure[CT_RESOLUTION_INDEX]);	//分辨率
	m_ImageButtonWindow.SetCheckBoxValue(m_Configure[CT_WINDOW] == 0 ? FALSE:TRUE); 			// 窗口模式
	m_ImageButtonVS.SetCheckBoxValue(m_Configure[CT_VS] == 0 ? FALSE:TRUE);					// 垂直同步
	m_ImageButtonNoSound.SetCheckBoxValue(m_Configure[CT_SOUND_OFF] == 0 ? FALSE:TRUE);		// 禁止声音
	m_comboboxTerrian.SetCurSel(m_Configure[CT_TERRIAN]);				// 地表细节
	m_comboboxShadow.SetCurSel(m_Configure[CT_SHADOW]);				// 阴影细节
	m_comboboxEffect.SetCurSel(m_Configure[CT_EFFECT]);				// 特效细节

	// 预下载选项
	m_iStandByValue = g_iStandByValue;
	m_iCPUValue = g_iCPUValue;
	m_iNetWorkValue = g_iNetWorkValue;

	m_ImageButtonStandBy.SetCheckBoxValue(g_iEnableStandBy == 0 ? FALSE : TRUE);
	m_ImageButtonCPU.SetCheckBoxValue(g_iEnableCPU == 0 ? FALSE : TRUE);
	m_ImageButtonNetWork.SetCheckBoxValue(g_iEnableNetWork == 0 ? FALSE : TRUE);

	m_editStandBy.SetReadOnly(!m_ImageButtonStandBy.GetCheckBoxValue());
	m_editCPU.SetReadOnly(!m_ImageButtonCPU.GetCheckBoxValue());
	m_editNetWork.SetReadOnly(!m_ImageButtonNetWork.GetCheckBoxValue());

	m_ImageButtonShutDown.SetCheckBoxValue(g_iEnableShutDown == 0 ? FALSE : TRUE);
	m_ImageButtonRemind.SetCheckBoxValue(g_iEnableUserRemind == 0 ? FALSE : TRUE);
	
	if(g_iEnableAutoUpdate == 0)
	{
		m_ImageButtonDoAutoUpdate.SetCheckBoxValue(FALSE);
		m_ImageButtonNoAutoUpdate.SetCheckBoxValue(TRUE);
		EnableAutoUpdate(FALSE);
	}
	else
	{
		m_ImageButtonDoAutoUpdate.SetCheckBoxValue(TRUE);
		m_ImageButtonNoAutoUpdate.SetCheckBoxValue(FALSE);
		EnableAutoUpdate(TRUE);
	}

	// 更新至界面
	UpdateData(FALSE);
}


void CSettingDlg::GetConfigure()
{
	// 从界面获取数据
	UpdateData(TRUE);

	// 存放入m_Configure
	m_Configure[CT_RESOLUTION_INDEX] = m_comboboxResolution.GetCurSel();//分辨率	
	m_Configure[CT_WINDOW] = m_ImageButtonWindow.GetCheckBoxValue() ? 1:0; 						// 窗口模式
	m_Configure[CT_VS] = m_ImageButtonVS.GetCheckBoxValue() ? 1:0;					// 垂直同步
	m_Configure[CT_SOUND_OFF] = m_ImageButtonNoSound.GetCheckBoxValue() ? 1:0;		// 禁止声音
	m_Configure[CT_TERRIAN] = m_comboboxTerrian.GetCurSel();				// 地表细节
	m_Configure[CT_SHADOW] = m_comboboxShadow.GetCurSel();				// 阴影细节
	m_Configure[CT_EFFECT] = m_comboboxEffect.GetCurSel();				// 特效细节

	// 预下载选项
	g_iStandByValue = m_iStandByValue;
	g_iCPUValue = m_iCPUValue;
	g_iNetWorkValue = m_iNetWorkValue;

	g_iEnableStandBy = m_ImageButtonStandBy.GetCheckBoxValue() ? 1 : 0;
	g_iEnableCPU = m_ImageButtonCPU.GetCheckBoxValue() ? 1 : 0;
	g_iEnableNetWork = m_ImageButtonNetWork.GetCheckBoxValue() ? 1 : 0;
	g_iEnableShutDown = m_ImageButtonShutDown.GetCheckBoxValue() ? 1 : 0;
	g_iEnableUserRemind = m_ImageButtonRemind.GetCheckBoxValue() ? 1 : 0;

	g_iEnableAutoUpdate = m_ImageButtonDoAutoUpdate.GetCheckBoxValue() ? 1 : 0;
	
}

void CSettingDlg::InitHardwareConfig()
{
	// 标题栏，后面的偏移量25
	m_strHardwareTitle = _T("加载计算机配置。。。。。。");
	m_rectHardwareTitle.SetRect(43, 65, 443, 79);

	// 处理器
	m_strCentralProcessor = _T("处理器：        ");
	m_rectCentralProcessor.SetRect(43, 90, 443, 104);
	
	// 内存
	m_strMemory = _T("内存：            ");
	m_rectMemory.SetRect(43, 115, 443, 129);

	// 显卡
	m_strVideoCard = _T("显卡：            ");
	m_rectVideoCard.SetRect(43, 140, 443, 154);

	// 操作系统
	m_strOS = _T("操作系统：    ");
	m_rectOS.SetRect(43, 165, 443, 179);
}

void CSettingDlg::GetHardwareConfig()
{
	CString strTemp;

	// 标题栏
	m_strHardwareTitle = _T("您当前的计算机配置：");

	// 处理器
	m_strCentralProcessor += ConfigManager::GetInstance()->GetRegQueryValue(HKEY_LOCAL_MACHINE, _T("HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0"), KEY_QUERY_VALUE, _T("ProcessorNameString"));

	// 内存
	MEMORYSTATUSEX MemoryStatus;
	MemoryStatus.dwLength = sizeof(MEMORYSTATUSEX);
	::GlobalMemoryStatusEx(&MemoryStatus);
	float iGB = MemoryStatus.ullTotalPhys / 1024.0f / 1024.0f / 1024.0f;
	strTemp.Format(_T("%3.2fGB (%d bytes)"), iGB, MemoryStatus.ullTotalPhys);
	m_strMemory += strTemp;

	// 显卡
	strTemp = ConfigManager::GetInstance()->GetRegQueryValue(HKEY_LOCAL_MACHINE, _T("HARDWARE\\DEVICEMAP\\VIDEO"), KEY_QUERY_VALUE, _T("\\Device\\Video0"));
	CString strVideoID = strTemp.Mid(18);
	CString strVideoCardName = ConfigManager::GetInstance()->GetRegQueryValue(HKEY_LOCAL_MACHINE, strVideoID, KEY_QUERY_VALUE, _T("Device Description"));
	if(strVideoCardName.GetLength() == 0)
	{
		strTemp = ConfigManager::GetInstance()->GetRegQueryValue(HKEY_LOCAL_MACHINE, _T("HARDWARE\\DEVICEMAP\\VIDEO"), KEY_QUERY_VALUE, _T("\\Device\\Video1"));
		strVideoID = strTemp.Mid(18);
		strVideoCardName = ConfigManager::GetInstance()->GetRegQueryValue(HKEY_LOCAL_MACHINE, strVideoID, KEY_QUERY_VALUE, _T("Device Description"));
	}
	if(strVideoCardName.GetLength() == 0)
	{
		strVideoCardName = _T("未知的设备");
	}

	// 显存
	float iVideoMemory = ConfigManager::GetInstance()->GetVideoMemoryTotal();
	strTemp.Format(_T("%3.2fMB"), iVideoMemory);
	m_strVideoCard += strVideoCardName;
	m_strVideoCard += _T("  (");
	m_strVideoCard += strTemp;
	m_strVideoCard += _T(")");

	// 操作系统
	CString strOSName = ConfigManager::GetInstance()->GetRegQueryValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), KEY_QUERY_VALUE, _T("ProductName"));
	CString strOSVersion = ConfigManager::GetInstance()->GetRegQueryValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), KEY_QUERY_VALUE, _T("CSDVersion"));
	m_strOS += strOSName;
	m_strOS += _T("  (");
	m_strOS += strOSVersion;
	m_strOS += _T(")");
}

void CSettingDlg::ShowHardwareConfig(CDC* pDC)
{
	MyShowText(pDC, m_strHardwareTitle, m_rectHardwareTitle, RGB(48, 49, 43), 14, FW_BOLD, _T("黑体"));
	MyShowText(pDC, m_strCentralProcessor, m_rectCentralProcessor, RGB(90, 91, 85), 14, FW_NORMAL, _T("黑体"));
	MyShowText(pDC, m_strMemory, m_rectMemory, RGB(90, 91, 85), 14, FW_NORMAL, _T("黑体"));
	MyShowText(pDC, m_strVideoCard, m_rectVideoCard, RGB(90, 91, 85), 14, FW_NORMAL, _T("黑体"));
	MyShowText(pDC, m_strOS, m_rectOS, RGB(90, 91, 85), 14, FW_NORMAL, _T("黑体"));
}

BEGIN_MESSAGE_MAP(CSettingDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON_SETTING_OK, &CSettingDlg::OnBnClickedButtonSettingOk)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_CANCEL, &CSettingDlg::OnBnClickedButtonSettingCancel)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_TAB_CONFIG, &CSettingDlg::OnBnClickedButtonSettingTabConfig)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_TAB_UPDATE, &CSettingDlg::OnBnClickedButtonSettingTabUpdate)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_SETTING_DOAUTOUPDATE, &CSettingDlg::OnBnClickedButtonSettingDoautoupdate)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_NOAUTOUPDATE, &CSettingDlg::OnBnClickedButtonSettingNoautoupdate)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_STANDBY, &CSettingDlg::OnBnClickedButtonSettingStandby)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_CPU, &CSettingDlg::OnBnClickedButtonSettingCpu)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_NETWORK, &CSettingDlg::OnBnClickedButtonSettingNetwork)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_CLOSE, &CSettingDlg::OnBnClickedButtonSettingClose)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_RECOMMAND, &CSettingDlg::OnBnClickedButtonSettingRecommand)
	ON_EN_CHANGE(IDC_EDIT_SETTING_STANDBY, &CSettingDlg::OnEnChangeEditSettingStandby)
	ON_EN_CHANGE(IDC_EDIT_SETTING_CPU, &CSettingDlg::OnEnChangeEditSettingCpu)
	ON_EN_CHANGE(IDC_EDIT_SETTING_NETWORK, &CSettingDlg::OnEnChangeEditSettingNetwork)
END_MESSAGE_MAP()


// CSettingDlg 消息处理程序

BOOL CSettingDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	if (m_pBkgndImage == NULL)
	{
		HBITMAP temp = (HBITMAP)LoadImage(NULL, m_strBkgndImageName/*L"launcherset\\setbg.bmp"*/, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		if (temp != NULL)
		{
			m_pBkgndImage = CBitmap::FromHandle(temp);
		}
		else
		{
			m_pBkgndImage = NULL;
		}
	}

	if (m_pBkgndImage != NULL)
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		BITMAP hBitmap;
		m_pBkgndImage->GetBitmap(&hBitmap);

		CBitmap* pOldBmp = memDC.SelectObject(m_pBkgndImage);

		//pDC->BitBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, SRCCOPY);
		pDC->TransparentBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, hBitmap.bmWidth, hBitmap.bmHeight, RGB(255, 0, 255));

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();

		// 刷新图片
		if (!m_ImageButtonGameConfig.IsWindowEnabled())
		{
			ShowGameConfig(pDC);
		}
		
		if (!m_ImageButtonAutoUpdate.IsWindowEnabled())
		{
			ShowAutoUpdate(pDC);
		}

		return TRUE;
	}
	else
	{
		CDialog::OnEraseBkgnd(pDC);

		CRect rect;
		this->GetClientRect(&rect);
		pDC->FillSolidRect(&rect, RGB(150, 150, 150));

		// 刷新图片
		if (!m_ImageButtonGameConfig.IsWindowEnabled())
		{
			ShowGameConfig(pDC);
		}
		
		if (!m_ImageButtonAutoUpdate.IsWindowEnabled())
		{
			ShowAutoUpdate(pDC);
		}

		return TRUE;
	}
}

void CSettingDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	//禁止显示移动矩形窗体框
	::SystemParametersInfo(SPI_SETDRAGFULLWINDOWS,TRUE,NULL,0);
	//非标题栏移动整个窗口
	SendMessage(WM_SYSCOMMAND,0xF012,0); 

	CDialog::OnLButtonDown(nFlags, point);
}

void CSettingDlg::OnBnClickedButtonSettingOk()
{
	// TODO: 在此添加控件通知处理程序代码
	this->GetConfigure();
	ConfigManager::GetInstance()->SaveGameConfigure("UserData/Configuration.xml");
	::AfxGetMainWnd()->EnableWindow(TRUE);
	this->ShowWindow(SW_HIDE);
}

void CSettingDlg::OnBnClickedButtonSettingCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	::AfxGetMainWnd()->EnableWindow(TRUE);
	this->ShowWindow(SW_HIDE);
}

void CSettingDlg::OnBnClickedButtonSettingTabConfig()
{
	// TODO: 在此添加控件通知处理程序代码
	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_CONFIG)->EnableWindow(FALSE);
	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_UPDATE)->EnableWindow(TRUE);

	VisableGameConfig(SW_SHOW);
	VisableAutoUpdate(SW_HIDE);
	
	this->Invalidate();
}

void CSettingDlg::OnBnClickedButtonSettingTabUpdate()
{
	// TODO: 在此添加控件通知处理程序代码
	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_UPDATE)->EnableWindow(FALSE);
	this->GetDlgItem(IDC_BUTTON_SETTING_TAB_CONFIG)->EnableWindow(TRUE);

	VisableGameConfig(SW_HIDE);
	VisableAutoUpdate(SW_SHOW);

	this->Invalidate();
}

void CSettingDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	//CDialog::OnClose();
}

void CSettingDlg::OnBnClickedButtonSettingDoautoupdate()
{
	// TODO: 在此添加控件通知处理程序代码
	m_ImageButtonDoAutoUpdate.SetCheckBoxValue(TRUE);
	m_ImageButtonNoAutoUpdate.SetCheckBoxValue(FALSE);
	EnableAutoUpdate(TRUE);
}

void CSettingDlg::OnBnClickedButtonSettingNoautoupdate()
{
	// TODO: 在此添加控件通知处理程序代码
	m_ImageButtonDoAutoUpdate.SetCheckBoxValue(FALSE);
	m_ImageButtonNoAutoUpdate.SetCheckBoxValue(TRUE);
	EnableAutoUpdate(FALSE);
}

void CSettingDlg::OnBnClickedButtonSettingStandby()
{
	// TODO: 在此添加控件通知处理程序代码
	m_editStandBy.SetReadOnly(!m_ImageButtonStandBy.GetCheckBoxValue());
	//this->GetDlgItem(IDC_EDIT_SETTING_STANDBY)->EnableWindow(m_ImageButtonStandBy.GetCheckBoxValue());
	/*if(m_ImageButtonStandBy.GetCheckBoxValue())
	{
		this->GetDlgItem(IDC_EDIT_SETTING_STANDBY)->ModifyStyleEx(ES_READONLY, NULL);
	}
	else
	{
		this->GetDlgItem(IDC_EDIT_SETTING_STANDBY)->ModifyStyleEx(NULL, ES_READONLY);
	}*/
}

void CSettingDlg::OnBnClickedButtonSettingCpu()
{
	// TODO: 在此添加控件通知处理程序代码
	m_editCPU.SetReadOnly(!m_ImageButtonCPU.GetCheckBoxValue());
	//this->GetDlgItem(IDC_EDIT_SETTING_CPU)->EnableWindow(m_ImageButtonCPU.GetCheckBoxValue());
}

void CSettingDlg::OnBnClickedButtonSettingNetwork()
{
	// TODO: 在此添加控件通知处理程序代码
	m_editNetWork.SetReadOnly(!m_ImageButtonNetWork.GetCheckBoxValue());
	//this->GetDlgItem(IDC_EDIT_SETTING_NETWORK)->EnableWindow(m_ImageButtonNetWork.GetCheckBoxValue());
}

void CSettingDlg::OnBnClickedButtonSettingClose()
{
	// TODO: 在此添加控件通知处理程序代码
	::AfxGetMainWnd()->EnableWindow(TRUE);
	this->ShowWindow(SW_HIDE);
}

void CSettingDlg::OnBnClickedButtonSettingRecommand()
{
	// TODO: 在此添加控件通知处理程序代码
	ConfigManager::GetInstance()->SetGameConfigureByHardware();
	this->ShowConfigure();
	this->Invalidate();
}

void CSettingDlg::OnEnChangeEditSettingStandby()
{
	// TODO:  如果该控件是 RICHEDIT 控件，则它将不会
	// 发送该通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);

	if(m_iStandByValue < 1)
	{
		m_iStandByValue = 1;
		UpdateData(FALSE);
	}
	else if(m_iStandByValue > 60)
	{
		m_iStandByValue = 60;
		UpdateData(FALSE);
	}
}

void CSettingDlg::OnEnChangeEditSettingCpu()
{
	// TODO:  如果该控件是 RICHEDIT 控件，则它将不会
	// 发送该通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);

	if(m_iCPUValue < 1)
	{
		m_iCPUValue = 1;
		UpdateData(FALSE);
	}
	else if(m_iCPUValue > 80)
	{
		m_iCPUValue = 80;
		UpdateData(FALSE);
	}
}

void CSettingDlg::OnEnChangeEditSettingNetwork()
{
	// TODO:  如果该控件是 RICHEDIT 控件，则它将不会
	// 发送该通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);

	if(m_iNetWorkValue < 1)
	{
		m_iNetWorkValue = 1;
		UpdateData(FALSE);
	}
	else if(m_iNetWorkValue > 80)
	{
		m_iNetWorkValue = 80;
		UpdateData(FALSE);
	}
}
