﻿#pragma once

#ifdef _WIN32_WCE
#error "Windows CE 不支持 CDHtmlDialog。"
#endif 

// CWebDlg 对话框

class CWebDlg : public CDHtmlDialog
{
	DECLARE_DYNCREATE(CWebDlg)

public:
	CWebDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CWebDlg();
// 重写
	HRESULT OnButtonOK(IHTMLElement *pElement);
	HRESULT OnButtonCancel(IHTMLElement *pElement);

// 对话框数据
	enum { IDD = IDD_WEB_DIALOG, IDH = 0/*IDR_HTML_WEBDLG*/ };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnNavigateComplete(LPDISPATCH pDisp, LPCTSTR szUrl);

	DECLARE_MESSAGE_MAP()
	DECLARE_DHTML_EVENT_MAP()
};
