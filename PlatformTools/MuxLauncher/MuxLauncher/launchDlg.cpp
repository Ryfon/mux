﻿// launchDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "launchDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ClaunchDlg 对话框




ClaunchDlg::ClaunchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ClaunchDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pBkgndImage = NULL;
}

ClaunchDlg::~ClaunchDlg()
{
	delete m_pPatchDlg;
	delete m_pServerDlg;
	delete m_pWebDlg;
}

void ClaunchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

void ClaunchDlg::SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent)
{
	// 获取配置信息
	CShapeConfig* pShapeConfig = ConfigManager::GetInstance()->GetShapeConfig(strBitmapFileName);
	vector<CRect*>& arrWorkRect = pShapeConfig->GetWorkRectList();

	// 定义工作区
	/*CRect arrWorkRect[1];
	int	iWorkRectNum = 1;
	arrWorkRect[0].SetRect(0, 0, 725, 59);*/

	// 定义变量
	CDC memDC;
	CBitmap* pBitmap;
	CBitmap* pOldMemBmp = NULL;
	COLORREF cl;
	CRect cRect;
	int x, y;
	CRgn wndRgn, rgnTemp;

	// 获得窗口大小
	GetWindowRect(&cRect);

	// 加载位图资源
	HBITMAP temp = (HBITMAP)LoadImage(NULL, pShapeConfig->GetImageName(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		pBitmap = CBitmap::FromHandle(temp);
		memDC.CreateCompatibleDC(pDC);
		pOldMemBmp = memDC.SelectObject(pBitmap);

		// 创建默认区域
		wndRgn.CreateRectRgn(0, 0, cRect.Width(), cRect.Height());

		//dwStart = GetTickCount();
		// 建立窗口
		for(int nRectIndex = 0;nRectIndex < arrWorkRect.size();++nRectIndex)
		{
			CRect* rectWork = arrWorkRect[nRectIndex];

			for (x = rectWork->left;x <= rectWork->right;++x)
			{
				for (y = rectWork->top;y <= rectWork->bottom;++y)
				{
					// 获取坐标处的像素值
					cl = memDC.GetPixel(x, y);

					// 扣除
					if (cl == pShapeConfig->GetTransparentColor())
					{

						rgnTemp.CreateRectRgn(x, y, x+1, y+1);
						wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
						rgnTemp.DeleteObject();

					}
				}
			}
		}
		//dwStop = GetTickCount();

		// 清除资源
		pBitmap->DeleteObject();
		memDC.SelectObject(pOldMemBmp);
		memDC.DeleteDC();

		// 指定窗口
		SetWindowRgn((HRGN)wndRgn, TRUE);
	}

	

	/*DWORD dwTime = dwStop - dwStart;
	
	CString strTime;
	strTime.Format(_T("%d"), dwTime);
	MessageBox(strTime);*/
}

BOOL ClaunchDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
			return TRUE;
		case VK_ESCAPE:
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void ClaunchDlg::PaintBk(CDC* pDC, CRect rect)
{
	CDC tempDC;
	CBitmap tempBmp;
	
	if (pDC != NULL)
	{
		tempDC.CreateCompatibleDC(pDC);
		tempBmp.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
		CBitmap* pOldBmp = tempDC.SelectObject(&tempBmp);

		tempDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
		
		pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &tempDC, 0, 0, SRCCOPY);
		//pDC->FillSolidRect(&rect, RGB(255, 0, 0));

		tempBmp.DeleteObject();
		tempDC.SelectObject(pOldBmp);
		tempDC.DeleteDC();
	}
} // End of PaintBk

void ClaunchDlg::ShowText(CDC* dc, CString strTextValue, CRect rect, COLORREF crColor)
{
	// 重绘背景
	PaintBk(dc, rect);

	// 字体输出
	CFont font;
	font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体") );
	CFont* def_font = dc->SelectObject(&font);
	dc->SetBkMode(TRANSPARENT);
	dc->SetTextColor(crColor);
	dc->TextOut(rect.left, rect.top, strTextValue);

	dc->SelectObject(def_font);
	font.DeleteObject();
}

BEGIN_MESSAGE_MAP(ClaunchDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &ClaunchDlg::OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_MIN, &ClaunchDlg::OnBnClickedButtonMin)
END_MESSAGE_MAP()


// ClaunchDlg 消息处理程序

BOOL ClaunchDlg::OnInitDialog()
{
	//DWORD dwStart = GetTickCount();

	CDialog::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	// 加载配置
	ConfigManager::GetInstance()->GetRecommendServer();
	ConfigManager::GetInstance()->LoadGameConfigure("UserData/Configuration.xml");
	ConfigManager::GetInstance()->SetOutPutData(g_uiCatID, g_uiAreaID, g_uiServerID);
	ConfigManager::GetInstance()->LoadUIConfig("LauncherUIConfig.xml");

	// 获取UI配置
	CDialogConfig* pDialogConfig = ConfigManager::GetInstance()->GetDialogConfig(_T("LaunchDlg"));
	TiXmlElement* pXmlDialogConfig = pDialogConfig->GetConfig();
	TiXmlElement* pXmlTextLoadRes = pDialogConfig->GetChildConfig(_T("LoadRes"));
	TiXmlElement* pXmlButtonClose = pDialogConfig->GetChildConfig(_T("Close"));
	TiXmlElement* pXmlButtonMin = pDialogConfig->GetChildConfig(_T("Min"));


	// 初始化自身大小
	m_strBkgndImageName = pXmlDialogConfig->Attribute("BGImage");
	this->MoveWindow(pXmlDialogConfig->IntAttribute("Left"), pXmlDialogConfig->IntAttribute("Top"), pXmlDialogConfig->IntAttribute("Width"), pXmlDialogConfig->IntAttribute("Height"));
	CString strCaption(pXmlDialogConfig->Attribute("Caption"));
	this->SetWindowText(strCaption);
	this->CenterWindow();

	// 文字
	m_strLoadRes = pXmlTextLoadRes->Attribute("Text");
	m_rectLoadRes.SetRect(pXmlTextLoadRes->IntAttribute("Left"), pXmlTextLoadRes->IntAttribute("Top"), pXmlTextLoadRes->IntAttribute("Right"), pXmlTextLoadRes->IntAttribute("Bottom"));
	m_crLoadRes = RGB(pXmlTextLoadRes->IntAttribute("R"), pXmlTextLoadRes->IntAttribute("G"), pXmlTextLoadRes->IntAttribute("B"));

	// 最后调整形状
	this->ShowWindow(SW_SHOW);
	//this->UpdateWindow();
	SetupRegion(GetDC(), pDialogConfig->GetShapeName(), RGB(255, 0, 255));
	//this->Invalidate();
	
	//DWORD dwStop = GetTickCount();

	// 初始化控件
	this->GetDlgItem(IDC_BUTTON_CLOSE)->MoveWindow(pXmlButtonClose->IntAttribute("Left"), pXmlButtonClose->IntAttribute("Top"), pXmlButtonClose->IntAttribute("Width"), pXmlButtonClose->IntAttribute("Height"));
	//m_ImageButtonClose.InitButton(IDC_BUTTON_CLOSE, this, CString(pXmlButtonClose->Attribute("BGImage")), pXmlButtonClose->IntAttribute("Width"), pXmlButtonClose->IntAttribute("Height"));
	m_ImageButtonClose.InitButton(IDC_BUTTON_CLOSE,this,CString(pXmlButtonClose->Attribute("NormalImage")),CString(pXmlButtonClose->Attribute("HoverImage")),CString(pXmlButtonClose->Attribute("PressedImage")));
	this->GetDlgItem(IDC_BUTTON_MIN)->MoveWindow(pXmlButtonMin->IntAttribute("Left"), pXmlButtonMin->IntAttribute("Top"), pXmlButtonMin->IntAttribute("Width"), pXmlButtonMin->IntAttribute("Height"));
	//m_ImageButtonMin.InitButton(IDC_BUTTON_MIN, this, CString(pXmlButtonMin->Attribute("BGImage")), pXmlButtonMin->IntAttribute("Width"), pXmlButtonMin->IntAttribute("Height"));
	m_ImageButtonMin.InitButton(IDC_BUTTON_MIN,this,CString(pXmlButtonMin->Attribute("NormalImage")),CString(pXmlButtonMin->Attribute("HoverImage")),CString(pXmlButtonMin->Attribute("PressedImage")));
	this->UpdateWindow();

	// 初始化ServerDlg
	m_pServerDlg = new CServerDlg(this);
	m_pServerDlg->Create(IDD_SERVER_DIALOG, this);
	m_pServerDlg->ShowWindow(SW_SHOW);
	this->UpdateWindow();

	// 初始化PatchDlg
	m_pPatchDlg = new CPatchDlg(this);
	m_pPatchDlg->Create(IDD_PATCH_DIALOG, this);
	m_pPatchDlg->ShowWindow(SW_SHOW);
	m_pPatchDlg->SetServerDlg(m_pServerDlg);
	this->UpdateWindow();

	// 初始化WebDlg
	m_pWebDlg = new CWebDlg(this);
	m_pWebDlg->Create(IDD_WEB_DIALOG, this);
	//m_pWebDlg->ShowWindow(SW_SHOW);
	m_pWebDlg->ModifyStyle(NULL, WS_CLIPSIBLINGS, NULL);
	//m_pPatchDlg->SetWindowPos(m_pWebDlg, 12 , 500, 694, 88, SWP_NOMOVE|SWP_NOSIZE);

	// 文字
	m_strLoadRes = _T("");

	this->Invalidate();

	//DWORD dwStop = GetTickCount();
	//DWORD dwTime = dwStop - dwStart;
	
	//CString strTime;
	//strTime.Format(_T("%d"), dwTime);
	//MessageBox(strTime);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void ClaunchDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR ClaunchDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


BOOL ClaunchDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_pBkgndImage == NULL)
	{
		HBITMAP temp = (HBITMAP)LoadImage(NULL, m_strBkgndImageName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		if (temp != NULL)
		{
			m_pBkgndImage = CBitmap::FromHandle(temp);
		}
		else
		{
			m_pBkgndImage = NULL;
		}
	}

	if (m_pBkgndImage != NULL)
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		BITMAP hBitmap;
		m_pBkgndImage->GetBitmap(&hBitmap);

		CBitmap* pOldBmp = memDC.SelectObject(m_pBkgndImage);

		//pDC->BitBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, SRCCOPY);
		pDC->TransparentBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, hBitmap.bmWidth, hBitmap.bmHeight, RGB(255, 0, 255));

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();
		
		ShowText(pDC, m_strLoadRes, m_rectLoadRes, m_crLoadRes);
		return TRUE;
	}
	else
	{
		CDialog::OnEraseBkgnd(pDC);

		CRect rect;
		this->GetClientRect(&rect);
		pDC->FillSolidRect(&rect, RGB(200, 200, 200));

		return TRUE;
	}
}

void ClaunchDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	//禁止显示移动矩形窗体框
	::SystemParametersInfo(SPI_SETDRAGFULLWINDOWS,TRUE,NULL,0);
	//非标题栏移动整个窗口
	SendMessage(WM_SYSCOMMAND,0xF012,0); 

	CDialog::OnLButtonDown(nFlags, point);
}

void ClaunchDlg::OnBnClickedButtonClose()
{
	// TODO: 在此添加控件通知处理程序代码
	PostQuitMessage(0);
	//SendMessage(WM_CLOSE, 0, 0);
}

void ClaunchDlg::OnBnClickedButtonMin()
{
	// TODO: 在此添加控件通知处理程序代码
	this->ShowWindow(SW_MINIMIZE);
}
