﻿#pragma once

#define FBN_DRAG		WM_USER+100
#define FBN_LBUTTONUP	WM_USER+101
#define FBN_LBLONGCLICK	WM_USER+102

#include <vector>
using namespace std;

#include "ColorText.h"

// CFourBitmapButton

class CFourBitmapButton : public CBitmapButton
{
	DECLARE_DYNAMIC(CFourBitmapButton)

public:
	CFourBitmapButton();
	virtual ~CFourBitmapButton();
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

	BOOL InitButton(UINT nID, CWnd* pParent, CString strNormalImage, CString strHoverImage = L"", CString strPressedImage = L"", CString strDisableImage = L"");
	BOOL InitButton(UINT nID, CWnd* pParent, CString strImage, int width, int height, BOOL isCheckBox = FALSE);
	BOOL SetCheckBoxDisableImage(CString strImage, int x, int y, int width, int height);
	BOOL SetCheckBoxCheckedImage(CString strImage, int x, int y, int width, int height);
	void SetIsCheckBox(BOOL bIs);
	void SetCheckBoxValue(BOOL bChecked);
	BOOL GetCheckBoxValue();
	void SetDisplayText(CString text);
	void SetDisplayTextColor(COLORREF color);
	void AddDisplayText(CColorText* text);
	void ChangeDisplayText(int index, CString text, COLORREF color);
	void SetTransparent(BOOL b);
	BOOL SetTransparentImage(CString strImage, int x, int y, int width, int height);
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
private:
	BOOL		m_bMouseOnButton;
	BOOL		m_bButtonDown;
	CBitmap*	m_bitmapMouse;
	int			m_iWidth;
	int			m_iHeight;
	BOOL		m_bIsCheckBox;
	BOOL		m_bIsChecked;
	CString		m_strDisplayText;
	COLORREF	m_crTextColor;
	CBitmap*	m_bitmapCheckBoxDisableNormal;
	CBitmap*	m_bitmapCheckBoxDisableChecked;
	CBitmap*	m_bitmapCheckBoxChecked;
	CBitmap*    m_bitmapTransparent;
	vector<CColorText*>	m_setTextList;
	BOOL		m_bTransparent;
	UINT		m_uiID;
	CPoint		m_cpDownLocation;
	BOOL		m_bLongClick;
private:
	void CancelHover();
	LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	void DrawMouseBitmap(CDC* dc, CBitmap* pBitmap);
	void DrawCheckBoxDisableImage(CDC* dc);
	void DrawDisplayText(CDC* dc, CString strText, CRect rect, COLORREF crColor);
	BOOL LoadButtonBitmap(CBitmap* pChangeImage, CString strFileName);
	BOOL LoadButtonBitmap(CBitmap* pChangeImage, CString strFileName, int x, int y, int width, int height);

	BOOL LoadNormalBitmap(CString strFileName);
	BOOL LoadHoverBitmap(CString strFileName);
	BOOL LoadPressedBitmap(CString strFileName);
	BOOL LoadDisableBitmap(CString strFileName);

	void SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent);
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};


