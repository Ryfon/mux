﻿// TabSheet.cpp : 实现文件
//

#include "stdafx.h"
#include "TabSheet.h"


// CTabSheet

IMPLEMENT_DYNAMIC(CTabSheet, CTabCtrl)

CTabSheet::CTabSheet()
{
	m_NumOfPages = 0;
	m_CurPageIndex = 0;
}

CTabSheet::~CTabSheet()
{

}


BEGIN_MESSAGE_MAP(CTabSheet, CTabCtrl)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

BOOL CTabSheet::AddPage(LPCTSTR strTitle, CDialog* pDialog, UINT ID)
{
	if (MAXPAGE == m_NumOfPages)
	{
		return FALSE;
	}
	m_NumOfPages++;
	m_pPages[m_NumOfPages - 1] = pDialog;
	m_DlgIds[m_NumOfPages - 1] = ID;
	m_strTitles[m_NumOfPages - 1] = strTitle;

	return TRUE;
}

void CTabSheet::SetRect()
{
	CRect tabRect, ItemRect;
	int nX, nY, nXc, nYc;

	this->GetClientRect(&tabRect);

	GetItemRect(0, &ItemRect);

	nX = ItemRect.left;
	nY = ItemRect.bottom + 1;
	nXc = tabRect.right - ItemRect.left + 2;
	CRect DlgRect;
	m_pPages[0]->GetClientRect(&DlgRect);
	nYc = DlgRect.bottom - nY - 2;

	m_pPages[0]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);
	for (int nIndex = 1; nIndex < m_NumOfPages; nIndex++)
	{
		m_pPages[nIndex]->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);
	}
}

void CTabSheet::Show()
{
	for (int nIndex = 0; nIndex < m_NumOfPages; nIndex++)
	{
		m_pPages[nIndex]->Create(m_DlgIds[nIndex], this->GetParent());
		InsertItem(nIndex, m_strTitles[nIndex]);
	}
	m_pPages[0]->ShowWindow(SW_SHOW);
	for (int nIndex = 1; nIndex < m_NumOfPages; nIndex++)
	{
		m_pPages[nIndex]->ShowWindow(SW_HIDE);
	}
	SetRect();
}

int CTabSheet::GetCurSel()
{
	return CTabCtrl::GetCurSel();
}

int CTabSheet::SetCurSel(int nItem)
{
	if (nItem < 0 || nItem > m_NumOfPages - 1)
	{
		return -1;
	}

	int nRet = m_CurPageIndex;
	if (m_CurPageIndex != nItem)
	{
		m_pPages[m_CurPageIndex]->ShowWindow(SW_HIDE);
		m_CurPageIndex = nItem;
		m_pPages[m_CurPageIndex]->ShowWindow(SW_SHOW);
		CTabCtrl::SetCurSel(m_CurPageIndex);
	}
	return nRet;
}
// CTabSheet 消息处理程序

void CTabSheet::OnLButtonDown(UINT nFlags, CPoint point)
{
	CTabCtrl::OnLButtonDown(nFlags, point);
	int nClickPageIndex = GetCurFocus();
	if (m_CurPageIndex != nClickPageIndex)
	{
		SetCurSel(nClickPageIndex);
	}
}

