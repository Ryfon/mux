﻿#pragma once
#include "dialogconfig.h"

class CShapeConfig :
	public CDialogConfig
{
public:
	CShapeConfig(void);
	~CShapeConfig(void);
public:
	void SetConfig(TiXmlElement* config);
public:
	CString GetImageName();
	COLORREF GetTransparentColor();
	vector<CRect*>& GetWorkRectList();
private:
	CString m_strImageName;
	COLORREF m_crTransparent;
	vector<CRect*> m_WorkRectList;
};
