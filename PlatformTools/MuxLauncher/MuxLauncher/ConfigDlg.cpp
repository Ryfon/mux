﻿// ConfigDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "MuxLauncherDlg.h"
#include "ConfigDlg.h"
#include "ConfigManager.h"

// CConfigDlg 对话框

IMPLEMENT_DYNAMIC(CConfigDlg, CDialog)

CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDlg::IDD, pParent)
{
	
}

CConfigDlg::~CConfigDlg()
{
}

void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TABCONFIG, m_tabCtrlConfig);
}

BOOL CConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	/*m_tabCtrlConfig.InsertItem(0, _T("系统设置"));
	m_tabCtrlConfig.InsertItem(1, _T("游戏设置"));
	m_tabCtrlConfig.InsertItem(2, _T("自动更新"));

	m_dlgSystemConfig.SetParent(&m_tabCtrlConfig);*/

	m_tabCtrlConfig.AddPage(_T("游戏设置"), &m_dlgGameConfig, CGameConfigDlg::IDD);
	m_tabCtrlConfig.AddPage(_T("自动更新"), &m_dlgSystemConfig, CSystemConfigDlg::IDD);
	m_tabCtrlConfig.Show();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TABCONFIG, &CConfigDlg::OnTcnSelchangeTabconfig)
	ON_BN_CLICKED(IDC_BUTTONCANCEL, &CConfigDlg::OnBnClickedButtoncancel)
	ON_BN_CLICKED(IDC_BUTTONOK, &CConfigDlg::OnBnClickedButtonok)
END_MESSAGE_MAP()


// CConfigDlg 消息处理程序

void CConfigDlg::OnTcnSelchangeTabconfig(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	int iCurSel = m_tabCtrlConfig.GetCurSel();
	if (iCurSel == 0)
	{
		this->SetWindowText(_T("游戏设置"));
	}
	else if (iCurSel == 1)
	{
		this->SetWindowText(_T("自动更新"));
	}
	else
	{
		this->SetWindowText(_T("游戏设置"));
	}
}

void CConfigDlg::OnBnClickedButtoncancel()
{
	// TODO: 在此添加控件通知处理程序代码】
	this->OnCancel();
}

void CConfigDlg::OnBnClickedButtonok()
{
	// TODO: 在此添加控件通知处理程序代码
	m_dlgGameConfig.GetConfigure();
	//m_dlgGameConfig.SaveConfigure("UserData/Configuration.xml");
	//((CMuxLauncherDlg*)this->GetParent())->SaveConfigure("UserData/Configuration.xml");
	ConfigManager::GetInstance()->SaveGameConfigure("UserData/Configuration.xml");
	this->OnOK();
}
