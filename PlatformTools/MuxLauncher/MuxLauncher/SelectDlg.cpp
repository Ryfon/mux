﻿// SelectDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "SelectDlg.h"
#include "ServerDlg.h"

// CSelectDlg 对话框

IMPLEMENT_DYNAMIC(CSelectDlg, CDialog)

CSelectDlg::CSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectDlg::IDD, pParent)
{
	m_pRealParent = pParent;
	m_pBkgndImage = NULL;
	m_pImageScrollBar = NULL;

	m_AreaButtonIDList[0] = IDC_BUTTON_SELECT_AREA_1;
	m_AreaButtonIDList[1] = IDC_BUTTON_SELECT_AREA_2;
	m_AreaButtonIDList[2] = IDC_BUTTON_SELECT_AREA_3;
	m_AreaButtonIDList[3] = IDC_BUTTON_SELECT_AREA_4;
	m_AreaButtonIDList[4] = IDC_BUTTON_SELECT_AREA_5;
	m_AreaButtonIDList[5] = IDC_BUTTON_SELECT_AREA_6;
	m_AreaButtonIDList[6] = IDC_BUTTON_SELECT_AREA_7;
	m_AreaButtonIDList[7] = IDC_BUTTON_SELECT_AREA_8;
	m_AreaButtonIDList[8] = IDC_BUTTON_SELECT_AREA_9;
	m_AreaButtonIDList[9] = IDC_BUTTON_SELECT_AREA_10;

	m_ServerButtonIDList[0] = IDC_BUTTON_SELECT_SERVER_1;
	m_ServerButtonIDList[1] = IDC_BUTTON_SELECT_SERVER_2;
	m_ServerButtonIDList[2] = IDC_BUTTON_SELECT_SERVER_3;
	m_ServerButtonIDList[3] = IDC_BUTTON_SELECT_SERVER_4;
	m_ServerButtonIDList[4] = IDC_BUTTON_SELECT_SERVER_5;
	m_ServerButtonIDList[5] = IDC_BUTTON_SELECT_SERVER_6;
	m_ServerButtonIDList[6] = IDC_BUTTON_SELECT_SERVER_7;
	m_ServerButtonIDList[7] = IDC_BUTTON_SELECT_SERVER_8;
	m_ServerButtonIDList[8] = IDC_BUTTON_SELECT_SERVER_9;
	m_ServerButtonIDList[9] = IDC_BUTTON_SELECT_SERVER_10;
	m_ServerButtonIDList[10] = IDC_BUTTON_SELECT_SERVER_11;
	m_ServerButtonIDList[11] = IDC_BUTTON_SELECT_SERVER_12;

	m_iTopIndex = 0;
	
	m_iSelectCatIndex = 0;
	m_iSelectAreaIndex = 0;
	m_iSelectServerIndex = 0;

	m_PatchCliIntf = CreatePatchIntf(NULL);
	m_ucServerNum = 0;
}

CSelectDlg::~CSelectDlg()
{
	DeletePatchIntf(m_PatchCliIntf);
}

void CSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BOOL CSelectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CDialogConfig* pDialogConfig = ConfigManager::GetInstance()->GetDialogConfig(_T("SelectDlg"));
	TiXmlElement* pXmlDialogConfig = pDialogConfig->GetConfig();
	
	TiXmlElement* pXmlButtonOk = pDialogConfig->GetChildConfig(_T("Ok"));
	TiXmlElement* pXmlButtonCancel = pDialogConfig->GetChildConfig(_T("Cancel"));
	TiXmlElement* pXmlButtonClose = pDialogConfig->GetChildConfig(_T("Close"));
	TiXmlElement* pXmlButtonAreaList = pDialogConfig->GetChildConfig(_T("AreaList"));
	TiXmlElement* pXmlButtonServerList = pDialogConfig->GetChildConfig(_T("ServerList"));

	TiXmlElement* pXmlImageScrollBG = pDialogConfig->GetChildConfig(_T("ScrollBG"));
	TiXmlElement* pXmlButtonScrollUp = pDialogConfig->GetChildConfig(_T("ScrollUp"));
	TiXmlElement* pXmlButtonScrollBar = pDialogConfig->GetChildConfig(_T("ScrollBar"));
	TiXmlElement* pXmlButtonScrollDown = pDialogConfig->GetChildConfig(_T("ScrollDown"));


	// 初始化自身
	//this->MoveWindow(0, 0, 471, 490);
	m_strBkgndImageName = pXmlDialogConfig->Attribute("BGImage");
	this->MoveWindow(pXmlDialogConfig->IntAttribute("Left"), pXmlDialogConfig->IntAttribute("Top"), pXmlDialogConfig->IntAttribute("Width"), pXmlDialogConfig->IntAttribute("Height"));
	this->CenterWindow();

	// 初始化按钮
	/*this->GetDlgItem(IDC_BUTTON_SELECT_OK)->MoveWindow(145, 450, 73, 23);
	m_ImageButtonOK.InitButton(IDC_BUTTON_SELECT_OK, this, L"launcherrealm\\btn_confirm.bmp", 73, 23);

	this->GetDlgItem(IDC_BUTTON_SELECT_CANCEL)->MoveWindow(251, 450, 73, 23);
	m_ImageButtonCancel.InitButton(IDC_BUTTON_SELECT_CANCEL, this, L"launcherrealm\\btn_cancel.bmp", 73, 23);

	this->GetDlgItem(IDC_BUTTON_SELECT_CLOSE)->MoveWindow(435, 10, 20, 19);
	m_ImageButtonClose.InitButton(IDC_BUTTON_SELECT_CLOSE, this, _T("launcherbmp\\btn_close.bmp"), 20, 19);*/
	
	this->GetDlgItem(IDC_BUTTON_SELECT_OK)->MoveWindow(pXmlButtonOk->IntAttribute("Left"), pXmlButtonOk->IntAttribute("Top"), pXmlButtonOk->IntAttribute("Width"), pXmlButtonOk->IntAttribute("Height"));
	//m_ImageButtonOK.InitButton(IDC_BUTTON_SELECT_OK, this, CString(pXmlButtonOk->Attribute("BGImage")), pXmlButtonOk->IntAttribute("Width"), pXmlButtonOk->IntAttribute("Height"));
	m_ImageButtonOK.InitButton(IDC_BUTTON_SELECT_OK,this,CString(pXmlButtonOk->Attribute("NormalImage")),CString(pXmlButtonOk->Attribute("HoverImage")),CString(pXmlButtonOk->Attribute("PressedImage")));

	this->GetDlgItem(IDC_BUTTON_SELECT_CANCEL)->MoveWindow(pXmlButtonCancel->IntAttribute("Left"), pXmlButtonCancel->IntAttribute("Top"), pXmlButtonCancel->IntAttribute("Width"), pXmlButtonCancel->IntAttribute("Height"));
	//m_ImageButtonCancel.InitButton(IDC_BUTTON_SELECT_CANCEL, this, CString(pXmlButtonCancel->Attribute("BGImage")), pXmlButtonCancel->IntAttribute("Width"), pXmlButtonCancel->IntAttribute("Height"));
	m_ImageButtonCancel.InitButton(IDC_BUTTON_SELECT_CANCEL,this,CString(pXmlButtonCancel->Attribute("NormalImage")),CString(pXmlButtonCancel->Attribute("HoverImage")),CString(pXmlButtonCancel->Attribute("PressedImage")));

	this->GetDlgItem(IDC_BUTTON_SELECT_CLOSE)->MoveWindow(pXmlButtonClose->IntAttribute("Left"), pXmlButtonClose->IntAttribute("Top"), pXmlButtonClose->IntAttribute("Width"), pXmlButtonClose->IntAttribute("Height"));
	//m_ImageButtonClose.InitButton(IDC_BUTTON_SELECT_CLOSE, this, CString(pXmlButtonClose->Attribute("BGImage")), pXmlButtonClose->IntAttribute("Width"), pXmlButtonClose->IntAttribute("Height"));
	m_ImageButtonClose.InitButton(IDC_BUTTON_SELECT_CLOSE,this,CString(pXmlButtonClose->Attribute("NormalImage")),CString(pXmlButtonClose->Attribute("HoverImage")),CString(pXmlButtonClose->Attribute("PressedImage")));

	//初始化Area列表
	for (int iIndex = 0;iIndex < MAX_SELECT_AREA_NUM;++iIndex)
	{
		// 获取属性
		int iLeft = pXmlButtonAreaList->IntAttribute("Left");
		int iTop = pXmlButtonAreaList->IntAttribute("Top");
		int iWidth = pXmlButtonAreaList->IntAttribute("Width");
		int iHeight = pXmlButtonAreaList->IntAttribute("Height");
		int iRange = pXmlButtonAreaList->IntAttribute("Range");
		//CString strBGImage = CString(pXmlButtonAreaList->Attribute("BGImage"));
		CString strNImage = CString(pXmlButtonAreaList->Attribute("NormalImage"));
		CString strHImage = CString(pXmlButtonAreaList->Attribute("HoverImage"));
		CString strPImage = CString(pXmlButtonAreaList->Attribute("PressedImage"));

		// 设置
		//this->GetDlgItem(m_AreaButtonIDList[iIndex])->MoveWindow(21, 88+iIndex*34, 105, 29);
		this->GetDlgItem(m_AreaButtonIDList[iIndex])->MoveWindow(iLeft, iTop+iIndex*iRange, iWidth, iHeight);
		//m_AreaButtonList[iIndex].InitButton(m_AreaButtonIDList[iIndex], this, _T("launcherrealm\\realmbtn.bmp"), 105, 29, FALSE);
		//m_AreaButtonList[iIndex].InitButton(m_AreaButtonIDList[iIndex], this, strBGImage, iWidth, iHeight, FALSE);
		m_AreaButtonList[iIndex].InitButton(m_AreaButtonIDList[iIndex],this,strNImage,strHImage,strPImage);
		m_AreaButtonList[iIndex].SetIsCheckBox(TRUE);
		//m_AreaButtonList[iIndex].SetCheckBoxCheckedImage(_T("launcherrealm\\realmbtn.bmp"), 420, 0, 105, 29);
		m_AreaButtonList[iIndex].SetCheckBoxCheckedImage(strPImage, iWidth*4, 0, iWidth, iHeight);

		CColorText text(_T(""), new CRect(0, 0, 105, 29), AREA_NAME_COLOR);

		m_AreaButtonList[iIndex].AddDisplayText(&text);

		m_AreaButtonList[iIndex].ShowWindow(SW_HIDE);
		/*m_AreaButtonList[iIndex].SetTransparent(TRUE);
		m_AreaButtonList[iIndex].SetTransparentImage(_T("launcherrealm\\realmbg.bmp"), 21, 88+iIndex*34, 105, 29);*/
	}
	
	//初始化Server列表
	for (int iIndex = 0;iIndex < MAX_SELECT_SERVER_NUM;++iIndex)
	{
		// 获取属性
		int iLeft = pXmlButtonServerList->IntAttribute("Left");
		int iTop = pXmlButtonServerList->IntAttribute("Top");
		int iWidth = pXmlButtonServerList->IntAttribute("Width");
		int iHeight = pXmlButtonServerList->IntAttribute("Height");
		int iRange = pXmlButtonServerList->IntAttribute("Range");
		CString strSelectImage = CString(pXmlButtonServerList->Attribute("BGImage"));
		CString strHoverImage = CString(pXmlButtonServerList->Attribute("HoverImage"));
		CString strBGImage = CString(pXmlDialogConfig->Attribute("BGImage"));

		// 设置
		//this->GetDlgItem(m_ServerButtonIDList[iIndex])->MoveWindow(137, 84+iIndex*28, 296, 26);
		this->GetDlgItem(m_ServerButtonIDList[iIndex])->MoveWindow(iLeft, iTop+iIndex*iRange, iWidth, iHeight);
		//m_ServerButtonList[iIndex].InitButton(m_ServerButtonIDList[iIndex], this, _T("launcherrealm\\selected.bmp"), _T("launcherrealm\\hover.bmp"), _T("launcherrealm\\selected.bmp"));
		m_ServerButtonList[iIndex].InitButton(m_ServerButtonIDList[iIndex], this, strSelectImage, strHoverImage, strSelectImage);
		m_ServerButtonList[iIndex].SetIsCheckBox(TRUE);
		//m_ServerButtonList[iIndex].SetCheckBoxCheckedImage(_T("launcherrealm\\selected.bmp"), 0, 0, 296, 26);
		m_ServerButtonList[iIndex].ShowWindow(SW_HIDE);

		// 服务器名字字体
		CFont nameFont;
		nameFont.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体"));
		CColorText nameText(_T(""), new CRect(18, 0, 170, 29), SERVER_NAME_COLOR, &nameFont);
		nameText.SetFormat(DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		m_ServerButtonList[iIndex].AddDisplayText(&nameText);
		
		// 负载字体
		CFont overFont;
		overFont.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体"));
		CColorText overText(_T(""), new CRect(170, 0, 200, 29), SERVER_GREE, &overFont);
		overText.SetFormat(DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		m_ServerButtonList[iIndex].AddDisplayText(&overText);

		// 延迟字体
		CFont delayFont;
		delayFont.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("宋体"));
		CColorText delayText(_T(""), new CRect(255, 0, 295, 29), SERVER_GREE, &delayFont);
		delayText.SetFormat(DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		m_ServerButtonList[iIndex].AddDisplayText(&delayText);

		// 背景透明
		m_ServerButtonList[iIndex].SetTransparent(TRUE);
		//m_ServerButtonList[iIndex].SetTransparentImage(_T("launcherrealm\\realmbg.bmp"), 137, 84+iIndex*28, 296, 26);
		m_ServerButtonList[iIndex].SetTransparentImage(strBGImage, iLeft, iTop+iIndex*iRange, iWidth, iHeight);

	}

	// 显示等待信息
	m_ServerButtonList[0].ChangeDisplayText(0, _T("正在获取服务器列表......"), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ChangeDisplayText(1, _T(""), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ChangeDisplayText(2, _T(""), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ShowWindow(SW_SHOW);

	// 初始化ScrollBar
	/*MyLoadImage(_T("launcherrealm\\scrollbar_bg.bmp"), &m_pImageScrollBar, &m_rectScrollBar, 435, 80);
	
	this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_UP)->MoveWindow(434, 78, 17, 17);
	m_ImageButtonScrollUp.InitButton(IDC_BUTTON_SELECT_SCROLL_UP, this, _T("launcherrealm\\scrollbar_up.bmp"), 17, 17, FALSE);
	
	this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_BAR)->MoveWindow(434, 134, 17, 60);
	m_ImageButtonScrollBar.InitButton(IDC_BUTTON_SELECT_SCROLL_BAR, this, _T("launcherrealm\\scrollbar_bar.bmp"), 17, 60, FALSE);

	this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_DOWN)->MoveWindow(434, 404, 17, 17);
	m_ImageButtonScrollDown.InitButton(IDC_BUTTON_SELECT_SCROLL_DOWN, this, _T("launcherrealm\\scrollbar_down.bmp"), 17, 17, FALSE);*/

	MyLoadImage(CString(pXmlImageScrollBG->Attribute("BGImage")), &m_pImageScrollBar, &m_rectScrollBar, pXmlImageScrollBG->IntAttribute("Left"), pXmlImageScrollBG->IntAttribute("Top"));
	
	this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_UP)->MoveWindow(pXmlButtonScrollUp->IntAttribute("Left"), pXmlButtonScrollUp->IntAttribute("Top"), pXmlButtonScrollUp->IntAttribute("Width"), pXmlButtonScrollUp->IntAttribute("Height"));
	//m_ImageButtonScrollUp.InitButton(IDC_BUTTON_SELECT_SCROLL_UP, this, CString(pXmlButtonScrollUp->Attribute("BGImage")), pXmlButtonScrollUp->IntAttribute("Width"), pXmlButtonScrollUp->IntAttribute("Height"), FALSE);
	m_ImageButtonScrollUp.InitButton(IDC_BUTTON_SELECT_SCROLL_UP,this,CString(pXmlButtonScrollUp->Attribute("NormalImage")),CString(pXmlButtonScrollUp->Attribute("HoverImage")),CString(pXmlButtonScrollUp->Attribute("PressedImage")));

	this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_BAR)->MoveWindow(pXmlButtonScrollBar->IntAttribute("Left"), pXmlButtonScrollBar->IntAttribute("Top"), pXmlButtonScrollBar->IntAttribute("Width"), pXmlButtonScrollBar->IntAttribute("Height"));
	//m_ImageButtonScrollBar.InitButton(IDC_BUTTON_SELECT_SCROLL_BAR, this, CString(pXmlButtonScrollBar->Attribute("BGImage")), pXmlButtonScrollBar->IntAttribute("Width"), pXmlButtonScrollBar->IntAttribute("Height"), FALSE);
	m_ImageButtonScrollBar.InitButton(IDC_BUTTON_SELECT_SCROLL_BAR,this,CString(pXmlButtonScrollBar->Attribute("NormalImage")),CString(pXmlButtonScrollBar->Attribute("HoverImage")),CString(pXmlButtonScrollBar->Attribute("PressedImage")));

	this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_DOWN)->MoveWindow(pXmlButtonScrollDown->IntAttribute("Left"), pXmlButtonScrollDown->IntAttribute("Top"), pXmlButtonScrollDown->IntAttribute("Width"), pXmlButtonScrollDown->IntAttribute("Height"));
	//m_ImageButtonScrollDown.InitButton(IDC_BUTTON_SELECT_SCROLL_DOWN, this, CString(pXmlButtonScrollDown->Attribute("BGImage")), pXmlButtonScrollDown->IntAttribute("Width"), pXmlButtonScrollDown->IntAttribute("Height"), FALSE);
	m_ImageButtonScrollDown.InitButton(IDC_BUTTON_SELECT_SCROLL_DOWN,this,CString(pXmlButtonScrollDown->Attribute("NormalImage")),CString(pXmlButtonScrollDown->Attribute("HoverImage")),CString(pXmlButtonScrollDown->Attribute("PressedImage")));

	// 先行显示
	this->ShowWindow(SW_SHOW);
	this->UpdateWindow();

	// 计算scrollbar的停靠位置
	m_barTop = pXmlButtonScrollUp->IntAttribute("Top") + pXmlButtonScrollUp->IntAttribute("Height") - 1;
	m_barBottom = pXmlButtonScrollDown->IntAttribute("Top") - pXmlButtonScrollBar->IntAttribute("Height") + 1;
	ConfigManager::GetInstance()->GetScrollBarPosList(m_barTop, m_barBottom, 20, m_barPosList, m_barRangeList);
	//ConfigManager::GetInstance()->GetScrollBarPosList(94, 345, 20, m_barPosList, m_barRangeList);
	//MoveBarByTopIndex(m_iTopIndex);

	// 最后调整大小
	
	SetupRegion(GetDC(), pDialogConfig->GetShapeName(), RGB(255, 0, 255));

	return TRUE;
}

BOOL CSelectDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
		case VK_RETURN:
			return TRUE;
		case VK_ESCAPE:
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CSelectDlg::SetupRegion(CDC * pDC, CString strBitmapFileName, COLORREF crTransparent)
{
	// 获取配置信息
	CShapeConfig* pShapeConfig = ConfigManager::GetInstance()->GetShapeConfig(strBitmapFileName);
	vector<CRect*>& arrWorkRect = pShapeConfig->GetWorkRectList();

	// 定义工作区
	/*CRect arrWorkRect[4];
	int	iWorkRectNum = 4;
	arrWorkRect[0].SetRect(0, 0, 5, 6);
	arrWorkRect[1].SetRect(466, 0, 470, 5);
	arrWorkRect[2].SetRect(0, 483, 5, 489);
	arrWorkRect[3].SetRect(465, 484, 470, 489);*/

	// 定义变量
	CDC memDC;
	CBitmap* pBitmap;
	CBitmap* pOldMemBmp = NULL;
	COLORREF cl;
	CRect cRect;
	int x, y;
	CRgn wndRgn, rgnTemp;

	// 获得窗口大小
	GetWindowRect(&cRect);

	// 加载位图资源
	HBITMAP temp = (HBITMAP)LoadImage(NULL, pShapeConfig->GetImageName(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		pBitmap = CBitmap::FromHandle(temp);
		memDC.CreateCompatibleDC(pDC);
		pOldMemBmp = memDC.SelectObject(pBitmap);

		// 创建默认区域
		wndRgn.CreateRectRgn(0, 0, cRect.Width(), cRect.Height());

		// 建立窗口
		//for (x = 0;x <= cRect.Width();++x)
		//{
		//	for (y = 0;y <= cRect.Height();++y)
		//	{
		//		// 获取坐标处的像素值
		//		cl = memDC.GetPixel(x, y);

		//		// 扣除
		//		if (cl == crTransparent)
		//		{
		//			rgnTemp.CreateRectRgn(x, y, x+1, y+1);
		//			wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
		//			rgnTemp.DeleteObject();
		//		}
		//	}
		//}
		for(int nRectIndex = 0;nRectIndex < arrWorkRect.size();++nRectIndex)
		{
			CRect* rectWork = arrWorkRect[nRectIndex];

			for (x = rectWork->left;x <= rectWork->right;++x)
			{
				for (y = rectWork->top;y <= rectWork->bottom;++y)
				{
					// 获取坐标处的像素值
					cl = memDC.GetPixel(x, y);

					// 扣除
					if (cl == pShapeConfig->GetTransparentColor())
					{
						rgnTemp.CreateRectRgn(x, y, x+1, y+1);
						wndRgn.CombineRgn(&wndRgn, &rgnTemp, RGN_XOR);
						rgnTemp.DeleteObject();

					}
				}
			}
		}

		// 清除资源
		pBitmap->DeleteObject();
		memDC.SelectObject(pOldMemBmp);
		memDC.DeleteDC();

		// 指定窗口
		SetWindowRgn((HRGN)wndRgn, TRUE);
	}
}

BOOL CSelectDlg::MyLoadImage(CString strFileName, CBitmap ** pImage, CRect* rect, int iPosX, int iPosY)
{
	BITMAP hBitmap;
	HBITMAP temp = (HBITMAP)LoadImage(NULL, strFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (temp != NULL)
	{
		*pImage = CBitmap::FromHandle(temp);

		(*pImage)->GetBitmap(&hBitmap);

		rect->SetRect(iPosX, iPosY, iPosX+hBitmap.bmWidth, iPosY+hBitmap.bmHeight);

		return TRUE;
	}
	else
	{
		*pImage = NULL;
		rect->SetRect(0, 0, 0, 0);
		return FALSE;
	}
}

BOOL CSelectDlg::MyShowImage(CDC *dc, CBitmap *pImage, CRect *rect, COLORREF crTransparent)
{
	if (dc == NULL || pImage == NULL || rect == NULL)
	{
		return FALSE;
	}
	else
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		CBitmap* pOldBmp = memDC.SelectObject(pImage);

		TransparentBlt(dc->m_hDC, rect->left, rect->top, rect->Width(), rect->Height(), memDC.m_hDC, 0, 0, rect->Width(), rect->Height(), crTransparent);

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();

		return TRUE;
	}
}

int CSelectDlg::InitAreaList(int iCatId, int iAreaId)
{
	unsigned char ucAreaNum;
	m_pAreaInfo = m_PatchCliIntf->GetAreaInfo(iCatId, ucAreaNum);
	int iSelectAreaIndex = 0;

	for (int i = 0;i < MAX_SELECT_AREA_NUM;++i)
	{
		if (i < ucAreaNum)
		{
			CString strAreaName;
			strAreaName = m_pAreaInfo[i].szAreaName;
			int id = m_pAreaInfo[i].nAreaID;

			m_AreaButtonList[i].ChangeDisplayText(0, strAreaName, AREA_NAME_COLOR);
			
			if (id == iAreaId)
			{
				m_AreaButtonList[i].SetCheckBoxValue(TRUE);
				iSelectAreaIndex = i;
			}
			else
			{
				m_AreaButtonList[i].SetCheckBoxValue(FALSE);
			}

			this->GetDlgItem(m_AreaButtonIDList[i])->ShowWindow(SW_SHOW);
		}
		else
		{
			this->GetDlgItem(m_AreaButtonIDList[i])->ShowWindow(SW_HIDE);
		}
	}

	if (ucAreaNum > 0)
	{
		m_AreaButtonList[iSelectAreaIndex].SetCheckBoxValue(TRUE);
		m_iSelectAreaID = m_pAreaInfo[iSelectAreaIndex].nAreaID;
		return iSelectAreaIndex;
	} 
	else
	{
		return -1;
	}
}

int CSelectDlg::InitServerList(int iAreaId, int iServerId)
{
	unsigned char ucServerNum;
	m_pServerInfo = m_PatchCliIntf->GetRegionInfo(iAreaId, ucServerNum);
	m_ucServerNum = ucServerNum;

	int iSelectButtonIndex = 0;

	// 计算scrollbar
	//ConfigManager::GetInstance()->GetScrollBarPosList(94, 345, ucServerNum - 11, m_barPosList, m_barRangeList);
	ConfigManager::GetInstance()->GetScrollBarPosList(m_barTop, m_barBottom, ucServerNum - 11, m_barPosList, m_barRangeList);

	m_iTopIndex = 0;
	MoveBarByTopIndex(m_iTopIndex);

	for (int i = 0;i < MAX_SELECT_SERVER_NUM;++i)
	{
		if (i < ucServerNum)
		{
			int id = m_pServerInfo[i].nServerID;
			SetServerButton(i, i);

			if (id == iServerId)
			{
				m_ServerButtonList[i].SetCheckBoxValue(TRUE);
				iSelectButtonIndex = i;
			}
			else
			{
				m_ServerButtonList[i].SetCheckBoxValue(FALSE);
			}

			this->GetDlgItem(m_ServerButtonIDList[i])->ShowWindow(SW_SHOW);
		}
		else
		{
			this->GetDlgItem(m_ServerButtonIDList[i])->ShowWindow(SW_HIDE);
		}

		this->GetDlgItem(m_ServerButtonIDList[i])->Invalidate();
	}

	if(ucServerNum > 0)
	{
		m_ServerButtonList[iSelectButtonIndex].SetCheckBoxValue(TRUE);
		m_iSelectServerID = m_pServerInfo[iSelectButtonIndex].nServerID;
		m_strSelectServerName = m_pServerInfo[iSelectButtonIndex].szServerName;
		return iSelectButtonIndex;
	}
	else
	{
		return -1;
	}
}

void CSelectDlg::SetServerButton(int index, int InfoIndex)
{
	if (index < MAX_SELECT_SERVER_NUM)
	{
		// 获取数据
		CString strServerName;
		strServerName = m_pServerInfo[InfoIndex].szServerName;
		unsigned int iPingValue = m_pServerInfo[InfoIndex].PintValue;
		CString strPingValue;
		strPingValue.Format(_T("%dms"), iPingValue);

		m_ServerButtonList[index].ChangeDisplayText(0, strServerName, SERVER_NAME_COLOR);

		if (iPingValue == -1)
		{
			m_ServerButtonList[index].ChangeDisplayText(1, _T("N/A"), SERVER_GRAY);
			m_ServerButtonList[index].ChangeDisplayText(2, _T("N/A"), SERVER_GRAY);	
		}
		else if (iPingValue <= 300)
		{
			m_ServerButtonList[index].ChangeDisplayText(1, _T("低"), SERVER_GREE);
			m_ServerButtonList[index].ChangeDisplayText(2, strPingValue, SERVER_GREE);	
		}
		else if (iPingValue <= 600)
		{
			m_ServerButtonList[index].ChangeDisplayText(1, _T("中"), SERVER_YELLOW);
			m_ServerButtonList[index].ChangeDisplayText(2, strPingValue, SERVER_YELLOW);	
		}
		else 
		{
			strPingValue = _T("600ms+");
			m_ServerButtonList[index].ChangeDisplayText(1, _T("高"), SERVER_RED);
			m_ServerButtonList[index].ChangeDisplayText(2, strPingValue, SERVER_RED);	
		}

	}
}

LRESULT CSelectDlg::OnButtonDrag(WPARAM wParam, LPARAM lParam)
{
	if (wParam == IDC_BUTTON_SELECT_SCROLL_BAR)
	{
		CRect posrect;
		this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_BAR)->GetWindowRect(&posrect);
		this->ScreenToClient(&posrect);

		int dy = LOBYTE(HIWORD(lParam));
		int direction = HIBYTE(HIWORD(lParam));
		if (direction == 2)
		{
			dy = -dy;
		}

		CRect rect;
		rect.SetRect(posrect.left, posrect.top + dy, posrect.right, posrect.bottom + dy);
		if (rect.left < 434 || rect.right > 451 || rect.top < 94 || rect.bottom > 404)
		{
		}
		else
		{
			this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_BAR)->MoveWindow(&rect);
			int iTemp = GetTopIndexByBarPos(rect.top);
			if (iTemp != m_iTopIndex)
			{
				m_iTopIndex = iTemp;
				MoveListByTopIndex(m_iTopIndex);
			}
		}
	}
	return 0;
}

LRESULT CSelectDlg::OnBarLButtonUp(WPARAM wParam, LPARAM lParam)
{
	if (wParam == IDC_BUTTON_SELECT_SCROLL_BAR)
	{
		CRect posrect;
		this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_BAR)->GetWindowRect(&posrect);
		this->ScreenToClient(&posrect);

		m_iTopIndex = GetTopIndexByBarPos(posrect.top);
		MoveBarByTopIndex(m_iTopIndex);
	}
	return 0;
}

LRESULT CSelectDlg::OnBarLBLongClick(WPARAM wParam, LPARAM lParam)
{
	if (wParam == IDC_BUTTON_SELECT_SCROLL_UP)
	{
		OnBnClickedButtonSelectScrollUp();		
	}
	else if (wParam == IDC_BUTTON_SELECT_SCROLL_DOWN)
	{
		OnBnClickedButtonSelectScrollDown();
	}

	return 0;
}

int CSelectDlg::GetTopIndexByBarPos(int pos)
{
	for (int i = 0;i < (int)m_barRangeList.size();++i)
	{
		CPoint range = m_barRangeList[i];
		if (pos >= range.x && pos <= range.y)
		{
			return i;
		}
	}

	return 0;
}

void CSelectDlg::MoveBarByTopIndex(int top)
{
	int iTopPos = m_barPosList[top];

	CRect rect;

	rect.SetRect(434, iTopPos, 451, iTopPos+60);
	this->GetDlgItem(IDC_BUTTON_SELECT_SCROLL_BAR)->MoveWindow(&rect);
	MoveListByTopIndex(top);
}

void CSelectDlg::MoveListByTopIndex(int top)
{
	int iTopIndex = top;
	for (int i = 0;i < MAX_SELECT_SERVER_NUM;++i)
	{
		if (iTopIndex < m_ucServerNum)
		{
			int id = m_pServerInfo[iTopIndex].nServerID;
			this->GetDlgItem(m_ServerButtonIDList[i])->ShowWindow(SW_SHOW);
			SetServerButton(i, iTopIndex);

			if (id == m_iSelectServerID)
			{
				m_ServerButtonList[i].SetCheckBoxValue(TRUE);	
			}
			else
			{
				m_ServerButtonList[i].SetCheckBoxValue(FALSE);
			}
		}
		else
		{
			this->GetDlgItem(m_ServerButtonIDList[i])->ShowWindow(SW_HIDE);
		}

		this->GetDlgItem(m_ServerButtonIDList[i])->Invalidate();

		iTopIndex++;
	}
}

void CSelectDlg::ShowSelect(int iCatIndex, int iCatId, int iAreaId, int iServerId)
{
	// 屏蔽显示
	for (int iIndex = 0;iIndex < MAX_SELECT_AREA_NUM;++iIndex)
	{	
		m_AreaButtonList[iIndex].ShowWindow(SW_HIDE);
	}
	for (int iIndex = 0;iIndex < MAX_SELECT_SERVER_NUM;++iIndex)
	{
		m_ServerButtonList[iIndex].ShowWindow(SW_HIDE);
	}

	// 显示等待信息
	m_ServerButtonList[0].ChangeDisplayText(0, _T("正在获取服务器列表......"), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ChangeDisplayText(1, _T(""), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ChangeDisplayText(2, _T(""), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ShowWindow(SW_SHOW);

	// 记录下初始的数据
	m_iSelectCatIndex = iCatIndex;
	m_iSelectCatID = iCatId;
	m_iSelectAreaID = iAreaId;
	m_iSelectServerID = iServerId;

	// 先行显示
	::AfxGetMainWnd()->EnableWindow(FALSE);
	this->ShowWindow(SW_SHOW);
	this->UpdateWindow();

	// 先初始化Area列表
	m_iSelectAreaIndex = InitAreaList(iCatId, iAreaId);
	
	// 再初始化Server列表
	if (m_iSelectAreaIndex != -1)
	{
		m_iSelectServerIndex = InitServerList(m_pAreaInfo[m_iSelectAreaIndex].nAreaID, iServerId);
	}

	// 刷新显示
	this->Invalidate();
}

BEGIN_MESSAGE_MAP(CSelectDlg, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON_SELECT_OK, &CSelectDlg::OnBnClickedButtonSelectOk)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_CANCEL, &CSelectDlg::OnBnClickedButtonSelectCancel)
	ON_MESSAGE(FBN_DRAG, OnButtonDrag)
	ON_MESSAGE(FBN_LBUTTONUP, OnBarLButtonUp)
	ON_MESSAGE(FBN_LBLONGCLICK, OnBarLBLongClick)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_SCROLL_UP, &CSelectDlg::OnBnClickedButtonSelectScrollUp)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_SCROLL_DOWN, &CSelectDlg::OnBnClickedButtonSelectScrollDown)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_BUTTON_SELECT_SERVER_1, IDC_BUTTON_SELECT_SERVER_12, &CSelectDlg::OnServerButtonClicked)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_BUTTON_SELECT_AREA_1, IDC_BUTTON_SELECT_AREA_10, &CSelectDlg::OnAreaButtonClicked)
	ON_WM_CLOSE()
	ON_CONTROL_RANGE(BN_DOUBLECLICKED, IDC_BUTTON_SELECT_SERVER_1, IDC_BUTTON_SELECT_SERVER_12, &CSelectDlg::OnServerButtonDoubleclicked)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_CLOSE, &CSelectDlg::OnBnClickedButtonSelectClose)
END_MESSAGE_MAP()


// CSelectDlg 消息处理程序

BOOL CSelectDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_pBkgndImage == NULL)
	{
		HBITMAP temp = (HBITMAP)LoadImage(NULL, m_strBkgndImageName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		if (temp != NULL)
		{
			m_pBkgndImage = CBitmap::FromHandle(temp);
		}
		else
		{
			m_pBkgndImage = NULL;
		}
	}

	if (m_pBkgndImage != NULL)
	{
		CDC memDC;
		memDC.CreateCompatibleDC(NULL);

		BITMAP hBitmap;
		m_pBkgndImage->GetBitmap(&hBitmap);

		CBitmap* pOldBmp = memDC.SelectObject(m_pBkgndImage);

		//pDC->BitBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, SRCCOPY);
		pDC->TransparentBlt(0, 0, hBitmap.bmWidth, hBitmap.bmHeight, &memDC, 0, 0, hBitmap.bmWidth, hBitmap.bmHeight, RGB(255, 0, 255));

		memDC.SelectObject(pOldBmp);

		memDC.DeleteDC();
		
		// 控件显示
		MyShowImage(pDC, m_pImageScrollBar, &m_rectScrollBar, RGB(255, 0, 255));
		/*m_ImageButtonOK.Invalidate();
		m_ImageButtonCancel.Invalidate();
		m_ImageButtonScrollUp.Invalidate();
		m_ImageButtonScrollBar.Invalidate();
		m_ImageButtonScrollDown.Invalidate();*/

		return TRUE;
	}
	else
	{
		CDialog::OnEraseBkgnd(pDC);

		CRect rect;
		this->GetClientRect(&rect);
		pDC->FillSolidRect(&rect, RGB(150, 150, 150));

		return TRUE;
	}
}

void CSelectDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	
	//禁止显示移动矩形窗体框
	::SystemParametersInfo(SPI_SETDRAGFULLWINDOWS,TRUE,NULL,0);
	//非标题栏移动整个窗口
	SendMessage(WM_SYSCOMMAND,0xF012,0); 

	CDialog::OnLButtonDown(nFlags, point);
}

void CSelectDlg::OnBnClickedButtonSelectOk()
{
	// TODO: 在此添加控件通知处理程序代码
	ConfigManager::GetInstance()->SetOutPutData(m_iSelectCatID, m_iSelectAreaID, m_iSelectServerID);
	ConfigManager::GetInstance()->SaveGameConfigure("UserData/Configuration.xml");

	//m_iDefaultServer = m_iSelectCatIndex;
	//g_szServerName = m_strSelectServerName;
	::AfxGetMainWnd()->EnableWindow(TRUE);
	this->ShowWindow(SW_HIDE);
	//this->GetParent()->SendMessage(WM_SERVER_DLG_UPDATE, 0, 0);
	m_pRealParent->SendMessage(WM_SERVER_DLG_UPDATE, 0, 0);
}

void CSelectDlg::OnBnClickedButtonSelectCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	::AfxGetMainWnd()->EnableWindow(TRUE);
	this->ShowWindow(SW_HIDE);
	//this->GetParent()->SendMessage(WM_SERVER_DLG_UPDATE, 0, 0);
	m_pRealParent->SendMessage(WM_SERVER_DLG_UPDATE, 0, 0);
}

void CSelectDlg::OnBnClickedButtonSelectScrollUp()
{
	// TODO: 在此添加控件通知处理程序代码
	int iNewIndex = m_iTopIndex - 1;
	if (iNewIndex >= 0)
	{
		m_iTopIndex = iNewIndex;
		MoveBarByTopIndex(m_iTopIndex);
	}
}

void CSelectDlg::OnBnClickedButtonSelectScrollDown()
{
	// TODO: 在此添加控件通知处理程序代码
	int iNewIndex = m_iTopIndex + 1;
	int iSize = m_barPosList.size() - 1;
	if (iNewIndex <= iSize)
	{
		m_iTopIndex = iNewIndex;
		MoveBarByTopIndex(m_iTopIndex);
	}
}

void CSelectDlg::OnServerButtonClicked(UINT uID)
{
	// 首先取消上次的选择
	m_ServerButtonList[m_iSelectServerIndex].SetCheckBoxValue(FALSE);

	// 根据ID寻找index
	for(int i = 0;i < MAX_SELECT_SERVER_NUM;++i)
	{
		if (uID == m_ServerButtonIDList[i])
		{
			m_iSelectServerIndex = i;
			m_ServerButtonList[i].SetCheckBoxValue(TRUE);
			int realIndex = i + m_iTopIndex;
			m_iSelectServerID = m_pServerInfo[realIndex].nServerID;
			m_strSelectServerName = m_pServerInfo[realIndex].szServerName;
		}
		else
		{
			m_ServerButtonList[i].SetCheckBoxValue(FALSE);
		}
	}
}

void CSelectDlg::OnAreaButtonClicked(UINT uID)
{
	// 取消上次的选择
	m_AreaButtonList[m_iSelectAreaIndex].SetCheckBoxValue(FALSE);

	// 屏蔽显示
	for (int iIndex = 0;iIndex < MAX_SELECT_SERVER_NUM;++iIndex)
	{
		m_ServerButtonList[iIndex].ShowWindow(SW_HIDE);
	}

	// 显示等待信息
	m_ServerButtonList[0].ChangeDisplayText(0, _T("正在获取服务器列表......"), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ChangeDisplayText(1, _T(""), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ChangeDisplayText(2, _T(""), SERVER_NAME_COLOR);
	m_ServerButtonList[0].ShowWindow(SW_SHOW);
	
	this->UpdateWindow();

	for (int i = 0;i < MAX_SELECT_AREA_NUM;++i)
	{
		if (uID == m_AreaButtonIDList[i])
		{
			m_iSelectAreaIndex = i;
			m_AreaButtonList[i].SetCheckBoxValue(TRUE);
			m_iSelectAreaID = m_pAreaInfo[i].nAreaID;
			m_iSelectServerIndex = InitServerList(m_iSelectAreaID, -1);
		}
		else
		{
			m_AreaButtonList[i].SetCheckBoxValue(FALSE);
		}
	}
}
void CSelectDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	m_pRealParent->SendMessage(WM_SERVER_DLG_UPDATE, 0, 0);
	CDialog::OnClose();
}

void CSelectDlg::OnServerButtonDoubleclicked(UINT uID)
{
	this->OnBnClickedButtonSelectOk();
}
void CSelectDlg::OnBnClickedButtonSelectClose()
{
	// TODO: 在此添加控件通知处理程序代码
	::AfxGetMainWnd()->EnableWindow(TRUE);
	this->ShowWindow(SW_HIDE);
	m_pRealParent->SendMessage(WM_SERVER_DLG_UPDATE, 0, 0);
}
