﻿#include "StdAfx.h"
#include "DialogConfig.h"

CDialogConfig::CDialogConfig(void)
	:m_Config(NULL)
	,m_strShapeName("")
{
	m_pXmlDefaultDialog = new TiXmlElement("DefaultDialog");
	m_pXmlDefaultDialog->SetAttribute("Left", "0");
	m_pXmlDefaultDialog->SetAttribute("Top", "0");
	m_pXmlDefaultDialog->SetAttribute("Width", "726");
	m_pXmlDefaultDialog->SetAttribute("Height", "667");
	m_pXmlDefaultDialog->SetAttribute("Caption", "奇迹传说MUX");
	m_pXmlDefaultDialog->SetAttribute("BGImage", "launcherbmp/main.bmp");
	m_pXmlDefaultDialog->SetAttribute("ShapeName", "LaunchDlg");

	m_pXmlDefaultChild = new TiXmlElement("DefaultChild");
	m_pXmlDefaultChild->SetAttribute("Left", "417");
	m_pXmlDefaultChild->SetAttribute("Top", "21");
	m_pXmlDefaultChild->SetAttribute("Right", "500");
	m_pXmlDefaultChild->SetAttribute("Bottom", "33");
	m_pXmlDefaultChild->SetAttribute("Width", "117");
	m_pXmlDefaultChild->SetAttribute("Height", "49");
	m_pXmlDefaultChild->SetAttribute("Text", "奇迹传说MUX");
	m_pXmlDefaultChild->SetAttribute("BGImage", "launcherbmp/start_button.bmp");
	m_pXmlDefaultChild->SetAttribute("R", "255");
	m_pXmlDefaultChild->SetAttribute("G", "255");
	m_pXmlDefaultChild->SetAttribute("B", "255");
}

CDialogConfig::~CDialogConfig(void)
{
	delete m_pXmlDefaultDialog;
	m_pXmlDefaultDialog = NULL;

	delete m_pXmlDefaultChild;
	m_pXmlDefaultChild = NULL;
}

void CDialogConfig::SetConfig(TiXmlElement* config)
{
	// 设置窗口属性
	m_Config = config;
	
	if(m_Config != NULL)
	{
		// 自身属性
		if(m_Config->Attribute("ShapeName") != NULL)
		{
			m_strShapeName = m_Config->Attribute("ShapeName");
		}

		// 子节点属性
		TiXmlElement* pXmlChild = m_Config->FirstChildElement();
		for(;pXmlChild;pXmlChild = pXmlChild->NextSiblingElement())
		{
			const char* czChildName = pXmlChild->Attribute("Name");
			if(czChildName != NULL)
			{
				if(strlen(czChildName) == 0)
				{
					continue;
				}
				else
				{
					CString strName(czChildName);
					m_ChildList[strName] = pXmlChild;
				}
			}
			else
			{
				continue;// 名字不正确的直接跳过
			}
		}
	}
}

TiXmlElement* CDialogConfig::GetConfig()
{
	return (m_Config == NULL ? m_pXmlDefaultDialog : m_Config);
}

TiXmlElement* CDialogConfig::GetChildConfig(CString name)
{
	return (m_ChildList[name] == NULL ? m_pXmlDefaultChild : m_ChildList[name]);
}

CString CDialogConfig::GetShapeName()
{
	return m_strShapeName;
}