﻿/** @file ImageProgressCtrl.h 
@brief 文件打包：可显示图片及文字的进度条
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：2009-08-28 
*	作    者：和萌
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#pragma once
#include "afxwin.h"
#include "atlimage.h"

#ifndef _MEMDC_H_
//////////////////////////////////////////////////
// CMemDC - memory DC
//
// Author: Keith Rule
// Email:  keithr@europa.com
// Copyright 1996-1997, Keith Rule
//
// You may freely use or modify this code provided this
// Copyright is included in all derived versions.
//
// History - 10/3/97 Fixed scrolling bug.
//                   Added print support.
//
// This class implements a memory Device Context

class CMemDC : public CDC
{
public:

	// constructor sets up the memory DC
	CMemDC(CDC* pDC) : CDC()
	{
		ASSERT(pDC != NULL);

		m_pDC = pDC;
		m_pOldBitmap = NULL;
		m_bMemDC = !pDC->IsPrinting();

		if (m_bMemDC)    // Create a Memory DC
		{
			pDC->GetClipBox(&m_rect);
			CreateCompatibleDC(pDC);
			m_bitmap.CreateCompatibleBitmap(pDC, m_rect.Width(), m_rect.Height());
			m_pOldBitmap = SelectObject(&m_bitmap);
			SetWindowOrg(m_rect.left, m_rect.top);
		}
		else        // Make a copy of the relevent parts of the current DC for printing
		{
			m_bPrinting = pDC->m_bPrinting;
			m_hDC       = pDC->m_hDC;
			m_hAttribDC = pDC->m_hAttribDC;
		}
	}
	// Destructor copies the contents of the mem DC to the original DC
	~CMemDC()
	{
		if (m_bMemDC) 
		{    
			// Copy the offscreen bitmap onto the screen.
			m_pDC->BitBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(),
				this, m_rect.left, m_rect.top, SRCCOPY);

			//Swap back the original bitmap.
			SelectObject(m_pOldBitmap);
		} else {
			// All we need to do is replace the DC with an illegal value,
			// this keeps us from accidently deleting the handles associated with
			// the CDC that was passed to the constructor.
			m_hDC = m_hAttribDC = NULL;
		}
	}

	// Allow usage as a pointer
	CMemDC* operator->() {return this;}
	// Allow usage as a pointer
	operator CMemDC*() {return this;}

private:
	CBitmap  m_bitmap;      // Offscreen bitmap
	CBitmap* m_pOldBitmap;  // bitmap originally found in CMemDC
	CDC*     m_pDC;         // Saves CDC passed in constructor
	CRect    m_rect;        // Rectangle of drawing area.
	BOOL     m_bMemDC;      // TRUE if CDC really is a Memory DC.
};

#endif

#ifndef _IMAGEPROGRESSCTRL_H_
#define _IMAGEPROGRESSCTRL_H_


class CImageProgressCtrl :
	public CProgressCtrl
{
// Construction
public:
	CImageProgressCtrl(void);

// Attributes
public:

// Operations
public:
	BOOL		InitImages(CString pszForeImageFile, CString pszBkImageFile = NULL);
	BOOL		InitImages(CString pszForeImageFile, int width, int height);
	int         SetPos(int nPos);
	int         StepIt();
	void        SetRange(int nLower, int nUpper);
	int         OffsetPos(int nPos);
	int         SetStep(int nStep);
	void        SetForeColour(COLORREF col);
	void        SetBkColour(COLORREF col);
	void        SetTextForeColour(COLORREF col);
	void        SetTextBkColour(COLORREF col);
	COLORREF    GetForeColour();
	COLORREF    GetBkColour();
	COLORREF    GetTextForeColour();
	COLORREF    GetTextBkColour();

	void        SetShowText(BOOL bShow);
	void		SetShowImage(BOOL bShow);
	void		SetDisplayText(CString strText);

protected:
	BOOL		LoadDynamicBitmap(CBitmap* pChangeImage, CString strFileName, int x, int y, int width, int height);
// Implementation
public:
	virtual ~CImageProgressCtrl(void);
	afx_msg		void	OnPaint();
	afx_msg		void	OnSize(UINT nType,int cx, int cy);

	DECLARE_MESSAGE_MAP()

protected:
	int			m_nPos;			// 当前位置 [8/27/2009 hemeng]
	int			m_nStepSize;	// 递进值 [8/27/2009 hemeng]
	int			m_nMax;			// 最大值 [8/27/2009 hemeng]
	int			m_nMin;			// 最小值 [8/27/2009 hemeng]
	CString		m_strText;		// 要显示的字 [8/27/2009 hemeng]
	BOOL		m_bShowText;	// 是否要显示字 [8/27/2009 hemeng]
	BOOL		m_bShowImage;	// 是否显示图像 [8/27/2009 hemeng]
	int			m_nBarWidth;
	COLORREF	m_colFore;		// 进度条前景色 [8/27/2009 hemeng]
	COLORREF	m_colBk;		// 进度条背景色 [8/27/2009 hemeng]
	COLORREF	m_colTextFore;	// 文字前景色 [8/27/2009 hemeng]
	COLORREF	m_colTextBk;	// 文字背景色 [8/27/2009 hemeng]

	CImage*		m_pBkImage;
	CImage*		m_pForeImage;


};

#endif