﻿#pragma once

class CColorText
{
public:
	CColorText(void);
	~CColorText(void);
public:
	CColorText(CString text, CRect* location, COLORREF textColor = RGB(0, 0, 0), CFont* font = NULL);
	void Copy(CColorText* src);
public:
	void SetText(CString text);
	void SetTextColor(COLORREF color);
	void SetBkMode(int mode);
	void SetFormat(UINT format);
	void SetShadow(COLORREF color, int num);
	void Draw(CDC* dc);
private:
	void DrawText(CDC* dc, CString text, CRect* rect, COLORREF color, CFont* font, int mode, UINT format);
	void DrawShadow(CDC* dc);
public:
	CString		m_strText;		// 字体内容
	CRect		m_rectLocation;	// 字体位置
	COLORREF	m_crTextColor;	// 字体颜色
	CFont		m_Font;			// 字型字号
	int			m_iBkMode;		// 背景模式，（透明等）
	UINT		m_nFormat;		// 输出模式，（居中对其方式）
	COLORREF	m_crShadowColor;// 阴影颜色
	int			m_iShadowNum;	// 阴影层数
};
