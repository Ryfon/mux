﻿// GameConfigDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MuxLauncher.h"
#include "GameConfigDlg.h"

// CGameConfigDlg 对话框

IMPLEMENT_DYNAMIC(CGameConfigDlg, CDialog)

CGameConfigDlg::CGameConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGameConfigDlg::IDD, pParent)
	, m_bWindow(FALSE)
	, m_bVS(FALSE)
	, m_bSoundOff(FALSE)
{
	//aSystemConfigDefaultValue[0] = 1;
}

CGameConfigDlg::~CGameConfigDlg()
{
}

void CGameConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_RESOLUTION, m_comboResolution);
	DDX_Check(pDX, IDC_CHECK_WINDOW, m_bWindow);
	DDX_Check(pDX, IDC_CHECK_VS, m_bVS);
	DDX_Check(pDX, IDC_CHECK_SOUND_OFF, m_bSoundOff);
	DDX_Control(pDX, IDC_COMBO_TERRAIN, m_comboTerrain);
	DDX_Control(pDX, IDC_COMBO_EFFECT, m_comboEffect);
	DDX_Control(pDX, IDC_COMBO_SHADOW, m_comboShadow);
}

BOOL CGameConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 加载配置文件
	//LoadConfigure("UserData/Configuration.xml");

	// 初始化分辨率列表
	vector<CString> resolutionSet;
	GetAllResolution(resolutionSet);
	m_comboResolution.Clear();
	for (vector<CString>::iterator it = resolutionSet.begin();it != resolutionSet.end();++it)
	{
		m_comboResolution.AddString(*it);
	}
	
	// 初始化细节列表（低，中，高）
	m_comboTerrain.Clear();
	m_comboTerrain.AddString(_T("低"));
	m_comboTerrain.AddString(_T("中"));
	m_comboTerrain.AddString(_T("高"));
	m_comboEffect.Clear();
	m_comboEffect.AddString(_T("低"));
	m_comboEffect.AddString(_T("中"));
	m_comboEffect.AddString(_T("高"));
	m_comboShadow.Clear();
	m_comboShadow.AddString(_T("低"));
	m_comboShadow.AddString(_T("中"));
	m_comboShadow.AddString(_T("高"));

	// 显示
	ShowConfigure();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}
//typedef struct stResolution 
//{
//	DWORD width;
//	DWORD height;
//	DWORD bit;
//
//	stResolution(DWORD w, DWORD h, DWORD b)
//	{
//		width = w;
//		height = h;
//		bit = b;
//	}
//}stResolution;

void CGameConfigDlg::GetAllResolution(vector<CString>& list)
{
	DWORD iMode = 0;
	DEVMODE devmode;
	set<stResolution*> temp;
	while (EnumDisplaySettings(NULL, iMode, &devmode))
	{	
		if (devmode.dmPelsWidth >= 1024 && devmode.dmPelsHeight >= 768 && devmode.dmBitsPerPel >= 16)	// 限定最小分辨率为1024*768*16
		{
			//CString str;

			//str.Format(_T("%d x %d x %d bpp"), devmode.dmPelsWidth, devmode.dmPelsHeight, devmode.dmBitsPerPel);
			temp.insert(new stResolution(devmode.dmPelsWidth, devmode.dmPelsHeight, devmode.dmBitsPerPel));
		}
		
		iMode++;
	}

	// 排序后插入
	while (temp.size() > 0)
	{
		set<stResolution*>::iterator smallest = temp.begin();
		for (set<stResolution*>::iterator it = temp.begin();it != temp.end();++it)
		{
			if((*it)->bit > (*smallest)->bit)
			{
				smallest = it;
			}
			else if ((*it)->bit == (*smallest)->bit)
			{
				if ((*it)->width < (*smallest)->width)
				{
					smallest = it;
				}
				else if ((*it)->width == (*smallest)->width)
				{
					if ((*it)->height < (*smallest)->height)
					{
						smallest = it;
					}
				}
			}
		}
		
		CString str;
		str.Format(_T("%d x %d x %d bpp"), (*smallest)->width, (*smallest)->height, (*smallest)->bit);
		bool bExist = false;
		for (int i = 0;i < (int)list.size();i++)
		{
			if (list[i].Compare(str) == 0)
			{
				bExist = true;
			}
		}
		if (!bExist)
		{
			list.push_back(str);
		}

		temp.erase(smallest);
	}

}


void CGameConfigDlg::ShowConfigure()
{
	m_comboResolution.SetCurSel(m_Configure[CT_RESOLUTION_INDEX]);	//分辨率
	m_bWindow = m_Configure[CT_WINDOW] == 0 ? FALSE:TRUE; 			// 窗口模式
	m_bVS = m_Configure[CT_VS] == 0 ? FALSE:TRUE;					// 垂直同步
	m_bSoundOff = m_Configure[CT_SOUND_OFF] == 0 ? FALSE:TRUE;		// 禁止声音
	m_comboTerrain.SetCurSel(m_Configure[CT_TERRIAN]);				// 地表细节
	m_comboShadow.SetCurSel(m_Configure[CT_SHADOW]);				// 阴影细节
	m_comboEffect.SetCurSel(m_Configure[CT_EFFECT]);				// 特效细节
	// 更新至界面
	UpdateData(FALSE);
}


void CGameConfigDlg::GetConfigure()
{
	// 从界面获取数据
	UpdateData(TRUE);

	// 存放入m_Configure
	m_Configure[CT_RESOLUTION_INDEX] = m_comboResolution.GetCurSel();//分辨率	
	m_Configure[CT_WINDOW] = m_bWindow ? 1:0; 						// 窗口模式
	m_Configure[CT_VS] = m_bVS ? 1:0;					// 垂直同步
	m_Configure[CT_SOUND_OFF] = m_bSoundOff ? 1:0;		// 禁止声音
	m_Configure[CT_TERRIAN] = m_comboTerrain.GetCurSel();				// 地表细节
	m_Configure[CT_SHADOW] = m_comboShadow.GetCurSel();				// 阴影细节
	m_Configure[CT_EFFECT] = m_comboEffect.GetCurSel();				// 特效细节
}

BEGIN_MESSAGE_MAP(CGameConfigDlg, CDialog)
END_MESSAGE_MAP()


// CGameConfigDlg 消息处理程序
