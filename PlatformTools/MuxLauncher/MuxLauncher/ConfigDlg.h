﻿#pragma once
#include "afxcmn.h"
#include "SystemConfigDlg.h"
#include "GameConfigDlg.h"
#include "TabSheet.h"

// CConfigDlg 对话框

class CConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CConfigDlg)

public:
	CConfigDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CConfigDlg();

// 对话框数据
	enum { IDD = IDD_DIALOGCONFIG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
private:
	CTabSheet m_tabCtrlConfig;
	CSystemConfigDlg m_dlgSystemConfig;
	CGameConfigDlg m_dlgGameConfig;
public:
	afx_msg void OnTcnSelchangeTabconfig(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtoncancel();
	afx_msg void OnBnClickedButtonok();
};
