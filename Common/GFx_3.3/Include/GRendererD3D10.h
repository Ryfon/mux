/**********************************************************************

Filename    :   GRendererD3D10.h
Content     :   D3D10 Renderer
Created     :   Oct 1, 2007
Authors     :   

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/


#ifndef INC_GRENDERERD3D10_H
#define INC_GRENDERERD3D10_H

#include "GTypes.h"
#include "GRefCount.h"
#include "GRenderer.h"
#include "GRendererCommonImpl.h"
#include <D3D10.h>


class GTextureD3D10 : public GTextureImplNode
{
public:
    GTextureD3D10() { }
    GTextureD3D10 (GRendererNode *plistRoot) : GTextureImplNode(plistRoot) { }

    virtual bool    InitTexture(GImageBase* pim, UInt usage = Usage_Wrap) = 0; 
    virtual bool    InitTexture(ID3D10Texture2D *ptex, bool unused = 0)   = 0;

    // does not addref
    virtual ID3D10Texture2D*  GetNativeTexture() const                                               = 0;
};

class GRenderTargetD3D10 : public GRenderTargetImplNode
{
public:
    GRenderTargetD3D10 () {}
    GRenderTargetD3D10 (GRendererNode *plistRoot) : GRenderTargetImplNode(plistRoot) { }

    struct D3D10RenderTargetParams
    {
        ID3D10RenderTargetView*    pRenderTextureView;
        ID3D10ShaderResourceView*  pRenderTextureSV;
        ID3D10DepthStencilView*    pRTDepthStencil;
    };
    virtual bool       InitRenderTarget(GTexture* ptarget) = 0;
    virtual bool       InitRenderTarget(D3D10RenderTargetParams params) = 0;
};

// Base class for Direct3D Renderer implementation.
// This class will be replaced with a new shape-centric version in the near future.

class GRendererD3D10 : public GRenderer
{
public:
    typedef GTextureD3D10       TextureType;
    typedef ID3D10Texture2D*    NativeTextureType;
    typedef GRenderTargetD3D10::D3D10RenderTargetParams NativeRenderTargetType;

    // Creates a new renderer object
    static GRendererD3D10* GSTDCALL CreateRenderer();

    // Returns created objects with a refCount of 1, must be user-released.
    virtual GTextureD3D10*      CreateTexture()                                                          = 0;    
    virtual GTextureD3D10*      CreateTextureYUV()                                                       = 0;    
    virtual GRenderTarget*      CreateRenderTarget()                                                     = 0;

    virtual GTextureD3D10* PushTempRenderTarget(const GRectF& frameRect, UInt targetW, UInt targetH)     = 0;


    // *** Implement Dependent Video Mode configuration

    enum VMConfigFlags
    {
        VMConfig_CombinePrims = 0x00000002,
        VMConfig_Multisample  = 0x00000004,
    };

    // A new GRenderer instance must be created to use a different D3D device.
    virtual bool                SetDependentVideoMode(
        ID3D10Device*           pd3dDevice,
        UInt32                  vmConfigFlags = 0)
    {
        GUNUSED2(pd3dDevice,vmConfigFlags);
        GASSERT(0);
        return 0;
    }

    // Returns back to original mode (cleanup)                                                      
    virtual bool                ResetVideoMode()
    {
        GASSERT(0);
        return 0;
    }


    // Query display status
    enum DisplayStatus
    {
        DisplayStatus_Ok            = 0,
        DisplayStatus_NoModeSet     = 1,    // Video mode 
        DisplayStatus_Unavailable   = 2,    // Display is unavailable for rendering; check status again later
        DisplayStatus_NeedsReset    = 3     // May be returned in Dependent mode to indicate external action being required
    };

    virtual DisplayStatus       CheckDisplayStatus() const
    {
        return DisplayStatus_NoModeSet;
    }




    // *** Direct3D Access

    // Return various Direct3D related information
    virtual ID3D10Device*       GetDirect3DDevice() const { return 0; }

    // 3D support
#ifndef GFC_NO_3D
    // Sets the perspective matrix for drawing a 3D movieclip
    virtual void  SetPerspective3D(const GMatrix3D &projMatIn)
    { 
        projMatrix3D = projMatIn;
    }

    // Sets the view matrix for drawing a 3D movieclip
    virtual void  SetView3D(const GMatrix3D &viewMatIn) 
    { 
        viewMatrix3D = viewMatIn;
    }

    // Sets the world matrix for drawing a 3D movieclip
    virtual void  SetWorld3D(const GMatrix3D *pWorldMatIn) 
    { 
        pWorldMatrix3D = pWorldMatIn;
    }

protected:

    GMatrix3D viewMatrix3D;
    GMatrix3D projMatrix3D;
    const GMatrix3D *pWorldMatrix3D;    // can be NULL if not using 3D
#endif

};

#endif
