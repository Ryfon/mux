/**********************************************************************

Filename    :   GSoundRenderer.h
Content     :   Sound Player/Driver interface
Created     :   Noveber, 2008
Authors     :   Maxim Didenko, Vladislav Merker

Copyright   :   (c) 1998-2009 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/


#ifndef INC_GSOUNDPLAYER_COMMON_IMPL_H
#define INC_GSOUNDPLAYER_COMMON_IMPL_H

#include "GConfig.h"
#ifndef GFC_NO_SOUND

#include "GSoundRenderer.h"

//////////////////////////////////////////////////////////////////////////
//

// A simple doubly-linked list node, used to track textures and other resources.
class GDListNode
{
public:
    union   {
        GDListNode *pPrev;       
        GDListNode *pLast;
    };
    union   {
        GDListNode *pNext;
        GDListNode *pFirst;      
    };

    // Default constructor, creates root node (empty list).
    GDListNode() { pNext = pPrev = this; }
    // Inserting constructor, inserts this node at the head of the list.
    GDListNode(GDListNode *proot)
    {
        pNext = proot->pFirst;
        pPrev = proot;
        proot->pFirst->pPrev = this;
        proot->pFirst = this;
    }

    virtual ~GDListNode() {}
    
    // Remove
    void RemoveNode()
    {
        pPrev->pNext = pNext;
        pNext->pPrev = pPrev;
        pNext = pPrev = 0;
    }

    virtual void ReleaseResource() {}
};

//////////////////////////////////////////////////////////////////////////
//

class GSoundSampleImplNode : public GSoundSample, public GDListNode
{
public:
    GSoundSampleImplNode(GDListNode* plistRoot);
    ~GSoundSampleImplNode();
};

class GSoundChannelImplNode : public GSoundChannel, public GDListNode
{
public:
    GSoundChannelImplNode(GDListNode* plistRoot);
    ~GSoundChannelImplNode();
};

#endif // GFC_NO_SOUND

#endif
