/**********************************************************************

Filename    :   GRendererD3D11.h
Content     :   D3D11 Renderer
Created     :   Oct 1, 2007
Authors     :   

Notes       :   
History     :   

Copyright   :   (c) 1998-2006 Scaleform Corp. All Rights Reserved.

Licensees may use this file in accordance with the valid Scaleform
Commercial License Agreement provided with the software.

This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR ANY PURPOSE.

**********************************************************************/


#ifndef INC_GRENDERERD3D11_H
#define INC_GRENDERERD3D11_H

#include "GTypes.h"
#include "GRefCount.h"
#include "GRenderer.h"
#include "GRendererCommonImpl.h"
#include <D3D11.h>


class GTextureD3D11 : public GTextureImplNode
{
public:
    GTextureD3D11() { }
    GTextureD3D11 (GRendererNode *plistRoot) : GTextureImplNode(plistRoot) { }

    virtual bool    InitTexture(GImageBase* pim, UInt usage = Usage_Wrap)                            = 0; 
    virtual bool    InitTexture(ID3D11Texture2D *ptex, bool unused = 0)                              = 0;

    // does not addref
    virtual ID3D11Texture2D*  GetNativeTexture() const                                               = 0;
};

class GRenderTargetD3D11 : public GRenderTargetImplNode
{
public:
    GRenderTargetD3D11 () {}
    GRenderTargetD3D11 (GRendererNode *plistRoot) : GRenderTargetImplNode(plistRoot) { }

    struct D3D11RenderTargetParams
    {
        ID3D11RenderTargetView*    pRenderTextureView;
        ID3D11ShaderResourceView*  pRenderTextureSV;
        ID3D11DepthStencilView*    pRTDepthStencil;
    };
    virtual bool       InitRenderTarget(GTexture* ptarget) = 0;
    virtual bool       InitRenderTarget(D3D11RenderTargetParams params) = 0;
};

// Base class for Direct3D Renderer implementation.
// This class will be replaced with a new shape-centric version in the near future.

class GRendererD3D11 : public GRenderer
{
public:
    typedef GTextureD3D11       TextureType;
    typedef ID3D11Texture2D*    NativeTextureType;

    // Creates a new renderer object
    static GRendererD3D11* GSTDCALL CreateRenderer();

    // Returns created objects with a refCount of 1, must be user-released.
    virtual GTextureD3D11* CreateTexture()                                                               = 0;    
    virtual GTextureD3D11* CreateTextureYUV()                                                            = 0;    

    virtual GTextureD3D11* PushTempRenderTarget(const GRectF& frameRect, UInt targetW, UInt targetH)      = 0;

    // *** Implement Dependent Video Mode configuration

    enum VMConfigFlags
    {
        VMConfig_CombinePrims = 0x00000002,
        VMConfig_Multisample  = 0x00000004,
    };

    // A new GRenderer instance must be created to use a different D3D device.
    virtual bool                SetDependentVideoMode(
        ID3D11Device*           pd3dDevice,
        ID3D11DeviceContext*    pdevContext,
        UInt32                  vmConfigFlags = 0)
    {
        GUNUSED3(pd3dDevice,pdevContext,vmConfigFlags);
        GASSERT(0);
        return 0;
    }

    // Returns back to original mode (cleanup)                                                      
    virtual bool                ResetVideoMode()
    {
        GASSERT(0);
        return 0;
    }


    // Query display status
    enum DisplayStatus
    {
        DisplayStatus_Ok            = 0,
        DisplayStatus_NoModeSet     = 1,    // Video mode 
        DisplayStatus_Unavailable   = 2,    // Display is unavailable for rendering; check status again later
        DisplayStatus_NeedsReset    = 3     // May be returned in Dependent mode to indicate external action being required
    };

    virtual DisplayStatus       CheckDisplayStatus() const
    {
        return DisplayStatus_NoModeSet;
    }




    // *** Direct3D Access

    // Return various Direct3D related information
    virtual ID3D11Device*           GetDirect3DDevice() const  { return 0; }
    virtual ID3D11DeviceContext*    GetDirect3DContext() const  { return 0; }
    // 3D support
#ifndef GFC_NO_3D
    // Sets the perspective matrix for drawing a 3D movieclip
    virtual void  SetPerspective3D(const GMatrix3D &projMatIn)
    { 
        projMatrix3D = projMatIn;
    }

    // Sets the view matrix for drawing a 3D movieclip
    virtual void  SetView3D(const GMatrix3D &viewMatIn) 
    { 
        viewMatrix3D = viewMatIn;
    }

    // Sets the world matrix for drawing a 3D movieclip
    virtual void  SetWorld3D(const GMatrix3D *pWorldMatIn) 
    { 
        pWorldMatrix3D = pWorldMatIn;
    }

protected:

    GMatrix3D viewMatrix3D;
    GMatrix3D projMatrix3D;
    const GMatrix3D *pWorldMatrix3D;    // can be NULL if not using 3D
#endif
};

#endif
