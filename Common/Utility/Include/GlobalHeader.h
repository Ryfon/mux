#ifndef GLOBAL_HEADER_H
#define GLOBAL_HEADER_H

// C运行库头文件
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cstdarg>
#include <cmath>

// STL容器库头文件
#include <string>
#include <vector>
#include <map>
#include <hash_map>
#include <set>
#include <list>
#include <deque>
#include <stack>
#include <queue>
#include <bitset>

// STL算法
#include <algorithm>
#include <functional>

// IO stream
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <ciso646>

// 使用std名字空间
using namespace std;

#endif //GLOBAL_HEADER_H