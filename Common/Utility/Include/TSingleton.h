#ifndef SINGLETON_H
#define SINGLETON_H

/**
 @brief 单件模板类
*/
template <class T> class TSingleton
{		
	/// 静态实例指针
	static T* ms_pSingleton;

protected:
	
	/// 构造
	TSingleton()
	{};

	/// 析构
	~TSingleton()
	{};

public:

	/// 获取实例
	static T* GetInstance()
	{
		if ( NULL == ms_pSingleton )
			ms_pSingleton = new T;

		return ms_pSingleton;		
	};

	/// 销毁实例
	static void Destroy()
	{
		if ( NULL != ms_pSingleton )
			SAFE_DELETE( ms_pSingleton );		
	};
};

template <class T> T* TSingleton<T>::ms_pSingleton = 0;

#endif//__SINGLETON_H__