#ifndef GLOBAL_MACRO_H
#define	GLOBAL_MACRO_H

#include "GlobalHeader.h"

/// 全局log文件指针, 注非线程安全
static FILE	*s_pLogFile;

////////////////////////////////////////////////////////////////////////////

const float	EPSILON	= 0.00001f;

// 浮点数与零比较
#ifndef FLOATEQUALZERO
	#define FLOATEQUALZERO( x )	 ( (x) >= -EPSILON && (x) <= EPSILON )	
#endif

/// 二浮点数比较
#ifndef FLOATCOMPARE
	#define FLOATCOMPARE(x, y) ( FLOATEQUALZERO(x-y))
#endif

////////////////////////////////////////////////////////////////////////////
/// 内存安全释放
#ifndef SAFE_DELETE
	#define SAFE_DELETE(p)		 { if(p) { delete (p);    (p)=NULL; } }
#endif

#ifndef SAFE_DELETE_ARRAY
	#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#endif

#ifndef SAFE_RELEASE
	#define SAFE_RELEASE(p)		 { if(p) { (p)->Release(); (p)=NULL; } }
#endif

#ifndef SAFE_DESTROY
	#define SAFE_DESTROY(p)		 { if(p) { (p)->Destroy(); (p)=NULL; } }
#endif

////////////////////////////////////////////////////////////////////////////

/// 日志
#ifndef LOG
	#define		LOG( info )		 { Log( info ); }
#endif

/// 日志文件, 非多线程安全
#ifndef LOG2FILE_BEGIN
	#define LOG2FILE_BEGIN( filename ) \
		fopen_s(&s_pLogFile, filename, "a+");
#endif

#ifndef LOG2FILE_END
	#define LOG2FILE_END	fclose( s_pLogFile );
#endif

#ifndef LOG2FILE
	#define		LOG2FILE( info ) \
		if ( s_pLogFile )\
		{\
			fwrite( info,strlen(info),1,s_pLogFile );\
		}
#endif

#ifndef LOG2SERVER
	#define		LOG2SERVER( info )  {}
#endif

#ifndef LOG0
	#define		LOG0( info )	{ Log( info ); }
#endif

#ifndef LOG1
	#define		LOG1( info, param1 ) { Log( info, param1 ); }
#endif

#ifndef LOG2
	#define		LOG2( info, param1, param2 ) { Log( info, param1, param2 ); }
#endif

#ifndef LOG3
	#define		LOG3( info, param1, param2, param3 ) { Log( info, param1, param2, param3 ); }
#endif

#ifndef LOG4
	#define		LOG4( info, param1, param2, param3, param4 ) { Log( info, param1, param2, param3, param4 ); }
#endif

#ifndef LOG5
	#define		LOG5( info, param1, param2, param3, param4, param5 ) { Log( info, param1, param2, param3, param4, param5 ); }
#endif

#ifndef LOG6
	#define		LOG6( info, param1, param2, param3, param4, param5, param6 ) { Log( info, param1, param2, param3, param4, param5, param6 ); }
#endif

#ifndef LOG7
	#define		LOG7( info, param1, param2, param3, param4, param5, param6, param7 ) { Log( info, param1, param2, param3, param4, param5, param6, param7 ); }
#endif

#ifndef LOG8
	#define		LOG8( info, param1, param2, param3, param4, param5, param6, param7, param8 ) { Log( info, param1, param2, param3, param4, param5, param6, param7, param8 ); }
#endif

#ifndef LOG9
	#define		LOG9( info, param1, param2, param3, param4, param5, param6, param7, param8, param9 ) { Log( info, param1, param2, param3, param4, param5, param6, param7, param8, param9 ); }
#endif

#ifndef LOG10
	#define		LOG10(info, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 ) { Log( info, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 ); }
#endif

////////////////////////////////////////////////////////////////////////////
#ifndef LOG2FILE0
	#define		LOG2FILE0( info )		{ Log2File( s_pLogFile, info ); }
#endif

#ifndef LOG2FILE1
	#define		LOG2FILE1( info, param1 )	{ Log2File( s_pLogFile, info, param1 ); }
#endif

#ifndef LOG2FILE2
	#define		LOG2FILE2( info, param1, param2 ) { Log2File( s_pLogFile, info, param1, param2 ); }
#endif

#ifndef LOG2FILE3
	#define		LOG2FILE3( info, param1, param2, param3 ) { Log2File( s_pLogFile, info, param1, param2, param3 ); }
#endif

#ifndef LOG2FILE4
	#define		LOG2FILE4( info, param1, param2, param3, param4 ) { Log2File( s_pLogFile, info, param1, param2, param3, param4 ); }
#endif

#ifndef LOG2FILE5
	#define		LOG2FILE5( info, param1, param2, param3, param4, param5 ) { Log2File( s_pLogFile, info, param1, param2, param3, param4, param5 ); }
#endif

#ifndef LOG2FILE6
	#define		LOG2FILE6( info, param1, param2, param3, param4, param5, param6 ) { Log2File( s_pLogFile, info, param1, param2, param3, param4, param5, param6 ); }
#endif

#ifndef LOG2FILE7
	#define		LOG2FILE7( info, param1, param2, param3, param4, param5, param6, param7 ) { Log2File( s_pLogFile, info, param1, param2, param3, param4, param5, param6, param7 ); }
#endif

#ifndef LOG2FILE8
	#define		LOG2FILE8( info, param1, param2, param3, param4, param5, param6, param7, param8 ) { Log2File( s_pLogFile, info, param1, param2, param3, param4, param5, param6, param7, param8 ); }
#endif

#ifndef LOG2FILE19
	#define		LOG2FILE9( info, param1, param2, param3, param4, param5, param6, param7, param8, param9 ) { Log2File( s_pLogFile, info, param1, param2, param3, param4, param5, param6, param7, param8, param9 ); }
#endif

#ifndef LOG2FILE10
	#define		LOG2FILE10(info, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 ) { Log2File( s_pLogFile, info, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10 ); }
#endif

////////////////////////////////////////////////////////////////////////////
/// 不可预测异常处理机制
#ifndef TRY_BEGIN
	#define		TRY_BEGIN					try {
#endif

#ifndef TRY_END
	#define		TRY_END						} catch(...) {}
#endif

#ifndef TRY_END_HANDLE
	#define		TRY_END_HANDLE( hHandleEvent )		} catch(...) { hHandleEvent; }
#endif

#ifndef TRY_END_RETURN
	#define		TRY_END_RETURN( hHandleEvent )		} catch(...) { return hHandleEvent; }
#endif

////////////////////////////////////////////////////////////////////////////
/// 可预测错误处理策略

/// 警告
#ifndef WARNING
	#define		WARNING( info ) { Log( info ); }
#endif

/// 此错误可忽略,程序继续执行IGNORE
#ifndef ERROR_I
	#define		ERROR_I( info )		{ MessageBox( NULL, info, "错误!", MB_OK ); }	
#endif

/// 此错误不可勿略, 返回
#ifndef ERROR_E
	#define		ERROR_E( info )\
		{	string strBuffer( info );\
		strBuffer.append( ":程序自动返回." );\
		MessageBox( NULL, strBuffer.c_str(), "错误!", MB_OK ); }
#endif

/// 此错误不可勿略, 程序调用退出处理ret后返回
#ifndef ERROR_R
	#define		ERROR_R( info, retFunc ) \
				{ 	string strBuffer( info );\
					strBuffer.append( ":程序自动返回." );\
					MessageBox( NULL, strBuffer.c_str(), "错误!", MB_OK );\
					return retFunc; }
#endif

/// 致命错误,程序直接退出
#ifndef ERROR_F
	#define		ERROR_F( info ) \
				{ 	string strBuffer( info );\
				strBuffer.append( ":程序将自动退出." );\
				MessageBox( NULL, strBuffer.c_str(), "致命错误!", MB_OK );\
				exit( 1 ); }
#endif

////////////////////////////////////////////////////////////////////////////
extern		void Log( const char *pszOutputInfo, ... );
extern		void Log2File( FILE* fp, const char *pszOutputInfo,... );

#endif	//GLOBAL_MACRO_H