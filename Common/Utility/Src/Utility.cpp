#include <Windows.h>
#include "GlobalMacro.h"

#define	BUFFER_LEN	1024*4

void Log( const char * pszOutputInfo, ... )
{
	char szBuffer[BUFFER_LEN];

	va_list valist;
	va_start( valist, pszOutputInfo );
	vsprintf_s( szBuffer, BUFFER_LEN, pszOutputInfo, valist );
	va_end( valist );

	OutputDebugString( szBuffer );
}

void Log2File( FILE* fp, const char *pszOutputInfo,... )
{
	if ( fp )
	{	
		char szBuffer[BUFFER_LEN];

		va_list valist;
		va_start( valist, pszOutputInfo );
		vsprintf_s( szBuffer, BUFFER_LEN, pszOutputInfo, valist );
		va_end( valist );	

		fwrite( szBuffer,strlen(szBuffer),1,fp );
	}
}