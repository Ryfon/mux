Call cd CenterSrv 
Start CenterSrv.exe

Call cd ../DBSrv 
Start DBServer.exe

Call cd ../FunctionSrv 
Start FunctionSrv.exe

Call cd ../MapSrv
Start MapSrv.exe

Call cd ../GateSrv
Start GateSrv.exe

Call cd ../ShopSrv
Start ShopSrv.exe

Call cd ../AssignSrv
Start AssignSrv.exe -d -f win32_assignsrv.conf

Call cd ../LoginSrv
Start LoginSrv.exe -d -f win32_loginsrv_svc.conf

Call cd ../RelationSrv
Start RelationSrv.exe -d -f win32_relationsrv_svc.conf

Call cd ../LogSrv 
Start LogSrv.exe -d -f win32_logservice_svc.conf