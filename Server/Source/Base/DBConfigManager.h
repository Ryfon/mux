﻿/** @file DBConfigManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-3-14
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef DB_CONFIG_MANAGER_H
#define DB_CONFIG_MANAGER_H

#include "MysqlWrapper.h"
#include <map>

/**
* @class CRoleInitManager
*
* @brief 管理角色创建时的相关缺省属性
* 
*/
class CDBConfigManager
{
	friend class ACE_Singleton<CDBConfigManager, ACE_Null_Mutex>;

private:
	/// 构造
	CDBConfigManager(void);

	/// 析构
	~CDBConfigManager(void);

public:
	int Init(const char* pszFileName, const char *pszServerType);

	stConnectionString * Find(unsigned short usConnectionStringIndex = 0);

	int Clear(void);

	size_t Num(){return m_dbConnectionInfos.size();}

	std::map<unsigned short, stConnectionString *>& ConnectionInfos(void){return m_dbConnectionInfos;}

private:
	/// 初始化连接列表
	std::map<unsigned short, stConnectionString *> m_dbConnectionInfos;

};

typedef ACE_Singleton<CDBConfigManager, ACE_Null_Mutex> CDBConfigManagerSingle;


#endif/*DB_CONFIG_MANAGER_H*/
