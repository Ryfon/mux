﻿#ifndef CONNECTOR_CPP
#define CONNECTOR_CPP

#include "Connector.h"

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1>
CConnector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>::CConnector(void)
:m_pReadQueue(NULL), m_pSendQueue(NULL)
{

}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1>
CConnector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>::CConnector(IBufQueue *pReadQueue, IBufQueue *pSendQueue, unsigned int uiPeerBufferSize)
	: m_pReadQueue(pReadQueue), m_pSendQueue(pSendQueue),m_uiPeerBufferSize(uiPeerBufferSize)
{
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1>
CConnector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>::~CConnector(void)
{

}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1>
int CConnector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>::make_svc_handler(SVC_HANDLER *&sh)
{
	if((m_pReadQueue == NULL)|| (m_pSendQueue == NULL))
		return -1;

	if(sh == 0)
		ACE_NEW_RETURN (sh, SVC_HANDLER(m_uiPeerBufferSize), -1);

	sh->reactor (this->reactor ());

	// 设置队列
	sh->ReadQueue(this->ReadQueue());
	sh->SendQueue(this->SendQueue());

	return 0;
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1>
IBufQueue * CConnector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>::ReadQueue(void)
{
	return m_pReadQueue;
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1>
void CConnector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>::ReadQueue(IBufQueue *pReadQueue)
{
	m_pReadQueue = pReadQueue;
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1>
IBufQueue * CConnector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>::SendQueue(void)
{
	return m_pSendQueue;
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1>
void CConnector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>::SendQueue(IBufQueue *pSendQueue)
{
	m_pSendQueue = pSendQueue;
}


#endif/*CONNECTOR_CPP*/
