﻿/** @file HandlerEx.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-6-17
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef HANDLER_EX_H
#define HANDLER_EX_H

//#include "HandlerT.h"
#include "BufQueue/BufQueue.h"
#include "BufQueue/BufQueueImp.h"
#include <queue>


class CHandlerEx : public ACE_Event_Handler
{
public:
	/// 
	typedef ACE_SOCK_Stream stream_type;

	/// 
	typedef ACE_INET_Addr addr_type;

	enum
	{
		UIO_SIZE = 1024
	};

public:	
	/// 缺省
	CHandlerEx(void);

	/// 构造
	explicit CHandlerEx(unsigned int uiPeerBufferSize);

	/// 析构
	virtual ~CHandlerEx(void);

public:
	/// 初始化 
	virtual int open(void * = 0);

	/// 清理
	virtual int close(u_long flags = 0);

	/// 输入事件发生时被框架回调（例如连接或者数据）
	virtual int handle_input(ACE_HANDLE fd = ACE_INVALID_HANDLE);

	/// 对应句柄可写时
	virtual int handle_output (ACE_HANDLE fd = ACE_INVALID_HANDLE);

	/** 
	* 当<handle_*>返回-1或者<remove_handler>被框架调用时.<close_mask>
	* 表明什么事件触发<handle_close>被调用,<handle>指定具体的句柄
	*/
	virtual int handle_close(ACE_HANDLE handle,
		ACE_Reactor_Mask close_mask);

	/// 获取句柄
	virtual ACE_HANDLE get_handle(void) const;

	/// 获取底层stream
	ACE_SOCK_Stream & peer(void) const;

public:
	/// 获取/设置队列
	/// 获取已读队列
	IBufQueue * ReadQueue(void);

	/// 设置已读队列
	void ReadQueue(IBufQueue *pReadQueue);

	/// 获取待发队列
	IBufQueue * SendQueue(void);

	/// 设置待发队列
	void SendQueue(IBufQueue *pSendQueue);

	/// 获取/设置SessionID
	/// 获取
	unsigned int SessionID(void);

	/// 设置
	void SessionID(unsigned int uiSessionID);

public:
	/// 追加数据
	int AppendMsg(MsgToPut *pMsgToPut);

	/// 是否使用?
	bool IsUsed(void);

	/// 设置使用
	void IsUsed(bool bIsUsed);

	/// 是否主动连接
	bool ActiveConnect(void);

	/// 设置是否连接
	void ActiveConnect(bool bActiveConnect);
protected:
	/// 连接建立时处理
	virtual int OnLinkUp(void);

	/// 连接段开始处理
	virtual int OnLinkDown(u_long flags = 0);

	/// 出错
	virtual void OnError(int closeType, int arg);

	/// 处理一个完整包
	virtual int HandlePackage(char* pszPack, unsigned int uiPakcLen);

	/// 获取对端信息
	int GetRemoteAddress(ACE_INET_Addr & remoteAddress);

	/// 是否是关闭消息（主应用逻辑发过来）
	bool IsCloseMsg(MsgToPut *pMsgToPut);

	/// 重置所有数据成员
	void Reset(u_long flags = 0);

	/// 发送
	int SendMsg(void);

private:
	/// 底层stream
	ACE_SOCK_Stream m_peer;

	/// 已读消息队列 
	IBufQueue *m_pReadQueue;

	/// 待发送消息队列
	IBufQueue *m_pSendQueue;

	/// 会话id
	unsigned int m_uiSessionID;

	/// 消息队列
	std::queue<MsgToPut *> m_messageQueue;

	// 发送消息列表
	MsgToPut *m_msgArray[UIO_SIZE];

	/// 发送缓冲区
	iovec m_iov[UIO_SIZE];

	/// 发送缓冲区实际长度
	short m_iovCount;

	/// 发送缓冲区中待发送的索引
	short m_iovOffset;

	/// 互斥锁
	ACE_Thread_Mutex m_mutex;

	/// 是否延迟关闭
	bool m_bDeferredClose;

	/// 是否使用
	bool m_bIsUsed;

	/// 是否主动连接
	bool m_bActiveConnect;

	/// 是否存在数据待发送
	bool m_bExistDataToSend;
};

#endif/*HANDLER_EX_H*/
