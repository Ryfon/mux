﻿
//EncryptSendPkg.h

//Created by shenchenkai
//shenchenkai@corp.the9.com
#ifdef CLI_DLL_ENC

unsigned char EncryptSendPkg_00001_code[];
#define EncryptSendPkg_00001_code_LEN 508

unsigned char EncryptSendPkg_00002_code[];
#define EncryptSendPkg_00002_code_LEN 508

unsigned char EncryptSendPkg_00003_code[];
#define EncryptSendPkg_00003_code_LEN 508

unsigned char EncryptSendPkg_00004_code[];
#define EncryptSendPkg_00004_code_LEN 349

#define EncryptSendPkg_ARRAY_NUM 4
#define EncryptSendPkg_CODE_LEN 1873

unsigned char DecryptRcvPkg_00001_code[];
#define DecryptRcvPkg_00001_code_LEN 508

unsigned char DecryptRcvPkg_00002_code[];
#define DecryptRcvPkg_00002_code_LEN 508

unsigned char DecryptRcvPkg_00003_code[];
#define DecryptRcvPkg_00003_code_LEN 508

unsigned char DecryptRcvPkg_00004_code[];
#define DecryptRcvPkg_00004_code_LEN 508

unsigned char DecryptRcvPkg_00005_code[];
#define DecryptRcvPkg_00005_code_LEN 241

#define DecryptRcvPkg_ARRAY_NUM 5
#define DecryptRcvPkg_CODE_LEN 2273

#endif //CLI_DLL_ENC