﻿
//EncryptSendPkg.h

//Created by shenchenkai
//shenchenkai@corp.the9.com
#include "stdafx.h"
#include "EncryptSendPkg.h"

#ifdef CLI_DLL_ENC

unsigned char EncryptSendPkg_00001_code[]="\
\x5e\x9a\x4f\x32\x59\x98\xc4\x75\xae\x54\x0f\x1e\x44\x3e\xb4\xc5\x39\x37\x4c\x19\
\x7b\x8c\x14\x91\xd6\xa7\x53\x29\x64\x3b\x55\xaa\x36\xb3\xf3\x0a\x5d\x64\x7f\x09\
\xc1\xac\xb7\xdb\x9e\x10\x6d\x9a\x9d\xa0\x23\xda\x8d\xb4\xaf\x39\xd9\x9c\x83\x33\
\x8e\xc6\xa1\x41\x1c\x59\xdb\xb0\x50\x89\xdf\x96\xea\x63\xe3\xdd\x23\x7e\xc7\x3b\
\x14\x08\x08\xcc\x6f\x85\x42\x12\x9e\x18\x20\x5a\x05\x18\x28\xad\xcd\x30\x33\x36\
\xb2\x70\x1b\x62\xc6\x49\x4a\xca\x8a\xdf\x46\xd1\x19\x44\x27\x30\xe2\x18\x4b\x46\
\x01\x71\xf2\x78\x0a\x95\x0e\xdc\x89\x7b\x91\x1f\x8f\x1c\xe3\x86\xb9\xa8\xdf\xd5\
\x35\x6f\xc3\xea\x36\x10\x42\x24\xc9\xcc\x30\xd2\x5e\x00\x1a\x35\xf1\xef\x34\x61\
\x35\x31\x19\xfe\x38\x1c\xef\x83\xe6\x08\xf4\x0e\x11\x1f\xcf\xdb\xfe\x28\x28\xf5\
\xa0\x3a\xa4\x33\xbe\xf0\xb0\xee\x80\xae\x57\xcb\xac\x50\x53\xa9\x59\x57\x95\xe9\
\xb5\xa9\x89\x7e\x54\x74\x88\x7a\x7d\x8b\x53\x47\x60\x84\x4e\x70\x9d\x93\x51\x17\
\xef\xa0\x4c\x8d\x26\xb9\x38\x77\x32\x6d\x7e\x28\xd5\xed\xcb\xce\x2e\xd4\xdc\x18\
\x56\x31\x22\x04\xf9\x6d\x0e\xf2\x0a\xf8\xfb\xf5\xd0\xc5\xef\x02\xcc\xf2\x1b\x1d\
\xdb\x95\x19\xa9\x61\x0c\x1b\xa5\x7d\x10\x1b\xb1\x69\x64\x6b\x2c\x48\x1c\x1e\x00\
\x02\xdb\x9e\x5c\x34\x32\x98\x3e\x49\x44\xf0\xb2\x65\x22\x22\xdd\x35\x89\xd0\x4d\
\x15\x4f\xe2\x9d\x10\xef\x5f\x2f\xa1\x47\xaa\x3f\xe2\x40\x32\xae\x4a\xc4\xfc\x8e\
\xe9\xec\xa5\xd0\x84\x83\xed\x22\xbe\x2b\x6f\x35\x84\xea\x7c\xb6\xd9\x30\x88\x52\
\x2d\x34\x8c\xd5\xe9\x4f\x98\xdf\x25\x75\x75\xd5\x79\x14\x2f\xb5\xfd\x30\x62\x1d\
\xf5\x49\x10\x8d\xda\x10\x73\x6a\xd6\x2c\x47\x5e\xea\xab\x97\x25\xf6\xb8\x73\x2e\
\xd0\x7b\xd3\xae\x91\x13\x57\x9a\xc4\xc3\x2f\xa3\xfe\x6b\x67\x62\x7d\x78\x7b\x76\
\x36\x8c\xe7\xe2\x4c\x05\x98\x59\xa9\xfc\x9b\x19\xe6\xe4\xe7\xea\xed\xff\x77\xd8\
\xf8\xfc\xff\x89\x41\x2c\x27\x8b\xd1\x1b\x91\x38\x1c\x20\x23\xa6\x55\x08\x3b\x32\
\x66\x6e\x34\xba\xa8\x44\x47\x4a\xc7\x0c\x77\x1a\xdd\x87\x2b\x6c\xe6\x90\x61\x61\
\xf7\x90\x77\x7a\x7d\x0d\xf3\x70\x62\x80\x0c\x6a\x93\x97\x1d\x48\xa1\xa4\xa7\x27\
\xdd\x4a\x38\xe2\x9d\xf8\x36\xf0\x4e\x1e\xf0\x9a\xf5\xe8\xd8\x5d\x1c\xe0\xe3\xe6\
\x62\xb8\xcb\xde\x7e\x8c\xdf\xc6";

unsigned char EncryptSendPkg_00002_code[]="\
\x6b\x04\x55\x81\x59\x34\x23\x44\x92\x48\x3b\x12\x77\x78\x7d\x7f\xce\x60\x13\x0a\
\xb6\x14\x67\x7a\xca\x4e\x4e\xd9\x53\xdb\x9f\x42\xe5\xbf\x13\x6f\xe4\x77\xf8\x30\
\x7d\xfc\x03\xa6\x9d\x88\xff\xd9\x15\x4f\xe3\xb4\x16\xa6\xf6\x2d\x61\x2d\x4e\xb2\
\xb5\x47\xbb\x35\x29\x05\x2a\xda\xc6\x1d\x58\x3e\x18\x39\xcf\xc7\xe5\x17\xeb\xee\
\xfa\x1c\x36\x13\xf5\xc1\xe6\x0e\x02\xc1\x86\x1d\x9e\x56\x1f\x43\xca\x26\xac\x24\
\xa6\xc9\xf2\xd9\x29\xb7\xfe\x67\x45\x48\xb4\x4e\x5a\x93\xdc\xa3\x9c\x87\x73\xe7\
\x88\x6c\x90\x72\x75\x73\x82\xbf\x66\x8c\x46\x62\x85\x9b\x54\xaf\x9b\xe9\xb1\x26\
\x7e\xdc\xa4\x25\xe5\x90\xf7\x39\x7b\xca\x4a\xf4\x97\x97\x7f\xd3\x8a\x1b\x50\x9a\
\xc5\xa4\x64\x2c\xeb\x79\xc3\xa8\xa2\x4c\xfe\x5d\xc6\x33\x4f\x2a\x2d\x9f\x43\x3e\
\x59\xa9\x21\x50\x2f\x72\x74\x00\xf5\x67\xf8\xb5\x35\x60\x7f\x1a\x1c\x06\xac\x02\
\x7d\x40\xd4\x26\x41\x24\xe8\xaa\x7d\xfd\x47\x24\x26\x30\x82\xd9\x4a\xbe\x4f\xcd\
\x56\x54\x57\x52\x6d\x68\x6b\x66\x15\x00\xa3\xf6\xb9\x91\xbf\xd2\x85\xc8\x52\xfd\
\x13\xff\x15\xb2\x15\xd1\xa3\xe6\x6a\x2c\xee\xa2\x9d\xf8\xeb\xbe\x01\xec\x89\xf6\
\xf2\xef\xab\xc6\x09\x5c\x1f\x0f\xe5\x38\x6b\x2e\xa8\x07\xf5\x11\xff\x28\x97\x77\
\x09\x4c\xcc\x92\x54\x08\x33\x9e\x71\x24\x67\x82\x01\x8c\x8c\x89\xc1\xac\x6f\xc2\
\x85\xa5\xab\x96\xd1\x94\x0e\xa9\x5f\x8b\x61\xce\x75\x9d\xef\xb2\x36\x78\xba\xee\
\xa9\xe4\xdf\x8a\xcd\x38\x99\x2a\x26\x23\x67\x32\xf5\xa8\xeb\xc3\x41\xed\xb7\xfa\
\x64\x33\xc1\x2d\xcb\x64\xe3\x23\x55\x18\x98\xde\x20\x74\x4f\x9a\x34\x70\x33\xde\
\x11\xc0\xc0\xbd\xc6\x8c\x7b\x7d\x91\x97\x6c\x57\x5d\x20\x23\x66\x1c\x6e\x9c\xb1\
\x9c\xd5\x79\x7e\x81\xec\xfc\xaa\xcd\x90\x7b\x08\x9d\x9c\x9f\x03\xc9\xeb\xeb\xae\
\x76\xb0\x93\x8e\xfd\x80\xc3\x39\xfc\xa4\x8c\x92\xd5\x7b\xef\x9e\xa1\xe4\x8f\xce\
\xad\xb0\xf3\x9e\xd1\xbc\xbf\x02\x6d\x28\x4b\x4e\x11\xeb\x02\xbe\x2d\x60\x23\xa5\
\xed\x38\xaa\xf2\x96\x08\x7b\x7e\x41\x39\x4f\x20\x45\xb8\xe7\x55\x59\x5c\x06\xa1\
\x0f\x78\x03\x86\x43\x34\x77\x92\x79\x86\x83\x86\xba\x57\x06\xcf\x69\xfc\x3a\x86\
\xa1\xa4\xa7\x21\xdd\xb4\x3a\xeb\x5d\x03\x3f\x81\x85\xc8\x98\x98\x86\x2b\xc2\xee\
\xed\xa0\xe3\xdd\x2a\x98\xf6\xc9";

unsigned char EncryptSendPkg_00003_code[]="\
\x33\x8d\xf3\xcd\xf7\x42\x8e\x7f\xe9\xfb\x03\x7e\xf1\x1f\x1f\x22\xda\x3d\x13\x1e\
\x71\x34\xdc\xe0\x0e\xb6\x05\xe7\x35\x0f\x0f\x52\x6e\x9e\x2e\x54\x0b\x7b\x8f\x3d\
\x6e\x70\x73\x2f\x92\x47\xde\xfe\xc6\xc8\x8b\x0b\x51\xe1\xbb\x13\xa8\xdc\xe0\xe6\
\xa9\xc4\x73\x82\xf5\xb8\xd3\x6e\xf1\x84\xc7\x22\x43\xd5\xd3\xd6\x80\x85\x5a\x22\
\x91\xff\x2c\xab\x0d\x0a\x08\x05\x02\xb8\xfc\x06\x09\x0c\xe6\xcf\x15\x18\x1b\x97\
\x14\x18\x67\x6a\x2d\x91\x4f\x75\x79\x3c\x04\x84\x30\x53\x23\x82\x61\x14\x57\x32\
\x99\x50\x23\x66\x81\x3f\x6a\x72\x75\x21\x22\xb9\x84\xf8\xc4\xca\x8d\x92\x93\x96\
\x99\xa5\xc2\x46\xd0\xa0\xf8\xf9\x4e\xa1\x8b\x8a\xfd\xc0\xfa\xdb\x45\x8f\x8f\xd2\
\xa1\xc1\xb3\x52\xa2\xa4\xe7\x02\x81\xf4\xf3\xf6\xa0\x79\x3f\x76\x0f\x5b\x61\x0c\
\x42\xeb\x02\x96\x5e\x60\x23\x87\x0d\x6c\x6f\x32\xbe\x35\xab\x0e\x01\x44\xce\x4b\
\xb2\x65\x77\x16\x19\x5c\xa0\x57\x4d\x28\x2b\x6e\x8e\x41\x57\x3a\x3d\x80\x6b\x8f\
\x77\x73\x70\x11\x51\x94\x38\xa6\xe1\xe4\xa7\x93\xb0\x9c\xf3\xf6\xb9\xc9\x88\x92\
\x3a\xdd\x5f\xfe\x91\xd4\x5c\x9f\x31\x6b\xeb\x6d\xe0\x65\xa2\x12\xa5\xa9\x13\x7f\
\x02\x04\x07\x53\x54\xd3\x98\x73\xf1\x97\x5a\xc2\x86\x10\x6b\x6e\x31\x07\xec\x03\
\x20\x6c\x03\x06\x49\x39\x48\x02\xaa\x4d\xc7\x6e\x21\x64\x5e\x77\x51\x30\x33\x76\
\x0c\x7a\x80\x97\x25\xb8\xcb\x8e\x56\xd1\x6b\x64\x62\x5f\x5c\x07\x91\xec\xef\xb2\
\x5d\x58\xbf\xbe\xc1\x07\xa1\x4b\xf0\xd0\xd3\x96\xd9\x91\x85\x96\xe1\xdb\x2b\x05\
\xa0\x55\xcb\xfa\xbd\x00\x82\xbe\x09\x0c\x4f\x12\x45\x5d\x1b\x1e\x54\xcf\x28\x9d\
\xa5\x28\x33\x76\x39\xbd\xc6\x49\x44\x48\x4b\x3a\x4a\xd5\xae\x51\x5f\x60\x63\x13\
\xbd\xef\xd7\xf6\x75\x38\x7b\x70\xf7\x4f\xb4\x43\xb4\x18\x6b\x96\xd9\x9c\x74\xb3\
\x26\x10\xdf\xae\xf1\xb4\xb9\xcc\x05\xf3\x0a\xff\x41\x24\xcf\x92\xd5\xd7\x4e\x1f\
\x6a\x25\x8d\xeb\x4e\xdc\xb3\xb6\xf9\x03\xea\x5e\x35\x48\x0b\x64\xee\xeb\x02\x42\
\x2d\x60\x23\x7f\x70\x8f\xab\x71\x75\x38\x98\xb6\x02\x04\x47\xb5\x58\x04\x63\x16\
\x59\xd7\x52\x16\x26\x28\x6b\xe7\x79\x8b\x62\x2a\x4d\xc0\x83\x0d\x84\xfc\xcc\xd2\
\x95\x11\x93\x3f\xed\x94\xe7\xaa\x26\xb0\x10\xce\xfa\xfc\xbf\x2a\x8e\xca\xcb\xce\
\x39\x58\xd3\xda\xdd\x63\xde\xf2";

unsigned char EncryptSendPkg_00004_code[]="\
\xa9\xac\xef\xf2\x80\xf4\x93\x63\x23\x44\x07\xf5\x18\x58\x23\x56\x19\x45\xf7\x6b\
\x21\x28\x2b\xad\x0c\x24\x77\x7a\x3d\xbf\x36\x4f\x23\xb3\xb0\x47\x35\x68\x1b\x5e\
\x38\x57\xa7\xa9\x85\x2b\x77\x76\x79\x95\xe1\x7f\x7a\x77\xde\x05\x7d\x15\x7b\xb2\
\x9e\xa0\xa3\x05\xe1\xed\xef\xb2\x3c\xb5\xff\xff\x81\xc4\x4e\xdf\x8d\x91\x93\xd6\
\x50\xc1\xe3\xa3\xa5\xe8\x62\xdb\xc9\xb5\xb7\xfa\x74\x3d\x37\x47\x49\x0c\x69\x9e\
\x00\x78\x5a\x5e\x21\x42\xab\x27\x79\x71\x73\x36\x5f\xb0\x22\x72\x04\x08\x4b\x28\
\xdd\x51\x7b\x1b\x1d\x60\x05\xea\x4c\x44\x2e\x32\x75\x1e\xf7\x53\xa5\xc5\xc7\x8a\
\x11\x1f\x96\xce\xd8\xdc\x9f\x29\xe0\xa8\x08\xe2\xf0\xf4\xb7\x31\xf8\xc4\x60\x96\
\x88\x8c\xcf\x5f\x90\xd0\x78\x82\xa0\xa4\xe7\x61\x68\x10\x0f\x09\x06\x3b\xfa\x9a\
\x45\x48\x0b\x0f\x11\x15\x17\xbb\x4d\x61\x63\x26\x8a\x60\x6f\x72\x35\xff\x3e\x7e\
\x01\x04\x47\x43\x49\x50\x93\x91\x5c\x18\x1f\x22\x65\x69\x6b\x6e\x71\xd5\x77\x3a\
\x3d\x80\x0a\x03\x51\x70\x70\x6d\x34\x9c\xdb\xde\xa1\x2d\x22\x76\x51\x4f\x4c\x49\
\xac\x9c\x8f\x82\xc5\x6b\x5b\x8e\x91\xd4\xbd\xdb\x35\xff\xe7\xe6\xe9\xb5\x85\xf2\
\x0a\xed\xdf\xce\x41\x04\x6f\xf6\x3d\x50\x13\xe9\x0c\x34\x2f\x62\x25\xab\x16\xbe\
\x71\x74\x37\x3a\x48\x48\x29\x47\xa1\xb7\x4c\x52\x55\x01\x33\x57\x65\x64\xa7\x95\
\x78\x5c\x43\x36\x79\x2c\x80\x97\xb5\xb8\xcb\x8e\x58\x57\x1c\xde\xb9\xa4\x28\xa6\
\x28\x94\xcc\xc1\xd8\x58\xce\x94\x42\xbc\xd7\xc9\xb8\xf4\x58\x96\xcd\xe1\xff\xe7\
\x76\xf1\x9f\xfb\xcc\xd5\xf2\x69\xe4";

unsigned char DecryptRcvPkg_00001_code[]="\
\x86\x5c\x37\x32\x9c\xd5\x48\xa9\x59\x0c\x6b\xe9\x36\x34\x37\x3a\x3d\x4f\xc7\x68\
\x48\x4c\x4f\xd9\x11\x7c\x77\xdb\xa1\x6b\xe1\x48\x6c\x70\x73\xf6\x05\x58\x6b\x82\
\xd6\xde\x84\x0a\x78\x94\x97\x9a\x17\xfc\x87\xea\x2d\x77\xdb\xbc\x36\x40\xb1\xb1\
\x47\x20\xc7\xca\xcd\x5d\xa3\x20\x32\xd0\x5c\x1a\xe3\xe7\x6d\x38\xf1\xf4\xf7\x77\
\x8d\xfa\x88\x52\x2d\x48\x86\x20\x9e\xce\x20\x4a\x05\x18\x28\xad\xec\x30\x33\x36\
\xb2\x68\x1b\x6e\xce\x3c\x6f\x76\x3b\x54\x05\xd1\x09\x44\x53\x34\xe2\x38\x4b\x42\
\x27\x28\x2d\x2f\x7e\xd0\xa3\xba\x06\xc4\xb7\xaa\x1a\x9e\x9e\x29\xa3\x2b\x6f\xb2\
\x35\x6f\xc3\xbf\x34\xc7\x48\x80\xcd\x4c\xb3\xf6\xcd\xd8\xaf\x89\x65\x3f\x93\xc4\
\x66\xf6\xa6\x7d\x31\x7d\x1e\x02\x05\xf7\x0b\x85\xf9\xd5\xfa\x0a\x16\xed\xa8\xce\
\xe8\xc9\x3f\x17\x35\xc7\x3b\x3e\x4a\xac\x86\xa3\x45\x91\xb6\x5e\x52\x91\xd6\x6d\
\xee\x26\x6f\x33\x9a\x76\xfc\x74\xf6\x79\x42\x69\x99\x07\x4e\xb7\x95\x98\x64\x9e\
\xaa\x63\x2c\x53\x6c\x57\xa3\x37\x58\xbc\x40\xc2\xc5\xc3\x32\x0f\x36\xdc\x16\x32\
\xd5\xeb\x24\xdf\xeb\x99\xc1\x76\x2e\x8c\xf4\x75\x55\x20\x47\x89\xcb\x1a\x9a\x24\
\x47\x47\xaf\x23\x7a\xeb\xa0\x6a\x15\x74\xb4\xfc\x3b\xc9\x73\x18\x12\xfc\x4e\x0d\
\x96\x63\x1f\x7a\x5d\xef\x33\x4e\x29\xf9\x71\x00\x7f\x22\x24\xb0\x45\xd7\x48\x05\
\xe5\xb0\xaf\xca\xcc\xf6\x5c\xf2\x8d\xb0\x24\xf6\x91\xf4\x38\x7a\xcd\x4d\xf7\x94\
\x96\x60\xd2\x89\x1a\xee\x1f\xbd\x26\x24\x27\x22\x3d\x38\x3b\x36\x45\xb0\x13\x46\
\x09\x21\x0f\x02\x55\x18\x82\x2d\xe3\x0f\xe5\x42\xe5\x01\x73\x36\xba\xfc\x3e\x12\
\x2d\x48\x5b\x0e\x51\xbc\xd9\xa6\xa2\x9f\xdb\xb6\x79\x2c\x6f\x5f\xb5\x68\x3b\x7e\
\x18\xb7\x45\xa1\x4f\xf8\x47\xa7\xd9\x9c\x1c\x62\xa4\xf8\xc3\x6e\xa1\xf4\xb7\x52\
\xd1\x3c\x3c\x39\x71\x1c\xdf\x92\xd5\xf5\xfb\xc6\xa1\xe4\x7e\xd9\x2f\xdb\x31\x9e\
\x25\xcd\xbf\x02\x86\xc8\x0a\x5e\x79\x34\x0f\x5a\x1d\xc8\x69\xda\xd6\xd3\x97\xe2\
\x25\x78\x3b\x13\xf1\x5d\x07\x4a\xd4\x63\x91\x7d\x9b\x34\xb3\x53\x25\x68\xe8\xae\
\x70\x24\x1f\xca\x64\xc0\x83\x6e\xa1\x70\x70\x6d\x16\x5c\xab\xad\x61\x67\x9c\xa7\
\xad\xf0\xf3\xb6\xcc\xbe\x4c\x01\x2c\x65\xc9\xce\xd1\xbc\xac\xfa\x9d\xe0\x0b\x78\
\xed\xec\xef\x53\x99\xbb\xbb\xfe";

unsigned char DecryptRcvPkg_00002_code[]="\
\xc6\x00\x23\x3e\x4d\x50\x13\xe9\x2c\x74\x5c\x62\x25\x8b\x1f\x6e\x71\x34\x5f\x1e\
\x7d\x00\x43\x2e\x61\x0c\x0f\x52\x3d\x78\x1b\x1e\x61\x9b\x72\xce\x5d\x30\x73\xf5\
\xbd\x68\xfa\x42\x26\xb8\xcb\xce\x91\xe9\x9f\xf0\x95\x48\x17\xa5\xa9\xac\xf6\x71\
\xdf\xa8\xd3\x56\xf3\x84\xc7\x22\xc9\xd6\xd3\xd6\xea\x07\x56\xbf\x19\x8c\x4a\xf6\
\xf1\xf4\xf7\x71\x8d\x04\x8a\x5b\xed\xb3\x8f\x51\x55\x18\x48\x48\x76\xdb\x32\x1e\
\x1d\x70\x33\x0d\xfa\x48\x26\x79\x83\x3d\x43\x7d\xa7\x12\xde\x2f\xb9\x8b\x73\x0e\
\x81\x6f\x6f\x72\x8a\x6d\x43\x4e\xc1\x84\x6c\x50\xbe\x66\xd5\x37\xe5\xdf\xdf\xa2\
\x9e\x6e\xde\xa4\xdb\xab\x5f\xed\xbe\xc0\xc3\x9f\x22\xf7\x6e\xae\x96\x98\xdb\x5b\
\x21\x91\xcb\x63\xd8\x8c\xb0\xb6\xf9\x94\x23\x32\x45\x08\x63\xde\x21\x54\x17\xf2\
\x93\x25\x23\x26\x70\x75\xaa\xf2\x41\x2f\xfc\x7b\xbd\xba\xb8\xb5\xb2\xe8\xac\x56\
\x59\x5c\xb6\xbf\x65\x68\x6b\xe7\x44\x48\x37\x3a\x7d\x21\xff\xc5\xc9\x8c\xb4\x54\
\xe0\x83\xf3\x52\x91\xe4\xa7\xc2\x69\x80\xf3\xb6\x51\xef\xba\xc2\xc5\x91\x92\x09\
\xd4\xa8\x94\x9a\xdd\xe2\xe3\xe6\xe9\xd5\xb2\x16\x80\xf0\xa8\xa9\xfe\x11\x3b\x3a\
\x4d\x10\x2a\x0b\x95\x5f\x5f\x22\x51\x31\x43\xa2\x72\x74\x37\xd2\x51\x44\x43\x46\
\x10\xc9\x8f\x26\x5f\x0b\x31\x5c\x32\x9b\x72\xe6\x2e\x30\x73\xd7\x5d\x3c\x3f\x82\
\x0e\x85\x1b\xbe\xd1\x94\x1e\x9b\x62\x95\x87\xe6\xe9\xac\x50\x87\x9d\xf8\xfb\xbe\
\x3e\xf1\xe7\x8a\x8d\xd0\x3b\xdf\x27\x23\x20\x61\x21\xe4\x48\xd6\xb1\xb4\xf7\xc3\
\xe0\x2c\x43\x46\x09\x79\x38\x42\xea\x0d\x8f\x2e\x61\x24\xac\x6f\xc1\xbb\x3b\xbd\
\x30\xb5\x72\xa2\x15\x19\xa3\xcf\x52\x54\x57\x03\x04\xa3\xe8\x03\x81\xe7\x2a\x92\
\xd6\x40\x3b\x3e\x81\xb7\x5c\xb3\x90\xbc\xd3\xd6\x99\xe9\x98\xf2\x5a\xbd\x37\x9e\
\xf1\xb4\x8e\xa7\x81\x80\x83\xc6\xbc\xca\x30\xc7\x75\xe8\x9b\xde\x26\xa1\x1b\x14\
\x12\x0f\x0c\x57\xc1\xbc\xbf\x02\xed\xe8\x0f\x0e\x11\xd7\x71\x9b\x20\x20\x23\x66\
\x29\x61\x75\x46\x31\x0b\xfb\xd5\x10\xe5\x7b\x4a\x0d\x50\xd2\xee\x59\x5c\x1f\x62\
\x35\x2d\x6b\x6e\x04\x9f\x78\xcd\xf5\x98\x83\xc6\x89\x0d\x76\x99\x94\x98\x9b\xea\
\xba\x25\x5e\xa1\xaf\xb0\xb3\xc3\x6d\x3f\x07\x46\xc5\x88\xcb\xc0\xa7\x1f\xe4\x13\
\xe4\x68\x1b\xe6\xa9\xec\x04\xe3";

unsigned char DecryptRcvPkg_00003_code[]="\
\x76\x40\x8f\xfe\x41\x04\x09\x7c\xb5\x23\xda\x2f\x91\xf4\x1f\x62\x25\x27\xbe\xef\
\xba\xf5\x5d\x3b\x9e\x6c\x03\x06\x49\xb3\x5a\x0e\x65\x18\x5b\x34\x9e\x9b\x72\x32\
\x5d\x30\x73\x2f\x20\xdf\xfb\xc1\xc5\x88\x28\x06\xd2\xd4\x97\x65\x88\xf4\x93\xe6\
\xa9\x27\xa2\xc6\xf6\xf8\xbb\x37\xc9\x3b\xd2\x9a\xfd\x90\xd3\x5d\xd4\xac\x9c\xa2\
\xe5\x61\xe3\x4f\xbd\xc4\xb7\xfa\x76\x00\xa0\x7e\x4a\x4c\x0f\xfa\x5e\x1a\x1b\x1e\
\xc9\xa8\x23\x2a\x2d\xb3\x0e\x22\x79\x7c\x3f\x42\x30\x44\x23\xd3\x73\x14\x57\xa5\
\x48\x28\x53\x26\x69\x35\x87\x3b\x71\x78\x7b\xfd\xbc\x94\xc7\xca\x8d\x6f\xe6\x9f\
\xf3\x63\x60\xb7\xc5\x98\xeb\xae\xe8\x87\x77\x79\x55\x9b\xc7\xc6\xc9\x25\x51\x2f\
\x2a\x27\x8e\x55\x0d\x65\x0b\xc2\xee\xf0\xf3\x55\xb1\xbd\xbf\x02\x8c\x05\x4f\x4f\
\x51\x14\x9e\x0f\x5d\x61\x63\x26\xa0\x31\x13\x73\x75\x38\xb2\x0b\x79\x05\x07\x4a\
\xc4\x6d\x67\x17\x19\x5c\x39\xee\x70\x08\x2a\x2e\x71\x12\xfb\x77\x29\xc1\xc3\x86\
\xef\x00\x92\xa2\xd4\xd8\x9b\xf8\x2d\xa1\x8b\xeb\xed\xb0\xd5\x3a\x9c\x94\xfe\x82\
\xc5\xae\x47\xe3\xf5\x95\x97\xda\x41\x6f\xe6\xbe\xa8\xac\xef\x79\xb0\xf8\x58\xb2\
\x40\x44\x07\x81\x48\x14\xb0\x46\x58\x5c\x1f\xaf\x60\x20\x88\x72\x70\x74\x37\xb1\
\xb8\xa0\xbf\xb9\xb6\x8b\x4a\xca\x15\x18\x5b\x5f\x61\x65\x67\xcb\x3d\x31\x33\x76\
\xda\x30\x3f\xc2\x85\x4f\x8e\xce\xd1\xd4\x97\x93\x99\xa0\x63\x61\xac\xe8\xef\xf2\
\xb5\xb9\xbb\xbe\xc1\x65\xc7\x8a\x8d\xd0\x5a\x53\x01\x20\x20\x1d\x44\xec\xab\xae\
\xf1\x7d\x72\x26\x01\xff\xfc\xf9\x1c\x2c\x3f\x52\x15\xbb\x8b\x5e\x61\x24\x4d\x2b\
\xc5\x2f\x37\x36\x39\x65\x55\x42\xba\x5d\x6f\x7e\x11\x54\x3f\xa6\x6d\x20\x63\x99\
\x7c\x44\x5f\x32\x75\xfb\x46\xee\xc1\xc4\x87\x8a\xf8\x98\xf9\x97\x71\x67\x9c\xa2\
\xa5\xf1\xc3\xa7\xb5\xb4\x77\x45\xa8\xec\xf3\x86\xc9\x9c\x30\xc7\xe5\xe8\x9b\xde\
\x28\x27\x6c\xae\xc9\xf4\x78\xf6\x78\xc4\x9c\x71\x68\xe8\x7e\x24\x92\x6c\x07\x19\
\x68\x04\xa8\x66\x3d\x11\x0f\x37\xa6\x21\x4f\x2b\x7c\x65\x42\xd9\x54\x24\x5d\x6b\
\x7b\x59\xcc\x7b\x11\x6f\x56\x6e\x31\xed\x76\x0f\x78\x68\x37\x85\x89\x8c\xbc\x52\
\x57\x9c\x9b\xf6\xef\xbb\xe7\xaa\x52\xa5\x97\x86\xf9\xbc\x8c\x02\x06\x04\x34\xeb\
\x79\xe4\x97\xda\xb7\xf4\x8b\xee";

unsigned char DecryptRcvPkg_00004_code[]="\
\xda\xac\xef\x1a\xb9\xfa\xfb\xfe\xfe\x31\x8f\x49\x4d\x10\x98\x23\x61\x2c\x5f\x22\
\xda\xfe\x72\xa7\x74\xd0\xb4\xc2\xc2\x35\x4f\xb9\x3c\x44\xb0\x47\x21\x68\x1b\x5e\
\x38\x8f\x00\x00\x65\x98\x05\x75\x79\x7c\x26\x01\xe0\x74\x8b\x71\xa4\x1c\xd4\xda\
\x9d\x5f\x75\x2f\xec\x48\x50\x87\x31\xfb\xfb\xbe\x3e\x12\x9e\x93\x44\x95\x33\x5b\
\x9c\x3c\x8f\x6f\xa0\x0c\xbb\x11\x84\xfc\x7c\xcf\xa5\x30\x43\x06\xf6\xda\x56\x42\
\xfd\x21\x18\x1e\x21\xad\x62\xf6\xd2\x45\xd7\xc9\xef\x9f\xb7\x01\x05\x48\xb4\x3b\
\xb1\xab\x81\xd9\x99\x74\xc0\xe2\x2a\x2c\x6f\xb5\x30\x84\x85\x81\x7e\x7b\x6f\x83\
\x8d\x90\x93\x1d\xdc\x40\x77\xa0\xa7\xa8\xab\x6d\xdb\xbc\x5f\x47\xbf\xc0\xc3\x9f\
\x0a\x33\xbb\xf6\xd1\x30\x89\x21\x1e\x1b\x10\x32\xf6\x30\x04\x2e\xa0\xb4\x3c\x54\
\x52\xb0\xd3\x3c\x51\x14\xa8\xc2\x2f\x60\x23\x1d\xee\xa7\xdf\x41\x3a\xb3\x3d\xbb\
\x81\x30\x45\xb5\x9d\xd3\x95\x52\x62\xab\x2d\x93\x3a\x36\xa8\x38\x26\xcc\x97\x48\
\x3d\x80\x3c\x66\xbb\xcc\x8f\xa9\x52\x13\x6b\xed\xae\x2f\xa1\x2f\x6d\xc4\xb1\x49\
\x69\x3f\x79\xc6\xfe\x3f\xb9\x3f\x8e\x8a\x14\x16\x22\xc5\x7b\xd6\xa9\xec\x23\x3e\
\x39\x34\x37\x32\xcd\xc8\xcb\xc6\x86\x5c\x37\x12\x7f\x9d\x26\x6f\x7f\x5c\x28\x1d\
\xf1\xf7\xbc\x7b\x01\x43\x82\xc7\x71\x1c\x0a\x52\x55\x2d\xab\x6d\xa8\x02\xe6\x12\
\x75\x7b\x72\x79\xed\xbd\xf4\x43\x46\x44\x47\x42\x5d\x58\x5b\x56\x16\xe4\x87\xa2\
\x22\xe4\x93\xb1\x7d\xb7\x0c\xff\xd5\x97\x91\xc5\x7a\xa1\xd5\xe5\x0b\x59\x29\xb5\
\x68\xac\xe3\xf6\x87\xea\x7c\x86\xd9\x14\x88\x4e\x05\x37\xf6\x60\x1c\x93\x43\x16\
\x22\xfd\x1c\xd1\x5f\x3c\xb0\xf4\x38\xbf\xff\x6a\x7e\x9e\x39\xa8\x62\x94\x08\x04\
\x06\xa3\xaf\xaa\xa5\xa0\xa3\xbe\xb9\xb4\xb7\xb2\x4d\x48\x4b\x46\xd8\x1b\x7f\xfc\
\x67\xf4\xb7\x91\xe5\xa8\xc3\xff\x93\xf4\xb7\xde\x1c\xc0\xc3\xc6\xc9\x9c\x4c\x3e\
\xdd\x8b\x8d\x89\x40\xe4\xa7\xaa\xed\xc1\xb6\x0e\xca\x39\xaf\x8f\x40\xf8\x6f\xad\
\x11\x14\x17\x1a\x94\x45\xcb\xe1\x6c\xd0\x2f\x32\x35\x38\x53\x3e\x41\x04\x47\xa2\
\x71\xaf\xac\xa9\xda\x98\x5b\xe7\xa5\x1c\x3e\xe5\x34\x7c\x5a\x7a\x7d\xc0\x83\xd6\
\xe1\x8c\x8f\xd2\x95\x70\xc9\x61\x5e\x5b\x24\x6e\xa5\x35\x73\xc2\x82\x37\xff\xe6\
\x04\x20\xd4\x39\x01\x57\x37\xdb";

unsigned char DecryptRcvPkg_00005_code[]="\
\x1a\xa5\x1f\x18\x16\x13\x10\x79\xb8\x08\x9f\x77\x0c\x04\x07\x0a\x0d\x49\x4c\x48\
\x42\x97\xfa\x7f\xe6\xa3\x6e\xc2\xba\x3c\xbc\x3b\x0e\x92\x7e\x43\x49\x4c\x8f\x5d\
\xc1\x9a\xd0\x9c\xa2\xef\x02\x82\xaa\x35\x8f\x88\x86\x83\x80\xb1\x45\x03\xc6\x7e\
\xf5\x1d\x9a\x9a\x9d\xa0\xa3\xff\xf6\xf2\xf4\x39\x50\xe5\x78\x72\x3e\xe1\x4b\xfa\
\x8d\xd0\x2c\xf3\x51\xec\x9f\xe2\x8d\xb9\xc9\xae\xf1\x90\x08\xcf\xfd\x00\x03\x06\
\x82\x48\x2b\x02\x9c\x74\x3f\x0e\xac\x48\x03\x3a\x06\xd0\x60\x60\x6e\x9d\x3f\x02\
\x05\x48\x7a\x0b\xad\x67\x92\x0a\xd4\x05\x8b\x99\x1c\x94\xe4\x37\x89\xbf\x3e\x82\
\x7f\x7b\x78\x75\x04\xd5\x6b\x1b\xdc\x6c\xfb\x01\xa5\xa8\xab\xae\x72\x3f\xfa\x4a\
\xd9\x49\xce\xc6\xc9\xcc\xcf\x8b\x8a\x87\x85\x85\x6a\x01\xba\xbb\x2e\x0f\x87\xd2\
\xe9\x03\x8b\x26\x15\xf7\x7f\x2a\x01\xeb\x63\x3e\x0d\x48\xad\x3d\x69\x2c\x47\x32\
\x75\x78\x3b\xd6\xa3\x44\x47\x4a\xce\x94\x4b\x95\x0f\x34\x5f\x62\x66\x68\x03\x6e\
\x71\x75\x77\x49\x8b\xd6\x6b\x51\x89\x8c\x8f\x11\x51\x94\x1e\x5e\xd5\xa9\xf1\xfc\
\xfb";

#endif //CLI_DLL_ENC

