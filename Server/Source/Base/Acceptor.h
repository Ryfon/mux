﻿/** @file Acceptor.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2007-10-10
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef ACCEPTOR_H
#define ACCEPTOR_H

#include "./BufQueue/BufQueue.h"
#include "./BufQueue/BufQueueImp.h"

/**
* @class CAcceptor
*
* @brief 抽象地接受器工厂类
*/
template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
class CAcceptor : public ACE_Acceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>
{
private:
	CAcceptor( const CAcceptor& map);
	CAcceptor& operator = ( const CAcceptor& map ); //屏蔽这两个操作；

public:
	/// 
	typedef ACE_Acceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2> parent;

	/// 缺省构造
	CAcceptor(void);

	/// 构造
	CAcceptor(IBufQueue *pReadQueue, IBufQueue *pSendQueue,unsigned int uiPeerBufferSize);

	/// 析构
	virtual ~CAcceptor(void);

public:
	/// 初始化
	virtual int open (const ACE_PEER_ACCEPTOR_ADDR &local_addr, ACE_Reactor *reactor,
		               int flags = 0, int use_select = 1, int reuse_addr = 1);
	
	/// 重写handler的获取规则
	virtual int make_svc_handler(SVC_HANDLER *&sh);


	/// 获取/设置队列
	/// 获取已读队列
	IBufQueue * ReadQueue(void);

	/// 设置已读队列
	void ReadQueue(IBufQueue *pReadQueue);

	/// 获取待发队列
	IBufQueue * SendQueue(void);

	/// 设置待发队列
	void SendQueue(IBufQueue *pSendQueue);

private:
	/// 已读消息队列 
	IBufQueue *m_pReadQueue;

	/// 待发消息队列
	IBufQueue *m_pSendQueue;

	/// 连接端发送缓存
	unsigned int m_uiPeerBufferSize;

};

#if defined(ACE_TEMPLATES_REQUIRE_SOURCE)
#include "Acceptor.cpp"
#endif

#if defined(ACE_TEMPLATES_REQUIRE_PRAGMA)
#pragma implementation("Acceptor.cpp")
#endif


#endif/*ACCEPTOR_H*/
