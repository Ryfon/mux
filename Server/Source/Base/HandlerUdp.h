﻿/** @file HandlerUdp.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-7-23
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef HANDLER_UDP_H
#define HANDLER_UDP_H

#include "aceall.h"

/**
* @class CHandlerUdp
*
* @brief 基于udp的处理模块
* 
* 该类有两种工作方式，一种直接使用，另外一种是依赖反应器使用
*/
class CHandlerUdp : public ACE_Event_Handler
{
	enum TMODE
	{
		NO_USE_REACTOR,
		USE_REACTOR
	};

public:
	/// 构造
	explicit CHandlerUdp(unsigned short usLocalPort);

	/// 构造
	explicit CHandlerUdp(const char *pszRemoteHost, unsigned short usRemotePort);

	/// 析构
	virtual ~CHandlerUdp(void);

	void ReleaseUdpHandle()
	{
		close();
	}

public:
	/// 初始化
	virtual int open(void * arg= 0);

	/// 清理
	virtual int close(u_long flags = 0);

	/// 输入事件发生时被框架回调（数据到来）
	virtual int handle_input(ACE_HANDLE fd = ACE_INVALID_HANDLE);

	/** 
	* 超时时被框架回调. <current_time>是<CHandlerT>因超时被分派的时间，
	* <act>是在<schedule_timer>时传进的参数
	*/	
	virtual int handle_timeout(const ACE_Time_Value &current_time, const void *act = 0);

	/** 
	* 当<handle_*>返回-1或者<remove_handler>被框架调用时.<close_mask>
	* 表明什么事件触发<handle_close>被调用,<handle>指定具体的句柄
	*/
	virtual int handle_close(ACE_HANDLE handle, ACE_Reactor_Mask close_mask);

	/// 获取句柄
	virtual ACE_HANDLE get_handle(void) const;


public:
	/// 发送数据
	int Send(const char *pszData, unsigned int uiDataLen);

private:
	/// 工作方式
	TMODE m_mode;

	/// 定时器
	long m_lTimerID;

	/// 对端地址
	ACE_INET_Addr m_remoteAddr;

	/// 本地地址
	ACE_INET_Addr m_localAddr;

	/// 对端
	ACE_SOCK_Dgram m_peer;
};


#endif/*HANDLER_UDP_H*/
