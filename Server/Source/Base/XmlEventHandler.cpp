﻿#include "XmlEventHandler.h"


#ifdef USE_ACE_SAX_PARSE

#include "XmlManager.h"
#include "Utility.h"


CXmlEventHandler::CXmlEventHandler(const ACEXML_Char* pszXmlName, std::vector<CElement*> &elementSet)
: m_pszFileName(ACE::strnew (pszXmlName)), m_pLocator(NULL), m_uiCurrentLevel(0), m_elementSet(elementSet),m_pTemp(NULL)
{
}

CXmlEventHandler::~CXmlEventHandler(void)
{
	delete []m_pszFileName;
}

void CXmlEventHandler::setDocumentLocator (ACEXML_Locator *pLocator)
{
	m_pLocator = pLocator;
}

void CXmlEventHandler::startDocument (ACEXML_ENV_SINGLE_ARG_DECL)
{

}

void CXmlEventHandler::endDocument (ACEXML_ENV_SINGLE_ARG_DECL)
{

}

void CXmlEventHandler::characters (const ACEXML_Char *pCh, size_t uiStart,
								   size_t uiLength ACEXML_ENV_ARG_DECL)
{
	ACE_UNUSED_ARG(uiStart); //by dzj,07.12.05;

	if(NULL == pCh)
		return;

	const ACEXML_Char *pTempCh = pCh;
	while((unsigned int)(pTempCh - pCh) < uiLength)
	{
		if((*pTempCh == '\x20') || (*pTempCh == '\x0D') || 
		   (*pTempCh == '\x0A') || (*pTempCh == '\x09'))
		{
			pTempCh++;
		}
		else
		{
			m_pTemp->Value(pCh);
			break;
		}
	}

	return;
}

void CXmlEventHandler::startElement (const ACEXML_Char *pszNamespaceURI,
									 const ACEXML_Char *pszLocalName,
									 const ACEXML_Char *pszQName,
									 ACEXML_Attributes *pAlist
									 ACEXML_ENV_ARG_DECL_NOT_USED)
{
	ACE_UNUSED_ARG(pszNamespaceURI); //by dzj,07.12.05;
	ACE_UNUSED_ARG(pszLocalName); //by dzj,07.12.05;

	// 属性数目
	unsigned int uiAttrNum = (pAlist == NULL) ? 0 : (unsigned int)(pAlist->getLength());
	unsigned int uiParentIndex = (m_elementSet.empty()) ? 0 : m_pTemp->Index();

	CElement * pElement = NEW CElement(uiParentIndex, (unsigned int)m_elementSet.size(), pszQName, m_uiCurrentLevel, uiAttrNum);
	if(pElement != NULL)
	{
		for(size_t i = 0; i < uiAttrNum; i++)
		{
			pElement->AddAttr(pAlist->getQName(i), pAlist->getValue(i));
		}
		
		m_elementSet.push_back(pElement);
	}

	IncLevel();

	// 当前元素
	m_pTemp = pElement;
}

void CXmlEventHandler::endElement (const ACEXML_Char *pszNamespaceURI,
								   const ACEXML_Char *pszLocalName,
								   const ACEXML_Char *qName ACEXML_ENV_ARG_DECL)
{
	ACE_UNUSED_ARG(pszNamespaceURI); //by dzj,07.12.05;
	ACE_UNUSED_ARG(pszLocalName); //by dzj,07.12.05;
	ACE_UNUSED_ARG(qName); //by dzj,07.12.05;
	if(m_uiCurrentLevel > 0)
		DecLevel();

	// 当前元素
	m_pTemp = m_elementSet[m_pTemp->ParentIndex()];
}

//void CXmlEventHandler::ignorableWhitespace (const ACEXML_Char *ch, int start, int length ACEXML_ENV_ARG_DECL)
//{
//	ACE_UNUSED_ARG(ch);
//	ACE_UNUSED_ARG(start);
//}

void CXmlEventHandler::error (ACEXML_SAXParseException &exception ACEXML_ENV_ARG_DECL)
{
	ACE_UNUSED_ARG(exception); //by dzj,07.12.05;
}

void CXmlEventHandler::fatalError (ACEXML_SAXParseException &exception ACEXML_ENV_ARG_DECL)
{
	ACE_UNUSED_ARG(exception); //by dzj,07.12.05;
}

void CXmlEventHandler::warning (ACEXML_SAXParseException &exception ACEXML_ENV_ARG_DECL)
{
	ACE_UNUSED_ARG(exception); //by dzj,07.12.05;
}

void CXmlEventHandler::IncLevel(void)
{
	m_uiCurrentLevel++;
}

void CXmlEventHandler::DecLevel(void)
{
	m_uiCurrentLevel--;
}

#endif/*USE_ACE_SAX_PARSE*/
