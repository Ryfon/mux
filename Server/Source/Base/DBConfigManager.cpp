﻿#include "DBConfigManager.h"
#include "XmlManager.h"
#include "Utility.h"

CDBConfigManager::CDBConfigManager(void)
{

}

CDBConfigManager::~CDBConfigManager(void)
{
	Clear();
}

int CDBConfigManager::Init(const char* pszFileName, const char *pszServerType)
{
	Clear();

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR("db配置文件加载出错");
		return -1;
	}

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("db配置文件可能为空");
		return -1;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);

	CElement *pServerElement = NULL;
	unsigned int uiServerElementIndex = 0;
	unsigned short uiConnStringIndex = 0;
	for(std::vector<CElement *>::iterator i = childElements.begin(); i != childElements.end(); i++)
	{
		if(strncmp((*i)->Name(), pszServerType, 16) == 0 )
		{
			pServerElement = *i;
			uiServerElementIndex = pServerElement->Index();

			continue;
		}

		if(pServerElement != NULL) 
		{	
			if((*i)->ParentIndex() != uiServerElementIndex)
				break;

			stConnectionString *pConnstr = NEW stConnectionString;
			if(pConnstr == NULL)
			{
				D_DEBUG("初始化stConnectionString错误\n");
				return -1;
			}
			//const char* dbstr = (*i)->GetAttr("db");
			//const char* hoststr = (*i)->GetAttr("host");
			//const char* passwordstr = (*i)->GetAttr("password");
			//const char* userstr = (*i)->GetAttr("user");
			//SafeStrCpy(pConnstr->szDb, dbstr);
			//SafeStrCpy(pConnstr->szHost, hoststr);
			//SafeStrCpy(pConnstr->szPassword, passwordstr);
			//SafeStrCpy(pConnstr->szUser, userstr);
			//StructMemCpy(pConnstr->szDb, dbstr, sizeof(pConnstr->szDb));
			//StructMemCpy(pConnstr->szHost, hoststr, sizeof(pConnstr->szHost));
			//StructMemCpy(pConnstr->szPassword, passwordstr, sizeof(pConnstr->szPassword));
			//StructMemCpy(pConnstr->szUser, userstr, sizeof(pConnstr->szUser));

			//StructMemCpy(pConnstr->szDb, (*i)->GetAttr("db"), sizeof(pConnstr->szDb));
			//StructMemCpy(pConnstr->szHost, (*i)->GetAttr("host"), sizeof(pConnstr->szHost));
			//StructMemCpy(pConnstr->szPassword, (*i)->GetAttr("password"), sizeof(pConnstr->szPassword));
			//StructMemCpy(pConnstr->szUser, (*i)->GetAttr("user"), sizeof(pConnstr->szUser));
			SafeStrCpy( pConnstr->szDb, (*i)->GetAttr("db") );
			SafeStrCpy( pConnstr->szHost, (*i)->GetAttr("host") );
			SafeStrCpy( pConnstr->szPassword, (*i)->GetAttr("password") );
			SafeStrCpy( pConnstr->szUser, (*i)->GetAttr("user") );
			pConnstr->port = STRING_TO_NUM((*i)->GetAttr("port"));

			m_dbConnectionInfos[uiConnStringIndex] = pConnstr;
			uiConnStringIndex++;
		}
	}

	return 0;
}

stConnectionString * CDBConfigManager::Find(unsigned short usConnectionStringIndex)
{
	std::map<unsigned short, stConnectionString *>::iterator i = m_dbConnectionInfos.find(usConnectionStringIndex);
	if(i == m_dbConnectionInfos.end())
		return NULL;
	else
		return i->second;
}

int CDBConfigManager::Clear(void)
{
	for(std::map<unsigned short, stConnectionString *>::iterator i = m_dbConnectionInfos.begin();
		i != m_dbConnectionInfos.end(); i++)
	{
		delete i->second;
		i->second = NULL;
	}

	m_dbConnectionInfos.clear();
	return 0;
}
