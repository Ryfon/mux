﻿
/*
  新任务接口
  目的：与dstwothread一起使用，区分辅助线程与主线程上下文，同时使用一个单线程池管理IDBTask对象
  by dzj, 10.08.04
*/

#ifndef DB_NEW_TASK
#define DB_NEW_TASK

#include "../tcache/tcache.h"

class CConnection;

template< typename T_Task >
class NewTaskPool;

//先后由主线程-->辅助线程-->主线程处理的对象，由于各线程对本对象的处理是先后进行的，因此对象内部无需加锁；
template< typename T_SubType >
class IDBTask
{
public:
	IDBTask() { m_pOrgPool = NULL; }

	bool InitPoolTask( NewTaskPool<T_SubType>* pOrgPool/*自身所属池*/ )
	{
		m_pOrgPool = pOrgPool;
		return true;
	}

	virtual ~IDBTask(){}

	PoolFlagDefine()
	{
	};

public:
	//辅助线程处理；
	virtual bool AssistProcMsg( CConnection* pDBConn ) = 0;
	//主线程结果处理；
	virtual bool MainRstProcMsg() = 0;

private:
	NewTaskPool<T_SubType>* m_pOrgPool;
};//class IDBTask

template< typename T_Task >
class NewTaskPool
{
public:
	NewTaskPool()
	{
		m_pThisTaskPool = NULL;
	}

	~NewTaskPool()
	{
		if ( NULL != m_pThisTaskPool )
		{
			delete m_pThisTaskPool; m_pThisTaskPool = NULL;
		}
	}

public:
	bool InitPool( unsigned int poolSize )
	{
		if ( NULL != m_pThisTaskPool )
		{
			return false;
		}
		m_pThisTaskPool = NEW TCache<T_Task>( poolSize );
		return true;
	}

public:
	//从池中分配一个T_Task;
	T_Task* GetNewTask()
	{
		if ( NULL == m_pThisTaskPool )
		{
			return NULL;
		}
		T_Task* pTask = m_pThisTaskPool->Retrieve();
		if ( NULL == pTask )
		{
			return NULL;
		}

		pTask->InitPoolTask( this );
		return pTask;
	}

	//释放一个T_Task回池;
	bool ReleseOldTask( T_Task* pToRelease )
	{
		if ( NULL == m_pThisTaskPool )
		{
			return false;
		}
		m_pThisTaskPool->Release( pToRelease );
		return true;
	}

private:
	TCache<T_Task>* m_pThisTaskPool;
};//class NewTaskPool


#endif //DB_NEW_TASK

