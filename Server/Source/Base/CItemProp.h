﻿/** @file CItemProp.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-4-29
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/


#ifndef ITEM_PROP_H
#define ITEM_PROP_H


#include "../../Base/ItemGloal.h"
using namespace ITEM_GLOBAL;



class CItemPublicProp
{
public:
	CItemPublicProp(){};
	~CItemPublicProp(){};

public:
	u32 itemTypeID;//道具类型ID
	u32 ItemModel;//道具模型
	u32 ItemIcon;//道具ICON
	s32 itemPrice;//道具价钱
	s32 itemLevel;//装备道具等级
	ITEM_TYPE itemType;//道具类型
	ITEM_METAL itemMetal;//道具的材质
	s32 itemMaxAmount;//道具的最大个数
	char ItemName[64];//道具名称

public:
	u32 GetItemTypeID(){ return itemTypeID ; }
};


//装备的二级属性
struct EquipSecondProp
{
	ITEM_GEARARM_TYPE mGearArmType;//装备类型
	ITEM_NATION       mNation;//道具的种族
	ITEM_CLASS        mClass;//道具的职业 
	s32               mSpeed;//道具的附加速度
};

//装备的三级属性
struct EquipThirdProp
{
	ITEM_MASSLEVEL  mMasslevel;//一个道具等级
	ITEM_TRADE      mTrade;//交易方式
	s32             mCannotChuck;//是否可丢弃
	s32             mDeathLost;//死忙是否掉落
	s32             mDeopt;//是否可存仓
	s32             mPhysicsDefend;//物理防御力
	s32             mMagicDefend;//魔法防御力
	s32             mPhysicsAttack;//物理攻击力
	s32             mMagicAttack;//魔法攻击力
};

//武器的二级属性
struct WeaponSecondProp
{
	ITEM_GEARARM_TYPE mGearArmType;//装备类型
	ITEM_NATION       mNation;//道具的种族
	ITEM_CLASS        mClass;//道具的职业
	s32               mSkillRatio;//技能触发概率
	s32               mSkillID;//技能ID
};

//武器的三级属性
struct WeaponThirdProp
{
	ITEM_MASSLEVEL  mMasslevel;//一个道具等级
	ITEM_TRADE      mTrade;//交易方式
	s32             mCannotChuck;//是否可丢弃
	s32             mDeathLost;//死忙是否掉落
	s32             mDeopt;//是否可存仓 
	s32             AttackPhyMax;//攻击的最大值
	s32             AttackPhyMin;//攻击的最小值
	s32             AttackMagMax;//魔法攻击最大值
	s32             AttackMagMin;//魔法攻击的最小值
};

struct FunctionItemProp
{
	u32 TouchScript;
};

struct SkillItemProp
{
	u32 m_SkillID;
};

struct TaskItemProp
{
	u32 m_TaskScript;
	u32 m_TaskID;
};

struct DrugItemProp
{
	u32 m_ItemSkill;
};

#endif  