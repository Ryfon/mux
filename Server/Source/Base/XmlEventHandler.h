﻿/** @file XmlEventHandler.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2007-11-2
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef XML_EVENT_HANDLER_H
#define XML_EVENT_HANDLER_H

#include "aceall.h"
#include <vector>

#ifdef USE_ACE_SAX_PARSE

class CElement;


/**
* @class CXmlEventHandler
*
* @brief 基于SAX的事件处理器
*
* 定义基于SAX的解析的相关操作
*/
class CXmlEventHandler : public ACEXML_DefaultHandler
{
public:
	/// 构造 
	CXmlEventHandler(const ACEXML_Char* pszXmlName, std::vector<CElement*> &elementSet);

	/// 析构
	virtual ~CXmlEventHandler(void);

public:
	/// 设置文档定位器
	virtual void setDocumentLocator (ACEXML_Locator *pLocator);

	/// 文档开始时触发
	virtual void startDocument (ACEXML_ENV_SINGLE_ARG_DECL);

	/// 文档结束时触发
	virtual void endDocument (ACEXML_ENV_SINGLE_ARG_DECL);

	/// 处理文本时触发
	virtual void characters (const ACEXML_Char *pCh, size_t uiStart,
							 size_t uiLength ACEXML_ENV_ARG_DECL);

	/// 元素开始时触发
	virtual void startElement (const ACEXML_Char *pszNamespaceURI,
								const ACEXML_Char *pszLocalName,
								const ACEXML_Char *pszQName,
								ACEXML_Attributes *pAlist
								ACEXML_ENV_ARG_DECL_NOT_USED);

	/// 元素结束时触发
	virtual void endElement (const ACEXML_Char *pszNamespaceURI,
							 const ACEXML_Char *pszLocalName,
							 const ACEXML_Char *qName ACEXML_ENV_ARG_DECL);
	/// 忽略空格
	/*virtual void ignorableWhitespace (const ACEXML_Char *ch, 
									  int start, 
									  int length ACEXML_ENV_ARG_DECL);*/


	/// 错误时触发
	virtual void error (ACEXML_SAXParseException &exception ACEXML_ENV_ARG_DECL);

	/// 致命错误时触发
	virtual void fatalError (ACEXML_SAXParseException &exception ACEXML_ENV_ARG_DECL);

	/// 警告时触发
	virtual void warning (ACEXML_SAXParseException &exception ACEXML_ENV_ARG_DECL);

	/// 层次自加
	void IncLevel(void);

	/// 层次自减
	void DecLevel(void);

private:
	/// 拷贝构造
	CXmlEventHandler(const CXmlEventHandler &);
	
	/// 赋值
	CXmlEventHandler &operator=(const CXmlEventHandler &);

private:
	/// 文件名
	ACEXML_Char *m_pszFileName;

	/// 定位器
	ACEXML_Locator * m_pLocator;

	/// 当前层次
	unsigned int m_uiCurrentLevel;

	/// 元素集
	std::vector<CElement*> &m_elementSet;

	/// 临时变量
	CElement *m_pTemp;
};

#endif/*USE_ACE_SAX_PARSE*/

#endif/*XML_EVENT_HANDLER_H*/
