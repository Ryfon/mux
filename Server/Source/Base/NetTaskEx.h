﻿/** @file NetTaskEx.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-6-16
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef NET_TASK_EX_H
#define NET_TASK_EX_H

#include "Acceptor.h"
#include "Connector.h"
#include "SessionManager.h"
#include "BufQueue/BufQueue.h"
#include "BufQueue/BufQueueImp.h"
#include "HandlerEx.h"
#include <list>


/// 前置声明
template <class SVC_HANDLER, class ACE_LOCK> class CNetTaskEx;


template <class SVC_HANDLER, class ACE_LOCK>
class CTask : public ACE_Task_Base
{
	friend class CNetTaskEx<SVC_HANDLER, ACE_LOCK>;

private:
	CTask(IBufQueue *pReadQueue, IBufQueue *pSendQueue, ACE_Reactor *pReactor, CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *pHandlerManager);
	virtual ~CTask(void);

public:
	int open(void *args /* = 0 */);

	int svc(void);

	int Quit(void);

private:
	/// 读取队列
	IBufQueue *m_pReadQueue;

	/// 发送队列
	IBufQueue *m_pSendQueue;

	/// 反应器
	ACE_Reactor *m_pReactor;

	/// 
	CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *m_pHandlerManager;

	/// 
	CAcceptorEx< SVC_HANDLER, ACE_SOCK_Acceptor, ACE_LOCK > *m_pAcceptor;
};



template <class SVC_HANDLER, class ACE_LOCK>
class CTaskHelper : public ACE_Task_Base
{
	friend class CNetTaskEx<SVC_HANDLER, ACE_LOCK>;

private:
	CTaskHelper(IBufQueue *pReadQueue, IBufQueue *pSendQueue, ACE_Reactor *pReactor, CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *pHandlerManager);
	virtual ~CTaskHelper(void);

public:
	virtual int open(void *args /* = 0 */);

	virtual int svc(void);

	void Quit(void);

private:
	IBufQueue *m_pReadQueue;

	IBufQueue *m_pSendQueue;

	ACE_Reactor *m_pReactor;

	CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *m_pHandlerManager;

	CConnectorEx< SVC_HANDLER, ACE_SOCK_Connector, ACE_LOCK > *m_pConnector;

	std::list<MsgToPut *> * m_pToSwitchQueueRead;

	bool m_bQuit;
};


template <class SVC_HANDLER, class ACE_LOCK>
class CNetTaskEx
{
public:
	CNetTaskEx(IBufQueue *pReadQueue, IBufQueue *pSendQueue);

	~CNetTaskEx(void);

public:
	int Open(void *args /* = 0 */);

	void Quit(void);

	int Wait(void);

private:
	/// 读取队列
	IBufQueue *m_pReadQueue;

	/// 发送队列
	IBufQueue *m_pSendQueue;

	/// 反应器
	ACE_Reactor *m_pReactor;

	/// 
	static CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> m_handlerManager;

	/// 
	CTask<SVC_HANDLER, ACE_LOCK> *m_pTask;

	/// 
	CTaskHelper<SVC_HANDLER, ACE_LOCK> *m_pTaskHelper;
};

#if defined(ACE_TEMPLATES_REQUIRE_SOURCE)
#include "NetTaskEx.cpp"
#endif

#if defined(ACE_TEMPLATES_REQUIRE_PRAGMA)
#pragma implementation("NetTaskEx.cpp")
#endif

#endif/*NET_TASK_EX_H*/
