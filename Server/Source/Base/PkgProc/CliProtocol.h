﻿#ifndef CLI_PROTOCOL_H
#define CLI_PROTOCOL_H

#include "srvincludecli.h"

#define CHECK_FUNC_PARAM_IS_NULL\
	if ( NULL == pOwner ){ return false; }\
	if ( NULL == pPkg ){ return false; }


#define  PKGPOINTCHECK( TYPE, P ){\
	P = (const TYPE *)(pPkg);\
	}

#ifndef MUX_CLI_INCLUDE
	#define  PKGSIZECHECK( TYPE ){\
		if ( wPkgLen != sizeof(TYPE) ){\
		D_DEBUG("SrvErr:消息包大小错误:%s\n", #TYPE );\
		return false;}\
		}
#endif //MUX_CLI_INCLUDE

#define  PKG_CONVERT(TYPE)    const TYPE* pMsg = NULL;\
	PKGSIZECHECK(TYPE);\
	PKGPOINTCHECK(TYPE,pMsg);

#define MAX_RACE_PRESTIGE  9999999

//装备对人物的随机属性加成对应ID号(对应策划配置文件中的配置号)
enum ItemRandAddAtt
{
	//幸运属性，随机集确定
    IRA_addStr = 1, //          | 1   力量
    IRA_addAgi = 2, //          | 2   敏捷
    IRA_addInt = 3, //          | 3   智力
    IRA_addSpi = 4, //          | 4   精神
    IRA_addVit = 5, //幸运      | 5   体质
    IRA_addHpMax = 6, //          | 6   HP上限
    IRA_addMpMax = 7, //          | 7   MP上限
    IRA_addLuckComb = 8, //          | 8   幸运装备合成率
    IRA_addLuckAtk = 9, //          | 9   幸运一击率

	//卓越属性，随机集确定
    IRA_addAtkStun = 10, //          | 10   击晕（几率）等级
    IRA_addAtkTie = 11, //          | 11   束缚（几率）等级
    IRA_addAtkPhExc = 12, //卓越      | 12   物理卓越一击伤害
    IRA_addAtkMgExc = 13, //武器      | 13   魔法卓越一击伤害
    IRA_addAtkSpd = 14, //          | 14   攻击速度提升

    IRA_addAntiStun = 15, //卓越      | 15   抗晕几率
    IRA_addAntiTie = 16, //防具      | 16   抗束几率
    IRA_addPhyHide = 17, //          | 17   物理闪避 
    IRA_addMgHide = 18, //          | 18   魔法闪避
    IRA_addAntiPE = 19, //          | 19   抗物理卓越一击
    IRA_addAntiME = 20, //          | 20   抗魔法卓越一击

	//除幸运与卓越外的其它属性，由IIP_TYPEID+基本XML属性确定
    IRA_add21 = 21, //           | 21   20%几率造成额外物理伤害
    IRA_add22 = 22, //           | 22   20%几率造成额外魔法伤害
    IRA_add23 = 23, //           | 23   抗晕（几率）等级
    IRA_add24 = 24, //           | 24   抗束缚（几率）等级
    IRA_add25 = 25, //未确定增加 | 25   物理闪躲率
    IRA_add26 = 26, //基本XML确定| 26   魔法闪躲率
    IRA_add27 = 27, //           | 27   抗物理卓越一击率
    IRA_add28 = 28, //           | 28   抗魔法卓越一击率
	IRA_max
};//enum ItemAttAdd

//装备对人物的基本属性加成枚举定义
enum ItemBaseAddAtt
{
	IBA_phyAtk = 100, //物攻(IIP_TYPEID+基本XML+升级XML属性)
	IBA_mgAtk = 101, //魔攻(IIP_TYPEID+基本XML+升级XML属性)
	IBA_phyHitRatio = 102, //物理命中(IIP_TYPEID+基本XML属性)
	IBA_mgHitRatio = 103,  //魔法命中(IIP_TYPEID+基本XML属性)
	IBA_phyDef = 104, //物防(IIP_TYPEID+基本XML+升级XML属性)
	IBA_mgDef = 105, //魔防(IIP_TYPEID+基本XML+升级XML属性)
	IBA_spdAdd = 106 //移动速度提升(IIP_TYPEID+基本XML)
};

struct ItemAddtionProp
{
	ItemAddtionProp()
	{
		ResetItemAddtionProp();
	}

	bool ResetItemAddtionProp()
	{
		memset( this, 0, sizeof(*this) );
		return true;
	}

	int Hp;
	int Mp;
	int PhysicsAttack; //物理攻击
	int MagicAttack;   //魔法攻击
	int PhysicsDefend; //物理防御
	int MagicDefend;   //魔法防御
	int PhysicsHit;    //物理命中率
	int MagicHit;      //魔法命中率
	int PhysicsCritical;
	int MagicCritical;
	int PhysicsAttackPercent;
	int MagicAttackPercent;
	int PhysicsCriticalDamage;
	int MagicCriticalDamage;
	int PhysicsDefendPercent;
	int MagicDefendPercent;
	int PhysicsCriticalDerate;
	int MagicCriticalDerate;
	int PhysicsCriticalDamageDerate;
	int MagicCriticalDamageDerate;
	int PhysicsJouk;
	int MagicJouk;
	int PhysicsDeratePercent;
	int MagicDeratePercent;
	int Stun;
	int Tie;
	int Antistun;
	int Antitie;
	int PhysicsAttackAdd;
	int	MagicAttackAdd;
	int PhysicsRift;
	int MagicRift;
	int AddSpeed;      //速度提升
	int AttackPhyMin;
	int AttackPhyMax;
	int AttackMagMin;
	int AttackMagMax;
	int strength;
	int agility;
	int intelligence;
	int spirit;
	int physique;
	int luckyCombindRatio;
	int luckyhitRatio;
	int phyexcellentdemage;
	int magexcellentdemage;
	int attackspeed;
	int ingorephydefend;
	int ingoremagdefend;
	int movespeed;
	int cureaddition;
	int hatredaddition;
	int enhancespecialskill;
	int antiPE;
	int antiME;
	int excellentbiff;//卓越一击
	int luckybiff;//幸运一击
	int dumpVal;//无用值，若有无效属性，则加到此值
};

namespace MUX_PROTO
{
	const int MAX_MSG_SIZE = 512;
	template<int> struct OrgMsgDefineChecker;
	template<> struct OrgMsgDefineChecker<0> {};

	//用于确保不会定义大于预定大小的消息结构,消息结构最大为507；
#define MSG_SIZE_GUARD(msgtype) \
	namespace MSG_CHECK{ struct check_##msgtype{\
	OrgMsgDefineChecker<(sizeof(msgtype)>=507)?sizeof(msgtype):0> struct_lager_than_507_##msgtype;\
	};}//namespace MSG_CHECK;
};

using namespace MUX_PROTO;

//注意由于戒指,耳环,手镯有2个栏位,自动判断下一个栏位+1是否能装备, INDEX_TWO_HAND需要7和8都空才能装备
const int EQUIPTYPE_POS[] = 
{
	0,       //IDNEX_HEAD      	=	0,         	//头
	1,       //INDEX_BODY      	=	1,         	//身体
	2,       //INDEX_HAND      	=	2,         	//手部
	3,       //INDEX_FOOT      	=	3,         	//脚部
	4,       //INDEX_SHOULDER  	=	4,         	//肩部
	5,       //INDEX_BACK      	=	5,         	//背部
	6,       //INDEX_RING      	=	6,         	//戒指
	7,       //INDEX_NECKLACE  	=	7,         	//项链
	8,       //INDEX_MAIN_HAND 	=	8,         	//主手
	9,       //INDEX_ASSIS_HAND	=	9,         	//副手
	10,      //INDEX_RIDE      	=	10,        	//坐骑
	11,      //INDEX_BADGE     	=	11,        	//徽章
	8,       //双手装备
	8,       //单手装备，既可装备在位置8又可装备在位置9
}; // ITEM_GEARARM_TYPE

//装备类型
enum ITEM_GEARARM_TYPE
{
	E_HEAD = 0x0,//头部装备
	E_BODY,//身体装备
	E_HAND,//手部装备
	E_FOOT,//脚部装备
	E_SHOULDER,//肩部装备
	E_BACK,//背部装备
	E_FINGER,//手指装备
	E_NECKLACE,//项链装备
	E_FIRSTHAND,//主手装备
	E_SECONDHAND,//副手装备
	E_DRIVE,//坐骑装备
	E_BADGE,//徽章
	E_DOUBLEHAND,//双手武器
	E_SINGLEHAND,//单手武器
	E_POSITION_MAX,
};

//背包列数
#define PKG_COLUMN 11


//取装备的可能装配位置，对于单手武器，可能有两个位置返回
inline bool GetDefaultEquipPos( ITEM_GEARARM_TYPE gearType, unsigned int& possiPos1, bool& isSecPosValid, unsigned int& possiPos2 )
{
	isSecPosValid = (gearType == E_SINGLEHAND) ? true:false;
	possiPos2 = 9;//只有单手武器，第二个位置可能有效，且第二个位置必定为9;
	if ( gearType >= (sizeof(EQUIPTYPE_POS)/sizeof(EQUIPTYPE_POS[0])) )
	{
		return false;
	}
	possiPos1 = EQUIPTYPE_POS[gearType];
	return true;
}// bool GetDefaultEquipPos( ITEM_GEARARM_TYPE gearType, unsigned int& possiPos1, bool& isSecPosValid, unsigned int& possiPos2 )

//装备类型与指定装配位置是否一致；
inline bool IsEquipPosAccord( ITEM_GEARARM_TYPE gearType, unsigned int checkPos )
{
	unsigned int possiPos1=0, possiPos2=0;
	bool isSecPosValid = false;
	bool isGetPosOK = GetDefaultEquipPos( gearType, possiPos1, isSecPosValid, possiPos2 );
	if ( !isGetPosOK )
	{
		return false;
	}
	return (checkPos==possiPos1) /*与第一个可能装配位置相符*/
		    || ( (isSecPosValid) && (checkPos==possiPos2) )/*与第二个可能装配位置相符*/;
}// bool IsEquipPosAccord( ITEM_GEARARM_TYPE gearType, unsigned int checkPos )

//两个装备类型的装备位置是否一致
inline bool IsGearTypeSameEquipPos( ITEM_GEARARM_TYPE gearType1, ITEM_GEARARM_TYPE gearType2 )
{
	if ( gearType1 == gearType2 )
	{
		return true;//同类型装备，装备位置自然一致；
	}

	unsigned int firstPos1=0, firstPos2=0;
	bool isSecOK1 = false;
	bool isGetPosOK = GetDefaultEquipPos( gearType1, firstPos1, isSecOK1, firstPos2 );
	if ( !isGetPosOK )
	{
		return false;
	}

	unsigned int secPos1=0, secPos2=0;
	bool isSecOK2 = false;
	isGetPosOK = GetDefaultEquipPos( gearType2, secPos1, isSecOK2, secPos2 );
	if ( !isGetPosOK )
	{
		return false;
	}

	return ( ( firstPos1 == secPos1 ) 
		|| ( isSecOK1 && ( firstPos2 == secPos1 ) )
		|| ( isSecOK1 && isSecOK2 && ( firstPos2 == secPos2 ) )
		|| ( isSecOK2 && ( firstPos1 == secPos2 ) )
		);
}// bool IsGearTypeSameEquipPos( ITEM_GEARARM_TYPE gearType1, ITEM_GEARARM_TYPE gearType2 )

//是否骑乘装备的装配位
inline bool IsDriveEquipPos( unsigned int equipPos )
{
	return equipPos == EQUIPTYPE_POS[E_DRIVE];
}


/*
    以下，从GCSimplePlayerInfo中取信息与设信息宏
	BYTE                     	raceClass;                                  	//种族与职业,高4位种族，低4位职业
	BYTE                     	ucFlag1;                                    	//按bit从高到低依次为:是否组队，是否队长，是否流氓，是否虚弱，是否时装态，是否新手保护，是否新手失效，是否族长
	简单信息中装备数组位置：胸(0)、手1(1)、手2(2)、脚(3)、肩(4)、背(5)；
*/

#define SE_BODY     0 //胸
#define SE_HAND     1 //手
#define SE_WEAPON   2 //武器
#define SE_FOOT     3 //脚
#define SE_SHOULDER 4 //肩
#define SE_BACK     5 //背(翅膀)
#define SE_HEAD     6 //头盔
#define SE_WEAPON2  7 //第二武器(副手武器)

#define GetRaceFromSimpleInfo( simpleInfo )     ( ((simpleInfo).raceClass) >> 4 ) //高4位种族
#define GetClassFromSimpleInfo( simpleInfo )    ( ((simpleInfo).raceClass) & (0x0f) ) //低4位职业

#define SetRaceClassPre( simpleInfo )           ( ((simpleInfo).raceClass) &= 0x00 ) //设种族职业前清0
#define SetRaceToSimpleInfo( simpleInfo, inrace ) ( ((simpleInfo).raceClass) |= ((inrace&0x000f)<<4) ) //高4位种族
#define SetClassToSimpleInfo( simpleInfo, inclass ) ( ((simpleInfo).raceClass) |= (inclass&0x000f) ) //低4位职业

#define IsTeamSimpleInfo( simpleInfo )       ( ( ( (simpleInfo).ucFlag1 & (0x01<<7) ) >> 7 ) > 0 ? true:false ) //是否组队
#define IsHeaderSimpleInfo( simpleInfo )     ( ( ( (simpleInfo).ucFlag1 & (0x01<<6) ) >> 6 ) > 0 ? true:false ) //是否队长
#define IsRogueSimpleInfo( simpleInfo )      ( ( ( (simpleInfo).ucFlag1 & (0x01<<5) ) >> 5 ) > 0 ? true:false ) //是否流氓
#define IsWeakSimpleInfo( simpleInfo )       ( ( ( (simpleInfo).ucFlag1 & (0x01<<4) ) >> 4 ) > 0 ? true:false ) //是否虚弱
#define IsFashionSimpleInfo( simpleInfo )    ( ( ( (simpleInfo).ucFlag1 & (0x01<<3) ) >> 3 ) > 0 ? true:false ) //是否时装态
#define IsRookieSimpleInfo( simpleInfo )     ( ( ( (simpleInfo).ucFlag1 & (0x01<<2) ) >> 2 ) > 0 ? true:false ) //是否新手保护
#define IsRFaultSimpleInfo( simpleInfo )     ( ( ( (simpleInfo).ucFlag1 & (0x01<<1) ) >> 1 ) > 0 ? true:false ) //是否新手失效
#define IsRMasterSimpleInfo( simpleInfo )    ( ( ( (simpleInfo).ucFlag1 & (0x01<<0) ) >> 0 ) > 0 ? true:false ) //是否族长

#define SetSimpleInfoFlag1Pre( simpleInfo )        ( ((simpleInfo).ucFlag1) &= 0x0000 ) //设flag1前清0   
#define SetSimpleInfoTeam( simpleInfo, inbool )    ( ((simpleInfo).ucFlag1) |= (((unsigned char)inbool)<<7) )  //设置是否组队
#define SetSimpleInfoHeader( simpleInfo, inbool )  ( ((simpleInfo).ucFlag1) |= (((unsigned char)inbool)<<6) )  //设置是否队长
#define SetSimpleInfoRogue( simpleInfo, inbool )   ( ((simpleInfo).ucFlag1) |= (((unsigned char)inbool)<<5) )  //设置是否流氓
#define SetSimpleInfoWeak( simpleInfo, inbool )    ( ((simpleInfo).ucFlag1) |= (((unsigned char)inbool)<<4) )  //设置是否虚弱
#define SetSimpleInfoFashion( simpleInfo, inbool ) ( ((simpleInfo).ucFlag1) |= (((unsigned char)inbool)<<3) )  //设置是否时装态
#define SetSimpleInfoRookie( simpleInfo, inbool )  ( ((simpleInfo).ucFlag1) = ( ( ((simpleInfo).ucFlag1) & (~((0x01)<<2))/*是否新手保护设置前可能没有pre，所以要先清下0*/ ) | (((unsigned char)inbool)<<2) ) )  //设置是否新手保护
#define SetSimpleInfoRBFault( simpleInfo, inbool ) ( ((simpleInfo).ucFlag1) |= (((unsigned char)inbool)<<1) )  //设置是否新手失效
#define SetSimpleInfoRMaster( simpleInfo, inbool ) ( ((simpleInfo).ucFlag1) |= (((unsigned char)inbool)<<0) )  //设置是否族长

//性别|称号掩码信息(BaseInfo1中的shFlag与SimplePlayerInfo中的shFlag)设置提取
#define GetSexFromSHFlag( inSHFlag ) ( inSHFlag&0x01 ) //从性别称号(掩码)中提取性别信息;
#define SetSexToSHFlag( inSex, outSHFlag ) ( outSHFlag = inSex?(outSHFlag|0x01):(outSHFlag&(~0x01)) ) //设置性别信息至性别称号(掩码)
#define GetHMaskFromSHFlag( inSHFlag ) ( inSHFlag>>1 ) //取shFlag中的称号掩码信息；
#define SetHMaskToSHFlag( inHMask, outSHFlag ) ( outSHFlag = (GetSexFromSHFlag(outSHFlag) | (inHMask<<1)) ) //设shFlag中的称号掩码信息

//namespace MUX_PROTO
//{
//	struct ItemAddtionProp
//	{
//		TYPE_SI32                	Hp;                                         	//
//		TYPE_SI32                	Mp;                                         	//
//		TYPE_SI32                	PhysicsAttack;                              	//
//		TYPE_SI32                	MagicAttack;                                	//
//		TYPE_SI32                	PhysicsDefend;                              	//
//		TYPE_SI32                	MagicDefend;                                	//
//		TYPE_SI32                	PhysicsHit;                                 	//
//		TYPE_SI32                	MagicHit;                                   	//
//		TYPE_SI32                	PhysicsCritical;                            	//
//		TYPE_SI32                	MagicCritical;                              	//
//		TYPE_SI32                	PhysicsAttackPercent;                       	//
//		TYPE_SI32                	MagicAttackPercent;                         	//
//		TYPE_SI32                	PhysicsCriticalDamage;                      	//
//		TYPE_SI32                	MagicCriticalDamage;                        	//
//		TYPE_SI32                	PhysicsDefendPercent;                       	//
//		TYPE_SI32                	MagicDefendPercent;                         	//
//		TYPE_SI32                	PhysicsCriticalDerate;                      	//
//		TYPE_SI32                	MagicCriticalDerate;                        	//
//		TYPE_SI32                	PhysicsCriticalDamageDerate;                	//
//		TYPE_SI32                	MagicCriticalDamageDerate;                  	//
//		TYPE_SI32                	PhysicsJouk;                                	//
//		TYPE_SI32                	MagicJouk;                                  	//
//		TYPE_SI32                	PhysicsDeratePercent;                       	//
//		TYPE_SI32                	MagicDeratePercent;                         	//
//		TYPE_SI32                	Stun;                                       	//
//		TYPE_SI32                	Tie;                                        	//
//		TYPE_SI32                	Antistun;                                   	//
//		TYPE_SI32                	Antitie;                                    	//
//		TYPE_SI32                	PhysicsAttackAdd;                           	//
//		TYPE_SI32                	MagicAttackAdd;                             	//
//		TYPE_SI32                	PhysicsRift;                                	//
//		TYPE_SI32                	MagicRift;                                  	//
//		TYPE_SI32                	AddSpeed;                                   	//
//		TYPE_SI32                	AttackPhyMin;                               	//
//		TYPE_SI32                	AttackPhyMax;                               	//
//		TYPE_SI32                	AttackMagMin;                               	//
//		TYPE_SI32                	AttackMagMax;                               	//
//		TYPE_SI32                	strength;                                   	//力量
//		TYPE_SI32                	agility;                                    	//敏捷
//		TYPE_SI32                	intelligence;                               	//智力
//		TYPE_SI32                	spirit;                                     	//精神
//		TYPE_SI32                	physique;                                   	//体质
//		TYPE_SI32                	luckyCombindRatio;                          	//幸运装备合成率
//		TYPE_SI32                	luckyhitRatio;                              	//幸运一击率
//		TYPE_SI32                	phyexcellentdemage;                         	//物理卓越一击伤害
//		TYPE_SI32                	magexcellentdemage;                         	//魔法卓越一击伤害
//		TYPE_SI32                	attackspeed;                                	//攻击速率
//		TYPE_SI32                	ingorephydefend;                            	//无视物理防御
//		TYPE_SI32                	ingoremagdefend;                            	//无视魔法防御几率
//		TYPE_SI32                	movespeed;                                  	//移动速度
//		TYPE_SI32                	cureaddition;                               	//治疗法术加成
//		TYPE_SI32                	hatredaddition;                             	//仇恨值提升
//		TYPE_SI32                	enhancespecialskill;                        	//特殊技能等级提升
//	};//struct ItemAddtionProp
//};//namespace MUX_PROTO

//};

#endif/*CLI_PROTOCOL_H*/

