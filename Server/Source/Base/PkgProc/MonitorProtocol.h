﻿#ifndef			__Monitor_Protocol__
#define			__Monitor_Protocol__
#include "ParserTool.h"
#include "map"

namespace MUX_BG_Monitor
{
	//Enums
	typedef	enum
	{
		ST_ASSIGN_SRV           	=	0,            	//Assign Server
		ST_GATE_SRV             	=	1,            	//Gate Server
		ST_LOGIN_SRV            	=	2,            	//
		ST_CENTER_SRV           	=	3,            	//
		ST_MAP_SRV              	=	4,            	//
		ST_DB_SRV               	=	5,            	//
		ST_RELATION_SRV         	=	6,            	//
	}ServerType;

	typedef	enum
	{
		X_G_QUERY_MONITOR_RECORD	=	0xd101,       	//消息 ID - 请求监控记录 agent to gate server
		G_X_MONITOR_RECORD      	=	0x1d02,       	//消息 ID - 监控记录 gate server to agent
		MS_X_CONTROL            	=	0xd103,       	//消息 ID - 控制信息 monitor server to agent
		MC_MS_SERVER_LIST_QUERY 	=	0xd104,       	//消息 ID - 查询服务器列表 mc to ms
		MS_MC_SERVER_LIST       	=	0xd105,       	//消息 ID - 服务器列表 ms to mc
		MC_MS_SET_HOT_SPOT      	=	0xd106,       	//消息 ID - 设置焦点服务器 monitor client to monitor server
		MC_MS_RECORD_QUERY      	=	0xd107,       	//消息 ID - 查询一个时间段内的所有记录
		MS_MC_MONITOR_RECORD    	=	0xd108,       	//消息 ID - 服务器的一组监控记录 monitor server to monitor client
	}MessageID;


	//Defines

	//Typedefs

	//Types
	struct Monitor_Record
	{
		static const USHORT	wCmd = G_X_MONITOR_RECORD;

		INT          	target_id;        	//目标服务器编号
		INT          	target_ip;        	//目标服务器 ip 地址
		INT          	target_type;      	//目标服务器类型
		INT          	online_player_num;	//在线人数
		INT          	cpu_usage_percent;	//CPU 占用率百分比
		INT          	memory_usage;     	//内存使用绝对量
		INT          	str_len;          	//字符串长度
		CHAR         	date_time[24];      	//日期 时间
	};

	struct Monitor_Control
	{
		static const USHORT	wCmd = MS_X_CONTROL;

		INT          	frequency;        	//agent 向 server 发送监控记录的频率
	};

	struct Server_List_Query
	{
		static const USHORT	wCmd = MC_MS_SERVER_LIST_QUERY;
	};

	struct Hot_Spot_Set
	{
		static const USHORT	wCmd = MC_MS_SET_HOT_SPOT;

		INT          	target_ip;        	//设置热点监控服务器
	};

	struct One_Server
	{
		INT          	target_id;        	//目标服务器编号
		INT          	target_ip;        	//目标服务器 ip 地址
		INT          	target_type;      	//目标服务器类型
	};

	struct Server_List
	{
		static const USHORT	wCmd = MS_MC_SERVER_LIST;

		INT          	ServerNum;        	//
		One_Server   	ServerVolum[128];   	//
	};

	struct Record_Query
	{
		static const USHORT	wCmd = MC_MS_RECORD_QUERY;

		INT          	str_len;          	//字符串长度
		CHAR         	begin_time[24];     	//起始时间
		CHAR         	end_time[24];       	//结束时间
		INT          	target_ip;        	//目标服务器 ip 地址
	};

	struct Simple_Record
	{
		INT          	online_player_num;	//在线人数
		INT          	cpu_usage_percent;	//CPU 占用率百分比
		INT          	memory_usage;     	//内存使用绝对量
		INT          	str_len;          	//字符串长度
		CHAR         	date_time[24];      	//时间
	};

	struct Range_Record
	{
		static const USHORT	wCmd = MS_MC_MONITOR_RECORD;

		INT          	target_id;        	//目标服务器编号
		INT          	target_ip;        	//目标服务器 ip 地址
		INT          	target_type;      	//目标服务器类型
		INT          	RecordNum;        	//
		Simple_Record	RecordVolum[24];    	//
	};


	//Messages
	typedef enum Message_id_type
	{
		MSG_Monitor_Record         	=	G_X_MONITOR_RECORD,     	//Monitor_Record
		MSG_Monitor_Control        	=	MS_X_CONTROL,           	//Monitor_Control
		MSG_MC_MS_Server_List_Query	=	MC_MS_SERVER_LIST_QUERY,	//Server_List_Query
		MSG_MS_MC_Server_List      	=	MS_MC_SERVER_LIST,      	//Server_List
		MSG_MC_MS_Set_Hot_Spot     	=	MC_MS_SET_HOT_SPOT,     	//Hot_Spot_Set
		MSG_MC_MS_Record_Query     	=	MC_MS_RECORD_QUERY,     	//Record_Query
		MSG_MS_MC_Monitor_Record   	=	MS_MC_MONITOR_RECORD,   	//Range_Record
	};

	//Class Data
	class Monitor_Protocol;
	typedef size_t (Monitor_Protocol::*EnCodeFunc)(void* pData);
	typedef size_t (Monitor_Protocol::*DeCodeFunc)(void* pData);

	class 
	#ifdef WIN32
	//_declspec(dllexport) 
	#endif
	Monitor_Protocol
	{
	public:
		Monitor_Protocol();
		~Monitor_Protocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__Monitor_Record(void* pData);
		size_t	DeCode__Monitor_Record(void* pData);

		size_t	EnCode__Monitor_Control(void* pData);
		size_t	DeCode__Monitor_Control(void* pData);

		size_t	EnCode__Server_List_Query(void* pData);
		size_t	DeCode__Server_List_Query(void* pData);

		size_t	EnCode__Hot_Spot_Set(void* pData);
		size_t	DeCode__Hot_Spot_Set(void* pData);

		size_t	EnCode__One_Server(void* pData);
		size_t	DeCode__One_Server(void* pData);

		size_t	EnCode__Server_List(void* pData);
		size_t	DeCode__Server_List(void* pData);

		size_t	EnCode__Record_Query(void* pData);
		size_t	DeCode__Record_Query(void* pData);

		size_t	EnCode__Simple_Record(void* pData);
		size_t	DeCode__Simple_Record(void* pData);

		size_t	EnCode__Range_Record(void* pData);
		size_t	DeCode__Range_Record(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool         m_kPackage;
	};
}
#endif			//__Monitor_Protocol__
