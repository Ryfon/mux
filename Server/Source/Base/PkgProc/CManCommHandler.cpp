﻿/**
* @file CManCommHandler.cpp
* @brief 管理到本机的所有连接信息
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: CManCommHandler.cpp
* 摘    要: 管理到本机的所有连接信息，新来连接时将连接添加到本管理器，收包时在本管理器中找到相应收包处理器进行处理；
* 作    者: dzj
* 完成日期: 2007.11.29
*
*/

//#include "stdafx.h"
#include "CManCommHandler.h"
