﻿#ifndef			__LogServiceProtocol__
#define			__LogServiceProtocol__
#include "ParserTool.h"
#include "map"

namespace LogServiceNS
{
	//Enums
	typedef	enum
	{
		MAX_TEAM_IND_LEN               	=	12,              	//
		MAX_TEAM_NAME_LEN              	=	20,              	//
		MAX_MEMBER_IND_LEN             	=	16,              	//
		MAX_TEAM_MEMBERS_COUNT         	=	7,               	//
		MAX_TEAM_ATTRI_KEY_LEN         	=	1,               	//
		MAX_TEAM_ATTRI_VALUE_LEN       	=	1,               	//
		MAX_TEAM_BROADCAST_DATA_LEN    	=	256,             	//
		MAX_ATTRI_COUNT                	=	1,               	//
	}PROTOCOL_CONSTANT;

	typedef	enum
	{
		G_O_LogTrade                   	=	0x1c01,          	//
		G_O_LogShopping                	=	0x1c02,          	//
		G_O_logplayerOP                	=	0x1c03,          	//
		G_O_logGMOP                    	=	0x1c04,          	//
		G_O_logGMpublish               	=	0x1c05,          	//
		G_O_logGMkickdown              	=	0x1c06,          	//
		G_O_logtrack_item              	=	0x1c07,          	//
		G_O_logtrack_player_gold       	=	0x1c08,          	//
		G_O_logtrack_player_login      	=	0x1c09,          	//
		G_O_logtrack_player_map_change 	=	0x1c0a,          	//
		G_O_logtrack_player_team_change	=	0x1c0b,          	//
		G_O_logtrack_item_complex      	=	0x1c0c,          	//
		G_O_logtrack_allot_skill_point 	=	0x1c0d,          	//
		G_O_logtrack_wash_skill_point  	=	0x1c0e,          	//
		G_O_logtrack_player_growing    	=	0x1c10,          	//
		G_O_logtrack_player_task       	=	0x1c11,          	//
	}MessageId;

	typedef	enum
	{
		G_O_RESULT_SUCCEED             	=	50001,           	//
		G_O_RESULT_FAILED              	=	50002,           	//
	}G_O_RESULT_CODE;

	typedef	enum
	{
		MAX_RNAME_SIZE                 	=	20,              	//
		MAX_RMSSAGE_SIZE               	=	128,             	//
		MAX_TRADE_SIZE                 	=	128,             	//
		MAX_SHOPPING_TYPE_SIZE         	=	128,             	//
		MAX_PLAYER_OP_TYPE_SIZE        	=	128,             	//
		MAX_GMOP_TYPE_SIZE             	=	128,             	//
		MAX_GM_CHANNEL_TYPE_SIZE       	=	128,             	//
		MAX_OP_ITEM_TYPE_SIZE          	=	128,             	//
		MAX_ACTIVE_OP_SIZE             	=	128,             	//
		MAX_ACTIVE_OP_TYPE_SIZE        	=	128,             	//
		MAX_HAND_TIME_SIZE             	=	128,             	//
	}DemarcateValue;


	//Defines

	//Typedefs

	//Types
	struct player
	{
		USHORT     	gate_srv_id;                           	//Gate Server's identification
		UINT       	player_pointer;                        	//Player's Object pointer in the Gate Server
	};

	struct trade_goods
	{
		UINT       	goods_id;                              	//商品id
		UINT       	sum;                                   	//个数
	};

	struct player_log_trade
	{
		static const USHORT	wCmd = G_O_LogTrade;

		UINT       	buyer_accont;                          	//买家帐号
		UINT       	buyer_id;                              	//买家id
		USHORT     	buyer_name_Len;                        	//买家名字的大小
		CHAR       	buyer_name[MAX_RNAME_SIZE];              	//
		USHORT     	buyer_goods_sum;                       	//买家物品的个数
		trade_goods	buyer_goods[MAX_TRADE_SIZE];             	//
		UINT       	buyer_money;                           	//买家金钱
		UINT       	seller_accont;                         	//卖家帐号
		UINT       	seller_id;                             	//卖家id
		USHORT     	seller_name_Len;                       	//卖家名字的大小
		CHAR       	seller_name[MAX_RNAME_SIZE];             	//
		USHORT     	seller_goods_sum;                      	//卖家物品的个数
		trade_goods	seller_goods[MAX_TRADE_SIZE];            	//
		UINT       	seller_money;                          	//卖家金钱
		UINT       	mapid;                                 	//地图编号
		UINT       	buyer_position;                        	//卖家位置
		UINT       	seller_position;                       	//卖家位置
		USHORT     	hand_time_Len;                         	//买家名字的大小
		CHAR       	hand_time[MAX_HAND_TIME_SIZE];           	//
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	};

	struct player_log_shopping
	{
		static const USHORT	wCmd = G_O_LogShopping;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		UINT       	npc_shopping_id;                       	//玩家id
		USHORT     	goods_lst_sum;                         	//交易物品列表的个数
		trade_goods	goods_lst[MAX_TRADE_SIZE];               	//
		UINT       	money;                                 	//金钱
		USHORT     	shopping_type_Len;                     	//交易类型的大小
		CHAR       	shopping_type[MAX_SHOPPING_TYPE_SIZE];   	//
		UINT       	mapid;                                 	//地图编号
		UINT       	player_position;                       	//玩家位置
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	};

	struct log_player_OP
	{
		static const USHORT	wCmd = G_O_logplayerOP;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		USHORT     	player_OP_type_Len;                    	//交易类型的大小
		CHAR       	player_OP_type[MAX_PLAYER_OP_TYPE_SIZE]; 	//
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	};

	struct log_GM_OP
	{
		static const USHORT	wCmd = G_O_logGMOP;

		UINT       	GM_id;                                 	//GM帐号
		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		USHORT     	GM_OP_type_Len;                        	//交易类型的大小
		CHAR       	GM_OP_type[MAX_GMOP_TYPE_SIZE];          	//
		UINT       	time_long;                             	//时间长度
		UINT       	mapid;                                 	//地图编号
		UINT       	GM_position;                           	//玩家位置
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	};

	struct log_GM_publish
	{
		static const USHORT	wCmd = G_O_logGMpublish;

		UINT       	GM_id;                                 	//GM帐号
		USHORT     	channel_Len;                           	//频道名字的大小
		CHAR       	channel[MAX_GM_CHANNEL_TYPE_SIZE];       	//
		USHORT     	publish_content_Len;                   	//公告内容的大小
		CHAR       	publish_content[MAX_RMSSAGE_SIZE];       	//
		UINT       	mapid;                                 	//地图编号
		UINT       	GM_position;                           	//玩家位置
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	};

	struct log_GM_kick_down
	{
		static const USHORT	wCmd = G_O_logGMkickdown;

		UINT       	GM_id;                                 	//GM帐号
		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		UINT       	mapid;                                 	//地图编号
		UINT       	player_position;                       	//玩家位置
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};

	struct log_track_item
	{
		static const USHORT	wCmd = G_O_logtrack_item;

		UINT       	item_id;                               	//道具标识
		USHORT     	OP_item_type_Len;                      	//玩家名字的大小
		CHAR       	OP_item_type[MAX_OP_ITEM_TYPE_SIZE];     	//
		USHORT     	active_OP_Len;                         	//主动操作对象的大小
		CHAR       	active_OP[MAX_ACTIVE_OP_SIZE];           	//
		USHORT     	active_OP_type_Len;                    	//主动操作对象的大小
		CHAR       	active_OP_type[MAX_ACTIVE_OP_TYPE_SIZE]; 	//
		USHORT     	passive_OP_Len;                        	//被动操作对象的大小
		CHAR       	passive_OP[MAX_ACTIVE_OP_SIZE];          	//
		USHORT     	passive_OP_type_Len;                   	//被动操作对象的大小
		CHAR       	passive_OP_type[MAX_ACTIVE_OP_TYPE_SIZE];	//
		UINT       	mapid;                                 	//地图编号
		UINT       	active_OP_position;                    	//主动操作对象位置
		UINT       	passive_OP_position;                   	//被动操作对象位置
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};

	struct log_track_player_gold
	{
		static const USHORT	wCmd = G_O_logtrack_player_gold;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		USHORT     	player_OP_type_Len;                    	//玩家操作对象类型的大小
		CHAR       	player_OP_type[MAX_PLAYER_OP_TYPE_SIZE]; 	//
		UINT       	money;                                 	//金钱
		UINT       	mapid;                                 	//地图编号
		UINT       	player_position;                       	//玩家位置
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};

	struct log_track_player_login
	{
		static const USHORT	wCmd = G_O_logtrack_player_login;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		USHORT     	online_timelong;                       	//在线时间长度
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};

	struct log_track_player_map_change
	{
		static const USHORT	wCmd = G_O_logtrack_player_map_change;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		UINT       	leave_map;                             	//离开的地图
		UINT       	Enter_map;                             	//离开的地图
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	};

	struct log_track_team_change
	{
		static const USHORT	wCmd = G_O_logtrack_player_team_change;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		UINT       	team_id;                               	//玩家id
		USHORT     	team_name_Len;                         	//玩家名字的大小
		CHAR       	team_name[MAX_RNAME_SIZE];               	//
		USHORT     	team_event_Len;                        	//玩家名字的大小
		CHAR       	team_event[MAX_RNAME_SIZE];              	//
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};

	struct log_track_item_complex
	{
		static const USHORT	wCmd = G_O_logtrack_item_complex;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		USHORT     	item_complex_Len;                      	//玩家名字的大小
		CHAR       	item_complex[MAX_RMSSAGE_SIZE];          	//
		USHORT     	material_Len;                          	//玩家名字的大小
		CHAR       	material[MAX_RMSSAGE_SIZE];              	//
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};

	struct log_track_allot_skill_point
	{
		static const USHORT	wCmd = G_O_logtrack_allot_skill_point;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		UINT       	nonce_level;                           	//当前等级
		UINT       	SkillID;                               	//技能ID
		UINT       	skill_value;                           	//技能值id
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};

	struct log_track_wash_skill_point
	{
		static const USHORT	wCmd = G_O_logtrack_wash_skill_point;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		UINT       	nonce_level;                           	//当前等级
		USHORT     	wash_type_Len;                         	//洗点的类型
		CHAR       	wash_type[MAX_RMSSAGE_SIZE];             	//
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
		USHORT     	remark_Len;                            	//备注的大小
		CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};

	struct log_track_player_growing
	{
		static const USHORT	wCmd = G_O_logtrack_player_growing;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		UINT       	nonce_level;                           	//当前等级
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	};

	struct log_track_player_task
	{
		static const USHORT	wCmd = G_O_logtrack_player_task;

		UINT       	player_accont;                         	//玩家帐号
		UINT       	player_id;                             	//玩家id
		USHORT     	player_name_Len;                       	//玩家名字的大小
		CHAR       	player_name[MAX_RNAME_SIZE];             	//
		UINT       	task_npc_id;                           	//任务npc的ID
		USHORT     	task_type_Len;                         	//任务类型
		CHAR       	task_type[MAX_RNAME_SIZE];               	//
		USHORT     	task_name_Len;                         	//任务名称
		CHAR       	task_name[MAX_RNAME_SIZE];               	//
		UINT       	task_id;                               	//任务的ID
		USHORT     	task_encouragement_Len;                	//任务名称
		CHAR       	task_encouragement[MAX_RNAME_SIZE];      	//
		USHORT     	task_move_out_Len;                     	//任务名称
		CHAR       	task_move_out[MAX_RNAME_SIZE];           	//
		USHORT     	happen_time_Len;                       	//发生时间
		CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	};


	//Messages
	typedef enum Message_id_type
	{
		GOplayer_log_trade           	=	G_O_LogTrade,                   	//player_log_trade
		GOplayer_log_shopping        	=	G_O_LogShopping,                	//player_log_shopping
		GOplayer_OP                  	=	G_O_logplayerOP,                	//log_player_OP
		GOGM_OP                      	=	G_O_logGMOP,                    	//log_GM_OP
		GOGM_publish                 	=	G_O_logGMpublish,               	//log_GM_publish
		GOGM_kickdown                	=	G_O_logGMkickdown,              	//log_GM_kick_down
		GOlog_track_item             	=	G_O_logtrack_item,              	//log_track_item
		GOlog_track_player_gold      	=	G_O_logtrack_player_gold,       	//log_track_player_gold
		GOlogtrack_player_login      	=	G_O_logtrack_player_login,      	//log_track_player_login
		GOlogtrack_player_map_change 	=	G_O_logtrack_player_map_change, 	//log_track_player_map_change
		GOlogtrack_player_team_change	=	G_O_logtrack_player_team_change,	//log_track_team_change
		GOlogtrack_item_complex      	=	G_O_logtrack_item_complex,      	//log_track_item_complex
		GOlogtrack_allot_skill_point 	=	G_O_logtrack_allot_skill_point, 	//log_track_allot_skill_point
		GOlogtrack_wash_skill_point  	=	G_O_logtrack_wash_skill_point,  	//log_track_wash_skill_point
		GOlogtrack_player_growing    	=	G_O_logtrack_player_growing,    	//log_track_player_growing
		GOlogtrack_player_task       	=	G_O_logtrack_player_task,       	//log_track_player_task
	};

	//Class Data
	class LogServiceProtocol;
	typedef size_t (LogServiceProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (LogServiceProtocol::*DeCodeFunc)(void* pData);

	class 
	#ifdef WIN32
	//_declspec(dllexport) 
	#endif
	LogServiceProtocol
	{
	public:
		LogServiceProtocol();
		~LogServiceProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__player(void* pData);
		size_t	DeCode__player(void* pData);

		size_t	EnCode__trade_goods(void* pData);
		size_t	DeCode__trade_goods(void* pData);

		size_t	EnCode__player_log_trade(void* pData);
		size_t	DeCode__player_log_trade(void* pData);

		size_t	EnCode__player_log_shopping(void* pData);
		size_t	DeCode__player_log_shopping(void* pData);

		size_t	EnCode__log_player_OP(void* pData);
		size_t	DeCode__log_player_OP(void* pData);

		size_t	EnCode__log_GM_OP(void* pData);
		size_t	DeCode__log_GM_OP(void* pData);

		size_t	EnCode__log_GM_publish(void* pData);
		size_t	DeCode__log_GM_publish(void* pData);

		size_t	EnCode__log_GM_kick_down(void* pData);
		size_t	DeCode__log_GM_kick_down(void* pData);

		size_t	EnCode__log_track_item(void* pData);
		size_t	DeCode__log_track_item(void* pData);

		size_t	EnCode__log_track_player_gold(void* pData);
		size_t	DeCode__log_track_player_gold(void* pData);

		size_t	EnCode__log_track_player_login(void* pData);
		size_t	DeCode__log_track_player_login(void* pData);

		size_t	EnCode__log_track_player_map_change(void* pData);
		size_t	DeCode__log_track_player_map_change(void* pData);

		size_t	EnCode__log_track_team_change(void* pData);
		size_t	DeCode__log_track_team_change(void* pData);

		size_t	EnCode__log_track_item_complex(void* pData);
		size_t	DeCode__log_track_item_complex(void* pData);

		size_t	EnCode__log_track_allot_skill_point(void* pData);
		size_t	DeCode__log_track_allot_skill_point(void* pData);

		size_t	EnCode__log_track_wash_skill_point(void* pData);
		size_t	DeCode__log_track_wash_skill_point(void* pData);

		size_t	EnCode__log_track_player_growing(void* pData);
		size_t	DeCode__log_track_player_growing(void* pData);

		size_t	EnCode__log_track_player_task(void* pData);
		size_t	DeCode__log_track_player_task(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool                    m_kPackage;
	};
}
#endif			//__LogServiceProtocol__
