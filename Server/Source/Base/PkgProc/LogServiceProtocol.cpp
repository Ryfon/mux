﻿#include "LogServiceProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

LogServiceNS::LogServiceProtocol::LogServiceProtocol()
{
	m_mapEnCodeFunc[G_O_LogTrade                   ]	=	&LogServiceProtocol::EnCode__player_log_trade;
	m_mapDeCodeFunc[G_O_LogTrade                   ]	=	&LogServiceProtocol::DeCode__player_log_trade;

	m_mapEnCodeFunc[G_O_LogShopping                ]	=	&LogServiceProtocol::EnCode__player_log_shopping;
	m_mapDeCodeFunc[G_O_LogShopping                ]	=	&LogServiceProtocol::DeCode__player_log_shopping;

	m_mapEnCodeFunc[G_O_logplayerOP                ]	=	&LogServiceProtocol::EnCode__log_player_OP;
	m_mapDeCodeFunc[G_O_logplayerOP                ]	=	&LogServiceProtocol::DeCode__log_player_OP;

	m_mapEnCodeFunc[G_O_logGMOP                    ]	=	&LogServiceProtocol::EnCode__log_GM_OP;
	m_mapDeCodeFunc[G_O_logGMOP                    ]	=	&LogServiceProtocol::DeCode__log_GM_OP;

	m_mapEnCodeFunc[G_O_logGMpublish               ]	=	&LogServiceProtocol::EnCode__log_GM_publish;
	m_mapDeCodeFunc[G_O_logGMpublish               ]	=	&LogServiceProtocol::DeCode__log_GM_publish;

	m_mapEnCodeFunc[G_O_logGMkickdown              ]	=	&LogServiceProtocol::EnCode__log_GM_kick_down;
	m_mapDeCodeFunc[G_O_logGMkickdown              ]	=	&LogServiceProtocol::DeCode__log_GM_kick_down;

	m_mapEnCodeFunc[G_O_logtrack_item              ]	=	&LogServiceProtocol::EnCode__log_track_item;
	m_mapDeCodeFunc[G_O_logtrack_item              ]	=	&LogServiceProtocol::DeCode__log_track_item;

	m_mapEnCodeFunc[G_O_logtrack_player_gold       ]	=	&LogServiceProtocol::EnCode__log_track_player_gold;
	m_mapDeCodeFunc[G_O_logtrack_player_gold       ]	=	&LogServiceProtocol::DeCode__log_track_player_gold;

	m_mapEnCodeFunc[G_O_logtrack_player_login      ]	=	&LogServiceProtocol::EnCode__log_track_player_login;
	m_mapDeCodeFunc[G_O_logtrack_player_login      ]	=	&LogServiceProtocol::DeCode__log_track_player_login;

	m_mapEnCodeFunc[G_O_logtrack_player_map_change ]	=	&LogServiceProtocol::EnCode__log_track_player_map_change;
	m_mapDeCodeFunc[G_O_logtrack_player_map_change ]	=	&LogServiceProtocol::DeCode__log_track_player_map_change;

	m_mapEnCodeFunc[G_O_logtrack_player_team_change]	=	&LogServiceProtocol::EnCode__log_track_team_change;
	m_mapDeCodeFunc[G_O_logtrack_player_team_change]	=	&LogServiceProtocol::DeCode__log_track_team_change;

	m_mapEnCodeFunc[G_O_logtrack_item_complex      ]	=	&LogServiceProtocol::EnCode__log_track_item_complex;
	m_mapDeCodeFunc[G_O_logtrack_item_complex      ]	=	&LogServiceProtocol::DeCode__log_track_item_complex;

	m_mapEnCodeFunc[G_O_logtrack_allot_skill_point ]	=	&LogServiceProtocol::EnCode__log_track_allot_skill_point;
	m_mapDeCodeFunc[G_O_logtrack_allot_skill_point ]	=	&LogServiceProtocol::DeCode__log_track_allot_skill_point;

	m_mapEnCodeFunc[G_O_logtrack_wash_skill_point  ]	=	&LogServiceProtocol::EnCode__log_track_wash_skill_point;
	m_mapDeCodeFunc[G_O_logtrack_wash_skill_point  ]	=	&LogServiceProtocol::DeCode__log_track_wash_skill_point;

	m_mapEnCodeFunc[G_O_logtrack_player_growing    ]	=	&LogServiceProtocol::EnCode__log_track_player_growing;
	m_mapDeCodeFunc[G_O_logtrack_player_growing    ]	=	&LogServiceProtocol::DeCode__log_track_player_growing;

	m_mapEnCodeFunc[G_O_logtrack_player_task       ]	=	&LogServiceProtocol::EnCode__log_track_player_task;
	m_mapDeCodeFunc[G_O_logtrack_player_task       ]	=	&LogServiceProtocol::DeCode__log_track_player_task;

}

LogServiceNS::LogServiceProtocol::~LogServiceProtocol()
{
}

size_t	LogServiceNS::LogServiceProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	LogServiceNS::LogServiceProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

LogServiceNS::EnCodeFunc	LogServiceNS::LogServiceProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

LogServiceNS::DeCodeFunc	LogServiceNS::LogServiceProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__player(void* pData)
{
	player* pkplayer = (player*)(pData);

	//EnCode gate_srv_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer->gate_srv_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_pointer
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer->player_pointer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__player(void* pData)
{
	player* pkplayer = (player*)(pData);

	//DeCode gate_srv_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer->gate_srv_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_pointer
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer->player_pointer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__trade_goods(void* pData)
{
	trade_goods* pktrade_goods = (trade_goods*)(pData);

	//EnCode goods_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pktrade_goods->goods_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pktrade_goods->sum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__trade_goods(void* pData)
{
	trade_goods* pktrade_goods = (trade_goods*)(pData);

	//DeCode goods_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pktrade_goods->goods_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pktrade_goods->sum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__player_log_trade(void* pData)
{
	player_log_trade* pkplayer_log_trade = (player_log_trade*)(pData);

	//EnCode buyer_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->buyer_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buyer_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->buyer_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buyer_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_trade->buyer_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buyer_name
	if(MAX_RNAME_SIZE < pkplayer_log_trade->buyer_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_trade->buyer_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pkplayer_log_trade->buyer_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buyer_goods_sum
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_trade->buyer_goods_sum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buyer_goods
	if(MAX_TRADE_SIZE < pkplayer_log_trade->buyer_goods_sum)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkplayer_log_trade->buyer_goods_sum; ++i)
	{
		if(EnCode__trade_goods(&(pkplayer_log_trade->buyer_goods[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode buyer_money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->buyer_money), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seller_accont
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->seller_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seller_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->seller_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seller_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_trade->seller_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seller_name
	if(MAX_RNAME_SIZE < pkplayer_log_trade->seller_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_trade->seller_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pkplayer_log_trade->seller_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seller_goods_sum
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_trade->seller_goods_sum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seller_goods
	if(MAX_TRADE_SIZE < pkplayer_log_trade->seller_goods_sum)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkplayer_log_trade->seller_goods_sum; ++i)
	{
		if(EnCode__trade_goods(&(pkplayer_log_trade->seller_goods[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode seller_money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->seller_money), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buyer_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->buyer_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seller_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_trade->seller_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode hand_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_trade->hand_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode hand_time
	if(MAX_HAND_TIME_SIZE < pkplayer_log_trade->hand_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_trade->hand_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pkplayer_log_trade->hand_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_trade->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pkplayer_log_trade->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_trade->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pkplayer_log_trade->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__player_log_trade(void* pData)
{
	player_log_trade* pkplayer_log_trade = (player_log_trade*)(pData);

	//DeCode buyer_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->buyer_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buyer_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->buyer_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buyer_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_trade->buyer_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buyer_name
	if(MAX_RNAME_SIZE < pkplayer_log_trade->buyer_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_trade->buyer_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pkplayer_log_trade->buyer_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buyer_goods_sum
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_trade->buyer_goods_sum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buyer_goods
	if(MAX_TRADE_SIZE < pkplayer_log_trade->buyer_goods_sum)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkplayer_log_trade->buyer_goods_sum; ++i)
	{
		if(DeCode__trade_goods(&(pkplayer_log_trade->buyer_goods[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode buyer_money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->buyer_money), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seller_accont
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->seller_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seller_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->seller_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seller_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_trade->seller_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seller_name
	if(MAX_RNAME_SIZE < pkplayer_log_trade->seller_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_trade->seller_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pkplayer_log_trade->seller_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seller_goods_sum
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_trade->seller_goods_sum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seller_goods
	if(MAX_TRADE_SIZE < pkplayer_log_trade->seller_goods_sum)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkplayer_log_trade->seller_goods_sum; ++i)
	{
		if(DeCode__trade_goods(&(pkplayer_log_trade->seller_goods[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode seller_money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->seller_money), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buyer_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->buyer_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seller_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_trade->seller_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode hand_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_trade->hand_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode hand_time
	if(MAX_HAND_TIME_SIZE < pkplayer_log_trade->hand_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_trade->hand_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pkplayer_log_trade->hand_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_trade->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pkplayer_log_trade->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_trade->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pkplayer_log_trade->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__player_log_shopping(void* pData)
{
	player_log_shopping* pkplayer_log_shopping = (player_log_shopping*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_shopping->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_shopping->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_shopping->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pkplayer_log_shopping->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_shopping->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pkplayer_log_shopping->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode npc_shopping_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_shopping->npc_shopping_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode goods_lst_sum
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_shopping->goods_lst_sum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode goods_lst
	if(MAX_TRADE_SIZE < pkplayer_log_shopping->goods_lst_sum)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkplayer_log_shopping->goods_lst_sum; ++i)
	{
		if(EnCode__trade_goods(&(pkplayer_log_shopping->goods_lst[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_shopping->money), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode shopping_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_shopping->shopping_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode shopping_type
	if(MAX_SHOPPING_TYPE_SIZE < pkplayer_log_shopping->shopping_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_shopping->shopping_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pkplayer_log_shopping->shopping_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_shopping->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkplayer_log_shopping->player_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkplayer_log_shopping->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pkplayer_log_shopping->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_shopping->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pkplayer_log_shopping->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__player_log_shopping(void* pData)
{
	player_log_shopping* pkplayer_log_shopping = (player_log_shopping*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_shopping->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_shopping->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_shopping->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pkplayer_log_shopping->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_shopping->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pkplayer_log_shopping->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode npc_shopping_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_shopping->npc_shopping_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode goods_lst_sum
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_shopping->goods_lst_sum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode goods_lst
	if(MAX_TRADE_SIZE < pkplayer_log_shopping->goods_lst_sum)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkplayer_log_shopping->goods_lst_sum; ++i)
	{
		if(DeCode__trade_goods(&(pkplayer_log_shopping->goods_lst[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_shopping->money), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode shopping_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_shopping->shopping_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode shopping_type
	if(MAX_SHOPPING_TYPE_SIZE < pkplayer_log_shopping->shopping_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_shopping->shopping_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pkplayer_log_shopping->shopping_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_shopping->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkplayer_log_shopping->player_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkplayer_log_shopping->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pkplayer_log_shopping->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkplayer_log_shopping->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pkplayer_log_shopping->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_player_OP(void* pData)
{
	log_player_OP* pklog_player_OP = (log_player_OP*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_player_OP->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_player_OP->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_player_OP->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_player_OP->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_player_OP->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_player_OP->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_OP_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_player_OP->player_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_OP_type
	if(MAX_PLAYER_OP_TYPE_SIZE < pklog_player_OP->player_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_player_OP->player_OP_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_player_OP->player_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_player_OP->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_player_OP->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_player_OP->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_player_OP->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_player_OP(void* pData)
{
	log_player_OP* pklog_player_OP = (log_player_OP*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_player_OP->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_player_OP->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_player_OP->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_player_OP->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_player_OP->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_player_OP->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_OP_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_player_OP->player_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_OP_type
	if(MAX_PLAYER_OP_TYPE_SIZE < pklog_player_OP->player_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_player_OP->player_OP_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_player_OP->player_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_player_OP->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_player_OP->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_player_OP->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_player_OP->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_GM_OP(void* pData)
{
	log_GM_OP* pklog_GM_OP = (log_GM_OP*)(pData);

	//EnCode GM_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_OP->GM_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_accont
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_OP->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_OP->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_OP->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_GM_OP->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_OP->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_OP->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GM_OP_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_OP->GM_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GM_OP_type
	if(MAX_GMOP_TYPE_SIZE < pklog_GM_OP->GM_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_OP->GM_OP_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_OP->GM_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode time_long
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_OP->time_long), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_OP->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GM_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_OP->GM_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_OP->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_GM_OP->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_OP->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_OP->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_OP->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_GM_OP->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_OP->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_OP->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_GM_OP(void* pData)
{
	log_GM_OP* pklog_GM_OP = (log_GM_OP*)(pData);

	//DeCode GM_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_OP->GM_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_accont
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_OP->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_OP->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_OP->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_GM_OP->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_OP->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_OP->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GM_OP_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_OP->GM_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GM_OP_type
	if(MAX_GMOP_TYPE_SIZE < pklog_GM_OP->GM_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_OP->GM_OP_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_OP->GM_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode time_long
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_OP->time_long), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_OP->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GM_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_OP->GM_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_OP->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_GM_OP->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_OP->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_OP->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_OP->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_GM_OP->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_OP->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_OP->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_GM_publish(void* pData)
{
	log_GM_publish* pklog_GM_publish = (log_GM_publish*)(pData);

	//EnCode GM_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_publish->GM_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode channel_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_publish->channel_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode channel
	if(MAX_GM_CHANNEL_TYPE_SIZE < pklog_GM_publish->channel_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_publish->channel_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_publish->channel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode publish_content_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_publish->publish_content_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode publish_content
	if(MAX_RMSSAGE_SIZE < pklog_GM_publish->publish_content_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_publish->publish_content_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_publish->publish_content), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_publish->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GM_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_publish->GM_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_publish->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_GM_publish->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_publish->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_publish->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_GM_publish(void* pData)
{
	log_GM_publish* pklog_GM_publish = (log_GM_publish*)(pData);

	//DeCode GM_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_publish->GM_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode channel_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_publish->channel_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode channel
	if(MAX_GM_CHANNEL_TYPE_SIZE < pklog_GM_publish->channel_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_publish->channel_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_publish->channel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode publish_content_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_publish->publish_content_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode publish_content
	if(MAX_RMSSAGE_SIZE < pklog_GM_publish->publish_content_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_publish->publish_content_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_publish->publish_content), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_publish->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GM_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_publish->GM_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_publish->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_GM_publish->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_publish->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_publish->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_GM_kick_down(void* pData)
{
	log_GM_kick_down* pklog_GM_kick_down = (log_GM_kick_down*)(pData);

	//EnCode GM_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_kick_down->GM_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_accont
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_kick_down->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_kick_down->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_kick_down->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_GM_kick_down->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_kick_down->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_kick_down->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_kick_down->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_GM_kick_down->player_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_kick_down->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_GM_kick_down->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_kick_down->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_kick_down->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_GM_kick_down->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_GM_kick_down->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_kick_down->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_GM_kick_down->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_GM_kick_down(void* pData)
{
	log_GM_kick_down* pklog_GM_kick_down = (log_GM_kick_down*)(pData);

	//DeCode GM_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_kick_down->GM_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_accont
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_kick_down->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_kick_down->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_kick_down->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_GM_kick_down->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_kick_down->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_kick_down->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_kick_down->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_GM_kick_down->player_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_kick_down->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_GM_kick_down->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_kick_down->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_kick_down->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_GM_kick_down->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_GM_kick_down->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_GM_kick_down->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_GM_kick_down->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_item(void* pData)
{
	log_track_item* pklog_track_item = (log_track_item*)(pData);

	//EnCode item_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_item->item_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OP_item_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item->OP_item_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OP_item_type
	if(MAX_OP_ITEM_TYPE_SIZE < pklog_track_item->OP_item_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->OP_item_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item->OP_item_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active_OP_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item->active_OP_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active_OP
	if(MAX_ACTIVE_OP_SIZE < pklog_track_item->active_OP_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->active_OP_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item->active_OP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active_OP_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item->active_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active_OP_type
	if(MAX_ACTIVE_OP_TYPE_SIZE < pklog_track_item->active_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->active_OP_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item->active_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode passive_OP_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item->passive_OP_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode passive_OP
	if(MAX_ACTIVE_OP_SIZE < pklog_track_item->passive_OP_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->passive_OP_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item->passive_OP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode passive_OP_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item->passive_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode passive_OP_type
	if(MAX_ACTIVE_OP_TYPE_SIZE < pklog_track_item->passive_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->passive_OP_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item->passive_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_item->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active_OP_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_item->active_OP_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode passive_OP_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_item->passive_OP_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_item->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_item->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_item(void* pData)
{
	log_track_item* pklog_track_item = (log_track_item*)(pData);

	//DeCode item_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_item->item_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OP_item_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item->OP_item_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OP_item_type
	if(MAX_OP_ITEM_TYPE_SIZE < pklog_track_item->OP_item_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->OP_item_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item->OP_item_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active_OP_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item->active_OP_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active_OP
	if(MAX_ACTIVE_OP_SIZE < pklog_track_item->active_OP_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->active_OP_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item->active_OP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active_OP_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item->active_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active_OP_type
	if(MAX_ACTIVE_OP_TYPE_SIZE < pklog_track_item->active_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->active_OP_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item->active_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode passive_OP_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item->passive_OP_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode passive_OP
	if(MAX_ACTIVE_OP_SIZE < pklog_track_item->passive_OP_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->passive_OP_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item->passive_OP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode passive_OP_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item->passive_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode passive_OP_type
	if(MAX_ACTIVE_OP_TYPE_SIZE < pklog_track_item->passive_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->passive_OP_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item->passive_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_item->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active_OP_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_item->active_OP_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode passive_OP_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_item->passive_OP_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_item->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_item->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_player_gold(void* pData)
{
	log_track_player_gold* pklog_track_player_gold = (log_track_player_gold*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_gold->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_gold->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_gold->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_gold->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_gold->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_gold->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_OP_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_gold->player_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_OP_type
	if(MAX_PLAYER_OP_TYPE_SIZE < pklog_track_player_gold->player_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_gold->player_OP_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_gold->player_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_gold->money), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_gold->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_position
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_gold->player_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_gold->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_gold->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_gold->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_gold->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_gold->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_player_gold->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_gold->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_gold->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_player_gold(void* pData)
{
	log_track_player_gold* pklog_track_player_gold = (log_track_player_gold*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_gold->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_gold->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_gold->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_gold->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_gold->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_gold->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_OP_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_gold->player_OP_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_OP_type
	if(MAX_PLAYER_OP_TYPE_SIZE < pklog_track_player_gold->player_OP_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_gold->player_OP_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_gold->player_OP_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_gold->money), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_gold->mapid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_position
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_gold->player_position), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_gold->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_gold->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_gold->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_gold->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_gold->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_player_gold->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_gold->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_gold->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_player_login(void* pData)
{
	log_track_player_login* pklog_track_player_login = (log_track_player_login*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_login->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_login->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_login->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_login->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_login->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_login->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode online_timelong
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_login->online_timelong), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_login->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_login->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_login->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_login->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_login->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_player_login->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_login->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_login->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_player_login(void* pData)
{
	log_track_player_login* pklog_track_player_login = (log_track_player_login*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_login->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_login->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_login->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_login->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_login->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_login->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode online_timelong
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_login->online_timelong), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_login->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_login->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_login->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_login->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_login->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_player_login->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_login->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_login->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_player_map_change(void* pData)
{
	log_track_player_map_change* pklog_track_player_map_change = (log_track_player_map_change*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_map_change->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_map_change->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_map_change->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_map_change->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_map_change->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_map_change->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode leave_map
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_map_change->leave_map), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Enter_map
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_map_change->Enter_map), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_map_change->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_map_change->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_map_change->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_map_change->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_player_map_change(void* pData)
{
	log_track_player_map_change* pklog_track_player_map_change = (log_track_player_map_change*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_map_change->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_map_change->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_map_change->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_map_change->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_map_change->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_map_change->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode leave_map
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_map_change->leave_map), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Enter_map
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_map_change->Enter_map), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_map_change->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_map_change->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_map_change->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_map_change->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_team_change(void* pData)
{
	log_track_team_change* pklog_track_team_change = (log_track_team_change*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_team_change->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_team_change->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_team_change->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_team_change->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_team_change->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_team_change->team_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_team_change->team_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team_name
	if(MAX_RNAME_SIZE < pklog_track_team_change->team_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->team_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_team_change->team_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team_event_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_team_change->team_event_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team_event
	if(MAX_RNAME_SIZE < pklog_track_team_change->team_event_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->team_event_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_team_change->team_event), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_team_change->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_team_change->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_team_change->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_team_change->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_team_change->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_team_change->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_team_change(void* pData)
{
	log_track_team_change* pklog_track_team_change = (log_track_team_change*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_team_change->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_team_change->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_team_change->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_team_change->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_team_change->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_team_change->team_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_team_change->team_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team_name
	if(MAX_RNAME_SIZE < pklog_track_team_change->team_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->team_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_team_change->team_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team_event_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_team_change->team_event_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team_event
	if(MAX_RNAME_SIZE < pklog_track_team_change->team_event_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->team_event_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_team_change->team_event), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_team_change->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_team_change->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_team_change->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_team_change->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_team_change->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_team_change->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_team_change->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_item_complex(void* pData)
{
	log_track_item_complex* pklog_track_item_complex = (log_track_item_complex*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_item_complex->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_item_complex->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item_complex->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_item_complex->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item_complex->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode item_complex_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item_complex->item_complex_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode item_complex
	if(MAX_RMSSAGE_SIZE < pklog_track_item_complex->item_complex_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->item_complex_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item_complex->item_complex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode material_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item_complex->material_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode material
	if(MAX_RMSSAGE_SIZE < pklog_track_item_complex->material_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->material_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item_complex->material), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item_complex->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_item_complex->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item_complex->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_item_complex->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_item_complex->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_item_complex->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_item_complex(void* pData)
{
	log_track_item_complex* pklog_track_item_complex = (log_track_item_complex*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_item_complex->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_item_complex->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item_complex->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_item_complex->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item_complex->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode item_complex_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item_complex->item_complex_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode item_complex
	if(MAX_RMSSAGE_SIZE < pklog_track_item_complex->item_complex_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->item_complex_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item_complex->item_complex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode material_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item_complex->material_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode material
	if(MAX_RMSSAGE_SIZE < pklog_track_item_complex->material_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->material_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item_complex->material), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item_complex->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_item_complex->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item_complex->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_item_complex->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_item_complex->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_item_complex->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_item_complex->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_allot_skill_point(void* pData)
{
	log_track_allot_skill_point* pklog_track_allot_skill_point = (log_track_allot_skill_point*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_allot_skill_point->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_allot_skill_point->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_allot_skill_point->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_allot_skill_point->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_allot_skill_point->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_allot_skill_point->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nonce_level
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_allot_skill_point->nonce_level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SkillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_allot_skill_point->SkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skill_value
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_allot_skill_point->skill_value), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_allot_skill_point->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_allot_skill_point->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_allot_skill_point->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_allot_skill_point->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_allot_skill_point->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_allot_skill_point->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_allot_skill_point->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_allot_skill_point->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_allot_skill_point(void* pData)
{
	log_track_allot_skill_point* pklog_track_allot_skill_point = (log_track_allot_skill_point*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_allot_skill_point->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_allot_skill_point->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_allot_skill_point->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_allot_skill_point->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_allot_skill_point->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_allot_skill_point->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nonce_level
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_allot_skill_point->nonce_level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SkillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_allot_skill_point->SkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skill_value
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_allot_skill_point->skill_value), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_allot_skill_point->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_allot_skill_point->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_allot_skill_point->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_allot_skill_point->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_allot_skill_point->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_allot_skill_point->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_allot_skill_point->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_allot_skill_point->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_wash_skill_point(void* pData)
{
	log_track_wash_skill_point* pklog_track_wash_skill_point = (log_track_wash_skill_point*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_wash_skill_point->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_wash_skill_point->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_wash_skill_point->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_wash_skill_point->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_wash_skill_point->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_wash_skill_point->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nonce_level
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_wash_skill_point->nonce_level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode wash_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_wash_skill_point->wash_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode wash_type
	if(MAX_RMSSAGE_SIZE < pklog_track_wash_skill_point->wash_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_wash_skill_point->wash_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_wash_skill_point->wash_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_wash_skill_point->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_wash_skill_point->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_wash_skill_point->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_wash_skill_point->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_wash_skill_point->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_wash_skill_point->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_wash_skill_point->remark_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_wash_skill_point->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_wash_skill_point(void* pData)
{
	log_track_wash_skill_point* pklog_track_wash_skill_point = (log_track_wash_skill_point*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_wash_skill_point->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_wash_skill_point->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_wash_skill_point->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_wash_skill_point->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_wash_skill_point->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_wash_skill_point->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nonce_level
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_wash_skill_point->nonce_level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode wash_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_wash_skill_point->wash_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode wash_type
	if(MAX_RMSSAGE_SIZE < pklog_track_wash_skill_point->wash_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_wash_skill_point->wash_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_wash_skill_point->wash_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_wash_skill_point->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_wash_skill_point->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_wash_skill_point->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_wash_skill_point->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_wash_skill_point->remark_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode remark
	if(MAX_RMSSAGE_SIZE < pklog_track_wash_skill_point->remark_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_wash_skill_point->remark_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_wash_skill_point->remark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_player_growing(void* pData)
{
	log_track_player_growing* pklog_track_player_growing = (log_track_player_growing*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_growing->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_growing->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_growing->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_growing->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_growing->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_growing->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nonce_level
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_growing->nonce_level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_growing->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_growing->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_growing->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_growing->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_player_growing(void* pData)
{
	log_track_player_growing* pklog_track_player_growing = (log_track_player_growing*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_growing->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_growing->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_growing->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_growing->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_growing->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_growing->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nonce_level
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_growing->nonce_level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_growing->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_growing->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_growing->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_growing->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::EnCode__log_track_player_task(void* pData)
{
	log_track_player_task* pklog_track_player_task = (log_track_player_task*)(pData);

	//EnCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_task->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_task->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_task->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_task->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->player_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_task->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_npc_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_task->task_npc_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_type_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_task->task_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_type
	if(MAX_RNAME_SIZE < pklog_track_player_task->task_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->task_type_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_task->task_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_name_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_task->task_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_name
	if(MAX_RNAME_SIZE < pklog_track_player_task->task_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->task_name_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_task->task_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_id
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pklog_track_player_task->task_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_encouragement_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_task->task_encouragement_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_encouragement
	if(MAX_RNAME_SIZE < pklog_track_player_task->task_encouragement_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->task_encouragement_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_task->task_encouragement), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_move_out_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_task->task_move_out_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task_move_out
	if(MAX_RNAME_SIZE < pklog_track_player_task->task_move_out_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->task_move_out_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_task->task_move_out), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pklog_track_player_task->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_task->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->happen_time_Len;
	if(!m_kPackage.Pack("CHAR", &(pklog_track_player_task->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LogServiceNS::LogServiceProtocol::DeCode__log_track_player_task(void* pData)
{
	log_track_player_task* pklog_track_player_task = (log_track_player_task*)(pData);

	//DeCode player_accont
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_task->player_accont), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_task->player_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_task->player_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode player_name
	if(MAX_RNAME_SIZE < pklog_track_player_task->player_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->player_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_task->player_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_npc_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_task->task_npc_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_type_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_task->task_type_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_type
	if(MAX_RNAME_SIZE < pklog_track_player_task->task_type_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->task_type_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_task->task_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_name_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_task->task_name_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_name
	if(MAX_RNAME_SIZE < pklog_track_player_task->task_name_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->task_name_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_task->task_name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_id
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pklog_track_player_task->task_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_encouragement_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_task->task_encouragement_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_encouragement
	if(MAX_RNAME_SIZE < pklog_track_player_task->task_encouragement_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->task_encouragement_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_task->task_encouragement), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_move_out_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_task->task_move_out_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task_move_out
	if(MAX_RNAME_SIZE < pklog_track_player_task->task_move_out_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->task_move_out_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_task->task_move_out), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time_Len
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pklog_track_player_task->happen_time_Len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode happen_time
	if(MAX_RNAME_SIZE < pklog_track_player_task->happen_time_Len)
	{
		return FAILEDRETCODE;
	}

	unCount = pklog_track_player_task->happen_time_Len;
	if(!m_kPackage.UnPack("CHAR", &(pklog_track_player_task->happen_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

