﻿/**
* @file SrvProtocol.h
* @brief 定义服务器端有关通信协议
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: SrvProtocol.h
* 摘    要: 定义服务器端有关通信协议（类型，命令字，协议结构）
* 作    者: dzj
* 完成日期: 2007.11.29
*
*/

#pragma once

#ifdef WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //WIN32

#include "CliProtocol.h" //包含与客户端有关的通信协议;
#include "RelationServiceProtocol.h"
#include "LoginServiceProtocol.h"
#include "ShopSvrProtocol.h"
#include "LogSvcProtocol.h"

#include "../Utility.h"

#include <vector>
using namespace std;
using namespace RelationServiceNS;
using namespace LoginServiceNS;
using namespace ShopServer;

#define SCOPE_BROADCAST

#define MAX_NOTIPLAYER_NUM_PERGATE 120 //群发消息，单gate单次最大通知数；

#define MSGTOPUT_SIZE 512

#define COPY_SCROLL_TYPE    225090001 //副本券ID号
#define BIG_BUGLE_TYPE      228010001 //大喇叭ID号
#define LITTLE_BUGLE_TYPE   228020001 //小喇叭ID号

class CPlayer;

namespace MUX_PROTO
{

#pragma pack(push, 1)

	/**
	*   第一部分：
	*   以下为参与通信各方的类别标识号，程序自身的标识号应在本文件之外的某处定义好
	*   例如：客户端程序应该这样定义，const int SELF_PROTO_TYPE = 0x00;
	*	参与者类别	类别编号	简写
	*	Client			0		C
	*	Gatesrv			1		G
	*	Assignsrv		2		A
	*	Centersrv		3		E	
	*	Loginsrv		4		L
	*	Mapsrv  		5		M
	*	Relation		6		R
	*	Dbsrv    		7		D
	*	Shopsrv  		8		S
	*	Runtools  		9		T
	*	Billingsystem	a		B
	*   NewLoginSrv     b       N
	*   LogServer	    c		O
	*   functionServer  d       F
	*/
	//#define C_A  	0x02	//客户端发往assignsrv的消息；
	//#define A_C  	0x20	//assign发往客户端的消息；
	//#define C_G  	0x01	//客户端发往gatesrv的消息；
	//#define G_C  	0x10	//gatesrv发往客户端的消息；
#define G_A  	0x12	//gatesrv发往assiginsrv的消息；
#define G_L  	0x14	//gatesrv发往loginsrv的消息；
#define L_G  	0x41	//loginsrv发往gatesrv的消息；
#define G_S  	0x18	//gatesrv发往shopsrv的消息；
#define S_G  	0x81	//shopsrv发往gatesrv的消息；
#define G_M  	0x15	//gatesrv发往mapsrv的消息；
#define M_G  	0x51	//mapsrv发往gatesrv的消息；
#define G_D  	0x17	//gatesrv发往dbsrv的消息；
#define D_G  	0x71	//dbsrv发往gatesrv的消息；
#define G_R		0x16	//gatesrv发往relationsrv的消息；
#define R_G		0x61	//relationsrv发往gatesrv的消息；
#define G_E		0x13	//gatesrv发往centersrv的消息；
#define E_G		0x31	//centersrv发往gatesrv的消息；
#define E_R		0x39	//centersrv发往runtools的消息；
#define R_E		0x93	//runtools发往centersrv的消息；
#define E_O		0x36	//centersrv发往societysrv的消息；
#define O_E		0x63	//societysrv发往societysrv的消息；
#define O_D		0x67	//societysrv发往dbsrv的消息；
#define D_O		0x76	//dbsrv发往societysrv的消息；
#define S_B		0x8a	//shopsrv发往billingsystem的消息；
#define B_S		0xa8	//billingsystem发往shopsrv的消息；
#define M_O     0x5c    //mapsrv --> logsrv 的消息
#define O_M     0xc5    //logsrv --> mapsrv 的消息
#define G_F     0x1d    //gatesrv--> functionsrv的消息
#define F_G     0xd1    //functionsrv--> gatesrv的消息

	/**
	*   第二部分：
	*   以下为通信协议命令字
	*   制订原则是按功能划分可用命令字段，以便能较容易地由命令字的值识别该命令的大致功能
	*   实际编码时只使用对应命令字的宏而不使用16进制数值
	*   这部分定义应按参与者类别的不同而分块，以便编码时能迅速查找到相应的命令字，
	*   同时命令字宏字母全为大定，命令字宏的头两个字母指出该消息的参与者，
	*   其余部分为该命令字意义的简写，参与者与消息意义之间以下划线分隔
	*/
	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往loginsrv的消息命令字,形式0xXXXX...
#define G_L_PLAYER_LOGIN 0x1400 //gatesrv发往loginsrv,玩家登陆验证
	///...gatesrv发往loginsrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///loginsrv发往gatesrv的消息命令字,形式0xXXXX...
#define L_G_PLAYER_LOGIN_RETURN 0x4100   //loginsrv发往gatesrv,返回玩家登陆验证结果
	///...loginsrv发往gatesrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往mapsrv的消息命令字,形式0xXXXX...

#define G_M_RUN  0x1504  //gatesrv发往mapsrv的跑动请求消息
#define G_M_PLAYER_APPEAR 0x1505 //gatesrv发往mapsrv玩家在地图上出现
#define G_M_PLAYER_LEAVE  0x1506 //gatesrv发往mapsrv玩家离开地图
#define G_M_PLAYER_BEYONDBLOCK 0x1507 //玩家移动中跨块，以GMRun消息为前提;
#define G_M_MANUAL_UPGRADE_REQ 0x1508 //手动升级
#define G_M_ALLOCATE_POINT_REQ 0x1509 //手动分配属性点
#define G_M_MOD_EFFECTIVE_SKILL_ID 0x150a //修改有效技能id

#define G_M_PLAYER_ROLE_INFO 0x1511 //gatesrv发往mapsrv玩家所选角色信息，mapsrv保存此信息用于逻辑处理或以后广播给玩家的周围人；
#define G_M_ATTACK_MONSTER   0x1512 //gatesrv发往mapsrv玩家攻击怪物信息；
#define G_M_REQ_ROLEINFO 0x1513 //gatesrv转发mapsrv请求某个未知角色信息；//该工作以后可以转由centersrv进行？
#define G_M_REQ_MONSTERINFO 0x1514 //gatesrv转发mapsrv请求某个未知Monster信息
//#define G_M_PLAYER_PKG       0x1515 //玩家角色包裹信息；
#define G_M_CHAT             0x1516 //聊天信息；
#define G_M_REQ_MANED_MAPS   0x1517 //请求MapSrv管理的地图编号列表；

#define G_M_ATTACK_PLAYER    0x1518 //攻击玩家；

#define G_M_SWITCH_MAP 0x1519 //地图跳转

#define G_M_SCRIPT_CHAT 0x151a //玩家点击NPC与其对话（包括玩家点击NPC以要求开始对话以及点击当前对话中的某选项）

#define G_M_FULL_PLAYER_INFO 0x151b //玩家完整信息

//消息大致顺序,G_C_PLAYER_DYING-->C_G_DEFAULT_REBIRTH-->G_C_REBIRTH_INFO-->C_G_REBIRTH_READY-->G_C_PLAYER_APPEAR
#define G_M_DEFAULT_REBIRTH 0x151c //玩家死亡倒计时时间到,发此消息通知SRV，默认复活至最近的复活点，并进行扣经验等操作；

//消息大致顺序,G_C_PLAYER_DYING-->C_G_DEFAULT_REBIRTH-->G_C_REBIRTH_INFO-->C_G_REBIRTH_READY-->G_C_PLAYER_APPEAR
#define G_M_REBIRTH_READY 0x151d //玩家重生准备好,发此消息通知SRV，SRV收到此消息后，将玩家Appear消息广播给复活点周围玩家；

	///道具系统 从MapSrv 发往 GateSrv 

#define  G_M_QUEARY_ITEMPACK 0x151e //查询道具包信息
#define  G_M_PICKITEM  0x151f //检物品
#define  G_M_PICKALLITEM 0x1520 //检包里所有物品
#define  G_M_BATCH_ITEM_SEQUENCE 0x1521//获取批量物品流水号
#define  G_M_USEITEM 0x1522 //客户端装备道具
#define  G_M_ITEM_INFO 0x1523 //道具信息
#define  G_M_UPDATE_ITEM_INFO 0x1524//更新道具信息
#define  G_M_UNUSEITEM 0x1525 //卸载装备
#define  G_M_SWAPITEM  0x1526 //交换道具包中道具
#define  G_M_DROPITEM  0x1527 //玩家丢弃道具
#define  G_M_SPLITITEM 0x1528 //玩家拆分物品
#define  G_M_EQUIPITEM 0x1529 //玩家使用可装备道具时发送的消息

#define  G_M_ADDSTH_TO_PLATE    0x152a //玩家往托盘中加物品
#define  G_M_TAKESTH_FROM_PLATE 0x152b //玩家从托盘中取物品
#define  G_M_PLATE_EXEC         0x152c //玩家尝试执行托盘操作
#define  G_M_CLOSE_PLATE        0x152d //玩家关闭托盘

#define  G_M_ADD_APT_REQ        0x152e //添加活点需求；

////////////////////////// 骑乘协议 /////////////////////////////////////////
#define  G_M_ACTIVEMOUNT 0x1530 ////玩家上马
#define  G_M_INVITEMOUNT 0x1531 ///玩家邀请别人上马
#define  G_M_AGREE_BEINVITE 0x1532//玩家是否统一被邀请
#define  G_M_LEAVE_MOUNTTEAM 0x1533 //玩家立即队伍
#define  G_M_NEWMAP_SETRIDESTATE 0x1534//新地图设置玩家的骑乘状态
#define  G_M_RIRDELEADER_SWITCHMAP 0x1535//当骑乘队长切换地图时

///////////////////////////关于BUFF//////////////
#define G_M_REQ_BUFFEND	 0x1540	 //向MAPSRV 发送请求
#define G_M_USE_ASSISTSKILL	0x1541	//使用辅助技能

//////////////////////////关于组队/////////////////////////

#define G_M_REQ_JOIN_TEAM 0x1550	//向玩家所在的mapsrv发送组队消息
#define G_M_TEAM_CREATE   0x1551 //创建组队信息
#define G_M_TEAM_DETAILINFO 0x1552  //返回整个队伍的详细信息
#define G_M_TEAM_EXIT 0x1553    //玩家退出组队
#define G_M_TEAM_ENTRANCE 0x1554 //玩家进入组队
#define G_M_UPDATE_PLAYERTEAMINFO 0x1555 //更新玩家在队伍中的信息
#define G_M_AGREE_ROLLITEM 0x1556 //同意是否Roll点
#define G_M_CHANGETEAM_LEADER 0x1557 //修改组队的队长
#define G_M_TEAM_KICKPLAYER  0x1558//踢出队员
#define G_M_TEAM_DISBAND 0x1559//队伍解散
#define G_M_CHANGETEAM_ITEMMODE  0x155a//改变组队的道具分配模式
#define G_M_CHANGETEAM_EXPREMODE 0x155b//改变组队的经验分享模式
#define G_M_CHANGETEAM_ROLLLEVEL 0x155c//改变组队的Roll物品等级
#define G_M_TEAMMEMBER_GETNEWITEM  0x155d//队伍中的玩家拾取了道具

/////////////////////////关于物品交易///////////////////////////
#define G_M_SEND_TRADE_REQ			0x1560 //发送交易请求
#define G_M_SEND_TRADE_REQ_RESULT	0x1561 //发送交易请求的结果
#define G_M_ADD_OR_DELETE_ITEM		0x1562 //交易中增加或删除物品
#define G_M_CANCEL_TRADE			0x1563 //取消交易
#define G_M_LOCK_TRADE				0x1564 //锁定交易
#define G_M_CONFIRM_TRADE			0x1565 //确认交易

////////////////////////关于仇人///////////////////////////////////
#define G_M_FOEID_LIST   0x1566   //发送在线仇人的在线列表
#define G_M_DEL_FOEID    0x1567   //删除一个仇人ID
#define G_M_ADD_FOEID    0x1568   //增加一个仇人ID

////////////////////////关于耐久度////////////////////////////////
#define G_M_REPAIR_WEAR_BY_ITEM   0x1569//修复道具的耐久度（使用修复性道具）
#define G_M_ADD_SPSKILL_REQ		0x156a

#define G_M_SET_TRADE_MONEY     0x156b //交易金钱

#define G_M_TIMED_WEAR_IS_ZERO  0x156c
#define G_M_USE_DAMAGE_ITEM		0x156d
#define G_M_UNION_MULTI_NTF		0x156e//n倍活跃度或威望

////////////////////////关于技能成长////////////////////////////////
#define G_M_UPGRADE_SKILL 0x1570	//技能升级的请求

////////////////////////关于NPC商店////////////////////////////////
/*
	1、show shop && 第一页物品数量
	2、CG请求id号shop第n页物品各自数量
	3、GC返回id号shop第n页物品各自数量
	4、CG请求购买id号shop第n页第n个物品，以及该物品的ID号与数量，货币类型与数量；
	5、GC返回玩家购买id号shop第n页第n个物品，以及该物品的ID号与数量，货币类型与数量，购买成功或失败；
	6、CG玩家请求售出id类型物品，售出物品在包裹中的位置，售出物品数量，售出价格；
	7、GC返回玩家售出id类型物品结果，售出物品在包裹中的位置，售出物品数量，售出价格，售出后玩家的总金钱数，售出成功或失败；
*/
#define G_M_QUERY_SHOP    0x1576 //查询商店物品数量
#define G_M_SHOP_BUY_REQ  0x1577 //购买商店物品请求
#define G_M_ITEM_SOLD_REQ 0x1578 //玩家请求售出物品


/////////////////////// 关于任务////////////////////////////////////
#define G_M_DELETE_TASK  0x1580//删除任务
#define G_M_SHARE_TASK   0x1581//分享任务

#define G_M_REPAIR_WEAR_BY_NPC 0x1582 //npc维修耐久度
#define G_M_CHANGE_BATTLEMODE 0x1583	//修改战斗模式
#define G_M_QUERY_EVILTIME 0x1584 //查询恶魔在线时间
#define G_M_QUERY_REPAIRITEM_COST 0x1585 //查询维修道具所花费的金钱 

#define G_M_SWAP_EQUIPITEM 0x1586 //交换装备栏道具
#define G_M_QUERY_PKGITEMPAGE  0x1587 //请求查询包裹道具
#define G_M_AOE_ATTACK 0x1588  //地面AOE技能

//天谴相关
#define G_M_PUNISHMENT_START 0x1589			//通知mapsrv天谴开始
#define G_M_PUNISHMENT_END	0x158a			//通知mapsrv天谴结束
#define G_M_PUNISH_PLAYER_INFO 0x158b		//更新天谴玩家的信息

//GM工具相关
#define G_M_GMCMD_REQUEST 0x158c	//通知mapsrv GM命令

//宠物系统初步
#define G_M_SUMMON_PET				0x158d			 //请求召唤宠物	
#define G_M_CLOSE_PET				0x158e			 //唤回宠物
#define G_M_CHANGE_PET_NAME			0x158f			 //请求更换宠物姓名
#define G_M_CHANGE_PET_SKILL_STATE	0x1590			 //新的怪物的技能设置状态
#define G_M_CHANGE_PET_IMAGE		0x1591			 //更改宠物的外形

//排行相关
#define G_M_RANKCHART_INFO_UPDATE 0x1592 //通知mapSrv更新排行榜信息
#define G_M_RANKCHART_REFRESH     0x1593 //通知mapsrv刷新排行榜信息
#define G_M_UPDATE_RANKPLAYER_RANKINFO 0x1594 //通知更新对应mapsrv上的排行榜的玩家
#define G_M_REMOVE_CHART_RANKPLAYER   0x1595 //通知删除排行榜上的玩家


//排队系统
#define G_M_QUERY_MAP_QUEUE   0x1596	//GateSrv查询该mapsrv的排队队列
#define G_M_PLAYER_QUIT_QUEUE  0x1597  //玩家退出或者取消排队

//工会系统
#define G_M_REGISTER_UNION_MEMBER 0x1598	//注册工会成员
#define G_M_REPORT_UNION_SKILLS 0x1599  // 通告工会技能
#define G_M_UNREISTER_UNION_MEMBER 0x159a  // 取消注册工会成员
#define G_M_ISSUE_UNION_TASKS 0x159b		//发布任务成功
#define G_M_CHANGE_UNION_ACTIVE 0x159c //通知工会活跃度改变


#define G_M_PLAYER_DELETE_ROLE      0x159d      //通知删除排行榜中的玩家
#define G_M_NOTIFY_WAR_TASK_STATE	0x159e		//通知mapsrv找到该taskid的玩家并且获得奖励
#define G_M_ATTACK_WAR_END			0x159f		//通知所有mapsrv该攻城战结束


//使用UseSkill2后已废弃，原ID号改由GMMouseTipReq使用，#define G_M_USE_PARTITION_SKILL 0x15a0		//使用动作分割技能
#define G_M_MOUSETIP_REQ 0x15a0		//鼠标停留信息查询(HP|MP)

#define G_M_CHECK_PKG_FREE_SPACE 0x15a1     //检查包裹剩余空间
#define G_M_FETCH_VIP_ITEM    0x15a2		//领取vip物品
#define G_M_PET_MOVE			  0x15a3	  //宠物移动协议
#define G_M_FETCH_NON_VIP_ITEM   0x15a4     //领取非vip物品

#define G_M_PICKITEM_FROM_MAIL 0x15a5  //从邮件中拾取了道具
#define G_M_PICKMONEY_FROM_MAIL 0x15a6 //从邮件中拾取了金钱

//右键查询玩家详细属性
#define G_M_REQ_PLAYER_DETAIL_INFO	0x15a7

#define G_M_QUERY_SUIT_INFO		0x15a8	//请求套装信息
#define G_M_CLEAR_SUIT_INFO		0x15a9  //清空套装消息
#define G_M_SAVE_SUIT_INFO		0x15aa	//保存套装消息
#define G_M_CHANGE_SUIT			0x15ab//一键换装
#define G_M_REPORT_UNION_LEVEL 0x15ac//通告工会等级
#define G_M_CITY_INFO			0x15ad	//告知mapsrv当前管理的地图关卡信息
#define G_M_DISCARD_COLLECT_ITEM 0x15ae		//放弃采集道具
#define G_M_PICK_COLLECT_ITEM  0x15af		//拾取采集道具
#define G_M_PICK_ALL_COLLECT_ITEM	0x15b0	//拾取所有采集道具


//玩家
#define G_M_PUTITEM_TO_PROTECTPLATE 0x15b1 
#define G_M_CONFIRM_PROTECTPLATE 0x15b2
#define G_M_RELEASE_SPECIALPROTECT 0x15b3
#define G_M_CANCEL_SPECIALPROTECT 0x15b4
#define G_M_TAKE_ITEM_FROM_PROTECTPLATE 0x15b5

//时装
#define G_M_EQUIP_FASHION_SWITCH 0x15b6

//防沉迷
#define G_M_GAME_STATE_NOTIFY	0x15b7

#define G_M_END_ANAMORPHIC 0x15b8//变形停止
#define G_M_ATTACK_WAR_BEGIN	0x15b9		//通知所有mapsrv该地图该任务ID的攻城战开始
#define G_M_NOTIFY_RELATION_NUMBER 0x15ba	//通知mapsrv玩家的关系人数
#define G_M_USE_SKILL	0x15bb				//玩家使用技能 **新技能体系
#define G_M_USE_SKILL_2 0x1503              //06.30矩形攻击，相应调整所有范围攻击协议

#define G_M_CHANAGE_UNION_PRESTIGE 0x15bc	//修改工会威望
#define G_M_CHANAGE_UNION_BADGE 0x15bd      //修改工会徽章

//仓库的协议
#define G_M_CLOSE_STORAGE_PLATE  0x15be//关闭仓库托盘
#define G_M_CHANGE_STORAGE_LOCKSTATE  0x15bf//改变仓库的锁定状态
#define G_M_TAKE_ITEM_STORAGE 0x15c0//从仓库中取出物品到背包,不指定位置
#define G_M_TAKE_ITEM_FROM_STORAGE_TO_PKG 0x15c1//从仓库中取出物品放入背包指定位置
#define G_M_PUT_ITEM_TO_STORAGE 0x15c2//将物品放入托盘中
#define G_M_PUT_ITEM_FROM_PKG_TO_STORAGE  0x15c3//将背包中的物品放入仓库中的指定位置
#define G_M_SET_NEW_STORAGE_PASSWORD 0x15c4//设置密码
#define G_M_RESET_STORAGE_PASSWORD 0x15c5//重新设置密码
#define G_M_TAKEITEM_CHECK_PASSWORD 0x15c6//取出物品时输入密码
#define G_M_CHANGELOCKSTATE_CHECK_PASSWORD 0x15c7//改变仓库锁定状态时输入密码
#define G_M_EXTEND_STORAGE 0x15c8//扩展仓库
#define G_M_SORT_STORAGE   0x15c9//排序仓库

#define G_M_DBGET_PRE                    0x15ca     //DB信息开始;
#define G_M_DBGET_POST                   0x15cb    //DB信息结束(与G_M_DBGET_PRE一一对应);
#define G_M_PLAYER_ENTER_MAP             0x15cc     //玩家离开地图(在G_M_DBGET_POST之后发送);
#define G_M_DBGET_INFO                   0x15cd     //DB取到的信息；
#define G_M_NDSMR_INFO                   0x15ce     //no db, switch map reserve信息；

#define G_M_STORAGE_SAFE_INFO            0x15cf     //仓库的安全信息
#define G_M_STORAGE_ITEM_INFO            0x15d0     //仓库的道具信息
#define G_M_STORAGE_FORBIDDEN_INFO       0x15d1     //仓库的禁用信息
#define G_M_SWAP_STORAGE_ITEM            0x15d2     //交换仓库中的道具
#define G_M_GMQUERY_REQ					 0x15d3     //gm查询信息
#define G_M_ITEM_NPC_REQ					0x15d4		//ITEM NPC的查询ID
#define G_M_SORT_PKG_ITEM                   0x15d5   //排序包裹的道具
#define G_M_BATCH_SELL_ITEM_REQ          0x15d6      //一键贱卖
#define G_M_REPURCHASE_ITEM_REQ          0x15d7      //回购

#define G_M_CLOSE_OLTEST_PLATE           0x15d8      //关闭答题界面
#define G_M_ANSWER_OLTEST_QUESTION       0x15d9      //回答在线问题
#define G_M_DROP_OLTEST                  0x15da      //放弃在线答题

#define G_M_SINGLE_PLAYER_TEAMID_NOTIFY         0x15db //单人的组队ID通知  

#define G_M_NEW_COPY_CMD                 0x15dc //通知创建新副本；
#define G_M_DEL_COPY_CMD                 0x15dd //通知mapsrv立即删除旧副本；
#define G_M_DEL_COPY_QUEST               0x15de //centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)

#define G_M_SELECT_WAR_REBIRTH_OPTION    0x15df //选择攻城战复活点选项
#define G_M_REPURCHASE_ITEMS_LIST_REQ    0x15e0 //获取回购列表

#define G_M_ON_RACEMASTER_SETNEW_PUBLICMSG  0x15e1 //族长设置新的公告信息
#define G_M_RACEMASTER_TIMEINFO_CHANGE   0x15e2 //族长的时间信息改变了
#define G_M_RACEMASTER_TIMEINFO_EXPIRE   0x15e3 //族长时间相关信息改变了
#define G_M_RACEMASTER_DECLAREWARINFO_CHANGE  0x15e4 //族长宣战信息改变了
#define G_M_RACEMASTER_DETIALINFO        0x15e5//接到族长的详细信息
#define G_M_RACEMASTER_NEW_BORN          0x15e6//新的族长诞生
#define G_M_RACEMASTER_SELECT_OPTYPE     0x15e7//族长选择操作类型
#define G_M_RACEMASTER_DECLAREWAR_TO_OTHERRACE 0x15e8 //族长对其他种族宣战
#define G_M_RACEMASTER_SETNEW_PUBLICMSG  0x15e9 //族长设置新的种族公告
#define G_M_LOAD_RACEMASTER_FROM_DB_RST  0x15ea //mapsrv查询族长信息
#define G_M_FORBIDDEN_RACEMASTER_AUTHORITY 0x15eb

#define G_M_LEAVE_STEAM_NOTI             0x15ec //通知玩家离开副本组队(准备踢玩家出副本，如果他当前在副本中的话)；
#define G_M_QUERY_GLOBE_COPYINFOS        0x15ed //向mapsrv查询全局副本信息(该mapsrv管理的全部伪副本)

#define G_M_ADD_PUBLICNOTICE             0x15ee //通知公告信息增加
#define G_M_REMOVE_PUBLICNOTICE          0x15ef //移除公告信息
#define G_M_MODIFY_PUBLICNOTICE          0x15f0 //修改公告信息
#define G_M_ADD_MARQUEUE_NOTICE          0x15f1 //增加一个跑马灯消息发送

#define G_M_TIMELIMIT_RES                0x15f2 //客户端点击倒计时确认框
#define G_M_BROADCAST_TMPNOTICE          0x15f3 //广播临时消息
#define G_M_BROADCAST_PARAMNOTICE        0x15f4 //广播可变参数消息
#define G_M_DIAMOND_TO_PRESTIGE_NOT		 0x15f5 //星钻转公会威望
#define G_M_GMACCOUNT_NTF				 0x15f6 //gm账号通知
#define G_M_FORWARD_UNION_CMD            0x15f7 //中转公会操作
#define G_M_CREATE_UNION_ERR_NTF		 0x15f8 //创建工会出错通知
#define G_M_MODIFY_PW_STATE_REQ			 0x15f9 //修改伪线状态
#define G_M_MODIFY_PW_MAP_STATE_REQ		 0x15fa //修改伪线地图状态
#define G_M_SHIFT_PW_REQ				 0x15fb //切线请求
#define G_M_START_NEWAUCTION             0x15fc //玩家拍卖新的道具
#define G_M_PLAYER_BID_ITEM              0x15fd //玩家竞拍道具
#define G_M_PLAYER_SUBMONEY              0x15fe //减去玩家的金钱

#define G_M_PLAYER_COMBO                 0x15ff //临时协议gatesrv发往mapsrv玩家连击数；

#define G_M_ITEM_UPGRADE_PLATE           0x1f00 //玩家弹出物品升级界面请求(随时随地升级), GMItemUpgradePlate

#define G_M_USE_MONEY_TYPE               0x1f01 //使用钱币类型；
#define G_M_HONOR_MASK                   0x1f02 //玩家选择显示的称号；
#define G_M_FRIEND_GAIN_NTF				 0x1f03	//好友收益通知（组队：攻防加成；非组队：经验） 
#define G_M_QUERY_EXP_OF_LEVEL_UP        0x1f04 //获取各职业的升级经验
#define G_M_OFFLINE_PLAYER_INFO_RST      0x1f05 //非在线玩家的查询结果（基本信息+装备信息）

#define G_M_BIAS_INFO                    0x1f06 //各排行榜阈值信息；

#define G_M_SORT_PKG_ITEM_BEGIN			 0x1f07 //开始排序
#define G_M_SORT_PKG_ITEM_END			 0x1f08 //结束排序

#define G_M_REPLY_CALL_MEMBER			 0x1f09	//玩家回应召唤

#define G_M_START_CAST					 0x1f0a //玩家开始读条
#define G_M_STOP_CAST					 0x1f0b //玩家中断读条

	///...gatesrv发往mapsrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///mapsrv发往gatesrv的消息命令字,形式0xXXXX...
#define M_G_MAP_ISFULL         0x5100 //mapsrv发往gatesrv,通知本mapsrv是否满；
#define M_G_MANED_MAP          0x5101 //mapsrv发往gatesrv,本mapsrv管理的一个地图代码

#define M_G_STEP               0x5103  //mapsrv发往gatesrv走动请求消息
#define M_G_RUN                0x5104  //mapsrv发往gatesrv跑动请求消息
#define M_G_KICK_PLAYER        0x5105 //mapsrv发往gatesrv，踢玩家

#define M_G_LEAVE_VIEW         0x5106 //mapsrv发往gatesrv,怪物或其它玩家离开玩家视野;
//#define M_G_PLAYER_APPEAR    0x5107 //mapsrv发往gatesrv，玩家在地图上出现；
#define M_G_PLAYER_LEAVE       0x5108 //mapsrv发往gatesrv，玩家离开地图；
#define M_G_OTHER_PLAYER_INFO  0x5109 //其它玩家信息;

#define M_G_PLAYER_NEWTASK     0x510a //给玩家添加了一个新任务；
#define M_G_PLAYER_TASKSTATUS  0x510b //通知玩家任务新状态；
#define M_G_PLAYER_TASKRECORD_UPDATE 0x510c//更新记录的状态
#define M_G_PLAYER_TASKHISTORY 0x510d//玩家曾经做过的任务列表
#define M_G_PLAYER_SHOWTASKUI  0x510e//显示任务有关的UI

#define M_G_ENVNOTI_FLAG       0x5110 //玩家跨块时，通知其一系列关于其周围环境的信息，本消息标记这一系列消息的开始或结束；
//#define M_G_PLAYER_NEWINFO   0x5110 //mapsrv发往GateSrv,更新玩家信息；
//#define M_G_PLAYER_NEWPKG    0x5111 //mapsrv发往GateSrv,更新玩家包裹；

#define M_G_CHAT               0x5112 //聊天消息；

#define M_G_MONSTER_BORN 0x5129 //mapsrv发往gatesrv，怪物在地图上出生；
#define M_G_MONSTER_MOVE 0x512a //mapsrv发往gatesrv，怪物移动；
#define M_G_MONSTER_DIE  0x512b //mapsrv发往gatesrv，怪物死亡；
#define M_G_PLAYER_ATTACK_TARGET 0x512c //mapsrv发往gatesrv，怪物被攻击；
#define M_G_MONSTER_ATTACK 0x512d //mapsrv发往gatesrv，怪物被攻击；
#define M_G_MONSTER_INFO 0x512e //mapsrv发往gatesrv,怪物信息(血量，速度，方向等)；
#define M_G_MONSTER_ATTACK_PLAYER 0x512f
#define M_G_MONSTER_DISAPPEAR 0x5130 //mapsrv发往gatesrv，怪物消失；

#define M_G_ERROR 0x5131 //mapsrv通知gatesrv各种错误发生；

#define M_G_ROLEATT_UPDATE 0x5132 //mapsrv通知gatesrv角色属性改变；

#define M_G_PLAYER_LEVELUP	0x5133 //mapsrv通知gatesrv角色升级

#define M_G_TMPINFO_UPDATE 0x5134 //mapsrv通知gatesrv角色非存盘属性变化

//#define M_G_PLAYER_ATTACK_PLAYER 0x5135 //mapsrv通知玩家攻击玩家；

#define M_G_SWITCH_MAP 0x5136 //通知客户端地图跳转

#define M_G_REQ_SWITCH_MAP 0x5137 //向gatesrv请求跳转地图（跳转目标不在本mapsrv管理范围内时发此请求）

#define M_G_FULL_PLAYER_INFO 0x5138//玩家完整信息

//消息大致顺序,G_C_PLAYER_DYING-->C_G_DEFAULT_REBIRTH-->G_C_REBIRTH_INFO-->C_G_REBIRTH_READY-->G_C_PLAYER_APPEAR
#define M_G_PLAYER_DYING    0x5139 //玩家死亡(倒计时)

//消息大致顺序,G_C_PLAYER_DYING-->C_G_DEFAULT_REBIRTH-->G_C_REBIRTH_INFO-->C_G_REBIRTH_READY-->G_C_PLAYER_APPEAR
#define M_G_REBIRTH_INFO    0x513a //玩家复活点信息；


#define  M_G_ITEMPACK_APPEAR      0x513b //道具包出现
#define  M_G_ITEMPACK_DISAPPEAR   0x513c //道具包消失
#define  M_G_ITEMPACK_INFO        0x513d //道具包的信息
#define  M_G_EQUIP_PLAYERITEM     0x513e //装备道具 CC
#define  M_G_TEAMMEMBER_GET_NEWITEM  0x513f //组队的玩家拾取了道具
#define  M_G_PICKITEM_FAILED      0x5141 //拾取道具失败 包含有本次失败的编号 和失败的道具
#define  M_G_ONECLIENTPICKITEM    0x5142 //其他玩家提取了道具包中的一个道具
#define  M_G_BATCH_ITEM_SEQUENCE  0x5143 //获取批量物品流水号
#define  M_G_ONEPLAYER_USEITEM    0x5144 //一个角色使用了道具
#define  M_G_USEITEM_RESULT       0x5145 //角色使用道具的结果
#define  M_G_UNUSEITEM            0x5147 //玩家不使用道具
#define  M_G_UNUSEITEM_RESULT     0x5148 //不使用道具的结果 cc
//#define  M_G_SWAPITEM_RESULT      0x5149 //交换道具包道具的结果 x
#define  M_G_SWAP_EQUIPITEM_RST   0x514b //玩家交换装备栏道具的结果 cc
#define  M_G_ITEMPKG_CONCISEINFO  0x514c //玩家包裹道具的简明信息
#define  M_G_DROPITEM_RESULT      0x514d //玩家丢弃道具的结果
#define  M_G_SET_PKGITEM          0x514e //设置玩家包裹中的道具

#define  M_G_PLAYER_BUFFSTART     0x514f   //玩家获得一个BUFF
#define  M_G_PLAYER_BUFFEND       0x5150   //玩家的一个BUFF消失
#define  M_G_PLAYER_BUFFUPDATE    0x5151   //关于玩家持续性BUFF的消息更新
#define  M_G_HONOR_MASK           0x5152 //玩家选择显示的称号；
//#define  M_G_MONEY_UPDATE         0x5152  //玩家金钱更新
#define  M_G_MONSTER_BUFFSTART    0x5153	//怪物的BUFF开始
#define  M_G_MONSTER_BUFFUPDATE   0x5154   //怪物的HP BUFF的更新
#define  M_G_REQ_BUFFEND	      0x5155   //关于停止BUFF的反馈
#define  M_G_USE_ASSISTSKILL	  0x5156	//关于使用辅助技能

#define  M_G_SET_PLAYERPOS        0x5157   //设置玩家位置（同地图跳位置）

#define  M_G_SHOW_PLATE           0x5158    //指示客户端显示托盘；
#define  M_G_ADDSTH_TO_PLATE_RST  0x5159    //往托盘放置物品结果；
#define  M_G_TAKESTH_FROM_PLATE_RST 0x515a  //从托盘取物品结果；
#define  M_G_PLATE_EXEC_RST       0x515b    //托盘执行结果；
#define  M_G_CLOSE_PLATE          0x515c    //对玩家关托盘的响应，或者主动关玩家托盘；
#define  M_G_CHANGEITEMCRI        0x515d    //升级道具时的概率改变
//#define  M_G_UPDATEITEMERROR      0x515e    //道具Update的错误
#define  M_G_MONSTER_BUFF_END	  0x515f    //怪物ＢＵＦＦ消失的通知

#define  M_G_ENTERMAP_RST         0x5164    //玩家进地图结果

#define  M_G_PLAYERBUSYING        0x5165    //玩家忙的消息

#define  M_G_FINAL_REBIRTH_RST    0x5166    //玩家最终复活结果

#define  M_G_ADD_APT_RST          0x5167    //添加活点结果；

#define  M_G_EVILTIME_RST         0x5168    //返回查询的恶魔时间给客户端
#define  M_G_FRIENDINFO_UPDATE    0x5169    //更新好友的信息
#define  M_G_REPAIRITEM_COST_RST  0x516a    //查询的维修道具的花费

////////////////////////关于NPC商店////////////////////////////////
/*
	1、show shop && 第一页物品数量
	2、CG请求id号shop第n页物品各自数量
	3、GC返回id号shop第n页物品各自数量
	4、CG请求购买id号shop第n页第n个物品，以及该物品的ID号与数量，货币类型与数量；
	5、GC返回玩家购买id号shop第n页第n个物品，以及该物品的ID号与数量，货币类型与数量，购买成功或失败；
	6、CG玩家请求售出id类型物品，售出物品在包裹中的位置，售出物品数量，售出价格；
	7、GC返回玩家售出id类型物品结果，售出物品在包裹中的位置，售出物品数量，售出价格，售出后玩家的总金钱数，售出成功或失败；
*/
#define M_G_SHOW_SHOP     0x5170 //查询商店物品数量
#define M_G_SHOP_BUY_RST  0x5171 //购买商店物品请求
#define M_G_ITEM_SOLD_RST 0x5172 //玩家请求售出物品

////////////////////////////骑乘系统/////////////////////////////////////
#define M_G_ACTIVEMOUNT_FAIL 0x5180 //玩家使用骑乘道具失败
//#define M_G_MOUNTLEADER 0x5181 //玩家骑乘队长,包含队伍的信息
//#define M_G_MOUNTASSIST 0x5182 //玩家骑乘队员时，包含的接受到的队伍信息
#define M_G_MOUNTTEAM_APPEAR 0x5183 //骑乘队伍在地图上出现
#define M_G_MOUNTTEAM_DISBAND 0x5184 //骑乘队伍解散
//#define M_G_PLAYER_LEAVE_MOUNTTEAM 0x5185 //一个队员离开了骑乘队伍
#define M_G_BEINVITE_MOUNTTEAM 0x5186 //被邀请加入骑乘队伍
#define M_G_INVITE_RESULT 0x5187 //邀请别人加入队伍的结果
//#define M_G_MOUNTTEAM_MOVE 0x5188 //骑乘队伍移动的消息
//#define M_G_JOIN_MOUNTTEAM 0x5189 //玩家加入骑乘队伍
#define M_G_MOUNTSTATE_CHANGE 0x518a //玩家骑乘状态改变
#define M_G_ONRIDELEADER_SWITCHMAP 0x518b //骑乘队长更换地图

#define M_G_SHOW_MAILBOX 0x518c //显示邮箱界面

///////////////////////////////组队系统///////////////////////
#define M_G_REQ_JOIN_TEAM 0x5190	//加入组队
#define M_G_REQ_PLAYERTEAMINFO 0x5191 //查询玩家在组队中的信息
#define M_G_REQ_AGREEROLLITEM  0x5192 //查询是否同意Roll点物品
#define M_G_ROLLITEM_NUM       0x5193 //通知玩家Roll到的点数
#define M_G_ROLLITEM_RESULT    0x5194 //通知Roll点的结果
#define M_G_MEMBER_DETIALINFO_UPDATE  0x5195 //组队玩家的信息更新
#define M_G_MEMBER_PICKITEM    0x5196//组队玩家拾取道具通知其他玩家

//////////////////////////////物品交易////////////////////////
#define M_G_SEND_TRADE_REQ_ERROR    0x51a0 //发送请求失败
#define M_G_RECV_TRADE_REQ			0x51a1 //收到交易邀请
#define M_G_SEND_TRADE_REQ_RESULT	0x51a2 //交易请求处理结果
#define M_G_ADD_OR_DELETE_ITEM		0x51a3 //交易中增加或删除物品
#define M_G_CANCEL_TRADE			0x51a4 //取消交易
#define M_G_LOCK_TRADE				0x51a5 //锁定交易
#define M_G_CONFIRM_TRADE			0x51a6 //确认交易
#define M_G_SET_TRADE_MONEY         0x51a8 //交易金钱

/////////////////////////////好友系统//////////////////////////////////
#define M_G_ADD_FOEINFO   0x51a7		//MAPSRV增加仇人信息


////////////////////////////修复耐久度///////////////////////////////////
#define M_G_REPAIR_WEAR_BY_ITEM   0x51a9	//修复道具的耐久度（使用修复性道具）
#define M_G_NOTIFY_WEAR_CHANGE    0x51aa    //通知耐久度变化

////////////////////////关于技能成长////////////////////////////////
#define M_G_UPGRADE_SKILL_RESULT 0x51ab		//技能升级的请求结果
#define M_G_SPSKILL_UPGRADE 0x51ac			//SP技能升级
#define M_G_ADD_SPSKILL_RESULT  0x51ad		//添加新的SP技能成功与否
#define M_G_CLEAN_SKILLPOINT 0x51af			//技能洗点

#define M_G_REPAIR_WEAR_BY_NPC 0x51b0		// npc维修耐久度
#define M_G_CHANGE_BATTLE_MODE 0x51b1		//修改战斗模式
#define M_G_PLAYER_USE_HEAL_ITEM 0x51b2		//mapsrv上玩家使用恢复道具
#define M_G_INFO_UPDATE    0x51b3         //mapsrv上战斗后信息的更新
#define M_G_BUFF_DATA      0x51b4			//mapsrv上的个体BUFF信息
#define M_G_EQUIPITEM_UPDATE 0x51b5         //更新玩家的装备信息
#define M_G_TARGET_GET_DAMAGE   0x51b6			//某个目标获得伤害
#define M_G_SWICHMAP_REQ 0x51b7    //跳转地图
#define M_G_CHANGE_TEAM_STATE 0x51b8  //队伍状态改变的通知

//天谴相关
#define M_G_BROADCAST_PUNISH_INFO 0x51b9 //更新天谴玩家的位置信息转发置所有mapsrv
#define M_G_PUNISHMENT_START     0x51ba  //告诉GateSrv0 天谴开始
#define M_G_PUNISHMENT_END       0x51bb	//告之GateSrv0，通知其他mapsrv天谴结束
#define M_G_PUNISH_INFO			  0x51bc	//暂时预留


//关于GM工具
#define M_G_GMCMD_REQUEST 0x51bd  	//由于可能人员不在mapsrv上向其他mapsrv发送GM命令的请求
#define M_G_GMCMD_RESULT  0x51be   //通知GM命令成功与否

//#define M_G_UPDATE_RANKCHART     0x51bf		//将mapsrv的最新的排行榜信息发送给客户端
//#define M_G_RANKPLAYER_ONLINE    0x51c0     //排行榜的玩家上线
#define M_G_RANKPLAYER_OFFLINE   0x51c1     //排行榜的玩家下线
#define M_G_REMOVE_RANKPLAYER    0x51c2     //移除排行榜的玩家


//宠物相关
//#define M_G_SUMMON_PET					 0x5102		//原0x51c2与M_G_REMOVE_RANKPLAYER冲突，找一个未被使用的命令字//召唤宠物，群发
//#define M_G_CLOSE_PET					 0x51c3		//唤回宠物，群发
//#define M_G_CHANGE_PET_NAME				 0x51c4		//更改宠物姓名，群发(看是否处于召唤状态）
//#define M_G_CHANGE_PET_SKILL_STATE		 0x51c5		//更改技能设置状态，单发
//#define M_G_CHANGE_PET_IMAGE			 0x51c6		//更改宠物外形，群发(看是否处于召唤状态)
//#define	M_G_UPDATE_PET_EXP				 0x51c7    //更新宠物经验
#define	M_G_PETINFO_INSTABLE			 0x5102     //宠物易变信息----只发送给宠物主人的信息；
#define M_G_PETINFO_STABLE				 0x51c3		//宠物稳定信息----群播给附近玩家的宠物信息( 宠物外形，名字 ）
#define M_G_PET_ABILITY                  0x51c4     //宠物能力;
//#define M_G_UPDATE_PET_STATE			 0x51cc		//更新宠物状态

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12


#define M_G_DBSAVE_PRE                   0x51c5     //DB保存开始;
#define M_G_DBSAVE_POST                  0x51c6     //DB保存结束(与M_G_DBSAVE_POST一一对应);
#define M_G_PLAYER_LEAVE_MAP             0x51c7     //玩家离开地图(在M_G_DBSAVE_POST之后发送);
#define M_G_DBSAVE_INFO                  0x51c8     //DB保存信息；
#define M_G_NDSMR_INFO                   0x51c9     //no db, switch map reserve信息；

#define M_G_SIMPLE_PLAYER_INFO           0x51ca    //广播给周围的玩家简略信息，信息分类修改，by dzj, 10.03.24;
#define M_G_SIMPLEST_INFO                0x51cb    //周围人群密集时的最简消息，信息分类修改，by dzj, 10.03.24;

#define G_D_DBSAVE_PRE                   0x17c5     //DB保存开始;
#define G_D_DBSAVE_POST                  0x17c6     //DB保存结束(与M_G_DBSAVE_POST一一对应);
#define G_D_DBSAVE_INFO                  0x17c8     //DB保存信息;

#define D_G_DBGET_PRE                    0x71c5     //DB取信息开始;
#define D_G_DBGET_POST                   0x71c6     //DB取信息结束(与M_G_DBSAVE_POST一一对应);
#define D_G_DBGET_INFO                   0x71c8     //DB保存信息;
///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define M_G_QUERY_PUNISH_PLAYER_INFO	 0x51cd    //告之天谴人员位置去客户端	

//排队系统
#define M_G_QUERY_QUEUE_RESULT        0x51ce		//查询mapsrv的队列结果 
#define M_G_QUEUE_OK                  0x51cf		//排队到达

//工会系统
#define M_G_DISPLAY_UNION_ISSUED_TASKS 0x51d0	//显示已发布任务
//#define M_G_CAN_LEARN_UNION_SKILLS 0x51d1	//可以学习的工会技能列表
#define M_G_CHANGE_UNION_ACTIVE 0x51d2      //工会活跃度变化

#define M_G_CHECK_PKG_FREE_SPACE 0x51d3		//检查包裹中剩余空间
#define M_G_FETCH_VIP_ITEM 0x51d4			//领取vip物品

//攻城系统
#define M_G_ATTACK_CITY_BEGIN	0x51d5			//通知攻城战开始
#define M_G_ATTACK_CITY_END		0x51d6			//通知攻城战结束
//0x51d7 空缺
#define M_G_NOTIFY_WAR_TASK_STATE   0x51d8		//通知GATESRV向MAPSRV组播所有得到攻城任务的状态


//动作分割
//使用UseSkill2后废弃，原ID号改由MOUSE_TIP使用, by dzj, 10.05.31,#define M_G_USE_PARTION_SKILL_RST 0x51d9		//玩家使用动作分割技能
#define M_G_MOUSE_TIP 0x51d9 //客户端的鼠标tip信息查询(HP|MP)

//宠物移动
//#define M_G_PET_MOVE	0x15da			//宠物移动
//#define M_G_RACE_GROUP_NOTIFY  0x51db	//种族群发消息

#define M_G_FETCH_NON_VIP_ITEM 0x51dc	//领取非VIP物品

#define M_G_PICKITEM_FROM_MAIL_RST 0x51dd //从邮件中拾取道具的结果
#define M_G_PICKMONEY_FROM_MAIL_RST 0x51de //从邮件中拾取金钱的结果
//#define M_G_WORLD_NOTIFY  0x51df   //世界群发消息
#define M_G_REQ_PLAYER_DETAIL_INFO_RST   0x51e0		//右键查询属性的结果
#define M_G_QUERY_SUIT_INFO_RST 0x51e1	//请求套装信息结果
#define M_G_CLEAR_SUIT_INFO_RST 0x51e2  //清空套装消息
#define M_G_SAVE_SUIT_INFO_RST 0x51e3	//保存套装消息
#define M_G_CHANGE_SUIT_RST 0x51e4//一键换装结果
#define M_G_GATE_UPDATE_GOODEVILPOINT		0x51e5		//GateSrv更新善恶值for组队
#define M_G_COLLECT_TASK_RST  0x51e6		//反馈采集的物品
#define M_G_UPDATE_CITY_INFO  0x51e7			//更新城市信息


#define M_G_THREAT_LIST			  0x51fd	//怪物的威胁列表发送给客户端用于调试
#define M_G_BROADCAST			  0x51fe	//关于范围通知的都是此协议
//#define M_G_PLAYER_COMBO          0x51ff //临时协议，mapsrv发往gatesrv，玩家连击数；


#define M_G_SHOW_PROTECTPLATE 0x51ff //显示保护道具界面
#define M_G_PUTITEM_TO_PROTECTPLATE_RST 0x5200 //放入界面的返回
#define M_G_SET_ITEM_PROTECTPROP 0x5201 //设置道具的保护属性
#define M_G_SENDITEM_WITH_PROTECTPROP 0x5202 //发送保护道具给客户端
#define M_G_RELEASE_SPECIALPROTECT_RST 0x5203 //解除特殊保护
#define M_G_CANCEL_SPECIALPROTECT_RST 0x5204 //取消特殊保护
#define M_G_CONFIRM_PROTECTPLATE_RST 0x5205 //执行托盘操作的返回
#define M_G_TAKE_ITEM_FROM_PROTECTPLATE_RST 0x5206 //把物品从托盘中取出

#define M_G_LOAD_RANKINFO       0x5207     //MapSrv请求对应排行榜的信息
#define M_G_UPDATE_RANKPLAYER_LEVEL 0x5208 //更新排行榜玩家的等级
#define M_G_UPDATE_RANKPLAYER_RANKINFO 0x5209 //通知其他mapsrv更新对应玩家的排行信息


//预留些位
#define M_G_UPDATE_PLAYERITEM_INFO 0x5210  //更新道具信息
#define M_G_UPDATE_TASK_INFO 0x5211  //更新任务信息
#define M_G_CHANGE_TARGET_NAME 0x5213	//更新目标的名字
#define M_G_KICK_ALL_PLAYER 0x5214	//踢除所有玩家
#define M_G_NOTIFY_ACTIVITY_STATE 0x5215 //通知活动状态
#define M_G_BACK_TASK_UPDATE 0x5216 //更新后台的一些任务
#define M_G_NOTIFY_NEW_TASK_RECORD 0x5217
#define M_G_SEND_SYSMAIL_TO_PLAYER  0x5218
#define M_G_EXCEPTION_NOTIFY  0x5219	 //MapSrv上的异常通知
#define M_G_UPDATE_PLAYER_CHANGE_BASE_INFO 0x521a //更新玩家的Base中的可变信息
#define M_G_SEND_SPECIAL_STRING 0x521b	//发送给客户端特殊的string
#define M_G_USE_SKILL_RST	0X521c	//使用技能的结果，**新技能体系的协议
#define M_G_CHANAGE_UNION_PRESTIGE_REQ 0x521d//修改工会威望(GM指令)
#define M_G_UNION_MEMBER_SHOW_INFO 0x521e//工会成员场景显示信息

//关于仓库的协议
#define M_G_SHOW_STORAGE 0x521f//显示仓库
#define M_G_STORAGE_SAFE_INFO  0x5220//关于仓库的安全
#define M_G_STORAGE_ITEM_INFO  0x5221//每半页半页通知
#define M_G_SHOW_STORAGE_PASSWORD_DIALOG 0x5222//弹出仓库对应类型的密码框
#define M_G_CHANGE_STORAGE_LOCKSTATE 0x5223//改变仓库的锁定状态
#define M_G_UPDATE_STORAGE_VALIDROW 0x5224//改变仓库的可用行数
#define M_G_UPDATE_STORAGE_SECURITY_STRATEGY 0x5225//更新仓库的安全策略
#define M_G_UPDATE_STORAGE_ITEMINFO_BYINDEX  0x5226//按照索引更新仓库信息
#define M_G_TAKEITEM_FROM_STORAGE_FAIL 0x5227//从仓库取出物品错误
#define M_G_PUTITEM_TO_STORAGE_FAIL 0x5228//物品放入仓库错误
#define M_G_SET_NEWPASSWORD_ERROR   0x5229//设置新密码错误
#define M_G_RESET_PASSWORD_ERROR  0x522a//重新设置密码错误
#define M_G_TAKEITEM_PASSWORD_ERROR 0x522b//取出物品输入密码错误
#define M_G_EXTEND_STORAGE_ERROR  0x522c//扩展仓库错误

#define M_G_UNION_MULTI_REQ   0x522d//工会n倍活跃度或威望
#define M_G_SAVE_REBIRTH_POS_NTF 0x522e//保存复活点通知

#define M_G_PLAYER_POSINFO       0x522f //通知玩家或怪物(GID)位置信息

#define M_G_UPDATE_STORAGE_SAFE_INFO  0x5230 //玩家更新仓库的安全信息
#define M_G_UPDATE_STORAGE_ITEM_INFO  0x5231 //玩家更新仓库的道具信息
#define M_G_UPDATE_STORAGE_FORBIDDEN_INFO 0x5232 //玩家更新仓库的禁止信息
#define M_G_QUERY_STORAGE_INFO 0x5233//玩家查询仓库信息
#define M_G_NOTIFY_ROOKIE_STATE	0x5234	//通知玩家新手状态
#define M_G_GMQUERY_RESP 0x5235//gm查询结果
#define M_G_ITEMNPC_INFO 0x5236 //物件NPC状态通知
#define M_G_ITEMNPC_ID	0x5237	//物件NPCid的通知

#define M_G_BATCH_SELL_ITEMS_RESP 0x5238//一键贱卖
#define M_G_REPURCHASE_ITEM_RESP  0x5239//回购
#define M_G_REPURCHASE_ITEM_LIST  0x523a//回购列表

#define M_G_CLOSE_OLTEST_PLATE  0x523b//关闭在线测试托盘
#define M_G_SHOW_PREVUE_OLTEST_DLG 0x523c//弹出预告框
#define M_G_SHOW_OLTEST_QUESTION 0x523d//弹出题目框
#define M_G_ANSWER_QUESTION_RST  0x523e//回答问题的结果
#define M_G_SHOW_ANSWER_ALLQUESTIONS 0x523f //已回答完所有问题
#define M_G_SPLIT_ITEM_ERROR         0x5240 //分解物品错误
#define M_G_LOG_PLAYER_INFO			 0x5241 //玩家日至信息

#define M_G_PLAYER_TRY_COPY_QUEST    0x5242 //玩家试图进副本请求(之前无副本的情况下包含新建副本)
#define M_G_PLAYER_LEAVE_COPY_QUEST  0x5243 //玩家离开副本请求
#define M_G_COPY_NOMORE_PLAYER       0x5244 //指示副本不可再进人(准备删副本前调用)
#define M_G_DEL_COPY_QUEST           0x5245 //删副本请求
#define M_G_PLAYER_OUTMAPINFO        0x5246 //玩家离开副本后去往的地图信息；
#define M_G_GLOBE_COPYINFOS          0x5247 //玩家查询全局副本信息，返回本mapsrv管理的伪副本信息

#define M_G_TIMELIMIT                0x5248 //指示客户端弹倒计时框；

#define M_G_BUFF_START               0x5250  //BUFF start
#define M_G_BUFF_END					0x5251	//buff end
#define M_G_BUFF_STATUS_UPDATE        0x5252	//buff change
#define M_G_BUFF_HPMP_UPDATE			0x5253	//buff dot hot

#define M_G_PLAYER_WAR_DYING		 0x5254 //攻城战死亡倒计时
#define M_G_WAR_REBIRTH_OPTION_ERR	 0x5255 //攻城战复活错误

#define M_G_NOTICE_RACEMASTER_TIMEINFO_EXPIRE       0x5256 //族长时间相关信息过期
#define M_G_NOTICE_RACEMASTER_DECALREWARINF0_CHANGE 0x5257 //族长的宣战信息过期
#define M_G_NOTICE_RACEMASTER_TIMEINFO_CHANGE    0x5258 //族长时间相关信息更改了
//#define M_G_NOTICE_RACEMASTER_PUBLICMSG          0x5259 //通知族长的公告
#define M_G_NOTICE_RACEMASTER_PUBLICMSG_CHANGE   0x525a //通知族长公告修改了
#define M_G_UPDATE_RACEMASTER_INFO_TO_DB         0x525b //将族长信息存盘
#define M_G_RACEMASTER_DECLAREWAR_RST            0x525c //宣战的结果
#define M_G_RACEMASTER_GET_SALARY_RST            0x525d //获取工资的结果
#define M_G_RACEMASTER_NOTICE_NEWMSG_RST         0x525e //宣布新的公告的结果
#define M_G_SHOW_RACEMASTER_OPPLATE              0x525f //显示族长托盘
#define M_G_SHOW_RACEMASTER_DECLAREWAR_PLATE     0x5260 //显示族长宣战托盘
#define M_G_SHOW_RACEMASTER_PUBLICMSG_PLATE      0x5261 //显示设置族长公告托盘
#define M_G_CLOSE_RACEMASTER_PLATE               0x5262 //关闭族长的托盘
#define M_G_SHOW_RACEMASTER_ASSIGNCITY_PLATE     0x5263 //显示分配城市的托盘
#define M_G_LOAD_RACEMASTER_INFO_FROM_DB         0x5264 //mapsrv请求族长信息从数据库
#define M_G_RESET_RACEMASTER_DECLAREWAR          0x5267 //重置族长宣战信息
#define M_G_PUBLICNOTICE_MODIFY                  0x5268 //公告修改
#define M_G_PUBLICNOTICE_ADD                     0x5269 //公告增加
#define M_G_PUBLICNOTICE_REMOVE                  0x526a //公告移出
#define M_G_MARQUEUE_MSG_ADD                     0x526b //通知跑马灯消息
#define M_G_PARAM_MSG_ADD                        0x526c //可变参数的消息通知
#define M_G_SHOW_TEMPNOTICE_BYPOSITION               0x526d //
#define M_G_DISPLAY_WAR_TIMER						0x526e	//通知计时器
#define M_G_DISPLAY_WAR_COUNTER						0x526f	//通知计数器
#define M_G_CHARATER_DAMAGE_REBOUND					0x5270	//伤害反弹
#define M_G_BLOCK_QUAD_STATE_CHANGE              0x5271 //阻挡区属性变化
#define M_G_TEMP_BUFF_DATA							0x5272		//跳转地图用的BUFF信息
#define M_G_START_SELECT_RACE                       0x5273  //开始选择族长
#define M_G_FORWARD_UNION_CMD_RST				 0x5274//中转的工会操作验证结果
#define M_G_RETURN_SEL_ROLE						0x5275//返回选择角色界面
#define M_G_UNION_COST_ARG						 0x5276//创建工会消耗参数
#define M_G_REPORT_PW_INFOS						 0x5277//报告伪线信息
#define M_G_REPORT_PW_MAP_INFOS					 0x5278//报告伪线地图信息
#define M_G_MODIFY_PW_STATE_FORWARD				 0x5279//修改伪线状态中转
#define M_G_MODIFY_PW_MAP_STATE_FORWARD			 0x527a//修改伪线地图状态中转
#define M_G_SHIFT_PW_REQ_FORWARD				 0x527b//切线请求中转
#define M_G_KICKOFF_PW_REQ_FORWARD				 0x527c//踢线请求中转

#define M_G_PLAYER_START_NEWAUCTION              0x527d//玩家开始拍卖新的道具
#define M_G_PLAYER_BID_ITEM_NEXT                 0x527e//玩家竞拍道具
#define M_G_PLAYER_NEWAUCTION_RST                0x527f//玩家拍卖物品错误
#define M_G_PLAYER_BID_ITEM_ERROR                0x5280//玩家竞拍物品错误
#define M_G_PLAYER_SHOW_AUCTIONPLATE             0x5281//显示玩家拍卖行托盘

#define M_G_RST_TO_ATKER                         0x5282//玩家攻击时返回给攻击者的攻击结果
#define M_G_NORMAL_ATTACK                        0x5283//玩家普通攻击时广播给周围人的消息
#define M_G_SPLIT_ATTACK                         0x5284//玩家分割动作攻击时广播给周围人的消息
#define M_G_MOD_EFFECTIVE_SKILLID_RST            0x5285//改变有效技能结果

#define M_G_USE_MONEY_TYPE                       0x5286 //使用的钱币类型(0:银币,1:金币)

#define M_G_APP_BROCAST                          0x5287 //M-->G，由G最终发送的应用层广播消息
#define M_G_ADD_FRIEND_DEGREE_REQ				 0x5288 //增加好友度请求 
#define M_G_ADD_TWEET_NTF						 0x5289 //增加好友动态通知
#define M_G_REPORT_FRIEND_NON_TEAM_GAIN			 0x528a //报告好友非费组队时的收益(经验)
#define M_G_QUERY_EXP_OF_LEVEL_UP_REQ            0x528b //升级经验信息的查询结果
#define M_G_OFF_LINE_PLAYER_INFO_REQ			 0x528c //非在线玩家信息查询请求

#define M_G_RANK_CACHE_INFO                      0x528d //影响排行榜的新缓存信息；

#define M_G_MAPSRV_READY                         0x528e //mapsrv准备好(目前是在读到functionsrv阈值信息时向gatesrv发)

#define M_G_INVITE_CALL_MEMBER					 0x528f //玩家召唤队友，向队友发出邀请

#define M_G_START_CAST							 0x5290 //玩家开始读条
#define M_G_STOP_CAST							 0x5291 //玩家中断读条

#define M_G_RUN_FAIL							 0x5292 //玩家移动失败

#define M_G_FIGHTPET_PRIVATE                     0x5293 //战斗宠私人信息
#define M_G_FIGHTPET_PUBLIC                      0x5294 //战斗宠公共信息(用于广播)

	///...mapsrv发往gatesrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往centersrv的消息命令字,形式0x13XX...
#define G_E_PLAYER_ISEXIST 0x1301  //gatesrv发往centersrv玩家唯一性查询；
#define G_E_PLAYER_OFFLINE 0x1302  //gatesrv发往centersrv玩家下线消息；
#define G_E_INVITE_PLAYER_JOIN_TEAM 0x1303  //gatesrv发往centersrv邀请组队消息
#define G_E_PLAYER_MAPINFO 0x1304			//gatesrv发往centersrv角色所在的mapsrvID 
#define G_E_INVITE_ERROR   0x1305		   //gatesrv发送给centresrv邀请错误消息
#define G_E_INVITE_SUCCESS 0x1306        //gatesrv发送给centersrv邀请成功消息

#define G_E_SRVGROUP_CHAT  0x1307        //gatesrv发往centersrv全服(世界)聊天消息，center收到后应广播给所有的gate；
#define G_E_PRIVATE_CHAT   0x1308        //gatesrv发往centersrv密聊聊天消息，center收到后应转发给密聊对象所在gate；

#define G_E_REPORT_SELFID  0x1309        //gatesrv发往centersrv，通报自身SELF_SRV_ID；

#define G_E_PLAYERROLE_ONLINE 0x1310     //gatesrv发送给centersrv玩家角色上线
#define G_E_ADDFRINED 0x1311 //添加好友
#define G_E_REQADDFRIEND_RESULT 0x1312 //添加好友的结果

#define G_E_REQ_JOIN_TEAM  0x1320		  //gatesrv发给censrv转发消息，申请入队
#define G_E_REQ_JOIN_ERROR  0x1321       //gatesrv返回错误
#define G_E_FEEDBACK_ERROR  0x1322	      //由于邀请人员下线需要的提示

#define G_E_ADD_CHAT_GROUP_MEMBER_BY_OWNER 0x1323//查找玩家是否存在
#define G_E_IRC_QUERY_PLAYERINFO 0x1324	   //IRC用户查询好友或者仇人的信息
#define G_E_IRC_QUERY_PLAYERINFO_RST 0x1325	//IRC用返回查询信息的结果

//GM工具相关
#define G_E_GMCMD_REQUEST	0x1326 //发送至CENTERSRV来验证 GM命令
#define G_E_CHECK_PUNISHMENT   0x1327  //内容可能会改变 由于排行榜系统还没完善,送CENTER来决定谁会遭受天谴!!??	HYN
//#define G_E_RACE_GROUP_NOTIFY		0x1328		//种族的群发通知

#define G_E_NOTICE_PLAYER_HAVE_UNREAD_MAIL 0x1329 //通知玩家有未读邮件
#define G_E_NOTIFY_ACTIVITY_STATE 0x132a	//全服通知活动状态
#define G_E_GMQUERY_TARGET_CHECK 0x132b

#define G_E_PLAYER_TRY_COPY_QUEST    0x132c //玩家请求进副本；
#define G_E_PLAYER_LEAVE_COPY_QUEST  0x132d //玩家请求离开副本；
#define G_E_COPY_NOMORE_PLAYER       0x132e //通知center,副本不能进更多人了；           
#define G_E_DEL_COPY_QUEST           0x132f //发往center的删除副本请求(在center发往mapsrv的删除副本请求之后，因为mapsrv收到那个请求后会先踢玩家，踢完后再发此G_E_DEL_COPY_QUEST给center，最后center发delcopy cmd给map)；
#define G_E_QUERY_GLOBE_COPYINFOS    0x1330 //发往center查询全局副本信息；
#define G_E_QUERY_TEAM_COPYINFOS     0x1331 //发往center查询组队副本信息；
#define G_E_STEAM_DESTORY_CENTERNOTI 0x1332 //gate通知center，副本组队解散；
#define G_E_LEAVE_STEAM_CENTERNOTI   0x1333 //gate通知center,玩家离开副本组队；

#define G_E_COPYMAPSRV_NOTI          0x1334 //gate通知center一个新的副本mapsrv出现；

#define G_E_RESET_COPY               0x1335 //gate通知center重置副本；

#define G_E_PW_BASE_INFO             0x1336	//伪线信息
#define G_E_PWMAP_BASE_INFO			 0x1337	//伪线地图信息
#define G_E_ENTER_PWMAP_CHECK		 0x1338	//检查伪线地图是否可以进入
#define G_E_ENTER_PWMAP_ERR			 0x1339	//进入伪线地图出错
#define G_E_LEAVE_PWMAP_NTF			 0x133a	//通知离开伪线地图
#define G_E_MODIFY_PW_STATE_REQ		 0x133b	//修改伪线状态
#define G_E_MODIFY_PWMAP_STATE_REQ	 0x133c	//修改伪线地图状态

#define G_E_ROLE_OFFLINE			0x1340 //gate通知center，玩家角色下线，但账号未下线（重新选择角色）

	///...gatesrv发往centersrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///centersrv发往gatesrv的消息命令字,形式0x31XX...
#define E_G_PLAYER_ISEXIST			 0x3101  //centersrv发往gatesrv玩家选角色；
#define E_G_INVITE_PLAYER_JOIN_TEAM  0x3102  //centre传给gatesrv邀请组队消息 
#define E_G_INVITE_ERROR			 0x3103  //centresrv转发组队错误消息
#define E_G_INVITE_SUCCESS			 0x3104	 //centresrv转发组队成功消息
#define E_G_REQADDFRIEND             0x3105  //询问是否愿意被加为好友
#define E_G_ADDFRIENDFAILED          0x3106  //加好友失败
#define E_G_ADDFRIENDSUCCESS         0x3107  //加好友成功

#define E_G_SRVGROUP_CHAT            0x3108  //center发往gate全服聊天消息，gate收到后应广播至自身gate上的所有玩家；
#define E_G_PRIVATE_CHAT             0x3109  //center发往gate密聊聊天消息，gate收到后应转发给密聊对象玩家；

#define E_G_REQ_JOIN_TEAM			 0x3110	 //申请入队 E-〉G
#define E_G_REQ_JOIN_ERROR		     0x3111	 //申请入队错误
#define E_G_FEEDBACK_ERROR	        0x3112  //反馈错误

#define E_G_CHAT_ERROR               0x3113 //聊天错误
#define E_G_ADD_CHAT_GROUP_MEMBER_BY_OWNER 0x3114//查找玩家是否存在
#define E_G_IRC_QUERY_PLAYERINFO 0x3115  
#define E_G_IRC_QUERY_PLAYERINFO_RST  0x3116

//天谴相关
#define E_G_CHECK_PUNISHMENT_RESULT 0x3117	//Center经过验证告之是否需要启动天谴

//GM工具相关
#define E_G_GMCMD_REQUEST 0x3118		//center转发至gatesrv
#define E_G_GMCMD_RESULT  0x3119		//center转发给gate告知使用者结果

//#define E_G_RACE_GROUP_NOTIFY 0x311a	//种族转发

#define E_G_NOTICE_PLAYER_HAVE_UNREAD_MAIL 0x311b //玩家有未读邮件
#define E_G_NOTIFY_ACTIVITY_STATE 0x311c	//全服通知活动状态
#define E_G_EXCEPTION_NOTIFY	0x311d		//CenterSrv通知异常
#define E_G_GMQUERY_TARGET_CHECK_RST 0x311e

#define E_G_NEW_COPY_CMD                 0x311f //通知创建新副本；
#define E_G_DEL_COPY_CMD                 0x3120 //通知删除旧副本；
#define E_G_DEL_COPY_QUEST               0x3121 //centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
#define E_G_COPY_RES_RST                 0x3122 //返回给gate的副本相关操作结果
#define E_G_ENTER_COPY_CMD               0x3123 //指令玩家进入副本；
#define E_G_LEAVE_COPY_CMD               0x3124 //指令玩家离开副本；
#define E_G_LEAVE_STEAM_NOTI                  0x3125 //通知玩家离开副本组队(删去玩家在各副本中的第一次进入信息，并从当前副本中踢出去)

#define E_G_GLOBE_COPYINFOS              0x1330 //发往gate的全局副本信息查询结果；
#define E_G_TEAM_COPYINFOS               0x1331 //发往center的组队副本信息查询结果；

#define E_G_ENTER_PWMAP_CHECK_RST			0x3126	//进入伪线地图前的检查结果
#define E_G_MODIFY_PW_STATE_RST				0x3127	//修改伪线状态的结果
#define E_G_MODIFY_PW_STATE_NTF				0x3128	//修改伪线状态的通知
#define E_G_MODIFY_PWMAP_STATE_RST			0x3129	//修改伪线地图状态的结果
#define E_G_MODIFY_PWMAP_STATE_NTF			0x312a	//修改伪线地图状态的通知
#define E_G_PWMAP_PLAYER_NUM_NTF			0x312b	//通知伪线地图的最新人数

	///...centersrv发往gatesrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往dbsrv的消息命令字,形式0xXXXX...
#define G_D_PLAYER_INFO_LOGIN 0x1700  //gatesrv发往dbsrv,登录获取玩家角色列表
#define G_D_CHOOSE_PLAYER_INFO 0x1701 //gatesrv发往dbsrv,选择一个角色
#define G_D_NEW_PLAYER_INFO 0x1702 //gatesrv发往dbsrv,新增一个角色
#define G_D_UPDATE_PLAYER_INFO 0x1703 //gatesrv发往dbsrv,更新一个角色
#define G_D_DELETE_PLAYER_INFO 0x1704 //gatesrv发往dbsrv,删除一个角色
#define G_D_PLAYER_INFO 0x1705  //gatesrv发往dbsrv,请求玩家角色具体信息
#define G_D_UPDATE_PLAYER_PKG 0x1706 //gatesrv发往dbsrv,更新玩家包裹；
#define G_D_BATCH_ITEM_SEQUENCE 0x1707 //获取批量物品流水号
#define G_D_UPDATE_ITEM_INFO 0x1708//更新道具信息
#define G_D_GET_CLIENT_DATA 0x1709//获取客户信息
#define G_D_SAVE_CLIENT_DATA 0x170a//保存客户信息
#define G_D_QUERY_PLAYER_EXIST 0x170b //查询接受邮件的玩家信息
#define G_D_QUERY_SUIT_INFO 0x170c //请求套装信息
#define G_D_CLEAR_SUIT_INFO 0x170d //清空套装信息
#define G_D_SAVE_SUIT_INFO  0x170e //保存套装信息
#define G_D_REQ_CITY_INFO   0x170f	//单个mapsrv申请管理地图中的关卡信息
#define G_D_UPDATE_CITY_INFO 0x1710	 //更新关卡地图信息
#define G_D_UPDATE_TASK_INFO 0x1712  //更新对应任务栏位的信息
#define G_D_HEARTBEAT_CHECK_RST  0x1713  //心跳检查
#define G_D_UPDATE_PLAYER_BASE_INFO 0x1714 //更新玩家的可变的base信息
#define G_D_QUERY_OFFLINE_PLAYER_INFO_REQ 0x1715 //查询非在线玩家的信息

///...gatesrv发往dbsrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///dbsrv发往gatesrv的消息命令字,形式0xXXXX...
#define D_G_PLAYER_INFO_LOGIN 0x7100  //db
#define D_G_CHOOSE_PLAYER_INFO_RETURN 0x7101 //dbsrv发往gatesrv,返回选择角色的详细信息
#define D_G_NEW_PLAYR_INFO_RETURN 0x7102 //dbsrv发往gatesrv,返回新增角色的结果
#define D_G_UPDATE_PLAYER_INFO_RETURN 0x7103 //dbsrv发往gatesrv,返回更新角色信息的结果
#define D_G_DELETE_PLAYER_INFO_RETURN 0x7104 //dbsrv发往gatesrv,返回删除角色信息的结果
#define D_G_PLAYER_INFO       0x7105  //dbsrv发往gatesrv,返回玩家角色具体信息；
#define D_G_PLAYER_PKG        0x7106  //玩家角色包裹信息；
#define D_G_BATCH_ITEM_SEQUENCE 0x7107 //获取批量物品流水号
#define D_G_ITEM_INFO 0x7108 //道具信息
#define D_G_UPDATE_ITEM_INFO 0x7109//更新道具信息
#define D_G_GET_CLIENT_DATA 0x710a//获取客户信息
#define D_G_QUERY_PLAYER_EXIST_RST 0x710b//返回的邮件接受着的玩家信息
#define D_G_CITY_INFO_RESULT	0x710c	//返回与mapsrv管理地图相对应的city信息
#define D_G_HEARTBEAT_CHECK_REQ 0x710d	//心跳检查
#define D_G_EXCEPTION_NOTIFY	0x710e	//异常通知
#define D_G_QUREY_OFFLINE_PLAYER_INFO_RST 0x710f //查询非在线玩家信息的结果
	///...dbsrv发往gatesrv的消息命令字
	//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
///gatesrv发送到排行榜服务器的消息
#define  G_F_RANKPLAYER_ONLINE  0x1d00 //排行榜上的玩家上线，通知排行榜服务器
#define  G_F_RANKPLAYER_OFFLINE 0x1d01 //排行榜上的玩家下线，通知排行榜服务器
#define  G_F_QUERY_RANKCHART_INFO 0x1d02 //查询排行榜的信息
#define  G_F_PUNISHMENT_BEGIN 0x1d03 //通知天谴活动开始
//#define  G_F_RANKCHART_UPDATE 0x1d04 //通知更新排行榜的信息
#define  G_F_OTHER_RANK       0x1d04   //其它服务器生成的排行榜通知functionsrv;
#define  G_F_REMOVE_RANKPLAYER_BYCHARTID 0x1d05 //删除排行榜的玩家
#define  G_F_QUERY_RANKINFO_BYNAME 0x1d06 //依据姓名查询排行榜玩家
#define  G_F_RECVMAIL_PLAYRINFO_RST 0x1d07 //查询邮件接受者的信息返回
#define  G_F_SENDMAIL_TO_PLAER  0x1d08 //发送邮件给玩家 
#define  G_F_DELETE_MAIL 0x1d09 //删除邮件
#define  G_F_QUERY_MAILLIST_BYPAGE  0x1d0a//查询邮件列表
#define  G_F_QUERY_MAIL_DETIALINFO  0x1d0b//查询邮件详细信息
#define  G_F_PICKITEM_FROM_MAIL 0x1d0c //拾取邮件道具
#define  G_F_PICKITEM_FROM_MAIL_ERROR 0x1d0d //拾取邮件附件道具错误
#define  G_F_PICKMONEY_FROM_MAIL_ERROR 0x1d0e //拾取邮件金钱错误
#define  G_F_SEND_SYSMAIL_TO_PLAYER    0x1d0f //发送系统邮件主题给玩家
#define  G_F_PICKMONEY_FROM_MAIL 0x1d10 //拾取邮件金钱
#define  G_F_DELETE_ALLMAIL 0x1d11//删除所有的邮件
#define  G_F_LOAD_RANKINFO 0x1d12 //请求排行榜的信息
#define  G_F_UPDATE_RANKPLAYER_LEVEL 0x1d13 //更新排行榜上玩家的等级
#define  G_F_PLAYER_DELETE_ROLE 0x1d14 //当玩家删除角色时
#define  G_F_UPDATE_STORAGE_SAFE_INFO 0x1d15 //更新仓库的安全信息
#define  G_F_UPDATE_STORAGE_ITEM_INFO  0x1d16//更新仓库的道具信息
#define  G_F_UPDATE_STORAGE_FORBIDDEN_INFO 0x1d17//更新仓库的禁用信息
#define  G_F_QUERY_STORAGE_INFO 0x1d18 //查询仓库信息
#define  G_F_QUERY_RACEMASTER_INFO 0x1d19 //查询族长信息
#define  G_F_UPDATE_RACEMASTER_INFO 0x1d1a //更新族长的信息
#define  G_F_NEW_RACEMASTER_BORN 0x1d1b //新的族长诞生
#define  G_F_RESET_RACEMASTER_DECLAREWARINFO 0x1d1c //重置族长的宣战信息
#define  G_F_LOAD_NEW_PUBLICNOTICE  0xd1dd//加载新的公告信息
#define  G_F_ADD_NEW_PUBLICNOTICE 0xd1de//插入新的公告信息
#define  G_F_MODIFY_PUBLICNOTICE 0xd1df//修改公告
#define  G_F_DELETE_PUBLICNOTICE 0xd1e0//删除公告
#define  G_F_QUERY_ALL_PUBLICNOTICE 0xd1e1//加载所有公告
#define  G_F_SELECT_RACEMASTER 0xd1e2 //选族长
#define  G_F_PLAYER_START_NEWAUCTION 0xd1e3 //开始新的拍卖
#define  G_F_PLAYER_BID_ITEM_SECTION1   0xd1e4 //玩家竞拍物品第一阶段
#define  G_F_PLAYER_BID_ITEM_SECTION2   0xd1e5 //玩家竞拍物品第二阶段
#define  G_F_PLAYER_CANCEL_AUCTION      0xd1e6 //玩家取消竞拍
#define  G_F_PLAYER_QUERY_AUCTION       0xd1e7 //玩家查询拍卖情况
#define  G_F_PLAERY_QUERY_SELF_AUCTION  0xd1e8 //玩家查询自身的拍卖情况
#define  G_F_PLAYER_QUERY_SELF_BID      0xd1e9 //玩家查询自身的竞拍情况
#define  G_F_RANK_CACHE_INFO            0xd1ea //影响排行榜的新缓存信息；

//排行榜服务器发往gatesrv的消息
#define  F_G_RANKCHART_INFO   0xd100 //排行榜发往gatesrv的排行榜信息
#define  F_G_RANKCHART_REFRESH 0xd101 //通知mapsrv清空对应的排行榜信息
//#define  F_G_RANKCHARTINFO_BYPAGE 0xd102 //通知对应排行榜页数的信息
#define  F_G_NEWRANKINFO          0xd102 //新的排行榜信息通知；
#define  F_G_PUNISHMENT_PLAYER    0xd103 //通知天谴的玩家
#define  F_G_REFRESH_RANKCHART    0xd104 //通知map清空排行榜信息
#define  F_G_RANKCHARTINFO_BYNAME 0xd105 //返回依据姓名查询的排行榜信息
#define  F_G_REMOVE_RANKPLAYER    0xd106 //通知Map删除排行中的玩家
#define  F_G_QUERYMAIL_RECVPLAYER_INFO 0xd107 //查询接收玩家的信息
#define  F_G_QUERYMAILLIST_BYPAGE_RST  0xd108 //查询玩家的邮件返回
#define  F_G_QUERY_MAIL_DETIALINFO_RST 0xd109 //查询的邮件的详细信息
#define  F_G_QUERY_MAIL_ATTACHINFO_RST 0xd10a //查询玩家的邮件附件信息
#define  F_G_SENDMIAL_FAIL 0xd10b //发送邮件失败
#define  F_G_DELETEMAIL_RESULT 0xd10c//删除邮件的结果
#define  F_G_PICK_MAIL_ATTACH_MONEY 0xd10d //玩家拾取邮件附件中的金钱
#define  F_G_PICK_MAIL_ATTACH_ITEM  0xd10e //玩家拾取邮件附件中的道具
#define  F_G_NOTICE_PLAYER_HAVE_UNREADMAIL 0xd10f //检测玩家是否有未读邮件
#define  F_G_PLAYER_UNREADMAIL_COUNT_UPDATE 0xd110 //通知玩家的未读邮件数目更新
#define  F_G_EXCEPTION_NOTIFY  0xd111			//异常通知
#define  F_G_STORAGE_SAFE_INFO 0xd112           //仓库的安全信息
#define  F_G_STORAGE_ITEM_INFO 0xd113           //仓库的道具信息
#define  F_G_STORAGE_FORBIDDEN_INFO 0xd114      //仓库的禁用信息
#define  F_G_NEW_STROAGE_NOTIFY 0xd115          //第一次使用仓库的通知
#define  F_G_QUERY_RACEMASTER_INFO_RST 0xd116   //查询族长信息的相关返回
#define  F_G_START_SELECT_RACEMASTER  0xd117    //开始选族长
#define  F_G_FORBIDDEN_RACEMASTER_AUTHORITY 0xd118 //禁止族长权利
#define  F_G_SENDMAIL_SUCCESS_NOTI_GM  0xd119 //发送邮件成功，通知GM
#define  F_G_RECV_PUBLICNOTICE 0xd11a//接受公告信息
#define  F_G_MODIFY_PUBLICNOTICE 0xd11b//修改公告信息
#define  F_G_ADD_NEW_PUBLICNOTICE 0xd11c//增加公告信息
#define  F_G_REMOVE_PUBLICNOTICE 0xd11d//删除公告信息
#define  F_G_PLAYER_BIDITEM_RST  0xd11e//竞拍物品的结果
#define  F_G_CANCELAUCTION_RST   0xd11f//取消拍卖的结果
#define  F_G_QUERY_MYAUCTION_RST 0xd120//查询自身拍卖的结果
#define  F_G_QUERY_MYBID_RST    0xd121//查询自身的竞拍
#define  F_G_QUERY_AUCTION_BYCONDITION_RST 0xd122//依据条件查询拍卖的物品
#define  F_G_PLAYER_BIDITEM_SECTION1 0xd123//竞拍物品的阶段1

#define  F_G_BIAS_INFO               0xd124 //排行榜阈值信息
#define  F_G_HONOR_PLAYER            0xd125 //称号拥有者信息

//////////////////////////////////////////////////////////////////////////

	/**
	*   第三部分：
	*   以下为通信消息结构
	*   需要在每个结构之前注明该结构对应消息的命令字，同时，对于结构的每个字段，都分别注明该字段的物理意义
	*   同时，对于结构的每个字段，都分别注明该字段的物理意义
	*/

	//待缓存的排行数据信息
	struct RankCacheRcd
	{
		RankCacheRcd()
		{
			playerUID = 0;
			//shFlag = 0;
			rankSeqID = 0;
			enterMapSeq = 0;
			//playerClass = 0;//玩家职业
			//playerLevel = 0;//玩家等级
			//rankInfo = 0;//排行值
			//StructMemSet( playerName, 0, sizeof(playerName) );//排行榜玩家姓名
			//extraInfo = 0;
			StructMemSet( rankItemInfo, 0, sizeof(rankItemInfo) );
			rankItemInfo.nameLen = ARRAY_SIZE(rankItemInfo.playerName) - 1;
		}

		//从mysql的结果集中初始化自身数据；
		bool DataFromDbField( const char* inPUID, const char* inPClass, const char* inPLevel
			, const char* inRankInfo, const char* inPName, const char* inMSeq
			, const char* inRankSeqID, const char* inExtraInfo )
		{
			if ( ( NULL == inPUID ) || ( NULL == inPClass ) || ( NULL == inPLevel ) 
				|| ( NULL == inRankInfo ) || ( NULL == inPName ) || ( NULL == inMSeq ) 
				|| ( NULL == inRankSeqID ) || ( NULL == inExtraInfo) )
			{
				return false;
			}
			playerUID = (unsigned int)atoi( inPUID );
			enterMapSeq = (unsigned int)atoi(inMSeq);//玩家身上计数，进入地图序号，表明此信息产生的先后，序号越大越新(但有可能越过unsigned int阈值,因此不能简单比较大小);
			rankSeqID = (unsigned int)atoi(inRankSeqID);//单排行榜计数，每次该排行榜刷新时++;
			rankItemInfo.playerClass = (unsigned char)atoi( inPClass );//玩家职业
			rankItemInfo.playerLevel = (unsigned char)atoi( inPLevel );//玩家等级
			rankItemInfo.rankInfo = atoi( inRankInfo );//排行值
			SafeStrCpy( rankItemInfo.playerName, inPName );
			rankItemInfo.nameLen = ARRAY_SIZE(rankItemInfo.playerName) - 1;
			rankItemInfo.extraInfo = (unsigned int)atoi(inExtraInfo);
			return true;
		}
		
		bool DataFrom( const RankCacheRcd& inCacheInfo )
		{
			StructMemCpy( *this, &inCacheInfo, sizeof(*this) );
			return true;
		}

		//compareInfo是否来自比本信息更新的地图，mapsrv不需要调用，因为同一mapsrv对同一玩家总是按先后顺序进缓存，因此后到信息总是更新些；
		bool IsNewerMapInfo( const RankCacheRcd& compareInfo )
		{
			return ( ( compareInfo.enterMapSeq - enterMapSeq ) < 10000 );//unsigned int的比较，在短时间内的两次比较数据之间，同一玩家不可能进出地图达到10000次；
		}

		//取性别标记，此信息存于extrainfo的最低一位
		bool IsFemale()
		{
			return ( ( rankItemInfo.extraInfo & (~(0x00000001)) ) > 0 );
		}
		//置性别标记，使用extrainfo中的最低一位保存此信息
		bool SetIsFemale( bool isFemale/*是否女性*/)
		{
			if ( isFemale )
			{
				rankItemInfo.extraInfo |= 0x01;//置最末位；
			} else {
				rankItemInfo.extraInfo &= (~(0x00000001));//清最末位；
			}
			return true;
		}

		unsigned int GetRankSeqID() { return rankSeqID; };

		unsigned int      playerUID;
		//unsigned char     shFlag;//玩家shFlag;
		unsigned int      enterMapSeq;//玩家身上计数，进入地图序号，表明此信息产生的先后，序号越大越新(但有可能越过unsigned int阈值,因此不能简单比较大小);
		unsigned int      rankSeqID;//单排行榜计数，每次该排行榜刷新时++;
		//unsigned short    playerClass;//玩家职业
		//unsigned short    playerLevel;//玩家等级
		//int               rankInfo;//排行值
		//unsigned int      extraInfo;//额外值，暂时只有用于等级排行榜的入榜时间
		//char              playerName[MAX_NAME_SIZE];//排行榜玩家昵称
		NewRankItem       rankItemInfo;
	};//struct RankPlayerInfo


	///进入副本请求结构
	struct EnterCopyReq
	{
		PlayerID       playerID;
		char           szAccount[MAX_NAME_SIZE];		//帐号信息
		unsigned int   teamID;//请求者所在队伍；
		unsigned short copyMapID;//副本地图ID号；
		unsigned short mapSrvID;//请求者所在mapsrv；
		unsigned int   scrollType;//进入卷轴类型；
	};

	///副本管理器中的元素结构
	struct CopyManagerEle
	{
		unsigned short mapsrvID;//副本所在mapsrv的ID号；
		unsigned int   copyID;//副本ID号，全局唯一；
		EnterCopyReq   enterReq;//创建者的进入请求；
		unsigned int   createTime;//创建时间；
		unsigned short curPlayerNum;//当前副本中人数；
		bool           isNoMorePlayer;//是否不允许更多玩家进入；
	};    

	struct BuffDataInfo
	{
		unsigned int buffID;
		unsigned int buffTime;
	};

    ///玩家死活状态信息
	struct PlayerStateInfo
	{
	public:
		explicit PlayerStateInfo(){}

		///重置；
		void ResetPlayerStateInfo()
		{
			m_CurBaseState = MAP_PS_INVALID;
		}
		///上线初始化;
		void OnlineProc( CPlayer* pPlayer, unsigned long& dyingleft );
		void CheckWeakBuff( CPlayer* pPlayer );

		///置活状态；
		void SetAlive()
		{
			m_CurBaseState = MAP_PS_ALIVE;
			m_StateStTick = time(NULL);
			if(m_ForWarDie)
				m_ForWarDie = false;
		}
		///置死亡状态
		void SetDie( CPlayer* pPlayer );
		//置虚弱状态
		void SetWeak( unsigned int weakTime );		

		void SetWaitClient()
		{
			m_CurBaseState = MAP_PS_WAITING_CLIENT;
		}
		void SetOffline()
		{
			m_StatePauseTick = time(NULL);//下线时，状态持续时间中断计数；
		}

		inline I64& GetStateEndTick() { return m_StateEndTick; }

		bool ForWarDie(void)
		{
			if((MAP_PS_DIE == m_CurBaseState) && m_ForWarDie)
				return true;
			else
				return false;
		}

		void ForWarDie(bool forWar){ m_ForWarDie = forWar;}

		const I64& GetOnlineTick(){return m_OnlineTick;}
		const I64& GetOffLineTick() {return m_StatePauseTick;}

		//记录被杀次数++;
		inline bool IncBeKillTimes( unsigned int beKillRankSeq )
		{
			if ( beKillRankSeq != m_beKillRankSeq )
			{
				m_beKillTimes = 0;//重置死亡次数记录
				m_beKillRankSeq = beKillRankSeq;
			}
			++m_beKillTimes;
			return true;
		}

		//取被杀次数记录；
		inline unsigned int GetBeKillTimes( unsigned int beKillRankSeq ) 
		{ 
			if ( beKillRankSeq != m_beKillRankSeq )
			{
				m_beKillTimes = 0;
				m_beKillRankSeq = beKillRankSeq;
			}
			return m_beKillTimes; 
		}

		inline unsigned int GetBeKillTimesSeq()
		{
			return m_beKillRankSeq;
		}

		//记录击杀次数++;
		inline bool IncKillTimes( unsigned int killRankSeq )
		{
			if ( killRankSeq != m_killRankSeq )
			{
				m_killTimes = 0;//重置死亡次数记录
				m_killRankSeq = killRankSeq;
			}
			++m_killTimes;
			return true;
		}

		//取击杀次数记录；
		inline unsigned int GetKillTimes( unsigned int killRankSeq ) 
		{ 
			if ( killRankSeq != m_killRankSeq )
			{
				m_killTimes = 0;
				m_killRankSeq = killRankSeq;
			}
			return m_killTimes; 
		}

		inline unsigned int GetKillTimesSeq()
		{
			return m_killRankSeq;
		}

		//可能的新的最大输出伤害
		inline bool NewOutDamage( unsigned int curRankSeq, unsigned int curOutDamage )
		{
			if ( curRankSeq != m_damageRankSeq )
			{
				m_maxOutDamage = 0;//重置最大输出伤害;
				m_damageRankSeq = curRankSeq;
			}
			if ( curOutDamage >= m_maxOutDamage )
			{
				m_maxOutDamage = curOutDamage;
			}
			return true;
		}

		//取最大输出伤害；
		inline unsigned int GetMaxOutDamage( unsigned int curRankSeq ) 
		{ 
			if ( curRankSeq != m_damageRankSeq )
			{
				m_maxOutDamage = 0;////重置最大输出伤害;
				m_damageRankSeq = curRankSeq;
			}
			return m_maxOutDamage; 
		}

		inline unsigned int GetMaxOutDamageSeq()
		{
			return m_damageRankSeq;
		}

	public:	
		EPlayerBaseState m_CurBaseState;//当前基本状态(若m_CurBaseState为PS_ALIVE，则后续字段皆无效)；
		I64				 m_OnlineTick;//上线时刻;
		I64				 m_StateStTick;//本状态开始时刻；
		I64				 m_StateEndTick;//本状态预计结束时刻(若m_CurBaseState为PS_ALIVE，则本字段无效)；
		I64				 m_StatePauseTick;//本状态的中断时刻(每次下线时将下线时刻记为m_StatePauseTick, 再次上线后根据本时刻重新计算m_StateEndTick)(若m_CurBaseState为PS_ALIVE，则本字段不发生作用)；
		bool			 m_ForWarDie;//"攻城战死亡"标记
		unsigned int     m_enterMapSeq;//进地图次数序号，玩家每次进地图时++，用于比较不同地图来源信息的最新程度
		unsigned int     m_beKillTimes;//被杀次数;
		unsigned int     m_beKillRankSeq;//m_beKillTimes对应的排行榜序号，每次functionsrv重刷排行榜时，此序号增一并广播给所有的服务器;
		unsigned int     m_killTimes;//击杀次数;
		unsigned int     m_killRankSeq;//m_killTimes对应的排行榜序号，每次functionsrv重刷排行榜时，此序号增一并广播给所有的服务器;
		unsigned int     m_maxOutDamage;//最大输出伤害；
		unsigned int     m_damageRankSeq;//对应最大输出伤害的排行榜序号
	};

	typedef struct StrPlayerRollResult
	{
		PlayerID rollPlayer;
		USHORT rollNum;
	}PlayerRollResult;

	typedef struct StrKillInfo
	{
		unsigned int roleUID;		//角色唯一ID
		unsigned int killCount;		//计数
	}KillInfo;

	typedef struct StrPunishTaskInfo
	{
		int killCounter;
		bool isHavePunishTask;
	}PunishTaskInfo;

	typedef struct StrCityInfo
	{
		unsigned int uiCityID;		//城市的ID
		unsigned int uiMapID;		//城市归属的mapID
		unsigned short cityLevel;	//城市的等级
		unsigned int cityExp;		//城市的经验
		unsigned char ucRace;		//该城市归属的种族
		unsigned char ucType;		//可争夺，不可争夺
	}CityInfo;

	typedef struct StrPunishPlayerInfo
	{
		UINT playerUID;		
		char name[MAX_NAME_SIZE];
		unsigned long mapID;
		unsigned short posX;
		unsigned short posY;
		bool bPunished;
		int punishTime;
	}PunishPlayerInfo;

	typedef struct StrInvitePlayerInfo
	{
		PlayerID invitePlayerID;					//邀请的玩家ID	
		char invitePlayerName[MAX_NAME_SIZE];		//邀请玩家的姓名
		int  invitePlayerNameLen;					//邀请玩家的姓名
		TYPE_UI16 invitePlayerRace;					//邀请玩家的种族
		bool isEvil;								//邀请玩家是否是魔头
	}InvitePlayerInfo;

	typedef struct StrPunishRaceInfo
	{
		PunishPlayerInfo player[3];
		int playerNum;
	}PunishRaceInfo;

	typedef struct StrWarTaskInfo
	{
		unsigned int warTaskId;			//攻城任务号
		ETaskStatType taskState;		//攻城任务状态 

		StrWarTaskInfo(){}
		StrWarTaskInfo( unsigned int inWarTaskId, ETaskStatType inTaskState ) : warTaskId( inWarTaskId ), taskState( inTaskState ){}
	}WarTaskInfo;

	/// 包裹信息
	typedef struct StrPlayerInfo_3_Pkgs
	{
		enum{INVALIDATE};
		ItemInfo_i pkgs[PACKAGE_SIZE];
	}PlayerInfo_3_Pkgs;

	/// 完整的玩家信息
	typedef struct StrFullPlayerInfo	  
	{
		PlayerInfo_1_Base baseInfo;
		PlayerInfo_2_Equips equipInfo;
		PlayerInfo_3_Pkgs pkgInfo;
		PlayerInfo_4_Skills skillInfo;
		PlayerInfo_5_Buffers bufferInfo;
		PlayerStateInfo stateInfo;
		TaskRecordInfo  taskInfo[MAX_TASKRD_SIZE];
	}FullPlayerInfo;
	
		
	template<typename T_Pkg,typename T_Str>
	bool ReceiveFullStr(const T_Pkg* pPkg, T_Str* pTStruct)
	{
		unsigned int nShouldRcvTotalLen = sizeof( *pTStruct );
		unsigned int curRcved = pPkg->ucIndex * sizeof( pPkg->pszData );
		if ( pPkg->usSendByte > sizeof( pPkg->pszData ) )
		{
			D_ERROR("pPkg->usSendByte > sizeof( pPkg->pszData )\n");
			return false;
		}
		if ( nShouldRcvTotalLen < curRcved + pPkg->usSendByte )
		{
			//D_ERROR( "ReceiveFullStr，已经收了足够多的信息\n" );
			return false;
		}

		memcpy( ((char*)pTStruct)+curRcved, pPkg->pszData, min( sizeof(*pTStruct)-curRcved, sizeof(pPkg->pszData) ) );

		if ( !pPkg->bEnd )
		{
			return false;
		}
		return true;
	}


	template<typename StrType, typename PkgType>
	void SendFullStr( StrType* pTStruct, vector<PkgType>& vecTPkg)
	{
		//暂存此状态，准备恢复，这样做很傻，临时用吧，以后再改成将waiting client状态单独建变量保存在mapsrv，而不放到存盘数据中；
		EPlayerBaseState orgState = pTStruct->stateInfo.m_CurBaseState;
		if ( MAP_PS_WAITING_CLIENT == orgState )
		{
			pTStruct->stateInfo.m_CurBaseState = MAP_PS_ALIVE;//如果客户端因为某种原因没有发预期响应就断了，再上线时认为该客户端为ALIVE；
		}
		vecTPkg.clear();
		unsigned char ucIndex = 0;
		int iLeft = sizeof(StrType);
		const char *pDataOffset = (const char *)pTStruct;
		while(iLeft > 0)
		{
			PkgType tmpPkg;
			tmpPkg.ucIndex = ucIndex;
			
			if( (unsigned int)iLeft <= sizeof(tmpPkg.pszData))
			{
				StructMemCpy(tmpPkg.pszData, pDataOffset, iLeft);
				tmpPkg.usSendByte = iLeft;
				tmpPkg.bEnd = true;
			} else {
				StructMemCpy(tmpPkg.pszData, pDataOffset, sizeof(tmpPkg.pszData));
				tmpPkg.usSendByte = sizeof(tmpPkg.pszData);
				tmpPkg.bEnd = false;
			}

			iLeft = iLeft - sizeof(tmpPkg.pszData);
			pDataOffset = pDataOffset + sizeof(tmpPkg.pszData);
			ucIndex++;

			vecTPkg.push_back(tmpPkg);
		}
		pTStruct->stateInfo.m_CurBaseState = orgState;//在mapsrv端总是保存任何状态，而不是每存一次DB就作改掉之前的状态；
	}

	typedef struct strNeedRepairedItems
	{
		ItemInfo_i *pItem;
		unsigned int newWear;
		unsigned int costMoney;
	}NeedRepairedItems;

	typedef struct strUnionCostArg
	{
		unsigned int money;
		unsigned int itemID;
		unsigned int itemCount;
	}UnionCostArg;

	typedef struct strPseudowireArg_i//伪线参数
	{
		enum ePWState
		{
			PM_CLOSE = 0,//关闭
			PM_OPEN = 1,//开启
			PM_FORBIT = 2//禁入
		};

		unsigned short idx;//伪线索引
		char name[20];//伪线名字
		ePWState state;//伪线状态
	} PseudowireArg_i;

	typedef struct strPseudowireMapArg_i//伪线地图参数
	{
		enum ePWMapState
		{
			PWM_UNACTIVE = 0,//未激活(缺省状态,任何玩家不可以进入)
			PWM_ACTIVE = 1,//激活(满足开启条件时新伪线地图进入此状态，此状态下大部分玩家可以进入，但到达一定数目后会依据玩家等级判断是否能够进入，或者开启新的伪线地图)
			PWM_FORBIT = 2,//禁入(物理禁入，任何玩家不可再进入)
		};

		unsigned short pwid;//伪线id
		unsigned short mapid;//伪线地图id
		unsigned short newCondition;//开启新伪线地图条件
		unsigned short poolLimit;//普通玩家准入极限
		unsigned short vipLimit;//vip玩家准入极限
		ePWMapState state;//伪线地图状态
	} PseudowireMapArg_i;


	typedef struct strPseudowireData//伪线数据
	{
		PseudowireArg_i psArg;//参数
		time_t lastTimeOfStateChange;//最后一次状态改变发生时间
	} PseudowireData;

	typedef struct strPseudowireMapData//伪线地图数据
	{
		PseudowireMapArg_i pwMapArg;//伪线地图参数
		unsigned short currenPlayerNum;//伪线地图当前玩家数目
		time_t lastTimeOfStateChange;//最后一次状态改变发生时间
	}PseudowireMapData;

	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往centersrv的消息命令字,形式0x13XX...
    //#define G_E_REPORT_SELFID  0x1309        //gatesrv发往centersrv，通报自身SELF_SRV_ID；
	typedef struct StrGEReportSelfID
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_REPORT_SELFID;
		unsigned long selfSrvID;
	}GEReportSelfID;
	MSG_SIZE_GUARD(GEReportSelfID);   

	//#define G_E_PLAYER_ISEXIST 0x1301  //gatesrv发往centersrv玩家选角色；
	typedef struct StrGEPlayerIsExist
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_PLAYER_ISEXIST;
		PlayerID playerID;//玩家唯一标识号，GateSrv生成；
		char szAccount[MAX_NAME_SIZE];		//帐号信息
		int  accountLen;		//帐号长度
		TYPE_ID sequenceID;
	}GEPlayerIsExist;
	MSG_SIZE_GUARD(GEPlayerIsExist);   

	//#define G_E_INVITE_PLAYER_JOIN_TEAM   //gatesrv发往centersrv邀请组队消息 0x1303 
	typedef struct StrGEInvitePlayerJoinTeam
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_INVITE_PLAYER_JOIN_TEAM;
		char beInvitedPlayerName[MAX_NAME_SIZE];		//被邀请玩家的姓名
		int  beInvitedPlayerNameLen;		//被邀请玩家的长度
		InvitePlayerInfo inviterInfo;		//邀请人信息
	}GEInvitePlayerJoinTeam;
	MSG_SIZE_GUARD(GEInvitePlayerJoinTeam);   


	//#define G_E_PLAYER_MAPINFO 0x1304  //gatesrv发往centersrv角色所在的mapsrvID ,当登陆成功以后发送
	typedef struct StrGEPlayerMapInfo
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_PLAYER_MAPINFO;
		char szPlayerName[32];		//玩家姓名
		int  nameLen;				//姓名长度
		PlayerID playerID;			//玩家唯一标识号，GateSrv生成；
		char	szAccount[/*MAX_NAME_SIZE*/32];
		int    accountLen;
		USHORT	mapID;		//该玩家所在的mapsrvID 
	}GEPlayerMapInfo;

	//#define G_E_PLAYER_OFFLINE 0x1302  //gatesrv发往centersrv玩家下线消息；
	typedef struct StrGEPlayerOffline
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_PLAYER_OFFLINE;
		PlayerID playerID;//玩家唯一标识号，GateSrv生成；
		char szAccount[MAX_NAME_SIZE];
	}GEPlayerOffline;
	MSG_SIZE_GUARD(GEPlayerOffline);   

	//#define G_E_INVITE_ERROR   0x1305		   //gatesrv发送给centresrv错误消息
	typedef struct StrGEInviteError
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = G_E_INVITE_ERROR;
		EInviteTeamResult nErrorCode;					//错误码
		PlayerID invitePlayerID;		//邀请的玩家ID
		char beInvitedPlayerName[MAX_NAME_SIZE];	//被邀请的玩家姓名
		int  beInvitedPlayerNameLen;	//被邀请姓名的长度
	}GEInviteError;
	MSG_SIZE_GUARD(GEInviteError);   

	//#define G_E_INVITE_SUCCESS  0x1306        //gatesrv发送给centersrv成功消息
	typedef struct StrGEInviteSuccess
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_INVITE_SUCCESS;
		Member beInvitedPlayer;		//被邀请玩家的信息
		PlayerID invitePlayerID;	//邀请玩家的ID
	}GEInviteSuccess;
	MSG_SIZE_GUARD(GEInviteSuccess); 

	//#define G_E_REQ_JOIN_TEAM  0x1320		  //gatesrv发给censrv转发消息，申请入队
	typedef struct StrGEReqJoinTeam
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_REQ_JOIN_TEAM;
		Member reqPlayer;					//申请加入玩家的信息
		bool   isReqPlayerEvil;				//申请加入的玩家是否是魔头
		char captainPlayerName[MAX_NAME_SIZE];			//队长的姓名
		int  captainPlayerNameLen;			//队长的姓名长度
	}GEReqJoinTeam;
	MSG_SIZE_GUARD(GEReqJoinTeam); 


	//#define G_E_REQ_JOIN_ERROR  0x1321       //gatesrv返回错误
	typedef struct StrGEReqJoinError
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_REQ_JOIN_ERROR;
		EInviteTeamResult nErrorCode;						//错误原因
		PlayerID reqPlayerID;				//邀请玩家的ID
		char captainPlayerName[MAX_NAME_SIZE];			//队长的姓名
		int  captainPlayerNameLen;			//队长的姓名长度
	}GEReqJoinError;
	MSG_SIZE_GUARD(GEReqJoinError); 

	//#define G_E_FEEDBACK_ERROR  0x1322	      //由于邀请人员下线需要的提示
	typedef struct StrGEFeedbackError
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_FEEDBACK_ERROR;
		PlayerID lNotiPlayerID;				//要通知玩家的ID
		EInviteTeamResult nErrorCode;		//错误原因,按了确定，仍然需要的提示,目前确定为需要提示
	}GEFeedbackError;
	MSG_SIZE_GUARD(GEFeedbackError); 


	
    //#define G_E_SRVGROUP_CHAT  0x1307        //gatesrv发往centersrv全服(世界)聊天消息，center收到后应广播给所有的gate；
	typedef struct StrGESrvGroupChat
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_SRVGROUP_CHAT;
		char   chatPlayerName[MAX_NAME_SIZE]; //说话者名字;
		char   chatContent[256];              //说话内容;
		CHAT_TYPE chatType;				//区分IRC世界聊天还是游戏世界的世界聊天
		unsigned int extInfo;           //额外信息，例如种聊消息时的种族
	}GESrvGroupChat;
	MSG_SIZE_GUARD(GESrvGroupChat);

	//#define G_E_PRIVATE_CHAT   0x1308        //gatesrv发往centersrv密聊聊天消息，center收到后应转发给密聊对象所在gate；
	typedef struct StrGEPrivateChat
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_PRIVATE_CHAT;
		char   chatPlayerName[MAX_NAME_SIZE]; //说话者名字;
		char   tgtPlayerName[MAX_NAME_SIZE];  //说话对象名字;
		char   chatContent[256];   //说话内容;
		CHAT_TYPE chatType;			//区分是IRC私聊还是游戏世界的私聊 
	}GEPrivateChat;
	MSG_SIZE_GUARD(GEPrivateChat);

	//通知centersrv玩家角色上线(选定某角色)
	typedef struct StrGEPlayerRoleOnLine
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_PLAYERROLE_ONLINE;
		char accountName[MAX_NAME_SIZE];
		char playerName[MAX_NAME_SIZE];
		int  playerUID;
		PlayerID playerID;
	}GEPlayerRoleOnLine;
	MSG_SIZE_GUARD(GEPlayerRoleOnLine);

	//加好友
	typedef struct StrGEAddFriend
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_ADDFRINED;
		char launchPlayerName[MAX_NAME_SIZE];
		char targetPlayerName[MAX_NAME_SIZE];
		PlayerID luanchPlayerID;//发起者的ID号
		unsigned int groupID;//组队ID
	}GEAddFriend;
	MSG_SIZE_GUARD(GEAddFriend);

	typedef struct StrGEAgreeAddFriendResult
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_REQADDFRIEND_RESULT;
		char                    launchPlayerName[MAX_NAME_SIZE];
		char                    targetPlayerName[MAX_NAME_SIZE];   
		PlayerID            	reqPlayerID;                  	//请求者ID号
		PlayerID                responsePlayerID;               //响应返回者的ID号
		char                	errorCode;                      	//响应者是否同意加好友请求,或者其他错误
		bool                	isAddReqer;                   	//响应者是否同时加请求者为好友
		unsigned int groupID;
	}GEAgreeAddFriendResult;
	MSG_SIZE_GUARD(GEAgreeAddFriendResult);

	////通知RelationSrv玩家下线
	//typedef struct StrGEPlayerOffLine
	//{
	//	static const unsigned char byPkgType = G_E;
	//	static const unsigned short wCmd = G_E_PLAYER_OFFLINE;
	//	PlayerID playerID;
	//}GEPlayerOffLine;
	//MSG_SIZE_GUARD(GEPlayerOffLine);

	typedef struct StrGEAddChatGroupMemberByOwner
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_ADD_CHAT_GROUP_MEMBER_BY_OWNER;
		UINT                	chatGroupID;
		PlayerID            	reqPlayerID;                  	//请求者ID号
		char                    targetPlayerName[MAX_NAME_SIZE];   
	}GEAddChatGroupMemberByOwner;
	MSG_SIZE_GUARD(GEAddChatGroupMemberByOwner);

	typedef struct StrGEIrcQueryPlayerInfo
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_IRC_QUERY_PLAYERINFO;
		char					targetPlayerName[MAX_NAME_SIZE];
		PlayerID              reqID;			//查询者的ID
	}GEIrcQueryPlayerInfo;
	MSG_SIZE_GUARD(GEIrcQueryPlayerInfo);

	typedef struct StrGEIrcQueryPlayerInfoRst
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_IRC_QUERY_PLAYERINFO_RST;
		
		//Enums
		typedef	enum
		{
			QUERY_SUCCESS         	=	0,            	//
			NOT_IRC_PLAYER        	=	1,            	//
			NOT_FOUND_QUERY_PLAYER	=	2,            	//
		}ItemType;

		PlayerID              reqID;												//查询者的ID
		INT                 	errorCode;                                  	//错误信息
		CHAR                	playerName[MAX_NAME_SIZE];                    	//请求信息的玩家姓名
		BYTE                	playerSex;                                  	//性别 0x01男 0x02女
		USHORT              	mapID;                                      	//玩家所在的地图
		USHORT              	playerLevel;                                	//等级
		USHORT              	playerClass;                                	//职业
		USHORT              	playerRace;                                 	//种族
		CHAR                	GuildName[MAX_NAME_SIZE];                     	//公会伲称
		CHAR                	FereName[MAX_NAME_SIZE];                      	//伴侣伲称
	}GEIrcQueryPlayerInfoRst;
	MSG_SIZE_GUARD(GEIrcQueryPlayerInfoRst);

	//#define G_E_GMCMD_REQUEST	0x1326 //发送至CENTERSRV来验证 GM命令
	typedef struct StrGEGmCmdRequest
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_GMCMD_REQUEST;
		char strChat[256];					//聊天消息内容，包括末尾的\0,具体请求的GM命令
		PlayerID requestPlayerID;			//申请GM命令的玩家
		char targetName[MAX_NAME_SIZE];		//请求的姓名,由需要角色名的GM命令的参数分离出来
	}GEGmCmdRequest;
	MSG_SIZE_GUARD(GEGmCmdRequest);


	//#define G_E_CHECK_PUNISHMENT   //内容可能会改变由于排行榜系统还没完善,送CENTER来决定谁会遭受天谴
	typedef struct StrGECheckPunishment
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_CHECK_PUNISHMENT;
	}GECheckPunishment;
	MSG_SIZE_GUARD(GECheckPunishment);

	////#define G_E_RACE_GROUP_NOTIFY		0x1328		//种族的群发通知
	//typedef struct StrGERaceGroupNotify
	//{
	//	static const unsigned char byPkgType = G_E;
	//	static const unsigned short wCmd = G_E_RACE_GROUP_NOTIFY;
	//	char   chatPlayerName[MAX_NAME_SIZE];	//说话者名字;
	//	char   chatContent[256];              //说话内容;
	//	CHAT_TYPE chatType;						//区分IRC世界聊天还是游戏世界的世界聊天
	//	ERace notifyRace;						//通知的种族
	//}GERaceGroupNotify;
	//MSG_SIZE_GUARD(GERaceGroupNotify);

	typedef struct StrGENoticePlayerHaveUnReadMail
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_NOTICE_PLAYER_HAVE_UNREAD_MAIL;
		char recvPlayerName[MAX_NAME_SIZE];
		unsigned int unreadmailCount;
	}GENoticePlayerHaveUnReadMail;
	MSG_SIZE_GUARD(GENoticePlayerHaveUnReadMail);

	//#define G_E_NOTIFY_ACTIVITY_STATE 0x132a	//全服通知活动状态
	typedef struct StrGENotifyActivityState
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_NOTIFY_ACTIVITY_STATE;
		unsigned int activityType;		//活动类型
		unsigned int activityState;		//活动状态
	}GENotifyActivityState;
	MSG_SIZE_GUARD(GENotifyActivityState);

	typedef struct StrGEGMQueryTargetCheck
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_GMQUERY_TARGET_CHECK;
		PlayerID gmPlayerId;
		GTCmd_Query_PlayerID_Req gmReq;
	}GEGMQueryTargetCheck;
	MSG_SIZE_GUARD(GEGMQueryTargetCheck);

	//#define G_E_PLAYER_TRY_COPY_QUEST 0x132c  //玩家请求进副本；
	typedef struct StrGEPlayerTryCopyQuest
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_PLAYER_TRY_COPY_QUEST;
		EnterCopyReq enterCopyReq;//进入请求内容；
	}GEPlayerTryCopyQuest;
	MSG_SIZE_GUARD(GEPlayerTryCopyQuest);

	//#define G_E_PLAYER_LEAVE_COPY_QUEST 0x132d //玩家请求离开副本；
	typedef struct StrGEPlayerLeaveCopyQuest
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_PLAYER_LEAVE_COPY_QUEST;
		PlayerID leaveCopyPlayerID;//离开副本的玩家ID；
		CopyManagerEle leaveCopyInfo;//欲离开的副本信息(只有部分字段填充，包括：玩家ID号，副本ID号，副本地图号，玩家帐号)；
	}GEPlayerLeaveCopyQuest;
	MSG_SIZE_GUARD(GEPlayerLeaveCopyQuest);

	//#define G_E_COPY_NOMORE_PLAYER           0x132e //通知center,副本不能进更多人了；           
	typedef struct StrGECopyNomorePlayer
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_COPY_NOMORE_PLAYER;
		unsigned int copyid;//不可再进的副本id号；
	}GECopyNomorePlayer;
	MSG_SIZE_GUARD(GECopyNomorePlayer);

	//#define G_E_DEL_COPY_QUEST          0x132f //发往center的删除副本请求(在center发往mapsrv的删除副本请求之后，因为mapsrv收到那个请求后会先踢玩家，踢完后再发此G_E_DEL_COPY_QUEST给center，最后center发delcopy cmd给map)；
	typedef struct StrGEDelCopyQuest
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_DEL_COPY_QUEST;
		unsigned int copyid;//待删副本id号；
	}GEDelCopyQuest;
	MSG_SIZE_GUARD(GEDelCopyQuest);

	//#define G_E_QUERY_GLOBE_COPYINFOS   0x1330 //发往center查询全局副本信息；
	typedef struct StrGEQueryGlobeCopyInfos
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_QUERY_GLOBE_COPYINFOS;
		PlayerID notiPlayerID;//进行查询的玩家ID号，查询结果向此玩家返回;
	}GEQueryGlobeCopyInfos;
	MSG_SIZE_GUARD(GEQueryGlobeCopyInfos);

	//#define G_E_QUERY_TEAM_COPYINFOS    0x1331 //发往center查询组队副本信息；
	typedef struct StrGEQueryTeamCopyInfos
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_QUERY_TEAM_COPYINFOS;
		PlayerID notiPlayerID;//进行查询的玩家ID号，查询结果向此玩家返回;
		unsigned int teamID;
	}GEQueryTeamCopyInfos;
	MSG_SIZE_GUARD(GEQueryTeamCopyInfos);

	//#define G_E_STEAM_DESTORY_CENTERNOTI 0x1332 //gate通知center，副本组队解散；
	typedef struct StrGESTeamDestoryCenterNoti
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_STEAM_DESTORY_CENTERNOTI;
		unsigned int steamID;//副本组队ID号；
	}GESTeamDestoryCenterNoti;
	MSG_SIZE_GUARD(GESTeamDestoryCenterNoti);

	//#define G_E_LEAVE_STEAM_CENTERNOTI   0x1333 //gate通知center,玩家离开副本组队；
	typedef struct StrGELeaveSTeamCenterNoti
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_LEAVE_STEAM_CENTERNOTI;
		PlayerID leavePlayerID;//离开副本组队的玩家号
		unsigned int steamID;//副本组队ID号；
		CHAR       	szNickName[MAX_NAME_SIZE];//离开副本组队的玩家昵称 
	}GELeaveSTeamCenterNoti;
	MSG_SIZE_GUARD(GELeaveSTeamCenterNoti);

	//#define G_E_COPYMAPSRV_NOTI          0x1334 //gate通知center一个新的副本mapsrv出现；
	typedef struct StrGECopyMapsrvNoti
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_COPYMAPSRV_NOTI;
		unsigned short copyMapsrvID;//副本mapsrv的ID号；
	}GECopyMapsrvNoti;
	MSG_SIZE_GUARD(GECopyMapsrvNoti);

	//#define G_E_RESET_COPY               0x1335 //gate通知center重置副本；
	typedef struct StrGEResetCopy
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_E_RESET_COPY;
		PlayerID captainPlayerID;//重置者的ID号；
		unsigned int steamID;//副本组队ID号；
		unsigned short copyMapID;//被重置副本的地图号；		
	}GEResetCopy;
	MSG_SIZE_GUARD(GEResetCopy);

	typedef struct strGEPWBaseInfo
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_PW_BASE_INFO;
		PseudowireArg_i psList[10];
		unsigned char count;
	}GEPWBaseInfo;
	MSG_SIZE_GUARD(GEPWBaseInfo);  

	typedef struct strGEPWMapBaseInfo
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_PWMAP_BASE_INFO;
		PseudowireMapArg_i psList[10];
		unsigned char count;
	}GEPWMapBaseInfo;
	MSG_SIZE_GUARD(GEPWMapBaseInfo);  

	typedef struct strGEEnterPWMapCheck
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_ENTER_PWMAP_CHECK;
		PlayerID   lNoticePlayerID;
		unsigned short pwid;
		unsigned short pwMapid;
		bool isVip;
		bool isShift;
	}GEEnterPWMapCheck;
	MSG_SIZE_GUARD(GEEnterPWMapCheck);  

	typedef struct strGEEnterPWMapErr
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_ENTER_PWMAP_ERR;
		PlayerID   lNoticePlayerID;
		unsigned short pwid;
		unsigned short pwMapid;
	}GEEnterPWMapErr;
	MSG_SIZE_GUARD(GEEnterPWMapErr);  

	typedef struct strGELeavePWMapNtf
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_LEAVE_PWMAP_NTF;
		PlayerID   lNoticePlayerID;
		unsigned short pwid;
		unsigned short pwMapid;
	}GELeavePWMapNtf;
	MSG_SIZE_GUARD(GELeavePWMapNtf);  

	typedef struct strGEModifyPWStateReq
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_MODIFY_PW_STATE_REQ;
		PlayerID   lNoticePlayerID;
		unsigned short pwid;
		unsigned char newState;
	}GEModifyPWStateReq;
	MSG_SIZE_GUARD(GEModifyPWStateReq);  

	typedef struct strGEModifyPWMapStateReq
	{
		static const unsigned char  byPkgType = G_E;
		static const unsigned short wCmd = G_E_MODIFY_PWMAP_STATE_REQ;
		PlayerID   lNoticePlayerID;
		unsigned short pwid;
		unsigned short pwMapid;
		unsigned char newState;
	}GEModifyPWMapStateReq;
	MSG_SIZE_GUARD(GEModifyPWMapStateReq);

	//#define G_E_ROLE_OFFLINE			 0x1340 //gate通知center，玩家角色下线，但账号未下线（重新选择角色）
	typedef struct strGECharacterOffline
	{
		static const unsigned char	byPkgType = G_E;
		static const unsigned short wCmd = G_E_ROLE_OFFLINE;
		PlayerID playerID;//玩家唯一标识号，GateSrv生成；
		char szName[MAX_NAME_SIZE];
	}GERoleOffline;
	MSG_SIZE_GUARD(GERoleOffline);

	///...gatesrv发往centersrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///centersrv发往gatesrv的消息命令字,形式0x31XX...

	//询问是否同意加好友
	typedef struct StrEGReqAgreeAddFriend
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_REQADDFRIEND;
		char luanchPlayerName[MAX_NAME_SIZE];
		char targetPlayerName[MAX_NAME_SIZE];
		PlayerID luanchID;
		PlayerID targetID;
		unsigned int groupID;
	}EGReqAgreeAddFriend;
	MSG_SIZE_GUARD(EGReqAgreeAddFriend);

	typedef struct StrEGAddFriendFailed
	{
		enum
		{
			ADD_FRIEND_OK       	=	0,            	//
			ADD_FRIEND_LIST_FULL	=	1,            	//
			ADD_FRIEND_FOE      	=	2,            	//
			ADD_FRIEND_REFUSED  	=	3,            	//
			ADD_FRIEND_NOT_EXIST	=	4,            	//
		};
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_ADDFRIENDFAILED;
		PlayerID launchPlayer;
		int    errCode;//错误编号
		char   targetPlayerName[MAX_NAME_SIZE];   
	}EGAddFriendFailed;
	MSG_SIZE_GUARD(EGAddFriendFailed);

	typedef struct StrEGAddFriendOK
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_ADDFRIENDSUCCESS;
		PlayerID   lNoticePlayerID;
		int        addFriendUID;
		unsigned int groupID;
	}EGAddFriendOK;
	MSG_SIZE_GUARD(EGAddFriendOK);

	//#define E_G_REQ_JOIN_TEAM			 0x3110		//申请入队 E-〉G
	typedef struct StrEGReqJoinTeam
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_REQ_JOIN_TEAM;
		Member reqPlayer;				//申请的玩家的信息
		bool   isReqPlayerEvil;			//申请玩家是否魔头
		PlayerID captainPlayerID;	    //被邀请的玩家的ID
		char captainPlayerName[MAX_NAME_SIZE];   //队长姓名
		int  captainNameLen;					 //队长姓名长度
	}EGReqJoinTeam;
	MSG_SIZE_GUARD(EGReqJoinTeam);

	//#define E_G_REQ_JOIN_ERROR		     0x3111		//申请入队错误
	typedef struct StrEGReqJoinError
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_REQ_JOIN_ERROR;
		char captainPlayerName[MAX_NAME_SIZE];		//队长的姓名
		int  captainPlayerNameLen;					//队长的姓名长度
		PlayerID reqPlayerID;						//申请玩家的ID
		EInviteTeamResult nErrorCode;								//错误码
	}EGReqJoinError;
	MSG_SIZE_GUARD(EGReqJoinError);

    //#define E_G_PLAYER_ISEXIST 0x3101  //centersrv发往gatesrv玩家选角色；
	typedef struct StrEGPlayerIsExist
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_PLAYER_ISEXIST;
		enum
		{
			UNIQUE_CHECK_OK,//正常通知，选角色成功;
			UNIQUE_CHECK_FAILED,//错误，相应角色已在线，在onlinePlayerID中返回已在线玩家的标识号；
		};
		int nErrCode;//ISERR_OK:正常通知，选角色成功; ISERR_FAILED_NOROLE:错误，相应角色已在线，在onlinePlayerID中返回已在线玩家的标识号；
		PlayerID playerID;//玩家唯一标识号，GateSrv生成；
		char szAccount[/*MAX_NAME_SIZE*/32];//方便gatesrv寻找或提示客户端；
		PlayerID onlinePlayerID;//已经在线的玩家ID号；
		TYPE_ID sequenceID;		//网络序列号
	}EGPlayerIsExist;
	MSG_SIZE_GUARD(EGPlayerIsExist);    


	//#define E_G_INVITE_PLAYER_JOIN_TEAM  0x3102  //centre传给gatesrv邀请组队消息 
	typedef struct StrEGInvitePlayerJoinTeam
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_INVITE_PLAYER_JOIN_TEAM;
		InvitePlayerInfo inviterInfo;	//邀请人的信息
		PlayerID beInvitedPlayerID;	    //被邀请的玩家的ID
		char beInvitedPlayerName[MAX_NAME_SIZE];	//被邀请的玩家的姓名
		int  beInvitedPlayerNameLen;	//被邀请玩家的姓名长度
	}EGInvitePlayerJoinTeam;

	//#define E_G_INVITE_ERROR			 0x3103   //centresrv转发组队错误消息
	typedef struct StrEGInviteError
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_INVITE_ERROR;
		PlayerID invitePlayerID;		//邀请的玩家ID
		char beInvitedPlayerName[MAX_NAME_SIZE];	//邀请的玩家姓名
		int  beInvitedPlayerNameLen;	//姓名的长度
		EInviteTeamResult  nErrorCode;	 //错误原因
	}EGInviteError;

	//#define E_G_INVITE_SUCCESS			 0x3104	  //centresrv转发组队成功消息
	typedef struct StrEGInviteSuccess
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_INVITE_SUCCESS;
		Member beInvitedPlayer;
		PlayerID invitePlayerID;	//邀请玩家的ID
	}EGInviteSuccess;
	MSG_SIZE_GUARD(EGInviteSuccess);   

    //#define E_G_SRVGROUP_CHAT            0x3105  //center发往gate全服聊天消息，gate收到后应广播至自身gate上的所有玩家；
	typedef struct StrEGSrvGroupChat
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_SRVGROUP_CHAT;
		char   chatPlayerName[MAX_NAME_SIZE]; //说话者名字;
		char   chatContent[256];              //说话内容;
		CHAT_TYPE chatType;		//区分世界聊天
		unsigned int extInfo;   //额外信息，例如种族消息时的种族
	}EGSrvGroupChat;
	MSG_SIZE_GUARD(EGSrvGroupChat);

	//#define E_G_PRIVATE_CHAT             0x3109  //center发往gate密聊聊天消息，gate收到后应转发给密聊对象玩家；
	typedef struct StrEGPrivateChat
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_PRIVATE_CHAT;
		char     chatPlayerName[MAX_NAME_SIZE]; //说话者名字;
		PlayerID tgtPlayerID;//目标对象ID，省去gate根据名字查找玩家的过程，只要校验一下名字即可；
		char     tgtPlayerName[MAX_NAME_SIZE];  //说话对象名字;
		char     chatContent[256];              //说话内容;
		CHAT_TYPE chatType;						//需区分是IRC私聊还是游戏世界的私聊
	}EGPrivateChat;
	MSG_SIZE_GUARD(EGPrivateChat);

	//#define E_G_FEEDBACK_ERROR	        0x3112  //反馈错误；
	typedef struct StrEGFeedbackError
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_FEEDBACK_ERROR;
		PlayerID lNotiPlayerID;				//通知的玩家
		EInviteTeamResult errorCode;		//错误原因
	}EGFeedbackError;
	MSG_SIZE_GUARD(EGFeedbackError);

	typedef struct StrEGChatError
	{
		enum
		{
			TARGET_NOT_EXIST         	=	0,               	//目标不存在
			NO_WORLD_CHAT_PRIVILEGE  	=	1,               	//没有世界聊天的权限
			NO_PRIVATE_CHAT_PRIVILEGE	=	2,               	//没有私聊权限
			NO_TEAM_CHAT_PRIVILEGE   	=	3,               	//没有组内聊天权限
		};

		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_CHAT_ERROR;
		CHAT_TYPE nType;//消息类型：如群聊，密聊，组队内通话等；
		unsigned char errorCode;		//错误原因
		PlayerID lNotiPlayerID;//聊天发起者
		char     tgtPlayerName[MAX_NAME_SIZE];  //聊天对象;
	}EGChatError;
	MSG_SIZE_GUARD(EGChatError);

	typedef struct StrEGAddChatGroupMemberByOwner
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_ADD_CHAT_GROUP_MEMBER_BY_OWNER;
		UINT     chatGroupID;
		PlayerID lNotiPlayerID;
		bool exitstTargetName;
		char     tgtPlayerName[MAX_NAME_SIZE];
		PlayerID targetPlayerID;
	}EGAddChatGroupMemberByOwner;
	MSG_SIZE_GUARD(EGAddChatGroupMemberByOwner);

	
	typedef struct StrEGIrcQueryPlayerInfo
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_IRC_QUERY_PLAYERINFO;
		PlayerID	reqID;		//请求查询的ID
		PlayerID	queryID;	//被查询的目标ID
	}EGIrcQueryPlayerInfo;
	MSG_SIZE_GUARD(EGIrcQueryPlayerInfo);


	typedef struct StrEGIrcQueryPlayerInfoRst
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_IRC_QUERY_PLAYERINFO_RST;

		//Enums
		typedef	enum
		{
			QUERY_SUCCESS         	=	0,            	//
			NOT_IRC_PLAYER        	=	1,            	//
			NOT_FOUND_QUERY_PLAYER	=	2,            	//
		}ItemType;

		PlayerID              reqID;												//查询者的ID
		INT                 	errorCode;                                  	//错误信息
		CHAR                	playerName[MAX_NAME_SIZE];                    	//请求信息的玩家姓名
		BYTE                	playerSex;                                  	//性别 0x01男 0x02女
		USHORT              	mapID;                                      	//玩家所在的地图
		USHORT              	playerLevel;                                	//等级
		USHORT              	playerClass;                                	//职业
		USHORT              	playerRace;                                 	//种族
		CHAR                	GuildName[MAX_NAME_SIZE];                     	//公会伲称
		CHAR                	FereName[MAX_NAME_SIZE];                      	//伴侣伲称
	}EGIrcQueryPlayerInfoRst;
	MSG_SIZE_GUARD(EGIrcQueryPlayerInfoRst);

	//#define E_G_CHECK_PUNISHMENT_RESULT 0x3117	//Center经过验证告之是否需要启动天谴
	typedef struct StrEGCheckPunishmentResult
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_CHECK_PUNISHMENT_RESULT;
		PunishRaceInfo humanRace;
		PunishRaceInfo elfRace;
		PunishRaceInfo ekkaRace;
	}EGCheckPunishmentResult;
	MSG_SIZE_GUARD(EGCheckPunishmentResult);

	//#define E_G_GMCMD_REQUEST 0x3118	//center转发至gatesrv
	typedef struct StrEGGmCmdRequest
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_GMCMD_REQUEST;
		PlayerID requestPlayerID;
		PlayerID targetPlayerID;
		char strChat[256];			//具体的gm命令的内容
	}EGGmCmdRequest;
	MSG_SIZE_GUARD(EGGmCmdRequest);

	//#define E_G_GMCMD_RESULT  0x3119	//center转发给gate告知使用者结果
	typedef struct StrEGGmCmdResult
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_GMCMD_RESULT;
		PlayerID requestPlayerID;	//申请GM命令的玩家
		int resultCode;			//具体返回的结果
	}EGGmCmdResult;
	MSG_SIZE_GUARD(EGGmCmdResult);

	////#define E_G_RACE_GROUP_NOTIFY 0x311a	//种族转发
	//typedef struct StrEGRaceGroupNotify
	//{
	//	static const unsigned char  byPkgType = E_G;
	//	static const unsigned short wCmd = E_G_RACE_GROUP_NOTIFY;
	//	char      chatPlayerName[MAX_NAME_SIZE];	//说话者名字;
	//	char      chatContent[256];              //说话内容;
	//	CHAT_TYPE  chatType;						//区分IRC世界聊天还是游戏世界的世界聊天
	//	ERace	   notifyRace;						//通知的种族
	//}EGRaceGroupNotify;
	//MSG_SIZE_GUARD(EGRaceGroupNotify);

	typedef struct StrEGNoticePlayerHaveUnReadMail
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_NOTICE_PLAYER_HAVE_UNREAD_MAIL;
		PlayerID recvPlayerID;
		unsigned int unReadMailCount;
	}EGNoticePlayerHaveUnReadMail;
	MSG_SIZE_GUARD(EGNoticePlayerHaveUnReadMail);

	//#define E_G_NOTIFY_ACTIVITY_STATE 0x311c	//全服通知活动状态
	typedef struct StrEGNotifyActivityState
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_NOTIFY_ACTIVITY_STATE;
		unsigned int activityType;
		unsigned int activityState;
	}EGNotifyActivityState;
	MSG_SIZE_GUARD(EGNotifyActivityState);

	//#define E_G_EXCEPTION_NOTIFY	0x311d		//CenterSrv通知异常
	typedef struct StrEGExceptionNotify
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_EXCEPTION_NOTIFY;
		unsigned int exceptionReason;	//原因
	}EGExceptionNotify;
	MSG_SIZE_GUARD(EGExceptionNotify);

	typedef struct StrEGGMQueryTargetCheckRst
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_GMQUERY_TARGET_CHECK_RST;
		PlayerID gmPlayerId;
		GTCmd_Query_PlayerID_Rsp resp;
	}EGGMQueryTargetCheckRst;
	MSG_SIZE_GUARD(EGGMQueryTargetCheckRst);

	//#define E_G_NEW_COPY_CMD                 0x311f //通知创建新副本；
	typedef struct StrEGNewCopyCmd
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_NEW_COPY_CMD;
		CopyManagerEle copyInfo;//欲建副本信息；
	}EGNewCopyCmd;
	MSG_SIZE_GUARD(EGNewCopyCmd);

	//#define E_G_DEL_COPY_CMD                 0x3120 //通知删除旧副本；
	typedef struct StrEGDelCopyCmd
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_DEL_COPY_CMD;
		CopyManagerEle copyInfo;//欲删副本信息；
	}EGDelCopyCmd;
	MSG_SIZE_GUARD(EGDelCopyCmd);

	//#define E_G_DEL_COPY_QUEST               0x3121 //centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
	typedef struct StrEGDelCopyQuest
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_DEL_COPY_QUEST;
		CopyManagerEle copyInfo;
	}EGDelCopyQuest;
	MSG_SIZE_GUARD(EGDelCopyQuest);

	//#define E_G_COPY_RES_RST             0x3122 //返回给gate的副本相关操作结果
	typedef struct StrEGCopyResRst
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_COPY_RES_RST;
		enum CopyResRst {
			NEW_COPY_FAIL=0,//创建新副本失败
		};
		CopyResRst resRst;//操作结果
	}EGCopyResRst;
	MSG_SIZE_GUARD(EGCopyResRst);

	//#define E_G_ENTER_COPY_CMD               0x3123 //指令玩家进入副本；
	typedef struct StrEGEnterCopyCmd
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_ENTER_COPY_CMD;
		PlayerID       enterPlayerID;
		unsigned int   scrollType;
		CopyManagerEle copyInfo;//欲进副本信息；
	}EGEnterCopyCmd;
	MSG_SIZE_GUARD(EGEnterCopyCmd);

	//#define E_G_LEAVE_COPY_CMD               0x3124 //指令玩家离开副本；
	typedef struct StrEGLeaveCopyCmd
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_LEAVE_COPY_CMD;
		PlayerID       leavePlayerID;
		CopyManagerEle copyInfo;//欲离开副本信息；
	}EGLeaveCopyCmd;
	MSG_SIZE_GUARD(EGLeaveCopyCmd);

    //#define E_G_LEAVE_STEAM_NOTI                  0x3125 //通知玩家离开副本组队(删去玩家在各副本中的第一次进入信息，并从当前副本中踢出去)
	typedef struct StrEGLeaveSTeamNoti
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_LEAVE_STEAM_NOTI;
		PlayerID       leavePlayerID;
		//unsigned short mapsrvID;//副本所在mapsrv的ID号；
		unsigned int   copyID;//副本组队拥有的副本号
	}EGLeaveSTeamNoti;
	MSG_SIZE_GUARD(EGLeaveSTeamNoti);

	//#define E_G_GLOBE_COPYINFOS              0x1330 //发往gate的全局副本信息查询结果；
	typedef struct StrEGGlobeCopyInfos
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_GLOBE_COPYINFOS;
		PlayerID         notiPlayerID;
		GCGlobeCopyInfos globeCopyInfos;		
	}EGGlobeCopyInfos;
	MSG_SIZE_GUARD(EGGlobeCopyInfos);

	//#define E_G_TEAM_COPYINFOS               0x1331 //发往center的组队副本信息查询结果；
	typedef struct StrEGTeamCopyInfos
	{
		static const unsigned char byPkgType = E_G;
		static const unsigned short wCmd = E_G_TEAM_COPYINFOS;
		PlayerID         notiPlayerID;
		unsigned int     steamID;
		GCTeamCopyInfos  teamCopyInfos;		
	}EGTeamCopyInfos;
	MSG_SIZE_GUARD(EGTeamCopyInfos);

	typedef struct strEGEnterPWMapCheckRst
	{
		//enum eRetCode
		//{
		//	OK = 0,//检查通过
		//	UNKNOWN_ERR,//其他未知错误
		//	NO_PW_INFO,//没有伪线信息
		//	NO_PW_MAP_INFO,//没有伪线地图信息
		//	PW_CANNOT_ENTER,//伪线不能进入
		//	PW_MAP_CANNOT_ENTER,//伪线地图不能进入
		//	POOL_LIMIT,//达到普通玩家上限
		//	VIP_LIMIT,//达到vip玩家上限
		//};

		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_ENTER_PWMAP_CHECK_RST;
		PlayerID   lNoticePlayerID;
		bool isShift;
		int ret;
	}EGEnterPWMapCheckRst;
	MSG_SIZE_GUARD(EGEnterPWMapCheckRst); 

	typedef struct strEGModifyPWStateRst
	{
		enum eRetCode
		{
			OK = 0,//成功
			CANNOT_MOIFY,//不能转换到新状态
		};

		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_MODIFY_PW_STATE_RST;
		PlayerID   lNoticePlayerID;
		unsigned short pwid;
		unsigned char state;
		int ret;
	}EGModifyPWStateRst;
	MSG_SIZE_GUARD(EGModifyPWStateRst); 

	typedef struct strEGModifyPWStateNtf
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_MODIFY_PW_STATE_NTF;
		unsigned short pwid;
		unsigned char state;
	}EGModifyPWStateNtf;
	MSG_SIZE_GUARD(EGModifyPWStateNtf); 

	typedef struct strEGModifyPWMapStateRst
	{
		enum eRetCode
		{
			OK = 0,//成功
			CANNOT_MOIFY,//不能转换到新状态
		};

		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_MODIFY_PWMAP_STATE_RST;
		PlayerID   lNoticePlayerID;
		unsigned short pwid;
		unsigned short pwMapid;
		unsigned char state;
		int ret;
	}EGModifyPWMapStateRst;
	MSG_SIZE_GUARD(EGModifyPWMapStateRst);

	typedef struct strEGModifyPWMapStateNtf
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_MODIFY_PWMAP_STATE_NTF;
		unsigned short pwid;
		unsigned short pwMapid;
		unsigned char state;
	}EGModifyPWMapStateNtf;
	MSG_SIZE_GUARD(EGModifyPWMapStateNtf); 

	typedef struct strEGPWMapPlayerNumNtf
	{
		static const unsigned char  byPkgType = E_G;
		static const unsigned short wCmd = E_G_PWMAP_PLAYER_NUM_NTF;
		unsigned short pwid;
		unsigned short pwMapid;
		unsigned short pwCurrentNum;
	}EGPWMapPlayerNumNtf;
	MSG_SIZE_GUARD(EGPWMapPlayerNumNtf);

	///...centersrv发往gatesrv的消息命令字
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往loginsrv的消息结构...
	typedef struct StrGLPlayerLogin
	{
		static const unsigned char  byPkgType = G_L;
		static const unsigned short wCmd = G_L_PLAYER_LOGIN;
		PlayerID playerID;//玩家唯一标识号，GateSrv生成；
		char szAccount[MAX_NAME_SIZE];
		char szPassword[MAX_NAME_SIZE];
	}GLPlayerLogin;
	MSG_SIZE_GUARD(GLPlayerLogin);
	///...gatesrv发往loginsrv的消息结构
	//////////////////////////////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往loginsrv的消息结构...
	typedef struct StrLGPlayerLoginReturn
	{
		static const unsigned char  byPkgType = L_G;
		static const unsigned short wCmd = L_G_PLAYER_LOGIN_RETURN;
		PlayerID playerID;//玩家唯一标识号，GateSrv生成；
		char szAccount[MAX_NAME_SIZE];
		int iRet;
	}LGPlayerLoginReturn;
	MSG_SIZE_GUARD(LGPlayerLoginReturn);
	///...gatesrv发往loginsrv的消息结构
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往mapsrv的消息结构...
    //#define G_M_REQ_MANED_MAPS   0x1517 //请求MapSrv管理的地图编号列表；
	typedef struct StrGMReqManedMaps
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_REQ_MANED_MAPS;
		unsigned long lGID;//发出请求的gatesrv的ID号(第几个gatesrv)
	}GMReqManedMaps; 
	MSG_SIZE_GUARD(GMReqManedMaps);

    //#define  G_M_ADD_APT_REQ      0x152e //添加活点需求；
	typedef struct StrGMAddActivePtReq
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ADD_APT_REQ;
		PlayerID                reqPlayer;//请求玩家的ID号；
		UINT                	reqID;                             	//请求号，服务器返回添加结果以及活点信息时使用此ID号，玩家短时间内连续添加多个活点时，以此区别不同添加目标
		UINT                	nickNameSize;                      	//活点目标名字长度
		CHAR                	nickName[MAX_NAME_SIZE];           	//活点目标名字
	}GMAddActivePtReq; 
	MSG_SIZE_GUARD(GMAddActivePtReq);

    //#define G_M_CHAT             0x1516 //聊天信息；
	typedef struct StrGMChat
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHAT;
		CHAT_TYPE  nType;//消息类型：如群聊，密聊，组队内通话等；
		UINT       usedItemID;//所使用道具的唯一ID号
		PlayerID sourcePlayerID;//发聊天消息玩家ID；
		PlayerID targetPlayerID;//如果是密聊，则为密聊对象；
		char strChat[256];//聊天消息内容，包括末尾的\0;
	}GMChat; //聊天消息
	MSG_SIZE_GUARD(GMChat);

	////#define G_M_PLAYER_PKG       0x1515 //玩家角色包裹信息；
	//typedef struct StrGMPlayerPkg // 返回对应玩家的角色列表
	//{
	//	static const unsigned char  byPkgType = G_M;
	//	static const unsigned short wCmd = G_M_PLAYER_PKG;
	//	PlayerID   playerID;//通知哪个玩家的相关信息；
	//	unsigned char ucIndex; //第几页
	//	ItemInfo_i    playerPkg[10];//玩家包裹信息；
	//}GMPlayerPkg;
	//MSG_SIZE_GUARD(GMPlayerPkg);

	//gatesrv发往mapsrv玩家在地图上出现
	typedef struct StrGMPlayerAppear
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLAYER_APPEAR;
		PlayerID lPlayerID;//玩家ID号；
		PlayerInfo_1_Base playerInfo;//玩家信息；
	}GMPlayerAppear;//gatesrv发往mapsrv玩家在地图上出现
	MSG_SIZE_GUARD(GMPlayerAppear);

	//gatesrv发往mapsrv玩家离开地图
	typedef struct StrGMPlayerLeave 
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLAYER_LEAVE;
		enum {//特别标记,目前只设两个:0:正常,1:玩家已不存在于gatesrv（发出进入世界请求还未等到mapsrv回复便已下线，此时mapsrv应将此玩家删去);
			NO_SPECIAL = 0,
			NOT_EXIST = 1,
			QUIT_QUEUE = 2,
			MAP_SWITCH_NORMAL = 3,//(从普通地图或副本地图)跳至普通地图
			MAP_SWITCH_COPY = 4, //(从普通地图或副本地图)跳至副本地图
			RETURN_SEL_ROLE = 5, //回到人物选择界面
		};
		PlayerID lPlayerID;//玩家ID号；
		//char playerIDData[MAX_PLAYERID_SIZE*sizeof(PlayerID) + 1];
		unsigned long lMapCode;//离开地图名；
		int nSpecialFlag;//特别标记,目前只设两个:0:正常,1:玩家已不存在于gatesrv（发出进入世界请求还未等到mapsrv回复便已下线，此时mapsrv应将此玩家删去);
	}GMPlayerLeave;//gatesrv发往mapsrv玩家离开地图
	MSG_SIZE_GUARD(GMPlayerLeave);

	//gatesrv发往mapsrv的跑动请求消息
	typedef struct StrGMRun
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_RUN;
		PlayerID lPlayerID;//玩家ID号；
		float fCurWorldPosX;//移动出发点X客户端坐标；
		float fCurWorldPosY;//移动出发点Y客户端坐标；
		float fTargetWorldPosX;//移动目标点X客户端坐标；
		float fTargetWorldPosY;//移动目标点Y客户端坐标；
		TYPE_ID sequenceID;		//网络序列号
	}GMRun; //gatesrv发往mapsrv的跑动请求消息
	MSG_SIZE_GUARD(GMRun);

	//#define G_M_PLAYER_BEYONDBLOCK 0x1507 //玩家移动中跨块，以GMRun消息为前提;
	typedef struct StrGMPlayerBeyondBlock
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLAYER_BEYONDBLOCK;
		PlayerID movePlayerID;//移动玩家的ID号；
		float fCurX;//玩家当前浮点坐标X；
		float fCurY;//玩家当前浮点坐标Y
		float fTargetX;//目的地浮点坐标X；
		float fTargetY;//目的地浮点坐标Y；
	}GMPlayerBeyondBlock; //玩家移动中跨块，以GMRun消息为前提;
	MSG_SIZE_GUARD(GMPlayerBeyondBlock);

	//gatesrv通知mapsrv玩家所选角色信息，mapsrv保存此信息用于逻辑处理或以后广播给玩家的周围人；
	typedef struct StrGMPlayerRoleInfo
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLAYER_ROLE_INFO;
		PlayerID lPlayerID;//玩家ID号；
		int nHp;//玩家血量；
		unsigned long posEquips[16];  //装备栏位,具体数组位置的物理意义待定；
		unsigned short wLevel;//等级；
		unsigned short wNation;//种族；
		unsigned short wSex;//性别；
		char strNickName[/*MAX_NAME_SIZE*/32];//昵称,0结尾字符串；
	}GMPlayerRoleInfo; //gatesrv发往mapsrv玩家所选角色信息，mapsrv保存此信息用于逻辑处理或以后广播给玩家的周围人；
	MSG_SIZE_GUARD(GMPlayerRoleInfo);

	//gatesrv发往mapsrv玩家攻击怪物信息；
	typedef struct StrGMAttackMonster
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ATTACK_MONSTER;
		unsigned long lMonsterID;//怪物ID，地图内唯一；
		PlayerID attackerID;//攻击者自身ID号；
		unsigned long lSkillType;//攻击所使用的方式或技能ID
		TYPE_POS nAttackPosX;	//针对地面技能的位置信息
		TYPE_POS nAttackPosY;	//针对地面技能的位置信息
		float fAttackerPosX;	//攻击者的客户端位置
		float fAttackerPosY;	//攻击者的客户端位置 
		TYPE_ID  sequenceID;	//网络序列ID
	}GMAttackMonster; //gatesrv发往mapsrv玩家攻击怪物信息；
	MSG_SIZE_GUARD(GMAttackMonster);

	//gatesrv发往mapsrv攻击玩家信息；#define G_M_ATTACK_PLAYER    0x1518 //攻击玩家；
	typedef struct StrGMAttackPlayer
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ATTACK_PLAYER;
		PlayerID attackerID;//攻击者ID号；
		PlayerID targetID;//被攻击者ID号；
		unsigned long lSkillType;//攻击所使用的方式或技能ID
		float fAttackerPosX;	//攻击者的位置客户端X
		float fAttackerPosY;	//攻击者的位置客户端Y
		TYPE_ID sequenceID;		//网络序列号
	}GMAttackPlayer; //gatesrv发往mapsrv攻击玩家信息；
	MSG_SIZE_GUARD(GMAttackPlayer);

	//切换地图
	typedef struct StrGMSwitchMap
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_SWITCH_MAP;
		PlayerID playerID;
		unsigned short usMapID;	// 地图ID
		TYPE_POS iPosX;	// X坐标
		TYPE_POS iPosY;	// Y坐标
	}GMSwitchMap;
	MSG_SIZE_GUARD(GMSwitchMap);

    //#define G_M_SCRIPT_CHAT 0x151a //玩家点击NPC与其对话（包括玩家点击NPC以要求开始对话以及点击当前对话中的某选项）
	typedef struct StrGMScriptChat
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SCRIPT_CHAT;
		PlayerID playerID;//参与对话的玩家；
        SCRIPT_TYPE   tgtType;                      	//对象类型，item脚本或npc脚本
		unsigned long tgtID;//参与对话的NPC ID；
		int           nOption;//玩家的选项，对于初始点击（开始对话），nOption填0；
		//如果玩家选择选项时还需有附带信息，则以后再加参数；
	}GMScriptChat;
	MSG_SIZE_GUARD(GMScriptChat);

	typedef struct StrGMFullPlayerInfo//玩家完整信息
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_FULL_PLAYER_INFO;
		PlayerID playerID;
        char szAccount[MAX_NAME_SIZE];									// 账号
		unsigned char ucIndex;
		bool bEnd;
		unsigned short usSendByte;
		char pszData[460];
		TYPE_ID	sequenceID;		//网络序列号
	}GMFullPlayerInfo;
	MSG_SIZE_GUARD(GMFullPlayerInfo);

	//消息大致顺序,G_C_PLAYER_DYING-->C_G_DEFAULT_REBIRTH-->G_C_REBIRTH_INFO-->C_G_REBIRTH_READY-->G_C_PLAYER_APPEAR
    //#define G_M_DEFAULT_REBIRTH 0x151c //玩家死亡倒计时时间到,发此消息通知SRV，默认复活至最近的复活点，并进行扣经验等操作；
	typedef struct StrGMDefaultRebirth
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_DEFAULT_REBIRTH;
		PlayerID playerID;
		TYPE_ID  sequenceID;	//网络序列ID
	}GMDefaultRebirth;
	MSG_SIZE_GUARD(GMDefaultRebirth);

	//消息大致顺序,G_C_PLAYER_DYING-->C_G_DEFAULT_REBIRTH-->G_C_REBIRTH_INFO-->C_G_REBIRTH_READY-->G_C_PLAYER_APPEAR
    //#define G_M_REBIRTH_READY 0x151d //玩家重生准备好,发此消息通知SRV，SRV收到此消息后，将玩家Appear消息广播给复活点周围玩家；
	typedef struct StrGMRebirthReady
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REBIRTH_READY;
		PlayerID rebirthPlayerID;//复活玩家ID号；
		TYPE_ID  sequenceID;	//网络序列ID
	}GMRebirthReady;
	MSG_SIZE_GUARD(GMRebirthReady);

	//临时协议，gatesrv发往mapsrv，玩家连击数；
	typedef struct StrGMPlayerCombo
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLAYER_COMBO;
		PlayerID playerID;//玩家ID号；
		unsigned long comboNum;//连击数；
	}GMPlayerCombo;//gatesrv发往mapsrv攻击怪物消息
	MSG_SIZE_GUARD(GMPlayerCombo);

	//gatesrv转发mapsrv请求某个未知角色信息；//该工作以后可以转由centersrv进行？
	typedef struct StrGMReqRoleInfo
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_REQ_ROLEINFO;
		PlayerID lSelfPlayerID;  //发出请求的玩家
		PlayerID lReqPlayerID;//玩家ID号，请求此玩家的当前角色信息；
	}GMReqRoleInfo; //客户端发往服务器请求某个未知角色信息
	MSG_SIZE_GUARD(GMReqRoleInfo);

	//gatesrv转发mapsrv请求某个未知Monster信息
	typedef struct StrGMReqMonsterInfo
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_REQ_MONSTERINFO;
		PlayerID lSelfPlayerID;  //发出请求的玩家
		unsigned long lMonsterID; //怪物ID号，请求此怪物的相关信息；
	}GMReqMonsterInfo; //客户端发往服务器请求某个未知怪物信息
	MSG_SIZE_GUARD(GMReqMonsterInfo);

	//发送交易请求
	typedef struct StrGMSendTradeReq
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_SEND_TRADE_REQ;

		PlayerID selfPlayerID; //发出请求的玩家
		PlayerID targetPlayerID;//目标玩家
	}GMSendTradeReq;
	MSG_SIZE_GUARD(GMSendTradeReq);

	//发送交易请求的结果
	typedef struct StrGMSendTradeReqResult
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_SEND_TRADE_REQ_RESULT;

		PlayerID selfPlayerID; //发出请求的玩家
		PlayerID targetPlayerID;//目标玩家
		bool bAgree;
	}GMSendTradeReqResult;
	MSG_SIZE_GUARD(GMSendTradeReqResult);
	
	//交易中增加或删除物品
	typedef struct StrGMAddOrDeleteItem
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ADD_OR_DELETE_ITEM;

		enum eOperateType
		{
			ADD_ITEM,	// 增加
			DELETE_ITEM	// 删除
		};

		PlayerID selfPlayerID;//玩家自身
		eOperateType tOperateType;
		unsigned int uiItemID;//物品唯一ID
		unsigned int uiItemTypeID;//物品类型ID
	}GMAddOrDeleteItem;
	MSG_SIZE_GUARD(GMAddOrDeleteItem);

	//交易金钱
	typedef struct StrGMSetTradeMoney
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_SET_TRADE_MONEY;

		PlayerID selfPlayerID;//玩家自身
		unsigned int uiGoldMoney;//金币数
	}GMSetTradeMoney;
	MSG_SIZE_GUARD(GMSetTradeMoney);
	
	//取消交易
	typedef struct StrGMCancelTrade
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_CANCEL_TRADE;

		PlayerID selfPlayerID;//玩家自身ID
	}GMCancelTrade;
	MSG_SIZE_GUARD(GMCancelTrade);
	
	//锁定交易
	typedef struct StrGMLockTrade
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_LOCK_TRADE;
		
		PlayerID selfPlayerID;//玩家自身ID
		bool bLock;
	}GMLockTrade;
	MSG_SIZE_GUARD(GMLockTrade);
	
	//确认交易
	typedef struct StrGMConfirmTrade
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_CONFIRM_TRADE;
		PlayerID selfPlayerID;	//玩家自身ID
	}GMConfirmTrade;
	MSG_SIZE_GUARD(GMConfirmTrade);

	//#define G_M_FOEID_LIST   0x1566   //发送在线仇人的在线列表
	typedef struct StrGMFoeIDList
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_FOEID_LIST;
		PlayerID foeList[25];	//仇人ID
		int listSize;			//在线列表的大小
	}GMFoeIDList;
	MSG_SIZE_GUARD(GMFoeIDList);

    //#define G_M_DEL_FOEID    0x1567   //删除一个仇人ID
	typedef struct StrGMDelFoeID
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_DEL_FOEID;
		PlayerID foeID;
	}GMDelFoeID;
	MSG_SIZE_GUARD(GMDelFoeID);

	//#define G_M_ADD_FOEID    0x1568   //增加一个仇人ID
	typedef struct StrGMAddFoeID
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ADD_FOEID;
		PlayerID foeID;
	}GMAddFoeID;
	MSG_SIZE_GUARD(GMAddFoeID);

	//#define  G_M_ADDSTH_TO_PLATE 0x1528 //玩家往托盘中加物品
	typedef struct StrGMAddSthToPlate
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ADDSTH_TO_PLATE;
		PlayerID selfPlayerID;//操作执行玩家；
		EPlateType plateType;//托盘类型
		unsigned int  platePos;//放置的位置；
		ItemInfo_i sthID;//所添加物品的ID号；
	}GMAddSthToPlate;
	MSG_SIZE_GUARD(GMAddSthToPlate);

    //#define G_M_ITEM_UPGRADE_PLATE           0x1600 //玩家弹出物品升级界面请求(随时随地升级), 
	typedef struct StrGMItemUpgradePlate
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ITEM_UPGRADE_PLATE;
		PlayerID selfPlayerID;//请求弹托盘玩家
	}GMItemUpgradePlate;
	MSG_SIZE_GUARD(GMItemUpgradePlate);

	//#define  G_M_TAKESTH_FROM_PLATE 0x1529 //玩家从托盘中取物品
	typedef struct StrGMTakeSthFromPlate
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_TAKESTH_FROM_PLATE;
		PlayerID selfPlayerID;//操作执行玩家；
		//EPlatePos platePos;//取走位置；
		TYPE_ID platePos;//取走位置
		ItemInfo_i sthID;//取走物品的ID号；
	}GMTakeSthFromPlate;
	MSG_SIZE_GUARD(GMTakeSthFromPlate);

	//#define  G_M_PLATE_EXEC         0x152a //玩家尝试执行托盘操作
	typedef struct StrGMPlateExec
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLATE_EXEC;
		PlayerID selfPlayerID;//操作执行玩家；
	}GMPlateExec;
	MSG_SIZE_GUARD(GMPlateExec);

	//#define  G_M_CLOSE_PLATE        0x152b //玩家关闭托盘
	typedef struct StrGMClosePlate
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_CLOSE_PLATE;
		PlayerID selfPlayerID;//操作执行玩家；
	}GMClosePlate;
	MSG_SIZE_GUARD(GMClosePlate);

	typedef struct StrGMRepairWearByItem
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_REPAIR_WEAR_BY_ITEM;

		TYPE_ID  itemRepairman; //具有修复功能的物品
		TYPE_ID  repairTarget;	//待修复的物品
		PlayerID selfPlayerID;	//操作玩家；
	}GMRepairWearByItem;
	MSG_SIZE_GUARD(GMRepairWearByItem);

	typedef struct StrGMQueryRepairItemCost
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_QUERY_REPAIRITEM_COST;
		PlayerID selfPlayerID;//操作执行玩家；
	}GMQueryRepairItemCost;
	MSG_SIZE_GUARD(GMQueryRepairItemCost);

	typedef struct StrGMTimedWearIsZero
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_TIMED_WEAR_IS_ZERO;

		PlayerID selfPlayerID;	//操作玩家；
		TYPE_ID itemID;//
	}GMTimedWearIsZero;
	MSG_SIZE_GUARD(GMTimedWearIsZero);

	//#define G_M_USE_DAMAGE_ITEM		0x156d
	typedef struct StrGMUseDamageItem
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_USE_DAMAGE_ITEM;
		PlayerID attackPlayer;
		TYPE_POS attackPosX;
		TYPE_POS attackPosY;
		PlayerID targetID;
		TYPE_ID  itemUID;
	}GMUseDamageItem;
	MSG_SIZE_GUARD(GMUseDamageItem);


	//#define G_M_ADD_SPSKILL_REQ		0x156a
	typedef struct StrGMAddSpSkillReq
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ADD_SPSKILL_REQ;
		PlayerID  reqPlayerID;	//请求的玩家的ID号
		TYPE_ID  spSkillID; //添加的SP技能
	}GMAddSpSkillReq;
	MSG_SIZE_GUARD(GMAddSpSkillReq);

	//#define G_M_UPGRADE_SKILL 0x1570``//技能升级的请求
	typedef struct StrGMUpgradeSkill
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_UPGRADE_SKILL;
		PlayerID reqPlayerID;	//申请人的ＩＤ
		TYPE_ID skillID;		//需要升级的技能ID
	}GMUpgradeSkill;
	MSG_SIZE_GUARD(GMUpgradeSkill);

	////////////////////////关于NPC商店////////////////////////////////
	/*
		1、show shop && 第一页物品数量
		2、CG请求id号shop第n页物品各自数量
		3、GC返回id号shop第n页物品各自数量
		4、CG请求购买id号shop第n页第n个物品，以及该物品的ID号与数量，货币类型与数量；
		5、GC返回玩家购买id号shop第n页第n个物品，以及该物品的ID号与数量，货币类型与数量，购买成功或失败；
		6、CG玩家请求售出id类型物品，售出物品在包裹中的位置，售出物品数量，售出价格；
		7、GC返回玩家售出id类型物品结果，售出物品在包裹中的位置，售出物品数量，售出价格，售出后玩家的总金钱数，售出成功或失败；
	*/
	//#define G_M_QUERY_SHOP    0x1576 //查询商店物品数量
	typedef struct StrGMQueryShop
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_QUERY_SHOP;
		PlayerID lQueryPlayerID;//查询玩家的ID号；
		unsigned long shopID;//商店ID号；
		unsigned long shopPageID;//商店页码；
	}GMQueryShop;
	MSG_SIZE_GUARD(GMQueryShop);

	//#define G_M_SHOP_BUY_REQ  0x1577 //购买商店物品请求
	typedef struct StrGMShopBuyReq
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_SHOP_BUY_REQ;
		PlayerID lBuyerID;//执行购买操作玩家的ID号；
		unsigned long shopID;//商店ID号；
		unsigned long shopPageID;//商店页码；
		unsigned long shopPagePos;//商品在页中位置
		unsigned long itemID;//商品类型ID号；
		unsigned long itemNum;//一次购买的商品数量；
		unsigned long moneyType;//货币类型；
		unsigned long moneyID;//货币ID
		unsigned long moneyNum;//购买使用的货币数量；
	}GMShopBuyReq;
	MSG_SIZE_GUARD(GMShopBuyReq);

	//#define G_M_ITEM_SOLD_REQ 0x1578 //玩家请求售出物品
	typedef struct StrGMItemSoldReq
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ITEM_SOLD_REQ;
		PlayerID sellerID;//售出者的ID号；
		unsigned long itemID;//物品类型ID号；
		unsigned long itemPos;//售出物品在包裹中的位置；
		unsigned long itemNum;//售出数量；
		unsigned long silverMoneyNum;//本次售出希望获得的银币数；
	}GMItemSoldReq;
	MSG_SIZE_GUARD(GMItemSoldReq);

	typedef struct StrGMRepairWearByNPC
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_REPAIR_WEAR_BY_NPC;
		
		bool repairAll;
		unsigned int itemID;
		PlayerID selfId;
	}GMRepairWearByNPC;
	MSG_SIZE_GUARD(GMRepairWearByNPC);

	//#define G_M_CHANGE_BATTLEMODE 0x1583	//修改战斗模式
	typedef struct StrGMChangeBattleMode
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGE_BATTLEMODE;
		
		BATTLE_MODE mode;
		PlayerID reqID;
	}GMChangeBattleMode;
	MSG_SIZE_GUARD(GMChangeBattleMode);

	//查询恶魔时间
	typedef struct StrGMQueryEvilTime
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_QUERY_EVILTIME;

		PlayerID playerID;
	}GMQueryEvilTime;
	MSG_SIZE_GUARD(GMQueryEvilTime);

	//#define G_M_SUMMON_PET				0x158d			 //请求召唤宠物	
	typedef struct StrGMSummonPet
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SUMMON_PET;
		PlayerID ownerID;				//召唤的玩家 
	}GMSummonPet;
	MSG_SIZE_GUARD(GMSummonPet);

	//#define G_M_CLOSE_PET				0x158e			 //唤回宠物
	typedef struct StrGMClosePet
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CLOSE_PET;
		PlayerID ownerID;				//唤回的玩家 
	}GMClosePet;
	MSG_SIZE_GUARD(GMClosePet);

	//#define G_M_CHANGE_PET_NAME			0x158f			 //请求更换宠物姓名
	typedef struct StrGMChangePetName
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGE_PET_NAME;
		PlayerID ownerID;				//宠物所有者
		char petName[MAX_NAME_SIZE];	//宠物新的名称
	}GMChangePetName;
	MSG_SIZE_GUARD(GMChangePetName);


	//#define G_M_CHANGE_PET_SKILL_STATE	0x1590			 //新的怪物的技能设置状态
	typedef struct StrGMChangePetSkillState
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGE_PET_SKILL_STATE;
		PlayerID ownerID;				//宠物所有者
		unsigned long petSkillState;	//新的宠物技能状态 
	}GMChangePetSkillState;
	MSG_SIZE_GUARD(GMChangePetSkillState);

	//#define G_M_CHANGE_PET_IMAGE		0x1591			 //更改宠物的外形
	typedef struct StrGMChangePetImage
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGE_PET_IMAGE;
		PlayerID ownerID;				//宠物所有者
		unsigned long imageID;	//新的宠物技能状态 
	}GMChangePetImage;
	MSG_SIZE_GUARD(GMChangePetImage);
	

	///...gatesrv发往mapsrv的消息结构
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///mapsrv发往gatesrv的消息结构...

	//#define M_G_MAP_ISFULL         0x5100 //mapsrv发往gatesrv,通知本mapsrv是否满；
	typedef struct StrMGMapIsFull
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MAP_ISFULL;
		bool isFull;
	}MGMapIsFull;//mapsrv发往gatesrv,通知本mapsrv是否满；
	MSG_SIZE_GUARD(MGMapIsFull);

    //#define M_G_ERROR 0x5131 //mapsrv通知gatesrv各种错误发生；
	typedef struct StrMGError
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ERROR;
		enum
		{
			LEAVE_ERR_MAPDISMATCH=0, //处理玩家离开错误，离开的地图号不匹配；
			LEAVE_ERR_NOPLAYER,    //处理玩家离开错误，找不到gatesrv指定的离开玩家；			
			APPEAR_ERR_NORMALERR,  //一般错误；
			APPEAR_ERR_POSITION,   //位置错误
			APPEAR_ERR_MAPDISMATCH,	//地图找不到
			APPEAR_ERR_SAME_ID,		//相同ID的上线消息
			FULLINFO_DISORDER_PKG0, //玩家FULL_INFO顺序有误，错误的第0个包；
			FULLINFO_DISORDER_PKGN, //玩家FULL_INFO顺序有误，错误的后续包；
			MAPSRV_PLAYER_FULL,      //mapsrv玩家满，拒绝该玩家登录mapsrv的请求；  
		};
		int nErrCode;
		PlayerID leavePlayerID;
		PlayerID lNotiPlayerID;
		char szLeaveAccount[MAX_NAME_SIZE];
	}MGError;//mapsrv通知gatesrv各种错误发生；
	MSG_SIZE_GUARD(MGError);

    //#define M_G_LEAVE_VIEW         0x5106 //mapsrv发往gatesrv,怪物或其它玩家离开玩家视野;
	typedef struct StrMGLeaveView
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_LEAVE_VIEW;
		PlayerID lNotiPlayerID;//通知对象;
		PlayerID leavePlayerID;//离开的玩家ID号，如果字段都为0，则无效；
		TYPE_ID  leaveMonsterID;//离开的怪物ID号，如果为0，则无效；
	}MGLeaveView;//mapsrv发往gatesrv,怪物或其它玩家离开玩家视野;
	MSG_SIZE_GUARD(MGLeaveView);

 	//mapsrv发往gatesrv,本mapsrv管理的一个地图代码
	typedef struct StrMGManedMap
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MANED_MAP;
		bool isCopyMapSrv;//是否为副本地图服务器；
		int nMapNum;                //管理地图的数目；
		unsigned long lMapCode[64];   //管理的地图代码；
	}MGManedMap;//mapsrv发往gatesrv,本mapsrv管理的一个地图代码
	MSG_SIZE_GUARD(MGManedMap);

	//#define  M_G_ENTERMAP_RST     0x5164 //玩家进入地图成功
	typedef struct StrMGEnterMapRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ENTERMAP_RST;
		enum {
			ENTER_MAP_SUC = 0,
			ENTER_MAP_FAILED
		};
		int           nErrCode;
		PlayerID      lNotiPlayerID;//通知玩家ID号(进入地图玩家的ID号)；
		unsigned long lMapCode;
		TYPE_POS      nPosX;
		TYPE_POS      nPosY;
		TYPE_ID		   sequenceID;		//网络序列号
	}MGEnterMapRst;
	MSG_SIZE_GUARD(MGEnterMapRst);

    //#define  M_G_FINAL_REBIRTH_RST          0x5166    //玩家最终复活结果
	typedef struct StrMGFinalRebirthRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_FINAL_REBIRTH_RST;
		enum {
			FINAL_REBIRTH_SUC = 0,
			FINAL_REBIRTH_FAILED
		};
		int           nErrCode;
		PlayerID      lNotiPlayerID;//通知玩家ID号(复活玩家的ID号)；
		unsigned long lMapCode;//复活地图；
		TYPE_POS      nPosX;//复活点
		TYPE_POS      nPosY;//复活点
		TYPE_ID       sequenceID;	//网络序列号
		PlayerInfo_1_Base baseInfo;
	}MGFinalRebirthRst;
	MSG_SIZE_GUARD(MGFinalRebirthRst);

	//#define  M_G_SET_PLAYERPOS        0x5156   //设置玩家位置（同地图跳位置，只发给跳地图者）
	typedef struct StrMGSetPlayerPos
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SET_PLAYERPOS;
		PlayerID lNotiPlayerID;//通知玩家ID号(变更位置玩家的ID号)；
		GCSetPlayerPos setpos;
	}MGSetPlayerPos;
	MSG_SIZE_GUARD(MGSetPlayerPos);

	////////////////////////关于NPC商店////////////////////////////////
	/*
		1、show shop && 第一页物品数量
		2、CG请求id号shop第n页物品各自数量
		3、GC返回id号shop第n页物品各自数量
		4、CG请求购买id号shop第n页第n个物品，以及该物品的ID号与数量，货币类型与数量；
		5、GC返回玩家购买id号shop第n页第n个物品，以及该物品的ID号与数量，货币类型与数量，购买成功或失败；
		6、CG玩家请求售出id类型物品，售出物品在包裹中的位置，售出物品数量，售出价格；
		7、GC返回玩家售出id类型物品结果，售出物品在包裹中的位置，售出物品数量，售出价格，售出后玩家的总金钱数，售出成功或失败；
	*/
	//#define M_G_SHOW_SHOP     0x5170 //显示商店物品数量
	typedef struct StrMGShowShop
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_SHOP;
		PlayerID lNotiPlayerID;//通知玩家ID号(变更位置玩家的ID号)；
		unsigned long shopID;//商店ID号；
		unsigned long shopPageID;//商店页码；
		unsigned long itemNum[ITEMNUM_PER_SHOPLABEL];//商店中该页中的各位置物品当前数目；
	}MGShowShop;
	MSG_SIZE_GUARD(MGShowShop);

	//#define M_G_SHOP_BUY_RST  0x5171 //购买商店物品结果
	typedef struct StrMGShopBuyRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOP_BUY_RST;
		PlayerID lNotiPlayerID;//通知玩家ID号(执行购买操作玩家的ID号)；
		unsigned long shopID;//商店ID号；
		unsigned long shopPageID;//商店页码；
		unsigned long shopPagePos;//商品在页中位置
		unsigned long itemID;//商品类型ID号；
		unsigned long itemNum;//一次购买的商品数量；
		unsigned long moneyType;//货币类型ID号；
		unsigned long moneyNum;//购买使用的货币数量；                
		GCShopBuyRst::BuyRstType buyRst;//购买结果；
	}MGShopBuyRst;
	MSG_SIZE_GUARD(MGShopBuyRst);

	//#define M_G_ITEM_SOLD_RST 0x5172 //玩家请求售出物品
	typedef struct StrMGItemSoldRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ITEM_SOLD_RST;
		PlayerID lNotiPlayerID;//通知玩家ID号(执行售出操作玩家的ID号)；                
		unsigned long itemID;//物品类型ID号；
		unsigned long itemPos;//售出物品在包裹中的位置；
		unsigned long itemNum;//售出数量；
		unsigned long silverMoneyNum;//本次售出希望获得的银币数；
		GCItemSoldRst::SoldRstType soldRst;//售出结果；
	}MGItemSoldRst;
	MSG_SIZE_GUARD(MGItemSoldRst);

    //#define M_G_CHAT    0x5112 //聊天消息；
	typedef struct StrMGChat
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHAT;
        PlayerID   lNotiPlayerID;//通知对象ID号；
		CHAT_TYPE  chatType;//消息类型：如群聊，密聊，组队内通话等；
		unsigned int extInfo;//额外信息：例如，种族消息时存放种族，世界聊天时存放世界聊天的类型
		PlayerID   sourcePlayerID;//如果是密聊，则为发密聊信息者，如果是ItemUiInfo或NPCUiInfo，则包含的属性字段dwPID指明对话的NPC或ITEM；
		char       strChat[256];//聊天消息内容，包括末尾的\0;
	}MGChat; //聊天消息
	MSG_SIZE_GUARD(MGChat);

	//#define M_G_PLAYER_LEVELUP 0x5133 //mapsrv通知gatesrv角色升级
	typedef struct StrMGPlayerLevelUp
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_LEVELUP;
       PlayerID lNotiPlayerID;//通知对象ID号；
	  	PlayerID playerID;//玩家ID号；
		int nNewLevel;	//新等级
		int levelUpType;	//升级的类型
	}MGPlayerLevelUp; //mapsrv通知gatesrv角色属性改变；
	MSG_SIZE_GUARD(MGPlayerLevelUp);
   

	//#define M_G_TMPINFO_UPDATE 0x5134 //mapsrv通知gatesrv角色非存盘属性变化
	typedef struct StrMGTmpInfoUpdate
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_TMPINFO_UPDATE;
       PlayerID lNotiPlayerID;//通知对象ID号；
		PlayerID playerID;//玩家ID号；
		PlayerTmpInfo tmpInfo;	//新更新的非存盘属性
	}MGTmpInfoUpdate; //mapsrv通知gatesrv角色属性改变；
	MSG_SIZE_GUARD(MGTmpInfoUpdate);

	//#define M_G_ROLEATT_UPDATE 0x5132 //mapsrv通知gatesrv角色属性改变；
	typedef struct StrMGRoleattUpdate
	{
		enum { TYPE_SET, TYPE_ADD };
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ROLEATT_UPDATE;
       PlayerID      lNotiPlayerID;//通知对象ID号；
		PlayerID      lChangedPlayerID;//属性改变的玩家ID号；
		ROLEATT_TYPE   nType;			//发生改变的属性所属类型：如血量，魔法值等;
		int           nOldValue;		//旧的值
		int           nNewValue;		//新值
		TYPE_TIME     changeTime;		//更改的时间  0就是永久
		int				nAddType;      //是直接设置新值，还是表现为 oldvalue+改变的值		
		bool          bFloat;			//是否是浮点型
	}MGRoleattUpdate; //mapsrv通知gatesrv角色属性改变；
	MSG_SIZE_GUARD(MGRoleattUpdate);

    //#define M_G_ENVNOTI_FLAG       0x5110 //玩家跨块时，通知其一系列关于其周围环境的信息，本消息标记这一系列消息的开始或结束；
	typedef struct StrMGEnvNotiFlag
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ENVNOTI_FLAG;
        PlayerID lNotiPlayerID;//通知对象ID号；
		GCEnvNotiFlag gcNotiFlag;
	}MGEnvNotiFlag;//玩家跨块时，通知其一系列关于其周围环境的信息，本消息标记这一系列消息的开始或结束；
	MSG_SIZE_GUARD(MGEnvNotiFlag);

    //玩家信息;
	typedef struct StrMGOtherPlayerInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_OTHER_PLAYER_INFO;
		enum
		{
			ISERR_OK,//正常通知，没有错误
			ISERR_FAILED_NOROLE,//错误，无相应角色
		};
        PlayerID lNotiPlayerID;//通知对象ID号；
		GCOtherPlayerInfo playerInfo;
	}MGOtherPlayerInfo;//mapsrv发往gatesrv，用于通知lNotiPlayerID，其它某一玩家otherPlayerID的相关信息；
	MSG_SIZE_GUARD(MGOtherPlayerInfo);

	//#define M_G_SIMPLE_PLAYER_INFO           0x51ca    //广播给周围的玩家简略信息，信息分类修改，by dzj, 10.03.24;
	typedef struct StrMGSimplePlayerInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SIMPLE_PLAYER_INFO;
        PlayerID lNotiPlayerID;//通知对象ID号；
		GCSimplePlayerInfo playerInfo;
	}MGSimplePlayerInfo;//广播给周围的玩家简略信息，信息分类修改，by dzj, 10.03.24;
	MSG_SIZE_GUARD(MGSimplePlayerInfo);

	//#define M_G_SIMPLEST_INFO                0x51cb    //周围人群密集时的最简消息，信息分类修改，by dzj, 10.03.24;
	typedef struct StrMGSimplestInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SIMPLEST_INFO;
        PlayerID lNotiPlayerID;//通知对象ID号；
		GCSimplestInfo playerInfo;
	}MGSimplestInfo;//周围人群密集时的最简消息，信息分类修改，by dzj, 10.03.24;
	MSG_SIZE_GUARD(MGSimplestInfo);


	//mapsrv发往gatesrv，踢玩家
	typedef struct StrMGKickPlayer
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_KICK_PLAYER;
		PlayerID lNotiPlayerID;		//玩家ID号；
		PlayerID targetPlayerID;	//目标玩家
	}MGKickPlayer;
	MSG_SIZE_GUARD(MGKickPlayer);

	//mapsrv发往gatesrv的跑动请求消息
	typedef struct StrMGRun
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_RUN;
		PlayerID lNotiPlayerID;//通知对象ID号；
		PlayerID lPlayerID;//玩家ID号；
		GCRun::ItemType nErrCode;//RUNRST_OK:跑动正常，RUNRST_TARGETINVALID:目标点无效；
		float fCurWorldPosX;//移动出发点X客户端坐标；
		float fCurWorldPosY;//移动出发点Y客户端坐标；
		float fSpeed;//移动速度；
		float fTargetWorldPosX;//移动目标点X客户端坐标；
		float fTargetWorldPosY;//移动目标点Y客户端坐标；
		TYPE_ID sequenceID;		//网络序列ID
	}MGRun; //mapsrv发往gatesrv的跑动请求消息
	MSG_SIZE_GUARD(MGRun);

	//#define M_G_PLAYER_NEWTASK     0x510a //给玩家添加了一个新任务；
	typedef struct StrMGPlayerNewTask
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_NEWTASK;
		PlayerID       lNotiPlayerID;//玩家ID号；
		TYPE_ID        taskID;//任务标识号(服务器端分配，与玩家的后续通信中标识特定任务)
		TYPE_ID        taskTypeID;//任务类型标识号(对应Missions.xml中的任务ID号)
	}MGPlayerNewTask;
	MSG_SIZE_GUARD(MGPlayerNewTask);

	//#define M_G_PLAYER_TASKSTATUS  0x510b //通知玩家任务新状态；
	typedef struct StrMGPlayerTaskStatus
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_TASKSTATUS;
		PlayerID       lNotiPlayerID;//玩家ID号；
		TYPE_ID        taskID;//任务标识号;
		ETaskStatType  taskStat;//任务状态;
		unsigned int   failTime;//任务失败时间，0表示无失败时间，或中止原失败计时；
	}MGPlayerTaskStatus;
	MSG_SIZE_GUARD(MGPlayerTaskStatus);

	typedef struct StrMGPlayerTaskRecordUpdate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_TASKRECORD_UPDATE;
		PlayerID       lNotiPlayerID;//玩家ID号；
		TaskRecordInfo taskRdInfo;//任务记录信息
		char           taskType;
	}MGPlayerTaskRecordUpdate;
	MSG_SIZE_GUARD(MGPlayerTaskRecordUpdate);

	//通知玩家曾经做过的任务列表
	typedef struct StrMGPlayerTaskHistory
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_TASKHISTORY;
		PlayerID lNotiPlayerID;//玩家ID号；
		GCTaskHistory gcTaskHistory;
	}MGPlayerTaskHistory;
	MSG_SIZE_GUARD(MGPlayerTaskHistory);

	//显示任务UI信息
	typedef struct StrMGPlayerShowTaskUI
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_SHOWTASKUI;
		PlayerID lNotiPlayerID;//玩家ID号
		ETaskUIMsgType eTaskUIInfo;
	}MGPlayerShowTaskUI;
	MSG_SIZE_GUARD(MGPlayerShowTaskUI);

	//mapsrv发往gatesrv，玩家离开地图；
	typedef struct StrMGPlayerLeave
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_LEAVE;
		PlayerID lNotiPlayerID;//通知对象ID号；
		PlayerID lPlayerID;//玩家ID号；
		unsigned long lMapCode;//离开地图名；
	}MGPlayerLeave;//mapsrv发往gatesrv，玩家离开地图；
	MSG_SIZE_GUARD(MGPlayerLeave);

	////临时协议，mapsrv发往gatesrv，玩家连击数；
	//typedef struct StrMGPlayerCombo
	//{
	//	static const unsigned char  byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_PLAYER_COMBO;
	//	PlayerID lNotiPlayerID;//通知对象ID号；
	//	PlayerID playerID;//玩家ID号；
	//	unsigned long comboNum;//连击数；
	//}MGPlayerCombo;//mapsrv发往gatesrv，玩家连击数；
	//MSG_SIZE_GUARD(MGPlayerCombo);

	//mapsrv发往gatesrv，怪物在地图上出生；
	typedef struct StrMGMonsterBorn
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MONSTER_BORN;
		PlayerID lNotiPlayerID;//通知对象ID号；
		unsigned long lMonsterTypeID;//怪物类别ID(指明为哪类怪物)
		unsigned long lMonsterID;//怪物ID，地图内唯一；
		TYPE_POS nMapPosX;//出生点X；
		TYPE_POS nMapPosY;//出生点Y；
	}MGMonsterBorn;//mapsrv发往gatesrv，怪物在地图上出生；
	MSG_SIZE_GUARD(MGMonsterBorn);

	//mapsrv发往gatesrv，怪物移动；
	typedef struct StrMGMonsterMove
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MONSTER_MOVE;
		PlayerID lNotiPlayerID;//通知对象ID号；
		GCMonsterMove gcMonsterMove;
		//unsigned long lMonsterID;//怪物ID，地图内唯一；
		//TYPE_POS nOrgMapPosX;//出发点X；
		//TYPE_POS nOrgMapPosY;//出发点Y；
		//TYPE_POS nNewMapPosX;//目标点X；
		//TYPE_POS nNewMapPosY;//目标点Y；
		//TYPE_POS nTargetMapPosX;	//怪物的远距离目标点
		//TYPE_POS nTargetMapPosY;	//怪物的远距离目标点
		//float fSpeed;//移动速度；
	}MGMonsterMove;//mapsrv发往gatesrv，怪物移动；
	MSG_SIZE_GUARD(MGMonsterMove);

	//mapsrv发往gatesrv，怪物消失；
	typedef struct StrMGMonsterDisappear
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MONSTER_DISAPPEAR;
		PlayerID                    lNotiPlayerID;//通知对象ID号；
		unsigned long               lMonsterID;//怪物ID，地图内唯一；
		GCMonsterDisappear::E_DIS_TYPE  disType;//死亡消失或直接消失；
	}MGMonsterDisappear;//mapsrv发往gatesrv，怪物消失；
	MSG_SIZE_GUARD(MGMonsterDisappear);

	//mapsrv发往gatesrv，怪物被攻击；
	typedef struct StrMGPlayerAttackTarget
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_ATTACK_TARGET;
		PlayerID lNotiPlayerID;//通知对象ID号；
		EBattleResult nErrCode;//错误代码，ERRINFO_NOERR:攻击正常无错误，ERRINFO_FAILED:攻击失败（如有具体失败原因，则贺轶男再添加对应枚举量）
		EScopeFlag scopeFlag;	//SINGLE_ATTACK,	//正常通知,独立完整包		SCOPE_ATTACK,	//正常通知,范围消息中的一个		SCOPE_ATTACK_END,	//范围列表结束
		PlayerID lPlayerID;//攻击者ID；
		EBattleFlag battleFlag;//爆击标记，CRITICAL_NO:正常，CRITICAL_YES:爆击;
		TYPE_ID	 skillID;	//攻击所使用的方式或技能ID
		unsigned long lCurseTime;//技能的动作时间
		unsigned long lCooldownTime;//技能的冷却时间
		PlayerID targetID;		//目标ID
		unsigned long  lHpSubed;		//怪物减去的HP;
		unsigned long  lHpLeft;			//怪物剩余HP;
		unsigned int nAtkPlayerHP;		//这次攻击攻击者的HP
		unsigned int nAtkPlayerMP;		//这次攻击攻击者的MP
		TYPE_ID  sequenceID;			//网络序列ID
		TYPE_ID  consumeItemTypeID;		//消耗道具的类型ID
		PlayerID rewardID;			//如果是怪物的话,需要通知其REWARD ID
		PlayerID mainTargetID;		//这次攻击的针对目标，AOE攻击
		TYPE_POS aoePosX;			//地面AOE的目标点			
		TYPE_POS aoePosY;			//地面AOE的目标点
	}MGPlayerAttackTarget;//mapsrv发往gatesrv，怪物被攻击；
	MSG_SIZE_GUARD(MGPlayerAttackTarget);

	//mapsrv发往gatesrv,怪物信息(血量，速度，方向等)；
	typedef struct StrMGMonsterInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MONSTER_INFO;
		PlayerID lNotiPlayerID;//通知对象ID号；
		GCMonsterInfo gcmonsterInfo;
	}MGMonsterInfo;//mapsrv发往gatesrv，怪物信息(血量，速度，方向等)；
	MSG_SIZE_GUARD(MGMonsterInfo);

	typedef struct StrMGMonsterAttackPlayer
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MONSTER_ATTACK_PLAYER;
		PlayerID lNotiPlayerID;//通知对象ID号；
		EBattleResult nErrCode;
		PlayerID lPlayerID;//被攻击的玩家
		EBattleFlag battleFlag;	//怪物攻击的暴击标志
		EScopeFlag scopeFlag;
		unsigned long lMonsterID;//怪物ID，地图内唯一；
		unsigned long lMonsterType;//怪物类型；
		TYPE_POS nMapPosX;//怪物位置X;
		TYPE_POS nMapPosY;//怪物位置Y;
		TYPE_ID nSkillID;	//怪物所使用的技能ID
		int nCurseTime;	//传给客户端，怪物攻击播放的时间;
		unsigned int nDamage;	//造成的伤害
		unsigned int nLeftHp;	//玩家被攻击后剩下的HP
		//其他，可能会有造成的状态== 暂时不添加
	}MGMonsterAttackPlayer;//mapsrv发往gatesrv，怪物信息(血量，速度，方向等)；
	MSG_SIZE_GUARD(MGMonsterAttackPlayer);

	////#define M_G_PLAYER_ATTACK_PLAYER 0x5135 //mapsrv通知玩家攻击玩家； //9月8日,hyn改
	//typedef struct StrMGPlayerAttackPlayer
	//{
	//	static const unsigned char  byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_PLAYER_ATTACK_PLAYER;
	//	PlayerID lNotiPlayerID;//通知对象ID号；
	//	EBattleResult nErrCode;		//错误码
	//	EBattleFlag battle;		//命中标志
	//	EScopeFlag scopeFlag;		//范围攻击标志
	//	PlayerID attackerID;		//攻击者ID号；
	//	PlayerID targetID;			//被攻击者ID号；
	//	TYPE_ID	 nSkillID;				//所使用的技能ID
	//	TYPE_POS attackerPosX;		//攻击者位置X;
	//	TYPE_POS attackerPosY;		//攻击者位置Y;
	//	TYPE_POS targetPosX;		//被攻击者位置X;
	//	TYPE_POS targetPosY;		//被攻击者位置Y;		
	//	unsigned int nDamage;		//造成的伤害
	//	unsigned int nLeftHp;		//玩家被攻击后剩下的HP
	//}MGPlayerAttackPlayer;
	//MSG_SIZE_GUARD(MGPlayerAttackPlayer);

    //#define M_G_REQ_SWITCH_MAP 0x5137 //向gatesrv请求跳转地图（跳转目标不在本mapsrv管理范围内时发此请求）
	typedef struct StrMGReqSwitchMap
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_REQ_SWITCH_MAP;
		PlayerID lNotiPlayerID;//通知对象ID号；
		PlayerID playerID;//玩家ID号；
		unsigned short usMapID;	// 地图ID
		TYPE_POS iPosX;	// X坐标
		TYPE_POS iPosY;	// Y坐标
	}MGReqSwitchMap;
	MSG_SIZE_GUARD(MGReqSwitchMap);

	typedef struct StrMGFullPlayerInfo//玩家完整信息
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_FULL_PLAYER_INFO;
		enum ValidFlag{
			MG_FULLINFO_OK = 0, //玩家full info有效；
			MG_FULLINFO_INVALID //玩家full info在mapsrv端没有完整保存；
		};
		ValidFlag validFlag;//MG_FULLINFO_OK = 0, //玩家full info有效；MG_FULLINFO_INVALID //玩家full info在mapsrv端没有完整保存；
		PlayerID lNotiPlayerID;//通知对象ID号；
		PlayerID playerID;
		unsigned char ucIndex;
		bool bEnd;
		EAppearType infoType;//INFO_NORMAL_UPDATE:常规更新, INFO_OFFLINE_UPDATE:玩家离开MapSrv时的更新
		unsigned short usSendByte;//本次发送的字节
		union{
		char pszData[460];//如果MG_FULLINFO_OK，则存放正常的full info的一部分；
		char pszAccount[460];//如果MG_FULLINFO_INVALID，则存放玩家帐号，以便通知centersrv玩家下线,之所以都要用484，以为了保证同类包的大小一致；
		};
	}MGFullPlayerInfo;
	MSG_SIZE_GUARD(MGFullPlayerInfo);

	//通知客户端玩家跳转地图
	typedef struct StrMGSwitchMap
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SWITCH_MAP;
		PlayerID lNotiPlayerID;//通知对象ID号；
		GCSwitchMap::ERR_T nErrCode;
		PlayerID playerID;//玩家ID号；
		unsigned short usMapID;	// 地图ID
		TYPE_POS iPosX;	// X坐标
		TYPE_POS iPosY;	// Y坐标
	}MGSwitchMap;
	MSG_SIZE_GUARD(MGSwitchMap);

	//消息大致顺序,G_C_PLAYER_DYING-->C_G_DEFAULT_REBIRTH-->G_C_REBIRTH_INFO-->C_G_REBIRTH_READY-->G_C_PLAYER_APPEAR
    //#define M_G_PLAYER_DYING    0x5139 //玩家死亡(倒计时)
	typedef struct StrMGPlayerDying
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_DYING;
       PlayerID lNotiPlayerID;//通知对象ID号；
		PlayerID dyingPlayerID;//死亡玩家ID号；
		unsigned long countDownSec;//倒计时时间(单位：秒)
	}MGPlayerDying;
	MSG_SIZE_GUARD(MGPlayerDying);

    //消息大致顺序,G_C_PLAYER_DYING-->C_G_DEFAULT_REBIRTH-->G_C_REBIRTH_INFO-->C_G_REBIRTH_READY-->G_C_PLAYER_APPEAR
    //#define M_G_REBIRTH_INFO    0x513a //玩家复活点信息；
	typedef struct StrMGRebirthInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_REBIRTH_INFO;
       PlayerID lNotiPlayerID;//通知对象ID号；
	  	PlayerID dyingPlayerID;//死亡玩家ID号；
		unsigned long mapID;//复活点所属地图；
		TYPE_POS nPosX;//复活点X坐标；
		TYPE_POS nPosY;//复活点Y坐标；
		TYPE_ID	 sequenceID;	//网络序列ID
	}MGRebirthInfo;
	MSG_SIZE_GUARD(MGRebirthInfo);

	//发送交易请求失败
	typedef struct StrMGSendTradeReqError
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SEND_TRADE_REQ_ERROR;

		PlayerID lNotiPlayerID;	//通知方
		GCSendTradeReqError	errInfo;
	}MGSendTradeReqError;
	MSG_SIZE_GUARD(MGSendTradeReqError);

	//收到交易邀请
	typedef struct StrMGRecvTradeReq
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_RECV_TRADE_REQ;
		PlayerID lNotiPlayerID;//通知谁；		
		PlayerID sendPlayerId;	//发送方ID
		char sendPlayerName[MAX_NAME_SIZE];
	}MGRecvTradeReq;
	MSG_SIZE_GUARD(MGRecvTradeReq);

	//收到交易结果
	typedef struct StrMGSendTradeReqResult
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SEND_TRADE_REQ_RESULT;
		
		enum eTradeResult
		{
			RECVER_HAS_DIED = 1,//1.自己已死亡
			RECVER_IN_TRADING = 2,//2.自己在交易状态
			RECVER_IN_CUDDLING = 3,//3.自己拥抱状态
			SENDER_NOT_EXIST = 4,//4.对方不存在
			SENDER_HAS_DIED = 5,//5.对方已死亡
			SENDER_IN_TRADING = 6,//6.对方已在交易中
			SENDER_IN_CUDDLING = 7,//7.对方在拥抱状态
			BOTH_OUT_TRADE_RANGE = 8,//8.双方在5m外
			OTHER_ERROR = 9,//9.其他错误

			TRADE_REQ_REFUSE = 100,//拒绝
			TRADE_CREATE_OK = 101//交易建立成功
		};

		PlayerID lNotiPlayerID;//通知谁；
		PlayerID lSelfPlayerID;//消息发起者
		eTradeResult tResult;	//结果
	}MGSendTradeReqResult;
	MSG_SIZE_GUARD(MGSendTradeReqResult);
	
	//交易中增加或删除物品
	typedef struct StrMGAddOrDeleteItem
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ADD_OR_DELETE_ITEM;

		enum eOperateType
		{
			ADD_ITEM,	// 增加
			DELETE_ITEM	// 删除
		};

		enum eOperateResult
		{
			FAILURE,//失败
			SUCCESS, //成功
			FAIL_PROTECT_ITEM,//保护道具
			FAIL_BIND_ITEM,//绑定道具
		};

		PlayerID lNotiPlayerID; //通知方ID
		PlayerID operatePlayerID;//
		eOperateType tOperateType;//操作类型
		unsigned int uiItemID;//物品唯一ID
		unsigned int uiItemTypeID;//物品类型ID
		TYPE_ID  usWearPoint;//耐久度
		CHAR     ucCount;  
		TYPE_UI16 usAddId;//道具的追加编号
		CHAR     ucLevelUp;//道具的升级等级
		eOperateResult tResult;//操作结果
	}MGAddOrDeleteItem;
	MSG_SIZE_GUARD(MGAddOrDeleteItem);

	//交易金钱
	typedef struct StrMGSetTradeMoney
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SET_TRADE_MONEY;

		PlayerID lNotiPlayerID; //通知方ID
		PlayerID operatePlayerID;//
		unsigned int uiGoldMoney;//金币数
		bool bOK;
	}MGSetTradeMoney;
	MSG_SIZE_GUARD(MGSetTradeMoney);
	
	//取消交易
	typedef struct StrMGCancelTrade
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_CANCEL_TRADE;
		
		PlayerID  lNotiPlayerID;//玩家自身ID
	}MGCancelTrade;
	MSG_SIZE_GUARD(MGCancelTrade);
	
	//锁定交易
	typedef struct StrMGLockTrade
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_LOCK_TRADE;

		PlayerID lNotiPlayerID;//玩家自身ID
		PlayerID lOperatePlayerID;//操作玩家
		bool bLock;
	}MGLockTrade;
	MSG_SIZE_GUARD(MGLockTrade);

	//确认交易
	typedef struct StrMGConfirmTrade
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_CONFIRM_TRADE;
		
		enum eTradeResult
		{
			SUCCESS = 0, //成功
			SELF_PKG_SPACE_NOT_ENOUGH = 1,//自己包裹空间不够
			PEER_PKG_SPACE_NOT_ENOUGH = 2 ,//对方包裹空间不够
			PEER_CONFIRM = 3,//对方确认
			PEER_NOT_CONFIRM = 4,//对方未确认
			OTHER_ERROR = 5,//其他错误
			SELF_IS_EVIL = 6,//自己是魔头
			PEER_IS_EVIL = 7,//对方是魔头
			SELF_MONEY_OVERFLOW = 8,//交易后自己可能金钱越界
			PEER_MONEY_OVERFLOW = 9 //交易后对方可能金钱越界
		};

		PlayerID lNotiPlayerID;//通知方ID
		PlayerID lOperatePlayerID;//操作玩家
		eTradeResult tResult;//结果
	}MGConfirmTrade;
	MSG_SIZE_GUARD(MGConfirmTrade);

	//#define  M_G_SHOW_PLATE           0x5157   //指示客户端显示托盘；
	typedef struct StrMGShowPlate
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_PLATE;
		PlayerID lNotiPlayerID;//玩家自身ID
		EPlateType plateType; 
	}MGShowPlate;
	MSG_SIZE_GUARD(MGShowPlate);

	//#define  M_G_ADDSTH_TO_PLATE_RST  0x5158   //往托盘放置物品结果；
	typedef struct StrMGAddSthToPlateRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ADDSTH_TO_PLATE_RST;
		PlayerID lNotiPlayerID;//玩家自身ID
		TYPE_ID platePos;//放置的位置；
		ItemInfo_i sthID;//所添加物品的ID号；
		GCAddSthToPlateRst::AddRstType addRst;//放置结果；
		bool isFulfilCon;//是否满足执行条件；
	}MGAddSthToPlateRst;
	MSG_SIZE_GUARD(MGAddSthToPlateRst);

	//#define  M_G_TAKESTH_FROM_PLATE_RST 0x5159 //从托盘取物品结果；
	typedef struct StrMGTakeSthFromPlateRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_TAKESTH_FROM_PLATE_RST;
		PlayerID lNotiPlayerID;//玩家自身ID
		//EPlatePos platePos;//取走位置；
		TYPE_ID platePos;//取走位置
		ItemInfo_i sthID;//所添加物品的ID号；
		GCTakeSthFromPlateRst::TakeRstType takeRst;//取结果，数值意义参见对应GC协议;
		bool isFulfilCon;//是否满足执行条件；
	}MGTakeSthFromPlateRst;
	MSG_SIZE_GUARD(MGTakeSthFromPlateRst);

	//#define  M_G_PLATE_EXEC_RST       0x515a   //托盘执行结果；
	typedef struct StrMGPlateExecRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLATE_EXEC_RST;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCPlateExecRst::PlateExecRstType execRst;///操作结果，数值意义参见对应GC协议；
	}MGPlateExecRst;
	MSG_SIZE_GUARD(MGPlateExecRst);

	//#define  M_G_CLOSE_PLATE          0x515b   //对玩家关托盘的响应，或者主动关玩家托盘；
	typedef struct StrMGClosePlate
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_CLOSE_PLATE;
		PlayerID lNotiPlayerID;//玩家自身ID
	}MGClosePlate;
	MSG_SIZE_GUARD(MGClosePlate);

	///...mapsrv发往gatesrv的消息结构
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///gatesrv发往dbsrv的消息结构...

	typedef struct StrGDPlayerInfoLogin	// 登录请求玩家角色列表
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_PLAYER_INFO_LOGIN;
		PlayerID playerID;//玩家唯一标识号，GateSrv生成；
		char szAccount[/*MAX_NAME_SIZE*/32];
	}GDPlayerInfoLogin;
	MSG_SIZE_GUARD(GDPlayerInfoLogin);

	typedef struct StrGDPlayerInfo	// 请求玩家角色具体信息
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_PLAYER_INFO;
		PlayerID playerID;//玩家唯一标识号，GateSrv生成；
		char szAccount[/*MAX_NAME_SIZE*/32];
		char strNickName[/*MAX_NAME_SIZE*/32];	//昵称,最大8个汉字
		unsigned int uiRoleID;
		TYPE_ID sequenceID;		//网络序列ID
	}GDPlayerInfo;
	MSG_SIZE_GUARD(GDPlayerInfo);

	typedef struct StrGDChoosePlayerInfo	// 选择一个角色
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_CHOOSE_PLAYER_INFO;
		PlayerID playerID;//玩家唯一标识号
		unsigned int uiPlayerInfoID;
	}GDChoosePlayerInfo;
	MSG_SIZE_GUARD(GDChoosePlayerInfo);

	typedef struct StrGDNewPlayerInfo // 创建玩家角色
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_NEW_PLAYER_INFO;
		PlayerID playerID;//玩家唯一标识号
		char szAccount[/*MAX_NAME_SIZE*/32];
		char strNickName[/*MAX_NAME_SIZE*/32];	//昵称,最大8个汉字
		unsigned short wRace;//种族；
		unsigned short wSex;//性别；
		unsigned int nClass;//人物职业
		unsigned int nFace;//脸型
		unsigned int nHair;//发型
		unsigned short usPortrait;// 头像ID
		TYPE_ID  sequenceID;	//序列ID
	}GDNewPlayerInfo;
	MSG_SIZE_GUARD(GDNewPlayerInfo);

	typedef struct StrGDUpdatePlayerInfo // 更新角色信息
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_UPDATE_PLAYER_INFO;
		PlayerID playerID;//玩家唯一标识号
		bool     isOffline;//玩家是否下线，如果下线，则无需再缓存该玩家信息；
		unsigned int uiRoleID;
		unsigned char ucIndex;//索引
		bool bEnd;//是否数据结束
		unsigned short usSendByte;
		char pszData[486];//数据
	}GDUpdatePlayerInfo;
	MSG_SIZE_GUARD(GDUpdatePlayerInfo);

	//typedef struct StrGDUpdatePlayerPkg // 更新包裹信息
	//{
	//	static const unsigned char  byPkgType = G_D;
	//	static const unsigned short wCmd = G_D_UPDATE_PLAYER_PKG;
	//	PlayerID playerID;//玩家唯一标识号
	//	unsigned int uiRoleID;//角色ID号;
	//	ItemInfo_i  pkgData[10];
	//}GDUpdatePlayerPkg;
	//MSG_SIZE_GUARD(GDUpdatePlayerPkg);

	typedef struct StrGDDeletePlayerInfo // 删除角色信息
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_DELETE_PLAYER_INFO;
		PlayerID playerID;//玩家唯一标识号
		char szAccount[/*MAX_NAME_SIZE*/32];
		char strNickName[/*MAX_NAME_SIZE*/32];	//昵称,最大8个汉字
		unsigned int uiPlayerInfoID;
	}GDDeletePlayerInfo;
	MSG_SIZE_GUARD(GDDeletePlayerInfo);

	typedef struct strGDBatchItemSequence //获取批量物品流水号
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_BATCH_ITEM_SEQUENCE;
		unsigned short uiServerID;
	}GDBatchItemSequence;
	MSG_SIZE_GUARD(GDBatchItemSequence);

	//typedef struct strGDUpdateItemInfo
	//{
	//	static const unsigned char byPkgType = G_D;
	//	static const unsigned short wCmd = G_D_UPDATE_ITEM_INFO;
	//	PlayerID   playerID;//通知哪个玩家的相关信息；
	//	unsigned char ucIndex;//索引
	//	EquipItemInfo itemInfo;
	//}GDUpdateItemInfo;
	//MSG_SIZE_GUARD(GDUpdateItemInfo);

	typedef struct StrGDUpdatePlayerChangeBaseInfo
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_UPDATE_PLAYER_BASE_INFO;
		unsigned int            playerUID;                                         
		TYPE_UI16              	usMapID;                                    	//地图ID
		TYPE_POS               	iPosX;                                      	//X坐标
		TYPE_POS               	iPosY;                                      	//Y坐标
		TYPE_UI32              	uiHP;                                       	//HP
		TYPE_UI32              	uiMP;                                       	//MP
		TYPE_SI16              	sGoodEvilPoint;                             	//善恶值
		TYPE_TIME              	uiEvilTime;                                 	//恶魔时间
		TYPE_UI16              	usLevel;                                    	//等级
		TYPE_UI32              	dwExp;                                      	//经验
		TYPE_UI8               	usPhysicsLevel;                             	//物理技能等级
		TYPE_UI8               	usMagicLevel;                               	//魔法技能等级
		TYPE_UI32              	dwPhysicsExp;                               	//物理技能熟练度
		TYPE_UI32              	dwMagicExp;                                 	//魔法技能熟练度
		TYPE_UI16              	usPhysicsAllotPoint;                        	//可分配物理技能点数
		TYPE_UI16              	usMagicAllotPoint;                          	//可分配魔法技能点数
		TYPE_UI16              	usPhyTotalGetPoint;                         	//总共获得的物理技能点数
		TYPE_UI16              	usMagTotalGetPoint;                         	//总共获得的魔法技能点数
		TYPE_UI32              	dwSilverMoney;                                 	//银币
		TYPE_UI32              	dwGoldMoney;                                   	//金币
		TYPE_UI32              	racePrestige;                               	//种族声望
		TYPE_UI16               strength;                                   	//力量
		TYPE_UI16               vitality;                                   	//体质
		TYPE_UI16               agility;                                    	//敏捷
		TYPE_UI16               spirit;                                     	//精神
		TYPE_UI16               intelligence;                               	//智力
		TYPE_UI16               unallocatePoint;                            	//未分配属性点数
		TYPE_UI32               totalExp;                                   	//总的经验
	}GDUpdatePlayerChangeBaseInfo;
	MSG_SIZE_GUARD(GDUpdatePlayerChangeBaseInfo)

	typedef struct StrGDUpdateItemInfo
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_UPDATE_ITEM_INFO;
		unsigned int playerUID;
		bool  isEquip;
		unsigned short itemIndex;
		ItemInfo_i     itemInfo;
	}GDUpdateItemInfo;
	MSG_SIZE_GUARD(GDUpdateItemInfo);

	typedef struct StrGDUpdateTaskRdInfo
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_UPDATE_TASK_INFO;
		unsigned int playerUID;
		bool isBackTask;
		unsigned short taskIndex;
		TaskRecordInfo taskInfo;
	}GDUpdateTaskRdInfo;
	MSG_SIZE_GUARD( GDUpdateTaskRdInfo );


	typedef struct strGDGetClientData
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_GET_CLIENT_DATA;
		PlayerID   playerID;//通知哪个玩家的相关信息；
		unsigned int uiRoleID;
	}GDGetClientData;
	MSG_SIZE_GUARD(GDGetClientData);

	typedef struct strGDSaveClientData
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_SAVE_CLIENT_DATA;
		PlayerID   playerID;//通知哪个玩家的相关信息；
		unsigned int uiRoleID;

		bool                	isOK;                         	//
		USHORT              	index;                        	//
		UINT                	dataSize;                     	//
		CHAR                	clientData[400];
	}GDSaveClientData;
	MSG_SIZE_GUARD(GDSaveClientData)

	enum QUERY_PLAYER_EXIST_TYPE
	{
		E_QUERY_MAIL,
		E_QUERY_SHOP,
	};

	typedef struct strGDQueryPlayerExist
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_QUERY_PLAYER_EXIST;
		unsigned int queryUID;
		char recvPlayerName[MAX_NAME_SIZE];
		QUERY_PLAYER_EXIST_TYPE queryType;
	}GDQueryPlayerExist;
	MSG_SIZE_GUARD(GDQueryPlayerExist)

	//#define G_D_REQ_CITY_INFO   0x170f	//单个mapsrv申请管理地图中的关卡信息
	typedef struct strGDReqCityInfo
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_REQ_CITY_INFO;
		unsigned int reqMapsrvID;		//请求的MAP ID
		int nMapNum;					//管理地图的数目；
		unsigned long lMapCode[64];		//管理的地图代码；
	}GDReqCityInfo;
	MSG_SIZE_GUARD(GDReqCityInfo)

	//#define G_D_UPDATE_CITY_INFO 0x1710		//更新关卡地图信息
	typedef struct strGDUpdateCityInfo
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_UPDATE_CITY_INFO;
		CityInfo updateCityInfo;
	}GDUpdateCityInfo;
	MSG_SIZE_GUARD(GDUpdateCityInfo)

	typedef struct strGDHeartbeatCheckRst
	{
		static const unsigned char byPkgType = G_D;
		static const unsigned short wCmd = G_D_HEARTBEAT_CHECK_RST;
		unsigned int sequenceID;
	}GDHeartbeatCheckRst;
	MSG_SIZE_GUARD(GDHeartbeatCheckRst)


	///...gatesrv发往dbsrv的消息结构
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///dbsrv发往gatesrv的消息结构...
	typedef struct StrDGPlayerInfoLogin // 登录返回对应玩家的角色列表
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_PLAYER_INFO_LOGIN;
		enum {
			FLAG_INVALID, //查询玩家信息失败
			FLAG_OK,//查询玩家信息成功
			FLAG_MSGEND//标记传送玩家角色信息结束
		};
		unsigned char byFlag;
		PlayerID   playerID;//通知哪个玩家的相关信息；
		//PlayerInfoLogin playerInfo[2];//暂时设2个角色，3个角色超过了507限制;
		PlayerInfoLogin playerInfo;
		PlayerInfo_Fashions fashion;
		unsigned short tag;
	}DGPlayerInfoLogin;
	MSG_SIZE_GUARD(DGPlayerInfoLogin);

	typedef struct StrDGPlayerInfo // 登录返回对应玩家的角色列表
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_PLAYER_INFO;
		enum {
			FLAG_INVALID, //查询玩家信息失败
			FLAG_OK //查询玩家信息成功
		};
		unsigned char byFlag;
		PlayerID   playerID;//通知哪个玩家的相关信息；
		unsigned char ucIndex;//索引
		bool bEnd;//是否结束
		unsigned short usSendByte;
		char pszData[490];//数据
		TYPE_ID sequenceID;		//网络序列ID
	}DGPlayerInfo;
	MSG_SIZE_GUARD(DGPlayerInfo);

    //#define D_G_PLAYER_PKG        0x7106  //玩家角色包裹信息；
	typedef struct StrDGPlayerPkg // 返回对应玩家的角色列表
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_PLAYER_PKG;
		PlayerID   playerID;//通知哪个玩家的相关信息；
		ItemInfo_i playerPkg[10];//玩家包裹信息；
	}DGPlayerPkg;
	MSG_SIZE_GUARD(DGPlayerPkg);

	typedef struct StrDGChoosePlayerInfoReturn // 返回所选角色的详细信息 
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_CHOOSE_PLAYER_INFO_RETURN;
		PlayerID playerID;//玩家唯一标识号
		int iRet;
	}DGChoosePlayerInfoReturn;
	MSG_SIZE_GUARD(DGChoosePlayerInfoReturn);

	typedef struct StrDGNewPlayerInfoReturn	// 创建玩家角色返回
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_NEW_PLAYR_INFO_RETURN;
		
		enum 
		{
			ISSELECTOK_SUCCESS             	=	0,  //成功
			ISSELECTOK_FAILED              	=	1,  //错误
			ISSELECTOK_EXIST_SAME_NAME_ROLE	=	2   //已存在同名角色
		};

		PlayerID playerID;//玩家唯一标识号
		int iRet;
		PlayerInfoLogin playerInfoLogin;
		TYPE_ID sequenceID;
	}DGNewPlayerInfoReturn;
	MSG_SIZE_GUARD(DGNewPlayerInfoReturn);

	typedef struct StrDGUpdatePlayerInfoReturn // 更新角色信息返回
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_UPDATE_PLAYER_INFO_RETURN;
		PlayerID playerID;//玩家唯一标识号
		int iRet;
	}DGUpdatePlayerInfoReturn;
	MSG_SIZE_GUARD(DGUpdatePlayerInfoReturn);

	typedef struct StrDGDeletePlayerInfoReturn // 删除角色信息返回
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_DELETE_PLAYER_INFO_RETURN;
		PlayerID playerID;//玩家唯一标识号
		unsigned int uiRoleID;
		int iRet;
	}DGDeletePlayerInfoReturn;
	MSG_SIZE_GUARD(DGDeletePlayerInfoReturn);

	typedef struct strDGBatchItemSequence //获取批量物品流水号
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd = D_G_BATCH_ITEM_SEQUENCE;
		unsigned short uiServerID;
		bool bSuccess;
		unsigned int uiStartSequence;
		unsigned int uiEndSequence;
	}DGBatchItemSequence;
	MSG_SIZE_GUARD(DGBatchItemSequence);

	typedef struct strDGUpdateItemInfo
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd = D_G_ITEM_INFO;
		PlayerID playerID;//玩家唯一标识号
		bool bUpdateSuccess;
	}DGUpdateItemInfo;
	MSG_SIZE_GUARD(DGUpdateItemInfo);

	typedef struct strDGGetClientData
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd = D_G_GET_CLIENT_DATA;
		PlayerID   playerID;//通知哪个玩家的相关信息；

		bool                	isOK;                         	//
		USHORT              	index;                        	//
		UINT                	dataSize;                     	//
		CHAR                	clientData[400];
	}DGGetClientData;
	MSG_SIZE_GUARD(DGGetClientData)

	typedef struct strDGQueryPlayerExistRst
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd =  D_G_QUERY_PLAYER_EXIST_RST;
		unsigned int queryUID;//查询id号
		char recvPlayerName[MAX_NAME_SIZE];
		QUERY_PLAYER_EXIST_TYPE queryType;
		unsigned int playeruid;//玩家的唯一编号
		bool         playerExist;
	}DGQueryPlayerExistRst;
	MSG_SIZE_GUARD( DGQueryPlayerExistRst );

	//#define D_G_CITY_INFO_RESULT	0x710c	//返回与mapsrv管理地图相对应的city信息
	typedef struct strDGCityInfoResult
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd =  D_G_CITY_INFO_RESULT;
		unsigned int reqMapsrvID;	//查询id的mapsrvID号
		CityInfo cityArr[20];		//城市的信息
		unsigned int cityNum;		//城市的个数	
	}DGCityInfoResult;
	MSG_SIZE_GUARD( DGCityInfoResult );

	typedef struct strDGHeartbeatCheckReq
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd =  D_G_HEARTBEAT_CHECK_REQ;
		unsigned int sequenceID;
	}DGHeartbeatCheckReq;
	MSG_SIZE_GUARD( DGHeartbeatCheckReq )

	//#define D_G_EXCEPTION_NOTIFY	0x710e	//异常通知
   typedef struct strDGExceptionNotify
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd =  D_G_EXCEPTION_NOTIFY;
		unsigned int exceptionReason;		//异常原因
		unsigned int srvId;					//srvId 
	}DGExceptionNotify;
	MSG_SIZE_GUARD( DGExceptionNotify )

	///...dbsrv发往gatesrv的消息结构
	//////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////

	//@拾取一个道具
	typedef struct StrGMPickItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd  = G_M_PICKITEM;
		PlayerID playID;
		unsigned long pkgID;//道具包编号
		unsigned long ItemID;//本次要添加的道具列表
		TYPE_INDEX ItemPkgIndex;//道具放入包裹位置
	}GMPickItem;
	MSG_SIZE_GUARD(GMPickItem);

	//@拾取所有道具
	typedef struct StrGMPickAllItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PICKALLITEM;
		PlayerID playID;
		unsigned long pkgID;//道具包的编号
	}GMPickAllItem;
	MSG_SIZE_GUARD(GMPickAllItem);

	typedef struct strGMBatchItemSequence //获取批量物品流水号
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_BATCH_ITEM_SEQUENCE;
		bool bSuccess;
		unsigned int uiStartSequence;
		unsigned int uiEndSequence;
	}GMBatchItemSequence;
	MSG_SIZE_GUARD(GMBatchItemSequence);

	typedef struct StrGMUseItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_USEITEM;
		PlayerID lPlayerID;
		unsigned long itemUID;
		TYPE_INDEX itemIndex;
		TYPE_POS attackPosX;
		TYPE_POS attackPosY;
		PlayerID targetID;
	}GMUseItem;
	MSG_SIZE_GUARD(GMUseItem);

	typedef struct StrGMEquipItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_EQUIPITEM;
		PlayerID lPlayerID;
		unsigned long equipitemUID;
		unsigned int  equipIndex;
		TYPE_INDEX equipItemIndex;
		TYPE_INDEX equipReturnIndex;
	}GMEquipItem;
	MSG_SIZE_GUARD(GMEquipItem);

	//@玩家查询一个道具包的信息
	typedef struct StrGMQueryItemPkg
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_QUEARY_ITEMPACK;
		PlayerID lplayerID;//玩家的ID
		unsigned long pkgID;//道具包的ID号
		unsigned int  queryType;
	}GMQueryItemPkg;
	MSG_SIZE_GUARD(GMQueryItemPkg);


	typedef struct StrGMUpdateItemInfo
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_UPDATE_ITEM_INFO;
		PlayerID   playerID;//通知哪个玩家的相关信息；
		bool bUpdateSuccess;
	}GMUpdateItemInfo;
	MSG_SIZE_GUARD(GMUpdateItemInfo);


	typedef struct StrMGItemPackAppear
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ITEMPACK_APPEAR;
		PlayerID lNotiPlayerID;//通知对象ID号；
		unsigned long pkgID;//道具包的ID号
		TYPE_POS px;//道具包的X位置
		TYPE_POS py;//道具包的Y位置
		int  type;//道具包的类型
		PlayerID dropOwnerID;//掉落所有者的ID号
		PlayerID  OwnerID;//道具所属玩家
		TYPE_INDEX itemIndex;//道具编号
		ItemInfo_i itemInfo;//掉落道具信息
	}MGItemPackAppear;
	MSG_SIZE_GUARD(MGItemPackAppear);

	typedef struct StrMGItemPackDisAppear
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ITEMPACK_DISAPPEAR;
		PlayerID lNotiPlayerID;//通知对象的ID号
		unsigned long pkgID;//道具包的编号
	}MGItemPackDisAppear;
	MSG_SIZE_GUARD(MGItemPackDisAppear);

	typedef struct StrMGItemPackInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ITEMPACK_INFO;
		PlayerID lNotiPlayerID;//消息所需通知人的ID号
		unsigned int pkgID;//道具包的编号
		unsigned int silverMoney;//道具包中所含有的金钱数
		unsigned int goldMoney;//道具包中所含有的金钱数
		ItemInfo_i  itemInfoArr[MAXPACKAGEITEM];
		unsigned char itemCount;
		unsigned int  queryType;
	}MGItemPackInfo;
	MSG_SIZE_GUARD(MGItemPackInfo);

	typedef struct StrMGNoticeEquipItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_EQUIP_PLAYERITEM;
		PlayerID lNotiPlayerID;//消息所需通知人的ID号
		PlayerID equipPlayer;//装备的人
		ItemInfo_i equipItem;//装备的道具
		unsigned int equipIndex;//装备的位置
	}MGNoticeEquipItem;
	MSG_SIZE_GUARD(MGNoticeEquipItem);

	//@brief:拾取道具失败
	typedef struct StrMGPickItemFaild
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PICKITEM_FAILED;
		PlayerID lNotiPlayerID;
		unsigned long itemID;
		EPickItemResult result;//用编号便是理由
	}MGPickItemFaild;
	MSG_SIZE_GUARD(MGPickItemFaild);

	//@brief:一个玩家提取了道具
	typedef struct StrMGOnePlayerPickItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ONECLIENTPICKITEM;
		PlayerID lNotiPlayerID;
		unsigned long pkgIndex;
		unsigned long itemID;
		bool bIsPickMoney;
	}MGOnePlayerPickItem;
	MSG_SIZE_GUARD(MGOnePlayerPickItem);

	typedef struct StrMGNoticeTeamMemberGetNewItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_TEAMMEMBER_GET_NEWITEM;
		PlayerID lNotiPlayerID;
		unsigned long teamID;
		unsigned long itemTypeID;
		PlayerID pickPlayerID;
	}MGNoticeTeamMemberGetNewItem;
	MSG_SIZE_GUARD( MGNoticeTeamMemberGetNewItem );

	typedef struct strMGBatchItemSequence //获取批量物品流水号
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_BATCH_ITEM_SEQUENCE;
	}MGBatchItemSequence;
	MSG_SIZE_GUARD(MGBatchItemSequence);

	typedef struct StrMGPlayerUseItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ONEPLAYER_USEITEM;
		PlayerID lNotiPlayerID;
		PlayerID lUsePlayerID;
		ItemInfo_i litemInfo;
	}MGPlayerUseItem;
	MSG_SIZE_GUARD(MGPlayerUseItem);

	//@brief:使用道具的结果
	typedef struct StrMGUseItemResult
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_USEITEM_RESULT;
		PlayerID lNotiPlayerID;
		bool     result;
	}MGUseItemResult;
	MSG_SIZE_GUARD(MGUseItemResult);

	typedef struct StrMGUnUseItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UNUSEITEM;
		PlayerID lNotiPlayerID;
		PlayerID playerID;//玩家ID号
		unsigned long equipIndex;//装备的索引
	}MGUnUseItem;
	MSG_SIZE_GUARD(MGUnUseItem);

	//@brief:不使用道具的结果
	typedef struct StrMGUnUseItemResult
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UNUSEITEM_RESULT;
		PlayerID lNotiPlayerID;
		bool result;
	}MGUnUseItemResult;
	MSG_SIZE_GUARD(MGUnUseItemResult);

	//玩家不使用道具
	typedef struct StrGMUnUseItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_UNUSEITEM;
		PlayerID unUsePlayer;
		unsigned int from;
		unsigned int to;
	}GMUnUseItem;
	MSG_SIZE_GUARD(GMUnUseItem);

	//玩家交换道具
	typedef struct StrGMSwapItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SWAPITEM;
		PlayerID playerID;
		unsigned int from;
		unsigned int to;
	}GMSwapItem;
	MSG_SIZE_GUARD(GMSwapItem);

	//玩家丢弃道具
	typedef struct StrGMPlayerDropItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_DROPITEM;
		PlayerID dropPlayer;//丢弃道具的玩家
		unsigned long itemUID;//丢弃的道具的编号
		TYPE_INDEX itemIndex;
	}GMPlayerDropItem;
	MSG_SIZE_GUARD(GMPlayerDropItem);

	typedef struct StrGMSplitItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SPLITITEM;
		PlayerID splitPlayer;
		unsigned long itemUID;
		unsigned int splitNum;
	}GMSplitItem;
	MSG_SIZE_GUARD(GMSplitItem);

	//交换装备栏道具
	typedef struct StrGMSwapEquipItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SWAP_EQUIPITEM;
		PlayerID swapPlayer;
		char from;
		char to;
	}GMSwapEquipItem;
	MSG_SIZE_GUARD(GMSwapEquipItem);


	////@brief:交换包裹道具的结果
	//typedef struct StrGMSwapItemResult
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_SWAPITEM_RESULT;
	//	PlayerID lNotiPlayerID;
	//	bool result;
	//}GMSwapItemResult;
	//MSG_SIZE_GUARD(GMSwapItemResult);

	//查询包裹的页数中的道具信息
	typedef struct StrGMQueryItemPkgPage
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_QUERY_PKGITEMPAGE;
		PlayerID queryPlayer;
		char     pageIndex;
	}GMQueryItemPkgPage;
	MSG_SIZE_GUARD(GMQueryItemPkgPage);

	//#define G_M_AOE_ATTACK 0x1588  //地面AOE技能
	typedef struct StrGMAoeAttack
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_AOE_ATTACK;
		PlayerID attackID;
		TYPE_ID skillID;
		TYPE_POS attackPosX;
		TYPE_POS attackPosY;
		float playerPosX;	
		float playerPosY;
		TYPE_ID sequenceID;
	}GMAoeAttack;
	MSG_SIZE_GUARD(GMAoeAttack);

	//#define G_M_PUNISHMENT_START 0x1589			//通知mapsrv天谴开始
	typedef struct StrGMPunishmentStart
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PUNISHMENT_START;
		PunishRaceInfo humanRace;		//最多就为3人不会再多
		PunishRaceInfo elfRace;			
		PunishRaceInfo ekkaRace;
	}GMPunishmentStart;
	MSG_SIZE_GUARD(GMPunishmentStart);

	//#define G_M_PUNISHMENT_END 0x158a			//通知mapsrv天谴结束
	typedef struct StrGMPunishmentEnd
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PUNISHMENT_END;
	}GMPunishmentEnd;
	MSG_SIZE_GUARD(GMPunishmentEnd);

	//#define G_M_PUNISH_PLAYER_INFO 0x158b		//更新天谴玩家的信息
	typedef struct StrGMPunishPlayerInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PUNISH_PLAYER_INFO;
		PunishPlayerInfo punishPlayer;
	}GMPunishPlayerInfo;
	MSG_SIZE_GUARD(GMPunishPlayerInfo);


	//#define G_M_GMCMD_REQUEST 0x158c	//通知mapsrv GM命令
	typedef struct StrGMGmCmdRequest
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_GMCMD_REQUEST;
		char strChat[256];			//聊天内容
		PlayerID requestPlayerID;	//申请玩家ID
		PlayerID targetPlayerID;	//目标玩家ID
	}GMGmCmdRequest;
	MSG_SIZE_GUARD(GMGmCmdRequest);

	//交换装备栏刀具是否成功
	typedef struct StrMGSwapEquipItemRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SWAP_EQUIPITEM_RST;
		PlayerID lNotiPlayerID;
		PlayerID swapPlayerID;
		char from;
		char to;
		bool isSuccess;
	}MGSwapEquipItemRst;
	MSG_SIZE_GUARD(MGSwapEquipItemRst);

	//玩家所拥有包裹的简明信息
	typedef struct StrMGItemPkgConciseInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ITEMPKG_CONCISEINFO;
		PlayerID   lNotiPlayerID;
		ItemPosition pkgInfo[PKG_PAGE_SIZE];
		//int        pageIndex;//包裹里的第几页
	}MGItemPkgConciseInfo;
	MSG_SIZE_GUARD(MGItemPkgConciseInfo);

	//玩家丢弃道具的结果
	typedef struct StrMGDropItemResult
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_DROPITEM_RESULT;
		PlayerID lNotiPlayerID;
		unsigned int uiItemID;
		bool     result;//丢弃的结果
	}MGDropItemResult;
	MSG_SIZE_GUARD(MGDropItemResult);

	//#define  M_G_HONOR_MASK           0x5152 //玩家选择显示的称号；
	typedef struct StrMGHonorMask
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_HONOR_MASK;
		PlayerID     lNotiPlayerID;
		EHonorMask   honorMask;
	}MGHonorMask;
	MSG_SIZE_GUARD(MGHonorMask);
	//typedef struct StrMGMoneyUpdate
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_MONEY_UPDATE;
	//	PlayerID lNotiPlayerID;
	//	TYPE_NUM money;
	//}MGMoneyUpdate;
	//MSG_SIZE_GUARD(MGMoneyUpdate);

	typedef struct StrMGSetPkgItem
	{
		static const unsigned char byPkgType= M_G;
		static const unsigned short wCmd = M_G_SET_PKGITEM;
		PlayerID lNotiPlayerID;
		TYPE_NUM pkgIndex;
		ItemInfo_i itemInfo;
	}MGSetPkgItem;
	MSG_SIZE_GUARD(MGSetPkgItem);

	typedef struct StrMGPlayerBusying
	{
		enum
		{
			E_GROUPTEAM = 0x0,//组队
			E_RIDETEAM,//骑乘队伍
		};
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd =  M_G_PLAYERBUSYING;
		PlayerID lNotiPlayerID;
		char PlayerNickName[MAX_NAME_SIZE];
		int  errCode;
	}MGPlayerBusying;
	MSG_SIZE_GUARD(MGPlayerBusying);


////////////////////////骑乘系统////////////////////////////////////////
	//玩家上马
	typedef struct StrGMActiveMount
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_ACTIVEMOUNT;
		PlayerID playerID;//使用人的ID号
		ItemInfo_i itemUID;//骑乘道具在服务器的编号
	}GMActiveMount;
	MSG_SIZE_GUARD(GMActiveMount);

	//玩家邀请别人上马
	typedef struct StrGMInviteMount
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_INVITEMOUNT;
		PlayerID playerID;//使用人的ID号
		PlayerID targetID;//邀请的目标玩家
	}GMInviteMount;
	MSG_SIZE_GUARD(GMInviteMount);

	//玩家是否同意被邀请上马
	typedef struct StrGMAgreeBeInvite
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_AGREE_BEINVITE;
		PlayerID playerID;//使用人的ID号
		PlayerID launchID;//邀请人的ID
		bool     isAgree;//是否同意
	}GMAgreeBeInvite;
	MSG_SIZE_GUARD(GMAgreeBeInvite);

	//玩家申请离开骑乘队伍
	typedef struct  StrGMLeaveMountTeam
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_LEAVE_MOUNTTEAM;
		PlayerID playerID;//使用人的ID号
	}GMLeaveMountTeam;
	MSG_SIZE_GUARD(GMLeaveMountTeam);

	enum ERIDESTATE
	{
		E_RIDE_EQUIP=0x0,//骑乘装备
		E_RIDE_CONSUME,//骑乘消耗品
		E_RIDE_ASSIST,//骑乘队员
		E_RIDE_INFO_UPDATE,//仅仅更新骑乘状态的信息
		E_UNRIDE,
		E_RIDESTATE_MAX,
	};

	//登陆新地图时，设置玩家的骑乘状态
	typedef struct StrGMNewMapSetPlayerRideState
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_NEWMAP_SETRIDESTATE;
		PlayerID   playerID;
		ERIDESTATE rideState;//玩家的骑乘状态
		TYPE_NUM   attachInfo;//骑乘的附属信息
	}GMNewMapSetPlayerRideState;
	MSG_SIZE_GUARD(GMNewMapSetPlayerRideState);

	//设置玩家的骑乘状态标志
	typedef struct StrMGMountStateChange
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_MOUNTSTATE_CHANGE;
		PlayerID lNotiPlayerID;
		ERIDESTATE rideState;//玩家的骑乘状态
		TYPE_NUM   attachInfo1;//骑乘的附属信息
		TYPE_NUM   attachInfo2;//骑乘的附属信息2
	}MGMountStateChange;
	MSG_SIZE_GUARD(MGMountStateChange);

	//骑乘队长切换地图时
	typedef struct StrGMRideLeaderSwitchMap
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RIRDELEADER_SWITCHMAP;
		PlayerID playerID;
		TYPE_NUM usMapID;
		TYPE_NUM oldMapID;
		TYPE_NUM iPosX;
		TYPE_NUM iPosY;
	}GMRideLeaderSwitchMap;
	MSG_SIZE_GUARD(GMRideLeaderSwitchMap);


	//骑乘队员对于骑乘队长更换地图的反应
	typedef struct StrMGOnRideLeaderSwichMap
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ONRIDELEADER_SWITCHMAP;
		PlayerID lNotiPlayerID;
		TYPE_NUM usMapID;
		TYPE_NUM oldMapID;
		TYPE_NUM iPosX;
		TYPE_NUM iPosY;
	}MGOnRideLeaderSwichMap;
	MSG_SIZE_GUARD(MGOnRideLeaderSwichMap);


	//#define M_G_REQ_JOIN_TEAM 0x5190	//加入组队
	typedef struct StrMGReqJoinTeam
	{
		static const unsigned short byPkgType = M_G;
		static const unsigned short wCmd = M_G_REQ_JOIN_TEAM;
		char invitePlayerName[32];  //邀请玩家的姓名
		int  nameLen;				//姓名长度
	}MGReqJoinTeam;
	MSG_SIZE_GUARD(MGReqJoinTeam);

	//#define G_M_REQBUFFEND  0x1540
	typedef struct  StrGMReqBuffEnd
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REQ_BUFFEND;
		unsigned int buffIndex;		//要结束的ID号
		PlayerID changePlayer;	 //要结束BUFF的玩家ID号
	}GMReqBuffEnd;
	MSG_SIZE_GUARD(GMReqBuffEnd);

	//#define G_M_USE_ASSISTSKILL	0x1541	//使用辅助技能
	typedef struct  StrGMUseAssistSkill
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_USE_ASSISTSKILL;
		PlayerID targetPlayer;		//对象
		PlayerID useSkillPlayer;	//使用的人
		bool bPlayer;		//是否玩家
		TYPE_ID skillID;	//技能ID
		bool isUnionSkill;	//是否是工会技能
	}GMUseAssistSkill;
	MSG_SIZE_GUARD(GMUseAssistSkill);

	////////////////////////////组队/////////////////////////////
	//#define G_M_REQ_JOIN_TEAM 0x1550	//向玩家所在的mapsrv发送组队消息
	typedef struct StrGMReqJoinTeam
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REQ_JOIN_TEAM;
		char invitePlayerName[32];	//邀请玩家的姓名		
		int  nameLen;	//名字长度
		PlayerID beInvitedPlayer;	//被邀请的玩家ID
	}GMReqJoinTeam;
	MSG_SIZE_GUARD(GMReqJoinTeam);


	/////////////////////////////骑乘///////////////////////////////////////
	//使用骑乘道具失败都使用该消息
	typedef struct StrMGActiveMountFail
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ACTIVEMOUNT_FAIL;
		PlayerID lNotiPlayerID;
		TYPE_NUM reason;//失败原因
	}MGActiveMountFail;
	MSG_SIZE_GUARD(MGActiveMountFail);

	//骑乘队伍出现在玩家面前时的消息
	typedef struct StrMGMountTeamInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_MOUNTTEAM_APPEAR;
		PlayerID lNotiPlayerID;
		PlayerID MountTeam[MAXMOUNTCOUNT];//队伍成员
		ItemInfo_i rideItem;//所骑乘的道具的编号
		TYPE_NUM x;//骑乘队伍出现的坐标X
		TYPE_NUM y;//骑乘队伍出现的坐标Y
		char     mountType;//骑乘装备类型
	}MGMountTeamInfo;
	MSG_SIZE_GUARD(MGMountTeamInfo);

	//骑乘队伍解散
	typedef struct StrMGMountTeamDisBand
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_MOUNTTEAM_DISBAND;
		PlayerID lNotiPlayerID;
		PlayerID mountTeamID;
	}MGMountTeamDisBand;
	MSG_SIZE_GUARD(MGMountTeamDisBand);

	//被人邀请加入队伍
	typedef struct StrMGBeInviteMount
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_BEINVITE_MOUNTTEAM;
		PlayerID lNotiPlayerID;
		PlayerID launchID;//发起者ID号
	}MGBeInviteMount;
	MSG_SIZE_GUARD(MGBeInviteMount);


	//邀请其他玩家邀请的结果
	typedef struct StrMGInviteResult
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_INVITE_RESULT;
		PlayerID lNotiPlayerID;
		PlayerID targetID;//同意的玩家
		bool     isAgree;//是否同意骑乘
	}MGInviteResult;
	MSG_SIZE_GUARD(MGInviteResult);

/////////////////////////// 任务//////////////////////////////////////////
	//删除任务
	typedef struct StrGMDeleteTask
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_DELETE_TASK;
		PlayerID playerID;
		unsigned int TaskID;
		ETaskType	taskType;
	}GMDeleteTask;
	MSG_SIZE_GUARD(GMDeleteTask);

	//共享任务
	typedef struct StrGMShareTask
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SHARE_TASK;
		PlayerID playerID;
		unsigned int TaskID;
	}GMShareTask;
	MSG_SIZE_GUARD(GMShareTask);


//////////////////////////////////////////////////////////////////////////

	//玩家获得一个BUFF
	typedef struct StrMGPlayerBuffStart
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_BUFFSTART;
		PlayerID lNotiPlayerID;
		GCPlayerBuffStart buffStart;
	}MGPlayerBufferStart;
	MSG_SIZE_GUARD(MGPlayerBufferStart);

	//玩家的一个BUFF消失
	typedef struct StrMGPlayerBuffEnd
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_BUFFEND;
		PlayerID lNotiPlayerID;
		PlayerID changePlayer;
		BuffID   buffID;
		int nBuffIndex;
	}MGPlayerBuffEnd;
	MSG_SIZE_GUARD(MGPlayerBuffEnd);


	//#define  M_G_MONSTER_BUFF_END	   0x515c   //怪物ＢＵＦＦ消失的通知
	//玩家的一个BUFF消失
	typedef struct StrMGMonsterBuffEnd
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MONSTER_BUFF_END;
		TYPE_ID	 monsterID;    //怪物ＩＤ
		PlayerID lNotiPlayerID; //通知的玩家
		BuffID   buffID;    //buff type ID 　
		int nBuffIndex;		//怪物的buff栏位
	}MGMonsterBuffEnd;
	MSG_SIZE_GUARD(MGMonsterBuffEnd);

	//关于玩家持续性BUFF的消息更新
	typedef struct StrMGPlayerBuffUpdate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_BUFFUPDATE;
		PlayerID lNotiPlayerID;
		PlayerID lChangeID;
		int nChangeHP;
		int nPlayerCurrentHP;
		int nBuffIndex;
		BuffID buffID;
		PlayerID createBuffPlayer;	//创建这个BUFF的人的PLAYER ID 
		EBattleFlag battleFlag;
	}MGPlayerBuffUpdate;
	MSG_SIZE_GUARD(MGPlayerBuffUpdate);


	//#define  M_G_MONSTER_BUFFSTART    0x5153	//怪物的BUFF开始
	typedef struct StrMGMonsterBuffStart
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MONSTER_BUFFSTART;
		PlayerID lNotiPlayerID;
		GCMonsterBuffStart	buffStart;
	}MGMonsterBuffStart;
	MSG_SIZE_GUARD(MGMonsterBuffStart);

	//#define  M_G_MONSTER_BUFFUPDATE   0x5154   //怪物的HP BUFF的更新
	typedef struct StrMGMonsterBuffUpdate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_MONSTER_BUFFUPDATE;
		PlayerID lNotiPlayerID; //通知的玩家ID
		TYPE_ID monsterID;		//怪物ID
		int nChangeHP;			//变化的HP
		int nMonsterCurrentHP;	//怪物现在的HP
		int nBuffIndex;			//BUFF的栏位
		BuffID buffID;			//BUFF的ID
	}MGMonsterBuffUpdate;
	MSG_SIZE_GUARD(MGMonsterBuffUpdate);

	//#define M_G_REQ_BUFFEND 0x5155
	typedef struct StrMGReqBuffEnd
	{
		enum { FAILED, SUCCESS };
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_REQ_BUFFEND;
		PlayerID lNotiPlayerID; //通知的玩家ID
		int nErrorCode;			//是否能结束
	}MGReqBuffEnd;
	MSG_SIZE_GUARD(MGReqBuffEnd);


///////////////////////// 组队操作 /////////////////////////////////////////

#define  MAXTEAMMEMBER 7 //组队的最大人数

	//队伍的详细信息
	typedef struct StrGMTeamDetialInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TEAM_DETAILINFO;
		int          ArrSize;
		PlayerID     playerArr[MAXTEAMMEMBER];
		TYPE_NUM     memberSeqArr[MAXTEAMMEMBER];
		ITEM_SHAREMODE itemShareMode;
       EXPERIENCE_SHAREMODE expShareMode;
		ROLLITEM_MODE rollItemMode;
		int          TeamID;
		int          leaderIndex;
		PlayerID	  lNoticePlayerID;		//通知具体的某个人
	}GMTeamDetialInfo;
	MSG_SIZE_GUARD(GMTeamDetialInfo);

	//更新组员的详细信息
	typedef struct StrMGTeamMemberDetailInfoUpdate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_MEMBER_DETIALINFO_UPDATE;
		PlayerID            	teamMemberID;                 	//组员的ID
		TYPE_UI32           	uiHP;                         	//HP
		TYPE_UI32           	uiMP;                         	//MP
		TYPE_UI32             maxHP;								//最大HP
		TYPE_UI32             maxMP;								//最大MP
		TYPE_POS            	nPosX;                        	//该组员的位置信息
		TYPE_POS            	nPosY;                        	//该组员的位置信息
		PlayerInfo_5_Buffers	buffData;							//Buff信息
		USHORT              	mapID;                        	//地图ID
		USHORT					usLevel;							//等级信息
		TYPE_SI32               goodevilPoint;                   //善恶值
		PlayerID              lNotiPlayerID;					//通知的玩家信息
	}MGTeamMemberDetailInfoUpdate;
	MSG_SIZE_GUARD(MGTeamMemberDetailInfoUpdate);

	//退出队伍
	typedef struct StrGMTeamExit
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_TEAM_EXIT;
		int TeamID;
		PlayerID playerID;
		int memberSeq;
	}GMTeamExit;
	MSG_SIZE_GUARD(GMTeamExit);

	//进入队伍
	typedef struct StrGMTeamEntrance
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TEAM_ENTRANCE;
		int TeamID;
		PlayerID playerID;
		int memberSeq;
	}GMTeamEntrance;
	MSG_SIZE_GUARD(GMTeamEntrance);

	//更新组队中的玩家信息
	typedef struct StrGMUpdateTeamPlayerInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_UPDATE_PLAYERTEAMINFO;
		int TeamID;
		PlayerID playerID;
		int memberSeq;
	}GMUpdateTeamPlayerInfo;
	MSG_SIZE_GUARD(GMUpdateTeamPlayerInfo);

	//改变组队中的队长
	typedef struct StrGMChangeTeamLeader
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGETEAM_LEADER;
		int TeamID;
		PlayerID playerID;
	}GMChangeTeamLeader;
	MSG_SIZE_GUARD(GMChangeTeamLeader);

	//踢出组队中的队员
	typedef struct StrGMKickTeamPlayer
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TEAM_KICKPLAYER;
		int TeamID;
		PlayerID playerID;
	}GMKickTeamPlayer;
	MSG_SIZE_GUARD(GMKickTeamPlayer);

	//销毁组队
	typedef struct StrGMDisBandTeam
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TEAM_DISBAND;
		int TeamID;
	}GMDisBandTeam;
	MSG_SIZE_GUARD(GMDisBandTeam);

	//改变组队的物品分配模式
	typedef struct StrGMChangeTeamItemMode
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGETEAM_ITEMMODE;
		int TeamID;
		ITEM_SHAREMODE itemShareMode;
	}GMChangeTeamItemMode;
	MSG_SIZE_GUARD(GMChangeTeamItemMode);

	//改变组队的经验分配模式
	typedef struct StrGMChangeTeamExpreMode
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGETEAM_EXPREMODE;
		int TeamID;
		EXPERIENCE_SHAREMODE expreShareMode;
	}GMChangeTeamExpreMode;
	MSG_SIZE_GUARD(GMChangeTeamExpreMode);

	//改变组队的Roll物品等级
	typedef struct StrGMChangeTeamRollLevel
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGETEAM_ROLLLEVEL;
		int  TeamID;
		int  rollItemLevel;
	}GMChangeTeamRollLevel;
	MSG_SIZE_GUARD(GMChangeTeamRollLevel);

	typedef struct StrGMTeamMemberGetNewItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TEAMMEMBER_GETNEWITEM;
		unsigned int TeamID;
		unsigned int itemTypeID;
		PlayerID pickPlayerID;
	}GMTeamMemberGetNewItem;
	MSG_SIZE_GUARD(GMTeamMemberGetNewItem);

	//询问是否同意Roll物品
	typedef struct StrGMAgreeRollItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_AGREE_ROLLITEM;
		PlayerID playerID;
		int  rollItemID;
		bool agree;
	}GMAgreeRollItem;
	MSG_SIZE_GUARD(GMAgreeRollItem);

	//询问是否参加Roll点
	typedef struct StrMGReqAgreeRollItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_REQ_AGREEROLLITEM;
		PlayerID lNotiPlayerID; //通知的玩家ID
		int rollID;
		int rollItemID;
	}MGReqAgreeRollItem;
	MSG_SIZE_GUARD(MGReqAgreeRollItem);

	//Roll到的点数
	typedef struct StrMGRollItemNum
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ROLLITEM_NUM;
		PlayerID lNotiPlayerID; //通知的玩家ID
		int rollID;
		int num;
		char rollPlayerName[MAX_NAME_SIZE];
	}MGRollItemNum;
	MSG_SIZE_GUARD(MGRollItemNum);

	//本次Roll点的结果
	typedef struct StrMGRollItemResult
	{
		static const unsigned char byPkgType= M_G;
		static const unsigned short wCmd = M_G_ROLLITEM_RESULT;
		PlayerID lNotiPlayerID; //通知的玩家ID
		int rollID;//本次roll的编号
		TYPE_ID rollItemID;			//roll的物品
		char   rollPlayerName[MAX_NAME_SIZE];//Roll到的玩家姓名
	}MGRollItemResult;
	MSG_SIZE_GUARD(MGRollItemResult);

	//通知组队中的其他人，拾取了什么道具
	typedef struct StrMGTeamMemberPickItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_MEMBER_PICKITEM;
		PlayerID lNotiPlayerID;
		PlayerID pickPlayerID;
		int itemTypeID;
	}MGTeamMemberPickItem;
	MSG_SIZE_GUARD(MGTeamMemberPickItem);

//////////////////////////////////////////////////////////////////////////

	//#define  M_G_USE_ASSISTSKILL	   0x5156	//关于使用辅助技能
	typedef struct StrMGUseAssistSkill
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_USE_ASSISTSKILL;
		PlayerID lNotiPlayerID;		//通知的玩家ID
		PlayerID useSkillPlayer;	//使用技能的玩家
		PlayerID targetPlayer;		//目标玩家
		TYPE_ID skillID;		//技能ID]
		bool bPlayer;
		int nUseSkillPlayerSpPower;
		int nUseSkillPlayerSpExp;
		int nUseSkillPlayerSkillExp;
		EBattleResult nErrorCode;			//错误代码
		unsigned int skillCurseTime;		//技能动作时间
		unsigned int skillCooldownTime;    //技能CD时间
		int nUseSkillPlayerMp;	//使用玩家的Mp
	}MGUseAssistSkill;
	MSG_SIZE_GUARD(MGUseAssistSkill);
	

	//#define M_G_BROADCAST			   0x51fe	//关于范围通知的都是此协议,群发消息的前置消息
	typedef struct StrMGBroadCast
	{
//		StrMGBroadCast() : testseqID(0) {};
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_BROADCAST;
		static const unsigned int  NOTI_EVERYONE=0x76543210;
		int nCurNum;	//此次有效的人数, 若有效人数为NOTI_EVERYONE，则表示广播给全gate上的玩家；
		unsigned long arrPlayerPID[MAX_NOTIPLAYER_NUM_PERGATE];
	}MGBroadCast;
	MSG_SIZE_GUARD(MGBroadCast);


	//#define M_G_THREAT_LIST			  0x51fd	//怪物的威胁列表发送给客户端用于调试
	typedef struct StrMGThreatList
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_THREAT_LIST;
		PlayerID lNotiPlayerID;		//发送给哪个玩家所在的gatesrv
		ThreatList   threatList;
	}MGThreatList;
	MSG_SIZE_GUARD(MGThreatList);

	//#define M_G_ADD_FOEINFO   0x51a7		//MAPSRV增加仇人信息
	typedef struct StrMGAddFoeInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ADD_FOEINFO;
		TYPE_ID playerUID;		//加在哪个UID身上
		PlayerID foePlayerID;		//发送给仇人的PlayerID
		USHORT incidentPosX;		//发生时的posx	
		USHORT incidentPosY;		//发生时的posY
	}MGAddFoeInfo;
	MSG_SIZE_GUARD(MGAddFoeInfo);

	typedef struct StrMGRepairWearByItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_REPAIR_WEAR_BY_ITEM;
		
		enum eResult
		{
			REPAIRMAN_NO_REPAIR_FUNCTION = 0,	//修复道具没有修复功能
			TARGET_DO_NOT_REPAIR = 1,			//目标道具不可修复
			NO_ENOUGH_REPAIR_MONEY = 2,			//修复费用不足
			OTHER_ERROR = 3,					//其他错误
			OK = 4								//成功					
		};

		PlayerID lNotiPlayerID;	//发送给哪个玩家所在的gatesrv
		TYPE_ID  itemRepairman; //具有修复功能的物品
		TYPE_ID  repairTarget;	//待修复的物品
		eResult  tResult;       //结果
		TYPE_ID  newWear;       //新的耐久度
		TYPE_ID  uiRepairSilverMoney;              	//修复花费银币
		TYPE_ID  uiRepairGoldMoney;               	//修复花费金币
	}MGRepairWearByItem;
	MSG_SIZE_GUARD(MGRepairWearByItem);

	typedef struct StrMGNotifyWearChange
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NOTIFY_WEAR_CHANGE;
		PlayerID lNotiPlayerID;		//发送给哪个玩家所在的gatesrv
		UINT itemSize;  //物品数目
		WearChangedItems items[32]; //物品列表
	}MGNotifyWearChange;
	MSG_SIZE_GUARD(MGNotifyWearChange);

	//改造道具时的概率
	typedef struct StrMGChangeItemCri
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHANGEITEMCRI;
		PlayerID lNotiPlayerID;
		unsigned int changeCri;
	}MGChangeItemCri;
	MSG_SIZE_GUARD(MGChangeItemCri);

	////给道具Update的错误
	//typedef struct StrMGUpdateItemError
	//{
	//	static const unsigned char byPkgType= M_G;
	//	static const unsigned short wCmd = M_G_UPDATEITEMERROR;
	//	PlayerID lNotiPlayerID;
	//	EUpdateItemError  updateError;
	//}MGUpdateItemError;
	//MSG_SIZE_GUARD(MGUpdateItemError);


	//#define M_G_UPGRADE_SKILL_RESULT 0x15ab		//技能升级的请求结果
	typedef struct StrMGUpgradeSkillResult
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPGRADE_SKILL_RESULT;
		PlayerID lNotiPlayerID;		//发送给哪个玩家所在的gatesrv
		EUpgradeSkillResult 	errorCode;                    	//对应枚举EUpgradeSkillResult
		TYPE_ID             	oldSkillID;                   	//旧的要被替换的技能ID
		TYPE_ID             	newSkillID;                   	//新的技能ID
		TYPE_UI16           	phySkillPoint;                	//升级后的物理技能点
		TYPE_UI16           	magSkillPoint;                	//升级后的魔法技能点
	}MGUpgradeSkillResult;
	MSG_SIZE_GUARD(MGUpgradeSkillResult);


	//#define M_G_SPSKILL_UPGRADE 0x51ac			//SP技能升级
	typedef struct StrMGSpSkillUpgrade
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SPSKILL_UPGRADE;
		PlayerID lNotiPlayerID;										//发送给哪个玩家所在的gatesrv
		TYPE_ID             	oldSpSkillID;                 	//旧的要被替换的sp技能ID
		TYPE_ID             	newSpSkillID;                 	//新的SP技能ID
	}MGSpSkillUpgrade;
	MSG_SIZE_GUARD(MGSpSkillUpgrade);


	//#define M_G_ADD_SPSKILL_RESULT  0x15ad		//添加新的SP技能成功与否
	typedef struct StrMGAddSpSkillResult
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ADD_SPSKILL_RESULT;
		PlayerID lNotiPlayerID;										//发送给哪个玩家所在的gatesrv
		TYPE_ID             	addSpSkillID;                 	//要添加的SP技能ID
		bool             		bSuccess;                 		//成功与否
		int						nSkillDataIndex;					//在SKILL里的栏位
	}MGAddSpSkillResult;
	MSG_SIZE_GUARD(MGAddSpSkillResult);


	typedef struct StrMGCleanSkillPoint
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CLEAN_SKILLPOINT;
		PlayerID lNotiPlayerID;									//发送给哪个玩家所在的gatesrv
		USHORT              	assignablePhyPoint;           	//洗点以后的物理可分配点数
		USHORT              	assignableMagPoint;           	//洗店以后的魔法可分配点数
		UINT                	skillSize;                   //
		SkillInfo_i         	skillData[SKILL_SIZE];         //
	}MGCleanSkillPoint;
	MSG_SIZE_GUARD(MGCleanSkillPoint);

	typedef struct StrMGRepairWearByNPC
	{
		enum eRepairResult
		{
			TARGET_DO_NOT_REPAIR = 0,
			NO_ENOUGH_REPAIR_MONEY = 1,
			OTHER_ERROR = 2,
			OK = 3
		};
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_REPAIR_WEAR_BY_NPC;
		PlayerID lNotiPlayerID;		//发送给哪个玩家所在的gatesrv
		eRepairResult tResult;
		bool repairAll;
		unsigned int repairTarget;
		unsigned int newWear;
		unsigned int repairSilverMoney;
		unsigned int repairGoldMoney;
	}MGRepairWearByNPC;
	MSG_SIZE_GUARD(MGRepairWearByNPC);

	//#define M_G_CHANGE_BATTLE_MODE 0x15b1		//修改战斗模式
	typedef struct StrMGChangeBattleMode
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHANGE_BATTLE_MODE;
		PlayerID lNotiPlayerID;									//发送给哪个玩家所在的gatesrv
		BATTLE_MODE mode;
		bool bSuccess;
	}MGChangeBattleMode;
	MSG_SIZE_GUARD(MGChangeBattleMode);

	//#define M_G_PLAYER_USE_HEAL_ITEM 0x15b2		//mapsrv上玩家使用恢复道具
	typedef struct StrMGPlayerUseHealItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_USE_HEAL_ITEM;
		PlayerID lNotiPlayerID;									//发送给哪个玩家所在的gatesrv
		GCPlayerUseHealItem gcHeal;
	}MGPlayerUseHealItem;
	MSG_SIZE_GUARD(MGPlayerUseHealItem);

	//#define M_G_INFO_UPDATE    0x15b3         //mapsrv上战斗后信息的更新
	typedef struct StrMGInfoUpdate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_INFO_UPDATE;
		PlayerID lNotiPlayerID;									//发送给哪个玩家所在的gatesrv
		unsigned int nAtkPlayerPhyExp;	//这次攻击攻击者的物理熟练度
		unsigned int nAtkPlayerMagExp;	//这次攻击攻击者的魔法熟练度
		unsigned int nAtkPlayerExp;		//这次攻击攻击者的经验值
		unsigned int nAtkPlayerHP;		//这次攻击攻击者的HP
		unsigned int nAtkPlayerMP;		//这次攻击攻击者的MP
		unsigned int nAtkPlayerMoney;	//这次攻击攻击者的金钱
		unsigned int nAtkPlayerSpPower;	//这次攻击攻击者的SP能量
		unsigned int nAtkSkillSpExp;	//这次攻击消耗的该技能的SP技能经验
		unsigned int nAtkSpExp;			//这次攻击玩家的SP熟练度
		TYPE_ID  nAtkSkillID;			//攻击消耗技能的ID,针对nAtkSkillSpExp
	}MGInfoUpdate;
	MSG_SIZE_GUARD(MGInfoUpdate);

	//#define M_G_BUFF_DATA      0x15b4			//mapsrv上的个体BUFF信息
	typedef struct StrMGBuffData
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_BUFF_DATA;
		PlayerID lNotiPlayerID;	
		PlayerInfo_5_Buffers	buffData;                                   	//玩家的BUFF数据
		PlayerID            	targetID;                                 	//玩家的ID
	}MGBuffData;
	MSG_SIZE_GUARD(MGBuffData);
	


    //#define  M_G_ADD_APT_RST          0x5167    //添加活点结果；
	typedef struct StrMGAddActivePtRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ADD_APT_RST;
		PlayerID lNotiPlayerID;
		GCAddActivePtRst::AddAPTRst reqRst;//加活点结果
		UINT                	    reqID; //请求顺序号
		PlayerID                    tgtPlayerID; //活点对应玩家的ID号；
		TYPE_POS            	    posX;  //活点位置信息X
		TYPE_POS            	    posY;  //活点位置信息Y
	}MGAddActivePtRst;
	MSG_SIZE_GUARD(MGAddActivePtRst);

	typedef  struct StrMGEvilTimeRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_EVILTIME_RST;
		PlayerID lNotiPlayerID;
		unsigned int EvilTime;
	}MGEvilTimeRst;
	MSG_SIZE_GUARD(MGEvilTimeRst);

	typedef struct StrMGFriendInfoUpdate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_FRIENDINFO_UPDATE;
		PlayerID lNotiPlayerID;
		unsigned int level ;
	}MGFriendInfoUpdate;
	MSG_SIZE_GUARD(MGFriendInfoUpdate);

	typedef struct StrMGRepairItemCostRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_REPAIRITEM_COST_RST;
		PlayerID lNotiPlayerID; 
		unsigned int costSilverMoney;
		unsigned int costGoldMoney;
	}MGRepairItemCostRst;
	MSG_SIZE_GUARD(MGRepairItemCostRst);

	typedef struct StrMGEquipItemUpdate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_EQUIPITEM_UPDATE;
		PlayerID lNotiPlayerID;
		PlayerID changePlayerID;
		unsigned int equipIndex;
		ItemInfo_i newEquipItem;
	}MGEquipItemUpdate;
	MSG_SIZE_GUARD(MGEquipItemUpdate);

	//跳转地图的实现
	typedef struct StrMGSwichMapReq
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd =M_G_SWICHMAP_REQ;
		PlayerID lNotiPlayerID;
		unsigned int newMapID;
		unsigned int targetX;
		unsigned int targetY;
	}MGSwichMapReq;
	MSG_SIZE_GUARD(MGSwichMapReq);

	//#define M_G_CHANGE_TEAM_STATE 0x15b8  //队伍状态改变的通知
	typedef struct StrMGChangeTeamState
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHANGE_TEAM_STATE;
		PlayerID lNotiPlayerID;
		PlayerID changePlayerID;
		CHAR     teamState;			//改变的队伍状态 
	}MGChangeTeamState;
	MSG_SIZE_GUARD(MGChangeTeamState);

	//#define M_G_BROADCAST_PUNISH_INFO 0x51b9 //更新天谴玩家的位置信息转发置所有mapsrv
	typedef struct StrMGBroadcastPunishInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_BROADCAST_PUNISH_INFO;
		PunishPlayerInfo punishPlayer;
	}MGBroadcastPunishInfo;
	MSG_SIZE_GUARD(MGBroadcastPunishInfo);

	//#define M_G_PUNISHMENT_START     0x51ba  //告诉GateSrv0 天谴开始
	typedef struct StrMGPunishmentStart
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PUNISHMENT_START;
	}MGPunishmentStart;
	MSG_SIZE_GUARD(MGPunishmentStart);

	//#define M_G_PUNISHMENT_END    0x51bb  //告诉GateSrv0 天谴结束
	typedef struct StrMGPunishmentEnd
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PUNISHMENT_END;
	}MGPunishmentEnd;
	MSG_SIZE_GUARD(MGPunishmentEnd);



	//GM工具相关
	//#define M_G_GMCMD_REQUEST 0x51bd  	//由于可能人员不在mapsrv上向其他mapsrv发送GM命令的请求
	typedef struct StrMGGmCmdRequest
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_GMCMD_REQUEST;
		char strChat[256];					//聊天消息内容，包括末尾的\0,具体请求的GM命令
		PlayerID requestPlayerID;			//申请GM命令的玩家
		PlayerID lNotiPlayerID;				
		char targetName[MAX_NAME_SIZE];		//请求的姓名,由需要角色名的GM命令的参数分离出来
	}MGGmCmdRequest;
	MSG_SIZE_GUARD(MGGmCmdRequest);

	//#define M_G_GMCMD_RESULT  0x51be   //通知GM命令成功与否
	typedef struct StrMGGmCmdResult
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_GMCMD_RESULT;
		PlayerID requestPlayerID;			//申请GM命令的玩家
		int resultCode;						//返回的值
	}MGGmCmdResult;
	MSG_SIZE_GUARD(MGGmCmdResult);		

	//#define M_G_TARGET_GET_DAMAGE   0x15b6			//某个目标获得伤害
	typedef struct StrMGTargetGetDamage
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_TARGET_GET_DAMAGE;
		PlayerID lNotiPlayerID;
		PlayerID targetID;
		PlayerID noticeID;
		UINT damage;
		TYPE_ID skillID;
		UINT targetLeftHp;
		EBattleFlag battleFlag;
	}MGTargetGetDamage;
	MSG_SIZE_GUARD(MGTargetGetDamage);

	//以下宠物相关;
	//#define	M_G_PETINFO_INSTABLE			 0x5102     //宠物易变信息----只发送给宠物主人的信息；
	typedef struct StrMGPetinfoInstable
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PETINFO_INSTABLE;
		PlayerID lNotiPlayerID;
		GCPetinfoInstable petInfoInstable;
	}MGPetinfoInstable;
	MSG_SIZE_GUARD(MGPetinfoInstable);

	//#define M_G_PET_ABILITY                0x51c4     //宠物能力(也只发送给宠物主人)
	typedef struct StrMGPetAbility
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PET_ABILITY;
		PlayerID lNotiPlayerID;
		GCPetAbility petAbility;
	}MGPetAbility;
	MSG_SIZE_GUARD(MGPetAbility);

	//#define M_G_PETINFO_STABLE				 0x51c3		//宠物稳定信息----群播给附近玩家的宠物信息( 宠物外形，名字 ）
	typedef struct StrMGPetinfoStable
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PETINFO_STABLE;
		PlayerID lNotiPlayerID;
		GCPetinfoStable petInfoStable;
	}MGPetinfoStable;
	MSG_SIZE_GUARD(MGPetinfoStable);

	//#define M_G_GATE_UPDATE_GOODEVILPOINT		0x51c5		//GateSrv更新善恶值for组队
	typedef struct StrMGGateUpdateGoodevilpoint
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_GATE_UPDATE_GOODEVILPOINT;
		PlayerID lNotiPlayerID;
		short goodevilPoint;
	}MGGateUpdateGoodevilpoint;
	MSG_SIZE_GUARD(MGGateUpdateGoodevilpoint);


	//#define M_G_COLLECT_TASK_RST  0x51e6		//反馈采集的物品
	typedef struct StrMGCollectTaskRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_COLLECT_TASK_RST;
		PlayerID lNotiPlayerID;
		GCCollectTaskRst collectRst;
	}MGCollectTaskRst;
	MSG_SIZE_GUARD(MGCollectTaskRst);

	//#define M_G_UPDATE_CITY_INFO 0x51e7	//更新城市信息
	typedef struct StrMGUpdateCityInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_CITY_INFO;
		CityInfo updateCityInfo;
	}MGUpdateCityInfo;
	MSG_SIZE_GUARD(MGUpdateCityInfo);

	
	////#define M_G_SUMMON_PET				 0x51c2		//召唤宠物，群发
	//typedef struct StrMGSummonPet
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_SUMMON_PET;
	//	PlayerID lNotiPlayerID;				//通知的人
	//	PlayerID ownerID;		
	//	char petName[MAX_NAME_SIZE];
	//	TYPE_ID imageID;					//宠物的外形ID
	//	int result;					//召唤的结果只针对发起者
	//}MGSummonPet;
	//MSG_SIZE_GUARD(MGSummonPet);

	////#define M_G_CLOSE_PET				 0x51c3		//唤回宠物，群发
	//typedef struct StrMGClosePet
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_CLOSE_PET;
	//	PlayerID lNotiPlayerID;				//通知的人
	//	PlayerID ownerID;					//宠物的所有者
	//	char petName[MAX_NAME_SIZE];		//宠物姓名
	//	TYPE_ID imageID;					//宠物的外形ID
	//	int result;					//唤回的结果只针对发起者
	//}MGClosePet;
	//MSG_SIZE_GUARD(MGClosePet);

	////#define M_G_CHANGE_PET_NAME			 0x51c4		//更改宠物姓名，群发
	//typedef struct StrMGChangePetName
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_CHANGE_PET_NAME;
	//	PlayerID lNotiPlayerID;				//通知的人
	//	PlayerID ownerID;					//宠物的所有者
	//	char petName[MAX_NAME_SIZE];		//宠物姓名
	//	int  result;						//唤回的结果只针对发起者
	//}MGChangePetName;
	//MSG_SIZE_GUARD(MGChangePetName);

	////#define M_G_CHANGE_PET_SKILL_STATE	 0x51c5		//更改技能设置状态，单发
	//typedef struct StrMGChangePetSkillState
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_CHANGE_PET_SKILL_STATE;
	//	PlayerID lNotiPlayerID;				//通知的人
	//	PlayerID ownerID;					//宠物的所有者
	//	unsigned long petSkillState;		//宠物的技能设置状态
	//	int  result;						//唤回的结果只针对发起者
	//}MGChangePetSkillState;
	//MSG_SIZE_GUARD(MGChangePetSkillState);

	////#define M_G_CHANGE_PET_IMAGE		 0x51c6		//更改宠物外形，群发(看是否处于召唤状态)
	//typedef struct StrMGChangePetImage
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_CHANGE_PET_IMAGE;
	//	PlayerID lNotiPlayerID;				//通知的人
	//	PlayerID ownerID;					//宠物的所有者
	//	TYPE_ID petImage;					//怪物的外形
	//	char	petName[MAX_NAME_SIZE];		//宠物姓名
	//	int		result;						//唤回的结果只针对发起者
	//}MGChangePetImage;
	//MSG_SIZE_GUARD(MGChangePetImage);


	////#define	M_G_UPDATE_PET_EXP				 0x51c7    //更新宠物经验
	//typedef struct StrMGUpdatePetExp
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_UPDATE_PET_EXP;
	//	PlayerID lNotiPlayerID;				//通知的人
	//	PlayerID ownerID;					//宠物的所有者
	//	unsigned long exp;					//现在的经验值
	//}MGUpdatePetExp;
	//MSG_SIZE_GUARD(MGUpdatePetExp);

	////#define	M_G_PET_INFO					 0x51c8    //群播附近玩家自身的宠物信息( 宠物外形，名字 ）
	//typedef struct StrMGPetInfo
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_PET_INFO;
	//	PlayerID lNotiPlayerID;				//通知的人
	//	CHAR                	petName[MAX_NAME_SIZE];                       	//宠物姓名
	//	UINT                	petImageID;                                 	//宠物现在的外形ID,1~256
	//	PlayerID            	petOwner;                                   	//宠物所有者
	//}MGPetInfo;
	//MSG_SIZE_GUARD(MGPetInfo);

	////#define M_G_SELF_PET_INFO				 0x51c9		//更新自身宠物的详细信息（经验值 饥饿度这种）
	//typedef struct StrMGSelfPetInfo
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_SELF_PET_INFO;
	//	PlayerID lNotiPlayerID;				//通知的人
	//	PlayerID ownerID;					//宠物的所有者
	//	UINT                	petNameLen;                                 	//宠物姓名长度
	//	CHAR                	petName[MAX_NAME_SIZE];                       	//宠物姓名
	//	UINT                	petState;                                   	//宠物状态
	//	UINT                	hungerDegree;                               	//宠物饥饿程度
	//	UINT                	petLevel;                                   	//宠物等级
	//	UINT                	petExp;                                     	//宠物经验值
	//	UINT                	petNextExp;                                 	//宠物下级需要经验值
	//	UINT                	imageBuffSize;                              	//宠物可换类型的SIZE，固定为MAX_PET_IMAGE_LEN
	//	CHAR                	imageBuff[MAX_PET_IMAGE_LEN];                 	//宠物可换类型
	//	UINT                	petImageID;                                 	//宠物现在的外形ID,1~256
	//}MGSelfPetInfo;
	//MSG_SIZE_GUARD(MGSelfPetInfo);
	//

	////#define M_G_UPDATE_PET_STATE			 0x51cc		//更新宠物状态
	//typedef struct StrMGUpdatePetState
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_UPDATE_PET_STATE;
	//	PlayerID lNotiPlayerID;								//通知的人
	//	PlayerID ownerID;									//宠物的所有者
	//	UINT	 petState;                              //更改当前的宠物技能状态	
	//}MGUpdatePetState;
	//MSG_SIZE_GUARD(MGUpdatePetState);

	//#define M_G_QUERY_PUNISH_PLAYER_INFO	 0x51cd    //告之天谴人员位置去客户端
	typedef struct StrMGQueryPunishPlayerInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_QUERY_PUNISH_PLAYER_INFO;
		PlayerID lNotiPlayerID;								//通知的人
		PunishPlayerCliInfo members[3];
		UINT memberSize;
	}MGQueryPunishPlayerInfo;
	MSG_SIZE_GUARD(MGQueryPunishPlayerInfo);

	//#define M_G_QUERY_QUEUE_RESULT        0x51ce		//查询mapsrv的队列结果
	typedef struct StrMGQueryQueueResult
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_QUERY_QUEUE_RESULT;
		PlayerID reqPlayer;						//通知的人
		UINT queueIndex;						//当前排队的人数，０表示可以进入	
	}MGQueryQueueResult;
	MSG_SIZE_GUARD(MGQueryQueueResult);


	//#define M_G_QUEUE_OK        0x51cf		//更新等待队列的序号，每１分钟更新1下
	typedef struct StrMGQueueOK
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_QUEUE_OK;
		PlayerID                    okPlayer;						//通知的人
	}MGQueueOK;
	MSG_SIZE_GUARD(MGQueueOK);
	//////////////////////////////////////////////////////////////////////////


	struct RankChartPlayer
	{
		RankChartPlayer()
		{
			StructMemSet( *this, 0, sizeof(*this) );
			SetInvalid();//初始将自身置为无效；
		}
		inline bool IsValidRankPlayer() { return (0 != playerUID); }
		inline bool SetInvalid() { playerUID = 0; return true; };
		unsigned int playerUID;
		char     playerName[MAX_NAME_SIZE];
		unsigned short level;
		unsigned short CLASS;
		unsigned short race;
		int      rankInfo;
		bool     onLineState;
	};
#define  RANKCHART_SIZE 10

	//GateSrv发往RankSrv的消息
	//////////////////////////////////////////////////////////////////////////
	typedef struct StrGMRankPlayerInfoUpdate
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_RANKCHART_INFO_UPDATE;
		unsigned int rankID;
		RankChartPlayer rankChartSegArr[RANKCHART_SIZE];//本次更新的排行榜的玩家信息
		char rankCount;//该页中有几个排行玩家
		char rankChartPage;//这是排行的第几页
	}GMRankPlayerInfoUpdate;
	MSG_SIZE_GUARD( GMRankPlayerInfoUpdate );

	//typedef struct StrMGUpdateRankChartInfo
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_UPDATE_RANKCHART;
	//	unsigned int rankID;//排行榜的编号
	//	RankChartPlayer rankChartSegArr[RANKCHART_SIZE];//本次更新的排行榜的玩家信息
	//	char rankCount;//排行榜本次传递的个数
	//	bool bleft;//是否还有后续排行信息传来
	//}MGUpdateRankChartInfo;
	//MSG_SIZE_GUARD( MGUpdateRankChartInfo );

	//typedef struct StrMGRankPlayerOnLine
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_RANKPLAYER_ONLINE;
	//	PlayerID lNotiPlayerID;	
	//	unsigned int playerUID;
	//	unsigned short race;
	//	char szPlayerName[MAX_NAME_SIZE];
	//}MGRankPlayerOnLine;
	//MSG_SIZE_GUARD( MGRankPlayerOnLine );

	typedef struct StrMGRankPlayerOffLine
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_RANKPLAYER_OFFLINE;
		PlayerID lNotiPlayerID;	
		unsigned int playerUID;
		unsigned short race;
	}MGRankPlayerOffLine;
	MSG_SIZE_GUARD( MGRankPlayerOffLine );

	//排行榜上的玩家上线
	typedef struct StrGFRankPlayerOnLine
	{
		static const unsigned char byPkgType= G_F;
		static const unsigned short wCmd = G_F_RANKPLAYER_ONLINE;
		PlayerID playerID;
		unsigned int rankPlayerUID;
		unsigned short rankPlayerRace;
		char szPlayerName[MAX_NAME_SIZE];
	}GFRankPlayerOnLine;
	MSG_SIZE_GUARD(GFRankPlayerOnLine);


	//排行榜的玩家下线
	typedef struct StrGFRankPlayerOffLine
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_RANKPLAYER_OFFLINE;
		unsigned int  rankPlayerUID;
		unsigned short rankPlayerRace;
	}GFRankPlayerOffLine;
	MSG_SIZE_GUARD(GFRankPlayerOffLine);

	//依据页数查询排行榜的信息
	typedef struct StrGFQueryRankChartInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_QUERY_RANKCHART_INFO;
		PlayerID queryPlayer;
		ERankType rankType;
	}GFQueryRankChartInfo;
	MSG_SIZE_GUARD( GFQueryRankChartInfo );

	//天谴活动开始
	typedef struct StrGFPunishmentBegin
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_PUNISHMENT_BEGIN;
	}GFPunishmentBegin;
	MSG_SIZE_GUARD( GFPunishmentBegin );

	//typedef struct StrGFUpdateRankChart
	//{
	//	static const unsigned char byPkgType = G_F;
	//	static const unsigned short wCmd = G_F_RANKCHART_UPDATE;
	//	unsigned int    rankChartID;
	//	unsigned int    rankCount;
	//	RankChartPlayer rankChartSegArr[RANKCHART_SIZE];
	//}GFUpdateRankChartInfo;
	//MSG_SIZE_GUARD( GFUpdateRankChartInfo );

	//#define  G_F_OTHER_RANK       0x1d04   //其它服务器生成的排行榜通知functionsrv;
	typedef struct StrGFOtherRank
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_OTHER_RANK;
		GCNewRankInfo rankInfo;//直接使用gate消息定义；
	}GFOtherRank;
	MSG_SIZE_GUARD( GFOtherRank );

	typedef struct StrFGRankChartInfo
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_RANKCHART_INFO;
		unsigned int rankID;//排行榜的编号
		RankChartPlayer rankChartSegArr[RANKCHART_SIZE];//本次更新的排行榜的玩家信息
		unsigned int pageIndex;//索引页
		unsigned int Count;//该页的元素数目
	}FGRankChartInfo;
	MSG_SIZE_GUARD(FGRankChartInfo);

	////返回对应页数的排行榜的信息
	//typedef struct StrFGRankChartInfoByPage
	//{
	//	static const unsigned char byPkgType = F_G;
	//	static const unsigned short wCmd = F_G_RANKCHARTINFO_BYPAGE;
	//	PlayerID notifyPlayerID;//通知的玩家编号
	//	unsigned int rankID;//排行榜的编号
	//	unsigned int pageIndex;//索引页
	//	unsigned int Count;//该页的元素数目
	//	char         pageEnd;//是否为最后一页
	//	RankChartPlayer rankChartSegArr[RANKCHART_SIZE];//本次更新的排行榜的玩家信息
	//}FGRankChartInfoByPage;
	//MSG_SIZE_GUARD( FGRankChartInfoByPage );
	//#define  F_G_NEWRANKINFO          0xd102 //新的排行榜信息通知；
	typedef struct StrFGNewRankInfo
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_NEWRANKINFO;
		PlayerID     notifyPlayerID;//通知的玩家编号
		GCNewRankInfo rankInfo;//排行榜信息；
	}FGNewRankInfo;
	MSG_SIZE_GUARD( FGNewRankInfo );

	typedef struct StrFGRemoveRankPlayerByUID
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_REMOVE_RANKPLAYER;
		unsigned int rankID;
		unsigned int playerUID;
	}FGRemoveRankPlayerByUID;
	MSG_SIZE_GUARD(FGRemoveRankPlayerByUID);

	//通知其他服务器刷新排行榜的信息
	typedef struct StrFGRankChartRefresh
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_RANKCHART_REFRESH;
		unsigned int rankID;
	}FGRankChartRefresh;
	MSG_SIZE_GUARD(FGRankChartRefresh);

	typedef struct StrGMRankChartRefresh
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RANKCHART_REFRESH;
		unsigned int rankChartID;
	}GMRankChartRefresh;
	MSG_SIZE_GUARD(GMRankChartRefresh);

	typedef struct StrGMRemoveChartPlayer
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REMOVE_CHART_RANKPLAYER;
		unsigned rankChartID;
		unsigned playerUID;
	}GMRemoveChartPlayer;
	MSG_SIZE_GUARD(GMRemoveChartPlayer);

	typedef struct StrGMUpdateRankPlayerRankInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd     = G_M_UPDATE_RANKPLAYER_RANKINFO;
		unsigned int rankChartID;
		unsigned int playerUID;
		int          rankInfo;
		unsigned int playerclass;
		unsigned int level;
		unsigned int race;
		char     playerName[MAX_NAME_SIZE];
	}GMUpdateRankPlayerRankInfo;
	MSG_SIZE_GUARD(GMUpdateRankPlayerRankInfo);

	typedef struct StrMGRemoveRankPlayer
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_REMOVE_RANKPLAYER;
		unsigned int rankChartID;
		unsigned int playerUID;
	}MGRemoveRankPlayer;
	MSG_SIZE_GUARD(MGRemoveRankPlayer);

	typedef struct StrMGLoadRankInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_LOAD_RANKINFO;
	}MGLoadRankInfo;
	MSG_SIZE_GUARD(MGLoadRankInfo);

	typedef struct StrMGUpdateRankPlayerLevel
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_RANKPLAYER_LEVEL;
		unsigned int   playerUID;
		unsigned short level;
		unsigned short race;
	}MGUpdateRankPlayerLevel;
	MSG_SIZE_GUARD(MGUpdateRankPlayerLevel);

	typedef struct StrMGUpdateRankPlayerRankInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_RANKPLAYER_RANKINFO;
		unsigned int playerUID;
		unsigned int rankChartID;
		unsigned int playerclass;
		unsigned int level;
		unsigned int race;
		char     playerName[MAX_NAME_SIZE];
		int rankInfo;
	}MGUpdateRankPlayerRankInfo;
	typedef struct StrGFRemoveRankPlayerByChartID
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd =  G_F_REMOVE_RANKPLAYER_BYCHARTID;
		unsigned int rankChartID;
		unsigned int playerUID;
	}GFRemoveRankPlayerByChartID;
	MSG_SIZE_GUARD(GFRemoveRankPlayerByChartID);

	typedef struct StrGFLoadRankChartInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_LOAD_RANKINFO;
		unsigned int mapsrvID;
	}GFLoadRankChartInfo;
	MSG_SIZE_GUARD( GFLoadRankChartInfo );

	typedef struct StrGFUpdateRankPlayerLevel
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_UPDATE_RANKPLAYER_LEVEL;
		unsigned int playerUID;
		unsigned short level;
		unsigned short race;
	}GFUpdateRankPlayerLevel;
	MSG_SIZE_GUARD(GFUpdateRankPlayerLevel);

	typedef struct StrGFOnPlayerDeleteRole
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_PLAYER_DELETE_ROLE;
		unsigned int playerUID;
		unsigned short race;
	}GFOnPlayerDeleteRole;
	MSG_SIZE_GUARD(GFOnPlayerDeleteRole);

	typedef struct StrGFUpdateStorageItemInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_UPDATE_STORAGE_ITEM_INFO;
		unsigned int storageUID;
		unsigned int storageIndex;
		ItemInfo_i   storageItem;
	}GFUpdateStorageItemInfo;
	MSG_SIZE_GUARD( GFUpdateStorageItemInfo );

	typedef struct StrGFUpdateStorageSafeInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_UPDATE_STORAGE_SAFE_INFO;
		unsigned int storageUID;
		unsigned int validRow;
		STORAGE_SECURITY_STRATEGY  strategyMode;
		char newPsd[MAX_STORAGE_PASSWORD_SIZE];
	}GFUpdateStorageSafeInfo;
	MSG_SIZE_GUARD( GFUpdateStorageSafeInfo );


	typedef struct StrGFUpateStorageForbiddenInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_UPDATE_STORAGE_FORBIDDEN_INFO;
		unsigned int storageUID;
		unsigned int forbiddenTime;
		unsigned int inputErrCount;
	}GFUpateStorageForbiddenInfo;
	MSG_SIZE_GUARD( GFUpateStorageForbiddenInfo );

	typedef struct StrGFQueryStorageInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_QUERY_STORAGE_INFO;
		PlayerID queryPlayerID;
		unsigned int storageUID;
	}GFQueryStorageInfo;
	MSG_SIZE_GUARD( GFQueryStorageInfo );

	typedef struct StrGFQueryRankPlayerByName
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_QUERY_RANKINFO_BYNAME;
		PlayerID queryPlayerID;
		unsigned int rankChartID;
		char     queryName[MAX_NAME_SIZE];
	}GFQueryRankPlayerByName;
	MSG_SIZE_GUARD(GFQueryRankPlayerByName);

	typedef struct StrFGQueryRankInfoByNameRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_RANKCHARTINFO_BYNAME;
		PlayerID queryPlayerID;
		bool isSuc;
		unsigned int    rankIndex;
		RankChartPlayer rankPlayer;
	}FGQueryRankInfoByNameRst;
	MSG_SIZE_GUARD(FGQueryRankInfoByNameRst);

	typedef struct StrGMPlayerDeleteRole
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd =  G_M_PLAYER_DELETE_ROLE;
		unsigned short race;
		unsigned int   playerUID;
	}GMPlayerDeleteRole;
	MSG_SIZE_GUARD( GMPlayerDeleteRole );


	typedef struct StrGFQueryRecvMailPlayerInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_RECVMAIL_PLAYRINFO_RST;
		unsigned int mailUID;
		unsigned int recvPlayerUID;
		bool     isExist;
	}GFQueryRecvMailPlayerInfo;
	MSG_SIZE_GUARD( GFQueryRecvMailPlayerInfo );

	typedef struct StrGFSendMailToPlayer
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_SENDMAIL_TO_PLAER;
		PlayerID sendPlayerID;
		unsigned sendPlayerUID;
		char sendPlayerName[MAX_NAME_SIZE];
		char sendMailTitle[MAX_MAIL_TITILE_LEN];
		char sendMailContent[MAX_MAIL_CONTENT_LEN];
		char recvPlayerName[MAX_NAME_SIZE];
	}GFSendMailToPlayer;
	MSG_SIZE_GUARD(GFSendMailToPlayer);

	typedef struct StrGFSendSysMailToPlayer
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_SEND_SYSMAIL_TO_PLAYER;
		char recvPlayerName[MAX_NAME_SIZE];
		char sendMailTitle[MAX_MAIL_TITILE_LEN];
		char sendMailContent[MAX_SYSMAIL_CONTENT_LEN];
		ItemInfo_i attachItem[MAX_MAIL_ATTACHITEM_COUNT];
		unsigned int  silverMoney;
		unsigned int  goldMoney;
	}GFSendSysMailToPlayer;
	MSG_SIZE_GUARD(GFSendSysMailToPlayer);

	typedef struct StrGFDeleteMail
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_DELETE_MAIL;
		unsigned int deleteplayeruid;
		unsigned int mailuid;
		PlayerID     deletePlayerID;
		MAIL_TYPE    mailType;
	}GFDeleteMail;
	MSG_SIZE_GUARD(GFDeleteMail);

	typedef struct StrGFDeleteAllMail
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_DELETE_ALLMAIL;
		unsigned int deletePlayerUID;
	}GFDeleteAllMail;
	MSG_SIZE_GUARD(GFDeleteAllMail);


	typedef struct StrGFQueryMailListByPage
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_QUERY_MAILLIST_BYPAGE;
		PlayerID queryPlayerID;
		unsigned int queryPlayerUID;
		MAIL_TYPE mailType;
		unsigned int pageIndex;
	}GFQueryMailListByPage;
	MSG_SIZE_GUARD(GFQueryMailListByPage);

	typedef struct StrGFQueryMailDetialInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_QUERY_MAIL_DETIALINFO;
		PlayerID queryPlayerID;
		unsigned int queryPlayerUID;
		MAIL_TYPE mailType;
		unsigned int mailUID;
	}GFQueryMailDetialInfo;
	MSG_SIZE_GUARD(GFQueryMailDetialInfo);

	typedef struct StrGFPickItemFromMail
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_PICKITEM_FROM_MAIL;
		PlayerID pickPlayerID;
		unsigned int pickPlayerUID;
		unsigned int mailUID;
		unsigned int pickIndex;
	}GFPickItemFromMail;
	MSG_SIZE_GUARD( GFPickItemFromMail );

	typedef struct StrGFPickMoneyFromMail
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_PICKMONEY_FROM_MAIL;
		PlayerID pickPlayerID;
		unsigned int pickPlayerUID;
		unsigned int mailuid;
	}GFPickMoneyFromMail;
	MSG_SIZE_GUARD(GFPickMoneyFromMail);


	typedef struct StrGFPickMailItemError
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd =  G_F_PICKITEM_FROM_MAIL_ERROR;
		unsigned int pickPlayerUID;
		unsigned int mailuid;
		unsigned int pickIndex;
		ItemInfo_i   itemInfo;
	}GFPickMailItemError;
	MSG_SIZE_GUARD(GFPickMailItemError);

	typedef struct StrGFPickMailMoneyError
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_PICKMONEY_FROM_MAIL_ERROR;
		unsigned  int pickPlayerUID;
		unsigned  int mailuid;
		int  money;
	}GFPickMailMoneyError;
	MSG_SIZE_GUARD(GFPickMailMoneyError);

	typedef struct StrFGQueryRecvMailPlayerInfo
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_QUERYMAIL_RECVPLAYER_INFO;
		unsigned int mailUID;
		char recvPlayerName[MAX_NAME_SIZE];
	}FGQueryRecvMailPlayerInfo;
	MSG_SIZE_GUARD( FGQueryRecvMailPlayerInfo );

	typedef struct StrFGQueryMailListInfoByPageRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_QUERYMAILLIST_BYPAGE_RST;
		PlayerID queryPlayer;
		unsigned int mailOwnerUID;
		MailConciseInfo mailConciseInfo[MAX_MAIL_CONCISE_COUNT];
		char     pageIndex;
		char     pageEnd;
		char     mailArrCount;
		MAIL_TYPE mailType;
	}FGQueryMailListInfoByPageRst;
	MSG_SIZE_GUARD( FGQueryMailListInfoByPageRst );

	typedef struct StrFGQueryMailDetialInfoRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_QUERY_MAIL_DETIALINFO_RST;
		unsigned int mailuid;
		PlayerID queryPlayer;
		char     szSendPlayerName[MAX_NAME_SIZE];
		char     szMailTitle[MAX_MAIL_TITILE_LEN];
		char     szContent[MAX_MAIL_CONTENT_LEN];
	}FGQueryMailDetialInfoRst;
	MSG_SIZE_GUARD( FGQueryMailDetialInfoRst );

	typedef struct StrFGQueryMailDetialAttachInfoRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_QUERY_MAIL_ATTACHINFO_RST;
		unsigned int mailuid;
		PlayerID queryPlayer;
		unsigned int  silverMoney;
		unsigned int  goldMoney;
		ItemInfo_i attachItem[MAX_MAIL_ATTACHITEM_COUNT];
	}FGQueryMailDetialAttachInfoRst;
	MSG_SIZE_GUARD(FGQueryMailDetialAttachInfoRst);

	typedef struct StrFGSendMailFail
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_SENDMIAL_FAIL;
		PlayerID sendPlayerID;
		SENDMAIL_FAIL_TYPE errNo;
	}FGSendMailFail;
	MSG_SIZE_GUARD(FGSendMailFail);

	typedef struct StrFGDeleteMailRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_DELETEMAIL_RESULT;
		PlayerID deletePlayerID;
		unsigned int mailuid;
		bool     delmailRst;
	}FGDeleteMailRst;
	MSG_SIZE_GUARD( FGDeleteMailRst );

	typedef struct StrFGPickMailAttachItem
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd= F_G_PICK_MAIL_ATTACH_ITEM;
		PlayerID     pickPlayerID;
		unsigned int mailuid;
		ItemInfo_i   itemInfo;
		unsigned int pickIndex;
	}FGPickMailAttachItem;
	MSG_SIZE_GUARD(FGPickMailAttachItem);

	typedef struct StrFGPickMailAttachMoney
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_PICK_MAIL_ATTACH_MONEY;
		PlayerID pickPlayerID;
		unsigned int mailuid;
		int      money;
	}FGPickMailAttachMoney;
	MSG_SIZE_GUARD( FGPickMailAttachMoney );

	typedef struct StrFGNoticePlayerHaveUnReadMail
	{
		static const unsigned char  byPkgType = F_G;
		static const unsigned short wCmd = F_G_NOTICE_PLAYER_HAVE_UNREADMAIL;
		char recvPlayerName[MAX_NAME_SIZE];
		unsigned int unreadCount;
	}FGNoticePlayerHaveUnReadMail;
	MSG_SIZE_GUARD( FGNoticePlayerHaveUnReadMail );

	typedef struct StrFGUpdatePlayerUnReadMailCount
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_PLAYER_UNREADMAIL_COUNT_UPDATE;
		PlayerID playerID;
		unsigned int unreadCount;
	}FGUpdatePlayerUnReadMailCount;
	MSG_SIZE_GUARD(FGUpdatePlayerUnReadMailCount);

	//#define  F_G_EXCEPTION_NOTIFY  0xd111			//异常通知
	typedef struct StrFGExceptionNotify
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_EXCEPTION_NOTIFY;
		unsigned int exceptionReason;
	}FGExceptionNotify;
	MSG_SIZE_GUARD(FGExceptionNotify);

	typedef struct StrGMPickMailItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PICKITEM_FROM_MAIL;
		PlayerID     pickPlayerID;
		unsigned int mailuid;
		ItemInfo_i   itemInfo;
		unsigned int itemIndex;
	}GMPickMailItem;
	MSG_SIZE_GUARD(GMPickMailItem);

	typedef struct StrGMPickMailMoney
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PICKMONEY_FROM_MAIL;
		PlayerID pickPlayerID;
		unsigned int mailuid;
		int money;
	}GMPickMailMoney;
	MSG_SIZE_GUARD(GMPickMailMoney);

	//#define G_M_REQ_PLAYER_DETAIL_INFO	0x15a6
	typedef struct StrGMReqPlayerDetailInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REQ_PLAYER_DETAIL_INFO;
		PlayerID reqTarget;
		PlayerID reqPlayer;
	}GMReqPlayerDetailInfo;
	MSG_SIZE_GUARD(GMReqPlayerDetailInfo);

	typedef struct StrMGPickMailItemRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PICKITEM_FROM_MAIL_RST;
		PlayerID lNotiPlayerID;
		unsigned int pickPlayerUID;
		unsigned int mailuid;
		ItemInfo_i itemInfo;
		unsigned int itemIndex;
		bool isSuss;
	}MGPickMailItemRst;
	MSG_SIZE_GUARD(MGPickMailItemRst);

	typedef struct StrMGShowMailBox
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_MAILBOX;
		PlayerID lNotiPlayerID;
	}MGShowMailBox;
	MSG_SIZE_GUARD( MGShowMailBox );

	typedef struct StrMGPickMailMoneyRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PICKMONEY_FROM_MAIL_RST;
		PlayerID lNotiPlayerID;
		unsigned int pickPlayerUID;
		unsigned int mailuid;
		int money;
		bool isSuss;
	}MGPickMailMoneyRst;
	MSG_SIZE_GUARD(MGPickMailMoneyRst);


	//#define G_M_NOTIFY_WAR_TASK_STATE 0x159c		//通知mapsrv找到该taskid的玩家并且获得奖励
	typedef struct StrGMNotifyWarTaskState
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd =  G_M_NOTIFY_WAR_TASK_STATE;
		unsigned int  warTaskId;
		ETaskStatType taskState;
	}GMNotifyWarTaskState;
	MSG_SIZE_GUARD( GMNotifyWarTaskState );

	//#define G_M_ATTACK_WAR_END			0x159f		//通知所有mapsrv该攻城战结束
	typedef struct StrGMAttackWarEnd
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd =  G_M_ATTACK_WAR_END;
		unsigned int  warTaskId;
	}GMAttackWarEnd;
	MSG_SIZE_GUARD( GMAttackWarEnd );

	//使用UseSkill2后废弃，原ID号改由GMMouseTipReq使用，//#define G_M_USE_PARTITION_SKILL 0x159f		//使用动作分割技能
	//typedef struct StrGMUsePartionSkill
	//{
	//	static const unsigned char byPkgType = G_M;
	//	static const unsigned short wCmd =  G_M_USE_PARTITION_SKILL;
	//	unsigned int partionSkillId;
	//	TYPE_POS movePosX;
	//	TYPE_POS movetPosY;
	//	PlayerID targetID;
	//	PlayerID attackPlayerID;	
	//	UINT	 adjustTime;		//调整时间,ms
	//	UINT	 sequenceID;		//网络雪列号
	//}GMUsePartionSkill;
	//MSG_SIZE_GUARD( GMUsePartionSkill );
	//#define G_M_MOUSETIP_REQ 0x15a0		//鼠标停留信息查询(HP|MP)
	typedef struct StrGMMouseTipReq
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd =  G_M_MOUSETIP_REQ;
		PlayerID qryPlayerID;
		PlayerID tgtPlayerID;
	}GMMouseTipReq;
	MSG_SIZE_GUARD( GMMouseTipReq );

	//#define G_M_PET_MOVE			  0x15a2	  //宠物移动协议
	typedef struct StrGMPetMove
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PET_MOVE;
		PlayerID            	petOwner;                                   	//宠物的主人
		FLOAT               	fOrgWorldPosX;                              	//移动出发点X客户端坐标；
		FLOAT               	fOrgWorldPosY;                              	//移动出发点Y客户端坐标；
		FLOAT               	fNewWorldPosX;                              	//移动目标点X客户端坐标；
		FLOAT               	fNewWorldPosY;                              	//移动目标点Y客户端坐标；
		FLOAT               	fPetSpeed;                                  	//宠物的速度
	}GMPetMove;
	MSG_SIZE_GUARD(GMPetMove);


	//#define G_M_QUERY_MAP_QUEUE   0x1592	//GateSrv查询该mapsrv的排队队列
	typedef struct StrGMQueryMapQueue
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_QUERY_MAP_QUEUE;
		PlayerID enterWorldPlayer;			//需要进入世界的玩家
	}GMQueryMapQueue;
	MSG_SIZE_GUARD(GMQueryMapQueue);

	//#define G_M_PLAYER_QUIT_QUEUE  0x1593  //玩家退出或者取消排队
	typedef struct StrGMPlayerQuitQueue
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLAYER_QUIT_QUEUE;
		PlayerID quitQueuePlayer;			//退出排队队列的玩家
	}GMPlayerQuitQueue;
	MSG_SIZE_GUARD(GMPlayerQuitQueue);	

	enum EPunishRaceType
	{
		E_HUMAN_TYPE = 0x0,
		E_AKA_TYPE,
		E_ELF_TYPE,
		E_RACE_MAX,
	};
	typedef struct StrFGPunishmentPlayer
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_PUNISHMENT_PLAYER;
		char punishCount[E_RACE_MAX];//每个种族惩罚的人数
		char playerName[E_RACE_MAX*3][MAX_NAME_SIZE];//每个种族惩罚人的名字
	}FGPunishmentPlayer;
	MSG_SIZE_GUARD( FGPunishmentPlayer );

	typedef struct StrGMRegisterUnionMember
	{
		enum
		{
			CREATE_UNION = 1,	//创建工会
			JOIN_UNION,		//加入工会
			ON_LINE			//工会已有成员上线(存在所属工会)
		};

		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REGISTER_UNION_MEMBER;
		PlayerID playerId;
		unsigned int unionID;
		char unionName[MAX_UNIONNAME_LEN];
		unsigned int ownerUiid; 
		unsigned int unionActive;
		unsigned short unionLevel;
		BYTE registerType;
	}GMRegisterUnionMember;
	MSG_SIZE_GUARD(GMRegisterUnionMember);

	typedef struct StrGMReportUnionSkills
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REPORT_UNION_SKILLS;
		PlayerID playerId;
		unsigned int unionID;
		unsigned int unionSkills[MAX_UNION_SKILL_NUM];
	}GMReportUnionSkills;
	MSG_SIZE_GUARD(GMReportUnionSkills);

	typedef struct StrGMUnregisterUnionMember
	{
		enum
		{
			QUIT_UNION_ACTIVE,		//主动退出工会
			QUIT_UNION_PASSIVE		//被动退出工会
		};

		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_UNREISTER_UNION_MEMBER;
		PlayerID playerId;
		BYTE unregisterType;
	}GMUnregisterUnionMember;
	MSG_SIZE_GUARD(GMUnregisterUnionMember);

	typedef struct StrGMIssueUnionTasks
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_ISSUE_UNION_TASKS;
		PlayerID playerId;
		unsigned int unionTasks[MAX_ISSUE_TASK_NUM];
		unsigned int taskEndTime;
		unsigned int costActive;
	}GMIssueUnionTasks;
	MSG_SIZE_GUARD(GMIssueUnionTasks);

	typedef struct StrGMChangeUnionActive
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGE_UNION_ACTIVE;
		PlayerID playerId;
		unsigned int newValue;
	}GMChangeUnionActive;
	MSG_SIZE_GUARD(GMChangeUnionActive);

	typedef struct StrGMChangeUnionPrestige
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANAGE_UNION_PRESTIGE;
		PlayerID playerId;
		unsigned int newValue;
	}GMChangeUnionPrestige;
	MSG_SIZE_GUARD(GMChangeUnionPrestige);

	typedef struct StrGMChanageUnionBadge
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANAGE_UNION_BADGE;
		PlayerID playerId;
		char newBadge[200]; 
	}GMChanageUnionBadge;
	MSG_SIZE_GUARD(GMChanageUnionBadge);

	typedef struct StrGMUnionMultiNtf
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_UNION_MULTI_NTF;
		PlayerID playerId;
		USHORT  type;
		USHORT  multi;
		UINT    endTime;
	}GMUnionMultiNtf;
	MSG_SIZE_GUARD(GMUnionMultiNtf);



	typedef struct StrMGDisplayUnionIssuedTasks
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_DISPLAY_UNION_ISSUED_TASKS;
		PlayerID lNotiPlayerID; //通知的玩家ID
		unsigned int unionTask[MAX_ISSUE_TASK_NUM];
		unsigned int taskEndTime;
	}MGDisplayUnionIssuedTasks;
	MSG_SIZE_GUARD(MGDisplayUnionIssuedTasks);

	//typedef struct StrMGCanLearnUnionSkills
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_CAN_LEARN_UNION_SKILLS;
	//	PlayerID lNotiPlayerID; //通知的玩家ID
	//}MGCanLearnUnionSkills;
	//MSG_SIZE_GUARD(MGCanLearnUnionSkills);

	typedef struct StrMGChangeUnionActive
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHANGE_UNION_ACTIVE;
		PlayerID lNotiPlayerID; //通知的玩家ID
		bool addFlag;
		unsigned int changeValue;
	}MGChangeUnionActive;
	MSG_SIZE_GUARD(MGChangeUnionActive);

	typedef struct StrMGChangeUnionPrestige
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHANAGE_UNION_PRESTIGE_REQ;
		PlayerID lNotiPlayerID; //通知的玩家ID
		bool addFlag;
		unsigned int changeValue;
	}MGChangeUnionPrestige;
	MSG_SIZE_GUARD(MGChangeUnionPrestige);

	typedef struct StrMGUnionMemberShowInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UNION_MEMBER_SHOW_INFO;
		PlayerID lNotiPlayerID; //通知的玩家ID
		GCUnionMemberShowInfo showInfos;
	}MGUnionMemberShowInfo;
	MSG_SIZE_GUARD(MGUnionMemberShowInfo);

	//#define M_G_ATTACK_CITY_BEGIN	0x51d5			//通知攻城战开始
	typedef struct StrMGAttackCityBegin
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ATTACK_CITY_BEGIN;
		unsigned int actID;			//活动ID
	}MGAttackCityBegin;
	MSG_SIZE_GUARD(MGAttackCityBegin);

	//#define M_G_ATTACK_CITY_END		0x51d6			//通知攻城战结束
	typedef struct StrMGAttackCityEnd
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ATTACK_CITY_END;
		unsigned int warTaskId;
	}MGAttackCityEnd;
	MSG_SIZE_GUARD(MGAttackCityEnd);


	//#define M_G_NOTIFY_WAR_TASK_STATE  0x51d8		//通知GATESRV向MAPSRV组播所有得到攻城任务的状态
	typedef struct StrMGNotifyWarTaskState
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NOTIFY_WAR_TASK_STATE;
		WarTaskInfo warTaskInfo;
	}MGNotifyWarTaskState;
	MSG_SIZE_GUARD(MGNotifyWarTaskState);

	//使用UseSkill2后废弃，原ID号由MOUSE_TIP使用, by dzj, 10.05.31,//#define M_G_USE_PARTION_SKILL_RST		//玩家使用动作分割技能
	//typedef struct StrMGUsePartionSkillRst
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_USE_PARTION_SKILL_RST;
	//	PlayerID				lNotiPlayerID;										//通知对象
	//	EBattleResult       	nErrCode;                                   	//错误代码
	//	EScopeFlag          	scopeFlag;                                  	//范围通知标志
	//	PlayerID            	lPlayerID;                                  	//攻击者ID；
	//	EBattleFlag         	battleFlag;                                 	//战斗显示标志
	//	TYPE_ID             	skillID;                                    	//攻击所使用的方式或技能ID
	//	TYPE_TIME           	lCooldownTime;                              	//技能的冷却时间
	//	TYPE_ID             	consumeItemID;                              	//如果是道具技能会填充,其他技能不填
	//	PlayerID            	targetID;                                   	//目标ID
	//	TYPE_UI32           	lHpSubed;                                   	//怪物减去的HP
	//	TYPE_UI32           	lHpLeft;                                    	//怪物剩余HP
	//	TYPE_UI32           	nAtkPlayerHP;                               	//这次攻击攻击者的HP
	//	TYPE_UI32           	nAtkPlayerMP;                               	//这次攻击攻击者的MP
	//	PlayerID            	targetRewardID;                             	//若target为怪物的话更新怪物的RewardID
	//	PlayerID            	aoeMainTargetID;                            	//目标AOE的主攻击对象
	//	UINT                	StartTime;                                  	//动作分割起始时间
	//	UINT                	MidwaySpeed;                                	//动作分割中途速度
	//	UINT                	EndTime;                                    	//动作分割结束速度
	//	UINT                	MinMidwayTime;                              	//过程中需要时间
	//	TYPE_POS            	FinalPositionPosX;									//动作分割结束位置X
	//	TYPE_POS            	FinalPositionPosY;									//动作分割结束位置Y
	//}MGUsePartionSkillRst;
	//MSG_SIZE_GUARD(MGUsePartionSkillRst);
	//#define M_G_MOUSE_TIP 0x51d9 //客户端的鼠标tip信息查询(HP|MP)
	typedef struct StrMGMouseTip
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_MOUSE_TIP;
		PlayerID				lNotiPlayerID;									//通知对象
		GCMouseTip              gcMouseTip;
	}MGMouseTip;
	MSG_SIZE_GUARD(MGMouseTip);

	////#define M_G_PET_MOVE	0x15da			//宠物移动
	//typedef struct StrMGPetMove
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_PET_MOVE;
	//	PlayerID              lNotiPlayerID;										//通知的玩家
	//	PlayerID            	petOwner;                                   	//宠物的主人
	//	FLOAT               	fOrgWorldPosX;                              	//移动出发点X客户端坐标；
	//	FLOAT               	fOrgWorldPosY;                              	//移动出发点Y客户端坐标；
	//	FLOAT               	fNewWorldPosX;                              	//移动目标点X客户端坐标；
	//	FLOAT               	fNewWorldPosY;                              	//移动目标点Y客户端坐标；
	//	FLOAT               	fPetSpeed;                                  	//宠物的速度
	//}MGPetMove;
	//MSG_SIZE_GUARD(MGPetMove);

	////#define M_G_RACE_GROUP_NOTIFY  0x15db	//种族群发消息
	//typedef struct StrMGRaceGroupNotify
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_RACE_GROUP_NOTIFY;
	//	PlayerID lNotiPlayerID;					//通知的GATESRV
	//	char   chatPlayerName[MAX_NAME_SIZE];	//说话者名字;
	//	char   chatContent[256];              //说话内容;
	//	CHAT_TYPE chatType;						//区分IRC世界聊天还是游戏世界的世界聊天
	//	ERace notifyRace;						//通知的种族
	//}MGRaceGroupNotify;
	//MSG_SIZE_GUARD(MGRaceGroupNotify);

	////#define M_G_WORLD_NOTIFY  //世界群发消息
	//typedef struct StrMGWorldNotify
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_WORLD_NOTIFY;
	//	PlayerID lNotiPlayerID;					//通知的GATESRV
	//	char   chatPlayerName[MAX_NAME_SIZE];	//说话者名字;
	//	char   chatContent[256];              //说话内容;
	//	CHAT_TYPE chatType;						//区分IRC世界聊天还是游戏世界的世界聊天
	//}MGWorldNotify;
	//MSG_SIZE_GUARD(MGWorldNotify);


	//#define M_G_REQ_PLAYER_DETAIL_INFO_RST   0x15e0		//右键查询属性的结果
	typedef struct StrMGReqPlayerDetailInfoRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_REQ_PLAYER_DETAIL_INFO_RST;
		PlayerID lNotiPlayerID;					//通知的GATESRV
		GCReqPlayerDetailInfoRst reqRst;		//结果
	}MGReqPlayerDetailInfoRst;
	//MSG_SIZE_GUARD(MGReqPlayerDetailInfoRst);
	

	//////////////////////////////////////////////////////////////////////////


	typedef struct StrGMCheckPkgFreeSpace
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHECK_PKG_FREE_SPACE;
		PlayerID lNotiPlayerID;
		BYTE PurchaseType;    //购买方式 0 星钻 1 积分
		UINT ItemID;          //道具类型ID
		UINT ItemNum;         //道具数目
		UINT CliCalc;         //客户端计算的金额数目
	}GMCheckPkgFreeSpace;
	MSG_SIZE_GUARD(GMCheckPkgFreeSpace);

	typedef struct StrGMFetchVipItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_FETCH_VIP_ITEM;
		PlayerID lNotiPlayerID;
		unsigned int vipID;
		BYTE arrNum;
		VipItem  itemArr[ITEMS_PER_PAGE];
	}GMFetchVipItem;
	MSG_SIZE_GUARD(GMFetchVipItem);

	typedef struct StrGMFetchNonVipItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_FETCH_NON_VIP_ITEM;
		PlayerID lNotiPlayerID;
		unsigned int tranid;
		unsigned int itemID;
		BYTE  itemNum;
		char destAccount[MAX_NAME_SIZE];
		unsigned int destRoleid;
		unsigned int diamond;
	}GMFetchNonVipItem;
	MSG_SIZE_GUARD(GMFetchNonVipItem);

	typedef struct StrMGCheckPkgFreeSpace
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHECK_PKG_FREE_SPACE;
		PlayerID lNotiPlayerID;
		int result;
		BYTE PurchaseType;    //购买方式 0 星钻 1 积分
		UINT ItemID;          //道具类型ID
		UINT ItemNum;         //道具数目
		UINT CliCalc;         //客户端计算的金额数目
	}MGCheckPkgFreeSpace;
	MSG_SIZE_GUARD(MGCheckPkgFreeSpace);

	typedef struct StrMGFetchVipItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_FETCH_VIP_ITEM;
		PlayerID lNotiPlayerID;
		int result;
		unsigned int vipID;
	}MGFetchVipItem;
	MSG_SIZE_GUARD(MGFetchVipItem);

	typedef struct StrMGFetchNonVipItem
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_FETCH_NON_VIP_ITEM;
		PlayerID lNotiPlayerID;
		int result;
		unsigned int tranid;
		unsigned int itemID;
		BYTE  itemNum;
		char destAccount[MAX_NAME_SIZE];
		unsigned int destRoleid;
		char note[MAX_NOTE_SIZE];
	}MGFetchNonVipItem;
	MSG_SIZE_GUARD(MGFetchNonVipItem);




	//关于保护道具的协议
	//////////////////////////////////////////////////////////////////////////

#define TASK_RD_PAGE_SIZE 100

	typedef struct TaskRdInfo
	{
		unsigned int mTaskRdIArr[TASK_RD_PAGE_SIZE];
		char  mCount;
		bool  isEnd;
	}TaskRdInfo;

	typedef struct ProtectItemInfo
	{
		ProtectItem mProtectItemArr[PKG_PAGE_SIZE];
		char    mCount;
		bool    isEnd;
	}ProtectItemInfo;

	typedef struct StrMGShowProtectPlate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_PROTECTPLATE;
		PlayerID lNotiPlayerID;
		PROTECTITEM_PLATE_TYPE plateType;
	}MGShowProtectPlate;
	MSG_SIZE_GUARD(MGShowProtectPlate);

	typedef struct StrMGPutItemToProtectPlateRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PUTITEM_TO_PROTECTPLATE_RST;
		PlayerID lNotiPlayerID;
		ADD_PROTECTITEM_RST addItemRst;
	}MGPutItemToProtectPlateRst;
	MSG_SIZE_GUARD(MGPutItemToProtectPlateRst);

	typedef struct StrMGSetItemProtectProp
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SET_ITEM_PROTECTPROP;
		PlayerID lNotiPlayerID;
		ProtectItem protectItem;
		UPDATE_PROTECTITEM_PROP opType; 
	}MGSetItemProtectProp;
	MSG_SIZE_GUARD(MGSetItemProtectProp);

	typedef struct StrMGReleaseSpecialPropRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_RELEASE_SPECIALPROTECT_RST;
		PlayerID lNotiPlayerID;
		unsigned int itemUID;
		unsigned int opRst;
	}MGReleaseSpecialPropRst;
	MSG_SIZE_GUARD(MGReleaseSpecialPropRst);

	typedef struct StrMGCancelSpecialPropRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CANCEL_SPECIALPROTECT_RST;
		PlayerID lNotiPlayerID;
		unsigned int itemUID;
		unsigned int opRst;
	}MGCancelSpecialPropRst;
	MSG_SIZE_GUARD(MGCancelSpecialPropRst);

	typedef struct StrMGSendProtectItemWithProp
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SENDITEM_WITH_PROTECTPROP;
		PlayerID lNotiPlayerID;
		ProtectItem protectItemArr[PKG_PAGE_SIZE];
		unsigned int itemCount;
	}MGSendProtectItemWithProp;
	MSG_SIZE_GUARD(MGSendProtectItemWithProp);

	typedef struct StrMGConfirmProtectPlateRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CONFIRM_PROTECTPLATE_RST;
		PlayerID lNotiPlayerID;
		bool     confirmType;
		char     confirmRst;
	}MGConfirmProtectPlateRst;
	MSG_SIZE_GUARD(MGConfirmProtectPlateRst);

	typedef struct StrMGTakeItemFromProtectPlateRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_TAKE_ITEM_FROM_PROTECTPLATE_RST;
		PlayerID lNotiPlayerID;
		unsigned int itemUID;
		char takeRst;
	}MGTakeItemFromProtectPlateRst;
	MSG_SIZE_GUARD(MGTakeItemFromProtectPlateRst);

	typedef struct StrGMPutItemToProtectItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd =  G_M_PUTITEM_TO_PROTECTPLATE;
		PlayerID lNotiPlayerID;
		unsigned int itemUID;
	}GMPutItemToProtectItem;
	MSG_SIZE_GUARD(GMPutItemToProtectItem);

	typedef struct StrGMConfirmProtectPlate
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CONFIRM_PROTECTPLATE;
		PlayerID lNotiPlayerID;
		bool isConfirm;
	}GMConfirmProtectPlate;
	MSG_SIZE_GUARD(GMConfirmProtectPlate);

	typedef struct StrGMRleaseItemSpecialProp
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RELEASE_SPECIALPROTECT;
		PlayerID lNotiPlayerID;
		unsigned int itemUID;
	}GMRleaseItemSpecialProp;
	MSG_SIZE_GUARD(GMRleaseItemSpecialProp);

	typedef struct StrGMCancelItemSpecialProp
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_CANCEL_SPECIALPROTECT;
		PlayerID lNotiPlayerID;
		unsigned int itemUID;
	}GMCancelItemSpecialProp;
	MSG_SIZE_GUARD(GMCancelItemSpecialProp);

	typedef struct StrGMTakeItemFromProtectPlate
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_TAKE_ITEM_FROM_PROTECTPLATE;
		PlayerID lNotiPlayerID;
		unsigned int itemUID;
	}GMTakeItemFromProtectPlate;
	MSG_SIZE_GUARD( GMTakeItemFromProtectPlate );

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//非存ＤＢ，但跳地图保存信息类型；
    enum E_NDSMR_INFO
	{
		NI_FOLLOW_MONSTER = 0,	//跟随怪物的类型信息；
		NI_BATTLE_MODE = 1,		//战斗模式
		NI_ROGUE_TIME = 2,		//流氓状态的时间
		NI_WAR_TASK = 3,		//攻城任务
		NI_GAME_STATE = 4,		//防沉迷状态	
		NI_PET_INFO = 5,       //宠物信息；
		NI_KILL_COUNTER = 6,		//天谴任务计数器
		NI_PET_TIMER = 7,		//宠物计时器
		NI_ANAMORPHIC_STATE = 8, //玩家变形状态
		NI_COPY_INFO = 11,   //玩家副本相关信息
		NI_STORAGE_INFO = 12, //玩家仓库的相关信息
		NI_ROOKIE_BREAK_TIME = 13,  //新手破保时间
		NI_BUFF_DATA = 14,		//跨地图时的BUFF传送
		NI_KILL_INFO = 15
	};

	//存盘信息类型；
	enum EDB_INFO_T
	{
		DI_PET = 0, //宠物信息；
		DI_SUIT = 1,//套装信息;
		DI_FASHION = 2, //时装服饰信息
		DI_PROTECTITEM = 3, //保护道具
		DI_TASKRD = 4, //任务存盘
		DI_SKILL  = 5, //技能存盘
		DI_COPYSTAGE = 6, //副本阶段信息；
	};

	struct PetTimer
	{
		unsigned int summonTimer;		//召唤时间的timer	
		unsigned int hungerTimer;		//用于计算召唤饥饿度的timer
		unsigned int aiPickTimer;		//自动拾取的timer
		unsigned int aiAttackTimer;		//自动攻击的timer
		unsigned int aiDrugsTimer;		//自动喝药的timer
		unsigned int aiRepairEquipTimer;	//自动修理的timer
		unsigned int aiAttackUpgradeTimer;	//自动攻击升级版的timer
	};

	///通信(G<-->M)用副本信息
	struct CommCopyInfo
	{
		bool           m_isNextCopy;//是否欲进副本；
		bool           m_isPCopy;//保存的为伪/真副本信息；
		unsigned short m_MapSrvID;//该玩家副本所在的mapsrv id号；
		unsigned short m_outMapID;//离开副本后要去的位置,地图号;
		unsigned short m_outMapPosX;//离开副本后要去的位置,坐标X;
		unsigned short m_outMapPosY;//离开副本后要去的位置,坐标Y;

		unsigned short m_curCopyMapID;//所在副本地图ID号；
		unsigned int   m_curCopyID;//所在副本ID号；

		bool           m_isLeavedCopyTeam;//是否已离开副本所在组队，在跳地图过程中离开组队有可能发生此情况；

		unsigned int   m_toUseScrollType;//本次进入欲使用的卷轴类型(注意不一定是真正的卷轴类型，因为如果之前进过同一副本实例的话，会以之前的扣除卷轴为准)；
	};

	//副本阶段信息；
	struct CopyStageInfo
	{
	//public:
	//	CopyStageInfo() : m_stageInfo(0), m_timeInfo(0) {};

		/////使用存DB数据初始化自身；
		//bool InitFromDBInfo( unsigned int dbInfo[2] )
		//{
		//	m_stageInfo = dbInfo[0];
		//	m_timeInfo = dbInfo[1];
		//}

	public:
		///设置副本起始时间(用于超时清阶段信息)；
		inline void SetTimeInfo( unsigned int timeInfo ) { m_timeInfo = timeInfo; }

		///设置阶段信息；
		inline void SetStageInfo( unsigned short mapid/*副本地图ID号16bit*/, unsigned short stagenum/*副本阶段号4bit*/, unsigned short copytype/*副本类型4bit*/ )
		{
			m_stageInfo = ( (mapid<<16) | (stagenum<<4) | copytype );
		};

		///本信息是否为有效的副本阶段信息；
		inline bool IsValidStageInfo() { return ( 0 != m_stageInfo ); }

		///是否为指定副本地图的阶段信息,如果是，则输出相关信息；
		inline bool IfHasStageInfo( unsigned short mapid, unsigned short& stagenum, unsigned short& copytype, unsigned int& timeinfo )
		{
			if ( !IsValidStageInfo() )
			{
				return false;
			}

			if ( GetMapID() != mapid )
			{
				return false;
			}

			stagenum = GetStageNum();
			copytype = GetCopyType();
			timeinfo = GetTimeInfo();

			return true;
		}

	public:
		inline unsigned short GetMapID() { return m_stageInfo>>16; }//取对应副本地图ID号；
		inline unsigned short GetStageNum() { return ((m_stageInfo<<16)>>20); }//取阶段信息；
		inline unsigned short GetCopyType()  { return ((m_stageInfo<<20)>>20); }//取副本类型(金银铜券)；
		inline unsigned int   GetTimeInfo() { return m_timeInfo; }

	private:
		unsigned int m_stageInfo;//副本阶段信息
		unsigned int m_timeInfo;//副本时间信息
	};

	///玩家身上保存的副本阶段信息；
	struct PlayerStageInfos
	{
	//public:
	//	PlayerStageInfos() { ResetCopyStageInfos(); }

	public:
		inline void ResetCopyStageInfos() { StructMemSet( m_rsvCopyStageInfos, 0, sizeof(m_rsvCopyStageInfos) ); };

		///检查玩家身上是否有指定副本地图的阶段信息；
		inline bool GetIfHasStageInfo( unsigned short mapid, unsigned short& stagenum, unsigned short& copytype, unsigned int& timeinfo )
		{
			for ( int i=0; i<ARRAY_SIZE(m_rsvCopyStageInfos); ++i )
			{
				if ( m_rsvCopyStageInfos[i].IfHasStageInfo( mapid, stagenum, copytype, timeinfo ) )
				{
					return true;
				}
			}
			return false;
		}

		inline bool SetStageInfo( unsigned short mapid, unsigned short stagenum, unsigned short copytype, unsigned int timeinfo )
		{	
			unsigned short orgstagenum=0, orgcopytype=0;
			unsigned int orgtimeinfo=0;
			//首先查找过去是否存过该副本地图的阶段信息，若有，则覆盖；
			for ( int i=0; i<ARRAY_SIZE(m_rsvCopyStageInfos); ++i )
			{
				if ( m_rsvCopyStageInfos[i].IfHasStageInfo( mapid, orgstagenum, orgcopytype, orgtimeinfo ) )
				{
					m_rsvCopyStageInfos[i].SetStageInfo( mapid, stagenum, copytype );
					if ( 0 != timeinfo )//本次欲设时间信息；
					{
						m_rsvCopyStageInfos[i].SetTimeInfo( timeinfo );
					}
					return true;
				}
			}

			//过去未存此地图的副本阶段，新增之；
			for ( int i=0; i<ARRAY_SIZE(m_rsvCopyStageInfos); ++i )
			{
				if ( !(m_rsvCopyStageInfos[i].IsValidStageInfo()) )
				{
					//找一个空位存放新的阶段信息；
					m_rsvCopyStageInfos[i].SetStageInfo( mapid, stagenum, copytype );
					if ( 0 != timeinfo )//本次欲设时间信息；
					{
						m_rsvCopyStageInfos[i].SetTimeInfo( timeinfo );
					}
					return true;
				}
			}

			return false;
		}

        inline bool SetStageInfoStageNum( unsigned short mapid, unsigned short stagenum )
		{
			unsigned short orgstagenum=0, orgcopytype=0;
			unsigned int orgtimeinfo=0;
			//首先查找过去是否存过该副本地图的阶段信息，若有，则覆盖；
			for ( int i=0; i<ARRAY_SIZE(m_rsvCopyStageInfos); ++i )
			{
				if ( m_rsvCopyStageInfos[i].IfHasStageInfo( mapid, orgstagenum, orgcopytype, orgtimeinfo ) )
				{
					m_rsvCopyStageInfos[i].SetStageInfo( mapid, stagenum, orgcopytype );
					return true;
				}
			}

			D_ERROR( "SetStageInfoStageNum，玩家没有存储当前所在的地图%d的副本信息\n", mapid );
			return false;
		}

	private:
		CopyStageInfo m_rsvCopyStageInfos[10];//玩家身上保存的副本阶段信息；
	};

	//#define M_G_TEMP_BUFF_DATA							0x5272		//跳转地图用的BUFF信息
	typedef struct StrMGTempBuffData
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_TEMP_BUFF_DATA;
		PlayerID lNotiPlayerID;
		BuffDataInfo buffDataArr[32];
		unsigned int buffArrSize;
	}MGTempBuffData;
	MSG_SIZE_GUARD(MGTempBuffData);


	struct DbPetInfo
	{
		MGPetinfoInstable petInfoInstable;//宠物易变信息(只宠物主人知晓)
		MGPetAbility      petInfoAbility;//宠物能力(只宠物主人知晓)
		MGPetinfoStable   petInfoStable;//宠物稳定信息(宠物主人及周边人知晓)
	};

	//#define G_M_DBGET_PRE                    0x15ca     //DB信息开始;
	typedef struct StrGMDbGetPre
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_DBGET_PRE;
		PlayerID     playerID;//存盘信息对应玩家的ID号
		bool         isNeedQueueCheck;//是否需要排队检查，第一次进时必需设，收到map发来排到队首因此再次尝试进入时，不必再设；
		char         roleName[MAX_NAME_SIZE];//由于gate收到存盘信息时，可能玩家已下线，对应的playerID玩家可能已无效或变成了其它人，因此需要；
		char         accountName[MAX_NAME_SIZE];
	}GMDbGetPre;
	MSG_SIZE_GUARD(GMDbGetPre);

	//#define G_M_DBGET_POST                   0x15cb     //DB信息结束(与G_M_DBGET_PRE一一对应);
	typedef struct StrGMDbGetPost
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_DBGET_POST;
		PlayerID     playerID;
	}GMDbGetPost;
	MSG_SIZE_GUARD(GMDbGetPost);

	//#define G_M_PLAYER_ENTER_MAP             0x15cc     //玩家离开地图(在G_M_DBGET_POST之后发送);
	typedef struct StrGMPlayerEnterMap
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLAYER_ENTER_MAP;
		PlayerID     playerID;//存盘信息对应玩家的ID号
		char         roleName[MAX_NAME_SIZE];//由于gate收到存盘信息时，可能玩家已下线，对应的playerID玩家可能已无效或变成了其它人，因此需要；
	}GMPlayerEnterMap;
	MSG_SIZE_GUARD(GMPlayerEnterMap);

    //#define G_M_DBGET_INFO                   0x15cd     //DB取到的信息；
	typedef struct StrGMDbGetInfo
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_DBGET_INFO;
		PlayerID     playerID;//信息对应的玩家，单独发送本消息时用到，与DbGetPre和DbGetPost合用时不需要;
		EDB_INFO_T   infoType;
		union
		{
			DbPetInfo petInfo;
			PlayerInfo_Suits suitInfo;
			PlayerInfo_Fashions fashionInfo;
			ProtectItemInfo  protectItemInfo; 
			TaskRdInfo       taskRdInfo;
			PlayerStageInfos copyStageInfos;
		};
	}GMDbGetInfo;
	MSG_SIZE_GUARD(GMDbGetInfo);

	//#define G_M_NDSMR_INFO                   0x15ce     //no db, switch map reserve信息；
	typedef struct StrGMNDSMRInfo
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_NDSMR_INFO;
		PlayerID       playerID;//信息对应的玩家
		E_NDSMR_INFO   infoType;
		union
		{
			unsigned int followMonsterType;//跟随怪物
			BATTLE_MODE  battleMode;
			unsigned int rogueTime;
			unsigned int warTaskId;
			UINT         gameState;			//防沉迷状态
			DbPetInfo    petInfo;           //宠物信息
			PunishTaskInfo punishTaskInfo;	//天谴任务信息
			PetTimer petTimer;
			unsigned int anamorphicState;   //变形状态
			CommCopyInfo    copyInfo;       //玩家副本信息
			bool         storageLock;       //玩家的仓库是否被锁定
			unsigned int  rookieBreakTime;
			MGTempBuffData tpBuffData;			//buff信息
			KillInfo	killInfo;				//杀人信息
		};
	}GMNDSMRInfo;
	MSG_SIZE_GUARD(GMNDSMRInfo);

	//#define M_G_DBSAVE_PRE                   0x51c5     //DB保存开始;
	typedef struct StrMGDbSavePre
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_DBSAVE_PRE;
		PlayerID     playerID;//存盘信息对应玩家的ID号
		char         roleName[MAX_NAME_SIZE];//由于gate收到存盘信息时，可能玩家已下线，对应的playerID玩家可能已无效或变成了其它人，因此需要；
		char         accountName[MAX_NAME_SIZE];//由于gate收到存盘信息时，可能玩家已下线，对应的playerID玩家可能已无效或变成了其它人，因此需要；
		unsigned     playerUID;
		//unsigned int mapsrvtag;//mapsrv端的标记，与前两个字段一一对应，方便gate区分消息，现在去除，因为mapsrv-->gatesrv的存盘消息总归是发完一人再发另一人的
	}MGDbSavePre;
	MSG_SIZE_GUARD(MGDbSavePre);

	//#define M_G_DBSAVE_POST                  0x51c6     //DB保存结束(与M_G_DBSAVE_POST一一对应);
	typedef struct StrMGDbSavePost
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_DBSAVE_POST;
	}MGDbSavePost;
	MSG_SIZE_GUARD(MGDbSavePost);

    //#define M_G_DBSAVE_INFO                  0x51c9     //DB保存信息；
	typedef struct StrMGDbSaveInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_DBSAVE_INFO;
		PlayerID     playerID;//信息对应的玩家，单独发送本消息时用到，与DbSavePre和DbSavePost合用时不需要;
		EDB_INFO_T   infoType;
		union
		{
			DbPetInfo petInfo;
			PlayerInfo_Suits suitInfo;
			PlayerInfo_Fashions fashionInfo;
			ProtectItemInfo     protectitemInfo;
			TaskRdInfo          taskRdInfo;
			PlayerInfo_4_Skills skillInfo;
			PlayerStageInfos copyStageInfos;
		};
	}MGDbSaveInfo;
	MSG_SIZE_GUARD(MGDbSaveInfo);

	//#define M_G_NDSMR_INFO                   0x51c9     //no db, switch map reserve信息；
	typedef struct StrMGNDSMRInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_NDSMR_INFO;
		PlayerID       playerID;//信息对应的玩家
		E_NDSMR_INFO   infoType;
		union
		{
			unsigned int    followMonsterType;//跟随怪物；
			BATTLE_MODE	    battleMode;
			unsigned int    rogueTime;
			unsigned int    warTaskId;
			unsigned int    gameState;
			MGDbSaveInfo    petInfo;		    //宠物信息；
			PunishTaskInfo	punishTaskInfo;		//天谴任务计数和任务状态
			PetTimer		petTimer;
			unsigned int    anamorphicState;    //变形状态
			bool            storageLock;        //玩家仓库是否被锁定
			unsigned int	rookieBreakTime;		//如果有新手破坏状态持续了多少时间
			MGTempBuffData  tpBuffData;			//跳转地图的BUFF信息
			KillInfo killInfo;				//杀人信息
		};
	}MGNDSMRInfo;
	MSG_SIZE_GUARD(MGNDSMRInfo);

	//#define M_G_PLAYER_LEAVE_MAP             0x51c7     //玩家离开地图(在M_G_DBSAVE_POST之后发送);
	typedef struct StrMGPlayerLeaveMap
	{
		enum ELEAVE_REASON
		{
			LR_OFFLINE = 0, //玩家下线
			LR_SWITCHMAP,   //玩家跳地图
			LR_REBIRTH      //玩家复活
		};
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_LEAVE_MAP;
		PlayerID     playerID;//存盘信息对应玩家的ID号
		char         roleName[MAX_NAME_SIZE];//由于gate收到存盘信息时，可能玩家已下线，对应的playerID玩家可能已无效或变成了其它人，因此需要；
		ELEAVE_REASON leaveReason;//离开原因；
	}MGPlayerLeaveMap;
	MSG_SIZE_GUARD(MGPlayerLeaveMap);

	//#define G_D_DBSAVE_PRE                   0x17c5     //DB保存开始;
	typedef struct StrGDDbSavePre
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_DBSAVE_PRE;
		PlayerID     playerID;//存盘信息对应玩家的ID号
		char         roleName[MAX_NAME_SIZE];
	}GDDbSavePre;
	MSG_SIZE_GUARD(GDDbSavePre);

	//#define G_D_DBSAVE_POST                  0x17c6     //DB保存结束(与G_D_DBSAVE_POST一一对应);
	typedef struct StrGDDbSavePost
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_DBSAVE_POST;
	}GDDbSavePost;
	MSG_SIZE_GUARD(GDDbSavePost);

	//#define G_D_DBSAVE_INFO                  0x17c8     //DB保存信息;
	typedef struct StrGDDbSaveInfo
	{
		static const unsigned char  byPkgType = G_D;
		static const unsigned short wCmd = G_D_DBSAVE_INFO;
		unsigned     playerUID;
		PlayerID     playerID;//信息对应的玩家，单独发送本消息时用到，与DbSavePre和DbSavePost合用时不需要;
		char         roleName[MAX_NAME_SIZE]; 
		EDB_INFO_T   infoType;
		union
		{
			DbPetInfo petInfo;
			PlayerInfo_Suits suitInfo;
			PlayerInfo_Fashions fashionInfo;
			ProtectItemInfo     protectInfo;
			TaskRdInfo          taskRdInfo;
			PlayerInfo_4_Skills skillInfo;
			PlayerStageInfos copyStageInfos;
		};
	}GDDbSaveInfo;
	MSG_SIZE_GUARD(GDDbSaveInfo);

	//#define D_G_DBGET_PRE                    0x71c5     //DB取信息开始;
	typedef struct StrDGDbGetPre
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_DBGET_PRE;
		PlayerID     playerID;//存盘信息对应玩家的ID号
	}DGDbGetPre;
	MSG_SIZE_GUARD(DGDbGetPre);

	//#define D_G_DBGET_POST                   0x71c6     //DB取信息结束(与D_G_DBGET_PRE一一对应);
	typedef struct StrDGDbGetPost
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_DBGET_POST;
		PlayerID     playerID;//存盘信息对应玩家的ID号
	}DGDbGetPost;
	MSG_SIZE_GUARD(DGDbGetPost);

	//#define D_G_DBGET_INFO                   0x71c8     //DB保存信息;
	typedef struct StrDGDbGetInfo
	{
		static const unsigned char  byPkgType = D_G;
		static const unsigned short wCmd = D_G_DBGET_INFO;
		PlayerID     playerID;//信息对应的玩家，单独发送本消息时用到，与DbSavePre和DbSavePost合用时不需要;
		EDB_INFO_T   infoType;
		union
		{
			DbPetInfo petInfo;
			PlayerInfo_Suits     suitInfo;
			PlayerInfo_Fashions  fashionInfo;
			ProtectItemInfo      protectInfo;
			TaskRdInfo           taskRdInfo;
			PlayerStageInfos     copyStageInfos;
		};
	}DGDbGetInfo;
	MSG_SIZE_GUARD(DGDbGetInfo);

	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	typedef struct StrGMQuerySuitInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_QUERY_SUIT_INFO;
		PlayerID lNotiPlayerID;
		BYTE index;
	}GMQuerySuitInfo;
	MSG_SIZE_GUARD(GMQuerySuitInfo);

	typedef struct StrGMClearSuitInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CLEAR_SUIT_INFO;
		PlayerID lNotiPlayerID;
		BYTE index;
	}GMClearSuitInfo;
	MSG_SIZE_GUARD(GMClearSuitInfo);

	typedef struct StrGMSaveSuitInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SAVE_SUIT_INFO;
		PlayerID lNotiPlayerID;
		BYTE index;
		SuitInfo_i suit;
	}GMSaveSuitInfo;
	MSG_SIZE_GUARD(GMSaveSuitInfo);

	typedef struct StrGMChangeSuit
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGE_SUIT;
		PlayerID lNotiPlayerID;
		BYTE index;
		bool repairConfirm;//是否修复
	}GMChangeSuit;
	MSG_SIZE_GUARD(GMChangeSuit);

	typedef struct StrGMReportUnionLevel
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REPORT_UNION_LEVEL;
		PlayerID lNotiPlayerID;
		unsigned short unionLevel;
	}GMReportUnionLevel;
	MSG_SIZE_GUARD(GMReportUnionLevel);

	//#define G_M_CITY_INFO			0x15ac	//告知mapsrv当前管理的地图关卡信息
	typedef struct StrGMCityInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CITY_INFO;
		CityInfo cityInfoArr[20];
		int cityNum;
	}GMCityInfo;
	MSG_SIZE_GUARD(GMCityInfo);

	//#define G_M_DISCARD_COLLECT_ITEM 0x15ad		//放弃采集道具
	typedef struct StrGMDiscardCollectItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_DISCARD_COLLECT_ITEM;
		PlayerID lNotiPlayerID;
	}GMDiscardCollectItem;
	MSG_SIZE_GUARD(GMDiscardCollectItem);

	//#define G_M_PICK_COLLECT_ITEM  0x15ae		//拾取采集道具
	typedef struct StrGMPickCollectItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PICK_COLLECT_ITEM;
		PlayerID lNotiPlayerID;
		UINT	 index;				//拾取信息的索引
	}GMPickCollectItem;
	MSG_SIZE_GUARD(GMPickCollectItem);

	//#define G_M_PICK_ALL_COLLECT_ITEM	0x15af	//拾取所有采集道具
	typedef struct StrGMPickAllCollectItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PICK_ALL_COLLECT_ITEM;
		PlayerID lNotiPlayerID;
	}GMPickAllCollectItem;
	MSG_SIZE_GUARD(GMPickAllCollectItem);

	typedef struct strMGQuerySuitInfoRst
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd =  M_G_QUERY_SUIT_INFO_RST;
		PlayerID   lNotiPlayerID;//通知哪个玩家的相关信息
		BYTE index;
		USHORT ret;
		SuitInfo_i suit;
	}MGQuerySuitInfoRst;
	MSG_SIZE_GUARD( MGQuerySuitInfoRst );

	typedef struct strMGClearSuitInfoRst
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd =  M_G_CLEAR_SUIT_INFO_RST;
		PlayerID   lNotiPlayerID;//通知哪个玩家的相关信息
		BYTE index;
		USHORT ret;
	}MGClearSuitInfoRst;
	MSG_SIZE_GUARD( MGClearSuitInfoRst );

	typedef struct strMGSaveSuitInfoRst
	{
		static const unsigned char byPkgType = D_G;
		static const unsigned short wCmd =  M_G_SAVE_SUIT_INFO_RST;
		PlayerID   lNotiPlayerID;//通知哪个玩家的相关信息
		BYTE index;
		USHORT ret;
	}MGSaveSuitInfoRst;
	MSG_SIZE_GUARD( MGSaveSuitInfoRst );

	typedef struct StrMGChangeSuitRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHANGE_SUIT_RST;
		PlayerID lNotiPlayerID;
		BYTE index;
		bool repairConfirm;//是否修复
		USHORT result;
	}MGChangeSuitRst;
	MSG_SIZE_GUARD(MGChangeSuitRst);


	typedef struct StrGMEquipFashionSwitch
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_EQUIP_FASHION_SWITCH;
		PlayerID lNotiPlayerID;
	}GMEquipFashionSwitch;
	MSG_SIZE_GUARD(GMEquipFashionSwitch);

	//#define G_M_GAME_STATE_NOTIFY	0x15b6
	typedef struct StrGMGameStateNotify
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_GAME_STATE_NOTIFY;
		PlayerID lNotiPlayerID;		//未成年的玩家
		UINT state;					//防沉迷的状态
	}GMGameStateNotify;
	MSG_SIZE_GUARD(GMGameStateNotify);

	typedef struct StrGMEndAnamorphic
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_END_ANAMORPHIC;
		PlayerID lNotiPlayerID;	
	}GMEndAnamorphic;
	MSG_SIZE_GUARD(GMEndAnamorphic);

	//#define G_M_ATTACK_WAR_BEGIN	0x15b9		//通知所有mapsrv该地图该任务ID的攻城战开始
	typedef struct StrGMAttackWarBegin
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_ATTACK_WAR_BEGIN;
		unsigned int actID;
	}GMAttackWarBegin;
	MSG_SIZE_GUARD(GMAttackWarBegin);

	//#define G_M_NOTIFY_RELATION_NUMBER 0x15ba	//通知mapsrv玩家的关系人数
	typedef struct StrGMNotifyRelationNumber
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_NOTIFY_RELATION_NUMBER;
		enum RelationType
		{
			Relation_Friend = 0,	//好友
			Relation_Enemy = 1		//仇人
		};

		enum RelationMode
		{
			Number_Set = 0,	//数量设置
			Number_Add = 1,	//数量增加
			Number_Sub = 2	//数量减少
		};

		RelationMode mode;		//是设置 增加 减少
		RelationType type;		//关系类型
		unsigned int relationNum;	//关系类型的数量
		char     friendsex;//玩家性别
		PlayerID lNotiPlayerID;		
	}GMNotifyRelationNumber;
	MSG_SIZE_GUARD(GMNotifyRelationNumber);

	//#define G_M_USE_SKILL	0x15bb				//玩家使用技能 **新技能体系
	typedef struct StrGMUseSkill
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_USE_SKILL;
		PlayerID attackID;
		CGUseSkill	useSkill;
	}GMUseSkill;
	MSG_SIZE_GUARD(GMUseSkill);

    //#define G_M_USE_SKILL_2 0x1503              //06.30矩形攻击，相应调整所有范围攻击协议
	typedef struct StrGMUseSkill2
	{
		static const unsigned char byPkgType  = G_M;
		static const unsigned short wCmd = G_M_USE_SKILL_2;
		PlayerID attackID;
		CGUseSkill2	useSkill;
	}GMUseSkill2;
	MSG_SIZE_GUARD(GMUseSkill2);

	typedef struct StrMGUpdatePlayerItemInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_PLAYERITEM_INFO;
		PlayerID lNotiPlayerID;
		unsigned int playerUID;
		bool  isEquip;
		unsigned short itemIndex;
		ItemInfo_i   itemInfo;
	}MGUpdatePlayerItemInfo;
	MSG_SIZE_GUARD(MGUpdatePlayerItemInfo);

	typedef struct StrMGUpdateTaskInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_TASK_INFO;
		PlayerID lNotiPlayerID;
		unsigned int playerUID;
		bool isBackTask;
		unsigned short taskIndex;
		TaskRecordInfo taskInfo;
	}MGUpdateTaskInfo;
	MSG_SIZE_GUARD( MGUpdateTaskInfo );

	typedef struct StrMGNotifyNewTaskRecord
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd     = M_G_NOTIFY_NEW_TASK_RECORD;
		PlayerID lNotiPlayerID;
		GCNotifyNewTaskRecord newTaskRecord;
	}MGNotifyNewTaskRecord;
	MSG_SIZE_GUARD( MGNotifyNewTaskRecord );

	typedef struct StrMGSendSysMailToPlayer
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SEND_SYSMAIL_TO_PLAYER;
		PlayerID lNotiPlayerID;
		GFSendSysMailToPlayer newSysMail;
	}MGSendSysMailToPlayer;
	MSG_SIZE_GUARD( MGSendSysMailToPlayer );

	typedef struct StrMGUpdateBackTaskInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_BACK_TASK_UPDATE;
		PlayerID lNotiPlayerID;
		GCUpdateNewTaskRecord taskUpdate;
	}MGUpdateBackTaskInfo;
	MSG_SIZE_GUARD( MGUpdateBackTaskInfo );

	//#define M_G_CHANGE_TARGET_NAME 0x5213	//更新目标的名字
	typedef struct StrMGChangeTargetName
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHANGE_TARGET_NAME;
		PlayerID lNotiPlayerID;	
		GCChangeTargetName changeMsg;
	}MGChangeTargetName;
	MSG_SIZE_GUARD(MGChangeTargetName);

	typedef struct StrMGUpdatePlayerChangeBaseInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_PLAYER_CHANGE_BASE_INFO;
		PlayerID lNotiPlayerID;
		GDUpdatePlayerChangeBaseInfo baseChangeInfo;
	}MGUpdatePlayerChangeBaseInfo;
	MSG_SIZE_GUARD(MGUpdatePlayerChangeBaseInfo);

	//#define M_G_SEND_SPECIAL_STRING 0x5221	//发送给客户端特殊的string
	typedef struct StrMGSendSpecialString
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SEND_SPECIAL_STRING;
		PlayerID lNotiPlayerID;
		GCSendSpecialString stringMsg;
	}MGSendSpecialString;
	MSG_SIZE_GUARD(MGSendSpecialString);

	//#define M_G_USE_SKILL_RST	0X5222	//使用技能的结果，**新技能体系的协议
	typedef struct StrMGUseSkillRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_USE_SKILL_RST;
		PlayerID lNotiPlayerID;
		GCUseSkillRst skillRst;
	}MGUseSkillRst;
	MSG_SIZE_GUARD(MGUseSkillRst);

	//#define M_G_RST_TO_ATKER                         0x5282//玩家攻击时返回给攻击者的攻击结果
	typedef struct StrMGRstToAtker
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_RST_TO_ATKER;
		PlayerID lNotiPlayerID;
		GCRstToAtker gcRstToAtker; 
	}MGRstToAtker;
	MSG_SIZE_GUARD(MGRstToAtker);

	//#define M_G_NORMAL_ATTACK                        0x5283//玩家普通攻击时广播给周围人的消息
	typedef struct StrMGNormalAttack
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NORMAL_ATTACK;
		PlayerID lNotiPlayerID;
		GCNormalAttack gcNormalAttack;
	}MGNormalAttack;
	MSG_SIZE_GUARD(MGNormalAttack);

	//#define M_G_SPLIT_ATTACK                         0x5284//玩家分割动作攻击时广播给周围人的消息
	typedef struct StrMGSplitAttack
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SPLIT_ATTACK;
		PlayerID lNotiPlayerID;
		GCSplitAttack gcSplitAttack;
	}MGSplitAttack;
	MSG_SIZE_GUARD(MGSplitAttack);

	//#define M_G_KICK_ALL_PLAYER 0x5214	//踢除所有玩家
	typedef struct StrMGKickAllPlayer
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_KICK_ALL_PLAYER;
	}MGKickAllPlayer;
	MSG_SIZE_GUARD(MGKickAllPlayer);

	//#define M_G_NOTIFY_ACTIVITY_STATE 0x5215 //通知活动状态
	typedef struct StrMGNotifyActivityState
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NOTIFY_ACTIVITY_STATE;
		PlayerID lNotiPlayerID;
		GCNotifyActivityState gateMsg;
	}MGNotifyActivityState;
	MSG_SIZE_GUARD(MGNotifyActivityState);

	//#define M_G_EXCEPTION_NOTIFY  0x5219	 //MapSrv上的异常通知
	typedef struct StrMGExceptionNotify
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NOTIFY_ACTIVITY_STATE;
		unsigned int exceptionReason;		//异常原因
		unsigned int srvId;					//出异常的srvId
	}MGExceptionNotify;
	MSG_SIZE_GUARD(MGExceptionNotify);

	//关闭仓库
	typedef struct StrGMCloseStoragePlate
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CLOSE_STORAGE_PLATE;
		PlayerID playerID;
	}GMCloseStoragePlate;
	MSG_SIZE_GUARD( GMCloseStoragePlate );

	//改变仓库锁定状态
	typedef struct StrGMChangeStorageLockState
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd =  G_M_CHANGE_STORAGE_LOCKSTATE;
		PlayerID playerID;
	}GMChangeStorageLockState;
	MSG_SIZE_GUARD( GMChangeStorageLockState );

	//从仓库中取出物品,放入背包
	typedef struct StrGMTakeItemFromStorage
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TAKE_ITEM_STORAGE;
		PlayerID playerID;
		unsigned int storageIndex;
	}GMTakeItemFromStorage;
	MSG_SIZE_GUARD( GMTakeItemFromStorage );

	//从仓库的指定位置移动到背包的指定位置
	typedef struct StrGMTakeItemFromStorageIndexToPkgIndex
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TAKE_ITEM_FROM_STORAGE_TO_PKG;
		PlayerID playerID;
		unsigned int storageIndex;
		unsigned int pkgIndex;
	}GMTakeItemFromStorageIndexToPkgIndex;
	MSG_SIZE_GUARD( GMTakeItemFromStorageIndexToPkgIndex );

	//从包裹取出物品,放入仓库
	typedef struct StrGMPutItemToStorage
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PUT_ITEM_TO_STORAGE;
		PlayerID playerID;
		unsigned int pkgIndex;
	}GMPutItemToStorage;
	MSG_SIZE_GUARD( GMPutItemToStorage );

	//从包裹的位置移动到仓库的位置
	typedef struct StrGMPutItemFromPkgIndexToStorageIndex
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PUT_ITEM_FROM_PKG_TO_STORAGE;
		PlayerID playerID;
		unsigned int pkgIndex;
		unsigned int storageIndex;
	}GMPutItemFromPkgIndexToStorageIndex;
	MSG_SIZE_GUARD( GMPutItemFromPkgIndexToStorageIndex );

	//设置新密码
	typedef struct StrGMSetNewStoragePassWord
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SET_NEW_STORAGE_PASSWORD;
		PlayerID playerID;
		char szStoragePassWord[MAX_STORAGE_PASSWORD_SIZE];//密码
		STORAGE_SECURITY_STRATEGY securityMode;//安全模式
	}GMSetNewStoragePassWord;
	MSG_SIZE_GUARD( GMSetNewStoragePassWord );

	//重新设置仓库密码
	typedef struct StrGMResetStoragePassWord
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RESET_STORAGE_PASSWORD;
		PlayerID playerID;
		char szOldStoragePassWord[MAX_STORAGE_PASSWORD_SIZE];//旧的密码
		char szStoragePassWord[MAX_STORAGE_PASSWORD_SIZE];//新密码
		STORAGE_SECURITY_STRATEGY securityMode;//新的安全模式
	}GMResetStoragePassWord;
	MSG_SIZE_GUARD( GMResetStoragePassWord );

	//从仓库取出物品来时验证密码 
	typedef struct StrGMTakeItemCheckPassWord
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TAKEITEM_CHECK_PASSWORD;
		PlayerID playerID;
		char szStoragePassWord[MAX_STORAGE_PASSWORD_SIZE];//仓库密码
	}GMTakeItemCheckPassWord;
	MSG_SIZE_GUARD( GMTakeItemCheckPassWord );

	typedef struct StrGMChangeStorageStateCheckPassWord
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CHANGELOCKSTATE_CHECK_PASSWORD;
		PlayerID playerID;
		char szStoragePassWord[MAX_STORAGE_PASSWORD_SIZE];//仓库密码
	}GMChangeStorageStateCheckPassWord;
	MSG_SIZE_GUARD( GMChangeStorageStateCheckPassWord );

	// 扩充
	typedef struct StrGMExtendStorage
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_EXTEND_STORAGE;
		PlayerID playerID;
	}GMExtendStorage;
	MSG_SIZE_GUARD( GMExtendStorage );

	//排序仓库
	typedef struct StrGMSortStorage
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SORT_STORAGE;
		PlayerID playerID;
		unsigned int storageLable;
	}GMSortStorage;
	MSG_SIZE_GUARD( GMSortStorage );

	typedef struct StrGMStorageItemInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_STORAGE_ITEM_INFO;
		PlayerID playerID;
		unsigned int labelIndex;
		ItemInfo_i   storageItemArr[STORAGE_PAGE_SIZE];
	}GMStorageItemInfo;
	MSG_SIZE_GUARD( GMStorageItemInfo );

	typedef struct StrGMStorageSafeInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_STORAGE_SAFE_INFO;
		PlayerID playerID;
		unsigned int validRow;
		STORAGE_SECURITY_STRATEGY securityMode;
		char  szStoragePsd[MAX_STORAGE_PASSWORD_SIZE];
	}GMStorageSafeInfo;
	MSG_SIZE_GUARD( GMStorageSafeInfo );

	typedef struct StrGMStorageForbiddenInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_STORAGE_FORBIDDEN_INFO;
		PlayerID playerID;
		unsigned int forbiddenTime;
		unsigned int inputErrCount;
	}GMStorageForbiddenInfo;
	MSG_SIZE_GUARD( GMStorageForbiddenInfo );

	typedef struct StrGMSwapStorageItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SWAP_STORAGE_ITEM;
		PlayerID playerID;
		unsigned int fromIndex;
		unsigned int toIndex;
	}GMSwapStorageItem;
	MSG_SIZE_GUARD( GMSwapStorageItem );

	//弹出仓库界面
	typedef struct StrMGShowStoragePlate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_STORAGE;
		PlayerID lNotiPlayerID;
	}MGShowStoragePlate;
	MSG_SIZE_GUARD( MGShowStoragePlate );

	//仓库安全信息
	typedef struct StrMGStorageSafeInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_STORAGE_SAFE_INFO ;
		PlayerID lNotiPlayerID;
		GCStorageSafeInfo safeInfo;
	}MGStorageSafeInfo;
	MSG_SIZE_GUARD( MGStorageSafeInfo );

	//仓库的道具信息
	typedef struct StrMGStorageItemInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_STORAGE_ITEM_INFO;
		PlayerID lNotiPlayerID;
		GCStorageItemInfo storageItemInfo;
	}MGStorageItemInfo;
	MSG_SIZE_GUARD( MGStorageItemInfo );

	//仓库弹出设置密码的对话框
	typedef struct StrMGShowStoragePasswordDlg
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_STORAGE_PASSWORD_DIALOG;
		PlayerID lNotiPlayerID;
		GCShowStoragePasswordDlg showPsdDlg;
	}MGShowStoragePasswordDlg;
	MSG_SIZE_GUARD( MGShowStoragePasswordDlg );

	//更新仓库的锁定状态 
	typedef struct StrMGChangeStorageLockState
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHANGE_STORAGE_LOCKSTATE ;
		PlayerID lNotiPlayerID;
		GCChangeStorageLockState changeStorageState;
	}MGChangeStorageLockState;
	MSG_SIZE_GUARD( MGChangeStorageLockState );

	// 更新仓库的可用行数
	typedef struct StrMGUpdateStorageValidRow
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_STORAGE_VALIDROW;
		PlayerID lNotiPlayerID;
		GCUpdateStorageValidRow updateValidRow;
	}MGUpdateStorageValidRow;
	MSG_SIZE_GUARD( MGUpdateStorageValidRow );

	//更新仓库的安全策略
	typedef struct StrMGUpdateStorageSecurityStrategy
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_STORAGE_SECURITY_STRATEGY;
		PlayerID lNotiPlayerID;
		GCUpdateStorageSecurityStrategy securityMode;
	}MGUpdateStorageSecurityStrategy;
	MSG_SIZE_GUARD( MGUpdateStorageSecurityStrategy );

	//更新仓库的道具信息 
	typedef struct StrMGUpdateStorageItemInfoByIndex
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_STORAGE_ITEMINFO_BYINDEX;
		PlayerID lNotiPlayerID;
		GCUpdateStorageItemInfoByIndex updateStorageItem;
	}MGUpdateStorageItemInfoByIndex;
	MSG_SIZE_GUARD(MGUpdateStorageItemInfoByIndex);

	//取出物品错误
	typedef struct StrMGTakeItemFromStorageError
	{
		static const unsigned char byPkgType =M_G;
		static const unsigned short wCmd = M_G_TAKEITEM_FROM_STORAGE_FAIL;
		PlayerID lNotiPlayerID;
		GCTakeItemFromStorageError  takeItemError;
	}MGTakeItemFromStorageError;
	MSG_SIZE_GUARD(MGTakeItemFromStorageError);

	//将物品放入仓库出错
	typedef struct StrMGPutItemToStorageError
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PUTITEM_TO_STORAGE_FAIL;
		PlayerID lNotiPlayerID;
		GCPutItemToStorageError putItemError;
	}MGPutItemToStorageError;
	MSG_SIZE_GUARD(MGPutItemToStorageError);

	//设置密码错误
	typedef struct StrMGSetNewPassWordError
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SET_NEWPASSWORD_ERROR;
		PlayerID lNotiPlayerID;
		GCSetNewPassWordError setNewPsdError;
	}MGSetNewPassWordError;
	MSG_SIZE_GUARD(MGSetNewPassWordError);

	//重新设置密码错误
	typedef struct StrMGResetPassWordError
	{
		static const unsigned char byPkgType =M_G;
		static const unsigned short wCmd = M_G_RESET_PASSWORD_ERROR;
		PlayerID lNotiPlayerID;
		GCResetPassWordError resetPsdError;
	}MGResetPassWordError;
	MSG_SIZE_GUARD( MGResetPassWordError );

	//取出物品时密码错误
	typedef struct StrMGTakeItemCheckPsdError
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_TAKEITEM_PASSWORD_ERROR;
		PlayerID lNotiPlayerID;
		GCTakeItemCheckPsdError checkPsdError;
	}MGTakeItemCheckPsdError;
	MSG_SIZE_GUARD(MGTakeItemCheckPsdError);

	//扩展仓库错误
	typedef struct StrMGExtendStorageError
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_EXTEND_STORAGE_ERROR;
		PlayerID lNotiPlayerID;
		GCExtendStorageError extendError;
	}MGExtendStorageError;
	MSG_SIZE_GUARD( MGExtendStorageError );

	typedef struct StrMGUpdateStorageItemInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_STORAGE_ITEM_INFO;
		PlayerID lNotiPlayerID;
		GFUpdateStorageItemInfo updateStorageItem;
	}MGUpdateStorageItemInfo;
	MSG_SIZE_GUARD( MGUpdateStorageItemInfo );

	typedef struct StrMGUpdateStorageSafeInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_STORAGE_SAFE_INFO;
		PlayerID lNotiPlayerID;
		GFUpdateStorageSafeInfo updateSafeInfo;
	}MGUpdateStorageSafeInfo;
	MSG_SIZE_GUARD( MGUpdateStorageSafeInfo );

	typedef struct StrMGUpdateStorageForbiddenInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_STORAGE_FORBIDDEN_INFO;
		PlayerID lNotiPlayerID;
		GFUpateStorageForbiddenInfo updateForbiddenInfo;
	}MGUpdateStorageForbiddenInfo;
	MSG_SIZE_GUARD( MGUpdateStorageForbiddenInfo );

	typedef struct StrMGQueryStorageInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_QUERY_STORAGE_INFO;
		PlayerID lNotiPlayerID;
		GFQueryStorageInfo  queryStorageInfo;
	}MGQueryStorageInfo;
	MSG_SIZE_GUARD( MGQueryStorageInfo );

	typedef struct StrGMSortPkgItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SORT_PKG_ITEM;
		PlayerID sortPlayerID;
		CGSortPkgItem sortPkgInfo;
	}GMSortPkgItem;
	MSG_SIZE_GUARD( GMSortPkgItem );

	typedef struct StrGMSortPkgItemBegin 
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SORT_PKG_ITEM_BEGIN;
		PlayerID sortPlayerID;
		CHAR	sortPage;
	}GMSortPkgItemBegin;
	MSG_SIZE_GUARD( GMSortPkgItemBegin );

	typedef struct StrGMSortPkgItemEnd 
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SORT_PKG_ITEM_END;
		PlayerID sortPlayerID;
	}GMSortPkgItemEnd;
	MSG_SIZE_GUARD( GMSortPkgItemEnd );

	typedef struct StrGMReplyCallMember 
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_REPLY_CALL_MEMBER;
		PlayerID replyPlayerID;
		CGReplyCallMember	replyCallMember;
	}GMReplyCallMember;
	MSG_SIZE_GUARD( GMReplyCallMember );

	//#define M_G_NOTIFY_ROOKIE_STATE	0x5234	//通知玩家新手状态
	typedef struct StrMGNotifyRookieState
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NOTIFY_ROOKIE_STATE;
		PlayerID lNotiPlayerID;
		GCNotifyRookieState rookieMsg;
	}MGNotifyRookieState;
	MSG_SIZE_GUARD( MGNotifyRookieState );

	typedef struct StrMGUnionMultiReq
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UNION_MULTI_REQ;
		PlayerID lNotiPlayerID;
		USHORT  type;
		USHORT  multi;
		UINT    endTime;
	}MGUnionMultiReq;
	MSG_SIZE_GUARD( MGUnionMultiReq );

	typedef struct StrMGSaveRebirthPosNtf
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SAVE_REBIRTH_POS_NTF;
		PlayerID lNotiPlayerID;
		GCSaveRebirthPosNtf saveRebirthPos;
	}MGSaveRebirthPosNtf;
	MSG_SIZE_GUARD( MGSaveRebirthPosNtf );

    //#define M_G_PLAYER_POSINFO       0x522f //通知玩家或怪物(GID)位置信息
	typedef struct StrMGPlayerPosInfo
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT	wCmd = M_G_PLAYER_POSINFO;
		PlayerID            lNotiPlayerID;
		GCPlayerPosInfo     playerPosInfo;//玩家位置信息；
	}MGPlayerPosInfo;
	MSG_SIZE_GUARD( MGPlayerPosInfo );

	typedef struct StrFGNoticeStorageItemInfo
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_STORAGE_ITEM_INFO;
		PlayerID playerID;
		GMStorageItemInfo itemInfo;
	}FGNoticeStorageItemInfo;
	MSG_SIZE_GUARD( FGNoticeStorageItemInfo );

	typedef struct StrFGNoticeStorageSafeInfo
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_STORAGE_SAFE_INFO;
		PlayerID playerID;
		GMStorageSafeInfo safeInfo;
	}FGNoticeStorageSafeInfo;
	MSG_SIZE_GUARD( FGNoticeStorageSafeInfo );

	typedef struct StrFGNoticeStorageForbiddenInfo
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_STORAGE_FORBIDDEN_INFO;
		PlayerID playerID;
		GMStorageForbiddenInfo forbiddenInfo;
	}FGNoticeStorageForbiddenInfo;
	MSG_SIZE_GUARD( FGNoticeStorageForbiddenInfo );

	typedef struct StrFGNoticeFirstUseStorage
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_NEW_STROAGE_NOTIFY;
		PlayerID playerID;
		GCShowStoragePasswordDlg showPsdDlg;
	}FGNoticeFirstUseStorage;
	MSG_SIZE_GUARD( FGNoticeFirstUseStorage );

	typedef struct StrGMGMQueryReq
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_M_GMQUERY_REQ;
		PlayerID gmPlayerId;
		PlayerID targetPlayerId;
		GTCmd_Query_Req gmReq;
	}GMGMQueryReq;
	MSG_SIZE_GUARD(GMGMQueryReq);

	//#define G_M_ITEM_NPC_REQ					0x15d4		//ITEM NPC的查询ID
	typedef struct StrGMItemNpcReq
	{
		static const unsigned char byPkgType = G_E;
		static const unsigned short wCmd = G_M_ITEM_NPC_REQ;
		PlayerID lNotiPlayerID;
		CGBuildNpcReq req;
	}GMItemNpcReq;
	MSG_SIZE_GUARD(GMItemNpcReq);

	typedef struct StrMGGMQueryResp
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_GMQUERY_RESP;
		PlayerID lNotiPlayerID;//玩家自身ID
		GTCmd_Query_Rsp resp;
	}MGGMQueryResp;
	//MSG_SIZE_GUARD(MGGMQueryResp);	

	//#define M_G_ITEMNPC_INFO 0x5236 //物件NPC通知
	typedef struct StrMGItemNpcStatusInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ITEMNPC_INFO;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCBuildNpcStatusInfo itemNpc;
	}MGItemNpcStatusInfo;
	MSG_SIZE_GUARD(MGItemNpcStatusInfo);

	//#define M_G_ITEMNPC_ID	0x5237	//物件NPCid的通知
	typedef struct StrMGItemNpcID
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_ITEMNPC_ID;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCBuildNpcID	idInfo;		//ID信息 
	}MGItemNpcID;
	MSG_SIZE_GUARD(MGItemNpcID);

	typedef struct StrGMBatchSellItemsReq
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_BATCH_SELL_ITEM_REQ;
		PlayerID lNotiPlayerID;//玩家自身ID
		CGBatchSellItemsReq	req;//ID信息 
	}GMBatchSellItemsReq;
	MSG_SIZE_GUARD(GMBatchSellItemsReq);

	typedef struct StrGMRepurchaseItemReq
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_REPURCHASE_ITEM_REQ;
		PlayerID lNotiPlayerID;//玩家自身ID
		CGRepurchaseItemReq	req;//ID信息 
	}GMRepurchaseItemReq;
	MSG_SIZE_GUARD(GMRepurchaseItemReq);

	typedef struct StrMGBatchSellItemsResp
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_BATCH_SELL_ITEMS_RESP;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCBatchSellItemsResp resp;//ID信息 
	}MGBatchSellItemsResp;
	MSG_SIZE_GUARD(MGBatchSellItemsResp);

	typedef struct StrMGRepurchaseItemResp
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_REPURCHASE_ITEM_RESP;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCRepurchaseItemResp resp;//ID信息 
	}MGRepurchaseItemResp;
	MSG_SIZE_GUARD(MGRepurchaseItemResp);

	typedef struct StrMGRepurchaseItemListNtf
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_REPURCHASE_ITEM_LIST;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCRepurchaseItemsListNtf ntf;//ID信息 
	}MGRepurchaseItemListNtf;
	MSG_SIZE_GUARD(MGRepurchaseItemListNtf);

	typedef struct StrMGCloseOLTestPlate
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_CLOSE_OLTEST_PLATE;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCCloseOLTestDlg closeTestDlg;
	}MGCloseOLTestPlate;
	MSG_SIZE_GUARD(MGCloseOLTestPlate);

	typedef struct StrMGShowProvueTestDlg
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_PREVUE_OLTEST_DLG;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCShowPrevueOLTestDlg showPrevueDlg;
	}MGShowProvueTestDlg;
	MSG_SIZE_GUARD( MGShowProvueTestDlg );

	typedef struct StrMGShowOLTestQuestion
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_OLTEST_QUESTION;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCShowOLTestQuestion showQuestion;
	}MGShowOLTestQuestion;
	MSG_SIZE_GUARD(MGShowOLTestQuestion);

	typedef struct StrMGAnswerQuestionRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_ANSWER_QUESTION_RST;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCAnswerQuestionRst answerQuestionRst;
	}MGAnswerQuestionRst;
	MSG_SIZE_GUARD(MGAnswerQuestionRst);

	typedef struct StrMGShowAnswerAllQuestionsDlg
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_ANSWER_ALLQUESTIONS;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCShowAnswerAllQuestionsDlg answerAllDlg;
	}MGShowAnswerAllQuestionsDlg;
	MSG_SIZE_GUARD(MGShowAnswerAllQuestionsDlg);


	typedef struct StrGMCloseOLTestDlg
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd     = G_M_CLOSE_OLTEST_PLATE;
		PlayerID closePlayerID;
	}GMCloseOLTestDlg;
	MSG_SIZE_GUARD(GMCloseOLTestDlg);

	typedef struct StrGMAnswerOLTestQuestion
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_ANSWER_OLTEST_QUESTION;
		PlayerID answerPlayerID;
		unsigned int selectID;
	}GMAnswerOLTestQuestion;
	MSG_SIZE_GUARD( GMAnswerOLTestQuestion );

	typedef struct StrGMDropOLTest
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_DROP_OLTEST;
		PlayerID dropTestPlayerID;
	}GMDropOLTest;
	MSG_SIZE_GUARD( GMDropOLTest );

	typedef struct StrGMSinglePlayerTeamIDNotify
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_SINGLE_PLAYER_TEAMID_NOTIFY;
		PlayerID upLinePlayerID;
		unsigned int teamID;//队伍ID
		unsigned int playerFlag;//playerflag
		unsigned int teamFlag;//teamflag
	}GMSinglePlayerTeamIDNotify;
	MSG_SIZE_GUARD( GMSinglePlayerTeamIDNotify );

	//#define G_M_NEW_COPY_CMD                 0x15dc //通知创建新副本；
	typedef struct StrGMNewCopyCmd
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_NEW_COPY_CMD;
		CopyManagerEle copyInfo;//欲建副本信息；
	}GMNewCopyCmd;
	MSG_SIZE_GUARD(GMNewCopyCmd);

	//#define G_M_DEL_COPY_CMD                 0x15dd //通知删除旧副本；
	typedef struct StrGMDelCopyCmd
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_DEL_COPY_CMD;
		CopyManagerEle copyInfo;//欲删副本信息；
	}GMDelCopyCmd;
	MSG_SIZE_GUARD(GMDelCopyCmd);

	//#define G_M_DEL_COPY_QUEST               0x15de //centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
	typedef struct StrGMDelCopyQuest
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_DEL_COPY_QUEST;
		CopyManagerEle copyInfo;//欲删副本信息；
	}GMDelCopyQuest;
	MSG_SIZE_GUARD(GMDelCopyQuest);

    //#define G_M_LEAVE_STEAM_NOTI             0x15eb //通知玩家离开副本组队(删去玩家在各副本中的第一次进入信息，并从当前副本中踢出去)；
	typedef struct StrGMLeaveSTeamNoti
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_LEAVE_STEAM_NOTI;
		PlayerID leavePlayerID;//离开组队的玩家ID号；
	}GMLeaveSTeamNoti;
	MSG_SIZE_GUARD(GMLeaveSTeamNoti);

    //#define G_M_QUERY_GLOBE_COPYINFOS        0x15ed //向mapsrv查询全局副本信息(该mapsrv管理的全部伪副本)
	typedef struct StrGMQueryGlobeCopyInfos
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_QUERY_GLOBE_COPYINFOS;
		PlayerID notiPlayerID;//进行查询的玩家ID号，查询结果向此玩家返回;
	}GMQueryGlobeCopyInfos;
	MSG_SIZE_GUARD(GMQueryGlobeCopyInfos);

    //#define G_M_TIMELIMIT_RES                0x15f2 //客户端点击倒计时确认框
	typedef struct StrGMTimeLimitRes
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_TIMELIMIT_RES;
		PlayerID resPlayerID;//点击确认的玩家ID号；
		unsigned int limitReason;//倒计时原因；
	}GMTimeLimitRes;
	MSG_SIZE_GUARD(GMTimeLimitRes);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	typedef struct StrMGSplitItemError
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SPLIT_ITEM_ERROR;
		PlayerID lNotiPlayerID;
		GCSplitItemError errMsg; 
	}MGSplitItemError;
	MSG_SIZE_GUARD(MGSplitItemError);

	typedef struct StrMGLogPlayerInfo
	{
		enum
		{
			ROLE_LEVEL_UP = 0,//玩家升级
			SKILL_LEVEL_UP,//技能省级
			ROLE_GET_ITEM,//获取物品
			ROLE_USE_ITEM,//使用物品
			ROLE_DROP_ITEM,//丢弃物品
			ROLE_TASK_CHANAGE,//任务变化
			ROLE_DEAD,//角色死亡
			ROLE_MAP_CHANAGE,//地图切换
			ROLE_TRADE,//交易	
			ROLE_SP_POWER_CHANGE,//sp豆变化
			ROLE_ITEM_PROP_CHANGE,//物品属性变化
			ROLE_MONEY_CHANGE,//金钱变化
			ROLE_PET_ADD,//宠物增加	
			ROLE_PET_LEVEL_UP,//宠物升级
			ROLE_PET_GAIN_SKILL,//宠物获取技能
			ROLE_PET_STATE_CHANGE,//宠物状态变化
			ROLE_USE_SKILL,//使用技能
			ROLE_KILL_PLAYER,//杀死玩家
			ROLE_REBIRTH,//玩家复活
			ROLE_REPAIR_ITEM,//修复物品
			MONSTER_DIE,//怪物死亡
			INSTANCE_CHANGE,//副本变化
			NPC_SHOP_TRADE,//商店买卖
			WAREHOUSE_OPEN,//打开仓库
			WAREHOUSE_ITEM_OPERATE,//从仓库存取物品
			WAREHOUSE_MONEY_OPERATE,//从仓库存取金钱
			WAREHOUSE_EXTEND,//仓库扩展
			WAREHOUSE_PRIVATE,//仓库安全设置
		};

		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_LOG_PLAYER_INFO;
		PlayerID lNotiPlayerID;
		unsigned int logID;
		union
		{
			LOG_SVC_NS::GORoleLevelUP levelUp;
			LOG_SVC_NS::GOSkillLevelUP skillLevelUp;
			LOG_SVC_NS::GORoleItemOwn getItem;
			LOG_SVC_NS::GORoleItemUse useItem; 
			LOG_SVC_NS::GORoleItemDrop dropItem;
			LOG_SVC_NS::GORoleQuestChange taskChanage;
			LOG_SVC_NS::GORoleDead  playerDead;
			LOG_SVC_NS::GORoleMapChange mapChanage;
			LOG_SVC_NS::GORoleTrade trade;
			LOG_SVC_NS::GOSPChange spChange;
			LOG_SVC_NS::GOItemPropertyChange itemChange;
			LOG_SVC_NS::GOMoneyChange moneyChange;
			LOG_SVC_NS::GOPetAdd petAdd;
			LOG_SVC_NS::GOPetLevelup petLevelUp;
			LOG_SVC_NS::GOPetFunctionAdd petAddSkill;
			LOG_SVC_NS::GOPetStateChange petStateChange;
			LOG_SVC_NS::GOUseSkill useskill;
			LOG_SVC_NS::GOKillPlayer killPlayer;
			LOG_SVC_NS::GORelive rebirth;
			LOG_SVC_NS::GOItemRepair repairItem;
			LOG_SVC_NS::GOMonsterDead monsterDead;
			LOG_SVC_NS::GORealInstanceLifeCycle instanceChange;
			LOG_SVC_NS::GOShopTrade npcShop;
			LOG_SVC_NS::GOWarehouseOpen warehouseOpen;
			LOG_SVC_NS::GOWarehouseItemTransport warehouseItemOperate;
			LOG_SVC_NS::GOWarehouseMoneyTransport warehouseMoneyOperate;
			LOG_SVC_NS::GOWarehouseExtend warehouseExtend;
			LOG_SVC_NS::GOWarehousePrivateOP warehousePrivate;
		}logInfo;
	}MGLogPlayerInfo;
	MSG_SIZE_GUARD(MGLogPlayerInfo);

    //#define M_G_PLAYER_TRY_COPY_QUEST          0x5242 //玩家试图进副本
	typedef struct StrMGPlayerTryCopyQuest
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_TRY_COPY_QUEST;
		PlayerID lNotiPlayerID;//进副本玩家ID
		bool         isPCopy;//是否为伪副本，伪副本为true，真副本为false;
		EnterCopyReq enterCopyReq;//进入请求内容；
	}MGPlayerTryCopyQuest;
	MSG_SIZE_GUARD(MGPlayerTryCopyQuest);

	//#define M_G_PLAYER_LEAVE_COPY_QUEST  0x5243 //玩家离开副本请求
	typedef struct StrMGPlayerLeaveCopyQuest
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_LEAVE_COPY_QUEST;
		PlayerID lNotiPlayerID;//离开副本玩家ID	
		CopyManagerEle leaveCopyInfo;//欲离开的副本信息；
	}MGPlayerLeaveCopyQuest;
	MSG_SIZE_GUARD(MGPlayerLeaveCopyQuest);

	//#define M_G_COPY_NOMORE_PLAYER       0x5244 //指示副本不可再进人(准备删副本前调用)
	typedef struct StrMGCopyNomorePlayer
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_COPY_NOMORE_PLAYER;
		unsigned int copyid;//不可再进的副本id号；
	}MGCopyNomorePlayer;
	MSG_SIZE_GUARD(MGCopyNomorePlayer);

	//#define M_G_DEL_COPY_QUEST           0x5245 //删副本请求
	typedef struct StrMGDelCopyQuest
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_DEL_COPY_QUEST;
		unsigned int copyid;//待删副本id号；
	}MGDelCopyQuest;
	MSG_SIZE_GUARD(MGDelCopyQuest);

	//#define M_G_PLAYER_OUTMAPINFO        0x5246 //玩家离开副本后去往的地图信息；
	typedef struct StrMGPlayerOutMapInfo
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_OUTMAPINFO;
		PlayerID lNotiPlayerID;//对象玩家；
		unsigned short mapID;
		unsigned short posX;
		unsigned short posY;
	}MGPlayerOutMapInfo;
	MSG_SIZE_GUARD(MGPlayerOutMapInfo);

	//#define M_G_GLOBE_COPYINFOS          0x5247 //玩家查询全局副本信息，返回本mapsrv管理的伪副本信息
	typedef struct StrMGGlobeCopyInfos
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_GLOBE_COPYINFOS;
		PlayerID         notiPlayerID;
		GCGlobeCopyInfos globeCopyInfos;	
	}MGGlobeCopyInfos;
	MSG_SIZE_GUARD(MGGlobeCopyInfos);

    //#define M_G_TIMELIMIT                0x5248 //指示客户端弹倒计时框；
	typedef struct StrMGTimeLimit
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_TIMELIMIT;
		PlayerID         lNotiPlayerID;
		GCTimeLimit  limitTime;	
	}MGTimeLimit;
	MSG_SIZE_GUARD(MGTimeLimit);

	//#define M_G_BUFF_START               0x5250  //BUFF start
	typedef struct StrMGBuffStart
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_BUFF_START;
		PlayerID lNotiPlayerID;
		GCBuffStart buffStart;
	}MGBuffStart;
	MSG_SIZE_GUARD(MGBuffStart);

	//#define M_G_BUFF_END					0x5251	//buff end
	typedef struct StrMGBuffEnd
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_BUFF_END;
		PlayerID lNotiPlayerID;
		GCBuffEnd buffEnd;
	}MGBuffEnd;
	MSG_SIZE_GUARD(MGBuffEnd);

	//#define M_G_BUFF_STATUS_UPDATE        0x5252	//buff change
	typedef struct StrMGBuffStatusUpdate
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_BUFF_STATUS_UPDATE;
		PlayerID lNotiPlayerID;
		GCBuffStatusUpdate buffChange;
	}MGBuffStatusUpdate;
	MSG_SIZE_GUARD(MGBuffStatusUpdate);

	//#define M_G_BUFF_HPMP_UPDATE			0x5253	//buff dot hot
	typedef struct StrMGBuffHpMpUpdate
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_BUFF_HPMP_UPDATE;
		PlayerID lNotiPlayerID;
		GCBuffHpMpUpdate hpmpUpdate;
	}MGBuffHpMpUpdate;
	MSG_SIZE_GUARD(MGBuffHpMpUpdate);

	typedef struct StrGMSelectWarRebirthOption
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_SELECT_WAR_REBIRTH_OPTION;
		PlayerID lNotiPlayerID;//玩家自身ID
		CGSelectWarRebirthOption	req; 
	}GMSelectWarRebirthOption;
	MSG_SIZE_GUARD(GMSelectWarRebirthOption);

	typedef struct StrGMRepurchaseItemsListReq
	{
		static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_REPURCHASE_ITEMS_LIST_REQ;
		PlayerID lNotiPlayerID;//玩家自身ID
		CGRepurchaseItemsListReq	req; 
	}GMRepurchaseItemsListReq;
	MSG_SIZE_GUARD(GMRepurchaseItemsListReq);

	typedef struct StrMGPlayerWarDying
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_WAR_DYING;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCPlayerWarDying dying;
	}MGPlayerWarDying;
	MSG_SIZE_GUARD(MGPlayerWarDying);

	typedef struct StrMGWarRebirthOptionErr
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_WAR_REBIRTH_OPTION_ERR;
		PlayerID lNotiPlayerID;//玩家自身ID
		GCSelectWarRebirthOptionErr warRebirthError;
	}MGWarRebirthOptionErr;
	MSG_SIZE_GUARD(MGWarRebirthOptionErr);

	typedef struct StrMGRaceMasterTimeInfoExpire
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NOTICE_RACEMASTER_TIMEINFO_EXPIRE;
		PlayerID lNotiPlayerID;//玩家自身ID
		unsigned short racetype;//玩家种族
	}MGRaceMasterTimeInfoExpire;
	MSG_SIZE_GUARD( MGRaceMasterTimeInfoExpire );

	typedef struct StrMGRaceMasterDeclareWarInfoChange
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd =  M_G_NOTICE_RACEMASTER_DECALREWARINF0_CHANGE;
		PlayerID lNotiPlayerID;//玩家自身ID
		unsigned short racetype;
		char declarewarinfo;//玩家信息
	}MGRaceMasterDeclareWarInfoChange;
	MSG_SIZE_GUARD(MGRaceMasterDeclareWarInfoChange);

	typedef struct StrMGRaceMasterTimeExpireInfoChange
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NOTICE_RACEMASTER_TIMEINFO_CHANGE;
		PlayerID lNotiPlayerID;//玩家自身ID;
		unsigned short racetype;
		unsigned char noticeCount;//通知次数
		unsigned int  lastupdatetime;//最后一次更新次数
		bool          isGotSalary;//是否领取过薪水
	}MGRaceMasterTimeExpireInfoChange;
	MSG_SIZE_GUARD(MGRaceMasterTimeExpireInfoChange);

	//typedef struct StrMGNoticeRaceMasterPublicMsg
	//{
	//	static const unsigned char byPkgType = M_G;
	//	static const unsigned short wCmd =  M_G_NOTICE_RACEMASTER_PUBLICMSG;
	//	PlayerID  lNotiPlayerID;//玩家自身ID;
	//	GCRaceMasterNoticesNewMsg newmsg;
	//}MGNoticeRaceMasterPublicMsg;
	//MSG_SIZE_GUARD(MGNoticeRaceMasterPublicMsg);

	typedef struct StrMGNoticeRaceMasterPublicMsgChange
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_NOTICE_RACEMASTER_PUBLICMSG_CHANGE;
		PlayerID lNotiPlayerID;//玩家自身的ID
		unsigned short racetype;
		char msgArr[MAX_RACE_NOTICE_LEN];
	}MGNoticeRaceMasterPublicMsgChange;
	MSG_SIZE_GUARD(MGNoticeRaceMasterPublicMsgChange);

	typedef struct StrMGUpdateRaceMasterInfoToDB
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UPDATE_RACEMASTER_INFO_TO_DB;
		PlayerID lNotiPlayerID;//玩家自身的ID
		unsigned short racetype;
		unsigned int   masterUID;
		unsigned char  noticeCount;
		unsigned int   lastupdatetime;
		bool           isGotSalary;
		char           declareWarInfo;
	}MGUpdateRaceMasterInfoToDB;
	MSG_SIZE_GUARD(MGUpdateRaceMasterInfoToDB);

	typedef struct StrMGRaceMasterDeclareWarRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_RACEMASTER_DECLAREWAR_RST;
		PlayerID lNotiPlayerID;//玩家自身的ID
		int  result;
		char declarewarinfo;
	}MGRaceMasterDeclareWarRst;
	MSG_SIZE_GUARD(MGRaceMasterDeclareWarRst);

	typedef struct StrMGRaceMasterGetSalaryRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd =  M_G_RACEMASTER_GET_SALARY_RST;
		PlayerID lNotiPlayerID;//玩家自身的ID
		int result;
		int silverMoney;
		int goldMoney;
	}MGRaceMasterGetSalaryRst;
	MSG_SIZE_GUARD(MGRaceMasterGetSalaryRst);

	typedef struct StrMGRaceMasterSetNewMsgRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_RACEMASTER_NOTICE_NEWMSG_RST;
		PlayerID lNotiPlayerID;//玩家自身的ID
		int result;
	}MGRaceMasterSetNewMsgRst;
	MSG_SIZE_GUARD(MGRaceMasterSetNewMsgRst);

	typedef struct StrMGShowRaceMasterOPPlate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd =  M_G_SHOW_RACEMASTER_OPPLATE;
		PlayerID lNotiPlayerID;//玩家自身的ID
		GCShowRaceMasterOpPlate showplate;
	}MGShowRaceMasterOPPlate;
	MSG_SIZE_GUARD(MGShowRaceMasterOPPlate);

	typedef struct StrMGShowRaceMasterDeclareWarPlate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_RACEMASTER_DECLAREWAR_PLATE;
		PlayerID lNotiPlayerID;//玩家自身的ID
		GCShowRaceMasterDeclareWarPlate showwarplate;
	}MGShowRaceMasterDeclareWarPlate;
	MSG_SIZE_GUARD(MGShowRaceMasterDeclareWarPlate);

	typedef struct StrMGShowRaceMasterPublicMsgPlate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_RACEMASTER_PUBLICMSG_PLATE;
		PlayerID lNotiPlayerID;//玩家自身的ID
		GCShowRaceMasterNoticeMsgPlate showmsgplate;
	}MGShowRaceMasterPublicMsgPlate;
	MSG_SIZE_GUARD(MGShowRaceMasterPublicMsgPlate);

	typedef struct StrMGCloseRaceMasterPlate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CLOSE_RACEMASTER_PLATE;
		PlayerID lNotiPlayerID;//玩家自身的ID
		GCRaceMasterClosePlate closeplate;
	}MGCloseRaceMasterPlate;
	MSG_SIZE_GUARD(MGCloseRaceMasterPlate);

	typedef struct StrMGShowRaceMasterAssignCityPlate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHOW_RACEMASTER_ASSIGNCITY_PLATE;
		PlayerID lNotiPlayerID;//玩家自身的ID
		GCShowRaceMasterAssignCityPlate assignPlate;
	}MGShowRaceMasterAssignCityPlate;
	MSG_SIZE_GUARD(MGShowRaceMasterAssignCityPlate);

	typedef struct StrGMOnRaceMasterSetNewPublicMsg
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_ON_RACEMASTER_SETNEW_PUBLICMSG;
		unsigned short raceType;
		char msgArr[MAX_RACE_NOTICE_LEN];
	}GMOnRaceMasterSetNewPublicMsg;
	MSG_SIZE_GUARD(GMOnRaceMasterSetNewPublicMsg);

	typedef struct StrGMRaceMasterTimeInfoChange
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RACEMASTER_TIMEINFO_CHANGE;
		unsigned short  raceType;
		unsigned char noticeCount;//通知次数
		unsigned int  lastupdatetime;//最后一次更新次数
		bool          isGotSalary;//是否领取过薪水
	}GMRaceMasterTimeInfoChange;
	MSG_SIZE_GUARD(GMRaceMasterTimeInfoChange);

	typedef struct StrGMRaceMasterTimeInfoExpire
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RACEMASTER_TIMEINFO_EXPIRE;
		unsigned short raceType;
	}GMRaceMasterTimeInfoExpire;
	MSG_SIZE_GUARD(GMRaceMasterTimeInfoExpire);

	typedef struct StrGMRaceMasterDeclareInfoChange
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd= G_M_RACEMASTER_DECLAREWARINFO_CHANGE;
		unsigned short raceType;
		char declarewarInfo;
	}GMRaceMasterDeclareInfoChange;
	MSG_SIZE_GUARD(GMRaceMasterDeclareInfoChange);

	typedef struct StrGMRecvRaceMasterDetialInfo
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RACEMASTER_DETIALINFO;
		unsigned short  raceType;
		unsigned int    masterUID;
		unsigned char   noticeCount;//通知次数
		unsigned int    lastupdatetime;//最后一次更新次数
		bool            isGotSalary;//是否领取过薪水
		char            declarewarinfo;
	}GMRecvRaceMasterDetialInfo;
	MSG_SIZE_GUARD(GMRecvRaceMasterDetialInfo);

	typedef struct StrGMRaceMasterNewBorn
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RACEMASTER_NEW_BORN;
		unsigned short racetype;
		unsigned int masterUID;
		char  playerName[MAX_NAME_SIZE];
		char  unionName[MAX_UNIONNAME_LEN];
	}GMRaceMasterNewBorn;
	MSG_SIZE_GUARD(GMRaceMasterNewBorn);

	typedef struct StrGMRaceMasterSelectOpType
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RACEMASTER_SELECT_OPTYPE;
		PlayerID selectPlayerID;
		char opType;
	}GMRaceMasterSelectOpType;
	MSG_SIZE_GUARD(GMRaceMasterSelectOpType);

	typedef struct StrGMRaceMasterDeclareWarToOtherRace
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RACEMASTER_DECLAREWAR_TO_OTHERRACE;
		PlayerID  playerID;
		unsigned  short race;
		unsigned  short otherracetype;
	}GMRaceMasterDeclareWarToOtherRace;
	MSG_SIZE_GUARD(GMRaceMasterDeclareWarToOtherRace);

	typedef struct StrGMRaceMasterSetNewPublicMsg
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_RACEMASTER_SETNEW_PUBLICMSG;
		PlayerID  playerID;
		unsigned short race;
		char msgArr[MAX_RACE_NOTICE_LEN];
	}GMRaceMasterSetNewPublicMsg;
	MSG_SIZE_GUARD(GMRaceMasterSetNewPublicMsg);

	typedef struct StrMGLoadRaceMasterInfoFromDB
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_LOAD_RACEMASTER_INFO_FROM_DB;
	}MGLoadRaceMasterInfoFromDB;
	MSG_SIZE_GUARD( MGLoadRaceMasterInfoFromDB );

	typedef struct StrGMMapSrvLoadRaceMasterRst
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_LOAD_RACEMASTER_FROM_DB_RST;
		unsigned short race;
		unsigned int masteruid;
		unsigned int todaymodifycount;
		unsigned int lastupdatetime;
		char     declarewarinfo;
		bool     isgotsalary;
	}GMMapSrvLoadRaceMasterRst;
	MSG_SIZE_GUARD(GMMapSrvLoadRaceMasterRst);

	typedef struct StrMGResetRaceMasterDeclareWarInfo
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_RESET_RACEMASTER_DECLAREWAR;
		unsigned short racetype;
	}MGResetRaceMasterDeclareWarInfo;
	MSG_SIZE_GUARD(MGResetRaceMasterDeclareWarInfo);

	//#define M_G_DISPLAY_WAR_TIMER						0x526e	//通知计时器
	typedef struct StrMGDisplayWarTimer
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_DISPLAY_WAR_TIMER;
		PlayerID lNotiPlayerID;
		GCDisplayWarTimer gcTimer;
	}MGDisplayWarTimer;
	MSG_SIZE_GUARD(MGDisplayWarTimer);

	//#define M_G_DISPLAY_WAR_COUNTER						0x526f	//通知计数器
	typedef struct StrMGDisplayWarCounter
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_DISPLAY_WAR_COUNTER;
		PlayerID lNotiPlayerID;
		GCDisplayWarCounter gcCounter;
	}MGDisplayWarCounter;
	MSG_SIZE_GUARD(MGDisplayWarCounter);

	//#define M_G_CHARATER_DAMAGE_REBOUND					0x5270	//伤害反弹
	typedef struct StrMGCharacterDamageRebound
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_CHARATER_DAMAGE_REBOUND;
		PlayerID lNotiPlayerID;
		GCCharacterDamageRebound gcRebound;
	}MGCharacterDamageRebound;
	MSG_SIZE_GUARD(MGCharacterDamageRebound);

	typedef struct StrGFQueryRaceMasterInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_QUERY_RACEMASTER_INFO;
	}GFQueryRaceMasterInfo;
	MSG_SIZE_GUARD(GFQueryRaceMasterInfo);

	typedef struct StrGFUpdateRaceMasterInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_UPDATE_RACEMASTER_INFO;
		MGUpdateRaceMasterInfoToDB updateInfo;
	}GFUpdateRaceMasterInfo;
	MSG_SIZE_GUARD(GFUpdateRaceMasterInfo);

	typedef struct StrGFNewRaceMasterBorn
	{
		static const unsigned char byPkgType =G_F;
		static const unsigned short wCmd = G_F_NEW_RACEMASTER_BORN;
		unsigned short racetype;
		unsigned int  masteruid;
		char  playerName[MAX_NAME_SIZE];
		char  unionName[MAX_UNIONNAME_LEN];
	}GFNewRaceMasterBorn;

	typedef struct StrGFResetRaceMasterDeclareWarInfo
	{
		static const unsigned char byPkgType =G_F;
		static const unsigned short wCmd = G_F_RESET_RACEMASTER_DECLAREWARINFO;
		unsigned int racetype;
	}GFResetRaceMasterDeclareWarInfo;

	typedef struct StrFGQueryRaceMasterInfoRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_QUERY_RACEMASTER_INFO_RST;
		GMMapSrvLoadRaceMasterRst queryRst;
	}FGQueryRaceMasterInfoRst;

	MSG_SIZE_GUARD(FGQueryRaceMasterInfoRst);

	typedef struct StrFGStartSelectRaceMaster
	{
		static const unsigned char byPkgType =F_G;
		static const unsigned short wCmd = F_G_START_SELECT_RACEMASTER;
	}FGStartSelectRaceMaster;
	MSG_SIZE_GUARD(FGStartSelectRaceMaster);

	typedef struct StrFGForbiddenRaceMasterAuthority
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_FORBIDDEN_RACEMASTER_AUTHORITY;
	}FGForbiddenRaceMasterAuthority;
	MSG_SIZE_GUARD(FGForbiddenRaceMasterAuthority);


	typedef struct StrGMForbiddenRaceMasterAuthority
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_FORBIDDEN_RACEMASTER_AUTHORITY;
	}GMForbiddenRaceMasterAuthority;
	MSG_SIZE_GUARD(GMForbiddenRaceMasterAuthority);

	typedef struct StrFGSendMailSuccNotifyGM
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short  wCmd = F_G_SENDMAIL_SUCCESS_NOTI_GM;
		char sendMailName[MAX_NAME_SIZE];
		unsigned int recvPlayerUID;
		unsigned int mailUID;
	}FGSendMailSuccNotifyGM;
	MSG_SIZE_GUARD(FGSendMailSuccNotifyGM);

	typedef struct StrGFLoadNewPublicNotice
	{
		static const unsigned char byPkgType= G_F;
		static const unsigned short wCmd = G_F_LOAD_NEW_PUBLICNOTICE;
	}GFLoadNewPublicNotice;
	MSG_SIZE_GUARD(GFLoadNewPublicNotice);

	typedef struct StrGFGMAddNewPublicNotice
	{
		static const unsigned char byPkgType =G_F;
		static const unsigned short wCmd = G_F_ADD_NEW_PUBLICNOTICE;
		PublicNotice srvmsg;
	}GFGMAddNewPublicNotice;
	MSG_SIZE_GUARD(GFGMAddNewPublicNotice);

	typedef struct StrGFModifyPublicNotice
	{
		static const unsigned char byPkgType =G_F;
		static const unsigned short wCmd=  G_F_MODIFY_PUBLICNOTICE;
		PublicNotice srvmsg;
	}GFModifyPublicNotice;
	MSG_SIZE_GUARD(GFModifyPublicNotice);

	typedef struct StrGFRemovePublicNotice
	{
		static const unsigned char byPkgType =G_F;
		static const unsigned short wCmd= G_F_DELETE_PUBLICNOTICE;
		unsigned int msgid;
	}GFRemovePublicNotice;
	MSG_SIZE_GUARD(GFRemovePublicNotice);

	typedef struct StrGFQueryAllPublicNotice
	{
		static const unsigned char byPkgType=G_F;
		static const unsigned short wCmd = G_F_QUERY_ALL_PUBLICNOTICE;
		PlayerID queryPlayerID;
	}GFQueryAllPublicNotice;
	MSG_SIZE_GUARD(GFQueryAllPublicNotice);

	typedef struct StrMGShowTmpNoticeByPosition
	{
		static const unsigned char byPkgType =M_G;
		static const unsigned short wCmd = M_G_SHOW_TEMPNOTICE_BYPOSITION;
		PlayerID lNotiPlayerID;
		char  isbroadcast;
		GCShowTempNoticeByPosition showtmpnotice;
	}MGShowTmpNoticeByPosition;
	MSG_SIZE_GUARD(MGShowTmpNoticeByPosition);

	typedef struct StrGMAddNewPublicNotice
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_ADD_PUBLICNOTICE;
		PublicNotice newnotice;
	}GMAddNewPublicNotice;
	MSG_SIZE_GUARD(GMAddNewPublicNotice);

	typedef struct StrGMModifyPublicNotice
	{
		static const unsigned char byPkgType =G_M;
		static const unsigned short wCmd = G_M_MODIFY_PUBLICNOTICE;
		PublicNotice modifynotice;
	}GMModifyPublicNotice;
	MSG_SIZE_GUARD(GMModifyPublicNotice);

	typedef struct StrGMRemovePublicNotice
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd= G_M_REMOVE_PUBLICNOTICE;
		unsigned int msgid;
	}GMRemovePublicNotice;
	MSG_SIZE_GUARD(GMRemovePublicNotice);

	typedef struct StrGMAddMarqueueNotice
	{
		static const unsigned char byPkgType =G_M;
		static const unsigned short wCmd = G_M_ADD_MARQUEUE_NOTICE;
		MarqueueNotice marqueue;
	}GMAddMarqueueNotice;
	MSG_SIZE_GUARD(GMAddMarqueueNotice);

	typedef struct StrGMBroadCastTempNotice
	{
		static const unsigned char byPkgType =G_M;
		static const unsigned short wCmd = G_M_BROADCAST_TMPNOTICE;
		MGShowTmpNoticeByPosition tempnotice;
	}GMBroadCastTempNotice;
	MSG_SIZE_GUARD(GMBroadCastTempNotice);

	typedef struct StrFGRecvPublicNotice
	{
		static const unsigned char byPkgType =F_G;
		static const unsigned short wCmd = F_G_RECV_PUBLICNOTICE;
		PlayerID queryPlayerID;
		GCRecvPublicNotices srvmsg;
	}FGRecvPublicNotice;
	MSG_SIZE_GUARD(FGRecvPublicNotice);

	typedef struct StrFGModifyPublicNotice
	{
		static const unsigned char byPkgType=F_G;
		static const unsigned short wCmd =  F_G_MODIFY_PUBLICNOTICE;
		GMModifyPublicNotice srvmsg;
	}FGModifyPublicNotice;
	MSG_SIZE_GUARD(FGModifyPublicNotice);

	typedef struct StrFGAddNewPublicNotice
	{
		static const unsigned char byPkgType=F_G;
		static const unsigned short wCmd = F_G_ADD_NEW_PUBLICNOTICE;
		GMAddNewPublicNotice srvmsg;
	}FGAddNewPublicNotice;
	MSG_SIZE_GUARD(FGAddNewPublicNotice);

	typedef struct StrFGRemovePublicNotice
	{
		static const unsigned char byPkgType=F_G;
		static const unsigned short wCmd = F_G_REMOVE_PUBLICNOTICE;
		unsigned int msgid;
	}FGRemovePublicNotice;
	MSG_SIZE_GUARD(FGRemovePublicNotice);

	
	typedef struct StrMGPublicNoticeModify
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PUBLICNOTICE_MODIFY;
		PlayerID lNotiPlayerID;
		GCGMModifyPublicNotice modifynotice;
	}MGPublicNoticeModify;
	MSG_SIZE_GUARD(MGPublicNoticeModify);

	typedef struct StrMGPublicNoticeAdd
	{
		static const unsigned char byPkgType =M_G;
		static const unsigned short wCmd = M_G_PUBLICNOTICE_ADD;
		PlayerID lNotiPlayerID;
		GCGMAddNewPublicNotice addnotice;
	}MGPublicNoticeAdd;
	MSG_SIZE_GUARD(MGPublicNoticeAdd);

	typedef struct StrMGPublicNoticeRemove
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PUBLICNOTICE_REMOVE;
		PlayerID lNotiPlayerID;
		GCGMRemovePublicNotice removenotice;
	}MGPublicNoticeRemove;
	MSG_SIZE_GUARD(MGPublicNoticeRemove);

	typedef struct StrMGMarqueueNoticeAdd
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_MARQUEUE_MSG_ADD;
		PlayerID lNotiPlayerID;
		GCRecvNewMarqueueNotice marqueuenotice;
	}MGMarqueueNoticeAdd;
	MSG_SIZE_GUARD(MGMarqueueNoticeAdd);

	typedef struct StrMGSendParamNotice
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PARAM_MSG_ADD;
		PlayerID lNotiPlayerID;
		char isgloabl;
		GCRecvParamMsgNotice parammsgnotice;
	}MGSendParamNotice;
	MSG_SIZE_GUARD(MGSendParamNotice);

	typedef struct StrGMBroadCastParamNotice
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_BROADCAST_PARAMNOTICE;
		MGSendParamNotice paramntoice;
	}GMBroadCastParamNotice;
	MSG_SIZE_GUARD(GMBroadCastParamNotice);

	typedef struct StrMGBlockQuadStateChange
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_BLOCK_QUAD_STATE_CHANGE;
		PlayerID lNotiPlayerID;
		GCBlockQuadStateChange stateChange;
	}MGBlockQuadStateChange;
	MSG_SIZE_GUARD(MGBlockQuadStateChange);

	typedef struct StrMGStartSelectRaceMaster
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_START_SELECT_RACE;
		PlayerID lNotiPlayerID;
	}MGStartSelectRaceMaster;
	MSG_SIZE_GUARD(MGStartSelectRaceMaster);

	typedef struct StrGFSelectRaceMaster
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_SELECT_RACEMASTER;
	}GFSelectRaceMaster;
	MSG_SIZE_GUARD(GFSelectRaceMaster);

	typedef struct StrGMDiamondToPrestigeNtf
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_DIAMOND_TO_PRESTIGE_NOT;
		PlayerID notiPlayerID;
		unsigned int prestige;
	}GMDiamondToPrestigeNtf;
	MSG_SIZE_GUARD(GMDiamondToPrestigeNtf);

	typedef struct StrGMGMAccountNtf
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_GMACCOUNT_NTF;
		PlayerID notiPlayerID;
	}GMGMAccountNtf;
	MSG_SIZE_GUARD(GMGMAccountNtf);

	typedef struct StrGMForwardUnionCmd
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_FORWARD_UNION_CMD;
		PlayerID lNotiPlayerID;
		BYTE cmdID;
		union
		{
			CGCreateUnionRequest createUnion;
			CGAdvanceUnionLevelReq advanceUnionLevel;
			CGIssueUnionTaskReq issueTask;
			CGLearnUnionSkillReq learnUnionSkill;
			CGModifyUnionBadgeReq modifyUnionBadge;
		};
	}GMForwardUnionCmd;
	MSG_SIZE_GUARD(GMForwardUnionCmd);

	typedef struct StrMGForwardUnionCmdRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_FORWARD_UNION_CMD_RST;
		PlayerID lNotiPlayerID;
		bool flag;
		BYTE cmdID;
		union
		{
			CGCreateUnionRequest createUnion;
			CGAdvanceUnionLevelReq advanceUnionLevel;
			CGIssueUnionTaskReq issueTask;
			CGLearnUnionSkillReq learnUnionSkill;
			CGModifyUnionBadgeReq modifyUnionBadge;
		};
	}MGForwardUnionCmdRst;
	MSG_SIZE_GUARD(MGForwardUnionCmdRst);

	typedef struct StrMGReturnSelRole 
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_RETURN_SEL_ROLE;
		PlayerID lNotiPlayerID;
	}MGReturnSelRole;
	MSG_SIZE_GUARD(MGReturnSelRole);

	typedef struct StrGMCreateUnionErrNtf
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_CREATE_UNION_ERR_NTF;
		PlayerID lNotiPlayerID;
		char recvPlayerName[MAX_NAME_SIZE];
	}GMCreateUnionErrNtf;
	MSG_SIZE_GUARD(GMCreateUnionErrNtf);

	typedef struct StrMGUnionCostArg 
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_UNION_COST_ARG;
		PlayerID lNotiPlayerID;
		UnionCostArg arg;
	}MGUnionCostArg;
	MSG_SIZE_GUARD(MGUnionCostArg);

	typedef struct strMGReportPWInfos//报告伪线信息
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_REPORT_PW_INFOS;
		PseudowireArg_i pwList[10];
		unsigned char count;
	}MGReportPWInfos;
	MSG_SIZE_GUARD(MGReportPWInfos);

	typedef struct strMGReportPWMapInfos//报告伪线地图信息
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_REPORT_PW_MAP_INFOS;
		PseudowireMapArg_i pwMapList[10];
		unsigned char count;
	}MGReportPWMapInfos;
	MSG_SIZE_GUARD(MGReportPWMapInfos);

	typedef struct strMGModifyPWStateForward//GM修改伪线状态
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MODIFY_PW_STATE_FORWARD;
		PlayerID lNotiPlayerID;
		unsigned short pwId;
		unsigned char newState;
	}MGModifyPWStateForward;
	MSG_SIZE_GUARD(MGModifyPWStateForward);

	typedef struct strMGModifyPWMapStateForward//GM修改伪线地图状态
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MODIFY_PW_MAP_STATE_FORWARD;
		PlayerID lNotiPlayerID;
		unsigned short pwId;
		unsigned short pwMapId;
		unsigned short newState;
	}MGModifyPWMapStateForward;
	MSG_SIZE_GUARD(MGModifyPWMapStateForward);

	typedef struct strMGShiftPWReqForward//GM切线请求
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_SHIFT_PW_REQ_FORWARD;
		PlayerID lNotiPlayerID;
		unsigned short targetPWId;
	}MGShiftPWReqForward;
	MSG_SIZE_GUARD(MGShiftPWReqForward);

	typedef struct strMGKickOffPWForward//GM强制切线
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_KICKOFF_PW_REQ_FORWARD;
		PlayerID lNotiPlayerID;
		unsigned short targetPWId;
	}MGKickOffPWForward;
	MSG_SIZE_GUARD(MGKickOffPWForward);

	typedef struct strGMPlayerStartNewAuction
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_START_NEWAUCTION;
		PlayerID  lNotiPlayerID;
		unsigned int pkgIndex;
		unsigned int origiprice;
		unsigned int fixprice;
		unsigned char expiretype;
	}GMPlayerStartNewAuction;
	MSG_SIZE_GUARD(GMPlayerStartNewAuction);

	typedef struct StrGMPlayerBidItem
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLAYER_BID_ITEM;
		PlayerID  lNotiPlayerID;
		unsigned  int  auctionUID;
		unsigned  int  lastprice;
		unsigned  int  newprice;
	}GMPlayerBidItem;
	MSG_SIZE_GUARD(GMPlayerBidItem);

	typedef struct StrGMSubPlayerMoney
	{
		static const unsigned char byPkgType =G_M;
		static const unsigned short wCmd = G_M_PLAYER_SUBMONEY;
		PlayerID playerID;
		unsigned int subMoney;
	}GMSubPlayerMoney;
	MSG_SIZE_GUARD(GMSubPlayerMoney);

    //#define G_M_USE_MONEY_TYPE               0x1601 //使用钱币类型；
	typedef struct StrGMUseMoneyType
	{
		static const unsigned char byPkgType =G_M;
		static const unsigned short wCmd = G_M_USE_MONEY_TYPE;
		PlayerID     playerID;
		BYTE         moneyType;                                  	//使用的钱币类型(0:银币,1:金币)
	}GMUseMoneyType;
	MSG_SIZE_GUARD(GMUseMoneyType);

	//#define G_M_HONOR_MASK                   0x1f02 //玩家选择显示的称号；
	typedef struct StrGMHonorMask
	{
		static const unsigned char byPkgType =G_M;
		static const unsigned short wCmd = G_M_HONOR_MASK;
		PlayerID     playerID;
		EHonorMask   honorMask;                                  	//选择显示的称号(EHonorMask)
	}GMHonorMask;
	MSG_SIZE_GUARD(GMHonorMask);

	//#define M_G_USE_MONEY_TYPE                       0x5286 //使用的钱币类型(0:银币,1:金币)
	typedef struct StrMGUseMoneyType
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_USE_MONEY_TYPE;
		PlayerID     playerID;
		BYTE         moneyType;                                  	//使用的钱币类型(0:银币,1:金币)
	}MGUseMoneyType;
	MSG_SIZE_GUARD(MGUseMoneyType);

	struct AppBroInfo
	{
		enum APPBRO_TYPE{
			APPBRO_MAP=0,//地图内广播
			APPBRO_UNION //战盟内广播
		};
		APPBRO_TYPE    broType;//广播类型；
		union {
			unsigned short mapID;//APPBRO_MAP时的地图号；
			unsigned int   unionID;//APPBRO_UNION时的战盟号；
		};
	};
	//#define M_G_APP_BROCAST                          0x5287 //M-->G，由G最终发送的应用层广播消息
	typedef struct StrMGAppBroCast
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_APP_BROCAST;
		unsigned short           broMsgCmd;//待广播消息的消息编号;
		AppBroInfo               broMsgInfo;//应用广播相关信息；
		unsigned char            toBroMsg[128];                                  	//真正的待广播消息
	}MGAppBroCast;
	MSG_SIZE_GUARD(MGAppBroCast);

	typedef struct StrMGInviteCallMember
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_INVITE_CALL_MEMBER;
		PlayerID			lNotiPlayerID;
		GCInviteCallMember	inviteCallMember;
	}MGInviteCallMember;
	MSG_SIZE_GUARD(MGInviteCallMember);

	//新添加的拍卖道具
	struct NewAuctionItem
	{
		char itemName[MAX_NAME_SIZE];//道具的名称
		char ownerName[MAX_NAME_SIZE];//拍卖玩家的姓名
		char itemClass[MAX_NAME_SIZE];//道具的职业
		char level;//道具等级
		char masslevel;//材质等级
		char equipPos;//装备位置
		int  fixprice;//一口价
		int  origiAuctionprice;//开始的拍卖价钱
		int  expiretime;//当前的拍卖结束的时间
		ItemInfo_i iteminfo;//当前拍卖物品的道具信息
		unsigned int playerUID;//本次竞排的玩家唯一编号
	};

	typedef struct StrGFPlayerStartNewAuction
	{
		static const unsigned char  byPkgType = G_F;
		static const unsigned short wCmd = G_F_PLAYER_START_NEWAUCTION;
		PlayerID playerID;
		NewAuctionItem newAuctionItem;
	}GFPlayerStartNewAuction;
	MSG_SIZE_GUARD(GFPlayerStartNewAuction);

	typedef struct StrMGPlayerStartNewAuctionNext
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_START_NEWAUCTION;
		PlayerID lNotiPlayerID;
		GFPlayerStartNewAuction startNewAuctionItem;
	}MGPlayerStartNewAuctionNext;
	MSG_SIZE_GUARD(MGPlayerStartNewAuctionNext);

	typedef struct StrMGPlayerStartNewAuctionRst
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_NEWAUCTION_RST;
		PlayerID lNotiPlayerID;
		GCBeginNewAuctionRst newAuctionRst;
	}MGPlayerStartNewAuctionRst;
	MSG_SIZE_GUARD(MGPlayerStartNewAuctionRst);

	typedef struct StrGFPlayerBidItemSection1
	{
		static const unsigned char  byPkgType = G_F;
		static const unsigned short wCmd = G_F_PLAYER_BID_ITEM_SECTION1;
		PlayerID playerID;
		unsigned int playerUID;
		unsigned int auctionUID;
		unsigned int newprice;
	}GFPlayerBidItemSection1;
	MSG_SIZE_GUARD(GFPlayerBidItemSection1);

	typedef struct StrGFPlayerBidItemSection2
	{
		static const unsigned char  byPkgType = G_F;
		static const unsigned short wCmd = G_F_PLAYER_BID_ITEM_SECTION2;
		PlayerID     playerID; 
		char         bidPlayerName[MAX_NAME_SIZE];
		unsigned int playerUID;
		unsigned int auctionUID;
		unsigned int newprice;
		unsigned int lastprice;
	}GFPlayerBidItemSection2;
	MSG_SIZE_GUARD(GFPlayerBidItemSection2);

	typedef struct StrMGPlayerBidItemNext
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_BID_ITEM_NEXT;
		PlayerID     lNotiPlayerID;
		GFPlayerBidItemSection2 bidItemSection2;
	}MGPlayerBidItemNext;
	MSG_SIZE_GUARD(MGPlayerBidItemNext);

	typedef struct StrMGPlayerBidItemErr
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_BID_ITEM_ERROR;
		PlayerID lNotiPlayerID;
		unsigned int auctionUID;
		int rstNo;
	}MGPlayerBidItemErr;
	MSG_SIZE_GUARD(MGPlayerBidItemErr);

	typedef struct StrMGShowAuctionPlate
	{
		static const unsigned char byPkgType = M_G;
		static const unsigned short wCmd = M_G_PLAYER_SHOW_AUCTIONPLATE;
		PlayerID lNotiPlayerID;
	}MGShowAuctionPlate;
	MSG_SIZE_GUARD(MGShowAuctionPlate);

	typedef struct StrFGBidItemRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_PLAYER_BIDITEM_RST;
		PlayerID bidPlayerID;
		unsigned int costprice;
		GCBidAuctionItemRst bidItemRst;
	}FGBidItemRst;
	MSG_SIZE_GUARD(FGBidItemRst);

	typedef struct StrGFPlayerCancelAuction
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_PLAYER_CANCEL_AUCTION;
		PlayerID playerID;
		unsigned int playerUID;
		unsigned int auctionUID;
	}GFPlayerCancelAuction;
	MSG_SIZE_GUARD(GFPlayerCancelAuction);

	typedef struct StrFGCancelAuctionRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_CANCELAUCTION_RST;
		PlayerID cancePlayerID;
		GCCancelMyAuctionRst cancelRst;
	}FGCancelAuctionRst;
	MSG_SIZE_GUARD(FGCancelAuctionRst);

	//搜索排行榜条件
	typedef struct StrGFQueryAuctionCondition
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_PLAYER_QUERY_AUCTION;
		PlayerID queryPlayerID;
		char  selectmask;//搜索条件标志位 
		char  itemname[MAX_NAME_SIZE];//道具名称
		short level1;//搜索的等级开始范围
		short level2;//搜索的等级结束范围
		char  orderby;//是否依据等级排序 如果为0，则依据材质排序 如果为非0 则依据等级排序
		char  levelorder;//从大到小 0 从小到大 1
		char  massorder;//从大到小 0 从小到大 1
		char  masslevel;//材质的等级
		char  itemclass;//道具的职业性质
		char  itempos;//道具的装备位置
		char  pageindex;//所查询的页数
	}GFQueryAuctionCondition;
	MSG_SIZE_GUARD(GFQueryAuctionCondition);

	typedef struct StrFGQueryAuctionConditionRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_QUERY_AUCTION_BYCONDITION_RST;
		PlayerID queryPlayerID;
		GCQueryAuctionConditionRst queryRst;
	}FGQueryAuctionConditionRst;
	MSG_SIZE_GUARD(FGQueryAuctionConditionRst);


	typedef struct StrGFQueryMyAuction
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd = G_F_PLAERY_QUERY_SELF_AUCTION;
		PlayerID queryPlayerID;
		unsigned int playerUID;
		char     pageIndex;
	}GFQueryMyAuction;
	MSG_SIZE_GUARD(GFQueryMyAuction);

	typedef struct StrFGQueryMyAuctionRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_QUERY_MYAUCTION_RST;
		PlayerID queryPlayerID;
		GCQueryMyAuctionsRst auctionRst;
	}FGQueryMyAuctionRst;
	MSG_SIZE_GUARD(FGQueryMyAuctionRst);


	typedef struct StrGFQueryMyBid
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd =  G_F_PLAYER_QUERY_SELF_BID;
		PlayerID queryPlayerID;
		unsigned int playerUID;
		char pageIndex;
	}GFQueryMyBid;
	MSG_SIZE_GUARD(GFQueryMyBid);

	//#define  G_F_RANK_CACHE_INFO            0xd1ea //影响排行榜的新缓存信息；
	typedef struct StrGFRankCacheInfo
	{
		static const unsigned char byPkgType = G_F;
		static const unsigned short wCmd =  G_F_RANK_CACHE_INFO;
		ERankType  rankType;//新缓存信息对应的排行榜类型
		RankCacheRcd cacheRcd;//新缓存信息;
	}GFRankCacheInfo;
	MSG_SIZE_GUARD(GFRankCacheInfo);

	typedef struct StrFGQueryMyBidRst
	{
		static const unsigned char byPkgType = F_G;
		static const unsigned short wCmd = F_G_QUERY_MYBID_RST;
		PlayerID queryPlayerID;
		GCQueryMyBidStateRst queryBidState;
	}FGQueryMyBidRst;
	MSG_SIZE_GUARD(FGQueryMyBidRst);

	typedef struct StrFGBidItemSection1
	{
		static const unsigned char byPkgType=F_G;
		static const unsigned short wCmd = F_G_PLAYER_BIDITEM_SECTION1;
		PlayerID playerID;
		int      newprice;//最新的价格
		int      lastPrice;//上一次竞拍的价格
		int      auctionUID;//竞拍的物品编号
	}FGBidItemSection1;
	MSG_SIZE_GUARD(FGBidItemSection1);

	//#define  F_G_BIAS_INFO               0xd124 //排行榜阈值信息
	typedef struct StrFGBiasInfo
	{
		static const unsigned char byPkgType=F_G;
		static const unsigned short wCmd = F_G_BIAS_INFO;
		unsigned int notiMapSrvID;//欲通知的mapsrvID号；
		int          arrBias[RANK_TYPE_NUM];//各阈值数组；
		unsigned int arrRankSeq[RANK_TYPE_NUM];//各排行榜序号(用于重新开始排行榜相关数据计算而又不必在重刷那一刻清所有玩家DB)
	}FGBiasInfo;
	MSG_SIZE_GUARD(FGBiasInfo);

	//#define  F_G_HONOR_PLAYER            0xd125 //称号拥有者信息
	typedef struct StrFGHonorPlayer
	{
		static const unsigned char byPkgType=F_G;
		static const unsigned short wCmd = F_G_HONOR_PLAYER;
		GCHonorPlayer honorPlayer;
	}FGHonorPlayer;
	MSG_SIZE_GUARD(FGHonorPlayer);

	typedef struct StrGMManualUpgradeReq
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_MANUAL_UPGRADE_REQ;
		PlayerID lNotiPlayerID;
		CGManualUpgradeReq req;
	}GMManualUpgradeReq;
	MSG_SIZE_GUARD(GMManualUpgradeReq);

	typedef struct StrGMAllocatePointReq
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_ALLOCATE_POINT_REQ;
		PlayerID lNotiPlayerID;
		CGAllocatePointReq req;
	}GMAllocatePointReq;
	MSG_SIZE_GUARD(GMAllocatePointReq);

	typedef struct StrGMModifyEffectSkillID
	{
		static const unsigned char byPkgType = G_M;
		static const unsigned short wCmd = G_M_MOD_EFFECTIVE_SKILL_ID;
		PlayerID lNotiPlayerID;
		CGModifyEffectiveSkillID req;
	}GMModifyEffectSkillID;
	MSG_SIZE_GUARD(GMModifyEffectSkillID);

	typedef struct StrMGModifyEffectSkillIDRst
	{
		static const unsigned char  byPkgType = M_G;
		static const unsigned short wCmd = M_G_MOD_EFFECTIVE_SKILLID_RST;
		PlayerID lNotiPlayerID;
		GCModifyEffectiveSkillIDRst rst;
	}MGModifyEffectSkillIDRst;
	MSG_SIZE_GUARD(MGModifyEffectSkillIDRst);

	typedef struct StrGMNotifyFriendGain
	{
		static const unsigned char byPkgType = G_M;
		static const USHORT	wCmd = G_M_FRIEND_GAIN_NTF;

		PlayerID 	lNotiPlayerID;
		bool        inTeam;
		UINT		expOrNot;                                	//组队情况下为攻防加成%，非组队情况下为经验
	}GMNotifyFriendGain;
	MSG_SIZE_GUARD(GMNotifyFriendGain);

	typedef struct StrGMQueryExpNeededWhenLvlUpRequest
	{
		static const unsigned char byPkgType = G_M;
		static const USHORT	wCmd = G_M_QUERY_EXP_OF_LEVEL_UP;
	}GMQueryExpNeededWhenLvlUpRequest;
	MSG_SIZE_GUARD(GMQueryExpNeededWhenLvlUpRequest);

	typedef struct StrMGAddFriendlyDegreeReq
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT	wCmd = M_G_ADD_FRIEND_DEGREE_REQ;

		PlayerID  lNotiPlayerID;
		UINT      friendUiid;                         	//好友的uiid
		UINT      degreeAdded;                        	//增加的友好度
	}MGAddFriendlyDegreeReq;
	MSG_SIZE_GUARD(MGAddFriendlyDegreeReq);

	typedef struct StrMGAddTweetNtf
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT	wCmd = M_G_ADD_TWEET_NTF;

		PlayerID  lNotiPlayerID;
		Tweet     tweet;                              	//
	}MGAddTweetNtf;
	MSG_SIZE_GUARD(MGAddTweetNtf);

	typedef struct StrMGReportFriendNonTeamGain
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT	wCmd = M_G_REPORT_FRIEND_NON_TEAM_GAIN;

		PlayerID  lNotiPlayerID;
		CHAR      isBoss;                             	//
		UINT      exp;                                	//当isBoss为false时，exp为杀死一个怪时获得的经验，否则，exp为0
	}MGReportFriendNonTeamGain;
	MSG_SIZE_GUARD(MGReportFriendNonTeamGain);

	typedef struct StrMGQueryExpNeededWhenLvlUpResult
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT	wCmd = M_G_QUERY_EXP_OF_LEVEL_UP_REQ;

		UINT      career;                             	//职业
		USHORT    lvlFirst;                           	//起始等级，下面的lvlLast表示结束等级，如1-20级，21-90级
		USHORT    lvlLast;                            	//
		UINT      lstLen;                             	//
		UINT      lst[90];                              //
	}MGQueryExpNeededWhenLvlUpResult;
	MSG_SIZE_GUARD(MGQueryExpNeededWhenLvlUpResult);

	typedef struct StrMGQueryOffLinePlayerInfoReq
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT	wCmd = M_G_OFF_LINE_PLAYER_INFO_REQ;
		PlayerID 	lNotiPlayerID;
		char		targetPlayerName[MAX_NAME_SIZE];
	}MGQueryOffLinePlayerInfoReq;
	MSG_SIZE_GUARD(MGQueryOffLinePlayerInfoReq);

	//#define M_G_RANK_CACHE_INFO                        0x528d //影响排行榜的新缓存信息；
	typedef struct StrMGRankCacheInfo
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT	wCmd = M_G_RANK_CACHE_INFO;
		ERankType  rankType;//新缓存信息对应的排行榜类型
		RankCacheRcd cacheRcd;//新缓存信息;
	}MGRankCacheInfo;
	MSG_SIZE_GUARD(MGRankCacheInfo);

	//#define M_G_MAPSRV_READY                         0x528e //mapsrv准备好(目前是在读到functionsrv阈值信息时向gatesrv发)
	typedef struct StrMGMapSrvReady
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT	wCmd = M_G_MAPSRV_READY;
	}MGMapSrvReady;
	MSG_SIZE_GUARD(MGMapSrvReady);

	typedef struct StrGMQueryOffLinePlayerInfoRst
	{
		static const unsigned char byPkgType = G_M;
		static const USHORT	wCmd = G_M_OFFLINE_PLAYER_INFO_RST;

		PlayerID 	lNotiPlayerID;
		char		targetPlayerName[MAX_NAME_SIZE];
		bool        isExist;
		PlayerInfoLogin rst;
	}GMQueryOffLinePlayerInfoRst;
	MSG_SIZE_GUARD(GMQueryOffLinePlayerInfoRst);

	//#define G_M_BIAS_INFO                    0x1f06 //各排行榜阈值信息；
	typedef struct StrGMBiasInfo
	{
		static const unsigned char byPkgType = G_M;
		static const USHORT	wCmd = G_M_BIAS_INFO;
		int          arrBias[RANK_TYPE_NUM];//各阈值数组；
		unsigned int arrRankSeq[RANK_TYPE_NUM];//各排行榜序号(用于重新开始排行榜相关数据计算而又不必在重刷那一刻清所有玩家DB)
	}GMBiasInfo;
	MSG_SIZE_GUARD(GMBiasInfo);

	typedef struct StrGDQueryOffLinePlayerInfoReq
	{
		static const unsigned char byPkgType = G_D;
		static const USHORT	wCmd = G_D_QUERY_OFFLINE_PLAYER_INFO_REQ;

		PlayerID 	lNotiPlayerID;
		CGQueryOffLinePlayerInfo req;
	}GDQueryOffLinePlayerInfoReq;
	MSG_SIZE_GUARD(GDQueryOffLinePlayerInfoReq);

	typedef struct StrDGQueryOffLinePlayerInfoRst
	{
		static const unsigned char byPkgType = D_G;
		static const USHORT	wCmd = D_G_QUREY_OFFLINE_PLAYER_INFO_RST;

		PlayerID 	lNotiPlayerID;
		GCQueryOffLinePlayerInfoRst rst;
	}DGQueryOffLinePlayerInfoRst;
	MSG_SIZE_GUARD(DGQueryOffLinePlayerInfoRst);

	typedef struct StrMGStartCast 
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT wCmd = M_G_START_CAST;

		PlayerID	lNotiPlayerID;
		GCStartCast req;
	}MGStartCast;
	MSG_SIZE_GUARD(MGStartCast);

	typedef struct StrGMStartCast 
	{
		static const unsigned char byPkgType = G_M;
		static const USHORT wCmd = G_M_START_CAST;

		PlayerID	lNotiPlayerID;
		CGStartCast req;
	}GMStartCast;
	MSG_SIZE_GUARD(GMStartCast);

	typedef struct StrMGStopCast 
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT wCmd = M_G_STOP_CAST;

		PlayerID	lNotiPlayerID;
	}MGStopCast;
	MSG_SIZE_GUARD(MGStopCast);

	typedef struct StrGMStopCast 
	{
		static const unsigned char byPkgType = G_M;
		static const USHORT wCmd = G_M_STOP_CAST;

		PlayerID	lNotiPlayerID;
	}GMStopCast;
	MSG_SIZE_GUARD(GMStopCast);

	typedef struct StrMGRunFail 
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT wCmd = M_G_RUN_FAIL;

		PlayerID	lNotiPlayerID;
		GCRunFail	rst;
	}MGRunFail;
	MSG_SIZE_GUARD(MGRunFail);

	typedef struct StrMGFightPetPrivate
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT wCmd = M_G_FIGHTPET_PRIVATE;

		PlayerID	lNotiPlayerID;
	}MGFightPetPrivate;
	MSG_SIZE_GUARD(MGFightPetPrivate);

	typedef struct StrMGFightPetPublic
	{
		static const unsigned char byPkgType = M_G;
		static const USHORT wCmd = M_G_FIGHTPET_PUBLIC;

		PlayerID	   lNotiPlayerID;
		PlayerID       ownerPlayer;//宠物主人ID；
		unsigned int   selfID;//宠物自身ID；
		unsigned int   typeID;//宠物类别(外观)
		unsigned char  petLev;//宠物等级
		unsigned char  petStage;//宠物阶层
		unsigned short curPosX;//当前位置X
		unsigned short curPosY;//当前位置Y
		unsigned short tgtPosX;//目标X
		unsigned short tgtPosY;//目标Y
		unsigned short curSpeed;//当前速度0x5293
	}MGFightPetPublic;
	MSG_SIZE_GUARD(MGFightPetPublic);

#pragma pack(pop)
} //namespace MUX_PROTO

#ifdef WIN32
#pragma warning( pop )
#endif //WIN32



