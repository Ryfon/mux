﻿//#define MUX_CLI_INCLUDE //客户端定义此宏
#ifndef MUX_CLI_INCLUDE
#include "../aceall.h"
#endif //MUX_CLI_INCLUDE

#ifdef MUX_CLI_INCLUDE
	#include "memory.h"
	#define MY_WIN32
#else //MUX_CLI_INCLUDE
	#ifdef ACE_WIN32
	#define MY_WIN32
	#endif //ACE_WIN32
#endif //MUX_CLI_INCLUDE

#ifdef MUX_CLI_INCLUDE

#define DealPlayerPkgPre( OUTTYPE, outPtr, inputPkg, inputLen ) \
	OUTTYPE  tmpStruct;\
    OUTTYPE* outPtr = &tmpStruct;\
	if ( g_pMuxApp->IsSinglePlayer() )\
	{\
		outPtr = (OUTTYPE*)inputPkg;\
	}\
	else\
	{\
		memset( outPtr, 0, sizeof(OUTTYPE) );\
		size_t bufLen = sizeof(tmpStruct);\
		if ( bufLen < inputLen )\
		{\
			NiMessageBox("Message size error:"#OUTTYPE, "Error");\
			return;\
		}\
		int nDecodeLen = (int)m_cliprotocol.DeCode( OUTTYPE::wCmd, &tmpStruct, bufLen, inputPkg, inputLen );\
		if ( nDecodeLen < 0 || nDecodeLen != bufLen )\
		{\
			Debug_WriteLog( "DealPlayerPkgPre: NetMessage Error!"#OUTTYPE " \n");\
			return;\
		}\
	}
#endif //MUX_CLI_INCLUDE

#ifdef MY_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "CliProtocol_T.h"
#ifdef MY_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

namespace MUX_PROTO
{
inline bool operator < ( const PlayerID& first,const PlayerID& sec )
{
	//return first.dwPID < sec.dwPID ? true : false;
	if (first.dwPID < sec.dwPID)
	{
		return true;
	}
	else if (first.dwPID == sec.dwPID)
	{
		return first.wGID < sec.wGID ? true : false;
	}
	else
	{
		return false;
	}
}

inline bool operator == ( const PlayerID& first,const PlayerID &sec )
{
	return ( ( first.wGID == sec.wGID ) && ( first.dwPID == sec.dwPID ) );
}

inline bool operator != ( const PlayerID& first,const PlayerID &sec )
{
	return ( ( first.wGID != sec.wGID ) || ( first.dwPID != sec.dwPID ) );
}

/*inline void operator+=(EquipAdditonInfo& first, const EquipAdditonInfo &sec)
{
	first.AddHP += sec.AddHP;
	first.AddMP += sec.AddMP;
	first.AddStr += sec.AddStr;
	first.AddInt += sec.AddInt;
	first.AddAgi += sec.AddAgi;
	first.AddPhysicsHit += sec.AddPhysicsHit;
	first.AddMagicHit += sec.AddMagicHit;
	first.AddPhysicsHitOdds += sec.AddPhysicsHitOdds;
	first.AddMagicHitOdds += sec.AddMagicHitOdds;
	first.AddPhysicsNum += sec.AddPhysicsNum;
	first.AddMagicNum += sec.AddMagicNum;
	first.AddPhysicsAttack += sec.AddPhysicsAttack;
	first.AddMagicAttack += sec.AddMagicAttack;
	first.AddDecPhyNumHarm += sec.AddDecPhyNumHarm;
	first.AddDecMagNumHarm += sec.AddDecMagNumHarm;
	first.AddPhysicsNumAffix += sec.AddPhysicsNumAffix;
	first.AddMagicNumAffix += sec.AddMagicNumAffix;
	first.AddPhysicsDef += sec.AddPhysicsDef;
	first.AddMagicDef += sec.AddMagicDef;

	return;
}*/

};//namespace MUX_PROTO



