﻿#include "MonitorProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

MUX_BG_Monitor::Monitor_Protocol::Monitor_Protocol()
{
	m_mapEnCodeFunc[G_X_MONITOR_RECORD     ]	=	&Monitor_Protocol::EnCode__Monitor_Record;
	m_mapDeCodeFunc[G_X_MONITOR_RECORD     ]	=	&Monitor_Protocol::DeCode__Monitor_Record;

	m_mapEnCodeFunc[MS_X_CONTROL           ]	=	&Monitor_Protocol::EnCode__Monitor_Control;
	m_mapDeCodeFunc[MS_X_CONTROL           ]	=	&Monitor_Protocol::DeCode__Monitor_Control;

	m_mapEnCodeFunc[MC_MS_SERVER_LIST_QUERY]	=	&Monitor_Protocol::EnCode__Server_List_Query;
	m_mapDeCodeFunc[MC_MS_SERVER_LIST_QUERY]	=	&Monitor_Protocol::DeCode__Server_List_Query;

	m_mapEnCodeFunc[MS_MC_SERVER_LIST      ]	=	&Monitor_Protocol::EnCode__Server_List;
	m_mapDeCodeFunc[MS_MC_SERVER_LIST      ]	=	&Monitor_Protocol::DeCode__Server_List;

	m_mapEnCodeFunc[MC_MS_SET_HOT_SPOT     ]	=	&Monitor_Protocol::EnCode__Hot_Spot_Set;
	m_mapDeCodeFunc[MC_MS_SET_HOT_SPOT     ]	=	&Monitor_Protocol::DeCode__Hot_Spot_Set;

	m_mapEnCodeFunc[MC_MS_RECORD_QUERY     ]	=	&Monitor_Protocol::EnCode__Record_Query;
	m_mapDeCodeFunc[MC_MS_RECORD_QUERY     ]	=	&Monitor_Protocol::DeCode__Record_Query;

	m_mapEnCodeFunc[MS_MC_MONITOR_RECORD   ]	=	&Monitor_Protocol::EnCode__Range_Record;
	m_mapDeCodeFunc[MS_MC_MONITOR_RECORD   ]	=	&Monitor_Protocol::DeCode__Range_Record;

}

MUX_BG_Monitor::Monitor_Protocol::~Monitor_Protocol()
{
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

MUX_BG_Monitor::EnCodeFunc	MUX_BG_Monitor::Monitor_Protocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

MUX_BG_Monitor::DeCodeFunc	MUX_BG_Monitor::Monitor_Protocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__Monitor_Record(void* pData)
{
	Monitor_Record* pkMonitor_Record = (Monitor_Record*)(pData);

	//EnCode target_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkMonitor_Record->target_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode target_ip
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkMonitor_Record->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode target_type
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkMonitor_Record->target_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode online_player_num
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkMonitor_Record->online_player_num), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode cpu_usage_percent
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkMonitor_Record->cpu_usage_percent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memory_usage
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkMonitor_Record->memory_usage), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode str_len
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkMonitor_Record->str_len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode date_time
	if(24 < pkMonitor_Record->str_len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMonitor_Record->str_len;
	if(!m_kPackage.Pack("CHAR", &(pkMonitor_Record->date_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__Monitor_Record(void* pData)
{
	Monitor_Record* pkMonitor_Record = (Monitor_Record*)(pData);

	//DeCode target_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkMonitor_Record->target_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode target_ip
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkMonitor_Record->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode target_type
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkMonitor_Record->target_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode online_player_num
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkMonitor_Record->online_player_num), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode cpu_usage_percent
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkMonitor_Record->cpu_usage_percent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memory_usage
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkMonitor_Record->memory_usage), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode str_len
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkMonitor_Record->str_len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode date_time
	if(24 < pkMonitor_Record->str_len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMonitor_Record->str_len;
	if(!m_kPackage.UnPack("CHAR", &(pkMonitor_Record->date_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__Monitor_Control(void* pData)
{
	Monitor_Control* pkMonitor_Control = (Monitor_Control*)(pData);

	//EnCode frequency
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkMonitor_Control->frequency), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__Monitor_Control(void* pData)
{
	Monitor_Control* pkMonitor_Control = (Monitor_Control*)(pData);

	//DeCode frequency
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkMonitor_Control->frequency), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__Server_List_Query(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__Server_List_Query(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__Hot_Spot_Set(void* pData)
{
	Hot_Spot_Set* pkHot_Spot_Set = (Hot_Spot_Set*)(pData);

	//EnCode target_ip
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkHot_Spot_Set->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__Hot_Spot_Set(void* pData)
{
	Hot_Spot_Set* pkHot_Spot_Set = (Hot_Spot_Set*)(pData);

	//DeCode target_ip
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkHot_Spot_Set->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__One_Server(void* pData)
{
	One_Server* pkOne_Server = (One_Server*)(pData);

	//EnCode target_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkOne_Server->target_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode target_ip
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkOne_Server->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode target_type
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkOne_Server->target_type), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__One_Server(void* pData)
{
	One_Server* pkOne_Server = (One_Server*)(pData);

	//DeCode target_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkOne_Server->target_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode target_ip
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkOne_Server->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode target_type
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkOne_Server->target_type), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__Server_List(void* pData)
{
	Server_List* pkServer_List = (Server_List*)(pData);

	//EnCode ServerNum
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkServer_List->ServerNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ServerVolum
	if(128 < pkServer_List->ServerNum)
	{
		return FAILEDRETCODE;
	}

	for(INT i = 0; i < pkServer_List->ServerNum; ++i)
	{
		if(EnCode__One_Server(&(pkServer_List->ServerVolum[i])) == -1)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__Server_List(void* pData)
{
	Server_List* pkServer_List = (Server_List*)(pData);

	//DeCode ServerNum
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkServer_List->ServerNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ServerVolum
	if(128 < pkServer_List->ServerNum)
	{
		return FAILEDRETCODE;
	}

	for(INT i = 0; i < pkServer_List->ServerNum; ++i)
	{
		if(DeCode__One_Server(&(pkServer_List->ServerVolum[i])) == -1)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__Record_Query(void* pData)
{
	Record_Query* pkRecord_Query = (Record_Query*)(pData);

	//EnCode str_len
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRecord_Query->str_len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode begin_time
	if(24 < pkRecord_Query->str_len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRecord_Query->str_len;
	if(!m_kPackage.Pack("CHAR", &(pkRecord_Query->begin_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode end_time
	if(24 < pkRecord_Query->str_len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRecord_Query->str_len;
	if(!m_kPackage.Pack("CHAR", &(pkRecord_Query->end_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode target_ip
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRecord_Query->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__Record_Query(void* pData)
{
	Record_Query* pkRecord_Query = (Record_Query*)(pData);

	//DeCode str_len
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRecord_Query->str_len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode begin_time
	if(24 < pkRecord_Query->str_len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRecord_Query->str_len;
	if(!m_kPackage.UnPack("CHAR", &(pkRecord_Query->begin_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode end_time
	if(24 < pkRecord_Query->str_len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRecord_Query->str_len;
	if(!m_kPackage.UnPack("CHAR", &(pkRecord_Query->end_time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode target_ip
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRecord_Query->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__Simple_Record(void* pData)
{
	Simple_Record* pkSimple_Record = (Simple_Record*)(pData);

	//EnCode online_player_num
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkSimple_Record->online_player_num), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode cpu_usage_percent
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkSimple_Record->cpu_usage_percent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memory_usage
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkSimple_Record->memory_usage), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode str_len
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkSimple_Record->str_len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode date_time
	if(24 < pkSimple_Record->str_len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSimple_Record->str_len;
	if(!m_kPackage.Pack("CHAR", &(pkSimple_Record->date_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__Simple_Record(void* pData)
{
	Simple_Record* pkSimple_Record = (Simple_Record*)(pData);

	//DeCode online_player_num
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkSimple_Record->online_player_num), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode cpu_usage_percent
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkSimple_Record->cpu_usage_percent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memory_usage
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkSimple_Record->memory_usage), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode str_len
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkSimple_Record->str_len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode date_time
	if(24 < pkSimple_Record->str_len)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSimple_Record->str_len;
	if(!m_kPackage.UnPack("CHAR", &(pkSimple_Record->date_time), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::EnCode__Range_Record(void* pData)
{
	Range_Record* pkRange_Record = (Range_Record*)(pData);

	//EnCode target_id
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRange_Record->target_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode target_ip
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRange_Record->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode target_type
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRange_Record->target_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RecordNum
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRange_Record->RecordNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RecordVolum
	if(24 < pkRange_Record->RecordNum)
	{
		return FAILEDRETCODE;
	}

	for(INT i = 0; i < pkRange_Record->RecordNum; ++i)
	{
		if(EnCode__Simple_Record(&(pkRange_Record->RecordVolum[i])) == -1)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_BG_Monitor::Monitor_Protocol::DeCode__Range_Record(void* pData)
{
	Range_Record* pkRange_Record = (Range_Record*)(pData);

	//DeCode target_id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRange_Record->target_id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode target_ip
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRange_Record->target_ip), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode target_type
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRange_Record->target_type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RecordNum
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRange_Record->RecordNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RecordVolum
	if(24 < pkRange_Record->RecordNum)
	{
		return FAILEDRETCODE;
	}

	for(INT i = 0; i < pkRange_Record->RecordNum; ++i)
	{
		if(DeCode__Simple_Record(&(pkRange_Record->RecordVolum[i])) == -1)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

