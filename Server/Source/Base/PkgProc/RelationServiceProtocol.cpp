﻿#include "RelationServiceProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

RelationServiceNS::RelationServiceProtocol::RelationServiceProtocol()
{
	m_mapEnCodeFunc[G_R_CreateTeam                       ]	=	&RelationServiceProtocol::EnCode__CreateTeamRequest;
	m_mapDeCodeFunc[G_R_CreateTeam                       ]	=	&RelationServiceProtocol::DeCode__CreateTeamRequest;

	m_mapEnCodeFunc[R_G_CreateTeamResult                 ]	=	&RelationServiceProtocol::EnCode__CreateTeamResult;
	m_mapDeCodeFunc[R_G_CreateTeamResult                 ]	=	&RelationServiceProtocol::DeCode__CreateTeamResult;

	m_mapEnCodeFunc[G_R_AddTeamMember                    ]	=	&RelationServiceProtocol::EnCode__AddTeamMemberRequest;
	m_mapDeCodeFunc[G_R_AddTeamMember                    ]	=	&RelationServiceProtocol::DeCode__AddTeamMemberRequest;

	m_mapEnCodeFunc[R_G_AddTeamMemberResult              ]	=	&RelationServiceProtocol::EnCode__AddTeamMemberResult;
	m_mapDeCodeFunc[R_G_AddTeamMemberResult              ]	=	&RelationServiceProtocol::DeCode__AddTeamMemberResult;

	m_mapEnCodeFunc[G_R_RemoveTeamMember                 ]	=	&RelationServiceProtocol::EnCode__RemoveTeamMemberRequest;
	m_mapDeCodeFunc[G_R_RemoveTeamMember                 ]	=	&RelationServiceProtocol::DeCode__RemoveTeamMemberRequest;

	m_mapEnCodeFunc[R_G_RemoveTeamMemberResult           ]	=	&RelationServiceProtocol::EnCode__RemoveTeamMemberResult;
	m_mapDeCodeFunc[R_G_RemoveTeamMemberResult           ]	=	&RelationServiceProtocol::DeCode__RemoveTeamMemberResult;

	m_mapEnCodeFunc[G_R_DestroyTeam                      ]	=	&RelationServiceProtocol::EnCode__DestroyTeamRequest;
	m_mapDeCodeFunc[G_R_DestroyTeam                      ]	=	&RelationServiceProtocol::DeCode__DestroyTeamRequest;

	m_mapEnCodeFunc[R_G_DestroyTeamResult                ]	=	&RelationServiceProtocol::EnCode__DestroyTeamResult;
	m_mapDeCodeFunc[R_G_DestroyTeamResult                ]	=	&RelationServiceProtocol::DeCode__DestroyTeamResult;

	m_mapEnCodeFunc[G_R_QueryTeam                        ]	=	&RelationServiceProtocol::EnCode__QueryTeamRequest;
	m_mapDeCodeFunc[G_R_QueryTeam                        ]	=	&RelationServiceProtocol::DeCode__QueryTeamRequest;

	m_mapEnCodeFunc[R_G_QueryTeamResult                  ]	=	&RelationServiceProtocol::EnCode__QueryTeamResult;
	m_mapDeCodeFunc[R_G_QueryTeamResult                  ]	=	&RelationServiceProtocol::DeCode__QueryTeamResult;

	m_mapEnCodeFunc[G_R_AddTeamAttri                     ]	=	&RelationServiceProtocol::EnCode__AddTeamAttriRequest;
	m_mapDeCodeFunc[G_R_AddTeamAttri                     ]	=	&RelationServiceProtocol::DeCode__AddTeamAttriRequest;

	m_mapEnCodeFunc[R_G_AddTeamAttriResult               ]	=	&RelationServiceProtocol::EnCode__AddTeamAttriResult;
	m_mapDeCodeFunc[R_G_AddTeamAttriResult               ]	=	&RelationServiceProtocol::DeCode__AddTeamAttriResult;

	m_mapEnCodeFunc[G_R_RemoveTeamAttri                  ]	=	&RelationServiceProtocol::EnCode__RemoveTeamAttriRequest;
	m_mapDeCodeFunc[G_R_RemoveTeamAttri                  ]	=	&RelationServiceProtocol::DeCode__RemoveTeamAttriRequest;

	m_mapEnCodeFunc[R_G_RemoveTeamAttriResult            ]	=	&RelationServiceProtocol::EnCode__RemoveTeamAttriResult;
	m_mapDeCodeFunc[R_G_RemoveTeamAttriResult            ]	=	&RelationServiceProtocol::DeCode__RemoveTeamAttriResult;

	m_mapEnCodeFunc[G_R_AddTeamMemberAttri               ]	=	&RelationServiceProtocol::EnCode__AddTeamMemberAttriRequest;
	m_mapDeCodeFunc[G_R_AddTeamMemberAttri               ]	=	&RelationServiceProtocol::DeCode__AddTeamMemberAttriRequest;

	m_mapEnCodeFunc[R_G_AddTeamMemberAttriResult         ]	=	&RelationServiceProtocol::EnCode__AddTeamMemberAttriResult;
	m_mapDeCodeFunc[R_G_AddTeamMemberAttriResult         ]	=	&RelationServiceProtocol::DeCode__AddTeamMemberAttriResult;

	m_mapEnCodeFunc[G_R_RemoveTeamMemberAttri            ]	=	&RelationServiceProtocol::EnCode__RemoveTeamMemberAttriRequest;
	m_mapDeCodeFunc[G_R_RemoveTeamMemberAttri            ]	=	&RelationServiceProtocol::DeCode__RemoveTeamMemberAttriRequest;

	m_mapEnCodeFunc[R_G_RemoveTeamMemberAttriResult      ]	=	&RelationServiceProtocol::EnCode__RemoveTeamMemberAttriResult;
	m_mapDeCodeFunc[R_G_RemoveTeamMemberAttriResult      ]	=	&RelationServiceProtocol::DeCode__RemoveTeamMemberAttriResult;

	m_mapEnCodeFunc[G_R_ReportGateId                     ]	=	&RelationServiceProtocol::EnCode__ReportGateSvrIdRequest;
	m_mapDeCodeFunc[G_R_ReportGateId                     ]	=	&RelationServiceProtocol::DeCode__ReportGateSvrIdRequest;

	m_mapEnCodeFunc[R_G_ReportGateIdResult               ]	=	&RelationServiceProtocol::EnCode__ReportGateSvrIdResult;
	m_mapDeCodeFunc[R_G_ReportGateIdResult               ]	=	&RelationServiceProtocol::DeCode__ReportGateSvrIdResult;

	m_mapEnCodeFunc[R_G_TeamDestroyedNotify              ]	=	&RelationServiceProtocol::EnCode__TeamDestroyedNotify;
	m_mapDeCodeFunc[R_G_TeamDestroyedNotify              ]	=	&RelationServiceProtocol::DeCode__TeamDestroyedNotify;

	m_mapEnCodeFunc[R_G_AddTeamMemberNotify              ]	=	&RelationServiceProtocol::EnCode__AddTeamMemberNotify;
	m_mapDeCodeFunc[R_G_AddTeamMemberNotify              ]	=	&RelationServiceProtocol::DeCode__AddTeamMemberNotify;

	m_mapEnCodeFunc[R_G_RemoveTeamMemberNotify           ]	=	&RelationServiceProtocol::EnCode__RemoveTeamMemberNotify;
	m_mapDeCodeFunc[R_G_RemoveTeamMemberNotify           ]	=	&RelationServiceProtocol::DeCode__RemoveTeamMemberNotify;

	m_mapEnCodeFunc[G_R_ModifyTeamCaptain                ]	=	&RelationServiceProtocol::EnCode__ModifyTeamCaptainRequest;
	m_mapDeCodeFunc[G_R_ModifyTeamCaptain                ]	=	&RelationServiceProtocol::DeCode__ModifyTeamCaptainRequest;

	m_mapEnCodeFunc[R_G_ModifyTeamCaptainResult          ]	=	&RelationServiceProtocol::EnCode__ModifyTeamCaptainResult;
	m_mapDeCodeFunc[R_G_ModifyTeamCaptainResult          ]	=	&RelationServiceProtocol::DeCode__ModifyTeamCaptainResult;

	m_mapEnCodeFunc[R_G_TeamCaptainModifiedNotify        ]	=	&RelationServiceProtocol::EnCode__ModifyTeamCaptainNotify;
	m_mapDeCodeFunc[R_G_TeamCaptainModifiedNotify        ]	=	&RelationServiceProtocol::DeCode__ModifyTeamCaptainNotify;

	m_mapEnCodeFunc[G_R_QueryTeamMember                  ]	=	&RelationServiceProtocol::EnCode__QueryTeamMemberRequest;
	m_mapDeCodeFunc[G_R_QueryTeamMember                  ]	=	&RelationServiceProtocol::DeCode__QueryTeamMemberRequest;

	m_mapEnCodeFunc[R_G_QueryTeamMemberResult            ]	=	&RelationServiceProtocol::EnCode__QueryTeamMemberResult;
	m_mapDeCodeFunc[R_G_QueryTeamMemberResult            ]	=	&RelationServiceProtocol::DeCode__QueryTeamMemberResult;

	m_mapEnCodeFunc[G_R_QueryTeamMembersCount            ]	=	&RelationServiceProtocol::EnCode__QueryTeamMembersCountRequest;
	m_mapDeCodeFunc[G_R_QueryTeamMembersCount            ]	=	&RelationServiceProtocol::DeCode__QueryTeamMembersCountRequest;

	m_mapEnCodeFunc[R_G_QueryTeamMembersCountResult      ]	=	&RelationServiceProtocol::EnCode__QueryTeamMembersCountResult;
	m_mapDeCodeFunc[R_G_QueryTeamMembersCountResult      ]	=	&RelationServiceProtocol::DeCode__QueryTeamMembersCountResult;

	m_mapEnCodeFunc[R_G_UpdateTeamMemberNotify           ]	=	&RelationServiceProtocol::EnCode__UpdateTeamMemberNotify;
	m_mapDeCodeFunc[R_G_UpdateTeamMemberNotify           ]	=	&RelationServiceProtocol::DeCode__UpdateTeamMemberNotify;

	m_mapEnCodeFunc[R_G_TeamCreatedNotify                ]	=	&RelationServiceProtocol::EnCode__TeamCreateedNotify;
	m_mapDeCodeFunc[R_G_TeamCreatedNotify                ]	=	&RelationServiceProtocol::DeCode__TeamCreateedNotify;

	m_mapEnCodeFunc[G_R_ModifyExpSharedMode              ]	=	&RelationServiceProtocol::EnCode__ModifyExpSharedModeRequest;
	m_mapDeCodeFunc[G_R_ModifyExpSharedMode              ]	=	&RelationServiceProtocol::DeCode__ModifyExpSharedModeRequest;

	m_mapEnCodeFunc[R_G_ModifyExpSharedModeResult        ]	=	&RelationServiceProtocol::EnCode__ModifyExpSharedModeResult;
	m_mapDeCodeFunc[R_G_ModifyExpSharedModeResult        ]	=	&RelationServiceProtocol::DeCode__ModifyExpSharedModeResult;

	m_mapEnCodeFunc[G_R_ModifyGoodSharedMode             ]	=	&RelationServiceProtocol::EnCode__ModifyGoodSharedModeRequest;
	m_mapDeCodeFunc[G_R_ModifyGoodSharedMode             ]	=	&RelationServiceProtocol::DeCode__ModifyGoodSharedModeRequest;

	m_mapEnCodeFunc[R_G_ModifyGoodSharedModeResult       ]	=	&RelationServiceProtocol::EnCode__ModifyGoodSharedModeResult;
	m_mapDeCodeFunc[R_G_ModifyGoodSharedModeResult       ]	=	&RelationServiceProtocol::DeCode__ModifyGoodSharedModeResult;

	m_mapEnCodeFunc[R_G_ModifyExpSharedModeNotify        ]	=	&RelationServiceProtocol::EnCode__ModifyExpSharedModeNotify;
	m_mapDeCodeFunc[R_G_ModifyExpSharedModeNotify        ]	=	&RelationServiceProtocol::DeCode__ModifyExpSharedModeNotify;

	m_mapEnCodeFunc[R_G_ModifyGoodSharedModeNotify       ]	=	&RelationServiceProtocol::EnCode__ModifyGoodSharedModeNotify;
	m_mapDeCodeFunc[R_G_ModifyGoodSharedModeNotify       ]	=	&RelationServiceProtocol::DeCode__ModifyGoodSharedModeNotify;

	m_mapEnCodeFunc[G_R_TeamBroadcast                    ]	=	&RelationServiceProtocol::EnCode__TeamBroadcastRequest;
	m_mapDeCodeFunc[G_R_TeamBroadcast                    ]	=	&RelationServiceProtocol::DeCode__TeamBroadcastRequest;

	m_mapEnCodeFunc[R_G_TeamBroadcastResult              ]	=	&RelationServiceProtocol::EnCode__TeamBroadcastResult;
	m_mapDeCodeFunc[R_G_TeamBroadcastResult              ]	=	&RelationServiceProtocol::DeCode__TeamBroadcastResult;

	m_mapEnCodeFunc[R_G_TeamBroadcastNotify              ]	=	&RelationServiceProtocol::EnCode__TeamBroadcastNotify;
	m_mapDeCodeFunc[R_G_TeamBroadcastNotify              ]	=	&RelationServiceProtocol::DeCode__TeamBroadcastNotify;

	m_mapEnCodeFunc[G_R_ModifyTeamMemberMapId            ]	=	&RelationServiceProtocol::EnCode__ModifyTeamMemberMapIdRequest;
	m_mapDeCodeFunc[G_R_ModifyTeamMemberMapId            ]	=	&RelationServiceProtocol::DeCode__ModifyTeamMemberMapIdRequest;

	m_mapEnCodeFunc[R_G_ModifyTeamMemberMapIdResult      ]	=	&RelationServiceProtocol::EnCode__ModifyTeamMemberMapIdResult;
	m_mapDeCodeFunc[R_G_ModifyTeamMemberMapIdResult      ]	=	&RelationServiceProtocol::DeCode__ModifyTeamMemberMapIdResult;

	m_mapEnCodeFunc[R_G_ModifyTeamMemberMapIdNotify      ]	=	&RelationServiceProtocol::EnCode__ModifyTeamMemberMapIdNotify;
	m_mapDeCodeFunc[R_G_ModifyTeamMemberMapIdNotify      ]	=	&RelationServiceProtocol::DeCode__ModifyTeamMemberMapIdNotify;

	m_mapEnCodeFunc[G_R_ModifyTeamRollItemLevel          ]	=	&RelationServiceProtocol::EnCode__ModifyTeamRollItemLevelRequest;
	m_mapDeCodeFunc[G_R_ModifyTeamRollItemLevel          ]	=	&RelationServiceProtocol::DeCode__ModifyTeamRollItemLevelRequest;

	m_mapEnCodeFunc[R_G_ModifyTeamRollItemLevelResult    ]	=	&RelationServiceProtocol::EnCode__ModifyTeamRollItemLevelResult;
	m_mapDeCodeFunc[R_G_ModifyTeamRollItemLevelResult    ]	=	&RelationServiceProtocol::DeCode__ModifyTeamRollItemLevelResult;

	m_mapEnCodeFunc[R_G_ModifyTeamRollItemLevelNotify    ]	=	&RelationServiceProtocol::EnCode__ModifyTeamRollItemLevelNotify;
	m_mapDeCodeFunc[R_G_ModifyTeamRollItemLevelNotify    ]	=	&RelationServiceProtocol::DeCode__ModifyTeamRollItemLevelNotify;

	m_mapEnCodeFunc[G_R_CreateChatGroup                  ]	=	&RelationServiceProtocol::EnCode__CreateChatGroupRequest;
	m_mapDeCodeFunc[G_R_CreateChatGroup                  ]	=	&RelationServiceProtocol::DeCode__CreateChatGroupRequest;

	m_mapEnCodeFunc[R_G_CreateChatGroupResult            ]	=	&RelationServiceProtocol::EnCode__CreateChatGroupResult;
	m_mapDeCodeFunc[R_G_CreateChatGroupResult            ]	=	&RelationServiceProtocol::DeCode__CreateChatGroupResult;

	m_mapEnCodeFunc[G_R_DestroyChatGroup                 ]	=	&RelationServiceProtocol::EnCode__DestroyChatGroupRequest;
	m_mapDeCodeFunc[G_R_DestroyChatGroup                 ]	=	&RelationServiceProtocol::DeCode__DestroyChatGroupRequest;

	m_mapEnCodeFunc[R_G_DestroyChatGroupResult           ]	=	&RelationServiceProtocol::EnCode__DestroyChatGroupResult;
	m_mapDeCodeFunc[R_G_DestroyChatGroupResult           ]	=	&RelationServiceProtocol::DeCode__DestroyChatGroupResult;

	m_mapEnCodeFunc[R_G_ChatGroupDestroyedNotify         ]	=	&RelationServiceProtocol::EnCode__ChatGroupDestroyedNotify;
	m_mapDeCodeFunc[R_G_ChatGroupDestroyedNotify         ]	=	&RelationServiceProtocol::DeCode__ChatGroupDestroyedNotify;

	m_mapEnCodeFunc[G_R_AddChatGroupMember               ]	=	&RelationServiceProtocol::EnCode__AddChatGroupMemberRequest;
	m_mapDeCodeFunc[G_R_AddChatGroupMember               ]	=	&RelationServiceProtocol::DeCode__AddChatGroupMemberRequest;

	m_mapEnCodeFunc[R_G_AddChatGroupMemberResult         ]	=	&RelationServiceProtocol::EnCode__AddChatGroupMemberResult;
	m_mapDeCodeFunc[R_G_AddChatGroupMemberResult         ]	=	&RelationServiceProtocol::DeCode__AddChatGroupMemberResult;

	m_mapEnCodeFunc[R_G_ChatGroupMemberAddedNotify       ]	=	&RelationServiceProtocol::EnCode__ChatGroupMemberAddedNotify;
	m_mapDeCodeFunc[R_G_ChatGroupMemberAddedNotify       ]	=	&RelationServiceProtocol::DeCode__ChatGroupMemberAddedNotify;

	m_mapEnCodeFunc[G_R_RemoveChatGroupMember            ]	=	&RelationServiceProtocol::EnCode__RemoveChatGroupMemberRequest;
	m_mapDeCodeFunc[G_R_RemoveChatGroupMember            ]	=	&RelationServiceProtocol::DeCode__RemoveChatGroupMemberRequest;

	m_mapEnCodeFunc[R_G_RemoveChatGroupMemberResult      ]	=	&RelationServiceProtocol::EnCode__RemoveChatGroupMemberResult;
	m_mapDeCodeFunc[R_G_RemoveChatGroupMemberResult      ]	=	&RelationServiceProtocol::DeCode__RemoveChatGroupMemberResult;

	m_mapEnCodeFunc[R_G_ChatGroupMemberRemovedNotify     ]	=	&RelationServiceProtocol::EnCode__ChatGroupMemberRemovedNotify;
	m_mapDeCodeFunc[R_G_ChatGroupMemberRemovedNotify     ]	=	&RelationServiceProtocol::DeCode__ChatGroupMemberRemovedNotify;

	m_mapEnCodeFunc[G_R_ChatGroupSpeek                   ]	=	&RelationServiceProtocol::EnCode__ChatGroupSpeekRequest;
	m_mapDeCodeFunc[G_R_ChatGroupSpeek                   ]	=	&RelationServiceProtocol::DeCode__ChatGroupSpeekRequest;

	m_mapEnCodeFunc[R_G_ChatGroupSpeekResult             ]	=	&RelationServiceProtocol::EnCode__ChatGroupSpeekResult;
	m_mapDeCodeFunc[R_G_ChatGroupSpeekResult             ]	=	&RelationServiceProtocol::DeCode__ChatGroupSpeekResult;

	m_mapEnCodeFunc[R_G_ChatGroupSpeekNotify             ]	=	&RelationServiceProtocol::EnCode__ChatGroupSpeekNotify;
	m_mapDeCodeFunc[R_G_ChatGroupSpeekNotify             ]	=	&RelationServiceProtocol::DeCode__ChatGroupSpeekNotify;

	m_mapEnCodeFunc[G_R_UPLINE_MSG                       ]	=	&RelationServiceProtocol::EnCode__GRUplineInfo;
	m_mapDeCodeFunc[G_R_UPLINE_MSG                       ]	=	&RelationServiceProtocol::DeCode__GRUplineInfo;

	m_mapEnCodeFunc[G_R_UPDATA_MSG                       ]	=	&RelationServiceProtocol::EnCode__GRInfoUpdate;
	m_mapDeCodeFunc[G_R_UPDATA_MSG                       ]	=	&RelationServiceProtocol::DeCode__GRInfoUpdate;

	m_mapEnCodeFunc[G_R_ADDFRIEND_MSG                    ]	=	&RelationServiceProtocol::EnCode__GRAddFriend;
	m_mapDeCodeFunc[G_R_ADDFRIEND_MSG                    ]	=	&RelationServiceProtocol::DeCode__GRAddFriend;

	m_mapEnCodeFunc[G_R_DELFRIEND_MSG                    ]	=	&RelationServiceProtocol::EnCode__GRDelFriend;
	m_mapDeCodeFunc[G_R_DELFRIEND_MSG                    ]	=	&RelationServiceProtocol::DeCode__GRDelFriend;

	m_mapEnCodeFunc[G_R_ADD_GROUP                        ]	=	&RelationServiceProtocol::EnCode__GRAddGroup;
	m_mapDeCodeFunc[G_R_ADD_GROUP                        ]	=	&RelationServiceProtocol::DeCode__GRAddGroup;

	m_mapEnCodeFunc[G_R_FRIEND_ALTER_GROUP_ID            ]	=	&RelationServiceProtocol::EnCode__GRFriendAlterGroupID;
	m_mapDeCodeFunc[G_R_FRIEND_ALTER_GROUP_ID            ]	=	&RelationServiceProtocol::DeCode__GRFriendAlterGroupID;

	m_mapEnCodeFunc[G_R_DEL_GROUP                        ]	=	&RelationServiceProtocol::EnCode__GRDelGroup;
	m_mapDeCodeFunc[G_R_DEL_GROUP                        ]	=	&RelationServiceProtocol::DeCode__GRDelGroup;

	m_mapEnCodeFunc[G_R_ALTERGROUPNAME                   ]	=	&RelationServiceProtocol::EnCode__GRAlterGroupName;
	m_mapDeCodeFunc[G_R_ALTERGROUPNAME                   ]	=	&RelationServiceProtocol::DeCode__GRAlterGroupName;

	m_mapEnCodeFunc[G_R_ADDTOBLACKLIST                   ]	=	&RelationServiceProtocol::EnCode__GRAddToBlacklist;
	m_mapDeCodeFunc[G_R_ADDTOBLACKLIST                   ]	=	&RelationServiceProtocol::DeCode__GRAddToBlacklist;

	m_mapEnCodeFunc[G_R_DELFROMBLACKLIST                 ]	=	&RelationServiceProtocol::EnCode__GRDelFromBlacklist;
	m_mapDeCodeFunc[G_R_DELFROMBLACKLIST                 ]	=	&RelationServiceProtocol::DeCode__GRDelFromBlacklist;

	m_mapEnCodeFunc[G_R_SENDGROUPMSG                     ]	=	&RelationServiceProtocol::EnCode__GRSendGroupMsg;
	m_mapDeCodeFunc[G_R_SENDGROUPMSG                     ]	=	&RelationServiceProtocol::DeCode__GRSendGroupMsg;

	m_mapEnCodeFunc[G_R_SENDMSG                          ]	=	&RelationServiceProtocol::EnCode__GRSendMsg;
	m_mapDeCodeFunc[G_R_SENDMSG                          ]	=	&RelationServiceProtocol::DeCode__GRSendMsg;

	m_mapEnCodeFunc[G_R_ADD_ENEMY                        ]	=	&RelationServiceProtocol::EnCode__GRAddEnemy;
	m_mapDeCodeFunc[G_R_ADD_ENEMY                        ]	=	&RelationServiceProtocol::DeCode__GRAddEnemy;

	m_mapEnCodeFunc[G_R_LOCK_ENEMY                       ]	=	&RelationServiceProtocol::EnCode__GRLockEnemy;
	m_mapDeCodeFunc[G_R_LOCK_ENEMY                       ]	=	&RelationServiceProtocol::DeCode__GRLockEnemy;

	m_mapEnCodeFunc[G_R_DEL_ENEMY                        ]	=	&RelationServiceProtocol::EnCode__GRDelEnemy;
	m_mapDeCodeFunc[G_R_DEL_ENEMY                        ]	=	&RelationServiceProtocol::DeCode__GRDelEnemy;

	m_mapEnCodeFunc[G_R_DEL_PLAYER                       ]	=	&RelationServiceProtocol::EnCode__G_R_DelPlayer;
	m_mapDeCodeFunc[G_R_DEL_PLAYER                       ]	=	&RelationServiceProtocol::DeCode__G_R_DelPlayer;

	m_mapEnCodeFunc[R_G_GROUP_MSG                        ]	=	&RelationServiceProtocol::EnCode__RGFriendGroups;
	m_mapDeCodeFunc[R_G_GROUP_MSG                        ]	=	&RelationServiceProtocol::DeCode__RGFriendGroups;

	m_mapEnCodeFunc[R_G_FRIENDS_MSG                      ]	=	&RelationServiceProtocol::EnCode__RGFriendInfos;
	m_mapDeCodeFunc[R_G_FRIENDS_MSG                      ]	=	&RelationServiceProtocol::DeCode__RGFriendInfos;

	m_mapEnCodeFunc[R_G_UPDATAFRIEND_MSG                 ]	=	&RelationServiceProtocol::EnCode__RGFriendInfoUpdate;
	m_mapDeCodeFunc[R_G_UPDATAFRIEND_MSG                 ]	=	&RelationServiceProtocol::DeCode__RGFriendInfoUpdate;

	m_mapEnCodeFunc[R_G_ADDFRIEND_MSG                    ]	=	&RelationServiceProtocol::EnCode__RGAddFriend;
	m_mapDeCodeFunc[R_G_ADDFRIEND_MSG                    ]	=	&RelationServiceProtocol::DeCode__RGAddFriend;

	m_mapEnCodeFunc[R_G_DELFRIEND_MSG                    ]	=	&RelationServiceProtocol::EnCode__RGDelFriend;
	m_mapDeCodeFunc[R_G_DELFRIEND_MSG                    ]	=	&RelationServiceProtocol::DeCode__RGDelFriend;

	m_mapEnCodeFunc[R_G_ADD_GROUP                        ]	=	&RelationServiceProtocol::EnCode__RGAddGroup;
	m_mapDeCodeFunc[R_G_ADD_GROUP                        ]	=	&RelationServiceProtocol::DeCode__RGAddGroup;

	m_mapEnCodeFunc[R_G_FRIEND_ALTER_GROUP_ID            ]	=	&RelationServiceProtocol::EnCode__RGFriendAlterGroupID;
	m_mapDeCodeFunc[R_G_FRIEND_ALTER_GROUP_ID            ]	=	&RelationServiceProtocol::DeCode__RGFriendAlterGroupID;

	m_mapEnCodeFunc[R_G_DEL_GROUP                        ]	=	&RelationServiceProtocol::EnCode__RGDelGroup;
	m_mapDeCodeFunc[R_G_DEL_GROUP                        ]	=	&RelationServiceProtocol::DeCode__RGDelGroup;

	m_mapEnCodeFunc[R_G_ALTERGROUPNAME                   ]	=	&RelationServiceProtocol::EnCode__RGAlterGroupName;
	m_mapDeCodeFunc[R_G_ALTERGROUPNAME                   ]	=	&RelationServiceProtocol::DeCode__RGAlterGroupName;

	m_mapEnCodeFunc[R_G_ADDTOBLACKLIST                   ]	=	&RelationServiceProtocol::EnCode__RGAddToBlacklist;
	m_mapDeCodeFunc[R_G_ADDTOBLACKLIST                   ]	=	&RelationServiceProtocol::DeCode__RGAddToBlacklist;

	m_mapEnCodeFunc[R_G_DELFROMBLACKLIST                 ]	=	&RelationServiceProtocol::EnCode__RGDelFromBlackList;
	m_mapDeCodeFunc[R_G_DELFROMBLACKLIST                 ]	=	&RelationServiceProtocol::DeCode__RGDelFromBlackList;

	m_mapEnCodeFunc[R_G_SENDGROUPMSG                     ]	=	&RelationServiceProtocol::EnCode__RGSendGroupMsg;
	m_mapDeCodeFunc[R_G_SENDGROUPMSG                     ]	=	&RelationServiceProtocol::DeCode__RGSendGroupMsg;

	m_mapEnCodeFunc[R_G_SENDMSG                          ]	=	&RelationServiceProtocol::EnCode__RGSendMsg;
	m_mapDeCodeFunc[R_G_SENDMSG                          ]	=	&RelationServiceProtocol::DeCode__RGSendMsg;

	m_mapEnCodeFunc[R_G_BLACKLIST                        ]	=	&RelationServiceProtocol::EnCode__RGBlacklist;
	m_mapDeCodeFunc[R_G_BLACKLIST                        ]	=	&RelationServiceProtocol::DeCode__RGBlacklist;

	m_mapEnCodeFunc[R_G_ADD_ENEMY                        ]	=	&RelationServiceProtocol::EnCode__RGAddEnemy;
	m_mapDeCodeFunc[R_G_ADD_ENEMY                        ]	=	&RelationServiceProtocol::DeCode__RGAddEnemy;

	m_mapEnCodeFunc[R_G_LOCK_ENEMY                       ]	=	&RelationServiceProtocol::EnCode__RGLockEnemy;
	m_mapDeCodeFunc[R_G_LOCK_ENEMY                       ]	=	&RelationServiceProtocol::DeCode__RGLockEnemy;

	m_mapEnCodeFunc[R_G_DEL_ENEMY                        ]	=	&RelationServiceProtocol::EnCode__RGDelEnemy;
	m_mapDeCodeFunc[R_G_DEL_ENEMY                        ]	=	&RelationServiceProtocol::DeCode__RGDelEnemy;

	m_mapEnCodeFunc[R_G_ENEMY_LIST                       ]	=	&RelationServiceProtocol::EnCode__RGEnemylist;
	m_mapDeCodeFunc[R_G_ENEMY_LIST                       ]	=	&RelationServiceProtocol::DeCode__RGEnemylist;

	m_mapEnCodeFunc[R_G_UPDATE_ENEMY                     ]	=	&RelationServiceProtocol::EnCode__RGUpdateEnemy;
	m_mapDeCodeFunc[R_G_UPDATE_ENEMY                     ]	=	&RelationServiceProtocol::DeCode__RGUpdateEnemy;

	m_mapEnCodeFunc[R_G_NOTIFY_R_GROUP_MSG_SUM           ]	=	&RelationServiceProtocol::EnCode__RGNotifyRGroupMsgNum;
	m_mapDeCodeFunc[R_G_NOTIFY_R_GROUP_MSG_SUM           ]	=	&RelationServiceProtocol::DeCode__RGNotifyRGroupMsgNum;

	m_mapEnCodeFunc[G_R_REPORT_PLAYER_UPLINE             ]	=	&RelationServiceProtocol::EnCode__ReportPlayerUpline;
	m_mapDeCodeFunc[G_R_REPORT_PLAYER_UPLINE             ]	=	&RelationServiceProtocol::DeCode__ReportPlayerUpline;

	m_mapEnCodeFunc[G_R_REPORT_PLAYER_OFFLINE            ]	=	&RelationServiceProtocol::EnCode__ReportPlayerOffline;
	m_mapDeCodeFunc[G_R_REPORT_PLAYER_OFFLINE            ]	=	&RelationServiceProtocol::DeCode__ReportPlayerOffline;

	m_mapEnCodeFunc[G_R_CREATE_UNION_REQUEST             ]	=	&RelationServiceProtocol::EnCode__CreateUnionRequest;
	m_mapDeCodeFunc[G_R_CREATE_UNION_REQUEST             ]	=	&RelationServiceProtocol::DeCode__CreateUnionRequest;

	m_mapEnCodeFunc[R_G_CREATE_UNION_RESULT              ]	=	&RelationServiceProtocol::EnCode__CreateUnionResult;
	m_mapDeCodeFunc[R_G_CREATE_UNION_RESULT              ]	=	&RelationServiceProtocol::DeCode__CreateUnionResult;

	m_mapEnCodeFunc[G_R_DESTROY_UNION_REQUEST            ]	=	&RelationServiceProtocol::EnCode__DestroyUnionRequest;
	m_mapDeCodeFunc[G_R_DESTROY_UNION_REQUEST            ]	=	&RelationServiceProtocol::DeCode__DestroyUnionRequest;

	m_mapEnCodeFunc[R_G_DESTROY_UNION_RESULT             ]	=	&RelationServiceProtocol::EnCode__DestroyUnionResult;
	m_mapDeCodeFunc[R_G_DESTROY_UNION_RESULT             ]	=	&RelationServiceProtocol::DeCode__DestroyUnionResult;

	m_mapEnCodeFunc[G_R_QUERY_UNION_BASIC_REQUEST        ]	=	&RelationServiceProtocol::EnCode__QueryUnionBasicRequest;
	m_mapDeCodeFunc[G_R_QUERY_UNION_BASIC_REQUEST        ]	=	&RelationServiceProtocol::DeCode__QueryUnionBasicRequest;

	m_mapEnCodeFunc[R_G_QUERY_UNION_BASIC_RESULT         ]	=	&RelationServiceProtocol::EnCode__QueryUnionBasicResult;
	m_mapDeCodeFunc[R_G_QUERY_UNION_BASIC_RESULT         ]	=	&RelationServiceProtocol::DeCode__QueryUnionBasicResult;

	m_mapEnCodeFunc[G_R_UNION_ADD_MEMBER_REQUEST         ]	=	&RelationServiceProtocol::EnCode__AddUnionMemberRequest;
	m_mapDeCodeFunc[G_R_UNION_ADD_MEMBER_REQUEST         ]	=	&RelationServiceProtocol::DeCode__AddUnionMemberRequest;

	m_mapEnCodeFunc[R_G_UNION_ADD_MEMBER_RESULT          ]	=	&RelationServiceProtocol::EnCode__AddUnionMemberResult;
	m_mapDeCodeFunc[R_G_UNION_ADD_MEMBER_RESULT          ]	=	&RelationServiceProtocol::DeCode__AddUnionMemberResult;

	m_mapEnCodeFunc[G_R_UNION_ADD_MEMBER_CONFIRMED       ]	=	&RelationServiceProtocol::EnCode__AddUnionMemberConfirmed;
	m_mapDeCodeFunc[G_R_UNION_ADD_MEMBER_CONFIRMED       ]	=	&RelationServiceProtocol::DeCode__AddUnionMemberConfirmed;

	m_mapEnCodeFunc[R_G_UNION_ADD_MEMBER_CONFIRM         ]	=	&RelationServiceProtocol::EnCode__AddUnionMemberConfirm;
	m_mapDeCodeFunc[R_G_UNION_ADD_MEMBER_CONFIRM         ]	=	&RelationServiceProtocol::DeCode__AddUnionMemberConfirm;

	m_mapEnCodeFunc[G_R_UNION_REMOVE_MEMBER_REQUEST      ]	=	&RelationServiceProtocol::EnCode__RemoveUnionMemberRequest;
	m_mapDeCodeFunc[G_R_UNION_REMOVE_MEMBER_REQUEST      ]	=	&RelationServiceProtocol::DeCode__RemoveUnionMemberRequest;

	m_mapEnCodeFunc[R_G_UNION_REMOVE_MEMBER_RESULT       ]	=	&RelationServiceProtocol::EnCode__RemoveUnionMemberResult;
	m_mapDeCodeFunc[R_G_UNION_REMOVE_MEMBER_RESULT       ]	=	&RelationServiceProtocol::DeCode__RemoveUnionMemberResult;

	m_mapEnCodeFunc[G_R_QUERY_UNION_MEMBER_LIST_REQUEST  ]	=	&RelationServiceProtocol::EnCode__QueryUnionMembersRequest;
	m_mapDeCodeFunc[G_R_QUERY_UNION_MEMBER_LIST_REQUEST  ]	=	&RelationServiceProtocol::DeCode__QueryUnionMembersRequest;

	m_mapEnCodeFunc[R_G_QUERY_UNION_MEMBER_LIST_RESULT   ]	=	&RelationServiceProtocol::EnCode__QueryUnionMembersResult;
	m_mapDeCodeFunc[R_G_QUERY_UNION_MEMBER_LIST_RESULT   ]	=	&RelationServiceProtocol::DeCode__QueryUnionMembersResult;

	m_mapEnCodeFunc[R_G_UNION_DESTROYED_NOTIFY           ]	=	&RelationServiceProtocol::EnCode__UnionDestroyedNotify;
	m_mapDeCodeFunc[R_G_UNION_DESTROYED_NOTIFY           ]	=	&RelationServiceProtocol::DeCode__UnionDestroyedNotify;

	m_mapEnCodeFunc[R_G_UNION_MEMBER_ADDED_NOTIFY        ]	=	&RelationServiceProtocol::EnCode__AddUnionMemberNotify;
	m_mapDeCodeFunc[R_G_UNION_MEMBER_ADDED_NOTIFY        ]	=	&RelationServiceProtocol::DeCode__AddUnionMemberNotify;

	m_mapEnCodeFunc[R_G_UNION_MEMBER_REMOVED_NOTIFY      ]	=	&RelationServiceProtocol::EnCode__RemoveUnionMemberNotify;
	m_mapDeCodeFunc[R_G_UNION_MEMBER_REMOVED_NOTIFY      ]	=	&RelationServiceProtocol::DeCode__RemoveUnionMemberNotify;

	m_mapEnCodeFunc[R_G_UNION_MEMBER_TITLE_NOTIFY        ]	=	&RelationServiceProtocol::EnCode__UnionMemberTitleNotify;
	m_mapDeCodeFunc[R_G_UNION_MEMBER_TITLE_NOTIFY        ]	=	&RelationServiceProtocol::DeCode__UnionMemberTitleNotify;

	m_mapEnCodeFunc[R_G_UNION_MODIFY_MEMBER_TITLE_RESULT ]	=	&RelationServiceProtocol::EnCode__ModifyUnionMemberTitleResult;
	m_mapDeCodeFunc[R_G_UNION_MODIFY_MEMBER_TITLE_RESULT ]	=	&RelationServiceProtocol::DeCode__ModifyUnionMemberTitleResult;

	m_mapEnCodeFunc[G_R_UNION_MODIFY_MEMBER_TITLE_REQUEST]	=	&RelationServiceProtocol::EnCode__ModifyUnionMemberTitleRequest;
	m_mapDeCodeFunc[G_R_UNION_MODIFY_MEMBER_TITLE_REQUEST]	=	&RelationServiceProtocol::DeCode__ModifyUnionMemberTitleRequest;

	m_mapEnCodeFunc[R_G_UNION_TRANSFORM_CAPTION_NOTIFY   ]	=	&RelationServiceProtocol::EnCode__TransformUnionCaptionNotify;
	m_mapDeCodeFunc[R_G_UNION_TRANSFORM_CAPTION_NOTIFY   ]	=	&RelationServiceProtocol::DeCode__TransformUnionCaptionNotify;

	m_mapEnCodeFunc[R_G_UNION_TRANSFORM_CAPTION_RESULT   ]	=	&RelationServiceProtocol::EnCode__TransformUnionCaptionResult;
	m_mapDeCodeFunc[R_G_UNION_TRANSFORM_CAPTION_RESULT   ]	=	&RelationServiceProtocol::DeCode__TransformUnionCaptionResult;

	m_mapEnCodeFunc[G_R_UNION_TRANSFROM_CAPTION_REQUEST  ]	=	&RelationServiceProtocol::EnCode__TransformUnionCaptionRequest;
	m_mapDeCodeFunc[G_R_UNION_TRANSFROM_CAPTION_REQUEST  ]	=	&RelationServiceProtocol::DeCode__TransformUnionCaptionRequest;

	m_mapEnCodeFunc[G_R_UNION_ADVANCE_MEMBER_POS_REQUEST ]	=	&RelationServiceProtocol::EnCode__AdvanceUnionMemberPosRequest;
	m_mapDeCodeFunc[G_R_UNION_ADVANCE_MEMBER_POS_REQUEST ]	=	&RelationServiceProtocol::DeCode__AdvanceUnionMemberPosRequest;

	m_mapEnCodeFunc[R_G_UNION_ADVANCE_MEMBER_POS_RESULT  ]	=	&RelationServiceProtocol::EnCode__AdvanceUnionMemberPosResult;
	m_mapDeCodeFunc[R_G_UNION_ADVANCE_MEMBER_POS_RESULT  ]	=	&RelationServiceProtocol::DeCode__AdvanceUnionMemberPosResult;

	m_mapEnCodeFunc[R_G_UNION_ADVANCE_MEMBER_POS_NOTIFY  ]	=	&RelationServiceProtocol::EnCode__AdvanceUnionMemberPosNotify;
	m_mapDeCodeFunc[R_G_UNION_ADVANCE_MEMBER_POS_NOTIFY  ]	=	&RelationServiceProtocol::DeCode__AdvanceUnionMemberPosNotify;

	m_mapEnCodeFunc[G_R_UNION_REDUCE_MEMBER_POS_REQUEST  ]	=	&RelationServiceProtocol::EnCode__ReduceUnionMemberPosRequest;
	m_mapDeCodeFunc[G_R_UNION_REDUCE_MEMBER_POS_REQUEST  ]	=	&RelationServiceProtocol::DeCode__ReduceUnionMemberPosRequest;

	m_mapEnCodeFunc[R_G_UNION_REDUCE_MEMBER_POS_RESULT   ]	=	&RelationServiceProtocol::EnCode__ReduceUnionMemberPosResult;
	m_mapDeCodeFunc[R_G_UNION_REDUCE_MEMBER_POS_RESULT   ]	=	&RelationServiceProtocol::DeCode__ReduceUnionMemberPosResult;

	m_mapEnCodeFunc[R_G_UNION_REDUCE_MEMBER_POS_NOTIFY   ]	=	&RelationServiceProtocol::EnCode__ReduceUnionMemberPosNotify;
	m_mapDeCodeFunc[R_G_UNION_REDUCE_MEMBER_POS_NOTIFY   ]	=	&RelationServiceProtocol::DeCode__ReduceUnionMemberPosNotify;

	m_mapEnCodeFunc[G_R_UNION_CHANNEL_SPEEK_REQUEST      ]	=	&RelationServiceProtocol::EnCode__UnionChannelSpeekRequest;
	m_mapDeCodeFunc[G_R_UNION_CHANNEL_SPEEK_REQUEST      ]	=	&RelationServiceProtocol::DeCode__UnionChannelSpeekRequest;

	m_mapEnCodeFunc[R_G_UNION_CHANNEL_SPEEK_RESULT       ]	=	&RelationServiceProtocol::EnCode__UnionChannelSpeekResult;
	m_mapDeCodeFunc[R_G_UNION_CHANNEL_SPEEK_RESULT       ]	=	&RelationServiceProtocol::DeCode__UnionChannelSpeekResult;

	m_mapEnCodeFunc[R_G_UNION_CHANNEL_SPEEK_NOTIFY       ]	=	&RelationServiceProtocol::EnCode__UnionChannelSpeekNotify;
	m_mapDeCodeFunc[R_G_UNION_CHANNEL_SPEEK_NOTIFY       ]	=	&RelationServiceProtocol::DeCode__UnionChannelSpeekNotify;

	m_mapEnCodeFunc[G_R_UNION_UPDATE_POS_CFG_REQUEST     ]	=	&RelationServiceProtocol::EnCode__UpdateUnionPosCfgRequest;
	m_mapDeCodeFunc[G_R_UNION_UPDATE_POS_CFG_REQUEST     ]	=	&RelationServiceProtocol::DeCode__UpdateUnionPosCfgRequest;

	m_mapEnCodeFunc[R_G_UNION_UPDATE_POS_CFG_RESULT      ]	=	&RelationServiceProtocol::EnCode__UpdateUnionPosCfgResult;
	m_mapDeCodeFunc[R_G_UNION_UPDATE_POS_CFG_RESULT      ]	=	&RelationServiceProtocol::DeCode__UpdateUnionPosCfgResult;

	m_mapEnCodeFunc[R_G_UNION_UPDATE_POS_CFG_NOTIFY      ]	=	&RelationServiceProtocol::EnCode__UpdateUnionPosCfgNotify;
	m_mapDeCodeFunc[R_G_UNION_UPDATE_POS_CFG_NOTIFY      ]	=	&RelationServiceProtocol::DeCode__UpdateUnionPosCfgNotify;

	m_mapEnCodeFunc[G_R_UNION_FORBID_SPEEK_REQUEST       ]	=	&RelationServiceProtocol::EnCode__ForbidUnionSpeekRequest;
	m_mapDeCodeFunc[G_R_UNION_FORBID_SPEEK_REQUEST       ]	=	&RelationServiceProtocol::DeCode__ForbidUnionSpeekRequest;

	m_mapEnCodeFunc[R_G_UNION_FORBID_SPEEK_RESULT        ]	=	&RelationServiceProtocol::EnCode__ForbidUnionSpeekResult;
	m_mapDeCodeFunc[R_G_UNION_FORBID_SPEEK_RESULT        ]	=	&RelationServiceProtocol::DeCode__ForbidUnionSpeekResult;

	m_mapEnCodeFunc[R_G_UNION_FORBID_SPEEK_NOTIFY        ]	=	&RelationServiceProtocol::EnCode__ForbidUnionSpeekNotify;
	m_mapDeCodeFunc[R_G_UNION_FORBID_SPEEK_NOTIFY        ]	=	&RelationServiceProtocol::DeCode__ForbidUnionSpeekNotify;

	m_mapEnCodeFunc[R_G_UNION_SKILLS_LIST_NOTIFY         ]	=	&RelationServiceProtocol::EnCode__UnionSkillsListNotify;
	m_mapDeCodeFunc[R_G_UNION_SKILLS_LIST_NOTIFY         ]	=	&RelationServiceProtocol::DeCode__UnionSkillsListNotify;

	m_mapEnCodeFunc[G_R_STUDY_UNION_SKILL_REQUEST        ]	=	&RelationServiceProtocol::EnCode__StudyUnionSkillRequest;
	m_mapDeCodeFunc[G_R_STUDY_UNION_SKILL_REQUEST        ]	=	&RelationServiceProtocol::DeCode__StudyUnionSkillRequest;

	m_mapEnCodeFunc[R_G_STUDY_UNION_SKILL_RESULT         ]	=	&RelationServiceProtocol::EnCode__StudyUnionSkillResult;
	m_mapDeCodeFunc[R_G_STUDY_UNION_SKILL_RESULT         ]	=	&RelationServiceProtocol::DeCode__StudyUnionSkillResult;

	m_mapEnCodeFunc[G_R_MODIFY_UNION_ACTIVE_POINT_REQUEST]	=	&RelationServiceProtocol::EnCode__ModifyUnionActivePointRequest;
	m_mapDeCodeFunc[G_R_MODIFY_UNION_ACTIVE_POINT_REQUEST]	=	&RelationServiceProtocol::DeCode__ModifyUnionActivePointRequest;

	m_mapEnCodeFunc[R_G_MODIFY_UNION_ACTIVE_POINT_RESULT ]	=	&RelationServiceProtocol::EnCode__ModifyUnionActivePointResult;
	m_mapDeCodeFunc[R_G_MODIFY_UNION_ACTIVE_POINT_RESULT ]	=	&RelationServiceProtocol::DeCode__ModifyUnionActivePointResult;

	m_mapEnCodeFunc[R_G_UNION_ACTIVE_POINT_NOTIFY        ]	=	&RelationServiceProtocol::EnCode__UnionActivePointNotify;
	m_mapDeCodeFunc[R_G_UNION_ACTIVE_POINT_NOTIFY        ]	=	&RelationServiceProtocol::DeCode__UnionActivePointNotify;

	m_mapEnCodeFunc[G_R_POST_UNION_TASK_LIST_REQUEST     ]	=	&RelationServiceProtocol::EnCode__PostUnionTasksListRequest;
	m_mapDeCodeFunc[G_R_POST_UNION_TASK_LIST_REQUEST     ]	=	&RelationServiceProtocol::DeCode__PostUnionTasksListRequest;

	m_mapEnCodeFunc[R_G_POST_UNION_TASK_LIST_RESULT      ]	=	&RelationServiceProtocol::EnCode__PostUnionTasksListResult;
	m_mapDeCodeFunc[R_G_POST_UNION_TASK_LIST_RESULT      ]	=	&RelationServiceProtocol::DeCode__PostUnionTasksListResult;

	m_mapEnCodeFunc[R_G_UNION_TASK_LIST_NOTIFY           ]	=	&RelationServiceProtocol::EnCode__UnionTasksListNotify;
	m_mapDeCodeFunc[R_G_UNION_TASK_LIST_NOTIFY           ]	=	&RelationServiceProtocol::DeCode__UnionTasksListNotify;

	m_mapEnCodeFunc[G_R_ADVANCE_UNION_LEVEL_REQUEST      ]	=	&RelationServiceProtocol::EnCode__AdvanceUnionLevelRequest;
	m_mapDeCodeFunc[G_R_ADVANCE_UNION_LEVEL_REQUEST      ]	=	&RelationServiceProtocol::DeCode__AdvanceUnionLevelRequest;

	m_mapEnCodeFunc[R_G_ADVANCE_UNION_LEVEL_RESULT       ]	=	&RelationServiceProtocol::EnCode__AdvanceUnionLevelResult;
	m_mapDeCodeFunc[R_G_ADVANCE_UNION_LEVEL_RESULT       ]	=	&RelationServiceProtocol::DeCode__AdvanceUnionLevelResult;

	m_mapEnCodeFunc[R_G_UNION_LEVEL_NOTIFY               ]	=	&RelationServiceProtocol::EnCode__UnionLevelNotify;
	m_mapDeCodeFunc[R_G_UNION_LEVEL_NOTIFY               ]	=	&RelationServiceProtocol::DeCode__UnionLevelNotify;

	m_mapEnCodeFunc[G_R_POST_UNION_BULLETIN_REQUEST      ]	=	&RelationServiceProtocol::EnCode__PostUnionBulletinRequest;
	m_mapDeCodeFunc[G_R_POST_UNION_BULLETIN_REQUEST      ]	=	&RelationServiceProtocol::DeCode__PostUnionBulletinRequest;

	m_mapEnCodeFunc[R_G_POST_UNION_BULLETIN_RESULT       ]	=	&RelationServiceProtocol::EnCode__PostUnionBulletinResult;
	m_mapDeCodeFunc[R_G_POST_UNION_BULLETIN_RESULT       ]	=	&RelationServiceProtocol::DeCode__PostUnionBulletinResult;

	m_mapEnCodeFunc[R_G_UNION_BULLETIN_NOTIFY            ]	=	&RelationServiceProtocol::EnCode__UnionBulletinNotify;
	m_mapDeCodeFunc[R_G_UNION_BULLETIN_NOTIFY            ]	=	&RelationServiceProtocol::DeCode__UnionBulletinNotify;

	m_mapEnCodeFunc[R_G_UNION_MEMBER_UPLINE_NOTIFY       ]	=	&RelationServiceProtocol::EnCode__UnionMemberUplineNotify;
	m_mapDeCodeFunc[R_G_UNION_MEMBER_UPLINE_NOTIFY       ]	=	&RelationServiceProtocol::DeCode__UnionMemberUplineNotify;

	m_mapEnCodeFunc[R_G_UNION_MEMBER_OFFLINE_NOTIFY      ]	=	&RelationServiceProtocol::EnCode__UnionMemberOfflineNotify;
	m_mapDeCodeFunc[R_G_UNION_MEMBER_OFFLINE_NOTIFY      ]	=	&RelationServiceProtocol::DeCode__UnionMemberOfflineNotify;

	m_mapEnCodeFunc[G_R_QUERY_UNION_LOG_REQUEST          ]	=	&RelationServiceProtocol::EnCode__QueryUnionLogRequest;
	m_mapDeCodeFunc[G_R_QUERY_UNION_LOG_REQUEST          ]	=	&RelationServiceProtocol::DeCode__QueryUnionLogRequest;

	m_mapEnCodeFunc[R_G_QUERY_UNION_LOG_RESULT           ]	=	&RelationServiceProtocol::EnCode__QueryUnionLogResult;
	m_mapDeCodeFunc[R_G_QUERY_UNION_LOG_RESULT           ]	=	&RelationServiceProtocol::DeCode__QueryUnionLogResult;

	m_mapEnCodeFunc[R_G_UNION_EMAIL_NOTIFY               ]	=	&RelationServiceProtocol::EnCode__UnionEmailNotify;
	m_mapDeCodeFunc[R_G_UNION_EMAIL_NOTIFY               ]	=	&RelationServiceProtocol::DeCode__UnionEmailNotify;

	m_mapEnCodeFunc[G_R_UNION_OWNER_CHECK_REQUEST        ]	=	&RelationServiceProtocol::EnCode__UnionOwnerCheckRequest;
	m_mapDeCodeFunc[G_R_UNION_OWNER_CHECK_REQUEST        ]	=	&RelationServiceProtocol::DeCode__UnionOwnerCheckRequest;

	m_mapEnCodeFunc[R_G_UNION_OWNER_CHECK_RESULT         ]	=	&RelationServiceProtocol::EnCode__UnionOwnerCheckResult;
	m_mapDeCodeFunc[R_G_UNION_OWNER_CHECK_RESULT         ]	=	&RelationServiceProtocol::DeCode__UnionOwnerCheckResult;

	m_mapEnCodeFunc[G_R_UNION_MODIFY_PRESTIGE_REQUEST    ]	=	&RelationServiceProtocol::EnCode__ModifyUnionPrestigeRequest;
	m_mapDeCodeFunc[G_R_UNION_MODIFY_PRESTIGE_REQUEST    ]	=	&RelationServiceProtocol::DeCode__ModifyUnionPrestigeRequest;

	m_mapEnCodeFunc[R_G_UNION_MODIFY_PRESTIGE_RESULT     ]	=	&RelationServiceProtocol::EnCode__ModifyUnionPrestigeResult;
	m_mapDeCodeFunc[R_G_UNION_MODIFY_PRESTIGE_RESULT     ]	=	&RelationServiceProtocol::DeCode__ModifyUnionPrestigeResult;

	m_mapEnCodeFunc[R_G_UNION_PRESTIGE_NOTIFY            ]	=	&RelationServiceProtocol::EnCode__UnionPrestigeNotify;
	m_mapDeCodeFunc[R_G_UNION_PRESTIGE_NOTIFY            ]	=	&RelationServiceProtocol::DeCode__UnionPrestigeNotify;

	m_mapEnCodeFunc[G_R_UNION_MODIFY_BADGE_REQUEST       ]	=	&RelationServiceProtocol::EnCode__ModifyUnionBadgeRequest;
	m_mapDeCodeFunc[G_R_UNION_MODIFY_BADGE_REQUEST       ]	=	&RelationServiceProtocol::DeCode__ModifyUnionBadgeRequest;

	m_mapEnCodeFunc[R_G_UNION_MODIFY_BADGE_RESULT        ]	=	&RelationServiceProtocol::EnCode__ModifyUnionBadgeResult;
	m_mapDeCodeFunc[R_G_UNION_MODIFY_BADGE_RESULT        ]	=	&RelationServiceProtocol::DeCode__ModifyUnionBadgeResult;

	m_mapEnCodeFunc[R_G_UNION_BADGE_NOTIFY               ]	=	&RelationServiceProtocol::EnCode__UnionBadgeNotify;
	m_mapDeCodeFunc[R_G_UNION_BADGE_NOTIFY               ]	=	&RelationServiceProtocol::DeCode__UnionBadgeNotify;

	m_mapEnCodeFunc[G_R_UNION_MODIFY_MULTI_REQUEST       ]	=	&RelationServiceProtocol::EnCode__ModifyUnionMultiRequest;
	m_mapDeCodeFunc[G_R_UNION_MODIFY_MULTI_REQUEST       ]	=	&RelationServiceProtocol::DeCode__ModifyUnionMultiRequest;

	m_mapEnCodeFunc[R_G_UNION_MULTI_NOTIFY               ]	=	&RelationServiceProtocol::EnCode__UnionMultiNotify;
	m_mapDeCodeFunc[R_G_UNION_MULTI_NOTIFY               ]	=	&RelationServiceProtocol::DeCode__UnionMultiNotify;

	m_mapEnCodeFunc[G_R_QUERY_PLAYER_RELATION_REQUEST    ]	=	&RelationServiceProtocol::EnCode__QueryPlayerRelationRequest;
	m_mapDeCodeFunc[G_R_QUERY_PLAYER_RELATION_REQUEST    ]	=	&RelationServiceProtocol::DeCode__QueryPlayerRelationRequest;

	m_mapEnCodeFunc[R_G_QUERY_PLAYER_RELATION_RESULT     ]	=	&RelationServiceProtocol::EnCode__QueryPlayerRelationResult;
	m_mapDeCodeFunc[R_G_QUERY_PLAYER_RELATION_RESULT     ]	=	&RelationServiceProtocol::DeCode__QueryPlayerRelationResult;

	m_mapEnCodeFunc[G_R_QUERY_PLAYER_INFO_REQUEST        ]	=	&RelationServiceProtocol::EnCode__QueryPlayerInfoRequest;
	m_mapDeCodeFunc[G_R_QUERY_PLAYER_INFO_REQUEST        ]	=	&RelationServiceProtocol::DeCode__QueryPlayerInfoRequest;

	m_mapEnCodeFunc[R_G_QUERY_PLAYER_INFO_RESULT         ]	=	&RelationServiceProtocol::EnCode__QueryPlayerInfoResult;
	m_mapDeCodeFunc[R_G_QUERY_PLAYER_INFO_RESULT         ]	=	&RelationServiceProtocol::DeCode__QueryPlayerInfoResult;

	m_mapEnCodeFunc[G_R_QUERY_UNION_RANK_REQUEST         ]	=	&RelationServiceProtocol::EnCode__QueryUnionRankRequest;
	m_mapDeCodeFunc[G_R_QUERY_UNION_RANK_REQUEST         ]	=	&RelationServiceProtocol::DeCode__QueryUnionRankRequest;

	m_mapEnCodeFunc[R_G_QUERY_UNION_RANK_RESULT          ]	=	&RelationServiceProtocol::EnCode__QueryUnionRankResult;
	m_mapDeCodeFunc[R_G_QUERY_UNION_RANK_RESULT          ]	=	&RelationServiceProtocol::DeCode__QueryUnionRankResult;

	m_mapEnCodeFunc[G_R_SEARCH_UNION_RANK_REQUEST        ]	=	&RelationServiceProtocol::EnCode__SearchUnionRankRequest;
	m_mapDeCodeFunc[G_R_SEARCH_UNION_RANK_REQUEST        ]	=	&RelationServiceProtocol::DeCode__SearchUnionRankRequest;

	m_mapEnCodeFunc[R_G_SEARCH_UNION_RANK_RESULT         ]	=	&RelationServiceProtocol::EnCode__SearchUnionRankResult;
	m_mapDeCodeFunc[R_G_SEARCH_UNION_RANK_RESULT         ]	=	&RelationServiceProtocol::DeCode__SearchUnionRankResult;

	m_mapEnCodeFunc[R_G_PLAYER_UPLINE_TEAMID_NOTIFY      ]	=	&RelationServiceProtocol::EnCode__PlayerUplineTeamIdNotify;
	m_mapDeCodeFunc[R_G_PLAYER_UPLINE_TEAMID_NOTIFY      ]	=	&RelationServiceProtocol::DeCode__PlayerUplineTeamIdNotify;

	m_mapEnCodeFunc[G_R_QUERY_RACEMASTER_REQUEST         ]	=	&RelationServiceProtocol::EnCode__QueryRaceMasterRequest;
	m_mapDeCodeFunc[G_R_QUERY_RACEMASTER_REQUEST         ]	=	&RelationServiceProtocol::DeCode__QueryRaceMasterRequest;

	m_mapEnCodeFunc[R_G_QUERY_RACEMASTER_RESULT          ]	=	&RelationServiceProtocol::EnCode__QueryRaceMasterResult;
	m_mapDeCodeFunc[R_G_QUERY_RACEMASTER_RESULT          ]	=	&RelationServiceProtocol::DeCode__QueryRaceMasterResult;

	m_mapEnCodeFunc[R_G_TEAMLOG                          ]	=	&RelationServiceProtocol::EnCode__TeamLogNotify;
	m_mapDeCodeFunc[R_G_TEAMLOG                          ]	=	&RelationServiceProtocol::DeCode__TeamLogNotify;

	m_mapEnCodeFunc[G_R_QUERY_GUILD_MEMBERS_REQUEST      ]	=	&RelationServiceProtocol::EnCode__QueryGuildMembersRequest;
	m_mapDeCodeFunc[G_R_QUERY_GUILD_MEMBERS_REQUEST      ]	=	&RelationServiceProtocol::DeCode__QueryGuildMembersRequest;

	m_mapEnCodeFunc[R_G_QUERY_GUILD_MEMBERS_RESULT       ]	=	&RelationServiceProtocol::EnCode__QueryGuildMembersResult;
	m_mapDeCodeFunc[R_G_QUERY_GUILD_MEMBERS_RESULT       ]	=	&RelationServiceProtocol::DeCode__QueryGuildMembersResult;

	m_mapEnCodeFunc[G_R_ADD_FRIENDLY_DEGREE              ]	=	&RelationServiceProtocol::EnCode__AddFriendlyDegree;
	m_mapDeCodeFunc[G_R_ADD_FRIENDLY_DEGREE              ]	=	&RelationServiceProtocol::DeCode__AddFriendlyDegree;

	m_mapEnCodeFunc[G_R_ADD_TWEET                        ]	=	&RelationServiceProtocol::EnCode__AddTweet;
	m_mapDeCodeFunc[G_R_ADD_TWEET                        ]	=	&RelationServiceProtocol::DeCode__AddTweet;

	m_mapEnCodeFunc[R_G_SHOW_TWEET                       ]	=	&RelationServiceProtocol::EnCode__ShowFriendTweet;
	m_mapDeCodeFunc[R_G_SHOW_TWEET                       ]	=	&RelationServiceProtocol::DeCode__ShowFriendTweet;

	m_mapEnCodeFunc[G_R_QUERY_TWEETS_REQUEST             ]	=	&RelationServiceProtocol::EnCode__QueryFriendTweetsRequest;
	m_mapDeCodeFunc[G_R_QUERY_TWEETS_REQUEST             ]	=	&RelationServiceProtocol::DeCode__QueryFriendTweetsRequest;

	m_mapEnCodeFunc[R_G_QUERY_TWEETS_RESULT              ]	=	&RelationServiceProtocol::EnCode__QueryFriendTweetsResult;
	m_mapDeCodeFunc[R_G_QUERY_TWEETS_RESULT              ]	=	&RelationServiceProtocol::DeCode__QueryFriendTweetsResult;

	m_mapEnCodeFunc[G_R_QUERY_FRIENDS_LIST_REQUEST       ]	=	&RelationServiceProtocol::EnCode__QueryFriendsListRequest;
	m_mapDeCodeFunc[G_R_QUERY_FRIENDS_LIST_REQUEST       ]	=	&RelationServiceProtocol::DeCode__QueryFriendsListRequest;

	m_mapEnCodeFunc[R_G_QUERY_FRIENDS_LIST_RESULT        ]	=	&RelationServiceProtocol::EnCode__QueryFriendsListResult;
	m_mapDeCodeFunc[R_G_QUERY_FRIENDS_LIST_RESULT        ]	=	&RelationServiceProtocol::DeCode__QueryFriendsListResult;

	m_mapEnCodeFunc[G_R_NOTIY_EXP_ADDED                  ]	=	&RelationServiceProtocol::EnCode__NotifyExpAdded;
	m_mapDeCodeFunc[G_R_NOTIY_EXP_ADDED                  ]	=	&RelationServiceProtocol::DeCode__NotifyExpAdded;

	m_mapEnCodeFunc[R_G_NOTIFY_ADD_EXP                   ]	=	&RelationServiceProtocol::EnCode__NotifyAddExp;
	m_mapDeCodeFunc[R_G_NOTIFY_ADD_EXP                   ]	=	&RelationServiceProtocol::DeCode__NotifyAddExp;

	m_mapEnCodeFunc[R_G_QUERY_EXP_NEEDED_LVL_UP_REQUEST  ]	=	&RelationServiceProtocol::EnCode__QueryExpNeededWhenLvlUpRequest;
	m_mapDeCodeFunc[R_G_QUERY_EXP_NEEDED_LVL_UP_REQUEST  ]	=	&RelationServiceProtocol::DeCode__QueryExpNeededWhenLvlUpRequest;

	m_mapEnCodeFunc[G_R_QUERY_EXP_NEEDED_LVL_UP_RESULT   ]	=	&RelationServiceProtocol::EnCode__QueryExpNeededWhenLvlUpResult;
	m_mapDeCodeFunc[G_R_QUERY_EXP_NEEDED_LVL_UP_RESULT   ]	=	&RelationServiceProtocol::DeCode__QueryExpNeededWhenLvlUpResult;

	m_mapEnCodeFunc[R_G_NOTIFY_TEAM_GAIN                 ]	=	&RelationServiceProtocol::EnCode__NotifyTeamGain;
	m_mapDeCodeFunc[R_G_NOTIFY_TEAM_GAIN                 ]	=	&RelationServiceProtocol::DeCode__NotifyTeamGain;

	m_mapEnCodeFunc[G_R_MODIFY_TWEET_RECEIVE_FLAG        ]	=	&RelationServiceProtocol::EnCode__ModifyTweetReceiveFlag;
	m_mapDeCodeFunc[G_R_MODIFY_TWEET_RECEIVE_FLAG        ]	=	&RelationServiceProtocol::DeCode__ModifyTweetReceiveFlag;

	m_mapEnCodeFunc[R_G_FRIEND_DEGREE_GAIN_EMAIL         ]	=	&RelationServiceProtocol::EnCode__FriendDegreeGainEmail;
	m_mapDeCodeFunc[R_G_FRIEND_DEGREE_GAIN_EMAIL         ]	=	&RelationServiceProtocol::DeCode__FriendDegreeGainEmail;

	m_mapEnCodeFunc[G_R_QUERY_NICKUNION                  ]	=	&RelationServiceProtocol::EnCode__GRQueryNickUnion;
	m_mapDeCodeFunc[G_R_QUERY_NICKUNION                  ]	=	&RelationServiceProtocol::DeCode__GRQueryNickUnion;

	m_mapEnCodeFunc[R_G_QUERY_NICKUNION_RST              ]	=	&RelationServiceProtocol::EnCode__RGQueryNickUnionRst;
	m_mapDeCodeFunc[R_G_QUERY_NICKUNION_RST              ]	=	&RelationServiceProtocol::DeCode__RGQueryNickUnionRst;

}

RelationServiceNS::RelationServiceProtocol::~RelationServiceProtocol()
{
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

RelationServiceNS::EnCodeFunc	RelationServiceNS::RelationServiceProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

RelationServiceNS::DeCodeFunc	RelationServiceNS::RelationServiceProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__G_R_DelPlayer(void* pData)
{
	G_R_DelPlayer* pkG_R_DelPlayer = (G_R_DelPlayer*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkG_R_DelPlayer->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__G_R_DelPlayer(void* pData)
{
	G_R_DelPlayer* pkG_R_DelPlayer = (G_R_DelPlayer*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkG_R_DelPlayer->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RPlayerID(void* pData)
{
	RPlayerID* pkRPlayerID = (RPlayerID*)(pData);

	//EnCode GateSrvID
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRPlayerID->GateSrvID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PPlayer
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRPlayerID->PPlayer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RPlayerID(void* pData)
{
	RPlayerID* pkRPlayerID = (RPlayerID*)(pData);

	//DeCode GateSrvID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRPlayerID->GateSrvID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PPlayer
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRPlayerID->PPlayer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__FriendGroupInfo(void* pData)
{
	FriendGroupInfo* pkFriendGroupInfo = (FriendGroupInfo*)(pData);

	//EnCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendGroupInfo->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendGroupInfo->GroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupName
	if((int)pkFriendGroupInfo->GroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkFriendGroupInfo->GroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendGroupInfo->GroupNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendGroupInfo->GroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__FriendGroupInfo(void* pData)
{
	FriendGroupInfo* pkFriendGroupInfo = (FriendGroupInfo*)(pData);

	//DeCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendGroupInfo->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendGroupInfo->GroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupName
	if((int)pkFriendGroupInfo->GroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkFriendGroupInfo->GroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendGroupInfo->GroupNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendGroupInfo->GroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__NameInfo(void* pData)
{
	NameInfo* pkNameInfo = (NameInfo*)(pData);

	//EnCode NameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNameInfo->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkNameInfo->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkNameInfo->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkNameInfo->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkNameInfo->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__NameInfo(void* pData)
{
	NameInfo* pkNameInfo = (NameInfo*)(pData);

	//DeCode NameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNameInfo->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkNameInfo->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkNameInfo->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkNameInfo->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkNameInfo->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__NameUnionInfo(void* pData)
{
	NameUnionInfo* pkNameUnionInfo = (NameUnionInfo*)(pData);

	//EnCode NickName
	if(EnCode__NameInfo(&(pkNameUnionInfo->NickName)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode UnionName
	if(EnCode__NameInfo(&(pkNameUnionInfo->UnionName)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__NameUnionInfo(void* pData)
{
	NameUnionInfo* pkNameUnionInfo = (NameUnionInfo*)(pData);

	//DeCode NickName
	if(DeCode__NameInfo(&(pkNameUnionInfo->NickName)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode UnionName
	if(DeCode__NameInfo(&(pkNameUnionInfo->UnionName)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__FriendInfo(void* pData)
{
	FriendInfo* pkFriendInfo = (FriendInfo*)(pData);

	//EnCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NickNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->NickNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NickName
	if((int)pkFriendInfo->NickNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkFriendInfo->NickNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->NickNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->NickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Flag
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->Flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode EnterModel
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->EnterModel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if((int)pkFriendInfo->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkFriendInfo->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if((int)pkFriendInfo->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkFriendInfo->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode degree
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->degree), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tweetsFlag
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->tweetsFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__FriendInfo(void* pData)
{
	FriendInfo* pkFriendInfo = (FriendInfo*)(pData);

	//DeCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NickNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->NickNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NickName
	if((int)pkFriendInfo->NickNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkFriendInfo->NickNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->NickNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->NickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Flag
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->Flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode EnterModel
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->EnterModel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if((int)pkFriendInfo->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkFriendInfo->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if((int)pkFriendInfo->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkFriendInfo->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode degree
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->degree), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tweetsFlag
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->tweetsFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRUplineInfo(void* pData)
{
	GRUplineInfo* pkGRUplineInfo = (GRUplineInfo*)(pData);

	//EnCode player
	if(EnCode__RPlayerID(&(pkGRUplineInfo->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode EnterModel
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRUplineInfo->EnterModel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRUplineInfo->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRUplineInfo->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Sex
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRUplineInfo->Sex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRUplineInfo->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRUplineInfo->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRUplineInfo->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRUplineInfo->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRUplineInfo->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkGRUplineInfo->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRUplineInfo->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRUplineInfo->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRUplineInfo->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRUplineInfo->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if((int)pkGRUplineInfo->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRUplineInfo->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRUplineInfo->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRUplineInfo->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRUplineInfo->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if((int)pkGRUplineInfo->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRUplineInfo->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRUplineInfo->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRUplineInfo->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRUplineInfo(void* pData)
{
	GRUplineInfo* pkGRUplineInfo = (GRUplineInfo*)(pData);

	//DeCode player
	if(DeCode__RPlayerID(&(pkGRUplineInfo->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode EnterModel
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRUplineInfo->EnterModel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRUplineInfo->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRUplineInfo->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Sex
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRUplineInfo->Sex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRUplineInfo->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRUplineInfo->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRUplineInfo->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRUplineInfo->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRUplineInfo->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkGRUplineInfo->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRUplineInfo->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRUplineInfo->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRUplineInfo->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRUplineInfo->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if((int)pkGRUplineInfo->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRUplineInfo->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRUplineInfo->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRUplineInfo->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRUplineInfo->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if((int)pkGRUplineInfo->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRUplineInfo->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRUplineInfo->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRUplineInfo->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGFriendGroups(void* pData)
{
	RGFriendGroups* pkRGFriendGroups = (RGFriendGroups*)(pData);

	//EnCode player
	if(EnCode__RPlayerID(&(pkRGFriendGroups->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode Remanent
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGFriendGroups->Remanent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode validSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendGroups->validSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendGroupsLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendGroups->friendGroupsLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendGroups
	if((int)pkRGFriendGroups->friendGroupsLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkRGFriendGroups->friendGroupsLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkRGFriendGroups->friendGroupsLen; ++i)
	{
		if(EnCode__FriendGroupInfo(&(pkRGFriendGroups->friendGroups[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGFriendGroups(void* pData)
{
	RGFriendGroups* pkRGFriendGroups = (RGFriendGroups*)(pData);

	//DeCode player
	if(DeCode__RPlayerID(&(pkRGFriendGroups->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode Remanent
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGFriendGroups->Remanent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode validSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendGroups->validSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendGroupsLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendGroups->friendGroupsLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendGroups
	if((int)pkRGFriendGroups->friendGroupsLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkRGFriendGroups->friendGroupsLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkRGFriendGroups->friendGroupsLen; ++i)
	{
		if(DeCode__FriendGroupInfo(&(pkRGFriendGroups->friendGroups[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGFriendInfos(void* pData)
{
	RGFriendInfos* pkRGFriendInfos = (RGFriendInfos*)(pData);

	//EnCode player
	if(EnCode__RPlayerID(&(pkRGFriendInfos->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode validSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendInfos->validSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendinfosLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendInfos->friendinfosLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendinfos
	if((int)pkRGFriendInfos->friendinfosLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(5 < pkRGFriendInfos->friendinfosLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkRGFriendInfos->friendinfosLen; ++i)
	{
		if(EnCode__FriendInfo(&(pkRGFriendInfos->friendinfos[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGFriendInfos(void* pData)
{
	RGFriendInfos* pkRGFriendInfos = (RGFriendInfos*)(pData);

	//DeCode player
	if(DeCode__RPlayerID(&(pkRGFriendInfos->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode validSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendInfos->validSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendinfosLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendInfos->friendinfosLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendinfos
	if((int)pkRGFriendInfos->friendinfosLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(5 < pkRGFriendInfos->friendinfosLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkRGFriendInfos->friendinfosLen; ++i)
	{
		if(DeCode__FriendInfo(&(pkRGFriendInfos->friendinfos[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRInfoUpdate(void* pData)
{
	GRInfoUpdate* pkGRInfoUpdate = (GRInfoUpdate*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRInfoUpdate->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRInfoUpdate->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRInfoUpdate->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRInfoUpdate->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRInfoUpdate->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRInfoUpdate->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRInfoUpdate->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if((int)pkGRInfoUpdate->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRInfoUpdate->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRInfoUpdate->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRInfoUpdate->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRInfoUpdate->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if((int)pkGRInfoUpdate->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRInfoUpdate->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRInfoUpdate->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRInfoUpdate->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Online
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRInfoUpdate->Online), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRInfoUpdate(void* pData)
{
	GRInfoUpdate* pkGRInfoUpdate = (GRInfoUpdate*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRInfoUpdate->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRInfoUpdate->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRInfoUpdate->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRInfoUpdate->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRInfoUpdate->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRInfoUpdate->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRInfoUpdate->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if((int)pkGRInfoUpdate->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRInfoUpdate->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRInfoUpdate->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRInfoUpdate->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRInfoUpdate->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if((int)pkGRInfoUpdate->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRInfoUpdate->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRInfoUpdate->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRInfoUpdate->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Online
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRInfoUpdate->Online), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGFriendInfoUpdate(void* pData)
{
	RGFriendInfoUpdate* pkRGFriendInfoUpdate = (RGFriendInfoUpdate*)(pData);

	//EnCode player
	if(EnCode__RPlayerID(&(pkRGFriendInfoUpdate->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendInfoUpdate->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRGFriendInfoUpdate->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRGFriendInfoUpdate->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGFriendInfoUpdate->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRGFriendInfoUpdate->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRGFriendInfoUpdate->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendInfoUpdate->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if((int)pkRGFriendInfoUpdate->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkRGFriendInfoUpdate->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRGFriendInfoUpdate->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkRGFriendInfoUpdate->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendInfoUpdate->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if((int)pkRGFriendInfoUpdate->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkRGFriendInfoUpdate->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRGFriendInfoUpdate->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkRGFriendInfoUpdate->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Online
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGFriendInfoUpdate->Online), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode degree
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendInfoUpdate->degree), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tweetsFlag
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGFriendInfoUpdate->tweetsFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGFriendInfoUpdate(void* pData)
{
	RGFriendInfoUpdate* pkRGFriendInfoUpdate = (RGFriendInfoUpdate*)(pData);

	//DeCode player
	if(DeCode__RPlayerID(&(pkRGFriendInfoUpdate->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendInfoUpdate->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRGFriendInfoUpdate->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRGFriendInfoUpdate->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGFriendInfoUpdate->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRGFriendInfoUpdate->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRGFriendInfoUpdate->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendInfoUpdate->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if((int)pkRGFriendInfoUpdate->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkRGFriendInfoUpdate->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRGFriendInfoUpdate->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRGFriendInfoUpdate->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendInfoUpdate->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if((int)pkRGFriendInfoUpdate->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkRGFriendInfoUpdate->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRGFriendInfoUpdate->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRGFriendInfoUpdate->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Online
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGFriendInfoUpdate->Online), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode degree
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendInfoUpdate->degree), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tweetsFlag
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGFriendInfoUpdate->tweetsFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGNotifyRGroupMsgNum(void* pData)
{
	RGNotifyRGroupMsgNum* pkRGNotifyRGroupMsgNum = (RGNotifyRGroupMsgNum*)(pData);

	//EnCode player
	if(EnCode__RPlayerID(&(pkRGNotifyRGroupMsgNum->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode Remanent
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGNotifyRGroupMsgNum->Remanent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGNotifyRGroupMsgNum(void* pData)
{
	RGNotifyRGroupMsgNum* pkRGNotifyRGroupMsgNum = (RGNotifyRGroupMsgNum*)(pData);

	//DeCode player
	if(DeCode__RPlayerID(&(pkRGNotifyRGroupMsgNum->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode Remanent
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGNotifyRGroupMsgNum->Remanent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__BlacklistItem(void* pData)
{
	BlacklistItem* pkBlacklistItem = (BlacklistItem*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBlacklistItem->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBlacklistItem->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkBlacklistItem->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkBlacklistItem->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkBlacklistItem->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkBlacklistItem->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__BlacklistItem(void* pData)
{
	BlacklistItem* pkBlacklistItem = (BlacklistItem*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBlacklistItem->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBlacklistItem->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkBlacklistItem->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkBlacklistItem->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkBlacklistItem->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkBlacklistItem->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGBlacklist(void* pData)
{
	RGBlacklist* pkRGBlacklist = (RGBlacklist*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGBlacklist->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode validSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGBlacklist->validSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode BlacklistLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGBlacklist->BlacklistLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Blacklist
	if((int)pkRGBlacklist->BlacklistLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkRGBlacklist->BlacklistLen)
	{
		return FAILEDRETCODE;
	}

	for(CHAR i = 0; i < pkRGBlacklist->BlacklistLen; ++i)
	{
		if(EnCode__BlacklistItem(&(pkRGBlacklist->Blacklist[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGBlacklist(void* pData)
{
	RGBlacklist* pkRGBlacklist = (RGBlacklist*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGBlacklist->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode validSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGBlacklist->validSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode BlacklistLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGBlacklist->BlacklistLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Blacklist
	if((int)pkRGBlacklist->BlacklistLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkRGBlacklist->BlacklistLen)
	{
		return FAILEDRETCODE;
	}

	for(CHAR i = 0; i < pkRGBlacklist->BlacklistLen; ++i)
	{
		if(DeCode__BlacklistItem(&(pkRGBlacklist->Blacklist[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddFriendError(void* pData)
{
	AddFriendError* pkAddFriendError = (AddFriendError*)(pData);

	//EnCode ErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddFriendError->ErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkAddFriendError->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddFriendError(void* pData)
{
	AddFriendError* pkAddFriendError = (AddFriendError*)(pData);

	//DeCode ErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddFriendError->ErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkAddFriendError->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddFriendSuc(void* pData)
{
	AddFriendSuc* pkAddFriendSuc = (AddFriendSuc*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkAddFriendSuc->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode friendinfo
	if(EnCode__FriendInfo(&(pkAddFriendSuc->friendinfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddFriendSuc(void* pData)
{
	AddFriendSuc* pkAddFriendSuc = (AddFriendSuc*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkAddFriendSuc->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode friendinfo
	if(DeCode__FriendInfo(&(pkAddFriendSuc->friendinfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddFriend(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__AddFriendError(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__AddFriendSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddFriend(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__AddFriendError(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__AddFriendSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGAddFriend(void* pData)
{
	RGAddFriend* pkRGAddFriend = (RGAddFriend*)(pData);

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGAddFriend->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode addFriend
	if(EnCode__AddFriend(&(pkRGAddFriend->addFriend), pkRGAddFriend->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGAddFriend(void* pData)
{
	RGAddFriend* pkRGAddFriend = (RGAddFriend*)(pData);

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGAddFriend->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode addFriend
	if(DeCode__AddFriend(&(pkRGAddFriend->addFriend), pkRGAddFriend->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRAddFriend(void* pData)
{
	GRAddFriend* pkGRAddFriend = (GRAddFriend*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddFriend->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ByAdduiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddFriend->ByAdduiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddFriend->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRAddFriend(void* pData)
{
	GRAddFriend* pkGRAddFriend = (GRAddFriend*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddFriend->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ByAdduiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddFriend->ByAdduiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddFriend->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRDelFriend(void* pData)
{
	GRDelFriend* pkGRDelFriend = (GRDelFriend*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRDelFriend->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ByAdduiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRDelFriend->ByAdduiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRDelFriend(void* pData)
{
	GRDelFriend* pkGRDelFriend = (GRDelFriend*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRDelFriend->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ByAdduiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRDelFriend->ByAdduiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelFriendSuc(void* pData)
{
	DelFriendSuc* pkDelFriendSuc = (DelFriendSuc*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkDelFriendSuc->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIDByDell
	if(EnCode__FriendInfo(&(pkDelFriendSuc->playerIDByDell)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelFriendSuc(void* pData)
{
	DelFriendSuc* pkDelFriendSuc = (DelFriendSuc*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkDelFriendSuc->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIDByDell
	if(DeCode__FriendInfo(&(pkDelFriendSuc->playerIDByDell)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelFriend(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__AddFriendError(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__DelFriendSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelFriend(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__AddFriendError(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__DelFriendSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGDelFriend(void* pData)
{
	RGDelFriend* pkRGDelFriend = (RGDelFriend*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGDelFriend->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGDelFriend->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode delfriend
	if(EnCode__DelFriend(&(pkRGDelFriend->delfriend), pkRGDelFriend->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGDelFriend(void* pData)
{
	RGDelFriend* pkRGDelFriend = (RGDelFriend*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGDelFriend->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGDelFriend->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode delfriend
	if(DeCode__DelFriend(&(pkRGDelFriend->delfriend), pkRGDelFriend->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRAddGroup(void* pData)
{
	GRAddGroup* pkGRAddGroup = (GRAddGroup*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddGroup->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupNameLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGRAddGroup->GroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupName
	if((int)pkGRAddGroup->GroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddGroup->GroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddGroup->GroupNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRAddGroup->GroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRAddGroup(void* pData)
{
	GRAddGroup* pkGRAddGroup = (GRAddGroup*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddGroup->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGRAddGroup->GroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupName
	if((int)pkGRAddGroup->GroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddGroup->GroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddGroup->GroupNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRAddGroup->GroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddGroupErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddGroupErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddGroupSuc(void* pData)
{
	AddGroupSuc* pkAddGroupSuc = (AddGroupSuc*)(pData);

	//EnCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkAddGroupSuc->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupNameLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkAddGroupSuc->GroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupName
	if((int)pkAddGroupSuc->GroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddGroupSuc->GroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddGroupSuc->GroupNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddGroupSuc->GroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddGroupSuc(void* pData)
{
	AddGroupSuc* pkAddGroupSuc = (AddGroupSuc*)(pData);

	//DeCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkAddGroupSuc->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkAddGroupSuc->GroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupName
	if((int)pkAddGroupSuc->GroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddGroupSuc->GroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddGroupSuc->GroupNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddGroupSuc->GroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddGroupResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__AddGroupErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__AddGroupSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddGroupResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__AddGroupErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__AddGroupSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGAddGroup(void* pData)
{
	RGAddGroup* pkRGAddGroup = (RGAddGroup*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGAddGroup->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGAddGroup->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__AddGroupResult(&(pkRGAddGroup->result), pkRGAddGroup->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGAddGroup(void* pData)
{
	RGAddGroup* pkRGAddGroup = (RGAddGroup*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGAddGroup->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGAddGroup->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__AddGroupResult(&(pkRGAddGroup->result), pkRGAddGroup->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRFriendAlterGroupID(void* pData)
{
	GRFriendAlterGroupID* pkGRFriendAlterGroupID = (GRFriendAlterGroupID*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRFriendAlterGroupID->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FriendID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRFriendAlterGroupID->FriendID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode preGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRFriendAlterGroupID->preGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRFriendAlterGroupID->newGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRFriendAlterGroupID(void* pData)
{
	GRFriendAlterGroupID* pkGRFriendAlterGroupID = (GRFriendAlterGroupID*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRFriendAlterGroupID->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FriendID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRFriendAlterGroupID->FriendID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode preGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRFriendAlterGroupID->preGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRFriendAlterGroupID->newGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AlterGroupIDErr(void* pData)
{
	AlterGroupIDErr* pkAlterGroupIDErr = (AlterGroupIDErr*)(pData);

	//EnCode err
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAlterGroupIDErr->err), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AlterGroupIDErr(void* pData)
{
	AlterGroupIDErr* pkAlterGroupIDErr = (AlterGroupIDErr*)(pData);

	//DeCode err
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAlterGroupIDErr->err), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AlterGroupIDSuc(void* pData)
{
	AlterGroupIDSuc* pkAlterGroupIDSuc = (AlterGroupIDSuc*)(pData);

	//EnCode FriendID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAlterGroupIDSuc->FriendID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode preGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAlterGroupIDSuc->preGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAlterGroupIDSuc->newGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AlterGroupIDSuc(void* pData)
{
	AlterGroupIDSuc* pkAlterGroupIDSuc = (AlterGroupIDSuc*)(pData);

	//DeCode FriendID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAlterGroupIDSuc->FriendID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode preGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAlterGroupIDSuc->preGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAlterGroupIDSuc->newGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AlterGroupResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__AlterGroupIDErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__AlterGroupIDSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AlterGroupResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__AlterGroupIDErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__AlterGroupIDSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGFriendAlterGroupID(void* pData)
{
	RGFriendAlterGroupID* pkRGFriendAlterGroupID = (RGFriendAlterGroupID*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGFriendAlterGroupID->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGFriendAlterGroupID->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__AlterGroupResult(&(pkRGFriendAlterGroupID->result), pkRGFriendAlterGroupID->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGFriendAlterGroupID(void* pData)
{
	RGFriendAlterGroupID* pkRGFriendAlterGroupID = (RGFriendAlterGroupID*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGFriendAlterGroupID->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGFriendAlterGroupID->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__AlterGroupResult(&(pkRGFriendAlterGroupID->result), pkRGFriendAlterGroupID->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRDelGroup(void* pData)
{
	GRDelGroup* pkGRDelGroup = (GRDelGroup*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRDelGroup->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRDelGroup->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRDelGroup(void* pData)
{
	GRDelGroup* pkGRDelGroup = (GRDelGroup*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRDelGroup->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRDelGroup->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelGroupErr(void* pData)
{
	DelGroupErr* pkDelGroupErr = (DelGroupErr*)(pData);

	//EnCode err
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDelGroupErr->err), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelGroupErr(void* pData)
{
	DelGroupErr* pkDelGroupErr = (DelGroupErr*)(pData);

	//DeCode err
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDelGroupErr->err), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelGroupSuc(void* pData)
{
	DelGroupSuc* pkDelGroupSuc = (DelGroupSuc*)(pData);

	//EnCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDelGroupSuc->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelGroupSuc(void* pData)
{
	DelGroupSuc* pkDelGroupSuc = (DelGroupSuc*)(pData);

	//DeCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDelGroupSuc->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelGroupResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__DelGroupErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__DelGroupSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelGroupResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__DelGroupErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__DelGroupSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGDelGroup(void* pData)
{
	RGDelGroup* pkRGDelGroup = (RGDelGroup*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGDelGroup->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGDelGroup->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__DelGroupResult(&(pkRGDelGroup->result), pkRGDelGroup->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGDelGroup(void* pData)
{
	RGDelGroup* pkRGDelGroup = (RGDelGroup*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGDelGroup->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGDelGroup->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__DelGroupResult(&(pkRGDelGroup->result), pkRGDelGroup->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRAlterGroupName(void* pData)
{
	GRAlterGroupName* pkGRAlterGroupName = (GRAlterGroupName*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAlterGroupName->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAlterGroupName->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newGroupNameLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGRAlterGroupName->newGroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newGroupName
	if((int)pkGRAlterGroupName->newGroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAlterGroupName->newGroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAlterGroupName->newGroupNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRAlterGroupName->newGroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRAlterGroupName(void* pData)
{
	GRAlterGroupName* pkGRAlterGroupName = (GRAlterGroupName*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAlterGroupName->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAlterGroupName->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newGroupNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGRAlterGroupName->newGroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newGroupName
	if((int)pkGRAlterGroupName->newGroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAlterGroupName->newGroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAlterGroupName->newGroupNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRAlterGroupName->newGroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AlterGroupNameErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AlterGroupNameErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AlterGroupNameSuc(void* pData)
{
	AlterGroupNameSuc* pkAlterGroupNameSuc = (AlterGroupNameSuc*)(pData);

	//EnCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAlterGroupNameSuc->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newGroupNameLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkAlterGroupNameSuc->newGroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newGroupName
	if((int)pkAlterGroupNameSuc->newGroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAlterGroupNameSuc->newGroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAlterGroupNameSuc->newGroupNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAlterGroupNameSuc->newGroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AlterGroupNameSuc(void* pData)
{
	AlterGroupNameSuc* pkAlterGroupNameSuc = (AlterGroupNameSuc*)(pData);

	//DeCode GroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAlterGroupNameSuc->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newGroupNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkAlterGroupNameSuc->newGroupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newGroupName
	if((int)pkAlterGroupNameSuc->newGroupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAlterGroupNameSuc->newGroupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAlterGroupNameSuc->newGroupNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAlterGroupNameSuc->newGroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AlterGroupNameResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__AlterGroupNameErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__AlterGroupNameSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AlterGroupNameResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__AlterGroupNameErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__AlterGroupNameSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGAlterGroupName(void* pData)
{
	RGAlterGroupName* pkRGAlterGroupName = (RGAlterGroupName*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGAlterGroupName->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGAlterGroupName->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__AlterGroupNameResult(&(pkRGAlterGroupName->result), pkRGAlterGroupName->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGAlterGroupName(void* pData)
{
	RGAlterGroupName* pkRGAlterGroupName = (RGAlterGroupName*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGAlterGroupName->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGAlterGroupName->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__AlterGroupNameResult(&(pkRGAlterGroupName->result), pkRGAlterGroupName->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRAddToBlacklist(void* pData)
{
	GRAddToBlacklist* pkGRAddToBlacklist = (GRAddToBlacklist*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddToBlacklist->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddToBlacklist->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkGRAddToBlacklist->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddToBlacklist->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddToBlacklist->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRAddToBlacklist->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRAddToBlacklist(void* pData)
{
	GRAddToBlacklist* pkGRAddToBlacklist = (GRAddToBlacklist*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddToBlacklist->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddToBlacklist->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkGRAddToBlacklist->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddToBlacklist->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddToBlacklist->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRAddToBlacklist->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddToBlacklistErr(void* pData)
{
	AddToBlacklistErr* pkAddToBlacklistErr = (AddToBlacklistErr*)(pData);

	//EnCode err
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddToBlacklistErr->err), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddToBlacklistErr(void* pData)
{
	AddToBlacklistErr* pkAddToBlacklistErr = (AddToBlacklistErr*)(pData);

	//DeCode err
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddToBlacklistErr->err), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddToBlacklistSuc(void* pData)
{
	AddToBlacklistSuc* pkAddToBlacklistSuc = (AddToBlacklistSuc*)(pData);

	//EnCode NameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddToBlacklistSuc->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkAddToBlacklistSuc->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddToBlacklistSuc->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddToBlacklistSuc->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddToBlacklistSuc->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddToBlacklistSuc(void* pData)
{
	AddToBlacklistSuc* pkAddToBlacklistSuc = (AddToBlacklistSuc*)(pData);

	//DeCode NameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddToBlacklistSuc->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkAddToBlacklistSuc->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddToBlacklistSuc->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddToBlacklistSuc->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddToBlacklistSuc->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddToBlacklistResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__AddToBlacklistErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__AddToBlacklistSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddToBlacklistResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__AddToBlacklistErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__AddToBlacklistSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGAddToBlacklist(void* pData)
{
	RGAddToBlacklist* pkRGAddToBlacklist = (RGAddToBlacklist*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGAddToBlacklist->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGAddToBlacklist->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__AddToBlacklistResult(&(pkRGAddToBlacklist->result), pkRGAddToBlacklist->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGAddToBlacklist(void* pData)
{
	RGAddToBlacklist* pkRGAddToBlacklist = (RGAddToBlacklist*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGAddToBlacklist->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGAddToBlacklist->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__AddToBlacklistResult(&(pkRGAddToBlacklist->result), pkRGAddToBlacklist->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRDelFromBlacklist(void* pData)
{
	GRDelFromBlacklist* pkGRDelFromBlacklist = (GRDelFromBlacklist*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRDelFromBlacklist->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRDelFromBlacklist->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkGRDelFromBlacklist->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRDelFromBlacklist->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRDelFromBlacklist->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRDelFromBlacklist->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRDelFromBlacklist(void* pData)
{
	GRDelFromBlacklist* pkGRDelFromBlacklist = (GRDelFromBlacklist*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRDelFromBlacklist->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRDelFromBlacklist->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkGRDelFromBlacklist->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRDelFromBlacklist->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRDelFromBlacklist->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRDelFromBlacklist->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelFromBlacklistErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelFromBlacklistErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelFromBlacklistSuc(void* pData)
{
	DelFromBlacklistSuc* pkDelFromBlacklistSuc = (DelFromBlacklistSuc*)(pData);

	//EnCode NameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDelFromBlacklistSuc->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkDelFromBlacklistSuc->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkDelFromBlacklistSuc->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkDelFromBlacklistSuc->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkDelFromBlacklistSuc->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelFromBlacklistSuc(void* pData)
{
	DelFromBlacklistSuc* pkDelFromBlacklistSuc = (DelFromBlacklistSuc*)(pData);

	//DeCode NameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDelFromBlacklistSuc->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkDelFromBlacklistSuc->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkDelFromBlacklistSuc->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkDelFromBlacklistSuc->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkDelFromBlacklistSuc->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelFromBlacklistResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__DelFromBlacklistErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__DelFromBlacklistSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelFromBlacklistResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__DelFromBlacklistErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__DelFromBlacklistSuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGDelFromBlackList(void* pData)
{
	RGDelFromBlackList* pkRGDelFromBlackList = (RGDelFromBlackList*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGDelFromBlackList->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGDelFromBlackList->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__DelFromBlacklistResult(&(pkRGDelFromBlackList->result), pkRGDelFromBlackList->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGDelFromBlackList(void* pData)
{
	RGDelFromBlackList* pkRGDelFromBlackList = (RGDelFromBlackList*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGDelFromBlackList->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGDelFromBlackList->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__DelFromBlacklistResult(&(pkRGDelFromBlackList->result), pkRGDelFromBlackList->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRAddEnemy(void* pData)
{
	GRAddEnemy* pkGRAddEnemy = (GRAddEnemy*)(pData);

	//EnCode player
	if(EnCode__RPlayerID(&(pkGRAddEnemy->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRAddEnemy->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddEnemy->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Sex
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRAddEnemy->Sex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRAddEnemy->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRAddEnemy->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRAddEnemy->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRAddEnemy->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddEnemy->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if((int)pkGRAddEnemy->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddEnemy->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddEnemy->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRAddEnemy->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddEnemy->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if((int)pkGRAddEnemy->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddEnemy->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddEnemy->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRAddEnemy->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddEnemy->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkGRAddEnemy->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddEnemy->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddEnemy->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRAddEnemy->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode addtouiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRAddEnemy->addtouiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRAddEnemy->posX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGRAddEnemy->posY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRAddEnemy(void* pData)
{
	GRAddEnemy* pkGRAddEnemy = (GRAddEnemy*)(pData);

	//DeCode player
	if(DeCode__RPlayerID(&(pkGRAddEnemy->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRAddEnemy->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddEnemy->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Sex
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRAddEnemy->Sex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRAddEnemy->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRAddEnemy->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRAddEnemy->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRAddEnemy->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddEnemy->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if((int)pkGRAddEnemy->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddEnemy->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddEnemy->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRAddEnemy->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddEnemy->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if((int)pkGRAddEnemy->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddEnemy->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddEnemy->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRAddEnemy->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddEnemy->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkGRAddEnemy->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGRAddEnemy->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRAddEnemy->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRAddEnemy->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode addtouiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRAddEnemy->addtouiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRAddEnemy->posX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGRAddEnemy->posY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddEnemyErr(void* pData)
{
	AddEnemyErr* pkAddEnemyErr = (AddEnemyErr*)(pData);

	//EnCode err
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddEnemyErr->err), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddEnemyErr(void* pData)
{
	AddEnemyErr* pkAddEnemyErr = (AddEnemyErr*)(pData);

	//DeCode err
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddEnemyErr->err), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddEnemySuc(void* pData)
{
	AddEnemySuc* pkAddEnemySuc = (AddEnemySuc*)(pData);

	//EnCode player
	if(EnCode__RPlayerID(&(pkAddEnemySuc->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddEnemySuc->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddEnemySuc->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Sex
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkAddEnemySuc->Sex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddEnemySuc->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkAddEnemySuc->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddEnemySuc->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddEnemySuc->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddEnemySuc->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if((int)pkAddEnemySuc->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddEnemySuc->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddEnemySuc->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddEnemySuc->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddEnemySuc->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if((int)pkAddEnemySuc->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddEnemySuc->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddEnemySuc->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddEnemySuc->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddEnemySuc->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkAddEnemySuc->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddEnemySuc->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddEnemySuc->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddEnemySuc->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Replace_uiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddEnemySuc->Replace_uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddEnemySuc(void* pData)
{
	AddEnemySuc* pkAddEnemySuc = (AddEnemySuc*)(pData);

	//DeCode player
	if(DeCode__RPlayerID(&(pkAddEnemySuc->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddEnemySuc->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddEnemySuc->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Sex
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkAddEnemySuc->Sex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddEnemySuc->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkAddEnemySuc->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddEnemySuc->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddEnemySuc->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddEnemySuc->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if((int)pkAddEnemySuc->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddEnemySuc->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddEnemySuc->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddEnemySuc->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddEnemySuc->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if((int)pkAddEnemySuc->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddEnemySuc->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddEnemySuc->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddEnemySuc->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddEnemySuc->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkAddEnemySuc->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkAddEnemySuc->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddEnemySuc->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddEnemySuc->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Replace_uiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddEnemySuc->Replace_uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddEnemyResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__AddEnemyErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__AddEnemySuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddEnemyResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__AddEnemyErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__AddEnemySuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGAddEnemy(void* pData)
{
	RGAddEnemy* pkRGAddEnemy = (RGAddEnemy*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGAddEnemy->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGAddEnemy->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__AddEnemyResult(&(pkRGAddEnemy->result), pkRGAddEnemy->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGAddEnemy(void* pData)
{
	RGAddEnemy* pkRGAddEnemy = (RGAddEnemy*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGAddEnemy->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGAddEnemy->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__AddEnemyResult(&(pkRGAddEnemy->result), pkRGAddEnemy->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRLockEnemy(void* pData)
{
	GRLockEnemy* pkGRLockEnemy = (GRLockEnemy*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRLockEnemy->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode IsLock
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRLockEnemy->IsLock), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode LockuiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRLockEnemy->LockuiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRLockEnemy(void* pData)
{
	GRLockEnemy* pkGRLockEnemy = (GRLockEnemy*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRLockEnemy->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode IsLock
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRLockEnemy->IsLock), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode LockuiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRLockEnemy->LockuiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__LockEnemyErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__LockEnemyErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__LockEnemySuc(void* pData)
{
	LockEnemySuc* pkLockEnemySuc = (LockEnemySuc*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLockEnemySuc->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__LockEnemySuc(void* pData)
{
	LockEnemySuc* pkLockEnemySuc = (LockEnemySuc*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLockEnemySuc->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__LockEnemyResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__LockEnemyErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__LockEnemySuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__LockEnemyResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__LockEnemyErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__LockEnemySuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGLockEnemy(void* pData)
{
	RGLockEnemy* pkRGLockEnemy = (RGLockEnemy*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGLockEnemy->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode IsLock
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGLockEnemy->IsLock), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGLockEnemy->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__LockEnemyResult(&(pkRGLockEnemy->result), pkRGLockEnemy->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGLockEnemy(void* pData)
{
	RGLockEnemy* pkRGLockEnemy = (RGLockEnemy*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGLockEnemy->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode IsLock
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGLockEnemy->IsLock), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGLockEnemy->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__LockEnemyResult(&(pkRGLockEnemy->result), pkRGLockEnemy->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRDelEnemy(void* pData)
{
	GRDelEnemy* pkGRDelEnemy = (GRDelEnemy*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRDelEnemy->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode deluiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRDelEnemy->deluiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRDelEnemy(void* pData)
{
	GRDelEnemy* pkGRDelEnemy = (GRDelEnemy*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRDelEnemy->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode deluiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRDelEnemy->deluiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelEnemyErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelEnemyErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelEnemySuc(void* pData)
{
	DelEnemySuc* pkDelEnemySuc = (DelEnemySuc*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDelEnemySuc->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelEnemySuc(void* pData)
{
	DelEnemySuc* pkDelEnemySuc = (DelEnemySuc*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDelEnemySuc->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DelEnemyResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__DelEnemyErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__DelEnemySuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DelEnemyResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__DelEnemyErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__DelEnemySuc(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGDelEnemy(void* pData)
{
	RGDelEnemy* pkRGDelEnemy = (RGDelEnemy*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGDelEnemy->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGDelEnemy->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__DelEnemyResult(&(pkRGDelEnemy->result), pkRGDelEnemy->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGDelEnemy(void* pData)
{
	RGDelEnemy* pkRGDelEnemy = (RGDelEnemy*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGDelEnemy->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGDelEnemy->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__DelEnemyResult(&(pkRGDelEnemy->result), pkRGDelEnemy->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__EnemylistItem(void* pData)
{
	EnemylistItem* pkEnemylistItem = (EnemylistItem*)(pData);

	//EnCode player
	if(EnCode__RPlayerID(&(pkEnemylistItem->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkEnemylistItem->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkEnemylistItem->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Sex
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkEnemylistItem->Sex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Online
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkEnemylistItem->Online), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkEnemylistItem->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkEnemylistItem->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkEnemylistItem->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkEnemylistItem->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkEnemylistItem->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if((int)pkEnemylistItem->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkEnemylistItem->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkEnemylistItem->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkEnemylistItem->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkEnemylistItem->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if((int)pkEnemylistItem->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkEnemylistItem->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkEnemylistItem->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkEnemylistItem->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkEnemylistItem->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkEnemylistItem->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkEnemylistItem->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkEnemylistItem->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkEnemylistItem->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode IsLock
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkEnemylistItem->IsLock), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode incidentMap
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkEnemylistItem->incidentMap), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode incidentPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkEnemylistItem->incidentPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode incidentPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkEnemylistItem->incidentPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bKilled
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkEnemylistItem->bKilled), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode incidentTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkEnemylistItem->incidentTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__EnemylistItem(void* pData)
{
	EnemylistItem* pkEnemylistItem = (EnemylistItem*)(pData);

	//DeCode player
	if(DeCode__RPlayerID(&(pkEnemylistItem->player)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkEnemylistItem->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkEnemylistItem->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Sex
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkEnemylistItem->Sex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Online
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkEnemylistItem->Online), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkEnemylistItem->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkEnemylistItem->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkEnemylistItem->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkEnemylistItem->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkEnemylistItem->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if((int)pkEnemylistItem->GuildNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkEnemylistItem->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkEnemylistItem->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkEnemylistItem->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkEnemylistItem->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if((int)pkEnemylistItem->FereNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkEnemylistItem->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkEnemylistItem->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkEnemylistItem->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkEnemylistItem->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkEnemylistItem->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkEnemylistItem->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkEnemylistItem->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkEnemylistItem->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode IsLock
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkEnemylistItem->IsLock), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode incidentMap
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkEnemylistItem->incidentMap), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode incidentPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkEnemylistItem->incidentPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode incidentPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkEnemylistItem->incidentPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bKilled
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkEnemylistItem->bKilled), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode incidentTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkEnemylistItem->incidentTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGEnemylist(void* pData)
{
	RGEnemylist* pkRGEnemylist = (RGEnemylist*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGEnemylist->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode validSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGEnemylist->validSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode EnemylistLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGEnemylist->EnemylistLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Enemylist
	if((int)pkRGEnemylist->EnemylistLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(4 < pkRGEnemylist->EnemylistLen)
	{
		return FAILEDRETCODE;
	}

	for(CHAR i = 0; i < pkRGEnemylist->EnemylistLen; ++i)
	{
		if(EnCode__EnemylistItem(&(pkRGEnemylist->Enemylist[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGEnemylist(void* pData)
{
	RGEnemylist* pkRGEnemylist = (RGEnemylist*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGEnemylist->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode validSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGEnemylist->validSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode EnemylistLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGEnemylist->EnemylistLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Enemylist
	if((int)pkRGEnemylist->EnemylistLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(4 < pkRGEnemylist->EnemylistLen)
	{
		return FAILEDRETCODE;
	}

	for(CHAR i = 0; i < pkRGEnemylist->EnemylistLen; ++i)
	{
		if(DeCode__EnemylistItem(&(pkRGEnemylist->Enemylist[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGUpdateEnemy(void* pData)
{
	RGUpdateEnemy* pkRGUpdateEnemy = (RGUpdateEnemy*)(pData);

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGUpdateEnemy->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode Enemy
	if(EnCode__EnemylistItem(&(pkRGUpdateEnemy->Enemy)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGUpdateEnemy(void* pData)
{
	RGUpdateEnemy* pkRGUpdateEnemy = (RGUpdateEnemy*)(pData);

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGUpdateEnemy->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode Enemy
	if(DeCode__EnemylistItem(&(pkRGUpdateEnemy->Enemy)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRSendGroupMsg(void* pData)
{
	GRSendGroupMsg* pkGRSendGroupMsg = (GRSendGroupMsg*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRSendGroupMsg->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRSendGroupMsg->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MsgLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRSendGroupMsg->MsgLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Msg
	if((int)pkGRSendGroupMsg->MsgLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RMSSAGE_SIZE < pkGRSendGroupMsg->MsgLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRSendGroupMsg->MsgLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRSendGroupMsg->Msg), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRSendGroupMsg(void* pData)
{
	GRSendGroupMsg* pkGRSendGroupMsg = (GRSendGroupMsg*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRSendGroupMsg->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRSendGroupMsg->GroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MsgLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRSendGroupMsg->MsgLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Msg
	if((int)pkGRSendGroupMsg->MsgLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RMSSAGE_SIZE < pkGRSendGroupMsg->MsgLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRSendGroupMsg->MsgLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRSendGroupMsg->Msg), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__SendGroupMsgErr(void* pData)
{
	SendGroupMsgErr* pkSendGroupMsgErr = (SendGroupMsgErr*)(pData);

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSendGroupMsgErr->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__SendGroupMsgErr(void* pData)
{
	SendGroupMsgErr* pkSendGroupMsgErr = (SendGroupMsgErr*)(pData);

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSendGroupMsgErr->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GeneralMsg(void* pData)
{
	GeneralMsg* pkGeneralMsg = (GeneralMsg*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGeneralMsg->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGeneralMsg->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Name
	if((int)pkGeneralMsg->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGeneralMsg->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGeneralMsg->NameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGeneralMsg->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MsgLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGeneralMsg->MsgLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Msg
	if((int)pkGeneralMsg->MsgLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RMSSAGE_SIZE < pkGeneralMsg->MsgLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGeneralMsg->MsgLen;
	if(!m_kPackage.Pack("CHAR", &(pkGeneralMsg->Msg), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GeneralMsg(void* pData)
{
	GeneralMsg* pkGeneralMsg = (GeneralMsg*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGeneralMsg->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGeneralMsg->NameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Name
	if((int)pkGeneralMsg->NameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RNAME_SIZE < pkGeneralMsg->NameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGeneralMsg->NameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGeneralMsg->Name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MsgLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGeneralMsg->MsgLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Msg
	if((int)pkGeneralMsg->MsgLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RMSSAGE_SIZE < pkGeneralMsg->MsgLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGeneralMsg->MsgLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGeneralMsg->Msg), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__SendGroupMsgResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//EnCode Error
	case	R_G_RESULT_FAILED: return EnCode__SendGroupMsgErr(pData);
	//EnCode Suc
	case	R_G_RESULT_SUCCEED: return EnCode__GeneralMsg(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__SendGroupMsgResult(void* pData, int nSelectId)
{
	switch(nSelectId)
	{
	//DeCode Error
	case	R_G_RESULT_FAILED: return DeCode__SendGroupMsgErr(pData);
	//DeCode Suc
	case	R_G_RESULT_SUCCEED: return DeCode__GeneralMsg(pData);
	//Default
	default: return FAILEDRETCODE;
	}
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGSendGroupMsg(void* pData)
{
	RGSendGroupMsg* pkRGSendGroupMsg = (RGSendGroupMsg*)(pData);

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGSendGroupMsg->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGSendGroupMsg->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode Remanent
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRGSendGroupMsg->Remanent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__SendGroupMsgResult(&(pkRGSendGroupMsg->result), pkRGSendGroupMsg->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGSendGroupMsg(void* pData)
{
	RGSendGroupMsg* pkRGSendGroupMsg = (RGSendGroupMsg*)(pData);

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGSendGroupMsg->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGSendGroupMsg->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode Remanent
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRGSendGroupMsg->Remanent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__SendGroupMsgResult(&(pkRGSendGroupMsg->result), pkRGSendGroupMsg->ErrCode) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRSendMsg(void* pData)
{
	GRSendMsg* pkGRSendMsg = (GRSendMsg*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRSendMsg->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rcvuiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRSendMsg->rcvuiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MsgLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGRSendMsg->MsgLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Msg
	if((int)pkGRSendMsg->MsgLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RMSSAGE_SIZE < pkGRSendMsg->MsgLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRSendMsg->MsgLen;
	if(!m_kPackage.Pack("CHAR", &(pkGRSendMsg->Msg), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRSendMsg(void* pData)
{
	GRSendMsg* pkGRSendMsg = (GRSendMsg*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRSendMsg->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rcvuiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRSendMsg->rcvuiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MsgLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGRSendMsg->MsgLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Msg
	if((int)pkGRSendMsg->MsgLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_RMSSAGE_SIZE < pkGRSendMsg->MsgLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGRSendMsg->MsgLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGRSendMsg->Msg), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__SendMsgErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__SendMsgErr(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__SendMsgResult(void* pData)
{
	SendMsgResult* pkSendMsgResult = (SendMsgResult*)(pData);

	//EnCode Error
	if(EnCode__SendGroupMsgErr(&(pkSendMsgResult->Error)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode Suc
	if(EnCode__GeneralMsg(&(pkSendMsgResult->Suc)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__SendMsgResult(void* pData)
{
	SendMsgResult* pkSendMsgResult = (SendMsgResult*)(pData);

	//DeCode Error
	if(DeCode__SendGroupMsgErr(&(pkSendMsgResult->Error)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode Suc
	if(DeCode__GeneralMsg(&(pkSendMsgResult->Suc)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGSendMsg(void* pData)
{
	RGSendMsg* pkRGSendMsg = (RGSendMsg*)(pData);

	//EnCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGSendMsg->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerID
	if(EnCode__RPlayerID(&(pkRGSendMsg->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	if(EnCode__SendMsgResult(&(pkRGSendMsg->result)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGSendMsg(void* pData)
{
	RGSendMsg* pkRGSendMsg = (RGSendMsg*)(pData);

	//DeCode ErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGSendMsg->ErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerID
	if(DeCode__RPlayerID(&(pkRGSendMsg->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	if(DeCode__SendMsgResult(&(pkRGSendMsg->result)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamAttriKey(void* pData)
{
	TeamAttriKey* pkTeamAttriKey = (TeamAttriKey*)(pData);

	//EnCode attriKeyLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamAttriKey->attriKeyLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if((int)pkTeamAttriKey->attriKeyLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_ATTRI_KEY_LEN < pkTeamAttriKey->attriKeyLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamAttriKey->attriKeyLen;
	if(!m_kPackage.Pack("CHAR", &(pkTeamAttriKey->attriKey), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamAttriKey(void* pData)
{
	TeamAttriKey* pkTeamAttriKey = (TeamAttriKey*)(pData);

	//DeCode attriKeyLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamAttriKey->attriKeyLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if((int)pkTeamAttriKey->attriKeyLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_ATTRI_KEY_LEN < pkTeamAttriKey->attriKeyLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamAttriKey->attriKeyLen;
	if(!m_kPackage.UnPack("CHAR", &(pkTeamAttriKey->attriKey), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamAttriValue(void* pData)
{
	TeamAttriValue* pkTeamAttriValue = (TeamAttriValue*)(pData);

	//EnCode attriValueLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamAttriValue->attriValueLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attriValue
	if((int)pkTeamAttriValue->attriValueLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_ATTRI_VALUE_LEN < pkTeamAttriValue->attriValueLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamAttriValue->attriValueLen;
	if(!m_kPackage.Pack("CHAR", &(pkTeamAttriValue->attriValue), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamAttriValue(void* pData)
{
	TeamAttriValue* pkTeamAttriValue = (TeamAttriValue*)(pData);

	//DeCode attriValueLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamAttriValue->attriValueLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attriValue
	if((int)pkTeamAttriValue->attriValueLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_ATTRI_VALUE_LEN < pkTeamAttriValue->attriValueLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamAttriValue->attriValueLen;
	if(!m_kPackage.UnPack("CHAR", &(pkTeamAttriValue->attriValue), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamAttri(void* pData)
{
	TeamAttri* pkTeamAttri = (TeamAttri*)(pData);

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkTeamAttri->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriValue
	if(EnCode__TeamAttriValue(&(pkTeamAttri->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamAttri(void* pData)
{
	TeamAttri* pkTeamAttri = (TeamAttri*)(pData);

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkTeamAttri->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriValue
	if(DeCode__TeamAttriValue(&(pkTeamAttri->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AttriArray(void* pData)
{
	AttriArray* pkAttriArray = (AttriArray*)(pData);

	//EnCode attrisLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAttriArray->attrisLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attris
	if((int)pkAttriArray->attrisLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_ATTRI_COUNT < pkAttriArray->attrisLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkAttriArray->attrisLen; ++i)
	{
		if(EnCode__TeamAttri(&(pkAttriArray->attris[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AttriArray(void* pData)
{
	AttriArray* pkAttriArray = (AttriArray*)(pData);

	//DeCode attrisLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAttriArray->attrisLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attris
	if((int)pkAttriArray->attrisLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_ATTRI_COUNT < pkAttriArray->attrisLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkAttriArray->attrisLen; ++i)
	{
		if(DeCode__TeamAttri(&(pkAttriArray->attris[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamInd(void* pData)
{
	TeamInd* pkTeamInd = (TeamInd*)(pData);

	//EnCode teamIndLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamInd->teamIndLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if((int)pkTeamInd->teamIndLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_IND_LEN < pkTeamInd->teamIndLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamInd->teamIndLen;
	if(!m_kPackage.Pack("CHAR", &(pkTeamInd->teamInd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamInd(void* pData)
{
	TeamInd* pkTeamInd = (TeamInd*)(pData);

	//DeCode teamIndLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamInd->teamIndLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if((int)pkTeamInd->teamIndLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_IND_LEN < pkTeamInd->teamIndLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamInd->teamIndLen;
	if(!m_kPackage.UnPack("CHAR", &(pkTeamInd->teamInd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamName(void* pData)
{
	TeamName* pkTeamName = (TeamName*)(pData);

	//EnCode teamNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamName->teamNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamName
	if((int)pkTeamName->teamNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_NAME_LEN < pkTeamName->teamNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamName->teamNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkTeamName->teamName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamName(void* pData)
{
	TeamName* pkTeamName = (TeamName*)(pData);

	//DeCode teamNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamName->teamNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamName
	if((int)pkTeamName->teamNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_NAME_LEN < pkTeamName->teamNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamName->teamNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkTeamName->teamName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__MemberName(void* pData)
{
	MemberName* pkMemberName = (MemberName*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMemberName->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if((int)pkMemberName->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_NAME_LEN < pkMemberName->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMemberName->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkMemberName->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__MemberName(void* pData)
{
	MemberName* pkMemberName = (MemberName*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMemberName->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if((int)pkMemberName->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_NAME_LEN < pkMemberName->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMemberName->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkMemberName->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__MemberInd(void* pData)
{
	MemberInd* pkMemberInd = (MemberInd*)(pData);

	//EnCode memberIndLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMemberInd->memberIndLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberInd
	if((int)pkMemberInd->memberIndLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_MEMBER_IND_LEN < pkMemberInd->memberIndLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMemberInd->memberIndLen;
	if(!m_kPackage.Pack("CHAR", &(pkMemberInd->memberInd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__MemberInd(void* pData)
{
	MemberInd* pkMemberInd = (MemberInd*)(pData);

	//DeCode memberIndLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMemberInd->memberIndLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberInd
	if((int)pkMemberInd->memberIndLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_MEMBER_IND_LEN < pkMemberInd->memberIndLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMemberInd->memberIndLen;
	if(!m_kPackage.UnPack("CHAR", &(pkMemberInd->memberInd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__MembersInd(void* pData)
{
	MembersInd* pkMembersInd = (MembersInd*)(pData);

	//EnCode membersSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMembersInd->membersSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if((int)pkMembersInd->membersSize < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_MEMBERS_COUNT < pkMembersInd->membersSize)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkMembersInd->membersSize; ++i)
	{
		if(EnCode__MemberInd(&(pkMembersInd->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__MembersInd(void* pData)
{
	MembersInd* pkMembersInd = (MembersInd*)(pData);

	//DeCode membersSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMembersInd->membersSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if((int)pkMembersInd->membersSize < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_MEMBERS_COUNT < pkMembersInd->membersSize)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkMembersInd->membersSize; ++i)
	{
		if(DeCode__MemberInd(&(pkMembersInd->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__Member(void* pData)
{
	Member* pkMember = (Member*)(pData);

	//EnCode memberInd
	if(EnCode__MemberInd(&(pkMember->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkMember->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkMember->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMember->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(EnCode__MemberName(&(pkMember->memberName)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode memberJob
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkMember->memberJob), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapId
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMember->mapId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode avatarId
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMember->avatarId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMember->memberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSex
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkMember->memberSex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__Member(void* pData)
{
	Member* pkMember = (Member*)(pData);

	//DeCode memberInd
	if(DeCode__MemberInd(&(pkMember->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkMember->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkMember->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMember->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(DeCode__MemberName(&(pkMember->memberName)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode memberJob
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkMember->memberJob), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapId
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMember->mapId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode avatarId
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMember->avatarId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMember->memberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSex
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkMember->memberSex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__Members(void* pData)
{
	Members* pkMembers = (Members*)(pData);

	//EnCode membersSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkMembers->membersSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if((int)pkMembers->membersSize < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_MEMBERS_COUNT < pkMembers->membersSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkMembers->membersSize; ++i)
	{
		if(EnCode__Member(&(pkMembers->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__Members(void* pData)
{
	Members* pkMembers = (Members*)(pData);

	//DeCode membersSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkMembers->membersSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if((int)pkMembers->membersSize < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_MEMBERS_COUNT < pkMembers->membersSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkMembers->membersSize; ++i)
	{
		if(DeCode__Member(&(pkMembers->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__Team(void* pData)
{
	Team* pkTeam = (Team*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkTeam->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode teamMembers
	if(EnCode__Members(&(pkTeam->teamMembers)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode captainGateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeam->captainGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captainPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeam->captainPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode expSharedMode
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeam->expSharedMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode goodSharedMode
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeam->goodSharedMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rollItemLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkTeam->rollItemLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__Team(void* pData)
{
	Team* pkTeam = (Team*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkTeam->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode teamMembers
	if(DeCode__Members(&(pkTeam->teamMembers)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode captainGateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeam->captainGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captainPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeam->captainPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode expSharedMode
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeam->expSharedMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode goodSharedMode
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeam->goodSharedMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rollItemLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkTeam->rollItemLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__CreateTeamRequest(void* pData)
{
	CreateTeamRequest* pkCreateTeamRequest = (CreateTeamRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCreateTeamRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team
	if(EnCode__Team(&(pkCreateTeamRequest->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__CreateTeamRequest(void* pData)
{
	CreateTeamRequest* pkCreateTeamRequest = (CreateTeamRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCreateTeamRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team
	if(DeCode__Team(&(pkCreateTeamRequest->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__CreateTeamResult(void* pData)
{
	CreateTeamResult* pkCreateTeamResult = (CreateTeamResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCreateTeamResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCreateTeamResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team
	if(EnCode__Team(&(pkCreateTeamResult->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__CreateTeamResult(void* pData)
{
	CreateTeamResult* pkCreateTeamResult = (CreateTeamResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCreateTeamResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCreateTeamResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team
	if(DeCode__Team(&(pkCreateTeamResult->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DestroyTeamRequest(void* pData)
{
	DestroyTeamRequest* pkDestroyTeamRequest = (DestroyTeamRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyTeamRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkDestroyTeamRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DestroyTeamRequest(void* pData)
{
	DestroyTeamRequest* pkDestroyTeamRequest = (DestroyTeamRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyTeamRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkDestroyTeamRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DestroyTeamResult(void* pData)
{
	DestroyTeamResult* pkDestroyTeamResult = (DestroyTeamResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyTeamResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyTeamResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkDestroyTeamResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DestroyTeamResult(void* pData)
{
	DestroyTeamResult* pkDestroyTeamResult = (DestroyTeamResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyTeamResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyTeamResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkDestroyTeamResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddTeamMemberRequest(void* pData)
{
	AddTeamMemberRequest* pkAddTeamMemberRequest = (AddTeamMemberRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberRequest->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkAddTeamMemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode palyerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberRequest->palyerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode member
	if(EnCode__Member(&(pkAddTeamMemberRequest->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberRequest->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddTeamMemberRequest(void* pData)
{
	AddTeamMemberRequest* pkAddTeamMemberRequest = (AddTeamMemberRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberRequest->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkAddTeamMemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode palyerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberRequest->palyerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode member
	if(DeCode__Member(&(pkAddTeamMemberRequest->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberRequest->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddTeamMemberResult(void* pData)
{
	AddTeamMemberResult* pkAddTeamMemberResult = (AddTeamMemberResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkAddTeamMemberResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode member
	if(EnCode__Member(&(pkAddTeamMemberResult->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode palyerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberResult->palyerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddTeamMemberResult(void* pData)
{
	AddTeamMemberResult* pkAddTeamMemberResult = (AddTeamMemberResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkAddTeamMemberResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode member
	if(DeCode__Member(&(pkAddTeamMemberResult->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode palyerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberResult->palyerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveTeamMemberRequest(void* pData)
{
	RemoveTeamMemberRequest* pkRemoveTeamMemberRequest = (RemoveTeamMemberRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkRemoveTeamMemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode palyerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberRequest->palyerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode removedMode
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRemoveTeamMemberRequest->removedMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveTeamMemberRequest(void* pData)
{
	RemoveTeamMemberRequest* pkRemoveTeamMemberRequest = (RemoveTeamMemberRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkRemoveTeamMemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode palyerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberRequest->palyerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode removedMode
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRemoveTeamMemberRequest->removedMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveTeamMemberResult(void* pData)
{
	RemoveTeamMemberResult* pkRemoveTeamMemberResult = (RemoveTeamMemberResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberResult->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode removedMode
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRemoveTeamMemberResult->removedMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveTeamMemberResult(void* pData)
{
	RemoveTeamMemberResult* pkRemoveTeamMemberResult = (RemoveTeamMemberResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberResult->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode removedMode
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRemoveTeamMemberResult->removedMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryTeamRequest(void* pData)
{
	QueryTeamRequest* pkQueryTeamRequest = (QueryTeamRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkQueryTeamRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryTeamRequest(void* pData)
{
	QueryTeamRequest* pkQueryTeamRequest = (QueryTeamRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkQueryTeamRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryTeamResult(void* pData)
{
	QueryTeamResult* pkQueryTeamResult = (QueryTeamResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team
	if(EnCode__Team(&(pkQueryTeamResult->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryTeamResult(void* pData)
{
	QueryTeamResult* pkQueryTeamResult = (QueryTeamResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team
	if(DeCode__Team(&(pkQueryTeamResult->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryTeamMemberRequest(void* pData)
{
	QueryTeamMemberRequest* pkQueryTeamMemberRequest = (QueryTeamMemberRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkQueryTeamMemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMemberRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryTeamMemberRequest(void* pData)
{
	QueryTeamMemberRequest* pkQueryTeamMemberRequest = (QueryTeamMemberRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkQueryTeamMemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMemberRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryTeamMemberResult(void* pData)
{
	QueryTeamMemberResult* pkQueryTeamMemberResult = (QueryTeamMemberResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkQueryTeamMemberResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode member
	if(EnCode__Member(&(pkQueryTeamMemberResult->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryTeamMemberResult(void* pData)
{
	QueryTeamMemberResult* pkQueryTeamMemberResult = (QueryTeamMemberResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkQueryTeamMemberResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode member
	if(DeCode__Member(&(pkQueryTeamMemberResult->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UpdateMemberRequest(void* pData)
{
	UpdateMemberRequest* pkUpdateMemberRequest = (UpdateMemberRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkUpdateMemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode member
	if(EnCode__Member(&(pkUpdateMemberRequest->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UpdateMemberRequest(void* pData)
{
	UpdateMemberRequest* pkUpdateMemberRequest = (UpdateMemberRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkUpdateMemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode member
	if(DeCode__Member(&(pkUpdateMemberRequest->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UpdateMemberResult(void* pData)
{
	UpdateMemberResult* pkUpdateMemberResult = (UpdateMemberResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkUpdateMemberResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode member
	if(EnCode__Member(&(pkUpdateMemberResult->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UpdateMemberResult(void* pData)
{
	UpdateMemberResult* pkUpdateMemberResult = (UpdateMemberResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkUpdateMemberResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode member
	if(DeCode__Member(&(pkUpdateMemberResult->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddTeamAttriRequest(void* pData)
{
	AddTeamAttriRequest* pkAddTeamAttriRequest = (AddTeamAttriRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamAttriRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkAddTeamAttriRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkAddTeamAttriRequest->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriValue
	if(EnCode__TeamAttriValue(&(pkAddTeamAttriRequest->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddTeamAttriRequest(void* pData)
{
	AddTeamAttriRequest* pkAddTeamAttriRequest = (AddTeamAttriRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamAttriRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkAddTeamAttriRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkAddTeamAttriRequest->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriValue
	if(DeCode__TeamAttriValue(&(pkAddTeamAttriRequest->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddTeamAttriResult(void* pData)
{
	AddTeamAttriResult* pkAddTeamAttriResult = (AddTeamAttriResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamAttriResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamAttriResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkAddTeamAttriResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkAddTeamAttriResult->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriValue
	if(EnCode__TeamAttriValue(&(pkAddTeamAttriResult->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddTeamAttriResult(void* pData)
{
	AddTeamAttriResult* pkAddTeamAttriResult = (AddTeamAttriResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamAttriResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamAttriResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkAddTeamAttriResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkAddTeamAttriResult->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriValue
	if(DeCode__TeamAttriValue(&(pkAddTeamAttriResult->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveTeamAttriRequest(void* pData)
{
	RemoveTeamAttriRequest* pkRemoveTeamAttriRequest = (RemoveTeamAttriRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamAttriRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkRemoveTeamAttriRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkRemoveTeamAttriRequest->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveTeamAttriRequest(void* pData)
{
	RemoveTeamAttriRequest* pkRemoveTeamAttriRequest = (RemoveTeamAttriRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamAttriRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkRemoveTeamAttriRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkRemoveTeamAttriRequest->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveTeamAttriResult(void* pData)
{
	RemoveTeamAttriResult* pkRemoveTeamAttriResult = (RemoveTeamAttriResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamAttriResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamAttriResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkRemoveTeamAttriResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkRemoveTeamAttriResult->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveTeamAttriResult(void* pData)
{
	RemoveTeamAttriResult* pkRemoveTeamAttriResult = (RemoveTeamAttriResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamAttriResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamAttriResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkRemoveTeamAttriResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkRemoveTeamAttriResult->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddTeamMemberAttriRequest(void* pData)
{
	AddTeamMemberAttriRequest* pkAddTeamMemberAttriRequest = (AddTeamMemberAttriRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberAttriRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkAddTeamMemberAttriRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberAttriRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberAttriRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkAddTeamMemberAttriRequest->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriValue
	if(EnCode__TeamAttriValue(&(pkAddTeamMemberAttriRequest->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddTeamMemberAttriRequest(void* pData)
{
	AddTeamMemberAttriRequest* pkAddTeamMemberAttriRequest = (AddTeamMemberAttriRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberAttriRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkAddTeamMemberAttriRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberAttriRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberAttriRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkAddTeamMemberAttriRequest->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriValue
	if(DeCode__TeamAttriValue(&(pkAddTeamMemberAttriRequest->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddTeamMemberAttriResult(void* pData)
{
	AddTeamMemberAttriResult* pkAddTeamMemberAttriResult = (AddTeamMemberAttriResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberAttriResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberAttriResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkAddTeamMemberAttriResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode memberInd
	if(EnCode__MemberInd(&(pkAddTeamMemberAttriResult->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberAttriResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberAttriResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberAttriResult->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkAddTeamMemberAttriResult->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriValue
	if(EnCode__TeamAttriValue(&(pkAddTeamMemberAttriResult->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddTeamMemberAttriResult(void* pData)
{
	AddTeamMemberAttriResult* pkAddTeamMemberAttriResult = (AddTeamMemberAttriResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberAttriResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberAttriResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkAddTeamMemberAttriResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode memberInd
	if(DeCode__MemberInd(&(pkAddTeamMemberAttriResult->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberAttriResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberAttriResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberAttriResult->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkAddTeamMemberAttriResult->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriValue
	if(DeCode__TeamAttriValue(&(pkAddTeamMemberAttriResult->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveTeamMemberAttriRequest(void* pData)
{
	RemoveTeamMemberAttriRequest* pkRemoveTeamMemberAttriRequest = (RemoveTeamMemberAttriRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberAttriRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkRemoveTeamMemberAttriRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode memberInd
	if(EnCode__MemberInd(&(pkRemoveTeamMemberAttriRequest->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkRemoveTeamMemberAttriRequest->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveTeamMemberAttriRequest(void* pData)
{
	RemoveTeamMemberAttriRequest* pkRemoveTeamMemberAttriRequest = (RemoveTeamMemberAttriRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberAttriRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkRemoveTeamMemberAttriRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode memberInd
	if(DeCode__MemberInd(&(pkRemoveTeamMemberAttriRequest->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkRemoveTeamMemberAttriRequest->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveTeamMemberAttriResult(void* pData)
{
	RemoveTeamMemberAttriResult* pkRemoveTeamMemberAttriResult = (RemoveTeamMemberAttriResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberAttriResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberAttriResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkRemoveTeamMemberAttriResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode memberInd
	if(EnCode__MemberInd(&(pkRemoveTeamMemberAttriResult->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberAttriResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberAttriResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberAttriResult->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attriKey
	if(EnCode__TeamAttriKey(&(pkRemoveTeamMemberAttriResult->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode attriValue
	if(EnCode__TeamAttriValue(&(pkRemoveTeamMemberAttriResult->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveTeamMemberAttriResult(void* pData)
{
	RemoveTeamMemberAttriResult* pkRemoveTeamMemberAttriResult = (RemoveTeamMemberAttriResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberAttriResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberAttriResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkRemoveTeamMemberAttriResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode memberInd
	if(DeCode__MemberInd(&(pkRemoveTeamMemberAttriResult->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberAttriResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberAttriResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberAttriResult->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attriKey
	if(DeCode__TeamAttriKey(&(pkRemoveTeamMemberAttriResult->attriKey)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode attriValue
	if(DeCode__TeamAttriValue(&(pkRemoveTeamMemberAttriResult->attriValue)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__BroadcastData(void* pData)
{
	BroadcastData* pkBroadcastData = (BroadcastData*)(pData);

	//EnCode dataLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBroadcastData->dataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode data
	if((int)pkBroadcastData->dataLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_BROADCAST_DATA_LEN < pkBroadcastData->dataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkBroadcastData->dataLen;
	if(!m_kPackage.Pack("CHAR", &(pkBroadcastData->data), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__BroadcastData(void* pData)
{
	BroadcastData* pkBroadcastData = (BroadcastData*)(pData);

	//DeCode dataLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBroadcastData->dataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode data
	if((int)pkBroadcastData->dataLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_TEAM_BROADCAST_DATA_LEN < pkBroadcastData->dataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkBroadcastData->dataLen;
	if(!m_kPackage.UnPack("CHAR", &(pkBroadcastData->data), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamBroadcastRequest(void* pData)
{
	TeamBroadcastRequest* pkTeamBroadcastRequest = (TeamBroadcastRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkTeamBroadcastRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromMemberInd
	if(EnCode__MemberInd(&(pkTeamBroadcastRequest->fromMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastRequest->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastRequest->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dataType
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamBroadcastRequest->dataType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode broadcastData
	if(EnCode__BroadcastData(&(pkTeamBroadcastRequest->broadcastData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamBroadcastRequest(void* pData)
{
	TeamBroadcastRequest* pkTeamBroadcastRequest = (TeamBroadcastRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkTeamBroadcastRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromMemberInd
	if(DeCode__MemberInd(&(pkTeamBroadcastRequest->fromMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastRequest->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastRequest->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dataType
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamBroadcastRequest->dataType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode broadcastData
	if(DeCode__BroadcastData(&(pkTeamBroadcastRequest->broadcastData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamBroadcastResult(void* pData)
{
	TeamBroadcastResult* pkTeamBroadcastResult = (TeamBroadcastResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkTeamBroadcastResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastResult->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastResult->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dataType
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamBroadcastResult->dataType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode broadcastData
	if(EnCode__BroadcastData(&(pkTeamBroadcastResult->broadcastData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamBroadcastResult(void* pData)
{
	TeamBroadcastResult* pkTeamBroadcastResult = (TeamBroadcastResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkTeamBroadcastResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastResult->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastResult->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dataType
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamBroadcastResult->dataType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode broadcastData
	if(DeCode__BroadcastData(&(pkTeamBroadcastResult->broadcastData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamBroadcastNotify(void* pData)
{
	TeamBroadcastNotify* pkTeamBroadcastNotify = (TeamBroadcastNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkTeamBroadcastNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromMemberInd
	if(EnCode__MemberInd(&(pkTeamBroadcastNotify->fromMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromGateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastNotify->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastNotify->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode toMemberInd
	if(EnCode__MemberInd(&(pkTeamBroadcastNotify->toMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode toGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastNotify->toGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode toPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamBroadcastNotify->toPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dataType
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamBroadcastNotify->dataType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode broadcastData
	if(EnCode__BroadcastData(&(pkTeamBroadcastNotify->broadcastData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamBroadcastNotify(void* pData)
{
	TeamBroadcastNotify* pkTeamBroadcastNotify = (TeamBroadcastNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkTeamBroadcastNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromMemberInd
	if(DeCode__MemberInd(&(pkTeamBroadcastNotify->fromMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromGateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastNotify->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastNotify->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode toMemberInd
	if(DeCode__MemberInd(&(pkTeamBroadcastNotify->toMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode toGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastNotify->toGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode toPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamBroadcastNotify->toPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dataType
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamBroadcastNotify->dataType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode broadcastData
	if(DeCode__BroadcastData(&(pkTeamBroadcastNotify->broadcastData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamMember2MemberRequest(void* pData)
{
	TeamMember2MemberRequest* pkTeamMember2MemberRequest = (TeamMember2MemberRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkTeamMember2MemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromMemberInd
	if(EnCode__MemberInd(&(pkTeamMember2MemberRequest->fromMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberRequest->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberRequest->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode toMemberInd
	if(EnCode__MemberInd(&(pkTeamMember2MemberRequest->toMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode toGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberRequest->toGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode toPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberRequest->toPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode data
	if(EnCode__BroadcastData(&(pkTeamMember2MemberRequest->data)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamMember2MemberRequest(void* pData)
{
	TeamMember2MemberRequest* pkTeamMember2MemberRequest = (TeamMember2MemberRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkTeamMember2MemberRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromMemberInd
	if(DeCode__MemberInd(&(pkTeamMember2MemberRequest->fromMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberRequest->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberRequest->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode toMemberInd
	if(DeCode__MemberInd(&(pkTeamMember2MemberRequest->toMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode toGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberRequest->toGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode toPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberRequest->toPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode data
	if(DeCode__BroadcastData(&(pkTeamMember2MemberRequest->data)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamMember2MemberResult(void* pData)
{
	TeamMember2MemberResult* pkTeamMember2MemberResult = (TeamMember2MemberResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkTeamMember2MemberResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromMemberInd
	if(EnCode__MemberInd(&(pkTeamMember2MemberResult->fromMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberResult->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberResult->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode toMemberInd
	if(EnCode__MemberInd(&(pkTeamMember2MemberResult->toMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode toGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberResult->toGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode toPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMember2MemberResult->toPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode data
	if(EnCode__BroadcastData(&(pkTeamMember2MemberResult->data)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamMember2MemberResult(void* pData)
{
	TeamMember2MemberResult* pkTeamMember2MemberResult = (TeamMember2MemberResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkTeamMember2MemberResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromMemberInd
	if(DeCode__MemberInd(&(pkTeamMember2MemberResult->fromMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberResult->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberResult->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode toMemberInd
	if(DeCode__MemberInd(&(pkTeamMember2MemberResult->toMemberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode toGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberResult->toGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode toPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMember2MemberResult->toPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode data
	if(DeCode__BroadcastData(&(pkTeamMember2MemberResult->data)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamMemberMapIdRequest(void* pData)
{
	ModifyTeamMemberMapIdRequest* pkModifyTeamMemberMapIdRequest = (ModifyTeamMemberMapIdRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamMemberMapIdRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode GateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdRequest->GateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdRequest->PlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapId
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyTeamMemberMapIdRequest->mapId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamMemberMapIdRequest(void* pData)
{
	ModifyTeamMemberMapIdRequest* pkModifyTeamMemberMapIdRequest = (ModifyTeamMemberMapIdRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamMemberMapIdRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode GateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdRequest->GateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdRequest->PlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapId
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyTeamMemberMapIdRequest->mapId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamMemberMapIdResult(void* pData)
{
	ModifyTeamMemberMapIdResult* pkModifyTeamMemberMapIdResult = (ModifyTeamMemberMapIdResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamMemberMapIdResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode GateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdResult->GateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdResult->PlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapId
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyTeamMemberMapIdResult->mapId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamMemberMapIdResult(void* pData)
{
	ModifyTeamMemberMapIdResult* pkModifyTeamMemberMapIdResult = (ModifyTeamMemberMapIdResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamMemberMapIdResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode GateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdResult->GateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdResult->PlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapId
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyTeamMemberMapIdResult->mapId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamMemberMapIdNotify(void* pData)
{
	ModifyTeamMemberMapIdNotify* pkModifyTeamMemberMapIdNotify = (ModifyTeamMemberMapIdNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamMemberMapIdNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode GateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdNotify->GateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamMemberMapIdNotify->PlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapId
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyTeamMemberMapIdNotify->mapId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamMemberMapIdNotify(void* pData)
{
	ModifyTeamMemberMapIdNotify* pkModifyTeamMemberMapIdNotify = (ModifyTeamMemberMapIdNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamMemberMapIdNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode GateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdNotify->GateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamMemberMapIdNotify->PlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapId
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyTeamMemberMapIdNotify->mapId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamCreateedNotify(void* pData)
{
	TeamCreateedNotify* pkTeamCreateedNotify = (TeamCreateedNotify*)(pData);

	//EnCode playerId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamCreateedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamCreateedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team
	if(EnCode__Team(&(pkTeamCreateedNotify->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamCreateedNotify(void* pData)
{
	TeamCreateedNotify* pkTeamCreateedNotify = (TeamCreateedNotify*)(pData);

	//DeCode playerId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamCreateedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamCreateedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team
	if(DeCode__Team(&(pkTeamCreateedNotify->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamDestroyedNotify(void* pData)
{
	TeamDestroyedNotify* pkTeamDestroyedNotify = (TeamDestroyedNotify*)(pData);

	//EnCode causeCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamDestroyedNotify->causeCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberInd
	if(EnCode__MemberInd(&(pkTeamDestroyedNotify->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamDestroyedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamDestroyedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamDestroyedNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkTeamDestroyedNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamDestroyedNotify(void* pData)
{
	TeamDestroyedNotify* pkTeamDestroyedNotify = (TeamDestroyedNotify*)(pData);

	//DeCode causeCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamDestroyedNotify->causeCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberInd
	if(DeCode__MemberInd(&(pkTeamDestroyedNotify->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamDestroyedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamDestroyedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamDestroyedNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkTeamDestroyedNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddTeamMemberNotify(void* pData)
{
	AddTeamMemberNotify* pkAddTeamMemberNotify = (AddTeamMemberNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkAddTeamMemberNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode memberInd
	if(EnCode__MemberInd(&(pkAddTeamMemberNotify->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode team
	if(EnCode__Team(&(pkAddTeamMemberNotify->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberNotify->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateIdAdded
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberNotify->gateIdAdded), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIdAdded
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTeamMemberNotify->playerIdAdded), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddTeamMemberNotify(void* pData)
{
	AddTeamMemberNotify* pkAddTeamMemberNotify = (AddTeamMemberNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkAddTeamMemberNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode memberInd
	if(DeCode__MemberInd(&(pkAddTeamMemberNotify->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode team
	if(DeCode__Team(&(pkAddTeamMemberNotify->team)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberNotify->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateIdAdded
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberNotify->gateIdAdded), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIdAdded
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTeamMemberNotify->playerIdAdded), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveTeamMemberNotify(void* pData)
{
	RemoveTeamMemberNotify* pkRemoveTeamMemberNotify = (RemoveTeamMemberNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkRemoveTeamMemberNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode memberInd
	if(EnCode__MemberInd(&(pkRemoveTeamMemberNotify->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveTeamMemberNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode removedMember
	if(EnCode__Member(&(pkRemoveTeamMemberNotify->removedMember)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode removeMode
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRemoveTeamMemberNotify->removeMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveTeamMemberNotify(void* pData)
{
	RemoveTeamMemberNotify* pkRemoveTeamMemberNotify = (RemoveTeamMemberNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkRemoveTeamMemberNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode memberInd
	if(DeCode__MemberInd(&(pkRemoveTeamMemberNotify->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveTeamMemberNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode removedMember
	if(DeCode__Member(&(pkRemoveTeamMemberNotify->removedMember)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode removeMode
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRemoveTeamMemberNotify->removeMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UpdateTeamMemberNotify(void* pData)
{
	UpdateTeamMemberNotify* pkUpdateTeamMemberNotify = (UpdateTeamMemberNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkUpdateTeamMemberNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode memberInd
	if(EnCode__MemberInd(&(pkUpdateTeamMemberNotify->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateTeamMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateTeamMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateTeamMemberNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode member
	if(EnCode__Member(&(pkUpdateTeamMemberNotify->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UpdateTeamMemberNotify(void* pData)
{
	UpdateTeamMemberNotify* pkUpdateTeamMemberNotify = (UpdateTeamMemberNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkUpdateTeamMemberNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode memberInd
	if(DeCode__MemberInd(&(pkUpdateTeamMemberNotify->memberInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateTeamMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateTeamMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateTeamMemberNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode member
	if(DeCode__Member(&(pkUpdateTeamMemberNotify->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ReportGateSvrIdRequest(void* pData)
{
	ReportGateSvrIdRequest* pkReportGateSvrIdRequest = (ReportGateSvrIdRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReportGateSvrIdRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReportGateSvrIdRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ReportGateSvrIdRequest(void* pData)
{
	ReportGateSvrIdRequest* pkReportGateSvrIdRequest = (ReportGateSvrIdRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReportGateSvrIdRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReportGateSvrIdRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ReportGateSvrIdResult(void* pData)
{
	ReportGateSvrIdResult* pkReportGateSvrIdResult = (ReportGateSvrIdResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReportGateSvrIdResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReportGateSvrIdResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ReportGateSvrIdResult(void* pData)
{
	ReportGateSvrIdResult* pkReportGateSvrIdResult = (ReportGateSvrIdResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReportGateSvrIdResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReportGateSvrIdResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamCaptainRequest(void* pData)
{
	ModifyTeamCaptainRequest* pkModifyTeamCaptainRequest = (ModifyTeamCaptainRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamCaptainRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamCaptainRequest(void* pData)
{
	ModifyTeamCaptainRequest* pkModifyTeamCaptainRequest = (ModifyTeamCaptainRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamCaptainRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamCaptainResult(void* pData)
{
	ModifyTeamCaptainResult* pkModifyTeamCaptainResult = (ModifyTeamCaptainResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamCaptainResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamCaptainResult(void* pData)
{
	ModifyTeamCaptainResult* pkModifyTeamCaptainResult = (ModifyTeamCaptainResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamCaptainResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamCaptainNotify(void* pData)
{
	ModifyTeamCaptainNotify* pkModifyTeamCaptainNotify = (ModifyTeamCaptainNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamCaptainNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captainPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainNotify->captainPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captainGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamCaptainNotify->captainGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamCaptainNotify(void* pData)
{
	ModifyTeamCaptainNotify* pkModifyTeamCaptainNotify = (ModifyTeamCaptainNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamCaptainNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captainPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainNotify->captainPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captainGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamCaptainNotify->captainGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyExpSharedModeRequest(void* pData)
{
	ModifyExpSharedModeRequest* pkModifyExpSharedModeRequest = (ModifyExpSharedModeRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyExpSharedModeRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyExpSharedModeRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mode
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyExpSharedModeRequest->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyExpSharedModeRequest(void* pData)
{
	ModifyExpSharedModeRequest* pkModifyExpSharedModeRequest = (ModifyExpSharedModeRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyExpSharedModeRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyExpSharedModeRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mode
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyExpSharedModeRequest->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyExpSharedModeResult(void* pData)
{
	ModifyExpSharedModeResult* pkModifyExpSharedModeResult = (ModifyExpSharedModeResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyExpSharedModeResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyExpSharedModeResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyExpSharedModeResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mode
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyExpSharedModeResult->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyExpSharedModeResult(void* pData)
{
	ModifyExpSharedModeResult* pkModifyExpSharedModeResult = (ModifyExpSharedModeResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyExpSharedModeResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyExpSharedModeResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyExpSharedModeResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mode
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyExpSharedModeResult->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyGoodSharedModeRequest(void* pData)
{
	ModifyGoodSharedModeRequest* pkModifyGoodSharedModeRequest = (ModifyGoodSharedModeRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyGoodSharedModeRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyGoodSharedModeRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mode
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyGoodSharedModeRequest->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyGoodSharedModeRequest(void* pData)
{
	ModifyGoodSharedModeRequest* pkModifyGoodSharedModeRequest = (ModifyGoodSharedModeRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyGoodSharedModeRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyGoodSharedModeRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mode
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyGoodSharedModeRequest->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyGoodSharedModeResult(void* pData)
{
	ModifyGoodSharedModeResult* pkModifyGoodSharedModeResult = (ModifyGoodSharedModeResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyGoodSharedModeResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyGoodSharedModeResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyGoodSharedModeResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mode
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyGoodSharedModeResult->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyGoodSharedModeResult(void* pData)
{
	ModifyGoodSharedModeResult* pkModifyGoodSharedModeResult = (ModifyGoodSharedModeResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyGoodSharedModeResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyGoodSharedModeResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyGoodSharedModeResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mode
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyGoodSharedModeResult->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyExpSharedModeNotify(void* pData)
{
	ModifyExpSharedModeNotify* pkModifyExpSharedModeNotify = (ModifyExpSharedModeNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyExpSharedModeNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mode
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyExpSharedModeNotify->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyExpSharedModeNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyExpSharedModeNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyExpSharedModeNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyExpSharedModeNotify(void* pData)
{
	ModifyExpSharedModeNotify* pkModifyExpSharedModeNotify = (ModifyExpSharedModeNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyExpSharedModeNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyExpSharedModeNotify->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyExpSharedModeNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyExpSharedModeNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyExpSharedModeNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyGoodSharedModeNotify(void* pData)
{
	ModifyGoodSharedModeNotify* pkModifyGoodSharedModeNotify = (ModifyGoodSharedModeNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyGoodSharedModeNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mode
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyGoodSharedModeNotify->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSeq
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyGoodSharedModeNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyGoodSharedModeNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyGoodSharedModeNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyGoodSharedModeNotify(void* pData)
{
	ModifyGoodSharedModeNotify* pkModifyGoodSharedModeNotify = (ModifyGoodSharedModeNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyGoodSharedModeNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyGoodSharedModeNotify->mode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSeq
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyGoodSharedModeNotify->memberSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyGoodSharedModeNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyGoodSharedModeNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamRollItemLevelRequest(void* pData)
{
	ModifyTeamRollItemLevelRequest* pkModifyTeamRollItemLevelRequest = (ModifyTeamRollItemLevelRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamRollItemLevelRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkModifyTeamRollItemLevelRequest->level), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamRollItemLevelRequest(void* pData)
{
	ModifyTeamRollItemLevelRequest* pkModifyTeamRollItemLevelRequest = (ModifyTeamRollItemLevelRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamRollItemLevelRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkModifyTeamRollItemLevelRequest->level), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamRollItemLevelResult(void* pData)
{
	ModifyTeamRollItemLevelResult* pkModifyTeamRollItemLevelResult = (ModifyTeamRollItemLevelResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamRollItemLevelResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkModifyTeamRollItemLevelResult->level), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamRollItemLevelResult(void* pData)
{
	ModifyTeamRollItemLevelResult* pkModifyTeamRollItemLevelResult = (ModifyTeamRollItemLevelResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamRollItemLevelResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkModifyTeamRollItemLevelResult->level), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTeamRollItemLevelNotify(void* pData)
{
	ModifyTeamRollItemLevelNotify* pkModifyTeamRollItemLevelNotify = (ModifyTeamRollItemLevelNotify*)(pData);

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkModifyTeamRollItemLevelNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fromGateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelNotify->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelNotify->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode toGateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelNotify->toGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode toPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTeamRollItemLevelNotify->toPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkModifyTeamRollItemLevelNotify->level), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTeamRollItemLevelNotify(void* pData)
{
	ModifyTeamRollItemLevelNotify* pkModifyTeamRollItemLevelNotify = (ModifyTeamRollItemLevelNotify*)(pData);

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkModifyTeamRollItemLevelNotify->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fromGateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelNotify->fromGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fromPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelNotify->fromPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode toGateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelNotify->toGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode toPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTeamRollItemLevelNotify->toPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkModifyTeamRollItemLevelNotify->level), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryTeamMembersCountRequest(void* pData)
{
	QueryTeamMembersCountRequest* pkQueryTeamMembersCountRequest = (QueryTeamMembersCountRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMembersCountRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkQueryTeamMembersCountRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMembersCountRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMembersCountRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryTeamMembersCountRequest(void* pData)
{
	QueryTeamMembersCountRequest* pkQueryTeamMembersCountRequest = (QueryTeamMembersCountRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMembersCountRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkQueryTeamMembersCountRequest->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMembersCountRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMembersCountRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryTeamMembersCountResult(void* pData)
{
	QueryTeamMembersCountResult* pkQueryTeamMembersCountResult = (QueryTeamMembersCountResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMembersCountResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMembersCountResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamInd
	if(EnCode__TeamInd(&(pkQueryTeamMembersCountResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMembersCountResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMembersCountResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode count
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryTeamMembersCountResult->count), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryTeamMembersCountResult(void* pData)
{
	QueryTeamMembersCountResult* pkQueryTeamMembersCountResult = (QueryTeamMembersCountResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMembersCountResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMembersCountResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamInd
	if(DeCode__TeamInd(&(pkQueryTeamMembersCountResult->teamInd)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMembersCountResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMembersCountResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode count
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryTeamMembersCountResult->count), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupName(void* pData)
{
	ChatGroupName* pkChatGroupName = (ChatGroupName*)(pData);

	//EnCode groupNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkChatGroupName->groupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupName
	if((int)pkChatGroupName->groupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_GROUP_NAME_LEN < pkChatGroupName->groupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkChatGroupName->groupNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkChatGroupName->groupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupName(void* pData)
{
	ChatGroupName* pkChatGroupName = (ChatGroupName*)(pData);

	//DeCode groupNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkChatGroupName->groupNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupName
	if((int)pkChatGroupName->groupNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_GROUP_NAME_LEN < pkChatGroupName->groupNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkChatGroupName->groupNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkChatGroupName->groupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupMember(void* pData)
{
	ChatGroupMember* pkChatGroupMember = (ChatGroupMember*)(pData);

	//EnCode memberId
	if(EnCode__MemberInd(&(pkChatGroupMember->memberId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupMember->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupMember->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupMember(void* pData)
{
	ChatGroupMember* pkChatGroupMember = (ChatGroupMember*)(pData);

	//DeCode memberId
	if(DeCode__MemberInd(&(pkChatGroupMember->memberId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupMember->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupMember->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupMembersList(void* pData)
{
	ChatGroupMembersList* pkChatGroupMembersList = (ChatGroupMembersList*)(pData);

	//EnCode membersCount
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkChatGroupMembersList->membersCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if((int)pkChatGroupMembersList->membersCount < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_GROUP_MEMBERS_COUNT < pkChatGroupMembersList->membersCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkChatGroupMembersList->membersCount; ++i)
	{
		if(EnCode__MemberInd(&(pkChatGroupMembersList->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupMembersList(void* pData)
{
	ChatGroupMembersList* pkChatGroupMembersList = (ChatGroupMembersList*)(pData);

	//DeCode membersCount
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkChatGroupMembersList->membersCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if((int)pkChatGroupMembersList->membersCount < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_GROUP_MEMBERS_COUNT < pkChatGroupMembersList->membersCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkChatGroupMembersList->membersCount; ++i)
	{
		if(DeCode__MemberInd(&(pkChatGroupMembersList->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupPassword(void* pData)
{
	ChatGroupPassword* pkChatGroupPassword = (ChatGroupPassword*)(pData);

	//EnCode passwordLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkChatGroupPassword->passwordLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode password
	if((int)pkChatGroupPassword->passwordLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_GROUP_PASSWORD_LEN < pkChatGroupPassword->passwordLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkChatGroupPassword->passwordLen;
	if(!m_kPackage.Pack("CHAR", &(pkChatGroupPassword->password), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupPassword(void* pData)
{
	ChatGroupPassword* pkChatGroupPassword = (ChatGroupPassword*)(pData);

	//DeCode passwordLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkChatGroupPassword->passwordLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode password
	if((int)pkChatGroupPassword->passwordLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_GROUP_PASSWORD_LEN < pkChatGroupPassword->passwordLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkChatGroupPassword->passwordLen;
	if(!m_kPackage.UnPack("CHAR", &(pkChatGroupPassword->password), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroup(void* pData)
{
	ChatGroup* pkChatGroup = (ChatGroup*)(pData);

	//EnCode groupId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroup->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if(EnCode__ChatGroupMembersList(&(pkChatGroup->members)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode password
	if(EnCode__ChatGroupPassword(&(pkChatGroup->password)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode owner
	if(EnCode__ChatGroupMember(&(pkChatGroup->owner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroup(void* pData)
{
	ChatGroup* pkChatGroup = (ChatGroup*)(pData);

	//DeCode groupId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroup->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if(DeCode__ChatGroupMembersList(&(pkChatGroup->members)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode password
	if(DeCode__ChatGroupPassword(&(pkChatGroup->password)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode owner
	if(DeCode__ChatGroupMember(&(pkChatGroup->owner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatData(void* pData)
{
	ChatData* pkChatData = (ChatData*)(pData);

	//EnCode dataLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkChatData->dataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode data
	if((int)pkChatData->dataLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_DATA_LEN < pkChatData->dataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkChatData->dataLen;
	if(!m_kPackage.Pack("CHAR", &(pkChatData->data), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatData(void* pData)
{
	ChatData* pkChatData = (ChatData*)(pData);

	//DeCode dataLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkChatData->dataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode data
	if((int)pkChatData->dataLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_DATA_LEN < pkChatData->dataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkChatData->dataLen;
	if(!m_kPackage.UnPack("CHAR", &(pkChatData->data), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__CreateChatGroupRequest(void* pData)
{
	CreateChatGroupRequest* pkCreateChatGroupRequest = (CreateChatGroupRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCreateChatGroupRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode group
	if(EnCode__ChatGroup(&(pkCreateChatGroupRequest->group)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__CreateChatGroupRequest(void* pData)
{
	CreateChatGroupRequest* pkCreateChatGroupRequest = (CreateChatGroupRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCreateChatGroupRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode group
	if(DeCode__ChatGroup(&(pkCreateChatGroupRequest->group)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__CreateChatGroupResult(void* pData)
{
	CreateChatGroupResult* pkCreateChatGroupResult = (CreateChatGroupResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCreateChatGroupResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCreateChatGroupResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode group
	if(EnCode__ChatGroup(&(pkCreateChatGroupResult->group)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__CreateChatGroupResult(void* pData)
{
	CreateChatGroupResult* pkCreateChatGroupResult = (CreateChatGroupResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCreateChatGroupResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCreateChatGroupResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode group
	if(DeCode__ChatGroup(&(pkCreateChatGroupResult->group)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DestroyChatGroupRequest(void* pData)
{
	DestroyChatGroupRequest* pkDestroyChatGroupRequest = (DestroyChatGroupRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupRequest->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DestroyChatGroupRequest(void* pData)
{
	DestroyChatGroupRequest* pkDestroyChatGroupRequest = (DestroyChatGroupRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupRequest->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DestroyChatGroupResult(void* pData)
{
	DestroyChatGroupResult* pkDestroyChatGroupResult = (DestroyChatGroupResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupResult->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyChatGroupResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DestroyChatGroupResult(void* pData)
{
	DestroyChatGroupResult* pkDestroyChatGroupResult = (DestroyChatGroupResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupResult->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyChatGroupResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupDestroyedNotify(void* pData)
{
	ChatGroupDestroyedNotify* pkChatGroupDestroyedNotify = (ChatGroupDestroyedNotify*)(pData);

	//EnCode groupId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupDestroyedNotify->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupDestroyedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupDestroyedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupDestroyedNotify(void* pData)
{
	ChatGroupDestroyedNotify* pkChatGroupDestroyedNotify = (ChatGroupDestroyedNotify*)(pData);

	//DeCode groupId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupDestroyedNotify->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupDestroyedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupDestroyedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddChatGroupMemberRequest(void* pData)
{
	AddChatGroupMemberRequest* pkAddChatGroupMemberRequest = (AddChatGroupMemberRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddChatGroupMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberAdded
	if(EnCode__ChatGroupMember(&(pkAddChatGroupMemberRequest->memberAdded)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode groupId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddChatGroupMemberRequest->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddChatGroupMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddChatGroupMemberRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode passwrod
	if(EnCode__ChatGroupPassword(&(pkAddChatGroupMemberRequest->passwrod)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddChatGroupMemberRequest(void* pData)
{
	AddChatGroupMemberRequest* pkAddChatGroupMemberRequest = (AddChatGroupMemberRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddChatGroupMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberAdded
	if(DeCode__ChatGroupMember(&(pkAddChatGroupMemberRequest->memberAdded)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode groupId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddChatGroupMemberRequest->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddChatGroupMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddChatGroupMemberRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode passwrod
	if(DeCode__ChatGroupPassword(&(pkAddChatGroupMemberRequest->passwrod)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddChatGroupMemberResult(void* pData)
{
	AddChatGroupMemberResult* pkAddChatGroupMemberResult = (AddChatGroupMemberResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddChatGroupMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddChatGroupMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode group
	if(EnCode__ChatGroup(&(pkAddChatGroupMemberResult->group)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddChatGroupMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddChatGroupMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddChatGroupMemberResult(void* pData)
{
	AddChatGroupMemberResult* pkAddChatGroupMemberResult = (AddChatGroupMemberResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddChatGroupMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddChatGroupMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode group
	if(DeCode__ChatGroup(&(pkAddChatGroupMemberResult->group)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddChatGroupMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddChatGroupMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupMemberAddedNotify(void* pData)
{
	ChatGroupMemberAddedNotify* pkChatGroupMemberAddedNotify = (ChatGroupMemberAddedNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupMemberAddedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupMemberAddedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberAdded
	if(EnCode__ChatGroupMember(&(pkChatGroupMemberAddedNotify->memberAdded)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupMemberAddedNotify(void* pData)
{
	ChatGroupMemberAddedNotify* pkChatGroupMemberAddedNotify = (ChatGroupMemberAddedNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupMemberAddedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupMemberAddedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberAdded
	if(DeCode__ChatGroupMember(&(pkChatGroupMemberAddedNotify->memberAdded)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveChatGroupMemberRequest(void* pData)
{
	RemoveChatGroupMemberRequest* pkRemoveChatGroupMemberRequest = (RemoveChatGroupMemberRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberRemoved
	if(EnCode__ChatGroupMember(&(pkRemoveChatGroupMemberRequest->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode groupId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberRequest->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveChatGroupMemberRequest(void* pData)
{
	RemoveChatGroupMemberRequest* pkRemoveChatGroupMemberRequest = (RemoveChatGroupMemberRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberRemoved
	if(DeCode__ChatGroupMember(&(pkRemoveChatGroupMemberRequest->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode groupId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberRequest->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveChatGroupMemberResult(void* pData)
{
	RemoveChatGroupMemberResult* pkRemoveChatGroupMemberResult = (RemoveChatGroupMemberResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberRemoved
	if(EnCode__ChatGroupMember(&(pkRemoveChatGroupMemberResult->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode groupId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberResult->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveChatGroupMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveChatGroupMemberResult(void* pData)
{
	RemoveChatGroupMemberResult* pkRemoveChatGroupMemberResult = (RemoveChatGroupMemberResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberRemoved
	if(DeCode__ChatGroupMember(&(pkRemoveChatGroupMemberResult->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode groupId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberResult->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveChatGroupMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupMemberRemovedNotify(void* pData)
{
	ChatGroupMemberRemovedNotify* pkChatGroupMemberRemovedNotify = (ChatGroupMemberRemovedNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupMemberRemovedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupMemberRemovedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupMemberRemovedNotify->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberRemoved
	if(EnCode__ChatGroupMember(&(pkChatGroupMemberRemovedNotify->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupMemberRemovedNotify(void* pData)
{
	ChatGroupMemberRemovedNotify* pkChatGroupMemberRemovedNotify = (ChatGroupMemberRemovedNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupMemberRemovedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupMemberRemovedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupMemberRemovedNotify->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberRemoved
	if(DeCode__ChatGroupMember(&(pkChatGroupMemberRemovedNotify->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupSpeekRequest(void* pData)
{
	ChatGroupSpeekRequest* pkChatGroupSpeekRequest = (ChatGroupSpeekRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekRequest->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode data
	if(EnCode__ChatData(&(pkChatGroupSpeekRequest->data)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupSpeekRequest(void* pData)
{
	ChatGroupSpeekRequest* pkChatGroupSpeekRequest = (ChatGroupSpeekRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekRequest->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode data
	if(DeCode__ChatData(&(pkChatGroupSpeekRequest->data)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupSpeekResult(void* pData)
{
	ChatGroupSpeekResult* pkChatGroupSpeekResult = (ChatGroupSpeekResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupSpeekResult(void* pData)
{
	ChatGroupSpeekResult* pkChatGroupSpeekResult = (ChatGroupSpeekResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ChatGroupSpeekNotify(void* pData)
{
	ChatGroupSpeekNotify* pkChatGroupSpeekNotify = (ChatGroupSpeekNotify*)(pData);

	//EnCode groupId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekNotify->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkChatGroupSpeekNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode talker
	if(EnCode__MemberInd(&(pkChatGroupSpeekNotify->talker)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode data
	if(EnCode__ChatData(&(pkChatGroupSpeekNotify->data)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ChatGroupSpeekNotify(void* pData)
{
	ChatGroupSpeekNotify* pkChatGroupSpeekNotify = (ChatGroupSpeekNotify*)(pData);

	//DeCode groupId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekNotify->groupId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkChatGroupSpeekNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode talker
	if(DeCode__MemberInd(&(pkChatGroupSpeekNotify->talker)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode data
	if(DeCode__ChatData(&(pkChatGroupSpeekNotify->data)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionMember(void* pData)
{
	UnionMember* pkUnionMember = (UnionMember*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMember->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMember->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMember->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if((int)pkUnionMember->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionMember->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMember->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionMember->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode job
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->job), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posSeq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if((int)pkUnionMember->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionMember->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMember->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionMember->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lastQuit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMember->lastQuit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode forbid
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->forbid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode onLine
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->onLine), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionMember(void* pData)
{
	UnionMember* pkUnionMember = (UnionMember*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMember->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMember->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMember->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if((int)pkUnionMember->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionMember->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMember->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionMember->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode job
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->job), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posSeq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if((int)pkUnionMember->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionMember->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMember->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionMember->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lastQuit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMember->lastQuit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode forbid
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->forbid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode onLine
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->onLine), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionPosRight(void* pData)
{
	UnionPosRight* pkUnionPosRight = (UnionPosRight*)(pData);

	//EnCode seq
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionPosRight->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionPosRight->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if((int)pkUnionPosRight->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(8 < pkUnionPosRight->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionPosRight->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionPosRight->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode right
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionPosRight->right), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionPosRight(void* pData)
{
	UnionPosRight* pkUnionPosRight = (UnionPosRight*)(pData);

	//DeCode seq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionPosRight->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionPosRight->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if((int)pkUnionPosRight->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(8 < pkUnionPosRight->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionPosRight->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionPosRight->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode right
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionPosRight->right), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__Union(void* pData)
{
	Union* pkUnion = (Union*)(pData);

	//EnCode id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnion->id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if((int)pkUnion->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkUnion->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnion->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnion->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode num
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->num), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ownerUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnion->ownerUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if((int)pkUnion->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_TITLE_LEN < pkUnion->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnion->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnion->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnion->active), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posListLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posList
	if((int)pkUnion->posListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkUnion->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnion->posListLen; ++i)
	{
		if(EnCode__UnionPosRight(&(pkUnion->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__Union(void* pData)
{
	Union* pkUnion = (Union*)(pData);

	//DeCode id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnion->id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if((int)pkUnion->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkUnion->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnion->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnion->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode num
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->num), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ownerUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnion->ownerUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if((int)pkUnion->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_TITLE_LEN < pkUnion->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnion->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnion->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnion->active), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posListLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posList
	if((int)pkUnion->posListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkUnion->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnion->posListLen; ++i)
	{
		if(DeCode__UnionPosRight(&(pkUnion->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ReportPlayerUpline(void* pData)
{
	ReportPlayerUpline* pkReportPlayerUpline = (ReportPlayerUpline*)(pData);

	//EnCode member
	if(EnCode__UnionMember(&(pkReportPlayerUpline->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ReportPlayerUpline(void* pData)
{
	ReportPlayerUpline* pkReportPlayerUpline = (ReportPlayerUpline*)(pData);

	//DeCode member
	if(DeCode__UnionMember(&(pkReportPlayerUpline->member)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ReportPlayerOffline(void* pData)
{
	ReportPlayerOffline* pkReportPlayerOffline = (ReportPlayerOffline*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReportPlayerOffline->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ReportPlayerOffline(void* pData)
{
	ReportPlayerOffline* pkReportPlayerOffline = (ReportPlayerOffline*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReportPlayerOffline->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__CreateUnionRequest(void* pData)
{
	CreateUnionRequest* pkCreateUnionRequest = (CreateUnionRequest*)(pData);

	//EnCode _union
	if(EnCode__Union(&(pkCreateUnionRequest->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__CreateUnionRequest(void* pData)
{
	CreateUnionRequest* pkCreateUnionRequest = (CreateUnionRequest*)(pData);

	//DeCode _union
	if(DeCode__Union(&(pkCreateUnionRequest->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__CreateUnionResult(void* pData)
{
	CreateUnionResult* pkCreateUnionResult = (CreateUnionResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCreateUnionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCreateUnionResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCreateUnionResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode _union
	if(EnCode__Union(&(pkCreateUnionResult->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode captionNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCreateUnionResult->captionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captionName
	if((int)pkCreateUnionResult->captionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkCreateUnionResult->captionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCreateUnionResult->captionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCreateUnionResult->captionName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__CreateUnionResult(void* pData)
{
	CreateUnionResult* pkCreateUnionResult = (CreateUnionResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCreateUnionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCreateUnionResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCreateUnionResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode _union
	if(DeCode__Union(&(pkCreateUnionResult->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode captionNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCreateUnionResult->captionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captionName
	if((int)pkCreateUnionResult->captionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkCreateUnionResult->captionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCreateUnionResult->captionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCreateUnionResult->captionName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DestroyUnionRequest(void* pData)
{
	DestroyUnionRequest* pkDestroyUnionRequest = (DestroyUnionRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyUnionRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DestroyUnionRequest(void* pData)
{
	DestroyUnionRequest* pkDestroyUnionRequest = (DestroyUnionRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyUnionRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__DestroyUnionResult(void* pData)
{
	DestroyUnionResult* pkDestroyUnionResult = (DestroyUnionResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkDestroyUnionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyUnionResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkDestroyUnionResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__DestroyUnionResult(void* pData)
{
	DestroyUnionResult* pkDestroyUnionResult = (DestroyUnionResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkDestroyUnionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyUnionResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkDestroyUnionResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionDestroyedNotify(void* pData)
{
	UnionDestroyedNotify* pkUnionDestroyedNotify = (UnionDestroyedNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionDestroyedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionDestroyedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionDestroyedNotify(void* pData)
{
	UnionDestroyedNotify* pkUnionDestroyedNotify = (UnionDestroyedNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionDestroyedNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionDestroyedNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionMemberList(void* pData)
{
	UnionMemberList* pkUnionMemberList = (UnionMemberList*)(pData);

	//EnCode count
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMemberList->count), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if((int)pkUnionMemberList->count < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_MEMBER_LIST_SIZE < pkUnionMemberList->count)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnionMemberList->count; ++i)
	{
		if(EnCode__UnionMember(&(pkUnionMemberList->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionMemberList(void* pData)
{
	UnionMemberList* pkUnionMemberList = (UnionMemberList*)(pData);

	//DeCode count
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMemberList->count), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if((int)pkUnionMemberList->count < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_MEMBER_LIST_SIZE < pkUnionMemberList->count)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnionMemberList->count; ++i)
	{
		if(DeCode__UnionMember(&(pkUnionMemberList->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryUnionBasicRequest(void* pData)
{
	QueryUnionBasicRequest* pkQueryUnionBasicRequest = (QueryUnionBasicRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionBasicRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryUnionBasicRequest(void* pData)
{
	QueryUnionBasicRequest* pkQueryUnionBasicRequest = (QueryUnionBasicRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionBasicRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryUnionBasicResult(void* pData)
{
	QueryUnionBasicResult* pkQueryUnionBasicResult = (QueryUnionBasicResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkQueryUnionBasicResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionBasicResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionBasicResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bGate
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkQueryUnionBasicResult->bGate), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode _union
	if(EnCode__Union(&(pkQueryUnionBasicResult->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryUnionBasicResult(void* pData)
{
	QueryUnionBasicResult* pkQueryUnionBasicResult = (QueryUnionBasicResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkQueryUnionBasicResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionBasicResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionBasicResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bGate
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkQueryUnionBasicResult->bGate), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode _union
	if(DeCode__Union(&(pkQueryUnionBasicResult->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryUnionMembersRequest(void* pData)
{
	QueryUnionMembersRequest* pkQueryUnionMembersRequest = (QueryUnionMembersRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionMembersRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryUnionMembersRequest(void* pData)
{
	QueryUnionMembersRequest* pkQueryUnionMembersRequest = (QueryUnionMembersRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionMembersRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryUnionMembersResult(void* pData)
{
	QueryUnionMembersResult* pkQueryUnionMembersResult = (QueryUnionMembersResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkQueryUnionMembersResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionMembersResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionMembersResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if(EnCode__UnionMemberList(&(pkQueryUnionMembersResult->members)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryUnionMembersResult(void* pData)
{
	QueryUnionMembersResult* pkQueryUnionMembersResult = (QueryUnionMembersResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkQueryUnionMembersResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionMembersResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionMembersResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if(DeCode__UnionMemberList(&(pkQueryUnionMembersResult->members)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddUnionMemberRequest(void* pData)
{
	AddUnionMemberRequest* pkAddUnionMemberRequest = (AddUnionMemberRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionMemberRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberAddedNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionMemberRequest->memberAddedNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberAddedName
	if((int)pkAddUnionMemberRequest->memberAddedNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionMemberRequest->memberAddedNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberRequest->memberAddedNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddUnionMemberRequest->memberAddedName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddUnionMemberRequest(void* pData)
{
	AddUnionMemberRequest* pkAddUnionMemberRequest = (AddUnionMemberRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionMemberRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberAddedNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionMemberRequest->memberAddedNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberAddedName
	if((int)pkAddUnionMemberRequest->memberAddedNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionMemberRequest->memberAddedNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberRequest->memberAddedNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddUnionMemberRequest->memberAddedName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddUnionMemberConfirm(void* pData)
{
	AddUnionMemberConfirm* pkAddUnionMemberConfirm = (AddUnionMemberConfirm*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionMemberConfirm->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionMemberConfirm->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionMemberConfirm->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionName
	if((int)pkAddUnionMemberConfirm->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkAddUnionMemberConfirm->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberConfirm->unionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddUnionMemberConfirm->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sponsorLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionMemberConfirm->sponsorLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sponsor
	if((int)pkAddUnionMemberConfirm->sponsorLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionMemberConfirm->sponsorLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberConfirm->sponsorLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddUnionMemberConfirm->sponsor), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddUnionMemberConfirm(void* pData)
{
	AddUnionMemberConfirm* pkAddUnionMemberConfirm = (AddUnionMemberConfirm*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionMemberConfirm->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionMemberConfirm->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionMemberConfirm->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionName
	if((int)pkAddUnionMemberConfirm->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkAddUnionMemberConfirm->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberConfirm->unionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddUnionMemberConfirm->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sponsorLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionMemberConfirm->sponsorLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sponsor
	if((int)pkAddUnionMemberConfirm->sponsorLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionMemberConfirm->sponsorLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberConfirm->sponsorLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddUnionMemberConfirm->sponsor), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddUnionMemberConfirmed(void* pData)
{
	AddUnionMemberConfirmed* pkAddUnionMemberConfirmed = (AddUnionMemberConfirmed*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionMemberConfirmed->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sponsorLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionMemberConfirmed->sponsorLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sponsor
	if((int)pkAddUnionMemberConfirmed->sponsorLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionMemberConfirmed->sponsorLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberConfirmed->sponsorLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddUnionMemberConfirmed->sponsor), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionMemberConfirmed->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionName
	if((int)pkAddUnionMemberConfirmed->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkAddUnionMemberConfirmed->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberConfirmed->unionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddUnionMemberConfirmed->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionMemberConfirmed->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddUnionMemberConfirmed(void* pData)
{
	AddUnionMemberConfirmed* pkAddUnionMemberConfirmed = (AddUnionMemberConfirmed*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionMemberConfirmed->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sponsorLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionMemberConfirmed->sponsorLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sponsor
	if((int)pkAddUnionMemberConfirmed->sponsorLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionMemberConfirmed->sponsorLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberConfirmed->sponsorLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddUnionMemberConfirmed->sponsor), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionMemberConfirmed->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionName
	if((int)pkAddUnionMemberConfirmed->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkAddUnionMemberConfirmed->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberConfirmed->unionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddUnionMemberConfirmed->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionMemberConfirmed->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddUnionMemberResult(void* pData)
{
	AddUnionMemberResult* pkAddUnionMemberResult = (AddUnionMemberResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkAddUnionMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberAddedNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionMemberResult->memberAddedNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberAddedName
	if((int)pkAddUnionMemberResult->memberAddedNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionMemberResult->memberAddedNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberResult->memberAddedNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddUnionMemberResult->memberAddedName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddUnionMemberResult(void* pData)
{
	AddUnionMemberResult* pkAddUnionMemberResult = (AddUnionMemberResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkAddUnionMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberAddedNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionMemberResult->memberAddedNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberAddedName
	if((int)pkAddUnionMemberResult->memberAddedNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionMemberResult->memberAddedNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionMemberResult->memberAddedNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddUnionMemberResult->memberAddedName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddUnionMemberNotify(void* pData)
{
	AddUnionMemberNotify* pkAddUnionMemberNotify = (AddUnionMemberNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberAdded
	if(EnCode__UnionMember(&(pkAddUnionMemberNotify->memberAdded)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddUnionMemberNotify(void* pData)
{
	AddUnionMemberNotify* pkAddUnionMemberNotify = (AddUnionMemberNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberAdded
	if(DeCode__UnionMember(&(pkAddUnionMemberNotify->memberAdded)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveUnionMemberRequest(void* pData)
{
	RemoveUnionMemberRequest* pkRemoveUnionMemberRequest = (RemoveUnionMemberRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionMemberRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberRemoved
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionMemberRequest->memberRemoved), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveUnionMemberRequest(void* pData)
{
	RemoveUnionMemberRequest* pkRemoveUnionMemberRequest = (RemoveUnionMemberRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionMemberRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberRemoved
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionMemberRequest->memberRemoved), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveUnionMemberResult(void* pData)
{
	RemoveUnionMemberResult* pkRemoveUnionMemberResult = (RemoveUnionMemberResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRemoveUnionMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberRemoved
	if(EnCode__UnionMember(&(pkRemoveUnionMemberResult->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveUnionMemberResult(void* pData)
{
	RemoveUnionMemberResult* pkRemoveUnionMemberResult = (RemoveUnionMemberResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRemoveUnionMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionMemberResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionMemberResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberRemoved
	if(DeCode__UnionMember(&(pkRemoveUnionMemberResult->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveUnionMemberNotify(void* pData)
{
	RemoveUnionMemberNotify* pkRemoveUnionMemberNotify = (RemoveUnionMemberNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRemoveUnionMemberNotify->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberRemoved
	if(EnCode__UnionMember(&(pkRemoveUnionMemberNotify->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveUnionMemberNotify(void* pData)
{
	RemoveUnionMemberNotify* pkRemoveUnionMemberNotify = (RemoveUnionMemberNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionMemberNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionMemberNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRemoveUnionMemberNotify->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberRemoved
	if(DeCode__UnionMember(&(pkRemoveUnionMemberNotify->memberRemoved)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionPosRightsNotify(void* pData)
{
	UnionPosRightsNotify* pkUnionPosRightsNotify = (UnionPosRightsNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionPosRightsNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionPosRightsNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rights
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionPosRightsNotify->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionPosRightsNotify->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if((int)pkUnionPosRightsNotify->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionPosRightsNotify->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionPosRightsNotify->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionPosRightsNotify->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionPosRightsNotify->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionPosRightsNotify(void* pData)
{
	UnionPosRightsNotify* pkUnionPosRightsNotify = (UnionPosRightsNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionPosRightsNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionPosRightsNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rights
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionPosRightsNotify->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionPosRightsNotify->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if((int)pkUnionPosRightsNotify->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionPosRightsNotify->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionPosRightsNotify->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionPosRightsNotify->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionPosRightsNotify->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionPosRighstRequest(void* pData)
{
	ModifyUnionPosRighstRequest* pkModifyUnionPosRighstRequest = (ModifyUnionPosRighstRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPosRighstRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyUnionPosRighstRequest->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if((int)pkModifyUnionPosRighstRequest->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkModifyUnionPosRighstRequest->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkModifyUnionPosRighstRequest->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkModifyUnionPosRighstRequest->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rights
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPosRighstRequest->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionPosRighstRequest(void* pData)
{
	ModifyUnionPosRighstRequest* pkModifyUnionPosRighstRequest = (ModifyUnionPosRighstRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPosRighstRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyUnionPosRighstRequest->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if((int)pkModifyUnionPosRighstRequest->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkModifyUnionPosRighstRequest->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkModifyUnionPosRighstRequest->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkModifyUnionPosRighstRequest->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rights
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPosRighstRequest->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionPosRighstResult(void* pData)
{
	ModifyUnionPosRighstResult* pkModifyUnionPosRighstResult = (ModifyUnionPosRighstResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkModifyUnionPosRighstResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPosRighstResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPosRighstResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyUnionPosRighstResult->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if((int)pkModifyUnionPosRighstResult->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkModifyUnionPosRighstResult->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkModifyUnionPosRighstResult->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkModifyUnionPosRighstResult->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionPosRighstResult(void* pData)
{
	ModifyUnionPosRighstResult* pkModifyUnionPosRighstResult = (ModifyUnionPosRighstResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkModifyUnionPosRighstResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPosRighstResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPosRighstResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyUnionPosRighstResult->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if((int)pkModifyUnionPosRighstResult->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkModifyUnionPosRighstResult->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkModifyUnionPosRighstResult->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkModifyUnionPosRighstResult->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddUnionPosRequest(void* pData)
{
	AddUnionPosRequest* pkAddUnionPosRequest = (AddUnionPosRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionPosRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionPosRequest->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if((int)pkAddUnionPosRequest->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionPosRequest->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionPosRequest->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddUnionPosRequest->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rights
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionPosRequest->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddUnionPosRequest(void* pData)
{
	AddUnionPosRequest* pkAddUnionPosRequest = (AddUnionPosRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionPosRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionPosRequest->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if((int)pkAddUnionPosRequest->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionPosRequest->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionPosRequest->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddUnionPosRequest->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rights
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionPosRequest->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddUnionPosResult(void* pData)
{
	AddUnionPosResult* pkAddUnionPosResult = (AddUnionPosResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkAddUnionPosResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionPosResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddUnionPosResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAddUnionPosResult->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if((int)pkAddUnionPosResult->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionPosResult->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionPosResult->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkAddUnionPosResult->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddUnionPosResult(void* pData)
{
	AddUnionPosResult* pkAddUnionPosResult = (AddUnionPosResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkAddUnionPosResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionPosResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddUnionPosResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAddUnionPosResult->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if((int)pkAddUnionPosResult->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAddUnionPosResult->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAddUnionPosResult->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAddUnionPosResult->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveUnionPosRequest(void* pData)
{
	RemoveUnionPosRequest* pkRemoveUnionPosRequest = (RemoveUnionPosRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionPosRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRemoveUnionPosRequest->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if((int)pkRemoveUnionPosRequest->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkRemoveUnionPosRequest->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRemoveUnionPosRequest->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkRemoveUnionPosRequest->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveUnionPosRequest(void* pData)
{
	RemoveUnionPosRequest* pkRemoveUnionPosRequest = (RemoveUnionPosRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionPosRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRemoveUnionPosRequest->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if((int)pkRemoveUnionPosRequest->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkRemoveUnionPosRequest->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRemoveUnionPosRequest->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRemoveUnionPosRequest->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RemoveUnionPosResult(void* pData)
{
	RemoveUnionPosResult* pkRemoveUnionPosResult = (RemoveUnionPosResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionPosResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionPosResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRemoveUnionPosResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRemoveUnionPosResult->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if((int)pkRemoveUnionPosResult->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkRemoveUnionPosResult->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRemoveUnionPosResult->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkRemoveUnionPosResult->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RemoveUnionPosResult(void* pData)
{
	RemoveUnionPosResult* pkRemoveUnionPosResult = (RemoveUnionPosResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionPosResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionPosResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRemoveUnionPosResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRemoveUnionPosResult->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if((int)pkRemoveUnionPosResult->posLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkRemoveUnionPosResult->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRemoveUnionPosResult->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRemoveUnionPosResult->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TransformUnionCaptionRequest(void* pData)
{
	TransformUnionCaptionRequest* pkTransformUnionCaptionRequest = (TransformUnionCaptionRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransformUnionCaptionRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newCaptionUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransformUnionCaptionRequest->newCaptionUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TransformUnionCaptionRequest(void* pData)
{
	TransformUnionCaptionRequest* pkTransformUnionCaptionRequest = (TransformUnionCaptionRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransformUnionCaptionRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newCaptionUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransformUnionCaptionRequest->newCaptionUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TransformUnionCaptionResult(void* pData)
{
	TransformUnionCaptionResult* pkTransformUnionCaptionResult = (TransformUnionCaptionResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransformUnionCaptionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransformUnionCaptionResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransformUnionCaptionResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TransformUnionCaptionResult(void* pData)
{
	TransformUnionCaptionResult* pkTransformUnionCaptionResult = (TransformUnionCaptionResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransformUnionCaptionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransformUnionCaptionResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransformUnionCaptionResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TransformUnionCaptionNotify(void* pData)
{
	TransformUnionCaptionNotify* pkTransformUnionCaptionNotify = (TransformUnionCaptionNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransformUnionCaptionNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransformUnionCaptionNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newCaptionNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTransformUnionCaptionNotify->newCaptionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newCaptionName
	if((int)pkTransformUnionCaptionNotify->newCaptionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkTransformUnionCaptionNotify->newCaptionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTransformUnionCaptionNotify->newCaptionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkTransformUnionCaptionNotify->newCaptionName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TransformUnionCaptionNotify(void* pData)
{
	TransformUnionCaptionNotify* pkTransformUnionCaptionNotify = (TransformUnionCaptionNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransformUnionCaptionNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransformUnionCaptionNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newCaptionNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTransformUnionCaptionNotify->newCaptionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newCaptionName
	if((int)pkTransformUnionCaptionNotify->newCaptionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkTransformUnionCaptionNotify->newCaptionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTransformUnionCaptionNotify->newCaptionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkTransformUnionCaptionNotify->newCaptionName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionMemberTitleRequest(void* pData)
{
	ModifyUnionMemberTitleRequest* pkModifyUnionMemberTitleRequest = (ModifyUnionMemberTitleRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionMemberTitleRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode modifiedUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionMemberTitleRequest->modifiedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyUnionMemberTitleRequest->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if((int)pkModifyUnionMemberTitleRequest->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkModifyUnionMemberTitleRequest->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkModifyUnionMemberTitleRequest->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkModifyUnionMemberTitleRequest->title), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionMemberTitleRequest(void* pData)
{
	ModifyUnionMemberTitleRequest* pkModifyUnionMemberTitleRequest = (ModifyUnionMemberTitleRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionMemberTitleRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode modifiedUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionMemberTitleRequest->modifiedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyUnionMemberTitleRequest->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if((int)pkModifyUnionMemberTitleRequest->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkModifyUnionMemberTitleRequest->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkModifyUnionMemberTitleRequest->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkModifyUnionMemberTitleRequest->title), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionMemberTitleResult(void* pData)
{
	ModifyUnionMemberTitleResult* pkModifyUnionMemberTitleResult = (ModifyUnionMemberTitleResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionMemberTitleResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionMemberTitleResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionMemberTitleResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionMemberTitleResult(void* pData)
{
	ModifyUnionMemberTitleResult* pkModifyUnionMemberTitleResult = (ModifyUnionMemberTitleResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionMemberTitleResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionMemberTitleResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionMemberTitleResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionMemberTitleNotify(void* pData)
{
	UnionMemberTitleNotify* pkUnionMemberTitleNotify = (UnionMemberTitleNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMemberTitleNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMemberTitleNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMemberTitleNotify->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if((int)pkUnionMemberTitleNotify->memberNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionMemberTitleNotify->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMemberTitleNotify->memberNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionMemberTitleNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMemberTitleNotify->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if((int)pkUnionMemberTitleNotify->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionMemberTitleNotify->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMemberTitleNotify->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionMemberTitleNotify->title), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionMemberTitleNotify(void* pData)
{
	UnionMemberTitleNotify* pkUnionMemberTitleNotify = (UnionMemberTitleNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMemberTitleNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMemberTitleNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMemberTitleNotify->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if((int)pkUnionMemberTitleNotify->memberNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionMemberTitleNotify->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMemberTitleNotify->memberNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionMemberTitleNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMemberTitleNotify->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if((int)pkUnionMemberTitleNotify->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionMemberTitleNotify->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMemberTitleNotify->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionMemberTitleNotify->title), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AdvanceUnionMemberPosRequest(void* pData)
{
	AdvanceUnionMemberPosRequest* pkAdvanceUnionMemberPosRequest = (AdvanceUnionMemberPosRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionMemberPosRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode advancedUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionMemberPosRequest->advancedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AdvanceUnionMemberPosRequest(void* pData)
{
	AdvanceUnionMemberPosRequest* pkAdvanceUnionMemberPosRequest = (AdvanceUnionMemberPosRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionMemberPosRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode advancedUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionMemberPosRequest->advancedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AdvanceUnionMemberPosResult(void* pData)
{
	AdvanceUnionMemberPosResult* pkAdvanceUnionMemberPosResult = (AdvanceUnionMemberPosResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionMemberPosResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionMemberPosResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionMemberPosResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AdvanceUnionMemberPosResult(void* pData)
{
	AdvanceUnionMemberPosResult* pkAdvanceUnionMemberPosResult = (AdvanceUnionMemberPosResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionMemberPosResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionMemberPosResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionMemberPosResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AdvanceUnionMemberPosNotify(void* pData)
{
	AdvanceUnionMemberPosNotify* pkAdvanceUnionMemberPosNotify = (AdvanceUnionMemberPosNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionMemberPosNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionMemberPosNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAdvanceUnionMemberPosNotify->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if((int)pkAdvanceUnionMemberPosNotify->memberNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAdvanceUnionMemberPosNotify->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAdvanceUnionMemberPosNotify->memberNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkAdvanceUnionMemberPosNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posSeq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkAdvanceUnionMemberPosNotify->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AdvanceUnionMemberPosNotify(void* pData)
{
	AdvanceUnionMemberPosNotify* pkAdvanceUnionMemberPosNotify = (AdvanceUnionMemberPosNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionMemberPosNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionMemberPosNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAdvanceUnionMemberPosNotify->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if((int)pkAdvanceUnionMemberPosNotify->memberNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkAdvanceUnionMemberPosNotify->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkAdvanceUnionMemberPosNotify->memberNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkAdvanceUnionMemberPosNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posSeq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkAdvanceUnionMemberPosNotify->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ReduceUnionMemberPosRequest(void* pData)
{
	ReduceUnionMemberPosRequest* pkReduceUnionMemberPosRequest = (ReduceUnionMemberPosRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReduceUnionMemberPosRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode reducedUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReduceUnionMemberPosRequest->reducedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ReduceUnionMemberPosRequest(void* pData)
{
	ReduceUnionMemberPosRequest* pkReduceUnionMemberPosRequest = (ReduceUnionMemberPosRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReduceUnionMemberPosRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode reducedUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReduceUnionMemberPosRequest->reducedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ReduceUnionMemberPosResult(void* pData)
{
	ReduceUnionMemberPosResult* pkReduceUnionMemberPosResult = (ReduceUnionMemberPosResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReduceUnionMemberPosResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReduceUnionMemberPosResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReduceUnionMemberPosResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ReduceUnionMemberPosResult(void* pData)
{
	ReduceUnionMemberPosResult* pkReduceUnionMemberPosResult = (ReduceUnionMemberPosResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReduceUnionMemberPosResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReduceUnionMemberPosResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReduceUnionMemberPosResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ReduceUnionMemberPosNotify(void* pData)
{
	ReduceUnionMemberPosNotify* pkReduceUnionMemberPosNotify = (ReduceUnionMemberPosNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReduceUnionMemberPosNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkReduceUnionMemberPosNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkReduceUnionMemberPosNotify->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if((int)pkReduceUnionMemberPosNotify->memberNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkReduceUnionMemberPosNotify->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkReduceUnionMemberPosNotify->memberNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkReduceUnionMemberPosNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posSeq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkReduceUnionMemberPosNotify->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ReduceUnionMemberPosNotify(void* pData)
{
	ReduceUnionMemberPosNotify* pkReduceUnionMemberPosNotify = (ReduceUnionMemberPosNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReduceUnionMemberPosNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkReduceUnionMemberPosNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkReduceUnionMemberPosNotify->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if((int)pkReduceUnionMemberPosNotify->memberNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkReduceUnionMemberPosNotify->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkReduceUnionMemberPosNotify->memberNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkReduceUnionMemberPosNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posSeq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkReduceUnionMemberPosNotify->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionChannelSpeekRequest(void* pData)
{
	UnionChannelSpeekRequest* pkUnionChannelSpeekRequest = (UnionChannelSpeekRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionChannelSpeekRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionChannelSpeekRequest->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if((int)pkUnionChannelSpeekRequest->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_DATA_LEN < pkUnionChannelSpeekRequest->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionChannelSpeekRequest->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionChannelSpeekRequest->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionChannelSpeekRequest(void* pData)
{
	UnionChannelSpeekRequest* pkUnionChannelSpeekRequest = (UnionChannelSpeekRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionChannelSpeekRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionChannelSpeekRequest->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if((int)pkUnionChannelSpeekRequest->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_DATA_LEN < pkUnionChannelSpeekRequest->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionChannelSpeekRequest->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionChannelSpeekRequest->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionChannelSpeekResult(void* pData)
{
	UnionChannelSpeekResult* pkUnionChannelSpeekResult = (UnionChannelSpeekResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionChannelSpeekResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionChannelSpeekResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionChannelSpeekResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionChannelSpeekResult(void* pData)
{
	UnionChannelSpeekResult* pkUnionChannelSpeekResult = (UnionChannelSpeekResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionChannelSpeekResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionChannelSpeekResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionChannelSpeekResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionChannelSpeekNotify(void* pData)
{
	UnionChannelSpeekNotify* pkUnionChannelSpeekNotify = (UnionChannelSpeekNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionChannelSpeekNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionChannelSpeekNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode speekerNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionChannelSpeekNotify->speekerNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode speekerName
	if((int)pkUnionChannelSpeekNotify->speekerNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionChannelSpeekNotify->speekerNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionChannelSpeekNotify->speekerNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionChannelSpeekNotify->speekerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode speekerLine
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionChannelSpeekNotify->speekerLine), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionChannelSpeekNotify->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if((int)pkUnionChannelSpeekNotify->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_DATA_LEN < pkUnionChannelSpeekNotify->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionChannelSpeekNotify->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionChannelSpeekNotify->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionChannelSpeekNotify(void* pData)
{
	UnionChannelSpeekNotify* pkUnionChannelSpeekNotify = (UnionChannelSpeekNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionChannelSpeekNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionChannelSpeekNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode speekerNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionChannelSpeekNotify->speekerNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode speekerName
	if((int)pkUnionChannelSpeekNotify->speekerNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionChannelSpeekNotify->speekerNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionChannelSpeekNotify->speekerNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionChannelSpeekNotify->speekerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode speekerLine
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionChannelSpeekNotify->speekerLine), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionChannelSpeekNotify->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if((int)pkUnionChannelSpeekNotify->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_CHAT_DATA_LEN < pkUnionChannelSpeekNotify->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionChannelSpeekNotify->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionChannelSpeekNotify->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UpdateUnionPosCfgRequest(void* pData)
{
	UpdateUnionPosCfgRequest* pkUpdateUnionPosCfgRequest = (UpdateUnionPosCfgRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateUnionPosCfgRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posListLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUpdateUnionPosCfgRequest->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posList
	if((int)pkUpdateUnionPosCfgRequest->posListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkUpdateUnionPosCfgRequest->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUpdateUnionPosCfgRequest->posListLen; ++i)
	{
		if(EnCode__UnionPosRight(&(pkUpdateUnionPosCfgRequest->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UpdateUnionPosCfgRequest(void* pData)
{
	UpdateUnionPosCfgRequest* pkUpdateUnionPosCfgRequest = (UpdateUnionPosCfgRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateUnionPosCfgRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posListLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUpdateUnionPosCfgRequest->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posList
	if((int)pkUpdateUnionPosCfgRequest->posListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkUpdateUnionPosCfgRequest->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUpdateUnionPosCfgRequest->posListLen; ++i)
	{
		if(DeCode__UnionPosRight(&(pkUpdateUnionPosCfgRequest->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UpdateUnionPosCfgResult(void* pData)
{
	UpdateUnionPosCfgResult* pkUpdateUnionPosCfgResult = (UpdateUnionPosCfgResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateUnionPosCfgResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateUnionPosCfgResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateUnionPosCfgResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UpdateUnionPosCfgResult(void* pData)
{
	UpdateUnionPosCfgResult* pkUpdateUnionPosCfgResult = (UpdateUnionPosCfgResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateUnionPosCfgResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateUnionPosCfgResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateUnionPosCfgResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UpdateUnionPosCfgNotify(void* pData)
{
	UpdateUnionPosCfgNotify* pkUpdateUnionPosCfgNotify = (UpdateUnionPosCfgNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateUnionPosCfgNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUpdateUnionPosCfgNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posListLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUpdateUnionPosCfgNotify->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posList
	if((int)pkUpdateUnionPosCfgNotify->posListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkUpdateUnionPosCfgNotify->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUpdateUnionPosCfgNotify->posListLen; ++i)
	{
		if(EnCode__UnionPosRight(&(pkUpdateUnionPosCfgNotify->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UpdateUnionPosCfgNotify(void* pData)
{
	UpdateUnionPosCfgNotify* pkUpdateUnionPosCfgNotify = (UpdateUnionPosCfgNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateUnionPosCfgNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUpdateUnionPosCfgNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posListLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUpdateUnionPosCfgNotify->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posList
	if((int)pkUpdateUnionPosCfgNotify->posListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkUpdateUnionPosCfgNotify->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUpdateUnionPosCfgNotify->posListLen; ++i)
	{
		if(DeCode__UnionPosRight(&(pkUpdateUnionPosCfgNotify->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ForbidUnionSpeekRequest(void* pData)
{
	ForbidUnionSpeekRequest* pkForbidUnionSpeekRequest = (ForbidUnionSpeekRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkForbidUnionSpeekRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode forbidUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkForbidUnionSpeekRequest->forbidUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ForbidUnionSpeekRequest(void* pData)
{
	ForbidUnionSpeekRequest* pkForbidUnionSpeekRequest = (ForbidUnionSpeekRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkForbidUnionSpeekRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode forbidUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkForbidUnionSpeekRequest->forbidUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ForbidUnionSpeekResult(void* pData)
{
	ForbidUnionSpeekResult* pkForbidUnionSpeekResult = (ForbidUnionSpeekResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkForbidUnionSpeekResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkForbidUnionSpeekResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkForbidUnionSpeekResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ForbidUnionSpeekResult(void* pData)
{
	ForbidUnionSpeekResult* pkForbidUnionSpeekResult = (ForbidUnionSpeekResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkForbidUnionSpeekResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkForbidUnionSpeekResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkForbidUnionSpeekResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ForbidUnionSpeekNotify(void* pData)
{
	ForbidUnionSpeekNotify* pkForbidUnionSpeekNotify = (ForbidUnionSpeekNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkForbidUnionSpeekNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkForbidUnionSpeekNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkForbidUnionSpeekNotify->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if((int)pkForbidUnionSpeekNotify->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkForbidUnionSpeekNotify->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkForbidUnionSpeekNotify->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkForbidUnionSpeekNotify->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ForbidUnionSpeekNotify(void* pData)
{
	ForbidUnionSpeekNotify* pkForbidUnionSpeekNotify = (ForbidUnionSpeekNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkForbidUnionSpeekNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkForbidUnionSpeekNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkForbidUnionSpeekNotify->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if((int)pkForbidUnionSpeekNotify->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkForbidUnionSpeekNotify->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkForbidUnionSpeekNotify->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkForbidUnionSpeekNotify->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionSkill(void* pData)
{
	UnionSkill* pkUnionSkill = (UnionSkill*)(pData);

	//EnCode skillId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionSkill->skillId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionSkill->skillLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionSkill(void* pData)
{
	UnionSkill* pkUnionSkill = (UnionSkill*)(pData);

	//DeCode skillId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionSkill->skillId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionSkill->skillLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionSkillsListNotify(void* pData)
{
	UnionSkillsListNotify* pkUnionSkillsListNotify = (UnionSkillsListNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionSkillsListNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionSkillsListNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bGate
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkUnionSkillsListNotify->bGate), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode guildId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionSkillsListNotify->guildId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillsListLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionSkillsListNotify->skillsListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillsList
	if((int)pkUnionSkillsListNotify->skillsListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkUnionSkillsListNotify->skillsListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnionSkillsListNotify->skillsListLen; ++i)
	{
		if(EnCode__UnionSkill(&(pkUnionSkillsListNotify->skillsList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionSkillsListNotify(void* pData)
{
	UnionSkillsListNotify* pkUnionSkillsListNotify = (UnionSkillsListNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionSkillsListNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionSkillsListNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bGate
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionSkillsListNotify->bGate), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode guildId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionSkillsListNotify->guildId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillsListLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionSkillsListNotify->skillsListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillsList
	if((int)pkUnionSkillsListNotify->skillsListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkUnionSkillsListNotify->skillsListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnionSkillsListNotify->skillsListLen; ++i)
	{
		if(DeCode__UnionSkill(&(pkUnionSkillsListNotify->skillsList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__StudyUnionSkillRequest(void* pData)
{
	StudyUnionSkillRequest* pkStudyUnionSkillRequest = (StudyUnionSkillRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkStudyUnionSkillRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkStudyUnionSkillRequest->skillId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__StudyUnionSkillRequest(void* pData)
{
	StudyUnionSkillRequest* pkStudyUnionSkillRequest = (StudyUnionSkillRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkStudyUnionSkillRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkStudyUnionSkillRequest->skillId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__StudyUnionSkillResult(void* pData)
{
	StudyUnionSkillResult* pkStudyUnionSkillResult = (StudyUnionSkillResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkStudyUnionSkillResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkStudyUnionSkillResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkStudyUnionSkillResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkStudyUnionSkillResult->skillId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__StudyUnionSkillResult(void* pData)
{
	StudyUnionSkillResult* pkStudyUnionSkillResult = (StudyUnionSkillResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkStudyUnionSkillResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkStudyUnionSkillResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkStudyUnionSkillResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkStudyUnionSkillResult->skillId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionActivePointRequest(void* pData)
{
	ModifyUnionActivePointRequest* pkModifyUnionActivePointRequest = (ModifyUnionActivePointRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionActivePointRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode point
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionActivePointRequest->point), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bAdd
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkModifyUnionActivePointRequest->bAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionActivePointRequest(void* pData)
{
	ModifyUnionActivePointRequest* pkModifyUnionActivePointRequest = (ModifyUnionActivePointRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionActivePointRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode point
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionActivePointRequest->point), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bAdd
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkModifyUnionActivePointRequest->bAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionActivePointResult(void* pData)
{
	ModifyUnionActivePointResult* pkModifyUnionActivePointResult = (ModifyUnionActivePointResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionActivePointResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionActivePointResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionActivePointResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionActivePointResult(void* pData)
{
	ModifyUnionActivePointResult* pkModifyUnionActivePointResult = (ModifyUnionActivePointResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionActivePointResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionActivePointResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionActivePointResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionActivePointNotify(void* pData)
{
	UnionActivePointNotify* pkUnionActivePointNotify = (UnionActivePointNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionActivePointNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionActivePointNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode point
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionActivePointNotify->point), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionActivePointNotify(void* pData)
{
	UnionActivePointNotify* pkUnionActivePointNotify = (UnionActivePointNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionActivePointNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionActivePointNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode point
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionActivePointNotify->point), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionTaskList(void* pData)
{
	UnionTaskList* pkUnionTaskList = (UnionTaskList*)(pData);

	//EnCode taskCount
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionTaskList->taskCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode task
	if((int)pkUnionTaskList->taskCount < 0)
	{
		return FAILEDRETCODE;
	}

	if(20 < pkUnionTaskList->taskCount)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionTaskList->taskCount;
	if(!m_kPackage.Pack("UINT", &(pkUnionTaskList->task), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionTaskList(void* pData)
{
	UnionTaskList* pkUnionTaskList = (UnionTaskList*)(pData);

	//DeCode taskCount
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionTaskList->taskCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode task
	if((int)pkUnionTaskList->taskCount < 0)
	{
		return FAILEDRETCODE;
	}

	if(20 < pkUnionTaskList->taskCount)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionTaskList->taskCount;
	if(!m_kPackage.UnPack("UINT", &(pkUnionTaskList->task), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__PostUnionTasksListRequest(void* pData)
{
	PostUnionTasksListRequest* pkPostUnionTasksListRequest = (PostUnionTasksListRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionTasksListRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode durationTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionTasksListRequest->durationTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__PostUnionTasksListRequest(void* pData)
{
	PostUnionTasksListRequest* pkPostUnionTasksListRequest = (PostUnionTasksListRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionTasksListRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode durationTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionTasksListRequest->durationTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__PostUnionTasksListResult(void* pData)
{
	PostUnionTasksListResult* pkPostUnionTasksListResult = (PostUnionTasksListResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionTasksListResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionTasksListResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionTasksListResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__PostUnionTasksListResult(void* pData)
{
	PostUnionTasksListResult* pkPostUnionTasksListResult = (PostUnionTasksListResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionTasksListResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionTasksListResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionTasksListResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionTasksListNotify(void* pData)
{
	UnionTasksListNotify* pkUnionTasksListNotify = (UnionTasksListNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionTasksListNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionTasksListNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode guildIt
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionTasksListNotify->guildIt), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode endTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionTasksListNotify->endTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode activePoint
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionTasksListNotify->activePoint), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionTasksListNotify(void* pData)
{
	UnionTasksListNotify* pkUnionTasksListNotify = (UnionTasksListNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionTasksListNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionTasksListNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode guildIt
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionTasksListNotify->guildIt), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode endTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionTasksListNotify->endTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode activePoint
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionTasksListNotify->activePoint), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AdvanceUnionLevelRequest(void* pData)
{
	AdvanceUnionLevelRequest* pkAdvanceUnionLevelRequest = (AdvanceUnionLevelRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionLevelRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AdvanceUnionLevelRequest(void* pData)
{
	AdvanceUnionLevelRequest* pkAdvanceUnionLevelRequest = (AdvanceUnionLevelRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionLevelRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AdvanceUnionLevelResult(void* pData)
{
	AdvanceUnionLevelResult* pkAdvanceUnionLevelResult = (AdvanceUnionLevelResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionLevelResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionLevelResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAdvanceUnionLevelResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AdvanceUnionLevelResult(void* pData)
{
	AdvanceUnionLevelResult* pkAdvanceUnionLevelResult = (AdvanceUnionLevelResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionLevelResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionLevelResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAdvanceUnionLevelResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionLevelNotify(void* pData)
{
	UnionLevelNotify* pkUnionLevelNotify = (UnionLevelNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionLevelNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionLevelNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionLevelNotify->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionLevelNotify->active), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionLevelNotify(void* pData)
{
	UnionLevelNotify* pkUnionLevelNotify = (UnionLevelNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionLevelNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionLevelNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionLevelNotify->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionLevelNotify->active), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__PostUnionBulletinRequest(void* pData)
{
	PostUnionBulletinRequest* pkPostUnionBulletinRequest = (PostUnionBulletinRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionBulletinRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPostUnionBulletinRequest->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode end
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPostUnionBulletinRequest->end), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPostUnionBulletinRequest->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if((int)pkPostUnionBulletinRequest->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(400 < pkPostUnionBulletinRequest->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPostUnionBulletinRequest->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkPostUnionBulletinRequest->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__PostUnionBulletinRequest(void* pData)
{
	PostUnionBulletinRequest* pkPostUnionBulletinRequest = (PostUnionBulletinRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionBulletinRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPostUnionBulletinRequest->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode end
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPostUnionBulletinRequest->end), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPostUnionBulletinRequest->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if((int)pkPostUnionBulletinRequest->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(400 < pkPostUnionBulletinRequest->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPostUnionBulletinRequest->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPostUnionBulletinRequest->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__PostUnionBulletinResult(void* pData)
{
	PostUnionBulletinResult* pkPostUnionBulletinResult = (PostUnionBulletinResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionBulletinResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionBulletinResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPostUnionBulletinResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__PostUnionBulletinResult(void* pData)
{
	PostUnionBulletinResult* pkPostUnionBulletinResult = (PostUnionBulletinResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionBulletinResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionBulletinResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPostUnionBulletinResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionBulletinNotify(void* pData)
{
	UnionBulletinNotify* pkUnionBulletinNotify = (UnionBulletinNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionBulletinNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionBulletinNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode seq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionBulletinNotify->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionBulletinNotify->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if((int)pkUnionBulletinNotify->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(400 < pkUnionBulletinNotify->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionBulletinNotify->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionBulletinNotify->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionBulletinNotify(void* pData)
{
	UnionBulletinNotify* pkUnionBulletinNotify = (UnionBulletinNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionBulletinNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionBulletinNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode seq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionBulletinNotify->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionBulletinNotify->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if((int)pkUnionBulletinNotify->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(400 < pkUnionBulletinNotify->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionBulletinNotify->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionBulletinNotify->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionMemberUplineNotify(void* pData)
{
	UnionMemberUplineNotify* pkUnionMemberUplineNotify = (UnionMemberUplineNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMemberUplineNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMemberUplineNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uplinePlayer
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMemberUplineNotify->uplinePlayer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionMemberUplineNotify(void* pData)
{
	UnionMemberUplineNotify* pkUnionMemberUplineNotify = (UnionMemberUplineNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMemberUplineNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMemberUplineNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uplinePlayer
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMemberUplineNotify->uplinePlayer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionMemberOfflineNotify(void* pData)
{
	UnionMemberOfflineNotify* pkUnionMemberOfflineNotify = (UnionMemberOfflineNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMemberOfflineNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMemberOfflineNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode offlinePlayer
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMemberOfflineNotify->offlinePlayer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionMemberOfflineNotify(void* pData)
{
	UnionMemberOfflineNotify* pkUnionMemberOfflineNotify = (UnionMemberOfflineNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMemberOfflineNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMemberOfflineNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode offlinePlayer
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMemberOfflineNotify->offlinePlayer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__LogEntryParam(void* pData)
{
	LogEntryParam* pkLogEntryParam = (LogEntryParam*)(pData);

	//EnCode paramLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkLogEntryParam->paramLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode param
	if((int)pkLogEntryParam->paramLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(32 < pkLogEntryParam->paramLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkLogEntryParam->paramLen;
	if(!m_kPackage.Pack("CHAR", &(pkLogEntryParam->param), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__LogEntryParam(void* pData)
{
	LogEntryParam* pkLogEntryParam = (LogEntryParam*)(pData);

	//DeCode paramLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkLogEntryParam->paramLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode param
	if((int)pkLogEntryParam->paramLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(32 < pkLogEntryParam->paramLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkLogEntryParam->paramLen;
	if(!m_kPackage.UnPack("CHAR", &(pkLogEntryParam->param), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionLogEntry(void* pData)
{
	UnionLogEntry* pkUnionLogEntry = (UnionLogEntry*)(pData);

	//EnCode eventId
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionLogEntry->eventId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode datetime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionLogEntry->datetime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode param1
	if(EnCode__LogEntryParam(&(pkUnionLogEntry->param1)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode param2
	if(EnCode__LogEntryParam(&(pkUnionLogEntry->param2)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionLogEntry(void* pData)
{
	UnionLogEntry* pkUnionLogEntry = (UnionLogEntry*)(pData);

	//DeCode eventId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionLogEntry->eventId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode datetime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionLogEntry->datetime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode param1
	if(DeCode__LogEntryParam(&(pkUnionLogEntry->param1)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode param2
	if(DeCode__LogEntryParam(&(pkUnionLogEntry->param2)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryUnionLogRequest(void* pData)
{
	QueryUnionLogRequest* pkQueryUnionLogRequest = (QueryUnionLogRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionLogRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bookmark
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionLogRequest->bookmark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryUnionLogRequest(void* pData)
{
	QueryUnionLogRequest* pkQueryUnionLogRequest = (QueryUnionLogRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionLogRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bookmark
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionLogRequest->bookmark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryUnionLogResult(void* pData)
{
	QueryUnionLogResult* pkQueryUnionLogResult = (QueryUnionLogResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionLogResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionLogResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode logListLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkQueryUnionLogResult->logListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode logList
	if((int)pkQueryUnionLogResult->logListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(5 < pkQueryUnionLogResult->logListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkQueryUnionLogResult->logListLen; ++i)
	{
		if(EnCode__UnionLogEntry(&(pkQueryUnionLogResult->logList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode bookmark
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionLogResult->bookmark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryUnionLogResult(void* pData)
{
	QueryUnionLogResult* pkQueryUnionLogResult = (QueryUnionLogResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionLogResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionLogResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode logListLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkQueryUnionLogResult->logListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode logList
	if((int)pkQueryUnionLogResult->logListLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(5 < pkQueryUnionLogResult->logListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkQueryUnionLogResult->logListLen; ++i)
	{
		if(DeCode__UnionLogEntry(&(pkQueryUnionLogResult->logList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode bookmark
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionLogResult->bookmark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionEmailNotify(void* pData)
{
	UnionEmailNotify* pkUnionEmailNotify = (UnionEmailNotify*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionEmailNotify->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionEmailNotify->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if((int)pkUnionEmailNotify->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(16 < pkUnionEmailNotify->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionEmailNotify->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionEmailNotify->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionEmailNotify->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode emailTitle
	if((int)pkUnionEmailNotify->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(40 < pkUnionEmailNotify->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionEmailNotify->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionEmailNotify->emailTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionEmailNotify->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode emailContent
	if((int)pkUnionEmailNotify->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(300 < pkUnionEmailNotify->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionEmailNotify->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionEmailNotify->emailContent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionEmailNotify(void* pData)
{
	UnionEmailNotify* pkUnionEmailNotify = (UnionEmailNotify*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionEmailNotify->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionEmailNotify->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if((int)pkUnionEmailNotify->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(16 < pkUnionEmailNotify->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionEmailNotify->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionEmailNotify->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionEmailNotify->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode emailTitle
	if((int)pkUnionEmailNotify->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(40 < pkUnionEmailNotify->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionEmailNotify->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionEmailNotify->emailTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionEmailNotify->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode emailContent
	if((int)pkUnionEmailNotify->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(300 < pkUnionEmailNotify->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionEmailNotify->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionEmailNotify->emailContent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionOwnerCheckRequest(void* pData)
{
	UnionOwnerCheckRequest* pkUnionOwnerCheckRequest = (UnionOwnerCheckRequest*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionOwnerCheckRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionOwnerCheckRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionOwnerCheckRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionOwnerCheckRequest(void* pData)
{
	UnionOwnerCheckRequest* pkUnionOwnerCheckRequest = (UnionOwnerCheckRequest*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionOwnerCheckRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionOwnerCheckRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionOwnerCheckRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionOwnerCheckResult(void* pData)
{
	UnionOwnerCheckResult* pkUnionOwnerCheckResult = (UnionOwnerCheckResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionOwnerCheckResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionOwnerCheckResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionOwnerCheckResult->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionOwnerCheckResult->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionOwnerCheckResult(void* pData)
{
	UnionOwnerCheckResult* pkUnionOwnerCheckResult = (UnionOwnerCheckResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionOwnerCheckResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionOwnerCheckResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionOwnerCheckResult->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionOwnerCheckResult->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionPrestigeRequest(void* pData)
{
	ModifyUnionPrestigeRequest* pkModifyUnionPrestigeRequest = (ModifyUnionPrestigeRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPrestigeRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode point
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPrestigeRequest->point), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bAdd
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkModifyUnionPrestigeRequest->bAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionPrestigeRequest(void* pData)
{
	ModifyUnionPrestigeRequest* pkModifyUnionPrestigeRequest = (ModifyUnionPrestigeRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPrestigeRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode point
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPrestigeRequest->point), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bAdd
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkModifyUnionPrestigeRequest->bAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionPrestigeResult(void* pData)
{
	ModifyUnionPrestigeResult* pkModifyUnionPrestigeResult = (ModifyUnionPrestigeResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPrestigeResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPrestigeResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionPrestigeResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionPrestigeResult(void* pData)
{
	ModifyUnionPrestigeResult* pkModifyUnionPrestigeResult = (ModifyUnionPrestigeResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPrestigeResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPrestigeResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionPrestigeResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionPrestigeNotify(void* pData)
{
	UnionPrestigeNotify* pkUnionPrestigeNotify = (UnionPrestigeNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionPrestigeNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionPrestigeNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode prestige
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionPrestigeNotify->prestige), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionPrestigeNotify(void* pData)
{
	UnionPrestigeNotify* pkUnionPrestigeNotify = (UnionPrestigeNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionPrestigeNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionPrestigeNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode prestige
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionPrestigeNotify->prestige), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionBadgeRequest(void* pData)
{
	ModifyUnionBadgeRequest* pkModifyUnionBadgeRequest = (ModifyUnionBadgeRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionBadgeRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dataLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionBadgeRequest->dataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode badgeData
	if((int)pkModifyUnionBadgeRequest->dataLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(200 < pkModifyUnionBadgeRequest->dataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkModifyUnionBadgeRequest->dataLen;
	if(!m_kPackage.Pack("CHAR", &(pkModifyUnionBadgeRequest->badgeData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionBadgeRequest(void* pData)
{
	ModifyUnionBadgeRequest* pkModifyUnionBadgeRequest = (ModifyUnionBadgeRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionBadgeRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dataLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionBadgeRequest->dataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode badgeData
	if((int)pkModifyUnionBadgeRequest->dataLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(200 < pkModifyUnionBadgeRequest->dataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkModifyUnionBadgeRequest->dataLen;
	if(!m_kPackage.UnPack("CHAR", &(pkModifyUnionBadgeRequest->badgeData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionBadgeResult(void* pData)
{
	ModifyUnionBadgeResult* pkModifyUnionBadgeResult = (ModifyUnionBadgeResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionBadgeResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionBadgeResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionBadgeResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionBadgeResult(void* pData)
{
	ModifyUnionBadgeResult* pkModifyUnionBadgeResult = (ModifyUnionBadgeResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionBadgeResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionBadgeResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionBadgeResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionBadgeNotify(void* pData)
{
	UnionBadgeNotify* pkUnionBadgeNotify = (UnionBadgeNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionBadgeNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionBadgeNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dataLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionBadgeNotify->dataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode badgeData
	if((int)pkUnionBadgeNotify->dataLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(200 < pkUnionBadgeNotify->dataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionBadgeNotify->dataLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionBadgeNotify->badgeData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionBadgeNotify(void* pData)
{
	UnionBadgeNotify* pkUnionBadgeNotify = (UnionBadgeNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionBadgeNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionBadgeNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dataLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionBadgeNotify->dataLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode badgeData
	if((int)pkUnionBadgeNotify->dataLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(200 < pkUnionBadgeNotify->dataLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionBadgeNotify->dataLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionBadgeNotify->badgeData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyUnionMultiRequest(void* pData)
{
	ModifyUnionMultiRequest* pkModifyUnionMultiRequest = (ModifyUnionMultiRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionMultiRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode type
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyUnionMultiRequest->type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode multi
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkModifyUnionMultiRequest->multi), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode endTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyUnionMultiRequest->endTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyUnionMultiRequest(void* pData)
{
	ModifyUnionMultiRequest* pkModifyUnionMultiRequest = (ModifyUnionMultiRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionMultiRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode type
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyUnionMultiRequest->type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode multi
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkModifyUnionMultiRequest->multi), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode endTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyUnionMultiRequest->endTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionMultiNotify(void* pData)
{
	UnionMultiNotify* pkUnionMultiNotify = (UnionMultiNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMultiNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMultiNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode type
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMultiNotify->type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode multi
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMultiNotify->multi), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode endTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMultiNotify->endTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionMultiNotify(void* pData)
{
	UnionMultiNotify* pkUnionMultiNotify = (UnionMultiNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMultiNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMultiNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode type
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMultiNotify->type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode multi
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMultiNotify->multi), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode endTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMultiNotify->endTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryPlayerRelationRequest(void* pData)
{
	QueryPlayerRelationRequest* pkQueryPlayerRelationRequest = (QueryPlayerRelationRequest*)(pData);

	//EnCode gmGateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerRelationRequest->gmGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gmPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerRelationRequest->gmPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerRelationRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryPlayerRelationRequest(void* pData)
{
	QueryPlayerRelationRequest* pkQueryPlayerRelationRequest = (QueryPlayerRelationRequest*)(pData);

	//DeCode gmGateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerRelationRequest->gmGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gmPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerRelationRequest->gmPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerRelationRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryPlayerRelationResult(void* pData)
{
	QueryPlayerRelationResult* pkQueryPlayerRelationResult = (QueryPlayerRelationResult*)(pData);

	//EnCode gmGateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerRelationResult->gmGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gmPlayerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerRelationResult->gmPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerRelationResult->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode relation
	if(EnCode__RGFriendInfos(&(pkQueryPlayerRelationResult->relation)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryPlayerRelationResult(void* pData)
{
	QueryPlayerRelationResult* pkQueryPlayerRelationResult = (QueryPlayerRelationResult*)(pData);

	//DeCode gmGateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerRelationResult->gmGateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gmPlayerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerRelationResult->gmPlayerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerRelationResult->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode relation
	if(DeCode__RGFriendInfos(&(pkQueryPlayerRelationResult->relation)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryPlayerInfoRequest(void* pData)
{
	QueryPlayerInfoRequest* pkQueryPlayerInfoRequest = (QueryPlayerInfoRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerInfoRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerInfoRequest->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetName
	if((int)pkQueryPlayerInfoRequest->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkQueryPlayerInfoRequest->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkQueryPlayerInfoRequest->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkQueryPlayerInfoRequest->targetName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryPlayerInfoRequest(void* pData)
{
	QueryPlayerInfoRequest* pkQueryPlayerInfoRequest = (QueryPlayerInfoRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerInfoRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerInfoRequest->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetName
	if((int)pkQueryPlayerInfoRequest->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkQueryPlayerInfoRequest->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkQueryPlayerInfoRequest->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkQueryPlayerInfoRequest->targetName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryPlayerInfoResult(void* pData)
{
	QueryPlayerInfoResult* pkQueryPlayerInfoResult = (QueryPlayerInfoResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerInfoResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerInfoResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryPlayerInfoResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetInfo
	if(EnCode__FriendInfo(&(pkQueryPlayerInfoResult->targetInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryPlayerInfoResult(void* pData)
{
	QueryPlayerInfoResult* pkQueryPlayerInfoResult = (QueryPlayerInfoResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerInfoResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerInfoResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryPlayerInfoResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetInfo
	if(DeCode__FriendInfo(&(pkQueryPlayerInfoResult->targetInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryUnionRankRequest(void* pData)
{
	QueryUnionRankRequest* pkQueryUnionRankRequest = (QueryUnionRankRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionRankRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode race
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionRankRequest->race), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode page
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionRankRequest->page), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryUnionRankRequest(void* pData)
{
	QueryUnionRankRequest* pkQueryUnionRankRequest = (QueryUnionRankRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionRankRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode race
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionRankRequest->race), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode page
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionRankRequest->page), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionRankInfo(void* pData)
{
	UnionRankInfo* pkUnionRankInfo = (UnionRankInfo*)(pData);

	//EnCode unionNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRankInfo->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionName
	if((int)pkUnionRankInfo->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkUnionRankInfo->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionRankInfo->unionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionRankInfo->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRankInfo->unionLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captionNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRankInfo->captionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captionName
	if((int)pkUnionRankInfo->captionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionRankInfo->captionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionRankInfo->captionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionRankInfo->captionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRankInfo->unionRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionPretige
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRankInfo->unionPretige), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionRankInfo(void* pData)
{
	UnionRankInfo* pkUnionRankInfo = (UnionRankInfo*)(pData);

	//DeCode unionNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRankInfo->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionName
	if((int)pkUnionRankInfo->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkUnionRankInfo->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionRankInfo->unionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionRankInfo->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRankInfo->unionLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captionNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRankInfo->captionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captionName
	if((int)pkUnionRankInfo->captionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionRankInfo->captionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionRankInfo->captionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionRankInfo->captionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRankInfo->unionRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionPretige
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRankInfo->unionPretige), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__UnionRaceMasterInfo(void* pData)
{
	UnionRaceMasterInfo* pkUnionRaceMasterInfo = (UnionRaceMasterInfo*)(pData);

	//EnCode unionNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRaceMasterInfo->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionName
	if((int)pkUnionRaceMasterInfo->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkUnionRaceMasterInfo->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionRaceMasterInfo->unionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionRaceMasterInfo->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRaceMasterInfo->unionLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captionNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRaceMasterInfo->captionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captionName
	if((int)pkUnionRaceMasterInfo->captionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionRaceMasterInfo->captionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionRaceMasterInfo->captionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionRaceMasterInfo->captionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionRaceMasterInfo->unionRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode captionId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionRaceMasterInfo->captionId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__UnionRaceMasterInfo(void* pData)
{
	UnionRaceMasterInfo* pkUnionRaceMasterInfo = (UnionRaceMasterInfo*)(pData);

	//DeCode unionNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRaceMasterInfo->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionName
	if((int)pkUnionRaceMasterInfo->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkUnionRaceMasterInfo->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionRaceMasterInfo->unionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionRaceMasterInfo->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRaceMasterInfo->unionLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captionNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRaceMasterInfo->captionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captionName
	if((int)pkUnionRaceMasterInfo->captionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkUnionRaceMasterInfo->captionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionRaceMasterInfo->captionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionRaceMasterInfo->captionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionRaceMasterInfo->unionRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode captionId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionRaceMasterInfo->captionId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryUnionRankResult(void* pData)
{
	QueryUnionRankResult* pkQueryUnionRankResult = (QueryUnionRankResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionRankResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionRankResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode page
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkQueryUnionRankResult->page), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode len
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryUnionRankResult->len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rank
	if((int)pkQueryUnionRankResult->len < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkQueryUnionRankResult->len)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkQueryUnionRankResult->len; ++i)
	{
		if(EnCode__UnionRankInfo(&(pkQueryUnionRankResult->rank[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode endFlag
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkQueryUnionRankResult->endFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryUnionRankResult(void* pData)
{
	QueryUnionRankResult* pkQueryUnionRankResult = (QueryUnionRankResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionRankResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionRankResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode page
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkQueryUnionRankResult->page), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode len
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryUnionRankResult->len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rank
	if((int)pkQueryUnionRankResult->len < 0)
	{
		return FAILEDRETCODE;
	}

	if(10 < pkQueryUnionRankResult->len)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkQueryUnionRankResult->len; ++i)
	{
		if(DeCode__UnionRankInfo(&(pkQueryUnionRankResult->rank[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode endFlag
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkQueryUnionRankResult->endFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__SearchUnionRankRequest(void* pData)
{
	SearchUnionRankRequest* pkSearchUnionRankRequest = (SearchUnionRankRequest*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSearchUnionRankRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode race
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSearchUnionRankRequest->race), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSearchUnionRankRequest->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionName
	if((int)pkSearchUnionRankRequest->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkSearchUnionRankRequest->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSearchUnionRankRequest->unionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkSearchUnionRankRequest->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__SearchUnionRankRequest(void* pData)
{
	SearchUnionRankRequest* pkSearchUnionRankRequest = (SearchUnionRankRequest*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSearchUnionRankRequest->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode race
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSearchUnionRankRequest->race), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSearchUnionRankRequest->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionName
	if((int)pkSearchUnionRankRequest->unionNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkSearchUnionRankRequest->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSearchUnionRankRequest->unionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSearchUnionRankRequest->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__SearchUnionRankResult(void* pData)
{
	SearchUnionRankResult* pkSearchUnionRankResult = (SearchUnionRankResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSearchUnionRankResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSearchUnionRankResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode resultCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSearchUnionRankResult->resultCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionInfo
	if(EnCode__UnionRankInfo(&(pkSearchUnionRankResult->unionInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__SearchUnionRankResult(void* pData)
{
	SearchUnionRankResult* pkSearchUnionRankResult = (SearchUnionRankResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSearchUnionRankResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSearchUnionRankResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode resultCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSearchUnionRankResult->resultCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionInfo
	if(DeCode__UnionRankInfo(&(pkSearchUnionRankResult->unionInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__PlayerUplineTeamIdNotify(void* pData)
{
	PlayerUplineTeamIdNotify* pkPlayerUplineTeamIdNotify = (PlayerUplineTeamIdNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerUplineTeamIdNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerUplineTeamIdNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerUplineTeamIdNotify->teamId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerUplineTeamIdNotify->playerFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerUplineTeamIdNotify->teamFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__PlayerUplineTeamIdNotify(void* pData)
{
	PlayerUplineTeamIdNotify* pkPlayerUplineTeamIdNotify = (PlayerUplineTeamIdNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerUplineTeamIdNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerUplineTeamIdNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerUplineTeamIdNotify->teamId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerUplineTeamIdNotify->playerFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerUplineTeamIdNotify->teamFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryRaceMasterRequest(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryRaceMasterRequest(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RaceMaster(void* pData)
{
	RaceMaster* pkRaceMaster = (RaceMaster*)(pData);

	//EnCode race
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRaceMaster->race), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRaceMaster->playerNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerName
	if((int)pkRaceMaster->playerNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkRaceMaster->playerNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRaceMaster->playerNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkRaceMaster->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode guildId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRaceMaster->guildId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RaceMaster(void* pData)
{
	RaceMaster* pkRaceMaster = (RaceMaster*)(pData);

	//DeCode race
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRaceMaster->race), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRaceMaster->playerNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerName
	if((int)pkRaceMaster->playerNameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkRaceMaster->playerNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRaceMaster->playerNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRaceMaster->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode guildId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRaceMaster->guildId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryRaceMasterResult(void* pData)
{
	QueryRaceMasterResult* pkQueryRaceMasterResult = (QueryRaceMasterResult*)(pData);

	//EnCode len
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryRaceMasterResult->len), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode masters
	if((int)pkQueryRaceMasterResult->len < 0)
	{
		return FAILEDRETCODE;
	}

	if(5 < pkQueryRaceMasterResult->len)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkQueryRaceMasterResult->len; ++i)
	{
		if(EnCode__UnionRaceMasterInfo(&(pkQueryRaceMasterResult->masters[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryRaceMasterResult(void* pData)
{
	QueryRaceMasterResult* pkQueryRaceMasterResult = (QueryRaceMasterResult*)(pData);

	//DeCode len
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryRaceMasterResult->len), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode masters
	if((int)pkQueryRaceMasterResult->len < 0)
	{
		return FAILEDRETCODE;
	}

	if(5 < pkQueryRaceMasterResult->len)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkQueryRaceMasterResult->len; ++i)
	{
		if(DeCode__UnionRaceMasterInfo(&(pkQueryRaceMasterResult->masters[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__TeamLogNotify(void* pData)
{
	TeamLogNotify* pkTeamLogNotify = (TeamLogNotify*)(pData);

	//EnCode uiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamLogNotify->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamLogNotify->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamLogNotify->teamId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode logType
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamLogNotify->logType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__TeamLogNotify(void* pData)
{
	TeamLogNotify* pkTeamLogNotify = (TeamLogNotify*)(pData);

	//DeCode uiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamLogNotify->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamLogNotify->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamLogNotify->teamId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode logType
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamLogNotify->logType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryGuildMembersRequest(void* pData)
{
	QueryGuildMembersRequest* pkQueryGuildMembersRequest = (QueryGuildMembersRequest*)(pData);

	//EnCode rank
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryGuildMembersRequest->rank), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryGuildMembersRequest(void* pData)
{
	QueryGuildMembersRequest* pkQueryGuildMembersRequest = (QueryGuildMembersRequest*)(pData);

	//DeCode rank
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryGuildMembersRequest->rank), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GuildMember(void* pData)
{
	GuildMember* pkGuildMember = (GuildMember*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGuildMember->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if((int)pkGuildMember->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkGuildMember->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGuildMember->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGuildMember->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GuildMember(void* pData)
{
	GuildMember* pkGuildMember = (GuildMember*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGuildMember->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if((int)pkGuildMember->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkGuildMember->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGuildMember->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGuildMember->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryGuildMembersResult(void* pData)
{
	QueryGuildMembersResult* pkQueryGuildMembersResult = (QueryGuildMembersResult*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryGuildMembersResult->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode guildName
	if((int)pkQueryGuildMembersResult->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkQueryGuildMembersResult->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkQueryGuildMembersResult->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkQueryGuildMembersResult->guildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rank
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryGuildMembersResult->rank), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode race
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryGuildMembersResult->race), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bEnd
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkQueryGuildMembersResult->bEnd), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode membersLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryGuildMembersResult->membersLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if((int)pkQueryGuildMembersResult->membersLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(20 < pkQueryGuildMembersResult->membersLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkQueryGuildMembersResult->membersLen; ++i)
	{
		if(EnCode__GuildMember(&(pkQueryGuildMembersResult->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryGuildMembersResult(void* pData)
{
	QueryGuildMembersResult* pkQueryGuildMembersResult = (QueryGuildMembersResult*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryGuildMembersResult->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode guildName
	if((int)pkQueryGuildMembersResult->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_UNION_NAME_LEN < pkQueryGuildMembersResult->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkQueryGuildMembersResult->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkQueryGuildMembersResult->guildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rank
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryGuildMembersResult->rank), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode race
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryGuildMembersResult->race), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bEnd
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkQueryGuildMembersResult->bEnd), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode membersLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryGuildMembersResult->membersLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if((int)pkQueryGuildMembersResult->membersLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(20 < pkQueryGuildMembersResult->membersLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkQueryGuildMembersResult->membersLen; ++i)
	{
		if(DeCode__GuildMember(&(pkQueryGuildMembersResult->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddFriendlyDegree(void* pData)
{
	AddFriendlyDegree* pkAddFriendlyDegree = (AddFriendlyDegree*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddFriendlyDegree->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddFriendlyDegree->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddFriendlyDegree->friendUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode degreeAdded
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddFriendlyDegree->degreeAdded), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddFriendlyDegree(void* pData)
{
	AddFriendlyDegree* pkAddFriendlyDegree = (AddFriendlyDegree*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddFriendlyDegree->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddFriendlyDegree->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddFriendlyDegree->friendUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode degreeAdded
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddFriendlyDegree->degreeAdded), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__Tweet(void* pData)
{
	Tweet* pkTweet = (Tweet*)(pData);

	//EnCode type
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTweet->type), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode createdTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTweet->createdTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tweetLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTweet->tweetLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tweet
	if((int)pkTweet->tweetLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(400 < pkTweet->tweetLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTweet->tweetLen;
	if(!m_kPackage.Pack("CHAR", &(pkTweet->tweet), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__Tweet(void* pData)
{
	Tweet* pkTweet = (Tweet*)(pData);

	//DeCode type
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTweet->type), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode createdTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTweet->createdTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tweetLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTweet->tweetLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tweet
	if((int)pkTweet->tweetLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(400 < pkTweet->tweetLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTweet->tweetLen;
	if(!m_kPackage.UnPack("CHAR", &(pkTweet->tweet), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__AddTweet(void* pData)
{
	AddTweet* pkAddTweet = (AddTweet*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTweet->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkAddTweet->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tweet
	if(EnCode__Tweet(&(pkAddTweet->tweet)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__AddTweet(void* pData)
{
	AddTweet* pkAddTweet = (AddTweet*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTweet->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkAddTweet->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tweet
	if(DeCode__Tweet(&(pkAddTweet->tweet)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ShowFriendTweet(void* pData)
{
	ShowFriendTweet* pkShowFriendTweet = (ShowFriendTweet*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkShowFriendTweet->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkShowFriendTweet->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkShowFriendTweet->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendName
	if((int)pkShowFriendTweet->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkShowFriendTweet->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkShowFriendTweet->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkShowFriendTweet->friendName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tweet
	if(EnCode__Tweet(&(pkShowFriendTweet->tweet)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ShowFriendTweet(void* pData)
{
	ShowFriendTweet* pkShowFriendTweet = (ShowFriendTweet*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkShowFriendTweet->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkShowFriendTweet->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkShowFriendTweet->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendName
	if((int)pkShowFriendTweet->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkShowFriendTweet->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkShowFriendTweet->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkShowFriendTweet->friendName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tweet
	if(DeCode__Tweet(&(pkShowFriendTweet->tweet)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryFriendTweetsRequest(void* pData)
{
	QueryFriendTweetsRequest* pkQueryFriendTweetsRequest = (QueryFriendTweetsRequest*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendTweetsRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendTweetsRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendTweetsRequest->targetUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryFriendTweetsRequest(void* pData)
{
	QueryFriendTweetsRequest* pkQueryFriendTweetsRequest = (QueryFriendTweetsRequest*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendTweetsRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendTweetsRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendTweetsRequest->targetUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryFriendTweetsResult(void* pData)
{
	QueryFriendTweetsResult* pkQueryFriendTweetsResult = (QueryFriendTweetsResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendTweetsResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendTweetsResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendTweetsResult->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendName
	if((int)pkQueryFriendTweetsResult->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkQueryFriendTweetsResult->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkQueryFriendTweetsResult->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkQueryFriendTweetsResult->friendName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tweet
	if(EnCode__Tweet(&(pkQueryFriendTweetsResult->tweet)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryFriendTweetsResult(void* pData)
{
	QueryFriendTweetsResult* pkQueryFriendTweetsResult = (QueryFriendTweetsResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendTweetsResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendTweetsResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendTweetsResult->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendName
	if((int)pkQueryFriendTweetsResult->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkQueryFriendTweetsResult->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkQueryFriendTweetsResult->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkQueryFriendTweetsResult->friendName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tweet
	if(DeCode__Tweet(&(pkQueryFriendTweetsResult->tweet)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryFriendsListRequest(void* pData)
{
	QueryFriendsListRequest* pkQueryFriendsListRequest = (QueryFriendsListRequest*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendsListRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendsListRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendsListRequest->targetUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryFriendsListRequest(void* pData)
{
	QueryFriendsListRequest* pkQueryFriendsListRequest = (QueryFriendsListRequest*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendsListRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendsListRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendsListRequest->targetUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryFriendsListResult(void* pData)
{
	QueryFriendsListResult* pkQueryFriendsListResult = (QueryFriendsListResult*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendsListResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendsListResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendsListResult->targetUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lstLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryFriendsListResult->lstLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendsLst
	if((int)pkQueryFriendsListResult->lstLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(5 < pkQueryFriendsListResult->lstLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkQueryFriendsListResult->lstLen; ++i)
	{
		if(EnCode__GRInfoUpdate(&(pkQueryFriendsListResult->friendsLst[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryFriendsListResult(void* pData)
{
	QueryFriendsListResult* pkQueryFriendsListResult = (QueryFriendsListResult*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendsListResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendsListResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendsListResult->targetUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lstLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryFriendsListResult->lstLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendsLst
	if((int)pkQueryFriendsListResult->lstLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(5 < pkQueryFriendsListResult->lstLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkQueryFriendsListResult->lstLen; ++i)
	{
		if(DeCode__GRInfoUpdate(&(pkQueryFriendsListResult->friendsLst[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__NotifyExpAdded(void* pData)
{
	NotifyExpAdded* pkNotifyExpAdded = (NotifyExpAdded*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyExpAdded->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyExpAdded->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isBoss
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkNotifyExpAdded->isBoss), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode exp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyExpAdded->exp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__NotifyExpAdded(void* pData)
{
	NotifyExpAdded* pkNotifyExpAdded = (NotifyExpAdded*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyExpAdded->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyExpAdded->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isBoss
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkNotifyExpAdded->isBoss), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode exp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyExpAdded->exp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__NotifyAddExp(void* pData)
{
	NotifyAddExp* pkNotifyAddExp = (NotifyAddExp*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyAddExp->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyAddExp->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode exp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyAddExp->exp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__NotifyAddExp(void* pData)
{
	NotifyAddExp* pkNotifyAddExp = (NotifyAddExp*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyAddExp->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyAddExp->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode exp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyAddExp->exp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryExpNeededWhenLvlUpRequest(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryExpNeededWhenLvlUpRequest(void* pData)
{
	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__QueryExpNeededWhenLvlUpResult(void* pData)
{
	QueryExpNeededWhenLvlUpResult* pkQueryExpNeededWhenLvlUpResult = (QueryExpNeededWhenLvlUpResult*)(pData);

	//EnCode career
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryExpNeededWhenLvlUpResult->career), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lvlFirst
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkQueryExpNeededWhenLvlUpResult->lvlFirst), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lvlLast
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkQueryExpNeededWhenLvlUpResult->lvlLast), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lstLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkQueryExpNeededWhenLvlUpResult->lstLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lst
	if((int)pkQueryExpNeededWhenLvlUpResult->lstLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(90 < pkQueryExpNeededWhenLvlUpResult->lstLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkQueryExpNeededWhenLvlUpResult->lstLen;
	if(!m_kPackage.Pack("UINT", &(pkQueryExpNeededWhenLvlUpResult->lst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__QueryExpNeededWhenLvlUpResult(void* pData)
{
	QueryExpNeededWhenLvlUpResult* pkQueryExpNeededWhenLvlUpResult = (QueryExpNeededWhenLvlUpResult*)(pData);

	//DeCode career
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryExpNeededWhenLvlUpResult->career), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lvlFirst
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkQueryExpNeededWhenLvlUpResult->lvlFirst), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lvlLast
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkQueryExpNeededWhenLvlUpResult->lvlLast), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lstLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkQueryExpNeededWhenLvlUpResult->lstLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lst
	if((int)pkQueryExpNeededWhenLvlUpResult->lstLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(90 < pkQueryExpNeededWhenLvlUpResult->lstLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkQueryExpNeededWhenLvlUpResult->lstLen;
	if(!m_kPackage.UnPack("UINT", &(pkQueryExpNeededWhenLvlUpResult->lst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__NotifyTeamGain(void* pData)
{
	NotifyTeamGain* pkNotifyTeamGain = (NotifyTeamGain*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyTeamGain->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyTeamGain->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode percent
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkNotifyTeamGain->percent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__NotifyTeamGain(void* pData)
{
	NotifyTeamGain* pkNotifyTeamGain = (NotifyTeamGain*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyTeamGain->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyTeamGain->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode percent
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkNotifyTeamGain->percent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__ModifyTweetReceiveFlag(void* pData)
{
	ModifyTweetReceiveFlag* pkModifyTweetReceiveFlag = (ModifyTweetReceiveFlag*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTweetReceiveFlag->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTweetReceiveFlag->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkModifyTweetReceiveFlag->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__ModifyTweetReceiveFlag(void* pData)
{
	ModifyTweetReceiveFlag* pkModifyTweetReceiveFlag = (ModifyTweetReceiveFlag*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTweetReceiveFlag->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTweetReceiveFlag->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkModifyTweetReceiveFlag->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__FriendDegreeGainEmail(void* pData)
{
	FriendDegreeGainEmail* pkFriendDegreeGainEmail = (FriendDegreeGainEmail*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendDegreeGainEmail->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if((int)pkFriendDegreeGainEmail->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkFriendDegreeGainEmail->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendDegreeGainEmail->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendDegreeGainEmail->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendDegreeGainEmail->itemId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendDegreeGainEmail->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if((int)pkFriendDegreeGainEmail->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(100 < pkFriendDegreeGainEmail->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendDegreeGainEmail->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendDegreeGainEmail->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendDegreeGainEmail->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if((int)pkFriendDegreeGainEmail->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(200 < pkFriendDegreeGainEmail->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendDegreeGainEmail->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendDegreeGainEmail->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__FriendDegreeGainEmail(void* pData)
{
	FriendDegreeGainEmail* pkFriendDegreeGainEmail = (FriendDegreeGainEmail*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendDegreeGainEmail->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if((int)pkFriendDegreeGainEmail->nameLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(MAX_PLAYER_NAME_LEN < pkFriendDegreeGainEmail->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendDegreeGainEmail->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendDegreeGainEmail->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendDegreeGainEmail->itemId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendDegreeGainEmail->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if((int)pkFriendDegreeGainEmail->titleLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(100 < pkFriendDegreeGainEmail->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendDegreeGainEmail->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendDegreeGainEmail->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendDegreeGainEmail->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if((int)pkFriendDegreeGainEmail->contentLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(200 < pkFriendDegreeGainEmail->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendDegreeGainEmail->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendDegreeGainEmail->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__GRQueryNickUnion(void* pData)
{
	GRQueryNickUnion* pkGRQueryNickUnion = (GRQueryNickUnion*)(pData);

	//EnCode queryPlayerID
	if(EnCode__RPlayerID(&(pkGRQueryNickUnion->queryPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nickArrLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGRQueryNickUnion->nickArrLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickArr
	if((int)pkGRQueryNickUnion->nickArrLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(NICKUNION_QRY_LEN < pkGRQueryNickUnion->nickArrLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGRQueryNickUnion->nickArrLen; ++i)
	{
		if(EnCode__NameInfo(&(pkGRQueryNickUnion->nickArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__GRQueryNickUnion(void* pData)
{
	GRQueryNickUnion* pkGRQueryNickUnion = (GRQueryNickUnion*)(pData);

	//DeCode queryPlayerID
	if(DeCode__RPlayerID(&(pkGRQueryNickUnion->queryPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nickArrLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGRQueryNickUnion->nickArrLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickArr
	if((int)pkGRQueryNickUnion->nickArrLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(NICKUNION_QRY_LEN < pkGRQueryNickUnion->nickArrLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGRQueryNickUnion->nickArrLen; ++i)
	{
		if(DeCode__NameInfo(&(pkGRQueryNickUnion->nickArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::EnCode__RGQueryNickUnionRst(void* pData)
{
	RGQueryNickUnionRst* pkRGQueryNickUnionRst = (RGQueryNickUnionRst*)(pData);

	//EnCode queryPlayerID
	if(EnCode__RPlayerID(&(pkRGQueryNickUnionRst->queryPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nickunionArrLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRGQueryNickUnionRst->nickunionArrLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickunionArr
	if((int)pkRGQueryNickUnionRst->nickunionArrLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(NICKUNION_QRY_LEN < pkRGQueryNickUnionRst->nickunionArrLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkRGQueryNickUnionRst->nickunionArrLen; ++i)
	{
		if(EnCode__NameUnionInfo(&(pkRGQueryNickUnionRst->nickunionArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	RelationServiceNS::RelationServiceProtocol::DeCode__RGQueryNickUnionRst(void* pData)
{
	RGQueryNickUnionRst* pkRGQueryNickUnionRst = (RGQueryNickUnionRst*)(pData);

	//DeCode queryPlayerID
	if(DeCode__RPlayerID(&(pkRGQueryNickUnionRst->queryPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nickunionArrLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRGQueryNickUnionRst->nickunionArrLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickunionArr
	if((int)pkRGQueryNickUnionRst->nickunionArrLen < 0)
	{
		return FAILEDRETCODE;
	}

	if(NICKUNION_QRY_LEN < pkRGQueryNickUnionRst->nickunionArrLen)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkRGQueryNickUnionRst->nickunionArrLen; ++i)
	{
		if(DeCode__NameUnionInfo(&(pkRGQueryNickUnionRst->nickunionArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

