﻿/**
* @file PacketBuild.cpp
* @brief 组包模块cpp文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: PacketBuild.cpp
* 摘    要: 根据通信协议（类型，命令字，协议结构）进行组包
* 作    者: dzj
* 完成日期: 2007.11.30
*
*/

//#include "stdafx.h"

#include "PacketBuild.h"
CliProtocol CPacketBuild::m_clipro;
RelationServiceProtocol CPacketBuild::m_relationpro;	//关系服务器的解析
LoginServiceProtocol    CPacketBuild::m_loginpro;	//新登录服务器的解析
ShopSrvProtocol CPacketBuild::m_shopPro; //商城服务器的解析
LOG_SVC_NS::LogSvcProtocol CPacketBuild::m_logPro;//日至服务器的解析
