﻿/**
* @file MsgToPut.cpp
* @brief 在网络与应用之间传递的消息结构定义
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MsgToPut.cpp
* 摘    要: 在网络与应用之间传递的消息结构定义
* 作    者: dzj
* 完成日期: 2007.11.30
*
*/

//#include "stdafx.h"
#include "MsgToPut.h"
