﻿/**
* @file OtherserverBase.cpp
* @brief 定义与本服务通信的服务器端其它服务基类
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: OtherserverBase.cpp
* 摘    要: 定义与本服务通信的服务器端其它服务基类
* 作    者: dzj
* 完成日期: 2007.12.10
*
*/

#include "OtherserverBase.h"

/**
请参照以下代码在自己的工程中添加下述两个函数的实现代码；
///置玩家的句柄信息，每次新建时必须调用;
void CSrvConn::SetHandleInfo( int nHandleID, int nSessionID )
{
	TRY_BEGIN;

	if ( ( nSessionID <=0 )
		|| ( nSessionID > SRV_SID_MAX )
		)
	{
		//错误，srv的nSessionID不应该大于SRV_SID_MAX;
		D_WARNING( "连接SRV:%d成功\n", nSessionID );
	}

	m_nHandleID = nHandleID;
	m_nSessionID = nSessionID;		
	if ( nSessionID <= SRV_SID_MAX )
	{
		//本srv主动连接的其它srv;
		if ( ( nSessionID>=MAPSRV_SID_BASE ) && ( nSessionID<MAPSRV_SID_MAX ) )
		{
			//mapsrv;
        	m_bySrvType = 0x05;//mapsrv;
			unsigned short wID = nSessionID - MAPSRV_SID_BASE;
			m_pSrvBase = NEW CMapSrv( wID, this );
			D_WARNING( "该SRV为mapsrv:%d\n\n", wID );
		} else if ( ( nSessionID>=DBSRV_SID_BASE ) && ( nSessionID<DBSRV_SID_MAX ) ) {
			//dbsrv;
        	m_bySrvType = 0x07;//dbsrv;
			unsigned short wID = nSessionID - DBSRV_SID_BASE;
			m_pSrvBase = NEW CDbSrv( wID, this );
			D_WARNING( "该SRV为dbsrv:%d\n\n", wID );

		} else if ( ( nSessionID>=CENTERSRV_SID_BASE ) && ( nSessionID<CENTERSRV_SID_MAX ) ) {
			//centersrv;
        	m_bySrvType = 0x07;//centersrv;
			unsigned short wID = nSessionID - CENTERSRV_SID_BASE;
			m_pSrvBase = NEW CCenterSrv( wID, this );
			D_WARNING( "该SRV为centersrv:%d\n\n", wID );
		} else if ( ( nSessionID>=LOGINSRV_SID_BASE ) && ( nSessionID<LOGINSRV_SID_MAX ) ) {
			//loginsrv;
        	m_bySrvType = 0x07;//loginsrv;
			unsigned short wID = nSessionID - LOGINSRV_SID_BASE;
			//m_pSrvBase = NEW CCenterSrv( wID, this );
			//D_WARNING( "该SRV为centersrv:%d\n\n", wID );

		} else {
			D_WARNING( "连上不可识别SRV，其sessionID:%d\n\n", nSessionID );
		}
	}
	return;
	TRY_END;
	return;
}

///销毁时的处理(连接断开)
///注意：在新建连接之前也会调用这里；
void CSrvConn::OnDestoryed()
{
	TRY_BEGIN;

	if ( m_nHandleID != 0 )
	{
		//确实为断开，区别于新建连接时的回调；
		//以下断开处理；
		if ( NULL != m_pSrvBase )
		{
			m_pSrvBase->OnDestory();
			delete m_pSrvBase; m_pSrvBase = NULL;
		}

		if ( m_nSessionID <= SRV_SID_MAX )
		{
			//本srv主动连接的其它srv;
			if ( ( m_nSessionID>=MAPSRV_SID_BASE ) && ( m_nSessionID<MAPSRV_SID_MAX ) )
			{
				unsigned short wID = m_nSessionID - MAPSRV_SID_BASE;
				g_vecKickMapSrvPlayer.push_back( wID );
				D_WARNING( "到mapserver:%d连接断开，删去所有在线玩家\n", wID );
			} else if ( ( m_nSessionID>=DBSRV_SID_BASE ) && ( m_nSessionID<DBSRV_SID_MAX ) ) {
				unsigned short wID = m_nSessionID - DBSRV_SID_BASE;
				D_WARNING( "到dbserver:%d连接断开\n", wID );
			} else if ( ( m_nSessionID>=CENTERSRV_SID_BASE ) && ( m_nSessionID<CENTERSRV_SID_MAX ) ) {
				unsigned short wID = m_nSessionID - CENTERSRV_SID_BASE;
				D_WARNING( "到centerserver:%d连接断开\n", wID );
			} else if ( ( m_nSessionID>=LOGINSRV_SID_BASE ) && ( m_nSessionID<LOGINSRV_SID_MAX ) ) {
				unsigned short wID = m_nSessionID - LOGINSRV_SID_BASE;
				D_WARNING( "到loginserver:%d连接断开\n", wID );
			} else {
				D_WARNING( "不可识别SRV连接断开，其sessionID:%d\n\n", m_nSessionID );
			}
		}
	}
	ResetInfo();
	return;
	TRY_END;
	return;
}
*/

///收包处理；
void CSrvConn::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	TRY_BEGIN;
	//以后要处理第一个连接服务标识号，目前因为只连mapsrv，因此直接转发；
	if ( NULL != m_pSrvBase )
	{
		m_pSrvBase->OnPkgRcved( wCmd, pBuf, wPkgLen );
	}
	return; 
	TRY_END;
	return;
};

///断开处理（销毁 ）
void CSrvBase::OnDestory()
{
	m_pConn = NULL;
}

