﻿/**
* @file DealPkgBase.h
* @brief 处理包基类头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkgBase.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#pragma once
#include <map>
#include "../../Base/Utility.h"
 
#include "../../Base/dscontainer/dshashtb.h"

using namespace std;

#ifdef USE_DSIOCP
    #define CreateSrvPkg( PKGTYPE, pSrv, pkgContent ) CPacketBuild::CreateServerPkg<PKGTYPE>( pSrv->GetHandleID(), pSrv->GetSessionID(), pSrv->GetUniqueDsSocket(), &pkgContent );
    #define CreateSrvMGTPkg( pSrv, pkgContent) CPacketBuild::CreateMGTPkg( pSrv->GetHandleID(), pSrv->GetSessionID(), pSrv->GetUniqueDsSocket(), &pkgContent );
    #define CreateGateSrvPkg( PKGTYPE, pGateSrv, pkgContent, isNewMsg ) CPacketBuild::CreateGatePkg<PKGTYPE>( pGateSrv->GetHandleID(), pGateSrv->GetSessionID(), pGateSrv->GetUniqueDsSocket(), &pkgContent, pGateSrv->GetOldToSend(), isNewMsg );
#else //USE_DSIOCP
    #define CreateSrvPkg( PKGTYPE, pSrv, pkgContent ) CPacketBuild::CreateServerPkg<PKGTYPE>( pSrv->GetHandleID(), pSrv->GetSessionID(), &pkgContent );
    #define CreateSrvMGTPkg( pSrv, pkgContent) CPacketBuild::CreateMGTPkg( pSrv->GetHandleID(), pSrv->GetSessionID(), &pkgContent );
    #define CreateGateSrvPkg( PKGTYPE, pGateSrv, pkgContent, isNewMsg ) CPacketBuild::CreateGatePkg<PKGTYPE>( pGateSrv->GetHandleID(), pGateSrv->GetSessionID(), &pkgContent, pGateSrv->GetOldToSend(), isNewMsg );
#endif //USE_DSIOCP

template< typename T_Owner >
///对方发来没有注册过的命令字，若为玩家，则断开之；
void OnDealPkgNoRegCmd( T_Owner* pOwner, unsigned short wCmd )
{
	ACE_UNUSED_ARG( pOwner );
	ACE_UNUSED_ARG( wCmd );
}

template< typename T_Owner >
///各srv处理包的基类;
class IDealPkg
{
public:
	IDealPkg() {};
	virtual ~IDealPkg() {};

	///命令处理函数定义;
    typedef bool (*PFN_PKG)( T_Owner* pOwner, const char* pPkg, const unsigned short wPkgLen );
	typedef bool (FN_PKG)( T_Owner* pOwner, const char* pPkg, const unsigned short wPkgLen );

public:
	///注册消息处理函数；
	static void Register( unsigned short wCmd, PFN_PKG pFn )
	{
#ifdef USE_SELF_HASH
        m_hashDeals.PushEle( HashTbPtrEle<FN_PKG>( (unsigned int)wCmd, pFn ) );
#else //USE_SELF_HASH
		m_mapDeals[wCmd] = pFn;
#endif //USE_SELF_HASH
	}

	///寻找对应命令的处理函数;
	static PFN_PKG FindPFN( unsigned short wCmd )
	{
		TRY_BEGIN;

#ifdef USE_SELF_HASH
		HashTbPtrEle<FN_PKG>* pFound = NULL;
		if ( m_hashDeals.FindEle( (unsigned int)wCmd, pFound ) )
		{
			if ( NULL != pFound )
			{
				return pFound->GetInnerPtr();
			}
		}
		return NULL;
#else //USE_SELF_HASH
		typename map<unsigned short, PFN_PKG>::iterator tmpiter = m_mapDeals.find( wCmd );
		if ( tmpiter != m_mapDeals.end() )
		{
			return tmpiter->second;
		} else {
			return NULL;
		}
#endif //USE_SELF_HASH
		TRY_END;
		return NULL;
	}

public:
	///处理命令消息(找到命令对应的处理函数并调用)
	static bool DealPkg( T_Owner* pOwner, unsigned short wCmd, const char* pPkg, const unsigned short wPkgLen )
	{ 
		TRY_BEGIN;

		if ( NULL == pPkg )
		{
			D_ERROR( "DealPkg, wCmd%x, pPkg 空\n", wCmd );
			return false;
		}

		if ( NULL == pOwner )
		{
			D_ERROR( "DealPkg, wCmd%x, pOwner 空\n", wCmd );
			return false;
		}

		PFN_PKG pFn = FindPFN( wCmd );
		if ( NULL != pFn )
		{
			return pFn( pOwner, pPkg, wPkgLen );
		} else {
		//todo wcj 2010.12.29 暂时屏蔽	D_WARNING( "DealPkg, %x命令字没有注册\n", wCmd );
			OnDealPkgNoRegCmd( pOwner, wCmd );
			return false; 
		}	

		TRY_END;
		return false;
	};

protected:

#ifdef USE_SELF_HASH
	///命令==处理函数映射;
    static DsHashTable< HashTbPtrEle<FN_PKG>, 512, 2 > m_hashDeals;
#else //USE_SELF_HASH
	///命令==处理函数映射;
    static map<unsigned short, FN_PKG> m_mapDeals;
#endif //USE_SELF_HASH

};

#ifdef USE_SELF_HASH
	///命令==处理函数映射;
	template< typename T_Owner >
    DsHashTable< HashTbPtrEle< typename IDealPkg<T_Owner>::FN_PKG >, 512, 2 > IDealPkg<T_Owner>::m_hashDeals;
#else //USE_SELF_HASH
	///命令==处理函数映射;
	template< typename T_Owner >
	map<unsigned short, typename IDealPkg<T_Owner>::PFN_PKG > IDealPkg<T_Owner>::m_mapDeals;
#endif //USE_SELF_HASH

