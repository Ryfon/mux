﻿#ifndef			__RelationServiceProtocol__
#define			__RelationServiceProtocol__
#include "ParserTool.h"
#include "map"

namespace RelationServiceNS
{
	//Enums
	typedef	enum
	{
		MAX_TEAM_IND_LEN                      	=	12,              	//
		MAX_TEAM_NAME_LEN                     	=	20,              	//
		MAX_MEMBER_IND_LEN                    	=	16,              	//
		MAX_TEAM_MEMBERS_COUNT                	=	6,               	//
		MAX_TEAM_ATTRI_KEY_LEN                	=	1,               	//
		MAX_TEAM_ATTRI_VALUE_LEN              	=	1,               	//
		MAX_TEAM_BROADCAST_DATA_LEN           	=	256,             	//
		MAX_ATTRI_COUNT                       	=	1,               	//
		MAX_CHAT_GROUP_PASSWORD_LEN           	=	6,               	//
		MAX_CHAT_GROUP_MEMBERS_COUNT          	=	20,              	//
		MAX_CHAT_GROUP_NAME_LEN               	=	20,              	//
		MAX_CHAT_DATA_LEN                     	=	256,             	//
		MAX_UNION_NAME_LEN                    	=	20,              	//
		MAX_UNION_TITLE_LEN                   	=	20,              	//
		MAX_PLAYER_NAME_LEN                   	=	20,              	//
		MAX_UNION_MEMBER_LIST_SIZE            	=	4,               	//
		UNION_RIGHT_LEAVE                     	=	0x00000001,      	//
		UNION_RIGHT_VIEW_MEMBERS              	=	0x00000002,      	//
		UNION_RIGHT_VIEW_UNION_LOG            	=	0x00000004,      	//
		UNION_RIGHT_CLAIM_TASK                	=	0x00000008,      	//
		UNION_RIGHT_OPEN_STORAGE              	=	0x00000010,      	//
		UNION_RIGHT_VIEW_STORAGE_LOG          	=	0x00000020,      	//
		UNION_RIGHT_USE_UNION_CHANNEL         	=	0x00000040,      	//
		UNION_RIGHT_USER_UNION_SKILL          	=	0x00000080,      	//
		UNION_RIGHT_ADD_MEMBER                	=	0x00000100,      	//
		UNION_RIGHT_REMOVE_MEMBER             	=	0x00000200,      	//
		UNION_RIGHT_EDIT_TITLE                	=	0x00000400,      	//
		UNION_RIGHT_ADVANCE_POS               	=	0x00000800,      	//
		UNION_RIGHT_REDUCE_POS                	=	0x00001000,      	//
		UNION_RIGHT_POST_ANNOUNCE             	=	0x00002000,      	//
		UNION_RIGHT_ADVANCE_UNION             	=	0x00004000,      	//
		UNION_RIGHT_POST_TASK                 	=	0x00008000,      	//
		UNION_RIGHT_STUDY_SKILL               	=	0x00010000,      	//
		UNION_RIGHT_REMOVE_SPEEK_RESTRICT     	=	0x00020000,      	//
		UNION_RIGHT_ADD_SPEEK_RESTRICT        	=	0x00040000,      	//
		UNION_RIGHT_TRANSFER_CHAIR            	=	0x00080000,      	//
		UNION_RIGHT_MANAGER_UNION             	=	0x00100000,      	//
		UNION_RIGHT_DESIGN_UNION_BADGE        	=	0x00200000,      	//
		UNION_RIGHT_EXTEND_STORAGE            	=	0x00400000,      	//
		UNION_RIGHT_MANAGER_STORAGE           	=	0x00800000,      	//
		UNION_RIGHT_GET_PAY                   	=	0x01000000,      	//
		UNION_LOG_CREATE_EVENT                	=	0,               	//
		UNION_LOG_CAPTION_CHANGED_EVENT       	=	1,               	//
		UNION_LOG_ADVANCE_LEVEL_EVENT         	=	2,               	//
		UNION_LOG_REDUCE_LEVEL_EVENT          	=	3,               	//
		UNION_LOG_CRAFT_EVENT                 	=	4,               	//
		UNION_LOG_BADGE_EVENT                 	=	5,               	//
		UNION_LOG_KILL_NPC_EVENT              	=	6,               	//
		UNION_LOG_FIRST_PLACE_EVENT           	=	7,               	//
		TWEET_TYPE_UPLINE                     	=	0,               	//好友上线
		TWEET_TYPE_OFFLINE                    	=	1,               	//好友下线
		TWEET_TYPE_KILL_BOSS                  	=	2,               	//好友击杀BOSS
		TWEET_TYPE_GET_TITLE                  	=	3,               	//好友获得称号
		TWEET_TYPE_GET_ACH                    	=	4,               	//好友获得成就
		TWEET_TYPE_UPGRADE                    	=	5,               	//好友升级
		TWEET_TYPE_DEAD                       	=	6,               	//好友死亡
		TWEET_TYPE_KILL_PLAYER                	=	7,               	//好友击杀玩家
		TWEET_TYPE_ADD_FRIEND                 	=	8,               	//好友加新的好友
		TWEET_TYPE_JOIN_GUILD                 	=	9,               	//好友加入战盟
		TWEET_TYPE_LEAVE_GUILD                	=	10,              	//好友离开战盟
		TWEET_TYPE_SHOPPING                   	=	11,              	//好友购买商城物品
		TWEET_TYPE_EQUIPMENT_UPGRADE          	=	12,              	//好友装备升级
		TWEET_TYPE_EQUIPMENT_MOSAIC           	=	13,              	//好友装备镶嵌
		TWEET_TYPE_EQUIPMENT_TRANSFORM        	=	14,              	//好友装备改造
	}PROTOCOL_CONSTANT;

	typedef	enum
	{
		G_R_CreateTeam                        	=	0x1601,          	//
		G_R_AddTeamMember                     	=	0x1602,          	//
		G_R_AddTeamMembers                    	=	0x1603,          	//
		G_R_RemoveTeamMember                  	=	0x1604,          	//
		G_R_RemoveTeamMembers                 	=	0x1605,          	//
		G_R_DestroyTeam                       	=	0x1606,          	//
		G_R_QueryTeam                         	=	0x1607,          	//
		G_R_AddTeamAttri                      	=	0x1608,          	//
		G_R_RemoveTeamAttri                   	=	0x1609,          	//
		G_R_AddTeamMemberAttri                	=	0x160a,          	//
		G_R_RemoveTeamMemberAttri             	=	0x160b,          	//
		G_R_ReportGateId                      	=	0x160c,          	//
		G_R_QueryTeamMember                   	=	0x160d,          	//
		G_R_TeamBroadcast                     	=	0x160e,          	//
		G_R_ModifyTeamCaptain                 	=	0x160f,          	//
		G_R_ModifyExpSharedMode               	=	0x1610,          	//
		G_R_ModifyGoodSharedMode              	=	0x1611,          	//
		G_R_ModifyTeamMemberMapId             	=	0x1612,          	//
		G_R_ModifyTeamRollItemLevel           	=	0x1650,          	//
		G_R_QueryTeamMembersCount             	=	0x1651,          	//
		G_R_CreateChatGroup                   	=	0x1660,          	//
		G_R_DestroyChatGroup                  	=	0x1661,          	//
		G_R_AddChatGroupMember                	=	0x1662,          	//
		G_R_RemoveChatGroupMember             	=	0x1663,          	//
		G_R_QueryChatGroup                    	=	0x1664,          	//
		G_R_ChatGroupSpeek                    	=	0x1665,          	//
		G_R_UPLINE_MSG                        	=	0x1613,          	//notify Relation server when user up line
		G_R_UPDATA_MSG                        	=	0x1614,          	//
		G_R_ADDFRIEND_MSG                     	=	0x1615,          	//
		G_R_DELFRIEND_MSG                     	=	0x1616,          	//
		G_R_ADD_GROUP                         	=	0x1617,          	//
		G_R_FRIEND_ALTER_GROUP_ID             	=	0x1618,          	//
		G_R_DEL_GROUP                         	=	0x1619,          	//
		G_R_ALTERGROUPNAME                    	=	0x161a,          	//
		G_R_ADDTOBLACKLIST                    	=	0x161b,          	//
		G_R_DELFROMBLACKLIST                  	=	0x161c,          	//
		G_R_SENDGROUPMSG                      	=	0x161d,          	//
		G_R_SENDMSG                           	=	0x161e,          	//
		G_R_ADD_ENEMY                         	=	0x161f,          	//
		G_R_LOCK_ENEMY                        	=	0x1620,          	//
		G_R_DEL_ENEMY                         	=	0x1621,          	//
		G_R_DEL_PLAYER                        	=	0x1622,          	//
		R_G_CreateTeamResult                  	=	0x6101,          	//
		R_G_AddTeamMemberResult               	=	0x6102,          	//
		R_G_AddTeamMembersResult              	=	0x6103,          	//
		R_G_RemoveTeamMemberResult            	=	0x6104,          	//
		R_G_RemoveTeamMembersResult           	=	0x6105,          	//
		R_G_DestroyTeamResult                 	=	0x6106,          	//
		R_G_QueryTeamResult                   	=	0x6107,          	//
		R_G_AddTeamAttriResult                	=	0x6108,          	//
		R_G_RemoveTeamAttriResult             	=	0x6109,          	//
		R_G_AddTeamMemberAttriResult          	=	0x610a,          	//
		R_G_RemoveTeamMemberAttriResult       	=	0x610b,          	//
		R_G_ReportGateIdResult                	=	0x610c,          	//
		R_G_QueryTeamMemberResult             	=	0x610d,          	//
		R_G_TeamBroadcastResult               	=	0x610e,          	//
		R_G_TeamBroadcastNotify               	=	0x610f,          	//
		R_G_TeamDestroyedNotify               	=	0x6110,          	//
		R_G_AddTeamMemberNotify               	=	0x6111,          	//
		R_G_RemoveTeamMemberNotify            	=	0x6112,          	//
		R_G_UpdateTeamMemberNotify            	=	0x6113,          	//
		R_G_TeamCreatedNotify                 	=	0x6114,          	//
		R_G_ModifyTeamCaptainResult           	=	0x6115,          	//
		R_G_TeamCaptainModifiedNotify         	=	0x6116,          	//
		R_G_ModifyExpSharedModeNotify         	=	0x6117,          	//
		R_G_ModifyGoodSharedModeNotify        	=	0x6118,          	//
		R_G_ModifyExpSharedModeResult         	=	0x6119,          	//
		R_G_ModifyGoodSharedModeResult        	=	0x611a,          	//
		R_G_ModifyTeamMemberMapIdResult       	=	0x611b,          	//
		R_G_ModifyTeamMemberMapIdNotify       	=	0x611c,          	//
		R_G_ModifyTeamRollItemLevelResult     	=	0x6150,          	//
		R_G_ModifyTeamRollItemLevelNotify     	=	0x6151,          	//
		R_G_QueryTeamMembersCountResult       	=	0x6152,          	//
		R_G_CreateChatGroupResult             	=	0x6160,          	//
		R_G_ChatGroupCreatedNotify            	=	0x6161,          	//
		R_G_DestroyChatGroupResult            	=	0x6162,          	//
		R_G_ChatGroupDestroyedNotify          	=	0x6163,          	//
		R_G_AddChatGroupMemberResult          	=	0x6164,          	//
		R_G_ChatGroupMemberAddedNotify        	=	0x6165,          	//
		R_G_RemoveChatGroupMemberResult       	=	0x6166,          	//
		R_G_ChatGroupMemberRemovedNotify      	=	0x6167,          	//
		R_G_ChatGroupSpeekResult              	=	0x6168,          	//
		R_G_ChatGroupSpeekNotify              	=	0x6169,          	//
		R_G_GROUP_MSG                         	=	0x611d,          	//notify Relation server when user up line
		R_G_FRIENDS_MSG                       	=	0x611e,          	//notify Relation server when user up line
		R_G_UPDATAFRIEND_MSG                  	=	0x6120,          	//notify Relation server when user up line
		R_G_ADDFRIEND_MSG                     	=	0x6121,          	//notify Relation server when user up line
		R_G_DELFRIEND_MSG                     	=	0x6122,          	//notify Relation server when user up line
		R_G_ADD_GROUP                         	=	0x6123,          	//
		R_G_FRIEND_ALTER_GROUP_ID             	=	0x6124,          	//
		R_G_DEL_GROUP                         	=	0x6125,          	//
		R_G_ALTERGROUPNAME                    	=	0x6126,          	//
		R_G_ADDTOBLACKLIST                    	=	0x6127,          	//
		R_G_DELFROMBLACKLIST                  	=	0x6128,          	//
		R_G_SENDGROUPMSG                      	=	0x6129,          	//
		R_G_SENDMSG                           	=	0x612a,          	//
		R_G_BLACKLIST                         	=	0x612b,          	//
		R_G_ADD_ENEMY                         	=	0x612c,          	//
		R_G_LOCK_ENEMY                        	=	0x612d,          	//
		R_G_DEL_ENEMY                         	=	0x612e,          	//
		R_G_ENEMY_LIST                        	=	0x612f,          	//
		R_G_UPDATE_ENEMY                      	=	0x6130,          	//
		R_G_NOTIFY_R_GROUP_MSG_SUM            	=	0x6131,          	//
		G_R_CREATE_UNION_REQUEST              	=	0x16a0,          	//
		R_G_CREATE_UNION_RESULT               	=	0x61a0,          	//
		G_R_UNION_ADD_MEMBER_REQUEST          	=	0x16a1,          	//
		R_G_UNION_ADD_MEMBER_RESULT           	=	0x61a1,          	//
		G_R_UNION_REMOVE_MEMBER_REQUEST       	=	0x16a2,          	//
		R_G_UNION_REMOVE_MEMBER_RESULT        	=	0x61a2,          	//
		G_R_DESTROY_UNION_REQUEST             	=	0x16a3,          	//
		R_G_DESTROY_UNION_RESULT              	=	0x61a3,          	//
		G_R_REPORT_PLAYER_UPLINE              	=	0x16a4,          	//
		G_R_REPORT_PLAYER_OFFLINE             	=	0x16a5,          	//
		G_R_UNION_ADD_MEMBER_CONFIRMED        	=	0x16a6,          	//
		R_G_UNION_ADD_MEMBER_CONFIRM          	=	0x61a6,          	//
		G_R_QUERY_UNION_BASIC_REQUEST         	=	0x16a7,          	//
		R_G_QUERY_UNION_BASIC_RESULT          	=	0x61a7,          	//
		G_R_QUERY_UNION_MEMBER_LIST_REQUEST   	=	0x16a8,          	//
		R_G_QUERY_UNION_MEMBER_LIST_RESULT    	=	0x61a8,          	//
		R_G_UNION_DESTROYED_NOTIFY            	=	0x61a9,          	//
		R_G_UNION_MEMBER_ADDED_NOTIFY         	=	0x61aa,          	//
		R_G_UNION_MEMBER_REMOVED_NOTIFY       	=	0x61ab,          	//
		G_R_UNION_MODIFY_MEMBER_TITLE_REQUEST 	=	0x16ac,          	//
		R_G_UNION_MODIFY_MEMBER_TITLE_RESULT  	=	0x61ac,          	//
		R_G_UNION_MEMBER_TITLE_NOTIFY         	=	0x61ad,          	//
		G_R_UNION_TRANSFROM_CAPTION_REQUEST   	=	0x16ae,          	//
		R_G_UNION_TRANSFORM_CAPTION_RESULT    	=	0x61ae,          	//
		R_G_UNION_TRANSFORM_CAPTION_NOTIFY    	=	0x61af,          	//
		G_R_UNION_REMOVE_POS_REQUEST          	=	0x16b0,          	//
		R_G_UNION_REMOVE_POS_RESULT           	=	0x61b0,          	//
		G_R_UNION_ADD_POS_REQUEST             	=	0x16b1,          	//
		R_G_UNION_ADD_POS_RESULT              	=	0x61b1,          	//
		G_R_UNION_MODIFY_POS_RIGHTS_REQUEST   	=	0x16b2,          	//
		R_G_UNION_MODIFY_POS_RIGHTS_RESULT    	=	0x61b2,          	//
		R_G_UNION_POS_RIGHTS_NOTIFY           	=	0x61b3,          	//
		G_R_UNION_ADVANCE_MEMBER_POS_REQUEST  	=	0x16b4,          	//
		R_G_UNION_ADVANCE_MEMBER_POS_RESULT   	=	0x61b4,          	//
		R_G_UNION_ADVANCE_MEMBER_POS_NOTIFY   	=	0x61b5,          	//
		G_R_UNION_REDUCE_MEMBER_POS_REQUEST   	=	0x16b6,          	//
		R_G_UNION_REDUCE_MEMBER_POS_RESULT    	=	0x61b6,          	//
		R_G_UNION_REDUCE_MEMBER_POS_NOTIFY    	=	0x61b7,          	//
		G_R_UNION_CHANNEL_SPEEK_REQUEST       	=	0x16b8,          	//
		R_G_UNION_CHANNEL_SPEEK_RESULT        	=	0x61b8,          	//
		R_G_UNION_CHANNEL_SPEEK_NOTIFY        	=	0x61b9,          	//
		G_R_UNION_UPDATE_POS_CFG_REQUEST      	=	0x16ba,          	//
		R_G_UNION_UPDATE_POS_CFG_RESULT       	=	0x61ba,          	//
		R_G_UNION_UPDATE_POS_CFG_NOTIFY       	=	0x61bb,          	//
		G_R_UNION_FORBID_SPEEK_REQUEST        	=	0x16bc,          	//
		R_G_UNION_FORBID_SPEEK_RESULT         	=	0x61bc,          	//
		R_G_UNION_FORBID_SPEEK_NOTIFY         	=	0x61bd,          	//
		R_G_UNION_SKILLS_LIST_NOTIFY          	=	0x61be,          	//
		G_R_STUDY_UNION_SKILL_REQUEST         	=	0x16bf,          	//
		R_G_STUDY_UNION_SKILL_RESULT          	=	0x61bf,          	//
		G_R_MODIFY_UNION_ACTIVE_POINT_REQUEST 	=	0x16c0,          	//
		R_G_MODIFY_UNION_ACTIVE_POINT_RESULT  	=	0x61c0,          	//
		R_G_UNION_ACTIVE_POINT_NOTIFY         	=	0x61c1,          	//
		G_R_POST_UNION_TASK_LIST_REQUEST      	=	0x16c2,          	//
		R_G_POST_UNION_TASK_LIST_RESULT       	=	0x61c2,          	//
		R_G_UNION_TASK_LIST_NOTIFY            	=	0x61c3,          	//
		G_R_ADVANCE_UNION_LEVEL_REQUEST       	=	0x16c4,          	//
		R_G_ADVANCE_UNION_LEVEL_RESULT        	=	0x61c4,          	//
		R_G_UNION_LEVEL_NOTIFY                	=	0x61c5,          	//
		G_R_POST_UNION_BULLETIN_REQUEST       	=	0x16c6,          	//
		R_G_POST_UNION_BULLETIN_RESULT        	=	0x61c6,          	//
		R_G_UNION_BULLETIN_NOTIFY             	=	0x61c7,          	//
		R_G_UNION_MEMBER_UPLINE_NOTIFY        	=	0x61c8,          	//
		R_G_UNION_MEMBER_OFFLINE_NOTIFY       	=	0x61c9,          	//
		G_R_QUERY_UNION_LOG_REQUEST           	=	0x16ca,          	//
		R_G_QUERY_UNION_LOG_RESULT            	=	0x61ca,          	//
		R_G_UNION_EMAIL_NOTIFY                	=	0x61cb,          	//
		G_R_UNION_OWNER_CHECK_REQUEST         	=	0x16cc,          	//
		R_G_UNION_OWNER_CHECK_RESULT          	=	0x61cc,          	//
		G_R_UNION_MODIFY_PRESTIGE_REQUEST     	=	0x16cd,          	//
		R_G_UNION_MODIFY_PRESTIGE_RESULT      	=	0x61cd,          	//
		R_G_UNION_PRESTIGE_NOTIFY             	=	0x61ce,          	//
		G_R_UNION_MODIFY_BADGE_REQUEST        	=	0x16cf,          	//
		R_G_UNION_MODIFY_BADGE_RESULT         	=	0x61cf,          	//
		R_G_UNION_BADGE_NOTIFY                	=	0x61d0,          	//
		G_R_UNION_MODIFY_MULTI_REQUEST        	=	0x16d1,          	//
		R_G_UNION_MULTI_NOTIFY                	=	0x61d1,          	//
		G_R_QUERY_PLAYER_RELATION_REQUEST     	=	0x16d2,          	//
		R_G_QUERY_PLAYER_RELATION_RESULT      	=	0x61d2,          	//
		G_R_QUERY_PLAYER_INFO_REQUEST         	=	0x16d3,          	//
		R_G_QUERY_PLAYER_INFO_RESULT          	=	0x61d3,          	//
		G_R_QUERY_UNION_RANK_REQUEST          	=	0x16d4,          	//
		R_G_QUERY_UNION_RANK_RESULT           	=	0x61d4,          	//
		G_R_SEARCH_UNION_RANK_REQUEST         	=	0x16d5,          	//
		R_G_SEARCH_UNION_RANK_RESULT          	=	0x61d5,          	//
		R_G_PLAYER_UPLINE_TEAMID_NOTIFY       	=	0x16d6,          	//
		G_R_QUERY_RACEMASTER_REQUEST          	=	0x16d7,          	//
		R_G_QUERY_RACEMASTER_RESULT           	=	0x61d7,          	//
		R_G_TEAMLOG                           	=	0x61d8,          	//
		G_R_QUERY_GUILD_MEMBERS_REQUEST       	=	0x16d9,          	//
		R_G_QUERY_GUILD_MEMBERS_RESULT        	=	0x61d9,          	//
		G_R_ADD_FRIENDLY_DEGREE               	=	0x16e0,          	//
		G_R_ADD_TWEET                         	=	0x16e1,          	//
		R_G_SHOW_TWEET                        	=	0x16e2,          	//
		G_R_QUERY_TWEETS_REQUEST              	=	0x16e3,          	//
		R_G_QUERY_TWEETS_RESULT               	=	0x61e3,          	//
		G_R_QUERY_FRIENDS_LIST_REQUEST        	=	0x16e4,          	//
		R_G_QUERY_FRIENDS_LIST_RESULT         	=	0x61e4,          	//
		G_R_NOTIY_EXP_ADDED                   	=	0x16e5,          	//
		R_G_NOTIFY_ADD_EXP                    	=	0x61e5,          	//
		R_G_QUERY_EXP_NEEDED_LVL_UP_REQUEST   	=	0x61e6,          	//
		G_R_QUERY_EXP_NEEDED_LVL_UP_RESULT    	=	0x16e6,          	//
		R_G_NOTIFY_TEAM_GAIN                  	=	0x61e7,          	//
		G_R_MODIFY_TWEET_RECEIVE_FLAG         	=	0x16e8,          	//
		R_G_FRIEND_DEGREE_GAIN_EMAIL          	=	0x61e9,          	//
		G_R_QUERY_NICKUNION                   	=	0x16e9,          	//
		R_G_QUERY_NICKUNION_RST               	=	0x61ea,          	//
	}MessageId;

	typedef	enum
	{
		R_G_RESULT_SUCCEED                    	=	50001,           	//
		R_G_RESULT_FAILED                     	=	50002,           	//
		R_G_RESULT_NOOP                       	=	50003,           	//
		R_G_RESULT_NO_GROUP                   	=	50004,           	//
		R_G_RESULT_NO_MEMBER                  	=	50005,           	//
		R_G_RESULT_GROUP_FULL                 	=	50006,           	//
		R_G_RESULT_GROUP_EMPTY                	=	50007,           	//
		R_G_RESULT_GATE_ID_DIRTY              	=	50008,           	//
		R_G_RESULT_NO_ATTRI                   	=	50009,           	//
		R_G_RESULT_ATTRI_FULL                 	=	50010,           	//
		R_G_RESULT_ATTRI_UPDATE               	=	50011,           	//
		R_G_RESULT_REPEAT                     	=	50012,           	//
		R_G_RESULT_CAPTAIN_NOTIN_TEAM         	=	50013,           	//
		R_G_RESULT_MEMBER_IN_OTHER_TEAM       	=	50014,           	//
		R_G_RESULT_SEND_GROUP_MSG_TIMES       	=	50015,           	//
		R_G_RESULT_NOT_FIND_FRIEND            	=	50016,           	//
		R_G_RESULT_FRIEND_COUNT_SLOP_OVER     	=	50017,           	//
		R_G_RESULT_BLACKLST_EXIST             	=	50018,           	//
		R_G_RESULT_FOE_EXIST                  	=	50019,           	//
		R_G_RESULT_FRIENT_EXIST               	=	50020,           	//
		R_G_RESULT_GROUP_NO_EXIST             	=	50021,           	//
		R_G_RESULT_GROUP_COUNT_SLOP_OVER      	=	50021,           	//
		R_G_RESULT_SOURCE_GROUP_NOT_FIND      	=	50022,           	//
		R_G_RESULT_DESC_GROUP_NOT_FIND        	=	50023,           	//
		R_G_RESULT_BLACKLST_COUNT_SLOP_OVER   	=	50024,           	//
		R_G_RESULT_GROUP_NO_EMPTY             	=	50025,           	//
		R_G_RESULT_FOE_COUNT_SLOP_OVER        	=	50026,           	//
		R_G_RESULT_FOE_NOT_FIND               	=	50027,           	//
		R_G_RESULT_ADD_FRIEND_SELF            	=	50028,           	//
		R_G_RESULT_ADD_BLACKLST_SELF          	=	50029,           	//
		R_G_RESULT_FRIEND_NAME_NO_EXIST       	=	50030,           	//
		R_G_RESULT_IS_NOT_OWNER               	=	50031,           	//
		R_G_RESULT_UNION_SUCCEED              	=	60001,           	//
		R_G_RESULT_UNION_NAME_REPEATED        	=	60002,           	//
		R_G_RESULT_UNION_NO_MEMBER            	=	60003,           	//
		R_G_RESULT_UNION_IS_FULL              	=	60004,           	//
		R_G_RESULT_IN_OTHER_UNION             	=	60005,           	//
		R_G_RESULT_SERVICE_ERROR              	=	60006,           	//
		R_G_RESULT_NO_PLAYER                  	=	60007,           	//
		R_G_RESULT_ADD_UNION_MEMBER_SUCCEED   	=	60008,           	//
		R_G_RESULT_NEED_HIGHER_LEVEL          	=	60009,           	//
		R_G_RESULT_PLAYER_NO_UNION            	=	60010,           	//
		R_G_RESULT_RIGHT_CHECK_FAILED         	=	60011,           	//
		R_G_RESULT_UNION_ADD_REJECT           	=	60012,           	//
		R_G_RESULT_UNION_ADD_AGREE            	=	60013,           	//
		R_G_RESULT_UNION_NO_POS               	=	60014,           	//
		R_G_RESULT_UNION_FORBID_SPEEK         	=	60015,           	//
		R_G_RESULT_UNION_STUDY_SKILL_FAILED   	=	60016,           	//
		R_G_RESULT_UNION_TASK_LIST_POST_REPEAT	=	60017,           	//
		R_G_RESULT_UNION_ACTIVE_POINT_LACK    	=	60018,           	//
		R_G_RESULT_UNION_UNEXPECTED_SKILL_ID  	=	60019,           	//
		R_G_RESULT_UNION_PLAYER_IN_DIFF_RACE  	=	60020,           	//
		R_G_RESULT_UNION_ADD_INVITE_REPEAT    	=	60021,           	//
	}R_G_RESULT_CODE;

	typedef	enum
	{
		MAX_RNAME_SIZE                        	=	20,              	//
		FRIEND_IS_HAT_ME_MASK                 	=	0x01,            	//
		MAX_RMSSAGE_SIZE                      	=	128,             	//
		NICKUNION_QRY_LEN                     	=	10,              	//
	}DemarcateValue;


	//Defines

	//Typedefs

	//Types
	struct G_R_DelPlayer
	{
		static const USHORT	wCmd = G_R_DEL_PLAYER;

		UINT                  	uiid;                               	//
	};

	struct RPlayerID
	{
		USHORT                	GateSrvID;                          	//Gate Server's identification
		UINT                  	PPlayer;                            	//Player's Object pointer in the Gate Server
	};

	struct FriendGroupInfo
	{
		UINT                  	GroupID;                            	//Group's identification
		UINT                  	GroupNameLen;                       	//GroupName's trim size
		CHAR                  	GroupName[MAX_RNAME_SIZE];            	//
	};

	struct NameInfo
	{
		UINT                  	NameLen;                            	//名字长度
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
	};

	struct NameUnionInfo
	{
		NameInfo              	NickName;                           	//昵称名字
		NameInfo              	UnionName;                          	//战盟名字
	};

	struct FriendInfo
	{
		UINT                  	GroupID;                            	//Group's identification
		UINT                  	NickNameLen;                        	//NickName's trim size
		CHAR                  	NickName[MAX_RNAME_SIZE];             	//
		CHAR                  	Flag;                               	//friend's flag  0x01 is hat me , 0x02 is male 0x04 is female
		CHAR                  	EnterModel;                         	//登陆模式：0x0是退出，0x1在游戏中登入,0x2在IRC中登入
		UINT                  	uiID;                               	//friend's identification in the server's database
		USHORT                	MapID;                              	//map id
		USHORT                	playerLevel;                        	//player level
		CHAR                  	playerClass;                        	//player class
		USHORT                	playerRace;                         	//player Race
		USHORT                	playerIcon;                         	//player Icon
		UINT                  	GuildNameLen;                       	//GuildName's trim size
		CHAR                  	GuildName[MAX_RNAME_SIZE];            	//
		UINT                  	FereNameLen;                        	//FereName's trim size
		CHAR                  	FereName[MAX_RNAME_SIZE];             	//
		UINT                  	degree;                             	//友好度
		CHAR                  	tweetsFlag;                         	//是否有未读动态，0表示没有，非0表示有
	};

	struct GRUplineInfo
	{
		static const USHORT	wCmd = G_R_UPLINE_MSG;

		RPlayerID             	player;                             	//friend's identification in the server system
		CHAR                  	EnterModel;                         	//登陆模式：0x0是退出，0x1在游戏中登入,0x2在IRC中登入
		USHORT                	MapID;                              	//map id
		UINT                  	uiID;                               	//Player id
		CHAR                  	Sex;                                	//friend's Sex  0x01 is male 0x00 is female 
		USHORT                	playerLevel;                        	//player level
		USHORT                	playerRace;                         	//player Race
		CHAR                  	playerClass;                        	//player class
		USHORT                	playerIcon;                         	//player Icon
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
		UINT                  	GuildNameLen;                       	//GuildName's trim size
		CHAR                  	GuildName[MAX_RNAME_SIZE];            	//
		UINT                  	FereNameLen;                        	//FereName's trim size
		CHAR                  	FereName[MAX_RNAME_SIZE];             	//
	};

	struct RGFriendGroups
	{
		static const USHORT	wCmd = R_G_GROUP_MSG;

		RPlayerID             	player;                             	//friend's identification in the server system
		CHAR                  	Remanent;                           	//剩余的消息数量
		UINT                  	validSize;                          	// remainder of the FriendGroupInfo's size 
		UINT                  	friendGroupsLen;                    	//friendGroups's trim size
		FriendGroupInfo       	friendGroups[10];                     	//
	};

	struct RGFriendInfos
	{
		static const USHORT	wCmd = R_G_FRIENDS_MSG;

		RPlayerID             	player;                             	//friend's identification in the server system
		UINT                  	validSize;                          	// remainder of the friendinfos's size 
		UINT                  	friendinfosLen;                     	//friendinfos's trim size
		FriendInfo            	friendinfos[5];                       	//
	};

	struct GRInfoUpdate
	{
		static const USHORT	wCmd = G_R_UPDATA_MSG;

		UINT                  	uiID;                               	//Player id
		USHORT                	MapID;                              	//map id
		USHORT                	playerLevel;                        	//player level
		CHAR                  	playerClass;                        	//player class
		USHORT                	playerRace;                         	//player Race
		USHORT                	playerIcon;                         	//player Icon
		UINT                  	GuildNameLen;                       	//GuildName's trim size
		CHAR                  	GuildName[MAX_RNAME_SIZE];            	//
		UINT                  	FereNameLen;                        	//FereName's trim size
		CHAR                  	FereName[MAX_RNAME_SIZE];             	//
		CHAR                  	Online;                             	//登陆模式：0x0是退出，0x1在游戏中登入,0x2在IRC中登入
	};

	struct RGFriendInfoUpdate
	{
		static const USHORT	wCmd = R_G_UPDATAFRIEND_MSG;

		RPlayerID             	player;                             	//被通知者的 playerid
		UINT                  	uiID;                               	//通知者的 uiID
		USHORT                	MapID;                              	//map id
		USHORT                	playerLevel;                        	//player level
		CHAR                  	playerClass;                        	//player class
		USHORT                	playerRace;                         	//player Race
		USHORT                	playerIcon;                         	//player Icon
		UINT                  	GuildNameLen;                       	//GuildName's trim size
		CHAR                  	GuildName[MAX_RNAME_SIZE];            	//
		UINT                  	FereNameLen;                        	//FereName's trim size
		CHAR                  	FereName[MAX_RNAME_SIZE];             	//
		CHAR                  	Online;                             	//登陆模式：0x0是退出，0x1在游戏中登入,0x2在IRC中登入
		UINT                  	degree;                             	//与FriendInfo::degree同
		CHAR                  	tweetsFlag;                         	//与FriendInfo::tweetsFlag同
	};

	struct RGNotifyRGroupMsgNum
	{
		static const USHORT	wCmd = R_G_NOTIFY_R_GROUP_MSG_SUM;

		RPlayerID             	player;                             	//被通知者的 playerid
		CHAR                  	Remanent;                           	//剩余的消息数量
	};

	struct BlacklistItem
	{
		UINT                  	uiID;                               	//
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
	};

	struct RGBlacklist
	{
		static const USHORT	wCmd = R_G_BLACKLIST;

		RPlayerID             	playerID;                           	//
		UINT                  	validSize;                          	// remainder of the Blacklist's size 
		CHAR                  	BlacklistLen;                       	//Blacklist's trim size
		BlacklistItem         	Blacklist[10];                        	//
	};

	struct AddFriendError
	{
		UINT                  	ErrorCode;                          	//
		RPlayerID             	playerID;                           	//
	};

	struct AddFriendSuc
	{
		RPlayerID             	playerID;                           	//
		FriendInfo            	friendinfo;                         	//
	};

	union AddFriend
	{
		AddFriendError        	Error;                              	//
		AddFriendSuc          	Suc;                                	//
	};

	struct RGAddFriend
	{
		static const USHORT	wCmd = R_G_ADDFRIEND_MSG;

		UINT                  	ErrCode;                            	//
		AddFriend             	addFriend;                          	//
	};

	struct GRAddFriend
	{
		static const USHORT	wCmd = G_R_ADDFRIEND_MSG;

		UINT                  	uiID;                               	//
		UINT                  	ByAdduiID;                          	//
		UINT                  	groupId;                            	//
	};

	struct GRDelFriend
	{
		static const USHORT	wCmd = G_R_DELFRIEND_MSG;

		UINT                  	uiID;                               	//
		UINT                  	ByAdduiID;                          	//
	};

	struct DelFriendSuc
	{
		RPlayerID             	playerID;                           	//
		FriendInfo            	playerIDByDell;                     	//
	};

	union DelFriend
	{
		AddFriendError        	Error;                              	//
		DelFriendSuc          	Suc;                                	//
	};

	struct RGDelFriend
	{
		static const USHORT	wCmd = R_G_DELFRIEND_MSG;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		DelFriend             	delfriend;                          	//
	};

	struct GRAddGroup
	{
		static const USHORT	wCmd = G_R_ADD_GROUP;

		UINT                  	uiID;                               	//
		BYTE                  	GroupNameLen;                       	//
		CHAR                  	GroupName[MAX_RNAME_SIZE];            	//
	};

	struct AddGroupErr
	{
	};

	struct AddGroupSuc
	{
		BYTE                  	GroupID;                            	//
		BYTE                  	GroupNameLen;                       	//
		CHAR                  	GroupName[MAX_RNAME_SIZE];            	//
	};

	union AddGroupResult
	{
		AddGroupErr           	Error;                              	//
		AddGroupSuc           	Suc;                                	//
	};

	struct RGAddGroup
	{
		static const USHORT	wCmd = R_G_ADD_GROUP;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		AddGroupResult        	result;                             	//
	};

	struct GRFriendAlterGroupID
	{
		static const USHORT	wCmd = G_R_FRIEND_ALTER_GROUP_ID;

		UINT                  	uiID;                               	//
		UINT                  	FriendID;                           	//
		UINT                  	preGroupID;                         	//
		UINT                  	newGroupID;                         	//
	};

	struct AlterGroupIDErr
	{
		UINT                  	err;                                	//
	};

	struct AlterGroupIDSuc
	{
		UINT                  	FriendID;                           	//
		UINT                  	preGroupID;                         	//
		UINT                  	newGroupID;                         	//
	};

	union AlterGroupResult
	{
		AlterGroupIDErr       	Error;                              	//
		AlterGroupIDSuc       	Suc;                                	//
	};

	struct RGFriendAlterGroupID
	{
		static const USHORT	wCmd = R_G_FRIEND_ALTER_GROUP_ID;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		AlterGroupResult      	result;                             	//
	};

	struct GRDelGroup
	{
		static const USHORT	wCmd = G_R_DEL_GROUP;

		UINT                  	uiID;                               	//
		UINT                  	GroupID;                            	//
	};

	struct DelGroupErr
	{
		UINT                  	err;                                	//
	};

	struct DelGroupSuc
	{
		UINT                  	GroupID;                            	//
	};

	union DelGroupResult
	{
		DelGroupErr           	Error;                              	//
		DelGroupSuc           	Suc;                                	//
	};

	struct RGDelGroup
	{
		static const USHORT	wCmd = R_G_DEL_GROUP;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		DelGroupResult        	result;                             	//
	};

	struct GRAlterGroupName
	{
		static const USHORT	wCmd = G_R_ALTERGROUPNAME;

		UINT                  	uiID;                               	//
		UINT                  	GroupID;                            	//
		BYTE                  	newGroupNameLen;                    	//
		CHAR                  	newGroupName[MAX_RNAME_SIZE];         	//
	};

	struct AlterGroupNameErr
	{
	};

	struct AlterGroupNameSuc
	{
		UINT                  	GroupID;                            	//
		BYTE                  	newGroupNameLen;                    	//
		CHAR                  	newGroupName[MAX_RNAME_SIZE];         	//
	};

	union AlterGroupNameResult
	{
		AlterGroupNameErr     	Error;                              	//
		AlterGroupNameSuc     	Suc;                                	//
	};

	struct RGAlterGroupName
	{
		static const USHORT	wCmd = R_G_ALTERGROUPNAME;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		AlterGroupNameResult  	result;                             	//
	};

	struct GRAddToBlacklist
	{
		static const USHORT	wCmd = G_R_ADDTOBLACKLIST;

		UINT                  	uiID;                               	//
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
	};

	struct AddToBlacklistErr
	{
		UINT                  	err;                                	//
	};

	struct AddToBlacklistSuc
	{
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
	};

	union AddToBlacklistResult
	{
		AddToBlacklistErr     	Error;                              	//
		AddToBlacklistSuc     	Suc;                                	//
	};

	struct RGAddToBlacklist
	{
		static const USHORT	wCmd = R_G_ADDTOBLACKLIST;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		AddToBlacklistResult  	result;                             	//
	};

	struct GRDelFromBlacklist
	{
		static const USHORT	wCmd = G_R_DELFROMBLACKLIST;

		UINT                  	uiID;                               	//
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
	};

	struct DelFromBlacklistErr
	{
	};

	struct DelFromBlacklistSuc
	{
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
	};

	union DelFromBlacklistResult
	{
		DelFromBlacklistErr   	Error;                              	//
		DelFromBlacklistSuc   	Suc;                                	//
	};

	struct RGDelFromBlackList
	{
		static const USHORT	wCmd = R_G_DELFROMBLACKLIST;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		DelFromBlacklistResult	result;                             	//
	};

	struct GRAddEnemy
	{
		static const USHORT	wCmd = G_R_ADD_ENEMY;

		RPlayerID             	player;                             	//friend's identification in the server system
		USHORT                	MapID;                              	//map id
		UINT                  	uiID;                               	//杀手ID
		CHAR                  	Sex;                                	//friend's Sex  0x01 is male 0x00 is female 
		USHORT                	playerLevel;                        	//player level
		CHAR                  	playerClass;                        	//player class
		USHORT                	playerRace;                         	//player Race
		USHORT                	playerIcon;                         	//player Icon
		UINT                  	GuildNameLen;                       	//GuildName's trim size
		CHAR                  	GuildName[MAX_RNAME_SIZE];            	//
		UINT                  	FereNameLen;                        	//FereName's trim size
		CHAR                  	FereName[MAX_RNAME_SIZE];             	//
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
		UINT                  	addtouiID;                          	//被杀的人的ID
		USHORT                	posX;                               	//
		USHORT                	posY;                               	//
	};

	struct AddEnemyErr
	{
		UINT                  	err;                                	//
	};

	struct AddEnemySuc
	{
		RPlayerID             	player;                             	//friend's identification in the server system
		USHORT                	MapID;                              	//map id
		UINT                  	uiID;                               	//Player id
		CHAR                  	Sex;                                	//friend's Sex  0x01 is male 0x00 is female 
		USHORT                	playerLevel;                        	//player level
		CHAR                  	playerClass;                        	//player class
		USHORT                	playerRace;                         	//player Race
		USHORT                	playerIcon;                         	//player Icon
		UINT                  	GuildNameLen;                       	//GuildName's trim size
		CHAR                  	GuildName[MAX_RNAME_SIZE];            	//
		UINT                  	FereNameLen;                        	//FereName's trim size
		CHAR                  	FereName[MAX_RNAME_SIZE];             	//
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
		UINT                  	Replace_uiID;                       	//Player id
	};

	union AddEnemyResult
	{
		AddEnemyErr           	Error;                              	//
		AddEnemySuc           	Suc;                                	//
	};

	struct RGAddEnemy
	{
		static const USHORT	wCmd = R_G_ADD_ENEMY;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		AddEnemyResult        	result;                             	//
	};

	struct GRLockEnemy
	{
		static const USHORT	wCmd = G_R_LOCK_ENEMY;

		UINT                  	uiID;                               	//Player id
		CHAR                  	IsLock;                             	//0x01 锁定  0x00 解锁
		UINT                  	LockuiID;                           	//Player id
	};

	struct LockEnemyErr
	{
	};

	struct LockEnemySuc
	{
		UINT                  	uiID;                               	//被锁定的仇人
	};

	union LockEnemyResult
	{
		LockEnemyErr          	Error;                              	//
		LockEnemySuc          	Suc;                                	//
	};

	struct RGLockEnemy
	{
		static const USHORT	wCmd = R_G_LOCK_ENEMY;

		RPlayerID             	playerID;                           	//
		CHAR                  	IsLock;                             	//0x01 锁定  0x00 解锁
		UINT                  	ErrCode;                            	//
		LockEnemyResult       	result;                             	//
	};

	struct GRDelEnemy
	{
		static const USHORT	wCmd = G_R_DEL_ENEMY;

		UINT                  	uiID;                               	//Player id
		UINT                  	deluiID;                            	//Player id
	};

	struct DelEnemyErr
	{
	};

	struct DelEnemySuc
	{
		UINT                  	uiID;                               	//被锁定的仇人
	};

	union DelEnemyResult
	{
		DelEnemyErr           	Error;                              	//
		DelEnemySuc           	Suc;                                	//
	};

	struct RGDelEnemy
	{
		static const USHORT	wCmd = R_G_DEL_ENEMY;

		RPlayerID             	playerID;                           	//
		UINT                  	ErrCode;                            	//
		DelEnemyResult        	result;                             	//
	};

	struct EnemylistItem
	{
		RPlayerID             	player;                             	//friend's identification in the server system
		USHORT                	MapID;                              	//map id
		UINT                  	uiID;                               	//Player id
		CHAR                  	Sex;                                	//friend's Sex  0x01 is male 0x00 is female 
		CHAR                  	Online;                             	//player level
		USHORT                	playerLevel;                        	//player level
		CHAR                  	playerClass;                        	//player class
		USHORT                	playerRace;                         	//player Race
		USHORT                	playerIcon;                         	//player Icon
		UINT                  	GuildNameLen;                       	//GuildName's trim size
		CHAR                  	GuildName[MAX_RNAME_SIZE];            	//
		UINT                  	FereNameLen;                        	//FereName's trim size
		CHAR                  	FereName[MAX_RNAME_SIZE];             	//
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
		CHAR                  	IsLock;                             	//仇人是否被锁定！
		USHORT                	incidentMap;                        	//
		USHORT                	incidentPosX;                       	//
		USHORT                	incidentPosY;                       	//
		CHAR                  	bKilled;                            	//
		UINT                  	incidentTime;                       	//
	};

	struct RGEnemylist
	{
		static const USHORT	wCmd = R_G_ENEMY_LIST;

		RPlayerID             	playerID;                           	//
		UINT                  	validSize;                          	// remainder of the Blacklist's size 
		CHAR                  	EnemylistLen;                       	//Blacklist's trim size
		EnemylistItem         	Enemylist[4];                         	//
	};

	struct RGUpdateEnemy
	{
		static const USHORT	wCmd = R_G_UPDATE_ENEMY;

		RPlayerID             	playerID;                           	//
		EnemylistItem         	Enemy;                              	//
	};

	struct GRSendGroupMsg
	{
		static const USHORT	wCmd = G_R_SENDGROUPMSG;

		UINT                  	uiID;                               	//
		UINT                  	GroupID;                            	//
		CHAR                  	MsgLen;                             	//
		CHAR                  	Msg[MAX_RMSSAGE_SIZE];                	//
	};

	struct SendGroupMsgErr
	{
		UINT                  	ErrCode;                            	//
	};

	struct GeneralMsg
	{
		UINT                  	uiID;                               	//
		UINT                  	NameLen;                            	//Name's trim size
		CHAR                  	Name[MAX_RNAME_SIZE];                 	//
		CHAR                  	MsgLen;                             	//
		CHAR                  	Msg[MAX_RMSSAGE_SIZE];                	//
	};

	union SendGroupMsgResult
	{
		SendGroupMsgErr       	Error;                              	//
		GeneralMsg            	Suc;                                	//
	};

	struct RGSendGroupMsg
	{
		static const USHORT	wCmd = R_G_SENDGROUPMSG;

		UINT                  	ErrCode;                            	//
		RPlayerID             	playerID;                           	//
		CHAR                  	Remanent;                           	//剩余的消息数量
		SendGroupMsgResult    	result;                             	//
	};

	struct GRSendMsg
	{
		static const USHORT	wCmd = G_R_SENDMSG;

		UINT                  	uiID;                               	//
		UINT                  	rcvuiID;                            	//
		CHAR                  	MsgLen;                             	//
		CHAR                  	Msg[MAX_RMSSAGE_SIZE];                	//
	};

	struct SendMsgErr
	{
	};

	struct SendMsgResult
	{
		SendGroupMsgErr       	Error;                              	//
		GeneralMsg            	Suc;                                	//
	};

	struct RGSendMsg
	{
		static const USHORT	wCmd = R_G_SENDMSG;

		UINT                  	ErrCode;                            	//
		RPlayerID             	playerID;                           	//
		SendMsgResult         	result;                             	//
	};

	struct TeamAttriKey
	{
		USHORT                	attriKeyLen;                        	//
		CHAR                  	attriKey[MAX_TEAM_ATTRI_KEY_LEN];     	//
	};

	struct TeamAttriValue
	{
		USHORT                	attriValueLen;                      	//
		CHAR                  	attriValue[MAX_TEAM_ATTRI_VALUE_LEN]; 	//
	};

	struct TeamAttri
	{
		TeamAttriKey          	attriKey;                           	//
		TeamAttriValue        	attriValue;                         	//
	};

	struct AttriArray
	{
		USHORT                	attrisLen;                          	//
		TeamAttri             	attris[MAX_ATTRI_COUNT];              	//
	};

	struct TeamInd
	{
		USHORT                	teamIndLen;                         	//
		CHAR                  	teamInd[MAX_TEAM_IND_LEN];            	//
	};

	struct TeamName
	{
		USHORT                	teamNameLen;                        	//
		CHAR                  	teamName[MAX_TEAM_NAME_LEN];          	//
	};

	struct MemberName
	{
		USHORT                	nameLen;                            	//
		CHAR                  	memberName[MAX_TEAM_NAME_LEN];        	//
	};

	struct MemberInd
	{
		USHORT                	memberIndLen;                       	//
		CHAR                  	memberInd[MAX_MEMBER_IND_LEN];        	//
	};

	struct MembersInd
	{
		USHORT                	membersSize;                        	//
		MemberInd             	members[MAX_TEAM_MEMBERS_COUNT];      	//
	};

	struct Member
	{
		MemberInd             	memberInd;                          	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	memberSeq;                          	//
		MemberName            	memberName;                         	//
		CHAR                  	memberJob;                          	//
		USHORT                	mapId;                              	//
		USHORT                	avatarId;                           	//
		USHORT                	memberLevel;                        	//
		CHAR                  	memberSex;                          	//
	};

	struct Members
	{
		UINT                  	membersSize;                        	//
		Member                	members[MAX_TEAM_MEMBERS_COUNT];      	//
	};

	struct Team
	{
		TeamInd               	teamInd;                            	//
		Members               	teamMembers;                        	//
		UINT                  	captainGateId;                      	//
		UINT                  	captainPlayerId;                    	//
		USHORT                	expSharedMode;                      	//
		USHORT                	goodSharedMode;                     	//
		BYTE                  	rollItemLevel;                      	//
	};

	struct CreateTeamRequest
	{
		static const USHORT	wCmd = G_R_CreateTeam;

		UINT                  	requestSeq;                         	//
		Team                  	team;                               	//
	};

	struct CreateTeamResult
	{
		static const USHORT	wCmd = R_G_CreateTeamResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		Team                  	team;                               	//
	};

	struct DestroyTeamRequest
	{
		static const USHORT	wCmd = G_R_DestroyTeam;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
	};

	struct DestroyTeamResult
	{
		static const USHORT	wCmd = R_G_DestroyTeamResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
	};

	struct AddTeamMemberRequest
	{
		static const USHORT	wCmd = G_R_AddTeamMember;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	palyerId;                           	//
		Member                	member;                             	//
		UINT                  	flag;                               	//
	};

	struct AddTeamMemberResult
	{
		static const USHORT	wCmd = R_G_AddTeamMemberResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		Member                	member;                             	//
		UINT                  	gateId;                             	//
		UINT                  	palyerId;                           	//
	};

	struct RemoveTeamMemberRequest
	{
		static const USHORT	wCmd = G_R_RemoveTeamMember;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	palyerId;                           	//
		CHAR                  	removedMode;                        	//
	};

	struct RemoveTeamMemberResult
	{
		static const USHORT	wCmd = R_G_RemoveTeamMemberResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	memberSeq;                          	//
		CHAR                  	removedMode;                        	//
	};

	struct QueryTeamRequest
	{
		static const USHORT	wCmd = G_R_QueryTeam;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct QueryTeamResult
	{
		static const USHORT	wCmd = R_G_QueryTeamResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		Team                  	team;                               	//
	};

	struct QueryTeamMemberRequest
	{
		static const USHORT	wCmd = G_R_QueryTeamMember;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct QueryTeamMemberResult
	{
		static const USHORT	wCmd = R_G_QueryTeamMemberResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		Member                	member;                             	//
	};

	struct UpdateMemberRequest
	{
		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		Member                	member;                             	//
	};

	struct UpdateMemberResult
	{
		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		Member                	member;                             	//
	};

	struct AddTeamAttriRequest
	{
		static const USHORT	wCmd = G_R_AddTeamAttri;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		TeamAttriKey          	attriKey;                           	//
		TeamAttriValue        	attriValue;                         	//
	};

	struct AddTeamAttriResult
	{
		static const USHORT	wCmd = R_G_AddTeamAttriResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		TeamAttriKey          	attriKey;                           	//
		TeamAttriValue        	attriValue;                         	//
	};

	struct RemoveTeamAttriRequest
	{
		static const USHORT	wCmd = G_R_RemoveTeamAttri;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		TeamAttriKey          	attriKey;                           	//
	};

	struct RemoveTeamAttriResult
	{
		static const USHORT	wCmd = R_G_RemoveTeamAttriResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		TeamAttriKey          	attriKey;                           	//
	};

	struct AddTeamMemberAttriRequest
	{
		static const USHORT	wCmd = G_R_AddTeamMemberAttri;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		TeamAttriKey          	attriKey;                           	//
		TeamAttriValue        	attriValue;                         	//
	};

	struct AddTeamMemberAttriResult
	{
		static const USHORT	wCmd = R_G_AddTeamMemberAttriResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		MemberInd             	memberInd;                          	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	memberSeq;                          	//
		TeamAttriKey          	attriKey;                           	//
		TeamAttriValue        	attriValue;                         	//
	};

	struct RemoveTeamMemberAttriRequest
	{
		static const USHORT	wCmd = G_R_RemoveTeamMemberAttri;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		MemberInd             	memberInd;                          	//
		TeamAttriKey          	attriKey;                           	//
	};

	struct RemoveTeamMemberAttriResult
	{
		static const USHORT	wCmd = R_G_RemoveTeamMemberAttriResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		MemberInd             	memberInd;                          	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	memberSeq;                          	//
		TeamAttriKey          	attriKey;                           	//
		TeamAttriValue        	attriValue;                         	//
	};

	struct BroadcastData
	{
		UINT                  	dataLen;                            	//
		CHAR                  	data[MAX_TEAM_BROADCAST_DATA_LEN];    	//
	};

	struct TeamBroadcastRequest
	{
		static const USHORT	wCmd = G_R_TeamBroadcast;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		MemberInd             	fromMemberInd;                      	//
		UINT                  	fromGateId;                         	//
		UINT                  	fromPlayerId;                       	//
		USHORT                	dataType;                           	//
		BroadcastData         	broadcastData;                      	//
	};

	struct TeamBroadcastResult
	{
		static const USHORT	wCmd = R_G_TeamBroadcastResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	fromGateId;                         	//
		UINT                  	fromPlayerId;                       	//
		USHORT                	dataType;                           	//
		BroadcastData         	broadcastData;                      	//
	};

	struct TeamBroadcastNotify
	{
		static const USHORT	wCmd = R_G_TeamBroadcastNotify;

		TeamInd               	teamInd;                            	//
		MemberInd             	fromMemberInd;                      	//
		UINT                  	fromGateId;                         	//
		UINT                  	fromPlayerId;                       	//
		MemberInd             	toMemberInd;                        	//
		UINT                  	toGateId;                           	//
		UINT                  	toPlayerId;                         	//
		USHORT                	dataType;                           	//
		BroadcastData         	broadcastData;                      	//
	};

	struct TeamMember2MemberRequest
	{
		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		MemberInd             	fromMemberInd;                      	//
		UINT                  	fromGateId;                         	//
		UINT                  	fromPlayerId;                       	//
		MemberInd             	toMemberInd;                        	//
		UINT                  	toGateId;                           	//
		UINT                  	toPlayerId;                         	//
		BroadcastData         	data;                               	//
	};

	struct TeamMember2MemberResult
	{
		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		MemberInd             	fromMemberInd;                      	//
		UINT                  	fromGateId;                         	//
		UINT                  	fromPlayerId;                       	//
		MemberInd             	toMemberInd;                        	//
		UINT                  	toGateId;                           	//
		UINT                  	toPlayerId;                         	//
		BroadcastData         	data;                               	//
	};

	struct ModifyTeamMemberMapIdRequest
	{
		static const USHORT	wCmd = G_R_ModifyTeamMemberMapId;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	GateId;                             	//
		UINT                  	PlayerId;                           	//
		USHORT                	mapId;                              	//
	};

	struct ModifyTeamMemberMapIdResult
	{
		static const USHORT	wCmd = R_G_ModifyTeamMemberMapIdResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	GateId;                             	//
		UINT                  	PlayerId;                           	//
		USHORT                	mapId;                              	//
	};

	struct ModifyTeamMemberMapIdNotify
	{
		static const USHORT	wCmd = R_G_ModifyTeamMemberMapIdNotify;

		TeamInd               	teamInd;                            	//
		UINT                  	GateId;                             	//
		UINT                  	PlayerId;                           	//
		USHORT                	mapId;                              	//
	};

	struct TeamCreateedNotify
	{
		static const USHORT	wCmd = R_G_TeamCreatedNotify;

		UINT                  	playerId;                           	//
		UINT                  	gateId;                             	//
		Team                  	team;                               	//
	};

	struct TeamDestroyedNotify
	{
		static const USHORT	wCmd = R_G_TeamDestroyedNotify;

		UINT                  	causeCode;                          	//
		MemberInd             	memberInd;                          	//
		UINT                  	playerId;                           	//
		UINT                  	gateId;                             	//
		UINT                  	memberSeq;                          	//
		TeamInd               	teamInd;                            	//
	};

	struct AddTeamMemberNotify
	{
		static const USHORT	wCmd = R_G_AddTeamMemberNotify;

		TeamInd               	teamInd;                            	//
		MemberInd             	memberInd;                          	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	memberSeq;                          	//
		Team                  	team;                               	//
		UINT                  	flag;                               	//
		UINT                  	gateIdAdded;                        	//
		UINT                  	playerIdAdded;                      	//
	};

	struct RemoveTeamMemberNotify
	{
		static const USHORT	wCmd = R_G_RemoveTeamMemberNotify;

		TeamInd               	teamInd;                            	//
		MemberInd             	memberInd;                          	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	memberSeq;                          	//
		Member                	removedMember;                      	//
		USHORT                	removeMode;                         	//
	};

	struct UpdateTeamMemberNotify
	{
		static const USHORT	wCmd = R_G_UpdateTeamMemberNotify;

		TeamInd               	teamInd;                            	//
		MemberInd             	memberInd;                          	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	memberSeq;                          	//
		Member                	member;                             	//
	};

	struct ReportGateSvrIdRequest
	{
		static const USHORT	wCmd = G_R_ReportGateId;

		UINT                  	requestSeq;                         	//
		UINT                  	gateId;                             	//
	};

	struct ReportGateSvrIdResult
	{
		static const USHORT	wCmd = R_G_ReportGateIdResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
	};

	struct ModifyTeamCaptainRequest
	{
		static const USHORT	wCmd = G_R_ModifyTeamCaptain;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ModifyTeamCaptainResult
	{
		static const USHORT	wCmd = R_G_ModifyTeamCaptainResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ModifyTeamCaptainNotify
	{
		static const USHORT	wCmd = R_G_TeamCaptainModifiedNotify;

		TeamInd               	teamInd;                            	//
		UINT                  	playerId;                           	//
		UINT                  	gateId;                             	//
		UINT                  	captainPlayerId;                    	//
		UINT                  	captainGateId;                      	//
	};

	struct ModifyExpSharedModeRequest
	{
		static const USHORT	wCmd = G_R_ModifyExpSharedMode;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		USHORT                	mode;                               	//
	};

	struct ModifyExpSharedModeResult
	{
		static const USHORT	wCmd = R_G_ModifyExpSharedModeResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		USHORT                	mode;                               	//
	};

	struct ModifyGoodSharedModeRequest
	{
		static const USHORT	wCmd = G_R_ModifyGoodSharedMode;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		USHORT                	mode;                               	//
	};

	struct ModifyGoodSharedModeResult
	{
		static const USHORT	wCmd = R_G_ModifyGoodSharedModeResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		USHORT                	mode;                               	//
	};

	struct ModifyExpSharedModeNotify
	{
		static const USHORT	wCmd = R_G_ModifyExpSharedModeNotify;

		TeamInd               	teamInd;                            	//
		USHORT                	mode;                               	//
		UINT                  	playerId;                           	//
		UINT                  	gateId;                             	//
		UINT                  	memberSeq;                          	//
	};

	struct ModifyGoodSharedModeNotify
	{
		static const USHORT	wCmd = R_G_ModifyGoodSharedModeNotify;

		TeamInd               	teamInd;                            	//
		USHORT                	mode;                               	//
		UINT                  	memberSeq;                          	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ModifyTeamRollItemLevelRequest
	{
		static const USHORT	wCmd = G_R_ModifyTeamRollItemLevel;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		BYTE                  	level;                              	//
	};

	struct ModifyTeamRollItemLevelResult
	{
		static const USHORT	wCmd = R_G_ModifyTeamRollItemLevelResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		BYTE                  	level;                              	//
	};

	struct ModifyTeamRollItemLevelNotify
	{
		static const USHORT	wCmd = R_G_ModifyTeamRollItemLevelNotify;

		TeamInd               	teamInd;                            	//
		UINT                  	fromGateId;                         	//
		UINT                  	fromPlayerId;                       	//
		UINT                  	toGateId;                           	//
		UINT                  	toPlayerId;                         	//
		BYTE                  	level;                              	//
	};

	struct QueryTeamMembersCountRequest
	{
		static const USHORT	wCmd = G_R_QueryTeamMembersCount;

		UINT                  	requestSeq;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct QueryTeamMembersCountResult
	{
		static const USHORT	wCmd = R_G_QueryTeamMembersCountResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		TeamInd               	teamInd;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	count;                              	//
	};

	struct ChatGroupName
	{
		USHORT                	groupNameLen;                       	//
		CHAR                  	groupName[MAX_CHAT_GROUP_NAME_LEN];   	//
	};

	struct ChatGroupMember
	{
		MemberInd             	memberId;                           	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ChatGroupMembersList
	{
		USHORT                	membersCount;                       	//
		MemberInd             	members[MAX_CHAT_GROUP_MEMBERS_COUNT];	//
	};

	struct ChatGroupPassword
	{
		USHORT                	passwordLen;                        	//
		CHAR                  	password[MAX_CHAT_GROUP_PASSWORD_LEN];	//
	};

	struct ChatGroup
	{
		UINT                  	groupId;                            	//
		ChatGroupMembersList  	members;                            	//
		ChatGroupPassword     	password;                           	//
		ChatGroupMember       	owner;                              	//
	};

	struct ChatData
	{
		USHORT                	dataLen;                            	//
		CHAR                  	data[MAX_CHAT_DATA_LEN];              	//
	};

	struct CreateChatGroupRequest
	{
		static const USHORT	wCmd = G_R_CreateChatGroup;

		UINT                  	requestSeq;                         	//
		ChatGroup             	group;                              	//
	};

	struct CreateChatGroupResult
	{
		static const USHORT	wCmd = R_G_CreateChatGroupResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		ChatGroup             	group;                              	//
	};

	struct DestroyChatGroupRequest
	{
		static const USHORT	wCmd = G_R_DestroyChatGroup;

		UINT                  	requestSeq;                         	//
		UINT                  	groupId;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct DestroyChatGroupResult
	{
		static const USHORT	wCmd = R_G_DestroyChatGroupResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		UINT                  	groupId;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ChatGroupDestroyedNotify
	{
		static const USHORT	wCmd = R_G_ChatGroupDestroyedNotify;

		UINT                  	groupId;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct AddChatGroupMemberRequest
	{
		static const USHORT	wCmd = G_R_AddChatGroupMember;

		UINT                  	requestSeq;                         	//
		ChatGroupMember       	memberAdded;                        	//
		UINT                  	groupId;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		ChatGroupPassword     	passwrod;                           	//
	};

	struct AddChatGroupMemberResult
	{
		static const USHORT	wCmd = R_G_AddChatGroupMemberResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		ChatGroup             	group;                              	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ChatGroupMemberAddedNotify
	{
		static const USHORT	wCmd = R_G_ChatGroupMemberAddedNotify;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		ChatGroupMember       	memberAdded;                        	//
	};

	struct RemoveChatGroupMemberRequest
	{
		static const USHORT	wCmd = G_R_RemoveChatGroupMember;

		UINT                  	requestSeq;                         	//
		ChatGroupMember       	memberRemoved;                      	//
		UINT                  	groupId;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct RemoveChatGroupMemberResult
	{
		static const USHORT	wCmd = R_G_RemoveChatGroupMemberResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		ChatGroupMember       	memberRemoved;                      	//
		UINT                  	groupId;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ChatGroupMemberRemovedNotify
	{
		static const USHORT	wCmd = R_G_ChatGroupMemberRemovedNotify;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	groupId;                            	//
		ChatGroupMember       	memberRemoved;                      	//
	};

	struct ChatGroupSpeekRequest
	{
		static const USHORT	wCmd = G_R_ChatGroupSpeek;

		UINT                  	requestSeq;                         	//
		UINT                  	groupId;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		ChatData              	data;                               	//
	};

	struct ChatGroupSpeekResult
	{
		static const USHORT	wCmd = R_G_ChatGroupSpeekResult;

		UINT                  	requestSeq;                         	//
		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ChatGroupSpeekNotify
	{
		static const USHORT	wCmd = R_G_ChatGroupSpeekNotify;

		UINT                  	groupId;                            	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		MemberInd             	talker;                             	//
		ChatData              	data;                               	//
	};

	struct UnionMember
	{
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	uiid;                               	//
		USHORT                	nameLen;                            	//
		CHAR                  	name[MAX_PLAYER_NAME_LEN];            	//
		USHORT                	job;                                	//
		USHORT                	posSeq;                             	//
		USHORT                	titleLen;                           	//
		CHAR                  	title[MAX_PLAYER_NAME_LEN];           	//
		USHORT                	level;                              	//
		UINT                  	lastQuit;                           	//
		USHORT                	forbid;                             	//
		USHORT                	onLine;                             	//
	};

	struct UnionPosRight
	{
		USHORT                	seq;                                	//
		USHORT                	posLen;                             	//
		CHAR                  	pos[8];                               	//
		UINT                  	right;                              	//
	};

	struct Union
	{
		UINT                  	id;                                 	//
		USHORT                	nameLen;                            	//
		CHAR                  	name[MAX_UNION_NAME_LEN];             	//
		USHORT                	level;                              	//
		USHORT                	num;                                	//
		UINT                  	ownerUiid;                          	//
		USHORT                	titleLen;                           	//
		CHAR                  	title[MAX_UNION_TITLE_LEN];           	//
		UINT                  	active;                             	//
		USHORT                	posListLen;                         	//
		UnionPosRight         	posList[10];                          	//
	};

	struct ReportPlayerUpline
	{
		static const USHORT	wCmd = G_R_REPORT_PLAYER_UPLINE;

		UnionMember           	member;                             	//
	};

	struct ReportPlayerOffline
	{
		static const USHORT	wCmd = G_R_REPORT_PLAYER_OFFLINE;

		UINT                  	uiid;                               	//
	};

	struct CreateUnionRequest
	{
		static const USHORT	wCmd = G_R_CREATE_UNION_REQUEST;

		Union                 	_union;                             	//
	};

	struct CreateUnionResult
	{
		static const USHORT	wCmd = R_G_CREATE_UNION_RESULT;

		INT                   	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		Union                 	_union;                             	//
		USHORT                	captionNameLen;                     	//
		CHAR                  	captionName[MAX_UNION_NAME_LEN];      	//
	};

	struct DestroyUnionRequest
	{
		static const USHORT	wCmd = G_R_DESTROY_UNION_REQUEST;

		UINT                  	uiid;                               	//
	};

	struct DestroyUnionResult
	{
		static const USHORT	wCmd = R_G_DESTROY_UNION_RESULT;

		INT                   	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct UnionDestroyedNotify
	{
		static const USHORT	wCmd = R_G_UNION_DESTROYED_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct UnionMemberList
	{
		USHORT                	count;                              	//
		UnionMember           	members[MAX_UNION_MEMBER_LIST_SIZE];  	//
	};

	struct QueryUnionBasicRequest
	{
		static const USHORT	wCmd = G_R_QUERY_UNION_BASIC_REQUEST;

		UINT                  	uiid;                               	//
	};

	struct QueryUnionBasicResult
	{
		static const USHORT	wCmd = R_G_QUERY_UNION_BASIC_RESULT;

		INT                   	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		CHAR                  	bGate;                              	//
		Union                 	_union;                             	//
	};

	struct QueryUnionMembersRequest
	{
		static const USHORT	wCmd = G_R_QUERY_UNION_MEMBER_LIST_REQUEST;

		UINT                  	uiid;                               	//
	};

	struct QueryUnionMembersResult
	{
		static const USHORT	wCmd = R_G_QUERY_UNION_MEMBER_LIST_RESULT;

		INT                   	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UnionMemberList       	members;                            	//
	};

	struct AddUnionMemberRequest
	{
		static const USHORT	wCmd = G_R_UNION_ADD_MEMBER_REQUEST;

		UINT                  	uiid;                               	//
		USHORT                	memberAddedNameLen;                 	//
		CHAR                  	memberAddedName[MAX_PLAYER_NAME_LEN]; 	//
	};

	struct AddUnionMemberConfirm
	{
		static const USHORT	wCmd = R_G_UNION_ADD_MEMBER_CONFIRM;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	unionNameLen;                       	//
		CHAR                  	unionName[MAX_UNION_NAME_LEN];        	//
		USHORT                	sponsorLen;                         	//
		CHAR                  	sponsor[MAX_PLAYER_NAME_LEN];         	//
	};

	struct AddUnionMemberConfirmed
	{
		static const USHORT	wCmd = G_R_UNION_ADD_MEMBER_CONFIRMED;

		UINT                  	uiid;                               	//
		USHORT                	sponsorLen;                         	//
		CHAR                  	sponsor[MAX_PLAYER_NAME_LEN];         	//
		USHORT                	unionNameLen;                       	//
		CHAR                  	unionName[MAX_UNION_NAME_LEN];        	//
		USHORT                	flag;                               	//
	};

	struct AddUnionMemberResult
	{
		static const USHORT	wCmd = R_G_UNION_ADD_MEMBER_RESULT;

		INT                   	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	memberAddedNameLen;                 	//
		CHAR                  	memberAddedName[MAX_PLAYER_NAME_LEN]; 	//
	};

	struct AddUnionMemberNotify
	{
		static const USHORT	wCmd = R_G_UNION_MEMBER_ADDED_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UnionMember           	memberAdded;                        	//
	};

	struct RemoveUnionMemberRequest
	{
		static const USHORT	wCmd = G_R_UNION_REMOVE_MEMBER_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	memberRemoved;                      	//
	};

	struct RemoveUnionMemberResult
	{
		static const USHORT	wCmd = R_G_UNION_REMOVE_MEMBER_RESULT;

		INT                   	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UnionMember           	memberRemoved;                      	//
	};

	struct RemoveUnionMemberNotify
	{
		static const USHORT	wCmd = R_G_UNION_MEMBER_REMOVED_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	flag;                               	//
		UnionMember           	memberRemoved;                      	//
	};

	struct UnionPosRightsNotify
	{
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	rights;                             	//
		USHORT                	posLen;                             	//
		CHAR                  	pos[MAX_PLAYER_NAME_LEN];             	//
		USHORT                	flag;                               	//
	};

	struct ModifyUnionPosRighstRequest
	{
		UINT                  	uiid;                               	//
		USHORT                	posLen;                             	//
		CHAR                  	pos[MAX_PLAYER_NAME_LEN];             	//
		UINT                  	rights;                             	//
	};

	struct ModifyUnionPosRighstResult
	{
		INT                   	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	posLen;                             	//
		CHAR                  	pos[MAX_PLAYER_NAME_LEN];             	//
	};

	struct AddUnionPosRequest
	{
		UINT                  	uiid;                               	//
		USHORT                	posLen;                             	//
		CHAR                  	pos[MAX_PLAYER_NAME_LEN];             	//
		UINT                  	rights;                             	//
	};

	struct AddUnionPosResult
	{
		INT                   	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	posLen;                             	//
		CHAR                  	pos[MAX_PLAYER_NAME_LEN];             	//
	};

	struct RemoveUnionPosRequest
	{
		UINT                  	uiid;                               	//
		USHORT                	posLen;                             	//
		CHAR                  	pos[MAX_PLAYER_NAME_LEN];             	//
	};

	struct RemoveUnionPosResult
	{
		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	posLen;                             	//
		CHAR                  	pos[MAX_PLAYER_NAME_LEN];             	//
	};

	struct TransformUnionCaptionRequest
	{
		static const USHORT	wCmd = G_R_UNION_TRANSFROM_CAPTION_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	newCaptionUiid;                     	//
	};

	struct TransformUnionCaptionResult
	{
		static const USHORT	wCmd = R_G_UNION_TRANSFORM_CAPTION_RESULT;

		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct TransformUnionCaptionNotify
	{
		static const USHORT	wCmd = R_G_UNION_TRANSFORM_CAPTION_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	newCaptionNameLen;                  	//
		CHAR                  	newCaptionName[MAX_PLAYER_NAME_LEN];  	//
	};

	struct ModifyUnionMemberTitleRequest
	{
		static const USHORT	wCmd = G_R_UNION_MODIFY_MEMBER_TITLE_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	modifiedUiid;                       	//
		USHORT                	titleLen;                           	//
		CHAR                  	title[MAX_PLAYER_NAME_LEN];           	//
	};

	struct ModifyUnionMemberTitleResult
	{
		static const USHORT	wCmd = R_G_UNION_MODIFY_MEMBER_TITLE_RESULT;

		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct UnionMemberTitleNotify
	{
		static const USHORT	wCmd = R_G_UNION_MEMBER_TITLE_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	memberNameLen;                      	//
		CHAR                  	memberName[MAX_PLAYER_NAME_LEN];      	//
		USHORT                	titleLen;                           	//
		CHAR                  	title[MAX_PLAYER_NAME_LEN];           	//
	};

	struct AdvanceUnionMemberPosRequest
	{
		static const USHORT	wCmd = G_R_UNION_ADVANCE_MEMBER_POS_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	advancedUiid;                       	//
	};

	struct AdvanceUnionMemberPosResult
	{
		static const USHORT	wCmd = R_G_UNION_ADVANCE_MEMBER_POS_RESULT;

		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct AdvanceUnionMemberPosNotify
	{
		static const USHORT	wCmd = R_G_UNION_ADVANCE_MEMBER_POS_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	memberNameLen;                      	//
		CHAR                  	memberName[MAX_PLAYER_NAME_LEN];      	//
		USHORT                	posSeq;                             	//
	};

	struct ReduceUnionMemberPosRequest
	{
		static const USHORT	wCmd = G_R_UNION_REDUCE_MEMBER_POS_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	reducedUiid;                        	//
	};

	struct ReduceUnionMemberPosResult
	{
		static const USHORT	wCmd = R_G_UNION_REDUCE_MEMBER_POS_RESULT;

		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ReduceUnionMemberPosNotify
	{
		static const USHORT	wCmd = R_G_UNION_REDUCE_MEMBER_POS_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	memberNameLen;                      	//
		CHAR                  	memberName[MAX_PLAYER_NAME_LEN];      	//
		USHORT                	posSeq;                             	//
	};

	struct UnionChannelSpeekRequest
	{
		static const USHORT	wCmd = G_R_UNION_CHANNEL_SPEEK_REQUEST;

		UINT                  	uiid;                               	//
		USHORT                	contentLen;                         	//
		CHAR                  	content[MAX_CHAT_DATA_LEN];           	//
	};

	struct UnionChannelSpeekResult
	{
		static const USHORT	wCmd = R_G_UNION_CHANNEL_SPEEK_RESULT;

		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct UnionChannelSpeekNotify
	{
		static const USHORT	wCmd = R_G_UNION_CHANNEL_SPEEK_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	speekerNameLen;                     	//
		CHAR                  	speekerName[MAX_PLAYER_NAME_LEN];     	//
		USHORT                	speekerLine;                        	//
		USHORT                	contentLen;                         	//
		CHAR                  	content[MAX_CHAT_DATA_LEN];           	//
	};

	struct UpdateUnionPosCfgRequest
	{
		static const USHORT	wCmd = G_R_UNION_UPDATE_POS_CFG_REQUEST;

		UINT                  	uiid;                               	//
		USHORT                	posListLen;                         	//
		UnionPosRight         	posList[10];                          	//
	};

	struct UpdateUnionPosCfgResult
	{
		static const USHORT	wCmd = R_G_UNION_UPDATE_POS_CFG_RESULT;

		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct UpdateUnionPosCfgNotify
	{
		static const USHORT	wCmd = R_G_UNION_UPDATE_POS_CFG_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	posListLen;                         	//
		UnionPosRight         	posList[10];                          	//
	};

	struct ForbidUnionSpeekRequest
	{
		static const USHORT	wCmd = G_R_UNION_FORBID_SPEEK_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	forbidUiid;                         	//
	};

	struct ForbidUnionSpeekResult
	{
		static const USHORT	wCmd = R_G_UNION_FORBID_SPEEK_RESULT;

		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
	};

	struct ForbidUnionSpeekNotify
	{
		static const USHORT	wCmd = R_G_UNION_FORBID_SPEEK_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	nameLen;                            	//
		CHAR                  	name[MAX_PLAYER_NAME_LEN];            	//
	};

	struct UnionSkill
	{
		UINT                  	skillId;                            	//
		USHORT                	skillLevel;                         	//
	};

	struct UnionSkillsListNotify
	{
		static const USHORT	wCmd = R_G_UNION_SKILLS_LIST_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		CHAR                  	bGate;                              	//
		UINT                  	guildId;                            	//
		USHORT                	skillsListLen;                      	//
		UnionSkill            	skillsList[10];                       	//
	};

	struct StudyUnionSkillRequest
	{
		static const USHORT	wCmd = G_R_STUDY_UNION_SKILL_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	skillId;                            	//
	};

	struct StudyUnionSkillResult
	{
		static const USHORT	wCmd = R_G_STUDY_UNION_SKILL_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	returnCode;                         	//
		UINT                  	skillId;                            	//
	};

	struct ModifyUnionActivePointRequest
	{
		static const USHORT	wCmd = G_R_MODIFY_UNION_ACTIVE_POINT_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	point;                              	//
		CHAR                  	bAdd;                               	//
	};

	struct ModifyUnionActivePointResult
	{
		static const USHORT	wCmd = R_G_MODIFY_UNION_ACTIVE_POINT_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	returnCode;                         	//
	};

	struct UnionActivePointNotify
	{
		static const USHORT	wCmd = R_G_UNION_ACTIVE_POINT_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	point;                              	//
	};

	struct UnionTaskList
	{
		USHORT                	taskCount;                          	//
		UINT                  	task[20];                             	//
	};

	struct PostUnionTasksListRequest
	{
		static const USHORT	wCmd = G_R_POST_UNION_TASK_LIST_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	durationTime;                       	//
	};

	struct PostUnionTasksListResult
	{
		static const USHORT	wCmd = R_G_POST_UNION_TASK_LIST_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	returnCode;                         	//
	};

	struct UnionTasksListNotify
	{
		static const USHORT	wCmd = R_G_UNION_TASK_LIST_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	guildIt;                            	//
		UINT                  	endTime;                            	//
		UINT                  	activePoint;                        	//
	};

	struct AdvanceUnionLevelRequest
	{
		static const USHORT	wCmd = G_R_ADVANCE_UNION_LEVEL_REQUEST;

		UINT                  	uiid;                               	//
	};

	struct AdvanceUnionLevelResult
	{
		static const USHORT	wCmd = R_G_ADVANCE_UNION_LEVEL_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	returnCode;                         	//
	};

	struct UnionLevelNotify
	{
		static const USHORT	wCmd = R_G_UNION_LEVEL_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	level;                              	//
		UINT                  	active;                             	//
	};

	struct PostUnionBulletinRequest
	{
		static const USHORT	wCmd = G_R_POST_UNION_BULLETIN_REQUEST;

		UINT                  	uiid;                               	//
		USHORT                	seq;                                	//
		USHORT                	end;                                	//
		USHORT                	contentLen;                         	//
		CHAR                  	content[400];                         	//
	};

	struct PostUnionBulletinResult
	{
		static const USHORT	wCmd = R_G_POST_UNION_BULLETIN_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	returnCode;                         	//
	};

	struct UnionBulletinNotify
	{
		static const USHORT	wCmd = R_G_UNION_BULLETIN_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	seq;                                	//
		USHORT                	contentLen;                         	//
		CHAR                  	content[400];                         	//
	};

	struct UnionMemberUplineNotify
	{
		static const USHORT	wCmd = R_G_UNION_MEMBER_UPLINE_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	uplinePlayer;                       	//
	};

	struct UnionMemberOfflineNotify
	{
		static const USHORT	wCmd = R_G_UNION_MEMBER_OFFLINE_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	offlinePlayer;                      	//
	};

	struct LogEntryParam
	{
		USHORT                	paramLen;                           	//
		CHAR                  	param[32];                            	//
	};

	struct UnionLogEntry
	{
		USHORT                	eventId;                            	//
		UINT                  	datetime;                           	//
		LogEntryParam         	param1;                             	//
		LogEntryParam         	param2;                             	//
	};

	struct QueryUnionLogRequest
	{
		static const USHORT	wCmd = G_R_QUERY_UNION_LOG_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	bookmark;                           	//
	};

	struct QueryUnionLogResult
	{
		static const USHORT	wCmd = R_G_QUERY_UNION_LOG_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	logListLen;                         	//
		UnionLogEntry         	logList[5];                           	//
		UINT                  	bookmark;                           	//
	};

	struct UnionEmailNotify
	{
		static const USHORT	wCmd = R_G_UNION_EMAIL_NOTIFY;

		UINT                  	uiid;                               	//
		USHORT                	nameLen;                            	//
		CHAR                  	name[16];                             	//
		USHORT                	titleLen;                           	//
		CHAR                  	emailTitle[40];                       	//
		USHORT                	contentLen;                         	//
		CHAR                  	emailContent[300];                    	//
	};

	struct UnionOwnerCheckRequest
	{
		static const USHORT	wCmd = G_R_UNION_OWNER_CHECK_REQUEST;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	uiid;                               	//
	};

	struct UnionOwnerCheckResult
	{
		static const USHORT	wCmd = R_G_UNION_OWNER_CHECK_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	uiid;                               	//
		USHORT                	flag;                               	//
	};

	struct ModifyUnionPrestigeRequest
	{
		static const USHORT	wCmd = G_R_UNION_MODIFY_PRESTIGE_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	point;                              	//
		CHAR                  	bAdd;                               	//
	};

	struct ModifyUnionPrestigeResult
	{
		static const USHORT	wCmd = R_G_UNION_MODIFY_PRESTIGE_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	returnCode;                         	//
	};

	struct UnionPrestigeNotify
	{
		static const USHORT	wCmd = R_G_UNION_PRESTIGE_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	prestige;                           	//
	};

	struct ModifyUnionBadgeRequest
	{
		static const USHORT	wCmd = G_R_UNION_MODIFY_BADGE_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	dataLen;                            	//
		CHAR                  	badgeData[200];                       	//
	};

	struct ModifyUnionBadgeResult
	{
		static const USHORT	wCmd = R_G_UNION_MODIFY_BADGE_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	returnCode;                         	//
	};

	struct UnionBadgeNotify
	{
		static const USHORT	wCmd = R_G_UNION_BADGE_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	dataLen;                            	//
		CHAR                  	badgeData[200];                       	//
	};

	struct ModifyUnionMultiRequest
	{
		static const USHORT	wCmd = G_R_UNION_MODIFY_MULTI_REQUEST;

		UINT                  	uiid;                               	//
		USHORT                	type;                               	//
		USHORT                	multi;                              	//
		UINT                  	endTime;                            	//
	};

	struct UnionMultiNotify
	{
		static const USHORT	wCmd = R_G_UNION_MULTI_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	type;                               	//
		USHORT                	multi;                              	//
		UINT                  	endTime;                            	//
	};

	struct QueryPlayerRelationRequest
	{
		static const USHORT	wCmd = G_R_QUERY_PLAYER_RELATION_REQUEST;

		UINT                  	gmGateId;                           	//
		UINT                  	gmPlayerId;                         	//
		UINT                  	uiid;                               	//
	};

	struct QueryPlayerRelationResult
	{
		static const USHORT	wCmd = R_G_QUERY_PLAYER_RELATION_RESULT;

		UINT                  	gmGateId;                           	//
		UINT                  	gmPlayerId;                         	//
		UINT                  	uiid;                               	//
		RGFriendInfos         	relation;                           	//
	};

	struct QueryPlayerInfoRequest
	{
		static const USHORT	wCmd = G_R_QUERY_PLAYER_INFO_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	nameLen;                            	//
		CHAR                  	targetName[MAX_PLAYER_NAME_LEN];      	//
	};

	struct QueryPlayerInfoResult
	{
		static const USHORT	wCmd = R_G_QUERY_PLAYER_INFO_RESULT;

		UINT                  	returnCode;                         	//
		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		FriendInfo            	targetInfo;                         	//
	};

	struct QueryUnionRankRequest
	{
		static const USHORT	wCmd = G_R_QUERY_UNION_RANK_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	race;                               	//
		UINT                  	page;                               	//
	};

	struct UnionRankInfo
	{
		USHORT                	unionNameLen;                       	//
		CHAR                  	unionName[MAX_UNION_NAME_LEN];        	//
		USHORT                	unionLevel;                         	//
		USHORT                	captionNameLen;                     	//
		CHAR                  	captionName[MAX_PLAYER_NAME_LEN];     	//
		USHORT                	unionRace;                          	//
		USHORT                	unionPretige;                       	//
	};

	struct UnionRaceMasterInfo
	{
		USHORT                	unionNameLen;                       	//
		CHAR                  	unionName[MAX_UNION_NAME_LEN];        	//
		USHORT                	unionLevel;                         	//
		USHORT                	captionNameLen;                     	//
		CHAR                  	captionName[MAX_PLAYER_NAME_LEN];     	//
		USHORT                	unionRace;                          	//
		UINT                  	captionId;                          	//
	};

	struct QueryUnionRankResult
	{
		static const USHORT	wCmd = R_G_QUERY_UNION_RANK_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		USHORT                	page;                               	//
		UINT                  	len;                                	//
		UnionRankInfo         	rank[10];                             	//
		USHORT                	endFlag;                            	//
	};

	struct SearchUnionRankRequest
	{
		static const USHORT	wCmd = G_R_SEARCH_UNION_RANK_REQUEST;

		UINT                  	uiid;                               	//
		UINT                  	race;                               	//
		USHORT                	unionNameLen;                       	//
		CHAR                  	unionName[MAX_UNION_NAME_LEN];        	//
	};

	struct SearchUnionRankResult
	{
		static const USHORT	wCmd = R_G_SEARCH_UNION_RANK_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	resultCode;                         	//
		UnionRankInfo         	unionInfo;                          	//
	};

	struct PlayerUplineTeamIdNotify
	{
		static const USHORT	wCmd = R_G_PLAYER_UPLINE_TEAMID_NOTIFY;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	teamId;                             	//
		UINT                  	playerFlag;                         	//
		UINT                  	teamFlag;                           	//
	};

	struct QueryRaceMasterRequest
	{
		static const USHORT	wCmd = G_R_QUERY_RACEMASTER_REQUEST;
	};

	struct RaceMaster
	{
		UINT                  	race;                               	//
		UINT                  	playerNameLen;                      	//
		CHAR                  	playerName[MAX_PLAYER_NAME_LEN];      	//
		UINT                  	guildId;                            	//
	};

	struct QueryRaceMasterResult
	{
		static const USHORT	wCmd = R_G_QUERY_RACEMASTER_RESULT;

		UINT                  	len;                                	//
		UnionRaceMasterInfo   	masters[5];                           	//
	};

	struct TeamLogNotify
	{
		static const USHORT	wCmd = R_G_TEAMLOG;

		UINT                  	uiid;                               	//
		USHORT                	level;                              	//
		UINT                  	teamId;                             	//
		USHORT                	logType;                            	//
	};

	struct QueryGuildMembersRequest
	{
		static const USHORT	wCmd = G_R_QUERY_GUILD_MEMBERS_REQUEST;

		UINT                  	rank;                               	//
	};

	struct GuildMember
	{
		USHORT                	nameLen;                            	//
		CHAR                  	name[MAX_PLAYER_NAME_LEN];            	//
	};

	struct QueryGuildMembersResult
	{
		static const USHORT	wCmd = R_G_QUERY_GUILD_MEMBERS_RESULT;

		UINT                  	nameLen;                            	//
		CHAR                  	guildName[MAX_UNION_NAME_LEN];        	//
		UINT                  	rank;                               	//
		UINT                  	race;                               	//
		CHAR                  	bEnd;                               	//
		UINT                  	membersLen;                         	//
		GuildMember           	members[20];                          	//
	};

	struct AddFriendlyDegree
	{
		static const USHORT	wCmd = G_R_ADD_FRIENDLY_DEGREE;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	friendUiid;                         	//好友的uiid
		UINT                  	degreeAdded;                        	//增加的友好度
	};

	struct Tweet
	{
		USHORT                	type;                               	//动态类别
		UINT                  	createdTime;                        	//动态的创建时间
		UINT                  	tweetLen;                           	//动态的字符串长度
		CHAR                  	tweet[400];                           	//动态的字符串
	};

	struct AddTweet
	{
		static const USHORT	wCmd = G_R_ADD_TWEET;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		Tweet                 	tweet;                              	//
	};

	struct ShowFriendTweet
	{
		static const USHORT	wCmd = R_G_SHOW_TWEET;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	nameLen;                            	//
		CHAR                  	friendName[MAX_PLAYER_NAME_LEN];      	//
		Tweet                 	tweet;                              	//
	};

	struct QueryFriendTweetsRequest
	{
		static const USHORT	wCmd = G_R_QUERY_TWEETS_REQUEST;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	targetUiid;                         	//被查询的目标
	};

	struct QueryFriendTweetsResult
	{
		static const USHORT	wCmd = R_G_QUERY_TWEETS_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	nameLen;                            	//
		CHAR                  	friendName[MAX_PLAYER_NAME_LEN];      	//
		Tweet                 	tweet;                              	//
	};

	struct QueryFriendsListRequest
	{
		static const USHORT	wCmd = G_R_QUERY_FRIENDS_LIST_REQUEST;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	targetUiid;                         	//被查询的目标
	};

	struct QueryFriendsListResult
	{
		static const USHORT	wCmd = R_G_QUERY_FRIENDS_LIST_RESULT;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	targetUiid;                         	//
		UINT                  	lstLen;                             	//
		GRInfoUpdate          	friendsLst[5];                        	//
	};

	struct NotifyExpAdded
	{
		static const USHORT	wCmd = G_R_NOTIY_EXP_ADDED;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		CHAR                  	isBoss;                             	//
		UINT                  	exp;                                	//当isBoss为false时，exp为杀死一个怪时获得的经验，否则，exp为0
	};

	struct NotifyAddExp
	{
		static const USHORT	wCmd = R_G_NOTIFY_ADD_EXP;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	exp;                                	//
	};

	struct QueryExpNeededWhenLvlUpRequest
	{
		static const USHORT	wCmd = R_G_QUERY_EXP_NEEDED_LVL_UP_REQUEST;
	};

	struct QueryExpNeededWhenLvlUpResult
	{
		static const USHORT	wCmd = G_R_QUERY_EXP_NEEDED_LVL_UP_RESULT;

		UINT                  	career;                             	//职业
		USHORT                	lvlFirst;                           	//起始等级，下面的lvlLast表示结束等级，如1-90级
		USHORT                	lvlLast;                            	//
		UINT                  	lstLen;                             	//
		UINT                  	lst[90];                              	//
	};

	struct NotifyTeamGain
	{
		static const USHORT	wCmd = R_G_NOTIFY_TEAM_GAIN;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	percent;                            	//单位:%
	};

	struct ModifyTweetReceiveFlag
	{
		static const USHORT	wCmd = G_R_MODIFY_TWEET_RECEIVE_FLAG;

		UINT                  	gateId;                             	//
		UINT                  	playerId;                           	//
		UINT                  	flag;                               	//
	};

	struct FriendDegreeGainEmail
	{
		static const USHORT	wCmd = R_G_FRIEND_DEGREE_GAIN_EMAIL;

		UINT                  	nameLen;                            	//收件人名字长度
		CHAR                  	name[MAX_PLAYER_NAME_LEN];            	//收件人名字
		UINT                  	itemId;                             	//物品ID
		UINT                  	titleLen;                           	//
		CHAR                  	title[100];                           	//
		UINT                  	contentLen;                         	//
		CHAR                  	content[200];                         	//
	};

	struct GRQueryNickUnion
	{
		static const USHORT	wCmd = G_R_QUERY_NICKUNION;

		RPlayerID             	queryPlayerID;                      	//
		UINT                  	nickArrLen;                         	//查询数组长度
		NameInfo              	nickArr[NICKUNION_QRY_LEN];           	//
	};

	struct RGQueryNickUnionRst
	{
		static const USHORT	wCmd = R_G_QUERY_NICKUNION_RST;

		RPlayerID             	queryPlayerID;                      	//
		UINT                  	nickunionArrLen;                    	//结果数组长度
		NameUnionInfo         	nickunionArr[NICKUNION_QRY_LEN];      	//
	};


	//Messages
	typedef enum Message_id_type
	{
		CreateTeamMessage                   	=	G_R_CreateTeam,                       	//CreateTeamRequest
		CreateTeamResultMessage             	=	R_G_CreateTeamResult,                 	//CreateTeamResult
		AddTeamMemberMessage                	=	G_R_AddTeamMember,                    	//AddTeamMemberRequest
		AddTeamMemberResultMessage          	=	R_G_AddTeamMemberResult,              	//AddTeamMemberResult
		RemoveTeamMemberMessage             	=	G_R_RemoveTeamMember,                 	//RemoveTeamMemberRequest
		RemoveTeamMemberResultMessage       	=	R_G_RemoveTeamMemberResult,           	//RemoveTeamMemberResult
		DestroyTeamMessage                  	=	G_R_DestroyTeam,                      	//DestroyTeamRequest
		DestroyTeamResultMessage            	=	R_G_DestroyTeamResult,                	//DestroyTeamResult
		QueryTeamMessage                    	=	G_R_QueryTeam,                        	//QueryTeamRequest
		QueryTeamResultMessage              	=	R_G_QueryTeamResult,                  	//QueryTeamResult
		AddTeamAttriMessage                 	=	G_R_AddTeamAttri,                     	//AddTeamAttriRequest
		AddTeamAttriResultMessage           	=	R_G_AddTeamAttriResult,               	//AddTeamAttriResult
		RemoveTeamAttriMessage              	=	G_R_RemoveTeamAttri,                  	//RemoveTeamAttriRequest
		RemoveTeamAttriResultMessage        	=	R_G_RemoveTeamAttriResult,            	//RemoveTeamAttriResult
		AddTeamMemberAttriMessage           	=	G_R_AddTeamMemberAttri,               	//AddTeamMemberAttriRequest
		AddTeamMemberAttriResultMessage     	=	R_G_AddTeamMemberAttriResult,         	//AddTeamMemberAttriResult
		RemoveTeamMemberAttriMessage        	=	G_R_RemoveTeamMemberAttri,            	//RemoveTeamMemberAttriRequest
		RemoveTeamMemberAttriResultMessage  	=	R_G_RemoveTeamMemberAttriResult,      	//RemoveTeamMemberAttriResult
		ReportGateSvrIdMessage              	=	G_R_ReportGateId,                     	//ReportGateSvrIdRequest
		ReportGateSvrIdResultMessage        	=	R_G_ReportGateIdResult,               	//ReportGateSvrIdResult
		TeamDestroyedNotifyMessage          	=	R_G_TeamDestroyedNotify,              	//TeamDestroyedNotify
		AddTeamMemberNotifyMessage          	=	R_G_AddTeamMemberNotify,              	//AddTeamMemberNotify
		RemoveTeamMemberNotifyMessage       	=	R_G_RemoveTeamMemberNotify,           	//RemoveTeamMemberNotify
		ModifyTeamCaptainMessage            	=	G_R_ModifyTeamCaptain,                	//ModifyTeamCaptainRequest
		ModifyTeamCaptainResultMessage      	=	R_G_ModifyTeamCaptainResult,          	//ModifyTeamCaptainResult
		ModifyTeamCaptainNotifyMessage      	=	R_G_TeamCaptainModifiedNotify,        	//ModifyTeamCaptainNotify
		QueryTeamMemberMessage              	=	G_R_QueryTeamMember,                  	//QueryTeamMemberRequest
		QueryTeamMemberResultMessage        	=	R_G_QueryTeamMemberResult,            	//QueryTeamMemberResult
		QueryTeamMembersCountMessage        	=	G_R_QueryTeamMembersCount,            	//QueryTeamMembersCountRequest
		QueryTeamMembersCountResultMessage  	=	R_G_QueryTeamMembersCountResult,      	//QueryTeamMembersCountResult
		UpdateTeamMemberNotifyMessage       	=	R_G_UpdateTeamMemberNotify,           	//UpdateTeamMemberNotify
		TeamCreatedNotifyMessage            	=	R_G_TeamCreatedNotify,                	//TeamCreateedNotify
		ModifyExpSharedModeMessage          	=	G_R_ModifyExpSharedMode,              	//ModifyExpSharedModeRequest
		ModifyExpSharedModeResultMessage    	=	R_G_ModifyExpSharedModeResult,        	//ModifyExpSharedModeResult
		ModifyGoodSharedModeMessage         	=	G_R_ModifyGoodSharedMode,             	//ModifyGoodSharedModeRequest
		ModifyGoodSharedModeResultMessage   	=	R_G_ModifyGoodSharedModeResult,       	//ModifyGoodSharedModeResult
		ModifyExpSharedModeNotifyMessage    	=	R_G_ModifyExpSharedModeNotify,        	//ModifyExpSharedModeNotify
		ModifyGoodSharedModeNotifyMessage   	=	R_G_ModifyGoodSharedModeNotify,       	//ModifyGoodSharedModeNotify
		TeamBroadcastMessage                	=	G_R_TeamBroadcast,                    	//TeamBroadcastRequest
		TeamBroadcastResultMessage          	=	R_G_TeamBroadcastResult,              	//TeamBroadcastResult
		TeamBroadcastNotifyMessage          	=	R_G_TeamBroadcastNotify,              	//TeamBroadcastNotify
		ModifyTeamMemberMapIdMessage        	=	G_R_ModifyTeamMemberMapId,            	//ModifyTeamMemberMapIdRequest
		ModifyTeamMemberMapIdResultMessage  	=	R_G_ModifyTeamMemberMapIdResult,      	//ModifyTeamMemberMapIdResult
		ModifyTeamMemberMapIdNotifyMessage  	=	R_G_ModifyTeamMemberMapIdNotify,      	//ModifyTeamMemberMapIdNotify
		ModifyTeamRollItemLevelMessage      	=	G_R_ModifyTeamRollItemLevel,          	//ModifyTeamRollItemLevelRequest
		ModifyTeamRollItemLevelResultMessage	=	R_G_ModifyTeamRollItemLevelResult,    	//ModifyTeamRollItemLevelResult
		ModifyTeamRollItemLevelNotifyMessage	=	R_G_ModifyTeamRollItemLevelNotify,    	//ModifyTeamRollItemLevelNotify
		CreateChatGroupMessage              	=	G_R_CreateChatGroup,                  	//CreateChatGroupRequest
		CreateChatGroupResultMessage        	=	R_G_CreateChatGroupResult,            	//CreateChatGroupResult
		DestroyChatGroupMessage             	=	G_R_DestroyChatGroup,                 	//DestroyChatGroupRequest
		DestroyChatGroupResultMessage       	=	R_G_DestroyChatGroupResult,           	//DestroyChatGroupResult
		ChatGroupDestroyedNotifyMessage     	=	R_G_ChatGroupDestroyedNotify,         	//ChatGroupDestroyedNotify
		AddChatGroupMemberMessage           	=	G_R_AddChatGroupMember,               	//AddChatGroupMemberRequest
		AddChatGroupMemberResultMessage     	=	R_G_AddChatGroupMemberResult,         	//AddChatGroupMemberResult
		ChatGroupMemberAddedNotifyMessage   	=	R_G_ChatGroupMemberAddedNotify,       	//ChatGroupMemberAddedNotify
		RemoveChatGroupMemberMessage        	=	G_R_RemoveChatGroupMember,            	//RemoveChatGroupMemberRequest
		RemoveChatGroupMemberResultMessage  	=	R_G_RemoveChatGroupMemberResult,      	//RemoveChatGroupMemberResult
		ChatGroupMemberRemovedNotifyMessage 	=	R_G_ChatGroupMemberRemovedNotify,     	//ChatGroupMemberRemovedNotify
		ChatGroupSpeekMessage               	=	G_R_ChatGroupSpeek,                   	//ChatGroupSpeekRequest
		ChatGroupSpeekResultMessage         	=	R_G_ChatGroupSpeekResult,             	//ChatGroupSpeekResult
		ChatGroupSpeekNotifyMessage         	=	R_G_ChatGroupSpeekNotify,             	//ChatGroupSpeekNotify
		G_R_UplineMsg                       	=	G_R_UPLINE_MSG,                       	//GRUplineInfo
		G_R_UpdataMsg                       	=	G_R_UPDATA_MSG,                       	//GRInfoUpdate
		G_R_AddFriendMsg                    	=	G_R_ADDFRIEND_MSG,                    	//GRAddFriend
		G_R_DelFriendMsg                    	=	G_R_DELFRIEND_MSG,                    	//GRDelFriend
		G_R_Add_Group                       	=	G_R_ADD_GROUP,                        	//GRAddGroup
		G_R_Friend_Alter_Group              	=	G_R_FRIEND_ALTER_GROUP_ID,            	//GRFriendAlterGroupID
		G_R_Del_Group                       	=	G_R_DEL_GROUP,                        	//GRDelGroup
		G_R_AlterGroupName                  	=	G_R_ALTERGROUPNAME,                   	//GRAlterGroupName
		G_R_AddToBlackList                  	=	G_R_ADDTOBLACKLIST,                   	//GRAddToBlacklist
		G_R_DelFromBlackList                	=	G_R_DELFROMBLACKLIST,                 	//GRDelFromBlacklist
		G_R_SendGroupMsg                    	=	G_R_SENDGROUPMSG,                     	//GRSendGroupMsg
		G_R_SendMsg                         	=	G_R_SENDMSG,                          	//GRSendMsg
		G_R_AddEnemy                        	=	G_R_ADD_ENEMY,                        	//GRAddEnemy
		G_R_LockEnemy                       	=	G_R_LOCK_ENEMY,                       	//GRLockEnemy
		G_R_DelEnemy                        	=	G_R_DEL_ENEMY,                        	//GRDelEnemy
		G_R_DELPLAYER                       	=	G_R_DEL_PLAYER,                       	//G_R_DelPlayer
		R_G_GroupMsg                        	=	R_G_GROUP_MSG,                        	//RGFriendGroups
		R_G_FriendsMsg                      	=	R_G_FRIENDS_MSG,                      	//RGFriendInfos
		R_G_UpdataFriendMsg                 	=	R_G_UPDATAFRIEND_MSG,                 	//RGFriendInfoUpdate
		R_G_AddFriendMsg                    	=	R_G_ADDFRIEND_MSG,                    	//RGAddFriend
		R_G_DelFriendMsg                    	=	R_G_DELFRIEND_MSG,                    	//RGDelFriend
		R_G_Add_Group                       	=	R_G_ADD_GROUP,                        	//RGAddGroup
		R_G_Friend_Alter_Group              	=	R_G_FRIEND_ALTER_GROUP_ID,            	//RGFriendAlterGroupID
		R_G_Del_Group                       	=	R_G_DEL_GROUP,                        	//RGDelGroup
		R_G_AlterGroupName                  	=	R_G_ALTERGROUPNAME,                   	//RGAlterGroupName
		R_G_AddToBlackList                  	=	R_G_ADDTOBLACKLIST,                   	//RGAddToBlacklist
		R_G_DelFromBlackList                	=	R_G_DELFROMBLACKLIST,                 	//RGDelFromBlackList
		R_G_SendGroupMsg                    	=	R_G_SENDGROUPMSG,                     	//RGSendGroupMsg
		R_G_SendMsg                         	=	R_G_SENDMSG,                          	//RGSendMsg
		R_G_Blacklist                       	=	R_G_BLACKLIST,                        	//RGBlacklist
		R_G_AddEnemy                        	=	R_G_ADD_ENEMY,                        	//RGAddEnemy
		R_G_LockEnemy                       	=	R_G_LOCK_ENEMY,                       	//RGLockEnemy
		R_G_DelEnemy                        	=	R_G_DEL_ENEMY,                        	//RGDelEnemy
		R_G_Enemylist                       	=	R_G_ENEMY_LIST,                       	//RGEnemylist
		R_G_UpdateEnemy                     	=	R_G_UPDATE_ENEMY,                     	//RGUpdateEnemy
		R_G_NotifyRGroupMsgNum              	=	R_G_NOTIFY_R_GROUP_MSG_SUM,           	//RGNotifyRGroupMsgNum
		G_R_ReportPlayerUpline              	=	G_R_REPORT_PLAYER_UPLINE,             	//ReportPlayerUpline
		G_R_ReportPlayerOffline             	=	G_R_REPORT_PLAYER_OFFLINE,            	//ReportPlayerOffline
		G_R_CreateUnionRequest              	=	G_R_CREATE_UNION_REQUEST,             	//CreateUnionRequest
		R_G_CreateUnionResult               	=	R_G_CREATE_UNION_RESULT,              	//CreateUnionResult
		G_R_DestroyUnionRequest             	=	G_R_DESTROY_UNION_REQUEST,            	//DestroyUnionRequest
		R_G_DestroyUnionResult              	=	R_G_DESTROY_UNION_RESULT,             	//DestroyUnionResult
		G_R_QueryUnionBasicRequest          	=	G_R_QUERY_UNION_BASIC_REQUEST,        	//QueryUnionBasicRequest
		R_G_QueryUnionBasicResult           	=	R_G_QUERY_UNION_BASIC_RESULT,         	//QueryUnionBasicResult
		G_R_AddUnionMemberRequest           	=	G_R_UNION_ADD_MEMBER_REQUEST,         	//AddUnionMemberRequest
		R_G_AddUnionMemberResult            	=	R_G_UNION_ADD_MEMBER_RESULT,          	//AddUnionMemberResult
		G_R_AddUnionMemberConfirmed         	=	G_R_UNION_ADD_MEMBER_CONFIRMED,       	//AddUnionMemberConfirmed
		R_G_AddUnionMemberConfirm           	=	R_G_UNION_ADD_MEMBER_CONFIRM,         	//AddUnionMemberConfirm
		G_R_RemoveUnionMemberRequest        	=	G_R_UNION_REMOVE_MEMBER_REQUEST,      	//RemoveUnionMemberRequest
		R_G_RemoveUnionMemberResult         	=	R_G_UNION_REMOVE_MEMBER_RESULT,       	//RemoveUnionMemberResult
		G_R_QueryUnionMemberListRequest     	=	G_R_QUERY_UNION_MEMBER_LIST_REQUEST,  	//QueryUnionMembersRequest
		R_G_QueryUnionMemberListResult      	=	R_G_QUERY_UNION_MEMBER_LIST_RESULT,   	//QueryUnionMembersResult
		R_G_UnionDestroyedNotify            	=	R_G_UNION_DESTROYED_NOTIFY,           	//UnionDestroyedNotify
		R_G_UnionMemberAddedNotify          	=	R_G_UNION_MEMBER_ADDED_NOTIFY,        	//AddUnionMemberNotify
		R_G_UnionMemberRemovedNotify        	=	R_G_UNION_MEMBER_REMOVED_NOTIFY,      	//RemoveUnionMemberNotify
		R_G_UnionMemberTitleNotify          	=	R_G_UNION_MEMBER_TITLE_NOTIFY,        	//UnionMemberTitleNotify
		R_G_ModifyUnonMemberTitleResult     	=	R_G_UNION_MODIFY_MEMBER_TITLE_RESULT, 	//ModifyUnionMemberTitleResult
		G_R_ModifyUnionMemberTitleRequest   	=	G_R_UNION_MODIFY_MEMBER_TITLE_REQUEST,	//ModifyUnionMemberTitleRequest
		R_G_TransformUnonCaptionNotify      	=	R_G_UNION_TRANSFORM_CAPTION_NOTIFY,   	//TransformUnionCaptionNotify
		R_G_TransformUnonCaptionResult      	=	R_G_UNION_TRANSFORM_CAPTION_RESULT,   	//TransformUnionCaptionResult
		G_R_TransformUnonCaptionRequest     	=	G_R_UNION_TRANSFROM_CAPTION_REQUEST,  	//TransformUnionCaptionRequest
		G_R_AdvanceUnionMemberPosRequest    	=	G_R_UNION_ADVANCE_MEMBER_POS_REQUEST, 	//AdvanceUnionMemberPosRequest
		R_G_AdvanceUnionMemberPosResult     	=	R_G_UNION_ADVANCE_MEMBER_POS_RESULT,  	//AdvanceUnionMemberPosResult
		R_G_AdvanceUnionMemberPosNotify     	=	R_G_UNION_ADVANCE_MEMBER_POS_NOTIFY,  	//AdvanceUnionMemberPosNotify
		G_R_ReduceUnionMemberPosRequest     	=	G_R_UNION_REDUCE_MEMBER_POS_REQUEST,  	//ReduceUnionMemberPosRequest
		R_G_ReduceUnionMemberPosResult      	=	R_G_UNION_REDUCE_MEMBER_POS_RESULT,   	//ReduceUnionMemberPosResult
		R_G_ReduceUnionMemberPosNotify      	=	R_G_UNION_REDUCE_MEMBER_POS_NOTIFY,   	//ReduceUnionMemberPosNotify
		G_R_UnionChannelSpeekRequest        	=	G_R_UNION_CHANNEL_SPEEK_REQUEST,      	//UnionChannelSpeekRequest
		R_G_UnionChannelSpeekResult         	=	R_G_UNION_CHANNEL_SPEEK_RESULT,       	//UnionChannelSpeekResult
		R_G_UnionChannelSpeekNotify         	=	R_G_UNION_CHANNEL_SPEEK_NOTIFY,       	//UnionChannelSpeekNotify
		G_R_UpdateUnionPosCfgRequest        	=	G_R_UNION_UPDATE_POS_CFG_REQUEST,     	//UpdateUnionPosCfgRequest
		R_G_UpdateUnionPosCfgResult         	=	R_G_UNION_UPDATE_POS_CFG_RESULT,      	//UpdateUnionPosCfgResult
		R_G_UpdateUnionPosCfgNotify         	=	R_G_UNION_UPDATE_POS_CFG_NOTIFY,      	//UpdateUnionPosCfgNotify
		G_R_UnionForbidSpeekRequest         	=	G_R_UNION_FORBID_SPEEK_REQUEST,       	//ForbidUnionSpeekRequest
		R_G_UnionForbidSpeekResult          	=	R_G_UNION_FORBID_SPEEK_RESULT,        	//ForbidUnionSpeekResult
		R_G_UnionForbidSpeekNotify          	=	R_G_UNION_FORBID_SPEEK_NOTIFY,        	//ForbidUnionSpeekNotify
		R_G_UnionSkillsListNotify           	=	R_G_UNION_SKILLS_LIST_NOTIFY,         	//UnionSkillsListNotify
		G_R_StudyUnionSkillRequest          	=	G_R_STUDY_UNION_SKILL_REQUEST,        	//StudyUnionSkillRequest
		R_G_StudyUnionSkillResult           	=	R_G_STUDY_UNION_SKILL_RESULT,         	//StudyUnionSkillResult
		G_R_ModifyUnionActivePointRequest   	=	G_R_MODIFY_UNION_ACTIVE_POINT_REQUEST,	//ModifyUnionActivePointRequest
		R_G_ModifyUnionActivePointResult    	=	R_G_MODIFY_UNION_ACTIVE_POINT_RESULT, 	//ModifyUnionActivePointResult
		R_G_UnionActivePointNotify          	=	R_G_UNION_ACTIVE_POINT_NOTIFY,        	//UnionActivePointNotify
		G_R_PostUnionTaskListRequest        	=	G_R_POST_UNION_TASK_LIST_REQUEST,     	//PostUnionTasksListRequest
		R_G_PostUnionTaskListResult         	=	R_G_POST_UNION_TASK_LIST_RESULT,      	//PostUnionTasksListResult
		R_G_UnionTaskListNotify             	=	R_G_UNION_TASK_LIST_NOTIFY,           	//UnionTasksListNotify
		G_R_AdvanceUnionLevelRequest        	=	G_R_ADVANCE_UNION_LEVEL_REQUEST,      	//AdvanceUnionLevelRequest
		R_G_AdvanceUnionLevelResult         	=	R_G_ADVANCE_UNION_LEVEL_RESULT,       	//AdvanceUnionLevelResult
		R_G_UnionLevelNotify                	=	R_G_UNION_LEVEL_NOTIFY,               	//UnionLevelNotify
		G_R_PostUnionBulletinRequest        	=	G_R_POST_UNION_BULLETIN_REQUEST,      	//PostUnionBulletinRequest
		R_G_PostUnionBulletinResult         	=	R_G_POST_UNION_BULLETIN_RESULT,       	//PostUnionBulletinResult
		R_G_UnionBulletinNotify             	=	R_G_UNION_BULLETIN_NOTIFY,            	//UnionBulletinNotify
		R_G_UnionMemberUplineNotify         	=	R_G_UNION_MEMBER_UPLINE_NOTIFY,       	//UnionMemberUplineNotify
		R_G_UnionMemberOfflineNotify        	=	R_G_UNION_MEMBER_OFFLINE_NOTIFY,      	//UnionMemberOfflineNotify
		G_R_QueryUnionLogRequest            	=	G_R_QUERY_UNION_LOG_REQUEST,          	//QueryUnionLogRequest
		R_G_QueryUnionLogResult             	=	R_G_QUERY_UNION_LOG_RESULT,           	//QueryUnionLogResult
		R_G_UnionEmailNotify                	=	R_G_UNION_EMAIL_NOTIFY,               	//UnionEmailNotify
		G_R_UnionOwnerCheckRequest          	=	G_R_UNION_OWNER_CHECK_REQUEST,        	//UnionOwnerCheckRequest
		G_R_UnionOwnerCheckResult           	=	R_G_UNION_OWNER_CHECK_RESULT,         	//UnionOwnerCheckResult
		G_R_UnionModifyPrestigeRequest      	=	G_R_UNION_MODIFY_PRESTIGE_REQUEST,    	//ModifyUnionPrestigeRequest
		R_G_UnionModifyPrestigeResult       	=	R_G_UNION_MODIFY_PRESTIGE_RESULT,     	//ModifyUnionPrestigeResult
		R_G_UnionPrestigeNotify             	=	R_G_UNION_PRESTIGE_NOTIFY,            	//UnionPrestigeNotify
		G_R_UnionModifyBadgeRequest         	=	G_R_UNION_MODIFY_BADGE_REQUEST,       	//ModifyUnionBadgeRequest
		R_G_UnionModifyBadgeResult          	=	R_G_UNION_MODIFY_BADGE_RESULT,        	//ModifyUnionBadgeResult
		R_G_UnionBadgeNotify                	=	R_G_UNION_BADGE_NOTIFY,               	//UnionBadgeNotify
		G_R_UnionModifyMultiRequest         	=	G_R_UNION_MODIFY_MULTI_REQUEST,       	//ModifyUnionMultiRequest
		R_G_UnionMultiNotify                	=	R_G_UNION_MULTI_NOTIFY,               	//UnionMultiNotify
		G_R_QueryPlayerRelationRequest      	=	G_R_QUERY_PLAYER_RELATION_REQUEST,    	//QueryPlayerRelationRequest
		R_G_QueryPlayerRelationResult       	=	R_G_QUERY_PLAYER_RELATION_RESULT,     	//QueryPlayerRelationResult
		G_R_QueryPlayerInfoRequest          	=	G_R_QUERY_PLAYER_INFO_REQUEST,        	//QueryPlayerInfoRequest
		R_G_QueryPlayerInfoResult           	=	R_G_QUERY_PLAYER_INFO_RESULT,         	//QueryPlayerInfoResult
		G_R_QueryUnionRankRequest           	=	G_R_QUERY_UNION_RANK_REQUEST,         	//QueryUnionRankRequest
		R_G_QueryUnionRankResult            	=	R_G_QUERY_UNION_RANK_RESULT,          	//QueryUnionRankResult
		G_R_SearchUnionRankRequest          	=	G_R_SEARCH_UNION_RANK_REQUEST,        	//SearchUnionRankRequest
		R_G_SearchUnionRankResult           	=	R_G_SEARCH_UNION_RANK_RESULT,         	//SearchUnionRankResult
		R_G_PlayerUplineTeamIdNotify        	=	R_G_PLAYER_UPLINE_TEAMID_NOTIFY,      	//PlayerUplineTeamIdNotify
		G_R_QueryRaceMasterRequest          	=	G_R_QUERY_RACEMASTER_REQUEST,         	//QueryRaceMasterRequest
		R_G_QueryRaceMasterResult           	=	R_G_QUERY_RACEMASTER_RESULT,          	//QueryRaceMasterResult
		R_G_TeamLogNotify                   	=	R_G_TEAMLOG,                          	//TeamLogNotify
		G_R_QueryGuildMembersRequest        	=	G_R_QUERY_GUILD_MEMBERS_REQUEST,      	//QueryGuildMembersRequest
		R_G_QueryGuildMembersResult         	=	R_G_QUERY_GUILD_MEMBERS_RESULT,       	//QueryGuildMembersResult
		G_R_AddFriendlyDegree               	=	G_R_ADD_FRIENDLY_DEGREE,              	//AddFriendlyDegree
		G_R_AddTweet                        	=	G_R_ADD_TWEET,                        	//AddTweet
		R_G_ShowTweet                       	=	R_G_SHOW_TWEET,                       	//ShowFriendTweet
		G_R_QueryTweetsRequest              	=	G_R_QUERY_TWEETS_REQUEST,             	//QueryFriendTweetsRequest
		R_G_QueryTwttesResult               	=	R_G_QUERY_TWEETS_RESULT,              	//QueryFriendTweetsResult
		G_R_QueryFriendsListRequest         	=	G_R_QUERY_FRIENDS_LIST_REQUEST,       	//QueryFriendsListRequest
		R_G_QueryFriendsListResult          	=	R_G_QUERY_FRIENDS_LIST_RESULT,        	//QueryFriendsListResult
		G_R_NotifyExpAdded                  	=	G_R_NOTIY_EXP_ADDED,                  	//NotifyExpAdded
		R_G_NotifyAddExp                    	=	R_G_NOTIFY_ADD_EXP,                   	//NotifyAddExp
		R_G_QueryExpNeededLvlUpRequest      	=	R_G_QUERY_EXP_NEEDED_LVL_UP_REQUEST,  	//QueryExpNeededWhenLvlUpRequest
		G_R_QueryExpNeededLvlUpResult       	=	G_R_QUERY_EXP_NEEDED_LVL_UP_RESULT,   	//QueryExpNeededWhenLvlUpResult
		R_G_NotifyTeamGain                  	=	R_G_NOTIFY_TEAM_GAIN,                 	//NotifyTeamGain
		G_R_ModifyTweetReceiveFlag          	=	G_R_MODIFY_TWEET_RECEIVE_FLAG,        	//ModifyTweetReceiveFlag
		R_G_FriendDegreeGainEmail           	=	R_G_FRIEND_DEGREE_GAIN_EMAIL,         	//FriendDegreeGainEmail
		G_R_QueryNickUnion                  	=	G_R_QUERY_NICKUNION,                  	//GRQueryNickUnion
		R_G_QueryNickUnionRst               	=	R_G_QUERY_NICKUNION_RST,              	//RGQueryNickUnionRst
	};

	//Class Data
	class RelationServiceProtocol;
	typedef size_t (RelationServiceProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (RelationServiceProtocol::*DeCodeFunc)(void* pData);

	class 
	#ifdef WIN32
	//_declspec(dllexport) 
	#endif
	RelationServiceProtocol
	{
	public:
		RelationServiceProtocol();
		~RelationServiceProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__G_R_DelPlayer(void* pData);
		size_t	DeCode__G_R_DelPlayer(void* pData);

		size_t	EnCode__RPlayerID(void* pData);
		size_t	DeCode__RPlayerID(void* pData);

		size_t	EnCode__FriendGroupInfo(void* pData);
		size_t	DeCode__FriendGroupInfo(void* pData);

		size_t	EnCode__NameInfo(void* pData);
		size_t	DeCode__NameInfo(void* pData);

		size_t	EnCode__NameUnionInfo(void* pData);
		size_t	DeCode__NameUnionInfo(void* pData);

		size_t	EnCode__FriendInfo(void* pData);
		size_t	DeCode__FriendInfo(void* pData);

		size_t	EnCode__GRUplineInfo(void* pData);
		size_t	DeCode__GRUplineInfo(void* pData);

		size_t	EnCode__RGFriendGroups(void* pData);
		size_t	DeCode__RGFriendGroups(void* pData);

		size_t	EnCode__RGFriendInfos(void* pData);
		size_t	DeCode__RGFriendInfos(void* pData);

		size_t	EnCode__GRInfoUpdate(void* pData);
		size_t	DeCode__GRInfoUpdate(void* pData);

		size_t	EnCode__RGFriendInfoUpdate(void* pData);
		size_t	DeCode__RGFriendInfoUpdate(void* pData);

		size_t	EnCode__RGNotifyRGroupMsgNum(void* pData);
		size_t	DeCode__RGNotifyRGroupMsgNum(void* pData);

		size_t	EnCode__BlacklistItem(void* pData);
		size_t	DeCode__BlacklistItem(void* pData);

		size_t	EnCode__RGBlacklist(void* pData);
		size_t	DeCode__RGBlacklist(void* pData);

		size_t	EnCode__AddFriendError(void* pData);
		size_t	DeCode__AddFriendError(void* pData);

		size_t	EnCode__AddFriendSuc(void* pData);
		size_t	DeCode__AddFriendSuc(void* pData);

		size_t	EnCode__AddFriend(void* pData, int nSelectId);
		size_t	DeCode__AddFriend(void* pData, int nSelectId);

		size_t	EnCode__RGAddFriend(void* pData);
		size_t	DeCode__RGAddFriend(void* pData);

		size_t	EnCode__GRAddFriend(void* pData);
		size_t	DeCode__GRAddFriend(void* pData);

		size_t	EnCode__GRDelFriend(void* pData);
		size_t	DeCode__GRDelFriend(void* pData);

		size_t	EnCode__DelFriendSuc(void* pData);
		size_t	DeCode__DelFriendSuc(void* pData);

		size_t	EnCode__DelFriend(void* pData, int nSelectId);
		size_t	DeCode__DelFriend(void* pData, int nSelectId);

		size_t	EnCode__RGDelFriend(void* pData);
		size_t	DeCode__RGDelFriend(void* pData);

		size_t	EnCode__GRAddGroup(void* pData);
		size_t	DeCode__GRAddGroup(void* pData);

		size_t	EnCode__AddGroupErr(void* pData);
		size_t	DeCode__AddGroupErr(void* pData);

		size_t	EnCode__AddGroupSuc(void* pData);
		size_t	DeCode__AddGroupSuc(void* pData);

		size_t	EnCode__AddGroupResult(void* pData, int nSelectId);
		size_t	DeCode__AddGroupResult(void* pData, int nSelectId);

		size_t	EnCode__RGAddGroup(void* pData);
		size_t	DeCode__RGAddGroup(void* pData);

		size_t	EnCode__GRFriendAlterGroupID(void* pData);
		size_t	DeCode__GRFriendAlterGroupID(void* pData);

		size_t	EnCode__AlterGroupIDErr(void* pData);
		size_t	DeCode__AlterGroupIDErr(void* pData);

		size_t	EnCode__AlterGroupIDSuc(void* pData);
		size_t	DeCode__AlterGroupIDSuc(void* pData);

		size_t	EnCode__AlterGroupResult(void* pData, int nSelectId);
		size_t	DeCode__AlterGroupResult(void* pData, int nSelectId);

		size_t	EnCode__RGFriendAlterGroupID(void* pData);
		size_t	DeCode__RGFriendAlterGroupID(void* pData);

		size_t	EnCode__GRDelGroup(void* pData);
		size_t	DeCode__GRDelGroup(void* pData);

		size_t	EnCode__DelGroupErr(void* pData);
		size_t	DeCode__DelGroupErr(void* pData);

		size_t	EnCode__DelGroupSuc(void* pData);
		size_t	DeCode__DelGroupSuc(void* pData);

		size_t	EnCode__DelGroupResult(void* pData, int nSelectId);
		size_t	DeCode__DelGroupResult(void* pData, int nSelectId);

		size_t	EnCode__RGDelGroup(void* pData);
		size_t	DeCode__RGDelGroup(void* pData);

		size_t	EnCode__GRAlterGroupName(void* pData);
		size_t	DeCode__GRAlterGroupName(void* pData);

		size_t	EnCode__AlterGroupNameErr(void* pData);
		size_t	DeCode__AlterGroupNameErr(void* pData);

		size_t	EnCode__AlterGroupNameSuc(void* pData);
		size_t	DeCode__AlterGroupNameSuc(void* pData);

		size_t	EnCode__AlterGroupNameResult(void* pData, int nSelectId);
		size_t	DeCode__AlterGroupNameResult(void* pData, int nSelectId);

		size_t	EnCode__RGAlterGroupName(void* pData);
		size_t	DeCode__RGAlterGroupName(void* pData);

		size_t	EnCode__GRAddToBlacklist(void* pData);
		size_t	DeCode__GRAddToBlacklist(void* pData);

		size_t	EnCode__AddToBlacklistErr(void* pData);
		size_t	DeCode__AddToBlacklistErr(void* pData);

		size_t	EnCode__AddToBlacklistSuc(void* pData);
		size_t	DeCode__AddToBlacklistSuc(void* pData);

		size_t	EnCode__AddToBlacklistResult(void* pData, int nSelectId);
		size_t	DeCode__AddToBlacklistResult(void* pData, int nSelectId);

		size_t	EnCode__RGAddToBlacklist(void* pData);
		size_t	DeCode__RGAddToBlacklist(void* pData);

		size_t	EnCode__GRDelFromBlacklist(void* pData);
		size_t	DeCode__GRDelFromBlacklist(void* pData);

		size_t	EnCode__DelFromBlacklistErr(void* pData);
		size_t	DeCode__DelFromBlacklistErr(void* pData);

		size_t	EnCode__DelFromBlacklistSuc(void* pData);
		size_t	DeCode__DelFromBlacklistSuc(void* pData);

		size_t	EnCode__DelFromBlacklistResult(void* pData, int nSelectId);
		size_t	DeCode__DelFromBlacklistResult(void* pData, int nSelectId);

		size_t	EnCode__RGDelFromBlackList(void* pData);
		size_t	DeCode__RGDelFromBlackList(void* pData);

		size_t	EnCode__GRAddEnemy(void* pData);
		size_t	DeCode__GRAddEnemy(void* pData);

		size_t	EnCode__AddEnemyErr(void* pData);
		size_t	DeCode__AddEnemyErr(void* pData);

		size_t	EnCode__AddEnemySuc(void* pData);
		size_t	DeCode__AddEnemySuc(void* pData);

		size_t	EnCode__AddEnemyResult(void* pData, int nSelectId);
		size_t	DeCode__AddEnemyResult(void* pData, int nSelectId);

		size_t	EnCode__RGAddEnemy(void* pData);
		size_t	DeCode__RGAddEnemy(void* pData);

		size_t	EnCode__GRLockEnemy(void* pData);
		size_t	DeCode__GRLockEnemy(void* pData);

		size_t	EnCode__LockEnemyErr(void* pData);
		size_t	DeCode__LockEnemyErr(void* pData);

		size_t	EnCode__LockEnemySuc(void* pData);
		size_t	DeCode__LockEnemySuc(void* pData);

		size_t	EnCode__LockEnemyResult(void* pData, int nSelectId);
		size_t	DeCode__LockEnemyResult(void* pData, int nSelectId);

		size_t	EnCode__RGLockEnemy(void* pData);
		size_t	DeCode__RGLockEnemy(void* pData);

		size_t	EnCode__GRDelEnemy(void* pData);
		size_t	DeCode__GRDelEnemy(void* pData);

		size_t	EnCode__DelEnemyErr(void* pData);
		size_t	DeCode__DelEnemyErr(void* pData);

		size_t	EnCode__DelEnemySuc(void* pData);
		size_t	DeCode__DelEnemySuc(void* pData);

		size_t	EnCode__DelEnemyResult(void* pData, int nSelectId);
		size_t	DeCode__DelEnemyResult(void* pData, int nSelectId);

		size_t	EnCode__RGDelEnemy(void* pData);
		size_t	DeCode__RGDelEnemy(void* pData);

		size_t	EnCode__EnemylistItem(void* pData);
		size_t	DeCode__EnemylistItem(void* pData);

		size_t	EnCode__RGEnemylist(void* pData);
		size_t	DeCode__RGEnemylist(void* pData);

		size_t	EnCode__RGUpdateEnemy(void* pData);
		size_t	DeCode__RGUpdateEnemy(void* pData);

		size_t	EnCode__GRSendGroupMsg(void* pData);
		size_t	DeCode__GRSendGroupMsg(void* pData);

		size_t	EnCode__SendGroupMsgErr(void* pData);
		size_t	DeCode__SendGroupMsgErr(void* pData);

		size_t	EnCode__GeneralMsg(void* pData);
		size_t	DeCode__GeneralMsg(void* pData);

		size_t	EnCode__SendGroupMsgResult(void* pData, int nSelectId);
		size_t	DeCode__SendGroupMsgResult(void* pData, int nSelectId);

		size_t	EnCode__RGSendGroupMsg(void* pData);
		size_t	DeCode__RGSendGroupMsg(void* pData);

		size_t	EnCode__GRSendMsg(void* pData);
		size_t	DeCode__GRSendMsg(void* pData);

		size_t	EnCode__SendMsgErr(void* pData);
		size_t	DeCode__SendMsgErr(void* pData);

		size_t	EnCode__SendMsgResult(void* pData);
		size_t	DeCode__SendMsgResult(void* pData);

		size_t	EnCode__RGSendMsg(void* pData);
		size_t	DeCode__RGSendMsg(void* pData);

		size_t	EnCode__TeamAttriKey(void* pData);
		size_t	DeCode__TeamAttriKey(void* pData);

		size_t	EnCode__TeamAttriValue(void* pData);
		size_t	DeCode__TeamAttriValue(void* pData);

		size_t	EnCode__TeamAttri(void* pData);
		size_t	DeCode__TeamAttri(void* pData);

		size_t	EnCode__AttriArray(void* pData);
		size_t	DeCode__AttriArray(void* pData);

		size_t	EnCode__TeamInd(void* pData);
		size_t	DeCode__TeamInd(void* pData);

		size_t	EnCode__TeamName(void* pData);
		size_t	DeCode__TeamName(void* pData);

		size_t	EnCode__MemberName(void* pData);
		size_t	DeCode__MemberName(void* pData);

		size_t	EnCode__MemberInd(void* pData);
		size_t	DeCode__MemberInd(void* pData);

		size_t	EnCode__MembersInd(void* pData);
		size_t	DeCode__MembersInd(void* pData);

		size_t	EnCode__Member(void* pData);
		size_t	DeCode__Member(void* pData);

		size_t	EnCode__Members(void* pData);
		size_t	DeCode__Members(void* pData);

		size_t	EnCode__Team(void* pData);
		size_t	DeCode__Team(void* pData);

		size_t	EnCode__CreateTeamRequest(void* pData);
		size_t	DeCode__CreateTeamRequest(void* pData);

		size_t	EnCode__CreateTeamResult(void* pData);
		size_t	DeCode__CreateTeamResult(void* pData);

		size_t	EnCode__DestroyTeamRequest(void* pData);
		size_t	DeCode__DestroyTeamRequest(void* pData);

		size_t	EnCode__DestroyTeamResult(void* pData);
		size_t	DeCode__DestroyTeamResult(void* pData);

		size_t	EnCode__AddTeamMemberRequest(void* pData);
		size_t	DeCode__AddTeamMemberRequest(void* pData);

		size_t	EnCode__AddTeamMemberResult(void* pData);
		size_t	DeCode__AddTeamMemberResult(void* pData);

		size_t	EnCode__RemoveTeamMemberRequest(void* pData);
		size_t	DeCode__RemoveTeamMemberRequest(void* pData);

		size_t	EnCode__RemoveTeamMemberResult(void* pData);
		size_t	DeCode__RemoveTeamMemberResult(void* pData);

		size_t	EnCode__QueryTeamRequest(void* pData);
		size_t	DeCode__QueryTeamRequest(void* pData);

		size_t	EnCode__QueryTeamResult(void* pData);
		size_t	DeCode__QueryTeamResult(void* pData);

		size_t	EnCode__QueryTeamMemberRequest(void* pData);
		size_t	DeCode__QueryTeamMemberRequest(void* pData);

		size_t	EnCode__QueryTeamMemberResult(void* pData);
		size_t	DeCode__QueryTeamMemberResult(void* pData);

		size_t	EnCode__UpdateMemberRequest(void* pData);
		size_t	DeCode__UpdateMemberRequest(void* pData);

		size_t	EnCode__UpdateMemberResult(void* pData);
		size_t	DeCode__UpdateMemberResult(void* pData);

		size_t	EnCode__AddTeamAttriRequest(void* pData);
		size_t	DeCode__AddTeamAttriRequest(void* pData);

		size_t	EnCode__AddTeamAttriResult(void* pData);
		size_t	DeCode__AddTeamAttriResult(void* pData);

		size_t	EnCode__RemoveTeamAttriRequest(void* pData);
		size_t	DeCode__RemoveTeamAttriRequest(void* pData);

		size_t	EnCode__RemoveTeamAttriResult(void* pData);
		size_t	DeCode__RemoveTeamAttriResult(void* pData);

		size_t	EnCode__AddTeamMemberAttriRequest(void* pData);
		size_t	DeCode__AddTeamMemberAttriRequest(void* pData);

		size_t	EnCode__AddTeamMemberAttriResult(void* pData);
		size_t	DeCode__AddTeamMemberAttriResult(void* pData);

		size_t	EnCode__RemoveTeamMemberAttriRequest(void* pData);
		size_t	DeCode__RemoveTeamMemberAttriRequest(void* pData);

		size_t	EnCode__RemoveTeamMemberAttriResult(void* pData);
		size_t	DeCode__RemoveTeamMemberAttriResult(void* pData);

		size_t	EnCode__BroadcastData(void* pData);
		size_t	DeCode__BroadcastData(void* pData);

		size_t	EnCode__TeamBroadcastRequest(void* pData);
		size_t	DeCode__TeamBroadcastRequest(void* pData);

		size_t	EnCode__TeamBroadcastResult(void* pData);
		size_t	DeCode__TeamBroadcastResult(void* pData);

		size_t	EnCode__TeamBroadcastNotify(void* pData);
		size_t	DeCode__TeamBroadcastNotify(void* pData);

		size_t	EnCode__TeamMember2MemberRequest(void* pData);
		size_t	DeCode__TeamMember2MemberRequest(void* pData);

		size_t	EnCode__TeamMember2MemberResult(void* pData);
		size_t	DeCode__TeamMember2MemberResult(void* pData);

		size_t	EnCode__ModifyTeamMemberMapIdRequest(void* pData);
		size_t	DeCode__ModifyTeamMemberMapIdRequest(void* pData);

		size_t	EnCode__ModifyTeamMemberMapIdResult(void* pData);
		size_t	DeCode__ModifyTeamMemberMapIdResult(void* pData);

		size_t	EnCode__ModifyTeamMemberMapIdNotify(void* pData);
		size_t	DeCode__ModifyTeamMemberMapIdNotify(void* pData);

		size_t	EnCode__TeamCreateedNotify(void* pData);
		size_t	DeCode__TeamCreateedNotify(void* pData);

		size_t	EnCode__TeamDestroyedNotify(void* pData);
		size_t	DeCode__TeamDestroyedNotify(void* pData);

		size_t	EnCode__AddTeamMemberNotify(void* pData);
		size_t	DeCode__AddTeamMemberNotify(void* pData);

		size_t	EnCode__RemoveTeamMemberNotify(void* pData);
		size_t	DeCode__RemoveTeamMemberNotify(void* pData);

		size_t	EnCode__UpdateTeamMemberNotify(void* pData);
		size_t	DeCode__UpdateTeamMemberNotify(void* pData);

		size_t	EnCode__ReportGateSvrIdRequest(void* pData);
		size_t	DeCode__ReportGateSvrIdRequest(void* pData);

		size_t	EnCode__ReportGateSvrIdResult(void* pData);
		size_t	DeCode__ReportGateSvrIdResult(void* pData);

		size_t	EnCode__ModifyTeamCaptainRequest(void* pData);
		size_t	DeCode__ModifyTeamCaptainRequest(void* pData);

		size_t	EnCode__ModifyTeamCaptainResult(void* pData);
		size_t	DeCode__ModifyTeamCaptainResult(void* pData);

		size_t	EnCode__ModifyTeamCaptainNotify(void* pData);
		size_t	DeCode__ModifyTeamCaptainNotify(void* pData);

		size_t	EnCode__ModifyExpSharedModeRequest(void* pData);
		size_t	DeCode__ModifyExpSharedModeRequest(void* pData);

		size_t	EnCode__ModifyExpSharedModeResult(void* pData);
		size_t	DeCode__ModifyExpSharedModeResult(void* pData);

		size_t	EnCode__ModifyGoodSharedModeRequest(void* pData);
		size_t	DeCode__ModifyGoodSharedModeRequest(void* pData);

		size_t	EnCode__ModifyGoodSharedModeResult(void* pData);
		size_t	DeCode__ModifyGoodSharedModeResult(void* pData);

		size_t	EnCode__ModifyExpSharedModeNotify(void* pData);
		size_t	DeCode__ModifyExpSharedModeNotify(void* pData);

		size_t	EnCode__ModifyGoodSharedModeNotify(void* pData);
		size_t	DeCode__ModifyGoodSharedModeNotify(void* pData);

		size_t	EnCode__ModifyTeamRollItemLevelRequest(void* pData);
		size_t	DeCode__ModifyTeamRollItemLevelRequest(void* pData);

		size_t	EnCode__ModifyTeamRollItemLevelResult(void* pData);
		size_t	DeCode__ModifyTeamRollItemLevelResult(void* pData);

		size_t	EnCode__ModifyTeamRollItemLevelNotify(void* pData);
		size_t	DeCode__ModifyTeamRollItemLevelNotify(void* pData);

		size_t	EnCode__QueryTeamMembersCountRequest(void* pData);
		size_t	DeCode__QueryTeamMembersCountRequest(void* pData);

		size_t	EnCode__QueryTeamMembersCountResult(void* pData);
		size_t	DeCode__QueryTeamMembersCountResult(void* pData);

		size_t	EnCode__ChatGroupName(void* pData);
		size_t	DeCode__ChatGroupName(void* pData);

		size_t	EnCode__ChatGroupMember(void* pData);
		size_t	DeCode__ChatGroupMember(void* pData);

		size_t	EnCode__ChatGroupMembersList(void* pData);
		size_t	DeCode__ChatGroupMembersList(void* pData);

		size_t	EnCode__ChatGroupPassword(void* pData);
		size_t	DeCode__ChatGroupPassword(void* pData);

		size_t	EnCode__ChatGroup(void* pData);
		size_t	DeCode__ChatGroup(void* pData);

		size_t	EnCode__ChatData(void* pData);
		size_t	DeCode__ChatData(void* pData);

		size_t	EnCode__CreateChatGroupRequest(void* pData);
		size_t	DeCode__CreateChatGroupRequest(void* pData);

		size_t	EnCode__CreateChatGroupResult(void* pData);
		size_t	DeCode__CreateChatGroupResult(void* pData);

		size_t	EnCode__DestroyChatGroupRequest(void* pData);
		size_t	DeCode__DestroyChatGroupRequest(void* pData);

		size_t	EnCode__DestroyChatGroupResult(void* pData);
		size_t	DeCode__DestroyChatGroupResult(void* pData);

		size_t	EnCode__ChatGroupDestroyedNotify(void* pData);
		size_t	DeCode__ChatGroupDestroyedNotify(void* pData);

		size_t	EnCode__AddChatGroupMemberRequest(void* pData);
		size_t	DeCode__AddChatGroupMemberRequest(void* pData);

		size_t	EnCode__AddChatGroupMemberResult(void* pData);
		size_t	DeCode__AddChatGroupMemberResult(void* pData);

		size_t	EnCode__ChatGroupMemberAddedNotify(void* pData);
		size_t	DeCode__ChatGroupMemberAddedNotify(void* pData);

		size_t	EnCode__RemoveChatGroupMemberRequest(void* pData);
		size_t	DeCode__RemoveChatGroupMemberRequest(void* pData);

		size_t	EnCode__RemoveChatGroupMemberResult(void* pData);
		size_t	DeCode__RemoveChatGroupMemberResult(void* pData);

		size_t	EnCode__ChatGroupMemberRemovedNotify(void* pData);
		size_t	DeCode__ChatGroupMemberRemovedNotify(void* pData);

		size_t	EnCode__ChatGroupSpeekRequest(void* pData);
		size_t	DeCode__ChatGroupSpeekRequest(void* pData);

		size_t	EnCode__ChatGroupSpeekResult(void* pData);
		size_t	DeCode__ChatGroupSpeekResult(void* pData);

		size_t	EnCode__ChatGroupSpeekNotify(void* pData);
		size_t	DeCode__ChatGroupSpeekNotify(void* pData);

		size_t	EnCode__UnionMember(void* pData);
		size_t	DeCode__UnionMember(void* pData);

		size_t	EnCode__UnionPosRight(void* pData);
		size_t	DeCode__UnionPosRight(void* pData);

		size_t	EnCode__Union(void* pData);
		size_t	DeCode__Union(void* pData);

		size_t	EnCode__ReportPlayerUpline(void* pData);
		size_t	DeCode__ReportPlayerUpline(void* pData);

		size_t	EnCode__ReportPlayerOffline(void* pData);
		size_t	DeCode__ReportPlayerOffline(void* pData);

		size_t	EnCode__CreateUnionRequest(void* pData);
		size_t	DeCode__CreateUnionRequest(void* pData);

		size_t	EnCode__CreateUnionResult(void* pData);
		size_t	DeCode__CreateUnionResult(void* pData);

		size_t	EnCode__DestroyUnionRequest(void* pData);
		size_t	DeCode__DestroyUnionRequest(void* pData);

		size_t	EnCode__DestroyUnionResult(void* pData);
		size_t	DeCode__DestroyUnionResult(void* pData);

		size_t	EnCode__UnionDestroyedNotify(void* pData);
		size_t	DeCode__UnionDestroyedNotify(void* pData);

		size_t	EnCode__UnionMemberList(void* pData);
		size_t	DeCode__UnionMemberList(void* pData);

		size_t	EnCode__QueryUnionBasicRequest(void* pData);
		size_t	DeCode__QueryUnionBasicRequest(void* pData);

		size_t	EnCode__QueryUnionBasicResult(void* pData);
		size_t	DeCode__QueryUnionBasicResult(void* pData);

		size_t	EnCode__QueryUnionMembersRequest(void* pData);
		size_t	DeCode__QueryUnionMembersRequest(void* pData);

		size_t	EnCode__QueryUnionMembersResult(void* pData);
		size_t	DeCode__QueryUnionMembersResult(void* pData);

		size_t	EnCode__AddUnionMemberRequest(void* pData);
		size_t	DeCode__AddUnionMemberRequest(void* pData);

		size_t	EnCode__AddUnionMemberConfirm(void* pData);
		size_t	DeCode__AddUnionMemberConfirm(void* pData);

		size_t	EnCode__AddUnionMemberConfirmed(void* pData);
		size_t	DeCode__AddUnionMemberConfirmed(void* pData);

		size_t	EnCode__AddUnionMemberResult(void* pData);
		size_t	DeCode__AddUnionMemberResult(void* pData);

		size_t	EnCode__AddUnionMemberNotify(void* pData);
		size_t	DeCode__AddUnionMemberNotify(void* pData);

		size_t	EnCode__RemoveUnionMemberRequest(void* pData);
		size_t	DeCode__RemoveUnionMemberRequest(void* pData);

		size_t	EnCode__RemoveUnionMemberResult(void* pData);
		size_t	DeCode__RemoveUnionMemberResult(void* pData);

		size_t	EnCode__RemoveUnionMemberNotify(void* pData);
		size_t	DeCode__RemoveUnionMemberNotify(void* pData);

		size_t	EnCode__UnionPosRightsNotify(void* pData);
		size_t	DeCode__UnionPosRightsNotify(void* pData);

		size_t	EnCode__ModifyUnionPosRighstRequest(void* pData);
		size_t	DeCode__ModifyUnionPosRighstRequest(void* pData);

		size_t	EnCode__ModifyUnionPosRighstResult(void* pData);
		size_t	DeCode__ModifyUnionPosRighstResult(void* pData);

		size_t	EnCode__AddUnionPosRequest(void* pData);
		size_t	DeCode__AddUnionPosRequest(void* pData);

		size_t	EnCode__AddUnionPosResult(void* pData);
		size_t	DeCode__AddUnionPosResult(void* pData);

		size_t	EnCode__RemoveUnionPosRequest(void* pData);
		size_t	DeCode__RemoveUnionPosRequest(void* pData);

		size_t	EnCode__RemoveUnionPosResult(void* pData);
		size_t	DeCode__RemoveUnionPosResult(void* pData);

		size_t	EnCode__TransformUnionCaptionRequest(void* pData);
		size_t	DeCode__TransformUnionCaptionRequest(void* pData);

		size_t	EnCode__TransformUnionCaptionResult(void* pData);
		size_t	DeCode__TransformUnionCaptionResult(void* pData);

		size_t	EnCode__TransformUnionCaptionNotify(void* pData);
		size_t	DeCode__TransformUnionCaptionNotify(void* pData);

		size_t	EnCode__ModifyUnionMemberTitleRequest(void* pData);
		size_t	DeCode__ModifyUnionMemberTitleRequest(void* pData);

		size_t	EnCode__ModifyUnionMemberTitleResult(void* pData);
		size_t	DeCode__ModifyUnionMemberTitleResult(void* pData);

		size_t	EnCode__UnionMemberTitleNotify(void* pData);
		size_t	DeCode__UnionMemberTitleNotify(void* pData);

		size_t	EnCode__AdvanceUnionMemberPosRequest(void* pData);
		size_t	DeCode__AdvanceUnionMemberPosRequest(void* pData);

		size_t	EnCode__AdvanceUnionMemberPosResult(void* pData);
		size_t	DeCode__AdvanceUnionMemberPosResult(void* pData);

		size_t	EnCode__AdvanceUnionMemberPosNotify(void* pData);
		size_t	DeCode__AdvanceUnionMemberPosNotify(void* pData);

		size_t	EnCode__ReduceUnionMemberPosRequest(void* pData);
		size_t	DeCode__ReduceUnionMemberPosRequest(void* pData);

		size_t	EnCode__ReduceUnionMemberPosResult(void* pData);
		size_t	DeCode__ReduceUnionMemberPosResult(void* pData);

		size_t	EnCode__ReduceUnionMemberPosNotify(void* pData);
		size_t	DeCode__ReduceUnionMemberPosNotify(void* pData);

		size_t	EnCode__UnionChannelSpeekRequest(void* pData);
		size_t	DeCode__UnionChannelSpeekRequest(void* pData);

		size_t	EnCode__UnionChannelSpeekResult(void* pData);
		size_t	DeCode__UnionChannelSpeekResult(void* pData);

		size_t	EnCode__UnionChannelSpeekNotify(void* pData);
		size_t	DeCode__UnionChannelSpeekNotify(void* pData);

		size_t	EnCode__UpdateUnionPosCfgRequest(void* pData);
		size_t	DeCode__UpdateUnionPosCfgRequest(void* pData);

		size_t	EnCode__UpdateUnionPosCfgResult(void* pData);
		size_t	DeCode__UpdateUnionPosCfgResult(void* pData);

		size_t	EnCode__UpdateUnionPosCfgNotify(void* pData);
		size_t	DeCode__UpdateUnionPosCfgNotify(void* pData);

		size_t	EnCode__ForbidUnionSpeekRequest(void* pData);
		size_t	DeCode__ForbidUnionSpeekRequest(void* pData);

		size_t	EnCode__ForbidUnionSpeekResult(void* pData);
		size_t	DeCode__ForbidUnionSpeekResult(void* pData);

		size_t	EnCode__ForbidUnionSpeekNotify(void* pData);
		size_t	DeCode__ForbidUnionSpeekNotify(void* pData);

		size_t	EnCode__UnionSkill(void* pData);
		size_t	DeCode__UnionSkill(void* pData);

		size_t	EnCode__UnionSkillsListNotify(void* pData);
		size_t	DeCode__UnionSkillsListNotify(void* pData);

		size_t	EnCode__StudyUnionSkillRequest(void* pData);
		size_t	DeCode__StudyUnionSkillRequest(void* pData);

		size_t	EnCode__StudyUnionSkillResult(void* pData);
		size_t	DeCode__StudyUnionSkillResult(void* pData);

		size_t	EnCode__ModifyUnionActivePointRequest(void* pData);
		size_t	DeCode__ModifyUnionActivePointRequest(void* pData);

		size_t	EnCode__ModifyUnionActivePointResult(void* pData);
		size_t	DeCode__ModifyUnionActivePointResult(void* pData);

		size_t	EnCode__UnionActivePointNotify(void* pData);
		size_t	DeCode__UnionActivePointNotify(void* pData);

		size_t	EnCode__UnionTaskList(void* pData);
		size_t	DeCode__UnionTaskList(void* pData);

		size_t	EnCode__PostUnionTasksListRequest(void* pData);
		size_t	DeCode__PostUnionTasksListRequest(void* pData);

		size_t	EnCode__PostUnionTasksListResult(void* pData);
		size_t	DeCode__PostUnionTasksListResult(void* pData);

		size_t	EnCode__UnionTasksListNotify(void* pData);
		size_t	DeCode__UnionTasksListNotify(void* pData);

		size_t	EnCode__AdvanceUnionLevelRequest(void* pData);
		size_t	DeCode__AdvanceUnionLevelRequest(void* pData);

		size_t	EnCode__AdvanceUnionLevelResult(void* pData);
		size_t	DeCode__AdvanceUnionLevelResult(void* pData);

		size_t	EnCode__UnionLevelNotify(void* pData);
		size_t	DeCode__UnionLevelNotify(void* pData);

		size_t	EnCode__PostUnionBulletinRequest(void* pData);
		size_t	DeCode__PostUnionBulletinRequest(void* pData);

		size_t	EnCode__PostUnionBulletinResult(void* pData);
		size_t	DeCode__PostUnionBulletinResult(void* pData);

		size_t	EnCode__UnionBulletinNotify(void* pData);
		size_t	DeCode__UnionBulletinNotify(void* pData);

		size_t	EnCode__UnionMemberUplineNotify(void* pData);
		size_t	DeCode__UnionMemberUplineNotify(void* pData);

		size_t	EnCode__UnionMemberOfflineNotify(void* pData);
		size_t	DeCode__UnionMemberOfflineNotify(void* pData);

		size_t	EnCode__LogEntryParam(void* pData);
		size_t	DeCode__LogEntryParam(void* pData);

		size_t	EnCode__UnionLogEntry(void* pData);
		size_t	DeCode__UnionLogEntry(void* pData);

		size_t	EnCode__QueryUnionLogRequest(void* pData);
		size_t	DeCode__QueryUnionLogRequest(void* pData);

		size_t	EnCode__QueryUnionLogResult(void* pData);
		size_t	DeCode__QueryUnionLogResult(void* pData);

		size_t	EnCode__UnionEmailNotify(void* pData);
		size_t	DeCode__UnionEmailNotify(void* pData);

		size_t	EnCode__UnionOwnerCheckRequest(void* pData);
		size_t	DeCode__UnionOwnerCheckRequest(void* pData);

		size_t	EnCode__UnionOwnerCheckResult(void* pData);
		size_t	DeCode__UnionOwnerCheckResult(void* pData);

		size_t	EnCode__ModifyUnionPrestigeRequest(void* pData);
		size_t	DeCode__ModifyUnionPrestigeRequest(void* pData);

		size_t	EnCode__ModifyUnionPrestigeResult(void* pData);
		size_t	DeCode__ModifyUnionPrestigeResult(void* pData);

		size_t	EnCode__UnionPrestigeNotify(void* pData);
		size_t	DeCode__UnionPrestigeNotify(void* pData);

		size_t	EnCode__ModifyUnionBadgeRequest(void* pData);
		size_t	DeCode__ModifyUnionBadgeRequest(void* pData);

		size_t	EnCode__ModifyUnionBadgeResult(void* pData);
		size_t	DeCode__ModifyUnionBadgeResult(void* pData);

		size_t	EnCode__UnionBadgeNotify(void* pData);
		size_t	DeCode__UnionBadgeNotify(void* pData);

		size_t	EnCode__ModifyUnionMultiRequest(void* pData);
		size_t	DeCode__ModifyUnionMultiRequest(void* pData);

		size_t	EnCode__UnionMultiNotify(void* pData);
		size_t	DeCode__UnionMultiNotify(void* pData);

		size_t	EnCode__QueryPlayerRelationRequest(void* pData);
		size_t	DeCode__QueryPlayerRelationRequest(void* pData);

		size_t	EnCode__QueryPlayerRelationResult(void* pData);
		size_t	DeCode__QueryPlayerRelationResult(void* pData);

		size_t	EnCode__QueryPlayerInfoRequest(void* pData);
		size_t	DeCode__QueryPlayerInfoRequest(void* pData);

		size_t	EnCode__QueryPlayerInfoResult(void* pData);
		size_t	DeCode__QueryPlayerInfoResult(void* pData);

		size_t	EnCode__QueryUnionRankRequest(void* pData);
		size_t	DeCode__QueryUnionRankRequest(void* pData);

		size_t	EnCode__UnionRankInfo(void* pData);
		size_t	DeCode__UnionRankInfo(void* pData);

		size_t	EnCode__UnionRaceMasterInfo(void* pData);
		size_t	DeCode__UnionRaceMasterInfo(void* pData);

		size_t	EnCode__QueryUnionRankResult(void* pData);
		size_t	DeCode__QueryUnionRankResult(void* pData);

		size_t	EnCode__SearchUnionRankRequest(void* pData);
		size_t	DeCode__SearchUnionRankRequest(void* pData);

		size_t	EnCode__SearchUnionRankResult(void* pData);
		size_t	DeCode__SearchUnionRankResult(void* pData);

		size_t	EnCode__PlayerUplineTeamIdNotify(void* pData);
		size_t	DeCode__PlayerUplineTeamIdNotify(void* pData);

		size_t	EnCode__QueryRaceMasterRequest(void* pData);
		size_t	DeCode__QueryRaceMasterRequest(void* pData);

		size_t	EnCode__RaceMaster(void* pData);
		size_t	DeCode__RaceMaster(void* pData);

		size_t	EnCode__QueryRaceMasterResult(void* pData);
		size_t	DeCode__QueryRaceMasterResult(void* pData);

		size_t	EnCode__TeamLogNotify(void* pData);
		size_t	DeCode__TeamLogNotify(void* pData);

		size_t	EnCode__QueryGuildMembersRequest(void* pData);
		size_t	DeCode__QueryGuildMembersRequest(void* pData);

		size_t	EnCode__GuildMember(void* pData);
		size_t	DeCode__GuildMember(void* pData);

		size_t	EnCode__QueryGuildMembersResult(void* pData);
		size_t	DeCode__QueryGuildMembersResult(void* pData);

		size_t	EnCode__AddFriendlyDegree(void* pData);
		size_t	DeCode__AddFriendlyDegree(void* pData);

		size_t	EnCode__Tweet(void* pData);
		size_t	DeCode__Tweet(void* pData);

		size_t	EnCode__AddTweet(void* pData);
		size_t	DeCode__AddTweet(void* pData);

		size_t	EnCode__ShowFriendTweet(void* pData);
		size_t	DeCode__ShowFriendTweet(void* pData);

		size_t	EnCode__QueryFriendTweetsRequest(void* pData);
		size_t	DeCode__QueryFriendTweetsRequest(void* pData);

		size_t	EnCode__QueryFriendTweetsResult(void* pData);
		size_t	DeCode__QueryFriendTweetsResult(void* pData);

		size_t	EnCode__QueryFriendsListRequest(void* pData);
		size_t	DeCode__QueryFriendsListRequest(void* pData);

		size_t	EnCode__QueryFriendsListResult(void* pData);
		size_t	DeCode__QueryFriendsListResult(void* pData);

		size_t	EnCode__NotifyExpAdded(void* pData);
		size_t	DeCode__NotifyExpAdded(void* pData);

		size_t	EnCode__NotifyAddExp(void* pData);
		size_t	DeCode__NotifyAddExp(void* pData);

		size_t	EnCode__QueryExpNeededWhenLvlUpRequest(void* pData);
		size_t	DeCode__QueryExpNeededWhenLvlUpRequest(void* pData);

		size_t	EnCode__QueryExpNeededWhenLvlUpResult(void* pData);
		size_t	DeCode__QueryExpNeededWhenLvlUpResult(void* pData);

		size_t	EnCode__NotifyTeamGain(void* pData);
		size_t	DeCode__NotifyTeamGain(void* pData);

		size_t	EnCode__ModifyTweetReceiveFlag(void* pData);
		size_t	DeCode__ModifyTweetReceiveFlag(void* pData);

		size_t	EnCode__FriendDegreeGainEmail(void* pData);
		size_t	DeCode__FriendDegreeGainEmail(void* pData);

		size_t	EnCode__GRQueryNickUnion(void* pData);
		size_t	DeCode__GRQueryNickUnion(void* pData);

		size_t	EnCode__RGQueryNickUnionRst(void* pData);
		size_t	DeCode__RGQueryNickUnionRst(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool                    m_kPackage;
	};
}
#endif			//__RelationServiceProtocol__
