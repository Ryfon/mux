﻿/**
* @file MsgToPut.h
* @brief 在网络与应用之间传递的消息结构定义
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MsgToPut.h
* 摘    要: 在网络与应用之间传递的消息结构定义
* 作    者: dzj
* 完成日期: 2007.11.30
*
*/

#pragma once

#include "../tcache/tcache.h"
#include "CliProtocol.h"

#ifdef USE_DSIOCP
    #include "iocp/dssocket.h"
 #endif //USE_DSIOCP

/////////////////////////////////////////////////////////////////////////////////////////////////
///以下为各srv使用的sessionID范围，参见文档《网络模块与上层应用协议.doc》...
const unsigned short SRV_SID_MAX = 1000;
const unsigned short DBSRV_SID_BASE = 400;//主动连接dbsrv时，dbsrv的sessionID base
const unsigned short DBSRV_SID_MAX = 500;//最大dbsrv sessionID号，最多100个dbsrv,目前只有一个
const unsigned short CENTERSRV_SID_BASE = 300;//主动连接centersrv时，centersrv的sessionID base
const unsigned short CENTERSRV_SID_MAX = 301;//最大centersrv sessionID号，只有一个
const unsigned short LOGINSRV_SID_BASE = 301;//主动连接centersrv时，centersrv的sessionID base
const unsigned short LOGINSRV_SID_MAX = 350;//最大centersrv sessionID号，只有一个
const unsigned short NEWLOGINSRV_SID_BASE = 351;//主动连接centersrv时，centersrv的sessionID base
const unsigned short NEWLOGINSRV_SID_MAX = 359;//最大centersrv sessionID号，只有一个
const unsigned short RELATIONSRV_SID_BASE = 600;//主动连接relationsrv时，relationsrv的sessionID base;
const unsigned short RELATIONSRV_SID_MAX = 650;//最大relationsrv sessionID,最多50个
const unsigned short MAPSRV_SID_BASE = 700; //主动连接mapsrv时，mapsrv的sessionID base;
const unsigned short MAPSRV_SID_MAX = 800; //最大mapsrv sessionID号，最多100个mapsrv;
const unsigned short RANKSRV_SID_BASE = 801;//主动连接RankSrv时，rankSrv的sessionID base;
const unsigned short RANKSRV_SID_MAX  = 802;//最大的ranksrv sessionID号,只有一个
const unsigned short SHOP_SID_BASE = 850;//主动连接shopsrv时,shopSrv的sessionID base;
const unsigned short SHOP_SID_MAX = 851;//最大的shopserver sessionID号，只有一个
const unsigned short LOG_SID_BASE = 900;//主动连接logsrv时,logSrv的sessionID base;
const unsigned short LOG_SID_MAX = 901;//最大的logserver sessionID号，只有一个
const unsigned short GATE_PLAYER_BROCASTSID = 999; //[800-1000)保留 （999：用于向nSessionID>=1000的已建立连接群发消息，07.24 by dzj）
///...以上为各srv使用的sessionID范围，参见文档《网络模块与上层应用协议.doc》
/////////////////////////////////////////////////////////////////////////////////////////////////


///在通信模块与缓冲队列之间传递的消息，中间包含了与通信模块相关的句柄值
///将此句柄值以及校验码传回通信模块，通信模块将可以唯一确定与该消息关联的客户端
///！！！注意：必须从全局池中分配，不可自行new，不可定义临时变量，不可定义静态变量，不可memset;
///因为所有的MsgToPut最后都要通过对象池回收，不遵守上述约定会造成内存错误；
struct MsgToPut
{
	static const unsigned int MSG_MAX_SIZE = MUX_PROTO::MAX_MSG_SIZE;
public:
	MsgToPut() : nHandleID(0), nSessionID(0), nMsgLen(0)
	{
#ifdef USE_DSIOCP
       pUniqueSocket = NULL;
#endif //USE_DSIOCP
	}
	~MsgToPut()
	{
		PoolObjInit();
	}

	PoolFlagDefine()
	{
#ifdef USE_DSIOCP
		if ( NULL != pUniqueSocket )
		{
			if ( NULL != g_poolUniDssocket )
			{
				g_poolUniDssocket->Release( pUniqueSocket );
			}
			pUniqueSocket = NULL;
		}
#endif //USE_DSIOCP
		nHandleID = 0;
		nSessionID = 0;
		nMsgLen = 0;
	}
public:
	int  nHandleID;/*与该包关联的句柄标识*/
	int  nSessionID;/*关联句柄校验信息(例如IP地址＋端口)没有被中途替换)*/
	int  nMsgLen;
	char pMsg[MSG_MAX_SIZE];/*消息设为固定大小*/

//for debug, #ifdef USE_DSIOCP
//	//int jjj, test,
//	unsigned long msgcmd;
//#endif //USE_DSIOCP

#ifdef USE_DSIOCP
	UniqueObj< CDsSocket >* pUniqueSocket;//该消息对应的socket;
#endif //USE_DSIOCP
};

extern TCache< MsgToPut >*  g_poolMsgToPut;
