﻿/** @file CItemProp.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-4-29
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef ITEM_GLOBAL_H
#define ITEM_GLOBAL_H

namespace ITEM_GLOBAL
{

	typedef unsigned int u32;
	typedef int s32;

	enum ITEM_TYPE
	{
		E_EQUIP = 0x0,//可装备物品
		E_CONSUME,//可消耗道具
		E_NORMAL,//普通物品
		E_SKILL,//技能书
		E_TASK,//任务道具
		E_FUNCTION,//功能性道具
		E_AXIOLOGY,//价值物
		E_BULEPRINT,//图纸
		E_FASHION,//时装
		E_TYPE_MAX,
	};

	enum ITEM_MASSLEVEL
	{
		E_GRAY = 0x0,//灰色
		E_WHITE,//白色
		E_ORANGE,//橙色
		E_BLUE,//蓝色
		E_GREEN,//绿色
		/*物品品质去掉紫色，红色两种（zsw/2010.8.13）
		E_PURPLE,//紫色
		E_RED,//红
		*/
		E_QUALITY_MAX,
	};

	enum ITEM_METAL
	{
		E_METAL_BIG = 0x0,//金属类质感大件的物品
		E_METAL_LITTLE,//金属类质感小件的物品	
		E_GEM,//宝石类质感的物品	
		E_COARSE,//粗质类质感的物品	
		E_LIQUID,//液体类质感的物品	
		E_BOOK,//书籍类质感的物品	
		E_PAGE,//书页类质感的物品	
		E_METAL_MAX,
	};

	//道具的装备方式
	enum ITEM_TRADE
	{
		E_NOR = 0x0,//无
		E_NO_PLAYER,//不可玩家交易
		E_NO_NPC,//不可NPC交易
		E_NO_TRADE,//全部不可交易
		E_TRADE_MAX,
	};

	////道具类型
	//enum ITEM_KIND
	//{
	//	E_FUNCTION_ITEM = 0x0,
	//	E_SKILL_ITEM,
	//	E_OTHER_ITEM,
	//	E_TASK_ITEM,
	//	E_WEAPON_ITEM,
	//	E_DRAG_ITEM,
	//	E_KIND_MAX,
	//};

	//所属种族
	enum ITEM_NATION
	{
		E_HUMAN = 0x1,//人类
		E_AIKA,//艾卡 
		E_FAIRY,//精灵
		E_NATION_MAX,
	};

	//所属职业
	enum ITEM_CLASS
	{
		E_SWORDMAN = 0x1,//剑士
		E_ASSASSIN,//刺客
		E_GOLDRIDER,//圣骑士
		E_DEVILSWORDMAN,//魔剑士
		E_ARCHER,//弓箭手
		E_ENCHANTER,//魔法师
		E_CLASS_MAX,
	};

	enum ITEM_TYPEID
	{
		E_TYPE_WEAPON = 0x0,//武器
		E_TYPE_EQUIP,//道具
		E_TYPE_DRUG,//药品
		E_TYPE_TASK,//任务
		E_TYPE_SKILL,//技能
		E_TYPE_FUNCTION,//功能
		E_TYPE_MONEY,//金钱
		E_TYPE_CONSUME,//消耗品
		E_TYPE_AXIOLOGY,//价值物
		E_TYPE_BULEPRINT,//图纸道具
		E_TYPE_FASHION,//时装
		E_TYPE_MOUNT,
		E_TYPE_OTHER,
	};

	enum ITEM_BIND_TYPE
	{
		E_BIND = 0x0 ,//道具绑定
		E_UN_BIND = 0x1 ,//道具未绑定
		E_EQUIP_BIND,//装备绑定
		E_PICK_BIND,//拾取绑定
		E_BIND_OTHER,
	};
	
};


#endif

