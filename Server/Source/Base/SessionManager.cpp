﻿#ifndef SESSION_MANAGER_CPP
#define SESSION_MANAGER_CPP

#include "SessionManager.h"

template <class SVC_HANDLER, class ACE_LOCK>
CHandlerManagerEx<SVC_HANDLER, ACE_LOCK>::CHandlerManagerEx(void)
:m_uiInitSize(0), m_pHandlers(NULL), m_uiLastHandlerIndex(0),m_uiLastSessionID(CHandlerManagerEx::SESSION_ID_START)
{
}

template <class SVC_HANDLER, class ACE_LOCK>
CHandlerManagerEx<SVC_HANDLER, ACE_LOCK>::~CHandlerManagerEx(void)
{
	if(NULL != m_pHandlers)
		delete []m_pHandlers;
}

template <class SVC_HANDLER, class ACE_LOCK>
int CHandlerManagerEx<SVC_HANDLER, ACE_LOCK>::Open(unsigned int uiInitSize)
{
	m_pHandlers = NEW SVC_HANDLER[uiInitSize];
	if(NULL == m_pHandlers)
		return -1;

	m_uiInitSize = uiInitSize;
	return 0;
}

template <class SVC_HANDLER, class ACE_LOCK>
SVC_HANDLER * CHandlerManagerEx<SVC_HANDLER, ACE_LOCK>::GetHandler(unsigned int uiSessionID)
{
	// 是否已初始化?
	if(NULL == m_pHandlers)
		return NULL;

	// 获取一个处理器
	bool bFindValidHandler = false;
	SVC_HANDLER *pHandler = NULL;

	for(unsigned int i = m_uiLastHandlerIndex + 1; i < m_uiInitSize; i++)
	{
		if(!(m_pHandlers[i].IsUsed()))
		{
			ACE_GUARD_RETURN(ACE_LOCK, mon, m_mutex, NULL);
			if(!(m_pHandlers[i].IsUsed()))
			{
				m_pHandlers[i].IsUsed(true);

				bFindValidHandler = true;
				m_uiLastHandlerIndex = i;

				pHandler = &(m_pHandlers[i]);

				if(uiSessionID == 0)
				{
					pHandler->SessionID(m_uiLastSessionID + 1);

					m_uiLastSessionID++;
				}
				else
					pHandler->SessionID(uiSessionID);

				//m_handlerSet.insert(pHandler);

				break;
			}
		}
	}

	if(!bFindValidHandler)
	{
		for(unsigned int i = 0; i < m_uiInitSize; i++)
		{
			if(!(m_pHandlers[i].IsUsed()))
			{
				ACE_GUARD_RETURN(ACE_LOCK, mon, m_mutex, NULL);
				if(!(m_pHandlers[i].IsUsed()))
				{
					m_pHandlers[i].IsUsed(true);

					bFindValidHandler = true;
					m_uiLastHandlerIndex = i;

					pHandler = &(m_pHandlers[i]);

					if(uiSessionID == 0)
					{
						pHandler->SessionID(m_uiLastSessionID + 1);

						m_uiLastSessionID++;
					}
					else
						pHandler->SessionID(uiSessionID);

					//m_handlerSet.insert(pHandler);

					break;
				}
			}
		}
	}

	return pHandler;
}

template <class SVC_HANDLER, class ACE_LOCK>
void CHandlerManagerEx<SVC_HANDLER, ACE_LOCK>::Handlers(std::set<SVC_HANDLER *>& handlers)
{
	handlers.clear();

	ACE_GUARD(ACE_LOCK, mon, m_mutex);
	for(unsigned int i = 0; i < m_uiInitSize; i++ )
	{
		if(m_pHandlers[i].IsUsed())
			handlers.insert(&(m_pHandlers[i]));
	}

	return;
}

//template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
//CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::CAcceptorEx(void)
//:m_pReadQueue(NULL), m_pSendQueue(NULL)
//{
//
//}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::CAcceptorEx(IBufQueue *pReadQueue, IBufQueue *pSendQueue, unsigned int uiPeerBufferSize, CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *pHandlerManager)
: m_pReadQueue(pReadQueue), m_pSendQueue(pSendQueue),m_uiPeerBufferSize(uiPeerBufferSize), m_pHandlerManager(pHandlerManager)
{
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::~CAcceptorEx(void)
{

}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
int CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::open(const ACE_PEER_ACCEPTOR_ADDR &local_addr, ACE_Reactor *reactor,int flags, int use_select, int reuse_addr )
{
	if(parent::open(local_addr, reactor, flags, use_select, reuse_addr) == -1)
		return -1;

	// 设置recvbuf,sendbuf
	int iOldBuf = 0;
	int iNewbuf = 512* 1024;
	int optLen = sizeof(iOldBuf);

	int i = this->acceptor().get_option(SOL_SOCKET, SO_SNDBUF, &iOldBuf, &optLen);

	i = this->acceptor().set_option(SOL_SOCKET, SO_RCVBUF, &iNewbuf, sizeof(iNewbuf));
	i = this->acceptor().set_option(SOL_SOCKET, SO_SNDBUF, &iNewbuf, sizeof(iNewbuf));

	i = this->acceptor().get_option(SOL_SOCKET, SO_SNDBUF, &iOldBuf, &optLen);

	return 0;

}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
int CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::make_svc_handler(SVC_HANDLER *&sh)
{
	if((m_pReadQueue == NULL)|| (m_pSendQueue == NULL))
		return -1;

	if(sh == 0)
	{	
		sh  = m_pHandlerManager->GetHandler();
		if(NULL == sh)
			return -1;
	}

	sh->reactor (this->reactor ());

	// 设置队列
	sh->ReadQueue(this->ReadQueue());
	sh->SendQueue(this->SendQueue());

	return 0;
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
IBufQueue * CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::ReadQueue(void)
{
	return m_pReadQueue;
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
void CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::ReadQueue(IBufQueue *pReadQueue)
{
	m_pReadQueue = pReadQueue;
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
IBufQueue * CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::SendQueue(void)
{
	return m_pSendQueue;
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
void CAcceptorEx<SVC_HANDLER,ACE_PEER_ACCEPTOR_2, ACE_LOCK>::SendQueue(IBufQueue *pSendQueue)
{
	m_pSendQueue = pSendQueue;
}


//template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
//CConnectorEx<SVC_HANDLER, ACE_PEER_CONNECTOR_2, ACE_LOCK>::CConnectorEx(void)
//:m_pReadQueue(NULL), m_pSendQueue(NULL)
//{
//
//}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
CConnectorEx<SVC_HANDLER, ACE_PEER_CONNECTOR_2, ACE_LOCK>::CConnectorEx(IBufQueue *pReadQueue, IBufQueue *pSendQueue, unsigned int uiPeerBufferSize, CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *pHandlerManager)
: m_pReadQueue(pReadQueue), m_pSendQueue(pSendQueue),m_uiPeerBufferSize(uiPeerBufferSize), m_pHandlerManager(pHandlerManager)
{
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
CConnectorEx<SVC_HANDLER, ACE_PEER_CONNECTOR_2, ACE_LOCK>::~CConnectorEx(void)
{

}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
int CConnectorEx<SVC_HANDLER, ACE_PEER_CONNECTOR_2, ACE_LOCK>::make_svc_handler(SVC_HANDLER *&sh)
{
	if((m_pReadQueue == NULL)|| (m_pSendQueue == NULL))
		return -1;

	if(sh == 0)
	{	
		sh  = m_pHandlerManager->GetHandler();
		if(NULL == sh)
			return -1;
	}

	sh->reactor (this->reactor ());

	// 设置队列
	sh->ReadQueue(this->ReadQueue());
	sh->SendQueue(this->SendQueue());

	return 0;
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
IBufQueue * CConnectorEx<SVC_HANDLER, ACE_PEER_CONNECTOR_2, ACE_LOCK>::ReadQueue(void)
{
	return m_pReadQueue;
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
void CConnectorEx<SVC_HANDLER, ACE_PEER_CONNECTOR_2, ACE_LOCK>::ReadQueue(IBufQueue *pReadQueue)
{
	m_pReadQueue = pReadQueue;
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
IBufQueue * CConnectorEx<SVC_HANDLER, ACE_PEER_CONNECTOR_2, ACE_LOCK>::SendQueue(void)
{
	return m_pSendQueue;
}

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
void CConnectorEx<SVC_HANDLER, ACE_PEER_CONNECTOR_2, ACE_LOCK>::SendQueue(IBufQueue *pSendQueue)
{
	m_pSendQueue = pSendQueue;
}

#endif/*SESSION_MANAGER_CPP*/
