﻿#include "HandlerUdp.h"
#include "Utility.h"


CHandlerUdp::CHandlerUdp(unsigned short usLocalPort)
	:m_mode(NO_USE_REACTOR), m_lTimerID(0), m_localAddr(usLocalPort), m_peer(m_localAddr)
{

}

CHandlerUdp::CHandlerUdp(const char *pszRemoteHost, unsigned short usRemotePort)
	:m_mode(NO_USE_REACTOR), m_lTimerID(0),m_remoteAddr(usRemotePort, pszRemoteHost), m_localAddr((unsigned short)0), m_peer(m_localAddr)
{

}

CHandlerUdp::~CHandlerUdp(void)
{
	//by dzj, 09.03.27，不在析构函数中调用虚函数，close();
}

int CHandlerUdp::open(void * arg)
{
	if(NULL != arg)
	{
		ACE_Reactor * pReactor = (ACE_Reactor *)arg;
		this->reactor(pReactor);

		// 注册读事件
		if(this->reactor()->register_handler(this, ACE_Event_Handler::READ_MASK) == -1)
		{
			D_ERROR("CHandlerUdp::open中register_handler出错\n");
			return -1;
		}

		// 注册定时器
		m_lTimerID = this->reactor()->schedule_timer(this, arg, ACE_Time_Value(0,1000), ACE_Time_Value(10,0));
		if( m_lTimerID == -1)
		{
			D_ERROR("CHandlerUdp::open中schedule_timer出错\n");
			return -1;
		}

		m_mode = USE_REACTOR;
	}

	return 0;
}

int CHandlerUdp::close(u_long flags)
{
	ACE_UNUSED_ARG(flags);

	if(m_peer.get_handle() != ACE_INVALID_HANDLE)
	{	
		if(m_mode == USE_REACTOR)
		{
			this->reactor()->remove_handler(this, ACE_Event_Handler::READ_MASK | ACE_Event_Handler::DONT_CALL);

			if(m_lTimerID > 0)
				this->reactor()->cancel_timer(m_lTimerID);
		}

		m_peer.close();
	}

	return 0;
}

int CHandlerUdp::handle_input(ACE_HANDLE fd)
{
	ACE_UNUSED_ARG( fd );
	return 0;
}

int CHandlerUdp::handle_timeout(const ACE_Time_Value &current_time, const void *act)
{
	ACE_UNUSED_ARG(current_time);
	ACE_UNUSED_ARG(act);

	char temp[] = "hello,world";
	Send(temp, sizeof(temp));

	return 0;
}

int CHandlerUdp::handle_close(ACE_HANDLE handle, ACE_Reactor_Mask close_mask)
{
	ACE_UNUSED_ARG( handle );
	ACE_UNUSED_ARG( close_mask );
	// 销毁自己
	delete this;

	return 0;
}

ACE_HANDLE CHandlerUdp::get_handle(void) const
{
	return m_peer.get_handle();
}

int CHandlerUdp::Send(const char *pszData, unsigned int uiDataLen)
{
	// 发送数据
	if(m_peer.send(pszData, uiDataLen, m_remoteAddr) == -1)
	{
		//D_ERROR("CHandlerUdp::Send发送到对端(%s:%d)时出错\n", m_remoteAddr.get_host_name(), m_remoteAddr.get_port_number());
		return -1;
	}

	return 0;
}
