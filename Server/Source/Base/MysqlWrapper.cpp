﻿#include "MysqlWrapper.h"
#include "string.h"
#include "stdlib.h"
#include "Utility.h"

CConnectionPool::CConnectionPool(unsigned short uiConnectionNum)
	: m_uiConnectionNum(0), m_pConnections(NULL)
{
	CConnection *pConnections = NEW CConnection[uiConnectionNum];
	if(NULL != pConnections)
	{
		m_uiConnectionNum = uiConnectionNum;
		m_pConnections = pConnections;
	}
}

CConnectionPool::~CConnectionPool(void)
{
	delete [] m_pConnections;
}

CConnection * CConnectionPool::GetConnection(unsigned uiConnectionIndex)
{
	if((m_pConnections != NULL) && (uiConnectionIndex < m_uiConnectionNum))
		return &m_pConnections[uiConnectionIndex];
	else
	{
		D_ERROR("CConnectionPool::GetConnection中m_pConnections为空或者uiConnectionIndex%d下标越界\n", uiConnectionIndex);
		return NULL;
	}
}

CConnection::CConnection(void)
{
	m_bIsInit = false;
	m_pMysql = NULL;

	memset(&m_stConnStr, 0x00, sizeof(m_stConnStr));
}

CConnection::~CConnection(void)
{
	// 关闭连接
	Close();
}

int CConnection::Open(stConnectionString *pstConnStr)
{
	if(NULL == pstConnStr)
		return -1;

	if(!m_bIsInit)
	{
		// 判断连接串
		if(    (strncmp(pstConnStr->szHost,     "", strlen(pstConnStr->szHost)) == 0)
			|| (strncmp(pstConnStr->szUser,     "", strlen(pstConnStr->szUser)) == 0)
			|| (strncmp(pstConnStr->szPassword, "", strlen(pstConnStr->szPassword)) == 0)
			|| (strncmp(pstConnStr->szDb,       "", strlen(pstConnStr->szDb)) == 0)   )
		   return -1;

		// 初始化mysql对象
		m_pMysql = mysql_init(NULL);
		if(NULL == m_pMysql)
			return -1;

		// 连接
		if(mysql_real_connect(m_pMysql, 
							  pstConnStr->szHost, pstConnStr->szUser, pstConnStr->szPassword, pstConnStr->szDb, pstConnStr->port, 
							  NULL, CLIENT_MULTI_STATEMENTS) == NULL)
		{
			D_ERROR("CConnection::Open出错,错误原因:%s.\n", mysql_error(m_pMysql));
			return -1;
		}

		m_bIsInit = true;
		ACE_OS::memcpy(&m_stConnStr, pstConnStr, sizeof(m_stConnStr));
	}

	return 0;
}

void CConnection::Close(void)
{
	mysql_close(m_pMysql);

	m_bIsInit = false;
}

int CConnection::Ping(void)
{
	if ( !m_pMysql )
		return -1;

	if(mysql_ping(m_pMysql) != 0)
	{
		// 关闭连接
		Close();

		// 尝试重连
		return Open(&m_stConnStr);
	}
	else
		return 0;
}

MYSQL *CConnection::Mysql(void)
{
	return m_pMysql;
}

CQuery * CConnection::CreateQuery(void)
{
	CQuery * pQuery = NEW CQuery(this);
	if(pQuery == NULL)
		return NULL;
	else
		return pQuery;
}

void CConnection::DestroyQuery(CQuery* pQuery)
{
	delete pQuery;
}

CQuery::CQuery(CConnection *pConnection)
{
	m_pConnection = pConnection;
	m_uiInsertID = 0;
}

CQuery::~CQuery(void)
{
}

CResult* CQuery::ExecuteSelect(const char *pszQuery, unsigned long ulLength)
{
	if(NULL == pszQuery)
		return NULL;

	// 判断连接是否有效?
	if(m_pConnection->Ping() != 0)
		return NULL;

	// 执行查询
	if(mysql_real_query(m_pConnection->Mysql(), pszQuery, ulLength) !=0)
	{
		D_ERROR("CQuery::ExecuteSelect出错,错误原因:%s.\n", mysql_error(m_pConnection->Mysql()));
		return NULL;
	}

	// 保存结果
	MYSQL_RES *res = mysql_store_result(m_pConnection->Mysql());
	if(NULL == res)
		return NULL;

	CResult * pResult = NEW CResult(m_pConnection, res);
	if(pResult == NULL)
	{
		// 释放结果
		mysql_free_result(res);
		return NULL;
	}

	return pResult;
}

int CQuery::ExecuteUpdate(const char *pszQuery, unsigned long ulLength)
{
	if(NULL == pszQuery)
		return -1;

	// 判断连接是否有效?
	if(m_pConnection->Ping() != 0)
		return -1;

	// 执行查询
	if(mysql_real_query(m_pConnection->Mysql(), pszQuery, ulLength) !=0)
	{
		D_ERROR("CQuery::ExecuteUpdate出错%s,错误原因:%s.\n", pszQuery, mysql_error(m_pConnection->Mysql()));
		return -1;
	}
	
	m_uiInsertID = (unsigned int) mysql_insert_id(m_pConnection->Mysql());
	return (int)mysql_affected_rows(m_pConnection->Mysql());	
}

void CQuery::FreeResult(CResult *pResult)
{
	if(NULL == pResult)
		return;

	// 清理
	mysql_free_result(pResult->Res());
	
	delete pResult;
	
	return;
}

unsigned long CQuery::RealEscape(char *pszDest, const char *pszSource, unsigned long ulSourceLen)
{	
	return mysql_real_escape_string(m_pConnection->Mysql(), pszDest, pszSource, ulSourceLen);
}

unsigned int CQuery::InsertID(void)
{
	return m_uiInsertID;
}

CResult::CResult(CConnection *pConnection, MYSQL_RES *pRes)
{
	m_pConnection = pConnection;
	m_pResult = pRes;

	m_rowNum = 0;
	m_fieldNum = 0;

	if(m_pResult != NULL)
	{
		m_rowNum = (unsigned int) mysql_num_rows(m_pResult);
		m_fieldNum = mysql_num_fields(m_pResult);
	}
}

CResult::~CResult(void)
{

}

MYSQL_RES * CResult::Res(void)
{
	return this->m_pResult;
}

unsigned int CResult::RowNum(void)
{
	return (unsigned int) (mysql_num_rows(m_pResult));
}

unsigned int CResult::FieldNum(void)
{
	return mysql_num_fields(m_pResult);
}

bool CResult::Next(void)
{
	m_row = mysql_fetch_row(m_pResult);
	if(NULL == m_row)
		return false;
	else
		return true;
}

int CResult::GetInt(unsigned int uiFieldIndex)
{	
	if(uiFieldIndex < m_fieldNum)
		return atoi(m_row[uiFieldIndex]);
	else
	{
		D_ERROR("CResult::GetInt,下标越界(m_fieldNum:uiFieldIndex):(%d:%d)\n", m_fieldNum, uiFieldIndex);
		return -1;
	}
}

char * CResult::GetString(unsigned int uiFieldIndex)
{
	if(uiFieldIndex < m_fieldNum)
		return m_row[uiFieldIndex];
	else 
	{
		D_ERROR("CResult::GetString,下标越界(m_fieldNum:uiFieldIndex):(%d:%d)\n", m_fieldNum, uiFieldIndex);
		return NULL;
	}
}

double CResult::GetDouble(unsigned int uiFieldIndex)
{
	if(uiFieldIndex < m_fieldNum)
		return atof(m_row[uiFieldIndex]);
	else
	{
		D_ERROR("CResult::GetDouble,下标越界(m_fieldNum:uiFieldIndex):(%d:%d)\n", m_fieldNum, uiFieldIndex);
		return -1;
	}
}

float CResult::GetFloat(unsigned int uiFieldIndex)
{
	if(uiFieldIndex < m_fieldNum)
		return (float) (atof(m_row[uiFieldIndex]));
	else
	{
		D_ERROR("CResult::GetFloat,下标越界(m_fieldNum:uiFieldIndex):(%d:%d)\n", m_fieldNum, uiFieldIndex);
		return -1;
	}
}

unsigned int CResult::GetDate(unsigned int uiFieldIndex)
{
	if(uiFieldIndex < m_fieldNum)
		return (unsigned int) (atoi(m_row[uiFieldIndex]));
	else
	{
		D_ERROR("CResult::GetDate,下标越界(m_fieldNum:uiFieldIndex):(%d:%d)\n", m_fieldNum, uiFieldIndex);
		return 0;
	}
}

CTableTransactionCtrl::CTableTransactionCtrl(CConnection *pConn, const char* pLockSql)
	:m_connection(pConn), m_query(NULL)
{
	if(NULL != m_connection)
	{
		m_query = m_connection->CreateQuery();
		if(NULL != m_query)
		{
			m_query->ExecuteUpdate(pLockSql, (unsigned long)strlen(pLockSql) );
		}
	}	
}

CTableTransactionCtrl::~CTableTransactionCtrl(void)
{
	if((NULL != m_connection) && (NULL != m_query))
	{
		const char* pUnlockSql = "UNLOCK TABLES";

		m_query->ExecuteUpdate(pUnlockSql, (unsigned long)strlen(pUnlockSql) );
		m_connection->DestroyQuery(m_query);
	}
}

bool CQueryWrapper::ExecQuery( const std::string& queryString )
{
	if ( m_isExcuted || ( NULL != m_pRst ) )
	{
		//之前已执行过查询，不可重复执行
		return false;
	}
	//if ( NULL == pQueryString )
	//{
	//	return false;
	//}
	if ( NULL == m_pQuery )
	{
		return false;
	}
	m_isExcuted = true;
	//if ( queryLen >= 10*1024*1024 )
	//{
	//	//不可能有这么大的查询;
	//	return false;
	//}
	//char* pRealQuery = NEW char[queryLen*2];
	//int realLen = m_pQuery->RealEscape( pRealQuery, pQueryString, queryLen );
	//if ( realLen <= 0 )
	//{
	//	delete [] pRealQuery; pRealQuery = NULL;
	//	return false;
	//}
	m_pRst = m_pQuery->ExecuteSelect( queryString.c_str(), (unsigned long)queryString.length() );
	if ( NULL != m_pRst )
	{
		m_rcdNum = m_pRst->RowNum();
		m_fieldNum = m_pRst->FieldNum();
	}
	//delete [] pRealQuery; pRealQuery = NULL;

	return true;
}
