﻿/**
* @file LoadSrvConfig.h
* @brief 服务配置信息读取
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: LoadSrvConfig.h
* 摘    要: 服务配置信息读取
* 作    者: dzj
* 完成日期: 2008.01.03
*
*/

#pragma once

#include "../XmlManager.h"
#include "../Utility.h"

#include <vector>

using namespace std;

/**
需要连接的其它服务信息；
Type="MapSrv" id="0" Addr="172.18.8.34" Port="6801" />
*/
struct OtherSrvConnInfo
{
	unsigned short srvType;
	unsigned long  srvID;
	char srvAddr[16];//服务器端内部通信用地址
	unsigned short srvPort;//服务器端内部通信用端口
	char srvAddr2[16];//服务器端对外通信用地址(gatesrv、assignsrv)
	unsigned short srvPort2;////服务器端对外通信用端口(gatesrv、assignsrv)
};

/**
本服务的服务配置信息读取
selfconfig.xml ： 本服务的类型与序号；
srvconfig.xml ： 整个服务器端各服务的类型与序号；
*/
class CLoadSrvConfig
{
private:
	CLoadSrvConfig();//屏蔽此操作；

public:
	/**
	读入服务配置；
	selfconfig.xml ： 本服务的类型与序号；
    srvconfig.xml ： 整个服务器端各服务的类型与序号；
	*/
	static bool LoadConfig();

	//取自身在同类服务中的ID序号；
	static unsigned long GetSelfID()
	{
		return m_srvID;
	}

	//取自身的外部监听地址；
	static const char* GetOuterListenAddr()
	{
		return (const char*)m_srvAddr2;
	}

	//取自身外部监听端口；
	static const unsigned short GetOuterListenPort()
	{
		return m_srvPort2;
	}

	//取本服务的监听地址；
	static const char* GetListenAddr()
	{
		return (const char*)m_srvAddr;
	}

	///取本服务的监听端口；
	static const unsigned short GetListenPort()
	{
		return m_srvPort;
	}

	///返回DbSrv的监听地址；
	static const char* GetDbSrvAddr( unsigned long dbSrvID );

	///返回DbSrv的监听端口；
	static const unsigned short GetDbSrvPort( unsigned long dbSrvID );

	///返回NewLoginSrv的监听地址；
	static const char* GetNewLoginSrvAddr();

	///返回NewLoginSrv的监听端口；
	static const unsigned short GetNewLoginSrvPort();

	///返回RelationSrv的监听地址；
	static const char* GetRelationSrvAddr();

	///返回RelationSrv的监听端口；
	static const unsigned short GetRelationSrvPort();

	///返回ShopSrv的监听地址；
	static const char* GetShopSrvAddr();

	///返回ShopSrv的监听端口；
	static const unsigned short GetShopSrvPort();

	///返回LogSrv的监听地址
	static const char* GetLogSrvAddr();

	///返回LogSrv的监听端口
	static const unsigned short GetLogSrvPort();

	///返回AssignSrv的地址；
	static const char* GetAssignSrvAddr();

	///返回AssignSrv的端口；
	static const unsigned short GetAssignSrvPort();

	///返回CenterSrv的监听地址；
	static const char* GetCenterSrvAddr();

	///返回CenterSrv的监听端口；
	static const unsigned short GetCenterSrvPort();

	///返回LoginSrv的监听地址；
	static const char* GetLoginSrvAddr( unsigned long loginSrvID );

	///返回LoginSrv的监听端口；
	static const unsigned short GetLoginSrvPort( unsigned long loginSrvID );

	///返回第mapSrvID个MapSrv的监听地址；
	static const char* GetMapSrvAddr( unsigned long mapSrvID );

	///返回第mapSrvID个MapSrv的端口号；
	static const unsigned short GetMapSrvPort( unsigned long mapSrvID );

	///返回第gateSrvID个GateSrv的监听地址；
	static const char* GetGateSrvAddr( unsigned long gateSrvID );

	///返回第gateSrvID个GateSrv的端口号；
	static const unsigned short GetGateSrvPort( unsigned long gateSrvID );

	//返回第rankSrvID个排行榜服务器的监听地址
	static const char* GetRandSrvAddr( unsigned long rankSrvID );

	//返回第rankSrvID个排行榜服务器的端口号
	static const unsigned short GetRandSrvPort( unsigned long rankSrvID );

	///根据字符串形式的服务类型描述得到数字形式的服务类型号
    static bool GetNumType( const char* strType, unsigned short& numType );

	///是否为副本mapsrv;
	static bool IsCopyMapSrv() { return m_isCopyMapSrv; }

	///返回本服务的计划运行时间，返回0表示始终运行；
	static const unsigned long GetRunTime()
	{
		if ( m_srvRunTime > 0 )
		{
			return m_srvRunTime;
		} else {
			return 0;//默认永远运行；
		}
	}

private:
	///读自身配置信息；
	static bool LoadSelfInfo();

	///读所有服务的配置信息；
	static bool LoadAllSrvInfo();

private:
	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
	{
		if ( NULL == pEle->GetAttr( attName ) )
		{
			D_ERROR( "服务器配置XML文件第%d条记录属性%s失败\n", nRecord, attName );
			return false;
		}
		return true;
	}

private:
	//本服务器的类型；
	static unsigned short m_srvType;
	//本服务在同类型服务中的编号；
	static unsigned long  m_srvID;
	//本服务的运行时间(单位:秒)；
	static unsigned long  m_srvRunTime;
	//本服务监听地址；
	static char m_srvAddr[16];
	//本服务监听端口；
	static unsigned short m_srvPort;
    //服务器端对外通信用地址(gatesrv、assignsrv)
	static char m_srvAddr2[16];
	//服务器端对外通信用端口(gatesrv、assignsrv)
	static unsigned short m_srvPort2;

	static bool m_isCopyMapSrv;

	//需要连接的其它服务；
	static vector<OtherSrvConnInfo> m_vecOtherSrvInfo;
};
