﻿/**
* @file LoadSrvConfig.cpp
* @brief 服务配置信息读取
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: LoadSrvConfig.cpp
* 摘    要: 服务配置信息读取
* 作    者: dzj
* 完成日期: 2008.01.03
*
*/

#include "LoadSrvConfig.h"

#include "../Utility.h"

unsigned short CLoadSrvConfig::m_srvType;
unsigned long  CLoadSrvConfig::m_srvID;
char CLoadSrvConfig::m_srvAddr[16];
unsigned short CLoadSrvConfig::m_srvPort;

unsigned long  CLoadSrvConfig::m_srvRunTime;

//服务器端对外通信用地址(gatesrv、assignsrv)
char CLoadSrvConfig::m_srvAddr2[16];
//服务器端对外通信用端口(gatesrv、assignsrv)
unsigned short CLoadSrvConfig::m_srvPort2;

bool CLoadSrvConfig::m_isCopyMapSrv = false;

vector<OtherSrvConnInfo> CLoadSrvConfig::m_vecOtherSrvInfo;

/**
读入服务配置；
selfconfig.xml ： 本服务的类型与序号；
srvconfig.xml ： 整个服务器端各服务的类型与序号；
*/
bool CLoadSrvConfig::LoadConfig()
{
	TRY_BEGIN;

	//初始化成员变量；
	m_srvType = 0xff;
	m_srvID = 0ul;//本服务在同类型服务中的编号；
	m_srvRunTime = 0ul;
	StructMemSet( m_srvAddr, 0, sizeof(m_srvAddr) );//本服务监听地址；
	m_srvPort = 0u;//本服务监听端口；
	StructMemSet( m_srvAddr2, 0, sizeof(m_srvAddr2) );//服务器端对外通信用地址(gatesrv、assignsrv)
	m_srvPort2 = 0;////服务器端对外通信用端口(gatesrv、assignsrv)
	m_vecOtherSrvInfo.clear();	//需要连接的其它服务；

	//先读自身配置信息（自身类型与在同类服务中的序号）；
	if ( ! LoadSelfInfo() )
	{
		return false;
	}

	//再读服务器端的整个配置(各服务的监听地址与端口)；
	if ( ! LoadAllSrvInfo() )
	{
		return false;
	}
 
	return true;
	TRY_END;
	return false;
}

///读自身配置信息；
bool CLoadSrvConfig::LoadSelfInfo()
{
	TRY_BEGIN;

	CXmlManager xmlLoader;
	if(!xmlLoader.Load("config/selfconfig.xml"))
	{
		D_ERROR("读取selfconfig.xml失败\n");
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		D_ERROR("root为空\n");
		return false;
	}

	unsigned int rootLevel =  pRoot->Level();

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if(elementSet.size() != 1)
	{
		D_ERROR("读取不到元素\n");
		return false;
	}

	CElement* tmpEle = NULL;

	vector<CElement*>::iterator tmpiter = elementSet.begin();
	tmpEle = *tmpiter;
	if ( tmpEle->Level() != rootLevel + 1 )
	{
		D_WARNING( "服务配置XML错1\n" );
		return false;
	}

	if ( !IsXMLValValid( tmpEle, "Type", 0 ) )
	{
		return false;
	}		
	if ( ! GetNumType( tmpEle->GetAttr( "Type" ), m_srvType ) )
	{
		D_WARNING( "服务配置XML中Type配置%s错\n", tmpEle->GetAttr( "Type" ) );
		return false;
	}

	//若为mapsrv，则检测是否为副本专用服务器；
	unsigned short mapSrvType = 0;
	GetNumType( "MapSrv", mapSrvType );
	if ( mapSrvType == m_srvType )
	{		
		if ( !IsXMLValValid( tmpEle, "isCopySrv", 0 ) )
		{
			m_isCopyMapSrv = false;			
		} else {
			if ( atoi( tmpEle->GetAttr("isCopySrv") ) > 0 )
			{
				D_DEBUG( "本服务器为副本mapsrv\n" );
				m_isCopyMapSrv = true;
			}
		}
	}

	if ( !IsXMLValValid( tmpEle, "id", 0 ) )
	{
		return false;
	}		
	m_srvID = atoi( tmpEle->GetAttr( "id" ) );

	//if ( IsXMLValValid( tmpEle, "runtime", 0 ) ) //如果没配置此项，则始终运行，否则运行指定时间后退出；
	if ( NULL != tmpEle->GetAttr( "runtime" ) ) //这个属性可以不配，为了去除不配时的“错误”日志，因此不用IsXMLValValid;
	{
		m_srvRunTime = atoi( tmpEle->GetAttr( "runtime" ) );
		D_INFO( "配置的本SRV运行时间:%d\n", m_srvRunTime );
	} else {
		m_srvRunTime = 0;
	}		

	return true;
	TRY_END;
	return false;
}

///读所有服务的配置信息；
bool CLoadSrvConfig::LoadAllSrvInfo()
{
	TRY_BEGIN;

	CXmlManager xmlLoader;
	if(!xmlLoader.Load("config/srvconfig.xml"))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	unsigned int rootLevel =  pRoot->Level();

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if(elementSet.empty())
	{
		return false;
	}

	CElement* tmpEle = NULL;
	OtherSrvConnInfo tmpInfo;

	for ( vector<CElement*>::iterator tmpiter = elementSet.begin(); tmpiter != elementSet.end(); ++tmpiter )
	{
		StructMemSet( tmpInfo, 0, sizeof(tmpInfo) );//初值清空;

		tmpEle = *tmpiter;
		if ( tmpEle->Level() != rootLevel + 1 )
		{
			D_WARNING( "服务配置XML错1\n" );
			return false;
		}

		if ( !IsXMLValValid( tmpEle, "Type", 0 ) )
		{
			return false;
		}		
		if ( ! GetNumType( tmpEle->GetAttr( "Type" ), tmpInfo.srvType ) )
		{
			D_WARNING( "服务配置XML中Type配置%s错\n", tmpEle->GetAttr( "Type" ) );
			return false;
		}		

		if ( !IsXMLValValid( tmpEle, "id", 0 ) )
		{
			return false;
		}	
		tmpInfo.srvID = atoi( tmpEle->GetAttr( "id" ) );
		if ( !IsXMLValValid( tmpEle, "Addr", 0 ) )
		{
			return false;
		}	
		SafeStrCpy( tmpInfo.srvAddr, tmpEle->GetAttr("Addr") );

		if ( !IsXMLValValid( tmpEle, "Port", 0 ) )
		{
			return false;
		}	
		tmpInfo.srvPort = atoi( tmpEle->GetAttr( "Port" ) );

		unsigned short GATESRV_TYPE = 1;
		GetNumType( "GateSrv", GATESRV_TYPE );
		if ( tmpInfo.srvType == GATESRV_TYPE )
		{
			//如果是gatesrv则继续读入对外服务地址与端口；
			if ( !IsXMLValValid( tmpEle, "Addr2", 0 ) )
			{
				return false;
			}	
			SafeStrCpy( tmpInfo.srvAddr2, tmpEle->GetAttr("Addr2") );
			if ( !IsXMLValValid( tmpEle, "Port2", 0 ) )
			{
				return false;
			}	
			tmpInfo.srvPort2 = atoi( tmpEle->GetAttr( "Port2" ) );
		}

		m_vecOtherSrvInfo.push_back( tmpInfo );

		//以下检查该项服务配置是否正好为本服务，如果是，则将相应信息拷入成员变量；
		if ( ( tmpInfo.srvType == m_srvType )
			&& ( tmpInfo.srvID == m_srvID )
			)
		{
			SafeStrCpy( m_srvAddr, tmpInfo.srvAddr );
			m_srvPort = tmpInfo.srvPort;
	
			if ( tmpInfo.srvType == GATESRV_TYPE )
			{
				//如果是gatesrv则拷贝对外服务地址；
				SafeStrCpy( m_srvAddr2, tmpInfo.srvAddr2 );
				m_srvPort2 = tmpInfo.srvPort2;
			}
		}
	}

	return true;
	TRY_END;
	return false;
}

///根据字符串形式的服务类型描述得到数字形式的服务类型号
bool CLoadSrvConfig::GetNumType( const char* strType, unsigned short& numType )
{
	/*
	*	Client			0		C
	*	Gatesrv			1		G
	*	Assignsrv		2		A
	*	Centersrv		3		E	
	*	Loginsrv		4		L
	*	Mapsrv  		5		M
	*	Societysrv		6		O
	*	Dbsrv    		7		D
	*	Shopsrv  		8		S
	*	Runtools  		9		R
	*	Billingsystem	a		B
	*   NewLoginSrv     b       N
	*   functionSrv     c       K
	*   LogSrv          d       O
	*/
	TRY_BEGIN;

	if ( NULL == strType )
	{
		return false;
	}

	if ( 0 == (strcmp( strType, "MapSrv" )) )
	{
		numType = 5;
	} else if ( 0 == (strcmp( strType, "GateSrv" )) ){
		numType = 1;
	} else if ( 0 == (strcmp( strType, "DbSrv" )) ){
		numType = 7;
	} else if ( 0 == (strcmp( strType, "CenterSrv" )) ){
		numType = 3;
	} else if ( 0 == (strcmp( strType, "LoginSrv" )) ){
		numType = 4;
	} else if ( 0 == (strcmp( strType, "RelationSrv" )) ){
		numType = 6;
	}else if ( 0 == (strcmp( strType, "AssignSrv" )) ){
		numType = 2;
	}else if ( 0 == (strcmp( strType, "ShopSrv" )) ){
		numType = 8;
	} else if ( 0 == (strcmp( strType, "NewLoginSrv" )) ){
		numType = 0x0b;
	} else if ( 0 == (strcmp( strType, "FunctionSrv" )) ){
		numType = 0x0c;
	} else if ( 0 == (strcmp( strType, "LogSrv" )) ){
		numType = 0x0d;
	}
	else {
		return false;
	}

	return true;
	TRY_END;
	return false;
}

///返回DbSrv的监听地址；
const char* CLoadSrvConfig::GetDbSrvAddr( unsigned long dbSrvID )
{
	TRY_BEGIN;
	unsigned short srvType;
	GetNumType( "DbSrv", srvType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == srvType )
			&& ( tmpInfo->srvID == dbSrvID )
			)
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回DbSrv的监听端口；
const unsigned short CLoadSrvConfig::GetDbSrvPort( unsigned long dbSrvID )
{
	TRY_BEGIN;
	unsigned short srvType;
	GetNumType( "DbSrv", srvType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == srvType )
			&& ( tmpInfo->srvID == dbSrvID )
			)

		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}


//获取排行榜服务器的地址
const char* CLoadSrvConfig::GetRandSrvAddr( unsigned long rankSrvID )
{
	TRY_BEGIN;

	unsigned short srvType;
	GetNumType("FunctionSrv",srvType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == srvType )
			&& ( tmpInfo->srvID == rankSrvID )
			)
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}


//获取排行榜服务器的端口
const unsigned short CLoadSrvConfig::GetRandSrvPort( unsigned long rankSrvID )
{
	TRY_BEGIN;

	unsigned short srvType;
	GetNumType("FunctionSrv",srvType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == srvType )
			&& ( tmpInfo->srvID == rankSrvID )
			)
		{
			return tmpInfo->srvPort;
		}
	}

	return 0;
	TRY_END;
	return 0;
}



///返回NewLoginSrv的监听地址；
const char* CLoadSrvConfig::GetNewLoginSrvAddr()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "NewLoginSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回NewLoginSrv的监听端口；
const unsigned short CLoadSrvConfig::GetNewLoginSrvPort()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "NewLoginSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}

///返回RelationSrv的监听地址；
const char* CLoadSrvConfig::GetRelationSrvAddr()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "RelationSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回RelationSrv的监听端口；
const unsigned short CLoadSrvConfig::GetRelationSrvPort()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "RelationSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}

///返回shopserver的监听地址；
const char* CLoadSrvConfig::GetShopSrvAddr()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "ShopSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回shopserver的监听端口；
const unsigned short CLoadSrvConfig::GetShopSrvPort()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "ShopSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}

///返回LogSrv的监听地址
const char* CLoadSrvConfig::GetLogSrvAddr()
{
	TRY_BEGIN;
	unsigned short serverType;
	GetNumType( "LogSrv", serverType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == serverType )
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回LogSrv的监听端口
const unsigned short CLoadSrvConfig::GetLogSrvPort()
{
	TRY_BEGIN;
	unsigned short serverType;
	GetNumType( "LogSrv", serverType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == serverType )
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}

///返回AssignSrv的地址；
const char* CLoadSrvConfig::GetAssignSrvAddr()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "AssignSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回AssignSrv的端口；
const unsigned short CLoadSrvConfig::GetAssignSrvPort()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "AssignSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}
///返回CenterSrv的监听地址；
const char* CLoadSrvConfig::GetCenterSrvAddr()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "CenterSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回CenterSrv的监听端口；
const unsigned short CLoadSrvConfig::GetCenterSrvPort()
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "CenterSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( tmpInfo->srvType == mapType )
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}

///返回LoginSrv的监听地址；
const char* CLoadSrvConfig::GetLoginSrvAddr( unsigned long loginSrvID )
{
	TRY_BEGIN;
	unsigned short loginsrvType;
	GetNumType( "LoginSrv", loginsrvType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == loginsrvType )
			&& ( tmpInfo->srvID == loginSrvID )
			)
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回LoginSrv的监听端口；
const unsigned short CLoadSrvConfig::GetLoginSrvPort( unsigned long loginSrvID )
{
	TRY_BEGIN;
	unsigned short loginsrvType;
	GetNumType( "LoginSrv", loginsrvType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == loginsrvType )
			&& ( tmpInfo->srvID == loginSrvID )
			)
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}

///返回第mapSrvID个MapSrv的监听地址；
const char* CLoadSrvConfig::GetMapSrvAddr( unsigned long mapSrvID )
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "MapSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == mapType )
			&& ( tmpInfo->srvID == mapSrvID )
			)
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回第mapSrvID个MapSrv的端口号；
const unsigned short CLoadSrvConfig::GetMapSrvPort( unsigned long mapSrvID )
{
	TRY_BEGIN;
	unsigned short mapType;
	GetNumType( "MapSrv", mapType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == mapType )
			&& ( tmpInfo->srvID == mapSrvID )
			)
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}

///返回第gateSrvID个GateSrv的监听地址；
const char* CLoadSrvConfig::GetGateSrvAddr( unsigned long gateSrvID )
{
	TRY_BEGIN;
	unsigned short gateType;
	GetNumType( "GateSrv", gateType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == gateType )
			&& ( tmpInfo->srvID == gateSrvID )
			)
		{
			return tmpInfo->srvAddr;
		}
	}
	return NULL;
	TRY_END;
	return NULL;
}

///返回第gateSrvID个GateSrv的端口号；
const unsigned short CLoadSrvConfig::GetGateSrvPort( unsigned long gateSrvID )
{
	TRY_BEGIN;
	unsigned short gateType;
	GetNumType( "GateSrv", gateType );
	OtherSrvConnInfo* tmpInfo = NULL;
	for ( vector<OtherSrvConnInfo>::iterator tmpiter=m_vecOtherSrvInfo.begin(); tmpiter!=m_vecOtherSrvInfo.end(); ++tmpiter )
	{
		tmpInfo = &(*tmpiter);
		if ( ( tmpInfo->srvType == gateType )
			&& ( tmpInfo->srvID == gateSrvID )
			)
		{
			return tmpInfo->srvPort;
		}
	}
	return 0;
	TRY_END;
	return 0;
}



