﻿#include "ace/Reactor.h"
#include "TimerManager.h"

CTimerHandler::CTimerHandler(void)
{

}

CTimerHandler::~CTimerHandler(void)
{

}

int CTimerHandler::handle_timeout(const ACE_Time_Value &current_time,
				   const void *act)
{
	ACE_UNUSED_ARG(current_time);
	ACE_UNUSED_ARG(act);
	return 0;
}

CTimerManager::CTimerManager(void)
{

}

CTimerManager::~CTimerManager(void)
{

}

int CTimerManager::RegisterTimer(long lEventType,
								 ACE_Event_Handler *event_handler,
								 const void *arg,
								 const ACE_Time_Value &delay,
								 const ACE_Time_Value &interval)
{
	// 判断eventType:eventHandler是否已经注册？
	if(IsBindHandlerWithEventType(lEventType, event_handler))
		return -1;

	// 注册
	long lTimerID = 0;
	if((lTimerID = ACE_Reactor::instance()->schedule_timer(event_handler, arg, delay, interval)) == -1)
		return -1;

	stTimerInfos timerInfos;
	timerInfos.lEventType = lEventType;
	timerInfos.pHandler = event_handler;

	m_timerList.insert(std::make_pair(lTimerID, timerInfos));
	return lTimerID;
}

int CTimerManager::CancelTimer(long lTimerID)
{
	// 判断指定lTimerID的定时器是否存在？
	if(!ExistTimer(lTimerID))
		return -1;

	// 取消
	if(ACE_Reactor::instance()->cancel_timer(lTimerID) == -1)
		return -1;

	m_timerList.erase(lTimerID);
	return 0;
}

bool CTimerManager::ExistTimer(long lTimerID)
{
	std::map<long, stTimerInfos>::iterator iter = m_timerList.find(lTimerID);
	if(iter == m_timerList.end())
		return false;

	return true;
}

bool CTimerManager::IsBindHandlerWithEventType(long lEventType, ACE_Event_Handler *event_handler)
{
	std::map<long, stTimerInfos>::iterator iter;
	for(iter = m_timerList.begin(); iter != m_timerList.end(); iter++)
	{
		if((iter->second.lEventType == lEventType) && (iter->second.pHandler == event_handler))
			return true;
	}

	return false;
}

