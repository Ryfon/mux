﻿#include "XmlManager.h"
#include "Utility.h"

CElement::CElement(unsigned int uiParentIndex, unsigned int uiIndex, const char *pszName, unsigned int uiLevel, unsigned int uiAttrNum)
: m_uiParentIndex(uiParentIndex), m_uiIndex(uiIndex), m_uiLevel(uiLevel), m_uiAttrNum(uiAttrNum)
{
	ACE_OS::strncpy(m_szName, pszName, sizeof(m_szName) );
	StructMemSet(m_szValue, 0x00, sizeof(m_szValue));
}

CElement::~CElement(void)
{

}
	
const char * CElement::Name(void)
{
	return this->m_szName;
}

unsigned int CElement::AttrNum(void)
{
	return this->m_uiAttrNum;
}

void CElement::AttrNum(unsigned int uiNum)
{
	this->m_uiAttrNum = uiNum;
}

unsigned int CElement::Level(void)
{
	return this->m_uiLevel;
}

unsigned int CElement::ParentIndex(void)
{
	return this->m_uiParentIndex;
}

unsigned int CElement::Index(void)
{
	return this->m_uiIndex;
}	

void CElement::AddAttr(const char* pszAttrName, const char* pszAttrValue)
{
	if((NULL == pszAttrName) || (NULL == pszAttrValue))
		return;
	
	m_attrList.insert(make_pair(std::string(pszAttrName), std::string(pszAttrValue)));
	return;
}

const char* CElement::GetAttr(const char* pszAttrName)
{
	if(NULL == pszAttrName)
		return NULL;

	std::map<std::string, std::string>::iterator iter = m_attrList.find(std::string(pszAttrName));
	if(iter != m_attrList.end())
		return iter->second.c_str();
	else
		return NULL;
}

const char* CElement::Value(void)
{
	return this->m_szValue;
}

void CElement::Value(const char *Value)
{
	if ( NULL == Value )
	{
		return;
	}

	SafeStrCpy(m_szValue, Value);//, sizeof(m_szValue));
	return;
}

CXmlManager::CXmlManager(void)
{
	
}

CXmlManager::~CXmlManager(void)
{
	Clear();
}

bool CXmlManager::Load(const char* pszXmlFile)
{
	if(pszXmlFile == NULL)
		return false;

	CXmlLoadTrace loadHelper(pszXmlFile);

	//清理
	Clear();

#ifdef USE_ACE_SAX_PARSE
	// 文件流
	ACEXML_FileCharStream *pFstm = 0;	
	ACE_NEW_RETURN(pFstm, ACEXML_FileCharStream(), false);
	if(pFstm->open(pszXmlFile) != 0)
		return false;

	// 设置字节流(字节流=文件流)
	ACEXML_CharStream *pStm = pFstm;
	ACEXML_InputSource input(pStm);/*pFstm会在ACEXML_InputSource中被隐含delete*/

	// sax事件处理器
	ACEXML_DefaultHandler *pHandler = 0;
	ACE_NEW_RETURN(pHandler, CXmlEventHandler(pszXmlFile, m_elementSet), false);
	auto_ptr<ACEXML_DefaultHandler> handlerAutoClean(pHandler);

	// 解析器
	ACEXML_Parser parser;
	parser.setContentHandler(pHandler);
	parser.setDTDHandler(pHandler);
	parser.setErrorHandler(pHandler);
	parser.setEntityResolver(pHandler);

	ACEXML_DECLARE_NEW_ENV;

	ACEXML_TRY_EX(FIRST)
	{
		// 解析
		parser.parse(&input ACEXML_ENV_ARG_PARAMETER);
		ACEXML_TRY_CHECK_EX(FIRST);
	}
	ACEXML_CATCH(ACEXML_Exception, ex)
	{
		ex.print();
	}
	ACEXML_ENDTRY;

	/*ACEXML_TRY_EX(SECOND)
	{
		// 解析
		parser.parse(&input ACEXML_ENV_ARG_PARAMETER);
		ACEXML_TRY_CHECK_EX(SECOND);
	}
	ACEXML_CATCH(ACEXML_SAXException,ex)
	{
		ex.print();
		return false;
	}
	ACEXML_ENDTRY;*/
#else
	TiXmlDocument xmlConfigFile;
	bool bResult = xmlConfigFile.LoadFile(pszXmlFile);
	if(!bResult)
	{
		D_ERROR("tinyxml读取文件%s失败\n", pszXmlFile);
		return false;
	}

	TiXmlElement* pRoot = xmlConfigFile.RootElement();
	if(NULL == pRoot)
	{
		D_ERROR("xml文件%s中root为空\n", pszXmlFile);
		return false;
	}

	unsigned int uiSelfIndex = 0;
	Parse(pRoot, 0, uiSelfIndex, 0);
	

#endif/*USE_ACE_SAX_PARSE*/
	
	return true;
}

bool CXmlManager::Reload(const char* pszXmlFile)
{
	return Load(pszXmlFile);
}

void CXmlManager::Clear(void)
{
	std::vector<CElement*>::iterator iter;
	for(iter = m_elementSet.begin(); iter != m_elementSet.end(); iter++)
	{
		delete *iter;
	}

	m_elementSet.clear();
}

CElement * CXmlManager::Root(void)
{
	if ( m_elementSet.empty() )
		return NULL;
	else if( m_elementSet[0]->Index() == 0)
		return m_elementSet[0];
	else
		return NULL;
}

void CXmlManager::FindChildElements(CElement *pParent, std::vector<CElement *> &childElements)
{
	unsigned int uiIndex,uiLevel;

	// 有效性
	if(pParent == NULL)
		return;

	uiIndex = pParent->Index();
	if(uiIndex >= m_elementSet.size())
		return;

	uiLevel = pParent->Level();
	for(unsigned int i = uiIndex + 1; i < m_elementSet.size(); i++ )
	{
		if(m_elementSet[i]->Level() <= uiLevel)
			break;

		childElements.push_back(m_elementSet[i]);
	}

}
void CXmlManager::FindChildElements(unsigned int uiParentIndex, std::vector<CElement *> &childElements)
{
	// 索引的有效性
	if(uiParentIndex >= m_elementSet.size())
		return;

	std::vector<CElement*>::iterator iter;
	for(iter = m_elementSet.begin(); iter != m_elementSet.end(); iter++)
	{

		if((*iter)->ParentIndex() == uiParentIndex)
			childElements.push_back(*iter);
	}

	return;
}

void CXmlManager::FindSameLevelElements(unsigned int uiLevel, std::vector<CElement *> &childElements)
{
	std::vector<CElement*>::iterator iter;
	for(iter = m_elementSet.begin(); iter != m_elementSet.end(); iter++)
	{
		if((*iter)->Level() == uiLevel)
			childElements.push_back(*iter);
	}

	return;
}

#ifndef USE_ACE_SAX_PARSE

bool CXmlManager::Parse(const TiXmlElement *pXmlElement, unsigned int uiParentIndex, unsigned int &uiSelfIndex, unsigned int uiLevelIndex)
{
	if(NULL == pXmlElement)
	{
		printf("pXmlElement为空\n");
		return true;
	}

	// 初始化并插入自身
	CElement *pElement = NEW CElement(uiParentIndex, uiSelfIndex, pXmlElement->Value(), uiLevelIndex, 0);
	
	if(NULL != pElement)
	{		
		unsigned int uiAttrNum = 0;
		const TiXmlAttribute *pAttribute = pXmlElement->FirstAttribute();/// 属性列表
		while(NULL != pAttribute)
		{
			pElement->AddAttr(pAttribute->Name(), pAttribute->Value());

			pAttribute = pAttribute->Next();
			uiAttrNum++;
		}

		pElement->AttrNum(uiAttrNum);///属性数目
		pElement->Value(pXmlElement->GetText());/// 文本

		//插入
		m_elementSet.push_back(pElement);

		//唯一索引自加
		uiSelfIndex++;
	}

	//如果有子元素，遍历子元素
	const TiXmlElement* pXmlChild = pXmlElement->FirstChildElement();
	for(; NULL != pXmlChild; pXmlChild = pXmlChild->NextSiblingElement())
	{
		Parse(pXmlChild, pElement->Index(), uiSelfIndex, pElement->Level() + 1);
	}

	return true;
}

#endif/*USE_ACE_SAX_PARSE*/

CXmlManager::CXmlLoadTrace::CXmlLoadTrace(const char *pszName)
	:m_pszName(pszName)
{
	if(NULL != m_pszName)
		D_INFO("配置文件%s读取开始...\n", m_pszName);
}

CXmlManager::CXmlLoadTrace::~CXmlLoadTrace(void)
{
	if(NULL != m_pszName)
		D_INFO("配置文件%s读取结束\n", m_pszName);
}
