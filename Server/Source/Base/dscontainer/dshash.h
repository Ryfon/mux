﻿/*
  简单hash值计算类，准备用于self_hashvector与self_list, by dzj, 08.09.09;
*/

//产生指定整数范围内的对应输入值的hash值,输出范围包括边界,返回值：是否成功产生该hash值;
bool NumRegionHash( const unsigned int lowEdge, const unsigned int upEdge
				  , const unsigned int inValue, unsigned int & hashValue );
