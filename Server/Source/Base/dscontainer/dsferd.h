﻿/*
  经常遍历、很少删除、没有查找的无序数组，准备用于遍历玩家, by dzj, 08.11.07;
*/

#include "../Utility.h"

template <typename T_Inner, unsigned int MAX_INNER_NUM >
class DsFerd
{
private:
	DsFerd( const DsFerd& );  //屏蔽这两个操作；
	DsFerd& operator = ( const DsFerd& );//屏蔽这两个操作；

public:
	DsFerd() 
	{
		m_arrOfPtr = NEW T_Inner*[MAX_INNER_NUM];
		for ( int i=0; i<MAX_INNER_NUM; ++i )
		{
			m_arrOfPtr[i] = NULL;
		}
		m_curNum = 0;//当前队尾；
	};
	~DsFerd() 
	{
		if ( NULL != m_arrOfPtr )
		{
			delete [] m_arrOfPtr; m_arrOfPtr = NULL;
		}
		m_curNum = 0;
	};

public:
	T_Inner** GetInnerArr()
	{
		return m_arrOfPtr;
	}

	int GetCurTail()
	{
		return m_curNum;
	}

	///如果已经插入，则返回真，否则返回假；
	bool InsertEle( T_Inner* pToInsert )
	{
		if ( m_curNum >= MAX_INNER_NUM )
		{
			//存放不下；
			return false;
		}
		m_arrOfPtr[m_curNum] = pToInsert;//元素放入最后；
		m_arrOfPtr[m_curNum]->SetFerdPos( this, m_curNum );
		++m_curNum;//队尾后移；
		return true;
	}

	///如果确实删去了元素，则返回真，否则返回假；
	bool DeleteEleAtPos( int iPos )
	{
		if ( ( iPos < 0 ) || ( m_curNum < 0 ) )
		{
			return false;
		}
		if ( 0 == m_curNum )
		{
			return false;
		}
		if ( iPos >= m_curNum)
		{
			return false;
		}
		m_arrOfPtr[iPos]->SetFerdPos( NULL, -1 );//清该元素指向本ferd数组的信息；
		if ( iPos != m_curNum-1 )
		{
			//非队尾元素；
			m_arrOfPtr[iPos] = m_arrOfPtr[m_curNum-1];//用最后一个元素替换当前元素；
			m_arrOfPtr[iPos]->SetFerdPos( this, iPos );//刷新置换过来的元素(原来的最后一个元素)指向本ferd数组的信息；
		}
		m_arrOfPtr[m_curNum-1] = NULL;//删去最后一个元素；
		--m_curNum;
		return true;
	}

private:
	T_Inner** m_arrOfPtr;//指针数组；
	int       m_curNum;//当前有效元素数目（或当前队尾--有效元素的后一个）
};

//确保不要在循环过程中删元素，如需删除，则在循环过程中记下待删元素，循环过后另删；
#define FERDFOR( T_Ele, ferdArr, tmpPtr ) \
  {\
	T_Ele* tmpPtr = NULL;\
	T_Ele** arrOfPtr = ferdArr->GetInnerArr();\
	int      curTail = ferdArr->GetCurTail();\
	for ( int i=0; i<curTail; ++i )\
	{\
		tmpPtr = arrOfPtr[i];\
		if ( NULL == tmpPtr )\
		{\
			break;\
		}

#define ENDFERDFOR \
	} \
  }


