﻿
#include "dshash.h"


//产生指定整数范围内的对应输入值的hash值,输出范围包括边界,返回值：是否成功产生该hash值;
bool NumRegionHash( const unsigned int lowEdge, const unsigned int upEdge
				  , const unsigned int inValue, unsigned int & hashValue )
{
	if ( upEdge < lowEdge )
	{
		return false;
	}
	if ( upEdge == lowEdge )
	{
		hashValue = upEdge;
		return true;
	}

	//Robert Jenkins's 32 bit integer hash function, from: http://www.concentric.net/~Ttwang/tech/inthash.htm;
	hashValue = (inValue+0x7ed55d16) + (inValue<<12);
	hashValue = (hashValue^0xc761c23c) ^ (hashValue>>19);
	hashValue = (hashValue+0x165667b1) + (hashValue<<5);
	hashValue = (hashValue+0xd3a2646c) ^ (hashValue<<9);
	hashValue = (hashValue+0xfd7046c5) + (hashValue<<3);
	hashValue = (hashValue^0xb55a4f09) ^ (hashValue>>16);

	hashValue = lowEdge + hashValue % (upEdge-lowEdge);//平均散列至目标区间；

	return true;
}

