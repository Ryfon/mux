﻿#ifndef ACCEPTOR_CPP
#define ACCEPTOR_CPP

#include "Acceptor.h"
#include "Utility.h"

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::CAcceptor(void)
	:m_pReadQueue(NULL), m_pSendQueue(NULL)
{

}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::CAcceptor(IBufQueue *pReadQueue, IBufQueue *pSendQueue, unsigned int uiPeerBufferSize)
	: m_pReadQueue(pReadQueue), m_pSendQueue(pSendQueue),m_uiPeerBufferSize(uiPeerBufferSize)
{
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::~CAcceptor(void)
{

}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
int CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::open(const ACE_PEER_ACCEPTOR_ADDR &local_addr, ACE_Reactor *reactor,int flags, int use_select, int reuse_addr )
{
	if(parent::open(local_addr, reactor, flags, use_select, reuse_addr) == -1)
		return -1;

	// 设置recvbuf,sendbuf
	int iOldBuf = 0;
	int iNewbuf = 512* 1024;
	int optLen = sizeof(iOldBuf);

	int i = this->acceptor().get_option(SOL_SOCKET, SO_SNDBUF, &iOldBuf, &optLen);
	
	i = this->acceptor().set_option(SOL_SOCKET, SO_RCVBUF, &iNewbuf, sizeof(iNewbuf));
	i = this->acceptor().set_option(SOL_SOCKET, SO_SNDBUF, &iNewbuf, sizeof(iNewbuf));
	
	i = this->acceptor().get_option(SOL_SOCKET, SO_SNDBUF, &iOldBuf, &optLen);

	return 0;
	
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
int CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::make_svc_handler(SVC_HANDLER *&sh)
{
	if((m_pReadQueue == NULL)|| (m_pSendQueue == NULL))
		return -1;

	if(sh == 0)
		ACE_NEW_RETURN (sh, SVC_HANDLER(m_uiPeerBufferSize), -1);

	sh->reactor (this->reactor ());

	// 设置队列
	sh->ReadQueue(this->ReadQueue());
	sh->SendQueue(this->SendQueue());

	return 0;
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
IBufQueue * CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::ReadQueue(void)
{
	return m_pReadQueue;
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
void CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::ReadQueue(IBufQueue *pReadQueue)
{
	m_pReadQueue = pReadQueue;
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
IBufQueue * CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::SendQueue(void)
{
	return m_pSendQueue;
}

template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1>
void CAcceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>::SendQueue(IBufQueue *pSendQueue)
{
	m_pSendQueue = pSendQueue;
}

#endif/*ACCEPTOR_CPP*/
