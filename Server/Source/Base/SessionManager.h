﻿#ifndef SESSION_MANAGER_H
#define SESSION_MANAGER_H

#include "./BufQueue/BufQueue.h"
#include "./BufQueue/BufQueueImp.h"
#include <set>

template <class SVC_HANDLER, class ACE_LOCK>
class CHandlerManagerEx
{
	enum
	{
		SESSION_ID_START = 1000
	};

public:
	CHandlerManagerEx(void);
	
	~CHandlerManagerEx(void);

public:
	int Open(unsigned int uiInitSize);

	SVC_HANDLER * GetHandler(unsigned int uiSessionID = 0);

	void Handlers(std::set<SVC_HANDLER *>& handlers);

private:
	/// 初始化数目
	unsigned int m_uiInitSize;

	/// 
	SVC_HANDLER *m_pHandlers;

	///上次得到的处理器的索引
	unsigned int m_uiLastHandlerIndex;	

	/// 上次得到的sessionID
	unsigned int m_uiLastSessionID;

	/// 
	std::set<SVC_HANDLER *> m_handlerSet;

	/// 
	ACE_LOCK m_mutex;
};


template <class SVC_HANDLER, ACE_PEER_ACCEPTOR_1, class ACE_LOCK>
class CAcceptorEx : public ACE_Acceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2>
{
public:
	/// 
	typedef ACE_Acceptor<SVC_HANDLER, ACE_PEER_ACCEPTOR_2> parent;

	/// 缺省构造
	//CAcceptorEx(void);

	/// 构造
	CAcceptorEx(IBufQueue *pReadQueue, IBufQueue *pSendQueue,unsigned int uiPeerBufferSize, CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *pHandlerManager);

	/// 析构
	virtual ~CAcceptorEx(void);

public:
	/// 初始化
	virtual int open (const ACE_PEER_ACCEPTOR_ADDR &local_addr, ACE_Reactor *reactor,
		int flags = 0, int use_select = 1, int reuse_addr = 1);

	/// 重写handler的获取规则
	virtual int make_svc_handler(SVC_HANDLER *&sh);


	/// 获取/设置队列
	/// 获取已读队列
	IBufQueue * ReadQueue(void);

	/// 设置已读队列
	void ReadQueue(IBufQueue *pReadQueue);

	/// 获取待发队列
	IBufQueue * SendQueue(void);

	/// 设置待发队列
	void SendQueue(IBufQueue *pSendQueue);

private:
	/// 已读消息队列 
	IBufQueue *m_pReadQueue;

	/// 待发消息队列
	IBufQueue *m_pSendQueue;

	/// 连接端发送缓存
	unsigned int m_uiPeerBufferSize;

	/// 处理器管理器
	CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *m_pHandlerManager;
};

template <class SVC_HANDLER, ACE_PEER_CONNECTOR_1, class ACE_LOCK>
class CConnectorEx : public ACE_Connector<SVC_HANDLER, ACE_PEER_CONNECTOR_2>
{
public:
	typedef ACE_Connector<SVC_HANDLER, ACE_PEER_CONNECTOR_2> parent;

	/// 缺省构造
	//CConnectorEx(void);

	/// 构造
	CConnectorEx(IBufQueue *pReadQueue, IBufQueue *pSendQueue, unsigned int uiPeerBufferSize,CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *pHandlerManager);

	/// 析构
	virtual ~CConnectorEx(void);

public:
	/// 重写handler的获取规则
	virtual int make_svc_handler(SVC_HANDLER *&sh);

	/// 获取/设置队列
	/// 获取已读队列
	IBufQueue * ReadQueue(void);

	/// 设置已读队列
	void ReadQueue(IBufQueue *pReadQueue);

	/// 获取待发队列
	IBufQueue * SendQueue(void);

	/// 设置待发队列
	void SendQueue(IBufQueue *pSendQueue);

private:
	/// 已读消息队列 
	IBufQueue *m_pReadQueue;

	/// 待发消息队列
	IBufQueue *m_pSendQueue;

	/// 连接端发送缓存
	unsigned int m_uiPeerBufferSize;

	/// 处理器管理器
	CHandlerManagerEx<SVC_HANDLER, ACE_LOCK> *m_pHandlerManager;
};

#if defined(ACE_TEMPLATES_REQUIRE_SOURCE)
#include "SessionManager.cpp"
#endif

#if defined(ACE_TEMPLATES_REQUIRE_PRAGMA)
#pragma implementation("SessionManager.cpp")
#endif

#endif/*SESSION_MANAGER_H*/
