﻿/** @file TimerManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2007-10-15
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef TIMER_MANAGER_H
#define TIMER_MANAGER_H

#include "ace/Event_Handler.h"
#include <map>
/**
* @class CTimerHandler
*
* @brief 定时器关联的处理器
*
* 定义需要定时执行的操作
*/
class CTimerHandler : public ACE_Event_Handler
{
public:
	/// 构造
	CTimerHandler(void);

	/// 析构
	virtual ~CTimerHandler(void);

	/** 
	* 超时时被框架回调. <current_time>是<CHandler>因超时被分派的时间，
	* <act>是在<schedule_timer>时传进的参数
	*/	
	virtual int handle_timeout(const ACE_Time_Value &current_time,
							   const void *act = 0);
};

/**
* @class CTimerManager
*
* @brief 定时器管理模块
* 
* 集中管理和网络事件有关的定时器
*/
class CTimerManager
{
public:
	/// 构造
	CTimerManager(void);

	/// 析构
	~CTimerManager(void);

public:
	/// 注册一个定时器
	int RegisterTimer(long lEventType,
					 ACE_Event_Handler *event_handler,
					 const void *arg,
				     const ACE_Time_Value &delay,
					 const ACE_Time_Value &interval = ACE_Time_Value::zero);

	/// 取消一个定时器
	int CancelTimer(long lTimerID);

	/// 判断定时器是否存在?
	bool ExistTimer(long lTimerID);

private:
	/// 判断EventHandler是否已和EventType关联且注册?
	bool IsBindHandlerWithEventType(long lEventType, ACE_Event_Handler *event_handler);
private:
	/// 定时器详细信息
	struct stTimerInfos
	{
		long lEventType;
		ACE_Event_Handler* pHandler;
	};

	/// 定时器列表
	std::map<long, stTimerInfos> m_timerList;

};



#endif/*TIMER_MANAGER_H*/
