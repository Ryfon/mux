﻿/**
* @file bufqueue.h
* @brief 定义缓冲队列接口
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: bufqueue.h
* 摘    要: 定义缓冲队列接口
* 作    者: dzj
* 完成日期: 2007.11.20
*
*/

#pragma once

#include "../aceall.h"
#include "../PkgProc/MsgToPut.h" //与应用之间的传递消息结构定义;

//#ifdef ACE_WIN32
////#define GetLastError() errno
//typedef ACE_Auto_Event READ_SIG_TYPE; //linux操作系统信号类型；
//#else //ACE_WIN32;
typedef int READ_SIG_TYPE;
//typedef HANDLE READ_SIG_TYPE;//windows系统事件类型；
////#include "ace\Manual_Event.h"
////typedef ACE_Manual_Event READ_SIG_TYPE;
//#endif //ACE_WIN32;

#ifdef ACE_WIN32
const int MYSIGREAD = SIGINT;
#else //ACE_WIN32
const int MYSIGREAD = SIGUSR1;
#endif //ACE_WIN32



///缓冲队列接口，模板参数:
///READ_SIG_TYPE:信号量类型，其定义的信号量指示缓冲中是否有消息可读；
class IBufQueue
{
public:
	virtual ~IBufQueue() {};
public:
	//////////////////////////////////////////////////////////////////////////
	///写信息使用
	///将信息放入缓冲；
	virtual int PushMsg( MsgToPut* pMsg ) = 0;
	/**
	* 置可读信号，促使等待读线程进行读操作，由写信息线程在特定时刻调用；
	* 本函数仅在this用作主逻辑线程向网络线程发消息缓冲时有用,
	* 用作网络线程发消息到主逻辑线程缓冲时不必使用，因为主逻辑线程会定时进行读网络消息的操作；
	*/
	virtual int SetReadEvent( bool isForce/*是否强制设置读事件，用于终止读线程*/ ) = 0; 
	///调用以等待读信号；
	virtual void WaitReadEvent() = 0;

	///调用以等待读信号或者超时,等到信号时isReadConFulfil=true，否则,如果是超时返回，则isReadConFulfil=false；
    virtual void WaitReadEventByTimeOut( bool& isReadConFulfil ) = 0;
	///写信息使用
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	///读信息使用
	/**
	* 准备进行一系列读，应在每次执行一系列读操作之前进行一次
	* 内部操作为:将之前填满的输入队列变成输出队列，同时将之前空的输出队列转成新的输入队列；
	* 例：
	    pBufQueue->PreSerialRead();
		pMsg = pBufQueue->PopMsg();
	    while ( NULL != pMsg )
	    {
	        ...process pMsg;
	        delete pMsg; 
			pMsg = pBufQueue->PopMsg();
	    }
	    ...other process;
	*/
	virtual list<MsgToPut*>* PreSerialRead(  list<MsgToPut*>* inQueue ) = 0;

	/////使用输入队列将输出队列交换出来供无需加锁的读；
	//virtual list<MsgToPut*>* SwitchOutMsgQueue( list<MsgToPut*>* inQueue ) = 0;

	/**
	* 清可读信号，防止反复收到可读消息；
	* 本函数仅在this用作主逻辑线程向网络线程发消息缓冲时有用,由网络读信号响应函数调用；
	* 用作网络线程发消息到主逻辑线程缓冲时不必使用，因为主逻辑线程会定时进行读网络消息的操作；
	*/
	virtual int ResetReadEvent() = 0;
	///从缓冲中取信息；
	virtual MsgToPut* PopMsg() = 0;
	///读信息使用
	//////////////////////////////////////////////////////////////////////////

	///取缓冲可读信号量；
	virtual READ_SIG_TYPE& GetReadEvent() = 0;
};

