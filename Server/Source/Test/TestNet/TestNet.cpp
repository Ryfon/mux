﻿#include "../../Base/NetTask.h"
#include "../../Base/Utility.h"
#include "../../Base/BufQueue/BufQueue.h"
#include "../../Base/PkgProc/MsgToPut.h"
#include "TestHandler.h"
#include "ace/OS.h"
#include "ace/Log_Msg.h"
#include "ace/Get_Opt.h"
#include <fstream>


TCache< MsgToPut >* g_poolMsgToPut;

static int print_usage (int argc, ACE_TCHAR *argv[])
{
	ACE_UNUSED_ARG(argc);
	ACE_UNUSED_ARG(argv);

	D_ERROR(ACE_TEXT ("\nusage: %s")
			ACE_TEXT ("\n-d <run as daemon process (no windows system)>")
			ACE_TEXT ("\n"),
			argv[0]);

	return 0;
}

static int parse_args(int argc, ACE_TCHAR *argv[])
{
	// 无参数直接返回
	if (argc == 1)
		return 0;

	// 解析参数
	int c;
	ACE_Get_Opt get_opt(argc, argv, ACE_TEXT ("d"));
	
	while((c = get_opt()) != EOF)
	{
		switch (c)
		{
		case 'd':
			return 1;

		default:
			return print_usage(argc,argv);
		}
	}

	return 0;
}

int ACE_TMAIN(int argc, ACE_TCHAR *argv[])
{	
	// 使自己成为守护进程（现在有错，待进一步调试）
//#if !defined(ACE_WIN32)
//	if(parse_args(argc, argv) == 1)
//		ACE::daemonize();
//#endif/*ACE_WIN32*/

	// 设置日志输出到文件
	//ACE_LOG_MSG->clr_flags (ACE_Log_Msg::STDERR);
	ACE_LOG_MSG->set_flags (ACE_Log_Msg::OSTREAM);

	const char *filename = "output";
	ofstream outfile (filename, ios::out | ios::trunc);
	ACE_LOG_MSG->msg_ostream (&outfile);
	
	g_poolMsgToPut = new TCache< MsgToPut >( 3 );
	IBufQueue* pReadNetMsgQueue = new CBufQueue;//从此队列读网络消息；
	IBufQueue* pSendNetMsgQueue = new CBufQueue;//向此队列写网络消息；

	// 激活任务
	ACE_INET_Addr addr(9999);
	CNetTask<CTestHandler> myTask(pReadNetMsgQueue, pSendNetMsgQueue);
	if(myTask.open((void*)&addr) == -1)
	{
		D_DEBUG("mytask activate error.\n");
		return 0;
	}

	//以下主线程继续工作，其间通过pWriteQueue、pReadQueue与网络线程通信；
	for ( int i = 0; i < 3000000; ++i )
	{
		//ACE_OS::sleep(5);

		MsgToPut * pMsg = pReadNetMsgQueue->PopMsg();//读网络消息直至网络队列空；
		bool isexistsendmsg = false;
		while ( NULL != pMsg )
		{
			//处理到达的网络消息；
			pSendNetMsgQueue->PushMsg( pMsg );//echo；
			isexistsendmsg = true;
			pMsg = pReadNetMsgQueue->PopMsg();
		}
		pReadNetMsgQueue->PreSerialRead();//准备读；
		pMsg = pReadNetMsgQueue->PopMsg();//读网络消息直至网络队列空；
		isexistsendmsg = false;
		while ( NULL != pMsg )
		{
			//处理到达的网络消息；
			pSendNetMsgQueue->PushMsg( pMsg );//echo；
			isexistsendmsg = true;
			pMsg = pReadNetMsgQueue->PopMsg();
		}
		if (isexistsendmsg)
		{
			pSendNetMsgQueue->SetReadEvent();
		}
	}

	// 通知任务结束
	//myTask.quit();

	// 等待任务结束
	myTask.wait();

	//删发送与接收缓冲；
	if ( NULL != pReadNetMsgQueue )
	{
		delete pReadNetMsgQueue; 
		pReadNetMsgQueue = NULL;
	}
	if ( NULL != pSendNetMsgQueue )
	{
		delete pSendNetMsgQueue; 
		pSendNetMsgQueue = NULL;
	}

	if ( NULL != g_poolMsgToPut )
	{
		delete g_poolMsgToPut; g_poolMsgToPut = NULL;
	}

	return 0;
}

