﻿#include "ConfigInfoManager.h"
#include "../../Base/XmlManager.h"
#include "../../Base/Utility.h"
#include <vector>


CRoleInitManager::CRoleInitManager(void)
{
}

CRoleInitManager::~CRoleInitManager(void)
{
	clear();
}

int CRoleInitManager::init(const char* pszFileName)
{
	clear();

	CXmlManager xmlManager;
	if(!xmlManager.load(pszFileName))
	{
		D_ERROR("玩家初始化文件加载出错");
		return -1;
	}

	CElement *pRoot = xmlManager.root();
	if(NULL == pRoot)
	{
		D_ERROR("配置文件可能为空");
		return -1;
	}

	std::vector<CElement *> childElements;
	xmlManager.findChildElements(pRoot, childElements);

	unsigned short usClass = 0;
	for(std::vector<CElement *>::iterator i = childElements.begin(); i != childElements.end(); i++)
	{
		if((*i)->level() == 1)
		{
			InitRoleInfo *pInitRoleInfo = new InitRoleInfo;
			if(pInitRoleInfo == NULL)
			{
				D_ERROR("初始化InitRoleInfo时出错");
				return -1;
			}
			
			pInitRoleInfo->usMapID = atoi((*i)->getAttr("MapID"));
			pInitRoleInfo->iPosX = atoi((*i)->getAttr("PosX"));
			pInitRoleInfo->iPosY = atoi((*i)->getAttr("PosY"));
			pInitRoleInfo->ucLevel = atoi((*i)->getAttr("Level"));
			pInitRoleInfo->uiHP = atoi((*i)->getAttr("HP"));
			pInitRoleInfo->uiMP = atoi((*i)->getAttr("MP"));
			pInitRoleInfo->uiMoney = atoi((*i)->getAttr("Money"));
			pInitRoleInfo->uiSkill1ID = atoi((*i)->getAttr("Skill1ID"));
			pInitRoleInfo->uiSkill2ID = atoi((*i)->getAttr("Skill2ID"));

			// 插入
			m_playerInitInfos[usClass] = pInitRoleInfo;
			usClass++;
		}		
	}

	return 0;
}

InitRoleInfo * CRoleInitManager::find(unsigned short usClass)
{
	std::map<unsigned short, InitRoleInfo *>::iterator i = m_playerInitInfos.find(usClass);
	if(i == m_playerInitInfos.end())
		return NULL;
	else
		return i->second;
}

int CRoleInitManager::clear(void)
{
	for(std::map<unsigned short, InitRoleInfo *>::iterator i = m_playerInitInfos.begin();
		i != m_playerInitInfos.end(); i++)
	{
		delete i->second;
		i->second = NULL;
	}

	m_playerInitInfos.clear();
	
	return 0;
}
