﻿#include "RobotPlayerInfo.h"
#include "ConfigInfoManager.h"
#include "../../Base/MysqlWrapper.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/XmlManager.h"
#include "../../Base/Utility.h"
#include "ace/OS.h"
#include <sstream>
#include <string>
#include <time.h>


using namespace MUX_PROTO;

#pragma pack(push, 1)

typedef struct StrFullPlayerInfo	/// 完整的玩家信息  
{
	PlayerInfo playerInfo;
	PkgData items;
}FullPlayerInfo;

#pragma pack(pop)


int CRobotPlayerInfo::insertPlayerInfo(const char *pszAccountPrefix, unsigned int uiNum)
{
	static int pos[10][2] = {
		{46, 170}, {96, 166}, {96, 124}, {98, 90}, {46, 93},
		{31, 52},  {132, 82}, {163, 103},{170, 58},{143, 35}
	};

	srand((unsigned) time(NULL));

	// 玩家角色信息初始化
	FullPlayerInfo fullPlayerInfo;
	ACE_OS::memset(&fullPlayerInfo, 0x00, sizeof(fullPlayerInfo));

	InitRoleInfo* pIntiRoleInfo = CRoleInitManagerSingle::instance()->find(0);
	if(pIntiRoleInfo == NULL)
		return false;

	char szAccount[32], szName[32];
	for(unsigned int i = 0; i < uiNum; i++)
	{
		ACE_OS::snprintf(szAccount, sizeof(szAccount), "%s%d", pszAccountPrefix, i);
		ACE_OS::snprintf(szName, sizeof(szName), "%s_n_%d", pszAccountPrefix, i);
		
		ACE_OS::memcpy(fullPlayerInfo.playerInfo.szAccount, szAccount, sizeof(fullPlayerInfo.playerInfo.szAccount));
		ACE_OS::memcpy(fullPlayerInfo.playerInfo.szNickName, szName, sizeof(fullPlayerInfo.playerInfo.szNickName));
		fullPlayerInfo.playerInfo.usNation = 0;
		fullPlayerInfo.playerInfo.bSex = 0;
		fullPlayerInfo.playerInfo.usClass = 0;
		fullPlayerInfo.playerInfo.equipData.uiEquips[0] = 0;
		fullPlayerInfo.playerInfo.equipData.uiEquips[1] = 0;
		fullPlayerInfo.playerInfo.equipData.uiEquips[2] = 1;
		fullPlayerInfo.playerInfo.equipData.uiEquips[3] = 2;
		fullPlayerInfo.playerInfo.equipData.uiEquips[4] = 3;
		fullPlayerInfo.playerInfo.equipData.uiEquips[5] = 4;

		unsigned short usTemp = rand()%10;
		fullPlayerInfo.playerInfo.usMapID = pIntiRoleInfo->usMapID;
		fullPlayerInfo.playerInfo.iPosX = pos[usTemp][0];
		fullPlayerInfo.playerInfo.iPosY = pos[usTemp][1];
		fullPlayerInfo.playerInfo.usLevel = pIntiRoleInfo->ucLevel;
		fullPlayerInfo.playerInfo.uiHP = pIntiRoleInfo->uiHP;
		fullPlayerInfo.playerInfo.uiMP = pIntiRoleInfo->uiMP;
		fullPlayerInfo.playerInfo.uiMoney = pIntiRoleInfo->uiMoney;
		fullPlayerInfo.playerInfo.skillData.uiSkillID[0] = pIntiRoleInfo->uiSkill1ID;
		fullPlayerInfo.playerInfo.skillData.uiSkillID[1] = pIntiRoleInfo->uiSkill2ID;

		// 插入玩家信息
		if(insertPlayerInfo_i((const char *)&fullPlayerInfo, sizeof(fullPlayerInfo)) == -1)
			D_DEBUG("创建账号为%s的角色信息出错", szAccount);
	}
}

int CRobotPlayerInfo::insertPlayerInfo_i(const char *pszData, unsigned int uiDataLen)
{
	if(pszData == NULL || uiDataLen != sizeof(FullPlayerInfo))
		return -1;

	int iRet = 0;
	FullPlayerInfo *pFullPlayerInfo = (FullPlayerInfo *)pszData;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->getConnection();
	if(pConnection == NULL)
		return -1;

	CQuery *pQuery = pConnection->createQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char escapeFullPlayerData[2 * sizeof(FullPlayerInfo) + 1] = {0};
	pQuery->realEscape(escapeFullPlayerData, (const char *)pFullPlayerInfo, sizeof(FullPlayerInfo)); // 转义
	char escapteEquips[2 * sizeof(EquipData) + 1] = {0};
	pQuery->realEscape(escapteEquips, (const char *)&pFullPlayerInfo->playerInfo.equipData, sizeof(EquipData)); // 转义

	std::stringstream ss;
	ss << "INSERT INTO playerinfo (Account, Nickname, Level, Nation, Class, Equips, MapID, PosX, PosY, FullData) VALUES("
		<< "'" << pFullPlayerInfo->playerInfo.szAccount << "',"
		<< "'" << pFullPlayerInfo->playerInfo.szNickName << "',"
		<< pFullPlayerInfo->playerInfo.usLevel << ","
		<< (unsigned short)(pFullPlayerInfo->playerInfo.usNation) << ","
		<< (unsigned short)(pFullPlayerInfo->playerInfo.usClass) << ","
		<< "'" << escapteEquips << "',"
		<< pFullPlayerInfo->playerInfo.usMapID << ","
		<< pFullPlayerInfo->playerInfo.iPosX << ","
		<< pFullPlayerInfo->playerInfo.iPosY << ","
		<< "'" << escapeFullPlayerData << "')";

	std::string statement = ss.str();

	// 执行查询
	iRet = pQuery->executeUpdate(statement.c_str(), statement.size());

	// 销毁查询
	pConnection->destroyQuery(pQuery);
	
	return 0;
}
