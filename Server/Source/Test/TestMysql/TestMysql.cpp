﻿#include "../../Base/MysqlWrapper.h"
#include "../../Base/PkgProc/MsgToPut.h"
#include "../../Base/BufQueue/BufQueue.h"
#include "RobotPlayerInfo.h"
#include "ConfigInfoManager.h"
#include <iostream>

// 全局消息池
TCache<MsgToPut> *g_poolMsgToPut;

/*
int testSelect(CConnection *pConnection)
{
	/// 创建一个请求
	CQuery *pQuery = pConnection->createQuery();
	if(pQuery == NULL)
		return 0;

	/// 操作语句
	char *pszStatement = "select id, name from persional";

	/// 执行一个请求
	CResult * pResult = pQuery->executeSelect(pszStatement, strlen(pszStatement));
	if(pResult == NULL)
	{
		pConnection->destroyQuery(pQuery);
		return 0;
	}

	/// 显示行信息
	std::cout << "row list:" << std::endl;
	while(pResult->next())
	{
		std::cout << "id = " << pResult->getInt(0) << ",name = " << pResult->getString(1) << std::endl;
	}

	/// 清除结果集 
	pQuery->freeResult(pResult);

	/// 删除请求
	pConnection->destroyQuery(pQuery);
	
	return 0;
}


int testInsert(CConnection* pConnection)
{
	/// 创建一个请求
	CQuery *pQuery = pConnection->createQuery();
	if(pQuery == NULL)
		return 0;

	/// 操作语句
	char *pszStatement = "insert into persional(id, name) values (11, 'shide')";

	/// 执行一个请求
	int iAffectedNum = pQuery->executeUpdate(pszStatement, strlen(pszStatement));
	if(iAffectedNum == -1)
	{
		pConnection->destroyQuery(pQuery);
		return -1;
	}

	/// 删除请求
	pConnection->destroyQuery(pQuery);
	return 0;
}

int testUpdate(CConnection* pConnection)
{
	/// 创建一个请求
	CQuery *pQuery = pConnection->createQuery();
	if(pQuery == NULL)
		return 0;

	/// 操作语句
	char *pszStatement = "update persional set name ='kiki' where id = 11";

	/// 执行一个请求
	int iAffectedNum = pQuery->executeUpdate(pszStatement, strlen(pszStatement));
	if(iAffectedNum == -1)
	{
		pConnection->destroyQuery(pQuery);
		return -1;
	}

	/// 删除请求
	pConnection->destroyQuery(pQuery);
	return 0;
}

int testDelete(CConnection* pConnection)
{
	/// 创建一个请求
	CQuery *pQuery = pConnection->createQuery();
	if(pQuery == NULL)
		return 0;

	/// 操作语句
	char *pszStatement = "delete from persional where id = 11";

	/// 执行一个请求
	int iAffectedNum = pQuery->executeUpdate(pszStatement, strlen(pszStatement));
	if(iAffectedNum == -1)
	{
		pConnection->destroyQuery(pQuery);
		return -1;
	}

	/// 删除请求
	pConnection->destroyQuery(pQuery);
	return 0;
}
*/


int ACE_TMAIN(int, ACE_TCHAR *[])
{
	CConnection * pConnection = CConnectionPoolSingle::instance()->getConnection();
	if(pConnection == NULL)
		return 0;

	stConnectionString connstr;
	memset(&connstr, 0x00, sizeof(connstr));

	memcpy(connstr.szDb, "test", sizeof(connstr.szDb));
	memcpy(connstr.szHost, "172.18.7.101", sizeof(connstr.szHost));
	memcpy(connstr.szPassword, "mux", sizeof(connstr.szPassword));
	memcpy(connstr.szUser, "mux", sizeof(connstr.szUser));

	if(pConnection->open(&connstr) == -1)
		return -1;

	if(CRoleInitManagerSingle::instance()->init("../../DBServer/DBServer/InitRoleInfo.xml") == -1)
		return -1;

	CRobotPlayerInfo::insertPlayerInfo("Robot", 1000);

	return 0;
};