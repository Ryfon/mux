﻿/** @file ConfigInfoManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-3-4
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CONFIG_INFO_MANAGER_H
#define CONFIG_INFO_MANAGER_H

#include "ace/Singleton.h"
#include <map>

#pragma pack(push, 1)

typedef struct StrInitRoleInfo  
{
	unsigned short usMapID;											// 地图ID
	int iPosX;														// X坐标
	int iPosY;														// Y坐标
	unsigned char ucLevel;											// 等级
	unsigned int uiHP;												// HP
	unsigned int uiMP;												// MP
	unsigned int uiMoney;											// 金钱
	unsigned int uiSkill1ID;										// 技能1
	unsigned int uiSkill2ID;										// 技能2
}InitRoleInfo;

#pragma pack(pop)

/**
* @class CRoleInitManager
*
* @brief 管理角色创建时的相关缺省属性
* 
*/
class CRoleInitManager
{
	friend class ACE_Singleton<CRoleInitManager, ACE_Null_Mutex>;

private:
	/// 构造
	CRoleInitManager(void);

	/// 析构
	~CRoleInitManager(void);

public:
	int init(const char* pszFileName);

	InitRoleInfo * find(unsigned short usClass);

	int clear(void);

private:
	/// 初始化信息列表
	std::map<unsigned short, InitRoleInfo*> m_playerInitInfos;
	
};

typedef ACE_Singleton<CRoleInitManager, ACE_Null_Mutex> CRoleInitManagerSingle;


#endif/*CONFIG_INFO_MANAGER_H*/

