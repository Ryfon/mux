﻿#ifndef ROBOT_PLAYER_INFO_H
#define ROBOT_PLAYER_INFO_H


class CRobotPlayerInfo
{
public:
	static int insertPlayerInfo(const char *pszAccountPrefix, unsigned int uiNum);

private:
	static int insertPlayerInfo_i(const char *pszData, unsigned int uiDataLen);
};

#endif/*ROBOT_PLAYER_INFO_H*/
