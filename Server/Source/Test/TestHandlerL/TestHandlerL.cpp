﻿#include "ace/Get_Opt.h"
#include "ace/Log_Msg.h"
#include "ace/SOCK_Acceptor.h"
#include "ace/SOCK_Connector.h"
#include "../../Base/Connector.h"
#include "../../Base/Acceptor.h"
#include "../../Base/HandlerT.h"


// // 前置声明
class CClientHandler;
class CServerHandler;


struct stTest
{
	int iFlag;
	int iId;
	char szName[100];
};

static const ACE_TCHAR *host = 0;
static u_short port = ACE_DEFAULT_SERVER_PORT;

typedef CConnector<CClientHandler, ACE_SOCK_Connector> MyConnector;
typedef CAcceptor<CServerHandler, ACE_SOCK_Acceptor> MyAcceptor;

/**
* @class CClientHandler
*
* @brief 基于轻量级处理器上的具体测试模块(客户)
*/

class CClientHandler : public CHandlerT
{
public:
	/// 构造
	CClientHandler(void);

	/// 析构
	virtual ~CClientHandler(void);

public:
	/// 连接建立时处理
	virtual int onLinkUp(void);

	/// 连接段开始处理
	virtual int onLinkDown(void);

	/// 处理一个完整包
	virtual int handlePackage(const char* pszPack, unsigned int uiPakcLen);
};

CClientHandler::CClientHandler(void)
{

}

CClientHandler::~CClientHandler(void)
{
	
}

int CClientHandler::onLinkUp(void)
{
	// 获取对端地址
	ACE_INET_Addr addr;
	getRemoteAddress(addr);

	ACE_DEBUG((LM_DEBUG, 
		       ACE_TEXT("connect to server(%s : %d) up\n"),
			   addr.get_host_addr(), addr.get_port_number()));

	// 测试包
	stTest test;
	ACE_OS::memset(&test, 0x00, sizeof(test));

	test.iFlag = 0;
	test.iId = 1;
	ACE_OS::memcpy(test.szName, "hello", sizeof(test.szName));

	sendMsg((const char*)&test, sizeof(test));	
	return 0;
}

int CClientHandler::onLinkDown(void)
{
	// 获取对端地址
	ACE_INET_Addr addr;
	getRemoteAddress(addr);

	ACE_DEBUG((LM_DEBUG, 
			   ACE_TEXT("connect to server(%s : %d) down\n"),
			   addr.get_host_addr(), addr.get_port_number()));

	return 0;
}

int CClientHandler::handlePackage(const char* pszPack, unsigned int uiPakcLen)
{
	if(uiPakcLen != sizeof(stTest))
		return -1;
	
	stTest *pTest=(stTest *)pszPack;
	ACE_DEBUG((LM_DEBUG,
		       ACE_TEXT("iFlag = %d, iID = %d, szName = %s\n"),
			   pTest->iFlag, pTest->iId, pTest->szName));

	return 0;
}


/**
* @class CServerHandler
*
* @brief 基于轻量级处理器上的具体测试模块(服务器)
*/

class CServerHandler : public CHandlerT
{
public:
	/// 构造
	CServerHandler(void);

	/// 析构
	virtual ~CServerHandler(void);

public:
	/// 连接建立时处理
	virtual int onLinkUp(void);

	/// 连接段开始处理
	virtual int onLinkDown(void);

	/// 处理一个完整包
	virtual int handlePackage(const char* pszPack, unsigned int uiPakcLen);
};

CServerHandler::CServerHandler(void)
{

}

CServerHandler::~CServerHandler(void)
{

}

int CServerHandler::onLinkUp(void)
{
	// 获取对端地址
	ACE_INET_Addr addr;
	getRemoteAddress(addr);

	ACE_DEBUG((LM_DEBUG, 
		       ACE_TEXT("client (%s : %d) up\n"),
		       addr.get_host_addr(), addr.get_port_number()));

	return 0;
}

int CServerHandler::onLinkDown(void)
{
	// 获取对端地址
	ACE_INET_Addr addr;
	getRemoteAddress(addr);

	ACE_DEBUG((LM_DEBUG, 
		       ACE_TEXT("client (%s : %d) down\n"),
		       addr.get_host_addr(), addr.get_port_number()));

	return 0;
}

int CServerHandler::handlePackage(const char* pszPack, unsigned int uiPakcLen)
{
	if(uiPakcLen != sizeof(stTest))
		return -1;

	stTest *pTest=(stTest *)pszPack;
	
	// 回复
	stTest resp;
	resp.iFlag = 1;
	resp.iId = pTest->iId;
	ACE_OS::memcpy(resp.szName, pTest->szName, sizeof(resp.szName));

	sendMsg((const char *)&resp, sizeof(resp));

	return 0;
}

int print_usage (int /* argc */, ACE_TCHAR *argv[])
{
	ACE_ERROR((LM_ERROR,
				ACE_TEXT ("\nusage: %s")
				ACE_TEXT ("\n-p <port to listen/connect>")
				ACE_TEXT ("\n-h <host> for Client mode")
				ACE_TEXT ("\n-u show this message")
				ACE_TEXT ("\n"),
				argv[0]
				));

	return -1;
}

static int parse_args (int argc, ACE_TCHAR *argv[])
{
	if (argc == 1)
		return 0;

	ACE_Get_Opt get_opt (argc, argv, ACE_TEXT ("p:h:u"));
	int c;

	while ((c = get_opt ()) != EOF)
	{
		switch (c)
		{
		case 'h':         // host for sender
			host = get_opt.opt_arg();
			break;
		case 'p':         // port number
			port = ACE_OS::atoi (get_opt.opt_arg());
			break;
		case 'u':
		default:
			return print_usage (argc,argv);
		} // switch
	} // while

	return 0;
}

int server(void)
{
	MyAcceptor acceptor;
	if(acceptor.open(ACE_INET_Addr(port)) == -1)
		return -1;

	ACE_Reactor::instance()->run_reactor_event_loop();

	return 0;
}

int client(void)
{
	MyConnector connector;
	ACE_INET_Addr addr(port, host);

	CClientHandler *pHandler;
	ACE_NEW_RETURN(pHandler, CClientHandler, -1);

	if(connector.connect(pHandler, addr) == -1)
		return -1;

	ACE_Reactor::instance()->run_reactor_event_loop();

	return 0;
}

int main (int argc, ACE_TCHAR *argv[])
{	
	if (::parse_args (argc, argv) == -1)
		return -1;
	
	if(host == 0)
	{
		server();
	}
	else
	{
		client();
	}
	
	return 0;
}

TCache< MsgToPut >* g_poolMsgToPut;


