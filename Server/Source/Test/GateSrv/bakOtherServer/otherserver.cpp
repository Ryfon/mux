﻿/**
* @file otherserver.cpp
* @brief 定义服务器端的其它服务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: otherserver.h
* 摘    要: 定义服务器端的其它服务
* 作    者: dzj
* 完成日期: 2007.12.10
*
*/

#include "otherserver.h"

map<DWORD, COtherServer*> CManOtherServer::m_mapOtherSrvs;//所有其它srv;

///置玩家的句柄信息，每次新建时必须调用;
void COtherServer::SetHandleInfo( int nHandleID, int nSessionID )
{
	if ( ( nSessionID <=0 )
		|| ( nSessionID > 1000 )
		)
	{
		//错误，srv的nSessionID不应该大于1000;
		D_WARNING( "错误的nSessionID%d, srv的sessionID应该小于等于1000\n\n", nSessionID );
	}

	m_nHandleID = nHandleID;
	m_nSessionID = nSessionID;		
	m_bySrvType = 0x0f;//注意，此处设为对应的srvType;
	CManOtherServer::AddOneSrv( this );
	return;
}

///销毁时的处理(连接断开)
///注意：在新建连接之前也会调用这里；
void COtherServer::OnDestoryed()
{
	if ( m_nHandleID != 0 )
	{
		//确实为断开，区别于新建连接时的回调；
		//以下断开处理；
		CManOtherServer::RemoveSrv( this );
	}
	ResetInfo();
	return;
}



