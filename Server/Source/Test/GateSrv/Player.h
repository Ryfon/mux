﻿/**
* @file Player.h
* @brief 定义玩家类
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: Player.h
* 摘    要: 定义玩家类；
* 作    者: dzj
* 完成日期: 2007.11.29
*
*/

#include <map>
#include "Utility.h"
#include "BufQueue/BufQueue.h"
#include "PkgProc/PacketBuild.h"

#pragma once
using namespace std;

extern IBufQueue* g_pClientSender;
extern bool g_bKickAllPlay;

#define CreatePlayerPkg( PKGTYPE, pPlayer, pPkg ) CPacketBuild::CreatePkg<PKGTYPE>( pPlayer->GetHandleID(), pPlayer->GetSessionID(), &pPkg );

class CManPlayer;

class CPlayer
{
public:
	CPlayer() : m_nHandleID(0), m_nSessionID(0){};
	~CPlayer() {};

	///置玩家的句柄信息，每次新建时必须调用;
    void SetHandleInfo( int nHandleID, int nSessionID );

public:
	//新建立时的处理(连接建立)
	void OnCreated()
	{
		ResetInfo();
		return;
	}

	///销毁时的处理(连接断开)
	///注意：在新建连接之前也会调用这里；
	void OnDestoryed();

	///请求销毁自身(断开连接)
	void ReqDestorySelf()
	{
		//发送长度为0消息断开自身；	
		MsgToPut* disconnectMsg = g_poolMsgToPut->RetrieveOrCreate();
		disconnectMsg->nHandleID = m_nHandleID;//向断开执行者传递自身标识；
		disconnectMsg->nSessionID = m_nSessionID;//向断开执行者传递自身标识；
		disconnectMsg->nMsgLen = 0;//表明断开连接请求;
		SendPkgToPlayer( disconnectMsg );
		D_WARNING( "本GS主动请求断开玩家(其sessionID:%d)\n", m_nSessionID );
		return;
	}

	//收包处理；
	void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

	int GetHandleID()
	{
		return m_nHandleID;
	}

	int GetSessionID()
	{
		return m_nSessionID;
	}

	///向外发包；
	void SendPkgToPlayer( MsgToPut* pPkg )
	{
		if ( NULL != g_pClientSender )
		{
			g_pClientSender->PushMsg( pPkg );
			g_pClientSender->SetReadEvent();//通知网络模块立即发送出去；
		}
		return;
	}

private:
	///重置自身信息；
	void ResetInfo()
	{
		m_nHandleID = 0;
		m_nSessionID = 0;
	}

private:
	///通信句柄标识，玩家通过该句柄向发包者标识自己的发包对象；
	int m_nHandleID;
	///通信句柄校验号，玩家通过该校验号向发包者校验自己的发包对象；
	int m_nSessionID; 

	char m_strAccount[32];
	char m_strPwd[32];
};

//其它服务管理器
class CManPlayer
{
public:
	static void KickAllPlayer()
	{
		CPlayer* pPlayer = NULL;
		bool iscont = true;
		while (iscont)
		{
			iscont = false;
			for ( map<DWORD_PTR, CPlayer*>::iterator tmpiter=m_mapPlayers.begin(); tmpiter!=m_mapPlayers.end(); ++tmpiter )
			{
				pPlayer = tmpiter->second;
				if ( pPlayer->GetSessionID() > 1000 )//普通玩家；
				{
					pPlayer->ReqDestorySelf();
					m_mapPlayers.erase( tmpiter );
					iscont = true;//继续删；
					break;
				}
			}
		}
		return;
	}

	static void AddOnePlayer( CPlayer* pPlayer )
	{
		map<DWORD_PTR, CPlayer*>::iterator tmpIter = m_mapPlayers.find( (DWORD_PTR)pPlayer );
		if ( tmpIter != m_mapPlayers.end() )
		{
			D_WARNING( "将玩家加入全局列表时，发现对应的玩家指针%d已在列表中\n", (DWORD_PTR)pPlayer );
			return;
		}
		m_mapPlayers.insert( pair<DWORD_PTR, CPlayer*>( (DWORD_PTR)pPlayer, pPlayer ) );
	}
	static bool RemovePlayer( CPlayer* pPlayer )
	{
		map<DWORD_PTR, CPlayer*>::iterator tmpIter = m_mapPlayers.find( (DWORD_PTR)pPlayer );
		if ( tmpIter != m_mapPlayers.end() )
		{
			m_mapPlayers.erase( tmpIter );
			return true;
		}
		return false;
	}
	static CPlayer* FindPlayer( DWORD_PTR dpToFind )
	{
		map<DWORD_PTR, CPlayer*>::iterator tmpIter = m_mapPlayers.find(dpToFind);
		if ( tmpIter != m_mapPlayers.end() )
		{
			return tmpIter->second;
		} else {
			return NULL;
		}
	}
	static CPlayer* GetMapserver( int nSessionID )
	{
		CPlayer* pPlayer = NULL;
		for ( map<DWORD_PTR, CPlayer*>::iterator tmpiter=m_mapPlayers.begin(); tmpiter!=m_mapPlayers.end(); ++tmpiter )
		{
			pPlayer = tmpiter->second;
			if ( pPlayer->GetSessionID() == nSessionID )
			{
				return pPlayer;
			}
		}
		return NULL;
	}
private:
	static map<DWORD_PTR, CPlayer*> m_mapPlayers;//所有其它srv;
};





