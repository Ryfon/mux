﻿// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// 从 Windows 头中排除极少使用的资料
#include <stdio.h>

//from <windows核心编程>
#ifdef ACE_WIN32
#include <tchar.h>
#include <process.h>    /* _beginthread, _endthread */
typedef unsigned (__stdcall * PTHREAD_START) (void*);
#define HXBCBEGINTHREADEX( psa, cbStack, pfnStartAddr\
	, pvParam, fdwCreate, pdwThreadID)\
	((HANDLE) _beginthreadex(\
	(void*) (psa),\
	(unsigned) (cbStack),\
	(PTHREAD_START)(pfnStartAddr),\
	(void*) (pvParam),\
	(unsigned) (fdwCreate),\
	(unsigned*) (pdwThreadID))) 
#endif //ACE_WIN32


// TODO: 在此处引用程序需要的其他头文件
