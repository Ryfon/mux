﻿/**
* @file otherserver.h
* @brief 定义服务器端的其它服务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: otherserver.h
* 摘    要: 定义服务器端的其它服务
* 作    者: dzj
* 完成日期: 2007.12.10
*
*/

#include <map>
#include "Utility.h"
#include "BufQueue/BufQueue.h"
#include "PkgProc/PacketBuild.h"

#pragma once
using namespace std;

extern IBufQueue* g_pSrvSender;

#define CreateServerPkg( PKGTYPE, pPkg ) CPacketBuild::CreatePkg<PKGTYPE>( m_nHandleID, m_nSessionID, &pPkg );

class CManOtherServer;
class COtherServer
{
public:
	COtherServer() : m_nHandleID(0), m_nSessionID(0), m_bySrvType(0x0f)/*0x0ff目前无效的服务标识号*/{};
	~COtherServer() {};

	///置玩家的句柄信息，每次新建时必须调用;
	void SetHandleInfo(/*T_PkgSender* pPkgSender,*/ int nHandleID, int nSessionID );

public:
	//新建立时的处理(连接建立)
	void OnCreated()
	{
		ResetInfo();
		return;
	}

	///销毁时的处理(连接断开)
	///注意：在新建连接之前也会调用这里；
	void OnDestoryed();

	///请求销毁自身(断开连接)
	void ReqDestorySelf()
	{
		//发送长度为0消息断开自身；
		static MsgToPut disconnectMsg;
		memset( &disconnectMsg, 0, sizeof(disconnectMsg) );
		disconnectMsg.nHandleID = m_nHandleID;//向断开执行者传递自身标识；
		disconnectMsg.nSessionID = m_nSessionID;//向断开执行者传递自身标识；
		SendPkgToPlayer( &disconnectMsg );
		return;
	}

	//收包处理；
	void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen ) 
	{ 
		//目前简单处理一个跑动包，作为测试用；
		if ( M_G_RUN == wCmd )
		{
			//检验收包大小是否正确；
			if ( wPkgLen != sizeof(CGRun) )
			{
				//收包大小错误，可能是通信两端消息结构不一致；
				return;
			}

			//结构校验通过
			MGRun* pRun = (MGRun*)pBuf;
			GCRun  returnMsg;
			returnMsg.lPlayerID = pRun->lPlayerID;
			returnMsg.lCurOffset = pRun->lCurOffset;
			returnMsg.nOrgX = pRun->nOrgX;
			returnMsg.nOrgY = pRun->nOrgY;
			returnMsg.nNewX = pRun->nNewX;
			returnMsg.nNewY = pRun->nNewY;
			MsgToPut* pNewMsg = CreateServerPkg( GCRun, returnMsg );
			//调用发包模块将消息包发送出去；
			SendPkgToPlayer( pNewMsg );
		}
		return; 
	};

private:
	///向外发包；
	void SendPkgToPlayer( MsgToPut* pPkg )
	{
		g_pSrvSender->PushMsg( pPkg );
		g_pSrvSender->SetReadEvent();//通知网络模块立即发送出去；
		return;
	}
	///重置自身信息；
	void ResetInfo()
	{
		m_nHandleID = 0;
		m_nSessionID = 0;
	}

private:
	///通信句柄标识，玩家通过该句柄向发包者标识自己的发包对象；
	int m_nHandleID;
	///通信句柄校验号，玩家通过该校验号向发包者校验自己的发包对象；
	int m_nSessionID; 

	///该服务对应的标识号,例如：C A等，见SrvProtocol.h;
	int m_bySrvType;

};

//其它服务管理器
class CManOtherServer
{
public:
	static void AddOneSrv( COtherServer* pOtherSrv )
	{
		m_mapOtherSrvs.insert( pair<DWORD_PTR, COtherServer*>( (DWORD_PTR)pOtherSrv, pOtherSrv ) );
	}
	static bool RemoveSrv( COtherServer* pOtherSrv )
	{
		map<DWORD_PTR, COtherServer*>::iterator tmpIter = m_mapOtherSrvs.find( (DWORD_PTR) pOtherSrv );
		if ( tmpIter != m_mapOtherSrvs.end() )
		{
			m_mapOtherSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}
	static COtherServer* FindOtherSrv( DWORD_PTR dpToFind )
	{
		map<DWORD_PTR, COtherServer*>::iterator tmpIter = m_mapOtherSrvs.find(dpToFind);
		if ( tmpIter != m_mapOtherSrvs.end() )
		{
			return tmpIter->second;
		} else {
			return NULL;
		}
	}
private:
	static map<DWORD_PTR, COtherServer*> m_mapOtherSrvs;//所有其它srv;
};
