﻿#include "../../Base/XmlManager.h"

int main(int, char *[])
{
	CXmlManager xmlManager;

	//const char *pszXmlFile = "test.xml";
	const char *pszXmlFile = "NPCSkill.xml";

	if(!xmlManager.load(pszXmlFile))
		return 0;

	CElement *pRoot = xmlManager.root();
	if(pRoot == NULL)
		return 0;

	std::vector<CElement*> elementSet;
	xmlManager.findChildElements(pRoot, elementSet);
	if(elementSet.size() <= 0)
		return 0;

	// 指定索引
	elementSet.clear();
	xmlManager.findChildElements(1, elementSet);
	if(elementSet.size() <= 0)
		return 0;

	// 同层 
	elementSet.clear();
	xmlManager.findSameLevelElements(2, elementSet);
	if(elementSet.size() <= 0)
		return 0;


	return 0;
}