﻿#include "ace/Get_Opt.h"
#include "ace/Log_Msg.h"
#include "ace/SOCK_Acceptor.h"
#include "ace/SOCK_Connector.h"
#include "ace/Acceptor.h"
#include "ace/Connector.h"
#include "ace/Reactor.h"
#include "ace/Auto_Ptr.h"
#include "ace/Event_Handler.h"
#include "ace/SOCK_Dgram.h"


// 全局类型
#pragma pack(push, 1)

typedef struct strTest
{
	char szName[32];
	int iFlag;
	int iId;
}strTest;

#pragma pack(pop)

static const ACE_TCHAR *host = NULL;
static u_short port = ACE_DEFAULT_SERVER_PORT;

class CMyHandler;


class CMyHandler : public ACE_Event_Handler
{
public:
	/// 构造(作为服务器使用时)
	CMyHandler(int iLocalPort)
		:m_mb(5000), m_timeid(0), m_localAddr(iLocalPort), m_peer(m_localAddr)
	{

	}

	/// 构造(作为客户端使用时)
	CMyHandler(const char *pszRemoteHost, int iRemotePort)
		:m_mb(5000), m_timeid(0),m_remoteAddr(iRemotePort, pszRemoteHost), m_localAddr((unsigned short)0), m_peer(m_localAddr)
	{
		
	}

	/// 析构
	virtual ~CMyHandler(void)
	{

	}

public:
	/// 
	virtual int open(void * arg= 0)
	{
		int *pFlag = (int *)arg;

		if(*pFlag == 0) //只接收数据
		{
			// 注册网络读事件
			if(this->reactor()->register_handler(this, ACE_Event_Handler::READ_MASK) == -1)
			{
				ACE_DEBUG((LM_DEBUG, "[%T]CMyHandler::open中register_handler出错\n"));
				return -1;
			}
		}
		
		if(*pFlag == 1)//只发送数据
		{
			// 注册定时器
			if( (m_timeid = this->reactor()->schedule_timer(this, NULL, ACE_Time_Value(0,1000), ACE_Time_Value(10,0)) ) == -1)
			{
				ACE_DEBUG((LM_DEBUG, "[%T]CMyHandler::open中schedule_timer出错\n"));
				return -1;
			}
		}
		
		return 0;
	}

	/// 清理
	virtual int close(u_long flags = 0)
	{
		if(m_peer.get_handle() != ACE_INVALID_HANDLE)
		{	
			this->reactor()->remove_handler(this, ACE_Event_Handler::READ_MASK|ACE_Event_Handler::WRITE_MASK|ACE_Event_Handler::DONT_CALL);

			if(m_timeid > 0)
				this->reactor()->cancel_timer(m_timeid);

			m_peer.close();
		}

		delete this;

		return 0;
	}

	/// 输入事件发生时被框架回调（例如连接或者数据）
	virtual int handle_input(ACE_HANDLE fd = ACE_INVALID_HANDLE)
	{
		if(m_peer.recv(m_mb.wr_ptr(), m_mb.space(), m_remoteAddr) != -1)
		{
			ACE_DEBUG((LM_DEBUG, "[%T]受到数据来自%s:%d的数据:%s\n", m_remoteAddr.get_host_name(), m_remoteAddr.get_port_number(), m_mb.rd_ptr()));

			m_mb.reset();
		}
		return 0;
	}

	/// 对应句柄可写时
	virtual int handle_output (ACE_HANDLE fd = ACE_INVALID_HANDLE)
	{
		ACE_DEBUG((LM_DEBUG,"[%T]%s:%d->handle_output\n", m_localAddr.get_host_name(), m_localAddr.get_port_number()));

		char temp[] = "hello, world";

		if(m_peer.send(temp, sizeof(temp), m_remoteAddr) != -1)
		{
			return this->reactor()->remove_handler(this, ACE_Event_Handler::WRITE_MASK|ACE_Event_Handler::DONT_CALL);
		}

		return 0;
	}

	/** 
	* 超时时被框架回调. <current_time>是<CHandlerT>因超时被分派的时间，
	* <act>是在<schedule_timer>时传进的参数
	*/	
	virtual int handle_timeout(const ACE_Time_Value &current_time,
		const void *act = 0)
	{
		ACE_DEBUG((LM_DEBUG, "[%T]%s:%d定时发送数据\n", m_localAddr.get_host_name(), m_localAddr.get_port_number()));

		char temp[] = "hello, world";

		// 发送数据
		if(m_peer.send(temp, sizeof(temp), m_remoteAddr) == -1)
		{
			// 注册写事件
			return this->reactor()->register_handler(this, ACE_Event_Handler::WRITE_MASK);
		}
		
		return 0;
	}

	/** 
	* 当<handle_*>返回-1或者<remove_handler>被框架调用时.<close_mask>
	* 表明什么事件触发<handle_close>被调用,<handle>指定具体的句柄
	*/
	virtual int handle_close(ACE_HANDLE handle,
		ACE_Reactor_Mask close_mask)
	{
		return close(0);
	}

	/// 获取句柄
	virtual ACE_HANDLE get_handle(void) const
	{
		return m_peer.get_handle();
	}


public:
	/// 处理一个完整包
	virtual int handlePackage(const char* pszPack, unsigned int uiPakcLen)
	{
		return 0;
	}

private:
	ACE_Message_Block m_mb;

	long m_timeid;

	ACE_INET_Addr m_remoteAddr;

	ACE_INET_Addr m_localAddr;

	ACE_SOCK_Dgram m_peer;
};


int print_usage (int /* argc */, ACE_TCHAR *argv[])
{
	ACE_ERROR((LM_ERROR,
		ACE_TEXT ("\nusage: %s")
		ACE_TEXT ("\n-p <port to listen/connect>")
		ACE_TEXT ("\n-h <host> for Client mode")
		ACE_TEXT ("\n-u show this message")
		ACE_TEXT ("\n"),
		argv[0]
	));

	return -1;
}

static int parse_args (int argc, ACE_TCHAR *argv[])
{
	if (argc == 1)
		return 0;

	ACE_Get_Opt get_opt (argc, argv, ACE_TEXT ("p:h:u"));
	int c;

	while ((c = get_opt ()) != EOF)
	{
		switch (c)
		{
		case 'h':         // host for sender
			host = get_opt.opt_arg();
			break;
		case 'p':         // port number
			port = ACE_OS::atoi (get_opt.opt_arg());
			break;
		case 'u':
		default:
			return print_usage (argc,argv);
		} // switch
	} // while

	return 0;
}

int server(void)
{
	CMyHandler *pHandler;
	ACE_NEW_RETURN(pHandler, CMyHandler(port), -1);
	pHandler->reactor(ACE_Reactor::instance());

	int iFlag = 0;
	if(pHandler->open(&iFlag) == -1)
	{
		ACE_DEBUG((LM_DEBUG, "server()::open()error\n"));

		pHandler->close();
		return -1;
	}

	ACE_Reactor::instance()->run_reactor_event_loop();

	return 0;
}

int client(void)
{

	CMyHandler *pHandler;
	ACE_NEW_RETURN(pHandler, CMyHandler(host, port), -1);
	pHandler->reactor(ACE_Reactor::instance());

	int iFlag = 1;
	if(pHandler->open(&iFlag) == -1)
	{
		ACE_DEBUG((LM_DEBUG, "clent()::open()error\n"));
		
		pHandler->close();
		return -1;
	}

	ACE_Reactor::instance()->run_reactor_event_loop();

	return 0;
}

int ACE_TMAIN(int argc, ACE_TCHAR* argv[])
{	
	if (::parse_args (argc, argv) == -1)
		return -1;

	if(host == 0)
	{
		server();
	}
	else
	{
		client();
	}

	return 0;
}
