﻿/*

测试收发包性能;

by dzj, 09.06.05
*/

#include "testplayer.h"

#ifdef TEST_CODE

#include "apppoolobjs.h"

extern bool g_IsSrv;//服务端或客户端；

///获得一个自身的对象实例，与DestorySelfIns对应;
CTestPlayer* CTestPlayer::CreateSelfIns()
{
	CTestPlayer* pTmpTestPlayer = g_TestPlayerPool->DsRetrieve( TestPlayerNO );
	if ( NULL == pTmpTestPlayer )
	{
		return NULL;
	}
	pTmpTestPlayer->OnFrameEleCreated();
	return pTmpTestPlayer;
}

///释放自身，与CreateSelfIns对应；
void CTestPlayer::DestorySelfIns() 
{
	NewLog( LOG_LEV_INFO, "CTestPlayer::DestorySelfIns，删除自身" );
	OnFrameEleDestoryed();
	g_TestPlayerPool->DsRelease( TestPlayerNO, this ); 
};

#ifndef WIN32

bool CTestPlayer::CheckIfTimeNeedDestory()
//测试随机断开，检查是否已到断开时刻；
{
	if ( m_lastrcvTime > m_endTime )
	{
		NewLog( LOG_LEV_INFO, "CheckIfTimeNeedDestory，主动断开socket%d", GetInnerSocket() );
		IssueDestory();
		return true;
	}

	return false;
}

#ifdef TEST_CODE
bool CTestPlayer::OnRcvedMsg( bool isBroPkg, unsigned int pkgid, unsigned int rcvLen )
{
	rcvLen = rcvLen;//unused arg;
	if ( 0!=m_lastrcvTime )
	{
		unsigned int rcvpassed = GetTickCount() - m_lastrcvTime;
		if ( rcvpassed > m_maxrcvInterval )
		{
			m_maxrcvInterval = rcvpassed;
			NewLog( LOG_LEV_INFO, "socket%d，最大收包间隔%d", GetInnerSocket(), m_maxrcvInterval );
		}
		m_avgrcvInterval = (unsigned int) ( rcvpassed / ((double)(m_lastrcvid+1)) + m_avgrcvInterval * ( ((double)m_lastrcvid) / (m_lastrcvid+1) ) );		
		//if ( 0 == rand() % 5000 )
		//{
		//	NewLog( LOG_LEV_DEBUG, "socket%d，收包次数%d，平均收包间隔%d", GetInnerSocket(), (m_lastrcvid+1), m_avgrcvInterval );
		//}
	}
	m_lastrcvTime = GetTickCount();

	if ( !isBroPkg ) //测试广播消息
	{
		//非广播包，检测序号；
		if ( pkgid != (m_lastrcvid + 1) )
		{
			NewLog( LOG_LEV_ERROR, "OnRcvedMsg，收包序号错，上次收包号%d，本次收包号%d，准备断开", m_lastrcvid, pkgid );
			IssueDestory();
			sleep( 2 ); //int jjj
			exit(0);			
		}

		m_lastrcvid = pkgid;
	} else {
		NewLog( LOG_LEV_INFO, "OnRcvedMsg，socket%d，收到广播包", GetInnerSocket() );
	}

	++m_rcvtimes;//递增收包次数；

	if ( 0 == rand() % 10000 )
	{
		unsigned int passed = GetTickCount() - m_initTime;
		if ( passed > 0 )
		{
			unsigned int pkgpersec = m_rcvtimes * 100 / passed;
			NewLog( LOG_LEV_DEBUG, "socket%d，过去%d秒，平均每秒收包%d", GetInnerSocket(), (passed/100), pkgpersec );
		}		
	}

	if ( isBroPkg )
	{
		return true;//广播包不再重复echo，避免信息量不断增长；
	}

	//测试广播消息//返回一个消息给对方；
	static const char* sendmsg = "hello, just for test!hello, just for test!hello, just for test!hello, just for test!";

	++m_lastsendid;
	if ( g_IsSrv )
	{
		//服务端，向客户端发送GCChat;
		GCChat testmsg;
		memset( &testmsg, 0, sizeof(testmsg) );
		testmsg.sourcePlayerID.dwPID = m_lastsendid;//最后一次的发包序号；
		strcpy( testmsg.strChat, sendmsg );
		testmsg.chatSize = strlen(testmsg.strChat) + 1;
		NewMsgToPut* pToSendMsg = CNewPkgBuild::CreatePkg(&testmsg);
		if ( NULL == pToSendMsg )
		{
			NewLog( LOG_LEV_ERROR, "服务端，OnRcvedMsg，创建测试发送消息失败" );
			return false;
		}
		LSSendMsg( &pToSendMsg, 1 );
	} else {
		//客户端，向服务器发送CGChat
		CGChat testmsg;
		memset( &testmsg, 0, sizeof(testmsg) );
		testmsg.targetPlayerID.dwPID = m_lastsendid;//最后一次的发包序号；
		strcpy( testmsg.strChat, sendmsg );
		testmsg.chatSize = strlen(testmsg.strChat) + 1;
		NewMsgToPut* pToSendMsg = CNewPkgBuild::CreatePkg(&testmsg);
		if ( NULL == pToSendMsg )
		{
			NewLog( LOG_LEV_ERROR, "客户端，OnRcvedMsg，创建测试发送消息失败" );
			return false;
		}
		LSSendMsg( &pToSendMsg, 1 );
	}

	NewLog( LOG_LEV_INFO, "OnRcvedMsg，向对端发送消息" );


	if ( CheckIfTimeNeedDestory() )
	{
		//测试随机断开，检查是否已到断开时刻；
		return true;
	}

	return true;
};
#else //TEST_CODE
bool CTestPlayer::OnPkgRcved( unsigned short wCmd, const char* pPkg, unsigned int rcvLen )
//gatesrv使用，传递消息内容供应用处理；
{
	NewLog( LOG_LEV_DEBUG, "%d:%s:%d", wCmd, pPkg, rcvLen );
	return true;
}
#endif //TEST_CODE///收到消息；

///被框架遍历；
bool CTestPlayer::OnBeExploredByFrame()
{
	if ( CheckIfTimeNeedDestory() )
	{
		//测试随机断开，检查是否已到断开时刻；
		return true;
	}

	return true;
};

///连接成功后，给对象一个初始化自身的机会，每连接只执行一次；
bool CTestPlayer::OnConnedInit() 
{ 
	m_lastsendid = 0;

	//测试广播消息，
	if ( !g_IsSrv )
	{
		//客户端，主动向服务器发送一条消息，以开始消息流转循环，服务器只返回信息，不主动发送；
		//向对端发一条消息；
		static const char* sendmsg = "hello, just for test!hello, just for test!hello, just for test!hello, just for test!";

		m_lastsendid = 1;
		CGChat testmsg;
		memset( &testmsg, 0, sizeof(testmsg) );
		testmsg.targetPlayerID.dwPID = m_lastsendid;//最后一次的发包序号；
		strcpy( testmsg.strChat, sendmsg );
		testmsg.chatSize = strlen(testmsg.strChat) + 1;
		NewMsgToPut* pToSendMsg = CNewPkgBuild::CreatePkg(&testmsg);
		if ( NULL == pToSendMsg )
		{
			NewLog( LOG_LEV_ERROR, "OnConnedInit，创建测试发送消息失败" );
			return false;
		}

		NewLog( LOG_LEV_INFO, "客户端，OnConnedInit，向对端发送消息" );

		LSSendMsg( &pToSendMsg, 1 );
	}

	m_initTime = GetTickCount();
	m_endTime = m_initTime + (rand()%1000 * 100);//随机在0-n秒之后断开;rand()%100;//

    return true; 
};

#endif //WIN32

#endif //TEST_CODE

