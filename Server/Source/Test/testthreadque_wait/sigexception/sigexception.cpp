﻿
#include "sigexception.h"

SignalTranslator<SegmentationFault> g_objSegmentationFaultTranslator;

SignalTranslator<AbortSig> g_objAbortSigTranslator;

#ifndef WIN32
SignalTranslator<BusFault> g_objBusFaultTranslator;
#endif //WIN32

#ifdef USE_SIG_TRY
	bool g_isException = false;
#endif //USE_SIG_TRY

SignalTranslator<FloatingPointException> g_objFloatingPointExceptionTranslator;

