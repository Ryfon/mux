﻿/*

    socket收包缓存;

	by dzj, 09.06.16
*/

#define RCV_INNERBUF_SIZE 4096

#ifndef RCV_BUF_H
#define RCV_BUF_H

#include <string.h>

#include "pkgproc/pkgbuild.h"

#include "cirqueue.h"

#ifdef USE_CRYPT
#include "../../Base/crypt/dscryptbf.h"
#endif //USE_CRYPT

typedef CirQueue< NewMsgToPut, 1024/*每socket队列最大可缓存消息数*/, 10/*发送通知阈值*/ >  LSOCKET_QUEUE;

class CRcvBuf
{

public:
	CRcvBuf() 
	{
		Reset();
	};

	~CRcvBuf()
	{
		Reset();
	}

public:
	///收包时使用这两个接口取得内部缓存指针与大小；
	inline char* GetInnerBuf() { return &(m_InnerRcvBuf[m_curTail]); }
	inline unsigned int GetInnerBufSize() { return RCV_INNERBUF_SIZE-m_curTail; }

public:
	///每次收包后调用，递增已收包计数；
	inline bool AddRcved( unsigned int rcvNum )
	{
		m_curTail += rcvNum;
		if ( m_curTail > RCV_INNERBUF_SIZE )
		{
			m_curTail = RCV_INNERBUF_SIZE;//最尾部，越过实际界限；
			return false;
		}
		return true;
	}

#ifdef USE_CRYPT
	///每次收包后调用，以向应用递送收到的完整包，返回值：表明解析包是否成功，如果解析失败，则需要断开连接(没有收到完整包的情形算解析成功)；
	bool SubmitValidPkg( DsCryptBF& cryptIns, LSOCKET_QUEUE* rcvAppQueue/*用于向应用递送消息的队列*/, bool& isAppNeedNoti );
#else  //USE_CRYPT
	///每次收包后调用，以向应用递送收到的完整包，返回值：表明解析包是否成功，如果解析失败，则需要断开连接(没有收到完整包的情形算解析成功)；
	bool SubmitValidPkg( LSOCKET_QUEUE* rcvAppQueue/*用于向应用递送消息的队列*/, bool& isAppNeedNoti );
#endif //USE_CRYPT

private:
	/**
	* 缓存中是否包含一个有效包，dwPkgLen为包长，包格式:
	* 1-2			3		4-5		不定长度
	* 包长度	 包类型	   命令字	协议结构内容
	* wPkgLen	 byPkgType	wCmd	pContent
	*/	
	inline bool IsPkgValid( unsigned short& pkgLen, bool& isParseError )
	{
		isParseError = false;//是否有解析错；

		//包头是一个表明本包长度的WORD字段，其中长度包含WORD的2个字节;
		if ( m_curTail <= m_curReadPos )
		{
			return false;//没有缓存数据；
		}

		unsigned int curBufed = m_curTail - m_curReadPos;
		if ( curBufed < sizeof(unsigned short)+sizeof(char)+sizeof(unsigned short) ) //包头长度字段为WORD,包类型字段BYTE, 命令字字段为WORD
		{
			return false;//包不完整;
		}

		//获得实际包长度
		memcpy( &pkgLen, &(m_InnerRcvBuf[m_curReadPos]), sizeof(pkgLen) );

		if ( pkgLen < sizeof(unsigned short)+sizeof(char)+sizeof(unsigned short) ) //包头长度字段为WORD,包类型字段BYTE, 命令字字段为WORD
		{
			//包头指明的长度小于最小长度；
			//解析包错误（应该断开连接）
			NewLog( LOG_LEV_ERROR, "解析包错误,IsPkgValid1" );
			isParseError = true;
			return false;
		}

		if ( pkgLen >= NewMsgToPut::MAX_MSG_SIZE )
		{
			//解析包错误（应该断开连接）
			NewLog( LOG_LEV_ERROR, "解析包错误,IsPkgValid2, %d", pkgLen );
			isParseError = true;
			return false;
		}

		if ( pkgLen > curBufed )
		{	
			return false;//包尚未收完；
		}

		//包已收完，返回包长；
		return true;

	};

public:
	void Reset()
	{
		m_curTail = 0;
		m_curReadPos = 0;
	}

private:
	char m_InnerRcvBuf[RCV_INNERBUF_SIZE];
	unsigned int m_curTail;//当前有效数据尾部；

private:
	unsigned int m_curReadPos;//读数据起始位置，每次收包之前清0；
};

#endif //RCV_BUF_H
