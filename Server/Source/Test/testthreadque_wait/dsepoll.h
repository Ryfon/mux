﻿/*

    epoll,主要为了同时使用多个epoll及其线程处理不同的socket集

	by dzj, 09.06.04
*/

#ifndef DSEPOLL_H
#define DSEPOLL_H

#ifndef WIN32

#include <fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <errno.h>

#include "buflog.h"

///epoll等待线程；
void* DsEPollWaitThread( void* pInDsEPoll );

class CDsEPoll
{
public:
	CDsEPoll();

	~CDsEPoll();

public:
	///返回内部句柄
	inline int GetInnerFd() { return m_epfd; }

public:
	///等待线程执行函数，返回false表示无需再循环；
	void WaitTrdProc();

private:
	bool m_bIsStopWaitTrd;//是否终止自身对应线程；

private:
	int m_epfd;//epoll句柄；
	struct epoll_event* m_arrPollEvents;//epoll事件数组；
	int m_maxfd;

private:
	pthread_t m_polltrd;//epoll工作线程；
};

#endif //#ifndef WIN32

#endif //#ifndef DSEPOLL_H

