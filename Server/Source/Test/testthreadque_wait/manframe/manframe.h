﻿/*
	管理活动句柄
	主要为了：避免原来的通信队列方法在消息中传递句柄的缺陷（1、句柄可能失效的处理，2、根据句柄号查找句柄的低效(包括同一活动句柄上反复到来消息导致的重复工作) ）；

	基本思路：1、使用bit置位表示句柄活动，借用ace中的n&~(n-1)方法快速查找已置位
	          2、使用多层置位进一步加快扫描活动句柄的速度
			  3、句柄与使用的句柄的对象紧密绑定，整个生命周期完全一致
			  4、在对象内部保存数组位置，frame内使用hash计算内部存放位置，以便能够使用ID号迅速找到句柄，
			     同时，使用协调的办法处理hash冲突，即：如果发生冲突，则让待存元素另选一个ID号重存，避开在hash数组内部处理冲突的低效；
	by dzj, 09.05.28
*/

#ifndef MAN_FRAME_H
#define MAN_FRAME_H

#include "../dslock.h"
#include "../buflog.h"

#include <vector>

using namespace std;

struct NewMsgToPut;
class CManFrame;
///管理框架所管理的句柄（使用句柄的对象）
class IFrameEle
{
public:
	IFrameEle();
	virtual ~IFrameEle();

	virtual void DestorySelfIns() = 0;

public:
	///自身被创建时执行的初始化操作，用于池对象的构造；
	void OnFrameEleCreated();
	///自身被销毁时执行的清理操作，用于池对象的析构；
	void OnFrameEleDestoryed();

public:
	///3个加入点，1、开始监听前；2、监听到新连接，将新连接加入epoll前；3、发起新连接前；
	///    如果加入失败，调用者负责将本元素删去；
	bool AddSelfToFrame( CManFrame* pManFrame );

public:
	//初始化自身ID为无效ＩＤ号，等待框架为自身赋有效ＩＤ号；
	inline void InitInvalidSelfID()
	{
		m_bIsNeedActiveExplore = false;
		m_bIsToDelSelf = false;
		m_IDInFrame = 0;
		m_PosInfo = 0;
	}

private:
	unsigned int m_delIssueTime;//删除发起时刻，延迟一段删除自身，防止已删对象又到来消息；

public:
	volatile bool m_bDbgIsClose;

public:
	inline void SetFrameEleID( unsigned int eleID )
	{
		m_IDInFrame = eleID;
	}
	inline void SetPosInfo( unsigned int posInfo )
	{
		m_PosInfo = posInfo;
	}

public:
	///将自身标为活动，以备相应线程explore;
	bool SetNeedActiveExplore();

	///关闭自身;
	void SelfClose();

private:
	bool m_bIsNeedActiveExplore;//自身已设活动遍历标记；
	bool m_bIsToDelSelf;//是否要删除自身；参见m_bActiveClose；

public:
	virtual void IssueDestory( bool isForce = false ) = 0;

public:
	///关闭socket，并重新初始化；
	virtual bool InnerClose() = 0;

public:
	///被活动遍历时需要执行的任务(//注意:有可能当前欲遍历句柄实际已无效，具体参见CManFrame中ClearActiveFlag中的说明)；
	virtual bool OnBaseActiveBeExplored()
	{
		if ( m_bIsToDelSelf )
		{
			if ( GetTickCount() - m_delIssueTime > 1000*3/*3秒*/ )
			{
				return false;//已过3秒，删去；
			} else {
				return true;//暂时不删
			}
		}

		if ( !m_bIsNeedActiveExplore )
		{
			//自身还未设置活动遍历标记，可能是自身所处旧位置的元素所设的旧活动标记导致的残留遍历，参见'todo.doc'，bug21、22、23；
			return true;
		}
		m_bIsNeedActiveExplore = false;

		OnActiveBeExplored();

		return true;
	};//被CManFrame调用，一般是之前将自身设为活动的结果;

	virtual bool OnBaseValidBeExplored()
	{
		if ( m_bIsToDelSelf )
		{
			if ( GetTickCount() - m_delIssueTime > 1000*3/*3秒*/ )
			{
				return false;//已过一定时刻，该socket上应该已没有了新消息，删去；
			} else {
				return true;//暂时不删
			}
		}

		OnValidBeExplored();

		return true;
	}

	virtual bool OnActiveBeExplored() = 0;//被CManFrame调用，一般是之前将自身设为活动的结果;

	///被CManFrame调用，一般为定时任务；
	virtual bool OnValidBeExplored() = 0;//被CManFrame调用，一般为定时任务；

///////////////////////////////////////////////////////////////////////////////////////////
//广播消息支持...
public:
	///发起广播消息，该消息广播给框架中除监听端口外的所有元素
	void IssueBrocastMsg( NewMsgToPut* pToSendMsg );

	inline void SetBroedID( unsigned int broID ) { m_BroedID = broID; };//设置本元素的已广播号；
	inline unsigned int& GetBroedIDRef() { return m_BroedID; }//取最近发过的广播包号；

	///执行发送框架广播消息；
	void ExecSendBroMsg( NewMsgToPut* pToSendMsg );
	///子类的消息发送实现；
	virtual void CrdInsSendMsg( NewMsgToPut* pToSendMsg ) = 0;

private:
	unsigned int        m_BroedID;//本元素上次已发的广播包；
//...广播消息支持
///////////////////////////////////////////////////////////////////////////////////////////

public:
	inline unsigned int GetSelfFrameID() { return m_IDInFrame; };//之所以要用两个不同的ID号，是因为这个ID号可能被传到别的地方，其中SelfFrameID永远递增，不太可能重复，而PosInfo很可能被重用；
	inline unsigned int GetPosInfo() { return m_PosInfo; };//之所以要用两个不同的ID号，是因为这个ID号可能被传到别的地方，其中SelfFrameID永远递增，不太可能重复，而PosInfo很可能被重用；

private:
	unsigned int        m_IDInFrame;//框架中的自身ID标识号；
	unsigned int        m_PosInfo;//在框架中的位置标识；

public:
	inline CManFrame*   GetManFrame() { return m_pManFrame; };

private:
	CManFrame*          m_pManFrame;//管理自身的主框架,主要用于删除；
};


///////////////////////////////////////////////////////////////////////////////////////////
//广播消息支持...
///需要广播给框架内所有活动元素的消息；
struct BrocastMsg
{
public:
	///构造，待广播的消息；
	BrocastMsg( NewMsgToPut* pInMsg )
	{
		++g_broedID;
		selfID = g_broedID;
		pToBro = pInMsg;
		selfInitTime = GetTickCount();
	}

	~BrocastMsg();//每个发送者都得到一份该消息的副本，原始消息在这里删去；

public:

	static inline unsigned int GetCurBroedID() { return g_broedID; };

	///淘汰决定，是否已到淘汰时刻；
	inline bool IsNeedDel( unsigned int curTime )
	{
		if ( curTime - selfInitTime > 5000 )
		{
			return true;
		}
		return false;
	}

	///根据传入的检查时刻决定：本广播消息是否为调用者需要发送的广播包，checkID由调用者传入，调用后，调用者更新自身保存的checkID(调用者必须从前向后检查广播包队列，否则将漏发消息)
	inline NewMsgToPut* GetIfNewBro( unsigned int checkID )
	{
		///是否selfID大于checkID，防止运行长时间后，g_broedID绕过unsigned int的最大值；//不能用==比较，因为要确定队列中哪些包发过，哪些包没发过
		if ( (selfID/*待发*/ - checkID/*已发*/) < 10000/*短时间内不可能有超过10000个未广播的广播包*/ )
		{
			/*
			若待发比已发新，则减结果将为一小于10000的正值，需要发送本包；
			若待发==已发，则说明当前包正是所检测元素发过的最新包，无需再发本包；
			若待发比已发旧，则减结果将为一极大正值，无需发送本包；
			*/
			if ( selfID == checkID )
			{
				return NULL;//当前消息已发；
			}
			//自身ID确实大于checkID
			return pToBro;
		} 

		return NULL;
	}

private:
	static unsigned int g_broedID;//递增，用于生成各广播包的selfID，基本上只有一个框架的主线程会往外发送广播包，因此不必考虑多线程问题;
	unsigned int selfID;//各个元素上也记录自己上次的广播检查时刻，用以确定自身是否需要发送此广播消息，由于同一时刻可能会有多个广播消息，因此另设一ID号是较好的选择，而不使用selfInitTime
	NewMsgToPut*    pToBro;//待广播消息；
	unsigned int selfInitTime;//初始有效时刻，此时刻+6秒即为消息失效时刻(因为超过5秒不活动的连接会被删去)；
};
//...广播消息支持
///////////////////////////////////////////////////////////////////////////////////////////

class CManFrame
{
private:
	CManFrame();

public:
	explicit CManFrame( DsEvent& inWaitEvent, unsigned int eleNum ) : m_DsEvent( inWaitEvent )
	{
		m_curBroID = 0;//最新广播包ID号
		m_vecToBrocast.clear();//待广播包数组；
		m_InnerValidEleNum = 0;
		m_innerArrSize = 0;
		m_innerArr = NULL;
		InitManFrame( eleNum );		
	};

	///初始化，主要限制管理的元素数目不能超过32*32*32;
	bool InitManFrame( unsigned int eleNum )
	{
		if ( eleNum >= 32*32*32 )
		{
			NewLog( LOG_LEV_ERROR, "InitManFrame，输入eleNum%d过大", eleNum );
			return false;
		}

		m_innerArrSize = eleNum;
		m_innerArr = NEW IFrameEle*[m_innerArrSize];//分配多点元素，尽量避免冲突;
		for ( unsigned int i=0; i<m_innerArrSize; ++i )
		{
			m_innerArr[i] = NULL;//初始存放元素清空；
		}

		memset( m_ValidPos, 0, sizeof(m_ValidPos) );//初始无有效元素；
		ClearActiveSet();

		return true;
	}

	~CManFrame() 
	{
		if ( NULL != m_innerArr )
		{
			for ( unsigned int i=0; i<m_innerArrSize; ++i )
			{
				if ( NULL != m_innerArr[i] )
				{
					m_innerArr[i]->DestorySelfIns();//!!!, dsepoll必须先delete;
					m_innerArr[i] = NULL;//除非之前已先关闭，并设置自身需要遍历中删去，否则不可删，因为有可能epoll线程正在使用此socket;
					//gate runtime bug修改，由于dsepoll会先delete，因此没有dsepoll来进行socket close的操作并激活manframe的活动标记，这样写会导致死循环，m_innerArr[i]->IssueDestory();
					//delete m_innerArr[i]; m_innerArr[i] = NULL;
				}
			}

			//do {
			//	//等待所有内部元素被删去；
			//	ExploreActiveEles();
			//} while ( GetInnerValidEleNum() > 0 );

			delete [] m_innerArr; m_innerArr = NULL;
		}
		m_innerArrSize = 0;
		m_innerArr = NULL;
		ClearActiveSet();
		memset( m_ValidPos, 0, sizeof(m_ValidPos) );//清有效元素；
	};

	//void IssureDestoryAllEle()
	//{
	//	if ( NULL != m_innerArr )
	//	{
	//		for ( unsigned int i=0; i<m_innerArrSize; ++i )
	//		{
	//			if ( NULL != m_innerArr[i] )
	//			{
	//				m_innerArr[i]->IssueDestory();
	//			}
	//		}
	//	}
	//}

public:
	///添加欲管理的元素；
	bool AddToManFrame( IFrameEle* pToAdd );

	///根据ID号找到管理的内部元素指针；
	IFrameEle* FindFrameEle( unsigned int inFrameID );

public:
	///删除欲管理的元素，只能由自身主线程(执行ExploreActiveEles线程)执行，以免与ExploreActiveEles冲突，否则有可能主线程在遍历的时候本函数将元素置空；
	bool DelFromManFrame( IFrameEle* pToDel );

public:
	///清指定位置的活动标记，以便稍后遍历活动元素时排除该元素；
	inline bool ClearActiveFlag( unsigned int hashValue )
	{
		//计算hash值的三层bit置位位置；
		unsigned int lev0bit = 0;/*lev1pos*/
		unsigned int lev1bit = 0;/*lev2pos*/
		unsigned int lev2bit = 0;
		if ( !CalBitPos( hashValue, lev0bit, lev1bit, lev2bit ) )
		{
			NewLog( LOG_LEV_ERROR, "ClearActiveFlag，CalBitPos失败，值:%d", hashValue );//不可能；
			return false;//不可能；
		}

		if ( (lev0bit>=32) 
			|| (lev1bit>=32)
			|| (lev2bit>=32)
			)
		{
			NewLog( LOG_LEV_ERROR, "ClearActiveFlag，非法的bit位置，值:%d", hashValue );//不可能；
			return false;
		}

		//清位大部分是由于对应句柄已无效，本处bit清位也没有使用原子操作，因此有可能出现刚刚置位，但被这里又清位的情况，
		//　　造成的后果，１是已收包可能由于清位而没来得及收便丢失，这是没有办法的事情
		//                ２是清位的同时被别处置位，因此导致稍后遍历无效句柄，虽然目前还想不出具体哪里会有这样的置位操作，但在遍历时必须注意这一点；
		m_ActbitLev2[lev0bit][lev1bit].ClearBit( lev2bit );//只能清自身的对应位，不要因为子元素而清根置位标记，因为同样根下还有其它子元素；

		return true;
	}

	///设指定位置的活动标记，以便稍后遍历活动元素时处理该元素；
	inline bool SetActiveFlag( unsigned int hashValue )
	{
		//计算hash值的三层bit置位位置；
		unsigned int lev0bit = 0;/*lev1pos*/
		unsigned int lev1bit = 0;/*lev2pos*/
		unsigned int lev2bit = 0;
		if ( !CalBitPos( hashValue, lev0bit, lev1bit, lev2bit ) )
		{
			NewLog( LOG_LEV_ERROR, "SetActiveFlag，CalBitPos失败，值:%d", hashValue );//不可能；
			return false;//不可能；
		}

		if ( (lev0bit>=32) 
			|| (lev1bit>=32)
			|| (lev2bit>=32)
			)
		{
			NewLog( LOG_LEV_ERROR, "AddToActiveSet，非法的bit位置，值:%d", hashValue );//不可能；
			return false;
		}

		//本函数与ExploreActiveEles并发执行，尽量防止重复检测已遍历位，绝不遗漏需遍历位
		//    如果不按以下倒序置位，则可能出现本函数先置0层，然后Explore检测0,1,2层，清0层标记，然后本函数继续得以执行并置2,3层的情况
		//    在这种情形下，2,3层的置位将会丢失；
		//    另外，本处bit置位也没有使用原子操作，因此有可能出现刚刚置位，但被ExploreActiveEles又清位的情况，但由于清位后Explore马上会
		//        对位对应句柄进行扫描，因此也不会丢失置位，虽然同样地会有已置位已被扫描的情形，但没有大影响。
		m_ActbitLev2[lev0bit][lev1bit].SetBit( lev2bit );
		m_ActbitLev1[lev0bit].SetBit( lev1bit );

		m_ActbitLev0.SetBit( lev0bit );

		NewLog( LOG_LEV_INFO, "SetActiveFlag，置活动标记,m_ActbitLev0:%x", m_ActbitLev0.GetAtomicVal() );

		m_DsEvent.SignalEvent();

		return true;
	}

public:
	///框架处理循环
	bool ManFrameLoop()
	{
		/*
		1、当前是否有置位标记，如果有，则进入收包然后函数返回
		2、如果当前无置位，则进入等待，等待返回后进入收包然后函数返回
		*/
		if ( 0 == m_ActbitLev0.GetAtomicVal() )
		{
			//由于m_ActbitLev0没有使用锁，因此有可能在进入Block之前m_ActbitLev0被置位，所以这里wait不能太久以免丢失active消息；
#ifdef TEST_CODE
			m_DsEvent.BlockWaitEventByTime( 5 );//gatesrv中不需要等，留给原srv通信队列去等待;
#endif //TEST_CODE
			//NewLog( LOG_LEV_DEBUG, "ManFrameLoop" );
		}

#ifndef WIN32
		static unsigned int lastExpTime = GetTickCount();//测量遍历用时
		static unsigned int lastDisTime = GetTickCount();//测量遍历用时
		unsigned int curTime = GetTickCount();
		if ( curTime - lastExpTime > 0 ) //隔一定时间遍历一次有效元素；
		{
			lastExpTime = curTime;
			//NewLog( LOG_LEV_DEBUG, "遍历有效元素" );
			ExploreValidEles();
			if ( curTime - lastDisTime > 2000 )
			{
				lastDisTime = curTime;
				NewLog( LOG_LEV_INFO, "当前框架有效元素数%d", m_InnerValidEleNum );
			}
		}
#endif //WIN32

		//以下检查遍历活动元素；

		if ( 0 == m_ActbitLev0.GetAtomicVal() )
		{
			//NewLog( LOG_LEV_DEBUG, "无待遍历活动元素" );
			return false;
		}

		//NewLog( LOG_LEV_DEBUG, "遍历活动元素" );
		ExploreActiveEles();

		return true;
	}

	///遍历已被置为活动的元素，遍历后将相应元素重置为非活动（整个框架保证：由于线程竞争，有可能已经非活动的又被遍历，但绝不会出现活动的未被遍历）
    bool ExploreActiveEles();

	///遍历所有有效元素，目前方法：加大遍历频率，每次遍历更少部分元素；
	bool ExploreValidEles();

private:
	///清指定活动标记集；
	inline void ClearActiveSet()
	{
		m_ActbitLev0.InitAtomicVal(0);//第0层置位；
		memset( m_ActbitLev1, 0, sizeof(m_ActbitLev1) );//第1层置位；
		memset( m_ActbitLev2, 0, sizeof(m_ActbitLev2) );//第2层置位；
	}

	//计算hash值的三层bit置位字符与位置；
	inline bool CalBitPos( const unsigned int inValue, unsigned int& lev0bit, unsigned int& lev1bit, unsigned int& lev2bit )
	{
		lev0bit/*lev1pos*/ = inValue / (32*32);	
		lev1bit/*lev2pos*/ = ( inValue % (32*32) ) / 32 ;
		lev2bit = ( inValue % (32*32) ) % 32;
		return true;
	}

	//根据内部数组大小产生对应输入值的hash值,返回值：是否成功产生该hash值;
	inline bool InnerNumHash( const unsigned int inValue, unsigned int & hashValue )
	{
		if ( 0 == m_innerArrSize )
		{
			return false;//不可能；
		}

		//Robert Jenkins's 32 bit integer hash function, from: http://www.concentric.net/~Ttwang/tech/inthash.htm;
		hashValue = (inValue+0x7ed55d16) + (inValue<<12);
		hashValue = (hashValue^0xc761c23c) ^ (hashValue>>19);
		hashValue = (hashValue+0x165667b1) + (hashValue<<5);
		hashValue = (hashValue+0xd3a2646c) ^ (hashValue<<9);
		hashValue = (hashValue+0xfd7046c5) + (hashValue<<3);
		hashValue = (hashValue^0xb55a4f09) ^ (hashValue>>16);

		hashValue = hashValue % m_innerArrSize;//平均散列至目标区间；

		return true;
	}

public:
	int GetInnerValidEleNum() { return m_InnerValidEleNum; };

private:
	int                 m_InnerValidEleNum;

private:
	DsNewCS                m_AddDelLock;//添加删除元素锁，添加删除次数相对较少，因此用用没大问题；

private:
	ATOMIC_T            m_ActbitLev0;//第0层置位A；
	ATOMIC_T            m_ActbitLev1[32];//第1层置位A；
	ATOMIC_T            m_ActbitLev2[32][32];//第2层置位A；

private:
	///记录数组指定位置存在有效元素，主要用于ExploreValidEles()处理，为防止竞态条件，必须在真正加入元素之后执行;
	bool SetPosEleValid( unsigned int elePos )
	{
		NewLog( LOG_LEV_INFO, "设置hash位置%d元素有效", elePos );

		//计算hash值的三层bit置位位置；
		unsigned int lev0bit = 0;/*lev1pos*/
		unsigned int lev1bit = 0;/*lev2pos*/
		unsigned int lev2bit = 0;
		if ( !CalBitPos( elePos, lev0bit, lev1bit, lev2bit ) )
		{
			NewLog( LOG_LEV_ERROR, "SetPosEleValid，CalBitPos失败，值:%d", elePos );//不可能；
			return false;//不可能；
		}

		if ( (lev0bit>=32) 
			|| (lev1bit>=32)
			|| (lev2bit>=32)
			)
		{
			NewLog( LOG_LEV_ERROR, "SetPosEleValid，非法的bit位置，值:%d", elePos );//不可能；
			return false;
		}

		m_ValidPos[lev0bit][lev1bit] |= (0x00000001 << lev2bit);
		return true;
	};

	///清数组指定位置元素为不再有效，主要用于防止被ExploreValidEles()处理，为防止竞态条件，必须先设置此位置，再将相应元素删去;
	bool ClearPosEleValid( unsigned int elePos )
	{
		NewLog( LOG_LEV_INFO, "清hash位置%d元素有效设置", elePos );

		//计算hash值的三层bit置位位置；
		unsigned int lev0bit = 0;/*lev1pos*/
		unsigned int lev1bit = 0;/*lev2pos*/
		unsigned int lev2bit = 0;
		if ( !CalBitPos( elePos, lev0bit, lev1bit, lev2bit ) )
		{
			NewLog( LOG_LEV_ERROR, "SetPosEleValid，CalBitPos失败，值:%d", elePos );//不可能；
			return false;//不可能；
		}

		if ( (lev0bit>=32) 
			|| (lev1bit>=32)
			|| (lev2bit>=32)
			)
		{
			NewLog( LOG_LEV_ERROR, "SetPosEleValid，非法的bit位置，值:%d", elePos );//不可能；
			return false;
		}

		m_ValidPos[lev0bit][lev1bit] &= ~(0x00000001 << lev2bit);
		return true;
	};

private:
	I32                 m_ValidPos[32][32];//记录m_innerArr中的有效元素，主要用于ExploreValidEles();

private:
	unsigned int        m_innerArrSize;
	IFrameEle* *        m_innerArr;	

private:
	static unsigned int g_IncID;//这里没有使用interlockedincreament或automic_inc，保险起见，就简单在一个线程里分配这些就可以了，当然，加入ManFrame时，同样的ＩＤ值会导致临界区中hash到同一位置，此时也会导致重新申请一个ＩＤ号，从而不会发生ＩＤ号冲突的现象，06.05，在加入框架管理时，已使用了临界区保护本值。

private:
	DsEvent&             m_DsEvent;//框架线程等待事件；

///////////////////////////////////////////////////////////////////////////////////////////
//广播消息支持...
public:
	///添加一个待广播消息；
	bool AddOneBrocastMsg( NewMsgToPut* pBroMsg )
	{
		if ( NULL == pBroMsg )
		{
			return false;
		}
		m_vecToBrocast.push_back( NEW BrocastMsg(pBroMsg) );
		NewLog( LOG_LEV_DEBUG, "BrocastRefreshProc，当前待广播包数量%d", m_vecToBrocast.size() );
		m_curBroID = BrocastMsg::GetCurBroedID();//保存最新广播包ID号
		return true;
	}

	///广播包刷新，淘汰过时广播包
	void BrocastRefreshProc()
	{
		unsigned int curTime = GetTickCount();
		if ( !m_vecToBrocast.empty() )
		{
			//有待广播消息，删去过时需要淘汰者；
			for ( vector<BrocastMsg*>::iterator iter = m_vecToBrocast.begin(); iter != m_vecToBrocast.end(); )
			{
				if ( (*iter)->IsNeedDel( curTime ) )
				{
					delete (*iter); *iter = NULL;
					iter = m_vecToBrocast.erase( iter );
					continue;
				}
				break;
			}			
		}
		return;
	}

	///检查是否有针对此元素的广播消息，元素有效遍历时由mainframeloop调用；
	inline bool FrameEleCheckBrocast( IFrameEle* pFrameEle )
	{
		if ( NULL == pFrameEle )
		{
			return false;
		}

		unsigned int eleBroed = pFrameEle->GetBroedIDRef();//该元素最近发过的广播包ID号；
		if ( ( m_curBroID == eleBroed )//不能只有==比较，因为要确定队列中哪些包发过，哪些包没发过
			|| ( m_curBroID/*最新*/ - eleBroed/*已发*/ > 10000/*短时间内不可能有超过10000个未广播的广播包*/ ) ) 
		{
			/*
			同GetIfNewBro中的检测；
			若待发比已发新，则减结果将为一小于10000的正值，需要发送本包；
			若待发==已发，则说明当前包正是所检测元素发过的最新包，无需再发本包；
			若待发比已发旧，则减结果将为一极大正值，无需发送本包；
			*/
			//该元素的广播包号新于广播队列中所有待广播者，直接返回；
			return true;
		}

		if ( !m_vecToBrocast.empty() )
		{
			//有待广播消息
			int tosendnum = 0;
			for ( vector<BrocastMsg*>::reverse_iterator iter=m_vecToBrocast.rbegin(); iter!=m_vecToBrocast.rend(); ++iter )
			//for ( vector<BrocastMsg*>::iterator iter = m_vecToBrocast.begin(); iter != m_vecToBrocast.end(); ++iter )
			{
				if ( NULL == (*iter)->GetIfNewBro( eleBroed ) )
				{
					//没有了需要给此玩家发送的广播包；
					break;
				}
				++tosendnum;//本次需要发送的新广播包数量，这里只计数而不发，因为要保证发包顺序；				
			}

			if ( tosendnum > 0 )
			{
				NewMsgToPut* pToSend = NULL;
				int cursize = (int) m_vecToBrocast.size();
				int stpos = cursize - tosendnum;
				vector<BrocastMsg*>::iterator iter = m_vecToBrocast.begin();
				advance( iter, stpos );
				for ( iter=iter; iter!=m_vecToBrocast.end(); ++iter )
				{
					pToSend = (*iter)->GetIfNewBro( eleBroed );
					if ( NULL != pToSend )
					{
						pFrameEle->ExecSendBroMsg( pToSend );//真正执行广播包发送;					
					}
				}
			}

			pFrameEle->SetBroedID( m_curBroID );//重设已广播的最后一个广播包ID号；
		}

		return true;
	}

private:
	vector<BrocastMsg*>  m_vecToBrocast;//待广播包数组；
	unsigned int        m_curBroID;//当前最新广播包ID号，用于为新加入元素赋初始广播check号，也用于FrameEleCheckBrocast时快速掠过无新广播包时的情形。
//...广播消息支持
///////////////////////////////////////////////////////////////////////////////////////////

public:
	/*50,000,000次
    version      | GetLogValue | this
    linux debug  | 2000        | 600
    linux release| 1000        | 0
    win debug    | 4000        | 2000
    win release  | 1500        | 200
	*/
	static inline unsigned int GetLogValAsm( unsigned int inVal )
	{
		if ( 0 == inVal )
		{
			return 0xffffffff;
		}
		unsigned int rst=0xffffffff;
#ifdef WIN32
		__asm {
			bsf eax, inVal
			mov rst, eax
		}
#else //WIN32
		__asm__( "BSF %%eax, %0" : "=r"(rst) : "a"(inVal));
#endif //WIN32
		return rst;
	}

	///取输入数置位，例:0x00000001返回0(第0位被置位)
	static inline unsigned int GetLogValue( unsigned int inVal )
	{
		switch ( inVal )
		{
		case (0x00000001<<0):
			return 0;
			break;
		case (0x00000001<<1):
			return 1;
			break;
		case (0x00000001<<2):
			return 2;
			break;
		case (0x00000001<<3):
			return 3;
			break;
		case (0x00000001<<4):
			return 4;
			break;
		case (0x00000001<<5):
			return 5;
			break;
		case (0x00000001<<6):
			return 6;
			break;
		case (0x00000001<<7):
			return 7;
			break;
		case (0x00000001<<8):
			return 8;
			break;
		case (0x00000001<<9):
			return 9;
			break;
		case (0x00000001<<10):
			return 10;
			break;
		case (0x00000001<<11):
			return 11;
			break;
		case (0x00000001<<12):
			return 12;
			break;
		case (0x00000001<<13):
			return 13;
			break;
		case (0x00000001<<14):
			return 14;
			break;
		case (0x00000001<<15):
			return 15;
			break;
		case (0x00000001<<16):
			return 16;
			break;
		case (0x00000001<<17):
			return 17;
			break;
		case (0x00000001<<18):
			return 18;
			break;
		case (0x00000001<<19):
			return 19;
			break;
		case (0x00000001<<20):
			return 20;
			break;
		case (0x00000001<<21):
			return 21;
			break;
		case (0x00000001<<22):
			return 22;
			break;
		case (0x00000001<<23):
			return 23;
			break;
		case (0x00000001<<24):
			return 24;
			break;
		case (0x00000001<<25):
			return 25;
			break;
		case (0x00000001<<26):
			return 26;
			break;
		case (0x00000001<<27):
			return 27;
			break;
		case (0x00000001<<28):
			return 28;
			break;
		case (0x00000001<<29):
			return 29;
			break;
		case (0x00000001<<30):
			return 30;
			break;
		case (0x00000001<<31):
			return 31;
			break;
		default:
			{
				NewLog( LOG_LEV_ERROR, "GetLogValue，输入值%x错误", inVal );
				return 0xffffffff;
			}
		}
		//if ( inVal > (0x00000001<<16) )
		//{
		//	//(16,
		//	if ( inVal > (0x00000001<<24) )
		//	{ 
		//		//(24,
		//		if ( inVal > (0x00000001<<28) )
		//		{
		//			//(28,
		//			if ( inVal > (0x00000001<<30) )
		//			{
		//				//(30,
		//				return 31;
		//			} else {
		//				//(28,30]
		//				if ( inVal > (0x00000001<<29) )
		//				{
		//					return 30;
		//				} else {
		//					return 29;
		//				}
		//			}
		//		} else {
		//			//(24,28]
		//		}
		//	}
		//} else {
		//	//,16]
		//}
		//return -1;
	}
};

#endif //MAN_FRAME_H
