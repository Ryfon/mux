﻿/*

    应用所使用的各种对象池；

	by dzj, 09.05.25
*/

#include "apppoolobjs.h"
#ifdef TEST_CODE
#include "testplayer.h"
#else //TEST_CODE
#include "../../GateSrv_IOCP/GateSrv/Player/Player.h"
#endif //TEST_CODE

#include "pkgproc/pkgbuild.h"

T_MsgToPutPool*    g_MsgToPutPool   = NULL;//NewMsgToPut池；
T_TestPlayerPool*  g_TestPlayerPool = NULL;//TestPlayer池；

///初始化全局对象池;
bool InitGloblePools()
{
	g_MsgToPutPool   = NEW T_MsgToPutPool(5240*4);//NewMsgToPut池,>可能连接socket数*4(暂)；
	g_TestPlayerPool = NEW T_TestPlayerPool(5240);//TestPlayer池,>可能玩家数+池缓冲*2(暂)；
	return true;
}

///释放全局对象池;
bool ReleaseGloblePools()
{
	if ( NULL != g_TestPlayerPool )
	{
		delete g_TestPlayerPool; g_TestPlayerPool = NULL;
	}

	if ( NULL != g_MsgToPutPool )
	{
		delete g_MsgToPutPool; g_MsgToPutPool = NULL;
	}

	return true;
}

