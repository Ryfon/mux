﻿
#include "dslock.h"
//#include "testpool.h"

#include "apppoolobjs.h"

#include "buflog.h"

#ifdef WIN32
unsigned long g_tlsKey;//全局私有数据键；
#else //WIN32
pthread_key_t g_tlsKey;
#endif //WIN32

#ifndef WIN32
#include <sys/times.h>
unsigned long GetTickCount()
{
	tms tm;
	return (times(&tm))*10;/*目前linux系统，1秒=100tick*/
}
#endif //WIN32

ThreadPriData::ThreadPriData()
{
	pTlsLog = NEW CDsBufLog;
	pRcvClipro = NEW MUX_PROTO::CliProtocol;
#ifndef WIN32
	pSigJmpbuf = NULL;//线程异常跳转返回上下文；
#endif  //WIN32
	return;
}

ThreadPriData::~ThreadPriData()
{
	if ( NULL != pRcvClipro )
	{
		delete pRcvClipro; pRcvClipro = NULL;
	}
	if ( NULL != pTlsLog )
	{
		delete pTlsLog; pTlsLog = NULL;
	}
	return;
}

//分配私有键；
void MainInitTls()
{
#ifdef WIN32
	g_tlsKey = TlsAlloc();
#else //WIN32
	pthread_key_create( &g_tlsKey, NULL ); 
#endif //WIN32
	ThreadInitTls();
};

void MainEndTls()
{
	ThreadEndTls();
#ifdef WIN32
	TlsFree( g_tlsKey );
#else //WIN32
	pthread_key_delete( g_tlsKey );
#endif //WIN32
}

//每线程开始时调用以初始化本线程TLS；
void ThreadInitTls()
{
	if ( NULL == ThreadGetPriData() )
	{
#ifdef WIN32
		TlsSetValue( g_tlsKey, new ThreadPriData );
#else //WIN32
		pthread_setspecific( g_tlsKey, new ThreadPriData );
#endif //WIN32
	}
	return;
}

//每线程结束时调用以释放本线程TLS相关数据；
void ThreadEndTls()
{
	ThreadPriData* pPriData = ThreadGetPriData();
	if ( NULL != pPriData )
	{
		delete pPriData; pPriData = NULL;
		//以下重置tls，以防止以后Get到无效的pridata指针；
#ifdef WIN32
		TlsSetValue( g_tlsKey, NULL );
#else //WIN32
		pthread_setspecific( g_tlsKey, NULL );
#endif //WIN32
	}

	return;
}

//得到本线程私有数据指针；
ThreadPriData* ThreadGetPriData()
{
#ifdef WIN32
	return (ThreadPriData*) TlsGetValue( g_tlsKey );
#else //WIN32
	return (ThreadPriData*) pthread_getspecific( g_tlsKey );
#endif //WIN32	
}

#ifdef WIN32
#ifdef _DEBUG
void* __cdecl operator new( size_t nSize, const char* lpszFileName, int nLine )
{
	return _malloc_dbg( nSize, _NORMAL_BLOCK, lpszFileName, nLine );
}

void __cdecl operator delete( void* p, const char*, int )
{
	_free_dbg( p, _CLIENT_BLOCK );
}
#endif //_DEBUG
#endif //WIN32
