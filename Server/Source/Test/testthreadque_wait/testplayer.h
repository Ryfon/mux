﻿/*

    测试收发包性能;

	by dzj, 09.06.05
*/

#ifndef TEST_PLAYER_H
#define TEST_PLAYER_H

#include "lsocket.h"

#ifdef TEST_CODE

#ifdef WIN32

class CTestPlayer : public CLSocket
{
public:
	CTestPlayer() 
	{
		TestPlayerInit();
	};

	virtual ~CTestPlayer() {};

	///获得一个自身的对象实例，与DestorySelfIns对应;
	static CTestPlayer* CreateSelfIns();

	///释放自身，与CreateSelfIns对应；
	virtual void DestorySelfIns();

public:
	void TestPlayerInit()
	{
	}
};


#else //WIN32

#include "pkgproc/testprot.h"

class CTestPlayer : public CLSocket
{
public:
	CTestPlayer() 
	{
		TestPlayerInit();
	};

	virtual ~CTestPlayer() {};

	///获得一个自身的对象实例，与DestorySelfIns对应;
	static CTestPlayer* CreateSelfIns();

	///释放自身，与CreateSelfIns对应；；
	virtual void DestorySelfIns();

	///从LSocket继承者需要做的池对象初始化工作；
	virtual void SocketChildPoolInit()
	{
		TestPlayerInit();
	}

public:
	void TestPlayerInit()
	{
		m_rcvtimes = 0;
		m_lastrcvTime = 0;//上次收包时间；
		m_initTime = 0;//初始连上时间；
		m_maxrcvInterval = 0;//最大收包间隔；
		m_maxsendInterval = 0;//最大发包间隔；

		m_avgrcvInterval = 0;//平均收包间隔；
		m_avgsendInterval = 0;//平均发包间隔；

		m_lastrcvid = 0;
		m_lastsendid = 0;

		m_endTime = 0;
	}

	///以下CLSocket需要定义的接口
public:
	///收到消息；
#ifdef TEST_CODE
	virtual bool OnRcvedMsg( bool isBroPkg, unsigned int pkgid, unsigned int rcvLen );//测试代码，只传递是否广播包；
#else //TEST_CODE
	virtual bool OnPkgRcved( unsigned short wCmd, const char* pPkg, unsigned int rcvLen );//gatesrv使用，传递消息内容供应用处理；
#endif //TEST_CODE
	///被框架遍历；
	virtual bool OnBeExploredByFrame();
    ///连接成功后，给对象一个初始化自身的机会，每连接只执行一次；
	virtual bool OnConnedInit();

private:
	unsigned int m_lastrcvTime;//上次收包时间；
	unsigned int m_initTime;//初始连接时间；
	unsigned int m_maxrcvInterval;//最大收包间隔；
	unsigned int m_maxsendInterval;//最大发包间隔；

	unsigned int m_avgrcvInterval;//平均收包间隔；
	unsigned int m_avgsendInterval;//平均发包间隔；

private:
	unsigned int m_rcvtimes;//收包次数；

private:
	unsigned int m_lastrcvid;//接收次数；
	unsigned int m_lastsendid;//发送次数；

private:
	///测试随机断开，检查是否已到断开时刻；
	bool CheckIfTimeNeedDestory();//测试随机断开，检查是否已到断开时刻；

private:
	unsigned int m_endTime;//初始化时刻确定，用于决定何时断开;

};

#endif //#ifndef WIN32

#endif //TEST_CODE

#endif //#ifndef TEST_PLAYER_H

