﻿/**
    组包
	by dzj, 09.06.16
*/

#include "pkgbuild.h"


MUX_PROTO::CliProtocol CNewPkgBuild::m_clipro;

#ifdef USE_CRYPT
char CNewPkgBuild::m_sendBuf[NewMsgToPut::MAX_MSG_SIZE+sizeof(unsigned int)];//此处只需多存一个包序号而不需要存加密包长度
#endif //USE_CRYPT

