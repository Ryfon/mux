﻿#include "CliProtocol_T.h"

#ifndef WIN32
//#pragma GCC diagnostic ignored "-Wno-char-subscripts"
#endif  //WIN32

#define	FAILEDRETCODE		0XFFFFFFFF

MUX_PROTO::CliProtocol::CliProtocol()
{
	m_mapEnCodeFunc[0x0100]	=	&CliProtocol::EnCode__CGQueryPkg;
	m_mapDeCodeFunc[0x0100]	=	&CliProtocol::DeCode__CGQueryPkg;

	m_mapEnCodeFunc[0x0101]	=	&CliProtocol::EnCode__CGNetDetect;
	m_mapDeCodeFunc[0x0101]	=	&CliProtocol::DeCode__CGNetDetect;

	m_mapEnCodeFunc[0x0102]	=	&CliProtocol::EnCode__CGChat;
	m_mapDeCodeFunc[0x0102]	=	&CliProtocol::DeCode__CGChat;

	m_mapEnCodeFunc[0x0103]	=	&CliProtocol::EnCode__CGTimeCheck;
	m_mapDeCodeFunc[0x0103]	=	&CliProtocol::DeCode__CGTimeCheck;

	m_mapEnCodeFunc[0x0104]	=	&CliProtocol::EnCode__CGReqRoleInfo;
	m_mapDeCodeFunc[0x0104]	=	&CliProtocol::DeCode__CGReqRoleInfo;

	m_mapEnCodeFunc[0x0105]	=	&CliProtocol::EnCode__CGReqMonsterInfo;
	m_mapDeCodeFunc[0x0105]	=	&CliProtocol::DeCode__CGReqMonsterInfo;

	m_mapEnCodeFunc[0x0106]	=	&CliProtocol::EnCode__CGRun;
	m_mapDeCodeFunc[0x0106]	=	&CliProtocol::DeCode__CGRun;

	m_mapEnCodeFunc[0x0107]	=	&CliProtocol::EnCode__CGLogin;
	m_mapDeCodeFunc[0x0107]	=	&CliProtocol::DeCode__CGLogin;

	m_mapEnCodeFunc[0x0108]	=	&CliProtocol::EnCode__CGSelectRole;
	m_mapDeCodeFunc[0x0108]	=	&CliProtocol::DeCode__CGSelectRole;

	m_mapEnCodeFunc[0x0109]	=	&CliProtocol::EnCode__CGEnterWorld;
	m_mapDeCodeFunc[0x0109]	=	&CliProtocol::DeCode__CGEnterWorld;

	m_mapEnCodeFunc[0x010a]	=	&CliProtocol::EnCode__CGSwitchEnterNewMap;
	m_mapDeCodeFunc[0x010a]	=	&CliProtocol::DeCode__CGSwitchEnterNewMap;

	m_mapEnCodeFunc[0x010b]	=	&CliProtocol::EnCode__CGAttackMonster;
	m_mapDeCodeFunc[0x010b]	=	&CliProtocol::DeCode__CGAttackMonster;

	m_mapEnCodeFunc[0x010c]	=	&CliProtocol::EnCode__CGAttackPlayer;
	m_mapDeCodeFunc[0x010c]	=	&CliProtocol::DeCode__CGAttackPlayer;

	m_mapEnCodeFunc[0x010d]	=	&CliProtocol::EnCode__CGSwitchMap;
	m_mapDeCodeFunc[0x010d]	=	&CliProtocol::DeCode__CGSwitchMap;

	m_mapEnCodeFunc[0x010e]	=	&CliProtocol::EnCode__CGCreateRole;
	m_mapDeCodeFunc[0x010e]	=	&CliProtocol::DeCode__CGCreateRole;

	m_mapEnCodeFunc[0x010f]	=	&CliProtocol::EnCode__CGDeleteRole;
	m_mapDeCodeFunc[0x010f]	=	&CliProtocol::DeCode__CGDeleteRole;

	m_mapEnCodeFunc[0x0110]	=	&CliProtocol::EnCode__CGScriptChat;
	m_mapDeCodeFunc[0x0110]	=	&CliProtocol::DeCode__CGScriptChat;

	m_mapEnCodeFunc[0x0111]	=	&CliProtocol::EnCode__CGDefaultRebirth;
	m_mapDeCodeFunc[0x0111]	=	&CliProtocol::DeCode__CGDefaultRebirth;

	m_mapEnCodeFunc[0x0112]	=	&CliProtocol::EnCode__CGRebirthReady;
	m_mapDeCodeFunc[0x0112]	=	&CliProtocol::DeCode__CGRebirthReady;

	m_mapEnCodeFunc[0x0113]	=	&CliProtocol::EnCode__CGPlayerCombo;
	m_mapDeCodeFunc[0x0113]	=	&CliProtocol::DeCode__CGPlayerCombo;

	m_mapEnCodeFunc[0x0114]	=	&CliProtocol::EnCode__CGQueryItemPackageInfo;
	m_mapDeCodeFunc[0x0114]	=	&CliProtocol::DeCode__CGQueryItemPackageInfo;

	m_mapEnCodeFunc[0x0115]	=	&CliProtocol::EnCode__CGPickItem;
	m_mapDeCodeFunc[0x0115]	=	&CliProtocol::DeCode__CGPickItem;

	m_mapEnCodeFunc[0x0116]	=	&CliProtocol::EnCode__CGPickAllItems;
	m_mapDeCodeFunc[0x0116]	=	&CliProtocol::DeCode__CGPickAllItems;

	m_mapEnCodeFunc[0x0117]	=	&CliProtocol::EnCode__CGUseItem;
	m_mapDeCodeFunc[0x0117]	=	&CliProtocol::DeCode__CGUseItem;

	m_mapEnCodeFunc[0x0118]	=	&CliProtocol::EnCode__CGUnUseItem;
	m_mapDeCodeFunc[0x0118]	=	&CliProtocol::DeCode__CGUnUseItem;

	m_mapEnCodeFunc[0x0119]	=	&CliProtocol::EnCode__CGSWAPITEM;
	m_mapDeCodeFunc[0x0119]	=	&CliProtocol::DeCode__CGSWAPITEM;

	m_mapEnCodeFunc[0x011a]	=	&CliProtocol::EnCode__CGDropItem;
	m_mapDeCodeFunc[0x011a]	=	&CliProtocol::DeCode__CGDropItem;

	m_mapEnCodeFunc[0x011b]	=	&CliProtocol::EnCode__CGActiveMount;
	m_mapDeCodeFunc[0x011b]	=	&CliProtocol::DeCode__CGActiveMount;

	m_mapEnCodeFunc[0x011c]	=	&CliProtocol::EnCode__CGInviteMount;
	m_mapDeCodeFunc[0x011c]	=	&CliProtocol::DeCode__CGInviteMount;

	m_mapEnCodeFunc[0x011d]	=	&CliProtocol::EnCode__CGAgreeBeInvite;
	m_mapDeCodeFunc[0x011d]	=	&CliProtocol::DeCode__CGAgreeBeInvite;

	m_mapEnCodeFunc[0x011e]	=	&CliProtocol::EnCode__CGLeaveMountTeam;
	m_mapDeCodeFunc[0x011e]	=	&CliProtocol::DeCode__CGLeaveMountTeam;

	m_mapEnCodeFunc[0x011f]	=	&CliProtocol::EnCode__CGReqBuffEnd;
	m_mapDeCodeFunc[0x011f]	=	&CliProtocol::DeCode__CGReqBuffEnd;

	m_mapEnCodeFunc[0x0120]	=	&CliProtocol::EnCode__CGInvitePlayerJoinTeam;
	m_mapDeCodeFunc[0x0120]	=	&CliProtocol::DeCode__CGInvitePlayerJoinTeam;

	m_mapEnCodeFunc[0x0121]	=	&CliProtocol::EnCode__CGRecvInviteJoinTeamFeedBack;
	m_mapDeCodeFunc[0x0121]	=	&CliProtocol::DeCode__CGRecvInviteJoinTeamFeedBack;

	m_mapEnCodeFunc[0x0122]	=	&CliProtocol::EnCode__CGReqQuitTeam;
	m_mapDeCodeFunc[0x0122]	=	&CliProtocol::DeCode__CGReqQuitTeam;

	m_mapEnCodeFunc[0x0123]	=	&CliProtocol::EnCode__CGChangeTeamExpMode;
	m_mapDeCodeFunc[0x0123]	=	&CliProtocol::DeCode__CGChangeTeamExpMode;

	m_mapEnCodeFunc[0x0125]	=	&CliProtocol::EnCode__CGChangeLeader;
	m_mapDeCodeFunc[0x0125]	=	&CliProtocol::DeCode__CGChangeLeader;

	m_mapEnCodeFunc[0x0126]	=	&CliProtocol::EnCode__CGReqJoinTeam;
	m_mapDeCodeFunc[0x0126]	=	&CliProtocol::DeCode__CGReqJoinTeam;

	m_mapEnCodeFunc[0x0127]	=	&CliProtocol::EnCode__CGBanTeamMember;
	m_mapDeCodeFunc[0x0127]	=	&CliProtocol::DeCode__CGBanTeamMember;

	m_mapEnCodeFunc[0x0128]	=	&CliProtocol::EnCode__CGRecvReqJoinTeamFeedBack;
	m_mapDeCodeFunc[0x0128]	=	&CliProtocol::DeCode__CGRecvReqJoinTeamFeedBack;

	m_mapEnCodeFunc[0x0129]	=	&CliProtocol::EnCode__CGDestoryTeam;
	m_mapDeCodeFunc[0x0129]	=	&CliProtocol::DeCode__CGDestoryTeam;

	m_mapEnCodeFunc[0x012a]	=	&CliProtocol::EnCode__CGTeamSwitch;
	m_mapDeCodeFunc[0x012a]	=	&CliProtocol::DeCode__CGTeamSwitch;

	m_mapEnCodeFunc[0x012b]	=	&CliProtocol::EnCode__CGUseAssistSkill;
	m_mapDeCodeFunc[0x012b]	=	&CliProtocol::DeCode__CGUseAssistSkill;

	m_mapEnCodeFunc[0x012c]	=	&CliProtocol::EnCode__CGChangeTeamItemMode;
	m_mapDeCodeFunc[0x012c]	=	&CliProtocol::DeCode__CGChangeTeamItemMode;

	m_mapEnCodeFunc[0x012d]	=	&CliProtocol::EnCode__CGAddFriendReq;
	m_mapDeCodeFunc[0x012d]	=	&CliProtocol::DeCode__CGAddFriendReq;

	m_mapEnCodeFunc[0x012e]	=	&CliProtocol::EnCode__CGAddFriendResponse;
	m_mapDeCodeFunc[0x012e]	=	&CliProtocol::DeCode__CGAddFriendResponse;

	m_mapEnCodeFunc[0x012f]	=	&CliProtocol::EnCode__CGDelFriendReq;
	m_mapDeCodeFunc[0x012f]	=	&CliProtocol::DeCode__CGDelFriendReq;

	m_mapEnCodeFunc[0x0130]	=	&CliProtocol::EnCode__CGCreateFriendGroupReq;
	m_mapDeCodeFunc[0x0130]	=	&CliProtocol::DeCode__CGCreateFriendGroupReq;

	m_mapEnCodeFunc[0x0132]	=	&CliProtocol::EnCode__CGChgFriendGroupNameReq;
	m_mapDeCodeFunc[0x0132]	=	&CliProtocol::DeCode__CGChgFriendGroupNameReq;

	m_mapEnCodeFunc[0x0133]	=	&CliProtocol::EnCode__CGDelFriendGroupReq;
	m_mapDeCodeFunc[0x0133]	=	&CliProtocol::DeCode__CGDelFriendGroupReq;

	m_mapEnCodeFunc[0x0134]	=	&CliProtocol::EnCode__CGMoveFriendGroupReq;
	m_mapDeCodeFunc[0x0134]	=	&CliProtocol::DeCode__CGMoveFriendGroupReq;

	m_mapEnCodeFunc[0x0135]	=	&CliProtocol::EnCode__CGFriendChat;
	m_mapDeCodeFunc[0x0135]	=	&CliProtocol::DeCode__CGFriendChat;

	m_mapEnCodeFunc[0x0136]	=	&CliProtocol::EnCode__CGAddBlacklistReq;
	m_mapDeCodeFunc[0x0136]	=	&CliProtocol::DeCode__CGAddBlacklistReq;

	m_mapEnCodeFunc[0x0137]	=	&CliProtocol::EnCode__CGDelBlacklistReq;
	m_mapDeCodeFunc[0x0137]	=	&CliProtocol::DeCode__CGDelBlacklistReq;

	m_mapEnCodeFunc[0x0138]	=	&CliProtocol::EnCode__CGLockFoeReq;
	m_mapDeCodeFunc[0x0138]	=	&CliProtocol::DeCode__CGLockFoeReq;

	m_mapEnCodeFunc[0x0139]	=	&CliProtocol::EnCode__CGQueryFoeReq;
	m_mapDeCodeFunc[0x0139]	=	&CliProtocol::DeCode__CGQueryFoeReq;

	m_mapEnCodeFunc[0x013a]	=	&CliProtocol::EnCode__CGFriendToBlacklistReq;
	m_mapDeCodeFunc[0x013a]	=	&CliProtocol::DeCode__CGFriendToBlacklistReq;

	m_mapEnCodeFunc[0x013b]	=	&CliProtocol::EnCode__CGRollItemFeedBack;
	m_mapDeCodeFunc[0x013b]	=	&CliProtocol::DeCode__CGRollItemFeedBack;

	m_mapEnCodeFunc[0x013c]	=	&CliProtocol::EnCode__CGAddSthToPlate;
	m_mapDeCodeFunc[0x013c]	=	&CliProtocol::DeCode__CGAddSthToPlate;

	m_mapEnCodeFunc[0x013d]	=	&CliProtocol::EnCode__CGTakeSthFromPlate;
	m_mapDeCodeFunc[0x013d]	=	&CliProtocol::DeCode__CGTakeSthFromPlate;

	m_mapEnCodeFunc[0x013e]	=	&CliProtocol::EnCode__CGPlateExec;
	m_mapDeCodeFunc[0x013e]	=	&CliProtocol::DeCode__CGPlateExec;

	m_mapEnCodeFunc[0x013f]	=	&CliProtocol::EnCode__CGClosePlate;
	m_mapDeCodeFunc[0x013f]	=	&CliProtocol::DeCode__CGClosePlate;

	m_mapEnCodeFunc[0x0140]	=	&CliProtocol::EnCode__CGSendTradeReq;
	m_mapDeCodeFunc[0x0140]	=	&CliProtocol::DeCode__CGSendTradeReq;

	m_mapEnCodeFunc[0x0141]	=	&CliProtocol::EnCode__CGSendTradeReqResult;
	m_mapDeCodeFunc[0x0141]	=	&CliProtocol::DeCode__CGSendTradeReqResult;

	m_mapEnCodeFunc[0x0142]	=	&CliProtocol::EnCode__CGAddOrDeleteItem;
	m_mapDeCodeFunc[0x0142]	=	&CliProtocol::DeCode__CGAddOrDeleteItem;

	m_mapEnCodeFunc[0x0143]	=	&CliProtocol::EnCode__CGCancelTrade;
	m_mapDeCodeFunc[0x0143]	=	&CliProtocol::DeCode__CGCancelTrade;

	m_mapEnCodeFunc[0x0144]	=	&CliProtocol::EnCode__CGLockTrade;
	m_mapDeCodeFunc[0x0144]	=	&CliProtocol::DeCode__CGLockTrade;

	m_mapEnCodeFunc[0x0145]	=	&CliProtocol::EnCode__CGConfirmTrade;
	m_mapDeCodeFunc[0x0145]	=	&CliProtocol::DeCode__CGConfirmTrade;

	m_mapEnCodeFunc[0x0146]	=	&CliProtocol::EnCode__CGChangeReqSwitch;
	m_mapDeCodeFunc[0x0146]	=	&CliProtocol::DeCode__CGChangeReqSwitch;

	m_mapEnCodeFunc[0x0147]	=	&CliProtocol::EnCode__CGChangeTeamRollLevel;
	m_mapDeCodeFunc[0x0147]	=	&CliProtocol::DeCode__CGChangeTeamRollLevel;

	m_mapEnCodeFunc[0x0148]	=	&CliProtocol::EnCode__CGDelFoeReq;
	m_mapDeCodeFunc[0x0148]	=	&CliProtocol::DeCode__CGDelFoeReq;

	m_mapEnCodeFunc[0x0149]	=	&CliProtocol::EnCode__CGSetTradeMoney;
	m_mapDeCodeFunc[0x0149]	=	&CliProtocol::DeCode__CGSetTradeMoney;

	m_mapEnCodeFunc[0x014a]	=	&CliProtocol::EnCode__CGSplitItem;
	m_mapDeCodeFunc[0x014a]	=	&CliProtocol::DeCode__CGSplitItem;

	m_mapEnCodeFunc[0x014b]	=	&CliProtocol::EnCode__CGRepairWearByItem;
	m_mapDeCodeFunc[0x014b]	=	&CliProtocol::DeCode__CGRepairWearByItem;

	m_mapEnCodeFunc[0x014c]	=	&CliProtocol::EnCode__CGUpgradeSkill;
	m_mapDeCodeFunc[0x014c]	=	&CliProtocol::DeCode__CGUpgradeSkill;

	m_mapEnCodeFunc[0x014d]	=	&CliProtocol::EnCode__CGAddSpSkillReq;
	m_mapDeCodeFunc[0x014d]	=	&CliProtocol::DeCode__CGAddSpSkillReq;

	m_mapEnCodeFunc[0x014e]	=	&CliProtocol::EnCode__CGRequestRandomString;
	m_mapDeCodeFunc[0x014e]	=	&CliProtocol::DeCode__CGRequestRandomString;

	m_mapEnCodeFunc[0x014f]	=	&CliProtocol::EnCode__CGUserMD5Value;
	m_mapDeCodeFunc[0x014f]	=	&CliProtocol::DeCode__CGUserMD5Value;

	m_mapEnCodeFunc[0x0150]	=	&CliProtocol::EnCode__CGEquipItem;
	m_mapDeCodeFunc[0x0150]	=	&CliProtocol::DeCode__CGEquipItem;

	m_mapEnCodeFunc[0x0151]	=	&CliProtocol::EnCode__CGQueryTaskRecordInfo;
	m_mapDeCodeFunc[0x0151]	=	&CliProtocol::DeCode__CGQueryTaskRecordInfo;

	m_mapEnCodeFunc[0x0152]	=	&CliProtocol::EnCode__CGGetClientData;
	m_mapDeCodeFunc[0x0152]	=	&CliProtocol::DeCode__CGGetClientData;

	m_mapEnCodeFunc[0x0153]	=	&CliProtocol::EnCode__CGSaveClientData;
	m_mapDeCodeFunc[0x0153]	=	&CliProtocol::DeCode__CGSaveClientData;

	m_mapEnCodeFunc[0x0154]	=	&CliProtocol::EnCode__CGDeleteTask;
	m_mapDeCodeFunc[0x0154]	=	&CliProtocol::DeCode__CGDeleteTask;

	m_mapEnCodeFunc[0x0155]	=	&CliProtocol::EnCode__CGShareTask;
	m_mapDeCodeFunc[0x0155]	=	&CliProtocol::DeCode__CGShareTask;

	m_mapEnCodeFunc[0x0156]	=	&CliProtocol::EnCode__CGTimedWearIsZero;
	m_mapDeCodeFunc[0x0156]	=	&CliProtocol::DeCode__CGTimedWearIsZero;

	m_mapEnCodeFunc[0x0157]	=	&CliProtocol::EnCode__CGCreateChatGroup;
	m_mapDeCodeFunc[0x0157]	=	&CliProtocol::DeCode__CGCreateChatGroup;

	m_mapEnCodeFunc[0x0158]	=	&CliProtocol::EnCode__CGDestroyChatGroup;
	m_mapDeCodeFunc[0x0158]	=	&CliProtocol::DeCode__CGDestroyChatGroup;

	m_mapEnCodeFunc[0x0159]	=	&CliProtocol::EnCode__CGAddChatGroupMemberByOwner;
	m_mapDeCodeFunc[0x0159]	=	&CliProtocol::DeCode__CGAddChatGroupMemberByOwner;

	m_mapEnCodeFunc[0x015a]	=	&CliProtocol::EnCode__CGAddChatGroupMember;
	m_mapDeCodeFunc[0x015a]	=	&CliProtocol::DeCode__CGAddChatGroupMember;

	m_mapEnCodeFunc[0x015b]	=	&CliProtocol::EnCode__CGRemoveChatGroupMember;
	m_mapDeCodeFunc[0x015b]	=	&CliProtocol::DeCode__CGRemoveChatGroupMember;

	m_mapEnCodeFunc[0x015c]	=	&CliProtocol::EnCode__CGChatGroupSpeak;
	m_mapDeCodeFunc[0x015c]	=	&CliProtocol::DeCode__CGChatGroupSpeak;

	m_mapEnCodeFunc[0x015d]	=	&CliProtocol::EnCode__CGClinetSynchTime;
	m_mapDeCodeFunc[0x015d]	=	&CliProtocol::DeCode__CGClinetSynchTime;

	m_mapEnCodeFunc[0x015e]	=	&CliProtocol::EnCode__CGIrcEnterWorld;
	m_mapDeCodeFunc[0x015e]	=	&CliProtocol::DeCode__CGIrcEnterWorld;

	m_mapEnCodeFunc[0x015f]	=	&CliProtocol::EnCode__CGQueryShop;
	m_mapDeCodeFunc[0x015f]	=	&CliProtocol::DeCode__CGQueryShop;

	m_mapEnCodeFunc[0x0160]	=	&CliProtocol::EnCode__CGShopBuyReq;
	m_mapDeCodeFunc[0x0160]	=	&CliProtocol::DeCode__CGShopBuyReq;

	m_mapEnCodeFunc[0x0161]	=	&CliProtocol::EnCode__CGItemSoldReq;
	m_mapDeCodeFunc[0x0161]	=	&CliProtocol::DeCode__CGItemSoldReq;

	m_mapEnCodeFunc[0x0162]	=	&CliProtocol::EnCode__CGRepairWearByNPC;
	m_mapDeCodeFunc[0x0162]	=	&CliProtocol::DeCode__CGRepairWearByNPC;

	m_mapEnCodeFunc[0x0163]	=	&CliProtocol::EnCode__CGChangeBattleMode;
	m_mapDeCodeFunc[0x0163]	=	&CliProtocol::DeCode__CGChangeBattleMode;

	m_mapEnCodeFunc[0x0164]	=	&CliProtocol::EnCode__CGAddActivePtReq;
	m_mapDeCodeFunc[0x0164]	=	&CliProtocol::DeCode__CGAddActivePtReq;

	m_mapEnCodeFunc[0x0165]	=	&CliProtocol::EnCode__CGQueryEvilTime;
	m_mapDeCodeFunc[0x0165]	=	&CliProtocol::DeCode__CGQueryEvilTime;

	m_mapEnCodeFunc[0x0166]	=	&CliProtocol::EnCode__CGUseDamageItem;
	m_mapDeCodeFunc[0x0166]	=	&CliProtocol::DeCode__CGUseDamageItem;

	m_mapEnCodeFunc[0x0167]	=	&CliProtocol::EnCode__CGQueryRepairItemCost;
	m_mapDeCodeFunc[0x0167]	=	&CliProtocol::DeCode__CGQueryRepairItemCost;

	m_mapEnCodeFunc[0x0168]	=	&CliProtocol::EnCode__CGIrcQueryPlayerInfo;
	m_mapDeCodeFunc[0x0168]	=	&CliProtocol::DeCode__CGIrcQueryPlayerInfo;

	m_mapEnCodeFunc[0x0169]	=	&CliProtocol::EnCode__CGPlayerBeyondBlock;
	m_mapDeCodeFunc[0x0169]	=	&CliProtocol::DeCode__CGPlayerBeyondBlock;

	m_mapEnCodeFunc[0x016a]	=	&CliProtocol::EnCode__CGSwapEquipItem;
	m_mapDeCodeFunc[0x016a]	=	&CliProtocol::DeCode__CGSwapEquipItem;

	m_mapEnCodeFunc[0x016b]	=	&CliProtocol::EnCode__CGQueryItemPkgPage;
	m_mapDeCodeFunc[0x016b]	=	&CliProtocol::DeCode__CGQueryItemPkgPage;

	m_mapEnCodeFunc[0x016c]	=	&CliProtocol::EnCode__CGCreateUnionRequest;
	m_mapDeCodeFunc[0x016c]	=	&CliProtocol::DeCode__CGCreateUnionRequest;

	m_mapEnCodeFunc[0x016d]	=	&CliProtocol::EnCode__CGDestroyUnionRequest;
	m_mapDeCodeFunc[0x016d]	=	&CliProtocol::DeCode__CGDestroyUnionRequest;

	m_mapEnCodeFunc[0x016e]	=	&CliProtocol::EnCode__CGQueryUnionBasicRequest;
	m_mapDeCodeFunc[0x016e]	=	&CliProtocol::DeCode__CGQueryUnionBasicRequest;

	m_mapEnCodeFunc[0x016f]	=	&CliProtocol::EnCode__CGQueryUnionMembersRequest;
	m_mapDeCodeFunc[0x016f]	=	&CliProtocol::DeCode__CGQueryUnionMembersRequest;

	m_mapEnCodeFunc[0x0170]	=	&CliProtocol::EnCode__CGAddUnionMemberRequest;
	m_mapDeCodeFunc[0x0170]	=	&CliProtocol::DeCode__CGAddUnionMemberRequest;

	m_mapEnCodeFunc[0x0171]	=	&CliProtocol::EnCode__CGAddUnionMemberConfirmed;
	m_mapDeCodeFunc[0x0171]	=	&CliProtocol::DeCode__CGAddUnionMemberConfirmed;

	m_mapEnCodeFunc[0x0172]	=	&CliProtocol::EnCode__CGRemoveUnionMemberRequest;
	m_mapDeCodeFunc[0x0172]	=	&CliProtocol::DeCode__CGRemoveUnionMemberRequest;

	m_mapEnCodeFunc[0x0173]	=	&CliProtocol::EnCode__CGChangePetName;
	m_mapDeCodeFunc[0x0173]	=	&CliProtocol::DeCode__CGChangePetName;

	m_mapEnCodeFunc[0x0174]	=	&CliProtocol::EnCode__CGClosePet;
	m_mapDeCodeFunc[0x0174]	=	&CliProtocol::DeCode__CGClosePet;

	m_mapEnCodeFunc[0x0175]	=	&CliProtocol::EnCode__CGChangePetSkillState;
	m_mapDeCodeFunc[0x0175]	=	&CliProtocol::DeCode__CGChangePetSkillState;

	m_mapEnCodeFunc[0x0176]	=	&CliProtocol::EnCode__CGChangePetImage;
	m_mapDeCodeFunc[0x0176]	=	&CliProtocol::DeCode__CGChangePetImage;

	m_mapEnCodeFunc[0x0177]	=	&CliProtocol::EnCode__CGModifyUnionPosRighstReq;
	m_mapDeCodeFunc[0x0177]	=	&CliProtocol::DeCode__CGModifyUnionPosRighstReq;

	m_mapEnCodeFunc[0x0178]	=	&CliProtocol::EnCode__CGAddUnionPosReq;
	m_mapDeCodeFunc[0x0178]	=	&CliProtocol::DeCode__CGAddUnionPosReq;

	m_mapEnCodeFunc[0x0179]	=	&CliProtocol::EnCode__CGRemoveUnionPosReq;
	m_mapDeCodeFunc[0x0179]	=	&CliProtocol::DeCode__CGRemoveUnionPosReq;

	m_mapEnCodeFunc[0x017a]	=	&CliProtocol::EnCode__CGTransformUnionCaptionReq;
	m_mapDeCodeFunc[0x017a]	=	&CliProtocol::DeCode__CGTransformUnionCaptionReq;

	m_mapEnCodeFunc[0x017b]	=	&CliProtocol::EnCode__CGModifyUnionMemberTitleReq;
	m_mapDeCodeFunc[0x017b]	=	&CliProtocol::DeCode__CGModifyUnionMemberTitleReq;

	m_mapEnCodeFunc[0x017c]	=	&CliProtocol::EnCode__CGAdvanceUnionMemberPosReq;
	m_mapDeCodeFunc[0x017c]	=	&CliProtocol::DeCode__CGAdvanceUnionMemberPosReq;

	m_mapEnCodeFunc[0x017d]	=	&CliProtocol::EnCode__CGReduceUnionMemberPosReq;
	m_mapDeCodeFunc[0x017d]	=	&CliProtocol::DeCode__CGReduceUnionMemberPosReq;

	m_mapEnCodeFunc[0x017e]	=	&CliProtocol::EnCode__CGUnionChannelSpeekReq;
	m_mapDeCodeFunc[0x017e]	=	&CliProtocol::DeCode__CGUnionChannelSpeekReq;

	m_mapEnCodeFunc[0x017f]	=	&CliProtocol::EnCode__CGUpdateUnionPosCfgReq;
	m_mapDeCodeFunc[0x017f]	=	&CliProtocol::DeCode__CGUpdateUnionPosCfgReq;

	m_mapEnCodeFunc[0x0180]	=	&CliProtocol::EnCode__CGSummonPet;
	m_mapDeCodeFunc[0x0180]	=	&CliProtocol::DeCode__CGSummonPet;

	m_mapEnCodeFunc[0x0181]	=	&CliProtocol::EnCode__CGForbidUnionSpeekReq;
	m_mapDeCodeFunc[0x0181]	=	&CliProtocol::DeCode__CGForbidUnionSpeekReq;

	m_mapEnCodeFunc[0x0182]	=	&CliProtocol::EnCode__CGQueryRankInfoByPageIndex;
	m_mapDeCodeFunc[0x0182]	=	&CliProtocol::DeCode__CGQueryRankInfoByPageIndex;

	m_mapEnCodeFunc[0x0183]	=	&CliProtocol::EnCode__CGQueryRankInfoByPlayerName;
	m_mapDeCodeFunc[0x0183]	=	&CliProtocol::DeCode__CGQueryRankInfoByPlayerName;

	m_mapEnCodeFunc[0x0184]	=	&CliProtocol::EnCode__CGIssueUnionTaskReq;
	m_mapDeCodeFunc[0x0184]	=	&CliProtocol::DeCode__CGIssueUnionTaskReq;

	m_mapEnCodeFunc[0x0185]	=	&CliProtocol::EnCode__CGLearnUnionSkillReq;
	m_mapDeCodeFunc[0x0185]	=	&CliProtocol::DeCode__CGLearnUnionSkillReq;

	m_mapEnCodeFunc[0x0186]	=	&CliProtocol::EnCode__CGLeaveWaitQueue;
	m_mapDeCodeFunc[0x0186]	=	&CliProtocol::DeCode__CGLeaveWaitQueue;

	m_mapEnCodeFunc[0x0187]	=	&CliProtocol::EnCode__CGUsePartionSkill;
	m_mapDeCodeFunc[0x0187]	=	&CliProtocol::DeCode__CGUsePartionSkill;

	m_mapEnCodeFunc[0x0188]	=	&CliProtocol::EnCode__CGAdvanceUnionLevelReq;
	m_mapDeCodeFunc[0x0188]	=	&CliProtocol::DeCode__CGAdvanceUnionLevelReq;

	m_mapEnCodeFunc[0x0189]	=	&CliProtocol::EnCode__CGPostUnionBulletinReq;
	m_mapDeCodeFunc[0x0189]	=	&CliProtocol::DeCode__CGPostUnionBulletinReq;

	m_mapEnCodeFunc[0x018a]	=	&CliProtocol::EnCode__CGQueryAccountInfo;
	m_mapDeCodeFunc[0x018a]	=	&CliProtocol::DeCode__CGQueryAccountInfo;

	m_mapEnCodeFunc[0x018b]	=	&CliProtocol::EnCode__CGPurchaseItem;
	m_mapDeCodeFunc[0x018b]	=	&CliProtocol::DeCode__CGPurchaseItem;

	m_mapEnCodeFunc[0x018c]	=	&CliProtocol::EnCode__CGQueryVipInfo;
	m_mapDeCodeFunc[0x018c]	=	&CliProtocol::DeCode__CGQueryVipInfo;

	m_mapEnCodeFunc[0x018d]	=	&CliProtocol::EnCode__CGPurchaseVip;
	m_mapDeCodeFunc[0x018d]	=	&CliProtocol::DeCode__CGPurchaseVip;

	m_mapEnCodeFunc[0x018e]	=	&CliProtocol::EnCode__CGFetchVipItem;
	m_mapDeCodeFunc[0x018e]	=	&CliProtocol::DeCode__CGFetchVipItem;

	m_mapEnCodeFunc[0x018f]	=	&CliProtocol::EnCode__CGPurchaseGift;
	m_mapDeCodeFunc[0x018f]	=	&CliProtocol::DeCode__CGPurchaseGift;

	m_mapEnCodeFunc[0x0190]	=	&CliProtocol::EnCode__CGPetMove;
	m_mapDeCodeFunc[0x0190]	=	&CliProtocol::DeCode__CGPetMove;

	m_mapEnCodeFunc[0x0191]	=	&CliProtocol::EnCode__CGQueryUnionLogReq;
	m_mapDeCodeFunc[0x0191]	=	&CliProtocol::DeCode__CGQueryUnionLogReq;

	m_mapEnCodeFunc[0x0192]	=	&CliProtocol::EnCode__CGFetchNonVipItem;
	m_mapDeCodeFunc[0x0192]	=	&CliProtocol::DeCode__CGFetchNonVipItem;

	m_mapEnCodeFunc[0x0193]	=	&CliProtocol::EnCode__CGSendMailToPlayer;
	m_mapDeCodeFunc[0x0193]	=	&CliProtocol::DeCode__CGSendMailToPlayer;

	m_mapEnCodeFunc[0x0194]	=	&CliProtocol::EnCode__CGDeleteMail;
	m_mapDeCodeFunc[0x0194]	=	&CliProtocol::DeCode__CGDeleteMail;

	m_mapEnCodeFunc[0x0195]	=	&CliProtocol::EnCode__CGQueryMailListByPage;
	m_mapDeCodeFunc[0x0195]	=	&CliProtocol::DeCode__CGQueryMailListByPage;

	m_mapEnCodeFunc[0x0196]	=	&CliProtocol::EnCode__CGQueryMailDetailInfo;
	m_mapDeCodeFunc[0x0196]	=	&CliProtocol::DeCode__CGQueryMailDetailInfo;

	m_mapEnCodeFunc[0x0197]	=	&CliProtocol::EnCode__CGPickMailAttachItem;
	m_mapDeCodeFunc[0x0197]	=	&CliProtocol::DeCode__CGPickMailAttachItem;

	m_mapEnCodeFunc[0x0198]	=	&CliProtocol::EnCode__CGPickMailAttachMoney;
	m_mapDeCodeFunc[0x0198]	=	&CliProtocol::DeCode__CGPickMailAttachMoney;

	m_mapEnCodeFunc[0x0199]	=	&CliProtocol::EnCode__CGDeleteAllMail;
	m_mapDeCodeFunc[0x0199]	=	&CliProtocol::DeCode__CGDeleteAllMail;

	m_mapEnCodeFunc[0x019a]	=	&CliProtocol::EnCode__CGCancelQueue;
	m_mapDeCodeFunc[0x019a]	=	&CliProtocol::DeCode__CGCancelQueue;

	m_mapEnCodeFunc[0x019b]	=	&CliProtocol::EnCode__CGReqPlayerDetailInfo;
	m_mapDeCodeFunc[0x019b]	=	&CliProtocol::DeCode__CGReqPlayerDetailInfo;

	m_mapEnCodeFunc[0x019c]	=	&CliProtocol::EnCode__CGQuerySuitInfo;
	m_mapDeCodeFunc[0x019c]	=	&CliProtocol::DeCode__CGQuerySuitInfo;

	m_mapEnCodeFunc[0x019d]	=	&CliProtocol::EnCode__CGClearSuitInfo;
	m_mapDeCodeFunc[0x019d]	=	&CliProtocol::DeCode__CGClearSuitInfo;

	m_mapEnCodeFunc[0x019e]	=	&CliProtocol::EnCode__CGSaveSuitInfo;
	m_mapDeCodeFunc[0x019e]	=	&CliProtocol::DeCode__CGSaveSuitInfo;

	m_mapEnCodeFunc[0x019f]	=	&CliProtocol::EnCode__CGChanageSuit;
	m_mapDeCodeFunc[0x019f]	=	&CliProtocol::DeCode__CGChanageSuit;

	m_mapEnCodeFunc[0x01a0]	=	&CliProtocol::EnCode__CGPickCollectItem;
	m_mapDeCodeFunc[0x01a0]	=	&CliProtocol::DeCode__CGPickCollectItem;

	m_mapEnCodeFunc[0x01a1]	=	&CliProtocol::EnCode__CGPickAllCollectItem;
	m_mapDeCodeFunc[0x01a1]	=	&CliProtocol::DeCode__CGPickAllCollectItem;

	m_mapEnCodeFunc[0x01a2]	=	&CliProtocol::EnCode__CGDiscardCollectItem;
	m_mapDeCodeFunc[0x01a2]	=	&CliProtocol::DeCode__CGDiscardCollectItem;

	m_mapEnCodeFunc[0x01a3]	=	&CliProtocol::EnCode__CGPutItemToProtectPlate;
	m_mapDeCodeFunc[0x01a3]	=	&CliProtocol::DeCode__CGPutItemToProtectPlate;

	m_mapEnCodeFunc[0x01a4]	=	&CliProtocol::EnCode__CGConfirmProtectPlate;
	m_mapDeCodeFunc[0x01a4]	=	&CliProtocol::DeCode__CGConfirmProtectPlate;

	m_mapEnCodeFunc[0x01a5]	=	&CliProtocol::EnCode__CGReleaseSpecialProtect;
	m_mapDeCodeFunc[0x01a5]	=	&CliProtocol::DeCode__CGReleaseSpecialProtect;

	m_mapEnCodeFunc[0x01a6]	=	&CliProtocol::EnCode__CGCancelSpecialProtect;
	m_mapDeCodeFunc[0x01a6]	=	&CliProtocol::DeCode__CGCancelSpecialProtect;

	m_mapEnCodeFunc[0x01a7]	=	&CliProtocol::EnCode__CGTakeItemFromProtectPlate;
	m_mapDeCodeFunc[0x01a7]	=	&CliProtocol::DeCode__CGTakeItemFromProtectPlate;

	m_mapEnCodeFunc[0x01a8]	=	&CliProtocol::EnCode__CGEquipFashionSwitch;
	m_mapDeCodeFunc[0x01a8]	=	&CliProtocol::DeCode__CGEquipFashionSwitch;

	m_mapEnCodeFunc[0x01a9]	=	&CliProtocol::EnCode__CGEndAnamorphic;
	m_mapDeCodeFunc[0x01a9]	=	&CliProtocol::DeCode__CGEndAnamorphic;

	m_mapEnCodeFunc[0x1000]	=	&CliProtocol::EnCode__GCNetDetect;
	m_mapDeCodeFunc[0x1000]	=	&CliProtocol::DeCode__GCNetDetect;

	m_mapEnCodeFunc[0x1001]	=	&CliProtocol::EnCode__GCNetBias;
	m_mapDeCodeFunc[0x1001]	=	&CliProtocol::DeCode__GCNetBias;

	m_mapEnCodeFunc[0x1002]	=	&CliProtocol::EnCode__GCChat;
	m_mapDeCodeFunc[0x1002]	=	&CliProtocol::DeCode__GCChat;

	m_mapEnCodeFunc[0x1003]	=	&CliProtocol::EnCode__GCRoleattUpdate;
	m_mapDeCodeFunc[0x1003]	=	&CliProtocol::DeCode__GCRoleattUpdate;

	m_mapEnCodeFunc[0x1004]	=	&CliProtocol::EnCode__GCPlayerLevelUp;
	m_mapDeCodeFunc[0x1004]	=	&CliProtocol::DeCode__GCPlayerLevelUp;

	m_mapEnCodeFunc[0x1005]	=	&CliProtocol::EnCode__GCTmpInfoUpdate;
	m_mapDeCodeFunc[0x1005]	=	&CliProtocol::DeCode__GCTmpInfoUpdate;

	m_mapEnCodeFunc[0x1006]	=	&CliProtocol::EnCode__GCPlayerPkg;
	m_mapDeCodeFunc[0x1006]	=	&CliProtocol::DeCode__GCPlayerPkg;

	m_mapEnCodeFunc[0x1007]	=	&CliProtocol::EnCode__GCTimeCheck;
	m_mapDeCodeFunc[0x1007]	=	&CliProtocol::DeCode__GCTimeCheck;

	m_mapEnCodeFunc[0x1008]	=	&CliProtocol::EnCode__GCRun;
	m_mapDeCodeFunc[0x1008]	=	&CliProtocol::DeCode__GCRun;

	m_mapEnCodeFunc[0x1009]	=	&CliProtocol::EnCode__GCPlayerPosInfo;
	m_mapDeCodeFunc[0x1009]	=	&CliProtocol::DeCode__GCPlayerPosInfo;

	m_mapEnCodeFunc[0x100a]	=	&CliProtocol::EnCode__GCPlayerSelfInfo;
	m_mapDeCodeFunc[0x100a]	=	&CliProtocol::DeCode__GCPlayerSelfInfo;

	m_mapEnCodeFunc[0x100b]	=	&CliProtocol::EnCode__GCPlayerAppear;
	m_mapDeCodeFunc[0x100b]	=	&CliProtocol::DeCode__GCPlayerAppear;

	m_mapEnCodeFunc[0x100c]	=	&CliProtocol::EnCode__GCPlayerLeave;
	m_mapDeCodeFunc[0x100c]	=	&CliProtocol::DeCode__GCPlayerLeave;

	m_mapEnCodeFunc[0x100d]	=	&CliProtocol::EnCode__GCPlayerLogin;
	m_mapDeCodeFunc[0x100d]	=	&CliProtocol::DeCode__GCPlayerLogin;

	m_mapEnCodeFunc[0x100e]	=	&CliProtocol::EnCode__GCPlayerRole;
	m_mapDeCodeFunc[0x100e]	=	&CliProtocol::DeCode__GCPlayerRole;

	m_mapEnCodeFunc[0x100f]	=	&CliProtocol::EnCode__GCOtherPlayerInfo;
	m_mapDeCodeFunc[0x100f]	=	&CliProtocol::DeCode__GCOtherPlayerInfo;

	m_mapEnCodeFunc[0x1010]	=	&CliProtocol::EnCode__GCSelectRole;
	m_mapDeCodeFunc[0x1010]	=	&CliProtocol::DeCode__GCSelectRole;

	m_mapEnCodeFunc[0x1011]	=	&CliProtocol::EnCode__GCCreateRole;
	m_mapDeCodeFunc[0x1011]	=	&CliProtocol::DeCode__GCCreateRole;

	m_mapEnCodeFunc[0x1012]	=	&CliProtocol::EnCode__GCDeleteRole;
	m_mapDeCodeFunc[0x1012]	=	&CliProtocol::DeCode__GCDeleteRole;

	m_mapEnCodeFunc[0x1013]	=	&CliProtocol::EnCode__GCPlayerDying;
	m_mapDeCodeFunc[0x1013]	=	&CliProtocol::DeCode__GCPlayerDying;

	m_mapEnCodeFunc[0x1014]	=	&CliProtocol::EnCode__GCRebirthInfo;
	m_mapDeCodeFunc[0x1014]	=	&CliProtocol::DeCode__GCRebirthInfo;

	m_mapEnCodeFunc[0x1015]	=	&CliProtocol::EnCode__GCMonsterBorn;
	m_mapDeCodeFunc[0x1015]	=	&CliProtocol::DeCode__GCMonsterBorn;

	m_mapEnCodeFunc[0x1016]	=	&CliProtocol::EnCode__GCMonsterMove;
	m_mapDeCodeFunc[0x1016]	=	&CliProtocol::DeCode__GCMonsterMove;

	m_mapEnCodeFunc[0x1017]	=	&CliProtocol::EnCode__GCMonsterDisappear;
	m_mapDeCodeFunc[0x1017]	=	&CliProtocol::DeCode__GCMonsterDisappear;

	m_mapEnCodeFunc[0x1018]	=	&CliProtocol::EnCode__GCPlayerAttackTarget;
	m_mapDeCodeFunc[0x1018]	=	&CliProtocol::DeCode__GCPlayerAttackTarget;

	m_mapEnCodeFunc[0x1019]	=	&CliProtocol::EnCode__GCMonsterAttackPlayer;
	m_mapDeCodeFunc[0x1019]	=	&CliProtocol::DeCode__GCMonsterAttackPlayer;

	m_mapEnCodeFunc[0x101a]	=	&CliProtocol::EnCode__GCSwitchMap;
	m_mapDeCodeFunc[0x101a]	=	&CliProtocol::DeCode__GCSwitchMap;

	m_mapEnCodeFunc[0x101b]	=	&CliProtocol::EnCode__GCPlayerAttackPlayer;
	m_mapDeCodeFunc[0x101b]	=	&CliProtocol::DeCode__GCPlayerAttackPlayer;

	m_mapEnCodeFunc[0x101c]	=	&CliProtocol::EnCode__GCMonsterInfo;
	m_mapDeCodeFunc[0x101c]	=	&CliProtocol::DeCode__GCMonsterInfo;

	m_mapEnCodeFunc[0x101d]	=	&CliProtocol::EnCode__GCPlayerCombo;
	m_mapDeCodeFunc[0x101d]	=	&CliProtocol::DeCode__GCPlayerCombo;

	m_mapEnCodeFunc[0x101e]	=	&CliProtocol::EnCode__GCSrvError;
	m_mapDeCodeFunc[0x101e]	=	&CliProtocol::DeCode__GCSrvError;

	m_mapEnCodeFunc[0x101f]	=	&CliProtocol::EnCode__GCItemPackageAppear;
	m_mapDeCodeFunc[0x101f]	=	&CliProtocol::DeCode__GCItemPackageAppear;

	m_mapEnCodeFunc[0x1020]	=	&CliProtocol::EnCode__GCItemsPkgDisappear;
	m_mapDeCodeFunc[0x1020]	=	&CliProtocol::DeCode__GCItemsPkgDisappear;

	m_mapEnCodeFunc[0x1021]	=	&CliProtocol::EnCode__GCItemPackageInfo;
	m_mapDeCodeFunc[0x1021]	=	&CliProtocol::DeCode__GCItemPackageInfo;

	m_mapEnCodeFunc[0x1022]	=	&CliProtocol::EnCode__GCPlayerPickItem;
	m_mapDeCodeFunc[0x1022]	=	&CliProtocol::DeCode__GCPlayerPickItem;

	m_mapEnCodeFunc[0x1023]	=	&CliProtocol::EnCode__GCWeaponPackageInfo;
	m_mapDeCodeFunc[0x1023]	=	&CliProtocol::DeCode__GCWeaponPackageInfo;

	m_mapEnCodeFunc[0x1024]	=	&CliProtocol::EnCode__GCEquipPackageInfo;
	m_mapDeCodeFunc[0x1024]	=	&CliProtocol::DeCode__GCEquipPackageInfo;

	m_mapEnCodeFunc[0x1025]	=	&CliProtocol::EnCode__GCPickItemFail;
	m_mapDeCodeFunc[0x1025]	=	&CliProtocol::DeCode__GCPickItemFail;

	m_mapEnCodeFunc[0x1026]	=	&CliProtocol::EnCode__GCUseItem;
	m_mapDeCodeFunc[0x1026]	=	&CliProtocol::DeCode__GCUseItem;

	m_mapEnCodeFunc[0x1027]	=	&CliProtocol::EnCode__GCUseItemResult;
	m_mapDeCodeFunc[0x1027]	=	&CliProtocol::DeCode__GCUseItemResult;

	m_mapEnCodeFunc[0x1028]	=	&CliProtocol::EnCode__GCUnUseItem;
	m_mapDeCodeFunc[0x1028]	=	&CliProtocol::DeCode__GCUnUseItem;

	m_mapEnCodeFunc[0x1029]	=	&CliProtocol::EnCode__GCUnUseItemResult;
	m_mapDeCodeFunc[0x1029]	=	&CliProtocol::DeCode__GCUnUseItemResult;

	m_mapEnCodeFunc[0x102a]	=	&CliProtocol::EnCode__GCSwapItemResult;
	m_mapDeCodeFunc[0x102a]	=	&CliProtocol::DeCode__GCSwapItemResult;

	m_mapEnCodeFunc[0x102b]	=	&CliProtocol::EnCode__GCItemDetialInfo;
	m_mapDeCodeFunc[0x102b]	=	&CliProtocol::DeCode__GCItemDetialInfo;

	m_mapEnCodeFunc[0x102c]	=	&CliProtocol::EnCode__GCEquipConciseInfo;
	m_mapDeCodeFunc[0x102c]	=	&CliProtocol::DeCode__GCEquipConciseInfo;

	m_mapEnCodeFunc[0x102d]	=	&CliProtocol::EnCode__GCItemPkgConciseInfo;
	m_mapDeCodeFunc[0x102d]	=	&CliProtocol::DeCode__GCItemPkgConciseInfo;

	m_mapEnCodeFunc[0x102e]	=	&CliProtocol::EnCode__GCDropItemResult;
	m_mapDeCodeFunc[0x102e]	=	&CliProtocol::DeCode__GCDropItemResult;

	m_mapEnCodeFunc[0x102f]	=	&CliProtocol::EnCode__GCMoneyUpdate;
	m_mapDeCodeFunc[0x102f]	=	&CliProtocol::DeCode__GCMoneyUpdate;

	m_mapEnCodeFunc[0x1030]	=	&CliProtocol::EnCode__GCActiveMountFail;
	m_mapDeCodeFunc[0x1030]	=	&CliProtocol::DeCode__GCActiveMountFail;

	m_mapEnCodeFunc[0x1031]	=	&CliProtocol::EnCode__GCMountLeader;
	m_mapDeCodeFunc[0x1031]	=	&CliProtocol::DeCode__GCMountLeader;

	m_mapEnCodeFunc[0x1032]	=	&CliProtocol::EnCode__GCMountAssist;
	m_mapDeCodeFunc[0x1032]	=	&CliProtocol::DeCode__GCMountAssist;

	m_mapEnCodeFunc[0x1033]	=	&CliProtocol::EnCode__GCMountTeamInfo;
	m_mapDeCodeFunc[0x1033]	=	&CliProtocol::DeCode__GCMountTeamInfo;

	m_mapEnCodeFunc[0x1034]	=	&CliProtocol::EnCode__GCMountTeamDisBand;
	m_mapDeCodeFunc[0x1034]	=	&CliProtocol::DeCode__GCMountTeamDisBand;

	m_mapEnCodeFunc[0x1035]	=	&CliProtocol::EnCode__GCPlayerLeaveMountTeam;
	m_mapDeCodeFunc[0x1035]	=	&CliProtocol::DeCode__GCPlayerLeaveMountTeam;

	m_mapEnCodeFunc[0x1036]	=	&CliProtocol::EnCode__GCJoinMountTeam;
	m_mapDeCodeFunc[0x1036]	=	&CliProtocol::DeCode__GCJoinMountTeam;

	m_mapEnCodeFunc[0x1037]	=	&CliProtocol::EnCode__GCBeInviteMount;
	m_mapDeCodeFunc[0x1037]	=	&CliProtocol::DeCode__GCBeInviteMount;

	m_mapEnCodeFunc[0x1038]	=	&CliProtocol::EnCode__GCInviteResult;
	m_mapDeCodeFunc[0x1038]	=	&CliProtocol::DeCode__GCInviteResult;

	m_mapEnCodeFunc[0x1039]	=	&CliProtocol::EnCode__GCMountTeamMove;
	m_mapDeCodeFunc[0x1039]	=	&CliProtocol::DeCode__GCMountTeamMove;

	m_mapEnCodeFunc[0x103a]	=	&CliProtocol::EnCode__GCPlayerBuffStart;
	m_mapDeCodeFunc[0x103a]	=	&CliProtocol::DeCode__GCPlayerBuffStart;

	m_mapEnCodeFunc[0x103b]	=	&CliProtocol::EnCode__GCPlayerBuffEnd;
	m_mapDeCodeFunc[0x103b]	=	&CliProtocol::DeCode__GCPlayerBuffEnd;

	m_mapEnCodeFunc[0x103c]	=	&CliProtocol::EnCode__GCPlayerBuffUpdate;
	m_mapDeCodeFunc[0x103c]	=	&CliProtocol::DeCode__GCPlayerBuffUpdate;

	m_mapEnCodeFunc[0x103d]	=	&CliProtocol::EnCode__GCMonsterBuffStart;
	m_mapDeCodeFunc[0x103d]	=	&CliProtocol::DeCode__GCMonsterBuffStart;

	m_mapEnCodeFunc[0x103e]	=	&CliProtocol::EnCode__GCMonsterBuffUpdate;
	m_mapDeCodeFunc[0x103e]	=	&CliProtocol::DeCode__GCMonsterBuffUpdate;

	m_mapEnCodeFunc[0x103f]	=	&CliProtocol::EnCode__GCReqBuffEnd;
	m_mapDeCodeFunc[0x103f]	=	&CliProtocol::DeCode__GCReqBuffEnd;

	m_mapEnCodeFunc[0x1040]	=	&CliProtocol::EnCode__GCInvitePlayerJoinTeam;
	m_mapDeCodeFunc[0x1040]	=	&CliProtocol::DeCode__GCInvitePlayerJoinTeam;

	m_mapEnCodeFunc[0x1041]	=	&CliProtocol::EnCode__GCRecvInviteJoinTeam;
	m_mapDeCodeFunc[0x1041]	=	&CliProtocol::DeCode__GCRecvInviteJoinTeam;

	m_mapEnCodeFunc[0x1042]	=	&CliProtocol::EnCode__GCRecvInviteJoinTeamFeedBackError;
	m_mapDeCodeFunc[0x1042]	=	&CliProtocol::DeCode__GCRecvInviteJoinTeamFeedBackError;

	m_mapEnCodeFunc[0x1043]	=	&CliProtocol::EnCode__GCReqQuitTeam;
	m_mapDeCodeFunc[0x1043]	=	&CliProtocol::DeCode__GCReqQuitTeam;

	m_mapEnCodeFunc[0x1045]	=	&CliProtocol::EnCode__GCChangeTeamExpMode;
	m_mapDeCodeFunc[0x1045]	=	&CliProtocol::DeCode__GCChangeTeamExpMode;

	m_mapEnCodeFunc[0x1046]	=	&CliProtocol::EnCode__GCChangeLeader;
	m_mapDeCodeFunc[0x1046]	=	&CliProtocol::DeCode__GCChangeLeader;

	m_mapEnCodeFunc[0x1047]	=	&CliProtocol::EnCode__GCTeamMemberDetailInfoUpdate;
	m_mapDeCodeFunc[0x1047]	=	&CliProtocol::DeCode__GCTeamMemberDetailInfoUpdate;

	m_mapEnCodeFunc[0x1048]	=	&CliProtocol::EnCode__GCReqJoinTeamError;
	m_mapDeCodeFunc[0x1048]	=	&CliProtocol::DeCode__GCReqJoinTeamError;

	m_mapEnCodeFunc[0x1049]	=	&CliProtocol::EnCode__GCBanTeamMember;
	m_mapDeCodeFunc[0x1049]	=	&CliProtocol::DeCode__GCBanTeamMember;

	m_mapEnCodeFunc[0x105a]	=	&CliProtocol::EnCode__GCRecvReqJoinTeamFeedBackError;
	m_mapDeCodeFunc[0x105a]	=	&CliProtocol::DeCode__GCRecvReqJoinTeamFeedBackError;

	m_mapEnCodeFunc[0x105b]	=	&CliProtocol::EnCode__GCDestoryTeam;
	m_mapDeCodeFunc[0x105b]	=	&CliProtocol::DeCode__GCDestoryTeam;

	m_mapEnCodeFunc[0x105c]	=	&CliProtocol::EnCode__GCTeamSwitch;
	m_mapDeCodeFunc[0x105c]	=	&CliProtocol::DeCode__GCTeamSwitch;

	m_mapEnCodeFunc[0x105d]	=	&CliProtocol::EnCode__GCPickItemSuccess;
	m_mapDeCodeFunc[0x105d]	=	&CliProtocol::DeCode__GCPickItemSuccess;

	m_mapEnCodeFunc[0x105e]	=	&CliProtocol::EnCode__GCUseAssistSkill;
	m_mapDeCodeFunc[0x105e]	=	&CliProtocol::DeCode__GCUseAssistSkill;

	m_mapEnCodeFunc[0x105f]	=	&CliProtocol::EnCode__GCSetPlayerPos;
	m_mapDeCodeFunc[0x105f]	=	&CliProtocol::DeCode__GCSetPlayerPos;

	m_mapEnCodeFunc[0x1060]	=	&CliProtocol::EnCode__GCChangeTeamItemMode;
	m_mapDeCodeFunc[0x1060]	=	&CliProtocol::DeCode__GCChangeTeamItemMode;

	m_mapEnCodeFunc[0x1061]	=	&CliProtocol::EnCode__GCFriendGroups;
	m_mapDeCodeFunc[0x1061]	=	&CliProtocol::DeCode__GCFriendGroups;

	m_mapEnCodeFunc[0x1062]	=	&CliProtocol::EnCode__GCPlayerFriends;
	m_mapDeCodeFunc[0x1062]	=	&CliProtocol::DeCode__GCPlayerFriends;

	m_mapEnCodeFunc[0x1063]	=	&CliProtocol::EnCode__GCFriendInfoUpdate;
	m_mapDeCodeFunc[0x1063]	=	&CliProtocol::DeCode__GCFriendInfoUpdate;

	m_mapEnCodeFunc[0x1064]	=	&CliProtocol::EnCode__GCAddFriendReq;
	m_mapDeCodeFunc[0x1064]	=	&CliProtocol::DeCode__GCAddFriendReq;

	m_mapEnCodeFunc[0x1065]	=	&CliProtocol::EnCode__GCDelFriendResponse;
	m_mapDeCodeFunc[0x1065]	=	&CliProtocol::DeCode__GCDelFriendResponse;

	m_mapEnCodeFunc[0x1066]	=	&CliProtocol::EnCode__GCCreateFriendGroupResponse;
	m_mapDeCodeFunc[0x1066]	=	&CliProtocol::DeCode__GCCreateFriendGroupResponse;

	m_mapEnCodeFunc[0x1067]	=	&CliProtocol::EnCode__GCChgFriendGroupNameResponse;
	m_mapDeCodeFunc[0x1067]	=	&CliProtocol::DeCode__GCChgFriendGroupNameResponse;

	m_mapEnCodeFunc[0x1068]	=	&CliProtocol::EnCode__GCDelFriendGroupResponse;
	m_mapDeCodeFunc[0x1068]	=	&CliProtocol::DeCode__GCDelFriendGroupResponse;

	m_mapEnCodeFunc[0x1069]	=	&CliProtocol::EnCode__GCMoveFriendGroupResponse;
	m_mapDeCodeFunc[0x1069]	=	&CliProtocol::DeCode__GCMoveFriendGroupResponse;

	m_mapEnCodeFunc[0x106a]	=	&CliProtocol::EnCode__GCFriendChat;
	m_mapDeCodeFunc[0x106a]	=	&CliProtocol::DeCode__GCFriendChat;

	m_mapEnCodeFunc[0x106b]	=	&CliProtocol::EnCode__GCPlayerBlacklist;
	m_mapDeCodeFunc[0x106b]	=	&CliProtocol::DeCode__GCPlayerBlacklist;

	m_mapEnCodeFunc[0x106c]	=	&CliProtocol::EnCode__GCAddBlacklistResponse;
	m_mapDeCodeFunc[0x106c]	=	&CliProtocol::DeCode__GCAddBlacklistResponse;

	m_mapEnCodeFunc[0x106d]	=	&CliProtocol::EnCode__GCDelBlacklistResponse;
	m_mapDeCodeFunc[0x106d]	=	&CliProtocol::DeCode__GCDelBlacklistResponse;

	m_mapEnCodeFunc[0x106e]	=	&CliProtocol::EnCode__GCFoeList;
	m_mapDeCodeFunc[0x106e]	=	&CliProtocol::DeCode__GCFoeList;

	m_mapEnCodeFunc[0x106f]	=	&CliProtocol::EnCode__GCNewFoe;
	m_mapDeCodeFunc[0x106f]	=	&CliProtocol::DeCode__GCNewFoe;

	m_mapEnCodeFunc[0x1070]	=	&CliProtocol::EnCode__GCDelFoeResponse;
	m_mapDeCodeFunc[0x1070]	=	&CliProtocol::DeCode__GCDelFoeResponse;

	m_mapEnCodeFunc[0x1071]	=	&CliProtocol::EnCode__GCLockFoeResponse;
	m_mapDeCodeFunc[0x1071]	=	&CliProtocol::DeCode__GCLockFoeResponse;

	m_mapEnCodeFunc[0x1072]	=	&CliProtocol::EnCode__GCQueryFoeResponse;
	m_mapDeCodeFunc[0x1072]	=	&CliProtocol::DeCode__GCQueryFoeResponse;

	m_mapEnCodeFunc[0x1073]	=	&CliProtocol::EnCode__GCFriendToBlacklistResponse;
	m_mapDeCodeFunc[0x1073]	=	&CliProtocol::DeCode__GCFriendToBlacklistResponse;

	m_mapEnCodeFunc[0x1074]	=	&CliProtocol::EnCode__GCAddFriendResponse;
	m_mapDeCodeFunc[0x1074]	=	&CliProtocol::DeCode__GCAddFriendResponse;

	m_mapEnCodeFunc[0x1075]	=	&CliProtocol::EnCode__GCReqAgreeRollItem;
	m_mapDeCodeFunc[0x1075]	=	&CliProtocol::DeCode__GCReqAgreeRollItem;

	m_mapEnCodeFunc[0x1076]	=	&CliProtocol::EnCode__GCRollItemResult;
	m_mapDeCodeFunc[0x1076]	=	&CliProtocol::DeCode__GCRollItemResult;

	m_mapEnCodeFunc[0x1077]	=	&CliProtocol::EnCode__GCShowPlate;
	m_mapDeCodeFunc[0x1077]	=	&CliProtocol::DeCode__GCShowPlate;

	m_mapEnCodeFunc[0x1078]	=	&CliProtocol::EnCode__GCAddSthToPlateRst;
	m_mapDeCodeFunc[0x1078]	=	&CliProtocol::DeCode__GCAddSthToPlateRst;

	m_mapEnCodeFunc[0x1079]	=	&CliProtocol::EnCode__GCTakeSthFromPlateRst;
	m_mapDeCodeFunc[0x1079]	=	&CliProtocol::DeCode__GCTakeSthFromPlateRst;

	m_mapEnCodeFunc[0x107a]	=	&CliProtocol::EnCode__GCPlateExecRst;
	m_mapDeCodeFunc[0x107a]	=	&CliProtocol::DeCode__GCPlateExecRst;

	m_mapEnCodeFunc[0x107b]	=	&CliProtocol::EnCode__GCClosePlate;
	m_mapDeCodeFunc[0x107b]	=	&CliProtocol::DeCode__GCClosePlate;

	m_mapEnCodeFunc[0x107c]	=	&CliProtocol::EnCode__GCSendTradeReqError;
	m_mapDeCodeFunc[0x107c]	=	&CliProtocol::DeCode__GCSendTradeReqError;

	m_mapEnCodeFunc[0x107d]	=	&CliProtocol::EnCode__GCRecvTradeReq;
	m_mapDeCodeFunc[0x107d]	=	&CliProtocol::DeCode__GCRecvTradeReq;

	m_mapEnCodeFunc[0x107e]	=	&CliProtocol::EnCode__GCSendTradeReqResult;
	m_mapDeCodeFunc[0x107e]	=	&CliProtocol::DeCode__GCSendTradeReqResult;

	m_mapEnCodeFunc[0x107f]	=	&CliProtocol::EnCode__GCAddOrDeleteItem;
	m_mapDeCodeFunc[0x107f]	=	&CliProtocol::DeCode__GCAddOrDeleteItem;

	m_mapEnCodeFunc[0x1080]	=	&CliProtocol::EnCode__GCCancelTrade;
	m_mapDeCodeFunc[0x1080]	=	&CliProtocol::DeCode__GCCancelTrade;

	m_mapEnCodeFunc[0x1081]	=	&CliProtocol::EnCode__GCLockTrade;
	m_mapDeCodeFunc[0x1081]	=	&CliProtocol::DeCode__GCLockTrade;

	m_mapEnCodeFunc[0x1082]	=	&CliProtocol::EnCode__GCConfirmTrade;
	m_mapDeCodeFunc[0x1082]	=	&CliProtocol::DeCode__GCConfirmTrade;

	m_mapEnCodeFunc[0x1083]	=	&CliProtocol::EnCode__GCChangeReqSwitch;
	m_mapDeCodeFunc[0x1083]	=	&CliProtocol::DeCode__GCChangeReqSwitch;

	m_mapEnCodeFunc[0x1084]	=	&CliProtocol::EnCode__GCChangeTeamRollLevel;
	m_mapDeCodeFunc[0x1084]	=	&CliProtocol::DeCode__GCChangeTeamRollLevel;

	m_mapEnCodeFunc[0x1085]	=	&CliProtocol::EnCode__GCPlayerNewTask;
	m_mapDeCodeFunc[0x1085]	=	&CliProtocol::DeCode__GCPlayerNewTask;

	m_mapEnCodeFunc[0x1086]	=	&CliProtocol::EnCode__GCPlayerTaskStatus;
	m_mapDeCodeFunc[0x1086]	=	&CliProtocol::DeCode__GCPlayerTaskStatus;

	m_mapEnCodeFunc[0x1087]	=	&CliProtocol::EnCode__GCRecvReqJoinTeam;
	m_mapDeCodeFunc[0x1087]	=	&CliProtocol::DeCode__GCRecvReqJoinTeam;

	m_mapEnCodeFunc[0x1088]	=	&CliProtocol::EnCode__GCRollItemNum;
	m_mapDeCodeFunc[0x1088]	=	&CliProtocol::DeCode__GCRollItemNum;

	m_mapEnCodeFunc[0x1089]	=	&CliProtocol::EnCode__GCTeamMemberPickItem;
	m_mapDeCodeFunc[0x1089]	=	&CliProtocol::DeCode__GCTeamMemberPickItem;

	m_mapEnCodeFunc[0x108a]	=	&CliProtocol::EnCode__GCMonsterBuffEnd;
	m_mapDeCodeFunc[0x108a]	=	&CliProtocol::DeCode__GCMonsterBuffEnd;

	m_mapEnCodeFunc[0x108b]	=	&CliProtocol::EnCode__GCSetTradeMoney;
	m_mapDeCodeFunc[0x108b]	=	&CliProtocol::DeCode__GCSetTradeMoney;

	m_mapEnCodeFunc[0x108c]	=	&CliProtocol::EnCode__GCSetPkgItem;
	m_mapDeCodeFunc[0x108c]	=	&CliProtocol::DeCode__GCSetPkgItem;

	m_mapEnCodeFunc[0x108d]	=	&CliProtocol::EnCode__GCFoeInfoUpdate;
	m_mapDeCodeFunc[0x108d]	=	&CliProtocol::DeCode__GCFoeInfoUpdate;

	m_mapEnCodeFunc[0x108e]	=	&CliProtocol::EnCode__GCLeaveView;
	m_mapDeCodeFunc[0x108e]	=	&CliProtocol::DeCode__GCLeaveView;

	m_mapEnCodeFunc[0x108f]	=	&CliProtocol::EnCode__GCRepairWearByItem;
	m_mapDeCodeFunc[0x108f]	=	&CliProtocol::DeCode__GCRepairWearByItem;

	m_mapEnCodeFunc[0x1090]	=	&CliProtocol::EnCode__GCNotifyItemWearChange;
	m_mapDeCodeFunc[0x1090]	=	&CliProtocol::DeCode__GCNotifyItemWearChange;

	m_mapEnCodeFunc[0x1091]	=	&CliProtocol::EnCode__GCUpgradeSkillResult;
	m_mapDeCodeFunc[0x1091]	=	&CliProtocol::DeCode__GCUpgradeSkillResult;

	m_mapEnCodeFunc[0x1092]	=	&CliProtocol::EnCode__GCSpSkillUpgrade;
	m_mapDeCodeFunc[0x1092]	=	&CliProtocol::DeCode__GCSpSkillUpgrade;

	m_mapEnCodeFunc[0x1093]	=	&CliProtocol::EnCode__GCPlayerIsBusying;
	m_mapDeCodeFunc[0x1093]	=	&CliProtocol::DeCode__GCPlayerIsBusying;

	m_mapEnCodeFunc[0x1094]	=	&CliProtocol::EnCode__GCTeamInfo;
	m_mapDeCodeFunc[0x1094]	=	&CliProtocol::DeCode__GCTeamInfo;

	m_mapEnCodeFunc[0x1095]	=	&CliProtocol::EnCode__GCAddSpSkillResult;
	m_mapDeCodeFunc[0x1095]	=	&CliProtocol::DeCode__GCAddSpSkillResult;

	m_mapEnCodeFunc[0x1096]	=	&CliProtocol::EnCode__GCThreatList;
	m_mapDeCodeFunc[0x1096]	=	&CliProtocol::DeCode__GCThreatList;

	m_mapEnCodeFunc[0x1097]	=	&CliProtocol::EnCode__GCChangeItemCri;
	m_mapDeCodeFunc[0x1097]	=	&CliProtocol::DeCode__GCChangeItemCri;

	m_mapEnCodeFunc[0x1098]	=	&CliProtocol::EnCode__GCUpdateItemError;
	m_mapDeCodeFunc[0x1098]	=	&CliProtocol::DeCode__GCUpdateItemError;

	m_mapEnCodeFunc[0x1099]	=	&CliProtocol::EnCode__GCRandomString;
	m_mapDeCodeFunc[0x1099]	=	&CliProtocol::DeCode__GCRandomString;

	m_mapEnCodeFunc[0x109a]	=	&CliProtocol::EnCode__GCChangeGroupMsgNum;
	m_mapDeCodeFunc[0x109a]	=	&CliProtocol::DeCode__GCChangeGroupMsgNum;

	m_mapEnCodeFunc[0x109b]	=	&CliProtocol::EnCode__GCUpdateTaskRecordInfo;
	m_mapDeCodeFunc[0x109b]	=	&CliProtocol::DeCode__GCUpdateTaskRecordInfo;

	m_mapEnCodeFunc[0x109c]	=	&CliProtocol::EnCode__GCTaskHistory;
	m_mapDeCodeFunc[0x109c]	=	&CliProtocol::DeCode__GCTaskHistory;

	m_mapEnCodeFunc[0x109d]	=	&CliProtocol::EnCode__GCGetClientData;
	m_mapDeCodeFunc[0x109d]	=	&CliProtocol::DeCode__GCGetClientData;

	m_mapEnCodeFunc[0x109e]	=	&CliProtocol::EnCode__GCChatError;
	m_mapDeCodeFunc[0x109e]	=	&CliProtocol::DeCode__GCChatError;

	m_mapEnCodeFunc[0x109f]	=	&CliProtocol::EnCode__GCPlayerEquipItem;
	m_mapDeCodeFunc[0x109f]	=	&CliProtocol::DeCode__GCPlayerEquipItem;

	m_mapEnCodeFunc[0x10a0]	=	&CliProtocol::EnCode__GCShowTaskUIInfo;
	m_mapDeCodeFunc[0x10a0]	=	&CliProtocol::DeCode__GCShowTaskUIInfo;

	m_mapEnCodeFunc[0x10a1]	=	&CliProtocol::EnCode__GCCleanSkillPoint;
	m_mapDeCodeFunc[0x10a1]	=	&CliProtocol::DeCode__GCCleanSkillPoint;

	m_mapEnCodeFunc[0x10a2]	=	&CliProtocol::EnCode__GCCreateChatGroupResult;
	m_mapDeCodeFunc[0x10a2]	=	&CliProtocol::DeCode__GCCreateChatGroupResult;

	m_mapEnCodeFunc[0x10a3]	=	&CliProtocol::EnCode__GCDestroyChatGroupResult;
	m_mapDeCodeFunc[0x10a3]	=	&CliProtocol::DeCode__GCDestroyChatGroupResult;

	m_mapEnCodeFunc[0x10a4]	=	&CliProtocol::EnCode__GCDestroyChatGroupNotify;
	m_mapDeCodeFunc[0x10a4]	=	&CliProtocol::DeCode__GCDestroyChatGroupNotify;

	m_mapEnCodeFunc[0x10a5]	=	&CliProtocol::EnCode__GCAddChatGroupMemberResult;
	m_mapDeCodeFunc[0x10a5]	=	&CliProtocol::DeCode__GCAddChatGroupMemberResult;

	m_mapEnCodeFunc[0x10a6]	=	&CliProtocol::EnCode__GCAddChatGroupMemberNotify;
	m_mapDeCodeFunc[0x10a6]	=	&CliProtocol::DeCode__GCAddChatGroupMemberNotify;

	m_mapEnCodeFunc[0x10a7]	=	&CliProtocol::EnCode__GCRemoveChatGroupMemberResult;
	m_mapDeCodeFunc[0x10a7]	=	&CliProtocol::DeCode__GCRemoveChatGroupMemberResult;

	m_mapEnCodeFunc[0x10a8]	=	&CliProtocol::EnCode__GCRemoveChatGroupNotify;
	m_mapDeCodeFunc[0x10a8]	=	&CliProtocol::DeCode__GCRemoveChatGroupNotify;

	m_mapEnCodeFunc[0x10a9]	=	&CliProtocol::EnCode__GCChatGroupSpeakResult;
	m_mapDeCodeFunc[0x10a9]	=	&CliProtocol::DeCode__GCChatGroupSpeakResult;

	m_mapEnCodeFunc[0x10aa]	=	&CliProtocol::EnCode__GCChatGroupSpeakNotify;
	m_mapDeCodeFunc[0x10aa]	=	&CliProtocol::DeCode__GCChatGroupSpeakNotify;

	m_mapEnCodeFunc[0x10ab]	=	&CliProtocol::EnCode__GCSrvSynchTime;
	m_mapDeCodeFunc[0x10ab]	=	&CliProtocol::DeCode__GCSrvSynchTime;

	m_mapEnCodeFunc[0x10ac]	=	&CliProtocol::EnCode__GCSynchClientTime;
	m_mapDeCodeFunc[0x10ac]	=	&CliProtocol::DeCode__GCSynchClientTime;

	m_mapEnCodeFunc[0x10ad]	=	&CliProtocol::EnCode__GCShowShop;
	m_mapDeCodeFunc[0x10ad]	=	&CliProtocol::DeCode__GCShowShop;

	m_mapEnCodeFunc[0x10ae]	=	&CliProtocol::EnCode__GCShopBuyRst;
	m_mapDeCodeFunc[0x10ae]	=	&CliProtocol::DeCode__GCShopBuyRst;

	m_mapEnCodeFunc[0x10af]	=	&CliProtocol::EnCode__GCItemSoldRst;
	m_mapDeCodeFunc[0x10af]	=	&CliProtocol::DeCode__GCItemSoldRst;

	m_mapEnCodeFunc[0x10b0]	=	&CliProtocol::EnCode__GCRepairWearByNPC;
	m_mapDeCodeFunc[0x10b0]	=	&CliProtocol::DeCode__GCRepairWearByNPC;

	m_mapEnCodeFunc[0x10b1]	=	&CliProtocol::EnCode__GCIrcEnterWorldRst;
	m_mapDeCodeFunc[0x10b1]	=	&CliProtocol::DeCode__GCIrcEnterWorldRst;

	m_mapEnCodeFunc[0x10b2]	=	&CliProtocol::EnCode__GCPlayerAddHp;
	m_mapDeCodeFunc[0x10b2]	=	&CliProtocol::DeCode__GCPlayerAddHp;

	m_mapEnCodeFunc[0x10b3]	=	&CliProtocol::EnCode__GCChangeBattleMode;
	m_mapDeCodeFunc[0x10b3]	=	&CliProtocol::DeCode__GCChangeBattleMode;

	m_mapEnCodeFunc[0x10b4]	=	&CliProtocol::EnCode__GCBuffData;
	m_mapDeCodeFunc[0x10b4]	=	&CliProtocol::DeCode__GCBuffData;

	m_mapEnCodeFunc[0x10b5]	=	&CliProtocol::EnCode__GCPlayerUseHealItem;
	m_mapDeCodeFunc[0x10b5]	=	&CliProtocol::DeCode__GCPlayerUseHealItem;

	m_mapEnCodeFunc[0x10b6]	=	&CliProtocol::EnCode__GCAddActivePtRst;
	m_mapDeCodeFunc[0x10b6]	=	&CliProtocol::DeCode__GCAddActivePtRst;

	m_mapEnCodeFunc[0x10b7]	=	&CliProtocol::EnCode__GCEvilTimeRst;
	m_mapDeCodeFunc[0x10b7]	=	&CliProtocol::DeCode__GCEvilTimeRst;

	m_mapEnCodeFunc[0x10b8]	=	&CliProtocol::EnCode__GCQueryRepairItemCostRst;
	m_mapDeCodeFunc[0x10b8]	=	&CliProtocol::DeCode__GCQueryRepairItemCostRst;

	m_mapEnCodeFunc[0x10b9]	=	&CliProtocol::EnCode__GCIrcQueryPlayerInfoRst;
	m_mapDeCodeFunc[0x10b9]	=	&CliProtocol::DeCode__GCIrcQueryPlayerInfoRst;

	m_mapEnCodeFunc[0x10ba]	=	&CliProtocol::EnCode__GCTargetPlaySound;
	m_mapDeCodeFunc[0x10ba]	=	&CliProtocol::DeCode__GCTargetPlaySound;

	m_mapEnCodeFunc[0x10bb]	=	&CliProtocol::EnCode__GCTargetChangeSize;
	m_mapDeCodeFunc[0x10bb]	=	&CliProtocol::DeCode__GCTargetChangeSize;

	m_mapEnCodeFunc[0x10bc]	=	&CliProtocol::EnCode__GCInfoUpdate;
	m_mapDeCodeFunc[0x10bc]	=	&CliProtocol::DeCode__GCInfoUpdate;

	m_mapEnCodeFunc[0x10bd]	=	&CliProtocol::EnCode__GCSetEquipItem;
	m_mapDeCodeFunc[0x10bd]	=	&CliProtocol::DeCode__GCSetEquipItem;

	m_mapEnCodeFunc[0x10be]	=	&CliProtocol::EnCode__GCTargetGetDamage;
	m_mapDeCodeFunc[0x10be]	=	&CliProtocol::DeCode__GCTargetGetDamage;

	m_mapEnCodeFunc[0x10bf]	=	&CliProtocol::EnCode__GCSwapEquipResult;
	m_mapDeCodeFunc[0x10bf]	=	&CliProtocol::DeCode__GCSwapEquipResult;

	m_mapEnCodeFunc[0x10c0]	=	&CliProtocol::EnCode__GCChangeTeamState;
	m_mapDeCodeFunc[0x10c0]	=	&CliProtocol::DeCode__GCChangeTeamState;

	m_mapEnCodeFunc[0x10c1]	=	&CliProtocol::EnCode__GCCreateUnionResult;
	m_mapDeCodeFunc[0x10c1]	=	&CliProtocol::DeCode__GCCreateUnionResult;

	m_mapEnCodeFunc[0x10c2]	=	&CliProtocol::EnCode__GCDestroyUnionResult;
	m_mapDeCodeFunc[0x10c2]	=	&CliProtocol::DeCode__GCDestroyUnionResult;

	m_mapEnCodeFunc[0x10c3]	=	&CliProtocol::EnCode__GCUnionDestroyedNotify;
	m_mapDeCodeFunc[0x10c3]	=	&CliProtocol::DeCode__GCUnionDestroyedNotify;

	m_mapEnCodeFunc[0x10c4]	=	&CliProtocol::EnCode__GCQueryUnionBasicResult;
	m_mapDeCodeFunc[0x10c4]	=	&CliProtocol::DeCode__GCQueryUnionBasicResult;

	m_mapEnCodeFunc[0x10c5]	=	&CliProtocol::EnCode__GCQueryUnionMembersResult;
	m_mapDeCodeFunc[0x10c5]	=	&CliProtocol::DeCode__GCQueryUnionMembersResult;

	m_mapEnCodeFunc[0x10c6]	=	&CliProtocol::EnCode__GCAddUnionMemberConfirm;
	m_mapDeCodeFunc[0x10c6]	=	&CliProtocol::DeCode__GCAddUnionMemberConfirm;

	m_mapEnCodeFunc[0x10c7]	=	&CliProtocol::EnCode__GCAddUnionMemberResult;
	m_mapDeCodeFunc[0x10c7]	=	&CliProtocol::DeCode__GCAddUnionMemberResult;

	m_mapEnCodeFunc[0x10c8]	=	&CliProtocol::EnCode__GCAddUnionMemberNotify;
	m_mapDeCodeFunc[0x10c8]	=	&CliProtocol::DeCode__GCAddUnionMemberNotify;

	m_mapEnCodeFunc[0x10c9]	=	&CliProtocol::EnCode__GCRemoveUnionMemberResult;
	m_mapDeCodeFunc[0x10c9]	=	&CliProtocol::DeCode__GCRemoveUnionMemberResult;

	m_mapEnCodeFunc[0x10ca]	=	&CliProtocol::EnCode__GCRemoveUnionMemberNotify;
	m_mapDeCodeFunc[0x10ca]	=	&CliProtocol::DeCode__GCRemoveUnionMemberNotify;

	m_mapEnCodeFunc[0x10cb]	=	&CliProtocol::EnCode__GCPunishPlayerInfo;
	m_mapDeCodeFunc[0x10cb]	=	&CliProtocol::DeCode__GCPunishPlayerInfo;

	m_mapEnCodeFunc[0x10cc]	=	&CliProtocol::EnCode__GCPetinfoInstable;
	m_mapDeCodeFunc[0x10cc]	=	&CliProtocol::DeCode__GCPetinfoInstable;

	m_mapEnCodeFunc[0x10cd]	=	&CliProtocol::EnCode__GCPetAbility;
	m_mapDeCodeFunc[0x10cd]	=	&CliProtocol::DeCode__GCPetAbility;

	m_mapEnCodeFunc[0x10ce]	=	&CliProtocol::EnCode__GCPetinfoStable;
	m_mapDeCodeFunc[0x10ce]	=	&CliProtocol::DeCode__GCPetinfoStable;

	m_mapEnCodeFunc[0x10d7]	=	&CliProtocol::EnCode__GCUnionPosRightsNtf;
	m_mapDeCodeFunc[0x10d7]	=	&CliProtocol::DeCode__GCUnionPosRightsNtf;

	m_mapEnCodeFunc[0x10d8]	=	&CliProtocol::EnCode__GCModifyUnionPosRighstRst;
	m_mapDeCodeFunc[0x10d8]	=	&CliProtocol::DeCode__GCModifyUnionPosRighstRst;

	m_mapEnCodeFunc[0x10d9]	=	&CliProtocol::EnCode__GCAddUnionPosRst;
	m_mapDeCodeFunc[0x10d9]	=	&CliProtocol::DeCode__GCAddUnionPosRst;

	m_mapEnCodeFunc[0x10da]	=	&CliProtocol::EnCode__GCRemoveUnionPosRst;
	m_mapDeCodeFunc[0x10da]	=	&CliProtocol::DeCode__GCRemoveUnionPosRst;

	m_mapEnCodeFunc[0x10db]	=	&CliProtocol::EnCode__GCTransformUnionCaptionRst;
	m_mapDeCodeFunc[0x10db]	=	&CliProtocol::DeCode__GCTransformUnionCaptionRst;

	m_mapEnCodeFunc[0x10dc]	=	&CliProtocol::EnCode__GCTransformUnionCaptionNtf;
	m_mapDeCodeFunc[0x10dc]	=	&CliProtocol::DeCode__GCTransformUnionCaptionNtf;

	m_mapEnCodeFunc[0x10dd]	=	&CliProtocol::EnCode__GCModifyUnionMemberTitleRst;
	m_mapDeCodeFunc[0x10dd]	=	&CliProtocol::DeCode__GCModifyUnionMemberTitleRst;

	m_mapEnCodeFunc[0x10de]	=	&CliProtocol::EnCode__GCUnionMemberTitleNtf;
	m_mapDeCodeFunc[0x10de]	=	&CliProtocol::DeCode__GCUnionMemberTitleNtf;

	m_mapEnCodeFunc[0x10df]	=	&CliProtocol::EnCode__GCAdvanceUnionMemberPosRst;
	m_mapDeCodeFunc[0x10df]	=	&CliProtocol::DeCode__GCAdvanceUnionMemberPosRst;

	m_mapEnCodeFunc[0x10e0]	=	&CliProtocol::EnCode__GCAdvanceUnionMemberPosNtf;
	m_mapDeCodeFunc[0x10e0]	=	&CliProtocol::DeCode__GCAdvanceUnionMemberPosNtf;

	m_mapEnCodeFunc[0x10e1]	=	&CliProtocol::EnCode__GCReduceUnionMemberPosRst;
	m_mapDeCodeFunc[0x10e1]	=	&CliProtocol::DeCode__GCReduceUnionMemberPosRst;

	m_mapEnCodeFunc[0x10e2]	=	&CliProtocol::EnCode__GCReduceUnionMemberPosNtf;
	m_mapDeCodeFunc[0x10e2]	=	&CliProtocol::DeCode__GCReduceUnionMemberPosNtf;

	m_mapEnCodeFunc[0x10e3]	=	&CliProtocol::EnCode__GCUnionChannelSpeekRst;
	m_mapDeCodeFunc[0x10e3]	=	&CliProtocol::DeCode__GCUnionChannelSpeekRst;

	m_mapEnCodeFunc[0x10e4]	=	&CliProtocol::EnCode__GCUnionChannelSpeekNtf;
	m_mapDeCodeFunc[0x10e4]	=	&CliProtocol::DeCode__GCUnionChannelSpeekNtf;

	m_mapEnCodeFunc[0x10e5]	=	&CliProtocol::EnCode__GCUpdateUnionPosCfgRst;
	m_mapDeCodeFunc[0x10e5]	=	&CliProtocol::DeCode__GCUpdateUnionPosCfgRst;

	m_mapEnCodeFunc[0x10e6]	=	&CliProtocol::EnCode__GCUpdateUnionPosCfgNtf;
	m_mapDeCodeFunc[0x10e6]	=	&CliProtocol::DeCode__GCUpdateUnionPosCfgNtf;

	m_mapEnCodeFunc[0x10e7]	=	&CliProtocol::EnCode__GCForbidUnionSpeekRst;
	m_mapDeCodeFunc[0x10e7]	=	&CliProtocol::DeCode__GCForbidUnionSpeekRst;

	m_mapEnCodeFunc[0x10e8]	=	&CliProtocol::EnCode__GCForbidUnionSpeekNtf;
	m_mapDeCodeFunc[0x10e8]	=	&CliProtocol::DeCode__GCForbidUnionSpeekNtf;

	m_mapEnCodeFunc[0x10e9]	=	&CliProtocol::EnCode__GCRankInfoByPageRst;
	m_mapDeCodeFunc[0x10e9]	=	&CliProtocol::DeCode__GCRankInfoByPageRst;

	m_mapEnCodeFunc[0x10ea]	=	&CliProtocol::EnCode__GCQueryRankPlayerByNameRst;
	m_mapDeCodeFunc[0x10ea]	=	&CliProtocol::DeCode__GCQueryRankPlayerByNameRst;

	m_mapEnCodeFunc[0x10eb]	=	&CliProtocol::EnCode__GCUnionSkills;
	m_mapDeCodeFunc[0x10eb]	=	&CliProtocol::DeCode__GCUnionSkills;

	m_mapEnCodeFunc[0x10ec]	=	&CliProtocol::EnCode__GCIssueUnionTaskRst;
	m_mapDeCodeFunc[0x10ec]	=	&CliProtocol::DeCode__GCIssueUnionTaskRst;

	m_mapEnCodeFunc[0x10ed]	=	&CliProtocol::EnCode__GCIssueUnionTaskNtf;
	m_mapDeCodeFunc[0x10ed]	=	&CliProtocol::DeCode__GCIssueUnionTaskNtf;

	m_mapEnCodeFunc[0x10ee]	=	&CliProtocol::EnCode__GCDisplayUnionTasks;
	m_mapDeCodeFunc[0x10ee]	=	&CliProtocol::DeCode__GCDisplayUnionTasks;

	m_mapEnCodeFunc[0x10ef]	=	&CliProtocol::EnCode__GCLearnUnionSkillRst;
	m_mapDeCodeFunc[0x10ef]	=	&CliProtocol::DeCode__GCLearnUnionSkillRst;

	m_mapEnCodeFunc[0x10f0]	=	&CliProtocol::EnCode__GCLearnUnionSkillNtf;
	m_mapDeCodeFunc[0x10f0]	=	&CliProtocol::DeCode__GCLearnUnionSkillNtf;

	m_mapEnCodeFunc[0x10f1]	=	&CliProtocol::EnCode__GCChanageUnionActive;
	m_mapDeCodeFunc[0x10f1]	=	&CliProtocol::DeCode__GCChanageUnionActive;

	m_mapEnCodeFunc[0x10f2]	=	&CliProtocol::EnCode__GCWaitQueueResult;
	m_mapDeCodeFunc[0x10f2]	=	&CliProtocol::DeCode__GCWaitQueueResult;

	m_mapEnCodeFunc[0x10f3]	=	&CliProtocol::EnCode__GCUsePartionSkillRst;
	m_mapDeCodeFunc[0x10f3]	=	&CliProtocol::DeCode__GCUsePartionSkillRst;

	m_mapEnCodeFunc[0x10f4]	=	&CliProtocol::EnCode__GCAdvanceUnionLevelRst;
	m_mapDeCodeFunc[0x10f4]	=	&CliProtocol::DeCode__GCAdvanceUnionLevelRst;

	m_mapEnCodeFunc[0x10f5]	=	&CliProtocol::EnCode__GCUnionLevelNtf;
	m_mapDeCodeFunc[0x10f5]	=	&CliProtocol::DeCode__GCUnionLevelNtf;

	m_mapEnCodeFunc[0x10f6]	=	&CliProtocol::EnCode__GCPostUnionBulletinRst;
	m_mapDeCodeFunc[0x10f6]	=	&CliProtocol::DeCode__GCPostUnionBulletinRst;

	m_mapEnCodeFunc[0x10f7]	=	&CliProtocol::EnCode__GCUnionBulletinNtf;
	m_mapDeCodeFunc[0x10f7]	=	&CliProtocol::DeCode__GCUnionBulletinNtf;

	m_mapEnCodeFunc[0x10f8]	=	&CliProtocol::EnCode__GCAccountInfo;
	m_mapDeCodeFunc[0x10f8]	=	&CliProtocol::DeCode__GCAccountInfo;

	m_mapEnCodeFunc[0x10f9]	=	&CliProtocol::EnCode__GCPurchaseItem;
	m_mapDeCodeFunc[0x10f9]	=	&CliProtocol::DeCode__GCPurchaseItem;

	m_mapEnCodeFunc[0x10fa]	=	&CliProtocol::EnCode__GCVipInfo;
	m_mapDeCodeFunc[0x10fa]	=	&CliProtocol::DeCode__GCVipInfo;

	m_mapEnCodeFunc[0x10fb]	=	&CliProtocol::EnCode__GCPurchaseVip;
	m_mapDeCodeFunc[0x10fb]	=	&CliProtocol::DeCode__GCPurchaseVip;

	m_mapEnCodeFunc[0x10fc]	=	&CliProtocol::EnCode__GCFetchVipItem;
	m_mapDeCodeFunc[0x10fc]	=	&CliProtocol::DeCode__GCFetchVipItem;

	m_mapEnCodeFunc[0x10fd]	=	&CliProtocol::EnCode__GCPurchaseGift;
	m_mapDeCodeFunc[0x10fd]	=	&CliProtocol::DeCode__GCPurchaseGift;

	m_mapEnCodeFunc[0x10fe]	=	&CliProtocol::EnCode__GCPetMove;
	m_mapDeCodeFunc[0x10fe]	=	&CliProtocol::DeCode__GCPetMove;

	m_mapEnCodeFunc[0x10ff]	=	&CliProtocol::EnCode__GCUnionMemberOnOffLineNtf;
	m_mapDeCodeFunc[0x10ff]	=	&CliProtocol::DeCode__GCUnionMemberOnOffLineNtf;

	m_mapEnCodeFunc[0x104a]	=	&CliProtocol::EnCode__GCQueryUnionLogRst;
	m_mapDeCodeFunc[0x104a]	=	&CliProtocol::DeCode__GCQueryUnionLogRst;

	m_mapEnCodeFunc[0x104b]	=	&CliProtocol::EnCode__GCFetchNonVipItem;
	m_mapDeCodeFunc[0x104b]	=	&CliProtocol::DeCode__GCFetchNonVipItem;

	m_mapEnCodeFunc[0x104c]	=	&CliProtocol::EnCode__GCUnionUISelectCmd;
	m_mapDeCodeFunc[0x104c]	=	&CliProtocol::DeCode__GCUnionUISelectCmd;

	m_mapEnCodeFunc[0x1100]	=	&CliProtocol::EnCode__GCQueryMailListByPageRst;
	m_mapDeCodeFunc[0x1100]	=	&CliProtocol::DeCode__GCQueryMailListByPageRst;

	m_mapEnCodeFunc[0x1101]	=	&CliProtocol::EnCode__GCQueryMailDetialInfoRst;
	m_mapDeCodeFunc[0x1101]	=	&CliProtocol::DeCode__GCQueryMailDetialInfoRst;

	m_mapEnCodeFunc[0x1102]	=	&CliProtocol::EnCode__GCQueryMailAttachInfoRst;
	m_mapDeCodeFunc[0x1102]	=	&CliProtocol::DeCode__GCQueryMailAttachInfoRst;

	m_mapEnCodeFunc[0x1103]	=	&CliProtocol::EnCode__GCSendMailError;
	m_mapDeCodeFunc[0x1103]	=	&CliProtocol::DeCode__GCSendMailError;

	m_mapEnCodeFunc[0x1104]	=	&CliProtocol::EnCode__GCDeleteMailRst;
	m_mapDeCodeFunc[0x1104]	=	&CliProtocol::DeCode__GCDeleteMailRst;

	m_mapEnCodeFunc[0x1105]	=	&CliProtocol::EnCode__GCPickMailAttachItemRst;
	m_mapDeCodeFunc[0x1105]	=	&CliProtocol::DeCode__GCPickMailAttachItemRst;

	m_mapEnCodeFunc[0x1106]	=	&CliProtocol::EnCode__GCPickMailAttachMoneyRst;
	m_mapDeCodeFunc[0x1106]	=	&CliProtocol::DeCode__GCPickMailAttachMoneyRst;

	m_mapEnCodeFunc[0x1107]	=	&CliProtocol::EnCode__GCPlayerHaveUnReadMail;
	m_mapDeCodeFunc[0x1107]	=	&CliProtocol::DeCode__GCPlayerHaveUnReadMail;

	m_mapEnCodeFunc[0x1108]	=	&CliProtocol::EnCode__GCReqPlayerDetailInfoRst;
	m_mapDeCodeFunc[0x1108]	=	&CliProtocol::DeCode__GCReqPlayerDetailInfoRst;

	m_mapEnCodeFunc[0x1109]	=	&CliProtocol::EnCode__GCQuerySuitInfoRst;
	m_mapDeCodeFunc[0x1109]	=	&CliProtocol::DeCode__GCQuerySuitInfoRst;

	m_mapEnCodeFunc[0x110a]	=	&CliProtocol::EnCode__GCClearSuitInfoRst;
	m_mapDeCodeFunc[0x110a]	=	&CliProtocol::DeCode__GCClearSuitInfoRst;

	m_mapEnCodeFunc[0x110b]	=	&CliProtocol::EnCode__GCSaveSuitInfoRst;
	m_mapDeCodeFunc[0x110b]	=	&CliProtocol::DeCode__GCSaveSuitInfoRst;

	m_mapEnCodeFunc[0x110c]	=	&CliProtocol::EnCode__GCChanageSuitRst;
	m_mapDeCodeFunc[0x110c]	=	&CliProtocol::DeCode__GCChanageSuitRst;

	m_mapEnCodeFunc[0x110d]	=	&CliProtocol::EnCode__GCCollectTaskRst;
	m_mapDeCodeFunc[0x110d]	=	&CliProtocol::DeCode__GCCollectTaskRst;

	m_mapEnCodeFunc[0x110e]	=	&CliProtocol::EnCode__GCShowProtectItemPlate;
	m_mapDeCodeFunc[0x110e]	=	&CliProtocol::DeCode__GCShowProtectItemPlate;

	m_mapEnCodeFunc[0x110f]	=	&CliProtocol::EnCode__GCPutItemToProtectPlateRst;
	m_mapDeCodeFunc[0x110f]	=	&CliProtocol::DeCode__GCPutItemToProtectPlateRst;

	m_mapEnCodeFunc[0x1110]	=	&CliProtocol::EnCode__GCSetItemProtectProp;
	m_mapDeCodeFunc[0x1110]	=	&CliProtocol::DeCode__GCSetItemProtectProp;

	m_mapEnCodeFunc[0x1111]	=	&CliProtocol::EnCode__GCRecvItemWithProtectProp;
	m_mapDeCodeFunc[0x1111]	=	&CliProtocol::DeCode__GCRecvItemWithProtectProp;

	m_mapEnCodeFunc[0x1112]	=	&CliProtocol::EnCode__GCReleaseSpecialProtectRst;
	m_mapDeCodeFunc[0x1112]	=	&CliProtocol::DeCode__GCReleaseSpecialProtectRst;

	m_mapEnCodeFunc[0x1113]	=	&CliProtocol::EnCode__GCCancelSpecialProtectRst;
	m_mapDeCodeFunc[0x1113]	=	&CliProtocol::DeCode__GCCancelSpecialProtectRst;

	m_mapEnCodeFunc[0x1114]	=	&CliProtocol::EnCode__GCTaskItemFromProtectPlateRst;
	m_mapDeCodeFunc[0x1114]	=	&CliProtocol::DeCode__GCTaskItemFromProtectPlateRst;

	m_mapEnCodeFunc[0x1115]	=	&CliProtocol::EnCode__GCConfirmProtectPlateRst;
	m_mapDeCodeFunc[0x1115]	=	&CliProtocol::DeCode__GCConfirmProtectPlateRst;

	m_mapEnCodeFunc[0x1116]	=	&CliProtocol::EnCode__GCGameStateNotify;
	m_mapDeCodeFunc[0x1116]	=	&CliProtocol::DeCode__GCGameStateNotify;

	m_mapEnCodeFunc[0x1117]	=	&CliProtocol::EnCode__GCChangeTargetName;
	m_mapDeCodeFunc[0x1117]	=	&CliProtocol::DeCode__GCChangeTargetName;

	m_mapEnCodeFunc[0x0201]	=	&CliProtocol::EnCode__CAGetSrvList;
	m_mapDeCodeFunc[0x0201]	=	&CliProtocol::DeCode__CAGetSrvList;

	m_mapEnCodeFunc[0x2001]	=	&CliProtocol::EnCode__ACSrvOption;
	m_mapDeCodeFunc[0x2001]	=	&CliProtocol::DeCode__ACSrvOption;

}

MUX_PROTO::CliProtocol::~CliProtocol()
{
}

size_t	MUX_PROTO::CliProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	MUX_PROTO::CliProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

MUX_PROTO::EnCodeFunc	MUX_PROTO::CliProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

MUX_PROTO::DeCodeFunc	MUX_PROTO::CliProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		//printf( "MUX_PROTO::CliProtocol::FindDeCodeFunc，找到%ddecode函数\n", nMessageId );
		return	itr->second;
	}
	else
	{
		//printf( "MUX_PROTO::CliProtocol::FindDeCodeFunc，寻找%ddecode函数失败\n", nMessageId );
		return NULL;
	}
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerID(void* pData)
{
	PlayerID* pkPlayerID = (PlayerID*)(pData);

	//EnCode wGID
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerID->wGID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dwPID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerID->dwPID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerID(void* pData)
{
	PlayerID* pkPlayerID = (PlayerID*)(pData);

	//DeCode wGID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerID->wGID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dwPID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerID->dwPID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PlayerID);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerIDEx(void* pData)
{
	PlayerIDEx* pkPlayerIDEx = (PlayerIDEx*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkPlayerIDEx->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode playerUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerIDEx->playerUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerIDEx(void* pData)
{
	PlayerIDEx* pkPlayerIDEx = (PlayerIDEx*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkPlayerIDEx->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode playerUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerIDEx->playerUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PlayerIDEx);
}

size_t	MUX_PROTO::CliProtocol::EnCode__TeamMemberCliInfo(void* pData)
{
	TeamMemberCliInfo* pkTeamMemberCliInfo = (TeamMemberCliInfo*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkTeamMemberCliInfo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTeamMemberCliInfo->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerName
	if(MAX_NAME_SIZE < pkTeamMemberCliInfo->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamMemberCliInfo->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTeamMemberCliInfo->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode portraitID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTeamMemberCliInfo->portraitID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkTeamMemberCliInfo->ucClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bSex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkTeamMemberCliInfo->bSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucRace
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkTeamMemberCliInfo->ucRace), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__TeamMemberCliInfo(void* pData)
{
	TeamMemberCliInfo* pkTeamMemberCliInfo = (TeamMemberCliInfo*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkTeamMemberCliInfo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTeamMemberCliInfo->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerName
	if(MAX_NAME_SIZE < pkTeamMemberCliInfo->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTeamMemberCliInfo->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTeamMemberCliInfo->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode portraitID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTeamMemberCliInfo->portraitID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkTeamMemberCliInfo->ucClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bSex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkTeamMemberCliInfo->bSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucRace
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkTeamMemberCliInfo->ucRace), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(TeamMemberCliInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerInfo_1_Base(void* pData)
{
	PlayerInfo_1_Base* pkPlayerInfo_1_Base = (PlayerInfo_1_Base*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode accountSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->accountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szAccount
	if(MAX_NAME_SIZE < pkPlayerInfo_1_Base->accountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerInfo_1_Base->accountSize;
	if(!m_kPackage.Pack("CHAR", &(pkPlayerInfo_1_Base->szAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szNickName
	if(MAX_NAME_SIZE < pkPlayerInfo_1_Base->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerInfo_1_Base->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkPlayerInfo_1_Base->szNickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->tag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiLastSaveTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->uiLastSaveTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiLogTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->uiLogTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiEvilTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->uiEvilTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->ucRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usPortrait
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->usPortrait), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usHair
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->usHair), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usHairColor
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->usHairColor), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usFace
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->usFace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->usLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dwExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->dwExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usMapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->usMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode iPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->iPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode iPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->iPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiHP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->uiHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->uiMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPlayerInfo_1_Base->usClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usPhysicsLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPlayerInfo_1_Base->usPhysicsLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usMagicLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPlayerInfo_1_Base->usMagicLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dwPhysicsExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->dwPhysicsExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dwMagicExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->dwMagicExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usPhysicsAllotPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->usPhysicsAllotPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usMagicAllotPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->usMagicAllotPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usPhyTotalGetPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->usPhyTotalGetPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usMagTotalGetPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->usMagTotalGetPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucMaxPhyLevelLimit
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPlayerInfo_1_Base->ucMaxPhyLevelLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucMaxMagLevelLimit
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPlayerInfo_1_Base->ucMaxMagLevelLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucMaxSpLimit
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPlayerInfo_1_Base->ucMaxSpLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usSpPower
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfo_1_Base->usSpPower), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usSpExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->usSpExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dwMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->dwMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sGoodEvilPoint
	unCount = 1;
	if(!m_kPackage.Pack("SHORT", &(pkPlayerInfo_1_Base->sGoodEvilPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode racePrestige
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_1_Base->racePrestige), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bSex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPlayerInfo_1_Base->bSex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerInfo_1_Base(void* pData)
{
	PlayerInfo_1_Base* pkPlayerInfo_1_Base = (PlayerInfo_1_Base*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode accountSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->accountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szAccount
	if(MAX_NAME_SIZE < pkPlayerInfo_1_Base->accountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerInfo_1_Base->accountSize;
	if(!m_kPackage.UnPack("CHAR", &(pkPlayerInfo_1_Base->szAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szNickName
	if(MAX_NAME_SIZE < pkPlayerInfo_1_Base->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerInfo_1_Base->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkPlayerInfo_1_Base->szNickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->tag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiLastSaveTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->uiLastSaveTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiLogTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->uiLogTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiEvilTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->uiEvilTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->ucRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usPortrait
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->usPortrait), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usHair
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->usHair), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usHairColor
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->usHairColor), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usFace
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->usFace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->usLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dwExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->dwExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usMapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->usMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode iPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->iPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode iPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->iPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiHP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->uiHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->uiMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPlayerInfo_1_Base->usClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usPhysicsLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPlayerInfo_1_Base->usPhysicsLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usMagicLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPlayerInfo_1_Base->usMagicLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dwPhysicsExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->dwPhysicsExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dwMagicExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->dwMagicExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usPhysicsAllotPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->usPhysicsAllotPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usMagicAllotPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->usMagicAllotPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usPhyTotalGetPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->usPhyTotalGetPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usMagTotalGetPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->usMagTotalGetPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucMaxPhyLevelLimit
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPlayerInfo_1_Base->ucMaxPhyLevelLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucMaxMagLevelLimit
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPlayerInfo_1_Base->ucMaxMagLevelLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucMaxSpLimit
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPlayerInfo_1_Base->ucMaxSpLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usSpPower
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfo_1_Base->usSpPower), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usSpExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->usSpExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dwMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->dwMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sGoodEvilPoint
	unCount = 1;
	if(!m_kPackage.UnPack("SHORT", &(pkPlayerInfo_1_Base->sGoodEvilPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode racePrestige
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_1_Base->racePrestige), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bSex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPlayerInfo_1_Base->bSex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PlayerInfo_1_Base);
}

size_t	MUX_PROTO::CliProtocol::EnCode__ItemInfo_i(void* pData)
{
	ItemInfo_i* pkItemInfo_i = (ItemInfo_i*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkItemInfo_i->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkItemInfo_i->usItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usWearPoint
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkItemInfo_i->usWearPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usAddId
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkItemInfo_i->usAddId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucCount
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkItemInfo_i->ucCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucLevelUp
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkItemInfo_i->ucLevelUp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__ItemInfo_i(void* pData)
{
	ItemInfo_i* pkItemInfo_i = (ItemInfo_i*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkItemInfo_i->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkItemInfo_i->usItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usWearPoint
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkItemInfo_i->usWearPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usAddId
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkItemInfo_i->usAddId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucCount
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkItemInfo_i->ucCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucLevelUp
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkItemInfo_i->ucLevelUp), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(ItemInfo_i);
}

size_t	MUX_PROTO::CliProtocol::EnCode__SkillInfo_i(void* pData)
{
	SkillInfo_i* pkSkillInfo_i = (SkillInfo_i*)(pData);

	//EnCode skillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSkillInfo_i->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillExp
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSkillInfo_i->skillExp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__SkillInfo_i(void* pData)
{
	SkillInfo_i* pkSkillInfo_i = (SkillInfo_i*)(pData);

	//DeCode skillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSkillInfo_i->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillExp
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSkillInfo_i->skillExp), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SkillInfo_i);
}

size_t	MUX_PROTO::CliProtocol::EnCode__BuffInfo(void* pData)
{
	BuffInfo* pkBuffInfo = (BuffInfo*)(pData);

	//EnCode uiBuffID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBuffInfo->uiBuffID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiLeftDurSeconds
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBuffInfo->uiLeftDurSeconds), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__BuffInfo(void* pData)
{
	BuffInfo* pkBuffInfo = (BuffInfo*)(pData);

	//DeCode uiBuffID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBuffInfo->uiBuffID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiLeftDurSeconds
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBuffInfo->uiLeftDurSeconds), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(BuffInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PunishPlayerCliInfo(void* pData)
{
	PunishPlayerCliInfo* pkPunishPlayerCliInfo = (PunishPlayerCliInfo*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPunishPlayerCliInfo->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode punishPlayerName
	if(MAX_NAME_SIZE < pkPunishPlayerCliInfo->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPunishPlayerCliInfo->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkPunishPlayerCliInfo->punishPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPunishPlayerCliInfo->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPunishPlayerCliInfo->posX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPunishPlayerCliInfo->posY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PunishPlayerCliInfo(void* pData)
{
	PunishPlayerCliInfo* pkPunishPlayerCliInfo = (PunishPlayerCliInfo*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPunishPlayerCliInfo->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode punishPlayerName
	if(MAX_NAME_SIZE < pkPunishPlayerCliInfo->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPunishPlayerCliInfo->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkPunishPlayerCliInfo->punishPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPunishPlayerCliInfo->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPunishPlayerCliInfo->posX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPunishPlayerCliInfo->posY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PunishPlayerCliInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__BuffID(void* pData)
{
	BuffID* pkBuffID = (BuffID*)(pData);

	//EnCode uiBuffTypeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBuffID->uiBuffTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiBuffID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBuffID->uiBuffID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMapSrvID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBuffID->uiMapSrvID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__BuffID(void* pData)
{
	BuffID* pkBuffID = (BuffID*)(pData);

	//DeCode uiBuffTypeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBuffID->uiBuffTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiBuffID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBuffID->uiBuffID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMapSrvID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBuffID->uiMapSrvID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(BuffID);
}

size_t	MUX_PROTO::CliProtocol::EnCode__BuffDetailInfo(void* pData)
{
	BuffDetailInfo* pkBuffDetailInfo = (BuffDetailInfo*)(pData);

	//EnCode buffID
	if(EnCode__BuffID(&(pkBuffDetailInfo->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nDurSeconds
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBuffDetailInfo->nDurSeconds), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__BuffDetailInfo(void* pData)
{
	BuffDetailInfo* pkBuffDetailInfo = (BuffDetailInfo*)(pData);

	//DeCode buffID
	if(DeCode__BuffID(&(pkBuffDetailInfo->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nDurSeconds
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBuffDetailInfo->nDurSeconds), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(BuffDetailInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerInfo_2_Equips(void* pData)
{
	PlayerInfo_2_Equips* pkPlayerInfo_2_Equips = (PlayerInfo_2_Equips*)(pData);

	//EnCode equipSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_2_Equips->equipSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equips
	if(EQUIP_SIZE < pkPlayerInfo_2_Equips->equipSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_2_Equips->equipSize; ++i)
	{
		if(EnCode__ItemInfo_i(&(pkPlayerInfo_2_Equips->equips[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerInfo_2_Equips(void* pData)
{
	PlayerInfo_2_Equips* pkPlayerInfo_2_Equips = (PlayerInfo_2_Equips*)(pData);

	//DeCode equipSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_2_Equips->equipSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equips
	if(EQUIP_SIZE < pkPlayerInfo_2_Equips->equipSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_2_Equips->equipSize; ++i)
	{
		if(DeCode__ItemInfo_i(&(pkPlayerInfo_2_Equips->equips[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(PlayerInfo_2_Equips);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerInfo_4_Skills(void* pData)
{
	PlayerInfo_4_Skills* pkPlayerInfo_4_Skills = (PlayerInfo_4_Skills*)(pData);

	//EnCode skillSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_4_Skills->skillSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillData
	if(SKILL_SIZE < pkPlayerInfo_4_Skills->skillSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_4_Skills->skillSize; ++i)
	{
		if(EnCode__SkillInfo_i(&(pkPlayerInfo_4_Skills->skillData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerInfo_4_Skills(void* pData)
{
	PlayerInfo_4_Skills* pkPlayerInfo_4_Skills = (PlayerInfo_4_Skills*)(pData);

	//DeCode skillSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_4_Skills->skillSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillData
	if(SKILL_SIZE < pkPlayerInfo_4_Skills->skillSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_4_Skills->skillSize; ++i)
	{
		if(DeCode__SkillInfo_i(&(pkPlayerInfo_4_Skills->skillData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(PlayerInfo_4_Skills);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerInfo_5_Buffers(void* pData)
{
	PlayerInfo_5_Buffers* pkPlayerInfo_5_Buffers = (PlayerInfo_5_Buffers*)(pData);

	//EnCode buffSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_5_Buffers->buffSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buffData
	if(BUFF_SIZE < pkPlayerInfo_5_Buffers->buffSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_5_Buffers->buffSize; ++i)
	{
		if(EnCode__BuffInfo(&(pkPlayerInfo_5_Buffers->buffData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerInfo_5_Buffers(void* pData)
{
	PlayerInfo_5_Buffers* pkPlayerInfo_5_Buffers = (PlayerInfo_5_Buffers*)(pData);

	//DeCode buffSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_5_Buffers->buffSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buffData
	if(BUFF_SIZE < pkPlayerInfo_5_Buffers->buffSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_5_Buffers->buffSize; ++i)
	{
		if(DeCode__BuffInfo(&(pkPlayerInfo_5_Buffers->buffData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(PlayerInfo_5_Buffers);
}

size_t	MUX_PROTO::CliProtocol::EnCode__SuitInfo_i(void* pData)
{
	SuitInfo_i* pkSuitInfo_i = (SuitInfo_i*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSuitInfo_i->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_SUIT_NAME_SIZE < pkSuitInfo_i->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSuitInfo_i->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkSuitInfo_i->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillIcon
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSuitInfo_i->skillIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equipCount
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSuitInfo_i->equipCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equips
	if(EQUIP_SIZE < pkSuitInfo_i->equipCount)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSuitInfo_i->equipCount;
	if(!m_kPackage.Pack("UINT", &(pkSuitInfo_i->equips), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__SuitInfo_i(void* pData)
{
	SuitInfo_i* pkSuitInfo_i = (SuitInfo_i*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSuitInfo_i->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_SUIT_NAME_SIZE < pkSuitInfo_i->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSuitInfo_i->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSuitInfo_i->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillIcon
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSuitInfo_i->skillIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equipCount
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSuitInfo_i->equipCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equips
	if(EQUIP_SIZE < pkSuitInfo_i->equipCount)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSuitInfo_i->equipCount;
	if(!m_kPackage.UnPack("UINT", &(pkSuitInfo_i->equips), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SuitInfo_i);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerInfo_Suits(void* pData)
{
	PlayerInfo_Suits* pkPlayerInfo_Suits = (PlayerInfo_Suits*)(pData);

	//EnCode suitSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_Suits->suitSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode suitData
	if(MAX_SUIT_COUNT < pkPlayerInfo_Suits->suitSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_Suits->suitSize; ++i)
	{
		if(EnCode__SuitInfo_i(&(pkPlayerInfo_Suits->suitData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerInfo_Suits(void* pData)
{
	PlayerInfo_Suits* pkPlayerInfo_Suits = (PlayerInfo_Suits*)(pData);

	//DeCode suitSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_Suits->suitSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode suitData
	if(MAX_SUIT_COUNT < pkPlayerInfo_Suits->suitSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_Suits->suitSize; ++i)
	{
		if(DeCode__SuitInfo_i(&(pkPlayerInfo_Suits->suitData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(PlayerInfo_Suits);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerInfo_Fashions(void* pData)
{
	PlayerInfo_Fashions* pkPlayerInfo_Fashions = (PlayerInfo_Fashions*)(pData);

	//EnCode fashionSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfo_Fashions->fashionSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fashionData
	if(MAX_FASHION_COUNT < pkPlayerInfo_Fashions->fashionSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_Fashions->fashionSize; ++i)
	{
		if(EnCode__ItemInfo_i(&(pkPlayerInfo_Fashions->fashionData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerInfo_Fashions(void* pData)
{
	PlayerInfo_Fashions* pkPlayerInfo_Fashions = (PlayerInfo_Fashions*)(pData);

	//DeCode fashionSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfo_Fashions->fashionSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fashionData
	if(MAX_FASHION_COUNT < pkPlayerInfo_Fashions->fashionSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkPlayerInfo_Fashions->fashionSize; ++i)
	{
		if(DeCode__ItemInfo_i(&(pkPlayerInfo_Fashions->fashionData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(PlayerInfo_Fashions);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerInfoLogin(void* pData)
{
	PlayerInfoLogin* pkPlayerInfoLogin = (PlayerInfoLogin*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfoLogin->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfoLogin->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szNickName
	if(MAX_NAME_SIZE < pkPlayerInfoLogin->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerInfoLogin->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkPlayerInfoLogin->szNickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfoLogin->usLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfoLogin->ucRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usClass
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfoLogin->usClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usHair
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfoLogin->usHair), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usHairColor
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfoLogin->usHairColor), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usFace
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfoLogin->usFace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usPortrait
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerInfoLogin->usPortrait), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usMapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfoLogin->usMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode iPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfoLogin->iPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode iPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerInfoLogin->iPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bSex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkPlayerInfoLogin->bSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equipInfo
	if(EnCode__PlayerInfo_2_Equips(&(pkPlayerInfoLogin->equipInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerInfoLogin(void* pData)
{
	PlayerInfoLogin* pkPlayerInfoLogin = (PlayerInfoLogin*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfoLogin->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfoLogin->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szNickName
	if(MAX_NAME_SIZE < pkPlayerInfoLogin->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerInfoLogin->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkPlayerInfoLogin->szNickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfoLogin->usLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfoLogin->ucRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usClass
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfoLogin->usClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usHair
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfoLogin->usHair), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usHairColor
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfoLogin->usHairColor), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usFace
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfoLogin->usFace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usPortrait
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerInfoLogin->usPortrait), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usMapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfoLogin->usMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode iPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfoLogin->iPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode iPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerInfoLogin->iPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bSex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkPlayerInfoLogin->bSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equipInfo
	if(DeCode__PlayerInfo_2_Equips(&(pkPlayerInfoLogin->equipInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(PlayerInfoLogin);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerTmpInfo(void* pData)
{
	PlayerTmpInfo* pkPlayerTmpInfo = (PlayerTmpInfo*)(pData);

	//EnCode nMagicAttack
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMagicDefend
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nMagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPhysicsAttack
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPhysicsDefend
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nPhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPhysicsHit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nPhysicsHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMagicHit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nMagicHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nNormalState
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nNormalState), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMaxHP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lMaxHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMaxMP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lMaxMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nStrength
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nStrength), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAgility
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nAgility), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nIntelligence
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->nIntelligence), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lNextLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lLastLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lLastLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextPhyLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lNextPhyLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lLastPhyLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lLastPhyLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextMagLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lNextMagLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lLastMagLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lLastMagLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextPhyPointExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lNextPhyPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lLastPhyPointExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lLastPhyPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextMagPointExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lNextMagPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lLastMagPointExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->lLastMagPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode currentLevelMaxPhyPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerTmpInfo->currentLevelMaxPhyPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode currentLevelMaxMagPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerTmpInfo->currentLevelMaxMagPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode maxLimitPhyExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->maxLimitPhyExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode maxLimitMagExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerTmpInfo->maxLimitMagExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode phySkillTitleSize
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerTmpInfo->phySkillTitleSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode phySkillTitle
	if(16 < pkPlayerTmpInfo->phySkillTitleSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerTmpInfo->phySkillTitleSize;
	if(!m_kPackage.Pack("CHAR", &(pkPlayerTmpInfo->phySkillTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode magSkillTitleSize
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkPlayerTmpInfo->magSkillTitleSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode magSkillTitle
	if(16 < pkPlayerTmpInfo->magSkillTitleSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerTmpInfo->magSkillTitleSize;
	if(!m_kPackage.Pack("CHAR", &(pkPlayerTmpInfo->magSkillTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fSpeed
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkPlayerTmpInfo->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerTmpInfo(void* pData)
{
	PlayerTmpInfo* pkPlayerTmpInfo = (PlayerTmpInfo*)(pData);

	//DeCode nMagicAttack
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMagicDefend
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nMagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPhysicsAttack
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPhysicsDefend
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nPhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPhysicsHit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nPhysicsHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMagicHit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nMagicHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nNormalState
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nNormalState), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMaxHP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lMaxHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMaxMP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lMaxMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nStrength
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nStrength), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAgility
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nAgility), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nIntelligence
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->nIntelligence), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lNextLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lLastLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lLastLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextPhyLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lNextPhyLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lLastPhyLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lLastPhyLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextMagLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lNextMagLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lLastMagLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lLastMagLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextPhyPointExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lNextPhyPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lLastPhyPointExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lLastPhyPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextMagPointExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lNextMagPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lLastMagPointExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->lLastMagPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode currentLevelMaxPhyPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerTmpInfo->currentLevelMaxPhyPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode currentLevelMaxMagPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerTmpInfo->currentLevelMaxMagPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode maxLimitPhyExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->maxLimitPhyExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode maxLimitMagExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerTmpInfo->maxLimitMagExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode phySkillTitleSize
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerTmpInfo->phySkillTitleSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode phySkillTitle
	if(16 < pkPlayerTmpInfo->phySkillTitleSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerTmpInfo->phySkillTitleSize;
	if(!m_kPackage.UnPack("CHAR", &(pkPlayerTmpInfo->phySkillTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode magSkillTitleSize
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkPlayerTmpInfo->magSkillTitleSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode magSkillTitle
	if(16 < pkPlayerTmpInfo->magSkillTitleSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkPlayerTmpInfo->magSkillTitleSize;
	if(!m_kPackage.UnPack("CHAR", &(pkPlayerTmpInfo->magSkillTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkPlayerTmpInfo->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PlayerTmpInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__PlayerBattleInfo(void* pData)
{
	PlayerBattleInfo* pkPlayerBattleInfo = (PlayerBattleInfo*)(pData);

	//EnCode nMagicAttack
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMagicDefend
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nMagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPhysicsAttack
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPhysicsDefend
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nPhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPhysicsHit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nPhysicsHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMagicHit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nMagicHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nNormalState
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nNormalState), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMaxHP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->lMaxHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMaxMP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->lMaxMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nStrength
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nStrength), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAgility
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nAgility), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nIntelligence
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nIntelligence), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->lNextLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lLastLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->lLastLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextPhyLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->lNextPhyLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextMagLevelExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->lNextMagLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextPhyPointExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->lNextPhyPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lNextMagPointExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->lNextMagPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPhyCriLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nPhyCriLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMagCriLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkPlayerBattleInfo->nMagCriLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fSpeed
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkPlayerBattleInfo->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__PlayerBattleInfo(void* pData)
{
	PlayerBattleInfo* pkPlayerBattleInfo = (PlayerBattleInfo*)(pData);

	//DeCode nMagicAttack
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMagicDefend
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nMagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPhysicsAttack
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPhysicsDefend
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nPhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPhysicsHit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nPhysicsHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMagicHit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nMagicHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nNormalState
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nNormalState), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMaxHP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->lMaxHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMaxMP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->lMaxMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nStrength
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nStrength), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAgility
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nAgility), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nIntelligence
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nIntelligence), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->lNextLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lLastLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->lLastLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextPhyLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->lNextPhyLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextMagLevelExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->lNextMagLevelExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextPhyPointExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->lNextPhyPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lNextMagPointExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->lNextMagPointExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPhyCriLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nPhyCriLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMagCriLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkPlayerBattleInfo->nMagCriLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkPlayerBattleInfo->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(PlayerBattleInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__EquipAdditonInfo(void* pData)
{
	EquipAdditonInfo* pkEquipAdditonInfo = (EquipAdditonInfo*)(pData);

	//EnCode AddHP
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddMP
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddStr
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddStr), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddInt
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddInt), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddAgi
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddAgi), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddPhysicsHit
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddPhysicsHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddMagicHit
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddMagicHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddPhysicsHitOdds
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddPhysicsHitOdds), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddMagicHitOdds
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddMagicHitOdds), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddPhysicsNum
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddPhysicsNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddMagicNum
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddMagicNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddPhysicsAttack
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddMagicAttack
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddDecPhyNumHarm
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddDecPhyNumHarm), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddDecMagNumHarm
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddDecMagNumHarm), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddPhysicsNumAffix
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddPhysicsNumAffix), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddMagicNumAffix
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddMagicNumAffix), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddPhysicsDef
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddPhysicsDef), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddMagicDef
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipAdditonInfo->AddMagicDef), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__EquipAdditonInfo(void* pData)
{
	EquipAdditonInfo* pkEquipAdditonInfo = (EquipAdditonInfo*)(pData);

	//DeCode AddHP
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddMP
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddStr
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddStr), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddInt
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddInt), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddAgi
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddAgi), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddPhysicsHit
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddPhysicsHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddMagicHit
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddMagicHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddPhysicsHitOdds
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddPhysicsHitOdds), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddMagicHitOdds
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddMagicHitOdds), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddPhysicsNum
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddPhysicsNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddMagicNum
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddMagicNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddPhysicsAttack
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddMagicAttack
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddDecPhyNumHarm
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddDecPhyNumHarm), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddDecMagNumHarm
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddDecMagNumHarm), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddPhysicsNumAffix
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddPhysicsNumAffix), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddMagicNumAffix
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddMagicNumAffix), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddPhysicsDef
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddPhysicsDef), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddMagicDef
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipAdditonInfo->AddMagicDef), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(EquipAdditonInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__WeaponPeculiarProp(void* pData)
{
	WeaponPeculiarProp* pkWeaponPeculiarProp = (WeaponPeculiarProp*)(pData);

	//EnCode mMasslevel
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->mMasslevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mTrade
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->mTrade), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mCannotChuck
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->mCannotChuck), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mDeathLost
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->mDeathLost), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mDeopt
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->mDeopt), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AttackPhyMax
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->AttackPhyMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AttackPhyMin
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->AttackPhyMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AttackMagMax
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->AttackMagMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AttackMagMin
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkWeaponPeculiarProp->AttackMagMin), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__WeaponPeculiarProp(void* pData)
{
	WeaponPeculiarProp* pkWeaponPeculiarProp = (WeaponPeculiarProp*)(pData);

	//DeCode mMasslevel
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->mMasslevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mTrade
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->mTrade), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mCannotChuck
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->mCannotChuck), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mDeathLost
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->mDeathLost), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mDeopt
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->mDeopt), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AttackPhyMax
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->AttackPhyMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AttackPhyMin
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->AttackPhyMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AttackMagMax
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->AttackMagMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AttackMagMin
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkWeaponPeculiarProp->AttackMagMin), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(WeaponPeculiarProp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__EquipPeculiarProp(void* pData)
{
	EquipPeculiarProp* pkEquipPeculiarProp = (EquipPeculiarProp*)(pData);

	//EnCode mMasslevel
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mMasslevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mTrade
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mTrade), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mCannotChuck
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mCannotChuck), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mDeathLost
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mDeathLost), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mDeopt
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mDeopt), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mPhysicsDefend
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mPhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mMagicDefend
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mMagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mPhysicsAttack
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mMagicAttack
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipPeculiarProp->mMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__EquipPeculiarProp(void* pData)
{
	EquipPeculiarProp* pkEquipPeculiarProp = (EquipPeculiarProp*)(pData);

	//DeCode mMasslevel
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mMasslevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mTrade
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mTrade), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mCannotChuck
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mCannotChuck), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mDeathLost
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mDeathLost), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mDeopt
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mDeopt), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mPhysicsDefend
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mPhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mMagicDefend
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mMagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mPhysicsAttack
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mMagicAttack
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipPeculiarProp->mMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(EquipPeculiarProp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__WeaponAdditionProp(void* pData)
{
	WeaponAdditionProp* pkWeaponAdditionProp = (WeaponAdditionProp*)(pData);

	//EnCode peculiarProp
	if(EnCode__WeaponPeculiarProp(&(pkWeaponAdditionProp->peculiarProp)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode addInfo
	if(EnCode__EquipAdditonInfo(&(pkWeaponAdditionProp->addInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__WeaponAdditionProp(void* pData)
{
	WeaponAdditionProp* pkWeaponAdditionProp = (WeaponAdditionProp*)(pData);

	//DeCode peculiarProp
	if(DeCode__WeaponPeculiarProp(&(pkWeaponAdditionProp->peculiarProp)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode addInfo
	if(DeCode__EquipAdditonInfo(&(pkWeaponAdditionProp->addInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(WeaponAdditionProp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__EquipAdditionProp(void* pData)
{
	EquipAdditionProp* pkEquipAdditionProp = (EquipAdditionProp*)(pData);

	//EnCode peculiarProp
	if(EnCode__EquipPeculiarProp(&(pkEquipAdditionProp->peculiarProp)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode addInfo
	if(EnCode__EquipAdditonInfo(&(pkEquipAdditionProp->addInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__EquipAdditionProp(void* pData)
{
	EquipAdditionProp* pkEquipAdditionProp = (EquipAdditionProp*)(pData);

	//DeCode peculiarProp
	if(DeCode__EquipPeculiarProp(&(pkEquipAdditionProp->peculiarProp)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode addInfo
	if(DeCode__EquipAdditonInfo(&(pkEquipAdditionProp->addInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(EquipAdditionProp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__EquipItemInfo(void* pData)
{
	EquipItemInfo* pkEquipItemInfo = (EquipItemInfo*)(pData);

	//EnCode mItemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkEquipItemInfo->mItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkEquipItemInfo->mItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mType
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mMasslevel
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mMasslevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mTrade
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mTrade), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mCannotChuck
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mCannotChuck), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mDeathLost
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mDeathLost), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mDeopt
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mDeopt), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mPhysicsDefend
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mPhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mPhysicsAttack
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mMagicAttack
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mMagicDefend
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkEquipItemInfo->mMagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode addInfo
	if(EnCode__EquipAdditonInfo(&(pkEquipItemInfo->addInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__EquipItemInfo(void* pData)
{
	EquipItemInfo* pkEquipItemInfo = (EquipItemInfo*)(pData);

	//DeCode mItemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkEquipItemInfo->mItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkEquipItemInfo->mItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mType
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mMasslevel
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mMasslevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mTrade
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mTrade), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mCannotChuck
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mCannotChuck), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mDeathLost
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mDeathLost), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mDeopt
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mDeopt), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mPhysicsDefend
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mPhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mPhysicsAttack
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mPhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mMagicAttack
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mMagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mMagicDefend
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkEquipItemInfo->mMagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode addInfo
	if(DeCode__EquipAdditonInfo(&(pkEquipItemInfo->addInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(EquipItemInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__ItemAddtionProp(void* pData)
{
	ItemAddtionProp* pkItemAddtionProp = (ItemAddtionProp*)(pData);

	//EnCode Hp
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->Hp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Mp
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->Mp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsAttack
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicAttack
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsDefend
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicDefend
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsHit
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicHit
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsCritical
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsCritical), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicCritical
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicCritical), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsAttackPercent
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsAttackPercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicAttackPercent
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicAttackPercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsCriticalDamage
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsCriticalDamage), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicCriticalDamage
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicCriticalDamage), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsDefendPercent
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsDefendPercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicDefendPercent
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicDefendPercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsCriticalDerate
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsCriticalDerate), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicCriticalDerate
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicCriticalDerate), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsCriticalDamageDerate
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsCriticalDamageDerate), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicCriticalDamageDerate
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicCriticalDamageDerate), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsJouk
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsJouk), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicJouk
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicJouk), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsDeratePercent
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsDeratePercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicDeratePercent
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicDeratePercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Stun
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->Stun), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Tie
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->Tie), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Antistun
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->Antistun), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Antitie
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->Antitie), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsAttackAdd
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsAttackAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicAttackAdd
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicAttackAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PhysicsRift
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->PhysicsRift), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MagicRift
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->MagicRift), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddSpeed
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->AddSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AttackPhyMin
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->AttackPhyMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AttackPhyMax
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->AttackPhyMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AttackMagMin
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->AttackMagMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AttackMagMax
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkItemAddtionProp->AttackMagMax), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__ItemAddtionProp(void* pData)
{
	ItemAddtionProp* pkItemAddtionProp = (ItemAddtionProp*)(pData);

	//DeCode Hp
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->Hp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Mp
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->Mp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsAttack
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicAttack
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicAttack), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsDefend
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicDefend
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicDefend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsHit
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicHit
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsCritical
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsCritical), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicCritical
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicCritical), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsAttackPercent
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsAttackPercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicAttackPercent
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicAttackPercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsCriticalDamage
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsCriticalDamage), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicCriticalDamage
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicCriticalDamage), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsDefendPercent
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsDefendPercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicDefendPercent
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicDefendPercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsCriticalDerate
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsCriticalDerate), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicCriticalDerate
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicCriticalDerate), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsCriticalDamageDerate
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsCriticalDamageDerate), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicCriticalDamageDerate
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicCriticalDamageDerate), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsJouk
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsJouk), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicJouk
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicJouk), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsDeratePercent
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsDeratePercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicDeratePercent
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicDeratePercent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Stun
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->Stun), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Tie
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->Tie), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Antistun
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->Antistun), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Antitie
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->Antitie), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsAttackAdd
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsAttackAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicAttackAdd
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicAttackAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PhysicsRift
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->PhysicsRift), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MagicRift
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->MagicRift), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->AddSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AttackPhyMin
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->AttackPhyMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AttackPhyMax
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->AttackPhyMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AttackMagMin
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->AttackMagMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AttackMagMax
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkItemAddtionProp->AttackMagMax), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(ItemAddtionProp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__TaskRecordInfo(void* pData)
{
	TaskRecordInfo* pkTaskRecordInfo = (TaskRecordInfo*)(pData);

	//EnCode taskID
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkTaskRecordInfo->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode completeCount
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTaskRecordInfo->completeCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode processData
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkTaskRecordInfo->processData), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode taskTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTaskRecordInfo->taskTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode taskState
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTaskRecordInfo->taskState), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__TaskRecordInfo(void* pData)
{
	TaskRecordInfo* pkTaskRecordInfo = (TaskRecordInfo*)(pData);

	//DeCode taskID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkTaskRecordInfo->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode completeCount
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTaskRecordInfo->completeCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode processData
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkTaskRecordInfo->processData), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode taskTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTaskRecordInfo->taskTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode taskState
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTaskRecordInfo->taskState), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(TaskRecordInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__FriendGroupInfo(void* pData)
{
	FriendGroupInfo* pkFriendGroupInfo = (FriendGroupInfo*)(pData);

	//EnCode groupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendGroupInfo->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendGroupInfo->groupNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupName
	if(MAX_NAME_SIZE < pkFriendGroupInfo->groupNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendGroupInfo->groupNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkFriendGroupInfo->groupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__FriendGroupInfo(void* pData)
{
	FriendGroupInfo* pkFriendGroupInfo = (FriendGroupInfo*)(pData);

	//DeCode groupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendGroupInfo->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendGroupInfo->groupNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupName
	if(MAX_NAME_SIZE < pkFriendGroupInfo->groupNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendGroupInfo->groupNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendGroupInfo->groupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(FriendGroupInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__FriendInfo(void* pData)
{
	FriendInfo* pkFriendInfo = (FriendInfo*)(pData);

	//EnCode friendID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode joinID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->joinID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickName
	if(MAX_NAME_SIZE < pkFriendInfo->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->nickNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isOnline
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->isOnline), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isHateMe
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkFriendInfo->isHateMe), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerSex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkFriendInfo->playerSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFriendInfo->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if(MAX_NAME_SIZE < pkFriendInfo->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFriendInfo->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if(MAX_NAME_SIZE < pkFriendInfo->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFriendInfo->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__FriendInfo(void* pData)
{
	FriendInfo* pkFriendInfo = (FriendInfo*)(pData);

	//DeCode friendID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode joinID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->joinID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickName
	if(MAX_NAME_SIZE < pkFriendInfo->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->nickNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isOnline
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->isOnline), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isHateMe
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkFriendInfo->isHateMe), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerSex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkFriendInfo->playerSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFriendInfo->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if(MAX_NAME_SIZE < pkFriendInfo->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFriendInfo->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if(MAX_NAME_SIZE < pkFriendInfo->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFriendInfo->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFriendInfo->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(FriendInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__FoeInfo(void* pData)
{
	FoeInfo* pkFoeInfo = (FoeInfo*)(pData);

	//EnCode foeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFoeInfo->foeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode joinID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFoeInfo->joinID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFoeInfo->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickName
	if(MAX_NAME_SIZE < pkFoeInfo->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFoeInfo->nickNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkFoeInfo->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerID
	if(EnCode__PlayerID(&(pkFoeInfo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFoeInfo->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFoeInfo->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFoeInfo->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFoeInfo->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerIcon
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkFoeInfo->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerSex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkFoeInfo->playerSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isLocked
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkFoeInfo->isLocked), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isHateMe
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkFoeInfo->isHateMe), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isOnline
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkFoeInfo->isOnline), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFoeInfo->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if(MAX_NAME_SIZE < pkFoeInfo->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFoeInfo->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFoeInfo->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFoeInfo->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if(MAX_NAME_SIZE < pkFoeInfo->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFoeInfo->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkFoeInfo->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__FoeInfo(void* pData)
{
	FoeInfo* pkFoeInfo = (FoeInfo*)(pData);

	//DeCode foeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFoeInfo->foeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode joinID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFoeInfo->joinID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFoeInfo->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickName
	if(MAX_NAME_SIZE < pkFoeInfo->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFoeInfo->nickNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkFoeInfo->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerID
	if(DeCode__PlayerID(&(pkFoeInfo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFoeInfo->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFoeInfo->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFoeInfo->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFoeInfo->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerIcon
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkFoeInfo->playerIcon), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerSex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkFoeInfo->playerSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isLocked
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkFoeInfo->isLocked), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isHateMe
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkFoeInfo->isHateMe), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isOnline
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkFoeInfo->isOnline), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFoeInfo->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if(MAX_NAME_SIZE < pkFoeInfo->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFoeInfo->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFoeInfo->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFoeInfo->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if(MAX_NAME_SIZE < pkFoeInfo->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkFoeInfo->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkFoeInfo->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(FoeInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__BlackListItemInfo(void* pData)
{
	BlackListItemInfo* pkBlackListItemInfo = (BlackListItemInfo*)(pData);

	//EnCode itemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBlackListItemInfo->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode joinID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBlackListItemInfo->joinID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkBlackListItemInfo->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickName
	if(MAX_NAME_SIZE < pkBlackListItemInfo->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkBlackListItemInfo->nickNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkBlackListItemInfo->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__BlackListItemInfo(void* pData)
{
	BlackListItemInfo* pkBlackListItemInfo = (BlackListItemInfo*)(pData);

	//DeCode itemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBlackListItemInfo->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode joinID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBlackListItemInfo->joinID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkBlackListItemInfo->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickName
	if(MAX_NAME_SIZE < pkBlackListItemInfo->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkBlackListItemInfo->nickNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkBlackListItemInfo->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(BlackListItemInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__FoeDetailInfo(void* pData)
{
	FoeDetailInfo* pkFoeDetailInfo = (FoeDetailInfo*)(pData);

	//EnCode foeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkFoeDetailInfo->foeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__FoeDetailInfo(void* pData)
{
	FoeDetailInfo* pkFoeDetailInfo = (FoeDetailInfo*)(pData);

	//DeCode foeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkFoeDetailInfo->foeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(FoeDetailInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__WearChangedItems(void* pData)
{
	WearChangedItems* pkWearChangedItems = (WearChangedItems*)(pData);

	//EnCode itemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkWearChangedItems->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newWear
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkWearChangedItems->newWear), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__WearChangedItems(void* pData)
{
	WearChangedItems* pkWearChangedItems = (WearChangedItems*)(pData);

	//DeCode itemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkWearChangedItems->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newWear
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkWearChangedItems->newWear), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(WearChangedItems);
}

size_t	MUX_PROTO::CliProtocol::EnCode__ThreatCliInfo(void* pData)
{
	ThreatCliInfo* pkThreatCliInfo = (ThreatCliInfo*)(pData);

	//EnCode threatPlayerID
	if(EnCode__PlayerID(&(pkThreatCliInfo->threatPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode threatValue
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkThreatCliInfo->threatValue), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__ThreatCliInfo(void* pData)
{
	ThreatCliInfo* pkThreatCliInfo = (ThreatCliInfo*)(pData);

	//DeCode threatPlayerID
	if(DeCode__PlayerID(&(pkThreatCliInfo->threatPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode threatValue
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkThreatCliInfo->threatValue), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(ThreatCliInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__ThreatList(void* pData)
{
	ThreatList* pkThreatList = (ThreatList*)(pData);

	//EnCode threatCount
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkThreatList->threatCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode threatList
	if(10 < pkThreatList->threatCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkThreatList->threatCount; ++i)
	{
		if(EnCode__ThreatCliInfo(&(pkThreatList->threatList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__ThreatList(void* pData)
{
	ThreatList* pkThreatList = (ThreatList*)(pData);

	//DeCode threatCount
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkThreatList->threatCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode threatList
	if(10 < pkThreatList->threatCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkThreatList->threatCount; ++i)
	{
		if(DeCode__ThreatCliInfo(&(pkThreatList->threatList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(ThreatList);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GroupChatMemberInfo(void* pData)
{
	GroupChatMemberInfo* pkGroupChatMemberInfo = (GroupChatMemberInfo*)(pData);

	//EnCode memberNameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGroupChatMemberInfo->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(MAX_NAME_SIZE < pkGroupChatMemberInfo->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGroupChatMemberInfo->memberNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGroupChatMemberInfo->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GroupChatMemberInfo(void* pData)
{
	GroupChatMemberInfo* pkGroupChatMemberInfo = (GroupChatMemberInfo*)(pData);

	//DeCode memberNameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGroupChatMemberInfo->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(MAX_NAME_SIZE < pkGroupChatMemberInfo->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGroupChatMemberInfo->memberNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGroupChatMemberInfo->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GroupChatMemberInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__UnionMember(void* pData)
{
	UnionMember* pkUnionMember = (UnionMember*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMember->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMember->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMember->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_PLAYERNAME_LEN < pkUnionMember->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMember->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionMember->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode job
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->job), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posSeq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if(MAX_PLAYERNAME_LEN < pkUnionMember->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMember->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionMember->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lastQuit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionMember->lastQuit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode forbid
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->forbid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode onLine
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMember->onLine), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__UnionMember(void* pData)
{
	UnionMember* pkUnionMember = (UnionMember*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMember->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMember->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMember->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_PLAYERNAME_LEN < pkUnionMember->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMember->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionMember->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode job
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->job), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posSeq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if(MAX_PLAYERNAME_LEN < pkUnionMember->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionMember->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionMember->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lastQuit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionMember->lastQuit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode forbid
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->forbid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode onLine
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMember->onLine), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(UnionMember);
}

size_t	MUX_PROTO::CliProtocol::EnCode__UnionPosRight(void* pData)
{
	UnionPosRight* pkUnionPosRight = (UnionPosRight*)(pData);

	//EnCode seq
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionPosRight->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionPosRight->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if(8 < pkUnionPosRight->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionPosRight->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnionPosRight->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode right
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionPosRight->right), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__UnionPosRight(void* pData)
{
	UnionPosRight* pkUnionPosRight = (UnionPosRight*)(pData);

	//DeCode seq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionPosRight->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionPosRight->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if(8 < pkUnionPosRight->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnionPosRight->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnionPosRight->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode right
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionPosRight->right), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(UnionPosRight);
}

size_t	MUX_PROTO::CliProtocol::EnCode__Union(void* pData)
{
	Union* pkUnion = (Union*)(pData);

	//EnCode id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnion->id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_UNIONNAME_LEN < pkUnion->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnion->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnion->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode level
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode num
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->num), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ownerUiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnion->ownerUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if(MAX_UNIONTITLE_LEN < pkUnion->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnion->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkUnion->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnion->active), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posListLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnion->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posList
	if(10 < pkUnion->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnion->posListLen; ++i)
	{
		if(EnCode__UnionPosRight(&(pkUnion->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__Union(void* pData)
{
	Union* pkUnion = (Union*)(pData);

	//DeCode id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnion->id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_UNIONNAME_LEN < pkUnion->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnion->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnion->name), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode level
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode num
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->num), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ownerUiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnion->ownerUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if(MAX_UNIONTITLE_LEN < pkUnion->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUnion->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUnion->title), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnion->active), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posListLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnion->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posList
	if(10 < pkUnion->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnion->posListLen; ++i)
	{
		if(DeCode__UnionPosRight(&(pkUnion->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(Union);
}

size_t	MUX_PROTO::CliProtocol::EnCode__UnionMembers(void* pData)
{
	UnionMembers* pkUnionMembers = (UnionMembers*)(pData);

	//EnCode count
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionMembers->count), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if(4 < pkUnionMembers->count)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnionMembers->count; ++i)
	{
		if(EnCode__UnionMember(&(pkUnionMembers->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__UnionMembers(void* pData)
{
	UnionMembers* pkUnionMembers = (UnionMembers*)(pData);

	//DeCode count
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionMembers->count), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if(4 < pkUnionMembers->count)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkUnionMembers->count; ++i)
	{
		if(DeCode__UnionMember(&(pkUnionMembers->members[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(UnionMembers);
}

size_t	MUX_PROTO::CliProtocol::EnCode__MailConciseInfo(void* pData)
{
	MailConciseInfo* pkMailConciseInfo = (MailConciseInfo*)(pData);

	//EnCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkMailConciseInfo->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkMailConciseInfo->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sendPlayerName
	if(MAX_NAME_SIZE < pkMailConciseInfo->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMailConciseInfo->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkMailConciseInfo->sendPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkMailConciseInfo->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mailTitle
	if(MAX_MAIL_CONCISE_TITLE_LEN < pkMailConciseInfo->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMailConciseInfo->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkMailConciseInfo->mailTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode expireTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkMailConciseInfo->expireTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isRead
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkMailConciseInfo->isRead), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__MailConciseInfo(void* pData)
{
	MailConciseInfo* pkMailConciseInfo = (MailConciseInfo*)(pData);

	//DeCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkMailConciseInfo->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkMailConciseInfo->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sendPlayerName
	if(MAX_NAME_SIZE < pkMailConciseInfo->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMailConciseInfo->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkMailConciseInfo->sendPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkMailConciseInfo->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mailTitle
	if(MAX_MAIL_CONCISE_TITLE_LEN < pkMailConciseInfo->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMailConciseInfo->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkMailConciseInfo->mailTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode expireTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkMailConciseInfo->expireTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isRead
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkMailConciseInfo->isRead), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(MailConciseInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPlayerBeyondBlock(void* pData)
{
	CGPlayerBeyondBlock* pkCGPlayerBeyondBlock = (CGPlayerBeyondBlock*)(pData);

	//EnCode fCurX
	size_t unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPlayerBeyondBlock->fCurX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fCurY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPlayerBeyondBlock->fCurY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fTargetX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPlayerBeyondBlock->fTargetX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fTargetY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPlayerBeyondBlock->fTargetY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPlayerBeyondBlock(void* pData)
{
	CGPlayerBeyondBlock* pkCGPlayerBeyondBlock = (CGPlayerBeyondBlock*)(pData);

	//DeCode fCurX
	size_t unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPlayerBeyondBlock->fCurX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fCurY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPlayerBeyondBlock->fCurY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fTargetX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPlayerBeyondBlock->fTargetX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fTargetY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPlayerBeyondBlock->fTargetY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPlayerBeyondBlock);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddActivePtReq(void* pData)
{
	CGAddActivePtReq* pkCGAddActivePtReq = (CGAddActivePtReq*)(pData);

	//EnCode reqID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddActivePtReq->reqID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddActivePtReq->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickName
	if(MAX_NAME_SIZE < pkCGAddActivePtReq->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddActivePtReq->nickNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGAddActivePtReq->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddActivePtReq(void* pData)
{
	CGAddActivePtReq* pkCGAddActivePtReq = (CGAddActivePtReq*)(pData);

	//DeCode reqID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddActivePtReq->reqID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddActivePtReq->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickName
	if(MAX_NAME_SIZE < pkCGAddActivePtReq->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddActivePtReq->nickNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGAddActivePtReq->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddActivePtReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryEvilTime(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryEvilTime(void* pData)
{
	(pData);
	return sizeof(CGQueryEvilTime);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUseDamageItem(void* pData)
{
	CGUseDamageItem* pkCGUseDamageItem = (CGUseDamageItem*)(pData);

	//EnCode itemTypeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUseDamageItem->itemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attackPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGUseDamageItem->attackPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attackPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGUseDamageItem->attackPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetID
	if(EnCode__PlayerID(&(pkCGUseDamageItem->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUseDamageItem(void* pData)
{
	CGUseDamageItem* pkCGUseDamageItem = (CGUseDamageItem*)(pData);

	//DeCode itemTypeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUseDamageItem->itemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attackPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGUseDamageItem->attackPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attackPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGUseDamageItem->attackPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetID
	if(DeCode__PlayerID(&(pkCGUseDamageItem->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGUseDamageItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddSthToPlate(void* pData)
{
	CGAddSthToPlate* pkCGAddSthToPlate = (CGAddSthToPlate*)(pData);

	//EnCode plateType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddSthToPlate->plateType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode platePos
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddSthToPlate->platePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sthID
	if(EnCode__ItemInfo_i(&(pkCGAddSthToPlate->sthID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddSthToPlate(void* pData)
{
	CGAddSthToPlate* pkCGAddSthToPlate = (CGAddSthToPlate*)(pData);

	//DeCode plateType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddSthToPlate->plateType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode platePos
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddSthToPlate->platePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sthID
	if(DeCode__ItemInfo_i(&(pkCGAddSthToPlate->sthID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddSthToPlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGTakeSthFromPlate(void* pData)
{
	CGTakeSthFromPlate* pkCGTakeSthFromPlate = (CGTakeSthFromPlate*)(pData);

	//EnCode platePos
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGTakeSthFromPlate->platePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sthID
	if(EnCode__ItemInfo_i(&(pkCGTakeSthFromPlate->sthID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGTakeSthFromPlate(void* pData)
{
	CGTakeSthFromPlate* pkCGTakeSthFromPlate = (CGTakeSthFromPlate*)(pData);

	//DeCode platePos
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGTakeSthFromPlate->platePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sthID
	if(DeCode__ItemInfo_i(&(pkCGTakeSthFromPlate->sthID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGTakeSthFromPlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPlateExec(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPlateExec(void* pData)
{
	(pData);
	return sizeof(CGPlateExec);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGClosePlate(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGClosePlate(void* pData)
{
	(pData);
	return sizeof(CGClosePlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryPkg(void* pData)
{
	CGQueryPkg* pkCGQueryPkg = (CGQueryPkg*)(pData);

	//EnCode nGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryPkg->nGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryPkg(void* pData)
{
	CGQueryPkg* pkCGQueryPkg = (CGQueryPkg*)(pData);

	//DeCode nGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryPkg->nGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryPkg);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGNetDetect(void* pData)
{
	CGNetDetect* pkCGNetDetect = (CGNetDetect*)(pData);

	//EnCode pkgID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGNetDetect->pkgID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGNetDetect(void* pData)
{
	CGNetDetect* pkCGNetDetect = (CGNetDetect*)(pData);

	//DeCode pkgID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGNetDetect->pkgID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGNetDetect);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChat(void* pData)
{
	CGChat* pkCGChat = (CGChat*)(pData);

	//EnCode nType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChat->nType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetPlayerID
	if(EnCode__PlayerID(&(pkCGChat->targetPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode tgtNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChat->tgtNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tgtName
	if(MAX_NAME_SIZE < pkCGChat->tgtNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGChat->tgtNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGChat->tgtName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChat->chatSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode strChat
	if(MAX_CHAT_SIZE < pkCGChat->chatSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGChat->chatSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGChat->strChat), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChat(void* pData)
{
	CGChat* pkCGChat = (CGChat*)(pData);

	//DeCode nType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChat->nType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetPlayerID
	if(DeCode__PlayerID(&(pkCGChat->targetPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode tgtNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChat->tgtNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tgtName
	if(MAX_NAME_SIZE < pkCGChat->tgtNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGChat->tgtNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGChat->tgtName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChat->chatSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode strChat
	if(MAX_CHAT_SIZE < pkCGChat->chatSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGChat->chatSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGChat->strChat), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChat);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGTimeCheck(void* pData)
{
	CGTimeCheck* pkCGTimeCheck = (CGTimeCheck*)(pData);

	//EnCode checkID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGTimeCheck->checkID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGTimeCheck->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGTimeCheck(void* pData)
{
	CGTimeCheck* pkCGTimeCheck = (CGTimeCheck*)(pData);

	//DeCode checkID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGTimeCheck->checkID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGTimeCheck->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGTimeCheck);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGReqRoleInfo(void* pData)
{
	CGReqRoleInfo* pkCGReqRoleInfo = (CGReqRoleInfo*)(pData);

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkCGReqRoleInfo->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGReqRoleInfo(void* pData)
{
	CGReqRoleInfo* pkCGReqRoleInfo = (CGReqRoleInfo*)(pData);

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkCGReqRoleInfo->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGReqRoleInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGReqMonsterInfo(void* pData)
{
	CGReqMonsterInfo* pkCGReqMonsterInfo = (CGReqMonsterInfo*)(pData);

	//EnCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGReqMonsterInfo->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGReqMonsterInfo(void* pData)
{
	CGReqMonsterInfo* pkCGReqMonsterInfo = (CGReqMonsterInfo*)(pData);

	//DeCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGReqMonsterInfo->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGReqMonsterInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRun(void* pData)
{
	CGRun* pkCGRun = (CGRun*)(pData);

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkCGRun->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fOrgWorldPosX
	size_t unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGRun->fOrgWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fOrgWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGRun->fOrgWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fNewWorldPosX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGRun->fNewWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fNewWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGRun->fNewWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRun->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRun(void* pData)
{
	CGRun* pkCGRun = (CGRun*)(pData);

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkCGRun->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fOrgWorldPosX
	size_t unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGRun->fOrgWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fOrgWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGRun->fOrgWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fNewWorldPosX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGRun->fNewWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fNewWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGRun->fNewWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRun->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRun);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRequestRandomString(void* pData)
{
	CGRequestRandomString* pkCGRequestRandomString = (CGRequestRandomString*)(pData);

	//EnCode accountSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRequestRandomString->accountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode strUserAccount
	if(32 < pkCGRequestRandomString->accountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGRequestRandomString->accountSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGRequestRandomString->strUserAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRequestRandomString->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRequestRandomString(void* pData)
{
	CGRequestRandomString* pkCGRequestRandomString = (CGRequestRandomString*)(pData);

	//DeCode accountSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRequestRandomString->accountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode strUserAccount
	if(32 < pkCGRequestRandomString->accountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGRequestRandomString->accountSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGRequestRandomString->strUserAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRequestRandomString->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRequestRandomString);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUserMD5Value(void* pData)
{
	CGUserMD5Value* pkCGUserMD5Value = (CGUserMD5Value*)(pData);

	//EnCode md5ValueSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUserMD5Value->md5ValueSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode md5Value
	if(MD5_VALUE_CHARLEN < pkCGUserMD5Value->md5ValueSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGUserMD5Value->md5ValueSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGUserMD5Value->md5Value), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUserMD5Value->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUserMD5Value(void* pData)
{
	CGUserMD5Value* pkCGUserMD5Value = (CGUserMD5Value*)(pData);

	//DeCode md5ValueSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUserMD5Value->md5ValueSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode md5Value
	if(MD5_VALUE_CHARLEN < pkCGUserMD5Value->md5ValueSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGUserMD5Value->md5ValueSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGUserMD5Value->md5Value), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUserMD5Value->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGUserMD5Value);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGLogin(void* pData)
{
	CGLogin* pkCGLogin = (CGLogin*)(pData);

	//EnCode accountSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGLogin->accountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode strUserAccount
	if(32 < pkCGLogin->accountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGLogin->accountSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGLogin->strUserAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode passwdSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGLogin->passwdSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode strUserPassword
	if(32 < pkCGLogin->passwdSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGLogin->passwdSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGLogin->strUserPassword), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGLogin(void* pData)
{
	CGLogin* pkCGLogin = (CGLogin*)(pData);

	//DeCode accountSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGLogin->accountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode strUserAccount
	if(32 < pkCGLogin->accountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGLogin->accountSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGLogin->strUserAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode passwdSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGLogin->passwdSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode strUserPassword
	if(32 < pkCGLogin->passwdSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGLogin->passwdSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGLogin->strUserPassword), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGLogin);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSelectRole(void* pData)
{
	CGSelectRole* pkCGSelectRole = (CGSelectRole*)(pData);

	//EnCode uiRoleNo
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGSelectRole->uiRoleNo), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGSelectRole->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSelectRole(void* pData)
{
	CGSelectRole* pkCGSelectRole = (CGSelectRole*)(pData);

	//DeCode uiRoleNo
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGSelectRole->uiRoleNo), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGSelectRole->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSelectRole);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGEnterWorld(void* pData)
{
	CGEnterWorld* pkCGEnterWorld = (CGEnterWorld*)(pData);

	//EnCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGEnterWorld->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGEnterWorld(void* pData)
{
	CGEnterWorld* pkCGEnterWorld = (CGEnterWorld*)(pData);

	//DeCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGEnterWorld->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGEnterWorld);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGIrcEnterWorld(void* pData)
{
	CGIrcEnterWorld* pkCGIrcEnterWorld = (CGIrcEnterWorld*)(pData);

	//EnCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGIrcEnterWorld->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGIrcEnterWorld(void* pData)
{
	CGIrcEnterWorld* pkCGIrcEnterWorld = (CGIrcEnterWorld*)(pData);

	//DeCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGIrcEnterWorld->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGIrcEnterWorld);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCIrcEnterWorldRst(void* pData)
{
	GCIrcEnterWorldRst* pkGCIrcEnterWorldRst = (GCIrcEnterWorldRst*)(pData);

	//EnCode bSuccess
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCIrcEnterWorldRst->bSuccess), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCIrcEnterWorldRst(void* pData)
{
	GCIrcEnterWorldRst* pkGCIrcEnterWorldRst = (GCIrcEnterWorldRst*)(pData);

	//DeCode bSuccess
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCIrcEnterWorldRst->bSuccess), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCIrcEnterWorldRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSwitchEnterNewMap(void* pData)
{
	CGSwitchEnterNewMap* pkCGSwitchEnterNewMap = (CGSwitchEnterNewMap*)(pData);

	//EnCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGSwitchEnterNewMap->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSwitchEnterNewMap(void* pData)
{
	CGSwitchEnterNewMap* pkCGSwitchEnterNewMap = (CGSwitchEnterNewMap*)(pData);

	//DeCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGSwitchEnterNewMap->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSwitchEnterNewMap);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAttackMonster(void* pData)
{
	CGAttackMonster* pkCGAttackMonster = (CGAttackMonster*)(pData);

	//EnCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAttackMonster->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attackerID
	if(EnCode__PlayerID(&(pkCGAttackMonster->attackerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode lSkillType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAttackMonster->lSkillType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAttackPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGAttackMonster->nAttackPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAttackPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGAttackMonster->nAttackPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAttackerPosX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGAttackMonster->nAttackerPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAttackerPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGAttackMonster->nAttackerPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAttackMonster->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAttackMonster(void* pData)
{
	CGAttackMonster* pkCGAttackMonster = (CGAttackMonster*)(pData);

	//DeCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAttackMonster->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attackerID
	if(DeCode__PlayerID(&(pkCGAttackMonster->attackerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode lSkillType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAttackMonster->lSkillType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAttackPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGAttackMonster->nAttackPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAttackPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGAttackMonster->nAttackPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAttackerPosX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGAttackMonster->nAttackerPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAttackerPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGAttackMonster->nAttackerPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAttackMonster->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAttackMonster);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAttackPlayer(void* pData)
{
	CGAttackPlayer* pkCGAttackPlayer = (CGAttackPlayer*)(pData);

	//EnCode attackerID
	if(EnCode__PlayerID(&(pkCGAttackPlayer->attackerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode targetID
	if(EnCode__PlayerID(&(pkCGAttackPlayer->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode lSkillType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAttackPlayer->lSkillType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAttackerPosX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGAttackPlayer->nAttackerPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAttackerPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGAttackPlayer->nAttackerPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAttackPlayer->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAttackPlayer(void* pData)
{
	CGAttackPlayer* pkCGAttackPlayer = (CGAttackPlayer*)(pData);

	//DeCode attackerID
	if(DeCode__PlayerID(&(pkCGAttackPlayer->attackerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode targetID
	if(DeCode__PlayerID(&(pkCGAttackPlayer->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode lSkillType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAttackPlayer->lSkillType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAttackerPosX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGAttackPlayer->nAttackerPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAttackerPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGAttackPlayer->nAttackerPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAttackPlayer->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAttackPlayer);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSwitchMap(void* pData)
{
	CGSwitchMap* pkCGSwitchMap = (CGSwitchMap*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkCGSwitchMap->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode usMapID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGSwitchMap->usMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode iPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGSwitchMap->iPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode iPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGSwitchMap->iPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGSwitchMap->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSwitchMap(void* pData)
{
	CGSwitchMap* pkCGSwitchMap = (CGSwitchMap*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkCGSwitchMap->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode usMapID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGSwitchMap->usMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode iPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGSwitchMap->iPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode iPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGSwitchMap->iPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGSwitchMap->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSwitchMap);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGCreateRole(void* pData)
{
	CGCreateRole* pkCGCreateRole = (CGCreateRole*)(pData);

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCreateRole->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode strNickName
	if(32 < pkCGCreateRole->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateRole->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGCreateRole->strNickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode wRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGCreateRole->wRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode wSex
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGCreateRole->wSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nClass
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCreateRole->nClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nFace
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCreateRole->nFace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nHair
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCreateRole->nHair), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usPortrait
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGCreateRole->usPortrait), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCreateRole->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGCreateRole(void* pData)
{
	CGCreateRole* pkCGCreateRole = (CGCreateRole*)(pData);

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCreateRole->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode strNickName
	if(32 < pkCGCreateRole->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateRole->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGCreateRole->strNickName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode wRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGCreateRole->wRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode wSex
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGCreateRole->wSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nClass
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCreateRole->nClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nFace
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCreateRole->nFace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nHair
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCreateRole->nHair), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usPortrait
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGCreateRole->usPortrait), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCreateRole->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGCreateRole);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDeleteRole(void* pData)
{
	CGDeleteRole* pkCGDeleteRole = (CGDeleteRole*)(pData);

	//EnCode usRoleIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGDeleteRole->usRoleIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDeleteRole(void* pData)
{
	CGDeleteRole* pkCGDeleteRole = (CGDeleteRole*)(pData);

	//DeCode usRoleIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGDeleteRole->usRoleIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDeleteRole);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGScriptChat(void* pData)
{
	CGScriptChat* pkCGScriptChat = (CGScriptChat*)(pData);

	//EnCode tgtType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGScriptChat->tgtType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tgtID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGScriptChat->tgtID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nOption
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGScriptChat->nOption), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGScriptChat(void* pData)
{
	CGScriptChat* pkCGScriptChat = (CGScriptChat*)(pData);

	//DeCode tgtType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGScriptChat->tgtType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tgtID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGScriptChat->tgtID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nOption
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGScriptChat->nOption), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGScriptChat);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryShop(void* pData)
{
	CGQueryShop* pkCGQueryShop = (CGQueryShop*)(pData);

	//EnCode shopID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryShop->shopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode shopPageID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryShop->shopPageID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryShop(void* pData)
{
	CGQueryShop* pkCGQueryShop = (CGQueryShop*)(pData);

	//DeCode shopID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryShop->shopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode shopPageID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryShop->shopPageID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryShop);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGShopBuyReq(void* pData)
{
	CGShopBuyReq* pkCGShopBuyReq = (CGShopBuyReq*)(pData);

	//EnCode shopID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGShopBuyReq->shopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode shopPageID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGShopBuyReq->shopPageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode shopPagePos
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGShopBuyReq->shopPagePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGShopBuyReq->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGShopBuyReq->itemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode moneyType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGShopBuyReq->moneyType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode moneyNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGShopBuyReq->moneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGShopBuyReq(void* pData)
{
	CGShopBuyReq* pkCGShopBuyReq = (CGShopBuyReq*)(pData);

	//DeCode shopID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGShopBuyReq->shopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode shopPageID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGShopBuyReq->shopPageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode shopPagePos
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGShopBuyReq->shopPagePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGShopBuyReq->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGShopBuyReq->itemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode moneyType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGShopBuyReq->moneyType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode moneyNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGShopBuyReq->moneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGShopBuyReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGItemSoldReq(void* pData)
{
	CGItemSoldReq* pkCGItemSoldReq = (CGItemSoldReq*)(pData);

	//EnCode itemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGItemSoldReq->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemPos
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGItemSoldReq->itemPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGItemSoldReq->itemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode moneyNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGItemSoldReq->moneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGItemSoldReq(void* pData)
{
	CGItemSoldReq* pkCGItemSoldReq = (CGItemSoldReq*)(pData);

	//DeCode itemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGItemSoldReq->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemPos
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGItemSoldReq->itemPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGItemSoldReq->itemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode moneyNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGItemSoldReq->moneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGItemSoldReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryTaskRecordInfo(void* pData)
{
	CGQueryTaskRecordInfo* pkCGQueryTaskRecordInfo = (CGQueryTaskRecordInfo*)(pData);

	//EnCode taskID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryTaskRecordInfo->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryTaskRecordInfo(void* pData)
{
	CGQueryTaskRecordInfo* pkCGQueryTaskRecordInfo = (CGQueryTaskRecordInfo*)(pData);

	//DeCode taskID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryTaskRecordInfo->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryTaskRecordInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDeleteTask(void* pData)
{
	CGDeleteTask* pkCGDeleteTask = (CGDeleteTask*)(pData);

	//EnCode taskID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDeleteTask->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode taskType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDeleteTask->taskType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDeleteTask(void* pData)
{
	CGDeleteTask* pkCGDeleteTask = (CGDeleteTask*)(pData);

	//DeCode taskID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDeleteTask->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode taskType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDeleteTask->taskType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDeleteTask);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGShareTask(void* pData)
{
	CGShareTask* pkCGShareTask = (CGShareTask*)(pData);

	//EnCode taskID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGShareTask->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGShareTask(void* pData)
{
	CGShareTask* pkCGShareTask = (CGShareTask*)(pData);

	//DeCode taskID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGShareTask->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGShareTask);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUpdateTaskRecordInfo(void* pData)
{
	GCUpdateTaskRecordInfo* pkGCUpdateTaskRecordInfo = (GCUpdateTaskRecordInfo*)(pData);

	//EnCode taskRecord
	if(EnCode__TaskRecordInfo(&(pkGCUpdateTaskRecordInfo->taskRecord)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUpdateTaskRecordInfo(void* pData)
{
	GCUpdateTaskRecordInfo* pkGCUpdateTaskRecordInfo = (GCUpdateTaskRecordInfo*)(pData);

	//DeCode taskRecord
	if(DeCode__TaskRecordInfo(&(pkGCUpdateTaskRecordInfo->taskRecord)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUpdateTaskRecordInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTaskHistory(void* pData)
{
	GCTaskHistory* pkGCTaskHistory = (GCTaskHistory*)(pData);

	//EnCode arrSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTaskHistory->arrSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ArrTaskHistory
	if(30 < pkGCTaskHistory->arrSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCTaskHistory->arrSize;
	if(!m_kPackage.Pack("UINT", &(pkGCTaskHistory->ArrTaskHistory), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTaskHistory(void* pData)
{
	GCTaskHistory* pkGCTaskHistory = (GCTaskHistory*)(pData);

	//DeCode arrSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTaskHistory->arrSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ArrTaskHistory
	if(30 < pkGCTaskHistory->arrSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCTaskHistory->arrSize;
	if(!m_kPackage.UnPack("UINT", &(pkGCTaskHistory->ArrTaskHistory), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTaskHistory);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDefaultRebirth(void* pData)
{
	CGDefaultRebirth* pkCGDefaultRebirth = (CGDefaultRebirth*)(pData);

	//EnCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDefaultRebirth->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDefaultRebirth(void* pData)
{
	CGDefaultRebirth* pkCGDefaultRebirth = (CGDefaultRebirth*)(pData);

	//DeCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDefaultRebirth->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDefaultRebirth);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRebirthReady(void* pData)
{
	CGRebirthReady* pkCGRebirthReady = (CGRebirthReady*)(pData);

	//EnCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRebirthReady->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRebirthReady(void* pData)
{
	CGRebirthReady* pkCGRebirthReady = (CGRebirthReady*)(pData);

	//DeCode sequenceID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRebirthReady->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRebirthReady);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPlayerCombo(void* pData)
{
	CGPlayerCombo* pkCGPlayerCombo = (CGPlayerCombo*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkCGPlayerCombo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode comboNum
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGPlayerCombo->comboNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPlayerCombo(void* pData)
{
	CGPlayerCombo* pkCGPlayerCombo = (CGPlayerCombo*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkCGPlayerCombo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode comboNum
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGPlayerCombo->comboNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPlayerCombo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerNewTask(void* pData)
{
	GCPlayerNewTask* pkGCPlayerNewTask = (GCPlayerNewTask*)(pData);

	//EnCode taskID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerNewTask->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode taskTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerNewTask->taskTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerNewTask(void* pData)
{
	GCPlayerNewTask* pkGCPlayerNewTask = (GCPlayerNewTask*)(pData);

	//DeCode taskID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerNewTask->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode taskTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerNewTask->taskTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerNewTask);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerTaskStatus(void* pData)
{
	GCPlayerTaskStatus* pkGCPlayerTaskStatus = (GCPlayerTaskStatus*)(pData);

	//EnCode taskID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerTaskStatus->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode taskStat
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerTaskStatus->taskStat), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode failTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerTaskStatus->failTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerTaskStatus(void* pData)
{
	GCPlayerTaskStatus* pkGCPlayerTaskStatus = (GCPlayerTaskStatus*)(pData);

	//DeCode taskID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerTaskStatus->taskID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode taskStat
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerTaskStatus->taskStat), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode failTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerTaskStatus->failTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerTaskStatus);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCShowShop(void* pData)
{
	GCShowShop* pkGCShowShop = (GCShowShop*)(pData);

	//EnCode shopID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShowShop->shopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode shopPageID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShowShop->shopPageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemArrSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShowShop->itemArrSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemNumArr
	if(ITEMNUM_PER_SHOPLABEL < pkGCShowShop->itemArrSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCShowShop->itemArrSize;
	if(!m_kPackage.Pack("UINT", &(pkGCShowShop->itemNumArr), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCShowShop(void* pData)
{
	GCShowShop* pkGCShowShop = (GCShowShop*)(pData);

	//DeCode shopID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShowShop->shopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode shopPageID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShowShop->shopPageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemArrSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShowShop->itemArrSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemNumArr
	if(ITEMNUM_PER_SHOPLABEL < pkGCShowShop->itemArrSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCShowShop->itemArrSize;
	if(!m_kPackage.UnPack("UINT", &(pkGCShowShop->itemNumArr), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCShowShop);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCShopBuyRst(void* pData)
{
	GCShopBuyRst* pkGCShopBuyRst = (GCShopBuyRst*)(pData);

	//EnCode shopID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->shopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode shopPageID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->shopPageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode shopPagePos
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->shopPagePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->itemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode moneyType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->moneyType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode moneyNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->moneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemPos
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->itemPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buyRst
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShopBuyRst->buyRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCShopBuyRst(void* pData)
{
	GCShopBuyRst* pkGCShopBuyRst = (GCShopBuyRst*)(pData);

	//DeCode shopID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->shopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode shopPageID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->shopPageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode shopPagePos
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->shopPagePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->itemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode moneyType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->moneyType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode moneyNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->moneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemPos
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->itemPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buyRst
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShopBuyRst->buyRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCShopBuyRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCItemSoldRst(void* pData)
{
	GCItemSoldRst* pkGCItemSoldRst = (GCItemSoldRst*)(pData);

	//EnCode itemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemSoldRst->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemPos
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemSoldRst->itemPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemSoldRst->itemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode moneyNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemSoldRst->moneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerMoneyNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemSoldRst->playerMoneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode soldRst
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemSoldRst->soldRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCItemSoldRst(void* pData)
{
	GCItemSoldRst* pkGCItemSoldRst = (GCItemSoldRst*)(pData);

	//DeCode itemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemSoldRst->itemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemPos
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemSoldRst->itemPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemSoldRst->itemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode moneyNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemSoldRst->moneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerMoneyNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemSoldRst->playerMoneyNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode soldRst
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemSoldRst->soldRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCItemSoldRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddActivePtRst(void* pData)
{
	GCAddActivePtRst* pkGCAddActivePtRst = (GCAddActivePtRst*)(pData);

	//EnCode reqRst
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddActivePtRst->reqRst), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode reqID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddActivePtRst->reqID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tgtPlayerID
	if(EnCode__PlayerID(&(pkGCAddActivePtRst->tgtPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode posX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAddActivePtRst->posX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAddActivePtRst->posY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddActivePtRst(void* pData)
{
	GCAddActivePtRst* pkGCAddActivePtRst = (GCAddActivePtRst*)(pData);

	//DeCode reqRst
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddActivePtRst->reqRst), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode reqID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddActivePtRst->reqID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tgtPlayerID
	if(DeCode__PlayerID(&(pkGCAddActivePtRst->tgtPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode posX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAddActivePtRst->posX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAddActivePtRst->posY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddActivePtRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCEvilTimeRst(void* pData)
{
	GCEvilTimeRst* pkGCEvilTimeRst = (GCEvilTimeRst*)(pData);

	//EnCode evilTime
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCEvilTimeRst->evilTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCEvilTimeRst(void* pData)
{
	GCEvilTimeRst* pkGCEvilTimeRst = (GCEvilTimeRst*)(pData);

	//DeCode evilTime
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCEvilTimeRst->evilTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCEvilTimeRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCShowPlate(void* pData)
{
	GCShowPlate* pkGCShowPlate = (GCShowPlate*)(pData);

	//EnCode plateType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShowPlate->plateType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCShowPlate(void* pData)
{
	GCShowPlate* pkGCShowPlate = (GCShowPlate*)(pData);

	//DeCode plateType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShowPlate->plateType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCShowPlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddSthToPlateRst(void* pData)
{
	GCAddSthToPlateRst* pkGCAddSthToPlateRst = (GCAddSthToPlateRst*)(pData);

	//EnCode platePos
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddSthToPlateRst->platePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sthID
	if(EnCode__ItemInfo_i(&(pkGCAddSthToPlateRst->sthID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode addRst
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddSthToPlateRst->addRst), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isConFulfil
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCAddSthToPlateRst->isConFulfil), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddSthToPlateRst(void* pData)
{
	GCAddSthToPlateRst* pkGCAddSthToPlateRst = (GCAddSthToPlateRst*)(pData);

	//DeCode platePos
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddSthToPlateRst->platePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sthID
	if(DeCode__ItemInfo_i(&(pkGCAddSthToPlateRst->sthID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode addRst
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddSthToPlateRst->addRst), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isConFulfil
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCAddSthToPlateRst->isConFulfil), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddSthToPlateRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTakeSthFromPlateRst(void* pData)
{
	GCTakeSthFromPlateRst* pkGCTakeSthFromPlateRst = (GCTakeSthFromPlateRst*)(pData);

	//EnCode platePos
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTakeSthFromPlateRst->platePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sthID
	if(EnCode__ItemInfo_i(&(pkGCTakeSthFromPlateRst->sthID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode takeRst
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTakeSthFromPlateRst->takeRst), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isConFulfil
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCTakeSthFromPlateRst->isConFulfil), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTakeSthFromPlateRst(void* pData)
{
	GCTakeSthFromPlateRst* pkGCTakeSthFromPlateRst = (GCTakeSthFromPlateRst*)(pData);

	//DeCode platePos
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTakeSthFromPlateRst->platePos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sthID
	if(DeCode__ItemInfo_i(&(pkGCTakeSthFromPlateRst->sthID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode takeRst
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTakeSthFromPlateRst->takeRst), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isConFulfil
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCTakeSthFromPlateRst->isConFulfil), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTakeSthFromPlateRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlateExecRst(void* pData)
{
	GCPlateExecRst* pkGCPlateExecRst = (GCPlateExecRst*)(pData);

	//EnCode execRst
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlateExecRst->execRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlateExecRst(void* pData)
{
	GCPlateExecRst* pkGCPlateExecRst = (GCPlateExecRst*)(pData);

	//DeCode execRst
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlateExecRst->execRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlateExecRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__ProtectItem(void* pData)
{
	ProtectItem* pkProtectItem = (ProtectItem*)(pData);

	//EnCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkProtectItem->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode protectType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkProtectItem->protectType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode protectTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkProtectItem->protectTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__ProtectItem(void* pData)
{
	ProtectItem* pkProtectItem = (ProtectItem*)(pData);

	//DeCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkProtectItem->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode protectType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkProtectItem->protectType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode protectTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkProtectItem->protectTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(ProtectItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCShowProtectItemPlate(void* pData)
{
	GCShowProtectItemPlate* pkGCShowProtectItemPlate = (GCShowProtectItemPlate*)(pData);

	//EnCode plateType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShowProtectItemPlate->plateType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCShowProtectItemPlate(void* pData)
{
	GCShowProtectItemPlate* pkGCShowProtectItemPlate = (GCShowProtectItemPlate*)(pData);

	//DeCode plateType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShowProtectItemPlate->plateType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCShowProtectItemPlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPutItemToProtectPlate(void* pData)
{
	CGPutItemToProtectPlate* pkCGPutItemToProtectPlate = (CGPutItemToProtectPlate*)(pData);

	//EnCode protectItem
	if(EnCode__ItemInfo_i(&(pkCGPutItemToProtectPlate->protectItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPutItemToProtectPlate(void* pData)
{
	CGPutItemToProtectPlate* pkCGPutItemToProtectPlate = (CGPutItemToProtectPlate*)(pData);

	//DeCode protectItem
	if(DeCode__ItemInfo_i(&(pkCGPutItemToProtectPlate->protectItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPutItemToProtectPlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPutItemToProtectPlateRst(void* pData)
{
	GCPutItemToProtectPlateRst* pkGCPutItemToProtectPlateRst = (GCPutItemToProtectPlateRst*)(pData);

	//EnCode addItemRst
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPutItemToProtectPlateRst->addItemRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPutItemToProtectPlateRst(void* pData)
{
	GCPutItemToProtectPlateRst* pkGCPutItemToProtectPlateRst = (GCPutItemToProtectPlateRst*)(pData);

	//DeCode addItemRst
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPutItemToProtectPlateRst->addItemRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPutItemToProtectPlateRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGTakeItemFromProtectPlate(void* pData)
{
	CGTakeItemFromProtectPlate* pkCGTakeItemFromProtectPlate = (CGTakeItemFromProtectPlate*)(pData);

	//EnCode takeItemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGTakeItemFromProtectPlate->takeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGTakeItemFromProtectPlate(void* pData)
{
	CGTakeItemFromProtectPlate* pkCGTakeItemFromProtectPlate = (CGTakeItemFromProtectPlate*)(pData);

	//DeCode takeItemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGTakeItemFromProtectPlate->takeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGTakeItemFromProtectPlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTaskItemFromProtectPlateRst(void* pData)
{
	GCTaskItemFromProtectPlateRst* pkGCTaskItemFromProtectPlateRst = (GCTaskItemFromProtectPlateRst*)(pData);

	//EnCode takeItemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTaskItemFromProtectPlateRst->takeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode takeRst
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCTaskItemFromProtectPlateRst->takeRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTaskItemFromProtectPlateRst(void* pData)
{
	GCTaskItemFromProtectPlateRst* pkGCTaskItemFromProtectPlateRst = (GCTaskItemFromProtectPlateRst*)(pData);

	//DeCode takeItemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTaskItemFromProtectPlateRst->takeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode takeRst
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCTaskItemFromProtectPlateRst->takeRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTaskItemFromProtectPlateRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGConfirmProtectPlate(void* pData)
{
	CGConfirmProtectPlate* pkCGConfirmProtectPlate = (CGConfirmProtectPlate*)(pData);

	//EnCode confirmPlate
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGConfirmProtectPlate->confirmPlate), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGConfirmProtectPlate(void* pData)
{
	CGConfirmProtectPlate* pkCGConfirmProtectPlate = (CGConfirmProtectPlate*)(pData);

	//DeCode confirmPlate
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGConfirmProtectPlate->confirmPlate), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGConfirmProtectPlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCConfirmProtectPlateRst(void* pData)
{
	GCConfirmProtectPlateRst* pkGCConfirmProtectPlateRst = (GCConfirmProtectPlateRst*)(pData);

	//EnCode confirmType
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCConfirmProtectPlateRst->confirmType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode confirmRst
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCConfirmProtectPlateRst->confirmRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCConfirmProtectPlateRst(void* pData)
{
	GCConfirmProtectPlateRst* pkGCConfirmProtectPlateRst = (GCConfirmProtectPlateRst*)(pData);

	//DeCode confirmType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCConfirmProtectPlateRst->confirmType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode confirmRst
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCConfirmProtectPlateRst->confirmRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCConfirmProtectPlateRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSetItemProtectProp(void* pData)
{
	GCSetItemProtectProp* pkGCSetItemProtectProp = (GCSetItemProtectProp*)(pData);

	//EnCode protectNewProp
	if(EnCode__ProtectItem(&(pkGCSetItemProtectProp->protectNewProp)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode opType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSetItemProtectProp->opType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSetItemProtectProp(void* pData)
{
	GCSetItemProtectProp* pkGCSetItemProtectProp = (GCSetItemProtectProp*)(pData);

	//DeCode protectNewProp
	if(DeCode__ProtectItem(&(pkGCSetItemProtectProp->protectNewProp)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode opType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSetItemProtectProp->opType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSetItemProtectProp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRecvItemWithProtectProp(void* pData)
{
	GCRecvItemWithProtectProp* pkGCRecvItemWithProtectProp = (GCRecvItemWithProtectProp*)(pData);

	//EnCode protectItemCount
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCRecvItemWithProtectProp->protectItemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode protectItemArr
	if(PKG_PAGE_SIZE < pkGCRecvItemWithProtectProp->protectItemCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCRecvItemWithProtectProp->protectItemCount; ++i)
	{
		if(EnCode__ProtectItem(&(pkGCRecvItemWithProtectProp->protectItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRecvItemWithProtectProp(void* pData)
{
	GCRecvItemWithProtectProp* pkGCRecvItemWithProtectProp = (GCRecvItemWithProtectProp*)(pData);

	//DeCode protectItemCount
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCRecvItemWithProtectProp->protectItemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode protectItemArr
	if(PKG_PAGE_SIZE < pkGCRecvItemWithProtectProp->protectItemCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCRecvItemWithProtectProp->protectItemCount; ++i)
	{
		if(DeCode__ProtectItem(&(pkGCRecvItemWithProtectProp->protectItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCRecvItemWithProtectProp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGReleaseSpecialProtect(void* pData)
{
	CGReleaseSpecialProtect* pkCGReleaseSpecialProtect = (CGReleaseSpecialProtect*)(pData);

	//EnCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGReleaseSpecialProtect->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGReleaseSpecialProtect(void* pData)
{
	CGReleaseSpecialProtect* pkCGReleaseSpecialProtect = (CGReleaseSpecialProtect*)(pData);

	//DeCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGReleaseSpecialProtect->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGReleaseSpecialProtect);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGCancelSpecialProtect(void* pData)
{
	CGCancelSpecialProtect* pkCGCancelSpecialProtect = (CGCancelSpecialProtect*)(pData);

	//EnCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCancelSpecialProtect->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGCancelSpecialProtect(void* pData)
{
	CGCancelSpecialProtect* pkCGCancelSpecialProtect = (CGCancelSpecialProtect*)(pData);

	//DeCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCancelSpecialProtect->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGCancelSpecialProtect);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCReleaseSpecialProtectRst(void* pData)
{
	GCReleaseSpecialProtectRst* pkGCReleaseSpecialProtectRst = (GCReleaseSpecialProtectRst*)(pData);

	//EnCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReleaseSpecialProtectRst->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode opRst
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReleaseSpecialProtectRst->opRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCReleaseSpecialProtectRst(void* pData)
{
	GCReleaseSpecialProtectRst* pkGCReleaseSpecialProtectRst = (GCReleaseSpecialProtectRst*)(pData);

	//DeCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReleaseSpecialProtectRst->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode opRst
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReleaseSpecialProtectRst->opRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCReleaseSpecialProtectRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCCancelSpecialProtectRst(void* pData)
{
	GCCancelSpecialProtectRst* pkGCCancelSpecialProtectRst = (GCCancelSpecialProtectRst*)(pData);

	//EnCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCCancelSpecialProtectRst->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode opRst
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCCancelSpecialProtectRst->opRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCCancelSpecialProtectRst(void* pData)
{
	GCCancelSpecialProtectRst* pkGCCancelSpecialProtectRst = (GCCancelSpecialProtectRst*)(pData);

	//DeCode protectItemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCCancelSpecialProtectRst->protectItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode opRst
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCCancelSpecialProtectRst->opRst), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCCancelSpecialProtectRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCClosePlate(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCClosePlate(void* pData)
{
	(pData);
	return sizeof(GCClosePlate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCNetDetect(void* pData)
{
	GCNetDetect* pkGCNetDetect = (GCNetDetect*)(pData);

	//EnCode pkgID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCNetDetect->pkgID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCNetDetect(void* pData)
{
	GCNetDetect* pkGCNetDetect = (GCNetDetect*)(pData);

	//DeCode pkgID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCNetDetect->pkgID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCNetDetect);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCNetBias(void* pData)
{
	GCNetBias* pkGCNetBias = (GCNetBias*)(pData);

	//EnCode pkgID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCNetBias->pkgID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCNetBias(void* pData)
{
	GCNetBias* pkGCNetBias = (GCNetBias*)(pData);

	//DeCode pkgID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCNetBias->pkgID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCNetBias);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChat(void* pData)
{
	GCChat* pkGCChat = (GCChat*)(pData);

	//EnCode nType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChat->nType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sourcePlayerID
	if(EnCode__PlayerID(&(pkGCChat->sourcePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode srcNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChat->srcNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode srcName
	if(MAX_NAME_SIZE < pkGCChat->srcNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChat->srcNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCChat->srcName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChat->chatSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode strChat
	if(MAX_CHAT_SIZE < pkGCChat->chatSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChat->chatSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCChat->strChat), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChat(void* pData)
{
	GCChat* pkGCChat = (GCChat*)(pData);

	//DeCode nType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChat->nType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sourcePlayerID
	if(DeCode__PlayerID(&(pkGCChat->sourcePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode srcNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChat->srcNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode srcName
	if(MAX_NAME_SIZE < pkGCChat->srcNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChat->srcNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCChat->srcName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChat->chatSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode strChat
	if(MAX_CHAT_SIZE < pkGCChat->chatSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChat->chatSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCChat->strChat), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChat);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRoleattUpdate(void* pData)
{
	GCRoleattUpdate* pkGCRoleattUpdate = (GCRoleattUpdate*)(pData);

	//EnCode lChangedPlayerID
	if(EnCode__PlayerID(&(pkGCRoleattUpdate->lChangedPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRoleattUpdate->nType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nOldValue
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRoleattUpdate->nOldValue), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nNewValue
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRoleattUpdate->nNewValue), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode changeTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRoleattUpdate->changeTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAddType
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRoleattUpdate->nAddType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bFloat
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCRoleattUpdate->bFloat), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRoleattUpdate(void* pData)
{
	GCRoleattUpdate* pkGCRoleattUpdate = (GCRoleattUpdate*)(pData);

	//DeCode lChangedPlayerID
	if(DeCode__PlayerID(&(pkGCRoleattUpdate->lChangedPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRoleattUpdate->nType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nOldValue
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRoleattUpdate->nOldValue), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nNewValue
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRoleattUpdate->nNewValue), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode changeTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRoleattUpdate->changeTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAddType
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRoleattUpdate->nAddType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bFloat
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCRoleattUpdate->bFloat), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRoleattUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerLevelUp(void* pData)
{
	GCPlayerLevelUp* pkGCPlayerLevelUp = (GCPlayerLevelUp*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCPlayerLevelUp->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nNewLevel
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerLevelUp->nNewLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode levelUpType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerLevelUp->levelUpType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerLevelUp(void* pData)
{
	GCPlayerLevelUp* pkGCPlayerLevelUp = (GCPlayerLevelUp*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCPlayerLevelUp->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nNewLevel
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerLevelUp->nNewLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode levelUpType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerLevelUp->levelUpType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerLevelUp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTmpInfoUpdate(void* pData)
{
	GCTmpInfoUpdate* pkGCTmpInfoUpdate = (GCTmpInfoUpdate*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCTmpInfoUpdate->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode tmpInfo
	if(EnCode__PlayerTmpInfo(&(pkGCTmpInfoUpdate->tmpInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTmpInfoUpdate(void* pData)
{
	GCTmpInfoUpdate* pkGCTmpInfoUpdate = (GCTmpInfoUpdate*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCTmpInfoUpdate->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode tmpInfo
	if(DeCode__PlayerTmpInfo(&(pkGCTmpInfoUpdate->tmpInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTmpInfoUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerPkg(void* pData)
{
	GCPlayerPkg* pkGCPlayerPkg = (GCPlayerPkg*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCPlayerPkg->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ucIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerPkg->ucIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pkgSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerPkg->pkgSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerPkg
	if(PKG_GROUP_NUM < pkGCPlayerPkg->pkgSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPlayerPkg->pkgSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCPlayerPkg->playerPkg), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerPkg(void* pData)
{
	GCPlayerPkg* pkGCPlayerPkg = (GCPlayerPkg*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCPlayerPkg->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ucIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerPkg->ucIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pkgSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerPkg->pkgSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerPkg
	if(PKG_GROUP_NUM < pkGCPlayerPkg->pkgSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPlayerPkg->pkgSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCPlayerPkg->playerPkg), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerPkg);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTimeCheck(void* pData)
{
	GCTimeCheck* pkGCTimeCheck = (GCTimeCheck*)(pData);

	//EnCode checkID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTimeCheck->checkID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTimeCheck->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTimeCheck(void* pData)
{
	GCTimeCheck* pkGCTimeCheck = (GCTimeCheck*)(pData);

	//DeCode checkID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTimeCheck->checkID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTimeCheck->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTimeCheck);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRun(void* pData)
{
	GCRun* pkGCRun = (GCRun*)(pData);

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkGCRun->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRun->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fOrgWorldPosX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCRun->fOrgWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fOrgWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCRun->fOrgWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fSpeed
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCRun->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fNewWorldPosX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCRun->fNewWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fNewWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCRun->fNewWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRun->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRun(void* pData)
{
	GCRun* pkGCRun = (GCRun*)(pData);

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkGCRun->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRun->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fOrgWorldPosX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCRun->fOrgWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fOrgWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCRun->fOrgWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCRun->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fNewWorldPosX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCRun->fNewWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fNewWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCRun->fNewWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRun->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRun);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerPosInfo(void* pData)
{
	GCPlayerPosInfo* pkGCPlayerPosInfo = (GCPlayerPosInfo*)(pData);

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkGCPlayerPosInfo->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode lMapCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerPosInfo->lMapCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMapPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPlayerPosInfo->nMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMapPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPlayerPosInfo->nMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fWorldPosX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPlayerPosInfo->fWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPlayerPosInfo->fWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerPosInfo(void* pData)
{
	GCPlayerPosInfo* pkGCPlayerPosInfo = (GCPlayerPosInfo*)(pData);

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkGCPlayerPosInfo->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode lMapCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerPosInfo->lMapCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMapPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPlayerPosInfo->nMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMapPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPlayerPosInfo->nMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fWorldPosX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPlayerPosInfo->fWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPlayerPosInfo->fWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerPosInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerSelfInfo(void* pData)
{
	GCPlayerSelfInfo* pkGCPlayerSelfInfo = (GCPlayerSelfInfo*)(pData);

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkGCPlayerSelfInfo->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerSelfInfo(void* pData)
{
	GCPlayerSelfInfo* pkGCPlayerSelfInfo = (GCPlayerSelfInfo*)(pData);

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkGCPlayerSelfInfo->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerSelfInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerAppear(void* pData)
{
	GCPlayerAppear* pkGCPlayerAppear = (GCPlayerAppear*)(pData);

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkGCPlayerAppear->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode playerInfo
	if(EnCode__PlayerInfo_1_Base(&(pkGCPlayerAppear->playerInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode playerSkills
	if(EnCode__PlayerInfo_4_Skills(&(pkGCPlayerAppear->playerSkills)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode appearType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAppear->appearType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAppear->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerAppear(void* pData)
{
	GCPlayerAppear* pkGCPlayerAppear = (GCPlayerAppear*)(pData);

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkGCPlayerAppear->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode playerInfo
	if(DeCode__PlayerInfo_1_Base(&(pkGCPlayerAppear->playerInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode playerSkills
	if(DeCode__PlayerInfo_4_Skills(&(pkGCPlayerAppear->playerSkills)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode appearType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAppear->appearType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAppear->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerAppear);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerLeave(void* pData)
{
	GCPlayerLeave* pkGCPlayerLeave = (GCPlayerLeave*)(pData);

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkGCPlayerLeave->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode lMapCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerLeave->lMapCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerLeave(void* pData)
{
	GCPlayerLeave* pkGCPlayerLeave = (GCPlayerLeave*)(pData);

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkGCPlayerLeave->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode lMapCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerLeave->lMapCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerLeave);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRandomString(void* pData)
{
	GCRandomString* pkGCRandomString = (GCRandomString*)(pData);

	//EnCode rdStrSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRandomString->rdStrSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode randomString
	if(MD5_RDSTR_LEN < pkGCRandomString->rdStrSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRandomString->rdStrSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCRandomString->randomString), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRandomString->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRandomString(void* pData)
{
	GCRandomString* pkGCRandomString = (GCRandomString*)(pData);

	//DeCode rdStrSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRandomString->rdStrSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode randomString
	if(MD5_RDSTR_LEN < pkGCRandomString->rdStrSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRandomString->rdStrSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRandomString->randomString), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRandomString->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRandomString);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerLogin(void* pData)
{
	GCPlayerLogin* pkGCPlayerLogin = (GCPlayerLogin*)(pData);

	//EnCode byIsLoginOK
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerLogin->byIsLoginOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerLogin->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerLogin(void* pData)
{
	GCPlayerLogin* pkGCPlayerLogin = (GCPlayerLogin*)(pData);

	//DeCode byIsLoginOK
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerLogin->byIsLoginOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerLogin->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerLogin);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerRole(void* pData)
{
	GCPlayerRole* pkGCPlayerRole = (GCPlayerRole*)(pData);

	//EnCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerRole->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode byFlag
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerRole->byFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerInfo
	if(EnCode__PlayerInfoLogin(&(pkGCPlayerRole->playerInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fashion
	if(EnCode__PlayerInfo_Fashions(&(pkGCPlayerRole->fashion)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode tag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerRole->tag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerRole(void* pData)
{
	GCPlayerRole* pkGCPlayerRole = (GCPlayerRole*)(pData);

	//DeCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerRole->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode byFlag
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerRole->byFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerInfo
	if(DeCode__PlayerInfoLogin(&(pkGCPlayerRole->playerInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fashion
	if(DeCode__PlayerInfo_Fashions(&(pkGCPlayerRole->fashion)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode tag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerRole->tag), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerRole);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCOtherPlayerInfo(void* pData)
{
	GCOtherPlayerInfo* pkGCOtherPlayerInfo = (GCOtherPlayerInfo*)(pData);

	//EnCode otherPlayerID
	if(EnCode__PlayerID(&(pkGCOtherPlayerInfo->otherPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCOtherPlayerInfo->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerInfo
	if(EnCode__PlayerInfoLogin(&(pkGCOtherPlayerInfo->playerInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nCurHp
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCOtherPlayerInfo->nCurHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMaxHp
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCOtherPlayerInfo->nMaxHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fSpeed
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCOtherPlayerInfo->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fTargetX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCOtherPlayerInfo->fTargetX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fTargetY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCOtherPlayerInfo->fTargetY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nCurMagic
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCOtherPlayerInfo->nCurMagic), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMaxMagic
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCOtherPlayerInfo->nMaxMagic), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isInTeam
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCOtherPlayerInfo->isInTeam), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isTeamCaptain
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCOtherPlayerInfo->isTeamCaptain), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sGoodEvilPoint
	unCount = 1;
	if(!m_kPackage.Pack("SHORT", &(pkGCOtherPlayerInfo->sGoodEvilPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bRogue
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCOtherPlayerInfo->bRogue), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bWeak
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCOtherPlayerInfo->bWeak), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bFashionState
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCOtherPlayerInfo->bFashionState), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Anamorphic
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCOtherPlayerInfo->Anamorphic), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCOtherPlayerInfo(void* pData)
{
	GCOtherPlayerInfo* pkGCOtherPlayerInfo = (GCOtherPlayerInfo*)(pData);

	//DeCode otherPlayerID
	if(DeCode__PlayerID(&(pkGCOtherPlayerInfo->otherPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCOtherPlayerInfo->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerInfo
	if(DeCode__PlayerInfoLogin(&(pkGCOtherPlayerInfo->playerInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nCurHp
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCOtherPlayerInfo->nCurHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMaxHp
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCOtherPlayerInfo->nMaxHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCOtherPlayerInfo->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fTargetX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCOtherPlayerInfo->fTargetX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fTargetY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCOtherPlayerInfo->fTargetY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nCurMagic
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCOtherPlayerInfo->nCurMagic), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMaxMagic
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCOtherPlayerInfo->nMaxMagic), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isInTeam
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCOtherPlayerInfo->isInTeam), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isTeamCaptain
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCOtherPlayerInfo->isTeamCaptain), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sGoodEvilPoint
	unCount = 1;
	if(!m_kPackage.UnPack("SHORT", &(pkGCOtherPlayerInfo->sGoodEvilPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bRogue
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCOtherPlayerInfo->bRogue), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bWeak
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCOtherPlayerInfo->bWeak), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bFashionState
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCOtherPlayerInfo->bFashionState), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Anamorphic
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCOtherPlayerInfo->Anamorphic), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCOtherPlayerInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSelectRole(void* pData)
{
	GCSelectRole* pkGCSelectRole = (GCSelectRole*)(pData);

	//EnCode byIsSelectOK
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCSelectRole->byIsSelectOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiRoleNo
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCSelectRole->uiRoleNo), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSelectRole->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSelectRole(void* pData)
{
	GCSelectRole* pkGCSelectRole = (GCSelectRole*)(pData);

	//DeCode byIsSelectOK
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCSelectRole->byIsSelectOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiRoleNo
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCSelectRole->uiRoleNo), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSelectRole->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSelectRole);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCCreateRole(void* pData)
{
	GCCreateRole* pkGCCreateRole = (GCCreateRole*)(pData);

	//EnCode byIsOK
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCCreateRole->byIsOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode wRoleNo
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCCreateRole->wRoleNo), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCCreateRole->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equipInfo
	if(EnCode__PlayerInfo_2_Equips(&(pkGCCreateRole->equipInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCCreateRole(void* pData)
{
	GCCreateRole* pkGCCreateRole = (GCCreateRole*)(pData);

	//DeCode byIsOK
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCCreateRole->byIsOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode wRoleNo
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCCreateRole->wRoleNo), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCCreateRole->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equipInfo
	if(DeCode__PlayerInfo_2_Equips(&(pkGCCreateRole->equipInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCCreateRole);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDeleteRole(void* pData)
{
	GCDeleteRole* pkGCDeleteRole = (GCDeleteRole*)(pData);

	//EnCode byIsOK
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDeleteRole->byIsOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode wRoleNo
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDeleteRole->wRoleNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDeleteRole(void* pData)
{
	GCDeleteRole* pkGCDeleteRole = (GCDeleteRole*)(pData);

	//DeCode byIsOK
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDeleteRole->byIsOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode wRoleNo
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDeleteRole->wRoleNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDeleteRole);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerDying(void* pData)
{
	GCPlayerDying* pkGCPlayerDying = (GCPlayerDying*)(pData);

	//EnCode dyingPlayerID
	if(EnCode__PlayerID(&(pkGCPlayerDying->dyingPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode countDownSec
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerDying->countDownSec), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerDying(void* pData)
{
	GCPlayerDying* pkGCPlayerDying = (GCPlayerDying*)(pData);

	//DeCode dyingPlayerID
	if(DeCode__PlayerID(&(pkGCPlayerDying->dyingPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode countDownSec
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerDying->countDownSec), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerDying);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRebirthInfo(void* pData)
{
	GCRebirthInfo* pkGCRebirthInfo = (GCRebirthInfo*)(pData);

	//EnCode dyingPlayerID
	if(EnCode__PlayerID(&(pkGCRebirthInfo->dyingPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mapID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRebirthInfo->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCRebirthInfo->nPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCRebirthInfo->nPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRebirthInfo->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRebirthInfo(void* pData)
{
	GCRebirthInfo* pkGCRebirthInfo = (GCRebirthInfo*)(pData);

	//DeCode dyingPlayerID
	if(DeCode__PlayerID(&(pkGCRebirthInfo->dyingPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mapID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRebirthInfo->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCRebirthInfo->nPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCRebirthInfo->nPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRebirthInfo->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRebirthInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMonsterBorn(void* pData)
{
	GCMonsterBorn* pkGCMonsterBorn = (GCMonsterBorn*)(pData);

	//EnCode lMonsterTypeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterBorn->lMonsterTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMonsterID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterBorn->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMapPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterBorn->nMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMapPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterBorn->nMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMonsterBorn(void* pData)
{
	GCMonsterBorn* pkGCMonsterBorn = (GCMonsterBorn*)(pData);

	//DeCode lMonsterTypeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterBorn->lMonsterTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMonsterID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterBorn->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMapPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterBorn->nMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMapPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterBorn->nMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMonsterBorn);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMonsterMove(void* pData)
{
	GCMonsterMove* pkGCMonsterMove = (GCMonsterMove*)(pData);

	//EnCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterMove->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nOrgMapPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterMove->nOrgMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nOrgMapPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterMove->nOrgMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nNewMapPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterMove->nNewMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nNewMapPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterMove->nNewMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nTargetMapPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterMove->nTargetMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nTargetMapPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterMove->nTargetMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fSpeed
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCMonsterMove->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMonsterMove(void* pData)
{
	GCMonsterMove* pkGCMonsterMove = (GCMonsterMove*)(pData);

	//DeCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterMove->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nOrgMapPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterMove->nOrgMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nOrgMapPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterMove->nOrgMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nNewMapPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterMove->nNewMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nNewMapPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterMove->nNewMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nTargetMapPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterMove->nTargetMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nTargetMapPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterMove->nTargetMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCMonsterMove->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMonsterMove);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMonsterDisappear(void* pData)
{
	GCMonsterDisappear* pkGCMonsterDisappear = (GCMonsterDisappear*)(pData);

	//EnCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterDisappear->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode disType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterDisappear->disType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMonsterDisappear(void* pData)
{
	GCMonsterDisappear* pkGCMonsterDisappear = (GCMonsterDisappear*)(pData);

	//DeCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterDisappear->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode disType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterDisappear->disType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMonsterDisappear);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerAttackTarget(void* pData)
{
	GCPlayerAttackTarget* pkGCPlayerAttackTarget = (GCPlayerAttackTarget*)(pData);

	//EnCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode scopeFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->scopeFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkGCPlayerAttackTarget->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode battleFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lCurseTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->lCurseTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lCooldownTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->lCooldownTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode consumeItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->consumeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetID
	if(EnCode__PlayerID(&(pkGCPlayerAttackTarget->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode lHpSubed
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->lHpSubed), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lHpLeft
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->lHpLeft), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerHP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->nAtkPlayerHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerMP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->nAtkPlayerMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetRewardID
	if(EnCode__PlayerID(&(pkGCPlayerAttackTarget->targetRewardID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode aoeMainTargetID
	if(EnCode__PlayerID(&(pkGCPlayerAttackTarget->aoeMainTargetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode aoeAttackPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPlayerAttackTarget->aoeAttackPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode aoeAttackPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPlayerAttackTarget->aoeAttackPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackTarget->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerAttackTarget(void* pData)
{
	GCPlayerAttackTarget* pkGCPlayerAttackTarget = (GCPlayerAttackTarget*)(pData);

	//DeCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode scopeFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->scopeFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkGCPlayerAttackTarget->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode battleFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lCurseTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->lCurseTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lCooldownTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->lCooldownTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode consumeItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->consumeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetID
	if(DeCode__PlayerID(&(pkGCPlayerAttackTarget->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode lHpSubed
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->lHpSubed), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lHpLeft
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->lHpLeft), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerHP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->nAtkPlayerHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerMP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->nAtkPlayerMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetRewardID
	if(DeCode__PlayerID(&(pkGCPlayerAttackTarget->targetRewardID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode aoeMainTargetID
	if(DeCode__PlayerID(&(pkGCPlayerAttackTarget->aoeMainTargetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode aoeAttackPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPlayerAttackTarget->aoeAttackPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode aoeAttackPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPlayerAttackTarget->aoeAttackPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackTarget->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerAttackTarget);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMonsterAttackPlayer(void* pData)
{
	GCMonsterAttackPlayer* pkGCMonsterAttackPlayer = (GCMonsterAttackPlayer*)(pData);

	//EnCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMonsterAttackPlayer->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkGCMonsterAttackPlayer->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode scopeFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterAttackPlayer->scopeFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode battleFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterAttackPlayer->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMonsterID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterAttackPlayer->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMonsterType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterAttackPlayer->lMonsterType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMapPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterAttackPlayer->nMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMapPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterAttackPlayer->nMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nSkillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterAttackPlayer->nSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nCurseTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterAttackPlayer->nCurseTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nDamage
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterAttackPlayer->nDamage), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nLeftHp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterAttackPlayer->nLeftHp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMonsterAttackPlayer(void* pData)
{
	GCMonsterAttackPlayer* pkGCMonsterAttackPlayer = (GCMonsterAttackPlayer*)(pData);

	//DeCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMonsterAttackPlayer->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkGCMonsterAttackPlayer->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode scopeFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterAttackPlayer->scopeFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode battleFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterAttackPlayer->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMonsterID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterAttackPlayer->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMonsterType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterAttackPlayer->lMonsterType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMapPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterAttackPlayer->nMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMapPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterAttackPlayer->nMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nSkillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterAttackPlayer->nSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nCurseTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterAttackPlayer->nCurseTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nDamage
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterAttackPlayer->nDamage), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nLeftHp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterAttackPlayer->nLeftHp), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMonsterAttackPlayer);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSwitchMap(void* pData)
{
	GCSwitchMap* pkGCSwitchMap = (GCSwitchMap*)(pData);

	//EnCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSwitchMap->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCSwitchMap->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode usMapID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSwitchMap->usMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode iPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCSwitchMap->iPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode iPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCSwitchMap->iPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSwitchMap->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSwitchMap(void* pData)
{
	GCSwitchMap* pkGCSwitchMap = (GCSwitchMap*)(pData);

	//DeCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSwitchMap->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCSwitchMap->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode usMapID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSwitchMap->usMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode iPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCSwitchMap->iPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode iPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCSwitchMap->iPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSwitchMap->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSwitchMap);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerAttackPlayer(void* pData)
{
	GCPlayerAttackPlayer* pkGCPlayerAttackPlayer = (GCPlayerAttackPlayer*)(pData);

	//EnCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackPlayer->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode scopeFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackPlayer->scopeFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode hitFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackPlayer->hitFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode criticalFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackPlayer->criticalFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attackerID
	if(EnCode__PlayerID(&(pkGCPlayerAttackPlayer->attackerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode targetID
	if(EnCode__PlayerID(&(pkGCPlayerAttackPlayer->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nSkillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackPlayer->nSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attackerPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPlayerAttackPlayer->attackerPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode attackerPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPlayerAttackPlayer->attackerPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPlayerAttackPlayer->targetPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPlayerAttackPlayer->targetPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nDamage
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackPlayer->nDamage), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nLeftHp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAttackPlayer->nLeftHp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerAttackPlayer(void* pData)
{
	GCPlayerAttackPlayer* pkGCPlayerAttackPlayer = (GCPlayerAttackPlayer*)(pData);

	//DeCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackPlayer->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode scopeFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackPlayer->scopeFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode hitFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackPlayer->hitFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode criticalFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackPlayer->criticalFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attackerID
	if(DeCode__PlayerID(&(pkGCPlayerAttackPlayer->attackerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode targetID
	if(DeCode__PlayerID(&(pkGCPlayerAttackPlayer->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nSkillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackPlayer->nSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attackerPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPlayerAttackPlayer->attackerPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode attackerPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPlayerAttackPlayer->attackerPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPlayerAttackPlayer->targetPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPlayerAttackPlayer->targetPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nDamage
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackPlayer->nDamage), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nLeftHp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAttackPlayer->nLeftHp), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerAttackPlayer);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMonsterInfo(void* pData)
{
	GCMonsterInfo* pkGCMonsterInfo = (GCMonsterInfo*)(pData);

	//EnCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterInfo->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMonsterType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterInfo->lMonsterType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMapPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterInfo->nMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMapPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCMonsterInfo->nMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fSpeed
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCMonsterInfo->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fTargetX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCMonsterInfo->fTargetX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fTargetY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCMonsterInfo->fTargetY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lMonsterHp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterInfo->lMonsterHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rewardID
	if(EnCode__PlayerID(&(pkGCMonsterInfo->rewardID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMonsterInfo(void* pData)
{
	GCMonsterInfo* pkGCMonsterInfo = (GCMonsterInfo*)(pData);

	//DeCode lMonsterID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterInfo->lMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMonsterType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterInfo->lMonsterType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMapPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterInfo->nMapPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMapPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCMonsterInfo->nMapPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCMonsterInfo->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fTargetX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCMonsterInfo->fTargetX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fTargetY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCMonsterInfo->fTargetY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lMonsterHp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterInfo->lMonsterHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rewardID
	if(DeCode__PlayerID(&(pkGCMonsterInfo->rewardID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMonsterInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerCombo(void* pData)
{
	GCPlayerCombo* pkGCPlayerCombo = (GCPlayerCombo*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCPlayerCombo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode comboNum
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerCombo->comboNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerCombo(void* pData)
{
	GCPlayerCombo* pkGCPlayerCombo = (GCPlayerCombo*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCPlayerCombo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode comboNum
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerCombo->comboNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerCombo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSrvError(void* pData)
{
	GCSrvError* pkGCSrvError = (GCSrvError*)(pData);

	//EnCode errNO
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCSrvError->errNO), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSrvError(void* pData)
{
	GCSrvError* pkGCSrvError = (GCSrvError*)(pData);

	//DeCode errNO
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCSrvError->errNO), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSrvError);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCItemPackageAppear(void* pData)
{
	GCItemPackageAppear* pkGCItemPackageAppear = (GCItemPackageAppear*)(pData);

	//EnCode PackageType
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCItemPackageAppear->PackageType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemPackageID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemPackageAppear->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode x
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCItemPackageAppear->x), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode y
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCItemPackageAppear->y), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dropOwnerID
	if(EnCode__PlayerID(&(pkGCItemPackageAppear->dropOwnerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCItemPackageAppear(void* pData)
{
	GCItemPackageAppear* pkGCItemPackageAppear = (GCItemPackageAppear*)(pData);

	//DeCode PackageType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCItemPackageAppear->PackageType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemPackageID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemPackageAppear->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode x
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCItemPackageAppear->x), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode y
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCItemPackageAppear->y), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dropOwnerID
	if(DeCode__PlayerID(&(pkGCItemPackageAppear->dropOwnerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCItemPackageAppear);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCItemsPkgDisappear(void* pData)
{
	GCItemsPkgDisappear* pkGCItemsPkgDisappear = (GCItemsPkgDisappear*)(pData);

	//EnCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemsPkgDisappear->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCItemsPkgDisappear(void* pData)
{
	GCItemsPkgDisappear* pkGCItemsPkgDisappear = (GCItemsPkgDisappear*)(pData);

	//DeCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemsPkgDisappear->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCItemsPkgDisappear);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryItemPackageInfo(void* pData)
{
	CGQueryItemPackageInfo* pkCGQueryItemPackageInfo = (CGQueryItemPackageInfo*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkCGQueryItemPackageInfo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryItemPackageInfo->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode queryType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryItemPackageInfo->queryType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryItemPackageInfo(void* pData)
{
	CGQueryItemPackageInfo* pkCGQueryItemPackageInfo = (CGQueryItemPackageInfo*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkCGQueryItemPackageInfo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryItemPackageInfo->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode queryType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryItemPackageInfo->queryType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryItemPackageInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCItemPackageInfo(void* pData)
{
	GCItemPackageInfo* pkGCItemPackageInfo = (GCItemPackageInfo*)(pData);

	//EnCode pkgIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCItemPackageInfo->pkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemCount
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCItemPackageInfo->itemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode money
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCItemPackageInfo->money), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemPackageInfo->itemSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemArr
	if(MAXPACKAGEITEM < pkGCItemPackageInfo->itemSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCItemPackageInfo->itemSize; ++i)
	{
		if(EnCode__ItemInfo_i(&(pkGCItemPackageInfo->ItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode queryType
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemPackageInfo->queryType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCItemPackageInfo(void* pData)
{
	GCItemPackageInfo* pkGCItemPackageInfo = (GCItemPackageInfo*)(pData);

	//DeCode pkgIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCItemPackageInfo->pkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemCount
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCItemPackageInfo->itemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode money
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCItemPackageInfo->money), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemPackageInfo->itemSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemArr
	if(MAXPACKAGEITEM < pkGCItemPackageInfo->itemSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCItemPackageInfo->itemSize; ++i)
	{
		if(DeCode__ItemInfo_i(&(pkGCItemPackageInfo->ItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode queryType
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemPackageInfo->queryType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCItemPackageInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerPickItem(void* pData)
{
	GCPlayerPickItem* pkGCPlayerPickItem = (GCPlayerPickItem*)(pData);

	//EnCode pkgIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerPickItem->pkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerPickItem->itemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode IsPickMoney
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCPlayerPickItem->IsPickMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerPickItem(void* pData)
{
	GCPlayerPickItem* pkGCPlayerPickItem = (GCPlayerPickItem*)(pData);

	//DeCode pkgIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerPickItem->pkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerPickItem->itemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode IsPickMoney
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCPlayerPickItem->IsPickMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerPickItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCWeaponPackageInfo(void* pData)
{
	GCWeaponPackageInfo* pkGCWeaponPackageInfo = (GCWeaponPackageInfo*)(pData);

	//EnCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCWeaponPackageInfo->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCWeaponPackageInfo->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode weaponInfo
	if(EnCode__WeaponAdditionProp(&(pkGCWeaponPackageInfo->weaponInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCWeaponPackageInfo(void* pData)
{
	GCWeaponPackageInfo* pkGCWeaponPackageInfo = (GCWeaponPackageInfo*)(pData);

	//DeCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCWeaponPackageInfo->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCWeaponPackageInfo->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode weaponInfo
	if(DeCode__WeaponAdditionProp(&(pkGCWeaponPackageInfo->weaponInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCWeaponPackageInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCEquipPackageInfo(void* pData)
{
	GCEquipPackageInfo* pkGCEquipPackageInfo = (GCEquipPackageInfo*)(pData);

	//EnCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCEquipPackageInfo->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCEquipPackageInfo->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equipInfo
	if(EnCode__EquipAdditionProp(&(pkGCEquipPackageInfo->equipInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCEquipPackageInfo(void* pData)
{
	GCEquipPackageInfo* pkGCEquipPackageInfo = (GCEquipPackageInfo*)(pData);

	//DeCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCEquipPackageInfo->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCEquipPackageInfo->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equipInfo
	if(DeCode__EquipAdditionProp(&(pkGCEquipPackageInfo->equipInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCEquipPackageInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPickItem(void* pData)
{
	CGPickItem* pkCGPickItem = (CGPickItem*)(pData);

	//EnCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPickItem->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPickItem->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPickItem(void* pData)
{
	CGPickItem* pkCGPickItem = (CGPickItem*)(pData);

	//DeCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPickItem->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPickItem->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPickItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPickAllItems(void* pData)
{
	CGPickAllItems* pkCGPickAllItems = (CGPickAllItems*)(pData);

	//EnCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPickAllItems->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPickAllItems(void* pData)
{
	CGPickAllItems* pkCGPickAllItems = (CGPickAllItems*)(pData);

	//DeCode ItemPackageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPickAllItems->ItemPackageID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPickAllItems);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSetPkgItem(void* pData)
{
	GCSetPkgItem* pkGCSetPkgItem = (GCSetPkgItem*)(pData);

	//EnCode ItemPkgIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSetPkgItem->ItemPkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemInfo
	if(EnCode__ItemInfo_i(&(pkGCSetPkgItem->ItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSetPkgItem(void* pData)
{
	GCSetPkgItem* pkGCSetPkgItem = (GCSetPkgItem*)(pData);

	//DeCode ItemPkgIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSetPkgItem->ItemPkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemInfo
	if(DeCode__ItemInfo_i(&(pkGCSetPkgItem->ItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSetPkgItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSetEquipItem(void* pData)
{
	GCSetEquipItem* pkGCSetEquipItem = (GCSetEquipItem*)(pData);

	//EnCode ItemEquipIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSetEquipItem->ItemEquipIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemInfo
	if(EnCode__ItemInfo_i(&(pkGCSetEquipItem->ItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSetEquipItem(void* pData)
{
	GCSetEquipItem* pkGCSetEquipItem = (GCSetEquipItem*)(pData);

	//DeCode ItemEquipIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSetEquipItem->ItemEquipIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemInfo
	if(DeCode__ItemInfo_i(&(pkGCSetEquipItem->ItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSetEquipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTargetGetDamage(void* pData)
{
	GCTargetGetDamage* pkGCTargetGetDamage = (GCTargetGetDamage*)(pData);

	//EnCode targetID
	if(EnCode__PlayerID(&(pkGCTargetGetDamage->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode notiecID
	if(EnCode__PlayerID(&(pkGCTargetGetDamage->notiecID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode skillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTargetGetDamage->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode damage
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTargetGetDamage->damage), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode battleFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTargetGetDamage->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetLeftHp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTargetGetDamage->targetLeftHp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTargetGetDamage(void* pData)
{
	GCTargetGetDamage* pkGCTargetGetDamage = (GCTargetGetDamage*)(pData);

	//DeCode targetID
	if(DeCode__PlayerID(&(pkGCTargetGetDamage->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode notiecID
	if(DeCode__PlayerID(&(pkGCTargetGetDamage->notiecID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode skillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTargetGetDamage->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode damage
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTargetGetDamage->damage), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode battleFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTargetGetDamage->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetLeftHp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTargetGetDamage->targetLeftHp), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTargetGetDamage);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPickItemFail(void* pData)
{
	GCPickItemFail* pkGCPickItemFail = (GCPickItemFail*)(pData);

	//EnCode itemTypeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPickItemFail->itemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPickItemFail->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPickItemFail(void* pData)
{
	GCPickItemFail* pkGCPickItemFail = (GCPickItemFail*)(pData);

	//DeCode itemTypeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPickItemFail->itemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPickItemFail->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPickItemFail);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUseItem(void* pData)
{
	CGUseItem* pkCGUseItem = (CGUseItem*)(pData);

	//EnCode itemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUseItem->itemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUseItem(void* pData)
{
	CGUseItem* pkCGUseItem = (CGUseItem*)(pData);

	//DeCode itemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUseItem->itemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGUseItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUseItem(void* pData)
{
	GCUseItem* pkGCUseItem = (GCUseItem*)(pData);

	//EnCode UserID
	if(EnCode__PlayerID(&(pkGCUseItem->UserID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode useItem
	if(EnCode__ItemInfo_i(&(pkGCUseItem->useItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUseItem(void* pData)
{
	GCUseItem* pkGCUseItem = (GCUseItem*)(pData);

	//DeCode UserID
	if(DeCode__PlayerID(&(pkGCUseItem->UserID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode useItem
	if(DeCode__ItemInfo_i(&(pkGCUseItem->useItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUseItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUseItemResult(void* pData)
{
	GCUseItemResult* pkGCUseItemResult = (GCUseItemResult*)(pData);

	//EnCode result
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCUseItemResult->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUseItemResult(void* pData)
{
	GCUseItemResult* pkGCUseItemResult = (GCUseItemResult*)(pData);

	//DeCode result
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCUseItemResult->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUseItemResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUnUseItem(void* pData)
{
	CGUnUseItem* pkCGUnUseItem = (CGUnUseItem*)(pData);

	//EnCode From
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGUnUseItem->From), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode To
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGUnUseItem->To), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUnUseItem(void* pData)
{
	CGUnUseItem* pkCGUnUseItem = (CGUnUseItem*)(pData);

	//DeCode From
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGUnUseItem->From), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode To
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGUnUseItem->To), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGUnUseItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnUseItem(void* pData)
{
	GCUnUseItem* pkGCUnUseItem = (GCUnUseItem*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCUnUseItem->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode equipIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCUnUseItem->equipIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnUseItem(void* pData)
{
	GCUnUseItem* pkGCUnUseItem = (GCUnUseItem*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCUnUseItem->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode equipIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCUnUseItem->equipIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnUseItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnUseItemResult(void* pData)
{
	GCUnUseItemResult* pkGCUnUseItemResult = (GCUnUseItemResult*)(pData);

	//EnCode result
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCUnUseItemResult->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnUseItemResult(void* pData)
{
	GCUnUseItemResult* pkGCUnUseItemResult = (GCUnUseItemResult*)(pData);

	//DeCode result
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCUnUseItemResult->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnUseItemResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSWAPITEM(void* pData)
{
	CGSWAPITEM* pkCGSWAPITEM = (CGSWAPITEM*)(pData);

	//EnCode playID
	if(EnCode__PlayerID(&(pkCGSWAPITEM->playID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode pkgFrom
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGSWAPITEM->pkgFrom), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pkgTo
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGSWAPITEM->pkgTo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSWAPITEM(void* pData)
{
	CGSWAPITEM* pkCGSWAPITEM = (CGSWAPITEM*)(pData);

	//DeCode playID
	if(DeCode__PlayerID(&(pkCGSWAPITEM->playID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode pkgFrom
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGSWAPITEM->pkgFrom), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pkgTo
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGSWAPITEM->pkgTo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSWAPITEM);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSplitItem(void* pData)
{
	CGSplitItem* pkCGSplitItem = (CGSplitItem*)(pData);

	//EnCode ItemPkgIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGSplitItem->ItemPkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SplitNum
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGSplitItem->SplitNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSplitItem(void* pData)
{
	CGSplitItem* pkCGSplitItem = (CGSplitItem*)(pData);

	//DeCode ItemPkgIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGSplitItem->ItemPkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SplitNum
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGSplitItem->SplitNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSplitItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSwapItemResult(void* pData)
{
	GCSwapItemResult* pkGCSwapItemResult = (GCSwapItemResult*)(pData);

	//EnCode result
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCSwapItemResult->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSwapItemResult(void* pData)
{
	GCSwapItemResult* pkGCSwapItemResult = (GCSwapItemResult*)(pData);

	//DeCode result
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCSwapItemResult->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSwapItemResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCItemDetialInfo(void* pData)
{
	GCItemDetialInfo* pkGCItemDetialInfo = (GCItemDetialInfo*)(pData);

	//EnCode itemDetialInfo
	if(EnCode__EquipItemInfo(&(pkGCItemDetialInfo->itemDetialInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCItemDetialInfo(void* pData)
{
	GCItemDetialInfo* pkGCItemDetialInfo = (GCItemDetialInfo*)(pData);

	//DeCode itemDetialInfo
	if(DeCode__EquipItemInfo(&(pkGCItemDetialInfo->itemDetialInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCItemDetialInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCEquipConciseInfo(void* pData)
{
	GCEquipConciseInfo* pkGCEquipConciseInfo = (GCEquipConciseInfo*)(pData);

	//EnCode equipInfo
	if(EnCode__PlayerInfo_2_Equips(&(pkGCEquipConciseInfo->equipInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCEquipConciseInfo(void* pData)
{
	GCEquipConciseInfo* pkGCEquipConciseInfo = (GCEquipConciseInfo*)(pData);

	//DeCode equipInfo
	if(DeCode__PlayerInfo_2_Equips(&(pkGCEquipConciseInfo->equipInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCEquipConciseInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCItemPkgConciseInfo(void* pData)
{
	GCItemPkgConciseInfo* pkGCItemPkgConciseInfo = (GCItemPkgConciseInfo*)(pData);

	//EnCode pkgInfoSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCItemPkgConciseInfo->pkgInfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pkgInfo
	if(PKG_PAGE_SIZE < pkGCItemPkgConciseInfo->pkgInfoSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCItemPkgConciseInfo->pkgInfoSize; ++i)
	{
		if(EnCode__ItemInfo_i(&(pkGCItemPkgConciseInfo->pkgInfo[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode pageIndex
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCItemPkgConciseInfo->pageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCItemPkgConciseInfo(void* pData)
{
	GCItemPkgConciseInfo* pkGCItemPkgConciseInfo = (GCItemPkgConciseInfo*)(pData);

	//DeCode pkgInfoSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCItemPkgConciseInfo->pkgInfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pkgInfo
	if(PKG_PAGE_SIZE < pkGCItemPkgConciseInfo->pkgInfoSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCItemPkgConciseInfo->pkgInfoSize; ++i)
	{
		if(DeCode__ItemInfo_i(&(pkGCItemPkgConciseInfo->pkgInfo[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode pageIndex
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCItemPkgConciseInfo->pageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCItemPkgConciseInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDropItemResult(void* pData)
{
	GCDropItemResult* pkGCDropItemResult = (GCDropItemResult*)(pData);

	//EnCode result
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDropItemResult->result), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemUID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDropItemResult->itemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDropItemResult(void* pData)
{
	GCDropItemResult* pkGCDropItemResult = (GCDropItemResult*)(pData);

	//DeCode result
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDropItemResult->result), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemUID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDropItemResult->itemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDropItemResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGEquipItem(void* pData)
{
	CGEquipItem* pkCGEquipItem = (CGEquipItem*)(pData);

	//EnCode equipItemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGEquipItem->equipItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equipIndex
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGEquipItem->equipIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGEquipItem(void* pData)
{
	CGEquipItem* pkCGEquipItem = (CGEquipItem*)(pData);

	//DeCode equipItemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGEquipItem->equipItemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equipIndex
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGEquipItem->equipIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGEquipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDropItem(void* pData)
{
	CGDropItem* pkCGDropItem = (CGDropItem*)(pData);

	//EnCode itemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDropItem->itemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDropItem(void* pData)
{
	CGDropItem* pkCGDropItem = (CGDropItem*)(pData);

	//DeCode itemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDropItem->itemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDropItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMoneyUpdate(void* pData)
{
	GCMoneyUpdate* pkGCMoneyUpdate = (GCMoneyUpdate*)(pData);

	//EnCode money
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMoneyUpdate->money), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMoneyUpdate(void* pData)
{
	GCMoneyUpdate* pkGCMoneyUpdate = (GCMoneyUpdate*)(pData);

	//DeCode money
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMoneyUpdate->money), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMoneyUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGActiveMount(void* pData)
{
	CGActiveMount* pkCGActiveMount = (CGActiveMount*)(pData);

	//EnCode itemUID
	if(EnCode__ItemInfo_i(&(pkCGActiveMount->itemUID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGActiveMount(void* pData)
{
	CGActiveMount* pkCGActiveMount = (CGActiveMount*)(pData);

	//DeCode itemUID
	if(DeCode__ItemInfo_i(&(pkCGActiveMount->itemUID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGActiveMount);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGInviteMount(void* pData)
{
	CGInviteMount* pkCGInviteMount = (CGInviteMount*)(pData);

	//EnCode targetID
	if(EnCode__PlayerID(&(pkCGInviteMount->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGInviteMount(void* pData)
{
	CGInviteMount* pkCGInviteMount = (CGInviteMount*)(pData);

	//DeCode targetID
	if(DeCode__PlayerID(&(pkCGInviteMount->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGInviteMount);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAgreeBeInvite(void* pData)
{
	CGAgreeBeInvite* pkCGAgreeBeInvite = (CGAgreeBeInvite*)(pData);

	//EnCode launchID
	if(EnCode__PlayerID(&(pkCGAgreeBeInvite->launchID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode isAgree
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGAgreeBeInvite->isAgree), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAgreeBeInvite(void* pData)
{
	CGAgreeBeInvite* pkCGAgreeBeInvite = (CGAgreeBeInvite*)(pData);

	//DeCode launchID
	if(DeCode__PlayerID(&(pkCGAgreeBeInvite->launchID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode isAgree
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGAgreeBeInvite->isAgree), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAgreeBeInvite);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGLeaveMountTeam(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGLeaveMountTeam(void* pData)
{
	(pData);
	return sizeof(CGLeaveMountTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCActiveMountFail(void* pData)
{
	GCActiveMountFail* pkGCActiveMountFail = (GCActiveMountFail*)(pData);

	//EnCode reason
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCActiveMountFail->reason), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCActiveMountFail(void* pData)
{
	GCActiveMountFail* pkGCActiveMountFail = (GCActiveMountFail*)(pData);

	//DeCode reason
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCActiveMountFail->reason), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCActiveMountFail);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMountLeader(void* pData)
{
	GCMountLeader* pkGCMountLeader = (GCMountLeader*)(pData);

	//EnCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMountLeader->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMountLeader->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMountLeader(void* pData)
{
	GCMountLeader* pkGCMountLeader = (GCMountLeader*)(pData);

	//DeCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMountLeader->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMountLeader->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMountLeader);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMountAssist(void* pData)
{
	GCMountAssist* pkGCMountAssist = (GCMountAssist*)(pData);

	//EnCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMountAssist->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mountSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMountAssist->mountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MountTeam
	if(MAXMOUNTCOUNT < pkGCMountAssist->mountSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCMountAssist->mountSize; ++i)
	{
		if(EnCode__PlayerID(&(pkGCMountAssist->MountTeam[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMountAssist(void* pData)
{
	GCMountAssist* pkGCMountAssist = (GCMountAssist*)(pData);

	//DeCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMountAssist->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mountSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMountAssist->mountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MountTeam
	if(MAXMOUNTCOUNT < pkGCMountAssist->mountSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCMountAssist->mountSize; ++i)
	{
		if(DeCode__PlayerID(&(pkGCMountAssist->MountTeam[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCMountAssist);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMountTeamInfo(void* pData)
{
	GCMountTeamInfo* pkGCMountTeamInfo = (GCMountTeamInfo*)(pData);

	//EnCode mountSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMountTeamInfo->mountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MountTeam
	if(MAXMOUNTCOUNT < pkGCMountTeamInfo->mountSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCMountTeamInfo->mountSize; ++i)
	{
		if(EnCode__PlayerID(&(pkGCMountTeamInfo->MountTeam[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode rideItem
	if(EnCode__ItemInfo_i(&(pkGCMountTeamInfo->rideItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode x
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMountTeamInfo->x), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode y
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMountTeamInfo->y), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMountTeamInfo(void* pData)
{
	GCMountTeamInfo* pkGCMountTeamInfo = (GCMountTeamInfo*)(pData);

	//DeCode mountSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMountTeamInfo->mountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MountTeam
	if(MAXMOUNTCOUNT < pkGCMountTeamInfo->mountSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCMountTeamInfo->mountSize; ++i)
	{
		if(DeCode__PlayerID(&(pkGCMountTeamInfo->MountTeam[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode rideItem
	if(DeCode__ItemInfo_i(&(pkGCMountTeamInfo->rideItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode x
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMountTeamInfo->x), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode y
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMountTeamInfo->y), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMountTeamInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMountTeamDisBand(void* pData)
{
	GCMountTeamDisBand* pkGCMountTeamDisBand = (GCMountTeamDisBand*)(pData);

	//EnCode mountTeamID
	if(EnCode__PlayerID(&(pkGCMountTeamDisBand->mountTeamID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMountTeamDisBand(void* pData)
{
	GCMountTeamDisBand* pkGCMountTeamDisBand = (GCMountTeamDisBand*)(pData);

	//DeCode mountTeamID
	if(DeCode__PlayerID(&(pkGCMountTeamDisBand->mountTeamID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMountTeamDisBand);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerLeaveMountTeam(void* pData)
{
	GCPlayerLeaveMountTeam* pkGCPlayerLeaveMountTeam = (GCPlayerLeaveMountTeam*)(pData);

	//EnCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerLeaveMountTeam->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode levelID
	if(EnCode__PlayerID(&(pkGCPlayerLeaveMountTeam->levelID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerLeaveMountTeam(void* pData)
{
	GCPlayerLeaveMountTeam* pkGCPlayerLeaveMountTeam = (GCPlayerLeaveMountTeam*)(pData);

	//DeCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerLeaveMountTeam->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode levelID
	if(DeCode__PlayerID(&(pkGCPlayerLeaveMountTeam->levelID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerLeaveMountTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCJoinMountTeam(void* pData)
{
	GCJoinMountTeam* pkGCJoinMountTeam = (GCJoinMountTeam*)(pData);

	//EnCode JoinPlayerID
	if(EnCode__PlayerID(&(pkGCJoinMountTeam->JoinPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCJoinMountTeam->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCJoinMountTeam(void* pData)
{
	GCJoinMountTeam* pkGCJoinMountTeam = (GCJoinMountTeam*)(pData);

	//DeCode JoinPlayerID
	if(DeCode__PlayerID(&(pkGCJoinMountTeam->JoinPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCJoinMountTeam->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCJoinMountTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCBeInviteMount(void* pData)
{
	GCBeInviteMount* pkGCBeInviteMount = (GCBeInviteMount*)(pData);

	//EnCode launchID
	if(EnCode__PlayerID(&(pkGCBeInviteMount->launchID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCBeInviteMount(void* pData)
{
	GCBeInviteMount* pkGCBeInviteMount = (GCBeInviteMount*)(pData);

	//DeCode launchID
	if(DeCode__PlayerID(&(pkGCBeInviteMount->launchID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCBeInviteMount);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCInviteResult(void* pData)
{
	GCInviteResult* pkGCInviteResult = (GCInviteResult*)(pData);

	//EnCode targetID
	if(EnCode__PlayerID(&(pkGCInviteResult->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode isAgree
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCInviteResult->isAgree), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCInviteResult(void* pData)
{
	GCInviteResult* pkGCInviteResult = (GCInviteResult*)(pData);

	//DeCode targetID
	if(DeCode__PlayerID(&(pkGCInviteResult->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode isAgree
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCInviteResult->isAgree), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCInviteResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMountTeamMove(void* pData)
{
	GCMountTeamMove* pkGCMountTeamMove = (GCMountTeamMove*)(pData);

	//EnCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMountTeamMove->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode moveInfo
	if(EnCode__GCRun(&(pkGCMountTeamMove->moveInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMountTeamMove(void* pData)
{
	GCMountTeamMove* pkGCMountTeamMove = (GCMountTeamMove*)(pData);

	//DeCode mountTeamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMountTeamMove->mountTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode moveInfo
	if(DeCode__GCRun(&(pkGCMountTeamMove->moveInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMountTeamMove);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerBuffStart(void* pData)
{
	GCPlayerBuffStart* pkGCPlayerBuffStart = (GCPlayerBuffStart*)(pData);

	//EnCode changePlayerID
	if(EnCode__PlayerID(&(pkGCPlayerBuffStart->changePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode createPlayerID
	if(EnCode__PlayerID(&(pkGCPlayerBuffStart->createPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode buffInfo
	if(EnCode__BuffDetailInfo(&(pkGCPlayerBuffStart->buffInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nBuffIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerBuffStart->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerBuffStart(void* pData)
{
	GCPlayerBuffStart* pkGCPlayerBuffStart = (GCPlayerBuffStart*)(pData);

	//DeCode changePlayerID
	if(DeCode__PlayerID(&(pkGCPlayerBuffStart->changePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode createPlayerID
	if(DeCode__PlayerID(&(pkGCPlayerBuffStart->createPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode buffInfo
	if(DeCode__BuffDetailInfo(&(pkGCPlayerBuffStart->buffInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nBuffIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerBuffStart->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerBuffStart);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerBuffEnd(void* pData)
{
	GCPlayerBuffEnd* pkGCPlayerBuffEnd = (GCPlayerBuffEnd*)(pData);

	//EnCode changePlayerID
	if(EnCode__PlayerID(&(pkGCPlayerBuffEnd->changePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode buffID
	if(EnCode__BuffID(&(pkGCPlayerBuffEnd->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nBuffIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerBuffEnd->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerBuffEnd(void* pData)
{
	GCPlayerBuffEnd* pkGCPlayerBuffEnd = (GCPlayerBuffEnd*)(pData);

	//DeCode changePlayerID
	if(DeCode__PlayerID(&(pkGCPlayerBuffEnd->changePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode buffID
	if(DeCode__BuffID(&(pkGCPlayerBuffEnd->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nBuffIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerBuffEnd->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerBuffEnd);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerBuffUpdate(void* pData)
{
	GCPlayerBuffUpdate* pkGCPlayerBuffUpdate = (GCPlayerBuffUpdate*)(pData);

	//EnCode nChangeHP
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerBuffUpdate->nChangeHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPlayerCurrentHP
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerBuffUpdate->nPlayerCurrentHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode changePlayerID
	if(EnCode__PlayerID(&(pkGCPlayerBuffUpdate->changePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode buffID
	if(EnCode__BuffID(&(pkGCPlayerBuffUpdate->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nBuffIndex
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerBuffUpdate->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode createBuffPlayerID
	if(EnCode__PlayerID(&(pkGCPlayerBuffUpdate->createBuffPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode battleFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerBuffUpdate->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerBuffUpdate(void* pData)
{
	GCPlayerBuffUpdate* pkGCPlayerBuffUpdate = (GCPlayerBuffUpdate*)(pData);

	//DeCode nChangeHP
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerBuffUpdate->nChangeHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPlayerCurrentHP
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerBuffUpdate->nPlayerCurrentHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode changePlayerID
	if(DeCode__PlayerID(&(pkGCPlayerBuffUpdate->changePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode buffID
	if(DeCode__BuffID(&(pkGCPlayerBuffUpdate->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nBuffIndex
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerBuffUpdate->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode createBuffPlayerID
	if(DeCode__PlayerID(&(pkGCPlayerBuffUpdate->createBuffPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode battleFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerBuffUpdate->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerBuffUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMonsterBuffEnd(void* pData)
{
	GCMonsterBuffEnd* pkGCMonsterBuffEnd = (GCMonsterBuffEnd*)(pData);

	//EnCode monsterID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterBuffEnd->monsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buffID
	if(EnCode__BuffID(&(pkGCMonsterBuffEnd->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nBuffIndex
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMonsterBuffEnd->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMonsterBuffEnd(void* pData)
{
	GCMonsterBuffEnd* pkGCMonsterBuffEnd = (GCMonsterBuffEnd*)(pData);

	//DeCode monsterID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterBuffEnd->monsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buffID
	if(DeCode__BuffID(&(pkGCMonsterBuffEnd->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nBuffIndex
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMonsterBuffEnd->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMonsterBuffEnd);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMonsterBuffStart(void* pData)
{
	GCMonsterBuffStart* pkGCMonsterBuffStart = (GCMonsterBuffStart*)(pData);

	//EnCode monsterID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterBuffStart->monsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buffInfo
	if(EnCode__BuffDetailInfo(&(pkGCMonsterBuffStart->buffInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode createPlayerID
	if(EnCode__PlayerID(&(pkGCMonsterBuffStart->createPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nBuffIndex
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMonsterBuffStart->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMonsterBuffStart(void* pData)
{
	GCMonsterBuffStart* pkGCMonsterBuffStart = (GCMonsterBuffStart*)(pData);

	//DeCode monsterID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterBuffStart->monsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buffInfo
	if(DeCode__BuffDetailInfo(&(pkGCMonsterBuffStart->buffInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode createPlayerID
	if(DeCode__PlayerID(&(pkGCMonsterBuffStart->createPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nBuffIndex
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMonsterBuffStart->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMonsterBuffStart);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMonsterBuffUpdate(void* pData)
{
	GCMonsterBuffUpdate* pkGCMonsterBuffUpdate = (GCMonsterBuffUpdate*)(pData);

	//EnCode nChangeHP
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMonsterBuffUpdate->nChangeHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nMonsterCurrentHP
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMonsterBuffUpdate->nMonsterCurrentHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode monsterID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterBuffUpdate->monsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buffID
	if(EnCode__BuffID(&(pkGCMonsterBuffUpdate->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nBuffIndex
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMonsterBuffUpdate->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode battleFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMonsterBuffUpdate->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMonsterBuffUpdate(void* pData)
{
	GCMonsterBuffUpdate* pkGCMonsterBuffUpdate = (GCMonsterBuffUpdate*)(pData);

	//DeCode nChangeHP
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMonsterBuffUpdate->nChangeHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nMonsterCurrentHP
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMonsterBuffUpdate->nMonsterCurrentHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode monsterID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterBuffUpdate->monsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buffID
	if(DeCode__BuffID(&(pkGCMonsterBuffUpdate->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nBuffIndex
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMonsterBuffUpdate->nBuffIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode battleFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMonsterBuffUpdate->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMonsterBuffUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGReqBuffEnd(void* pData)
{
	CGReqBuffEnd* pkCGReqBuffEnd = (CGReqBuffEnd*)(pData);

	//EnCode buffID
	if(EnCode__BuffID(&(pkCGReqBuffEnd->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGReqBuffEnd(void* pData)
{
	CGReqBuffEnd* pkCGReqBuffEnd = (CGReqBuffEnd*)(pData);

	//DeCode buffID
	if(DeCode__BuffID(&(pkCGReqBuffEnd->buffID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGReqBuffEnd);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCReqBuffEnd(void* pData)
{
	GCReqBuffEnd* pkGCReqBuffEnd = (GCReqBuffEnd*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCReqBuffEnd->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCReqBuffEnd(void* pData)
{
	GCReqBuffEnd* pkGCReqBuffEnd = (GCReqBuffEnd*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCReqBuffEnd->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCReqBuffEnd);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGInvitePlayerJoinTeam(void* pData)
{
	CGInvitePlayerJoinTeam* pkCGInvitePlayerJoinTeam = (CGInvitePlayerJoinTeam*)(pData);

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGInvitePlayerJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerName
	if(MAX_NAME_SIZE < pkCGInvitePlayerJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGInvitePlayerJoinTeam->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGInvitePlayerJoinTeam->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGInvitePlayerJoinTeam(void* pData)
{
	CGInvitePlayerJoinTeam* pkCGInvitePlayerJoinTeam = (CGInvitePlayerJoinTeam*)(pData);

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGInvitePlayerJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerName
	if(MAX_NAME_SIZE < pkCGInvitePlayerJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGInvitePlayerJoinTeam->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGInvitePlayerJoinTeam->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGInvitePlayerJoinTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCInvitePlayerJoinTeam(void* pData)
{
	GCInvitePlayerJoinTeam* pkGCInvitePlayerJoinTeam = (GCInvitePlayerJoinTeam*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInvitePlayerJoinTeam->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInvitePlayerJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerName
	if(MAX_NAME_SIZE < pkGCInvitePlayerJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCInvitePlayerJoinTeam->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCInvitePlayerJoinTeam->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCInvitePlayerJoinTeam(void* pData)
{
	GCInvitePlayerJoinTeam* pkGCInvitePlayerJoinTeam = (GCInvitePlayerJoinTeam*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInvitePlayerJoinTeam->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInvitePlayerJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerName
	if(MAX_NAME_SIZE < pkGCInvitePlayerJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCInvitePlayerJoinTeam->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCInvitePlayerJoinTeam->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCInvitePlayerJoinTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRecvInviteJoinTeam(void* pData)
{
	GCRecvInviteJoinTeam* pkGCRecvInviteJoinTeam = (GCRecvInviteJoinTeam*)(pData);

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRecvInviteJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode invitePlayerName
	if(MAX_NAME_SIZE < pkGCRecvInviteJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRecvInviteJoinTeam->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCRecvInviteJoinTeam->invitePlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode invitePlayerID
	if(EnCode__PlayerID(&(pkGCRecvInviteJoinTeam->invitePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRecvInviteJoinTeam(void* pData)
{
	GCRecvInviteJoinTeam* pkGCRecvInviteJoinTeam = (GCRecvInviteJoinTeam*)(pData);

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRecvInviteJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode invitePlayerName
	if(MAX_NAME_SIZE < pkGCRecvInviteJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRecvInviteJoinTeam->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRecvInviteJoinTeam->invitePlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode invitePlayerID
	if(DeCode__PlayerID(&(pkGCRecvInviteJoinTeam->invitePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRecvInviteJoinTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRecvInviteJoinTeamFeedBack(void* pData)
{
	CGRecvInviteJoinTeamFeedBack* pkCGRecvInviteJoinTeamFeedBack = (CGRecvInviteJoinTeamFeedBack*)(pData);

	//EnCode invitePlayerID
	if(EnCode__PlayerID(&(pkCGRecvInviteJoinTeamFeedBack->invitePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ucAnswer
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGRecvInviteJoinTeamFeedBack->ucAnswer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRecvInviteJoinTeamFeedBack(void* pData)
{
	CGRecvInviteJoinTeamFeedBack* pkCGRecvInviteJoinTeamFeedBack = (CGRecvInviteJoinTeamFeedBack*)(pData);

	//DeCode invitePlayerID
	if(DeCode__PlayerID(&(pkCGRecvInviteJoinTeamFeedBack->invitePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ucAnswer
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGRecvInviteJoinTeamFeedBack->ucAnswer), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRecvInviteJoinTeamFeedBack);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRecvInviteJoinTeamFeedBackError(void* pData)
{
	GCRecvInviteJoinTeamFeedBackError* pkGCRecvInviteJoinTeamFeedBackError = (GCRecvInviteJoinTeamFeedBackError*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRecvInviteJoinTeamFeedBackError->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRecvInviteJoinTeamFeedBackError(void* pData)
{
	GCRecvInviteJoinTeamFeedBackError* pkGCRecvInviteJoinTeamFeedBackError = (GCRecvInviteJoinTeamFeedBackError*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRecvInviteJoinTeamFeedBackError->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRecvInviteJoinTeamFeedBackError);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGReqQuitTeam(void* pData)
{
	CGReqQuitTeam* pkCGReqQuitTeam = (CGReqQuitTeam*)(pData);

	//EnCode teamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGReqQuitTeam->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGReqQuitTeam(void* pData)
{
	CGReqQuitTeam* pkCGReqQuitTeam = (CGReqQuitTeam*)(pData);

	//DeCode teamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGReqQuitTeam->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGReqQuitTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCReqQuitTeam(void* pData)
{
	GCReqQuitTeam* pkGCReqQuitTeam = (GCReqQuitTeam*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqQuitTeam->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode quitMemberID
	if(EnCode__PlayerID(&(pkGCReqQuitTeam->quitMemberID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCReqQuitTeam(void* pData)
{
	GCReqQuitTeam* pkGCReqQuitTeam = (GCReqQuitTeam*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqQuitTeam->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode quitMemberID
	if(DeCode__PlayerID(&(pkGCReqQuitTeam->quitMemberID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCReqQuitTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangeTeamExpMode(void* pData)
{
	CGChangeTeamExpMode* pkCGChangeTeamExpMode = (CGChangeTeamExpMode*)(pData);

	//EnCode teamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChangeTeamExpMode->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucExpMode
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGChangeTeamExpMode->ucExpMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangeTeamExpMode(void* pData)
{
	CGChangeTeamExpMode* pkCGChangeTeamExpMode = (CGChangeTeamExpMode*)(pData);

	//DeCode teamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChangeTeamExpMode->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucExpMode
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGChangeTeamExpMode->ucExpMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangeTeamExpMode);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangeTeamItemMode(void* pData)
{
	CGChangeTeamItemMode* pkCGChangeTeamItemMode = (CGChangeTeamItemMode*)(pData);

	//EnCode teamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChangeTeamItemMode->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucItemMode
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGChangeTeamItemMode->ucItemMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangeTeamItemMode(void* pData)
{
	CGChangeTeamItemMode* pkCGChangeTeamItemMode = (CGChangeTeamItemMode*)(pData);

	//DeCode teamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChangeTeamItemMode->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucItemMode
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGChangeTeamItemMode->ucItemMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangeTeamItemMode);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeTeamExpMode(void* pData)
{
	GCChangeTeamExpMode* pkGCChangeTeamExpMode = (GCChangeTeamExpMode*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChangeTeamExpMode->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucExpMode
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCChangeTeamExpMode->ucExpMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeTeamExpMode(void* pData)
{
	GCChangeTeamExpMode* pkGCChangeTeamExpMode = (GCChangeTeamExpMode*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChangeTeamExpMode->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucExpMode
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCChangeTeamExpMode->ucExpMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeTeamExpMode);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeTeamItemMode(void* pData)
{
	GCChangeTeamItemMode* pkGCChangeTeamItemMode = (GCChangeTeamItemMode*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChangeTeamItemMode->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucItemMode
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCChangeTeamItemMode->ucItemMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeTeamItemMode(void* pData)
{
	GCChangeTeamItemMode* pkGCChangeTeamItemMode = (GCChangeTeamItemMode*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChangeTeamItemMode->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucItemMode
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCChangeTeamItemMode->ucItemMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeTeamItemMode);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTeamInfo(void* pData)
{
	GCTeamInfo* pkGCTeamInfo = (GCTeamInfo*)(pData);

	//EnCode ulTeamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTeamInfo->ulTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode leaderPlayerID
	if(EnCode__PlayerID(&(pkGCTeamInfo->leaderPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode teamSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTeamInfo->teamSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode membersInfo
	if(7 < pkGCTeamInfo->teamSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCTeamInfo->teamSize; ++i)
	{
		if(EnCode__TeamMemberCliInfo(&(pkGCTeamInfo->membersInfo[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode ucExpMode
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCTeamInfo->ucExpMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucItemMode
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCTeamInfo->ucItemMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucRollItemMode
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCTeamInfo->ucRollItemMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTeamInfo(void* pData)
{
	GCTeamInfo* pkGCTeamInfo = (GCTeamInfo*)(pData);

	//DeCode ulTeamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTeamInfo->ulTeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode leaderPlayerID
	if(DeCode__PlayerID(&(pkGCTeamInfo->leaderPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode teamSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTeamInfo->teamSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode membersInfo
	if(7 < pkGCTeamInfo->teamSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCTeamInfo->teamSize; ++i)
	{
		if(DeCode__TeamMemberCliInfo(&(pkGCTeamInfo->membersInfo[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode ucExpMode
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCTeamInfo->ucExpMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucItemMode
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCTeamInfo->ucItemMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucRollItemMode
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCTeamInfo->ucRollItemMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTeamInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangeLeader(void* pData)
{
	CGChangeLeader* pkCGChangeLeader = (CGChangeLeader*)(pData);

	//EnCode teamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChangeLeader->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newLeaderID
	if(EnCode__PlayerID(&(pkCGChangeLeader->newLeaderID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangeLeader(void* pData)
{
	CGChangeLeader* pkCGChangeLeader = (CGChangeLeader*)(pData);

	//DeCode teamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChangeLeader->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newLeaderID
	if(DeCode__PlayerID(&(pkCGChangeLeader->newLeaderID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangeLeader);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeLeader(void* pData)
{
	GCChangeLeader* pkGCChangeLeader = (GCChangeLeader*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChangeLeader->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode leaderID
	if(EnCode__PlayerID(&(pkGCChangeLeader->leaderID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode memberSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChangeLeader->memberSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode membersID
	if(6 < pkGCChangeLeader->memberSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCChangeLeader->memberSize; ++i)
	{
		if(EnCode__PlayerID(&(pkGCChangeLeader->membersID[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeLeader(void* pData)
{
	GCChangeLeader* pkGCChangeLeader = (GCChangeLeader*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChangeLeader->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode leaderID
	if(DeCode__PlayerID(&(pkGCChangeLeader->leaderID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode memberSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChangeLeader->memberSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode membersID
	if(6 < pkGCChangeLeader->memberSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCChangeLeader->memberSize; ++i)
	{
		if(DeCode__PlayerID(&(pkGCChangeLeader->membersID[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCChangeLeader);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTeamMemberDetailInfoUpdate(void* pData)
{
	GCTeamMemberDetailInfoUpdate* pkGCTeamMemberDetailInfoUpdate = (GCTeamMemberDetailInfoUpdate*)(pData);

	//EnCode teamMemberID
	if(EnCode__PlayerID(&(pkGCTeamMemberDetailInfoUpdate->teamMemberID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode uiHP
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTeamMemberDetailInfoUpdate->uiHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTeamMemberDetailInfoUpdate->uiMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode maxHP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTeamMemberDetailInfoUpdate->maxHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode maxMP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTeamMemberDetailInfoUpdate->maxMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCTeamMemberDetailInfoUpdate->nPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCTeamMemberDetailInfoUpdate->nPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode buffData
	if(EnCode__PlayerInfo_5_Buffers(&(pkGCTeamMemberDetailInfoUpdate->buffData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode mapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCTeamMemberDetailInfoUpdate->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usPlayerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCTeamMemberDetailInfoUpdate->usPlayerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode goodEvilPoint
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCTeamMemberDetailInfoUpdate->goodEvilPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTeamMemberDetailInfoUpdate(void* pData)
{
	GCTeamMemberDetailInfoUpdate* pkGCTeamMemberDetailInfoUpdate = (GCTeamMemberDetailInfoUpdate*)(pData);

	//DeCode teamMemberID
	if(DeCode__PlayerID(&(pkGCTeamMemberDetailInfoUpdate->teamMemberID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode uiHP
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTeamMemberDetailInfoUpdate->uiHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTeamMemberDetailInfoUpdate->uiMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode maxHP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTeamMemberDetailInfoUpdate->maxHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode maxMP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTeamMemberDetailInfoUpdate->maxMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCTeamMemberDetailInfoUpdate->nPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCTeamMemberDetailInfoUpdate->nPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode buffData
	if(DeCode__PlayerInfo_5_Buffers(&(pkGCTeamMemberDetailInfoUpdate->buffData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode mapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCTeamMemberDetailInfoUpdate->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usPlayerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCTeamMemberDetailInfoUpdate->usPlayerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode goodEvilPoint
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCTeamMemberDetailInfoUpdate->goodEvilPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTeamMemberDetailInfoUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGReqJoinTeam(void* pData)
{
	CGReqJoinTeam* pkCGReqJoinTeam = (CGReqJoinTeam*)(pData);

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGReqJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamLeadName
	if(MAX_NAME_SIZE < pkCGReqJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGReqJoinTeam->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGReqJoinTeam->teamLeadName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGReqJoinTeam(void* pData)
{
	CGReqJoinTeam* pkCGReqJoinTeam = (CGReqJoinTeam*)(pData);

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGReqJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamLeadName
	if(MAX_NAME_SIZE < pkCGReqJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGReqJoinTeam->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGReqJoinTeam->teamLeadName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGReqJoinTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCReqJoinTeamError(void* pData)
{
	GCReqJoinTeamError* pkGCReqJoinTeamError = (GCReqJoinTeamError*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqJoinTeamError->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqJoinTeamError->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamLeadName
	if(MAX_NAME_SIZE < pkGCReqJoinTeamError->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCReqJoinTeamError->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCReqJoinTeamError->teamLeadName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCReqJoinTeamError(void* pData)
{
	GCReqJoinTeamError* pkGCReqJoinTeamError = (GCReqJoinTeamError*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqJoinTeamError->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqJoinTeamError->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamLeadName
	if(MAX_NAME_SIZE < pkGCReqJoinTeamError->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCReqJoinTeamError->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCReqJoinTeamError->teamLeadName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCReqJoinTeamError);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGBanTeamMember(void* pData)
{
	CGBanTeamMember* pkCGBanTeamMember = (CGBanTeamMember*)(pData);

	//EnCode teamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGBanTeamMember->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode banPlayerID
	if(EnCode__PlayerID(&(pkCGBanTeamMember->banPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGBanTeamMember(void* pData)
{
	CGBanTeamMember* pkCGBanTeamMember = (CGBanTeamMember*)(pData);

	//DeCode teamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGBanTeamMember->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode banPlayerID
	if(DeCode__PlayerID(&(pkCGBanTeamMember->banPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGBanTeamMember);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCBanTeamMember(void* pData)
{
	GCBanTeamMember* pkGCBanTeamMember = (GCBanTeamMember*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCBanTeamMember->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCBanTeamMember->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode banPlayerID
	if(EnCode__PlayerID(&(pkGCBanTeamMember->banPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCBanTeamMember(void* pData)
{
	GCBanTeamMember* pkGCBanTeamMember = (GCBanTeamMember*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCBanTeamMember->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCBanTeamMember->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode banPlayerID
	if(DeCode__PlayerID(&(pkGCBanTeamMember->banPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCBanTeamMember);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRecvReqJoinTeam(void* pData)
{
	GCRecvReqJoinTeam* pkGCRecvReqJoinTeam = (GCRecvReqJoinTeam*)(pData);

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRecvReqJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode reqPlayerName
	if(MAX_NAME_SIZE < pkGCRecvReqJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRecvReqJoinTeam->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCRecvReqJoinTeam->reqPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode reqPlayerID
	if(EnCode__PlayerID(&(pkGCRecvReqJoinTeam->reqPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRecvReqJoinTeam(void* pData)
{
	GCRecvReqJoinTeam* pkGCRecvReqJoinTeam = (GCRecvReqJoinTeam*)(pData);

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRecvReqJoinTeam->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode reqPlayerName
	if(MAX_NAME_SIZE < pkGCRecvReqJoinTeam->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRecvReqJoinTeam->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRecvReqJoinTeam->reqPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode reqPlayerID
	if(DeCode__PlayerID(&(pkGCRecvReqJoinTeam->reqPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRecvReqJoinTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRecvReqJoinTeamFeedBack(void* pData)
{
	CGRecvReqJoinTeamFeedBack* pkCGRecvReqJoinTeamFeedBack = (CGRecvReqJoinTeamFeedBack*)(pData);

	//EnCode reqPlayerID
	if(EnCode__PlayerID(&(pkCGRecvReqJoinTeamFeedBack->reqPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ucAnswer
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGRecvReqJoinTeamFeedBack->ucAnswer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRecvReqJoinTeamFeedBack(void* pData)
{
	CGRecvReqJoinTeamFeedBack* pkCGRecvReqJoinTeamFeedBack = (CGRecvReqJoinTeamFeedBack*)(pData);

	//DeCode reqPlayerID
	if(DeCode__PlayerID(&(pkCGRecvReqJoinTeamFeedBack->reqPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ucAnswer
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGRecvReqJoinTeamFeedBack->ucAnswer), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRecvReqJoinTeamFeedBack);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRecvReqJoinTeamFeedBackError(void* pData)
{
	GCRecvReqJoinTeamFeedBackError* pkGCRecvReqJoinTeamFeedBackError = (GCRecvReqJoinTeamFeedBackError*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRecvReqJoinTeamFeedBackError->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRecvReqJoinTeamFeedBackError(void* pData)
{
	GCRecvReqJoinTeamFeedBackError* pkGCRecvReqJoinTeamFeedBackError = (GCRecvReqJoinTeamFeedBackError*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRecvReqJoinTeamFeedBackError->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRecvReqJoinTeamFeedBackError);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDestoryTeam(void* pData)
{
	CGDestoryTeam* pkCGDestoryTeam = (CGDestoryTeam*)(pData);

	//EnCode teamID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDestoryTeam->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDestoryTeam(void* pData)
{
	CGDestoryTeam* pkCGDestoryTeam = (CGDestoryTeam*)(pData);

	//DeCode teamID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDestoryTeam->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDestoryTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDestoryTeam(void* pData)
{
	GCDestoryTeam* pkGCDestoryTeam = (GCDestoryTeam*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDestoryTeam->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDestoryTeam->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDestoryTeam(void* pData)
{
	GCDestoryTeam* pkGCDestoryTeam = (GCDestoryTeam*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDestoryTeam->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDestoryTeam->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDestoryTeam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGTeamSwitch(void* pData)
{
	CGTeamSwitch* pkCGTeamSwitch = (CGTeamSwitch*)(pData);

	//EnCode ucTeamSwitch
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGTeamSwitch->ucTeamSwitch), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGTeamSwitch(void* pData)
{
	CGTeamSwitch* pkCGTeamSwitch = (CGTeamSwitch*)(pData);

	//DeCode ucTeamSwitch
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGTeamSwitch->ucTeamSwitch), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGTeamSwitch);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTeamSwitch(void* pData)
{
	GCTeamSwitch* pkGCTeamSwitch = (GCTeamSwitch*)(pData);

	//EnCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTeamSwitch->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucNewTeamSwitch
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCTeamSwitch->ucNewTeamSwitch), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTeamSwitch(void* pData)
{
	GCTeamSwitch* pkGCTeamSwitch = (GCTeamSwitch*)(pData);

	//DeCode nErrorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTeamSwitch->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucNewTeamSwitch
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCTeamSwitch->ucNewTeamSwitch), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTeamSwitch);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPickItemSuccess(void* pData)
{
	GCPickItemSuccess* pkGCPickItemSuccess = (GCPickItemSuccess*)(pData);

	//EnCode ItemPkgIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPickItemSuccess->ItemPkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemInfo
	if(EnCode__ItemInfo_i(&(pkGCPickItemSuccess->ItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPickItemSuccess(void* pData)
{
	GCPickItemSuccess* pkGCPickItemSuccess = (GCPickItemSuccess*)(pData);

	//DeCode ItemPkgIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPickItemSuccess->ItemPkgIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemInfo
	if(DeCode__ItemInfo_i(&(pkGCPickItemSuccess->ItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPickItemSuccess);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUseAssistSkill(void* pData)
{
	CGUseAssistSkill* pkCGUseAssistSkill = (CGUseAssistSkill*)(pData);

	//EnCode targetPlayer
	if(EnCode__PlayerID(&(pkCGUseAssistSkill->targetPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode bPlayer
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGUseAssistSkill->bPlayer), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUseAssistSkill->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isUnionSkill
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGUseAssistSkill->isUnionSkill), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUseAssistSkill(void* pData)
{
	CGUseAssistSkill* pkCGUseAssistSkill = (CGUseAssistSkill*)(pData);

	//DeCode targetPlayer
	if(DeCode__PlayerID(&(pkCGUseAssistSkill->targetPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode bPlayer
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGUseAssistSkill->bPlayer), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUseAssistSkill->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isUnionSkill
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGUseAssistSkill->isUnionSkill), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGUseAssistSkill);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUseAssistSkill(void* pData)
{
	GCUseAssistSkill* pkGCUseAssistSkill = (GCUseAssistSkill*)(pData);

	//EnCode useSkillPlayer
	if(EnCode__PlayerID(&(pkGCUseAssistSkill->useSkillPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode targetPlayer
	if(EnCode__PlayerID(&(pkGCUseAssistSkill->targetPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode skillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUseAssistSkill->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillCurseTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUseAssistSkill->skillCurseTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillCooldownTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUseAssistSkill->skillCooldownTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nErrorCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUseAssistSkill->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerSpExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUseAssistSkill->nAtkPlayerSpExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerSpSkillExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUseAssistSkill->nAtkPlayerSpSkillExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerSpPower
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUseAssistSkill->nAtkPlayerSpPower), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerMp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUseAssistSkill->nAtkPlayerMp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUseAssistSkill(void* pData)
{
	GCUseAssistSkill* pkGCUseAssistSkill = (GCUseAssistSkill*)(pData);

	//DeCode useSkillPlayer
	if(DeCode__PlayerID(&(pkGCUseAssistSkill->useSkillPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode targetPlayer
	if(DeCode__PlayerID(&(pkGCUseAssistSkill->targetPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode skillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUseAssistSkill->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillCurseTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUseAssistSkill->skillCurseTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillCooldownTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUseAssistSkill->skillCooldownTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nErrorCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUseAssistSkill->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerSpExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUseAssistSkill->nAtkPlayerSpExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerSpSkillExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUseAssistSkill->nAtkPlayerSpSkillExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerSpPower
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUseAssistSkill->nAtkPlayerSpPower), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerMp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUseAssistSkill->nAtkPlayerMp), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUseAssistSkill);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddFriendReq(void* pData)
{
	CGAddFriendReq* pkCGAddFriendReq = (CGAddFriendReq*)(pData);

	//EnCode nickNameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddFriendReq->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickName
	if(MAX_NAME_SIZE < pkCGAddFriendReq->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddFriendReq->nickNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGAddFriendReq->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddFriendReq(void* pData)
{
	CGAddFriendReq* pkCGAddFriendReq = (CGAddFriendReq*)(pData);

	//DeCode nickNameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddFriendReq->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickName
	if(MAX_NAME_SIZE < pkCGAddFriendReq->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddFriendReq->nickNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGAddFriendReq->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddFriendReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCFriendGroups(void* pData)
{
	GCFriendGroups* pkGCFriendGroups = (GCFriendGroups*)(pData);

	//EnCode friendGroupsSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCFriendGroups->friendGroupsSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendGroups
	if(10 < pkGCFriendGroups->friendGroupsSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCFriendGroups->friendGroupsSize; ++i)
	{
		if(EnCode__FriendGroupInfo(&(pkGCFriendGroups->friendGroups[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode groupMsgCount
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCFriendGroups->groupMsgCount), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCFriendGroups(void* pData)
{
	GCFriendGroups* pkGCFriendGroups = (GCFriendGroups*)(pData);

	//DeCode friendGroupsSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCFriendGroups->friendGroupsSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendGroups
	if(10 < pkGCFriendGroups->friendGroupsSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCFriendGroups->friendGroupsSize; ++i)
	{
		if(DeCode__FriendGroupInfo(&(pkGCFriendGroups->friendGroups[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode groupMsgCount
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCFriendGroups->groupMsgCount), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCFriendGroups);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerFriends(void* pData)
{
	GCPlayerFriends* pkGCPlayerFriends = (GCPlayerFriends*)(pData);

	//EnCode pageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerFriends->pageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerFriendsSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerFriends->playerFriendsSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerFriends
	if(5 < pkGCPlayerFriends->playerFriendsSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCPlayerFriends->playerFriendsSize; ++i)
	{
		if(EnCode__FriendInfo(&(pkGCPlayerFriends->playerFriends[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerFriends(void* pData)
{
	GCPlayerFriends* pkGCPlayerFriends = (GCPlayerFriends*)(pData);

	//DeCode pageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerFriends->pageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerFriendsSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerFriends->playerFriendsSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerFriends
	if(5 < pkGCPlayerFriends->playerFriendsSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCPlayerFriends->playerFriendsSize; ++i)
	{
		if(DeCode__FriendInfo(&(pkGCPlayerFriends->playerFriends[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCPlayerFriends);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCFriendInfoUpdate(void* pData)
{
	GCFriendInfoUpdate* pkGCFriendInfoUpdate = (GCFriendInfoUpdate*)(pData);

	//EnCode friendInfo
	if(EnCode__FriendInfo(&(pkGCFriendInfoUpdate->friendInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCFriendInfoUpdate(void* pData)
{
	GCFriendInfoUpdate* pkGCFriendInfoUpdate = (GCFriendInfoUpdate*)(pData);

	//DeCode friendInfo
	if(DeCode__FriendInfo(&(pkGCFriendInfoUpdate->friendInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCFriendInfoUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddFriendReq(void* pData)
{
	GCAddFriendReq* pkGCAddFriendReq = (GCAddFriendReq*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCAddFriendReq->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode reqPlayerNameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddFriendReq->reqPlayerNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode reqPlayerName
	if(MAX_NAME_SIZE < pkGCAddFriendReq->reqPlayerNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddFriendReq->reqPlayerNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCAddFriendReq->reqPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAddFriendReq->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCAddFriendReq->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerSex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCAddFriendReq->playerSex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddFriendReq(void* pData)
{
	GCAddFriendReq* pkGCAddFriendReq = (GCAddFriendReq*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCAddFriendReq->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode reqPlayerNameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddFriendReq->reqPlayerNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode reqPlayerName
	if(MAX_NAME_SIZE < pkGCAddFriendReq->reqPlayerNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddFriendReq->reqPlayerNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCAddFriendReq->reqPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAddFriendReq->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCAddFriendReq->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerSex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCAddFriendReq->playerSex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddFriendReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddFriendResponse(void* pData)
{
	CGAddFriendResponse* pkCGAddFriendResponse = (CGAddFriendResponse*)(pData);

	//EnCode reqPlayerID
	if(EnCode__PlayerID(&(pkCGAddFriendResponse->reqPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode invitePlayerNameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddFriendResponse->invitePlayerNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode invitePlayerName
	if(MAX_NAME_SIZE < pkCGAddFriendResponse->invitePlayerNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddFriendResponse->invitePlayerNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGAddFriendResponse->invitePlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isAgree
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGAddFriendResponse->isAgree), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isAddReqer
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGAddFriendResponse->isAddReqer), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddFriendResponse(void* pData)
{
	CGAddFriendResponse* pkCGAddFriendResponse = (CGAddFriendResponse*)(pData);

	//DeCode reqPlayerID
	if(DeCode__PlayerID(&(pkCGAddFriendResponse->reqPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode invitePlayerNameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddFriendResponse->invitePlayerNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode invitePlayerName
	if(MAX_NAME_SIZE < pkCGAddFriendResponse->invitePlayerNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddFriendResponse->invitePlayerNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGAddFriendResponse->invitePlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isAgree
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGAddFriendResponse->isAgree), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isAddReqer
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGAddFriendResponse->isAddReqer), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddFriendResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddFriendResponse(void* pData)
{
	GCAddFriendResponse* pkGCAddFriendResponse = (GCAddFriendResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCAddFriendResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isAgree
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCAddFriendResponse->isAgree), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isAddReqer
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCAddFriendResponse->isAddReqer), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isSynAddFriend
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCAddFriendResponse->isSynAddFriend), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendInfo
	if(EnCode__FriendInfo(&(pkGCAddFriendResponse->friendInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddFriendResponse(void* pData)
{
	GCAddFriendResponse* pkGCAddFriendResponse = (GCAddFriendResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCAddFriendResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isAgree
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCAddFriendResponse->isAgree), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isAddReqer
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCAddFriendResponse->isAddReqer), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isSynAddFriend
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCAddFriendResponse->isSynAddFriend), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendInfo
	if(DeCode__FriendInfo(&(pkGCAddFriendResponse->friendInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddFriendResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDelFriendReq(void* pData)
{
	CGDelFriendReq* pkCGDelFriendReq = (CGDelFriendReq*)(pData);

	//EnCode friendID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDelFriendReq->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDelFriendReq(void* pData)
{
	CGDelFriendReq* pkCGDelFriendReq = (CGDelFriendReq*)(pData);

	//DeCode friendID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDelFriendReq->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDelFriendReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDelFriendResponse(void* pData)
{
	GCDelFriendResponse* pkGCDelFriendResponse = (GCDelFriendResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDelFriendResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDelFriendResponse->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDelFriendResponse(void* pData)
{
	GCDelFriendResponse* pkGCDelFriendResponse = (GCDelFriendResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDelFriendResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDelFriendResponse->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDelFriendResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGCreateFriendGroupReq(void* pData)
{
	CGCreateFriendGroupReq* pkCGCreateFriendGroupReq = (CGCreateFriendGroupReq*)(pData);

	//EnCode groupNameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCreateFriendGroupReq->groupNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupName
	if(MAX_NAME_SIZE < pkCGCreateFriendGroupReq->groupNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateFriendGroupReq->groupNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGCreateFriendGroupReq->groupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGCreateFriendGroupReq(void* pData)
{
	CGCreateFriendGroupReq* pkCGCreateFriendGroupReq = (CGCreateFriendGroupReq*)(pData);

	//DeCode groupNameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCreateFriendGroupReq->groupNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupName
	if(MAX_NAME_SIZE < pkCGCreateFriendGroupReq->groupNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateFriendGroupReq->groupNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGCreateFriendGroupReq->groupName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGCreateFriendGroupReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCCreateFriendGroupResponse(void* pData)
{
	GCCreateFriendGroupResponse* pkGCCreateFriendGroupResponse = (GCCreateFriendGroupResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCCreateFriendGroupResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newFriendGroup
	if(EnCode__FriendGroupInfo(&(pkGCCreateFriendGroupResponse->newFriendGroup)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCCreateFriendGroupResponse(void* pData)
{
	GCCreateFriendGroupResponse* pkGCCreateFriendGroupResponse = (GCCreateFriendGroupResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCCreateFriendGroupResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newFriendGroup
	if(DeCode__FriendGroupInfo(&(pkGCCreateFriendGroupResponse->newFriendGroup)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCCreateFriendGroupResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChgFriendGroupNameReq(void* pData)
{
	CGChgFriendGroupNameReq* pkCGChgFriendGroupNameReq = (CGChgFriendGroupNameReq*)(pData);

	//EnCode toupdateFriendGroup
	if(EnCode__FriendGroupInfo(&(pkCGChgFriendGroupNameReq->toupdateFriendGroup)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChgFriendGroupNameReq(void* pData)
{
	CGChgFriendGroupNameReq* pkCGChgFriendGroupNameReq = (CGChgFriendGroupNameReq*)(pData);

	//DeCode toupdateFriendGroup
	if(DeCode__FriendGroupInfo(&(pkCGChgFriendGroupNameReq->toupdateFriendGroup)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChgFriendGroupNameReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChgFriendGroupNameResponse(void* pData)
{
	GCChgFriendGroupNameResponse* pkGCChgFriendGroupNameResponse = (GCChgFriendGroupNameResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCChgFriendGroupNameResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode updatedFriendGroup
	if(EnCode__FriendGroupInfo(&(pkGCChgFriendGroupNameResponse->updatedFriendGroup)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChgFriendGroupNameResponse(void* pData)
{
	GCChgFriendGroupNameResponse* pkGCChgFriendGroupNameResponse = (GCChgFriendGroupNameResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCChgFriendGroupNameResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode updatedFriendGroup
	if(DeCode__FriendGroupInfo(&(pkGCChgFriendGroupNameResponse->updatedFriendGroup)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChgFriendGroupNameResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDelFriendGroupReq(void* pData)
{
	CGDelFriendGroupReq* pkCGDelFriendGroupReq = (CGDelFriendGroupReq*)(pData);

	//EnCode groupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDelFriendGroupReq->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDelFriendGroupReq(void* pData)
{
	CGDelFriendGroupReq* pkCGDelFriendGroupReq = (CGDelFriendGroupReq*)(pData);

	//DeCode groupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDelFriendGroupReq->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDelFriendGroupReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDelFriendGroupResponse(void* pData)
{
	GCDelFriendGroupResponse* pkGCDelFriendGroupResponse = (GCDelFriendGroupResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDelFriendGroupResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDelFriendGroupResponse->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDelFriendGroupResponse(void* pData)
{
	GCDelFriendGroupResponse* pkGCDelFriendGroupResponse = (GCDelFriendGroupResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDelFriendGroupResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDelFriendGroupResponse->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDelFriendGroupResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGMoveFriendGroupReq(void* pData)
{
	CGMoveFriendGroupReq* pkCGMoveFriendGroupReq = (CGMoveFriendGroupReq*)(pData);

	//EnCode orgGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGMoveFriendGroupReq->orgGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGMoveFriendGroupReq->newGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGMoveFriendGroupReq->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGMoveFriendGroupReq(void* pData)
{
	CGMoveFriendGroupReq* pkCGMoveFriendGroupReq = (CGMoveFriendGroupReq*)(pData);

	//DeCode orgGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGMoveFriendGroupReq->orgGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGMoveFriendGroupReq->newGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGMoveFriendGroupReq->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGMoveFriendGroupReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCMoveFriendGroupResponse(void* pData)
{
	GCMoveFriendGroupResponse* pkGCMoveFriendGroupResponse = (GCMoveFriendGroupResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCMoveFriendGroupResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode orgGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMoveFriendGroupResponse->orgGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMoveFriendGroupResponse->newGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCMoveFriendGroupResponse->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCMoveFriendGroupResponse(void* pData)
{
	GCMoveFriendGroupResponse* pkGCMoveFriendGroupResponse = (GCMoveFriendGroupResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCMoveFriendGroupResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode orgGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMoveFriendGroupResponse->orgGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMoveFriendGroupResponse->newGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCMoveFriendGroupResponse->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCMoveFriendGroupResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGFriendChat(void* pData)
{
	CGFriendChat* pkCGFriendChat = (CGFriendChat*)(pData);

	//EnCode friendChatType
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGFriendChat->friendChatType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode groupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGFriendChat->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode friendID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGFriendChat->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatContentSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGFriendChat->chatContentSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatContent
	if(256 < pkCGFriendChat->chatContentSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGFriendChat->chatContentSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGFriendChat->chatContent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGFriendChat(void* pData)
{
	CGFriendChat* pkCGFriendChat = (CGFriendChat*)(pData);

	//DeCode friendChatType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGFriendChat->friendChatType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode groupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGFriendChat->groupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode friendID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGFriendChat->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatContentSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGFriendChat->chatContentSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatContent
	if(256 < pkCGFriendChat->chatContentSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGFriendChat->chatContentSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGFriendChat->chatContent), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGFriendChat);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCFriendChat(void* pData)
{
	GCFriendChat* pkGCFriendChat = (GCFriendChat*)(pData);

	//EnCode friendChatType
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCFriendChat->friendChatType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chaterNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCFriendChat->chaterNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chaterName
	if(MAX_NAME_SIZE < pkGCFriendChat->chaterNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCFriendChat->chaterNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCFriendChat->chaterName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatContentSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCFriendChat->chatContentSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatContent
	if(256 < pkGCFriendChat->chatContentSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCFriendChat->chatContentSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCFriendChat->chatContent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Remanent
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCFriendChat->Remanent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCFriendChat(void* pData)
{
	GCFriendChat* pkGCFriendChat = (GCFriendChat*)(pData);

	//DeCode friendChatType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCFriendChat->friendChatType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chaterNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCFriendChat->chaterNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chaterName
	if(MAX_NAME_SIZE < pkGCFriendChat->chaterNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCFriendChat->chaterNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCFriendChat->chaterName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatContentSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCFriendChat->chatContentSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatContent
	if(256 < pkGCFriendChat->chatContentSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCFriendChat->chatContentSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCFriendChat->chatContent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Remanent
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCFriendChat->Remanent), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCFriendChat);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerBlacklist(void* pData)
{
	GCPlayerBlacklist* pkGCPlayerBlacklist = (GCPlayerBlacklist*)(pData);

	//EnCode pageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCPlayerBlacklist->pageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerBlacklistSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerBlacklist->playerBlacklistSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerBlacklist
	if(EnCode__BlackListItemInfo(&(pkGCPlayerBlacklist->playerBlacklist)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerBlacklist(void* pData)
{
	GCPlayerBlacklist* pkGCPlayerBlacklist = (GCPlayerBlacklist*)(pData);

	//DeCode pageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCPlayerBlacklist->pageID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerBlacklistSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerBlacklist->playerBlacklistSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerBlacklist
	if(DeCode__BlackListItemInfo(&(pkGCPlayerBlacklist->playerBlacklist)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerBlacklist);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddBlacklistReq(void* pData)
{
	CGAddBlacklistReq* pkCGAddBlacklistReq = (CGAddBlacklistReq*)(pData);

	//EnCode nickNameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddBlacklistReq->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nickName
	if(MAX_NAME_SIZE < pkCGAddBlacklistReq->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddBlacklistReq->nickNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGAddBlacklistReq->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddBlacklistReq(void* pData)
{
	CGAddBlacklistReq* pkCGAddBlacklistReq = (CGAddBlacklistReq*)(pData);

	//DeCode nickNameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddBlacklistReq->nickNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nickName
	if(MAX_NAME_SIZE < pkCGAddBlacklistReq->nickNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddBlacklistReq->nickNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGAddBlacklistReq->nickName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddBlacklistReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddBlacklistResponse(void* pData)
{
	GCAddBlacklistResponse* pkGCAddBlacklistResponse = (GCAddBlacklistResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCAddBlacklistResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode blacklistItemInfo
	if(EnCode__BlackListItemInfo(&(pkGCAddBlacklistResponse->blacklistItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddBlacklistResponse(void* pData)
{
	GCAddBlacklistResponse* pkGCAddBlacklistResponse = (GCAddBlacklistResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCAddBlacklistResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode blacklistItemInfo
	if(DeCode__BlackListItemInfo(&(pkGCAddBlacklistResponse->blacklistItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddBlacklistResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDelBlacklistReq(void* pData)
{
	CGDelBlacklistReq* pkCGDelBlacklistReq = (CGDelBlacklistReq*)(pData);

	//EnCode addBlackNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGDelBlacklistReq->addBlackNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode addBlackName
	if(MAX_NAME_SIZE < pkCGDelBlacklistReq->addBlackNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGDelBlacklistReq->addBlackNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGDelBlacklistReq->addBlackName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDelBlacklistReq(void* pData)
{
	CGDelBlacklistReq* pkCGDelBlacklistReq = (CGDelBlacklistReq*)(pData);

	//DeCode addBlackNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGDelBlacklistReq->addBlackNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode addBlackName
	if(MAX_NAME_SIZE < pkCGDelBlacklistReq->addBlackNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGDelBlacklistReq->addBlackNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGDelBlacklistReq->addBlackName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDelBlacklistReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDelBlacklistResponse(void* pData)
{
	GCDelBlacklistResponse* pkGCDelBlacklistResponse = (GCDelBlacklistResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDelBlacklistResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode delBlackNameLen
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDelBlacklistResponse->delBlackNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode delBlackName
	if(MAX_NAME_SIZE < pkGCDelBlacklistResponse->delBlackNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCDelBlacklistResponse->delBlackNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCDelBlacklistResponse->delBlackName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDelBlacklistResponse(void* pData)
{
	GCDelBlacklistResponse* pkGCDelBlacklistResponse = (GCDelBlacklistResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDelBlacklistResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode delBlackNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDelBlacklistResponse->delBlackNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode delBlackName
	if(MAX_NAME_SIZE < pkGCDelBlacklistResponse->delBlackNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCDelBlacklistResponse->delBlackNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCDelBlacklistResponse->delBlackName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDelBlacklistResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCFoeList(void* pData)
{
	GCFoeList* pkGCFoeList = (GCFoeList*)(pData);

	//EnCode playerFoeListSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCFoeList->playerFoeListSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerFoeList
	if(5 < pkGCFoeList->playerFoeListSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCFoeList->playerFoeListSize; ++i)
	{
		if(EnCode__FoeInfo(&(pkGCFoeList->playerFoeList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCFoeList(void* pData)
{
	GCFoeList* pkGCFoeList = (GCFoeList*)(pData);

	//DeCode playerFoeListSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCFoeList->playerFoeListSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerFoeList
	if(5 < pkGCFoeList->playerFoeListSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCFoeList->playerFoeListSize; ++i)
	{
		if(DeCode__FoeInfo(&(pkGCFoeList->playerFoeList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCFoeList);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCNewFoe(void* pData)
{
	GCNewFoe* pkGCNewFoe = (GCNewFoe*)(pData);

	//EnCode newFoe
	if(EnCode__FoeInfo(&(pkGCNewFoe->newFoe)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode orgFoeItemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCNewFoe->orgFoeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCNewFoe(void* pData)
{
	GCNewFoe* pkGCNewFoe = (GCNewFoe*)(pData);

	//DeCode newFoe
	if(DeCode__FoeInfo(&(pkGCNewFoe->newFoe)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode orgFoeItemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCNewFoe->orgFoeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCNewFoe);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDelFoeReq(void* pData)
{
	CGDelFoeReq* pkCGDelFoeReq = (CGDelFoeReq*)(pData);

	//EnCode foeItemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDelFoeReq->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDelFoeReq(void* pData)
{
	CGDelFoeReq* pkCGDelFoeReq = (CGDelFoeReq*)(pData);

	//DeCode foeItemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDelFoeReq->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDelFoeReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDelFoeResponse(void* pData)
{
	GCDelFoeResponse* pkGCDelFoeResponse = (GCDelFoeResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDelFoeResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode foeItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDelFoeResponse->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDelFoeResponse(void* pData)
{
	GCDelFoeResponse* pkGCDelFoeResponse = (GCDelFoeResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDelFoeResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode foeItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDelFoeResponse->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDelFoeResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGLockFoeReq(void* pData)
{
	CGLockFoeReq* pkCGLockFoeReq = (CGLockFoeReq*)(pData);

	//EnCode foeItemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGLockFoeReq->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bLock
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGLockFoeReq->bLock), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGLockFoeReq(void* pData)
{
	CGLockFoeReq* pkCGLockFoeReq = (CGLockFoeReq*)(pData);

	//DeCode foeItemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGLockFoeReq->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bLock
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGLockFoeReq->bLock), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGLockFoeReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCFoeInfoUpdate(void* pData)
{
	GCFoeInfoUpdate* pkGCFoeInfoUpdate = (GCFoeInfoUpdate*)(pData);

	//EnCode updateFoe
	if(EnCode__FoeInfo(&(pkGCFoeInfoUpdate->updateFoe)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCFoeInfoUpdate(void* pData)
{
	GCFoeInfoUpdate* pkGCFoeInfoUpdate = (GCFoeInfoUpdate*)(pData);

	//DeCode updateFoe
	if(DeCode__FoeInfo(&(pkGCFoeInfoUpdate->updateFoe)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCFoeInfoUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCLockFoeResponse(void* pData)
{
	GCLockFoeResponse* pkGCLockFoeResponse = (GCLockFoeResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCLockFoeResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bLock
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCLockFoeResponse->bLock), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode foeItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCLockFoeResponse->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCLockFoeResponse(void* pData)
{
	GCLockFoeResponse* pkGCLockFoeResponse = (GCLockFoeResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCLockFoeResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bLock
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCLockFoeResponse->bLock), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode foeItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCLockFoeResponse->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCLockFoeResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryFoeReq(void* pData)
{
	CGQueryFoeReq* pkCGQueryFoeReq = (CGQueryFoeReq*)(pData);

	//EnCode foeItemID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryFoeReq->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryFoeReq(void* pData)
{
	CGQueryFoeReq* pkCGQueryFoeReq = (CGQueryFoeReq*)(pData);

	//DeCode foeItemID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryFoeReq->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryFoeReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryFoeResponse(void* pData)
{
	GCQueryFoeResponse* pkGCQueryFoeResponse = (GCQueryFoeResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCQueryFoeResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode foeItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCQueryFoeResponse->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode foeDetailInfo
	if(EnCode__FoeDetailInfo(&(pkGCQueryFoeResponse->foeDetailInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryFoeResponse(void* pData)
{
	GCQueryFoeResponse* pkGCQueryFoeResponse = (GCQueryFoeResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCQueryFoeResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode foeItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCQueryFoeResponse->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode foeDetailInfo
	if(DeCode__FoeDetailInfo(&(pkGCQueryFoeResponse->foeDetailInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCQueryFoeResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGFriendToBlacklistReq(void* pData)
{
	CGFriendToBlacklistReq* pkCGFriendToBlacklistReq = (CGFriendToBlacklistReq*)(pData);

	//EnCode friendID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGFriendToBlacklistReq->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGFriendToBlacklistReq(void* pData)
{
	CGFriendToBlacklistReq* pkCGFriendToBlacklistReq = (CGFriendToBlacklistReq*)(pData);

	//DeCode friendID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGFriendToBlacklistReq->friendID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGFriendToBlacklistReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCFriendToBlacklistResponse(void* pData)
{
	GCFriendToBlacklistResponse* pkGCFriendToBlacklistResponse = (GCFriendToBlacklistResponse*)(pData);

	//EnCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCFriendToBlacklistResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode foeItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCFriendToBlacklistResponse->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode blacklistItemInfo
	if(EnCode__BlackListItemInfo(&(pkGCFriendToBlacklistResponse->blacklistItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCFriendToBlacklistResponse(void* pData)
{
	GCFriendToBlacklistResponse* pkGCFriendToBlacklistResponse = (GCFriendToBlacklistResponse*)(pData);

	//DeCode nErrcode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCFriendToBlacklistResponse->nErrcode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode foeItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCFriendToBlacklistResponse->foeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode blacklistItemInfo
	if(DeCode__BlackListItemInfo(&(pkGCFriendToBlacklistResponse->blacklistItemInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCFriendToBlacklistResponse);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRollItemFeedBack(void* pData)
{
	CGRollItemFeedBack* pkCGRollItemFeedBack = (CGRollItemFeedBack*)(pData);

	//EnCode rollID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRollItemFeedBack->rollID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rollItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRollItemFeedBack->rollItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode agree
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGRollItemFeedBack->agree), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRollItemFeedBack(void* pData)
{
	CGRollItemFeedBack* pkCGRollItemFeedBack = (CGRollItemFeedBack*)(pData);

	//DeCode rollID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRollItemFeedBack->rollID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rollItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRollItemFeedBack->rollItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode agree
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGRollItemFeedBack->agree), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRollItemFeedBack);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRollItemResult(void* pData)
{
	GCRollItemResult* pkGCRollItemResult = (GCRollItemResult*)(pData);

	//EnCode rollID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRollItemResult->rollID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rollItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRollItemResult->rollItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NameSize
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRollItemResult->NameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rollPlayerName
	if(MAX_NAME_SIZE < pkGCRollItemResult->NameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRollItemResult->NameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCRollItemResult->rollPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRollItemResult(void* pData)
{
	GCRollItemResult* pkGCRollItemResult = (GCRollItemResult*)(pData);

	//DeCode rollID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRollItemResult->rollID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rollItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRollItemResult->rollItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NameSize
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRollItemResult->NameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rollPlayerName
	if(MAX_NAME_SIZE < pkGCRollItemResult->NameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRollItemResult->NameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRollItemResult->rollPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRollItemResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCReqAgreeRollItem(void* pData)
{
	GCReqAgreeRollItem* pkGCReqAgreeRollItem = (GCReqAgreeRollItem*)(pData);

	//EnCode rollID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqAgreeRollItem->rollID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rollItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqAgreeRollItem->rollItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCReqAgreeRollItem(void* pData)
{
	GCReqAgreeRollItem* pkGCReqAgreeRollItem = (GCReqAgreeRollItem*)(pData);

	//DeCode rollID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqAgreeRollItem->rollID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rollItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqAgreeRollItem->rollItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCReqAgreeRollItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTeamMemberPickItem(void* pData)
{
	GCTeamMemberPickItem* pkGCTeamMemberPickItem = (GCTeamMemberPickItem*)(pData);

	//EnCode getPlayerID
	if(EnCode__PlayerID(&(pkGCTeamMemberPickItem->getPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode rollItemTypeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTeamMemberPickItem->rollItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTeamMemberPickItem(void* pData)
{
	GCTeamMemberPickItem* pkGCTeamMemberPickItem = (GCTeamMemberPickItem*)(pData);

	//DeCode getPlayerID
	if(DeCode__PlayerID(&(pkGCTeamMemberPickItem->getPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode rollItemTypeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTeamMemberPickItem->rollItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTeamMemberPickItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSendTradeReq(void* pData)
{
	CGSendTradeReq* pkCGSendTradeReq = (CGSendTradeReq*)(pData);

	//EnCode targetPlayerID
	if(EnCode__PlayerID(&(pkCGSendTradeReq->targetPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSendTradeReq(void* pData)
{
	CGSendTradeReq* pkCGSendTradeReq = (CGSendTradeReq*)(pData);

	//DeCode targetPlayerID
	if(DeCode__PlayerID(&(pkCGSendTradeReq->targetPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSendTradeReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSendTradeReqError(void* pData)
{
	GCSendTradeReqError* pkGCSendTradeReqError = (GCSendTradeReqError*)(pData);

	//EnCode tResult
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSendTradeReqError->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetPlayerID
	if(EnCode__PlayerID(&(pkGCSendTradeReqError->targetPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSendTradeReqError(void* pData)
{
	GCSendTradeReqError* pkGCSendTradeReqError = (GCSendTradeReqError*)(pData);

	//DeCode tResult
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSendTradeReqError->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetPlayerID
	if(DeCode__PlayerID(&(pkGCSendTradeReqError->targetPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSendTradeReqError);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRecvTradeReq(void* pData)
{
	GCRecvTradeReq* pkGCRecvTradeReq = (GCRecvTradeReq*)(pData);

	//EnCode sendReqPlayerID
	if(EnCode__PlayerID(&(pkGCRecvTradeReq->sendReqPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRecvTradeReq->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sendPlayerName
	if(MAX_NAME_SIZE < pkGCRecvTradeReq->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRecvTradeReq->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCRecvTradeReq->sendPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRecvTradeReq(void* pData)
{
	GCRecvTradeReq* pkGCRecvTradeReq = (GCRecvTradeReq*)(pData);

	//DeCode sendReqPlayerID
	if(DeCode__PlayerID(&(pkGCRecvTradeReq->sendReqPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRecvTradeReq->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sendPlayerName
	if(MAX_NAME_SIZE < pkGCRecvTradeReq->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRecvTradeReq->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRecvTradeReq->sendPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRecvTradeReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSendTradeReqResult(void* pData)
{
	CGSendTradeReqResult* pkCGSendTradeReqResult = (CGSendTradeReqResult*)(pData);

	//EnCode tResult
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGSendTradeReqResult->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSendTradeReqResult(void* pData)
{
	CGSendTradeReqResult* pkCGSendTradeReqResult = (CGSendTradeReqResult*)(pData);

	//DeCode tResult
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGSendTradeReqResult->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSendTradeReqResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSendTradeReqResult(void* pData)
{
	GCSendTradeReqResult* pkGCSendTradeReqResult = (GCSendTradeReqResult*)(pData);

	//EnCode tResult
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSendTradeReqResult->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetPlayerId
	if(EnCode__PlayerID(&(pkGCSendTradeReqResult->targetPlayerId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSendTradeReqResult(void* pData)
{
	GCSendTradeReqResult* pkGCSendTradeReqResult = (GCSendTradeReqResult*)(pData);

	//DeCode tResult
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSendTradeReqResult->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetPlayerId
	if(DeCode__PlayerID(&(pkGCSendTradeReqResult->targetPlayerId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSendTradeReqResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddOrDeleteItem(void* pData)
{
	CGAddOrDeleteItem* pkCGAddOrDeleteItem = (CGAddOrDeleteItem*)(pData);

	//EnCode tResult
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddOrDeleteItem->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddOrDeleteItem->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddOrDeleteItem(void* pData)
{
	CGAddOrDeleteItem* pkCGAddOrDeleteItem = (CGAddOrDeleteItem*)(pData);

	//DeCode tResult
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddOrDeleteItem->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddOrDeleteItem->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddOrDeleteItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddOrDeleteItem(void* pData)
{
	GCAddOrDeleteItem* pkGCAddOrDeleteItem = (GCAddOrDeleteItem*)(pData);

	//EnCode operatorID
	if(EnCode__PlayerID(&(pkGCAddOrDeleteItem->operatorID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode tOperateType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddOrDeleteItem->tOperateType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tOperateResult
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddOrDeleteItem->tOperateResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddOrDeleteItem->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddOrDeleteItem->uiItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode usWearPoint
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddOrDeleteItem->usWearPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ucCount
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCAddOrDeleteItem->ucCount), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddOrDeleteItem(void* pData)
{
	GCAddOrDeleteItem* pkGCAddOrDeleteItem = (GCAddOrDeleteItem*)(pData);

	//DeCode operatorID
	if(DeCode__PlayerID(&(pkGCAddOrDeleteItem->operatorID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode tOperateType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddOrDeleteItem->tOperateType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tOperateResult
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddOrDeleteItem->tOperateResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddOrDeleteItem->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddOrDeleteItem->uiItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode usWearPoint
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddOrDeleteItem->usWearPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ucCount
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCAddOrDeleteItem->ucCount), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddOrDeleteItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSetTradeMoney(void* pData)
{
	CGSetTradeMoney* pkCGSetTradeMoney = (CGSetTradeMoney*)(pData);

	//EnCode uiMoney
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGSetTradeMoney->uiMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSetTradeMoney(void* pData)
{
	CGSetTradeMoney* pkCGSetTradeMoney = (CGSetTradeMoney*)(pData);

	//DeCode uiMoney
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGSetTradeMoney->uiMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSetTradeMoney);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSetTradeMoney(void* pData)
{
	GCSetTradeMoney* pkGCSetTradeMoney = (GCSetTradeMoney*)(pData);

	//EnCode operatorID
	if(EnCode__PlayerID(&(pkGCSetTradeMoney->operatorID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMoney
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSetTradeMoney->uiMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bOK
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCSetTradeMoney->bOK), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSetTradeMoney(void* pData)
{
	GCSetTradeMoney* pkGCSetTradeMoney = (GCSetTradeMoney*)(pData);

	//DeCode operatorID
	if(DeCode__PlayerID(&(pkGCSetTradeMoney->operatorID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMoney
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSetTradeMoney->uiMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bOK
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCSetTradeMoney->bOK), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSetTradeMoney);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGCancelTrade(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGCancelTrade(void* pData)
{
	(pData);
	return sizeof(CGCancelTrade);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCCancelTrade(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCCancelTrade(void* pData)
{
	(pData);
	return sizeof(GCCancelTrade);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGLockTrade(void* pData)
{
	CGLockTrade* pkCGLockTrade = (CGLockTrade*)(pData);

	//EnCode tType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGLockTrade->tType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGLockTrade(void* pData)
{
	CGLockTrade* pkCGLockTrade = (CGLockTrade*)(pData);

	//DeCode tType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGLockTrade->tType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGLockTrade);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCLockTrade(void* pData)
{
	GCLockTrade* pkGCLockTrade = (GCLockTrade*)(pData);

	//EnCode tType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCLockTrade->tType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode operatePlayerID
	if(EnCode__PlayerID(&(pkGCLockTrade->operatePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCLockTrade(void* pData)
{
	GCLockTrade* pkGCLockTrade = (GCLockTrade*)(pData);

	//DeCode tType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCLockTrade->tType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode operatePlayerID
	if(DeCode__PlayerID(&(pkGCLockTrade->operatePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCLockTrade);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGConfirmTrade(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGConfirmTrade(void* pData)
{
	(pData);
	return sizeof(CGConfirmTrade);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCConfirmTrade(void* pData)
{
	GCConfirmTrade* pkGCConfirmTrade = (GCConfirmTrade*)(pData);

	//EnCode tResult
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCConfirmTrade->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode operatePlayerID
	if(EnCode__PlayerID(&(pkGCConfirmTrade->operatePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCConfirmTrade(void* pData)
{
	GCConfirmTrade* pkGCConfirmTrade = (GCConfirmTrade*)(pData);

	//DeCode tResult
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCConfirmTrade->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode operatePlayerID
	if(DeCode__PlayerID(&(pkGCConfirmTrade->operatePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCConfirmTrade);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangeReqSwitch(void* pData)
{
	CGChangeReqSwitch* pkCGChangeReqSwitch = (CGChangeReqSwitch*)(pData);

	//EnCode bReq
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGChangeReqSwitch->bReq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangeReqSwitch(void* pData)
{
	CGChangeReqSwitch* pkCGChangeReqSwitch = (CGChangeReqSwitch*)(pData);

	//DeCode bReq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGChangeReqSwitch->bReq), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangeReqSwitch);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeReqSwitch(void* pData)
{
	GCChangeReqSwitch* pkGCChangeReqSwitch = (GCChangeReqSwitch*)(pData);

	//EnCode bReq
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCChangeReqSwitch->bReq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nErrorCode
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCChangeReqSwitch->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeReqSwitch(void* pData)
{
	GCChangeReqSwitch* pkGCChangeReqSwitch = (GCChangeReqSwitch*)(pData);

	//DeCode bReq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCChangeReqSwitch->bReq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nErrorCode
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCChangeReqSwitch->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeReqSwitch);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangeTeamRollLevel(void* pData)
{
	CGChangeTeamRollLevel* pkCGChangeTeamRollLevel = (CGChangeTeamRollLevel*)(pData);

	//EnCode ucRollLevel
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGChangeTeamRollLevel->ucRollLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode teamID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChangeTeamRollLevel->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangeTeamRollLevel(void* pData)
{
	CGChangeTeamRollLevel* pkCGChangeTeamRollLevel = (CGChangeTeamRollLevel*)(pData);

	//DeCode ucRollLevel
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGChangeTeamRollLevel->ucRollLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode teamID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChangeTeamRollLevel->teamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangeTeamRollLevel);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDelFoeInfo(void* pData)
{
	CGDelFoeInfo* pkCGDelFoeInfo = (CGDelFoeInfo*)(pData);

	//EnCode foeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDelFoeInfo->foeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDelFoeInfo(void* pData)
{
	CGDelFoeInfo* pkCGDelFoeInfo = (CGDelFoeInfo*)(pData);

	//DeCode foeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDelFoeInfo->foeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDelFoeInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeTeamRollLevel(void* pData)
{
	GCChangeTeamRollLevel* pkGCChangeTeamRollLevel = (GCChangeTeamRollLevel*)(pData);

	//EnCode ucRollLevel
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCChangeTeamRollLevel->ucRollLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nErrorCode
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCChangeTeamRollLevel->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeTeamRollLevel(void* pData)
{
	GCChangeTeamRollLevel* pkGCChangeTeamRollLevel = (GCChangeTeamRollLevel*)(pData);

	//DeCode ucRollLevel
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCChangeTeamRollLevel->ucRollLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nErrorCode
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCChangeTeamRollLevel->nErrorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeTeamRollLevel);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRollItemNum(void* pData)
{
	GCRollItemNum* pkGCRollItemNum = (GCRollItemNum*)(pData);

	//EnCode NameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRollItemNum->NameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rollPlayerName
	if(MAX_NAME_SIZE < pkGCRollItemNum->NameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRollItemNum->NameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCRollItemNum->rollPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rollNum
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRollItemNum->rollNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rollID
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRollItemNum->rollID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRollItemNum(void* pData)
{
	GCRollItemNum* pkGCRollItemNum = (GCRollItemNum*)(pData);

	//DeCode NameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRollItemNum->NameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rollPlayerName
	if(MAX_NAME_SIZE < pkGCRollItemNum->NameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRollItemNum->NameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRollItemNum->rollPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rollNum
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRollItemNum->rollNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rollID
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRollItemNum->rollID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRollItemNum);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeGroupMsgNum(void* pData)
{
	GCChangeGroupMsgNum* pkGCChangeGroupMsgNum = (GCChangeGroupMsgNum*)(pData);

	//EnCode MsgNum
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCChangeGroupMsgNum->MsgNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeGroupMsgNum(void* pData)
{
	GCChangeGroupMsgNum* pkGCChangeGroupMsgNum = (GCChangeGroupMsgNum*)(pData);

	//DeCode MsgNum
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCChangeGroupMsgNum->MsgNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeGroupMsgNum);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCLeaveView(void* pData)
{
	GCLeaveView* pkGCLeaveView = (GCLeaveView*)(pData);

	//EnCode leavePlayerID
	if(EnCode__PlayerID(&(pkGCLeaveView->leavePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode leaveMonsterID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCLeaveView->leaveMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCLeaveView(void* pData)
{
	GCLeaveView* pkGCLeaveView = (GCLeaveView*)(pData);

	//DeCode leavePlayerID
	if(DeCode__PlayerID(&(pkGCLeaveView->leavePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode leaveMonsterID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCLeaveView->leaveMonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCLeaveView);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRepairWearByItem(void* pData)
{
	CGRepairWearByItem* pkCGRepairWearByItem = (CGRepairWearByItem*)(pData);

	//EnCode itemRepairman
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRepairWearByItem->itemRepairman), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode repairTarget
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRepairWearByItem->repairTarget), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRepairWearByItem(void* pData)
{
	CGRepairWearByItem* pkCGRepairWearByItem = (CGRepairWearByItem*)(pData);

	//DeCode itemRepairman
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRepairWearByItem->itemRepairman), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode repairTarget
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRepairWearByItem->repairTarget), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRepairWearByItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryRepairItemCost(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryRepairItemCost(void* pData)
{
	(pData);
	return sizeof(CGQueryRepairItemCost);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryRepairItemCostRst(void* pData)
{
	GCQueryRepairItemCostRst* pkGCQueryRepairItemCostRst = (GCQueryRepairItemCostRst*)(pData);

	//EnCode costMoney
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCQueryRepairItemCostRst->costMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryRepairItemCostRst(void* pData)
{
	GCQueryRepairItemCostRst* pkGCQueryRepairItemCostRst = (GCQueryRepairItemCostRst*)(pData);

	//DeCode costMoney
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCQueryRepairItemCostRst->costMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCQueryRepairItemCostRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGIrcQueryPlayerInfo(void* pData)
{
	CGIrcQueryPlayerInfo* pkCGIrcQueryPlayerInfo = (CGIrcQueryPlayerInfo*)(pData);

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkCGIrcQueryPlayerInfo->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerName
	if(MAX_NAME_SIZE < pkCGIrcQueryPlayerInfo->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGIrcQueryPlayerInfo->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGIrcQueryPlayerInfo->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGIrcQueryPlayerInfo(void* pData)
{
	CGIrcQueryPlayerInfo* pkCGIrcQueryPlayerInfo = (CGIrcQueryPlayerInfo*)(pData);

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkCGIrcQueryPlayerInfo->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerName
	if(MAX_NAME_SIZE < pkCGIrcQueryPlayerInfo->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGIrcQueryPlayerInfo->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGIrcQueryPlayerInfo->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGIrcQueryPlayerInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCIrcQueryPlayerInfoRst(void* pData)
{
	GCIrcQueryPlayerInfoRst* pkGCIrcQueryPlayerInfoRst = (GCIrcQueryPlayerInfoRst*)(pData);

	//EnCode errorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCIrcQueryPlayerInfoRst->errorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameSize
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCIrcQueryPlayerInfoRst->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerName
	if(MAX_NAME_SIZE < pkGCIrcQueryPlayerInfoRst->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCIrcQueryPlayerInfoRst->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCIrcQueryPlayerInfoRst->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerSex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCIrcQueryPlayerInfoRst->playerSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mapID
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCIrcQueryPlayerInfoRst->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerLevel
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCIrcQueryPlayerInfoRst->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerClass
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCIrcQueryPlayerInfoRst->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerRace
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCIrcQueryPlayerInfoRst->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCIrcQueryPlayerInfoRst->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildName
	if(MAX_NAME_SIZE < pkGCIrcQueryPlayerInfoRst->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCIrcQueryPlayerInfoRst->GuildNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCIrcQueryPlayerInfoRst->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereNameLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCIrcQueryPlayerInfoRst->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FereName
	if(MAX_NAME_SIZE < pkGCIrcQueryPlayerInfoRst->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCIrcQueryPlayerInfoRst->FereNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCIrcQueryPlayerInfoRst->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCIrcQueryPlayerInfoRst(void* pData)
{
	GCIrcQueryPlayerInfoRst* pkGCIrcQueryPlayerInfoRst = (GCIrcQueryPlayerInfoRst*)(pData);

	//DeCode errorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCIrcQueryPlayerInfoRst->errorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameSize
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCIrcQueryPlayerInfoRst->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerName
	if(MAX_NAME_SIZE < pkGCIrcQueryPlayerInfoRst->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCIrcQueryPlayerInfoRst->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCIrcQueryPlayerInfoRst->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerSex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCIrcQueryPlayerInfoRst->playerSex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mapID
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCIrcQueryPlayerInfoRst->mapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCIrcQueryPlayerInfoRst->playerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerClass
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCIrcQueryPlayerInfoRst->playerClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerRace
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCIrcQueryPlayerInfoRst->playerRace), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCIrcQueryPlayerInfoRst->GuildNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildName
	if(MAX_NAME_SIZE < pkGCIrcQueryPlayerInfoRst->GuildNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCIrcQueryPlayerInfoRst->GuildNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCIrcQueryPlayerInfoRst->GuildName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCIrcQueryPlayerInfoRst->FereNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FereName
	if(MAX_NAME_SIZE < pkGCIrcQueryPlayerInfoRst->FereNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCIrcQueryPlayerInfoRst->FereNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCIrcQueryPlayerInfoRst->FereName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCIrcQueryPlayerInfoRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTargetPlaySound(void* pData)
{
	GCTargetPlaySound* pkGCTargetPlaySound = (GCTargetPlaySound*)(pData);

	//EnCode pathSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCTargetPlaySound->pathSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode soundPath
	if(256 < pkGCTargetPlaySound->pathSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCTargetPlaySound->pathSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCTargetPlaySound->soundPath), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetID
	if(EnCode__PlayerID(&(pkGCTargetPlaySound->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTargetPlaySound(void* pData)
{
	GCTargetPlaySound* pkGCTargetPlaySound = (GCTargetPlaySound*)(pData);

	//DeCode pathSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCTargetPlaySound->pathSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode soundPath
	if(256 < pkGCTargetPlaySound->pathSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCTargetPlaySound->pathSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCTargetPlaySound->soundPath), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetID
	if(DeCode__PlayerID(&(pkGCTargetPlaySound->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTargetPlaySound);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTargetChangeSize(void* pData)
{
	GCTargetChangeSize* pkGCTargetChangeSize = (GCTargetChangeSize*)(pData);

	//EnCode newSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCTargetChangeSize->newSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetID
	if(EnCode__PlayerID(&(pkGCTargetChangeSize->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode arg
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTargetChangeSize->arg), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTargetChangeSize(void* pData)
{
	GCTargetChangeSize* pkGCTargetChangeSize = (GCTargetChangeSize*)(pData);

	//DeCode newSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCTargetChangeSize->newSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetID
	if(DeCode__PlayerID(&(pkGCTargetChangeSize->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode arg
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTargetChangeSize->arg), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTargetChangeSize);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCInfoUpdate(void* pData)
{
	GCInfoUpdate* pkGCInfoUpdate = (GCInfoUpdate*)(pData);

	//EnCode useSkillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->useSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerPhyExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerPhyExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerMagExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerMagExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerHP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerMP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerSpExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerSpExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerSpSkillExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerSpSkillExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerSpPower
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCInfoUpdate->nAtkPlayerSpPower), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCInfoUpdate(void* pData)
{
	GCInfoUpdate* pkGCInfoUpdate = (GCInfoUpdate*)(pData);

	//DeCode useSkillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->useSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerPhyExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerPhyExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerMagExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerMagExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerHP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerMP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerSpExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerSpExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerSpSkillExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerSpSkillExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerSpPower
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCInfoUpdate->nAtkPlayerSpPower), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCInfoUpdate);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRepairWearByItem(void* pData)
{
	GCRepairWearByItem* pkGCRepairWearByItem = (GCRepairWearByItem*)(pData);

	//EnCode itemRepairman
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByItem->itemRepairman), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode repairTarget
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByItem->repairTarget), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tResult
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByItem->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newWear
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByItem->newWear), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiRepairMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByItem->uiRepairMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRepairWearByItem(void* pData)
{
	GCRepairWearByItem* pkGCRepairWearByItem = (GCRepairWearByItem*)(pData);

	//DeCode itemRepairman
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByItem->itemRepairman), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode repairTarget
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByItem->repairTarget), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tResult
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByItem->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newWear
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByItem->newWear), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiRepairMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByItem->uiRepairMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRepairWearByItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCNotifyItemWearChange(void* pData)
{
	GCNotifyItemWearChange* pkGCNotifyItemWearChange = (GCNotifyItemWearChange*)(pData);

	//EnCode itemSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCNotifyItemWearChange->itemSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode items
	if(32 < pkGCNotifyItemWearChange->itemSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCNotifyItemWearChange->itemSize; ++i)
	{
		if(EnCode__WearChangedItems(&(pkGCNotifyItemWearChange->items[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCNotifyItemWearChange(void* pData)
{
	GCNotifyItemWearChange* pkGCNotifyItemWearChange = (GCNotifyItemWearChange*)(pData);

	//DeCode itemSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCNotifyItemWearChange->itemSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode items
	if(32 < pkGCNotifyItemWearChange->itemSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCNotifyItemWearChange->itemSize; ++i)
	{
		if(DeCode__WearChangedItems(&(pkGCNotifyItemWearChange->items[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCNotifyItemWearChange);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGTimedWearIsZero(void* pData)
{
	CGTimedWearIsZero* pkCGTimedWearIsZero = (CGTimedWearIsZero*)(pData);

	//EnCode uiID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGTimedWearIsZero->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGTimedWearIsZero(void* pData)
{
	CGTimedWearIsZero* pkCGTimedWearIsZero = (CGTimedWearIsZero*)(pData);

	//DeCode uiID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGTimedWearIsZero->uiID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGTimedWearIsZero);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUpgradeSkill(void* pData)
{
	CGUpgradeSkill* pkCGUpgradeSkill = (CGUpgradeSkill*)(pData);

	//EnCode skillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUpgradeSkill->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUpgradeSkill(void* pData)
{
	CGUpgradeSkill* pkCGUpgradeSkill = (CGUpgradeSkill*)(pData);

	//DeCode skillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUpgradeSkill->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGUpgradeSkill);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddSpSkillReq(void* pData)
{
	CGAddSpSkillReq* pkCGAddSpSkillReq = (CGAddSpSkillReq*)(pData);

	//EnCode skillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddSpSkillReq->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddSpSkillReq(void* pData)
{
	CGAddSpSkillReq* pkCGAddSpSkillReq = (CGAddSpSkillReq*)(pData);

	//DeCode skillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddSpSkillReq->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddSpSkillReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddSpSkillResult(void* pData)
{
	GCAddSpSkillResult* pkGCAddSpSkillResult = (GCAddSpSkillResult*)(pData);

	//EnCode skillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddSpSkillResult->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bSuccess
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCAddSpSkillResult->bSuccess), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nSkillDataIndex
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCAddSpSkillResult->nSkillDataIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddSpSkillResult(void* pData)
{
	GCAddSpSkillResult* pkGCAddSpSkillResult = (GCAddSpSkillResult*)(pData);

	//DeCode skillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddSpSkillResult->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bSuccess
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCAddSpSkillResult->bSuccess), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nSkillDataIndex
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCAddSpSkillResult->nSkillDataIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddSpSkillResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCThreatList(void* pData)
{
	GCThreatList* pkGCThreatList = (GCThreatList*)(pData);

	//EnCode threatList
	if(EnCode__ThreatList(&(pkGCThreatList->threatList)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCThreatList(void* pData)
{
	GCThreatList* pkGCThreatList = (GCThreatList*)(pData);

	//DeCode threatList
	if(DeCode__ThreatList(&(pkGCThreatList->threatList)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCThreatList);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUpgradeSkillResult(void* pData)
{
	GCUpgradeSkillResult* pkGCUpgradeSkillResult = (GCUpgradeSkillResult*)(pData);

	//EnCode errorCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUpgradeSkillResult->errorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode oldSkillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUpgradeSkillResult->oldSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newSkillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUpgradeSkillResult->newSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode phySkillPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUpgradeSkillResult->phySkillPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode magSkillPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUpgradeSkillResult->magSkillPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUpgradeSkillResult(void* pData)
{
	GCUpgradeSkillResult* pkGCUpgradeSkillResult = (GCUpgradeSkillResult*)(pData);

	//DeCode errorCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUpgradeSkillResult->errorCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode oldSkillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUpgradeSkillResult->oldSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newSkillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUpgradeSkillResult->newSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode phySkillPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUpgradeSkillResult->phySkillPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode magSkillPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUpgradeSkillResult->magSkillPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUpgradeSkillResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerIsBusying(void* pData)
{
	GCPlayerIsBusying* pkGCPlayerIsBusying = (GCPlayerIsBusying*)(pData);

	//EnCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerIsBusying->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayerName
	if(32 < pkGCPlayerIsBusying->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPlayerIsBusying->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCPlayerIsBusying->PlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode busyCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerIsBusying->busyCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerIsBusying(void* pData)
{
	GCPlayerIsBusying* pkGCPlayerIsBusying = (GCPlayerIsBusying*)(pData);

	//DeCode nameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerIsBusying->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayerName
	if(32 < pkGCPlayerIsBusying->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPlayerIsBusying->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCPlayerIsBusying->PlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode busyCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerIsBusying->busyCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerIsBusying);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSpSkillUpgrade(void* pData)
{
	GCSpSkillUpgrade* pkGCSpSkillUpgrade = (GCSpSkillUpgrade*)(pData);

	//EnCode oldSpSkillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSpSkillUpgrade->oldSpSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newSpSkillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSpSkillUpgrade->newSpSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSpSkillUpgrade(void* pData)
{
	GCSpSkillUpgrade* pkGCSpSkillUpgrade = (GCSpSkillUpgrade*)(pData);

	//DeCode oldSpSkillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSpSkillUpgrade->oldSpSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newSpSkillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSpSkillUpgrade->newSpSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSpSkillUpgrade);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeItemCri(void* pData)
{
	GCChangeItemCri* pkGCChangeItemCri = (GCChangeItemCri*)(pData);

	//EnCode changeItemCri
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChangeItemCri->changeItemCri), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeItemCri(void* pData)
{
	GCChangeItemCri* pkGCChangeItemCri = (GCChangeItemCri*)(pData);

	//DeCode changeItemCri
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChangeItemCri->changeItemCri), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeItemCri);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUpdateItemError(void* pData)
{
	GCUpdateItemError* pkGCUpdateItemError = (GCUpdateItemError*)(pData);

	//EnCode updateItemError
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUpdateItemError->updateItemError), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUpdateItemError(void* pData)
{
	GCUpdateItemError* pkGCUpdateItemError = (GCUpdateItemError*)(pData);

	//DeCode updateItemError
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUpdateItemError->updateItemError), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUpdateItemError);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGGetClientData(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGGetClientData(void* pData)
{
	(pData);
	return sizeof(CGGetClientData);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCGetClientData(void* pData)
{
	GCGetClientData* pkGCGetClientData = (GCGetClientData*)(pData);

	//EnCode isOK
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCGetClientData->isOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode index
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCGetClientData->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dataSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCGetClientData->dataSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode clientData
	if(400 < pkGCGetClientData->dataSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCGetClientData->dataSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCGetClientData->clientData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCGetClientData(void* pData)
{
	GCGetClientData* pkGCGetClientData = (GCGetClientData*)(pData);

	//DeCode isOK
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCGetClientData->isOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode index
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCGetClientData->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dataSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCGetClientData->dataSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode clientData
	if(400 < pkGCGetClientData->dataSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCGetClientData->dataSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCGetClientData->clientData), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCGetClientData);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSaveClientData(void* pData)
{
	CGSaveClientData* pkCGSaveClientData = (CGSaveClientData*)(pData);

	//EnCode isOK
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGSaveClientData->isOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode index
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGSaveClientData->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dataSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGSaveClientData->dataSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode clientData
	if(400 < pkCGSaveClientData->dataSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGSaveClientData->dataSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGSaveClientData->clientData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSaveClientData(void* pData)
{
	CGSaveClientData* pkCGSaveClientData = (CGSaveClientData*)(pData);

	//DeCode isOK
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGSaveClientData->isOK), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode index
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGSaveClientData->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dataSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGSaveClientData->dataSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode clientData
	if(400 < pkCGSaveClientData->dataSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGSaveClientData->dataSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGSaveClientData->clientData), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSaveClientData);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerEquipItem(void* pData)
{
	GCPlayerEquipItem* pkGCPlayerEquipItem = (GCPlayerEquipItem*)(pData);

	//EnCode equipPlayer
	if(EnCode__PlayerID(&(pkGCPlayerEquipItem->equipPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode equipItem
	if(EnCode__ItemInfo_i(&(pkGCPlayerEquipItem->equipItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode equipIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerEquipItem->equipIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerEquipItem(void* pData)
{
	GCPlayerEquipItem* pkGCPlayerEquipItem = (GCPlayerEquipItem*)(pData);

	//DeCode equipPlayer
	if(DeCode__PlayerID(&(pkGCPlayerEquipItem->equipPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode equipItem
	if(DeCode__ItemInfo_i(&(pkGCPlayerEquipItem->equipItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode equipIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerEquipItem->equipIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerEquipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCShowTaskUIInfo(void* pData)
{
	GCShowTaskUIInfo* pkGCShowTaskUIInfo = (GCShowTaskUIInfo*)(pData);

	//EnCode taskUIInfo
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCShowTaskUIInfo->taskUIInfo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCShowTaskUIInfo(void* pData)
{
	GCShowTaskUIInfo* pkGCShowTaskUIInfo = (GCShowTaskUIInfo*)(pData);

	//DeCode taskUIInfo
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCShowTaskUIInfo->taskUIInfo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCShowTaskUIInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCCleanSkillPoint(void* pData)
{
	GCCleanSkillPoint* pkGCCleanSkillPoint = (GCCleanSkillPoint*)(pData);

	//EnCode assignablePhyPoint
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCCleanSkillPoint->assignablePhyPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode assignableMagPoint
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCCleanSkillPoint->assignableMagPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCCleanSkillPoint->skillSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillData
	if(SKILL_SIZE < pkGCCleanSkillPoint->skillSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCCleanSkillPoint->skillSize; ++i)
	{
		if(EnCode__SkillInfo_i(&(pkGCCleanSkillPoint->skillData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCCleanSkillPoint(void* pData)
{
	GCCleanSkillPoint* pkGCCleanSkillPoint = (GCCleanSkillPoint*)(pData);

	//DeCode assignablePhyPoint
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCCleanSkillPoint->assignablePhyPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode assignableMagPoint
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCCleanSkillPoint->assignableMagPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCCleanSkillPoint->skillSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillData
	if(SKILL_SIZE < pkGCCleanSkillPoint->skillSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCCleanSkillPoint->skillSize; ++i)
	{
		if(DeCode__SkillInfo_i(&(pkGCCleanSkillPoint->skillData[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCCleanSkillPoint);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangeBattleMode(void* pData)
{
	CGChangeBattleMode* pkCGChangeBattleMode = (CGChangeBattleMode*)(pData);

	//EnCode changeMode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChangeBattleMode->changeMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangeBattleMode(void* pData)
{
	CGChangeBattleMode* pkCGChangeBattleMode = (CGChangeBattleMode*)(pData);

	//DeCode changeMode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChangeBattleMode->changeMode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangeBattleMode);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeBattleMode(void* pData)
{
	GCChangeBattleMode* pkGCChangeBattleMode = (GCChangeBattleMode*)(pData);

	//EnCode changeMode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChangeBattleMode->changeMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode bSuccess
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCChangeBattleMode->bSuccess), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeBattleMode(void* pData)
{
	GCChangeBattleMode* pkGCChangeBattleMode = (GCChangeBattleMode*)(pData);

	//DeCode changeMode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChangeBattleMode->changeMode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode bSuccess
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCChangeBattleMode->bSuccess), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeBattleMode);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCBuffData(void* pData)
{
	GCBuffData* pkGCBuffData = (GCBuffData*)(pData);

	//EnCode buffData
	if(EnCode__PlayerInfo_5_Buffers(&(pkGCBuffData->buffData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode creatureID
	if(EnCode__PlayerID(&(pkGCBuffData->creatureID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCBuffData(void* pData)
{
	GCBuffData* pkGCBuffData = (GCBuffData*)(pData);

	//DeCode buffData
	if(DeCode__PlayerInfo_5_Buffers(&(pkGCBuffData->buffData)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode creatureID
	if(DeCode__PlayerID(&(pkGCBuffData->creatureID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCBuffData);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerUseHealItem(void* pData)
{
	GCPlayerUseHealItem* pkGCPlayerUseHealItem = (GCPlayerUseHealItem*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCPlayerUseHealItem->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode useItem
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerUseHealItem->useItem), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerHp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerUseHealItem->playerHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerMp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerUseHealItem->playerMp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerSp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerUseHealItem->playerSp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemSkillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerUseHealItem->itemSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerUseHealItem(void* pData)
{
	GCPlayerUseHealItem* pkGCPlayerUseHealItem = (GCPlayerUseHealItem*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCPlayerUseHealItem->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode useItem
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerUseHealItem->useItem), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerHp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerUseHealItem->playerHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerMp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerUseHealItem->playerMp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerSp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerUseHealItem->playerSp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemSkillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerUseHealItem->itemSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerUseHealItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSwapEquipItem(void* pData)
{
	CGSwapEquipItem* pkCGSwapEquipItem = (CGSwapEquipItem*)(pData);

	//EnCode equipFrom
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkCGSwapEquipItem->equipFrom), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equipTo
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkCGSwapEquipItem->equipTo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSwapEquipItem(void* pData)
{
	CGSwapEquipItem* pkCGSwapEquipItem = (CGSwapEquipItem*)(pData);

	//DeCode equipFrom
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkCGSwapEquipItem->equipFrom), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equipTo
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkCGSwapEquipItem->equipTo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSwapEquipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryItemPkgPage(void* pData)
{
	CGQueryItemPkgPage* pkCGQueryItemPkgPage = (CGQueryItemPkgPage*)(pData);

	//EnCode queryPage
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkCGQueryItemPkgPage->queryPage), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryItemPkgPage(void* pData)
{
	CGQueryItemPkgPage* pkCGQueryItemPkgPage = (CGQueryItemPkgPage*)(pData);

	//DeCode queryPage
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkCGQueryItemPkgPage->queryPage), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryItemPkgPage);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSwapEquipResult(void* pData)
{
	GCSwapEquipResult* pkGCSwapEquipResult = (GCSwapEquipResult*)(pData);

	//EnCode swapPlayerID
	if(EnCode__PlayerID(&(pkGCSwapEquipResult->swapPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode equipFrom
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCSwapEquipResult->equipFrom), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equipTo
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCSwapEquipResult->equipTo), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode swapResult
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCSwapEquipResult->swapResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSwapEquipResult(void* pData)
{
	GCSwapEquipResult* pkGCSwapEquipResult = (GCSwapEquipResult*)(pData);

	//DeCode swapPlayerID
	if(DeCode__PlayerID(&(pkGCSwapEquipResult->swapPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode equipFrom
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCSwapEquipResult->equipFrom), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equipTo
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCSwapEquipResult->equipTo), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode swapResult
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCSwapEquipResult->swapResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSwapEquipResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeTeamState(void* pData)
{
	GCChangeTeamState* pkGCChangeTeamState = (GCChangeTeamState*)(pData);

	//EnCode changePlayerID
	if(EnCode__PlayerID(&(pkGCChangeTeamState->changePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode teamState
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCChangeTeamState->teamState), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeTeamState(void* pData)
{
	GCChangeTeamState* pkGCChangeTeamState = (GCChangeTeamState*)(pData);

	//DeCode changePlayerID
	if(DeCode__PlayerID(&(pkGCChangeTeamState->changePlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode teamState
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCChangeTeamState->teamState), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeTeamState);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CAGetSrvList(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CAGetSrvList(void* pData)
{
	(pData);
	return sizeof(CAGetSrvList);
}

size_t	MUX_PROTO::CliProtocol::EnCode__ACSrvOption(void* pData)
{
	ACSrvOption* pkACSrvOption = (ACSrvOption*)(pData);

	//EnCode byFlag
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkACSrvOption->byFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode wSrvNo
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkACSrvOption->wSrvNo), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode wPort
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkACSrvOption->wPort), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode addrSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkACSrvOption->addrSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode strAddr
	if(32 < pkACSrvOption->addrSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkACSrvOption->addrSize;
	if(!m_kPackage.Pack("CHAR", &(pkACSrvOption->strAddr), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkACSrvOption->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode srvName
	if(32 < pkACSrvOption->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkACSrvOption->nameSize;
	if(!m_kPackage.Pack("CHAR", &(pkACSrvOption->srvName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode byOverload
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkACSrvOption->byOverload), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__ACSrvOption(void* pData)
{
	ACSrvOption* pkACSrvOption = (ACSrvOption*)(pData);

	//DeCode byFlag
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkACSrvOption->byFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode wSrvNo
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkACSrvOption->wSrvNo), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode wPort
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkACSrvOption->wPort), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode addrSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkACSrvOption->addrSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode strAddr
	if(32 < pkACSrvOption->addrSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkACSrvOption->addrSize;
	if(!m_kPackage.UnPack("CHAR", &(pkACSrvOption->strAddr), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkACSrvOption->nameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode srvName
	if(32 < pkACSrvOption->nameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkACSrvOption->nameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkACSrvOption->srvName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode byOverload
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkACSrvOption->byOverload), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(ACSrvOption);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSetPlayerPos(void* pData)
{
	GCSetPlayerPos* pkGCSetPlayerPos = (GCSetPlayerPos*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCSetPlayerPos->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode posX
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCSetPlayerPos->posX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCSetPlayerPos->posY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSetPlayerPos(void* pData)
{
	GCSetPlayerPos* pkGCSetPlayerPos = (GCSetPlayerPos*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCSetPlayerPos->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode posX
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCSetPlayerPos->posX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCSetPlayerPos->posY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSetPlayerPos);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChatError(void* pData)
{
	GCChatError* pkGCChatError = (GCChatError*)(pData);

	//EnCode nType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChatError->nType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nErrorType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCChatError->nErrorType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tgtNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChatError->tgtNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tgtName
	if(MAX_NAME_SIZE < pkGCChatError->tgtNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChatError->tgtNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCChatError->tgtName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChatError(void* pData)
{
	GCChatError* pkGCChatError = (GCChatError*)(pData);

	//DeCode nType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChatError->nType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nErrorType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCChatError->nErrorType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tgtNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChatError->tgtNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tgtName
	if(MAX_NAME_SIZE < pkGCChatError->tgtNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChatError->tgtNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCChatError->tgtName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChatError);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGCreateChatGroup(void* pData)
{
	CGCreateChatGroup* pkCGCreateChatGroup = (CGCreateChatGroup*)(pData);

	//EnCode chatGroupNameSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCreateChatGroup->chatGroupNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupName
	if(MAX_CHATGROUP_NAME_LEN < pkCGCreateChatGroup->chatGroupNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateChatGroup->chatGroupNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGCreateChatGroup->chatGroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupPasswdSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGCreateChatGroup->chatGroupPasswdSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode charGroupPasswd
	if(MAX_CHATGROUP_PASSWORD_LEN < pkCGCreateChatGroup->chatGroupPasswdSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateChatGroup->chatGroupPasswdSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGCreateChatGroup->charGroupPasswd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGCreateChatGroup(void* pData)
{
	CGCreateChatGroup* pkCGCreateChatGroup = (CGCreateChatGroup*)(pData);

	//DeCode chatGroupNameSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCreateChatGroup->chatGroupNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupName
	if(MAX_CHATGROUP_NAME_LEN < pkCGCreateChatGroup->chatGroupNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateChatGroup->chatGroupNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGCreateChatGroup->chatGroupName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupPasswdSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGCreateChatGroup->chatGroupPasswdSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode charGroupPasswd
	if(MAX_CHATGROUP_PASSWORD_LEN < pkCGCreateChatGroup->chatGroupPasswdSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateChatGroup->chatGroupPasswdSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGCreateChatGroup->charGroupPasswd), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGCreateChatGroup);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCCreateChatGroupResult(void* pData)
{
	GCCreateChatGroupResult* pkGCCreateChatGroupResult = (GCCreateChatGroupResult*)(pData);

	//EnCode ret
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCCreateChatGroupResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCCreateChatGroupResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCCreateChatGroupResult(void* pData)
{
	GCCreateChatGroupResult* pkGCCreateChatGroupResult = (GCCreateChatGroupResult*)(pData);

	//DeCode ret
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCCreateChatGroupResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCCreateChatGroupResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCCreateChatGroupResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDestroyChatGroup(void* pData)
{
	CGDestroyChatGroup* pkCGDestroyChatGroup = (CGDestroyChatGroup*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDestroyChatGroup->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDestroyChatGroup(void* pData)
{
	CGDestroyChatGroup* pkCGDestroyChatGroup = (CGDestroyChatGroup*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDestroyChatGroup->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDestroyChatGroup);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDestroyChatGroupResult(void* pData)
{
	GCDestroyChatGroupResult* pkGCDestroyChatGroupResult = (GCDestroyChatGroupResult*)(pData);

	//EnCode ret
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDestroyChatGroupResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDestroyChatGroupResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDestroyChatGroupResult(void* pData)
{
	GCDestroyChatGroupResult* pkGCDestroyChatGroupResult = (GCDestroyChatGroupResult*)(pData);

	//DeCode ret
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDestroyChatGroupResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDestroyChatGroupResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDestroyChatGroupResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDestroyChatGroupNotify(void* pData)
{
	GCDestroyChatGroupNotify* pkGCDestroyChatGroupNotify = (GCDestroyChatGroupNotify*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDestroyChatGroupNotify->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDestroyChatGroupNotify(void* pData)
{
	GCDestroyChatGroupNotify* pkGCDestroyChatGroupNotify = (GCDestroyChatGroupNotify*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDestroyChatGroupNotify->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDestroyChatGroupNotify);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddChatGroupMemberByOwner(void* pData)
{
	CGAddChatGroupMemberByOwner* pkCGAddChatGroupMemberByOwner = (CGAddChatGroupMemberByOwner*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddChatGroupMemberByOwner->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddChatGroupMemberByOwner->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(MAX_NAME_SIZE < pkCGAddChatGroupMemberByOwner->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddChatGroupMemberByOwner->memberNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGAddChatGroupMemberByOwner->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddChatGroupMemberByOwner(void* pData)
{
	CGAddChatGroupMemberByOwner* pkCGAddChatGroupMemberByOwner = (CGAddChatGroupMemberByOwner*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddChatGroupMemberByOwner->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddChatGroupMemberByOwner->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(MAX_NAME_SIZE < pkCGAddChatGroupMemberByOwner->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddChatGroupMemberByOwner->memberNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGAddChatGroupMemberByOwner->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddChatGroupMemberByOwner);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddChatGroupMember(void* pData)
{
	CGAddChatGroupMember* pkCGAddChatGroupMember = (CGAddChatGroupMember*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddChatGroupMember->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupPasswdSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddChatGroupMember->chatGroupPasswdSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode charGroupPasswd
	if(MAX_CHATGROUP_PASSWORD_LEN < pkCGAddChatGroupMember->chatGroupPasswdSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddChatGroupMember->chatGroupPasswdSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGAddChatGroupMember->charGroupPasswd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddChatGroupMember(void* pData)
{
	CGAddChatGroupMember* pkCGAddChatGroupMember = (CGAddChatGroupMember*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddChatGroupMember->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupPasswdSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddChatGroupMember->chatGroupPasswdSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode charGroupPasswd
	if(MAX_CHATGROUP_PASSWORD_LEN < pkCGAddChatGroupMember->chatGroupPasswdSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddChatGroupMember->chatGroupPasswdSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGAddChatGroupMember->charGroupPasswd), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddChatGroupMember);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddChatGroupMemberResult(void* pData)
{
	GCAddChatGroupMemberResult* pkGCAddChatGroupMemberResult = (GCAddChatGroupMemberResult*)(pData);

	//EnCode ret
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddChatGroupMemberResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddChatGroupMemberResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupMemberCount
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddChatGroupMemberResult->chatGroupMemberCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupMembers
	if(MAX_CHATGROUP_MEMBERS_COUNT < pkGCAddChatGroupMemberResult->chatGroupMemberCount)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCAddChatGroupMemberResult->chatGroupMemberCount; ++i)
	{
		if(EnCode__GroupChatMemberInfo(&(pkGCAddChatGroupMemberResult->chatGroupMembers[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddChatGroupMemberResult(void* pData)
{
	GCAddChatGroupMemberResult* pkGCAddChatGroupMemberResult = (GCAddChatGroupMemberResult*)(pData);

	//DeCode ret
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddChatGroupMemberResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddChatGroupMemberResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupMemberCount
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddChatGroupMemberResult->chatGroupMemberCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupMembers
	if(MAX_CHATGROUP_MEMBERS_COUNT < pkGCAddChatGroupMemberResult->chatGroupMemberCount)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCAddChatGroupMemberResult->chatGroupMemberCount; ++i)
	{
		if(DeCode__GroupChatMemberInfo(&(pkGCAddChatGroupMemberResult->chatGroupMembers[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCAddChatGroupMemberResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddChatGroupMemberNotify(void* pData)
{
	GCAddChatGroupMemberNotify* pkGCAddChatGroupMemberNotify = (GCAddChatGroupMemberNotify*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddChatGroupMemberNotify->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddChatGroupMemberNotify->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(MAX_NAME_SIZE < pkGCAddChatGroupMemberNotify->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddChatGroupMemberNotify->memberNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCAddChatGroupMemberNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddChatGroupMemberNotify(void* pData)
{
	GCAddChatGroupMemberNotify* pkGCAddChatGroupMemberNotify = (GCAddChatGroupMemberNotify*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddChatGroupMemberNotify->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddChatGroupMemberNotify->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(MAX_NAME_SIZE < pkGCAddChatGroupMemberNotify->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddChatGroupMemberNotify->memberNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCAddChatGroupMemberNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddChatGroupMemberNotify);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRemoveChatGroupMember(void* pData)
{
	CGRemoveChatGroupMember* pkCGRemoveChatGroupMember = (CGRemoveChatGroupMember*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRemoveChatGroupMember->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRemoveChatGroupMember->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(MAX_NAME_SIZE < pkCGRemoveChatGroupMember->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGRemoveChatGroupMember->memberNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGRemoveChatGroupMember->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRemoveChatGroupMember(void* pData)
{
	CGRemoveChatGroupMember* pkCGRemoveChatGroupMember = (CGRemoveChatGroupMember*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRemoveChatGroupMember->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRemoveChatGroupMember->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(MAX_NAME_SIZE < pkCGRemoveChatGroupMember->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGRemoveChatGroupMember->memberNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGRemoveChatGroupMember->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRemoveChatGroupMember);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRemoveChatGroupMemberResult(void* pData)
{
	GCRemoveChatGroupMemberResult* pkGCRemoveChatGroupMemberResult = (GCRemoveChatGroupMemberResult*)(pData);

	//EnCode ret
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRemoveChatGroupMemberResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatGroupID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRemoveChatGroupMemberResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRemoveChatGroupMemberResult(void* pData)
{
	GCRemoveChatGroupMemberResult* pkGCRemoveChatGroupMemberResult = (GCRemoveChatGroupMemberResult*)(pData);

	//DeCode ret
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRemoveChatGroupMemberResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatGroupID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRemoveChatGroupMemberResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRemoveChatGroupMemberResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRemoveChatGroupNotify(void* pData)
{
	GCRemoveChatGroupNotify* pkGCRemoveChatGroupNotify = (GCRemoveChatGroupNotify*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRemoveChatGroupNotify->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRemoveChatGroupNotify->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(MAX_NAME_SIZE < pkGCRemoveChatGroupNotify->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRemoveChatGroupNotify->memberNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCRemoveChatGroupNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRemoveChatGroupNotify(void* pData)
{
	GCRemoveChatGroupNotify* pkGCRemoveChatGroupNotify = (GCRemoveChatGroupNotify*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRemoveChatGroupNotify->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRemoveChatGroupNotify->memberNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(MAX_NAME_SIZE < pkGCRemoveChatGroupNotify->memberNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRemoveChatGroupNotify->memberNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRemoveChatGroupNotify->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRemoveChatGroupNotify);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChatGroupSpeak(void* pData)
{
	CGChatGroupSpeak* pkCGChatGroupSpeak = (CGChatGroupSpeak*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChatGroupSpeak->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatDataSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChatGroupSpeak->chatDataSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatData
	if(MAX_CHATDATA_LEN < pkCGChatGroupSpeak->chatDataSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGChatGroupSpeak->chatDataSize;
	if(!m_kPackage.Pack("CHAR", &(pkCGChatGroupSpeak->chatData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChatGroupSpeak(void* pData)
{
	CGChatGroupSpeak* pkCGChatGroupSpeak = (CGChatGroupSpeak*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChatGroupSpeak->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatDataSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChatGroupSpeak->chatDataSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatData
	if(MAX_CHATDATA_LEN < pkCGChatGroupSpeak->chatDataSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGChatGroupSpeak->chatDataSize;
	if(!m_kPackage.UnPack("CHAR", &(pkCGChatGroupSpeak->chatData), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChatGroupSpeak);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChatGroupSpeakResult(void* pData)
{
	GCChatGroupSpeakResult* pkGCChatGroupSpeakResult = (GCChatGroupSpeakResult*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChatGroupSpeakResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ret
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChatGroupSpeakResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChatGroupSpeakResult(void* pData)
{
	GCChatGroupSpeakResult* pkGCChatGroupSpeakResult = (GCChatGroupSpeakResult*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChatGroupSpeakResult->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ret
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChatGroupSpeakResult->ret), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChatGroupSpeakResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChatGroupSpeakNotify(void* pData)
{
	GCChatGroupSpeakNotify* pkGCChatGroupSpeakNotify = (GCChatGroupSpeakNotify*)(pData);

	//EnCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChatGroupSpeakNotify->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode speakerNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChatGroupSpeakNotify->speakerNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode speakerName
	if(MAX_NAME_SIZE < pkGCChatGroupSpeakNotify->speakerNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChatGroupSpeakNotify->speakerNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCChatGroupSpeakNotify->speakerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatDataSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChatGroupSpeakNotify->chatDataSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode chatData
	if(MAX_CHATDATA_LEN < pkGCChatGroupSpeakNotify->chatDataSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChatGroupSpeakNotify->chatDataSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCChatGroupSpeakNotify->chatData), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChatGroupSpeakNotify(void* pData)
{
	GCChatGroupSpeakNotify* pkGCChatGroupSpeakNotify = (GCChatGroupSpeakNotify*)(pData);

	//DeCode chatGroupID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChatGroupSpeakNotify->chatGroupID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode speakerNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChatGroupSpeakNotify->speakerNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode speakerName
	if(MAX_NAME_SIZE < pkGCChatGroupSpeakNotify->speakerNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChatGroupSpeakNotify->speakerNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCChatGroupSpeakNotify->speakerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatDataSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChatGroupSpeakNotify->chatDataSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode chatData
	if(MAX_CHATDATA_LEN < pkGCChatGroupSpeakNotify->chatDataSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChatGroupSpeakNotify->chatDataSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCChatGroupSpeakNotify->chatData), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChatGroupSpeakNotify);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGClinetSynchTime(void* pData)
{
	CGClinetSynchTime* pkCGClinetSynchTime = (CGClinetSynchTime*)(pData);

	//EnCode timeSynchID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGClinetSynchTime->timeSynchID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGClinetSynchTime(void* pData)
{
	CGClinetSynchTime* pkCGClinetSynchTime = (CGClinetSynchTime*)(pData);

	//DeCode timeSynchID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGClinetSynchTime->timeSynchID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGClinetSynchTime);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSrvSynchTime(void* pData)
{
	GCSrvSynchTime* pkGCSrvSynchTime = (GCSrvSynchTime*)(pData);

	//EnCode timeSynchID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSrvSynchTime->timeSynchID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSrvSynchTime(void* pData)
{
	GCSrvSynchTime* pkGCSrvSynchTime = (GCSrvSynchTime*)(pData);

	//DeCode timeSynchID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSrvSynchTime->timeSynchID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSrvSynchTime);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSynchClientTime(void* pData)
{
	GCSynchClientTime* pkGCSynchClientTime = (GCSynchClientTime*)(pData);

	//EnCode delayTime
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSynchClientTime->delayTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode srvTimeSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSynchClientTime->srvTimeSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode srvTime
	if(MAX_NAME_SIZE < pkGCSynchClientTime->srvTimeSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCSynchClientTime->srvTimeSize;
	if(!m_kPackage.Pack("CHAR", &(pkGCSynchClientTime->srvTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSynchClientTime(void* pData)
{
	GCSynchClientTime* pkGCSynchClientTime = (GCSynchClientTime*)(pData);

	//DeCode delayTime
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSynchClientTime->delayTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode srvTimeSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSynchClientTime->srvTimeSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode srvTime
	if(MAX_NAME_SIZE < pkGCSynchClientTime->srvTimeSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCSynchClientTime->srvTimeSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGCSynchClientTime->srvTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSynchClientTime);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRepairWearByNPC(void* pData)
{
	CGRepairWearByNPC* pkCGRepairWearByNPC = (CGRepairWearByNPC*)(pData);

	//EnCode repairAll
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGRepairWearByNPC->repairAll), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode repairTarget
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRepairWearByNPC->repairTarget), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRepairWearByNPC(void* pData)
{
	CGRepairWearByNPC* pkCGRepairWearByNPC = (CGRepairWearByNPC*)(pData);

	//DeCode repairAll
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGRepairWearByNPC->repairAll), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode repairTarget
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRepairWearByNPC->repairTarget), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRepairWearByNPC);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRepairWearByNPC(void* pData)
{
	GCRepairWearByNPC* pkGCRepairWearByNPC = (GCRepairWearByNPC*)(pData);

	//EnCode tResult
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByNPC->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode repairAll
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCRepairWearByNPC->repairAll), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode repairTarget
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByNPC->repairTarget), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newWear
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByNPC->newWear), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiRepairMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRepairWearByNPC->uiRepairMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRepairWearByNPC(void* pData)
{
	GCRepairWearByNPC* pkGCRepairWearByNPC = (GCRepairWearByNPC*)(pData);

	//DeCode tResult
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByNPC->tResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode repairAll
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCRepairWearByNPC->repairAll), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode repairTarget
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByNPC->repairTarget), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newWear
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByNPC->newWear), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiRepairMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRepairWearByNPC->uiRepairMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRepairWearByNPC);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerAddHp(void* pData)
{
	GCPlayerAddHp* pkGCPlayerAddHp = (GCPlayerAddHp*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkGCPlayerAddHp->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode MaxHp
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAddHp->MaxHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode currentHp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAddHp->currentHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode healVal
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAddHp->healVal), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAddHp->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode cdTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerAddHp->cdTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerAddHp(void* pData)
{
	GCPlayerAddHp* pkGCPlayerAddHp = (GCPlayerAddHp*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkGCPlayerAddHp->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode MaxHp
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAddHp->MaxHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode currentHp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAddHp->currentHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode healVal
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAddHp->healVal), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAddHp->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode cdTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerAddHp->cdTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerAddHp);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGCreateUnionRequest(void* pData)
{
	CGCreateUnionRequest* pkCGCreateUnionRequest = (CGCreateUnionRequest*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGCreateUnionRequest->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_UNIONNAME_LEN < pkCGCreateUnionRequest->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateUnionRequest->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGCreateUnionRequest->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGCreateUnionRequest(void* pData)
{
	CGCreateUnionRequest* pkCGCreateUnionRequest = (CGCreateUnionRequest*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGCreateUnionRequest->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_UNIONNAME_LEN < pkCGCreateUnionRequest->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGCreateUnionRequest->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGCreateUnionRequest->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGCreateUnionRequest);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCCreateUnionResult(void* pData)
{
	GCCreateUnionResult* pkGCCreateUnionResult = (GCCreateUnionResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCCreateUnionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode _union
	if(EnCode__Union(&(pkGCCreateUnionResult->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCCreateUnionResult(void* pData)
{
	GCCreateUnionResult* pkGCCreateUnionResult = (GCCreateUnionResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCCreateUnionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode _union
	if(DeCode__Union(&(pkGCCreateUnionResult->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCCreateUnionResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDestroyUnionRequest(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDestroyUnionRequest(void* pData)
{
	(pData);
	return sizeof(CGDestroyUnionRequest);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDestroyUnionResult(void* pData)
{
	GCDestroyUnionResult* pkGCDestroyUnionResult = (GCDestroyUnionResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCDestroyUnionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDestroyUnionResult(void* pData)
{
	GCDestroyUnionResult* pkGCDestroyUnionResult = (GCDestroyUnionResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCDestroyUnionResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDestroyUnionResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionDestroyedNotify(void* pData)
{
	GCUnionDestroyedNotify* pkGCUnionDestroyedNotify = (GCUnionDestroyedNotify*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionDestroyedNotify->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_PLAYERNAME_LEN < pkGCUnionDestroyedNotify->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionDestroyedNotify->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCUnionDestroyedNotify->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionDestroyedNotify(void* pData)
{
	GCUnionDestroyedNotify* pkGCUnionDestroyedNotify = (GCUnionDestroyedNotify*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionDestroyedNotify->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_PLAYERNAME_LEN < pkGCUnionDestroyedNotify->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionDestroyedNotify->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCUnionDestroyedNotify->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionDestroyedNotify);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryUnionBasicRequest(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryUnionBasicRequest(void* pData)
{
	(pData);
	return sizeof(CGQueryUnionBasicRequest);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryUnionBasicResult(void* pData)
{
	GCQueryUnionBasicResult* pkGCQueryUnionBasicResult = (GCQueryUnionBasicResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCQueryUnionBasicResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode _union
	if(EnCode__Union(&(pkGCQueryUnionBasicResult->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryUnionBasicResult(void* pData)
{
	GCQueryUnionBasicResult* pkGCQueryUnionBasicResult = (GCQueryUnionBasicResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCQueryUnionBasicResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode _union
	if(DeCode__Union(&(pkGCQueryUnionBasicResult->_union)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCQueryUnionBasicResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryUnionMembersRequest(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryUnionMembersRequest(void* pData)
{
	(pData);
	return sizeof(CGQueryUnionMembersRequest);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryUnionMembersResult(void* pData)
{
	GCQueryUnionMembersResult* pkGCQueryUnionMembersResult = (GCQueryUnionMembersResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCQueryUnionMembersResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode members
	if(EnCode__UnionMembers(&(pkGCQueryUnionMembersResult->members)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryUnionMembersResult(void* pData)
{
	GCQueryUnionMembersResult* pkGCQueryUnionMembersResult = (GCQueryUnionMembersResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCQueryUnionMembersResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode members
	if(DeCode__UnionMembers(&(pkGCQueryUnionMembersResult->members)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCQueryUnionMembersResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddUnionMemberRequest(void* pData)
{
	CGAddUnionMemberRequest* pkCGAddUnionMemberRequest = (CGAddUnionMemberRequest*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGAddUnionMemberRequest->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_PLAYERNAME_LEN < pkCGAddUnionMemberRequest->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddUnionMemberRequest->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGAddUnionMemberRequest->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddUnionMemberRequest(void* pData)
{
	CGAddUnionMemberRequest* pkCGAddUnionMemberRequest = (CGAddUnionMemberRequest*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGAddUnionMemberRequest->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_PLAYERNAME_LEN < pkCGAddUnionMemberRequest->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddUnionMemberRequest->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGAddUnionMemberRequest->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddUnionMemberRequest);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddUnionMemberConfirm(void* pData)
{
	GCAddUnionMemberConfirm* pkGCAddUnionMemberConfirm = (GCAddUnionMemberConfirm*)(pData);

	//EnCode id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAddUnionMemberConfirm->id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionNameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAddUnionMemberConfirm->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionName
	if(MAX_UNIONNAME_LEN < pkGCAddUnionMemberConfirm->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddUnionMemberConfirm->unionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCAddUnionMemberConfirm->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAddUnionMemberConfirm->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_PLAYERNAME_LEN < pkGCAddUnionMemberConfirm->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddUnionMemberConfirm->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCAddUnionMemberConfirm->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddUnionMemberConfirm(void* pData)
{
	GCAddUnionMemberConfirm* pkGCAddUnionMemberConfirm = (GCAddUnionMemberConfirm*)(pData);

	//DeCode id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAddUnionMemberConfirm->id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAddUnionMemberConfirm->unionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionName
	if(MAX_UNIONNAME_LEN < pkGCAddUnionMemberConfirm->unionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddUnionMemberConfirm->unionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCAddUnionMemberConfirm->unionName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAddUnionMemberConfirm->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_PLAYERNAME_LEN < pkGCAddUnionMemberConfirm->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddUnionMemberConfirm->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCAddUnionMemberConfirm->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddUnionMemberConfirm);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddUnionMemberConfirmed(void* pData)
{
	CGAddUnionMemberConfirmed* pkCGAddUnionMemberConfirmed = (CGAddUnionMemberConfirmed*)(pData);

	//EnCode id
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddUnionMemberConfirmed->id), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGAddUnionMemberConfirmed->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddUnionMemberConfirmed(void* pData)
{
	CGAddUnionMemberConfirmed* pkCGAddUnionMemberConfirmed = (CGAddUnionMemberConfirmed*)(pData);

	//DeCode id
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddUnionMemberConfirmed->id), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGAddUnionMemberConfirmed->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddUnionMemberConfirmed);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddUnionMemberResult(void* pData)
{
	GCAddUnionMemberResult* pkGCAddUnionMemberResult = (GCAddUnionMemberResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCAddUnionMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAddUnionMemberResult->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_PLAYERNAME_LEN < pkGCAddUnionMemberResult->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddUnionMemberResult->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCAddUnionMemberResult->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddUnionMemberResult(void* pData)
{
	GCAddUnionMemberResult* pkGCAddUnionMemberResult = (GCAddUnionMemberResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCAddUnionMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAddUnionMemberResult->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_PLAYERNAME_LEN < pkGCAddUnionMemberResult->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddUnionMemberResult->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCAddUnionMemberResult->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddUnionMemberResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddUnionMemberNotify(void* pData)
{
	GCAddUnionMemberNotify* pkGCAddUnionMemberNotify = (GCAddUnionMemberNotify*)(pData);

	//EnCode memberAdded
	if(EnCode__UnionMember(&(pkGCAddUnionMemberNotify->memberAdded)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddUnionMemberNotify(void* pData)
{
	GCAddUnionMemberNotify* pkGCAddUnionMemberNotify = (GCAddUnionMemberNotify*)(pData);

	//DeCode memberAdded
	if(DeCode__UnionMember(&(pkGCAddUnionMemberNotify->memberAdded)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddUnionMemberNotify);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRemoveUnionMemberRequest(void* pData)
{
	CGRemoveUnionMemberRequest* pkCGRemoveUnionMemberRequest = (CGRemoveUnionMemberRequest*)(pData);

	//EnCode memberRemoved
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGRemoveUnionMemberRequest->memberRemoved), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRemoveUnionMemberRequest(void* pData)
{
	CGRemoveUnionMemberRequest* pkCGRemoveUnionMemberRequest = (CGRemoveUnionMemberRequest*)(pData);

	//DeCode memberRemoved
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGRemoveUnionMemberRequest->memberRemoved), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRemoveUnionMemberRequest);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionPosRightsNtf(void* pData)
{
	GCUnionPosRightsNtf* pkGCUnionPosRightsNtf = (GCUnionPosRightsNtf*)(pData);

	//EnCode rights
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUnionPosRightsNtf->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionPosRightsNtf->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if(MAX_PLAYERNAME_LEN < pkGCUnionPosRightsNtf->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionPosRightsNtf->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCUnionPosRightsNtf->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode flag
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionPosRightsNtf->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionPosRightsNtf(void* pData)
{
	GCUnionPosRightsNtf* pkGCUnionPosRightsNtf = (GCUnionPosRightsNtf*)(pData);

	//DeCode rights
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUnionPosRightsNtf->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionPosRightsNtf->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if(MAX_PLAYERNAME_LEN < pkGCUnionPosRightsNtf->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionPosRightsNtf->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCUnionPosRightsNtf->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode flag
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionPosRightsNtf->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionPosRightsNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGModifyUnionPosRighstReq(void* pData)
{
	CGModifyUnionPosRighstReq* pkCGModifyUnionPosRighstReq = (CGModifyUnionPosRighstReq*)(pData);

	//EnCode posLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGModifyUnionPosRighstReq->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if(MAX_PLAYERNAME_LEN < pkCGModifyUnionPosRighstReq->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGModifyUnionPosRighstReq->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGModifyUnionPosRighstReq->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rights
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGModifyUnionPosRighstReq->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGModifyUnionPosRighstReq(void* pData)
{
	CGModifyUnionPosRighstReq* pkCGModifyUnionPosRighstReq = (CGModifyUnionPosRighstReq*)(pData);

	//DeCode posLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGModifyUnionPosRighstReq->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if(MAX_PLAYERNAME_LEN < pkCGModifyUnionPosRighstReq->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGModifyUnionPosRighstReq->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGModifyUnionPosRighstReq->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rights
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGModifyUnionPosRighstReq->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGModifyUnionPosRighstReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCModifyUnionPosRighstRst(void* pData)
{
	GCModifyUnionPosRighstRst* pkGCModifyUnionPosRighstRst = (GCModifyUnionPosRighstRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCModifyUnionPosRighstRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCModifyUnionPosRighstRst->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if(MAX_PLAYERNAME_LEN < pkGCModifyUnionPosRighstRst->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCModifyUnionPosRighstRst->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCModifyUnionPosRighstRst->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCModifyUnionPosRighstRst(void* pData)
{
	GCModifyUnionPosRighstRst* pkGCModifyUnionPosRighstRst = (GCModifyUnionPosRighstRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCModifyUnionPosRighstRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCModifyUnionPosRighstRst->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if(MAX_PLAYERNAME_LEN < pkGCModifyUnionPosRighstRst->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCModifyUnionPosRighstRst->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCModifyUnionPosRighstRst->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCModifyUnionPosRighstRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAddUnionPosReq(void* pData)
{
	CGAddUnionPosReq* pkCGAddUnionPosReq = (CGAddUnionPosReq*)(pData);

	//EnCode posLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGAddUnionPosReq->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if(MAX_PLAYERNAME_LEN < pkCGAddUnionPosReq->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddUnionPosReq->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGAddUnionPosReq->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rights
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAddUnionPosReq->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAddUnionPosReq(void* pData)
{
	CGAddUnionPosReq* pkCGAddUnionPosReq = (CGAddUnionPosReq*)(pData);

	//DeCode posLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGAddUnionPosReq->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if(MAX_PLAYERNAME_LEN < pkCGAddUnionPosReq->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGAddUnionPosReq->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGAddUnionPosReq->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rights
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAddUnionPosReq->rights), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAddUnionPosReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAddUnionPosRst(void* pData)
{
	GCAddUnionPosRst* pkGCAddUnionPosRst = (GCAddUnionPosRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCAddUnionPosRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAddUnionPosRst->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if(MAX_PLAYERNAME_LEN < pkGCAddUnionPosRst->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddUnionPosRst->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCAddUnionPosRst->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAddUnionPosRst(void* pData)
{
	GCAddUnionPosRst* pkGCAddUnionPosRst = (GCAddUnionPosRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCAddUnionPosRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAddUnionPosRst->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if(MAX_PLAYERNAME_LEN < pkGCAddUnionPosRst->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAddUnionPosRst->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCAddUnionPosRst->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAddUnionPosRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGRemoveUnionPosReq(void* pData)
{
	CGRemoveUnionPosReq* pkCGRemoveUnionPosReq = (CGRemoveUnionPosReq*)(pData);

	//EnCode posLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGRemoveUnionPosReq->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if(MAX_PLAYERNAME_LEN < pkCGRemoveUnionPosReq->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGRemoveUnionPosReq->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGRemoveUnionPosReq->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGRemoveUnionPosReq(void* pData)
{
	CGRemoveUnionPosReq* pkCGRemoveUnionPosReq = (CGRemoveUnionPosReq*)(pData);

	//DeCode posLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGRemoveUnionPosReq->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if(MAX_PLAYERNAME_LEN < pkCGRemoveUnionPosReq->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGRemoveUnionPosReq->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGRemoveUnionPosReq->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGRemoveUnionPosReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRemoveUnionPosRst(void* pData)
{
	GCRemoveUnionPosRst* pkGCRemoveUnionPosRst = (GCRemoveUnionPosRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRemoveUnionPosRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCRemoveUnionPosRst->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pos
	if(MAX_PLAYERNAME_LEN < pkGCRemoveUnionPosRst->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRemoveUnionPosRst->posLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCRemoveUnionPosRst->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRemoveUnionPosRst(void* pData)
{
	GCRemoveUnionPosRst* pkGCRemoveUnionPosRst = (GCRemoveUnionPosRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRemoveUnionPosRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCRemoveUnionPosRst->posLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pos
	if(MAX_PLAYERNAME_LEN < pkGCRemoveUnionPosRst->posLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCRemoveUnionPosRst->posLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRemoveUnionPosRst->pos), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRemoveUnionPosRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGTransformUnionCaptionReq(void* pData)
{
	CGTransformUnionCaptionReq* pkCGTransformUnionCaptionReq = (CGTransformUnionCaptionReq*)(pData);

	//EnCode newCaptionUiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGTransformUnionCaptionReq->newCaptionUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGTransformUnionCaptionReq(void* pData)
{
	CGTransformUnionCaptionReq* pkCGTransformUnionCaptionReq = (CGTransformUnionCaptionReq*)(pData);

	//DeCode newCaptionUiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGTransformUnionCaptionReq->newCaptionUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGTransformUnionCaptionReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTransformUnionCaptionRst(void* pData)
{
	GCTransformUnionCaptionRst* pkGCTransformUnionCaptionRst = (GCTransformUnionCaptionRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCTransformUnionCaptionRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTransformUnionCaptionRst(void* pData)
{
	GCTransformUnionCaptionRst* pkGCTransformUnionCaptionRst = (GCTransformUnionCaptionRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCTransformUnionCaptionRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTransformUnionCaptionRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCTransformUnionCaptionNtf(void* pData)
{
	GCTransformUnionCaptionNtf* pkGCTransformUnionCaptionNtf = (GCTransformUnionCaptionNtf*)(pData);

	//EnCode newCaptionNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCTransformUnionCaptionNtf->newCaptionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode newCaptionName
	if(MAX_PLAYERNAME_LEN < pkGCTransformUnionCaptionNtf->newCaptionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCTransformUnionCaptionNtf->newCaptionNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCTransformUnionCaptionNtf->newCaptionName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCTransformUnionCaptionNtf(void* pData)
{
	GCTransformUnionCaptionNtf* pkGCTransformUnionCaptionNtf = (GCTransformUnionCaptionNtf*)(pData);

	//DeCode newCaptionNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCTransformUnionCaptionNtf->newCaptionNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode newCaptionName
	if(MAX_PLAYERNAME_LEN < pkGCTransformUnionCaptionNtf->newCaptionNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCTransformUnionCaptionNtf->newCaptionNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCTransformUnionCaptionNtf->newCaptionName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCTransformUnionCaptionNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGModifyUnionMemberTitleReq(void* pData)
{
	CGModifyUnionMemberTitleReq* pkCGModifyUnionMemberTitleReq = (CGModifyUnionMemberTitleReq*)(pData);

	//EnCode modifiedUiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGModifyUnionMemberTitleReq->modifiedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGModifyUnionMemberTitleReq->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if(MAX_PLAYERNAME_LEN < pkCGModifyUnionMemberTitleReq->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGModifyUnionMemberTitleReq->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGModifyUnionMemberTitleReq->title), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGModifyUnionMemberTitleReq(void* pData)
{
	CGModifyUnionMemberTitleReq* pkCGModifyUnionMemberTitleReq = (CGModifyUnionMemberTitleReq*)(pData);

	//DeCode modifiedUiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGModifyUnionMemberTitleReq->modifiedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGModifyUnionMemberTitleReq->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if(MAX_PLAYERNAME_LEN < pkCGModifyUnionMemberTitleReq->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGModifyUnionMemberTitleReq->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGModifyUnionMemberTitleReq->title), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGModifyUnionMemberTitleReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCModifyUnionMemberTitleRst(void* pData)
{
	GCModifyUnionMemberTitleRst* pkGCModifyUnionMemberTitleRst = (GCModifyUnionMemberTitleRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCModifyUnionMemberTitleRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCModifyUnionMemberTitleRst(void* pData)
{
	GCModifyUnionMemberTitleRst* pkGCModifyUnionMemberTitleRst = (GCModifyUnionMemberTitleRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCModifyUnionMemberTitleRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCModifyUnionMemberTitleRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionMemberTitleNtf(void* pData)
{
	GCUnionMemberTitleNtf* pkGCUnionMemberTitleNtf = (GCUnionMemberTitleNtf*)(pData);

	//EnCode memberNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionMemberTitleNtf->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(MAX_PLAYERNAME_LEN < pkGCUnionMemberTitleNtf->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionMemberTitleNtf->memberNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCUnionMemberTitleNtf->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionMemberTitleNtf->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode title
	if(MAX_PLAYERNAME_LEN < pkGCUnionMemberTitleNtf->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionMemberTitleNtf->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCUnionMemberTitleNtf->title), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionMemberTitleNtf(void* pData)
{
	GCUnionMemberTitleNtf* pkGCUnionMemberTitleNtf = (GCUnionMemberTitleNtf*)(pData);

	//DeCode memberNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionMemberTitleNtf->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(MAX_PLAYERNAME_LEN < pkGCUnionMemberTitleNtf->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionMemberTitleNtf->memberNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCUnionMemberTitleNtf->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionMemberTitleNtf->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode title
	if(MAX_PLAYERNAME_LEN < pkGCUnionMemberTitleNtf->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionMemberTitleNtf->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCUnionMemberTitleNtf->title), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionMemberTitleNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAdvanceUnionMemberPosReq(void* pData)
{
	CGAdvanceUnionMemberPosReq* pkCGAdvanceUnionMemberPosReq = (CGAdvanceUnionMemberPosReq*)(pData);

	//EnCode advancedUiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGAdvanceUnionMemberPosReq->advancedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAdvanceUnionMemberPosReq(void* pData)
{
	CGAdvanceUnionMemberPosReq* pkCGAdvanceUnionMemberPosReq = (CGAdvanceUnionMemberPosReq*)(pData);

	//DeCode advancedUiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGAdvanceUnionMemberPosReq->advancedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGAdvanceUnionMemberPosReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAdvanceUnionMemberPosRst(void* pData)
{
	GCAdvanceUnionMemberPosRst* pkGCAdvanceUnionMemberPosRst = (GCAdvanceUnionMemberPosRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAdvanceUnionMemberPosRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAdvanceUnionMemberPosRst(void* pData)
{
	GCAdvanceUnionMemberPosRst* pkGCAdvanceUnionMemberPosRst = (GCAdvanceUnionMemberPosRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAdvanceUnionMemberPosRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAdvanceUnionMemberPosRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAdvanceUnionMemberPosNtf(void* pData)
{
	GCAdvanceUnionMemberPosNtf* pkGCAdvanceUnionMemberPosNtf = (GCAdvanceUnionMemberPosNtf*)(pData);

	//EnCode memberNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAdvanceUnionMemberPosNtf->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(MAX_PLAYERNAME_LEN < pkGCAdvanceUnionMemberPosNtf->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAdvanceUnionMemberPosNtf->memberNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCAdvanceUnionMemberPosNtf->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posSeq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAdvanceUnionMemberPosNtf->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAdvanceUnionMemberPosNtf(void* pData)
{
	GCAdvanceUnionMemberPosNtf* pkGCAdvanceUnionMemberPosNtf = (GCAdvanceUnionMemberPosNtf*)(pData);

	//DeCode memberNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAdvanceUnionMemberPosNtf->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(MAX_PLAYERNAME_LEN < pkGCAdvanceUnionMemberPosNtf->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCAdvanceUnionMemberPosNtf->memberNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCAdvanceUnionMemberPosNtf->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posSeq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAdvanceUnionMemberPosNtf->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAdvanceUnionMemberPosNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGReduceUnionMemberPosReq(void* pData)
{
	CGReduceUnionMemberPosReq* pkCGReduceUnionMemberPosReq = (CGReduceUnionMemberPosReq*)(pData);

	//EnCode reducedUiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGReduceUnionMemberPosReq->reducedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGReduceUnionMemberPosReq(void* pData)
{
	CGReduceUnionMemberPosReq* pkCGReduceUnionMemberPosReq = (CGReduceUnionMemberPosReq*)(pData);

	//DeCode reducedUiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGReduceUnionMemberPosReq->reducedUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGReduceUnionMemberPosReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCReduceUnionMemberPosRst(void* pData)
{
	GCReduceUnionMemberPosRst* pkGCReduceUnionMemberPosRst = (GCReduceUnionMemberPosRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReduceUnionMemberPosRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCReduceUnionMemberPosRst(void* pData)
{
	GCReduceUnionMemberPosRst* pkGCReduceUnionMemberPosRst = (GCReduceUnionMemberPosRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReduceUnionMemberPosRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCReduceUnionMemberPosRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCReduceUnionMemberPosNtf(void* pData)
{
	GCReduceUnionMemberPosNtf* pkGCReduceUnionMemberPosNtf = (GCReduceUnionMemberPosNtf*)(pData);

	//EnCode memberNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCReduceUnionMemberPosNtf->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberName
	if(MAX_PLAYERNAME_LEN < pkGCReduceUnionMemberPosNtf->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCReduceUnionMemberPosNtf->memberNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCReduceUnionMemberPosNtf->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posSeq
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCReduceUnionMemberPosNtf->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCReduceUnionMemberPosNtf(void* pData)
{
	GCReduceUnionMemberPosNtf* pkGCReduceUnionMemberPosNtf = (GCReduceUnionMemberPosNtf*)(pData);

	//DeCode memberNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCReduceUnionMemberPosNtf->memberNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberName
	if(MAX_PLAYERNAME_LEN < pkGCReduceUnionMemberPosNtf->memberNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCReduceUnionMemberPosNtf->memberNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCReduceUnionMemberPosNtf->memberName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posSeq
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCReduceUnionMemberPosNtf->posSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCReduceUnionMemberPosNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUnionChannelSpeekReq(void* pData)
{
	CGUnionChannelSpeekReq* pkCGUnionChannelSpeekReq = (CGUnionChannelSpeekReq*)(pData);

	//EnCode contentLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGUnionChannelSpeekReq->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if(MAX_CHATDATA_LEN < pkCGUnionChannelSpeekReq->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGUnionChannelSpeekReq->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGUnionChannelSpeekReq->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUnionChannelSpeekReq(void* pData)
{
	CGUnionChannelSpeekReq* pkCGUnionChannelSpeekReq = (CGUnionChannelSpeekReq*)(pData);

	//DeCode contentLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGUnionChannelSpeekReq->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if(MAX_CHATDATA_LEN < pkCGUnionChannelSpeekReq->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGUnionChannelSpeekReq->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGUnionChannelSpeekReq->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGUnionChannelSpeekReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionChannelSpeekRst(void* pData)
{
	GCUnionChannelSpeekRst* pkGCUnionChannelSpeekRst = (GCUnionChannelSpeekRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUnionChannelSpeekRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionChannelSpeekRst(void* pData)
{
	GCUnionChannelSpeekRst* pkGCUnionChannelSpeekRst = (GCUnionChannelSpeekRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUnionChannelSpeekRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionChannelSpeekRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionChannelSpeekNtf(void* pData)
{
	GCUnionChannelSpeekNtf* pkGCUnionChannelSpeekNtf = (GCUnionChannelSpeekNtf*)(pData);

	//EnCode speekerNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionChannelSpeekNtf->speekerNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode speekerName
	if(MAX_PLAYERNAME_LEN < pkGCUnionChannelSpeekNtf->speekerNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionChannelSpeekNtf->speekerNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCUnionChannelSpeekNtf->speekerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionChannelSpeekNtf->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if(MAX_CHATDATA_LEN < pkGCUnionChannelSpeekNtf->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionChannelSpeekNtf->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCUnionChannelSpeekNtf->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionChannelSpeekNtf(void* pData)
{
	GCUnionChannelSpeekNtf* pkGCUnionChannelSpeekNtf = (GCUnionChannelSpeekNtf*)(pData);

	//DeCode speekerNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionChannelSpeekNtf->speekerNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode speekerName
	if(MAX_PLAYERNAME_LEN < pkGCUnionChannelSpeekNtf->speekerNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionChannelSpeekNtf->speekerNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCUnionChannelSpeekNtf->speekerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionChannelSpeekNtf->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if(MAX_CHATDATA_LEN < pkGCUnionChannelSpeekNtf->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionChannelSpeekNtf->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCUnionChannelSpeekNtf->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionChannelSpeekNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUpdateUnionPosCfgReq(void* pData)
{
	CGUpdateUnionPosCfgReq* pkCGUpdateUnionPosCfgReq = (CGUpdateUnionPosCfgReq*)(pData);

	//EnCode posListLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGUpdateUnionPosCfgReq->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posList
	if(10 < pkCGUpdateUnionPosCfgReq->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkCGUpdateUnionPosCfgReq->posListLen; ++i)
	{
		if(EnCode__UnionPosRight(&(pkCGUpdateUnionPosCfgReq->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUpdateUnionPosCfgReq(void* pData)
{
	CGUpdateUnionPosCfgReq* pkCGUpdateUnionPosCfgReq = (CGUpdateUnionPosCfgReq*)(pData);

	//DeCode posListLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGUpdateUnionPosCfgReq->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posList
	if(10 < pkCGUpdateUnionPosCfgReq->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkCGUpdateUnionPosCfgReq->posListLen; ++i)
	{
		if(DeCode__UnionPosRight(&(pkCGUpdateUnionPosCfgReq->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(CGUpdateUnionPosCfgReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUpdateUnionPosCfgRst(void* pData)
{
	GCUpdateUnionPosCfgRst* pkGCUpdateUnionPosCfgRst = (GCUpdateUnionPosCfgRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUpdateUnionPosCfgRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUpdateUnionPosCfgRst(void* pData)
{
	GCUpdateUnionPosCfgRst* pkGCUpdateUnionPosCfgRst = (GCUpdateUnionPosCfgRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUpdateUnionPosCfgRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUpdateUnionPosCfgRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUpdateUnionPosCfgNtf(void* pData)
{
	GCUpdateUnionPosCfgNtf* pkGCUpdateUnionPosCfgNtf = (GCUpdateUnionPosCfgNtf*)(pData);

	//EnCode posListLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUpdateUnionPosCfgNtf->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode posList
	if(10 < pkGCUpdateUnionPosCfgNtf->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCUpdateUnionPosCfgNtf->posListLen; ++i)
	{
		if(EnCode__UnionPosRight(&(pkGCUpdateUnionPosCfgNtf->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUpdateUnionPosCfgNtf(void* pData)
{
	GCUpdateUnionPosCfgNtf* pkGCUpdateUnionPosCfgNtf = (GCUpdateUnionPosCfgNtf*)(pData);

	//DeCode posListLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUpdateUnionPosCfgNtf->posListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode posList
	if(10 < pkGCUpdateUnionPosCfgNtf->posListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCUpdateUnionPosCfgNtf->posListLen; ++i)
	{
		if(DeCode__UnionPosRight(&(pkGCUpdateUnionPosCfgNtf->posList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCUpdateUnionPosCfgNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGForbidUnionSpeekReq(void* pData)
{
	CGForbidUnionSpeekReq* pkCGForbidUnionSpeekReq = (CGForbidUnionSpeekReq*)(pData);

	//EnCode forbidUiid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGForbidUnionSpeekReq->forbidUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGForbidUnionSpeekReq(void* pData)
{
	CGForbidUnionSpeekReq* pkCGForbidUnionSpeekReq = (CGForbidUnionSpeekReq*)(pData);

	//DeCode forbidUiid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGForbidUnionSpeekReq->forbidUiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGForbidUnionSpeekReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCForbidUnionSpeekRst(void* pData)
{
	GCForbidUnionSpeekRst* pkGCForbidUnionSpeekRst = (GCForbidUnionSpeekRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCForbidUnionSpeekRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCForbidUnionSpeekRst(void* pData)
{
	GCForbidUnionSpeekRst* pkGCForbidUnionSpeekRst = (GCForbidUnionSpeekRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCForbidUnionSpeekRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCForbidUnionSpeekRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCForbidUnionSpeekNtf(void* pData)
{
	GCForbidUnionSpeekNtf* pkGCForbidUnionSpeekNtf = (GCForbidUnionSpeekNtf*)(pData);

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCForbidUnionSpeekNtf->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode name
	if(MAX_PLAYERNAME_LEN < pkGCForbidUnionSpeekNtf->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCForbidUnionSpeekNtf->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCForbidUnionSpeekNtf->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCForbidUnionSpeekNtf(void* pData)
{
	GCForbidUnionSpeekNtf* pkGCForbidUnionSpeekNtf = (GCForbidUnionSpeekNtf*)(pData);

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCForbidUnionSpeekNtf->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode name
	if(MAX_PLAYERNAME_LEN < pkGCForbidUnionSpeekNtf->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCForbidUnionSpeekNtf->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCForbidUnionSpeekNtf->name), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCForbidUnionSpeekNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionSkills(void* pData)
{
	GCUnionSkills* pkGCUnionSkills = (GCUnionSkills*)(pData);

	//EnCode unionID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUnionSkills->unionID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionSkills->skillLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode unionSkill
	if(MAX_UNION_SKILL_NUM < pkGCUnionSkills->skillLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionSkills->skillLen;
	if(!m_kPackage.Pack("UINT", &(pkGCUnionSkills->unionSkill), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionSkills(void* pData)
{
	GCUnionSkills* pkGCUnionSkills = (GCUnionSkills*)(pData);

	//DeCode unionID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUnionSkills->unionID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionSkills->skillLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode unionSkill
	if(MAX_UNION_SKILL_NUM < pkGCUnionSkills->skillLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionSkills->skillLen;
	if(!m_kPackage.UnPack("UINT", &(pkGCUnionSkills->unionSkill), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionSkills);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGIssueUnionTaskReq(void* pData)
{
	CGIssueUnionTaskReq* pkCGIssueUnionTaskReq = (CGIssueUnionTaskReq*)(pData);

	//EnCode taskValidInterval
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGIssueUnionTaskReq->taskValidInterval), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGIssueUnionTaskReq(void* pData)
{
	CGIssueUnionTaskReq* pkCGIssueUnionTaskReq = (CGIssueUnionTaskReq*)(pData);

	//DeCode taskValidInterval
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGIssueUnionTaskReq->taskValidInterval), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGIssueUnionTaskReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCIssueUnionTaskRst(void* pData)
{
	GCIssueUnionTaskRst* pkGCIssueUnionTaskRst = (GCIssueUnionTaskRst*)(pData);

	//EnCode result
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCIssueUnionTaskRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCIssueUnionTaskRst(void* pData)
{
	GCIssueUnionTaskRst* pkGCIssueUnionTaskRst = (GCIssueUnionTaskRst*)(pData);

	//DeCode result
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCIssueUnionTaskRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCIssueUnionTaskRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCIssueUnionTaskNtf(void* pData)
{
	GCIssueUnionTaskNtf* pkGCIssueUnionTaskNtf = (GCIssueUnionTaskNtf*)(pData);

	//EnCode costActive
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCIssueUnionTaskNtf->costActive), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode taskEndTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCIssueUnionTaskNtf->taskEndTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCIssueUnionTaskNtf(void* pData)
{
	GCIssueUnionTaskNtf* pkGCIssueUnionTaskNtf = (GCIssueUnionTaskNtf*)(pData);

	//DeCode costActive
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCIssueUnionTaskNtf->costActive), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode taskEndTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCIssueUnionTaskNtf->taskEndTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCIssueUnionTaskNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDisplayUnionTasks(void* pData)
{
	GCDisplayUnionTasks* pkGCDisplayUnionTasks = (GCDisplayUnionTasks*)(pData);

	//EnCode result
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDisplayUnionTasks->result), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode taskLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCDisplayUnionTasks->taskLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode tasks
	if(MAX_ISSUE_TASK_NUM < pkGCDisplayUnionTasks->taskLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCDisplayUnionTasks->taskLen;
	if(!m_kPackage.Pack("UINT", &(pkGCDisplayUnionTasks->tasks), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode taskEndTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDisplayUnionTasks->taskEndTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDisplayUnionTasks(void* pData)
{
	GCDisplayUnionTasks* pkGCDisplayUnionTasks = (GCDisplayUnionTasks*)(pData);

	//DeCode result
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDisplayUnionTasks->result), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode taskLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCDisplayUnionTasks->taskLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode tasks
	if(MAX_ISSUE_TASK_NUM < pkGCDisplayUnionTasks->taskLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCDisplayUnionTasks->taskLen;
	if(!m_kPackage.UnPack("UINT", &(pkGCDisplayUnionTasks->tasks), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode taskEndTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDisplayUnionTasks->taskEndTime), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDisplayUnionTasks);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGLearnUnionSkillReq(void* pData)
{
	CGLearnUnionSkillReq* pkCGLearnUnionSkillReq = (CGLearnUnionSkillReq*)(pData);

	//EnCode skillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGLearnUnionSkillReq->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGLearnUnionSkillReq(void* pData)
{
	CGLearnUnionSkillReq* pkCGLearnUnionSkillReq = (CGLearnUnionSkillReq*)(pData);

	//DeCode skillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGLearnUnionSkillReq->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGLearnUnionSkillReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGAdvanceUnionLevelReq(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGAdvanceUnionLevelReq(void* pData)
{
	(pData);
	return sizeof(CGAdvanceUnionLevelReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAdvanceUnionLevelRst(void* pData)
{
	GCAdvanceUnionLevelRst* pkGCAdvanceUnionLevelRst = (GCAdvanceUnionLevelRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAdvanceUnionLevelRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAdvanceUnionLevelRst(void* pData)
{
	GCAdvanceUnionLevelRst* pkGCAdvanceUnionLevelRst = (GCAdvanceUnionLevelRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAdvanceUnionLevelRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAdvanceUnionLevelRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionLevelNtf(void* pData)
{
	GCUnionLevelNtf* pkGCUnionLevelNtf = (GCUnionLevelNtf*)(pData);

	//EnCode level
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUnionLevelNtf->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode active
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUnionLevelNtf->active), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionLevelNtf(void* pData)
{
	GCUnionLevelNtf* pkGCUnionLevelNtf = (GCUnionLevelNtf*)(pData);

	//DeCode level
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUnionLevelNtf->level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode active
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUnionLevelNtf->active), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionLevelNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPostUnionBulletinReq(void* pData)
{
	CGPostUnionBulletinReq* pkCGPostUnionBulletinReq = (CGPostUnionBulletinReq*)(pData);

	//EnCode seq
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGPostUnionBulletinReq->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode end
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGPostUnionBulletinReq->end), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGPostUnionBulletinReq->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if(400 < pkCGPostUnionBulletinReq->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGPostUnionBulletinReq->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGPostUnionBulletinReq->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPostUnionBulletinReq(void* pData)
{
	CGPostUnionBulletinReq* pkCGPostUnionBulletinReq = (CGPostUnionBulletinReq*)(pData);

	//DeCode seq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGPostUnionBulletinReq->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode end
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGPostUnionBulletinReq->end), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGPostUnionBulletinReq->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if(400 < pkCGPostUnionBulletinReq->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGPostUnionBulletinReq->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGPostUnionBulletinReq->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPostUnionBulletinReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPostUnionBulletinRst(void* pData)
{
	GCPostUnionBulletinRst* pkGCPostUnionBulletinRst = (GCPostUnionBulletinRst*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPostUnionBulletinRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPostUnionBulletinRst(void* pData)
{
	GCPostUnionBulletinRst* pkGCPostUnionBulletinRst = (GCPostUnionBulletinRst*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPostUnionBulletinRst->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPostUnionBulletinRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionBulletinNtf(void* pData)
{
	GCUnionBulletinNtf* pkGCUnionBulletinNtf = (GCUnionBulletinNtf*)(pData);

	//EnCode seq
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionBulletinNtf->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUnionBulletinNtf->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode content
	if(400 < pkGCUnionBulletinNtf->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionBulletinNtf->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCUnionBulletinNtf->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionBulletinNtf(void* pData)
{
	GCUnionBulletinNtf* pkGCUnionBulletinNtf = (GCUnionBulletinNtf*)(pData);

	//DeCode seq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionBulletinNtf->seq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUnionBulletinNtf->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode content
	if(400 < pkGCUnionBulletinNtf->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCUnionBulletinNtf->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCUnionBulletinNtf->content), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionBulletinNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionMemberOnOffLineNtf(void* pData)
{
	GCUnionMemberOnOffLineNtf* pkGCUnionMemberOnOffLineNtf = (GCUnionMemberOnOffLineNtf*)(pData);

	//EnCode flag
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCUnionMemberOnOffLineNtf->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUnionMemberOnOffLineNtf->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionMemberOnOffLineNtf(void* pData)
{
	GCUnionMemberOnOffLineNtf* pkGCUnionMemberOnOffLineNtf = (GCUnionMemberOnOffLineNtf*)(pData);

	//DeCode flag
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCUnionMemberOnOffLineNtf->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUnionMemberOnOffLineNtf->uiid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionMemberOnOffLineNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGLeaveWaitQueue(void* pData)
{
	CGLeaveWaitQueue* pkCGLeaveWaitQueue = (CGLeaveWaitQueue*)(pData);

	//EnCode leavePlayer
	if(EnCode__PlayerID(&(pkCGLeaveWaitQueue->leavePlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGLeaveWaitQueue(void* pData)
{
	CGLeaveWaitQueue* pkCGLeaveWaitQueue = (CGLeaveWaitQueue*)(pData);

	//DeCode leavePlayer
	if(DeCode__PlayerID(&(pkCGLeaveWaitQueue->leavePlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGLeaveWaitQueue);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGUsePartionSkill(void* pData)
{
	CGUsePartionSkill* pkCGUsePartionSkill = (CGUsePartionSkill*)(pData);

	//EnCode partionSkillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUsePartionSkill->partionSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetID
	if(EnCode__PlayerID(&(pkCGUsePartionSkill->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode clientTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUsePartionSkill->clientTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGUsePartionSkill->targetPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGUsePartionSkill->targetPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode sequenceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGUsePartionSkill->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGUsePartionSkill(void* pData)
{
	CGUsePartionSkill* pkCGUsePartionSkill = (CGUsePartionSkill*)(pData);

	//DeCode partionSkillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUsePartionSkill->partionSkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetID
	if(DeCode__PlayerID(&(pkCGUsePartionSkill->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode clientTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUsePartionSkill->clientTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGUsePartionSkill->targetPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGUsePartionSkill->targetPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode sequenceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGUsePartionSkill->sequenceID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGUsePartionSkill);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCLearnUnionSkillRst(void* pData)
{
	GCLearnUnionSkillRst* pkGCLearnUnionSkillRst = (GCLearnUnionSkillRst*)(pData);

	//EnCode result
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCLearnUnionSkillRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCLearnUnionSkillRst->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode costActive
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCLearnUnionSkillRst->costActive), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCLearnUnionSkillRst(void* pData)
{
	GCLearnUnionSkillRst* pkGCLearnUnionSkillRst = (GCLearnUnionSkillRst*)(pData);

	//DeCode result
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCLearnUnionSkillRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCLearnUnionSkillRst->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode costActive
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCLearnUnionSkillRst->costActive), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCLearnUnionSkillRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCLearnUnionSkillNtf(void* pData)
{
	GCLearnUnionSkillNtf* pkGCLearnUnionSkillNtf = (GCLearnUnionSkillNtf*)(pData);

	//EnCode skillID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCLearnUnionSkillNtf->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode costActive
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCLearnUnionSkillNtf->costActive), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCLearnUnionSkillNtf(void* pData)
{
	GCLearnUnionSkillNtf* pkGCLearnUnionSkillNtf = (GCLearnUnionSkillNtf*)(pData);

	//DeCode skillID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCLearnUnionSkillNtf->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode costActive
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCLearnUnionSkillNtf->costActive), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCLearnUnionSkillNtf);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChanageUnionActive(void* pData)
{
	GCChanageUnionActive* pkGCChanageUnionActive = (GCChanageUnionActive*)(pData);

	//EnCode val
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChanageUnionActive->val), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChanageUnionActive(void* pData)
{
	GCChanageUnionActive* pkGCChanageUnionActive = (GCChanageUnionActive*)(pData);

	//DeCode val
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChanageUnionActive->val), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChanageUnionActive);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUnionUISelectCmd(void* pData)
{
	GCUnionUISelectCmd* pkGCUnionUISelectCmd = (GCUnionUISelectCmd*)(pData);

	//EnCode cmdID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUnionUISelectCmd->cmdID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUnionUISelectCmd(void* pData)
{
	GCUnionUISelectCmd* pkGCUnionUISelectCmd = (GCUnionUISelectCmd*)(pData);

	//DeCode cmdID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUnionUISelectCmd->cmdID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUnionUISelectCmd);
}

size_t	MUX_PROTO::CliProtocol::EnCode__LogEntryParam(void* pData)
{
	LogEntryParam* pkLogEntryParam = (LogEntryParam*)(pData);

	//EnCode paramLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkLogEntryParam->paramLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode param
	if(32 < pkLogEntryParam->paramLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkLogEntryParam->paramLen;
	if(!m_kPackage.Pack("CHAR", &(pkLogEntryParam->param), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__LogEntryParam(void* pData)
{
	LogEntryParam* pkLogEntryParam = (LogEntryParam*)(pData);

	//DeCode paramLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkLogEntryParam->paramLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode param
	if(32 < pkLogEntryParam->paramLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkLogEntryParam->paramLen;
	if(!m_kPackage.UnPack("CHAR", &(pkLogEntryParam->param), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(LogEntryParam);
}

size_t	MUX_PROTO::CliProtocol::EnCode__UnionLogEntry(void* pData)
{
	UnionLogEntry* pkUnionLogEntry = (UnionLogEntry*)(pData);

	//EnCode eventId
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUnionLogEntry->eventId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode datetime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkUnionLogEntry->datetime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode param1
	if(EnCode__LogEntryParam(&(pkUnionLogEntry->param1)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode param2
	if(EnCode__LogEntryParam(&(pkUnionLogEntry->param2)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__UnionLogEntry(void* pData)
{
	UnionLogEntry* pkUnionLogEntry = (UnionLogEntry*)(pData);

	//DeCode eventId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUnionLogEntry->eventId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode datetime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkUnionLogEntry->datetime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode param1
	if(DeCode__LogEntryParam(&(pkUnionLogEntry->param1)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode param2
	if(DeCode__LogEntryParam(&(pkUnionLogEntry->param2)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(UnionLogEntry);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryUnionLogReq(void* pData)
{
	CGQueryUnionLogReq* pkCGQueryUnionLogReq = (CGQueryUnionLogReq*)(pData);

	//EnCode bookmark
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryUnionLogReq->bookmark), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryUnionLogReq(void* pData)
{
	CGQueryUnionLogReq* pkCGQueryUnionLogReq = (CGQueryUnionLogReq*)(pData);

	//DeCode bookmark
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryUnionLogReq->bookmark), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryUnionLogReq);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryUnionLogRst(void* pData)
{
	GCQueryUnionLogRst* pkGCQueryUnionLogRst = (GCQueryUnionLogRst*)(pData);

	//EnCode bookmark
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCQueryUnionLogRst->bookmark), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode logListLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCQueryUnionLogRst->logListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode logList
	if(6 < pkGCQueryUnionLogRst->logListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCQueryUnionLogRst->logListLen; ++i)
	{
		if(EnCode__UnionLogEntry(&(pkGCQueryUnionLogRst->logList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryUnionLogRst(void* pData)
{
	GCQueryUnionLogRst* pkGCQueryUnionLogRst = (GCQueryUnionLogRst*)(pData);

	//DeCode bookmark
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCQueryUnionLogRst->bookmark), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode logListLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCQueryUnionLogRst->logListLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode logList
	if(6 < pkGCQueryUnionLogRst->logListLen)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCQueryUnionLogRst->logListLen; ++i)
	{
		if(DeCode__UnionLogEntry(&(pkGCQueryUnionLogRst->logList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCQueryUnionLogRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCWaitQueueResult(void* pData)
{
	GCWaitQueueResult* pkGCWaitQueueResult = (GCWaitQueueResult*)(pData);

	//EnCode waitPlayer
	if(EnCode__PlayerID(&(pkGCWaitQueueResult->waitPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode waitIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCWaitQueueResult->waitIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCWaitQueueResult(void* pData)
{
	GCWaitQueueResult* pkGCWaitQueueResult = (GCWaitQueueResult*)(pData);

	//DeCode waitPlayer
	if(DeCode__PlayerID(&(pkGCWaitQueueResult->waitPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode waitIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCWaitQueueResult->waitIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCWaitQueueResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCUsePartionSkillRst(void* pData)
{
	GCUsePartionSkillRst* pkGCUsePartionSkillRst = (GCUsePartionSkillRst*)(pData);

	//EnCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode scopeFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->scopeFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lPlayerID
	if(EnCode__PlayerID(&(pkGCUsePartionSkillRst->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode battleFlag
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode skillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lCooldownTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->lCooldownTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode consumeItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->consumeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetID
	if(EnCode__PlayerID(&(pkGCUsePartionSkillRst->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode lHpSubed
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->lHpSubed), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lHpLeft
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->lHpLeft), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerHP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->nAtkPlayerHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nAtkPlayerMP
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->nAtkPlayerMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode targetRewardID
	if(EnCode__PlayerID(&(pkGCUsePartionSkillRst->targetRewardID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode aoeMainTargetID
	if(EnCode__PlayerID(&(pkGCUsePartionSkillRst->aoeMainTargetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode StartTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->StartTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MidwaySpeed
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->MidwaySpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode EndTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->EndTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MinMidwayTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCUsePartionSkillRst->MinMidwayTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FinalPositionPosX
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUsePartionSkillRst->FinalPositionPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FinalPositionPosY
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCUsePartionSkillRst->FinalPositionPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCUsePartionSkillRst(void* pData)
{
	GCUsePartionSkillRst* pkGCUsePartionSkillRst = (GCUsePartionSkillRst*)(pData);

	//DeCode nErrCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->nErrCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode scopeFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->scopeFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lPlayerID
	if(DeCode__PlayerID(&(pkGCUsePartionSkillRst->lPlayerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode battleFlag
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->battleFlag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode skillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->skillID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lCooldownTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->lCooldownTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode consumeItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->consumeItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetID
	if(DeCode__PlayerID(&(pkGCUsePartionSkillRst->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode lHpSubed
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->lHpSubed), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lHpLeft
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->lHpLeft), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerHP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->nAtkPlayerHP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nAtkPlayerMP
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->nAtkPlayerMP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode targetRewardID
	if(DeCode__PlayerID(&(pkGCUsePartionSkillRst->targetRewardID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode aoeMainTargetID
	if(DeCode__PlayerID(&(pkGCUsePartionSkillRst->aoeMainTargetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode StartTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->StartTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MidwaySpeed
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->MidwaySpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode EndTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->EndTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MinMidwayTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCUsePartionSkillRst->MinMidwayTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FinalPositionPosX
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUsePartionSkillRst->FinalPositionPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FinalPositionPosY
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCUsePartionSkillRst->FinalPositionPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCUsePartionSkillRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangePetName(void* pData)
{
	CGChangePetName* pkCGChangePetName = (CGChangePetName*)(pData);

	//EnCode petNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChangePetName->petNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode petName
	if(MAX_NAME_SIZE < pkCGChangePetName->petNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGChangePetName->petNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGChangePetName->petName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangePetName(void* pData)
{
	CGChangePetName* pkCGChangePetName = (CGChangePetName*)(pData);

	//DeCode petNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChangePetName->petNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode petName
	if(MAX_NAME_SIZE < pkCGChangePetName->petNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGChangePetName->petNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGChangePetName->petName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangePetName);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGClosePet(void* pData)
{
	CGClosePet* pkCGClosePet = (CGClosePet*)(pData);

	//EnCode petOwner
	if(EnCode__PlayerID(&(pkCGClosePet->petOwner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGClosePet(void* pData)
{
	CGClosePet* pkCGClosePet = (CGClosePet*)(pData);

	//DeCode petOwner
	if(DeCode__PlayerID(&(pkCGClosePet->petOwner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGClosePet);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangePetSkillState(void* pData)
{
	CGChangePetSkillState* pkCGChangePetSkillState = (CGChangePetSkillState*)(pData);

	//EnCode skillState
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChangePetSkillState->skillState), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangePetSkillState(void* pData)
{
	CGChangePetSkillState* pkCGChangePetSkillState = (CGChangePetSkillState*)(pData);

	//DeCode skillState
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChangePetSkillState->skillState), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangePetSkillState);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChangePetImage(void* pData)
{
	CGChangePetImage* pkCGChangePetImage = (CGChangePetImage*)(pData);

	//EnCode imageID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGChangePetImage->imageID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChangePetImage(void* pData)
{
	CGChangePetImage* pkCGChangePetImage = (CGChangePetImage*)(pData);

	//DeCode imageID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGChangePetImage->imageID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChangePetImage);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSummonPet(void* pData)
{
	CGSummonPet* pkCGSummonPet = (CGSummonPet*)(pData);

	//EnCode petOwner
	if(EnCode__PlayerID(&(pkCGSummonPet->petOwner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSummonPet(void* pData)
{
	CGSummonPet* pkCGSummonPet = (CGSummonPet*)(pData);

	//DeCode petOwner
	if(DeCode__PlayerID(&(pkCGSummonPet->petOwner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSummonPet);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPetinfoInstable(void* pData)
{
	GCPetinfoInstable* pkGCPetinfoInstable = (GCPetinfoInstable*)(pData);

	//EnCode petLevel
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPetinfoInstable->petLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode petExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPetinfoInstable->petExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode petHunger
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPetinfoInstable->petHunger), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode petSkillSet
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPetinfoInstable->petSkillSet), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPetinfoInstable(void* pData)
{
	GCPetinfoInstable* pkGCPetinfoInstable = (GCPetinfoInstable*)(pData);

	//DeCode petLevel
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPetinfoInstable->petLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode petExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPetinfoInstable->petExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode petHunger
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPetinfoInstable->petHunger), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode petSkillSet
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPetinfoInstable->petSkillSet), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPetinfoInstable);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPetAbility(void* pData)
{
	GCPetAbility* pkGCPetAbility = (GCPetAbility*)(pData);

	//EnCode skillValid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPetAbility->skillValid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lookValidLen
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPetAbility->lookValidLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lookValid
	if(MAX_PET_IMAGE_LEN < pkGCPetAbility->lookValidLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPetAbility->lookValidLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCPetAbility->lookValid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPetAbility(void* pData)
{
	GCPetAbility* pkGCPetAbility = (GCPetAbility*)(pData);

	//DeCode skillValid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPetAbility->skillValid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lookValidLen
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPetAbility->lookValidLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lookValid
	if(MAX_PET_IMAGE_LEN < pkGCPetAbility->lookValidLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPetAbility->lookValidLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCPetAbility->lookValid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPetAbility);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPetinfoStable(void* pData)
{
	GCPetinfoStable* pkGCPetinfoStable = (GCPetinfoStable*)(pData);

	//EnCode petOwnerID
	if(EnCode__PlayerID(&(pkGCPetinfoStable->petOwnerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode petNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPetinfoStable->petNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode petName
	if(MAX_NAME_SIZE < pkGCPetinfoStable->petNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPetinfoStable->petNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCPetinfoStable->petName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode lookPos
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPetinfoStable->lookPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isSummoned
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCPetinfoStable->isSummoned), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPetinfoStable(void* pData)
{
	GCPetinfoStable* pkGCPetinfoStable = (GCPetinfoStable*)(pData);

	//DeCode petOwnerID
	if(DeCode__PlayerID(&(pkGCPetinfoStable->petOwnerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode petNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPetinfoStable->petNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode petName
	if(MAX_NAME_SIZE < pkGCPetinfoStable->petNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPetinfoStable->petNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCPetinfoStable->petName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode lookPos
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPetinfoStable->lookPos), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isSummoned
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCPetinfoStable->isSummoned), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPetinfoStable);
}

size_t	MUX_PROTO::CliProtocol::EnCode__RankChartItem(void* pData)
{
	RankChartItem* pkRankChartItem = (RankChartItem*)(pData);

	//EnCode itemUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRankChartItem->itemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemNameLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRankChartItem->itemNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemName
	if(MAX_NAME_SIZE < pkRankChartItem->itemNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRankChartItem->itemNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkRankChartItem->itemName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rankInfo
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRankChartItem->rankInfo), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemClassLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkRankChartItem->itemClassLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemClass
	if(MAX_NAME_SIZE < pkRankChartItem->itemClassLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRankChartItem->itemClassLen;
	if(!m_kPackage.Pack("CHAR", &(pkRankChartItem->itemClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemLevel
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkRankChartItem->itemLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__RankChartItem(void* pData)
{
	RankChartItem* pkRankChartItem = (RankChartItem*)(pData);

	//DeCode itemUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRankChartItem->itemUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRankChartItem->itemNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemName
	if(MAX_NAME_SIZE < pkRankChartItem->itemNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRankChartItem->itemNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRankChartItem->itemName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rankInfo
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRankChartItem->rankInfo), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemClassLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkRankChartItem->itemClassLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemClass
	if(MAX_NAME_SIZE < pkRankChartItem->itemClassLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRankChartItem->itemClassLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRankChartItem->itemClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemLevel
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkRankChartItem->itemLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(RankChartItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryRankInfoByPageIndex(void* pData)
{
	CGQueryRankInfoByPageIndex* pkCGQueryRankInfoByPageIndex = (CGQueryRankInfoByPageIndex*)(pData);

	//EnCode rankChartLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryRankInfoByPageIndex->rankChartLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rankChartID
	if(MAX_CHARTID_LEN < pkCGQueryRankInfoByPageIndex->rankChartLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGQueryRankInfoByPageIndex->rankChartLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGQueryRankInfoByPageIndex->rankChartID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ChartPageIndex
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkCGQueryRankInfoByPageIndex->ChartPageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryRankInfoByPageIndex(void* pData)
{
	CGQueryRankInfoByPageIndex* pkCGQueryRankInfoByPageIndex = (CGQueryRankInfoByPageIndex*)(pData);

	//DeCode rankChartLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryRankInfoByPageIndex->rankChartLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rankChartID
	if(MAX_CHARTID_LEN < pkCGQueryRankInfoByPageIndex->rankChartLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGQueryRankInfoByPageIndex->rankChartLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGQueryRankInfoByPageIndex->rankChartID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ChartPageIndex
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkCGQueryRankInfoByPageIndex->ChartPageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryRankInfoByPageIndex);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryRankInfoByPlayerName(void* pData)
{
	CGQueryRankInfoByPlayerName* pkCGQueryRankInfoByPlayerName = (CGQueryRankInfoByPlayerName*)(pData);

	//EnCode rankChartLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryRankInfoByPlayerName->rankChartLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rankChartID
	if(MAX_CHARTID_LEN < pkCGQueryRankInfoByPlayerName->rankChartLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGQueryRankInfoByPlayerName->rankChartLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGQueryRankInfoByPlayerName->rankChartID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerNameLen
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkCGQueryRankInfoByPlayerName->playerNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerName
	if(MAX_NAME_SIZE < pkCGQueryRankInfoByPlayerName->playerNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGQueryRankInfoByPlayerName->playerNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGQueryRankInfoByPlayerName->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryRankInfoByPlayerName(void* pData)
{
	CGQueryRankInfoByPlayerName* pkCGQueryRankInfoByPlayerName = (CGQueryRankInfoByPlayerName*)(pData);

	//DeCode rankChartLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryRankInfoByPlayerName->rankChartLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rankChartID
	if(MAX_CHARTID_LEN < pkCGQueryRankInfoByPlayerName->rankChartLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGQueryRankInfoByPlayerName->rankChartLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGQueryRankInfoByPlayerName->rankChartID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkCGQueryRankInfoByPlayerName->playerNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerName
	if(MAX_NAME_SIZE < pkCGQueryRankInfoByPlayerName->playerNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGQueryRankInfoByPlayerName->playerNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGQueryRankInfoByPlayerName->playerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryRankInfoByPlayerName);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRankInfoByPageRst(void* pData)
{
	GCRankInfoByPageRst* pkGCRankInfoByPageRst = (GCRankInfoByPageRst*)(pData);

	//EnCode rankPageIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCRankInfoByPageRst->rankPageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rankCount
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCRankInfoByPageRst->rankCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pageEnd
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCRankInfoByPageRst->pageEnd), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rankInfoArr
	if(MAX_CHARTPAGE_LEN < pkGCRankInfoByPageRst->rankCount)
	{
		return FAILEDRETCODE;
	}

	for(CHAR i = 0; i < pkGCRankInfoByPageRst->rankCount; ++i)
	{
		if(EnCode__RankChartItem(&(pkGCRankInfoByPageRst->rankInfoArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRankInfoByPageRst(void* pData)
{
	GCRankInfoByPageRst* pkGCRankInfoByPageRst = (GCRankInfoByPageRst*)(pData);

	//DeCode rankPageIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRankInfoByPageRst->rankPageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rankCount
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRankInfoByPageRst->rankCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pageEnd
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCRankInfoByPageRst->pageEnd), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rankInfoArr
	if(MAX_CHARTPAGE_LEN < pkGCRankInfoByPageRst->rankCount)
	{
		return FAILEDRETCODE;
	}

	for(CHAR i = 0; i < pkGCRankInfoByPageRst->rankCount; ++i)
	{
		if(DeCode__RankChartItem(&(pkGCRankInfoByPageRst->rankInfoArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCRankInfoByPageRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryRankPlayerByNameRst(void* pData)
{
	GCQueryRankPlayerByNameRst* pkGCQueryRankPlayerByNameRst = (GCQueryRankPlayerByNameRst*)(pData);

	//EnCode queryRst
	size_t unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCQueryRankPlayerByNameRst->queryRst), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rankIndex
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCQueryRankPlayerByNameRst->rankIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode rankPlayer
	if(EnCode__RankChartItem(&(pkGCQueryRankPlayerByNameRst->rankPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryRankPlayerByNameRst(void* pData)
{
	GCQueryRankPlayerByNameRst* pkGCQueryRankPlayerByNameRst = (GCQueryRankPlayerByNameRst*)(pData);

	//DeCode queryRst
	size_t unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCQueryRankPlayerByNameRst->queryRst), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rankIndex
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCQueryRankPlayerByNameRst->rankIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode rankPlayer
	if(DeCode__RankChartItem(&(pkGCQueryRankPlayerByNameRst->rankPlayer)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCQueryRankPlayerByNameRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSendMailToPlayer(void* pData)
{
	CGSendMailToPlayer* pkCGSendMailToPlayer = (CGSendMailToPlayer*)(pData);

	//EnCode titleLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGSendMailToPlayer->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szMailTitle
	if(MAX_MAIL_TITILE_LEN < pkCGSendMailToPlayer->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGSendMailToPlayer->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGSendMailToPlayer->szMailTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGSendMailToPlayer->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szMailContent
	if(MAX_MAIL_CONTENT_LEN < pkCGSendMailToPlayer->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGSendMailToPlayer->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGSendMailToPlayer->szMailContent), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkCGSendMailToPlayer->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szRecvMailPlayerName
	if(MAX_NAME_SIZE < pkCGSendMailToPlayer->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGSendMailToPlayer->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGSendMailToPlayer->szRecvMailPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSendMailToPlayer(void* pData)
{
	CGSendMailToPlayer* pkCGSendMailToPlayer = (CGSendMailToPlayer*)(pData);

	//DeCode titleLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGSendMailToPlayer->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szMailTitle
	if(MAX_MAIL_TITILE_LEN < pkCGSendMailToPlayer->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGSendMailToPlayer->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGSendMailToPlayer->szMailTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGSendMailToPlayer->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szMailContent
	if(MAX_MAIL_CONTENT_LEN < pkCGSendMailToPlayer->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGSendMailToPlayer->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGSendMailToPlayer->szMailContent), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkCGSendMailToPlayer->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szRecvMailPlayerName
	if(MAX_NAME_SIZE < pkCGSendMailToPlayer->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGSendMailToPlayer->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGSendMailToPlayer->szRecvMailPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSendMailToPlayer);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryMailListByPage(void* pData)
{
	CGQueryMailListByPage* pkCGQueryMailListByPage = (CGQueryMailListByPage*)(pData);

	//EnCode mailType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryMailListByPage->mailType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode page
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryMailListByPage->page), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryMailListByPage(void* pData)
{
	CGQueryMailListByPage* pkCGQueryMailListByPage = (CGQueryMailListByPage*)(pData);

	//DeCode mailType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryMailListByPage->mailType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode page
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryMailListByPage->page), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryMailListByPage);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDeleteMail(void* pData)
{
	CGDeleteMail* pkCGDeleteMail = (CGDeleteMail*)(pData);

	//EnCode mailType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDeleteMail->mailType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mailuid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGDeleteMail->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDeleteMail(void* pData)
{
	CGDeleteMail* pkCGDeleteMail = (CGDeleteMail*)(pData);

	//DeCode mailType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDeleteMail->mailType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mailuid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGDeleteMail->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGDeleteMail);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryMailDetailInfo(void* pData)
{
	CGQueryMailDetailInfo* pkCGQueryMailDetailInfo = (CGQueryMailDetailInfo*)(pData);

	//EnCode mailType
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryMailDetailInfo->mailType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mailuid
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGQueryMailDetailInfo->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryMailDetailInfo(void* pData)
{
	CGQueryMailDetailInfo* pkCGQueryMailDetailInfo = (CGQueryMailDetailInfo*)(pData);

	//DeCode mailType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryMailDetailInfo->mailType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mailuid
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGQueryMailDetailInfo->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQueryMailDetailInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPickMailAttachItem(void* pData)
{
	CGPickMailAttachItem* pkCGPickMailAttachItem = (CGPickMailAttachItem*)(pData);

	//EnCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPickMailAttachItem->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pickIndex
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPickMailAttachItem->pickIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPickMailAttachItem(void* pData)
{
	CGPickMailAttachItem* pkCGPickMailAttachItem = (CGPickMailAttachItem*)(pData);

	//DeCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPickMailAttachItem->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pickIndex
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPickMailAttachItem->pickIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPickMailAttachItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPickMailAttachMoney(void* pData)
{
	CGPickMailAttachMoney* pkCGPickMailAttachMoney = (CGPickMailAttachMoney*)(pData);

	//EnCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPickMailAttachMoney->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPickMailAttachMoney(void* pData)
{
	CGPickMailAttachMoney* pkCGPickMailAttachMoney = (CGPickMailAttachMoney*)(pData);

	//DeCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPickMailAttachMoney->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPickMailAttachMoney);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDeleteAllMail(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDeleteAllMail(void* pData)
{
	(pData);
	return sizeof(CGDeleteAllMail);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGCancelQueue(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGCancelQueue(void* pData)
{
	(pData);
	return sizeof(CGCancelQueue);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGReqPlayerDetailInfo(void* pData)
{
	CGReqPlayerDetailInfo* pkCGReqPlayerDetailInfo = (CGReqPlayerDetailInfo*)(pData);

	//EnCode playerID
	if(EnCode__PlayerID(&(pkCGReqPlayerDetailInfo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGReqPlayerDetailInfo(void* pData)
{
	CGReqPlayerDetailInfo* pkCGReqPlayerDetailInfo = (CGReqPlayerDetailInfo*)(pData);

	//DeCode playerID
	if(DeCode__PlayerID(&(pkCGReqPlayerDetailInfo->playerID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGReqPlayerDetailInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryMailListByPageRst(void* pData)
{
	GCQueryMailListByPageRst* pkGCQueryMailListByPageRst = (GCQueryMailListByPageRst*)(pData);

	//EnCode arrCount
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCQueryMailListByPageRst->arrCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode mailList
	if(MAX_MAIL_CONCISE_COUNT < pkGCQueryMailListByPageRst->arrCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCQueryMailListByPageRst->arrCount; ++i)
	{
		if(EnCode__MailConciseInfo(&(pkGCQueryMailListByPageRst->mailList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode pageIndex
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCQueryMailListByPageRst->pageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pageEnd
	unCount = 1;
	if(!m_kPackage.Pack("CHAR", &(pkGCQueryMailListByPageRst->pageEnd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryMailListByPageRst(void* pData)
{
	GCQueryMailListByPageRst* pkGCQueryMailListByPageRst = (GCQueryMailListByPageRst*)(pData);

	//DeCode arrCount
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCQueryMailListByPageRst->arrCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode mailList
	if(MAX_MAIL_CONCISE_COUNT < pkGCQueryMailListByPageRst->arrCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCQueryMailListByPageRst->arrCount; ++i)
	{
		if(DeCode__MailConciseInfo(&(pkGCQueryMailListByPageRst->mailList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode pageIndex
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCQueryMailListByPageRst->pageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pageEnd
	unCount = 1;
	if(!m_kPackage.UnPack("CHAR", &(pkGCQueryMailListByPageRst->pageEnd), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCQueryMailListByPageRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryMailDetialInfoRst(void* pData)
{
	GCQueryMailDetialInfoRst* pkGCQueryMailDetialInfoRst = (GCQueryMailDetialInfoRst*)(pData);

	//EnCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCQueryMailDetialInfoRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCQueryMailDetialInfoRst->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szSendPlayerName
	if(MAX_NAME_SIZE < pkGCQueryMailDetialInfoRst->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCQueryMailDetialInfoRst->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCQueryMailDetialInfoRst->szSendPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode titleLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCQueryMailDetialInfoRst->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szMailTitle
	if(MAX_MAIL_TITILE_LEN < pkGCQueryMailDetialInfoRst->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCQueryMailDetialInfoRst->titleLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCQueryMailDetialInfoRst->szMailTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode contentLen
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCQueryMailDetialInfoRst->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szMailContent
	if(MAX_MAIL_CONTENT_LEN < pkGCQueryMailDetialInfoRst->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCQueryMailDetialInfoRst->contentLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCQueryMailDetialInfoRst->szMailContent), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryMailDetialInfoRst(void* pData)
{
	GCQueryMailDetialInfoRst* pkGCQueryMailDetialInfoRst = (GCQueryMailDetialInfoRst*)(pData);

	//DeCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCQueryMailDetialInfoRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCQueryMailDetialInfoRst->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szSendPlayerName
	if(MAX_NAME_SIZE < pkGCQueryMailDetialInfoRst->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCQueryMailDetialInfoRst->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCQueryMailDetialInfoRst->szSendPlayerName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode titleLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCQueryMailDetialInfoRst->titleLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szMailTitle
	if(MAX_MAIL_TITILE_LEN < pkGCQueryMailDetialInfoRst->titleLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCQueryMailDetialInfoRst->titleLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCQueryMailDetialInfoRst->szMailTitle), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode contentLen
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCQueryMailDetialInfoRst->contentLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szMailContent
	if(MAX_MAIL_CONTENT_LEN < pkGCQueryMailDetialInfoRst->contentLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCQueryMailDetialInfoRst->contentLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCQueryMailDetialInfoRst->szMailContent), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCQueryMailDetialInfoRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQueryMailAttachInfoRst(void* pData)
{
	GCQueryMailAttachInfoRst* pkGCQueryMailAttachInfoRst = (GCQueryMailAttachInfoRst*)(pData);

	//EnCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCQueryMailAttachInfoRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode money
	unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCQueryMailAttachInfoRst->money), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode itemCount
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCQueryMailAttachInfoRst->itemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode szItemInfo
	if(MAX_MAIL_ATTACHITEM_COUNT < pkGCQueryMailAttachInfoRst->itemCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCQueryMailAttachInfoRst->itemCount; ++i)
	{
		if(EnCode__ItemInfo_i(&(pkGCQueryMailAttachInfoRst->szItemInfo[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQueryMailAttachInfoRst(void* pData)
{
	GCQueryMailAttachInfoRst* pkGCQueryMailAttachInfoRst = (GCQueryMailAttachInfoRst*)(pData);

	//DeCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCQueryMailAttachInfoRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode money
	unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCQueryMailAttachInfoRst->money), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode itemCount
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCQueryMailAttachInfoRst->itemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode szItemInfo
	if(MAX_MAIL_ATTACHITEM_COUNT < pkGCQueryMailAttachInfoRst->itemCount)
	{
		return FAILEDRETCODE;
	}

	for(USHORT i = 0; i < pkGCQueryMailAttachInfoRst->itemCount; ++i)
	{
		if(DeCode__ItemInfo_i(&(pkGCQueryMailAttachInfoRst->szItemInfo[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCQueryMailAttachInfoRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSendMailError(void* pData)
{
	GCSendMailError* pkGCSendMailError = (GCSendMailError*)(pData);

	//EnCode errNo
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCSendMailError->errNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSendMailError(void* pData)
{
	GCSendMailError* pkGCSendMailError = (GCSendMailError*)(pData);

	//DeCode errNo
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCSendMailError->errNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSendMailError);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCDeleteMailRst(void* pData)
{
	GCDeleteMailRst* pkGCDeleteMailRst = (GCDeleteMailRst*)(pData);

	//EnCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCDeleteMailRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isSucc
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCDeleteMailRst->isSucc), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCDeleteMailRst(void* pData)
{
	GCDeleteMailRst* pkGCDeleteMailRst = (GCDeleteMailRst*)(pData);

	//DeCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCDeleteMailRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isSucc
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCDeleteMailRst->isSucc), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCDeleteMailRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPickMailAttachItemRst(void* pData)
{
	GCPickMailAttachItemRst* pkGCPickMailAttachItemRst = (GCPickMailAttachItemRst*)(pData);

	//EnCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPickMailAttachItemRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode pickIndex
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPickMailAttachItemRst->pickIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isSucc
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCPickMailAttachItemRst->isSucc), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPickMailAttachItemRst(void* pData)
{
	GCPickMailAttachItemRst* pkGCPickMailAttachItemRst = (GCPickMailAttachItemRst*)(pData);

	//DeCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPickMailAttachItemRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode pickIndex
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPickMailAttachItemRst->pickIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isSucc
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCPickMailAttachItemRst->isSucc), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPickMailAttachItemRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPickMailAttachMoneyRst(void* pData)
{
	GCPickMailAttachMoneyRst* pkGCPickMailAttachMoneyRst = (GCPickMailAttachMoneyRst*)(pData);

	//EnCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPickMailAttachMoneyRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode isSucc
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCPickMailAttachMoneyRst->isSucc), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPickMailAttachMoneyRst(void* pData)
{
	GCPickMailAttachMoneyRst* pkGCPickMailAttachMoneyRst = (GCPickMailAttachMoneyRst*)(pData);

	//DeCode mailuid
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPickMailAttachMoneyRst->mailuid), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode isSucc
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCPickMailAttachMoneyRst->isSucc), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPickMailAttachMoneyRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPlayerHaveUnReadMail(void* pData)
{
	GCPlayerHaveUnReadMail* pkGCPlayerHaveUnReadMail = (GCPlayerHaveUnReadMail*)(pData);

	//EnCode unReadMailCount
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPlayerHaveUnReadMail->unReadMailCount), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPlayerHaveUnReadMail(void* pData)
{
	GCPlayerHaveUnReadMail* pkGCPlayerHaveUnReadMail = (GCPlayerHaveUnReadMail*)(pData);

	//DeCode unReadMailCount
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPlayerHaveUnReadMail->unReadMailCount), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPlayerHaveUnReadMail);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCReqPlayerDetailInfoRst(void* pData)
{
	GCReqPlayerDetailInfoRst* pkGCReqPlayerDetailInfoRst = (GCReqPlayerDetailInfoRst*)(pData);

	//EnCode uiHp
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiStr
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiStr), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiInt
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiInt), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiAgi
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiAgi), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiPhyAtkMin
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyAtkMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiPhyAtkMax
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyAtkMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMagAtkMin
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagAtkMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMagAtkMax
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagAtkMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiPhyDef
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyDef), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMagDef
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagDef), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiPhyHit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMagHit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiPhyCriLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyCriLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMagCriLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagCriLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiPhyDodge
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyDodge), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode uiMagDodge
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagDodge), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode antiFaint
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->antiFaint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode antiBondage
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->antiBondage), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode antiPhyCri
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->antiPhyCri), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode antiMagCri
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->antiMagCri), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode goodEvilPoint
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->goodEvilPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode evilOnlineTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCReqPlayerDetailInfoRst->evilOnlineTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode equips
	if(EnCode__PlayerInfo_2_Equips(&(pkGCReqPlayerDetailInfoRst->equips)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fashions
	if(EnCode__PlayerInfo_Fashions(&(pkGCReqPlayerDetailInfoRst->fashions)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCReqPlayerDetailInfoRst(void* pData)
{
	GCReqPlayerDetailInfoRst* pkGCReqPlayerDetailInfoRst = (GCReqPlayerDetailInfoRst*)(pData);

	//DeCode uiHp
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiHp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiStr
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiStr), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiInt
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiInt), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiAgi
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiAgi), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiPhyAtkMin
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyAtkMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiPhyAtkMax
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyAtkMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMagAtkMin
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagAtkMin), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMagAtkMax
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagAtkMax), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiPhyDef
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyDef), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMagDef
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagDef), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiPhyHit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMagHit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagHit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiPhyCriLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyCriLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMagCriLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagCriLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiPhyDodge
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiPhyDodge), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode uiMagDodge
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->uiMagDodge), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode antiFaint
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->antiFaint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode antiBondage
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->antiBondage), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode antiPhyCri
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->antiPhyCri), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode antiMagCri
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->antiMagCri), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode goodEvilPoint
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->goodEvilPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode evilOnlineTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCReqPlayerDetailInfoRst->evilOnlineTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode equips
	if(DeCode__PlayerInfo_2_Equips(&(pkGCReqPlayerDetailInfoRst->equips)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fashions
	if(DeCode__PlayerInfo_Fashions(&(pkGCReqPlayerDetailInfoRst->fashions)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCReqPlayerDetailInfoRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRemoveUnionMemberResult(void* pData)
{
	GCRemoveUnionMemberResult* pkGCRemoveUnionMemberResult = (GCRemoveUnionMemberResult*)(pData);

	//EnCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.Pack("INT", &(pkGCRemoveUnionMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberRemoved
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRemoveUnionMemberResult->memberRemoved), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRemoveUnionMemberResult(void* pData)
{
	GCRemoveUnionMemberResult* pkGCRemoveUnionMemberResult = (GCRemoveUnionMemberResult*)(pData);

	//DeCode returnCode
	size_t unCount = 1;
	if(!m_kPackage.UnPack("INT", &(pkGCRemoveUnionMemberResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberRemoved
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRemoveUnionMemberResult->memberRemoved), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRemoveUnionMemberResult);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCRemoveUnionMemberNotify(void* pData)
{
	GCRemoveUnionMemberNotify* pkGCRemoveUnionMemberNotify = (GCRemoveUnionMemberNotify*)(pData);

	//EnCode flag
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCRemoveUnionMemberNotify->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode memberRemoved
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCRemoveUnionMemberNotify->memberRemoved), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCRemoveUnionMemberNotify(void* pData)
{
	GCRemoveUnionMemberNotify* pkGCRemoveUnionMemberNotify = (GCRemoveUnionMemberNotify*)(pData);

	//DeCode flag
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCRemoveUnionMemberNotify->flag), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode memberRemoved
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCRemoveUnionMemberNotify->memberRemoved), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCRemoveUnionMemberNotify);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPunishPlayerInfo(void* pData)
{
	GCPunishPlayerInfo* pkGCPunishPlayerInfo = (GCPunishPlayerInfo*)(pData);

	//EnCode memberSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPunishPlayerInfo->memberSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode punishMember
	if(3 < pkGCPunishPlayerInfo->memberSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCPunishPlayerInfo->memberSize; ++i)
	{
		if(EnCode__PunishPlayerCliInfo(&(pkGCPunishPlayerInfo->punishMember[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPunishPlayerInfo(void* pData)
{
	GCPunishPlayerInfo* pkGCPunishPlayerInfo = (GCPunishPlayerInfo*)(pData);

	//DeCode memberSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPunishPlayerInfo->memberSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode punishMember
	if(3 < pkGCPunishPlayerInfo->memberSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCPunishPlayerInfo->memberSize; ++i)
	{
		if(DeCode__PunishPlayerCliInfo(&(pkGCPunishPlayerInfo->punishMember[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GCPunishPlayerInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryAccountInfo(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryAccountInfo(void* pData)
{
	(pData);
	return sizeof(CGQueryAccountInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCAccountInfo(void* pData)
{
	GCAccountInfo* pkGCAccountInfo = (GCAccountInfo*)(pData);

	//EnCode GameMoney
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAccountInfo->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Score
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAccountInfo->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCAccountInfo->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCAccountInfo->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCAccountInfo->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCAccountInfo->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCAccountInfo->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCAccountInfo(void* pData)
{
	GCAccountInfo* pkGCAccountInfo = (GCAccountInfo*)(pData);

	//DeCode GameMoney
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAccountInfo->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Score
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAccountInfo->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCAccountInfo->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCAccountInfo->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCAccountInfo->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCAccountInfo->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCAccountInfo->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCAccountInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__VipItem(void* pData)
{
	VipItem* pkVipItem = (VipItem*)(pData);

	//EnCode ItemTypeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkVipItem->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkVipItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__VipItem(void* pData)
{
	VipItem* pkVipItem = (VipItem*)(pData);

	//DeCode ItemTypeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkVipItem->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkVipItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(VipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPurchaseItem(void* pData)
{
	CGPurchaseItem* pkCGPurchaseItem = (CGPurchaseItem*)(pData);

	//EnCode PurchaseType
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGPurchaseItem->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CliCalc
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseItem->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPurchaseItem(void* pData)
{
	CGPurchaseItem* pkCGPurchaseItem = (CGPurchaseItem*)(pData);

	//DeCode PurchaseType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGPurchaseItem->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CliCalc
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseItem->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPurchaseItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPurchaseItem(void* pData)
{
	GCPurchaseItem* pkGCPurchaseItem = (GCPurchaseItem*)(pData);

	//EnCode TransID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseItem->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GameMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseItem->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Score
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseItem->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCPurchaseItem->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCPurchaseItem->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseItem->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPurchaseItem->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPurchaseItem->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPurchaseItem(void* pData)
{
	GCPurchaseItem* pkGCPurchaseItem = (GCPurchaseItem*)(pData);

	//DeCode TransID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseItem->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GameMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseItem->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Score
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseItem->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCPurchaseItem->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCPurchaseItem->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseItem->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPurchaseItem->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPurchaseItem->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPurchaseItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQueryVipInfo(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQueryVipInfo(void* pData)
{
	(pData);
	return sizeof(CGQueryVipInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCVipInfo(void* pData)
{
	GCVipInfo* pkGCVipInfo = (GCVipInfo*)(pData);

	//EnCode VipID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCVipInfo->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode LeftDays
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCVipInfo->LeftDays), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Fetched
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCVipInfo->Fetched), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCVipInfo->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCVipInfo(void* pData)
{
	GCVipInfo* pkGCVipInfo = (GCVipInfo*)(pData);

	//DeCode VipID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCVipInfo->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode LeftDays
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCVipInfo->LeftDays), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Fetched
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCVipInfo->Fetched), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCVipInfo->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCVipInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPurchaseVip(void* pData)
{
	CGPurchaseVip* pkCGPurchaseVip = (CGPurchaseVip*)(pData);

	//EnCode VipID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseVip->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CliCalc
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseVip->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPurchaseVip(void* pData)
{
	CGPurchaseVip* pkCGPurchaseVip = (CGPurchaseVip*)(pData);

	//DeCode VipID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseVip->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CliCalc
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseVip->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPurchaseVip);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPurchaseVip(void* pData)
{
	GCPurchaseVip* pkGCPurchaseVip = (GCPurchaseVip*)(pData);

	//EnCode TransID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseVip->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseVip->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GameMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseVip->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Score
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseVip->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCPurchaseVip->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCPurchaseVip->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseVip->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPurchaseVip->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPurchaseVip->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPurchaseVip(void* pData)
{
	GCPurchaseVip* pkGCPurchaseVip = (GCPurchaseVip*)(pData);

	//DeCode TransID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseVip->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseVip->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GameMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseVip->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Score
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseVip->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCPurchaseVip->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCPurchaseVip->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseVip->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPurchaseVip->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPurchaseVip->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPurchaseVip);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGFetchVipItem(void* pData)
{
	CGFetchVipItem* pkCGFetchVipItem = (CGFetchVipItem*)(pData);

	//EnCode VipID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGFetchVipItem->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGFetchVipItem(void* pData)
{
	CGFetchVipItem* pkCGFetchVipItem = (CGFetchVipItem*)(pData);

	//DeCode VipID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGFetchVipItem->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGFetchVipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCFetchVipItem(void* pData)
{
	GCFetchVipItem* pkGCFetchVipItem = (GCFetchVipItem*)(pData);

	//EnCode VipID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCFetchVipItem->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipItemLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCFetchVipItem->VipItemLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipItemArr
	if(ITEMS_PER_PAGE < pkGCFetchVipItem->VipItemLen)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkGCFetchVipItem->VipItemLen; ++i)
	{
		if(EnCode__VipItem(&(pkGCFetchVipItem->VipItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCFetchVipItem->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCFetchVipItem(void* pData)
{
	GCFetchVipItem* pkGCFetchVipItem = (GCFetchVipItem*)(pData);

	//DeCode VipID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCFetchVipItem->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipItemLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCFetchVipItem->VipItemLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipItemArr
	if(ITEMS_PER_PAGE < pkGCFetchVipItem->VipItemLen)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkGCFetchVipItem->VipItemLen; ++i)
	{
		if(DeCode__VipItem(&(pkGCFetchVipItem->VipItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCFetchVipItem->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCFetchVipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPurchaseGift(void* pData)
{
	CGPurchaseGift* pkCGPurchaseGift = (CGPurchaseGift*)(pData);

	//EnCode PurchaseType
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGPurchaseGift->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseGift->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseGift->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGPurchaseGift->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccount
	if(MAX_NAME_SIZE < pkCGPurchaseGift->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGPurchaseGift->DestAccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGPurchaseGift->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseGift->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CliCalc
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPurchaseGift->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NoteLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGPurchaseGift->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Note
	if(MAX_NOTE_SIZE < pkCGPurchaseGift->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGPurchaseGift->NoteLen;
	if(!m_kPackage.Pack("CHAR", &(pkCGPurchaseGift->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPurchaseGift(void* pData)
{
	CGPurchaseGift* pkCGPurchaseGift = (CGPurchaseGift*)(pData);

	//DeCode PurchaseType
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGPurchaseGift->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseGift->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseGift->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGPurchaseGift->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccount
	if(MAX_NAME_SIZE < pkCGPurchaseGift->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGPurchaseGift->DestAccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGPurchaseGift->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseGift->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CliCalc
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPurchaseGift->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NoteLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGPurchaseGift->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Note
	if(MAX_NOTE_SIZE < pkCGPurchaseGift->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkCGPurchaseGift->NoteLen;
	if(!m_kPackage.UnPack("CHAR", &(pkCGPurchaseGift->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPurchaseGift);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPetMove(void* pData)
{
	CGPetMove* pkCGPetMove = (CGPetMove*)(pData);

	//EnCode petOwner
	if(EnCode__PlayerID(&(pkCGPetMove->petOwner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fOrgWorldPosX
	size_t unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPetMove->fOrgWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fOrgWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPetMove->fOrgWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fNewWorldPosX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPetMove->fNewWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fNewWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPetMove->fNewWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fPetSpeed
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkCGPetMove->fPetSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPetMove(void* pData)
{
	CGPetMove* pkCGPetMove = (CGPetMove*)(pData);

	//DeCode petOwner
	if(DeCode__PlayerID(&(pkCGPetMove->petOwner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fOrgWorldPosX
	size_t unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPetMove->fOrgWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fOrgWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPetMove->fOrgWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fNewWorldPosX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPetMove->fNewWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fNewWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPetMove->fNewWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fPetSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkCGPetMove->fPetSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPetMove);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPurchaseGift(void* pData)
{
	GCPurchaseGift* pkGCPurchaseGift = (GCPurchaseGift*)(pData);

	//EnCode TransID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseGift->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseGift->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseGift->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCPurchaseGift->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccount
	if(MAX_NAME_SIZE < pkGCPurchaseGift->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPurchaseGift->DestAccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCPurchaseGift->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseGift->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GameMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseGift->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Score
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseGift->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCPurchaseGift->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCPurchaseGift->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCPurchaseGift->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPurchaseGift->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCPurchaseGift->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NoteLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCPurchaseGift->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Note
	if(MAX_NOTE_SIZE < pkGCPurchaseGift->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPurchaseGift->NoteLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCPurchaseGift->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPurchaseGift(void* pData)
{
	GCPurchaseGift* pkGCPurchaseGift = (GCPurchaseGift*)(pData);

	//DeCode TransID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseGift->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseGift->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseGift->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCPurchaseGift->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccount
	if(MAX_NAME_SIZE < pkGCPurchaseGift->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPurchaseGift->DestAccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCPurchaseGift->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseGift->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GameMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseGift->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Score
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseGift->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCPurchaseGift->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCPurchaseGift->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCPurchaseGift->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPurchaseGift->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCPurchaseGift->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NoteLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCPurchaseGift->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Note
	if(MAX_NOTE_SIZE < pkGCPurchaseGift->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCPurchaseGift->NoteLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCPurchaseGift->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPurchaseGift);
}

size_t	MUX_PROTO::CliProtocol::EnCode__TransItem(void* pData)
{
	TransItem* pkTransItem = (TransItem*)(pData);

	//EnCode TransID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransItem->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OpeDate
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransItem->OpeDate), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__TransItem(void* pData)
{
	TransItem* pkTransItem = (TransItem*)(pData);

	//DeCode TransID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransItem->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OpeDate
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransItem->OpeDate), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(TransItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCPetMove(void* pData)
{
	GCPetMove* pkGCPetMove = (GCPetMove*)(pData);

	//EnCode petOwner
	if(EnCode__PlayerID(&(pkGCPetMove->petOwner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode fOrgWorldPosX
	size_t unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPetMove->fOrgWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fOrgWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPetMove->fOrgWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fSpeed
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPetMove->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fNewWorldPosX
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPetMove->fNewWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode fNewWorldPosY
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkGCPetMove->fNewWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCPetMove(void* pData)
{
	GCPetMove* pkGCPetMove = (GCPetMove*)(pData);

	//DeCode petOwner
	if(DeCode__PlayerID(&(pkGCPetMove->petOwner)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode fOrgWorldPosX
	size_t unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPetMove->fOrgWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fOrgWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPetMove->fOrgWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fSpeed
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPetMove->fSpeed), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fNewWorldPosX
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPetMove->fNewWorldPosX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode fNewWorldPosY
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkGCPetMove->fNewWorldPosY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCPetMove);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGFetchNonVipItem(void* pData)
{
	CGFetchNonVipItem* pkCGFetchNonVipItem = (CGFetchNonVipItem*)(pData);

	//EnCode itemId
	if(EnCode__TransItem(&(pkCGFetchNonVipItem->itemId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGFetchNonVipItem(void* pData)
{
	CGFetchNonVipItem* pkCGFetchNonVipItem = (CGFetchNonVipItem*)(pData);

	//DeCode itemId
	if(DeCode__TransItem(&(pkCGFetchNonVipItem->itemId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGFetchNonVipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCFetchNonVipItem(void* pData)
{
	GCFetchNonVipItem* pkGCFetchNonVipItem = (GCFetchNonVipItem*)(pData);

	//EnCode itemId
	if(EnCode__TransItem(&(pkGCFetchNonVipItem->itemId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode Result
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCFetchNonVipItem->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCFetchNonVipItem(void* pData)
{
	GCFetchNonVipItem* pkGCFetchNonVipItem = (GCFetchNonVipItem*)(pData);

	//DeCode itemId
	if(DeCode__TransItem(&(pkGCFetchNonVipItem->itemId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode Result
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCFetchNonVipItem->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCFetchNonVipItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGQuerySuitInfo(void* pData)
{
	CGQuerySuitInfo* pkCGQuerySuitInfo = (CGQuerySuitInfo*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGQuerySuitInfo->index), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGQuerySuitInfo(void* pData)
{
	CGQuerySuitInfo* pkCGQuerySuitInfo = (CGQuerySuitInfo*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGQuerySuitInfo->index), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGQuerySuitInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCQuerySuitInfoRst(void* pData)
{
	GCQuerySuitInfoRst* pkGCQuerySuitInfoRst = (GCQuerySuitInfoRst*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCQuerySuitInfoRst->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCQuerySuitInfoRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode suit
	if(EnCode__SuitInfo_i(&(pkGCQuerySuitInfoRst->suit)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCQuerySuitInfoRst(void* pData)
{
	GCQuerySuitInfoRst* pkGCQuerySuitInfoRst = (GCQuerySuitInfoRst*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCQuerySuitInfoRst->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCQuerySuitInfoRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode suit
	if(DeCode__SuitInfo_i(&(pkGCQuerySuitInfoRst->suit)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCQuerySuitInfoRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGClearSuitInfo(void* pData)
{
	CGClearSuitInfo* pkCGClearSuitInfo = (CGClearSuitInfo*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGClearSuitInfo->index), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGClearSuitInfo(void* pData)
{
	CGClearSuitInfo* pkCGClearSuitInfo = (CGClearSuitInfo*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGClearSuitInfo->index), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGClearSuitInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCClearSuitInfoRst(void* pData)
{
	GCClearSuitInfoRst* pkGCClearSuitInfoRst = (GCClearSuitInfoRst*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCClearSuitInfoRst->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCClearSuitInfoRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCClearSuitInfoRst(void* pData)
{
	GCClearSuitInfoRst* pkGCClearSuitInfoRst = (GCClearSuitInfoRst*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCClearSuitInfoRst->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCClearSuitInfoRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCClearSuitInfoRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGSaveSuitInfo(void* pData)
{
	CGSaveSuitInfo* pkCGSaveSuitInfo = (CGSaveSuitInfo*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGSaveSuitInfo->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode suit
	if(EnCode__SuitInfo_i(&(pkCGSaveSuitInfo->suit)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGSaveSuitInfo(void* pData)
{
	CGSaveSuitInfo* pkCGSaveSuitInfo = (CGSaveSuitInfo*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGSaveSuitInfo->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode suit
	if(DeCode__SuitInfo_i(&(pkCGSaveSuitInfo->suit)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGSaveSuitInfo);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCSaveSuitInfoRst(void* pData)
{
	GCSaveSuitInfoRst* pkGCSaveSuitInfoRst = (GCSaveSuitInfoRst*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCSaveSuitInfoRst->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCSaveSuitInfoRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCSaveSuitInfoRst(void* pData)
{
	GCSaveSuitInfoRst* pkGCSaveSuitInfoRst = (GCSaveSuitInfoRst*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCSaveSuitInfoRst->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCSaveSuitInfoRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCSaveSuitInfoRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGChanageSuit(void* pData)
{
	CGChanageSuit* pkCGChanageSuit = (CGChanageSuit*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkCGChanageSuit->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode repairConfirm
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkCGChanageSuit->repairConfirm), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGChanageSuit(void* pData)
{
	CGChanageSuit* pkCGChanageSuit = (CGChanageSuit*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkCGChanageSuit->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode repairConfirm
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkCGChanageSuit->repairConfirm), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGChanageSuit);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChanageSuitRst(void* pData)
{
	GCChanageSuitRst* pkGCChanageSuitRst = (GCChanageSuitRst*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGCChanageSuitRst->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode repairConfirm
	unCount = 1;
	if(!m_kPackage.Pack("bool", &(pkGCChanageSuitRst->repairConfirm), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGCChanageSuitRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChanageSuitRst(void* pData)
{
	GCChanageSuitRst* pkGCChanageSuitRst = (GCChanageSuitRst*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGCChanageSuitRst->index), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode repairConfirm
	unCount = 1;
	if(!m_kPackage.UnPack("bool", &(pkGCChanageSuitRst->repairConfirm), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGCChanageSuitRst->result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChanageSuitRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGEndAnamorphic(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGEndAnamorphic(void* pData)
{
	(pData);
	return sizeof(CGEndAnamorphic);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCCollectTaskRst(void* pData)
{
	GCCollectTaskRst* pkGCCollectTaskRst = (GCCollectTaskRst*)(pData);

	//EnCode itemSize
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCCollectTaskRst->itemSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemArr
	if(MAXPACKAGEITEM < pkGCCollectTaskRst->itemSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCCollectTaskRst->itemSize; ++i)
	{
		if(EnCode__ItemInfo_i(&(pkGCCollectTaskRst->ItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCCollectTaskRst->money), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCCollectTaskRst(void* pData)
{
	GCCollectTaskRst* pkGCCollectTaskRst = (GCCollectTaskRst*)(pData);

	//DeCode itemSize
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCCollectTaskRst->itemSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemArr
	if(MAXPACKAGEITEM < pkGCCollectTaskRst->itemSize)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGCCollectTaskRst->itemSize; ++i)
	{
		if(DeCode__ItemInfo_i(&(pkGCCollectTaskRst->ItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCCollectTaskRst->money), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCCollectTaskRst);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPickCollectItem(void* pData)
{
	CGPickCollectItem* pkCGPickCollectItem = (CGPickCollectItem*)(pData);

	//EnCode index
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkCGPickCollectItem->index), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPickCollectItem(void* pData)
{
	CGPickCollectItem* pkCGPickCollectItem = (CGPickCollectItem*)(pData);

	//DeCode index
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkCGPickCollectItem->index), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(CGPickCollectItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGPickAllCollectItem(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGPickAllCollectItem(void* pData)
{
	(pData);
	return sizeof(CGPickAllCollectItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGDiscardCollectItem(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGDiscardCollectItem(void* pData)
{
	(pData);
	return sizeof(CGDiscardCollectItem);
}

size_t	MUX_PROTO::CliProtocol::EnCode__CGEquipFashionSwitch(void* pData)
{
	(pData);
	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__CGEquipFashionSwitch(void* pData)
{
	(pData);
	return sizeof(CGEquipFashionSwitch);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCGameStateNotify(void* pData)
{
	GCGameStateNotify* pkGCGameStateNotify = (GCGameStateNotify*)(pData);

	//EnCode state
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCGameStateNotify->state), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode accOnlinePeriod
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCGameStateNotify->accOnlinePeriod), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode accOfflinePeriod
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCGameStateNotify->accOfflinePeriod), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCGameStateNotify(void* pData)
{
	GCGameStateNotify* pkGCGameStateNotify = (GCGameStateNotify*)(pData);

	//DeCode state
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCGameStateNotify->state), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode accOnlinePeriod
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCGameStateNotify->accOnlinePeriod), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode accOfflinePeriod
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCGameStateNotify->accOfflinePeriod), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCGameStateNotify);
}

size_t	MUX_PROTO::CliProtocol::EnCode__GCChangeTargetName(void* pData)
{
	GCChangeTargetName* pkGCChangeTargetName = (GCChangeTargetName*)(pData);

	//EnCode targetID
	if(EnCode__PlayerID(&(pkGCChangeTargetName->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGCChangeTargetName->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode changeName
	if(MAX_NAME_SIZE < pkGCChangeTargetName->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChangeTargetName->nameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGCChangeTargetName->changeName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	MUX_PROTO::CliProtocol::DeCode__GCChangeTargetName(void* pData)
{
	GCChangeTargetName* pkGCChangeTargetName = (GCChangeTargetName*)(pData);

	//DeCode targetID
	if(DeCode__PlayerID(&(pkGCChangeTargetName->targetID)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode nameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGCChangeTargetName->nameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode changeName
	if(MAX_NAME_SIZE < pkGCChangeTargetName->nameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGCChangeTargetName->nameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGCChangeTargetName->changeName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GCChangeTargetName);
}

#ifndef WIN32
//#pragma GCC diagnostic warning "-Wno-char-subscripts"
#endif  //WIN32


