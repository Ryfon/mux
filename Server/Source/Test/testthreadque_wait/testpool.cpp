﻿
#include "testpool.h"

#ifndef WIN32
    #include <unistd.h>
#else //WIN32
    #include <stdio.h>
#endif //WIN32

#include <stdlib.h>

#include "threadrwque.h"
#include "buflog.h"

#ifndef WIN32
#include <signal.h>
#include "lsocket.h"
//static int g_epfd;//epoll句柄；
//static struct epoll_event* g_arrPollEvents = NULL;
//static int g_maxfd = 1024;
#endif //WIN32

#include "apppoolobjs.h"

#include "manframe/manframe.h"

#include "sigexception/sigexception.h"

#include "testplayer.h"

#ifdef TEST_CODE

bool g_IsSrv = true;//服务端或客户端(用于GateSrv时，GateSrv不会显式置此值，因此初始化为true)；

struct ThreadParam
{
	T_TestPool* pTestPool;

	int    loopnum;
};

///测试线程；
void* TestThread( void* pInParam )
{
    ThreadInitTls();

	ThreadParam* pThreadParam = (ThreadParam*) pInParam;
	T_TestPool* pTestPool = pThreadParam->pTestPool;

	int loopnum = pThreadParam->loopnum;
#ifndef WIN32
	unsigned long selfid = pthread_self();
#else //WIN32
	unsigned long selfid = ::GetCurrentThreadId();
#endif //WIN32
	TestPoolObj* pCurID = NULL;

	for ( int i=0; i<loopnum; ++i )
	{
		int counter = 0;
		while (counter<loopnum)
		{
			++counter;
			pCurID = pTestPool->DsRetrieveOrCreate( TestPoolObjNO );
			if ( NULL != pCurID )
			{
				//printf( "%x,%x处理成功\n", selfid, pCurID->testi );
				//NewLog( LOG_LEV_INFO, "%x,%x处理成功", selfid, pCurID->testi );
				pTestPool->DsRelease( TestPoolObjNO, pCurID );
			} else {
				//printf( "取到空元素!\n" );
				NewLog( LOG_LEV_INFO, "取到空元素!" );
			}
		}
		//printf( "%x,第%d次%d循环\n", selfid, i, loopnum );
		NewLog( LOG_LEV_INFO, "%x,第%d次%d循环", selfid, i, loopnum );
	}	

	ThreadEndTls();

	return NULL;
}

/////测试线程；
//void* EPollWaitThread( void* )
//{
//    ThreadInitTls();
//
//#ifndef WIN32
//	CLSocket* pLSocket = NULL;
//	int waitrst = -1;
//	while ( -1 == waitrst ) 
//	{
//		NewLog( LOG_LEV_INFO, "epoll wait start" );
//		waitrst = epoll_wait( g_epfd, g_arrPollEvents, g_maxfd, 1000 );
//		int tmperr = errno;//即刻取错误号；
//		NewLog( LOG_LEV_INFO, "epoll wait end" );
//		if ( -1 != waitrst )
//		{
//			for ( int i=0; i<waitrst; ++i )
//			{
//				if ( EPOLLERR & g_arrPollEvents[i].events ) 
//				{					
//					NewLog( LOG_LEV_INFO, "EPOLLERR" );
//					pLSocket = (CLSocket*) (g_arrPollEvents[i].data.ptr);
//					pLSocket->OnEPollError( tmperr );//由于-1!=waitrst，因此tmperr中不是真正的socket错误号，虽然多半是对端关闭，但如果一定要知道真正的socket错误号，可以在OnEPollError中再对此socket作一次操作。
//					continue;
//				} 
//
//				if ( EPOLLOUT & g_arrPollEvents[i].events )
//				{
//					NewLog( LOG_LEV_INFO, "EPOLLOUT" );
//					pLSocket = (CLSocket*) (g_arrPollEvents[i].data.ptr);
//					pLSocket->OnEPollOut();
//				} 
//				if ( EPOLLIN & g_arrPollEvents[i].events ) {
//					NewLog( LOG_LEV_INFO, "EPOLLIN" );
//					pLSocket = (CLSocket*) (g_arrPollEvents[i].data.ptr);
//					pLSocket->OnEPollIn();
//				} 
//				if ( EPOLLONESHOT & g_arrPollEvents[i].events ) {
//					NewLog( LOG_LEV_INFO, "EPOLLONESHOT" );
//				} 
//				if ( EPOLLPRI & g_arrPollEvents[i].events ) {
//					NewLog( LOG_LEV_INFO, "EPOLPRI" );
//				} 
//				if ( EPOLLHUP & g_arrPollEvents[i].events )
//				{
//					NewLog( LOG_LEV_INFO, "EPOLLHUP" );
//					pLSocket = (CLSocket*) (g_arrPollEvents[i].data.ptr);
//					pLSocket->OnEPollHup();
//				}
//				{
//					NewLog( LOG_LEV_INFO, "epoll events:%d", g_arrPollEvents[i].events );
//				} 
//			}
//			if ( -1 == waitrst )
//			{				
//				break;
//			}
//			//NewLog( LOG_LEV_INFO, "等待1秒\n" );
//			waitrst = -1;
//		} else {
//			NewLog( LOG_LEV_INFO, "epoll wait err : %d\n", tmperr );
//			if ( EBADF == tmperr )
//			{
//				NewLog( LOG_LEV_INFO, "EBADF\n" );
//			} else if ( EFAULT == tmperr ) {
//				NewLog( LOG_LEV_INFO,  "EFAULT\n" );
//			} else if ( EINTR == tmperr ) {
//				NewLog( LOG_LEV_INFO, "EINTR\n" );
//			} else if ( EINVAL == tmperr ) {
//				NewLog( LOG_LEV_INFO, "EINVAL\n" );
//			} else {
//				NewLog( LOG_LEV_INFO, "unknown err\n" );
//			}
//		}
//	}
//#endif //WIN32
//
//	ThreadEndTls();
//	return NULL;
//}

class CTestFrameEle : public IFrameEle
{
public:
	CTestFrameEle() {};
	virtual ~CTestFrameEle() 
	{
		NewLog( LOG_LEV_INFO, "CTestFrameEle，%d析构", GetSelfFrameID() );
		return;
	};

public:
	virtual bool OnActiveBeExplored()
	{
		NewLog( LOG_LEV_INFO, "CTestFrameEle被遍历，其在框架中的ID%d", GetSelfFrameID() );
		return true;
	};//被CManFrame调用，一般是之前将自身设为活动的结果;

	///被CManFrame调用，一般为定时任务；
	virtual bool OnValidBeExplored()
	{
		NewLog( LOG_LEV_INFO, "OnValidBeExplored被遍历，其在框架中的ID%d", GetSelfFrameID() );
		return true;
	};//被CManFrame调用，一般为定时任务；

};

class A
{
public:
    A()
    {
		static unsigned int ctimes = 0;
		++ctimes;
		cout << "pre construct A instance, seq : " << ctimes << endl;
        //*(int*)0 = 0;
		test();
		cout << "after construct A instance, seq : "<< ctimes << endl;
    }

	inline void test()
	{
		*(int*)0 = 0;
	}
};

///服务端主循环；
int srv()
{
	NewLog( LOG_LEV_ERROR, "测试服务器..." );

	DsEvent manframeEvent;

	CManFrame* pTestManFrame = NEW CManFrame( manframeEvent, 32000 );

#ifndef WIN32
	CDsEPoll* pEPoll = NEW CDsEPoll[3];

	CLSocket* listenSock = NEW CLSocket();
	if ( !(listenSock->AddSelfToFrame(pTestManFrame) 
		   ) 
		)
	{
		delete listenSock; listenSock = NULL;
		return 0;
	}			
	listenSock->SetEPollArr( pEPoll, 3 );
	listenSock->LSListen( "172.18.7.109", 12000 );

	while ( true )
	{
		DS_TRY_BEGIN {
			NewLog( LOG_LEV_DEBUG, "srv, pre crash ................" );
			usleep( 300 * 1000 );
			A tmpa;
			pTestManFrame->ManFrameLoop();
		} DS_CATCH_SIG {
			NewLog( LOG_LEV_DEBUG, "DS_CATCH_SIG" ); 
		} DS_CATCH_NORMAL {
			NewLog( LOG_LEV_DEBUG, "DS_CATCH_NORMAL" ); 
		} DS_END_TRY;
	}
#endif //WIN32

#ifndef WIN32
	delete listenSock; listenSock = NULL;
	if ( NULL != pEPoll )
	{
		delete [] pEPoll; pEPoll = NULL;
	}
#endif //WIN32

	delete pTestManFrame; pTestManFrame = NULL;

	return 0;
}

///客户端主循环；
int cli( unsigned int cliNum )
{
	NewLog( LOG_LEV_ERROR, "测试客户端：测试%d个客户端...", cliNum );

	DsEvent manframeEvent;

	CManFrame* pTestManFrame = NEW CManFrame( manframeEvent, 32000 );

#ifndef WIN32

	CDsEPoll* pEPoll = NEW CDsEPoll[3];

	NewLog( LOG_LEV_INFO, "等2秒.." );
	sleep( 1 );
	NewLog( LOG_LEV_INFO, "2秒等待结束" );

	unsigned int curconned = 0;

	while ( true ) //首先执行一系列连接；
	{
		if ( curconned<cliNum )
		{
			++curconned;
			NewLog( LOG_LEV_DEBUG, "当前发起连接数%d", curconned );
			R_PLAYER* newSock = R_PLAYER::CreateSelfIns();
			if ( NULL == newSock )
			{
				NewLog( LOG_LEV_DEBUG, "发起测试连接，分配R_PLAYER失败" );
				continue;
			}
			if ( !(newSock->AddSelfToFrame(pTestManFrame)
				   ) 
				)
			{
				//delete newSock; 
				newSock->DestorySelfIns();
				newSock = NULL;
				continue;
			}
			newSock->SetEPollArr( pEPoll, 3 );
			NewLog( LOG_LEV_DEBUG, "main,socket%d,位置%d", newSock->GetInnerSocket(), newSock->GetPosInfo() );				
			newSock->LSConnect( "172.18.7.109", 12000 );			
			pTestManFrame->ManFrameLoop();				
		}	else {
			break;
		}	
	}	

	static unsigned int lastConnTime = GetTickCount();//测量遍历用时

	while ( true )
	{
		if ( pTestManFrame->GetInnerValidEleNum() < (int)(cliNum) )
		{
			unsigned int passedconn = GetTickCount() - lastConnTime;
			if ( passedconn > 3 )
			{
				lastConnTime = GetTickCount();
				R_PLAYER* newSock = R_PLAYER::CreateSelfIns();
				if ( NULL == newSock )
				{
					NewLog( LOG_LEV_DEBUG, "补充连接，分配R_PLAYER失败" );
				} else {
					if ( !(newSock->AddSelfToFrame(pTestManFrame)
						) 
						)
					{
						//delete newSock; 
						newSock->DestorySelfIns();
						newSock = NULL;
						continue;
					}				
					newSock->SetEPollArr( pEPoll, 3 );
					NewLog( LOG_LEV_INFO, "补充连接,socket%d,位置%d", newSock->GetInnerSocket(), newSock->GetPosInfo() );				
					newSock->LSConnect( "172.18.7.109", 12000 );									
				}
			}
		}
		pTestManFrame->ManFrameLoop();
	}	

#endif //WIN32

#ifndef WIN32
	if ( NULL != pEPoll )
	{
		delete [] pEPoll; pEPoll = NULL;
	}
#endif //WIN32

	delete pTestManFrame; pTestManFrame = NULL;

	return 0;
}

int main(int argc, char *argv[])
{
#ifdef WIN32
    #ifdef _DEBUG
        _CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
    #endif //_DEBUG
#endif //WIN32
		;

#ifndef WIN32
	struct sigaction sigpipecat;
	memset( &sigpipecat, 0, sizeof(sigpipecat) );
	sigpipecat.sa_handler = SIG_IGN;
	sigaction( SIGPIPE, &sigpipecat, NULL );
#endif //WIN32

	MainInitTls();

    InitGloblePools();

	/*
	测试asm位扫描指令;
	*/
	unsigned int* testarr = new unsigned int[1024];
	for ( int i=0; i<1024; ++i ) //赋值；
	{
		testarr[i] = (unsigned int)( 0x01 ) << (rand()%31);
	}

	for ( unsigned int j=0; j<1024; ++j ) //验证；
	{
		if ( CManFrame::GetLogValue( testarr[j] ) != CManFrame::GetLogValAsm( testarr[j] ) )
		{
			NewLog( LOG_LEV_ERROR, "ASM计算错误, (%d-->%d(true:%d))\n", testarr[j], CManFrame::GetLogValAsm(testarr[j]), CManFrame::GetLogValue(testarr[j]) );
		}
	}

	unsigned int sttime = GetTickCount();
	NewLog( LOG_LEV_DEBUG, "普通C计算开始...\n" );
	for ( unsigned int x=0; x<50000; ++x )
	{
		for ( unsigned int j=0; j<1024; ++j )
		{
			CManFrame::GetLogValue( testarr[j] );
		}
	}
	NewLog( LOG_LEV_DEBUG, "...普通C计算结束\n" );
	NewLog( LOG_LEV_DEBUG, "普通C计算费时:%d\n", GetTickCount()-sttime );

	sttime = GetTickCount();
	NewLog( LOG_LEV_DEBUG, "ASM计算开始...\n" );
	for ( unsigned int x=0; x<50000; ++x )
	{
		for ( unsigned int j=0; j<1024; ++j )
		{
			CManFrame::GetLogValAsm( testarr[j] );
		}
	}
	NewLog( LOG_LEV_DEBUG, "...ASM计算结束\n" );
	NewLog( LOG_LEV_DEBUG, "ASM计算费时:%d\n", GetTickCount()-sttime );

	NewLog( LOG_LEV_DEBUG, "ASM测试完成\n" );
	return 0;

	unsigned int loopnum = 0;
	if ( 2 != argc )
	{
		NewLog( LOG_LEV_ERROR, "输入参数错误, 可用参数:s/c*" );
		return -1;
	} else {
		if ( strlen(argv[0]) <= 0 )
		{
			NewLog( LOG_LEV_ERROR, "输入参数错误, 可用参数:s/c*" );
		}
		if ( 's' == argv[1][0] )
		{
			///服务器
			g_IsSrv = true;
			srv();
		} else if ( 'c' == argv[1][0] ) {
			g_IsSrv = false;
			loopnum = ( unsigned int ) atoi( (const char*) &(argv[1][1]) );
			if ( loopnum >= 20480 )
			{
				NewLog( LOG_LEV_ERROR, "输入参数错误, 测试客户端数必须小于20480" );
				return -1;
			}
			cli( loopnum );
		} else {
			NewLog( LOG_LEV_ERROR, "输入参数错误, 可用参数:s/c*\n" );
			return -1;
		}
	}

    ReleaseGloblePools();

    MainEndTls();	
	return 0;
};

#endif //TEST_CODE
