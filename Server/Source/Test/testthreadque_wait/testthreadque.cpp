﻿
#include "testthreadque.h"

#ifndef WIN32
    #include <unistd.h>
#else //WIN32
    #include <stdio.h>
#endif //WIN32

#include <stdlib.h>

#include "threadrwque.h"

#include "buflog.h"

//extern time_t  g_curTimeT;//定时由主逻辑线程更新,而不是由各记日志线程反复进行系统调用
//extern tm      g_curTm;//定时由主逻辑线程更新,而不是由各记日志线程反复进行系统调用
//
//typedef CThreadRWQue<int, 1000, 10> T_RWQUE;
//
//struct ThreadParam
//{
//	T_RWQUE* pTestRWQue;
//	int    loopnum;
//};
//
/////读线程；
//void* ReadThread( void* pInParam )
//{
//	CDsBufLog* pCurLog = NEW CDsBufLog;
//
//	ThreadParam* pThreadParam = (ThreadParam*) pInParam;
//	T_RWQUE* pWriteQueue = pThreadParam->pTestRWQue;
//
//	int loopnum = pThreadParam->loopnum;
//#ifndef WIN32
//	unsigned long selfid = pthread_self();
//#else //WIN32
//	unsigned long selfid = ::GetCurrentThreadId();
//#endif //WIN32
//	int* pCurID = NULL;
//
//	for ( int i=0; i<loopnum; ++i )
//	{
//		bool iscontinue = true;
//		while (iscontinue)
//		{
//			pCurID = pWriteQueue->PopEle();//NullWaitTime();
//			if ( NULL == pCurID )
//			{
//				//printf( "线程%x:读队列时队列空，稍等再读\n", selfid );
//			} else {
//				//printf( "线程%x:元素%d从队列中弹出\n", selfid, *pCurID );
//				//DsLog( LOG_LEV_INFO, pCurLog, "线程%x:元素%d从队列中弹出", selfid, *pCurID ); 
//				if ( *pCurID == loopnum )
//				{
//					//printf( "%x:第%d次，读取到最后一个元素%d\n", selfid, i, loopnum );
//					DsLog( LOG_LEV_INFO, pCurLog, "%x:第%d次，读取到最后一个元素%d", selfid, i, loopnum ); 
//					iscontinue = false;
//				}
//				delete pCurID; pCurID = NULL;
//			}
//		}
//	}
//
//	delete pCurLog; pCurLog = NULL;
//	return NULL;
//}
//
/////写线程；
//void* WriteThread( void* pInParam )
//{
//	CDsBufLog* pCurLog = NEW CDsBufLog;
//
//	ThreadParam* pThreadParam = (ThreadParam*) pInParam;
//	T_RWQUE* pWriteQueue = pThreadParam->pTestRWQue;
//
//	int loopnum = pThreadParam->loopnum;
//#ifndef WIN32
//	unsigned long selfid = pthread_self();
//#else //WIN32
//	unsigned long selfid = ::GetCurrentThreadId();
//#endif //WIN32
//
//	int curID = 0;
//
//	bool isok = false;
//	int* pCurID = NULL;
//	for ( int i=0; i<loopnum; ++i )
//	{
//		curID = 0;
//		while ( curID < loopnum )
//		{
//			++curID;
//
//			pCurID = NEW int(curID);
//			isok = pWriteQueue->PushEle(pCurID);//NotifyRead( pCurID );
//			if ( !isok )
//			{
//				delete pCurID; pCurID = NULL;
//				//printf( "队列已写满，稍等再写\n" );
//				--curID;
//			} else {
//				//printf( "线程%x:元素%d压入队列\n", selfid, curID );
//				//DsLog( LOG_LEV_INFO, pCurLog, "线程%x:元素%d压入队列", selfid, curID ); 
//				if ( curID == loopnum )
//				{
//					//printf( "%x:第%d次，已压入最后一个元素%d\n", selfid, i, curID );
//					DsLog( LOG_LEV_INFO, pCurLog, "%x:第%d次，已压入最后一个元素%d\n", selfid, i, curID ); 
//				}
//			}
//		}
//	}
//
//	delete pCurLog; pCurLog = NULL;
//	return NULL;
//}
//
//int main(int argc, char *argv[])
//{
//#ifdef WIN32
//	#ifdef _DEBUG
//		_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
//	#endif //_DEBUG
//#endif //WIN32
//	int loopnum = 0;
//	if ( 2 != argc )
//	{
//		printf( "默认测试10000个数字\n" );
//		loopnum = 10000;
//	} else {		
//		loopnum = atoi( argv[1] );
//		printf( "测试%d个数字\n", loopnum );
//	}
//
//#ifdef WIN32
//	unsigned long sttick = ::GetTickCount();
//#endif //WIN32
//
//	//int curID = 0;
//	//int* pInt = NEW int(curID);	
//	//for ( unsigned int i=0; i<loopnum; ++i )
//	//{
//	//	++ (*pInt);		
//	//	++ curID;
//	//	delete pInt; pInt = NULL;
//	//	pInt = NEW int(curID);
//	//}
//	//delete pInt; pInt = NULL;
//	//return 0;
//
//	ThreadParam* pThreadParam = NEW ThreadParam;
//	pThreadParam->loopnum = loopnum;
//	pThreadParam->pTestRWQue = NEW T_RWQUE;
//
//	int err = 0;
//
//#ifndef WIN32
//	pthread_t tid1 = 0, tid2= 0;
//	err = pthread_create( &tid1, NULL, ReadThread, pThreadParam );
//	if ( 0 != err )
//	{
//		printf( "创建读线程失败\n" );
//		return -1;
//	}
//	err = pthread_create( &tid2, NULL, WriteThread, pThreadParam );
//	if ( 0 != err )
//	{
//		printf( "创建写线程失败\n" );
//		return -1;
//	}
//
//	pthread_join( tid2, NULL );
//	printf( "写线程已退出\n" );
//
//	pthread_join( tid1, NULL );
//	printf( "读线程已退出\n" );
//#else //WIN32
//	HANDLE    ht[2];
//	ht[0] = HXBCBEGINTHREADEX( NULL, 0, ReadThread, pThreadParam, 0, NULL );
//	if ( NULL == ht[0] )
//	{
//		printf( "创建读线程失败\n" );
//		return -1;
//	}
//	ht[1] = HXBCBEGINTHREADEX( NULL, 0, WriteThread, pThreadParam, 0, NULL );
//	if ( NULL == ht[1] )
//	{
//		printf( "创建写线程失败\n" );
//		return -1;
//	}
//	::WaitForMultipleObjects( 2, (const HANDLE*)&ht, true, INFINITE );//等待所有线程结束；
//	printf( "读写线程都已退出\n" );
//#endif //WIN32
//
//#ifdef WIN32
//	unsigned long timeconsume = ::GetTickCount() - sttick;
//	printf ( "耗时%d\n", timeconsume );
//#endif //WIN32
//	
//	delete pThreadParam->pTestRWQue; pThreadParam->pTestRWQue = NULL;
//	delete pThreadParam; pThreadParam = NULL;
//
//	return 0;
//};
//
