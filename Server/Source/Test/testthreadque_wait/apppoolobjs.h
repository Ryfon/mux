﻿/*

    应用所使用的各种对象池；

	by dzj, 09.05.25
*/

#include "newcache.h"

typedef TNewCache<TestPoolObj> T_TestPool;//测试池；
typedef TNewCache<NewMsgToPut>    T_MsgToPutPool;//NewMsgToPut池；
typedef TNewCache<R_PLAYER>    T_TestPlayerPool;//TestPlayer池；

extern T_MsgToPutPool* g_MsgToPutPool;//NewMsgToPut池；
extern T_TestPlayerPool*  g_TestPlayerPool;//LSocket池；

///初始化全局对象池;
bool InitGloblePools();

///释放全局对象池;
bool ReleaseGloblePools();




