﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#pragma once
#include "../../../Base/PkgProc/DealPkgBase.h"
#include "../../../Base/PkgProc/SrvProtocol.h"
#include <vector>
#include <string>

#include "../../../Base/tcache/tcache.h"

using namespace MUX_PROTO;

class CGateSrv;

struct PlayerGateID
{
public:
	PlayerGateID()
	{
		PoolObjInit();
	};

	PoolFlagDefine()
	{
		nSessionID = 0;
		playerID.dwPID = 0;
		playerID.wGID = 0;
		mapID = 0;
		playerUID = 0;
		SafeStrCpy( playerRoleName, "" );
		playerCopyID = 0;
	}

public:
	int nSessionID;//所属的连接，如果到此gatesrv的连接断开，则清去该连接上的在线玩家???
	PlayerID playerID;//玩家的在线唯一标识；
	USHORT   mapID;
	int      playerUID;//玩家的唯一物理标识
	char     playerRoleName[MAX_NAME_SIZE];//帐号所选角色的昵称，初始为空，待玩家选定角色后变为所选角色的昵称;
	unsigned int playerCopyID;//玩家当前所在的副本ID号；
};

///mapsrv消息处理类;
class CDealGateSrvPkg : public IDealPkg<CGateSrv>
{
public:
	CDealGateSrvPkg() {};
	virtual ~CDealGateSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();
public:
	///gatesrv断开时，认为所有该srv上的玩家都已下线；
    static void GateSrvDisconnect( unsigned short gateServerID );

public:
	///gate报告自身ID号；//G_E_REPORT_SELFID
	static bool OnReportGateID( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///gatesrv发来的查询该玩家是否已经在线消息;
	static bool OnPlayerIsExist( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///gatesrv发来的玩家离线消息，收到后将玩家从列表中删去;
	static bool OnPlayerOffline( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//gatesrv发来的角色离线消息，收到后将角色从列表中删去；
	static bool OnRoleOffline( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///gatesrv发来的玩家查询消息
	static bool OnInvitePlayerJoinTeam( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///gatesrv发来转发的ERROR消息
	static bool OnInviteError( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///gatesrv发来转发的mapID消息
	//static bool OnPlayerMapInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	///gatesrv发来成功消息
	static bool OnInviteSuccess( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///gatesrv发来世界聊天消息；//GESrvGroupChat
	static bool OnSrvGroupChat( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
    ///私聊；//GEPrivateChat
	static bool OnPrivateChat( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///gatesrv发来玩家角色上线的消息
	static bool OnRecvPlayerRoleOnLine( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///gatesrv发送来加好友的请求
	static bool OnRecvPlayerAddFriendReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///gatesrv接到了传来的请求
	static bool OnRecvPlayerReturnAddFriendReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///gatesrv传来的申请入队请求]
	static bool OnReqJoinTeam( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///gatesrv传来的ERROR
	static bool OnReqJoinError( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//反馈失败
	static bool OnFeedbackError( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	/// 群聊加入玩家(群主)
	static bool OnAddChatGroupMemberByOwner( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//关于IRC用户查询玩家信息
	static bool OnIrcQueryPlayerInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//关于IRC用户查询的反馈
	static bool OnIrcQueryPlayerInfoRst( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//天谴询问排行榜在线的魔头
	static bool OnCheckPunishment( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//GM工具转发
	static bool OnGmCmdRequest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	////种族群发
	//static bool OnRaceGroupNotify( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnNoticePlayerHaveUnReadMail( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//通知活动状态变更
	static bool OnNotifyActivityState( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnGMQueryTargetCheck( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	///Register( G_E_PLAYER_TRY_COPY_QUEST, &OnPlayerTryCopyQuest );//玩家试图进副本；
	static bool OnPlayerTryCopyQuest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
    ///Register( G_E_PLAYER_LEAVE_COPY_QUEST, &OnPlayerLeaveCopyQuest );//玩家离开副本请求；
	static bool OnPlayerLeaveCopyQuest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( G_E_COPY_NOMORE_PLAYER, &OnCopyNomorePlayer );//mapsrv发来，不允许更多玩家进入指令(多半是准备待删)
	static bool OnCopyNomorePlayer( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( G_E_DEL_COPY_QUEST, &OnDelCopyQuest );//mapsrv发来，请求删副本；
	static bool OnDelCopyQuest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//Register( G_E_QUERY_GLOBE_COPYINFOS, &OnQueryGlobeCopyInfos );//gate发来，查询全局副本信息；
	static bool OnQueryGlobeCopyInfos( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//Register( G_E_QUERY_TEAM_COPYINFOS, &OnQueryTeamCopyInfos );//gate发来，查询组队副本信息；
	static bool OnQueryTeamCopyInfos( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//Register( G_E_STEAM_DESTORY_CENTERNOTI, &OnSTeamDestoryCenterNoti );//gate通知center，副本组队解散；
	static bool OnSTeamDestoryCenterNoti( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//Register( G_E_LEAVE_STEAM_CENTERNOTI, &OnLeaveSTeamCenterNoti );//gate通知center,玩家离开副本组队；
	static bool OnLeaveSTeamCenterNoti( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//Register( G_E_COPYMAPSRV_NOTI, &OnCopyMapsrvNoti );//gate发来，新的copymapsrvID号；
	static bool OnCopyMapsrvNoti( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//Register( G_E_RESET_COPY, &OnResetCopy );//玩家请求重置副本；
	static bool OnResetCopy( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//接受伪线信息
	static bool OnRecvPWBaseInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//接受伪线地图信息
	static bool OnRecvPWMapBaseInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//玩家进入伪线前的检查
	static bool OnEnterPWMapCheck( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//玩家进入伪线出错
	static bool OnEnterPWMapErr( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//玩家离开伪线的通知
	static bool OnLeavePWMapNtf( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//修改伪线状态
	static bool OnModifyPWStateReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//修改伪线地图状态
	static bool OnModifyPWMapStateReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );


public:
	static CGateSrv* FindGateSrv( unsigned short wGID );

private:
	///添加 昵称<-->帐号 映射；
	//管传淇修改 2008.9.8 添加了playerUID
	static bool AddNameAccountMap( const char* inName/*昵称*/, const char* inAccount/*帐号*/,unsigned int playerUID );

	///添加 帐号<-->PlayerGateID 映射；
	static void AddAccountPlayerIDMap( const char* inAccount, PlayerGateID* playerGateID );

	///帐号对应玩家下线时，将本center保存的该玩家在线信息删去, 如果之前确实有该玩家，返回true，否则返回false；
	static bool DelPlayerMapInfo( const char* inAccount );

	///角色对应玩家重新选择角色时，将本center保存的该玩家在线信息删去, 如果之前确实有该玩家，返回true，否则返回false；
	static bool DelRoleMapInfo( const char* inName );

	static PlayerGateID* FindOnlinePlayerGateIDByNickName( const char* inName/*昵称*/ )
	{
		map< string, string >::iterator nickiter = m_mapNameAccount.find( inName );
		if ( m_mapNameAccount.end() == nickiter )
		{
			return NULL;
		}

		map< string, PlayerGateID* >::iterator accountiter = m_mapAccountPlayerID.find( nickiter->second );
		if ( m_mapAccountPlayerID.end() == accountiter )
		{
			//有"名字<-->帐号"映射，但是帐号当前已下线，应清除"名字<-->帐号"映射
			m_mapNameAccount.erase( nickiter );
			return NULL;
		}

		bool isNameCheckOK = ( 0 == strcmp( inName, accountiter->second->playerRoleName ) );
		if ( isNameCheckOK )
		{
			//该帐号的在线角色确实为角色inName
			return accountiter->second;
		} else {
			return NULL;//帐号之前的inName角色已下线，帐号另选了一个其它角色游戏；
		}
	}

public:
	static PlayerGateID* FindOnlinePlayerGateIDByAccount( const char* inAccount/*帐号*/ );

private:
	static map< string, PlayerGateID* > m_mapAccountPlayerID;	 //所有在线玩家，帐号<--->玩家信息映射；
	static map< string, string > m_mapNameAccount;               //角色名<-->帐号名映射

	static map< unsigned short, CGateSrv* > m_mapGateIDGateSrv;	 //所有gateID<-->CGateSrv*；
};

