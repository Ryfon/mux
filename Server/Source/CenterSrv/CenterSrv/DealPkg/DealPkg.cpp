﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "DealPkg.h"
#include "../../../Base/PkgProc/SrvProtocol.h"
#include "../OtherServer/otherserver.h"

#include "../copymanager/copymanager.h"
#include "../PseudowireManager.h"

#include <string.h>

#include <algorithm>

map< string, PlayerGateID* > CDealGateSrvPkg::m_mapAccountPlayerID;		//所有在线玩家，帐号<--->玩家信息映射；
map< unsigned short, CGateSrv* > CDealGateSrvPkg::m_mapGateIDGateSrv;	//所有gateID<-->CGateSrv*；
map< string, string > CDealGateSrvPkg::m_mapNameAccount;                //角色名<-->帐号名映射

TCache< PlayerGateID >* g_poolPlayerGateID = NULL;

//组建向本组服务器所有gate广播的广播包；
#define CreateCenterBrocastPkg( PKGTYPE, pPkg ) CPacketBuild::CreateSrvBrocastPkg<PKGTYPE>( &pPkg );

///初始化(注册各命令字对应的处理函数);
void CDealGateSrvPkg::Init()
{
	Register( G_E_REPORT_SELFID, &OnReportGateID );//gate报告自身ID号；
	Register( G_E_PLAYER_ISEXIST, &OnPlayerIsExist );//玩家选择角色；
	Register( G_E_PLAYER_OFFLINE, &OnPlayerOffline );//玩家离线；
	Register( G_E_ROLE_OFFLINE, &OnRoleOffline );//角色离线
	Register( G_E_INVITE_PLAYER_JOIN_TEAM, &OnInvitePlayerJoinTeam );  //玩家邀请入队
	Register( G_E_INVITE_ERROR,  &OnInviteError );	 //组队错误的转发
	Register( G_E_INVITE_SUCCESS, &OnInviteSuccess );	//组队应答成功的转发
//	Register( G_E_PLAYER_MAPINFO, &OnPlayerMapInfo );   //第一次上线通知具体姓名

	Register( G_E_SRVGROUP_CHAT, &OnSrvGroupChat );//世界聊天；//GESrvGroupChat
	Register( G_E_PRIVATE_CHAT, &OnPrivateChat );//私聊；//GEPrivateChat

	Register( G_E_PLAYERROLE_ONLINE, &OnRecvPlayerRoleOnLine );//玩家上线
	Register( G_E_ADDFRINED,&OnRecvPlayerAddFriendReq );//玩家加好友的请求
	Register( G_E_REQADDFRIEND_RESULT,&OnRecvPlayerReturnAddFriendReq );//返回玩家加好友的结果

	Register( G_E_REQ_JOIN_TEAM, &OnReqJoinTeam );//转发申请组队
	Register( G_E_REQ_JOIN_ERROR, &OnReqJoinError );//转发申请组队错误
	Register( G_E_FEEDBACK_ERROR, &OnFeedbackError );//转发申请组队错误

	Register( G_E_ADD_CHAT_GROUP_MEMBER_BY_OWNER, &OnAddChatGroupMemberByOwner);//群聊加入成员(群主)
	Register( G_E_IRC_QUERY_PLAYERINFO, &OnIrcQueryPlayerInfo);
	Register( G_E_IRC_QUERY_PLAYERINFO_RST, &OnIrcQueryPlayerInfoRst);
	Register( G_E_CHECK_PUNISHMENT, &OnCheckPunishment);
	Register( G_E_GMCMD_REQUEST, &OnGmCmdRequest );  
	//Register( G_E_RACE_GROUP_NOTIFY, &OnRaceGroupNotify );  
	Register( G_E_NOTICE_PLAYER_HAVE_UNREAD_MAIL, &OnNoticePlayerHaveUnReadMail );
	Register( G_E_NOTIFY_ACTIVITY_STATE, &OnNotifyActivityState );
	Register( G_E_GMQUERY_TARGET_CHECK, &OnGMQueryTargetCheck );

	Register( G_E_PLAYER_TRY_COPY_QUEST, &OnPlayerTryCopyQuest );//玩家试图进副本；
	Register( G_E_PLAYER_LEAVE_COPY_QUEST, &OnPlayerLeaveCopyQuest );//玩家离开副本请求；
	Register( G_E_COPY_NOMORE_PLAYER, &OnCopyNomorePlayer );//mapsrv发来，不允许更多玩家进入指令(多半是准备待删)
	Register( G_E_DEL_COPY_QUEST, &OnDelCopyQuest );//mapsrv发来，请求删副本；

	Register( G_E_QUERY_GLOBE_COPYINFOS, &OnQueryGlobeCopyInfos );//gate发来，查询全局副本信息；
	Register( G_E_QUERY_TEAM_COPYINFOS, &OnQueryTeamCopyInfos );//gate发来，查询组队副本信息；

	Register( G_E_STEAM_DESTORY_CENTERNOTI, &OnSTeamDestoryCenterNoti );//gate通知center，副本组队解散；
	Register( G_E_LEAVE_STEAM_CENTERNOTI, &OnLeaveSTeamCenterNoti );//gate通知center,玩家离开副本组队；

	Register( G_E_COPYMAPSRV_NOTI, &OnCopyMapsrvNoti );//gate发来，新的copymapsrvID号；

	Register( G_E_RESET_COPY, &OnResetCopy );//玩家请求重置副本；

	Register( G_E_PW_BASE_INFO, &OnRecvPWBaseInfo );//接受伪线信息
	Register( G_E_PWMAP_BASE_INFO, &OnRecvPWMapBaseInfo );//接受伪线地图信息
	Register( G_E_ENTER_PWMAP_CHECK, &OnEnterPWMapCheck );//玩家进入伪线前的检查
	Register( G_E_ENTER_PWMAP_ERR, &OnEnterPWMapErr );//玩家进入伪线出错
	Register( G_E_LEAVE_PWMAP_NTF, &OnLeavePWMapNtf );//玩家离开伪线的通知
	Register( G_E_MODIFY_PW_STATE_REQ, &OnModifyPWStateReq );//修改伪线状态
	Register( G_E_MODIFY_PWMAP_STATE_REQ, &OnModifyPWMapStateReq );//修改伪线地图状态
}

CGateSrv* CDealGateSrvPkg::FindGateSrv( unsigned short wGID )
{
	CGateSrv* pOutGateSrv = NULL;
	map< unsigned short, CGateSrv* >::iterator iter = m_mapGateIDGateSrv.find( wGID );
	if ( iter != m_mapGateIDGateSrv.end() )
	{
		pOutGateSrv = iter->second;
		if ( NULL != pOutGateSrv )
		{
			if ( pOutGateSrv->GetGateSrvID() != wGID )
			{
				D_ERROR( "FindGateSrv, m_mapGateIDGateSrv的SELF_SRV_ID校验失败!\n" );
				return NULL;
			} else {
				return pOutGateSrv;
			}
		}
	}
	return NULL;
}

///gatesrv断开时，认为所有该srv上的玩家都已下线；
void CDealGateSrvPkg::GateSrvDisconnect( unsigned short gateServerID )
{
	TRY_BEGIN;

	for ( map< string, PlayerGateID* >::iterator it = m_mapAccountPlayerID.begin(); it != m_mapAccountPlayerID.end(); )
	{
		if ( NULL == it->second )
		{
			++it;
			continue;
		}
		if ( it->second->playerID.wGID == gateServerID )
		{
			if ( NULL != g_poolPlayerGateID )
			{
				g_poolPlayerGateID->Release( it->second );
			}
			m_mapAccountPlayerID.erase( it++ );
		} else {
			++it;
		}
	}

	m_mapGateIDGateSrv.erase( gateServerID );//清gatesrv map中的对应gatesrv指针
	D_INFO( "GateSrv:%d已断开\n\n", gateServerID );

	return;
	TRY_END;
	return;
}

//由于linux下无此函数，故添加, by dzj, 09.02.12；
inline bool mystrlwr( char* instr )
{
	if ( NULL == instr )
	{
		return false;
	}

	int inlen = (int) strlen( instr );
	if ( ( inlen <= 0 )
		|| ( inlen >= 512 )
		)
	{
		//不可能，本程序中不会超过此大小；
		return false;
	}

	char* tmpptr = instr;
	for ( ; *tmpptr!='\0'; tmpptr++ )
	{
		*tmpptr = tolower(*tmpptr);
	}

	return true;
}

///gatesrv发来的玩家离线消息，收到后将玩家从列表中删去;
bool CDealGateSrvPkg::OnPlayerOffline( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerOffline，pOwner指针空\n", 0 );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnPlayerOffline NULL == pPkg\n");
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GEPlayerOffline) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const GEPlayerOffline* playerOffline = (const GEPlayerOffline*)pPkg;

	char tmpAccount[128];
	StructMemSet( tmpAccount, 0, sizeof(tmpAccount) );
	SafeStrCpy( tmpAccount, playerOffline->szAccount );
	if ( !mystrlwr( tmpAccount ) )
	{
		D_ERROR( "CDealGateSrvPkg::OnPlayerOffline，错误的帐号名，playerID(%d:%d)\n"
			, playerOffline->playerID.wGID, playerOffline->playerID.dwPID );
		return false;
	}

	if ( DelPlayerMapInfo( tmpAccount ) )
	{
		//从列表中删去该玩家；
		D_FORCE( "离开服务器组,玩家:%s\n", playerOffline->szAccount );
	} else {
		D_WARNING( "OnPlayerOffline，centersrv中找不到玩家%s\n", playerOffline->szAccount );
	}

	return true;
	TRY_END;
	return false;
}


//gatesrv发来的角色离线消息，收到后将角色从列表中删去；
bool CDealGateSrvPkg::OnRoleOffline(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnRoleOffline，pOwner指针空\n", 0 );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnRoleOffline NULL == pPkg\n");
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GERoleOffline) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const GERoleOffline* roleOffline = (const GERoleOffline*)pPkg;

	if ( DelRoleMapInfo( roleOffline->szName ) )
	{
		//从列表中删去该角色；
		D_FORCE( "离开服务器组,角色:%s\n", roleOffline->szName );
	} else {
		D_WARNING( "OnRoleOffline，centersrv中找不到角色%s\n", roleOffline->szName );
	}

	return true;
	TRY_END;
	return false;
}

///添加 昵称<-->帐号 映射；
//管传淇修改 2008.9.8 添加了playerUID
bool CDealGateSrvPkg::AddNameAccountMap( const char* inName/*昵称*/, const char* inAccount/*帐号*/,unsigned int playerUID )
{
	if ( ( NULL == inName ) || ( NULL == inAccount ) )
	{
		D_ERROR( "CDealGateSrvPkg::AddNameAccountMap, ( ( NULL == inName ) || ( NULL == inAccount ) )\n" );
		return false;
	}

	char tmpAccount[128];
	StructMemSet( tmpAccount, 0, sizeof(tmpAccount) );
	SafeStrCpy( tmpAccount, inAccount );
	if ( !mystrlwr( tmpAccount ) )
	{
		D_ERROR( "CDealGateSrvPkg::AddNameAccountMap，错误的帐号名，playerUID(%d)\n"
			, playerUID );
		return false;
	}

	PlayerGateID* playerGateID = FindOnlinePlayerGateIDByAccount( tmpAccount );
	if ( NULL == playerGateID )
	{
		//该帐号不在线，可能期间已下线；
		return false;
	}
	playerGateID->playerUID = playerUID;
	SafeStrCpy( playerGateID->playerRoleName, inName );//帐号在线，记录该帐号玩家所选角色的昵称；

	map< string, string >::iterator iter = m_mapNameAccount.find( inName );
	if ( m_mapNameAccount.end() != iter )
	{
		iter->second = string( tmpAccount );
	} else {
		m_mapNameAccount.insert( pair<string, string>( inName, tmpAccount ) );
	}

	return true;
}

///添加 帐号<-->PlayerGateID 映射；
void CDealGateSrvPkg::AddAccountPlayerIDMap( const char* inAccount, PlayerGateID* playerGateID )
{
	if ( NULL == playerGateID )
	{
		D_ERROR( "AddAccountPlayerIDMap, NULL == playerGateID\n" );
		return;
	}

	char tmpAccount[128];
	StructMemSet( tmpAccount, 0, sizeof(tmpAccount) );
	SafeStrCpy( tmpAccount, inAccount );
	if ( !mystrlwr( tmpAccount ) )
	{
		D_ERROR( "CDealGateSrvPkg::AddAccountPlayerIDMap，错误的帐号名，playerID(%d:%d)\n"
			, playerGateID->playerID.wGID, playerGateID->playerID.dwPID );
		return;
	}

	map< string, PlayerGateID* >::iterator iter = m_mapAccountPlayerID.find( tmpAccount );
	if ( m_mapAccountPlayerID.end() == iter )
	{
		m_mapAccountPlayerID.insert( pair<string, PlayerGateID*>( tmpAccount, playerGateID ) );
	} else {
		iter->second = playerGateID;
	}
}

PlayerGateID* CDealGateSrvPkg::FindOnlinePlayerGateIDByAccount( const char* inAccount/*帐号*/ )
{
	if ( NULL == inAccount )
	{
		return NULL;
	}

	char tmpAccount[128];
	StructMemSet( tmpAccount, 0, sizeof(tmpAccount) );
	SafeStrCpy( tmpAccount, inAccount );
	if ( !mystrlwr( tmpAccount ) )
	{
		D_ERROR( "CDealGateSrvPkg::FindOnlinePlayerGateIDByAccount，错误的帐号名\n" );
		return NULL;
	}

	map< string, PlayerGateID* >::iterator accountiter = m_mapAccountPlayerID.find( tmpAccount );
	if ( m_mapAccountPlayerID.end() == accountiter )
	{
		return NULL;
	}

	return accountiter->second;
}

bool CDealGateSrvPkg::OnInvitePlayerJoinTeam( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnInvitePlayerJoinTeam，pOwner指针空\n", 0 );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnInvitePlayerJoinTeam NULL == pPkg\n");
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GEInvitePlayerJoinTeam) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GEInvitePlayerJoinTeam* pMsg = (GEInvitePlayerJoinTeam*)pPkg;

	//寻找被邀请的玩家id
	PlayerGateID* pGateID = FindOnlinePlayerGateIDByNickName( pMsg->beInvitedPlayerName );
	if( NULL == pGateID )
	{
		EGInviteError errMsg;
		errMsg.nErrorCode = PLAYER_NOT_FIND;
		errMsg.invitePlayerID = pMsg->inviterInfo.invitePlayerID;
		//被邀请玩家
		SafeStrCpy( errMsg.beInvitedPlayerName, pMsg->beInvitedPlayerName );
		errMsg.beInvitedPlayerNameLen = pMsg->beInvitedPlayerNameLen;

		MsgToPut* pNewMsg = CreateSrvPkg( EGInviteError, pOwner, errMsg );
		pOwner->SendPkgToSrv( pNewMsg );
		return true;
	}
	
	EGInvitePlayerJoinTeam inviteMsg;
	//被邀请玩家
	inviteMsg.beInvitedPlayerID = pGateID->playerID;
	SafeStrCpy( inviteMsg.beInvitedPlayerName, pMsg->beInvitedPlayerName );
	inviteMsg.beInvitedPlayerNameLen = pMsg->beInvitedPlayerNameLen;
	inviteMsg.inviterInfo = pMsg->inviterInfo;
	
	//找到对应的gatesrv进行发送
	CGateSrv* pSrv = FindGateSrv( pGateID->playerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR( "OnInvitePlayerJoinTeam,找不到对应的GATESRV%d\n", pGateID->playerID.wGID );
		return false;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( EGInvitePlayerJoinTeam, pSrv, inviteMsg );
	pSrv->SendPkgToSrv( pNewMsg );	

	return true;
}


bool CDealGateSrvPkg::OnInviteError( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnInviteError，pOwner指针空\n", 0 );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnInviteError NULL == pPkg\n");
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GEInviteError) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}
	
	const GEInviteError* pSrvMsg = (GEInviteError*)pPkg;
	EGInviteError errMsg;	
	errMsg.nErrorCode = pSrvMsg->nErrorCode;
	SafeStrCpy( errMsg.beInvitedPlayerName, pSrvMsg->beInvitedPlayerName );
	errMsg.beInvitedPlayerNameLen = pSrvMsg->beInvitedPlayerNameLen;
	errMsg.invitePlayerID = pSrvMsg->invitePlayerID;

	CGateSrv* pSrv = FindGateSrv( pSrvMsg->invitePlayerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR("OnInviteError 找不到GateSrv\n159");
		return false;
	}
	
	MsgToPut* pNewMsg = CreateSrvPkg( EGInviteError, pSrv, errMsg );
	pSrv->SendPkgToSrv( pNewMsg );
	return true;
}

bool CDealGateSrvPkg::OnInviteSuccess( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnInviteSuccess，pOwner指针空\n", 0 );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnInviteSuccess NULL == pPkg\n");
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GEInviteSuccess) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	GEInviteSuccess* pSrvMsg = (GEInviteSuccess*)pPkg;
	
	EGInviteSuccess gateMsg;
	gateMsg.beInvitedPlayer = pSrvMsg->beInvitedPlayer;
	gateMsg.invitePlayerID = pSrvMsg->invitePlayerID;

	CGateSrv* pGateSrv = FindGateSrv( pSrvMsg->invitePlayerID.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR("OnInviteSuccess 找不到对应的GateSrv\n");
		return false;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( EGInviteSuccess, pGateSrv, gateMsg );
	pGateSrv->SendPkgToSrv( pNewMsg );
	return true;
}

///私聊；//GEPrivateChat
bool CDealGateSrvPkg::OnPrivateChat( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPrivateChat，pOwner指针空\n", 0 );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnPrivateChat NULL == pPkg\n");
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GEPrivateChat) )
	{ 
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GEPrivateChat* pSrvMsg = (const GEPrivateChat*)pPkg;

	//找到说话对象对应的帐号，根据帐号找到playerid，进而找到所在的gate，将聊天消息转发出去；
	//根据姓名，找到帐号名
	PlayerGateID* playerGateID = FindOnlinePlayerGateIDByNickName( pSrvMsg->tgtPlayerName );
	if ( NULL == playerGateID )
	{
		//没有该在线角色；
		D_WARNING( "OnPrivateChat，找不到在线角色%s\n", pSrvMsg->tgtPlayerName );

		//通知聊天发起者
		PlayerGateID *pChatPlayerID = FindOnlinePlayerGateIDByNickName( pSrvMsg->chatPlayerName);
		if(NULL != pChatPlayerID)
		{		
			EGChatError chatError;
			
			chatError.nType = CT_PRIVATE_CHAT;
			chatError.errorCode = (unsigned char)EGChatError::TARGET_NOT_EXIST;
			chatError.lNotiPlayerID = pChatPlayerID->playerID;
			SafeStrCpy(chatError.tgtPlayerName, pSrvMsg->tgtPlayerName);//, sizeof(chatError.tgtPlayerName));

			CGateSrv* pGateSrv = FindGateSrv( pChatPlayerID->playerID.wGID );
			if(NULL != pGateSrv)
			{
				MsgToPut *pChatErrorMsg = CreateSrvPkg( EGChatError, pGateSrv, chatError);
				pGateSrv->SendPkgToSrv(pChatErrorMsg);
			}
		}
		return false;
	}

	CGateSrv* pGateSrv = FindGateSrv( playerGateID->playerID.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR("OnPrivateChat，找不到对应的GateSrv\n");
		return false;
	}
	
	EGPrivateChat gateMsg;
	StructMemSet( gateMsg, 0, sizeof( EGPrivateChat ) );
	gateMsg.chatType = pSrvMsg->chatType;
	gateMsg.tgtPlayerID = playerGateID->playerID;//目标玩家ID号；
	SafeStrCpy( gateMsg.chatPlayerName, pSrvMsg->chatPlayerName );
	SafeStrCpy( gateMsg.tgtPlayerName, pSrvMsg->tgtPlayerName );
	SafeStrCpy( gateMsg.chatContent, pSrvMsg->chatContent );

	MsgToPut* pGateMsg = CreateSrvPkg( EGPrivateChat, pGateSrv, gateMsg );
	pGateSrv->SendPkgToSrv( pGateMsg );

	return true;
	TRY_END;
	return false;
}

///gatesrv发来世界聊天消息；//GESrvGroupChat
bool CDealGateSrvPkg::OnSrvGroupChat(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnSrvGroupChat，pOwner指针空\n", 0 );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnSrvGroupChat NULL == pPkg\n");
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GESrvGroupChat) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GESrvGroupChat* pSrvMsg = (const GESrvGroupChat*)pPkg;
	
	EGSrvGroupChat gateMsg;
	StructMemSet( gateMsg, 0, sizeof( EGSrvGroupChat ) );
	gateMsg.chatType = pSrvMsg->chatType;
	gateMsg.extInfo = pSrvMsg->extInfo;
	SafeStrCpy( gateMsg.chatPlayerName, pSrvMsg->chatPlayerName );
	SafeStrCpy( gateMsg.chatContent, pSrvMsg->chatContent );

	MsgToPut* pBrocastMsg = CreateCenterBrocastPkg( EGSrvGroupChat, gateMsg );//全gate广播消息；
	pOwner->SendPkgToSrv( pBrocastMsg );//随便选一个连接发送消息，因为到了底层会解析广播包发送给所有的连入连接，而不是pOwner;

	return true;
	TRY_END;
	return false;
}


//bool CDealGateSrvPkg::OnPlayerMapInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(GEPlayerMapInfo) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//	
//	GEPlayerMapInfo* pSrvMsg = (GEPlayerMapInfo*)pPkg;
//	PlayerGateID tmpID;
//	tmpID.playerID = pSrvMsg->playerID;
//	tmpID.nSessionID = pOwner->GetSessionID();
//	m_mapNameOnlinePlayers.insert( pair<string, PlayerGateID>( pSrvMsg->szPlayerName, tmpID ) );		//新名字差入map
//
//	return true;
//}

///帐号对应玩家下线时，将本center保存的该玩家在线信息删去, 如果之前确实有该玩家，返回true，否则返回false；
bool CDealGateSrvPkg::DelPlayerMapInfo( const char* inAccount )
{
	if ( NULL == inAccount )
	{
		D_ERROR( "CDealGateSrvPkg::DelPlayerMapInfo，NULL == inAccount\n" );
		return false;
	}

	map< string, PlayerGateID* >::iterator iter = m_mapAccountPlayerID.find( inAccount );
	if ( m_mapAccountPlayerID.end() != iter )
	{
		PlayerGateID* pPlayerGateID = iter->second;
		if ( NULL == pPlayerGateID )
		{
			D_ERROR( "CDealGateSrvPkg::DelPlayerMapInfo，NULL == pPlayerGateID\n" );
			return false;
		}

		if ( 0 != pPlayerGateID->playerCopyID )
		{
			D_DEBUG( "玩家(%d:%x)下线，离开副本%d\n", pPlayerGateID->playerID.wGID, pPlayerGateID->playerID.dwPID, pPlayerGateID->playerCopyID );
			CCopyManager::PlayerOfflineLeaveCopy( pPlayerGateID->playerCopyID, pPlayerGateID->playerID );
			pPlayerGateID->playerCopyID = 0;
		}

		if ( NULL != g_poolPlayerGateID )
		{
			g_poolPlayerGateID->Release( iter->second );
		}
		m_mapAccountPlayerID.erase( iter );
		return true;
	} else {
		return false;
	}
}

///角色对应玩家重新选择角色时，将本center保存的该玩家在线信息删去, 如果之前确实有该玩家，返回true，否则返回false；
bool CDealGateSrvPkg::DelRoleMapInfo( const char* inName )
{
	if ( NULL == inName )
	{
		D_ERROR( "CDealGateSrvPkg::DelCharacterMapInfo，NULL == inName\n" );
		return false;
	}

	map< string, string >::iterator iter = m_mapNameAccount.find( inName );
	if ( m_mapNameAccount.end() != iter )
	{

		map< string, PlayerGateID* >::iterator accountIt = m_mapAccountPlayerID.find( iter->second );
		if ( m_mapAccountPlayerID.end() != accountIt )
		{
			PlayerGateID* pPlayerGateID = accountIt->second;
			if ( NULL == pPlayerGateID )
			{
				D_ERROR( "CDealGateSrvPkg::DelRoleMapInfo，NULL == pPlayerGateID\n" );
				return false;
			}

			if ( 0 != pPlayerGateID->playerCopyID )
			{
				D_DEBUG( "玩家(%d:%x)下线，离开副本%d\n", pPlayerGateID->playerID.wGID, pPlayerGateID->playerID.dwPID, pPlayerGateID->playerCopyID );
				CCopyManager::PlayerOfflineLeaveCopy( pPlayerGateID->playerCopyID, pPlayerGateID->playerID );
				pPlayerGateID->playerCopyID = 0;
			}
		}
		m_mapNameAccount.erase( iter );
		return true;
	} else {
		return false;
	}
}

///gate报告自身ID号；//G_E_REPORT_SELFID
bool CDealGateSrvPkg::OnReportGateID( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnReportGateID，pOwner指针空\n", 0 );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnReportGateID NULL == pPkg\n");
		return false;
	}

	//GEReportSelfID
	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GEReportSelfID) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const GEReportSelfID* gateIDReport = (const GEReportSelfID*)pPkg;
	unsigned short wGID = (unsigned short) (gateIDReport->selfSrvID);
	D_INFO( "gatesrv : %d, 报来其SELF_SRV_ID\n", gateIDReport->selfSrvID );

	map< unsigned short, CGateSrv* >::iterator gatemapiter = m_mapGateIDGateSrv.find( wGID );//对应gatesrv加入gateID<-->gatesrv映射表；
	if ( m_mapGateIDGateSrv.end() != gatemapiter )
	{
		D_ERROR( "报来的gatesrvID号与之前的ID号冲突，断开此连接\n" );
		pOwner->ReqDestorySelf();	
		return true;
	} else {
		m_mapGateIDGateSrv.insert( pair<unsigned short, CGateSrv*>( wGID, pOwner ) );
	}

	pOwner->SetGateSrvID( wGID );

	return true;
	TRY_END;
	return false;
}

///gatesrv发来的查询该玩家是否已经在线消息;
bool CDealGateSrvPkg::OnPlayerIsExist( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnPlayerIsExist pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnPlayerIsExist NULL == pPkg\n");
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GEPlayerIsExist) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const GEPlayerIsExist* playerIsExist = (const GEPlayerIsExist*)pPkg;

	//以下检查玩家是否在线；
	bool isAlreadyOnline = false;
	PlayerID orgOnlinePlayerID;

	PlayerGateID* playerGate = FindOnlinePlayerGateIDByAccount( playerIsExist->szAccount );
	if ( NULL != playerGate )
	{
		//原来有同帐号玩家；
		D_FORCE( "重复登录,玩家%s\n", playerIsExist->szAccount );
		orgOnlinePlayerID = playerGate->playerID;
		isAlreadyOnline = true;
	} else {
		//该帐号为第一次登录，在map中增加相应表项；
		D_FORCE( "登录服务器组,玩家%s\n", playerIsExist->szAccount );
		if ( NULL == g_poolPlayerGateID )
		{
			D_ERROR( "OnPlayerIsExist,g_poolPlayerGateID空\n" );
			return false;
		}
		playerGate = g_poolPlayerGateID->RetrieveOrCreate();
		playerGate->nSessionID = pOwner->GetSessionID();
		playerGate->playerID = playerIsExist->playerID;
		SafeStrCpy( playerGate->playerRoleName, "" );//初始昵称空；

		AddAccountPlayerIDMap( playerIsExist->szAccount, playerGate );//帐号加入在线帐号列表；	
	}

	//消息回GateSrv;
	EGPlayerIsExist  returnMsg;
	if ( !isAlreadyOnline )
	{
		//之前角色不在线；
		returnMsg.nErrCode = EGPlayerIsExist::UNIQUE_CHECK_OK;
		SafeStrCpy( returnMsg.szAccount, playerIsExist->szAccount );
		returnMsg.playerID = playerIsExist->playerID;
		returnMsg.sequenceID = playerIsExist->sequenceID;
	} else {
		//角色之前已经在线，返回已在线玩家的ID号；
		returnMsg.nErrCode = EGPlayerIsExist::UNIQUE_CHECK_FAILED;
		SafeStrCpy( returnMsg.szAccount, playerIsExist->szAccount );
		returnMsg.playerID = playerIsExist->playerID;
		returnMsg.onlinePlayerID = orgOnlinePlayerID;
		returnMsg.sequenceID = playerIsExist->sequenceID;
	}

//多余判断，因为之前已经判断过了；	if ( NULL != pOwner )
	{
		MsgToPut* pNewMsg = CreateSrvPkg( EGPlayerIsExist, pOwner, returnMsg );
		pOwner->SendPkgToSrv( pNewMsg );
	}

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnRecvPlayerRoleOnLine(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPlayerRoleOnLine pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPlayerRoleOnLine NULL == pPkg\n");
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(GEPlayerRoleOnLine) )
	{
		return false;
	}

	const GEPlayerRoleOnLine* pMsg = (const GEPlayerRoleOnLine*) pPkg;

	if ( ! AddNameAccountMap( pMsg->playerName, pMsg->accountName ,pMsg->playerUID ) )
	{
		D_WARNING( "OnRecvPlayerRoleOnLine, 帐号%s已下线\n", pMsg->accountName );
	}

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnRecvPlayerAddFriendReq(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPlayerAddFriendReq pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPlayerAddFriendReq NULL == pPkg\n");
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GEAddFriend) )
	{
		return false;
	}

	const GEAddFriend* pMsg = ( const GEAddFriend *)pPkg;

	D_DEBUG("接到GateSrv加好友的请求:%s请求加%s\n",pMsg->launchPlayerName,pMsg->targetPlayerName );

	//根据姓名，找到帐号名
	PlayerGateID* pGateID = FindOnlinePlayerGateIDByNickName( pMsg->targetPlayerName );
	if ( NULL == pGateID )
	{
		EGAddFriendFailed addFriendError;
		addFriendError.errCode = EGAddFriendFailed::ADD_FRIEND_NOT_EXIST;
		addFriendError.launchPlayer = pMsg->luanchPlayerID;
		SafeStrCpy( addFriendError.targetPlayerName, pMsg->targetPlayerName );
		MsgToPut* pNewMsg = CreateSrvPkg( EGAddFriendFailed, pOwner, addFriendError );
		pOwner->SendPkgToSrv( pNewMsg );	

		D_DEBUG("加好友失败，目标玩家(昵称:%s)不在线\n", pMsg->targetPlayerName);
		return false;
	}

	CGateSrv* pGateSrv = FindGateSrv( pGateID->playerID.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR("OnRecvPlayerAddFriendReq 找不到对应的GateSrv\n");
		return false;
	}

	EGReqAgreeAddFriend srvmsg;
	srvmsg.luanchID = pMsg->luanchPlayerID;//发起者的ID
	srvmsg.targetID = pGateID->playerID;//目标者的ID
	srvmsg.groupID  = pMsg->groupID;//加入的目标组
	SafeStrCpy( srvmsg.luanchPlayerName, pMsg->launchPlayerName );
	SafeStrCpy( srvmsg.targetPlayerName, pMsg->targetPlayerName )
	MsgToPut* pNewMsg = CreateSrvPkg( EGReqAgreeAddFriend, pGateSrv, srvmsg );
	pGateSrv->SendPkgToSrv( pNewMsg );	

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnRecvPlayerReturnAddFriendReq(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPlayerReturnAddFriendReq pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPlayerReturnAddFriendReq NULL == pPkg\n");
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GEAgreeAddFriendResult) )
	{
		return false;
	}

	const GEAgreeAddFriendResult* pMsg = ( const GEAgreeAddFriendResult*)pPkg;

	D_DEBUG("接到了%s加%s好友的结果:%d\n",pMsg->launchPlayerName, pMsg->targetPlayerName,pMsg->errorCode );

	//加好友请求者的ID
	CGateSrv* pGateSrv1 = FindGateSrv( pMsg->reqPlayerID.wGID );
	if( NULL == pGateSrv1 )
	{
		D_ERROR("OnRecvPlayerReturnAddFriendReq 找不到对应的GateSrv\n");
		return false;
	}
	
	//如果不同意加好友，告诉发起者，拒绝了加好友
	if ( pMsg->errorCode != GCAddFriendResponse::ADD_FRIEND_OK )
	{
		EGAddFriendFailed addFriendFaild;
		addFriendFaild.errCode  = pMsg->errorCode;		//错误原因
		SafeStrCpy( addFriendFaild.targetPlayerName, pMsg->targetPlayerName );//被谁拒绝
		addFriendFaild.launchPlayer = pMsg->reqPlayerID;
		MsgToPut* pNewMsg = CreateSrvPkg( EGAddFriendFailed, pGateSrv1, addFriendFaild );
		pGateSrv1->SendPkgToSrv( pNewMsg );	
	} else {
		//Test试验消息
		PlayerGateID* luanchPlayerID = FindOnlinePlayerGateIDByNickName( pMsg->launchPlayerName );
		PlayerGateID* targetPlayerID = FindOnlinePlayerGateIDByNickName( pMsg->targetPlayerName );

		//if ( ( NULL == luanchPlayerID )
		//	|| ( NULL == targetPlayerID )
		//	)
		//{
		//	D_ERROR("OnRecvPlayerReturnAddFriendReq，center找不到玩家(%s or %s)\n", pMsg->launchPlayerName, pMsg->targetPlayerName );
		//	return false;
		//}

		if (NULL == luanchPlayerID)
		{//加好友发起人此时下线，通知回应人加好友失败
			EGAddFriendFailed addFriendFaild;
			addFriendFaild.errCode  = EGAddFriendFailed::ADD_FRIEND_NOT_EXIST;		//错误原因
			SafeStrCpy( addFriendFaild.targetPlayerName, pMsg->launchPlayerName );//被谁拒绝
			addFriendFaild.launchPlayer = pMsg->responsePlayerID;
			MsgToPut* pNewMsg = CreateSrvPkg( EGAddFriendFailed, pOwner, addFriendFaild );
			pOwner->SendPkgToSrv( pNewMsg );	
			return false;
		}
		else if (NULL == targetPlayerID)
		{
			D_ERROR("OnRecvPlayerReturnAddFriendReq，center找不到玩家(%s)\n", pMsg->targetPlayerName );
			return false;
		}

		//发送给加好友的发起者
		EGAddFriendOK addFriendOKLaunch;
		addFriendOKLaunch.lNoticePlayerID = luanchPlayerID->playerID;
		addFriendOKLaunch.addFriendUID    = targetPlayerID->playerUID;//加这个ID的人为好友
		addFriendOKLaunch.groupID         = pMsg->groupID;
		MsgToPut* pNewMsg = CreateSrvPkg( EGAddFriendOK, pGateSrv1, addFriendOKLaunch );
		pGateSrv1->SendPkgToSrv( pNewMsg );	
		D_DEBUG("发送给加好友成功的消息给GateSrv %s\n", pMsg->launchPlayerName );

		//是否同意加加好友的人为好友
		if ( pMsg->isAddReqer )
		{

			CGateSrv* pGateSrv2 = FindGateSrv(  targetPlayerID->playerID.wGID );
			if( NULL == pGateSrv2 )
			{
				D_ERROR("OnRecvPlayerAddFriendReq 找不到对应的GateSrv\n");
				return false;
			}

			EGAddFriendOK addFriendOKTarget;
			addFriendOKTarget.lNoticePlayerID = targetPlayerID->playerID;
			addFriendOKTarget.addFriendUID  = luanchPlayerID->playerUID;
			addFriendOKTarget.groupID       = 0;
			MsgToPut* pNewMsg1 = CreateSrvPkg( EGAddFriendOK, pGateSrv2, addFriendOKTarget );
			pGateSrv2->SendPkgToSrv( pNewMsg1 );	

			D_DEBUG("发送给加好友成功的消息给GateSrv %s\n",pMsg->targetPlayerName );
		}
	}

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnReqJoinTeam( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnReqJoinTeam pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnReqJoinTeam NULL == pPkg\n");
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GEReqJoinTeam) )
	{
		return false;
	}
	
 	GEReqJoinTeam* pSrvMsg = (GEReqJoinTeam*)pPkg;
	PlayerGateID* pGateID = FindOnlinePlayerGateIDByNickName( pSrvMsg->captainPlayerName );
	if( NULL == pGateID )
	{
		D_WARNING("CDealGateSrvPkg::OnReqJoinTeam 申请入队的队长已经不在 %s\n", pSrvMsg->captainPlayerName);
		EGReqJoinError errmsg;
		SafeStrCpy( errmsg.captainPlayerName, pSrvMsg->captainPlayerName );
		errmsg.captainPlayerNameLen = pSrvMsg->captainPlayerNameLen;
		errmsg.reqPlayerID.dwPID = pSrvMsg->reqPlayer.playerId;
		errmsg.reqPlayerID.wGID = pSrvMsg->reqPlayer.gateId;
		errmsg.nErrorCode = PLAYER_NOT_FIND;
		MsgToPut* pNewMsg = CreateSrvPkg( EGReqJoinError, pOwner, errmsg );
		pOwner->SendPkgToSrv( pNewMsg );
		return false;
	}

	EGReqJoinTeam srvmsg;
	srvmsg.captainPlayerID = pGateID->playerID;
	srvmsg.reqPlayer = pSrvMsg->reqPlayer;
	srvmsg.isReqPlayerEvil = pSrvMsg->isReqPlayerEvil;
	CGateSrv* pSrv = FindGateSrv( pGateID->playerID.wGID );
	if ( NULL == pSrv )
	{
		D_ERROR(" OnReqJoinTeam 找不到对应的GateSrv\n");
		return false;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( EGReqJoinTeam, pSrv, srvmsg );
	pSrv->SendPkgToSrv( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealGateSrvPkg::OnReqJoinError( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnReqJoinError pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnReqJoinError NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEReqJoinError) )
	{
		return false;
	}

	GEReqJoinError* pSrvMsg = (GEReqJoinError*)pPkg;
	
	EGReqJoinError errmsg;
	SafeStrCpy( errmsg.captainPlayerName, pSrvMsg->captainPlayerName );
	errmsg.captainPlayerNameLen = pSrvMsg->captainPlayerNameLen;
	errmsg.reqPlayerID = pSrvMsg->reqPlayerID;
	errmsg.nErrorCode = pSrvMsg->nErrorCode;
	CGateSrv* pGateSrv = FindGateSrv( pSrvMsg->reqPlayerID.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR("OnReqJoinError 找不到GATESRV\n");
		return false;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( EGReqJoinError, pGateSrv, errmsg );
	pGateSrv->SendPkgToSrv( pNewMsg );
	return true;
}


bool CDealGateSrvPkg::OnFeedbackError( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnFeedbackError pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnFeedbackError NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEFeedbackError) )
	{
		return false;
	}
	
	GEFeedbackError* pSrvMsg = (GEFeedbackError*)pPkg;

	CGateSrv* pGateSrv = FindGateSrv( pSrvMsg->lNotiPlayerID.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR("CDealGateSrvPkg::OnFeedbackError 找不到通知的GateSrv\n");
		return false;
	}
	
	EGFeedbackError errMsg;
	errMsg.errorCode = pSrvMsg->nErrorCode;
	errMsg.lNotiPlayerID = pSrvMsg->lNotiPlayerID;
	MsgToPut* pNewMsg = CreateSrvPkg( EGFeedbackError, pGateSrv, errMsg );
	pGateSrv->SendPkgToSrv( pNewMsg );
	return true;
}

bool CDealGateSrvPkg::OnAddChatGroupMemberByOwner(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnAddChatGroupMemberByOwner pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnAddChatGroupMemberByOwner NULL == pPkg\n");
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GEAddChatGroupMemberByOwner) )
	{
		return false;
	}

	const GEAddChatGroupMemberByOwner* pMsg = ( const GEAddChatGroupMemberByOwner*)pPkg;
	CGateSrv* pGateSrv1 = FindGateSrv( pMsg->reqPlayerID.wGID );
	if( NULL == pGateSrv1 )
	{
		D_ERROR("OnAddChatGroupMemberByOwner 找不到对应的GateSrv\n");
		return false;
	}

	EGAddChatGroupMemberByOwner resp;
	StructMemSet( resp, 0x00, sizeof(resp) );
	resp.chatGroupID = pMsg->chatGroupID;
	resp.lNotiPlayerID = pMsg->reqPlayerID;
	SafeStrCpy(resp.tgtPlayerName, pMsg->targetPlayerName);

	//是否目标玩家在线?
	PlayerGateID* targetPlayerID = FindOnlinePlayerGateIDByNickName( pMsg->targetPlayerName );
	if ( NULL == targetPlayerID )
	{
		resp.exitstTargetName = false;
	}
	else
	{
		resp.exitstTargetName = true;
		resp.targetPlayerID = targetPlayerID->playerID;
	}

	MsgToPut* pNewMsg = CreateSrvPkg( EGAddChatGroupMemberByOwner, pGateSrv1, resp );
	pGateSrv1->SendPkgToSrv( pNewMsg );	

	return true;
	TRY_END;
	return false;
}


bool CDealGateSrvPkg::OnIrcQueryPlayerInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnIrcQueryPlayerInfo pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnIrcQueryPlayerInfo NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof( GEIrcQueryPlayerInfo ) )
	{
		return false;
	}
	
	GEIrcQueryPlayerInfo* pSrvMsg = (GEIrcQueryPlayerInfo*)pPkg;

	PlayerGateID* pGateID = FindOnlinePlayerGateIDByNickName( pSrvMsg->targetPlayerName );
	if( NULL == pGateID )
	{
		EGIrcQueryPlayerInfoRst errMsg;
		errMsg.errorCode = GCIrcQueryPlayerInfoRst::NOT_FOUND_QUERY_PLAYER;
		errMsg.reqID = pSrvMsg->reqID;
		MsgToPut* pNewMsg = CreateSrvPkg(  EGIrcQueryPlayerInfoRst, pOwner, errMsg );
		pOwner->SendPkgToSrv( pNewMsg );
		return false;
	}

	EGIrcQueryPlayerInfo srvmsg;
	srvmsg.queryID = pGateID->playerID;
	srvmsg.reqID = pSrvMsg->reqID;
	CGateSrv* pGateSrv = FindGateSrv( srvmsg.queryID.wGID );
	if( pGateSrv )
	{
		MsgToPut* pNewMsg = CreateSrvPkg( EGIrcQueryPlayerInfo, pGateSrv, srvmsg );
		pGateSrv->SendPkgToSrv( pNewMsg );
	}
	return true;
}


bool CDealGateSrvPkg::OnIrcQueryPlayerInfoRst( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnIrcQueryPlayerInfoRst pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnIrcQueryPlayerInfoRst NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof( GEIrcQueryPlayerInfoRst ) )
	{
		return false;
	}
	
	GEIrcQueryPlayerInfoRst* pSrvMsg = (GEIrcQueryPlayerInfoRst*)pPkg;

	EGIrcQueryPlayerInfoRst srvmsg;
	srvmsg.reqID = pSrvMsg->reqID;
	srvmsg.errorCode = pSrvMsg->errorCode;
	srvmsg.mapID = pSrvMsg->mapID;
	srvmsg.playerClass = pSrvMsg->playerClass;
	srvmsg.playerLevel = pSrvMsg->playerLevel;
	SafeStrCpy( srvmsg.playerName, pSrvMsg->playerName );
	srvmsg.playerSex = pSrvMsg->playerSex;
	SafeStrCpy( srvmsg.FereName, pSrvMsg->FereName );
	SafeStrCpy( srvmsg.GuildName, pSrvMsg->GuildName );
	CGateSrv* pGateSrv = FindGateSrv( srvmsg.reqID.wGID );
	if( pGateSrv )
	{
		MsgToPut* pNewMsg = CreateSrvPkg( EGIrcQueryPlayerInfoRst, pGateSrv, srvmsg );
		pGateSrv->SendPkgToSrv( pNewMsg );
	}
	return true;
}


bool CDealGateSrvPkg::OnCheckPunishment( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnCheckPunishment pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnCheckPunishment NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof( GECheckPunishment ) )
	{
		return false;
	}
	
	GECheckPunishment* pSrvMsg = (GECheckPunishment*)pPkg; ACE_UNUSED_ARG( pSrvMsg );

	//现在随即取9个人填充下...,以后要修改!!!
	EGCheckPunishmentResult gateMsg;
	StructMemSet( gateMsg, 0, sizeof(EGCheckPunishmentResult) );
	
	int count = 0;
	for( map< string, string >::iterator iter = m_mapNameAccount.begin();  iter != m_mapNameAccount.end(); iter++,count++ )
	{
		if ( count >= ARRAY_SIZE(gateMsg.humanRace.player) )
		{
			D_ERROR( "CDealGateSrvPkg::OnCheckPunishment, count(%d) >= ARRAY_SIZE(gateMsg.humanRace.player)\n", count );
			break;
		}
		SafeStrCpy( gateMsg.humanRace.player[count].name, iter->first.c_str() );
		gateMsg.humanRace.player[count].punishTime = RandNumber( 0,59 );
		if( count >= 3 )
		{
			break;
		}
	}
	gateMsg.humanRace.playerNum = count;

	MsgToPut* pNewMsg = CreateSrvPkg( EGCheckPunishmentResult, pOwner, gateMsg );
	pOwner->SendPkgToSrv( pNewMsg );
	return true;
}


bool CDealGateSrvPkg::OnGmCmdRequest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnGmCmdRequest pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnGmCmdRequest NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof( GEGmCmdRequest ) )
	{
		return false;
	}

	GEGmCmdRequest* pSrvMsg = (GEGmCmdRequest*)pPkg;

	PlayerGateID* pGateID = FindOnlinePlayerGateIDByNickName( pSrvMsg->targetName );
	if( NULL == pGateID )
	{
		//找不到该角色的玩家,返回给GM命令错误消息使用者
		EGGmCmdResult gateMsg;
		gateMsg.requestPlayerID = pSrvMsg->requestPlayerID;
		gateMsg.resultCode = 0;		//0表示失败
		CGateSrv* pGateSrv = FindGateSrv( pSrvMsg->requestPlayerID.wGID );
		if( pGateSrv )
		{
			MsgToPut* pNewMsg = CreateSrvPkg( EGGmCmdResult, pGateSrv, gateMsg );
			pGateSrv->SendPkgToSrv( pNewMsg );
		}
		return false;
	}

	//当玩家在线时转发玩家所在的mapsrv
	EGGmCmdRequest gateMsg;
	gateMsg.targetPlayerID = pGateID->playerID;
	gateMsg.requestPlayerID = pSrvMsg->requestPlayerID;
	SafeStrCpy( gateMsg.strChat, pSrvMsg->strChat );
	CGateSrv* pGateSrv = FindGateSrv( pGateID->playerID.wGID );
	if( pGateSrv )
	{
		MsgToPut* pNewMsg = CreateSrvPkg( EGGmCmdRequest, pGateSrv, gateMsg );
		pGateSrv->SendPkgToSrv( pNewMsg );
	}
	return true;
}


//bool CDealGateSrvPkg::OnRaceGroupNotify( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
//{
//	if( NULL == pOwner )
//	{
//		D_ERROR("CDealGateSrvPkg::OnRaceGroupNotify pOwner=NULL\n");
//		return false;
//	}
//
//	if ( NULL == pPkg )
//	{
//		D_ERROR("CDealGateSrvPkg::OnRaceGroupNotify NULL == pPkg\n");
//		return false;
//	}
//
//	if ( wPkgLen != sizeof( GERaceGroupNotify ) )
//	{
//		return false;
//	}
//
//	GERaceGroupNotify* pSrvMsg = (GERaceGroupNotify*)pPkg;
//
//	EGRaceGroupNotify gateMsg;
//	SafeStrCpy( gateMsg.chatContent, pSrvMsg->chatContent );
//	SafeStrCpy( gateMsg.chatPlayerName,  pSrvMsg->chatPlayerName );
//	gateMsg.chatType = pSrvMsg->chatType;
//	gateMsg.notifyRace = pSrvMsg->notifyRace;
//
//	MsgToPut* pBrocastMsg = CreateCenterBrocastPkg( EGRaceGroupNotify, gateMsg );//全gate广播消息；
//	pOwner->SendPkgToSrv( pBrocastMsg );//随便选一个连接发送消息，因为到了底层会解析广播包发送给所有的连入连接，而不是pOwner;
//
//	return true;
//}

bool  CDealGateSrvPkg::OnNoticePlayerHaveUnReadMail(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnNoticePlayerHaveUnReadMail pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnNoticePlayerHaveUnReadMail NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GENoticePlayerHaveUnReadMail) )
	{
		return false;
	}

	GENoticePlayerHaveUnReadMail* pSrvMsg  = ( GENoticePlayerHaveUnReadMail *)pPkg;

	//根据角色名称判断是否上线了
	PlayerGateID* pGateID = FindOnlinePlayerGateIDByNickName( pSrvMsg->recvPlayerName );
	if( NULL != pGateID )
	{
		CGateSrv* pGateSrv = FindGateSrv( pGateID->playerID.wGID );
		if( pGateSrv )
		{
			EGNoticePlayerHaveUnReadMail noticemsg;
			noticemsg.recvPlayerID = pGateID->playerID;
			noticemsg.unReadMailCount = pSrvMsg->unreadmailCount;

			MsgToPut* pNewMsg = CreateSrvPkg( EGNoticePlayerHaveUnReadMail, pGateSrv, noticemsg );
			pGateSrv->SendPkgToSrv( pNewMsg );
			return true;
		}
	}
	return false;
}


bool CDealGateSrvPkg::OnNotifyActivityState( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnNotifyActivityState pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnNotifyActivityState NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GENotifyActivityState) )
	{
		return false;
	}

	GENotifyActivityState* pSrvMsg  = ( GENotifyActivityState *)pPkg;

	EGNotifyActivityState gateMsg;
	StructMemSet( gateMsg, 0, sizeof( EGNotifyActivityState ) );
	gateMsg.activityState = pSrvMsg->activityState;
	gateMsg.activityType = pSrvMsg->activityType;

	MsgToPut* pBrocastMsg = CreateCenterBrocastPkg( EGNotifyActivityState, gateMsg );//全gate广播消息；
	pOwner->SendPkgToSrv( pBrocastMsg );//随便选一个连接发送消息，因为到了底层会解析广播包发送给所有的连入连接，而不是pOwner;
	
	return true;
}

bool CDealGateSrvPkg::OnGMQueryTargetCheck( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnGMQueryTargetCheck pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnGMQueryTargetCheck NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEGMQueryTargetCheck) )
	{
		return false;
	}

	const GEGMQueryTargetCheck* pMsg = ( const GEGMQueryTargetCheck*)pPkg;
	
	EGGMQueryTargetCheckRst checkRst;
	StructMemSet( checkRst, 0x00, sizeof(checkRst) );
	checkRst.gmPlayerId = pMsg->gmPlayerId;
	//checkRst.resp.uRoleID = pMsg->gmReq.xKey.unKey.xRoleKey.uRoleID;
	checkRst.resp.uRoleID = pMsg->gmReq.uRoleID;
	
	PlayerGateID* targetPlayerID = FindOnlinePlayerGateIDByNickName( pMsg->gmReq.szRoleName);
	if ( NULL != targetPlayerID )
	{
		CGateSrv* pTargetGateSrv = FindGateSrv(  targetPlayerID->playerID.wGID );
		if( NULL != pTargetGateSrv )
		{
			checkRst.resp.uOnline = true;
			checkRst.resp.uPlayerID = targetPlayerID->playerID;
		}
	}

	MsgToPut* pNewMsg = CreateSrvPkg( EGGMQueryTargetCheckRst, pOwner, checkRst );
	pOwner->SendPkgToSrv( pNewMsg );
	
	return true;
	TRY_END;
	return false;
}

///Register( G_E_PLAYER_TRY_COPY_QUEST, &OnPlayerTryCopyQuest );//玩家试图进副本；
bool CDealGateSrvPkg::OnPlayerTryCopyQuest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnPlayerTryCopyQuest pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnPlayerTryCopyQuest NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEPlayerTryCopyQuest) )
	{
		return false;
	}

	const GEPlayerTryCopyQuest* pMsg = ( const GEPlayerTryCopyQuest*)pPkg;

	D_DEBUG( "CDealGateSrvPkg::OnPlayerTryCopyQuest, player %s, mapid %d\n", pMsg->enterCopyReq.szAccount, pMsg->enterCopyReq.copyMapID );

	CCopyManager::OnEnterCopyQuest( pMsg->enterCopyReq );

	return true;
	TRY_END;
	return false;
}

///Register( G_E_PLAYER_LEAVE_COPY_QUEST, &OnPlayerLeaveCopyQuest );//玩家离开副本请求；
bool CDealGateSrvPkg::OnPlayerLeaveCopyQuest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnPlayerLeaveCopyQuest pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnPlayerLeaveCopyQuest NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEPlayerLeaveCopyQuest) )
	{
		return false;
	}

	const GEPlayerLeaveCopyQuest* pMsg = ( const GEPlayerLeaveCopyQuest*)pPkg;

	D_DEBUG( "CDealGateSrvPkg::OnPlayerLeaveCopyQuest, player %s, copyid %d, mapid %d\n"
		, pMsg->leaveCopyInfo.enterReq.szAccount, pMsg->leaveCopyInfo.copyID, pMsg->leaveCopyInfo.enterReq.copyMapID );

	CCopyManager::OnLeaveCopyQuest( pMsg->leaveCopyPlayerID
		, pMsg->leaveCopyInfo.enterReq.szAccount, pMsg->leaveCopyInfo.copyID, pMsg->leaveCopyInfo.enterReq.copyMapID );

	return true;
	TRY_END;
	return false;
}

///Register( G_E_COPY_NOMORE_PLAYER, &OnCopyNomorePlayer );//mapsrv发来，不允许更多玩家进入指令(多半是准备待删)
bool CDealGateSrvPkg::OnCopyNomorePlayer( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnCopyNomorePlayer pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnCopyNomorePlayer NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GECopyNomorePlayer) )
	{
		return false;
	}

	const GECopyNomorePlayer* pMsg = ( const GECopyNomorePlayer*)pPkg;

	D_DEBUG( "CDealGateSrvPkg::OnCopyNomorePlayer, copyid %d\n", pMsg->copyid );

	CCopyManager::OnCopyNomorePlayer( pMsg->copyid );

	return true;
	TRY_END;
	return false;
}

///Register( G_E_DEL_COPY_QUEST, &OnDelCopyQuest );//mapsrv发来，请求删副本；
bool CDealGateSrvPkg::OnDelCopyQuest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnDelCopyQuest pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnDelCopyQuest NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEDelCopyQuest) )
	{
		return false;
	}

	const GEDelCopyQuest* pMsg = ( const GEDelCopyQuest*)pPkg;

	D_DEBUG( "CDealGateSrvPkg::OnDelCopyQuest, copyid %d\n", pMsg->copyid );

	CCopyManager::OnDelCopyQuest( pMsg->copyid );

	return true;
	TRY_END;
	return false;
}

//Register( G_E_QUERY_GLOBE_COPYINFOS, &OnQueryGlobeCopyInfos );//gate发来，查询全局副本信息；
bool CDealGateSrvPkg::OnQueryGlobeCopyInfos( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnQueryGlobeCopyInfos pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnQueryGlobeCopyInfos NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEQueryGlobeCopyInfos) )
	{
		return false;
	}

	const GEQueryGlobeCopyInfos* pMsg = ( const GEQueryGlobeCopyInfos*)pPkg;

	D_DEBUG( "CDealGateSrvPkg::OnQueryGlobeCopyInfos, playerid(%d,%d)\n", pMsg->notiPlayerID.wGID, pMsg->notiPlayerID.dwPID );

	CCopyManager::SendGlobeCopyInfoToPlayer( pMsg->notiPlayerID );

	return true;
	TRY_END;
	return false;
}

//Register( G_E_QUERY_TEAM_COPYINFOS, &OnQueryTeamCopyInfos );//gate发来，查询组队副本信息；
bool CDealGateSrvPkg::OnQueryTeamCopyInfos( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnQueryTeamCopyInfos pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnQueryTeamCopyInfos NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEQueryTeamCopyInfos) )
	{
		return false;
	}

	const GEQueryTeamCopyInfos* pMsg = ( const GEQueryTeamCopyInfos*)pPkg;

	D_DEBUG( "CDealGateSrvPkg::OnQueryGlobeCopyInfos, playerid(%d,%d), steamid:%d\n", pMsg->notiPlayerID.wGID, pMsg->notiPlayerID.dwPID, pMsg->teamID );

	CCopyManager::SendTeamCopyInfoToPlayer( pMsg->notiPlayerID, pMsg->teamID );

	return true;
	TRY_END;
	return false;
}

//Register( G_E_STEAM_DESTORY_CENTERNOTI, &OnSTeamDestoryCenterNoti );//gate通知center，副本组队解散；
bool CDealGateSrvPkg::OnSTeamDestoryCenterNoti( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnSTeamDestoryCenterNoti pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnSTeamDestoryCenterNoti NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GESTeamDestoryCenterNoti) )
	{
		return false;
	}

	const GESTeamDestoryCenterNoti* pMsg = ( const GESTeamDestoryCenterNoti*)pPkg;

	D_DEBUG( "CDealGateSrvPkg::OnSTeamDestoryCenterNoti, steamid %d\n", pMsg->steamID );

	CCopyManager::OnSTeamDestoryed( pMsg->steamID );

	return true;
	TRY_END;
	return false;
}

//Register( G_E_LEAVE_STEAM_CENTERNOTI, &OnLeaveSTeamCenterNoti );//gate通知center,玩家离开副本组队；
bool CDealGateSrvPkg::OnLeaveSTeamCenterNoti( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnLeaveSTeamCenterNoti pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnLeaveSTeamCenterNoti NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GELeaveSTeamCenterNoti) )
	{
		return false;
	}

	const GELeaveSTeamCenterNoti* pMsg = ( const GELeaveSTeamCenterNoti*)pPkg;

	if ( strlen( pMsg->szNickName ) >= ARRAY_SIZE( pMsg->szNickName ) )
	{
		D_ERROR( "CDealGateSrvPkg::OnLeaveSTeamCenterNoti, 错误的szNickName, playerID(%d,%d)\n"
			, pMsg->leavePlayerID.wGID, pMsg->leavePlayerID.dwPID );
		return false;
	}

	D_DEBUG( "CDealGateSrvPkg::OnLeaveSTeamCenterNoti, player%s, playerID(%d,%d), steamid %d\n"
		, pMsg->szNickName, pMsg->leavePlayerID.wGID, pMsg->leavePlayerID.dwPID, pMsg->steamID );

	PlayerGateID* playerGateID = FindOnlinePlayerGateIDByNickName( pMsg->szNickName );
	if ( NULL == playerGateID )
	{
		D_WARNING( "CDealGateSrvPkg::OnLeaveSTeamCenterNoti，找不到对应的PlayerGateID,玩家%s可能已下线\n", pMsg->szNickName );
		return false;
	}

	if ( pMsg->leavePlayerID != playerGateID->playerID )
	{
		D_ERROR( "CDealGateSrvPkg::OnLeaveSTeamCenterNoti，playerID != playerGateID->playerID，玩家%s\n", pMsg->szNickName );
		return false;
	}

	CCopyManager::OnPlayerLeaveSTeam( pMsg->szNickName, pMsg->leavePlayerID, *playerGateID );

	return true;
	TRY_END;
	return false;
}

//Register( G_E_RESET_COPY, &OnResetCopy );//玩家请求重置副本；
bool CDealGateSrvPkg::OnResetCopy( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnResetCopy pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnResetCopy NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEResetCopy) )
	{
		return false;
	}

	const GEResetCopy* pMsg = ( const GEResetCopy*)pPkg;

	CCopyManager::OnResetCopy( pMsg->captainPlayerID, pMsg->steamID, pMsg->copyMapID );

	return true;
	TRY_END;
	return false;
}

//Register( G_E_COPYMAPSRV_NOTI, &OnCopyMapsrvNoti );//gate发来，新的copymapsrvID号；
bool CDealGateSrvPkg::OnCopyMapsrvNoti( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnCopyMapsrvNoti pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnCopyMapsrvNoti NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GECopyMapsrvNoti) )
	{
		return false;
	}

	const GECopyMapsrvNoti* pMsg = ( const GECopyMapsrvNoti*)pPkg;

	D_DEBUG( "CDealGateSrvPkg::OnCopyMapsrvNoti,new copymapsrv's id %d\n", pMsg->copyMapsrvID );

	CCopyManager::OnNewCopyMapsrvID( pMsg->copyMapsrvID );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnRecvPWBaseInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPWBaseInfo pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPWBaseInfo NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEPWBaseInfo) )
	{
		return false;
	}

	const GEPWBaseInfo* pMsg = ( const GEPWBaseInfo*)pPkg;
	if ( pMsg->count >= ARRAY_SIZE( pMsg->psList ) )
	{
		D_ERROR( "CDealGateSrvPkg::OnRecvPWBaseInfo, pMsg->count(%d) >= ARRAY_SIZE( pMsg->psList )\n", pMsg->count );
		return false;
	}
	CPseudowireManagerSingle::instance()->OnRecvPWItem(pMsg);//(PseudowireArg_i *)&pMsg->psList[0], pMsg->count);

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnRecvPWMapBaseInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPWMapBaseInfo pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnRecvPWMapBaseInfo NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEPWMapBaseInfo) )
	{
		return false;
	}

	const GEPWMapBaseInfo* pMsg = ( const GEPWMapBaseInfo*)pPkg;
	if ( pMsg->count >= ARRAY_SIZE( pMsg->psList ) )
	{
		D_ERROR( "CDealGateSrvPkg::OnRecvPWMapBaseInfo, pMsg->count(%d) >= ARRAY_SIZE( pMsg->psList )\n", pMsg->count );
		return false;
	}
	CPseudowireManagerSingle::instance()->OnRecvPWMapItem( pMsg );//(PseudowireMapArg_i *)&pMsg->psList[0], pMsg->count);

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnEnterPWMapCheck( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnEnterPWMapCheck pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnEnterPWMapCheck NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEEnterPWMapCheck) )
	{
		return false;
	}

	const GEEnterPWMapCheck* pMsg = ( const GEEnterPWMapCheck*)pPkg;
	
	EGEnterPWMapCheckRst rst;
	rst.ret = CPseudowireManagerSingle::instance()->OnEnterPWMapCheck(pOwner, pMsg->pwid, pMsg->pwid, pMsg->lNoticePlayerID, pMsg->isVip);
	rst.lNoticePlayerID = pMsg->lNoticePlayerID;
	rst.isShift = pMsg->isShift;

	MsgToPut* pNewMsg = CreateSrvPkg( EGEnterPWMapCheckRst, pOwner, rst );
	pOwner->SendPkgToSrv( pNewMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnEnterPWMapErr( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnEnterPWMapErr pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnEnterPWMapErr NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEEnterPWMapErr) )
	{
		return false;
	}

	const GEEnterPWMapErr* pMsg = ( const GEEnterPWMapErr*)pPkg;

	CPseudowireManagerSingle::instance()->OnEnterPWMapErr(pOwner, pMsg->pwid, pMsg->pwid, pMsg->lNoticePlayerID);

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnLeavePWMapNtf( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnLeavePWMapNtf pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnLeavePWMapNtf NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GELeavePWMapNtf) )
	{
		return false;
	}

	const GELeavePWMapNtf* pMsg = ( const GELeavePWMapNtf*)pPkg;

	CPseudowireManagerSingle::instance()->OnLeavePWMapNtf(pOwner, pMsg->pwid, pMsg->pwid, pMsg->lNoticePlayerID);

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnModifyPWStateReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnModifyPWStateReq pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnModifyPWStateReq NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEModifyPWStateReq) )
	{
		return false;
	}

	const GEModifyPWStateReq* pMsg = ( const GEModifyPWStateReq*)pPkg;

	CPseudowireManagerSingle::instance()->OnModifyPWState(pOwner, pMsg->pwid, (PseudowireArg_i::ePWState)pMsg->newState);

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnModifyPWMapStateReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealGateSrvPkg::OnModifyPWMapStateReq pOwner=NULL\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealGateSrvPkg::OnModifyPWMapStateReq NULL == pPkg\n");
		return false;
	}

	if ( wPkgLen != sizeof(GEModifyPWMapStateReq) )
	{
		return false;
	}

	const GEModifyPWMapStateReq* pMsg = ( const GEModifyPWMapStateReq*)pPkg;

	CPseudowireManagerSingle::instance()->OnModifyPWMapState(pOwner, pMsg->pwid, pMsg->pwMapid, (PseudowireMapArg_i::ePWMapState)pMsg->newState);

	return true;
	TRY_END;
	return false;
}

