﻿/**
* @file otherserver.cpp
* @brief 定义服务器端的其它服务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: otherserver.h
* 摘    要: 定义服务器端的其它服务
* 作    者: dzj
* 完成日期: 2007.12.10
*
*/

#include "otherserver.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h" //读服务配置；
#include "../DealPkg/DealPkg.h"

map<unsigned short, CGateSrv*> CManGateSrvs::m_mapGateSrvs;//所有gatesrv, pair(byID,CGateSrv*);

///置玩家的句柄信息，每次新建时必须调用;
void CSrvConn::SetHandleInfo( int nHandleID, int nSessionID )
{
	TRY_BEGIN;

	D_WARNING( "连接%d建立\n", nSessionID );

	//添加新的gatesrv;
	//目前认为连过来的全部都是GateSrv;
	//临时，具体的序号通信交流过程再议，目前做法：centersrv无法确定各个gatesrv的序号
	//以后如果要与其它SRV交流同语义的gatesrvID号，需要另外确定办法
	//注意绝对不要使用CManGateSrvs去取与其它Srv交流意义上的GateSrv;	
	static unsigned short byID = 0;
	CLoadSrvConfig::GetNumType( "GateSrv", m_bySrvType );
	m_pSrvBase = NEW CGateSrv( byID++, this );
	D_WARNING( "建立对应的gatesrv实例\n" );

	m_nHandleID = nHandleID;
	m_nSessionID = nSessionID;		

	return;
	TRY_END;
	return;
}

///销毁时的处理(连接断开)
///注意：在新建连接之前也会调用这里；
void CSrvConn::OnDestoryed()
{
	TRY_BEGIN;

#ifdef USE_DSIOCP
	if ( NULL != m_pUniqueSocket )
	{
		if ( NULL != g_poolUniDssocket )
		{
				g_poolUniDssocket->Release( m_pUniqueSocket );
				m_pUniqueSocket = NULL;
		}
	}
#endif //USE_DSIOCP

	if ( m_nHandleID != 0 )
	{
		//确实为断开，区别于新建连接时的回调；
		//以下断开处理；
		if ( NULL != m_pSrvBase )
		{
			m_pSrvBase->OnDestory();
			delete m_pSrvBase; m_pSrvBase = NULL;
		}
	}
	ResetInfo();
	return;
	TRY_END;
	return;
}

///构造函数；
CGateSrv::CGateSrv( unsigned short byID, CSrvConn* pConn ) : CSrvBase(pConn)
{
	TRY_BEGIN;
	m_wID = byID;
	m_GateSrvID = 9999;
	CManGateSrvs::AddOneSrv( this );//自身添加到服务管理器；
	TRY_END;
};

CGateSrv::~CGateSrv()
{
	TRY_BEGIN;
	CManGateSrvs::RemoveSrv(this);
	TRY_END;
};

///断开处理（销毁 ）
void CGateSrv::OnDestory()
{
	TRY_BEGIN;
	CDealGateSrvPkg::GateSrvDisconnect( GetGateSrvID() );
	CSrvBase::OnDestory();
	//ResetConn();//汗，重写此虚函数时小心点
	TRY_END;
}

///收包处理；
void CGateSrv::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	TRY_BEGIN;
	CDealGateSrvPkg::DealPkg( this, wCmd, pBuf, wPkgLen );
	TRY_END;
}
