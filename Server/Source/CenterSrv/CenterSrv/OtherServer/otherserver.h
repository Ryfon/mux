﻿/**
* @file otherserver.h
* @brief 定义与本服务通信的服务器端其它服务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: otherserver.h
* 摘    要: 定义与本服务通信的服务器端其它服务
* 作    者: dzj
* 完成日期: 2007.12.10
*
*/

#pragma once

#include "../../../Base/PkgProc/OtherserverBase.h"

///gatesrv类；
class CGateSrv : public CSrvBase
{
public:
	CGateSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CGateSrv();

public:
	///取自身ID,用于CManGateSrvs管理gate；
	unsigned short GetID()
	{
		return m_wID;
	}

	///设置连接对应gatesrv的SELF_SRV_ID号；
	void SetGateSrvID( unsigned short gateSrvID )
	{
		m_GateSrvID = gateSrvID;
	}

	///取连接对应gatesrv的SELF_SRV_ID号；
	unsigned short GetGateSrvID()
	{
		return m_GateSrvID;
	}

public:
	///请求销毁自身(断开连接)
	void ReqDestorySelf()
	{
		TRY_BEGIN;

		m_pConn->ReqDestorySelf();

		return;
		TRY_END;
		return;
	}

public:
	///断开处理（销毁 ）
	virtual void OnDestory();
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

private:
	///自身序号，用于CManGateSrvs管理gate；
	unsigned short m_wID;
	unsigned short m_GateSrvID;//对应gatesrv的SELF_SRV_ID;
};

///gatesrv管理器
class CManGateSrvs
{
private:
	CManGateSrvs();
public:
	///添加新gatesrv;
	static void AddOneSrv( CGateSrv* pGateSrv )
	{
		m_mapGateSrvs.insert( pair<unsigned short, CGateSrv*>( pGateSrv->GetID(), pGateSrv ) );
	}
	///删gatesrv;
	static bool RemoveSrv( CGateSrv* pGateSrv )
	{
		map<unsigned short, CGateSrv*>::iterator tmpIter = m_mapGateSrvs.find( pGateSrv->GetID() );
		if ( tmpIter != m_mapGateSrvs.end() )
		{
			m_mapGateSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}
	/////得到特定的gatesrv;
	//static CGateSrv* GetGateserver( unsigned short byID )
	//{
	//	map<unsigned short, CGateSrv*>::iterator tmpiter=m_mapGateSrvs.find( byID );
	//	if ( tmpiter != m_mapGateSrvs.end() )
	//	{
	//		CGateSrv* pSrv = tmpiter->second;
	//		return pSrv;
	//	} else {
	//		return NULL;
	//	}
	//}

private:
	///本管理器管理的所有gatesrv;
	static map<unsigned short, CGateSrv*> m_mapGateSrvs;//所有gatesrv, pair(byID,CGateSrv*);
};
