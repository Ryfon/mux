﻿#include "PseudowireManager.h"
#include "../../Base/Utility.h"
#include "OtherServer/otherserver.h"


CPseudowireMapInfo::CPseudowireMapInfo(void)
{
	StructMemSet( m_data, 0x00, sizeof(m_data) );
}

CPseudowireMapInfo::CPseudowireMapInfo(PseudowireMapArg_i &dt)
{
	StructMemSet( m_data, 0x00, sizeof(m_data) );
	m_data.pwMapArg = dt;
}

CPseudowireMapInfo::~CPseudowireMapInfo(void)
{

}

void CPseudowireMapInfo::OnEnterPWMapCheckSuccess(PlayerID &playerid)
{
	AddToList(playerid);
	return;
}

void CPseudowireMapInfo::OnEnterPWMapErr(PlayerID &playerid)
{
	DelFromList(playerid);
	return;
}

inline void CPseudowireMapInfo::OnLeavePWMapNtf(PlayerID &playerid)
{
	DelFromList(playerid);
	return;
}

void CPseudowireMapInfo::SetPWMapArg(PseudowireMapArg_i &dt)
{
	m_data.pwMapArg = dt;

	return;
}

inline PseudowireMapData& CPseudowireMapInfo::GetData(void)
{
	return m_data;
}

inline PseudowireMapArg_i& CPseudowireMapInfo::GetPWMapArg(void)
{
	return m_data.pwMapArg;
}

inline PseudowireMapArg_i::ePWMapState CPseudowireMapInfo::GetPWMapState(void)
{
	return m_data.pwMapArg.state;
}

inline unsigned short CPseudowireMapInfo::GetPWMapNewCondition(void)
{
	return m_data.pwMapArg.newCondition;
}

unsigned short CPseudowireMapInfo::GetPWMapPoolLimit(void)
{
	return m_data.pwMapArg.poolLimit;
}

unsigned short CPseudowireMapInfo::GetPWMapVipLimit(void)
{
	return m_data.pwMapArg.vipLimit;
}

unsigned short CPseudowireMapInfo::GetTotalSize(void)
{
	return (unsigned short)m_playerList.size();
}

bool CPseudowireMapInfo::HasArchieveLimit(bool isVipPlayer)
{
	unsigned short totalNum = GetTotalSize();
	if(!isVipPlayer)
	{
		if(totalNum < m_data.pwMapArg.poolLimit)
			return true;
	}
	else
	{
		if(totalNum < m_data.pwMapArg.vipLimit)
			return true;
	}

	return false;
}

bool CPseudowireMapInfo::HasArchieveNewCondition(void)
{
	return (m_data.currenPlayerNum >= m_data.pwMapArg.newCondition) ? true : false;
}

void CPseudowireMapInfo::AddToList(PlayerID &playerid)
{
	m_playerList.push_back(playerid);
	m_data.currenPlayerNum = (unsigned short)m_playerList.size();

	return;
}

void CPseudowireMapInfo::DelFromList(PlayerID &playerid)
{
	for(std::vector<PlayerID>::iterator iter = m_playerList.begin(); m_playerList.end() != iter; ++iter)
	{
		if(*iter == playerid)
		{
			m_playerList.erase(iter);
			m_data.currenPlayerNum = (unsigned short)m_playerList.size();

			break;
		}
	}

	return ;
}

CPseudowireManager::CPseudowireManager(void)
{

}

CPseudowireManager::~CPseudowireManager(void)
{
	Clear();
}

void CPseudowireManager::Clear()
{
	for(std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.begin(); m_pwInfo.end() != iter; ++iter)
	{
		delete iter->second;
		iter->second = NULL;
	}
	m_pwInfo.clear();

	for(std::map<unsigned int, CPseudowireMapInfo*>::iterator iter = m_pwMapInfos.begin(); m_pwMapInfos.end() != iter; ++iter)
	{
		delete iter->second;
		iter->second = NULL;
	}
	m_pwMapInfos.clear();

	return;
}

int CPseudowireManager::OnEnterPWMapCheck(CGateSrv* pOwner, unsigned short pwid, unsigned short pwMapid, PlayerID playerid, bool isVipPlayer)
{
	GCEnterPWErr::eEnterPWMapErr retCode = GCEnterPWErr::UNKNOWN_ERR;//未知错误
	do 
	{
		//对应线是否为空
		PseudowireData* pPwInfo = GetPWItem(pwid);
		if(NULL == pPwInfo)
		{
			retCode = GCEnterPWErr::NO_PW_INFO;
			break;
		}

		//对应线是否没有打开
		if(pPwInfo->psArg.state != PseudowireArg_i::PM_OPEN)
		{
			retCode = GCEnterPWErr::PW_CANNOT_ENTER;
			break;
		}

		//对应地图是否为空
		CPseudowireMapInfo* pMapInfo = GetPWMapItem(pwid, pwMapid);
		if(NULL == pMapInfo)
		{
			retCode = GCEnterPWErr::NO_PW_MAP_INFO;
			break;
		}

		//对应地图是否未激活
		if(pMapInfo->GetPWMapState() != PseudowireMapArg_i::PWM_ACTIVE)
		{
			retCode = GCEnterPWErr::PW_MAP_CANNOT_ENTER;
			break;
		}

		//对应地图是否大达到人数上限
		if(!pMapInfo->HasArchieveLimit(isVipPlayer))
		{
			retCode = isVipPlayer ? GCEnterPWErr::VIP_LIMIT : GCEnterPWErr::POOL_LIMIT;
			break;
		}

		retCode = GCEnterPWErr::OK;

		//玩家进入检查通过
		pMapInfo->OnEnterPWMapCheckSuccess(playerid);
		
		//通知人数变化
		NotifyPWMapPlayerChange(pOwner, pwid, pwMapid, pMapInfo->GetTotalSize());

		//满足激活条件，尝试激活新的地图
		if(pMapInfo->HasArchieveNewCondition())
			TryToActivateNewPWMap(pOwner, pwMapid);

	} while(false);
	
	return (int)retCode;
}

void CPseudowireManager::OnEnterPWMapErr(CGateSrv* pOwner, unsigned short pwid, unsigned short pwMapid, PlayerID playerid)
{
	CPseudowireMapInfo* pMapInfo = GetPWMapItem(pwid, pwMapid);
	if(NULL == pMapInfo)
	{
		D_ERROR("没有找到伪线地图(pwid = %d, pwmapid= %d)信息\n", pwid, pwMapid);
		return;
	}

	pMapInfo->OnEnterPWMapErr(playerid);

	//通知人数变化
	NotifyPWMapPlayerChange(pOwner, pwid, pwMapid, pMapInfo->GetTotalSize());
	
	return;
}

void CPseudowireManager::OnLeavePWMapNtf(CGateSrv* pOwner, unsigned short pwid, unsigned short pwMapid, PlayerID playerid)
{
	CPseudowireMapInfo* pMapInfo = GetPWMapItem(pwid, pwMapid);
	if(NULL == pMapInfo)
	{
		D_ERROR("没有找到伪线地图(pwid = %d, pwmapid= %d)信息\n", pwid, pwMapid);
		return;
	}

	pMapInfo->OnLeavePWMapNtf(playerid);

	//通知人数变化
	NotifyPWMapPlayerChange(pOwner, pwid, pwMapid, pMapInfo->GetTotalSize());

	return;
}

void CPseudowireManager::OnRecvPWItem( const GEPWBaseInfo* pMsg )//PseudowireArg_i pItems[], unsigned char count)
{
	if ( NULL == pMsg )
	{
		D_ERROR( "CPseudowireManager::OnRecvPWItem, NULL == pMsg\n" );
		return;
	}
	for ( unsigned int i=0; i<pMsg->count; ++i )
	{
		if ( i>=ARRAY_SIZE(pMsg->psList) )
		{
			D_ERROR( "CPseudowireManager::OnRecvPWItem, i(%d)>=ARRAY_SIZE(pMsg->psList)\n", i );
			return;
		}
		AddPWItem((PseudowireArg_i*)(&(pMsg->psList[i])));
	}
	//if((NULL == pItems) || (0 == count))
	//	return;

	//for(unsigned char i = 0; i < count; i++)
	//{
	//	AddPWItem(&pItems[i]);
	//}

	return;
}

void CPseudowireManager::OnRecvPWMapItem( const GEPWMapBaseInfo* pMsg )//PseudowireMapArg_i pItems[], unsigned char count)
{
	if ( NULL == pMsg )
	{
		D_ERROR( "CPseudowireManager::OnRecvPWMapItem, NULL == pMsg\n" );
		return;
	}
	for ( unsigned int i=0; i<pMsg->count; ++i )
	{
		if ( i>=ARRAY_SIZE(pMsg->psList) )
		{
			D_ERROR( "CPseudowireManager::OnRecvPWMapItem, i(%d)>=ARRAY_SIZE(pMsg->psList)\n", i );
			return;
		}
		AddPWMapItem((PseudowireMapArg_i*)(&(pMsg->psList[i])));
	}
	//if((NULL == pItems) || (0 == count))
	//	return;

	//for(unsigned char i = 0; i < count; i++)
	//{
	//	AddPWMapItem(&pItems[i]);
	//}

	return;
}

void CPseudowireManager::OnModifyPWState(CGateSrv* pOwner, unsigned short idx, PseudowireArg_i::ePWState newState)
{
	if(NULL == pOwner)
		return;

	std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.find(idx);
	if(m_pwInfo.end() == iter)
		return;

	if(!IsValidPWChange(iter->second->psArg.state, newState))
		return;

	//修改线下所有地图状态
	OnModifyPWMapStateByPWIdx(pOwner, idx, (PseudowireMapArg_i::ePWMapState)newState);

	//修改线本身状态
	iter->second->psArg.state = newState;
	iter->second->lastTimeOfStateChange = time(NULL);

	EGModifyPWStateNtf ntf;
	StructMemSet( ntf, 0x00, sizeof(ntf) );
	ntf.pwid = idx;
	ntf.state = newState;

	MsgToPut* pBrocastMsg = CPacketBuild::CreateSrvBrocastPkg<EGModifyPWStateNtf>( &ntf );
	pOwner->SendPkgToSrv( pBrocastMsg );
	return;
}

void CPseudowireManager::OnModifyAllPWState(CGateSrv* pOwner, PseudowireArg_i::ePWState newState)
{
	for(std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.begin(); m_pwInfo.end() != iter; ++iter)
	{
		OnModifyPWState(pOwner, iter->first, newState);
	}

	return;
}

void CPseudowireManager::OnModifyPWMapState(CGateSrv* pOwner, unsigned short pwIdx, unsigned short pwMapId, PseudowireMapArg_i::ePWMapState newState)
{
	if(NULL == pOwner)
		return;

	CPseudowireMapInfo* pPWMap = GetPWMapItem(pwIdx, pwMapId);
	if(NULL != pPWMap)
	{	
		if(IsValidPWChange((PseudowireArg_i::ePWState)pPWMap->GetPWMapState(), (PseudowireArg_i::ePWState)newState))
		{
			pPWMap->GetPWMapArg().state = newState;
			pPWMap->GetData().lastTimeOfStateChange = (time_t)time(NULL);

			EGModifyPWMapStateNtf ntf;
			StructMemSet( ntf, 0x00, sizeof(ntf) );
			ntf.pwid = pwIdx;
			ntf.pwMapid = pwMapId;
			ntf.state = newState;

			MsgToPut* pBrocastMsg = CPacketBuild::CreateSrvBrocastPkg<EGModifyPWMapStateNtf>( &ntf );
			pOwner->SendPkgToSrv( pBrocastMsg );
		}
	}

	return;
}
void CPseudowireManager::OnModifyPWMapStateByPWIdx(CGateSrv* pOwner, unsigned short pwIdx, PseudowireMapArg_i::ePWMapState newState)
{
	if(NULL == pOwner)
		return;

	for(std::map<unsigned int, CPseudowireMapInfo*>::iterator iter = m_pwMapInfos.begin(); m_pwMapInfos.end() != iter; ++iter)
	{
		unsigned short tempIdx = GET_HIGH_WORD(iter->first);
		if(tempIdx > pwIdx)
			break;

		if(tempIdx == pwIdx)
		{
			if(NULL != iter->second)
			{
				OnModifyPWMapState(pOwner, pwIdx, GET_LOW_WORD(iter->first), newState);
			}
		}
	}

	return;
}

PseudowireData* CPseudowireManager::GetPWItem(unsigned short pwIdx)
{
	std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.find(pwIdx);
	if(m_pwInfo.end() != iter)
		return iter->second;

	return NULL;
}

inline std::map<unsigned short, PseudowireData*>& CPseudowireManager::GetAllPWItems(void) 
{
	return m_pwInfo;
}

CPseudowireMapInfo* CPseudowireManager::GetPWMapItem(unsigned short pwIdx, unsigned short pwMapId)
{
	unsigned int id = GEN_DOUBLE_WORD(pwIdx, pwMapId);
	std::map<unsigned int, CPseudowireMapInfo*>::iterator iter = m_pwMapInfos.find(id);
	if(m_pwMapInfos.end() != iter)
		return iter->second;

	return NULL;
}

size_t CPseudowireManager::GetPWMapItemsByIndex(unsigned short pwIdx, std::vector<CPseudowireMapInfo *> &pwMapItems)
{
	for(std::map<unsigned int, CPseudowireMapInfo*>::iterator iter = m_pwMapInfos.begin(); m_pwMapInfos.end() != iter; ++iter)
	{
		unsigned short tempIndex = GET_HIGH_WORD(iter->first);
		if (tempIndex > pwIdx)
		{
			break;
		}

		if(tempIndex == pwIdx)
		{
			pwMapItems.push_back(iter->second);
		}
	}

	return pwMapItems.size();
}

inline std::map<unsigned int, CPseudowireMapInfo*>& CPseudowireManager::GetAllPWMapItems(void)
{
	return m_pwMapInfos;
}

size_t CPseudowireManager::GetSameMapSet(unsigned short mapid, std::map<unsigned int, CPseudowireMapInfo*>& mapset)
{
	std::map<unsigned int, CPseudowireMapInfo*>::iterator iter;// = mapset.find(iter->first);
	for(std::map<unsigned int, CPseudowireMapInfo*>::iterator iter = m_pwMapInfos.begin(); m_pwMapInfos.end() != iter; ++iter)
	{
		if(GET_LOW_WORD(iter->first) == mapid)
		{
			std::map<unsigned int, CPseudowireMapInfo*>::iterator tmpiter2 = mapset.find(iter->first);
			if ( mapset.end() == tmpiter2 )
			{
				//新插入
				mapset.insert( pair<unsigned int, CPseudowireMapInfo*>(iter->first, iter->second) );
			} else {
				D_ERROR( "CPseudowireManager::GetSameMapSet, 插入mapset时，重复的键值%d\n", iter->first );
			}
		 //   else {
			////	//替换已存在值

			////}
			//mapset[iter->first] = iter->second;
		}
	}

	return mapset.size();
}

void CPseudowireManager::AddPWItem(PseudowireArg_i *pItem)
{
	if ( NULL == pItem )
	{
		D_ERROR( "CPseudowireManager::AddPWItem, NULL == pItem\n" );
		return;
	}

	std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.find(pItem->idx);
	if(m_pwInfo.end() == iter)
	{
		PseudowireData* pObject = NEW PseudowireData;
		if(NULL != pObject)
		{
			pObject->psArg = *pItem;
			pObject->lastTimeOfStateChange = 0;

			m_pwInfo.insert( pair<unsigned short, PseudowireData*>(pItem->idx, pObject) );
			//m_pwInfo[pObject->psArg.idx] = pObject;
		}
	}

	return;
}

void CPseudowireManager::AddPWMapItem(PseudowireMapArg_i *pItem)
{
	if ( NULL == pItem )
	{
		D_ERROR( "CPseudowireManager::AddPWMapItem, NULL == pItem\n" );
		return;
	}

	unsigned int id = GEN_DOUBLE_WORD(pItem->pwid, pItem->mapid);
	std::map<unsigned int, CPseudowireMapInfo*>::iterator iter = m_pwMapInfos.find(id);
	if(m_pwMapInfos.end() == iter)
	{
		CPseudowireMapInfo* pObject = NEW CPseudowireMapInfo(*pItem);
		if ( NULL != pObject )
		{
			m_pwMapInfos.insert( pair<unsigned int, CPseudowireMapInfo*>(id, pObject) );
			//m_pwMapInfos[id] = pObject;
		}
	}

	return;
}

void CPseudowireManager::TryToActivateNewPWMap(CGateSrv* pOwner, unsigned short mapid, bool isAuto)
{
	if(!isAuto)
		return;

	std::map<unsigned int, CPseudowireMapInfo*> mapset;//同一目标地图对应的所有伪线地图的集合
	size_t mapCount = GetSameMapSet(mapid, mapset);
	if(0 == mapCount)
		return;

	unsigned int firstMapToActive = 0;//第一个待激活的地图
	unsigned int needActiveFlag = true;//激活标记
	for(std::map<unsigned int, CPseudowireMapInfo*>::iterator iter = mapset.begin(); mapset.end() != iter; ++iter)
	{
		CPseudowireMapInfo *ptemp = iter->second;
		if(NULL != ptemp)
		{
			PseudowireMapArg_i::ePWMapState mapSate  = ptemp->GetPWMapState();
			if(PseudowireMapArg_i::PWM_UNACTIVE == mapSate)
			{
				if(0 == firstMapToActive)
				{
					PseudowireData* pPwInfo = GetPWItem(GET_HIGH_WORD(iter->first));
					if((NULL == pPwInfo) && (PseudowireArg_i::PM_OPEN == pPwInfo->psArg.state))
					{	
						firstMapToActive = iter->first;
						break;
					}
				}
			}
			else if(PseudowireMapArg_i::PWM_ACTIVE == mapSate)
			{
				if(ptemp->GetTotalSize() < ptemp->GetPWMapNewCondition())
					needActiveFlag = false;
			}
		}
	}

	if(needActiveFlag && (0 != firstMapToActive))
	{
		OnModifyPWMapState(pOwner, (firstMapToActive), GET_LOW_WORD(firstMapToActive), PseudowireMapArg_i::PWM_ACTIVE);
	}

	return;
}

bool CPseudowireManager::IsValidPWChange(PseudowireArg_i::ePWState currentState, PseudowireArg_i::ePWState newState)
{
	///////////////////////////////////
	//　		关闭　开启　禁入	 //
	//		关闭　-     y　	  n		 //
	//		开启  y		-     y      //
	//		禁入  y		n	  -      //
	///////////////////////////////////

	if(currentState != newState)
	{
		if(currentState == PseudowireArg_i::PM_CLOSE)
		{
			if(newState == PseudowireArg_i::PM_OPEN)
				return true;
		}
		else if(currentState == PseudowireArg_i::PM_OPEN)   
		{	
			if((newState == PseudowireArg_i::PM_CLOSE) || (newState == PseudowireArg_i::PM_FORBIT))
				return true;
		}
		else if(currentState == PseudowireArg_i::PM_FORBIT)
		{
			if(newState == PseudowireArg_i::PM_CLOSE)
				return true;
		}
	}

	return false;
}

void CPseudowireManager::NotifyPWMapPlayerChange(CGateSrv* pOwner, unsigned short pwid, unsigned mapid, unsigned short playerNum)
{
	if(NULL == pOwner)
		return;

	EGPWMapPlayerNumNtf ntf;
	ntf.pwCurrentNum = playerNum;
	ntf.pwid = pwid;
	ntf.pwMapid = mapid;

	MsgToPut* pBrocastMsg = CPacketBuild::CreateSrvBrocastPkg<EGPWMapPlayerNumNtf>( &ntf );
	pOwner->SendPkgToSrv( pBrocastMsg );
}
