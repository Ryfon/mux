﻿#ifndef PSEUDO_WIRE_MANAGER_H
#define PSEUDO_WIRE_MANAGER_H


#include "../../Base/aceall.h"
#include <map>
#include <vector>
#include "../../Base/PkgProc/SrvProtocol.h"


using namespace MUX_PROTO;

class CGateSrv;

class CPseudowireMapInfo
{
public:
	CPseudowireMapInfo(void);
	CPseudowireMapInfo(PseudowireMapArg_i &dt);
	
	~CPseudowireMapInfo(void);
public:
	void OnEnterPWMapCheckSuccess(PlayerID &playerid);
	void OnEnterPWMapErr(PlayerID &playerid);
	void OnLeavePWMapNtf(PlayerID &playerid);

public:
	void SetPWMapArg(PseudowireMapArg_i &dt);
	PseudowireMapData& GetData(void);
	PseudowireMapArg_i& GetPWMapArg(void);
	PseudowireMapArg_i::ePWMapState GetPWMapState(void);
	unsigned short GetPWMapNewCondition(void);
	unsigned short GetPWMapPoolLimit(void);
	unsigned short GetPWMapVipLimit(void);
	
	//获取当前地图人数
	unsigned short GetTotalSize(void); 

	//是否达到玩家进入的人数上限
	bool HasArchieveLimit(bool isVipPlayer);

	//是否本地图达到newcondition条件
	bool HasArchieveNewCondition(void);

private:
	void AddToList(PlayerID &playerid);
	void DelFromList(PlayerID &playerid);

private:
	PseudowireMapData m_data;//伪线地图基本信息
	std::vector<PlayerID> m_playerList;//玩家列表
};

class CPseudowireManager
{
	friend class ACE_Singleton<CPseudowireManager, ACE_Null_Mutex>;
public:
	CPseudowireManager(void);
	~CPseudowireManager(void);
public:
	void Clear();

	//进出伪线地图的操作
	int OnEnterPWMapCheck(CGateSrv* pOwner,unsigned short pwid, unsigned short pwMapid, PlayerID playerid, bool isVipPlayer);
	void OnEnterPWMapErr(CGateSrv* pOwner,unsigned short pwid, unsigned short pwMapid, PlayerID playerid);
	void OnLeavePWMapNtf(CGateSrv* pOwner,unsigned short pwid, unsigned short pwMapid, PlayerID playerid);

	///增加操作
	void OnRecvPWItem( const GEPWBaseInfo* pMsg );//PseudowireArg_i pItems[], unsigned char count);
	void OnRecvPWMapItem( const GEPWMapBaseInfo* pMsg );//PseudowireMapArg_i pItems[], unsigned char count);

	///修改状态操作(状态转化矩阵）
	//修改伪线状态
	void OnModifyPWState(CGateSrv* pOwner, unsigned short idx, PseudowireArg_i::ePWState newState);
	void OnModifyAllPWState(CGateSrv* pOwner, PseudowireArg_i::ePWState newState);

	//改变伪线地图状态
	void OnModifyPWMapState(CGateSrv* pOwner, unsigned short pwIdx, unsigned short pwMapId, PseudowireMapArg_i::ePWMapState newState);
	void OnModifyPWMapStateByPWIdx(CGateSrv* pOwner, unsigned short pwIdx, PseudowireMapArg_i::ePWMapState newState);

public:
	///获取信息
	//获取指定伪线信息
	PseudowireData* GetPWItem(unsigned short pwIdx);
	std::map<unsigned short, PseudowireData*> & GetAllPWItems(void); 

	//获取伪线地图信息
	CPseudowireMapInfo* GetPWMapItem(unsigned short pwIdx, unsigned short pwMapId);
	size_t GetPWMapItemsByIndex(unsigned short pwIdx, std::vector<CPseudowireMapInfo *> &pwMapItems);
	std::map<unsigned int, CPseudowireMapInfo*>& GetAllPWMapItems(void);
	size_t GetSameMapSet(unsigned short mapid, std::map<unsigned int, CPseudowireMapInfo*>& mapset);

private:
	void AddPWItem(PseudowireArg_i *pItem);
	void AddPWMapItem(PseudowireMapArg_i *pItem);

	//激活某一地图的新的拷贝(假设已经冰封谷１，冰封谷２已经开放，此时如果条件满足的话激活冰封谷３)
	void TryToActivateNewPWMap(CGateSrv* pOwner, unsigned short mapid, bool isAuto = true);

	//是否是有效转换
	bool IsValidPWChange(PseudowireArg_i::ePWState currentState, PseudowireArg_i::ePWState newState);

	//通知伪线地图变化
	void NotifyPWMapPlayerChange(CGateSrv* pOwner, unsigned short pwid, unsigned mapid, unsigned short playerNum);

private:
	std::map<unsigned short, PseudowireData*> m_pwInfo;//伪线列表
	std::map<unsigned int, CPseudowireMapInfo*> m_pwMapInfos;//伪线地图列表

};

typedef ACE_Singleton<CPseudowireManager, ACE_Null_Mutex> CPseudowireManagerSingle;

#endif/*PSEUDO_WIRE_MANAGER_H*/
