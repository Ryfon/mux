﻿// CenterSrv.cpp : 定义控制台应用程序的入口点。
//

//#include "stdafx.h"
#if defined(_WIN32) || defined(_WIN64)
#define  _SECURE_SCL_THROWS 1
#endif

#include "../../Base/BufQueue/BufQueueImp.h"
#include "../../Base/Utility.h"
#include "../../Base/PkgProc/RcvPkgHandler.h" //收包处理器(CRcvPkgHandler<CPlayer>)；
#include "../../Base/PkgProc/CManCommHandler.h" //所有连接管理器；

#include "../../Base/LoadConfig/LoadSrvConfig.h" //读服务配置；

//网络模块需要;
//#include "../../Base/NetTask.h"
#include "../../Base/NetTaskEx.h"
//#include "../../Base/HandlerT.h"
//#include "../../Base/CacheMessageBlock.h"

#include "OtherServer/otherserver.h" //给其它srv收包处理器提供包处理者；
#include "DealPkg/DealPkg.h" //给其它srv收包处理器提供包处理者；

#include "copymanager/copymanager.h"


//crash模块
#ifdef ACE_WIN32
#include "CrashLib/CrashLib.h"
#endif

#ifdef USE_DSIOCP
#include "iocp/dsiocp.h"
#endif //USE_DSIOCP

#include "../../Test/testthreadque_wait/sigexception/sigexception.h"

#ifdef USE_DSIOCP
TCache< CDsSocket >* g_poolDsSocket = NULL;
TCache< UniqueObj< CDsSocket > >* g_poolUniDssocket = NULL;
TCache< PerIOData >* g_poolPerIOData = NULL;
TCache< DsIOBuf >*   g_poolDsIOBuf = NULL;
#endif //USE_DSIOCP

TCache< MsgToPut >* g_poolMsgToPut = NULL;

extern TCache< PlayerGateID >* g_poolPlayerGateID;

const unsigned char SELF_PROTO_TYPE = 0x03;//自身为GateSrv，定义见srvProtocol.h;
unsigned short SELF_SRV_ID = 0x00;//自身为第几个GateSrv;

#define SRV_HANDLER_TYPE CRcvPkgHandler< CSrvConn > //服务器端通信处理器类型；

///向服务器其它srv发包的队列;
IBufQueue* g_pSrvSender;//向服务器其它srv发包的队列;

void FastTimer()
{
}

void SlowTimer()
{
	CCopyManager::CopyManagerTimerProc();
}

///时钟检测函数，循环执行以确定是否需要执行快时钟或慢时钟；
void TimerCheck()
{
	TRY_BEGIN;
	static ACE_Time_Value fastLastTime = ACE_OS::gettimeofday();
	static ACE_Time_Value slowLastTime = ACE_OS::gettimeofday();
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	ACE_Time_Value fastPastTime = curTime - fastLastTime;
	ACE_Time_Value slowPastTime = curTime - slowLastTime;
	if ( fastPastTime.msec() > 361 ) //36毫秒；
	{
		//激活快时钟；
		fastLastTime = curTime;
		FastTimer();
	} 
	if ( slowLastTime.msec() > 1031 )
	{
		//激活慢时钟；
		slowLastTime = curTime;
		SlowTimer();
	}
	return;
	TRY_END;
	return;
}

#ifndef ACE_WIN32
void catchpipe(int signo)
{
	signo = signo;
	D_WARNING( "收到SIGPIPE信号，忽略" );
}
#endif  //ACE_WIN32

static void sigIntHandler(int signo)
{
	D_DEBUG("收到%d信号", signo);
	return;
}

///应用程序主函数；
int ACE_TMAIN(int /*argc*/, ACE_TCHAR* /*argv*/[])
{
	TRY_BEGIN
		;

	MainInitTls();

	srand( (unsigned int)time(0) );		//随即种

	SetTSTP();//TSTP信号终止程序添加

	// 捕捉SIGINT信号
	ACE_Sig_Action handleSigInt(reinterpret_cast <ACE_SignalHandler>(sigIntHandler), SIGINT);
	ACE_UNUSED_ARG(handleSigInt);

	// 忽略SIGPIPE信号
	ACE_Sig_Action noSigPipe((ACE_SignalHandler)SIG_IGN, SIGPIPE);
	ACE_UNUSED_ARG(noSigPipe);

#ifdef ACE_WIN32
	CCrashLib CrashHandler;
#endif

	//读服务配置信息；
	if ( ! CLoadSrvConfig::LoadConfig() )
	{
		D_WARNING( "服务配置错！服务启动失败\n" );
		return 0;
	}

	SELF_SRV_ID = (unsigned short) CLoadSrvConfig::GetSelfID();

	D_INFO( "服务类型CenterSrv，序号:%d\n", CLoadSrvConfig::GetSelfID() );

	//初始化各收包处理函数；
	CDealGateSrvPkg::Init();

	g_poolMsgToPut = NEW TCache< MsgToPut >( 2000 );//全局MsgToPut对象池；
	g_poolPlayerGateID = NEW TCache< PlayerGateID >( 20000 );//全局PlayerGateID对象池；

#ifdef USE_DSIOCP
	CDsSocket::Init();
    g_poolPerIOData = NEW TCache< PerIOData >( 2000 );//尽量分配足够；
    g_poolUniDssocket = NEW TCache< UniqueObj< CDsSocket > >( 2000 );//尽量分配足够；
	g_poolDsSocket = NEW TCache< CDsSocket>( 256 );//全局DsSocket对象池，必须分配足够，因为只能使用Retrieve而不能使用RetrieveOrCreate；
	g_poolDsIOBuf = NEW TCache< DsIOBuf >( 2560 );//因为服务器端连接数少，虽然通信量大；
#endif //USE_DSIOCP

	IBufQueue* pSrvReadQueue = NEW CBufQueue();//从此队列读其它srv网络消息；
	IBufQueue* pSrvWriteQueue = NEW CBufQueue();//向此队列写其它srv网络消息；
	g_pSrvSender = pSrvWriteQueue;//其它srv发包器；

#ifdef USE_DSIOCP
	CDsIocp< IBufQueue, IBufQueue, CDsSocket >* srvIocp = NEW CDsIocp< IBufQueue, IBufQueue, CDsSocket >;
	srvIocp->Init( 128, pSrvReadQueue, pSrvWriteQueue );
	srvIocp->AddListen( CLoadSrvConfig::GetListenPort() );
#else //USE_DSIOCP
#ifdef USE_NET_TASK
	CNetTask<CHandlerT> srvTask(pSrvReadQueue, pSrvWriteQueue);//与服务端的网络通信任务；
	ACE_INET_Addr srvListenAddr( CLoadSrvConfig::GetListenPort() );
	if(srvTask.open((void*)&srvListenAddr) == -1)
	{
		D_DEBUG("srvtask activate error ,d=%d\n", 1);
		return 0;
	}
#else
	CNetTaskEx<CHandlerEx, ACE_Thread_Mutex> srvTask(pSrvReadQueue, pSrvWriteQueue);//与服务端的网络通信任务；
	ACE_INET_Addr srvListenAddr( CLoadSrvConfig::GetListenPort() );
	if(srvTask.Open((void*)&srvListenAddr) == -1)
	{
		D_DEBUG("srvtask activate error ,d=%d\n", 1);
		return 0;
	}
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP

	CManCommHandler< SRV_HANDLER_TYPE >* g_ManSrvCommHandler  //服务端收包句柄管理；
		= NEW CManCommHandler< SRV_HANDLER_TYPE >;
	g_ManSrvCommHandler->SetNetQueue( pSrvReadQueue );//服务端收包句柄管理，置收包来源(收包队列)；
	g_ManSrvCommHandler->SetHandlePoolSize( 256 );//服务端收包句柄管理，设置收包句柄对象池；
 
	//以下主线程继续工作，其间通过pWriteQueue、pReadQueue与网络线程通信；
	ACE_Time_Value stTime = ACE_OS::gettimeofday();
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	ACE_Time_Value pastTime;

	CCopyManager::InitCopyManager();

	D_INFO( "centersrv:运行中...\n" );

	const unsigned long runTime = CLoadSrvConfig::GetRunTime() * 1000;
	while (!g_isGlobeStop)
	{		
		curTime = ACE_OS::gettimeofday();
		pastTime = curTime - stTime;
		if ( ( runTime>0 )  //runTime == 0 不中断运行；
			&& ( pastTime.msec()>runTime )
			)
		{
#ifndef USE_DSIOCP
			srvTask.Quit();
			g_pSrvSender->SetReadEvent( true );//通知网络模块发送
#endif //USE_DSIOCP
			D_DEBUG( "runTime时刻到，退出\n" );
			break;
		}

		ST_SIG_CATCH {

			TimerCheck();

			g_ManSrvCommHandler->CommTimerProc();//服务端网络消息处理；

			g_pSrvSender->SetReadEvent( false );//通知网络模块发送

		} END_SIG_CATCH;
	}

#ifndef USE_DSIOCP
	// 等待任务结束
#ifdef USE_NET_TASK
	srvTask.wait();
#else
	srvTask.Wait();
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP

	D_INFO( "centersrv:服务正常结束\n" );

	CCopyManager::ReleaseCopyManager();

	//删服务端收包管理器；
	if ( NULL != g_ManSrvCommHandler )
	{
		delete g_ManSrvCommHandler; g_ManSrvCommHandler = NULL;
	}

#ifdef USE_DSIOCP
	delete srvIocp; srvIocp = NULL;
#endif //USE_DSIOCP

	//删服务端发送与接收缓冲；
	if ( NULL != pSrvReadQueue )
	{
		delete pSrvReadQueue; pSrvReadQueue = NULL;
	}
	if ( NULL != pSrvWriteQueue )
	{
		delete pSrvWriteQueue; pSrvWriteQueue = NULL;
	}

#ifdef USE_DSIOCP
	//删DsSocket对象池；
	if ( NULL != g_poolDsSocket )
	{
		delete g_poolDsSocket; g_poolDsSocket = NULL;
	}
    if ( NULL != g_poolDsIOBuf )
	{
		delete g_poolDsIOBuf; g_poolDsIOBuf = NULL;
	}
	//删唯一对象对象池；
	if ( NULL != g_poolUniDssocket )
	{
		delete g_poolUniDssocket; g_poolUniDssocket = NULL;
	}
	if ( NULL != g_poolPerIOData )
	{
		delete g_poolPerIOData; g_poolPerIOData = NULL;
	}
#endif //USE_DSIOCP

	if ( NULL != g_poolPlayerGateID )
	{
		delete g_poolPlayerGateID; g_poolPlayerGateID = NULL;
	}

	//删MsgToPut对象池；
	if ( NULL != g_poolMsgToPut )
	{
		delete g_poolMsgToPut; g_poolMsgToPut = NULL;
	}

	MainEndTls();

	return 0;
	TRY_END;
	return -1;
}
	

