﻿/**
* @file copymanager.h
* @brief 副本管理器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: copymanager.h
* 摘    要: 副本管理器
* 作    者: dzj
* 完成日期: 2009.11.02--
*
*/

#include "../../../Base/dscontainer/dshashtb.h"
#include "../../../Base/PkgProc/SrvProtocol.h"

#include <map>
#include <vector>
#include <set>

using namespace std;

//前置声明
struct PlayerGateID;

static const unsigned int   COPY_PLAYER_MAXNUM = 6; //副本最大人数；

///每个组队所拥有的副本信息；
struct TeamCopyInfo
{
	unsigned int curCopyNum;//当前队伍所拥有的副本数；
	unsigned int teamCopyIDs[COPY_PLAYER_MAXNUM];//当前队伍所拥有的各副本ID号；
};

///描述一个有效的副本地图服务器；
struct ValidCopyMapsrv
{
public:
	ValidCopyMapsrv() : copyMapsrvID(0), curCopyNum(0) {};
	ValidCopyMapsrv( unsigned short mapsrvID ) : copyMapsrvID(mapsrvID), curCopyNum(0) {};
	unsigned short copyMapsrvID;//副本地图srv ID号；
	unsigned int   curCopyNum;//该服务器当前管理的副本数；
};

///曾进过本副本的玩家信息；
class CopyPlayerInfos
{
	struct CopyPlayerEle
	{
	private:
		CopyPlayerEle& operator = ( const CopyPlayerEle& );//屏蔽这个操作，因为有string成员；

	public:
		CopyPlayerEle() { InitCopyPlayerEle(); };
		void InitCopyPlayerEle()
		{
			playerID.wGID = 0;
			playerID.dwPID = 0;
			scrollType = 0;//现语义已改为需要的副本券数量；
			//playerRoleName = "";
		}

		void SetCopyPlayerEleVal( const PlayerID& inPlayerID, unsigned int inScrollType/*, const char* inRoleName*/ )
		{
			playerID = inPlayerID;
			scrollType = inScrollType;//现语义已改为需要的副本券数量；
			//playerRoleName = inRoleName;
		}

		PlayerID     playerID;
		unsigned int scrollType;//玩家使用的副本券信息；
		//string       playerRoleName;//玩家昵称,由于使用次数较少，因此直接使用string，而不使用const char*；
	};

public:
	CopyPlayerInfos() { InitCopyPlayerInfos(); }

	inline void InitCopyPlayerInfos() 
	{
		playerInfoNum = 0;
		for ( unsigned int i=0; i<ARRAY_SIZE(playerInfos); ++i )
		{
			playerInfos[i].InitCopyPlayerEle();
		}
		return;
	}

public:
	///玩家离开组队删去该玩家之前保存的进副本信息；
	bool OnPlayerLeaveTeamDelInfo( const PlayerID& playerID )
	{
		if ( playerInfoNum <=  0 )
		{
			return false;
		}

		bool isFound = false;
		unsigned int  foundPos = 0;
		for ( unsigned int i=0; i<playerInfoNum; ++i )
		{
			if ( i > ARRAY_SIZE(playerInfos) )
			{
				D_ERROR( "OnPlayerLeaveTeamDelInfo, i(%d) > ARRAY_SIZE(playerInfos)\n", i );
				return false;
			}
			if ( playerID == playerInfos[i].playerID )
			{
				isFound = true;
				foundPos = i;
				break;
			}
		}

		if ( !isFound )
		{
			return false;
		}

		if ( ( foundPos >= ARRAY_SIZE(playerInfos) ) 
			|| ( playerInfoNum-1 >= ARRAY_SIZE(playerInfos) )
			)
		{
			D_ERROR( "OnPlayerLeaveTeamDelInfo, foundPos(%d)或playerInfoNum(%d)-1\n", foundPos, playerInfoNum );
			return false;
		}

		//已找到该玩家的初始进入信息，删去；
		playerInfos[foundPos].SetCopyPlayerEleVal( playerInfos[playerInfoNum-1].playerID
			, playerInfos[playerInfoNum-1].scrollType/*, playerInfos[playerInfoNum-1].playerRoleName.c_str()*/ );
		--playerInfoNum;

		return true;
	}

	//玩家进入副本时的检测(之前是否进过此副本，如果进过，是用何种卷轴进入的等等) 
	bool PlayerEnterCheck( const PlayerID& playerID/*, const char* roleName*/, unsigned int& scrollType/*in_out scrollType*/, bool& isOrgEnterThis/*之前是否进过此副本*/ )
	{
		bool isFound = false;
		unsigned int  foundPos = 0;
		for ( unsigned int i=0; i<playerInfoNum; ++i )
		{
			if ( i >= ARRAY_SIZE(playerInfos) )
			{
				D_ERROR( "PlayerEnterCheck, i(%d) >= ARRAY_SIZE(playerInfos)\n", i );
				break;
			}
			if ( playerID == playerInfos[i].playerID )
			{
				isFound = true;
				foundPos = i;
				break;
			}
		}

		if ( isFound )
		{
			//该玩家原来进过此副本；
			isOrgEnterThis = true;
			if ( foundPos >= ARRAY_SIZE(playerInfos) )
			{
				D_ERROR( "PlayerEnterCheck, foundPos(%d) >= ARRAY_SIZE(playerInfos)\n", foundPos );
				return false;
			}
			scrollType = playerInfos[foundPos].scrollType;//现语义已改为需要的副本券数量；
		} else {
			//该玩家原来未曾进过此副本；
			isOrgEnterThis = false;
			if ( playerInfoNum >= ARRAY_SIZE( playerInfos ) )
			{
				//超过预定数量的玩家曾进入单一副本；
				D_ERROR( "超过预定数量的玩家曾进入单一副本\n" );
				return false;
			}
			playerInfos[playerInfoNum].playerID = playerID;
			playerInfos[playerInfoNum].scrollType = scrollType;//现语义已改为需要的副本券数量；
			//playerInfos[playerInfoNum].playerRoleName = roleName;
			++playerInfoNum;
		}

		return true;
	}

private:
	unsigned int  playerInfoNum;//当前有效的曾进过本副本的玩家信息；
	CopyPlayerEle playerInfos[COPY_PLAYER_MAXNUM];
};

///centersrv上管理某一副本的所有信息；
struct CenterCopyInfo
{
	CopyManagerEle   copyManagerEle;//副本信息；
	CopyPlayerInfos  everEntered;//曾进过此副本的玩家；
	PlayerID         deler;//删除本副本者，在副本成功删除后通知此人；
};

///管理可用的副本地图服务器；
class CopyMapSrvManager
{
public:
	CopyMapSrvManager()
	{
		m_validCopyMapSrvs.clear();
	}

public:
	//收到了新的副本服务器号；
	bool OnNewCopyMapSrvID( unsigned short mapsrvID )
	{
		for ( vector<ValidCopyMapsrv>::iterator iter=m_validCopyMapSrvs.begin(); iter!=m_validCopyMapSrvs.end(); ++iter )
		{
			if ( mapsrvID == iter->copyMapsrvID )
			{
				D_ERROR( "OnNewCopyMapSrvID,重复通知可用副本服务器%d\n", mapsrvID );
				return false;
			}
		}

		D_DEBUG( "收到可用的副本地图服务器%d通知\n", mapsrvID );

		//确实为新copymapsrv;
		m_validCopyMapSrvs.push_back( ValidCopyMapsrv(mapsrvID) );

		return true;
	}

public:
	///选择一个负担最小的副本地图服务器；
	bool SelectOneCopyMapSrv( unsigned short& selectedSrvID )
	{
		if ( m_validCopyMapSrvs.empty() )
		{
			D_WARNING( "无可用的副本地图服务器\n" );
			return false;
		}

		if ( m_validCopyMapSrvs.back().curCopyNum > 100 )
		{
			D_WARNING( "各副本服务器都已满，当前可用服务器数量%d，负担最轻服务器当前负担副本数%d\n", m_validCopyMapSrvs.size(), m_validCopyMapSrvs.back().curCopyNum );
			return false;
		}

		selectedSrvID = m_validCopyMapSrvs.back().copyMapsrvID;
		return true;
	}

public:
	///某个副本地图服务器被分配了一个副本；
	bool CopyMapSrvAddOneCopy( unsigned short usedSrvID )
	{
		bool isFound=false;
		for ( vector<ValidCopyMapsrv>::reverse_iterator riter=m_validCopyMapSrvs.rbegin(); riter!=m_validCopyMapSrvs.rend(); ++riter )
		{
			if ( usedSrvID == riter->copyMapsrvID )
			{
				isFound = true;
				++(riter->curCopyNum);
				D_DEBUG( "CopyMapSrvAddOneCopy，分配副本至srv%d，分配后该srv负责了%d个副本\n", usedSrvID, riter->curCopyNum );

				///重新整理顺序；
				ValidCopyMapsrv& modifiedEle = *riter;//保存更新后的服务器负担信息；
				//以下找到自身在vector中的新位置；
				for ( vector<ValidCopyMapsrv>::iterator fiter=m_validCopyMapSrvs.begin(); fiter!=m_validCopyMapSrvs.end(); ++fiter )
				{
					if ( fiter->curCopyNum < modifiedEle.curCopyNum )//不可用=号，防止交换一个同样大负载者到队尾（例如，最后一个加1后，与第一个相等时的情形）
					{
						//找到了交换位置；
						unsigned int tmpnum = modifiedEle.curCopyNum;
						unsigned short tmpsrvid = modifiedEle.copyMapsrvID;
						modifiedEle.curCopyNum = fiter->curCopyNum;
						modifiedEle.copyMapsrvID = fiter->copyMapsrvID;
						fiter->curCopyNum = tmpnum;
						fiter->copyMapsrvID = tmpsrvid;
						break;
					}
				}
				break;
			}
		}
		return isFound;
	}

	///某个副本地图服务器上运行的副本被释放；
	bool CopyMapSrvReleaseOneCopy( unsigned short usedSrvID )
	{
		bool isFound=false;
		for ( vector<ValidCopyMapsrv>::iterator fiter=m_validCopyMapSrvs.begin(); fiter!=m_validCopyMapSrvs.end(); ++fiter )
		{
			if ( usedSrvID == fiter->copyMapsrvID )
			{
				isFound = true;
				if ( fiter->curCopyNum <= 0 )
				{
					D_ERROR( "CopyMapSrvReleaseOneCopy, 服务器%d无任何负担，之前数据维护出错\n", usedSrvID );
					return false;//该服务器无任何负担，之前数据维护出了错；
				}
				--(fiter->curCopyNum);
				D_DEBUG( "CopyMapSrvReleaseOneCopy，从srv%d释放一个副本，该srv释放后负责了%d个副本\n", usedSrvID, fiter->curCopyNum );
				ValidCopyMapsrv& modifiedEle = *fiter;//保存更新后的服务器负担信息；
				//以下找到自身在vector中的新位置；
				for ( vector<ValidCopyMapsrv>::reverse_iterator riter=m_validCopyMapSrvs.rbegin(); riter!=m_validCopyMapSrvs.rend(); ++riter )
				{
					if ( riter->curCopyNum >= modifiedEle.curCopyNum ) //=号防止越过自身去交换(例如，最后一个释放后自身仍旧为最后一个的情形);
					{
						unsigned int tmpnum = modifiedEle.curCopyNum;
						unsigned short tmpsrvid = modifiedEle.copyMapsrvID;
						modifiedEle.curCopyNum = riter->curCopyNum;
						modifiedEle.copyMapsrvID = riter->copyMapsrvID;
						riter->curCopyNum = tmpnum;
						riter->copyMapsrvID = tmpsrvid;
						break;
					}
				}
				break;
			}
		}
		return isFound;
	}

private:
	vector<ValidCopyMapsrv> m_validCopyMapSrvs;//换负担顺序存放的各副本服务器负担；
};

///维护内部的，用于向client广播的全局副本信息；
struct GlobeCopyInfo
{
public:
	GlobeCopyInfo()
	{
		ClearGlobeCopyInfo();
	}

    inline void ClearGlobeCopyInfo()
	{
		StructMemSet( m_globeCopyInfos, 0, sizeof(m_globeCopyInfos) );
		return;
	}

public:
	//向指定玩家发送全局副本信息；
	bool SendGlobeCopyInfoToPlayer( const PlayerID& playerID );

public:
	bool SetMapIDNum( unsigned short mapid, unsigned int curnum )
	{
		bool isFound = false;
		unsigned int  foundPos = 0;
		for ( unsigned int i=0; i<m_globeCopyInfos.globeCopyInfos.infoNum; ++i )
		{
			if ( i >= ARRAY_SIZE(m_globeCopyInfos.globeCopyInfos.infoArr) )
			{
				D_ERROR( "SetMapIDNum, i(%d) >= ARRAY_SIZE(m_globeCopyInfos.globeCopyInfos.infoArr)\n", i );
				break;
			}
			if ( mapid == m_globeCopyInfos.globeCopyInfos.infoArr[i].copyMapID )
			{
				isFound = true;
				foundPos = i;
				break;
			}
		}
		if ( isFound )
		{
			//原来有该副本，该副本的人数变化
			if ( foundPos >= ARRAY_SIZE(m_globeCopyInfos.globeCopyInfos.infoArr) )
			{
				D_ERROR( "SetMapIDNum, foundPos(%d) >= ARRAY_SIZE(m_globeCopyInfos.globeCopyInfos.infoArr)\n", foundPos );
				return false;
			}
			m_globeCopyInfos.globeCopyInfos.infoArr[foundPos].curCopyNum = curnum;//修改该副本人数；
			if ( 0 == curnum )
			{
				//此类副本的拷贝数已降为0，删去此副本信息(用最后一个副本信息覆盖待删信息)
				if ( m_globeCopyInfos.globeCopyInfos.infoNum-1 >= ARRAY_SIZE(m_globeCopyInfos.globeCopyInfos.infoArr) )
				{
					D_ERROR( "SetMapIDNum, m_globeCopyInfos.globeCopyInfos.infoNum(%d)-1 >= ARRAY_SIZE(m_globeCopyInfos.globeCopyInfos.infoArr)\n"
						, m_globeCopyInfos.globeCopyInfos.infoNum-1 );
					return false;
				}
				m_globeCopyInfos.globeCopyInfos.infoArr[foundPos] = m_globeCopyInfos.globeCopyInfos.infoArr[m_globeCopyInfos.globeCopyInfos.infoNum-1];
				--m_globeCopyInfos.globeCopyInfos.infoNum;
			}
		} else {
			//原来没有该副本
			if ( 1 != curnum  )
			{
				//新建副本，必定当前copy数为1;
				D_ERROR( "新建副本类型%d时，当前copy数%d不为1\n", mapid, curnum );
				return false;
			}
			if ( m_globeCopyInfos.globeCopyInfos.infoNum>=ARRAY_SIZE(m_globeCopyInfos.globeCopyInfos.infoArr) )
			{
				//副本类型数超过了GC消息中的数组容量；
				D_ERROR( "副本类型数超过了GC消息中的数组容量\n" );
				return false;
			}
			m_globeCopyInfos.globeCopyInfos.infoArr[m_globeCopyInfos.globeCopyInfos.infoNum].copyMapID = mapid;
			m_globeCopyInfos.globeCopyInfos.infoArr[m_globeCopyInfos.globeCopyInfos.infoNum].curCopyNum = curnum;
			m_globeCopyInfos.globeCopyInfos.infoArr[m_globeCopyInfos.globeCopyInfos.infoNum].isCopyOpen = true;
			m_globeCopyInfos.globeCopyInfos.infoArr[m_globeCopyInfos.globeCopyInfos.infoNum].isMaxCopyNum = false;
			++m_globeCopyInfos.globeCopyInfos.infoNum;
		}

		return true;
	}

public:
	EGGlobeCopyInfos* GetInnerCopyInfos() { return &m_globeCopyInfos; };

private:
	EGGlobeCopyInfos  m_globeCopyInfos;//全局真副本信息；
};

///全服副本管理器；
class CCopyManager
{
public:
	///初始化全服副本管理器；
	static void InitCopyManager()
	{
		m_AllCopys.ClearAllEle();
		m_teamInfos.clear();
		m_mapCopyNums.clear();
		m_globeCopyInfos.ClearGlobeCopyInfo();
	};

	///释放全服副本管理器；
	static void ReleaseCopyManager()
	{
		m_AllCopys.ExploreProcess( &(CCopyManager::OnCopyEleBeReleaseExplored), NULL );
		m_teamInfos.clear();
		m_mapCopyNums.clear();
		m_globeCopyInfos.ClearGlobeCopyInfo();
	};

public:
	///副本管理器时钟
	static void CopyManagerTimerProc();

private:
	///元素被时钟遍历；
	static bool OnCopyEleBeTimeExplored( HashTbPtrEle<CenterCopyInfo>* beExploredEle, void* pParam );
	///管理器release时，元素被遍历；
	static bool OnCopyEleBeReleaseExplored( HashTbPtrEle<CenterCopyInfo>* beExploredEle, void* pParam );

public:
	///处理玩家的进副本请求；
	static bool OnEnterCopyQuest( const EnterCopyReq& enterReq );
	///处理玩家离开副本请求
	static bool OnLeaveCopyQuest( const PlayerID& playerID, const char* playerAccount, unsigned int copyid, unsigned short copymapid );
	///处理禁止更多玩家进入标记
	static bool OnCopyNomorePlayer( unsigned int copyid );
	///处理玩家离开副本
	static bool PlayerOfflineLeaveCopy( unsigned int copyID, const PlayerID& playerID );
	///处理mapsrv发来的删除副本请求；
	static bool OnDelCopyQuest( unsigned int copyID );
	///取玩家组队拥有的副本信息，必须使用teamID查而不能使用玩家当前副本号去查，因为玩家查询时可能在副本外，但他所在队伍仍可能拥有副本；
	static bool SendTeamCopyInfoToPlayer( const PlayerID& playerID, unsigned int steamID );
	///向指定玩家发送全局副本信息；
	static bool SendGlobeCopyInfoToPlayer( const PlayerID& playerID )
	{
		return m_globeCopyInfos.SendGlobeCopyInfoToPlayer( playerID );
	}

public:
	///处理玩家离开副本组队处理；
	static bool OnPlayerLeaveSTeam( const char* playerRoleName, const PlayerID& playerID, const PlayerGateID& playerGateID );
	///副本组队解散；
	static bool OnSTeamDestoryed( unsigned int steamid );
	///玩家重置副本，是否队长已在gate检测
	static bool OnResetCopy( const PlayerID& captainPlayerID, unsigned int steamID, unsigned short copyMapsrvID );

public:
	///收到gate通知有新的可用副本地图服务器；
	static bool OnNewCopyMapsrvID( unsigned short newCopyMapsrvID )
	{
		return m_copyMapSrvManager.OnNewCopyMapSrvID( newCopyMapsrvID );
	}

private:
	///选择一个负担最小的副本地图服务器；
	static bool SelectOneCopyMapSrv( unsigned short& selectedSrvID )
	{
		return m_copyMapSrvManager.SelectOneCopyMapSrv( selectedSrvID );
	}
	///某个副本地图服务器被分配了一个副本；
	static bool CopyMapSrvAddOneCopy( unsigned short usedSrvID )
	{
		return m_copyMapSrvManager.CopyMapSrvAddOneCopy( usedSrvID );
	}
	///某个副本地图服务器上运行的副本被释放；
	static bool CopyMapSrvReleaseOneCopy( unsigned short usedSrvID )
	{
		return m_copyMapSrvManager.CopyMapSrvReleaseOneCopy( usedSrvID );
	}

private:
	///通知玩家系统警告消息
    static bool NotiPlayerSystemWarning( const PlayerID& playerID, const char* systemWarning );

private:
	///玩家进入旧副本；
	static bool EnterOldCopy( const EnterCopyReq& enterReq, CenterCopyInfo* orgCopyInfo );
	///玩家创建新副本；
	static bool EnterNewCopy( const EnterCopyReq& enterReq );
	///通知玩家进副本；
	static bool NotiPlayerEnterCopy( const EnterCopyReq& enterReq, CenterCopyInfo* copyInfo, unsigned int needScrollNum/*需要的副本券数量*/ );
	///通知玩家离开副本；
	static bool NotiPlayerLeaveCopy( const PlayerID& playerID, const CenterCopyInfo* orgCopyInfo );
	///删除旧副本；
	static bool DelOldCopy( unsigned int copyID );

private:
	static DsHashTable< HashTbPtrEle<CenterCopyInfo>, 20480, 1/*若有冲突，则协商重放*/ > m_AllCopys;//所有副本；//不再使用hash宏了，直接使用；
	static map<unsigned int, TeamCopyInfo> m_teamInfos;//各队伍的副本信息；
	static map<unsigned short, unsigned int> m_mapCopyNums;//各不同地图分别创建了多少副本；

	static unsigned int g_CopyIDGener;//副本号分配器;

private:
	/*正常情况下不可能在收到mapsrv删自身请求时发现副本中还有人。
	但也有极小概率情形为：之前centersrv的playerA进入消息还未到mapsrv(此时center playernum > 0)，
	而mapsrv的副本检测到删除条件已满足(比如已空闲达一定秒数)，因此发来了删除副本自身请求，到center后就将进到此处。
	在此情形下，稍后，playerA的进副本过程将在mapsrv失败(因为mapsrv已发删自身请求，因此不会让playerA进副本)并被踢下线，
	而centersrv收到playerA下线消息时会减pCopyInfo->curPlayerNum至0。
	为了处理这种情况，有必要之后在centersrv不断检测该副本人数，直至副本人数为0后，继续此删副本过程；
	*/
	static set<unsigned int> m_toDelCopyWaitPlayerNum0;

	static GlobeCopyInfo     m_globeCopyInfos;//全局副本信息；

private:
	static CopyMapSrvManager m_copyMapSrvManager;//管理可用的各个副本服务器；
};