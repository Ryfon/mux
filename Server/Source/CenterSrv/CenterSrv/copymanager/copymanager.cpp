﻿/**
* @file copymanager.h
* @brief 副本管理器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: copymanager.h
* 摘    要: 副本管理器
* 作    者: dzj
* 完成日期: 2009.11.02--
*
*/

#include "copymanager.h"
#include "../DealPkg/DealPkg.h"

#include "../OtherServer/otherserver.h"

DsHashTable< HashTbPtrEle<CenterCopyInfo>, 20480, 1/*若有冲突，则协商重放*/ > CCopyManager::m_AllCopys;//所有副本；//不再使用hash宏了，直接使用；
map<unsigned int/*队伍号*/, TeamCopyInfo/*队伍所拥有的各副本*/> CCopyManager::m_teamInfos;//各队伍的副本信息；
map<unsigned short/*地图号*/, unsigned int/*副本数*/> CCopyManager::m_mapCopyNums;//各不同地图分别创建了多少副本；
unsigned int CCopyManager::g_CopyIDGener = 0;
GlobeCopyInfo CCopyManager::m_globeCopyInfos;//用于通知客户端的全局副本信息(各mapid各自开了多少副本，是否已达上限，伪副本是否开启)；

/*正常情况下不可能在收到mapsrv删自身请求时发现副本中还有人。
但也有极小概率情形为：之前centersrv的playerA进入消息还未到mapsrv(此时center playernum > 0)，
而mapsrv的副本检测到删除条件已满足(比如已空闲达一定秒数)，因此发来了删除副本自身请求，到center后就将进到此处。
在此情形下，稍后，playerA的进副本过程将在mapsrv失败(因为mapsrv已发删自身请求，因此不会让playerA进副本)并被踢下线，
而centersrv收到playerA下线消息时会减pCopyInfo->curPlayerNum至0。
为了处理这种情况，有必要之后在centersrv不断检测该副本人数，直至副本人数为0后，继续此删副本过程；
*/
set<unsigned int> CCopyManager::m_toDelCopyWaitPlayerNum0;

CopyMapSrvManager CCopyManager::m_copyMapSrvManager;//管理可用的各个副本服务器；

bool GlobeCopyInfo::SendGlobeCopyInfoToPlayer( const PlayerID& playerID )
{
	m_globeCopyInfos.notiPlayerID = playerID;

	//找到对应的gatesrv进行发送
	CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( m_globeCopyInfos.notiPlayerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR( "CCopyManager::SendGlobeCopyInfoToPlayer,找不到对应的GATESRV%d\n", m_globeCopyInfos.notiPlayerID.wGID );
		return false;
	}

	MsgToPut* pNewMsg = CreateSrvPkg( EGGlobeCopyInfos, pSrv, m_globeCopyInfos );
	pSrv->SendPkgToSrv( pNewMsg );

	return true;
}


///元素被时钟遍历；
bool CCopyManager::OnCopyEleBeTimeExplored( HashTbPtrEle<CenterCopyInfo>* beExploredEle, void* pParam )
{
	if ( NULL == beExploredEle )
	{
		return false;
	}
	if ( NULL == pParam )
	{
		return false;
	}

	CenterCopyInfo* pCenterCopyInfo = beExploredEle->GetInnerPtr();
	if ( NULL == pCenterCopyInfo )
	{
		return false;
	}

	return true;
}

///管理器release时，元素被遍历；
bool CCopyManager::OnCopyEleBeReleaseExplored( HashTbPtrEle<CenterCopyInfo>* beExploredEle, void* pParam )
{
	if ( NULL == beExploredEle )
	{
		return false;
	}
	ACE_UNUSED_ARG( pParam );

	CenterCopyInfo* pCenterCopyInfo = beExploredEle->GetInnerPtr();
	if ( NULL == pCenterCopyInfo )
	{
		return false;
	}

	delete pCenterCopyInfo; pCenterCopyInfo = NULL;

	return true;
}

///副本管理器时钟
void CCopyManager::CopyManagerTimerProc()
{
	if ( !m_toDelCopyWaitPlayerNum0.empty() )
	{
		/*正常情况下不可能在收到mapsrv删自身请求时发现副本中还有人。
		但也有极小概率情形为：之前centersrv的playerA进入消息还未到mapsrv(此时center playernum > 0)，
		而mapsrv的副本检测到删除条件已满足(比如已空闲达一定秒数)，因此发来了删除副本自身请求，到center后就将进到此处。
		在此情形下，稍后，playerA的进副本过程将在mapsrv失败(因为mapsrv已发删自身请求，因此不会让playerA进副本)并被踢下线，
		而centersrv收到playerA下线消息时会减pCopyInfo->curPlayerNum至0。
		为了处理这种情况，有必要之后在centersrv不断检测该副本人数，直至副本人数为0后，继续此删副本过程；
		*/
		//此处再次检查这些副本的人数是否已为0，若为0则继续删除过程；
		vector<unsigned int> delOKCopyids;
		for ( set<unsigned int>::iterator iter=m_toDelCopyWaitPlayerNum0.begin(); iter!=m_toDelCopyWaitPlayerNum0.end(); ++iter )
		{
			if ( DelOldCopy( *iter ) )
			{
				delOKCopyids.push_back( *iter );//已顺利删除者；
			}
		}
		if ( !delOKCopyids.empty() )
		{
			//已顺利删除者从m_toDelCopyWaitPlayerNum0中删去；
			for ( vector<unsigned int>::iterator iter=delOKCopyids.begin(); iter!=delOKCopyids.end(); ++iter )
			{
				m_toDelCopyWaitPlayerNum0.erase( *iter );
			}
		}
	}

	return;
}

///处理玩家的进副本请求；
bool CCopyManager::OnEnterCopyQuest( const EnterCopyReq& enterReq )
{
	//1、查找该玩家所在队伍的副本信息，依次检查队伍当前副本是否为所欲进入的mapid;
	//2、如有找到，副本人数++;
	//3、如未找到，新建副本，修改各容器管理器，选玩家所在mapsrv通知其创建新副本;
	//4、在玩家身上置副本号标记，通知gate玩家进入副本，gate置副本信息后，两头发跳转地图通知;

	//if ( 0 != enterReq.szAccount[ARRAY_SIZE(enterReq.szAccount)-1] )
	//{
	//	D_ERROR( "OnEnterCopyQuest, 玩家(%d:%x)欲进副本(mapid:%d)，错误的帐号名\n"
	//		, enterReq.playerID.wGID, enterReq.playerID.dwPID, enterReq.copyMapID );
	//	return false;
	//}

	map<unsigned int, TeamCopyInfo>::iterator iter = m_teamInfos.find( enterReq.teamID );
	if ( iter == m_teamInfos.end() )
	{
		//原来无该队伍的副本，新建之
		return EnterNewCopy( enterReq );		
	}

	bool isFound = false;
	TeamCopyInfo* teamCopyInfo= &(iter->second);
	HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	CenterCopyInfo* pCopyInfo = NULL;
	for ( unsigned int i=0; i<teamCopyInfo->curCopyNum; ++i )
	{
		if ( i >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs) )
		{
			D_ERROR( "CCopyManager::OnEnterCopyQuest, i(%d) >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs), teamID:%d\n", i, enterReq.teamID );
			break;
		}
		//检查该队伍中是否有对应mapid的副本；
		if ( !m_AllCopys.FindEle( teamCopyInfo->teamCopyIDs[i], ptrEle ) )
		{
			D_ERROR( "OnEnterCopyQuest, 不可能错误，队伍%d的第%d个副本%d在管理器中不存在\n", enterReq.teamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		if ( NULL == ptrEle ) 
		{
			D_ERROR( "OnEnterCopyQuest, 不可能错误，队伍%d的第%d个副本%d取不到对应信息指针\n", enterReq.teamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		pCopyInfo = ptrEle->GetInnerPtr();
		if ( NULL == pCopyInfo ) 
		{
			D_ERROR( "OnEnterCopyQuest, 不可能错误，队伍%d的第%d个副本%d取到的副本信息指针空\n", enterReq.teamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		if ( enterReq.copyMapID == pCopyInfo->copyManagerEle.enterReq.copyMapID )
		{
			isFound = true;
			break;
		}		
	}

	if ( !isFound )
	{
		//原来无该队伍的副本，新建之
		return EnterNewCopy( enterReq );
	}

	if ( ( NULL == pCopyInfo ) 
		|| ( enterReq.copyMapID != pCopyInfo->copyManagerEle.enterReq.copyMapID ) 
		)
	{
		D_ERROR( "OnEnterCopyQuest, 不可能错误，取到的队伍%d的副本信息空(地图号:%d)\n", enterReq.teamID, enterReq.copyMapID );
		return false;
	}

	//旧副本，进此副本；
	return EnterOldCopy( enterReq, pCopyInfo );
}

///处理玩家离开副本请求
bool CCopyManager::OnLeaveCopyQuest( const PlayerID& playerID, const char* playerAccount, unsigned int copyid, unsigned short copymapid )
{
	if ( NULL == playerAccount )
	{
		D_WARNING( "CCopyManager::OnLeaveCopyQuest，找不到对应的PlayerGateID,玩家%s可能已下线\n", playerAccount );
		return false;
	}

	PlayerGateID* playerGateID = CDealGateSrvPkg::FindOnlinePlayerGateIDByAccount( playerAccount );
	if ( NULL == playerGateID )
	{
		D_WARNING( "CCopyManager::OnLeaveCopyQuest，找不到对应的PlayerGateID,玩家%s可能已下线\n", playerAccount );
		return false;
	}

	if ( playerID != playerGateID->playerID )
	{
		D_ERROR( "CCopyManager::OnLeaveCopyQuest，playerID != playerGateID->playerID，玩家%s, copymapid%d\n", playerAccount, copymapid );
		return false;
	}

	if ( 0 == playerGateID->playerCopyID )
	{
		D_WARNING( "CCopyManager::OnLeaveCopyQuest，0 == playerGateID->playerCopyID，玩家%s在此之前已先行离开了副本\n", playerAccount );
		return false;
	}

	if ( copyid != playerGateID->playerCopyID )
	{
		D_WARNING( "CCopyManager::OnLeaveCopyQuest，消息中的copyid%d与centersrv的当前保存id%d不匹配，可能玩家%s在此之前换副本\n"
			, copyid, playerGateID->playerCopyID, playerAccount );
		return false;
	}

	//1、找到指定副本;
	HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	if ( !(m_AllCopys.FindEle( copyid, ptrEle )) )
	{
		D_ERROR( "CCopyManager::OnLeaveCopyQuest, 找不到指定的欲离开副本%d\n", copyid );
		return false;
	}

	if ( NULL == ptrEle )
	{
		D_ERROR ( "CCopyManager::OnLeaveCopyQuest， NULL == ptrEle\n" );
		return false;
	}

	CenterCopyInfo* pCopyInfo = ptrEle->GetInnerPtr();
	if ( NULL == pCopyInfo )
	{
		D_ERROR ( "CCopyManager::OnLeaveCopyQuest， NULL == pCopyInfo\n" );
		return false;
	}

	if ( (int/*人数不可能大到这种程度*/)(pCopyInfo->copyManagerEle.curPlayerNum) <= 0 )
	{
		D_ERROR ( "CCopyManager::OnLeaveCopyQuest， pCopyInfo->curPlayerNum <= 0\n" );
		return false;
	}

	//找到对应的gatesrv进行发送
	CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( playerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR( "CCopyManager::OnLeaveCopyQuest,找不到对应的GATESRV%d\n", playerID.wGID );
		return false;
	}

	--(pCopyInfo->copyManagerEle.curPlayerNum);
	if ( !(pCopyInfo->everEntered.OnPlayerLeaveTeamDelInfo( playerID )) )
	{
		//玩家离开副本，清玩家的曾进信息；
		D_ERROR( "玩家(%d:%d)离开副本%d(mapid:%d)，发现玩家此前没有进过该副本\n"
			, playerID.wGID, playerID.dwPID, pCopyInfo->copyManagerEle.copyID, pCopyInfo->copyManagerEle.enterReq.copyMapID );
	}

	playerGateID->playerCopyID = 0;

	if ( (int/*人数不可能大到这种程度*/)(pCopyInfo->copyManagerEle.curPlayerNum) < 0 )
	{
		D_ERROR( "CCopyManager::OnLeaveCopyQuest， pCopyInfo->curPlayerNum < 0\n" );
		return false;
	}

	D_DEBUG( "player %s leave old copy %d, send cmd to gatesrv\n", playerAccount, pCopyInfo->copyManagerEle.copyID );

	//通知玩家离开副本；
	EGLeaveCopyCmd leaveCopyCmd;
	leaveCopyCmd.leavePlayerID = playerID;
	leaveCopyCmd.copyInfo = pCopyInfo->copyManagerEle;

	MsgToPut* pNewMsg = CreateSrvPkg( EGLeaveCopyCmd, pSrv, leaveCopyCmd );
	pSrv->SendPkgToSrv( pNewMsg );

	return true;
}

///处理禁止更多玩家进入标记
bool CCopyManager::OnCopyNomorePlayer( unsigned int copyid )
{
	//1、找到指定副本;
	HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	if ( !(m_AllCopys.FindEle( copyid, ptrEle )) )
	{
		D_ERROR( "CCopyManager::OnCopyNomorePlayer, 找不到指定的不可再进副本%d\n", copyid );
		return false;
	}

	if ( NULL == ptrEle )
	{
		D_ERROR ( "CCopyManager::OnCopyNomorePlayer， NULL == ptrEle\n" );
		return false;
	}

	CenterCopyInfo* pCopyInfo = ptrEle->GetInnerPtr();
	if ( NULL == pCopyInfo )
	{
		D_ERROR ( "CCopyManager::OnCopyNomorePlayer， NULL == pCopyInfo\n" );
		return false;
	}

	if ( pCopyInfo->copyManagerEle.isNoMorePlayer )
	{
		D_ERROR ( "CCopyManager::OnCopyNomorePlayer， isNoMorePlayer标记已置\n" );
		return false;
	}

	pCopyInfo->copyManagerEle.isNoMorePlayer = true;

	return true;
}

///玩家离开副本处理；
bool CCopyManager::PlayerOfflineLeaveCopy( unsigned int copyID, const PlayerID& playerID )
{
	//1、找到指定副本;
	//2、--副本人数；
	HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	if ( !(m_AllCopys.FindEle( copyID, ptrEle )) )
	{
		D_ERROR( "CCopyManager::PlayerLeaveCopy, 找不到指定的离开副本%d\n", copyID );
		return false;
	}

	if ( NULL == ptrEle )
	{
		D_ERROR ( "CCopyManager::PlayerLeaveCopy， NULL == ptrEle\n" );
		return false;
	}

	CenterCopyInfo* pCopyInfo = ptrEle->GetInnerPtr();
	if ( NULL == pCopyInfo )
	{
		D_ERROR ( "CCopyManager::PlayerLeaveCopy， NULL == pCopyInfo\n" );
		return false;
	}

	if ( pCopyInfo->copyManagerEle.curPlayerNum <= 0 )
	{
		D_ERROR ( "CCopyManager::PlayerLeaveCopy， pCopyInfo->curPlayerNum <= 0\n" );
		return false;
	}

	--(pCopyInfo->copyManagerEle.curPlayerNum);
	if ( !(pCopyInfo->everEntered.OnPlayerLeaveTeamDelInfo( playerID )) )
	{
		//玩家离开副本，清玩家的曾进信息；
		D_ERROR( "玩家(%d:%d)离开副本%d(mapid:%d)，发现玩家此前没有进过该副本\n"
			, playerID.wGID, playerID.dwPID, pCopyInfo->copyManagerEle.copyID, pCopyInfo->copyManagerEle.enterReq.copyMapID );
	}

	if ( (int/*人数不可能大到这种程度*/)(pCopyInfo->copyManagerEle.curPlayerNum) < 0 )
	{
		D_ERROR( "CCopyManager::PlayerLeaveCopy， pCopyInfo->curPlayerNum < 0\n" );
	}

	return true;
}

///处理mapsrv发来的删除副本请求；
bool CCopyManager::OnDelCopyQuest( unsigned int copyID )
{
	return DelOldCopy( copyID );
}

///取玩家组队拥有的副本信息；
bool CCopyManager::SendTeamCopyInfoToPlayer( const PlayerID& playerID, unsigned int steamID )
{
	EGTeamCopyInfos outTeamCopyInfos;
	StructMemSet( outTeamCopyInfos, 0, sizeof(outTeamCopyInfos) );

	outTeamCopyInfos.notiPlayerID = playerID;
	outTeamCopyInfos.steamID = steamID;

	map<unsigned int, TeamCopyInfo>::iterator iter = m_teamInfos.find( steamID );
	if ( iter == m_teamInfos.end() )
	{
		outTeamCopyInfos.teamCopyInfos.infoNum = 0;//没有组队拥有的副本，不向client发消息；
		D_DEBUG( "SendTeamCopyInfoToPlayer，玩家(%d,%d)，副本组队%d，不拥有任何副本\n", playerID.wGID, playerID.dwPID, steamID );
		return false;
	}

	//bool isFound = false;
	TeamCopyInfo* teamCopyInfo= &(iter->second);
	HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	CenterCopyInfo* pCopyInfo = NULL;
	for ( unsigned int i=0; i<teamCopyInfo->curCopyNum; ++i )
	{
		if ( i >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs) )
		{
			D_ERROR( "CCopyManager::SendTeamCopyInfoToPlayer, i(%d) >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs), teamID(%d)\n", i, steamID );
			break;
		}
		//检查该队伍中是否有对应mapid的副本；
		if ( !m_AllCopys.FindEle( teamCopyInfo->teamCopyIDs[i], ptrEle ) )
		{
			D_ERROR( "OnQueryPlayerTeamCopyInfo, 不可能错误，队伍%d的第%d个副本%d在管理器中不存在\n", steamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		if ( NULL == ptrEle ) 
		{
			D_ERROR( "OnQueryPlayerTeamCopyInfo, 不可能错误，队伍%d的第%d个副本%d取不到对应信息指针\n", steamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		pCopyInfo = ptrEle->GetInnerPtr();
		if ( NULL == pCopyInfo ) 
		{
			D_ERROR( "OnQueryPlayerTeamCopyInfo, 不可能错误，队伍%d的第%d个副本%d取到的副本信息指针空\n", steamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		//if ( outTeamCopyInfos.teamCopyInfos.infoNum >= ARRAY_SIZE(outTeamCopyInfos.teamCopyInfos.infoArr) )
		//{
		//	D_ERROR( "组队拥有的副本类型数量，超过了GC消息的容量限制\n" );
		//	break;
		//}
		if ( outTeamCopyInfos.teamCopyInfos.infoNum >= ARRAY_SIZE(outTeamCopyInfos.teamCopyInfos.infoArr) )
		{
			D_ERROR( "OnQueryPlayerTeamCopyInfo, outTeamCopyInfos.teamCopyInfos.infoNum(%d) >= ARRAY_SIZE(outTeamCopyInfos.etamCopyInfos.infoArr), teamID:%d\n"
				, outTeamCopyInfos.teamCopyInfos.infoNum, steamID );
			break;
		}
		outTeamCopyInfos.teamCopyInfos.infoArr[outTeamCopyInfos.teamCopyInfos.infoNum].copyMapID = pCopyInfo->copyManagerEle.enterReq.copyMapID;
		outTeamCopyInfos.teamCopyInfos.infoArr[outTeamCopyInfos.teamCopyInfos.infoNum].validTimeLeft = 60*60*1000/*60分钟*/ - ( GetTickCount()-pCopyInfo->copyManagerEle.createTime );
		outTeamCopyInfos.teamCopyInfos.infoArr[outTeamCopyInfos.teamCopyInfos.infoNum].realTimeStage = 0;//暂时没有实时进度，全为0;
		++(outTeamCopyInfos.teamCopyInfos.infoNum);
	}

	//找到对应的gatesrv进行发送
	CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( outTeamCopyInfos.notiPlayerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR( "OnQueryPlayerTeamCopyInfo,找不到对应的GATESRV%d\n", outTeamCopyInfos.notiPlayerID.wGID );
		return false;
	}

	MsgToPut* pNewMsg = CreateSrvPkg( EGTeamCopyInfos, pSrv, outTeamCopyInfos );
	pSrv->SendPkgToSrv( pNewMsg );

	return true;
}

///处理玩家离开副本组队处理；
bool CCopyManager::OnPlayerLeaveSTeam( const char* playerRoleName, const PlayerID& playerID, const PlayerGateID& playerGateID )
{
	if ( NULL == playerRoleName )
	{
		D_ERROR( "CCopyManager::OnPlayerLeaveSTeam, NULL == playerRoleName, playerID(%d:%d)\n", playerID.wGID, playerID.dwPID );
		return false;
	}

	if ( playerID != playerGateID.playerID )
	{
		D_ERROR( "CCopyManager::OnPlayerLeaveSTeam, playerID != playerGateID.playerID, player%s(%d:%d)\n", playerRoleName, playerID.wGID, playerID.dwPID );
		return false;
	}

	//若玩家当前在副本中，则应通知对应的副本，准备踢此玩家出副本；
	D_DEBUG( "CCopyManager::OnPlayerLeaveSTeam，处理玩家%s(%d,%d)离开副本组队...\n", playerRoleName, playerID.wGID, playerID.dwPID );

	if ( 0 == playerGateID.playerCopyID )
	{
		D_DEBUG( "CCopyManager::OnPlayerLeaveSTeam，玩家%s当前不在副本中，处理终止，不再向mapsrv发送消息\n", playerRoleName );
		return false;
	}

	D_DEBUG( "CCopyManager::OnPlayerLeaveSTeam，玩家%s(%d:%d)离开副本组队，通知玩家当前所在副本%d\n"
		, playerRoleName, playerID.wGID, playerID.dwPID, playerGateID.playerCopyID );

	EGLeaveSTeamNoti leaveSTeamNoti;
	leaveSTeamNoti.leavePlayerID = playerID;
	leaveSTeamNoti.copyID = playerGateID.playerCopyID;
	//leaveSTeamNoti.mapsrvID = pCopyInfo->copyManagerEle.mapsrvID;
	//leaveSTeamNoti.copyID = pCopyInfo->copyManagerEle.copyID;		

	//找到对应的gatesrv进行发送
	CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( playerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR( "CCopyManager::OnPlayerLeaveSTeam,找不到对应的GATESRV%d\n", playerID.wGID );
		return false;
	}

	MsgToPut* pNewMsg = CreateSrvPkg( EGLeaveSTeamNoti, pSrv, leaveSTeamNoti );
	pSrv->SendPkgToSrv( pNewMsg );

	//map<unsigned int, TeamCopyInfo>::iterator iter = m_teamInfos.find( steamid );
	//if ( iter == m_teamInfos.end() )
	//{
	//	return false;
	//}	

	//bool isFound = false;
	//TeamCopyInfo* teamCopyInfo= &(iter->second);
	//HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	//CenterCopyInfo* pCopyInfo = NULL;
	//for ( unsigned int i=0; i<teamCopyInfo->curCopyNum; ++i )
	//{
	//	if ( !m_AllCopys.FindEle( teamCopyInfo->teamCopyIDs[i], ptrEle ) )
	//	{
	//		D_ERROR( "CCopyManager::OnPlayerLeaveSTeam, 不可能错误，队伍%d的第%d个副本%d在管理器中不存在\n", steamid, i, teamCopyInfo->teamCopyIDs[i] );
	//		continue;
	//	}

	//	if ( NULL == ptrEle ) 
	//	{
	//		D_ERROR( "CCopyManager::OnPlayerLeaveSTeam, 不可能错误，队伍%d的第%d个副本%d取不到对应信息指针\n", steamid, i, teamCopyInfo->teamCopyIDs[i] );
	//		continue;
	//	}

	//	pCopyInfo = ptrEle->GetInnerPtr();
	//	if ( NULL == pCopyInfo ) 
	//	{
	//		D_ERROR( "CCopyManager::OnPlayerLeaveSTeam, 不可能错误，队伍%d的第%d个副本%d取到的副本信息指针空\n", steamid, i, teamCopyInfo->teamCopyIDs[i] );
	//		continue;
	//	}

	//	D_DEBUG( "CCopyManager::OnPlayerLeaveSTeam，玩家(%d:%d)离开副本组队%d，通知组队拥有的副本%d\n", playerID.wGID, playerID.dwPID, steamid, teamCopyInfo->teamCopyIDs[i] );

	//	EGLeaveSTeamNoti leaveSTeamNoti;
	//	leaveSTeamNoti.leavePlayerID = playerID;
	//	leaveSTeamNoti.mapsrvID = pCopyInfo->copyManagerEle.mapsrvID;
	//	leaveSTeamNoti.copyID = pCopyInfo->copyManagerEle.copyID;		
	//	
	//	//找到对应的gatesrv进行发送
	//	CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( playerID.wGID );
	//	if( NULL == pSrv )
	//	{
	//		D_ERROR( "CCopyManager::OnPlayerLeaveSTeam,找不到对应的GATESRV%d\n", playerID.wGID );
	//		return false;
	//	}

	//	MsgToPut* pNewMsg = CreateSrvPkg( EGLeaveSTeamNoti, pSrv, leaveSTeamNoti );
	//	pSrv->SendPkgToSrv( pNewMsg );
	//	
	//}

	return true;
}

///副本组队解散；
bool CCopyManager::OnSTeamDestoryed( unsigned int steamid )
{
	//若该副本组队拥有副本，则应为对应的各副本发删除副本请求；
	D_DEBUG( "CCopyManager::OnSTeamDestoryed，收到副本组队%d销毁通知\n", steamid );

	map<unsigned int, TeamCopyInfo>::iterator iter = m_teamInfos.find( steamid );
	if ( iter == m_teamInfos.end() )
	{
		return false;
	}	

	//bool isFound = false;
	TeamCopyInfo* teamCopyInfo= &(iter->second);
	HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	CenterCopyInfo* pCopyInfo = NULL;
	for ( unsigned int i=0; i<teamCopyInfo->curCopyNum; ++i )
	{
		if ( i >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs) )
		{
			D_ERROR( "CCopyManager::OnSTeamDestoryed, i(%d) >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs), teamID:%d\n", i, steamid );
			break;
		}
		if ( !m_AllCopys.FindEle( teamCopyInfo->teamCopyIDs[i], ptrEle ) )
		{
			D_ERROR( "CCopyManager::OnSTeamDestoryed, 不可能错误，队伍%d的第%d个副本%d在管理器中不存在\n", steamid, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		if ( NULL == ptrEle ) 
		{
			D_ERROR( "CCopyManager::OnSTeamDestoryed, 不可能错误，队伍%d的第%d个副本%d取不到对应信息指针\n", steamid, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		pCopyInfo = ptrEle->GetInnerPtr();
		if ( NULL == pCopyInfo ) 
		{
			D_ERROR( "CCopyManager::OnSTeamDestoryed, 不可能错误，队伍%d的第%d个副本%d取到的副本信息指针空\n", steamid, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		D_DEBUG( "CCopyManager::OnSTeamDestoryed，解散的副本组队%d拥有副本%d，发起删除此副本\n", steamid, teamCopyInfo->teamCopyIDs[i] );

		EGDelCopyQuest delCopyQuest;
		delCopyQuest.copyInfo = pCopyInfo->copyManagerEle;
		
		//找到对应的gatesrv进行发送
		CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( delCopyQuest.copyInfo.enterReq.playerID.wGID );
		if( NULL == pSrv )
		{
			D_ERROR( "CCopyManager::OnSTeamDestoryed,找不到对应的GATESRV%d\n", teamCopyInfo->teamCopyIDs[i] );
			return false;
		}

		MsgToPut* pNewMsg = CreateSrvPkg( EGDelCopyQuest, pSrv, delCopyQuest );
		pSrv->SendPkgToSrv( pNewMsg );
		
	}

	return true;
}

///玩家重置副本，是否队长已在gate检测
bool CCopyManager::OnResetCopy( const PlayerID& captainPlayerID, unsigned int steamID, unsigned short copyMapID )
{
	//若该副本组队拥有副本，则应为对应的各副本发删除副本请求；
	D_DEBUG( "CCopyManager::OnResetCopy，收到玩家(%d:%d)重置组队%d副本请求，类型%d\n"
		, captainPlayerID.wGID, captainPlayerID.dwPID, steamID, copyMapID );

	map<unsigned int, TeamCopyInfo>::iterator iter = m_teamInfos.find( steamID );
	if ( iter == m_teamInfos.end() )
	{
		//玩家所在组队不拥有任何副本；
		D_WARNING( "CCopyManager::OnResetCopy，玩家(%d:%d)所在组队%d不拥有任何副本，重置失败\n"
		, captainPlayerID.wGID, captainPlayerID.dwPID, steamID );

		NotiPlayerSystemWarning( captainPlayerID, "当前没有可以重置的副本" );

		return false;
	}	

	bool isFound = false;
	bool isResetOK = false;
	TeamCopyInfo* teamCopyInfo= &(iter->second);
	HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	CenterCopyInfo* pCopyInfo = NULL;
	for ( unsigned int i=0; i<teamCopyInfo->curCopyNum; ++i )
	{
		if ( i >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs) )
		{
			D_ERROR( "CCopyManager::OnResetCopy, i(%d) >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs)\n", i );
			break;
		}

		if ( !m_AllCopys.FindEle( teamCopyInfo->teamCopyIDs[i], ptrEle ) )
		{
			D_ERROR( "CCopyManager::OnResetCopy, 不可能错误，队伍%d的第%d个副本%d在管理器中不存在\n", steamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		if ( NULL == ptrEle ) 
		{
			D_ERROR( "CCopyManager::OnResetCopy, 不可能错误，队伍%d的第%d个副本%d取不到对应信息指针\n", steamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		pCopyInfo = ptrEle->GetInnerPtr();
		if ( NULL == pCopyInfo ) 
		{
			D_ERROR( "CCopyManager::OnResetCopy, 不可能错误，队伍%d的第%d个副本%d取到的副本信息指针空\n", steamID, i, teamCopyInfo->teamCopyIDs[i] );
			continue;
		}

		if ( copyMapID != pCopyInfo->copyManagerEle.enterReq.copyMapID )
		{
			continue;
		}

		if ( (0 != pCopyInfo->deler.wGID) 
			|| (0 != pCopyInfo->deler.dwPID) 
			)
		{
			D_WARNING( "CCopyManager::OnResetCopy，副本%d之前已有删除者(%d:%d)\n"
				, pCopyInfo->copyManagerEle.copyID, pCopyInfo->deler.wGID, pCopyInfo->deler.dwPID );
			return false;
		}

		isFound = true;
		D_DEBUG( "CCopyManager::OnResetCopy，找到了待重置的副本%d\n", pCopyInfo->copyManagerEle.copyID );
		if ( pCopyInfo->copyManagerEle.curPlayerNum > 0 )
		{
			D_WARNING( "副本%d当前人数%d大于0，不可重置\n", pCopyInfo->copyManagerEle.copyID, pCopyInfo->copyManagerEle.curPlayerNum );
			NotiPlayerSystemWarning( captainPlayerID, "该副本中还有队友，不可重置" );
			return false;
		}

		D_DEBUG( "CCopyManager::OnResetCopy，副本%d重置条件满足，准备删除之\n", pCopyInfo->copyManagerEle.copyID );

		EGDelCopyQuest delCopyQuest;
		delCopyQuest.copyInfo = pCopyInfo->copyManagerEle;
		
		//找到对应的gatesrv进行发送
		CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( delCopyQuest.copyInfo.enterReq.playerID.wGID );
		if( NULL == pSrv )
		{
			D_ERROR( "CCopyManager::OnResetCopy,找不到对应的GATESRV%d\n", teamCopyInfo->teamCopyIDs[i] );
			return false;
		}

		MsgToPut* pNewMsg = CreateSrvPkg( EGDelCopyQuest, pSrv, delCopyQuest );
		pSrv->SendPkgToSrv( pNewMsg );

		isResetOK = true;
		pCopyInfo->deler.wGID = captainPlayerID.wGID;//记录删除者以便稍后真正删除时通知；
		pCopyInfo->deler.dwPID = captainPlayerID.dwPID;
		NotiPlayerSystemWarning( captainPlayerID, "重置副本成功" );

		D_DEBUG( "重置副本%d成功，记录副本删除者(%d:%d)", pCopyInfo->copyManagerEle.copyID, pCopyInfo->deler.wGID, pCopyInfo->deler.dwPID );

		break;
	}

	if ( !isFound )
	{
		D_WARNING( "CCopyManager::OnResetCopy，玩家(%d:%d)重置组队%d副本请求，当前不拥有此类型%d副本\n"
			, captainPlayerID.wGID, captainPlayerID.dwPID, steamID, copyMapID );
		NotiPlayerSystemWarning( captainPlayerID, "您不拥有此类型副本，不可重置" );
		return false;
	}

	return true;
}

///玩家进入旧副本；
bool CCopyManager::EnterOldCopy( const EnterCopyReq& enterReq, CenterCopyInfo* orgCopyInfo )
{
	//2、如有找到，副本人数++;
	//4、在玩家身上置副本号标记，通知gate玩家进入副本，gate置副本信息后，两头发跳转地图通知;

	if ( NULL == orgCopyInfo )
	{
		D_ERROR( "CCopyManager::EnterOldCopy，输入参数空\n" );
		return false;
	}

	if ( orgCopyInfo->copyManagerEle.curPlayerNum >= COPY_PLAYER_MAXNUM )
	{
		D_WARNING( "CCopyManager::EnterOldCopy，%s欲进副本%d人数已满\n", enterReq.szAccount, orgCopyInfo->copyManagerEle.copyID );
		NotiPlayerSystemWarning( enterReq.playerID, "您欲进入的副本人数已满" );
		return false;
	}

	if ( orgCopyInfo->copyManagerEle.isNoMorePlayer )
	{
		D_WARNING( "CCopyManager::EnterOldCopy，%s欲进的副本%d已设置不可再进标记\n", enterReq.szAccount, orgCopyInfo->copyManagerEle.copyID );
		NotiPlayerSystemWarning( enterReq.playerID, "您欲进入的副本已不可再进" );
		return false;
	}

	//如果玩家之前进过此副本，则不需要副本券，否则需要；
	unsigned int needScrollNum = 1;//默认需要1个副本券
	bool isOrgEnterThis = false;
	unsigned int dumpparam = 0;//不管不同类型副本券了，因此为无效参数，暂时保留
	orgCopyInfo->everEntered.PlayerEnterCheck( enterReq.playerID, dumpparam, isOrgEnterThis );
	if ( !isOrgEnterThis )
	{
		needScrollNum = 1;//原来没进过此副本，需要扣券；
		if ( enterReq.scrollType <= 0 )
		{
			D_DEBUG( "玩家%s尝试进旧副本%d(mapid:%d)时，没有足够副本券\n"
				, enterReq.szAccount, orgCopyInfo->copyManagerEle.copyID, orgCopyInfo->copyManagerEle.enterReq.copyMapID );
			NotiPlayerSystemWarning( enterReq.playerID, "您没有足够的副本券" );
			return false;
		}		
	} else {
		needScrollNum = 0;//原来进过此副本，无需扣券；
	}

	D_DEBUG( "玩家%s尝试进旧副本%d(mapid:%d),teamid:%d\n", enterReq.szAccount, orgCopyInfo->copyManagerEle.copyID, enterReq.copyMapID, enterReq.teamID );

	NotiPlayerEnterCopy( enterReq, orgCopyInfo, needScrollNum );//通知玩家进副本；

	return true;
}

///通知玩家系统警告消息
bool CCopyManager::NotiPlayerSystemWarning( const PlayerID& playerID, const char* systemWarning )
{
	//找到对应的gatesrv进行发送
	CGateSrv* pGateSrv = CDealGateSrvPkg::FindGateSrv( playerID.wGID );
	if ( NULL == pGateSrv )
	{
		D_ERROR( "NotiPlayerSystemWarning, 玩家(%d:%d)，找不到该玩家所在gate，警告信息%s\n", playerID.wGID, playerID.dwPID, systemWarning );
		return false;
	}

	EGPrivateChat gateMsg;
	StructMemSet( gateMsg, 0, sizeof( EGPrivateChat ) );
	gateMsg.chatType = CT_SYS_WARNING;
	gateMsg.tgtPlayerID = playerID;
	SafeStrCpy( gateMsg.chatContent, systemWarning );

	MsgToPut* pGateMsg = CreateSrvPkg( EGPrivateChat, pGateSrv, gateMsg );
	pGateSrv->SendPkgToSrv( pGateMsg );

	return true;
}

///玩家创建新副本；
bool CCopyManager::EnterNewCopy( const EnterCopyReq& enterReq )
{
	//新建副本必须拥有副本券，同时进副本命令中必须指令扣副本券
	if ( enterReq.scrollType <= 0 )
	{
		//玩家当前副本券数目不够；
		D_DEBUG( "CCopyManager::EnterNewCopy, 玩家%s建新副本%d，副本券不够\n", enterReq.szAccount, enterReq.copyMapID );

		NotiPlayerSystemWarning( enterReq.playerID, "您没有足够的副本券" );

		return false;
	}

	//3、如未找到，新建副本，修改各容器管理器，选玩家所在mapsrv通知其创建新副本;
	//4、在玩家身上置副本号标记，通知gate玩家进入副本，gate置副本信息后，两头发跳转地图通知;
	CenterCopyInfo* pNewCopyInfo = NEW CenterCopyInfo;
	if ( NULL == pNewCopyInfo )
	{
		D_ERROR( "CCopyManager::EnterNewCopy, NULL == pNewCopyInfo\n" );
		NotiPlayerSystemWarning( enterReq.playerID, "异常错误0，进入副本失败" );
		return false;
	}

	pNewCopyInfo->everEntered.InitCopyPlayerInfos();//初始化为没有一个人进过此副本；
	pNewCopyInfo->deler.wGID = 0;//初始无删除者；
	pNewCopyInfo->deler.dwPID = 0;

	++g_CopyIDGener;
	if ( 0 == g_CopyIDGener )
	{
		g_CopyIDGener = 1;
	}
	//HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	bool isValidCopyID = false;//假设为冲突的副本ID号；
	for ( int i=0; i<500; ++i )
	{
		//试500次，500次都协调不好，认为全服不可再建新副本；
		if ( m_AllCopys.IsHashPosEmpty( g_CopyIDGener ) )
		{
			isValidCopyID = true;
			break;
		} else {
			++g_CopyIDGener;
			if ( 0 == g_CopyIDGener )
			{
				g_CopyIDGener = 1;
			}
			continue;
		}
	}

	if ( !isValidCopyID )
	{
		D_ERROR( "全服副本数已达极限，当前副本数目%d\n", m_AllCopys.GetEleNum() );
		delete pNewCopyInfo; pNewCopyInfo = NULL;
		NotiPlayerSystemWarning( enterReq.playerID, "暂时不可进副本，请稍后再试" );
		return false;
	}

	unsigned short toUseCopyMapSrvID = 0;

	if ( !SelectOneCopyMapSrv(toUseCopyMapSrvID) )
	{
		D_ERROR( "选择副本地图服务器失败\n" );
		delete pNewCopyInfo; pNewCopyInfo = NULL;
		NotiPlayerSystemWarning( enterReq.playerID, "没有可用的副本信息，进副本失败" );
		return false;
	}

	pNewCopyInfo->copyManagerEle.mapsrvID = toUseCopyMapSrvID;
	pNewCopyInfo->copyManagerEle.isNoMorePlayer = false;
	pNewCopyInfo->copyManagerEle.copyID = g_CopyIDGener;
	pNewCopyInfo->copyManagerEle.createTime = GetTickCount();
	pNewCopyInfo->copyManagerEle.curPlayerNum = 0;
	pNewCopyInfo->copyManagerEle.enterReq = enterReq;

	//修改队伍拥有副本信息；
	map<unsigned int, TeamCopyInfo>::iterator titer = m_teamInfos.find( enterReq.teamID );
	if ( titer == m_teamInfos.end() )
	{
		//该队伍的第一个副本
		TeamCopyInfo tmpTeamCopyInfo;
		tmpTeamCopyInfo.curCopyNum = 1;
		if ( tmpTeamCopyInfo.curCopyNum-1 >= ARRAY_SIZE(tmpTeamCopyInfo.teamCopyIDs) )
		{
			D_ERROR( "CCopyManager::EnterNewCopy, tmpTeamCopyInfo.curCopyNum(%d)-1 >= ARRAY_SIZE(tmpTeamCopyInfo.teamCopyIDs)\n", tmpTeamCopyInfo.curCopyNum );
			return false;
		}
		tmpTeamCopyInfo.teamCopyIDs[tmpTeamCopyInfo.curCopyNum-1] = pNewCopyInfo->copyManagerEle.copyID;
		m_teamInfos.insert( pair<unsigned int, TeamCopyInfo>( enterReq.teamID, tmpTeamCopyInfo ) ); 
	} else {
		//该队伍之前已有其它副本，修改该队伍的拥有副本信息；
		if ( titer->second.curCopyNum >= ARRAY_SIZE(titer->second.teamCopyIDs) )
		{
			D_ERROR( "CCopyManager::EnterNewCopy, 队伍%d拥有副本数超限%d\n", enterReq.teamID, ARRAY_SIZE(titer->second.teamCopyIDs) );
			delete pNewCopyInfo; pNewCopyInfo = NULL;
			NotiPlayerSystemWarning( enterReq.playerID, "队伍拥有的副本数超过限额，请先退出其它副本" );
			return false;
		}
		++(titer->second.curCopyNum);
		if ( titer->second.curCopyNum-1 >= ARRAY_SIZE(titer->second.teamCopyIDs) )
		{
			D_ERROR( "CCopyManager::EnterNewCopy, titer->second.curCopyNum(%d)-1 >= ARRAY_SIZE(tmpTeamCopyInfo.teamCopyIDs)\n", titer->second.curCopyNum );
			return false;
		}
		titer->second.teamCopyIDs[titer->second.curCopyNum-1] = pNewCopyInfo->copyManagerEle.copyID;
	}

	//修改各副本地图各自创建的平行空间数数值；
	map<unsigned short, unsigned int>::iterator miter = m_mapCopyNums.find( pNewCopyInfo->copyManagerEle.enterReq.copyMapID );
	if ( miter == m_mapCopyNums.end() )
	{
		//该副本地图的第一个副本；
		m_mapCopyNums.insert( pair<unsigned short, unsigned int>( pNewCopyInfo->copyManagerEle.enterReq.copyMapID, 1 ) );//第一个副本；
		m_globeCopyInfos.SetMapIDNum( pNewCopyInfo->copyManagerEle.enterReq.copyMapID, 1 );
	} else {
		//原来就有该副本地图的空间；
		++(miter->second);//增加该副本地图的空间数；
		m_globeCopyInfos.SetMapIDNum( miter->first, miter->second );
	}

	//副本加入全局管理器；
	m_AllCopys.PushEle( HashTbPtrEle<CenterCopyInfo>( pNewCopyInfo->copyManagerEle.copyID, pNewCopyInfo ) );

	if ( !CopyMapSrvAddOneCopy(toUseCopyMapSrvID) )
	{
		D_ERROR( "设置副本地图服务器%d添加副本失败\n", toUseCopyMapSrvID );
	}

	//找到对应的gatesrv进行发送
	CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( pNewCopyInfo->copyManagerEle.enterReq.playerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR( "CCopyManager::EnterNewCopy,找不到对应的GATESRV%d\n", pNewCopyInfo->copyManagerEle.enterReq.playerID.wGID );
		NotiPlayerSystemWarning( enterReq.playerID, "异常错误1，进入副本失败" );
		return false;
	}

	D_DEBUG( "center create new copy %d, team %d, mapid %d, send cmd to mapsrv %d\n"
		, pNewCopyInfo->copyManagerEle.copyID, pNewCopyInfo->copyManagerEle.enterReq.teamID
		, pNewCopyInfo->copyManagerEle.enterReq.copyMapID, pNewCopyInfo->copyManagerEle.mapsrvID );

	EGNewCopyCmd newCopy;//指令创建新副本；
	newCopy.copyInfo = pNewCopyInfo->copyManagerEle;
	MsgToPut* pNewMsg = CreateSrvPkg( EGNewCopyCmd, pSrv, newCopy );
	pSrv->SendPkgToSrv( pNewMsg );

	bool isOrgEnterThis = false;
	unsigned int dumpparam = 0;//不管不同类型副本券了，因此为无效参数，暂时保留
	pNewCopyInfo->everEntered.PlayerEnterCheck( enterReq.playerID, dumpparam, isOrgEnterThis/*必定为false*/ );//记录玩家曾进过此副本；
	if ( isOrgEnterThis )
	{
		D_ERROR( "玩家%s新进副本%d(mapid:%d)，但isOrgEnterThis==true\n"
			, enterReq.szAccount, pNewCopyInfo->copyManagerEle.copyID, pNewCopyInfo->copyManagerEle.enterReq.copyMapID );
	}

	NotiPlayerEnterCopy( pNewCopyInfo->copyManagerEle.enterReq, pNewCopyInfo, 1/*由于是新进副本，因此固定扣1个副本券*/ );

	return true;
}

///删除旧副本；
bool CCopyManager::DelOldCopy( unsigned int copyID )
{
	//1、找到指定副本;
	//2、删去该副本;
	HashTbPtrEle<CenterCopyInfo>* ptrEle = NULL;
	if ( !(m_AllCopys.FindEle( copyID, ptrEle )) )
	{
		D_ERROR( "CCopyManager::DelOldCopy, 找不到指定的待删副本%d\n", copyID );
		return false;
	}

	if ( NULL == ptrEle )
	{
		D_ERROR ( "CCopyManager::DelOldCopy， NULL == ptrEle\n" );
		return false;
	}

	CenterCopyInfo* pCopyInfo = ptrEle->GetInnerPtr();
	if ( NULL == pCopyInfo )
	{
		D_ERROR ( "CCopyManager::DelOldCopy， NULL == pCopyInfo\n" );
		return false;
	}

	if ( pCopyInfo->copyManagerEle.curPlayerNum > 0 )
	{
		/*正常情况下不可能在收到mapsrv删自身请求时发现副本中还有人。
		但也有极小概率情形为：之前centersrv的playerA进入消息还未到mapsrv(此时center playernum > 0)，
		而mapsrv的副本检测到删除条件已满足(比如已空闲达一定秒数)，因此发来了删除副本自身请求，到center后就将进到此处。
		在此情形下，稍后，playerA的进副本过程将在mapsrv失败(因为mapsrv已发删自身请求，因此不会让playerA进副本)并被踢下线，
		而centersrv收到playerA下线消息时会减pCopyInfo->curPlayerNum至0。
		为了处理这种情况，有必要之后在centersrv不断检测该副本人数，直至副本人数为0后，继续此删副本过程；
		*/
		pair< set<unsigned int>::iterator, bool > pr = m_toDelCopyWaitPlayerNum0.insert( copyID );
		if ( pr.second )
		{
			D_ERROR ( "CCopyManager::DelOldCopy， pCopyInfo->copyManagerEle.curPlayerNum > 0, copyid%d\n", copyID );
		}
		return false;
	}

	//修改本队伍拥有的副本信息
	map<unsigned int, TeamCopyInfo>::iterator iter = m_teamInfos.find( pCopyInfo->copyManagerEle.enterReq.teamID );
	if ( iter == m_teamInfos.end() )
	{
		D_ERROR( "CCopyManager::DelOldCopy，所在队伍%d不拥有任何副本\n", pCopyInfo->copyManagerEle.enterReq.teamID );
	} else {
		bool isFound = false;
		unsigned int foundPos = 0;
		TeamCopyInfo* teamCopyInfo= &(iter->second);
		if ( teamCopyInfo->curCopyNum <= 0 )
		{
			D_ERROR( "CCopyManager::DelOldCopy，所在队伍%d拥有的副本数量为0\n", pCopyInfo->copyManagerEle.enterReq.teamID );
			return false;
		}
		for ( unsigned int i=0; i<teamCopyInfo->curCopyNum; ++i )
		{
			if ( i >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs) )
			{
				D_ERROR( "CCopyManager::DelOldCopy, i(%d) >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs), teamID:%d\n", i, pCopyInfo->copyManagerEle.enterReq.teamID );
				break;
			}
			if ( pCopyInfo->copyManagerEle.copyID == teamCopyInfo->teamCopyIDs[i] )
			{
				isFound = true;
				foundPos = i;
				break;
			}
		}
		if ( isFound )
		{
			if ( ( foundPos >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs) )
				|| ( teamCopyInfo->curCopyNum-1 >= ARRAY_SIZE(teamCopyInfo->teamCopyIDs) ) 
				)
			{
				D_ERROR( "CCopyManager::DelOldCopy，foundPos(%d)或teamCopyInfo->curCopyNum(%d)-1越界\n", foundPos, teamCopyInfo->curCopyNum );
				return false;
			}
			teamCopyInfo->teamCopyIDs[foundPos] = teamCopyInfo->teamCopyIDs[teamCopyInfo->curCopyNum-1];//删去拥有的此副本
			--teamCopyInfo->curCopyNum;
			if ( teamCopyInfo->curCopyNum <= 0 )
			{
				m_teamInfos.erase( pCopyInfo->copyManagerEle.enterReq.teamID );
			}
		} else {
			D_ERROR( "CCopyManager::DelOldCopy，所在队伍%d不拥有副本地图%d", pCopyInfo->copyManagerEle.enterReq.teamID, pCopyInfo->copyManagerEle.copyID );
		}
	}

	//修改各副本地图各自创建的平行空间数数值；
	map<unsigned short, unsigned int>::iterator miter = m_mapCopyNums.find( pCopyInfo->copyManagerEle.enterReq.copyMapID );
	if ( miter == m_mapCopyNums.end() )
	{
		D_ERROR( "不可能错误，删副本%d时m_mapCopyNums中没有对应mapid%d的项\n"
			, pCopyInfo->copyManagerEle.copyID, pCopyInfo->copyManagerEle.enterReq.copyMapID );
	} else {
		//原来就有该副本地图的空间；
		if ( miter->second <= 0 )
		{
			D_ERROR( "不可能错误，删副本%d时m_mapCopyNums中对应mapid%d的项值为%d\n"
				, pCopyInfo->copyManagerEle.copyID, pCopyInfo->copyManagerEle.enterReq.copyMapID, miter->second );
		} else {
			--(miter->second);//减该副本地图的空间数；
			m_globeCopyInfos.SetMapIDNum( miter->first, miter->second );
			if ( miter->second <= 0 )
			{
				D_DEBUG( "删副本%d后，m_mapCopyNums中对应mapid%d的项值为0，删去此项\n"
					, pCopyInfo->copyManagerEle.copyID, pCopyInfo->copyManagerEle.enterReq.copyMapID );
				m_mapCopyNums.erase( miter );
			}
		}
	}

	CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( pCopyInfo->copyManagerEle.enterReq.playerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR( "CCopyManager::DelOldCopy,找不到对应的GATESRV%d, 副本%d\n"
			, pCopyInfo->copyManagerEle.enterReq.playerID.wGID, pCopyInfo->copyManagerEle.copyID );
		return false;
	}

	EGDelCopyCmd delCopyCmd;
	delCopyCmd.copyInfo = pCopyInfo->copyManagerEle;

	MsgToPut* pNewMsg = CreateSrvPkg( EGDelCopyCmd, pSrv, delCopyCmd );
	pSrv->SendPkgToSrv( pNewMsg );

	D_DEBUG( "copy %d deled, send del cmd to mapsrv\n", copyID );

	if ( !CopyMapSrvReleaseOneCopy(delCopyCmd.copyInfo.mapsrvID) )
	{
		D_ERROR( "设置副本地图服务器%d释放副本失败\n", delCopyCmd.copyInfo.mapsrvID );
	}

	if ( (0 != pCopyInfo->deler.wGID)
		|| (0 != pCopyInfo->deler.dwPID)
		)
	{
		//通知删除副本者，新的组队副本信息；
		SendTeamCopyInfoToPlayer( pCopyInfo->deler, pCopyInfo->copyManagerEle.enterReq.teamID );
		D_DEBUG( "通知玩家(%d:%d)，组队%d新的拥有副本\n", pCopyInfo->deler.wGID, pCopyInfo->deler.dwPID, pCopyInfo->copyManagerEle.enterReq.teamID );
	}

	delete pCopyInfo; pCopyInfo = NULL;
	m_AllCopys.DelEle( copyID );

	return true;
}

///通知玩家进副本；
bool CCopyManager::NotiPlayerEnterCopy( const EnterCopyReq& enterReq, CenterCopyInfo* copyInfo, unsigned int needScrollNum/*需要的副本券数量*/ )
{
	if ( NULL == copyInfo )
	{
		D_ERROR( "CCopyManager::NotiPlayerEnterCopy，NULL == copyInfo\n" );
		return false;
	}

	PlayerGateID* playerGateID = CDealGateSrvPkg::FindOnlinePlayerGateIDByAccount( enterReq.szAccount );
	if ( NULL == playerGateID )
	{
		D_ERROR( "CCopyManager::NotiPlayerEnterCopy，找不到对应的PlayerGateID,玩家%s\n", enterReq.szAccount );
		return false;
	}
	playerGateID->playerCopyID = copyInfo->copyManagerEle.copyID;//在玩家身上记录其当前所在的副本号，以便下线时减副本人数；

	++(copyInfo->copyManagerEle.curPlayerNum);//副本人数++;

	//找到对应的gatesrv进行发送
	CGateSrv* pSrv = CDealGateSrvPkg::FindGateSrv( enterReq.playerID.wGID );
	if( NULL == pSrv )
	{
		D_ERROR( "CCopyManager::EnterNewCopy,找不到对应的GATESRV%d\n", copyInfo->copyManagerEle.enterReq.playerID.wGID );
		return false;
	}

	D_DEBUG( "player %s enter copy %d, copy playernum %d, send enter cmd to gate\n"
		, enterReq.szAccount, copyInfo->copyManagerEle.copyID, copyInfo->copyManagerEle.curPlayerNum );

	EGEnterCopyCmd enterCopy;//指令玩家进入此新副本；
	enterCopy.scrollType = needScrollNum;//需要的副本券数量；
	enterCopy.enterPlayerID = enterReq.playerID;
	enterCopy.copyInfo = copyInfo->copyManagerEle;
	MsgToPut* pNewMsg = CreateSrvPkg( EGEnterCopyCmd, pSrv, enterCopy );
	pSrv->SendPkgToSrv( pNewMsg );

	return true;
}

///通知玩家离开副本；
bool CCopyManager::NotiPlayerLeaveCopy( const PlayerID& playerID, const CenterCopyInfo* orgCopyInfo )
{
	ACE_UNUSED_ARG( playerID );
	ACE_UNUSED_ARG( orgCopyInfo );
	return true;
}

