﻿/********************************************************************
	created:	2010/03/17
	created:	17:3:2010   16:58
	file:		AuctionBase.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef AUCTIONBASE_H_ 
#define AUCTIONBASE_H_

#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/PkgProc/DealPkgBase.h"
#include "../../Base/PkgProc/MsgToPut.h"
#include "../../Base/PkgProc/PacketBuild.h"
#include "../../Base/MysqlWrapper.h"
#include "OtherServer.h"
#include "GateSrvManager.h"
using namespace MUX_PROTO;


struct AuctionItemCSInfo
{
	unsigned int  auctionID;//拍卖的物品编号
	ItemInfo_i    auctionItem;//道具信息
	unsigned int  fixprice;//一口价
	unsigned int  curauctionprice;//当前的拍卖价格
	unsigned int  origiprice;//原始价钱  
	unsigned int  expiretime;//拍卖过期时间

	AuctionItemCSInfo()
	{
		auctionID = fixprice = curauctionprice = origiprice = expiretime = 0;
		auctionItem.ucCount =  auctionItem.ucLevelUp =  auctionItem.uiID = auctionItem.randSet/*usAddId*/ = auctionItem.usItemTypeID = auctionItem.usWearPoint = 0;
	}

	AuctionItemCSInfo& operator=( const AuctionItemCSInfo& itemCSInfo )
	{
		if ( this == &itemCSInfo )
			return *this;

		auctionID =  itemCSInfo.auctionID;
		auctionItem = itemCSInfo.auctionItem;
		fixprice =  itemCSInfo.fixprice;
		curauctionprice =  itemCSInfo.curauctionprice;
		origiprice  = itemCSInfo.origiprice;
		expiretime  = itemCSInfo.expiretime;
		return *this;
	}
	
};

#endif