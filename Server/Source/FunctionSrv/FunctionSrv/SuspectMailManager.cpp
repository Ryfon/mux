﻿
#include "SuspectMailManager.h"
#include "MailIDGenerator.h"
#include "PlayerMail.h"

#include "Task/TaskPool.h"
#include "Task/InsertMailTask.h"
#include "GateSrvManager.h"

CSupectMailManager::~CSupectMailManager()
{
	std::vector<SuspectMail>::iterator lter = mSuspectMailVec.begin();
	for ( ;lter != mSuspectMailVec.end(); ++lter )
	{
		SuspectMail& supectMail = *lter;
		if ( supectMail.pMail )
			delete supectMail.pMail;
	}
	mSuspectMailVec.clear();
}


bool CSupectMailManager::CreateSuspectNormalMail(unsigned int sessionID, PlayerID& playerID, const char* pMailRecvPlayerName, unsigned int sendPlayerUID,const char* pMailSendPlayerName, const char* pMailTitle, const char* pMailContent )
{
	unsigned int mailUID    = MailIDGeneratorSingle::instance()->GetUnqiueMailID();
	CNormalMail* pNormalMail = NEW CNormalMail( mailUID, false,0 , 0,sendPlayerUID,pMailSendPlayerName, pMailTitle, pMailContent );
	if ( pNormalMail )
	{
		SuspectMail supectMail;
		supectMail.sendPlayerID = playerID;
		supectMail.pMail = pNormalMail;
		supectMail.sessionID = sessionID;
		ACE_OS::strncpy( supectMail.recvMailPlayerName , pMailRecvPlayerName, MAX_NAME_SIZE );
		mSuspectMailVec.push_back( supectMail );
		
		//邮件编号+1
		MailIDGeneratorSingle::instance()->IncMailID();

		//查询接受邮件的玩家的信息
		QueryRecvMailPlayerInfo( mailUID, pMailRecvPlayerName );

		return true;
	}

	return false;
}

bool CSupectMailManager::CreateSuspectSystemMail(unsigned int sessionID, const char* pMailRecvPlayerName, const char* pMailTitle, const char* pMailContent, int money, ItemInfo_i* pItemArr )
{
	unsigned int mailUID = MailIDGeneratorSingle::instance()->GetUnqiueMailID();
	CSystemMail* pSystemMail  = NEW CSystemMail( mailUID, false, 0, pMailTitle, pMailContent, money, pItemArr );
	if ( pSystemMail )
	{
		SuspectMail supectMail;
		supectMail.sendPlayerID.wGID = 0;//系统邮件无发信者;
		supectMail.sendPlayerID.dwPID = 0;
		supectMail.pMail = (CNormalMail *)pSystemMail;
		supectMail.sessionID = sessionID;
		ACE_OS::strncpy( supectMail.recvMailPlayerName , pMailRecvPlayerName, MAX_NAME_SIZE );
		mSuspectMailVec.push_back( supectMail );

		//邮件编号+1
		MailIDGeneratorSingle::instance()->IncMailID();

		//查询接受邮件的玩家的信息
		QueryRecvMailPlayerInfo( mailUID, pMailRecvPlayerName );
		return true;
	}
	return false;
}

void CSupectMailManager::QueryRecvMailPlayerInfo(unsigned int mailuid, const char* pRecvPlayerName )
{
	if ( !pRecvPlayerName )
		return;

	FGQueryRecvMailPlayerInfo queryInfo;
	StructMemSet( queryInfo, 0x0, sizeof(FGQueryRecvMailPlayerInfo) );
	queryInfo.mailUID  = mailuid;
	ACE_OS::strncpy( queryInfo.recvPlayerName, pRecvPlayerName , MAX_NAME_SIZE );

	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if ( pGateSrv )
	{
		MsgToPut *pMsgToPut = CreateSrvPkg(FGQueryRecvMailPlayerInfo,pGateSrv,queryInfo );
		pGateSrv->SendPkgToSrv(pMsgToPut);
	}
}


bool CSupectMailManager::RemoveSuspectMail( unsigned int mailuid, SuspectMail& supectMail )
{
	if ( mSuspectMailVec.empty() )
		return false;

	std::vector<SuspectMail>::iterator lter = mSuspectMailVec.begin();
	for (; lter != mSuspectMailVec.end(); ++lter )
	{
		const SuspectMail& mail = *lter;
		if (  mail.pMail && mail.pMail->GetMailUID() == mailuid )
		{
			supectMail = mail;
			mSuspectMailVec.erase( lter );
			return true;
		}
	}
	return false;
}

void CSupectMailManager::DeleteSuspectMail(unsigned int mailuid )
{
	if ( mSuspectMailVec.empty() )
		return;

	std::vector<SuspectMail>::iterator lter = mSuspectMailVec.begin();
	for ( ;lter != mSuspectMailVec.end(); ++lter )
	{
		SuspectMail& Mail = *lter;
		if ( Mail.pMail && Mail.pMail->GetMailUID() == mailuid )
		{
			mSuspectMailVec.erase( lter );
			delete Mail.pMail;
			return;
		}
	}
}

void CSupectMailManager::AffairMail(unsigned int mailuid , unsigned int recvplayeruid, bool isExist )
{

	//从未被确认的邮件列表中删除,并获取邮件信息
	SuspectMail supectMail;
	bool isRemoveOK = RemoveSuspectMail( mailuid ,supectMail );
	if ( !isRemoveOK )
	{
		D_ERROR( "CSupectMailManager::AffairMail，RemoveSuspectMail失败, mailuid:%d, recvplayeruid:%d\n", mailuid, recvplayeruid );
		return;
	}

	////找到通知的GateSrv
	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetGateSrvBySessionID( supectMail.sessionID );
	if ( NULL == pGateSrv )
	{
		D_ERROR( "CSupectMailManager::AffairMail，NULL == pGateSrv, mailuid:%d, recvplayeruid:%d\n", mailuid, recvplayeruid );
		if( supectMail.pMail )
		{
			delete supectMail.pMail;
		}
		return;
	}

	if ( !isExist )
	{
		if ( supectMail.pMail )
		{
			delete supectMail.pMail; supectMail.pMail = NULL;
		}

		if ( ( 0 != supectMail.sendPlayerID.wGID ) || ( 0 != supectMail.sendPlayerID.dwPID ) )
		{
			//非系统邮件；
			FGSendMailFail sendMailFail;
			sendMailFail.errNo = E_PLAYER_NOEXIST;
			sendMailFail.sendPlayerID = supectMail.sendPlayerID;
			//通知client接受邮件失败
			MsgToPut *pMsgToPut = CreateSrvPkg(FGSendMailFail,pGateSrv,sendMailFail );
			pGateSrv->SendPkgToSrv(pMsgToPut);
			D_ERROR( "CSupectMailManager::AffairMail，收信人不存在，通知发信者(%d:%d), 邮件发送失败, mailuid:%d, recvplayeruid:%d\n"
				, sendMailFail.sendPlayerID.wGID, sendMailFail.sendPlayerID.dwPID, mailuid, recvplayeruid );
		} else {
			D_ERROR( "CSupectMailManager::AffairMail，收信人不存在，系统邮件发送失败, mailuid:%d, recvplayeruid:%d\n", mailuid, recvplayeruid );
		}
		return;
	}

	//如果玩家存在,则该邮件合法

	//邮件发送失败的消息通知(如果邮箱满，则发送仍可能失败)
	FGSendMailFail sendMailFail;
	sendMailFail.sendPlayerID = supectMail.sendPlayerID;

	CNormalMail* pMail = supectMail.pMail;
	if ( NULL == pMail )
	{
		D_ERROR( "CSupectMailManager::AffairMail, NULL == pMail, mailuid:%d, recvplayeruid:%d\n", mailuid, recvplayeruid );
		return;
	}

	//邮件身份被确认
	pMail->MailBeAffirm(  recvplayeruid );

	FGSendMailSuccNotifyGM srvmsg;
	StructMemSet(srvmsg, 0x0, sizeof(srvmsg) );
	srvmsg.mailUID = mailuid;
	srvmsg.recvPlayerUID = recvplayeruid;
	SafeStrCpy( srvmsg.sendMailName,  pMail->GetSendPlayerName() );
	//通知client接受邮件失败
	MsgToPut *pMsgToPut = CreateSrvPkg(FGSendMailSuccNotifyGM,pGateSrv,srvmsg );
	pGateSrv->SendPkgToSrv(pMsgToPut);

	bool isSessionExist = false;
	//如果玩家在邮件管理器中
	CPlayerMailSession* pPlayerMailSession = PlayerMailSessionManagerSingle::instance()->FindPlayerMailSession( recvplayeruid );
	//将邮件放入玩家的邮件管理器中
	if ( pPlayerMailSession )
	{
		isSessionExist = true;
		//玩家在线,那么如果接受邮件失败,通知发送玩家
		if( !pPlayerMailSession->GetNewMail( pMail ) )
		{
			//邮箱满了
			sendMailFail.errNo = E_PLAYER_MAIL_FULL;

			if ( ( 0 != sendMailFail.sendPlayerID.wGID ) || ( 0 != sendMailFail.sendPlayerID.dwPID ) )
			{
				//非系统邮件；
				//通知client接受邮件失败
				MsgToPut *pMsgToPut = CreateSrvPkg(FGSendMailFail,pGateSrv,sendMailFail );
				pGateSrv->SendPkgToSrv(pMsgToPut);
				D_ERROR( "CSupectMailManager::AffairMail，收信人信箱满，通知发信者(%d:%d), 邮件发送失败, mailuid:%d, recvplayeruid:%d\n"
					, sendMailFail.sendPlayerID.wGID, sendMailFail.sendPlayerID.dwPID, mailuid, recvplayeruid );					
			}
			//回收邮件内存
			delete pMail; pMail = NULL;
			return;
		}
	}

	//邮件插入数据库
	InsertMailTask* pInsertTask  = NEW InsertMailTask( pGateSrv, pMail ,supectMail.sendPlayerID, supectMail.recvMailPlayerName, isSessionExist );
	TaskPoolSingle::instance()->PushTask( pInsertTask );

	return;
}// void CSupectMailManager::AffairMail(unsigned int mailuid , unsigned int recvplayeruid, bool isExist )

