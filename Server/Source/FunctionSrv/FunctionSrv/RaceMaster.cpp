﻿#include "RaceMaster.h"
#include "RecordHandler.h"
#include "OtherServer.h"
#include "../../Base/PkgProc/DealPkgBase.h"
#include "../../Base/PkgProc/MsgToPut.h"
#include "../../Base/PkgProc/PacketBuild.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "XmlManager.h"
#include "GateSrvManager.h"
#include "PublicNoticeManager.h"
#include <ctime>

void CRaceMasterMan::LoadRaceMasterInfo()
{
	for ( unsigned int racetype = 1 ; racetype <= 3; racetype++ )
	{
		RaceMasterInfo newracemaster;
		newracemaster.racetype = racetype;
		CRecordHandler::GetRaceMasterInfoByRaceID( racetype, &newracemaster );
		m_raceMasterInfoVec.push_back( newracemaster );
	}

	const char xmlconfigpath[] = {"config/racemaster.xml"};
	CXmlManager xmlManager;
	if ( !xmlManager.Load(xmlconfigpath) )
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", xmlconfigpath );
		return;
	}

	CElement* pRoot = xmlManager.Root();
	if ( NULL == pRoot )
	{
		D_ERROR("%s配置文件可能为空\n",xmlconfigpath);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	for (std::vector<CElement *>::iterator lter =childElements.begin(); lter != childElements.end(); ++lter )
	{
		CElement* pElement = *lter;
		if ( pElement )
		{
			const char* pdayofweek = pElement->GetAttr("dayofweek");
			const char* phour      = pElement->GetAttr("time");
			if ( pdayofweek && phour )
			{
				m_selecthour = ACE_OS::atoi( phour );
				m_selectdayofweek = ACE_OS::atoi( pdayofweek );
				break;
			}
		}
	}
}

void CRaceMasterMan::UpdateRaceMasterInfo(unsigned short race, const RaceMasterInfo& newracemasterinfo )
{
	if ( race> 3 )
		return;

	std::vector<RaceMasterInfo>::iterator lter = m_raceMasterInfoVec.begin();
	for( ;lter != m_raceMasterInfoVec.end(); ++lter )
	{
		RaceMasterInfo& racemaster = *lter;
		if ( racemaster.racetype ==  race )
		{
			racemaster = newracemasterinfo;
			CRecordHandler::UpdateRaceMasterInfo( racemaster );
		}
	}
}

//将族长信息发送给查询的mapsrv
void CRaceMasterMan::OnMapSrvQueryRaceMasterInfo(CGateServer* pGateSrv )
{
	if ( !pGateSrv )
		return;

	std::vector<RaceMasterInfo>::iterator lter = m_raceMasterInfoVec.begin();
	for( ;lter != m_raceMasterInfoVec.end(); ++lter )
	{
		const RaceMasterInfo& racemaster = *lter;
		if ( racemaster.masterUID != 0  )
		{
	
		    FGQueryRaceMasterInfoRst srvmsg;
			srvmsg.queryRst.declarewarinfo = racemaster.declarewarinfo;
			srvmsg.queryRst.isgotsalary = racemaster.isgotsalary;
			srvmsg.queryRst.lastupdatetime = racemaster.lastupdatetime;
			srvmsg.queryRst.masteruid = racemaster.masterUID;
			srvmsg.queryRst.race = racemaster.racetype;
			srvmsg.queryRst.todaymodifycount = racemaster.todaymodifycount;
			MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryRaceMasterInfoRst,pGateSrv,srvmsg );
			pGateSrv->SendPkgToSrv(pMsgToPut);
		}
	}
}

void CRaceMasterMan::OnDeclareWarReset( unsigned int race )
{
	std::vector<RaceMasterInfo>::iterator lter = m_raceMasterInfoVec.begin();
	for( ;lter != m_raceMasterInfoVec.end(); ++lter )
	{
		RaceMasterInfo& racemaster = *lter;
		if ( racemaster.racetype == race && (racemaster.declarewarinfo != 0) )
		{
			racemaster.declarewarinfo = 0;
			CRecordHandler::UpdateRaceMasterInfo( racemaster );
		}
	}
}

void CRaceMasterMan::OnNewRaceMasterBorn(unsigned short race, unsigned int masteruid , const char* playername, const char* unionname )
{
	if ( !playername || !unionname )
		return;

	if ( race > 3 || race == 0 )
	{
		D_ERROR("OnNewRaceMasterBorn 种族错误:%d\n", race );
		return;
	}

	
	for( std::vector<RaceMasterInfo>::iterator lter = m_raceMasterInfoVec.begin();
		 lter != m_raceMasterInfoVec.end(); 
		 ++lter )
	{
		RaceMasterInfo& racemaster = *lter;
		if ( racemaster.racetype == race )
		{
			//1.新的族长诞生，当天的限时功能重置
			racemaster.isgotsalary = false;//是否领过工资
			racemaster.lastupdatetime = (unsigned)time(NULL);//最后一次更新时间
			racemaster.masterUID = masteruid;//新族长的编号
			racemaster.todaymodifycount = 5;//今天的工会公告次数
			//2.更新DB的存盘
			CRecordHandler::UpdateRaceMasterInfo( racemaster );

			//3.新玩家当选族长了，需要发送公告告诉全服所有玩家
			time_t startime = time(NULL);
			time_t endtime = startime + 60;
			struct tm starttm = *localtime( &startime );
			struct tm endtm   = *localtime( &endtime );
			PublicNoticeEx  tmpNotice;
			tmpNotice.broadcastnum = 1;
			tmpNotice.distancetime = 30;
			tmpNotice.top = 1;
			tmpNotice.msgendtime.hour    = endtm.tm_hour;
			tmpNotice.msgendtime.mins    = endtm.tm_min;
			tmpNotice.msgendtime.day     = endtm.tm_mday;
			tmpNotice.msgendtime.month   = endtm.tm_mon  + 1;
			tmpNotice.msgendtime.year    = endtm.tm_year + 1900;
			tmpNotice.msgstarttime.hour  = starttm.tm_hour;
			tmpNotice.msgstarttime.mins  = starttm.tm_min;
			tmpNotice.msgstarttime.day   = starttm.tm_mday;
			tmpNotice.msgstarttime.month = starttm.tm_mon  + 1;
			tmpNotice.msgstarttime.year  = starttm.tm_year + 1900;
			static const char* szFormatArr[] = { "",
				                    "%s战盟的盟主%s当选人族族长",
			                        "%s战盟的盟主%s当选艾卡族长",
			                        "%s战盟的盟主%s当选精灵族长" };
			ACE_OS::snprintf( tmpNotice.szContent, 
				sizeof(tmpNotice.szContent),
				szFormatArr[race],
				unionname,playername
				);

			D_DEBUG("%s\n",tmpNotice.szContent );

			CPublicNoticeManagerSingle::instance()->NoticePublicNoticeAdd( tmpNotice );

			break;
		}
	}
}

void CRaceMasterMan::MonitorSelectRaceMasterBegin()
{
	time_t curtime  = time(NULL);
	
	if ( curtime - lastupdatetime > 15 )//如果需要检测，每30秒检测一次
	{
		struct tm curtm = *ACE_OS::localtime( &curtime );
		
		D_DEBUG("当前时间 month:%d day:%d hour:%d min:%d\n", curtm.tm_mon+1, curtm.tm_mday, curtm.tm_hour, curtm.tm_min);
		if ( curtm.tm_mday > 8 )//只需检测每个月的1号到8号
		{
			D_DEBUG("因为选族长只会在每个月的第一个星期的星期%d举行,今天已经是%d号了,星期%d .时间已过了,不需要选族长了\n",m_selectdayofweek, curtm.tm_mday,curtm.tm_wday );
		}
		else
		{
			if ( curtm.tm_wday == m_selectdayofweek )//如果当前的时间是需要选择族长的日期
			{
				
				//unsigned int dayofindex = 0;
				//for ( unsigned int dayofindex = 1 ; dayofindex< 8; dayofindex++ )
				//{
				//	struct tm tmptm = curtm;
				//	tmptm.tm_mday   = dayofindex ;
				//	time_t tmptime  = mktime( &tmptm );
				//	struct tm newtm = *localtime( &tmptime );

				//	if ( newtm.tm_wday == m_selectdayofweek )
				//	{
				//	}

				//	if( newtm.tm_wday == m_selectdayofweek  &&//如果是每个月第一个星期规定的选择族长的日子
				//		newtm.tm_mday == curtm.tm_mday  )//如果这个日子就在今天
				//	{
				//		break;
				//	}
				//}
					if ( curtm.tm_hour == m_selecthour )//判断是否到了规定开始的小时
					{
						if ( !isbSelectRaceMaster )//开始选组长
						{
							isbSelectRaceMaster = true;

							StartSelectRaceMater();

							D_DEBUG("时间到了%d点，与规定的选族长的时间%d相等,开始选族长\n", curtm.tm_hour, m_selecthour );
						}			
					}
					else
					{
						D_DEBUG("还没到点，当前时间为星期%d hour:%d, 目标时间为:%d\n",m_selectdayofweek, curtm.tm_hour,  m_selecthour  );

						if( isbSelectRaceMaster == true )//如果不满足条件了，重置
							isbSelectRaceMaster = false;

						
						if ( curtm.tm_hour == (m_selecthour - 1) )//到了族长限制时间了
						{
							if ( m_noticecount < 4 )
							{
								if ( !isforbiddenracemaster )//如果还没限制
								{
									isforbiddenracemaster = true;
								}

								D_DEBUG("到达选族长前一个小时,族长功能被禁用,通知所有服务器 %d\n",m_noticecount );
								StartForbiddenRaceMaster();
								m_noticecount++;
							}
							
						}
						else
						{
							if ( isforbiddenracemaster )
								isforbiddenracemaster = false;

							m_noticecount=0;
						}
					}
			}
			else
			{
				D_DEBUG("当前不在选族长的时间范围之内\n");
			}
		}
		lastupdatetime = curtime;
	}
}

void CRaceMasterMan::StartSelectRaceMater()
{
	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if ( pGateSrv )
	{
		D_DEBUG("发送选择族长消息:FGStartSelectRaceMaster\n");

		FGStartSelectRaceMaster srvmsg;
		MsgToPut *pMsgToPut = CreateSrvPkg( FGStartSelectRaceMaster,pGateSrv,srvmsg );
		pGateSrv->SendPkgToSrv(pMsgToPut);
	}
}

void CRaceMasterMan::StartForbiddenRaceMaster()
{
	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if ( pGateSrv )
	{
		FGForbiddenRaceMasterAuthority srvmsg;
		MsgToPut *pMsgToPut = CreateSrvPkg( FGForbiddenRaceMasterAuthority,pGateSrv,srvmsg );
		pGateSrv->SendPkgToSrv(pMsgToPut);
	}
}