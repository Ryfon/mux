﻿#include "GateSrvManager.h"
#include "OtherServer.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/PkgProc/PacketBuild.h"
#include "../../Base/PkgProc/DealPkgBase.h"

#include "rankcache.h"

void CGateSrvManger::PushGateSrv(CGateServer* pServer )
{
	if ( !pServer )
		return;

	mGateSrvVec.push_back( pServer );

	CRankCacheManager::NotiRankResInfoToGateSrv();
}

CGateServer* CGateSrvManger::GetValidGateSrv()
{
	if ( mGateSrvVec.empty() )
		return NULL;

	return mGateSrvVec[0];
}

void CGateSrvManger::RemoveGateSrv(CGateServer* pGateSrv )
{
	std::vector<CGateServer *>::iterator lter = mGateSrvVec.begin();
	for ( ;lter != mGateSrvVec.end(); ++lter )
	{
		if ( pGateSrv == *lter )
		{
			mGateSrvVec.erase( lter );
			return;
		}
	}
}

CGateServer* CGateSrvManger::GetGateSrvBySessionID(unsigned int wID )
{
	if ( mGateSrvVec.empty() )
		return NULL;

	std::vector<CGateServer *>::iterator lter = mGateSrvVec.begin();
	for ( ;lter != mGateSrvVec.end(); ++lter )
	{
		CGateServer* pSrv = *lter;
		if ( pSrv  && pSrv->GetSessionID() == (int)wID )
			return pSrv;
	}

	return NULL;
}

void CGateSrvManger::TrySendMsg()
{
	ACE_Guard<ACE_Thread_Mutex> guard( mutex_ );

	if ( !mMsgContentVector.empty() )
	{
		for( std::vector<MsgToPutContent *>::iterator lter = mMsgContentVector.begin(); 
			lter != mMsgContentVector.end(); 
			++lter
			)
		{
			SendMsgToPut( *lter );
		}

		for ( std::vector<MsgToPutContent *>::iterator lter = mMsgContentVector.begin(); 
			lter != mMsgContentVector.end(); 
			++lter )
		{
			delete *lter;
		}
		mMsgContentVector.clear();
	}
}

void CGateSrvManger::PushMsgContentToVecotr( MsgToPutContent* pContent )
{
	ACE_Guard<ACE_Thread_Mutex> guard( mutex_ );

	mMsgContentVector.push_back( pContent );
}

void CGateSrvManger::SendMsgToPut( MsgToPutContent* pContent )
{
	if ( !pContent )
		return;

	CGateServer* pGateSrv = GetGateSrvBySessionID( pContent->sessionID );
	if ( !pGateSrv )
		return;

	MsgToPut *pMsgToPut = NULL;
	switch( pContent->msgType )
	{
	case FGNoticePlayerHaveUnReadMail::wCmd:
		{
			pMsgToPut = CreateSrvPkg( FGNoticePlayerHaveUnReadMail, pGateSrv,  *(( FGNoticePlayerHaveUnReadMail *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case FGUpdatePlayerUnReadMailCount::wCmd:
		{
			pMsgToPut = CreateSrvPkg( FGUpdatePlayerUnReadMailCount, pGateSrv,  *(( FGUpdatePlayerUnReadMailCount *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case FGQueryMailListInfoByPageRst::wCmd:
		{
			pMsgToPut = CreateSrvPkg( FGQueryMailListInfoByPageRst, pGateSrv,  *(( FGQueryMailListInfoByPageRst *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case FGSendMailFail::wCmd:
		{
			pMsgToPut = CreateSrvPkg( FGSendMailFail, pGateSrv, *(( FGSendMailFail *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	default:
		break;
	}
}

