﻿/** @file RecordHandler.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2007-12-28
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef RECORD_HANDLER_H
#define RECORD_HANDLER_H


#include "../../Base/MysqlWrapper.h"
#include "../../Base/PkgProc/SrvProtocol.h"

struct StorageInfo;
struct RankUpdateInfo;
struct RaceMasterInfo;
struct PublicNoticeEx;

/**
* @class CRecordHandler
*
* @brief db中相关表数据的处理器
* 
*/
class CRecordHandler
{
private:
	CRecordHandler();
	~CRecordHandler();

public:
	//检测是否存在这个表
	static bool IsExistTable( const char* pTableName );

	//创建排行榜服务器
	static bool CreateRankChartTable();

	//创建仓库表
	static bool CreateStorageTable();

	//创建族长表
	static bool CreateRaceMasterTable();

	//创排行榜记录信息表
	static bool CreateRankUpdateInfoTable();

	//创建公告信息榜
	static bool CreatePublicMessageInfoTable();

	//检测是否存在邮件列表
	static bool IsExistMailTable();

	//检测是否存在publicmsg表
	static bool IsExistPublicMsgTable();

	//检测是否存在族长列表
	static bool IsExistRaceMasterTable();

	//是否存在排行榜的表
	static bool IsExistRankChartTable();

	//是否存在仓库表
	static bool IsExistStorageTable();

	//是否存在排行榜更新信息表
	static bool IsExistRankUpdateInfoTable();

	//是否存在拍卖行的表
	static bool IsExistAuctionTable();

	//检查排行榜缓存数据表
	static bool CheckAllRankDataTable();
	//检查并创建某个排行榜数据缓存表;
	static bool CheckRankCacheTable( ERankType rankType );
	//检查并创建某个排行榜待查表;
	static bool CheckRankQueryTable( ERankType rankType );
	//创建某个排行榜数据缓存表;
	static bool CreateRankResTable( const char* tbName );
	//检查并创建排行榜信息查询表;
	static bool CheckRankInfoTable();
	//创建排行榜信息表;
    static bool CreateRankInfoTable( const char* tbName );

	//是否存在拍卖行关系表
	static bool IsExistAuctionRelationTable();

	//创建拍卖行表
	static bool CreateAuctionTable();

	//创建拍卖行关系表
	static bool CreateAuctionRelationTable();

	//创建邮件列表
	static bool CreateMailTable();

	//获取邮件列表中的最大的邮件编号
	static unsigned GetMailMaxID();

	//检查对应ID的仓库编号是否存在
	static bool CheckQueryIDStorageIsExist( unsigned int queryID );
	
	//获取仓库的信息 
	static bool GetStorageInfoByQueryID( unsigned int queryID , StorageInfo* pStorageInfo );

	//获取保存的族长信息
	static bool GetRaceMasterInfoByRaceID( unsigned int racetype,RaceMasterInfo* pRaceMasterInfo );

	//创建一个新的仓库
	static bool InsertNewStorage( unsigned int storageUID );

	////创建一个新的排行更新信息
	//static bool InsertNewRankUpdateInfo( RankUpdateInfo* pRankUpdateInfo );

	//创建种族表
	static bool InsertRaceMasterInfo();

	//更新种族族长信息
	static bool UpdateRaceMasterInfo( RaceMasterInfo& raceMasterRef );

	//更新中央公告信息
	static bool UpdatePublicNoticeInfo( PublicNoticeEx& publicNoticeRef ,const char* pstarttime, const char* pendtime);

	//删除中央公告信息
	static bool DeletePublicNoticeInfo( unsigned int msgid );

	//删除所有公告信息
	static bool DeleteRemoveAllPublicNotice();

	//增加新的中央公告
	static bool InsertNewPublicNotice( PublicNoticeEx& publicNoticeRef ,const char* pstarttime, const char* pendtime );
};


#endif/*RECORD_HANDLER_H*/
