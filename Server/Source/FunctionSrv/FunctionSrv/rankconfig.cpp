﻿
#include "rankconfig.h"

CRankConfig CRankConfigManager::m_arrRankConfig[RANK_TYPE_NUM];
bool        CRankConfigManager::m_isRankConfigRead = false;

bool CRankReword::InitRewordWithXML( CElement* pXmlItem )
{
	if ( NULL == pXmlItem )
	{
		return false;
	}

	/*<desc number="1" rewardid="235012010" rewardtype="1" rewardnumber="1"  />*/
	if ( !IsXMLValid( pXmlItem, "number", -1 ) )
	{
		return false;
	}
	m_playerNum = (unsigned int) atoi( pXmlItem->GetAttr("number") );
	if ( !IsXMLValid( pXmlItem, "rewardid", -1 ) )
	{
		return false;
	}
	m_rewardID = (unsigned int) atoi( pXmlItem->GetAttr("rewardid") );
	if ( !IsXMLValid( pXmlItem, "rewardtype", -1 ) )
	{
		return false;
	}
	m_rewardType = (unsigned int) atoi( pXmlItem->GetAttr("rewardtype") );
	if ( !IsXMLValid( pXmlItem, "rewardnumber", -1 ) )
	{
		return false;
	}
	m_rewardNum = (unsigned int) atoi( pXmlItem->GetAttr("rewardnumber") );

	return true;
}

bool CRankConfig::InitRankConfigWithXML( CElement* pXmlItem )
{
	/*<rankList id="01" refreshtime="2" sort="0" >*/
	if ( NULL == pXmlItem )
	{
		return false;
	}

	if ( !IsXMLValid( pXmlItem, "id", -1 ) )
	{
		return false;
	}
	m_rankType = (ERankType) atoi( pXmlItem->GetAttr("id") );
	if ( !IsXMLValid( pXmlItem, "refreshtype", -1 ) )
	{
		return false;
	}
	m_refreshType = (RankRefreshType) atoi( pXmlItem->GetAttr("refreshtype") );
	if ( !IsXMLValid( pXmlItem, "sort", -1 ) )
	{
		return false;
	}
	m_isDesc = (atoi( pXmlItem->GetAttr("sort") ) == 0) ? false:true;

	return true;
}

bool CRankConfig::InitRankRewardWithXML( CElement* pXmlItem )
{
	return m_reword.InitRewordWithXML( pXmlItem );
}

//取指定排行榜的刷新类型；
bool CRankConfigManager::GetRankRefreshType( ERankType rankType, RankRefreshType& refreshType )
{
	if ( rankType >= ARRAY_SIZE(m_arrRankConfig) )
	{
		NewLog( LOG_LEV_ERROR, "CRankConfigManager::GetRankRefreshType，错误的rankType:%d", rankType );
		return false;
	}

	refreshType = m_arrRankConfig[rankType].GetRefreshType();	
	return true;
}

//读取排行榜相关配置
bool CRankConfigManager::ReadRankConfig() 
{
	const char* rankconfig = "config/ranklist.xml";
	NewLog( LOG_LEV_INFO, "开始读取排行榜配置文件%s", rankconfig );

	if ( m_isRankConfigRead )
	{
		NewLog( LOG_LEV_ERROR, "CRankConfigManager::ReadRankConfig，重复读取排行榜配置文件%s，返回失败", rankconfig );
		return false;
	}
	m_isRankConfigRead = false;

	CXmlManager xmlManager;
	if(!xmlManager.Load(rankconfig))
	{
		NewLog( LOG_LEV_ERROR, "db配置文件%s加载出错", rankconfig );
		return false;
	}

	CElement* pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		NewLog( LOG_LEV_ERROR, "db配置文件%s可能为空", rankconfig );
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);

	CElement *pServerElement = NULL;
	unsigned int uiServerElementIndex = 0;
	unsigned short uiConnStringIndex = 0;
	CElement* pTmpItem = NULL;
	CRankConfig tmpConfig;
	for(std::vector<CElement *>::iterator iter = childElements.begin(); iter!=childElements.end(); ++iter)
	{
		pTmpItem = (CElement*) *iter;
		if ( NULL == pTmpItem )
		{
			continue;
		}

		if ( 2 == pTmpItem->Level() )
		{
			tmpConfig.InitRankConfigWithXML( pTmpItem );
		}
		if ( 3 == pTmpItem->Level() )
		{
			tmpConfig.InitRankRewardWithXML( pTmpItem );
			ERankType tmpRankType = tmpConfig.GetResRankType();
			if ( tmpRankType >= ARRAY_SIZE(m_arrRankConfig) )
			{
				NewLog( LOG_LEV_ERROR, "配置文件中错误的rankType:%d", tmpRankType );
				return false;
			}
			m_arrRankConfig[tmpRankType].DepthCopyFrom( tmpConfig );
		}
	}

	NewLog( LOG_LEV_INFO, "排行榜配置文件%s读取完毕", rankconfig );

	m_isRankConfigRead = true;

	return true; 
};

