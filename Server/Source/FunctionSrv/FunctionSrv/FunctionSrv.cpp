﻿#if defined(_WIN32) || defined(_WIN64)
#define  _SECURE_SCL_THROWS 1
#endif

#include "../../Base/Utility.h"
#include "../../Base/PkgProc/MsgToPut.h"
#include "../../Base/BufQueue/BufQueue.h"
#include "../../Base/tcache/tcache.h"
//#include "../../Base/CacheMessageBlock.h"
#include "Service.h"
#include "GateSrvManager.h"

#ifdef USE_DSIOCP
#include "../../Base/LoadConfig/LoadSrvConfig.h"
#include "iocp/dsiocp.h"
#endif //USE_DSIOCP

#include "../../Test/testthreadque_wait/sigexception/sigexception.h"

#include "rankcache.h"

#ifdef USE_DSIOCP
CManCommHandler< CRcvPkgHandler<CServerConnection> >* g_ManCommHandler;
TCache< CDsSocket >* g_poolDsSocket = NULL;
TCache< UniqueObj< CDsSocket > >* g_poolUniDssocket = NULL;
TCache< PerIOData >* g_poolPerIOData = NULL;
TCache< DsIOBuf >*   g_poolDsIOBuf = NULL;
#endif //USE_DSIOCP

// 全局发送队列
IBufQueue *g_pSrvSender;

// 全局消息池
TCache<MsgToPut> *g_poolMsgToPut;

//// 全局小缓存池
//CCacheMessageBlock *g_poolSmallMB;

// 全局协议类型
const unsigned char SELF_PROTO_TYPE = 0x07;
//
//#ifdef WIN32
//#include "../../crashrpt/include/CrashRpt.h"
//#pragma comment(lib,"../../crashrpt/lib/crashrpt")
//#endif

#define  TEST


static void SigIntHandler(int signo)
{
	D_DEBUG("收到%d信号", signo);
	return;
}

int ACE_TMAIN(int argc, ACE_TCHAR *argv[])
{	
	// 是否作为守护进程运行？
#if !defined(ACE_WIN32)
	if(Parse_args(argc, argv) == 1)
		ACE::daemonize();
#endif/*ACE_WIN32*/

	MainInitTls();

	SetTSTP();//TSTP信号终止程序添加

	// 捕捉SIGINT信号
	ACE_Sig_Action handleSigInt(reinterpret_cast <ACE_SignalHandler>(SigIntHandler), SIGINT);
	ACE_UNUSED_ARG(handleSigInt);

	// 忽略SIGPIPE信号
	ACE_Sig_Action noSigPipe((ACE_SignalHandler)SIG_IGN, SIGPIPE);
	ACE_UNUSED_ARG(noSigPipe);

	// 创建全局消息池
	g_poolMsgToPut = NEW TCache<MsgToPut>(5000);
	if(g_poolMsgToPut == NULL)
		return -1;
	// 自动销毁
	auto_ptr< TCache<MsgToPut> > poolAutoClean(g_poolMsgToPut);

	//// 创建全局小缓冲块池
	//g_poolSmallMB = NEW CCacheMessageBlock(10, 50*1024);
	//if(g_poolSmallMB == NULL)
	//	return -1;
	//// 自动销毁
	//auto_ptr< CCacheMessageBlock > poolAutoCleanSmall(g_poolSmallMB);

#ifdef USE_DSIOCP
	CDsSocket::Init();
	g_poolPerIOData = NEW TCache< PerIOData >( 2000 );//尽量分配足够；
	g_poolUniDssocket = NEW TCache< UniqueObj< CDsSocket > >( 2000 );//尽量分配足够；
	g_poolDsSocket = NEW TCache< CDsSocket>( 256 );//全局DsSocket对象池，必须分配足够，因为只能使用Retrieve而不能使用RetrieveOrCreate；
	g_poolDsIOBuf = NEW TCache< DsIOBuf >( 2560 );//因为服务器端连接数少，虽然通信量大；
#endif //USE_DSIOCP

	// 服务初始化
	CService service;
	if(service.Open() == -1)
	{
		D_DEBUG("CService::open 出错\n");
		goto exit;
	}

	CRankCacheManager::InitRankCacheManager();	
	service.Run();// 运行
	CRankCacheManager::ReleaseRankCacheManager();

	// 关闭服务
	service.Close();

exit:
#ifdef USE_DSIOCP
	//删DsSocket对象池；
	if ( NULL != g_poolDsSocket )
	{
		delete g_poolDsSocket; g_poolDsSocket = NULL;
	}
	if ( NULL != g_poolDsIOBuf )
	{
		delete g_poolDsIOBuf; g_poolDsIOBuf = NULL;
	}
	//删唯一对象对象池；
	if ( NULL != g_poolUniDssocket )
	{
		delete g_poolUniDssocket; g_poolUniDssocket = NULL;
	}
	if ( NULL != g_poolPerIOData )
	{
		delete g_poolPerIOData; g_poolPerIOData = NULL;
	}
#endif //USE_DSIOCP

	MainEndTls();

	return 0;
}

