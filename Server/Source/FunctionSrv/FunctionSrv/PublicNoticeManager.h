﻿/********************************************************************
	created:	2009/12/07
	created:	7:12:2009   16:25
	file:		PublicNoticeManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/
#ifndef PUBLIC_NOTICE_MANAGER_H
#define PUBLIC_NOTICE_MANAGER_H

#include <vector>
#include "aceall.h"
#include "../../Base/PkgProc/CliProtocol_T.h"
#include "XmlManager.h"
#include "../../Base/Utility.h"
using namespace MUX_PROTO;

#define MAX_PUBLIC_MSG MAX_PUBLICNOTICE_LEN//中央公告的内容最大长度
#define MAX_PUBLICMSG_COUNT 100//允许存盘的中央公告的个数

class CGateServer;

struct MsgTimeInfo
{
	unsigned int year;
	unsigned int month;
	unsigned int day;
	unsigned int hour;
	unsigned int mins;
	MsgTimeInfo():year(0),month(0),day(0),hour(0),mins(0)
	{
	}

	void Reset()
	{
		year = month = day = hour = mins = 0;
	}
};

struct PublicNoticeEx
{
	unsigned int msgid;
	char top;
	int  broadcastnum;
	int  distancetime;
	char szContent[MAX_PUBLIC_MSG];
	MsgTimeInfo  msgstarttime;
	MsgTimeInfo  msgendtime;

	PublicNoticeEx():msgid(0),top(0),broadcastnum(0),distancetime(0)
	{
		StructMemSet( szContent, 0x0, sizeof(szContent) );
	}

	void Reset()
	{
		msgid = top = broadcastnum = distancetime = 0;
		msgstarttime.Reset();
		msgendtime.Reset();
		StructMemSet( szContent, 0x0, sizeof(szContent) );
	}
};

void ConvertToPublicNotice( const PublicNoticeEx& noticeex, PublicNotice& noticeref );

class CPublicNoticeManager
{
	friend class ACE_Singleton<CPublicNoticeManager, ACE_Null_Mutex>;

public:
	CPublicNoticeManager():noticecount(0)
	{
		for ( unsigned int i = 0 ; i<MAX_PUBLICMSG_COUNT;i++)
		{
			publicnoticeVec.push_back( &publicnoticeArr[i] );
		}
	}

public:
	void LoadPublicNoticeXML();

public:
	//加载中央公告信息
	void LoadPublicNotice();

	//玩家上线时,发送中央公告信息给玩家
	void SendPublicNoticeToPlayer( CGateServer* pGateSrv ,PlayerID playerID );

	//修改中央公告信息
	void ModifyPublicNotice( unsigned msgid, const char* content, char istop, const char* starttime, const char* endtime, int broadcastnum, int distancetime );

	//删除中央公告的信息
	void DeletePublicNotice( unsigned msgid );

	//添加新的中央公告信息
	PublicNoticeEx* AddNewPublicNotice( unsigned int msgid, const char* content, char istop, const char* starttime, const char* endtime, int broadcastnum, int distancetime ,bool needNotice );

	//重新加载所有的公告信息
	void ReLoadGMPublicNotice();

	//当GM查询所有的公告信息
	void QueryPublicNotice( CGateServer* pGateSrv, PlayerID& gmplayerid );

	void ResetPublicNotice()
	{
		noticecount = 0;
		mlocalpublicnotice.clear();
		for ( unsigned int i = 0 ; i<MAX_PUBLICMSG_COUNT;i++)
		{
			publicnoticeArr[i].Reset();
		}
	}

private:
	bool IsLocalPublicNotice( unsigned int msgid );

	PublicNoticeEx* GetPublicNoticeByMsgID( unsigned int msgid );

	void RemovePublicNoticeByMsgID( unsigned int msgid );

	void ClearAllPublicNotices();

	bool SplitTimeStrToMsgTimeInfo( MsgTimeInfo& timeinfo, char* ptimestr );

	bool CheckPublicNoticeExipre( const MsgTimeInfo& starttime, const MsgTimeInfo& endtime ,time_t& curtime );

public:
	void NoticePublicNoticeModify(  const PublicNoticeEx& publicNotice );

	void NoticePublicNoticeRemove(  unsigned int msgid );

	void NoticePublicNoticeAdd( const PublicNoticeEx& publicNotice );

private:
	PublicNoticeEx publicnoticeArr[MAX_PUBLICMSG_COUNT];
	unsigned int noticecount;
	std::vector<PublicNoticeEx *> publicnoticeVec;
	std::vector<unsigned int> mlocalpublicnotice;
};

typedef ACE_Singleton<CPublicNoticeManager, ACE_Null_Mutex> CPublicNoticeManagerSingle;

#endif