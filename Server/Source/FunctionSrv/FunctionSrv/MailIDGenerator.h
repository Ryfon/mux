﻿/********************************************************************
	created:	2009/04/22
	created:	22:4:2009   15:49
	file:		MailIDGenerator.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef MAILID_GENERATOR_H
#define MAILID_GENERATOR_H

#include "aceall.h"

class CMailIDGenerator
{
	friend class ACE_Singleton<CMailIDGenerator, ACE_Null_Mutex>;
public:
	unsigned int GetUnqiueMailID() { return mMailIndexID; }

	void IncMailID() { mMailIndexID ++ ; }

	//从数据库中获取当前最大的邮件编号
	void GetMaxMailIndexIDFromDB();

private:
	unsigned int  mMailIndexID;
};

typedef ACE_Singleton<CMailIDGenerator, ACE_Null_Mutex> MailIDGeneratorSingle;

#endif

