﻿#include "OtherServer.h"
#include "ServerConnection.h"
#include "DealPkg.h"


CSrvBase::CSrvBase(CServerConnection* pConn)
{
	m_pConn = pConn;
}

CSrvBase::~CSrvBase(void)
{

}


#ifdef USE_DSIOCP
UniqueObj< CDsSocket>* CSrvBase::GetUniqueDsSocket()
{
	if ( NULL != m_pConn )
	{
		return m_pConn->GetUniqueDsSocket();//返回连接对应属性；
	} else {
		return NULL;
	}
}
#endif //USE_DSIOCP

int CSrvBase::GetHandleID(void)
{
	if(NULL != m_pConn)
		return m_pConn->GetHandleID();

	return -1;
}

int CSrvBase::GetSessionID(void)
{	
	if(NULL != m_pConn)
		return m_pConn->GetSessionID();

	return -1;
}

void CSrvBase::SendPkgToSrv( MsgToPut* pPkg )
{
	m_pConn->SendPkgToSrv(pPkg);
	return;
}

void CSrvBase::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	ACE_UNUSED_ARG(wCmd);
	ACE_UNUSED_ARG(pBuf);
	ACE_UNUSED_ARG(wPkgLen);
}


CGateServer::CGateServer(CServerConnection* pConn, unsigned int wID )
	: CSrvBase(pConn),mWID(wID)
{

}

CGateServer::~CGateServer(void)
{

}

void CGateServer::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	CDealGateSrvPkg::DealPkg(this, wCmd, pBuf, wPkgLen);
	
	return;
}


COtherServerManager::ServerVector COtherServerManager::m_serverVector;

int COtherServerManager::addSrv(unsigned short usServerType, unsigned short usServerID, CSrvBase* pServer)
{
	ACE_UNUSED_ARG(usServerType);
	ACE_UNUSED_ARG(usServerID);
	ACE_UNUSED_ARG(pServer);

	return 0;
}

void COtherServerManager::removeSrv(unsigned short usServerType, unsigned short usServerID)
{
	ACE_UNUSED_ARG(usServerType);
	ACE_UNUSED_ARG(usServerID);

	return;
}

CSrvBase* COtherServerManager::findSvr(unsigned short usServerType, unsigned short usServerID)
{
	ACE_UNUSED_ARG(usServerType);
	ACE_UNUSED_ARG(usServerID);

	return NULL;
}
