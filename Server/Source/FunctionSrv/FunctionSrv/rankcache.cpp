﻿
#include "rankcache.h"

#include "../../Base/PkgProc/MsgToPut.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/PkgProc/DealPkgBase.h"
#include "../../Base/PkgProc/PacketBuild.h"

#include "rankconfig.h"

#include "OtherServer.h"
#include "GateSrvManager.h"

#include "SuspectMailManager.h"

using namespace MUX_PROTO;

/*
	普通排行榜:
	    若当前不在缓存中，则：1、若当前缓存中人数未满，则加入缓存；
		                      2、若缓存中人数已满，则检查是否超过阈值，超过时加入缓存(发生次数较少)，重新扫描确定阈值，同时将原阈值对应玩家出缓存(对应信息替换成新人信息)；
	    若当前已在缓存中，更新其数据，若对应玩家即为阈值，则重新扫描确定阈值(可能性最小)

	特殊排行榜：
	    除记录数超过普通排行榜外，其余一致？？？
*/

NewTaskPool<CRankDBTask> CRankDBTask::m_TaskPool;

CRankCache CRankCacheManager::m_arrRankCache[RANK_TYPE_NUM];//缓存表；
CRankCache CRankCacheManager::m_arrRankQuery[RANK_TYPE_NUM];//查询表；
RankProcInfo CRankCacheManager::m_arrRankProcInfo[RANK_TYPE_NUM];//排行榜处理信息；

unsigned int CRankCacheManager::m_lastRefreshCheckTime;//上次刷新检查时间；
unsigned int CRankCacheManager::m_lastCacheSaveTime;//上次缓存存DB时间；

CRankDBIns CRankCacheManager::m_DBIns;
bool CRankCacheManager::m_isAllRankDbLoad = false;//是否所有需要的排行榜数据缓存都已根据DB表初始化；

bool CacheRcdComp(RankCacheRcd* elem1, RankCacheRcd* elem2 )
{
	if ( ( NULL == elem1 ) || ( NULL == elem2 ) )
	{
		NewLog( LOG_LEV_ERROR, "" );
		return false;
	}
	return elem1->rankItemInfo.rankInfo > elem2->rankItemInfo.rankInfo;
}

//使用输入参数判断自身是否需要更新；
bool RankProcInfo::IsNeedRefresh( RankRefreshType refreshType )
{
	time_t curtimet = time(NULL);

	double passed = difftime( curtimet, m_updateTime );
	if ( passed < 1 )
	{
		NewLog( LOG_LEV_ERROR, "RankProcInfo::IsNeedRefresh, error IsNeedRefresh" );
		return false;
	}

	bool isNeed = false;

	switch ( refreshType )
	{
	case RR_DAY:
		{
			isNeed = ( passed > 3600/*秒*/*24/*小时*/ );
		}
		break;
	case RR_MONTH:
		{
			isNeed = ( passed > 3600/*秒*/*24/*小时*/*30/*天*/ );
		}
		break;
	case RR_WEEK:
		{
			isNeed = ( passed > 3600/*秒*/*24/*小时*/*7/*天*/ );
		}
		break;
	case RR_LEVEL:
		isNeed = (passed > 30);//level接近实时更新；
		break;
	default:
		{
			NewLog( LOG_LEV_ERROR, "RankProcInfo::IsNeedRefresh，错误的refreshType:%d", refreshType );
			isNeed = false;
		}
	};

	if ( isNeed )
	{
		m_updateTime = (unsigned int)curtimet;		
	}

	return isNeed;
}

//设置自身已更新；
bool RankProcInfo::SetRefreshed()
{
	++ m_rankSeq;

	m_updateTime = (unsigned int)time(NULL);

	CRankCacheManager::IssueUpdateRankProcInfo( GetRankType(), this );//更新信息存至DB；

	return true;
}


//取排行榜信息表名字(其中存储排行榜更新时间|序号等信息)；
const char* CRankDBTask::GetRankInfoTbName()
{
	return "rank_info";
}

//取排行榜对应的称号类型
EHonorMask CRankDBTask::GetRankResHonorMask( ERankType rankType, unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ )
{
	/*
	typedef	enum
	{
		INVALID_HONOR                        	=	0,           	//无效称号
		SHASHENG                             	=	1,           	//杀神
		ZHANSHENG                            	=	2,           	//战神
		SISHENG                              	=	3,           	//死神
		MOWANG                               	=	4,           	//魔王
		CAISHENG                             	=	5,           	//财神
		TIANWANG                             	=	6,           	//天王(传说英雄榜)
		TIANHOU                              	=	7,           	//天后(传说英雄榜)
	}EHonorMask;
	*/
	switch ( rankType )
	{
	case RANK_DAMAGE:
		return ZHANSHENG;
		break;
	case RANK_FORTUNE:
		return CAISHENG;
		break;
	case RANK_GOODEVIL:
		return MOWANG;
		break;
	case RANK_KILL:
		return SHASHENG;
		break;
	case RANK_BEKILL:
		return SISHENG;
		break;
	case RANK_LEGEND:
		{
			if ( 0 == sexFilter )
			{
				NewLog( LOG_LEV_ERROR, "CRankDBTask::GetRankResHonorMask，取传说英雄榜称号未指定性别信息" );
			} else if ( 1 == sexFilter ) {
				return TIANWANG;
			} else if ( 2 == sexFilter ) {
				return TIANHOU;
			}
		}
		break;
	default:
		{ return INVALID_HONOR;}
	}
	return INVALID_HONOR;
}

//各排行榜的名字(用于奖励邮件内容填写)；
const char* CRankDBTask::GetRankName( ERankType rankType )
{
	switch ( rankType )
	{
	case RANK_LEVEL:
		return "等级排行榜";
		break;
	case RANK_DAMAGE:
		return "单体伤害排行榜";
		break;
	case RANK_GOLD:
		return "金币排行榜";
		break;
	case RANK_SILVER:
		return "银币排行榜";
		break;
	case RANK_FORTUNE:
		return "财富排行榜";
		break;
	case RANK_GOODEVIL:
		return "魔头排行榜";
		break;
	case RANK_KILL:
		return "击杀次数排行榜";
		break;
	case RANK_BEKILL:
		return "死亡次数排行榜";
		break;
	case RANK_LEGEND:
		return "传说英雄榜";
		break;
	default:
		{ return "未知排行榜";}
	}
	return "未知排行榜";
}

//各排行榜待查表对应的表名；
const char* CRankDBTask::GetQueryTbName( ERankType rankType )
{
	switch ( rankType )
	{
	case RANK_LEVEL:
		return "rank_level_query";
		break;
	case RANK_DAMAGE:
		return "rank_damage_query";
		break;
	case RANK_GOLD:
		return "rank_gold_query";
		break;
	case RANK_SILVER:
		return "rank_silver_query";
		break;
	case RANK_FORTUNE:
		return "rank_fortune_query";
		break;
	case RANK_GOODEVIL:
		return "rank_goodevil_query";
		break;
	case RANK_KILL:
		return "rank_kill_query";
		break;
	case RANK_BEKILL:
		return "rank_bekill_query";
		break;
	case RANK_LEGEND:
		return "rank_legend_query";
		break;
	default:
		{ return NULL;}
	}
	return NULL;
}

//各排行榜缓存数据对应的表名；
const char* CRankDBTask::GetResTbName( ERankType rankType )
{
	switch ( rankType )
	{
	case RANK_LEVEL:
		return "rank_level_data";
		break;
	case RANK_DAMAGE:
		return "rank_damage_data";
		break;
	case RANK_GOLD:
		return "rank_gold_data";
		break;
	case RANK_SILVER:
		return "rank_silver_data";
		break;
	case RANK_GOODEVIL:
		return "rank_goodevil_data";
		break;
	case RANK_KILL:
		return "rank_kill_data";
		break;
	case RANK_BEKILL:
		return "rank_bekill_data";
		break;
	default:
		{ return NULL;}
	}
	return NULL;
};

//辅助线程处理，或者从DB中新取缓存数据，或者向DB中更新缓存数据；
bool CRankDBTask::AssistProcMsg( CConnection* pDBConn )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::AssistProcMsg, NULL == pDBConn" );
		return false;
	}

	switch ( m_taskType )
	{
	case GET_RANK_DATA:
		GetRankData( pDBConn );
		break;
	case UPDATE_RANK_DATA:
		UpdateRankData( pDBConn );
		break;
	case PERIOD_CLEAN_RANK:
		PeriodCleanRank( pDBConn );
		break;
	case QUERY_TB_WRITE:
		QueryTbWrite( pDBConn );
		break;
	case QUERY_TB_READ:
		QueryTbRead( pDBConn );
		break;
	case LOAD_RANKINFO:
		LoadRankInfo( pDBConn );
		break;
	case UPDATE_RANKINFO:
		UpdateRankInfo( pDBConn );
		break;
	default:
		{
			NewLog( LOG_LEV_ERROR, "CRankDBTask::AssistProcMsg, 未知的RankDBTask%d", m_taskType );
		}
	}

	return true;
}//bool CRankDBTask::AssistProcMsg( CConnection* pDBConn )

//主线程结果处理；
bool CRankDBTask::MainRstProcMsg()
{
	switch ( m_taskType )
	{
	case GET_RANK_DATA:
		//读DB缓存数据完毕；
		ProcRankDataFromDB();
		break;
	case UPDATE_RANK_DATA:
		//向DB更新缓存数据，无需处理；
		break;
	case PERIOD_CLEAN_RANK:
		//清DB缓存数据完毕，无需处理
		break;
	case QUERY_TB_WRITE:
		//写查询表，无需处理
		break;
	case QUERY_TB_READ:
		//读查询表
		ProcQueryTbFromDB();
		break;
	case LOAD_RANKINFO:
		//读各排行榜信息(更新时间，序号等等)
		ProcLoadRankInfo();
		break;
	case UPDATE_RANKINFO:
		//更新排行榜信息，无需处理
		break;
	default:
		{
			NewLog( LOG_LEV_ERROR, "CRankDBTask::MainRstProcMsg, 未知的RankDBTask%d", m_taskType );
		}
	}
	return true;
}//bool CRankDBTask::MainRstProcMsg()

//从DB中取排行榜缓存数据；
bool CRankDBTask::GetRankData( CConnection* pDBConn )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::GetRankData, NULL == pDBConn" );
		return false;
	}
	const char* tbName = GetResTbName( m_rankType );
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::GetRankData, NULL == tbName" );
		return false;
	}
	const char* orderRule = GetOrderRule();
	if ( NULL == orderRule )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::GetRankData, NULL == orderRule" );
		return false;
	}
	
	return ReadRankResTB( pDBConn, tbName );
}

//向DB更新排行榜缓存数据；
bool CRankDBTask::UpdateRankData( CConnection* pDBConn )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::UpdateRankData, NULL == pDBConn" );
		return false;
	}
	const char* tbName = GetResTbName( m_rankType );
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::UpdateRankData, NULL == tbName" );
		return false;
	}

	return WriteRankResTB( pDBConn, tbName );//实际必须全更新，否则逻辑混乱;
}

//定时清DB排行榜缓存数据
bool CRankDBTask::PeriodCleanRank( CConnection* pDBConn )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::PeriodCleanRank, NULL == pDBConn" );
		return false;
	}
	const char* tbName = GetResTbName( m_rankType );
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::PeriodCleanRank, NULL == tbName" );
		return false;
	}

	//先删去之前的表；
	stringstream query;
	query << "truncate table " << tbName;
	CQueryWrapper truncateWrapper( *pDBConn );
	truncateWrapper.ExecQuery( query.str() );

	NewLog( LOG_LEV_DEBUG, "定时清DB中的排行榜%d:%s缓存数据完成", m_rankType, tbName ); 

	return true;
}

//写查询表；
bool CRankDBTask::QueryTbWrite( CConnection* pDBConn )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::QueryTbWrite, NULL == pDBConn" );
		return false;
	}
	const char* tbName = GetQueryTbName( m_rankType );
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::QueryTbWrite, NULL == tbName" );
		return false;
	}

	return WriteRankResTB( pDBConn, tbName );
}// bool CRankDBTask::QueryTbWrite( CConnection* pDBConn )

//更新排行榜信息
bool CRankDBTask::UpdateRankInfo( CConnection* pDBConn )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::UpdateRankInfo, NULL == pDBConn" );
		return false;
	}
	const char* tbName = CRankDBTask::GetRankInfoTbName();
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::UpdateRankInfo, NULL == tbName" );
		return false;
	}

	/*表字段顺序: ranktype, rankseq, updatetime*/
	stringstream query;
	query << "update " << tbName << " set rankseq=" << m_ProcInfo.GetRankSeq()
		<< ", updatetime = " << m_ProcInfo.GetLastUpdateTime() 
		<< " where ranktype =" << m_rankType ;
	CQueryWrapper queryWrapper( *pDBConn );
	queryWrapper.ExecQuery( query.str() );

	return true;
}

//读排行榜信息(更新时间，序号等)
bool CRankDBTask::LoadRankInfo( CConnection* pDBConn )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::LoadRankInfo, NULL == pDBConn" );
		return false;
	}
	const char* tbName = CRankDBTask::GetRankInfoTbName();
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::LoadRankInfo, NULL == tbName" );
		return false;
	}

	/*表字段顺序: ranktype, rankseq, updatetime*/
	stringstream query;
	query << "select ranktype, rankseq, updatetime from " 
		<< tbName << " where ranktype = " << m_rankType ;
	CQueryWrapper queryWrapper( *pDBConn );
	queryWrapper.ExecQuery( query.str() );

	unsigned int rcdNum = queryWrapper.GetRstRcdNum();
	if ( rcdNum != 1 )
	{
		NewLog( LOG_LEV_WARNING, "DB中的表%d:%s中记录数目%d不正确, 查询语句:%s", m_rankType, tbName, rcdNum, query.str().c_str() );
		return false;
	}

	if ( !queryWrapper.LoadNextRcd() )
	{
		NewLog( LOG_LEV_WARNING, "DB中的表%d:%s中读记录失败, 查询语句:%s", m_rankType, tbName, rcdNum, query.str().c_str() );
		return false;
	}

	m_ProcInfo.DataFromDbField( queryWrapper.GetCurRcdField(0), queryWrapper.GetCurRcdField(1), queryWrapper.GetCurRcdField(2) );

	return true;
}

//读查询表；
bool CRankDBTask::QueryTbRead( CConnection* pDBConn )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::QueryTbRead, NULL == pDBConn" );
		return false;
	}
	const char* tbName = GetQueryTbName( m_rankType );
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::QueryTbRead, NULL == tbName" );
		return false;
	}

	return ReadRankResTB( pDBConn, tbName );
}// bool CRankDBTask::QueryTbRead( CConnection* pDBConn )

bool CRankDBTask::WriteRankResTB( CConnection* pDBConn, const char* tbName )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::WriteRankResTB, NULL == pDBConn" );
		return false;
	}
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::WriteRankResTB, NULL == tbName" );
		return false;
	}

	//先删去之前的表；
	stringstream query;
	query << "truncate table " << tbName;
	CQueryWrapper truncateWrapper( *pDBConn );
	truncateWrapper.ExecQuery( query.str() );

	if ( m_rcdSet.GetValidRcdNum() <= 0 )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::QueryTbWrite，写入的有效数据量为0，新表%s记录数将为0", tbName );
		return true;
	}

	/*表字段顺序: inPUID, inPClass, inPLevel, inRankInfo, inPName, inMSeq, inRankSeqID*/
	//然后填表中的新数据；
	query.str("");//清之前的sql语句
	query << "insert into " << tbName << " (puid, pclass, plevel, rankinfo, pname, mseq, rankseqid, extrainfo) values ";
	RankCacheRcd* pTmpRcd = NULL;
	unsigned int rcdNum = m_rcdSet.GetValidRcdNum();
	for ( unsigned int i=0; i<rcdNum; ++i )
	{
		pTmpRcd = m_rcdSet.GetIndexRcd( i );
		if ( NULL == pTmpRcd )
		{
			NewLog( LOG_LEV_ERROR, "srv写DB表%s时，第%d条数据空", tbName, i );
			continue;
		}
		if ( 0!=i )
		{
			//第二条记录之后要多加一个逗号分隔
			query << ",";
		}
		query << "(" << pTmpRcd->playerUID << "," << (unsigned int)pTmpRcd->rankItemInfo.playerClass << "," << pTmpRcd->rankItemInfo.playerLevel << "," << pTmpRcd->rankItemInfo.rankInfo \
			<< ",'" << pTmpRcd->rankItemInfo.playerName << "'," << pTmpRcd->enterMapSeq << "," << pTmpRcd->rankSeqID << "," << pTmpRcd->rankItemInfo.extraInfo << ")";
	}
	CQueryWrapper newdataWrapper( *pDBConn );
	newdataWrapper.ExecQuery( query.str() );

	NewLog( LOG_LEV_DEBUG, "srv写DB表%d:%s完成，写入的记录数%d", m_rankType, tbName, rcdNum ); 

	return true;
}

bool CRankDBTask::ReadRankResTB( CConnection* pDBConn, const char* tbName )
{
	if ( NULL == pDBConn )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::ReadRankResTB, NULL == pDBConn" );
		return false;
	}
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::ReadRankResTB, NULL == tbName" );
		return false;
	}
	const char* orderRule = GetOrderRule();
	if ( NULL == orderRule )
	{
		NewLog( LOG_LEV_ERROR, "CRankDBTask::ReadRankResTB, NULL == orderRule" );
		return false;
	}

	m_rcdSet.ClearRcdSetData();//先清自身数据；

	/*表字段顺序: inPUID, inPClass, inPLevel, inRankInfo, inPName, inMSeq, inRankSeqID*/
	stringstream query;
	query << "select puid, pclass, plevel, rankinfo, pname, mseq, rankseqid, extrainfo from " << tbName << " order by rankinfo " << orderRule << " limit 200 ";
	CQueryWrapper queryWrapper( *pDBConn );

	queryWrapper.ExecQuery( query.str() );

	unsigned int rcdNum = queryWrapper.GetRstRcdNum();
	if ( rcdNum < 0 )
	{
		NewLog( LOG_LEV_WARNING, "DB中的表%d:%s中还没有记录，查询语句:%s", m_rankType, tbName, query.str().c_str() );
		return true;
	}

	RankCacheRcd tmpRcd;
	unsigned int innerSize = m_rcdSet.GetMaxRcdNum();
	unsigned int readNum = 0;
	for ( unsigned int i=0; i<innerSize; ++i )
	{
		if ( !queryWrapper.LoadNextRcd() )
		{
			break;//DB中的数据已读完
		}
		++readNum;
		tmpRcd.DataFromDbField( queryWrapper.GetCurRcdField(0), queryWrapper.GetCurRcdField(1), queryWrapper.GetCurRcdField(2)
			, queryWrapper.GetCurRcdField(3), queryWrapper.GetCurRcdField(4), queryWrapper.GetCurRcdField(5)
			, queryWrapper.GetCurRcdField(6), queryWrapper.GetCurRcdField(7) );
		m_rcdSet.PushRcd( tmpRcd );
	}

	NewLog( LOG_LEV_DEBUG, "读DB中的表%d:%s完成，读取到的记录数%d", m_rankType, tbName, readNum ); 

	return true;
}

//处理从DB中取到的排行榜缓存数据；
bool CRankDBTask::ProcRankDataFromDB()
{
	CRankCacheManager::InitRankCacheDataFromDB( m_rcdSet );//使用自身数据初始化srv中的缓存；
	return true;
}

//处理从DB中取到的查询表；
bool CRankDBTask::ProcQueryTbFromDB()
{
	CRankCacheManager::InitRankQueryDataFromDB( m_rcdSet );//使用自身数据初始化srv中的查询表；	
	return true;
}

//处理从DB中取到的排行榜信息；
bool CRankDBTask::ProcLoadRankInfo()
{
	CRankCacheManager::InitRankProcInfoFromDB( m_ProcInfo );//使用自身数据初始化srv中的排行榜信息；	
	return true;
}

RankRcdSet::RankRcdSet()
{
	m_arrCacheRecords = NULL;
	ResetRcdSet();
}

//清内部数据，但保留内部数组；
bool RankRcdSet::ClearRcdSetData()
{
	m_curRcdNum = 0;
	return true;
}

//清内部数据，并删除内部数组；
bool RankRcdSet::ResetRcdSet()
{
	ERankType bakType = m_cacheType;
	if ( NULL != m_arrCacheRecords )
	{
		delete [] m_arrCacheRecords; m_arrCacheRecords = NULL;
	}
	m_curRcdNum = 0;
	m_maxRcdNum = 0;
	m_arrCacheRecords = NULL;
	m_cacheType = bakType;//清数据时不清自身类型；
	return true;
}

bool RankRcdSet::InitRankCacheSet( ERankType rankType, bool isDescOrder/*是否降序排序*/, unsigned int innerRcdNum )
{
	ResetRcdSet();
	m_cacheType = rankType;
	m_isDescOrder = isDescOrder;
	m_curRcdNum = 0;
	m_maxRcdNum = innerRcdNum;
	m_arrCacheRecords = NEW RankCacheRcd[m_maxRcdNum];
	return true;
}

//使用vector数据初始化自身；
bool RankRcdSet::InitRankCacheSetWithDataVec( ERankType rankType, bool isDescOrder/*是否降序排序*/, unsigned int innerRcdNum, const vector<RankCacheRcd*>& inVec )
{
	InitRankCacheSet( rankType, isDescOrder, innerRcdNum );
	if ( NULL == m_arrCacheRecords )
	{
		NewLog( LOG_LEV_ERROR, "RankRcdSet::InitRankCacheSetWithDataVec，NULL == m_arrCacheRecords" );
		return false;
	}
	if ( inVec.empty() )
	{
		return true;
	}
	unsigned int validNum = (unsigned int)inVec.size();
	if ( validNum >= m_maxRcdNum )
	{
		validNum = m_maxRcdNum;
	}

	for ( unsigned int i=0; i<validNum; ++i )
	{
		PushRcd( *(inVec[i]) );
	}

	return true;
}//bool RankRcdSet::InitRankCacheSetWithDataVec( ERankType rankType, bool isDescOrder/*是否降序排序*/, unsigned int innerRcdNum, vector<RankCacheRcd*> inVec )

RankRcdSet::~RankRcdSet()
{
	ResetRcdSet();
}

bool RankRcdSet::DataFromOtherSet( const RankRcdSet& rankRcdSet )
{
	unsigned int srcMaxRcdNum = rankRcdSet.GetMaxRcdNum();
	if ( srcMaxRcdNum <= 0 )
	{
		//输入的集中包含0个数据记录，则也将自身置空；
		NewLog( LOG_LEV_ERROR, "RankRcdSet::DataFromOtherSet, 源数据集记录数为0，因此自身也置空，自身类型%d", GetRankType() );
		ClearRcdSetData();
		return true;
	}

	//输入记录数大于0
	unsigned int selfMaxRcdNum = GetMaxRcdNum();//自身最大记录数；
	if ( selfMaxRcdNum != srcMaxRcdNum )
	{
		//重新生成内部数组;
		InitRankCacheSet( rankRcdSet.GetRankType(), rankRcdSet.IsDescOrder(), rankRcdSet.GetMaxRcdNum() );
	}
	selfMaxRcdNum = GetMaxRcdNum();
	if ( selfMaxRcdNum != srcMaxRcdNum )
	{
		//不可能！
		NewLog( LOG_LEV_ERROR, "RankRcdSet::DataFromOtherSet，selfMaxRcdNum != srcMaxRcdNum" );
		ClearRcdSetData();
		return false;
	}

	m_cacheType = rankRcdSet.GetRankType();
	m_isDescOrder = rankRcdSet.IsDescOrder();
	m_curRcdNum = 0;

	RankCacheRcd* pTmpRcd = NULL;
	unsigned int srcValidRcdNum = rankRcdSet.GetValidRcdNum();
	for ( unsigned int i=0; i<srcValidRcdNum,i<selfMaxRcdNum,i<srcMaxRcdNum; ++i )
	{
		pTmpRcd = rankRcdSet.GetIndexRcd(i);
		if ( NULL == pTmpRcd )
		{
			break;
		}
		++m_curRcdNum;
		m_arrCacheRecords[i].DataFrom( *pTmpRcd );
		//StructMemCpy( m_arrCacheRecords[i], pTmpRcd, sizeof(m_arrCacheRecords[i]) );
	}	
	return true;
}

//将自身的排行信息发给客户端;
bool RankRcdSet::NotiRankToPlayer( const PlayerID& playerID )
{
	if ( m_curRcdNum > m_maxRcdNum )
	{
		NewLog( LOG_LEV_ERROR, "NotiRankToPlayer, m_curRcdNum(%d) > m_maxRcdNum(%d)，排行榜%d", m_curRcdNum, m_maxRcdNum, GetRankType() );
		return false;
	}

	if ( m_curRcdNum <= 0 )
	{
		NewLog( LOG_LEV_WARNING, "NotiRankToPlayer, m_curRcdNum(%d) <= 0, 排行榜%d", m_curRcdNum, GetRankType() );
		return true;
	}

	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if ( NULL == pGateSrv )
	{
		NewLog( LOG_LEV_WARNING, "NotiRankToPlayer, NULL == pGateSrv, 排行榜%d", m_curRcdNum, GetRankType() );
		return false;
	}

	FGNewRankInfo notiInfo;//最少会有两个消息；
	notiInfo.notifyPlayerID = playerID;
	notiInfo.rankInfo.startEndFlag = 0;//1:起始，2:结束，0:普通；
	notiInfo.rankInfo.selfVal = 0;//selfVal由gate去填写；
	notiInfo.rankInfo.rankType = GetRankType();
	notiInfo.rankInfo.rankCount = 0;

	bool isFirst = true;
	for ( unsigned int i=0; i<m_curRcdNum; ++i )
	{		
		if ( notiInfo.rankInfo.rankCount >= ARRAY_SIZE( notiInfo.rankInfo.rankInfoArr ) )
		{
			notiInfo.rankInfo.startEndFlag = 0;//1:起始，2:结束，0:普通；
			if ( isFirst )
			{
				notiInfo.rankInfo.startEndFlag = 1;//1:起始，2:结束，0:普通；
			}
			if ( m_curRcdNum-1 == i )
			{
				notiInfo.rankInfo.startEndFlag = 2;//1:起始，2:结束，0:普通；
			}
			//发送已填充数据；
			MsgToPut* pMsgToPut = CreateSrvPkg( FGNewRankInfo, pGateSrv, notiInfo );
			if ( NULL == pMsgToPut )
			{
				NewLog( LOG_LEV_ERROR, "通知排行榜%d时，NULL == pMsgToPut", GetRankType() );
				return false;
			}
			pGateSrv->SendPkgToSrv( pMsgToPut );
			StructMemSet( notiInfo.rankInfo.rankInfoArr, 0, sizeof(notiInfo.rankInfo.rankInfoArr) );
			notiInfo.rankInfo.rankCount = 0;//准备重新开始填充
			if ( 2 == notiInfo.rankInfo.startEndFlag )
			{
				break;
			}
		}
		notiInfo.rankInfo.rankInfoArr[notiInfo.rankInfo.rankCount] = m_arrCacheRecords[i].rankItemInfo;
		++notiInfo.rankInfo.rankCount;
	}

	if ( notiInfo.rankInfo.rankCount > 0 )
	{
		//还有部分剩余需要通知；
		notiInfo.rankInfo.startEndFlag = 2;//1:起始，2:结束，0:普通；
		MsgToPut* pMsgToPut = CreateSrvPkg( FGNewRankInfo, pGateSrv, notiInfo );
		if ( NULL == pMsgToPut )
		{
			NewLog( LOG_LEV_ERROR, "通知排行榜%d时，NULL == pMsgToPut", GetRankType() );
			return false;
		}
		pGateSrv->SendPkgToSrv( pMsgToPut );
	}

	return true;
}

//取最大记录对应玩家；
bool RankRcdSet::GetMaxRcd( RankCacheRcd*& rankCacheRcd, unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ )
{
	if ( m_curRcdNum > m_maxRcdNum )
	{
		NewLog( LOG_LEV_ERROR, "GetMaxRcd, rankType:%s, m_curRcdNum(%d) > m_maxRcdNum(%d)", CRankDBTask::GetRankName(GetRankType()), m_curRcdNum, m_maxRcdNum );
		return false;
	}

	if ( m_curRcdNum <= 0 )
	{
		NewLog( LOG_LEV_ERROR, "GetMaxRcd, rankType:%s, m_curRcdNum(%d) <= 0", CRankDBTask::GetRankName(GetRankType()), m_curRcdNum );
		return false;
	}

	int factor = IsDescOrder() ? 1:-1;
	int maxVal = m_arrCacheRecords[0].rankItemInfo.rankInfo*factor;
	rankCacheRcd = &(m_arrCacheRecords[0]);
	for ( unsigned int i=0; i<m_curRcdNum; ++i )
	{
		if ( 0 != sexFilter )
		{
			//有性别条件；
			if ( (1==sexFilter)/*条件男*/ && (m_arrCacheRecords[i].IsFemale())/*该记录为女*/ )
			{
				continue;
			}
			if ( (2==sexFilter)/*条件女*/ && (!(m_arrCacheRecords[i].IsFemale()))/*该记录非女*/ )
			{
				continue;
			}
		}
		if ( m_arrCacheRecords[i].rankItemInfo.rankInfo*factor > maxVal )
		{
			maxVal = m_arrCacheRecords[i].rankItemInfo.rankInfo;
			rankCacheRcd = &(m_arrCacheRecords[i]);
		}
	}

	return true;
}

bool RankRcdSet::GetMinMax( int& minVal, int& minPlayer, int& maxVal, int& maxPlayer )
{
	if ( m_curRcdNum > m_maxRcdNum )
	{
		NewLog( LOG_LEV_ERROR, "GetMinMax, rankType:%s, m_curRcdNum(%d) > m_maxRcdNum(%d)", CRankDBTask::GetRankName(GetRankType()), m_curRcdNum, m_maxRcdNum );
		return false;
	}

	if ( m_curRcdNum <= 0 )
	{
		NewLog( LOG_LEV_ERROR, "GetMinMax, rankType:%s, m_curRcdNum(%d) <= 0", CRankDBTask::GetRankName(GetRankType()), m_curRcdNum );
		return false;
	}

	minVal = maxVal = m_arrCacheRecords[0].rankItemInfo.rankInfo;
	minPlayer = maxPlayer = m_arrCacheRecords[0].playerUID;
	for ( unsigned int i=0; i<m_curRcdNum; ++i )
	{
		if ( m_arrCacheRecords[i].rankItemInfo.rankInfo < minVal )
		{
			minVal = m_arrCacheRecords[i].rankItemInfo.rankInfo;
			minPlayer = m_arrCacheRecords[i].playerUID;
		}
		if ( m_arrCacheRecords[i].rankItemInfo.rankInfo > maxVal )
		{
			maxVal = m_arrCacheRecords[i].rankItemInfo.rankInfo;
			maxPlayer = m_arrCacheRecords[i].playerUID;
		}
	}
	return true;
}

//设置新的阈值；
bool CRankCache::SetValBias( unsigned int newValBias, unsigned int playerUID )
{
	m_isBiasValid = true;
	m_enterValBias = newValBias;
	m_biasResPlayer = playerUID;
	if ( CRankCache::RCT_CACHE == m_cacheType )
	{
		CRankCacheManager::OnMapSrvLoadRankChartInfo( NULL/*随便选一个gatesrv*/, -1/*通知所有mapsrv*/ );//每当阈值改变时，都即时通知mapsrv;
	}
	return true;
}

//取进入某排行榜的最小阈值
bool CRankCache::GetRankMinBias( ERankType rankType, int& outMinBias )
{
	/*
	    all must rank:
		RANK_LEVEL RANK_DAMAGE RANK_GOLD RANK_SILVER
		RANK_GOODEVIL RANK_KILL	RANK_BEKILL
	*/
	switch (rankType)
	{
	case RANK_LEVEL:
		outMinBias = 5;//最小5级；
		break;
	case RANK_DAMAGE:
		outMinBias = 300;//最小300输出伤害;
		break;
	case RANK_GOLD:
		outMinBias = 500;//最小500金币;
		break;
	case RANK_SILVER:
		outMinBias = 500;//最小500银币;
		break;
	case RANK_GOODEVIL:
		outMinBias = 3;//最小善恶值3;
		break;
	case RANK_KILL:
		outMinBias = 3;//最少杀3人以上;
		break;
	case RANK_BEKILL:
		outMinBias = 3;//最少被杀3次以上;
		break;
	case RANK_FORTUNE:
		outMinBias = 500;//除了数据进榜时刻，总发奖时也要利用cache结构的trycachedata，因此虽然不会在cachedata中用到这一阈值，但仍在此设置一下；
		break;
	case RANK_LEGEND:
		outMinBias = 0;//除了数据进榜时刻，总发奖时也要利用cache结构的trycachedata，因此虽然不会在cachedata中用到这一阈值，但仍在此设置一下；
	default:
		{
			NewLog( LOG_LEV_ERROR, "CRankCache::GetRankMinBias，错误的排行榜类型%d", rankType );
			return false;
		}
	}

	return true;	
}

//使用其它SRV生成的排行榜更新自身(只有星钻排行需要)
bool CRankCache::OnRcvOtherRankInfo( const GCNewRankInfo& otherRankInfo )
{
	if ( RANK_STRDIAM != otherRankInfo.rankType )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::OnRcvOtherRankInfo，非星钻榜调用" );
		return false;
	}
	if ( CRankCache::RCT_QUERY != m_cacheType )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::OnRcvOtherRankInfo，非查询表调用" );
		return false;
	}

	if ( otherRankInfo.rankCount >= ARRAY_SIZE(otherRankInfo.rankInfoArr) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::OnRcvOtherRankInfo，otherRankInfo.rankCount(%d) >= ARRAY_SIZE(otherRankInfo.rankInfoArr)", otherRankInfo.rankCount );
		return false;
	}

	if ( 1 == otherRankInfo.startEndFlag )
	{
		//起始消息；
		SetDbToInit();//设置信息待收；
		ResetRankCache();//清自身；
	} else if ( 2 == otherRankInfo.startEndFlag ) {
		//结束消息；
		SetDbInited();//信息已收完；
	} else if ( 0 == otherRankInfo.startEndFlag ) {
		//中间消息；
		if ( IsDbInited() )
		{
			NewLog( LOG_LEV_ERROR, "OnRcvOtherRankInfo，不可能的情形，更新星钻榜时，dbinit标记已置" );
			return false;
		}
	} else {
		NewLog( LOG_LEV_ERROR, "OnRcvOtherRankInfo，错误的startEndFlag:%d", otherRankInfo.startEndFlag );
		return false;
	}

	RankCacheRcd tmpRcd;
	for ( unsigned char i=0; i<otherRankInfo.rankCount; ++i )//收到的信息填入钻榜；
	{
		tmpRcd.enterMapSeq = 0;
		tmpRcd.playerUID = 0;
		tmpRcd.rankSeqID = 0;
		tmpRcd.rankItemInfo = otherRankInfo.rankInfoArr[i];
		if ( !m_dataSet.PushRcd( tmpRcd ) )
		{
			NewLog( LOG_LEV_ERROR, "OnRcvOtherRankInfo，m_dataSet.PushRcd失败" );
			return false;
		}
	}

	return true;
}

//使用DB数据初始化服务器缓存；
bool CRankCache::InitRankCacheData( const RankRcdSet& rcdSet )
{
	if ( m_dataSet.GetRankType() != rcdSet.GetRankType() ) 
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::InitRankCacheData,输入榜数据类型%d与自身%d不符", rcdSet.GetRankType(), m_dataSet.GetRankType() );
		return false;
	}
	SetDbInited();

	m_dataSet.DataFromOtherSet( rcdSet );

	//确立玩家<-->位置map
	m_playerCachePos.clear();
	RankCacheRcd* pTmpRcd = NULL;
	unsigned int playernum = m_dataSet.GetValidRcdNum();
	for ( unsigned int i=0; i<playernum; ++i )
	{
		pTmpRcd = m_dataSet.GetIndexRcd(i);
		if ( NULL == pTmpRcd )
		{
			NewLog( LOG_LEV_ERROR, "CRankCache::InitRankCacheData,第%d条输入记为空，自身类型%d", i, m_dataSet.GetRankType() );
			continue;
		}

		m_playerCachePos[pTmpRcd->playerUID] = i;
	}

	//确立阈值
	RefreshBiasInfo();

	return true;
}//bool InitRankCacheData( const RankRcdSet& rcdSet )

//使用自身数据更新DB缓存；
bool CRankCache::UpdateRankCacheDataToDB()
{
	if ( CRankCache::RCT_CACHE != m_cacheType )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::UpdateRankCacheDataToDB, CRankCache::RCT_CACHE != m_cacheType(%d)", m_cacheType );
		return false;
	}

	if ( !m_isDbInited )
	{
		NewLog( LOG_LEV_DEBUG, "CRankCache::UpdateRankCacheDataToDB，还未初始化的数据，不向DB更新, rankType:%d", m_dataSet.GetRankType() );
		return false;
	}

	CRankCacheManager::IssueOneRankDBUpdate( m_dataSet.GetRankType(), m_dataSet );
	return true;
}

//尝试将玩家信息加入缓存，若执行了加入操作则返回true;
bool CRankCache::TryCachePlayerInfo(  const RankCacheRcd& rankPlayerInfo, bool isSkipSeqCheck )
{
	if ( !m_isDbInited )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::TryCachePlayerInfo, rank%d，还未曾用DB数据初始化缓存，不应该收到更新数据:", m_dataSet.GetRankType() );
		NewLog( LOG_LEV_ERROR, "%d|%s|%d|%d|%d|%d|%d|"
			, rankPlayerInfo.playerUID, rankPlayerInfo.rankItemInfo.playerName, rankPlayerInfo.rankItemInfo.playerClass, rankPlayerInfo.rankItemInfo.playerLevel
			, rankPlayerInfo.rankItemInfo.rankInfo, rankPlayerInfo.enterMapSeq, rankPlayerInfo.rankSeqID );
		return false;
	}

	if ( !isSkipSeqCheck ) //不忽略sepid检测(只有计算发奖时的tmpcache可以忽略此检测)
	{
		unsigned int thisSeq = 0;
		bool isRankSeqGot = GetRankSeq( thisSeq );
		if ( !isRankSeqGot )
		{
			NewLog( LOG_LEV_ERROR, "CRankCache::TryCachePlayerInfo，取自身rankSeq失败" );
			return false;
		}

		if ( IsRefreshNeedClearCache() //周期性清空的缓存数据
			&& ( rankPlayerInfo.rankSeqID != thisSeq ) )//且到来的rankSeq不等于当前rankSeq，说明为旧数据；
		{
			NewLog( LOG_LEV_DEBUG, "CRankCache::TryCachePlayerInfo, 排行榜%s,输入信息rankSeq(%d)，自身rankSeq(%d)，忽略此更新信息"
				, CRankDBTask::GetRankName(GetRankType()), rankPlayerInfo.rankSeqID, thisSeq );
			return true;
		}
	}

	if ( !IsPlayerInCache( rankPlayerInfo.playerUID ) )
	{
		//玩家不在缓存中，较多数的情况		
		if ( TryAddNewPlayerInfo( rankPlayerInfo ) )//尝试新加入缓存
		{
			//玩家不在缓存中，且缓存未满，且值超过进入阈值，直接加入成功(较少数情况)
			return true;
		}
		//玩家不在缓存中，且缓存已满|不满足进入阈值条件(重复检查了一次)，检查是否超过阈值
		if ( !IsValBeyondBias( rankPlayerInfo.rankItemInfo.rankInfo ) )
		{
			//未超过则无需加入缓存，直接返回
			return false;//绝大多数情况
		}
		//玩家不在缓存中，且缓存已满，且玩家超过阈值，新玩家进缓存，旧阈值玩家出缓存
		//    替换原阈值对应玩家的信息为新信息（加入新玩家信息，删去旧玩家信息），并重新确定阈值
		TryReplacePlayerInfo( rankPlayerInfo );//最少数情况；
	}

	//玩家已在缓存中，直接更新原玩家信息，若玩家为原阈值玩家，则刷新可能改变阈值；
	unsigned int playerPos = m_playerCachePos[rankPlayerInfo.playerUID];
	UpdateCachePlayerInfo( playerPos, rankPlayerInfo );

	return true;
}// bool CRankCache::TryCachePlayerInfo( const RankPlayerInfo& rankPlayerInfo )

//尝试将玩家加入缓存,若缓存未满，且新记录值满足进入阈值条件，则应该加入成功，加入成功时返回true;
bool CRankCache::TryAddNewPlayerInfo( const RankCacheRcd& rankPlayerInfo )
{
	if ( m_dataSet.GetValidRcdNum() >= m_dataSet.GetMaxRcdNum() )
	{
		return false;
	}

	//新记录小于当前阈值
	if ( !IsValBeyondBias( rankPlayerInfo.rankItemInfo.rankInfo ) ) 
	{
		//调用本函数的前提是该玩家不在缓存中，进到此处说明本身不在缓存中且排行值小于当前阈值，不可进缓存。
		return false;
	}

	m_isCacheChanged = true;

	//新加入缓存；
	FlagPlayerInCache( rankPlayerInfo.playerUID, m_dataSet.GetValidRcdNum()/*加入后在缓存中的位置*/ );
	m_dataSet.SetIndexRcd( m_dataSet.GetValidRcdNum(), rankPlayerInfo );
	m_dataSet.IncRcdNum();

	if ( !m_isBiasValid )
	{
		//新加记录导致缓存满时，确定一次阈值(也就是说，缓存未满之前，以最低阈值为阈值，缓存满后，以缓存中的最小值为阈值)
		if ( m_dataSet.IsRankCacheFull() )
		{
			RefreshBiasInfo();
		}
	}

	return true;
}

//尝试替换新玩家(肯定替换原阈值对应玩家)
bool CRankCache::TryReplacePlayerInfo( const RankCacheRcd& newPlayerInfo )
{
	if ( !m_isBiasValid )
	{
		D_ERROR( "TryReplacePlayerInfo, 不可能错误, !m_isBiasValid\n" );
		return false;
	}

	//原阈值玩家信息位置填为新玩家信息
	//原阈值对应玩家出缓存
	//新玩家进缓存
	unsigned int orgPlayerPos = m_playerCachePos[m_biasResPlayer];
	if ( orgPlayerPos >= m_dataSet.GetMaxRcdNum() )
	{
		D_ERROR( "TryReplacePlayerInfo, 不可能错误, orgPlayerPos(%d) >= m_maxRcdNum(%d)\n", orgPlayerPos, m_dataSet.GetMaxRcdNum() );
		return false;
	}

	m_isCacheChanged = true;

	m_dataSet.SetIndexRcd( orgPlayerPos, newPlayerInfo );//原阈值玩家信息位置填为新玩家信息
	FlagPlayerOutCache( m_biasResPlayer );//原阈值对应玩家出缓存
	FlagPlayerInCache( newPlayerInfo.playerUID, orgPlayerPos );//新玩家进缓存

	RefreshBiasInfo();//刷新Bias信息；

	return true;
}//bool TryReplacePlayerInfo( const RankPlayerInfo& newPlayerInfo )

//玩家已在缓存中，直接更新原玩家信息，若玩家为原阈值玩家，则刷新可能改变阈值；
bool CRankCache::UpdateCachePlayerInfo( unsigned int playerPos, const RankCacheRcd& newRankPlayerInfo )
{
	if ( playerPos >= m_dataSet.GetMaxRcdNum() )
	{
		D_ERROR( "CRankCache::UpdateCachePlayerInfo, playerPos(%d) >= m_maxRcdNum(%d)\n", playerPos, m_dataSet.GetMaxRcdNum() );
		return false;
	}

	//if ( m_arrCacheRecords[playerPos].playerUID != newRankPlayerInfo.playerUID )
	if ( m_dataSet.GetIndexPlayerUID(playerPos) != newRankPlayerInfo.playerUID )
	{
		D_ERROR( "CRankCache::UpdateCachePlayerInfo, 不可能错误， m_arrCacheRecords[playerPos].playerUID(%d) != newRankPlayerInfo.playerUID(%d)\n"
			, m_dataSet.GetIndexPlayerUID(playerPos), newRankPlayerInfo.playerUID );
		return false;
	}

	m_isCacheChanged = true;

	m_dataSet.SetIndexRcd( playerPos, newRankPlayerInfo );
	if ( newRankPlayerInfo.playerUID == m_biasResPlayer )
	{
		//是原阈值玩家
		if ( !IsValBeyondBias( newRankPlayerInfo.rankItemInfo.rankInfo ) )
		{
			//新值比旧值要小，则阈值对应玩家不变，但阈值更新为新值
			SetValBias( newRankPlayerInfo.rankItemInfo.rankInfo, newRankPlayerInfo.playerUID );
		} else {
			//新值比旧值要大，可能阈值对应玩家发生了变化，需要刷新阈值信息
			RefreshBiasInfo();
		}
	}

	return true;
}// bool UpdateCachePlayerInfo( unsigned int playerPos, const RankPlayerInfo& newRankPlayerInfo )

//玩家删角色时的处理，若玩家在排行榜中，则将玩家出榜，若玩家为阈值玩家，则刷新阈值；
bool  CRankCache::ProcWhenRoleDelete( unsigned int playerUID )
{
	if ( CRankCache::RCT_CACHE != m_cacheType )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::ProcWhenRoleDelete, CRankCache::RCT_CACHE != m_cacheType(%d)", m_cacheType );
		return false;
	}

	if ( !IsPlayerInCache( playerUID ) )
	{
		return true;
	}

	//将玩家从排行榜中删去；
	unsigned int playerPos = m_playerCachePos[playerUID];
	if ( ( playerPos >= m_dataSet.GetValidRcdNum() ) || ( playerPos >= m_dataSet.GetMaxRcdNum() ) )
	{
		D_ERROR( "CRankCache::ProcWhenRoleDelete，待删角色在排行榜%d中的位置%d错\n", m_dataSet.GetRankType(), playerPos );
		return false;
	}
	if ( m_dataSet.GetValidRcdNum() <= 0 )
	{
		D_ERROR( "CRankCache::ProcWhenRoleDelete，删角色时，排行榜数据记录数为:%d\n", m_dataSet.GetValidRcdNum() );
		return false;
	}

	const RankCacheRcd* pTmpRcd = m_dataSet.GetIndexRcd(m_dataSet.GetValidRcdNum()-1);
	if ( NULL == pTmpRcd )
	{
		D_ERROR( "CRankCache::ProcWhenRoleDelete，删角色时，取不到最末记录，无法调换，记录数:%d\n", m_dataSet.GetValidRcdNum() );
		return false;
	}

	m_dataSet.SetIndexRcd( playerPos, *(pTmpRcd) );//最末记录信息调到待删位置；
	//m_arrCacheRecords[playerPos].DataFrom( m_arrCacheRecords[m_curRcdNum-1] );
	FlagPlayerOutCache( playerUID );//已删玩家出缓存
	m_dataSet.DecRcdNum();//有效记录数--;

	//若所删玩家为阈值玩家，则重新确定阈值；
	if ( playerUID == m_biasResPlayer )
	{
		RefreshBiasInfo();
	}

	return true;
}

//刷新Bias信息；
bool CRankCache::RefreshBiasInfo()
{
	//如果缓存满了，则真正需要确定阈值，否则，以最小阈值为阈值；
	if ( !(m_dataSet.IsRankCacheFull()) )
	{
		m_isBiasValid = false;//阈值无效，以最小阈值为阈值		
		return true;
	}

	int minVal=0, minPlayer=0, maxVal=0, maxPlayer=0;
	bool isMinMaxGot = m_dataSet.GetMinMax( minVal, minPlayer, maxVal, maxPlayer );
	if ( !isMinMaxGot )
	{
		NewLog( LOG_LEV_DEBUG, "可能暂时没有缓存数据，阈值无效，排行榜%d", m_dataSet.GetRankType() );
		return false;
	}
	if ( m_dataSet.IsDescOrder() )
	{
		SetValBias( minVal, minPlayer );
	} else {
		SetValBias( maxVal, maxPlayer );
	}

	return true;
}

//获取排行榜称号玩家
bool CRankCache::GetHonorPlayer( RankCacheRcd*& rankCacheRcd, unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ )
{
	if ( !IsDbInited() )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::GetHonorPlayer, m_cacheType(%d)尚未用DB数据初始化", m_cacheType );
		return false;
	}
	if ( CRankCache::RCT_QUERY != m_cacheType )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::GetHonorPlayer, CRankCache::RCT_QUERY != m_cacheType(%d)", m_cacheType );
		return false;
	}

	return m_dataSet.GetMaxRcd( rankCacheRcd, sexFilter );
}

//前10名填充，用于生成传说英雄榜
bool CRankCache::FillLegendTable( map<unsigned int, RankCacheRcd>& outRst )
{
	if ( CRankCache::RCT_QUERY != m_cacheType )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::FillLegendTable，非查询表调用" );
		return false;
	}

	if ( !IsDbInited() )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::FillLegendTable, %s自身尚未拥有完整数据，失败", CRankDBTask::GetRankName(m_dataSet.GetRankType()) );
		return false;
	}
	RankCacheRcd* pRankRcd = NULL;

	//挑选出前10名，依次赋分；
	//1、将自身数据添加到一个vector；
	//2、对vector排序
	//3、前10名依次赋分；
	vector<RankCacheRcd*> tmpSortVec;
	for ( unsigned int i=0; i<m_dataSet.GetValidRcdNum(); ++i )
	{
		pRankRcd = m_dataSet.GetIndexRcd( i );
		if ( NULL == pRankRcd )
		{
			NewLog( LOG_LEV_ERROR, "CRankCache::FillLegendTable, %s取记录失败", CRankDBTask::GetResTbName(m_dataSet.GetRankType()) );
			return false;
		}
		tmpSortVec.push_back( pRankRcd );
	}

	sort( tmpSortVec.begin(), tmpSortVec.end(), CacheRcdComp );

	//赋相应分值(第一名10分，第二名9分，依次类推)；
	int curval = 10;
	RankCacheRcd* pCacheRcd = NULL;
	for ( vector<RankCacheRcd*>::iterator iter=tmpSortVec.begin(); iter!=tmpSortVec.end(); ++iter )
	{
		pCacheRcd = *iter;
		if ( NULL == pCacheRcd )
		{
			NewLog( LOG_LEV_ERROR, "CRankCache::FillLegendTable, NULL == pCacheRcd" );
			continue;
		}
		if ( outRst.end() == outRst.find( pRankRcd->playerUID ) )
		{
			//原来无此玩家记录,新加;
			outRst.insert( pair<unsigned int, RankCacheRcd>( pRankRcd->playerUID, RankCacheRcd() ) );
			outRst[pRankRcd->playerUID].DataFrom( *pCacheRcd );
			outRst[pRankRcd->playerUID].rankItemInfo.rankInfo = 0;
		} 
		//原来有此玩家记录，更新值
		outRst[pRankRcd->playerUID].rankItemInfo.rankInfo += curval;
		--curval;
		if ( curval <= 0 )
		{
			break;
		}
	}

	return true;
}// bool CRankCache::FillLegendTable( map<unsigned int, RankCacheRcd>& outRst )

//增量加因子填充，用于根据多个查询表生成二级查询表(财富榜，传说英雄榜)
bool CRankCache::FillIncTableWithFactor( map<unsigned int, RankCacheRcd>& outRst, float multiFactor )
{
	if ( CRankCache::RCT_QUERY != m_cacheType )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::FillIncTableWithFactor，非查询表调用" );
		return false;
	}

	if ( !IsDbInited() )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::FillIncTableWithFactor, %s自身尚未拥有完整数据，失败", CRankDBTask::GetRankName(m_dataSet.GetRankType()) );
		return false;
	}
	RankCacheRcd* pRankRcd = NULL;
	for ( unsigned int i=0; i<m_dataSet.GetValidRcdNum(); ++i )
	{
		pRankRcd = m_dataSet.GetIndexRcd( i );
		if ( NULL == pRankRcd )
		{
			NewLog( LOG_LEV_ERROR, "CRankCache::FillIncTableWithFactor, %s取记录失败", CRankDBTask::GetResTbName(m_dataSet.GetRankType()) );
			return false;
		}
		if ( outRst.end() == outRst.find( pRankRcd->playerUID ) )
		{
			//原来无此玩家记录,新加;
			outRst.insert( pair<unsigned int, RankCacheRcd>( pRankRcd->playerUID, RankCacheRcd() ) );
			outRst[pRankRcd->playerUID].DataFrom( *pRankRcd );
			outRst[pRankRcd->playerUID].rankItemInfo.rankInfo = 0;
		}
		//原来有此玩家记录，更新值
		outRst[pRankRcd->playerUID].rankItemInfo.rankInfo += (int)(multiFactor*pRankRcd->rankItemInfo.rankInfo);
	}
	return true;
}

//若自身缓存内容发生过变化，则存盘
bool CRankCache::CheckIfSelfNeedSave()
{
	if ( !m_isCacheChanged )
	{
		return false;
	}

	m_isCacheChanged = false;
	UpdateRankCacheDataToDB();

	return true;
}

//生成新查询表时是否需要清之前缓存；
bool CRankCache::IsRefreshNeedClearCache()
{
	ERankType rankType = GetRankType();
	if ( ( RANK_LEVEL == rankType ) || ( RANK_GOLD == rankType ) || ( RANK_SILVER == rankType ) || ( RANK_GOODEVIL == rankType ) )
	{
		return false;//这些数值都不是周期性的，无法清；
	}
	return true;//其余值计算的是周期内的，每周期清；
}

//刷新缓存(根据当前缓存重新生成查询表，发奖，清空当前数据缓存)
bool CRankCache::RefreshRankCache( unsigned int newRankSeq )
{
	/*
	1、使用自身信息更新当前查询表(函数内部将查询表更新至DB，同时根据新查询表进行奖励发放)
	2、清自身缓存信息
	*/
	if ( CRankCache::RCT_CACHE != m_cacheType )
	{
		NewLog( LOG_LEV_ERROR, "CRankCache::RefreshRankCache, CRankCache::RCT_CACHE != m_cacheType(%d)", m_cacheType );
		return false;
	}

	//1、使用自身信息更新当前查询表(函数内部包括：新的查询表更新至DB查询表，根据新查询表进行奖励发放，新称号通知)
	CRankCacheManager::GenQueryTableFromCache( m_dataSet );

	//2、清自身缓存信息
	SetRankSeq( newRankSeq );

	if ( IsRefreshNeedClearCache() )//生成新查询表时是否需要清之前缓存；
	{
		m_dataSet.ClearRcdSetData();//清缓存；
		RefreshBiasInfo();//刷新阈值；
		UpdateRankCacheDataToDB();//每次清空缓存后，立刻将空缓存更新至DB；
	}

	return true;	
};//bool CRankCache::RefreshRankCache( unsigned int newRankSeq )

//将自身的排行信息发给客户端;
bool CRankCache::NotiRankToPlayer( const PlayerID& queryPlayerID )
{
	m_dataSet.NotiRankToPlayer( queryPlayerID );	
	return true;
}

//通知新的称号拥有者；
bool CRankCache::NotifyRankHonorPlayer( unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ )
{
	RankCacheRcd* honorPlayerInfo = NULL;
	bool isGot = GetHonorPlayer( honorPlayerInfo, sexFilter );
	if ( ( !isGot ) || ( NULL == honorPlayerInfo ) )
	{
		NewLog( LOG_LEV_ERROR, "NotifyRankHonorPlayer时，%s中暂时没有玩家拥有相应称号", CRankDBTask::GetRankName(m_dataSet.GetRankType()) );
		return false;
	}

	//以下广播称号拥有者；
	FGHonorPlayer honorPlayer;
	honorPlayer.honorPlayer.honorType = CRankDBTask::GetRankResHonorMask( m_dataSet.GetRankType(), sexFilter );
	SafeStrCpy( honorPlayer.honorPlayer.nickName, honorPlayerInfo->rankItemInfo.playerName );
	honorPlayer.honorPlayer.nickNameLen = (unsigned int) min( ARRAY_SIZE(honorPlayer.honorPlayer.nickName), strlen( honorPlayer.honorPlayer.nickName ) );

	vector<CGateServer*>& gateServers = GateSrvMangerSingle::instance()->GetGateSrvVec();
	if ( gateServers.empty() )
	{
		NewLog( LOG_LEV_ERROR, "NotifyRankHonorPlayer时，%s，暂时没有连入的gatesrv，放弃通知", CRankDBTask::GetRankName(m_dataSet.GetRankType()) );
		return false;
	}

	CGateServer* pTmpGate = NULL;
	for ( vector<CGateServer*>::iterator iter=gateServers.begin(); iter!=gateServers.end(); ++iter )
	{
		pTmpGate = *iter;
		if ( NULL == pTmpGate )
		{
			NewLog( LOG_LEV_ERROR, "NotifyRankHonorPlayer时，排行榜%d，NULL == pTmpGate", m_dataSet.GetRankType() );
			continue;
		}
		MsgToPut* pMsgToPut = CreateSrvPkg( FGHonorPlayer, pTmpGate, honorPlayer );
		pTmpGate->SendPkgToSrv( pMsgToPut );
	}

	return true;
}

//给缓存中的所有玩家发指定奖励，只由SendReword中的临时缓存调用!!!
bool CRankCache::RewordAllPlayer( unsigned int rewordID, unsigned int rewordNum )
{
	ItemInfo_i rewordArr[MAX_MAIL_ATTACHITEM_COUNT];
	StructMemSet( rewordArr, 0x0, sizeof(rewordArr) );
	rewordArr[0].ucCount = rewordNum;
	rewordArr[0].uiID = rewordID;

	CGateServer* pGateServer = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if ( NULL == pGateServer )
	{
		NewLog( LOG_LEV_ERROR, "RewordAllPlayer时，排行榜%d，没有合适的gatesrv", m_dataSet.GetRankType() );
		return false;
	}

	const char* rankName = CRankDBTask::GetRankName( m_dataSet.GetRankType() );
	if ( NULL == rankName )
	{
		NewLog( LOG_LEV_ERROR, "RewordAllPlayer时，取%d的排行榜对应名字失败", m_dataSet.GetRankType() );
		return false;
	}

	stringstream ss;
	ss << "您进入" << rankName << "获得的奖励";

	unsigned int rewordPlayerNum = m_dataSet.GetValidRcdNum();
	RankCacheRcd* cacheRcd = NULL;
	//CRankDBTask::GetRankName
	for ( unsigned int i=0; i<rewordPlayerNum; ++i )
	{
		cacheRcd = m_dataSet.GetIndexRcd( i );
		if ( NULL == cacheRcd )
		{
			NewLog( LOG_LEV_DEBUG, "排行榜%d奖励人数%d，排行榜有效人数%d，未满", m_dataSet.GetRankType(), rewordPlayerNum, m_dataSet.GetValidRcdNum() );
			break;
		}
		SupectMailManagerSingle::instance()->CreateSuspectSystemMail( pGateServer->GetSessionID(),
			cacheRcd->rankItemInfo.playerName, "排行榜奖励", ss.str().c_str(), 0/*金币数*/, rewordArr );
		//检查修改，区分系统发信者的查询与玩家发信者查询（目标是否存在通知）， CSupectMailManager::AffairMail;
	}

	return true;
}//给缓存中的所有玩家发指定奖励，只由SendReword中的临时缓存调用!!!

bool RankProcInfo::DataFromDbField( const char* inRankType, const char* inRankSeq, const char* inUpdateTime )
{
	if ( (NULL == inRankType) || (NULL == inRankSeq) || (NULL == inUpdateTime) )
	{
		return false;
	}

	m_rankType = (ERankType)atoi( inRankType );
	m_rankSeq = (unsigned int)atoi( inRankSeq );
	m_updateTime = (unsigned int)atoi( inUpdateTime );

	m_isDbInited = true;

	return true;
}


//初始化排行榜数据缓存管理器(读配置，初始化，等等，待实现)；
bool CRankCacheManager::InitRankCacheManager()
{
	/*
	RANK_LEVEL    = 0, //等级排行榜 RANK_DAMAGE   = 1, //输出伤害排行榜	RANK_GOLD     = 2, //金币排行榜	RANK_SILVER   = 3, //银币排行榜
	RANK_STRDIAM  = 4, //星钻排行榜	RANK_FORTUNE  = 5, //财富排行榜	RANK_GOODEVIL = 6, //善恶值排行榜	RANK_UNION    = 7, //战盟声望排行榜
	RANK_KILL     = 8, //击杀数排行榜	RANK_BEKILL   = 9, //被击杀数排行榜	RANK_PRESTIGE = 10,//玩家声望(废弃)	RANK_BEDAMAGE = 11,//玩家被伤害(废弃)
	RANK_TYPE_NUM = 12 //排行榜类型个数(准备用于数组定义)
	*/
	CRankConfigManager::ReadRankConfig();//读排行榜配置文件；

	m_arrRankCache[RANK_LEVEL].InitRankCache( CRankCache::RCT_CACHE, RANK_LEVEL, false, 200 );
	m_arrRankCache[RANK_DAMAGE].InitRankCache( CRankCache::RCT_CACHE, RANK_DAMAGE, false, 200 );
	m_arrRankCache[RANK_GOLD].InitRankCache( CRankCache::RCT_CACHE, RANK_GOLD, false, 200 );//可升降排行
	m_arrRankCache[RANK_SILVER].InitRankCache( CRankCache::RCT_CACHE, RANK_SILVER, false, 200 );//可升降排行
	m_arrRankCache[RANK_GOODEVIL].InitRankCache( CRankCache::RCT_CACHE, RANK_GOODEVIL, false, 200 );//可升降排行
	m_arrRankCache[RANK_KILL].InitRankCache( CRankCache::RCT_CACHE, RANK_KILL, false, 200 );
	m_arrRankCache[RANK_BEKILL].InitRankCache( CRankCache::RCT_CACHE, RANK_BEKILL, false, 200 );

	m_arrRankQuery[RANK_LEVEL].InitRankCache( CRankCache::RCT_QUERY, RANK_LEVEL, false, 200 );
	m_arrRankQuery[RANK_DAMAGE].InitRankCache( CRankCache::RCT_QUERY, RANK_DAMAGE, false, 200 );
	m_arrRankQuery[RANK_GOLD].InitRankCache( CRankCache::RCT_QUERY, RANK_GOLD, false, 200 );//可升降排行
	m_arrRankQuery[RANK_SILVER].InitRankCache( CRankCache::RCT_QUERY, RANK_SILVER, false, 200 );//可升降排行
	m_arrRankQuery[RANK_GOODEVIL].InitRankCache( CRankCache::RCT_QUERY, RANK_GOODEVIL, false, 200 );//可升降排行
	m_arrRankQuery[RANK_KILL].InitRankCache( CRankCache::RCT_QUERY, RANK_KILL, false, 200 );
	m_arrRankQuery[RANK_BEKILL].InitRankCache( CRankCache::RCT_QUERY, RANK_BEKILL, false, 200 );

	//星钻榜,财富榜和传说英雄榜只初始化查询表
	//  其中星钻榜来自shopsrv，且不会接受玩家查询(只供计算财富榜和传说英雄榜使用)；
	//      财富榜和传说英雄榜在更新时刻到来时，根据其它表计算而来；
	m_arrRankQuery[RANK_STRDIAM].InitRankCache( CRankCache::RCT_QUERY, RANK_STRDIAM, false, 100 );
	m_arrRankQuery[RANK_FORTUNE].InitRankCache( CRankCache::RCT_QUERY, RANK_FORTUNE, false, 100 );
	m_arrRankQuery[RANK_LEGEND].InitRankCache( CRankCache::RCT_QUERY, RANK_LEGEND, false, 100 );

	CRankDBTask::InitInnerPool();//排行榜相关任务池初始化；		

	m_DBIns.InitRankDBIns( CDBConfigManagerSingle::instance()->Find() );//双线程对象启动；

	IssueAllRankDBLoad();//发出load指令，从DB中装入排行榜缓存数据；

	m_lastRefreshCheckTime = GetTickCount();

	return true;
}

//发出load指令，从DB中装入排行榜缓存数据与查询数据；
bool CRankCacheManager::IssueAllRankDBLoad()
{
	IssueOneCacheDBLoad( RANK_LEVEL );
	IssueOneCacheDBLoad( RANK_DAMAGE );
	IssueOneCacheDBLoad( RANK_GOLD );
	IssueOneCacheDBLoad( RANK_SILVER );
	IssueOneCacheDBLoad( RANK_GOODEVIL );
	IssueOneCacheDBLoad( RANK_KILL );
	IssueOneCacheDBLoad( RANK_BEKILL );

	IssueOneQueryDBLoad( RANK_LEVEL );
	IssueOneQueryDBLoad( RANK_DAMAGE );
	IssueOneQueryDBLoad( RANK_GOLD );
	IssueOneQueryDBLoad( RANK_SILVER );
	IssueOneQueryDBLoad( RANK_FORTUNE );
	IssueOneQueryDBLoad( RANK_GOODEVIL );
	IssueOneQueryDBLoad( RANK_KILL );
	IssueOneQueryDBLoad( RANK_BEKILL );
	IssueOneQueryDBLoad( RANK_LEGEND );

	return true;
}

bool CRankCacheManager::IssueOneQueryDBLoad( ERankType rankType )
{
	//查询数据
	CRankDBTask* pTmpTask = CRankDBTask::NewRankDBTask();
	if ( NULL == pTmpTask )
	{
		NewLog( LOG_LEV_ERROR, "IssueOneCacheDBLoad, 查询数据, NULL == pTmpTask, rankType:%d", rankType );
		return false;
	}
	pTmpTask->SetRankTaskInfo( CRankDBTask::QUERY_TB_READ, rankType, false/*目前的排行榜全是由大至小排列*/, NULL, NULL );
	m_DBIns.AddOneAssistTask( pTmpTask );

	//排行榜更新时间序号等数据
	pTmpTask = CRankDBTask::NewRankDBTask();
	if ( NULL == pTmpTask )
	{
		NewLog( LOG_LEV_ERROR, "IssueOneCacheDBLoad, 处理信息, NULL == pTmpTask, rankType:%d", rankType );
		return false;
	}
	pTmpTask->SetRankTaskInfo( CRankDBTask::LOAD_RANKINFO, rankType, false/*目前的排行榜全是由大至小排列*/, NULL, NULL );
	m_DBIns.AddOneAssistTask( pTmpTask );

	return true;
}

//指令从DB中装入某排行榜缓存数据与查询数据；
bool CRankCacheManager::IssueOneCacheDBLoad( ERankType rankType )
{
	//缓存数据
	CRankDBTask* pTmpTask = CRankDBTask::NewRankDBTask();
	if ( NULL == pTmpTask )
	{
		NewLog( LOG_LEV_ERROR, "IssueOneCacheDBLoad, 缓存数据, NULL == pTmpTask, rankType:%d", rankType );
		return false;
	}
	pTmpTask->SetRankTaskInfo( CRankDBTask::GET_RANK_DATA, rankType, false/*目前的排行榜全是由大至小排列*/, NULL, NULL );
	m_DBIns.AddOneAssistTask( pTmpTask );

	return true;
}

//指令向DB更新某排行榜缓存数据；
bool CRankCacheManager::IssueOneRankDBUpdate( ERankType rankType, const RankRcdSet& rankSet )
{
	CRankDBTask* pTmpTask = CRankDBTask::NewRankDBTask();
	if ( NULL == pTmpTask )
	{
		NewLog( LOG_LEV_ERROR, "IssueOneRankDBUpdate, NULL == pTmpTask, rankType:%d", rankSet.GetRankType() );
		return false;
	}
	pTmpTask->SetRankTaskInfo( CRankDBTask::UPDATE_RANK_DATA, rankSet.GetRankType(), false/*目前的排行榜全是由大至小排列*/, &rankSet, NULL );
	m_DBIns.AddOneAssistTask( pTmpTask );
	return true;
}

bool CRankCacheManager::OnQueryRankChartInfoByPageIndex( CGateServer* pOwner, const PlayerID& queryPlayerID, ERankType rankType ) 
{ 
	if ( rankType > ARRAY_SIZE(m_arrRankQuery) )
	{
		NewLog( LOG_LEV_WARNING, "CRankCacheManager::OnQueryRankChartInfoByPageIndex，玩家(%d:%d)请求的排行榜类型%d错误"
			, queryPlayerID.wGID, queryPlayerID.dwPID, rankType );
		return false;
	}

	m_arrRankQuery[rankType].NotiRankToPlayer( queryPlayerID );

	return true; 
};

bool CRankCacheManager::OnMapSrvLoadRankChartInfo( CGateServer* pOwner, unsigned int mapsrvID  ) 
{ 
	if ( NULL == pOwner )
	{
		pOwner = GateSrvMangerSingle::instance()->GetValidGateSrv();
		if ( NULL == pOwner )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::OnMapSrvLoadRankChartInfo, NULL == pOwner" );
			return false;
		}
	}

	if ( ! m_isAllRankDbLoad )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::OnMapSrvLoadRankChartInfo, 排行榜数据还未初始化，稍后初始化后会全部通知" );
		return false;
	}

	//通知此mapsrv各排行榜阈值；
	FGBiasInfo biasInfo;
	biasInfo.notiMapSrvID = mapsrvID;
	for ( unsigned int i=0; i<ARRAY_SIZE(m_arrRankCache); ++i )
	{
		ERankType tmpRankType = (ERankType)i;
		if ( ( RANK_STRDIAM == tmpRankType ) 
			|| ( RANK_UNION == tmpRankType )
			|| ( RANK_FORTUNE == tmpRankType )
			|| ( RANK_LEGEND == tmpRankType )
			)
		{
			//这些榜或者为二级榜或者为其它服务器的生成榜，所以阈值不由本地管理；
			continue;
		}
		if ( ( i >= ARRAY_SIZE(biasInfo.arrBias) ) 
			|| ( i >= ARRAY_SIZE(biasInfo.arrRankSeq) ) 
			)
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::OnMapSrvLoadRankChartInfo, i(%d)越界", i );
			return false;
		}
		if ( ( !m_arrRankCache[i].GetBias(biasInfo.arrBias[i]) )
			|| ( !m_arrRankCache[i].GetRankSeq(biasInfo.arrRankSeq[i]) )
			)
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::OnMapSrvLoadRankChartInfo, 取排行榜%d的阈值时失败", i );
			return false;
		}
	}

	MsgToPut* pMsgToPut = CreateSrvPkg( FGBiasInfo, pOwner, biasInfo );
	pOwner->SendPkgToSrv(pMsgToPut);

	return true; 
};

//检查之前是否有mapsrv需要通知而未通知
bool CRankCacheManager::NotiMapSrvNewBias()
{
	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if( NULL == pGateSrv )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::NotiMapSrvNewBias，找不到有效的gateserver" );
		return false;
	}

	OnMapSrvLoadRankChartInfo( pGateSrv, -1 );

	return true;
}

//玩家删角色，若该角色在缓存数据中，则应将此角色信息去除；
bool CRankCacheManager::OnPlayerDeleteRole( unsigned int playerUID ) 
{
	/*
	    all must rank:
		RANK_LEVEL RANK_DAMAGE RANK_GOLD RANK_SILVER
		RANK_GOODEVIL RANK_KILL	RANK_BEKILL
	*/
	RankCacheDealPlayerDel( playerUID, RANK_LEVEL );
	RankCacheDealPlayerDel( playerUID, RANK_DAMAGE );
	RankCacheDealPlayerDel( playerUID, RANK_GOLD );
	RankCacheDealPlayerDel( playerUID, RANK_SILVER );
	RankCacheDealPlayerDel( playerUID, RANK_GOODEVIL );
	RankCacheDealPlayerDel( playerUID, RANK_KILL );
	RankCacheDealPlayerDel( playerUID, RANK_BEKILL );
	return true;
};

//特定排行榜处理玩家删角色；
bool CRankCacheManager::RankCacheDealPlayerDel( unsigned int playerUID, ERankType rankType )
{
	if ( rankType >= ARRAY_SIZE(m_arrRankCache) )
	{
		NewLog( LOG_LEV_ERROR, "RankCacheDealPlayerDel，类型%d不可识别", rankType );
		return false;
	}
	m_arrRankCache[rankType].ProcWhenRoleDelete( playerUID );
	return true;
}

//处理其它srv生成的排行榜
bool CRankCacheManager::OnRcvOtherRank( GFOtherRank* pOtherRank )
{
	if ( NULL == pOtherRank )
	{
		NewLog( LOG_LEV_ERROR, "OnRcvOtherRank, NULL == pOtherRank" );
		return false;
	}

	//只处理星钻排行，工会排行functionsrv不必理会，其余排行都由functionsrv自身生成；
	if ( RANK_STRDIAM != pOtherRank->rankInfo.rankType )
	{
		NewLog( LOG_LEV_ERROR, "OnRcvOtherRank，错误的rankType:%d", pOtherRank->rankInfo.rankType );
		return false;
	}

	if ( RANK_STRDIAM >= ARRAY_SIZE(m_arrRankQuery) )
	{
		NewLog( LOG_LEV_ERROR, "OnRcvOtherRank，m_arrRankQuery size:%d错误", ARRAY_SIZE(m_arrRankQuery) );
		return false;
	}

	m_arrRankQuery[RANK_STRDIAM].OnRcvOtherRankInfo( pOtherRank->rankInfo );

	return true;
}

//处理新收到的cacheInfo
bool CRankCacheManager::OnRcvNewCacheInfo( GFRankCacheInfo* pRankCacheInfo )
{
	if ( NULL == pRankCacheInfo )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::OnRcvNewCacheInfo, NULL == pRankCacheInfo" );
		return false;
	}
	ERankType rankType = pRankCacheInfo->rankType;
	if ( rankType >= ARRAY_SIZE(m_arrRankCache) )
	{
		NewLog( LOG_LEV_ERROR, "错误的排行榜缓存更新消息，类型%d不可识别", rankType );
		return false;
	}
	if ( RANK_LEVEL == rankType )
	{
		//等级榜的playerLevel实际存放等级到达时间，而等级本身已由rankInfo保存；
		pRankCacheInfo->cacheRcd.rankItemInfo.playerLevel = (unsigned int)time(NULL);
	}
	m_arrRankCache[rankType].TryCachePlayerInfo( pRankCacheInfo->cacheRcd );
	return true;
}//bool CRankCacheManager::OnRcvNewCacheInfo( GFRankCacheInfo*  )

//使用DB数据初始化服务器缓存；
bool CRankCacheManager::InitRankCacheDataFromDB( const RankRcdSet& rcdSet )
{
	if ( rcdSet.GetRankType() >= ARRAY_SIZE(m_arrRankCache) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::InitRankCacheDataFromDB, 错误的rankType:%d", rcdSet.GetRankType() );
		return false;
	}
	if ( m_arrRankCache[rcdSet.GetRankType()].IsDbInited() )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::InitRankCacheDataFromDB, %s查询榜重复使用DB数据初始化", CRankDBTask::GetRankName( rcdSet.GetRankType() ) );
		return false;
	}

	bool isOK = m_arrRankCache[rcdSet.GetRankType()].InitRankCacheData( rcdSet );
	CheckIfAllNeedRankDbInited();//每load一个新表，检查一次是否所有数据(缓存与查询)都已用DB数据初始化；
	return isOK;
}

//使用自身数据初始化srv中的排行榜信息；
bool CRankCacheManager::InitRankProcInfoFromDB( const RankProcInfo& procInfo )
{
	if ( procInfo.m_rankType >= ARRAY_SIZE(m_arrRankProcInfo) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::InitRankProcInfoFromDB, 错误的rankType:%d", procInfo.m_rankType );
		return false;
	}
	bool isOK = m_arrRankProcInfo[procInfo.m_rankType].DataFromOtherProcInfo( procInfo );
	CheckIfAllNeedRankDbInited();//每load一个新表，检查一次是否所有数据(缓存与查询)都已用DB数据初始化；
	return true;
}

//使用自身数据初始化srv中的查询表；
bool CRankCacheManager::InitRankQueryDataFromDB( const RankRcdSet& rcdSet )
{
	if ( rcdSet.GetRankType() >= ARRAY_SIZE(m_arrRankQuery) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::InitRankQueryDataFromDB, 错误的rankType:%d", rcdSet.GetRankType() );
		return false;
	}
	if ( m_arrRankQuery[rcdSet.GetRankType()].IsDbInited() )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::InitRankQueryDataFromDB, %s查询榜重复使用DB数据初始化", CRankDBTask::GetRankName( rcdSet.GetRankType() ) );
		return false;
	}

	if ( InitRankQueryWithData( rcdSet ) )
	{
	    CheckIfAllNeedRankDbInited();//每load一个新表，检查一次是否所有数据(缓存与查询)都已用DB数据初始化；
		return true;
	} else {
		return false;
	}
}

//使用数据更新查询表；
bool CRankCacheManager::InitRankQueryWithData( const RankRcdSet& rcdSet )
{
	if ( rcdSet.GetRankType() >= ARRAY_SIZE(m_arrRankCache) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::InitRankQueryDataFromDB, 错误的rankType:%d", rcdSet.GetRankType() );
		return false;
	}
	bool isOK = m_arrRankQuery[rcdSet.GetRankType()].InitRankCacheData( rcdSet );//说是缓存，其实其中数据只供查询；	
	return isOK;
}

//1、使用自身信息更新当前查询表(函数内部包括：新的查询表更新至DB查询表，根据新查询表进行奖励发放，新称号通知)
bool CRankCacheManager::GenQueryTableFromCache( const RankRcdSet& rcdSet )
{
	//1、更新缓存查询表
	InitRankQueryWithData( rcdSet );//利用DB更新函数更新查询表；
	//2、新的查询表更新至DB查询表
	IssueUpdateQueryTB( rcdSet.GetRankType(), rcdSet );
	//3、奖励发放；
	SendRankReward( rcdSet.GetRankType() );

	//4、新称号通知；
	if ( RANK_LEGEND != rcdSet.GetRankType() )
	{
		NotifyHonorPlayer( rcdSet.GetRankType(), 0 );
	} else {
		NotifyHonorPlayer( rcdSet.GetRankType(), 1/*天王*/ );
		NotifyHonorPlayer( rcdSet.GetRankType(), 2/*天后*/ );
	}
	
	return true;
}

//发出指令：查询表更新至DB；
bool CRankCacheManager::IssueUpdateQueryTB( ERankType rankType, const RankRcdSet& rcdSet )
{
	if ( rankType >= ARRAY_SIZE(m_arrRankQuery) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::IssueUpdateQueryTB, 错误的rankType:%d", rankType );
		return false;
	}

	if ( rankType != rcdSet.GetRankType() )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::IssueUpdateQueryTB, 输入参数中rankType不一致(%d,%d)", rankType, rcdSet.GetRankType() );
		return false;
	}

	CRankDBTask* pTmpTask = CRankDBTask::NewRankDBTask();
	if ( NULL == pTmpTask )
	{
		NewLog( LOG_LEV_ERROR, "IssueUpdateQueryTB, NULL == pTmpTask, rankType:%d", rankType );
		return false;
	}

	pTmpTask->SetRankTaskInfo( CRankDBTask::QUERY_TB_WRITE, rankType, false/*目前的排行榜全是由大至小排列*/, &rcdSet, NULL );
	m_DBIns.AddOneAssistTask( pTmpTask );

	return true;
}

//检查是否所有需要的排行榜缓存数据和查询数据都已根据DB表初始化；
bool CRankCacheManager::CheckIfAllNeedRankDbInited()
{
	if ( m_isAllRankDbLoad )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfAllNeedRankDbInited，不可能，所有表都初始化后又收到了新的表load信息" );
		return true;
	}

	/*
	    all must rank:
		RANK_LEVEL RANK_DAMAGE RANK_GOLD RANK_SILVER
		RANK_GOODEVIL RANK_KILL	RANK_BEKILL
	*/
	m_isAllRankDbLoad = CheckIfCacheDbInited( RANK_LEVEL) 
		&& CheckIfCacheDbInited( RANK_DAMAGE ) 
		&& CheckIfCacheDbInited( RANK_GOLD )
		&& CheckIfCacheDbInited( RANK_SILVER )
		&& CheckIfCacheDbInited( RANK_GOODEVIL )
		&& CheckIfCacheDbInited( RANK_KILL )
		&& CheckIfCacheDbInited( RANK_BEKILL )//以上缓存表
		&& CheckIfQueryDbInited( RANK_LEVEL) 
		&& CheckIfQueryDbInited( RANK_DAMAGE ) 
		&& CheckIfQueryDbInited( RANK_GOLD )
		&& CheckIfQueryDbInited( RANK_SILVER )
		&& CheckIfQueryDbInited( RANK_FORTUNE )
		&& CheckIfQueryDbInited( RANK_GOODEVIL )
		&& CheckIfQueryDbInited( RANK_KILL )
		&& CheckIfQueryDbInited( RANK_BEKILL )
		&& CheckIfQueryDbInited( RANK_LEGEND );//以上查询表；

	if ( m_isAllRankDbLoad )
	{
		OnAllDataInitedWithDBInfo();//所有数据(缓存与查询)都已使用DB信息初始化；
	}

	return m_isAllRankDbLoad;
}

//设置各缓冲的排行榜序号；
bool CRankCacheManager::SetAllCacheSeq()
{
	/*
	    all must rank:
		RANK_LEVEL RANK_DAMAGE RANK_GOLD RANK_SILVER
		RANK_GOODEVIL RANK_KILL	RANK_BEKILL
	*/
	SetOneCacheSeq( RANK_LEVEL );
	SetOneCacheSeq( RANK_DAMAGE );
	SetOneCacheSeq( RANK_GOLD );
	SetOneCacheSeq( RANK_SILVER );
	SetOneCacheSeq( RANK_GOODEVIL );
	SetOneCacheSeq( RANK_KILL );
	SetOneCacheSeq( RANK_BEKILL );
	return true;
}

//设置某缓冲的排行榜序号
bool CRankCacheManager::SetOneCacheSeq( ERankType rankType )
{
	if ( ( rankType >= ARRAY_SIZE( m_arrRankCache ) ) 
		|| ( rankType >= ARRAY_SIZE( m_arrRankProcInfo ) ) 
		)
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::SetOneCacheSeq, 无效的rankType:%d", rankType );
		return false;
	}

	m_arrRankCache[rankType].SetRankSeq( m_arrRankProcInfo[rankType].GetRankSeq() );

	return true;
}

//排行榜相关信息通知gatesrv;
bool CRankCacheManager::NotiRankResInfoToGateSrv()
{
	//mapsrv阈值通知；
	NotiMapSrvNewBias();

	//通知各称号拥有者
	NotifyAllHonorPlayer();

	return true;
}

//所有数据(缓存与查询)都已使用DB信息初始化；
bool CRankCacheManager::OnAllDataInitedWithDBInfo()
{

	SetAllCacheSeq();//设置各缓冲的排行榜序号；

	NotiRankResInfoToGateSrv();//通知gatesrv排行榜相关消息；

	return true;
}

//所有称号玩家通知
bool CRankCacheManager::NotifyAllHonorPlayer()
{
	/*
	RANK_LEVEL    = 0, //等级排行榜
	RANK_DAMAGE   = 1, //输出伤害排行榜
	RANK_GOLD     = 2, //金币排行榜
	RANK_SILVER   = 3, //银币排行榜
	RANK_STRDIAM  = 4, //星钻排行榜
	RANK_FORTUNE  = 5, //财富排行榜
	RANK_GOODEVIL = 6, //善恶值排行榜
	RANK_UNION    = 7, //战盟声望排行榜
	RANK_KILL     = 8, //击杀数排行榜
	RANK_BEKILL   = 9, //被击杀数排行榜
	RANK_PRESTIGE = 10,//玩家声望(废弃)
	RANK_BEDAMAGE = 11,//玩家被伤害(废弃)
	RANK_TYPE_NUM = 12 //排行榜类型个数(准备用于数组定义)
	*/
	NotifyHonorPlayer( RANK_DAMAGE, 0 );
	NotifyHonorPlayer( RANK_FORTUNE, 0 );
	NotifyHonorPlayer( RANK_GOODEVIL, 0 );
	//工会称号不由functionsrv通知，NotifyHonorPlayer( RANK_UNION );
	NotifyHonorPlayer( RANK_KILL, 0 );
	NotifyHonorPlayer( RANK_BEKILL, 0 );
	NotifyHonorPlayer( RANK_LEGEND, 1/*天王*/ );
	NotifyHonorPlayer( RANK_LEGEND, 2/*天后*/ );

	return true;
}

//3、奖励发放；
bool CRankCacheManager::SendRankReward( ERankType rankType )
{
	if ( rankType >= ARRAY_SIZE( m_arrRankQuery ) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::SendRankReward, 错误的排行榜类型%d", rankType );
		return false;
	}

	unsigned int playerNum = 0, rewordID = 0, rewordNum = 0;//奖励人数，奖品ID号，奖品数目；
	bool isRewordInfoGot = CRankConfigManager::GetRewordInfo( rankType, playerNum, rewordID, rewordNum );
	if ( !isRewordInfoGot )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::SendRankReward, 读取排行榜%d奖励失败", rankType );
		return false;
	}

	m_arrRankQuery[rankType].SendReword( playerNum, rewordID, rewordNum );//依次进行奖励发放；

	return true;
}

//4、新称号通知；
bool CRankCacheManager::NotifyHonorPlayer( ERankType rankType, unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ )
{
	if ( rankType >= ARRAY_SIZE( m_arrRankQuery ) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::NotifyHonorPlayer, 无效的rankType:%d", rankType );
		return false;
	}
	if ( !m_arrRankQuery[rankType].IsDbInited() )
	{
		NewLog( LOG_LEV_DEBUG, "CRankCacheManager::NotifyHonorPlayer, %s尚未DB初始化", CRankDBTask::GetRankName(rankType) );
		return false;
	}
	m_arrRankQuery[rankType].NotifyRankHonorPlayer( sexFilter );//通知新的称号拥有者；
	return true;
}

//指定的排行榜缓存数据是否已根据DB初始化；
bool CRankCacheManager::CheckIfCacheDbInited( ERankType rankType )
{
	if ( rankType >= ARRAY_SIZE(m_arrRankCache) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfCacheDbInited, 无效的rankType:%d", rankType );
		return false;
	}
	return m_arrRankCache[rankType].IsDbInited();
}

//指定的排行榜查询数据是否已根据DB初始化；
bool CRankCacheManager::CheckIfQueryDbInited( ERankType rankType )
{
	if ( ( rankType >= ARRAY_SIZE( m_arrRankQuery ) )
		|| ( rankType >= ARRAY_SIZE( m_arrRankProcInfo ) )
		)
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfQueryDbInited, 无效的rankType:%d", rankType );
		return false;
	}
	return m_arrRankQuery[rankType].IsDbInited() && m_arrRankProcInfo[rankType].IsDbInited();
}

//保存所有变化过的缓存；
bool CRankCacheManager::CacheDataDBSave()
{
	CheckIfCacheNeedSave( RANK_LEVEL );
	CheckIfCacheNeedSave( RANK_DAMAGE );
	CheckIfCacheNeedSave( RANK_GOLD );
	CheckIfCacheNeedSave( RANK_SILVER );
	CheckIfCacheNeedSave( RANK_GOODEVIL );
	CheckIfCacheNeedSave( RANK_KILL );
	CheckIfCacheNeedSave( RANK_BEKILL );
	return true;
}

//rankType对应排行榜是否需要刷新
bool CRankCacheManager::CheckIfCacheNeedSave( ERankType rankType )
{
	if ( rankType >= ARRAY_SIZE(m_arrRankCache) ) 
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfCacheNeedSave, 错误的rankType:%d", rankType );
		return false;
	}

	return m_arrRankCache[rankType].CheckIfSelfNeedSave();
}

//每分钟执行一次的各排行榜刷新检查；
bool CRankCacheManager::CheckAllRankRefresh()
{
	/*
	    all must rank:
		RANK_LEVEL RANK_DAMAGE RANK_GOLD RANK_SILVER
		RANK_GOODEVIL RANK_KILL	RANK_BEKILL
	*/

	CheckIfRankNeedRefresh( RANK_LEVEL );
	CheckIfRankNeedRefresh( RANK_DAMAGE );
	CheckIfRankNeedRefresh( RANK_GOLD );
	CheckIfRankNeedRefresh( RANK_SILVER );
	CheckIfRankNeedRefresh( RANK_GOODEVIL );
	CheckIfRankNeedRefresh( RANK_KILL );
	CheckIfRankNeedRefresh( RANK_BEKILL );

	//财富榜|传说英雄榜刷新检测；
	CheckIfRankNeedRefresh( RANK_FORTUNE );
	CheckIfRankNeedRefresh( RANK_LEGEND );

	return true;
}

//rankType对应排行榜是否需要刷新
bool CRankCacheManager::CheckIfRankNeedRefresh( ERankType rankType )
{
	if ( ( rankType >= ARRAY_SIZE( m_arrRankProcInfo ) ) 
		|| ( rankType >= ARRAY_SIZE( m_arrRankCache ) ) 
		)
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, 错误的rankType:%d", rankType );
		return false;
	}

	RankRefreshType refreshType;
	bool isRefreshTypeGot = CRankConfigManager::GetRankRefreshType( rankType, refreshType );
	if ( !isRefreshTypeGot )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, 取refreshType失败，rankType:%d", rankType );
		return false;
	}

	bool isNeedRefresh = m_arrRankProcInfo[rankType].IsNeedRefresh( refreshType );
	if ( !isNeedRefresh )
	{
		return false;//无需刷新;
	}

	//以下刷新查询榜
	//其中的rankSeq主要用于区分不同周期的缓存数据，二级表无缓存数据，因此也不需要这个rankSeq号
	//    同时，非周期刷新的数据，实际上也不需要此rankSeq，例如等级榜，金钱榜等。
	if ( (RANK_FORTUNE != rankType) && (RANK_LEGEND != rankType) )
	{
		//非(财富榜|传说英雄榜)，直接收缓存生成；
		m_arrRankProcInfo[rankType].SetRefreshed();//设置其已更新；
		m_arrRankCache[rankType].RefreshRankCache( m_arrRankProcInfo[rankType].GetRankSeq() );
	} else {
		//财富榜|传说英雄榜，使用多个榜生成;
		bool isGened = GenSecondQuery( rankType );
		if ( isGened )
		{
			m_arrRankProcInfo[rankType].SetRefreshed();//设置其已更新；
		}
	}

	return true;
}

//生成新的二级榜
bool CRankCacheManager::GenSecondQuery( ERankType rankType )
{
	if ( ( RANK_FORTUNE != rankType ) && ( RANK_LEGEND != rankType ) )
	{
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::GenSecondQuery，%s不应该通过二级榜方法生成", CRankDBTask::GetRankName(rankType) );
		return false;
	}

	map<unsigned int, RankCacheRcd> tmpRst;		
	bool isOK = false;

	if ( RANK_FORTUNE == rankType ) 
	{
		//暂时忽略星钻榜
		isOK = m_arrRankQuery[RANK_GOLD].FillIncTableWithFactor( tmpRst, 1/*因子*/ );//增量加因子填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败1", RANK_GOLD );
			return false;
		}
		isOK = m_arrRankQuery[RANK_SILVER].FillIncTableWithFactor( tmpRst, 1/*因子*/ );//增量加因子填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败2", RANK_SILVER );
			return false;
		}
	} else if ( RANK_LEGEND == rankType ) {
		//暂时忽略星钻榜和工会榜
		isOK = m_arrRankQuery[RANK_LEVEL].FillLegendTable( tmpRst );//前10名填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败3", RANK_GOLD );
			return false;
		}
		isOK = m_arrRankQuery[RANK_DAMAGE].FillLegendTable( tmpRst );//前10名填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败4", RANK_SILVER );
			return false;
		}
		isOK = m_arrRankQuery[RANK_GOLD].FillLegendTable( tmpRst );//前10名填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败5", RANK_SILVER );
			return false;
		}
		isOK = m_arrRankQuery[RANK_SILVER].FillLegendTable( tmpRst );//前10名填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败6", RANK_SILVER );
			return false;
		}
		isOK = m_arrRankQuery[RANK_FORTUNE].FillLegendTable( tmpRst );//前10名填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败7", RANK_SILVER );
			return false;
		}
		isOK = m_arrRankQuery[RANK_GOODEVIL].FillLegendTable( tmpRst );//前10名填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败8", RANK_SILVER );
			return false;
		}
		isOK = m_arrRankQuery[RANK_KILL].FillLegendTable( tmpRst );//前10名填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败9", RANK_SILVER );
			return false;
		}
		isOK = m_arrRankQuery[RANK_BEKILL].FillLegendTable( tmpRst );//前10名填充
		if ( !isOK )
		{
			NewLog( LOG_LEV_ERROR, "CRankCacheManager::CheckIfRankNeedRefresh, rank:%d填充失败10", RANK_SILVER );
			return false;
		}
	} else {
		NewLog( LOG_LEV_ERROR, "CRankCacheManager::GenSecondQuery，%s，不可能到此处", CRankDBTask::GetRankName(rankType) );
		return false;
	}

	//以下挑选出前100名，并填至相应榜；
	vector<RankCacheRcd*> tmpVec;
	for ( map<unsigned int, RankCacheRcd>::iterator iter=tmpRst.begin(); iter!=tmpRst.end(); ++iter )
	{
		tmpVec.push_back( &(iter->second) );
	}
	sort( tmpVec.begin(), tmpVec.end(), CacheRcdComp );

	RankRcdSet tmpSet;
	tmpSet.InitRankCacheSetWithDataVec( RANK_FORTUNE, false, 100, tmpVec );
	CRankCacheManager::GenQueryTableFromCache( tmpSet );//生成查询表；

	return true;
}

//指令向DB更新某排行榜缓存数据；
bool CRankCacheManager::IssueUpdateRankProcInfo( ERankType rankType, const RankProcInfo* pRankProcInfo )
{
	if ( NULL == pRankProcInfo )
	{
		NewLog( LOG_LEV_ERROR, "IssueUpdateRankProcInfo, NULL == pRankProcInfo, rankType:%d", rankType );
		return false;
	}

	CRankDBTask* pTmpTask = CRankDBTask::NewRankDBTask();
	if ( NULL == pTmpTask )
	{
		NewLog( LOG_LEV_ERROR, "IssueUpdateRankProcInfo, NULL == pTmpTask, rankType:%d", rankType );
		return false;
	}

	pTmpTask->SetRankTaskInfo( CRankDBTask::UPDATE_RANKINFO, rankType, false/*目前的排行榜全是由大至小排列*/, NULL, pRankProcInfo );
	m_DBIns.AddOneAssistTask( pTmpTask );

	return true;
}




