﻿/********************************************************************
	created:	2010/03/17
	created:	17:3:2010   16:58
	file:		AuctionQueryHandle.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef AUCTION_QUERY_HANDLE_H
#define AUCTION_QUERY_HANDLE_H

#include "AuctionBase.h"

class CGateServer;

class QueryAuctionHandle
{
public: 
	//依据条件查询排行榜,返回给玩家
	static bool QueryAuctionByCondition( CGateServer* pGateSrv, const PlayerID& playerID,const GFQueryAuctionCondition& condition );

	//查询自己的竞拍情况
	static void QueryMyBid(  CGateServer* pGateSrv, const PlayerID& playerID , unsigned int playeruid, unsigned int page );

	//查询自己物品的拍卖情况
	static void QueryMyAuction( CGateServer* pGateSrv, const PlayerID& playerID , unsigned int playeruid, unsigned int page );

	//开始一个新的拍卖
	static int  StartNewAuction( const PlayerID& playerID, const NewAuctionItem& newAuctionItem );

	//玩家竞排物品
	static bool PlayerBidItem( const PlayerID& playerID, unsigned int playeruid, const char* playerName, unsigned int auctionUID , unsigned int newprice );

	//结束拍卖物品
	static bool OnAuctionEnd( unsigned int auctionUID );

	//玩家取消拍卖
	static bool OwnerCancelAuction( CGateServer* pGateSrv, unsigned int playerUID, unsigned int auctionUID );

	//玩家取消了竞拍
	//static bool PlayerCancelBid( unsigned int playerUID, unsigned int auctionUID );

	//查询玩家最后一次竞拍此物品的出价
	static bool QueryPlayerLastBidPrice( unsigned int playerUID, unsigned int auctionUID, unsigned int& price );
};


#endif