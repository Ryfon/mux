﻿#include "Service.h"
#include "DealPkg.h"
#include "../../Base/MysqlWrapper.h"
#include "../../Base/DBConfigManager.h"
#include "../../Base/LoadConfig/LoadSrvConfig.h"
#include "Task/TaskPool.h"

//#include "RankManager.h"
#include "rankcache.h"
//#include "RankChartPropManager.h"
#include "PlayerMail.h"
#include "MailIDGenerator.h"
#include "OtherServer.h"
//#include "RankUpdateInfoManager.h"
#include "RaceMaster.h"
#include "AuctionManager.h"
#include "../../Test/testthreadque_wait/sigexception/sigexception.h"

extern IBufQueue *g_pSrvSender;

void FastTimer()
{
}

void SlowTimer()
{
}

#ifdef USE_SIG_TRY
bool ExceptionCheck()
{
	if( g_isException )
	{
		FGExceptionNotify expMsg;
		expMsg.exceptionReason = 0;
		CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
		if( NULL != pGateSrv )
		{
			MsgToPut *pMsgToPut  = CreateSrvPkg(FGExceptionNotify,pGateSrv,expMsg );
			pGateSrv->SendPkgToSrv(pMsgToPut);
		}
	}
	return true;
}
#endif //USE_SIG_TRY

///时钟检测函数，循环执行以确定是否需要执行快时钟或慢时钟；
void TimerCheck()
{
	static ACE_Time_Value fastLastTime = ACE_OS::gettimeofday();
	static ACE_Time_Value slowLastTime = ACE_OS::gettimeofday();
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	ACE_Time_Value fastPastTime = curTime - fastLastTime;
	ACE_Time_Value slowPastTime = curTime - slowLastTime;
	if ( fastPastTime.msec() > 361 ) //361毫秒；
	{
		//激活快时钟；
		fastLastTime = curTime;
		FastTimer();
	} 
	if ( slowPastTime.msec() > 517 ) //517毫秒；
	{
		//激活慢时钟；
		slowLastTime = curTime;
		SlowTimer();

		PlayerMailSessionManagerSingle::instance()->MonitorPlayerSessionLiveTime();
		//种族已废弃，by dzj, 10.08.16, CRaceMasterManSingle::instance()->MonitorSelectRaceMasterBegin();
		AuctionManagerSingle::instance()->MonitorAllAuctions();
	}

#ifdef USE_SIG_TRY
	static ACE_Time_Value exceptionCheckLastTime = ACE_OS::gettimeofday();
	ACE_Time_Value exceptionPastTime = curTime - exceptionCheckLastTime;
	if( exceptionPastTime.sec() >= 10 )   //每10秒检测一次
	{
		exceptionCheckLastTime = curTime;
		ExceptionCheck();
	}
#endif //USE_SIG_TRY

	CRankCacheManager::RankMainProc();//总是执行的排行榜处理主线程；

	return;
}

CService::CService(void)
: m_pOutFileStream(NULL), m_pReadNetMsgQueue(NULL), m_pSendNetMsgQueue(NULL), 
  m_pManCommandler(NULL)
#ifndef USE_DSIOCP
  , m_pNetTask(NULL)
#endif //USE_DSIOCP
{

}

CService::~CService(void)
{

}

int CService::Open(void *pArgs)
{
	ACE_UNUSED_ARG(pArgs);

	// 返回值
	int iRet = -1;

	// 设置日志属性
	//if(this->setLog() == -1)
	//	return -1;

	// 装载配置文件
	if(this->LoadConfigFile() == -1)
		return -1;
	
	// 设置其他
	if(this->SetOther() == -1)
		return -1;

	// 创建中间队列
	m_pReadNetMsgQueue = NEW CBufQueue;
	m_pSendNetMsgQueue = NEW CBufQueue;
	if((m_pReadNetMsgQueue != NULL) && (m_pSendNetMsgQueue != NULL))
	{
		g_pSrvSender = m_pSendNetMsgQueue;

#ifdef USE_DSIOCP
		m_srvIocp = NEW CDsIocp< IBufQueue, IBufQueue, CDsSocket >;
		m_srvIocp->Init( 128, m_pReadNetMsgQueue, m_pSendNetMsgQueue );
		m_srvIocp->AddListen( CLoadSrvConfig::GetListenPort() );
#endif //USE_DSIOCP

		// 创建主逻辑处理模块
		m_pManCommandler = NEW CManCommHandler< CRcvPkgHandler<CServerConnection> >;  ///句柄管理类
		if(m_pManCommandler != NULL)
		{
			m_pManCommandler->SetNetQueue(m_pReadNetMsgQueue);//收包句柄管理，置收包来源(收包队列)
			m_pManCommandler->SetHandlePoolSize( 256 );//收包句柄管理，设置收包句柄对象池

#ifdef USE_DSIOCP
			return 0;
#else //USE_DSIOCP
#ifdef USE_NET_TASK
			// 创建任务
			m_pNetTask = NEW CNetTask<CHandlerT>(m_pReadNetMsgQueue, m_pSendNetMsgQueue);
			ACE_INET_Addr addr(CLoadSrvConfig::GetListenPort());
			if((m_pNetTask != NULL) && (m_pNetTask->open(&addr) == 0))
			{
				iRet = 0;
			}
#else
			// 创建任务
			m_pNetTask = NEW CNetTaskEx<CHandlerEx, ACE_Thread_Mutex>(m_pReadNetMsgQueue, m_pSendNetMsgQueue);
			ACE_INET_Addr addr(CLoadSrvConfig::GetListenPort());
			if((m_pNetTask != NULL) && (m_pNetTask->Open(&addr) == 0))
			{
				iRet = 0;
			}
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP
		}
	}

	if(iRet == -1)
	{
		Close();
	}

	return iRet;
}

int CService::Run(void)
{
	//主逻辑线程
	ACE_Time_Value startRunTime = ACE_OS::gettimeofday();
	ACE_Time_Value nowTime;
	ACE_Time_Value interval;

	D_INFO( "FunctionSrv:运行中...\n" );


	const unsigned long runTime = CLoadSrvConfig::GetRunTime() * 1000;
	ACE_UNUSED_ARG( runTime );
	while (!g_isGlobeStop)
	{
		nowTime = ACE_OS::gettimeofday();
		interval = nowTime - startRunTime;
		if ( ( runTime>0 )  //runtime == 0 不中断运行；
			&& ( interval.msec()>runTime )
			)
		{
#ifndef USE_DSIOCP
			m_pNetTask->Quit();
			g_pSrvSender->SetReadEvent( true );//通知网络模块发送
#endif //USE_DSIOCP
			D_DEBUG( "runTime时刻到，退出\n" );
			break;
		}

		ST_SIG_CATCH {

			TimerCheck();

			// 处理接受到所有消息
			m_pManCommandler->CommTimerProc();

			GateSrvMangerSingle::instance()->TrySendMsg();

			g_pSrvSender->SetReadEvent( false );//通知网络模块发送

			//ACE_Time_Value tmpTime;//等待一会；
			//tmpTime.sec(0);
			//tmpTime.usec( 1000 );
			//ACE_OS::sleep( tmpTime );
		} END_SIG_CATCH;
	}

#ifndef USE_DSIOCP
	// 等待网络线程
#ifdef USE_NET_TASK
	m_pNetTask->wait();
#else
	m_pNetTask->Wait();
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP

	return 0;
}

int CService::Close(void)
{
	//TaskPoolSingle::instance()->CloseThread(0);

#ifndef USE_DSIOCP
	// 删除网络模块
	if(m_pNetTask != NULL)
	{
		delete m_pNetTask;
		m_pNetTask = NULL;
	}
#endif //USE_DSIOCP

	// 删除主逻辑模块
	if(m_pManCommandler != NULL)
	{
		delete m_pManCommandler;
		m_pManCommandler = NULL;
	}

#ifdef USE_DSIOCP
	delete m_srvIocp; m_srvIocp = NULL;
#endif //USE_DSIOCP

	// 删除发送队列对象
	if(m_pSendNetMsgQueue != NULL)
	{
		delete m_pSendNetMsgQueue;
		m_pSendNetMsgQueue = NULL;
	}

	// 删除读队列对象
	if(m_pReadNetMsgQueue != NULL)
	{
		delete m_pReadNetMsgQueue;
		m_pReadNetMsgQueue = NULL;
	}

	return 0;
}

int CService::SetLog(void)
{
	// 设置日志输出到文件
	//ACE_LOG_MSG->clr_flags (ACE_Log_Msg::STDERR);
	ACE_LOG_MSG->set_flags (ACE_Log_Msg::OSTREAM);

	const char *filename = "rankserver.log";
	m_pOutFileStream = NEW ofstream(filename, ios::out | ios::trunc);
	if(NULL == m_pOutFileStream)
		return -1;

	ACE_LOG_MSG->msg_ostream(m_pOutFileStream, 1);
	return 0;
}

int CService::LoadConfigFile(void)
{
	int iRet = 0;

	// 装入服务器配置文件
	if(!CLoadSrvConfig::LoadConfig())
	{
		D_DEBUG("装入服务器配置文件出错\n");
		return -1;
	}

	D_INFO( "服务类型FunctionSrv，序号:%d\n", CLoadSrvConfig::GetSelfID() );

	// 装入数据库配置文件
	iRet = CDBConfigManagerSingle::instance()->Init("config/dbconfig.xml", "DBServer");
	if(-1 == iRet)
	{
		D_DEBUG("装入数据库配置文件出错\n");
		return -1;
	}

	//加载排行榜的配置文件
	//CRankChartPropManagerSingle::instance()->LoadXML("config/ranklist.xml");

	// 连接数据库
	CConnection * pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;
	else 
	{	
		stConnectionString *pConnStr =CDBConfigManagerSingle::instance()->Find();
		if(NULL == pConnStr)
			return -1;

		bool bCheckRank = true;
		bool bCheckMail = true;
		bool bCheckStorage = true;
		bool bCheckRankUpdateInfo = true;
		bool bCheckpublicmsg = true;
		bool bCheckracemaster = true;
		bool bCheckAuction = true;
		bool bCheckAuctionRelation = true;

		//连接数据库成功 
		if( pConnection->Open(pConnStr) != -1 )
		{
			if( !CRecordHandler::IsExistRankChartTable() )
			{
				if( !CRecordHandler::CreateRankChartTable() )
					bCheckRank = false;
			}

			if( !CRecordHandler::IsExistMailTable() )
			{
				if ( !CRecordHandler::CreateMailTable() )
					bCheckMail = false;
			}

			if ( !CRecordHandler::IsExistStorageTable()) 
			{
				if ( !CRecordHandler::CreateStorageTable() )
					bCheckStorage = false;
			}

			if ( !CRecordHandler::IsExistRankUpdateInfoTable() )
			{
				if ( !CRecordHandler::CreateRankUpdateInfoTable() )
					bCheckRankUpdateInfo = false; 
			}

			if ( !CRecordHandler::IsExistRaceMasterTable() )
			{
				if ( !CRecordHandler::CreateRaceMasterTable() )
					bCheckracemaster = false;	

				if ( bCheckracemaster )
					CRecordHandler::InsertRaceMasterInfo();
			}

			if ( !CRecordHandler::IsExistPublicMsgTable() )
			{
				if ( !CRecordHandler::CreatePublicMessageInfoTable() )
					bCheckpublicmsg = false;
			}

			if ( !CRecordHandler::IsExistAuctionTable() )
			{
				if ( !CRecordHandler::CreateAuctionTable())
					bCheckAuction = false;
			}

			if ( !CRecordHandler::IsExistAuctionRelationTable() )
			{
				if ( !CRecordHandler::CreateAuctionRelationTable() )
					bCheckAuctionRelation = false;
			}

			CRecordHandler::CheckAllRankDataTable();//检查所有的排行榜缓存数据表；

			//从数据库中获取最大的编号
			MailIDGeneratorSingle::instance()->GetMaxMailIndexIDFromDB();
		}

		if ( bCheckRank && bCheckMail && bCheckStorage && bCheckRankUpdateInfo && bCheckracemaster && bCheckAuction && bCheckAuctionRelation )
		{
			//附属连接字符串 2008/06/30
			TaskPoolSingle::instance()->AttachDBConnecting( pConnStr);
			//开启线程
			TaskPoolSingle::instance()->open();

		}
		
	}

	return 0;
}

int CService::SetOther(void)
{
	CDealGateSrvPkg::Init();
	return 0;
}
