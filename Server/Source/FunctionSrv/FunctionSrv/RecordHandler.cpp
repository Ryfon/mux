﻿#include "RecordHandler.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/Utility.h"
#include <string>
#include <sstream>

//#include "RankManager.h"
#include "rankcache.h"
#include "storage.h"
//#include "RankUpdateInfoManager.h"
#include "RaceMaster.h"
#include "PublicNoticeManager.h"

using namespace MUX_PROTO;

///检查表是否存在, 如果表字段不对，则先删除旧表，由于开发过程中rankinfo表结构有变动，而之前的表其数据实际无效，为了方便项目组其它人员，特意这样做，一段时间后删除；
bool RankIsTableExist( const char* tbName, unsigned int fieldNum )
{
    stConnectionString* pConnStr = CDBConfigManagerSingle::instance()->Find();
	if ( NULL == pConnStr )
	{
		D_ERROR( "RankCheckTable, NULL == pConnStr\n" );
		return false;
	}

	// 判断连接串
	if (    (strncmp(pConnStr->szHost,     "", strlen(pConnStr->szHost)) == 0)
		|| (strncmp(pConnStr->szUser,     "", strlen(pConnStr->szUser)) == 0)
		|| (strncmp(pConnStr->szPassword, "", strlen(pConnStr->szPassword)) == 0)
		|| (strncmp(pConnStr->szDb,       "", strlen(pConnStr->szDb)) == 0)   )
	{
		D_ERROR( "RankCheckTable, 连接信息不充分\n" );
		return false;
	}

	// 初始化mysql对象
	MYSQL* pMysql = mysql_init(NULL);
	if (NULL == pMysql)
	{
		D_ERROR( "RankCheckTable:%s时，mysql初始化失败", tbName );
		return false;
	}

	// 连接
	if ( mysql_real_connect( pMysql, pConnStr->szHost, pConnStr->szUser, pConnStr->szPassword, pConnStr->szDb
		  , pConnStr->port, NULL, CLIENT_MULTI_STATEMENTS) == NULL)
	{
		D_ERROR( "RankCheckTable:%s时, 连DB出错,错误原因:%s.\n", tbName, mysql_error(pMysql) );
		mysql_close( pMysql );
		return false;
	}

	stringstream ss;
	ss << "desc " << tbName;
	if ( mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() ) != 0 )
	{
		D_ERROR( "RankCheckTable:%s时, desc %s错,原因:%s.\n", tbName, mysql_error(pMysql), ss.str().c_str() );
		ss.str("");
		ss << "drop table " << CRankDBTask::GetResTbName( RANK_LEVEL );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table " << CRankDBTask::GetResTbName( RANK_DAMAGE );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table " << CRankDBTask::GetResTbName( RANK_GOLD );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table " << CRankDBTask::GetResTbName( RANK_SILVER );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table " << CRankDBTask::GetResTbName( RANK_GOODEVIL );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table " << CRankDBTask::GetResTbName( RANK_KILL );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table " << CRankDBTask::GetResTbName( RANK_BEKILL );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );

		mysql_close( pMysql );
		return false;
	}

	// 保存结果
	MYSQL_RES* res = mysql_store_result( pMysql );
	if ( NULL == res )
	{
		D_ERROR( "RankCheckTable:%s时, store result错,原因:%s.\n", tbName, mysql_error(pMysql) );
		mysql_close( pMysql );
		return false;
	}

	//字段总数;
	int curFieldNum = (int)mysql_num_rows( res );
	mysql_free_result(res);
	D_INFO( "RankCheckTable:%s，表字段总数%d\n", tbName, curFieldNum );

	bool isCheckOK = false;
	if ( curFieldNum != fieldNum )
	{
		D_WARNING( "RankCheckTable:%s，表字段数不正确，删去此旧表\n", tbName );
		//之前的表错误
		ss.str("");
		ss << "drop table " << tbName;
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		/*
		RANK_LEVEL                           	=	0,           	//等级排行榜
		RANK_DAMAGE                          	=	1,           	//输出伤害排行榜
		RANK_GOLD                            	=	2,           	//金币排行榜
		RANK_SILVER                          	=	3,           	//银币排行榜
		RANK_STRDIAM                         	=	4,           	//星钻排行榜
		RANK_FORTUNE                         	=	5,           	//财富排行榜
		RANK_GOODEVIL                        	=	6,           	//善恶值排行榜
		RANK_UNION                           	=	7,           	//战盟声望排行榜
		RANK_KILL                            	=	8,           	//击杀数排行榜
		RANK_BEKILL                          	=	9,           	//被击杀数排行榜
		RANK_LEGEND                          	=	10,          	//传说英雄榜
		RANK_TYPE_NUM                        	=	11,          	//排行榜类型个数(准备用于数组定义)
		*/
		ss.str("");
		ss << "drop table" << CRankDBTask::GetResTbName( RANK_LEVEL );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table" << CRankDBTask::GetResTbName( RANK_DAMAGE );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table" << CRankDBTask::GetResTbName( RANK_GOLD );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table" << CRankDBTask::GetResTbName( RANK_SILVER );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table" << CRankDBTask::GetResTbName( RANK_GOODEVIL );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table" << CRankDBTask::GetResTbName( RANK_KILL );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		ss.str("");
		ss << "drop table" << CRankDBTask::GetResTbName( RANK_BEKILL );
		mysql_real_query( pMysql, ss.str().c_str(), (unsigned long)ss.str().length() );
		isCheckOK = false;
	} else {
		isCheckOK = true;
	}

	mysql_close( pMysql );

	return isCheckOK;
}


bool CRecordHandler::IsExistTable(const char* pTableName )
{
	//FunctionTrace( "CRecordHandler::IsExistTable"/*__FUNCTION__*/ );

	if ( !pTableName )
		return false;

	// 返回值
	int iRet = 0;

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	
	char szStatement[128] = {0};
	char szStateFormat[] = {"show tables like '%s'"};
	int len = ACE_OS::snprintf( szStatement, sizeof(szStatement), szStateFormat, pTableName );
	
	bool isFind = false;

	// 执行查询
	CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, len );
	if(pResult == NULL)
	{
		D_ERROR("执行sql语句:%s错误\n", szStatement);
		iRet = -1;
	}
	else
	{
		if( pResult->RowNum() > 0 )
			isFind = true;

		// 销毁查询
		pQuery->FreeResult(pResult);
	}
	
	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return isFind;

	TRY_END;

	return false;
}

bool CRecordHandler::IsExistMailTable()
{
	return IsExistTable("mail");
}

bool CRecordHandler::IsExistPublicMsgTable()
{
	return IsExistTable("publicmessage");
}

bool CRecordHandler::IsExistRankChartTable()
{
	return IsExistTable("rankchart");
}

bool CRecordHandler::IsExistStorageTable()
{
	return IsExistTable("storage");
}

bool CRecordHandler::IsExistRankUpdateInfoTable()
{
	return IsExistTable("rankupdateinfo");
}

bool CRecordHandler::IsExistRaceMasterTable()
{
	return IsExistTable("racemaster");
}

bool  CRecordHandler::IsExistAuctionTable()
{
	return IsExistTable("auction");
}

bool CRecordHandler::IsExistAuctionRelationTable()
{
	return IsExistTable("auctionrelation");
}

//检查排行榜缓存数据表
bool CRecordHandler::CheckAllRankDataTable()
{
	CheckRankInfoTable();

	CheckRankCacheTable( RANK_LEVEL );
	CheckRankCacheTable( RANK_DAMAGE );
	CheckRankCacheTable( RANK_GOLD );
	CheckRankCacheTable( RANK_SILVER );
	CheckRankCacheTable( RANK_GOODEVIL );
	CheckRankCacheTable( RANK_KILL );
	CheckRankCacheTable( RANK_BEKILL );

	CheckRankQueryTable( RANK_LEVEL );
	CheckRankQueryTable( RANK_DAMAGE );
	CheckRankQueryTable( RANK_GOLD );
	CheckRankQueryTable( RANK_SILVER );
	CheckRankQueryTable( RANK_FORTUNE );
	CheckRankQueryTable( RANK_GOODEVIL );
	CheckRankQueryTable( RANK_KILL );
	CheckRankQueryTable( RANK_BEKILL );
	CheckRankQueryTable( RANK_LEGEND );
	return true;
}

//检查并创建排行榜信息查询表;
bool CRecordHandler::CheckRankInfoTable()
{
	const char* tbName = CRankDBTask::GetRankInfoTbName();
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRecordHandler::CheckRankInfoTable，取不到rankinfo表名" );
		return false;
	}
	if ( !RankIsTableExist(tbName, 3) )
	{
		NewLog( LOG_LEV_DEBUG, "排行榜信息表%s不存在，创建之", tbName );
		CreateRankInfoTable( tbName );
	}
	return true;
}

//创建排行榜信息表;
bool CRecordHandler::CreateRankInfoTable( const char* tbName )
{
	/*表字段顺序: ranktype, rankseq, updatetime*/
	stringstream query;
	query << "create table " << tbName << " ( ranktype int UNIQUE, rankseq int(11) NOT NULL, updatetime int NOT NULL, PRIMARY KEY(ranktype) );";

	CConnection* pDBConn = CConnectionPoolSingle::instance()->GetConnection();
	CQueryWrapper queryWrapper( *pDBConn );
	queryWrapper.ExecQuery( query.str() );
	NewLog( LOG_LEV_DEBUG, "创建排行榜信息表%s", tbName );

	//插入初始数据；
	unsigned int curTime = (unsigned int)time(NULL);

	for ( unsigned int rankType=0; rankType<RANK_TYPE_NUM; ++rankType )
	{
		query.str("");
		query << "insert into " << tbName << " ( ranktype, rankseq, updatetime ) values ( " 
			<< rankType << "," << 0 << "," << curTime << ")";
		CQueryWrapper queryWrapper( *pDBConn );
		queryWrapper.ExecQuery( query.str() );
	}

	return true;
}

//检查并创建某个排行榜数据缓存表;
bool CRecordHandler::CheckRankCacheTable( ERankType rankType )
{
	const char* tbName = CRankDBTask::GetResTbName( rankType );
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRecordHandler::CheckRankCacheTable，错误的rankType:%d", rankType );
		return false;
	}
	if ( !IsExistTable(tbName) )
	{
		NewLog( LOG_LEV_DEBUG, "排行榜%s的缓存表不存在，创建之", tbName );
		CreateRankResTable( tbName );
	}
	return true;
}

//检查并创建某个排行榜待查表;
bool CRecordHandler::CheckRankQueryTable( ERankType rankType )
{
	const char* tbName = CRankDBTask::GetQueryTbName( rankType );
	if ( NULL == tbName )
	{
		NewLog( LOG_LEV_ERROR, "CRecordHandler::CheckRankQueryTable，错误的rankType:%d", rankType );
		return false;
	}
	if ( !IsExistTable(tbName) )
	{
		NewLog( LOG_LEV_DEBUG, "排行榜%s的查询表不存在，创建之", tbName );
		CreateRankResTable( tbName );
	}
	return true;
}//bool CRecordHandler::CheckRankQueryTable( ERankType rankType )

//创建某个排行榜数据缓存表;
bool CRecordHandler::CreateRankResTable( const char* tbName )
{
	/*表字段顺序: inPUID, inPClass, inPLevel, inRankInfo, inPName, inMSeq, inRankSeqID*/
	stringstream query;
	query << "create table " << tbName << " ( rcduid int(11) AUTO_INCREMENT, puid int(11) NOT NULL, pclass int NOT NULL,\
										  plevel int NOT NULL, rankinfo int(11) NOT NULL, pname varchar(20) NOT NULL, \
										  mseq int NOT NULL, rankseqid int NOT NULL, extrainfo int(11) NOT NULL, PRIMARY KEY(rcduid), KEY(rankinfo) );";

	CConnection* pDBConn = CConnectionPoolSingle::instance()->GetConnection();
	CQueryWrapper queryWrapper( *pDBConn );
	queryWrapper.ExecQuery( query.str() );
	NewLog( LOG_LEV_DEBUG, "创建排行榜缓存数据表%s", tbName );

	return true;
}

bool CRecordHandler::CreateAuctionRelationTable()
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<" CREATE TABLE auctionrelation ("
	  <<" playeruid int(11) NOT NULL, auctionid int(11) NOT NULL, auctionprice int(11) DEFAULT NULL,bidPlayerName varchar(20)  DEFAULT NULL,"
	  <<" PRIMARY KEY (playeruid, auctionid )) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet == -1  )
	{
		D_ERROR("执行sql语句:%s错误\n", statement.c_str());
		return false;
	}
	else
	{
		D_DEBUG("创建拍卖关系表成功\n");
	}


	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;
	TRY_END;

	return false;
}

bool CRecordHandler::CreateAuctionTable()
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<" CREATE TABLE auction ("<<" id int(11) NOT NULL AUTO_INCREMENT," <<"playerUID int(11) NOT NULL, itemname varchar(30) DEFAULT NULL,"
	  <<" level int(11) DEFAULT NULL,"<<" masslevel int(11) DEFAULT NULL,"
	  <<" equippos int(11) DEFAULT NULL,"<<" itemclass varchar(20) DEFAULT NULL,"
	  <<" auctionplayer varchar(20) DEFAULT NULL,"<<" expiretime int(11) DEFAULT NULL,"
	  <<" fixprice int(11) DEFAULT NULL," <<" curauctionprice int(11) DEFAULT NULL,"
	  <<" originalprice int(11) DEFAULT NULL,"<< " itemtypeid int(11) DEFAULT NULL," 
	  <<" iteminfo blob, PRIMARY KEY (id) )ENGINE=InnoDB DEFAULT CHARSET=utf8;";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet == -1  )
	{
		D_ERROR("执行sql语句:%s错误\n", statement.c_str());
		return false;
	}
	else
	{
		D_DEBUG("创建拍卖表成功\n");
	}

	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;
	TRY_END;

	return false;
}

bool CRecordHandler::CreateRankChartTable()
{
	//FunctionTrace( "CRecordHandler::CreateRankChartTable"/*__FUNCTION__*/ );

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<"CREATE TABLE rankchart"<< "( rankid INTEGER(11) NOT NULL,"<< " playeruid INTEGER(11) NOT NULL,"
	  <<" playername VARCHAR(20) COLLATE latin1_swedish_ci DEFAULT NULL,"<<" playerclass INTEGER(11) DEFAULT NULL,"<<" playerlevel INTEGER(11) DEFAULT NULL,"
	  <<" playerrace INTEGER(11) DEFAULT NULL, "
	  <<" rankinfo INTEGER(11) DEFAULT NULL )ENGINE=MyISAM;";
	
	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet == -1  )
	{
		D_ERROR("执行sql语句:%s错误\n", statement.c_str());
		return false;
	}
	else
	{
		D_DEBUG("创建排行榜表成功\n");
	}

	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;
	TRY_END;

	return false;
}

bool CRecordHandler::CreateStorageTable()
{
	//FunctionTrace( "CRecordHandler::CreateStorageTable"/*__FUNCTION__*/ );

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<" CREATE TABLE storage " << "(id int(11) NOT NULL,iteminfo blob,password varchar(20) DEFAULT NULL,securityMode int(11) DEFAULT 0,"
	  <<" validRow int(11) DEFAULT 2,lastForbiddenTime int(11) DEFAULT 0, inputErrCount int(11) DEFAULT 0,"
	  <<" PRIMARY KEY (id),UNIQUE KEY id (id)) " 
	  <<" ENGINE=MyISAM DEFAULT CHARSET=latin1;";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet == -1  )
	{
		D_ERROR("执行sql语句:%s错误\n", statement.c_str());
		return false;
	}
	else
	{
		D_DEBUG("创建仓库表成功\n");
	}

	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;
	TRY_END;

	return false;
}

bool CRecordHandler::CreateRaceMasterTable()
{

	//FunctionTrace( "CRecordHandler::CreateRaceMasterTable"/*__FUNCTION__*/ );

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<"CREATE TABLE racemaster "
	  << "( racetype int(11) NOT NULL,"
	  << "lastupdatetime int(11) unsigned NOT NULL DEFAULT 0 ,"
	  <<" todaymodeifycount int(11) NOT NULL DEFAULT 0,"
	  <<" isgotsalary tinyint(1) NOT NULL DEFAULT 0, "
	  <<" declarewarinfo int(11) NOT NULL DEFAULT 0, "
	  <<" masteruid int(11) NOT NULL DEFAULT 0, "
	  <<" PRIMARY KEY ( racetype ) )"
	  <<" ENGINE=MyISAM DEFAULT CHARSET=latin1;";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet == -1  )
	{
		D_ERROR("执行sql语句:%s错误\n", statement.c_str());
		return false;
	}
	else
	{
		D_DEBUG("创建仓库表成功\n");
	}

	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;
	TRY_END;

	return false;
}

bool CRecordHandler::CreateRankUpdateInfoTable()
{
	//FunctionTrace( "CRecordHandler::CreateRankUpdateInfoTable"/*__FUNCTION__*/ );

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<" CREATE TABLE rankupdateinfo( " << " rankid int(11) NOT NULL, lastupdatetime int(11) DEFAULT 0, nextupdatetime int(11) DEFAULT 0, "
	  <<" lastrewordtime int(11) DEFAULT 0, nextrewordtime int(11) DEFAULT 0 , PRIMARY KEY (rankid), UNIQUE KEY rankid (rankid) "
	  <<" ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet == -1  )
	{
		D_ERROR("执行sql语句:%s错误\n", statement.c_str());
		return false;
	}
	else
	{
		D_DEBUG("创建排行榜更新信息榜成功\n");
	}

	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;
	TRY_END;

	return false;
}


bool CRecordHandler::CreatePublicMessageInfoTable()
{
	//FunctionTrace( "CRecordHandler::CreatePublicMessageInfoTable");
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<" CREATE TABLE publicmessage( " << "msgid int(11) NOT NULL, content blob, istop int(11) DEFAULT NULL,"
	  <<" starttime varchar(20) DEFAULT NULL, endtime varchar(20) DEFAULT NULL, broadcastnum int(11) DEFAULT NULL, distancetime int(11) DEFAULT NULL,"
	  <<" PRIMARY KEY (msgid))ENGINE=MyISAM DEFAULT CHARSET=latin1;";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet == -1  )
	{
		D_ERROR("执行sql语句:%s错误\n", statement.c_str());
		return false;
	}
	else
	{
		D_DEBUG("创建公共公告信息表成功\n");
	}

	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;

	TRY_END;

	return false;
}


bool CRecordHandler::CreateMailTable()
{
	//FunctionTrace( "CRecordHandler::CreateMailTable"/*__FUNCTION__*/ );

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL

	std::stringstream ss;
	ss  <<"CREATE TABLE mail"<< "( mailuid INTEGER(11) NOT NULL,"<< " type INTEGER(5) NOT NULL,"<<" bopen  tinyint(1) NOT NULL,"<<" sendplayeruid int(11) NOT NULL,"
		<<" recvplayeruid int(11) NOT NULL,"<<" sendplayername varchar(20) NOT NULL,"<<" expiretime int(11) NULL," << " title varchar(50) NULL,"
		<<" content varchar(400) NULL,"<<" attachinfo blob )ENGINE=MyISAM;";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet == -1  )
	{
		D_ERROR("执行sql语句:%s错误\n", statement.c_str());
		return false;
	}
	else
	{
		D_DEBUG("创建邮件表成功\n");
	}

	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;
	TRY_END;

	return false;
}


unsigned int CRecordHandler::GetMailMaxID()
{
	unsigned int  maxUID = 1;

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return maxUID;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return maxUID;

	// 构造SQL
	std::stringstream ss;

	ss  <<"select COUNT(*) ,MAX(mailuid) from mail";
	std::string statement = ss.str();
	CResult * pResult = pQuery->ExecuteSelect( statement.c_str(), (unsigned long)(statement.size()) );
	if ( pResult )
	{
		if ( pResult->RowNum() > 0  && pResult->Next()  )
		{
			unsigned int count = pResult->GetInt(0);
			if ( count > 0 )
			{
				maxUID = pResult->GetInt(1);
			}
		}
		pQuery->FreeResult( pResult );
	}
	//// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return maxUID + 1 ;
	TRY_END;

	return maxUID;
}

bool CRecordHandler::CheckQueryIDStorageIsExist(unsigned int queryID )
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	bool isFind = false;

	// 构造SQL
	std::stringstream ss;
	ss<<"select 1 from storage where id = " << queryID;
	std::string statement = ss.str();
	CResult * pResult = pQuery->ExecuteSelect( statement.c_str(), (unsigned long)(statement.size()) );
	if ( pResult )
	{
		if ( pResult->RowNum() > 0 && pResult->Next() )
		{
			isFind = true;
		}
		pQuery->FreeResult( pResult );
	}

	pConnection->DestroyQuery( pQuery );
	return isFind;
	TRY_END;

	return  false;
}

bool CRecordHandler::GetRaceMasterInfoByRaceID(unsigned int racetype,RaceMasterInfo* pRaceMasterInfo )
{
	if ( !pRaceMasterInfo )
		return false;

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<" select masteruid, lastupdatetime,todaymodeifycount,isgotsalary,declarewarinfo from racemaster where racetype = " << racetype;
	std::string statement = ss.str();
	CResult * pResult = pQuery->ExecuteSelect( statement.c_str(), (unsigned long)(statement.size()) );
	if ( !pResult )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement );
		return false;
	}
	else
	{
		if ( pResult->RowNum() > 0  && pResult->Next()  )
		{
			pRaceMasterInfo->racetype  = racetype;
			pRaceMasterInfo->masterUID = pResult->GetInt(0);
			pRaceMasterInfo->lastupdatetime = pResult->GetInt(1);
			pRaceMasterInfo->todaymodifycount = pResult->GetInt(2);
			pRaceMasterInfo->isgotsalary  = ( pResult->GetInt(3) == 1 );
			pRaceMasterInfo->declarewarinfo = pResult->GetInt(4);
		}
		pQuery->FreeResult( pResult );
	}
	pConnection->DestroyQuery( pQuery );
	return true;
	TRY_END;

	return false;
}

bool CRecordHandler::GetStorageInfoByQueryID(unsigned int queryID , StorageInfo* pStorageInfo )
{
	if ( !pStorageInfo )
		return false;

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss<<" select * from storage where id = " << queryID;
	std::string statement = ss.str();
	CResult * pResult = pQuery->ExecuteSelect( statement.c_str(), (unsigned long)(statement.size()) );
	if ( !pResult )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement );
		return false;
	}
	else
	{
		if( pResult->Next() )
		{
			//唯一编号
			unsigned int storageID = pResult->GetInt( 0 );
			pStorageInfo->SetStorageUID( storageID );

			//仓库的道具信息
			char* pItemInfoArr = pResult->GetString( 1 );
			if ( pItemInfoArr )
				pStorageInfo->SetStorageItem( (ItemInfo_i *)pItemInfoArr );

			//仓库的密码
			const char* pStoragePsd  = pResult->GetString( 2 );
			if ( pStoragePsd )
				pStorageInfo->SetStoragePsd( pStoragePsd );

			//仓库的安全模式
			unsigned int securityMode = pResult->GetInt( 3 );
			pStorageInfo->SetStorageSecurityMode( (STORAGE_SECURITY_STRATEGY)securityMode );

			//仓库的可用行数
			unsigned int validRow  = pResult->GetInt( 4 );
			pStorageInfo->SetStorageValidRow( validRow );

			//被禁止使用仓库的时间
			unsigned int forbiddentime = pResult->GetInt( 5 );
			pStorageInfo->SetLastForbiddenTime( forbiddentime );

			//输入错的次数
			unsigned int inputErrCount = pResult->GetInt( 6 );
			pStorageInfo->SetInputErrCount( inputErrCount );

			pQuery->FreeResult( pResult ); 
		}
	}
	pConnection->DestroyQuery(  pQuery );

	TRY_END;

	return true;
}


bool  CRecordHandler::InsertNewStorage(unsigned int storageUID )
{

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss << "INSERT INTO storage ( id ) VALUES( " << storageUID << " )";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet ==  -1  )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement );
		return false;
	}

	pConnection->DestroyQuery( pQuery );
	return true;

	TRY_END;

	return false;
}

bool CRecordHandler::InsertRaceMasterInfo()
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 构造SQL
	std::stringstream ss;
	ss << " INSERT INTO racemaster( racetype ) VALUES(1);"
	   << " INSERT INTO racemaster( racetype ) VALUES(2);"
	   << " INSERT INTO racemaster( racetype ) VALUES(3);";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
	if ( iRet ==  -1  )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement );
		return false;
	}

	pConnection->DestroyQuery( pQuery );
	return true;

	TRY_END;

	return false;
}

bool CRecordHandler::UpdateRaceMasterInfo( RaceMasterInfo& raceMasterRef )
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	int ch =  raceMasterRef.isgotsalary ? 1:0;
	// 构造SQL
	char sql[512] = {0};
	int len = ACE_OS::snprintf( sql,
		sizeof(sql),
		"update racemaster set lastupdatetime=%d,todaymodeifycount=%d,isgotsalary=%d,declarewarinfo=%d,masteruid=%d where racetype=%d",
		raceMasterRef.lastupdatetime,
		raceMasterRef.todaymodifycount,
		ch,
		raceMasterRef.declarewarinfo,
		raceMasterRef.masterUID,
		 raceMasterRef.racetype
		);

	int iRet = pQuery->ExecuteUpdate( sql, (unsigned long)len  );
	if ( iRet ==  -1  )
	{
		D_ERROR("执行SQL语句:%s 错误\n",sql );
		return false;
	}

	pConnection->DestroyQuery( pQuery );
	return true;

	TRY_END;
	return false;
}


bool CRecordHandler::InsertNewPublicNotice(PublicNoticeEx& publicNoticeRef ,const char* pstarttime, const char* pendtime )
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	//发布的内容要经过转义
	char escapemsgcontent[2*sizeof(publicNoticeRef.szContent)+1] = {0};
	pQuery->RealEscape( escapemsgcontent, publicNoticeRef.szContent, (unsigned long)sizeof(publicNoticeRef.szContent) );

	std::stringstream ss;
	ss<<"insert into publicmessage (msgid,content,istop,starttime,endtime,broadcastnum,distancetime) values( "
	  << publicNoticeRef.msgid <<",'"<< escapemsgcontent<<"',"<<(unsigned int)publicNoticeRef.top<<",'"<<pstarttime<<"','"<<pendtime<<"',"
	  << publicNoticeRef.broadcastnum <<","<<publicNoticeRef.distancetime<<");";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(),(unsigned long)(statement.size())  );
	if ( iRet ==  -1  )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement.c_str() );
		return false;
	}

	pConnection->DestroyQuery( pQuery );
	return true;

	TRY_END;
	return false;
}

bool CRecordHandler::DeleteRemoveAllPublicNotice()
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	std::stringstream ss;
	ss<<"delete from publicmessage";


	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(),(unsigned long)(statement.size())  );
	if ( iRet ==  -1  )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement.c_str() );
		return false;
	}

	pConnection->DestroyQuery( pQuery );
	return true;

	TRY_END;
	return false;
}

bool CRecordHandler::DeletePublicNoticeInfo(unsigned int msgid )
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	std::stringstream ss;
	ss<<"delete from publicmessage where msgid=" << msgid;

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(),(unsigned long)(statement.size())  );
	if ( iRet ==  -1  )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement.c_str() );
		return false;
	}

	pConnection->DestroyQuery( pQuery );
	return true;

	TRY_END;
	return false;
}

bool CRecordHandler::UpdatePublicNoticeInfo(PublicNoticeEx& publicNoticeRef ,const char* pstarttime, const char* pendtime)
{
	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	//发布的内容要经过转义
	char escapemsgcontent[2*sizeof(publicNoticeRef.szContent)+1] = {0};
	pQuery->RealEscape( escapemsgcontent, publicNoticeRef.szContent, (unsigned long)sizeof(publicNoticeRef.szContent) );

	std::stringstream ss;
	ss <<"update publicmessage set content='"<< escapemsgcontent<< "',istop="
	   <<(unsigned int)publicNoticeRef.top <<",starttime='"<<pstarttime<<"',endtime='"<<pendtime<<"',broadcastnum="
	   <<publicNoticeRef.broadcastnum<<",distancetime="<<publicNoticeRef.distancetime<<" where msgid="<< publicNoticeRef.msgid<<";";

	std::string statement = ss.str();
	int iRet = pQuery->ExecuteUpdate( statement.c_str(),(unsigned long)(statement.size())  );
	if ( iRet ==  -1  )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement.c_str() );
		return false;
	}

	pConnection->DestroyQuery( pQuery );
	return true;

	TRY_END;
	return false;
}

//bool CRecordHandler::InsertNewRankUpdateInfo( RankUpdateInfo* pRankUpdateInfo )
//{
//	TRY_BEGIN;
//
//	if ( pRankUpdateInfo )
//	{
//		// 创建查询
//		CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
//		if(pConnection == NULL)
//			return false;
//
//		CQuery *pQuery = pConnection->CreateQuery();
//		if(pQuery == NULL)
//			return false;
//
//		// 构造SQL
//		std::stringstream ss;
//		ss << "insert into rankupdateinfo ( rankid, lastupdatetime, nextupdatetime, lastrewordtime, nextrewordtime ) values( "
//		   << pRankUpdateInfo->rankchartID << "," << pRankUpdateInfo->lastupdatetime << "," << pRankUpdateInfo->nextupdatetime 
//		   <<","<<pRankUpdateInfo->lastrewordtime <<"," << pRankUpdateInfo->nextrewordtime <<" )";
//
//		std::string statement = ss.str();
//		int iRet = pQuery->ExecuteUpdate( statement.c_str(), (unsigned long)(statement.size()) );
//		if ( iRet ==  -1  )
//		{
//			D_ERROR("执行SQL语句:%s 错误\n",statement );
//			return false;
//		}
//
//		pConnection->DestroyQuery( pQuery );
//		return true;
//	}
//
//	TRY_END;
//	return false;
//}