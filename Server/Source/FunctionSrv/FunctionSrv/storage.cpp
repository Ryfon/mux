﻿#include "storage.h"
#include "RecordHandler.h"
#include "GateSrvManager.h"
#include "../../Base/Utility.h"


#include "Task/TaskPool.h"
#include "Task/UpdateStorageItemInfo.h"
#include "Task/UpdateStorageSafeInfoTask.h"
#include "Task/UpdateStorageForbiddenInfoTask.h"

void CStorageManager::MonitorStorageLive()
{
	time_t curtime = time(NULL);

	std::map<unsigned int , StorageInfo *>::iterator lter = mStorageInfoMap.begin();
	for ( ; lter != mStorageInfoMap.end(); )
	{
		StorageInfo* pStorageInfo = lter->second;
		if ( pStorageInfo )
		{
			if ( ( curtime - pStorageInfo->GetBornTime() ) >= STORAGE_LIVE_TIME )
			{
				Dellocate( pStorageInfo );

				mStorageInfoMap.erase( lter++ );
			}
			else
			{
				lter++;
			}
		}
		else
		{
			mStorageInfoMap.erase( lter++ );
		}

	}
}

StorageInfo* CStorageManager::GetStorageInfo(unsigned int storageUID  )
{
	std::map<unsigned int , StorageInfo *>::iterator findIter = mStorageInfoMap.find( storageUID );
	if ( findIter != mStorageInfoMap.end() )
		return findIter->second;

	return NULL;
}

void CStorageManager::NoticeStorageInfoToPlayer(CGateServer* pGateSrv , const PlayerID& queryPlayer, StorageInfo* pStorageInfo )
{
	if ( !pGateSrv )
	{
		D_ERROR("通知仓库信息,但找不到对应的GateSrv ");
		return;
	}

	if ( !pStorageInfo )
		return;


	//通知安全信息
	FGNoticeStorageSafeInfo storageSafeInfo;
	storageSafeInfo.playerID = queryPlayer;
	storageSafeInfo.safeInfo.playerID = queryPlayer;
	storageSafeInfo.safeInfo.validRow = pStorageInfo->GetValidRow();
	storageSafeInfo.safeInfo.securityMode = pStorageInfo->GetSecurityStrategy();
	ACE_OS::strncpy( storageSafeInfo.safeInfo.szStoragePsd , pStorageInfo->GetStoragePsd(), sizeof(storageSafeInfo.safeInfo.szStoragePsd) );
	MsgToPut *pMsgToPut1 = CreateSrvPkg( FGNoticeStorageSafeInfo,pGateSrv,storageSafeInfo );
	pGateSrv->SendPkgToSrv( pMsgToPut1 );

	//通知道具信息
	int validRow = pStorageInfo->GetValidRow();
	if ( validRow )
	{
		if ( validRow > MAX_STORAGE_ROWCOUNT )
			validRow = MAX_STORAGE_ROWCOUNT;

		//计算需要几页
		unsigned int PageCount = ( validRow - 1 ) / 4 + 1;

		for( unsigned int i = 0 ; i < PageCount; i++ )
		{
			ItemInfo_i* pCopyPos = pStorageInfo->GetStorageItem() + i * STORAGE_PAGE_SIZE;

			FGNoticeStorageItemInfo storageItemInfo;
			StructMemSet( storageItemInfo, 0x0, sizeof(storageItemInfo) );
			storageItemInfo.playerID = queryPlayer;
			storageItemInfo.itemInfo.playerID   = queryPlayer;
			storageItemInfo.itemInfo.labelIndex = i;
			StructMemCpy( storageItemInfo.itemInfo.storageItemArr , pCopyPos, sizeof(storageItemInfo.itemInfo.storageItemArr) );
			MsgToPut *pMsgToPut2 = CreateSrvPkg( FGNoticeStorageItemInfo,pGateSrv,storageItemInfo );
			pGateSrv->SendPkgToSrv( pMsgToPut2 );
		}
	}

	//通知仓库的禁用信息
	FGNoticeStorageForbiddenInfo forbiddenInfo;
	forbiddenInfo.playerID = queryPlayer;
	forbiddenInfo.forbiddenInfo.playerID = queryPlayer;
	forbiddenInfo.forbiddenInfo.forbiddenTime = pStorageInfo->GetLastForbiddentTime();
	forbiddenInfo.forbiddenInfo.inputErrCount = pStorageInfo->GetInputPsdErrCount();
	MsgToPut *pMsgToPut3 = CreateSrvPkg( FGNoticeStorageForbiddenInfo,pGateSrv,forbiddenInfo );
	pGateSrv->SendPkgToSrv( pMsgToPut3 );
}


void CStorageManager::NoticePlayerShowSetStoragePsdDlg( CGateServer* pGateSrv , const PlayerID& queryPlayer )
{
	if ( !pGateSrv )
		return;

	FGNoticeFirstUseStorage firstuseStorage;
	firstuseStorage.playerID = queryPlayer;
	firstuseStorage.showPsdDlg.dlgType = E_SET_PASSWORD;
	MsgToPut *pMsgToPut = CreateSrvPkg( FGNoticeFirstUseStorage,pGateSrv,firstuseStorage );
	pGateSrv->SendPkgToSrv( pMsgToPut );
}

void CStorageManager::QueryStorageInfo( CGateServer* pGateSrv , const PlayerID& queryPlayer, unsigned int queryUID )
{
	if ( !pGateSrv )
	{
		D_ERROR("查询仓库信息时,传来的GateSrv指针为空\n");
		return ;
	}

	StorageInfo* pStorageInfo = GetStorageInfo( queryUID );
	if ( pStorageInfo )//如果在内存中
	{
		NoticeStorageInfoToPlayer( pGateSrv,queryPlayer,pStorageInfo  );
	}
	else
	{
		//检查对应仓库的ID是否存在,如果存在 
		if( CRecordHandler::CheckQueryIDStorageIsExist( queryUID ) )
		{
			//如果仓库表中有对应的仓库
			StorageInfo* pStorageInfo = Allocate();
			if ( pStorageInfo )
			{
				if( CRecordHandler::GetStorageInfoByQueryID( queryUID , pStorageInfo ) )
				{
					mStorageInfoMap.insert( std::pair<unsigned int , StorageInfo *>( queryUID ,pStorageInfo ) );

					//将仓库信息通知client
					NoticeStorageInfoToPlayer( pGateSrv, queryPlayer , pStorageInfo );
				}
				else
				{
					Dellocate( pStorageInfo );
				}
			}
		}
		else//如果是新创建的仓库,第一次使用仓库
		{
			StorageInfo* pNewStorageInfo = Allocate();
			if ( pNewStorageInfo )
			{
				if( CRecordHandler::InsertNewStorage( queryUID ) )//如果创建仓库成功
				{
					//设置新的仓库编号
					pNewStorageInfo->SetStorageUID( queryUID );

					mStorageInfoMap.insert( std::pair<unsigned int , StorageInfo *>( queryUID ,pNewStorageInfo ) );

					NoticeStorageInfoToPlayer( pGateSrv, queryPlayer , pNewStorageInfo );

					//因为第一次使用仓库,提示设置仓库密码框的功能
					NoticePlayerShowSetStoragePsdDlg( pGateSrv, queryPlayer );
				}
			}
		}
	}
}


void CStorageManager::UpdateStorageItemInfo(unsigned int storageID, unsigned int storageIndex, const MUX_PROTO::ItemInfo_i &updateItem)
{
	StorageInfo* pStorageInfo = GetStorageInfo( storageID );
	if ( pStorageInfo )
	{
		pStorageInfo->UpdateItemInfo( storageIndex, updateItem );
	}
	else//如果已经不在内存，则创建出来，放入内存管理
	{
		pStorageInfo = Allocate();
		if ( pStorageInfo )
		{
			if( CRecordHandler::GetStorageInfoByQueryID( storageID , pStorageInfo ) )
			{
				//放入内存
				mStorageInfoMap.insert( std::pair<unsigned int , StorageInfo *>( storageID ,pStorageInfo ) );

				pStorageInfo->UpdateItemInfo( storageIndex, updateItem );//更新道具信息
			}
			else
			{
				Dellocate( pStorageInfo );
			}
		}
		
	}

	if( pStorageInfo )
	{
		UpdateStorageItemInfoTask* pTask = NEW UpdateStorageItemInfoTask( pStorageInfo->GetStorageItem(),storageID );
		TaskPoolSingle::instance()->PushTask( pTask );
	}
}

void CStorageManager::UpdateStorageSafeInfo(unsigned int storageID, unsigned int validRow, STORAGE_SECURITY_STRATEGY securityMode , const char* psd )
{
	StorageInfo* pStorageInfo = GetStorageInfo( storageID );
	if ( pStorageInfo )
	{
		pStorageInfo->UpdateSafeInfo( validRow, securityMode, psd );
	}
	else//如果已经不在内存，则创建出来，放入内存管理
	{
		pStorageInfo = Allocate();
		if ( pStorageInfo )
		{
			if( CRecordHandler::GetStorageInfoByQueryID( storageID , pStorageInfo ) )
			{
				//放入内存
				mStorageInfoMap.insert( std::pair<unsigned int , StorageInfo *>( storageID ,pStorageInfo ) );

				pStorageInfo->UpdateSafeInfo( validRow, securityMode, psd );//更新安全信息
			}
			else
			{
				Dellocate( pStorageInfo );
			}
		}

	}

	if( pStorageInfo )
	{
		UpdateStorageSafeInfoTask* pTask = NEW UpdateStorageSafeInfoTask( storageID, pStorageInfo->GetValidRow(), pStorageInfo->GetSecurityStrategy(), pStorageInfo->GetStoragePsd() );
		TaskPoolSingle::instance()->PushTask( pTask );
	}
}

void CStorageManager::UpdateStorageInputPsdErrInfo(unsigned int storageID, unsigned int forbiddenTime, unsigned int errCount)
{
	StorageInfo* pStorageInfo = GetStorageInfo( storageID );
	if ( pStorageInfo )
	{
		pStorageInfo->UpdateForbiddenInfo( forbiddenTime ,errCount );
	}
	else//如果已经不在内存，则创建出来，放入内存管理
	{
		pStorageInfo = Allocate();
		if ( pStorageInfo )
		{
			if( CRecordHandler::GetStorageInfoByQueryID( storageID , pStorageInfo ) )
			{
				//放入内存
				mStorageInfoMap.insert( std::pair<unsigned int , StorageInfo *>( storageID ,pStorageInfo ) );

				pStorageInfo->UpdateForbiddenInfo( forbiddenTime ,errCount );//更新玩家输入错密码的信息
			}
			else
			{
				Dellocate( pStorageInfo );
			}
		}

	}

	if( pStorageInfo )
	{
		UpdateStorageForbiddenInfoTask* pTask = NEW UpdateStorageForbiddenInfoTask( storageID, pStorageInfo->GetInputPsdErrCount(), pStorageInfo->GetLastForbiddentTime() );
		TaskPoolSingle::instance()->PushTask( pTask );
	}
}