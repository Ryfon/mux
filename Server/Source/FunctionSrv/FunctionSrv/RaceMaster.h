﻿/********************************************************************
	created:	2009/11/17
	created:	17:11:2009   19:05
	file:		RaceMaster.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef C_RACE_MASTER_H
#define C_RACE_MASTER_H

#include <vector>
#include "aceall.h"
class CGateServer;

struct RaceMasterInfo
{
	unsigned short racetype;
	unsigned int masterUID;
	unsigned int todaymodifycount;
	unsigned int lastupdatetime;
	char     declarewarinfo;
	bool     isgotsalary;

	RaceMasterInfo():racetype(0),masterUID(0),todaymodifycount(0),lastupdatetime(0),declarewarinfo(0),isgotsalary(false)
	{}
};

class CRaceMasterMan
{
	friend class ACE_Singleton<CRaceMasterMan, ACE_Null_Mutex>;
public:
	CRaceMasterMan():m_selectdayofweek(0),m_selecthour(0),isbSelectRaceMaster(false),isforbiddenracemaster(false),m_noticecount(0)
	{
		lastupdatetime = time(NULL);
	}

public:
	//加载族长信息
	void LoadRaceMasterInfo();

	//更新对应种族的族长信息
	void UpdateRaceMasterInfo( unsigned short race, const RaceMasterInfo& newracemasterinfo );

	//当接到mapsrv请求族长信息的请求时
	void OnMapSrvQueryRaceMasterInfo(CGateServer* pGateSrv );

	//监视选族长
	void MonitorSelectRaceMasterBegin();

	//当新族长产生时
	void OnNewRaceMasterBorn( unsigned short race, unsigned int masteruid , const char* playername, const char* unionname );

	//当宣战信息改变时
	void OnDeclareWarReset( unsigned int race );

	void StartSelectRaceMater();//开始族长选举

	void StartForbiddenRaceMaster();//开始禁止族长权限

private:
	std::vector<RaceMasterInfo> m_raceMasterInfoVec;
	int  m_selectdayofweek;
	int  m_selecthour;
	bool isbSelectRaceMaster;
	bool isforbiddenracemaster;
	int  m_noticecount;
	time_t lastupdatetime;
};

typedef ACE_Singleton<CRaceMasterMan, ACE_Null_Mutex> CRaceMasterManSingle;

#endif