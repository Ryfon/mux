﻿#include "PlayerMail.h"
#include "MailIDGenerator.h"
#include "SuspectMailManager.h"
#include "GateSrvManager.h"

#include "Task/TaskPool.h"
#include "Task/InsertMailTask.h"
#include "Task/UpdateMailListTask.h"
#include "Task/QueryMailListTask.h"
#include "Task/DeleteMailTask.h"
#include "Task/DeleteAllMailTask.h"


void CPlayerMailSession::UpdateMailListInfo(CNormalMail* pMail )
{
	if ( !pMail )
		return;

	UpdateMailListTask* pUpdateTask = NEW UpdateMailListTask( NULL, pMail );
	if ( pUpdateTask )
		TaskPoolSingle::instance()->PushTask( pUpdateTask );
}

bool CPlayerMailSession::GetNewMail(CNormalMail* pMail )
{
	if ( !pMail )
		return false;

	if ( pMail->GetMailType() == E_NORMAL_MAIL )
	{
		//普通邮件的大小只能限定在100封
		if ( mPlayerNormalMailVec.size() >= MAX_NORMALMAIL_COUNT )
			return false;

		mPlayerNormalMailVec.insert( mPlayerNormalMailVec.begin(), pMail );
	}
	else if( pMail->GetMailType() == E_SYSYTEM_MAIL )
	{
		mPlayerSystemMailVec.insert( mPlayerSystemMailVec.begin(), (CSystemMail *)pMail );
	}
	return true;
}

void CPlayerMailSession::OnOpenUnReadMail(CNormalMail* pMail )
{
	if ( !pMail )
		return;

	if ( pMail->IsMailBeOpen() )
	{
		D_ERROR("OnOpenUnReadMail 邮件已经已读过\n");
		return;
	}

	//打开邮件
	pMail->OpenMail();

	MAIL_TYPE mailType = pMail->GetMailType();
	if ( mailType == E_SYSYTEM_MAIL )
	{
		CSystemMail* pSysMail = ( CSystemMail * )pMail;
		if ( pSysMail && pSysMail->HaveAttachInfo() )//如果是系统邮件,且带有附件 
		{
			unsigned int expiretime = pSysMail->GetMailExpireTime();
			unsigned int timenow = (unsigned int)time(NULL);
			int subtime = expiretime - timenow;
			if ( subtime >  SYSMAIL_ATTACH_LEFTTIME  )//带有附件的邮件被打开后保存3天
			{
				pSysMail->SetMailExpireTime( timenow + SYSMAIL_ATTACH_LEFTTIME );
			}
		}
	}

	UpdateMailListInfo( pMail );
}

CSystemMail* CPlayerMailSession::GetSystemMail(unsigned int mailuid )
{
	std::vector<CSystemMail *>::iterator lter = mPlayerSystemMailVec.begin(),
		lterEnd = mPlayerSystemMailVec.end();
	for ( ; lter != lterEnd; ++lter )
	{
		CSystemMail* pMail = *lter;
		if ( pMail && pMail->GetMailUID() == mailuid )
		{
			return pMail;
		}
	}
	return NULL;
}

CNormalMail* CPlayerMailSession::GetNormalMail(unsigned int mailuid )
{
	std::vector<CNormalMail *>::iterator lter = mPlayerNormalMailVec.begin(),
		lterEnd = mPlayerNormalMailVec.end();
	for ( ; lter != lterEnd; ++lter )
	{
		CNormalMail* pMail = *lter;
		if ( pMail && pMail->GetMailUID() == mailuid )
		{
			return pMail;
		}
	}
	return NULL;
}

bool CPlayerMailSession::RemoveMail(MAIL_TYPE mailtype ,unsigned int mailuid , bool& isUnReadMail )
{
	if (mailtype >= E_INVAILD_MAIL )
		return false;

	isUnReadMail = false;

	if ( mailtype == E_NORMAL_MAIL )
	{
		std::vector<CNormalMail *>::iterator lter = mPlayerNormalMailVec.begin();
		for ( ;lter !=  mPlayerNormalMailVec.end(); ++lter )
		{
			CNormalMail* pMail = *lter;
			if ( pMail && pMail->GetMailUID() == mailuid )
			{
				if ( !pMail->IsMailBeOpen() )
					isUnReadMail = true;

				delete *lter;
				mPlayerNormalMailVec.erase(lter);
				return true;
			}
		}
	}
	else if( mailtype == E_SYSYTEM_MAIL )
	{
		std::vector<CSystemMail *>::iterator lter = mPlayerSystemMailVec.begin();
		for ( ; lter != mPlayerSystemMailVec.end(); ++lter )
		{
			CSystemMail* pMail = *lter;
			if ( pMail && pMail->GetMailUID() == mailuid )
			{
				if ( !pMail->IsMailBeOpen() )
					isUnReadMail = true;

				delete *lter;
				mPlayerSystemMailVec.erase( lter );
				return true;
			}
		}
	}

	return false;
}

bool CPlayerMailSession::LoadNormalMail(unsigned int mailuid,bool isOpen,unsigned int recvPlayerUID, unsigned int sendPlayerUID,const char* pMailSendPlayerName, int expireTime, const char* pMailTitle, const char* pMailContent )
{
	if ( pMailSendPlayerName == NULL  || pMailTitle == NULL || pMailContent == NULL )
		return false;

	//最大限制200封
	if ( mPlayerNormalMailVec.size() >= MAX_NORMALMAIL_COUNT )
	{
		D_ERROR("玩家%d 的普通邮件收件箱满了,不能接受邮件\n", recvPlayerUID);
		return false;
	}

	CNormalMail* pNormalMail = NEW CNormalMail( mailuid, isOpen, expireTime, recvPlayerUID, sendPlayerUID,  pMailSendPlayerName, pMailTitle, pMailContent );
	if ( pNormalMail )
	{
		mPlayerNormalMailVec.push_back( pNormalMail );
		return true;
	}
	return false;
}


bool CPlayerMailSession::LoadSystemMail(unsigned int mailuid, bool isOpen,unsigned int recvPlayerUID, unsigned int sendPlayerUID,const char* pMailSendPlayerName,int expireTime, const char* pMailTitle, const char* pMailContent ,int money, ItemInfo_i* pItemArr )
{
	ACE_UNUSED_ARG( sendPlayerUID );

	if ( pMailSendPlayerName == NULL  || pMailTitle == NULL || pMailContent == NULL )
		return false;

	CSystemMail* pSystemMail = NEW CSystemMail( mailuid, isOpen, expireTime, recvPlayerUID, pMailTitle, pMailContent , money, pItemArr );
	if ( pSystemMail )
	{
		mPlayerSystemMailVec.push_back( pSystemMail );
		return true;
	}

	return false;
}


void CPlayerMailSession::OnQueryMailDetailInfo( CGateServer* pGateSrv, const PlayerID& queryPlayerID, MAIL_TYPE mailType , unsigned int mailUID )
{
	if ( !pGateSrv )
		return;

	if ( mailType >= E_INVAILD_MAIL )
		return;

	CheckMailIsExpire();

	FGQueryMailDetialInfoRst detialRst;
	StructMemSet( detialRst, 0x0, sizeof(FGQueryMailDetialInfoRst) ); 
	detialRst.queryPlayer = queryPlayerID;
	detialRst.mailuid = mailUID;
	bool openStateChange = false;

	if ( mailType == E_NORMAL_MAIL  )
	{
		CNormalMail* pNormalMail = GetNormalMail( mailUID );
		if ( pNormalMail )
		{
			if ( !pNormalMail->IsMailBeOpen() )//如果没有打开邮件
			{
				OnOpenUnReadMail( pNormalMail );
				openStateChange = true;
			}
			StructMemCpy( detialRst.szContent, pNormalMail->GetMailContent(), sizeof(detialRst.szContent)  );
			StructMemCpy( detialRst.szMailTitle, pNormalMail->GetMailTitle(), sizeof(detialRst.szMailTitle) );
			StructMemCpy( detialRst.szSendPlayerName, pNormalMail->GetSendPlayerName(), sizeof(detialRst.szSendPlayerName) );
		}
		else
		{
			detialRst.mailuid = 0;//标示该邮件已过期 
		}
	}
	else
	{
		CSystemMail* pSystemMail = GetSystemMail( mailUID );
		if ( pSystemMail )
		{
			if ( !pSystemMail->IsMailBeOpen() )//如果没有打开邮件
			{
				OnOpenUnReadMail( pSystemMail );
				openStateChange = true;
			}

			StructMemCpy( detialRst.szContent, pSystemMail->GetMailContent(), sizeof(detialRst.szContent)  );
			StructMemCpy( detialRst.szMailTitle, pSystemMail->GetMailTitle(), sizeof(detialRst.szMailTitle) );
			StructMemCpy( detialRst.szSendPlayerName, pSystemMail->GetSendPlayerName(), sizeof(detialRst.szSendPlayerName) );
	

			//如果是系统邮件，则需要发送附件
			FGQueryMailDetialAttachInfoRst queryAttachInfo;
			queryAttachInfo.mailuid =  mailUID;
			queryAttachInfo.queryPlayer = queryPlayerID;
			queryAttachInfo.silverMoney   =  pSystemMail->GetAttachInfoMoney();//双币种判断；
			ACE_OS::memcpy(  queryAttachInfo.attachItem ,  pSystemMail->GetAttachInfoItem(), sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );
			MsgToPut *pMsgToPut = CreateSrvPkg(FGQueryMailDetialAttachInfoRst,pGateSrv,queryAttachInfo );
			pGateSrv->SendPkgToSrv(pMsgToPut);
		}
		else
		{
			detialRst.mailuid = 0;//标示该邮件已过期 
		}
	}

	MsgToPut *pMsgToPut = CreateSrvPkg(FGQueryMailDetialInfoRst,pGateSrv,detialRst );
	pGateSrv->SendPkgToSrv(pMsgToPut);
	

	//如果玩家的未读邮件的个数改变了
	//if ( openStateChange )
	{
		unsigned int unreadCount = GetUnReadMailCount();

		FGUpdatePlayerUnReadMailCount updateUnreadCount;
		updateUnreadCount.playerID = queryPlayerID;
		updateUnreadCount.unreadCount = unreadCount;

		MsgToPut* pMsgToPut = CreateSrvPkg(FGUpdatePlayerUnReadMailCount, pGateSrv, updateUnreadCount );
		pGateSrv->SendPkgToSrv( pMsgToPut );
	}	
}

void CPlayerMailSession::OnQueryMailListByPage(CGateServer* pGateSrv , const PlayerID& queryPlayerID, MAIL_TYPE mailType, unsigned int pageIndex )
{
	if ( mailType >= E_INVAILD_MAIL )
		return;

	//每次查询时，监测这些邮件是否过期
	CheckMailIsExpire();

	unsigned int nowtime = (unsigned int)time(NULL);

	//如果是普通的邮件
	if ( mailType == E_NORMAL_MAIL )
	{
		unsigned int mailCount = (unsigned )mPlayerNormalMailVec.size();
		if ( mailCount > MAX_NORMALMAIL_COUNT )
			mailCount = MAX_NORMALMAIL_COUNT;

		unsigned int maxpage = 0;

		if ( mailCount > 0 )
			maxpage = ( mailCount-1)/MAX_MAILPAGE_COUNT + 1;//玩家的最大邮件页数

		if ( maxpage && (pageIndex >=  maxpage) )
		{
			D_ERROR("查询的页数 %d 大于最大的页数 %d \n", pageIndex ,maxpage );
			return;
		}

		unsigned int mailbegin = ( MAX_MAILPAGE_COUNT *  pageIndex);
		unsigned int mailend   =  mailbegin + MAX_MAILPAGE_COUNT;
		mailend   =  mailend   >  mailCount ? mailCount:mailend;

		//如果相等，则证明该页没有邮件
		if ( mailbegin == mailend )
		{
			FGQueryMailListInfoByPageRst queryRst;
			StructMemSet( queryRst, 0x0 , sizeof(FGQueryMailListInfoByPageRst) );
			queryRst.mailOwnerUID = mSessionOwnerID;
			queryRst.pageEnd   = 0x2;
			queryRst.pageIndex = pageIndex;
			queryRst.queryPlayer = queryPlayerID;
			queryRst.mailType    = mailType;
			MsgToPut *pMsgToPut  = CreateSrvPkg(FGQueryMailListInfoByPageRst,pGateSrv,queryRst );
			pGateSrv->SendPkgToSrv(pMsgToPut);
			return;
		}

		bool isPageEnd = ( pageIndex == ( maxpage - 1 ) ); //该页是否为最后一页

		FGQueryMailListInfoByPageRst queryRst1, queryRst2;
		unsigned int query1len = 0 ,query2len = 0; 
		StructMemSet( queryRst1, 0x0 , sizeof(FGQueryMailListInfoByPageRst) );
		StructMemSet( queryRst2, 0x0 , sizeof(FGQueryMailListInfoByPageRst) );
		queryRst1.mailOwnerUID = queryRst2.mailOwnerUID = mSessionOwnerID;
		queryRst1.mailType  = queryRst2.mailType = mailType;

		unsigned int index = 0;
		for ( unsigned int mailIndex = mailbegin; mailIndex < mailend; mailIndex ++ )
		{
			CNormalMail* pMail = mPlayerNormalMailVec[mailIndex];
			if ( pMail )
			{
				//在这一页的编号
				index = mailIndex - mailbegin;

				if ( index < MAX_MAIL_CONCISE_COUNT )
				{
					queryRst1.mailConciseInfo[index].mailuid    = pMail->GetMailUID();
					queryRst1.mailConciseInfo[index].expireTime = pMail->GetExpireTime() - nowtime;
					queryRst1.mailConciseInfo[index].isRead     = pMail->IsMailBeOpen();
					queryRst1.mailConciseInfo[index].titleLen = MAX_MAIL_CONCISE_TITLE_LEN;
					queryRst1.mailConciseInfo[index].nameLen  = MAX_NAME_SIZE;
					SafeStrCpy( queryRst1.mailConciseInfo[index].sendPlayerName,pMail->GetSendPlayerName() );
					queryRst1.mailConciseInfo[index].sendPlayerName[MAX_NAME_SIZE -1 ] = '\0';
					SafeStrCpy( queryRst1.mailConciseInfo[index].mailTitle, pMail->GetMailTitle() );
					queryRst1.mailConciseInfo[index].mailTitle[MAX_MAIL_CONCISE_TITLE_LEN - 1 ] = '\0';
					query1len++;
				}
				else
				{
					unsigned int tmpIndex = index - MAX_MAIL_CONCISE_COUNT;
					queryRst2.mailConciseInfo[tmpIndex].mailuid    = pMail->GetMailUID();
					queryRst2.mailConciseInfo[tmpIndex].expireTime = pMail->GetExpireTime() - nowtime;
					queryRst2.mailConciseInfo[tmpIndex].isRead     = pMail->IsMailBeOpen();
					queryRst2.mailConciseInfo[tmpIndex].titleLen = MAX_MAIL_CONCISE_TITLE_LEN;
					queryRst2.mailConciseInfo[tmpIndex].nameLen  = MAX_NAME_SIZE;
					SafeStrCpy( queryRst2.mailConciseInfo[tmpIndex].sendPlayerName,pMail->GetSendPlayerName() );
					queryRst2.mailConciseInfo[tmpIndex].sendPlayerName[MAX_NAME_SIZE -1 ] = '\0';
					SafeStrCpy( queryRst2.mailConciseInfo[tmpIndex].mailTitle, pMail->GetMailTitle() );
					queryRst2.mailConciseInfo[tmpIndex].mailTitle[MAX_MAIL_CONCISE_TITLE_LEN - 1 ] = '\0';
					query2len++;
				}
			}
		}
		//将前半页发送出去
		queryRst1.pageEnd   |= ( isPageEnd ?0x2:0x0 );
		queryRst1.queryPlayer = queryPlayerID;
		queryRst1.pageIndex   = pageIndex;
		queryRst1.mailArrCount = query1len;
		MsgToPut *pMsgToPut1 = CreateSrvPkg(FGQueryMailListInfoByPageRst,pGateSrv,queryRst1 );
		pGateSrv->SendPkgToSrv(pMsgToPut1);
		//将后半页发送出去
		if ( query2len > 0 )
		{
			queryRst2.pageEnd |= ( isPageEnd ?0x2:0x0 );
			queryRst2.pageEnd |= 0x1;
			queryRst2.queryPlayer = queryPlayerID;
			queryRst2.pageIndex = pageIndex;
			queryRst2.mailArrCount = query2len;
			MsgToPut *pMsgToPut2 = CreateSrvPkg(FGQueryMailListInfoByPageRst,pGateSrv,queryRst2 );
			pGateSrv->SendPkgToSrv(pMsgToPut2);
		}
	}
	else if( mailType == E_SYSYTEM_MAIL )
	{
		unsigned int mailCount = (unsigned )mPlayerSystemMailVec.size();

		unsigned int mailSysBase = 0;
		unsigned int mailValidCount = mailCount;
		if ( mailCount > MAX_SHOW_SYSTEMMAIL_COUNT  )
		{
			mailSysBase = ( mailCount - MAX_SHOW_SYSTEMMAIL_COUNT );
			mailValidCount = MAX_SHOW_SYSTEMMAIL_COUNT;
		}

		unsigned int maxpage = 0;

		if ( mailValidCount>0 )
			maxpage = (mailValidCount-1)/MAX_MAILPAGE_COUNT +1;

		if ( maxpage && (pageIndex >= maxpage) )
		{
			D_ERROR("查询的页数 %d 大于最大的页数 %d \n", pageIndex ,maxpage );
			return;
		}

		//本次系统邮件的起始地址到结束地址
		unsigned int mailbegin =   mailSysBase  + ( MAX_MAILPAGE_COUNT *  pageIndex);
		unsigned int mailend   =   mailbegin +    MAX_MAILPAGE_COUNT;
		mailend   =  mailend   >   mailCount ?    mailCount:mailend;

		//如果相等，则证明该页没有邮件
		if ( mailbegin == mailend )
		{
			FGQueryMailListInfoByPageRst queryRst;
			StructMemSet( queryRst, 0x0 , sizeof(FGQueryMailListInfoByPageRst) );
			queryRst.mailOwnerUID = mSessionOwnerID;
			queryRst.pageEnd   = 0x2;
			queryRst.pageIndex = pageIndex;
			queryRst.queryPlayer = queryPlayerID;
			queryRst.mailType = mailType;
			MsgToPut *pMsgToPut  = CreateSrvPkg(FGQueryMailListInfoByPageRst,pGateSrv,queryRst );
			pGateSrv->SendPkgToSrv(pMsgToPut);
			return;
		}

		bool isPageEnd = ( pageIndex == ( maxpage - 1 ) ); //该页是否为最后一页

		FGQueryMailListInfoByPageRst queryRst1, queryRst2;
		unsigned int query1len = 0 ,query2len = 0; 
		StructMemSet( queryRst1, 0x0 , sizeof(FGQueryMailListInfoByPageRst) );
		StructMemSet( queryRst2, 0x0 , sizeof(FGQueryMailListInfoByPageRst) );
		queryRst1.mailOwnerUID = queryRst2.mailOwnerUID = mSessionOwnerID;
		queryRst1.mailType  = queryRst2.mailType = mailType;

		unsigned int index = 0;
		for ( unsigned int mailIndex = mailbegin; mailIndex < mailend; mailIndex ++ )
		{
			CSystemMail * pMail = mPlayerSystemMailVec[mailIndex];
			if ( pMail )
			{
				index = mailIndex - mailbegin;

				if ( index < MAX_MAIL_CONCISE_COUNT )
				{
					queryRst1.mailConciseInfo[index].mailuid    = pMail->GetMailUID();
					queryRst1.mailConciseInfo[index].expireTime = pMail->GetExpireTime() - nowtime;
					queryRst1.mailConciseInfo[index].isRead     = pMail->IsMailBeOpen();
					queryRst1.mailConciseInfo[index].titleLen = MAX_MAIL_CONCISE_TITLE_LEN;
					queryRst1.mailConciseInfo[index].nameLen  = MAX_NAME_SIZE;
					SafeStrCpy( queryRst1.mailConciseInfo[index].sendPlayerName,pMail->GetSendPlayerName() );
					queryRst1.mailConciseInfo[index].sendPlayerName[MAX_NAME_SIZE -1 ] = '\0';
					SafeStrCpy( queryRst1.mailConciseInfo[index].mailTitle, pMail->GetMailTitle() );
					queryRst1.mailConciseInfo[index].mailTitle[MAX_MAIL_CONCISE_TITLE_LEN - 1 ] = '\0';

					query1len++;
				}
				else
				{
					unsigned int tmpIndex = index - MAX_MAIL_CONCISE_COUNT;
					queryRst2.mailConciseInfo[tmpIndex].mailuid    = pMail->GetMailUID();
					queryRst2.mailConciseInfo[tmpIndex].expireTime = pMail->GetExpireTime() - nowtime;
					queryRst2.mailConciseInfo[tmpIndex].isRead     = pMail->IsMailBeOpen();
					queryRst2.mailConciseInfo[tmpIndex].titleLen = MAX_MAIL_CONCISE_TITLE_LEN;
					queryRst2.mailConciseInfo[tmpIndex].nameLen  = MAX_NAME_SIZE;
					SafeStrCpy( queryRst2.mailConciseInfo[tmpIndex].sendPlayerName,pMail->GetSendPlayerName() );
					queryRst2.mailConciseInfo[tmpIndex].sendPlayerName[MAX_NAME_SIZE -1 ] = '\0';
					SafeStrCpy( queryRst2.mailConciseInfo[tmpIndex].mailTitle, pMail->GetMailTitle() );
					queryRst2.mailConciseInfo[tmpIndex].mailTitle[MAX_MAIL_CONCISE_TITLE_LEN - 1 ] = '\0';

					query2len++;
				}
			}
		}
		//将前半页发送出去
		queryRst1.pageEnd    |= ( isPageEnd ?0x2:0x0 );
		queryRst1.queryPlayer = queryPlayerID;
		queryRst1.pageIndex   = pageIndex;
		queryRst1.mailArrCount = query1len;
		MsgToPut *pMsgToPut1  = CreateSrvPkg(FGQueryMailListInfoByPageRst,pGateSrv,queryRst1 );
		pGateSrv->SendPkgToSrv(pMsgToPut1);
		//将后半页发送出去
		if ( query2len>0 )
		{
			queryRst2.pageEnd |= ( isPageEnd ?0x2:0x0 );
			queryRst2.pageEnd |= 0x1;
			queryRst2.pageIndex   = pageIndex;
			queryRst2.queryPlayer = queryPlayerID;
			queryRst2.mailArrCount = query2len;
			MsgToPut *pMsgToPut2 = CreateSrvPkg(FGQueryMailListInfoByPageRst,pGateSrv,queryRst2 );
			pGateSrv->SendPkgToSrv(pMsgToPut2);
		}
	}
}


void CPlayerMailSession::PickItemFromMail(CGateServer* pSrv, const PlayerID& pickPlayerID, unsigned int mailuid, unsigned int pickIndex )
{
	if ( !pSrv )
		return;

	CheckMailIsExpire();

	CSystemMail* pSysMail = GetSystemMail( mailuid );
	if ( pSysMail )
	{
		MailAttachInfo& mailAttachInfo = pSysMail->GetMailAttachInfo();

		if ( pickIndex >=  MAX_MAIL_ATTACHITEM_COUNT )
		{
			D_ERROR("拾取的范围超过了附件的范围\n");
			return;
		}

		if ( mailAttachInfo.mAttachItem[pickIndex].usItemTypeID == 0 )
		{
			D_ERROR("拾取邮件附件的对应的位置为空\n");
			return;
		}

		FGPickMailAttachItem pickItem;
		pickItem.mailuid   = mailuid;
		pickItem.pickIndex = pickIndex;
		pickItem.itemInfo  = mailAttachInfo.mAttachItem[pickIndex];
		pickItem.pickPlayerID = pickPlayerID;
		MsgToPut *pMsgToPut = CreateSrvPkg(FGPickMailAttachItem,pSrv,pickItem );
		pSrv->SendPkgToSrv(pMsgToPut);

		//清除对应位置的道具
		pSysMail->ClearAttachItem( pickIndex );


		//存入数据库
		UpdateMailListTask* pUpdateTask = NEW UpdateMailListTask( NULL, pSysMail );
		if ( pUpdateTask )
			TaskPoolSingle::instance()->PushTask( pUpdateTask );
	}
	else
	{
		FGQueryMailDetialInfoRst mailDetialInfo;
		StructMemSet( mailDetialInfo, 0x0,sizeof(mailDetialInfo) );
		mailDetialInfo.queryPlayer = pickPlayerID;
		MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMailDetialInfoRst,pSrv,mailDetialInfo );
		pSrv->SendPkgToSrv(pMsgToPut);

		D_ERROR("取出附件的系统邮件在玩家邮箱中找不到\n");
	}
}


void CPlayerMailSession::PickMoneyFromMail(CGateServer* pSrv, const PlayerID& pickPlayerID, unsigned int mailuid )
{
	if ( !pSrv )
		return;

	CheckMailIsExpire();

	CSystemMail* pSysMail = GetSystemMail( mailuid );
	if ( pSysMail )
	{
		MailAttachInfo& mailAttachInfo = pSysMail->GetMailAttachInfo();

		if ( mailAttachInfo.mMoney !=0 )
		{
			FGPickMailAttachMoney pickMoney;
			pickMoney.mailuid = mailuid;
			pickMoney.money   = mailAttachInfo.mMoney;
			pickMoney.pickPlayerID = pickPlayerID;
			MsgToPut *pMsgToPut = CreateSrvPkg(FGPickMailAttachMoney,pSrv,pickMoney );
			pSrv->SendPkgToSrv(pMsgToPut);

			mailAttachInfo.mMoney = 0;
			//存入数据库
			UpdateMailListTask* pUpdateTask = NEW UpdateMailListTask( NULL, pSysMail );
			if ( pUpdateTask )
				TaskPoolSingle::instance()->PushTask( pUpdateTask );
			return;
		}

	}
	else
	{
		FGQueryMailDetialInfoRst mailDetialInfo;
		StructMemSet( mailDetialInfo, 0x0,sizeof(mailDetialInfo) );
		mailDetialInfo.queryPlayer = pickPlayerID;
		MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMailDetialInfoRst,pSrv,mailDetialInfo );
		pSrv->SendPkgToSrv(pMsgToPut);
	}
}

void CPlayerMailSession::SetMailAttachMoney(unsigned int mailuid ,int money )
{
	CSystemMail* pSysMail = GetSystemMail( mailuid );
	if ( pSysMail )
	{
		pSysMail->SetAttachMoney( money );

		//存入数据库
		UpdateMailListTask* pUpdateTask = NEW UpdateMailListTask( NULL, pSysMail );
		if ( pUpdateTask )
			TaskPoolSingle::instance()->PushTask( pUpdateTask );
	}
}

void CPlayerMailSession::DeleteAllMail()
{
	TRY_BEGIN;

	std::vector<CNormalMail *>::iterator lter = mPlayerNormalMailVec.begin();
	for ( ; lter != mPlayerNormalMailVec.end(); ++lter )
	{
		if( *lter )
			delete *lter;
	}
	mPlayerNormalMailVec.clear();


	std::vector<CSystemMail *>::iterator iter = mPlayerSystemMailVec.begin();
	for ( ; iter != mPlayerSystemMailVec.end(); ++iter )
	{
		if( *iter )
			delete *iter;
	}
	mPlayerSystemMailVec.clear();

	TRY_END;
}

void CPlayerMailSession::CheckMailIsExpire()
{
	unsigned int nowtime = (unsigned int)time(NULL);

	if ( !mPlayerNormalMailVec.empty() )
	{
		std::vector<CNormalMail *>::iterator lter = mPlayerNormalMailVec.begin();
		for ( ; lter != mPlayerNormalMailVec.end(); )
		{
			CNormalMail* pNormalMail = *lter;
			if( pNormalMail )
			{
				unsigned int expiretime = pNormalMail->GetExpireTime();
				if ( nowtime >= expiretime )
				{
					lter = mPlayerNormalMailVec.erase(lter);
					continue;
				}
			}
			++lter;
		}
	}

	if( !mPlayerSystemMailVec.empty() )
	{
		std::vector<CSystemMail *>::iterator iter = mPlayerSystemMailVec.begin();
		for ( ; iter != mPlayerSystemMailVec.end() ; )
		{
			CSystemMail* pSysMail = *iter;
			if ( pSysMail )
			{
				unsigned int expiretime = pSysMail->GetExpireTime();
				if ( nowtime >= expiretime )
				{
					iter = mPlayerSystemMailVec.erase(iter);
					continue;
				}
			}
			++iter;
		}
	}
}

unsigned int CPlayerMailSession::GetUnReadMailCount()
{
	int unreadCount = 0;
	for ( std::vector<CNormalMail *>::iterator lter = mPlayerNormalMailVec.begin(); lter != mPlayerNormalMailVec.end(); ++lter )
	{
		CNormalMail* pNormal = *lter;
		if ( pNormal && !pNormal->IsMailBeOpen() )
			unreadCount++;
	}

	for( std::vector<CSystemMail *>::iterator lter = mPlayerSystemMailVec.begin(); lter != mPlayerSystemMailVec.end(); ++lter )
	{
		CSystemMail* pSystem = *lter;
		if ( pSystem && !pSystem->IsMailBeOpen())
			unreadCount++;
	}

	return unreadCount;
}

unsigned int CPlayerMailSession::GetMailPageCount( MAIL_TYPE mailtype )
{
	unsigned int mailcount = 0;
	if ( mailtype == E_SYSYTEM_MAIL )
	{
		mailcount = (unsigned int)mPlayerSystemMailVec.size();
	}else if( mailtype == E_NORMAL_MAIL )
	{
		mailcount = (unsigned int)mPlayerNormalMailVec.size();
	}

	unsigned int mailpage = 0;
	if ( mailcount > 0 )
		 mailpage = (mailcount-1)/MAX_MAILPAGE_COUNT + 1;
	else
		mailpage  = 0;

	return mailpage;
}

void CPlayerMailSession::SetMailAttachItem(unsigned int mailuid ,unsigned int itemIndex, ItemInfo_i& itemInfo )
{
	CSystemMail* pSysMail = GetSystemMail( mailuid );
	if ( pSysMail )
	{
		pSysMail->SetItemAttachItem( itemIndex , itemInfo );

		//存入数据库
		UpdateMailListTask* pUpdateTask = NEW UpdateMailListTask( NULL, pSysMail );
		if ( pUpdateTask )
			TaskPoolSingle::instance()->PushTask( pUpdateTask );
	}
}

CPlayerMailSession* CPlayerMailSessionManager::FindPlayerMailSession(unsigned int playeruid)
{
	std::map<unsigned int,CPlayerMailSession *>::iterator lter = mPlayermailMap.find( playeruid );
	if ( lter !=  mPlayermailMap.end() )
	{
		return lter->second;
	}
	return NULL;
}


CPlayerMailSession* CPlayerMailSessionManager::NewPlayerMailSession( unsigned int playeruid )
{
	CPlayerMailSession* pPlayerMail = FindPlayerMailSession( playeruid );
	if ( !pPlayerMail )
	{
		pPlayerMail = NEW CPlayerMailSession( playeruid );
		mPlayermailMap.insert( std::pair<unsigned int ,CPlayerMailSession*>( playeruid,pPlayerMail ) );
	}
	return pPlayerMail;
}

void CPlayerMailSessionManager::DeletePlayerMailSession(unsigned int playeruid )
{
	std::map<unsigned int,CPlayerMailSession *>::iterator lter = mPlayermailMap.find( playeruid );
	if ( lter !=  mPlayermailMap.end() )
	{
		if ( lter->second )
			delete lter->second;
		
		mPlayermailMap.erase( lter );
		
	}
}

void CPlayerMailSessionManager::QueryMailListByPage(CGateServer* pGateSrv, unsigned int queryPlayerUID, PlayerID& playerID,
													MAIL_TYPE mailType ,unsigned int page )
{
	if ( !pGateSrv )
		return;

	CPlayerMailSession* pSession = FindPlayerMailSession( queryPlayerUID );
	if ( pSession == NULL )
	{
		pSession = NewPlayerMailSession( queryPlayerUID  );
		if ( !pSession )
		{
			D_ERROR("创建MailSession对象失败\n");
			return;
		}

#ifdef _DEBUG
		D_DEBUG("玩家%d不在邮件管理中，重新从数据库中加载邮件\n",queryPlayerUID );
#endif
		QueryMailListTask* pQueryTask = NEW QueryMailListTask( pGateSrv, pSession, playerID,E_SYSYTEM_MAIL, 0 );
		if ( pQueryTask )
			TaskPoolSingle::instance()->PushTask( pQueryTask );
	}
	else
	{
		if ( !pSession->LoadMailOK() )
		{
			D_DEBUG("邮件还未加载完成,暂且忽略查询消息\n");
			return;
		}

		pSession->OnQueryMailListByPage( pGateSrv, playerID,mailType, page );
	}
}

void CPlayerMailSessionManager::QueryMailDetialInfo(CGateServer* pGateSrv, unsigned int queryPlayerUID, PlayerID& playerID, MAIL_TYPE mailType, unsigned int mailuid )
{
	if ( !pGateSrv )
		return;

	CPlayerMailSession* pSession = FindPlayerMailSession( queryPlayerUID );
	if ( pSession == NULL )
	{
		D_ERROR("查询玩家的邮件详细信息时，找不到玩家对应的邮件管理session\n");
		return;
	}

	if ( !pSession->LoadMailOK() )
	{
		D_ERROR("邮件还未加载成功，就接到了查询邮件详细信息的消息\n");
		return;
	}

	pSession->OnQueryMailDetailInfo( pGateSrv, playerID,mailType, mailuid );

}

void CPlayerMailSessionManager::DeleteMail(CGateServer* pGateSrv, unsigned int queryPlayerUID, PlayerID& playerID, MAIL_TYPE mailType, unsigned int mailuid )
{
	if ( !pGateSrv )
		return;

	CPlayerMailSession* pSession  = FindPlayerMailSession( queryPlayerUID );
	if ( pSession )
	{
		if ( !pSession->LoadMailOK() )
		{
			D_ERROR("加载邮件未完成，不能删除邮件\n");
			return;
		}

		//检查邮件是否过期
		pSession->CheckMailIsExpire();

		FGDeleteMailRst delmailRst;
		delmailRst.mailuid = mailuid;
		delmailRst.deletePlayerID = playerID;
		delmailRst.delmailRst = false;

		bool isUnReadMail = false;
		unsigned int mailindex = 0;
		unsigned int mailcount = 0;

		unsigned int oldpagecount = pSession->GetMailPageCount( mailType );
		//如果已经删除邮件成功，通知client
		if( pSession->RemoveMail( mailType, mailuid , isUnReadMail ) )
		{
			delmailRst.delmailRst = true;

			unsigned int newpagecount = pSession->GetMailPageCount( mailType );
		}
		else
		{
			FGQueryMailDetialInfoRst detialRst;
			StructMemSet( detialRst, 0x0, sizeof(FGQueryMailDetialInfoRst) ); 
			detialRst.queryPlayer = playerID;
			detialRst.mailuid = 0;
			MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMailDetialInfoRst,pGateSrv,detialRst );
			pGateSrv->SendPkgToSrv(pMsgToPut);
		}

		MsgToPut *pMsgToPut = CreateSrvPkg(FGDeleteMailRst,pGateSrv,delmailRst );
		pGateSrv->SendPkgToSrv(pMsgToPut);

		//如果删除的是未读邮件
		if ( isUnReadMail )
		{
			unsigned int unreadCount =  pSession->GetUnReadMailCount();

			//通知客户端改变
			FGUpdatePlayerUnReadMailCount updateUnreadCount;
			updateUnreadCount.playerID = playerID;
			updateUnreadCount.unreadCount = unreadCount;
			MsgToPut *pMsgToPut = CreateSrvPkg(FGUpdatePlayerUnReadMailCount,pGateSrv,updateUnreadCount );
			pGateSrv->SendPkgToSrv(pMsgToPut);
		}
	}

	DeleteMailTask* pTask = NEW DeleteMailTask( queryPlayerUID, mailuid );
	if ( pTask )
		TaskPoolSingle::instance()->PushTask( pTask );
}


void CPlayerMailSessionManager::PickMailAttachInfo(CGateServer* pGateSrv, unsigned int pickPlayerUID, PlayerID& playerID,unsigned int mailuid, unsigned int pickIndex )
{
	if ( !pGateSrv )
		return;

	CPlayerMailSession* pSession = FindPlayerMailSession( pickPlayerUID );
	if ( pSession )
	{
		if ( !pSession->LoadMailOK() )
		{
			D_DEBUG("加载邮件未完成，不能拾取邮件附件\n");
			return;
		}
		pSession->PickItemFromMail( pGateSrv, playerID,mailuid,pickIndex  );
	}
	else
	{
		D_ERROR("玩家邮件信息还未加载进入内存，不能拾取附件\n");
	}
}

void CPlayerMailSessionManager::PickMailMoney(CGateServer* pGateSrv, unsigned int pickPlayerUID, PlayerID& playerID,unsigned int mailuid )
{
	if ( !pGateSrv )
		return;

	CPlayerMailSession* pSession = FindPlayerMailSession( pickPlayerUID );
	if ( pSession )
	{
		if ( !pSession->LoadMailOK() )
		{
			D_DEBUG("加载邮件未完成，不能拾取邮件附件\n");
			return;
		}

		pSession->PickMoneyFromMail( pGateSrv, playerID, mailuid );
	}
	else
	{
		D_ERROR("玩家邮件信息还未加载进入内存，不能拾取附件\n");
	}
}

void CPlayerMailSessionManager::PickMailAttachItemError(unsigned int pickPlayerUID, unsigned int mailuid, unsigned int pickIndex, ItemInfo_i& pickItem )
{
	CPlayerMailSession* pSession = FindPlayerMailSession( pickPlayerUID );
	if ( pSession )
	{
		if ( !pSession->LoadMailOK() )
		{
			D_DEBUG("加载邮件未完成，不能拾取邮件附件\n");
			return;
		}

		pSession->SetMailAttachItem( mailuid, pickIndex, pickItem );
	}
	else
	{
		D_ERROR("玩家邮件信息还未加载进入内存，不能拾取附件\n");
	}
}

void CPlayerMailSessionManager::PickMailAttachMoneyError(unsigned int pickPlayerUID, unsigned int mailuid, int money )
{
	CPlayerMailSession* pSession = FindPlayerMailSession( pickPlayerUID );
	if ( pSession )
	{
		if ( !pSession->LoadMailOK() )
		{
			D_DEBUG("加载邮件未完成，不能拾取邮件附件\n");
			return;
		}

		pSession->SetMailAttachMoney( mailuid, money );
	}
	else
	{
		D_ERROR("玩家邮件信息还未加载进入内存，不能拾取附件\n");
	}
}

void CPlayerMailSessionManager::DeleteAllMail(unsigned int deletePlayerUID )
{
	CPlayerMailSession* pSession = FindPlayerMailSession( deletePlayerUID );
	if ( pSession )
	{
		if ( !pSession->LoadMailOK() )
		{
			D_DEBUG("加载邮件未完成，不能拾取邮件附件\n");
			return;
		}

		//删除所有邮件
		pSession->DeleteAllMail();
	}

	DeleteAllMailTask* pTask = NEW DeleteAllMailTask( deletePlayerUID );
	TaskPoolSingle::instance()->PushTask( pTask );
}

void CPlayerMailSessionManager::MonitorPlayerSessionLiveTime()
{
#define  SESSION_LIVE_TIME 300 

	time_t curTime = time(NULL); 
	if ( curTime - m_lastMonitorTime > 15 )//每15秒更新一次session状态
	{
		std::map<unsigned int , CPlayerMailSession* >::iterator lter =  mPlayermailMap.begin();
		for ( ; lter != mPlayermailMap.end(); )
		{
			CPlayerMailSession* pSession = lter->second;
			if ( pSession )
			{
				if ( curTime - pSession->GetSessionStartTime() > SESSION_LIVE_TIME )
				{
					delete lter->second;
					mPlayermailMap.erase( lter++ );
					continue;
				}
			}
			++lter;
		}

		m_lastMonitorTime = curTime;
	}
}

