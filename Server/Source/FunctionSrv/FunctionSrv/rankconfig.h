﻿/**
* @file rankconfig.h
* @brief rankconfig.h
* Copyright(c) 2010,上海第九城市游戏研发部
* All rights reserved
* 文件名称: rankconfig.h
* 摘    要: 新的排行榜配置
* 作    者: dzj
* 完成日期: 2010.08.09
*/

#ifndef RANK_CONFIG_H
#define RANK_CONFIG_H

#include "../../Test/testthreadque_wait/buflog.h"

#include "../../Base/XmlManager.h"
#include "../../Base/PkgProc/SrvProtocol.h"

/*
	<rankList type="击杀次数 " title4="种族" id="01" refreshtime="2" title6="击杀次数" title1="名次" sort="0" descript="每月的有效提供声望的击杀次数记录" title3="等级" title5="职业" title2="玩家姓名" >
		<desc number="1" rewardid="235012010" rewardtype="1" rewardnumber="1"  />
	</rankList>
*/

//排行榜刷新类型
enum RankRefreshType
{
	RR_DAY = 0, //每日刷新
	RR_WEEK = 1, //每周刷新
	RR_MONTH = 2, //每月刷新
	RR_LEVEL = 3 //等级刷新，近似实时刷新
};

//排行榜奖励
class CRankReword
{
public:
	CRankReword()
	{
		m_playerNum = 0;
		m_rewardID = 0;
		m_rewardType = 0;
		m_rewardNum = 0;
	}

public:
	bool InitRewordWithXML( CElement* pXmlItem );

	unsigned int GetRewordPlayerNum() { return m_playerNum; }
	unsigned int GetRewardID() { return m_rewardID; }
	unsigned int GetRewardNum() { return m_rewardNum; }

private:
	unsigned int m_playerNum;//奖励的玩家人数；
	unsigned int m_rewardID;//奖品ID号；
	unsigned int m_rewardType;//奖品类型；
	unsigned int m_rewardNum;//奖品数目；
};

//排行榜配置
class CRankConfig
{
public:
	CRankConfig()
	{
		m_rankType = RANK_LEVEL;
		m_refreshType = RR_MONTH;
		m_isDesc = false;
	}

public:
	bool InitRankConfigWithXML( CElement* pXmlItem );
	bool InitRankRewardWithXML( CElement* pXmlItem );

	bool DepthCopyFrom( CRankConfig& inRankConfig )
	{
		StructMemCpy( *this, &inRankConfig, sizeof(*this) );
		return true;
	}

public:
	unsigned int GetRewordPlayerNum() { return m_reword.GetRewordPlayerNum(); }
	unsigned int GetRewardID() { return m_reword.GetRewardID(); }
	unsigned int GetRewardNum() { return m_reword.GetRewardNum(); }

public:
	ERankType GetResRankType() { return m_rankType; };

	RankRefreshType GetRefreshType() { return m_refreshType; };

private:
	ERankType     m_rankType;//对应的排行榜类型；
	RankRefreshType m_refreshType;//刷新时间；
	bool            m_isDesc;//排序顺序是否逆序；
	CRankReword     m_reword;//奖励；
};

class CRankConfigManager
{
public:
	//读取排行榜相关配置
	static bool ReadRankConfig();

public:
	//取指定排行榜的刷新类型；
	static bool GetRankRefreshType( ERankType rankType, RankRefreshType& refreshType );

public:
	static bool GetRewordInfo( ERankType rankType, unsigned int& rewordPlayerNum, unsigned int& rewordID, unsigned int& rewordNum )
	{
		if ( !m_isRankConfigRead )
		{
			NewLog( LOG_LEV_ERROR, "CRankConfigManager::GetRewordInfo，读ranktype:%d奖励时，奖励配置还未初始化", rankType );
			return false;
		}

		if ( rankType >= ARRAY_SIZE( m_arrRankConfig ) )
		{
			NewLog( LOG_LEV_ERROR, "CRankConfigManager::GetRewordInfo，错误的ranktype:%d", rankType );
			return false;
		}

		rewordPlayerNum = m_arrRankConfig[rankType].GetRewordPlayerNum();
		rewordID = m_arrRankConfig[rankType].GetRewardID();
		rewordNum = m_arrRankConfig[rankType].GetRewardNum();
		return true;
	}

public:
	static bool        m_isRankConfigRead;
	static CRankConfig m_arrRankConfig[RANK_TYPE_NUM];
};

#endif //RANK_CONFIG_H

