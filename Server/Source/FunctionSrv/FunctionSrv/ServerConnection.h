﻿/** @file ServerConnection.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-1-3
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef SERVER_CONNECTION_H
#define SERVER_CONNECTION_H

#include <map>
#include "../../Base/Utility.h"
#include "../../Base/BufQueue/BufQueue.h"
#include "../../Base/PkgProc/PacketBuild.h"

#ifdef USE_DSIOCP
extern TCache< UniqueObj< CDsSocket > >* g_poolUniDssocket;//保存socket唯一对象的对象池；
#endif //USE_DSIOCP

#ifdef USE_DSIOCP
    #define CreateServerPkg( PKGTYPE, pPkg ) CPacketBuild::CreatePkg<PKGTYPE>( m_nHandleID, m_nSessionID, GetUniqueDsSocket(), &pPkg );
#else //USE_DSIOCP
    #define CreateServerPkg( PKGTYPE, pPkg ) CPacketBuild::CreatePkg<PKGTYPE>( m_nHandleID, m_nSessionID, &pPkg );
#endif //USE_DSIOCP

class CSrvBase;


/**
* @class CServerConnection
*
* @brief 各服务端srv连接的抽象
* 
*/
class CServerConnection
{
public:
	/// 构造
	CServerConnection(void);
	
	/// 析构
	~CServerConnection(void);

private:
	CServerConnection(const CServerConnection &);

	CServerConnection &operator=(const CServerConnection &);

#ifdef USE_DSIOCP
public:
	void SetUniqueDsSocket( UniqueObj< CDsSocket >* pUniqueSocket )
	{
		if ( NULL == pUniqueSocket )
		{
			return;
		}
		if ( ( NULL == m_pUniqueSocket )
			&& ( NULL != pUniqueSocket->GetUniqueObj() )
			)
		{
			//自己保存一份，因为MsgToPut中的uniqueSocket在回收时会被删去；
			m_pUniqueSocket = g_poolUniDssocket->RetrieveOrCreate();
			m_pUniqueSocket->Init( pUniqueSocket->GetUniqueObj() );
		}		
	}
	UniqueObj< CDsSocket>* GetUniqueDsSocket()
	{
		if ( ( NULL != m_pUniqueSocket ) //非空
			&& ( NULL != m_pUniqueSocket->GetUniqueObj() ) //且对应的句柄仍然有效；
			)
		{
			//复制一份往外传，因为MsgToPut中的uniqueSocket在回收时会被删去；
			UniqueObj< CDsSocket >* pOutUniqueObj = g_poolUniDssocket->RetrieveOrCreate();
			pOutUniqueObj->Init( m_pUniqueSocket->GetUniqueObj() );
			return pOutUniqueObj;
		} else {
			return NULL;
		}
	}
private:
	UniqueObj< CDsSocket >* m_pUniqueSocket;
#endif //USE_DSIOCP

public:
	/// 置相关句柄信息(每次新建时必须调用)
	int SetHandleInfo(/*T_PkgSender* pPkgSender,*/ int nHandleID, int nSessionID );

public:
	/// 新建立时的处理(连接建立)
	void OnCreated(void);

	/// 销毁时的处理(连接断开)
	void OnDestoryed(void);

	/// 请求销毁自身(断开连接)
	void ReqDestorySelf(void);

	/// 如果某连接上解析包错误，则断开此连接；
	void OnPkgError(void);

	/// 收包处理；
	void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

	/// 取句柄ID；
	int GetHandleID(void);

	/// 取sessionID;
	int GetSessionID(void);

	/// 向外发包；
	void SendPkgToSrv(MsgToPut* pPkg);

	//获取对应的Srv的指针
	CSrvBase* GetSrvBase() { return m_pSrvBase; }

private:
	/// 重置自身信息；
	void ResetInfo(void);

private:
	/// 通信句柄标识
	int m_nHandleID;

	/// 通信句柄校验号
	int m_nSessionID; 

	/// 该服务对应的标识号,例如：C、A等，见SrvProtocol.h;
	int m_bySrvType;
 
	/// 对应的具体srv指针；
	CSrvBase* m_pSrvBase;

	bool m_bIsDesSelf;

	//线程同步对象,用于同步消息发送队列
	ACE_Thread_Mutex mutex_;

};


#endif/*SERVER_CONNECTION_H*/
