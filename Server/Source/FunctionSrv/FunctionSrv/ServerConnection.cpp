﻿#include "ServerConnection.h"
#include "OtherServer.h"

//#include "RankManager.h"
#include "rankcache.h"
#include "GateSrvManager.h"
#include "SuspectMailManager.h"
#include "RaceMaster.h"
#include "PublicNoticeManager.h"
#include "AuctionManager.h"

extern IBufQueue *g_pSrvSender;

CServerConnection::CServerConnection(void)
	:m_nHandleID(0), m_nSessionID(0), m_bySrvType(0), m_pSrvBase(NULL)
{
#ifdef USE_DSIOCP
		m_pUniqueSocket = NULL;
#endif //USE_DSIOCP
}

CServerConnection::~CServerConnection(void)
{
}

int CServerConnection::SetHandleInfo(/*T_PkgSender* pPkgSender,*/ int nHandleID, int nSessionID )
{
	m_bIsDesSelf = false;

	m_nHandleID = nHandleID;
	m_nSessionID = nSessionID;


	m_pSrvBase = NEW CGateServer(this, nSessionID );
	if(NULL == m_pSrvBase)
		return -1;

	GateSrvMangerSingle::instance()->PushGateSrv( (CGateServer *)m_pSrvBase );

	//第一个GateSrv连接时加载数据
	if ( GateSrvMangerSingle::instance()->GetGateSrvCount() == 1 )
	{
		//加载所有的族长信息
		CRaceMasterManSingle::instance()->LoadRaceMasterInfo();

		//加载中央公告信息
		CPublicNoticeManagerSingle::instance()->LoadPublicNotice();

		//加载所有的拍卖道具信息
		AuctionManagerSingle::instance()->LoadAuctions();
	}

	return 0;
}

void CServerConnection::OnCreated(void)
{
	ResetInfo();

	return;
}

void CServerConnection::OnDestoryed(void)
{

#ifdef USE_DSIOCP
	if ( NULL != m_pUniqueSocket )
	{
		if ( NULL != g_poolUniDssocket )
		{
			if ( NULL != g_poolUniDssocket )
			{
				g_poolUniDssocket->Release( m_pUniqueSocket );
				m_pUniqueSocket = NULL;
			}
		}
	}
#endif //USE_DSIOCP

	if ( m_nHandleID != 0 )
	{
		//确实为断开，区别于新建连接时的回调；
		if ( NULL != m_pSrvBase)
		{
			D_INFO( "连接%d断开\n", m_nHandleID );

			//移除该gateSrv
			GateSrvMangerSingle::instance()->RemoveGateSrv( (CGateServer *)m_pSrvBase );
			//AllRaceRankChartManagerSingle::instance()->RemoveGateSrv( (CGateServer *)m_pSrvBase );

			delete m_pSrvBase; 
			m_pSrvBase = NULL;
		}
	}

	ResetInfo();

	return;
}

void CServerConnection::ReqDestorySelf(void)
{
	if ( m_bIsDesSelf )
	{
		return;//已经发过断开请求，不再请求断开；
	}

	MsgToPut* disconnectMsg = g_poolMsgToPut->RetrieveOrCreate();

#ifdef USE_DSIOCP
	disconnectMsg->pUniqueSocket = GetUniqueDsSocket();//唯一需要主动设置unisocket者，其余都通过createsrvpkg的方式来生成；
#endif //USE_DSIOCP

	disconnectMsg->nHandleID = m_nHandleID;//向断开执行者传递自身标识；
	disconnectMsg->nSessionID = m_nSessionID;//向断开执行者传递自身标识；
	disconnectMsg->nMsgLen = 0;//表明断开连接请求;
	
	//发送断开自身；
	SendPkgToSrv( disconnectMsg );

	m_bIsDesSelf = true;
	
	D_DEBUG( "主动请求断开SRV(其sessionID:%d)\n", m_nSessionID );
	
	return;
}

void CServerConnection::OnPkgError(void)
{
	//解析包错误时断开连接；
	ReqDestorySelf();

	return;
}

void CServerConnection::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	//以后要处理第一个连接服务标识号，目前因为只连mapsrv，因此直接转发；
	if ( NULL != m_pSrvBase )
	{
		m_pSrvBase->OnPkgRcved( wCmd, pBuf, wPkgLen );
	}
	return; 
}

int CServerConnection::GetHandleID(void)
{
	return m_nHandleID;
}

int CServerConnection::GetSessionID(void)
{
	return m_nSessionID;
}

void CServerConnection::SendPkgToSrv( MsgToPut* pPkg )
{
	if ( NULL != g_pSrvSender )
	{
		//添加与2008.7.7号,为了发送消息的同步机制
		ACE_Guard<ACE_Thread_Mutex> guard( mutex_ );

		g_pSrvSender->PushMsg( pPkg );
		//g_pSrvSender->SetReadEvent();//通知网络模块立即发送出去；
	}

	return;
}

void CServerConnection::ResetInfo(void)
{
	m_nHandleID = 0;
	m_nSessionID = 0;

	return;
}
