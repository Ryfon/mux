﻿/********************************************************************
	created:	2009/12/15
	created:	15:12:2009   14:33
	file:		SimpleObjectPool.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef SIMPLE_MEMORY_POOL_H
#define SIMPLE_MEMORY_POOL_H

#include <list>


struct ObjMemsetTrait
{
	template<class T>
	bool operator()( T& _obj )
	{
		StructMemSet( _obj, 0x0, sizeof(T) );
		return true;
	}
};


struct ObjInitFuncTrait
{
	template<class T>
	bool operator()( T& _obj )
	{
		_obj.Init();
		return true;
	}
};

struct NewObjArrTrait
{
	enum{ objarrlen = 100 };

	template<class T>
	T* operator()( std::list<T *>& objlistT )
	{
		T*   tmpObjectArr  = NEW T[objarrlen];
		if ( tmpObjectArr )
		{
			for ( unsigned int i = 0; i< objarrlen; i++ )
				objlistT.push_back( &tmpObjectArr[i] );

			return tmpObjectArr;
		}
		return NULL;
	}
};

template<class T>
struct NewObjTrait
{
	T* operator()()
	{
		T* tmpObj = NEW T;
		return tmpObj;
	}
};

template<class T,unsigned int poolobjnum, 
         class ObjInitTrait=ObjMemsetTrait,
		 class ObjOutOfRangeTrait=NewObjArrTrait >
class SimpleObjectPool
{
private:
	struct PoolObject
	{
		T            _obj;//对象
		bool         ispoolobj;//是否是池中的对象
		bool         isusenow;//是否现在被使用
		PoolObject():ispoolobj(true),isusenow(false){}

		~PoolObject()
		{
			_obj.~T();
			ispoolobj = true;
			isusenow  = false;
		}
	};

	class DeletePoolObjectFunctor
	{
	public:
		bool operator()( PoolObject*& _poolobj )
		{
			if ( _poolobj )
			{
				delete []_poolobj;
				return true;
			}
			return false;
		}
	};

   typedef typename std::list<PoolObject *>::iterator IterT;


public:
	SimpleObjectPool():mObjectArr(NULL)
	{	
		mpoolinitobjnum = poolobjnum;
		mObjectArr  = NEW PoolObject[mpoolinitobjnum];
		if ( mObjectArr ) 
		{
			for ( unsigned int i = 0 ; i< mpoolinitobjnum; i++ )
				mAllocateList.push_back( &mObjectArr[i] );

			RecordAllocateObjsArrPoint( mObjectArr );
		}
	}

	virtual ~SimpleObjectPool()
	{
		std::for_each( mAllAllocatedobjArrList.begin(), mAllAllocatedobjArrList.end(),DeletePoolObjectFunctor() );
		mAllAllocatedobjArrList.clear();
	}

private:
	SimpleObjectPool( const SimpleObjectPool& );
	SimpleObjectPool& operator=( const SimpleObjectPool& );

private:
	void RecordAllocateObjsArrPoint( PoolObject * allocateobjArr )
	{
		if ( allocateobjArr )
			 mAllAllocatedobjArrList.push_back( allocateobjArr );
	}

	void MarkPoolObjAllocated( PoolObject* _pobj )
	{
		if ( _pobj )
		{
			_pobj->ispoolobj = true;
			_pobj->isusenow  = true;
		}
	}

	void MarkPoolObjDeallocated( PoolObject* _pobj )
	{
		if ( _pobj )
		{
			_pobj->~PoolObject();
		}
	}

public:

	T* Allocate()
	{
		PoolObject* pobjT = NULL;
		if ( mAllocateList.empty() )//如果分配列表为空
		{
			if ( !mDeallocateList.empty() )//回收列表如果不为空,则将回收的对象放入可分配队列
			{
				std::copy( mDeallocateList.begin(), mDeallocateList.end(), back_inserter( mAllocateList) );
				mDeallocateList.clear();
			}
			else//如果回收列表也为空,则依据策略分配新的对象数组
			{
				PoolObject* pTmpObjectArr = ObjOutOfRangeTrait()(mAllocateList);
				if(  pTmpObjectArr )
				{
					mpoolinitobjnum += (unsigned int)mAllocateList.size();//增长poolobjnumber的长度
					RecordAllocateObjsArrPoint( pTmpObjectArr );
				}
			}
		}
		
		if ( !mAllocateList.empty() )//如果可分配对象不为空
		{
			pobjT = mAllocateList.front();
			mAllocateList.pop_front();
			ObjInitTrait()( pobjT->_obj );//初始化对象Trait

			MarkPoolObjAllocated( pobjT );//打上正在使用的标志
		}

		return (T *)pobjT;
	}

	void Dellocate( T* pObjT )
	{
		PoolObject* _pPoolObj = (PoolObject *)pObjT;
		if ( pObjT )
		{
			MarkPoolObjDeallocated( _pPoolObj );//打上不使用的标志

			mDeallocateList.push_back( _pPoolObj );//放入回收列表
		}
	}

private:
	PoolObject*  mObjectArr;
	unsigned int   mpoolinitobjnum;
	std::list<PoolObject *> mAllocateList;
	std::list<PoolObject *> mDeallocateList;
	std::list<PoolObject *> mAllAllocatedobjArrList;
};

#endif