﻿/********************************************************************
	created:	2009/04/24
	created:	24:4:2009   14:19
	file:		SuspectMailManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef SUSPECT_MAIL_MANAGER
#define SUSPECT_MAIL_MANAGER

#include <vector>
#include "MailBase.h"

class  CNormalMail;

struct SuspectMail
{
	PlayerID sendPlayerID;
	CNormalMail* pMail;
	unsigned int sessionID;
	char recvMailPlayerName[MAX_NAME_SIZE];

	SuspectMail():pMail(NULL),sessionID(0)
	{
		sendPlayerID.dwPID = sendPlayerID.wGID = 0;
		recvMailPlayerName[0] = '\0';
	}

	SuspectMail& operator=( const SuspectMail& supectMail )
	{
		if ( this != &supectMail )
		{
			sendPlayerID = supectMail.sendPlayerID;
			pMail = supectMail.pMail;
			sessionID = supectMail.sessionID;
			ACE_OS::strncpy( recvMailPlayerName, supectMail.recvMailPlayerName, MAX_NAME_SIZE );
		}

		return *this;
	}

	SuspectMail( const SuspectMail& supectMail )
	{
		if ( this != &supectMail )
		{
			pMail = supectMail.pMail;
			sendPlayerID = supectMail.sendPlayerID;
			sessionID = supectMail.sessionID;
			ACE_OS::strncpy( recvMailPlayerName, supectMail.recvMailPlayerName, MAX_NAME_SIZE );
		}
	}
};


class CSupectMailManager
{
	friend class ACE_Singleton<CSupectMailManager, ACE_Null_Mutex>;

public:
	CSupectMailManager(){}

	~CSupectMailManager();

public:
	//移出不可信任的邮件
	bool RemoveSuspectMail( unsigned int mailuid, SuspectMail& supectMail );
	//删除不可信任邮件
	void DeleteSuspectMail( unsigned int mailuid );
	//确认邮件
	void AffairMail( unsigned int mailuid , unsigned int recvplayeruid, bool isExist );
	//创建一个普通的不可信的邮件
	bool CreateSuspectNormalMail( unsigned int sessionID, PlayerID& playerID, const char* pMailRecvPlayerName, unsigned int sendPlayerUID,const char* pMailSendPlayerName, const char* pMailTitle, const char* pMailContent  );
	////创建一个系统的不可信的邮件
	bool CreateSuspectSystemMail( unsigned int sessionID, const char* pMailRecvPlayerName, const char* pMailTitle, const char* pMailContent, int money, ItemInfo_i* pItemArr );
	//查询邮件接受者的信息
	void QueryRecvMailPlayerInfo( unsigned int mailuid, const char* pRecvPlayerName );

private:
	std::vector<SuspectMail> mSuspectMailVec;
};

typedef ACE_Singleton<CSupectMailManager, ACE_Null_Mutex> SupectMailManagerSingle;

#endif

