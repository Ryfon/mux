﻿/** @file DealPkg.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-1-3
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef DEAL_PKG_H
#define DEAL_PKG_H

#include "../../Base/PkgProc/DealPkgBase.h"
#include <map>

class CGateServer;


/// Gatesrv消息处理类;
class CDealGateSrvPkg : public IDealPkg<CGateServer>
{
public:
	/// 构造
	CDealGateSrvPkg(void);

	/// 析构
	virtual ~CDealGateSrvPkg(void);

public:
	/// 初始化(注册各命令字对应的处理函数)
	static void Init(void);

public:
	static bool OnRankPlayerOnLine( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRankPlayerOffLine( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPunishPlayerBegin( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryRankChartInfoByPage(  CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( G_F_RANK_CACHE_INFO, &OnRankCacheInfo );//处理mapsrv发来的可能的新入榜信息；
	static bool OnRankCacheInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( G_F_OTHER_RANK, &OnOtherRank );//处理其它srv的排行榜通知(shopsrv生成的星钻排行榜,relationsrv生成的工会排行榜等)；	
	static bool OnOtherRank( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//static bool OnRankChartInfoUpdate( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//static bool OnRemoveRankPlayerByChartID( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen   );

	static bool OnQueryRankInfoByPlayerName( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSendMailToPlayer( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnRecvMailPlayerInfoRst(  CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnDeleteMail( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnQueryMailListByPageIndex( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryMailDetialInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPickItemFromMail( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPickMoneyFromMail(  CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPickItemFromMailError( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPickMoneyFromMailError( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen ) ;

	static bool OnSendSysMailToPlayer( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnDeleteAllMail( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRecvLoadRankChartReq( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//static bool OnRankPlayerLevelUp( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerDeleteRole( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerQueryStorageInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerUpdateStorageItemInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerUpdateStorageSafeInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerUpdateStorageForbiddenInfo(  CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnMapSrvLoadRaceMasterInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateRaceMasterInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnResetRaceMasterDeclareWarInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterNewBorn( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMAddPublicNotice( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMRemovePublicNotice( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	static bool OnGMModifyPublicNotice( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMReLoadPublicNotice( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMQueryAllPublicNotice( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMSelectRaceMaster(  CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBidNewAuction( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBidItemSection1( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBidItemSection2( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerCancelAuction( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerQuerySelfAuction( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerQuerySelfBid( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerQueryAuctionByCondition( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );
};

#endif/*DEAL_PKG_H*/
