﻿/** @file OtherServer.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-1-3
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef OTHER_SERVER_H
#define OTHER_SERVER_H

#include "../../Base/BufQueue/BufQueue.h"
#include <map>
#include <vector>


class CServerConnection;

/**
* @class CSrvBase
*
* @brief 各服务端srv的基类
* 
*/
class CSrvBase
{
public:
	/// 构造
	explicit CSrvBase(CServerConnection* pConn);

	/// 析构
	virtual ~CSrvBase(void);

public:
#ifdef USE_DSIOCP
	UniqueObj< CDsSocket>* CSrvBase::GetUniqueDsSocket();
#endif //USE_DSIOCP
	///取handle;
	int GetHandleID(void);

	///取sessionID;
	int GetSessionID(void);

	///向外发包；
	void SendPkgToSrv(MsgToPut* pPkg);

public:
	/// 收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen ) = 0;

private:
	/// 该服务对应的连接；
	CServerConnection* m_pConn;

};

class CGateServer : public CSrvBase
{
public:
	/// 构造
	explicit CGateServer(CServerConnection* pConn, unsigned int wID );

	/// 析构
	virtual ~CGateServer(void);

public:
	/// 收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

	unsigned int GetID() { return mWID; }

private:
	unsigned int mWID;

};


/**
* @class COtherServerManager
*
* @brief 管理各种类型的server
* 
*/
class COtherServerManager
{
private:
	COtherServerManager();
	~COtherServerManager();

public:
	enum
	{
		GATE_SERVER = 0,
		MAP_SERVER
	};

	typedef std::map<unsigned short, CSrvBase*> ServerInfoMap;
	typedef std::vector<ServerInfoMap> ServerVector;

public:
	/// 添加新server;
	static int addSrv(unsigned short usServerType, unsigned short usServerID, CSrvBase* pServer);

	/// 删除server;
	static void removeSrv(unsigned short usServerType, unsigned short usServerID);
	
	/// 得到特定的mapsrv;
	static CSrvBase* findSvr(unsigned short usServerType, unsigned short usServerID);

private:
	/// 管理所有server
	static ServerVector m_serverVector;

};

#endif/*OTHER_SERVER_H*/
