﻿#include "MailBase.h"


unsigned int CSystemMail::GetAttachItemCount()
{
	int c = 0;
	for ( unsigned int i = 0 ; i< MAX_MAIL_ATTACHITEM_COUNT; i++  )
	{
		if ( mAttachInfo.mAttachItem[i].usItemTypeID != 0 )
			c++;
	}
	return c;
}

bool CSystemMail::HaveAttachInfo()
{
	if ( mAttachInfo.mMoney !=0 || GetAttachItemCount() > 0 )
	{
		return true;
	}
	return false;
}


void CNormalMail::MailBeAffirm(  unsigned int playeruid  )
{
	mMaildetial.mRecvPlayerUID = playeruid;//记录接受玩家的ID号

	MAIL_TYPE mailType = GetMailType();//获取邮件类型
	if ( mailType == E_NORMAL_MAIL )//如果是普通的邮件
	{
		unsigned int expireTime = (unsigned)time(NULL) +  NORMAL_EXPIRE_DAY * DAY_SECOND;//邮件的过期时间
		mMaildetial.mexpireTime = expireTime;
	}
	else if( mailType == E_SYSYTEM_MAIL )//如果是系统邮件
	{
		CSystemMail* pSysMail = ( CSystemMail *)this;
		unsigned int expireTime = (unsigned ) time(NULL);//邮件过期时间
		if ( pSysMail->HaveAttachInfo() )//如果系统邮件含有附件
		{
			expireTime += ATTACH_EXPIRE_DAY * DAY_SECOND;
		}
		else
		{
			expireTime += NORMAL_EXPIRE_DAY * DAY_SECOND;
		}
		mMaildetial.mexpireTime = expireTime;
	}
}

void CSystemMail::SetItemAttachItem(unsigned int itemIndex, ItemInfo_i itemInfo )
{
	if ( itemIndex >= MAX_MAIL_ATTACHITEM_COUNT )
	{
		D_ERROR("CSystemMail::SetItemAttachItem 下标越界，itemIndex=%d\n", itemIndex);
		return;
	}

	mAttachInfo.mAttachItem[itemIndex] = itemInfo;
}

void CSystemMail::ClearAttachItem(unsigned int itemIndex )
{
	if ( itemIndex >= MAX_MAIL_ATTACHITEM_COUNT )
	{	
		D_ERROR("CSystemMail::ClearAttachItem 下标越界，itemIndex=%d\n", itemIndex);
		return;
	}

	ItemInfo_i& itemInfo = mAttachInfo.mAttachItem[ itemIndex ];
	StructMemSet( itemInfo, 0x0, sizeof(ItemInfo_i) );
}

