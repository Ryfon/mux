﻿/********************************************************************
	created:	2010/03/17
	created:	17:3:2010   16:58
	file:		AuctionManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef AUCTION_MANAGER_H
#define AUCTION_MANAGER_H

#include <map>
#include "AuctionQueryHandle.h"
#include "ace/Singleton.h"

class CGateServer;

class AuctionManager
{
	friend class ACE_Singleton<AuctionManager, ACE_Null_Mutex>;

public:
	typedef std::map<unsigned int ,AuctionItemCSInfo>             AuctionItemCSInfoMap;
	typedef std::map<unsigned int ,AuctionItemCSInfo>::iterator   AuctionItemIter;

	AuctionManager()
	{
		m_lastupdatetime = (unsigned int)time(NULL);
	}

	~AuctionManager()
	{

	}

public:
	//当玩家拍卖新的道具时
	void OnPlayerBidNewItem( CGateServer* pGateSrv, const PlayerID& playerID, const NewAuctionItem& newAuctionItem );

	//当玩家发来新的竞拍价格时
	void OnPlayerBidByNewPrice( CGateServer* pGateSrv, const PlayerID& playerID, unsigned int playerUID, unsigned int auctionID, unsigned int newprice );

	//玩家拍卖新的道具时
	void BidItemByNewPrice( CGateServer* pGateSrv, const PlayerID& playerID, unsigned int playerUID, const char* bidPlayerName, unsigned int auctionID, int newprice, int lastprice );

	//依据条件搜索排行榜信息
	void OnPlayerQueryAuctionByCondition(  CGateServer* pGateSrv, const PlayerID& playerID, const GFQueryAuctionCondition& condition );

	//玩家取消拍卖
	void OnPlayerCancelAuction( CGateServer* pGateSrv, const PlayerID& playerID,unsigned int playerUID, unsigned int auctionUID );

	//监视所有的拍卖信息
	void MonitorAllAuctions();

	//玩家查询参与的所有拍卖
	void OnPlayerQuerySelfBid( CGateServer* pGateSrv, const PlayerID& playerID,unsigned int playerUID, unsigned char page );

	//玩家所开始的所有拍卖
	void OnPlayerQuerySelfAuction( CGateServer* pGateSrv, const PlayerID& playerID,unsigned int playerUID, unsigned char page );

	//加载所有的拍卖道具
	void LoadAuctions();


	void InsertNewAuctionItem( const AuctionItemCSInfo& auctionNewItem )
	{
		AuctionItemIter iter = m_auctionItemManager.find(auctionNewItem.auctionID);
		if(m_auctionItemManager.end() != iter)
		{
			D_ERROR("InsertNewAuctionItem插入时key=%d重复\n", auctionNewItem.auctionID);
			return;
		}

		m_auctionItemManager[auctionNewItem.auctionID] = auctionNewItem;
	}

	void RemoveAuction( unsigned int auctionUID )
	{
		AuctionItemIter iter = m_auctionItemManager.find( auctionUID );
		if ( iter != m_auctionItemManager.end() ){
			m_auctionItemManager.erase(iter);
		}
	}

private:

	AuctionItemCSInfo* GetAuctionItemByID( unsigned int auctionUID )
	{
		AuctionItemIter iter = m_auctionItemManager.find( auctionUID );
		if ( iter != m_auctionItemManager.end() ){
			return &iter->second;
		}
		return NULL;
	}

	


private:
	AuctionItemCSInfoMap m_auctionItemManager;
	unsigned int         m_lastupdatetime;
};

typedef ACE_Singleton<AuctionManager, ACE_Null_Mutex> AuctionManagerSingle;


#endif