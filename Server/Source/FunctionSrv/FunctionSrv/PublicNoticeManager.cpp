﻿#include "PublicNoticeManager.h"
#include "RecordHandler.h"
#include "OtherServer.h"
#include "../../Base/PkgProc/DealPkgBase.h"
#include "../../Base/PkgProc/MsgToPut.h"
#include "../../Base/PkgProc/PacketBuild.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "GateSrvManager.h"


void ConvertToPublicNotice( const PublicNoticeEx& noticeex, PublicNotice& noticeref )
{
	noticeref.msgid      = noticeex.msgid;
	noticeref.contentlen = ACE_OS::snprintf( noticeref.content,
		sizeof(noticeref.content),
		"%s",
		noticeex.szContent);
	noticeref.istop = noticeex.top;
	noticeref.broadcastnum = noticeex.broadcastnum;
	noticeref.distancetime = noticeex.distancetime;
	noticeref.starttimelen = ACE_OS::snprintf( noticeref.starttime, 
		sizeof(noticeref.starttime),
		"%d/%d/%d/%d/%d",
		noticeex.msgstarttime.year,
		noticeex.msgstarttime.month,
		noticeex.msgstarttime.day,
		noticeex.msgstarttime.hour,
		noticeex.msgstarttime.mins );

	noticeref.endtimelen = ACE_OS::snprintf( noticeref.endtime, 
		sizeof(noticeref.endtime),
		"%d/%d/%d/%d/%d",
		noticeex.msgendtime.year,
		noticeex.msgendtime.month,
		noticeex.msgendtime.day,
		noticeex.msgendtime.hour,
		noticeex.msgendtime.mins );
}

bool CPublicNoticeManager::SplitTimeStrToMsgTimeInfo(MsgTimeInfo& timeinfo, char* ptimestr )
{
	if ( !ptimestr )
		return false;

	const char* year=NULL;
	const char* month=NULL;
	const char* day=NULL;
	const char* hour=NULL;
	const char* minute=NULL;

	year = ACE_OS::strtok( ptimestr, "/");
	if( year )
	{
		month = ACE_OS::strtok( NULL,"/");
		if ( month )
		{
		   day = ACE_OS::strtok( NULL, "/");
		   if ( day )
		   {
			   hour = ACE_OS::strtok(NULL, "/");
			   if ( hour )
			   {
				   minute = ACE_OS::strtok(NULL,"/");
				   if ( minute )
				   {
					   timeinfo.year  = ACE_OS::atoi( year );
					   timeinfo.month = ACE_OS::atoi( month );
					   timeinfo.day   = ACE_OS::atoi( day);
					   timeinfo.hour  = ACE_OS::atoi( hour );
					   timeinfo.mins  = ACE_OS::atoi( minute );
					   return true;
				   }
			   }
		   }
		}
	}

	return false;
}

bool CPublicNoticeManager::CheckPublicNoticeExipre( const MsgTimeInfo& starttime, const MsgTimeInfo& endtime ,time_t& curtime )
{
	return true;
}

PublicNoticeEx* CPublicNoticeManager::GetPublicNoticeByMsgID(unsigned int msgid )
{
	if( publicnoticeVec.empty() )
		return NULL;

	std::vector<PublicNoticeEx *>::iterator iter = publicnoticeVec.begin();
	for ( unsigned int i=0; iter != publicnoticeVec.end() && i<noticecount; ++iter,++i )
	{
		PublicNoticeEx* publicnotice = *iter;
		if ( !publicnotice )
			continue;
		
		if ( publicnotice->msgid == msgid )
		{
			return publicnotice;
		}
	}
	return NULL;
}

bool CPublicNoticeManager::IsLocalPublicNotice(unsigned int msgid )
{
	if( mlocalpublicnotice.empty() )
		return false;

	return ( std::find( mlocalpublicnotice.begin(),mlocalpublicnotice.end(),msgid  ) != mlocalpublicnotice.end() );
}

void CPublicNoticeManager::RemovePublicNoticeByMsgID(unsigned int msgid )
{
	if( publicnoticeVec.empty() )
		return;

	
	std::vector<PublicNoticeEx *>::iterator iter = publicnoticeVec.begin();
	for ( unsigned int i = 0 ; iter != publicnoticeVec.end() && i<noticecount ; ++iter,++i )//找到需要删除的中央公告
	{
		PublicNoticeEx* publicmsg = *iter;
		if ( publicmsg && publicmsg->msgid == msgid )
		{
			publicmsg->Reset();//清空原来的数据
			break;
		}
	}

	if ( iter != publicnoticeVec.end() )//从公告列表中删除
	{
		publicnoticeVec.push_back( *iter );//将要删除的iter所指的指针放到vector的最后面
		publicnoticeVec.erase(iter);//将iter删除,总个数不变
		noticecount--;//通知个数--;
	}
}

void CPublicNoticeManager::ClearAllPublicNotices()
{
	if( publicnoticeVec.empty() )
		return;

	std::vector<PublicNoticeEx *>::iterator iter = publicnoticeVec.begin();
	for ( unsigned int i = 0 ; iter != publicnoticeVec.end() && i<noticecount ; ++iter,++i )//找到需要删除的中央公告
	{
		PublicNoticeEx* publicmsg = *iter;
		if ( publicmsg )
			publicmsg->Reset();//清空原来的数据
	}
	publicnoticeVec.clear();
	noticecount = 0;
}

void CPublicNoticeManager::SendPublicNoticeToPlayer(CGateServer* pGateSrv ,PlayerID playerID )
{
	if ( !pGateSrv )
		return;

	time_t curtime = time(NULL);
	std::vector<PublicNoticeEx *>::iterator iter = publicnoticeVec.begin();
	for ( unsigned int i = 0 ; iter != publicnoticeVec.end() && i<noticecount ; ++iter,++i )//找到需要删除的中央公告
	{
		PublicNoticeEx* pPublicNotice = *iter;
		if ( pPublicNotice && pPublicNotice->msgid != 0 )
		{
			if ( pPublicNotice->broadcastnum > 0 )//证明是GM修改的,或新增加的公告
			{
				FGRecvPublicNotice noticemsg;
				noticemsg.queryPlayerID = playerID;
				ConvertToPublicNotice( *pPublicNotice, noticemsg.srvmsg.notices );

				MsgToPut *pMsgToPut = CreateSrvPkg( FGRecvPublicNotice,pGateSrv,noticemsg );
				pGateSrv->SendPkgToSrv(pMsgToPut);
			}
			else if( pPublicNotice->broadcastnum == 0 )//证明需要删除的公告
			{
				FGRemovePublicNotice removemsg;
				removemsg.msgid = pPublicNotice->msgid;
				MsgToPut *pMsgToPut = CreateSrvPkg( FGRemovePublicNotice,pGateSrv,removemsg );
				pGateSrv->SendPkgToSrv(pMsgToPut);
			}
		}
	}
}

void CPublicNoticeManager::NoticePublicNoticeModify(const PublicNoticeEx& publicNotice )
{	
	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if ( pGateSrv )
	{
		FGModifyPublicNotice modifynotice;
		StructMemSet( modifynotice, 0x0, sizeof(modifynotice) );
		ConvertToPublicNotice( publicNotice, modifynotice.srvmsg.modifynotice );

		MsgToPut *pMsgToPut = CreateSrvPkg( FGModifyPublicNotice,pGateSrv,modifynotice );
		pGateSrv->SendPkgToSrv(pMsgToPut);
	}
}


void CPublicNoticeManager::NoticePublicNoticeAdd(const PublicNoticeEx& publicNotice )
{
	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if ( pGateSrv )
	{
		FGAddNewPublicNotice addnotice;
		StructMemSet( addnotice, 0x0, sizeof(addnotice) );
		ConvertToPublicNotice( publicNotice, addnotice.srvmsg.newnotice );

		MsgToPut *pMsgToPut = CreateSrvPkg( FGAddNewPublicNotice,pGateSrv,addnotice );
		pGateSrv->SendPkgToSrv(pMsgToPut);
	}
}

void CPublicNoticeManager::NoticePublicNoticeRemove(unsigned int msgid )
{
	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if ( pGateSrv )
	{
		FGRemovePublicNotice  removenotice;
		removenotice.msgid = msgid;
		MsgToPut *pMsgToPut = CreateSrvPkg( FGRemovePublicNotice,pGateSrv,removenotice );
		pGateSrv->SendPkgToSrv(pMsgToPut);
	}
}

void CPublicNoticeManager::ModifyPublicNotice(unsigned msgid, const char* content, char istop, const char* starttime, const char* endtime, int broadcastnum, int distancetime )
{
	if ( !starttime || !endtime || !content )
		return;

	if ( msgid!=0 && broadcastnum==0 )
	{
		D_ERROR("修改公告%d错误,最大播放次数为0\n", msgid );
		return;
	}

	PublicNoticeEx* publicnotice = GetPublicNoticeByMsgID( msgid );
	if ( publicnotice )//如果是所要修改的公告已经是存入数据库的公告,则修改内容,存入数据库
	{
		string starttimestr = starttime;
		string endtimestr   = endtime;
		SafeStrCpy( publicnotice->szContent, content );//新的内容
		publicnotice->distancetime = distancetime;//新的间隔时间
		publicnotice->broadcastnum = broadcastnum;//新的广播次数
		publicnotice->top = istop;//新的是否置顶
		SplitTimeStrToMsgTimeInfo( publicnotice->msgstarttime, const_cast<char *>(starttime) );
		SplitTimeStrToMsgTimeInfo( publicnotice->msgendtime,   const_cast<char *>(endtime) );
		if( CRecordHandler::UpdatePublicNoticeInfo( *publicnotice, starttimestr.c_str(), endtimestr.c_str() ) )//存入数据库
		{
			//通知所有mapsrv,公告被修改了,让他们通知在线玩家
			NoticePublicNoticeModify( *publicnotice );
		}
	}
	else//如果是修改的未存入数据库的公告,(如第一次修改本地配置文件的公告)
	{
		if ( IsLocalPublicNotice(msgid) )
		{
			publicnotice = AddNewPublicNotice( msgid, content, istop, starttime,endtime, broadcastnum, distancetime, false );
			if ( publicnotice )
			{
				//通知所有mapsrv,公告被修改了,让他们通知在线玩家
				NoticePublicNoticeModify( *publicnotice );
			}		
		}
		else
		{
			D_ERROR("修改了错误的公告 MsgID:%d content:%s\n", msgid, content );
		}
	}
}

void CPublicNoticeManager::DeletePublicNotice(unsigned msgid )
{
	bool islocalmsg = IsLocalPublicNotice( msgid );
	if ( islocalmsg )//如果是公告配置文件中的公告
	{
		PublicNoticeEx* publicnotice = GetPublicNoticeByMsgID( msgid );//如果之前已经存在与数据库中了
		if ( publicnotice )
		{
			if ( publicnotice->broadcastnum == 0 )//如果表示已经是已删除
				return;
			else
			{
				publicnotice->Reset();
				publicnotice->msgid = msgid;
				CRecordHandler::UpdatePublicNoticeInfo( *publicnotice, "", "" );//存入数据库,该配置公告已被删除
			}
		}
		else
		{
			PublicNoticeEx* pPublicNotice= AddNewPublicNotice( msgid, "", 0, "","", 0, 0, false );
			if ( pPublicNotice )
			{
				CRecordHandler::UpdatePublicNoticeInfo( *pPublicNotice, "", "" );//存入数据库,该配置公告已被删除
			}
		}
	}
	else//不是配置文件中的公告,直接从数据库中删除
	{
		if( CRecordHandler::DeletePublicNoticeInfo( msgid ) )//从数据库中删除
		{
			RemovePublicNoticeByMsgID(msgid);
		}
	}

	//通知所有mapsrv,公告删除了,让他们通知在线玩家
	NoticePublicNoticeRemove( msgid );
}


void CPublicNoticeManager::ReLoadGMPublicNotice()
{
	if( CRecordHandler::DeleteRemoveAllPublicNotice() )//重新加载所有的公告
	{
		ClearAllPublicNotices();
	}

}

PublicNoticeEx* CPublicNoticeManager::AddNewPublicNotice(unsigned int msgid, const char* content, char istop, const char* starttime, const char* endtime, int broadcastnum, int distancetime ,bool needNotice )
{
	if ( !starttime || !endtime || !content )
		return NULL;

	if ( noticecount>= MAX_PUBLICMSG_COUNT )
	{
		D_ERROR("CPublicNoticeManager::AddNewPublicNotice: noticecount>= MAX_PUBLICMSG_COUNT,其中noticecount=%d\n", noticecount);
		return NULL;
	}

	if (  GetPublicNoticeByMsgID( msgid ) !=NULL )//这个公告之前是否已经存在了
	{
		D_ERROR("已存在中央公告%d,但还接到了增加新公告ID%d 的消息:%s\n", msgid ,msgid, content );
		return NULL;
	}

	PublicNoticeEx* publicnotice = publicnoticeVec[noticecount];
	if ( publicnotice )
	{
		string starttimestr = starttime;
		string endtimestr   = endtime;


		publicnotice->msgid = msgid;
		SafeStrCpy( publicnotice->szContent, content );//新的内容
		publicnotice->distancetime = distancetime;//新的间隔时间
		publicnotice->broadcastnum = broadcastnum;//新的广播次数
		publicnotice->top = istop;//新的是否置顶
		SplitTimeStrToMsgTimeInfo( publicnotice->msgstarttime, const_cast<char *>(starttime) );
		SplitTimeStrToMsgTimeInfo( publicnotice->msgendtime,   const_cast<char *>(endtime) );

		if( CRecordHandler::InsertNewPublicNotice( *publicnotice, starttimestr.c_str(), endtimestr.c_str() ) )
		{
			//通知所有mapsrv,让他们通知client,新的中央公告诞生了
			if( needNotice )
				NoticePublicNoticeAdd( *publicnotice );

			noticecount++;
		}
		return publicnotice;
	}

	return NULL;
}

void CPublicNoticeManager::LoadPublicNoticeXML()
{
	const char xmlconfigpath[] = {"config/centermsg.xml"};
	CXmlManager xmlManager;
	if ( !xmlManager.Load(xmlconfigpath) )
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", xmlconfigpath );
		return;
	}

	CElement* pRoot = xmlManager.Root();
	if ( NULL == pRoot )
	{
		D_ERROR("%s配置文件可能为空\n",xmlconfigpath);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	for (std::vector<CElement *>::iterator lter =childElements.begin(); lter != childElements.end(); ++lter )
	{
		CElement* pElement = *lter;
		if ( pElement && pElement->GetAttr("id") )
		{
			unsigned int msgid =  ACE_OS::atoi( pElement->GetAttr("id") );
			mlocalpublicnotice.push_back(msgid);
		}
	}
}

void CPublicNoticeManager::LoadPublicNotice()
{
	ResetPublicNotice();

	LoadPublicNoticeXML();

	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return;

	// 构造SQL
	std::stringstream ss;
	ss<<"select msgid,content, istop, starttime,endtime,broadcastnum,distancetime from publicmessage";
	std::string statement = ss.str();
	CResult * pResult = pQuery->ExecuteSelect( statement.c_str(), (unsigned long)(statement.size()) );
	if ( pResult ==  NULL )
	{
		D_ERROR("执行SQL语句:%s 错误\n",statement );
		return;
	}
	else
	{
		//本次影响的行数
		int  affectNum = pResult->RowNum();
		if ( affectNum )
		{
			while( pResult->Next() )
			{
				PublicNoticeEx& publicnotice = *publicnoticeVec[noticecount];

				publicnotice.msgid = pResult->GetInt(0);//msgid
				
				if(pResult->GetString(1))
					StructMemCpy( publicnotice.szContent, pResult->GetString(1), sizeof(publicnotice.szContent) );//content

				publicnotice.top = pResult->GetInt(2);//istop
				char* starttime  = const_cast<char *>( pResult->GetString(3));//starttime
				char* endtime    = const_cast<char *>( pResult->GetString(4));//endtime
				publicnotice.broadcastnum = pResult->GetInt(5);//broadcastnum
				publicnotice.distancetime = pResult->GetInt(6);//distancetime

				if ( starttime && endtime )
				{
					SplitTimeStrToMsgTimeInfo( publicnotice.msgstarttime, starttime );
					SplitTimeStrToMsgTimeInfo( publicnotice.msgendtime,   endtime );
				}
				else
					continue;
				
				noticecount++;

				if ( noticecount == MAX_PUBLICMSG_COUNT )
					break;
			}
		}
	}

	pConnection->DestroyQuery( pQuery );
}