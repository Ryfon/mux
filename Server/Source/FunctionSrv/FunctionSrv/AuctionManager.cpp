﻿#include "AuctionManager.h"
#include "Task/TaskPool.h"
#include "Task/LoadAuctionInfoTask.h"

void AuctionManager::OnPlayerBidNewItem( CGateServer* pGateSrv, const PlayerID& playerID, const NewAuctionItem& newAuctionItem )
{
	if ( !pGateSrv )
		return;

	int insertID = QueryAuctionHandle::StartNewAuction( playerID,  newAuctionItem );
	if ( insertID != -1 )//如果插入成功
	{
		AuctionItemCSInfo itemcsInfo;
		itemcsInfo.auctionID       = insertID;
		itemcsInfo.auctionItem     = newAuctionItem.iteminfo;
		itemcsInfo.curauctionprice = newAuctionItem.origiAuctionprice;
		itemcsInfo.expiretime      = newAuctionItem.expiretime;
		itemcsInfo.fixprice        = newAuctionItem.fixprice;
		itemcsInfo.origiprice      = newAuctionItem.origiAuctionprice;

		if( GetAuctionItemByID( insertID ) != NULL )
		{
			D_ERROR("插入拍卖行管理器的唯一编号重复 %d",insertID );
			return;
		}
		else
		{
			InsertNewAuctionItem( itemcsInfo );
		}
	}
}

void AuctionManager::OnPlayerBidByNewPrice( CGateServer* pGateSrv, const PlayerID& playerID, unsigned int playerUID, unsigned int auctionID, unsigned int newprice )
{
	if ( !pGateSrv )
		return;

	int errNo = 0;
	unsigned int lastprice =  0;
	if( QueryAuctionHandle::QueryPlayerLastBidPrice( playerUID, auctionID, lastprice ) )//
	{
		if ( newprice > lastprice )//新的价格应该大于以前的竞拍价格
		{
			int subprice = newprice - lastprice;//看增长的幅度是不是大于%5

			AuctionItemCSInfo* auctionItemInfo = GetAuctionItemByID(auctionID);
			if ( auctionItemInfo )
			{
				if ( ( subprice > (auctionItemInfo->origiprice*0.05)) || 
					   newprice  == auctionItemInfo->fixprice )
				{
					FGBidItemSection1 queryPriceValid;
					queryPriceValid.playerID   = playerID;
					queryPriceValid.auctionUID = auctionID;
					queryPriceValid.newprice   = newprice;
					queryPriceValid.lastPrice  = lastprice;
					MsgToPut *pMsgToPut = CreateSrvPkg( FGBidItemSection1,pGateSrv,queryPriceValid );
					pGateSrv->SendPkgToSrv(pMsgToPut);
				}
				else
					errNo = 3;//价格错误,没有大于最初价格的%5
			}
			else
				errNo = 4;//道具已被竞拍
		}
		else
		{
			D_ERROR("出现错误，玩家本次竞拍的价格比最后一次竞拍还低");
			errNo = 2;
		}
	}
	else
		errNo = 2;

	if( errNo > 0 )
	{
		FGBidItemRst bidItemRst;
		bidItemRst.bidItemRst.auctionUID = auctionID;
		bidItemRst.bidItemRst.rstNo = errNo;//竞拍发生错误
		bidItemRst.bidPlayerID = playerID;
		bidItemRst.costprice = 0;
		MsgToPut *pMsgToPut = CreateSrvPkg( FGBidItemRst,pGateSrv,bidItemRst );
		pGateSrv->SendPkgToSrv(pMsgToPut);
	}
}

void AuctionManager::BidItemByNewPrice(CGateServer* pGateSrv, const PlayerID& playerID, unsigned int playerUID, const char* bidPlayerName, 
									   unsigned int auctionID, int newprice, int lastprice )
{
	if ( !pGateSrv )
		return;

	if ( newprice < 0 )
		return;

	int errNo = 0;
	AuctionItemCSInfo* pItemInfo = GetAuctionItemByID( auctionID );
	if ( pItemInfo )
	{
		if ( pItemInfo->fixprice == newprice )//如果一口价购买了
		{
			if( QueryAuctionHandle::PlayerBidItem( playerID, playerUID, bidPlayerName, auctionID, newprice ) )
			{
				if( !QueryAuctionHandle::OnAuctionEnd( auctionID ) )
				{
					D_ERROR("玩家%s结束竞拍%d失败\n", bidPlayerName, auctionID );
					errNo = 2;
				}
				RemoveAuction( auctionID );
			}
			else
			{
				D_ERROR("玩家%s一口价竞拍失败\n", bidPlayerName );
				errNo = 2;
			}
		}
		else
		{
			if ( (int)pItemInfo->curauctionprice < newprice )//如果是竞价购买
			{
				if( QueryAuctionHandle::PlayerBidItem( playerID, playerUID, bidPlayerName, auctionID, newprice ) )
				{
					pItemInfo->curauctionprice = newprice;
				}
				else
				{
					D_ERROR("玩家%s 竞拍物品出现了错误\n", bidPlayerName );
					errNo = 2;
				}
			}
		}
	}
	else
	{
		D_DEBUG("%s玩家拍卖的物品已经不存在了 %d\n",bidPlayerName, auctionID );
		errNo = 4;
	}

	FGBidItemRst srvmsg;
	srvmsg.bidPlayerID = playerID;
	srvmsg.bidItemRst.auctionUID = auctionID;
	srvmsg.bidItemRst.rstNo = errNo;
	srvmsg.costprice  = newprice - lastprice;
	MsgToPut *pMsgToPut = CreateSrvPkg( FGBidItemRst,pGateSrv,srvmsg );
	pGateSrv->SendPkgToSrv(pMsgToPut);
}

void AuctionManager::OnPlayerQueryAuctionByCondition(CGateServer* pGateSrv, const PlayerID& playerID, const GFQueryAuctionCondition& condition )
{
	if ( !pGateSrv )
		return;

	//玩家查询拍卖行
	if( !QueryAuctionHandle::QueryAuctionByCondition( pGateSrv, playerID, condition )  )
	{
		D_ERROR("玩家查询拍卖榜出错\n");
	}
}

void AuctionManager::OnPlayerCancelAuction(CGateServer *pGateSrv, const MUX_PROTO::PlayerID &playerID, unsigned int playerUID, unsigned int auctionUID)
{
	if ( !pGateSrv )
		return;


	int errNo = 1;
	if( QueryAuctionHandle::OwnerCancelAuction( pGateSrv, playerUID, auctionUID ) )//如果玩家取消拍卖
	{
		RemoveAuction( auctionUID );
		errNo = 0;
	}

	FGCancelAuctionRst srvmsg;
	srvmsg.cancePlayerID        = playerID;
	srvmsg.cancelRst.auctionUID = auctionUID;
	srvmsg.cancelRst.rstNo      = errNo;
	MsgToPut *pMsgToPut = CreateSrvPkg( FGCancelAuctionRst,pGateSrv,srvmsg );
	pGateSrv->SendPkgToSrv(pMsgToPut);
}

void AuctionManager::OnPlayerQuerySelfBid( CGateServer* pGateSrv, const PlayerID& playerID,unsigned int playerUID, unsigned char page )
{
	QueryAuctionHandle::QueryMyBid( pGateSrv, playerID, playerUID, page );
}

void AuctionManager::OnPlayerQuerySelfAuction(CGateServer* pGateSrv, const PlayerID& playerID,unsigned int playerUID, unsigned char page )
{
	QueryAuctionHandle::QueryMyAuction( pGateSrv, playerID, playerUID, page );
}

void AuctionManager::LoadAuctions()
{
	LoadAuctionInfoTask* pTask = NEW LoadAuctionInfoTask( NULL );
	TaskPoolSingle::instance()->PushTask( pTask );
}


void AuctionManager::MonitorAllAuctions()
{
	unsigned int curtime = (unsigned int)time(NULL);

	if ( curtime - m_lastupdatetime >= 60 )//每3分钟遍历一次
	{
		if ( !m_auctionItemManager.empty() )//如果不为空
		{
			AuctionItemIter iter = m_auctionItemManager.begin();
			while ( iter != m_auctionItemManager.end() )
			{
				AuctionItemCSInfo& csAuctionInfo =  iter->second;
				if ( curtime >= csAuctionInfo.expiretime )
				{
					D_DEBUG("拍卖%d 时间过期，结束该拍卖\n", csAuctionInfo.auctionID );
					if( !QueryAuctionHandle::OnAuctionEnd( csAuctionInfo.auctionID ) )
					{
						D_ERROR("结束拍卖%d失败\n",csAuctionInfo.auctionID );
					}
					m_auctionItemManager.erase( iter++ );
				}
				else
					iter++;
			}
		}
		m_lastupdatetime = curtime;
	}


}