﻿#include "LoadRankChartTask.h"
#include "../../../Base/aceall.h"
//#include "../RankManager.h"
#include "../rankcache.h"

//void LoadRankChartTask::OnThread(CConnection* pConnect )
//{
//	//FunctionTrace( "LoadRankChartTask::OnThread"/*__FUNCTION__*/ );
//
//	if ( pConnect == NULL )
//		return;
//
//	if ( m_pRankChart == NULL )
//		return;
//
//	TRY_BEGIN;
//
//	int iRet = 0;
//	CQuery *pQuery = pConnect->CreateQuery();
//	if(pQuery == NULL)
//		iRet = -1;
//
//	if ( iRet != -1 )
//	{
//		char szStatement[1024];
//		int  iStatementLen = 0;
//		if( m_pRankChart->GetRankOrderType() == E_ORDER )
//		{
//			// 构造SQL
//			iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
//				"select playeruid, playername,playerclass,playerlevel,rankinfo,playerrace from rankchart where rankid = %d order by rankinfo desc limit 0, 100 ", 
//				m_pRankChart->GetRankID()
//				);
//		}
//		else if( m_pRankChart->GetRankOrderType() == E_NEGATIVE  )
//		{
//			iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
//				"select playeruid, playername,playerclass,playerlevel,rankinfo,playerrace from rankchart where rankid = %d order by rankinfo asc limit 0,  100", 
//				m_pRankChart->GetRankID()
//				);
//		}
//		else if( m_pRankChart->GetRankOrderType() ==  E_SECOND_ORDER  )
//		{
//			iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
//				"select playeruid, playername,playerclass,playerlevel,rankinfo,playerrace from rankchart where rankid = %d order by playerlevel desc, rankinfo desc limit 0,  100", 
//				m_pRankChart->GetRankID()
//				);
//		}
//		else if( m_pRankChart->GetRankOrderType() ==  E_SECOND_NEGATIVE )
//		{
//			iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
//				"select playeruid, playername,playerclass,playerlevel,rankinfo,playerrace from rankchart where rankid = %d order by playerlevel desc, rankinfo asc limit 0,  100", 
//				m_pRankChart->GetRankID()
//				);
//		}
//
//		// 执行查询
//		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
//		if(pResult == NULL)
//		{
//			D_ERROR("执行sql语句:%s错误\n", szStatement);
//			iRet = -1;
//		}
//		else
//		{
//			//本次影响的行数
//			int  affectNum = pResult->RowNum();
//			if ( affectNum )
//			{
//				//请空排行榜
//				m_pRankChart->ClearChart();
//
//				while( pResult->Next() )
//				{
//					char* NAME = pResult->GetString(1);
//					if ( NAME == NULL )
//						continue;
//
//					int UID    = pResult->GetInt(0);
//					int CLASS  = pResult->GetInt(2);
//					int LEVEL  = pResult->GetInt(3);
//					int RANKINFO = pResult->GetInt(4);
//
//					char* race = pResult->GetString(5);
//					int RACE = 1;
//					if ( race )
//						RACE = atoi( race );
//					else 
//						RACE = m_pRankChart->GetRankRace();
//						       
//					if( !m_pRankChart->NewRankPlayerInfo( NAME ,UID, CLASS,LEVEL, RACE, RANKINFO ) )
//						break;
//				}
//			}
//			else
//			{
//				D_DEBUG("%d 排行榜排行玩家为0\n",m_pRankChart->GetRankID() );
//			}
//			pQuery->FreeResult(pResult);
//		}
//
//		pConnect->DestroyQuery(pQuery);
//	}
//
//	TRY_END;
//}
//
//
//void LoadRankChartTask::OnResult()
//{
//
//}
//
//
//
