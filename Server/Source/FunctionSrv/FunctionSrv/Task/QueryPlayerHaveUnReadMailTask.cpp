﻿#include "QueryPlayerHaveUnReadMailTask.h"

//#include "../RankManager.h"

void CQueryPlayerHaveUnReadMailTask::OnThread(CConnection *pConnect)
{
	//FunctionTrace( "CQueryPlayerHaveUnReadMailTask::OnThread"/*__FUNCTION__*/ );

	if ( !pConnect )
		return;

	if ( !isInit )
		return;

	TRY_BEGIN;


	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		return;

	char szStatement[1024]= {0};
	int  iStatementLen = 0;

	iStatementLen = ACE_OS::snprintf( szStatement, sizeof(szStatement), 
		"select count(*) from mail where  recvplayeruid = %d and bopen = 0 ", mplayerUID );

	CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
	if(pResult == NULL)
	{
		D_ERROR("执行sql语句:%s错误\n", szStatement);
		return;
	}
	else
	{
		if ( pResult->RowNum() > 0 && pResult->Next() )
		{
			unReadCount = pResult->GetInt(0);
		}
		pQuery->FreeResult( pResult );
	}

	pConnect->DestroyQuery( pQuery );
	

	TRY_END;
}

void CQueryPlayerHaveUnReadMailTask::OnResult()
{
	TRY_BEGIN;


	if ( unReadCount > 0)
	{
		FGUpdatePlayerUnReadMailCount srvmsg;
		srvmsg.unreadCount = unReadCount;
		srvmsg.playerID  = mPlayerID;


		MsgToPutContent* pContent = NEW MsgToPutContent;
		pContent->msgType = FGUpdatePlayerUnReadMailCount::wCmd;
		pContent->sessionID = GetSessionID();
		StructMemCpy( pContent->szContent , &srvmsg, sizeof(srvmsg) );
		GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );
	}



	TRY_END;
}

