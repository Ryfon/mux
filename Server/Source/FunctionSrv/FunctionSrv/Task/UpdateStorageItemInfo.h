﻿/********************************************************************
	created:	2009/09/21
	created:	21:9:2009   14:09
	file:		UpdateStorageItemInfo.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef UPDATE_STORAGEITEM_INF0_H_
#define UPDATE_STORAGEITEM_INF0_H_

#include "ITask.h"

class UpdateStorageItemInfoTask:public ITask
{
public:
	UpdateStorageItemInfoTask( ItemInfo_i* newItemInfoArr ,unsigned int storageID ):ITask(NULL),updateStorageID( storageID ),isOk(false)
	{
		if ( newItemInfoArr )
		{
			StructMemCpy( updateStorageItemArr, newItemInfoArr, sizeof(updateStorageItemArr) );
			isOk  = true;
		}
	}

	
	~UpdateStorageItemInfoTask()
	{
	}

public:

	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
	ItemInfo_i updateStorageItemArr[MAX_STORAGE_ITEMCOUNT];
	unsigned int updateStorageID;
	bool isOk;
};


#endif