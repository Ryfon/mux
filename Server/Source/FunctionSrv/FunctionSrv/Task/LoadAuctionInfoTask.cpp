﻿#include "LoadAuctionInfoTask.h"

#include "../AuctionManager.h"

void LoadAuctionInfoTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	stringstream ss;
	ss<<"select id, iteminfo,fixprice,curauctionprice,originalprice,expiretime from auction";

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery != NULL)
	{
		CResult* pResult = pQuery->ExecuteSelect( ss.str().c_str(),(unsigned long)ss.str().size() );
		if ( pResult && pResult->RowNum()> 0 )
		{
			while ( pResult->Next() )
			{
				const char* pItemInfoArr = pResult->GetString(1);
				if ( !pItemInfoArr )
					continue;

				AuctionItemCSInfo tmpCSInfo;
				tmpCSInfo.auctionID = pResult->GetInt(0);
				StructMemCpy( tmpCSInfo.auctionItem, pItemInfoArr, sizeof(ItemInfo_i));
				tmpCSInfo.fixprice = pResult->GetInt(2);
				tmpCSInfo.curauctionprice = pResult->GetInt(3);
				tmpCSInfo.origiprice = pResult->GetInt(4);
				tmpCSInfo.expiretime = pResult->GetInt(5);

				AuctionManagerSingle::instance()->InsertNewAuctionItem( tmpCSInfo );
			}
			pQuery->FreeResult( pResult);
		}

		pConnect->DestroyQuery( pQuery );
	}
}


void LoadAuctionInfoTask::OnResult()
{

}