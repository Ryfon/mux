﻿/********************************************************************
	created:	2009/05/06
	created:	6:5:2009   17:29
	file:		QueryPlayerHaveUnReadMailTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef QUERY_PLAYER_HAVE_UNREAD_MAILTASK_H
#define QUERY_PLAYER_HAVE_UNREAD_MAILTASK_H

#include "ITask.h"

class CQueryPlayerHaveUnReadMailTask:public ITask
{
public:
	CQueryPlayerHaveUnReadMailTask( CGateServer* pServer ,const char* szPlayerName, unsigned int playerUID ,const PlayerID&  playerID ):ITask(pServer),mplayerUID(playerUID)
	{
		unReadCount = 0;
		isInit = false;
		
		if ( szPlayerName )
		{
			ACE_OS::strncpy( mszPlayerName, szPlayerName, MAX_NAME_SIZE );
			mPlayerID = playerID;
			isInit = true;
		}
	}

	~CQueryPlayerHaveUnReadMailTask(){}


public:
	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
	unsigned int mplayerUID;
	char mszPlayerName[MAX_NAME_SIZE];
	PlayerID mPlayerID;
	unsigned int unReadCount;
	bool isInit;//是否加载完毕
};

#endif

