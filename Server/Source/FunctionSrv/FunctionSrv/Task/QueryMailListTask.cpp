﻿#include "QueryMailListTask.h"

#include "../SuspectMailManager.h"
//#include "../RankManager.h"

void QueryMailListTask::OnThread(CConnection* pConnect )
{
	//FunctionTrace( "QueryMailListTask::OnThread"/*__FUNCTION__*/ );

	if ( !pConnect )
	{
		D_DEBUG("QueryMailListTask::OnThread pConnect == NULL\n");
		return;
	}

	if ( !mpPlayerMail )
	{
		D_DEBUG("QueryMailListTask::OnThread mpPlayerMail == NULL\n");
		return;
	}

	TRY_BEGIN;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
	{
		D_DEBUG( "QueryMailListTask::OnThread pQuery == NULL\n" );
		return;
	}

	char szStatement[1024]= {0};
	int  iStatementLen = 0;

	iStatementLen = ACE_OS::snprintf( szStatement, sizeof(szStatement), 
		"select mailuid,type,bopen,sendplayeruid,recvplayeruid,sendplayername,expiretime,title,content,attachinfo from mail where  recvplayeruid = %d order by mailuid desc", mpPlayerMail->GetSessionOwnerID() );

	CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
	if(pResult == NULL)
	{
		D_ERROR("执行sql语句:%s错误\n", szStatement);
		return;
	}
	else
	{
		mIsInit = true;
		//本次影响的行数
		unsigned int affectNum = pResult->RowNum();
		if ( affectNum > 0 )
		{
			while( pResult->Next() )
			{
				unsigned  mailuid    = (unsigned)pResult->GetInt(0); //mailuid
				MAIL_TYPE mailType   = (MAIL_TYPE)pResult->GetInt(1);//mailtype
				bool      mailopen   = pResult->GetInt(2)?true:false;//bopen
				unsigned  sendplayer = (unsigned)pResult->GetInt(3);//sendplayeruid
				unsigned  recvplayer = (unsigned)pResult->GetInt(4);//recvplayeruid
				char*     sendname   = pResult->GetString(5);//sendplayername
				unsigned  expiretime = (unsigned)pResult->GetInt(6);//expiretime			
				char*     mailtitle  = pResult->GetString(7);//title
				char*     mailcontent= pResult->GetString(8);//content

				//只加载在生命期的邮件
				if ( mTimeNow < expiretime )
				{
					if( mailType == E_SYSYTEM_MAIL )
					{
						//系统邮件可能包含附件
						MailAttachInfo* pAttachinfo = (MailAttachInfo *)pResult->GetString(9);//attachinfo
						if ( pAttachinfo )
							mpPlayerMail->LoadSystemMail( mailuid, mailopen,recvplayer, sendplayer, sendname,expiretime, mailtitle, mailcontent, pAttachinfo->mMoney, pAttachinfo->mAttachItem );
					}
					else if ( mailType == E_NORMAL_MAIL )
					{
						mpPlayerMail->LoadNormalMail( mailuid, mailopen, recvplayer, sendplayer, sendname, expiretime, mailtitle, mailcontent );
					}
				}
			}
		}
		pQuery->FreeResult(pResult);

		//加载邮件成功
		mpPlayerMail->SetLoadMailOK();

		D_DEBUG("%d 加载邮件成功,邮件个数:%d \n", mpPlayerMail->GetSessionOwnerID(), affectNum );
	}
	pConnect->DestroyQuery(pQuery);
	TRY_END;
}

void QueryMailListTask::OnResult()
{
	//FunctionTrace( "QueryMailListTask::OnResult"/*__FUNCTION__*/ );

	if ( !mpPlayerMail )
	{
		D_DEBUG("QueryMailListTask::OnResult mpPlayerMail == NULL\n");
		return;
	}

	if ( !mIsInit )
		return;

	const std::vector<CSystemMail *>& systemMailVec = mpPlayerMail->GetSystemMailVec();
	//if ( systemMailVec.size() > 0 )
	{		
		FGQueryMailListInfoByPageRst queryRst1,queryRst2;//前半页和后半页的信息
		unsigned int query1len = 0 ,query2len = 0; 
		StructMemSet( queryRst1, 0x0 ,sizeof(FGQueryMailListInfoByPageRst) );
		StructMemSet( queryRst2, 0x0 ,sizeof(FGQueryMailListInfoByPageRst) );
		queryRst1.mailOwnerUID = queryRst2.mailOwnerUID = mpPlayerMail->GetSessionOwnerID();
		queryRst1.mailType = queryRst2.mailType = E_SYSYTEM_MAIL;

		unsigned int mailCount = (unsigned )systemMailVec.size();
		unsigned int mailSysBase = 0;
		unsigned int mailValidCount = mailCount;
		if ( mailCount > MAX_SHOW_SYSTEMMAIL_COUNT  )
		{
			mailSysBase    = ( mailCount - MAX_SHOW_SYSTEMMAIL_COUNT );
			mailValidCount = MAX_SHOW_SYSTEMMAIL_COUNT;
		}

		unsigned int maxpage = 0;
		if ( mailValidCount && ( mailValidCount % MAX_MAILPAGE_COUNT == 0 ) )
		{ 
			maxpage = mailValidCount / MAX_MAILPAGE_COUNT;
		}
		else
		{
			maxpage = ( mailValidCount / MAX_MAILPAGE_COUNT ) + 1;
		}

		bool isPageEnd = ( ( maxpage - 1 ) == 0 );

		unsigned int mailbegin = mailSysBase;
		unsigned int mailend   = mailbegin + MAX_MAILPAGE_COUNT;
		if ( mailend > mailCount )
			mailend = mailCount;
		
		for ( unsigned int mailindex = mailbegin; mailindex <  mailend ; mailindex++ )
		{
			CSystemMail* pSysmail = systemMailVec[mailindex];
			if ( pSysmail )
			{
				unsigned int index = mailindex - mailbegin;
				if ( index < MAX_MAIL_CONCISE_COUNT )//邮件的前半页
				{
					queryRst1.mailConciseInfo[index].mailuid    = pSysmail->GetMailUID();
					queryRst1.mailConciseInfo[index].expireTime = pSysmail->GetExpireTime() - mTimeNow;
					queryRst1.mailConciseInfo[index].isRead     = pSysmail->IsMailBeOpen();
					queryRst1.mailConciseInfo[index].titleLen   = ACE_OS::snprintf( queryRst1.mailConciseInfo[index].mailTitle,
						MAX_MAIL_CONCISE_TITLE_LEN - 1,
						"%s",
						pSysmail->GetMailTitle()
						);

					queryRst1.mailConciseInfo[index].nameLen    = ACE_OS::snprintf( queryRst1.mailConciseInfo[index].sendPlayerName,
						MAX_NAME_SIZE - 1,
						"%s",
						pSysmail->GetSendPlayerName() 
						);
					query1len++;
				}
				else if( index < MAX_MAILPAGE_COUNT )//邮件的后半页
				{

					unsigned int tmpIndex = index - MAX_MAIL_CONCISE_COUNT;
					queryRst2.mailConciseInfo[tmpIndex].mailuid    = pSysmail->GetMailUID();
					queryRst2.mailConciseInfo[tmpIndex].expireTime = pSysmail->GetExpireTime() - mTimeNow;
					queryRst2.mailConciseInfo[tmpIndex].isRead     = pSysmail->IsMailBeOpen();
					queryRst2.mailConciseInfo[tmpIndex].titleLen = ACE_OS::snprintf( queryRst2.mailConciseInfo[tmpIndex].mailTitle,
						MAX_MAIL_CONCISE_TITLE_LEN - 1,
						"%s",
						pSysmail->GetMailTitle()
						);

					queryRst2.mailConciseInfo[tmpIndex].nameLen  = ACE_OS::snprintf( queryRst2.mailConciseInfo[tmpIndex].sendPlayerName,
						MAX_NAME_SIZE - 1,
						"%s",
						pSysmail->GetSendPlayerName() 
						);

					query2len++;
				}
			}
		}

		//将前半页发送出去
		queryRst1.pageEnd    |= ( isPageEnd ?0x2:0x0 );
		queryRst1.queryPlayer = mQueryPlayerID;
		queryRst1.pageIndex   = 0;
		queryRst1.mailArrCount = query1len;
		//MsgToPut *pMsgToPut1 = CreateSrvPkg(FGQueryMailListInfoByPageRst,mp_gateSrv,queryRst1 );
		//mp_gateSrv->SendPkgToSrv(pMsgToPut1);
		MsgToPutContent* pContent1 = NEW MsgToPutContent;
		pContent1->msgType = FGQueryMailListInfoByPageRst::wCmd;
		pContent1->sessionID = GetSessionID();
		StructMemCpy( pContent1->szContent , &queryRst1, sizeof(queryRst1) );
		GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent1 );
		D_DEBUG("线程查询完毕，发送邮件列表给玩家%d\n", mpPlayerMail->GetSessionOwnerID() );

		//如果有后半页信息,则将后半页页发送出去
		if ( query2len > 0 )
		{
			queryRst2.pageEnd |= ( isPageEnd ?0x2:0x0 );
			queryRst2.pageEnd |= 0x1;
			queryRst2.queryPlayer = mQueryPlayerID;
			queryRst2.pageIndex = 0;
			queryRst2.mailArrCount = query2len;
			//MsgToPut *pMsgToPut2 = CreateSrvPkg(FGQueryMailListInfoByPageRst,mp_gateSrv,queryRst2 );
			//mp_gateSrv->SendPkgToSrv(pMsgToPut2);

			MsgToPutContent* pContent2 = NEW MsgToPutContent;
			pContent2->msgType = FGQueryMailListInfoByPageRst::wCmd;
			pContent2->sessionID = GetSessionID();
			StructMemCpy( pContent2->szContent , &queryRst2, sizeof(queryRst2) );
			GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent2 );
		}
	}
}


