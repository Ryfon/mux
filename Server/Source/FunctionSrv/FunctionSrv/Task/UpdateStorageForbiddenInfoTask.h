﻿/********************************************************************
	created:	2009/09/21
	created:	21:9:2009   16:05
	file:		UpdateStorageForbiddenInfoTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef UPDATE_STORAGE_FORBIDDENINFO_TASK_H
#define UPDATE_STORAGE_FORBIDDENINFO_TASK_H

#include "ITask.h"

class UpdateStorageForbiddenInfoTask:public ITask
{
public:

	UpdateStorageForbiddenInfoTask( unsigned int storageID, unsigned int inputErrNum, unsigned int forbiddenTime ):ITask(NULL),mforbiddenTime(forbiddenTime),
		minputErrCount(inputErrNum),mstorageUpdateID( storageID )
	{

	}

	~UpdateStorageForbiddenInfoTask()
	{

	}


public:

	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
	unsigned int mforbiddenTime;
	unsigned int minputErrCount;
	unsigned int mstorageUpdateID;
};


#endif