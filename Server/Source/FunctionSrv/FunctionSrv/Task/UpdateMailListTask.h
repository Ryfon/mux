﻿/********************************************************************
	created:	2009/04/23
	created:	23:4:2009   13:46
	file:		UpdateMailListTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef UPDATE_MAILLIST_TASK_H
#define UPDATE_MAILLIST_TASK_H


#include "ITask.h"
#include "../MailBase.h"

class UpdateMailListTask:public ITask
{
public:
	UpdateMailListTask( CGateServer* pGateSrv , CNormalMail* normalMail ):ITask( pGateSrv ),isInit(false)
	{
		if ( normalMail )
		{
			mDetailInfo = normalMail->GetMailDetialInfo();

			if ( mDetailInfo.mMailType == E_SYSYTEM_MAIL )
			{
				CSystemMail* systemmail = ( CSystemMail *)normalMail;
				if ( systemmail )
					mAttachinfo = systemmail->GetMailAttachInfo();
			}

			isInit = true;
		}
	}

	~UpdateMailListTask()
	{
	}

public:
	
	virtual void OnThread(CConnection* pConnect );


	virtual void OnResult();

private:
	MailDetailInfo mDetailInfo;
	MailAttachInfo mAttachinfo;
	bool isInit;

};

#endif

