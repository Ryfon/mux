﻿/********************************************************************
	created:	2009/09/21
	created:	21:9:2009   15:49
	file:		UpdateStorageSafeInfoTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef UPDATE_STROAGE_SAFE_INF0_TASK_H
#define UPDATE_STROAGE_SAFE_INF0_TASK_H

#include "ITask.h"

class UpdateStorageSafeInfoTask:public ITask
{
public:
	UpdateStorageSafeInfoTask( unsigned int StorageID , unsigned int validRow, STORAGE_SECURITY_STRATEGY securityMode ,const char* psd ):ITask(NULL ), mupdateStorageID(StorageID),
		updatevalidRow( validRow ),updateSecurityMode( securityMode )
	{
		StructMemSet( mnewPsdArr, '\0',  sizeof(mnewPsdArr) );
		ACE_OS::strncpy( mnewPsdArr , psd, MAX_STORAGE_PASSWORD_SIZE );
	}

	~UpdateStorageSafeInfoTask()
	{}

public:

	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:

	unsigned int mupdateStorageID;
	unsigned int updatevalidRow;
	STORAGE_SECURITY_STRATEGY updateSecurityMode;
	char  mnewPsdArr[MAX_STORAGE_PASSWORD_SIZE];
};


#endif