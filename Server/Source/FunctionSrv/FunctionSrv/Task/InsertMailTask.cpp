﻿#include "InsertMailTask.h"
#include "../MailBase.h"

void InsertMailTask::OnThread( CConnection* pConnect)
{
	if ( !pConnect )
		return;

	if ( !isInit)
		return;

	TRY_BEGIN;

	int iRet = 0;
	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		iRet = -1;

	if ( iRet != -1 )
	{
		std::stringstream ss;
		//如果是普通邮件
		if ( mDetailInfo.mMailType == E_NORMAL_MAIL )
		{
			unsigned int mailCount = 0;

			//检测玩家所拥有的普通邮件的个数
			std::stringstream checknormalss;
			checknormalss<<"select count(*) from mail where type = 0 and recvplayeruid = " << mDetailInfo.mRecvPlayerUID;
			std::string checknormalstr = checknormalss.str();
			CResult* pCheckResult =  pQuery->ExecuteSelect( checknormalstr.c_str(),(unsigned long)( checknormalstr.size() ) );
			if ( pCheckResult && pCheckResult->Next() )
			{
				mailCount = pCheckResult->GetInt(0);
				pQuery->FreeResult( pCheckResult );
			}

			//最大的普通邮件的个数//超过这个限制，则通知对应的client 玩家信箱已经满了
			if ( mailCount >= MAX_NORMALMAIL_COUNT  )
			{
				FGSendMailFail sendMailFail;
				sendMailFail.sendPlayerID = mPlayerID;
				sendMailFail.errNo = E_PLAYER_MAIL_FULL;

				MsgToPutContent* pContent = NEW MsgToPutContent;
				pContent->msgType   = FGSendMailFail::wCmd;
				pContent->sessionID = GetSessionID();
				StructMemCpy( pContent->szContent ,&sendMailFail, sizeof(sendMailFail) );
				GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );
				return;
			}

			ss << " insert into mail ( mailuid,type,bopen, sendplayeruid , recvplayeruid , sendplayername , expiretime ,title, content ) values( "
				<< mDetailInfo.mUID<<", "
				<< mDetailInfo.mMailType <<", "
				<< mDetailInfo.mIsOpen <<", "
				<< mDetailInfo.mSendPlayerUID <<", "
				<< mDetailInfo.mRecvPlayerUID <<", "
				<< "'" << mDetailInfo.mSendPlayer<<"', "
				<< mDetailInfo.mexpireTime <<", "
				<< "'" << mDetailInfo.mTitle<<"', "
				<< "'" << mDetailInfo.mContent << "' )";

		}//如果是系统邮件
		else if( mDetailInfo.mMailType == E_SYSYTEM_MAIL )
		{
			char escapeAttachData[2 * sizeof(mAttachinfo)  + 1] = {0};
			pQuery->RealEscape(escapeAttachData, (const char *)&(mAttachinfo), (unsigned long)sizeof(mAttachinfo) ); // 转义

			ss << " insert into mail ( mailuid,type,bopen, sendplayeruid , recvplayeruid , sendplayername , expiretime ,title, content , attachinfo ) values( "
				<< mDetailInfo.mUID<<", "
				<< mDetailInfo.mMailType <<", "
				<< mDetailInfo.mIsOpen <<", "
				<< mDetailInfo.mSendPlayerUID <<", "
				<< mDetailInfo.mRecvPlayerUID <<", "
				<< "'" << mDetailInfo.mSendPlayer<<"', "
				<< mDetailInfo.mexpireTime <<", "
				<< "'" << mDetailInfo.mTitle<<"', "
				<< "'" << mDetailInfo.mContent << "',"
				<< "'" << escapeAttachData << "') ";
		}
		else
		{
			return;
		}

		std::string statement = ss.str();
		// 执行查询
		if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
			iRet = -1;
		else
			isOK = true;

		// 销毁查询
		pConnect->DestroyQuery(pQuery);
	}

	TRY_END;
}

void InsertMailTask::OnResult()
{
	TRY_BEGIN;

	if ( isOK )
	{
		FGNoticePlayerHaveUnReadMail srvmsg;
		ACE_OS::strncpy( srvmsg.recvPlayerName , mRecvMailPlayerName , MAX_NAME_SIZE );
		srvmsg.unreadCount = 1;
	
		MsgToPutContent* pContent = NEW MsgToPutContent;
		pContent->msgType = FGNoticePlayerHaveUnReadMail::wCmd;
		pContent->sessionID = GetSessionID();
		StructMemCpy( pContent->szContent ,&srvmsg, sizeof(srvmsg) );
		GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );
	}

	TRY_END;
}

