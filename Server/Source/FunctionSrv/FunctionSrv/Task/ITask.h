﻿/********************************************************************
	created:	2008/06/27
	created:	27:6:2008   15:12
	file:		ITask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/


#ifndef ITask_H_
#define ITask_H_

#include "../OtherServer.h"
#include "../../../Base/MysqlWrapper.h"
#include "../../../Base/PkgProc/SrvProtocol.h"
#include "../../../Base/PkgProc/MsgToPut.h"
#include "../../../Base/PkgProc/PacketBuild.h"
#include "../../../Base/PkgProc/DealPkgBase.h"
#include "../RecordHandler.h"
#include "../GateSrvManager.h"
using namespace MUX_PROTO;

//任务函数
class ITask
{
public:
	ITask( CGateServer* pGateSrv ):m_gateID(0)
	{
		if ( pGateSrv )
			m_gateID = pGateSrv->GetSessionID();
	};

	virtual ~ITask(){}

public:
	//线程处理过程
	virtual void OnThread( CConnection* pConnect ) = 0;

	//线程处理善后的过程
	virtual void OnResult() = 0;

	unsigned int GetSessionID() 
	{
		return m_gateID;
	}

protected:
	//CGateServer* mp_gateSrv;
	unsigned int m_gateID;
};


#endif

