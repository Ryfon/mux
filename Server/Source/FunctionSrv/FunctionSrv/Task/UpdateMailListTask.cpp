﻿#include "UpdateMailListTask.h"

void UpdateMailListTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	if ( !isInit)
		return;

	TRY_BEGIN;

	int iRet = 0;
	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		iRet = -1;

	if ( iRet != -1 )
	{
		std::stringstream ss;
		//如果是普通邮件
		if ( mDetailInfo.mMailType == E_NORMAL_MAIL )
		{
			ss << " update mail set "
			   << " type = "<< mDetailInfo.mMailType << ","
			   << " bopen= "<< mDetailInfo.mIsOpen << ","
			   << " expiretime = " << mDetailInfo.mexpireTime 
			   << " where mailuid = "<<mDetailInfo.mUID;
			
		}//如果是系统邮件
		else if( mDetailInfo.mMailType == E_SYSYTEM_MAIL )
		{
			char escapeAttachData[2 * sizeof(mAttachinfo)  + 1] = {0};
			pQuery->RealEscape(escapeAttachData, (const char *)&(mAttachinfo), (unsigned long)sizeof(mAttachinfo) ); // 转义

			ss  << " update mail set "
				<< " type = "<< mDetailInfo.mMailType << ","
				<< " bopen= "<< mDetailInfo.mIsOpen << ","
				<< " expiretime = " << mDetailInfo.mexpireTime << ","
				<< " attachinfo='" << escapeAttachData
				<< " ' where mailuid = "<<mDetailInfo.mUID;
		}
		else//如果类型错误,则不更新数据库
		{
			return;
		}

		std::string statement = ss.str();
		// 执行查询
		if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
			iRet = -1;

		// 销毁查询
		pConnect->DestroyQuery(pQuery);
	}

	TRY_END;
}

void UpdateMailListTask::OnResult()
{

}

