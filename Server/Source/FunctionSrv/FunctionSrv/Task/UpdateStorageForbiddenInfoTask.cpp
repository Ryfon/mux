﻿#include "UpdateStorageForbiddenInfoTask.h"

void UpdateStorageForbiddenInfoTask::OnThread(CConnection *pConnect)
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	CQuery *pQuery = pConnect->CreateQuery();
	if( pQuery == NULL )
		return;

	stringstream ss;
	ss << " update storage set lastForbiddenTime = " << mforbiddenTime <<" ,"
		<< " inputErrCount =  " << minputErrCount 
		<< " where id = " << mstorageUpdateID;

	std::string statement = ss.str();
	if( pQuery->ExecuteUpdate(  statement.c_str(), (unsigned long)statement.size() ) < 0 )
	{
		D_ERROR("执行SQL语句:%s 失败\n", statement.c_str() );
	}

	pConnect->DestroyQuery( pQuery );


	TRY_END;
}

void UpdateStorageForbiddenInfoTask::OnResult()
{

}