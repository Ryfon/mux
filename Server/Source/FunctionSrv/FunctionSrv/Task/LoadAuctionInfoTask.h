﻿/********************************************************************
	created:	2010/04/06
	created:	6:4:2010   17:20
	file:		LoadAuctionInfoTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef LOAD_AUCTIONINFO_TASK_H
#define LOAD_AUCTIONINFO_TASK_H

#include "ITask.h"

class LoadAuctionInfoTask:public ITask
{
public:
	LoadAuctionInfoTask( CGateServer* pGateSrv ):ITask( pGateSrv )
	{
	}

	virtual ~LoadAuctionInfoTask()
	{
	}

public:
	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
};

#endif