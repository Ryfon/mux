﻿#include "TaskPool.h"
#include "ITask.h"
#include "../../../Base/Utility.h"

//LSOCKET_QUEUE* g_MsgQueue[CTaskPool::default_pool_size] =  { NULL };
//unsigned int   g_threadIndex = 0;

void CTaskPool::open(int pool_size /* = default_pool_size  */)
{
	this->activate(THR_NEW_LWP|THR_JOINABLE|THR_INHERIT_SCHED,pool_size );
}

int CTaskPool::CloseThread(u_long flags )
{
	ACE_UNUSED_ARG(flags);

	int counter = active_threads.value();

	D_DEBUG("准备关闭连接线程，现在连接的线程数目为:%d\n",counter );

	while ( counter-- )
	{
		ACE_Message_Block* mb = NULL;

		ACE_NEW_RETURN(mb,
			ACE_Message_Block(sizeof(int),ACE_Message_Block::MB_STOP),
			-1);
	
		if( this->putq(mb) == -1 )
			mb->release();
	}
	
	while( true  )
	{
		if (  this->wait() == 0 )
		{
			D_DEBUG("线程池回收完毕\n");
			break;
		}
	}

	return 0;
}

//每个线程的执行函数
int CTaskPool::svc()
{
	ACE_Message_Block* mb = NULL;

	Counter_Guard  counter_guard( active_threads );

	//每个线程建立了个连接
	CConnection* pConnect = NEW CConnection();
	if( pConnect->Open( szConnectstr ) == -1 )
		return -1;

	D_DEBUG("连接成功,连接线程开始运行:%d\n",ACE_Thread::self());

	/*unsigned int threadIndex = 0;
	m_threadMutex.acquire();
	threadIndex = g_threadIndex;
	g_MsgQueue[threadIndex] = NEW LSOCKET_QUEUE;
	g_threadIndex++;
	m_threadMutex.release();*/

	while ( this->getq(mb) != -1 )
	{
		Message_Block_Guard message_block_guard(mb);

		if( mb->msg_type() == ACE_Message_Block::MB_STOP )
			break;

		//获取执行任务
		char* c_data = mb->base();
		if ( c_data )
		{
			ITask* pTask = (ITask *)c_data;
			if ( pTask )
			{
				//任务主执行,用于执行数据库查询
				pTask->OnThread(pConnect);

				//任务执行后的收尾工作,如:反馈查询信息给玩家等
				pTask->OnResult();

				delete pTask;
				pTask = NULL;
			}
		}
	}

	//线程退出时，结束连接
	delete pConnect;

	D_DEBUG("执行线程结束,线程编号:%d\n",  ACE_Thread::self() );	
	return 0;
}

bool CTaskPool::Handle_message( CConnection* connect, ACE_Message_Block*& mb )
{
	if ( NULL == mb )
		return false;

	if ( NULL == connect )
		return false;

	char* c_data = mb->base();
	if( c_data )
	{
		ITask* pTask =(ITask *)c_data;
		ACE_UNUSED_ARG( pTask );
	}
	return true;
}

