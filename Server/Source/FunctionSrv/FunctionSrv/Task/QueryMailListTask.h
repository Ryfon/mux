﻿/********************************************************************
	created:	2009/04/30
	created:	30:4:2009   9:24
	file:		QueryMailListTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef QUERY_MAILLIST_TASK_H
#define QUERY_MAILLIST_TASK_H


#include "ITask.h"
#include "../PlayerMail.h"

class QueryMailListTask:public ITask
{
public:
	QueryMailListTask( CGateServer* pGateSrv ,CPlayerMailSession* pPlayerMail , const PlayerID& playerID, MAIL_TYPE mailType ,unsigned int queryPage ):
	  ITask( pGateSrv ),mpPlayerMail( pPlayerMail ),mQueryPage(queryPage),mQueryMailType( mailType ),mIsInit(false)
	{
		mQueryPlayerID =  playerID;
		mTimeNow = (unsigned)time(NULL);
	}

	virtual ~QueryMailListTask()
	{
	}

public:
	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
	CPlayerMailSession* mpPlayerMail;
	unsigned int  mQueryPage;
	PlayerID      mQueryPlayerID;
	MAIL_TYPE     mQueryMailType;
	unsigned int  mTimeNow;
	bool          mIsInit;
};

#endif

