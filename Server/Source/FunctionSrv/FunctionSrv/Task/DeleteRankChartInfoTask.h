﻿/********************************************************************
	created:	2009/03/18
	created:	18:3:2009   10:56
	file:		DeleteRankChartInfo.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef DELETE_RANKCHART_INFO_H
#define DELETE_RANKCHART_INFO_H

#include "ITask.h"


class DeleteRankChartInfoTask:public ITask
{
public:
	DeleteRankChartInfoTask( unsigned int rankID ):ITask(NULL),mRankID(rankID)
	{
	}

	~DeleteRankChartInfoTask()
	{

	}

public:
	virtual void OnThread(CConnection* pConnect);

	virtual void OnResult();

private:
	unsigned int mRankID;

};



#endif



