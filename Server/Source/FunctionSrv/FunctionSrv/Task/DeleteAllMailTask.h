﻿/********************************************************************
	created:	2009/05/13
	created:	13:5:2009   14:29
	file:		DeleteAllMailTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef DELETE_ALLMAIL_TASK_H
#define DELETE_ALLMAIL_TASK_H


#include "ITask.h"

class DeleteAllMailTask:public ITask
{
public:
	DeleteAllMailTask( unsigned int playeruid ):ITask(NULL),mPlayeruid(playeruid)
	{
	}

	~DeleteAllMailTask()
	{
	}

public:

	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
	unsigned int mPlayeruid;
};


#endif

