﻿/********************************************************************
	created:	2008/06/27
	created:	27:6:2008   14:38
	file:		TaskThread.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef  TASK_POOL_H
#define  TASK_POOL_H

#include "../../../Base/aceall.h"

#include "../../../Base/DBConfigManager.h"
#include "../GateSrvManager.h"
//#include "../../../Test/testthreadque_wait/cirqueue.h"

//typedef CirQueue< MsgToPutContent, 1024/*每socket队列最大可缓存消息数*/, 10/*发送通知阈值*/ >  LSOCKET_QUEUE;


class ITask;//任务接口

//任务池
class CTaskPool:public ACE_Task<ACE_MT_SYNCH>
{
	friend class ACE_Singleton<CTaskPool, ACE_Null_Mutex>;

public:
	typedef ACE_Task<ACE_MT_SYNCH> Inherited;

	typedef ACE_Atomic_Op<ACE_Mutex,int> ThreadCounter_t;

	enum size_t
	{
		default_pool_size=5,
	};

public:
	CTaskPool ():active_threads(0),szConnectstr(NULL)
	{}

	virtual ~CTaskPool()
	{
		CloseThread(0);
	}

	void AttachDBConnecting( stConnectionString* pszConnectionString )
	{
		szConnectstr = pszConnectionString;
	}

	//开启线程
	void open( int pool_size = default_pool_size );

	virtual int open(void *args /* = 0 */)
	{
		return Inherited::open(args);
	}

	//重写关闭函数,用于关闭对应的线程
	int CloseThread(u_long flags /* = 0 */);

	//获取线程ID号
	ACE_thread_t thread_id(void){ return m_threadID; }

	//放入任务
	int PushTask( ITask* pTask )
	{
		if ( !pTask )
			return -1;

		char* c_data = (char *)pTask;

		ACE_Message_Block *mb;

		ACE_NEW_RETURN(mb,
			ACE_Message_Block(c_data),
			-1);

		//插入数据
		if ( this->putq(mb) == -1 )
		{
			mb->release();
			return -1;
		}
		return 0;
	}


protected:
	//重载svc,每个线程的处理过程
	virtual int svc(void);

	//真正的处理消息的流程
	bool Handle_message( CConnection*  connect, ACE_Message_Block*& mb );

protected:
	//线程编号
	ACE_thread_t m_threadID;

	//当前正在执行的线程个数
	ThreadCounter_t active_threads;

	//每个线程建立连接的字符串
	stConnectionString* szConnectstr;

	//ACE_Thread_Mutex m_threadMutex;
};

//任务池
typedef ACE_Singleton<CTaskPool, ACE_Null_Mutex> TaskPoolSingle;


class Message_Block_Guard
{
public:
	Message_Block_Guard( ACE_Message_Block*& mb ):
	  mb_(mb)
	  {
	  }

	  ~Message_Block_Guard()
	  {
		  mb_->release();
	  }

protected:
	ACE_Message_Block *&mb_;
};

class Counter_Guard
{
public:
	Counter_Guard( CTaskPool::ThreadCounter_t& counter ):counter_(counter)
	{
		++counter;
	}

	~Counter_Guard()
	{
		--counter_;
	}

protected:
	CTaskPool::ThreadCounter_t &counter_;
};

#endif

