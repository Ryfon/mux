﻿#include "DeleteMailTask.h"
//#include "../RankManager.h"

void DeleteMailTask::OnThread(CConnection* pConnect)
{
	//FunctionTrace( "DeleteMailTask::OnThread"/*__FUNCTION__*/ );

	if ( !pConnect )
		return;

	TRY_BEGIN;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery != NULL)
	{
		// 构造SQL
		char szStatement[1024];
		int  iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
			"delete from mail where mailuid = %d and recvplayeruid = %d ",
			mMailuid,
			mPlayeruid
			);

		//执行删除表中有关这个排行的信息
		int iRet = pQuery->ExecuteUpdate((const char*)szStatement, iStatementLen);
		if( iRet == -1 )
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			return;
		}

		pConnect->DestroyQuery( pQuery );
	}

	TRY_END;
}


void DeleteMailTask::OnResult()
{
}

