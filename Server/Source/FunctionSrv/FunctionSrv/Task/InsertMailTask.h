﻿/********************************************************************
	created:	2009/04/23
	created:	23:4:2009   16:45
	file:		InsertMailTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef INSERT_MAIL_TASK_H
#define INSERT_MAIL_TASK_H

#include "ITask.h"
#include "../PlayerMail.h"

class InsertMailTask:public ITask
{
public:
	InsertMailTask( CGateServer* pGateSrv ,CNormalMail*& pMail, PlayerID& playerID, const char* pszRecvMailPlayerName , bool isExistSession ):ITask( pGateSrv ),isInit(false),isOK(false)
	{
		if ( pMail &&  pszRecvMailPlayerName )
		{
			mDetailInfo = pMail->GetMailDetialInfo();
			if (  pMail->GetMailType() == E_SYSYTEM_MAIL )//如果是系统邮件,还需要关心附件的信息
			{
				CSystemMail* pSystemMail = ( CSystemMail *)pMail;
				if ( pSystemMail )
					mAttachinfo = pSystemMail->GetMailAttachInfo();
			}

			mPlayerID = playerID;
			ACE_OS::strncpy( mRecvMailPlayerName ,pszRecvMailPlayerName ,MAX_NAME_SIZE );
			isInit = true;

			//使用完后,回收此块内存,因为玩家的session不存在,所以回收该内存,如过存在session,则该内存交给session管理,由session释放
			if ( !isExistSession )
				delete pMail;
		}
	}

	virtual ~InsertMailTask()
	{
	}

public:
	virtual void OnThread(CConnection* pConnect);

	virtual void OnResult();

private:
	MailDetailInfo mDetailInfo;
	MailAttachInfo mAttachinfo;
	char mRecvMailPlayerName[MAX_NAME_SIZE];
	PlayerID  mPlayerID;
	bool isInit;
	bool isOK;
	bool isPlayerOnLine;
	unsigned int sessionID;
};



#endif

