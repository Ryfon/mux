﻿#include "UpdateStorageItemInfo.h"

void UpdateStorageItemInfoTask::OnThread( CConnection* pConnect )
{
	if ( !pConnect )
		return;

	if ( !isOk )
		return;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		return;

	char escapeStorageItemData[ 2 * sizeof(updateStorageItemArr)  + 1] = {0};
	pQuery->RealEscape( escapeStorageItemData , (const char *)updateStorageItemArr , (unsigned long)sizeof(updateStorageItemArr) );

	stringstream ss;
	ss << " update storage set iteminfo = '" << escapeStorageItemData 
	   << " ' where id = " << updateStorageID ;

	std::string statement = ss.str();
	if( pQuery->ExecuteUpdate(  statement.c_str(), (unsigned long)statement.size() ) < 0 )
	{
		D_ERROR("执行SQL语句:%s 失败\n", statement.c_str() );
	}

	pConnect->DestroyQuery( pQuery );
}


void UpdateStorageItemInfoTask::OnResult()
{

}