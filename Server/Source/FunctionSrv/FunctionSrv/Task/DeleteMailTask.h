﻿/********************************************************************
	created:	2009/04/30
	created:	30:4:2009   11:19
	file:		DeleteMailTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef _DELETE_MAILTASK_H
#define _DELETE_MAILTASK_H

#include "ITask.h"


class DeleteMailTask:public ITask
{
public:
	DeleteMailTask( unsigned int playeruid ,unsigned int mailuid ):ITask(NULL),mMailuid(mailuid),mPlayeruid(playeruid)
	{
	}

	~DeleteMailTask()
	{
	}

public:

	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
	unsigned int mMailuid;
	unsigned int mPlayeruid;
};

#endif

