﻿#include "DealPkg.h"

#include "RecordHandler.h"
#include "OtherServer.h"
#include "../../Base/PkgProc/MsgToPut.h"
#include "../../Base/PkgProc/PacketBuild.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/Utility.h"
#include <vector>
using namespace MUX_PROTO;

//#include "RankManager.h"
#include "rankcache.h"
#include "SuspectMailManager.h"
#include "PlayerMail.h"
#include "storage.h"
#include "Task/QueryPlayerHaveUnReadMailTask.h"
#include "RaceMaster.h"
#include "PublicNoticeManager.h"
#include "AuctionManager.h"

#include "Task/TaskPool.h"

#define FUNC_PARAM_CHECK(T_MSG) \
	if ((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))\
    {\
		D_ERROR( "%s:%d, 输入参数空\n", __FILE__, __LINE__ );\
		return false;\
	}\
	if ( sizeof(T_MSG) != wPkgLen )\
	{\
		D_ERROR( "%s:%d, 消息大小不正确\n", __FILE__, __LINE__ );\
		return false;\
	}

CDealGateSrvPkg::CDealGateSrvPkg(void)
{
}

CDealGateSrvPkg::~CDealGateSrvPkg(void)
{
}

void CDealGateSrvPkg::Init(void)
{
	
	Register( G_F_RANKPLAYER_ONLINE, &OnRankPlayerOnLine );//处理排行榜上的玩家上线的消息
	Register( G_F_RANKPLAYER_OFFLINE, &OnRankPlayerOffLine );//处理排行榜的玩家下线的消息
	Register( G_F_PUNISHMENT_BEGIN, &OnPunishPlayerBegin );//处理天谴
	Register( G_F_QUERY_RANKCHART_INFO, &OnQueryRankChartInfoByPage );//查询排行榜的信息	
	Register( G_F_RANK_CACHE_INFO, &OnRankCacheInfo );//处理mapsrv发来的可能的新入榜信息；
	Register( G_F_OTHER_RANK, &OnOtherRank );//处理其它srv的排行榜通知(shopsrv生成的星钻排行榜,relationsrv生成的工会排行榜等)；	
	//Register( G_F_RANKCHART_UPDATE, &OnRankChartInfoUpdate );//更新排行榜的信息
	Register( G_F_LOAD_RANKINFO, &OnRecvLoadRankChartReq );//获取查询排行榜的请求
	//Register( G_F_REMOVE_RANKPLAYER_BYCHARTID, &OnRemoveRankPlayerByChartID );//删除排行榜的玩家
	Register( G_F_QUERY_RANKINFO_BYNAME ,&OnQueryRankInfoByPlayerName );//依据姓名查询排行榜玩家
	Register( G_F_SENDMAIL_TO_PLAER, &OnSendMailToPlayer );//发送邮件给玩家
	Register( G_F_RECVMAIL_PLAYRINFO_RST, &OnRecvMailPlayerInfoRst );//接受邮件玩家的查询信息返回
	Register( G_F_DELETE_MAIL, &OnDeleteMail );//删除邮件
	Register( G_F_QUERY_MAILLIST_BYPAGE, &OnQueryMailListByPageIndex );//查询邮件列表
	Register( G_F_QUERY_MAIL_DETIALINFO, &OnQueryMailDetialInfo );//查询邮件详细信息
	Register( G_F_PICKITEM_FROM_MAIL, &OnPickItemFromMail );//从邮件中拾取附件
	Register( G_F_PICKITEM_FROM_MAIL_ERROR, &OnPickItemFromMailError );//拾取附件错误
	Register( G_F_PICKMONEY_FROM_MAIL_ERROR, &OnPickMoneyFromMailError );//拾取附件金钱错误
	Register( G_F_SEND_SYSMAIL_TO_PLAYER, &OnSendSysMailToPlayer );//发送系统邮件
	Register( G_F_PICKMONEY_FROM_MAIL  ,&OnPickMoneyFromMail );
	Register( G_F_DELETE_ALLMAIL, &OnDeleteAllMail );
	//Register( G_F_UPDATE_RANKPLAYER_LEVEL, &OnRankPlayerLevelUp );
	Register( G_F_PLAYER_DELETE_ROLE, &OnPlayerDeleteRole );
	Register( G_F_UPDATE_STORAGE_SAFE_INFO, &OnPlayerUpdateStorageSafeInfo );
	Register( G_F_UPDATE_STORAGE_ITEM_INFO, &OnPlayerUpdateStorageItemInfo );
	Register( G_F_UPDATE_STORAGE_FORBIDDEN_INFO , &OnPlayerUpdateStorageForbiddenInfo );
	Register( G_F_QUERY_STORAGE_INFO , & OnPlayerQueryStorageInfo );
	Register( G_F_QUERY_RACEMASTER_INFO ,&OnMapSrvLoadRaceMasterInfo );
	Register( G_F_UPDATE_RACEMASTER_INFO, &OnUpdateRaceMasterInfo );
	Register( G_F_RESET_RACEMASTER_DECLAREWARINFO,&OnResetRaceMasterDeclareWarInfo );
	Register( G_F_NEW_RACEMASTER_BORN,&OnRaceMasterNewBorn );
	Register( G_F_LOAD_NEW_PUBLICNOTICE, &OnGMReLoadPublicNotice );
	Register( G_F_ADD_NEW_PUBLICNOTICE, &OnGMAddPublicNotice );
	Register( G_F_MODIFY_PUBLICNOTICE, &OnGMModifyPublicNotice );
	Register( G_F_DELETE_PUBLICNOTICE,&OnGMRemovePublicNotice );
	Register( G_F_QUERY_ALL_PUBLICNOTICE, &OnGMQueryAllPublicNotice );
	Register( G_F_SELECT_RACEMASTER ,&OnGMSelectRaceMaster );
	Register( G_F_PLAYER_QUERY_SELF_BID, &OnPlayerQuerySelfBid );
	Register( G_F_PLAERY_QUERY_SELF_AUCTION,&OnPlayerQuerySelfAuction );
	Register( G_F_PLAYER_CANCEL_AUCTION, &OnPlayerCancelAuction );
	Register( G_F_PLAYER_BID_ITEM_SECTION2 ,&OnPlayerBidItemSection2 );
	Register( G_F_PLAYER_BID_ITEM_SECTION1, &OnPlayerBidItemSection1 );
	Register( G_F_PLAYER_START_NEWAUCTION, &OnPlayerBidNewAuction );
	Register( G_F_PLAYER_QUERY_AUCTION, &OnPlayerQueryAuctionByCondition );
	return;
}



bool CDealGateSrvPkg::OnRankPlayerOnLine(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFRankPlayerOnLine);

	TRY_BEGIN;

	//更新排行榜玩家上线状态
	GFRankPlayerOnLine* pRankLine = (GFRankPlayerOnLine *)pPkg;
	//AllRaceRankChartManagerSingle::instance()->OnRecvUpdatePlayerOnLineState( pRankLine->rankPlayerUID/*, pRankLine->rankPlayerRace*/, true );
	CRankCacheManager::OnRecvUpdatePlayerOnLineState( pRankLine->rankPlayerUID/*, pRankLine->rankPlayerRace*/, true );

	//发送公告信息
	CPublicNoticeManagerSingle::instance()->SendPublicNoticeToPlayer( pOwner, pRankLine->playerID );

	//查询是否有未读邮件
	CQueryPlayerHaveUnReadMailTask* pTask = NEW CQueryPlayerHaveUnReadMailTask( pOwner, pRankLine->szPlayerName, pRankLine->rankPlayerUID,pRankLine->playerID );
	TaskPoolSingle::instance()->PushTask( pTask );

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnRankPlayerOffLine(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFRankPlayerOffLine);

	TRY_BEGIN;

	GFRankPlayerOffLine* pRankOffLine = ( GFRankPlayerOffLine *)pPkg;
	//AllRaceRankChartManagerSingle::instance()->OnRecvUpdatePlayerOnLineState( pRankOffLine->rankPlayerUID/*, pRankOffLine->rankPlayerRace*/, false );
	CRankCacheManager::OnRecvUpdatePlayerOnLineState( pRankOffLine->rankPlayerUID/*, pRankOffLine->rankPlayerRace*/, false );

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPunishPlayerBegin(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPunishmentBegin);

	TRY_BEGIN;

	GFPunishmentBegin* pPunishment = ( GFPunishmentBegin *)pPkg;

	ACE_UNUSED_ARG(pPunishment);
	
	D_DEBUG("天谴活动开始\n");

	//AllRaceRankChartManagerSingle::instance()->PunishMentBegin( pOwner );
	//CRankCacheManager::PunishMentBegin( pOwner );

	return true;
	TRY_END;
	return false;
}

//Register( G_F_OTHER_RANK, &OnOtherRank );//处理其它srv的排行榜通知(shopsrv生成的星钻排行榜,relationsrv生成的工会排行榜等)；	
bool CDealGateSrvPkg::OnOtherRank( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFOtherRank);

	TRY_BEGIN;

	GFOtherRank* pOtherRank = (GFOtherRank *) pPkg;
	CRankCacheManager::OnRcvOtherRank( pOtherRank );

	return true;
	TRY_END;
	return false;
}

//Register( G_F_RANK_CACHE_INFO, &OnRankCacheInfo );//处理mapsrv发来的可能的新入榜信息；
bool CDealGateSrvPkg::OnRankCacheInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFRankCacheInfo);

	TRY_BEGIN;

	GFRankCacheInfo* pRankCacheInfo = ( GFRankCacheInfo *) pPkg;
	CRankCacheManager::OnRcvNewCacheInfo( pRankCacheInfo );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnQueryRankChartInfoByPage(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryRankChartInfo);

	TRY_BEGIN;

	GFQueryRankChartInfo* pQueryInfo = ( GFQueryRankChartInfo *)pPkg;
	CRankCacheManager::OnQueryRankChartInfoByPageIndex( pOwner, pQueryInfo->queryPlayer, (ERankType)pQueryInfo->rankType );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnQueryRankInfoByPlayerName(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryRankPlayerByName);

	TRY_BEGIN;

	GFQueryRankPlayerByName* pQueryMsg = ( GFQueryRankPlayerByName *)pPkg;
	
	//AllRaceRankChartManagerSingle::instance()->OnQueryRankInfoByPlayerName( pOwner, pQueryMsg->queryPlayerID, pQueryMsg->rankChartID, pQueryMsg->queryName );
	CRankCacheManager::OnQueryRankInfoByPlayerName( pOwner, pQueryMsg->queryPlayerID, (ERankType)pQueryMsg->rankChartID, pQueryMsg->queryName );

	return true;
	TRY_END;	
	return false;

}

bool CDealGateSrvPkg::OnRecvLoadRankChartReq(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFLoadRankChartInfo);

	TRY_BEGIN;

	GFLoadRankChartInfo* pQueryInfo = ( GFLoadRankChartInfo* )pPkg;

	if ( NULL == pQueryInfo )
	{
		D_ERROR( "CDealGateSrvPkg::OnRecvLoadRankChartReq\n" );
		return false;
	}

	CRankCacheManager::OnMapSrvLoadRankChartInfo( pOwner, pQueryInfo->mapsrvID );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPlayerDeleteRole(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFOnPlayerDeleteRole);

	TRY_BEGIN;

	GFOnPlayerDeleteRole* pDelRole = ( GFOnPlayerDeleteRole *)pPkg;
	
	//AllRaceRankChartManagerSingle::instance()->OnPlayerDeleteRole( pDelRole->playerUID/*, pDelRole->race 种族去除, by dzj, 10.08.02*/);
	CRankCacheManager::OnPlayerDeleteRole( pDelRole->playerUID );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnSendMailToPlayer(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFSendMailToPlayer);
	
	TRY_BEGIN;

	GFSendMailToPlayer* pSendMail = ( GFSendMailToPlayer *)pPkg;
	SupectMailManagerSingle::instance()->CreateSuspectNormalMail( pOwner->GetSessionID(), pSendMail->sendPlayerID,  pSendMail->recvPlayerName, pSendMail->sendPlayerUID, pSendMail->sendPlayerName, pSendMail->sendMailTitle, 
		pSendMail->sendMailContent );

	return true;

	TRY_END;

	return false;
}


bool CDealGateSrvPkg::OnRecvMailPlayerInfoRst( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryRecvMailPlayerInfo);

	TRY_BEGIN;

	GFQueryRecvMailPlayerInfo* pRst = ( GFQueryRecvMailPlayerInfo *)pPkg;
	SupectMailManagerSingle::instance()->AffairMail( pRst->mailUID, pRst->recvPlayerUID , pRst->isExist );

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnDeleteMail(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFDeleteMail);

	TRY_BEGIN;

	GFDeleteMail* pDeleteMail = ( GFDeleteMail *)pPkg;
	PlayerMailSessionManagerSingle::instance()->DeleteMail( pOwner, pDeleteMail->deleteplayeruid,
		pDeleteMail->deletePlayerID, pDeleteMail->mailType, pDeleteMail->mailuid  );

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnQueryMailListByPageIndex(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryMailListByPage);

	TRY_BEGIN;

	GFQueryMailListByPage* pQueryMailpage = ( GFQueryMailListByPage *)pPkg;

	PlayerMailSessionManagerSingle::instance()->QueryMailListByPage( pOwner, pQueryMailpage->queryPlayerUID, pQueryMailpage->queryPlayerID,  
		pQueryMailpage->mailType, pQueryMailpage->pageIndex );

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnQueryMailDetialInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryMailDetialInfo);

	TRY_BEGIN;

	GFQueryMailDetialInfo* pQueryMailDetial = ( GFQueryMailDetialInfo *)pPkg;

	PlayerMailSessionManagerSingle::instance()->QueryMailDetialInfo(  pOwner, pQueryMailDetial->queryPlayerUID, 
		pQueryMailDetial->queryPlayerID,
		pQueryMailDetial->mailType,
		pQueryMailDetial->mailUID );


	return true;

	TRY_END;
	return false;
}


bool CDealGateSrvPkg::OnPickItemFromMail(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPickItemFromMail);

	TRY_BEGIN;

	GFPickItemFromMail* pPickAttach = ( GFPickItemFromMail *)pPkg;


	PlayerMailSessionManagerSingle::instance()->PickMailAttachInfo( pOwner, pPickAttach->pickPlayerUID, pPickAttach->pickPlayerID,
		pPickAttach->mailUID,
		pPickAttach->pickIndex 
		);

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPickMoneyFromMail(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPickMoneyFromMail);

	TRY_BEGIN;

	GFPickMoneyFromMail* pPickAttach = ( GFPickMoneyFromMail *)pPkg;


	PlayerMailSessionManagerSingle::instance()->PickMailMoney( pOwner, pPickAttach->pickPlayerUID,
		pPickAttach->pickPlayerID,
		pPickAttach->mailuid
		);

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPickItemFromMailError(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPickMailItemError);

	TRY_BEGIN;

	GFPickMailItemError* pPickError = (GFPickMailItemError * )pPkg;


	PlayerMailSessionManagerSingle::instance()->PickMailAttachItemError( pPickError->pickPlayerUID,
		pPickError->mailuid,
		pPickError->pickIndex,
		pPickError->itemInfo );

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPickMoneyFromMailError(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPickMailMoneyError);

	TRY_BEGIN;

	GFPickMailMoneyError* pPickError = ( GFPickMailMoneyError *)pPkg;

	PlayerMailSessionManagerSingle::instance()->PickMailAttachMoneyError( pPickError->pickPlayerUID,
		pPickError->mailuid,
		pPickError->money );

	return true;
	TRY_END;
	return false;
}


bool CDealGateSrvPkg::OnSendSysMailToPlayer(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFSendSysMailToPlayer);

	TRY_BEGIN;

	GFSendSysMailToPlayer* pSysMail = ( GFSendSysMailToPlayer *)pPkg;

	SupectMailManagerSingle::instance()->CreateSuspectSystemMail( pOwner->GetSessionID(), pSysMail->recvPlayerName, 
		pSysMail->sendMailTitle,
		pSysMail->sendMailContent,
		pSysMail->silverMoney,
		pSysMail->attachItem );//双币种判断


	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnDeleteAllMail(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFDeleteAllMail);

	TRY_BEGIN;

	GFDeleteAllMail* pDeleteMail = ( GFDeleteAllMail *)pPkg;

	PlayerMailSessionManagerSingle::instance()->DeleteAllMail( pDeleteMail->deletePlayerUID );

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPlayerUpdateStorageForbiddenInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFUpateStorageForbiddenInfo);

	TRY_BEGIN;

	GFUpateStorageForbiddenInfo* pForbiddenInfo = ( GFUpateStorageForbiddenInfo *)pPkg;

	StorageManagerSingle::instance()->UpdateStorageInputPsdErrInfo( pForbiddenInfo->storageUID ,pForbiddenInfo->forbiddenTime, pForbiddenInfo->inputErrCount );

	return true;

	TRY_END;

	return false;
}

bool CDealGateSrvPkg::OnMapSrvLoadRaceMasterInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryRaceMasterInfo);

	TRY_BEGIN;

	CRaceMasterManSingle::instance()->OnMapSrvQueryRaceMasterInfo( pOwner );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnUpdateRaceMasterInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFUpdateRaceMasterInfo);

	TRY_BEGIN;

	GFUpdateRaceMasterInfo* pUpdateRaceMaterInfo = (GFUpdateRaceMasterInfo *)pPkg;

	RaceMasterInfo racemasterinfo;
	racemasterinfo.declarewarinfo = pUpdateRaceMaterInfo->updateInfo.declareWarInfo;
	racemasterinfo.isgotsalary    = pUpdateRaceMaterInfo->updateInfo.isGotSalary;
	racemasterinfo.lastupdatetime = pUpdateRaceMaterInfo->updateInfo.lastupdatetime;
	racemasterinfo.masterUID      = pUpdateRaceMaterInfo->updateInfo.masterUID;
	racemasterinfo.racetype       = pUpdateRaceMaterInfo->updateInfo.racetype;
	racemasterinfo.todaymodifycount = pUpdateRaceMaterInfo->updateInfo.noticeCount;

	CRaceMasterManSingle::instance()->UpdateRaceMasterInfo( racemasterinfo.racetype, racemasterinfo );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnResetRaceMasterDeclareWarInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFResetRaceMasterDeclareWarInfo);

	TRY_BEGIN;

	GFResetRaceMasterDeclareWarInfo* pUpdateDeclareWarInfo = (GFResetRaceMasterDeclareWarInfo *)pPkg;

	CRaceMasterManSingle::instance()->OnDeclareWarReset( pUpdateDeclareWarInfo->racetype );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnRaceMasterNewBorn(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFNewRaceMasterBorn);

	TRY_BEGIN;

	GFNewRaceMasterBorn* pnewraceborn = ( GFNewRaceMasterBorn *)pPkg;

	CRaceMasterManSingle::instance()->OnNewRaceMasterBorn( pnewraceborn->racetype, pnewraceborn->masteruid, pnewraceborn->playerName, pnewraceborn->unionName );


	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPlayerUpdateStorageItemInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFUpdateStorageItemInfo);

	TRY_BEGIN;

	GFUpdateStorageItemInfo* pUpdateStorageInfo = ( GFUpdateStorageItemInfo *)pPkg;

	StorageManagerSingle::instance()->UpdateStorageItemInfo( pUpdateStorageInfo->storageUID,
		pUpdateStorageInfo->storageIndex, 
		pUpdateStorageInfo->storageItem );

	return true;

	TRY_END;

	return false;
}

bool CDealGateSrvPkg::OnPlayerUpdateStorageSafeInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFUpdateStorageSafeInfo);

	TRY_BEGIN;

	GFUpdateStorageSafeInfo* pUpdateStorageSafeInfo = ( GFUpdateStorageSafeInfo *)pPkg;

	D_DEBUG("更新玩家 %d 的仓库安全信息 Row:%d Mode:%d Psd:%s \n" , pUpdateStorageSafeInfo->storageUID ,
		pUpdateStorageSafeInfo->validRow ,
		pUpdateStorageSafeInfo->strategyMode,
		pUpdateStorageSafeInfo->newPsd
		);

	StorageManagerSingle::instance()->UpdateStorageSafeInfo( pUpdateStorageSafeInfo->storageUID , pUpdateStorageSafeInfo->validRow ,
		pUpdateStorageSafeInfo->strategyMode,
		pUpdateStorageSafeInfo->newPsd );

	return true;

	TRY_END;

	return false;
}

bool CDealGateSrvPkg::OnPlayerQueryStorageInfo(CGateServer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	FUNC_PARAM_CHECK(GFQueryStorageInfo);

	TRY_BEGIN;

	GFQueryStorageInfo* pQueryStorage = ( GFQueryStorageInfo *)pPkg;

	StorageManagerSingle::instance()->QueryStorageInfo( pOwner , pQueryStorage->queryPlayerID, pQueryStorage->storageUID );

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnGMQueryAllPublicNotice(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryAllPublicNotice);

	TRY_BEGIN;

	GFQueryAllPublicNotice* pQueryAllPublicnotices = (GFQueryAllPublicNotice *)pPkg;
	CPublicNoticeManagerSingle::instance()->SendPublicNoticeToPlayer( pOwner, pQueryAllPublicnotices->queryPlayerID );
	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnGMSelectRaceMaster(  CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFSelectRaceMaster);

	TRY_BEGIN;

	CRaceMasterManSingle::instance()->StartSelectRaceMater();
	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnGMAddPublicNotice(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFGMAddNewPublicNotice);

	TRY_BEGIN;

	GFGMAddNewPublicNotice* paddnewpublicnotices = (GFGMAddNewPublicNotice *)pPkg;
	CPublicNoticeManagerSingle::instance()->AddNewPublicNotice( paddnewpublicnotices->srvmsg.msgid, paddnewpublicnotices->srvmsg.content, 
		paddnewpublicnotices->srvmsg.istop,
		paddnewpublicnotices->srvmsg.starttime,
		paddnewpublicnotices->srvmsg.endtime,
		paddnewpublicnotices->srvmsg.broadcastnum,
		paddnewpublicnotices->srvmsg.distancetime,
		true );

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnGMModifyPublicNotice(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFModifyPublicNotice);

	TRY_BEGIN;

	GFModifyPublicNotice* pmodifypublicnotices = (GFModifyPublicNotice *)pPkg;
	CPublicNoticeManagerSingle::instance()->ModifyPublicNotice(  pmodifypublicnotices->srvmsg.msgid, pmodifypublicnotices->srvmsg.content, 
		pmodifypublicnotices->srvmsg.istop,
		pmodifypublicnotices->srvmsg.starttime,
		pmodifypublicnotices->srvmsg.endtime,
		pmodifypublicnotices->srvmsg.broadcastnum,
		pmodifypublicnotices->srvmsg.distancetime);

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnGMRemovePublicNotice(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFRemovePublicNotice);

	TRY_BEGIN;

	GFRemovePublicNotice* premovepublicnotice = (GFRemovePublicNotice *)pPkg;
	CPublicNoticeManagerSingle::instance()->DeletePublicNotice( premovepublicnotice->msgid );
	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnGMReLoadPublicNotice(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFLoadNewPublicNotice);

	TRY_BEGIN;

	GFLoadNewPublicNotice* loadnewpublic = (GFLoadNewPublicNotice *)pPkg;
	CPublicNoticeManagerSingle::instance()->ReLoadGMPublicNotice();
	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPlayerBidNewAuction(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPlayerStartNewAuction);

	TRY_BEGIN;

	GFPlayerStartNewAuction* newAuction = ( GFPlayerStartNewAuction *)pPkg;
	AuctionManagerSingle::instance()->OnPlayerBidNewItem( pOwner, newAuction->playerID, newAuction->newAuctionItem );

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPlayerBidItemSection1(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPlayerBidItemSection1);

	TRY_BEGIN;

	GFPlayerBidItemSection1* biditemsection1 = ( GFPlayerBidItemSection1 *)pPkg;
	AuctionManagerSingle::instance()->OnPlayerBidByNewPrice( pOwner, biditemsection1->playerID, biditemsection1->playerUID,
		biditemsection1->auctionUID, biditemsection1->newprice  );

	return true;
	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPlayerBidItemSection2(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPlayerBidItemSection2);

	TRY_BEGIN;

	GFPlayerBidItemSection2* biditemsection2 = ( GFPlayerBidItemSection2 *)pPkg;
	AuctionManagerSingle::instance()->BidItemByNewPrice( pOwner, biditemsection2->playerID, biditemsection2->playerUID, biditemsection2->bidPlayerName, biditemsection2->auctionUID,
		biditemsection2->newprice,biditemsection2->lastprice );

	return true;
	TRY_END;
	return false;

}

bool CDealGateSrvPkg::OnPlayerCancelAuction(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFPlayerCancelAuction);

	TRY_BEGIN;

	GFPlayerCancelAuction* cancelAuction = ( GFPlayerCancelAuction *)pPkg;
	AuctionManagerSingle::instance()->OnPlayerCancelAuction( pOwner, cancelAuction->playerID, cancelAuction->playerUID,cancelAuction->auctionUID );
	return true;

	TRY_END;

	return false;
}

bool CDealGateSrvPkg::OnPlayerQuerySelfAuction(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryMyAuction);

	TRY_BEGIN;

	GFQueryMyAuction* queryMyAuction = (GFQueryMyAuction *)pPkg;
	AuctionManagerSingle::instance()->OnPlayerQuerySelfAuction( pOwner, queryMyAuction->queryPlayerID, queryMyAuction->playerUID, queryMyAuction->pageIndex );
	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPlayerQuerySelfBid(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryMyBid);

	TRY_BEGIN;

	GFQueryMyBid* queryMyBid = ( GFQueryMyBid*  )pPkg;
	AuctionManagerSingle::instance()->OnPlayerQuerySelfBid( pOwner, queryMyBid->queryPlayerID,queryMyBid->playerUID, queryMyBid->pageIndex );
	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnPlayerQueryAuctionByCondition(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	FUNC_PARAM_CHECK(GFQueryAuctionCondition);

	TRY_BEGIN;

	GFQueryAuctionCondition* queryCondition = ( GFQueryAuctionCondition *)pPkg;
	AuctionManagerSingle::instance()->OnPlayerQueryAuctionByCondition( pOwner, queryCondition->queryPlayerID, *queryCondition );
	return true;


	return true;
	TRY_END;
}