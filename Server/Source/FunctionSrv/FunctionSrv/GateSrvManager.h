﻿/********************************************************************
	created:	2009/04/30
	created:	30:4:2009   15:06
	file:		GateSrvManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef GATESRV_MANAGER_H
#define GATESRV_MANAGER_H


#include <vector>
#include "aceall.h"
#include "../../Base/Utility.h"

class CGateServer;


struct MsgToPutContent
{
	unsigned int sessionID;
	unsigned int msgType;
	char szContent[512];

	MsgToPutContent():sessionID(0),msgType(0)
	{
		StructMemSet( szContent, 0x0, sizeof(szContent) );
	}

};


class CGateSrvManger
{
	friend class ACE_Singleton<CGateSrvManger, ACE_Null_Mutex>;

public:
	void PushGateSrv( CGateServer* pServer );

	CGateServer* GetValidGateSrv();

	CGateServer* GetGateSrvBySessionID( unsigned int wID );

	void RemoveGateSrv( CGateServer* pGateSrv );

	unsigned int GetGateSrvCount() { return (unsigned)mGateSrvVec.size(); }

	void TrySendMsg();

	void SendMsgToPut( MsgToPutContent* pContent );

	void PushMsgContentToVecotr(  MsgToPutContent* pContent );

public:
	std::vector<CGateServer*>& GetGateSrvVec() { return mGateSrvVec; };

private:
	std::vector<CGateServer *> mGateSrvVec;
	std::vector<MsgToPutContent *> mMsgContentVector;
	ACE_Thread_Mutex mutex_;
};

typedef ACE_Singleton<CGateSrvManger, ACE_Null_Mutex> GateSrvMangerSingle;

#endif

