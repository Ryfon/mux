﻿/********************************************************************
	created:	2009/04/21
	created:	21:4:2009   13:20
	file:		PlayerMail.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef PLAYER_MAIL_H
#define PLAYER_MAIL_H

#include <vector>
#include "MailBase.h"
#include <time.h>

class CGateServer;
class CPlayerMailSession
{
public:
	CPlayerMailSession( unsigned int PlayerUID ):mSessionOwnerID( PlayerUID ),IsLoadMailOK(false)
	{
		mSessionTime = time(NULL);
	}

	~CPlayerMailSession()
	{
		DeleteAllMail();
	}


public:
	unsigned int GetSessionOwnerID() { return mSessionOwnerID; }

	const std::vector<CNormalMail *>& GetNormalMailVec() { return mPlayerNormalMailVec; }

	const std::vector<CSystemMail *>& GetSystemMailVec() { return mPlayerSystemMailVec; }

	//当查询邮件的消息时
	void OnQueryMailDetailInfo( CGateServer* pGateSrv, const PlayerID& queryPlayerID, MAIL_TYPE mailType , unsigned int mailUID );

	//将信息存入数据库
	void UpdateMailListInfo( CNormalMail* pMail );

	//当打开一封未读邮件时
	void OnOpenUnReadMail( CNormalMail* pMail );

	//查询邮件列表依据页数
	void OnQueryMailListByPage( CGateServer* pSrv , const PlayerID& queryPlayerID, MAIL_TYPE mailType, unsigned int pageIndex );

	void PickItemFromMail( CGateServer* pSrv, const PlayerID& pickPlayerID, unsigned int mailuid, unsigned int pickIndex );

	void PickMoneyFromMail( CGateServer* pSrv, const PlayerID& pickPlayerID, unsigned int mailuid );

	//删除所有邮件
	void DeleteAllMail();

	//检测邮件是否过期
	void CheckMailIsExpire();

	time_t GetSessionStartTime() { return mSessionTime; }

public:
	//加载普通邮件
	bool LoadNormalMail( unsigned int mailuid,bool isOpen,unsigned int recvPlayerUID, unsigned int sendPlayerUID,const char* pMailSendPlayerName, int expireTime, const char* pMailTitle, const char* pMailContent );

	//加载系统邮件
	bool LoadSystemMail( unsigned int mailuid, bool isOpen,unsigned int recvPlayerUID, unsigned int sendPlayerUID,const char* pMailSendPlayerName,int expireTime, const char* pMailTitle, const char* pMailContent ,int money, ItemInfo_i* pItemArr );

	//获得新的邮件
	bool GetNewMail( CNormalMail* pMail );

	//删除邮件
	bool RemoveMail( MAIL_TYPE mailtype ,unsigned int mailuid , bool& isUnReadMail );

	//获取普通邮件
	CNormalMail* GetNormalMail( unsigned int mailuid );

	//获取系统邮件
	CSystemMail* GetSystemMail( unsigned int mailuid );

	//加载邮件成功
	void SetLoadMailOK() { IsLoadMailOK = true; }

	//加载邮件是否成功
	bool LoadMailOK() { return IsLoadMailOK; }

	//设置附件的道具
	void SetMailAttachItem( unsigned int mailuid ,unsigned int itemIndex, ItemInfo_i& itemInfo );

	//设置附件的金钱
	void SetMailAttachMoney( unsigned int mailuid ,int money );

	//获取未读邮件的个数
	unsigned int GetUnReadMailCount();

	//获取系统邮件的个数
	unsigned int GetSystemMailCount()  { return (unsigned int)mPlayerSystemMailVec.size(); }

	unsigned int GetMailPageCount( MAIL_TYPE mailtype );

private:
	unsigned int mSessionOwnerID;//玩家的ID号
	std::vector<CNormalMail *> mPlayerNormalMailVec;//玩家的普通邮件
	std::vector<CSystemMail *> mPlayerSystemMailVec;//玩家的系统邮件
	time_t mSessionTime;//这个session开始的时间
	bool   IsLoadMailOK;
};



class CPlayerMailSessionManager
{
	friend class ACE_Singleton<CPlayerMailSessionManager, ACE_Null_Mutex>;

public:
	CPlayerMailSessionManager()
	{
		m_lastMonitorTime = time(NULL);
	}

	~CPlayerMailSessionManager()
	{
		std::map<unsigned int , CPlayerMailSession* >::iterator lter = mPlayermailMap.begin();
		for ( ; lter != mPlayermailMap.end(); ++lter )
		{
			if ( lter->second )
				delete lter->second;
		}
		mPlayermailMap.clear();
	}

public:
	CPlayerMailSession* NewPlayerMailSession( unsigned int playeruid );

	void DeletePlayerMailSession( unsigned int playeruid );

	CPlayerMailSession* FindPlayerMailSession( unsigned int playeruid );

	void QueryMailListByPage( CGateServer* pGateSrv, unsigned int queryPlayerUID, PlayerID& playerID, MAIL_TYPE mailType ,unsigned int page );

	void QueryMailDetialInfo( CGateServer* pGateSrv, unsigned int queryPlayerUID, PlayerID& playerID, MAIL_TYPE mailType, unsigned int mailuid );

	void DeleteMail( CGateServer* pGateSrv, unsigned int queryPlayerUID, PlayerID& playerID, MAIL_TYPE mailType, unsigned int mailuid );

	void PickMailAttachInfo( CGateServer* pGateSrv, unsigned int pickPlayerUID, PlayerID& playerID,unsigned int mailuid,  unsigned int pickIndex );

	void PickMailMoney( CGateServer* pGateSrv, unsigned int pickPlayerUID, PlayerID& playerID,unsigned int mailuid );

	void PickMailAttachItemError( unsigned int pickPlayerUID, unsigned int mailuid, unsigned int pickIndex, ItemInfo_i& pickItem );

	void PickMailAttachMoneyError( unsigned int pickPlayerUID, unsigned int mailuid, int money );

	void DeleteAllMail( unsigned int deletePlayerUID );

	void MonitorPlayerSessionLiveTime();

private:
	std::map<unsigned int , CPlayerMailSession* > mPlayermailMap;
	time_t m_lastMonitorTime;
};

typedef ACE_Singleton<CPlayerMailSessionManager, ACE_Null_Mutex> PlayerMailSessionManagerSingle;

#endif

