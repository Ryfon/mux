﻿#include "AuctionQueryHandle.h"

#include "SuspectMailManager.h"

#include <string>

#define CHECK_MASK( T, POS )  (((T)&(1<<POS))>0)
#define RESET_MASK( T, POS )  ( (T)&=(~(1<<POS)))

#define SET_QUERY_CONDITION( T, POS ){\
	RESET_MASK( T, POS );\
	if( T > 0 ){ ss<<" and "; }\
};\

bool QueryAuctionHandle::QueryAuctionByCondition( CGateServer* pGateSrv, const PlayerID& playerID,const GFQueryAuctionCondition& condition )
{
	if ( !pGateSrv )
		return false;

	if ( (int)condition.pageindex < 1 )
		return false;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	stringstream ss;
	ss<<"select auctionplayer, iteminfo, id, fixprice,expiretime,curauctionprice,originalprice from auction ";
	if ( condition.selectmask > 0 )//如果有条件的
	{
		int tmpselectmask = condition.selectmask;

		ss<<"where ";
		if ( CHECK_MASK( tmpselectmask, 0) )//如果要依据姓名
		{
			ss<<" itemname = binary '"<< condition.itemname <<"' ";
			SET_QUERY_CONDITION( tmpselectmask, 0 );
		}

		if ( CHECK_MASK( tmpselectmask, 1) )//如果需要依据等级来搜索
		{
			ss<<" level>="<<(int)condition.level1 <<" and level <="<<(int)condition.level2<<" "; 
			SET_QUERY_CONDITION( tmpselectmask, 1 );
		}

		if ( CHECK_MASK( tmpselectmask, 2) )//如果需要依据材质等级排序
		{
			ss<<" masslevel>=" <<(int)condition.masslevel<<" ";
			SET_QUERY_CONDITION( tmpselectmask, 2 );
		}

		if ( CHECK_MASK(tmpselectmask, 3) )//要搜索特定的职业的
		{
			ss<<" LOCATE('"<<condition.itemclass <<"',itemclass)>0 ";
			SET_QUERY_CONDITION( tmpselectmask, 3 );
		}

		if ( CHECK_MASK(tmpselectmask, 4) )//搜索特定的位置
		{
			ss<<" equippos=" << (int)condition.itempos <<" ";
			SET_QUERY_CONDITION( tmpselectmask, 4 );
		}
	}

	if ( condition.orderby == 0  )//如果按材质排序
	{
		ss<<" order by masslevel ";
		if ( condition.massorder == 0  ){
			ss<<" desc ,";
		}
		else 
			ss<<" asc ,";

		ss<<" level ";
		if ( condition.levelorder == 0){
			ss<<" desc,";
		}
		else
			ss<<" asc,";
	}
	else
	{
		ss<<" order by level ";
		if ( condition.levelorder == 0 ){
			ss<<" desc ,";
		}
		else
			ss<<" asc,";

		ss<<" masslevel ";
		if ( condition.massorder == 0 ){
			ss<<" desc,";
		}
		else
			ss<<" asc,";
	}
	ss<<" itemtypeid desc ";

	int rowoffset = (condition.pageindex - 1)* 10;
	ss<<"limit "<<rowoffset<<",51 ";

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	// 执行查询
	CResult * pResult = pQuery->ExecuteSelect((const char*)ss.str().c_str(), (unsigned long)ss.str().size() );
	if(pResult == NULL)
	{
		D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
		// 销毁查询
		pConnection->DestroyQuery(pQuery);
		return false;
	}
	else
	{
		FGQueryAuctionConditionRst queryConditionRst;
		StructMemSet( queryConditionRst, 0x0, sizeof(FGQueryAuctionConditionRst ) );
		int rowindex = 0,effectIndex = 0;
		int queryPageEnd = pResult->RowNum() > 50? 0x0:0x2;//如果还能搜出50个以上的，则证明还没有结束，否则告诉client，搜索结束
		if( pResult->RowNum() > 0 )
		{
			while ( pResult->Next() && effectIndex < 50  )
			{
				AuctionItem& auctionItem = queryConditionRst.queryRst.auctionArr[rowindex];

				const char* bidplayername  = pResult->GetString(0);
				if ( bidplayername == NULL )
					continue;
				strncpy( auctionItem.auctionplayer ,bidplayername, MAX_NAME_SIZE );
				auctionItem.playernamesize = (char)ACE_OS::strlen(auctionItem.auctionplayer);
			
				const char* pTmpItemInfo   = pResult->GetString(1);
				if ( pTmpItemInfo == NULL )
					continue;

				StructMemCpy( auctionItem.iteminfo, pTmpItemInfo, sizeof(ItemInfo_i) );

				auctionItem.auctionUID  = pResult->GetInt(2);
				auctionItem.fixprice    = pResult->GetInt(3);
				auctionItem.expiretime  = pResult->GetInt(4);
				auctionItem.curprice    = pResult->GetInt(5);
				auctionItem.orgiprice   = pResult->GetInt(6);
				

				rowindex++;
				effectIndex++;
				if ( rowindex == QUERY_AUCTION_PAGE_SIZE )
				{
					queryConditionRst.queryPlayerID = condition.queryPlayerID;
					queryConditionRst.queryRst.pageindex  = condition.pageindex;
					queryConditionRst.queryRst.arrlen   = rowindex;
					queryConditionRst.queryRst.pageend  = 0;
					queryConditionRst.queryRst.pageend |= queryPageEnd;

					MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryAuctionConditionRst,pGateSrv,queryConditionRst );
					pGateSrv->SendPkgToSrv(pMsgToPut);

					StructMemSet( queryConditionRst, 0x0, sizeof(queryConditionRst) );
					rowindex = 0;
				}
			}
			if ( rowindex >= 0 )
			{
				queryConditionRst.queryPlayerID = condition.queryPlayerID;
				queryConditionRst.queryRst.pageindex  = condition.pageindex;
				queryConditionRst.queryRst.pageend    = 1;
				queryConditionRst.queryRst.pageend   |= queryPageEnd;
				queryConditionRst.queryRst.arrlen     = rowindex;

				MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryAuctionConditionRst,pGateSrv,queryConditionRst );
				pGateSrv->SendPkgToSrv(pMsgToPut);
			}
		}
		else
		{
			queryConditionRst.queryPlayerID = condition.queryPlayerID;
			queryConditionRst.queryRst.pageend   = 1;
			queryConditionRst.queryRst.pageend  |= queryPageEnd;
			queryConditionRst.queryRst.pageindex = condition.pageindex;

			MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryAuctionConditionRst,pGateSrv,queryConditionRst );
			pGateSrv->SendPkgToSrv(pMsgToPut);
		}

		// 销毁查询
		pQuery->FreeResult(pResult);
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return true;
}

void QueryAuctionHandle::QueryMyAuction(CGateServer* pGateSrv, const PlayerID& playerID , unsigned int playeruid, unsigned int page )
{
	if ( !pGateSrv )
		return;

	if ( page < 1 )
		return;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return;

	stringstream ss;
	ss<<"select iteminfo, id, fixprice,curauctionprice, expiretime from auction where playerUID=" << playeruid ;
	int rowoffset =  (page-1) * 50;
	ss<<" limit " <<rowoffset<<", 51";

	// 执行查询
	CResult * pResult = pQuery->ExecuteSelect((const char*)ss.str().c_str(), (unsigned long)ss.str().size() );
	if(pResult == NULL)
	{
		D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
	}
	else
	{
		FGQueryMyAuctionRst selfauctions;
		StructMemSet(selfauctions,0x0, sizeof(FGQueryMyAuctionRst) );
		int rowIndex = 0,effectIndex = 0;
		int queryPageEnd =  pResult->RowNum() > 50 ?0x0:0x2;//如果还能搜出50个以上的，则证明还没有结束，否则告诉client，搜索结束

		if( pResult->RowNum() > 0 )
		{
			while ( pResult->Next() && effectIndex < 50 )
			{
				AuctionItem& auctionItem = selfauctions.auctionRst.auctionsArr[rowIndex];

				const char* pTmpItemInfo   = pResult->GetString(0);
				if ( pTmpItemInfo == NULL )
					continue;
				StructMemCpy( auctionItem.iteminfo , pTmpItemInfo, sizeof(ItemInfo_i) );

				auctionItem.auctionUID = pResult->GetInt(1);
				auctionItem.fixprice   = pResult->GetInt(2);
				auctionItem.curprice   = pResult->GetInt(3);
				auctionItem.expiretime = pResult->GetInt(4);

				rowIndex++;
				effectIndex++;
				if ( rowIndex == QUERY_AUCTION_PAGE_SIZE )
				{
					selfauctions.queryPlayerID = playerID;
					selfauctions.auctionRst.arrlen    = rowIndex;
					selfauctions.auctionRst.pageEnd   = 0;
					selfauctions.auctionRst.pageEnd  |= queryPageEnd;
					selfauctions.auctionRst.pageIndex =page;
					
					MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMyAuctionRst,pGateSrv,selfauctions );
					pGateSrv->SendPkgToSrv(pMsgToPut);

					StructMemSet(selfauctions,0x0, sizeof(FGQueryMyAuctionRst) );
					rowIndex = 0;
				}
			}

			if ( rowIndex >= 0 )
			{
				selfauctions.queryPlayerID = playerID;
				selfauctions.auctionRst.arrlen    = rowIndex;
				selfauctions.auctionRst.pageEnd   = 1;
				selfauctions.auctionRst.pageEnd  |= queryPageEnd;
				selfauctions.auctionRst.pageIndex =page;
				
				MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMyAuctionRst,pGateSrv,selfauctions );
				pGateSrv->SendPkgToSrv(pMsgToPut);
			}

		}
		else
		{
			selfauctions.queryPlayerID =  playerID;
			selfauctions.auctionRst.pageEnd  = 1;
			selfauctions.auctionRst.pageEnd |= queryPageEnd;
			selfauctions.auctionRst.pageIndex= page;
			MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMyAuctionRst,pGateSrv,selfauctions );
			pGateSrv->SendPkgToSrv(pMsgToPut);
		}

		pQuery->FreeResult( pResult );
	}
	pConnection->DestroyQuery( pQuery );
}

void QueryAuctionHandle::QueryMyBid( CGateServer* pGateSrv, const PlayerID& playerID , unsigned int playeruid, unsigned int page )
{
	if ( !pGateSrv )
		return;

	if ( page < 1)
		return;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return;

	stringstream ss;
	ss<<"select A.auctionplayer,A.iteminfo,A.id, A.fixprice,A.curauctionprice,A.originalprice,A.expiretime, B.auctionprice "
	  <<"from auction as A, auctionrelation as B where A.id = B.auctionid and B.playeruid ="<< playeruid<<" order by A.id desc";

	int rowoffset =  (page-1) * 50;
	ss<<" limit " <<rowoffset<<", 51";

	// 执行查询
	CResult * pResult = pQuery->ExecuteSelect((const char*)ss.str().c_str(), (unsigned long)ss.str().size() );
	if(pResult == NULL)
	{
		D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
	}
	else
	{
		FGQueryMyBidRst selfBidStateRst;
		StructMemSet( selfBidStateRst, 0x0, sizeof(FGQueryMyBidRst) );
		int  rowIndex = 0, effectRow = 0;
		int  queryPageEnd = pResult->RowNum() > 50?0x0:0x2;

		if( pResult->RowNum() > 0 )
		{
			while ( pResult->Next() && effectRow < 50 )
			{
				SelfBidState& bidstate = selfBidStateRst.queryBidState.bidStateArr[rowIndex];

				const char* bidplayername  = pResult->GetString(0);
				if ( bidplayername == NULL )
					continue;
				strncpy( bidstate.bidItem.auctionplayer ,bidplayername, MAX_NAME_SIZE );
				bidstate.bidItem.playernamesize = (char)strlen(bidstate.bidItem.auctionplayer );

				const char* pTmpItemInfo   = pResult->GetString(1);
				if ( pTmpItemInfo == NULL )
					continue;
				StructMemCpy( bidstate.bidItem.iteminfo, pTmpItemInfo, sizeof(ItemInfo_i) );
				
				bidstate.bidItem.auctionUID = pResult->GetInt(2);
				bidstate.bidItem.fixprice   = pResult->GetInt(3);
				bidstate.bidItem.curprice   = pResult->GetInt(4);
				bidstate.bidItem.orgiprice  = pResult->GetInt(5);
				bidstate.bidItem.expiretime = pResult->GetInt(6);
				bidstate.myprice   = pResult->GetInt(7);

				rowIndex++;
				effectRow++;
				if (  QUERY_AUCTION_PAGE_SIZE == rowIndex )
				{
					selfBidStateRst.queryBidState.arrlen = rowIndex;
					selfBidStateRst.queryBidState.pageIndex = page;
					selfBidStateRst.queryBidState.pageEnd  = 0;
					selfBidStateRst.queryBidState.pageEnd |= queryPageEnd;
					selfBidStateRst.queryPlayerID = playerID;

					MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMyBidRst,pGateSrv,selfBidStateRst );
					pGateSrv->SendPkgToSrv(pMsgToPut);

					StructMemSet( selfBidStateRst, 0x0, sizeof(selfBidStateRst));
					rowIndex = 0;
				}
			}

			if ( rowIndex >= 0 )
			{
				selfBidStateRst.queryBidState.arrlen = rowIndex;
				selfBidStateRst.queryBidState.pageIndex = page;
				selfBidStateRst.queryBidState.pageEnd = 1;
				selfBidStateRst.queryBidState.pageEnd |= queryPageEnd;
				selfBidStateRst.queryPlayerID = playerID;
				MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMyBidRst,pGateSrv,selfBidStateRst );
				pGateSrv->SendPkgToSrv(pMsgToPut);

			}
		}
		else
		{
			selfBidStateRst.queryBidState.pageIndex = page;
			selfBidStateRst.queryBidState.pageEnd   = 1;
			selfBidStateRst.queryBidState.pageEnd  |= queryPageEnd;
			selfBidStateRst.queryPlayerID = playerID;

			MsgToPut *pMsgToPut = CreateSrvPkg( FGQueryMyBidRst,pGateSrv,selfBidStateRst );
			pGateSrv->SendPkgToSrv(pMsgToPut);
		}

		// 销毁查询
		pQuery->FreeResult(pResult);
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);
}

int QueryAuctionHandle::StartNewAuction(const PlayerID& playerID, const NewAuctionItem& newAuctionItem )
{

	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	char iteminfoarr[2*sizeof(ItemInfo_i)+1] = {0};
	pQuery->RealEscape( iteminfoarr, (const char *)&newAuctionItem.iteminfo, (unsigned long)sizeof(ItemInfo_i) );
	char itemOwnerName[2*MAX_NAME_SIZE+1] = {0};
	pQuery->RealEscape( itemOwnerName,newAuctionItem.ownerName, sizeof(newAuctionItem.ownerName) );
	char itemName[2*MAX_NAME_SIZE+1] = {0};
	pQuery->RealEscape( itemName, newAuctionItem.itemName, sizeof(newAuctionItem.itemName) );
	char itemClass[2*MAX_NAME_SIZE+1] = {0};
	pQuery->RealEscape( itemClass, newAuctionItem.itemClass,sizeof(newAuctionItem.itemClass ) );

	stringstream ss;
	ss<<"insert into auction( itemname, playerUID,level,masslevel,equippos,itemclass,auctionplayer,expiretime,fixprice,curauctionprice,originalprice,itemtypeid,iteminfo) values('"
	  << itemName <<"',"<<newAuctionItem.playerUID<<","<<(int)newAuctionItem.level<<","<<(int)newAuctionItem.masslevel<<","<<(int)newAuctionItem.equipPos<<",'"<<itemClass
	  <<" ','" << itemOwnerName <<"',"<< newAuctionItem.expiretime<<","<<newAuctionItem.fixprice <<","<<newAuctionItem.origiAuctionprice<<","<<newAuctionItem.origiAuctionprice<<","
	  <<newAuctionItem.iteminfo.usItemTypeID<<",'"<<iteminfoarr<<"');";

	int insertID = -1;
	// 执行查询
	if( pQuery->ExecuteUpdate((const char*)ss.str().c_str(), (unsigned long)ss.str().size() ) == -1 )
	{
		D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
	}
	else
	{
		insertID = pQuery->InsertID();
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);
	return insertID;
}

bool QueryAuctionHandle::OnAuctionEnd(unsigned int auctionUID )
{
	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
	if (pGateSrv == NULL )
		return false;

	bool  isAuctionEnd = false;

	do 
	{
		stringstream ss;
		ss<<"select iteminfo,auctionplayer from auction where id=" <<auctionUID;
		CResult * pRst = pQuery->ExecuteSelect((const char*)ss.str().c_str(), (unsigned long)ss.str().size() );
		if ( pRst == NULL ){
			D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
			break;
		}

		if ( pRst->RowNum() == 0 )
			break;
		else
			pRst->Next();

		char* iteminfoarr =  pRst->GetString(0);//拍卖物品的信息
		if ( iteminfoarr == NULL ){
			break;
		}

		ItemInfo_i itemInfo;
		StructMemCpy( itemInfo, iteminfoarr, sizeof(ItemInfo_i) );

		if ( pRst->GetString(1) == NULL ){
			D_ERROR("拍卖行竞排的物品存储的竞拍玩家姓名为空");
			break;
		}

		std::string auctionPlayerName = pRst->GetString(1);//拍卖物品的玩家信息
		pQuery->FreeResult( pRst );

		stringstream relationss;
		relationss<<"select playeruid,auctionprice,bidPlayerName from auctionrelation where auctionid =" <<auctionUID << " order by auctionprice desc";
		CResult * pRelationRst = pQuery->ExecuteSelect( relationss.str().c_str(), (unsigned long)relationss.str().size() );
		if ( pRelationRst == NULL ){
			D_ERROR("执行sql语句:%s错误\n", relationss.str().c_str());
			break;
		}

		//如果有人参与竞拍
		if ( pRelationRst->RowNum() > 0 )
		{
			int ownerIndex = 0;
			while( pRelationRst->Next() )
			{
				unsigned int playerUID    = pRelationRst->GetInt(0);
				unsigned int price        = pRelationRst->GetInt(1);
				const char* bidPlayerName = pRelationRst->GetString(2);
				if ( bidPlayerName == NULL )
					continue;

				ItemInfo_i itemArr[MAX_MAIL_ATTACHITEM_COUNT];
				StructMemSet( itemArr, 0,sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );

				if ( ownerIndex == 0 )//如果是获得者
				{
					itemArr[0] = itemInfo;
					SupectMailManagerSingle::instance()->CreateSuspectSystemMail( pGateSrv->GetSessionID(), 
						bidPlayerName,
						"竞拍成功",
						"恭喜您竞拍成功",
						0,
						itemArr
						);
				}
				else 
				{
					SupectMailManagerSingle::instance()->CreateSuspectSystemMail( pGateSrv->GetSessionID(), 
						bidPlayerName,
						"竞拍失败",
						"您的出价被人超过",
						price,
						itemArr
						);
				}
				ownerIndex++;
			}
		}
		else//如果一个人都没有
		{
			ItemInfo_i itemArr[MAX_MAIL_ATTACHITEM_COUNT];
			StructMemSet( itemArr, 0,sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );

			itemArr[0] = itemInfo;
			SupectMailManagerSingle::instance()->CreateSuspectSystemMail( pGateSrv->GetSessionID(), 
				auctionPlayerName.c_str(),
				"拍卖物品退回",
				"拍卖物品到期前无人竞拍",
				0,
				itemArr
				);
		}
		pQuery->FreeResult( pRelationRst );

		stringstream deletess;
		deletess<<"delete from auction where id="<<auctionUID;
		if( pQuery->ExecuteUpdate( deletess.str().c_str(), (unsigned long)deletess.str().size() ) == -1 ){
			D_ERROR("执行sql语句:%s错误\n", deletess.str().c_str());
			break;
		}

		stringstream delrelationss;
		delrelationss<<"delete from auctionrelation where auctionid="<< auctionUID;
		if( pQuery->ExecuteUpdate( delrelationss.str().c_str(), (unsigned long)delrelationss.str().size() ) == -1 ){
			D_ERROR("执行sql语句:%s错误\n", delrelationss.str().c_str());
			break;
		}

		isAuctionEnd = true;

	} while( false );

	pConnection->DestroyQuery( pQuery );
	return isAuctionEnd;
}

bool QueryAuctionHandle::PlayerBidItem( const PlayerID& playerID, unsigned int playeruid, const char* playerName, unsigned int auctionUID , unsigned int newprice )
{
	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return false;

	bool isBidRst = false;
	stringstream checkauctionss;
	checkauctionss<<"select id from auction where id=" << auctionUID;
	CResult * pCheckAuctionRst = pQuery->ExecuteSelect((const char*)checkauctionss.str().c_str(), (unsigned long)checkauctionss.str().size() );
	if ( pCheckAuctionRst == NULL )
	{
		D_ERROR("执行sql语句:%s错误\n", checkauctionss.str().c_str());
	}
	else
	{
		if ( pCheckAuctionRst->RowNum() > 0 )//如果这个道具还在拍卖中
		{
			pQuery->FreeResult( pCheckAuctionRst );

			stringstream checkauctionrelationss;
			checkauctionrelationss<<"select 1 from auctionrelation where playeruid="<<playeruid<<" and auctionid="<< auctionUID;
			CResult * pCheckRst = pQuery->ExecuteSelect((const char*)checkauctionrelationss.str().c_str(), (unsigned long)checkauctionrelationss.str().size() );
			if(pCheckRst == NULL){
				D_ERROR("执行sql语句:%s错误\n", checkauctionrelationss.str().c_str());
			}
			else
			{
				unsigned int affectnum = pCheckRst->RowNum();
				pQuery->FreeResult( pCheckRst );

				if ( affectnum > 0 )//如果以前竞拍过了
				{
					stringstream updatess;
					updatess<<"update auctionrelation set auctionprice=" << newprice <<" where playeruid="<<playeruid<<" and auctionid="<< auctionUID;
					if( pQuery->ExecuteUpdate((const char*)updatess.str().c_str(), (unsigned long)updatess.str().size() ) == -1 ){
						pConnection->DestroyQuery( pQuery );
						return isBidRst;
					}
				}
				else//如果以前没有竞拍过这个物品
				{
					char  szEscapeNameArr[2*MAX_NAME_SIZE+1] = {0};
					pQuery->RealEscape( szEscapeNameArr, playerName, (long)strlen(playerName)+1 );

					stringstream insertss;
					insertss<<"insert into auctionrelation(playeruid,auctionid,auctionprice,bidPlayerName ) values("<<playeruid<<","<<auctionUID<<","<<newprice
						<<",'"<<szEscapeNameArr<<"')";
					if( pQuery->ExecuteUpdate((const char*)insertss.str().c_str(), (unsigned long)insertss.str().size()) == -1 ){
						pConnection->DestroyQuery( pQuery );
						return isBidRst;
					}
				}

				stringstream updateauctionss;
				updateauctionss<<"update auction set curauctionprice ="<<newprice<<" where id="<<auctionUID;
				if( pQuery->ExecuteUpdate((const char*)updateauctionss.str().c_str(), (unsigned long)updateauctionss.str().size() ) != -1 ){
					isBidRst = true;
				}
			}
		}
		else
		{
			D_DEBUG("拍卖物品已经被人取走,玩家竞排无效");
		}
	}
	pConnection->DestroyQuery( pQuery );
	return isBidRst;
}

bool QueryAuctionHandle::OwnerCancelAuction( CGateServer* pGateSrv, unsigned int playerUID, unsigned int auctionUID )
{
	if ( !pGateSrv )
		return false;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if( pQuery == NULL)
		return false;

	bool  isCancelRst = false;
	do 
	{
		/************************************************************************/
		/* 将竞拍物品返还给物品所有者                                           */
		/************************************************************************/
		stringstream ss;
		ss<<"select iteminfo,auctionplayer from auction where id=" <<auctionUID<< " and playerUID=" <<playerUID;
		CResult * pRst = pQuery->ExecuteSelect((const char*)ss.str().c_str(), (unsigned long)ss.str().size() );
		if ( pRst == NULL ){
			D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
			break;
		}

		if ( pRst->RowNum() == 0 )
			break;
		else
			pRst->Next();

		char* iteminfoarr =  pRst->GetString(0);//拍卖物品的信息
		if ( iteminfoarr == NULL )
			break;

		ItemInfo_i itemInfo;
		StructMemCpy( itemInfo, iteminfoarr, sizeof(ItemInfo_i) );

		if ( pRst->GetString(1) == NULL ){
			D_ERROR("拍卖行竞排的物品存储的竞拍玩家姓名为空");
			break;
		}
		const char * auctionPlayerName = pRst->GetString(1);//拍卖物品的玩家信息

		ItemInfo_i itemArr[MAX_MAIL_ATTACHITEM_COUNT];
		StructMemSet( itemArr, 0,sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );
		itemArr[0] = itemInfo;
		SupectMailManagerSingle::instance()->CreateSuspectSystemMail( pGateSrv->GetSessionID(), 
			auctionPlayerName,
			"拍卖物品退回",
			"您取消了拍卖",
			0,
			itemArr
			);
		pQuery->FreeResult( pRst );


		/************************************************************************/
		/* 退还给所有参与竞拍的人金钱                                           */
		/************************************************************************/
		stringstream relationss;
		relationss<<"select auctionprice,bidPlayerName from auctionrelation where auctionid =" <<auctionUID << " order by auctionprice desc";
		CResult * pRelationRst = pQuery->ExecuteSelect( relationss.str().c_str(), (unsigned long)relationss.str().size() );
		if ( pRelationRst == NULL ){
			D_ERROR("执行sql语句:%s错误\n", relationss.str().c_str());
			break;
		}

		if ( pRelationRst->RowNum() > 0 )
		{
			//如果有人参与竞拍,将他们竞拍的金钱返还给竞拍的玩家
			while( pRelationRst->Next() )
			{
				unsigned int price         = pRelationRst->GetInt(0);
				const char*  bidPlayerName = pRelationRst->GetString(1);

				ItemInfo_i itemArr[MAX_MAIL_ATTACHITEM_COUNT];
				StructMemSet( itemArr, 0,sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );
				SupectMailManagerSingle::instance()->CreateSuspectSystemMail( pGateSrv->GetSessionID(), 
					bidPlayerName,
					"竞拍失败",
					"玩家取消了本次拍卖",
					price,
					itemArr
					);
			}
		}
		
		pQuery->FreeResult( pRelationRst );

		/************************************************************************/
		/* 清空拍卖纪录                                                         */
		/************************************************************************/
		stringstream deletess;
		deletess<<"delete from auction where id="<<auctionUID;
		if( pQuery->ExecuteUpdate( deletess.str().c_str(), (unsigned long)deletess.str().size() ) == -1 ){
			D_ERROR("执行sql语句:%s错误\n", deletess.str().c_str());
			break;
		}

		stringstream delrelationss;
		delrelationss<<"delete from auctionrelation where auctionid="<< auctionUID;
		if( pQuery->ExecuteUpdate( delrelationss.str().c_str(), (unsigned long)delrelationss.str().size() ) == -1 ){
			D_ERROR("执行sql语句:%s错误\n", delrelationss.str().c_str());
			break;
		}

		isCancelRst = true;

	} while( false );

	pConnection->DestroyQuery( pQuery );
	return isCancelRst;
}

bool QueryAuctionHandle::QueryPlayerLastBidPrice(unsigned int playerUID, unsigned int auctionUID, unsigned int& price )
{
	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return false;

	CQuery *pQuery = pConnection->CreateQuery();
	if( pQuery == NULL)
		return false;

	bool isExecRst = false;
	stringstream ss;
	ss<<"select auctionprice from auctionrelation where playeruid="<< playerUID << " and auctionid="<<auctionUID;
	CResult * pResult = pQuery->ExecuteSelect( ss.str().c_str(), (unsigned long)ss.str().size() );
	if ( pResult != NULL )
	{
		price = 0;
		if ( ( pResult->RowNum() > 0 ) && pResult->Next() )
		{
			price = pResult->GetInt(0);
			isExecRst = true;
		}
		pQuery->FreeResult( pResult );
	}
	else
	{
		D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
	}
	pConnection->DestroyQuery( pQuery );
	return isExecRst;
}

//bool QueryAuctionHandle::PlayerCancelBid(unsigned int playerUID, unsigned int auctionUID )
//{
//	// 创建查询
//	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
//	if(pConnection == NULL)
//		return false;
//
//	CQuery *pQuery = pConnection->CreateQuery();
//	if( pQuery == NULL)
//		return false;
//
//	CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
//	if (pGateSrv == NULL )
//		return false;
//
//	bool isCancelBid = false;
//	do 
//	{
//		stringstream ss;
//		ss<<"select auctionprice, bidPlayerName from auctionrelation where playeruid="<< playerUID << " and auctionid="<<auctionUID;
//		CResult * pResult = pQuery->ExecuteSelect( ss.str().c_str(), (unsigned long)ss.str().size() );
//		if ( pResult == NULL ){
//			D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
//			break;
//		}
//
//		if ( pResult->RowNum() > 0 )
//		{
//			ItemInfo_i itemArr[MAX_MAIL_ATTACHITEM_COUNT];
//			StructMemSet( itemArr, 0,sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );
//			while( pResult->Next() )
//			{
//				int price = pResult->GetInt(0);
//				const char* bidplayername = pResult->GetString(1);
//				SupectMailManagerSingle::instance()->CreateSuspectSystemMail( pGateSrv->GetSessionID(), 
//					bidplayername,
//					"竞拍失败",
//					"您取消了本次竞拍",
//					price,
//					itemArr
//					);
//			}
//		}
//
//		pQuery->FreeResult( pResult );
//
//		stringstream deletess;
//		deletess<<"delete from auctionrelation where playeruid="<< playerUID << " and  auctionid= "<<auctionUID;
//		if( pQuery->ExecuteUpdate( deletess.str().c_str(),(long)deletess.str().size() ) == -1 ){
//			D_ERROR("执行sql语句:%s错误\n", deletess.str().c_str());
//			break;
//		}
//
//		isCancelBid = true;
//
//	} while( false );
//
//	pConnection->DestroyQuery( pQuery );
//	
//	return  isCancelBid;
//
//}