﻿/**
* @file rankcache.h
* @brief rankcache
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: rankcache.h
* 摘    要: 排行榜数据缓存
  榜内人员的相关数据发生变化时记录，或有新人数据超过缓存内人员时将新人进缓存。（维持的数据记录数同DB内数据记录数）。
  已在缓存玩家的数据变化可能影响最大最小值，所以，只记一个进榜阈值不够，即使按历史最大最小阈值进缓存也不够，
  例如：这种情况将无法处理，100名的数值为300，这时的进缓存条件将为300，然后第100名的数值变为了90，
        则此时的进缓存条件应变为90，之前假设有人达到了95，则之前那人应该进缓存。
  所以，应区分只升排名缓存与可升降排名缓存，分别处理。对于可升降排名缓存，应多记一部分数据，并定时更新缓存数据。
* 作    者: dzj
* 完成日期: 2010.07.28
*/

#ifndef RANK_CACHE
#define RANK_CACHE

#include "Utility.h"
#include "../../Base/PkgProc/SrvProtocol.h"

#include "../../Base/DBConfigManager.h"

#include "../../Base/twothread/dstwothread.h"
#include "../../Base/twothread/dbnewtask.h"

#include "rankconfig.h"

#include <vector>

#ifdef WIN32
#include <hash_map>
#define HASH_MAP stdext::hash_map
#else //WIN32
#include <ext/hash_map>
#define HASH_MAP __gnu_cxx::hash_map
#endif //WIN32

using namespace std;

class CGateServer;

//单个排行榜的处理信息(更新时间，排行榜序号等)
class RankProcInfo
{
public:
	RankProcInfo()
	{
		StructMemSet( *this, 0, sizeof(*this) );
		m_isDbInited = false;//尚未使用DB数据初始化；
	}
	/*
	query << "create table " << tbName << " ( ranktype int UNIQUE, rankseq int(11) NOT NULL, updatetime int NOT NULL\
	                                           PRIMARY KEY(ranktype) );";
	*/
public:
	bool DataFromOtherProcInfo( const RankProcInfo& inProcInfo )
	{
		StructMemCpy( *this, &inProcInfo, sizeof(*this) );
		return true;
	}

	bool DataFromDbField( const char* inRankType, const char* inRankSeq, const char* inUpdateTime );

	inline bool IsDbInited() { return m_isDbInited; };

public:
	//使用输入参数判断自身是否需要更新；
	bool IsNeedRefresh( RankRefreshType refreshType );

	//设置自身已更新；
	bool SetRefreshed();

	unsigned int GetRankSeq() const { return m_rankSeq; };

	ERankType GetRankType() const { return m_rankType; };

	unsigned int GetLastUpdateTime() { return m_updateTime; };

public:
	bool         m_isDbInited;//是否已使用DB数据初始化;
	ERankType    m_rankType;
	unsigned int m_rankSeq;
	unsigned int m_updateTime;//上次更新时刻(time(NULL)获得;
};//class RankProcInfo

//缓存记录集;
struct RankRcdSet
{
public:
	RankRcdSet();

	bool ResetRcdSet();//清内部数据，并删除内部数组；

	bool ClearRcdSetData();//清内部数据，但保留内部数组；

	bool InitRankCacheSet( ERankType rankType, bool isDescOrder/*是否降序排序*/, unsigned int innerRcdNum );

	//使用vector数据初始化自身；
	bool InitRankCacheSetWithDataVec( ERankType rankType, bool isDescOrder/*是否降序排序*/, unsigned int innerRcdNum, const vector<RankCacheRcd*>& inVec );

	~RankRcdSet();

	bool DataFromOtherSet( const RankRcdSet& rankRcdSet );

	//取最大最小值；
	bool GetMinMax( int& minVal, int& minPlayer, int& maxVal, int& maxPlayer );

	//取最大记录对应玩家；
	bool GetMaxRcd( RankCacheRcd*& rankCacheRcd, unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ );

	bool IsRankCacheFull() { return GetValidRcdNum() == GetMaxRcdNum(); };

public:
	//将自身的排行信息发给客户端;
	bool NotiRankToPlayer( const PlayerID& playerID );

public:
	inline unsigned int GetValidRcdNum() const { return m_curRcdNum; };
	inline unsigned int GetMaxRcdNum() const { return m_maxRcdNum; };
	inline bool IsDescOrder() const { return m_isDescOrder; };
	inline ERankType GetRankType() const { return m_cacheType; };
	inline RankCacheRcd* GetIndexRcd( unsigned int index ) const
	{
		if ( ( index >= m_curRcdNum ) || ( index >= m_maxRcdNum ) )		
		{
			return NULL;
		}
		return &(m_arrCacheRecords[index]);
	};
	inline unsigned int GetIndexPlayerUID( unsigned int index )
	{
		if ( ( index >= m_curRcdNum ) || ( index >= m_maxRcdNum ) )		
		{
			return 0;
		}
		return m_arrCacheRecords[index].playerUID;

	}
	inline bool SetIndexRcd( unsigned int index, const RankCacheRcd& rankPlayerInfo )
	{
		if ( index >= m_maxRcdNum )		
		{
			NewLog( LOG_LEV_ERROR, "RankRcdSet::SetIndexRcd, index(%d) >= m_maxRcdNum(%d)", index, m_maxRcdNum );
			return false;
		}
		m_arrCacheRecords[index].DataFrom( rankPlayerInfo );
		return true;
	}

	//添加新的记录至Set，仅由OnRcvOtherRankInfo调用(使用来自shopsrv的星钻榜更新)
	inline bool PushRcd( const RankCacheRcd& rankPlayerInfo )
	{
		if ( m_curRcdNum >= m_maxRcdNum )
		{
			return false;
		}
		m_arrCacheRecords[m_curRcdNum] = rankPlayerInfo;
		++m_curRcdNum;
		return true;
	}

	inline bool IncRcdNum()
	{
		++m_curRcdNum;
		if ( m_curRcdNum > m_maxRcdNum )
		{
			NewLog( LOG_LEV_ERROR, "RankRcdSet::IncRcdNum, m_curRcdNum(%d) > m_maxRcdNum(%d)", m_curRcdNum, m_maxRcdNum );
			--m_curRcdNum;
			return false;
		}
		return true;
	}
	inline bool DecRcdNum()
	{
		if ( m_curRcdNum <= 0 )
		{
			NewLog( LOG_LEV_ERROR, "RankRcdSet::DecRcdNum, m_curRcdNum(%d) <= 0", m_curRcdNum );
			return false;
		}
		--m_curRcdNum;
		return true;
	}

	inline unsigned int GetRankSeqID()
	{
		if ( NULL == m_arrCacheRecords )
		{
			return 0;
		}
		return m_arrCacheRecords->GetRankSeqID();
	}

private:
	ERankType     m_cacheType;//缓存数据对应的排行榜类型
	bool            m_isDescOrder;//是否降序排序；
	unsigned int    m_curRcdNum;//当前记录数；
	unsigned int    m_maxRcdNum;//最大记录数；
	RankCacheRcd*   m_arrCacheRecords;//记录数组；
};

class CRankDBTask : public IDBTask<CRankDBTask>
{
public:
	enum RankTaskType{
		GET_RANK_DATA = 0,//从DB中取缓存数据；
		UPDATE_RANK_DATA = 1,//将srv中的缓存数据更新到DB；
		PERIOD_CLEAN_RANK = 2, //定时清DB中的缓存数据(每周/每月)；
		QUERY_TB_WRITE = 3,//写查询表
		QUERY_TB_READ = 4, //读查询表
		LOAD_RANKINFO = 5, //装载rank info(排行榜的更新时间,序号等等)
		UPDATE_RANKINFO = 6 //刷新rankinfo(排行榜更新时间，序号等等)
	};

public:
	//取排行榜对应的称号类型
	static EHonorMask GetRankResHonorMask( ERankType rankType, unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ );
	//取排行榜信息表；
	static const char* GetRankInfoTbName();
	//各排行榜缓存数据对应的表名；
	static const char* GetResTbName( ERankType rankType );
	//各排行榜待查表对应的表名；
	static const char* GetQueryTbName( ERankType rankType );
	//各排行榜的名字(用于奖励邮件内容填写)；
	static const char* GetRankName( ERankType rankType );

private:
	//取排序规则字符串
	inline const char* GetOrderRule()
	{
		if ( m_isDesc )
		{
			return " desc ";
		} else {
			return " asc ";
		}
	};

public:
	//设置任务信息；
	bool SetRankTaskInfo( RankTaskType taskType, ERankType rankType, bool isDesc/*是否由小至大排列*/, const RankRcdSet* pResRcdSet, const RankProcInfo* pProcInfo )
	{
		m_taskType = taskType;
		m_rankType = rankType;
		m_isDesc   = isDesc;
		m_rcdSet.InitRankCacheSet( rankType, isDesc, 200 );
		if ( ( UPDATE_RANK_DATA == taskType ) || ( QUERY_TB_WRITE == taskType ) )
		{
			if ( NULL == pResRcdSet )
			{
				NewLog( LOG_LEV_ERROR, "CRankDBTask::SetRankTaskInfo，更新数据库，但输入记录集空, taskType:%d", taskType );
				return false;
			}
			if ( m_rankType != pResRcdSet->GetRankType() )
			{
				NewLog( LOG_LEV_ERROR, "CRankDBTask::SetRankTaskInfo，m_rankType%d与输入数据的rankType%d不一致", m_rankType, pResRcdSet->GetRankType() );
				return false;
			}
			m_rcdSet.DataFromOtherSet( *pResRcdSet );
		} else if ( UPDATE_RANKINFO == taskType ) {
			if ( NULL == pProcInfo )
			{
				NewLog( LOG_LEV_ERROR, "CRankDBTask::SetRankTaskInfo，更新排行榜信息，但输入信息空" );
				return false;
			}
			if ( m_rankType != pProcInfo->GetRankType() )
			{
				NewLog( LOG_LEV_ERROR, "CRankDBTask::SetRankTaskInfo，更新排行榜信息，m_rankType%d与输入数据的rankType%d不一致", m_rankType, pProcInfo->GetRankType() );
				return false;
			}
			m_ProcInfo.DataFromOtherProcInfo( *pProcInfo );
		}
		return true;
	}

public:
	//辅助线程处理，或者从DB中新取缓存数据，或者向DB中更新缓存数据；
	virtual bool AssistProcMsg( CConnection* pDBConn );

	//主线程结果处理；
	virtual bool MainRstProcMsg();

private:
	bool GetRankData( CConnection* pDBConn );//从DB中取排行榜缓存数据；
	bool UpdateRankData( CConnection* pDBConn );//向DB更新排行榜缓存数据；	
	bool PeriodCleanRank( CConnection* pDBConn );//定时清DB排行榜缓存数据；
	
	bool LoadRankInfo( CConnection* pDBConn );//读排行榜信息(更新时间，序号等)
	bool UpdateRankInfo( CConnection* pDBConn );//更新排行榜信息

	bool QueryTbWrite( CConnection* pDBConn );//写查询表；
	bool QueryTbRead( CConnection* pDBConn );//读查询表；

private:
	bool ProcRankDataFromDB();//处理从DB中取到的排行榜缓存数据；			
	bool ProcQueryTbFromDB();//处理从DB中取到的查询表；
	bool ProcLoadRankInfo();//处理从DB中取到的排行榜信息；

private:
	bool WriteRankResTB( CConnection* pDBConn, const char* tbName );
	bool ReadRankResTB( CConnection* pDBConn, const char* tbName );

private:
	RankTaskType m_taskType;//任务类别
	ERankType  m_rankType;//排行榜类别
	bool         m_isDesc;
	RankRcdSet   m_rcdSet;//任务相关数据集

	RankProcInfo m_ProcInfo;//排行榜信息数据；

public:
	/////////////////////////////////////////////////////////////////////////////
	/***************************************************************************
	以下CRankDBTask池相关;
	****************************************************************************/
	/////////////////////////////////////////////////////////////////////////////
public:
	static bool InitInnerPool()
	{
		return m_TaskPool.InitPool( 1000 );
	}
	static CRankDBTask* NewRankDBTask()
	{
		return m_TaskPool.GetNewTask();
	}
	static bool DelRankDBTask( CRankDBTask* pToDel )
	{
		return m_TaskPool.ReleseOldTask( pToDel );
	}

private:
	static NewTaskPool<CRankDBTask> m_TaskPool;
	/////////////////////////////////////////////////////////////////////////////
	/***************************************************************************
	以上CRankDBTask池相关;
	****************************************************************************/
	/////////////////////////////////////////////////////////////////////////////
};

class CRankDBIns : public CDsTwoThread< CRankDBTask, 128/*inner queue size*/ >
{
public:
	CRankDBIns(){};
	virtual ~CRankDBIns(){};

public:
	bool InitRankDBIns( stConnectionString* pszConnectionString )
	{
		WorkPre( pszConnectionString );//CDsTwoThread需要；
		return true;
	}

	bool ReleaseRankDBIns()
	{
		WorkPost();//CDsTwoThread需要；
		return true;
	}
};

//single data cache for one rank;
class CRankCache
{
public:
	enum RankCacheType
	{
		RCT_INVALID = 0,//无效类型
		RCT_TMP = 1,//临时缓存
		RCT_CACHE = 2,//数据缓存
		RCT_QUERY = 3 //查询表
	};

public:
	CRankCache()
	{
		m_cacheType = RCT_INVALID;
		ResetRankCache();
	}

	~CRankCache()
	{
		ResetRankCache();
	}

	void ResetRankCache()
	{
		m_isCacheChanged = false;
		m_isDbInited = false;
	    m_enterValBias = 0;//进入缓存阈值；
	    m_isBiasValid = false;//阈值是否有效；
		m_dataSet.ClearRcdSetData();
	}

public:
	//增量加因子填充，用于根据多个查询表生成二级查询表(财富榜，传说英雄榜)
	bool FillIncTableWithFactor( map<unsigned int, RankCacheRcd>& tmpRst, float multiFactor );
	//前10名填充，用于生成传说英雄榜
	bool FillLegendTable( map<unsigned int, RankCacheRcd>& tmpRst );

public:
	//初始化缓存
	bool InitRankCache( RankCacheType cacheType, ERankType rankType, bool isDescOrder/*是否降序排序*/, unsigned int cacheNum )
	{
		ResetRankCache();
		m_cacheType = cacheType;
		m_dataSet.InitRankCacheSet( rankType, isDescOrder, cacheNum );
		return true;
	}// bool InitRankCache( ERankType cacheType, unsigned int cacheNum, bool isTrivialRank=false )

	//刷新缓存(根据当前缓存重新生成查询表，发奖，清空当前数据缓存，刷新rankinfo表)
	bool RefreshRankCache( unsigned int newRankSeq );

	//若自身缓存内容发生过变化，则存盘
	bool CheckIfSelfNeedSave();

private:
	//生成新查询表时是否需要清之前缓存；
	bool IsRefreshNeedClearCache();

private:
	//强制通过hashmap find确定玩家是否在本缓存中,两种调用情形，一、玩家上线时检测；二、玩家是否在本缓存中的精确确认(IsPlayerInCache)
	inline bool IsPlayerInCache(unsigned int playerUID)
	{
		return ( m_playerCachePos.end() != m_playerCachePos.find(playerUID) );//曾经进过缓存，确认其现在是否仍在缓存;
	}

	//标记玩家已在缓存
	inline bool FlagPlayerInCache( unsigned int playerUID, unsigned int playerPos )
	{
		m_playerCachePos[playerUID] = playerPos;
		return true;
	}

	//标记玩家出缓存
	inline bool FlagPlayerOutCache( unsigned int playerUID )
	{
		m_playerCachePos.erase(playerUID);
		return true;
	}

	//输入值排名是否在当前阈值之前
	inline bool IsValBeyondBias( int inVal )
	{
		int multifactor = 1;
		if ( m_dataSet.IsDescOrder() )
		{
			multifactor = -1;
		}

		if ( !m_isBiasValid )
		{
			int minBias = 0;
			bool isMinBiasValid = GetRankMinBias( GetRankType(), minBias );
			if ( !isMinBiasValid )
			{
				return false;
			}			
			return ( (inVal*multifactor) > (minBias*multifactor) );
		}

		return ( (inVal*multifactor) > (m_enterValBias*multifactor) );
	}

	//设置新的阈值；
	inline bool SetValBias( unsigned int newValBias, unsigned int playerUID );

	//取进入某排行榜的最小阈值
	bool GetRankMinBias( ERankType rankType, int& outMinBias );

	//获取排行榜称号玩家
	bool GetHonorPlayer( RankCacheRcd*& rankCacheRcd, unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ );

private:
	//尝试将玩家加入缓存,若缓存未满，且新记录值满足进入阈值条件，则应该加入成功，加入成功时返回true;
	bool TryAddNewPlayerInfo( const RankCacheRcd& rankPlayerInfo );

	//尝试替换新玩家(替换的肯定是原阈值对应玩家)
	bool TryReplacePlayerInfo( const RankCacheRcd& newPlayerInfo );

	//玩家已在缓存中，直接更新原玩家信息，若玩家为原阈值玩家，则刷新可能改变阈值；
	bool UpdateCachePlayerInfo( unsigned int playerPos, const RankCacheRcd& newRankPlayerInfo );

	//刷新Bias信息；
	bool RefreshBiasInfo();

public:
	//尝试将玩家信息加入缓存，若执行了加入操作则返回true;
	bool TryCachePlayerInfo( const RankCacheRcd& rankPlayerInfo, bool isSkipSeqCheck=false );
	//玩家删角色时的处理，若玩家在排行榜中，则将玩家出榜，若玩家为阈值玩家，则刷新阈值；
	bool ProcWhenRoleDelete( unsigned int playerUID );

public:
	//是否已根据DB数据初始化；
	inline bool IsDbInited() { return m_isDbInited; };
	inline bool SetDbInited() { m_isDbInited = true; return true; };//设置已初始化标记；
	inline bool SetDbToInit() { m_isDbInited = false; return true; };//设置待初始化标记
	//取阈值；
	bool GetBias( int& outBias )
	{
		if ( !m_isDbInited )
		{
			NewLog( LOG_LEV_WARNING, "收到mapsrv阈值请求信息时，缓存还未初始化，暂存此请求" );
			return false;
		}
		if ( !m_isBiasValid )
		{
			//NewLog( LOG_LEV_WARNING, "收到mapsrv阈值请求，缓存未满，返回默认最小阈值" );
			bool isMinBiasGot = GetRankMinBias( m_dataSet.GetRankType(), outBias );
			return isMinBiasGot;
		}
		outBias = m_enterValBias;
		return true;//阈值有效；
	}

	bool SetRankSeq( unsigned int inRankSeq ) 
	{
		m_rankSeq = inRankSeq;
		return true;
	}

	//取当前排行榜刷新序号；
	bool GetRankSeq( unsigned int& outRankSeq )
	{
		if ( !m_isDbInited )
		{
			NewLog( LOG_LEV_WARNING, "收到mapsrv排行榜刷新序号请求信息时，缓存还未初始化，暂存此请求" );
			return false;
		}
		outRankSeq = m_rankSeq;
		return true;
	}

	ERankType GetRankType()
	{
		return m_dataSet.GetRankType();
	}

private:
	//给缓存中的所有玩家发指定奖励，只由SendReword中的临时缓存调用!!!
	bool RewordAllPlayer( unsigned int rewordID, unsigned int rewordNum );

public:
	//将自身的排行信息发给客户端;
	bool NotiRankToPlayer( const PlayerID& queryPlayerID );
	//通知新的称号拥有者；
	bool NotifyRankHonorPlayer( unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ );
	//给前playerNum名玩家发送奖励；
    bool SendReword( unsigned int playerNum, unsigned int rewordID, unsigned int rewordNum )
	{
		CRankCache tmpCache;//临时用于发奖的缓存；
		tmpCache.InitRankCache( CRankCache::RCT_TMP, m_dataSet.GetRankType(), m_dataSet.IsDescOrder(), playerNum );
		tmpCache.SetDbInited();//设置已由DB初始化，以便稍后执行TryCachePlayerInfo;
		//unsigned int curRankSeq = 0;
		//bool isRankSeqGot = GetRankSeq( curRankSeq );
		//if ( !isRankSeqGot )
		//{
		//	NewLog( LOG_LEV_ERROR, "排行榜SendReword, 取rankSeq失败, rankType:%d", m_dataSet.GetRankType() );
		//	return false;
		//}
		//tmpCache.SetRankSeq( m_dataSet.m_arrCacheRecords.rankSeq );
		//查询缓存的rankseqid无意义，该id只用于缓存数据时确定数据的新旧程度，而查询表中的数据全有效，
		//  这里为了防止后续的TryCachePlayerInfo失败，将tmpCache的seqID直接设为数据的seqID;
		//tmpCache.SetRankSeq( m_dataSet.GetRankSeqID() );
		RankCacheRcd* pTmpRcd = NULL;
		unsigned int allPlayerNum = m_dataSet.GetValidRcdNum();
		for ( unsigned int i=0; i<allPlayerNum; ++i )
		{
			pTmpRcd = m_dataSet.GetIndexRcd(i);
			if ( NULL == pTmpRcd )
			{
				NewLog( LOG_LEV_ERROR, "排行榜SendReword, NULL == pTmpRcd, rankType:%d, i:%d", m_dataSet.GetRankType(), i );
				return false;
			}
			tmpCache.TryCachePlayerInfo( *(m_dataSet.GetIndexRcd(i)), true );
		}
		tmpCache.RewordAllPlayer( rewordID, rewordNum );
		return true;
	}

public:
	//使用其它SRV生成的排行榜更新自身(只有星钻排行需要)
	bool OnRcvOtherRankInfo( const GCNewRankInfo& otherRankInfo );

	//使用DB数据初始化服务器缓存；
	bool InitRankCacheData( const RankRcdSet& rcdSet );

	//使用自身数据更新DB缓存；
	bool UpdateRankCacheDataToDB();	

private:
	RankCacheType   m_cacheType;//是否临时缓存
	bool            m_isDbInited;//是否已使用DB数据初始化；
	bool            m_isCacheChanged;//自上次存DB后，数据是否发生过变化；
	unsigned int    m_rankSeq;
	RankRcdSet      m_dataSet;//缓存数据记录集；

	bool            m_isBiasValid;//阈值是否有效；
	int             m_enterValBias;//进入缓存阈值；
	unsigned int    m_biasResPlayer;//阈值对应的玩家UID；

	HASH_MAP<unsigned int, unsigned int> m_playerCachePos;//表明玩家(对应UID)是否在缓存中的hash表，若在，则值为该玩家在m_arrCacheRecords中的索引号；
	
};// class CRankCache

///排行榜管理数据缓存管理器
class CRankCacheManager
{
public:
	//初始化排行榜数据缓存管理器(读配置，初始化，等等，待实现)；
	static bool InitRankCacheManager();

	//发出load指令，从DB中装入排行榜缓存数据与查询数据；
	static bool IssueAllRankDBLoad();
	//指令从DB中装入某排行榜缓存数据与查询数据；
	static bool IssueOneCacheDBLoad( ERankType rankType );
	static bool IssueOneQueryDBLoad( ERankType rankType );
	//指令向DB更新某排行榜缓存数据；
	static bool IssueOneRankDBUpdate( ERankType rankType, const RankRcdSet& rankSet );
	//指令向DB更新某排行榜相关处理信息(更新日期，序号等等)；
	static bool IssueUpdateRankProcInfo( ERankType rankType, const RankProcInfo* pRankProcInfo );
	//发出指令：查询表更新至DB；
	static bool IssueUpdateQueryTB( ERankType rankType, const RankRcdSet& rcdSet );

public:
	//反复执行的排行榜相关处理
	static bool RankMainProc()
	{
		m_DBIns.MainThreadTimer();
		CheckRefreshTime();
		CheckDBCacheSave();//查询表与rank_info表都会在发生变化时即时更新，只有缓存数据表需要定时更新
		return true;
	}

	static bool ReleaseRankCacheManager() 
	{
		CheckDBCacheSave( true );//强制刷新缓存；
		m_DBIns.ReleaseRankDBIns();//双线程对象停止；
		return false;
	}

private:
	//查询表与rank_info表都会在发生变化时即时更新，只有缓存数据表需要定时更新
	static inline bool CheckRefreshTime()
	{
		if ( !m_isAllRankDbLoad )
		{
			return true;
		}
		unsigned int checkPassed = GetTickCount() - m_lastRefreshCheckTime;
		if ( checkPassed < 60*1000 ) /*一分钟检查一次，是否需要刷新*/
		{
			//未到检查时刻；
			return true;
		}
		m_lastRefreshCheckTime = GetTickCount();
		CheckAllRankRefresh();
		return true;		
	}

	static bool CheckDBCacheSave( bool isForce = false/*是否强制执行，关functionsrv时会强制执行一次*/ )
	{
		if ( !m_isAllRankDbLoad )
		{
			return true;
		}

		if ( !isForce ) 
		{
			//非强制执行，检查是否到了保存检查时间；
			unsigned int checkPassed = GetTickCount() - m_lastCacheSaveTime;
			if ( checkPassed < 30*1000 ) /*一分钟检查一次，是否需要刷新*/
			{
				//未到检查时刻；
				return true;
			}
			m_lastCacheSaveTime = GetTickCount();
		}

		CacheDataDBSave();

		return true;
	}

	//保存所有变化过的缓存；
	static bool CacheDataDBSave();

	//每分钟执行一次的各排行榜刷新检查；
	static bool CheckAllRankRefresh();

	//设置各缓冲的排行榜序号；
	static bool SetAllCacheSeq();

	//设置某缓冲的排行榜序号
	static bool SetOneCacheSeq( ERankType rankType );

public:
	//使用DB数据初始化服务器缓存；
	static bool InitRankCacheDataFromDB( const RankRcdSet& rcdSet );
	//使用自身数据初始化srv中的查询表；
	static bool InitRankQueryDataFromDB( const RankRcdSet& rcdSet );
	//使用自身数据初始化srv中的排行榜信息；
	static bool InitRankProcInfoFromDB( const RankProcInfo& procInfo );

	//1、使用自身信息更新当前查询表(函数内部包括：新的查询表更新至DB查询表，根据新查询表进行奖励发放，新称号通知)
	static bool GenQueryTableFromCache( const RankRcdSet& m_dataSet );

private:
	//使用数据更新查询表；
	static bool InitRankQueryWithData( const RankRcdSet& rcdSet );

public://待实现接口
	static bool OnRecvUpdatePlayerOnLineState( unsigned int rankPlayerUID, bool isOnline ) { return true; };
	static bool OnQueryRankInfoByPlayerName( CGateServer* pOwner, const PlayerID& queryPlayerID, ERankType rankType, const char* queryName ) { return true; };

public:
	static bool OnMapSrvLoadRankChartInfo( CGateServer* pOwner, unsigned int mapsrvID );
	static bool OnQueryRankChartInfoByPageIndex( CGateServer* pOwner, const PlayerID& queryPlayerID, ERankType rankType );

public:
	//static bool PunishMentBegin( CGateServer* pOwner ) { return true; };

public:
	//玩家删角色，若该角色在缓存数据中，则应将此角色信息去除；
	static bool OnPlayerDeleteRole( unsigned int playerUID );
	//处理新收到的cacheInfo
	static bool OnRcvNewCacheInfo( GFRankCacheInfo* pRankCacheInfo );
	//处理其它srv生成的排行榜
	static bool OnRcvOtherRank( GFOtherRank* pOtherRank );
	//检查是否所有需要的排行榜缓存数据和查询数据都已根据DB表初始化，若已初始化，则进行必要的通知；
	static bool CheckIfAllNeedRankDbInited();

public:
	//排行榜相关信息通知gatesrv，初始化完毕或有新gatesrv连入时进行此通知;
	static bool NotiRankResInfoToGateSrv();

private:
	//指定的排行榜缓存数据是否已根据DB初始化；
	static bool CheckIfCacheDbInited( ERankType rankType );
	//指定的排行榜查询数据是否已根据DB初始化；
	static bool CheckIfQueryDbInited( ERankType rankType );
	////指定的排行榜缓存数据与查询数据是否已根据DB初始化；；
	//static bool CheckIfSpecRankDbInited( ERankType rankType );

	//特定排行榜处理玩家删角色；
	static bool RankCacheDealPlayerDel( unsigned int playerUID, ERankType rankType );

	static bool OnAllDataInitedWithDBInfo();//所有数据(缓存与查询)都已使用DB信息初始化；

	//rankType对应排行榜是否需要刷新
	static bool CheckIfCacheNeedSave( ERankType rankType );

	//指定rank是否需要刷新；
	static bool CheckIfRankNeedRefresh( ERankType rankType );

	//生成新的二级榜
	static bool GenSecondQuery( ERankType rankType );

private:
	//所有称号玩家通知
	static bool NotifyAllHonorPlayer();
	//3、奖励发放；
	static bool SendRankReward( ERankType rankType );
	//4、新称号通知；
	static bool NotifyHonorPlayer( ERankType rankType, unsigned int sexFilter/*性别条件，0:无条件,1:男,2:女*/ );

private:
	//通知mapsrv新阈值
	static bool NotiMapSrvNewBias();

private:
	static bool m_isAllRankDbLoad;//是否所有需要的排行榜数据缓存都已根据DB表初始化；

	static CRankCache m_arrRankCache[RANK_TYPE_NUM];//缓存表；

	static CRankCache m_arrRankQuery[RANK_TYPE_NUM];//查询表；

	static RankProcInfo m_arrRankProcInfo[RANK_TYPE_NUM];//排行榜处理信息；

	static CRankDBIns m_DBIns;

	static unsigned int m_lastRefreshCheckTime;//上次刷新检查时间；

	static unsigned int m_lastCacheSaveTime;//上次缓存存DB时间；
}; //class CRankCacheManager

#endif // RANK_CACHE