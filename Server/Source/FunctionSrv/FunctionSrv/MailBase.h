﻿/********************************************************************
	created:	2009/04/14
	created:	14:4:2009   16:05
	file:		MailBase.h
	author:		管传淇
	
	purpose:	
*********************************************************************/


#ifndef _MAILBASE_H_
#define _MAILBASE_H_
//
#include "../../Base/PkgProc/SrvProtocol.h"
using namespace MUX_PROTO;
#include "aceall.h"
#include "../../Base/Utility.h"


#define MAX_NORMALMAIL_COUNT 100 //玩家的普通邮件的最大个数
#define MAX_SHOW_SYSTEMMAIL_COUNT 200 //系统邮件显示的最大个数
#define MAX_MAILPAGE_COUNT 8//每页显示邮件个数
#define DAY_SECOND  86400//一天有这么多秒
#define NORMAL_EXPIRE_DAY 15 //普通的邮件和不带附件的系统邮件的天数是 15天
#define ATTACH_EXPIRE_DAY 7	//带有附件的系统邮件是7天
#define SYSMAIL_ATTACH_LEFTTIME 259200 //系统附属邮件被打开时,系统邮件的监测时间


//邮件的附件
struct MailAttachInfo
{
	int mMoney;//系统的金钱
	ItemInfo_i   mAttachItem[MAX_MAIL_ATTACHITEM_COUNT];//邮件所包含的附件道具

	MailAttachInfo():mMoney(0)
	{
		StructMemSet( mAttachItem, 0x0 ,sizeof(ItemInfo_i)* MAX_MAIL_ATTACHITEM_COUNT );
	}

	~MailAttachInfo()
	{}

	MailAttachInfo& operator=( const MailAttachInfo& attachinfo )
	{
		if ( this != &attachinfo )
		{
			mMoney = attachinfo.mMoney;
			StructMemCpy( mAttachItem, attachinfo.mAttachItem,  sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );
		}
		return *this;
	}

	MailAttachInfo( const MailAttachInfo& attachinfo )
	{
		if ( this != &attachinfo )
		{
			mMoney = attachinfo.mMoney;
			StructMemCpy( mAttachItem, attachinfo.mAttachItem,  sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );
		}
	}
};

//邮件的详细信息
struct MailDetailInfo
{
	MAIL_TYPE mMailType;//玩家的邮件类型
	unsigned int mUID;//邮件的唯一编号
	bool mIsOpen;//是否被阅读过
	unsigned int mexpireTime;//邮件的生命期
	unsigned int mSendPlayerUID;//发送玩家的唯一编号
	unsigned int mRecvPlayerUID;//接受的玩家唯一编号
	char mTitle[MAX_MAIL_TITILE_LEN];//邮件标题
	char mSendPlayer[MAX_NAME_SIZE];//发送玩家姓名
	char mContent[MAX_MAIL_CONTENT_LEN];//发送内容
	

	MailDetailInfo():mMailType(E_INVAILD_MAIL),mUID(0),mIsOpen(false),mexpireTime(0),mSendPlayerUID(0),mRecvPlayerUID(0)
	{
		StructMemSet( mTitle, 0x0 ,MAX_MAIL_TITILE_LEN);
		StructMemSet( mSendPlayer, 0x0 ,MAX_NAME_SIZE );
		StructMemSet( mContent, 0x0, MAX_MAIL_CONTENT_LEN );
	}

	~MailDetailInfo()
	{}

	MailDetailInfo& operator=( const MailDetailInfo& maildetail )
	{
		if ( this != &maildetail )
		{
			mMailType = maildetail.mMailType;
			mUID      = maildetail.mUID;
			mIsOpen   = maildetail.mIsOpen;
			mexpireTime = maildetail.mexpireTime;
			mSendPlayerUID = maildetail.mSendPlayerUID;
			mRecvPlayerUID = maildetail.mRecvPlayerUID;
			StructMemCpy( mTitle, maildetail.mTitle , sizeof(mTitle) );
			StructMemCpy( mSendPlayer,maildetail.mSendPlayer, sizeof(mSendPlayer) );
			StructMemCpy( mContent, maildetail.mContent, sizeof(mContent) );
		}
		return *this;
	}


	MailDetailInfo( const MailDetailInfo& maildetail )
	{
		if ( this != &maildetail )
		{
			mMailType = maildetail.mMailType;
			mUID      = maildetail.mUID;
			mIsOpen   = maildetail.mIsOpen;
			mexpireTime = maildetail.mexpireTime;
			mSendPlayerUID = maildetail.mSendPlayerUID;
			mRecvPlayerUID = maildetail.mRecvPlayerUID;
			StructMemCpy( mTitle, maildetail.mTitle , sizeof(mTitle) );
			StructMemCpy( mSendPlayer,maildetail.mSendPlayer, sizeof(mSendPlayer) );
			StructMemCpy( mContent, maildetail.mContent, sizeof(mContent) );
		}
	}

};

class CNormalMail
{
public:
	CNormalMail( unsigned int uid, bool isOpen, unsigned int expiretime,unsigned int  sendPlayerID,const char * pSendPlayer, const char* pTitle, const char* pCotent )
	{
		mMaildetial.mUID = uid;
		mMaildetial.mIsOpen = isOpen;
		mMaildetial.mexpireTime = expiretime;
		mMaildetial.mSendPlayerUID = sendPlayerID;
		ACE_OS::strncpy( mMaildetial.mTitle, pTitle, sizeof(mMaildetial.mTitle) );
		ACE_OS::strncpy( mMaildetial.mSendPlayer, pSendPlayer, sizeof(mMaildetial.mSendPlayer) );
		ACE_OS::strncpy( mMaildetial.mContent, pCotent, sizeof(mMaildetial.mContent) );
		SetMailType( E_NORMAL_MAIL );
	}

	CNormalMail( unsigned int uid, bool isOpen, unsigned int expiretime,unsigned int recvPlayerUID, unsigned int  sendPlayerID,const char * pSendPlayer, const char* pTitle, const char* pCotent )
	{
		mMaildetial.mUID = uid;
		mMaildetial.mIsOpen = isOpen;
		mMaildetial.mexpireTime = expiretime;
		mMaildetial.mSendPlayerUID = sendPlayerID;
		mMaildetial.mRecvPlayerUID = recvPlayerUID;
		ACE_OS::strncpy( mMaildetial.mTitle, pTitle, sizeof(mMaildetial.mTitle) );
		ACE_OS::strncpy( mMaildetial.mSendPlayer, pSendPlayer, sizeof(mMaildetial.mSendPlayer) );
		ACE_OS::strncpy( mMaildetial.mContent, pCotent, sizeof(mMaildetial.mContent) );
		SetMailType( E_NORMAL_MAIL );
	}



  ~CNormalMail()
  {}

private:
	CNormalMail( const CNormalMail& );
	CNormalMail& operator=( const CNormalMail& mail );

public:
	unsigned int GetMailUID() { return mMaildetial.mUID; }

	bool    IsMailBeOpen() { return mMaildetial.mIsOpen; }

	unsigned int GetSendMailPlayerUID() { return mMaildetial.mSendPlayerUID; }

	unsigned int GetMailExpireTime() { return mMaildetial.mexpireTime; }

	const char*  GetMailTitle() { return mMaildetial.mTitle; }

	const char*  GetSendPlayerName() { return mMaildetial.mSendPlayer; }

	const char*  GetMailContent() { return mMaildetial.mContent; }
	
	unsigned int GetExpireTime() { return mMaildetial.mexpireTime; }

	void SetMailRecvPlayerID( unsigned int recvplayeruid ) {  mMaildetial.mRecvPlayerUID = recvplayeruid; }

	void OpenMail() {  mMaildetial.mIsOpen = true; }

	void SetMailContent( const char* pMailTitle , const char* pMailContent ) 
	{
		if ( pMailTitle )
			ACE_OS::strncpy( mMaildetial.mTitle, pMailTitle, MAX_MAIL_TITILE_LEN ); 

		if ( pMailContent )
			ACE_OS::strncpy( mMaildetial.mContent, pMailContent, MAX_MAIL_CONTENT_LEN ); 
	}

	void SetSendMailPlayerInfo( unsigned int sendPlayerUID, const char* psendPlayerName )
	{
		if ( psendPlayerName )
		{
			mMaildetial.mSendPlayerUID = sendPlayerUID;
			ACE_OS::strncpy( mMaildetial.mSendPlayer, psendPlayerName ,MAX_NAME_SIZE );
		}
	}

	//设置邮件类型
	void SetMailType( MAIL_TYPE mailType )
	{
		mMaildetial.mMailType = mailType;
	}


	void SetMailExpireTime( unsigned int expireTime )
	{
		mMaildetial.mexpireTime = expireTime;
	}

	//获得邮件的类型
	MAIL_TYPE GetMailType() { return mMaildetial.mMailType; }

	//获取邮件的详细信息
	const MailDetailInfo& GetMailDetialInfo() { return mMaildetial; }

	//当邮件被确认时
	void  MailBeAffirm( unsigned int playeruid );

private:
	MailDetailInfo mMaildetial;//邮件详细信息
};



class CSystemMail:public CNormalMail
{
public:
	CSystemMail( unsigned int uid, bool isOpen, unsigned int leftTime, const char* pTitle, const char* pCotent , int money , ItemInfo_i* itemArr )
		:CNormalMail( uid, isOpen, leftTime, 0,"system", pTitle, pCotent )
	{
		if ( itemArr )
		{
			for ( unsigned int  i = 0 ; i<  MAX_MAIL_ATTACHITEM_COUNT; i++ )
				mAttachInfo.mAttachItem[i] = itemArr[i];

			mAttachInfo.mMoney = money;
		}
		SetMailType( E_SYSYTEM_MAIL );
	}

	CSystemMail( unsigned int uid, bool isOpen, unsigned int leftTime,unsigned int recvPlayerUID, const char* pTitle, const char* pCotent , int money , ItemInfo_i* itemArr )
		:CNormalMail( uid, isOpen, leftTime,recvPlayerUID, 0,"system", pTitle, pCotent )
	{
		if ( itemArr )
		{
			for ( unsigned int  i = 0 ; i<  MAX_MAIL_ATTACHITEM_COUNT; i++ )
				mAttachInfo.mAttachItem[i] = itemArr[i];

			mAttachInfo.mMoney = money;
		}
		SetMailType( E_SYSYTEM_MAIL );
	}

	~CSystemMail()
	{
	}

	int   GetAttachInfoMoney() { return mAttachInfo.mMoney ; }

	const ItemInfo_i* GetAttachInfoItem() { return mAttachInfo.mAttachItem; }

	MailAttachInfo& GetMailAttachInfo() { return mAttachInfo; } 

	//含有的附属的道具的个数
	unsigned int GetAttachItemCount();

	//是否含有附属的道具信息
	bool HaveAttachInfo();

	void ClearAttachItem( unsigned int itemIndex );

	void SetItemAttachItem( unsigned int itemIndex, ItemInfo_i  itemInfo );

	void SetAttachMoney( int money ) { mAttachInfo.mMoney = money; }

private:
	MailAttachInfo mAttachInfo;
};



#endif

