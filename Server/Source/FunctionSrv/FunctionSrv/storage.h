﻿/********************************************************************
	created:	2009/09/10
	created:	10:9:2009   10:40
	file:		storage.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef STROAGE_H_
#define STROAGE_H_

#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/MysqlWrapper.h"
using namespace MUX_PROTO;
#include "aceall.h"
#include "SimpleMemoryPool.h"

#include <map>

class CGateServer;

#define  STORAGE_LIVE_TIME 900//每个有15分钟的生存期

//玩家的仓库信息
struct StorageInfo
{
	unsigned int storageUID;//仓库的唯一编号
	int          validRow;//可用的行数
	ItemInfo_i   storageItemArr[MAX_STORAGE_ITEMCOUNT];//仓库道具数组
	char         storagePassWord[MAX_STORAGE_PASSWORD_SIZE];//仓库的密码
	STORAGE_SECURITY_STRATEGY securityMode;//安全模式
	unsigned int lastForbiddenTime;//最后一次被限制使用仓库的时间
	unsigned int inputPsdErrCount;//输入密码错误的次数

	time_t       bornTime;//被创建的时间

	StorageInfo():storageUID(0),validRow(2),securityMode(E_UNSECURITY_MODE),lastForbiddenTime(0),inputPsdErrCount(0)
	{
		StructMemSet( storageItemArr, 0x0 ,sizeof(ItemInfo_i)*MAX_STORAGE_ITEMCOUNT );
		StructMemSet( storagePassWord, '\0', MAX_STORAGE_PASSWORD_SIZE );
		bornTime = time(NULL);
	}

	void Init()
	{
		storageUID = 0;
		validRow = 2;
		securityMode = E_UNSECURITY_MODE;
		lastForbiddenTime = 0;
		inputPsdErrCount = 0;
		StructMemSet( storageItemArr, 0x0 ,sizeof(ItemInfo_i)*MAX_STORAGE_ITEMCOUNT );
		StructMemSet( storagePassWord, '\0', MAX_STORAGE_PASSWORD_SIZE );
		bornTime = time(NULL);
	}

	unsigned int GetStorageUID() { return storageUID; }
	int          GetValidRow()   { return validRow; }
	ItemInfo_i*  GetStorageItem(){ return storageItemArr;}
	char*        GetStoragePsd() { return storagePassWord; }
	STORAGE_SECURITY_STRATEGY GetSecurityStrategy() { return securityMode; }
	unsigned int GetLastForbiddentTime() { return lastForbiddenTime; }
	unsigned int GetInputPsdErrCount() { return inputPsdErrCount; }
	time_t       GetBornTime() { return bornTime; }  
	void         UpdateItemInfo( unsigned int storageIndex, const ItemInfo_i& storageItem )
	{
		if ( storageIndex < MAX_STORAGE_ITEMCOUNT )
		{
			storageItemArr[ storageIndex ] = storageItem;
		}
		else
		{
			D_ERROR("StorageInfo::UpdateItemInfo 下标越界，storageIndex=%d\n", storageIndex);
		}
	}

	void         UpdateSafeInfo( unsigned int in_validRow, STORAGE_SECURITY_STRATEGY in_securityMode, const char* in_psd )
	{
		validRow = in_validRow;
		securityMode = in_securityMode;
		ACE_OS::strncpy( storagePassWord , in_psd ,sizeof(storagePassWord) );
	}

	void        UpdateForbiddenInfo( unsigned int in_forbiddenTime ,unsigned int in_inputErrCount )
	{
		lastForbiddenTime = in_forbiddenTime;
		inputPsdErrCount  = in_inputErrCount;
	}

	void SetStorageUID( unsigned int in_storageUID ) {  storageUID = in_storageUID ; }
	void SetStorageValidRow( unsigned int in_validRow ) { validRow = in_validRow; }
	void SetStorageItem( ItemInfo_i* in_itemRow ) { if(in_itemRow) StructMemCpy( storageItemArr,in_itemRow, sizeof(storageItemArr) ); }
	void SetStoragePsd( const char* in_psd ) { if(in_psd) ACE_OS::strncpy( storagePassWord, in_psd, MAX_STORAGE_PASSWORD_SIZE ); }
	void SetStorageSecurityMode( STORAGE_SECURITY_STRATEGY in_securityMode ) { securityMode = in_securityMode; }
	void SetLastForbiddenTime( unsigned int in_lastForbiddenTime ) { lastForbiddenTime = in_lastForbiddenTime; }
	void SetInputErrCount( unsigned int in_errCount ) { inputPsdErrCount = in_errCount; }
};


//仓库的管理器 
class CStorageManager:public SimpleObjectPool<StorageInfo,1000,ObjInitFuncTrait>
{
	friend class ACE_Singleton<CStorageManager, ACE_Null_Mutex>;

public:

	void PushStorageInfo( unsigned int playerUID, StorageInfo * storageInfo )
	{
		std::map<unsigned int , StorageInfo *>::iterator findIter = mStorageInfoMap.find( playerUID );
		if ( findIter!= mStorageInfoMap.end() )
		{
			StorageInfo *pTmpStorage = findIter->second;
			Dellocate( pTmpStorage );
		}
		mStorageInfoMap[playerUID] = storageInfo;
	}

	void RemoeStorageInfo( unsigned int playerUID )
	{
		std::map<unsigned int , StorageInfo *>::iterator findIter = mStorageInfoMap.find( playerUID );
		if ( findIter != mStorageInfoMap.end() )
		{
			StorageInfo *pTmpStorage = findIter->second;
			Dellocate( pTmpStorage );

			mStorageInfoMap.erase( findIter );
		}
	}

	//监视每个仓库的生存期
	void MonitorStorageLive();

	//查询玩家的仓库信息
	void QueryStorageInfo(  CGateServer* pGateSrv , const PlayerID& queryPlayer,  unsigned int queryUID );

	//更新仓库的道具信息
	void UpdateStorageItemInfo( unsigned int storageID, unsigned int storageIndex , const  ItemInfo_i& updateItem );

	//更新仓库的安全信息
	void UpdateStorageSafeInfo( unsigned int storageID, unsigned int validRow,  STORAGE_SECURITY_STRATEGY securityMode , const char* psd );

	//更新仓库的禁止使用的时间
	void UpdateStorageInputPsdErrInfo( unsigned int storageID, unsigned int forbiddenTime, unsigned int errCount );

private:

	//获取对应编号的仓库信息 
	StorageInfo* GetStorageInfo( unsigned int storageUID );

	//将玩家的仓库信息发送给玩家
	void NoticeStorageInfoToPlayer( CGateServer* pGateSrv , const PlayerID& queryPlayer, StorageInfo* pStorageInfo );

	//第一次创建仓库,通知client弹出设置密码框的功能
	void NoticePlayerShowSetStoragePsdDlg( CGateServer* pGateSrv , const PlayerID& queryPlayer );

private:
	std::map<unsigned int , StorageInfo *> mStorageInfoMap;
};

typedef ACE_Singleton<CStorageManager, ACE_Null_Mutex> StorageManagerSingle;

#endif