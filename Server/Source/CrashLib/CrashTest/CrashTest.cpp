﻿// CrashTest.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "../../CrashLib/CrashLib.h"
//#define  _SECURE_SCL 0
#define  _SECURE_SCL_THROWS 1
#include <iostream>
using namespace std;
#include <vector>

//#define INVALID_ACCESS
#define INVALID_ITERATOR

int _tmain(int argc, _TCHAR* argv[])
{
	CCrashLib crashLib;

#ifdef INVALID_ACCESS

	try
	{
		int* p = NULL;
		*p = 3;
	}
	catch(...)
	{

	}
#endif

#ifdef INVALID_ITERATOR

	//try
	{

		std::vector<int> vec;
		vec.push_back( 1 );
		vec.push_back( 2 );
		vec.push_back( 3 );
		vec.push_back( 4 );

		for ( std::vector<int> ::iterator lter = vec.begin(); lter != vec.end(); ++lter )
		{
			for ( std::vector<int> ::iterator iter = vec.begin(); iter != vec.end(); ++iter )
			{
				vec.erase( iter );
			}
		}

	}
	/*catch(...)
	{
		
	}*/
	 
#endif

	return 0;
}

