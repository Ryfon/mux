﻿#ifndef CRASHHANDLE_H_
#define CRASHHANDLE_H_


#if defined(_WIN32) || defined(_WIN64)

#include <DbgHelp.h>
#include <cstdio>
#include <crtdbg.h>
#include "DumpFile.h"

#define SAFERELEASE(p) {if (p) {(p)->Release(); p = NULL;}}


class CExceptionReport
{
public:

	//construct
	CExceptionReport( PEXCEPTION_POINTERS ExceptionInfo );

	//destructor
	~CExceptionReport();

public:
	//处理异常信息
	void ExceptionHandle( LPEXCEPTION_POINTERS lpEP );

private:
	//创建个dump包
	void CreateMiniDump();

	//记录死机时的CPU信息
	void Dump_ProcessInfo( EXCEPTION_RECORD* exception, CONTEXT* context );

	//记录死机时的调用堆栈
	void Dump_CallStack( CONTEXT* context );

private:
	//convert code of exception to const char *
	const char* exception_string( int exception );

private:
	PEXCEPTION_POINTERS m_excpInfo;
	_invalid_parameter_handler m_oldHandler ,m_newHandler;
	DumpFile mdumpFile;

};

#endif

#endif CRASHHANDLE_H_