﻿#ifndef COMMONFUNCTION_H
#define COMMONFUNCTION_H

#if defined(_WIN32) || defined(_WIN64)

#pragma warning(push)
#pragma warning(disable:4996)

static char *
convert_wbcs_to_utf8(const wchar_t *str)
{
	size_t len = wcslen(str);
	char *utf8_str = (char *)malloc(sizeof(wchar_t) * len + 1);
	len = wcstombs(utf8_str, str, len);

	if (len == -1)
		return NULL;

	utf8_str[len] = '\0';

	return utf8_str;
}

char* StorkFileName( char* pFilePath )
{
	char * q = NULL;
	if ( pFilePath !=NULL )
	{
		char* p = strtok( pFilePath,"\\");
		while(p !=NULL)
		{
			p = strtok( NULL,"\\" );
			if ( p )
				q = p;
		}
	}

	return q;
}
#pragma warning(pop)

#endif

#endif  COMMONFUNCTION_H