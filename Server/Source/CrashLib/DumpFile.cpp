﻿
#include "stdafx.h"

#ifdef  WIN_PLATFORM

#include "DumpFile.h"
#include <atlstr.h>

//disable warning 4996
#pragma   warning(push)            
#pragma   warning(disable:4996)   


DumpFile::~DumpFile()
{
	CloseDumpFile();
}


int DumpFile::OpenDumpFile()
{
	if ( hFile == INVALID_HANDLE_VALUE )
	{
#define BUFLEN 56
		char timeBuf[BUFLEN]={0};
		struct   tm   *ptr= NULL;
		time_t   lt;     
		lt   =time(NULL);     
		ptr  =gmtime(&lt);     
		if ( ptr == NULL )
			return CREATE_DUMPFILETXT_FAIL;

		sprintf( timeBuf ,"%d_%d_%d_%d_%d_%d_CrashFile.txt", 
			1900 + ptr->tm_year,
			ptr->tm_mon+1,
			ptr->tm_mday,
			22 -ptr->tm_hour,
			ptr->tm_min,
			ptr->tm_sec );


		USES_CONVERSION;
		hFile = CreateFile( A2T(timeBuf), GENERIC_WRITE, 0,// 不共享
			NULL, OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile != INVALID_HANDLE_VALUE)
		{
			return CREATE_DUMPFILETXT_SUCCESS;
		}
	}


	return CREATE_DUMPFILETXT_FAIL;
}

void DumpFile::CloseDumpFile()
{
	if ( hFile != INVALID_HANDLE_VALUE )
	{
		CloseHandle( hFile ); 
	}
}

void DumpFile::WriteDumpInfoToFile(const char* szFormat,...)
{
	if ( hFile != NULL)
	{
		va_list args;
		va_start(args,szFormat);

		char szBuffer[MAXSTRINGLEN]={0};
		vsprintf(
			szBuffer,
			szFormat,
			args
			);
		va_end(args);

		USES_CONVERSION;
		CString strContent = A2T( szBuffer );
		DWORD dwReceive = 0;
		if( WriteFile( hFile,strContent.GetBuffer(), strContent.GetLength()*2, &dwReceive, NULL ) == FALSE )
		{
			_tprintf(_T("写入文件失败\n"));
		}
	}
}

bool DumpFile::Init()
{

	bool isSuccess =  (OpenDumpFile() == CREATE_DUMPFILETXT_SUCCESS);
	return isSuccess;
}

#pragma   warning(pop) 

#endif
