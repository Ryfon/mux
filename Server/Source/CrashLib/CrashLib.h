﻿
#if defined(_WIN32) || defined(_WIN64)

#ifdef CRASHLIB_EXPORTS
#define CRASHLIB_API __declspec(dllexport)
#else
#define CRASHLIB_API __declspec(dllimport)
#endif

#include <Windows.h>
#include <DbgHelp.h>
#pragma comment(lib,"dbghelp.lib")

class CExceptionReport;

// 此类是从 CrashLib.dll 导出的
class CRASHLIB_API CCrashLib {
public:
	CCrashLib(void);
	// TODO: 在此添加您的方法。

	~CCrashLib();
private:
	CExceptionReport* m_exceptionRpt;
	LPTOP_LEVEL_EXCEPTION_FILTER  m_oldFilter;      // previous exception filter
};

#endif