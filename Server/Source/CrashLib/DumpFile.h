﻿#ifndef DUMPFILE_H
#define DUMPFILE_H


#if defined(_WIN32) || defined(_WIN64)

#include <cstdarg>
#include <cstdio>
#include <ctime>
#include <Windows.h>

#define  CREATE_DUMPFILETXT_FAIL -1
#define  CREATE_DUMPFILETXT_SUCCESS 1

class DumpFile
{
public:
	DumpFile():hFile(INVALID_HANDLE_VALUE){}

	~DumpFile();

private:
	void CloseDumpFile();

	int  OpenDumpFile();

public:
	bool Init();

	void WriteDumpInfoToFile( const char* szFormat,...);

	bool IsFileOpen() { return hFile!= INVALID_HANDLE_VALUE;}
private:
	HANDLE hFile;
};


#endif

#endif  DUMPFILE_H