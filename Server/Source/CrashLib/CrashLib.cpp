﻿// CrashLib.cpp : 定义 DLL 应用程序的入口点。
//

#include "stdafx.h"

#ifdef WIN_PLATFORM

#include <atlbase.h>
#include "CrashLib.h"
#include "CrashHandle.h"



#ifdef _MANAGED
#pragma managed(push, off)
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif


//异常回调函数
LONG WINAPI CustomUnhandledExceptionFilter(PEXCEPTION_POINTERS pExInfo)
{
	CExceptionReport   exceptionRrp( pExInfo );
	exceptionRrp.ExceptionHandle(pExInfo);

	return EXCEPTION_EXECUTE_HANDLER;
}


// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 CrashLib.h
CCrashLib::CCrashLib()
{
	HRESULT hRes = ::CoInitialize(NULL);
	m_oldFilter = SetUnhandledExceptionFilter(CustomUnhandledExceptionFilter);
	return;
}

CCrashLib::~CCrashLib()
{
	if (m_oldFilter)
		SetUnhandledExceptionFilter(m_oldFilter);
	::CoUninitialize();
}

#endif