﻿#ifndef __SERVICE_SPACE_DB2O_FIELD_H__
#define __SERVICE_SPACE_DB2O_FIELD_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/utilib/named_tuple.h"
#include "../ServiceSpaceLib/utilib/to_string.h"
#include "type2sqlstring.h"
#include <memory>
#include <stdexcept>
#include <cassert>
#include <string>
#include <mysql.h>
#include <cstdio>

__SERVICE_SPACE_BEGIN_NS__

using namespace utilib;

class strict_base
	//: public noncopyable
{
public:
	strict_base()
		: next_(0)
	{ }

	strict_base(strict_base * next)
		: next_(0)
	{
		if (next)
			next_ = next->clone();
	}

	strict_base(const strict_base & st)
		: next_(0)
	{
		if (st.next_)
			next_ = st.next_->clone();
	}

	virtual ~strict_base()
	{
		if (next_)
		{
			next_->destroy();
			next_ = 0;
		}
	}

	virtual unsigned to_string(char * to)const = 0;

	virtual strict_base * clone()const = 0;

	virtual void destroy() = 0;

	const strict_base * next()const
	{
		return next_;
	}

	strict_base * next()
	{
		return next_;
	}

	void next(strict_base * st)
	{
		if (st)
		{
			st->next(next_);
		}
		next_ = st;
	}

private:
	strict_base * next_;
};

template<
	class Strict, class Allocator = std::allocator<Strict>
>
class strict
	: public strict_base
{
	typedef typename Allocator::template rebind<Strict>::other strict_alloc;
public:
	strict()
		:strict_base()
	{ }

	strict(strict & st)
		:strict_base(st)
	{ }

	virtual strict_base * clone()const
	{
		strict_alloc alloc;
		strict * st = alloc.allocate(1);
		if (!st)
			throw std::bad_alloc();
		new(st) Strict((Strict &)*this);
		return st;
	}

	virtual void destroy()
	{
		strict_alloc alloc;
		alloc.destroy((Strict *)this);
		alloc.deallocate((Strict *)this, 1);
	}
};

template<
	const char * (Name)(),
	class T,
	bool Key = false,
	bool Idx = false,
	bool Unique = false
>
struct field;

template<
	const char * (Name)(),
	class T,
	bool Key,
	bool Idx,
	bool Unique
>
struct field_base
	: public named_field<Name, T>
{
	enum { IsKey = Key };
	enum { IsIdx = Idx };
	enum { IsStr = false };
	enum { IsUnique = Unique };

	typedef T value_type;

	field_base()
		: strict_(0)
		, inited_(0)
	{ } 

	~field_base()
	{
		if (strict_)
		{
			strict_->destroy();
			strict_ = 0;
		}
	}

	const strict_base * get_strict()const
	{
		return strict_;
	}

	strict_base * get_strict()
	{
		return strict_;
	}

	void set_strict(const strict_base * st)
	{
		strict_base * p = st->clone();
		p->next(strict_);
		strict_ = p;
	}

	void set(const T & t)
	{
		this->value = t;
		inited_ = true;
	}
	
	bool inited()const
	{
		return inited_;
	}

	void inited(bool i)
	{
		inited_ = i;
	}

private:
	strict_base * strict_;
	bool inited_;
};

template<
	const char * (Name)(),
	class T,
	bool Key,
	bool Idx,
	bool Unique
>
struct field
	: public field_base<Name, T, Key, Idx, Unique>
{
};

template<
	class _Elem,
	class _Traits,
	class _Ax,
	const char * (Name)(),
	bool Key,
	bool Idx,
	bool Unique
>
struct field<Name, std::basic_string<_Elem, _Traits, _Ax>, Key, Idx, Unique>
	: public field_base<Name, std::basic_string<_Elem, _Traits, _Ax>, Key, Idx, Unique>
{
	enum { IsStr = true };

	field()
		: size_(0)
	{ }

	size_t max_size()const
	{
		return size_;
	}

	void max_size(size_t size)
	{
		size_ = size;
	}

private:
	size_t size_;
	
};

template<
	class T, class Strict, class Allocator = std::allocator<Strict>
>
class arith_strict
	: public strict<Strict, Allocator>
{
public:
	arith_strict(const T & t)
		: value_(t)
	{ }

	arith_strict(const arith_strict & st)
		: value_(st.value_)
	{
	}

protected:
	T value_;
};

template<
	class T, class Allocator = std::allocator<strict_base>
>
class equal_strict
	: public arith_strict<T, equal_strict<T, Allocator>, Allocator>
{
	typedef arith_strict<T, equal_strict<T, Allocator>, Allocator> base;
public:
	equal_strict(const T & t)
		:base(t)
	{ }

	equal_strict(equal_strict & es)
		:base(es)
	{ }

	virtual unsigned to_string(char * to)const
	{
		sprintf(to, "%s", "=");
		utilib::to_string(this->value_, to + 1);
		return 1;
	}
};

template<
	class T, class Allocator = std::allocator<strict_base>
>
class notequal_strict
	: public arith_strict<T, notequal_strict<T, Allocator>, Allocator>
{
	typedef arith_strict<T, notequal_strict<T, Allocator>, Allocator> base;
public:
	notequal_strict(const T & t)
		:base(t)
	{ }

	notequal_strict(notequal_strict & es)
		:base(es)
	{ }

	virtual unsigned to_string(char * to)const
	{
		sprintf(to, "%s", "!=");
		utilib::to_string(this->value_, to + 2);
		return 2;
	}
};

template<
	class T, class Allocator = std::allocator<strict_base>
>
class order_strict 
	: public arith_strict<T, order_strict<T, Allocator>, Allocator>
{
	typedef arith_strict<T, order_strict<T, Allocator>, Allocator> base;
public:
	order_strict(const T & t)
		: base(t) 
	{ }

	order_strict(order_strict & os)
		: base(os)
	{ }

	virtual unsigned to_string(char * to)const
	{
		sprintf(to, " order by %s desc", this->value_.c_str());
		return 10 + this->value_.size();
	}
};

template<
	class T, class Allocator = std::allocator<strict_base>
>
class greater_strict
	: public arith_strict<T, greater_strict<T, Allocator>, Allocator>
{
	typedef arith_strict<T, greater_strict<T, Allocator>, Allocator> base;
public:
	greater_strict(const T & t)
		:base(t)
	{ }

	greater_strict(greater_strict & es)
		:base(es)
	{ }

	virtual unsigned to_string(char * to)const
	{
		sprintf(to, "%s", ">");
		utilib::to_string(this->value_, to + 1);
		return 1;
	}
};

template<
	class T, class Allocator = std::allocator<strict_base>
>
class greater_equal_strict
	: public arith_strict<T, greater_equal_strict<T, Allocator>, Allocator>
{
	typedef arith_strict<T, greater_equal_strict<T, Allocator>, Allocator> base;
public:
	greater_equal_strict(const T & t)
		:base(t)
	{ }

	greater_equal_strict(greater_equal_strict & es)
		:base(es)
	{ }

	virtual unsigned to_string(char * to)const
	{
		sprintf(to, "%s", ">=");
		utilib::to_string(this->value_, to + 2);
		return 1;
	}
};

/*template<
	class T, class Allocator = std::allocator<strict_base>
>
class strict_not_equal
{
public:
	virtual const char strict(MYSQL * mysql)const
	{
		if (check_string<T>::Result)
		{
			const std::string value_string = value2string(values_);
			char buf[1024];
			mysql_real_escape_string(mysql, buf,  value_string.c_str(), value_string.size());
			return std::string("!=") + buf;				
		}
		else
		{
			return std::string("!=") + value2string(values_);
		}
	}

protected:
	T value_;
};

template<class T>
class strict_greater
{
public:
	virtual const char strict(MYSQL * mysql)const
	{
		if (check_string<T>::Result)
		{
			const std::string value_string = value2string(values_);
			char buf[1024];
			mysql_real_escape_string(mysql, buf,  value_string.c_str(), value_string.size());
			return std::string(">") + buf;				
		}
		else
		{
			return std::string(">") + value2string(values_);
		}
	}

protected:
	T value_;
};

template<class T>
class strict_less
{
public:
	virtual const char strict(MYSQL * mysql)const
	{
		if (check_string<T>::Result)
		{
			const std::string value_string = value2string(values_);
			char buf[1024];
			mysql_real_escape_string(mysql, buf,  value_string.c_str(), value_string.size());
			return std::string("<") + buf;				
		}
		else
		{
			return std::string("<") + value2string(values_);
		}
	}

protected:
	T value_;
};

template<class T>
class strict_greater_equal
{
public:
	virtual const char strict(MYSQL * mysql)const
	{
		if (check_string<T>::Result)
		{
			const std::string value_string = value2string(values_);
			char buf[1024];
			mysql_real_escape_string(mysql, buf,  value_string.c_str(), value_string.size());
			return std::string(">=") + buf;				
		}
		else
		{
			return std::string(">=") + value2string(values_);
		}
	}

protected:
	T value_;
};

template<class T>
class strict_less_equal
{
public:
	virtual const char strict(MYSQL * mysql)const
	{
		if (check_string<T>::Result)
		{
			const std::string value_string = value2string(values_);
			char buf[1024];
			mysql_real_escape_string(mysql, buf,  value_string.c_str(), value_string.size());
			return std::string("<=") + buf;				
		}
		else
		{
			return std::string("<=") + value2string(values_);
		}
	}

protected:
	T value_;
};*/

__SERVICE_SPACE_END_NS__

#endif
