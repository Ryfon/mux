﻿/********************************************************************
	created:	2008/06/12
	created:	12:6:2008   14:05
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\DBXMLResultConverter.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	DBXMLResultConverter
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_DB_XML_RESULT_CONVERTER_H__
#define __SERVICE_SPACE_DB_XML_RESULT_CONVERTER_H__

#pragma once

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/ServiceTask.h"
#include "DBQueryParser.h"

__SERVICE_SPACE_BEGIN_NS__


class ACE_Svc_Export DBXMLResultConverter
	: public ServiceTask
{
public:
	virtual int init(int argc, ACE_TCHAR * argv[]);

protected:
	virtual int handle_task_message(ACE_Message_Block * message);	

};

__SERVICE_SPACE_END_NS__

#endif
