﻿#define ACE_BUILD_SVC_DLL

#include "DBXMLResultConverter.h"
#include "../ServiceSpaceLib/ObjectMessageBlock.h"
#include "../ServiceSpaceLib/utilib/stringN.h"

__SERVICE_SPACE_BEGIN_NS__


int DBXMLResultConverter::init(int argc, ACE_TCHAR *argv[])
{
	int err = ServiceTask::init(argc, argv);
	if (-1 == err)
		return -1;
	err = this->RegisterTask();
	return err;
}



int DBXMLResultConverter::handle_task_message(ACE_Message_Block * message)
{
	int err = 0;
#ifdef WIN32
	ObjectMessageBlock<std::pair<db_result_type, tcp_session> > * result = 
		dynamic_cast<ObjectMessageBlock<std::pair<db_result_type, tcp_session> > *>(message);
#else
	ObjectMessageBlock<std::pair<db_result_type, tcp_session> > * result = 
		(ObjectMessageBlock<std::pair<db_result_type, tcp_session> > *)message;
#endif
	std::pair<db_result_type, tcp_session> & result_session = result->obj();
	utilib::stringN<64 * 1024> xml_result;
	db_result_type & query_result = result_session.first;
	std::vector<db_result_type::row_type>::const_iterator it = query_result.result_rows.begin();
	for(; it != query_result.result_rows.end(); ++it)
	{
		db_result_type::row_type::const_iterator it2 = it->begin();
		for(; it2 != it->end(); ++it2)
		{
			ACE_DEBUG((LM_DEBUG, ACE_TEXT("%s:%s\n"), it2->first.c_str(), it2->second.c_str()));
		}
	}
	return err;
}


__SERVICE_SPACE_END_NS__
