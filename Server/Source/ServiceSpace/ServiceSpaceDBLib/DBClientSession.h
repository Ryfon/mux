﻿/********************************************************************
	created:	2008/07/16
	created:	16:7:2008   11:01
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceDBLib\DBClientSession.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceDBLib
	file base:	DBClientSession
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_DBCLIENT_SESSION_H__
#define __SERVICE_SPACE_DBCLIENT_SESSION_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/TcpSession.h"
#include "../ServiceSpaceLib/utilib/obj_pool.h"

__SERVICE_SPACE_BEGIN_NS__

class DBClientSession;

typedef utilib::obj_pool<DBClientSession, 1000> db_session_pool;

typedef utilib::smart_ptr<DBClientSession, utilib::destroyer::deallocator_traits<db_session_pool>::deallocator, 
	utilib::null_lock, db_session_pool> db_session;

class  DBClientSession
	: public TcpSession
{
public:
protected:
private:
};

__SERVICE_SPACE_END_NS__

#endif
