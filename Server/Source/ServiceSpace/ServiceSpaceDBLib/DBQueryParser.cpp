﻿#define ACE_BUILD_SVC_DLL

#include "../ServiceSpaceLib/utilib/parse.h"
#include "../ServiceSpaceLib/MessageBlock.h"
#include "../ServiceSpaceLib/ObjectMessageBlock.h"
#include "../ServiceSpaceLib/GetOpts.h"
#include "../ServiceSpaceLib/TcpService.h"
#include "DBQueryParser.h"
#include <utility>

__SERVICE_SPACE_BEGIN_NS__

template<class _DBQueryParser>
struct parser_pool
{
	typedef utilib::block_pool<sizeof(_DBQueryParser), 5> pool;
	static pool pool_;
};

template<class _DBQueryParser>
typename parser_pool<_DBQueryParser>::pool parser_pool<_DBQueryParser>::pool_;

DBQueryParser::DBQueryParser()
{

}


DBQueryParser::~DBQueryParser()
{

}

int DBQueryParser::init(int argc, ACE_TCHAR * argv[])
{
	GetOpts<ACE_TCHAR> opts(argc, argv);

	int err = ServiceTask::init(argc, argv);
	if (-1 == err)
		return -1;

	const ACE_TCHAR * options = ACE_TEXT(":");
	ACE_Get_Opt get_opt(argc, (ACE_TCHAR **)opts.argv, options);
	int c;
	while((c = get_opt()) != EOF)
	{
		;
	}

	err = this->RegisterTask();

	return err;
}



int DBQueryParser::handle_task_message(ACE_Message_Block * message)
{
#ifdef WIN32
	ObjectMessageBlock<TcpService::Tcp_Notify> * p = dynamic_cast<ObjectMessageBlock<TcpService::Tcp_Notify> *>(message);
#else
	ObjectMessageBlock<TcpService::Tcp_Notify> * p = (ObjectMessageBlock<TcpService::Tcp_Notify> *)message;
#endif
	TcpService::Tcp_Notify & notify = p->obj();
	if (notify.flag != TcpService::Tcp_Notify::TCP_RECEIVED)
	{
		message->release();
		return 0;
	}

	ObjectMessageBlock<std::pair<db_query_type, tcp_session> > * query = new ObjectMessageBlock<std::pair<db_query_type, tcp_session> >();
	std::pair<db_query_type, tcp_session> & query_addr = query->obj();
	db_query_type & qt = query_addr.first;
	int err = utilib::parse_kv(qt, notify.message->rd_ptr(), (int)notify.message->length());
	if (-1 == err)
	{
		ACE_DEBUG((LM_ERROR, "query message[%s] error\n", utilib::stringN<512>(message->rd_ptr(), message->length()).c_str()));
	}
	else
	{
		query_addr.second = notify.session;
		this->put_next(query);
	}

	message->release();
	return 0;
}



void * DBQueryParser::operator new(size_t)
{
	return parser_pool<DBQueryParser>::pool_.allocate();
}


void DBQueryParser::operator delete(void *p)
{
	parser_pool<DBQueryParser>::pool_.deallocate(p);
}

extern "C" ACE_Svc_Export DBQueryParser * CreateQueryParser()
{
	return new DBQueryParser();
}

ACE_SVC_FACTORY_DECLARE(DBQueryParser);
ACE_SVC_FACTORY_DEFINE(DBQueryParser);



__SERVICE_SPACE_END_NS__
