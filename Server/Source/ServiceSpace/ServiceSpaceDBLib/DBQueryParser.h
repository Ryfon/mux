﻿/********************************************************************
	created:	2008/06/16
	created:	16:6:2008   9:59
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\DBHandler.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	DBHandler
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_DB_HANDLER_H__
#define __SERVICE_SPACE_DB_HANDLER_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/SessionHandler.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "../ServiceSpaceLib/utilib/block_pool.h"
#include <map>
#include <vector>

__SERVICE_SPACE_BEGIN_NS__

typedef utilib::stringN<1024> db_query_key_type;
typedef db_query_key_type db_query_value_type;
typedef db_query_key_type db_result_key_type;
typedef utilib::stringN<1024 * 64> db_result_value_type;
typedef std::map<db_query_key_type, db_query_value_type> db_query_type;

struct db_result_type
{
	typedef std::map<db_result_key_type, db_result_value_type> row_type;
	int affected;
	int error;
	void * sql_data;
	std::vector<row_type> result_rows;
};


class ACE_Svc_Export DBQueryParser
	: public ServiceTask
{
public:
	DBQueryParser();

	~DBQueryParser();

	virtual int init(int argc, ACE_TCHAR * argv[]);

	static void * operator new(size_t size);

	static void operator delete(void * p);

private:
	virtual int handle_task_message(ACE_Message_Block * message);
};


__SERVICE_SPACE_END_NS__

#endif
