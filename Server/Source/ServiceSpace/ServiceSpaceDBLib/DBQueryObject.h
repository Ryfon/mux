﻿/********************************************************************
	created:	2008/06/12
	created:	12:6:2008   16:59
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\DBQueryObject.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	DBQueryObject
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __SERVICE_SPACE_DB_QUERY_OBJECT_H__
#define __SERVICE_SPACE_DB_QUERY_OBJECT_H__

#pragma once

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "../ServiceSpaceLib/utilib/null_type.h"
#include "../ServiceSpaceLib/utilib/tuple.h"
#include "../ServiceSpaceLib/utilib/tuple_foreach.h"
#include "../ServiceSpaceLib/utilib/type_traits.h"
#include "../ServiceSpaceLib/utilib/type_check.h"
#include "../ServiceSpaceLib/utilib/assign.h"
#include "../ServiceSpaceLib/utilib/parse.h"
#include "../ServiceSpaceLib/utilib/type2string.h"
#include "../ServiceSpaceLib/utilib/int2type.h"
#include "DBQueryParser.h"
#include "DBMysqlHandler.h"
#include <ace/Message_Block.h>

__SERVICE_SPACE_BEGIN_NS__

const size_t MAX_LEN_FIELD_NAME = 64;
const size_t MAX_LEN_SQL = 4 * 1024;
const size_t MAX_LEN_QUERY_OBJECT_NAME = 64;

typedef utilib::stringN<MAX_LEN_QUERY_OBJECT_NAME, char> name_string;
typedef utilib::stringN<MAX_LEN_SQL, char> attri_string;

template<class T>
struct _is_string
{
	enum { Result = false };
};

template<size_t N, typename Ch>
struct _is_string<utilib::stringN<N, Ch> >
{
	enum { Result = true };
};

template<>
struct _is_string<std::string>
{
	enum { Result = true };
};

template<class T>
struct Field
{
	typedef T value_type;
	typedef utilib::stringN<MAX_LEN_FIELD_NAME, char> string_type;

	Field(const string_type & name, const value_type & val)
		:val_(val), name_(name), set_(true)
	{

	}

	Field(const string_type & name) 
		: name_(name)
	{ 
		val_ = value_type();
		set_ = false; 
	}

	Field()
	{
		val_ = value_type();
		set_ = false;
	}

	const value_type & value()const
	{
		return val_;
	}

	bool is_set()const
	{
		return set_;
	}

	const string_type & name()const
	{
		return name_;
	}

	void name(const string_type & _name)
	{
		name_ = _name;
	}

	void value(const value_type & val)
	{
		val_ = val;
		set_ = true;
	}

	value_type val_;
	bool set_;
	string_type name_;
};


//typedef Field<utilib::null_type> NullField;
typedef utilib::null_type NullField;

struct _get_attris
{
	_get_attris(attri_string & attri) 
		: attri_(attri) 
	{ }

	template<class FieldType>
	int operator()(FieldType & field)
	{
		if (attri_.size())
			attri_ << ',';
		attri_ << field.name();
		return 0;
	}

	attri_string & attri_;
};

struct _get_where
{
	_get_where(attri_string & attri, bool _sep_and = true, bool _is_update = false)
		: attri_(attri), sep_and_(_sep_and), is_update_(_is_update)
	{ }

	template<class FieldType>
	int operator()(FieldType & field)
	{
		if (attri_.size() && field.is_set() && sep_and_)
			attri_ << " and ";
		//else
		//	attri_ << ' ';

		if (field.is_set())
		{
			attri_string tmp;
			utilib::type2string(field.value(), tmp);
			if (tmp.size())
			{
				if (_is_string<typename FieldType::value_type>::Result)
				{
					MYSQL * mysql = &DBMysqlHandlerT<TcpSession>::mysql_;
					char buf[MAX_LEN_SQL * 2];
					mysql_real_escape_string(mysql, buf, tmp.c_str(), tmp.size());
					attri_ << ' ' << field.name() << "='" << buf << "'";
				}
				else
					attri_ << " " << field.name() << "=" << tmp;
				
			}
			else if (is_update_)
			{
				attri_ << ' ' << field.name() << "=" << "\"\"";
			}

			//utilib::type2string(field.value(), attri_);
		}
		return 0;
	}

	attri_string & attri_;
	bool sep_and_;
	bool is_update_;
};

template<bool TypeCheck>
struct _check_assign
{
	template<class T1, class T2>
	static void assign(T1 & dst, const T2 & src) { }
};

template<>
struct _check_assign<true>
{
	template<class FieldType, class T2>
	static void assign(FieldType & dst, const T2 & src) 
	{
		utilib::assign(dst.val_, src);
		dst.set_ = true;
	}
};

template<class T>
struct _set_attri
{
	typedef typename utilib::type_traits<T>::value_type value_type;

	_set_attri(const name_string & attri, const value_type & t)
		: attri_(attri)
	{
		utilib::assign(t_, t);
	}

	template<class FieldType>
	int operator()(FieldType & field)
	{
		if (attri_ == field.name())
		{
			_check_assign<utilib::type_check<typename FieldType::value_type, value_type>::Result>::assign(field, t_);
			return 1;
		}
		return 0;
	}

	const name_string & attri_;
	value_type  t_;
};


template<class Field1 = NullField, class Field2 = NullField,class Field3 = NullField,class Field4 = NullField,class Field5 = NullField,
		 class Field6 = NullField, class Field7 = NullField,class Field8 = NullField,class Field9 = NullField,class Field10 = NullField,
		 class Field11 = NullField,class Field12 = NullField,class Field13 = NullField,class Field14 = NullField,class Field15 = NullField,
		 class Field16 = NullField,class Field17 = NullField,class Field18 = NullField,class Field19 = NullField,class Field20 = NullField,
		 class Field21 = NullField,class Field22 = NullField,class Field23 = NullField,class Field24 = NullField,class Field25 = NullField,
		 class Field26 = NullField,class Field27 = NullField,class Field28 = NullField,class Field29 = NullField,class Field30 = NullField,
		 class Field31 = NullField,class Field32 = NullField,class Field33 = NullField,class Field34 = NullField,class Field35 = NullField,
		 class Field36 = NullField,class Field37 = NullField,class Field38 = NullField,class Field39 = NullField,class Field40 = NullField
>
class QueryObject;

template<class Field1, class Field2,class Field3,class Field4,class Field5,
		 class Field6, class Field7,class Field8,class Field9,class Field10,
		 class Field11,class Field12,class Field13,class Field14,class Field15,
		 class Field16,class Field17,class Field18,class Field19,class Field20,
		 class Field21,class Field22,class Field23,class Field24,class Field25,
		 class Field26,class Field27,class Field28,class Field29 ,class Field30,
		 class Field31,class Field32,class Field33,class Field34,class Field35,
		 class Field36,class Field37,class Field38,class Field39,class Field40
>
class QueryObject
	: public utilib::tuple<Field1, Field2, Field3, Field4, Field5, Field6, Field7, Field8, Field9, Field10,
						   Field11, Field12, Field13, Field14, Field15, Field16, Field17, Field18, Field19, Field20,
						   Field21, Field22, Field23, Field24, Field25, Field26, Field27, Field28, Field29, Field30,
						   Field31, Field32, Field33, Field34, Field35, Field36, Field37, Field38, Field39, Field40
	>
{
	typedef utilib::tuple<Field1, Field2, Field3, Field4, Field5, Field6, Field7, Field8, Field9, Field10,
		Field11, Field12, Field13, Field14, Field15, Field16, Field17, Field18, Field19, Field20,
		Field21, Field22, Field23, Field24, Field25, Field26, Field27, Field28, Field29, Field30,
		Field31, Field32, Field33, Field34, Field35, Field36, Field37, Field38, Field39, Field40> base;

	typedef base tuple_type;

public:

	QueryObject(const name_string & name) : name_(name),data_(0) { }

	void get_attri_list(attri_string & attri)
	{
		_get_attris get(attri);
		utilib::do_tuple_foreach((base &)*this, get);
	}

	typename tuple_type::rest_tuple & operator<<(const Field1 & field)
	{
		tuple_type & tu = (tuple_type &)*this;
		return tu << field;
	}

	template<class T>
	void set_attri(const name_string & attri, const T & t)
	{
		_set_attri<T> set(attri, t);
		utilib::do_tuple_foreach((base &)*this, set);
	}

	void make_select_sql(utilib::stringN<MAX_LEN_SQL> & sql)
	{
		sql << "cmd=select ";
		attri_string attri;
		get_attri_list(attri);
		sql << "fields=" << attri << " obj=" << name_;
		attri.clean();
		_get_where where(attri);
		utilib::do_tuple_foreach((base &)*this, where);
		if (attri.size())
			sql << " where=\"" << attri << "\""  << " sql_data=" << get_data_str();;
	}

	void make_delete_sql(utilib::stringN<MAX_LEN_SQL> & sql)
	{
		sql << "cmd=delete obj=" << name_;
		attri_string attri;
		_get_where where(attri);
		utilib::do_tuple_foreach((base &)*this, where);
		if (attri.size())
			sql << " where=\"" << attri << "\"" << " sql_data=" << get_data_str();
		else
			sql.clean();
	}

	void make_update_sql(utilib::stringN<MAX_LEN_SQL> & sql)
	{
		sql << "cmd=update obj=" << name_;
		attri_string attri;
		_get_where where(attri, false, true);
		utilib::do_tuple_foreach((base &)*this, where);
		sql  << " " << attri << " sql_data=" << get_data_str();
	}

	void make_insert_sql(utilib::stringN<MAX_LEN_SQL> & sql)
	{
		sql << "cmd=insert obj=" << name_;
		attri_string attri;
		_get_where where(attri, false);
		utilib::do_tuple_foreach((base &)*this, where);
		if (attri.size())
			sql << " " << attri << " sql_data=" << get_data_str();
		else
			sql.clean();
	}

	void set_obj_data(void * data)
	{
		data_ = data;
	}

private:
	utilib::stringN<16> get_data_str()const
	{
		utilib::stringN<16> str;
		unsigned n = (unsigned)data_;
		type2string(n, str);
		return str;
	}

	name_string name_; 
	void * data_;
};


typedef QueryObject<NullField, NullField, NullField, NullField, NullField,NullField, NullField, NullField, NullField, NullField,
					NullField, NullField, NullField, NullField, NullField,NullField, NullField, NullField, NullField, NullField,
					NullField, NullField, NullField, NullField, NullField,NullField, NullField, NullField, NullField, NullField,
					NullField, NullField, NullField, NullField, NullField,NullField, NullField, NullField, NullField, NullField
> NullQueryObject;


struct QueryResultBase
{
	enum { STRUCT_XML, STRUCT_LUA, STRUCT_PYTHON, STRUCT_MAP };
	unsigned ind;
	int type;

	QueryResultBase(int _type)
		: type(_type)
	{

	}

	virtual ~QueryResultBase()
	{

	}

	virtual void parse(const ACE_Message_Block *) = 0;
};

struct QueryMapResult
	: public QueryResultBase
{
	QueryMapResult()
		: QueryResultBase(STRUCT_MAP){ }

	virtual void parse(const ACE_Message_Block *) { }

	db_result_type result;
};

struct QueryXmlResult
	: public QueryResultBase
{
	QueryXmlResult()
		: QueryResultBase(STRUCT_XML){ }

	virtual void parse(const ACE_Message_Block * ) { }
};

struct QueryLuaResult
	: public QueryResultBase
{
	QueryLuaResult()
		: QueryResultBase(STRUCT_LUA){ }

	virtual void parse(const ACE_Message_Block *) { }
};

struct QueryPythonResult
	: public QueryResultBase
{
	QueryPythonResult()
		: QueryResultBase(STRUCT_PYTHON){ }

	virtual void parse(const ACE_Message_Block *) { }
};


template<class Field1 = NullQueryObject,class Field2 = NullQueryObject,class Field3 = NullQueryObject,class Field4 = NullQueryObject,
		 class Field5 = NullQueryObject,class Field6 = NullQueryObject,class Field7 = NullQueryObject,class Field8 = NullQueryObject,
		 class Field9 = NullQueryObject,class Field10 = NullQueryObject,class Field11 = NullQueryObject,class Field12 = NullQueryObject,
		 class Field13 = NullQueryObject,class Field14 = NullQueryObject,class Field15 = NullQueryObject,class Field16 = NullQueryObject,
		 class Field17 = NullQueryObject,class Field18 = NullQueryObject,class Field19 = NullQueryObject,class Field20 = NullQueryObject,
		 class Field21 = NullQueryObject,class Field22 = NullQueryObject,class Field23 = NullQueryObject,class Field24 = NullQueryObject,
		 class Field25 = NullQueryObject,class Field26 = NullQueryObject,class Field27 = NullQueryObject,class Field28 = NullQueryObject,
		 class Field29 = NullQueryObject,class Field30 = NullQueryObject,class Field31 = NullQueryObject,class Field32 = NullQueryObject,
		 class Field33 = NullQueryObject,class Field34 = NullQueryObject,class Field35 = NullQueryObject,class Field36 = NullQueryObject,
		 class Field37 = NullQueryObject,class Field38 = NullQueryObject,class Field39 = NullQueryObject,class Field40 = NullQueryObject
>
class Query;




__SERVICE_SPACE_END_NS__

#endif
