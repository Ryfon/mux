﻿#include "DBMysqlHandler.h"
#include "DBQueryObject.h"
#include "../ServiceSpaceLib/GetOpts.h"
#include "../ServiceSpaceLib/utilib/split.h"
#include "../ServiceSpaceLib/utilib/vectorN.h"
#include <functional>
#include <strstream>

__SERVICE_SPACE_BEGIN_NS__


template<class Session>
void DBMysqlHandlerT<Session>::connect_mysql(MYSQL * *mysql)
{
	if (!*mysql)
	{
		*mysql = mysql_init(*mysql);
		mysql_real_connect(*mysql, connString_.host.c_str(), connString_.user.c_str(), connString_.passwrd.c_str(),
			connString_.dbname.c_str(), connString_.port, 0, 0);
	}
}

template<class Session>
int DBMysqlHandlerT<Session>::init(int argc, ACE_TCHAR *argv[])
{
	GetOpts<ACE_TCHAR> opts(argc, argv);
	int err = ServiceTask::init(argc, argv);
	if (-1 == err)
	{
		return -1;
	}

	const ACE_TCHAR * options = ACE_TEXT(":h:u:p:n:o:");
	ACE_Get_Opt get_opt(argc, (ACE_TCHAR **)opts.argv, options);
	int c;
	while ((c = get_opt()) != EOF)
	{
		switch(c)
		{
		case 'h':
			connString_.host = get_opt.optarg;
			break;
		case 'u':
			connString_.user = get_opt.optarg;
			break;
		case 'p':
			connString_.passwrd = get_opt.optarg;
			break;
		case 'n':
			connString_.dbname = get_opt.optarg;
			break;
		case 'o':
			connString_.port = atoi(get_opt.optarg ? get_opt.optarg : "3306");
			break;
		}
	}

	err = this->RegisterTask();

	return err;
}


template<class Session>
int DBMysqlHandlerT<Session>::open(void *args )
{
	int err = ServiceTask::open(args);
	if (-1 == err)
		return -1;

	mysql_init(&mysql_);
	mysql_real_connect(&mysql_, connString_.host.c_str(), connString_.user.c_str(), connString_.passwrd.c_str(),
		connString_.dbname.c_str(), connString_.port, 0, 0);

	return 0;

}


template<class Session>
int DBMysqlHandlerT<Session>::handle_task_message(ACE_Message_Block * message)
{
	ObjectMessageBlock<std::pair<db_result_type, session_ptr> > * result_message = new ObjectMessageBlock<std::pair<db_result_type, session_ptr> >(); 
	std::pair<db_result_type, session_ptr> & result_session = result_message->obj();
	int err = 0;
	try{
#ifdef WIN32
		ObjectMessageBlock<std::pair<db_query_type, session_ptr> > * p = dynamic_cast<ObjectMessageBlock<std::pair<db_query_type,session_ptr> > *>(message);
#else
		ObjectMessageBlock<std::pair<db_query_type, session_ptr> > * p = (ObjectMessageBlock<std::pair<db_query_type,session_ptr> > *)message;
#endif
		std::pair<db_query_type, session_ptr> & query_session = p->obj();
		db_query_type & query = query_session.first;
		db_query_type::const_iterator it = query.find("cmd");
		if (it == query.end())
		{
			ACE_DEBUG((LM_INFO, ACE_TEXT("mysql service[%t] get a query without cmd\n")));
		}

		db_result_type & result = result_session.first;
		std::strstream stream;
		stream << query["sql_data"].c_str();
		unsigned sql_data = 0;
		stream >> sql_data;
		result.sql_data = (void *)sql_data;
		result.error = 0;
		int err = 0;
		if (strcmp("select", it->second.c_str()) == 0)
			err = handle_select(result, query);
		else if (strcmp("insert", it->second.c_str()) == 0)
			err = handle_insert(result, query);
		else if (strcmp("delete", it->second.c_str()) == 0)
			err = handle_delete(result, query);
		else if (strcmp("update", it->second.c_str()) == 0)
			err = handle_update(result, query);
		else
		{
			ACE_DEBUG((LM_INFO, ACE_TEXT("mysql service[%t] get a query with cmd which is not used\n")));
			err = -1;
		}

		if(!err)
		{
			result_session.second = query_session.second; 
		}

	}catch(...)
	{
		ACE_DEBUG((LM_INFO, ACE_TEXT("mysql service[%t] exception occurred\n")));
		err = -1;
	}

	message->release();

	if (!err)
		err = this->put_next(result_message);
	
	return err;
}


template<class Session>
int DBMysqlHandlerT<Session>::handle_select(db_result_type & result, db_query_type & query)
{
	utilib::stringN<512> sql;
	db_query_type::const_iterator obj = query.find("obj");
	if (obj == query.end())
	{
		return -1;
	}
	db_query_type::const_iterator fields = query.find("fields");
	if (fields == query.end())
	{
		return -1;
	}

	db_query_type::const_iterator where = query.find("where");

	sql << "select " << fields->second << " from " << obj->second;
	if (where != query.end())
	{
		sql << " where " << where->second;
	}

	connect_sql_functor connector(this, &DBMysqlHandlerT::connect_mysql);
	MYSQL * * pMysql = mysql_pool_.allocate(connector);
	MYSQL * mysql = *pMysql;
	if (mysql_query(mysql, sql.c_str()))
	{
		result.error = mysql_errno(mysql);
		ACE_DEBUG((LM_ERROR, ACE_TEXT("mysql[%s] error:%d desc:%s\n"), sql.c_str(), result.error, mysql_error(mysql)));
	}
	else
	{
		MYSQL_RES * mysql_result = 0;
		unsigned int num_fields = 0;
		unsigned int num_rows = 0;
		mysql_result = mysql_store_result(mysql);
		if (mysql_result)
		{
			num_fields = mysql_num_fields(mysql_result);
			num_rows = (unsigned)mysql_affected_rows(mysql);
		}
		else
		{
			if(mysql_field_count(mysql) == 0)
				num_rows = (unsigned)mysql_affected_rows(mysql);
			else 
			{
				result.error = mysql_errno(mysql);
				ACE_DEBUG((LM_ERROR, ACE_TEXT("mysql[%s] error:%d\n"), sql.c_str(), result.error));
			}
		}

		result.affected = num_rows;
		if (num_fields)
		{
			// TODO: max fields num definition
			utilib::vectorN<db_query_key_type, 128> vecFields;
			utilib::split(vecFields, fields->second.c_str(), fields->second.c_str() + fields->second.size(), ',');
			if (vecFields.size() != num_fields)
			{
				ACE_ERROR((LM_ERROR, "mysql cannot get enough fields\n"));
			}
			else
			{
				MYSQL_ROW row = mysql_fetch_row(mysql_result);
				for (int i = 0; row ; row = mysql_fetch_row(mysql_result), ++i)
				{
					std::map<db_result_key_type, db_result_value_type> row_map;
					for (size_t j = 0; j < num_fields; ++j)
					{
						row_map[vecFields[j].c_str()] = row[j] ? row[j] : "";
					}
					result.result_rows.push_back(row_map);
				} // for
			}
		}
		mysql_free_result(mysql_result);
	}
	mysql_pool_.deallocate(pMysql);
	return 0;
}


template<class Session>
int DBMysqlHandlerT<Session>::handle_update(db_result_type & result, db_query_type & query)
{
	utilib::stringN<MAX_LEN_SQL> sql;
	db_query_type::const_iterator obj = query.find("obj");
	if (obj == query.end())
	{
		return -1;
	}
	sql << "update " << obj->second << " set ";
	db_query_type::const_iterator it = query.begin();
	utilib::stringN<MAX_LEN_SQL> tmp;
	for(; it != query.end(); ++it)
	{
		if (!(it->first == "obj") &&
			!(it->first == "where") &&
			!(it->first == "cmd") &&
			!(it->first == "sql_data"))
		{
			if (tmp.size())
				tmp << ",";
			if(it->second.size())
				tmp << it->first << "=" << it->second << " ";
			else
				tmp << it->first << "=''";
			
		}
	}
	sql << " " << tmp << " ";
	db_query_type::const_iterator where = query.find("where");
	if (where != query.end())
	{
		sql << " where " << where->second;
	}

	connect_sql_functor connector(this, &DBMysqlHandlerT::connect_mysql);
	MYSQL * * pMysql = mysql_pool_.allocate(connector);
	MYSQL * mysql = *pMysql;
	if (mysql_query(mysql, sql.c_str()))
	{
		result.error = mysql_errno(mysql);
		ACE_DEBUG((LM_ERROR, ACE_TEXT("mysql[%s] error:%d\n"), sql.c_str(), result.error));
	}
	else
	{
		MYSQL_RES * mysql_result = 0;
		mysql_result = mysql_store_result(mysql);
		result.affected = (unsigned)mysql_affected_rows(mysql);
		mysql_free_result(mysql_result);
	}
	mysql_pool_.deallocate(pMysql);
	return 0;
}


template<class Session>
int DBMysqlHandlerT<Session>::handle_delete(db_result_type & result, db_query_type & query)
{
	utilib::stringN<512> sql;
	db_query_type::const_iterator obj = query.find("obj");
	if (obj == query.end())
	{
		return -1;
	}
	sql << "delete from " << obj->second;
	db_query_type::const_iterator where = query.find("where");
	if (where != query.end())
	{
		sql << " where " << where->second;
	}

	connect_sql_functor connector(this, &DBMysqlHandlerT::connect_mysql);
	MYSQL * * pMysql = mysql_pool_.allocate(connector);
	MYSQL * mysql = *pMysql;
	if (mysql_query(mysql, sql.c_str()))
	{
		result.error = mysql_errno(mysql);
		ACE_DEBUG((LM_ERROR, ACE_TEXT("mysql[%s] error:%d\n"), sql.c_str(), result.error));
	}
	else
	{
		MYSQL_RES * mysql_result = 0;
		mysql_result = mysql_store_result(mysql);
		result.affected = (unsigned)mysql_affected_rows(mysql);
		mysql_free_result(mysql_result);
	}
	mysql_pool_.deallocate(pMysql);
	return 0;
}


template<class Session>
int DBMysqlHandlerT<Session>::handle_insert(db_result_type & result, db_query_type & query)
{
	utilib::stringN<512> sql;
	db_query_type::const_iterator obj = query.find("obj");
	if (obj == query.end())
	{
		return -1;
	}
	sql << "insert into " << obj->second << "(";
	
	utilib::stringN<512> values;
	db_query_type::const_iterator it = query.begin();
	for(; it != query.end(); ++it)
	{
		if (!(it->first == "obj") &&
			!(it->first == "cmd") &&
			!(it->first == "sql_data"))
		{
			if (values.size())
			{
				values << ",";
				sql << ",";
			}
			sql << it->first;
			values << it->second ;
		}
	}
	if (!values.size())
		return -1;
	sql << ") values(" << values << ")";

	connect_sql_functor connector(this, &DBMysqlHandlerT::connect_mysql);
	MYSQL * * pMysql = mysql_pool_.allocate(connector);
	MYSQL * mysql = *pMysql;
	if (mysql_query(mysql, sql.c_str()))
	{
		result.error = mysql_errno(mysql);
		ACE_DEBUG((LM_ERROR, ACE_TEXT("mysql[%s] error:%d\n"), sql.c_str(), result.error));
	}
	else
	{
		MYSQL_RES * mysql_result = 0;
		mysql_result = mysql_store_result(mysql);
		result.affected = (unsigned)mysql_affected_rows(mysql);
		mysql_free_result(mysql_result);
	}
	mysql_pool_.deallocate(pMysql);

	return 0;
}

template<class Session>
DBMysqlHandlerT<Session> * DBMysqlHandlerT<Session>::instance()
{
	static DBMysqlHandlerT<Session> inst;
	return &inst;
}

template<class Session>
MYSQL * DBMysqlHandlerT<Session>::get_mysql()
{
	return 0;
}


__SERVICE_SPACE_END_NS__
