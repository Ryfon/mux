﻿#ifndef __SERVICE_SPACE_DB2O_OBJECT_H__
#define __SERVICE_SPACE_DB2O_OBJECT_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "field.h"
#include <memory>
#include <map>

__SERVICE_SPACE_BEGIN_NS__

template<int DB>
class database;

template<
	const char * (*Name)(),
	class Field1 = null_type, 
	class Field2 = null_type, 
	class Field3 = null_type, 
	class Field4 = null_type, 
	class Field5 = null_type, 
	class Field6 = null_type,
	class Field7 = null_type,
	class Field8 = null_type,
	class Field9 = null_type,
	class Field10 = null_type,
	class Field11 = null_type,
	class Field12 = null_type,
	class Field13 = null_type,
	class Field14 = null_type,
	class Field15 = null_type,
	class Field16 = null_type,
	class Field17 = null_type,
	class Field18 = null_type,
	class Field19 = null_type,
	class Field20 = null_type
>
class object
	: public named_tuple<Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,Field9,Field10,
		Field11, Field12, Field13, Field14, Field15, Field16, Field17, Field18, Field19, Field20>
{
protected:
	~object()
	{
		if (next_)
		{
			next_->destroy();
			next_ = 0;
		}
	}

public:
	typedef named_tuple<Field1,Field2,Field3,Field4,Field5,Field6,Field7,Field8,Field9,Field10,
		Field11, Field12, Field13, Field14, Field15, Field16, Field17, Field18, Field19, Field20> tuple_type;

	object()
		: next_(0)
	{ }
	
	void next(object * next)
	{
		if (next_)
		{
			next_ ->destroy();
			next_ = 0;
		}
		next_ = next;
	}

	const object * next()const
	{
		return next_;
	}

	static const char * name()
	{
		return Name();
	}

	void destroy()
	{
		delete this;
	}

private:
	object * next_;
}; // end class 

__SERVICE_SPACE_END_NS__

#endif
