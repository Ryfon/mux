﻿#ifndef __SERVICE_SPACE_DB2O_SQL_STRING_H__
#define __SERVICE_SPACE_DB2O_SQL_STRING_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include <string>

__SERVICE_SPACE_BEGIN_NS__

template<class T>
struct type2sqlstring;
//{
//	static const char * type()
//	{
//		return utilib::type2string<T>::type();
//	}
//};

template<>
struct type2sqlstring<int>
{
	static const char * type()
	{
		return "int";
	}
};

template<>
struct type2sqlstring<unsigned>
{
	static const char * type()
	{
		return "int unsigned";
	}
};

template<>
struct type2sqlstring<short>
{
	static const char * type()
	{
		return "smallint";
	}
};

template<>
struct type2sqlstring<unsigned short>
{
	static const char * type()
	{
		return "smallint unsigned";
	}
};

template<>
struct type2sqlstring<char>
{
	static const char * type()
	{
		return "tinyint";
	}
};

template<>
struct type2sqlstring<unsigned char>
{
	static const char * type()
	{
		return "tinyint unsigned";
	}
};

template<>
struct type2sqlstring<long>
{
	static const char * type()
	{
		return "int";
	}
};

template<>
struct type2sqlstring<unsigned long>
{
	static const char * type()
	{
		return "int unsigned";
	}
};

template<>
struct type2sqlstring<long long>
{
	static const char * type()
	{
		return "bigint";
	}
};

template<>
struct type2sqlstring<unsigned long long>
{
	static const char * type()
	{
		return "bigint unsigned";
	}
};

template<>
struct type2sqlstring<float>
{
	static const char * type()
	{
		return "float";
	}
};

template<>
struct type2sqlstring<double>
{
	static const char * type()
	{
		return "double";
	}
};


template<>
struct type2sqlstring<std::string>
{
	static const char * type()
	{
		return "varchar";
	}
};

__SERVICE_SPACE_END_NS__

#endif
