﻿/********************************************************************
	created:	2008/06/25
	created:	25:6:2008   11:25
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\DBMysqlHandler.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	DBMysqlHandler
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_DBMYSQLHANDLER_H__
#define __SERVICE_SPACE_DBMYSQLHANDLER_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/ServiceTask.h"
#include "../ServiceSpaceLib/ObjectMessageBlock.h"
#include "../ServiceSpaceLib/utilib/obj_pool.h"
#include "../ServiceSpaceLib/utilib/functor.h"
#include "DBQueryParser.h"
#include <mysql.h>

#ifdef WIN32
#pragma comment(lib, "libmysql.lib")
#endif

__SERVICE_SPACE_BEGIN_NS__

struct DBMysqlConntionString
{
	utilib::stringN<32> host;
	utilib::stringN<32> user;
	utilib::stringN<32> passwrd;
	utilib::stringN<32> dbname;
	int port;

	DBMysqlConntionString(): port(3306) { }
};

template<class Session>
class DBMysqlHandlerT
	: public ServiceTask
{
public:
	typedef typename Session::session_ptr session_ptr;

	virtual int init(int argc, ACE_TCHAR *argv[]);

	virtual int open(void *args );

	DBMysqlHandlerT * instance();

	MYSQL * get_mysql();

protected:
	virtual int handle_task_message(ACE_Message_Block * message);

	int handle_select(db_result_type & result, db_query_type & query); 

	int handle_delete(db_result_type & result, db_query_type & query);

	int handle_insert(db_result_type & result, db_query_type & query);

	int handle_update(db_result_type & result, db_query_type & query);

	void connect_mysql(MYSQL * *mysql);

	typedef utilib::functor<void (DBMysqlHandlerT::*)(MYSQL * *)> connect_sql_functor;
	DBMysqlConntionString connString_;
	typedef utilib::obj_pool<MYSQL *, 32> mysql_pool;
	mysql_pool mysql_pool_;
public:
	static MYSQL mysql_;
};

template<class Session>
MYSQL DBMysqlHandlerT<Session>::mysql_;

__SERVICE_SPACE_END_NS__

#endif
