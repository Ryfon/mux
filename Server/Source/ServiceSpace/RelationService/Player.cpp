﻿#include "Player.h"
#include "SocialRelationManager.h"
#include <algorithm>

using namespace utilib;

__SERVICE_SPACE_BEGIN_NS__

utilib::basic_text_ostream & operator<< (utilib::basic_text_ostream & s, const Player::FriendInfo & info)
{
	s << info.uiid << info.degree << info.readTimeLine;
	return s;
}

utilib::basic_text_istream & operator>>(utilib::basic_text_istream & s, Player::FriendInfo & info)
{
	s >> info.uiid >> info.degree >> info.readTimeLine;
	return s;
}

utilib::basic_text_ostream & operator<< (utilib::basic_text_ostream & s, const Player::GuildInfo & info)
{
	s << info.guildId
	  << info.guildPos
	  << info.guildTitle
	  << info.joinTime
	  << info.exitTime
	  << info.forbid;
	return s;
}

utilib::basic_text_istream & operator>> (utilib::basic_text_istream & s, Player::GuildInfo & info)
{
	unsigned id = info.guildId;
	s >> info.guildId
	  >> info.guildPos
	  >> info.guildTitle
	  >> info.joinTime
	  >> info.exitTime
	  >> info.forbid;
	info.guildId = id;
	return s;
}

utilib::basic_text_ostream & operator<< (utilib::basic_text_ostream & s, const Player::EnemyInfo & info)
{
	s << info.uiid << info.locked << info.mapId << info.posX << info.posY << info.isKilled << info.time;
	return s;
}

utilib::basic_text_istream & operator>> (utilib::basic_text_istream & s, Player::EnemyInfo & info)
{
	s >> info.uiid >> info.locked >> info.mapId >> info.posX >> info.posY >> info.isKilled >> info.time;
	return s;
}

utilib::basic_text_ostream & operator<< (utilib::basic_text_ostream & s, const Player::GroupInfo & info)
{
	s << info.groupId << info.name << info.createdTime << info.lst;
	return s;
}

utilib::basic_text_istream & operator>> (utilib::basic_text_istream & s, Player::GroupInfo & info)
{
	s >> info.groupId >> info.name >> info.createdTime >> info.lst;
	return s;
}

utilib::basic_text_ostream & operator<< (utilib::basic_text_ostream & s, const Player & player)
{
	s << player.race_
	  << player.sex_
	  << player.level_
	  << player.icon_
	  << player.job_
	  << player.mapId_
	  << player.guildInfo_
	  << player.friends_
	  << player.blacks_
	  << player.enemys_
	  << player.uplineTime_
	  << player.offlineTime_
	  << player.fere_
	  << player.accUplineTimeDay_
	  << player.beginFriendGroupMsgCountTime_
	  << player.restFriendGroupMsgCount_
	  << player.tweetFlag_;
	return s;
}

utilib::basic_text_istream & operator>> (utilib::basic_text_istream & s, Player & player)
{
	s >> player.race_
	  >> player.sex_
	  >> player.level_
	  >> player.icon_
	  >> player.job_
	  >> player.mapId_
	  >> player.guildInfo_
	  >> player.friends_
	  >> player.blacks_
	  >> player.enemys_
	  >> player.uplineTime_
	  >> player.offlineTime_
	  >> player.fere_
	  >> player.accUplineTimeDay_
	  >> player.beginFriendGroupMsgCountTime_
	  >> player.restFriendGroupMsgCount_
	  >> player.tweetFlag_;
	return s;
}

void Player::Tweet::set(RelationServiceNS::Tweet & tweet)
{
	type = tweet.type;
	createdTime = tweet.createdTime;
	msg.clear();

	switch(tweet.type)
	{
	case RelationServiceNS::TWEET_TYPE_UPLINE:
	case RelationServiceNS::TWEET_TYPE_OFFLINE:
	case RelationServiceNS::TWEET_TYPE_KILL_BOSS:
	case RelationServiceNS::TWEET_TYPE_UPGRADE:
		{
			char buf[32];
			unsigned * flag = (unsigned *)tweet.tweet;
			sprintf(buf, "%u", *flag);
			msg = buf;
		}
		break;
	case RelationServiceNS::TWEET_TYPE_GET_TITLE:
	case RelationServiceNS::TWEET_TYPE_GET_ACH:
	case RelationServiceNS::TWEET_TYPE_KILL_PLAYER:
	case RelationServiceNS::TWEET_TYPE_ADD_FRIEND:
	case RelationServiceNS::TWEET_TYPE_JOIN_GUILD:
	case RelationServiceNS::TWEET_TYPE_LEAVE_GUILD:
		{
			 if (tweet.tweetLen > sizeof(tweet.tweet) - 1)
				tweet.tweetLen = sizeof(tweet.tweet) - 1;
			 tweet.tweet[tweet.tweetLen] = 0;
			 msg = tweet.tweet;
		}
		break;
	case RelationServiceNS::TWEET_TYPE_DEAD:
		break;
	case RelationServiceNS::TWEET_TYPE_SHOPPING:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_UPGRADE:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_MOSAIC:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_TRANSFORM:
		break;
	default:
		break;
	}
}

void Player::Tweet::set(unsigned _type, unsigned _createdTime, void * data, size_t len)
{
	type = _type;
	createdTime = _createdTime;
	msg.clear();

	switch(type)
	{
	case RelationServiceNS::TWEET_TYPE_UPLINE:
	case RelationServiceNS::TWEET_TYPE_OFFLINE:
	case RelationServiceNS::TWEET_TYPE_KILL_BOSS:
	case RelationServiceNS::TWEET_TYPE_UPGRADE:
		{
			assert(len == sizeof(unsigned));
			char buf[32];
			unsigned * flag = (unsigned *)data;
			sprintf(buf, "%u", *flag);
			msg = buf;
		}
		break;
	case RelationServiceNS::TWEET_TYPE_GET_TITLE:
	case RelationServiceNS::TWEET_TYPE_GET_ACH:
	case RelationServiceNS::TWEET_TYPE_KILL_PLAYER:
	case RelationServiceNS::TWEET_TYPE_ADD_FRIEND:
	case RelationServiceNS::TWEET_TYPE_JOIN_GUILD:
	case RelationServiceNS::TWEET_TYPE_LEAVE_GUILD:
		{
			((char *)data)[len - 1] = 0;
			msg = (char *)data;
		}
		break;
	case RelationServiceNS::TWEET_TYPE_DEAD:
		break;
	case RelationServiceNS::TWEET_TYPE_SHOPPING:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_UPGRADE:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_MOSAIC:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_TRANSFORM:
		break;
	default:
		break;
	}
}

void Player::Tweet::get(RelationServiceNS::Tweet & tweet)const
{
	tweet.type = type;
	tweet.createdTime = createdTime;
	tweet.tweetLen = 0;

	switch(tweet.type)
	{
	case RelationServiceNS::TWEET_TYPE_UPLINE:
	case RelationServiceNS::TWEET_TYPE_OFFLINE:
	case RelationServiceNS::TWEET_TYPE_KILL_BOSS:
	case RelationServiceNS::TWEET_TYPE_UPGRADE:
		{
			unsigned * p = (unsigned *)tweet.tweet;
			sscanf(msg.c_str(), "%u", p);
		}
		break;
	case RelationServiceNS::TWEET_TYPE_GET_TITLE:
	case RelationServiceNS::TWEET_TYPE_GET_ACH:
	case RelationServiceNS::TWEET_TYPE_KILL_PLAYER:
	case RelationServiceNS::TWEET_TYPE_ADD_FRIEND:
	case RelationServiceNS::TWEET_TYPE_JOIN_GUILD:
	case RelationServiceNS::TWEET_TYPE_LEAVE_GUILD:
		{
			tweet.tweetLen = StringToBuf(msg, tweet.tweet, sizeof(tweet.tweet));
		}
		break;
	case RelationServiceNS::TWEET_TYPE_DEAD:
		break;
	case RelationServiceNS::TWEET_TYPE_SHOPPING:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_UPGRADE:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_MOSAIC:
		break;
	case RelationServiceNS::TWEET_TYPE_EQUIPMENT_TRANSFORM:
		break;
	default:
		break;
	}
}

Player::Player(const unsigned uiid)
: uiid_(uiid)
, race_(0)
, sex_(0)
, level_(0)
, icon_(0)
, gateId_(0)
, playerId_(0)
, mapId_(0)
, lineState_(LINE_OFFLINE)
, job_(0)
, fere_(0)
, uplineTime_(0)
, offlineTime_(0)
, accUplineTimeDay_(0)
, beginFriendGroupMsgCountTime_(0)
, restFriendGroupMsgCount_(10)
, teamId_(INVALID_TEAM_ID)
, chatGroupId_(INVALID_CHATGROUP_ID)
, tweetFlag_(0)
{
	GroupInfo info;
	info.name = default_group_name;
	info.groupId = 1;
	friends_.push_back(info);
}

Player::~Player()
{
}

const std::string & Player::AccountId()const
{
	return account_;
}

const unsigned Player::Uiid()const
{
	return uiid_;
}

const std::string & Player::Name()const
{
	return name_;
}

const int Player::Race()const
{
	return race_;
}

const int Player::Sex()const
{
	return sex_;
}

const int Player::Icon()const
{
	return icon_;
}

const unsigned Player::Level()const
{
	return level_;
}


const unsigned Player::GateId()const
{
	return gateId_;
}

const unsigned Player::PlayerId()const
{
	return playerId_;
}

const unsigned Player::MapId()const
{
	return mapId_;
}

const Player::GuildInfo	& Player::GetGuildInfo()const
{
	return guildInfo_;
}

const int Player::LineState()const
{
	return lineState_;
}

const int Player::Job()const
{
	return job_;
}

const unsigned Player::Fere()const
{
	return fere_;
}

const unsigned Player::GetUplineTime()const
{
	return uplineTime_;
}

const unsigned Player::GetOfflineTime()const
{
	return offlineTime_;
}

const unsigned Player::GetAccUplineDay()const
{
	return accUplineTimeDay_ + time(0) - uplineTime_;
}

const Player::FriendGroupList & Player::GetFriendGroupList()const
{
	return friends_;
}

const Player::FriendList * Player::GetFriendList(const std::string & group)const
{
	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		if (it->name == group)
			return &it->lst;
	}
	return 0;
}

const Player::FriendList *  Player::GetFriendList(const unsigned groupId)const
{
	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		if (it->groupId == groupId)
			return &it->lst;
	}
	return 0;
}

const Player::BlackList	 &	Player::GetBlackList()const
{
	return blacks_;
}

const Player::EnemyList & Player::GetEnemyList()const
{
	return enemys_;
}

const unsigned Player::GetFriendCount(const std::string & group)const
{
	unsigned count = 0;
	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		if (it->name == group)
			count += it->lst.size();
	}
	return count;
}

const unsigned Player::GetFriendCount()const
{
	unsigned count = 0;
	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		count += it->lst.size();
	}
	return count;
}

const unsigned Player::GetBlackCount()const
{
	return blacks_.size();
}

const unsigned Player::GetEnemyCount()const
{
	return enemys_.size();
}


const unsigned	Player::GetFriendGroupId(const unsigned id)const
{
	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		for(FriendList::const_iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			if (fit->uiid == id)
				return it->groupId;
		}
	}
	return -1;
}

const std::string & Player::GetFriendGroupName(const unsigned groupId)const
{
	static std::string nullname;

	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		if (it->groupId == groupId)
			return it->name;
	}
	return nullname;
}

const unsigned	Player::RestGroupMsgCount()const
{
	return restFriendGroupMsgCount_;
}

const Player::WhoseFriendList &  Player::GetWhoseFriendList()const
{
	return whoseFriends_;
}

const Player::WhoseBlackList  &  Player::GetWhoseBlackList()const
{
	return whoseBlacks_;
}

const Player::WhoseEnemyList  &  Player::GetWhoseEnemyList()const
{
 	return whoseEnemys_;
}

const unsigned	Player::GetMaxFriendGroupId()const
{
	unsigned n = 0;
	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		if (n < it->groupId)
			n = it->groupId;
	}
	return n;
}

const unsigned Player::GetBeginCalGroupMsgCountTime()const
{
	return beginFriendGroupMsgCountTime_;
}

const unsigned	Player::GetTeamId()const
{
	return teamId_;
}

const unsigned Player::GetChatGroupId()const
{
	return chatGroupId_;
}

const Player::FriendInfo *  Player::GetFriendInfo(const unsigned uiid)const
{
	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		for(FriendList::const_iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			if (fit->uiid == uiid)
				return &*fit;
		}
	}
	return 0;
}

Player::FriendInfo * Player::GetFriendInfo(const unsigned uiid)
{
	for(FriendGroupList::iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		for(FriendList::iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			if (fit->uiid == uiid)
				return &*fit;
		}
	}
	return 0;
}

const Player::TweetsList & Player::GetTweets()const
{
	return tweets_;
}

const unsigned Player::GetTweetFlag()const
{
	return tweetFlag_;
}

void Player::AccountId(const std::string & id)
{
	account_ = id;
}

void Player::Uiid(const unsigned uiid)
{
	uiid_ = uiid;
}

void Player::Name(const std::string & name)
{
	name_ = name;
}

void Player::Race(const int race)
{
	race_ = race;
}

void Player::Sex(const int sex)
{
	sex_ = sex;
}

void Player::Level(const unsigned Lvl)
{
	level_ = Lvl;
}

void Player::Icon(const int icon)
{
	icon_ = icon;
}

void Player::GateId(const unsigned gateId)
{
	gateId_ = gateId;
}

void Player::PlayerId(const unsigned playerId)
{
	playerId_ = playerId;
}

void Player::MapId(const unsigned mapId)
{
	mapId_ = mapId;
}


void Player::SetGuildInfo(const GuildInfo & info)
{
	guildInfo_ = info;
}

void Player::LineState(const int state)
{
	lineState_ = state;
}

void Player::Job(const int b)
{
	job_ = b;
}

void Player::Fere(const unsigned id)
{
	fere_ = id;
}

void Player::SetUplineTime(const unsigned t)
{
	uplineTime_ = t;
}

void Player::SetOfflineTime(const unsigned t)
{
	if (uplineTime_)
		accUplineTimeDay_ += (t - uplineTime_);
	offlineTime_ = t;
}

void Player::ResetAccUplineTime(const unsigned t)
{
	accUplineTimeDay_ = t;
}

void Player::AddFriend(const std::string & group, Player & player)
{
	AddFriend(group, player.Uiid());
}

void Player::AddFriend(const std::string & group, const unsigned uiid, unsigned degree)
{
	for(FriendGroupList::iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		if (it->name == group)
		{
			FriendInfo info;
			info.uiid = uiid;
			info.degree = degree;
			info.readTimeLine = 0;
			it->lst.push_back(info);
		}
	}
}

void Player::RemoveFriend(Player & player)
{
	RemoveFriend(player.Uiid());
}

void Player::RemoveFriend(const unsigned uiid)
{
	for(FriendGroupList::iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		for(FriendList::iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			if (fit->uiid == uiid)
			{
				it->lst.erase(fit);
				break;
			}
		}
	}
}

void Player::TransferFriend(const unsigned uiid, const unsigned oldGroupId, const unsigned newGroupId)
{
	std::string groupName = GetFriendGroupName(newGroupId);
	if (!groupName.empty())
	{
		RemoveFriend(uiid);
		AddFriend(groupName, uiid);
	}
}

void Player::AddBlack(Player & player)
{
	AddBlack(player.Uiid());
}

void Player::AddBlack(const unsigned uiid)
{
	blacks_.push_back(uiid);
}

void Player::RemoveBlack(Player & player)
{
	RemoveBlack(player.Uiid());
}

void Player::RemoveBlack(const unsigned uiid)
{
	for(BlackList::iterator it = blacks_.begin(); it != blacks_.end(); ++it)
	{
		if (uiid == *it)
		{
			blacks_.erase(it);
			break;
		}
	}
}

void Player::AddEnemy(Player & player, unsigned short mapId, unsigned short posx, unsigned short posy, bool bKilled)
{
	AddEnemy(player.Uiid(), mapId, posx, posy, bKilled);
}

void Player::AddEnemy(const unsigned uiid, unsigned short mapId, unsigned short posx, unsigned short posy, bool bKilled)
{
	EnemyInfo info;
	info.uiid = uiid;
	info.locked = false;
	info.mapId = mapId;
	info.posX = posx;
	info.posY = posy;
	info.time = (unsigned)::time(0);
	info.isKilled = bKilled;
	enemys_.push_back(info);
}

void Player::RemoveEnemy(Player & player)
{
	RemoveEnemy(player.Uiid());
}

void Player::RemoveEnemy(const unsigned uiid)
{
	for(EnemyList::iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		if (uiid == it->uiid)
		{
			enemys_.erase(it);
			break;
		}
	}
}

const unsigned Player::AddFriendGroup(const std::string & group)
{
	if (GetFriendList(group) == 0)
	{
		GroupInfo info;
		info.groupId = GetMaxFriendGroupId() + 1;
		info.name = group;
		info.createdTime = time(0);
		friends_.push_back(info);
		return info.groupId;
	}
	return -1;
}

void Player::RemoveFriendGroup(const unsigned groupId)
{
	for(FriendGroupList::iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		if (it->groupId == groupId)
		{
			friends_.erase(it);
			break;
		}
	}
}

void Player::RenameFriendGroup(const unsigned groupId, const std::string & name)
{
	for(FriendGroupList::iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		if (it->groupId == groupId)
		{
			it->name = name;
			break;
		}
	}
}

void Player::AddWhoseFriend(const unsigned id)
{
	if (std::find(whoseFriends_.begin(), whoseFriends_.end(), id) == whoseFriends_.end())
		whoseFriends_.push_back(id);
}

void Player::AddWhoseBlack(const unsigned id)
{
	if (std::find(whoseBlacks_.begin(), whoseBlacks_.end(), id) == whoseBlacks_.end())
		whoseBlacks_.push_back(id);
}

void Player::AddWhoseEnemy(const unsigned id)
{
	if (std::find(whoseEnemys_.begin(), whoseEnemys_.end(), id) == whoseEnemys_.end())
		whoseEnemys_.push_back(id);
}

void Player::RemoveWhoseFriend(const unsigned id)
{
	for(WhoseFriendList::iterator it = whoseFriends_.begin(); it != whoseFriends_.end(); ++it)
	{
		if (*it == id)
		{
			whoseFriends_.erase(it);
			break;
		}
	}
}

void Player::RemoveWhoseBlack(const unsigned id)
{
	for(WhoseBlackList::iterator it = whoseBlacks_.begin(); it != whoseBlacks_.end(); ++it)
	{
		if (*it == id)
		{
			whoseBlacks_.erase(it);
			break;
		}
	}
}

void Player::RemoveWhoseEnemy(const unsigned id)
{
	for(WhoseEnemyList::iterator it = whoseEnemys_.begin(); it != whoseEnemys_.end(); ++it)
	{
		if (*it == id)
		{
			whoseEnemys_.erase(it);
			break;
		}
	}
}

void Player::SetRestGroupsMsgCount(const unsigned n, const time_t t)
{
	beginFriendGroupMsgCountTime_ = t; 
	restFriendGroupMsgCount_ = n;
}

void Player::LockEnemy(const unsigned id)
{
	for(EnemyList::iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		if (it->uiid == id)
		{
			it->locked = true;
			break;
		}
	}
}

void Player::UnlockEnemy(const unsigned id)
{
	for(EnemyList::iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		if (it->uiid == id)
		{
			it->locked = false;
			break;
		}
	}
}

const unsigned Player::RemoveFirstUnlockedEnemy()
{
	unsigned tmp = 0;
	for(EnemyList::iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		if (it->locked == false)
		{
			tmp = it->uiid;
			enemys_.erase(it);
			break;
		}
	}
	return tmp;
}

void Player::SetChatGroupId(const unsigned id)
{
	chatGroupId_ = id;
}

void Player::AddTweet(const Player::Tweet & tt)
{
	tweets_.push_back(tt);
}

void Player::AddFriendLyDegree(const unsigned friendUiid, unsigned degree)
{
	for(FriendGroupList::iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		for(FriendList::iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			if (fit->uiid == friendUiid)
			{
				fit->degree += degree;
				return;
			}
		}
	}
}

void Player::SetFriendLyDegree(const unsigned friendUiid, unsigned degree)
{
	for(FriendGroupList::iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		for(FriendList::iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			if (fit->uiid == friendUiid)
			{
				fit->degree = degree;
				return;
			}
		}
	}
}

void Player::SetTweetFlag(const unsigned flag)
{
	tweetFlag_ = flag;
}

void Player::SetTeamId(const unsigned id)
{
	teamId_ = id;
}

const bool Player::IsFriend(const Player & player)const
{
	return IsFriend(player.Uiid());
}

const bool Player::IsFriend(const unsigned id)const
{
	for(FriendGroupList::const_iterator it = friends_.begin(); it != friends_.end(); ++it)
	{
		for(FriendList::const_iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			if (fit->uiid == id)
				return true;
		}
	}
	return false;
}

const bool Player::IsBlack(const Player & player)const
{
	return IsBlack(player.Uiid());
}

const bool Player::IsBlack(const unsigned id)const
{
	return std::find(blacks_.begin(), blacks_.end(), id) != blacks_.end();
}

const bool Player::IsEnemy(const Player & player)const
{
	return IsEnemy(player.Uiid());
}

const bool Player::IsEnemy(const unsigned id)const
{
	for(EnemyList::const_iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		if (it->uiid == id)
		{
			return true;
		}
	}
	return false;
}

const bool Player::IsLockedEnemy(const unsigned id)const
{
	for(EnemyList::const_iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		if (it->uiid == id)
		{
			return it->locked;
		}
	}
	return false;
}

const Player::EnemyInfo * Player::GetEnemyInfo(const unsigned id)const
{
	for(EnemyList::const_iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		if (it->uiid == id)
		{
			return &(*it);
		}
	}
	return 0;
}


Player::TeamFlag & Player::GetTeamFlag()
{
	return teamFlag_;
}

const Player::TeamFlag & Player::GetTeamFlag()const
{
	return teamFlag_;
}

void Player::AddUpLineTweet()
{
	Tweet tweet;
	unsigned flag = 1;
	tweet.set(RelationServiceNS::TWEET_TYPE_UPLINE, time(0), &flag, sizeof(flag));
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddOffLineTweet()
{
	Tweet tweet;
	unsigned flag = 0;
	tweet.set(RelationServiceNS::TWEET_TYPE_OFFLINE, time(0), &flag, sizeof(flag));
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddKillBossTweet(unsigned bossId)
{
	Tweet tweet;
	tweet.set(RelationServiceNS::TWEET_TYPE_KILL_BOSS, time(0), &bossId, sizeof(bossId));
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddGetTitleTweet(const std::string & title)
{
	Tweet tweet;
	tweet.type = RelationServiceNS::TWEET_TYPE_GET_TITLE;
	tweet.createdTime = time(0);
	tweet.msg = title;
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddGetAchTweet(const std::string & title)
{
	Tweet tweet;
	tweet.type = RelationServiceNS::TWEET_TYPE_GET_ACH;
	tweet.createdTime = time(0);
	tweet.msg = title;
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddUpgradeTweet(unsigned lvl)
{
	Tweet tweet;
	tweet.set(RelationServiceNS::TWEET_TYPE_UPGRADE, time(0), &lvl, sizeof(lvl));
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddDeadTweet()
{
	Tweet tweet;
	tweet.type = RelationServiceNS::TWEET_TYPE_DEAD;
	tweet.createdTime = time(0);
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddKillPlayerTweet(const std::string & player)
{
	Tweet tweet;
	tweet.type = RelationServiceNS::TWEET_TYPE_KILL_PLAYER;
	tweet.createdTime = time(0);
	tweet.msg = player;
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddNewFriendAddedTweet(const std::string & player)
{
	Tweet tweet;
	tweet.type = RelationServiceNS::TWEET_TYPE_ADD_FRIEND;
	tweet.createdTime = time(0);
	tweet.msg = player;
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddJoinGuildTweet(const std::string & guild)
{
	Tweet tweet;
	tweet.type = RelationServiceNS::TWEET_TYPE_JOIN_GUILD;
	tweet.createdTime = time(0);
	tweet.msg = guild;
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

void Player::AddLeaveGuildTweet(const std::string & guild)
{
	Tweet tweet;
	tweet.type = RelationServiceNS::TWEET_TYPE_LEAVE_GUILD;
	tweet.createdTime = time(0);
	tweet.msg = guild;
	AddTweet(tweet);
	SocialRelationManager::instance()->GetDB().SaveTweet(*this, tweet);
}

__SERVICE_SPACE_END_NS__