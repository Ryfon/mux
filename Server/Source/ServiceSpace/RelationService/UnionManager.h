﻿#ifndef __SERVICE_SPACE_RELEATION_UNIONMANAGER_H__
#define __SERVICE_SPACE_RELEATION_UNIONMANAGER_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/utilib/noncopyable.h"
#include "../ServiceSpaceDBLib/db2o.h"
#include "../ServiceSpaceLib/SessionHandler.h"
#include "../ServiceSpaceLib/utilib/split.h"
#include "Union.h"
#include "Player.h"
#include "Guild.h"
#include "RelationServiceProtocol.h"
#include "MessageHandler.h"
#include <string>
#include <map>
#include <list>
#include <set>
#include <ctime>

using RelationServiceNS::UnionRankInfo;

__SERVICE_SPACE_BEGIN_NS__

class SocialRelationManager;

class GuildManager
{
public:
	~GuildManager();

private:	
	friend class SocialRelationManager;

	GuildManager();

	int HandleCreateGuild(tcp_session session, const RelationServiceNS::CreateUnionRequest & req);
	int HandleDestroyGuild(tcp_session session, const RelationServiceNS::DestroyUnionRequest & req);
	int HandleQueryGuildBasicInfo(tcp_session session, const RelationServiceNS::QueryUnionBasicRequest & req);
	int HandleQueryGuildMemberList(tcp_session session, const RelationServiceNS::QueryUnionMembersRequest & req);
	int HandleInvitePlayerJoinGuild(tcp_session session, const RelationServiceNS::AddUnionMemberRequest & req);
	int HandleExitGuildMember(tcp_session session, const RelationServiceNS::RemoveUnionMemberRequest & req);
	int HandleInvitePlayerConfirmed(tcp_session session, const RelationServiceNS::AddUnionMemberConfirmed & req);
	int HandleModifyGuildMemberTitle(tcp_session session, const RelationServiceNS::ModifyUnionMemberTitleRequest & req);
	int HandleTransformGuildCaption(tcp_session session, const RelationServiceNS::TransformUnionCaptionRequest & req);
	int HandleAdvanceGuildMemberPos(tcp_session session, const RelationServiceNS::AdvanceUnionMemberPosRequest & req);
	int HandleReduceGuildMemberPos(tcp_session session, const  RelationServiceNS::ReduceUnionMemberPosRequest & req);
	int HandleGuildChannelSpeek(tcp_session session, const RelationServiceNS::UnionChannelSpeekRequest & req);
	int HandleUpdateGuildPosList(tcp_session session, const RelationServiceNS::UpdateUnionPosCfgRequest & req);
	int HandleStudyUnionSkill(tcp_session session, const RelationServiceNS::StudyUnionSkillRequest & req);
	int HandlePostGuildTask(tcp_session session, const RelationServiceNS::PostUnionTasksListRequest & req);
	int HandleAdvanceGuildLevel(tcp_session session, const RelationServiceNS::AdvanceUnionLevelRequest & req);
	int HandlePostGuildBulletin(tcp_session session, const RelationServiceNS::PostUnionBulletinRequest & req);
	int HandleQueryGuildLog(tcp_session session, const RelationServiceNS::QueryUnionLogRequest & req);
	int HandleModifyGuildActive(tcp_session session, const RelationServiceNS::ModifyUnionActivePointRequest & req);
	int HandleForbidGuildMember(tcp_session session, const RelationServiceNS::ForbidUnionSpeekRequest & req);
	int HandleGuildOwnerCheck(tcp_session session, const RelationServiceNS::UnionOwnerCheckRequest & req);
	int HandleModifyGuildPrestige(tcp_session session, const RelationServiceNS::ModifyUnionPrestigeRequest & req);
	int HandleModifyGuildBadge(tcp_session session, const RelationServiceNS::ModifyUnionBadgeRequest & req);
	int HandleModifyGuildMult(tcp_session session, const RelationServiceNS::ModifyUnionMultiRequest & req);
	int HandleQueryGuildRankRequest(tcp_session session, const RelationServiceNS::QueryUnionRankRequest & req);
	int HandleSearchGuildRankRequest(tcp_session session, const RelationServiceNS::SearchUnionRankRequest & req);
	int HandleQueryRaceMasterRequest(tcp_session session, const RelationServiceNS::QueryRaceMasterRequest & req);
	int HandleQueryGuildMembersRequest(tcp_session sesssion, const RelationServiceNS::QueryGuildMembersRequest & req);
	int HandleQueryPlayerGuild(tcp_session session, const RelationServiceNS::GRQueryNickUnion & req);

	int SendGuildMemberList(const Player & player, const Guild & guild);
	int SendGuildBasicInfo(const Player & player, const Guild & guild);
	int SendGuildSkillList(const Player & player, const Guild & guild);
	int SendGuildSkillListNotify(const Guild & guild);
	int SendGuildDestroyedNotify(const Guild & guild);
	int SendGuildMemberExitNotify(const Player & player, const Guild & guild, const bool flag);
	int SendGuildMemberJoinNotify(const Player & player, const Guild & guild);
	int SendGuildMemberTitleChangedNotify(const Player & player, const Guild & guild);
	int SendGuildCaptionChangedNotify(const Player & caption, const Guild & guild);
	int SendGuildMemberForbidNotify(const Player & player, const Guild & guild);
	int SendGuildActiveNotify(const Guild & guild);
	int SendGuildPosListNotify(const Guild & guild);
	int SendGuildTaskPostNotify(const Guild & guild);
	int SendGuildMemberTaskPost(const Player & plaeyr, const Guild & guild);
	int SendGuildBulletinNotify(const Guild & guild);
	int SendGuildBulletinNotify(const Player & player, const Guild & guild);
	int SendGuildLevelNotify(const Guild & guild, const unsigned oldLvl);
	int SendGuildMemberPosAdvancedNotify(const Player & player, const Guild & guild);
	int SendGuildMemberPosReducedNotify(const Player & player, const Guild & guild);
	int SendGuildMemberUplineNotify(const Player & player, const Guild & guild);
	int SendGuildMemberOfflineNotify(const Player & player, const Guild & guild);
	int SendGuildEmail(const Guild & guild, const Player & player, const std::string & title, const std::string & message);
	int SendGuildLevelChangedEmail(const Guild & guild, const Player & player, const unsigned oldLvl);
	int SendGuildMemberExitedResult(const Player & player, const Guild & guild);
	int SendGuildPrestigeNotify(const Guild & guild);
	int SendGuildPrestigeNotify(const Player & player, const Guild & guild);
	int SendGuildBadgeNotify(const Guild & guild);
	int SendGuildBadgeNotify(const Player & player, const Guild & guild);
	int SendGuildMultiNotify(const Guild & guild);
	int SendGuildMultiNotify(const Guild & guild, unsigned type);
	int SendGuildMultiNotify(const Player & player, const Guild & guild);
	int SendGuildMultiNotify(const Player & player, const Guild & guild, unsigned type);

	int HandleDeletePlayer(const Player & player, Guild & guild);
	int HandlePlayerUpline(Player & player, Guild & guild);
	int HandlePlayerOffline(const Player & player, Guild & guild);
	int HandlePlayerAdvanceLevel(const Player & player, Guild & guild, int d);
	int HandleGuildDayActiveExpend(Guild & guild);
	int HandleGuildMemberUplineDuration(const unsigned t, Guild & guild);

	int UpdateGuildRank(const unsigned race, const Guild & guild);
	int RemoveGuildRank(const unsigned race, const unsigned guildId);

	int ResetGuildSkillList(Guild & guild);
	unsigned GetMaxCount(const unsigned lvl);
	const Guild::SkillInfo * GetSkill(const unsigned id);
	const unsigned GetTaskPostNeedActive(const unsigned duration);
	const unsigned GetAdvanceGuildNeedActive(const unsigned level);
	const unsigned GetAdvanceGuildGainActive(const unsigned level);
	void GetAvailableSkillList(const Guild & guild, Guild::SkillList & lst);
	const unsigned GetActiveByUplineDuration(const unsigned t);
	const unsigned _Eval(RelationServiceNS::UnionSkill (&l)[10], const Guild::SkillList & lst);
	const bool RepeatInviteCheck(const unsigned invitor, const unsigned invitee)const;
	const bool CheckSkillStudyed(const unsigned skill, const Guild & guild)const;
	const unsigned GetModifyBadgeNeedActive(const Guild & guild);

	void LoadGuildBasicCfg();
	void LoadGuildSkillCfg();
	void LoadGuildSkillShopCfg();
	void LoadGuildRank();
	void LoadGuildRank(unsigned race);
	
	struct InviteInfo
	{
		unsigned invitor;
		unsigned invitee;
		time_t inviteTime;

		InviteInfo():invitor(0), invitee(0), inviteTime(0) {}
	};

	struct guildbasiccfg_t
	{
		int level;
		int maxPopulation;
		int dayExpendActive;
		int advanceExpendActive;
		int advanceGainActive;
	};

	struct posttaskexpendcfg_t
	{
		int duration;	
		int expendActive;
	};

	struct studyskillexpendcfg_t
	{
		int skillLevel;
		int expendActive;
	};

	struct extendstoreexpendcfg_t
	{
		int row;
		int expendActive;
	};

	struct gainactivewaycfg_t
	{
		int way;
		int gainActive;
		int needHours;
	};

	struct GuildRankInfo : public UnionRankInfo
	{
		unsigned guildId;
		unsigned captionId;

		struct CheckId
		{
			unsigned id;
			CheckId(unsigned _id) : id(_id) {}
			const bool operator()(const GuildRankInfo & info)const { return id == info.guildId; }
		};
	};

	std::multimap<unsigned, InviteInfo> invitees_;
	std::vector<Guild::SkillInfo> skills_;

	std::map<int, guildbasiccfg_t> guildbasiccfg_;
	std::map<int, posttaskexpendcfg_t> posttaskexpendcfg_;
	std::map<int, studyskillexpendcfg_t> studyskillexpendcfg_;
	std::map<int, extendstoreexpendcfg_t> extendstoreexpendcfg_;
	std::map<int, gainactivewaycfg_t> gainactivewaycfg_;
	std::map<unsigned, std::vector<GuildRankInfo> > guildRank_;
};

__SERVICE_SPACE_END_NS__

#endif
