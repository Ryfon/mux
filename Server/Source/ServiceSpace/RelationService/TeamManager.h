﻿/********************************************************************
	created:	2008/07/08
	created:	8:7:2008   15:28
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\RelationService\TeamManager.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\RelationService
	file base:	TeamManager
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __SERVICE_SPACE_RELATION_SERVICE_TEAM_MANAGER_H__
#define __SERVICE_SPACE_RELATION_SERVICE_TEAM_MANAGER_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "../ServiceSpaceLib/utilib/pool_lock.h"
#include "RelationServiceProtocol.h"
#include "MessageHandler.h"
#include "Player.h"
#include "Team.h"
#include <map>

__SERVICE_SPACE_BEGIN_NS__

class SocialRelationManager;
	  
class TeamManager
{
public:
	~TeamManager();

private:
	TeamManager();

	int HandleCreateTeam(tcp_session session, const RelationServiceNS::CreateTeamRequest & req);
	int HandleDestroyTeam(tcp_session session, const RelationServiceNS::DestroyTeamRequest & req, bool = true);
	int HandleAddTeamMember(tcp_session session, const RelationServiceNS::AddTeamMemberRequest & req);
	int HandleRemoveTeamMember(tcp_session session, const RelationServiceNS::RemoveTeamMemberRequest & req);
	int HandleQueryTeam(tcp_session session, const RelationServiceNS::QueryTeamRequest & req);
	int HandleQueryTeamMember(tcp_session session, const RelationServiceNS::QueryTeamMemberRequest & req);
	int HandleModifyTeamCaption(tcp_session session, const RelationServiceNS::ModifyTeamCaptainRequest & req);
	int HandleModifyTeamExpMode(tcp_session session, const RelationServiceNS::ModifyExpSharedModeRequest & req);
	int HandleModifyTeamGoodMode(tcp_session session, const RelationServiceNS::ModifyGoodSharedModeRequest & req);
	int HandleTeamChannelSpeek(tcp_session session, const RelationServiceNS::TeamBroadcastRequest & req);
	int HandleModifyTeamRollItemLevel(tcp_session session, const RelationServiceNS::ModifyTeamRollItemLevelRequest & req);
	int HandleQueryTeamMemberCount(tcp_session session, const RelationServiceNS::QueryTeamMembersCountRequest & req);
	int HandleNotifyExpAdded(tcp_session session, const RelationServiceNS::NotifyExpAdded & req);

	int HandlePlayerUpline(Player & player);
	int HandlePlayerOffline(Player & player);

	void SendTeamCreatedNotify(const Team & team);
	void SendTeamDestroyedNotify(const Team & team);
	void SendTeamMemberAddedNotify(const Player & player, const Team & team, const unsigned flag, const unsigned gateId, const unsigned playerId);
	void SendTeamMemberRemovedNotify(const Player & player, const Team & team, const unsigned mode);
	void SendTeamBoradcastNotify(const RelationServiceNS::BroadcastData & data, const unsigned type, const Player::TmpId & player, const Team & team);
	void SendTeamCaptionChangedNotify(const Team & team);
	void SendTeamGoodSharedModeNotify(const Team & team);
	void SendTeamRollItemLevelNotify(const Player::TmpId & player, const Team & team);
	void SendTeamExpSharedModeNotify(const Team & team);
	void SendTeamFlag(const Player & player);
	void SendTeamLog(unsigned uiid, unsigned short level, unsigned teamId, unsigned short type);

	void MakeTeamMemberSeq(Team::MemberList & lst);

	void CalTeamGain(Team & team);
	void ResetPlayerTeamGain(const Player & player);

	typedef std::map<unsigned, Team> TeamMap;

	TeamMap teams_;
	unsigned maxTeamId_;

	typedef std::map<std::string, unsigned> FriendTeamGainMap;
	FriendTeamGainMap friendTeamGain_;

	friend class SocialRelationManager;
};

__SERVICE_SPACE_END_NS__

#endif
