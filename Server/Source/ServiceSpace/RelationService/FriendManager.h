﻿#pragma once



#ifndef __SERVICE_SPACE_RELATION_SERVICE_FRIEND_MANAGER_H__
#define __SERVICE_SPACE_RELATION_SERVICE_FRIEND_MANAGER_H__

#include "MessageHandler.h"
#include "Player.h"


__SERVICE_SPACE_BEGIN_NS__

class SocialRelationManager;

class FriendManager
{
public:
	FriendManager();

private:	
	~FriendManager(void);
	
	int HandlePlayerUpline(tcp_session session, Player & player);
	int HandlePlayerOffline(tcp_session session, const Player & player);
	int HandlePlayerUpdateInfo(tcp_session session, const Player & player);
	int HandleDeletePlayer(Player & player);

	int HandleAddFriendRequest(tcp_session session, RelationServiceNS::GRAddFriend & req);
	int HandleRemoveFriendRequest(tcp_session session, const RelationServiceNS::GRDelFriend & req);

	int HandleCreeateFriendGroup(tcp_session session, const RelationServiceNS::GRAddGroup & req);
	int HandleDeleteFriendGroup(tcp_session session, const RelationServiceNS::GRDelGroup & req);
	int HandleRenameFriendGroup(tcp_session session, const RelationServiceNS::GRAlterGroupName & req);

	int HandleTransferFriend(tcp_session session, const RelationServiceNS::GRFriendAlterGroupID & req);

	int HandleAddBlacklistPlayer(tcp_session  session, const RelationServiceNS::GRAddToBlacklist & req);
	int HandleRemoveBlacklistPlayer(tcp_session session, const RelationServiceNS::GRDelFromBlacklist & req);

	int HandleSendFriendGroupMessage(tcp_session session, const RelationServiceNS::GRSendGroupMsg & req);
	int HandleSendPersonalMessage(tcp_session session, const RelationServiceNS::GRSendMsg & req);

	int HandleAddEnemy(tcp_session session, const RelationServiceNS::GRAddEnemy & req, bool bKilled);
	int HandleRemoveEnemy(tcp_session session, const RelationServiceNS::GRDelEnemy & req);
	int HandleLockEnemy(tcp_session session, const RelationServiceNS::GRLockEnemy & req);

	int HandleQueryPlayerRelationRequest(tcp_session session, const RelationServiceNS::QueryPlayerRelationRequest & req);
	int HandleQueryPlayerInfoRequest(tcp_session session, const RelationServiceNS::QueryPlayerInfoRequest & req);

	int HandleAddFriendlyDegreeRequest(tcp_session session, const RelationServiceNS::AddFriendlyDegree & req);

	int HandleAddTweetRequest(tcp_session session, const RelationServiceNS::AddTweet & req);

	int HandleQueryTweetsRequest(tcp_session session, const RelationServiceNS::QueryFriendTweetsRequest & req);

	int HandleQueryFriendsListRequest(tcp_session session, const RelationServiceNS::QueryFriendsListRequest & req);

	int HandleNotifyExpAdded(tcp_session session, const RelationServiceNS::NotifyExpAdded & req);

	int HandleQueryExpNeededWhenLvlUpResult(tcp_session session, const RelationServiceNS::QueryExpNeededWhenLvlUpResult & res);

	int HandleKillPlayer(tcp_session session, const RelationServiceNS::GRAddEnemy & req);

	int HandleTweetFlagModify(tcp_session session, const RelationServiceNS::ModifyTweetReceiveFlag & req);

	void AddWhoseFriend(const Player & player);
	void AddWhoseBlack(const Player & player);
	void AddWhoseEnemy(const Player & player);

	void SendPlayerUplineNotify(const Player & player);
	void SendPlayerUpdateInfoNotify(const Player & player);
	void SendPlayerUpdateInfoNotify(const Player & player, const Player & target);
	void SendPlayerOfflineNotify(const Player & player);
	void SendPlayerDeletedNotify(const Player & player);
	void SendPlayerFriendLineNotify(const Player & player, const Player & target);

	void SendPlayerFriendGroupsInfo(const Player & player);
	void SendPlayerGroupFriends(const Player & player, const std::string & group);
	void SendPlayerGroupFriends(const Player & player, const Player::FriendList & lst);
	void SendPlayerGroupFriends2(const Player & player, const Player::FriendList & lst);
	void SendPlayerAllGroupsFriends(const Player & player);
	void SendPlayerBlackList(const Player & player);
	void SendPlayerEnemyList(const Player & player);
	void SendPlayerRestGroupMsgCount(const Player & player);
	void SendFriendPlayerRemoved(const Player & player, const Player & target);
	void SendBlackPlayerRemoved(const Player & player, const Player & target);
	void SendEnemyPlayerRemoved(const Player & player, const Player & target);

	void SendFriendDegreeGainEmail(tcp_session session, const Player & player);

	unsigned GetLevelUpNeededExp(const Player & player);

	unsigned GetFriendGainItem(unsigned level, unsigned job, unsigned degree);

	void LoadFriendGainCfg();

	struct FriendGain
	{
		unsigned level;
		unsigned job;
		unsigned degree;
		unsigned itemId;
	};

	struct FriendInviteInfo
	{
		unsigned invitor;
		time_t inviteTime;
	};

	std::map<unsigned, FriendInviteInfo> invitees_;
	std::vector<unsigned> lvlUpNeedExp_;
	std::vector<FriendGain> friendGain_;

	friend class SocialRelationManager;
};



__SERVICE_SPACE_END_NS__

#endif

