﻿#include "SocialRelationDB.h"
#include "RelationServiceProtocol.h"
#include "SocialRelationManager.h"
#include "Union.h"

__SERVICE_SPACE_BEGIN_NS__

const char * _table_character()
{
	return "characterinfo";
}

const char * _character_id()
{
	return "uiid";
}

const char * _character_name()
{
	return "name";
}

const char * _character_guildid()
{
	return "guildid";
}

const char * _character_info()
{
	return "info";
}

typedef field<_character_id, unsigned, true> CharacterId;
typedef field<_character_name, std::string, false, true, true>	CharacterName;	// unique
typedef field<_character_guildid, unsigned, false, true> CharacterGuildID;
typedef field<_character_info, std::string>	CharacterInfo;
typedef object<_table_character, CharacterId, CharacterName, CharacterGuildID, CharacterInfo> Character;

const char * _tweet_id()
{
	return "uiid";
}

const char * _tweet_type()
{
	return "type";
}

const char * _tweet_time()
{
	return "createdtime";
}

const char * _tweet_msg()
{
	return "tweet";
}

const char * _tweet()
{
	return "tweets";
}

typedef field<_tweet_id, unsigned, false, true> TweetId;
typedef field<_tweet_type, unsigned> TweetType;
typedef field<_tweet_time, unsigned> TweetTime;
typedef field<_tweet_msg, std::string> TweetMsg;
typedef object<_tweet, TweetId, TweetTime, TweetType, TweetMsg> Tweet;

void Eval(Player & player, const Character & character)
{
	player.Uiid(character.get<CharacterId>().value);
	player.Name(character.get<CharacterName>().value);
	utilib::basic_stream_buffer buffer(const_cast<char *>(character.get<CharacterInfo>().value.c_str()), character.get<CharacterInfo>().value.size());
	buffer.resize(character.get<CharacterInfo>().value.size());
	utilib::basic_text_istream stream(buffer);
	stream >> player;
	Player::GuildInfo guildInfo = player.GetGuildInfo();
	guildInfo.guildId = character.get<CharacterGuildID>().value;
	player.SetGuildInfo(guildInfo);
}

void Eval(Character & character, const Player & player)
{
	character.get<CharacterId>().set(player.Uiid());
	character.get<CharacterName>().set(player.Name());
	character.get<CharacterGuildID>().set(player.GetGuildInfo().guildId);
	char buf[1024 * 64];
	utilib::basic_stream_buffer buffer(buf, sizeof(buf));
	utilib::basic_text_ostream stream(buffer);
	stream << player;
	std::string info(buf, buffer.size());
	character.get<CharacterInfo>().set(info);
}

void Eval(Guild & guild, const GuildInfo & info)
{
	guild.GuildId(info.get<GuildID>().value);
	guild.GuildName(info.get<GuildName2>().value);
	guild.Level(info.get<GuildLevel>().value);
	guild.AddActive(info.get<GuildActive>().value);
	guild.GuildOwner(info.get<GuildOwner>().value);
	guild.SetBulletin(info.get<GuildBulletin>().value);
	guild.Prestige(info.get<GuildPrestige>().value);
	
	utilib::basic_stream_buffer buffer(const_cast<char *>(info.get<GuildOther>().value.c_str()), info.get<GuildOther>().value.size());
	buffer.resize(info.get<GuildOther>().value.size());
	
	utilib::basic_text_istream s(buffer);
	s >> guild;
}

void Eval(GuildInfo & info, const Guild & guild)
{
	info.get<GuildID>().set(guild.GuildId());
	info.get<GuildName2>().set(guild.GuildName());
	info.get<GuildLevel>().set(guild.Level());
	info.get<GuildActive>().set(guild.Active());
	info.get<GuildOwner>().set(guild.GuildOwner());
	info.get<GuildBulletin>().set(guild.GetBulletin());
	info.get<GuildPrestige>().set(guild.Prestige());

	Player * caption = SocialRelationManager::instance()->GetPlayerByUiid(guild.GuildOwner());
	if (caption)
		info.get<GuildOwnerRace>().set(caption->Race());
	else
		info.get<GuildOwnerRace>().set(0);

	char buf[16 * 1024];
	utilib::basic_stream_buffer buffer(buf, sizeof(buf));
	utilib::basic_text_ostream s(buffer);
	s << guild;

	std::string str(buf, buffer.size());
	info.get<GuildOther>().set(str);
}

SocialRelationDB::SocialRelationDB()
{
}

SocialRelationDB::~SocialRelationDB()
{
}

int SocialRelationDB::init(const std::string & str)
{
	utilib::vectorN<utilib::stringN<16>, 6> vec;
	utilib::split(vec, str.c_str(), str.c_str() + str.size(), ':');
	if (vec.size() < 5)
	{
		SS_ERROR(" RelationServiceMessageHandler::parse_db_param 错误！配置文件错误! vec.size()=%d \n",vec.size()) ;
		return -1;
	}

	return init_i(vec[2].c_str(), vec[0].c_str(), vec[4].c_str(), atoi(vec[1].c_str()), vec[3].c_str());
}

int SocialRelationDB::init_i(const char* dbname,const char* host,const char* passwrd,unsigned int port,const char* user)
{
	dbdesc<DB_MYSQL> desc;
	strcpy(desc.charset, "");
	strcpy(desc.host,host);
	desc.port = port;
	strcpy(desc.user, user);
	strcpy(desc.passwrd, passwrd);
	strcpy(desc.dbname, dbname);
	desc.flags = 0;

	int ret = 0;
	Character * character = 0;

	try{
		db_.reset(new database<DB_MYSQL>(desc));

		character = new Character();
		character->get<CharacterName>().max_size(32);
		character->get<CharacterInfo>().max_size(6 * 1024);
		db_->create(*character);

	}catch(const std::exception & e)
	{
		ret = -1;
		SS_ERROR( "%s\n", e.what() ? e.what() : "unknown");
	}

	if (character)
		character->destroy();

	GuildInfo * guild = new GuildInfo();
	try
	{
		guild->get<GuildName2>().max_size(32);
		guild->get<GuildBulletin>().max_size(1024);
		guild->get<GuildOther>().max_size(2 * 1024);

		db_->create(*guild);

	}catch(const std::exception & e)
	{
		ret = -1;
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	if(guild)
		guild->destroy();

	GuildLog * log = new GuildLog;
	try{
		log->get<GuildLogParam1>().max_size(64);
		log->get<GuildLogParam2>().max_size(64);
		log->get<GuildLogParam3>().max_size(64);
		log->get<GuildLogParam4>().max_size(64);

		db_->create(*log);

	}catch(const std::exception & e)
	{
		ret = -1;
		SS_ERROR(ACE_TEXT("%s\n"), e.what() ? e.what() : "unknown");
	}

	if (log)
		log->destroy();

	return ret;
}

void SocialRelationDB::Load(const unsigned uiid, Player &player)
{
	ACE_TRACE("SocialRelationDB::Load");
	
	Character * character = new Character();
	try
	{
		equal_strict<unsigned> eq(uiid);
		character->get<CharacterId>().set_strict(&eq);

		if (1 == db_->query(*character))
		{
			Eval(player, *character);
		}
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
		player.Uiid(0);
	}

	character->destroy();
}

void SocialRelationDB::Load(const std::string & name, Player & player)
{
	ACE_TRACE("SocialRelationDB::Load");

	Character * character = new Character();
	try
	{
		equal_strict<std::string> eq(name);
		character->get<CharacterName>().set_strict(&eq);

		if (1 == db_->query(*character))
		{
			Eval(player, *character);
		}
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
		std::string tmp;
		player.Name(tmp);
	}

	character->destroy();
}

void SocialRelationDB::FirstSave(const Player & player)
{
	ACE_TRACE("SocialRelationDB::FirstSave");

	Character * character = new Character();
	try
	{
		equal_strict<unsigned> eq(player.Uiid());
		character->get<CharacterId>().set_strict(&eq);

		Eval(*character, player);

		db_->insert(*character);
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	character->destroy();
}

void SocialRelationDB::Save(const Player & player)
{
	ACE_TRACE("SocialRelationDB::Save");

	Character * character = new Character();
	try
	{
		equal_strict<unsigned> eq(player.Uiid());
		character->get<CharacterId>().set_strict(&eq);

		Eval(*character, player);

		db_->replace(*character);
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	character->destroy();
}

void SocialRelationDB::Destroy(const Player & player)
{
	ACE_TRACE("SocialRelationDB::Destroy");

	Character * character = new Character();
	try
	{
		equal_strict<unsigned> eq(player.Uiid());
		character->get<CharacterId>().set_strict(&eq);

		Eval(*character, player);

		db_->remove(*character);
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	character->destroy();
}

void SocialRelationDB::Load(const unsigned id, Guild & guild)
{
	ACE_TRACE("SocialRelationDB::Load guild");


}

void SocialRelationDB::Load(const std::string & guildName, Guild & guild)
{
	ACE_TRACE("SocialRelationDB::Load guild");
}

void SocialRelationDB::SaveTweet(const Player & player, const Player::Tweet & tweet)
{
	ACE_TRACE("SocialRelationDB::SaveTweet");

	Tweet * p = new Tweet;
	try
	{
		p->get<TweetMsg>().max_size(512);
		
		p->get<TweetId>().set(player.Uiid());
		p->get<TweetType>().set(tweet.type);
		p->get<TweetTime>().set(tweet.createdTime);
		p->get<TweetMsg>().set(tweet.msg);

		db_->insert(*p);

	}catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	p->destroy();
}

void SocialRelationDB::LoadTweets(Player & player)
{
	ACE_TRACE("SocialRelationDB::LoadTweets");

	Tweet * p = new Tweet;
	try
	{
		p->get<TweetMsg>().max_size(512);
		equal_strict<unsigned> st(player.Uiid());
		p->get<TweetId>().set_strict(&st);

		int count = db_->query(*p, 0, 500);
		if (count > 0)
		{
			const Tweet * tmp = p;
			while(tmp)
			{
				Player::Tweet tt;
				tt.createdTime = tmp->get<TweetTime>().value;
				tt.msg = tmp->get<TweetMsg>().value;
				player.AddTweet(tt);

				tmp = tmp->next();
			}
		}
	
	}catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	p->destroy();
}

bool SocialRelationDB::CheckGuildNameRepeat(const std::string & guildName)
{
	ACE_TRACE("SocialRelationDB::CheckGuildNameRepeat");

	GuildInfo * p = new GuildInfo();
	bool result = false;
	try
	{
		equal_strict<std::string> st(guildName);
		p->get<GuildName2>().set_strict(&st);

		if (db_->query(*p))
			result = true;

	}catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	p->destroy();
	return result;
}

void SocialRelationDB::Load(const Player & player, Guild & guild)
{
	ACE_TRACE("SocialRelationDB::Load");

	GuildInfo * p = new GuildInfo();

	try
	{
		equal_strict<unsigned> st(player.GetGuildInfo().guildId);
		p->get<GuildID>().set_strict(&st);

		if (db_->query(*p))
		{
			Eval(guild, *p);

			Guild::MemberList v;
			LoadAll(guild.GuildId(), v);
			for(Guild::MemberList::const_iterator it = v.begin(); it != v.end(); ++it)
			{
				guild.AddMember(*it);
			}
		}

	}catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	p->destroy();
}

bool SocialRelationDB::FirstSave(const Guild & guild)
{
	ACE_TRACE("SocialRelationDB::FirstSave guild");

	bool result = false;

	GuildInfo * p = new GuildInfo();

	try
	{
		p->get<GuildName2>().max_size(32);
		p->get<GuildOther>().max_size(2 * 1024);

		Eval(*p, guild);

		db_->insert(*p);

		result = true;

	}catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	p->destroy();
	return result;
}

bool SocialRelationDB::Save(const Guild & guild)
{
	ACE_TRACE("SocialRelationDB::Save guild");

	bool result = false;

	GuildInfo * p = new GuildInfo();

	try
	{
		p->get<GuildName2>().max_size(32);
		p->get<GuildOther>().max_size(2 * 1024);

		Eval(*p, guild);

		db_->replace(*p);

		result = true;

	}catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	p->destroy();
	return result;
}

void SocialRelationDB::Destroy(const Guild & guild)
{
	ACE_TRACE("SocialRelationDB::Destroy");

	GuildInfo * pGuild = new GuildInfo();
	try
	{
		equal_strict<unsigned> eq(guild.GuildId());
		pGuild->get<GuildID>().set_strict(&eq);

		db_->remove(*pGuild);
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	pGuild->destroy();

	GuildLog * pLog = new GuildLog();
	try
	{
		equal_strict<unsigned> eq(guild.GuildId());
		pLog->get<GuildID2>().set_strict(&eq);

		db_->remove(*pLog);
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}

	pLog->destroy();
}

void SocialRelationDB::LoadAll(std::vector<Guild> & vec)
{
	ACE_TRACE("SocialRelationDB::LoadAll");

	bool loop = true;
	for(size_t i = 0; loop; ++i)
	{
		GuildInfo * guild = new GuildInfo();

		try
		{
			guild->get<GuildName2>().max_size(32);
			guild->get<GuildBulletin>().max_size(1024);
			guild->get<GuildOther>().max_size(4 * 1024);

			unsigned n = db_->query(*guild, i * 500, 500);
			if (0 == n)
				break;

			const GuildInfo * next = guild;
			while(next)
			{
				Guild tmp;
				Eval(tmp, *next);
				vec.push_back(tmp);
				next = next->next();
			}

			if (n < 500)
				break;

		}catch(const std::exception & e)
		{
			SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
			loop = false;
		}

		guild->destroy();

	} // end for
}

void SocialRelationDB::LoadAll(const unsigned guildId, Guild::MemberList & vec)
{
	ACE_TRACE("SocialRelationDB::LoadAll");

	Character * character = new Character();
	try
	{
		equal_strict<unsigned> eq(guildId);
		character->get<CharacterGuildID>().set_strict(&eq);

		if (db_->query(*character, 0, 500))
		{
			const Character * next = character;
			while(next)
			{
				vec.push_back(next->get<CharacterId>().value);
				next = next->next();
			}
		}
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
		vec.clear();
	}

	character->destroy();
}

const unsigned SocialRelationDB::GetGuildCount()
{
	ACE_TRACE("SocialRelationDB::Save");

	try
	{
		return db_->get_count<GuildInfo>();
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}
	
	return 0;
}

const unsigned SocialRelationDB::GetMaxGuildId()
{
	ACE_TRACE("SocialRelationDB::");

	try
	{
		return db_->get_max_value<GuildInfo, GuildID>();
	}
	catch(const std::exception & e)
	{
		SS_ERROR("%s\n", e.what() ? e.what() : "unknown");
	}
	
	return 0;
}

void SocialRelationDB::GetGuildRank(std::vector<Guild> & vec, unsigned race)
{
	ACE_TRACE("SocialRelationDB::GetGuildRank");

	GuildInfo * guild = new GuildInfo();

	try
	{
		//order_strict<std::string> os(GuildLevel::name());
		//guild->get<GuildLevel>().set_strict(&os);

		equal_strict<unsigned> strict2(race);
		guild->get<GuildOwnerRace>().set_strict(&strict2);

		std::string append;
		append = " order by ";
		append += GuildLevel::name();

		unsigned count = db_->query(*guild, 0, 200, append.c_str());
		if(count > 0)
		{
			const GuildInfo * tmp = guild;
			while(tmp)
			{
				Guild _guild;
				Eval(_guild, *tmp);
				vec.push_back(_guild);
				tmp = tmp->next();
			}
		}

	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("%s\n"), e.what() ? e.what() : "unknown");
	}

	guild->destroy();

}

void SocialRelationDB::Save(const unsigned id, unsigned event, const char * param1, const char * param2, const char * param3, const char * param4)
{
	ACE_TRACE("SocialRelationDB::Save");

	GuildLog * log = new GuildLog;
	if (!log)
		return;

	log->get<GuildLogParam1>().max_size(64);
	log->get<GuildLogParam2>().max_size(64);
	log->get<GuildLogParam3>().max_size(64);
	log->get<GuildLogParam4>().max_size(64);

	log->get<GuildID2>().set(id);
	log->get<GuildLogEvent>().set(event);
	log->get<GuildLogDatetime>().set(time(0) + (event == RelationServiceNS::UNION_LOG_CAPTION_CHANGED_EVENT? 1 : 0));
	if (param1)
		log->get<GuildLogParam1>().set(param1);
	if (param2)
		log->get<GuildLogParam2>().set(param2);
	if (param3)
		log->get<GuildLogParam3>().set(param3);
	if (param4)
		log->get<GuildLogParam4>().set(param4);
	
	try{
		db_->insert(*log);

	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("%s\n"), e.what() ? e.what() : "unknown");
	}

	log->destroy();
}

const unsigned SocialRelationDB::LoadGuildLog(const unsigned id, RelationServiceNS::UnionLogEntry (&vec)[5], const unsigned bookmark, bool & nextPage)
{
	ACE_TRACE("SocialRelationDB::LoadGuildLog");

	nextPage = false;

	GuildLog * log = new GuildLog;
	if (!log)
		return 0;

	log->get<GuildLogParam1>().max_size(64);
	log->get<GuildLogParam2>().max_size(64);
	log->get<GuildLogParam3>().max_size(64);
	log->get<GuildLogParam4>().max_size(64);

	unsigned n = 0;
	
	try{
		order_strict<std::string> os(GuildLogDatetime::name());
		log->get<GuildID2>().set_strict(&os);

		equal_strict<unsigned> eq(id);
		log->get<GuildID2>().set_strict(&eq);

		int count = db_->query(*log, bookmark * 5, 6);
		if(count > 0)
		{
			const GuildLog * tmp = log;
			while(tmp && n < 5)
			{
				vec[n].eventId = tmp->get<GuildLogEvent>().value;
				vec[n].datetime = tmp->get<GuildLogDatetime>().value;

				vec[n].param1.paramLen = StringToBuf(tmp->get<GuildLogParam1>().value, vec[n].param1.param, sizeof(vec[n].param1.param));
				vec[n].param2.paramLen = StringToBuf(tmp->get<GuildLogParam2>().value, vec[n].param2.param, sizeof(vec[n].param2.param));
				
				++n;
				tmp = tmp->next();
			}
		}

		if (count == 6)
			nextPage = true;

	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("%s\n"), e.what() ? e.what() : "unknown");
	}

	log->destroy();

	return n;
}

__SERVICE_SPACE_END_NS__