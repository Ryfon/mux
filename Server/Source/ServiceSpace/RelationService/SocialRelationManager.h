﻿/********************************************************************
	created:	2008/07/26
	created:	26:7:2008   13:34
	filename: 	d:\Projects\mux\Developer\PlatformTools\ServiceSpace\RelationService\SocialRelationManager.h
	file path:	d:\Projects\mux\Developer\PlatformTools\ServiceSpace\RelationService
	file base:	SocialRelationManager
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_SOCIAL_RELATION_MANAGER_H__
#define __SERVICE_SPACE_SOCIAL_RELATION_MANAGER_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "../ServiceSpaceLib/TcpSession.h"
#include "RelationServiceProtocol.h"
#include "MessageTool.h"
#include "Player.h"
#include "Guild.h"
#include "SocialRelationDB.h"
#include "UnionManager.h"
#include "FriendManager.h"
#include "TeamManager.h"
#include "ChatGroupManager.h"
#include <map>
#include <list>
#include <string>
#include <vector>

__SERVICE_SPACE_BEGIN_NS__


class SocialRelationManager : public ACE_Event_Handler
{
public:
	static SocialRelationManager * instance();

	int init(const std::string & str);

	int HandleMessage(tcp_session session, const char * message, const unsigned len);

	Player * GetPlayerByUiid(const unsigned uiid, const bool L = true);
	Player * GetPlayerByName(const std::string & name, const bool L = true);
	Player * GetPlayerByTmpId(const unsigned gateId, const unsigned tmpId);
	Player * GetPlayerByTmpId(const Player::TmpId & id);

	Guild  * GetGuildById(const unsigned id);
	Guild  * GetGuildByName(const std::string & name);
	Guild  * LoadPlayerGuild(const Player & player);

	void AddGuild(Guild & guild);
	void RemoveGuild(Guild & guild);

	void NotifyPlayerFriendGuildInfo(const Player & player);

	TeamManager & GetTeamManager();

	SocialRelationDB & GetDB();

private:
	typedef std::map<unsigned, Player>		PlayerMap;
	typedef std::map<std::string, unsigned> PlayerNameMap;
	typedef std::map<std::string, unsigned>	PlayerIdMap;
	typedef std::map<unsigned, Guild>		GuildMap;
	typedef std::map<std::string, unsigned> GuildNameMap;

	typedef int (SocialRelationManager::*handler_type)(tcp_session, char *, const unsigned);
	typedef std::map<unsigned, handler_type> HandlerMap;

	int handle_timeout(const ACE_Time_Value &tv, const void *act = 0);

	int HandleUpdatePlayerInfoMessage(tcp_session session, char * message, const unsigned len);
	int HandleUpdatePlayerInfo(tcp_session session, const RelationServiceNS::GRInfoUpdate & info);
	int HandlePlayerOffline(tcp_session session, const RelationServiceNS::GRInfoUpdate & info);
	int HandleDeletePlayerMessage(tcp_session session, char * message, const unsigned len);
	int HandleDeletePlayer(tcp_session session, const RelationServiceNS::G_R_DelPlayer & req);
	int HandlePlayerUplineMessage(tcp_session session, char * message, const unsigned len);
	int HandlePlayerUpline(tcp_session session, const RelationServiceNS::GRUplineInfo & info);

	int HandleCreateGuildMessage(tcp_session session, char * message, const unsigned len);
	int HandleInvitePlayerJoinGuildMessage(tcp_session session, char * message, const unsigned len);
	int HandleExitGuildMemberMessage(tcp_session session, char * message, const unsigned len);
	int HandleDestroyGuildMessage(tcp_session session, char * message, const unsigned len);
	int HandleInvitePlayerConfirmedMessage(tcp_session session, char * message, const unsigned len);
	int HandleQueryGuildBasicInfoMessage(tcp_session session, char * message, const unsigned len);
	int HandleQueryGuildMemberListMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyGuildMemberTitleMessage(tcp_session session, char * message, const unsigned len);
	int HandleTransformGuildCaptionMessage(tcp_session session, char * message, const unsigned len);
	int HandleAdvanceGuildMemberPosMessage(tcp_session session, char * message, const unsigned len);
	int HandleReduceGuildMemberPosMessage(tcp_session session, char * message, const unsigned len);
	int HandleGuildChannelSpeekMessage(tcp_session session, char * message, const unsigned len);
	int HandleUpdateGuildPosListMessage(tcp_session session, char * message, const unsigned len);
	int HandleStudyUnionSkillMessage(tcp_session session, char * message, const unsigned len);
	int HandlePostGuildTaskMessage(tcp_session session, char * message, const unsigned len);
	int HandleAdvanceGuildLevelMessage(tcp_session session, char * message, const unsigned len);
	int HandlePostGuildBulletinMessage(tcp_session session, char * message, const unsigned len);
	int HandleQueryGuildLogMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyGuildActiveMessage(tcp_session session, char * message, const unsigned len);
	int HandleForbidGuildMemberMessage(tcp_session session, char * message, const unsigned len);
	int HandleGuildOwnerCheckMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyGuildPrestigeMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyGuildBadgeMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyGuildMultMessage(tcp_session session, char * message, const unsigned len);

	int HandleAddFriendRequestMessage(tcp_session session, char * message, const unsigned len);
	int HandleRemoveFrienRequestMessage(tcp_session session, char * message, const unsigned len);
	int HandleCreeateFriendGroupMessage(tcp_session session, char * message, const unsigned len);
	int HandleDeleteFriendGroupMessage(tcp_session session, char * message, const unsigned len);
	int HandleRenameFriendGroupMessage(tcp_session session, char * message, const unsigned len);
	int HandleTransferFriendMessage(tcp_session session, char * message, const unsigned lenq);
	int HandleAddBlacklistPlayerMessage(tcp_session session, char * message, const unsigned len);
	int HandleRemoveBlacklistPlayerMessage(tcp_session session, char * message, const unsigned len);
	int HandleSendFriendGroupMessage(tcp_session session, char * message, const unsigned len);
	int HandleSendPersonalMessage(tcp_session session, char * message, const unsigned len);
	int HandleAddEnemyMessage(tcp_session session, char * message, const unsigned len);
	int HandleRemoveEnemyMessage(tcp_session session, char * message, const unsigned len);
	int HandleLockEnemyMessage(tcp_session session, char * message, const unsigned len);

	int HandleCreateTeamMessage(tcp_session session, char * message, const unsigned len);
	int HandleDestroyTeamMessage(tcp_session session, char * message, const unsigned len);
	int HandleAddTeamMemberMessage(tcp_session session, char * message, const unsigned len);
	int HandleRemoveTeamMemberMessage(tcp_session session, char * message, const unsigned len);
	int HandleQueryTeamMessage(tcp_session session, char * message, const unsigned len);
	int HandleQueryTeamMemberMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyTeamCaptionMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyTeamExpModeMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyTeamGoodModeMessage(tcp_session session, char * message, const unsigned len);
	int HandleTeamChannelSpeekMessage(tcp_session session, char * message, const unsigned len);
	int HandleModifyTeamRollItemLevelMessage(tcp_session session, char * message, const unsigned len);
	int HandleQueryTeamMemberCountMessage(tcp_session session, char * message, const unsigned len);

	int HandleCreateChatGroupMessage(tcp_session session, char * message, const unsigned len);
	int HandleDestroyChatGroupMessage(tcp_session session, char * message, const unsigned len);
	int HandleAddChatGroupMemberMessage(tcp_session session, char * message, const unsigned len);
	int HandleRemoveChatGroupMember(tcp_session session, char * message, const unsigned len);
	int HandleChatGroupChannelSpeek(tcp_session session, char * message, const unsigned len);

	int HandleQueryPlayerRelationRequest(tcp_session session, char * message, const unsigned len);
	int HandleQueryPlayerInfoRequest(tcp_session session, char * message, const unsigned len);

	int HandleQueryGuildRankRequest(tcp_session session, char * message, const unsigned len);
	int HandleSearchGuildRankRequest(tcp_session session, char * message, const unsigned len);

	int HandleQueryRaceMasterRequest(tcp_session session, char * message, const unsigned len);

	int HandleQueryGuildMembersRequest(tcp_session session, char * message, const unsigned len);

	int HandleAddFriendlyDegreeRequest(tcp_session session, char * message, const unsigned len);
	
	int HandleAddTweetRequest(tcp_session session, char * message, const unsigned len);

	int HandleQueryTweetsRequest(tcp_session session, char * message, const unsigned len);

	int HandleQueryFriendsListRequest(tcp_session session, char * message, const unsigned len);

	int HandleNotifyExpAdded(tcp_session session, char * message, const unsigned len);

	int HandleQueryExpNeededWhenLvlUpResult(tcp_session session, char * message, const unsigned len);

	int HandleTweetFlagModify(tcp_session session, char * message, const unsigned len);

	int HandleQueryPlayerGuild(tcp_session, char * message, const unsigned len);

	int DoGuildDayActiveExpend();
	int DoGuildMemberActiveCal();
	int DoFriendGroupMsgCountReset();

	void AddPlayer(const Player & player);
	void RemovePlayer(const Player & player);
	void LoadAllGuilds();
	void LoadGuildMembers(const unsigned id);

	HandlerMap		handlers_;
	PlayerMap		players_;
	PlayerNameMap	names_;
	PlayerIdMap		tmpIds_;
	GuildMap		guilds_;
	GuildNameMap	guildNames_;

	unsigned maxGuildId_;

	SocialRelationDB db_;
	GuildManager guildManager_;
	FriendManager friendManager_;
	TeamManager teamManager_;
	ChatGroupManager chatGroupManager_;
	bool activeCalFlag_;
	time_t lastCalTime_;

	ACE_Token token_;
};

const bool DiffDayCheck(const time_t t1, const time_t t2);

const unsigned GetSameDayMidnight(const time_t t);

const size_t BuffToString(const char * buf, const size_t len, const size_t maxLen, std::string & str);

const size_t StringToBuf(const std::string & str, char * buf, const size_t maxLen);

__SERVICE_SPACE_END_NS__

#endif
