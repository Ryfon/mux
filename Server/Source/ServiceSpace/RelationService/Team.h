﻿#ifndef __SERVICE_SPACE_TEAM_H__
#define __SERVICE_SPACE_TEAM_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "Player.h"
#include <map>
#include <list>
#include <string>
#include <vector>

__SERVICE_SPACE_BEGIN_NS__


class Team
{
public:
	struct Member
	{
		Player::TmpId tmpId;
		unsigned gainPercent;

		Member(Player::TmpId id) : tmpId(id), gainPercent(0) {  }

	};

	typedef std::vector<Member> MemberList;

	Team();

	~Team();

	const unsigned TeamId()const;
	const Player::TmpId TeamCaption()const;
	const unsigned GoodSharedMode()const;
	const unsigned ExpSharedMode()const;
	const unsigned RollItemLevel()const;
	const MemberList & GetMemberList()const;
	MemberList & GetMemberList();
	const unsigned GetMemberSeq(const Player::TmpId id)const;

	void SetTeamId(const unsigned id);
	void SetTeamCaption(const Player::TmpId id);
	void SetGoodSharedMode(const unsigned m);
	void SetExpSharedMode(const unsigned m);
	void SetRollItemLevel(const unsigned lvl);
	void AddMember(const Player::TmpId id);
	void RemoveMember(const Player::TmpId id);

	const bool IsMember(const Player::TmpId uiid)const;

	unsigned GetExp()const;
	void SetExp(unsigned exp);

private:
	unsigned id_;
	Player::TmpId caption_;
	unsigned expSharedMode_;
	unsigned goodSharedMode_;
	unsigned rollItemLevel_;
	MemberList members_;
	unsigned exp_;
};

__SERVICE_SPACE_END_NS__

#endif