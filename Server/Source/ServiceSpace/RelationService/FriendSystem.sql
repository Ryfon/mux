

 drop database if exists RelationDB;
 create database RelationDB ;
 

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
/*
MySQL Data Transfer
Source Host: localhost
Source Database: relationdb
Target Host: localhost
Target Database: relationdb
Date: 2008-8-26 ���� 10:42:32
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
CREATE TABLE `userinfo` (
  `uiID` bigint(20) NOT NULL,
  `username` varchar(32) NOT NULL,
  `SEX` tinyint(1) NOT NULL COMMENT '1 is male  0 is female ;',
  `playerLevel` smallint(6) NOT NULL,
  `playerClass` smallint(6) NOT NULL,
  `groupdata` varchar(10000) NOT NULL,
  `whoesFriend` varchar(10000) default '',
  `enemy` varchar(10000) default '',
  `blacklist` varchar(10000) default '',
  `whoseEnemy` varchar(10000) default '',
  `lasttime` bigint(20) default 0 ,
  `times` smallint(6) default 0 ,
  PRIMARY KEY  (`uiID`)
)  ;


-- ----------------------------
-- Records 
-- ----------------------------
