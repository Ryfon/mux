﻿/********************************************************************
	created:	2008/11/18
	created:	18:11:2008   9:58
	filename: 	d:\Projects\mux\Developer\PlatformTools\ServiceSpace\RelationService\ChatGroupManager.h
	file path:	d:\Projects\mux\Developer\PlatformTools\ServiceSpace\RelationService
	file base:	ChatGroupManager
	file ext:	h
	author:		geqinglong
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_RELATION_SERVICE_CHAT_GROUP_MANAGER_H__
#define __SERVICE_SPACE_RELATION_SERVICE_CHAT_GROUP_MANAGER_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "../ServiceSpaceLib/utilib/pool_lock.h"
#include "RelationServiceProtocol.h"
#include "MessageHandler.h"
#include "TeamManager.h"
#include "ChatGroup.h"

#include <map>

__SERVICE_SPACE_BEGIN_NS__

class SocialRelationManager;

class ChatGroupManager
{
public:
	~ChatGroupManager();

private:
	ChatGroupManager();

	int HandleCreateGroup(tcp_session session, const RelationServiceNS::CreateChatGroupRequest & req);
	int HandleDestroyGroup(tcp_session session, const RelationServiceNS::DestroyChatGroupRequest & req);
	int HandleAddMember(tcp_session session, const RelationServiceNS::AddChatGroupMemberRequest & req);
	int HandleRemoveMember(tcp_session session, const RelationServiceNS::RemoveChatGroupMemberRequest & req);
	int HandleGroupChannelSpeek(tcp_session session, const RelationServiceNS::ChatGroupSpeekRequest & req);
	int HandlePlayerOffline(Player & player);

	void SendGroupCreatedNotify(const ChatGroup & group);
	void SendGroupDestroyedNotify(const ChatGroup & group);
	void SendMemberAddedNotify(const Player::TmpId & player, const ChatGroup & group);
	void SendMemberRemovedNotify(const Player::TmpId & player, const ChatGroup & group);
	void SendGroupChannelSpeekNotify(const RelationServiceNS::ChatData & data, const std::string & player, const ChatGroup & group);

	typedef std::map<unsigned, ChatGroup> ChatGroupMap;	

	ChatGroupMap groups_;
	unsigned maxGroupId_;

	friend class SocialRelationManager;

}; // class ChatGroupManager

__SERVICE_SPACE_END_NS__

#endif