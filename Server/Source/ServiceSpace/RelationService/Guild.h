﻿#ifndef __SERVICE_SPACE_GUILD_H__
#define __SERVICE_SPACE_GUILD_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/utilib/basic_text_istream.h"
#include "../ServiceSpaceLib/utilib/basic_text_ostream.h"
#include "RelationServiceProtocol.h"
#include "Player.h"
#include <map>
#include <list>
#include <string>
#include <vector>
#include <algorithm>

__SERVICE_SPACE_BEGIN_NS__

class Guild;

utilib::basic_text_ostream & operator<<(utilib::basic_text_ostream & s, const Guild & guild);

utilib::basic_text_istream & operator>>(utilib::basic_text_istream & s, Guild & guild);

class Guild
{
public:
	enum {STATE_NORMAL = 0, STATE_LOWACTIVE};

	struct PosInfo
	{
		unsigned idx;
		std::string name;
		unsigned right;

		PosInfo(): idx(0), right(0) { }
	};

	struct TaskPostInfo
	{
		unsigned postTime;
		unsigned taskPeriod;

		TaskPostInfo(): postTime(0), taskPeriod(0) { }
	};

	struct SkillInfo
	{
		unsigned needGuildLevel;
		unsigned skillId;
		unsigned skillLevel;
		unsigned preSkillId;
		unsigned nextSkillId;
		unsigned expendActive;

		SkillInfo() : needGuildLevel(0), skillId(0), skillLevel(0), preSkillId(0), nextSkillId(0), expendActive(0)
		{ }

		const bool operator==(const SkillInfo & other)const { return skillId == other.skillId; }

		const bool operator==(const unsigned other)const { return skillId == other; }
	};

	struct MultiInfo
	{
		unsigned type;
		unsigned endTime;
		unsigned multi;

		MultiInfo() : type(0), endTime(0), multi(0) { }
	};

	typedef std::vector<unsigned> MemberList;
	typedef std::vector<unsigned> SkillList;
	typedef std::vector<PosInfo> PosList;

	Guild(const unsigned guildId = Player::INVALID_GUILD_ID);

	~Guild();

	const unsigned		GuildId()const;
	const std::string & GuildName()const;
	const unsigned		GuildOwner()const;
	const int			State()const;
	const unsigned		Level()const;
	const unsigned		Active()const;
	const MemberList &  GetMemberList()const;
	const unsigned		GetMemberCount()const;
	const SkillList &   GetSkillList()const;
	const PosList	&	GetPosList()const;
	const TaskPostInfo & GetTaskPostInfo()const;
	const unsigned       GetMaxPosIdx()const;
	const std::string &  GetBulletin()const;
	const unsigned		 GetMaxMemberCount()const;
	const unsigned		 Prestige()const;
	const std::string &  GetBadge()const;
	const MultiInfo   &  GetMulti(unsigned type);
	const unsigned       GetLastDayExpendCalTime()const;
	const std::map<unsigned, MultiInfo> & GetMultis()const;

	void GuildId(const unsigned id);
	void GuildName(const std::string & name);
	void GuildOwner(const unsigned uiid);
	void State(const int s);
	void Level(const unsigned lvl);
	void Prestige(const unsigned);
	void AddActive(int d);
	void AddMember(const unsigned uiid);
	void RemoveMember(const unsigned uiid);
	void AddSkill(const unsigned skill);
	void RemoveSkill(const unsigned skill);
	void SetSkillList(SkillList & lst);
	void AddPos(PosInfo & pos);
	void RemovePos(const PosInfo & pos);
	void SetTaskPostInfo(const TaskPostInfo & info);
	void SetPosList(const PosList & lst);
	void SetBulletin(const std::string & str);
	void SetMaxMemberCount(const unsigned cout);
	void AddPrestige(const unsigned val, const bool add);
	void SetBadge(const std::string & bge);
	void SetMulti(const MultiInfo & m);
	void SetDayExpendCalTime(const unsigned t);

	const bool IsMember(const Player & player)const;
	const bool IsMember(const unsigned uiid)const;
	const bool IsSkillStudyed(const unsigned skill)const;
	const bool IsHasRight(const unsigned pos, const unsigned op)const;
	const int GetPosDiff(const unsigned p1, const unsigned p2)const;
	const bool CheckPostTaskTiming()const;

	friend utilib::basic_text_ostream & operator<<(utilib::basic_text_ostream & s, const Guild & guild);

	friend utilib::basic_text_istream & operator>>(utilib::basic_text_istream & s, Guild & guild);

private:
	unsigned id_;
	std::string name_;
	unsigned owner_;
	unsigned level_;
	unsigned active_;
	int		 state_;
	unsigned maxMemberCount_;
	std::string bulletin_;
	unsigned prestige_;
	std::string badge_;
	std::map<unsigned, MultiInfo> multis_;
	unsigned lastDayExpendCalTime_;
	
	MemberList members_;
	PosList poss_;
	SkillList skills_;
	TaskPostInfo taskPost_;
};

__SERVICE_SPACE_END_NS__

#endif