﻿#include "Guild.h"
#include <limits>

__SERVICE_SPACE_BEGIN_NS__

utilib::basic_text_ostream & operator<<(utilib::basic_text_ostream & s, const Guild::TaskPostInfo & info)
{
	s << info.postTime << info.taskPeriod;
	return s;
}

utilib::basic_text_istream & operator>>(utilib::basic_text_istream & s, Guild::TaskPostInfo & info)
{
	s >> info.postTime >> info.taskPeriod;
	return s;
}

utilib::basic_text_ostream & operator<<(utilib::basic_text_ostream & s, const Guild::PosInfo & info)
{
	s << info.idx << info.name << info.right;
	return s;
}

utilib::basic_text_istream & operator>>(utilib::basic_text_istream & s, Guild::PosInfo & info)
{
	s >> info.idx >> info.name >> info.right;
	return s;
}

utilib::basic_text_ostream & operator<<(utilib::basic_text_ostream & s, const Guild & guild)
{
	s << guild.Level()
	  << guild.GuildOwner()
	  << guild.Active()
	  << guild.GetPosList()
	  << guild.GetSkillList()
	  << guild.GetTaskPostInfo()
	  << guild.State()
	  << guild.GetMaxMemberCount()
	  << guild.Prestige()
	  << guild.GetBadge()
	  << guild.GetLastDayExpendCalTime();
	return s;
}

utilib::basic_text_istream & operator>>(utilib::basic_text_istream & s, Guild & guild)
{
	unsigned _active = 0;
	s >> guild.level_
	  >> guild.owner_
	  >> _active
	  >> guild.poss_
	  >> guild.skills_
	  >> guild.taskPost_
	  >> guild.state_
	  >> guild.maxMemberCount_;
	if (s.get_buffer().size())
		s >> guild.prestige_;
	if (s.get_buffer().size())
		s >> guild.badge_;
	if (s.get_buffer().size())
		s >> guild.lastDayExpendCalTime_;
	return s;
}

Guild::Guild(const unsigned int guildId)
: id_(guildId)
, owner_(0)
, level_(0)
, active_(0)
, state_(0)
, maxMemberCount_(0)
, prestige_(0)
, lastDayExpendCalTime_(0)
{
	lastDayExpendCalTime_ = time(0);
}

Guild::~Guild()
{
}

const unsigned Guild::GuildId()const
{
	return id_;
}

const std::string & Guild::GuildName()const
{
	return name_;
}

const unsigned Guild::GuildOwner()const
{
	return owner_;
}

const int Guild::State()const
{
	return state_;
}

const unsigned Guild::Level()const
{
	return level_;
}

const unsigned Guild::Active()const
{
	return active_;
}

const Guild::MemberList & Guild::GetMemberList()const
{
	return members_;
}

const unsigned Guild::GetMemberCount()const
{
	return members_.size();
}

const Guild::PosList & Guild::GetPosList()const
{
	return poss_;
}

const Guild::SkillList & Guild::GetSkillList()const
{
	return skills_;
}

const Guild::TaskPostInfo & Guild::GetTaskPostInfo()const
{
	return taskPost_;
}

const unsigned Guild::GetMaxPosIdx()const
{
	unsigned res = 0;
	for(PosList::const_iterator it = poss_.begin(); it != poss_.end(); ++it)
	{
		if (res < it->idx)
			res = it->idx;
	}
	return res;
}

const std::string &  Guild::GetBulletin()const
{
	return bulletin_;
}

const unsigned Guild::GetMaxMemberCount()const
{
	return maxMemberCount_;
}

const unsigned Guild::Prestige()const
{
	return prestige_;
}

void Guild::Prestige(const unsigned p)
{
	prestige_ = p;
}

const std::string &  Guild::GetBadge()const
{
	return badge_;
}

const Guild::MultiInfo & Guild::GetMulti(unsigned type)
{
	return multis_[type];
}

const unsigned Guild::GetLastDayExpendCalTime()const
{
	return lastDayExpendCalTime_;
}

const std::map<unsigned, Guild::MultiInfo> & Guild::GetMultis()const
{
	return multis_;
}

void Guild::GuildId(const unsigned id)
{
	id_ = id;
}

void Guild::GuildName(const std::string & name)
{
	name_ = name;
}

void Guild::GuildOwner(const unsigned uiid)
{
	owner_ = uiid;
}

void Guild::State(const int s)
{
	state_ = s;
}

void Guild::Level(const unsigned lvl)
{
	level_ = lvl;
}

void Guild::AddActive(int d)
{
	if (d == 0)
		return ;

	if (d > 0)
	{		
		if (active_ <= UINT_MAX - d)
			active_ += d;
		else
			active_ = UINT_MAX;
	}
	else if (active_ >= (-1 * d))
		active_ += d;
	else
		active_ = 0;
}

void Guild::AddMember(const unsigned uiid)
{
	members_.push_back(uiid);
}

void Guild::RemoveMember(const unsigned uiid)
{
	for(MemberList::iterator it = members_.begin(); it != members_.end(); ++it)
	{
		if (*it == uiid)
		{
			if (uiid == owner_)
				owner_ = 0;
			members_.erase(it);
			break;
		}
	}
}

void Guild::AddSkill(const unsigned skill)
{
	skills_.push_back(skill);
}

void Guild::RemoveSkill(const unsigned skill)
{
	for(SkillList::iterator it = skills_.begin(); it != skills_.end(); ++it)
	{
		if (skill == *it)
		{
			skills_.erase(it);
			break;
		}
	}
}

void Guild::SetSkillList(SkillList & lst)
{
	skills_.clear();
	skills_ = lst;
}

void Guild::AddPos(PosInfo & pos)
{
	pos.idx = GetMaxPosIdx();
	++pos.idx;
	poss_.push_back(pos);
}

void Guild::RemovePos(const PosInfo & pos)
{
	for(PosList::iterator it = poss_.begin(); it != poss_.end(); ++it)
	{
		if (it->idx == pos.idx && pos.name == it->name)
		{
			poss_.erase(it);
			break;
		}
	}
}

void Guild::SetTaskPostInfo(const TaskPostInfo & info)
{
	taskPost_ = info;
}

void Guild::SetPosList(const PosList & lst)
{
	poss_ = lst;
}

void Guild::AddPrestige(const unsigned val, const bool add)
{
	if (add)
	{
		if (prestige_ <= UINT_MAX - val)
			prestige_ += val;
		else
			prestige_ = UINT_MAX;
	}
	else
	{
		if (prestige_ > val)
			prestige_ -= val;
		else
			prestige_ = 0;
	}
}

void Guild::SetBadge(const std::string & bge)
{
	badge_ = bge;
}

void Guild::SetMulti(const MultiInfo & m)
{
	multis_[m.type] = m;
}

void Guild::SetDayExpendCalTime(const unsigned t)
{
	lastDayExpendCalTime_ = t;
}

void Guild::SetMaxMemberCount(const unsigned count)
{
	maxMemberCount_ = count;
}

void Guild::SetBulletin(const std::string & str)
{
	bulletin_ = str;
}

const bool Guild::IsMember(const Player & player)const
{
	return IsMember(player.Uiid());
}

const bool Guild::IsMember(const unsigned uiid)const
{
	for(MemberList::const_iterator it = members_.begin(); it != members_.end(); ++it)
	{
		if (*it == uiid)
		{
			return true;
		}
	}

	return false;
}

const bool Guild::IsHasRight(const unsigned pos, const unsigned op)const
{
	for(PosList::const_iterator it = poss_.begin(); it != poss_.end(); ++it)
	{
		if (it->idx == pos)
		{
			return (it->right & op) != 0;
		}
	}
	return false;
}

const int Guild::GetPosDiff(const unsigned p1, const unsigned p2)const
{
	int i = 0, j = 0;
	for(PosList::const_iterator it = poss_.begin(); it != poss_.end(); ++it)
	{
		if (it->idx == p1)
			break;
		++i;
	}

	for(PosList::const_iterator it = poss_.begin(); it != poss_.end(); ++it)
	{
		if (it->idx == p2)
			break;
		++j;
	}

	return j - i;
}


const bool Guild::CheckPostTaskTiming()const
{
	if (!taskPost_.postTime)
		return true;

	time_t t = time(0);
	tm * pTm = localtime(&t);
	tm tmNow = *pTm;
	t = taskPost_.postTime;
	pTm = localtime(&t);
	tm tmPost = *pTm;

	if (tmNow.tm_year != tmPost.tm_year || tmNow.tm_mon != tmPost.tm_mon || tmNow.tm_mday != tmPost.tm_mday)
		return true;

	return false;
}

__SERVICE_SPACE_END_NS__