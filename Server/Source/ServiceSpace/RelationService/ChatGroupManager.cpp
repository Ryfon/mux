﻿#define ACE_NTRACE 0
#include "ChatGroupManager.h"
#include "SocialRelationManager.h"
#include <algorithm>

__SERVICE_SPACE_BEGIN_NS__

void Eval(RelationServiceNS::ChatGroupMember & l, const Player::TmpId & r)
{
	l.gateId = r.gateId;
	l.playerId = r.playerId;
	l.memberId.memberIndLen = 0;

	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(r.gateId, r.playerId);
	if (player)
		l.memberId.memberIndLen = StringToBuf(player->Name(), l.memberId.memberInd, sizeof(l.memberId.memberInd));
}

void Eval(RelationServiceNS::ChatGroup & l, const ChatGroup & r)
{
	l.groupId = r.GroupId();
	Eval(l.owner, r.GroupOwner());
	l.password.passwordLen = 0;
	l.members.membersCount = 0;

	for(ChatGroup::MemberList::const_iterator it = r.GetMemberList().begin();
		it != r.GetMemberList().end() && l.members.membersCount < RelationServiceNS::MAX_CHAT_GROUP_MEMBERS_COUNT; ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(*it);
		if (!player)
			continue;

		l.members.members[l.members.membersCount].memberIndLen = StringToBuf(player->Name(), l.members.members[l.members.membersCount].memberInd,
			sizeof(l.members.members[l.members.membersCount].memberInd));
		 ++l.members.membersCount;
	}
}

ChatGroupManager::ChatGroupManager()
: maxGroupId_(0)
{
}

ChatGroupManager::~ChatGroupManager()
{
}

int ChatGroupManager::HandleCreateGroup(tcp_session session, const RelationServiceNS::CreateChatGroupRequest & req)
{
	ACE_TRACE("ChatGroupManager::HandleCreateGroup");

	RelationServiceNS::CreateChatGroupResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.group = req.group;

	Player * player  = SocialRelationManager::instance()->GetPlayerByTmpId(req.group.owner.gateId, req.group.owner.playerId);
	if (!player)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (player->GetChatGroupId() != Player::INVALID_CHATGROUP_ID)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_MEMBER_IN_OTHER_TEAM;
	}
	else
	{
		ChatGroup group;
		group.GroupId(++maxGroupId_);
		group.AddMember(Player::TmpId(req.group.owner.gateId, req.group.owner.playerId));
		
		std::string passwrd;
		BuffToString(req.group.password.password, req.group.password.passwordLen, sizeof(req.group.password.password), passwrd);
		group.SetChatPasswrd(passwrd);

		group.GroupOwner(Player::TmpId(req.group.owner.gateId, req.group.owner.playerId));

		player->SetChatGroupId(group.GroupId());

		groups_[group.GroupId()] = group;

		Eval(result.group, group);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::CreateChatGroupResult::wCmd);

	return 0;
}

int ChatGroupManager::HandleDestroyGroup(tcp_session session, const RelationServiceNS::DestroyChatGroupRequest & req)
{
	ACE_TRACE("ChatGroupManager::HandleDestroyGroup");

	RelationServiceNS::DestroyChatGroupResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = req.gateId;
	result.playerId = req.playerId;
	result.groupId = req.groupId;

	ChatGroupMap::iterator groupIt = groups_.find(req.groupId);
	if (groupIt == groups_.end())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (!(groupIt->second.GroupOwner() == Player::TmpId(req.gateId, req.playerId)))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_IS_NOT_OWNER;
	}
	else
	{
		for(ChatGroup::MemberList::const_iterator it = groupIt->second.GetMemberList().begin(); it != groupIt->second.GetMemberList().end(); ++it)
		{
			Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(*it);
			if (!player)
				continue;

			player->SetChatGroupId(Player::INVALID_CHATGROUP_ID);
		}

		SendGroupDestroyedNotify(groupIt->second);

		groups_.erase(groupIt);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::DestroyChatGroupResult::wCmd);

	return 0;
}

int ChatGroupManager::HandleAddMember(tcp_session, const RelationServiceNS::AddChatGroupMemberRequest & req)
{
	ACE_TRACE("ChatGroupManager::HandleAddMember");

	RelationServiceNS::AddChatGroupMemberResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = req.gateId;
	result.playerId = req.playerId;

	std::string passwrd;
	BuffToString(req.passwrod.password, req.passwrod.passwordLen, sizeof(req.passwrod.password), passwrd);

	Player * target = 0;
	ChatGroupMap::iterator groupIt = groups_.find(req.groupId);
	if (groupIt == groups_.end())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (!(groupIt->second.GroupOwner() == Player::TmpId(req.gateId, req.playerId)) && groupIt->second.GetChatPasswrd() != passwrd)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_IS_NOT_OWNER;
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByTmpId(req.memberAdded.gateId, req.memberAdded.playerId)) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (target->GetChatGroupId() != Player::INVALID_CHATGROUP_ID)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_MEMBER_IN_OTHER_TEAM;			
	}
	else if (groupIt->second.GetMemberList().size() >= RelationServiceNS::MAX_CHAT_GROUP_MEMBERS_COUNT)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_GROUP_FULL;
	}
	else
	{
		Player::TmpId tmpId(req.memberAdded.gateId, req.memberAdded.playerId);
		groupIt->second.AddMember(tmpId);

		target->SetChatGroupId(groupIt->second.GroupId());

		SendMemberAddedNotify(tmpId, groupIt->second);

		Eval(result.group, groupIt->second);

		result.gateId = req.memberAdded.gateId;
		result.playerId = req.memberAdded.playerId;
	}

	tcp_session session = RelationServiceMessageHandler::get_session(result.gateId);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::AddChatGroupMemberResult::wCmd);

	return 0;
}

int ChatGroupManager::HandleRemoveMember(tcp_session session, const RelationServiceNS::RemoveChatGroupMemberRequest & req)
{
	ACE_TRACE("ChatGroupManager::HandleRemoveMember");
	
	RelationServiceNS::RemoveChatGroupMemberResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.memberRemoved = req.memberRemoved;
	result.gateId = req.memberRemoved.gateId;
	result.playerId = req.memberRemoved.playerId;

	Player * target = SocialRelationManager::instance()->GetPlayerByTmpId(req.memberRemoved.gateId, req.memberRemoved.playerId);
	ChatGroupMap::iterator groupIt;
	if (!target)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (target->GetChatGroupId() == Player::INVALID_CHATGROUP_ID)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if ((groupIt = groups_.find(target->GetChatGroupId())) == groups_.end())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (!groupIt->second.IsMember(Player::TmpId(req.memberRemoved.gateId, req.memberRemoved.playerId)))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_MEMBER;
	}
	else 
	{
		Player::TmpId owner = groupIt->second.GroupOwner();
		Player::TmpId tmpId(req.memberRemoved.gateId, req.memberRemoved.playerId);

		groupIt->second.RemoveMember(tmpId);

		target->SetChatGroupId(Player::INVALID_CHATGROUP_ID);

		result.groupId = groupIt->second.GroupId();

		if (owner == tmpId)
		{
			RelationServiceNS::DestroyChatGroupRequest req2;
			memset(&req2, 0, sizeof(req2));
			req2.groupId = groupIt->second.GroupId();
			req2.gateId = tmpId.gateId;
			req2.playerId = tmpId.playerId;

			return HandleDestroyGroup(session, req2);
		}		
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RemoveChatGroupMemberResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_SUCCEED)
		SendMemberRemovedNotify(Player::TmpId(req.memberRemoved.gateId, req.memberRemoved.playerId), groupIt->second);

	return 0;
}

int ChatGroupManager::HandleGroupChannelSpeek(tcp_session session, const RelationServiceNS::ChatGroupSpeekRequest & req)
{
	ACE_TRACE("ChatGroupManager::HandleGroupChannelSpeek");

	RelationServiceNS::ChatGroupSpeekResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = req.gateId;
	result.playerId = req.playerId;

	ChatGroupMap::const_iterator groupIt;
	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.playerId);
	if (!player)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (player->GetChatGroupId() == Player::INVALID_CHATGROUP_ID)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if ((groupIt = groups_.find(player->GetChatGroupId())) == groups_.end())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else
	{
		SendGroupChannelSpeekNotify(req.data, player->Name(), groupIt->second);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::ChatGroupSpeekResult::wCmd);

	return 0;
}

int ChatGroupManager::HandlePlayerOffline(Player & player)
{
	ACE_TRACE("ChatGroupManager::HandlePlayerOffline");

	if (player.GetChatGroupId() == Player::INVALID_CHATGROUP_ID)
		return 0;

	ChatGroupMap::iterator it = groups_.find(player.GetChatGroupId());
	if (it == groups_.end())
	{
		player.SetChatGroupId(Player::INVALID_CHATGROUP_ID);
		return -1;
	}

	RelationServiceNS::RemoveChatGroupMemberRequest req;
	memset(&req, 0, sizeof(req));
	req.gateId = player.GateId();
	req.playerId = player.PlayerId();
	req.memberRemoved.gateId = player.GateId();
	req.memberRemoved.playerId = player.PlayerId();
	req.groupId = it->second.GroupId();
	
	HandleRemoveMember(RelationServiceMessageHandler::get_session(player.GateId()), req);

	player.SetChatGroupId(Player::INVALID_CHATGROUP_ID);
	
	return 0;
}

void ChatGroupManager::SendGroupCreatedNotify(const ChatGroup & group)
{
	ACE_TRACE("ChatGroupManager::SendGroupCreatedNotify");
}

void ChatGroupManager::SendGroupDestroyedNotify(const ChatGroup & group)
{
	ACE_TRACE("ChatGroupManager::SendGroupDestroyedNotify");

	RelationServiceNS::ChatGroupDestroyedNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.groupId = group.GroupId();

	for(ChatGroup::MemberList::const_iterator it = group.GetMemberList().begin(); it != group.GetMemberList().end(); ++it)
	{
		notify.gateId = it->gateId;
		notify.playerId = it->playerId;

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::ChatGroupDestroyedNotify::wCmd);
	}
}

void ChatGroupManager::SendMemberAddedNotify(const Player::TmpId & player, const ChatGroup & group)
{
	ACE_TRACE("ChatGroupManager::SendMemberAddedNotify");

	RelationServiceNS::ChatGroupMemberAddedNotify notify;
	memset(&notify, 0, sizeof(notify));
	Eval(notify.memberAdded, player);

	for(ChatGroup::MemberList::const_iterator it = group.GetMemberList().begin(); it != group.GetMemberList().end(); ++it)
	{
		if (player.gateId == it->gateId && player.playerId == it->playerId)
			continue;

		notify.gateId = it->gateId;
		notify.playerId = it->playerId;

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::ChatGroupMemberAddedNotify::wCmd);
	}
}

void ChatGroupManager::SendMemberRemovedNotify(const Player::TmpId & player, const ChatGroup & group)
{
	ACE_TRACE("ChatGroupManager::SendMemberRemovedNotify");

	RelationServiceNS::ChatGroupMemberRemovedNotify notify;
	memset(&notify, 0, sizeof(notify));
	Eval(notify.memberRemoved, player);

	for(ChatGroup::MemberList::const_iterator it = group.GetMemberList().begin(); it != group.GetMemberList().end(); ++it)
	{
		notify.gateId = it->gateId;
		notify.playerId = it->playerId;

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::ChatGroupMemberRemovedNotify::wCmd);
	}
}

void ChatGroupManager::SendGroupChannelSpeekNotify(const RelationServiceNS::ChatData & data, const std::string & player, const ChatGroup & group)
{
	ACE_TRACE("ChatGroupManager::SendGroupChannelSpeekNotify");

	RelationServiceNS::ChatGroupSpeekNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.data = data;
	notify.groupId = group.GroupId();
	notify.talker.memberIndLen = StringToBuf(player, notify.talker.memberInd, sizeof(notify.talker.memberInd));

	for(ChatGroup::MemberList::const_iterator it = group.GetMemberList().begin(); it != group.GetMemberList().end(); ++it)
	{
		notify.gateId = it->gateId;
		notify.playerId = it->playerId;

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::ChatGroupSpeekNotify::wCmd);
	}
}

__SERVICE_SPACE_END_NS__