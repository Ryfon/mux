﻿#define ACE_NTRACE 0
#include "UnionManager.h"
#include "FriendManager.h"
#include <algorithm>
#ifndef WIN32
#ifndef TIXML_USE_STL
#define TIXML_USE_STL
#endif
#endif
#include <tinyxml.h>
#include <sstream>

#include "SocialRelationDB.h"
#include "SocialRelationManager.h"

namespace std 
{
	const bool operator<(const UnionRankInfo & l, const UnionRankInfo & r)
	{
		return l.unionLevel > r.unionLevel || 
			(l.unionLevel == r.unionLevel && l.unionPretige > r.unionPretige);
	}
}

__SERVICE_SPACE_BEGIN_NS__



struct GuildMember : RelationServiceNS::UnionMember
{
	unsigned joinTime;
};

void Eval(RelationServiceNS::UnionMember & l, const Player & r)
{
	l.gateId = r.GateId();
	l.playerId = r.PlayerId();
	l.uiid = r.Uiid();
	l.nameLen = StringToBuf(r.Name(), l.name, sizeof(l.name));
	l.job = r.Job();
	l.posSeq = r.GetGuildInfo().guildPos;
	l.titleLen = StringToBuf(r.GetGuildInfo().guildTitle, l.title, sizeof(l.title));
	l.level = r.Level();
	l.lastQuit = r.GetGuildInfo().exitTime;
	l.forbid = r.GetGuildInfo().forbid;
	l.onLine = r.LineState();
}

void Eval(RelationServiceNS::UnionPosRight & l, const Guild::PosList::value_type & r)
{
	l.seq = r.idx;
	l.right = r.right;
	l.posLen = StringToBuf(r.name, l.pos, sizeof(l.pos));
}

const unsigned Eval(RelationServiceNS::UnionPosRight (&l)[10], const Guild::PosList & lst)
{
	size_t i = 0;
	for(Guild::PosList::const_iterator it = lst.begin(); it != lst.end() && i < 10; ++it, ++i)
	{
		Eval(l[i], *it);
	}

	return i;
}

const unsigned Eval(Guild::PosList & lst, const RelationServiceNS::UnionPosRight * l, const unsigned len)
{
	for(unsigned i = 0; i < 10 && i < len; ++i)
	{
		Guild::PosList::value_type val;
		val.idx = l[i].seq;
		BuffToString(l[i].pos, l[i].posLen, sizeof(l[i].pos), val.name);
		val.right = l[i].right;
		lst.push_back(val);
	}
	return lst.size();
}

void Eval(RelationServiceNS::Union & l, const Guild & r)
{
	l.id = r.GuildId();
	l.nameLen = StringToBuf(r.GuildName(), l.name, sizeof(l.name));                            	
	l.level = r.Level();                              	
	l.num = r.GetMemberList().size();                          
	l.ownerUiid = r.GuildOwner();                          	
	l.titleLen = 0;                          
	l.active = r.Active();                             	
	Eval(l.posList, r.GetPosList());
	l.posListLen = r.GetPosList().size();
}

const bool operator<(const GuildMember & l, const GuildMember & r)
{
	return l.joinTime < r.joinTime;
}

const bool operator==(const Guild::PosInfo & l, const Guild::PosInfo & r)
{
	return l.idx == r.idx;
}

const bool operator==(const Guild::SkillInfo & l, const Guild::SkillInfo & r)
{
	return l.skillId == r.skillId;
}

struct SkillCheck
{
	const unsigned skillId;

	SkillCheck(const unsigned id): skillId(id) {}

	const bool operator()(const Guild::SkillInfo & r)const
	{
		return skillId == r.skillId;
	}
};

struct PosCheck
{
	const unsigned guildPos;

	PosCheck(const unsigned pos): guildPos(pos) {}

	const bool operator()(const Guild::PosInfo & r)const
	{
		return guildPos == r.idx;
	}
};

GuildManager::GuildManager()
{
	LoadGuildBasicCfg();
	LoadGuildSkillCfg();
	LoadGuildSkillShopCfg();
}	

GuildManager::~GuildManager()
{
}

int GuildManager::HandleCreateGuild(tcp_session session, const RelationServiceNS::CreateUnionRequest & req)
{
	ACE_TRACE("GuildManager::HandleCreateGuild");

	RelationServiceNS::CreateUnionResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;
	
	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req._union.ownerUiid, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	std::string name;
	BuffToString(req._union.name, req._union.nameLen, sizeof(req._union.name), name);

	SS_DEBUG("%s is creating guild[%s]\n", player->Name().c_str(), name.c_str());

	if (player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
		result.returnCode = RelationServiceNS::R_G_RESULT_IN_OTHER_UNION;
	else if (player->GetGuildInfo().exitTime && time(0) - player->GetGuildInfo().exitTime < 3600 * 24)
		result.returnCode = RelationServiceNS::R_G_RESULT_IN_OTHER_UNION;
	else if (SocialRelationManager::instance()->GetGuildByName(name) ||
		SocialRelationManager::instance()->GetDB().CheckGuildNameRepeat(name))
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_NAME_REPEATED;
	else if (player->Level() < 20)
		result.returnCode = RelationServiceNS::R_G_RESULT_NEED_HIGHER_LEVEL;

	Guild guild;
	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		result.captionNameLen = StringToBuf(player->Name().c_str(), result.captionName, sizeof(result.captionName));
		guild.Level(1);
		guild.SetMaxMemberCount(GetMaxCount(guild.Level()));
		guild.GuildName(name);
		guild.GuildOwner(player->Uiid());
		guild.AddMember(player->Uiid());
		guild.SetBulletin("盟主每天可至战盟管理员处发布日常任务。积累战盟活跃度好处多多，"
			"消耗一定的活跃度可进行战盟升级、购买战盟技能、扩充战盟仓库等（双击公告栏区域可更改战盟公告信息）");
		SocialRelationManager::instance()->AddGuild(guild);
		if (SocialRelationManager::instance()->GetDB().FirstSave(guild))
		{
			SocialRelationManager::instance()->GetDB().Save(
			guild.GuildId(), RelationServiceNS::UNION_LOG_CREATE_EVENT, player->Name().c_str(), 0);

			Player::GuildInfo info;
			info.guildId = guild.GuildId();
			info.joinTime = time(0);
			player->SetGuildInfo(info);

			SocialRelationManager::instance()->GetDB().Save(*player);

			Eval(result._union, guild);
		}
		else
		{
			SocialRelationManager::instance()->RemoveGuild(guild);
			result.returnCode = RelationServiceNS::R_G_RESULT_SERVICE_ERROR;
		}
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::CreateUnionResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildMemberList(*player, guild);	

		SendGuildBulletinNotify(*player, guild);

		SocialRelationManager::instance()->NotifyPlayerFriendGuildInfo(*player);

		UpdateGuildRank(player->Uiid(), guild);
	}
	return 0;
}

int GuildManager::HandleDestroyGuild(tcp_session session, const RelationServiceNS::DestroyUnionRequest & req)
{
	ACE_TRACE("GuildManager::HandleDestroyGuild");

	RelationServiceNS::DestroyUnionResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid);
	if (!player)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	result.returnCode = RelationServiceNS::R_G_RESULT_PLAYER_NO_UNION;
	if (player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
		if (guild)
		{	
			if (guild->GetMemberList().size() > 1)
				return -1;

			//SendGuildDestroyedNotify(*guild);
			RemoveGuildRank(player->Race(), guild->GuildId());
			SocialRelationManager::instance()->GetDB().Destroy(*guild);
			SocialRelationManager::instance()->RemoveGuild(*guild);

			result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;
		}

		Player::GuildInfo info;
		info.guildId = Player::INVALID_GUILD_ID;
		info.exitTime = time(0);
		player->SetGuildInfo(info);
		SocialRelationManager::instance()->GetDB().Save(*player);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::DestroyUnionResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SocialRelationManager::instance()->NotifyPlayerFriendGuildInfo(*player);
	}

	return 0;
}

int GuildManager::HandleQueryGuildBasicInfo(tcp_session session, const RelationServiceNS::QueryUnionBasicRequest & req)
{
	ACE_TRACE("GuildManager::HandleQueryGuildBasicInfo");

	RelationServiceNS::QueryUnionBasicResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	if (player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
		if (guild)
		{
			SendGuildBasicInfo(*player, *guild);
			SendGuildSkillList(*player, *guild);
			if (guild->GetTaskPostInfo().postTime)
				SendGuildMemberTaskPost(*player, *guild);
			return 0;
		}
	}

	/*result.returnCode = RelationServiceNS::R_G_RESULT_PLAYER_NO_UNION;

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryUnionBasicResult::wCmd);*/

	return 0;
}

int GuildManager::HandleQueryGuildMemberList(tcp_session session, const RelationServiceNS::QueryUnionMembersRequest & req)
{
	ACE_TRACE("GuildManager::HandleQueryGuildMemberList");

	RelationServiceNS::QueryUnionMembersResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	if (player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
		if (guild)
		{
			return SendGuildMemberList(*player, *guild);
		}
	}

	result.returnCode = RelationServiceNS::R_G_RESULT_PLAYER_NO_UNION;

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryUnionMembersResult::wCmd);

	return 0;
}

int GuildManager::HandleInvitePlayerJoinGuild(tcp_session session, const RelationServiceNS::AddUnionMemberRequest   & req)
{
	ACE_TRACE("GuildManager::HandleInvitePlayerJoinGuild");

	RelationServiceNS::AddUnionMemberResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	Guild * guild = 0;
	Player * invitee = 0;
	std::string inviteeName;
	BuffToString(req.memberAddedName, req.memberAddedNameLen, sizeof(req.memberAddedName), inviteeName);

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_PLAYER_NO_UNION;
	}
	else if ((guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId)) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_PLAYER_NO_UNION;
	}
	else if ((invitee = SocialRelationManager::instance()->GetPlayerByName(inviteeName, false)) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (invitee->LineState() == Player::LINE_OFFLINE)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (player->Race() != invitee->Race())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_PLAYER_IN_DIFF_RACE;
	}
	else if (invitee->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_IN_OTHER_UNION;
	}
	else if (invitee->GetGuildInfo().exitTime && time(0) - invitee->GetGuildInfo().exitTime < 24 * 3600)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_IN_OTHER_UNION;
	}
	else if (guild->GetMemberCount() >= guild->GetMaxMemberCount())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_IS_FULL;
	}
	else if (req.uiid != guild->GuildOwner() && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_ADD_MEMBER))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if (!RepeatInviteCheck(player->Uiid(), invitee->Uiid()))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_ADD_INVITE_REPEAT;
	}

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		InviteInfo info;
		info.invitee = invitee->Uiid();
		info.invitor = player->Uiid();
		info.inviteTime = time(0);
		invitees_.insert(std::make_pair(info.invitee, info));

		RelationServiceNS::AddUnionMemberConfirm cfm;
		cfm.gateId = invitee->GateId();
		cfm.playerId = invitee->PlayerId();

		cfm.unionNameLen = StringToBuf(guild->GuildName(), cfm.unionName, sizeof(cfm.unionName));
		cfm.sponsorLen = StringToBuf(player->Name(), cfm.sponsor, sizeof(cfm.sponsor));

		tcp_session s = RelationServiceMessageHandler::get_session(cfm.gateId);
		return RelationServiceMessageHandler::send_result_message(s, &cfm, RelationServiceNS::AddUnionMemberConfirm::wCmd);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::AddUnionMemberResult::wCmd);
	
	return 0;
}

int GuildManager::HandleExitGuildMember(tcp_session session, const RelationServiceNS::RemoveUnionMemberRequest & req)
{
	ACE_TRACE("GuildManager::HandleExitGuildMember");

	RelationServiceNS::RemoveUnionMemberResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player || player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	if (!guild->IsMember(req.memberRemoved))
		return -1;

	Player * target = SocialRelationManager::instance()->GetPlayerByUiid(req.memberRemoved);
	if (!target)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	if (req.uiid != guild->GuildOwner() && req.uiid != req.memberRemoved && guild->GetPosDiff(player->GetGuildInfo().guildPos,
		target->GetGuildInfo().guildPos) < 1 && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_REMOVE_MEMBER))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if (req.memberRemoved == guild->GuildOwner() && guild->GetMemberCount() > 1)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		Player::GuildInfo info;
		info.guildId = Player::INVALID_GUILD_ID;
		info.exitTime = time(0);

		if (guild->GetMemberList().size() == 1)
		{
			SendGuildMemberExitedResult(*target, *guild);

			RelationServiceNS::DestroyUnionRequest req2;
			memset(&req2, 0, sizeof(req2));
			req2.uiid = req.uiid;
			return HandleDestroyGuild(session, req2);
		}
		else
		{
			target->SetGuildInfo(info);

			SocialRelationManager::instance()->GetDB().Save(*target);

			Eval(result.memberRemoved, *target);

			guild->RemoveMember(target->Uiid());

			std::map<int, gainactivewaycfg_t>::const_iterator it = gainactivewaycfg_.find(6);
			if (it != gainactivewaycfg_.end())
			{
				guild->AddActive(it->second.gainActive);

				SocialRelationManager::instance()->GetDB().Save(*guild);

				SendGuildActiveNotify(*guild);
			}

			if (req.uiid != req.memberRemoved)
			{
				result.gateId = target->GateId();
				result.playerId = target->PlayerId();
			}
		}
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	session = RelationServiceMessageHandler::get_session(result.gateId);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RemoveUnionMemberResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildMemberExitNotify(*target, *guild, req.uiid != req.memberRemoved);
		target->AddLeaveGuildTweet(guild->GuildName());
	}
	
	return 0;
}

int GuildManager::HandleInvitePlayerConfirmed(tcp_session session, const RelationServiceNS::AddUnionMemberConfirmed & req)
{
	ACE_TRACE("GuildManager::HandleInvitePlayerConfirmed");
	
	RelationServiceNS::AddUnionMemberResult result;
	memset(&result, 0, sizeof(result));

	std::multimap<unsigned, InviteInfo>::iterator it = invitees_.find(req.uiid);
	if (it == invitees_.end())
	{
		return -1;
	}

	if (time(0) - it->second.inviteTime > 60 * 10)
	{
		invitees_.erase(it);
		return -1;
	}

	Player * invitee = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!invitee)
	{
		invitees_.erase(it);
		return -1;
	}

	result.memberAddedNameLen = StringToBuf(invitee->Name(), result.memberAddedName, sizeof(result.memberAddedName));

	Player * invitor = SocialRelationManager::instance()->GetPlayerByUiid(it->second.invitor, false);
	if (!invitor)
	{
		invitees_.erase(it);
		return -1;
	}

	result.gateId = invitor->GateId();
	result.playerId = invitor->PlayerId();

	Guild * guild = SocialRelationManager::instance()->GetGuildById(invitor->GetGuildInfo().guildId);
	if (!guild)
	{
		invitees_.erase(it);
		return -1;
	}

	if (req.flag == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_ADD_REJECT;	
	}
	else if (invitee->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_IN_OTHER_UNION;
	}
	else if (guild->GetMemberCount() >= guild->GetMaxMemberCount())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_IS_FULL;
	}
	else if (invitor->Uiid() != guild->GuildOwner() && !guild->IsHasRight(invitor->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_ADD_MEMBER))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else
	{
		Player::GuildInfo info;
		info.guildId = guild->GuildId();
		info.joinTime = time(0);
		invitee->SetGuildInfo(info);
		guild->AddMember(invitee->Uiid());
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_ADD_AGREE;

		SocialRelationManager::instance()->GetDB().Save(*invitee);
	}
	
	invitees_.erase(it);

	tcp_session s = RelationServiceMessageHandler::get_session(result.gateId);
	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::AddUnionMemberResult::wCmd);

	result.gateId = invitee->GateId();
	result.playerId = invitee->PlayerId();
	s = RelationServiceMessageHandler::get_session(result.gateId);
	RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::AddUnionMemberResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_ADD_AGREE)
	{		
		SendGuildBasicInfo(*invitee, *guild);
		SendGuildSkillList(*invitee, *guild);
		SendGuildBulletinNotify(*invitee, *guild);
		if (guild->GetTaskPostInfo().postTime)
			SendGuildMemberTaskPost(*invitee, *guild);
		SendGuildPrestigeNotify(*invitee, *guild);
		SendGuildBadgeNotify(*invitee, *guild);
		SendGuildMemberList(*invitee, *guild);

		SendGuildMemberJoinNotify(*invitee, *guild);

		invitee->AddJoinGuildTweet(guild->GuildName());
	}

	return 0;
}

int GuildManager::HandleModifyGuildMemberTitle(tcp_session session, const RelationServiceNS::ModifyUnionMemberTitleRequest & req)
{
	ACE_TRACE("GuildManager::HandleModifyGuildMemberTitle");

	RelationServiceNS::ModifyUnionMemberTitleResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player || player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	if (!guild->IsMember(req.modifiedUiid))
		return -1;

	Player * target = SocialRelationManager::instance()->GetPlayerByUiid(req.modifiedUiid);
	if (!target)
		return -1;

	if (guild->GuildOwner() != req.uiid && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_EDIT_TITLE))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else
	{
		Player::GuildInfo info(target->GetGuildInfo());
		BuffToString(req.title, req.titleLen, sizeof(req.title), info.guildTitle);
		target->SetGuildInfo(info);
		SocialRelationManager::instance()->GetDB().Save(*target);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::ModifyUnionMemberTitleResult::wCmd);
	
	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
		SendGuildMemberTitleChangedNotify(*target, *guild);

	return 0;
}

int GuildManager::HandleTransformGuildCaption(tcp_session session, const RelationServiceNS::TransformUnionCaptionRequest & req)
{
	ACE_TRACE("GuildManager::HandleTransformGuildCaption");

	RelationServiceNS::TransformUnionCaptionResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	if (!guild->IsMember(req.newCaptionUiid))
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	Player * target = 0;

	if (guild->GuildOwner() != player->Uiid())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.newCaptionUiid)) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else
	{
		Player::GuildInfo info(player->GetGuildInfo());
		info.guildPos = 0;
		player->SetGuildInfo(info);

		Player::GuildInfo info2(target->GetGuildInfo());
		info2.guildPos = 0;
		target->SetGuildInfo(info2);

		guild->GuildOwner(target->Uiid());

		SocialRelationManager::instance()->GetDB().Save(*player);
		SocialRelationManager::instance()->GetDB().Save(*target);
		SocialRelationManager::instance()->GetDB().Save(*guild);
		SocialRelationManager::instance()->GetDB().Save(guild->GuildId(), RelationServiceNS::UNION_LOG_CAPTION_CHANGED_EVENT, target->Name().c_str(), 0);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::TransformUnionCaptionResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildCaptionChangedNotify(*target, *guild);
	}
	
	return 0;
}

int GuildManager::HandleAdvanceGuildMemberPos(tcp_session session, const RelationServiceNS::AdvanceUnionMemberPosRequest & req)
{
	ACE_TRACE("GuildManager::HandleAdvanceGuildMemberPos");

	RelationServiceNS::AdvanceUnionMemberPosResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	if (guild->GuildOwner() == req.advancedUiid)
		return -1;

	if (!guild->IsMember(req.advancedUiid))
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	Player * target = 0;

	if (player->Uiid() != guild->GuildOwner() && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_ADVANCE_POS))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.advancedUiid)) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (player->Uiid() != guild->GuildOwner() && target->GetGuildInfo().guildPos && 
		guild->GetPosDiff(player->GetGuildInfo().guildPos, target->GetGuildInfo().guildPos) < 2)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if (guild->GetPosList().empty())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_NO_POS;
	}
	else
	{
		Player::GuildInfo info(target->GetGuildInfo());
		PosCheck check(target->GetGuildInfo().guildPos);
		Guild::PosList::const_iterator it = std::find_if(guild->GetPosList().begin(), guild->GetPosList().end(), check);
		if (it == guild->GetPosList().end())
		{
			info.guildPos = guild->GetPosList().back().idx;
		}
		else if (it == guild->GetPosList().begin())
		{
			return 0;
		}
		else
		{
			--it;
			info.guildPos = it->idx;
		}

		target->SetGuildInfo(info);

		SocialRelationManager::instance()->GetDB().Save(*target);		
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::AdvanceUnionMemberPosResult::wCmd);

	if(result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildMemberPosAdvancedNotify(*target, *guild);
	}

	return 0;
}

int GuildManager::HandleReduceGuildMemberPos(tcp_session session, const  RelationServiceNS::ReduceUnionMemberPosRequest & req)
{
	ACE_TRACE("GuildManager::HandleReduceGuildMemberPos");

	RelationServiceNS::ReduceUnionMemberPosResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	if (guild->GuildOwner() == req.reducedUiid)
		return -1;

	if (!guild->IsMember(req.reducedUiid))
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	Player * target = 0;

	if (player->Uiid() != guild->GuildOwner() && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_REDUCE_POS))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.reducedUiid)) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (player->Uiid() != guild->GuildOwner() && guild->GetPosDiff(player->GetGuildInfo().guildPos, target->GetGuildInfo().guildPos) < 1)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if (guild->GetPosList().empty())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_NO_POS;
	}
	else
	{
		Player::GuildInfo info(target->GetGuildInfo());
		PosCheck check(target->GetGuildInfo().guildPos);
		Guild::PosList::const_iterator it = std::find_if(guild->GetPosList().begin(), guild->GetPosList().end(), check);
		if (it == guild->GetPosList().end())
		{
			return 0;
		}
		else if (it->idx == guild->GetPosList().back().idx)
		{
			info.guildPos = 0;
		}
		else
		{
			++it;
			info.guildPos = it->idx;
		}

		target->SetGuildInfo(info);

		SocialRelationManager::instance()->GetDB().Save(*target);		
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::ReduceUnionMemberPosResult::wCmd);

	if(result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildMemberPosReducedNotify(*target, *guild);
	}

	return 0;
}

int GuildManager::HandleGuildChannelSpeek(tcp_session session, const RelationServiceNS::UnionChannelSpeekRequest & req)
{
	ACE_TRACE("GuildManager::HandleGuildChannelSpeek");

	if (req.contentLen > sizeof(req.content))
		return -1;

	RelationServiceNS::UnionChannelSpeekResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	if (player->GetGuildInfo().forbid)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_FORBID_SPEEK;
	}
	
	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::UnionChannelSpeekResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		RelationServiceNS::UnionChannelSpeekNotify notify;
		memset(&notify, 0, sizeof(notify));
		memcpy(notify.content, req.content, req.contentLen);
		notify.contentLen = req.contentLen;
		notify.speekerNameLen = StringToBuf(player->Name(), notify.speekerName, sizeof(notify.speekerName));
		notify.speekerLine = player->LineState();

		for(Guild::MemberList::const_iterator it = guild->GetMemberList().begin(); it != guild->GetMemberList().end(); ++it)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
			if (!p || p->LineState() == Player::LINE_OFFLINE)
				continue;

			notify.gateId = p->GateId();
			notify.playerId = p->PlayerId();

			tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
			RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionChannelSpeekNotify::wCmd);
		}
	}

	return 0;
}

int GuildManager::HandleUpdateGuildPosList(tcp_session session, const RelationServiceNS::UpdateUnionPosCfgRequest & req)
{
	ACE_TRACE("GuildManager::HandleUpdateGuildPosList");

	RelationServiceNS::UpdateUnionPosCfgResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	if (guild->GuildOwner() != player->Uiid())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else
	{
		unsigned len = req.posListLen > 10 ? 10 : req.posListLen;
		Guild::PosList lst;
		Eval(lst, req.posList, len);

		for(Guild::MemberList::const_iterator it = guild->GetMemberList().begin(); it != guild->GetMemberList().end(); ++it)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
			if (!p || p->GetGuildInfo().guildPos == 0)
				continue;

			Guild::PosInfo pos;
			pos.idx = p->GetGuildInfo().guildPos;
			if (std::find(lst.begin(), lst.end(), pos) == lst.end())
			{
				Player::GuildInfo info(p->GetGuildInfo());
				info.guildPos = 0;
				p->SetGuildInfo(info);

				SocialRelationManager::instance()->GetDB().Save(*p);
			}
		}

		unsigned maxPosIdx = guild->GetMaxPosIdx();
		for(Guild::PosList::iterator _it = lst.begin(); _it != lst.end(); ++_it)
		{
			if (_it->idx == (unsigned short)-1)
			{
				_it->idx =  ++maxPosIdx;
			}
		}

		guild->SetPosList(lst);

		SocialRelationManager::instance()->GetDB().Save(*guild);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::UpdateUnionPosCfgResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildPosListNotify(*guild);
	}

	return 0;
}

int GuildManager::HandleStudyUnionSkill(tcp_session session, const RelationServiceNS::StudyUnionSkillRequest & req)
{
	ACE_TRACE("GuildManager::HandleStudyUnionSkill");

	RelationServiceNS::StudyUnionSkillResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	Guild::SkillList lst;
	GetAvailableSkillList(*guild, lst);
	const Guild::SkillInfo * skill = GetSkill(req.skillId);
	if (!skill)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_UNEXPECTED_SKILL_ID;
	}
	//else if (std::find(guild->GetSkillList().begin(), guild->GetSkillList().end(), skill->skillId) != guild->GetSkillList().end())
	else if (CheckSkillStudyed(skill->skillId, *guild))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_UNEXPECTED_SKILL_ID;
	}
	else if (skill->preSkillId && std::find(guild->GetSkillList().begin(), guild->GetSkillList().end(), skill->preSkillId) == guild->GetSkillList().end())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_UNEXPECTED_SKILL_ID;
	}
	else if (skill->needGuildLevel > guild->Level())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NEED_HIGHER_LEVEL;
	}
	else if (guild->Active() < skill->expendActive)	
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_ACTIVE_POINT_LACK;
	}
	else if (req.uiid != guild->GuildOwner() && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_STUDY_SKILL))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else
	{
		guild->AddSkill(skill->skillId);
		guild->RemoveSkill(skill->preSkillId);

		guild->AddActive(-1 * skill->expendActive);

		SendGuildActiveNotify(*guild);

		SocialRelationManager::instance()->GetDB().Save(*guild);
	}

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildSkillListNotify(*guild);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::StudyUnionSkillResult::wCmd);

	return 0;
}

int GuildManager::HandlePostGuildTask(tcp_session session, const RelationServiceNS::PostUnionTasksListRequest & req)
{
	ACE_TRACE("GuildManager::HandlePostGuildTask");

	if (req.durationTime > 12)
		return -1;

	RelationServiceNS::PostUnionTasksListResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	if (player->Uiid() != guild->GuildOwner() && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_POST_TASK))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if (!guild->CheckPostTaskTiming())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_TASK_LIST_POST_REPEAT;
	}
	else if (GetTaskPostNeedActive(req.durationTime) > guild->Active())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_ACTIVE_POINT_LACK;
	}
	else
	{
		Guild::TaskPostInfo info;
		info.postTime = time(0);
		info.taskPeriod = req.durationTime * 3600;

		guild->SetTaskPostInfo(info);
		guild->AddActive(-1 * ((int)GetTaskPostNeedActive(req.durationTime)));

		SendGuildActiveNotify(*guild);
				
		SocialRelationManager::instance()->GetDB().Save(*guild);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::PostUnionTasksListResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildTaskPostNotify(*guild);
	}

	return 0;
}

int GuildManager::HandleAdvanceGuildLevel(tcp_session session, const RelationServiceNS::AdvanceUnionLevelRequest & req)
{
	ACE_TRACE("GuildManager::HandleAdvanceGuildLevel");

	RelationServiceNS::AdvanceUnionLevelResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	if (guild->Level() >= 10)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	if (guild->GuildOwner() != player->Uiid() && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_ADVANCE_UNION))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if (GetAdvanceGuildNeedActive(guild->Level()) > guild->Active())
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_UNION_ACTIVE_POINT_LACK;
	}
	else
	{
		guild->AddActive(-1 * ((int)GetAdvanceGuildNeedActive(guild->Level())));
		guild->AddActive(GetAdvanceGuildGainActive(guild->Level()));

		guild->Level(guild->Level() + 1);
		guild->SetMaxMemberCount(GetMaxCount(guild->Level()));

		SocialRelationManager::instance()->GetDB().Save(*guild);

		char buf[32];
		sprintf(buf, "%u", guild->Level());
		SocialRelationManager::instance()->GetDB().Save(guild->GuildId(), RelationServiceNS::UNION_LOG_ADVANCE_LEVEL_EVENT, player->Name().c_str(), buf);

		UpdateGuildRank(0, *guild);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::AdvanceUnionLevelResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildLevelNotify(*guild, guild->Level() - 1);
	}

	return 0;
}

int GuildManager::HandlePostGuildBulletin(tcp_session session, const RelationServiceNS::PostUnionBulletinRequest & req)
{
	ACE_TRACE("GuildManager::HandlePostGuildBulletin");

	RelationServiceNS::PostUnionBulletinResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	if (guild->GuildOwner() != player->Uiid() && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_POST_ANNOUNCE))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else
	{
		std::string str;
		BuffToString(req.content, req.contentLen, sizeof(req.content), str);

		if (req.seq)
		{
			if (guild->GetBulletin().size() < 1000)
			{
				guild->SetBulletin(guild->GetBulletin() + str);
			}
		}
		else
			guild->SetBulletin(str);

		SocialRelationManager::instance()->GetDB().Save(*guild);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::PostUnionBulletinResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED && req.end)
	{
		SendGuildBulletinNotify(*guild);
	}

	return 0;
}

int GuildManager::HandleQueryGuildLog(tcp_session session, const RelationServiceNS::QueryUnionLogRequest & req)
{
	ACE_TRACE("GuildManager::HandleQueryGuildLog");

	RelationServiceNS::QueryUnionLogResult result;
	memset(&result, 0, sizeof(result));
	result.bookmark = req.bookmark;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	Guild * guild = 0;
	if (player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		bool nextPage = false;
		result.logListLen = SocialRelationManager::instance()->GetDB().LoadGuildLog(player->GetGuildInfo().guildId, result.logList, req.bookmark, nextPage);
		if (nextPage)
			result.bookmark |= 0x80000000;
	}

	if (result.logListLen)
		RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryUnionLogResult::wCmd);

	return 0;
}

int GuildManager::HandleModifyGuildActive(tcp_session session, const RelationServiceNS::ModifyUnionActivePointRequest & req)
{
	ACE_TRACE("GuildManager::HandleModifyGuildActive");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	guild->AddActive(req.bAdd ? req.point : (-1 * (int)req.point));

	SocialRelationManager::instance()->GetDB().Save(*guild);

	SendGuildActiveNotify(*guild);

	return 0;
}

int GuildManager::HandleForbidGuildMember(tcp_session session, const RelationServiceNS::ForbidUnionSpeekRequest & req)
{
	ACE_TRACE("GuildManager::HandleForbidGuildMember");

	RelationServiceNS::ForbidUnionSpeekResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	if (!guild->IsMember(req.forbidUiid))
		return -1;

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	Player * target = 0;

	if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.forbidUiid)) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (guild->GuildOwner() != req.uiid && !guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_ADD_SPEEK_RESTRICT))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	/*else if (guild->GuildOwner() != req.uiid && !target->GetGuildInfo().forbid && 
		!guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_ADD_SPEEK_RESTRICT))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}
	else if (guild->GuildOwner() != req.uiid && target->GetGuildInfo().forbid && 
		!guild->IsHasRight(player->GetGuildInfo().guildPos, RelationServiceNS::UNION_RIGHT_REMOVE_SPEEK_RESTRICT))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_RIGHT_CHECK_FAILED;
	}*/
	else
	{
		Player::GuildInfo info(target->GetGuildInfo());
		info.forbid = !info.forbid;
		target->SetGuildInfo(info);
		
		SocialRelationManager::instance()->GetDB().Save(*target);
	}

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::ForbidUnionSpeekResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_UNION_SUCCEED)
	{
		SendGuildMemberForbidNotify(*target, *guild);
	}

	return 0;
}

int GuildManager::HandleGuildOwnerCheck(tcp_session session, const RelationServiceNS::UnionOwnerCheckRequest & req)
{
	ACE_TRACE("GuildManager::HandleGuildOwnerCheck");

	RelationServiceNS::UnionOwnerCheckResult result;
	memset(&result, 0, sizeof(result));
	result.gateId = req.gateId;
	result.playerId = req.playerId;
	result.uiid = req.uiid;
	result.flag = 0;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid);
	if (player && player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * guild = SocialRelationManager::instance()->LoadPlayerGuild(*player);
		if (guild && guild->GuildOwner() == req.uiid)
			result.flag = 1;
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::UnionOwnerCheckResult::wCmd);

	return 0;
}


int GuildManager::HandleModifyGuildPrestige(tcp_session session, const RelationServiceNS::ModifyUnionPrestigeRequest & req)
{	
	ACE_TRACE("GuildManager::HandleModifyGuildPrestige");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	guild->AddPrestige(req.point, req.bAdd);

	SocialRelationManager::instance()->GetDB().Save(*guild);

	SendGuildPrestigeNotify(*guild);

	UpdateGuildRank(guild->GuildOwner(), *guild);

	return 0;
}

int GuildManager::HandleModifyGuildBadge(tcp_session session, const RelationServiceNS::ModifyUnionBadgeRequest & req)
{
	ACE_TRACE("GuildManager::HandleModifyGuildBadge");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	if (player->Uiid() != guild->GuildOwner())
	{
		SS_ERROR("%s is not guild(%s) caption\n", player->Name().c_str(), guild->GuildName().c_str());
		return -1;
	}

	std::string badge;
	BuffToString(req.badgeData, req.dataLen, sizeof(req.badgeData), badge);

	guild->SetBadge(badge);

	SocialRelationManager::instance()->GetDB().Save(*guild);

	SendGuildBadgeNotify(*guild);

	return 0;
}

int GuildManager::HandleModifyGuildMult(tcp_session session, const RelationServiceNS::ModifyUnionMultiRequest & req)
{
	ACE_TRACE("GuildManager::HandleModifyGuildMult");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (player->GetGuildInfo().guildId == Player::INVALID_GUILD_ID)
		return -1;

	Guild * guild = SocialRelationManager::instance()->GetGuildById(player->GetGuildInfo().guildId);
	if (!guild)
		return -1;

	Guild::MultiInfo multi;
	multi.type = req.type;
	multi.endTime = req.endTime;
	multi.multi = req.multi;

	guild->SetMulti(multi);
	
	SendGuildMultiNotify(*guild, req.type);

	return 0;
}

int GuildManager::HandleQueryGuildRankRequest(tcp_session session, const RelationServiceNS::QueryUnionRankRequest & req)
{
	ACE_TRACE("GuildManager::HandleQueryGuildRankRequest");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	if (req.page >= 10)
		return -1;

	RelationServiceNS::QueryUnionRankResult result;
	memset(&result, 0, sizeof(result));
	result.gateId = player->GateId();
	result.playerId = player->PlayerId();
	result.page = req.page;
	result.endFlag = true;

	std::map<unsigned, std::vector<GuildRankInfo> >::iterator it = guildRank_.find(req.race);
	if (it !=guildRank_.end())
	{
		result.endFlag = it->second.size() <= (result.page + 1) * 10;
		for(result.len = 0; result.len < 10 && 10 * req.page + result.len < it->second.size(); ++result.len)
		{
			result.rank[result.len] = it->second[10 * req.page + result.len];
		}
	}
	else
	{
		std::vector<GuildRankInfo> vec;
		for(it = guildRank_.begin(); it != guildRank_.end(); ++it)
		{
			for(std::vector<GuildRankInfo>::iterator git = it->second.begin(); git != it->second.end(); ++git)
			{
				vec.push_back(*git);
			}
		}

		std::sort(vec.begin(), vec.end());
		
		result.endFlag = vec.size() <= (result.page + 1) * 10;
		for(result.len = 0; result.len < 10 && (10 * req.page + result.len < vec.size()); ++result.len)
		{
			result.rank[result.len] = vec[10 * req.page + result.len];
		}
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryUnionRankResult::wCmd);

	return 0;
}

int GuildManager::HandleSearchGuildRankRequest(tcp_session session, const RelationServiceNS::SearchUnionRankRequest & req)
{
	ACE_TRACE("GuildManager::HandleSearchGuildRankRequest");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid, false);
	if (!player)
		return -1;

	RelationServiceNS::SearchUnionRankResult result;
	memset(&result, 0, sizeof(result));
	result.gateId = player->GateId();
	result.playerId = player->PlayerId();
	result.resultCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	std::string guildName;
	BuffToString(req.unionName, req.unionNameLen, sizeof(req.unionName), guildName);

	Guild * guild = SocialRelationManager::instance()->GetGuildByName(guildName);
	if (!guild)
	{
		result.resultCode = RelationServiceNS::R_G_RESULT_FAILED;
	}
	else
	{
		Player * caption = SocialRelationManager::instance()->GetPlayerByUiid(guild->GuildOwner());
		if (caption)
		{
			result.unionInfo.captionNameLen = StringToBuf(caption->Name(), result.unionInfo.captionName, sizeof(result.unionInfo.captionName));
			result.unionInfo.unionRace = caption->Race();
		}

		result.unionInfo.unionLevel = guild->Level();
		result.unionInfo.unionNameLen = StringToBuf(guildName, result.unionInfo.unionName, sizeof(result.unionInfo.unionName));
		result.unionInfo.unionPretige = guild->Prestige();
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::SearchUnionRankResult::wCmd);
	return 0;
}

int GuildManager::HandleQueryRaceMasterRequest(tcp_session session, const RelationServiceNS::QueryRaceMasterRequest & req)
{
	ACE_TRACE("GuildManager::HandleQueryRaceMasterRequest");

	RelationServiceNS::QueryRaceMasterResult result;
	memset(&result, 0, sizeof(result));

	result.len = 0;
	for(std::map<unsigned, std::vector<GuildRankInfo> >::const_iterator it = guildRank_.begin();
		it != guildRank_.end() && result.len < sizeof(result.masters) / sizeof(result.masters[0]); ++it)
	{
		if (!it->second.empty())
		{
			const GuildRankInfo & rankInfo = it->second.front();
			result.masters[result.len].captionId = rankInfo.captionId;
			result.masters[result.len].captionNameLen = rankInfo.captionNameLen;
			memcpy(result.masters[result.len].captionName, rankInfo.captionName, rankInfo.captionNameLen);
			result.masters[result.len].unionLevel = rankInfo.unionLevel;
			result.masters[result.len].unionNameLen = rankInfo.unionNameLen;
			memcpy(result.masters[result.len].unionName, rankInfo.unionName, rankInfo.unionNameLen);
			result.masters[result.len].unionRace = rankInfo.unionRace;
			++result.len;
		}
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryRaceMasterResult::wCmd);

	return 0;
}

int GuildManager::HandleQueryGuildMembersRequest(tcp_session session, const RelationServiceNS::QueryGuildMembersRequest & req)
{
	ACE_TRACE("GuildManager::HandleQueryGuildMembersRequest");

	std::vector<GuildRankInfo> vec;
	do 
	{ 
		std::map<unsigned, std::vector<GuildRankInfo> >::const_iterator it;
		for(it = guildRank_.begin(); it != guildRank_.end(); ++it)
		{
			for(std::vector<GuildRankInfo>::const_iterator git = it->second.begin(); git != it->second.end(); ++git)
				vec.push_back(*git);
		}
	}while(false);

	std::sort(vec.begin(), vec.end());

	if (req.rank > vec.size())
		return -1;

	GuildRankInfo & guildRank = vec.at(req.rank);

	Guild * guild = SocialRelationManager::instance()->GetGuildById(guildRank.guildId);
	if (!guild)
		return -1;

	RelationServiceNS::QueryGuildMembersResult result;
	memset(&result, 0, sizeof(result));

	memcpy(result.guildName, guildRank.unionName, guildRank.unionNameLen);
	result.nameLen = guildRank.unionNameLen;

	result.race = guildRank.unionRace;
	result.rank = req.rank;
	result.membersLen = 0;
	result.bEnd = false;

	const Guild::MemberList & members = guild->GetMemberList();
	for(Guild::MemberList::const_iterator it = members.begin(); it != members.end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it);
		if (!player)
			continue;

		result.members[result.membersLen].nameLen = StringToBuf(
			player->Name(), result.members[result.membersLen].name, sizeof(result.members[result.membersLen].name));
		++result.membersLen;

		if (result.membersLen == sizeof(result.members) / sizeof(result.members[0]))
		{
			RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryGuildMembersResult::wCmd);

			result.membersLen = 0;
		}
	}

	result.bEnd = true;
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryUnionMembersResult::wCmd);

	return 0;
}

int GuildManager::HandleQueryPlayerGuild(tcp_session session, const RelationServiceNS::GRQueryNickUnion & req)
{
	ACE_TRACE("GuildManager::HandleQueryPlayerGuild");

	RelationServiceNS::RGQueryNickUnionRst result;
	result.queryPlayerID = req.queryPlayerID;
	result.nickunionArrLen = 0;

	for(unsigned i = 0; i < req.nickArrLen && i < sizeof(req.nickArr)/sizeof(req.nickArr[0]); ++i, ++result.nickunionArrLen)
	{
		std::string name;
		BuffToString(req.nickArr[i].Name, req.nickArr[i].NameLen, sizeof(req.nickArr[i].Name), name);
		result.nickunionArr[i].NickName.NameLen = StringToBuf(name, result.nickunionArr[i].NickName.Name, sizeof(result.nickunionArr[i].NickName.Name));
		result.nickunionArr[i].UnionName.NameLen = 0;

		Player * p = SocialRelationManager::instance()->GetPlayerByName(name);
		if (p)
		{
			if (Player::INVALID_GUILD_ID != p->GetGuildInfo().guildId)
			{
				Guild * guild = SocialRelationManager::instance()->LoadPlayerGuild(*p);
				if (guild)
				{
					result.nickunionArr[i].UnionName.NameLen = StringToBuf(guild->GuildName(), result.nickunionArr[i].UnionName.Name,
						sizeof(result.nickunionArr[i].UnionName.Name));
				}
			}
		}
	}
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGQueryNickUnionRst::wCmd);
	return 0;
}

int GuildManager::SendGuildMemberList(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberList");

	if (player.LineState() == Player::LINE_OFFLINE)
		return -1;
	
	tcp_session session = RelationServiceMessageHandler::get_session(player.GateId());
	if (!check_session(session))
		return -1;

	std::vector<GuildMember> members;
	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it);
		if (!player)
			continue;
	
		GuildMember tmp;
		Eval(tmp, *player);
		tmp.joinTime = player->GetGuildInfo().joinTime;

		members.push_back(tmp);
	}

	std::sort(members.begin(), members.end());

	RelationServiceNS::QueryUnionMembersResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;
	result.gateId = player.GateId();
	result.playerId = player.PlayerId();	

	for(std::vector<GuildMember>::const_iterator it = members.begin(); it != members.end(); ++it)
	{
		result.members.members[result.members.count] = *it;
		++result.members.count;
					
		if (result.members.count == sizeof(result.members.members) / sizeof(result.members.members[0]))
		{
			RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryUnionMembersResult::wCmd);

			result.members.count = 0;
		}
	}

	if (result.members.count)
	{
		SS_DEBUG("return code:%d\n", result.returnCode);
		RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryUnionMembersResult::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildBasicInfo(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildBasicInfo");

	tcp_session session = RelationServiceMessageHandler::get_session(player.GateId());
	if (!check_session(session))
		return -1;

	RelationServiceNS::QueryUnionBasicResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;
	result.gateId = player.GateId();
	result.playerId = player.PlayerId();	
	result.bGate = 1;
	Eval(result._union, guild);

	SS_DEBUG("return code:%d\n", result.returnCode);
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryUnionBasicResult::wCmd);

	return 0;
}

int GuildManager::SendGuildSkillList(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildSkillList");
	
	RelationServiceNS::UnionSkillsListNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.guildId = guild.GuildId();
	notify.skillsListLen = _Eval(notify.skillsList, guild.GetSkillList());

	notify.gateId = player.GateId();
	notify.playerId = player.PlayerId();
	notify.bGate = true;
	tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
	RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionSkillsListNotify::wCmd);

	return 0;
}

int GuildManager::SendGuildSkillListNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildSkillListNotify");

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		SendGuildSkillList(*player, guild);
	}

	return 0;
}

int GuildManager::SendGuildDestroyedNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildDestroyedNotify");

	RelationServiceNS::UnionDestroyedNotify notify;
	memset(&notify, 0, sizeof(notify));
	
	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = player->GateId();
		notify.playerId = player->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionDestroyedNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildMemberExitNotify(const Player & player, const Guild & guild, const bool flag)
{
	ACE_TRACE("GuildManager::SendGuildMemberExitNotify");

	RelationServiceNS::RemoveUnionMemberNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.flag = flag;
	Eval(notify.memberRemoved, player);

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = player->GateId();
		notify.playerId = player->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::RemoveUnionMemberNotify::wCmd);
	}

	if (player.LineState() != Player::LINE_OFFLINE)
	{
		notify.gateId = player.GateId();
		notify.playerId = player.PlayerId();
		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::RemoveUnionMemberNotify::wCmd);
	}

	SocialRelationManager::instance()->NotifyPlayerFriendGuildInfo(player);

	return 0;
}

int GuildManager::SendGuildMemberJoinNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberJoinNotify");

	RelationServiceNS::AddUnionMemberNotify notify;
	Eval(notify.memberAdded, player);

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE || p->Uiid() == player.Uiid())
			continue;

		notify.gateId = p->GateId();
		notify.playerId = p->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::AddUnionMemberNotify::wCmd);
	}

	SocialRelationManager::instance()->NotifyPlayerFriendGuildInfo(player);

	return 0;
}

int GuildManager::SendGuildMemberTitleChangedNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberTitleChangedNotify");
	
	RelationServiceNS::UnionMemberTitleNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.titleLen = StringToBuf(player.GetGuildInfo().guildTitle, notify.title, sizeof(notify.title));
	notify.memberNameLen = StringToBuf(player.Name(), notify.memberName, sizeof(notify.memberName));

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = p->GateId();
		notify.playerId = p->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionMemberTitleNotify::wCmd);
	}
	return 0;
}

int GuildManager::SendGuildCaptionChangedNotify(const Player & caption, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildCaptionChangedNotify");

	RelationServiceNS::TransformUnionCaptionNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.newCaptionNameLen = StringToBuf(caption.Name(), notify.newCaptionName, sizeof(notify.newCaptionName));

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = player->GateId();
		notify.playerId = player->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::TransformUnionCaptionNotify::wCmd);			
	}

	return 0;
}

int GuildManager::SendGuildMemberForbidNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberForbidNotify");

	RelationServiceNS::ForbidUnionSpeekNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.nameLen = StringToBuf(player.Name(), notify.name, sizeof(notify.name));

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = p->GateId();
		notify.playerId = p->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::ForbidUnionSpeekNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildActiveNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildActiveNotify");

	RelationServiceNS::UnionActivePointNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.point = guild.Active();

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = player->GateId();
		notify.playerId = player->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionActivePointNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildPosListNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildPosListNotify");

	RelationServiceNS::UpdateUnionPosCfgNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.posListLen = Eval(notify.posList, guild.GetPosList());

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = player->GateId();
		notify.playerId = player->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UpdateUnionPosCfgNotify::wCmd);
	}
	
	return 0;
}

int GuildManager::SendGuildBulletinNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildBulletinNotify");
	
	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		SendGuildBulletinNotify(*player, guild);
	}

	return 0;
}

int GuildManager::SendGuildBulletinNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildBulletinNotify");

	RelationServiceNS::UnionBulletinNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.gateId = player.GateId();
	notify.playerId = player.PlayerId();

	tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
	
	if (guild.GetBulletin().size())
	{
		for(size_t c = 0; c < guild.GetBulletin().size(); ++notify.seq)
		{
			std::string sub = guild.GetBulletin().substr(c, sizeof(notify.content));
			notify.contentLen = StringToBuf(sub, notify.content, sizeof(notify.content));
			c += notify.contentLen;

			RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionBulletinNotify::wCmd);
		}
	}
	else
	{
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionBulletinNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildLevelNotify(const Guild & guild, const unsigned oldLvl)
{
	ACE_TRACE("GuildManager::SendGuildLevelNotify");

	RelationServiceNS::UnionLevelNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.level = guild.Level();
	notify.active = guild.Active();

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it);
		if (!player)
			continue;

		if (player->LineState() == Player::LINE_OFFLINE)
		{
			SendGuildLevelChangedEmail(guild, *player, oldLvl);
			continue;
		}

		notify.gateId = player->GateId();
		notify.playerId = player->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionLevelNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildMemberPosAdvancedNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberPosChangedNotify");

	RelationServiceNS::AdvanceUnionMemberPosNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.posSeq = player.GetGuildInfo().guildPos;
	notify.memberNameLen = StringToBuf(player.Name(), notify.memberName,sizeof(notify.memberName));

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = player->GateId();
		notify.playerId = player->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::AdvanceUnionMemberPosNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildMemberPosReducedNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberPosReducedNotify");

	RelationServiceNS::ReduceUnionMemberPosNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.posSeq = player.GetGuildInfo().guildPos;
	notify.memberNameLen = StringToBuf(player.Name(), notify.memberName,sizeof(notify.memberName));

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = player->GateId();
		notify.playerId = player->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::ReduceUnionMemberPosNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildMemberUplineNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberUplineNotify");

	RelationServiceNS::UnionMemberUplineNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.uplinePlayer = player.Uiid();

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE)
			continue;

		notify.gateId = p->GateId();
		notify.playerId = p->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionMemberUplineNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildMemberOfflineNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberOfflineNotify");

	RelationServiceNS::UnionMemberOfflineNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.offlinePlayer = player.Uiid();

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE || p->Uiid() == player.Uiid())
			continue;

		notify.gateId = p->GateId();
		notify.playerId = p->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionMemberOfflineNotify::wCmd);
	}

	return 0;
}

int GuildManager::SendGuildEmail(const Guild & guild, const Player & player, const std::string & title, const std::string & message)
{
	ACE_TRACE("GuildManager::SendGuildEmail");

	RelationServiceNS::UnionEmailNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.uiid = player.Uiid();
	notify.nameLen = StringToBuf(player.Name(), notify.name, sizeof(notify.name));
	notify.titleLen = StringToBuf(title, notify.emailTitle, sizeof(notify.emailTitle));
	notify.contentLen = StringToBuf(message, notify.emailContent, sizeof(notify.emailContent));

	tcp_session s = RelationServiceMessageHandler::get_session(0);
	RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionEmailNotify::wCmd);

	return 0;
}

int GuildManager::SendGuildLevelChangedEmail(const Guild & guild, const Player & player, const unsigned oldLvl)
{
	ACE_TRACE("GuildManager::SendGuildLevelChangedEmail");

	int ld = guild.Level() > oldLvl ? 1 : -1;
	char buf[128];
	if (ld > 0)
		sprintf(buf, "您的战盟等级已经从%u级升为%u级", oldLvl, oldLvl + 1);
	else
		sprintf(buf, "您的战盟等级已经从%u级降为%u级", oldLvl, oldLvl - 1);

	if(ld > 0)
		SendGuildEmail(guild, player, "战盟等级上升通知", buf);
	else
		SendGuildEmail(guild, player, "战盟等级降低通知", buf);
	
	return 0;
}

int GuildManager::SendGuildMemberExitedResult(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberExitedResult");
	
	RelationServiceNS::RemoveUnionMemberResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_UNION_SUCCEED;

	result.gateId = player.GateId();
	result.playerId = player.PlayerId();
	Eval(result.memberRemoved, player);

	tcp_session s = RelationServiceMessageHandler::get_session(result.gateId);
	RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::RemoveUnionMemberResult::wCmd);

	return 0;
}

int GuildManager::SendGuildPrestigeNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildPrestigeNotify");

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE)
			continue;

		SendGuildPrestigeNotify(*p, guild);
	}
	return 0;
}

int GuildManager::SendGuildPrestigeNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildPrestigeNotify");

	RelationServiceNS::UnionPrestigeNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.gateId = player.GateId();
	notify.playerId = player.PlayerId();
	notify.prestige = guild.Prestige();

	tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
	RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionPrestigeNotify::wCmd);
	return 0;
}

int GuildManager::SendGuildBadgeNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildBadgeNotify");

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE)
			continue;

		SendGuildBadgeNotify(*p, guild);
	}

	return 0;
}

int GuildManager::SendGuildBadgeNotify(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildBadgeNotify");

	RelationServiceNS::UnionBadgeNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.gateId = player.GateId();
	notify.playerId = player.PlayerId();
	notify.dataLen = StringToBuf(guild.GetBadge(), notify.badgeData, sizeof(notify.badgeData));

	tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
	RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionBadgeNotify::wCmd);
	return 0;
}

int GuildManager::SendGuildMultiNotify(const Guild & guild, unsigned type)
{
	ACE_TRACE("GuildManager::SendGuildMultiNotify");

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE)
			continue;

		SendGuildMultiNotify(*p, guild, type);
	}

	return 0;
}

int GuildManager::SendGuildMultiNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMultiNotify");

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE)
			continue;

		SendGuildMultiNotify(*p, guild);
	}
	return 0;
}

int GuildManager::SendGuildMultiNotify(const Player & player, const Guild & guild, unsigned type)
{
	Guild & _guild = const_cast<Guild &>(guild);
	const Guild::MultiInfo & info = _guild.GetMulti(type);
	if (info.endTime == 0 || info.endTime < time(0))
		return 0;

	ACE_TRACE("GuildManager::SendGuildMultiNotify");

	RelationServiceNS::UnionMultiNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.endTime = info.endTime;
	notify.multi = info.multi;
	notify.type = info.type;
	notify.gateId = player.GateId();
	notify.playerId = player.PlayerId();

	tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
	RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionMultiNotify ::wCmd);
	return 0;
}

int GuildManager::SendGuildMultiNotify(const Player & player, const Guild & guild)
{
	const std::map<unsigned, Guild::MultiInfo> & infos = guild.GetMultis();
	for(std::map<unsigned, Guild::MultiInfo>::const_iterator it = infos.begin(); it != infos.end(); ++it)
	{
		SendGuildMultiNotify(player, guild, it->second.type);
	}

	return 0;
}

int GuildManager::SendGuildMemberTaskPost(const Player & player, const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildMemberTaskPost");

	RelationServiceNS::UnionTasksListNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.activePoint = guild.Active();
	notify.endTime = guild.GetTaskPostInfo().postTime + guild.GetTaskPostInfo().taskPeriod;
	notify.guildIt = guild.GuildId();

	notify.gateId = player.GateId();
	notify.playerId = player.PlayerId();

	tcp_session s = RelationServiceMessageHandler::get_session(notify.gateId);
	RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::UnionTasksListNotify::wCmd);

	return 0;
}

int GuildManager::SendGuildTaskPostNotify(const Guild & guild)
{
	ACE_TRACE("GuildManager::SendGuildTaskPostNotify");

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!player || player->LineState() == Player::LINE_OFFLINE)
			continue;

		SendGuildMemberTaskPost(*player, guild);
	}

	return 0;
}

int GuildManager::HandleDeletePlayer(const Player & player, Guild & guild)
{
	ACE_TRACE("GuildManager::HandleDeletePlayer");

	guild.RemoveMember(player.Uiid());

	std::map<int, gainactivewaycfg_t>::const_iterator it = gainactivewaycfg_.find(6);
	if (it != gainactivewaycfg_.end())
	{
		guild.AddActive(it->second.gainActive);

		SocialRelationManager::instance()->GetDB().Save(guild);

		SendGuildActiveNotify(guild);
	}

	SendGuildMemberExitNotify(player, guild, false);

	return 0;
}

int GuildManager::HandlePlayerUpline(Player & player, Guild & guild)
{
	ACE_TRACE("GuildManager::HandlePlayerUpline");
	
	SendGuildBasicInfo(player, guild);
	SendGuildSkillList(player, guild);
	SendGuildBulletinNotify(player, guild);
	if (guild.GetTaskPostInfo().postTime/* && guild.GetTaskPostInfo().postTime + guild.GetTaskPostInfo().taskPeriod > time(0)*/)
		SendGuildMemberTaskPost(player, guild);
	//SendGuildMemberList(player, guild);
	SendGuildPrestigeNotify(player, guild);
	SendGuildBadgeNotify(player, guild);

	SendGuildMemberUplineNotify(player, guild);
	SendGuildMultiNotify(player, guild);
	return 0;
}

int GuildManager::HandlePlayerOffline(const Player & player, Guild & guild)
{
	ACE_TRACE("GuildManager::HandlePlayerUpline");
	
	invitees_.erase(player.Uiid());

	SendGuildMemberOfflineNotify(player, guild);
	return 0;
}

int GuildManager::HandlePlayerAdvanceLevel(const Player & player, Guild & guild, int d)
{
	ACE_TRACE("GuildManager::HandlePlayerAdvanceLevel");

	if (player.Level() > 30)
	{
		std::map<int, gainactivewaycfg_t>::const_iterator it = gainactivewaycfg_.find(2);
		if (it == gainactivewaycfg_.end())
			return -1;

		guild.AddActive(it->second.gainActive * d);
	
		SendGuildActiveNotify(guild);
	}

	for(Guild::MemberList::const_iterator it = guild.GetMemberList().begin(); it != guild.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it, false);
		if (!p || p->LineState() == Player::LINE_OFFLINE || player.IsFriend(*p))
			continue;

		RelationServiceNS::RGFriendInfoUpdate info;
		memset(&info, 0, sizeof(info));
		Eval(info, *p, player);

		info.player.GateSrvID = p->GateId();
		info.player.PPlayer = p->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(p->GateId());
		RelationServiceMessageHandler::send_result_message(s, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);
	}

	return 0;
}

int GuildManager::HandleGuildDayActiveExpend(Guild & guild)
{
	ACE_TRACE("GuildManager::HandleGuildDayActiveExpend");

	std::map<int, guildbasiccfg_t>::const_iterator it = guildbasiccfg_.find(guild.Level());
	if (it == guildbasiccfg_.end())
		return -1;

	guild.SetDayExpendCalTime(time(0));

	if (guild.Active() >= it->second.dayExpendActive)
	{
		guild.AddActive(-1 * (it->second.dayExpendActive));

		SendGuildActiveNotify(guild);
	}
	else
	{
		if (guild.Level() > 1)
		{
			guild.Level(guild.Level() - 1);
			guild.AddActive(-1 * guild.Active());

			ResetGuildSkillList(guild);

			SendGuildLevelNotify(guild, guild.Level() + 1);
			SendGuildSkillListNotify(guild);

			if (guild.Level() == 6)
			{
				guild.SetBadge("");
				SendGuildBadgeNotify(guild);
			}

			char buf[32];
			sprintf(buf, "%u", guild.Level());
			SocialRelationManager::instance()->GetDB().Save(guild.GuildId(), RelationServiceNS::UNION_LOG_REDUCE_LEVEL_EVENT, buf);
		}
	}

	SocialRelationManager::instance()->GetDB().Save(guild);
	return 0;
}

int GuildManager::HandleGuildMemberUplineDuration(const unsigned t, Guild & guild)
{
	ACE_TRACE("GuildManager::HandleGuildMemberUplineDuration");

	guild.AddActive(GetActiveByUplineDuration(t));

	SS_DEBUG("guild[%s] current active point: %u\n", guild.GuildName().c_str(), guild.Active());

	SendGuildActiveNotify(guild);

	return 0;
}

int GuildManager::UpdateGuildRank(const unsigned captionId, const Guild & guild)
{
	ACE_TRACE("GuildManager::UpdateGuildRank");

	Player * caption = SocialRelationManager::instance()->GetPlayerByUiid(guild.GuildOwner());
	if (!caption)
		return -1;

	std::vector<GuildRankInfo> & rank = guildRank_[caption->Race()];
	std::vector<GuildRankInfo>::iterator it = rank.erase(std::remove_if(rank.begin(), rank.end(), GuildRankInfo::CheckId(guild.GuildId())), rank.end());

	GuildRankInfo tmp;
	memset(&tmp, 0, sizeof(tmp));
	tmp.unionNameLen = StringToBuf(guild.GuildName(), tmp.unionName, sizeof(tmp.unionName));
	tmp.unionLevel = guild.Level();
	tmp.guildId = guild.GuildId();
	tmp.captionNameLen = StringToBuf(caption->Name(), tmp.captionName, sizeof(tmp.captionName));
	tmp.unionRace = caption->Race();
	tmp.unionPretige = guild.Prestige();
	tmp.captionId = guild.GuildOwner();
	rank.push_back(tmp);

	std::sort(rank.begin(), rank.end());
	if (rank.size() > 100)
		rank.resize(100);

	return 0;
}

int GuildManager::RemoveGuildRank(const unsigned race, const unsigned guildId)
{
	ACE_TRACE("GuildManager::RemoveGuildRank");

	std::vector<GuildRankInfo> & rank = guildRank_[race];
	rank.erase(std::remove_if(rank.begin(), rank.end(), GuildRankInfo::CheckId(guildId)), rank.end());
	return 0;
}

int GuildManager::ResetGuildSkillList(Guild & guild)
{
	ACE_TRACE("GuildManager::ResetGuildSkillList");

	Guild::SkillList lst;
	for(Guild::SkillList::const_iterator it = guild.GetSkillList().begin(); it != guild.GetSkillList().end(); ++it)
	{
		const Guild::SkillInfo * skill = GetSkill(*it);
		if (!skill)
			continue;

		if (skill->needGuildLevel > guild.Level())
		{
			const Guild::SkillInfo * preSkill = GetSkill(skill->preSkillId);
			if (preSkill)
				lst.push_back(skill->preSkillId);
		}
		else
		{
			lst.push_back(skill->skillId);
		}
	}

	guild.SetSkillList(lst);

	return 0;
}

unsigned GuildManager::GetMaxCount(const unsigned lvl)
{
	std::map<int, guildbasiccfg_t>::const_iterator it = guildbasiccfg_.find(lvl);
	if (it != guildbasiccfg_.end())
		return it->second.maxPopulation;

	return 0;
}

const Guild::SkillInfo * GuildManager::GetSkill(const unsigned id)
{
	for(std::vector<Guild::SkillInfo>::const_iterator it = skills_.begin(); it != skills_.end(); ++it)
	{
		if (id == it->skillId)
			return &(*it);
	}
	return 0;
}

const unsigned GuildManager::GetTaskPostNeedActive(const unsigned duration)
{
	std::map<int, posttaskexpendcfg_t>::const_iterator it = posttaskexpendcfg_.find(duration);
	if (it != posttaskexpendcfg_.end())
		return it->second.expendActive;

	return 0;
}

const unsigned GuildManager::GetAdvanceGuildNeedActive(const unsigned level)
{
	std::map<int, guildbasiccfg_t>::const_iterator it = guildbasiccfg_.find(level);
	if (it != guildbasiccfg_.end())
		return it->second.advanceExpendActive;

	return 0;
}

const unsigned GuildManager::GetAdvanceGuildGainActive(const unsigned level)
{
	std::map<int, guildbasiccfg_t>::const_iterator it = guildbasiccfg_.find(level);
	if (it != guildbasiccfg_.end())
		return it->second.advanceGainActive;

	return 0;
}

void GuildManager::GetAvailableSkillList(const Guild & guild, Guild::SkillList & lst)
{
	for(std::vector<Guild::SkillInfo>::const_iterator it = skills_.begin(); it != skills_.end(); ++it)
	{
		if (it->needGuildLevel == guild.GuildId() && 
			std::find(guild.GetSkillList().begin(), guild.GetSkillList().end(), it->skillId) == guild.GetSkillList().end())
		{
			lst.push_back(it->skillId);
		}
	}
}

const unsigned GuildManager::GetActiveByUplineDuration(const unsigned t)
{
	const unsigned h = t / 3600;

	std::map<int, gainactivewaycfg_t>::const_iterator it = gainactivewaycfg_.find(1);
	if (it == gainactivewaycfg_.end())
	{
		SS_ERROR("cannot find gain active way(1) config\n");
		return 0;
	}

	return ((it->second.gainActive) * h) / (it->second.needHours) ;
}

const unsigned GuildManager::_Eval(RelationServiceNS::UnionSkill (&l)[10], const Guild::SkillList & lst)
{
	int i = 0;
	for(Guild::SkillList::const_iterator it = lst.begin(); it != lst.end() && i < 10; ++it)
	{
		const Guild::SkillInfo * info = GetSkill(*it);
		if (info)
		{
			l[i].skillId = info->skillId;
			l[i].skillLevel = info->skillLevel;

			++i;
		}
	}
	return i;
}

void GuildManager::LoadGuildBasicCfg()
{
	ACE_TRACE("GuildManager::load_guild_config()");

	TiXmlDocument cfg;
	if (!cfg.LoadFile("./config/guild/guildconfig.xml"))
	{
		SS_ERROR("failed to load guild xml config\n");
		return;
	}

	TiXmlElement * root = cfg.RootElement();
	if (!root)
	{
		SS_ERROR("cannot find guild xml config root element\n");
		return;
	}

	SS_INFO("loading GuildBasic config.......\n");
	TiXmlElement * nodeGuildBasic = root->FirstChildElement("GuildBasic");
	while(nodeGuildBasic)
	{
		TiXmlAttribute * attr = nodeGuildBasic->FirstAttribute();
		guildbasiccfg_t basicCfg;
		memset(&basicCfg, 0, sizeof(basicCfg));

		while(attr)
		{
			if (strcmp(attr->Name(), "level") == 0)
			{
				basicCfg.level = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "MaxPopulation") == 0)
			{
				basicCfg.maxPopulation = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "DayExpendActive") == 0)
			{
				basicCfg.dayExpendActive = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "AdvanceExpendActive") == 0)
			{
				basicCfg.advanceExpendActive = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "AdvanceGainActive") == 0)
			{
				basicCfg.advanceGainActive = atoi(attr->Value());
			}
			else
			{
				SS_ERROR("unknown attribute %s\n", attr->Name());
				return;
			}

			attr = attr->Next();
		}

		guildbasiccfg_[basicCfg.level] = basicCfg;
		
		nodeGuildBasic = nodeGuildBasic->NextSiblingElement("GuildBasic");
	}

	SS_INFO("load PostTaskExpend config......\n");
	TiXmlElement * nodePostTaskExpend = root->FirstChildElement("PostTaskExpend");
	while(nodePostTaskExpend)
	{
		TiXmlAttribute * attr = nodePostTaskExpend->FirstAttribute();
		posttaskexpendcfg_t cfg;
		memset(&cfg, 0, sizeof(cfg));

		while(attr)
		{			
			if (strcmp(attr->Name(), "Duration") == 0)
			{
				cfg.duration = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "ExpendActive") == 0)
			{
				cfg.expendActive = atoi(attr->Value());
			}
			else
			{
				SS_ERROR("unknown attribute %s\n", attr->Name());
				return;
			}

			posttaskexpendcfg_[cfg.duration] = cfg;

			attr = attr->Next();
		}

		nodePostTaskExpend = nodePostTaskExpend->NextSiblingElement("PostTaskExpend");
	}

	SS_INFO("load StudySkillExpend config......\n");
	TiXmlElement * nodeStudySkillExpend = root->FirstChildElement("StudySkillExpend");
	while(nodeStudySkillExpend)
	{
		TiXmlAttribute * attr = nodeStudySkillExpend->FirstAttribute();
		studyskillexpendcfg_t cfg;
		memset(&cfg, 0, sizeof(cfg));
		while(attr)
		{
			if (strcmp(attr->Name(), "SkillLevel") == 0)
			{
				cfg.skillLevel = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "ExpendActive") == 0)
			{
				cfg.expendActive = atoi(attr->Value());
			}
			else
			{
				SS_ERROR("unknown attribute %s\n", attr->Name());
				return;
			}

			attr = attr->Next();
		}

		studyskillexpendcfg_[cfg.skillLevel] = cfg;

		nodeStudySkillExpend = nodeStudySkillExpend->NextSiblingElement("StudySkillExpend");
	}

	SS_INFO("load ExtendStoreExpend config......\n");
	TiXmlElement * nodeExtendStoreExpend = root->FirstChildElement("ExtendStoreExpend");
	while(nodeExtendStoreExpend)
	{
		TiXmlAttribute * attr = nodeExtendStoreExpend->FirstAttribute();
		extendstoreexpendcfg_t cfg;
		memset(&cfg, 0, sizeof(cfg));
		while(attr)
		{
			if (strcmp(attr->Name(), "Row") == 0)
			{
				cfg.row = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "ExpendActive") == 0)
			{
				cfg.expendActive = atoi(attr->Value());
			}
			else
			{
				SS_ERROR("unknown attribute %s\n", attr->Name());
				return;
			}

			attr = attr->Next();
		}

		extendstoreexpendcfg_[cfg.row] = cfg;

		nodeExtendStoreExpend = nodeExtendStoreExpend->NextSiblingElement("ExtendStoreExpend");
	}

	SS_INFO("load GainActiveWay config......\n");
	TiXmlElement * nodeGainActiveWay = root->FirstChildElement("GainActiveWay");
	while(nodeGainActiveWay)
	{
		TiXmlAttribute * attr = nodeGainActiveWay->FirstAttribute();
		gainactivewaycfg_t cfg;
		memset(&cfg, 0, sizeof(cfg));
		while(attr)
		{
			if (strcmp(attr->Name(), "way") == 0)
			{
				cfg.way = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "GainActive") == 0)
			{
				std::vector<std::string> tmp;
				utilib::split(tmp, attr->Value(), attr->Value() + strlen(attr->Value()), ':');
				if (tmp.size() == 2)
				{
					cfg.gainActive = atoi(tmp[0].c_str());
					cfg.needHours = atoi(tmp[1].c_str());	
					if (cfg.needHours == 0)
						cfg.needHours = 1;
				}
				else if (tmp.size() == 1)
				{
					cfg.needHours = 0;
					cfg.gainActive = atoi(tmp[0].c_str());
				}
				else
				{
					SS_ERROR("GainActiveWay config error\n");
					return;
				}
				
			}
			else
			{
				SS_ERROR("unknown attribute %s\n", attr->Name());
				return;
			}

			attr = attr->Next();
		}

		gainactivewaycfg_[cfg.way] = cfg;

		nodeGainActiveWay = nodeGainActiveWay->NextSiblingElement("GainActiveWay");
	}
}

void GuildManager::LoadGuildSkillCfg()
{
	ACE_TRACE("GuildManager::load_guild_skill_config");

	TiXmlDocument cfg;
	if (!cfg.LoadFile("./config/guild/guildskill.xml"))
	{
		SS_ERROR("failed to load guideskill xml config\n");
		return;
	}

	TiXmlElement * root = cfg.RootElement();
	if (!root)
	{
		SS_ERROR("cannot find guideskill xml config root element\n");
		return;
	}

	SS_INFO("load GainActiveWay config......\n");
	TiXmlElement * nodeSkill = root->FirstChildElement("skill");
	while(nodeSkill)
	{
		TiXmlAttribute * attr = nodeSkill->FirstAttribute();
		Guild::SkillInfo skill;
		memset(&skill, 0, sizeof(skill));
		while(attr)
		{
			if(strcmp(attr->Name(), "nUpgradeLevel") == 0)
			{
				skill.needGuildLevel = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "nID") == 0)
			{
				skill.skillId = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "nLevel") == 0)
			{
				skill.skillLevel = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "nNextSkillID") == 0)
			{
				skill.nextSkillId = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "") == 0)
			{
			}

			attr = attr->Next();
		}

		for(size_t i = 0; i < skills_.size(); ++i)
		{
			if (skills_[i].nextSkillId == skill.skillId)
				skill.preSkillId = skills_[i].skillId;
		}

		std::map<int, studyskillexpendcfg_t>::const_iterator it = studyskillexpendcfg_.find(skill.skillLevel);
		if (it != studyskillexpendcfg_.end())
			skill.expendActive = it->second.expendActive;
		skills_.push_back(skill);

		nodeSkill = nodeSkill->NextSiblingElement("skill");
	}

	for(std::vector<Guild::SkillInfo>::iterator it = skills_.begin(); it != skills_.end(); ++it)
	{
		for(std::vector<Guild::SkillInfo>::iterator preIt = skills_.begin(); preIt != skills_.end(); ++preIt)
		{
			if (it->nextSkillId == preIt->skillId)
			{
				preIt->preSkillId = it->skillId;
			}
		}
	}
}

void GuildManager::LoadGuildSkillShopCfg()
{
	ACE_TRACE("GuildManager::LoadGuildSkillShopCfg");

	TiXmlDocument cfg;
	if (!cfg.LoadFile("./config/guild/skillshop.xml"))
	{
		SS_ERROR("failed to load skillshop xml config\n");
		return;
	}

	TiXmlElement * root = cfg.RootElement();
	if (!root)
	{
		SS_ERROR("cannot find skillshop xml config root element\n");
		return;
	}

	SS_INFO("load GuildSkill config......\n");
	TiXmlElement * nodeSkill = root->FirstChildElement("SkillShop");
	while(nodeSkill)
	{
		const char * attr = nodeSkill->Attribute("ID");
		if (!attr)
		{
			SS_ERROR("cannot find ID attribute for SkillShop\n");
			return;
		}

		if (strcmp(attr, "9007") == 0)
		{
			TiXmlElement * label = nodeSkill->FirstChildElement("Label");
			if (!label)
			{
				SS_ERROR("cannot find Label element for SkillShop\n");
				return;
			}

			TiXmlElement * skill = label->FirstChildElement("Skill");
			if (!skill)
			{
				SS_ERROR("cannot find Skill element for SkillShop\n");
				return;
			}
			
			while(skill)
			{
				const char * attr = skill->Attribute("SkillID");
				if (!attr)
				{
					SS_ERROR("cannot find SkillID attribute for Skill\n");
					return;
				}

				unsigned skillId = atoi(attr);

				attr = skill->Attribute("TradePrice");

				unsigned price = atoi(attr);

				for(std::vector<Guild::SkillInfo>::iterator it = skills_.begin(); it != skills_.end(); ++it)
				{
					if (it->skillId == skillId)
					{
						it->expendActive = price;
						break;
					}
				}
			
				skill = skill->NextSiblingElement("Skill");
			}

			break;
		}

		nodeSkill = nodeSkill->NextSiblingElement("SkillShop");
	}
}

void GuildManager::LoadGuildRank()
{
	LoadGuildRank(1);
	LoadGuildRank(2);
	LoadGuildRank(3);
}

void GuildManager::LoadGuildRank(unsigned race)
{
	ACE_TRACE("GuildManager::LoadGuildRank");

	std::vector<Guild> rank;
	SocialRelationManager::instance()->GetDB().GetGuildRank(rank, race);
	for(std::vector<Guild>::iterator it = rank.begin(); it != rank.end(); ++it)
	{
		GuildRankInfo info;
		info.unionNameLen = StringToBuf(it->GuildName(), info.unionName, sizeof(info.unionName));
		info.unionLevel = it->Level();
		Player * player = SocialRelationManager::instance()->GetPlayerByUiid(it->GuildOwner());
		if (player)
		{
			info.captionNameLen = StringToBuf(player->Name(), info.captionName, sizeof(info.captionName));
			info.captionId = player->Uiid();
		}
		info.unionRace = race;
		info.unionPretige = it->Prestige();		
		info.guildId = it->GuildId();

		guildRank_[race].push_back(info);
	}

	std::sort(guildRank_[race].begin(), guildRank_[race].end());
}

const bool GuildManager::RepeatInviteCheck(const unsigned invitor, const unsigned invitee)const
{
	ACE_TRACE("GuildManager::RepeatInviteCheck");

	typedef std::multimap<unsigned, InviteInfo> InviteMap;

	std::pair<InviteMap::const_iterator, InviteMap::const_iterator> range = invitees_.equal_range(invitee);
	for(InviteMap::const_iterator it = range.first; it != range.second; ++it)
	{
		if (it->second.invitor == invitor)
		{
			if (time(0) - it->second.inviteTime <= 60)
				return false;
		}
	}

	return true;
}

const bool GuildManager::CheckSkillStudyed(const unsigned skillid, const Guild & guild)const
{
	ACE_TRACE("GuildManager::CheckSkillStudyed");

	std::vector<Guild::SkillInfo>::const_iterator it = std::find(skills_.begin(), skills_.end(), skillid);
	if (it == skills_.end())
		return true;

	if (std::find(guild.GetSkillList().begin(), guild.GetSkillList().end(), skillid) != guild.GetSkillList().end())
		return true;

	for(;;)
	{
		std::vector<Guild::SkillInfo>::const_iterator it2 = std::find(skills_.begin(), skills_.end(), it->nextSkillId);
		if (it2 == skills_.end())
			return false;

		if (std::find(guild.GetSkillList().begin(), guild.GetSkillList().end(), it2->skillId) != guild.GetSkillList().end())
			return true;

		it = it2;
	}

	return false;
}

const unsigned GuildManager::GetModifyBadgeNeedActive(const Guild & guild)
{
	ACE_TRACE("GuildManager::GetModifyBadgeNeedActive");

	return 0;
}

__SERVICE_SPACE_END_NS__