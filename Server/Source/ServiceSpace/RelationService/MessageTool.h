﻿/********************************************************************
	created:	2008/07/21
	created:	21:7:2008   13:45
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\RelationService\message_tool.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\RelationService
	file base:	message_tool
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __SERVICE_SPACE_RELATION_SERVICE_MESSAGE_TOOL_H_
#define __SERVICE_SPACE_RELATION_SERVICE_MESSAGE_TOOL_H_

#include "../ServiceSpaceLib/StdHdr.h"

__SERVICE_SPACE_BEGIN_NS__



const char * const message_header_format = "%2u%1u%2u";

inline void build_message_header(char * buf, short msgLen, char msgType, short msgId)
{
	short * tmp = (short *)buf;
	//*tmp = htons(msgLen);
	*tmp = msgLen;
	buf += 2;
	*buf = msgType;
	++buf;
	tmp = (short *)buf;
	//*tmp = htons(msgId);
	*tmp = msgId;
}

inline void parse_message_header(const char * buf, short & msgLen, char & msgType, short & msgId)
{
	short * tmp = (short *)buf;
	//msgLen = ntohs(*tmp);
	msgLen = *tmp;
	buf += 2;
	msgType = *buf;
	++buf;
	tmp = (short *)buf;
	//msgId = ntohs(*tmp);
	msgId = *tmp;
}

__SERVICE_SPACE_END_NS__

#endif