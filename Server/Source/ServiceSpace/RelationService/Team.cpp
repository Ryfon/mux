﻿#include "Team.h"
#include <algorithm>

__SERVICE_SPACE_BEGIN_NS__


Team::Team()
: id_(0)
, caption_()
, goodSharedMode_(0)
, expSharedMode_(0)
, rollItemLevel_(0)
, exp_(0)
{
}

Team::~Team()
{
}


const unsigned Team::TeamId()const
{
	return id_;
}

const Player::TmpId Team::TeamCaption()const
{
	return caption_;
}

const unsigned Team::GoodSharedMode()const
{
	return goodSharedMode_;
}

const unsigned Team::ExpSharedMode()const
{
	return expSharedMode_;
}

const unsigned Team::RollItemLevel()const
{
	return rollItemLevel_;
}

const Team::MemberList & Team::GetMemberList()const
{
	return members_;
}

Team::MemberList & Team::GetMemberList()
{
	return members_;
}

const unsigned Team::GetMemberSeq(const Player::TmpId id)const
{
	unsigned seq = 0;
	for(MemberList::const_iterator it = members_.begin(); it != members_.end(); ++it, ++seq)
	{
		if (id == it->tmpId)
		{
			return seq;
		}
	}
	return -1;
}

void Team::SetTeamId(const unsigned id)
{
	id_ = id;
}

void Team::SetTeamCaption(const Player::TmpId id)
{
	caption_ = id;
}

void Team::SetGoodSharedMode(const unsigned m)
{
	goodSharedMode_ = m;
}

void Team::SetExpSharedMode(const unsigned m)
{
	expSharedMode_ = m;
}

void Team::SetRollItemLevel(const unsigned lvl)
{
	rollItemLevel_ = lvl;
}

void Team::AddMember(const Player::TmpId id)
{
	if (!IsMember(id))
		members_.push_back(id);
}

void Team::RemoveMember(const Player::TmpId id)
{
	for(MemberList::iterator it = members_.begin(); it != members_.end(); ++it)
	{
		if (id == it->tmpId)
		{
			members_.erase(it);
			break;
		}
	}
}

const bool Team::IsMember(const Player::TmpId id)const
{
	for(MemberList::const_iterator it = members_.begin(); it != members_.end(); ++it)
	{
		if (id == it->tmpId)
			return true;
	}
	return false;
}

unsigned Team::GetExp()const
{
	return exp_;
}

void Team::SetExp(unsigned exp)
{
	exp_ = exp;
}

__SERVICE_SPACE_END_NS__