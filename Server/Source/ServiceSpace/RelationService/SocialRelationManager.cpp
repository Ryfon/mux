﻿#include "SocialRelationManager.h"
#include "ChatGroupManager.h"

__SERVICE_SPACE_BEGIN_NS__

const unsigned ActiveCalInterval = 1 * 60;

const bool DiffDayCheck(const time_t t1, const time_t t2)
{
	tm * ptm1 = localtime(&t1);
	tm tm1 = *ptm1;
	tm * ptm2 = localtime(&t2);
	tm tm2 = *ptm2;

	if (tm1.tm_year != tm2.tm_year || tm1.tm_mon != tm2.tm_mon || tm1.tm_mday != tm2.tm_mday)
		return true;
	//if (t1 / 86400 != t2 / 86400)
	//	return true;

	return false;
}

const unsigned GetSameDayMidnight(const time_t t)
{
	tm tmp = *localtime(&t);
	tmp.tm_hour = 23;
	tmp.tm_min = 59;
	tmp.tm_sec = 59;
	return mktime(&tmp);
}

template<class T>
static const bool MessageParse(char * message, const unsigned len, T & t)
{
	static RelationServiceNS::RelationServiceProtocol protocol;
	if (!message || len < 5)
		return false;

	if (-1 == protocol.DeCode(T::wCmd, &t, sizeof(t), message + 5, len - 5))
	{
		SS_ERROR("decode error for message %d\n", T::wCmd);
		return false;
	}

	return true;
}

static int GetMessageId(const char * message, const unsigned len)
{
	short _len;
	short id;
	char type;
	parse_message_header(const_cast<char *>(message), _len, type, id);
	return (int)id;
}

static const std::string & GetTmpIdString(const unsigned gateId, const unsigned playerId, std::string & str)
{
	char buf[128];
	sprintf(buf, "%u%u", gateId, playerId);
	str = buf;
	return str;
}

const size_t BuffToString(const char * buf, const size_t len, const size_t maxLen, std::string & str)
{
	if (!buf) return 0;

	size_t n = len;
	if (n > maxLen)
	{
		SS_ERROR("unexpected buf len %u\n", n);
		n = maxLen;
	}
	str = std::string(buf, n);
	return n;
}

const size_t StringToBuf(const std::string & str, char * buf, const size_t maxLen)
{
	if (!buf) return 0;

	size_t n = str.size();
	if (n > maxLen)
	{
		SS_ERROR("unexpected string len %u\n", n);
		n = maxLen;
	}
	memcpy(buf, str.c_str(), n);
	return n;
}

SocialRelationManager * SocialRelationManager::instance()
{
	static SocialRelationManager inst;
	return &inst;
}

int SocialRelationManager::init(const std::string & str)
{
	ACE_TRACE("SocialRelationManager::init");

	maxGuildId_ = 0;

	handlers_[RelationServiceNS::GRUplineInfo::wCmd] = &SocialRelationManager::HandlePlayerUplineMessage;
	handlers_[RelationServiceNS::GRInfoUpdate::wCmd] = &SocialRelationManager::HandleUpdatePlayerInfoMessage;
	handlers_[RelationServiceNS::G_R_DelPlayer::wCmd] = &SocialRelationManager::HandleDeletePlayerMessage;
	handlers_[RelationServiceNS::CreateUnionRequest::wCmd] = &SocialRelationManager::HandleCreateGuildMessage;
	handlers_[RelationServiceNS::AddUnionMemberRequest::wCmd] = &SocialRelationManager::HandleInvitePlayerJoinGuildMessage;
	handlers_[RelationServiceNS::RemoveUnionMemberRequest::wCmd] = &SocialRelationManager::HandleExitGuildMemberMessage;
	handlers_[RelationServiceNS::DestroyUnionRequest::wCmd] = &SocialRelationManager::HandleDestroyGuildMessage;
	handlers_[RelationServiceNS::AddUnionMemberConfirmed::wCmd] = &SocialRelationManager::HandleInvitePlayerConfirmedMessage;
	handlers_[RelationServiceNS::QueryUnionBasicRequest::wCmd] = &SocialRelationManager::HandleQueryGuildBasicInfoMessage;
	handlers_[RelationServiceNS::QueryUnionMembersRequest::wCmd] = &SocialRelationManager::HandleQueryGuildMemberListMessage;
	handlers_[RelationServiceNS::ModifyUnionMemberTitleRequest::wCmd] = &SocialRelationManager::HandleModifyGuildMemberTitleMessage;
	handlers_[RelationServiceNS::TransformUnionCaptionRequest::wCmd] = &SocialRelationManager::HandleTransformGuildCaptionMessage;
	handlers_[RelationServiceNS::AdvanceUnionMemberPosRequest::wCmd] = &SocialRelationManager::HandleAdvanceGuildMemberPosMessage;
	handlers_[RelationServiceNS::ReduceUnionMemberPosRequest::wCmd] = &SocialRelationManager::HandleReduceGuildMemberPosMessage;
	handlers_[RelationServiceNS::UnionChannelSpeekRequest::wCmd] = &SocialRelationManager::HandleGuildChannelSpeekMessage;
	handlers_[RelationServiceNS::UpdateUnionPosCfgRequest::wCmd] = &SocialRelationManager::HandleUpdateGuildPosListMessage;
	handlers_[RelationServiceNS::StudyUnionSkillRequest::wCmd] = &SocialRelationManager::HandleStudyUnionSkillMessage;
	handlers_[RelationServiceNS::PostUnionTasksListRequest::wCmd] = &SocialRelationManager::HandlePostGuildTaskMessage;
	handlers_[RelationServiceNS::AdvanceUnionLevelRequest::wCmd] = &SocialRelationManager::HandleAdvanceGuildLevelMessage;
	handlers_[RelationServiceNS::PostUnionBulletinRequest::wCmd] = &SocialRelationManager::HandlePostGuildBulletinMessage;
	handlers_[RelationServiceNS::QueryUnionLogRequest::wCmd] = &SocialRelationManager::HandleQueryGuildLogMessage;
	handlers_[RelationServiceNS::ModifyUnionActivePointRequest::wCmd] = &SocialRelationManager::HandleModifyGuildActiveMessage;
	handlers_[RelationServiceNS::ForbidUnionSpeekRequest::wCmd] = &SocialRelationManager::HandleForbidGuildMemberMessage;
	handlers_[RelationServiceNS::UnionOwnerCheckRequest::wCmd] = &SocialRelationManager::HandleGuildOwnerCheckMessage;
	handlers_[RelationServiceNS::ModifyUnionPrestigeRequest::wCmd] = &SocialRelationManager::HandleModifyGuildPrestigeMessage;
	handlers_[RelationServiceNS::ModifyUnionBadgeRequest::wCmd] = &SocialRelationManager::HandleModifyGuildBadgeMessage;
	handlers_[RelationServiceNS::ModifyUnionMultiRequest::wCmd] = &SocialRelationManager::HandleModifyGuildMultMessage;

	handlers_[RelationServiceNS::GRAddFriend::wCmd] = &SocialRelationManager::HandleAddFriendRequestMessage;
	handlers_[RelationServiceNS::GRDelFriend::wCmd] = &SocialRelationManager::HandleRemoveFrienRequestMessage;
	handlers_[RelationServiceNS::GRAddGroup::wCmd] = &SocialRelationManager::HandleCreeateFriendGroupMessage;
	handlers_[RelationServiceNS::GRDelGroup::wCmd] = &SocialRelationManager::HandleDeleteFriendGroupMessage;
	handlers_[RelationServiceNS::GRAlterGroupName::wCmd] = &SocialRelationManager::HandleRenameFriendGroupMessage;
	handlers_[RelationServiceNS::GRFriendAlterGroupID::wCmd] = &SocialRelationManager::HandleTransferFriendMessage;
	handlers_[RelationServiceNS::GRAddToBlacklist::wCmd] = &SocialRelationManager::HandleAddBlacklistPlayerMessage;
	handlers_[RelationServiceNS::GRDelFromBlacklist::wCmd] = &SocialRelationManager::HandleRemoveBlacklistPlayerMessage;
	handlers_[RelationServiceNS::GRSendGroupMsg::wCmd] = &SocialRelationManager::HandleSendFriendGroupMessage;
	handlers_[RelationServiceNS::GRSendMsg::wCmd] = &SocialRelationManager::HandleSendPersonalMessage;
	handlers_[RelationServiceNS::GRAddEnemy::wCmd] = &SocialRelationManager::HandleAddEnemyMessage;
	handlers_[RelationServiceNS::GRDelEnemy::wCmd] = &SocialRelationManager::HandleRemoveEnemyMessage;
	handlers_[RelationServiceNS::GRLockEnemy::wCmd] = &SocialRelationManager::HandleLockEnemyMessage;

	handlers_[RelationServiceNS::CreateTeamRequest::wCmd] = &SocialRelationManager::HandleCreateTeamMessage;
	handlers_[RelationServiceNS::DestroyTeamRequest::wCmd] =  &SocialRelationManager::HandleDestroyTeamMessage;
	handlers_[RelationServiceNS::AddTeamMemberRequest::wCmd] =  &SocialRelationManager::HandleAddTeamMemberMessage;
	handlers_[RelationServiceNS::RemoveTeamMemberRequest::wCmd] =  &SocialRelationManager::HandleRemoveTeamMemberMessage;
	handlers_[RelationServiceNS::QueryTeamRequest::wCmd] =  &SocialRelationManager::HandleQueryTeamMessage;
	handlers_[RelationServiceNS::QueryTeamMemberRequest::wCmd] =  &SocialRelationManager::HandleQueryTeamMemberMessage;
	handlers_[RelationServiceNS::ModifyTeamCaptainRequest::wCmd] =  &SocialRelationManager::HandleModifyTeamCaptionMessage;
	handlers_[RelationServiceNS::ModifyExpSharedModeRequest::wCmd] =  &SocialRelationManager::HandleModifyTeamExpModeMessage;
	handlers_[RelationServiceNS::ModifyGoodSharedModeRequest::wCmd] =  &SocialRelationManager::HandleModifyTeamGoodModeMessage;
	handlers_[RelationServiceNS::TeamBroadcastRequest::wCmd] =  &SocialRelationManager::HandleTeamChannelSpeekMessage;
	handlers_[RelationServiceNS::ModifyTeamRollItemLevelRequest::wCmd] =  &SocialRelationManager::HandleModifyTeamRollItemLevelMessage;
	handlers_[RelationServiceNS::QueryTeamMembersCountRequest::wCmd] =  &SocialRelationManager::HandleQueryTeamMemberCountMessage;

	handlers_[RelationServiceNS::CreateChatGroupRequest::wCmd] =  &SocialRelationManager::HandleCreateChatGroupMessage;
	handlers_[RelationServiceNS::DestroyChatGroupRequest::wCmd] =  &SocialRelationManager::HandleDestroyChatGroupMessage;
	handlers_[RelationServiceNS::AddChatGroupMemberRequest::wCmd] =  &SocialRelationManager::HandleAddChatGroupMemberMessage;
	handlers_[RelationServiceNS::RemoveChatGroupMemberRequest::wCmd] =  &SocialRelationManager::HandleRemoveChatGroupMember;
	handlers_[RelationServiceNS::ChatGroupSpeekRequest::wCmd] =  &SocialRelationManager::HandleChatGroupChannelSpeek;

	handlers_[RelationServiceNS::QueryPlayerRelationRequest::wCmd] =  &SocialRelationManager::HandleQueryPlayerRelationRequest;
	handlers_[RelationServiceNS::QueryPlayerInfoRequest::wCmd] = &SocialRelationManager::HandleQueryPlayerInfoRequest;

	handlers_[RelationServiceNS::QueryUnionRankRequest::wCmd] = &SocialRelationManager::HandleQueryGuildRankRequest;
	handlers_[RelationServiceNS::SearchUnionRankRequest::wCmd] = &SocialRelationManager::HandleSearchGuildRankRequest;

	handlers_[RelationServiceNS::QueryRaceMasterRequest::wCmd] = &SocialRelationManager::HandleQueryRaceMasterRequest;
	handlers_[RelationServiceNS::QueryGuildMembersRequest::wCmd] = &SocialRelationManager::HandleQueryGuildMembersRequest;

	handlers_[RelationServiceNS::AddFriendlyDegree::wCmd] = &SocialRelationManager::HandleAddFriendlyDegreeRequest;

	handlers_[RelationServiceNS::AddTweet::wCmd] = &SocialRelationManager::HandleAddTweetRequest;

	handlers_[RelationServiceNS::QueryFriendTweetsRequest::wCmd] = &SocialRelationManager:: HandleQueryTweetsRequest;

	handlers_[RelationServiceNS::QueryFriendsListRequest::wCmd] = &SocialRelationManager::HandleQueryFriendsListRequest;

	handlers_[RelationServiceNS::NotifyExpAdded::wCmd] = &SocialRelationManager::HandleNotifyExpAdded;

	handlers_[RelationServiceNS::QueryExpNeededWhenLvlUpResult::wCmd] = &SocialRelationManager::HandleQueryExpNeededWhenLvlUpResult;

	handlers_[RelationServiceNS::ModifyTweetReceiveFlag::wCmd] = &SocialRelationManager::HandleTweetFlagModify;

	handlers_[RelationServiceNS::GRQueryNickUnion::wCmd] = &SocialRelationManager::HandleQueryPlayerGuild;

	if (-1 == db_.init(str))
		return -1;

	/*for(int i = 0; i < 10000000; ++i)
	{
		for(int j = 0; j < 4; ++j)
		{
			char buf[32];
			sprintf(buf, "Robot1%07d_%d", i, j);

			Player * p = GetPlayerByName(buf);
			if (!p)
				continue;

			p->Level(30);
			p->LineState(Player::LINE_ONLINE);

			RelationServiceNS::CreateUnionRequest req;
			memset(&req, 0, sizeof(req));
			req._union.nameLen = strlen(buf);
			memcpy(req._union.name, buf, req._union.nameLen);
			req._union.ownerUiid = p->Uiid();

			tcp_session s = RelationServiceMessageHandler::get_session(0);
			guildManager_.HandleCreateGuild(s, req);
		}
	}*/
	activeCalFlag_ = false;
	lastCalTime_ = 0;
	ACE_Time_Value tv(ActiveCalInterval);
	ACE_Reactor::instance()->schedule_timer(this, 0, tv);

	maxGuildId_ = GetDB().GetMaxGuildId();
	guildManager_.LoadGuildRank(1);
	guildManager_.LoadGuildRank(2);
	guildManager_.LoadGuildRank(3);

	//LoadAllGuilds();
	return 0;
}

Player * SocialRelationManager::GetPlayerByUiid(const unsigned uiid, const bool L)
{
	PlayerMap::iterator it = players_.find(uiid);
	if (it == players_.end())
	{
		if (!L)
			return 0;

		Player player;
		db_.Load(uiid, player);
		if (uiid == player.Uiid())
		{
			AddPlayer(player);
			it = players_.find(uiid);
		}
	}

	return it == players_.end() ? 0 : &it->second;
}

Player * SocialRelationManager::GetPlayerByName(const std::string & name, const bool L)
{
	if (name.empty())
		return 0;

	PlayerNameMap::const_iterator it = names_.find(name);
	if (it == names_.end())	
	{
		if (!L)
			return 0;

		Player player;
		db_.Load(name, player);
		bool b = name == player.Name();
		if (b)
		{
			AddPlayer(player);
			it = names_.find(name);
		}
	}

	return it != names_.end() ? GetPlayerByUiid(it->second) : 0;
}

Player * SocialRelationManager::GetPlayerByTmpId(const unsigned gateId, const unsigned tmpId)
{
	std::string tmp;	
	PlayerIdMap::const_iterator it = tmpIds_.find(GetTmpIdString(gateId, tmpId, tmp));
	if (it == tmpIds_.end())
		return 0;

	return GetPlayerByUiid(it->second);
}

Player * SocialRelationManager::GetPlayerByTmpId(const Player::TmpId & id)
{
	return GetPlayerByTmpId(id.gateId, id.playerId);
}

Guild  * SocialRelationManager::GetGuildById(const unsigned id)
{
	GuildMap::iterator it = guilds_.find(id);
	if (it == guilds_.end())
		return 0;

	return &it->second;
}

Guild  * SocialRelationManager::GetGuildByName(const std::string & name)
{
	GuildNameMap::const_iterator it = guildNames_.find(name);
	if (it == guildNames_.end())
		return 0;

	return GetGuildById(it->second);
}

void SocialRelationManager::AddGuild(Guild & guild)
{
	++maxGuildId_;
	guild.GuildId(maxGuildId_);
	guilds_[guild.GuildId()] = guild;
	guildNames_[guild.GuildName()] = guild.GuildId();
}

void SocialRelationManager::RemoveGuild(Guild & guild)
{
	guildNames_.erase(guild.GuildName());
	guilds_.erase(guild.GuildId());	
}

void SocialRelationManager::NotifyPlayerFriendGuildInfo(const Player & player)
{
	friendManager_.SendPlayerUpdateInfoNotify(player);
}

TeamManager & SocialRelationManager::GetTeamManager()
{
	return teamManager_;
}

SocialRelationDB & SocialRelationManager::GetDB()
{
	return db_;
}

int SocialRelationManager::HandleMessage(tcp_session session, const char * message, const unsigned len)
{
	ACE_TRACE("SocialRelationManager::HandleMessage");

	if (!check_session(session) || !message || !len)
	{
		SS_ERROR("invalid arguments detected in SocialRelationManager::HandleMessage calling\n");
		return -1;
	}

	int messageId = GetMessageId(message, len);
	if (messageId == -1)
	{
		SS_ERROR("invalid message id -1 detected\n");
		return -1;
	}
	SS_INFO("\n----------------SocialRelationManager::HandleMessage.%d\n",messageId);

	HandlerMap::iterator it = handlers_.find(messageId);
	if (it == handlers_.end())
	{
		SS_ERROR("invalid message id %d detected\n", messageId);
		return -1;
	}

	return (this->*(it->second))(session, const_cast<char *>(message), len);
}

int SocialRelationManager::DoGuildDayActiveExpend()
{
	ACE_TRACE("SocialRelationManager::DoGuildDayActiveExpend");

	for(GuildMap::iterator it = guilds_.begin(); it != guilds_.end(); ++it)
	{
		guildManager_.HandleGuildDayActiveExpend(it->second);
	}

	return 0;
}

int SocialRelationManager::DoGuildMemberActiveCal()
{
	ACE_TRACE("SocialRelationManager::DoGuildMemberActiveCal");

	for(GuildMap::iterator it = guilds_.begin(); it != guilds_.end(); ++it)
	{
		for(Guild::MemberList::const_iterator mit = it->second.GetMemberList().begin(); mit != it->second.GetMemberList().end(); ++mit)
		{
			Player * player = GetPlayerByUiid(*mit, false);
			if (!player || player->LineState() == Player::LINE_OFFLINE)
				continue;

			if (player->GetAccUplineDay() >= 4 * 3600)
			{
				guildManager_.HandleGuildMemberUplineDuration(player->GetAccUplineDay(), it->second);
				player->SetUplineTime(time(0));
				player->ResetAccUplineTime(0);
			}
		}
	}

	return 0;
}

int SocialRelationManager::DoFriendGroupMsgCountReset()
{
	ACE_TRACE("SocialRelationManager::DoFriendGroupMsgCountReset");

	time_t now = time(0);
	for(PlayerMap::iterator it = players_.begin(); it != players_.end(); ++it)
	{
		if (it->second.LineState() == Player::LINE_OFFLINE)
			continue;

		if (DiffDayCheck(now, it->second.GetBeginCalGroupMsgCountTime()))
		{
			it->second.SetRestGroupsMsgCount(10, now);
			friendManager_.SendPlayerRestGroupMsgCount(it->second);
		}
	}

	return 0;
}

int SocialRelationManager::handle_timeout(const ACE_Time_Value &, const void *)
{
	ACE_TRACE("SocialRelationManager::handle_output");

	ACE_Guard<ACE_Token> guard(token_);

	static time_t lastTime = time(0);
	time_t now = time(0);

	DoGuildMemberActiveCal();

	if (DiffDayCheck(now, lastTime))
	{
		lastTime = now;

		DoGuildDayActiveExpend();
		DoFriendGroupMsgCountReset();
	}

	ACE_Time_Value tv(ActiveCalInterval);
	ACE_Reactor::instance()->schedule_timer(this, 0, tv);

	return 0;
}

int SocialRelationManager::HandleUpdatePlayerInfoMessage(tcp_session session, char * message, const unsigned len)
{
	ACE_TRACE("SocialRelationManager::HandleUpdatePlayerInfo");

	ACE_Guard<ACE_Token> guard(token_);

	RelationServiceNS::GRInfoUpdate rep;
	if (!MessageParse(message, len, rep))
		return -1;

	if (rep.Online == 0)
	{
		return HandlePlayerOffline(session, rep);
	}
	else
	{
		return HandleUpdatePlayerInfo(session, rep);
	}
}

int SocialRelationManager::HandlePlayerOffline(tcp_session session, const RelationServiceNS::GRInfoUpdate & info)
{
	ACE_TRACE("SocialRelationManager::HandlePlayerOffline");

	Player * player = GetPlayerByUiid(info.uiID);
	if (!player)
		return -1;

	player->SetOfflineTime(time(0));
	player->LineState(Player::LINE_OFFLINE);

	if (player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * guild = GetGuildById(player->GetGuildInfo().guildId);
		if (guild)
			guildManager_.HandlePlayerOffline(*player, *guild);
	}

	if (player->GetTeamId() != Player::INVALID_TEAM_ID)
		teamManager_.HandlePlayerOffline(*player);

	if (player->GetChatGroupId() != Player::INVALID_CHATGROUP_ID)
		chatGroupManager_.HandlePlayerOffline(*player);

	std::string tmpStr;
	tmpIds_.erase(GetTmpIdString(player->GateId(), player->PlayerId(), tmpStr));

	friendManager_.HandlePlayerOffline(session, *player);

	player->GateId(0);
	player->PlayerId(0);

	player->AddOffLineTweet();

	db_.Save(*player);

	// erase player from players ???
	// TODO:

	return 0;
}

int SocialRelationManager::HandleUpdatePlayerInfo(tcp_session session, const RelationServiceNS::GRInfoUpdate & info)
{
	ACE_TRACE("SocialRelationManager::HandleUpdatePlayerInfo_i");

	Player * player = GetPlayerByUiid(info.uiID);
	if (!player)
		return -1;

	unsigned lvl = player->Level();
	player->Level(info.playerLevel);
	player->MapId(info.MapID);

	friendManager_.HandlePlayerUpdateInfo(session, *player);

	if (lvl < player->Level())
	{
		friendManager_.SendFriendDegreeGainEmail(session, *player);
		player->AddUpgradeTweet(player->Level());
	}
	
	if (lvl < player->Level() && player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * guild = GetGuildById(player->GetGuildInfo().guildId);
		if (guild)
		{
			guildManager_.HandlePlayerAdvanceLevel(*player, *guild, player->Level() - lvl);
		}
	}

	return 0;
}


int SocialRelationManager::HandleDeletePlayerMessage(tcp_session session, char * message, const unsigned len)
{
	ACE_TRACE("SocialRelationManager::HandleDeletePlayerMessage");

	RelationServiceNS::G_R_DelPlayer req;
	if (!MessageParse(message, len, req))
		return -1;

	return HandleDeletePlayer(session, req);
}

int SocialRelationManager::HandleDeletePlayer(tcp_session session, const RelationServiceNS::G_R_DelPlayer & req)
{
	ACE_TRACE("SocialRelationManager::HandleDeletePlayer");

	Player * player = GetPlayerByUiid(req.uiid);
	if (!player)
		return -1;

	db_.Destroy(*player);

	if (player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * guild = GetGuildById(player->GetGuildInfo().guildId);
		if (guild)
			guildManager_.HandleDeletePlayer(*player, *guild);
	}

	friendManager_.HandleDeletePlayer(*player);

	RemovePlayer(*player);

	player = 0;

	return 0;
}

int SocialRelationManager::HandlePlayerUplineMessage(tcp_session session, char * message, const unsigned len)
{
	ACE_TRACE("SocialRelationManager::HandlePlayerUpline");

	RelationServiceNS::GRUplineInfo rep;
	if (!MessageParse(message, len, rep))
		return -1;

	return HandlePlayerUpline(session, rep);
}

int SocialRelationManager::HandlePlayerUpline(tcp_session session, const RelationServiceNS::GRUplineInfo & info)
{
	ACE_TRACE("SocialRelationManager::HandlePlayerUpline_i");

	if (info.EnterModel != Player::LINE_ONLINE && info.EnterModel != Player::LINE_IRCLINE)
	{
		SS_ERROR("invalid line state %d detected\n", info.EnterModel);
		return -1;
	}

	std::string name;
	BuffToString(info.Name, info.NameLen, sizeof(info.Name), name);

	Player * player = GetPlayerByUiid(info.uiID);
	if (!player)
	{
		Player tmp;
		tmp.Uiid(info.uiID);
		tmp.Name(name);
		tmp.GateId(info.player.GateSrvID);
		tmp.PlayerId(info.player.PPlayer);
		tmp.LineState(info.EnterModel);
		AddPlayer(tmp);

		player = GetPlayerByUiid(info.uiID);
		if (!player)
			return -1;
	}
	else
	{
		if (player->Uiid() != info.uiID || player->Name() != name)
		{
			SS_ERROR("角色信息异常：已有(%u,%s)，登记(%u,%s)\n", player->Uiid(), player->Name().c_str(), info.uiID, name.c_str());
			return -1;
		}

		if (player->LineState() != Player::LINE_OFFLINE)
		{
			std::string tmpStr;
			tmpIds_.erase(GetTmpIdString(player->GateId(), player->PlayerId(), tmpStr));

			SS_ERROR("角色信息异常：上线角色(%s)未下线却再次上线\n", player->Name().c_str());

			player->LineState(Player::LINE_OFFLINE);
		}
	}

	player->Name(name);
	player->Sex(info.Sex);
	player->Race(info.playerRace);
	player->Icon(info.playerIcon);
	player->GateId(info.player.GateSrvID);
	player->PlayerId(info.player.PPlayer);
	player->MapId(info.MapID);

	
	if (player->LineState() == Player::LINE_OFFLINE)
	{
		std::string tmpStr;
		tmpIds_[GetTmpIdString(player->GateId(), player->PlayerId(), tmpStr)] = player->Uiid();
	}


	player->LineState(info.EnterModel);
	player->Level(info.playerLevel);
	player->Job(info.playerClass);

	time_t now = time(0);
	if (DiffDayCheck(now, player->GetOfflineTime()))
		player->ResetAccUplineTime(0);
	player->SetUplineTime(now);

	if (player->GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * guild = LoadPlayerGuild(*player);

		if (guild)
		{
			if (guild->GetMemberList().empty())
				LoadGuildMembers(guild->GuildId());

			guildManager_.HandlePlayerUpline(*player, *guild);
		}
		else
		{
			Player::GuildInfo info;
			player->SetGuildInfo(info);
		}
	}

	friendManager_.HandlePlayerUpline(session, *player);
	teamManager_.HandlePlayerUpline(*player);
	player->AddUpLineTweet();
	
	return 0;
}

void SocialRelationManager::AddPlayer(const Player & player)
{
	ACE_TRACE("SocialRelationManager::AddPlayer");

	players_[player.Uiid()] = player;
	names_[player.Name()] = player.Uiid();
	if (player.LineState() != Player::LINE_OFFLINE)
	{
		std::string idstr;
		tmpIds_[GetTmpIdString(player.GateId(), player.PlayerId(), idstr)] = player.Uiid();
	}
}

void SocialRelationManager::RemovePlayer(const Player & player)
{
	ACE_TRACE("SocialRelationManager::RemovePlayer");

	names_.erase(player.Name());
	std::string idstr;
	tmpIds_.erase(GetTmpIdString(player.GateId(), player.PlayerId(), idstr));
	players_.erase(player.Uiid());
}

void SocialRelationManager::LoadAllGuilds()
{
	ACE_TRACE("SocialRelationManager::LoadAllGuilds");

	std::vector<Guild> vec;
	db_.LoadAll(vec);

	for(std::vector<Guild>::iterator it = vec.begin(); it != vec.end(); ++it)
	{
		if (maxGuildId_ < it->GuildId())
			maxGuildId_ = it->GuildId();

		Guild::MemberList lst;
		db_.LoadAll(it->GuildId(), lst);
		for(Guild::MemberList::const_iterator _it = lst.begin(); _it != lst.end(); ++_it)
		{
			it->AddMember(*_it);
		}

		guilds_[it->GuildId()] = *it;
		guildNames_[it->GuildName()] = it->GuildId();
	}
}

void SocialRelationManager::LoadGuildMembers(const unsigned id)
{
	ACE_TRACE("SocialRelationManager::LoadGuildMembers");

	Guild * guild = GetGuildById(id);
	if (!guild)
		return;

	for(Guild::MemberList::const_iterator it = guild->GetMemberList().begin(); it != guild->GetMemberList().end(); ++it)
	{
		Player * player = GetPlayerByUiid(*it);
		if (player)
			continue;

		Player tmp;
		db_.Load(*it, tmp);
		if (*it != tmp.Uiid())
			continue;

		AddPlayer(tmp);
	}
}

Guild * SocialRelationManager::LoadPlayerGuild(const Player & player)
{
	ACE_TRACE("SocialRelationManager::LoadPlayerGuild");

	Guild * guild = GetGuildById(player.GetGuildInfo().guildId);
	if (!guild)
	{
		Guild tmp;
		db_.Load(player, tmp);
		if (tmp.GuildId() != Player::INVALID_GUILD_ID)
		{
			guilds_[tmp.GuildId()] = tmp;
			guildNames_[tmp.GuildName()] = tmp.GuildId();
			Guild * guild = &guilds_[tmp.GuildId()];

			time_t now = time(0);
			if (guild->GetLastDayExpendCalTime() && DiffDayCheck(guild->GetLastDayExpendCalTime(), now))
			{
				double d = difftime(now, guild->GetLastDayExpendCalTime());
				d /= (3600 *24);

				for(double i = 0; i < d; i += 1.0f)
				{
					if (guild->Level() <= 1)
						break;
					guildManager_.HandleGuildDayActiveExpend(*guild);
				}
			}
		}
	}

	return GetGuildById(player.GetGuildInfo().guildId);
}

int SocialRelationManager::HandleCreateGuildMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::CreateUnionRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleCreateGuild(session, req);
}

int SocialRelationManager::HandleInvitePlayerJoinGuildMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::AddUnionMemberRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleInvitePlayerJoinGuild(session, req);
}

int SocialRelationManager::HandleExitGuildMemberMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::RemoveUnionMemberRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleExitGuildMember(session, req);
}

int SocialRelationManager::HandleDestroyGuildMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::DestroyUnionRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleDestroyGuild(session, req);
}

int SocialRelationManager::HandleInvitePlayerConfirmedMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::AddUnionMemberConfirmed req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleInvitePlayerConfirmed(session, req);
}

int SocialRelationManager::HandleQueryGuildBasicInfoMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryUnionBasicRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleQueryGuildBasicInfo(session, req);
}

int SocialRelationManager::HandleQueryGuildMemberListMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryUnionMembersRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleQueryGuildMemberList(session, req);
}

int SocialRelationManager::HandleModifyGuildMemberTitleMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyUnionMemberTitleRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleModifyGuildMemberTitle(session, req);
}

int SocialRelationManager::HandleTransformGuildCaptionMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::TransformUnionCaptionRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleTransformGuildCaption(session, req);
}

int SocialRelationManager::HandleAdvanceGuildMemberPosMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::AdvanceUnionMemberPosRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleAdvanceGuildMemberPos(session, req);
}

int SocialRelationManager::HandleReduceGuildMemberPosMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ReduceUnionMemberPosRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleReduceGuildMemberPos(session, req);
}

int SocialRelationManager::HandleGuildChannelSpeekMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::UnionChannelSpeekRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleGuildChannelSpeek(session, req);
}

int SocialRelationManager::HandleUpdateGuildPosListMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::UpdateUnionPosCfgRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleUpdateGuildPosList(session, req);
}

int SocialRelationManager::HandleStudyUnionSkillMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::StudyUnionSkillRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleStudyUnionSkill(session, req);
}

int SocialRelationManager::HandlePostGuildTaskMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::PostUnionTasksListRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandlePostGuildTask(session, req);
}

int SocialRelationManager::HandleAdvanceGuildLevelMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::AdvanceUnionLevelRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleAdvanceGuildLevel(session, req);
}

int SocialRelationManager::HandlePostGuildBulletinMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::PostUnionBulletinRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandlePostGuildBulletin(session, req);
}

int SocialRelationManager::HandleQueryGuildLogMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryUnionLogRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleQueryGuildLog(session, req);
}

int SocialRelationManager::HandleModifyGuildActiveMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyUnionActivePointRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleModifyGuildActive(session, req);
}

int SocialRelationManager::HandleForbidGuildMemberMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ForbidUnionSpeekRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleForbidGuildMember(session, req);
}

int SocialRelationManager::HandleGuildOwnerCheckMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::UnionOwnerCheckRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleGuildOwnerCheck(session, req);
}

int SocialRelationManager::HandleModifyGuildPrestigeMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyUnionPrestigeRequest req;
	if (!MessageParse(message, len , req))
		return -1;

	return guildManager_.HandleModifyGuildPrestige(session, req);
}

int SocialRelationManager::HandleModifyGuildBadgeMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyUnionBadgeRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleModifyGuildBadge(session, req);
}

int SocialRelationManager::HandleModifyGuildMultMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyUnionMultiRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleModifyGuildMult(session, req);
}

int SocialRelationManager::HandleAddFriendRequestMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRAddFriend req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleAddFriendRequest(session, req);
}

int SocialRelationManager::HandleRemoveFrienRequestMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRDelFriend req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleRemoveFriendRequest(session, req);
}

int SocialRelationManager::HandleCreeateFriendGroupMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRAddGroup req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleCreeateFriendGroup(session, req);
}

int SocialRelationManager::HandleDeleteFriendGroupMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRDelGroup req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleDeleteFriendGroup(session, req);
}

int SocialRelationManager::HandleRenameFriendGroupMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRAlterGroupName req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleRenameFriendGroup(session, req);
}

int SocialRelationManager::HandleTransferFriendMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRFriendAlterGroupID req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleTransferFriend(session, req);
}

int SocialRelationManager::HandleAddBlacklistPlayerMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRAddToBlacklist req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleAddBlacklistPlayer(session, req);
}

int SocialRelationManager::HandleRemoveBlacklistPlayerMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRDelFromBlacklist req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleRemoveBlacklistPlayer(session, req);
}

int SocialRelationManager::HandleSendFriendGroupMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRSendGroupMsg req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleSendFriendGroupMessage(session, req);
}

int SocialRelationManager::HandleSendPersonalMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRSendMsg req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleSendPersonalMessage(session, req);
}

int SocialRelationManager::HandleAddEnemyMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRAddEnemy req;
	if (!MessageParse(message, len, req))
		return -1;

	RelationServiceNS::GRAddEnemy req2;
	req2.uiID = req.addtouiID;
	req2.addtouiID = req.uiID;
	req2.MapID = req.MapID;
	req2.posX = req.posX;
	req2.posY = req.posY;
	friendManager_.HandleAddEnemy(session, req, false);
	friendManager_.HandleAddEnemy(session, req2, true);
	friendManager_.HandleKillPlayer(session, req);

	return 0;
}

int SocialRelationManager::HandleRemoveEnemyMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRDelEnemy req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleRemoveEnemy(session, req);
}

int SocialRelationManager::HandleLockEnemyMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRLockEnemy req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleLockEnemy(session, req);
}

int SocialRelationManager::HandleCreateTeamMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::CreateTeamRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleCreateTeam(session, req);
}

int SocialRelationManager::HandleDestroyTeamMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::DestroyTeamRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleDestroyTeam(session, req);
}

int SocialRelationManager::HandleAddTeamMemberMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::AddTeamMemberRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleAddTeamMember(session, req);
}

int SocialRelationManager::HandleRemoveTeamMemberMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::RemoveTeamMemberRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleRemoveTeamMember(session, req);
}

int SocialRelationManager::HandleQueryTeamMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryTeamRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleQueryTeam(session, req);
}

int SocialRelationManager::HandleQueryTeamMemberMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryTeamMemberRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleQueryTeamMember(session, req);
}

int SocialRelationManager::HandleModifyTeamCaptionMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyTeamCaptainRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleModifyTeamCaption(session, req);
}

int SocialRelationManager::HandleModifyTeamExpModeMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyExpSharedModeRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleModifyTeamExpMode(session, req);
}

int SocialRelationManager::HandleModifyTeamGoodModeMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyGoodSharedModeRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleModifyTeamGoodMode(session, req);
}

int SocialRelationManager::HandleTeamChannelSpeekMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::TeamBroadcastRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleTeamChannelSpeek(session, req);
}

int SocialRelationManager::HandleModifyTeamRollItemLevelMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyTeamRollItemLevelRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleModifyTeamRollItemLevel(session, req);
}

int SocialRelationManager::HandleQueryTeamMemberCountMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryTeamMembersCountRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return teamManager_.HandleQueryTeamMemberCount(session, req);
}

int SocialRelationManager::HandleCreateChatGroupMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::CreateChatGroupRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return chatGroupManager_.HandleCreateGroup(session, req);
}

int SocialRelationManager::HandleDestroyChatGroupMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::DestroyChatGroupRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return chatGroupManager_.HandleDestroyGroup(session, req);
}

int SocialRelationManager::HandleAddChatGroupMemberMessage(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::AddChatGroupMemberRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return chatGroupManager_.HandleAddMember(session, req);
}

int SocialRelationManager::HandleRemoveChatGroupMember(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::RemoveChatGroupMemberRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return chatGroupManager_.HandleRemoveMember(session, req);
}

int SocialRelationManager::HandleChatGroupChannelSpeek(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ChatGroupSpeekRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return chatGroupManager_.HandleGroupChannelSpeek(session, req);
}

int SocialRelationManager::HandleQueryPlayerRelationRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryPlayerRelationRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleQueryPlayerRelationRequest(session, req);
}

int SocialRelationManager::HandleQueryPlayerInfoRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryPlayerInfoRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleQueryPlayerInfoRequest(session, req);
}

int SocialRelationManager::HandleQueryGuildRankRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryUnionRankRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleQueryGuildRankRequest(session, req);
}

int SocialRelationManager::HandleSearchGuildRankRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::SearchUnionRankRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleSearchGuildRankRequest(session, req);
}

int SocialRelationManager::HandleQueryRaceMasterRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryRaceMasterRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleQueryRaceMasterRequest(session, req);
}

int SocialRelationManager::HandleQueryGuildMembersRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryGuildMembersRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleQueryGuildMembersRequest(session, req);
}

int SocialRelationManager::HandleAddFriendlyDegreeRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::AddFriendlyDegree req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleAddFriendlyDegreeRequest(session, req);
}

int SocialRelationManager::HandleAddTweetRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::AddTweet req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleAddTweetRequest(session, req);
}

int SocialRelationManager::HandleQueryTweetsRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryFriendTweetsRequest req;
	if (!MessageParse(message, len , req))
		return -1;

	return friendManager_.HandleQueryTweetsRequest(session, req);
}

int SocialRelationManager::HandleQueryFriendsListRequest(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryFriendsListRequest req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleQueryFriendsListRequest(session, req);
}

int SocialRelationManager::HandleNotifyExpAdded(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::NotifyExpAdded req;
	if (!MessageParse(message, len , req))
		return -1;

	return teamManager_.HandleNotifyExpAdded(session, req);
}

int SocialRelationManager::HandleQueryExpNeededWhenLvlUpResult(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::QueryExpNeededWhenLvlUpResult result;
	if (!MessageParse(message, len, result))
		return -1;

	return friendManager_.HandleQueryExpNeededWhenLvlUpResult(session, result);
}

int SocialRelationManager::HandleTweetFlagModify(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::ModifyTweetReceiveFlag req;
	if (!MessageParse(message, len, req))
		return -1;

	return friendManager_.HandleTweetFlagModify(session, req);
}

int SocialRelationManager::HandleQueryPlayerGuild(tcp_session session, char * message, const unsigned len)
{
	RelationServiceNS::GRQueryNickUnion req;
	if (!MessageParse(message, len, req))
		return -1;

	return guildManager_.HandleQueryPlayerGuild(session, req);
}


__SERVICE_SPACE_END_NS__
