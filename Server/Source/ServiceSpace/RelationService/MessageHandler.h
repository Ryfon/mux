﻿/********************************************************************
	created:	2008/07/07
	created:	7:7:2008   17:19
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\RelationService\MessageHandler.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\RelationService
	file base:	MessageHandler
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_RELATION_SERVICE_MESSAGE_HANDLER_H__
#define __SERVICE_SPACE_RELATION_SERVICE_MESSAGE_HANDLER_H__

//#define ACE_NTRACE 0

#include "../ServiceSpaceLib/TimerHandler.h"
#include "../ServiceSpaceLib/utilib/smart_ptr.h"
#include "../ServiceSpaceLib/utilib/obj_pool.h"


#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/SessionHandler.h"
#include "../ServiceSpaceLib/MessageHandler.h"
#include "../ServiceSpaceLib/utilib/pool_lock.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "../ServiceSpaceLib/TcpSession.h"
#include "../ServiceSpaceLib/ObjectMessageBlock.h"
#include "../ServiceSpaceLib/TcpService.h"
#include "../ServiceSpaceLib/utilib/split.h"

#include "../ServiceSpaceDBLib/db2o.h"
#include "../ServiceSpaceDBLib/DBQueryObject.h"

//#include <strstream>
#include <ace/Synch.h>
#include <ace/Thread.h>
#include <ace/Token.h>
#include <cstdlib>

#include "RelationServiceProtocol.h"
#include <algorithm>
#include <map>
#include <list>
using RelationServiceNS::RelationServiceProtocol;

__SERVICE_SPACE_BEGIN_NS__

/**
* 缓存中是否包含一个有效包，dwPkgLen为包长，包格式:
* 1-2			3		4-5		不定长度
* 包长度	 包类型	   命令字	协议结构内容
* wPkgLen	 byPkgType	wCmd	pContent
*/	

#define __TRY_BENGIN try {
#define __TRY_END  } catch (std::exception& e) { SS_ERROR(ACE_TEXT(" 在 %s 文件 的 %d 行发生异常！err = %s \n"),__FILE__,__LINE__,(e.what() ? e.what() : "无原因" )) ; }

class ACE_Svc_Export RelationServiceMessageHandler
	: public MessageHandler<RelationServiceMessageHandler>
{
	typedef MessageHandler<RelationServiceMessageHandler> base;
public:
	typedef std::map<unsigned, tcp_session> gate_map;

	virtual int init(int argc, ACE_TCHAR * argv[]);

	virtual int open(void *args = 0);

	static ACE_Message_Block * build_result_message(void * data, int msgId);

	static int  send_result_message(tcp_session & session, void * data, int msgId);
	static tcp_session get_session(unsigned short gate_id) ;
	static unsigned get_gate_id(tcp_session & session);

protected:
	int handle_task_message(ACE_Message_Block * message);

	int handle_message(tcp_session & session, char * message, unsigned len);

	int handle_report_gate_id(tcp_session & session, const char * message, unsigned len);

	int handle_gate_session_closed(tcp_session & session);

	virtual int get_message_id(const char * message);
	
	ACE_INET_Addr listen_addr_;
	typedef utilib::pool_lock lock_type;
	static lock_type gate_svr_lock;

public:
	static gate_map gates;
	static RelationServiceProtocol protocol;
};

__SERVICE_SPACE_END_NS__

#endif
