﻿#ifndef __SERVICE_SPACE_PLAYER_H__
#define __SERVICE_SPACE_PLAYER_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/utilib/basic_text_ostream.h"
#include "RelationServiceProtocol.h"
#include <map>
#include <list>
#include <string>
#include <vector>

__SERVICE_SPACE_BEGIN_NS__

class Player;

utilib::basic_text_ostream & operator<< (utilib::basic_text_ostream & s, const Player & player);
utilib::basic_text_istream & operator>> (utilib::basic_text_istream & s, Player & player);

void Eval(RelationServiceNS::FriendInfo & info, const Player & whoseFriend, const Player & _friend);
void Eval(RelationServiceNS::RGFriendInfoUpdate & info, const Player & whoseFriend, const Player & _friend);
void Eval(RelationServiceNS::GRInfoUpdate & info, const Player & player);

#define default_group_name "我的好友"

class Player
{
public:
	enum { INVALID_GUILD_ID = -1 };
	enum { LINE_OFFLINE = 0, LINE_ONLINE, LINE_IRCLINE };
	enum { SEX_MALE = 0, SEX_FEMALE };
	enum { INVALID_TEAM_ID = -1 };
	enum { INVALID_CHATGROUP_ID = -1 };

	struct GuildInfo
	{
		unsigned guildId;
		unsigned joinTime;
		unsigned exitTime;
		unsigned guildPos;
		std::string guildTitle;
		bool forbid;

		GuildInfo(): guildId(INVALID_GUILD_ID), joinTime(0), exitTime(0), guildPos(0), forbid(false)
		{ }
	};

	struct TmpId
	{
		unsigned gateId;
		unsigned playerId;

		TmpId(const unsigned gid = 0, const unsigned pid = 0):gateId(gid), playerId(pid) { }

		inline const bool operator==(const TmpId & r)const { return gateId == r.gateId && playerId == r.playerId; } 
	};

	struct TeamFlag
	{
		unsigned teamId;
		unsigned playerFlag;
		unsigned teamFlag;

		TeamFlag() : teamId(0), playerFlag(0), teamFlag(0) { }
	};

	struct FriendInfo
	{
		unsigned uiid;
		unsigned degree;
		unsigned readTimeLine;

		FriendInfo() : uiid(0), degree(0), readTimeLine(0) { }
	};

	typedef std::vector<FriendInfo> FriendList;

	struct GroupInfo
	{
		std::string name;
		unsigned groupId;
		unsigned createdTime;
		FriendList lst;

		GroupInfo():groupId(0), createdTime(0) { }

		const bool operator<(const GroupInfo & r)const
		{
			return name < r.name;
		}
	};

	struct EnemyInfo
	{
		unsigned uiid;
		bool locked;
		unsigned short mapId;
		unsigned short posX;
		unsigned short posY;
		bool isKilled;
		unsigned time;

		EnemyInfo(): uiid(0), locked(false) { }
	};

	struct Tweet
	{
		unsigned short type;
		unsigned createdTime;
		std::string msg;

		Tweet() : type(-1) { }

		void set(RelationServiceNS::Tweet & tweet);
		void set(unsigned type, unsigned createdTime, void * data, size_t len);
		void get(RelationServiceNS::Tweet & tweet)const;
	};
	
	typedef std::vector<GroupInfo> FriendGroupList;
	typedef std::vector<unsigned> BlackList;
	typedef std::vector<EnemyInfo> EnemyList;
	typedef std::vector<unsigned> WhoseFriendList;
	typedef std::vector<unsigned> WhoseBlackList;
	typedef std::vector<unsigned> WhoseEnemyList;
	typedef std::vector<Tweet> TweetsList;
	
	explicit Player(const unsigned uiid = 0);

	~Player();

	const std::string & AccountId()const;
	const unsigned		Uiid()const;
	const std::string & Name()const;
	const int			Race()const;
	const int			Sex()const;
	const unsigned		Level()const;
	const int			Icon()const;
	const unsigned		GateId()const;
	const unsigned		PlayerId()const;
	const unsigned		MapId()const;
	const GuildInfo	&	GetGuildInfo()const;
	const int			LineState()const;
	const int			Job()const;
	const unsigned      Fere()const;
	const unsigned      GetUplineTime()const;
	const unsigned		GetOfflineTime()const;
	const unsigned		GetAccUplineDay()const;
	const FriendGroupList &	GetFriendGroupList()const;
	const FriendList *  GetFriendList(const std::string & name)const;
	const FriendList *  GetFriendList(const unsigned groupId)const;
	const BlackList	 &	GetBlackList()const;
	const EnemyList  &	GetEnemyList()const;
	const unsigned		GetFriendCount(const std::string & group)const;
	const unsigned		GetFriendCount()const;
	const unsigned		GetBlackCount()const;
	const unsigned		GetEnemyCount()const;
	const unsigned		GetFriendGroupId(const unsigned uiid)const;
	const std::string & GetFriendGroupName(const unsigned groupId)const;
	const unsigned		RestGroupMsgCount()const;
	const WhoseFriendList &  GetWhoseFriendList()const;
	const WhoseBlackList  &  GetWhoseBlackList()const;
	const WhoseEnemyList  &  GetWhoseEnemyList()const;
	const unsigned		GetMaxFriendGroupId()const;
	const unsigned		GetBeginCalGroupMsgCountTime()const;
	const unsigned		GetTeamId()const;
	const unsigned		GetChatGroupId()const;
	const FriendInfo *  GetFriendInfo(const unsigned uiid)const;
	FriendInfo * GetFriendInfo(const unsigned uiid);
	const TweetsList &  GetTweets()const;
	const unsigned GetTweetFlag()const;
	
	void AccountId(const std::string & id);
	void Uiid(const unsigned uiid);
	void Name(const std::string & name);
	void Race(const int race);
	void Sex(const int sex);
	void Level(const unsigned Lvl);
	void Icon(const int icon);
	void GateId(const unsigned gateId);
	void PlayerId(const unsigned playerId);
	void MapId(const unsigned mapId);
	void SetGuildInfo(const GuildInfo & info);
	void Job(const int b);
	void Fere(const unsigned id);
	void SetUplineTime(const unsigned t);
	void SetOfflineTime(const unsigned t);
	void ResetAccUplineTime(const unsigned t);
	void LineState(const int state);
	const unsigned AddFriendGroup(const std::string & group);
	void RemoveFriendGroup(const unsigned groupId);
	void RenameFriendGroup(const unsigned groupId, const std::string & name);
	void AddFriend(const std::string & group, Player &);
	void AddFriend(const std::string & group, const unsigned uiid, unsigned degree = 0);
	void RemoveFriend(Player &);
	void RemoveFriend(const unsigned uiid);
	void TransferFriend(const unsigned uiid, const unsigned oldGroupId, const unsigned newGroupId);
	void AddBlack(Player &);
	void AddBlack(const unsigned uiid);
	void RemoveBlack(Player &);
	void RemoveBlack(const unsigned uiid);
	void AddEnemy(Player &, unsigned short mapId, unsigned short posx, unsigned short posy, bool bKilled);
	void AddEnemy(const unsigned uiid, unsigned short mapId, unsigned short posx, unsigned short posy, bool bKilled);
	void RemoveEnemy(Player &);
	void RemoveEnemy(const unsigned uiid);
	void AddWhoseFriend(const unsigned id);
	void AddWhoseBlack(const unsigned id);
	void AddWhoseEnemy(const unsigned id);
	void RemoveWhoseFriend(const unsigned id);
	void RemoveWhoseBlack(const unsigned id);
	void RemoveWhoseEnemy(const unsigned id);
	void SetRestGroupsMsgCount(const unsigned n, const time_t t);
	void LockEnemy(const unsigned id);
	void UnlockEnemy(const unsigned id);
	const unsigned RemoveFirstUnlockedEnemy();
	void SetTeamId(const unsigned id);
	void SetChatGroupId(const unsigned id);
	void AddTweet(const Tweet & tt);
	void AddFriendLyDegree(const unsigned friendUiid, unsigned degree);
	void SetFriendLyDegree(const unsigned friendUiid, unsigned degree);
	void SetTweetFlag(const unsigned flag);

	const bool IsFriend(const Player &)const;
	const bool IsFriend(const unsigned id)const;
	const bool IsBlack(const Player &)const;
	const bool IsBlack(const unsigned id)const;
	const bool IsEnemy(const Player &)const;
	const bool IsEnemy(const unsigned id)const;
	const bool IsLockedEnemy(const unsigned id)const;
	const EnemyInfo * GetEnemyInfo(const unsigned id)const;

	TeamFlag & GetTeamFlag();
	const TeamFlag & GetTeamFlag()const;

	void AddUpLineTweet();
	void AddOffLineTweet();
	void AddKillBossTweet(unsigned bossId);
	void AddGetTitleTweet(const std::string & title);
	void AddGetAchTweet(const std::string & title);
	void AddUpgradeTweet(unsigned lvl);
	void AddDeadTweet();
	void AddKillPlayerTweet(const std::string & player);
	void AddNewFriendAddedTweet(const std::string & player);
	void AddJoinGuildTweet(const std::string & guild);
	void AddLeaveGuildTweet(const std::string & guild);

	friend utilib::basic_text_ostream & operator<< (utilib::basic_text_ostream & s, const Player & player);
	friend utilib::basic_text_istream & operator>> (utilib::basic_text_istream & s, Player & player);

private:
	unsigned uiid_;
	std::string account_;
	std::string name_;
	int race_;
	int sex_;
	unsigned level_;
	int icon_;
	unsigned gateId_;
	unsigned playerId_;
	unsigned mapId_;
	int lineState_;
	int job_;
	unsigned fere_;
	unsigned uplineTime_;
	unsigned offlineTime_;
	unsigned accUplineTimeDay_;
	unsigned beginFriendGroupMsgCountTime_;
	unsigned restFriendGroupMsgCount_;
	unsigned teamId_;
	unsigned chatGroupId_;
	GuildInfo guildInfo_;
	FriendGroupList friends_;
	BlackList blacks_;
	EnemyList enemys_;
	WhoseFriendList whoseFriends_;
	WhoseBlackList whoseBlacks_;
	WhoseEnemyList whoseEnemys_;

	TeamFlag teamFlag_;
	TweetsList tweets_;
	unsigned tweetFlag_;
};

__SERVICE_SPACE_END_NS__

#endif