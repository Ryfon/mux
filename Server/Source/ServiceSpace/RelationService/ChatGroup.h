﻿#ifndef __SERVICE_SPACE_CHATGROUP_H__
#define __SERVICE_SPACE_CHATGROUP_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "Player.h"
#include <map>
#include <list>
#include <string>
#include <vector>

__SERVICE_SPACE_BEGIN_NS__


class ChatGroup
{
public:
	typedef std::vector<Player::TmpId> MemberList;

	ChatGroup();

	~ChatGroup();

	const unsigned GroupId()const;
	const Player::TmpId & GroupOwner()const;
	const std::string & GetChatPasswrd()const;
	const MemberList & GetMemberList()const;

	void GroupId(const unsigned id);
	void GroupOwner(const Player::TmpId & id);
	void SetChatPasswrd(const std::string & passrd);
	void AddMember(const Player::TmpId & id);
	void RemoveMember(const Player::TmpId & id);

	const bool IsMember(const Player::TmpId & id)const;

private:
	unsigned id_;
	Player::TmpId owner_;
	std::string passwrd_;
	MemberList members_;
};

__SERVICE_SPACE_END_NS__

#endif