﻿#define ACE_NTRACE 0
#include "TeamManager.h"
#include "FriendManager.h"
#include "SocialRelationManager.h"
#include <algorithm>

__SERVICE_SPACE_BEGIN_NS__

enum {
	TEAM_LOG_CREATE = 0,
	TEAM_LOG_DESTROY = 1,
	TEAM_LOG_ADD = 2,
	TEAM_LOG_LEAVE = 3,
	TEAM_LOG_NEWCAPTION = 4
};

void Eval(RelationServiceNS::TeamInd & ind, const unsigned id)
{
	sprintf(ind.teamInd, "%u", id);
	ind.teamIndLen = strlen(ind.teamInd);
}

void Eval(unsigned & id, const RelationServiceNS::TeamInd & ind)
{
	id = 0;
	char buf[sizeof(ind.teamInd) + 1];
	memset(buf, 0, sizeof(buf));
	memcpy(buf, ind.teamInd, ind.teamIndLen < sizeof(ind.teamInd) ? ind.teamIndLen : sizeof(ind.teamInd));
	sscanf(buf, "%u", &id);
}

void Eval(RelationServiceNS::Member & l, const Player & r, const unsigned seq)
{
	l.avatarId = r.Icon();
	l.gateId = r.GateId();
	l.mapId = r.MapId();
	l.memberInd.memberIndLen = StringToBuf(r.AccountId(), l.memberInd.memberInd, sizeof(l.memberInd.memberInd));
	l.memberJob = r.Job();
	l.memberLevel = r.Level();
	l.memberName.nameLen = StringToBuf(r.Name(), l.memberName.memberName, sizeof(l.memberName.memberName));
	l.memberSeq = seq;
	l.memberSex = r.Sex();
	l.playerId = r.PlayerId();
}

const unsigned Eval(RelationServiceNS::Member (&l)[RelationServiceNS::MAX_TEAM_MEMBERS_COUNT], const Team::MemberList & r)
{
	unsigned count = 0;
	for(Team::MemberList::const_iterator it = r.begin(); it != r.end() && count < RelationServiceNS::MAX_TEAM_MEMBERS_COUNT; ++it, ++count)
	{
		Player * player =  SocialRelationManager::instance()->GetPlayerByTmpId(it->tmpId);
		if (!player)
			continue;

		Eval(l[count], *player, count);
	}

	return count;
}

void Eval(RelationServiceNS::Team & l, const Team & r)
{
	l.captainGateId = r.TeamCaption().gateId;
	l.captainPlayerId = r.TeamCaption().playerId;
	l.expSharedMode = r.ExpSharedMode();
	l.goodSharedMode = r.GoodSharedMode();
	l.rollItemLevel = r.RollItemLevel();
	Eval(l.teamInd, r.TeamId());
	l.teamMembers.membersSize = Eval(l.teamMembers.members, r.GetMemberList());
}

TeamManager::TeamManager()
: maxTeamId_(0)
{
}

TeamManager::~TeamManager()
{
}

int TeamManager::HandleCreateTeam(tcp_session session, const RelationServiceNS::CreateTeamRequest & req)
{
	ACE_TRACE("TeamManager::HandleCreateTeam");

	RelationServiceNS::CreateTeamResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;

	if (req.team.teamMembers.membersSize != 2)
	{
		SS_DEBUG("team member size is not 2\n");
		return -1;
	}

	Player * caption = SocialRelationManager::instance()->GetPlayerByTmpId(
		req.team.teamMembers.members[0].gateId, req.team.teamMembers.members[0].playerId);
	
	Player * target = 0;
	if (!caption)
	{
		SS_DEBUG("cannot find team caption\n");
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (caption->GetTeamId() != Player::INVALID_TEAM_ID)
	{
		SS_DEBUG("caption is in other team\n");
		result.returnCode = RelationServiceNS::R_G_RESULT_MEMBER_IN_OTHER_TEAM;
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByTmpId(
		req.team.teamMembers.members[1].gateId, req.team.teamMembers.members[1].playerId)) == 0)
	{
		SS_DEBUG("cannot find team player\n");
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else if (target->GetTeamId() != Player::INVALID_TEAM_ID)
	{
		SS_DEBUG("team player is in other team\n");
		result.returnCode = RelationServiceNS::R_G_RESULT_MEMBER_IN_OTHER_TEAM;
	}
	/*else if (caption->IsEnemy(*target))
	{
		SS_DEBUG("cannot create team with enemy\n");
		return -1;
	}*/
	else if (caption->Uiid() == target->Uiid())
	{
		SS_DEBUG("cannot create team with self\n");
		return -1;
	}
	else
	{
		std::string account;
		BuffToString(req.team.teamMembers.members[0].memberInd.memberInd, req.team.teamMembers.members[0].memberInd.memberIndLen, 
			sizeof(req.team.teamMembers.members[0].memberInd.memberInd), account);
		caption->AccountId(account);
		account.clear();
		BuffToString(req.team.teamMembers.members[1].memberInd.memberInd, req.team.teamMembers.members[1].memberInd.memberIndLen, 
			sizeof(req.team.teamMembers.members[1].memberInd.memberInd), account);
		target->AccountId(account);

		Team team;
		team.SetTeamId(++maxTeamId_);

		Player::TmpId captionTmpId;
		captionTmpId.gateId = caption->GateId();
		captionTmpId.playerId = caption->PlayerId();
		team.SetTeamCaption(captionTmpId);

		team.SetGoodSharedMode(req.team.goodSharedMode);
		team.SetExpSharedMode(req.team.expSharedMode);
		team.SetRollItemLevel(req.team.rollItemLevel);

		Player::TmpId targetTmpId;
		targetTmpId.gateId = target->GateId();
		targetTmpId.playerId = target->PlayerId();
		team.AddMember(captionTmpId);
		team.AddMember(targetTmpId);

		teams_[team.TeamId()] = team;
		Eval(result.team, team);

		caption->SetTeamId(team.TeamId());
		target->SetTeamId(team.TeamId());

		SendTeamCreatedNotify(team);

		caption->GetTeamFlag().teamId = caption->GetTeamId();
		
		target->GetTeamFlag().teamId = caption->GetTeamFlag().teamId;
		target->GetTeamFlag().teamFlag = caption->GetTeamFlag().teamFlag;
		target->GetTeamFlag().playerFlag = caption->GetTeamFlag().teamFlag;

		SendTeamFlag(*caption);
		SendTeamFlag(*target);

		SendTeamLog(caption->Uiid(), caption->Level(), team.TeamId(), TEAM_LOG_CREATE);
		SendTeamLog(target->Uiid(), target->Level(), team.TeamId(), TEAM_LOG_ADD);

		CalTeamGain(team);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::CreateTeamResult::wCmd);

	return 0;
}

int TeamManager::HandleDestroyTeam(tcp_session session, const RelationServiceNS::DestroyTeamRequest & req, bool notifyFlag)
{
	ACE_TRACE("TeamManager::HandleDestroyTeam");
	
	RelationServiceNS::DestroyTeamResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.teamInd = req.teamInd;

	unsigned teamId;
	Eval(teamId, req.teamInd);

	TeamMap::iterator teamIt = teams_.find(teamId);
	if (teamIt == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else
	{
		for(Team::MemberList::const_iterator it = teamIt->second.GetMemberList().begin(); it != teamIt->second.GetMemberList().end(); ++it)
		{
			Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(it->tmpId);
			if (!player)
				continue;

			player->SetTeamId(Player::INVALID_TEAM_ID);

			SendTeamLog(player->Uiid(), player->Level(), teamId, TEAM_LOG_LEAVE);
		}

		SendTeamDestroyedNotify(teamIt->second);

		teams_.erase(teamId);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::DestroyTeamResult::wCmd);

	return 0;
}

int TeamManager::HandleAddTeamMember(tcp_session session, const RelationServiceNS::AddTeamMemberRequest & req)
{
	ACE_TRACE("TeamManager::HandleAddTeamMember");

	RelationServiceNS::AddTeamMemberResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = req.gateId;
	result.palyerId = req.palyerId;
	result.teamInd = req.teamInd;
	result.member = req.member;

	unsigned teamId;
	Eval(teamId, req.teamInd);

	Player * target = 0;
	TeamMap::iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (it->second.GetMemberList().size() >= RelationServiceNS::MAX_TEAM_MEMBERS_COUNT)
	{
		SS_ERROR("team[%u] is full\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_GROUP_FULL;
	}
	else if (it->second.IsMember(Player::TmpId(req.member.gateId, req.member.playerId)))
	{
		SS_ERROR("player is in team[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_REPEAT;
	}
	else if (!(Player::TmpId(req.gateId, req.palyerId) == it->second.TeamCaption()))
	{
		SS_ERROR("only team caption can add member\n");
		return -1;
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByTmpId(req.member.gateId, req.member.playerId)) == 0)
	{
		SS_ERROR("cannot find player by playerid\n");
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_MEMBER;
	}
	else if (target->GetTeamId() != Player::INVALID_TEAM_ID)
	{
		SS_ERROR("player[%s] is in other team[%u]\n", target->Name().c_str(), target->GetTeamId());
		result.returnCode = RelationServiceNS::R_G_RESULT_MEMBER_IN_OTHER_TEAM;
	}
	else
	{
		std::string account;
		BuffToString(req.member.memberInd.memberInd, req.member.memberInd.memberIndLen, sizeof(req.member.memberInd.memberInd), account);
		target->AccountId(account);

		it->second.AddMember(Player::TmpId(req.member.gateId, req.member.playerId));

		target->SetTeamId(it->second.TeamId());
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::AddTeamMemberResult::wCmd);

	if (result.returnCode == RelationServiceNS::R_G_RESULT_SUCCEED)
	{
		SendTeamMemberAddedNotify(*target, it->second, req.flag, req.member.gateId, req.member.playerId);

		Player * caption = SocialRelationManager::instance()->GetPlayerByTmpId(it->second.TeamCaption());
		if (caption)
		{
			target->GetTeamFlag().teamId = caption->GetTeamFlag().teamId;
			target->GetTeamFlag().teamFlag = caption->GetTeamFlag().teamFlag;
			target->GetTeamFlag().playerFlag = caption->GetTeamFlag().teamFlag;
		}
		SendTeamFlag(*target);

		SendTeamLog(target->Uiid(), target->Level(), it->second.TeamId(), TEAM_LOG_ADD);

		CalTeamGain(it->second);
	}

	return 0;
}

int TeamManager::HandleRemoveTeamMember(tcp_session session, const RelationServiceNS::RemoveTeamMemberRequest & req)
{
	ACE_TRACE("TeamManager::HandleRemoveTeamMember");

	RelationServiceNS::RemoveTeamMemberResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = req.gateId;
	result.playerId = req.palyerId;
	result.removedMode = req.removedMode;

	unsigned teamId;
	Eval(teamId, req.teamInd);

	Player * player = 0;
	TeamMap::iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (!it->second.IsMember(Player::TmpId(req.gateId, req.palyerId)))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_MEMBER;
	}
	else if ((player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.palyerId)) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else
	{
		Player::TmpId captionId = it->second.TeamCaption();
		Player::TmpId tmpId(req.gateId, req.palyerId);
		
		SendTeamMemberRemovedNotify(*player, it->second, req.removedMode);

		bool destroy_team = it->second.GetMemberList().size() <= 2;
		if (it->second.GetMemberList().size() > 2)
		{
			it->second.RemoveMember(tmpId);

			SendTeamLog(player->Uiid(), player->Level(), teamId, TEAM_LOG_LEAVE);
		}

		player->SetTeamId(Player::INVALID_TEAM_ID);
		
		if (it->second.GetMemberList().size() <= 2 && destroy_team)
		{
			RelationServiceNS::DestroyTeamRequest req2;
			memset(&req2, 0, sizeof(req2));
			req2.teamInd = req.teamInd;
			HandleDestroyTeam(session, req2, false);

			/*player->GetTeamFlag().teamId = ++maxTeamId_;
			player->GetTeamFlag().playerFlag = player->GetTeamFlag().teamId;

			SendTeamFlag(*player);*/
		}
		else if (tmpId == captionId)
		{
			RelationServiceNS::ModifyTeamCaptainRequest req2;
			memset(&req2, 0, sizeof(req2));
			req2.teamInd = req.teamInd;
			req2.gateId = it->second.GetMemberList()[0].tmpId.gateId;
			req2.playerId = it->second.GetMemberList()[0].tmpId.playerId;
			HandleModifyTeamCaption(session, req2);

			player->GetTeamFlag().teamId = ++maxTeamId_;
			player->GetTeamFlag().playerFlag = player->GetTeamFlag().teamId;
			player->GetTeamFlag().teamFlag = player->GetTeamFlag().teamId;

			SendTeamFlag(*player);
			CalTeamGain(it->second);
		}
		else
		{
			player->GetTeamFlag().teamId = ++maxTeamId_;
			player->GetTeamFlag().playerFlag = player->GetTeamFlag().teamId;
			player->GetTeamFlag().teamFlag = player->GetTeamFlag().teamId;

			SendTeamFlag(*player);
			CalTeamGain(it->second);
		}

		ResetPlayerTeamGain(*player);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RemoveTeamMemberResult::wCmd);

	return 0;
}

int TeamManager::HandleQueryTeam(tcp_session session, const RelationServiceNS::QueryTeamRequest & req)
{
	ACE_TRACE("TeamManager::HandleQueryTeam");

	RelationServiceNS::QueryTeamResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = req.gateId;
	result.playerId = req.playerId;
	
	unsigned teamId;
	Eval(teamId, req.teamInd);

	Player * player = 0;
	TeamMap::const_iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else
	{
		Eval(result.team, it->second);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryTeamResult::wCmd);

	return 0;
}

int TeamManager::HandleQueryTeamMember(tcp_session session, const RelationServiceNS::QueryTeamMemberRequest & req)
{
	ACE_TRACE("TeamManager::HandleQueryTeamMember");

	RelationServiceNS::QueryTeamMemberResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.teamInd = req.teamInd;

	unsigned teamId;
	Eval(teamId, req.teamInd);

	Player * player = 0;
	TeamMap::const_iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (!it->second.IsMember(Player::TmpId(req.gateId, req.playerId)))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_MEMBER;
	}
	else if ((player = SocialRelationManager::instance()->GetPlayerByTmpId(Player::TmpId(req.gateId, req.playerId))) == 0)
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_PLAYER;
	}
	else
	{
		Eval(result.member, *player, it->second.GetMemberSeq(Player::TmpId(req.gateId, req.playerId)));
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryTeamMemberResult::wCmd);
	
	return 0;
}

int TeamManager::HandleModifyTeamCaption(tcp_session session, const RelationServiceNS::ModifyTeamCaptainRequest & req)
{
	ACE_TRACE("TeamManager::HandleModifyTeamCaption");

	RelationServiceNS::ModifyTeamCaptainResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = req.gateId;
	result.playerId = req.playerId;
	result.teamInd = req.teamInd;

	unsigned teamId;
	Eval(teamId, req.teamInd);

	TeamMap::iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (it->second.TeamCaption() == Player::TmpId(req.gateId, req.playerId))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_REPEAT;
	}
	else if (!it->second.IsMember(Player::TmpId(req.gateId, req.playerId)))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_MEMBER;
	}
	else
	{
		Player::TmpId caption(req.gateId, req.playerId);
		it->second.SetTeamCaption(caption);

		SendTeamCaptionChangedNotify(it->second);

		Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(caption);
		if (player)
			SendTeamLog(player->Uiid(), player->Level(), teamId, TEAM_LOG_NEWCAPTION);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::ModifyTeamCaptainResult::wCmd);

	return 0;
}

int TeamManager::HandleModifyTeamExpMode(tcp_session session, const RelationServiceNS::ModifyExpSharedModeRequest & req)
{
	ACE_TRACE("TeamManager::HandleModifyTeamExpMode");

	RelationServiceNS::ModifyExpSharedModeResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.mode = req.mode;
	result.teamInd = req.teamInd;

	unsigned teamId;
	Eval(teamId, req.teamInd);
	
	TeamMap::iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else
	{
		it->second.SetExpSharedMode(req.mode);

		SendTeamExpSharedModeNotify(it->second);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::ModifyExpSharedModeResult::wCmd);
	
	return 0;
}

int TeamManager::HandleModifyTeamGoodMode(tcp_session session, const RelationServiceNS::ModifyGoodSharedModeRequest & req)
{
	ACE_TRACE("TeamManager::HandleModifyTeamGoodMode");

	RelationServiceNS::ModifyGoodSharedModeResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.mode = req.mode;
	result.teamInd = req.teamInd;

	unsigned teamId;
	Eval(teamId, req.teamInd);
	
	TeamMap::iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else
	{
		it->second.SetGoodSharedMode(req.mode);

		SendTeamGoodSharedModeNotify(it->second);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::ModifyGoodSharedModeResult::wCmd);
	
	return 0;
}

int TeamManager::HandleTeamChannelSpeek(tcp_session session, const RelationServiceNS::TeamBroadcastRequest & req)
{
	ACE_TRACE("TeamManager::HandleTeamChannelSpeek");

	RelationServiceNS::TeamBroadcastResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.fromGateId = req.fromGateId;
	result.fromPlayerId = req.fromPlayerId;
	result.teamInd = req.teamInd;

	unsigned teamId;
	Eval(teamId, req.teamInd);
	
	TeamMap::iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (!it->second.IsMember(Player::TmpId(req.fromGateId, req.fromPlayerId)))
	{
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_MEMBER;
	}
	else
	{
		SendTeamBoradcastNotify(req.broadcastData, req.dataType, Player::TmpId(req.fromGateId, req.fromPlayerId), it->second);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::TeamBroadcastResult::wCmd);

	return 0;
}

int TeamManager::HandleModifyTeamRollItemLevel(tcp_session session, const RelationServiceNS::ModifyTeamRollItemLevelRequest & req)
{
	ACE_TRACE("TeamManager::HandleModifyTeamRollItemLevel");

	RelationServiceNS::ModifyTeamRollItemLevelResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = req.gateId;
	result.playerId = req.playerId;
	result.teamInd = req.teamInd;

	unsigned teamId;
	Eval(teamId, req.teamInd);
	
	TeamMap::iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else if (!(Player::TmpId(req.gateId, req.playerId) == it->second.TeamCaption()))
	{
		return -1;
	}
	else
	{
		it->second.SetRollItemLevel(req.level);

		SendTeamRollItemLevelNotify(Player::TmpId(req.gateId, req.playerId), it->second);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::ModifyTeamRollItemLevelResult::wCmd);
	return 0;
}

int TeamManager::HandleQueryTeamMemberCount(tcp_session session, const RelationServiceNS::QueryTeamMembersCountRequest & req)
{
	ACE_TRACE("TeamManager::HandleQueryTeamMemberCount");

	RelationServiceNS::QueryTeamMembersCountResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.teamInd = req.teamInd;
	result.gateId = req.gateId;
	result.playerId = req.playerId;

	unsigned teamId;
	Eval(teamId, req.teamInd);

	TeamMap::const_iterator it = teams_.find(teamId);
	if (it == teams_.end())
	{
		SS_ERROR("cannot find team by id[%u]\n", teamId);
		result.returnCode = RelationServiceNS::R_G_RESULT_NO_GROUP;
	}
	else
	{
		result.count = it->second.GetMemberList().size();
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryTeamMembersCountResult::wCmd);
	
	return 0;
}

int TeamManager::HandleNotifyExpAdded(tcp_session session, const RelationServiceNS::NotifyExpAdded & req)
{
	ACE_TRACE("TeamManager::HandleNotifyExpAdded\n");

	// MapSrv在某个玩家杀死怪物时发送一次此信息

	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.playerId);
	if (!player)
		return -1;

	if (Player::INVALID_TEAM_ID != player->GetTeamId())
	{
		TeamMap::iterator it = teams_.find(player->GetTeamId());
		if (it != teams_.end())
		{
			Team & team = it->second;
			team.SetExp(team.GetExp() + req.exp);
			// TODO:
			if (team.GetExp() < 1)
				return 0;
			else
				team.SetExp(0);

			Team::MemberList::const_iterator it;
			for(it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
			{
				if (req.gateId == it->tmpId.gateId && req.playerId == it->tmpId.playerId)
					continue;

				Team::MemberList::const_iterator _it;
				for(_it = it; _it != team.GetMemberList().end(); ++_it)
				{
					Player * p = SocialRelationManager::instance()->GetPlayerByTmpId(_it->tmpId);
					if (!p)
						continue;

					if (!player->IsFriend(*p) || !p->IsFriend(*player))
						continue;

					player->AddFriendLyDegree(p->Uiid(), 100);
					p->AddFriendLyDegree(player->Uiid(), 100);

					RelationServiceNS::RGFriendInfoUpdate info;

					memset(&info, 0, sizeof(info));
					Eval(info, *p, *player);

					info.player.GateSrvID = p->GateId();
					info.player.PPlayer = p->PlayerId();

					tcp_session s = RelationServiceMessageHandler::get_session(p->GateId());
					RelationServiceMessageHandler::send_result_message(s, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);

					memset(&info, 0, sizeof(info));
					Eval(info, *player, *p);

					info.player.GateSrvID = player->GateId();
					info.player.PPlayer = player->PlayerId();

					s = RelationServiceMessageHandler::get_session(player->GateId());
					RelationServiceMessageHandler::send_result_message(s, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);
				}
			}
		}
	}

	return 0;
}

int TeamManager::HandlePlayerUpline(Player & player)
{
	ACE_TRACE("TeamManager::HandlePlayerUpline");

	player.GetTeamFlag().teamId = ++maxTeamId_;
	player.GetTeamFlag().playerFlag = player.GetTeamFlag().teamId;
	player.GetTeamFlag().teamFlag = player.GetTeamFlag().playerFlag;
	
	SendTeamFlag(player);
	return 0;
}

int TeamManager::HandlePlayerOffline(Player & player)
{
	ACE_TRACE("TeamManager::HandlePlayerOffline");

	if (player.GetTeamId() == Player::INVALID_TEAM_ID)
		return 0;

	TeamMap::iterator it = teams_.find(player.GetTeamId());
	if (it == teams_.end())
	{
		player.SetTeamId(Player::INVALID_TEAM_ID);
		return -1;
	}

	if (Player::TmpId(player.GateId(), player.PlayerId()) == it->second.TeamCaption())
	{
		RelationServiceNS::DestroyTeamRequest req;
		memset(&req, 0, sizeof(req));
		req.requestSeq = 0;
		Eval(req.teamInd, it->second.TeamId());

		return HandleDestroyTeam(RelationServiceMessageHandler::get_session(player.GateId()), req);
	}
	
	RelationServiceNS::RemoveTeamMemberRequest req;
	memset(&req, 0, sizeof(req));
	Eval(req.teamInd, it->second.TeamId());
	req.gateId = player.GateId();
	req.palyerId = player.PlayerId();

	HandleRemoveTeamMember(RelationServiceMessageHandler::get_session(player.GateId()), req);

	player.SetTeamId(Player::INVALID_TEAM_ID);

	return 0;
}

void TeamManager::SendTeamCreatedNotify(const Team & team)
{
	ACE_TRACE("TeamManager::SendTeamCreatedNotify");

	RelationServiceNS::TeamCreateedNotify notify;
	memset(&notify, 0, sizeof(notify));
	Eval(notify.team, team);

	for (Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		notify.gateId = it->tmpId.gateId;
		notify.playerId = it->tmpId.playerId;

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::TeamCreateedNotify::wCmd);
	}

}

void TeamManager::SendTeamDestroyedNotify(const Team & team)
{
	ACE_TRACE("TeamManager::SendTeamDestroyedNotify");

	RelationServiceNS::TeamDestroyedNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.causeCode = 0;
	Eval(notify.teamInd, team.TeamId());

	for(Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(it->tmpId);
		if (!player)
			continue;

		notify.gateId = it->tmpId.gateId;
		notify.playerId = it->tmpId.playerId;

		notify.memberInd.memberIndLen = StringToBuf(player->AccountId(), notify.memberInd.memberInd, sizeof(notify.memberInd.memberInd));

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::TeamDestroyedNotify::wCmd);

		player->GetTeamFlag().teamId = ++maxTeamId_;
		if (team.TeamCaption().gateId != player->GateId() || team.TeamCaption().playerId != player->PlayerId())
			player->GetTeamFlag().teamFlag = player->GetTeamFlag().teamId;
		else
			SendTeamLog(player->Uiid(), player->Level(), team.TeamId(), TEAM_LOG_DESTROY);
		player->GetTeamFlag().playerFlag = player->GetTeamFlag().teamId;
		SendTeamFlag(*player);
	}

}

void TeamManager::SendTeamMemberAddedNotify(const Player & player, const Team & team, const unsigned flag, const unsigned gateId, const unsigned playerId)
{
	ACE_TRACE("TeamManager::SendTeamMemberAddedNotify");

	RelationServiceNS::AddTeamMemberNotify notify;
	memset(&notify, 0, sizeof(notify));
	Eval(notify.teamInd, team.TeamId());
	Eval(notify.team, team);
	notify.flag = flag;
	notify.gateIdAdded = gateId;
	notify.playerIdAdded = playerId;

	for(Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(it->tmpId);
		if (!player)
			continue;

		notify.gateId = it->tmpId.gateId;
		notify.playerId = it->tmpId.playerId;

		notify.memberInd.memberIndLen = StringToBuf(player->AccountId(), notify.memberInd.memberInd, sizeof(notify.memberInd.memberInd));
		
		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::AddTeamMemberNotify::wCmd);
	}
}

void TeamManager::SendTeamMemberRemovedNotify(const Player & player, const Team & team, const unsigned mode)
{
	ACE_TRACE("TeamManager::SendTeamMemberRemovedNotify");

	RelationServiceNS::RemoveTeamMemberNotify notify;
	memset(&notify, 0, sizeof(notify));
	Eval(notify.teamInd, team.TeamId());
	Eval(notify.removedMember, player, team.GetMemberSeq(Player::TmpId(player.GateId(), player.PlayerId())));
	notify.removeMode = mode;

	for(Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByTmpId(it->tmpId);
		if (!p)
			continue;

		notify.gateId = it->tmpId.gateId;
		notify.playerId = it->tmpId.playerId;

		notify.memberInd.memberIndLen = StringToBuf(p->AccountId(), notify.memberInd.memberInd, sizeof(notify.memberInd.memberInd));

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::RemoveTeamMemberNotify::wCmd);
	}
}

void TeamManager::SendTeamBoradcastNotify(const RelationServiceNS::BroadcastData & data, const unsigned type, const Player::TmpId & player, const Team & team)
{
	ACE_TRACE("TeamManager::SendTeamBoradcastNotify");

	RelationServiceNS::TeamBroadcastNotify notify;
	memset(&notify, 0, sizeof(notify));
	Eval(notify.teamInd, team.TeamId());
	notify.dataType = type;
	notify.broadcastData = data;
	notify.fromGateId = player.gateId;
	notify.fromPlayerId = player.playerId;

	for(Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByTmpId(it->tmpId);
		if (!p)
			continue;

		notify.toGateId = it->tmpId.gateId;
		notify.toPlayerId = it->tmpId.playerId;
		notify.toMemberInd.memberIndLen = StringToBuf(p->AccountId(), notify.toMemberInd.memberInd, sizeof(notify.toMemberInd.memberInd));

		tcp_session session = RelationServiceMessageHandler::get_session(notify.toGateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::TeamBroadcastNotify::wCmd);
	}
}

void TeamManager::SendTeamCaptionChangedNotify(const Team & team)
{
	ACE_TRACE("TeamManager::SendTeamCaptionChangedNotify");

	RelationServiceNS::ModifyTeamCaptainNotify notify;
	memset(&notify, 0, sizeof(notify));
	Eval(notify.teamInd, team.TeamId());
	notify.captainGateId = team.TeamCaption().gateId;
	notify.captainPlayerId = team.TeamCaption().playerId;
	
	for(Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		notify.gateId = it->tmpId.gateId;
		notify.playerId = it->tmpId.playerId;

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::ModifyTeamCaptainNotify::wCmd);
	}
}

void TeamManager::SendTeamGoodSharedModeNotify(const Team & team)
{
	ACE_TRACE("TeamManager::SendTeamGoodSharedModeNotify");

	RelationServiceNS::ModifyGoodSharedModeNotify notify; 
	memset(&notify, 0, sizeof(notify));
	notify.mode = team.GoodSharedMode();
	Eval(notify.teamInd, team.TeamId());

	for(Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		notify.gateId = it->tmpId.gateId;
		notify.playerId = it->tmpId.playerId;
		notify.memberSeq = team.GetMemberSeq(it->tmpId);

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::ModifyGoodSharedModeNotify::wCmd);
	}
}

void TeamManager::SendTeamRollItemLevelNotify(const Player::TmpId & player, const Team & team)
{
	ACE_TRACE("TeamManager::SendTeamRollItemLevelNotify");

	RelationServiceNS::ModifyTeamRollItemLevelNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.fromGateId = player.gateId;
	notify.fromPlayerId = player.playerId;
	notify.level = team.RollItemLevel();
	Eval(notify.teamInd, team.TeamId());

	for(Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		notify.toGateId = it->tmpId.gateId;
		notify.toPlayerId = it->tmpId.playerId;

		tcp_session session = RelationServiceMessageHandler::get_session(notify.toGateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::ModifyTeamRollItemLevelNotify::wCmd);
	}
}


void TeamManager::SendTeamExpSharedModeNotify(const Team & team)
{
	ACE_TRACE("TeamManager::SendTeamExpSharedModeNotify");

	RelationServiceNS::ModifyExpSharedModeNotify notify;
	memset(&notify, 0, sizeof(notify));
	notify.mode = team.ExpSharedMode();
	Eval(notify.teamInd, team.TeamId());

	for(Team::MemberList::const_iterator it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		notify.gateId = it->tmpId.gateId;
		notify.playerId = it->tmpId.playerId;
		notify.memberSeq = team.GetMemberSeq(it->tmpId);

		tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
		RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::ModifyExpSharedModeNotify::wCmd);
	}
}

void TeamManager::SendTeamFlag(const Player & player)
{
	ACE_TRACE("TeamManager::SendTeamFlag");

	RelationServiceNS::PlayerUplineTeamIdNotify notify;
	notify.gateId = player.GateId();
	notify.playerId = player.PlayerId();
	notify.teamId = player.GetTeamFlag().teamId;
	notify.playerFlag = player.GetTeamFlag().playerFlag;
	notify.teamFlag = player.GetTeamFlag().teamFlag;

	tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
	RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::PlayerUplineTeamIdNotify::wCmd);
}

void TeamManager::SendTeamLog(unsigned uiid, unsigned short level, unsigned teamId, unsigned short type)
{
	ACE_TRACE("TeamManager::SendTeamLog");

	if (RelationServiceMessageHandler::gates.empty())
		return;

	RelationServiceNS::TeamLogNotify notify;
	notify.uiid = uiid;
	notify.level = level;
	notify.teamId = teamId;
	notify.logType = type;

	RelationServiceMessageHandler::gate_map::iterator it = RelationServiceMessageHandler::gates.begin();
	RelationServiceMessageHandler::send_result_message(it->second, &notify, RelationServiceNS::TeamLogNotify::wCmd);
}
void TeamManager::ResetPlayerTeamGain(const Player & player)
{
	ACE_TRACE("TeamManager::ResetPlayerTeamGain");

	RelationServiceNS::NotifyTeamGain notify;
	notify.gateId = player.GateId();
	notify.playerId = player.PlayerId();
	notify.percent = 0;

	tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
	RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::NotifyTeamGain::wCmd);
}

void TeamManager::CalTeamGain(Team & team)
{
	ACE_TRACE("TeamManager::CalTeamGain");

	RelationServiceNS::NotifyTeamGain notify;

	Team::MemberList::iterator it;
	for(it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		it->gainPercent = 0;
	}

	for(it = team.GetMemberList().begin(); it != team.GetMemberList().end(); ++it)
	{
		Player * p1  = SocialRelationManager::instance()->GetPlayerByTmpId(it->tmpId);
		if (!p1)
			continue;

		for(Team::MemberList::iterator _it = it + 1; _it != team.GetMemberList().end(); ++_it)
		{
			Player * p2 = SocialRelationManager::instance()->GetPlayerByTmpId(_it->tmpId);
			if (!p2)
				continue;

			if (!p1->IsFriend(*p2) || !p2->IsFriend(*p1))
				continue;

			Player::FriendInfo * info = p1->GetFriendInfo(p2->Uiid());
			if (!info)
				continue;

			if (info->degree >= 1000)
				notify.percent = 5;
			else if (info->degree >= 600)
				notify.percent = 4;
			else if (info->degree >= 300)
				notify.percent = 5;
			else if(info->degree >= 100)
				notify.percent = 2;
			else if (info->degree >= 1)
				notify.percent = 1;
			else
				notify.percent = 0;

			if (notify.percent > it->gainPercent)
			{
				notify.gateId = it->tmpId.gateId;
				notify.playerId = it->tmpId.playerId;

				tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
				RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::NotifyTeamGain::wCmd);
			}

			if (notify.percent > _it->gainPercent)
			{
				notify.gateId = _it->tmpId.gateId;
				notify.playerId = _it->tmpId.playerId;

				tcp_session session = RelationServiceMessageHandler::get_session(notify.gateId);
				RelationServiceMessageHandler::send_result_message(session, &notify, RelationServiceNS::NotifyTeamGain::wCmd);
			}
		}
	}
}

__SERVICE_SPACE_END_NS__
