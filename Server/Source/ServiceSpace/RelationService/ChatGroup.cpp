﻿#include "ChatGroup.h"
#include <algorithm>

__SERVICE_SPACE_BEGIN_NS__

ChatGroup::ChatGroup()
: id_(0)
{
}

ChatGroup::~ChatGroup()
{
}

const unsigned ChatGroup::GroupId()const
{
	return id_;
}

const Player::TmpId & ChatGroup::GroupOwner()const
{
	return owner_;
}

const std::string & ChatGroup::GetChatPasswrd()const
{
	return passwrd_;
}

const ChatGroup::MemberList & ChatGroup::GetMemberList()const
{
	return members_;
}

void ChatGroup::GroupId(const unsigned id)
{
	id_ = id;
}

void ChatGroup::GroupOwner(const Player::TmpId & id)
{
	owner_ = id;
}

void ChatGroup::SetChatPasswrd(const std::string & passrd)
{
	passwrd_ = passrd;
}

void ChatGroup::AddMember(const Player::TmpId & id)
{
	if (!IsMember(id))
		members_.push_back(id);
}

void ChatGroup::RemoveMember(const Player::TmpId & id)
{
	for(MemberList::iterator it = members_.begin(); it != members_.end(); ++it)
	{
		if (id == *it)
		{
			members_.erase(it);
			break;
		}
	}
}

const bool ChatGroup::IsMember(const Player::TmpId & id)const
{
	return std::find(members_.begin(), members_.end(), id) != members_.end();
}

__SERVICE_SPACE_END_NS__