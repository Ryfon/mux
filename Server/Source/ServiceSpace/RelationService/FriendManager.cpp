﻿#define ACE_NTRACE 0

#include "FriendManager.h"
#include "ChatGroupManager.h"
#include "UnionManager.h"
#include "SocialRelationManager.h"
#ifndef WIN32
#ifndef TIXML_USE_STL
#define TIXML_USE_STL
#endif
#endif
#include <tinyxml.h>


#define	FAILEDRETCODE		0XFFFFFFFF

__SERVICE_SPACE_BEGIN_NS__


void Eval(RelationServiceNS::FriendInfo & info, const Player & whoseFriend, const Player & _friend)
{
	if (!_friend.IsBlack(whoseFriend))
		info.EnterModel = _friend.LineState();
	else
		info.EnterModel = 0;

	std::string fere;
	if (_friend.Fere())
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(_friend.Fere());
		if (p)
			fere = p->Name();
	}

	info.FereNameLen = StringToBuf(fere, info.FereName, sizeof(info.FereName));
	info.Flag = _friend.Sex() ? 2 : 4;
	info.GroupID = whoseFriend.GetFriendGroupId(_friend.Uiid());

	std::string guild;
	if (_friend.GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * p = SocialRelationManager::instance()->GetGuildById(_friend.GetGuildInfo().guildId);
		if (p)
			guild = p->GuildName();
	}

	info.GuildNameLen = StringToBuf(guild, info.GuildName, sizeof(info.GuildName));
	info.MapID = _friend.MapId();
	info.NickNameLen = StringToBuf(_friend.Name(), info.NickName, sizeof(info.NickName));
	info.playerClass = _friend.Job();
	info.playerIcon = _friend.Icon();
	info.playerLevel = _friend.Level();
	info.playerRace = _friend.Race();
	info.uiID = _friend.Uiid();

	const Player::FriendInfo * friendInfo = whoseFriend.GetFriendInfo(_friend.Uiid());
	if (friendInfo)
		info.degree = friendInfo->degree;
	else
		info.degree = 0;

	if (friendInfo && _friend.GetTweets().size() && friendInfo->readTimeLine < _friend.GetTweets().back().createdTime)
	{
		info.tweetsFlag = 1;
	}
	else
	{
		info.tweetsFlag = 0;
	}
}

void Eval(RelationServiceNS::AddEnemySuc & info, const Player & player)
{
	std::string fere;
	if (player.Fere())
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(player.Fere());
		if (p)
			fere = p->Name();
	}

	info.FereNameLen = StringToBuf(fere, info.FereName, sizeof(info.FereName));
	
	std::string guild;
	if (player.GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * p = SocialRelationManager::instance()->GetGuildById(player.GetGuildInfo().guildId);
		if (p)
			guild = p->GuildName();
	}

	info.GuildNameLen = StringToBuf(guild, info.GuildName, sizeof(info.GuildName));
	
	info.MapID = player.MapId();
	info.NameLen = StringToBuf(player.Name(), info.Name, sizeof(info.Name));
	info.player.GateSrvID = player.GateId();
	info.player.PPlayer = player.PlayerId();
	info.playerClass = player.Job();
	info.playerIcon = player.Icon();
	info.playerLevel = player.Level();
	info.playerRace = player.Race();
	info.Replace_uiID = player.Uiid();
	info.Sex = player.Sex();
	info.uiID = player.Uiid();
}
void Eval(RelationServiceNS::EnemylistItem & info, const Player & whoseEnemy, const Player & enemy)
{
	std::string fere;
	if (enemy.Fere())
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(enemy.Fere());
		if (p)
			fere = p->Name();
	}

	info.FereNameLen = StringToBuf(fere, info.FereName, sizeof(info.FereName));
	
	std::string guild;
	if (enemy.GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * p = SocialRelationManager::instance()->GetGuildById(enemy.GetGuildInfo().guildId);
		if (p)
			guild = p->GuildName();
	}

	info.GuildNameLen = StringToBuf(guild, info.GuildName, sizeof(info.GuildName));

	const Player::EnemyInfo * enemyInfo = whoseEnemy.GetEnemyInfo(enemy.Uiid());
	if (!enemyInfo)
		return;

	info.IsLock = enemyInfo->locked;
	info.MapID = enemy.MapId();
	info.NameLen = StringToBuf(enemy.Name(), info.Name, sizeof(info.Name));
	info.Online = enemy.LineState();
	info.player.GateSrvID = enemy.GateId();
	info.player.PPlayer = enemy.PlayerId();
	info.playerClass = enemy.Job();
	info.playerIcon = enemy.Icon();
	info.playerLevel = enemy.Level();
	info.playerRace = enemy.Race();
	info.Sex = enemy.Sex();
	info.uiID = enemy.Uiid();
	info.incidentMap = enemyInfo->mapId;
	info.incidentPosX = enemyInfo->posX;
	info.incidentPosY = enemyInfo->posY;
	info.bKilled = enemyInfo->isKilled;
	info.incidentTime = enemyInfo->time;
}

void Eval(RelationServiceNS::RGFriendInfoUpdate & info, const Player & whoseFriend, const Player & _friend)
{
	std::string fere;
	if (_friend.Fere())
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(_friend.Fere());
		if (p)
			fere = p->Name();
	}

	info.FereNameLen = StringToBuf(fere, info.FereName, sizeof(info.FereName));

	std::string guild;
	if (_friend.GetGuildInfo().guildId != Player::INVALID_GUILD_ID)
	{
		Guild * p = SocialRelationManager::instance()->GetGuildById(_friend.GetGuildInfo().guildId);
		if (p)
			guild = p->GuildName();
	}

	info.GuildNameLen = StringToBuf(guild, info.GuildName, sizeof(info.GuildName));
	info.MapID = _friend.MapId();
	info.Online = _friend.LineState();
	info.player.GateSrvID = _friend.GateId();
	info.player.PPlayer = _friend.PlayerId();
	info.playerClass = _friend.Job();
	info.playerIcon = _friend.Icon();
	info.playerLevel = _friend.Level();
	info.playerRace = _friend.Race();
	info.uiID = _friend.Uiid();

	const Player::FriendInfo * friendInfo = whoseFriend.GetFriendInfo(_friend.Uiid());
	if (friendInfo)
		info.degree = friendInfo->degree;
	else
		info.degree = 0;

	if (_friend.GetTweets().size() && friendInfo->readTimeLine < _friend.GetTweets().back().createdTime)
	{
		info.tweetsFlag = 1;
	}
	else
	{
		info.tweetsFlag = 0;
	}
}

void Eval(RelationServiceNS::GRInfoUpdate & info, const Player & player)
{
	info.uiID = player.Uiid();
	info.playerRace = player.Race();
	info.playerLevel = player.Level();
	info.playerIcon = player.Icon();
	info.playerClass = player.Job();
	info.Online = player.LineState();
	info.MapID = player.MapId();
	info.GuildNameLen = 0;
	info.FereNameLen = 0;
}

const unsigned Eval(RelationServiceNS::FriendGroupInfo (&info)[10], const Player::FriendGroupList & groups)
{
	unsigned i = 0;
	for(Player::FriendGroupList::const_iterator it = groups.begin(); it != groups.end() && i < 10; ++it, ++i)
	{
		info[i].GroupID = it->groupId;
		info[i].GroupNameLen = StringToBuf(it->name, info[i].GroupName, sizeof(info[i].GroupName));
	}

	return i;
}

FriendManager::FriendManager(void)
{	
	LoadFriendGainCfg();
}

FriendManager::~FriendManager(void)
{
}

void FriendManager::AddWhoseFriend(const Player & player)
{
	ACE_TRACE("FriendManager::AddWhoseFriend");

	for(Player::FriendGroupList::const_iterator it = player.GetFriendGroupList().begin(); it != player.GetFriendGroupList().end(); ++it)
	{
		for(Player::FriendList::const_iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(fit->uiid);
			if (!p)
			{
				SS_ERROR("cannot find friend player(uiid:%u)\n", *fit);
				continue;
			}

			p->AddWhoseFriend(player.Uiid());
		}
	}
}

void FriendManager::AddWhoseBlack(const Player & player)
{
	ACE_TRACE("FriendManager::AddWhoseBlack");

	for(Player::BlackList::const_iterator it = player.GetBlackList().begin(); it != player.GetBlackList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
		if (!p)
		{
			SS_ERROR("cannot find black player(uiid:%u)\n", *it);
			continue;
		}
		
		p->AddWhoseBlack(*it);
	}
}

void FriendManager::AddWhoseEnemy(const Player & player)
{
	ACE_TRACE("FriendManager::AddWhoseEnemy");

	for(Player::EnemyList::const_iterator it = player.GetEnemyList().begin(); it != player.GetEnemyList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(it->uiid);
		if (!p)
		{
			SS_ERROR("cannot find enemy player(uiid:%u)\n", it->uiid);
			continue;
		}

		p->AddWhoseEnemy(player.Uiid());
	}
}

void FriendManager::SendPlayerUplineNotify(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerUplineNotify");

	SendPlayerUpdateInfoNotify(player);
}

void FriendManager::SendPlayerOfflineNotify(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerOfflineNotify");

	SendPlayerUpdateInfoNotify(player);
}

void FriendManager::SendPlayerUpdateInfoNotify(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerUpdateInfoNotify");

	RelationServiceNS::RGFriendInfoUpdate info;

	for(std::vector<unsigned>::const_iterator it = player.GetWhoseFriendList().begin(); it != player.GetWhoseFriendList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
		if (!p || p->LineState() == Player::LINE_OFFLINE || player.IsBlack(*p))
			continue;

		memset(&info, 0, sizeof(info));
		Eval(info, *p, player);

		info.player.GateSrvID = p->GateId();
		info.player.PPlayer = p->PlayerId();
		
		tcp_session s = RelationServiceMessageHandler::get_session(p->GateId());
		RelationServiceMessageHandler::send_result_message(s, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);
		
	}

	RelationServiceNS::RGUpdateEnemy info2;
	memset(&info2, 0, sizeof(info2));

	for(Player::WhoseEnemyList::const_iterator it = player.GetWhoseEnemyList().begin(); it != player.GetWhoseEnemyList().end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
		if (!p || p->LineState() == Player::LINE_OFFLINE || player.IsBlack(*p))
			continue;

		Eval(info2.Enemy, *p, player);

		info2.playerID.GateSrvID = p->GateId();
		info2.playerID.PPlayer = p->PlayerId();

		info2.Enemy.IsLock = p->IsLockedEnemy(player.Uiid());

		tcp_session s = RelationServiceMessageHandler::get_session(p->GateId());
		RelationServiceMessageHandler::send_result_message(s, &info2, RelationServiceNS::RGUpdateEnemy::wCmd);
	}
}

void FriendManager::SendPlayerUpdateInfoNotify(const Player & player, const Player & target)
{
	ACE_TRACE("FriendManager::SendPlayerUpdateInfoNotify");

	if (target.IsBlack(player))
		return;

	RelationServiceNS::RGFriendInfoUpdate info;
	memset(&info, 0, sizeof(info));
	Eval(info, target, player);

	info.player.GateSrvID = target.GateId();
	info.player.PPlayer = target.PlayerId();

	tcp_session s = RelationServiceMessageHandler::get_session(target.GateId());
	RelationServiceMessageHandler::send_result_message(s, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);
}

void FriendManager::SendPlayerFriendLineNotify(const Player & player, const Player & target)
{
	ACE_TRACE("FriendManager::SendPlayerFriendLineNotify");

	RelationServiceNS::RGFriendInfoUpdate info;
	memset(&info, 0, sizeof(info));
	Eval(info, target, player);

	info.player.GateSrvID = target.GateId();
	info.player.PPlayer = target.PlayerId();

	tcp_session s = RelationServiceMessageHandler::get_session(info.player.GateSrvID);
	RelationServiceMessageHandler::send_result_message(s, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);
}

void FriendManager::SendPlayerDeletedNotify(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerDeletedNotify");
}

void FriendManager::SendPlayerFriendGroupsInfo(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerFriendGroups");

	RelationServiceNS::RGFriendGroups groups ;
	memset(&groups, 0, sizeof(groups));
	groups.friendGroupsLen = Eval(groups.friendGroups, player.GetFriendGroupList());
	groups.player.GateSrvID = player.GateId();
	groups.player.PPlayer = player.PlayerId();
	groups.Remanent = player.RestGroupMsgCount();
	groups.validSize = player.GetFriendCount();

	tcp_session s = RelationServiceMessageHandler::get_session(player.GateId());
	RelationServiceMessageHandler::send_result_message(s, &groups, RelationServiceNS::RGFriendGroups::wCmd);
}

void FriendManager::SendPlayerGroupFriends(const Player & player, const std::string & group)
{
	ACE_TRACE("FriendManager::SendPlayerGroupFriends");

	const Player::FriendList * lst = player.GetFriendList(group);
	if (lst)
		SendPlayerGroupFriends(player, *lst);
}

void FriendManager::SendPlayerGroupFriends(const Player & player, const Player::FriendList & lst)
{
	ACE_TRACE("FriendManager::SendPlayerGroupFriends");

	RelationServiceNS::RGFriendInfos friends;
	memset(&friends, 0, sizeof(friends));
	friends.player.GateSrvID = player.GateId();
	friends.player.PPlayer = player.PlayerId();
	friends.validSize = lst.size();
	friends.friendinfosLen = 0;

	bool send_empty = (friends.validSize == 0);

	tcp_session s = RelationServiceMessageHandler::get_session(player.GateId());
	for(Player::FriendList::const_iterator it = lst.begin(); it != lst.end(); ++it)
	{
		send_empty = false;

		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(it->uiid);
		if (!p)
			continue;

		Eval(friends.friendinfos[friends.friendinfosLen], player, *p);
		++friends.friendinfosLen;

		if (friends.friendinfosLen == sizeof(friends.friendinfos) / sizeof(friends.friendinfos[0]))
		{
			RelationServiceMessageHandler::send_result_message(s, &friends, RelationServiceNS::RGFriendInfos::wCmd);

			friends.friendinfosLen = 0;
		}

		--friends.validSize;
	}

	if (friends.friendinfosLen || send_empty)
	{
		RelationServiceMessageHandler::send_result_message(s, &friends, RelationServiceNS::RGFriendInfos::wCmd);
	}
	
}

void FriendManager::SendPlayerGroupFriends2(const Player & player, const Player::FriendList & lst)
{
	ACE_TRACE("FriendManager::SendPlayerGroupFriends2");

	RelationServiceNS::QueryFriendsListResult result;
	result.gateId = player.GateId();
	result.playerId = player.PlayerId();
	result.lstLen = 0;
	result.targetUiid = 0;

	tcp_session s = RelationServiceMessageHandler::get_session(player.GateId());
	for(Player::FriendList::const_iterator it = lst.begin(); it != lst.end(); ++it)
	{
		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(it->uiid);
		if (!p)
			continue;

#ifndef _DEBUG
		if (p->Uiid() == player.Uiid())
			continue;
#endif

		Eval(result.friendsLst[result.lstLen], *p);
		++result.lstLen;

		if (result.lstLen == sizeof(result.friendsLst) / sizeof(result.friendsLst[0]))
		{
			RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::QueryFriendsListResult::wCmd);

			result.lstLen = 0;
		}
	}

	if (result.lstLen)
	{
		RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::QueryFriendsListResult::wCmd);
	}
}


void FriendManager::SendPlayerAllGroupsFriends(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerAllGroupsFriends");

	for(Player::FriendGroupList::const_iterator it = player.GetFriendGroupList().begin(); it != player.GetFriendGroupList().end(); ++it)
	{
		SendPlayerGroupFriends(player, it->lst);
	}
}

void FriendManager::SendPlayerBlackList(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerBlackList");

	RelationServiceNS::RGBlacklist blacks;
	memset(&blacks, 0, sizeof(blacks));
	blacks.playerID.GateSrvID = player.GateId();
	blacks.playerID.PPlayer = player.PlayerId();
	blacks.validSize = player.GetBlackList().size();
	blacks.BlacklistLen = 0;

	bool send_empty = (blacks.validSize == 0);

	tcp_session s = RelationServiceMessageHandler::get_session(player.GateId());
	for(Player::BlackList::const_iterator it = player.GetBlackList().begin(); it != player.GetBlackList().end(); ++it)
	{
		send_empty = false;

		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
		if (!p)
			continue;

		blacks.Blacklist[blacks.BlacklistLen].uiID = p->Uiid();
		blacks.Blacklist[blacks.BlacklistLen].NameLen = StringToBuf(p->Name(), blacks.Blacklist[blacks.BlacklistLen].Name, sizeof(blacks.Blacklist[blacks.BlacklistLen].Name));

		++blacks.BlacklistLen;

		if (blacks.BlacklistLen == sizeof(blacks.Blacklist) / sizeof(blacks.Blacklist[0]))
		{
			RelationServiceMessageHandler::send_result_message(s, &blacks, RelationServiceNS::RGBlacklist::wCmd);

			blacks.BlacklistLen = 0;
		}

		--blacks.validSize;
	}

	if (blacks.BlacklistLen || send_empty)
		RelationServiceMessageHandler::send_result_message(s, &blacks, RelationServiceNS::RGBlacklist::wCmd);
	
}

void FriendManager::SendPlayerEnemyList(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerEnemyList");

	RelationServiceNS::RGEnemylist enemys;
	memset(&enemys, 0, sizeof(enemys));
	enemys.playerID.GateSrvID = player.GateId();
	enemys.playerID.PPlayer = player.PlayerId();
	enemys.validSize = player.GetEnemyList().size();
	enemys.EnemylistLen = 0;

	bool send_empty = (enemys.validSize == 0);

	tcp_session s = RelationServiceMessageHandler::get_session(player.GateId());

	for(Player::EnemyList::const_iterator it = player.GetEnemyList().begin(); it != player.GetEnemyList().end(); ++it)
	{
		send_empty = false;

		Player * p = SocialRelationManager::instance()->GetPlayerByUiid(it->uiid);
		if (!p)
			continue;

		Eval(enemys.Enemylist[enemys.EnemylistLen], player, *p);
		++enemys.EnemylistLen;

		if (enemys.EnemylistLen == sizeof(enemys.Enemylist) / sizeof(enemys.Enemylist[0]))
		{
			RelationServiceMessageHandler::send_result_message(s, &enemys, RelationServiceNS::RGEnemylist::wCmd);

			enemys.EnemylistLen = 0;
		}

		//--enemys.validSize;
	}

	if (enemys.EnemylistLen || send_empty)
		RelationServiceMessageHandler::send_result_message(s, &enemys, RelationServiceNS::RGEnemylist::wCmd);
}

void FriendManager::SendPlayerRestGroupMsgCount(const Player & player)
{
	ACE_TRACE("FriendManager::SendPlayerRestGroupMsgCount");

	RelationServiceNS::RGNotifyRGroupMsgNum notify;
	memset(&notify, 0, sizeof(notify));
	notify.player.GateSrvID = player.GateId();
	notify.player.PPlayer = player.PlayerId();
	notify.Remanent = player.RestGroupMsgCount();

	tcp_session s = RelationServiceMessageHandler::get_session(notify.player.GateSrvID);
	RelationServiceMessageHandler::send_result_message(s, &notify, RelationServiceNS::RGNotifyRGroupMsgNum::wCmd);
}

void FriendManager::SendFriendPlayerRemoved(const Player & player, const Player & target)
{
	ACE_TRACE("FriendManager::SendFriendPlayerRemoved");

	RelationServiceNS::RGDelFriend result;
	memset(&result, 0, sizeof(result));
	result.playerID.GateSrvID = player.GateId();
	result.playerID.PPlayer = player.PlayerId();
	Eval(result.delfriend.Suc.playerIDByDell, player, target);
	result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

	tcp_session s = RelationServiceMessageHandler::get_session(result.playerID.GateSrvID);
	RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::RGDelFriend::wCmd);
}

void FriendManager::SendBlackPlayerRemoved(const Player & player, const Player & target)
{
	ACE_TRACE("FriendManager::SendBlackPlayerRemoved");

	RelationServiceNS::RGDelFromBlackList result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.playerID.GateSrvID = player.GateId();
	result.playerID.PPlayer = player.PlayerId();
	result.result.Suc.NameLen = StringToBuf(target.Name(), result.result.Suc.Name, sizeof(result.result.Suc.Name));

	tcp_session s = RelationServiceMessageHandler::get_session(result.playerID.GateSrvID);
	RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::RGDelFromBlackList::wCmd);
}

void FriendManager::SendEnemyPlayerRemoved(const Player & player, const Player & target)
{
	ACE_TRACE("FriendManager::SendFriendPlayerRemoved");

	RelationServiceNS::RGDelEnemy result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.playerID.GateSrvID = player.GateId();
	result.playerID.PPlayer = player.PlayerId();
	result.result.Suc.uiID = target.Uiid();

	tcp_session s = RelationServiceMessageHandler::get_session(result.playerID.GateSrvID);
	RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::RGDelEnemy::wCmd);
}

unsigned FriendManager::GetLevelUpNeededExp(const Player & player)
{
	return 100;
}

unsigned FriendManager::GetFriendGainItem(unsigned level, unsigned job, unsigned degree)
{
	unsigned item = 0;
	std::vector<FriendGain>::const_iterator it = friendGain_.begin();
	for(; it != friendGain_.end(); ++it)
	{
		if (it->level <= level && it->job == job && it->degree <= degree)
			item = it->itemId;
	}
	return item;
}

void FriendManager::LoadFriendGainCfg()
{
	ACE_TRACE("FriendManager::LoadFriendGainCfg");

	TiXmlDocument cfg;
	if (!cfg.LoadFile("./config/guild/guildconfig.xml"))
	{
		SS_ERROR("failed to load guildconfig xml config\n");
		return;
	}

	TiXmlElement * root = cfg.RootElement();
	if (!root)
	{
		SS_ERROR("cannot find guildconfig xml config root element\n");
		return;
	}

	SS_INFO("load GainActiveWay config......\n");
	TiXmlElement * nodeSkill = root->FirstChildElement("LevelUpGift");
	while(nodeSkill)
	{
		TiXmlAttribute * attr = nodeSkill->FirstAttribute();
		FriendGain gain;
		memset(&gain, 0, sizeof(gain));
		while(attr)
		{
			if(strcmp(attr->Name(), "Level") == 0)
			{
				gain.level = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "PlayerJob") == 0)
			{
				gain.job = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "FriendShipLever") == 0)
			{
				gain.degree = atoi(attr->Value());
			}
			else if (strcmp(attr->Name(), "Itemid") == 0)
			{
				gain.itemId = atoi(attr->Value());
			}

			attr = attr->Next();
		}

		friendGain_.push_back(gain);

		nodeSkill = nodeSkill->NextSiblingElement("LevelUpGift");
	}
}

void FriendManager::SendFriendDegreeGainEmail(tcp_session session, const Player & player)
{
	ACE_TRACE("FriendManager::SendFriendDegreeGainEmail");

	RelationServiceNS::FriendDegreeGainEmail email;

	std::stringstream s;
	s << "由于好友升级到"  << player.Level()  << "级,你获得宝箱奖励.";
	
	std::string content = s.str();
	email.contentLen = StringToBuf(content, email.content, sizeof(email.content));
	
	std::string title;
	title = "物品奖励";

	email.titleLen = StringToBuf(title, email.title, sizeof(email.title));

	for(Player::FriendGroupList::const_iterator it = player.GetFriendGroupList().begin(); it != player.GetFriendGroupList().end(); ++it)
	{
		for(Player::FriendList::const_iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(fit->uiid, false);
			if (!p)
				continue;

			const Player::FriendInfo * info = player.GetFriendInfo(fit->uiid);
			if (!info)
				continue;

			email.nameLen = StringToBuf(p->Name(), email.name, sizeof(email.name));

			email.itemId = GetFriendGainItem(player.Level(), p->Job(), info->degree);

			RelationServiceMessageHandler::send_result_message(session, &email, RelationServiceNS::FriendDegreeGainEmail::wCmd);
		}
	}
}

int FriendManager::HandlePlayerUpline(tcp_session session, Player & player)
{
	ACE_TRACE("FriendManager::HandlePlayerUpline");

	time_t now = time(0);
	if (DiffDayCheck(now, player.GetBeginCalGroupMsgCountTime()))
	{
		player.SetRestGroupsMsgCount(10, now);
	}

	AddWhoseFriend(player);
	AddWhoseBlack(player);
	AddWhoseEnemy(player);

	SendPlayerFriendGroupsInfo(player);
	SendPlayerAllGroupsFriends(player);
	SendPlayerBlackList(player);
	SendPlayerEnemyList(player);

	SendPlayerUplineNotify(player);

	return 0;
}

int FriendManager::HandlePlayerOffline(tcp_session session, const Player & player)
{
	ACE_TRACE("FriendManager::HandlePlayerOffline");

	SendPlayerOfflineNotify(player);
	return 0;
}

int FriendManager::HandlePlayerUpdateInfo(tcp_session sessin, const Player & player)
{
	ACE_TRACE("FriendManager::HandlePlayerUpdateInfo");

	SendPlayerUpdateInfoNotify(player);

	return 0;
}

int FriendManager::HandleDeletePlayer(Player & player)
{
	ACE_TRACE("FriendManager::HandleDeletePlayer");

	do{
		for(Player::WhoseFriendList::const_iterator it = player.GetWhoseFriendList().begin(); it != player.GetWhoseFriendList().end(); ++it)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
			if (!p)
				continue;

			unsigned groupId = p->GetFriendGroupId(player.Uiid());

			p->RemoveFriend(player);

			if (p->LineState() != Player::LINE_OFFLINE)
			{
				SendFriendPlayerRemoved(*p, player);
			}

			SocialRelationManager::instance()->GetDB().Save(*p);
		}
	}while(0);

	do{
		for(Player::WhoseBlackList::const_iterator it = player.GetWhoseBlackList().begin(); it != player.GetWhoseBlackList().end(); ++it)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
			if (!p)
				continue;

			p->RemoveBlack(player);

			if (p->LineState() != Player::LINE_OFFLINE)
			{
				SendBlackPlayerRemoved(*p, player);
			}

			SocialRelationManager::instance()->GetDB().Save(*p);
		}
	}while(0);

	do{
		for(Player::WhoseEnemyList::const_iterator it = player.GetWhoseEnemyList().begin(); it != player.GetWhoseEnemyList().end(); ++it)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
			if (!p)
				continue;

			p->RemoveEnemy(player);

			if (p->LineState() != Player::LINE_OFFLINE)
			{
				SendEnemyPlayerRemoved(*p, player);
			}

			SocialRelationManager::instance()->GetDB().Save(*p);
		}
	}while(0);

	SendPlayerDeletedNotify(player);

	return 0;
}

int FriendManager::HandleAddFriendRequest(tcp_session session, RelationServiceNS::GRAddFriend & req)
{
	ACE_TRACE("FriendManager::HandleAddFriendRequest");

	RelationServiceNS::RGAddFriend result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player)
		return -1;

	Player * target = 0;
	if (player->IsFriend(req.ByAdduiID))
	{
		//result.addFriend.Error.ErrorCode = 
		return -1;
	}
	else if (player->IsBlack(req.ByAdduiID))
	{
		return 0;
	}
	else if (player->IsEnemy(req.ByAdduiID))
	{
		return 0;
	}
	else if (player->GetFriendCount() >= 50)
	{
		return 0;
	}
	else if (player->Uiid() == req.ByAdduiID)
	{
		return 0;
	}
	else if (invitees_.find(req.ByAdduiID) != invitees_.end())
	{
		return 0;
	}
	else if((target	= SocialRelationManager::instance()->GetPlayerByUiid(req.ByAdduiID)) == 0)
	{
		return 0;
	}
	else if (target->LineState() == Player::LINE_OFFLINE)
	{
		return 0;
	}
	else
	{
		player->AddFriend(default_group_name, *target);
		target->AddWhoseFriend(player->Uiid());
		if (req.groupId)
			player->TransferFriend(req.ByAdduiID, player->GetFriendGroupId(req.ByAdduiID), req.groupId);		

		result.addFriend.Suc.playerID.GateSrvID = player->GateId();
		result.addFriend.Suc.playerID.PPlayer = player->PlayerId();
		Eval(result.addFriend.Suc.friendinfo, *player, *target);

		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;	

		player->AddNewFriendAddedTweet(target->Name());
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGAddFriend::wCmd);

	if (player->IsFriend(*target) && target->IsFriend(*player))
	{
		Player::FriendInfo * info = player->GetFriendInfo(target->Uiid());
		if (info)
			info->degree = 10;
		else
			SS_ERROR("%s cannot find friend[%s] info\n", player->Name().c_str(), target->Name().c_str());

		info = target->GetFriendInfo(player->Uiid());
		if (info)
		{
			info->degree = 10;
		}
		else
			SS_ERROR("%s cannot find friend[%s] info\n", target->Name().c_str(), player->Name().c_str());

		SocialRelationManager::instance()->GetDB().Save(*player);
		SocialRelationManager::instance()->GetDB().Save(*target);

		SendPlayerUpdateInfoNotify(*player, *target);
		SendPlayerUpdateInfoNotify(*target, *player);
	}

	return 0;
}

int FriendManager::HandleRemoveFriendRequest(tcp_session session, const RelationServiceNS::GRDelFriend & req)
{
	ACE_TRACE("FriendManager::HandleRemoveFrienRequest");

	RelationServiceNS::RGDelFriend result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	Player * target = 0;
	if (!player->IsFriend(req.ByAdduiID))
	{
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.ByAdduiID)) == 0)
	{
	}
	else
	{
		Player::FriendInfo * info = target->GetFriendInfo(player->Uiid());
		if (info)
		{
			info->degree = 0;
			SendPlayerUpdateInfoNotify(*player, *target);
		}

		player->RemoveFriend(req.ByAdduiID);
		target->RemoveWhoseFriend(req.uiID);
		Eval(result.delfriend.Suc.playerIDByDell, *player, *target);
		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		SocialRelationManager::instance()->GetDB().Save(*player);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGDelFriend::wCmd);

	return 0;
}

int FriendManager::HandleCreeateFriendGroup(tcp_session session, const RelationServiceNS::GRAddGroup & req)
{
	ACE_TRACE("FriendManager::HandleCreeateFriendGroup");

	RelationServiceNS::RGAddGroup result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	std::string groupName;
	BuffToString(req.GroupName, req.GroupNameLen, sizeof(req.GroupName), groupName);

	if (player->GetFriendGroupList().size() >= 10)
	{
	}
	else
	{
		result.result.Suc.GroupID = player->AddFriendGroup(groupName);
		result.result.Suc.GroupNameLen = StringToBuf(groupName, result.result.Suc.GroupName, sizeof(result.result.Suc.GroupName));
		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		SocialRelationManager::instance()->GetDB().Save(*player);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGAddGroup::wCmd);

	return 0;
}

int FriendManager::HandleDeleteFriendGroup(tcp_session session, const RelationServiceNS::GRDelGroup & req)
{
	ACE_TRACE("FriendManager::HandleDeleteFriendGroup");

	RelationServiceNS::RGDelGroup result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	const Player::FriendList * lst = player->GetFriendList(req.GroupID);
	if (!lst)
	{
	}
	else if (!lst->empty())
	{
	}
	else if (player->GetFriendGroupName(req.GroupID) == default_group_name)
	{
	}
	else
	{
		player->RemoveFriendGroup(req.GroupID);
		result.result.Suc.GroupID = req.GroupID;
		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		SocialRelationManager::instance()->GetDB().Save(*player);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGDelGroup::wCmd);

	return 0;
}

int FriendManager::HandleRenameFriendGroup(tcp_session session, const RelationServiceNS::GRAlterGroupName & req)
{
	ACE_TRACE("FriendManager::HandleRenameFriendGroup");
	
	RelationServiceNS::RGAlterGroupName result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	std::string groupName;
	BuffToString(req.newGroupName, req.newGroupNameLen, sizeof(req.newGroupName), groupName);

	const Player::FriendList * lst = player->GetFriendList(req.GroupID);
	if (!lst)
	{
	}
	else if (player->GetFriendGroupName(req.GroupID) == default_group_name)
	{
	}
	else
	{
		player->RenameFriendGroup(req.GroupID, groupName);
		result.result.Suc.GroupID = req.GroupID;
		result.result.Suc.newGroupNameLen = StringToBuf(groupName, result.result.Suc.newGroupName, sizeof(result.result.Suc.newGroupName));
		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		SocialRelationManager::instance()->GetDB().Save(*player);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGAlterGroupName::wCmd);

	return 0;
}

int FriendManager::HandleTransferFriend(tcp_session session, const RelationServiceNS::GRFriendAlterGroupID & req)
{
	ACE_TRACE("FriendManager::HandleTransferFriend");

	RelationServiceNS::RGFriendAlterGroupID result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	const Player::FriendList * oldLst = player->GetFriendList(req.preGroupID);
	const Player::FriendList * newLst = player->GetFriendList(req.newGroupID);
	if (!oldLst || !newLst)
	{
	}
	else if (player->IsFriend(req.FriendID))
	{
		player->TransferFriend(req.FriendID, req.preGroupID, req.newGroupID);
		result.result.Suc.FriendID = req.FriendID;
		result.result.Suc.preGroupID = req.preGroupID;
		result.result.Suc.newGroupID = req.newGroupID;
		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		SocialRelationManager::instance()->GetDB().Save(*player);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGFriendAlterGroupID::wCmd);

	return 0;
}

int FriendManager::HandleAddBlacklistPlayer(tcp_session  session, const RelationServiceNS::GRAddToBlacklist & req)
{
	ACE_TRACE("FriendManager::HandleAddBlacklistPlayer");

	RelationServiceNS::RGAddToBlacklist result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	std::string name;
	BuffToString(req.Name, req.NameLen, sizeof(req.Name), name);

	Player * target = 0;
	if (player->GetBlackCount() >= 50)
	{
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByName(name)) == 0)
	{
	}
	else if (player->IsBlack(*target))
	{
	}
	else if (player->Uiid() == target->Uiid())
	{
	}
	else
	{
		player->RemoveFriend(*target);
		target->RemoveWhoseFriend(player->Uiid());

		player->AddBlack(*target);
		target->AddWhoseBlack(player->Uiid());

		result.result.Suc.NameLen = StringToBuf(target->Name(), result.result.Suc.Name, sizeof(result.result.Suc.Name));
		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		SocialRelationManager::instance()->GetDB().Save(*player);

		if (target->IsFriend(*player))
		{
			RelationServiceNS::RGFriendInfoUpdate info;
			memset(&info, 0, sizeof(info));
			Eval(info, *target, *player);
			info.Online = 0;
			info.player.GateSrvID = target->GateId();
			info.player.PPlayer = target->PlayerId();

			tcp_session tmp = RelationServiceMessageHandler::get_session(info.player.GateSrvID);
			RelationServiceMessageHandler::send_result_message(tmp, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);
		}
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGAddToBlacklist::wCmd);
	
	return 0;
}

int FriendManager::HandleRemoveBlacklistPlayer(tcp_session session, const RelationServiceNS::GRDelFromBlacklist & req)
{
	ACE_TRACE("FriendManager::HandleRemoveBlacklistPlayer");

	RelationServiceNS::RGDelFromBlackList result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	std::string name;
	BuffToString(req.Name, req.NameLen, sizeof(req.Name), name);
	Player * target = 0;
	if ((target = SocialRelationManager::instance()->GetPlayerByName(name)) == 0)
	{
	}
	else if (!player->IsBlack(*target))
	{
	}
	else
	{
		player->RemoveBlack(*target);
		target->RemoveWhoseBlack(player->Uiid());

		result.result.Suc.NameLen = StringToBuf(target->Name(), result.result.Suc.Name, sizeof(result.result.Suc.Name));
		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGDelFromBlackList::wCmd);

	return 0;
}


int FriendManager::HandleSendFriendGroupMessage(tcp_session session, const RelationServiceNS::GRSendGroupMsg & req)
{
	ACE_TRACE("FriendManager::HandleSendFriendGroupMessage");

	RelationServiceNS::RGSendGroupMsg result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	const Player::FriendList * lst = player->GetFriendList(req.GroupID);
	if (!lst)
	{
	}
	else if (player->RestGroupMsgCount() == 0)
	{
	}
	else
	{
		result.result.Suc.uiID = player->Uiid();
		result.result.Suc.NameLen = StringToBuf(player->Name(), result.result.Suc.Name, sizeof(result.result.Suc.Name));
		memcpy(result.result.Suc.Msg, req.Msg, sizeof(result.result.Suc.Msg));
		result.result.Suc.MsgLen = req.MsgLen;

		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		for(Player::FriendList::const_iterator it = lst->begin(); it != lst->end(); ++it)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(it->uiid, false);
			if (!p || p->LineState() == Player::LINE_OFFLINE || p->IsBlack(*player))
				continue;

			result.playerID.GateSrvID = p->GateId();
			result.playerID.PPlayer = p->PlayerId();

			tcp_session s = RelationServiceMessageHandler::get_session(result.playerID.GateSrvID);
			RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::RGSendGroupMsg::wCmd);
		}

		player->SetRestGroupsMsgCount(player->RestGroupMsgCount() - 1, time(0));
		result.Remanent = player->RestGroupMsgCount();
	}

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGSendGroupMsg::wCmd);

	return 0;
}

int FriendManager::HandleSendPersonalMessage(tcp_session session, const RelationServiceNS::GRSendMsg & req)
{
	ACE_TRACE("FriendManager::HandleSendPersonalMessage");

	RelationServiceNS::RGSendMsg result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	Player * target = 0;
	if (!player->IsFriend(req.rcvuiID))
	{
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.rcvuiID, false)) == 0)
	{
	}
	else if (target->LineState() == Player::LINE_OFFLINE)
	{
	}
	else if (target->IsBlack(*player))
	{
	}
	else
	{
		result.playerID.GateSrvID = target->GateId();
		result.playerID.PPlayer = target->PlayerId();

		result.result.Suc.uiID = player->Uiid();
		result.result.Suc.NameLen = StringToBuf(player->Name(), result.result.Suc.Name, sizeof(result.result.Suc.Name));
		memcpy(result.result.Suc.Msg, req.Msg, sizeof(result.result.Suc.Msg));

		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	}

	tcp_session s = RelationServiceMessageHandler::get_session(result.playerID.GateSrvID);
	RelationServiceMessageHandler::send_result_message(s, &result, RelationServiceNS::RGSendMsg::wCmd);

	return 0;
}

int FriendManager::HandleAddEnemy(tcp_session, const RelationServiceNS::GRAddEnemy & req, bool bKilled)
{
	ACE_TRACE("FriendManager::HandleAddEnemy");

	RelationServiceNS::RGAddEnemy result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	Player * target = 0;
	if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.addtouiID)) == 0)
	{
		return 0;
	}
	else if (player->Uiid() == target->Uiid())
	{
		return 0;
	}
	else if (player->IsFriend(target->Uiid()))
	{
		return 0;
	}
	else
	{
		bool locked = player->IsLockedEnemy(target->Uiid());

		player->RemoveEnemy(*target);
		target->RemoveWhoseEnemy(player->Uiid());

		if (player->GetEnemyList().size() >= 25)
		{
			const unsigned uiid = player->RemoveFirstUnlockedEnemy();
			Player * p = 0;
			if (uiid && (p = SocialRelationManager::instance()->GetPlayerByUiid(uiid)) != 0)
				SendEnemyPlayerRemoved(*player, *p);
		}

		if (player->GetEnemyList().size() >= 25)
		{
		}
		else
		{
			if (!bKilled)
				player->AddKillPlayerTweet(target->Name());

			player->AddEnemy(*target, req.MapID, req.posX, req.posY, bKilled);

			if (locked)
				player->LockEnemy(target->Uiid());

			target->AddWhoseEnemy(player->Uiid());

			Eval(result.result.Suc, *target);

			result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

			SocialRelationManager::instance()->GetDB().Save(*player);
		}
	}

	tcp_session session = RelationServiceMessageHandler::get_session(player->GateId());
	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGAddEnemy::wCmd);

	if (result.ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED)
	{
		this->SendPlayerEnemyList(*player);
	}

	return 0;
}

int FriendManager::HandleRemoveEnemy(tcp_session session, const RelationServiceNS::GRDelEnemy & req)
{
	ACE_TRACE("FriendManager::HandleRemoveEnemy");

	RelationServiceNS::RGDelEnemy result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	Player * target = 0;
	if (!player->IsEnemy(req.deluiID))
	{
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.deluiID)) == 0)
	{
	}
	else
	{
		player->RemoveEnemy(*target);
		target->RemoveWhoseEnemy(player->Uiid());

		result.result.Suc.uiID = target->Uiid();

		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		SocialRelationManager::instance()->GetDB().Save(*player);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGDelEnemy::wCmd);

	return 0;
}

int FriendManager::HandleLockEnemy(tcp_session session, const RelationServiceNS::GRLockEnemy & req)
{
	ACE_TRACE("FriendManager::HandleLockEnemy");

	RelationServiceNS::RGLockEnemy result;
	memset(&result, 0, sizeof(result));
	result.ErrCode = RelationServiceNS::R_G_RESULT_FAILED;

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID, false);
	if (!player || player->LineState() == Player::LINE_OFFLINE)
		return -1;

	result.playerID.GateSrvID = player->GateId();
	result.playerID.PPlayer = player->PlayerId();

	Player * target = 0;
	if (!player->IsEnemy(req.LockuiID))
	{
	}
	else if ((target = SocialRelationManager::instance()->GetPlayerByUiid(req.LockuiID)) == 0)
	{
	}
	else
	{
		if (req.IsLock)
			player->LockEnemy(target->Uiid());
		else
			player->UnlockEnemy(target->Uiid());

		result.IsLock = req.IsLock;
		result.result.Suc.uiID = target->Uiid();
		result.ErrCode = RelationServiceNS::R_G_RESULT_SUCCEED;

		SocialRelationManager::instance()->GetDB().Save(*player);
	}

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::RGLockEnemy::wCmd);


	return 0;
}

int FriendManager::HandleQueryPlayerRelationRequest(tcp_session session, const RelationServiceNS::QueryPlayerRelationRequest & req)
{
	ACE_TRACE("FriendManager::HandleQueryPlayerRelationRequest");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid);
	if (!player)
		return -1;

	RelationServiceNS::QueryPlayerRelationResult result;
	memset(&result, 0, sizeof(result));
	result.gmGateId = req.gmGateId;
	result.gmPlayerId = req.gmPlayerId;
	result.relation.player.GateSrvID = result.gmGateId;
	result.relation.player.PPlayer = result.gmPlayerId;
	result.uiid = req.uiid;
	result.relation.validSize = player->GetFriendCount() + player->GetEnemyCount() + player->GetBlackCount();
	result.relation.friendinfosLen = 0;

	bool send_empty = (result.relation.validSize == 0);

	do {
		for(Player::FriendGroupList::const_iterator it = player->GetFriendGroupList().begin();
			it != player->GetFriendGroupList().end(); ++it)
		{
			for(Player::FriendList::const_iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
			{
				send_empty = false;

				Player * p = SocialRelationManager::instance()->GetPlayerByUiid(fit->uiid);
				if (!p)
					continue;

				Eval(result.relation.friendinfos[result.relation.friendinfosLen], *player, *p);
				result.relation.friendinfos[result.relation.friendinfosLen].GroupID = 0;
				++result.relation.friendinfosLen;

				if (result.relation.friendinfosLen == sizeof(result.relation.friendinfos) / sizeof(result.relation.friendinfos[0]))
				{
					RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryPlayerRelationResult::wCmd);

					result.relation.friendinfosLen = 0;
				}
				--result.relation.validSize;
			}
		}
	}
	while(false);

	do{
		for(Player::EnemyList::const_iterator it = player->GetEnemyList().begin(); it != player->GetEnemyList().end(); ++it)
		{
			send_empty = false;

			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(it->uiid);
			if (!p)
				continue;

			Eval(result.relation.friendinfos[result.relation.friendinfosLen], *player, *p);
			result.relation.friendinfos[result.relation.friendinfosLen].GroupID = 1;
			++result.relation.friendinfosLen;

			if (result.relation.friendinfosLen == sizeof(result.relation.friendinfos) / sizeof(result.relation.friendinfos[0]))
			{
				RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryPlayerRelationResult::wCmd);

				result.relation.friendinfosLen = 0;
			}
			--result.relation.validSize;
		}
	}
	while(false);

	do
	{
		for(Player::BlackList::const_iterator it = player->GetBlackList().begin(); it != player->GetBlackList().end(); ++it)
		{
			send_empty = false;

			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(*it);
			if (!p)
				continue;

			Eval(result.relation.friendinfos[result.relation.friendinfosLen], *player, *p);
			result.relation.friendinfos[result.relation.friendinfosLen].GroupID = 2;
			++result.relation.friendinfosLen;

			if (result.relation.friendinfosLen == sizeof(result.relation.friendinfos) / sizeof(result.relation.friendinfos[0]))
			{
				RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryPlayerRelationResult::wCmd);

				result.relation.friendinfosLen = 0;
			}
			--result.relation.validSize;
		}
	}
	while(false);

	if (!send_empty)
		send_empty = ((player->GetFriendCount()  + player->GetEnemyCount() + player->GetBlackCount()) % (sizeof(result.relation.friendinfos) / sizeof(result.relation.friendinfos[0]))) != 0;

	if (result.relation.friendinfosLen || send_empty)
	{
		RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryPlayerRelationResult::wCmd);
	}
	
	return 0;
}

int FriendManager::HandleQueryPlayerInfoRequest(tcp_session session, const RelationServiceNS::QueryPlayerInfoRequest & req)
{
	ACE_TRACE("FriendManager::HandleQueryPlayerInfoRequest");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiid);
	if (!player)
		return -1;

	RelationServiceNS::QueryPlayerInfoResult result;
	memset(&result, 0, sizeof(result));
	result.returnCode = RelationServiceNS::R_G_RESULT_SUCCEED;
	result.gateId = player->GateId();
	result.playerId = player->PlayerId();

	std::string name;
	BuffToString(req.targetName, req.nameLen, sizeof(req.targetName), name);
	Player * target = SocialRelationManager::instance()->GetPlayerByName(name);
	if (!target)
		return -1;

	Eval(result.targetInfo, *player, *target);

	RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryPlayerInfoResult::wCmd);

	return 0;
}

int FriendManager::HandleAddFriendlyDegreeRequest(tcp_session session, const RelationServiceNS::AddFriendlyDegree & req)
{
	ACE_TRACE("FriendManager::HandleAddFriendlyDegreeRequest");

	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.playerId);
	if (!player)
		return -1;

	Player * target = SocialRelationManager::instance()->GetPlayerByUiid(req.friendUiid, false);
	if (!target)
		return -1;

	if (player->IsFriend(*target) && target->IsFriend(*player))
	{
		player->AddFriendLyDegree(target->Uiid(), req.degreeAdded);
		target->AddFriendLyDegree(player->Uiid(), req.degreeAdded);

		RelationServiceNS::RGFriendInfoUpdate info;

		memset(&info, 0, sizeof(info));
		Eval(info, *target, *player);
		
		info.player.GateSrvID = target->GateId();
		info.player.PPlayer = target->PlayerId();

		tcp_session s = RelationServiceMessageHandler::get_session(target->GateId());
		RelationServiceMessageHandler::send_result_message(s, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);

		memset(&info, 0, sizeof(info));
		Eval(info, *player, *target);
		
		info.player.GateSrvID = player->GateId();
		info.player.PPlayer = player->PlayerId();

		s = RelationServiceMessageHandler::get_session(player->GateId());
		RelationServiceMessageHandler::send_result_message(s, &info, RelationServiceNS::RGFriendInfoUpdate::wCmd);

	}
	return 0;
}

int FriendManager::HandleAddTweetRequest(tcp_session session, const RelationServiceNS::AddTweet & req)
{
	ACE_TRACE("FriendManager::HandleAddTweetRequest");

	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.playerId);
	if (!player)
		return -1;

	Player::Tweet tweet;
	tweet.set(const_cast<RelationServiceNS::Tweet &>(req.tweet));

	player->AddTweet(tweet);
	this->SendPlayerUpdateInfoNotify(*player);
	SocialRelationManager::instance()->GetDB().SaveTweet(*player, tweet);
	return 0;
}

int FriendManager::HandleQueryTweetsRequest(tcp_session session, const RelationServiceNS::QueryFriendTweetsRequest & req)
{
	ACE_TRACE("FriendManager::HandleQueryTweetsRequest");

	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.playerId);
	if (!player)
		return -1;

	Player * target = SocialRelationManager::instance()->GetPlayerByUiid(req.targetUiid, false);
	if (!target)
		return -1;

	RelationServiceNS::QueryFriendTweetsResult result;
	::memset(&result, 0, sizeof(result));

	result.gateId = player->GateId();
	result.playerId = player->PlayerId();
	result.nameLen = StringToBuf(target->Name(), result.friendName, sizeof(result.friendName));

	for(Player::TweetsList::const_reverse_iterator it = target->GetTweets().rbegin(); it != target->GetTweets().rend(); ++it)
	{
		it->get(result.tweet);
		
		RelationServiceMessageHandler::send_result_message(session, &result, RelationServiceNS::QueryFriendTweetsResult::wCmd);
	}
	
	Player::FriendInfo * info = player->GetFriendInfo(req.targetUiid);
	if (info && target->GetTweets().size())
		info->readTimeLine = target->GetTweets().back().createdTime;
	return 0;
}

int FriendManager::HandleQueryFriendsListRequest(tcp_session session, const RelationServiceNS::QueryFriendsListRequest & req)
{
	ACE_TRACE("FriendManager::HandleQueryFriendsListRequest");

	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.playerId);
	if (!player)
		return -1;

	Player * target = SocialRelationManager::instance()->GetPlayerByUiid(req.targetUiid, false);
	if (!target)
		return -1;

	Player::FriendGroupList::const_iterator it;
	for(it  = target->GetFriendGroupList().begin(); it != target->GetFriendGroupList().end(); ++it)
	{
		this->SendPlayerGroupFriends2(*player, it->lst);
	}
	
	return 0;
}

int FriendManager::HandleNotifyExpAdded(tcp_session session, const RelationServiceNS::NotifyExpAdded & req)
{
	ACE_TRACE("FriendManager::HandleNotifyExpAdded");

	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.playerId);
	if (!player)
		return -1;

	Player::FriendGroupList::const_iterator it;
	for(it  = player->GetFriendGroupList().begin(); it != player->GetFriendGroupList().end(); ++it)
	{
		for(Player::FriendList::const_iterator fit = it->lst.begin(); fit != it->lst.end(); ++fit)
		{
			Player * p = SocialRelationManager::instance()->GetPlayerByUiid(fit->uiid);
			if (!p)
				continue;

			if (!p->IsFriend(*player) || !player->IsFriend(*p))
				continue;

			Player::FriendInfo * info = player->GetFriendInfo(fit->uiid);
			if (!info)
				continue;

			RelationServiceNS::NotifyAddExp addExp;
			if (info->degree >= 1000)
			{
				if (!req.isBoss)
					addExp.exp = 0.04f * req.exp;
				else
					addExp.exp = GetLevelUpNeededExp(*p) * 0.02f;
			}
			else if (info->degree >= 600)
			{
				if (!req.isBoss)
					addExp.exp = 0.03f * req.exp;
				else
					addExp.exp = GetLevelUpNeededExp(*p) * 0.01f;
			}
			else if (info->degree >= 300)
			{
				addExp.exp = 0.02f * req.exp;
			}
			else if (info->degree >= 100)
			{
				addExp.exp = 0.01f * req.exp;
			}
			else
				continue;

			addExp.gateId = p->GateId();
			addExp.playerId = p->PlayerId();

			tcp_session s = RelationServiceMessageHandler::get_session(addExp.gateId);
			RelationServiceMessageHandler::send_result_message(s, &addExp, RelationServiceNS::NotifyAddExp::wCmd);
		}
	}

	return 0;
}

int FriendManager::HandleQueryExpNeededWhenLvlUpResult(tcp_session session, const RelationServiceNS::QueryExpNeededWhenLvlUpResult & res)
{
	ACE_TRACE("FriendManager::HandleQueryExpNeededWhenLvlUpResult");

	return 0;
}

int FriendManager::HandleKillPlayer(tcp_session session, const RelationServiceNS::GRAddEnemy & req)
{
	ACE_TRACE("FriendManager::HandleKillPlayer");

	Player * player = SocialRelationManager::instance()->GetPlayerByUiid(req.uiID);
	if (!player)
		return -1;

	Player * target = SocialRelationManager::instance()->GetPlayerByUiid(req.addtouiID);
	if (!target)
		return -1;

	if (player->IsFriend(*target) && target->IsFriend(*player))
	{
		Player::FriendInfo * info = player->GetFriendInfo(target->Uiid());
		if (!info)
			return -1;

		Player::FriendInfo * info2 = target->GetFriendInfo(player->Uiid());
		if (!info2)
			return -1;

		if (info->degree > 100)
		{
			info->degree -= 100;
			info2->degree = info->degree;
		}
		else
		{
			info->degree = 0;
			info2->degree = 0;
		}

		SendPlayerUpdateInfoNotify(*player, *target);
		SendPlayerUpdateInfoNotify(*target, *player);
		
	}
	return 0;
}

int FriendManager::HandleTweetFlagModify(tcp_session session, const RelationServiceNS::ModifyTweetReceiveFlag & req)
{
	ACE_TRACE("FriendManager::HandleTweetFlagModify");

	Player * player = SocialRelationManager::instance()->GetPlayerByTmpId(req.gateId, req.playerId);
	if (!player)
		return -1;

	player->SetTweetFlag(req.flag);

	return 0;
}

__SERVICE_SPACE_END_NS__

