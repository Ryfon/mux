﻿#ifndef __SERVICE_SPACE_RELATIONDB_H__
#define __SERVICE_SPACE_RELATIONDB_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceDBLib/db2o.h"
#include "../ServiceSpaceLib/utilib/split.h"
#include "../ServiceSpaceLib/utilib/vectorN.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "RelationServiceProtocol.h"
#include "Player.h"
#include "Guild.h"
#include "Union.h"
#include <sstream>

__SERVICE_SPACE_BEGIN_NS__

class SocialRelationDB
{
public:
	SocialRelationDB();

	~SocialRelationDB();

	int init(const std::string & str);

	void Load(const unsigned uiid, Player & player);
	void Load(const std::string & name, Player & player);
	void FirstSave(const Player & player);
	void Save(const Player & player);
	void Destroy(const Player & player);

	void Load(const unsigned id, Guild & guild);
	void Load(const Player & player, Guild & guild);
	bool FirstSave(const Guild & guild);
	bool Save(const Guild & guild);
	void Destroy(const Guild & guild);

	void Load(const std::string & guildName, Guild & guild);

	void SaveTweet(const Player & player, const Player::Tweet & tweet);
	void LoadTweets(Player & player);

	bool CheckGuildNameRepeat(const std::string & guildName);

	void LoadAll(std::vector<Guild> & vec);
	void LoadAll(const unsigned guildId, Guild::MemberList & vec);

	const unsigned GetGuildCount();

	const unsigned GetMaxGuildId();

	void GetGuildRank(std::vector<Guild> & vec, unsigned race);

	void Save(const unsigned id, unsigned event, const char * param1 = 0, const char * param2 = 0, const char * param3 = 0, const char * param4 = 0);
	const unsigned LoadGuildLog(const unsigned id, RelationServiceNS::UnionLogEntry (&vec)[5], const unsigned bookmark, bool & nextPage);

private:
	int init_i(const char* dbname,const char* host,const char* passwrd,unsigned int port,const char* user);

	std::auto_ptr<database<DB_MYSQL> > db_;

};

__SERVICE_SPACE_END_NS__

#endif