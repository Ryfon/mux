﻿#ifndef __SERVICE_SPACE_RELEATION_UNION_H__
#define __SERVICE_SPACE_RELEATION_UNION_H__

#include "../ServiceSpaceLib/StdHdr.h"
#include "RelationServiceProtocol.h"
#include "../ServiceSpaceLib/utilib/noncopyable.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "../ServiceSpaceDBLib/db2o.h"
#include <string>

__SERVICE_SPACE_BEGIN_NS__

inline const char * _guild()
{
	return "characterguild";
}

inline const char * _guild_name2()
{
	return "guildname";
}

inline const char * _guild_id()
{
	return "guildid";
}

inline const char * _guild_level()
{
	return "level";
}

inline const char * _guild_owner()
{
	return "owner";
}

inline const char * _guild_owner_race()
{
	return "ownerrace";
}

inline const char * _guild_active()
{
	return "active";
}

inline const char * _guild_prestige()
{
	return "prestige";
}

inline const char * _guild_bulletin()
{
	return "bulletin";
}

inline const char * _guild_other()
{
	return "otherinfo";
}

inline const char * _guild_log()
{
	return "guildlog";
}

inline const char * _guild_log_event()
{
	return "event";
}

inline const char * _guild_log_param1()
{
	return "param1";
}

inline const char * _guild_log_param2()
{
	return "param2";
}


inline const char * _guild_log_param3()
{
	return "param3";
}


inline const char * _guild_log_param4()
{
	return "param4";
}


inline const char * _guild_log_datetime()
{
	return "date_time";
}


typedef field<_guild_name2, std::string, false, true, true> GuildName2;		// unique
typedef field<_guild_id, unsigned, true> GuildID;
typedef field<_guild_id, unsigned, false, true> GuildID2;
typedef field<_guild_level, unsigned> GuildLevel;
typedef field<_guild_owner, unsigned, false, true, true> GuildOwner;		// unique
typedef field<_guild_active, unsigned> GuildActive;
typedef field<_guild_prestige, unsigned, false, true> GuildPrestige;
typedef field<_guild_owner_race, unsigned, false, true> GuildOwnerRace;
typedef field<_guild_bulletin, std::string> GuildBulletin;
typedef field<_guild_other, std::string> GuildOther;

//typedef object<_guild, GuildID, GuildName2, GuildLevel, GuildOwner, GuildActive, GuildOwnerRace,/*GuildPrestige,*/ GuildBulletin, GuildOther> GuildInfo;
typedef object<_guild, GuildID, GuildName2, GuildLevel, GuildOwner, GuildActive,GuildOwnerRace, GuildPrestige, GuildBulletin, GuildOther> GuildInfo;

typedef field<_guild_log_datetime, unsigned> GuildLogDatetime;
typedef field<_guild_log_event, unsigned> GuildLogEvent;
typedef field<_guild_log_param1, std::string> GuildLogParam1;
typedef field<_guild_log_param2, std::string> GuildLogParam2;
typedef field<_guild_log_param3, std::string> GuildLogParam3;
typedef field<_guild_log_param4, std::string> GuildLogParam4;
typedef object<_guild_log, GuildID2, GuildLogDatetime, GuildLogEvent, GuildLogParam1, GuildLogParam2, GuildLogParam3, GuildLogParam4> GuildLog;


__SERVICE_SPACE_END_NS__

#endif