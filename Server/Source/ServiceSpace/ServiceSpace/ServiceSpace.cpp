﻿#include <ace/OS_main.h>
#include <ace/Global_Macros.h>
#include <ace/Service_Config.h>
#include <ace/Proactor.h>
#include <ace/Get_Opt.h>
#include <ace/Thread_Manager.h>
#include <ace/DLL_Manager.h>
#include <ace/Thread_Manager.h>
#include <strstream>
#include <csignal>
#include <ace/Reactor.h>
#include "../ServiceSpaceLib/GetOpts.h"

__SERVICE_SPACE_USING_NS__

int ACE_TMAIN(int argc, ACE_TCHAR * argv[])
{
	ACE::init();
	const ACE_TCHAR * options = ACE_TEXT(":x:");
	GetOpts<ACE_TCHAR> opts(argc, argv);
	ACE_Get_Opt get_opt(argc, (ACE_TCHAR **)(opts.argv), options);
	int c = 0;
	unsigned expired = (unsigned)-1; 
	while((c = get_opt()) != EOF)
	{
		switch(c)
		{
		case 'x':
			std::strstream stream;
			stream << get_opt.optarg;
			stream >> expired;
			break;
		}
		
	}

	if (0 != ACE_Service_Config::open(argc, argv, ACE_DEFAULT_LOGGER_KEY, 0))
		return -1;

#ifdef SS_USE_PROACTOR

	if (expired == (unsigned)-1)
		ACE_Proactor::instance()->run_event_loop();
	else
	{
		ACE_Time_Value tv;
		tv.sec((time_t)expired);
		ACE_Proactor::instance()->run_event_loop(tv);
	}

#else

	if (expired == (unsigned)-1)
		ACE_Reactor::instance()->run_event_loop();
	else
	{
		ACE_Time_Value tv;
		tv.sec((time_t)expired);
		ACE_Reactor::instance()->run_event_loop(tv);
	}

#endif
	
	//ACE_Thread_Manager::instance()->kill_all(0);
	raise(SIGINT);
	return 0;
}