﻿#include "TcpSessionMgr.h"
#include "PkgUtil.h"
#include "Log.h"
#include "LogSvcProtocol.h"
using namespace LOG_SVC_NS;
#include "QuerySvcProtocol.h"
using namespace QUE_SVC_NS;

__SERVICE_SPACE_BEGIN_NS__

//UINT TcpSessionMgr::session_id_ = 0;

#ifdef WIN32
    #pragma warning(disable : 4311)
#endif//WIN32

void TcpSessionMgr::update_session( TcpSession* ts )
{
    ACE_GUARD (ACE_Token, _lock, session_lock_);
    UINT uid = (UINT)ts;
    session_map_[uid] = ts;
}

void TcpSessionMgr::remove_session( TcpSession* ts )
{
    ACE_GUARD (ACE_Token, _lock, session_lock_);
    UINT uid = (UINT)ts;
    std::map<UINT, TcpSession*>::iterator i = session_map_.find(uid);
    if ( i != session_map_.end() )
    {
        session_map_.erase(i);
    }
}

TcpSession* TcpSessionMgr::find_session( UINT id )
{
    TcpSession* ts = 0;
    std::map<UINT, TcpSession*>::iterator i = session_map_.find(id);    
    if ( i != session_map_.end() )
    {
        ts = i->second;
    }
    return ts;
}

ACE_Message_Block* TcpSessionMgr::build_pkg( UINT pkg_cmd, void* pkg, unsigned char mt )
{
    static QUE_SVC_NS::QuerySvcProtocol loqMk;
    static LOG_SVC_NS::LogSvcProtocol   losMk;

    ACE_Message_Block * message = new MessageBlock();
    message->wr_ptr(PkgUtil::PKG_HEAD_LEN);
    size_t size = -1;
    if ( PkgUtil::MT_LOS == mt )
    {
        size = losMk.EnCode(pkg_cmd, pkg, message->wr_ptr(), message->space());
    }
    else
    {
        size = loqMk.EnCode(pkg_cmd, pkg, message->wr_ptr(), message->space());
    }    
    if ( (UINT)(-1) == size  )
    {
        __ERR(ACE_TEXT("TSM::build_pkg|build pkg failed"));
        message->release();
        return 0;
    }

    // ahead size len
    message->wr_ptr(size);
    PkgUtil::build_pkg_head(message->rd_ptr(), (short)size + PkgUtil::PKG_HEAD_LEN, 0, pkg_cmd);
    return message;
}

int TcpSessionMgr::send_pkg( UINT session_id, UINT pkg_cmd, void* pkg, unsigned char mt )
{
    ACE_GUARD_RETURN (ACE_Token, _lock, session_lock_, -1);
    TcpSession* ts = find_session(session_id);
    if ( 0 == ts )
    {
        __ERR(ACE_TEXT("TSM::send_pkg|can't find session(%x)"), session_id);
        return -1;
    }

    ACE_Message_Block* mb = build_pkg(pkg_cmd, pkg, mt);
    if ( 0 == mb )
    {        
        return -1;
    }

    ts->post_asynch_write(mb);
    return 0;
}

__SERVICE_SPACE_END_NS__
