﻿#ifndef DBDDL_H
#define DBDDL_H

//== 数据结构定义

//== 表名定义
inline const char* table_name_account_login(void)       { return "account_login";}
inline const char* table_name_account_login_irc(void)   { return "account_login_irc";}
inline const char* table_name_account_logoff(void)      { return "account_logoff";}
inline const char* table_name_role_enter(void)          { return "role_enter";}
inline const char* table_name_role_exit(void)           { return "role_exit";}
inline const char* table_name_role_dead(void)           { return "role_dead";}
inline const char* table_name_role_levelup(void)        { return "role_levelup";}
inline const char* table_name_role_skill_levelup(void)  { return "role_skill_levelup";}
inline const char* table_name_role_sp_skill_levelup(void){ return "role_sp_skill_levelup";}
inline const char* table_name_role_item_own(void)       { return "role_item_own";}
inline const char* table_name_role_item_use(void)       { return "role_item_use";}
inline const char* table_name_role_item_drop(void)      { return "role_item_drop";}
inline const char* table_name_role_map_change(void)     { return "role_map_change";}
inline const char* table_name_role_relation_change(void){ return "role_relation_change";}
inline const char* table_name_role_quest_change(void)   { return "role_quest_change";}
inline const char* table_name_role_union_change(void)   { return "role_union_change";}
inline const char* table_name_role_trade(void)          { return "role_trade";}
inline const char* table_name_associated_item(void)     { return "associated_item";}
inline const char* table_name_role_sp_change(void)      { return "role_sp_change";}
inline const char* table_name_item_mass_change(void)    { return "item_mass_change";}
inline const char* table_name_item_level_change(void)   { return "item_level_change";}
inline const char* table_name_item_add_change(void)     { return "item_add_change";}
inline const char* table_name_item_bind_change(void)    { return "item_bind_change";}
inline const char* table_name_money_change(void)        { return "money_change";}
inline const char* table_name_team_change(void)         { return "team_change";}
inline const char* table_name_team_lifecycle(void)      { return "team_lifecycle";}
inline const char* table_name_pet_add(void)             { return "pet_add";}
inline const char* table_name_pet_levelup(void)         { return "pet_levelup";}
inline const char* table_name_pet_function_add(void)    { return "pet_function_add";}
inline const char* table_name_pet_state_change(void)    { return "pet_state_change";}
inline const char* table_name_cast_skill_sp(void)       { return "cast_skill_sp";}
inline const char* table_name_cast_skill_physics(void)  { return "cast_skill_physics";}
inline const char* table_name_cast_skill_magic(void)    { return "cast_skill_magic";}
inline const char* table_name_kill_player(void)         { return "kill_player";}
inline const char* table_name_role_relive(void)         { return "role_relive";}
inline const char* table_name_mail_exchange(void)       { return "mail_exchange";}
inline const char* table_name_shop_trade(void)          { return "shop_trade";}
inline const char* table_name_item_repair(void)         { return "item_repair";}
inline const char* table_name_market_trade(void)        { return "market_trade";}
inline const char* table_name_warehouse_open(void)      { return "warehouse_open";}
inline const char* table_name_warehouse_extend(void)    { return "warehouse_extend";}
inline const char* table_name_warehouse_item_transport(void)      { return "warehouse_item_transport";}
inline const char* table_name_warehouse_money_transport(void)     { return "warehouse_money_transport";}
inline const char* table_name_warehouse_lock_change(void)         { return "warehouse_lock_change";}
inline const char* table_name_warehouse_password_change(void)     { return "warehouse_password_change";}
inline const char* table_name_monster_dead(void)        { return "monster_dead";}
inline const char* table_name_real_instance_lifecycle(void)       { return "real_instance_lifecycle";}

inline const char* table_name_tix_list(void)            { return "tix_list";}
inline const char* table_name_tix_mod(void)             { return "tix_mod";}

//== 字段名定义
inline const char* filed_name_id(void)              {return "id";}
inline const char* filed_name_time(void)            {return "time";}
inline const char* filed_name_account(void)         {return "account";}
inline const char* filed_name_player_ip(void)       {return "ip";}
inline const char* filed_name_player_type(void)     {return "type";}
inline const char* filed_name_duration(void)        {return "duration";}
inline const char* filed_name_role_uuid(void)       {return "role_uuid";}
inline const char* filed_name_role_id(void)         {return "role_id";}
inline const char* filed_name_role_level(void)      {return "role_level";}
inline const char* filed_name_role_name(void)       {return "role_name";}
inline const char* filed_name_level_old(void)       {return "level_old";}
inline const char* filed_name_level_new(void)       {return "level_new";}
inline const char* filed_name_skill_id(void)        {return "skill_id";}
inline const char* filed_name_item_uuid(void)       {return "item_uuid";}
inline const char* filed_name_item_aid(void)        {return "item_aid";}
inline const char* filed_name_item_typeid(void)     {return "item_typeid";}
inline const char* filed_name_item_add(void)        {return "item_add";}
inline const char* filed_name_item_level(void)      {return "item_level";}
inline const char* filed_name_item_num(void)        {return "item_num";}
inline const char* filed_name_own_type(void)        {return "own_type";}
inline const char* filed_name_map_id_old(void)      {return "map_id_old";}
inline const char* filed_name_map_id_new(void)      {return "map_id_new";}
inline const char* filed_name_map_id(void)          {return "map_id";}
inline const char* filed_name_map_x(void)           {return "map_x";}
inline const char* filed_name_map_y(void)           {return "map_y";}
inline const char* filed_name_relation_id(void)     {return "relation_id";}
inline const char* filed_name_relation_type(void)   {return "relation_type";}
inline const char* filed_name_change_type(void)     {return "change_type";}
inline const char* filed_name_quest_uuid(void)      {return "quest_uuid";}
inline const char* filed_name_union_uuid(void)      {return "union_uuid";}
inline const char* filed_name_killer_type(void)     {return "killer_type";}
inline const char* filed_name_killer_id(void)       {return "killer_id";}
inline const char* filed_name_killer_level(void)    {return "killer_level";}
inline const char* filed_name_drop_exp(void)        {return "drop_exp";}
inline const char* filed_name_drop_item_aid(void)   {return "drop_item_aid";}
inline const char* filed_name_unit_a_uuid(void)     {return "unit_a_uuid";}
inline const char* filed_name_unit_a_level(void)    {return "unit_a_level";}
inline const char* filed_name_unit_a_item_aid(void) {return "unit_a_item_aid";}
inline const char* filed_name_unit_a_money(void)    {return "unit_a_money";}
inline const char* filed_name_unit_b_uuid(void)     {return "unit_b_uuid";}
inline const char* filed_name_unit_b_level(void)    {return "unit_b_level";}
inline const char* filed_name_unit_b_item_aid(void) {return "unit_b_item_aid";}
inline const char* filed_name_unit_b_money(void)    {return "unit_b_money";}
inline const char* filed_name_change_num(void)      {return "change_num";}
inline const char* filed_name_change_result(void)   {return "change_result";}
inline const char* filed_name_value_old(void)       {return "value_old";}
inline const char* filed_name_value_new(void)       {return "value_new";}
inline const char* filed_name_acction_type(void)    {return "acction_type";}
inline const char* filed_name_money_amout(void)     {return "money_amout";}
inline const char* filed_name_team_id(void)         {return "team_id";}
inline const char* filed_name_pet_id(void)          {return "pet_id";}
inline const char* filed_name_function_id(void)     {return "function_id";}
inline const char* filed_name_pet_state(void)       {return "pet_state";}
inline const char* filed_name_player_uuid(void)     {return "player_uuid";}
inline const char* filed_name_player_level(void)    {return "player_level";}
inline const char* filed_name_send_id(void)         {return "send_id";}
inline const char* filed_name_recv_id(void)         {return "recv_id";}
inline const char* filed_name_mail_id(void)         {return "mail_id";}
inline const char* filed_name_shop_id(void)         {return "shop_id";}
inline const char* filed_name_trade_type(void)      {return "trade_type";}
inline const char* filed_name_cost_money(void)      {return "cost_money";}
inline const char* filed_name_cost_diamond(void)    {return "cost_diamond";}
inline const char* filed_name_cost_credit(void)     {return "cost_credit";}
inline const char* filed_name_warehouse_id(void)    {return "warehouse_id";}
inline const char* filed_name_use_password(void)    {return "use_password";}
inline const char* filed_name_transport_type(void)  {return "transport_type";}
inline const char* filed_name_extend(void)          {return "extend";}
inline const char* filed_name_creater_id(void)      {return "creater_id";}
inline const char* filed_name_monster_id(void)      {return "monster_id";}
inline const char* filed_name_instance_id(void)     {return "instance_id";}
inline const char* filed_name_world_id(void)        {return "world_id";}
inline const char* filed_name_cycle_point(void)     {return "cycle_point";}

inline const char* filed_name_tid(void)             {return "tid";}
inline const char* filed_name_type(void)            {return "type";}
inline const char* filed_name_wait(void)            {return "wait";}
inline const char* filed_name_role(void)            {return "role";}
inline const char* filed_name_realm(void)           {return "realm";}
inline const char* filed_name_world(void)           {return "world";}
inline const char* filed_name_due(void)             {return "due";}
inline const char* filed_name_title(void)           {return "title";}
inline const char* filed_name_info(void)            {return "info";}
inline const char* filed_name_state(void)           {return "state";}
inline const char* filed_name_close_type(void)      {return "close_type";}
inline const char* filed_name_close_info(void)      {return "close_info";}
inline const char* filed_name_close_time(void)      {return "close_time";}

#endif

