﻿// -*- C++ -*-
#include "logic_task.h"
#include "Log.h"
#include "LogSvcProtocol.h"
using namespace LOG_SVC_NS;

#include "util.h"

int logic_task::open( void *args /*= 0*/ )
{
		args = args;
		
    // open logic dump file
    ACE_FILE_Connector con;
    if ( -1 == con.connect(file_,
                           ACE_FILE_Addr(ACE_TEXT("online")), 
                           0, 
                           ACE_Addr::sap_any, 
                           0, 
                           O_WRONLY|O_TRUNC|O_TEXT|O_CREAT, 
                           ACE_DEFAULT_FILE_PERMS) )
    {
        __ERR("logic_task::open|(%m)\n");
    }

    if ( -1 == activate(THR_NEW_LWP, 1) )
    {
        __ERR("logic_task::open|(%m)\n");
        return -1;
    }

    return 0;
}

int logic_task::svc( void )
{
    while(1)
    {
        ACE_Message_Block* mb = NULL;
        int result = getq(mb);
        if ( -1 == result )
        {
            __ERR("logic_task::getq|error(%m)\n");
        }

        try
        {
            tmsg* l = (tmsg*)mb->rd_ptr();
            if ( l && mb->size() >= MIN_MESSAGE_LEN )
            {
                handle_logic(l);
            }
            else
            {
                __ERR("logic_task::svc|bad message find!\n");
            }
            mb->release();
        }
        catch (...)
        {
            __ERR("logic_task::svc|raise exception!\n");
        }        
    }  

    return 0;
}

int logic_task::handle_logic( tmsg* msg )
{
    switch( msg->cmd )
    {
    case G_O_ACCOUNT_LOGIN:
        handle_account(true);
        break;
    case G_O_ACCOUNT_LOGOFF:
        handle_account(false);
        break;
    default:
        break;
    }

    return 0;
}

int logic_task::handle_account( bool on /*= true*/ )
{
    static UINT account_num = 0;
    if ( on )
    {
        account_num++;
    }
    else
    {
        if (account_num > 0)
        {
            account_num--;
        }
    }

    static ACE_TCHAR dummy[512];
    static ACE_TCHAR day_and_time[35];
    memset(dummy, 0, sizeof(dummy));
    memset(day_and_time, 0, sizeof(day_and_time));
    Util::time_s((UINT)ACE_OS::time(), day_and_time);
    ACE_OS::sprintf(dummy, ACE_TEXT("%s, %d\r\n"), day_and_time, account_num);

    //ACE_DEBUG((LM_DEBUG, ACE_TEXT("[DBG] %s\n"), dummy));
    __DBG("online account count:%4d\n", account_num);

    write_file((const char*)dummy, sizeof(dummy));

    return 0;
}

int logic_task::write_file( const char* data, int size )
{
    if ( ACE_INVALID_HANDLE == file_.get_handle() )
    {
        return -1;
    }

    file_.seek(0, SEEK_SET);
    if ( -1 == file_.send(data, size) )
    {
        __DBG("logic_task::write_file|(%m)\n");
        return -1;
    }
    return 0;
}

