﻿#include "log_task.h"
#include "DBDDL.h"
#include "DBAccess.h"
#include "Log.h"
#include "TcpSessionMgr.h"
#include "util.h"
#include "PkgUtil.h"
#include <map>
#include <sstream>

__SERVICE_SPACE_BEGIN_NS__

int log_task::set_db_params( const char* dbname, 
                             const char* host, 
                             const char* password, 
                             const char* port, 
                             const char* user )
{
    std::map<string, string> params;
    params["host"] = host;
    params["port"] = port;
    params["user"] = user;
    params["password"] = password;
    params["database"] = dbname;
    params["charset"] = "gb2312";
    
    connection_id_ = DB::Access::instance()->newConnection(params);
    if ( connection_id_ )
    {
        return 0;
    }
    else
    {
        return -1;
    }
}


int log_task::open( void *args /*= 0*/ )
{
		args = args;
    if ( -1 == activate(THR_NEW_LWP, 1) )
    {
        __ERR(ACE_TEXT("log_task::open|(%m)\n"));
        return -1;
    }

    return 0;
}

int log_task::svc( void )
{
    while(1)
    {
        ACE_Message_Block* mb = NULL;
        int result = getq(mb);
        if ( -1 == result )
        {
            __ERR("log_task::svc|error(%m)\n");
        }
        
        try
        {
            tmsg* l = (tmsg*)mb->rd_ptr();
            if ( l && mb->size() >= MIN_MESSAGE_LEN )
            {
                handle_log(l);
            }
            else
            {
                __ERR("log_task::svc|bad message find!\n");
            }
            mb->release();
        }
        catch (...)
        {
            __ERR("log_task::svc|raise exception!\n");
        }        
    }  

    return 0;
}

int log_task::handle_log( tmsg* lg )
{
    DB::Connection* conn = DB::Access::instance()->getConnection(connection_id_);
    if ( !conn )
    {
        __ERR("log_task::handle_log|can't find db connection(%d)\n", connection_id_);
        return -1;
    }

    vector<string> sql;
    if ( -1 == buildSQL(lg, sql) )
    {
        return -1;
    }

    size_t n = sql.size();
    for (size_t i = 0; i < n; ++i)
    {
        conn->execute(sql[i]);
    }

    // 处理一些需要回应的消息
    handle_rsp(lg);

    return 0;
}

int log_task::buildSQL( tmsg* lg, vector<string>& sql )
{
    int result;

    // 账号登录
    if ( G_O_ACCOUNT_LOGIN == lg->cmd )
    {
        GOAccountLogin* req = (GOAccountLogin*)lg->msg;
        result = buildAccoutLoginInsertSQL(req, sql);

        return result;
    }

    // 角色进入
    if ( G_O_ROLE_ENTER == lg->cmd )
    {
        GORoleEnter* req = (GORoleEnter*)lg->msg;
        result = buildRoleEnterInsertSQL(req, sql);

        return result; 
    }

    // 角色退出
    if ( G_O_ROLE_EXIT == lg->cmd )
    {
        GORoleExit* req = (GORoleExit*)lg->msg;
        result = buildRoleExitInsertSQL(req, sql);

        return result; 
   }

    // 账号登出
    if ( G_O_ACCOUNT_LOGOFF == lg->cmd )
    {
        GOAccountLogoff* req = (GOAccountLogoff*)lg->msg;
        result = buildAccoutLogoffInsertSQL(req, sql);

        return result; 
    }
    
    // 角色升级
    if ( G_O_ROLE_LEVELUP == lg->cmd )
    {
        GORoleLevelUP*  req = (GORoleLevelUP*)lg->msg;
        result = buildRoleLevelupInsertSQL(req, sql);

        return result; 
    }

    // 技能升级        
    if ( G_O_SKILL_LEVELUP == lg->cmd )
    {
        GOSkillLevelUP*  req = (GOSkillLevelUP*)lg->msg;
        result = buildSkillLevelupInsertSQL(req, sql);

        return result; 
    }

    // 角色死亡
    if ( G_O_ROLE_DEAD == lg->cmd )
    {
        GORoleDead* req = (GORoleDead*)lg->msg;
        result = buildRoleDeadInsertSQL(req, sql);

        return result; 
    }

    // 角色物品获得
    if ( G_O_ROLE_ITEM_OWN == lg->cmd )
    {
        GORoleItemOwn* req = (GORoleItemOwn*)lg->msg;
        result = buildRoleItemOwnInsertSQL(req, sql);

        return result; 
    }

    // 角色物品使用
    if ( G_O_ROLE_ITEM_USE == lg->cmd )
    {
        GORoleItemUse* req = (GORoleItemUse*)lg->msg;
        result = buildRoleItemUseInsertSQL(req, sql);

        return result;
    }

    // 角色物品丢弃
    if ( G_O_ROLE_ITEM_DROP == lg->cmd )
    {
        GORoleItemDrop* req = (GORoleItemDrop*)lg->msg;
        result = buildRoleItemDropInsertSQL(req, sql);

        return result;
    }

    // 角色地图变更
    if ( G_O_ROLE_MAP_CHANGE == lg->cmd )
    {
        GORoleMapChange* req = (GORoleMapChange*)lg->msg;
        result = buildRoleMapChangeInsertSQL(req, sql);

        return result;
    }

    // 角色关系变更
    if ( G_O_ROLE_RELATION_CHANGE == lg->cmd )
    {
        GORoleRelationChange* req = (GORoleRelationChange*)lg->msg;
        result = buildRoleRelationChangeInsertSQL(req, sql);

        return result;
    }

    // 角色任务变更
    if ( G_O_ROLE_QUEST_CHANGE == lg->cmd )
    {
        GORoleQuestChange* req = (GORoleQuestChange*)lg->msg;
        result = buildRoleQuestChangeInsertSQL(req, sql);

        return result;
    }

    // 角色公会变更
    if ( G_O_ROLE_UNION_CHANGE == lg->cmd )
    {
        GORoleUnionChange* req = (GORoleUnionChange*)lg->msg;
        result = buildRoleUnionChangeInsertSQL(req, sql);

        return result;
    }

    // 角色交易
    if ( G_O_ROLE_TRADE == lg->cmd )
    {
        GORoleTrade* req = (GORoleTrade*)lg->msg;
        result = buildRoleTradeInsertSQL(req, sql);

        return result;
    }

    // 角色SP豆改变
    if ( G_O_SP_CHANGE == lg->cmd )
    {
        GOSPChange* req = (GOSPChange*)lg->msg;
        result = buildSPChangeInsertSQL(req, sql);

        return result;
    }

    // 物品属性改变
    if ( G_O_ITEM_PROPERTY_CHANGE == lg->cmd )
    {
        GOItemPropertyChange* req = (GOItemPropertyChange*)lg->msg;
        result = buildItemPropertyChangeInsertSQL(req, sql);

        return result;
    }

    // 金钱改变
    if ( G_O_MONEY_CHANGE == lg->cmd )
    {
        GOMoneyChange* req = (GOMoneyChange*)lg->msg;
        result = buildMoneyChangeInsertSQL(req, sql);

        return result;
    }

    // 队伍改变
    if ( G_O_TEAM_CHANGE == lg->cmd )
    {
        GOTeamChange* req = (GOTeamChange*)lg->msg;
        result = buildTeamChangeInsertSQL(req, sql);

        return result;
    }

    // 增加宠物
    if ( G_O_PET_ADD == lg->cmd )
    {
        GOPetAdd* req = (GOPetAdd*)lg->msg;
        result = buildPetAddInsertSQL(req, sql);

        return result;
    }

    // 宠物升级
    if ( G_O_PET_LEVELUP == lg->cmd )
    {
        GOPetLevelup* req = (GOPetLevelup*)lg->msg;
        result = buildPetLevelupInsertSQL(req, sql);

        return result;
    }

    // 宠物功能增加
    if ( G_O_PET_FUNCTION_ADD == lg->cmd )
    {
        GOPetFunctionAdd* req = (GOPetFunctionAdd*)lg->msg;
        result = buildPetFunctionAddInsertSQL(req, sql);

        return result;
    }

    // 宠物状态改变
    if ( G_O_PET_STATE_CHANGE == lg->cmd )
    {
        GOPetStateChange* req = (GOPetStateChange*)lg->msg;
        result = buildPetStateChangeInsertSQL(req, sql);

        return result;
    }

    // 使用技能
    if ( G_O_USE_SKILL == lg->cmd )
    {
        GOUseSkill* req = (GOUseSkill*)lg->msg;
        result = buildUseSkillInsertSQL(req, sql);

        return result;
    }

    // 杀死玩家
    if ( G_O_KILL_PLAYER == lg->cmd )
    {
        GOKillPlayer* req = (GOKillPlayer*)lg->msg;
        result = buildKillPlayerInsertSQL(req, sql);

        return result;
    }

    // 复活
    if ( G_O_RELIVE == lg->cmd )
    {
        GORelive* req = (GORelive*)lg->msg;
        result = buildReliveInsertSQL(req, sql);

        return result;
    }

    // 发送邮件
    if ( G_O_SEND_MAIL == lg->cmd )
    {
        GOSendMail* req = (GOSendMail*)lg->msg;
        result = buildSendMailInsertSQL(req, sql);

        return result;
    }

    // 商店买卖
    if ( G_O_SHOP_TRADE == lg->cmd )
    {
        GOShopTrade* req = (GOShopTrade*)lg->msg;
        result = buildShopTradeInsertSQL(req, sql);

        return result;
    }

    // 物品修理
    if ( G_O_ITEM_REPAIR == lg->cmd )
    {
        GOItemRepair* req = (GOItemRepair*)lg->msg;
        result = buildItemRepairInsertSQL(req, sql);

        return result;
    }

    // 商城买卖
    if ( G_O_MARKET_TRADE == lg->cmd )
    {
        GOMarketTrade* req = (GOMarketTrade*)lg->msg;
        result = buildMarketTradeInsertSQL(req, sql);

        return result;
    }

    // 打开仓库
    if ( G_O_WAREHOUSE_OPEN == lg->cmd )
    {
        GOWarehouseOpen* req = (GOWarehouseOpen*)lg->msg;
        result = buildWarehouseOpenInsertSQL(req, sql);

        return result;
    }

    // 仓库物品存取
    if ( G_O_WAREHOUSE_ITEM_TRANSPORT == lg->cmd )
    {
        GOWarehouseItemTransport* req = (GOWarehouseItemTransport*)lg->msg;
        result = buildWarehouseItemTransportInsertSQL(req, sql);

        return result;
    }

    // 仓库金钱存取
    if ( G_O_WAREHOUSE_MONEY_TRANSPORT == lg->cmd )
    {
        GOWarehouseMoneyTransport* req = (GOWarehouseMoneyTransport*)lg->msg;
        result = buildWarehouseMoneyTransportInsertSQL(req, sql);

        return result;
    }

    // 仓库扩充
    if ( G_O_WAREHOUSE_EXTEND == lg->cmd )
    {
        GOWarehouseExtend* req = (GOWarehouseExtend*)lg->msg;
        result = buildWarehouseExtendInsertSQL(req, sql);

        return result;
    }

    // 仓库安全相关操作
    if ( G_O_WAREHOUSE_PRIVATEOP == lg->cmd )
    {
        GOWarehousePrivateOP* req = (GOWarehousePrivateOP*)lg->msg;
        result = buildWarehousePrivateOPInsertSQL(req, sql);

        return result;
    }

    // 怪物死亡
    if ( G_O_MONSTER_DEAD == lg->cmd )
    {
        GOMonsterDead* req = (GOMonsterDead*)lg->msg;
        result = buildMonsterDeadInsertSQL(req, sql);

        return result;
    }

    // 真副本生命周期
    if ( G_O_REAL_INSTANCE_LIFECYCLE == lg->cmd )
    {
        GORealInstanceLifeCycle* req = (GORealInstanceLifeCycle*)lg->msg;
        result = buildRealInstanceLifecycleInsertSQL(req, sql);

        return result;
    }

    // 新的诉求
    if ( G_O_TIX_NEW == lg->cmd )
    {
        GOTixNew* req = (GOTixNew*)lg->msg;
        result = buildTixNewInsertSQL(req, sql);

        return result;
    }

    // 诉求操作
    if ( G_O_TIX_MOD == lg->cmd )
    {
        GOTixMod* req = (GOTixMod*)lg->msg;
        result = buildTixModInsertSQL(req, sql);

        return result;
    }


    return -1;
}

int log_task::getAID( void )
{
    DB::Connection* conn = DB::Access::instance()->getConnection(connection_id_);
    if ( !conn )
    {
        __ERR("log_task::getAID|can't find connection(%d)\n", connection_id_);
        return -1;
    }

    string sql = "select seq('item_aid')";
    auto_ptr<sql::ResultSet> rset;
    if ( !conn->query(sql, rset) )
    {
        return 0;
    }

    if ( 0 == rset->rowsCount() )
    {
        return 0;
    }

    rset->first();
    return (rset->getInt(1));
}

int log_task::getTID( void )
{
    DB::Connection* conn = DB::Access::instance()->getConnection(connection_id_);
    if ( !conn )
    {
        __ERR("log_task::getTID|can't find connection(%d)\n", connection_id_);
        return -1;
    }

    string sql = "select seq('tix_id')";
    auto_ptr<sql::ResultSet> rset;
    if ( !conn->query(sql, rset) )
    {
        return 0;
    }

    if ( 0 == rset->rowsCount() )
    {
        return 0;
    }

    rset->first();
    return (rset->getInt(1));
}

int log_task::buildAccoutLoginInsertSQL( GOAccountLogin* req, vector<string>& sql )
{
    std::stringstream ss;
    char timestamp[35];

    /*
    INSERT INTO account_login/account_login_irc
    (time, account, ip) 
    VALUES ('time', 'account', 'ip');
    */

    // 获得表名
    const char* tname = 0;
    if ( PT_IRC == req->PlayType ) 
    {
        tname = table_name_account_login_irc();
    }
    else
    {
        tname = table_name_account_login();
    }

    req->Account[MAX_NAME_SIZE-1] = 0;
    req->PlayerIP[MAX_IP_SIZE-1] = 0;
    ss << "INSERT INTO `" << tname << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_account()     << "`, ";
    ss << "`" << filed_name_player_ip()   << "` ";
    ss << ") ";

    Util::time_s(req->Time, timestamp);
    ss << "VALUES (";
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp)) << ",";
    ss << Util::sql_str(req->Account, req->AccountSize)       << ",";
    ss << Util::sql_str(req->PlayerIP, req->PlayerIPSize)     << "";
    ss << ") ";

    sql.push_back(ss.str());

    return 0;
}

int log_task::buildRoleEnterInsertSQL( GORoleEnter* req, vector<string>& sql )
{
    /*
    INSERT INTO role_enter
    (time, role_uuid, role_level, account, role_name) 
    VALUES (time, role_uuid, role_level, 'account', 'role_name');
    */

    std::stringstream ss;
    char timestamp[35];

    req->Account[MAX_NAME_SIZE-1] = 0;
    req->RoleName[MAX_NAME_SIZE-1] = 0;
    ss << "INSERT INTO `" << table_name_role_enter() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_account()     << "`, ";
    ss << "`" << filed_name_role_name()   << "` ";
    ss << ") ";

    Util::time_s(req->Time, timestamp);
    ss << "VALUES (";
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp))   << ",";
    ss << (UINT)req->RoleID                                     << ",";
    ss << (UINT)req->RoleLevel                                  << ",";
    ss << Util::sql_str(req->Account, req->AccountSize)         << ",";
    ss << Util::sql_str(req->RoleName, req->RoleNameSize)       << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleExitInsertSQL( GORoleExit* req, vector<string>& sql )
{
    /*
    INSERT INTO role_exit
    (time, role_uuid, role_level, duration) 
    VALUES (time, role_uuid, role_level, duration);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_role_exit() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_duration()    << "` ";
    ss << ") ";

    Util::time_s(req->Time, timestamp);
    ss << "VALUES (";
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp))   << ",";
    ss << (UINT)req->RoleID                                     << ",";
    ss << (UINT)req->RoleLevel                                  << ",";
    ss << (UINT)req->Duration                                   << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;

}

int log_task::buildAccoutLogoffInsertSQL( GOAccountLogoff* req, vector<string>& sql )
{
    /*
    INSERT INTO account_logoff
    (time, account, duration) 
    VALUES ('time', 'account', duration);
    */

    std::stringstream ss;
    char timestamp[35];

    req->Account[MAX_NAME_SIZE-1] = 0;
    ss << "INSERT INTO `" << table_name_account_logoff() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_account()     << "`, ";
    ss << "`" << filed_name_duration()    << "`";
    ss << ") ";

    Util::time_s(req->Time, timestamp);
    ss << "VALUES (";
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp))   << ",";
    ss << Util::sql_str(req->Account, req->AccountSize)         << ",";
    ss << (UINT)req->Duration                                   << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleLevelupInsertSQL( GORoleLevelUP* req, vector<string>& sql )
{
    /*
    INSERT INTO role_levelup
    (time, role_uuid, level_old, level_new) 
    VALUES (time, role_uuid, level_old, level_new);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_role_levelup() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_level_old()   << "`, ";
    ss << "`" << filed_name_level_new()   << "` ";
    ss << ") ";

    Util::time_s(req->Time, timestamp);
    ss << "VALUES (";
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp))   << ",";
    ss << (UINT)req->RoleID                                     << ",";
    ss << (UINT)req->OldLevel                                   << ",";
    ss << (UINT)req->NewLevel                                   << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;

}

int log_task::buildSkillLevelupInsertSQL( GOSkillLevelUP* req, vector<string>& sql )
{
    /*
    INSERT INTO role_skill_levelup
    (time, role_uuid, role_level, level_old, level_new) 
    VALUES (time, role_uuid, role_level, level_old, level_new);
    */

    std::stringstream ss;
    char timestamp[35];

    const char* table_name = 0;
    if ( ST_SP == req->SkillType )
    {
        table_name = table_name_role_sp_skill_levelup();
    }
    else
    {
        table_name = table_name_role_skill_levelup();
    }

    ss << "INSERT INTO `" << table_name << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_level_old()   << "`, ";
    ss << "`" << filed_name_level_new()   << "` ";
    ss << ") ";

    Util::time_s(req->Time, timestamp);
    ss << "VALUES (";
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp))   << ",";
    ss << (UINT)req->RoleID                                     << ",";
    ss << (UINT)req->RoleLevel                                  << ",";
    ss << (UINT)req->OldLevel                                   << ",";
    ss << (UINT)req->NewLevel                                   << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleDeadInsertSQL( GORoleDead* req, vector<string>& sql )
{
    /*
    INSERT INTO role_dead
    (time, role_uuid, role_level, map_id, map_x, map_y, killer_type, killer_id, drop_exp, drop_item_aid) 
    VALUES (time, role_uuid, role_level, map_id, map_x, map_y, 'killer_type', killer_id, drop_exp, drop_item_aid);
    ------------------------------
    INSERT INTO associated_item
    (item_aid, item_uuid, item_typeid, item_add, item_level, item_num) 
    VALUES (item_aid, item_uuid, item_typeid, item_add, item_level, item_num);
    */
    std::stringstream ss;
    char timestamp[35];

    static const char* killerTypes[] = 
    {
        "系统",
        "NPC",
        "玩家"
    };
    const char* killerType = killerTypes[req->KillerType];

    UINT aid = 0;
    if ( req->DropItemCount > 0 )
    {
        aid = (UINT)getAID();
        if ( !aid )
        {
            __ERR("log_task::buildSQL|get item_aid failed\n");
            aid = 9999990;
        }
    }    

    ss << "INSERT INTO `" << table_name_role_dead() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_map_id()      << "`, ";
    ss << "`" << filed_name_map_x()       << "`, ";
    ss << "`" << filed_name_map_y()       << "`, ";
    ss << "`" << filed_name_killer_type() << "`, ";
    ss << "`" << filed_name_killer_id()   << "`, ";
    ss << "`" << filed_name_drop_exp()    << "`, ";
    ss << "`" << filed_name_drop_item_aid() << "` ";
    ss << ") ";

    Util::time_s(req->Time, timestamp);
    ss << "VALUES (";
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp))   << ",";
    ss << (UINT)req->RoleID                                     << ",";
    ss << (UINT)req->RoleLevel                                  << ",";
    ss << (UINT)req->MapID                                      << ",";
    ss << (UINT)req->MapX                                       << ",";
    ss << (UINT)req->MapY                                       << ",";
    ss << Util::sql_str(killerType, ACE_OS::strlen(killerType)) << ",";
    ss << (UINT)req->KillerID                                   << ",";
    ss << (UINT)req->DropExp                                    << ",";
    ss << (UINT)aid                                             << "";
    ss << ") ";

    sql.push_back(ss.str());

    for (int i = 0; i < (int)req->DropItemCount; ++i )
    {
        ItemInfo_i& ii = req->DropItem[i];
        if ( 0 == ii.UUID )
        {
            continue;
        }

        stringstream si;
        si << "INSERT INTO `" << table_name_associated_item() << "` ";
        si << "(";
        si << "`" << filed_name_item_aid()        << "`, ";
        si << "`" << filed_name_item_uuid()       << "`, ";
        si << "`" << filed_name_item_typeid()     << "`, ";
        si << "`" << filed_name_item_add()        << "`, ";
        si << "`" << filed_name_item_level()      << "`, ";
        si << "`" << filed_name_item_num()        << "` ";
        si << ") ";

        si << "VALUES (";            
        si << (UINT)aid                       << ",";
        si << (UINT)ii.UUID                   << ",";  
        si << (UINT)ii.TypeID                 << ","; 
        si << (UINT)ii.AddId                  << ","; 
        si << (UINT)ii.LevelUp                << ","; 
        si << (UINT)ii.Count                  << ""; 
        si << ") ";

        sql.push_back(si.str());
    }

    return 0;
}

int log_task::buildRoleItemOwnInsertSQL( GORoleItemOwn* req, vector<string>& sql )
{
    /*
    INSERT INTO role_item_own
    (time, role_uuid, role_level, item_uuid, item_typeid, item_add, item_level, item_num) 
    VALUES ('time', role_uuid, role_level, item_uuid, item_typeid, item_add, item_level, item_num);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_role_item_own() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_item_uuid()   << "`, ";
    ss << "`" << filed_name_item_typeid() << "`, ";
    ss << "`" << filed_name_item_add()    << "`, ";
    ss << "`" << filed_name_item_level()  << "`, ";
    ss << "`" << filed_name_item_num()    << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID               << ",";
    ss << (UINT)req->RoleLevel            << ",";
    ss << (UINT)req->Item.UUID            << ",";
    ss << (UINT)req->Item.TypeID          << ",";
    ss << (UINT)req->Item.AddId           << ",";
    ss << (UINT)req->Item.LevelUp         << ",";
    ss << (UINT)req->Item.Count           << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleItemUseInsertSQL( GORoleItemUse* req, vector<string>& sql )
{
    /*
    INSERT INTO role_item_use
    (time, role_uuid, role_level, item_uuid, item_typeid) 
    VALUES ('time', role_uuid, role_level, item_uuid, item_typeid);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_role_item_use() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_item_uuid()   << "`, ";
    ss << "`" << filed_name_item_typeid() << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID               << ",";
    ss << (UINT)req->RoleLevel            << ",";
    ss << (UINT)req->Item.UUID            << ",";
    ss << (UINT)req->Item.TypeID          << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleItemDropInsertSQL( GORoleItemDrop* req, vector<string>& sql )
{
    /*
    INSERT INTO role_item_drop
    (time, role_uuid, role_level, item_uuid, item_typeid) 
    VALUES ('time', role_uuid, role_level, item_uuid, item_typeid);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_role_item_drop() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_item_uuid()   << "`, ";
    ss << "`" << filed_name_item_typeid() << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID               << ",";
    ss << (UINT)req->RoleLevel            << ",";
    ss << (UINT)req->Item.UUID            << ",";
    ss << (UINT)req->Item.TypeID          << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleMapChangeInsertSQL( GORoleMapChange* req, vector<string>& sql )
{
    /*
    INSERT INTO role_map_change
    (time, role_uuid, role_level, map_id_old, map_id_new) 
    VALUES (time, role_uuid, role_level, map_id_old, map_id_new);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_role_map_change() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_map_id_old()   << "`, ";
    ss << "`" << filed_name_map_id_new()   << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID               << ",";
    ss << (UINT)req->RoleLevel            << ",";
    ss << (UINT)req->OldMapID             << ",";
    ss << (UINT)req->NewMapID             << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleRelationChangeInsertSQL( GORoleRelationChange* req, vector<string>& sql )
{
    /*
    INSERT INTO role_relation_change
    (time, role_uuid, role_level, relation_id, relation_type, change_type) 
    VALUES (time, role_uuid, role_level, relation_id, relation_type, change_type);
    */

    std::stringstream ss;
    char timestamp[35];

    static const char* relationNames[] = 
    {
        "好友",
        "仇人",
        "黑名单"
    };
    static const char* changeNames[] = 
    {
        "增加",
        "删除"
    };

    const char* relationType = relationNames[req->RelationType];
    const char* changeType = changeNames[req->ChangeType];

    ss << "INSERT INTO `" << table_name_role_relation_change() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_relation_id() << "`, ";
    ss << "`" << filed_name_relation_type() << "`, ";
    ss << "`" << filed_name_change_type()   << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID               << ",";
    ss << (UINT)req->RoleLevel            << ",";
    ss << (UINT)req->RelationID           << ",";
    ss << "'" << relationType   << "'"    << ",";
    ss << "'" << changeType     << "'"    << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleQuestChangeInsertSQL( GORoleQuestChange* req, vector<string>& sql )
{
    /*
    INSERT INTO role_quest_change
    (time, role_uuid, role_level, quest_uuid, change_type) 
    VALUES (time, role_uuid, role_level, quest_uuid, change_type);
    */

    std::stringstream ss;
    char timestamp[35];

    static const char* questChangeTypes[] = 
    {
        "未知", // 0
        "接受", // 1
        "达成", // 2
        "交还", // 3
        "未知", // 4
        "失败", // 5
        "放弃", // 6
    };
    const char* changeType = questChangeTypes[req->ChangeType];

    ss << "INSERT INTO `" << table_name_role_quest_change() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_quest_uuid()  << "`, ";
    ss << "`" << filed_name_change_type() << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID               << ",";
    ss << (UINT)req->RoleLevel            << ",";
    ss << (UINT)req->QuestID              << ",";
    ss << "'" <<  changeType << "'"       << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleUnionChangeInsertSQL( GORoleUnionChange* req, vector<string>& sql )
{
    /*
    INSERT INTO role_union_change
    (time, role_uuid, role_level, union_uuid, change_type) 
    VALUES (time, role_uuid, role_level, union_uuid, change_type);
    */

    static const char* changeNames[] = 
    {
        "加入",
        "离开"
    };
    const char* changeType = changeNames[req->ChangeType];

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_role_union_change() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()        << "`, ";
    ss << "`" << filed_name_role_uuid()   << "`, ";
    ss << "`" << filed_name_role_level()  << "`, ";
    ss << "`" << filed_name_union_uuid()  << "`, ";
    ss << "`" << filed_name_change_type() << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID               << ",";
    ss << (UINT)req->RoleLevel            << ",";
    ss << (UINT)req->UnionID              << ",";
    ss << "'" << changeType << "'"        << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRoleTradeInsertSQL( GORoleTrade* req, vector<string>& sql )
{
    /* 
    INSERT INTO game_log.role_trade
    (time, unit_a_uuid, unit_a_level, uint_a_money, unit_a_item_aid, unit_b_uuid, unit_b_level, uint_b_money, unit_b_item_aid) 
    VALUES ('time', unit_a_uuid, unit_a_level, uint_a_money, unit_a_item_aid, unit_b_uuid, unit_b_level, uint_b_money, unit_b_item_aid);
    ------------------------------
    INSERT INTO associated_item
    (item_aid, item_uuid, item_mod, item_add, item_level, item_num) 
    VALUES (item_aid, item_uuid, item_mod, item_add, item_level, item_num);
    */

    std::stringstream ss;
    char timestamp[35];    

    UINT unit_a_aid = 0;
    if ( req->UnitA.ItemCount > 0 )
    {
        unit_a_aid = (UINT)getAID();
        if ( !unit_a_aid )
        {
            __ERR("log_task::buildSQL|get item_aid failed\n");
            unit_a_aid = 9999991;
        }
    }
    

    UINT unit_b_aid = 0;
    if ( req->UnitB.ItemCount > 0 )
    {
        unit_b_aid = (UINT)getAID();
        if ( !unit_b_aid )
        {
            __ERR("log_task::buildSQL|get item_aid failed\n");
            unit_b_aid = 9999992;
        }
    }    

    ss << "INSERT INTO `" << table_name_role_trade() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_unit_a_uuid()       << "`, ";
    ss << "`" << filed_name_unit_a_level()      << "`, ";
    ss << "`" << filed_name_unit_a_money()      << "`, ";
    ss << "`" << filed_name_unit_a_item_aid()   << "`, ";
    ss << "`" << filed_name_unit_b_uuid()       << "`, ";
    ss << "`" << filed_name_unit_b_level()      << "`, ";
    ss << "`" << filed_name_unit_b_money()      << "`, ";
    ss << "`" << filed_name_unit_b_item_aid()   << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->UnitA.RoleID         << ",";
    ss << (UINT)req->UnitA.RoleLevel      << ",";
    ss << (UINT)req->UnitA.Money          << ",";
    ss << (UINT)unit_a_aid                << ",";
    ss << (UINT)req->UnitB.RoleID         << ",";
    ss << (UINT)req->UnitB.RoleLevel      << ",";
    ss << (UINT)req->UnitB.Money          << ",";
    ss << (UINT)unit_b_aid                << "";
    ss << ") ";
    sql.push_back(ss.str());

    // unit_a_item
    for (int i = 0; i < (int)req->UnitA.ItemCount; ++i )
    {
        ItemInfo_i& ii = req->UnitA.Item[i];
        if ( 0 == ii.UUID )
        {
            continue;
        }

        stringstream si;
        si << "INSERT INTO `" << table_name_associated_item() << "` ";
        si << "(";
        si << "`" << filed_name_item_aid()        << "`, ";
        si << "`" << filed_name_item_uuid()       << "`, ";
        si << "`" << filed_name_item_typeid()        << "`, ";
        si << "`" << filed_name_item_add()        << "`, ";
        si << "`" << filed_name_item_level()      << "`, ";
        si << "`" << filed_name_item_num()        << "` ";
        si << ") ";

        si << "VALUES (";            
        si << (UINT)unit_a_aid                << ",";
        si << (UINT)ii.UUID                   << ",";  
        si << (UINT)ii.TypeID                 << ","; 
        si << (UINT)ii.AddId                  << ","; 
        si << (UINT)ii.LevelUp                << ","; 
        si << (UINT)ii.Count                  << ""; 
        si << ") ";

        sql.push_back(si.str());
    }

    // unit_b_item
    for (int i = 0; i < (int)req->UnitB.ItemCount; ++i )
    {
        ItemInfo_i& ii = req->UnitB.Item[i];
        if ( 0 == ii.UUID )
        {
            continue;
        }

        stringstream si;
        si << "INSERT INTO `" << table_name_associated_item() << "` ";
        si << "(";
        si << "`" << filed_name_item_aid()        << "`, ";
        si << "`" << filed_name_item_uuid()       << "`, ";
        si << "`" << filed_name_item_typeid()     << "`, ";
        si << "`" << filed_name_item_add()        << "`, ";
        si << "`" << filed_name_item_level()      << "`, ";
        si << "`" << filed_name_item_num()        << "` ";
        si << ") ";

        si << "VALUES (";            
        si << (UINT)unit_b_aid                << ",";
        si << (UINT)ii.UUID                   << ",";  
        si << (UINT)ii.TypeID                 << ","; 
        si << (UINT)ii.AddId                  << ","; 
        si << (UINT)ii.LevelUp                << ","; 
        si << (UINT)ii.Count                  << ""; 
        si << ") ";

        sql.push_back(si.str());
    }

    return 0;
}

int log_task::buildSPChangeInsertSQL( GOSPChange* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.role_sp_change
    (role_uuid, change_type, change_num) 
    VALUES (role_uuid, 'change_type', change_num);
    */

    std::stringstream ss;
    char timestamp[35];

    static const char* changeTypes[] = 
    {
        "增加",
        "消耗",
    };
    const char* changeType = changeTypes[req->ChangeType];

    ss << "INSERT INTO `" << table_name_role_sp_change() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_change_type()       << "`, ";
    ss << "`" << filed_name_change_num()        << "`";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << "'" << changeType  << "'"                        << ",";
    ss << (UINT)req->ChangeNum                             << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildItemPropertyChangeInsertSQL( GOItemPropertyChange* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.item_mass_change
    (time, role_uuid, role_level, item_uuid, item_typeid, change_result, value_old, value_new) 
    VALUES ('time', role_uuid, role_level, item_uuid, item_typeid, change_result, value_old, value_new);
    */

    std::stringstream ss;
    char timestamp[35];

    const char* table_name = 0;
    int change_result = 0;
    if ( req->NewValue > req->OldValue )
    {
        change_result = 1;
    }
    switch (req->PropertyType)
    {
    case IPT_MASS:
        table_name = table_name_item_mass_change();        
        break;
    case IPT_LEVEL:
        table_name = table_name_item_level_change();
        break;
    case IPT_ADD:
        table_name = table_name_item_add_change();
        change_result = 1;    
        break;
    default:
        table_name = table_name_item_bind_change();
        change_result = 1;
        break;
    }

    ss << "INSERT INTO `" << table_name << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_role_level()        << "`, ";
    ss << "`" << filed_name_item_uuid()         << "`, ";
    ss << "`" << filed_name_item_typeid()       << "`, ";
    ss << "`" << filed_name_change_result()     << "`, ";
    ss << "`" << filed_name_value_old()         << "`, ";
    ss << "`" << filed_name_value_new()         << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->RoleLevel                             << ",";
    ss << (UINT)req->ItemUUID                              << ",";
    ss << (UINT)req->ItemTypeID                            << ",";
    ss << (UINT)change_result                              << ",";
    ss << (UINT)req->OldValue                              << ",";
    ss << (UINT)req->NewValue                              << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildMoneyChangeInsertSQL( GOMoneyChange* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.money_change
    (time, role_uuid, change_type, money_amout, acction_type) 
    VALUES ('time', role_uuid, change_type, money_amout, 'acction_type');
    */
    
    static const char* changeTypes[] = 
    {
        "增加",
        "减少",
    };
    const char* changeType = changeTypes[req->ChangeType];

    static const char* actionTypes[] = 
    {
        "商店买卖",
        "玩家交易",
        "物品修理",
        "捡取包裹",
        "任务奖励",
    };
    const char* actionType = actionTypes[req->ReasonType];

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_money_change() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_change_type()       << "`, ";
    ss << "`" << filed_name_money_amout()       << "`, ";
    ss << "`" << filed_name_acction_type()      << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << "'" << changeType   << "'"                       << ",";
    ss << (UINT)req->MoneyCount                            << ",";
    ss << "'" << actionType   << "'"                       << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildTeamChangeInsertSQL( GOTeamChange* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.team_change
    (time, role_uuid, role_level, team_id, change_type) 
    VALUES ('time', role_uuid, role_level, team_id, 'change_type');
    */

    static const char* changeTypes[] = 
    {
        "创建",
        "解散",
        "加入",
        "离开",
    };
    const char* changeType = changeTypes[req->ChangeType];
    const char* table_name = table_name_team_lifecycle();
    const char* filed_name = filed_name_cycle_point();
    if ( req->ChangeType >= TCT_ENTER )
    {
        table_name = table_name_team_change();
        filed_name = filed_name_change_type();
    }

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_role_level()        << "`, ";
    ss << "`" << filed_name_team_id()           << "`, ";
    ss << "`" << filed_name                     << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->RoleLevel                             << ",";
    ss << (UINT)req->TeamID                                << ",";
    ss << "'" << changeType   << "'"                       << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;

}

int log_task::buildPetAddInsertSQL( GOPetAdd* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.pet_add
    (time, role_uuid, role_level, pet_id) 
    VALUES ('time', role_uuid, role_level, pet_id);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_pet_add() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_role_level()        << "`, ";
    ss << "`" << filed_name_pet_id()            << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->RoleLevel                             << ",";
    ss << (UINT)req->PetID                                 << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildPetLevelupInsertSQL( GOPetLevelup* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.pet_levelup
    (time, role_uuid, role_level, pet_id, level_old, level_new) 
    VALUES ('time', role_uuid, role_level, pet_id, level_old, level_new);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_pet_levelup() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_role_level()        << "`, ";
    ss << "`" << filed_name_pet_id()            << "`,  ";
    ss << "`" << filed_name_level_old()         << "`,  ";
    ss << "`" << filed_name_level_new()         << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->RoleLevel                             << ",";
    ss << (UINT)req->PetID                                 << ",";
    ss << (UINT)req->OldLevel                              << ",";
    ss << (UINT)req->NewLevel                              << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildPetFunctionAddInsertSQL( GOPetFunctionAdd* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.pet_function_add
    (time, role_uuid, role_level, pet_id, function_id) 
    VALUES ('time', role_uuid, role_level, pet_id, function_id)
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_pet_function_add() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_role_level()        << "`, ";
    ss << "`" << filed_name_pet_id()            << "`,  ";
    ss << "`" << filed_name_function_id()       << "`  ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->RoleLevel                             << ",";
    ss << (UINT)req->PetID                                 << ",";
    ss << (UINT)req->FunctionID                            << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildPetStateChangeInsertSQL( GOPetStateChange* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.pet_state_change
    (time, role_uuid, pet_id, pet_state) 
    VALUES ('time', role_uuid, pet_id, 'pet_state');
    */
    static const char* petStates[] =
    {
        "收起",
        "唤出",
    };
    const char* petState = petStates[req->StateType];

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_pet_state_change() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_pet_id()            << "`,  ";
    ss << "`" << filed_name_pet_state()         << "`  ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->PetID                                 << ",";
    ss << "'" << petState                           << "'" << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildUseSkillInsertSQL( GOUseSkill* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.cast_skill_physics
    (time, role_uuid, skill_id) 
    VALUES ('time', role_uuid, skill_id);
    */

    const char* table_name = 0;
    switch (req->SkillType)
    {
    case ST_SP:
        table_name = table_name_cast_skill_sp();
        break;
    case ST_PHYSICS:
        table_name = table_name_cast_skill_physics();
        break;
    default:
        table_name = table_name_cast_skill_magic();
        break;
    }

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_skill_id()          << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->SkillID                               << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildKillPlayerInsertSQL( GOKillPlayer* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.kill_player
    (time, role_uuid, role_level, player_uuid, player_level) 
    VALUES ('time', role_uuid, role_level, player_uuid, player_level);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_kill_player() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_role_level()        << "`, ";
    ss << "`" << filed_name_player_uuid()       << "`, ";
    ss << "`" << filed_name_player_level()      << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->RoleLevel                             << ",";
    ss << (UINT)req->PlayerID                              << ",";
    ss << (UINT)req->PlayerLevel                           << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildReliveInsertSQL( GORelive* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.role_relive
    (time, role_uuid, role_level, map_id, map_x, map_y) 
    VALUES ('time', role_uuid, role_level, map_id, map_x, map_y);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_role_relive() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_role_level()        << "`, ";
    ss << "`" << filed_name_map_id()            << "`, ";
    ss << "`" << filed_name_map_x()             << "`, ";
    ss << "`" << filed_name_map_y()             << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << (UINT)req->RoleID                                << ",";
    ss << (UINT)req->RoleLevel                             << ",";
    ss << (UINT)req->MapID                                 << ",";
    ss << (UINT)req->MapX                                  << ",";
    ss << (UINT)req->MapY                                  << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildSendMailInsertSQL( GOSendMail* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.mail_exchange
    (time, send_id, recv_id, mail_id) 
    VALUES ('time', send_id, recv_id, mail_id);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_mail_exchange() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_send_id()           << "`, ";
    ss << "`" << filed_name_recv_id()           << "`, ";
    ss << "`" << filed_name_mail_id()           << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->SendRoleID                                  << ",";
    ss << req->RecvRoleID                                  << ",";
    ss << req->MailID                                      << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildShopTradeInsertSQL( GOShopTrade* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.shop_trade
    (time, role_uuid, shop_id, trade_type, money_amout, item_typeid, item_num) 
    VALUES ('time', role_uuid, shop_id, 'trade_type', money_amout, item_typeid, item_num);
    */

    static const char* tradeTypes[] = 
    {
        "卖出",
        "购入",
    };
    const char* tradeType = tradeTypes[req->SaleBuy];

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_shop_trade() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_shop_id()           << "`, ";
    ss << "`" << filed_name_trade_type()        << "`, ";
    ss << "`" << filed_name_money_amout()       << "`, ";
    ss << "`" << filed_name_item_typeid()       << "`, ";
    ss << "`" << filed_name_item_num()          << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->RoleID                                      << ",";
    ss << req->ShopID                                      << ",";
    ss << "'" << tradeType                          << "'" << ",";
    ss << req->Money                                       << ",";
    ss << req->ItemTypeID                                  << ",";
    ss << req->ItemNum                                     << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildItemRepairInsertSQL( GOItemRepair* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.item_repair
    (time, role_uuid, shop_id, cost_money, item_uuid, item_typeid) 
    VALUES ('time', role_uuid, shop_id, cost_money, item_uuid, item_typeid);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_item_repair() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_shop_id()           << "`, ";
    ss << "`" << filed_name_cost_money()        << "`, ";
    ss << "`" << filed_name_item_uuid()         << "`, ";
    ss << "`" << filed_name_item_typeid()       << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->RoleID                                      << ",";
    ss << req->ShopID                                      << ",";    
    ss << req->Money                                       << ",";
    ss << req->ItemUUID                                    << ",";
    ss << req->ItemTypeID                                  << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildMarketTradeInsertSQL( GOMarketTrade* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.market_trade
    (time, role_uuid, item_uuid, item_num, cost_diamond, cost_credit) 
    VALUES ('time', role_uuid, item_uuid, item_num, cost_diamond, cost_credit);
    */
    
    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_market_trade() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_item_uuid()         << "`, ";
    ss << "`" << filed_name_item_num()          << "`, ";
    ss << "`" << filed_name_cost_diamond()      << "`, ";
    ss << "`" << filed_name_cost_credit()       << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->RoleID                                      << ",";
    ss << req->ItemTypeID                                  << ",";    
    ss << req->ItemNum                                     << ",";
    ss << req->Diamond                                     << ",";
    ss << req->Credit                                      << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildWarehouseOpenInsertSQL( GOWarehouseOpen* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.warehouse_open
    (time, role_uuid, warehouse_id, use_password, map_id, map_x, map_y) 
    VALUES ('time', role_uuid, warehouse_id, use_password, map_id, map_x, map_y);
    */

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_warehouse_open() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_warehouse_id()      << "`, ";
    ss << "`" << filed_name_use_password()      << "`, ";
    ss << "`" << filed_name_map_id()            << "`, ";
    ss << "`" << filed_name_map_x()             << "`, ";
    ss << "`" << filed_name_map_y()             << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->RoleID                                      << ",";
    ss << req->WarehouseID                                 << ",";    
    ss << (UINT)req->HasPassword                           << ",";
    ss << req->MapID                                       << ",";
    ss << req->MapX                                        << ",";
    ss << req->MapY                                        << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;

}

int log_task::buildWarehouseItemTransportInsertSQL( GOWarehouseItemTransport* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.warehouse_item_transport
    (time, role_uuid, warehouse_id, transport_type, item_uuid, item_typeid, item_num) 
    VALUES ('time', role_uuid, warehouse_id, 'transport_type', item_uuid, item_typeid, item_num);
    */

    static const char* transportTypes[] = 
    {
        "存入",
        "取出",
    };
    const char* transportType = transportTypes[req->TransportType];

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_warehouse_item_transport() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_warehouse_id()      << "`, ";
    ss << "`" << filed_name_transport_type()    << "`, ";
    ss << "`" << filed_name_item_uuid()         << "`, ";
    ss << "`" << filed_name_item_typeid()       << "`, ";
    ss << "`" << filed_name_item_num()          << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->RoleID                                      << ",";
    ss << req->WarehouseID                                 << ",";
    ss << "'" << transportType                      << "'" << ",";    
    ss << req->ItemUUID                                    << ",";
    ss << req->ItemTypeID                                  << ",";    
    ss << req->ItemNum                                     << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildWarehouseMoneyTransportInsertSQL( GOWarehouseMoneyTransport* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.warehouse_money_transport
    (time, role_uuid, warehouse_id, transport_type, money_amout) 
    VALUES ('time', role_uuid, warehouse_id, 'transport_type', money_amout);
    */

    static const char* transportTypes[] = 
    {
        "存入",
        "取出",
    };
    const char* transportType = transportTypes[req->TransportType];

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_warehouse_money_transport() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_warehouse_id()      << "`, ";
    ss << "`" << filed_name_transport_type()    << "`, ";
    ss << "`" << filed_name_money_amout()       << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->RoleID                                      << ",";
    ss << req->WarehouseID                                 << ",";
    ss << "'" << transportType                      << "'" << ",";    
    ss << req->Money                                       << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildWarehouseExtendInsertSQL( GOWarehouseExtend* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.warehouse_extend
    (time, role_uuid, warehouse_id, extend) 
    VALUES ('time', role_uuid, warehouse_id, extend);
    */
    
    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_warehouse_extend() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_uuid()         << "`, ";
    ss << "`" << filed_name_warehouse_id()      << "`, ";
    ss << "`" << filed_name_extend()            << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->RoleID                                      << ",";
    ss << req->WarehouseID                                 << ",";
    ss << req->SlotNum                                     << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildWarehousePrivateOPInsertSQL( GOWarehousePrivateOP* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.warehouse_lock_change
    (id, time, role_id, warehouse_id, change_result) 
    VALUES (id, 'time', role_id, warehouse_id, change_result);
    */

    const char* table_name = 0;
    if ( WPT_CHANGE_PASSWORD == req->PrivateType )
    {
        table_name = table_name_warehouse_password_change();
    }
    else
    {
        table_name = table_name_warehouse_lock_change();
    }

    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_role_id()           << "`, ";
    ss << "`" << filed_name_warehouse_id()      << "`, ";
    ss << "`" << filed_name_change_result()     << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->RoleID                                      << ",";
    ss << req->WarehouseID                                 << ",";
    ss << (UINT)req->Result                                << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildMonsterDeadInsertSQL( GOMonsterDead* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.monster_dead
    (time, map_id, creater_id, monster_id, killer_id, killer_level) 
    VALUES ('time', map_id, creater_id, monster_id, killer_id, killer_level);
    */


    std::stringstream ss;
    char timestamp[35];

    ss << "INSERT INTO `" << table_name_monster_dead() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_map_id()            << "`, ";
    ss << "`" << filed_name_creater_id()        << "`, ";
    ss << "`" << filed_name_monster_id()        << "`, ";
    ss << "`" << filed_name_killer_id()         << "`, ";
    ss << "`" << filed_name_killer_level()      << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->MapID                                       << ",";
    ss << req->CreaterID                                   << ",";
    ss << req->MonsterID                                   << ",";
    ss << req->KillerID                                    << ",";
    ss << req->KillerLevel                                 << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildRealInstanceLifecycleInsertSQL( GORealInstanceLifeCycle* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.real_instance_lifecycle
    (time, instance_id, world_id, cycle_point) 
    VALUES ('time', instance_id, world_id, 'cycle_point');
    */

    std::stringstream ss;
    char timestamp[35];

    static const char* cyclePoints[] = 
    {
        "创建",
        "销毁",
    };
    const char* cyclePoint = cyclePoints[req->CyclePoint];


    ss << "INSERT INTO `" << table_name_real_instance_lifecycle() << "` ";
    ss << "(";
    ss << "`" << filed_name_time()              << "`, ";
    ss << "`" << filed_name_instance_id()       << "`, ";
    ss << "`" << filed_name_world_id()          << "`, ";
    ss << "`" << filed_name_cycle_point()       << "` ";
    ss << ") ";

    ss << "VALUES (";
    ss << "'" << Util::time_s(req->Time, timestamp) << "'" << ",";
    ss << req->InstanceID                                  << ",";
    ss << req->WorldID                                     << ",";
    ss << "'" << cyclePoint                         << "'" << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;

}

int log_task::buildTixNewInsertSQL( GOTixNew* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.tix_list
           (tid,  time,   type,   account,  role_id,  role,   realm,   world,   title,   info, ) 
    VALUES (tid, 'time', 'type', 'account', role_id, 'role', 'realm', 'world', 'title', 'info' );
    */

    std::stringstream ss;
    char timestamp[35];

    UINT tid = 0;
    tid = (UINT)getTID();
    if ( !tid )
    {
        __ERR("log_task::buildSQL|get tix_id failed\n");

        tid = req->TixInfo.ID > 0 ? req->TixInfo.ID : 9999993;
    }

    if ( 9999993 != tid )
    {
        // 给处理回应用
        req->TixInfo.ID = tid;
    }


    TixNew_i& tS = req->TixInfo;

    ss << "INSERT INTO `" << table_name_tix_list()      << "` ";
    ss << "(";
    ss << "`" << filed_name_time()                      << "`, ";
    ss << "`" << filed_name_tid()                       << "`, ";
    ss << "`" << filed_name_type()                      << "`, ";
    ss << "`" << filed_name_account()                   << "`, ";
    ss << "`" << filed_name_role_id()                   << "`, ";
    ss << "`" << filed_name_role()                      << "`, ";
    ss << "`" << filed_name_realm()                     << "`, ";
    ss << "`" << filed_name_world()                     << "`, ";
    ss << "`" << filed_name_title()                     << "`, ";
    ss << "`" << filed_name_info()                      << "` ";
    ss << ") ";

    ss << "VALUES (";
    Util::time_s(tS.Time, timestamp);
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp))       << ",";
    ss << UINT(tid)                                                 << ",";
    ss << Util::sql_str(tS.TypeString, tS.TypeStringSize)           << ",";
    ss << Util::sql_str(tS.AccountName, tS.AccountNameSize)         << ",";
    ss << UINT(tS.RoleID)                                           << ",";
    ss << Util::sql_str(tS.RoleName, tS.RoleNameSize)               << ",";
    ss << Util::sql_str(tS.RealmName, tS.RealmNameSize)             << ",";
    ss << Util::sql_str(tS.WorldName, tS.WorldNameSize)             << ",";
    ss << Util::sql_str(tS.Title, tS.TitleSize)                     << ",";
    ss << Util::sql_str(tS.Info, tS.InfoSize)                       << "";
    ss << ") ";

    sql.push_back(ss.str());
    return 0;
}

int log_task::buildTixModInsertSQL( GOTixMod* req, vector<string>& sql )
{
    /*
    INSERT INTO game_log.tix_mod
           ( time,  tid,  type,   due,   close_type,   close_time,   close_info) 
    VALUES ('time', tid, 'type', 'due', 'close_type', 'close_time', 'close_info');
    */

    std::stringstream ss;
    char timestamp[35];

    const char* typeName = PkgUtil::tix_mod_name(req->ModType);
    // ------ 插入操作记录 -------------
    ss << "INSERT INTO `" << table_name_tix_mod()       << "` ";
    ss << "(";
    ss << "`" << filed_name_time()                      << "`, ";
    ss << "`" << filed_name_tid()                       << "`, ";
    ss << "`" << filed_name_type()                      << "`, ";
    ss << "`" << filed_name_due()                       << "`, ";
    ss << "`" << filed_name_close_type()                << "`, ";
    ss << "`" << filed_name_close_info()                << "` ";
    ss << ") ";

    ss << "VALUES (";
    Util::time_s(UINT(ACE_OS::time()), timestamp);
    ss << Util::sql_str(timestamp, ACE_OS::strlen(timestamp)) << ",";
    ss << UINT(req->TixID)                                                  << ",";
    ss << Util::sql_str(typeName, ACE_OS::strlen(typeName))                 << ",";
    ss << Util::sql_str(req->DueName, req->DueNameSize)                     << ",";
    ss << Util::sql_str(req->CloseTypeString, req->CloseTypeStringSize)     << ",";
    ss << Util::sql_str(req->CloseInfo, req->CloseInfoSize)                 << "";
    ss << ") ";

    sql.push_back(ss.str());
    // ------ ------------- -------------

    // ------ 更新诉求 ------------------
    /*
    UPDATE game_log.tix_list 
    SET wait = wait, due = 'due', state = 'state', close_type = 'close_type', close_info = 'close_info', close_time = 'close_time' 
    WHERE tid = tid
     */
    if (    req->CloseTypeStringSize    > 1
         || req->CloseInfoSize          > 1
         || req->DueNameSize            > 1 )
    {
        ss.str("");
        bool notFirst = false;
        ss << "UPDATE `" << table_name_tix_list()       << "` ";
        ss << "SET "                                    << "  ";
        if ( req->ModType >= TMT_INIT )
        {
            if ( notFirst )
            {
                ss << "," ;
            }
            notFirst = true;
            
            const char* stateName = PkgUtil::tix_state_name(req->ModType);
            ss << "`" << filed_name_state() << "`=" << Util::sql_str(stateName, ACE_OS::strlen(stateName)) << " ";
        }
        if ( req->DueNameSize > 1 )
        {
            if ( notFirst )
            {
                ss << "," ;
            }
            notFirst = true;

            ss << "`" << filed_name_due() << "`=" << Util::sql_str(req->DueName, req->DueNameSize) << " ";
        }
        if ( req->CloseTypeStringSize > 1 )
        {
            if ( notFirst )
            {
                ss << "," ;
            }
            notFirst = true;

            ss << "`" << filed_name_close_type() << "`=" << Util::sql_str(req->CloseTypeString, req->CloseTypeStringSize) << " ";
        }
        if ( req->CloseInfoSize > 1 )
        {
            if ( notFirst )
            {
                ss << "," ;
            }
            notFirst = true;

            ss << "`" << filed_name_close_info() << "`=" << Util::sql_str(req->CloseInfo, req->CloseInfoSize) << " ";
        }
        if ( req->ModType == TMT_PROCESS )
        {
            // wait=TIME_TO_SEC(TIMEDIFF(NOW(), `time`))
            if ( notFirst )
            {
                ss << "," ;
            }
            notFirst = true;

            ss << "`" << filed_name_wait() << "`=" << "TIME_TO_SEC(TIMEDIFF(NOW(), `" << filed_name_time() << "`))" << " ";
        }
        if ( req->ModType == TMT_CLOSE )
        {
            if ( notFirst )
            {
                ss << "," ;
            }
            notFirst = true;

            Util::time_s(UINT(ACE_OS::time()), timestamp);
            ss << "`" << filed_name_close_time() << "`=" << Util::sql_str(timestamp, ACE_OS::strlen(timestamp)) << " ";
        }
        ss << "WHERE `"<< filed_name_tid() << "`=" << UINT(req->TixID) << "  ";

        sql.push_back(ss.str());
    }
    // ------ -------- ------------------

    return 0;
}

int log_task::handle_rsp( tmsg* lg )
{
    switch(lg->cmd)
    {
    case G_O_TIX_NEW:
        return (handleTixNewRsp(lg));
    case G_O_TIX_MOD:
        return (handleTixModRsp(lg));
    default:
        return 0;
    }
}

int log_task::handleTixNewRsp( tmsg* lg )
{
    GOTixNew*   req = (GOTixNew*)lg->msg;

    // 创建数据包
    OGTixNewRsp rsp;
    memset(&rsp, 0, sizeof(rsp));
    rsp.LocalTixID = req->LocalTixID;
    rsp.TixID      = req->TixInfo.ID;
    rsp.ErrorNo    = 0; // 目前无法传递处理结果

    // 打包并发送
    TSMGR->send_pkg(lg->id, rsp.wCmd, &rsp, PkgUtil::MT_LOS);

    return 0;
}

int log_task::handleTixModRsp( tmsg* lg )
{
    GOTixMod*   req = (GOTixMod*)lg->msg;

    // 创建数据包
    OGTixModRsp rsp;
    memset(&rsp, 0, sizeof(rsp));
    rsp.LocalTixID = req->LocalTixID;
    rsp.TixID      = req->TixID;
    rsp.ModType    = req->ModType;
    rsp.ErrorNo    = 0; // 目前无法传递处理结果
    // 打包并发送
    TSMGR->send_pkg(lg->id, rsp.wCmd, &rsp, PkgUtil::MT_LOS);

    return 0;
}

__SERVICE_SPACE_END_NS__

