﻿#include "query_task.h"
#include "TcpSessionMgr.h"
#include "DBDDL.h"
#include "Log.h"
#include "util.h"
#include "PkgUtil.h"
#include "QuerySvcProtocol.h"
using namespace QUE_SVC_NS;

#include <sstream>

__SERVICE_SPACE_BEGIN_NS__

int query_task::open( void *args /*= 0*/ )
{
	args = args;
    if ( -1 == activate(THR_NEW_LWP, 1) )
    {
        __ERR(ACE_TEXT("query_task::open|(%m)\n"));
        return -1;
    }

    return 0;
}

int query_task::set_db_params( const char* dbname, 
                               const char* host, 
                               const char* password, 
                               const char* port, 
                               const char* user )
{
    std::map<string, string> params;
    params["host"] = host;
    params["port"] = port;
    params["user"] = user;
    params["password"] = password;
    params["database"] = dbname;
    params["charset"] = "gb2312";

    connection_id_ = DB::Access::instance()->newConnection(params);
    if ( connection_id_ )
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int query_task::svc( void )
{
    while(1)
    {
        ACE_Message_Block* mb = NULL;
        int result = getq(mb);
        if ( -1 == result )
        {
            __ERR("query_task::getq|(%m)\n");
        }

        try
        {
            tmsg* l = (tmsg*)mb->rd_ptr();
            if ( l && mb->size() >= MIN_MESSAGE_LEN )
            {
                handle_query(l);
            }
            else
            {
                __ERR("query_task::svc|bad message find!\n");
            }
            mb->release();
        }
        catch (...)
        {
            __ERR("query_task::svc|raise exception!\n");
        }
    }  

    return 0;
}

int query_task::handle_query( tmsg* lg )
{
    DB::Connection* conn = DB::Access::instance()->getConnection(connection_id_);
    if (!conn)
    {
        __ERR("query_task::handle_query|can't find db connection(%d)\n", connection_id_);
        return -1;
    }

    string sql;
    if (-1 == build_sql(lg, sql))
    {
        return -1;
    }

    auto_ptr<sql::ResultSet> rs;
    if (!conn->query(sql, rs))
    {
        return -1;
    }

    handle_result(lg, rs.get());

    return 0;
}

int query_task::build_sql( tmsg* lg, std::string& sql )
{
    if ( T_O_QUERY_TIX == lg->cmd )
    {
        build_tix_select_sql(lg->msg, sql);
        return 0;
    }

    return -1;
}

int query_task::handle_result( tmsg* lg, sql::ResultSet* rs )
{
    if ( T_O_QUERY_TIX == lg->cmd )
    {
        handle_query_tix_result(lg, rs);
        return 0;
    }        
    
    return -1;
}

int query_task::build_tix_select_sql( void* pkg, std::string& sql )
{
    /*
    SELECT id, time, type, wait, account, role, realm, world, accepter, title, info, state, close_type, close_info, close_time 
    FROM tix_list;
    */

    std::stringstream ss;
    std::stringstream sw;
    char timestamp[35];
    bool has_cond;

    TOQueryTix* cond = (TOQueryTix*)pkg;

    ss << "SELECT ";
    ss << "`" << filed_name_tid()       << "`,";
    ss << "`" << filed_name_type()      << "`,"; 
    ss << "`" << filed_name_time()      << "`,";    
    ss << "`" << filed_name_wait()      << "`,"; 
    ss << "`" << filed_name_account()   << "`,"; 
    ss << "`" << filed_name_role()      << "`,"; 
    ss << "`" << filed_name_world()     << "`,"; 
    ss << "`" << filed_name_realm()     << "`,"; 
    ss << "`" << filed_name_due()       << "`,"; 
    ss << "`" << filed_name_title()     << "`,"; 
    ss << "`" << filed_name_info()      << "`,"; 
    ss << "`" << filed_name_state()     << "`,"; 
    ss << "`" << filed_name_close_type()<< "`,"; 
    ss << "`" << filed_name_close_time()<< "`,"; 
    ss << "`" << filed_name_close_info()<< "`,"; 
    ss << "`" << filed_name_role_id()   << "`"; 

    ss << "FROM `" << table_name_tix_list() << "` ";

    has_cond = false;
    if ( cond->AccountNameSize > 0 )
    {
        if ( has_cond )
        {
            sw << "AND ";
        }
        cond->AccountName[cond->AccountNameSize-1] = 0;
        sw << "`" << filed_name_account() << "` = ";
        sw << "'" << cond->AccountName << "' ";

        has_cond = true;
    }

    if ( cond->RoleNameSize > 0 )
    {
        if ( has_cond )
        {
            sw << "AND ";
        }
        cond->RoleName[cond->RoleNameSize-1] = 0;
        sw << "`" << filed_name_role() << "` = ";
        sw << "'" << cond->RoleName << "' ";

        has_cond = true;
    }

    const char* stateName = PkgUtil::tix_state_name(cond->State);
    if ( ACE_OS::strlen(stateName) > 1 )
    {
        if ( has_cond )
        {
            sw << "AND ";
        }
        cond->RoleName[cond->RoleNameSize-1] = 0;
        sw << "`" << filed_name_state() << "` = ";
        sw << "'" << stateName << "' ";

        has_cond = true;
    }

    if ( cond->TimeLowLimit > 0 )
    {
        if ( has_cond )
        {
            sw << "AND ";
        }
        cond->RoleName[cond->RoleNameSize-1] = 0;
        sw << "`" << filed_name_time() << "` >= ";
        sw << "'" << Util::time_s(cond->TimeLowLimit, timestamp) << "' ";

        has_cond = true;
    }    

    if ( cond->TimeHighLimit > 0 )
    {
        if ( has_cond )
        {
            sw << "AND ";
        }
        cond->RoleName[cond->RoleNameSize-1] = 0;
        sw << "`" << filed_name_time() << "` <= ";
        sw << "'" << Util::time_s(cond->TimeHighLimit, timestamp) << "' ";

        has_cond = true;
    }    

    if ( has_cond )
    {
        ss << "WHERE " << sw.str();
    }

    sql = ss.str();
    return 0;
}

int query_task::handle_query_tix_result( tmsg* lg, sql::ResultSet* rs )
{    
    static OTQueryTixRsp rsp;
    TOQueryTix* req = (TOQueryTix*)lg->msg;

    size_t rows = rs->rowsCount();
    size_t current = 0;
    memset(&rsp, 0, sizeof(rsp));
    rsp.ID = req->ID;
    while(rs->next())
    {
        ++current;
        memset(&rsp, 0, sizeof(rsp));
        if ( current < rows )
        {
            rsp.More = QRT_EIN;
        }
        else
        {
            rsp.More = QRS_END;
        }

        TixInfo_i& t = rsp.RefInfo;
        t.ID                = UINT(rs->getInt(filed_name_tid()));
        t.TypeStringSize    = UINT(rs->getString(filed_name_type()).size()+1);
        memcpy(t.TypeString, rs->getString(filed_name_type()).c_str(), t.TypeStringSize);
        t.Time              = Util::time_u(rs->getString(filed_name_time()).c_str(), rs->getString(filed_name_time()).size());
        t.Wait              = UINT(rs->getInt(filed_name_wait()));
        t.AccountNameSize   = UINT(rs->getString(filed_name_account()).size()+1);
        memcpy(t.AccountName, rs->getString(filed_name_account()).c_str(), t.AccountNameSize);        
        t.RoleNameSize      = UINT(rs->getString(filed_name_role()).size()+1);
        memcpy(t.RoleName, rs->getString(filed_name_role()).c_str(), t.RoleNameSize);        
        t.WorldNameSize     = UINT(rs->getString(filed_name_world()).size()+1);
        memcpy(t.WorldName, rs->getString(filed_name_world()).c_str(), t.WorldNameSize);
        t.RealmNameSize     = UINT(rs->getString(filed_name_realm()).size()+1);
        memcpy(t.RealmName, rs->getString(filed_name_realm()).c_str(), t.RealmNameSize);
        t.DueNameSize       = UINT(rs->getString(filed_name_due()).size()+1);
        memcpy(t.DueName, rs->getString(filed_name_due()).c_str(), t.DueNameSize);
        t.TitleSize         = UINT(rs->getString(filed_name_title()).size()+1); 
        memcpy(t.Title, rs->getString(filed_name_title()).c_str(), t.TitleSize);
        t.InfoSize          = UINT(rs->getString(filed_name_info()).size()+1); 
        memcpy(t.Info, rs->getString(filed_name_info()).c_str(), t.InfoSize);
        t.State             = PkgUtil::tix_state_id(rs->getString(filed_name_state()).c_str());
        t.CloseTypeStringSize = UINT(rs->getString(filed_name_close_type()).size()+1); 
        memcpy(t.CloseTypeString, rs->getString(filed_name_close_type()).c_str(), t.CloseTypeStringSize);
        t.CloseTime         = Util::time_u(rs->getString(filed_name_close_time()).c_str(), rs->getString(filed_name_close_time()).size());
        t.CloseInfoSize     = UINT(rs->getString(filed_name_close_info()).size()+1); 
        memcpy(t.CloseInfo, rs->getString(filed_name_close_info()).c_str(), t.CloseInfoSize);
        t.RoleID            = UINT(rs->getInt(filed_name_role_id()));

        TSMGR->send_pkg(lg->id, rsp.wCmd, &rsp, PkgUtil::MT_LOQ);
    }

    if ( 0 == rows )
    {
        rsp.More = QRS_EOF;
        TSMGR->send_pkg(lg->id, rsp.wCmd, &rsp, PkgUtil::MT_LOQ);
    }

    return 0;
}

__SERVICE_SPACE_END_NS__

