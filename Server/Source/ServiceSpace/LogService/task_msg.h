﻿// -*- C++ -*-

//=============================================================================
/**
 *  @file    task_msg.h
 *
 *  $Version 0.01 2009-10-30 11:07:35
 *
 *  @author chenshaobin <chenshaobin@corp.the9.com>
 */
//=============================================================================


#ifndef TASK_MSG_H
#define TASK_MSG_H

#define MAX_MESSAGE_LEN  1024
#define MIN_MESSAGE_LEN  32
struct tmsg
{
    unsigned int    id;
    unsigned int    cmd;
    unsigned int    typ;
    char    msg[MAX_MESSAGE_LEN];
};

#endif

