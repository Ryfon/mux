﻿#ifndef			__QuerySvcProtocol__
#define			__QuerySvcProtocol__
#include "ParserTool.h"
#include "map"

namespace QUE_SVC_NS
{
    #pragma pack(push, 1)
	//Enums
	typedef	enum
	{
		TST_UNKOWN   	=	0,         	//未知状态
		TST_INIT     	=	1,         	//原始状态
		TST_SUBSCRIBE	=	2,         	//申请中状态
		TST_APPLY    	=	3,         	//已受理状态
		TST_PROCESS  	=	4,         	//处理中状态
		TST_CLOSE    	=	5,         	//已关闭状态
	}ETixStateType;

	typedef	enum
	{
		QRT_EIN      	=	0,         	//查询中    （本次数据有效，有后续数据）
		QRS_END      	=	1,         	//查询完成  （本次数据有效，无后续数据）
		QRS_EOF      	=	2,         	//查询完成  （本次数据无效）
	}EQueryResultType;


	//Defines
	#define	MAX_NAME_SIZE          	 	20 	//名字最长字节数
	#define	MAX_TIX_TITLE_SIZE     	 	64 	//诉求标题最大长度
	#define	MAX_TYPE_SIZE          	 	16 	//类别文字最大长度
	#define	MAX_SRV_NAME_SIZE      	 	40 	//服务器名最大长度
	#define	MAX_TIX_INFO_SIZE      	 	300	//诉求内容最大长度
	#define	MAX_TIX_CLOSE_INFO_SIZE	 	150	//关闭说明最大长度

	//Typedefs

	//Types
	template<bool> struct TypeMaxBytes;
	template<> struct TypeMaxBytes<true> {};
	#define	QUERYSVCPROTOCOL_MAXBYTES_CHECK(typename) \
	struct type##typename \
	{\
		TypeMaxBytes< sizeof(typename) < 1109 > struct_larger_##typename;\
	}

	struct TixInfo_i
	{
		UINT     	ID;                              	//唯一编号
		UINT     	TypeStringSize;                  	//种类文字长度
		CHAR     	TypeString[MAX_TYPE_SIZE];         	//种类文字
		UINT     	Time;                            	//产生时刻
		UINT     	Wait;                            	//等待时间（秒）
		UINT     	AccountNameSize;                 	//账号名长度
		CHAR     	AccountName[MAX_NAME_SIZE];        	//发起诉求的账号名
		UINT     	RoleID;                          	//角色ID
		UINT     	RoleNameSize;                    	//角色名长度
		CHAR     	RoleName[MAX_NAME_SIZE];           	//发起诉求的角色名
		UINT     	DueNameSize;                     	//受理者名称长度
		CHAR     	DueName[MAX_NAME_SIZE];            	//受理者名称
		UINT     	TitleSize;                       	//标题长度
		CHAR     	Title[MAX_TIX_TITLE_SIZE];         	//诉求标题
		UINT     	InfoSize;                        	//内容长度
		CHAR     	Info[MAX_TIX_INFO_SIZE];           	//诉求内容
		BYTE     	State;                           	//状态（ETixStateType）
		UINT     	CloseTypeStringSize;             	//关闭种类文字长度
		CHAR     	CloseTypeString[MAX_TYPE_SIZE];    	//关闭种类文字
		UINT     	CloseTime;                       	//关闭时刻
		UINT     	CloseInfoSize;                   	//关闭说明长度
		CHAR     	CloseInfo[MAX_TIX_CLOSE_INFO_SIZE];	//关闭说明
		UINT     	RealmNameSize;                   	//游戏区名长度
		CHAR     	RealmName[MAX_SRV_NAME_SIZE];      	//游戏区名
		UINT     	WorldNameSize;                   	//游戏世界名长度
		CHAR     	WorldName[MAX_SRV_NAME_SIZE];      	//游戏世界名
	};

	struct TOQueryTix
	{
		static const USHORT	wCmd = 0x2c01;

		UINT     	ID;                              	//检索唯一标记（在应答消息中返回）
		UINT     	AccountNameSize;                 	//账号名长度
		CHAR     	AccountName[MAX_NAME_SIZE];        	//账号名（如果长度>0则作为检索条件）
		UINT     	RoleNameSize;                    	//角色名长度
		CHAR     	RoleName[MAX_NAME_SIZE];           	//角色名（如果长度>0则作为检索条件）
		BYTE     	State;                           	//诉求状态（参照ETixStateType，如果属于合法枚举范围则作为检索条件）
		UINT     	TimeLowLimit;                    	//最小产生时间（如果>0则作为检索条件）
		UINT     	TimeHighLimit;                   	//最大产生时间（如果>0则作为检索条件）
	};
	QUERYSVCPROTOCOL_MAXBYTES_CHECK(TOQueryTix);

	struct OTQueryTixRsp
	{
		static const USHORT	wCmd = 0x7c01;

		UINT     	ID;                              	//检索唯一标记（对应检索请求中的ID）
		BYTE     	More;                            	//结果状态（参照EQueryResultType）
		TixInfo_i	RefInfo;                         	//诉求信息
	};
	QUERYSVCPROTOCOL_MAXBYTES_CHECK(OTQueryTixRsp);


	//Messages
	typedef enum Message_id_type
	{
		T_O_QUERY_TIX    	=	0x2c01,	//TOQueryTix
		O_T_QUERY_TIX_RSP	=	0x7c01,	//OTQueryTixRsp
	};
    #pragma pack(pop)

	//Class Data
	class QuerySvcProtocol;
	typedef size_t (QuerySvcProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (QuerySvcProtocol::*DeCodeFunc)(void* pData);

	class 
	QuerySvcProtocol
	{
	public:
		QuerySvcProtocol();
		~QuerySvcProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__TixInfo_i(void* pData);
		size_t	DeCode__TixInfo_i(void* pData);

		size_t	EnCode__TOQueryTix(void* pData);
		size_t	DeCode__TOQueryTix(void* pData);

		size_t	EnCode__OTQueryTixRsp(void* pData);
		size_t	DeCode__OTQueryTixRsp(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool                    m_kPackage;
	};
}
#endif			//__QuerySvcProtocol__
