﻿// -*- C++ -*-

//=============================================================================
/**
 *  @file    DBAccess.h
 *
 *  $Version 0.01 2009-10-27 15:31:18
 *
 *  @author chenshaobin <chenshaobin@corp.the9.com>
 */
//=============================================================================

#ifndef DBACCESS_H
#define DBACCESS_H

// MS compatible compilers support #pragma once
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include "DBConnection.h"
#include <vector>
using namespace std;

namespace DB
{

class Access
{
public:

    //== 接口
    /**
    * 获得本对象的实例对象指针    
    */
    static Access* instance(void);   
    /**
    * 默认构造/析构
    * 
    */
    Access(void);    
    ~Access();

    /**
    * 创建一个新的数据库连接
    * @params:数据库连接参数
    * @return: =0 创建失败
    *          >0 连接ID
    */
    int newConnection(const map<string, string> params);
    /**
    * 根据连接ID获得数据库连接对象指针
    * @id: DB连接ID
    * @return: = NULL 找不到指定的DB连接
    *         != NULL DB连接对象指针
    */
    Connection* getConnection(int id);

private:
    // 当前连接ID
    int                connection_id_;
    // 连接集合
    vector<Connection*> connections_;
};

}


#endif//DBACCESS_H

