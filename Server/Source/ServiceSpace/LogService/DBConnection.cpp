﻿#include "DBConnection.h"
#include "Log.h"

namespace DB
{

Connection::Connection( int id )
{
    id_ = id;
    conn_ = 0;
}


int Connection::open( void )
{
    try
    {
        if (conn_)
        {
            conn_->close();

            delete conn_;
            conn_ = 0;
        }

        auto_ptr<sql::Connection> conn( get_driver_instance()->connect(host_, user_, password_) );
        if ( 0 == conn.get() )
        {
            __ERR("Connection::open|sql::driver::connect = null\n");
            return false;
        }

        if ( charset_.length() > 0 )
        {
            //conn->setClientOption(string("characterSetResults"), charset_.c_str());
            string sql("SET NAMES ");
            sql.append("'");
            sql.append(charset_.c_str());
            sql.append("'");
            auto_ptr<sql::Statement> stmt(conn->createStatement());            
            stmt->execute(sql);

            /**
            * SET NAMES 'gb2312' 等价于
            * SET character_set_client=gb2312;
            * SET character_set_results=gb2312;
            * SET character_set_connection=gb2312;
            */
        }

        if ( database_.length() > 0 )
        {
            conn->setSchema( database_ );
        }

        conn_ = conn.release();
    }
    catch(exception& e)
    {
        __ERR("Connection::open|exception(%s)\n", e.what());
        return false;
    }

    return true;
}

int Connection::open( const map<string, string>& params )
{
    host_ = "";
    user_ = "";
    password_ = "";
    charset_ = "";
    database_ = "";

    for (map<string, string>::const_iterator i = params.begin(); i != params.end(); ++i)
    {
        if (!i->first.compare("host"))
        {
            host_ = i->second;
            continue;
        }
        if (!i->first.compare("user"))
        {
            user_ = i->second;
            continue;
        }
        if (!i->first.compare("password"))
        {
            password_ = i->second;
            continue;
        }
        if (!i->first.compare("charset"))
        {
            charset_ = i->second;
            continue;
        }    
        if (!i->first.compare("database"))
        {
            database_ = i->second;
            continue;
        }
    }

    return (open());
}

int Connection::ping( void )
{
    int result = false;

    if ( conn_ )
    {
        try
        {
            string sql = "select 1 limit 1";
            auto_ptr<sql::Statement> stmt(conn_->createStatement());
            auto_ptr<sql::ResultSet> rset(stmt->executeQuery(sql));

            result = true;
        }
        catch (exception& e)
        {
            __ERR("Connection::ping|exception(%s)\n", e.what());
            result = false;
        }
    }

    // try reopen
    if ( !result )
    {
        result = open();
    }

    return result;
}

int Connection::execute( const string& sql )
{
    if ( !ping() )
    {
        return false;
    }

    try
    {
        auto_ptr<sql::Statement> stmt(conn_->createStatement());
        stmt->execute(sql);
        return ((int)stmt->getUpdateCount());
    }
    catch (exception& e)
    {
        __ERR("Connection::execute|exception(%s)\n", e.what());
        return false;
    }
}

int Connection::query( const string& sql, auto_ptr<sql::ResultSet>& rset )
{
    if ( !ping() )
    {
        return false;
    }

    try
    {
        auto_ptr<sql::Statement> stmt(conn_->createStatement());
        rset.reset(stmt->executeQuery(sql));

        return true;
    }
    catch (exception& e)
    {
        __ERR("Connection::query|exception(%s)\n", e.what());
        return false;
    }
}

}//namespace DB

