-- MySQL dump 10.13  Distrib 5.1.30, for Win32 (ia32)
--
-- Host: localhost    Database: game_log
-- ------------------------------------------------------
-- Server version	5.1.30-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Database for `game_log`
--
DROP DATABASE IF EXISTS `game_log`;
CREATE DATABASE `game_log` default charset 'gb2312' COLLATE 'gb2312_chinese_ci';
USE `game_log`;

--
-- Table structure for table `account_login`
--

DROP TABLE IF EXISTS `account_login`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `account_login` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `account` varchar(20) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `account_login`
--

LOCK TABLES `account_login` WRITE;
/*!40000 ALTER TABLE `account_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_login_irc`
--

DROP TABLE IF EXISTS `account_login_irc`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `account_login_irc` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `account` varchar(20) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `account_login_irc`
--

LOCK TABLES `account_login_irc` WRITE;
/*!40000 ALTER TABLE `account_login_irc` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_login_irc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_logoff`
--

DROP TABLE IF EXISTS `account_logoff`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `account_logoff` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `account` varchar(20) DEFAULT NULL,
  `duration` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `account_logoff`
--

LOCK TABLES `account_logoff` WRITE;
/*!40000 ALTER TABLE `account_logoff` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_logoff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `associated_item`
--

DROP TABLE IF EXISTS `associated_item`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `associated_item` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `item_aid` int(10) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  `item_add` smallint(6) unsigned DEFAULT NULL,
  `item_level` tinyint(4) unsigned DEFAULT NULL,
  `item_num` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `associated_item`
--

LOCK TABLES `associated_item` WRITE;
/*!40000 ALTER TABLE `associated_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `associated_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cast_skill_magic`
--

DROP TABLE IF EXISTS `cast_skill_magic`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cast_skill_magic` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `skill_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cast_skill_magic`
--

LOCK TABLES `cast_skill_magic` WRITE;
/*!40000 ALTER TABLE `cast_skill_magic` DISABLE KEYS */;
/*!40000 ALTER TABLE `cast_skill_magic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cast_skill_physics`
--

DROP TABLE IF EXISTS `cast_skill_physics`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cast_skill_physics` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `skill_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cast_skill_physics`
--

LOCK TABLES `cast_skill_physics` WRITE;
/*!40000 ALTER TABLE `cast_skill_physics` DISABLE KEYS */;
/*!40000 ALTER TABLE `cast_skill_physics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cast_skill_sp`
--

DROP TABLE IF EXISTS `cast_skill_sp`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cast_skill_sp` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `skill_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `cast_skill_sp`
--

LOCK TABLES `cast_skill_sp` WRITE;
/*!40000 ALTER TABLE `cast_skill_sp` DISABLE KEYS */;
/*!40000 ALTER TABLE `cast_skill_sp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_add_change`
--

DROP TABLE IF EXISTS `item_add_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `item_add_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  `change_result` tinyint(4) DEFAULT NULL,
  `value_old` int(10) unsigned DEFAULT NULL,
  `value_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `item_add_change`
--

LOCK TABLES `item_add_change` WRITE;
/*!40000 ALTER TABLE `item_add_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_add_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_bind_change`
--

DROP TABLE IF EXISTS `item_bind_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `item_bind_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  `change_result` tinyint(4) DEFAULT NULL,
  `value_old` int(10) unsigned DEFAULT NULL,
  `value_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `item_bind_change`
--

LOCK TABLES `item_bind_change` WRITE;
/*!40000 ALTER TABLE `item_bind_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_bind_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_level_change`
--

DROP TABLE IF EXISTS `item_level_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `item_level_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  `change_result` tinyint(4) DEFAULT NULL,
  `value_old` int(10) unsigned DEFAULT NULL,
  `value_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `item_level_change`
--

LOCK TABLES `item_level_change` WRITE;
/*!40000 ALTER TABLE `item_level_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_level_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_mass_change`
--

DROP TABLE IF EXISTS `item_mass_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `item_mass_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(3) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  `change_result` tinyint(4) DEFAULT NULL,
  `value_old` int(10) unsigned DEFAULT NULL,
  `value_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `item_mass_change`
--

LOCK TABLES `item_mass_change` WRITE;
/*!40000 ALTER TABLE `item_mass_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_mass_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_repair`
--

DROP TABLE IF EXISTS `item_repair`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `item_repair` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(11) DEFAULT NULL,
  `shop_id` int(10) unsigned DEFAULT NULL,
  `cost_money` int(10) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `item_repair`
--

LOCK TABLES `item_repair` WRITE;
/*!40000 ALTER TABLE `item_repair` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_repair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kill_player`
--

DROP TABLE IF EXISTS `kill_player`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `kill_player` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(3) unsigned DEFAULT NULL,
  `player_uuid` int(10) unsigned DEFAULT NULL,
  `player_level` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `kill_player`
--

LOCK TABLES `kill_player` WRITE;
/*!40000 ALTER TABLE `kill_player` DISABLE KEYS */;
/*!40000 ALTER TABLE `kill_player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_exchange`
--

DROP TABLE IF EXISTS `mail_exchange`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `mail_exchange` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `send_id` int(10) unsigned DEFAULT NULL,
  `recv_id` int(10) unsigned DEFAULT NULL,
  `mail_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `mail_exchange`
--

LOCK TABLES `mail_exchange` WRITE;
/*!40000 ALTER TABLE `mail_exchange` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_exchange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `market_trade`
--

DROP TABLE IF EXISTS `market_trade`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `market_trade` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_num` tinyint(3) unsigned DEFAULT NULL,
  `cost_diamond` int(10) unsigned DEFAULT NULL,
  `cost_credit` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `market_trade`
--

LOCK TABLES `market_trade` WRITE;
/*!40000 ALTER TABLE `market_trade` DISABLE KEYS */;
/*!40000 ALTER TABLE `market_trade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `money_change`
--

DROP TABLE IF EXISTS `money_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `money_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `change_type` varchar(8) DEFAULT NULL,
  `money_amout` int(11) DEFAULT NULL,
  `acction_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `money_change`
--

LOCK TABLES `money_change` WRITE;
/*!40000 ALTER TABLE `money_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `money_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monster_dead`
--

DROP TABLE IF EXISTS `monster_dead`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `monster_dead` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `map_id` int(10) unsigned DEFAULT NULL,
  `creater_id` int(10) unsigned DEFAULT NULL,
  `monster_id` int(10) unsigned DEFAULT NULL,
  `killer_id` int(10) unsigned DEFAULT NULL,
  `killer_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `monster_dead`
--

LOCK TABLES `monster_dead` WRITE;
/*!40000 ALTER TABLE `monster_dead` DISABLE KEYS */;
/*!40000 ALTER TABLE `monster_dead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_add`
--

DROP TABLE IF EXISTS `pet_add`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pet_add` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` int(10) unsigned DEFAULT NULL,
  `pet_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pet_add`
--

LOCK TABLES `pet_add` WRITE;
/*!40000 ALTER TABLE `pet_add` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet_add` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_function_add`
--

DROP TABLE IF EXISTS `pet_function_add`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pet_function_add` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` int(10) unsigned DEFAULT NULL,
  `pet_id` int(10) unsigned DEFAULT NULL,
  `function_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pet_function_add`
--

LOCK TABLES `pet_function_add` WRITE;
/*!40000 ALTER TABLE `pet_function_add` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet_function_add` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_levelup`
--

DROP TABLE IF EXISTS `pet_levelup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pet_levelup` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` int(10) unsigned DEFAULT NULL,
  `pet_id` int(10) unsigned DEFAULT NULL,
  `level_old` tinyint(3) unsigned DEFAULT NULL,
  `level_new` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pet_levelup`
--

LOCK TABLES `pet_levelup` WRITE;
/*!40000 ALTER TABLE `pet_levelup` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet_levelup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_state_change`
--

DROP TABLE IF EXISTS `pet_state_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `pet_state_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `pet_id` int(10) unsigned DEFAULT NULL,
  `pet_state` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `pet_state_change`
--

LOCK TABLES `pet_state_change` WRITE;
/*!40000 ALTER TABLE `pet_state_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet_state_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `real_instance_lifecycle`
--

DROP TABLE IF EXISTS `real_instance_lifecycle`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `real_instance_lifecycle` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `instance_id` int(10) unsigned DEFAULT NULL,
  `world_id` int(10) unsigned DEFAULT NULL,
  `cycle_point` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `real_instance_lifecycle`
--

LOCK TABLES `real_instance_lifecycle` WRITE;
/*!40000 ALTER TABLE `real_instance_lifecycle` DISABLE KEYS */;
/*!40000 ALTER TABLE `real_instance_lifecycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_dead`
--

DROP TABLE IF EXISTS `role_dead`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_dead` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `map_id` int(10) unsigned DEFAULT NULL,
  `map_x` smallint(6) DEFAULT NULL,
  `map_y` smallint(6) DEFAULT NULL,
  `killer_type` varchar(8) DEFAULT NULL,
  `killer_id` int(10) unsigned DEFAULT NULL,
  `drop_exp` int(10) unsigned DEFAULT NULL,
  `drop_item_aid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_dead`
--

LOCK TABLES `role_dead` WRITE;
/*!40000 ALTER TABLE `role_dead` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_dead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_enter`
--

DROP TABLE IF EXISTS `role_enter`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_enter` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `account` varchar(20) DEFAULT NULL,
  `role_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_enter`
--

LOCK TABLES `role_enter` WRITE;
/*!40000 ALTER TABLE `role_enter` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_enter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_exit`
--

DROP TABLE IF EXISTS `role_exit`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_exit` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `duration` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_exit`
--

LOCK TABLES `role_exit` WRITE;
/*!40000 ALTER TABLE `role_exit` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_exit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_item_drop`
--

DROP TABLE IF EXISTS `role_item_drop`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_item_drop` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_item_drop`
--

LOCK TABLES `role_item_drop` WRITE;
/*!40000 ALTER TABLE `role_item_drop` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_item_drop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_item_own`
--

DROP TABLE IF EXISTS `role_item_own`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_item_own` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  `item_add` smallint(6) unsigned DEFAULT NULL,
  `item_level` tinyint(4) unsigned DEFAULT NULL,
  `item_num` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_item_own`
--

LOCK TABLES `role_item_own` WRITE;
/*!40000 ALTER TABLE `role_item_own` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_item_own` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_item_use`
--

DROP TABLE IF EXISTS `role_item_use`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_item_use` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_item_use`
--

LOCK TABLES `role_item_use` WRITE;
/*!40000 ALTER TABLE `role_item_use` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_item_use` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_levelup`
--

DROP TABLE IF EXISTS `role_levelup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_levelup` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `level_old` int(10) unsigned DEFAULT NULL,
  `level_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_levelup`
--

LOCK TABLES `role_levelup` WRITE;
/*!40000 ALTER TABLE `role_levelup` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_levelup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_map_change`
--

DROP TABLE IF EXISTS `role_map_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_map_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `map_id_old` int(10) unsigned DEFAULT NULL,
  `map_id_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_map_change`
--

LOCK TABLES `role_map_change` WRITE;
/*!40000 ALTER TABLE `role_map_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_map_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_quest_change`
--

DROP TABLE IF EXISTS `role_quest_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_quest_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `quest_uuid` int(10) unsigned DEFAULT NULL,
  `change_type` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_quest_change`
--

LOCK TABLES `role_quest_change` WRITE;
/*!40000 ALTER TABLE `role_quest_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_quest_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_relation_change`
--

DROP TABLE IF EXISTS `role_relation_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_relation_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `relation_id` int(10) unsigned DEFAULT NULL,
  `relation_type` varchar(8) DEFAULT NULL,
  `change_type` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_relation_change`
--

LOCK TABLES `role_relation_change` WRITE;
/*!40000 ALTER TABLE `role_relation_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_relation_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_relive`
--

DROP TABLE IF EXISTS `role_relive`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_relive` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(11) DEFAULT NULL,
  `role_level` int(10) unsigned DEFAULT NULL,
  `map_id` int(10) unsigned DEFAULT NULL,
  `map_x` int(11) DEFAULT NULL,
  `map_y` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_relive`
--

LOCK TABLES `role_relive` WRITE;
/*!40000 ALTER TABLE `role_relive` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_relive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_skill_levelup`
--

DROP TABLE IF EXISTS `role_skill_levelup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_skill_levelup` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `skill_uuid` int(10) unsigned DEFAULT NULL,
  `level_old` int(10) unsigned DEFAULT NULL,
  `level_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_skill_levelup`
--

LOCK TABLES `role_skill_levelup` WRITE;
/*!40000 ALTER TABLE `role_skill_levelup` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_skill_levelup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_sp_change`
--

DROP TABLE IF EXISTS `role_sp_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_sp_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `change_type` varchar(8) DEFAULT NULL,
  `change_num` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_sp_change`
--

LOCK TABLES `role_sp_change` WRITE;
/*!40000 ALTER TABLE `role_sp_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_sp_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_sp_skill_levelup`
--

DROP TABLE IF EXISTS `role_sp_skill_levelup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_sp_skill_levelup` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `skill_uuid` int(10) unsigned DEFAULT NULL,
  `level_old` int(10) unsigned DEFAULT NULL,
  `level_new` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_sp_skill_levelup`
--

LOCK TABLES `role_sp_skill_levelup` WRITE;
/*!40000 ALTER TABLE `role_sp_skill_levelup` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_sp_skill_levelup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_trade`
--

DROP TABLE IF EXISTS `role_trade`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_trade` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `unit_a_uuid` int(10) unsigned DEFAULT NULL,
  `unit_a_level` tinyint(4) unsigned DEFAULT NULL,
  `unit_a_money` int(10) unsigned zerofill DEFAULT NULL,
  `unit_a_item_aid` int(10) unsigned DEFAULT NULL,
  `unit_b_uuid` int(10) unsigned DEFAULT NULL,
  `unit_b_level` tinyint(4) unsigned DEFAULT NULL,
  `unit_b_money` int(10) unsigned zerofill DEFAULT NULL,
  `unit_b_item_aid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_trade`
--

LOCK TABLES `role_trade` WRITE;
/*!40000 ALTER TABLE `role_trade` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_trade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_union_change`
--

DROP TABLE IF EXISTS `role_union_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `role_union_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(4) unsigned DEFAULT NULL,
  `union_uuid` int(10) unsigned DEFAULT NULL,
  `change_type` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `role_union_change`
--

LOCK TABLES `role_union_change` WRITE;
/*!40000 ALTER TABLE `role_union_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_union_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq`
--

DROP TABLE IF EXISTS `seq`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `seq` (
  `name` varchar(20) DEFAULT NULL,
  `val` int(10) unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `seq`
--

LOCK TABLES `seq` WRITE;
/*!40000 ALTER TABLE `seq` DISABLE KEYS */;
INSERT INTO `seq` VALUES ('item_aid',0);
INSERT INTO `seq` VALUES ('tix_id',0);
/*!40000 ALTER TABLE `seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_trade`
--

DROP TABLE IF EXISTS `shop_trade`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `shop_trade` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `shop_id` int(10) unsigned DEFAULT NULL,
  `trade_type` varchar(8) DEFAULT NULL,
  `money_amout` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  `item_num` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `shop_trade`
--

LOCK TABLES `shop_trade` WRITE;
/*!40000 ALTER TABLE `shop_trade` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_trade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_change`
--

DROP TABLE IF EXISTS `team_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `team_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(3) unsigned DEFAULT NULL,
  `team_id` int(10) unsigned DEFAULT NULL,
  `change_type` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `team_change`
--

LOCK TABLES `team_change` WRITE;
/*!40000 ALTER TABLE `team_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_lifecycle`
--

DROP TABLE IF EXISTS `team_lifecycle`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `team_lifecycle` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `role_level` tinyint(3) unsigned DEFAULT NULL,
  `team_id` int(10) unsigned DEFAULT NULL,
  `cycle_point` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `team_lifecycle`
--

LOCK TABLES `team_lifecycle` WRITE;
/*!40000 ALTER TABLE `team_lifecycle` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_lifecycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tix_list`
--

DROP TABLE IF EXISTS `tix_list`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tix_list` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(10) unsigned DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `wait` int(10) unsigned DEFAULT NULL,
  `account` varchar(20) DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `realm` varchar(20) DEFAULT NULL,
  `world` varchar(20) DEFAULT NULL,
  `due` varchar(20) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `info` varchar(300) DEFAULT NULL,
  `state` varchar(8) DEFAULT NULL,
  `close_type` varchar(16) DEFAULT NULL,
  `close_info` varchar(300) DEFAULT NULL,
  `close_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tix_list`
--

LOCK TABLES `tix_list` WRITE;
/*!40000 ALTER TABLE `tix_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `tix_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tix_mod`
--

DROP TABLE IF EXISTS `tix_mod`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tix_mod` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(10) unsigned DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `due` varchar(20) DEFAULT NULL,
  `close_type` varchar(20) DEFAULT NULL,
  `close_info` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tix_mod`
--

LOCK TABLES `tix_mod` WRITE;
/*!40000 ALTER TABLE `tix_mod` DISABLE KEYS */;
/*!40000 ALTER TABLE `tix_mod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_extend`
--

DROP TABLE IF EXISTS `warehouse_extend`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `warehouse_extend` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `warehouse_id` int(10) unsigned DEFAULT NULL,
  `extend` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `warehouse_extend`
--

LOCK TABLES `warehouse_extend` WRITE;
/*!40000 ALTER TABLE `warehouse_extend` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_extend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_item_transport`
--

DROP TABLE IF EXISTS `warehouse_item_transport`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `warehouse_item_transport` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `warehouse_id` int(10) unsigned DEFAULT NULL,
  `transport_type` varchar(8) DEFAULT NULL,
  `item_uuid` int(10) unsigned DEFAULT NULL,
  `item_typeid` int(10) unsigned DEFAULT NULL,
  `item_num` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `warehouse_item_transport`
--

LOCK TABLES `warehouse_item_transport` WRITE;
/*!40000 ALTER TABLE `warehouse_item_transport` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_item_transport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_lock_change`
--

DROP TABLE IF EXISTS `warehouse_lock_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `warehouse_lock_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  `warehouse_id` int(10) unsigned DEFAULT NULL,
  `change_result` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `warehouse_lock_change`
--

LOCK TABLES `warehouse_lock_change` WRITE;
/*!40000 ALTER TABLE `warehouse_lock_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_lock_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_money_transport`
--

DROP TABLE IF EXISTS `warehouse_money_transport`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `warehouse_money_transport` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `warehouse_id` int(10) unsigned DEFAULT NULL,
  `transport_type` varchar(8) DEFAULT NULL,
  `money_amout` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `warehouse_money_transport`
--

LOCK TABLES `warehouse_money_transport` WRITE;
/*!40000 ALTER TABLE `warehouse_money_transport` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_money_transport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_open`
--

DROP TABLE IF EXISTS `warehouse_open`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `warehouse_open` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_uuid` int(10) unsigned DEFAULT NULL,
  `warehouse_id` int(10) unsigned DEFAULT NULL,
  `use_password` tinyint(1) unsigned DEFAULT NULL,
  `map_id` int(10) unsigned DEFAULT NULL,
  `map_x` int(11) DEFAULT NULL,
  `map_y` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `warehouse_open`
--

LOCK TABLES `warehouse_open` WRITE;
/*!40000 ALTER TABLE `warehouse_open` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_open` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse_password_change`
--

DROP TABLE IF EXISTS `warehouse_password_change`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `warehouse_password_change` (
  `id` bigint(20)  unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  `warehouse_id` int(10) unsigned DEFAULT NULL,
  `change_result` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=gb2312;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `warehouse_password_change`
--

LOCK TABLES `warehouse_password_change` WRITE;
/*!40000 ALTER TABLE `warehouse_password_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse_password_change` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Function structure for function `seq`
--
delimiter //
DROP FUNCTION IF EXISTS `seq`//
CREATE FUNCTION `seq`(seq_name varchar(20) CHARACTER SET  gb2312) RETURNS int(11)
begin
	update `seq` set val = LAST_INSERT_ID(val+1) where name = seq_name;
	return LAST_INSERT_ID();
end//
delimiter ;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-12-02  2:07:02
