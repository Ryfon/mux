﻿// -*- C++ -*-

//=============================================================================
/**
 *  @file    logic_task.h
 *
 *  $Version 0.01 2009-10-30 11:06:06
 *
 *  @author chenshaobin <chenshaobin@corp.the9.com>
 */
//=============================================================================

#ifndef LOGIC_TASK_H
#define LOGIC_TASK_H

#include "inc_ace.h"
#include "task_msg.h"

class logic_task : public ACE_Task<ACE_MT_SYNCH>
{
public:
    int open (void *args = 0);
    int svc (void);

protected:
    int handle_logic(tmsg* msg);

    int write_file(const char* data, int size);
    int handle_account(bool on = true);

private:

    ACE_FILE_IO     file_;
};


#endif//LOGIC_TASK_H

