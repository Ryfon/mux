﻿// -*- C++ -*-

//=============================================================================
/**
 *  @file    Log.h
 *
 *  $Version 0.01 2009-10-16 16:18:18
 *
 *  @author chenshaobin <chenshaobin@corp.the9.com>
 */
//=============================================================================

// MS compatible compilers support #pragma once
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#ifndef LOG_H
#define LOG_H

#include <ace/Log_Msg.h>
#include "../ServiceSpaceLib/StdHdr.h"

__SERVICE_SPACE_BEGIN_NS__

#ifdef ACE_NTRACE
#undef ACE_NTRACE
#endif
#define ACE_NTRACE 0


class iLog_Scope_Depth
{
public:
    iLog_Scope_Depth(void)
    {
        ACE_LOG_MSG->inc();
    }
    ~iLog_Scope_Depth(void)
    {
        ACE_LOG_MSG->dec();
    }
};

#define TRACE_PREFIX       ACE_TEXT ("[DBG][%T] %I")
#define DBG_PREFIX         ACE_TEXT ("[DBG][%T] %I")
#define WRN_PREFIX         ACE_TEXT ("[WRN][%T] %I")
#define ERR_PREFIX         ACE_TEXT ("[ERR][%T] %I")
//#define DBG_PREFIX          ACE_TEXT (" %I")
//#define WRN_PREFIX          ACE_TEXT (" %I")
//#define ERR_PREFIX          ACE_TEXT (" %I")

#define __DBG(...)       ACE_DEBUG((LM_DEBUG  , DBG_PREFIX __VA_ARGS__))
#define __WRN(...)       ACE_DEBUG((LM_WARNING, WRN_PREFIX __VA_ARGS__))
#define __ERR(...)       ACE_ERROR((LM_ERROR  , ERR_PREFIX __VA_ARGS__))

class Trace
{
public:
    Trace (const ACE_TCHAR *name,
           int line,
           const ACE_TCHAR *file)
    {
        this->name_   = name;
        this->line_   = line;
        this->file_   = file;

        ACE_Log_Msg *lm = ACE_LOG_MSG;
        if (lm->tracing_enabled ()
            && lm->trace_active () == 0)
        {

            lm->trace_active (1);
            ACE_DEBUG
                ((LM_TRACE,
                TRACE_PREFIX
                ACE_TEXT ("%s enter @%d\n"),
                this->name_,
                this->line_));
            lm->inc();
            lm->trace_active (0);
        }
    }

    void setLine (int line)
    {
        this->line_ = line;
    }

    ~Trace (void)
    {
        ACE_Log_Msg *lm = ACE_LOG_MSG;
        if (lm->tracing_enabled ()
            && lm->trace_active () == 0)
        {
            lm->trace_active (1);
            lm->dec();

            ACE_DEBUG
                ((LM_TRACE,
                TRACE_PREFIX
                ACE_TEXT ("%s exit @%d\n"),
                this->name_,
                this->line_));
            lm->trace_active (0);
        }
    }

private:
    enum { nesting_indent_ = 3 };

    const ACE_TCHAR *name_;
    const ACE_TCHAR *file_;
    int line_;
};

#if (ACE_NTRACE == 1)
#    define __TRACE(X)
#    define __TRACE_RETURN(V)         do { return V; } while (0)
#    define __TRACE_RETURN_VOID()     do { return; } while (0)
#else
#   define __TRACE(X)                             \
    Trace ____ (ACE_TEXT (X),                   \
                __LINE__,                       \
                ACE_TEXT (__FILE__))

#   define __TRACE_RETURN(V)                     \
    do { ____.setLine(__LINE__); return V; } while (0)

#   define __TRACE_RETURN_VOID()                 \
    do { ____.setLine(__LINE__); } while (0)
#endif

__SERVICE_SPACE_END_NS__

#endif//LOG_H

