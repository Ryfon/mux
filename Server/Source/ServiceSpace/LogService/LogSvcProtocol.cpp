﻿#include "LogSvcProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

LOG_SVC_NS::LogSvcProtocol::LogSvcProtocol()
{
	m_mapEnCodeFunc[0x1c01]	=	&LogSvcProtocol::EnCode__GOAccountLogin;
	m_mapDeCodeFunc[0x1c01]	=	&LogSvcProtocol::DeCode__GOAccountLogin;

	m_mapEnCodeFunc[0x1c02]	=	&LogSvcProtocol::EnCode__GOAccountLogoff;
	m_mapDeCodeFunc[0x1c02]	=	&LogSvcProtocol::DeCode__GOAccountLogoff;

	m_mapEnCodeFunc[0x1c03]	=	&LogSvcProtocol::EnCode__GORoleEnter;
	m_mapDeCodeFunc[0x1c03]	=	&LogSvcProtocol::DeCode__GORoleEnter;

	m_mapEnCodeFunc[0x1c04]	=	&LogSvcProtocol::EnCode__GORoleExit;
	m_mapDeCodeFunc[0x1c04]	=	&LogSvcProtocol::DeCode__GORoleExit;

	m_mapEnCodeFunc[0x1c05]	=	&LogSvcProtocol::EnCode__GORoleLevelUP;
	m_mapDeCodeFunc[0x1c05]	=	&LogSvcProtocol::DeCode__GORoleLevelUP;

	m_mapEnCodeFunc[0x1c06]	=	&LogSvcProtocol::EnCode__GOSkillLevelUP;
	m_mapDeCodeFunc[0x1c06]	=	&LogSvcProtocol::DeCode__GOSkillLevelUP;

	m_mapEnCodeFunc[0x1c07]	=	&LogSvcProtocol::EnCode__GORoleItemOwn;
	m_mapDeCodeFunc[0x1c07]	=	&LogSvcProtocol::DeCode__GORoleItemOwn;

	m_mapEnCodeFunc[0x1c08]	=	&LogSvcProtocol::EnCode__GORoleItemUse;
	m_mapDeCodeFunc[0x1c08]	=	&LogSvcProtocol::DeCode__GORoleItemUse;

	m_mapEnCodeFunc[0x1c09]	=	&LogSvcProtocol::EnCode__GORoleItemDrop;
	m_mapDeCodeFunc[0x1c09]	=	&LogSvcProtocol::DeCode__GORoleItemDrop;

	m_mapEnCodeFunc[0x1c0a]	=	&LogSvcProtocol::EnCode__GORoleRelationChange;
	m_mapDeCodeFunc[0x1c0a]	=	&LogSvcProtocol::DeCode__GORoleRelationChange;

	m_mapEnCodeFunc[0x1c0b]	=	&LogSvcProtocol::EnCode__GORoleUnionChange;
	m_mapDeCodeFunc[0x1c0b]	=	&LogSvcProtocol::DeCode__GORoleUnionChange;

	m_mapEnCodeFunc[0x1c0c]	=	&LogSvcProtocol::EnCode__GORoleQuestChange;
	m_mapDeCodeFunc[0x1c0c]	=	&LogSvcProtocol::DeCode__GORoleQuestChange;

	m_mapEnCodeFunc[0x1c0d]	=	&LogSvcProtocol::EnCode__GORoleDead;
	m_mapDeCodeFunc[0x1c0d]	=	&LogSvcProtocol::DeCode__GORoleDead;

	m_mapEnCodeFunc[0x1c0e]	=	&LogSvcProtocol::EnCode__GORoleMapChange;
	m_mapDeCodeFunc[0x1c0e]	=	&LogSvcProtocol::DeCode__GORoleMapChange;

	m_mapEnCodeFunc[0x1c0f]	=	&LogSvcProtocol::EnCode__GORoleTrade;
	m_mapDeCodeFunc[0x1c0f]	=	&LogSvcProtocol::DeCode__GORoleTrade;

	m_mapEnCodeFunc[0x1c10]	=	&LogSvcProtocol::EnCode__GOSPChange;
	m_mapDeCodeFunc[0x1c10]	=	&LogSvcProtocol::DeCode__GOSPChange;

	m_mapEnCodeFunc[0x1c11]	=	&LogSvcProtocol::EnCode__GOItemPropertyChange;
	m_mapDeCodeFunc[0x1c11]	=	&LogSvcProtocol::DeCode__GOItemPropertyChange;

	m_mapEnCodeFunc[0x1c12]	=	&LogSvcProtocol::EnCode__GOMoneyChange;
	m_mapDeCodeFunc[0x1c12]	=	&LogSvcProtocol::DeCode__GOMoneyChange;

	m_mapEnCodeFunc[0x1c13]	=	&LogSvcProtocol::EnCode__GOTeamChange;
	m_mapDeCodeFunc[0x1c13]	=	&LogSvcProtocol::DeCode__GOTeamChange;

	m_mapEnCodeFunc[0x1c14]	=	&LogSvcProtocol::EnCode__GOPetAdd;
	m_mapDeCodeFunc[0x1c14]	=	&LogSvcProtocol::DeCode__GOPetAdd;

	m_mapEnCodeFunc[0x1c15]	=	&LogSvcProtocol::EnCode__GOPetLevelup;
	m_mapDeCodeFunc[0x1c15]	=	&LogSvcProtocol::DeCode__GOPetLevelup;

	m_mapEnCodeFunc[0x1c16]	=	&LogSvcProtocol::EnCode__GOPetFunctionAdd;
	m_mapDeCodeFunc[0x1c16]	=	&LogSvcProtocol::DeCode__GOPetFunctionAdd;

	m_mapEnCodeFunc[0x1c17]	=	&LogSvcProtocol::EnCode__GOPetStateChange;
	m_mapDeCodeFunc[0x1c17]	=	&LogSvcProtocol::DeCode__GOPetStateChange;

	m_mapEnCodeFunc[0x1c18]	=	&LogSvcProtocol::EnCode__GOUseSkill;
	m_mapDeCodeFunc[0x1c18]	=	&LogSvcProtocol::DeCode__GOUseSkill;

	m_mapEnCodeFunc[0x1c19]	=	&LogSvcProtocol::EnCode__GOKillPlayer;
	m_mapDeCodeFunc[0x1c19]	=	&LogSvcProtocol::DeCode__GOKillPlayer;

	m_mapEnCodeFunc[0x1c1a]	=	&LogSvcProtocol::EnCode__GORelive;
	m_mapDeCodeFunc[0x1c1a]	=	&LogSvcProtocol::DeCode__GORelive;

	m_mapEnCodeFunc[0x1c1b]	=	&LogSvcProtocol::EnCode__GOSendMail;
	m_mapDeCodeFunc[0x1c1b]	=	&LogSvcProtocol::DeCode__GOSendMail;

	m_mapEnCodeFunc[0x1c1c]	=	&LogSvcProtocol::EnCode__GOShopTrade;
	m_mapDeCodeFunc[0x1c1c]	=	&LogSvcProtocol::DeCode__GOShopTrade;

	m_mapEnCodeFunc[0x1c1d]	=	&LogSvcProtocol::EnCode__GOItemRepair;
	m_mapDeCodeFunc[0x1c1d]	=	&LogSvcProtocol::DeCode__GOItemRepair;

	m_mapEnCodeFunc[0x1c1e]	=	&LogSvcProtocol::EnCode__GOMarketTrade;
	m_mapDeCodeFunc[0x1c1e]	=	&LogSvcProtocol::DeCode__GOMarketTrade;

	m_mapEnCodeFunc[0x1c1f]	=	&LogSvcProtocol::EnCode__GOWarehouseOpen;
	m_mapDeCodeFunc[0x1c1f]	=	&LogSvcProtocol::DeCode__GOWarehouseOpen;

	m_mapEnCodeFunc[0x1c20]	=	&LogSvcProtocol::EnCode__GOWarehouseItemTransport;
	m_mapDeCodeFunc[0x1c20]	=	&LogSvcProtocol::DeCode__GOWarehouseItemTransport;

	m_mapEnCodeFunc[0x1c21]	=	&LogSvcProtocol::EnCode__GOWarehouseMoneyTransport;
	m_mapDeCodeFunc[0x1c21]	=	&LogSvcProtocol::DeCode__GOWarehouseMoneyTransport;

	m_mapEnCodeFunc[0x1c22]	=	&LogSvcProtocol::EnCode__GOWarehouseExtend;
	m_mapDeCodeFunc[0x1c22]	=	&LogSvcProtocol::DeCode__GOWarehouseExtend;

	m_mapEnCodeFunc[0x1c23]	=	&LogSvcProtocol::EnCode__GOWarehousePrivateOP;
	m_mapDeCodeFunc[0x1c23]	=	&LogSvcProtocol::DeCode__GOWarehousePrivateOP;

	m_mapEnCodeFunc[0x1c24]	=	&LogSvcProtocol::EnCode__GOMonsterDead;
	m_mapDeCodeFunc[0x1c24]	=	&LogSvcProtocol::DeCode__GOMonsterDead;

	m_mapEnCodeFunc[0x1c25]	=	&LogSvcProtocol::EnCode__GORealInstanceLifeCycle;
	m_mapDeCodeFunc[0x1c25]	=	&LogSvcProtocol::DeCode__GORealInstanceLifeCycle;

	m_mapEnCodeFunc[0x1d01]	=	&LogSvcProtocol::EnCode__GOTixNew;
	m_mapDeCodeFunc[0x1d01]	=	&LogSvcProtocol::DeCode__GOTixNew;

	m_mapEnCodeFunc[0x1d81]	=	&LogSvcProtocol::EnCode__OGTixNewRsp;
	m_mapDeCodeFunc[0x1d81]	=	&LogSvcProtocol::DeCode__OGTixNewRsp;

	m_mapEnCodeFunc[0x1d02]	=	&LogSvcProtocol::EnCode__GOTixMod;
	m_mapDeCodeFunc[0x1d02]	=	&LogSvcProtocol::DeCode__GOTixMod;

	m_mapEnCodeFunc[0x1d82]	=	&LogSvcProtocol::EnCode__OGTixModRsp;
	m_mapDeCodeFunc[0x1d82]	=	&LogSvcProtocol::DeCode__OGTixModRsp;

}

LOG_SVC_NS::LogSvcProtocol::~LogSvcProtocol()
{
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

LOG_SVC_NS::EnCodeFunc	LOG_SVC_NS::LogSvcProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

LOG_SVC_NS::DeCodeFunc	LOG_SVC_NS::LogSvcProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__ItemInfo_i(void* pData)
{
	ItemInfo_i* pkItemInfo_i = (ItemInfo_i*)(pData);

	//EnCode UUID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkItemInfo_i->UUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkItemInfo_i->TypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WearPoint
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkItemInfo_i->WearPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AddId
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkItemInfo_i->AddId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Count
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkItemInfo_i->Count), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode LevelUp
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkItemInfo_i->LevelUp), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__ItemInfo_i(void* pData)
{
	ItemInfo_i* pkItemInfo_i = (ItemInfo_i*)(pData);

	//DeCode UUID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkItemInfo_i->UUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkItemInfo_i->TypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WearPoint
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkItemInfo_i->WearPoint), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AddId
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkItemInfo_i->AddId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Count
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkItemInfo_i->Count), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode LevelUp
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkItemInfo_i->LevelUp), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(ItemInfo_i);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOAccountLogin(void* pData)
{
	GOAccountLogin* pkGOAccountLogin = (GOAccountLogin*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOAccountLogin->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOAccountLogin->AccountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if(MAX_NAME_SIZE < pkGOAccountLogin->AccountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOAccountLogin->AccountSize;
	if(!m_kPackage.Pack("CHAR", &(pkGOAccountLogin->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayerIPSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOAccountLogin->PlayerIPSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayerIP
	if(MAX_IP_SIZE < pkGOAccountLogin->PlayerIPSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOAccountLogin->PlayerIPSize;
	if(!m_kPackage.Pack("CHAR", &(pkGOAccountLogin->PlayerIP), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOAccountLogin->PlayType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOAccountLogin(void* pData)
{
	GOAccountLogin* pkGOAccountLogin = (GOAccountLogin*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOAccountLogin->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOAccountLogin->AccountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if(MAX_NAME_SIZE < pkGOAccountLogin->AccountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOAccountLogin->AccountSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGOAccountLogin->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayerIPSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOAccountLogin->PlayerIPSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayerIP
	if(MAX_IP_SIZE < pkGOAccountLogin->PlayerIPSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOAccountLogin->PlayerIPSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGOAccountLogin->PlayerIP), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOAccountLogin->PlayType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOAccountLogin);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOAccountLogoff(void* pData)
{
	GOAccountLogoff* pkGOAccountLogoff = (GOAccountLogoff*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOAccountLogoff->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOAccountLogoff->AccountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if(MAX_NAME_SIZE < pkGOAccountLogoff->AccountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOAccountLogoff->AccountSize;
	if(!m_kPackage.Pack("CHAR", &(pkGOAccountLogoff->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Duration
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOAccountLogoff->Duration), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOAccountLogoff->PlayType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOAccountLogoff(void* pData)
{
	GOAccountLogoff* pkGOAccountLogoff = (GOAccountLogoff*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOAccountLogoff->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOAccountLogoff->AccountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if(MAX_NAME_SIZE < pkGOAccountLogoff->AccountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOAccountLogoff->AccountSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGOAccountLogoff->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Duration
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOAccountLogoff->Duration), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOAccountLogoff->PlayType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOAccountLogoff);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleEnter(void* pData)
{
	GORoleEnter* pkGORoleEnter = (GORoleEnter*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleEnter->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleEnter->AccountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if(MAX_NAME_SIZE < pkGORoleEnter->AccountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGORoleEnter->AccountSize;
	if(!m_kPackage.Pack("CHAR", &(pkGORoleEnter->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleEnter->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleEnter->RoleNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleName
	if(MAX_NAME_SIZE < pkGORoleEnter->RoleNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGORoleEnter->RoleNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGORoleEnter->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleEnter->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleEnter->PlayType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleEnter(void* pData)
{
	GORoleEnter* pkGORoleEnter = (GORoleEnter*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleEnter->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleEnter->AccountSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if(MAX_NAME_SIZE < pkGORoleEnter->AccountSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGORoleEnter->AccountSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGORoleEnter->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleEnter->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleEnter->RoleNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleName
	if(MAX_NAME_SIZE < pkGORoleEnter->RoleNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGORoleEnter->RoleNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGORoleEnter->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleEnter->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleEnter->PlayType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleEnter);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleExit(void* pData)
{
	GORoleExit* pkGORoleExit = (GORoleExit*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleExit->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleExit->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleExit->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Duration
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleExit->Duration), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleExit->PlayType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleExit(void* pData)
{
	GORoleExit* pkGORoleExit = (GORoleExit*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleExit->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleExit->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleExit->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Duration
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleExit->Duration), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleExit->PlayType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleExit);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleLevelUP(void* pData)
{
	GORoleLevelUP* pkGORoleLevelUP = (GORoleLevelUP*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleLevelUP->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleLevelUP->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OldLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleLevelUP->OldLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NewLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleLevelUP->NewLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleLevelUP(void* pData)
{
	GORoleLevelUP* pkGORoleLevelUP = (GORoleLevelUP*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleLevelUP->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleLevelUP->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OldLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleLevelUP->OldLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NewLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleLevelUP->NewLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleLevelUP);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOSkillLevelUP(void* pData)
{
	GOSkillLevelUP* pkGOSkillLevelUP = (GOSkillLevelUP*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSkillLevelUP->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSkillLevelUP->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOSkillLevelUP->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SkillType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOSkillLevelUP->SkillType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OldLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSkillLevelUP->OldLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NewLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSkillLevelUP->NewLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOSkillLevelUP(void* pData)
{
	GOSkillLevelUP* pkGOSkillLevelUP = (GOSkillLevelUP*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSkillLevelUP->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSkillLevelUP->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOSkillLevelUP->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SkillType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOSkillLevelUP->SkillType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OldLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSkillLevelUP->OldLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NewLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSkillLevelUP->NewLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOSkillLevelUP);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleItemOwn(void* pData)
{
	GORoleItemOwn* pkGORoleItemOwn = (GORoleItemOwn*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleItemOwn->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleItemOwn->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleItemOwn->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OwnType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleItemOwn->OwnType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Item
	if(EnCode__ItemInfo_i(&(pkGORoleItemOwn->Item)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleItemOwn(void* pData)
{
	GORoleItemOwn* pkGORoleItemOwn = (GORoleItemOwn*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleItemOwn->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleItemOwn->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleItemOwn->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OwnType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleItemOwn->OwnType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Item
	if(DeCode__ItemInfo_i(&(pkGORoleItemOwn->Item)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleItemOwn);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleItemUse(void* pData)
{
	GORoleItemUse* pkGORoleItemUse = (GORoleItemUse*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleItemUse->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleItemUse->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleItemUse->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Item
	if(EnCode__ItemInfo_i(&(pkGORoleItemUse->Item)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleItemUse(void* pData)
{
	GORoleItemUse* pkGORoleItemUse = (GORoleItemUse*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleItemUse->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleItemUse->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleItemUse->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Item
	if(DeCode__ItemInfo_i(&(pkGORoleItemUse->Item)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleItemUse);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleItemDrop(void* pData)
{
	GORoleItemDrop* pkGORoleItemDrop = (GORoleItemDrop*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleItemDrop->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleItemDrop->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleItemDrop->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Item
	if(EnCode__ItemInfo_i(&(pkGORoleItemDrop->Item)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleItemDrop(void* pData)
{
	GORoleItemDrop* pkGORoleItemDrop = (GORoleItemDrop*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleItemDrop->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleItemDrop->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleItemDrop->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Item
	if(DeCode__ItemInfo_i(&(pkGORoleItemDrop->Item)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleItemDrop);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleRelationChange(void* pData)
{
	GORoleRelationChange* pkGORoleRelationChange = (GORoleRelationChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleRelationChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleRelationChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleRelationChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RelationID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleRelationChange->RelationID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RelationType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleRelationChange->RelationType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ChangeType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleRelationChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleRelationChange(void* pData)
{
	GORoleRelationChange* pkGORoleRelationChange = (GORoleRelationChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleRelationChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleRelationChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleRelationChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RelationID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleRelationChange->RelationID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RelationType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleRelationChange->RelationType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ChangeType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleRelationChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleRelationChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleUnionChange(void* pData)
{
	GORoleUnionChange* pkGORoleUnionChange = (GORoleUnionChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleUnionChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleUnionChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleUnionChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UnionID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleUnionChange->UnionID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ChangeType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleUnionChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleUnionChange(void* pData)
{
	GORoleUnionChange* pkGORoleUnionChange = (GORoleUnionChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleUnionChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleUnionChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleUnionChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UnionID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleUnionChange->UnionID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ChangeType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleUnionChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleUnionChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleQuestChange(void* pData)
{
	GORoleQuestChange* pkGORoleQuestChange = (GORoleQuestChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleQuestChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleQuestChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleQuestChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode QuestID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleQuestChange->QuestID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ChangeType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleQuestChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleQuestChange(void* pData)
{
	GORoleQuestChange* pkGORoleQuestChange = (GORoleQuestChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleQuestChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleQuestChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleQuestChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode QuestID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleQuestChange->QuestID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ChangeType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleQuestChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleQuestChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleDead(void* pData)
{
	GORoleDead* pkGORoleDead = (GORoleDead*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleDead->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleDead->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleDead->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleDead->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapX
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleDead->MapX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapY
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleDead->MapY), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode KillerType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleDead->KillerType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode KillerID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleDead->KillerID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DropExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleDead->DropExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DropItemCount
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleDead->DropItemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DropItem
	if(MAX_DROP_ITEM_SIZE < pkGORoleDead->DropItemCount)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGORoleDead->DropItemCount; ++i)
	{
		if(EnCode__ItemInfo_i(&(pkGORoleDead->DropItem[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleDead(void* pData)
{
	GORoleDead* pkGORoleDead = (GORoleDead*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleDead->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleDead->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleDead->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleDead->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapX
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleDead->MapX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapY
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleDead->MapY), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode KillerType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleDead->KillerType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode KillerID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleDead->KillerID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DropExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleDead->DropExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DropItemCount
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleDead->DropItemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DropItem
	if(MAX_DROP_ITEM_SIZE < pkGORoleDead->DropItemCount)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkGORoleDead->DropItemCount; ++i)
	{
		if(DeCode__ItemInfo_i(&(pkGORoleDead->DropItem[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(GORoleDead);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleMapChange(void* pData)
{
	GORoleMapChange* pkGORoleMapChange = (GORoleMapChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleMapChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleMapChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORoleMapChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OldMapID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleMapChange->OldMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NewMapID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleMapChange->NewMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleMapChange(void* pData)
{
	GORoleMapChange* pkGORoleMapChange = (GORoleMapChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleMapChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleMapChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORoleMapChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OldMapID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleMapChange->OldMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NewMapID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleMapChange->NewMapID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleMapChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__TradeUnit_i(void* pData)
{
	TradeUnit_i* pkTradeUnit_i = (TradeUnit_i*)(pData);

	//EnCode RoleID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTradeUnit_i->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkTradeUnit_i->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemCount
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTradeUnit_i->ItemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Item
	if(MAX_TRADE_ITEM_SIZE < pkTradeUnit_i->ItemCount)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkTradeUnit_i->ItemCount; ++i)
	{
		if(EnCode__ItemInfo_i(&(pkTradeUnit_i->Item[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode Money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTradeUnit_i->Money), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__TradeUnit_i(void* pData)
{
	TradeUnit_i* pkTradeUnit_i = (TradeUnit_i*)(pData);

	//DeCode RoleID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTradeUnit_i->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkTradeUnit_i->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemCount
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTradeUnit_i->ItemCount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Item
	if(MAX_TRADE_ITEM_SIZE < pkTradeUnit_i->ItemCount)
	{
		return FAILEDRETCODE;
	}

	for(UINT i = 0; i < pkTradeUnit_i->ItemCount; ++i)
	{
		if(DeCode__ItemInfo_i(&(pkTradeUnit_i->Item[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode Money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTradeUnit_i->Money), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(TradeUnit_i);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORoleTrade(void* pData)
{
	GORoleTrade* pkGORoleTrade = (GORoleTrade*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORoleTrade->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UnitA
	if(EnCode__TradeUnit_i(&(pkGORoleTrade->UnitA)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode UnitB
	if(EnCode__TradeUnit_i(&(pkGORoleTrade->UnitB)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORoleTrade(void* pData)
{
	GORoleTrade* pkGORoleTrade = (GORoleTrade*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORoleTrade->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UnitA
	if(DeCode__TradeUnit_i(&(pkGORoleTrade->UnitA)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode UnitB
	if(DeCode__TradeUnit_i(&(pkGORoleTrade->UnitB)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORoleTrade);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOSPChange(void* pData)
{
	GOSPChange* pkGOSPChange = (GOSPChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSPChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSPChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOSPChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ChangeType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOSPChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ChangeNum
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOSPChange->ChangeNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOSPChange(void* pData)
{
	GOSPChange* pkGOSPChange = (GOSPChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSPChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSPChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOSPChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ChangeType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOSPChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ChangeNum
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOSPChange->ChangeNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOSPChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOItemPropertyChange(void* pData)
{
	GOItemPropertyChange* pkGOItemPropertyChange = (GOItemPropertyChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemPropertyChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemPropertyChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOItemPropertyChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PropertyType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOItemPropertyChange->PropertyType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemUUID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemPropertyChange->ItemUUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemPropertyChange->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OldValue
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemPropertyChange->OldValue), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NewValue
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemPropertyChange->NewValue), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOItemPropertyChange(void* pData)
{
	GOItemPropertyChange* pkGOItemPropertyChange = (GOItemPropertyChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemPropertyChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemPropertyChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOItemPropertyChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PropertyType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOItemPropertyChange->PropertyType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemUUID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemPropertyChange->ItemUUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemPropertyChange->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OldValue
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemPropertyChange->OldValue), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NewValue
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemPropertyChange->NewValue), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOItemPropertyChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOMoneyChange(void* pData)
{
	GOMoneyChange* pkGOMoneyChange = (GOMoneyChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMoneyChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMoneyChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOMoneyChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ChangeType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOMoneyChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ReasonType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOMoneyChange->ReasonType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MoneyType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOMoneyChange->MoneyType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MoneyCount
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMoneyChange->MoneyCount), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOMoneyChange(void* pData)
{
	GOMoneyChange* pkGOMoneyChange = (GOMoneyChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMoneyChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMoneyChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOMoneyChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ChangeType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOMoneyChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ReasonType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOMoneyChange->ReasonType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MoneyType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOMoneyChange->MoneyType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MoneyCount
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMoneyChange->MoneyCount), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOMoneyChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOTeamChange(void* pData)
{
	GOTeamChange* pkGOTeamChange = (GOTeamChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTeamChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTeamChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOTeamChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ChangeType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOTeamChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TeamID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTeamChange->TeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOTeamChange(void* pData)
{
	GOTeamChange* pkGOTeamChange = (GOTeamChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTeamChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTeamChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOTeamChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ChangeType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOTeamChange->ChangeType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TeamID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTeamChange->TeamID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOTeamChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOPetAdd(void* pData)
{
	GOPetAdd* pkGOPetAdd = (GOPetAdd*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetAdd->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetAdd->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOPetAdd->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PetID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetAdd->PetID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOPetAdd(void* pData)
{
	GOPetAdd* pkGOPetAdd = (GOPetAdd*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetAdd->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetAdd->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOPetAdd->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PetID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetAdd->PetID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOPetAdd);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOPetLevelup(void* pData)
{
	GOPetLevelup* pkGOPetLevelup = (GOPetLevelup*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetLevelup->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetLevelup->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOPetLevelup->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PetID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetLevelup->PetID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OldLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetLevelup->OldLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NewLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetLevelup->NewLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOPetLevelup(void* pData)
{
	GOPetLevelup* pkGOPetLevelup = (GOPetLevelup*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetLevelup->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetLevelup->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOPetLevelup->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PetID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetLevelup->PetID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OldLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetLevelup->OldLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NewLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetLevelup->NewLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOPetLevelup);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOPetFunctionAdd(void* pData)
{
	GOPetFunctionAdd* pkGOPetFunctionAdd = (GOPetFunctionAdd*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetFunctionAdd->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetFunctionAdd->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOPetFunctionAdd->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PetID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetFunctionAdd->PetID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode FunctionID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetFunctionAdd->FunctionID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOPetFunctionAdd(void* pData)
{
	GOPetFunctionAdd* pkGOPetFunctionAdd = (GOPetFunctionAdd*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetFunctionAdd->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetFunctionAdd->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOPetFunctionAdd->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PetID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetFunctionAdd->PetID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode FunctionID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetFunctionAdd->FunctionID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOPetFunctionAdd);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOPetStateChange(void* pData)
{
	GOPetStateChange* pkGOPetStateChange = (GOPetStateChange*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetStateChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetStateChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOPetStateChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PetID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOPetStateChange->PetID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode StateType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOPetStateChange->StateType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOPetStateChange(void* pData)
{
	GOPetStateChange* pkGOPetStateChange = (GOPetStateChange*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetStateChange->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetStateChange->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOPetStateChange->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PetID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOPetStateChange->PetID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode StateType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOPetStateChange->StateType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOPetStateChange);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOUseSkill(void* pData)
{
	GOUseSkill* pkGOUseSkill = (GOUseSkill*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOUseSkill->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOUseSkill->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOUseSkill->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SkillType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOUseSkill->SkillType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SkillID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOUseSkill->SkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOUseSkill(void* pData)
{
	GOUseSkill* pkGOUseSkill = (GOUseSkill*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOUseSkill->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOUseSkill->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOUseSkill->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SkillType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOUseSkill->SkillType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SkillID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOUseSkill->SkillID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOUseSkill);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOKillPlayer(void* pData)
{
	GOKillPlayer* pkGOKillPlayer = (GOKillPlayer*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOKillPlayer->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOKillPlayer->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOKillPlayer->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayerID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOKillPlayer->PlayerID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PlayerLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOKillPlayer->PlayerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOKillPlayer(void* pData)
{
	GOKillPlayer* pkGOKillPlayer = (GOKillPlayer*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOKillPlayer->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOKillPlayer->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOKillPlayer->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayerID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOKillPlayer->PlayerID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PlayerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOKillPlayer->PlayerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOKillPlayer);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORelive(void* pData)
{
	GORelive* pkGORelive = (GORelive*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORelive->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORelive->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORelive->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORelive->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapX
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORelive->MapX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapY
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORelive->MapY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORelive(void* pData)
{
	GORelive* pkGORelive = (GORelive*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORelive->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORelive->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORelive->RoleLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORelive->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapX
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORelive->MapX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapY
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORelive->MapY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORelive);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOSendMail(void* pData)
{
	GOSendMail* pkGOSendMail = (GOSendMail*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSendMail->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SendRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSendMail->SendRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RecvRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSendMail->RecvRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MailID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOSendMail->MailID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOSendMail(void* pData)
{
	GOSendMail* pkGOSendMail = (GOSendMail*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSendMail->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SendRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSendMail->SendRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RecvRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSendMail->RecvRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MailID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOSendMail->MailID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOSendMail);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOShopTrade(void* pData)
{
	GOShopTrade* pkGOShopTrade = (GOShopTrade*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOShopTrade->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOShopTrade->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ShopID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOShopTrade->ShopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SaleBuy
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOShopTrade->SaleBuy), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOShopTrade->Money), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOShopTrade->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOShopTrade->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOShopTrade(void* pData)
{
	GOShopTrade* pkGOShopTrade = (GOShopTrade*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOShopTrade->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOShopTrade->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ShopID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOShopTrade->ShopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SaleBuy
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOShopTrade->SaleBuy), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOShopTrade->Money), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOShopTrade->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOShopTrade->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOShopTrade);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOItemRepair(void* pData)
{
	GOItemRepair* pkGOItemRepair = (GOItemRepair*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemRepair->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemRepair->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ShopID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemRepair->ShopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemRepair->Money), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemUUID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemRepair->ItemUUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOItemRepair->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOItemRepair(void* pData)
{
	GOItemRepair* pkGOItemRepair = (GOItemRepair*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemRepair->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemRepair->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ShopID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemRepair->ShopID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemRepair->Money), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemUUID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemRepair->ItemUUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOItemRepair->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOItemRepair);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOMarketTrade(void* pData)
{
	GOMarketTrade* pkGOMarketTrade = (GOMarketTrade*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMarketTrade->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMarketTrade->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Diamond
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMarketTrade->Diamond), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Credit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMarketTrade->Credit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMarketTrade->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMarketTrade->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOMarketTrade(void* pData)
{
	GOMarketTrade* pkGOMarketTrade = (GOMarketTrade*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMarketTrade->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMarketTrade->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Diamond
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMarketTrade->Diamond), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Credit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMarketTrade->Credit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMarketTrade->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMarketTrade->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOMarketTrade);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOWarehouseOpen(void* pData)
{
	GOWarehouseOpen* pkGOWarehouseOpen = (GOWarehouseOpen*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseOpen->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseOpen->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WarehouseID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseOpen->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode HasPassword
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOWarehouseOpen->HasPassword), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseOpen->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapX
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseOpen->MapX), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapY
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseOpen->MapY), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOWarehouseOpen(void* pData)
{
	GOWarehouseOpen* pkGOWarehouseOpen = (GOWarehouseOpen*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseOpen->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseOpen->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WarehouseID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseOpen->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode HasPassword
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOWarehouseOpen->HasPassword), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseOpen->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapX
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseOpen->MapX), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapY
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseOpen->MapY), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOWarehouseOpen);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOWarehouseItemTransport(void* pData)
{
	GOWarehouseItemTransport* pkGOWarehouseItemTransport = (GOWarehouseItemTransport*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseItemTransport->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseItemTransport->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WarehouseID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseItemTransport->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TransportType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOWarehouseItemTransport->TransportType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemUUID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseItemTransport->ItemUUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseItemTransport->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseItemTransport->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOWarehouseItemTransport(void* pData)
{
	GOWarehouseItemTransport* pkGOWarehouseItemTransport = (GOWarehouseItemTransport*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseItemTransport->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseItemTransport->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WarehouseID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseItemTransport->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TransportType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOWarehouseItemTransport->TransportType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemUUID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseItemTransport->ItemUUID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemTypeID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseItemTransport->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseItemTransport->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOWarehouseItemTransport);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOWarehouseMoneyTransport(void* pData)
{
	GOWarehouseMoneyTransport* pkGOWarehouseMoneyTransport = (GOWarehouseMoneyTransport*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseMoneyTransport->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseMoneyTransport->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WarehouseID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseMoneyTransport->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TransportType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOWarehouseMoneyTransport->TransportType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Money
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseMoneyTransport->Money), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOWarehouseMoneyTransport(void* pData)
{
	GOWarehouseMoneyTransport* pkGOWarehouseMoneyTransport = (GOWarehouseMoneyTransport*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseMoneyTransport->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseMoneyTransport->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WarehouseID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseMoneyTransport->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TransportType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOWarehouseMoneyTransport->TransportType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Money
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseMoneyTransport->Money), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOWarehouseMoneyTransport);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOWarehouseExtend(void* pData)
{
	GOWarehouseExtend* pkGOWarehouseExtend = (GOWarehouseExtend*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseExtend->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseExtend->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WarehouseID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseExtend->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SlotNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehouseExtend->SlotNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOWarehouseExtend(void* pData)
{
	GOWarehouseExtend* pkGOWarehouseExtend = (GOWarehouseExtend*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseExtend->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseExtend->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WarehouseID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseExtend->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SlotNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehouseExtend->SlotNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOWarehouseExtend);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOWarehousePrivateOP(void* pData)
{
	GOWarehousePrivateOP* pkGOWarehousePrivateOP = (GOWarehousePrivateOP*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehousePrivateOP->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehousePrivateOP->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WarehouseID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOWarehousePrivateOP->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PrivateType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOWarehousePrivateOP->PrivateType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOWarehousePrivateOP->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOWarehousePrivateOP(void* pData)
{
	GOWarehousePrivateOP* pkGOWarehousePrivateOP = (GOWarehousePrivateOP*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehousePrivateOP->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehousePrivateOP->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WarehouseID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOWarehousePrivateOP->WarehouseID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PrivateType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOWarehousePrivateOP->PrivateType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOWarehousePrivateOP->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOWarehousePrivateOP);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOMonsterDead(void* pData)
{
	GOMonsterDead* pkGOMonsterDead = (GOMonsterDead*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMonsterDead->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMonsterDead->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MapID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMonsterDead->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CreaterID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMonsterDead->CreaterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MonsterID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMonsterDead->MonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode KillerID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMonsterDead->KillerID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode KillerLevel
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOMonsterDead->KillerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOMonsterDead(void* pData)
{
	GOMonsterDead* pkGOMonsterDead = (GOMonsterDead*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMonsterDead->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMonsterDead->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MapID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMonsterDead->MapID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CreaterID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMonsterDead->CreaterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MonsterID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMonsterDead->MonsterID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode KillerID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMonsterDead->KillerID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode KillerLevel
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOMonsterDead->KillerLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOMonsterDead);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GORealInstanceLifeCycle(void* pData)
{
	GORealInstanceLifeCycle* pkGORealInstanceLifeCycle = (GORealInstanceLifeCycle*)(pData);

	//EnCode Time
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORealInstanceLifeCycle->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode InstanceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORealInstanceLifeCycle->InstanceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WorldID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGORealInstanceLifeCycle->WorldID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CyclePoint
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGORealInstanceLifeCycle->CyclePoint), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GORealInstanceLifeCycle(void* pData)
{
	GORealInstanceLifeCycle* pkGORealInstanceLifeCycle = (GORealInstanceLifeCycle*)(pData);

	//DeCode Time
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORealInstanceLifeCycle->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode InstanceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORealInstanceLifeCycle->InstanceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WorldID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGORealInstanceLifeCycle->WorldID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CyclePoint
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGORealInstanceLifeCycle->CyclePoint), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GORealInstanceLifeCycle);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__TixNew_i(void* pData)
{
	TixNew_i* pkTixNew_i = (TixNew_i*)(pData);

	//EnCode ID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->ID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TypeStringSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->TypeStringSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TypeString
	if(MAX_TYPE_SIZE < pkTixNew_i->TypeStringSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->TypeStringSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixNew_i->TypeString), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Time
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->AccountNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountName
	if(MAX_NAME_SIZE < pkTixNew_i->AccountNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->AccountNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixNew_i->AccountName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->RoleNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleName
	if(MAX_NAME_SIZE < pkTixNew_i->RoleNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->RoleNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixNew_i->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TitleSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->TitleSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Title
	if(MAX_TIX_TITLE_SIZE < pkTixNew_i->TitleSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->TitleSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixNew_i->Title), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode InfoSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->InfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Info
	if(MAX_TIX_INFO_SIZE < pkTixNew_i->InfoSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->InfoSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixNew_i->Info), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RealmNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->RealmNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RealmName
	if(MAX_SRV_NAME_SIZE < pkTixNew_i->RealmNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->RealmNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixNew_i->RealmName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WorldNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixNew_i->WorldNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WorldName
	if(MAX_SRV_NAME_SIZE < pkTixNew_i->WorldNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->WorldNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixNew_i->WorldName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__TixNew_i(void* pData)
{
	TixNew_i* pkTixNew_i = (TixNew_i*)(pData);

	//DeCode ID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->ID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TypeStringSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->TypeStringSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TypeString
	if(MAX_TYPE_SIZE < pkTixNew_i->TypeStringSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->TypeStringSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixNew_i->TypeString), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Time
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->AccountNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountName
	if(MAX_NAME_SIZE < pkTixNew_i->AccountNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->AccountNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixNew_i->AccountName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->RoleNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleName
	if(MAX_NAME_SIZE < pkTixNew_i->RoleNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->RoleNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixNew_i->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TitleSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->TitleSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Title
	if(MAX_TIX_TITLE_SIZE < pkTixNew_i->TitleSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->TitleSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixNew_i->Title), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode InfoSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->InfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Info
	if(MAX_TIX_INFO_SIZE < pkTixNew_i->InfoSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->InfoSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixNew_i->Info), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RealmNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->RealmNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RealmName
	if(MAX_SRV_NAME_SIZE < pkTixNew_i->RealmNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->RealmNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixNew_i->RealmName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WorldNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixNew_i->WorldNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WorldName
	if(MAX_SRV_NAME_SIZE < pkTixNew_i->WorldNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixNew_i->WorldNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixNew_i->WorldName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(TixNew_i);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOTixNew(void* pData)
{
	GOTixNew* pkGOTixNew = (GOTixNew*)(pData);

	//EnCode LocalTixID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTixNew->LocalTixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TixInfo
	if(EnCode__TixNew_i(&(pkGOTixNew->TixInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOTixNew(void* pData)
{
	GOTixNew* pkGOTixNew = (GOTixNew*)(pData);

	//DeCode LocalTixID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTixNew->LocalTixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TixInfo
	if(DeCode__TixNew_i(&(pkGOTixNew->TixInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOTixNew);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__OGTixNewRsp(void* pData)
{
	OGTixNewRsp* pkOGTixNewRsp = (OGTixNewRsp*)(pData);

	//EnCode LocalTixID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkOGTixNewRsp->LocalTixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TixID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkOGTixNewRsp->TixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrorNo
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkOGTixNewRsp->ErrorNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__OGTixNewRsp(void* pData)
{
	OGTixNewRsp* pkOGTixNewRsp = (OGTixNewRsp*)(pData);

	//DeCode LocalTixID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkOGTixNewRsp->LocalTixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TixID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkOGTixNewRsp->TixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrorNo
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkOGTixNewRsp->ErrorNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(OGTixNewRsp);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__GOTixMod(void* pData)
{
	GOTixMod* pkGOTixMod = (GOTixMod*)(pData);

	//EnCode LocalTixID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTixMod->LocalTixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TixID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTixMod->TixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ModType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGOTixMod->ModType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DueNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTixMod->DueNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DueName
	if(MAX_NAME_SIZE < pkGOTixMod->DueNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOTixMod->DueNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkGOTixMod->DueName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseTypeStringSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTixMod->CloseTypeStringSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseTypeString
	if(MAX_TYPE_SIZE < pkGOTixMod->CloseTypeStringSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOTixMod->CloseTypeStringSize;
	if(!m_kPackage.Pack("CHAR", &(pkGOTixMod->CloseTypeString), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseInfoSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGOTixMod->CloseInfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseInfo
	if(MAX_TIX_CLOSE_INFO_SIZE < pkGOTixMod->CloseInfoSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOTixMod->CloseInfoSize;
	if(!m_kPackage.Pack("CHAR", &(pkGOTixMod->CloseInfo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__GOTixMod(void* pData)
{
	GOTixMod* pkGOTixMod = (GOTixMod*)(pData);

	//DeCode LocalTixID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTixMod->LocalTixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TixID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTixMod->TixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ModType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGOTixMod->ModType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DueNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTixMod->DueNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DueName
	if(MAX_NAME_SIZE < pkGOTixMod->DueNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOTixMod->DueNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGOTixMod->DueName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseTypeStringSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTixMod->CloseTypeStringSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseTypeString
	if(MAX_TYPE_SIZE < pkGOTixMod->CloseTypeStringSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOTixMod->CloseTypeStringSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGOTixMod->CloseTypeString), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseInfoSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGOTixMod->CloseInfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseInfo
	if(MAX_TIX_CLOSE_INFO_SIZE < pkGOTixMod->CloseInfoSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGOTixMod->CloseInfoSize;
	if(!m_kPackage.UnPack("CHAR", &(pkGOTixMod->CloseInfo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GOTixMod);
}

size_t	LOG_SVC_NS::LogSvcProtocol::EnCode__OGTixModRsp(void* pData)
{
	OGTixModRsp* pkOGTixModRsp = (OGTixModRsp*)(pData);

	//EnCode LocalTixID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkOGTixModRsp->LocalTixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TixID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkOGTixModRsp->TixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ModType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkOGTixModRsp->ModType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ErrorNo
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkOGTixModRsp->ErrorNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LOG_SVC_NS::LogSvcProtocol::DeCode__OGTixModRsp(void* pData)
{
	OGTixModRsp* pkOGTixModRsp = (OGTixModRsp*)(pData);

	//DeCode LocalTixID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkOGTixModRsp->LocalTixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TixID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkOGTixModRsp->TixID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ModType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkOGTixModRsp->ModType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ErrorNo
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkOGTixModRsp->ErrorNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(OGTixModRsp);
}

