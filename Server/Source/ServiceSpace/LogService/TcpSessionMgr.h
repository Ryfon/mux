﻿#ifndef TCPSESSIONMGR_H
#define TCPSESSIONMGR_H

#include "inc_ace.h"
#include "inc_std.h"
#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/TcpSession.h"
#include "../ServiceSpaceLib/MessageBlock.h"

__SERVICE_SPACE_BEGIN_NS__

class TcpSessionMgr
{
public:
    static TcpSessionMgr* instance(void)
    {
        static TcpSessionMgr single_inst;
        return (&single_inst);
    }

public:
    void update_session(TcpSession* ts);
    void remove_session(TcpSession* ts);
    TcpSession* find_session(unsigned int id);
    int send_pkg(unsigned int session_id, unsigned int pkg_cmd, void* pkg, unsigned char mt);

protected:
    ACE_Message_Block* build_pkg(unsigned int pkg_cmd, void* raw, unsigned char mt);

private:
    ACE_Token   session_lock_;
    std::map<unsigned int, TcpSession*> session_map_;
};
#define TSMGR   TcpSessionMgr::instance()

__SERVICE_SPACE_END_NS__

#endif//TCPSESSIONMGR_H

