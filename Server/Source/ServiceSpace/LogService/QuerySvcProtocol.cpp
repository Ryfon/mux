﻿#include "QuerySvcProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

QUE_SVC_NS::QuerySvcProtocol::QuerySvcProtocol()
{
	m_mapEnCodeFunc[0x2c01]	=	&QuerySvcProtocol::EnCode__TOQueryTix;
	m_mapDeCodeFunc[0x2c01]	=	&QuerySvcProtocol::DeCode__TOQueryTix;

	m_mapEnCodeFunc[0x7c01]	=	&QuerySvcProtocol::EnCode__OTQueryTixRsp;
	m_mapDeCodeFunc[0x7c01]	=	&QuerySvcProtocol::DeCode__OTQueryTixRsp;

}

QUE_SVC_NS::QuerySvcProtocol::~QuerySvcProtocol()
{
}

size_t	QUE_SVC_NS::QuerySvcProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	QUE_SVC_NS::QuerySvcProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

QUE_SVC_NS::EnCodeFunc	QUE_SVC_NS::QuerySvcProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

QUE_SVC_NS::DeCodeFunc	QUE_SVC_NS::QuerySvcProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	QUE_SVC_NS::QuerySvcProtocol::EnCode__TixInfo_i(void* pData)
{
	TixInfo_i* pkTixInfo_i = (TixInfo_i*)(pData);

	//EnCode ID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->ID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TypeStringSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->TypeStringSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TypeString
	if(MAX_TYPE_SIZE < pkTixInfo_i->TypeStringSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->TypeStringSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->TypeString), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Time
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Wait
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->Wait), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->AccountNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountName
	if(MAX_NAME_SIZE < pkTixInfo_i->AccountNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->AccountNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->AccountName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->RoleNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleName
	if(MAX_NAME_SIZE < pkTixInfo_i->RoleNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->RoleNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DueNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->DueNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DueName
	if(MAX_NAME_SIZE < pkTixInfo_i->DueNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->DueNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->DueName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TitleSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->TitleSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Title
	if(MAX_TIX_TITLE_SIZE < pkTixInfo_i->TitleSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->TitleSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->Title), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode InfoSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->InfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Info
	if(MAX_TIX_INFO_SIZE < pkTixInfo_i->InfoSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->InfoSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->Info), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode State
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkTixInfo_i->State), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseTypeStringSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->CloseTypeStringSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseTypeString
	if(MAX_TYPE_SIZE < pkTixInfo_i->CloseTypeStringSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->CloseTypeStringSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->CloseTypeString), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseTime
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->CloseTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseInfoSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->CloseInfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CloseInfo
	if(MAX_TIX_CLOSE_INFO_SIZE < pkTixInfo_i->CloseInfoSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->CloseInfoSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->CloseInfo), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RealmNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->RealmNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RealmName
	if(MAX_SRV_NAME_SIZE < pkTixInfo_i->RealmNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->RealmNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->RealmName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WorldNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTixInfo_i->WorldNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode WorldName
	if(MAX_SRV_NAME_SIZE < pkTixInfo_i->WorldNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->WorldNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTixInfo_i->WorldName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	QUE_SVC_NS::QuerySvcProtocol::DeCode__TixInfo_i(void* pData)
{
	TixInfo_i* pkTixInfo_i = (TixInfo_i*)(pData);

	//DeCode ID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->ID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TypeStringSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->TypeStringSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TypeString
	if(MAX_TYPE_SIZE < pkTixInfo_i->TypeStringSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->TypeStringSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->TypeString), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Time
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->Time), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Wait
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->Wait), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->AccountNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountName
	if(MAX_NAME_SIZE < pkTixInfo_i->AccountNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->AccountNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->AccountName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->RoleNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleName
	if(MAX_NAME_SIZE < pkTixInfo_i->RoleNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->RoleNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DueNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->DueNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DueName
	if(MAX_NAME_SIZE < pkTixInfo_i->DueNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->DueNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->DueName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TitleSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->TitleSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Title
	if(MAX_TIX_TITLE_SIZE < pkTixInfo_i->TitleSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->TitleSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->Title), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode InfoSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->InfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Info
	if(MAX_TIX_INFO_SIZE < pkTixInfo_i->InfoSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->InfoSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->Info), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode State
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkTixInfo_i->State), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseTypeStringSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->CloseTypeStringSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseTypeString
	if(MAX_TYPE_SIZE < pkTixInfo_i->CloseTypeStringSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->CloseTypeStringSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->CloseTypeString), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseTime
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->CloseTime), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseInfoSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->CloseInfoSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CloseInfo
	if(MAX_TIX_CLOSE_INFO_SIZE < pkTixInfo_i->CloseInfoSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->CloseInfoSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->CloseInfo), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RealmNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->RealmNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RealmName
	if(MAX_SRV_NAME_SIZE < pkTixInfo_i->RealmNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->RealmNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->RealmName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WorldNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTixInfo_i->WorldNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode WorldName
	if(MAX_SRV_NAME_SIZE < pkTixInfo_i->WorldNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTixInfo_i->WorldNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTixInfo_i->WorldName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(TixInfo_i);
}

size_t	QUE_SVC_NS::QuerySvcProtocol::EnCode__TOQueryTix(void* pData)
{
	TOQueryTix* pkTOQueryTix = (TOQueryTix*)(pData);

	//EnCode ID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTOQueryTix->ID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTOQueryTix->AccountNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountName
	if(MAX_NAME_SIZE < pkTOQueryTix->AccountNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTOQueryTix->AccountNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTOQueryTix->AccountName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleNameSize
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTOQueryTix->RoleNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleName
	if(MAX_NAME_SIZE < pkTOQueryTix->AccountNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTOQueryTix->AccountNameSize;
	if(!m_kPackage.Pack("CHAR", &(pkTOQueryTix->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode State
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkTOQueryTix->State), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TimeLowLimit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTOQueryTix->TimeLowLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode TimeHighLimit
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTOQueryTix->TimeHighLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	QUE_SVC_NS::QuerySvcProtocol::DeCode__TOQueryTix(void* pData)
{
	TOQueryTix* pkTOQueryTix = (TOQueryTix*)(pData);

	//DeCode ID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTOQueryTix->ID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTOQueryTix->AccountNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountName
	if(MAX_NAME_SIZE < pkTOQueryTix->AccountNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTOQueryTix->AccountNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTOQueryTix->AccountName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleNameSize
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTOQueryTix->RoleNameSize), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleName
	if(MAX_NAME_SIZE < pkTOQueryTix->AccountNameSize)
	{
		return FAILEDRETCODE;
	}

	unCount = pkTOQueryTix->AccountNameSize;
	if(!m_kPackage.UnPack("CHAR", &(pkTOQueryTix->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode State
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkTOQueryTix->State), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TimeLowLimit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTOQueryTix->TimeLowLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode TimeHighLimit
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTOQueryTix->TimeHighLimit), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(TOQueryTix);
}

size_t	QUE_SVC_NS::QuerySvcProtocol::EnCode__OTQueryTixRsp(void* pData)
{
	OTQueryTixRsp* pkOTQueryTixRsp = (OTQueryTixRsp*)(pData);

	//EnCode ID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkOTQueryTixRsp->ID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode More
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkOTQueryTixRsp->More), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RefInfo
	if(EnCode__TixInfo_i(&(pkOTQueryTixRsp->RefInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	QUE_SVC_NS::QuerySvcProtocol::DeCode__OTQueryTixRsp(void* pData)
{
	OTQueryTixRsp* pkOTQueryTixRsp = (OTQueryTixRsp*)(pData);

	//DeCode ID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkOTQueryTixRsp->ID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode More
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkOTQueryTixRsp->More), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RefInfo
	if(DeCode__TixInfo_i(&(pkOTQueryTixRsp->RefInfo)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(OTQueryTixRsp);
}

