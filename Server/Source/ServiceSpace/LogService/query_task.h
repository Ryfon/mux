﻿#ifndef QUERY_TASK_H
#define QUERY_TASK_H

#include "inc_ace.h"
#include "inc_std.h"
#include "task_msg.h"
#include "DBAccess.h"

#include "../ServiceSpaceLib/StdHdr.h"


__SERVICE_SPACE_BEGIN_NS__

class query_task : public ACE_Task<ACE_MT_SYNCH>
{
public:
    int set_db_params( const char* dbname, 
                       const char* host,
                       const char* password,
                       const char* port,
                       const char* user );

    int open (void *args = 0);
    int svc (void);

protected:
    int handle_query(tmsg* lg);
    int build_sql(tmsg* lg, std::string& sql);
    int handle_result(tmsg* lg, sql::ResultSet* rs);

    int build_tix_select_sql(void* pkg, std::string& sql);
    int handle_query_tix_result(tmsg* lg, sql::ResultSet* rs);

private:
    int connection_id_;
};

__SERVICE_SPACE_END_NS__

#endif//QUERY_TASK_H
