﻿
#define ACE_BUILD_SVC_DLL

#include "../ServiceSpaceLib/ObjectMessageBlock.h"
#include "../ServiceSpaceLib/TcpService.h"
#include "../ServiceSpaceLib/GetOpts.h"

#include "../ServiceSpaceLib/StreamBuffer.h"
#include "../ServiceSpaceLib/utilib/split.h"


#include "LogService.h"
#include "LogSvcProtocol.h"
#include "QuerySvcProtocol.h"
#include "PkgUtil.h"
#include "TcpSessionMgr.h"
#include "Log.h"

using namespace LOG_SVC_NS;
using namespace QUE_SVC_NS;

#ifdef WIN32
    #pragma warning(disable : 4311)
#endif//WIN32

__SERVICE_SPACE_BEGIN_NS__

void LogServiceMessageHandler::parse_db_param(std::string str)
{	

	utilib::vectorN<utilib::stringN<16>, 6> vec;
	utilib::split(vec, str.c_str(), str.c_str() + str.size(), ':');
	if (vec.size()>=5)
	{		
        log_task_.set_db_params(vec[2].c_str(), 
                                vec[0].c_str(), 
                                vec[4].c_str(), 
                                vec[1].c_str(), 
                                vec[3].c_str());

        query_task_.set_db_params(vec[2].c_str(), 
                                  vec[0].c_str(), 
                                  vec[4].c_str(), 
                                  vec[1].c_str(), 
                                  vec[3].c_str());
		
	}else
	{
		__ERR(ACE_TEXT("LSMH::parse_db_param|vec.size()=%d \n"), vec.size()) ;
	}
}

int LogServiceMessageHandler::init(int argc, ACE_TCHAR * argv[])
{	
    __DBG( "LSMH::init\n" ) ;

    GetOpts<ACE_TCHAR> opts(argc, argv);

	//int err = 0;

	const ACE_TCHAR * options = ACE_TEXT(":h:p:z:");
	ACE_Get_Opt get_opt(argc, (ACE_TCHAR **)opts.argv, options);
	int c; 

	std::string db_str ;

	while((c = get_opt()) != EOF)
	{
		switch(c)
		{
		case 'h':
			{
				unsigned long addr = ::inet_addr(get_opt.optarg);
				listen_addr_.set_address((const char *)&addr, sizeof(addr), 0);
			}
			break;
		case 'p':
			listen_addr_.set_port_number(ACE_OS::atoi(get_opt.optarg));
			break;	
		case 'z': //数据库ip地址
			{
				db_str = get_opt.optarg ;
				break ;
			}	
		}
	}		

	parse_db_param(db_str) ;	

    int result = base::init(argc,argv) ;

    return (result);
}


int LogServiceMessageHandler::open(void *args /* = 0*/)
{
    __DBG( ACE_TEXT("LSMH::open\n") ) ;

	//this->threads_ = 4;
	int err = base::open(args);	
	if (-1 == err)
		return -1;	

    // init database access & logic process task
    if ( -1 == log_task_.open() )
    {
        __ERR(ACE_TEXT("log task open failed(%m)\n"));
        return -1;
    }
    if ( -1 == query_task_.open() )
    {
        __ERR(ACE_TEXT("query task open failed(%m)\n"));
        return -1;
    }
    if ( -1 == logic_task_.open() )
    {
        __ERR(ACE_TEXT("logic task open failed(%m)\n"));
        return -1;
    }

#ifdef WIN32
	TcpService * tcp = ACE_Dynamic_Service<TcpService>::instance("listener");
#else
	const ACE_Service_Type * svc_rec;
	if (ACE_Service_Repository::instance()->find("listener", &svc_rec) == -1)
		return -1;
	const ACE_Service_Type_Impl * type = svc_rec->type();
	if (type == 0)
		return -1;
	ACE_Service_Object * obj = ACE_static_cast (ACE_Service_Object *, type->object());
	TcpService * tcp = (TcpService *)obj;
#endif
	tcp->create_tcp_acceptor(listen_addr_, 0);


	return 0;
}

int LogServiceMessageHandler::handle_task_message(ACE_Message_Block * message)
{
	ObjectMessageBlock<TcpService::Tcp_Notify> * notif_message = (ObjectMessageBlock<TcpService::Tcp_Notify> *)message;
	TcpService::Tcp_Notify & notify = notif_message->obj();
    UINT session_id = (UINT)notify.session;
	int err = -1;
	if (notify.flag == TcpService::Tcp_Notify::TCP_RECEIVED)
	{
		ACE_Message_Block * real_message = notify.message;
		err = StreamBuffer(real_message, 0, 2, false);
		if (err == 0)
		{
			handle_message(real_message, session_id);
			while (real_message->cont())
			{
				handle_message(real_message->cont(), session_id);
				real_message = real_message->cont();
			}
		}
		else if (err == 1)
		{
			while (real_message->cont())
			{
				handle_message(real_message, session_id);
				ACE_Message_Block * tmp = real_message;
				real_message = real_message->cont();
				if (!real_message->cont())
				{
					tmp->cont(0);
				}
			}

			if (notify.message == real_message)
				notify.message = 0;
			notify.session->post_asynch_read(real_message);

		}
		else if (err == -1)
		{
			__ERR(ACE_TEXT("error message from [%s:%d]\n"), notify.session->remote_address().get_host_addr(), 
				notify.session->remote_address().get_port_number());
		}

		if (err != 1)
		{
			notify.session->post_asynch_read();
		}
	}
	else if (notify.flag == TcpService::Tcp_Notify::TCP_SESSION_BUILT && check_session(notify.session))
	{
		__DBG(ACE_TEXT("session[%s:%d] built\n"), notify.session->remote_address().get_host_addr(),
			notify.session->remote_address().get_port_number());

        TSMGR->update_session(notify.session);

	}
	else if (notify.flag == TcpService::Tcp_Notify::TCP_SESSION_CLOSED && check_session(notify.session))
	{
		__DBG(ACE_TEXT("session[%s:%d] closed\n"), notify.session->remote_address().get_host_addr(),
			notify.session->remote_address().get_port_number());

        TSMGR->remove_session(notify.session);
	}
	message->release();
	return err;
}

int LogServiceMessageHandler::handle_message( ACE_Message_Block* mb, UINT session_id )
{
    static LogSvcProtocol   logd;
    static QuerySvcProtocol queryd;

    static char buff[MAX_MESSAGE_LEN];
    UINT cmd = PkgUtil::pkg_cmd(mb->rd_ptr(), mb->length());
    if ( 0 == cmd )
    {
        __ERR(ACE_TEXT("LSMH::handle_message|unkown message\n"));
        return -1;
    }
    else
    {
        __DBG(ACE_TEXT("LSMH::handle_message|new message = 0x%04x\n"), cmd);
    }

    size_t len = 0;
    if ( cmd >= LOG_SVC_NS::MIN_LOG_ID &&
         cmd <= LOG_SVC_NS::MAX_LOG_ID)
    {
        len = logd.DeCode(cmd, 
             buff, 
             sizeof(buff), 
             mb->rd_ptr()+PkgUtil::PKG_HEAD_LEN, 
             mb->length()-PkgUtil::PKG_HEAD_LEN);
    }
    else
    {
        len = queryd.DeCode(cmd, 
            buff, 
            sizeof(buff), 
            mb->rd_ptr()+PkgUtil::PKG_HEAD_LEN, 
            mb->length()-PkgUtil::PKG_HEAD_LEN);
    }

    if ( UINT(-1) ==  (UINT)len )
    {
        __ERR(ACE_TEXT("LSMH::handle_message|decode message failed\n"));
        return -1;
    }

    ACE_Message_Block* msg = new ACE_Message_Block(sizeof(tmsg));
    if ( 0 == msg )
    {
        __ERR(ACE_TEXT("LSMH::handle_message|alloc mem failed\n"));
        return -1;
    }
    if ( sizeof(tmsg) != msg->space() )
    {
        __ERR(ACE_TEXT("LSMH::handle_message|alloc msg failed\n"));
        return -1;
    }

    tmsg* l = (tmsg*)msg->wr_ptr();
    l->id = session_id;
    l->cmd = cmd;
    memcpy(l->msg, buff, len);
    msg->wr_ptr(sizeof(tmsg));

    ACE_Message_Block* m1 = msg->duplicate();
    ACE_Message_Block* m2 = msg->duplicate();
    ACE_Message_Block* m3 = msg->duplicate();
    log_task_  .putq(m1);
    logic_task_.putq(m2);
    query_task_.putq(m3);
    msg->release();

    return 0;
}

extern "C" ACE_Svc_Export LogServiceMessageHandler * CreateMessageHandler()
{
    return new LogServiceMessageHandler();
}

__SERVICE_SPACE_END_NS__

