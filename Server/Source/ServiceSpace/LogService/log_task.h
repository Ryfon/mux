﻿#ifndef LOG_TASK_H
#define LOG_TASK_H

#include <ace/Task_T.h>
#include "inc_std.h"
#include "task_msg.h"
#include "LogSvcProtocol.h"
using namespace LOG_SVC_NS;

#include "../ServiceSpaceLib/StdHdr.h"

__SERVICE_SPACE_BEGIN_NS__

class log_task : public ACE_Task<ACE_MT_SYNCH>
{
public:
    int set_db_params( const char* dbname, 
                       const char* host,
                       const char* password,
                       const char* port,
                       const char* user );

    int open (void *args = 0);
    int svc (void);

protected:
    int buildSQL(tmsg* lg, vector<string>& sql);
    int buildAccoutLoginInsertSQL(GOAccountLogin* req, vector<string>& sql);
    int buildRoleEnterInsertSQL(GORoleEnter* req, vector<string>& sql);
    int buildRoleExitInsertSQL(GORoleExit* req, vector<string>& sql);
    int buildAccoutLogoffInsertSQL(GOAccountLogoff* req, vector<string>& sql);
    int buildRoleLevelupInsertSQL(GORoleLevelUP* req, vector<string>& sql);
    int buildSkillLevelupInsertSQL(GOSkillLevelUP* req, vector<string>& sql);
    int buildRoleDeadInsertSQL(GORoleDead* req, vector<string>& sql);
    int buildRoleItemOwnInsertSQL(GORoleItemOwn* req, vector<string>& sql);
    int buildRoleItemUseInsertSQL(GORoleItemUse* req, vector<string>& sql);
    int buildRoleItemDropInsertSQL(GORoleItemDrop* req, vector<string>& sql);
    int buildRoleMapChangeInsertSQL(GORoleMapChange* req, vector<string>& sql);
    int buildRoleRelationChangeInsertSQL(GORoleRelationChange* req, vector<string>& sql);
    int buildRoleQuestChangeInsertSQL(GORoleQuestChange* req, vector<string>& sql);
    int buildRoleUnionChangeInsertSQL(GORoleUnionChange* req, vector<string>& sql);
    int buildRoleTradeInsertSQL(GORoleTrade* req, vector<string>& sql);
    int buildSPChangeInsertSQL(GOSPChange* req, vector<string>& sql);
    int buildItemPropertyChangeInsertSQL(GOItemPropertyChange* req, vector<string>& sql);
    int buildMoneyChangeInsertSQL(GOMoneyChange* req, vector<string>& sql);
    int buildTeamChangeInsertSQL(GOTeamChange* req, vector<string>& sql);
    int buildPetAddInsertSQL(GOPetAdd* req, vector<string>& sql);
    int buildPetLevelupInsertSQL(GOPetLevelup* req, vector<string>& sql);
    int buildPetFunctionAddInsertSQL(GOPetFunctionAdd* req, vector<string>& sql);
    int buildPetStateChangeInsertSQL(GOPetStateChange* req, vector<string>& sql);
    int buildUseSkillInsertSQL(GOUseSkill* req, vector<string>& sql);
    int buildKillPlayerInsertSQL(GOKillPlayer* req, vector<string>& sql);
    int buildReliveInsertSQL(GORelive* req, vector<string>& sql);
    int buildSendMailInsertSQL(GOSendMail* req, vector<string>& sql);
    int buildShopTradeInsertSQL(GOShopTrade* req, vector<string>& sql);
    int buildItemRepairInsertSQL(GOItemRepair* req, vector<string>& sql);
    int buildMarketTradeInsertSQL(GOMarketTrade* req, vector<string>& sql);
    int buildWarehouseOpenInsertSQL(GOWarehouseOpen* req, vector<string>& sql);
    int buildWarehouseItemTransportInsertSQL(GOWarehouseItemTransport* req, vector<string>& sql);
    int buildWarehouseMoneyTransportInsertSQL(GOWarehouseMoneyTransport* req, vector<string>& sql);
    int buildWarehouseExtendInsertSQL(GOWarehouseExtend* req, vector<string>& sql);
    int buildWarehousePrivateOPInsertSQL(GOWarehousePrivateOP* req, vector<string>& sql);
    int buildMonsterDeadInsertSQL(GOMonsterDead* req, vector<string>& sql);
    int buildRealInstanceLifecycleInsertSQL(GORealInstanceLifeCycle* req, vector<string>& sql);
    int buildTixNewInsertSQL(GOTixNew* req, vector<string>& sql);
    int buildTixModInsertSQL(GOTixMod* req, vector<string>& sql);

    int handleTixNewRsp(tmsg* lg);
    int handleTixModRsp(tmsg* lg);

    int handle_log(tmsg* lg);
    int handle_rsp(tmsg* lg);
    int getAID(void);
    int getTID(void);

private:
    int connection_id_;

};

__SERVICE_SPACE_END_NS__

#endif//LOG_TASK_H


