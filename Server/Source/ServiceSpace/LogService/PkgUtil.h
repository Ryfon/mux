﻿#ifndef PKG_UTIL_H
#define PKG_UTIL_H

#include "LogSvcProtocol.h"
#include "QuerySvcProtocol.h"

class PkgUtil
{
public:
    // 消息结构定义
    enum PkgDefine
    {
        PKG_HEAD_LEN        = 5,    //消息头长度
        PKG_MAX_LEN         = 512,  //最大消息长度
        PKG_OFF_LEN         = 0,    //消息中标记消息长度字段的偏移
        PKG_OFF_TYP         = 2,    //
        PKG_OFF_CMD         = 3,    //消息中标记消息类型字段的偏移
    };
    typedef USHORT PKG_LEN_TYPE;    //消息中标记消息长度字段的类型
    typedef USHORT PKG_CMD_TYPE;    //消息中标记消息长度字段的类型
    typedef BYTE   PKG_TYP_TYPE;    //

    // 消息类型
    enum CmdClass
    {
        CCT_UNKOWN   =   0x0000, //未知类别的消息
        CCT_LOG      =   0x0001, //需要进行日志记录的消息
        CCT_LOGIC    =   0x0002, //需要进行逻辑处理的消息
        CCT_QUERY    =   0x0004, //需要进行数据查询的消息
    };

    // 协议类型
    enum PkgMakeType
    {
        MT_LOS  = 1,     // 日志
        MT_LOQ  = 2,     // 查询
    };


    // 获取消息的类别
    static UINT pkg_class(UINT cmd)
    {
        UINT cls = CCT_UNKOWN;

        // 需要日志记录
        if ( cmd >= LOG_SVC_NS::G_O_ACCOUNT_LOGIN &&
             cmd <= LOG_SVC_NS::G_O_REAL_INSTANCE_LIFECYCLE )
        {
            cls |= CCT_LOG;
        }

        // 需要逻辑处理
        if ( cmd == LOG_SVC_NS::G_O_ACCOUNT_LOGIN ||
             cmd == LOG_SVC_NS::G_O_ACCOUNT_LOGOFF )
        {
            cls |= CCT_LOGIC;
        }

        // 需要日志查询
        // ...

        return cls;
    }

    // 获取消息的类型
    static UINT pkg_cmd(const char* pkg, int len)
    {
        UINT cmd = 0;
        if ( len >= PKG_HEAD_LEN && len <= PKG_MAX_LEN )
        {
            PKG_CMD_TYPE* p = (PKG_CMD_TYPE*)(pkg + PKG_OFF_CMD);
            cmd = (UINT)p[0];
        }

        return cmd;
    }

    // 创建消息头
    static int build_pkg_head(char* pkg, PKG_LEN_TYPE len, PKG_TYP_TYPE typ, PKG_CMD_TYPE cmd)
    {
        PKG_LEN_TYPE* pl = (PKG_LEN_TYPE*)(pkg+PKG_OFF_LEN);
        pl[0] = len;

        PKG_TYP_TYPE* pt = (PKG_TYP_TYPE*)(pkg+PKG_OFF_TYP);
        pt[0] = typ;

        PKG_CMD_TYPE* pc = (PKG_CMD_TYPE*)(pkg+PKG_OFF_CMD);
        pc[0] = cmd;

        return 0;
    }

    static const char* tix_mod_name(BYTE mod)
    {
        /*
        TMT_UNKOWN         	=	0,      	//未知操作
        TMT_INIT          	=	1,      	//尚未操作
        TMT_SUBSCRIBE      	=	2,      	//申请操作
        TMT_APPLY          	=	3,      	//受理操作
        TMT_PROCESS        	=	4,      	//处理操作
        TMT_CLOSE          	=	5,      	//关闭操作
        */
        static const char* modNames[] =
        {
            "",
            "",
            "申请",
            "受理",
            "处理",
            "关闭",
        };
        if ( mod > LOG_SVC_NS::TMT_CLOSE || mod < LOG_SVC_NS::TMT_INIT )
        {
            return modNames[0];
        }
        else
        {
            return modNames[mod];
        }
    }
    static const char* tix_state_name(BYTE state)
    {
        /*
        TST_UNKOWN 	    =	0,         	//未知状态
        TST_INIT   	    =	1,         	//原始状态
        TST_SUBSCRIBE   =   2,          //申请中状态
        TST_APPLY  	    =	3,         	//已分配状态
        TST_PROCESS	    =	4,         	//处理中状态
        TST_CLOSE  	    =	5,         	//已关闭状态
        */
        
        static const char* stateNames[] =
        {
            "",
            "",
            "申请中",
            "已受理",
            "处理中",
            "已关闭",
        };

        if ( state > QUE_SVC_NS::TST_CLOSE || state < QUE_SVC_NS::TST_INIT )
        {
            return stateNames[0];
        }
        else
        {
            return stateNames[state];
        }
    }
    static BYTE tix_state_id(const char* s)
    {
        /*
        TMT_UNKOWN         	=	0,      	//未知操作
        TMT_INIT          	=	1,      	//尚未操作
        TMT_SUBSCRIBE      	=	2,      	//申请操作
        TMT_APPLY          	=	3,      	//受理操作
        TMT_PROCESS        	=	4,      	//处理操作
        TMT_CLOSE          	=	5,      	//关闭操作
        */
        typedef struct
        {
            const char* Name;
            BYTE        ID;
        }StateNameID;

        static StateNameID entities[] =
        {
            { "",       QUE_SVC_NS::TST_INIT        },
            { "申请中", QUE_SVC_NS::TST_SUBSCRIBE   },
            { "已受理", QUE_SVC_NS::TST_APPLY       },
            { "处理中", QUE_SVC_NS::TST_PROCESS     },
            { "已关闭", QUE_SVC_NS::TST_CLOSE       },                
        };
        static const int iNum = sizeof(entities)/sizeof(entities[0]);
        for (int i = 0; i < iNum; ++i)
        {
            if ( !strcmp(entities[i].Name, s) )
            {
                return (entities[i].ID);
            }
        }
        return QUE_SVC_NS::TST_INIT;
    }
};


#endif//PROTOCOL_UTIL_H
