﻿#ifndef INC_ACE_H
#define INC_ACE_H

#ifdef WIN32
    #pragma warning(disable : 4996 4267)
#endif//WIN32

#include <ace/OS.h>
#include <ace/Token.h>
#include <ace/Log_Msg.h>
#include <ace/Message_Block.h>
#include <ace/Task.h>
#include <ace/Task_T.h>
#include <ace/INET_Addr.h>
#include <ace/FILE_IO.h>
#include <ace/Task_T.h>
#include "ace/FILE_Addr.h"
#include "ace/FILE_Connector.h"

#endif//INC_ACE_H
