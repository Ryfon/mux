﻿#include "DBAccess.h"

namespace DB
{

Access* Access::instance( void )
{
    static Access inst_; 
    return &inst_;
}

Access::Access( void )
{
    connection_id_ = 0;
}

Access::~Access()
{
    int count = (int)connections_.size();
    for (int i=0; i<count; ++i)
    {
        delete connections_[i];
    }
}

int Access::newConnection( const map<string, string> params )
{
    auto_ptr<Connection> conn(new Connection(++connection_id_));
    if ( !conn->open(params) )
    {
        return 0;
    }

    connections_.push_back(conn.release());
    return connection_id_;
}

Connection* Access::getConnection( int id )
{
    int count = (int)connections_.size();
    for (int i=0; i<count; ++i)
    {
        if (id == connections_[i]->getID())
        {
            return (connections_[i]);
        }
    }

    return 0;
}


}//namespace DB

