﻿// -*- C++ -*-

//=============================================================================
/**
 *  @file    DBConnection.h
 *
 *  $Version 0.01 2009-10-27 15:33:21
 *
 *  @author chenshaobin <chenshaobin@corp.the9.com>
 */
//=============================================================================

#ifndef DBCONNECTION_H
#define DBCONNECTION_H

// MS compatible compilers support #pragma once
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <driver/mysql_public_iface.h>
#include <vector>
#include <map>
using namespace std;

namespace DB
{

class Connection
{
public:
    //== 接口
    /**
    * 构造函数
    * @id:本对象ID
    */
    Connection(int id);
    /**
    * 查询本对象ID
    * 
    */
    int getID(void)
    {
        return id_;
    }
    /**
    * 打开连接
    * @params:连接参数
    */
    int open(const map<string, string>& params);
    /**
    * 执行查询
    * @sql: 查询SQL
    * @return: 查询结果中的记录条数
    */
    int query(const string& sql, auto_ptr<sql::ResultSet>& rset);
    /**
    * 执行运行
    * @sql: 运行SQL
    * @return: 影响到的记录条数
    */
    int execute(const string& sql);
    /**
    * 保存连接
    * 
    */
    int ping(void);

protected:
    /**
    * 使用内部存储的连接信息进行连接
    * 
    */
    int open(void);

private:
    Connection(void);

    // 本对象ID
    int    id_;

    // 连接信息 
    string           host_;
    string           user_;
    string           database_;
    string           password_;
    string           charset_;
    string           schema_;
    // 数据库实际连接对象
    sql::Connection* conn_;
};

}

#endif//DBCONNECTION_H

