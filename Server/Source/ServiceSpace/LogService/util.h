﻿// -*- C++ -*-

//=============================================================================
/**
 *  @file    util.h
 *
 *  $Version 0.01 2009-11-05 17:25:07
 *
 *  @author chenshaobin <chenshaobin@corp.the9.com>
 */
//=============================================================================

// MS compatible compilers support #pragma once
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#ifndef UTIL_H
#define UTIL_H

#include "ace/OS.h"
#include "inc_std.h"

class Util
{
public:
    static char* time_s(unsigned int sec, char timestamp[])
    {
        tm loc;
        time_t t = sec;
        ACE_OS::localtime_r(&t, &loc);
        ACE_OS::sprintf(timestamp, "%d-%02d-%02d %02d:%02d:%02d", 
                        loc.tm_year+1900,
                        loc.tm_mon+1,
                        loc.tm_mday,
                        loc.tm_hour,
                        loc.tm_min,
                        loc.tm_sec);

        return timestamp;
    }

    static tm time_tm(const char* timestamp, int len)
    {
        // 2009-12-01 11:23:04
        // 0123456789012345678
        enum TM_LOC{
            TM_YEAR = 1,
            TM_MON  = 2,
            TM_DAY  = 3,
            TM_HOUR = 4,
            TM_MIN  = 5,
            TM_SEC  = 6,
            TM_END  = 7,
        };

        char tmp[6];
        tm   loc;
        memset(&loc, 0, sizeof(loc));        
        int  state = TM_YEAR;
        int  digit = 0;
        bool next = false;
        for ( int i = 0; i < len; ++i )
        {
            if ( isdigit(timestamp[i]) )
            {
                tmp[digit++] = timestamp[i];
            }
            else
            {
                // year
                if ( state == TM_YEAR && digit == 4 )
                {
                    tmp[digit] = 0;
                    loc.tm_year = ACE_OS::atoi(tmp) - 1900;
                    next = true;
                }

                // month
                if ( state == TM_MON && digit <= 2 )
                {
                    tmp[digit] = 0;
                    loc.tm_mon = ACE_OS::atoi(tmp) - 1;
                    next = true;
                }

                // day
                if ( state == TM_DAY && digit <= 2 )
                {
                    tmp[digit] = 0;
                    loc.tm_mday = ACE_OS::atoi(tmp);
                    next = true;
                }

                // hour
                if ( state == TM_HOUR && digit <= 2 )
                {
                    tmp[digit] = 0;
                    loc.tm_hour = ACE_OS::atoi(tmp);
                    next = true;
                }

                // min
                if ( state == TM_MIN && digit <= 2 )
                {
                    tmp[digit] = 0;
                    loc.tm_min = ACE_OS::atoi(tmp);
                    next = true;
                }

                // sec
                if ( state == TM_SEC && digit <= 2 )
                {
                    tmp[digit] = 0;
                    loc.tm_sec = ACE_OS::atoi(tmp);
                    next = true;

                    break;
                }

                if (next)
                {
                    state ++;
                }
                digit = 0;
                memset(tmp, 0, sizeof(tmp));
            }
        }
        // sec
        if ( state == TM_SEC && digit <= 2 )
        {
            tmp[digit] = 0;
            loc.tm_sec = ACE_OS::atoi(tmp);
        }

        return loc;
    }

    static unsigned int time_u(const char* timestamp, int len)
    {
        if ( len < 16 )
        {
            return 0;
        }

        tm   t = time_tm(timestamp, len);

        unsigned int u =  (unsigned int)ACE_OS::mktime(&t);
        if ( u == (unsigned int)(-1) )
        {
            return 0;
        }

        return u;
    }

    static string sql_str(const char* s, size_t len)
    {
        static const int MAX_STR_LEN = 512;
        char sql[MAX_STR_LEN];        
        size_t  z = 0;
        for ( size_t i = 1; i <= len; ++i )
        {
            if ( 0 == s[i-1] )
            {
                z = i;
                break;
            }
        }
        // 去除包含0的长度
        if ( z > 0 )
        {
            len = z - 1;
        }
        if ( 0 == len )
        {
            ACE_OS::strcpy(sql, "''");
        }
        else
        {
            if ( len > MAX_STR_LEN - 3 )
            {
                len = MAX_STR_LEN - 3;
            }

            ACE_OS::memcpy(&sql[1], s, len);
            sql[0]      = '\'';
            sql[len+1]  = '\'';
            sql[len+2]  = 0;
        }        
       
        return sql;
    }
};

#endif//UTIL_H

