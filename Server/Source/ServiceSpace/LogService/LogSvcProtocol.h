﻿#ifndef			__LogSvcProtocol__
#define			__LogSvcProtocol__
#include "ParserTool.h"
#include "map"

namespace LOG_SVC_NS
{
    #pragma pack(push, 1)
	//Enums
	typedef	enum
	{
		PT_GAME            	=	0,      	//客户端
		PT_IRC             	=	1,      	//IRC
		PT_GT              	=	2,      	//GM工具
	}EPlayType;

	typedef	enum
	{
		IOT_LOOT           	=	0,      	//怪物LOOT
		IOT_BUY            	=	1,      	//商店买入
		IOT_EXCHG          	=	2,      	//玩家交易
		IOT_QUEST          	=	3,      	//任务奖励
	}EItemOwnType;

	typedef	enum
	{
		RT_FRIEND          	=	0,      	//好友
		RT_ENEMY           	=	1,      	//仇人
		RT_BLACK           	=	2,      	//黑名单
	}ERelationType;

	typedef	enum
	{
		RCT_ADD            	=	0,      	//添加
		RCT_DEL            	=	1,      	//删除
	}ERelationChangeType;

	typedef	enum
	{
		QCT_UN1            	=	0,      	//无效（不需要记录，为了与游戏协议兼容）
		QCT_ACCEPT         	=	1,      	//接收
		QCT_FINISH         	=	2,      	//达成
		QCT_COMPLETE       	=	3,      	//交还
		QCT_UN2            	=	4,      	//无效（不需要记录，为了与游戏协议兼容）
		QCT_FAILED         	=	5,      	//失败（不需要记录，为了与游戏协议兼容）
		QCT_GIVEUP         	=	6,      	//放弃
	}EQuestChangeType;

	typedef	enum
	{
		KT_SYSTEM          	=	0,      	//系统（如天谴）
		KT_NPC             	=	1,      	//NPC
		KT_PLAYER          	=	2,      	//玩家
	}EKillerType;

	typedef	enum
	{
		CT_PLUS            	=	0,      	//增加/加入
		CT_MINUS           	=	1,      	//减少/离开
	}EChangeType;

	typedef	enum
	{
		IPT_MASS           	=	0,      	//品质属性
		IPT_LEVEL          	=	1,      	//等级属性
		IPT_ADD            	=	2,      	//追加属性
		IPT_BIND           	=	3,      	//绑定属性
	}EItemPropertyType;

	typedef	enum
	{
		IBT_NO             	=	0,      	//物品未绑定
		IBT_YES            	=	1,      	//物品已绑定
	}EItemBindType;

	typedef	enum
	{
		MCT_TRADE_SYSTEM   	=	0,      	//与系统交易
		MCT_TRADE_PLAYER   	=	1,      	//与玩家交易
		MCT_REPAIR         	=	2,      	//物品修理
		MCT_LOOT           	=	3,      	//捡包
		MCT_QUEST          	=	4,      	//任务
	}EMoneyChangeType;

	typedef	enum
	{
		MT_MONEY           	=	0,      	//游戏币
		MT_DIAMOND         	=	1,      	//星钻点
		MT_CREDIT          	=	2,      	//信用点
	}EMoneyType;

	typedef	enum
	{
		TCT_CREATE         	=	0,      	//队伍创建
		TCT_CLOSE          	=	1,      	//队伍解散
		TCT_ENTER          	=	2,      	//加入队伍
		TCT_EXIT           	=	3,      	//离开队伍
	}ETeamChangeType;

	typedef	enum
	{
		PST_SLEEP          	=	0,      	//宠物处于收起状态
		PST_ACTIVE         	=	1,      	//宠物处于唤出状态
	}EPetStateType;

	typedef	enum
	{
		ST_PHYSICS         	=	0,      	//物理技能
		ST_MAGIC           	=	1,      	//魔法技能
		ST_SP              	=	2,      	//SP技能
	}ESkillType;

	typedef	enum
	{
		TT_SALE            	=	0,      	//卖出
		TT_BUY             	=	1,      	//购入
	}ESaleBuyType;

	typedef	enum
	{
		WTT_DEPOSIT        	=	0,      	//存入
		WTT_DRAW           	=	1,      	//取出
	}EWarehouseTransportType;

	typedef	enum
	{
		WPT_LOCK           	=	0,      	//上锁
		WPT_UNLOCK         	=	1,      	//解锁
		WPT_CHANGE_PASSWORD	=	2,      	//修改密码
	}EWarehousePrivateType;

	typedef	enum
	{
		CPT_CREATE         	=	0,      	//创建
		CPT_DESTROY        	=	1,      	//销毁
	}ECyclePointType;

	typedef	enum
	{
		TMT_UNKOWN         	=	0,      	//未知操作
		TMT_INIT          	=	1,      	//尚未操作
		TMT_SUBSCRIBE      	=	2,      	//申请操作
		TMT_APPLY          	=	3,      	//受理操作
		TMT_PROCESS        	=	4,      	//处理操作
		TMT_CLOSE          	=	5,      	//关闭操作
	}ETixModType;


	//Defines
	#define	MAX_NAME_SIZE          	 	20 	//名字最长字节数
	#define	MAX_DROP_ITEM_SIZE     	 	7  	//角色死亡时最多掉物品的数目
	#define	MAX_TRADE_ITEM_SIZE    	 	10 	//追踪交易时每个消息中包含交易一方的物品最大数目
	#define	MAX_IP_SIZE            	 	16 	//IP地址字符串最大长度
	#define	MAX_TYPE_SIZE          	 	16 	//类别文字最大长度
	#define	MAX_SRV_NAME_SIZE      	 	40 	//服务器名最大长度
	#define	MAX_TIX_TITLE_SIZE     	 	64 	//诉求标题最大长度
	#define	MAX_TIX_INFO_SIZE      	 	300	//诉求内容最大长度
	#define	MAX_TIX_CLOSE_INFO_SIZE	 	150	//关闭说明最大长度

	//Typedefs

	//Types
	template<bool> struct TypeMaxBytes;
	template<> struct TypeMaxBytes<true> {};
	#define	LOGSVCPROTOCOL_MAXBYTES_CHECK(typename) \
	struct type##typename \
	{\
		TypeMaxBytes< sizeof(typename) < 1109 > struct_larger_##typename;\
	}

	struct ItemInfo_i
	{
		UINT       	UUID;                            	//唯一编号
		UINT       	TypeID;                          	//模板编号
		UINT       	WearPoint;                       	//耐久度
		USHORT     	AddId;                           	//道具的追加编号
		BYTE       	Count;                           	//道具的个数
		BYTE       	LevelUp;                         	//道具的升级等级
	};

	struct GOAccountLogin
	{
		static const USHORT	wCmd = 0x1c01;

		UINT       	Time;                            	//发生时间
		UINT       	AccountSize;                     	//账号名长度
		CHAR       	Account[MAX_NAME_SIZE];            	//账号名
		UINT       	PlayerIPSize;                    	//玩家IP地址长度
		CHAR       	PlayerIP[MAX_IP_SIZE];             	//玩家IP地址
		BYTE       	PlayType;                        	//接入方式（参照EPlayType）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOAccountLogin);

	struct GOAccountLogoff
	{
		static const USHORT	wCmd = 0x1c02;

		UINT       	Time;                            	//发生时间
		UINT       	AccountSize;                     	//账号名长度
		CHAR       	Account[MAX_NAME_SIZE];            	//账号名
		UINT       	Duration;                        	//本次账号延续时长（秒）
		BYTE       	PlayType;                        	//接入方式（参照EPlayType）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOAccountLogoff);

	struct GORoleEnter
	{
		static const USHORT	wCmd = 0x1c03;

		UINT       	Time;                            	//发生时间
		UINT       	AccountSize;                     	//账号名长度
		CHAR       	Account[MAX_NAME_SIZE];            	//账号名
		UINT       	RoleID;                          	//角色ID
		UINT       	RoleNameSize;                    	//角色名长度
		CHAR       	RoleName[MAX_NAME_SIZE];           	//角色名
		BYTE       	RoleLevel;                       	//角色等级
		BYTE       	PlayType;                        	//接入方式（参照EPlayType）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleEnter);

	struct GORoleExit
	{
		static const USHORT	wCmd = 0x1c04;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	Duration;                        	//本次角色延续时长（秒）
		BYTE       	PlayType;                        	//接入方式（参照EPlayType）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleExit);

	struct GORoleLevelUP
	{
		static const USHORT	wCmd = 0x1c05;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	OldLevel;                        	//升级前等级
		UINT       	NewLevel;                        	//升级后等级
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleLevelUP);

	struct GOSkillLevelUP
	{
		static const USHORT	wCmd = 0x1c06;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		BYTE       	SkillType;                       	//技能类别(ESkillType)
		UINT       	OldLevel;                        	//升级前等级
		UINT       	NewLevel;                        	//升级后等级
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOSkillLevelUP);

	struct GORoleItemOwn
	{
		static const USHORT	wCmd = 0x1c07;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		BYTE       	OwnType;                         	//获得方式（参照EItemOwnType）
		ItemInfo_i 	Item;                            	//物品信息
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleItemOwn);

	struct GORoleItemUse
	{
		static const USHORT	wCmd = 0x1c08;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		ItemInfo_i 	Item;                            	//物品信息
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleItemUse);

	struct GORoleItemDrop
	{
		static const USHORT	wCmd = 0x1c09;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		ItemInfo_i 	Item;                            	//物品信息
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleItemDrop);

	struct GORoleRelationChange
	{
		static const USHORT	wCmd = 0x1c0a;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	RelationID;                      	//关系对象角色ID
		BYTE       	RelationType;                    	//关系类别（参照ERelationType）
		BYTE       	ChangeType;                      	//变更类别（参照ERelationChangeType）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleRelationChange);

	struct GORoleUnionChange
	{
		static const USHORT	wCmd = 0x1c0b;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	UnionID;                         	//公会ID
		BYTE       	ChangeType;                      	//变更类别（参照ERelationChangeType）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleUnionChange);

	struct GORoleQuestChange
	{
		static const USHORT	wCmd = 0x1c0c;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	QuestID;                         	//任务ID
		BYTE       	ChangeType;                      	//变更类别（参照EQuestChangeType）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleQuestChange);

	struct GORoleDead
	{
		static const USHORT	wCmd = 0x1c0d;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	MapID;                           	//地图ID
		UINT       	MapX;                            	//地图X坐标
		UINT       	MapY;                            	//地图Y坐标
		BYTE       	KillerType;                      	//杀手类型（参照EKillerType）
		UINT       	KillerID;                        	//杀手ID
		UINT       	DropExp;                         	//掉落经验
		UINT       	DropItemCount;                   	//掉落物品数目
		ItemInfo_i 	DropItem[MAX_DROP_ITEM_SIZE];      	//掉落物品
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleDead);

	struct GORoleMapChange
	{
		static const USHORT	wCmd = 0x1c0e;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	OldMapID;                        	//旧地图ID
		UINT       	NewMapID;                        	//新地图ID
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleMapChange);

	struct TradeUnit_i
	{
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	ItemCount;                       	//交易物品数目
		ItemInfo_i 	Item[MAX_TRADE_ITEM_SIZE];         	//交易物品
		UINT       	Money;                           	//交易金钱数目
	};

	struct GORoleTrade
	{
		static const USHORT	wCmd = 0x1c0f;

		UINT       	Time;                            	//发生时间
		TradeUnit_i	UnitA;                           	//交易A方
		TradeUnit_i	UnitB;                           	//交易B方
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORoleTrade);

	struct GOSPChange
	{
		static const USHORT	wCmd = 0x1c10;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		BYTE       	ChangeType;                      	//变更类别（EChangeType）
		BYTE       	ChangeNum;                       	//变更数量
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOSPChange);

	struct GOItemPropertyChange
	{
		static const USHORT	wCmd = 0x1c11;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		BYTE       	PropertyType;                    	//变更的属性类型（EItemPropertyType）
		UINT       	ItemUUID;                        	//物品流水号
		UINT       	ItemTypeID;                      	//物品类型号
		UINT       	OldValue;                        	//旧的属性值
		UINT       	NewValue;                        	//新的属性值
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOItemPropertyChange);

	struct GOMoneyChange
	{
		static const USHORT	wCmd = 0x1c12;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		BYTE       	ChangeType;                      	//变更类型（EChangeType）
		BYTE       	ReasonType;                      	//引起变更的活动类型（EMoneyChangeType）
		BYTE       	MoneyType;                       	//金钱种类（EMoneyType）
		UINT       	MoneyCount;                      	//金钱数额
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOMoneyChange);

	struct GOTeamChange
	{
		static const USHORT	wCmd = 0x1c13;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		BYTE       	ChangeType;                      	//变更类型（ETeamChangeType）
		UINT       	TeamID;                          	//队伍ID
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOTeamChange);

	struct GOPetAdd
	{
		static const USHORT	wCmd = 0x1c14;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	PetID;                           	//宠物ID
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOPetAdd);

	struct GOPetLevelup
	{
		static const USHORT	wCmd = 0x1c15;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	PetID;                           	//宠物ID
		UINT       	OldLevel;                        	//宠物原等级
		UINT       	NewLevel;                        	//宠物新等级
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOPetLevelup);

	struct GOPetFunctionAdd
	{
		static const USHORT	wCmd = 0x1c16;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	PetID;                           	//宠物ID
		UINT       	FunctionID;                      	//宠物功能ID
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOPetFunctionAdd);

	struct GOPetStateChange
	{
		static const USHORT	wCmd = 0x1c17;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	PetID;                           	//宠物ID
		BYTE       	StateType;                       	//改变后状态（EPetStateType）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOPetStateChange);

	struct GOUseSkill
	{
		static const USHORT	wCmd = 0x1c18;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		BYTE       	SkillType;                       	//技能类别（ESkillType）
		UINT       	SkillID;                         	//技能ID
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOUseSkill);

	struct GOKillPlayer
	{
		static const USHORT	wCmd = 0x1c19;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	PlayerID;                        	//被杀的玩家ID
		BYTE       	PlayerLevel;                     	//被杀的玩家等级
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOKillPlayer);

	struct GORelive
	{
		static const USHORT	wCmd = 0x1c1a;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		BYTE       	RoleLevel;                       	//角色等级
		UINT       	MapID;                           	//复活地图ID
		UINT       	MapX;                            	//复活坐标X
		UINT       	MapY;                            	//复活坐标Y
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORelive);

	struct GOSendMail
	{
		static const USHORT	wCmd = 0x1c1b;

		UINT       	Time;                            	//发生时间
		UINT       	SendRoleID;                      	//发送方角色ID
		UINT       	RecvRoleID;                      	//接收方角色ID
		UINT       	MailID;                          	//邮件ID
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOSendMail);

	struct GOShopTrade
	{
		static const USHORT	wCmd = 0x1c1c;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	ShopID;                          	//商店ID
		BYTE       	SaleBuy;                         	//购入/卖出（ESaleBuyType）
		UINT       	Money;                           	//交易金额
		UINT       	ItemTypeID;                      	//交易物品类型号
		UINT       	ItemNum;                         	//物品堆叠数目
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOShopTrade);

	struct GOItemRepair
	{
		static const USHORT	wCmd = 0x1c1d;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	ShopID;                          	//商店ID
		UINT       	Money;                           	//修理金额
		UINT       	ItemUUID;                        	//修理物品流水号
		UINT       	ItemTypeID;                      	//修理物品类型号
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOItemRepair);

	struct GOMarketTrade
	{
		static const USHORT	wCmd = 0x1c1e;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	Diamond;                         	//消费星钻额
		UINT       	Credit;                          	//消费信用额
		UINT       	ItemTypeID;                      	//物品类型号
		UINT       	ItemNum;                         	//物品堆叠数目
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOMarketTrade);

	struct GOWarehouseOpen
	{
		static const USHORT	wCmd = 0x1c1f;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	WarehouseID;                     	//仓库ID
		BYTE       	HasPassword;                     	//是否使用密码
		UINT       	MapID;                           	//所处地图ID
		UINT       	MapX;                            	//所处地图X坐标
		UINT       	MapY;                            	//所处地图Y坐标
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOWarehouseOpen);

	struct GOWarehouseItemTransport
	{
		static const USHORT	wCmd = 0x1c20;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	WarehouseID;                     	//仓库ID
		BYTE       	TransportType;                   	//存/取（EWarehouseTransportType）
		UINT       	ItemUUID;                        	//物品流水号
		UINT       	ItemTypeID;                      	//物品类型号
		UINT       	ItemNum;                         	//物品堆叠数目
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOWarehouseItemTransport);

	struct GOWarehouseMoneyTransport
	{
		static const USHORT	wCmd = 0x1c21;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	WarehouseID;                     	//仓库ID
		BYTE       	TransportType;                   	//存/取（EWarehouseTransportType）
		UINT       	Money;                           	//存取金钱数目
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOWarehouseMoneyTransport);

	struct GOWarehouseExtend
	{
		static const USHORT	wCmd = 0x1c22;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	WarehouseID;                     	//仓库ID
		UINT       	SlotNum;                         	//扩充格子数
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOWarehouseExtend);

	struct GOWarehousePrivateOP
	{
		static const USHORT	wCmd = 0x1c23;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//角色ID
		UINT       	WarehouseID;                     	//仓库ID
		BYTE       	PrivateType;                     	//操作类型(EWarehousePrivateType)
		BYTE       	Result;                          	//操作结果
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOWarehousePrivateOP);

	struct GOMonsterDead
	{
		static const USHORT	wCmd = 0x1c24;

		UINT       	Time;                            	//发生时间
		UINT       	RoleID;                          	//杀死怪物角色ID
		UINT       	MapID;                           	//地图ID
		UINT       	CreaterID;                       	//生成器ID
		UINT       	MonsterID;                       	//怪物ID
		UINT       	KillerID;                        	//杀死怪物的对象ID
		UINT       	KillerLevel;                     	//杀死怪物的对象等级
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOMonsterDead);

	struct GORealInstanceLifeCycle
	{
		static const USHORT	wCmd = 0x1c25;

		UINT       	Time;                            	//发生时间
		UINT       	InstanceID;                      	//真副本ID
		UINT       	WorldID;                         	//平行空间ID
		BYTE       	CyclePoint;                      	//创建/销毁(ECyclePointType)
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GORealInstanceLifeCycle);

	struct TixNew_i
	{
		UINT       	ID;                              	//诉求服务端唯一标识
		UINT       	TypeStringSize;                  	//种类文字长度
		CHAR       	TypeString[MAX_TYPE_SIZE];         	//种类文字
		UINT       	Time;                            	//产生时刻
		UINT       	AccountNameSize;                 	//账号名长度
		CHAR       	AccountName[MAX_NAME_SIZE];        	//发起诉求的账号名
		UINT       	RoleID;                          	//角色ID
		UINT       	RoleNameSize;                    	//角色名长度
		CHAR       	RoleName[MAX_NAME_SIZE];           	//发起诉求的角色名
		UINT       	TitleSize;                       	//标题长度
		CHAR       	Title[MAX_TIX_TITLE_SIZE];         	//诉求标题
		UINT       	InfoSize;                        	//内容长度
		CHAR       	Info[MAX_TIX_INFO_SIZE];           	//诉求内容
		UINT       	RealmNameSize;                   	//游戏区名长度
		CHAR       	RealmName[MAX_SRV_NAME_SIZE];      	//游戏区名
		UINT       	WorldNameSize;                   	//游戏世界名长度
		CHAR       	WorldName[MAX_SRV_NAME_SIZE];      	//游戏世界名
	};

	struct GOTixNew
	{
		static const USHORT	wCmd = 0x1d01;

		UINT       	LocalTixID;                      	//发送端诉求唯一标识
		TixNew_i   	TixInfo;                         	//新诉求信息
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOTixNew);

	struct OGTixNewRsp
	{
		static const USHORT	wCmd = 0x1d81;

		UINT       	LocalTixID;                      	//发送端诉求唯一标识
		UINT       	TixID;                           	//服务端诉求唯一标识
		UINT       	ErrorNo;                         	//处理结果（非0表示错误消息ID）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(OGTixNewRsp);

	struct GOTixMod
	{
		static const USHORT	wCmd = 0x1d02;

		UINT       	LocalTixID;                      	//发送端诉求唯一标识
		UINT       	TixID;                           	//服务端诉求唯一标识
		BYTE       	ModType;                         	//操作类型（ETixModType）
		UINT       	DueNameSize;                     	//受理者名称长度
		CHAR       	DueName[MAX_NAME_SIZE];            	//受理者名称
		UINT       	CloseTypeStringSize;             	//关闭种类文字长度
		CHAR       	CloseTypeString[MAX_TYPE_SIZE];    	//关闭种类文字
		UINT       	CloseInfoSize;                   	//关闭说明长度
		CHAR       	CloseInfo[MAX_TIX_CLOSE_INFO_SIZE];	//关闭说明
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(GOTixMod);

	struct OGTixModRsp
	{
		static const USHORT	wCmd = 0x1d82;

		UINT       	LocalTixID;                      	//发送端诉求唯一标识
		UINT       	TixID;                           	//服务端诉求唯一标识
		BYTE       	ModType;                         	//操作类型（与请求中的相等）
		UINT       	ErrorNo;                         	//处理结果（非0表示错误消息ID）
	};
	LOGSVCPROTOCOL_MAXBYTES_CHECK(OGTixModRsp);


	//Messages
	typedef enum Message_id_type
	{
        MIN_LOG_ID                      =   0x1c01,

		G_O_ACCOUNT_LOGIN            	=	0x1c01,	//GOAccountLogin
		G_O_ACCOUNT_LOGOFF           	=	0x1c02,	//GOAccountLogoff
		G_O_ROLE_ENTER               	=	0x1c03,	//GORoleEnter
		G_O_ROLE_EXIT                	=	0x1c04,	//GORoleExit
		G_O_ROLE_LEVELUP             	=	0x1c05,	//GORoleLevelUP
		G_O_SKILL_LEVELUP            	=	0x1c06,	//GOSkillLevelUP
		G_O_ROLE_ITEM_OWN            	=	0x1c07,	//GORoleItemOwn
		G_O_ROLE_ITEM_USE            	=	0x1c08,	//GORoleItemUse
		G_O_ROLE_ITEM_DROP           	=	0x1c09,	//GORoleItemDrop
		G_O_ROLE_RELATION_CHANGE     	=	0x1c0a,	//GORoleRelationChange
		G_O_ROLE_UNION_CHANGE        	=	0x1c0b,	//GORoleUnionChange
		G_O_ROLE_QUEST_CHANGE        	=	0x1c0c,	//GORoleQuestChange
		G_O_ROLE_DEAD                	=	0x1c0d,	//GORoleDead
		G_O_ROLE_MAP_CHANGE          	=	0x1c0e,	//GORoleMapChange
		G_O_ROLE_TRADE               	=	0x1c0f,	//GORoleTrade
		G_O_SP_CHANGE                	=	0x1c10,	//GOSPChange
		G_O_ITEM_PROPERTY_CHANGE     	=	0x1c11,	//GOItemPropertyChange
		G_O_MONEY_CHANGE             	=	0x1c12,	//GOMoneyChange
		G_O_TEAM_CHANGE              	=	0x1c13,	//GOTeamChange
		G_O_PET_ADD                  	=	0x1c14,	//GOPetAdd
		G_O_PET_LEVELUP              	=	0x1c15,	//GOPetLevelup
		G_O_PET_FUNCTION_ADD         	=	0x1c16,	//GOPetFunctionAdd
		G_O_PET_STATE_CHANGE         	=	0x1c17,	//GOPetStateChange
		G_O_USE_SKILL                	=	0x1c18,	//GOUseSkill
		G_O_KILL_PLAYER              	=	0x1c19,	//GOKillPlayer
		G_O_RELIVE                   	=	0x1c1a,	//GORelive
		G_O_SEND_MAIL                	=	0x1c1b,	//GOSendMail
		G_O_SHOP_TRADE               	=	0x1c1c,	//GOShopTrade
		G_O_ITEM_REPAIR              	=	0x1c1d,	//GOItemRepair
		G_O_MARKET_TRADE             	=	0x1c1e,	//GOMarketTrade
		G_O_WAREHOUSE_OPEN           	=	0x1c1f,	//GOWarehouseOpen
		G_O_WAREHOUSE_ITEM_TRANSPORT 	=	0x1c20,	//GOWarehouseItemTransport
		G_O_WAREHOUSE_MONEY_TRANSPORT	=	0x1c21,	//GOWarehouseMoneyTransport
		G_O_WAREHOUSE_EXTEND         	=	0x1c22,	//GOWarehouseExtend
		G_O_WAREHOUSE_PRIVATEOP      	=	0x1c23,	//GOWarehousePrivateOP
		G_O_MONSTER_DEAD             	=	0x1c24,	//GOMonsterDead
		G_O_REAL_INSTANCE_LIFECYCLE  	=	0x1c25,	//GORealInstanceLifeCycle
		G_O_TIX_NEW                  	=	0x1d01,	//GOTixNew
		O_G_TIX_NEW_RSP              	=	0x1d81,	//OGTixNewRsp
		G_O_TIX_MOD                  	=	0x1d02,	//GOTixMod
		O_G_TIX_MOD_RSP              	=	0x1d82,	//OGTixModRsp

        MAX_LOG_ID                      =   0x1d82,
	};
    #pragma pack(pop)

	//Class Data
	class LogSvcProtocol;
	typedef size_t (LogSvcProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (LogSvcProtocol::*DeCodeFunc)(void* pData);

	class 
	LogSvcProtocol
	{
	public:
		LogSvcProtocol();
		~LogSvcProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__ItemInfo_i(void* pData);
		size_t	DeCode__ItemInfo_i(void* pData);

		size_t	EnCode__GOAccountLogin(void* pData);
		size_t	DeCode__GOAccountLogin(void* pData);

		size_t	EnCode__GOAccountLogoff(void* pData);
		size_t	DeCode__GOAccountLogoff(void* pData);

		size_t	EnCode__GORoleEnter(void* pData);
		size_t	DeCode__GORoleEnter(void* pData);

		size_t	EnCode__GORoleExit(void* pData);
		size_t	DeCode__GORoleExit(void* pData);

		size_t	EnCode__GORoleLevelUP(void* pData);
		size_t	DeCode__GORoleLevelUP(void* pData);

		size_t	EnCode__GOSkillLevelUP(void* pData);
		size_t	DeCode__GOSkillLevelUP(void* pData);

		size_t	EnCode__GORoleItemOwn(void* pData);
		size_t	DeCode__GORoleItemOwn(void* pData);

		size_t	EnCode__GORoleItemUse(void* pData);
		size_t	DeCode__GORoleItemUse(void* pData);

		size_t	EnCode__GORoleItemDrop(void* pData);
		size_t	DeCode__GORoleItemDrop(void* pData);

		size_t	EnCode__GORoleRelationChange(void* pData);
		size_t	DeCode__GORoleRelationChange(void* pData);

		size_t	EnCode__GORoleUnionChange(void* pData);
		size_t	DeCode__GORoleUnionChange(void* pData);

		size_t	EnCode__GORoleQuestChange(void* pData);
		size_t	DeCode__GORoleQuestChange(void* pData);

		size_t	EnCode__GORoleDead(void* pData);
		size_t	DeCode__GORoleDead(void* pData);

		size_t	EnCode__GORoleMapChange(void* pData);
		size_t	DeCode__GORoleMapChange(void* pData);

		size_t	EnCode__TradeUnit_i(void* pData);
		size_t	DeCode__TradeUnit_i(void* pData);

		size_t	EnCode__GORoleTrade(void* pData);
		size_t	DeCode__GORoleTrade(void* pData);

		size_t	EnCode__GOSPChange(void* pData);
		size_t	DeCode__GOSPChange(void* pData);

		size_t	EnCode__GOItemPropertyChange(void* pData);
		size_t	DeCode__GOItemPropertyChange(void* pData);

		size_t	EnCode__GOMoneyChange(void* pData);
		size_t	DeCode__GOMoneyChange(void* pData);

		size_t	EnCode__GOTeamChange(void* pData);
		size_t	DeCode__GOTeamChange(void* pData);

		size_t	EnCode__GOPetAdd(void* pData);
		size_t	DeCode__GOPetAdd(void* pData);

		size_t	EnCode__GOPetLevelup(void* pData);
		size_t	DeCode__GOPetLevelup(void* pData);

		size_t	EnCode__GOPetFunctionAdd(void* pData);
		size_t	DeCode__GOPetFunctionAdd(void* pData);

		size_t	EnCode__GOPetStateChange(void* pData);
		size_t	DeCode__GOPetStateChange(void* pData);

		size_t	EnCode__GOUseSkill(void* pData);
		size_t	DeCode__GOUseSkill(void* pData);

		size_t	EnCode__GOKillPlayer(void* pData);
		size_t	DeCode__GOKillPlayer(void* pData);

		size_t	EnCode__GORelive(void* pData);
		size_t	DeCode__GORelive(void* pData);

		size_t	EnCode__GOSendMail(void* pData);
		size_t	DeCode__GOSendMail(void* pData);

		size_t	EnCode__GOShopTrade(void* pData);
		size_t	DeCode__GOShopTrade(void* pData);

		size_t	EnCode__GOItemRepair(void* pData);
		size_t	DeCode__GOItemRepair(void* pData);

		size_t	EnCode__GOMarketTrade(void* pData);
		size_t	DeCode__GOMarketTrade(void* pData);

		size_t	EnCode__GOWarehouseOpen(void* pData);
		size_t	DeCode__GOWarehouseOpen(void* pData);

		size_t	EnCode__GOWarehouseItemTransport(void* pData);
		size_t	DeCode__GOWarehouseItemTransport(void* pData);

		size_t	EnCode__GOWarehouseMoneyTransport(void* pData);
		size_t	DeCode__GOWarehouseMoneyTransport(void* pData);

		size_t	EnCode__GOWarehouseExtend(void* pData);
		size_t	DeCode__GOWarehouseExtend(void* pData);

		size_t	EnCode__GOWarehousePrivateOP(void* pData);
		size_t	DeCode__GOWarehousePrivateOP(void* pData);

		size_t	EnCode__GOMonsterDead(void* pData);
		size_t	DeCode__GOMonsterDead(void* pData);

		size_t	EnCode__GORealInstanceLifeCycle(void* pData);
		size_t	DeCode__GORealInstanceLifeCycle(void* pData);

		size_t	EnCode__TixNew_i(void* pData);
		size_t	DeCode__TixNew_i(void* pData);

		size_t	EnCode__GOTixNew(void* pData);
		size_t	DeCode__GOTixNew(void* pData);

		size_t	EnCode__OGTixNewRsp(void* pData);
		size_t	DeCode__OGTixNewRsp(void* pData);

		size_t	EnCode__GOTixMod(void* pData);
		size_t	DeCode__GOTixMod(void* pData);

		size_t	EnCode__OGTixModRsp(void* pData);
		size_t	DeCode__OGTixModRsp(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool                    m_kPackage;
	};
}
#endif			//__LogSvcProtocol__
