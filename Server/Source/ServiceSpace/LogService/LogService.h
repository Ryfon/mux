﻿/********************************************************************
created:	2008/09/08
created:	2008/09/08   10:28
filename: 	E:\Developer\PlatformTools\ServiceSpace\LogService\LogService.h 
file path:	E:\Developer\PlatformTools\ServiceSpace\LogService
file base:	LogService
file ext:	h
author:		

purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_LOG_SERVICE_MESSAGE_HANDLER_H__
#define __SERVICE_SPACE_LOG_SERVICE_MESSAGE_HANDLER_H__

//#define  SS_NDEBUG

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/SessionHandler.h"
#include "../ServiceSpaceLib/MessageHandler.h"
#include "../ServiceSpaceLib/utilib/pool_lock.h"
#include "../ServiceSpaceLib/utilib/stringN.h"

#include <map>
#include "log_task.h"
#include "query_task.h"
#include "logic_task.h"

__SERVICE_SPACE_BEGIN_NS__

#define __TRY_BENGIN try {
#define __TRY_END  } catch (std::exception& e) {SS_ERROR(ACE_TEXT("exception = %s \n"), (e.what()) );}

class ACE_Svc_Export LogServiceMessageHandler
	: public MessageHandler<LogServiceMessageHandler>
{
	typedef MessageHandler<LogServiceMessageHandler> base;
public:
	typedef std::map<unsigned, tcp_session> gate_map;	

	int init(int argc, ACE_TCHAR * argv[]) ;
	virtual int open(void *args = 0);

protected:
	int handle_task_message(ACE_Message_Block * message);
    int handle_message(ACE_Message_Block* mb, UINT session_id = 0);

private:
	ACE_INET_Addr listen_addr_;
	void parse_db_param(std::string str) ;

    log_task    log_task_;
    query_task  query_task_;
    logic_task  logic_task_;
};

__SERVICE_SPACE_END_NS__

#endif


