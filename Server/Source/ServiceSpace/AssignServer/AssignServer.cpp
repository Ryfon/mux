﻿// AssignServer.cpp : 定义 DLL 应用程序的入口点。
//

#include "AssignServer.h"
#include "ace/OS_Memory.h"

	
	const short MESSAGE_TYPE = 0;


	ACE_Message_Block * AssignUdpHandler::build_result_message(void * data, int msgId)
	{
		ACE_Message_Block * message  = 0 ;
		__TRY_BENGIN
		message = new MessageBlock();
		message->wr_ptr(5);
		size_t size = assign_proto_.EnCode(msgId, data, message->wr_ptr(), message->space());
		if (-1 == size)
		{
			SS_ERROR(ACE_TEXT("protocol encode error, message id: %d\n"), msgId);
		}
		message->wr_ptr(size);
		build_message_header(message->rd_ptr(), short(size + 5), MESSAGE_TYPE, msgId);
		__TRY_END
		return message;
		
	}

	void AssignUdpHandler::parse_addr(int , ACE_TCHAR *argv[])
	{
		__TRY_BENGIN
		std::vector<std::string> vec;
		ACE_TCHAR * arg = argv[0];
		utilib::split(vec, &arg[0], &arg[strlen(arg)], ' ');
		for (int i=0;i<(int)vec.size();++i)
		{
			if (vec[i]=="-g")
			{
				++i;
				if(i<(int)vec.size())
				{
					unsigned long addr = ::inet_addr(vec[i].c_str());
					gate_addr_.set_address((const char *)&addr, sizeof(addr), 0);
				}
					
			}else
			if(vec[i]=="-c")
			{
				++i;
				if(i<(int)vec.size())
				{
					unsigned long addr = ::inet_addr(vec[i].c_str());
					listen_addr.set_address((const char *)&addr, sizeof(addr), 0);
				}
					
			}
		}
		__TRY_END
	}

	void AssignUdpHandler::parse_port(int , ACE_TCHAR *argv[])
	{
		__TRY_BENGIN
		std::vector<std::string> vec;
		ACE_TCHAR * arg = argv[0];
		utilib::split(vec, &arg[0], &arg[strlen(arg)], ' ');
		for (int i=0;i<(int)vec.size();++i)
		{
			if (vec[i]=="-G")
			{
				++i;
				if(i<(int)vec.size())
				{
					unsigned short port = ::atoi(vec[i].c_str());
					gate_addr_.set_port_number(port);
				}

			}else
				if(vec[i]=="-C")
				{
					++i;
					if(i<(int)vec.size())
					{
						unsigned short port = ::atoi(vec[i].c_str());
						listen_addr.set_port_number(port);
					}

				}
		}
		__TRY_END
	}

	void AssignUdpHandler::parse_boundary(int , ACE_TCHAR *argv[])
	{
		__TRY_BENGIN
		std::vector<std::string> vec;
		ACE_TCHAR * arg = argv[0];
		utilib::split(vec, &arg[0], &arg[strlen(arg)], ' ');
		for (int i=0;i<(int)vec.size();++i)
		{
			if (vec[i]=="-H")
			{
				++i;
				if(i<(int)vec.size())
				{
					unsigned short boundary = ::atoi(vec[i].c_str());
					boundary_[2] = boundary ;
				}

			}else
				if(vec[i]=="-M")
				{
					++i;
					if(i<(int)vec.size())
					{
						unsigned short boundary = ::atoi(vec[i].c_str());
						boundary_[1] = boundary ;
					}

				}else if(vec[i]=="-L")
				{
					++i;
					if(i<(int)vec.size())
					{
						unsigned short boundary = ::atoi(vec[i].c_str());
						boundary_[0] = boundary ;
					}
				}

		}						
		SS_INFO(ACE_TEXT("当前设定人数上限 是 %d 中等是 %d 下限是 %d \n "),boundary_[2],boundary_[1],boundary_[0]) ;
		__TRY_END
	}

	
	int AssignUdpHandler::init(int argc, ACE_TCHAR *argv[])
	{
		__TRY_BENGIN
		num_ = 0 ;
		GetOpts<ACE_TCHAR> opts(argc, argv);
		const ACE_TCHAR * options = ACE_TEXT(":h:p:b:");
		if (argc < 3)
		{
			SS_ERROR( "command line arguments count is less than 4\n");
			return -1;
		}
		name_ = argv[0];

		ACE_Get_Opt get_opt(argc, (ACE_TCHAR **)(opts.argv), options);
		int c;
		std::string sAddr;
		std::string sPort;

		while((c = get_opt()) != EOF)
		{
			switch(c)
			{
			case 'h':
				parse_addr(1, &get_opt.optarg);
				break;
			case 'p':				
				parse_port(1, &get_opt.optarg);
				break;	
			case 'b':
				parse_boundary(1, &get_opt.optarg);
				break ;
			}
		}
	
		last_recrods_update_time_ = 0;
		return UdpHandler::init(argc,argv) ; 
		__TRY_END
		return 0 ;
	}
	int AssignUdpHandler::open(void *args )
	{
		__TRY_BENGIN
		int err = UdpHandler::open(args);
		if (-1 == err)
			return -1;

		udp_ = ACE_Dynamic_Service<UdpService>::instance(ACE_TEXT("udp"));
			
		udp_1_ = (void*)1;
		udp_2_ = (void*)2 ;

		if (udp_)
		{
			gate_srv_id_ = udp_->create_udp_receiver(gate_addr_, (void *)udp_1_);
			client_id_ = udp_->create_udp_receiver(listen_addr, (void *)udp_2_);

			udp_->post_asynch_read(gate_srv_id_, (MessageBlock * )0);
			udp_->post_asynch_read(client_id_, (MessageBlock * )0);		
		}
		else
		{
			exit(1) ;
		}
		
		std::ofstream _record("records.log", std::ios::out | std::ios::app);
		if (!_record.is_open())
		{
			SS_ERROR("Failed to open records.log file by out\n");
			exit(2);
		}
		_record.close();
		std::ifstream record("records.log", std::ios::in);
		if (!record.is_open())
		{
			SS_ERROR("Failed to open records.log file\n");
			exit(2);
		}

		while(!record.eof())
		{
			char buf[32];
			unsigned mapId;
			memset(buf, 0, sizeof(buf));
			record >> mapId >> buf;
			account_t account(buf, strlen(buf));
			if (account.size())
				records_[account] = mapId;
		}

		SS_INFO(ACE_TEXT(" Assign Service begin ! \n")) ;
		__TRY_END	
		return 0;

	}

	int AssignUdpHandler::close(u_long flags)
	{
		save_player_record(true);
		return 0;
	}

	void AssignUdpHandler::handle_message_read(const ACE_INET_Addr & remote_addr, ACE_Message_Block * message, void * , int error, void * user_data)
	{			
		SS_INFO(ACE_TEXT("收到UDP消息包：from：%s ! \n"),remote_addr.get_host_addr());
		__TRY_BENGIN
		if((error==0)&&message)
		{		
			ACE_GUARD (ACE_Thread_Mutex,guard,mutex_) ;
			short Cmd=0;
			char  pkgType=0;
			short len=0;					
			parse_message_header(message->rd_ptr(),len,pkgType,Cmd) ;
			SS_INFO(ACE_TEXT(" handle_message_read  memssage len = %d ! error = %d from (ip addr %s port %d)\n"),(int)len,error,remote_addr.get_host_addr(),remote_addr.get_port_number() ) ;
			char msgBuf[2048] ;
			char* pDate =  (char*)(message->rd_ptr()+PACKETHEADERLENGTH) ;
			if(assign_proto_.DeCode(Cmd,msgBuf+PACKETHEADERLENGTH,2048,pDate,len)!=(size_t)(-1))
			{
				switch(Cmd)
				{
				case Assign_namespace::G_A_SRV_MSG_ID :				
					on_gate_srv_msg(remote_addr,msgBuf) ;
					break;
				case Assign_namespace::G_A_NOTIFY_PLAYER_MAP_ID:
					on_gate_srv_notify_player_map(remote_addr, msgBuf);
					break;

				case Assign_namespace::C_A_QUERY_SRV_STATE_ID:				
						on_client_query_srv(remote_addr,msgBuf) ;
					break;
				case Assign_namespace::C_A_REQUEST_GATESRV_ADDR_ID:				
						on_client_request_gate_srv(remote_addr,msgBuf) ;
					break;
				case Assign_namespace::C_A_REQUEST_GATE_SRV_ADDR_WITH_ACCOUNT:
						on_client_request_gate_srv_with_account(remote_addr, msgBuf);
					break;
				default:
					break;
				}
			}
		}
		else
		{
			SS_ERROR(ACE_TEXT("handle_message_read error = %d \n"),error) ;
		}
		if(message)
			message->release() ;
		if (udp_1_==(user_data))
			udp_->post_asynch_read(gate_srv_id_, (MessageBlock * )0);
		else
			udp_->post_asynch_read(client_id_, (MessageBlock * )0);
		__TRY_END
	}

	

	

	void AssignUdpHandler::on_gate_srv_msg(const ACE_INET_Addr & ,char* Msg)
	{
		__TRY_BENGIN
		Assign_namespace::GAGateSrvMsg* pGateSrvMsg = (Assign_namespace::GAGateSrvMsg*)(Msg+PACKETHEADERLENGTH) ;
		ACE_INET_Addr addr(ntohs(pGateSrvMsg->Port),ntohl(pGateSrvMsg->IP)) ;	
		if (num_!=pGateSrvMsg->Num)
		{
			SS_DEBUG(ACE_TEXT("\n OnGateSrvMsg收到消息时间(%d) 地址（%s） 端口(%d)  当时在线人数 %d \n "),GetTickCount(),addr.get_host_addr(),pGateSrvMsg->Port,pGateSrvMsg->Num) ;
			num_=pGateSrvMsg->Num ;
		}		
		bool isFind = false ;
		{
			//ACE_GUARD (ACE_Thread_Mutex,guard,mutex_) ;			
			for (int i=0;i<((int)(gate_srv_key_.size()));++i)
			{
				SS_DEBUG(ACE_TEXT(" on_gate_srv_msg gate_srv_key_[%d] \n"),i) ;
				if(gate_srv_key_[i].GateAddr==addr)
				{
					isFind = true ;
					gate_srv_key_[i].OnlineNum = pGateSrvMsg->Num ;
					gate_srv_key_[i].LastTime = GetTickCount();
					break;
				}
			}
			if (!isFind)
			{
				GateSrvKey kGateSrvKey;
				kGateSrvKey.OnlineNum = pGateSrvMsg->Num ;
				kGateSrvKey.LastTime = ::GetTickCount();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
				kGateSrvKey.GateAddr = addr ;
				kGateSrvKey.mapId = -1;
				gate_srv_key_.push_back(kGateSrvKey);
			}
			sort(gate_srv_key_.begin(),gate_srv_key_.end()) ;
		}			

		save_player_record();
		__TRY_END
	};


	void AssignUdpHandler::on_client_query_srv(const ACE_INET_Addr & remote_addr,char* ) 
	{			
		__TRY_BENGIN
		long TimeSep = GetTickCount() ;	
		Assign_namespace::ACNotifyASrvState  NotifySrvState ;
		memset(&NotifySrvState,0,sizeof(Assign_namespace::ACNotifyASrvState));

		//ACE_GUARD (ACE_Thread_Mutex,guard,mutex_) ;	
		do 	
		{			
			while(gate_srv_key_.size()>0)
			{
				if( (GetTickCount()-gate_srv_key_[0].LastTime) >SRVMSG_TIMEOUT)
				{
					vecGateSrvKey::iterator iter = gate_srv_key_.begin() ;
					gate_srv_key_.erase(iter) ;	
				}
				else
				{
					break ;
				}
			}

			if (gate_srv_key_.size()>0)
			{					
				if (gate_srv_key_[0].OnlineNum<boundary_[1])
				{
					NotifySrvState.State = 0 ;
				}
				else if (gate_srv_key_[0].OnlineNum<boundary_[2])
				{
					NotifySrvState.State = 1 ;
				}
				else 
				{
					NotifySrvState.State = 2 ;		
				}
			}
			else 
			{
				NotifySrvState.State = 2 ;					
			}

		}while(false);
		
		send_result_message(udp_,remote_addr,&NotifySrvState,Assign_namespace::ACNotifyASrvState::wCmd) ;
		SS_DEBUG(ACE_TEXT(" 发送消息时间(%d) 时间差额(%d)  \n"),GetTickCount(),GetTickCount()-TimeSep) ;

		__TRY_END ;
	};
	void AssignUdpHandler::on_client_request_gate_srv(const ACE_INET_Addr & remote_addr,char* Msg)	
	{	
		__TRY_BENGIN
		//ACE_HEX_DUMP((LM_ERROR,message->rd_ptr(),message->length())) ;
		long TimeSep = GetTickCount() ;
		SS_DEBUG(ACE_TEXT(" on_client_request_gate_srv收到消息时间(%d)  \n "),TimeSep) ;
		{
			//ACE_GUARD (ACE_Thread_Mutex,guard,mutex_) ;
			
			if (((int)gate_srv_key_.size()>1) && (gate_srv_key_[0].OnlineNum>(gate_srv_key_[1].OnlineNum+20)))
			{
				sort(gate_srv_key_.begin(),gate_srv_key_.end()) ;
			}
			else if (((int)gate_srv_key_.size()>0) && (gate_srv_key_[0].OnlineNum<boundary_[2]))
			{

			}
			else
			{
				SS_ERROR(ACE_TEXT(" OnClientRequestGateSrv 出错 gatesrv 的个数 ( %d )  当前最低的在线量 ( %d ) \n "),(int)gate_srv_key_.size(),((int)gate_srv_key_.size()>0 ? gate_srv_key_[0].OnlineNum :0)) ;
				return ;
			}			
		}
		
		
		Assign_namespace::ACNotifyGateSrvAddr NotifyClientGateSrvAddr ;
		memset(&NotifyClientGateSrvAddr,0,sizeof(Assign_namespace::ACNotifyGateSrvAddr)) ;
		{
			//ACE_GUARD (ACE_Thread_Mutex,guard,mutex_) ;
			NotifyClientGateSrvAddr.IP = gate_srv_key_[0].GateAddr.get_ip_address();
			NotifyClientGateSrvAddr.Port = gate_srv_key_[0].GateAddr.get_port_number();
			++ gate_srv_key_[0].OnlineNum ;
		}

		
		send_result_message(udp_,remote_addr,&NotifyClientGateSrvAddr,Assign_namespace::ACNotifyGateSrvAddr::wCmd) ;
		SS_DEBUG(ACE_TEXT(" 发送消息时间(%d) 时间差额(%d)  \n"),GetTickCount(),GetTickCount()-TimeSep) ;

		__TRY_END
	};

	struct check_map_id
	{
		unsigned mapId;

		check_map_id(unsigned id): mapId(id) {}

		const bool operator()(const GateSrvKey & k)const
		{
			return k.mapId == mapId;
		}
	};

	void AssignUdpHandler::on_client_request_gate_srv_with_account(const ACE_INET_Addr & remote_addr,char* Msg)
	{
		__TRY_BENGIN;

		if (!gate_srv_key_.size())
		{
			return;
		}

		/*if (gate_srv_key_[0].OnlineNum >= boundary_[2])
		{
			return;
		}*/

		Assign_namespace::CARequestGateSrvAddrWithAccount * req = 
			(Assign_namespace::CARequestGateSrvAddrWithAccount *)(Msg + PACKETHEADERLENGTH);

		account_t account(req->account, req->accountLen);
		std::map<account_t, unsigned>::iterator it = records_.find(account);
		if (it != records_.end())
		{
			check_map_id chk(it->second);
			vecGateSrvKey::iterator _it = std::find_if(gate_srv_key_.begin(), gate_srv_key_.end(), chk);
			if (_it != gate_srv_key_.end())
			{
				if (_it->OnlineNum < 2000)
				{
					Assign_namespace::ACNotifyGateSrvAddr notifyGateSrvAddr;
					notifyGateSrvAddr.IP = _it->GateAddr.get_ip_address();
					notifyGateSrvAddr.Port = _it->GateAddr.get_port_number();
					++_it->OnlineNum;
					send_result_message(udp_,remote_addr, &notifyGateSrvAddr, Assign_namespace::ACNotifyGateSrvAddr::wCmd) ;
				}
				else
				{
					on_client_request_gate_srv(remote_addr, 0);
				}
			}
			else
			{
				chk.mapId = -1;
				_it = std::find_if(gate_srv_key_.begin(), gate_srv_key_.end(), chk);
				if (_it != gate_srv_key_.end())
				{
					_it->mapId = it->second;
				}
				on_client_request_gate_srv(remote_addr, 0);
			}
		}
		else
		{
			records_[account] = -1;
			on_client_request_gate_srv(remote_addr, 0);
		}

		__TRY_END;
	}

	void AssignUdpHandler::on_gate_srv_notify_player_map(const ACE_INET_Addr & remote_addr,char* Msg)
	{
		__TRY_BENGIN;

		Assign_namespace::GANotifyPlayerMapId * notify = (
			Assign_namespace::GANotifyPlayerMapId *)(Msg + PACKETHEADERLENGTH);

		account_t account(notify->account, notify->accountLen);
		records_[account] = notify->mapId;

		// TODO:
		__TRY_END;
	}

	void AssignUdpHandler::save_player_record(bool force)
	{
		if (!force)
		{
			long now = GetTickCount();
			if (now - last_recrods_update_time_ > 1000 * 60 * 5)
			{
				force = true;
			}
		}

		if (force)
		{
			std::fstream file("records.log", std::ios_base::out | std::ios::trunc);
			for(std::map<account_t, unsigned>::iterator it = records_.begin();
				it != records_.end(); ++it)
			{				
				file << it->second << " " << it->first.c_str() << std::endl;
			}
			last_recrods_update_time_ = GetTickCount();
		}
	}

	int AssignUdpHandler::send_result_message(UdpService * udp,const ACE_INET_Addr & remote_addr,void *data, int msgId) 
	{
		ACE_Message_Block* pBlock = build_result_message(data, msgId) ;	

		if (pBlock->length()<5)
		{
			SS_ERROR(ACE_TEXT("AssignUdpHandler::send_result_message | sent message length is less 5\n")) ;
			pBlock->release();
			return -1;
		}
		SS_DEBUG(ACE_TEXT("AssignUdpHandler::send_result_message | sent message id is %d\n"), msgId);
		udp_->post_asynch_write(client_id_, remote_addr, pBlock, 0);
		return 0;
	}

