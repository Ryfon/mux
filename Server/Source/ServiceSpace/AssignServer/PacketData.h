﻿
#pragma  once


#define PACKETHEADERLENGTH sizeof(unsigned char)*5

/**
*   第一部分：
*   以下为参与通信各方的类别标识号，程序自身的标识号应在本文件之外的某处定义好
*   例如：客户端程序应该这样定义，const int SELF_PROTO_TYPE = 0x00;
*	参与者类别	类别编号	简写
*	Client			0		C
*	Gatesrv			1		G
*	Assignsrv		2		A
*/

#define C_A  	0x02	//客户端发往assignsrv的消息；
#define A_C  	0x20	//assign发往客户端的消息；
#define A_G  	0x21	//assign发往gatesrv的消息；
#define G_A  	0x12	//gatesrv发往assign的消息；



const char * const message_header_format = "%2u%1u%2u";

inline void build_message_header(char * buf, short msgLen, char msgType, short msgId)
{
	short * tmp = (short *)buf;
	//*tmp = htons(msgLen);
	*tmp = msgLen;
	buf += 2;
	*buf = msgType;
	++buf;
	tmp = (short *)buf;
	//*tmp = htons(msgId);
	*tmp = msgId;
}

inline void parse_message_header(char * buf, short & msgLen, char & msgType, short & msgId)
{
	short * tmp = (short *)buf;
	//msgLen = ntohs(*tmp);
	msgLen = *tmp;
	buf += 2;
	msgType = *buf;
	++buf;
	tmp = (short *)buf;
	//msgId = ntohs(*tmp);
	msgId = *tmp;
}

inline void Set_message_header_len(char * buf, unsigned short & msgLen)
{
	unsigned short * tmp = (unsigned short *)buf;
	//*tmp = htons(msgLen);
	*tmp = msgLen;
}

inline void get_message_header_len(char * buf, unsigned short & msgLen)
{
	unsigned short * tmp = (unsigned short *)buf;
	//msgLen = ntohs(*tmp);
	msgLen = *tmp;
}




//
//#define	CMDLENGTH sizeof(unsigned short)
//#define PACKETHEADERLENGTH sizeof(PACKETHEADER)
//#define PACKETHEADERLENGTH sizeof(PACKETHEADER)
//#define MAXPACKETLENGTH			2048
//#define MAXCOMMUPACKETBLICKED	1024
//#define BUFFLEN                 1024*16 

//typedef struct  
//{
//	unsigned short PacketLen;
//	unsigned short Cmd;
//}PACKETHEADER,*PPACKETHEADER;
//
//
//typedef struct  
//{
//	PPACKETHEADER	kPackerHeader;
//	char			PacketData[BUFFLEN] ;
//}PACKETDATA,*PPACKETDATA;




