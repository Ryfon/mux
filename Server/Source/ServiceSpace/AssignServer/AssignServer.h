﻿#define ACE_BUILD_SVC_DLL

#define  SS_NDEBUG

#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/UdpService.h"
#include "../ServiceSpaceLib/UdpHandler.h"
#include <ace/Dynamic_Service.h>
#include <ace/ACE.h>

#include "AssignProtocol.h"
#include "PacketData.h"

#include "../ServiceSpaceLib/GetOpts.h"
#include "../ServiceSpaceLib/utilib/split.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include <string>
#include <vector>
#include <fstream>
#include <map>

__SERVICE_SPACE_USING_NS__

#define __TRY_BENGIN try {
#define __TRY_END  } catch (std::exception& e) { SS_ERROR(ACE_TEXT(" 在 %s 文件 的 %d 行发生异常！err = %s \n"),__FILE__,__LINE__,(e.what() ? e.what() : "无原因" )) ; }


#ifndef WIN32
#include <sys/times.h>
long GetTickCount()
{
	tms tm;
	return times(&tm) ;
}
#define  SRVMSG_TIMEOUT (600*100)
#else
#define  SRVMSG_TIMEOUT (30*1000)
#endif

typedef struct GATESRVKEY
{
	ACE_INET_Addr GateAddr;
	unsigned short OnlineNum ;
	unsigned int LastTime;
	unsigned mapId;

	
	bool operator < (const GATESRVKEY &m) const
	{

		return OnlineNum < m.OnlineNum ;
	}
}GateSrvKey ;

typedef std::vector<GateSrvKey> vecGateSrvKey; 




class ACE_Svc_Export AssignUdpHandler
	: public UdpHandler
{
public:
		
	int init(int argc, ACE_TCHAR *argv[]);
	int open(void *args );

	int close(u_long flags);

	void handle_message_read(const ACE_INET_Addr & remote_addr, ACE_Message_Block * message, void * act, int error, void * user_data);
	void handle_message_write(const ACE_INET_Addr & , ACE_Message_Block * message, void * , int , void * )
	{
		if(message)
			message->release();
	}
private:
	ACE_Message_Block * build_result_message(void * data, int msgId);

	void on_gate_srv_msg(const ACE_INET_Addr & remote_addr,char* Msg);
	void on_client_query_srv(const ACE_INET_Addr & remote_addr,char* Msg) ;
	void on_client_request_gate_srv(const ACE_INET_Addr & remote_addr,char* Msg);

	void on_client_request_gate_srv_with_account(const ACE_INET_Addr & remote_addr,char* Msg);

	void on_gate_srv_notify_player_map(const ACE_INET_Addr & remote_addr,char* Msg);

	void parse_addr(int argc, ACE_TCHAR *argv[]);
	void parse_port(int argc, ACE_TCHAR *argv[]);
	void parse_boundary(int argc, ACE_TCHAR *argv[]);

	void save_player_record(bool force = false);

	int send_result_message(UdpService * udp,const ACE_INET_Addr & remote_addr,void *data, int msgId) ;
	int gate_srv_id_ ;
	int client_id_;

	void * udp_1_ ;////监听服务器端口
	void * udp_2_ ;////监听客户端端口

	vecGateSrvKey gate_srv_key_ ;

	int boundary_[3] ;////上中下等人数上限

	Assign_namespace::AssignProtocol assign_proto_ ;

	ACE_Thread_Mutex mutex_ ;

	ACE_INET_Addr gate_addr_;
	ACE_INET_Addr listen_addr ;

	UdpService * udp_;

	int num_ ;

	typedef utilib::stringN<16> account_t;

	std::map<account_t, unsigned> records_;
	long last_recrods_update_time_;
};

extern "C" ACE_Svc_Export AssignUdpHandler * CreateAssignUdpHandler()
{
	return new AssignUdpHandler();
}


ACE_SVC_FACTORY_DECLARE(AssignUdpHandler);
ACE_SVC_FACTORY_DEFINE(AssignUdpHandler);





