﻿/********************************************************************
	created:	2008/05/30
	created:	30:5:2008   10:02
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\UdpHandler.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	UdpHandler
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __SERVICE_SPACE_UDP_HANDLER_H__
#define __SERVICE_SPACE_UDP_HANDLER_H__

#pragma once

#include "StdHdr.h"
#include "ServiceTask.h"
#include <ace/SOCK_Dgram.h>
#include <ace/INET_Addr.h>

__SERVICE_SPACE_BEGIN_NS__

class ACE_Svc_Export UdpHandler
	: public ServiceTask
{
public:
	virtual int init(int argc, ACE_TCHAR *argv[]);

	virtual void handle_message_read(const ACE_INET_Addr & remote_addr, ACE_Message_Block * message, void * act, int error, void * user_data);

	virtual void handle_message_write(const ACE_INET_Addr & remote_addr, ACE_Message_Block * message, void * act, int error, void * user_data);

protected:
	virtual int handle_task_message(ACE_Message_Block * message);

	ACE_SOCK_Dgram udp_;
};

__SERVICE_SPACE_END_NS__

#endif
