﻿/********************************************************************
	created:	2008/06/02
	created:	2:6:2008   13:18
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\MessageHandler.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	MessageHandler
	file ext:	h
	author:		
	
	purpose:	通用的消息分派器，注册处理特定消息的函数，消息到达时，
				相应的处理函数自动被调用。
*********************************************************************/

#ifndef __SERVICE_SPACE_MESSAGE_HANDLER_H__
#define __SERVICE_SPACE_MESSAGE_HANDLER_H__

#pragma once

#include "StdHdr.h"
#include "ServiceTask.h"
#include "TcpSession.h"
#include "GetOpts.h"

#include "utilib/function_map.h"

__SERVICE_SPACE_BEGIN_NS__

template<class Handler>
class MessageHandler
	: public ServiceTask
{
public:
	virtual int init(int argc, ACE_TCHAR * argv[])
	{
		GetOpts<ACE_TCHAR> opts(argc, argv);
		int err = ServiceTask::init(argc, argv);
		if (-1 == err)
			return -1;

		offset_ = -1;
		bytes_ = -1;

		const ACE_TCHAR * options = ACE_TEXT(":o:b:");
		ACE_Get_Opt get_opt(argc, (ACE_TCHAR **)opts.argv, options);
		int c;
		while((c = get_opt()) != EOF)
		{
			switch(c)
			{
			case 'o':
				{
					offset_ = ACE_OS::atoi(get_opt.optarg);
				}
				break;
			case 'b':
				{
					bytes_ = ACE_OS::atoi(get_opt.optarg);
				}
				break;
			}
		}

		if (offset_ == -1 || bytes_ == -1 || bytes_ == 0 || bytes_ > 4)
			return -1;

		err = this->RegisterTask();

		return err;
	}

protected:
	typedef int (Handler::*handler_type)(tcp_session & session, const char * message, unsigned len);
	typedef utilib::function_map<int,handler_type> handler_map;

	void register_handler(int msgid, handler_type handler)
	{
#ifdef WIN32
		handlers_.insert(typename handler_map::value_type(msgid, typename handler_map::value_type::second_type(dynamic_cast<Handler *>(this), handler)));
#else
		handlers_.insert(typename handler_map::value_type(msgid, typename handler_map::value_type::second_type((Handler *)this, handler)));
#endif
	}

	virtual int get_message_id(ACE_Message_Block * message)
	{
		if (!message ||
			message->length() <= offset_ ||
			message->length() <= bytes_)
			return -1;

		size_t len = message->length();
		int id = -1;
		if (bytes_ == 1 && len > 1 + offset_)
		{
			id = (unsigned char)*(message->rd_ptr() + offset_);
		}
		else if (bytes_ == 2 && len > 2 + offset_)
		{
			unsigned short tmp = (unsigned short)*(message->rd_ptr() + offset_);
			id = ntohs(tmp);
		}
		else if (bytes_ == 4 && len > 4 + offset_)
		{
			unsigned tmp = (unsigned)*(message->rd_ptr() + offset_);
			id = ntohl(tmp);
		}

		return id;
	}

	int dispatch_message_handler(tcp_session & session, ACE_Message_Block * message)
	{
		int msgId = get_message_id(message);
		SS_DEBUG(ACE_TEXT("[DEBUG] recv gate server  msgid = %d  \n"), msgId);
		typename handler_map::iterator it = handlers_.find(msgId);
		int err = -1;
		if (it != handlers_.end())
			err = it->second(session, message->rd_ptr(), (int)message->length());
		else
			SS_ERROR(ACE_TEXT("MessageHandler::dispatch_message_handler | unknown message id: %d\n"), msgId);
		return err;
	}

	int handle_task_message(ACE_Message_Block * message) = 0;

	handler_map handlers_;
	size_t offset_;
	size_t bytes_;
};

__SERVICE_SPACE_END_NS__

#endif
