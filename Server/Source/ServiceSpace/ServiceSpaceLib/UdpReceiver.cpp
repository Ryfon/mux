﻿#define ACE_BUILD_SVC_DLL

#include "UdpReceiver.h"
#include "UdpService.h"
#include "ObjectMessageBlock.h"

__SERVICE_SPACE_BEGIN_NS__

__SERVICE_SPACE_OBJ_DYNAMIC_NEW_DEFINITION__(UdpReceiver)

UdpReceiver::UdpReceiver(UdpService * udp /* = 0 */)
: udp_(udp), user_data_(0)
{

}


int UdpReceiver::open(const ACE_INET_Addr & addr, UdpService * udp /* = 0 */, void * _user_data /* = 0 */)
{
	int err = 0;
	address(addr);

	udp_ = udp ? udp : udp_;
	if (!udp_)
	{
		SS_ERROR(ACE_TEXT("UdpReceiver::open | no udp service\n"));
		return -1;
	}

	if (sock_dgram_.open(addr) == -1)
	{
		SS_ERROR(ACE_TEXT("[error:%d]%p\n"), errno, ACE_TEXT("UdpReceiver::open | sock dgram failed to open"));
		return -1;
	}

#ifdef SS_USE_PROACTOR

	BOOL flag = FALSE;
	DWORD bytes  = 0;
	::WSAIoctl((SOCKET)sock_dgram_.get_handle(), SIO_UDP_CONNRESET, &flag, sizeof(flag), NULL, 0, &bytes, NULL, NULL);

	if (read_dgram_.open(*udp_, sock_dgram_.get_handle()) == -1)
	{
		SS_ERROR(ACE_TEXT("[error:%d]%p\n"), errno, ACE_TEXT("UdpReceiver::open | asynch read dgram failed to open"));
		return -1;
	}

	if (write_dgram_.open(*udp_, sock_dgram_.get_handle()) == -1)
	{
		SS_ERROR(ACE_TEXT("[error:%p]\n"), ACE_TEXT("UdpReceiver::open | asynch write dgram failed to open"));
		return -1;
	}
#else
	err = this->activate(THR_NEW_LWP | THR_JOINABLE | THR_INHERIT_SCHED, 2);

	sender_.open(&sock_dgram_, udp_, user_data_);
#endif

	user_data_ = _user_data;

	return err;
}

#ifdef SS_USE_PROACTOR
void UdpReceiver::post_asynch_read(ACE_Message_Block * block /* = 0 */, void * act /* = 0 */)
#else
void UdpReceiver::post_asynch_read(ACE_Message_Block * block /* = 0 */, void *  act/* = 0 */)
#endif
{
	try{
#ifdef SS_USE_PROACTOR
		if (!block)
			block = new MessageBlock();
		if (!block)
			throw std::bad_alloc();
		int err = 0;
		size_t read_bytes;
		err = read_dgram_.recv(block, read_bytes, 0, PF_INET, act);
		if (err == 0)
			;
		else if (err == 1)
			err = 0;
		else
		{
			err = errno;
			udp_->handle_post_asynch_read_error(address_, block, act, user_data_, err);
		}
#else
		delete ((UdpService::ACT *)act);
		/*ObjectMessageBlock<Asynch_Op> * op_msg = new ObjectMessageBlock<Asynch_Op>();
		Asynch_Op & op = op_msg->obj();
		op.flag = Asynch_Op::ASYNCH_OP_READ_UDP;
		op.msg = block;
		op.act = act;
		this->putq(op_msg);*/
#endif
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void UdpReceiver::post_asynch_write(const ACE_INET_Addr & remote_addr, const char * message, size_t size, void * act /* = 0 */)
{
	try{
		if (!size || !message)
			return ;

		ACE_Message_Block * block = new MessageBlock();
		if (!block)
			throw std::bad_alloc();
		if (size > block->capacity())
		{
			SS_DEBUG(ACE_TEXT("UdpReceiver::post_asynch_write | message length goes beyond the [%d] limit\n"), block->space());
			return;
		}

		block->copy(message, size);

		post_asynch_write(remote_addr, block, act);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void UdpReceiver::post_asynch_write(const ACE_INET_Addr & remote_addr, ACE_Message_Block * block /* = 0 */, void * act /* = 0 */)
{
	try{
#ifdef SS_USE_PROACTOR
		if (!block)
			return ;

		int err = 0;
		size_t sent_bytes = 0;
		err = write_dgram_.send(block, sent_bytes, 0, remote_addr, act);
		if (err == 0)
			;
		else if (err == 1)
			err = 0;
		else
		{
			err = errno;
			udp_->handle_post_asynch_write_error(remote_addr, block, act, user_data_, err);
		}
#else
		ObjectMessageBlock<Asynch_Op> * op_msg = new ObjectMessageBlock<Asynch_Op>();
		if (!op_msg)
			throw std::bad_alloc();
		Asynch_Op & op = op_msg->obj();
		op.flag = Asynch_Op::ASYNCH_OP_WRITE_UDP;
		op.msg = block;
		op.act = act;
		op.address = remote_addr;
		sender_.putq(op_msg);
#endif
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


const ACE_INET_Addr & UdpReceiver::address()const
{
	return address_;
}


void UdpReceiver::address(const ACE_INET_Addr & addr)
{
	address_ = addr;
}


void UdpReceiver::user_data(void * ud)
{
	user_data_ = ud;
}


void * UdpReceiver::user_data()const
{
	return user_data_;
}

#ifndef SS_USE_PROACTOR
int UdpReceiver::svc()
{
	int err = 0;

	while (true)
	{
		try {
			ACE_Message_Block * block = new MessageBlock();
			ACE_INET_Addr remote_addr;
			ssize_t size = sock_dgram_.recv(block->wr_ptr(), block->space(), remote_addr, 0);
			if (-1 != size)
			{
				block->wr_ptr(size);
				udp_->handle_post_asynch_read_error(remote_addr, block, 0, user_data_, 0);
			}
			else
			{
				ACE_ERROR((LM_ERROR, ACE_TEXT("error:%p\n"), ACE_TEXT("UdpReceiver::recv")));
				block->release();
			}
		}catch(const std::exception & e)
		{
			SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
		}catch(...)
		{
			SS_ERROR(ACE_TEXT("unknown exception\n"));
		}
	}

	return err;
}
#endif

__SERVICE_SPACE_END_NS__
