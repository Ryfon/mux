﻿/********************************************************************
	created:	2008/07/07
	created:	7:7:2008   14:53
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\TcpConnector.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	TcpConnector
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __SERVICE_SPACE_TCP_CONNECTOR_H__
#define __SERVICE_SPACE_TCP_CONNECTOR_H__

#include "StdHdr.h"
#include "TcpSession.h"
#include "utilib/noncopyable.h"
#include <ace/Asynch_Connector.h>
#include <ace/Connector.h>
#include <ace/SOCK_Connector.h>

__SERVICE_SPACE_BEGIN_NS__

class TcpService;

#ifdef SS_USE_PROACTOR

class ACE_Svc_Export TcpConnector
	: public ACE_Asynch_Connector<TcpSession>
	, public utilib::noncopyable
{
public:
	friend class TcpService;

	typedef TcpSession session_type;
	typedef session_type::session_ptr session_ptr;

	TcpConnector(TcpService * tcp = 0);

	void * user_data();

	void user_data(void * ud);

protected:
	virtual void handle_connect (const ACE_Asynch_Connect::Result &result);

	virtual void handle_session_built(session_ptr & session, int error, void * act);

	void * user_data_;
	TcpService * tcp_;

	int pass_addresses_;
	int validate_new_connection_;
};

#else

class ACE_Svc_Export TcpConnector
	: public ACE_Connector<TcpSession, ACE_SOCK_CONNECTOR>
	, public utilib::noncopyable
{
public:
	TcpConnector();

	void * user_data();

	void user_data(void * d);

private:
	void * user_data_;

	friend class TcpService;
};

#endif
__SERVICE_SPACE_END_NS__

#endif
