﻿#pragma once

#include "basic_binary_ostream.h"

namespace utilib
{
	class basic_binary_stream : public basic_binary_istream, public basic_binary_ostream
	{
	public:
		basic_binary_stream(basic_stream_buffer & buf)
			: basic_binary_istream(buf), basic_binary_ostream(buf)
		{ }

		~basic_binary_stream()
		{ }

	}; // class binary_stream

} // namespace utilib