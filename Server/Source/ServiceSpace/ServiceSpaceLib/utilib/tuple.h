﻿/********************************************************************
	created:	2008/06/13
	created:	13:6:2008   10:42
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\tuple.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	tuple
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __UTILIB_TUPLE_H__
#define __UTILIB_TUPLE_H__

#include "null_type.h"
#include "int2type.h"
#include <utility>

namespace utilib
{
	template<class T1 = null_type, class T2 = null_type, class T3 = null_type, class T4 = null_type, class T5 = null_type,
			 class T6 = null_type, class T7 = null_type, class T8 = null_type, class T9 = null_type, class T10 = null_type,
			 class T11 = null_type, class T12 = null_type, class T13 = null_type, class T14 = null_type, class T15 = null_type,
			 class T16 = null_type, class T17 = null_type, class T18 = null_type, class T19 = null_type, class T20 = null_type,
			 class T21 = null_type, class T22 = null_type, class T23 = null_type, class T24 = null_type, class T25 = null_type,
			 class T26 = null_type, class T27 = null_type, class T28 = null_type, class T29 = null_type, class T30 = null_type,
			 class T31 = null_type, class T32 = null_type, class T33 = null_type, class T34 = null_type, class T35 = null_type,
			 class T36 = null_type, class T37 = null_type, class T38 = null_type, class T39 = null_type, class T40 = null_type
	> 
	class tuple;

	template<>
	class tuple<null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type,
				null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type,
				null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type,
				null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type>
	{
	public:
		enum { COUNTS = 0 };

		template<int>
		struct element
		{
			typedef null_type type;
		};
	};

//	namespace _internal
//	{
#ifdef WIN32

#else

	template<int N, class Tuple>
	class element;

	template<class T1, class T2,class T3, class T4,class T5,class T6, class T7,class T8, class T9,class T10,
		class T11, class T12,class T13, class T14,class T15,class T16, class T17,class T18, class T19,class T20,
		class T21, class T22,class T23, class T24,class T25,class T26, class T27,class T28, class T29,class T30,
		class T31, class T32,class T33, class T34,class T35,class T36, class T37,class T38, class T39,class T40
	> 
	class element<0, tuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10,
		T11, T12,T13, T14, T15, T16,  T17, T18,  T19, T20,
		T21, T22,T23, T24, T25, T26,  T27, T28,  T29, T30,
		T31, T32,T33, T34, T35, T36,  T37, T38,  T39, T40 >  
	>
	{
	public:
		typedef T1 type;
	};

	template<int N, class T1, class T2,class T3, class T4,class T5,class T6, class T7,class T8, class T9,class T10,
		class T11, class T12,class T13, class T14,class T15,class T16, class T17,class T18, class T19,class T20,
		class T21, class T22,class T23, class T24,class T25,class T26, class T27,class T28, class T29,class T30,
		class T31, class T32,class T33, class T34,class T35,class T36, class T37,class T38, class T39,class T40
	> 
	class element<N, tuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10,
		T11, T12,T13, T14, T15, T16,  T17, T18,  T19, T20,
		T21, T22,T23, T24, T25, T26,  T27, T28,  T29, T30,
		T31, T32,T33, T34, T35, T36,  T37, T38,  T39, T40 >  
	>
	{
		typedef tuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10,
			T11, T12,T13, T14, T15, T16,  T17, T18,  T19, T20,
			T21, T22,T23, T24, T25, T26,  T27, T28,  T29, T30,
			T31, T32,T33, T34, T35, T36,  T37, T38,  T39, T40 > tuple_type;
	public:
		typedef typename element<N-1, typename tuple_type::rest_tuple>::type type;
	};


#endif

		template<class T1, class T2,class T3, class T4,class T5,class T6, class T7,class T8, class T9,class T10,
				 class T11, class T12,class T13, class T14,class T15,class T16, class T17,class T18, class T19,class T20,
				 class T21, class T22,class T23, class T24,class T25,class T26, class T27,class T28, class T29,class T30,
				 class T31, class T32,class T33, class T34,class T35,class T36, class T37,class T38, class T39,class T40
		> 
		class tuple
			: protected tuple<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20,
							T21, T22, T23, T24, T25, T26, T27, T28, T29, T30, T31, T32, T33, T34, T35, T36, T37, T38, T39, T40>
		{
			typedef tuple<T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T20,
				T21, T22, T23, T24, T25, T26, T27, T28, T29, T30, T31, T32, T33, T34, T35, T36, T37, T38, T39, T40> base;

		public:
			enum { COUNTS = 1 + base::COUNTS };

			typedef base rest_tuple;	
#ifdef WIN32
			template<int>
			struct element;

			template<>
			struct element<0>
			{
				typedef T1 type;
			};

			template<int N>
			struct element
			{
				typedef typename base::template element<N-1>::type type;
			};
#else

#endif

		public:
#ifdef WIN32
			const typename element<0>::type & value_i(int2type<0> &)const
			{
				return t_;
			}

			template<int N>
			const typename element<N>::type & value_i(int2type<N> &)const
			{
				return base::value_i(int2type<N-1>());
			}

			typename element<0>::type & value_i(int2type<0> &)
			{
				return t_;
			}

			template<int N>
			typename element<N>::type & value_i(int2type<N> &)
			{
				return base::value_i(int2type<N-1>());
			}
#else
			const typename element<0, tuple>::type & value_i(int2type<0> &)const
			{
				return t_;
			}

			template<int N>
			const typename element<N, tuple>::type & value_i(int2type<N> &)const
			{
				int2type<N - 1> t;
				return base::value_i(t);
			}

			typename element<0, tuple>::type & value_i(int2type<0> &)
			{
				return t_;
			}

			template<int N>
			typename element<N, tuple>::type & value_i(int2type<N> &)
			{
				int2type<N - 1> t;
				return base::value_i(t);
			}
#endif

		public:
#ifdef WIN32

			template<int N>
			const typename element<N>::type & value()const
			{
				return value_i(int2type<N>());
			}

			template<int N>
			void value(const typename element<N>::type & t)
			{
				value_i(int2type<N>()) = t;
			}
#else
			template<int N>
			const typename element<N, tuple>::type & value()const
			{
				int2type<N> t;
				return value_i(t);
			}

			template<int N>
			void value(const typename element<N, tuple>::type & t)
			{
				int2type<N> t2;
				value_i(t2) = t;
			}
#endif

			rest_tuple & operator<<(const T1 & t)
			{
				t_ = t;
				return (rest_tuple &)*this;
			}

		private:
			T1 t_;

		}; 


} // namespace utilib

#endif
