﻿/********************************************************************
	created:	2008/06/20
	created:	20:6:2008   10:22
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\obj_pool.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	obj_pool
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __UTILIB_MEM_POOL_H__
#define __UTILIB_MEM_POOL_H__

#include <stdexcept>
#include "pool_lock.h"
#include "block_pool.h"

namespace utilib
{
	// decouple

	template<class T, size_t Count, class Grow, class Lock>
	struct __obj_alloc
	{
		typedef block_pool<sizeof(T), Count, Grow, Lock, true> __alloc_type;
		static __alloc_type __alloc;
	};

	template<class T, size_t Count, class Grow, class Lock>
	typename __obj_alloc<T, Count, Grow, Lock>::__alloc_type __obj_alloc<T, Count, Grow, Lock>::__alloc;

	template<class, size_t, class, class>
	struct __obj_alloc_type_ship { };

	template<class T, size_t Count, class Grow, class Lock>
	inline void * __obj_allocate(__obj_alloc_type_ship<T, Count, Grow, Lock> &)
	{
		return __obj_alloc<T, Count, Grow, Lock>::__alloc.allocate();
	}

	template<class T, size_t Count, class Grow, class Lock>
	inline void __obj_deallocate(__obj_alloc_type_ship<T, Count, Grow, Lock> &, void *p)
	{
		__obj_alloc<T, Count, Grow, Lock>::__alloc.deallocate(p);
	}

	template<
		class T, 
		size_t Count = 50,
		class Grow = grow::arithmetic<1>,
		class Lock = pool_lock
	>
	class obj_pool
	{
	public:
		typedef T value_type;
		typedef value_type * pointer;
		typedef const value_type * const_pointer;
		typedef value_type & reference;
		typedef const value_type & const_reference;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;

		template<class U>
		struct rebind
		{
			typedef obj_pool<U, Count, Grow, Lock> other;
		};

		T * allocate(size_t n = 1, void * = 0)
		{
			if (n == 1)
			{
				__obj_alloc_type_ship<T, Count, Grow, Lock> tmp;
				return (pointer)__obj_allocate(tmp);
			}
			return 0;
		}

		T * allocate(const T & val)
		{
			void * p = allocate();
			return new(p) T(val);
		}

		template<class _Initializer>
		T * allocate(_Initializer initer)
		{
			T * p = static_cast<T *>(allocate());
			initer(p);
			return p;
		}

		void deallocate(void * p, size_t n = 1)
		{
			if (n == 1)
			{
				__obj_alloc_type_ship<T, Count, Grow, Lock> ship;
				__obj_deallocate(ship, p);
			}
		}

		size_type max_size()const { return 1; }
	};
}

#endif
