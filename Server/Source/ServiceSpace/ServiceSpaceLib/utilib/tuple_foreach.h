﻿/********************************************************************
	created:	2008/06/13
	created:	13:6:2008   12:47
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\tuple_foreach.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	tuple_foreach
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_TUPLE_FOREACH_H__
#define __SERVICE_SPACE_TUPLE_FOREACH_H__

#include "tuple.h"
#include "int2type.h"

namespace utilib
{
	namespace __internal
	{

		template<class Tuple, class Vistor, size_t Index = 0>
		struct tuple_foreach_i;

		template<class Vistor, size_t Index>
		struct tuple_foreach_i<tuple<>, Vistor, Index>
		{
			typedef tuple<> tuple_type;

			tuple_foreach_i(tuple_type & tu, Vistor &) { }
		};


	/*	template<class T1, class T2,class T3, class T4,class T5,class T6, class T7,class T8, class T9,class T10,
				 class T11, class T12,class T13, class T14,class T15,class T16, class T17,class T18, class T19,class T20,
				 class T21, class T22,class T23, class T24,class T25,class T26, class T27,class T28, class T29,class T30,
				 class T31, class T32,class T33, class T34,class T35,class T36, class T37,class T38, class T39,class T40,
				 class Vistor, size_t Index
		>*/

		template<class Tuple, class Vistor, size_t Index>
		struct tuple_foreach_i
		{
			typedef Tuple tuple_type;

#ifdef WIN32
			template<bool _do_foreach>
			struct _foreach_i;
			
			template<>
			struct _foreach_i<true>
			{
				static void do_foreach(tuple_type & tu, Vistor & vistor)
				{
					
				}
			};

			template<>
			struct _foreach_i<false>
			{
				static void do_foreach(tuple_type & tu, Vistor & vistor) 
				{ 
					if (0 == vistor(tu.value<Index>()))
						tuple_foreach_i<tuple_type, Vistor, Index+1>(tu, vistor);
				}
			};

			tuple_foreach_i(tuple_type & tu, Vistor & vistor)
			{
				_foreach_i<is_null_type<typename tuple_type::element<Index>::type>::Result>::do_foreach(tu, vistor);
			}
#endif
			
		};

	} // namespace __internal

	template<class Tuple, class Vistor>
	struct tuple_foreach
		: protected __internal::tuple_foreach_i<Tuple, Vistor, 0>
	{
	private:
		typedef __internal::tuple_foreach_i<Tuple, Vistor, 0> base;

	public:
		tuple_foreach(Tuple & tu, Vistor & vistor)
			: base(tu, vistor)
		{

		}
	};

//#ifdef WIN32

//#else
	template<class Tuple, class Vistor, int Index>
	void do_tuple_foreach_i(Tuple & tu, Vistor & vistor, int2type<Index>)
	{
		//vistor(tu.value<Index>());
		int2type<Index> t;
		vistor(tu.value_i(t));
		do_tuple_foreach_i(tu, vistor, int2type<Index - 1>());
	}

	template<class Tuple, class Vistor>
	void do_tuple_foreach_i(Tuple & tu, Vistor & vistor, int2type<0> )
	{
		//vistor(tu.value<0>());
		int2type<0> t;
		vistor(tu.value_i(t));
	}
//#endif

	template<class Tuple, class Vistor>
	void do_tuple_foreach(Tuple & tu, Vistor & vistor)
	{
		
#ifdef WIN32
		tuple_foreach<Tuple, Vistor>(tu, vistor);
#else
		do_tuple_foreach_i(tu, vistor, int2type<Tuple::COUNTS - 1>());
#endif
	}
}

#endif
