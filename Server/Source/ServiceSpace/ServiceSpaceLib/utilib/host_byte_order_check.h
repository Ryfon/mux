﻿#pragma once

namespace utilib
{
	inline const bool is_host_byte_order_same_as_network()
	{
		// check byte order is big-endian
		unsigned i = 1;
		return *((char *)&i) == 0;
	}

} // namespace utilib