﻿#ifndef __UTILIB_TO_VALUE_H__
#define __UTILIB_TO_VALUE_H__

#include <strstream>

namespace utilib
{
	template<class T>
	void to_value(const char * str, T & t);

	inline void to_value(const char * str, char & ch)
	{
		ch = *str;
	}

	inline void to_value(const char * str, unsigned char & ch)
	{
		ch = (unsigned char)*str;
	}

	inline void to_value(const char * str, short & n)
	{
		int _n = atoi(str);
		n = (short)_n;
	}

	inline void to_value(const char * str, unsigned short & n)
	{
		int _n = atoi(str);
		n = (unsigned short)_n;
	}

	inline void to_value(const char * str, int & n)
	{
		n = atoi(str);
	}

	inline void to_value(const char * str, unsigned int & n)
	{
		std::strstream s;
		s << str;
		s >> n;
	}

	inline void to_value(const char * str, long long & n)
	{
		std::strstream s;
		s << str;
		s >> n;
	}

	inline void to_value(const char * str, unsigned long long & n)
	{
		std::strstream s;
		s << str;
		s >> n;
	}

	inline void to_value(const char * str, char * to)
	{
		::strcpy(to, str);
	}

	inline void to_value(const char * str, float & f)
	{
		::sscanf(str, "%f", &f);
	}

	template<class _Elem, class _Traits, class _Ax>
	inline void to_value(const std::basic_string<_Elem, _Traits, _Ax> & str, char * to)
	{
		::strcpy(to, str.c_str());
	}

	template<class _Elem, class _Traits, class _Ax>
	inline void to_value(const char * str, std::basic_string<_Elem, _Traits, _Ax> & to)
	{
		to = str;
	}

} // namespace utilib

#endif
