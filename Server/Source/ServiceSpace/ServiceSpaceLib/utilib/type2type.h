﻿#ifndef __UTILIB_TYPE2TYPE_H__
#define __UTILIB_TYPE2TYPE_H__

namespace utilib
{
	template<class T>
	struct type2type
	{
		typedef T type;
	};
} // namespace utilib

#endif

