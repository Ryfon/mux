﻿/********************************************************************
	created:	2008/06/20
	created:	20:6:2008   14:42
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\pool_alloc.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	pool_alloc
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __UTILIB_STD_POOL_H__
#define __UTILIB_STD_POOL_H__

#include "small_pool.h"

namespace utilib
{
	template<
		class T, 
		size_t Align = 4,
		size_t MaxBytes = 512,
		size_t Count = 50, 
		class Grow = grow::arithmetic<1>, 
		class Lock = pool_lock
	>
	class pool_alloc
	{
	public:
		enum { TALIGN = Align };
		typedef T value_type;
		typedef value_type * pointer;
		typedef const value_type * const_pointer;
		typedef value_type & reference;
		typedef const value_type & const_reference;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;

		template<class U>
		struct rebind
		{
			typedef pool_alloc<U, TALIGN, MaxBytes, Count, Grow, Lock> other;
		};

		pool_alloc()throw() { }

		template<class U>
		pool_alloc(const pool_alloc<T, TALIGN, MaxBytes, Count, Grow, Lock> &)throw() { }

		~pool_alloc() { }

		pointer address(reference r) { return &r; }

		const_pointer address(const_reference r)const { return &r; }

		pointer allocate(size_type n = 1, const void * hint = 0)
		{
			size_type bytes = n * sizeof(T);
			pointer p = 0;
			if (bytes > small_pool<TALIGN, MaxBytes, Count, Grow, Lock>::MAXBYTES)
			{
				p = (pointer)new char[bytes];
			}
			else
			{
				p = (pointer)small_pool_.allocate(bytes);
			}
			return p;
		}

		void deallocate(pointer p, size_type n = 1)
		{
			size_type bytes = n * sizeof(T);
			if (bytes > small_pool<TALIGN, MaxBytes, Count, Grow, Lock>::MAXBYTES)
			{
				delete []p;
			}
			else
			{
				small_pool_.deallocate(p);
			}
		}

		void construct(pointer p, const T & val) { new(p) T(val); }

		void destroy(pointer p) { p->~T(); }

		size_type max_size()const { return (size_type)-1 / sizeof(T); }

	private:
		typedef small_pool<TALIGN, MaxBytes, Count, Grow, Lock> small_pool_type;
		static small_pool_type small_pool_;
	};

	template<class T, size_t Align, size_t MaxBytes, size_t Count, class Grow, class Lock>
	typename pool_alloc<T, Align, MaxBytes, Count,Grow, Lock>::small_pool_type pool_alloc<T, Align, MaxBytes, Count,Grow, Lock>::small_pool_;

	template<class T,size_t Align, size_t MaxBytes, size_t Count, class Grow, class Lock>
	bool operator==(const pool_alloc<T, Align,MaxBytes, Count,Grow, Lock> & p1, const pool_alloc<T, Align, MaxBytes, Count,Grow, Lock> & p2)
	{
		return true;
	}

	template<class T, size_t Align, size_t MaxBytes, size_t Count, class Grow, class Lock>
	bool operator!=(const pool_alloc<T, Align,MaxBytes, Count,Grow, Lock> & p1, const pool_alloc<T, Align, MaxBytes, Count,Grow, Lock> & p2)
	{
		return false;
	}


}

#endif
