﻿#ifndef __UTILIB_NAMED_TUPLE_H__
#define __UTILIB_NAMED_TUPLE_H__

#include "null_type.h"
#include "type2type.h"

namespace utilib
{
	template<class NamedTuple, class Vistor>
	void foreach(NamedTuple & tuple, Vistor & vistor);

	template<
		const char * (Name)(),
		class T
	>
	struct named_field
	{
		static const char * name()
		{
			return Name();
		}

		typedef T value_type;
		T value;
	};

	template<
		class Field1 = null_type,
		class Field2 = null_type,
		class Field3 = null_type,
		class Field4 = null_type,
		class Field5 = null_type,
		class Field6 = null_type,
		class Field7 = null_type,
		class Field8 = null_type,
		class Field9 = null_type,
		class Field10 = null_type,
		class Field11 = null_type,
		class Field12 = null_type,
		class Field13 = null_type,
		class Field14 = null_type,
		class Field15 = null_type,
		class Field16 = null_type,
		class Field17 = null_type,
		class Field18 = null_type,
		class Field19 = null_type,
		class Field20 = null_type
	>
	class named_tuple;

	template<>
	class named_tuple<null_type, null_type, null_type, null_type, null_type, null_type, 
		null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, null_type, 
		null_type, null_type, null_type, null_type>
	{
	};

	template<class Vistor>
	void foreach(named_tuple<null_type,null_type,null_type,null_type, null_type,null_type,null_type,null_type,null_type,
		null_type,null_type, null_type, null_type, null_type, null_type, null_type, null_type,
		null_type, null_type, null_type> &, Vistor &)
	{
	}

	template<
		class Field1,
		class Field2,
		class Field3,
		class Field4,
		class Field5,
		class Field6,
		class Field7,
		class Field8,
		class Field9,
		class Field10,
		class Field11,
		class Field12,
		class Field13,
		class Field14,
		class Field15,
		class Field16,
		class Field17,
		class Field18,
		class Field19,
		class Field20
	>
	class named_tuple
		: public named_tuple<Field2, Field3, Field4, Field5, Field6, Field7, Field8, Field9, Field10,
			Field11,Field12, Field13, Field14, Field15, Field16, Field17, Field18, Field19, Field20>
	{
		typedef named_tuple<Field2, Field3, Field4, Field5, Field6, Field7, Field8, Field9, Field10,
			Field11,Field12, Field13, Field14, Field15, Field16, Field17, Field18, Field19, Field20> base;
	public:
		template<class Field>
		Field & get()
		{
			return get(type2type<Field>());
		}

		template<class Field>
		const Field & get()const
		{
			return get(type2type<Field>());
		}

		template<class Field>
		void set(const Field & fl)
		{
			base::set(fl);
		}

		void set(const Field1 & fl)
		{
			field_ = fl;
		}

		template<class NamedTuple, class Vistor>
		friend void foreach(NamedTuple & tuple, Vistor & vistor);

		template<class Vistor>
		friend void foreach(named_tuple<Field1,Field2, Field3, Field4,Field5,
			Field6,Field7,Field8,Field9,Field10, Field11,Field12, Field13, 
			Field14, Field15, Field16, Field17, Field18, Field19, Field20> & tuple, Vistor & vistor)
		{
			vistor(tuple.field_);
			foreach((base&)tuple, vistor);
		}

	protected:
		template<class Field>
		Field & get(const type2type<Field> ft)
		{
			return base::get(ft);
		}

		Field1 & get(const type2type<Field1> )
		{
			return field_;
		}

		template<class Field>
		const Field & get(const type2type<Field> ft)const
		{
			return base::get(ft);
		}

		const Field1 & get(const type2type<Field1> )const
		{
			return field_;
		}
		
	private:
		Field1 field_;
	};

	
} // namespace utilib

#endif
