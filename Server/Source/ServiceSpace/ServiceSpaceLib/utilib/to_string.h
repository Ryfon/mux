﻿#ifndef __UTILIB_TO_STRING_H__
#define __UTILIB_TO_STRING_H__

namespace utilib
{
	inline void to_string(int n, char * to)
	{
		sprintf(to, "%d", n);
	}

	inline void to_string(char c, char * to)
	{
		int n = (int)c;
		to_string(n, to);
	}

	inline void to_string(unsigned char c, char * to)
	{
		int n = (int)c;
		to_string(n, to);
	}

	inline void to_string(short s, char * to)
	{
		int n = (int)s;
		to_string(n, to);
	}

	inline void to_string(unsigned short s, char * to)
	{
		int n = (int)s;
		to_string(n, to);
	}

	inline void to_string(unsigned n, char * to)
	{
		sprintf(to, "%u", n);
	}

	inline void to_string(long n, char * to)
	{
		int _n = (int)n;
		to_string(_n, to);
	}

	inline void to_string(unsigned long n, char * to)
	{
		unsigned _n = (unsigned)n;
		to_string(_n, to);
	}

	inline void to_string(long long n, char * to)
	{
		sprintf(to, "%lld", n);
	}

	inline void to_string(unsigned long long n, char * to)
	{
		sprintf(to, "%lld", n);
	}

	inline void to_string(float f, char * to)
	{
		sprintf(to, "%f", f);
	}

	template<class _Elem, class _Traits, class _Ax>
	inline void to_string(const std::basic_string<_Elem, _Traits, _Ax> & str, char * to)
	{
		sprintf(to, "%s", str.c_str());
	}
}
#endif
