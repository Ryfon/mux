﻿#pragma once

#include "basic_text_istream.h"

namespace utilib
{
	class basic_text_ostream : public noncopyable
	{
	public:
		template<class T>
		class array
		{
		public:
			array(const T * p, const size_t size)
				:p_(p), size_(size)
			{ }

			~array()
			{ }

		private:
			const T * p_;
			const size_t size_;
			friend class basic_text_ostream;

		}; // class array

		basic_text_ostream(basic_stream_buffer & buf)
			: s_(buf)
		{ }

		~basic_text_ostream() 
		{ }

		basic_text_ostream & operator<< (const bool b)
		{
			char c = b ? '1' : '0';
			return this->operator <<(c);
		}

		basic_text_ostream & operator<< (const char c)
		{
			s_.copy(&c, sizeof(char));

			return *this;
		}

		basic_text_ostream & operator<< (const unsigned char c)
		{
			s_.copy(&c, sizeof(unsigned char));

			return *this;
		}

		basic_text_ostream & operator<< (const short n)
		{
			transfer("%d", (int)n);

			return *this;
		}

		basic_text_ostream & operator<< (const unsigned short n)
		{
			transfer("%u", (unsigned)n);

			return *this;
		}

		basic_text_ostream & operator<< (const int n)
		{
			transfer("%d", n);

			return *this;
		}

		basic_text_ostream & operator<< (const unsigned int n)
		{
			transfer("%u", n);

			return *this;
		}

		basic_text_ostream & operator<< (const long long l);

		basic_text_ostream & operator>> (const unsigned long long l);

		basic_text_ostream & operator<< (const float val)
		{
			transfer("%f", val);

			return *this;
		}		

		basic_text_ostream & operator<< (const double val);

		basic_text_ostream & operator<< (const long double val);

		// no definition
		basic_text_ostream & operator<< (const char * lpszStr);

		basic_text_ostream & operator<< (const wchar_t * lpszStr);

		template<class T, size_t N>
		basic_text_ostream & operator<< (const T (&v)[N]);

		template<class T>
		basic_text_ostream & operator<< (const T *);

		template<class T>
		basic_text_ostream & operator<< (const array<T> arr)
		{
			this->operator <<(arr.size_);
			for(size_t i = 0; i < arr.size_; ++i)
			{
				*this << arr.p_[i];
			}

			return *this;
		}

		template<class CharType, class Traits, class Allocator>
		basic_text_ostream & operator<< (const std::basic_string<CharType, Traits, Allocator> & str)
		{
			const size_t len = str.size();
			this->operator <<(len);
			if (sizeof(CharType) == sizeof(char))
				s_.copy(str.c_str(), len);
			else
				for(size_t i = 0; i < len; ++i)
					*this << str[i];

			return *this;
		}

		template<class Type, class Allocator>
		basic_text_ostream & operator<< (const std::vector<Type, Allocator> & vec)
		{
			typedef const std::vector<Type, Allocator> vector_type;

			const size_t len = vec.size();
			this->operator <<(len);
			
			for (typename vector_type::const_iterator it = vec.begin(); it != vec.end(); ++it)
			{
				*this << *it;
			}

			return *this;
		}
		
		template<class Type, class Allocator>
		basic_text_ostream & operator<< (const std::list<Type, Allocator> & lst)
		{
			typedef const std::list<Type, Allocator> list_type;

			const size_t len = lst.size();
			this->operator <<(len);

			for (typename list_type::const_iterator it = lst.begin(); it != lst.end(); ++it)
			{
				*this << *it;
			}

			return *this;
		}

		template<class Type, class Container>
		basic_text_ostream & operator<< (std::queue<Type, Container> qu)
		{
#pragma message("Performance Warning! copy queue.")

			typedef std::queue<Type, Container> queue_type;

			const size_t len = qu.size();
			this->operator <<(len);

			while (!qu.empty())
			{
				*this << qu.front();
				qu.pop();
			}

			return *this;
		}		
		
		template<class Type, class Allocator>
		basic_text_ostream & operator<< (const std::deque<Type, Allocator> & dq)
		{
			typedef std::deque<Type, Allocator> deque_type;

			const size_t len = dq.size();
			this->operator <<(len);

			for(typename deque_type::const_iterator it = dq.begin(); it != dq.end(); ++it)
			{
				*this << *it;
			}

			return *this;
		}

		template<class Key, class Traits, class Allocator>
		basic_text_ostream & operator<< (const std::set<Key, Traits, Allocator> & st)
		{
			typedef std::set<Key, Traits, Allocator> set_type;

			const size_t len = st.size();
			this->operator <<(len);

			for(typename set_type::const_iterator it = st.begin(); it != st.end(); ++it)
			{
				*this << *it;
			}

			return *this;
		}

		template<class Type1, class Type2>
		basic_text_ostream & operator<< (const std::pair<Type1, Type2> & par)
		{
			typedef std::pair<Type1, Type2> pair_type;	

			*this << par.first;
			*this << par.second;

			return *this;
		}
		
		template<class Key, class Type, class Traits, class Allocator>
		basic_text_ostream & operator<< (const std::map<Key, Type, Traits, Allocator> & mp)
		{
			typedef std::map<Key, Type, Traits, Allocator> map_type;

			const size_t len = mp.size();
			this->operator <<(len);

			for(typename map_type::const_iterator it = mp.begin(); it != mp.end(); ++it)
			{
				*this << it->first;
				*this << it->second;
			}

			return *this;
		}

		basic_stream_buffer & get_buffer()const
		{
			return s_;
		}

	private:
		template<class T>
		void transfer(const char * fmt, const T val)
		{
			char buf[256];
			int n = ::sprintf(buf, fmt, val);
			if (n == -1)
				throw std::runtime_error("error when calling sprintf");

			this->operator <<((char)n);
			s_.copy(buf, n);
		}

		basic_stream_buffer & s_;

	}; // class basic_text_ostream

} //