﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   10:09
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\null_type.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	null_type
	file ext:	h
	author:		
	
	purpose:	空类型，用于结束某些递推模板
*********************************************************************/


#ifndef __UTI_LIB_NULL_TYPE_HH__
#define __UTI_LIB_NULL_TYPE_HH__

namespace utilib
{
	struct null_type {};

	template<class T>
	struct is_null_type;

	template<class T>
	struct is_null_type
	{
		enum { Result = false };
	};

	template<>
	struct is_null_type<null_type>
	{
		enum { Result = true };
	};
}

#endif
