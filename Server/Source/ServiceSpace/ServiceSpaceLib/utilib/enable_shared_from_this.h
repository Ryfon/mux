﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   10:07
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\enable_shared_from_this.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	enable_shared_from_this
	file ext:	h
	author:		
	
	purpose:	可自管理对象的接口
*********************************************************************/


#ifndef __UTI_LIB_ENABLE_SHARE_FROM_THIS_H__
#define __UTI_LIB_ENABLE_SHARE_FROM_THIS_H__

#include "smart_ptr.h"

namespace utilib
{
	template<class ThisPtr>
	class enable_shared_from_this
	{
	public:
		enable_shared_from_this()
		{

		}

		~enable_shared_from_this()
		{

		}

		ThisPtr shared_from_this()
		{
			return weak_ptr_.make_smart_ptr();
		}

	protected:
		enable_shared_from_this(ThisPtr & sp)
			: weak_ptr_(sp)
		{

		}

		typename ThisPtr::weak_ptr_type weak_ptr_;
	};

}

#endif
