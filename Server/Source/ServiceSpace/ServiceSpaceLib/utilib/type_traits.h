﻿/********************************************************************
	created:	2008/06/16
	created:	16:6:2008   14:43
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\type_traits.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	type_traits
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_TYPE_TRAITS_H__
#define __SERVICE_SPACE_TYPE_TRAITS_H__

namespace utilib
{
	template<class T>
	struct type_traits
	{
		typedef T value_type;
		typedef T * pointer_type;
		typedef T & reference_type;
	};

	template<class T>
	struct type_traits<const T>
		: public type_traits<T>
	{
	};

	template<class T>
	struct type_traits<const T *>
		: public type_traits<T>
	{
	};

	template<class T>
	struct type_traits<T *>
		: public type_traits<T>
	{

	};

	template<class T>
	struct type_traits<T &>
		: public type_traits<T>
	{

	};

	template<class T>
	struct type_traits<const T &>
		: public type_traits<T>
	{

	};

	template<class T>
	struct type_traits<const T * *>
		: public type_traits<T>
	{

	};

	template<class T>
	struct type_traits<T * *>
		: public type_traits<T>
	{

	};


	template<class T, size_t N>
	struct type_traits<const T [N]>
		: public type_traits<T [N]>
	{

	};


}

#endif
