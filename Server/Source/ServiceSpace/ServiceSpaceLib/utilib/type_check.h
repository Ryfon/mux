﻿/********************************************************************
	created:	2008/06/16
	created:	16:6:2008   16:48
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\type_check.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	type_check
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __UTILIB_TYPE_CHECK_H__
#define __UTILIB_TYPE_CHECK_H__

namespace utilib
{
	template<class T1, class T2>
	struct type_check
	{
		enum { Result = false };
	};

	template<class T>
	struct type_check<T, T>
	{
		enum { Result = true };
	};
}
#endif
