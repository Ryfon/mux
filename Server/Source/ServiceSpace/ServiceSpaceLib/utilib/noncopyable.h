﻿/********************************************************************
	created:	2008/06/02
	created:	2:6:2008   16:54
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\noncopyable.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	noncopyable
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __UTILIB_NONCOPYABLE_H__
#define __UTILIB_NONCOPYABLE_H__

namespace utilib
{
	class noncopyable
	{
	protected:
		noncopyable() { }
		~noncopyable() { }

	private:
		noncopyable(const noncopyable &);
		const noncopyable & operator=(const noncopyable &);
	};

}

#endif
