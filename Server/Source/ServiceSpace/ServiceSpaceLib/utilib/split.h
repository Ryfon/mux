﻿/********************************************************************
	created:	2008/05/30
	created:	30:5:2008   13:10
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\split.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	split
	file ext:	h
	author:		
	
	purpose:	死烂的分割
*********************************************************************/

#ifndef __UTILIB_SPLIT_H__
#define __UTILIB_SPLIT_H__

#pragma once

#include <algorithm>
#include <string>

namespace utilib
{
	template<class _Vec, class _Iter, class _Sep>
	void split(_Vec & vec, _Iter beg, _Iter end, _Sep sep)
	{
		vec.push_back(typename _Vec::value_type());
		for(int i = 0; beg < end; ++beg)
		{
			if (*beg == sep)
			{
				++i;
				vec.push_back(typename _Vec::value_type());
			}
			else
			{
				vec[i] += *beg;
			}
		}
	}
}

#endif
