﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   10:10
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\stringN.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	stringN
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __UTILIB_STRINGN_H__
#define __UTILIB_STRINGN_H__

#include <string>
#include <stdexcept>

namespace utilib
{
	template<class Ch>
	size_t uti_strncpy(Ch * dst, size_t dst_max_size, const char * src, size_t src_max_size)
	{
		if (!dst || !src) return 0;

		if (!dst_max_size) return 0;

		--dst_max_size;
		size_t dst_max_size_norm = dst_max_size / (sizeof(int));
		size_t src_max_size_norm = src_max_size / (sizeof(int));

		size_t i = 0, j = 0;
		int * int_dst = (int *)dst;
		int * int_src = (int *)src;
		for(; i < dst_max_size_norm && i < src_max_size_norm; ++i, j += sizeof(int))
		{
			int_dst[i] = int_src[i];
		}

		for(; j < dst_max_size && j < src_max_size; ++j)
		{
			dst[j] = src[j];
		}

		dst[j] = 0;
		return j;
	}

	template<size_t N, class Ch = char>
	class stringN
	{
	public:
		typedef Ch value_type;

		stringN() : size_(0)
		{
			str_[size_] = 0;
		}

		stringN(const Ch * str) : size_(0)
		{
			size_ = uti_strncpy(str_, N, str, ::strlen(str));
		}

		stringN(const Ch * str, size_t size) : size_(0)
		{
			size_ = uti_strncpy(str_, N, str, size);
		}

		template<size_t N2>
		stringN(const Ch (*str)[N2])
		{
			size_ = uti_strncpy(str_, N, str, N2);
		}

		template<size_t N2>
		stringN(const stringN<N2, Ch> & str)
		{
			size_ = uti_strncpy(str_, N, str.c_str(), str.size());
		}

		stringN(const stringN & str) : size_(0)
		{
			size_ = uti_strncpy(str_, N, str.str_, str.size_);
		}

		template<size_t N2>
		const stringN & operator=(const stringN<N2, Ch> & str)
		{
			size_ = uti_strncpy(str_, N, str.c_str(), str.size());
			return *this;
		}

		const stringN & operator=(const stringN & str)
		{
			size_ = uti_strncpy(str_, N, str.str_, str.size_);
			return *this;
		}

		const stringN & operator=(const Ch * str)
		{
			size_ = uti_strncpy(str_, N, str, ::strlen(str));
			return *this;
		}

		template<size_t N2>
		const stringN & operator+=(const stringN<N2, Ch> & str)
		{
			size_ += uti_strncpy(&str_[size_], N - size_, str.c_str(), str.size());
			return *this;
		}

		const stringN & operator+=(const Ch * str)
		{
			size_ += uti_strncpy(&str_[size_], N - size_, str, ::strlen(str));
			return *this;
		}

		const stringN & operator+=(Ch ch)
		{
			Ch tmp[2];
			tmp[0] = ch;
			tmp[1] = 0;
			size_ += uti_strncpy(&str_[size_], N - size_, tmp, 1);
			return *this;
		}

		stringN & operator<<(Ch ch)
		{
			operator+=(ch);
			return *this;
		}

		stringN & operator<<(const Ch * str)
		{
			operator+=(str);
			return *this;
		}

		template<size_t N2>
		stringN & operator<<(const stringN<N2, Ch> & str)
		{
			operator+=(str);
			return *this;
		}

		stringN & operator<<(const stringN & str)
		{
			operator+=(str);
			return *this;
		}


		Ch & operator[](size_t index)
		{
			if (index >= N)
				throw std::out_of_range("");
			return str_[index];
		}

		const Ch & operator[](size_t index)const
		{
			if (index >= N)
				throw std::out_of_range("");
			return str_[index];
		}

		const Ch * c_str()const
		{
			return str_;
		}

		size_t find(Ch ch)const
		{
			for(size_t i = 0; i < size_; ++i)
			{
				if (str_[i] == ch)
					return i;
			}
			return (size_t)-1;
		}

		void clean()
		{
			size_ = 0;
			str_[0] = 0;
		}

		size_t size()const
		{
			return size_;
		}

		bool empty()const
		{
			return size_ == 0;
		}

		template<size_t N2>
		friend bool operator<(const stringN & str1, const stringN<N2, Ch> & str2)
		{
			return (strcmp(str1.str_, str2.str_) < 0);
		}

		template<size_t N2>
		friend bool operator>(const stringN & str1, const stringN<N2, Ch> & str2)
		{
			return (strcmp(str1.str_, str2.str_) > 0);
		}

		template<size_t N2>
		friend bool operator==(const stringN & str1, const stringN<N2, Ch> & str2)
		{
			return (strcmp(str1.str_, str2.c_str()) == 0);
		}

		friend bool operator==(const stringN & str1, const Ch * str2)
		{
			return (strcmp(str1.str_, str2) == 0);
		}


	private:
		Ch str_[N + 1];
		size_t size_;
	};
}

#endif
