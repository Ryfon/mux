﻿/********************************************************************
	created:	2008/06/13
	created:	13:6:2008   10:52
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\int2type.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	int2type
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __UTI_LIB_INT_TO_TYPE_H__
#define __UTI_LIB_INT_TO_TYPE_H__

namespace utilib
{
	template<int N>
	struct int2type
	{
		enum { Value = N };
	};
}

#endif
