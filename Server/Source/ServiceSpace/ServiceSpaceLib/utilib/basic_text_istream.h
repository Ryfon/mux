﻿#pragma once

#include "basic_binary_istream.h"

namespace utilib
{
	class basic_text_istream : public noncopyable
	{
	public:
		template<class T>
		class array
		{
		public:
			array(T * p, const size_t maxSize)
				:p_(p), max_size_(maxSize), size_(0)
			{ }

			const size_t size()const
			{
				return size_;
			}

		private:
			T * p_;
			const size_t max_size_;
			size_t size_;

			friend class basic_text_istream;

		}; // class array

		basic_text_istream(basic_stream_buffer & buf)
			: s_(buf)
		{ }

		~basic_text_istream()
		{ }

		basic_text_istream & operator>> (bool & b)
		{
			char c = 0;
			this->operator >>(c);
			b = c == '0' ? false : true;

			return *this;
		}

		basic_text_istream & operator>> (char & c)
		{
			s_.read(&c, sizeof(char));

			return *this;
		}

		basic_text_istream & operator>> (unsigned char & c)
		{
			s_.read(&c, sizeof(unsigned char));

			return *this;
		}

		basic_text_istream & operator>> (short & n)
		{
			int d = 0;
			transfer("%u", d);
			n = d;
			return *this;
		}

		basic_text_istream & operator>> (unsigned short & n)
		{
			unsigned d = 0;
			transfer("%d", d);
			n = d;
			return *this;
		}

		basic_text_istream & operator>> (int & n)
		{
			transfer("%d", n);

			return *this;
		}

		basic_text_istream & operator>> (unsigned int & n)
		{
			transfer("%u", n);

			return *this;
		}

		basic_text_istream & operator>> (long long & l);

		basic_text_istream & operator>> (unsigned long long & l);

		basic_text_istream & operator>> (float & val)
		{
			transfer("%f", val);

			return *this;
		}

		basic_text_istream & operator>> (double & val);

		basic_text_istream & operator>> (long double & val);

		// no definition
		basic_text_istream & operator>> (char *);

		basic_text_istream & operator>> (wchar_t *);

		template<class T, size_t N>
		basic_text_istream & operator>> (T (&v)[N]);

		template<class T>
		basic_text_istream & operator>> (T *);

		template<class T>
		basic_text_istream & operator>> (array<T> & arr)
		{
			size_t len = 0;
			this->operator >>(len);
			for(size_t i = 0; i < len && i < arr.max_size_; ++i)
			{
				*this >> arr.p_[i];
				++arr.size_;
			}

			return *this;
		}

		template<class CharType, class Traits, class Allocator>
		basic_text_istream & operator>> (std::basic_string<CharType, Traits, Allocator> & str)
		{
			str.clear();

			size_t len = 0;
			this->operator >>(len);

			for(size_t i = 0; i < len; ++i)
			{
				CharType ch;
				*this >> ch;
				str.push_back(ch);
			}

			return *this;
		}

		template<class Type, class Allocator>
		basic_text_istream & operator>> (std::vector<Type, Allocator> & vec)
		{
			typedef std::vector<Type, Allocator> vector_type;

			vec.clear();

			size_t len = 0;
			this->operator >>(len);

			for (size_t i = 0; i < len; ++i)
			{
				typename vector_type::value_type val;
				*this >> val;
				vec.push_back(val);
			}

			return *this;
		}

		template<class Type, class Allocator>
		basic_text_istream & operator>> (std::list<Type, Allocator> & lst)
		{
			typedef std::list<Type, Allocator> list_type;

			lst.clear();

			size_t len = 0;
			this->operator >>(len);

			for (size_t i = 0; i < len; ++i)
			{
				typename list_type::value_type val;
				*this >> val;
				lst.push_back(val);
			}

			return *this;
		}

		template<class Type, class Container>
		basic_text_istream & operator>> (std::queue<Type, Container> & qu)
		{
			typedef std::queue<Type, Container> queue_type;

			size_t len = 0;
			this->operator >>(len);

			for(size_t i = 0; i < len; ++i)
			{
				typename queue_type::value_type val;
				*this >> val;
				qu.push(val);
			}

			return *this;
		}

		template<class Type, class Allocator>
		basic_text_istream & operator>> (std::deque<Type, Allocator> & dq)
		{
			typedef std::deque<Type, Allocator> deque_type;

			dq.clear();

			size_t len = 0;
			this->operator >>(len);

			for(size_t i = 0; i < len; ++i)
			{
				typename deque_type::value_type val;
				*this >> val;
				dq.push_back(val);
			}

			return *this;
		}

		template<class Key, class Traits, class Allocator>
		basic_text_istream & operator>> (const std::set<Key, Traits, Allocator> & st)
		{
			typedef std::set<Key, Traits, Allocator> set_type;

			size_t len = 0;
			this->operator >>(len);

			for(size_t i = 0; i < len; ++i)
			{
				typename set_type::value_type val;
				*this >> val;
				st.insert(val);
			}

			return *this;
		}

		template<class Type1, class Type2>
		basic_text_istream & operator>> (std::pair<Type1, Type2> & par)
		{
			typedef std::pair<Type1, Type2> pair_type;	

			*this >> par.first;
			*this >> par.second;

			return *this;
		}

		template<class Key, class Type, class Traits, class Allocator>
		basic_text_istream & operator>> (std::map<Key, Type, Traits, Allocator> & mp)
		{
			typedef std::map<Key, Type, Traits, Allocator> map_type;

			size_t len = 0;
			this->operator >>(len);

			for(size_t i = 0; i < len; ++i)
			{
				std::pair<Key, Type> val;
				*this >> val.first;
				*this >> val.second;
				mp[val.first] = val.second;
			}

			return *this;
		}

		basic_stream_buffer & get_buffer()const
		{
			return s_;
		}

	private:
		template<class T>
		void transfer(char * fmt, T & t)
		{
			char w;
			this->operator >>(w);

			char buf[256];
			s_.read(buf, w);
			buf[w] = 0;
			::sscanf(buf, fmt, &t);
		}

		basic_stream_buffer & s_;

	}; // class basic_text_istream

} // namespace matrixcoding