﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   10:08
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\functor.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	functor
	file ext:	h
	author:		
	
	purpose:	将类的成员函数包装成函数对象的形式
*********************************************************************/


#ifndef __UTILIB_FUNCTOR_H__
#define __UTILIB_FUNCTOR_H__

#include "null_type.h"

namespace utilib
{
	template<class MemFun>
	struct functor;

	template<class Obj>
	struct functor<void (Obj::*)(void)>
	{
		typedef Obj obj_type;
		typedef void (Obj::*mem_fun_type)(void);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		void operator()(void)
		{
			if (obj_ && f_)
				(obj_->*f_)();
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

	template<class Obj, class Arg>
	struct functor<void (Obj::*)(Arg)>
	{
		typedef Obj obj_type;
		typedef void (Obj::*mem_fun_type)(Arg);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		void operator()(Arg arg)
		{
			if (obj_ && f_)
				(obj_->*f_)(arg);
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

	template<class Obj,
		     class Ret
	>
	struct functor<Ret (Obj::*)()>
	{
		typedef Obj obj_type;
		typedef Ret (Obj::*mem_fun_type)(void);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		Ret operator()(void)
		{
			if (obj_ && f_)
				return ((obj_->*f_)());
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

	template<class Obj,
			 class Ret,
			 class Arg
	>
	struct functor<Ret (Obj::*)(Arg)>
	{
		typedef Obj obj_type;
		typedef Ret (Obj::*mem_fun_type)(Arg);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		Ret operator()(Arg arg)
		{
			if (obj_ && f_)
				return ((obj_->*f_)(arg));
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

	template<class Obj,
			 class Ret,
			 class Arg1,
			 class Arg2
	>
	struct functor<Ret (Obj::*)(Arg1,Arg2)>
	{
		typedef Obj obj_type;
		typedef Ret (Obj::*mem_fun_type)(Arg1,Arg2);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		Ret operator()(Arg1 arg1, Arg2 arg2)
		{
			if (obj_ && f_)
				return ((obj_->*f_)(arg1, arg2));
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

	template<class Obj,
			 class Ret,
			 class Arg1,
			 class Arg2,
			 class Arg3
	>
	struct functor<Ret (Obj::*)(Arg1,Arg2,Arg3)>
	{
		typedef Obj obj_type;
		typedef Ret (Obj::*mem_fun_type)(Arg1,Arg2,Arg3);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		Ret operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3)
		{
			if (obj_ && f_)
				return ((obj_->*f_)(arg1, arg2, arg3));
			return Ret();
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

	template<class Obj,
			 class Ret,
			 class Arg1,
			 class Arg2,
			 class Arg3,
			 class Arg4
	>
	struct functor<Ret (Obj::*)(Arg1,Arg2,Arg3,Arg4)>
	{
		typedef Obj obj_type;
		typedef Ret (Obj::*mem_fun_type)(Arg1,Arg2,Arg3,Arg4);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		Ret operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3, Arg4 arg4)
		{
			if (obj_ && f_)
				return ((obj_->*f_)(arg1, arg2, arg3, arg4));
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

	template<class Obj,
			 class Ret,
			 class Arg1,
			 class Arg2,
			 class Arg3,
			 class Arg4,
			 class Arg5
	>
	struct functor<Ret (Obj::*)(Arg1,Arg2,Arg3,Arg4, Arg5)>
	{
		typedef Obj obj_type;
		typedef Ret (Obj::*mem_fun_type)(Arg1,Arg2,Arg3,Arg4, Arg5);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		Ret operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3, Arg4 arg4, Arg5 arg5)
		{
			if (obj_ && f_)
				return ((obj_->*f_)(arg1, arg2, arg3, arg4, arg5));
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

	template<class Obj,
			 class Ret,
			 class Arg1,
			 class Arg2,
			 class Arg3,
			 class Arg4,
			 class Arg5,
			 class Arg6
	>
	struct functor<Ret (Obj::*)(Arg1,Arg2,Arg3,Arg4, Arg5, Arg6)>
	{
		typedef Obj obj_type;
		typedef Ret (Obj::*mem_fun_type)(Arg1,Arg2,Arg3,Arg4, Arg5, Arg6);

		functor(obj_type * obj = 0, mem_fun_type f = 0)
			:obj_(obj), f_(f)
		{

		}

		Ret operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3, Arg4 arg4, Arg5 arg5, Arg6 arg6)
		{
			if (obj_ && f_)
				return ((obj_->*f_)(arg1, arg2, arg3, arg4, arg5, arg6));
		}

	private:
		obj_type * obj_;
		mem_fun_type f_;
	};

}

#endif

