﻿/********************************************************************
	created:	2008/06/19
	created:	19:6:2008   14:43
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\alloc.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	alloc
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __UTILIB_ALLOC_H__
#define __UTILIB_ALLOC_H__

class _alloc
{
public:
	void * allocate(size_t size);
	void deallocate(void * p);
};

#endif
