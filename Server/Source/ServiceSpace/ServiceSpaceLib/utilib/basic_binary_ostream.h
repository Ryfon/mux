﻿#pragma once

#include "basic_binary_istream.h"

namespace utilib
{
	class basic_binary_ostream : public noncopyable
	{
	public:
		template<class T>
		class array
		{
		public:
			array(const T * p, const size_t size)
				:p_(p), size_(size)
			{ }

			~array()
			{ }

		private:
			const T * p_;
			const size_t size_;
			friend class basic_binary_ostream;

		}; // class array

		basic_binary_ostream(basic_stream_buffer & buf)
			: s_(buf)
		{ }

		~basic_binary_ostream() 
		{ }

		basic_binary_ostream & operator<< (const bool b)
		{
			char c = b ? '1' : '0';
			return this->operator <<(c);
		}

		basic_binary_ostream & operator<< (const char c)
		{
			s_.copy(&c, sizeof(char));

			return *this;
		}

		basic_binary_ostream & operator<< (const unsigned char c)
		{
			s_.copy(&c, sizeof(unsigned char));

			return *this;
		}

		basic_binary_ostream & operator<< (const short n)
		{
			if (!is_host_byte_order_same_as_network())
				s_.copy(&n, sizeof(short));
			else
				s_.rcopy(&n, sizeof(short));

			return *this;
		}

		basic_binary_ostream & operator<< (const unsigned short n)
		{
			if (!is_host_byte_order_same_as_network())
				s_.copy(&n, sizeof(unsigned short));
			else
				s_.rcopy(&n, sizeof(unsigned short));

			return *this;
		}

		basic_binary_ostream & operator<< (const int n)
		{
			if (!is_host_byte_order_same_as_network())
				s_.copy(&n, sizeof(int));
			else
				s_.rcopy(&n, sizeof(int));

			return *this;
		}

		basic_binary_ostream & operator<< (const unsigned int n)
		{
			if (!is_host_byte_order_same_as_network())
				s_.copy(&n, sizeof(unsigned int));
			else
				s_.rcopy(&n, sizeof(unsigned int));

			return *this;
		}

		basic_binary_ostream & operator<< (const long long l)
		{
			if (!is_host_byte_order_same_as_network())
				s_.copy(&l, sizeof(long long));
			else
				s_.rcopy(&l, sizeof(long long));

			return *this;
		}

		basic_binary_ostream & operator<< (const unsigned long long l)
		{
			if (!is_host_byte_order_same_as_network())
				s_.copy(&l, sizeof(unsigned long long));
			else
				s_.copy(&l, sizeof(unsigned long long));

			return *this;
		}

		basic_binary_ostream & operator<< (const float val)
		{
			s_.copy(&val, sizeof(float));

			return *this;
		}		

		basic_binary_ostream & operator<< (const double val)
		{
			s_.copy(&val, sizeof(double));

			return *this;
		}

		basic_binary_ostream & operator<< (const long double val)
		{
			s_.copy(&val, sizeof(long double));

			return *this;
		}	

		// no definition
		basic_binary_ostream & operator<< (const char * lpszStr);

		basic_binary_ostream & operator<< (const wchar_t * lpszStr);

		template<class T, size_t N>
		basic_binary_ostream & operator<< (const T (&v)[N]);

		template<class T>
		basic_binary_ostream & operator<< (const T *);

		template<class T>
		basic_binary_ostream & operator<< (const array<T> arr)
		{
			this->operator <<(arr.size_);
			for(size_t i = 0; i < arr.size_; ++i)
			{
				*this << arr.p_[i];
			}

			return *this;
		}

		template<class CharType, class Traits, class Allocator>
		basic_binary_ostream & operator<< (const std::basic_string<CharType, Traits, Allocator> & str)
		{
			const size_t len = str.size();
			this->operator <<(len);
			if (sizeof(CharType) == sizeof(char))
				s_.copy(str.c_str(), len);
			else
				for(size_t i = 0; i < len; ++i)
					*this << str[i];

			return *this;
		}

		template<class Type, class Allocator>
		basic_binary_ostream & operator<< (const std::vector<Type, Allocator> & vec)
		{
			typedef const std::vector<Type, Allocator> vector_type;

			const size_t len = vec.size();
			this->operator <<(len);
			
			for (typename vector_type::const_iterator it = vec.begin(); it != vec.end(); ++it)
			{
				*this << *it;
			}

			return *this;
		}
		
		template<class Type, class Allocator>
		basic_binary_ostream & operator<< (const std::list<Type, Allocator> & lst)
		{
			typedef const std::list<Type, Allocator> list_type;

			const size_t len = lst.size();
			this->operator <<(len);

			for (typename list_type::const_iterator it = lst.begin(); it != lst.end(); ++it)
			{
				*this << *it;
			}

			return *this;
		}

		template<class Type, class Container>
		basic_binary_ostream & operator<< (std::queue<Type, Container> qu)
		{
#pragma message("Performance Warning! copy queue.")

			typedef std::queue<Type, Container> queue_type;

			const size_t len = qu.size();
			this->operator <<(len);

			while (!qu.empty())
			{
				*this << qu.front();
				qu.pop();
			}

			return *this;
		}		
		
		template<class Type, class Allocator>
		basic_binary_ostream & operator<< (const std::deque<Type, Allocator> & dq)
		{
			typedef std::deque<Type, Allocator> deque_type;

			const size_t len = dq.size();
			this->operator <<(len);

			for(typename deque_type::const_iterator it = dq.begin(); it != dq.end(); ++it)
			{
				*this << *it;
			}

			return *this;
		}

		template<class Key, class Traits, class Allocator>
		basic_binary_ostream & operator<< (const std::set<Key, Traits, Allocator> & st)
		{
			typedef std::set<Key, Traits, Allocator> set_type;

			const size_t len = st.size();
			this->operator <<(len);

			for(typename set_type::const_iterator it = st.begin(); it != st.end(); ++it)
			{
				*this << *it;
			}

			return *this;
		}

		template<class Type1, class Type2>
		basic_binary_ostream & operator<< (const std::pair<Type1, Type2> & par)
		{
			typedef std::pair<Type1, Type2> pair_type;	

			*this << par.first;
			*this << par.second;

			return *this;
		}
		
		template<class Key, class Type, class Traits, class Allocator>
		basic_binary_ostream & operator<< (const std::map<Key, Type, Traits, Allocator> & mp)
		{
			typedef std::map<Key, Type, Traits, Allocator> map_type;

			const size_t len = mp.size();
			this->operator <<(len);

			for(typename map_type::const_iterator it = mp.begin(); it != mp.end(); ++it)
			{
				*this << it->first;
				*this << it->second;
			}

			return *this;
		}

	private:
		basic_stream_buffer & s_;

	}; // class basic_binary_ostream

	class memory_ostream_adapter : public noncopyable
	{
	public:
		memory_ostream_adapter(char * p, const size_t len)
			: p_(p), len_(len), wr_size_(0)
		{
			if (p_ == 0)
				throw std::runtime_error("invalid memory pointer detected");
		}

		~memory_ostream_adapter()
		{ }

		const size_t capacity()const
		{
			return len_ - wr_size_;
		}

		const size_t size()const
		{
			return len_ - wr_size_;
		}

		const char * get()const
		{
			return p_;
		}

		void copy(const void * p, const size_t n)
		{
			if (!p)
				throw std::invalid_argument("invalid memory pointer detected");

			if (capacity() < n)
				throw std::overflow_error("memory size lack detected");

			::memcpy(p_ + wr_size_, p, n);
			wr_size_ += n;
		}

		void rcopy(const void * p, const size_t n)
		{
			// reverse copy
			if (!p)
				throw std::invalid_argument("invalid memory pointer detected");

			if (capacity() < n)
				throw std::overflow_error("memory size lack detected");

			std::reverse_copy((char *)p, (char *)p + n, p_ + wr_size_);
			wr_size_ += n;
		}

	protected:
		char * p_;
		const size_t len_;
		size_t wr_size_;

	}; // class memory_ostream_adapter

	template<class Allocator>
	class vector_ostream_adapter : public noncopyable
	{ 
	public:
		typedef std::vector<char, typename Allocator::template rebind<char>::other> vector_type;

		vector_ostream_adapter(vector_type & vec)
			: vec_(vec), wr_size_(0)
		{ }

		~vector_ostream_adapter()
		{ }

		const size_t capacity()const
		{
			return vec_.capacity();
		}

		const size_t size()const
		{
			return wr_size_;
		}

		const char * get()const
		{
			if (wr_size_ == 0)
				throw std::runtime_error("invalid vector memory detected");

			return &vec_[0];
		}

		void copy(const void * p, const size_t n)
		{
			if (!p)
				throw std::invalid_argument("invalid memory pointer detected");

			vec_.insert(vec_.end(), (char *)p, (char *)p + n);
			wr_size_ += n;
		}

		void rcopy(const void * p, const size_t n)
		{
			if (!p)
				throw std::invalid_argument("invalid memory pointer detected");

			for(size_t i = 0; i < n; ++i)
			{
				vec_.push_back(p[n - i - 1]);
				wr_size_ += 1;
			}
		}

	protected:
		vector_type & vec_;
		size_t wr_size_;

	}; // class vector_ostream_adapter

} //