﻿/********************************************************************
	created:	2008/06/16
	created:	16:6:2008   14:58
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\assign.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	assign
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __UTILIB_ASSIGN_H__
#define __UTILIB_ASSIGN_H__

#include "type_traits.h"
#include <algorithm>

namespace utilib
{

	namespace __internal
	{
		
	} //namespace __internal

	template<class T1, class T2>
	inline void assign(T1 & dst, const T2 & src)
	{
		dst = src;
	}

	template<class T1, class T2>
	inline void assign(T1 (&dst)[1], const T2 (&src)[1])
	{
		dst[0] = src[0];
	}

	template<class T1, class T2, size_t N>
	inline void assign(T1 (&dst)[N] , const T2 (&src)[N])
	{
		for(size_t i = 0; i < N; ++i)
			dst[i] = src[i];
	}


}

#endif
