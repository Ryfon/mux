﻿ /********************************************************************
	created:	2008/06/12
	created:	12:6:2008   14:37
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\vectorN.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	vectorN
	file ext:	h
	author:		
	
	purpose:	定长数组
*********************************************************************/


#ifndef __UTILIB_VECTORN_H__
#define __UTILIB_VECTORN_H__

#include <stdexcept>

namespace utilib
{
	template<class T, size_t N>
	class vectorN
	{
	public:
		enum { MAXSIZE = N };
		typedef T value_type;
		typedef size_t size_type;

		vectorN()
			:size_(0)
		{
		}

		value_type & operator[](size_type index)
		{
			if (index < size_)
				return v_[index];
			throw std::out_of_range("vectorN");
		}

		const value_type & operator[](size_type index)const
		{
			if (index < size_)
				return v_[index];
			throw std::out_of_range("vectorN");
		}

		void push_back(const value_type & value)
		{
			if (size_ < MAXSIZE)
			{
				this->operator [](size_++) = value;
				return ;
			}
			throw std::overflow_error("vectorN");
		}
		
		size_type size()const 
		{
			return  size_;
		}

		size_type capacity()const
		{
			return MAXSIZE - size_ - 1;
		}

		size_type resize(size_type _size)
		{
			if (_size < MAXSIZE)
				size_ = _size;
			return size_;
		}

		size_type max_size()const
		{
			return MAXSIZE;
		}

	private:
		value_type v_[N];
		size_type size_;

	}; // class vectorN

} // namespace utilib

#endif
