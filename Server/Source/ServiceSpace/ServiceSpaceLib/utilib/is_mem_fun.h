﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   10:09
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\is_mem_fun.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	is_mem_fun
	file ext:	h
	author:		
	
	purpose:	函数/成员函数类型的判断
*********************************************************************/


#ifndef __UTI_LIB_IS_MEM_FUN_H__
#define __UTI_LIB_IS_MEM_FUN_H__

namespace utilib
{
	template<class Handler>
	struct is_mem_func
	{
		enum { result = false };
	};

	template<class ClassType, 
	class RetType
	>
	struct is_mem_func<RetType (ClassType::*)()>
	{
		enum { result = true };

	};

	template<class ClassType, 
	class RetType,
	class Arg1
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1)>
	{
		enum { result = true };

	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2)>
	{
		enum { result = true };

	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2,
	class Arg3
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2, Arg3)>
	{
		enum { result = true };

	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2,
	class Arg3,
	class Arg4
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2, Arg3, Arg4)>
	{
		enum { result = true };

	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2,
	class Arg3,
	class Arg4,
	class Arg5
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2, Arg3, Arg4, Arg5)>
	{
		enum { result = true };

	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2,
	class Arg3,
	class Arg4,
	class Arg5,
	class Arg6
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2, Arg3, Arg4, Arg5, Arg6)>
	{
		enum { result = true };

	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2,
	class Arg3,
	class Arg4,
	class Arg5,
	class Arg6,
	class Arg7
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)>
	{
		enum { result = true };

	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2,
	class Arg3,
	class Arg4,
	class Arg5,
	class Arg6,
	class Arg7,
	class Arg8
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)>
	{
		enum { result = true };
	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2,
	class Arg3,
	class Arg4,
	class Arg5,
	class Arg6,
	class Arg7,
	class Arg8,
	class Arg9
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9)>
	{
		enum { result = true };
	};

	template<class ClassType, 
	class RetType,
	class Arg1,
	class Arg2,
	class Arg3,
	class Arg4,
	class Arg5,
	class Arg6,
	class Arg7,
	class Arg8,
	class Arg9,
	class Arg10
	>
	struct is_mem_func<RetType (ClassType::*)(Arg1,Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9, Arg10)>
	{
		enum { result = true };
	};
}

#endif
