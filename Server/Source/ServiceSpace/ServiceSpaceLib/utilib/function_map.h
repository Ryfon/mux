﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   10:08
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\function_map.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	function_map
	file ext:	h
	author:		
	
	purpose:	提供普通函数/成员函数基于索引策略的访问机制
*********************************************************************/


#ifndef __UTILIB_FUN_MAP_H__
#define __UTILIB_FUN_MAP_H__

#include <map>

#include "functor.h"
#include "is_mem_fun.h"

namespace utilib
{
	namespace _internal
	{

		template<class Key,
				 class Fun,
				 class Less = std::less<Key>,
				 class Allocator = std::allocator<std::pair<const Key, Fun*> >,
				 bool IsMemFun = false
		>
		class function_map_base;

		template<class Key,
				 class Fun,
				 class Less,
				 class Allocator
		>
		class function_map_base<Key, Fun, Less, Allocator, false>
			: public std::map<Key, Fun ,Less, Allocator> 
		{
			typedef std::map<Key, Fun, Less, Allocator>  base;

		public:
			typedef Fun * function_type;
		};

		template<class Key,
				 class Fun,
				 class Less,
				 class Allocator
		>
		class function_map_base<Key, Fun, Less, Allocator, true>
			: public std::map<Key, functor<Fun>, Less, Allocator>
		{
			typedef std::map<Key, functor<Fun>, Less, Allocator> base;

		public:
			typedef functor<Fun> function_type;

		};


	} //  namespace _internal

	template<class Key,
			 class Fun,
			 class Less = std::less<Key>,
			 class Allocator = std::allocator<std::pair<const Key, Fun> >
	>
	class function_map
		: public _internal::function_map_base<Key, Fun, Less, Allocator, is_mem_func<Fun>::result>
	{

	}; // class function_map
}

#endif
