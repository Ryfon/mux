﻿/********************************************************************
	created:	2008/07/16
	created:	16:7:2008   13:06
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\type2string.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	type2string
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __UTILIB_TYPE_2_STRING_H__
#define __UTILIB_TYPE_2_STRING_H__

#include <strstream>
#include "stringN.h"

namespace utilib
{
	template<size_t N>
	std::strstream & operator>>(std::strstream & s, stringN<N> & str)
	{
		str << stringN<N>(s.str(), s.pcount());
		return s;
	}

	template<class T, class String>
	void type2string(const T & t, String & str)
	{
		std::strstream s;
		s << t;
		s >> str;
	}

	template<class String>
	void type2string(char ch, String & str)
	{
		str << ch;
	}

	template<class String>
	void type2string(unsigned char ch, String & str)
	{
		str << (char)ch;
	}

	template<class String>
	void type2string(int n, String & str)
	{
		char buf[32];
		sprintf(buf, "%d", n);
		str << buf;
	}

	template<class String>
	void type2string(unsigned n, String & str)
	{
		char buf[32];
		sprintf(buf, "%u", n);
		str << buf;
	}

	template<class String>
	void type2string(short n, String & str)
	{
		type2string((int)n, str);
	}

	template<class String>
	void type2string(unsigned short n, String & str)
	{
		type2string((unsigned)n, str);
	}



	template<size_t N, class String>
	void type2string(const stringN<N> & src, String & dst)
	{
		dst << src;
	}

	
}

#endif
