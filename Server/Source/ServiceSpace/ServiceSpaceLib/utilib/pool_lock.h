﻿/********************************************************************
	created:	2008/06/23
	created:	23:6:2008   16:54
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib\pool_lock.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\utilib
	file base:	pool_lock
	file ext:	h
	author:		
	
	purpose:	用于内存池的锁，尽可能的简单，所以无防错机制。
*********************************************************************/


#ifndef __UTILIB_POOL_LOCK_H__
#define __UTILIB_POOL_LOCK_H__

#ifndef WIN32
#include <pthread.h>
#include <sys/time.h>
#endif

namespace utilib
{
	class null_lock
	{
	public:
		void lock() {}

		void unlock() {}

	}; // class null_lock

	template<size_t = 0>
	struct cpu_size
	{
		static int get()
		{
#ifdef WIN32
			 SYSTEM_INFO si;
			 GetSystemInfo(&si);
			 return si.dwNumberOfProcessors;
#else
			return sysconf(_SC_NPROCESSORS_ONLN);
#endif
		}

		static int size;
	};

	template<size_t _NOTUSE>
	int cpu_size<_NOTUSE>::size = cpu_size<_NOTUSE>::get();

	template<class lock>
	struct pool_guard
	{
	public:
		pool_guard(lock & _lock)
			: lock_(_lock)
		{
			lock_.acquire();
		}

		~pool_guard()
		{
			lock_.release();
		}

	private:
		lock & lock_;
	};

#ifndef WIN32
	template<>
	struct pool_guard<pthread_mutex_t>
	{
	public:
		pool_guard(pthread_mutex_t & mut)
			: mut_(mut)
		{
			pthread_mutex_lock(&mut_);
		}

		~pool_guard()
		{
			pthread_mutex_unlock(&mut_);
		}

	private:
		pthread_mutex_t & mut_;
	};
#endif

	template<>
	struct pool_guard<null_lock>
	{
	public:
		pool_guard(null_lock &)
		{

		}
	};

	class pool_lock
	{
	public:
		pool_lock()
		{
#ifdef WIN32
			flag_ = 0;
#else
			pthread_spin_init(&lock_, PTHREAD_PROCESS_PRIVATE);
#endif
		}

		~pool_lock()
		{
#ifndef WIN32
			pthread_spin_destroy(&lock_);
#endif
		}

		void acquire()
		{
#ifdef WIN32
			bool mult_cpu = cpu_size<>::size > 1;
			while(true)
			{
				unsigned count = mult_cpu ? 4000 : 1;
				while(count)
				{
					if (0 == InterlockedCompareExchange(&flag_, 1, 0))
						return;
					--count;
				}

				Sleep(0);
			}
#else
			pthread_spin_lock(&lock_);
#endif
		}

		void release()
		{
#ifdef WIN32
			InterlockedCompareExchange(&flag_, 0, 1);
#else
			pthread_spin_unlock(&lock_);
#endif
		}

	private:
#ifdef WIN32
		long flag_;	
#else
		pthread_spinlock_t lock_;
#endif

	};

	
}

#endif
