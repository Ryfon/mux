﻿#define ACE_BUILD_SVC_DLL

#include "StreamBuffer.h"
#include "MessageBlock.h"
#include "utilib/stringN.h"

__SERVICE_SPACE_BEGIN_NS__

int get_package_length(ACE_Message_Block * blk, size_t len_offset, size_t len_bytes, bool encode, size_t & pkgLen)
{
	size_t len = blk->length();
	if (len_bytes == 1 && len > 1 + len_offset)
	{
		pkgLen = *((unsigned char*)(blk->rd_ptr() + len_offset));
	}
	else if (len_bytes == 2 && len > 2 + len_offset)
	{
		unsigned short tmp = *((unsigned short*)(blk->rd_ptr() + len_offset));
		if (encode)
			pkgLen = (size_t)ntohs(tmp);
		else
			pkgLen = (size_t)tmp;
	}
	else if (len_bytes == 4 && len > 4 + len_offset)
	{
		unsigned tmp = *((unsigned *)(blk->rd_ptr() + len_offset));
		if (encode)
			pkgLen = (size_t)ntohl(tmp);
		else
			pkgLen = (size_t)tmp;
	}
	else
		return 1;
	return 0;
}

extern "C" ACE_Svc_Export int  StreamBuffer(ACE_Message_Block * message, size_t len_offset, size_t len_bytes, bool encode)
{
	if (len_offset > 1024)
	{
		SS_ERROR(ACE_TEXT("StreamBuffer | error offset param\n"));
		return -1;
	}

	if (!(len_bytes == 1 || len_bytes == 2 || len_bytes == 4))
	{
		SS_ERROR(ACE_TEXT("StreamBuffer | error bytes param\n"));
		return -1;
	}

	size_t pkgLen = 0;
	int ret = get_package_length(message, len_offset, len_bytes, encode, pkgLen);
	if (0 != ret)
	{
		return 1;
	}

	if (pkgLen == (size_t)-1)
	{
		SS_ERROR(ACE_TEXT("StreamBuffer | message body length is error\n"));
		return -1;
	}

	if (pkgLen == 0)
	{
		SS_ERROR(ACE_TEXT("StreamBuffer | message body length is zero\n"));
		return -1;
	}

	if (pkgLen > 1024)
	{
		SS_ERROR(ACE_TEXT("StreamBuffer | message body length is bigger than 1024\n"));
		return -1;
	}

	if (pkgLen == message->length())
	{
		return 0;
	}
	else if (pkgLen > message->length())
	{
		return 1;
	}
	else	// if (pkgLen < message->length())
	{
		try{
			ACE_Message_Block * next_message = new MessageBlock();
			if (!next_message)
			{
				SS_ERROR("failed to new MessageBlock\n");
				return 1;
			}
			next_message->copy(message->rd_ptr() + pkgLen, message->length() - pkgLen);
			message->wr_ptr(message->base() + pkgLen);
			message->cont(next_message);
			return StreamBuffer(next_message, len_offset, len_bytes, encode);

		}catch(const std::exception & e)
		{
			SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");
		}catch(...)
		{
			SS_ERROR(ACE_TEXT("unknown exception\n"));
		}
		return -1;
	}
}

//StreamBuffer::message_alloc StreamBuffer::alloc_;
//
//StreamBuffer::StreamBuffer()
//: len_offset_(-1)
//, len_bytes_(-1)
//, encode_(false)
//{
//
//}
//
//
//StreamBuffer::~StreamBuffer()
//{
//
//}
//
//int StreamBuffer::init(int argc, ACE_TCHAR * argv[])
//{
//	int err = ServiceTask::init(argc, argv);
//	if (-1 == err)
//		return -1;
//
//	// offset, bytes
//	const ACE_TCHAR * options = ACE_TEXT(":o:b:e:");
//	ACE_Get_Opt get_opt(argc, argv, options);
//	int c;
//	while((c = get_opt()) != EOF)
//	{
//		switch(c)
//		{
//		case 'o':
//			len_offset_ = ACE_OS::atoi(get_opt.optarg);
//			break;
//		case 'b':
//			len_bytes_ = ACE_OS::atoi(get_opt.optarg);
//			break;
//		case 'e':
//			encode_ = get_opt.optarg[0] == ACE_TCHAR('0') ? false : true;
//		}
//	}
//
//	if ((size_t)-1 == len_offset_ || (size_t)-1 == len_bytes_)
//	{
//		ACE_ERROR((LM_ERROR, ACE_TEXT("StreamBuffer::init | offset or len_bytes is unavailable\n")));
//		err = -1;
//	}
//
//	return err;
//}
//
//int StreamBuffer::handle_task_message(ACE_Message_Block * message)
//{
//	ObjectMessageBlock<Tcp
//	this->put_next(message);
//}
//
//
//ACE_Message_Block * StreamBuffer::buffer_message(ACE_Message_Block *blk, size_t len_offset, size_t len_bytes)
//{
//	if (!blk || !len_bytes) return 0;
//
//	ACE_Message_Block * msg = 0;
//	ACE_Message_Block * cur = 0;
//
//
//
//	while(true)
//	{
//		size_t pkgLen = get_package_length(blk, len_offset, len_bytes);
//		if (!pkgLen)
//			break;
//		if (!msg && blk->length() == pkgLen)
//			return blk;
//
//		try
//		{
//			//ACE_Message_Block  * tmp = alloc_.allocate(1);
//			// TODO:
//			ACE_Message_Block * tmp = new ACE_Message_Block();
//			if (!tmp)
//				throw std::bad_alloc();
//
//			//msg = new(msg) ACE_Message_Block();
//			tmp->init(pkgLen);
//			tmp->copy(blk->rd_ptr(), pkgLen);
//			blk->rd_ptr(pkgLen); 
//
//			if (!msg)
//				msg = tmp;
//			else
//				cur->cont(tmp);
//			cur = msg;
//		}
//		catch (...)
//		{
//		}
//	} // while
//	return msg;
//}
//
//

//
//
//ACE_SVC_FACTORY_DECLARE(StreamBuffer);
//ACE_SVC_FACTORY_DEFINE(StreamBuffer);
//
//extern "C" ACE_Svc_Export StreamBuffer * CreateStreamBuffer()
//{
//	try
//	{
//		return new StreamBuffer();
//	}
//	catch (...)
//	{
//		return 0;
//	}
//}
//
//
//extern "C" ACE_Svc_Export void DestroyStreamBuffer(StreamBuffer * s)
//{
//	try
//	{
//		delete s;
//	}
//	catch (...)
//	{
//		 
//	}
//}


__SERVICE_SPACE_END_NS__
