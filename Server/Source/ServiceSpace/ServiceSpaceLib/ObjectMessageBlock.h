﻿/********************************************************************
	created:	2008/06/26
	created:	26:6:2008   11:09
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\ObjectMessageBlock.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	ObjectMessageBlock
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_OBJECT_MESSAGE_BLOCK_H__
#define __SERVICE_SPACE_OBJECT_MESSAGE_BLOCK_H__

#include "StdHdr.h"
#include "MessageBlock.h"
#include "utilib/block_pool.h"
#include "utilib/obj_pool.h"

__SERVICE_SPACE_BEGIN_NS__


template<class T>
class ObjectMessageBlock
	: public ACE_Message_Block
{
public:
	ObjectMessageBlock(const T & val = T())
	{
		//this->init(static_cast<const char *>(pool_.allocate(sizeof(T))), sizeof(T));
		this->init(obj_, sizeof(obj_));
		new(obj_) T(val);
		this->wr_ptr(sizeof(T));
	}

	~ObjectMessageBlock()
	{
		T * p = (T *)obj_;
		p->~T();
	}

	T & obj()
	{
		return *((T *)obj_);
	}

	static void * operator new(size_t)
	{
		return pool_.allocate(1);
	}

	static void operator delete(void * p)
	{
		pool_.deallocate((ObjectMessageBlock *)p, 1);
	}

	char obj_[sizeof(T)];

	//typedef utilib::obj_pool<ObjectMessageBlock> pool;
	typedef std::allocator<ObjectMessageBlock> pool;
	static pool pool_;
};

template<class T>
typename ObjectMessageBlock<T>::pool ObjectMessageBlock<T>::pool_;

__SERVICE_SPACE_END_NS__

#endif

