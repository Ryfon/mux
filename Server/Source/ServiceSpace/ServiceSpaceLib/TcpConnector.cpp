﻿#define ACE_BUILD_SVC_DLL

#include "TcpConnector.h"
#include "TcpService.h"

__SERVICE_SPACE_BEGIN_NS__

#ifdef SS_USE_PROACTOR

TcpConnector::TcpConnector(TcpService * tcp)
: user_data_(0),  tcp_(tcp)
, pass_addresses_(1)
, validate_new_connection_(0)
{

}


void TcpConnector::handle_connect(const ACE_Asynch_Connect::Result &result)
{
	int error = 0;

	if (!result.success () || result.connect_handle () == ACE_INVALID_HANDLE)
	{
		error = 1;
	}

	if (result.error () != 0)
	{
		SS_ERROR(ACE_TEXT("(%d)TcpConnector::handle_connect\n"), result.error());
		error = 1;
	}

	// set blocking mode
	if (!error && ACE::clr_flags(result.connect_handle (), ACE_NONBLOCK) != 0)
	{
		error = 1;
		SS_ERROR (
			ACE_TEXT ("%p\n"),
			ACE_TEXT ("ACE_Asynch_Connector::handle_connect : Set blocking mode"));
	}

	// Parse the addresses.
	ACE_INET_Addr local_address;
	ACE_INET_Addr remote_address;
	if (!error &&
		(this->validate_new_connection_ || this->pass_addresses_))
		this->parse_address (result,
		remote_address,
		local_address);

	if (this->validate_new_connection_ &&
		this->validate_connection (result, remote_address, local_address) == -1)
	{
		error = 1;
	}

	session_ptr new_handler;
	if (!error)
	{
		session_type * phandler = 0;
		try{
			phandler = tcp_session_pool().allocate();
			if (!phandler)
				throw std::bad_alloc();

			phandler = new(phandler) session_type();
			session_ptr handler(phandler, utilib::destroyer::deallocator_traits<tcp_session_pool>::deallocator<session_type>::inst);
			phandler->weak_ptr_ = utilib::make_weak_ptr(handler);
			phandler->tcp_ = tcp_;
			new_handler = handler;
		}
		catch (...)
		{
			error = 1;
		}
	}

	// If no errors
	if (!error)
	{
		// Update the Proactor.
		new_handler->proactor (this->proactor ());

		// Pass the addresses
		if (this->pass_addresses_)
			new_handler->addresses (remote_address,
			local_address);

		// Pass the ACT
		if (result.act () != 0)
			new_handler->act (result.act ());

		// Set up the handler's new handle value
		new_handler->handle (result.connect_handle ());

		ACE_Message_Block  mb;

		// Initiate the handler with empty message block;
		new_handler->open (result.connect_handle (), mb);
	}

	// On failure, no choice but to close the socket
	if (error &&
		result.connect_handle() != ACE_INVALID_HANDLE)
		ACE_OS::closesocket (result.connect_handle ());

	this->handle_session_built(new_handler, result.error(), (void *)result.act());
}


void TcpConnector::handle_session_built(session_ptr & session, int error, void * act)
{
	if (tcp_)
		tcp_->handle_session_built(session, 0, error, act, user_data_);
}


void TcpConnector::user_data(void * ud)
{
	user_data_ = ud;
}


void * TcpConnector::user_data()
{
	return user_data_;
}

#else

TcpConnector::TcpConnector()
: user_data_(0)
{ }

void TcpConnector::user_data(void * d)
{
	user_data_ = d;
}


void * TcpConnector::user_data()
{
	return user_data_;
}

#endif

__SERVICE_SPACE_END_NS__
