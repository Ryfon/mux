﻿#define ACE_BUILD_SVC_DLL

#include "UdpService.h"
#include "MessageBlock.h"
#include "ObjectMessageBlock.h"


__SERVICE_SPACE_BEGIN_NS__


UdpService::act_pool UdpService::act_pool_;

void * UdpService::ACT::operator new(size_t)
{
	return UdpService::act_pool_.allocate();
}


void UdpService::ACT::operator delete(void *p)
{
	UdpService::act_pool_.deallocate(p);
}


int UdpService::init(int argc, ACE_TCHAR *argv[])
{
	int err = ServiceTask::init(argc, argv);
	if (-1 == err)
		return -1;

	const ACE_TCHAR * options = ACE_TEXT(":h:p:");
	ACE_Get_Opt get_opt(argc, argv, options);
	int c;
	while((c = get_opt()) != EOF)
	{
	}

	err = this->RegisterTask();

	return err;
}


int UdpService::open(void *)
{
	return 0;
}


int UdpService::create_udp_receiver(const ACE_INET_Addr & address, void * user_data /* = 0 */) 
{
	utilib::pool_guard<utilib::pool_lock> guard(receiver_lock_);
	if (!receivers_.capacity())
		return -1;

	receivers_.resize(receivers_.size() + 1);
	int err = receivers_[receivers_.size() - 1].open(address, this, user_data);
	if (-1 == err)
		return -1;

	return int(receivers_.size() - 1);
}


void UdpService::post_asynch_read(int ind, ACE_Message_Block * block, void * act)
{
	utilib::pool_guard<utilib::pool_lock> guard(receiver_lock_);
	try
	{
		UdpReceiver & receiver = receivers_[ind];
		ACT * pAct = new ACT;
		pAct->user_act = act;
		pAct->ind = ind;
		pAct->user_data = receiver.user_data();
		receiver.post_asynch_read(block, pAct);
	}
	catch(...)
	{
	}
}


void UdpService::post_asynch_write(int ind, const ACE_INET_Addr & address, ACE_Message_Block * block, void * act)
{
	utilib::pool_guard<utilib::pool_lock> guard(receiver_lock_);
	try
	{
		UdpReceiver & sender = receivers_[ind];
		ACT * pAct = new ACT();
		pAct->user_act = act;
		pAct->ind = ind;
		pAct->remote_addr = address;
		sender.post_asynch_write(address, block, pAct);
	}
	catch(...)
	{
	}
}


void UdpService::handle_read_dgram(const ACE_Asynch_Read_Dgram::Result &result)
{
	try{
		ObjectMessageBlock<UDP_Notify> * message = new ObjectMessageBlock<UDP_Notify>();
		UDP_Notify & notify = message->obj();
		result.remote_address(notify.remote_addr);
		notify.message = result.message_block();
		notify.flag = UDP_Notify::UDP_RECEIVED;
		notify.error = result.error();
		ACT * pAct = (ACT *)result.act();
		notify.user_act = pAct->user_act;
		notify.user_data = pAct->user_data;
		pAct->user_act = 0;
		delete pAct;
		this->put_next(message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void UdpService::handle_write_dgram(const ACE_Asynch_Write_Dgram::Result &result)
{
	try{
		ObjectMessageBlock<UDP_Notify> * message = new ObjectMessageBlock<UDP_Notify>();
		UDP_Notify & notify = message->obj();
		notify.message = result.message_block();
		notify.flag = UDP_Notify::UDP_SEND;
		notify.error = result.error();
		ACT * pAct = (ACT *)result.act();
		notify.remote_addr = pAct->remote_addr;
		notify.user_act = pAct->user_act;
		notify.user_data = pAct->user_data;
		pAct->user_act = 0;
		delete pAct;
		this->put_next(message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void UdpService::handle_post_asynch_read_error(const ACE_INET_Addr & remote_addr, ACE_Message_Block * block, void * act, void * user_data, int error)
{
	try{
		ObjectMessageBlock<UDP_Notify> * message = new ObjectMessageBlock<UDP_Notify>();
		UDP_Notify & notify = message->obj();
		notify.message = block;
		notify.error = error;
		notify.remote_addr = remote_addr;
		notify.flag = UDP_Notify::UDP_RECEIVED;
		ACT * pAct = (ACT *)act;
		notify.user_act = pAct ? pAct->user_act : 0;
		notify.user_data = user_data;
		delete pAct;
		this->put_next(message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void UdpService::handle_post_asynch_write_error(const ACE_INET_Addr & remote_addr, ACE_Message_Block * block, void * act, void * user_data, int error)
{
	try{
		ObjectMessageBlock<UDP_Notify> * message = new ObjectMessageBlock<UDP_Notify>();
		UDP_Notify & notify = message->obj();
		notify.message = block;
		notify.error = error;
		notify.remote_addr = remote_addr;
		notify.flag = UDP_Notify::UDP_SEND;
		ACT * pAct = (ACT *)act;
		notify.user_act = pAct->user_act;
		notify.user_data = user_data;
		delete pAct;
		this->put_next(message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


extern "C" ACE_Svc_Export UdpService * CreateUdpService()
{
	try
	{
		return new UdpService();
	}
	catch (...)
	{
		return 0;
	}
	
};


extern "C" ACE_Svc_Export void DestroyUdpService(UdpService * s)
{
	try
	{
		delete s;
	}
	catch(...)
	{

	}
};

ACE_SVC_FACTORY_DECLARE(UdpService);
ACE_SVC_FACTORY_DEFINE(UdpService);

__SERVICE_SPACE_END_NS__
