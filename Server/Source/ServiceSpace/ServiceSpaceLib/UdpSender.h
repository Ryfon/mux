﻿/********************************************************************
	created:	2008/07/02
	created:	2:7:2008   17:08
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\UdpSender.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	UdpSender
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_UDP_SENDER_H__
#define __SERVICE_SPACE_UDP_SENDER_H__

#include "StdHdr.h"
#include "ServiceTask.h"
#include "utilib/noncopyable.h"
#include <ace/SOCK_Dgram.h>
#include <ace/Asynch_IO.h>

__SERVICE_SPACE_BEGIN_NS__

struct Asynch_Op
{
	enum {
		ASYNCH_OP_READ_UDP,
		ASYNCH_OP_WRITE_UDP
	};

	int flag;
	ACE_Message_Block * msg;
	ACE_INET_Addr address;
	void * act;

	Asynch_Op():msg(0), act(0)
	{

	}

	~Asynch_Op()
	{
		if (msg)
			msg->release();
	}
};

#ifndef SS_USE_PROACTOR

class UdpService;

class ACE_Svc_Export UdpSender
	: public utilib::noncopyable
	, public ACE_Task<ACE_MT_SYNCH>
{
public:
	UdpSender(UdpService * udp = 0);

	int open(ACE_SOCK_Dgram * dgram, UdpService * udp = 0, void * _user_data = 0);

	void post_asynch_write(const ACE_INET_Addr & remote_addr, ACE_Message_Block * block = 0, void * act = 0);

	void post_asynch_write(const ACE_INET_Addr & remote_addr, const char * message, size_t size, void * act = 0);

	void user_data(void * ud);

	void * user_data()const;

	__SERVICE_SPACE_OBJ_DYNAMIC_NEW_DECLARATION__(UdpSender)

private:
	virtual int svc();

	ACE_SOCK_Dgram * sock_dgram_;
	UdpService * udp_;
	void * user_data_;
};

#endif

__SERVICE_SPACE_END_NS__

#endif
