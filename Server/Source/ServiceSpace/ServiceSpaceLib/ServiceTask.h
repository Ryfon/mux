﻿/********************************************************************
	created:	2008/05/26
	created:	26:5:2008   11:10
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpace\ServiceTask.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpace
	file base:	ServiceTask
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_SERVICE_TASK_H__
#define __SERVICE_SPACE_SERVICE_TASK_H__


#pragma once

#include "StdHdr.h"
#include <ace/Log_Msg.h>
#include <ace/Task.h>
#include <ace/Get_Opt.h>
#include <ace/svc_export.h>
#include <ace/Service_Repository.h>
#include "utilib/obj_pool.h"
#include "StreamCreator.h"


__SERVICE_SPACE_BEGIN_NS__

class ACE_Svc_Export ServiceTask
	: public ACE_Task<ACE_MT_SYNCH>
{
	typedef ACE_Task<ACE_MT_SYNCH> base;

public:
	ServiceTask();

	virtual ~ServiceTask();

	virtual int init(int argc, ACE_TCHAR *argv[]);

	virtual int open(void *args /* = 0 */);

	virtual int putq(ACE_Message_Block *, ACE_Time_Value *timeout = 0);

	virtual int put(ACE_Message_Block *, ACE_Time_Value * /* = 0 */);

	const TaskNameType & GetTaskName()const;

protected:
	int RegisterTask();

	void SetTaskName(const ACE_TCHAR * name);

	virtual int handle_task_message(ACE_Message_Block * message);

	virtual int svc(void);

	TaskNameType name_;
	StreamNameType stream_;
	ModuleNameType module_;
	int threads_;

public:
	static const ACE_TCHAR * options;
};


#define __SERVICE_SPACE_OBJ_DYNAMIC_NEW_DECLARATION__(CLASSNAME) static void * operator new(size_t size); \
	static void operator delete(void *p);

#define __SERVICE_SPACE_OBJ_DYNAMIC_NEW_DEFINITION__(CLASSNAME) static utilib::obj_pool<CLASSNAME, 5> __local_##CLASSNAME_alloc; \
	void * CLASSNAME::operator new(size_t) { return __local_##CLASSNAME_alloc.allocate(); } \
	void CLASSNAME::operator delete(void * p) { __local_##CLASSNAME_alloc.deallocate(p); }

__SERVICE_SPACE_END_NS__

#endif
