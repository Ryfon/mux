﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   10:11
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\StreamBuffer.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	StreamBuffer
	file ext:	h
	author:		
	
	purpose:	流消息缓冲器
*********************************************************************/

#ifndef __SERVICE_SPACE_STREAM_BUFFER_H__
#define __SERVICE_SPACE_STREAM_BUFFER_H__

#pragma once

#include "StdHdr.h"
#include "ServiceTask.h"

#include <ace/Message_Block.h>
#include <ace/Null_Mutex.h>
#include <ace/Task.h>
#include <ace/Get_Opt.h>
#include <memory>

__SERVICE_SPACE_BEGIN_NS__

extern "C" ACE_Svc_Export int StreamBuffer(ACE_Message_Block * message, size_t len_offset, size_t len_bytes, bool encode);


__SERVICE_SPACE_END_NS__

#endif
