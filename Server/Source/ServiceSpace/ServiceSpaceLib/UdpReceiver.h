﻿/********************************************************************
	created:	2008/07/02
	created:	2:7:2008   9:06
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\UdpReceiver.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	UdpReceiver
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_UDP_RECEIVER_H__
#define __SERVICE_SPACE_UDP_RECEIVER_H__

#include "StdHdr.h"
#include "ServiceTask.h"
#include "MessageBlock.h"
#include "UdpSender.h"
#include "utilib/noncopyable.h"
#include <ace/SOCK_Dgram.h>
#include <ace/Asynch_IO.h>
#include <ace/Task.h>
#include <ace/Mutex.h>

__SERVICE_SPACE_BEGIN_NS__

class UdpService;

class ACE_Svc_Export UdpReceiver
	: public utilib::noncopyable
#ifndef SS_USE_PROACTOR
	, public ACE_Task<ACE_MT_SYNCH>
#endif
{
	

public:
	UdpReceiver(UdpService * udp = 0);

	int open(const ACE_INET_Addr & addr, UdpService * udp = 0, void * _user_data = 0);

	void post_asynch_read(ACE_Message_Block * block = 0, void * act = 0);

	void post_asynch_write(const ACE_INET_Addr & remote_addr, ACE_Message_Block * block = 0, void * act = 0);

	void post_asynch_write(const ACE_INET_Addr & remote_addr, const char * message, size_t size, void * act = 0);

	const ACE_INET_Addr & address()const;

	void address(const ACE_INET_Addr & addr);

	void user_data(void * ud);

	void * user_data()const;

	__SERVICE_SPACE_OBJ_DYNAMIC_NEW_DECLARATION__(UdpReceiver)

protected:

#ifndef SS_USE_PROACTOR
	virtual int svc(void);

#endif

private:
	ACE_SOCK_Dgram sock_dgram_;
#ifdef SS_USE_PROACTOR
	ACE_Asynch_Read_Dgram read_dgram_;
	ACE_Asynch_Write_Dgram write_dgram_;
#else
	UdpSender sender_;
#endif
	ACE_INET_Addr address_;
	UdpService * udp_;
	void * user_data_;
};

__SERVICE_SPACE_END_NS__

#endif
