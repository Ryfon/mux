﻿/********************************************************************
	created:	2008/06/03
	created:	3:6:2008   11:31
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\TimerHandler.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	TimerHandler
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_TIMER_HANDLER_H__
#define __SERVICE_SPACE_TIMER_HANDLER_H__

#pragma once

#include "StdHdr.h"
#include "utilib/enable_shared_from_this.h"

#include <ace/Asynch_IO.h>
#include <ace/svc_export.h>
#include <ace/Proactor.h>

__SERVICE_SPACE_BEGIN_NS__

template<class ThisPtr,
		 class ActAllocator = std::allocator<void *> 
>
class TimerHandler  
	: public ACE_Service_Handler
	, public utilib::enable_shared_from_this<ThisPtr>
{
public:
	typedef ThisPtr handler_ptr;
	typedef typename handler_ptr::value_type handler_type;
	
protected:
	struct ACT
	{
		handler_ptr handler;
		void * act;

		ACT(): act(0) { }
		~ACT() {}
	};

public:
	typedef typename ActAllocator::template rebind<ACT>::other act_allocaotr;

	TimerHandler() { }

	~TimerHandler() { }

	virtual long schedule_timer(const ACE_Time_Value &time, void * act = 0)
	{
		ACT * pAct = 0;
		try
		{
			pAct = act_alloc_.allocate(1);
			if (!pAct)
				throw std::bad_alloc();

			ACE_OS::memset(pAct, 0, sizeof(ACT));
			pAct->act = 0;
			pAct->handler = this->shared_from_this();

			return ACE_Proactor::instance()->schedule_timer(*this, pAct, time);

		}
		catch (...)
		{
			if (pAct)
			{
				pAct->~ACT();
				act_alloc_.deallocate(pAct, 1);
			}
		}
	}

	virtual long schedule_repeating_timer (const ACE_Time_Value &interval, const void *act = 0)
	{
		ACT * pAct = 0;
		try
		{
			pAct = act_alloc_.allocate(1);
			if (!pAct)
				throw std::bad_alloc();

			ACE_OS::memset(pAct, 0, sizeof(ACT));
			pAct->act = 0;
			pAct->handler = this->shared_from_this();

			return ACE_Proactor::instance()->schedule_repeating_timer(*this, pAct, interval);

		}
		catch (...)
		{
			if (pAct)
			{
				pAct->~ACT();
				act_alloc_.deallocate(pAct, 1);
			}
		}
	}

	virtual void handle_time_out (const ACE_Time_Value &tv, const void *act = 0)
	{
		if (act)
		{
			ACT * p = (ACT *)act;
			p->~ACT();
			act_alloc_.deallocate(p, 1);
		}
	}

protected:
	act_allocaotr act_alloc_;

};

__SERVICE_SPACE_END_NS__

#endif
