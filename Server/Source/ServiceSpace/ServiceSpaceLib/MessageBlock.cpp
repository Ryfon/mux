﻿#define ACE_BUILD_SVC_DLL

#include <memory>
#include "MessageBlock.h"

__SERVICE_SPACE_BEGIN_NS__

//static utilib::block_pool<sizeof(MessageBlock), INIT_MESSAGE_BLOCK_COUNT> message_block_pool;
//static utilib::block_pool<MESSAGE_BLOCK_MAX_BYTES, INIT_MESSAGE_BLOCK_COUNT> message_block_bytes_pool; 
static std::allocator<MessageBlock> message_block_pool;
typedef char (block_bytes)[MESSAGE_BLOCK_MAX_BYTES];
static std::allocator<block_bytes> message_block_bytes_pool;

MessageBlock::MessageBlock()
: data_(0)
{
	data_ = message_block_bytes_pool.allocate(1);
	if (!data_)
		throw std::bad_alloc();
	ACE_Message_Block::init((const char *)data_, MESSAGE_BLOCK_MAX_BYTES);
}

MessageBlock::MessageBlock(const char * buf, size_t size)
: data_(0)
{
	ACE_Message_Block::init(buf, size);
}


MessageBlock::~MessageBlock()
{
	if (data_)
		message_block_bytes_pool.deallocate((block_bytes *)data_,1);
}

void * MessageBlock::operator new(size_t)
{
	return message_block_pool.allocate(1);
}


void MessageBlock::operator delete(void * p)
{
	message_block_pool.deallocate((MessageBlock *)p,1);
}

__SERVICE_SPACE_END_NS__
