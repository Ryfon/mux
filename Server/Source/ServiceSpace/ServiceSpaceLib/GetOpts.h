﻿/********************************************************************
	created:	2008/05/28
	created:	28:5:2008   13:21
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\GetOpts.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	GetOpts
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_GET_OPT_H__
#define __SERVICE_SPACE_GET_OPT_H__

#pragma once

#include <ace/OS.h>
#include <memory>
#include "StdHdr.h"

__SERVICE_SPACE_BEGIN_NS__


template<class Ch>
class GetOpts
{
public:
	Ch * argv[128];

	GetOpts(int argc, const Ch * const _argv[])
	{
		unsigned _argc = (unsigned)argc;
		memset(argv, 0, sizeof(argv));
		for(unsigned i = 0, j = 0; i < _argc && j < sizeof(argv)/sizeof(argv[0]); ++i, ++j)
		{
			size_t len = strlen(_argv[i]);
			argv[j] = new Ch[len + 1];			if (!argv[j])
				throw std::bad_alloc();
			ACE_OS::strcpy(argv[j], _argv[i]);			
		}
	}

	~GetOpts()
	{
		for(unsigned i = 0; i < sizeof(argv)/sizeof(argv[0]); ++i)
		{
			if (argv[i])
				delete argv[i];
		}
	}
};

__SERVICE_SPACE_END_NS__

#endif 
