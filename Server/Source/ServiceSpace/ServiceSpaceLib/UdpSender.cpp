﻿#define ACE_BUILD_SVC_DLL

#include "UdpSender.h"
#include "UdpService.h"
#include "ObjectMessageBlock.h"

__SERVICE_SPACE_BEGIN_NS__

#ifndef SS_USE_PROACTOR

__SERVICE_SPACE_OBJ_DYNAMIC_NEW_DEFINITION__(UdpSender)

UdpSender::UdpSender(UdpService * udp /* = 0 */)
: sock_dgram_(0),udp_(udp), user_data_(0)
{

}


int UdpSender::open(ACE_SOCK_Dgram * dgram, UdpService * udp /* = 0 */, void * _user_data /* = 0 */)
{
	udp_ = udp ? udp : udp_;

	if (!udp_)
	{
		SS_ERROR(ACE_TEXT("UdpSender::open | no udp service\n"));
		return -1;
	}

	sock_dgram_ = dgram;

	user_data_ = _user_data;

	this->activate();

	return 0;
}


void UdpSender::post_asynch_write(const ACE_INET_Addr & remote_addr, ACE_Message_Block * block /* = 0 */, void * act /* = 0 */)
{
	try{
		ObjectMessageBlock<Asynch_Op> * op_msg = new ObjectMessageBlock<Asynch_Op>();
		Asynch_Op & op = op_msg->obj();
		op.flag = Asynch_Op::ASYNCH_OP_WRITE_UDP;
		op.msg = block;
		op.act = act;
		op.address = remote_addr;
		this->putq(op_msg);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void UdpSender::post_asynch_write(const ACE_INET_Addr & remote_addr, const char * message, size_t size, void * act /* = 0 */)
{
	try{
		if (!size)
			return ;

		ACE_Message_Block * block = new MessageBlock();
		if (size > block->capacity())
		{
			SS_DEBUG(ACE_TEXT("UdpSender::post_asynch_write | message length goes beyond the [%d] limit\n"), block->space());
			return;
		}

		block->copy(message, size);

		post_asynch_write(remote_addr, block, act);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void UdpSender::user_data(void * ud)
{
	user_data_ = ud;
}


void * UdpSender::user_data()const
{
	return user_data_;
}


int UdpSender::svc()
{
	while (true)
	{
		ACE_Message_Block * msg = 0;
		if (-1 == this->getq(msg))
			continue;

		ObjectMessageBlock<Asynch_Op> * msg_op = dynamic_cast<ObjectMessageBlock<Asynch_Op> *>(msg);
		if (msg_op)
		{
			Asynch_Op & op = msg_op->obj();
			ssize_t size = sock_dgram_->send((const void *)op.msg->rd_ptr(), op.msg->length(), op.address, 0);
			if (size != -1)
			{
				op.msg->rd_ptr(size);
				udp_->handle_post_asynch_write_error(op.address, op.msg, op.act, user_data_, 0);
				op.msg = 0;
				op.act = 0;
			}
			else
				SS_ERROR(ACE_TEXT("%p\n"), ACE_TEXT("dgram send"));
		}

		if (msg)
			msg->release();
	}
}

#endif

__SERVICE_SPACE_END_NS__
