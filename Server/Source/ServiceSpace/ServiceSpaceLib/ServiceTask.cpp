﻿#define ACE_BUILD_SVC_DLL

#include "ServiceTask.h"
#include "GetOpts.h"

__SERVICE_SPACE_BEGIN_NS__

const ACE_TCHAR * ServiceTask::options = ACE_TEXT(":s:m:");

ServiceTask::ServiceTask()
: threads_(0)
{

}

ServiceTask::~ServiceTask()
{
	threads_ = 0;
}

int ServiceTask::init(int argc, ACE_TCHAR * argv[])
{
	threads_ = 0;
	GetOpts<ACE_TCHAR> opts(argc, argv);
	const ACE_TCHAR * options = ACE_TEXT(":s:m:t:");
	if (argc < 4)
	{
		SS_ERROR("command line arguments count is less than 4\n");
		return -1;
	}
	name_ = argv[0];

	ACE_Get_Opt get_opt(argc, (ACE_TCHAR **)(opts.argv), options);
	int c;
	while((c = get_opt()) != EOF)
	{
		switch(c)
		{
		case 's':
			stream_ = get_opt.optarg;
			break;
		case 'm':
			module_ = get_opt.optarg;
			break;
		case 'r':
			break;
		case 'w':
			break;
		case 't':
			threads_ = atoi(get_opt.optarg ? get_opt.optarg : "0");
			break;
		}
	}
	if (stream_.empty() || module_.empty())
	{
		SS_ERROR("stream or module is empty\n");
		return -1;
	}

	return 0;
}


int ServiceTask::open(void *)
{
	if (threads_)
	{
		int err = this->activate(THR_NEW_LWP | THR_JOINABLE | THR_INHERIT_SCHED, threads_);
		if (err == -1)
		{
			threads_  = 0;
			return -1;
		}
	}
	return 0;
}


int ServiceTask::putq(ACE_Message_Block * message, ACE_Time_Value *timeout /* = 0 */)
{
	if (threads_)
		return base::putq(message, timeout);
	else
		return this->handle_task_message(message);
}


int ServiceTask::put(ACE_Message_Block * message, ACE_Time_Value * timeout)
{
	return this->putq(message, timeout);
}

int ServiceTask::handle_task_message(ACE_Message_Block *)
{
	return 0;
}


void ServiceTask::SetTaskName(const ACE_TCHAR * name)
{
	name_ = name;
}


const TaskNameType & ServiceTask::GetTaskName()const
{
	return name_;
}


int ServiceTask::RegisterTask()
{
	StreamCreator * creator = StreamCreator::GetStreamCreator(stream_.c_str());
	if (!creator)
		return -1;

	return creator->RegisterTask(this, module_.c_str());
}


int ServiceTask::svc()
{
	while(true)
	{
		ACE_Message_Block * message = 0;
		if (-1 == this->getq(message))
			break;

		if (-1 == this->handle_task_message(message))
			break;
	}

	return 0;
}

__SERVICE_SPACE_END_NS__
