﻿#define ACE_BUILD_SVC_DLL

#include "StreamCreator.h"
#include "ServiceTask.h"


__SERVICE_SPACE_BEGIN_NS__

StreamCreator::StreamListType StreamCreator::stream_lst_;

int StreamCreator::init(int argc, ACE_TCHAR *argv[])
{
	const ACE_TCHAR * options = ACE_TEXT(":m:");
	if (argc < 1)
	{
		SS_ERROR("command line arguments count is zero\n");
		return -1;
	}

	stream_.stream_name = StreamNameType(argv[0]);

	SS_DEBUG("\n\n\n\n=======Stream[%s] inited=======begin\n", stream_.stream_name.c_str());

	ACE_Get_Opt get_opt(argc, argv, options);
	int c;
	while((c = get_opt()) != EOF)
	{
		switch(c)
		{
		case 'm':
			{
				ModuleDescType module_desc(get_opt.optarg);
				Module module;
				int ret = ParseModule(module_desc, module);
				if (-1 == ret)
					return -1;
				stream_.module_vec.push_back(module);
			}
			break;
		default:
		    break;
		}
	}

	stream_lst_.push_back(this);
	SS_DEBUG("\n=======Stream[%s] inited=======end\n\n\n\n", stream_.stream_name.c_str());
	return 0;
}


int StreamCreator::ParseModule(const ModuleDescType & desc, Module & module)
{
	size_t i = desc.find(ACE_TCHAR('['));
	if (i == (size_t)-1)
	{
		SS_ERROR("no task\n");
		return -1;
	}

	module.module_name = ModuleNameType(desc.c_str(), i);
	if (module.module_name.size() == 0)
	{
		SS_ERROR("null module\n");
		return -1;
	}

	if (desc[desc.size() - 1] != ACE_TCHAR(']'))
	{
		ACE_ERROR((LM_ERROR, "'['unmatchv"));
		return -1;
	}

	ModuleDescType tasks_desc(desc.c_str() + i + 1, desc.size() - i - 2);
	i = tasks_desc.find(ACE_TCHAR(','));
	if (i != (size_t)-1)
	{
		module.writer.task_name = TaskNameType(tasks_desc.c_str(), i);
		module.reader.task_name = TaskNameType(tasks_desc.c_str() + i + 1, tasks_desc.size() - i - 1);
	}
	else
	{
		module.writer.task_name = TaskNameType(tasks_desc.c_str(), tasks_desc.size());
	}
	
	if (module.reader.task_name.size() == 0 && module.writer.task_name.size() == 0)
	{
		SS_ERROR("no task\n");
		return -1;
	}

	return 0;
}


int StreamCreator::RegisterTask(ServiceTask * task, const ModuleNameType & module_name)
{
	if (!task)
		return -1;

	ModuleVecType::iterator it = stream_.module_vec.begin();
	for (; it != stream_.module_vec.end(); ++it)
	{
		if (it->module_name == module_name)
		{
			if (it->reader.task_name == task->GetTaskName())
			{
				it->reader.task = task;
			}
			else if (it->writer.task_name == task->GetTaskName())
			{
				it->writer.task = task;
			}
			else
			{
				return -1;
			}
			break;
		}
	}

	return this->Active();
}


int StreamCreator::Active()
{
	ModuleVecType::iterator it = stream_.module_vec.begin();
	for (; it != stream_.module_vec.end(); ++it)
	{
		if (it->reader.task_name.empty() == false && it->reader.task == 0)
			return 0;

		if (it->writer.task_name.empty() == false && it->writer.task == 0)
			return 0;
	}

	if (stream_.stream)
		return 0;

	try
	{
		stream_.stream = new MTStream();
		if (!stream_.stream)
			throw std::bad_alloc();

		ModuleVecType::reverse_iterator rit = stream_.module_vec.rbegin();
		for (; rit != stream_.module_vec.rend(); ++rit)
		{
			MTModule * module = new MTModule(rit->module_name.c_str(), 
				rit->writer.task, rit->reader.task, 0, MTModule::M_DELETE);

			if (-1 == stream_.stream->push(module))
				throw -1;
		}
	}
	catch (...)
	{
		// TODO: clean
		return -1;
	}
	
	return 0;
}


StreamCreator * StreamCreator::GetStreamCreator(const ACE_TCHAR * svc_name)
{
	/*struct equal_svc_name
	{
		equal_svc_name(const ACE_TCHAR * svc)
			: name(svc) { }

		bool operator()(StreamCreator * s)
		{
			if (s->stream_.stream_name == name)
				return true;
			return false;
		}    

		const ACE_TCHAR * name;
	};

	if (!svc_name)
		return 0;

	StreamListType::iterator it = std::find_if(stream_lst_.begin(), stream_lst_.end(), equal_svc_name(svc_name));
	return it != stream_lst_.end() ? *it : 0;*/

	try
	{
		return ACE_Dynamic_Service<StreamCreator>::instance(svc_name);
	}
	catch (...)
	{
		return 0;
	}
	
}


ACE_SVC_FACTORY_DECLARE(StreamCreator);
ACE_SVC_FACTORY_DEFINE(StreamCreator);


extern "C" ACE_Svc_Export StreamCreator * CreateStream(void)
{
	try
	{
		return new StreamCreator();
	}
	catch(...)
	{
		return 0;
	}
}


extern "C" ACE_Svc_Export void DestroyStream(StreamCreator * stream)
{
	try
	{
		delete stream;
	}
	catch (...)
	{
		
	}
}

__SERVICE_SPACE_END_NS__
