﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   14:10
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\NetSession.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	NetSession
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_NET_SESSION_H__
#define __SERVICE_SPACE_NET_SESSION_H__

#pragma once

#include <ace/INET_Addr.h>
#include <ace/SOCK_Stream.h>
#include <ace/Asynch_IO.h>
#include <ace/Message_Block.h>
#include <ace/Signal.h>
#include <ace/Proactor.h>
#include <ace/Svc_Handler.h>
#include <ace/Token.h>
#include <ace/Guard_T.h>

#include <list>

#include "utilib/smart_ptr.h"
#include "utilib/enable_shared_from_this.h"
#include "MessageBlock.h"
#include "utilib/obj_pool.h"

__SERVICE_SPACE_BEGIN_NS__

class TcpService;

class TcpNotifyHandler;

class TcpSession;

#ifdef SS_USE_PROACTOR

typedef utilib::obj_pool<TcpSession, 1000> tcp_session_pool;
//typedef std::allocator<TcpSession> tcp_session_pool;

typedef utilib::smart_ptr<TcpSession, utilib::destroyer::deallocator_traits<tcp_session_pool>::deallocator, 
utilib::null_lock, tcp_session_pool> tcp_session;


class ACE_Svc_Export TcpSession
	: public ACE_Service_Handler
	, public utilib::enable_shared_from_this<tcp_session>
{
	struct ACT
	{
		tcp_session session;
		void * act;

		ACT() : act(0) { }

		static void * operator new(size_t);

		static void operator delete(void * p);
	};

	typedef utilib::block_pool<sizeof(ACT), 1000> act_alloc;

	static act_alloc act_alloc_;

public:
	friend class TcpService;
	friend class TcpAcceptor;
	friend class TcpConnector;

	typedef tcp_session session_ptr;

	TcpSession();

	virtual ~TcpSession();

	virtual void open(ACE_HANDLE fd, ACE_Message_Block &);

	virtual void post_asynch_read(ACE_Message_Block * blk = 0, void * act = 0);

	virtual void post_asynch_write(ACE_Message_Block * blk, void * act = 0);

	virtual void post_asynch_write(const char * message, size_t len, void * act = 0);

	virtual void set_user_data(void * data);

	virtual void * get_user_data();

	virtual void addresses (const ACE_INET_Addr & remote_addr, const ACE_INET_Addr & local_addr);

	virtual const ACE_INET_Addr & remote_address()const;

	virtual int close(u_long flags /* = 0 */);
protected:
	virtual void handle_read_stream(const ACE_Asynch_Read_Stream::Result &result);

	virtual void handle_write_stream(const ACE_Asynch_Write_Stream::Result &result);

private:
	void post_asynch_write_i();

	ACE_SOCK_Stream peer_;
	ACE_Asynch_Read_Stream reader_;
	ACE_Asynch_Write_Stream writer_;

	void * user_data_;
	void * acceptor_data_;
	ACE_INET_Addr remote_address_;
	TcpService * tcp_;

	struct SendData
	{
		const ACE_Message_Block * mb;
		void * act;
	};

	utilib::pool_lock send_lock_;
	std::list<SendData> send_list_;
};

inline const bool check_session(tcp_session s)
{
	return s.raw_ptr();
}

#else

class ACE_Svc_Export TcpSession 
	: public ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_NULL_SYNCH>
{
	typedef ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_NULL_SYNCH> base;
public:
	typedef TcpSession * session_ptr;

	TcpSession();

	virtual int open(void * arg);

	virtual int handle_close(ACE_HANDLE, ACE_Reactor_Mask);

	virtual int handle_input(ACE_HANDLE);

	virtual int handle_output(ACE_HANDLE);

	void post_asynch_read(ACE_Message_Block * blk = 0, void * act = 0);

	void post_asynch_write(ACE_Message_Block * blk, void * act = 0);

	const ACE_INET_Addr remote_address()const;

private:
	unsigned id_;
	void * acceptor_data_;
	ACE_Message_Block * mb_;
	ACE_Token send_lock_;
	std::list<ACE_Message_Block *> send_list_;

	static unsigned session_id_;
	static utilib::pool_lock id_lock_;
};

typedef TcpSession * tcp_session;

inline const bool check_session(tcp_session s)
{
	return s != 0;
}

#endif


__SERVICE_SPACE_END_NS__

#endif
