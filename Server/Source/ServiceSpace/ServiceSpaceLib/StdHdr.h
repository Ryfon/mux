﻿/********************************************************************
	created:	2008/05/28
	created:	28:5:2008   10:29
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\StdHdr.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	StdHdr
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __SERVICE_SPACE_STD_HDR_H__
#define __SERVICE_SPACE_STD_HDR_H__

#pragma once


#define __SERVICE_SPACE_BEGIN_NS__ namespace ss {
#define __SERVICE_SPACE_END_NS__ }

#define __SERVICE_SPACE_USING_NS__ using namespace ss;

#ifdef WIN32
#pragma warning(disable:4996)
#endif

#include <ace/ACE.h>
#include <ace/Log_Msg.h>
#include <ace/Mutex.h>
#include <ace/Guard_T.h>
#include <stdexcept>
#include <memory>
#include <fstream>
#include <ace/Time_Value.h>

class _create_error_log_msg
{
	_create_error_log_msg()
		:log_(0)
	{
		try{
			log_ = new std::ofstream("service_pace_error_log.log", ios::out | ios::app);
			if (!log_)
				throw std::bad_alloc();

		}catch(...)
		{
			delete log_;
			log_ = 0;
		}
	}

	std::ofstream * log_;
	ACE_Mutex mutex_;

public:
	~_create_error_log_msg()
	{
		delete log_;
	}

	void log(char * str)
	{
		ACE_Guard<ACE_Mutex> guard(mutex_);
		*log_ << str;
		log_->flush();
	}

	static _create_error_log_msg * get_log_msg()
	{
		static _create_error_log_msg inst;
		return &inst;
	}
};


#define _DEBUG_PREFIX ACE_TEXT("[DEBUG][%D][%t] ")
#define _ERROR_PREFIX ACE_TEXT("[ERROR][%D][%t] ")
#define _WARN_PREFIX ACE_TEXT("[WARN][%D][%t] ")
#define _INFO_PREFIX ACE_TEXT("[INFO][%D][%t] ")

#ifdef SS_NDEBUG
#	define SS_DEBUG(...) 
#else
#	define SS_DEBUG(...) ACE_DEBUG((LM_DEBUG, _DEBUG_PREFIX __VA_ARGS__))
#endif

#define SS_ERROR(...) do { \
	if (_create_error_log_msg::get_log_msg()) \
	{ \
		char buf[512]; \
		time_t now = time(0); \
		char * now_str = ctime(&now); \
		now_str[strlen(now_str) - 1] = 0; \
		sprintf(buf, "[%s]", now_str); \
		::sprintf(buf + strlen(buf), __VA_ARGS__); \
		_create_error_log_msg::get_log_msg()->log(buf); \
	} \
	ACE_DEBUG((LM_ERROR, _ERROR_PREFIX __VA_ARGS__)); \
	}while(false);

#define SS_WARN(...) ACE_DEBUG((LM_WARNING, _WARN_PREFIX __VA_ARGS__))
#define SS_INFO(...) ACE_DEBUG((LM_INFO, _INFO_PREFIX __VA_ARGS__))

//#define SS_USE_PROACTOR

#endif
