﻿/********************************************************************
	created:	2008/07/03
	created:	3:7:2008   16:44
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\TcpAcceptor.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	TcpAcceptor
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_TCP_ACCEPTOR_H__
#define __SERVICE_SPACE_TCP_ACCEPTOR_H__

#include "StdHdr.h"
#include "TcpSession.h"
#include "utilib/noncopyable.h"
#include <ace/Asynch_Acceptor.h>
#include <ace/Acceptor.h>
#include <ace/SOCK_Acceptor.h>

__SERVICE_SPACE_BEGIN_NS__

class TcpService;

#ifdef SS_USE_PROACTOR

class ACE_Svc_Export TcpAcceptor
	: public ACE_Asynch_Acceptor<TcpSession>
	, public utilib::noncopyable
{
	typedef  ACE_Asynch_Acceptor<TcpSession> base;
public:
	friend class TcpService;

	typedef TcpSession session_type;
	typedef session_type::session_ptr session_ptr;

	TcpAcceptor(TcpService * tcp = 0);

	void * user_data();

	void user_data(void * ud);

protected:
	virtual session_type * make_handler(void);

	virtual void handle_accept(const ACE_Asynch_Accept::Result &result);

	virtual void handle_session_built(session_ptr & session, ACE_Message_Block * message, int error, void * act);

	TcpService * tcp_;
	void * user_data_;

	int pass_addresses_;
	int validate_new_connection_;
};

#else

class ACE_Svc_Export TcpAcceptor
	: public ACE_Acceptor<TcpSession, ACE_SOCK_ACCEPTOR>
	, public utilib::noncopyable
{
public:
	TcpAcceptor();

	void * user_data();

	void user_data(void * d);

private:
	void * user_data_;

	friend class TcpService;
};

#endif

__SERVICE_SPACE_END_NS__

#endif
