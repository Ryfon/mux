﻿/********************************************************************
	created:	2008/05/28
	created:	28:5:2008   10:14
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\UdpService.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	UdpService
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_UDP_SERVICE_H__
#define __SERVICE_SPACE_UDP_SERVICE_H__

#pragma once

#include "StdHdr.h"
#include "ServiceTask.h"

#include <ace/SOCK_Dgram.h>
#include <ace/INET_Addr.h>
#include <ace/Asynch_IO.h>
#include "MessageBlock.h"
#include "utilib/block_pool.h"
#include "utilib/pool_lock.h"
#include "utilib/vectorN.h"
#include "UdpSender.h"
#include "UdpReceiver.h"

__SERVICE_SPACE_BEGIN_NS__

class ACE_Svc_Export UdpService
	: public ServiceTask
	, public ACE_Handler
{
public:
	friend class UdpReceiver;
	friend class UdpSender;

	struct ACT
	{
		ACE_INET_Addr remote_addr;
		void * user_act;
		void * user_data;
		int ind;

		static void * operator new(size_t size);

		static void operator delete(void * p);
	};

	typedef utilib::block_pool<sizeof(ACT), 500> act_pool;
	static act_pool act_pool_;

	struct UDP_Notify
	{
		enum {
			UDP_UNKNOWN,
			UDP_RECEIVED,
			UDP_SEND
		};

		ACE_INET_Addr remote_addr;
		ACE_Message_Block * message;
		void * user_act;
		void * user_data;
		int error;
		int flag;

		UDP_Notify()
			: message(0), user_act(0),error(0) ,flag(UDP_UNKNOWN)
		{
		}

		~UDP_Notify()
		{
			if (message)
				message->release();
		}
	};

	virtual int init(int argc, ACE_TCHAR *argv[]);

	virtual int open(void *args );

	int create_udp_receiver(const ACE_INET_Addr & address, void * user_data = 0);

	void close_udp_receiver(int ind);

	void post_asynch_read(int ind, ACE_Message_Block * block, void * act = 0);

	void post_asynch_write(int ind, const ACE_INET_Addr & address, ACE_Message_Block * block, void * act = 0);

protected:
	virtual void handle_read_dgram(const ACE_Asynch_Read_Dgram::Result &result);

	virtual void handle_write_dgram(const ACE_Asynch_Write_Dgram::Result &result);

	virtual void handle_post_asynch_read_error(const ACE_INET_Addr & remote_addr, ACE_Message_Block * message, void * act, void * user_data, int error);

	virtual void handle_post_asynch_write_error(const ACE_INET_Addr & remote_addr, ACE_Message_Block * message, void * act,  void * user_data, int error);

	utilib::pool_lock receiver_lock_;
	utilib::vectorN<UdpReceiver, 16> receivers_;
};

__SERVICE_SPACE_END_NS__

#endif



