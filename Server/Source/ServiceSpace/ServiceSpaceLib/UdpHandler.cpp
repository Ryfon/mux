﻿#define ACE_BUILD_SVC_DLL

#include "UdpHandler.h"
#include "UdpService.h"
#include "ObjectMessageBlock.h"
#include <ace/SOCK_Dgram.h>
#include <ace/Asynch_IO.h>

__SERVICE_SPACE_BEGIN_NS__

int UdpHandler::init(int argc, ACE_TCHAR *argv[])
{
	int err = ServiceTask::init(argc, argv);
	if (-1 == err)
		return -1;

	err = this->RegisterTask();

	return err;
}



int UdpHandler::handle_task_message(ACE_Message_Block * message)
{
	ObjectMessageBlock<UdpService::UDP_Notify> * p = dynamic_cast<ObjectMessageBlock<UdpService::UDP_Notify> *>(message);
	UdpService::UDP_Notify & notify = p->obj();
	try{
		if (notify.flag == UdpService::UDP_Notify::UDP_RECEIVED)
		{
			try 
			{ this->handle_message_read(notify.remote_addr, notify.message, notify.user_act, notify.error, notify.user_data); 
			}
			catch(...) 
			{}
			notify.message = 0;
		}
		else if (notify.flag == UdpService::UDP_Notify::UDP_SEND)
		{
			try { 
				this->handle_message_write(notify.remote_addr, notify.message, notify.user_act, notify.error, notify.user_data);
			}
			catch(...)
			{}
			notify.message = 0;
		}

	}catch(...)
	{
	}

	message->release();
	return 0;
}


void UdpHandler::handle_message_read(const ACE_INET_Addr & remote_addr, ACE_Message_Block * message, void * act, int error, void * user_data)
{
	try{
		SS_DEBUG("%d bytes received from [%s:%d]\n", message->length(), remote_addr.get_host_addr(), remote_addr.get_port_number());
		ObjectMessageBlock<UdpService::UDP_Notify> * p = new ObjectMessageBlock<UdpService::UDP_Notify>();
		UdpService::UDP_Notify & notify = p->obj();
		notify.flag = UdpService::UDP_Notify::UDP_RECEIVED;
		notify.remote_addr = remote_addr;
		notify.message = message;
		notify.user_act = act;
		notify.error = error;
		notify.user_data = user_data;
		this->put_next(p);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void UdpHandler::handle_message_write(const ACE_INET_Addr & remote_addr, ACE_Message_Block * message, void * act, int error, void * user_data)
{
	try{
		SS_DEBUG("[error:%d] UdpHandler::handle_message_write | remote address[%s:%d]\n", error, remote_addr.get_host_addr(), remote_addr.get_port_number());
		ObjectMessageBlock<UdpService::UDP_Notify> * p = new ObjectMessageBlock<UdpService::UDP_Notify>();
		UdpService::UDP_Notify & notify = p->obj();
		notify.flag = UdpService::UDP_Notify::UDP_SEND;
		notify.remote_addr = remote_addr;
		notify.message = message;
		notify.user_act = act;
		notify.error = error;
		notify.user_data = user_data;
		this->put_next(p);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


extern "C" ACE_Svc_Export UdpHandler * CreateUdpHandler()
{
	try
	{
		return new UdpHandler();
	}
	catch (...)
	{
		return 0;
	}
}

ACE_SVC_FACTORY_DECLARE(UdpHandler);
ACE_SVC_FACTORY_DEFINE(UdpHandler);

__SERVICE_SPACE_END_NS__
