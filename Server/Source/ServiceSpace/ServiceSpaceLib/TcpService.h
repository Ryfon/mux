﻿/********************************************************************
	created:	2008/07/07
	created:	7:7:2008   11:00
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\TcpService.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	TcpService
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVIE_SPACE_TCP_SERVICE_H__
#define __SERVIE_SPACE_TCP_SERVICE_H__


#include "StdHdr.h"
#include "ServiceTask.h"
#include "TcpAcceptor.h"
#include "TcpConnector.h"
#include "TcpSession.h"
#include "utilib/pool_lock.h"
#include "utilib/block_pool.h"
#include "utilib/vectorN.h"

__SERVICE_SPACE_BEGIN_NS__

class ACE_Svc_Export TcpService
	: public ServiceTask
{
	typedef ServiceTask base;
public:
	friend class TcpAcceptor;
	friend class TcpConnector;
	friend class TcpSession;

	struct Tcp_Notify
	{
		enum {
			TCP_UNKNOWN,
			TCP_RECEIVED,
			TCP_SEND,
			TCP_SESSION_BUILT,
			TCP_SESSION_CLOSED
		};

		tcp_session session;
		ACE_Message_Block * message;
		int error;
		int flag;
		void * act;
		void * user_data;

		Tcp_Notify(): message(0), error(0), flag(TCP_UNKNOWN),act(0), user_data(0)
		{

		}

		~Tcp_Notify()
		{
			if (message)
				message->release();
		}
	};

	TcpService();

	virtual int init(int argc, ACE_TCHAR *argv[]);

	int create_tcp_acceptor(const ACE_INET_Addr & address, void * user_data = 0);

	int create_tcp_connector(void * user_data = 0);

	void close_tcp_acceptor(int ind);

	void close_tcp_connector(int ind);

	void post_asynch_accept(int ind, void * act = 0);

	void post_asynch_connect(int ind, const ACE_INET_Addr & address, void * act = 0);

	static TcpService * instance();

	__SERVICE_SPACE_OBJ_DYNAMIC_NEW_DECLARATION__(TcpService)

protected:
	virtual void handle_session_built(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data);

	virtual void handle_session_closed(tcp_session & session, int cause, void * user_data);

	virtual void handle_session_received(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data);

	virtual void handle_session_send(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data);

private:
	typedef utilib::pool_lock lock_type;

	lock_type acceptors_lock_;
	lock_type connectors_lock_;
	utilib::vectorN<TcpAcceptor, 16> acceptors_;
	utilib::vectorN<TcpConnector, 16> connectors_;
};

__SERVICE_SPACE_END_NS__

#endif
