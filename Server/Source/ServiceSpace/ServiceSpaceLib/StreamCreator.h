﻿/********************************************************************
	created:	2008/05/26
	created:	26:5:2008   10:04
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpace\StreamCreator.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpace
	file base:	StreamCreator
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_STREAM_CREATOR_H__
#define __SERVICE_SPACE_STREAM_CREATOR_H__


#pragma once

#include <ace/Log_Msg.h>
#include <ace/Get_Opt.h>
#include <ace/svc_export.h>
#include <ace/Global_Macros.h>
#include <ace/Service_Object.h>
#include <ace/Service_Config.h>
#include <ace/Service_Types.h>
#include <ace/Service_Repository.h>
#include <ace/Dynamic_Service.h>
#include <ace/Stream.h>
#include <ace/Module.h>

#include <map>
#include <vector>
#include <list>
#include <algorithm>
#include "StdHdr.h"
#include "utilib/stringN.h"

__SERVICE_SPACE_BEGIN_NS__

typedef utilib::stringN<32, ACE_TCHAR> StreamNameType;
typedef utilib::stringN<128, ACE_TCHAR> ModuleDescType;
typedef StreamNameType ModuleNameType;
typedef ModuleNameType TaskNameType;
typedef ACE_Stream<ACE_MT_SYNCH> MTStream;
typedef ACE_Module<ACE_MT_SYNCH> MTModule;

class ServiceTask;

struct Module
{
	struct Task
	{
		TaskNameType task_name;
		ServiceTask * task;

		Task() : task(0) { }
	};

	ModuleNameType module_name;
	MTModule * module;
	Task reader;
	Task writer;

	Module() : module(0) { }
};

typedef std::vector<Module> ModuleVecType;

struct Stream
{
	StreamNameType stream_name;
	MTStream * stream;
	ModuleVecType module_vec;

	Stream() : stream(0) { }
};

class ACE_Svc_Export StreamCreator
	: public ACE_Service_Object
{
public:
	virtual int init(int argc, ACE_TCHAR * argv[]);

	int RegisterTask(ServiceTask * task, const ModuleNameType & module_name);

	static StreamCreator * GetStreamCreator(const ACE_TCHAR * svc_name);

protected:
	int Active();

	static int ParseModule(const ModuleDescType & desc, Module & module);

	Stream stream_;

	typedef std::list<StreamCreator *> StreamListType;

	static StreamListType stream_lst_;
};

extern "C" ACE_Svc_Export StreamCreator * CreateStream(void);

extern "C" ACE_Svc_Export void DestroyStream(StreamCreator * stream);

__SERVICE_SPACE_END_NS__

#endif
