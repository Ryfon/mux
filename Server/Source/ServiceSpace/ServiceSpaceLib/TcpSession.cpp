﻿#define ACE_BUILD_SVC_DLL
//#define ACE_NTRACE 0
#include "TcpSession.h"
#include "TcpService.h"

__SERVICE_SPACE_BEGIN_NS__

#ifdef SS_USE_PROACTOR

TcpSession::act_alloc TcpSession::act_alloc_;


void * TcpSession::ACT::operator new(size_t)
{
	return TcpSession::act_alloc_.allocate();
}


void TcpSession::ACT::operator delete(void * p)
{
	TcpSession::act_alloc_.deallocate(p);
}


TcpSession::TcpSession()
: user_data_(0)
{

}


TcpSession::~TcpSession()
{
	tcp_ = 0;
}


void TcpSession::set_user_data(void *data)
{
	user_data_ = data;
}


void * TcpSession::get_user_data()
{
	return user_data_;
}


void TcpSession::open(ACE_HANDLE fd, ACE_Message_Block &)
{
	peer_.set_handle(fd);
	int bufSize = 1024;
	peer_.set_option(SOL_SOCKET, SO_SNDBUF, &bufSize, sizeof(bufSize));
	bufSize = 0;
	peer_.set_option(SOL_SOCKET, SO_RCVBUF, &bufSize, sizeof(bufSize));
	bufSize = 1;
#ifdef WIN32
	peer_.set_option(IPPROTO_TCP, TCP_NODELAY, &bufSize, sizeof(bufSize));
#endif

	this->proactor(ACE_Proactor::instance());
	reader_.open(*this, fd, 0, this->proactor());
	writer_.open(*this, fd, 0, this->proactor());

	this->post_asynch_read(0);
	ACE_Sig_Action no_sigpipe((ACE_SignalHandler)SIG_IGN);
	no_sigpipe.register_action(SIGPIPE, 0);
}


void TcpSession::post_asynch_read(ACE_Message_Block * blk /* = 0 */, void * _act /* = 0 */)
{
	ACT * act = 0;
	ACE_Message_Block * mb = blk;
	try
	{
		act = new ACT();
		mb = mb ? mb : (new MessageBlock());
		if (!act || !mb)
			throw std::bad_alloc();

		tcp_session tmp = this->shared_from_this();
		act->session = tmp;
		act->act = _act;

		// !!!
		reader_.read(*mb, mb->space(), act);

	}
	catch (...)
	{
		if (act)
			delete act;
		if (mb)
			delete mb;

		SS_DEBUG(ACE_TEXT("%p"),ACE_TEXT("NetSession::post_asynch_read\n"));
	}
}

void TcpSession::post_asynch_write_i()
{
	if (send_list_.size() == 0)
		return;

	ACT * act = 0;
	ACE_Message_Block * mb = const_cast<ACE_Message_Block *>(send_list_.front().mb);
	try
	{
		act = new ACT();
		if (!act)
			throw std::bad_alloc();

		tcp_session tmp = this->shared_from_this();
		act->session = tmp;
		act->act = send_list_.front().act;

		send_list_.pop_front();
		writer_.write(*mb, mb->length(), act);

	}catch(...)
	{
		if (act)
			delete act;
		if (mb)
		{
			send_list_.pop_front();
			mb->release();
		}

		SS_DEBUG(ACE_TEXT("%p"),"NetSession::post_asynch_write\n");
	}
}


void TcpSession::post_asynch_write(ACE_Message_Block * blk, void * _act /* = 0 */)
{
	if (!blk)
		return ;

	utilib::pool_guard<utilib::pool_lock> guard(send_lock_);
	SendData sd;
	sd.mb = blk;
	sd.act = _act;
	send_list_.push_back(sd);
	
	if (send_list_.size() == 1)
		post_asynch_write_i();
}


void TcpSession::post_asynch_write(const char * message, size_t len, void * _act /* = 0 */)
{
	try{
		if (!message || !len)
			return;

		ACE_Message_Block * blk = new MessageBlock();
		blk->copy(message, len);
		post_asynch_write(blk, _act);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}

}



int TcpSession::close(u_long flags /* = 0 */)
{
	tcp_session session = this->shared_from_this();
	tcp_->handle_session_closed(session, flags, acceptor_data_);
	acceptor_data_ = 0;
	peer_.close();
	return -1;
}


void TcpSession::handle_read_stream(const ACE_Asynch_Read_Stream::Result &result)
{
	ACT * act = (ACT *)result.act();
	try{

		if (result.bytes_transferred() != 0)
		{
			tcp_session session = this->shared_from_this();
			tcp_->handle_session_received(session, &result.message_block(), result.error(), act->act, acceptor_data_);
		}
		else
			this->close(0);

	}catch(...)
	{

	}

	delete act;
}


void TcpSession::addresses(const ACE_INET_Addr & remote_addr, const ACE_INET_Addr &)
{
	remote_address_ = remote_addr;
}


const ACE_INET_Addr & TcpSession::remote_address()const
{
	return remote_address_;
}



void TcpSession::handle_write_stream(const ACE_Asynch_Write_Stream::Result &result)
{	
	ACT * act = (ACT *)result.act();
	try{
		if (result.bytes_transferred() != 0)
		{
			tcp_session session = this->shared_from_this();
			tcp_->handle_session_send(session, &result.message_block(), result.error(), act->act, acceptor_data_);

			utilib::pool_guard<utilib::pool_lock> guard(send_lock_);
			//send_list_.pop_front();
			post_asynch_write_i();
		}
		else
			this->close((u_long)-1);

	}catch(...)
	{

	}

	delete act;
}

#else

unsigned TcpSession::session_id_ = 0;
utilib::pool_lock TcpSession::id_lock_;

TcpSession::TcpSession()
: id_(0), acceptor_data_(0), mb_(0)
{ }

int TcpSession::open(void * arg)
{
	ACE_Reactor::instance()->register_handler(this, ACE_Event_Handler::READ_MASK);
	utilib::pool_guard<utilib::pool_lock> guard(id_lock_);
	id_ = ++session_id_;

	this->peer().enable(ACE_NONBLOCK);

	TcpSession * s = this;
	TcpService::instance()->handle_session_built(s, 0, 0, 0, acceptor_data_);

	return 0;
}

int TcpSession::handle_close(ACE_HANDLE fd, ACE_Reactor_Mask mask)
{
	TcpSession * s = this;
	TcpService::instance()->handle_session_closed(s, 0, acceptor_data_);
	return base::handle_close(fd, mask);
}

int TcpSession::handle_input(ACE_HANDLE)
{
	ACE_Message_Block * mb = 0;
	//try{
		mb = mb_ ? mb_ : new MessageBlock();
		if (!mb)
			return -1;
	/*}catch(...)
	{
		return -1;
	}*/

	while(true)
	{
		ssize_t n = this->peer().recv(mb->wr_ptr(), mb->space());
		if (n > 0)
		{
			mb->wr_ptr(mb->wr_ptr() + n);
			if (mb->space() == 0)
				break;
		}
		else if (n == 0)
		{
			mb->release();
			mb_ = 0;
			return -1;
		}
#ifdef _WINDOWS
		else if (WSAGetLastError() == EWOULDBLOCK)
		{
			break;
		}
#else
		else if (errno == EWOULDBLOCK)
		{
			break;
		}
#endif
		else
		{
			mb->release();
			mb_ = 0;
			int code = errno;
			SS_ERROR("error[%d] occurred when recv\n", code);
			return -1;
		}
	}

	TcpSession * s = this;
	TcpService::instance()->handle_session_received(s, mb, 0, 0, acceptor_data_);

	return 0;
}

int TcpSession::handle_output(ACE_HANDLE)
{
	//ACE_Guard<ACE_Token> guard(send_lock_);
	//if (send_list_.size() == 0)
	//	return 0;

	//ACE_Message_Block * mb = send_list_.front();
	//send_list_.pop_front();
	//SS_DEBUG("begin send\n");
	//ssize_t n = this->peer().send_n(mb->rd_ptr(), mb->length());
	//SS_DEBUG("end send\n");
	//// TODO:
	//mb->release();
	return 0;
}

void TcpSession::post_asynch_read(ACE_Message_Block * blk, void * act)
{
	mb_ = blk;
	return;
}

void TcpSession::post_asynch_write(ACE_Message_Block * blk, void * act)
{
	ACE_TRACE("TcpSession::post_asynch_write");
	if (!blk)
		return;

	//ACE_Guard<ACE_Token> guard(send_lock_);
	//send_list_.push_back(blk);

	//if (send_list_.size() == 1)
	//	handle_output(this->get_handle());

	while(true)
	{
		ssize_t n = this->peer().send_n(blk->rd_ptr(), blk->length());
		if (n == -1)
		{
			break;
		}

		if (n != blk->length())
		{
			blk->rd_ptr(n);
		}
		else
		{
			break;
		}
	}
	
	blk->release();
}

const ACE_INET_Addr TcpSession::remote_address()const
{
	ACE_INET_Addr addr;
	this->peer().get_remote_addr(addr);
	return addr;
}

#endif

__SERVICE_SPACE_END_NS__
