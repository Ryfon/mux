﻿/********************************************************************
	created:	2008/06/25
	created:	25:6:2008   14:24
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\MessageBlock.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	MessageBlock
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/


#ifndef __SERVICE_SPACE_MESSAGE_BLOCK_H__
#define __SERVICE_SPACE_MESSAGE_BLOCK_H__

#include "StdHdr.h"
#include <ace/Message_Block.h>
#include <ace/svc_export.h>
#include "utilib/block_pool.h"
#include "utilib/obj_pool.h"

__SERVICE_SPACE_BEGIN_NS__

enum { 
	INIT_MESSAGE_BLOCK_COUNT = 1000,
	MESSAGE_BLOCK_MAX_BYTES = 1024
};

class ACE_Svc_Export MessageBlock
	: public ACE_Message_Block
{
public:	
	MessageBlock();
	MessageBlock(const char * buf, size_t size);
	virtual ~MessageBlock();

	static void * operator new(size_t size);
	static void operator delete(void * p);	

private:
	void * data_;
};


__SERVICE_SPACE_END_NS__
#endif
