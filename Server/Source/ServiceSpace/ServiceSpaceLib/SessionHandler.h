﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   17:14
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\SessionHandler.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	SessionHandler
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_SESSION_HANDLER_H__
#define __SERVICE_SPACE_SESSION_HANDLER_H__

#pragma once

#include <ace/svc_export.h>

#include "StdHdr.h"
#include "utilib/function_map.h"
#include "ServiceTask.h"
#include "TcpSession.h"

__SERVICE_SPACE_BEGIN_NS__

class ACE_Svc_Export SessionHandler
	: public ServiceTask
{
public:
	SessionHandler();

	virtual ~SessionHandler();

	virtual int init(int argc, ACE_TCHAR *argv[]);

protected:
	virtual int handle_task_message(ACE_Message_Block * message);

	virtual void handle_session_built(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data);

	virtual void handle_session_closed(tcp_session & session, int cause, void * user_data);

	virtual void handle_message_received(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data);

	virtual void handle_message_send(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data);
};

__SERVICE_SPACE_END_NS__

#endif
