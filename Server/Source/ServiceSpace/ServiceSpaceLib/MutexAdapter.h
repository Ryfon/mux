﻿/********************************************************************
	created:	2008/05/27
	created:	27:5:2008   10:10
	filename: 	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib\MutexAdapter.h
	file path:	d:\Projects\ServiceSpace\ServiceSpace\ServiceSpaceLib
	file base:	MutexAdapter
	file ext:	h
	author:		
	
	purpose:	转换ACE同步类接口
*********************************************************************/

#ifndef __SERVICE_SPACE_MUTEX_ADAPTER_H__
#define __SERVICE_SPACE_MUTEX_ADAPTER_H__

#pragma once

#include "StdHdr.h"
#include <ace/Mutex.h>

__SERVICE_SPACE_BEGIN_NS__

class MutexAdapter
	: protected ACE_Mutex
{
public:
	void lock()
	{
		ACE_Mutex::acquire();
	}

	void unlock()
	{
		ACE_Mutex::release();
	}
};

__SERVICE_SPACE_END_NS__

#endif
