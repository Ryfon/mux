﻿#define ACE_BUILD_SVC_DLL

#include "TcpService.h"
#include "ObjectMessageBlock.h"

__SERVICE_SPACE_BEGIN_NS__

__SERVICE_SPACE_OBJ_DYNAMIC_NEW_DEFINITION__(TcpService)

TcpService::TcpService()
{

}


int TcpService::init(int argc, ACE_TCHAR *argv[])
{
	int err = base::init(argc, argv);
	if (-1 == err)
		return -1;

	return this->RegisterTask();
}


int TcpService::create_tcp_acceptor(const ACE_INET_Addr & address, void * _user_data /* = 0 */)
{
	utilib::pool_guard<lock_type> guard(acceptors_lock_);
	if (!acceptors_.capacity())
		return -1;

	acceptors_.resize(acceptors_.size() + 1);
#ifdef SS_USE_PROACTOR
	acceptors_[acceptors_.size() - 1].tcp_ = this;
#endif
	int err = acceptors_[acceptors_.size() - 1].open(address);
	if (-1 == err)
		return -1;
	
	acceptors_[acceptors_.size() - 1].user_data(_user_data);
	return int(acceptors_.size() - 1);
}


int TcpService::create_tcp_connector(void * _user_data /* = 0 */)
{
	utilib::pool_guard<lock_type> guard(connectors_lock_);
	if (!connectors_.capacity())
		return -1;

	connectors_.resize(connectors_.size() + 1);
#ifdef SS_USE_PROACTOR
	connectors_[connectors_.size() - 1].tcp_ = this;
	int err = connectors_[connectors_.size() - 1].open(1);
#else
	int err = connectors_[connectors_.size() - 1].open(0);
#endif
	if (-1 == err)
		return -1;

	connectors_[connectors_.size() - 1].user_data(_user_data);
	return int(connectors_.size() - 1);
}


void TcpService::close_tcp_acceptor(int)
{
}


void TcpService::close_tcp_connector(int)
{

}


void TcpService::post_asynch_accept(int ind, void * act)
{
	utilib::pool_guard<lock_type> guard(acceptors_lock_);
	try{
		
		TcpAcceptor & acceptor = acceptors_[ind];
#ifdef SS_USE_PROACTOR
		acceptor.accept(acceptor.bytes_to_read(), act);
#endif

	}catch(...)
	{

	}
}


void TcpService::post_asynch_connect(int ind, const ACE_INET_Addr & address, void * act)
{
	utilib::pool_guard<lock_type> guard(connectors_lock_);
	try{

		TcpConnector & connector = connectors_[ind];
#ifdef SS_USE_PROACTOR
		connector.connect(address, (const ACE_INET_Addr &)ACE_Addr::sap_any, 1, act);
#else
		TcpSession * s = new TcpSession();
		if (!s)
			return ;

		connector.connect(s, address);
#endif

	}catch(...)
	{

	}
}


void TcpService::handle_session_built(tcp_session & session, ACE_Message_Block *, int error, void * act, void * user_data)
{
	try{
		if (!check_session(session))
			return;
		ObjectMessageBlock<Tcp_Notify> * notify_message = new ObjectMessageBlock<Tcp_Notify>();
		Tcp_Notify & notify = notify_message->obj();
		notify.error = error;
		notify.session = session;
		notify.flag = Tcp_Notify::TCP_SESSION_BUILT;
		notify.act = act;
		notify.user_data = user_data;
		this->put_next(notify_message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void TcpService::handle_session_closed(tcp_session & session, int cause, void * user_data)
{
	try{
		ObjectMessageBlock<Tcp_Notify> * notify_message = new ObjectMessageBlock<Tcp_Notify>();
		Tcp_Notify & notify = notify_message->obj();
		notify.error = cause;
		notify.session = session;
		notify.flag = Tcp_Notify::TCP_SESSION_CLOSED;
		notify.user_data = user_data;
		this->put_next(notify_message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void TcpService::handle_session_received(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data)
{
	try{
		ObjectMessageBlock<Tcp_Notify> * notify_message = new ObjectMessageBlock<Tcp_Notify>();
		Tcp_Notify & notify = notify_message->obj();
		notify.error = error;
		notify.session = session;
		notify.flag = Tcp_Notify::TCP_RECEIVED;
		notify.act = act;
		notify.user_data = user_data;
		notify.message = message;
		this->put_next(notify_message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void TcpService::handle_session_send(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data)
{
	try{
		ObjectMessageBlock<Tcp_Notify> * notify_message = new ObjectMessageBlock<Tcp_Notify>();
		Tcp_Notify & notify = notify_message->obj();
		notify.error = error;
		notify.session = session;
		notify.flag = Tcp_Notify::TCP_SEND;
		notify.act = act;
		notify.user_data = user_data;
		notify.message = message;
		this->put_next(notify_message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");	
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}

TcpService * TcpService::instance()
{
	static TcpService * inst = new TcpService();
	return inst;
}

extern "C" ACE_Svc_Export TcpService * CreateTcpService()
{
	return TcpService::instance();
}


ACE_SVC_FACTORY_DECLARE(TcpService);
ACE_SVC_FACTORY_DEFINE(TcpService);

__SERVICE_SPACE_END_NS__
