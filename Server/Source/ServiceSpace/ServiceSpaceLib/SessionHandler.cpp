﻿#define ACE_BUILD_SVC_DLL

#include "SessionHandler.h"
#include "TcpService.h"
#include "ObjectMessageBlock.h"
#include "MessageBlock.h"
#include <stdexcept>

__SERVICE_SPACE_BEGIN_NS__

SessionHandler::SessionHandler()
{

}


SessionHandler::~SessionHandler()
{

}


int SessionHandler::init(int argc, ACE_TCHAR *argv[])
{
	int err = ServiceTask::init(argc, argv);
	if (-1 == err)
		return -1;

	err = this->RegisterTask();

	return err;
}


int SessionHandler::handle_task_message(ACE_Message_Block * message)
{
	ObjectMessageBlock<TcpService::Tcp_Notify> * mb = dynamic_cast<ObjectMessageBlock<TcpService::Tcp_Notify> *>(message);
	TcpService::Tcp_Notify & notify = mb->obj();
	try
	{
		switch(notify.flag)
		{
		case TcpService::Tcp_Notify::TCP_RECEIVED:
			{
				this->handle_message_received(notify.session, notify.message, notify.error, notify.act, notify.user_data);
				notify.message = 0;
			}
			break;
		case TcpService::Tcp_Notify::TCP_SEND:
			{
				this->handle_message_send(notify.session, notify.message, notify.error, notify.act, notify.user_data);
				notify.message = 0;
			}
			break;
		case TcpService::Tcp_Notify::TCP_SESSION_BUILT:
			{
				this->handle_session_built(notify.session, notify.message, notify.error, notify.act, notify.user_data);
				notify.message = 0;
			}
			break;
		case TcpService::Tcp_Notify::TCP_SESSION_CLOSED:
			{
				this->handle_session_closed(notify.session, notify.error, notify.user_data);
				notify.message = 0;
			}
			break;
		default:
			break;
		}
	}
	catch(...)
	{

	}

	message->release();
	return 0;
}



void SessionHandler::handle_session_built(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data)
{
	try{
		SS_DEBUG("session[%s:%d] built\n", session->remote_address().get_host_addr(), session->remote_address().get_port_number());
		ObjectMessageBlock<TcpService::Tcp_Notify> * notify_message = new ObjectMessageBlock<TcpService::Tcp_Notify>();
		TcpService::Tcp_Notify & notify = notify_message->obj();
		notify.session = session;
		notify.message = message;
		notify.flag = TcpService::Tcp_Notify::TCP_SESSION_BUILT;
		notify.error = error;
		notify.act = act;
		notify.user_data = user_data;
		this->put_next(notify_message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}



void SessionHandler::handle_message_received(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data)
{
	try{
		ObjectMessageBlock<TcpService::Tcp_Notify> * notify_message = new ObjectMessageBlock<TcpService::Tcp_Notify>();
		TcpService::Tcp_Notify & notify = notify_message->obj();
		notify.session = session;
		notify.message = message;
		notify.flag = TcpService::Tcp_Notify::TCP_RECEIVED;
		notify.act = act;
		notify.error = error;
		notify.user_data = user_data;
		this->put_next(notify_message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void SessionHandler::handle_message_send(tcp_session & session, ACE_Message_Block * message, int error, void * act, void * user_data)
{
	try{
		ObjectMessageBlock<TcpService::Tcp_Notify> * notify_message = new ObjectMessageBlock<TcpService::Tcp_Notify>();
		TcpService::Tcp_Notify & notify = notify_message->obj();
		notify.session = session;
		notify.message = message;
		notify.flag = TcpService::Tcp_Notify::TCP_SEND;
		notify.error = error;
		notify.act = act;
		notify.user_data = user_data;
		this->put_next(notify_message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


void SessionHandler::handle_session_closed(tcp_session & session, int cause, void * user_data)
{
	try{
		SS_DEBUG("session[%s:%d] closed\n", session->remote_address().get_host_addr(), session->remote_address().get_port_number());
		ObjectMessageBlock<TcpService::Tcp_Notify> * notify_message = new ObjectMessageBlock<TcpService::Tcp_Notify>();
		TcpService::Tcp_Notify & notify = notify_message->obj();
		notify.session = session;
		notify.message = 0;
		notify.flag = TcpService::Tcp_Notify::TCP_SESSION_CLOSED;
		notify.error = cause;
		notify.user_data = user_data;
		this->put_next(notify_message);
	}catch(const std::exception & e)
	{
		SS_ERROR(ACE_TEXT("exception: %s\n"), e.what() ? e.what() : "unknown exception");
	}catch(...)
	{
		SS_ERROR(ACE_TEXT("unknown exception\n"));
	}
}


__SERVICE_SPACE_END_NS__
