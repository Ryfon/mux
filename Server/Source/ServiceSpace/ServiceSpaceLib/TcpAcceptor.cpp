﻿#define ACE_BUILD_SVC_DLL

#include "TcpAcceptor.h"
#include "TcpService.h"

__SERVICE_SPACE_BEGIN_NS__

#ifdef SS_USE_PROACTOR

TcpAcceptor::TcpAcceptor(TcpService * tcp)
: tcp_(tcp), user_data_(0)
, pass_addresses_(1)
, validate_new_connection_(0)
{

}

void TcpAcceptor::handle_accept(const ACE_Asynch_Accept::Result &result)
{
	int error = 0;
	if (!result.success () || result.accept_handle () == ACE_INVALID_HANDLE)
	{
		error = 1;
	}

#if defined (ACE_WIN32)
	ACE_HANDLE listen_handle = this->get_handle();
	if (!error && ACE_OS::setsockopt (result.accept_handle (), SOL_SOCKET,
		SO_UPDATE_ACCEPT_CONTEXT, (char *) &listen_handle,	sizeof (listen_handle)) == -1)
	{
		error = 1;
	}
#endif /* ACE_WIN32 */

	// Parse address.
	ACE_INET_Addr local_address;
	ACE_INET_Addr remote_address;
	if (!error && (this->validate_new_connection() || 1))
		this->parse_address (result, remote_address, local_address);

	// Validate remote address
	if (!error && this->validate_new_connection_ &&
		(this->validate_connection (result, remote_address, local_address) == -1
		|| this->validate_new_connection (remote_address) == -1))
	{
		error = 1;
	}

	session_ptr new_handler;
	if (!error)
	{
		session_type * phandler = 0;
		try{
			phandler = tcp_session_pool().allocate();
			if (!phandler)
				throw std::bad_alloc();

			phandler = new(phandler) session_type();
			session_ptr handler(phandler, utilib::destroyer::deallocator_traits<tcp_session_pool>::deallocator<session_type>::inst);
			phandler->weak_ptr_ = utilib::make_weak_ptr(handler);
			phandler->acceptor_data_ = user_data_;
			phandler->tcp_ = tcp_;
			new_handler = handler;
		}
		catch (...)
		{
			error = 1;
		}
	}

	// If no errors
	if (!error)
	{
		new_handler->proactor (this->proactor ());

		if (this->pass_addresses_)
			new_handler->addresses (remote_address, local_address);

		if (result.act () != 0)
			new_handler->act (result.act ());

		new_handler->handle (result.accept_handle ());

		new_handler->open (result.accept_handle (), result.message_block ());
	}

	if (error && result.accept_handle() != ACE_INVALID_HANDLE )
		ACE_OS::closesocket (result.accept_handle ());

	// !!!
	this->handle_session_built(new_handler, &result.message_block(), 0, (void *)result.act());

	if (this->should_reissue_accept () && this->get_handle() != ACE_INVALID_HANDLE
#if defined (ACE_WIN32)
		&& result.error () != ERROR_OPERATION_ABORTED
#else
		&& result.error () != ECANCELED
#endif
		)
		this->accept (this->bytes_to_read());
}


void TcpAcceptor::handle_session_built(session_ptr & session, ACE_Message_Block * message, int error, void * act)
{
	tcp_->handle_session_built(session, message, error, act, user_data_);
}


void TcpAcceptor::user_data(void * ud)
{
	user_data_ = ud;
}


void * TcpAcceptor::user_data()
{
	return user_data_;
}


TcpAcceptor::session_type * TcpAcceptor::make_handler()
{
	return 0;
}

#else

TcpAcceptor::TcpAcceptor()
: user_data_(0)
{ }

void * TcpAcceptor::user_data()
{
	return user_data_;
}

void TcpAcceptor::user_data(void * d)
{
	user_data_ = d;
}

#endif
__SERVICE_SPACE_END_NS__
