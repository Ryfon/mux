﻿#include "LoginServiceProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

LoginServiceNS::LoginServiceProtocol::LoginServiceProtocol()
{
	m_mapEnCodeFunc[G_L_CHECK_USER_MD5                ]	=	&LoginServiceProtocol::EnCode__GL_CheckUserMD5Request;
	m_mapDeCodeFunc[G_L_CHECK_USER_MD5                ]	=	&LoginServiceProtocol::DeCode__GL_CheckUserMD5Request;

	m_mapEnCodeFunc[G_L_REPORT_USER                   ]	=	&LoginServiceProtocol::EnCode__GL_ReportUserRequest;
	m_mapDeCodeFunc[G_L_REPORT_USER                   ]	=	&LoginServiceProtocol::DeCode__GL_ReportUserRequest;

	m_mapEnCodeFunc[L_G_REPORT_USER_RESULT            ]	=	&LoginServiceProtocol::EnCode__LG_ReportUserResult;
	m_mapDeCodeFunc[L_G_REPORT_USER_RESULT            ]	=	&LoginServiceProtocol::DeCode__LG_ReportUserResult;

	m_mapEnCodeFunc[L_G_CHECK_USER_MD5_RESULT         ]	=	&LoginServiceProtocol::EnCode__LG_CheckUserMD5dResult;
	m_mapDeCodeFunc[L_G_CHECK_USER_MD5_RESULT         ]	=	&LoginServiceProtocol::DeCode__LG_CheckUserMD5dResult;

	m_mapEnCodeFunc[G_L_REPORT_USER_ENTER_GAME        ]	=	&LoginServiceProtocol::EnCode__GL_ReportUserEnterGame;
	m_mapDeCodeFunc[G_L_REPORT_USER_ENTER_GAME        ]	=	&LoginServiceProtocol::DeCode__GL_ReportUserEnterGame;

	m_mapEnCodeFunc[G_L_REPORT_USER_LEAVE_GAME        ]	=	&LoginServiceProtocol::EnCode__GL_ReportUserLeaveGame;
	m_mapDeCodeFunc[G_L_REPORT_USER_LEAVE_GAME        ]	=	&LoginServiceProtocol::DeCode__GL_ReportUserLeaveGame;

	m_mapEnCodeFunc[L_G_USER_GAME_STATE_NOTIFY        ]	=	&LoginServiceProtocol::EnCode__LG_UserGameStateNotify;
	m_mapDeCodeFunc[L_G_USER_GAME_STATE_NOTIFY        ]	=	&LoginServiceProtocol::DeCode__LG_UserGameStateNotify;

	m_mapEnCodeFunc[L_G_REPORT_USER_RESULT_WITH_KEY   ]	=	&LoginServiceProtocol::EnCode__LG_ReportUserResultWithKey;
	m_mapDeCodeFunc[L_G_REPORT_USER_RESULT_WITH_KEY   ]	=	&LoginServiceProtocol::DeCode__LG_ReportUserResultWithKey;

	m_mapEnCodeFunc[L_G_CHECK_USER_MD5_RESULT_WITH_KEY]	=	&LoginServiceProtocol::EnCode__LG_CheckUserMD5dResultWithKey;
	m_mapDeCodeFunc[L_G_CHECK_USER_MD5_RESULT_WITH_KEY]	=	&LoginServiceProtocol::DeCode__LG_CheckUserMD5dResultWithKey;

}

LoginServiceNS::LoginServiceProtocol::~LoginServiceProtocol()
{
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

LoginServiceNS::EnCodeFunc	LoginServiceNS::LoginServiceProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

LoginServiceNS::DeCodeFunc	LoginServiceNS::LoginServiceProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__UserId(void* pData)
{
	UserId* pkUserId = (UserId*)(pData);

	//EnCode userIdLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUserId->userIdLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(MAX_USER_ID_LEN < pkUserId->userIdLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUserId->userIdLen;
	if(!m_kPackage.Pack("CHAR", &(pkUserId->userId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__UserId(void* pData)
{
	UserId* pkUserId = (UserId*)(pData);

	//DeCode userIdLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUserId->userIdLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(MAX_USER_ID_LEN < pkUserId->userIdLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUserId->userIdLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUserId->userId), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__UserPasswrd(void* pData)
{
	UserPasswrd* pkUserPasswrd = (UserPasswrd*)(pData);

	//EnCode userPasswrdLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkUserPasswrd->userPasswrdLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userPasswrd
	if(MAX_USER_PASSWRD_LEN < pkUserPasswrd->userPasswrdLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUserPasswrd->userPasswrdLen;
	if(!m_kPackage.Pack("CHAR", &(pkUserPasswrd->userPasswrd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__UserPasswrd(void* pData)
{
	UserPasswrd* pkUserPasswrd = (UserPasswrd*)(pData);

	//DeCode userPasswrdLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkUserPasswrd->userPasswrdLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userPasswrd
	if(MAX_USER_PASSWRD_LEN < pkUserPasswrd->userPasswrdLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkUserPasswrd->userPasswrdLen;
	if(!m_kPackage.UnPack("CHAR", &(pkUserPasswrd->userPasswrd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__MD5Value(void* pData)
{
	MD5Value* pkMD5Value = (MD5Value*)(pData);

	//EnCode md5ValueLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMD5Value->md5ValueLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode md5Value
	if(16 < pkMD5Value->md5ValueLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMD5Value->md5ValueLen;
	if(!m_kPackage.Pack("CHAR", &(pkMD5Value->md5Value), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__MD5Value(void* pData)
{
	MD5Value* pkMD5Value = (MD5Value*)(pData);

	//DeCode md5ValueLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMD5Value->md5ValueLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode md5Value
	if(16 < pkMD5Value->md5ValueLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkMD5Value->md5ValueLen;
	if(!m_kPackage.UnPack("CHAR", &(pkMD5Value->md5Value), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__RandomString(void* pData)
{
	RandomString* pkRandomString = (RandomString*)(pData);

	//EnCode randomStringLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRandomString->randomStringLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode randomString
	if(MAX_USER_RANDOM_STRING < pkRandomString->randomStringLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRandomString->randomStringLen;
	if(!m_kPackage.Pack("CHAR", &(pkRandomString->randomString), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__RandomString(void* pData)
{
	RandomString* pkRandomString = (RandomString*)(pData);

	//DeCode randomStringLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRandomString->randomStringLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode randomString
	if(MAX_USER_RANDOM_STRING < pkRandomString->randomStringLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRandomString->randomStringLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRandomString->randomString), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__GL_CheckUserMD5Request(void* pData)
{
	GL_CheckUserMD5Request* pkGL_CheckUserMD5Request = (GL_CheckUserMD5Request*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_CheckUserMD5Request->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_CheckUserMD5Request->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_CheckUserMD5Request->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkGL_CheckUserMD5Request->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//EnCode md5Value
	if(EnCode__MD5Value(&(pkGL_CheckUserMD5Request->md5Value)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__GL_CheckUserMD5Request(void* pData)
{
	GL_CheckUserMD5Request* pkGL_CheckUserMD5Request = (GL_CheckUserMD5Request*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_CheckUserMD5Request->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_CheckUserMD5Request->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_CheckUserMD5Request->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkGL_CheckUserMD5Request->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//DeCode md5Value
	if(DeCode__MD5Value(&(pkGL_CheckUserMD5Request->md5Value)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__GL_ReportUserRequest(void* pData)
{
	GL_ReportUserRequest* pkGL_ReportUserRequest = (GL_ReportUserRequest*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkGL_ReportUserRequest->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__GL_ReportUserRequest(void* pData)
{
	GL_ReportUserRequest* pkGL_ReportUserRequest = (GL_ReportUserRequest*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserRequest->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserRequest->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserRequest->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkGL_ReportUserRequest->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__LG_ReportUserResult(void* pData)
{
	LG_ReportUserResult* pkLG_ReportUserResult = (LG_ReportUserResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_ReportUserResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_ReportUserResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_ReportUserResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_ReportUserResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkLG_ReportUserResult->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//EnCode randomString
	if(EnCode__RandomString(&(pkLG_ReportUserResult->randomString)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__LG_ReportUserResult(void* pData)
{
	LG_ReportUserResult* pkLG_ReportUserResult = (LG_ReportUserResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_ReportUserResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_ReportUserResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_ReportUserResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_ReportUserResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkLG_ReportUserResult->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//DeCode randomString
	if(DeCode__RandomString(&(pkLG_ReportUserResult->randomString)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__LG_ReportUserResultWithKey(void* pData)
{
	LG_ReportUserResultWithKey* pkLG_ReportUserResultWithKey = (LG_ReportUserResultWithKey*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_ReportUserResultWithKey->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_ReportUserResultWithKey->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_ReportUserResultWithKey->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_ReportUserResultWithKey->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkLG_ReportUserResultWithKey->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//EnCode randomString
	if(EnCode__RandomString(&(pkLG_ReportUserResultWithKey->randomString)) == -1)
	{
		return FAILEDRETCODE;
	}

	//EnCode randomKey
	if(EnCode__RandomString(&(pkLG_ReportUserResultWithKey->randomKey)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__LG_ReportUserResultWithKey(void* pData)
{
	LG_ReportUserResultWithKey* pkLG_ReportUserResultWithKey = (LG_ReportUserResultWithKey*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_ReportUserResultWithKey->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_ReportUserResultWithKey->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_ReportUserResultWithKey->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_ReportUserResultWithKey->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkLG_ReportUserResultWithKey->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//DeCode randomString
	if(DeCode__RandomString(&(pkLG_ReportUserResultWithKey->randomString)) == -1)
	{
		return FAILEDRETCODE;
	}

	//DeCode randomKey
	if(DeCode__RandomString(&(pkLG_ReportUserResultWithKey->randomKey)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__LG_CheckUserMD5dResult(void* pData)
{
	LG_CheckUserMD5dResult* pkLG_CheckUserMD5dResult = (LG_CheckUserMD5dResult*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_CheckUserMD5dResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_CheckUserMD5dResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_CheckUserMD5dResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_CheckUserMD5dResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkLG_CheckUserMD5dResult->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__LG_CheckUserMD5dResult(void* pData)
{
	LG_CheckUserMD5dResult* pkLG_CheckUserMD5dResult = (LG_CheckUserMD5dResult*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_CheckUserMD5dResult->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_CheckUserMD5dResult->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_CheckUserMD5dResult->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_CheckUserMD5dResult->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkLG_CheckUserMD5dResult->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__LG_CheckUserMD5dResultWithKey(void* pData)
{
	LG_CheckUserMD5dResultWithKey* pkLG_CheckUserMD5dResultWithKey = (LG_CheckUserMD5dResultWithKey*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_CheckUserMD5dResultWithKey->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode returnCode
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_CheckUserMD5dResultWithKey->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_CheckUserMD5dResultWithKey->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_CheckUserMD5dResultWithKey->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkLG_CheckUserMD5dResultWithKey->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//EnCode key
	if(EnCode__RandomString(&(pkLG_CheckUserMD5dResultWithKey->key)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__LG_CheckUserMD5dResultWithKey(void* pData)
{
	LG_CheckUserMD5dResultWithKey* pkLG_CheckUserMD5dResultWithKey = (LG_CheckUserMD5dResultWithKey*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_CheckUserMD5dResultWithKey->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode returnCode
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_CheckUserMD5dResultWithKey->returnCode), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_CheckUserMD5dResultWithKey->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_CheckUserMD5dResultWithKey->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkLG_CheckUserMD5dResultWithKey->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//DeCode key
	if(DeCode__RandomString(&(pkLG_CheckUserMD5dResultWithKey->key)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__GL_ReportUserEnterGame(void* pData)
{
	GL_ReportUserEnterGame* pkGL_ReportUserEnterGame = (GL_ReportUserEnterGame*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserEnterGame->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserEnterGame->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserEnterGame->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkGL_ReportUserEnterGame->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__GL_ReportUserEnterGame(void* pData)
{
	GL_ReportUserEnterGame* pkGL_ReportUserEnterGame = (GL_ReportUserEnterGame*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserEnterGame->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserEnterGame->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserEnterGame->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkGL_ReportUserEnterGame->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__GL_ReportUserLeaveGame(void* pData)
{
	GL_ReportUserLeaveGame* pkGL_ReportUserLeaveGame = (GL_ReportUserLeaveGame*)(pData);

	//EnCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserLeaveGame->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode gateId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserLeaveGame->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGL_ReportUserLeaveGame->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkGL_ReportUserLeaveGame->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__GL_ReportUserLeaveGame(void* pData)
{
	GL_ReportUserLeaveGame* pkGL_ReportUserLeaveGame = (GL_ReportUserLeaveGame*)(pData);

	//DeCode requestSeq
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserLeaveGame->requestSeq), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode gateId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserLeaveGame->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGL_ReportUserLeaveGame->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkGL_ReportUserLeaveGame->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::EnCode__LG_UserGameStateNotify(void* pData)
{
	LG_UserGameStateNotify* pkLG_UserGameStateNotify = (LG_UserGameStateNotify*)(pData);

	//EnCode gateId
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_UserGameStateNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode playerId
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_UserGameStateNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode userId
	if(EnCode__UserId(&(pkLG_UserGameStateNotify->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//EnCode state
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_UserGameStateNotify->state), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode accOnlinePeriod
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_UserGameStateNotify->accOnlinePeriod), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode accOfflinePeriod
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkLG_UserGameStateNotify->accOfflinePeriod), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	LoginServiceNS::LoginServiceProtocol::DeCode__LG_UserGameStateNotify(void* pData)
{
	LG_UserGameStateNotify* pkLG_UserGameStateNotify = (LG_UserGameStateNotify*)(pData);

	//DeCode gateId
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_UserGameStateNotify->gateId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode playerId
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_UserGameStateNotify->playerId), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode userId
	if(DeCode__UserId(&(pkLG_UserGameStateNotify->userId)) == -1)
	{
		return FAILEDRETCODE;
	}

	//DeCode state
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_UserGameStateNotify->state), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode accOnlinePeriod
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_UserGameStateNotify->accOnlinePeriod), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode accOfflinePeriod
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkLG_UserGameStateNotify->accOfflinePeriod), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

