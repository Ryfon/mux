﻿#ifndef			__LoginServiceProtocol__
#define			__LoginServiceProtocol__
#include "ParserTool.h"
#include "map"

namespace LoginServiceNS
{
	//Enums
	typedef	enum
	{
		MAX_USER_ID_LEN                   	=	32,             	//
		MAX_USER_PASSWRD_LEN              	=	32,             	//
		MAX_USER_RANDOM_STRING            	=	16,             	//
		USER_GAME_STATE_HEALTH            	=	0,              	//
		USER_GAME_STATE_TIRED             	=	1,              	//
		USER_GAME_STATE_ILLHEALTH         	=	2,              	//
		USER_GAME_STATE_LEAVE_NOW         	=	3,              	//
	}PROTOCOL_CONSTANT;

	typedef	enum
	{
		G_L_CHECK_USER_MD5                	=	0x1401,         	//
		G_L_REPORT_USER                   	=	0x1402,         	//
		L_G_REPORT_USER_RESULT            	=	0x4101,         	//
		L_G_CHECK_USER_MD5_RESULT         	=	0x4102,         	//
		G_L_REPORT_USER_ENTER_GAME        	=	0x1403,         	//
		G_L_REPORT_USER_LEAVE_GAME        	=	0x1404,         	//
		L_G_USER_GAME_STATE_NOTIFY        	=	0x4103,         	//
		L_G_REPORT_USER_RESULT_WITH_KEY   	=	0x4104,         	//
		L_G_CHECK_USER_MD5_RESULT_WITH_KEY	=	0x4105,         	//
	}MessageId;

	typedef	enum
	{
		L_G_RESULT_SUCCEED                	=	5000,           	//
		L_G_RESULT_ERROR_USER             	=	5001,           	//
		L_G_RESULT_ERROR_PASSWRD          	=	5002,           	//
	}L_G_RESULT_CODE;


	//Defines

	//Typedefs

	//Types
	struct UserId
	{
		USHORT      	userIdLen;                         	//
		CHAR        	userId[MAX_USER_ID_LEN];             	//
	};

	struct UserPasswrd
	{
		USHORT      	userPasswrdLen;                    	//
		CHAR        	userPasswrd[MAX_USER_PASSWRD_LEN];   	//
	};

	struct MD5Value
	{
		USHORT      	md5ValueLen;                       	//
		CHAR        	md5Value[16];                        	//
	};

	struct RandomString
	{
		USHORT      	randomStringLen;                   	//
		CHAR        	randomString[MAX_USER_RANDOM_STRING];	//
	};

	struct GL_CheckUserMD5Request
	{
		static const USHORT	wCmd = G_L_CHECK_USER_MD5;

		UINT        	requestSeq;                        	//
		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
		MD5Value    	md5Value;                          	//
	};

	struct GL_ReportUserRequest
	{
		static const USHORT	wCmd = G_L_REPORT_USER;

		UINT        	requestSeq;                        	//
		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
	};

	struct LG_ReportUserResult
	{
		static const USHORT	wCmd = L_G_REPORT_USER_RESULT;

		UINT        	requestSeq;                        	//
		UINT        	returnCode;                        	//
		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
		RandomString	randomString;                      	//
	};

	struct LG_ReportUserResultWithKey
	{
		static const USHORT	wCmd = L_G_REPORT_USER_RESULT_WITH_KEY;

		UINT        	requestSeq;                        	//
		UINT        	returnCode;                        	//
		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
		RandomString	randomString;                      	//
		RandomString	randomKey;                         	//
	};

	struct LG_CheckUserMD5dResult
	{
		static const USHORT	wCmd = L_G_CHECK_USER_MD5_RESULT;

		UINT        	requestSeq;                        	//
		UINT        	returnCode;                        	//
		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
	};

	struct LG_CheckUserMD5dResultWithKey
	{
		static const USHORT	wCmd = L_G_CHECK_USER_MD5_RESULT_WITH_KEY;

		UINT        	requestSeq;                        	//
		UINT        	returnCode;                        	//
		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
		RandomString	key;                               	//
	};

	struct GL_ReportUserEnterGame
	{
		static const USHORT	wCmd = G_L_REPORT_USER_ENTER_GAME;

		UINT        	requestSeq;                        	//
		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
	};

	struct GL_ReportUserLeaveGame
	{
		static const USHORT	wCmd = G_L_REPORT_USER_LEAVE_GAME;

		UINT        	requestSeq;                        	//
		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
	};

	struct LG_UserGameStateNotify
	{
		static const USHORT	wCmd = L_G_USER_GAME_STATE_NOTIFY;

		UINT        	gateId;                            	//
		UINT        	playerId;                          	//
		UserId      	userId;                            	//
		UINT        	state;                             	//
		UINT        	accOnlinePeriod;                   	//
		UINT        	accOfflinePeriod;                  	//
	};


	//Messages
	typedef enum Message_id_type
	{
		CheckUserMD5Message             	=	G_L_CHECK_USER_MD5,                	//GL_CheckUserMD5Request
		ReportUserMessage               	=	G_L_REPORT_USER,                   	//GL_ReportUserRequest
		ReportUserResultMessage         	=	L_G_REPORT_USER_RESULT,            	//LG_ReportUserResult
		CheckUserMD5ResultMessage       	=	L_G_CHECK_USER_MD5_RESULT,         	//LG_CheckUserMD5dResult
		ReportUserEnterGame             	=	G_L_REPORT_USER_ENTER_GAME,        	//GL_ReportUserEnterGame
		ReportUserLeaveGame             	=	G_L_REPORT_USER_LEAVE_GAME,        	//GL_ReportUserLeaveGame
		UserGameStateNotify             	=	L_G_USER_GAME_STATE_NOTIFY,        	//LG_UserGameStateNotify
		ReportUserResultMessageWithKey  	=	L_G_REPORT_USER_RESULT_WITH_KEY,   	//LG_ReportUserResultWithKey
		CheckUserMD5ResultMessageWithKey	=	L_G_CHECK_USER_MD5_RESULT_WITH_KEY,	//LG_CheckUserMD5dResultWithKey
	};

	//Class Data
	class LoginServiceProtocol;
	typedef size_t (LoginServiceProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (LoginServiceProtocol::*DeCodeFunc)(void* pData);

	class 
	#ifdef WIN32
	_declspec(dllexport) 
	#endif
	LoginServiceProtocol
	{
	public:
		LoginServiceProtocol();
		~LoginServiceProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__UserId(void* pData);
		size_t	DeCode__UserId(void* pData);

		size_t	EnCode__UserPasswrd(void* pData);
		size_t	DeCode__UserPasswrd(void* pData);

		size_t	EnCode__MD5Value(void* pData);
		size_t	DeCode__MD5Value(void* pData);

		size_t	EnCode__RandomString(void* pData);
		size_t	DeCode__RandomString(void* pData);

		size_t	EnCode__GL_CheckUserMD5Request(void* pData);
		size_t	DeCode__GL_CheckUserMD5Request(void* pData);

		size_t	EnCode__GL_ReportUserRequest(void* pData);
		size_t	DeCode__GL_ReportUserRequest(void* pData);

		size_t	EnCode__LG_ReportUserResult(void* pData);
		size_t	DeCode__LG_ReportUserResult(void* pData);

		size_t	EnCode__LG_ReportUserResultWithKey(void* pData);
		size_t	DeCode__LG_ReportUserResultWithKey(void* pData);

		size_t	EnCode__LG_CheckUserMD5dResult(void* pData);
		size_t	DeCode__LG_CheckUserMD5dResult(void* pData);

		size_t	EnCode__LG_CheckUserMD5dResultWithKey(void* pData);
		size_t	DeCode__LG_CheckUserMD5dResultWithKey(void* pData);

		size_t	EnCode__GL_ReportUserEnterGame(void* pData);
		size_t	DeCode__GL_ReportUserEnterGame(void* pData);

		size_t	EnCode__GL_ReportUserLeaveGame(void* pData);
		size_t	DeCode__GL_ReportUserLeaveGame(void* pData);

		size_t	EnCode__LG_UserGameStateNotify(void* pData);
		size_t	DeCode__LG_UserGameStateNotify(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool                    m_kPackage;
	};
}
#endif			//__LoginServiceProtocol__
