﻿/********************************************************************
	created:	2008/07/28
	created:	28:7:2008   16:14
	filename: 	d:\Projects\mux\Developer\PlatformTools\ServiceSpace\LoginService\MessageHandler.h
	file path:	d:\Projects\mux\Developer\PlatformTools\ServiceSpace\LoginService
	file base:	MessageHandler
	file ext:	h
	author:		
	
	purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_LOGIN_SERVICE_H__
#define __SERVICE_SPACE_LOGIN_SERVICE_H__

//#define ACE_NTRACE 0
#include "../ServiceSpaceLib/StdHdr.h"
#include "../ServiceSpaceLib/TcpSession.h"
#include "../ServiceSpaceLib/TcpService.h"
#include "../ServiceSpaceLib/MessageHandler.h"
#include "../ServiceSpaceLib/utilib/stringN.h"
#include "../ServiceSpaceLib/utilib/block_pool.h"
#include "../ServiceSpaceLib/utilib/pool_lock.h"
#include "../ServiceSpaceDBLib/db2o.h"
#include <ace/Token.h>
#include <ace/Guard_T.h>
#include "LoginServiceProtocol.h"
#include <map>
#include <memory>

using namespace LoginServiceNS;

__SERVICE_SPACE_BEGIN_NS__

typedef utilib::stringN<MAX_USER_ID_LEN> user_id_type;
typedef user_id_type user_passwrd_type;

class MysqlService;

class ACE_Svc_Export LoginServiceMessageHandler
	: public MessageHandler<LoginServiceMessageHandler>
{
	typedef MessageHandler<LoginServiceMessageHandler> base;
public:
	int init(int argc, ACE_TCHAR * argv[]);

	int open(void *args = 0);

	int handle_timeout (const ACE_Time_Value &current_time, const void *act = 0);

	static LoginServiceMessageHandler * instance();

protected:
	struct user_state : LG_UserGameStateNotify
	{
		time_t uplineTime;
		time_t lastUplineTime;
		time_t lastOfflineTime;
		std::string identification;
		unsigned identType;
		unsigned age;
		bool notifyTired;
		bool notifyIllhealth;
		bool notifyLeave;

		user_state(): uplineTime(0), lastUplineTime(0), lastOfflineTime(0), 
			identType(0), age(0), notifyTired(false),notifyIllhealth(false), notifyLeave(0)
		    {
		        this->gateId = 0;                            
				this->playerId = 0;                          	
				this->userId.userIdLen = 0;                            	
		        this->state = 0;                             	
		        this->accOnlinePeriod = 0;                   	
		        this->accOfflinePeriod = 0; 
		}
	};

	struct user_data
	{
		char randomString[16];
		char randomKey[16];
		char md5Value[16];
		UINT gateId;
		UINT playerId;
		tcp_session session;
	};

	struct gm_data
	{
		std::string account;
		std::string passwrd;
		unsigned level;

		gm_data() : account(), level(0) {}
	};

	int handle_task_message(ACE_Message_Block * message);

	int handle_report_user_account(tcp_session & session, const char * message, unsigned len);

	int handle_check_user_md5(tcp_session & session, const char * message, unsigned len);

	int handle_report_user_enter_game(tcp_session & session, const char * message, unsigned len);

	int handle_report_user_leave_game(tcp_session & session, const char * message, unsigned len);

	int get_message_id(ACE_Message_Block * message);

	int query_user_game_state(const user_id_type & user, user_state & state);

	int save_user_game_state(const user_id_type & user);

	const bool check_user_adult(const std::string & ident, unsigned len);

	ACE_Message_Block * build_result_message(void * data, int msgId);

	int send_result_message(ss::tcp_session &session, void *data, int msgId);

	void load_gm_account();
	
	friend class MysqlService;	

	//utilib::pool_lock users_lock_;
	ACE_Token users_lock_;
	std::map<user_id_type, user_data> users_;
	std::map<user_id_type, user_state> usersstate_;
	LoginServiceProtocol protocol_;
	std::auto_ptr<database<DB_MYSQL> > database_;
	std::map<int, tcp_session> gate_session_;
	std::map<std::string, gm_data> gms_;

	ACE_INET_Addr listen_addr_;
};
__SERVICE_SPACE_END_NS__

#endif
