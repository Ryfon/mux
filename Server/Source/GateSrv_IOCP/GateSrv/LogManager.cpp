﻿#include "LogManager.h"
#include "Player/Player.h"
#include "DealPkg/DealPkg.h"
#include "../../Base/PkgProc/LogSvcProtocol.h"
#include "OtherServer/otherserver.h"


//账号登陆
void CLogManager::DoAccountLogin(CPlayer* pPlayer, unsigned char type)
{
	if(NULL != pPlayer)
	{
		LOG_SVC_NS::GOAccountLogin logInfo;
		StructMemSet(logInfo, 0x00, sizeof(logInfo));

		logInfo.PlayType = type;
		logInfo.Time =(unsigned int) ACE_OS::time(NULL);
		SafeStrCpy(logInfo.Account, pPlayer->GetAccount());
		logInfo.AccountSize = (UINT)strlen(logInfo.Account);
#ifdef DS_EPOLL
		const char *pIP = pPlayer->GetLSocketRemoteAddr();
#else
		const char *pIP = NULL;
#endif/*DS_EPOLL*/
		if(NULL != pIP)
		{
			SafeStrCpy(logInfo.PlayerIP, pIP);
			logInfo.PlayerIPSize = (UINT)strlen(logInfo.PlayerIP);
		}

		NewSendMsgToLogSrv(LOG_SVC_NS::GOAccountLogin, logInfo);
	}

	return;
}

//进入游戏
void CLogManager::DoRoleEnterGame(CPlayer *pPlayer, unsigned char type)
{
	if(NULL != pPlayer)
	{
		LOG_SVC_NS::GORoleEnter logInfo;
		StructMemSet(logInfo, 0x00, sizeof(logInfo));
		
		logInfo.Time = (unsigned int)ACE_OS::time(NULL);
		SafeStrCpy(logInfo.Account, pPlayer->GetAccount());
		logInfo.AccountSize = (UINT)strlen(logInfo.Account);
		logInfo.RoleID = pPlayer->GetSeledRole();
		SafeStrCpy(logInfo.RoleName, pPlayer->GetRoleName());
		logInfo.RoleNameSize = (UINT)strlen(logInfo.RoleName);
		logInfo.RoleLevel = (BYTE) pPlayer->GetLevel();
		if(0x01 == type)//客户端
			logInfo.PlayType = 0;
		else if(0x02 == type)//irc
			logInfo.PlayType = 1;
		
		NewSendMsgToLogSrv(LOG_SVC_NS::GORoleEnter, logInfo);
	}

	return;
}

//账号登出
void CLogManager::DoAccountLogout(CPlayer *pPlayer, unsigned char type)
{
	if(NULL != pPlayer)
	{
		if(pPlayer->GetCurStat() >= PS_DB_PRE)//已向CenterServer发送过角色上线消息并记录了角色上线日至,此处记录对应的角色下线日志
		{
			LOG_SVC_NS::GORoleExit logRoleInfo;
			StructMemSet(logRoleInfo, 0x00, sizeof(logRoleInfo));
			
			logRoleInfo.Time = (unsigned int)ACE_OS::time(NULL);
			logRoleInfo.RoleID = pPlayer->GetSeledRole();
			logRoleInfo.RoleLevel =(BYTE) pPlayer->GetLevel();
			logRoleInfo.Duration = 0;
			logRoleInfo.PlayType = type;
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleExit, logRoleInfo);
		}

		LOG_SVC_NS::GOAccountLogoff logAccountInfo;
		StructMemSet(logAccountInfo, 0x00, sizeof(logAccountInfo));

		SafeStrCpy(logAccountInfo.Account, pPlayer->GetAccount());
		logAccountInfo.AccountSize = (UINT)strlen(logAccountInfo.Account);
		logAccountInfo.Time = (unsigned int)ACE_OS::time(NULL);
		logAccountInfo.Duration = 0;
		logAccountInfo.PlayType = type;
		NewSendMsgToLogSrv(LOG_SVC_NS::GOAccountLogoff, logAccountInfo);
	}

	return;
}

void CLogManager::DoAccountLogout(const char *pAccount, unsigned int duration, unsigned char type)
{
	if(NULL != pAccount)
	{
		LOG_SVC_NS::GOAccountLogoff logAccountInfo;
		StructMemSet(logAccountInfo, 0x00, sizeof(logAccountInfo));
		
		SafeStrCpy(logAccountInfo.Account, pAccount);
		logAccountInfo.AccountSize = (UINT)strlen(logAccountInfo.Account);
		logAccountInfo.Time = (unsigned int)ACE_OS::time(NULL);
		logAccountInfo.Duration = duration;
		logAccountInfo.PlayType = type;

		NewSendMsgToLogSrv(LOG_SVC_NS::GOAccountLogoff, logAccountInfo);
	}

	return;
}

//离开游戏
void CLogManager::DoRoleLeaveGame(unsigned int roleID, unsigned short roleLevel, unsigned int duration, unsigned char type)
{
	LOG_SVC_NS::GORoleExit logRoleInfo;
	StructMemSet(logRoleInfo, 0x00, sizeof(logRoleInfo));

	logRoleInfo.Time = (unsigned int)ACE_OS::time(NULL);
	logRoleInfo.RoleID = roleID;
	logRoleInfo.RoleLevel =(BYTE) roleLevel;
	logRoleInfo.Duration = duration;
	logRoleInfo.PlayType = type;
	
	NewSendMsgToLogSrv(LOG_SVC_NS::GORoleExit, logRoleInfo);
	return;
}

//关系变化
void CLogManager::DoRoleRelationChanage( CPlayer *pPlayer, unsigned int relationRoleId, unsigned char relationType, unsigned char chanageType)
{
	if(NULL != pPlayer)
	{
		LOG_SVC_NS::GORoleRelationChange logInfo;
		StructMemSet(logInfo, 0x00, sizeof(logInfo));
		
		logInfo.Time = (unsigned int)ACE_OS::time(NULL);
		logInfo.RoleID = pPlayer->GetSeledRole();
		logInfo.RoleLevel = (unsigned char) pPlayer->GetLevel();
		logInfo.RelationID = relationRoleId;
		logInfo.RelationType = relationType;
		logInfo.ChangeType = chanageType;
		
		NewSendMsgToLogSrv(LOG_SVC_NS::GORoleRelationChange, logInfo);
	}
	return;
}


//工会变化
void CLogManager::DoRoleUnionChanage(  CPlayer *pPlayer, unsigned int unionID, unsigned char chanageType)
{
	if(NULL != pPlayer)
	{
		LOG_SVC_NS::GORoleUnionChange logInfo;
		StructMemSet(logInfo, 0x00, sizeof(logInfo));
		
		logInfo.Time = (unsigned int)ACE_OS::time(NULL);
		logInfo.RoleID = pPlayer->GetSeledRole();
		logInfo.RoleLevel = (unsigned char)pPlayer->GetLevel();
		logInfo.UnionID = unionID;
		logInfo.ChangeType = chanageType;
		NewSendMsgToLogSrv(LOG_SVC_NS::GORoleUnionChange, logInfo);
	}

	return;
}


//组队变化
void CLogManager::DoRoleTeamChanage(  unsigned int roleID, unsigned short roleLevel, unsigned int teamID, unsigned char chanageType)
{
	LOG_SVC_NS::GOTeamChange loginfo;
	StructMemSet(loginfo, 0x00, sizeof(loginfo));

	loginfo.ChangeType = chanageType;//见类型ETeamChangeType
	loginfo.RoleID = roleID;
	loginfo.RoleLevel = (unsigned char)roleLevel;
	loginfo.TeamID = teamID;
	loginfo.Time = Now();

	NewSendMsgToLogSrv(LOG_SVC_NS::GOTeamChange, loginfo);

	return;
}

void CLogManager::DoMarketPruchase(CPlayer *pPlayer, unsigned int diamond, unsigned int credit, unsigned int itemType, unsigned itemNum)
{
	if(NULL != pPlayer)
	{
		LOG_SVC_NS::GOMarketTrade loginfo;
		StructMemSet(loginfo, 0x00, sizeof(loginfo));

		loginfo.Time = Now();
		loginfo.RoleID = pPlayer->GetSeledRole();
		loginfo.Diamond = diamond;
		loginfo.Credit = credit;
		loginfo.ItemTypeID = itemType;
		loginfo.ItemNum = itemNum;

		NewSendMsgToLogSrv(LOG_SVC_NS::GOMarketTrade, loginfo);
	}
	
	return;
}

//发送邮件
void CLogManager::DoSendMail(CPlayer *pPlayer, unsigned int recvRoleID, unsigned int mailID)
{
	if(true/*NULL != pPlayer*/)
	{
		LOG_SVC_NS::GOSendMail loginfo;
		StructMemSet(loginfo, 0x00, sizeof(loginfo));

		loginfo.Time = Now();
		loginfo.SendRoleID = 0;
		loginfo.RecvRoleID = recvRoleID;
		loginfo.MailID = mailID;

		NewSendMsgToLogSrv(LOG_SVC_NS::GOSendMail, loginfo);
	}

	return;
}

