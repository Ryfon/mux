﻿/** @file MapTeleporterManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-3-27
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef MAP_TELEPORTER_MANAGER_H
#define MAP_TELEPORTER_MANAGER_H

#include "../../../Base/aceall.h"
#include <vector>


/**
* @class CMapTeleporter
*
* @brief 地图上切换点信息
* 
*/

struct MuxPoint
{
	MuxPoint() : nPosX(0), nPosY(0) {}
	MuxPoint( int nX, int nY ) : nPosX(nX), nPosY(nY) {};
	int nPosX;
	int nPosY;
};

class CMapTeleporter
{
	friend class CMapTeleporterManager;

	enum 
	{
		IN_OFFSET = 0,      // 入口区域
		INVALD_OFFSET = 1,	// 无效区域
		OUT_OFFSET = 2		// 出口区域
	};
private:
	/// 构造
	CMapTeleporter(unsigned int uiID, unsigned short usMapID, const MuxPoint &leftTopPoint, const MuxPoint &rightBottomPoint, unsigned int uiLinkID);

	/// 析构
	~CMapTeleporter(void);

public:
	/// 地图上某点是否在该切换点有效范围内？
	bool IsValidPoint(int iPosX, int iPosY);

	/// 切换点周围的有效点集合
	void GetValidPoint(int &iPosX, int &iPosY);

public:
	/// 获取ID
	unsigned int ID(void);

	/// 自身地图ID
	unsigned short MapID(void);

	/// 连接地图ID
	unsigned short LinkMapID(void);

	/// 目标切换点ID
	unsigned int LinkID(void);

private:
	/// 切换点唯一ID
	unsigned int m_uiID;

	/// 地图编号
	unsigned short m_usMapID; 
	
	/// 左上
	MuxPoint m_leftTopPoint;

	/// 右下
	MuxPoint m_rightBottomPoint;
	
	/// 目标切换点ID
	unsigned int m_uiLinkID;
};


/**
* @class CMapTeleporterManager
*
* @brief 管理所有地图上的切换点信息
* 
*/
class CMapTeleporterManager
{
	friend class ACE_Singleton<CMapTeleporterManager, ACE_Null_Mutex>;
private:
	/// 构造
	CMapTeleporterManager(void);

	/// 析构
	~CMapTeleporterManager(void);

public:
	/// 初始化
	bool Init(const char * pszMapTeleporterFile);

	/// 释放
	void Clear(void);

	/// 获取跳转点（该跳转点同时覆盖两点)
	CMapTeleporter * GetMapTelePorter(unsigned short usMapID, int iPosX, int iPosY, int iPosX2, int iPosY2);

	/// 获取跳转点（根据mapId获取（posx,posy）最近的跳转点）
	CMapTeleporter * GetMapTelePorter(unsigned short usMapID, int iPosX, int iPosY);

	/// 获取跳转点（根据teleporterID）
	CMapTeleporter * GetMapTelePorter(unsigned int uiTeleporterID); 

private:
	/// 切换点列表
	std::vector<CMapTeleporter *> m_MapTeleporters;	
};


typedef ACE_Singleton<CMapTeleporterManager, ACE_Null_Mutex> CMapTeleManagerSingle;


#endif/*MAP_TELEPORTER_MANAGER_H*/
