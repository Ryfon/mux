﻿#include "MapTeleporterManager.h"
#include "XmlManager.h"
#include "Utility.h"
#include <math.h>


CMapTeleporter::CMapTeleporter(unsigned int uiID, unsigned short usMapID, const MuxPoint &leftTopPoint, const MuxPoint &rightBottomPoint, unsigned int uiLinkID)
:m_uiID(uiID), m_usMapID(usMapID), m_leftTopPoint(leftTopPoint), m_rightBottomPoint(rightBottomPoint), m_uiLinkID(uiLinkID)
{

}

CMapTeleporter::~CMapTeleporter(void)
{

}

bool CMapTeleporter::IsValidPoint(int iPosX, int iPosY)
{
	// 地图最左上点坐标（0,0）
	if((iPosX >= m_leftTopPoint.nPosX-2) && (iPosX <= m_rightBottomPoint.nPosX+2)
	   &&(iPosY >= m_leftTopPoint.nPosY-2) && (iPosY <= m_rightBottomPoint.nPosY+2))//适当放宽跳转范围;
	{
		return true;
	}
	else
	{
		return false;
	}
}

void CMapTeleporter::GetValidPoint(int &iPosX, int &iPosY)
{	
	iPosX = m_rightBottomPoint.nPosX + (INVALD_OFFSET + OUT_OFFSET);
	iPosY = m_rightBottomPoint.nPosY + (INVALD_OFFSET + OUT_OFFSET);

	return;
}

unsigned int CMapTeleporter::ID(void)
{
	return m_uiID;
}

unsigned short CMapTeleporter::MapID(void)
{
	return m_uiID/1000;
}

unsigned short CMapTeleporter::LinkMapID(void)
{
	return m_uiLinkID/1000;
}

unsigned int CMapTeleporter::LinkID(void)
{
	return m_uiLinkID;
}

CMapTeleporterManager::CMapTeleporterManager(void)
{

}

CMapTeleporterManager::~CMapTeleporterManager(void)
{
	Clear();
}

bool CMapTeleporterManager::Init(const char * pszMapTeleporterFile)
{
	if(NULL == pszMapTeleporterFile)
		return false;

	// 清空
	Clear();

	// 装载
	CXmlManager xmlManager;
	if(!xmlManager.Load(pszMapTeleporterFile))
	{
		D_ERROR("地图切换点信息加载出错");
		return false;
	}

	// 根元素
	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("地图切换点信息配置文件可能为空");
		return false;
	}

	// 根元素的所有子元素
	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);
	
	for(std::vector<CElement *>::iterator i = childElements.begin(); childElements.end() != i; i++)
	{
		CElement *pElement = *i;
		if(pElement->Level() == 1)
		{
			unsigned int uiID = atoi(pElement->GetAttr("TeleporterID"));
			unsigned short usMapID = atoi(pElement->GetAttr("MapID"));
			int iLeftTopX = atoi(pElement->GetAttr("LeftTopPosX"));
			int iLeftTopY = atoi(pElement->GetAttr("LeftTopPosY"));
			int iRightBottomX = atoi(pElement->GetAttr(("RightBottomPosX")));
			int iRightBottomY = atoi(pElement->GetAttr("RightBottomPosY"));
			unsigned int uiLinkID = atoi(pElement->GetAttr("LinkID"));
			
			if(uiID/1000 == usMapID)
			{
				CMapTeleporter *pMapTeleporter = NEW CMapTeleporter(uiID, usMapID, MuxPoint(iLeftTopX, iLeftTopY), MuxPoint(iRightBottomX, iRightBottomY), uiLinkID);
				if(pMapTeleporter == NULL)
				{
					D_ERROR("初始化CMapTeleporter时出错");
					return false;
				}

				// 插入
				m_MapTeleporters.push_back(pMapTeleporter);
			}
		}
	}

	return true;
}

void CMapTeleporterManager::Clear(void)
{
	for(std::vector<CMapTeleporter *>::iterator i = m_MapTeleporters.begin(); m_MapTeleporters.end() != i; i++)
	{
		delete *i;
		*i = NULL;
	}

	m_MapTeleporters.clear();

	return;
}

CMapTeleporter * CMapTeleporterManager::GetMapTelePorter(unsigned short usMapID, int iPosX, int iPosY, int iPosX2, int iPosY2)
{
	for(std::vector<CMapTeleporter *>::iterator i = m_MapTeleporters.begin(); m_MapTeleporters.end() != i; i++)
	{
		CMapTeleporter *pTemeporter = *i;
		if((NULL != pTemeporter) && (pTemeporter->MapID() == usMapID) && (pTemeporter->IsValidPoint(iPosX, iPosY)) && (pTemeporter->IsValidPoint(iPosX2, iPosY2)))
			return pTemeporter;
	}

	return NULL;
}

CMapTeleporter * CMapTeleporterManager::GetMapTelePorter(unsigned short usMapID, int iPosX, int iPosY)
{
	for(std::vector<CMapTeleporter *>::iterator i = m_MapTeleporters.begin(); m_MapTeleporters.end() != i; i++)
	{
		CMapTeleporter *pTemeporter = *i;
		if((NULL != pTemeporter) && (pTemeporter->MapID() == usMapID) && (pTemeporter->IsValidPoint(iPosX, iPosY)))
			return pTemeporter;
	}
	
	return NULL;
}


CMapTeleporter * CMapTeleporterManager::GetMapTelePorter(unsigned int uiTeleporterID)
{
	for(std::vector<CMapTeleporter *>::iterator i = m_MapTeleporters.begin(); m_MapTeleporters.end() != i; i++)
	{
		CMapTeleporter *pTemeporter = *i;
		if((NULL != pTemeporter) && (pTemeporter->ID() == uiTeleporterID) )
			return pTemeporter;
	}

	return NULL;
}
