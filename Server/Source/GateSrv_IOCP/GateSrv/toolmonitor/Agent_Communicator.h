﻿/** @file Agent_Communicator.h 
@brief 提供与 agent 通信的接口，由被监控服务器调用
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：史亚征
*	完成日期：2008-12-21
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef _AGENT_COMMUNICATOR_H_
#define _AGENT_COMMUNICATOR_H_


#ifdef WIN32

#ifdef AGENT_COMMUNICATOR_EXPORT
// DLL library project uses this
#define MAIN_ENTRY __declspec(dllexport)
#else
#ifdef AGENT_COMMUNICATOR_IMPORT
// client of DLL uses this
#define MAIN_ENTRY __declspec(dllimport)
#else
// static library project uses this
#define MAIN_ENTRY
#endif
#endif

#else
// Linux uses this
#define MAIN_ENTRY

#endif // WIN32


namespace MUX_BG_Monitor
{
	class Threaded_Pipe_Stream;
	class MAIN_ENTRY Agent_Communicator
	{
	public:
		~Agent_Communicator(void);

		// 连接 Agent.
		int connect();

		// 关闭与 Agent 的连接
		int close();

		// 向 agent 写数据。
		// return	-1	: 发送失败并且缓冲已满
		//			0	: 发送失败，数据存入缓冲
		//			>0	: 发送成功，返回发送的 buffer 长度
		unsigned int send(const void* buf, unsigned int size);

		// 接收从 Agent 发来的控制信息
		unsigned int  recv(void* buf, unsigned int len);

		static Agent_Communicator& get_instance();

	protected:
		Agent_Communicator(void);

		Threaded_Pipe_Stream*	m_Threaded_Stream;
	};
}

extern "C" MAIN_ENTRY MUX_BG_Monitor::Agent_Communicator* get_agent_communicator();

#endif

