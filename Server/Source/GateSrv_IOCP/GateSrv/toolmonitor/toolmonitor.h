﻿/*
  使用平台组件与监控端通信, by dzj, 09.03.04;
*/

#pragma once

#include "../../../Base/Utility.h"

#ifdef TOOL_MONITOR

//#include "../testso/test_pre.h"
#include "Agent_Communicator.h"

class CToolMonitor
{
private:
	CToolMonitor() {}
	~CToolMonitor() {}

public:
	static bool InitToolMonitor();

public:
	static bool SendPkgToMonitor( const char* pPkg, size_t pkgLen );

private:
	static MUX_BG_Monitor::Agent_Communicator* m_pAgentComm;

};

#define InitToolMonitor() CToolMonitor::InitToolMonitor()
#define SendPkgToMonitor( pPkg, pkgLen )  CToolMonitor::SendPkgToMonitor( pPkg, pkgLen )

#else //TOOL_MONITOR

#define InitToolMonitor() true
#define SendPkgToMonitor( pPkg, pkgLen ) true

#endif //TOOL_MONITOR


