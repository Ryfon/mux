﻿/*
  使用平台组件与监控端通信, by dzj, 09.03.04;
*/

#include "toolmonitor.h"


#ifdef TOOL_MONITOR

MUX_BG_Monitor::Agent_Communicator* CToolMonitor::m_pAgentComm = NULL;

bool CToolMonitor::InitToolMonitor()
{
	m_pAgentComm = get_agent_communicator();
	if ( NULL == m_pAgentComm )
	{
		return false;
	}

	int result = m_pAgentComm->connect();

	if (result < 0)
	{
		return false;
	}

	return true;
}

bool CToolMonitor::SendPkgToMonitor( const char* pPkg, size_t pkgLen )
{
	if ( ( NULL == pPkg ) 
		|| ( pkgLen <= 0 )
		)
	{
		D_WARNING( "CToolMonitor::SendPkgToMonitor, 待发送包空或包长%d错\n", pkgLen );
		return false;
	}
	m_pAgentComm->send( pPkg, pkgLen );
}

#endif //TOOL_MONITOR

