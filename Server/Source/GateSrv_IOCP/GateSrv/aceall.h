﻿/**
* @file aceall.h
* @brief 包含需要的ace头文件；
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: aceall.h
* 摘    要: 包含需要的ace头文件；
* 作    者: dzj
* 完成日期: 2007.11.20
*
*/

#pragma once

#ifdef WIN32
#pragma warning( push )
#pragma warning( disable: 4996 ) 
#endif //WIN32

#include "ace/OS_main.h"

#include "ace/Thread_Manager.h"
#include "ace/Thread_Mutex.h"

#include "ace/Auto_Event.h"
#include "ace/Log_Msg.h"

#include "ace/OS_NS_sys_time.h"
#include "ace/Synch.h"

#include "ace/Guard_T.h"

#include "ace/Message_Block.h"

#include "ace/Reactor.h"

#include "ace/OS.h"

#include "ace/Event_Handler.h"

#include "ace/SOCK_Stream.h"
#include "ace/SOCK_Dgram.h"

#include "ace/INET_Addr.h"

#include "ace/Signal.h"
#include "ace/WFMO_Reactor.h"
#include "ace/Dev_Poll_Reactor.h"
#include "ace/Select_Reactor.h"
#include "ace/ACE.h"

#include "ace/Task.h"
#include "ace/Reactor.h"
#include "ace/SOCK_Acceptor.h"
#include "ace/SOCK_Connector.h"

#include "ace/Time_Value.h" //用于结构中PlayerStateInfo的ACE_Time_Value操作；
#include "ace/OS_NS_sys_time.h"

#include "ace/Get_Opt.h"

#include "ace/Singleton.h"
#include "ace/Null_Mutex.h"

#include "ace/Auto_Ptr.h"

#include "ACEXML/common/DefaultHandler.h"
#include "ACEXML/common/StrCharStream.h"
#include "ACEXML/parser/parser/Parser.h"
#include "ACEXML/common/FileCharStream.h"

#ifdef WIN32
#pragma warning( pop )
#else //WIN32
