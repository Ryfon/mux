﻿
#include "AppBrocast.h"
#include "../Player/Player.h"

CAppBroMsgs* m_pMapBroMsgs = NULL;//地图内广播消息数组；
CAppBroMsgs* m_pUnionBroMsgs = NULL;//战盟内广播消息数组；

//检测是否有某玩家的消息；
bool CAppBroMsgs::CheckPlayerBroMsg( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CheckPlayerBroMsg, NULL == pPlayer\n" );
		return false;
	}

	unsigned int checkID = 0;
	if ( AppBroInfo::APPBRO_MAP == m_broType )
	{
		checkID = pPlayer->GetMapBroCheckID();
	} else if ( AppBroInfo::APPBRO_UNION == m_broType ) {
		checkID = pPlayer->GetUnionBroCheckID();
	} else {
		D_ERROR( "CheckPlayerBroMsg, unknown m_broType %d\n", m_broType );
		return false;
	}

	//比较玩家已测试ID与最新有效ID号，检查是否有该玩家未测试过的待发广播包
	if ( checkID == m_endMsgID )
	{
		//最大多数的情况:自上次测试以来没有新的待广播包；
		return true;
	}

	if ( GetValidEleNum() <= 0 )
	{
		//置玩家新的已测试ID；
		if ( AppBroInfo::APPBRO_MAP == m_broType )
		{
			pPlayer->SetMapBroCheckID( m_endMsgID );
		} else if ( AppBroInfo::APPBRO_UNION == m_broType ) {
			pPlayer->SetUnionBroCheckID( m_endMsgID );
		} else {
			D_ERROR( "CheckPlayerBroMsg, 不可能错误，unknown m_broType %d\n", m_broType );
			return false;
		}
		return true;
	}

	//容器中有待广播且未检测过的包；
	//比较checkID与m_stMsgID以及m_endMsgID决定测试开始位置，
	//  多数情况下:checkID>=m_stMsgID && checkID<m_endMsgID，说明待广播包中有一部分已测试而另一部分未测试
	//  少数情况下:checkID<m_stMsgID或checkID>m_endMsgID，说明之前玩家有漏测包或者玩家第一次执行测试或待发送包队列正好跨越了45和0亿边界(m_endMsgID<m_stMsgID);
	unsigned int chkPosOffset = 0;//测试位置相对于m_stPos的偏移；
	if ( m_endMsgID >= m_stMsgID )
	{
		if (( checkID >= m_stMsgID ) && ( checkID < m_endMsgID ))
		{
			chkPosOffset = checkID - m_stMsgID;
		} else {
			//需要全检测
		}
	} else {
		//消息号跨越了unsigned int边界；
		if ( checkID >= m_stMsgID )
		{
			chkPosOffset = checkID - m_stMsgID;
		} else {
			if ( checkID < m_endMsgID )
			{
				chkPosOffset = 0xffffffff-m_stMsgID + m_endMsgID;
			} else {
				//需要全检测
			}
		}
	}

	unsigned short playerMapID = pPlayer->GetMapID();
	unsigned int   playerUnionID = pPlayer->GetUnionID();
	unsigned int i = m_stPos+chkPosOffset;//起始测试位置；
	if ( m_endPos >= m_stPos )
	{
		//待发数组形式：-----******------，*为待广播包
		for ( i; i<m_endPos; ++i )
		{
			if ( m_arrBroMsgs[i].IsSendInfoAccord( playerMapID, playerUnionID ) )
			{
				//SendBroMsgToPlayer();
			}
		}
	} else {
		//待发数组形式：***--******，*为待广播包
		if ( i >= m_stPos )
		{
			//后半段；
			for ( i; i<m_innerSize; ++i )
			{
				if ( m_arrBroMsgs[i].IsSendInfoAccord( playerMapID, playerUnionID ) )
				{
					//SendBroMsgToPlayer();
				}
			}
			i = 0;//准备接下来遍历前半段;
		}//if ( i >= m_stPos )
		//前半段
		for ( i; i<m_endPos; ++i )
		{
			if ( m_arrBroMsgs[i].IsSendInfoAccord( playerMapID, playerUnionID ) )
			{
				//SendBroMsgToPlayer();
			}
		}//for ( i; i<m_endPos; ++i )
	}//if ( m_endPos >= m_stPos )

	//置玩家已测试ID为最新；
	if ( AppBroInfo::APPBRO_MAP == m_broType )
	{
		pPlayer->SetMapBroCheckID( m_endMsgID );
	} else if ( AppBroInfo::APPBRO_UNION == m_broType ) {
		pPlayer->SetUnionBroCheckID( m_endMsgID );
	} else {
		D_ERROR( "CheckPlayerBroMsg, 不可能错误，unknown m_broType %d\n", m_broType );
		return false;
	}

	return true;
} //bool CAppBroMsgs::CheckPlayerBroMsg( CPlayer* pPlayer )