﻿#ifndef PSEUDO_WIRE_MANAGER_H
#define PSEUDO_WIRE_MANAGER_H


#include "../../Base/aceall.h"
#include <map>
#include <vector>
#include "../../Base/PkgProc/SrvProtocol.h"

using namespace MUX_PROTO;

class CPseudowireManager
{
public:
	CPseudowireManager(void);
	~CPseudowireManager(void);
public:
	//接受切线信息
	void OnRecvPWInfo(PseudowireArg_i *pItems, unsigned char count);
	void OnRecvPWMapInfo(PseudowireMapArg_i *pItems, unsigned char count);

	//伪线状态变化(或者伪线地图人数变化)
	void OnPWStateChange(unsigned short pwid, PseudowireArg_i::ePWState newState);
	void OnPWMapStateChange(unsigned short pwid, unsigned short mapid, PseudowireMapArg_i::ePWMapState newState);
	void OnPWMapPlayerNumChange(unsigned short pwid, unsigned short mapid, unsigned short newPlayerNum);

public:
	//伪线选择信息(登录后的选项)
    void GetSelectPWOption(std::vector<PseudowireData*>& psMap, std::vector<PseudowireMapData*>& pswMap);

	//切线选项(在游戏中切线时的选项)
	void GetShiftPWOption(unsigned short mapid, std::vector<unsigned short>& mapset);

private:
	void Clear(void);

	void AddPWItem(PseudowireArg_i *pItem);
	void AddPWMapItem(PseudowireMapArg_i *pItem);

	PseudowireData* GetPWItem(unsigned short pwid);
	PseudowireMapData* GetPWMapItem(unsigned short pwid, unsigned short mapid);

	//获取包含指定伪线地图的伪线列表
	size_t GetPWBySameMap(unsigned short mapid, std::vector<unsigned short>& mapset);

private:
	std::map<unsigned short, PseudowireData*> m_pwInfo;
	std::map<unsigned int, PseudowireMapData*> m_pwMapInfo;

};

typedef ACE_Singleton<CPseudowireManager, ACE_Null_Mutex> CPseudowireManagerSingle;

#endif/*PSEUDO_WIRE_MANAGER_H*/

