﻿
#pragma once
#include "../../../Base/PkgProc/SrvProtocol.h"
using namespace MUX_PROTO;

#include "../../../Base/PkgProc/DealPkgBase.h"
#include "../../../Base/PkgProc/ShopSvrProtocol.h"

using namespace ShopServer;

#ifdef WIN32
#include <hash_map>
#define HASH_MAP stdext::hash_map
#else
#include <ext/hash_map>
#define HASH_MAP __gnu_cxx::hash_map
#endif

//#define PKG_STAT		//开关数据包日志纪录

#ifdef PKG_STAT
#define SEND_TO_SRV_STAT( cmd, size ) \
	PkgStatManager::SendToSrv( cmd, size );
#else
#define SEND_TO_SRV_STAT( cmd, size )
#endif 

#ifdef PKG_STAT
#define SEND_TO_CLI_STAT( cmd, size ) \
	PkgStatManager::SendToCli( cmd, size );
#else
#define SEND_TO_CLI_STAT( cmd, size )
#endif 

#ifdef PKG_STAT
#define RECV_FROM_SRV_STAT( cmd, size ) \
	PkgStatManager::RecvFromSrv( cmd, size );
#else
#define RECV_FROM_SRV_STAT( cmd, size )
#endif

#ifdef PKG_STAT
#define RECV_FROM_CLI_STAT( cmd, size ) \
	PkgStatManager::RecvFromCli( cmd, size );
#else
#define RECV_FROM_CLI_STAT( cmd, size )
#endif


//用于GateSrv向MapSrv的消息转发
#define  GateSrvSendMsgToMapSrv( TYPE ){\
	CMapSrv* pMapsrv = pOwner->GetMapSrv();\
	if ( pMapsrv ){\
	MsgToPut* pMsg = CreateSrvPkg( TYPE, pMapsrv, srvmsg );\
	pMapsrv->SendPkgToSrv( pMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
	return true;}\
}

//用于GateSrv向Player的消息
#define  GateSrvSendMsgToPlayer( TYPE ){\
	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( srvmsg->lNotiPlayerID.dwPID );\
	if ( pNotiPlayer ){\
	NewSendPlayerPkg( TYPE, pNotiPlayer, climsg );\
	return true;}else{\
	D_DEBUG("SrvErr:向玩家发送消息时,依据ID号找不到玩家:%s\n",#TYPE);\
	return false;}\
}

/*
////用于检验是否要群发
//#define BroadCastSendCheck( TYPE, pPkg){\
//	if ( pOwner->IsBroadCast() ) {\
//	pOwner->BroadcastSend<TYPE>( pPkg );\
//	return true;}\
//}
*/

#define BroadCastSendCheck( TYPE, pPkg)

#ifdef DS_EPOLL
#define DealPlayerPkgPre( OUTTYPE, outPtr, inputPkg, inputLen ) \
	if ( inputLen != sizeof(OUTTYPE) )\
{\
	return false;\
};\
	const OUTTYPE* outPtr = (const OUTTYPE*) inputPkg;
#else  //DS_EPOLL
#define DealPlayerPkgPre( OUTTYPE, outPtr, inputPkg, inputLen ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	StructMemSet( tmpStruct, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)m_cliprotocol.DeCode( OUTTYPE::wCmd, &tmpStruct, outBufLen, inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
{\
	return false;\
}
#endif //DS_EPOLL

#define DealRelationPkgPre( OUTTYPE, outPtr, inputPkg, inputLen ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	StructMemSet( tmpStruct, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)m_relationProtocol.DeCode( OUTTYPE::wCmd, &tmpStruct, outBufLen, inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
{\
	return false;\
}

#define DealNewLoginPkgPre( OUTTYPE, outPtr, inputPkg, inputLen ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	StructMemSet( tmpStruct, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)m_loginProtocol.DeCode( OUTTYPE::wCmd, &tmpStruct, outBufLen, inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
{\
	return false;\
}

#define SendMsgToRelationSrv( TYPE, Pkg )\
{\
	CRelationSrv* pSrv = CManRelationSrvs::GetRelationserver();\
	if( NULL == pSrv )\
{\
	D_ERROR("找不到relationSrv\n");\
	/*return false;*/\
}\
	else\
{\
	MsgToPut* pNewMsg = CreateRelationPkg( TYPE, pSrv, Pkg );\
	pSrv->SendPkgToSrv( pNewMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
}\
}

#define SendMsgToShopSrv( TYPE, Pkg )\
{\
	CShopSrv* pSrv = CManShopSrvs::GetShopserver();\
	if( NULL == pSrv )\
{\
	D_ERROR("找不到shopSrv\n");\
	/*return false;*/\
} else {\
	MsgToPut* pNewMsg = CreateShopPkg( TYPE, pSrv, Pkg );\
	pSrv->SendPkgToSrv( pNewMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
}\
}

#define  SendMsgToFunctionSrv( TYPE,Pkg )\
{\
	CFunctionSrv* pSrv = CManRankSrv::GetRankSrv();\
	if ( NULL == pSrv )\
{\
	/*return false;*/\
}\
	else\
{\
	MsgToPut* pNewMsg = CreateSrvPkg( TYPE, pSrv, Pkg );\
	pSrv->SendPkgToSrv( pNewMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
}\
}

#define SendMsgToCenterSrv( TYPE, Pkg )\
{\
	CCenterSrv* pSrv = CManCenterSrvs::GetCenterserver();\
	if( NULL == pSrv )\
{\
	D_ERROR("(%s:%d)找不到CenterSrv\n", __FILE__, __LINE__ );\
	return false;\
}\
	MsgToPut* pNewMsg = CreateSrvPkg( TYPE, pSrv, Pkg );\
	pSrv->SendPkgToSrv( pNewMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
}

#define SendMsgToNewLoginSrv( TYPE, Pkg )\
{\
	CNewLoginSrv* pNewLoginSrv = CManNewLoginSrv::GetNewLoginSrv();\
	if ( NULL != pNewLoginSrv )\
{\
	MsgToPut* pNewMsg = CreateNewLoginSrvPkg( TYPE, pNewLoginSrv, Pkg );\
	pNewLoginSrv->SendPkgToSrv( pNewMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
}\
}

#define SendMsgToMapSrvByPlayer( TYPE, pPlayer, Pkg ) \
{\
	CMapSrv* pMapSrv = pPlayer->GetMapSrv();\
	if( NULL == pMapSrv )\
{\
	D_ERROR("找不到玩家所在的MAPSRV\n");\
	return false;\
}\
	MsgToPut* pNewMsg = CreateSrvPkg( TYPE, pMapSrv, Pkg );\
	pMapSrv->SendPkgToSrv( pNewMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
}

#define NewSendMsgToLogSrv( TYPE, Pkg )\
{\
	CLogSrv* pSrv = CManLogSrvs::GetLogserver();\
	if( NULL == pSrv )\
{\
	D_ERROR("找不到logSrv\n");\
	/*return false;*/\
}\
	else{\
	MsgToPut* pNewMsg = CreateLogPkg( TYPE, pSrv, Pkg );\
	pSrv->SendPkgToSrv( pNewMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
}\
}

#define SendMsgToSrv( TYPE, pSrv, Pkg ) \
{\
	if ( NULL == pSrv )\
{\
	D_ERROR("找不到SRV\n");\
}\
	else\
{\
	MsgToPut* pNewMsg = CreateSrvPkg( TYPE, pSrv, Pkg );\
	pSrv->SendPkgToSrv( pNewMsg );\
	SEND_TO_SRV_STAT( TYPE::wCmd, sizeof(TYPE) );\
}\
}


#define SAFE_ARRSIZE(arrName, checkValue) \
{\
	UINT arrEleNum = sizeof(arrName) / sizeof(arrName[0]);\
	checkValue = ( checkValue>arrEleNum ) ? arrEleNum : checkValue;\
	checkValue = ( checkValue<=0 ) ? 0 : checkValue;\
}

#define SafeStruct( TYPE, name)\
	TYPE name;\
	StructMemSet( name, 0, sizeof(name) );

#include "../../../Base/PkgProc/MonitorProtocol.h"
#include "../../../Base/PkgProc/LogSvcProtocol.h"

#ifdef TOOL_MONITOR

static MUX_BG_Monitor::Monitor_Protocol g_monitorpro; //监控服务器协议工具；
static LogServiceNS::LogServiceProtocol g_logsrvpro; //日志服务器协议工具；
///往监控发送消息；
template < typename MSG_TYPE >
bool SendMsgToMonitor( MSG_TYPE& msgToMonitor ) 
{
	static char monitorMsgBuf[512];

	TRY_BEGIN;

	if ( msgToMonitor.wCmd != MSG_TYPE::wCmd )
	{
		D_ERROR( "SendMsgToMonitor，传入类型%x与待发送包类型%x不一致", MSG_TYPE::wCmd, msgToMonitor.wCmd );
		return false;
	}

	if (sizeof(MSG_TYPE) > (sizeof(monitorMsgBuf) - 5) )
	{
		D_ERROR( "SendMsgToMonitor，欲发包的包长超过了monitorMsgBuf的内部缓存大小，包类型%x", MSG_TYPE::wCmd );
		return false;
	}

	unsigned short wPkgLen = 0;
	// encode
	int iEnLen = g_monitorpro.EnCode((int)msgToMonitor.wCmd, &msgToMonitor, &(monitorMsgBuf[5]), (int)(sizeof(monitorMsgBuf) - 5));
	if( iEnLen < 0 )
	{
		D_ERROR("SendMsgToMonitor, encode出错，包类型%x\n", msgToMonitor.wCmd);
		return false;
	}

	wPkgLen = iEnLen + 5;
	unsigned char byPkgType = 0;
	unsigned short wCmd = msgToMonitor.wCmd;

	memcpy( &(monitorMsgBuf[0]), &wPkgLen, 2 );//包长：wPkgLen;
	memcpy( &(monitorMsgBuf[2]), &byPkgType, 1 );//包类型：byPkgType;
	memcpy( &(monitorMsgBuf[3]), &wCmd, 2 );//命令字：wCmd;

	SendPkgToMonitor( monitorMsgBuf, wPkgLen );

	return true; 
	TRY_END;
	return false;
};

///往日志服务器发送消息；
template < typename MSG_TYPE >
bool SendMsgToLogSrv( MSG_TYPE& msgToLogSrv )
{
	static char logMsgBuf[512];
	TRY_BEGIN;

	if ( msgToLogSrv.wCmd != MSG_TYPE::wCmd )
	{
		D_ERROR( "SendMsgToLogSrv，传入类型%x与待发送包类型%x不一致", MSG_TYPE::wCmd, msgToLogSrv.wCmd );
		return false;
	}

	if (sizeof(MSG_TYPE) > (sizeof(logMsgBuf) - 5) )
	{
		D_ERROR( "SendMsgToLogSrv，欲发包的包长超过了logMsgBuf的内部缓存大小，包类型%x", MSG_TYPE::wCmd );
		return false;
	}

	unsigned short wPkgLen = 0;
	// encode
	int iEnLen = g_logsrvpro.EnCode((int)msgToLogSrv.wCmd, &msgToLogSrv, &(logMsgBuf[5]), (int)(sizeof(logMsgBuf) - 5));
	if( iEnLen < 0 )
	{
		D_ERROR("SendMsgToLogSrv, encode出错，包类型%x\n", msgToLogSrv.wCmd);
		return false;
	}

	wPkgLen = iEnLen + 5;
	unsigned char byPkgType = 0;
	unsigned short wCmd = msgToLogSrv.wCmd;

	memcpy( &(logMsgBuf[0]), &wPkgLen, 2 );//包长：wPkgLen;
	memcpy( &(logMsgBuf[2]), &byPkgType, 1 );//包类型：byPkgType;
	memcpy( &(logMsgBuf[3]), &wCmd, 2 );//命令字：wCmd;

	SendPkgToMonitor( logMsgBuf, wPkgLen );

	return true; 
	TRY_END;
	return false;

};

#else  //TOOL_MONITOR

template < typename MSG_TYPE >
bool SendMsgToMonitor( MSG_TYPE& msgToMonitor )
{
	ACE_UNUSED_ARG( msgToMonitor );
	//D_ERROR( "未开TOOL_MONITOR时调用SendMsgToMonitor\n" );
	return true;
}


template < typename MSG_TYPE >
bool SendMsgToLogSrv( MSG_TYPE& msgToLogSrv )
{
	ACE_UNUSED_ARG( msgToLogSrv );
	//D_ERROR( "未开TOOL_MONITOR时调用SendMsgToMonitor\n" );
	return true;
}

#endif //TOOL_MONITOR


#define SafeStrCpy2( _Dest, _Src , _Len ){\
	stringstream ss;\
	ss<<_Src;\
	unsigned int _srclen = (unsigned int)ss.str().length();\
	if( _srclen >= sizeof(_Dest) ){\
	D_ERROR("(%s %d ) 的字符串长度超过了规定的安全长度(%d), 实际长度为:%d\n", __FUNCTION__,__LINE__, sizeof(_Dest), _srclen );\
	return false;\
	}\
	_Len = _srclen;\
	SafeStrCpy( _Dest, ss.str().c_str() );\
}

#define DEAL_FUNCTION_MSG_CHECK( MSG_TYPE )\
	if ( pOwner == NULL ){\
	D_ERROR( "%s,pOwner空\n" ,__FUNCTION__ );\
	return false;}\
	if ( wPkgLen != sizeof(MSG_TYPE) ){\
	D_ERROR( "包大小错误:Func:%s MSG:%s \n" ,__FUNCTION__ ,#MSG_TYPE );\
	return false;}\
	TRY_BEGIN;\
	MSG_TYPE*  pMsg = (MSG_TYPE *)pPkg;

#define  DEAL_MAP_MSG_CHECK(MSG_TYPE)\
	if ( pOwner == NULL ){\
	D_ERROR( "%s,pOwner空\n" ,__FUNCTION__ );\
	return false;}\
	if ( wPkgLen != sizeof(MSG_TYPE) ){\
	D_ERROR( "包大小错误:Func:%s MSG:%s \n" ,__FUNCTION__,#MSG_TYPE );\
	return false;}\
	MSG_TYPE*  pMsg = (MSG_TYPE *)pPkg;