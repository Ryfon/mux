﻿#include "DealShopSrvPkg.h"
#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../LogManager.h"


#define DealShopPkgPre( OUTTYPE, outPtr, inputPkg, inputLen ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	StructMemSet( tmpStruct, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)m_ShopProtocol.DeCode( OUTTYPE::wCmd, &tmpStruct, outBufLen, inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
	{\
	return false;\
	}


#define DealLogPkgPre( OUTTYPE, outPtr, inputPkg, inputLen ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	StructMemSet( tmpStruct, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)m_LogProtocol.DeCode( OUTTYPE::wCmd, &tmpStruct, outBufLen, inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
	{\
	return false;\
	}

ShopServer::ShopSrvProtocol CDealShopSrvPkg::m_ShopProtocol;


///初始化(注册各命令字对应的处理函数);
void CDealShopSrvPkg::Init()
{
	Register( S_G_ACCOUNTINFO, &OnAccountInfo );//账户信息
	Register( S_G_PURCHASEITEM, &OnPurchaseItem );//购买物品
	Register( S_G_VIPNIFO, &OnVipInfo );//vip信息
	Register( S_G_PURCHASEVIP, &OnPurchaseVip );//购买vip
	Register( S_G_FETCHVIPITEM, &OnFetchVipItem );//领取vip物品
	Register( S_G_PURCHASEGIFT, &OnPurchaseGift );//赠送
	Register( S_G_FETCHCACHEDITEM, &OnFetchNonVipItem );//领取非vip物品
	Register( S_G_QUERYROLEINFO, &OnCheckRoleByName);//检查角色名字的有效性
	Register( S_G_RANKINFO, &OnQueryShopRankRst );//查询的排行信息返回
	Register( S_G_GUILD_PRESTIGE_ADD, &OnDiamondToPrestigeNotify );//星钻转公会威望
	Register( S_G_NEW_RANKRST, &OnNewRankRst );//新的星钻持有排行；
}

/// 帐户信息
bool CDealShopSrvPkg::OnAccountInfo( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAccountInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGAccountInfo , pShopMsg, pPkg, wPkgLen );
	if( pShopMsg->UserId.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnAccountInfo 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAccountInfo,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCAccountInfo resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.GameMoney = pShopMsg->GameMoney;
	resp.MemberClass = pShopMsg->MemberClass;
	resp.MemberExp = pShopMsg->MemberExp;
	resp.MemberLevel = pShopMsg->MemberLevel;
	resp.PurchaseDisc = pShopMsg->PurchaseDisc;
	resp.Score = pShopMsg->Score;
	resp.Result = pShopMsg->Result;

    NewSendPlayerPkg( GCAccountInfo, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAccountInfo, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

/// 购买物品
bool CDealShopSrvPkg::OnPurchaseItem( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnPurchaseItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGPurchaseItem , pShopMsg, pPkg, wPkgLen );
	if( pShopMsg->UserId.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnPurchaseItem 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPurchaseItem,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCPurchaseItem resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.TransID = pShopMsg->TransID;
	resp.ItemID = pShopMsg->ItemID;
	resp.ItemNum = pShopMsg->ItemNum;
	resp.GameMoney = pShopMsg->GameMoney;
	resp.MemberClass = pShopMsg->MemberClass;
	resp.MemberExp = pShopMsg->MemberExp;
	resp.MemberLevel = pShopMsg->MemberLevel;
	resp.PurchaseDisc = pShopMsg->PurchaseDisc;
	resp.Score = pShopMsg->Score;
	resp.PurchaseResult = pShopMsg->PurchaseResult;

	NewSendPlayerPkg( GCPurchaseItem, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPurchaseItem, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	if(pShopMsg->PurchaseResult == 0)//购买成功则直接放到到包中
	{
		unsigned int diamond = pShopMsg->PurchaseType == 0 ? pShopMsg->ComsumeNum : 0; //购买类型:星钻
		unsigned int credit = pShopMsg->PurchaseType == 1 ? pShopMsg->ComsumeNum : 0;//购买类型:积分
		CLogManager::DoMarketPruchase(pNotifyPlayer, diamond, credit, pShopMsg->ItemID, pShopMsg->ItemNum);//记日至

		//转发给mapserver，放到包裹中去
		GMFetchNonVipItem gmFetchNonVipItem;
		StructMemSet(gmFetchNonVipItem, 0x00,sizeof(gmFetchNonVipItem));

		gmFetchNonVipItem.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
		gmFetchNonVipItem.itemID = pShopMsg->ItemID;
		gmFetchNonVipItem.itemNum = pShopMsg->ItemNum;
		gmFetchNonVipItem.tranid = pShopMsg->TransID;
		gmFetchNonVipItem.diamond = diamond;
		
		SendMsgToMapSrvByPlayer(GMFetchNonVipItem, pNotifyPlayer, gmFetchNonVipItem);

		//购买作为好友动态记录
		AddTweet addTweet;
		addTweet.gateId = pNotifyPlayer->GetPlayerID().wGID;
		addTweet.playerId = pNotifyPlayer->GetPlayerID().dwPID;
		addTweet.tweet.createdTime = (UINT)ACE_OS::time(NULL);
		addTweet.tweet.type = MUX_PROTO::TWEET_TYPE_SHOPPING;
		unsigned int temp[2] = {0,0};
		temp[0] = pShopMsg->ItemID;
		temp[1] = pShopMsg->ItemNum;
		StructMemCpy(addTweet.tweet.tweet, temp ,sizeof(temp));
		addTweet.tweet.tweetLen = sizeof(temp);

		SendMsgToRelationSrv(AddTweet, addTweet);
	}

	return true;

	TRY_END;
	return false;
}

///vip信息
bool CDealShopSrvPkg::OnVipInfo( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnVipInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGVipInfo , pShopMsg, pPkg, wPkgLen );
	if( pShopMsg->UserId.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnVipInfo 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnVipInfo,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCVipInfo resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.Fetched = pShopMsg->Fetched;
	resp.LeftDays = pShopMsg->LeftDays;
	resp.Result = pShopMsg->Result;
	resp.VipID = pShopMsg->VipID;

	NewSendPlayerPkg( GCVipInfo, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCVipInfo, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

/// 购买vip
bool CDealShopSrvPkg::OnPurchaseVip( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnPurchaseVip,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGPurchaseVip , pShopMsg, pPkg, wPkgLen );
	if( pShopMsg->UserId.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnPurchaseVip 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPurchaseVip,找不到通知的玩家\n");
		return false;
	}

	if(pShopMsg->PurchaseResult == 0)//购买成功
		CLogManager::DoMarketPruchase(pNotifyPlayer, pShopMsg->ComsumeNum, 0, pShopMsg->VipID, 0);//记日至
		
	//发送给客户端
	GCPurchaseVip resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.GameMoney =pShopMsg->GameMoney;
	resp.MemberClass = pShopMsg->MemberClass;
	resp.MemberExp = pShopMsg->MemberExp;
	resp.MemberLevel = pShopMsg->MemberLevel;
	resp.PurchaseDisc = pShopMsg->PurchaseDisc;
	resp.PurchaseResult = pShopMsg->PurchaseResult;
	resp.Score = pShopMsg->Score;
	resp.TransID = pShopMsg->TransID;
	resp.VipID = pShopMsg->VipID;

	NewSendPlayerPkg( GCPurchaseVip, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPurchaseVip, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

/// 领取vip物品
bool CDealShopSrvPkg::OnFetchVipItem( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnFetchVipItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGFetchVipItem , pShopMsg, pPkg, wPkgLen );
	if( pShopMsg->UserId.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnFetchVipItem 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnFetchVipItem,找不到通知的玩家\n");
		return false;
	}

	if(pShopMsg->Result != 0)//失败直接返回
	{
		//发送给客户端
		GCFetchVipItem resp;
		StructMemSet(resp, 0x00, sizeof(resp));

		resp.Result = pShopMsg->Result;
		resp.VipID = pShopMsg->VipID;
		resp.VipItemLen = (pShopMsg->VipItemLen <= ARRAY_SIZE(resp.VipItemArr)) ? pShopMsg->VipItemLen : ARRAY_SIZE(resp.VipItemArr);
		StructMemCpy( resp.VipItemArr, pShopMsg->VipItemArr, resp.VipItemLen*sizeof(resp.VipItemArr[0]) );

		NewSendPlayerPkg( GCFetchVipItem, pNotifyPlayer, resp );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCFetchVipItem, pNotifyPlayer, resp );
		//pNotifyPlayer->SendPkgToPlayer(pNewMsg);
	}
	else
	{
		//转发给mapserver，放到包裹中去
		GMFetchVipItem gmFetchVipItem;
		StructMemSet(gmFetchVipItem, 0x00,sizeof(gmFetchVipItem));

		if(pShopMsg->VipItemLen > sizeof(gmFetchVipItem.itemArr)/sizeof(gmFetchVipItem.itemArr[0]))
		{
			D_ERROR("CDealShopSrvPkg::OnFetchVipItem时超过领取最大数目\n");
			return true;
		}

		gmFetchVipItem.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
		gmFetchVipItem.arrNum = pShopMsg->VipItemLen;
		gmFetchVipItem.vipID = pShopMsg->VipID;
		if(gmFetchVipItem.arrNum > 0)
		{
			for(int i = 0; i < gmFetchVipItem.arrNum; i++)
			{
				if ( ( i >= ARRAY_SIZE(gmFetchVipItem.itemArr) )
					|| ( i >= ARRAY_SIZE(pShopMsg->VipItemArr) )
					)
				{
					D_ERROR( "CDealShopSrvPkg::OnFetchVipItem, i(%d)越界，gmFetchVipItem.itemArr或pShopMsg->VipItemArr\n", i );
					return true;
				}
				gmFetchVipItem.itemArr[i].ItemTypeID = pShopMsg->VipItemArr[i].ItemTypeID;
				gmFetchVipItem.itemArr[i].ItemNum = pShopMsg->VipItemArr[i].ItemNum;
			}
		}

		SendMsgToMapSrvByPlayer(GMFetchVipItem, pNotifyPlayer, gmFetchVipItem);
	}

	return true;

	TRY_END;
	return false;
}

/// 赠送
bool CDealShopSrvPkg::OnPurchaseGift( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnPurchaseGift,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGPurchaseGift , pShopMsg, pPkg, wPkgLen );
	if( pShopMsg->UserId.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnPurchaseGift 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPurchaseGift,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCPurchaseGift resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.PurchaseResult = pShopMsg->PurchaseResult;
	resp.ItemID = pShopMsg->ItemID;
	resp.ItemNum = pShopMsg->ItemNum;
	resp.DestAccountLen = (pShopMsg->DestAccountLen <= sizeof(resp.DestAccount)) ? pShopMsg->DestAccountLen :sizeof(resp.DestAccount);
	SafeStrCpy( resp.DestAccount, pShopMsg->DestAccount );//, resp.DestAccountLen);
	resp.DestRoleID = pShopMsg->DestRoleID;
	resp.GameMoney =pShopMsg->GameMoney;
	resp.Score = pShopMsg->Score;
	resp.MemberClass = pShopMsg->MemberClass;
	resp.MemberExp = pShopMsg->MemberExp;
	resp.MemberLevel = pShopMsg->MemberLevel;
	resp.PurchaseDisc = pShopMsg->PurchaseDisc;
	resp.NoteLen = (BYTE)(pShopMsg->NoteLen <= sizeof(resp.Note) ? pShopMsg->NoteLen : sizeof(resp.Note));
	StructMemCpy( resp.Note, pShopMsg->Note, resp.NoteLen );

	NewSendPlayerPkg( GCPurchaseGift, pNotifyPlayer, resp );
	
	if(pShopMsg->PurchaseResult == 0)//购买成功发送邮件
	{
		CLogManager::DoMarketPruchase(pNotifyPlayer, pShopMsg->MoneyCost, pShopMsg->SocreCost, pShopMsg->ItemID, pShopMsg->ItemNum);//记日至

		GFSendSysMailToPlayer sendMail;
		StructMemSet(sendMail, 0x00, sizeof(sendMail));

		stringstream ss;
		ss<<"来自你朋友"<< pNotifyPlayer->GetRoleName() <<"的礼物";
		if ( ss.str().length() >= sizeof(sendMail.sendMailTitle) )
		{
			D_ERROR("OnPurchaseGift mail_title 长度错误\n");
			return false;
		}
		
		SafeStrCpy( sendMail.sendMailTitle, ss.str().c_str() );
		//ACE_OS::snprintf(sendMail.sendMailTitle, sizeof(sendMail.sendMailTitle), "来自你朋友%s的礼物", pNotifyPlayer->GetRoleName());
		SafeStrCpy( sendMail.recvPlayerName,  pShopMsg->DestAccount );
		SafeStrCpy( sendMail.sendMailContent, pShopMsg->Note );
		sendMail.attachItem[0].ucCount = pShopMsg->ItemNum;
		sendMail.attachItem[0].usItemTypeID = pShopMsg->ItemID;
		sendMail.silverMoney = 0;
		sendMail.goldMoney = 0;

		SendMsgToFunctionSrv( GFSendSysMailToPlayer, sendMail );
	}

	return true;

	TRY_END;
	return false;
}

/// 领取非vip物品
bool CDealShopSrvPkg::OnFetchNonVipItem( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnFetchNonVipItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGFetchCachedItem , pShopMsg, pPkg, wPkgLen );
	if( pShopMsg->UserId.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnFetchNonVipItem 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnFetchNonVipItem,找不到通知的玩家\n");
		return false;
	}

	if(/*pShopMsg->Result*/0 != 0)//领取失败直接返回
	{
		//发送给客户端
		GCFetchNonVipItem resp;
		StructMemSet(resp, 0x00, sizeof(resp));

		resp.itemId.ItemID = pShopMsg->CachedItem.ItemID;
		resp.Result = pShopMsg->Result;

		NewSendPlayerPkg( GCFetchNonVipItem, pNotifyPlayer, resp );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCFetchNonVipItem, pNotifyPlayer, resp );
		//pNotifyPlayer->SendPkgToPlayer(pNewMsg);
	}
	else
	{
		//转发给mapserver，放到包裹中去
		GMFetchNonVipItem gmFetchNonVipItem;
		StructMemSet(gmFetchNonVipItem, 0x00,sizeof(gmFetchNonVipItem));

		gmFetchNonVipItem.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
		gmFetchNonVipItem.itemID = pShopMsg->CachedItem.ItemID;
		gmFetchNonVipItem.itemNum = pShopMsg->CachedItem.ItemNum;
		gmFetchNonVipItem.tranid = pShopMsg->CachedItem.TransID;

		SendMsgToMapSrvByPlayer(GMFetchNonVipItem, pNotifyPlayer, gmFetchNonVipItem);
	}

	return true;

	TRY_END;
	return false;
}

//Register( S_G_NEW_RANKRST, &OnNewRankRst );//新的星钻持有排行；
bool CDealShopSrvPkg::OnNewRankRst( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnNewRankRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGNewRankRst, pShopRank, pPkg, wPkgLen );

	if ( pShopRank->rankCount >= ARRAY_SIZE(pShopRank->rankInfoArr) )
	{
		D_ERROR( "OnNewRankRst,消息中错误的rankCount%d\n", pShopRank->rankCount );
		return false;
	}

	GFOtherRank gfMsg;
	gfMsg.rankInfo.rankCount = pShopRank->rankCount;
	StructMemCpy( gfMsg.rankInfo.rankInfoArr, pShopRank->rankInfoArr, sizeof(gfMsg.rankInfo.rankInfoArr) );
	gfMsg.rankInfo.rankType = RANK_STRDIAM;
	gfMsg.rankInfo.selfVal = pShopRank->selfVal;
	gfMsg.rankInfo.startEndFlag = pShopRank->startEndFlag;

	if ( ( 0 == pShopRank->userId.wGID ) && ( 0 == pShopRank->userId.dwPID ) )
	{
		//通知functionsrv;
		SendMsgToFunctionSrv( GFOtherRank, gfMsg );
	} else {
		//通知玩家；
		CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pShopRank->userId.dwPID );
		if( NULL == pNotifyPlayer )
		{
			D_ERROR( "OnNewRankRst,找不到欲通知的玩家%d:%d\n", pShopRank->userId.wGID, pShopRank->userId.dwPID );
			return false;
		}
		NewSendPlayerPkg( GCNewRankInfo, pNotifyPlayer, gfMsg.rankInfo );
	}

	return true;
	TRY_END;
	return false;
}

bool CDealShopSrvPkg::OnQueryShopRankRst(CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnQueryShopRankRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGShopRank , pShopMsg, pPkg, wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnQueryShopRankRst,找不到通知的玩家\n");
		return false;
	}

	if ( pShopMsg->RanksLen > (sizeof(pShopMsg->RankList)/sizeof(pShopMsg->RankList[0])) )
	{
		D_ERROR("CDealShopSrvPkg::OnQueryShopRankRst 接到一个非法长度的消息, pShopMsg->RanksLen=%d\n", pShopMsg->RanksLen);
		return false;
	}
	else if(pShopMsg->RanksLen > 2* MAX_CHARTPAGE_LEN)
	{
		D_ERROR("CDealShopSrvPkg::OnQueryShopRankRst 接到一个大于2倍MAX_CHARTPAGE_LEN的消息, pShopMsg->RanksLen=%d\n", pShopMsg->RanksLen);
		return false;
	}
	else if ( pShopMsg->RanksLen > MAX_CHARTPAGE_LEN )//如果需要分页发送
	{
		//发送前半页
		GCRankInfoByPageRst rankInfoRst1;
		StructMemSet( rankInfoRst1 ,0x0, sizeof(GCRankInfoByPageRst) );
		rankInfoRst1.rankCount     = MAX_CHARTPAGE_LEN;
		rankInfoRst1.rankPageIndex = (char)( pShopMsg->PageIndex & 0x7F );//后面的7位表示页数,查询的页数
		for ( int i = 0 ; i< rankInfoRst1.rankCount ; i++ )
		{
			if ( ( i >= ARRAY_SIZE(rankInfoRst1.rankInfoArr) )
				|| ( i >= ARRAY_SIZE(pShopMsg->RankList) )
				)
			{
				D_ERROR( "CDealShopSrvPkg::OnQueryShopRankRst, i(%d)越界1，rankInfoRst1.rankInfoArr或pShopMsg->RankList\n", i );
				return false;
			}
			ConvertShopRankInfoToCliRankInfo( rankInfoRst1.rankInfoArr[i], pShopMsg->RankList[i] );
		}
		NewSendPlayerPkg( GCRankInfoByPageRst, pNotifyPlayer, rankInfoRst1 );

		//发送后半页
		GCRankInfoByPageRst rankInfoRst2;
		StructMemSet( rankInfoRst2, 0x0, sizeof(GCRankInfoByPageRst) );
		rankInfoRst2.rankCount     =  pShopMsg->RanksLen - MAX_CHARTPAGE_LEN ;
		rankInfoRst2.rankPageIndex =  (char)( pShopMsg->PageIndex & 0x7F );//后面的7位表示页数
		rankInfoRst2.pageEnd       =  ((pShopMsg->PageIndex & 0x80 ) == 0 )?0x1:0x0;//是否是最后一页
		for ( int i = 0 ; i< rankInfoRst2.rankCount ; i++ )
		{
			if ( ( i >= ARRAY_SIZE(rankInfoRst2.rankInfoArr) )
				|| ( MAX_CHARTPAGE_LEN+i >= ARRAY_SIZE(pShopMsg->RankList) )
				)
			{
				D_ERROR( "CDealShopSrvPkg::OnQueryShopRankRst, i(%d)越界2，rankInfoRst2.rankInfoArr或pShopMsg->RankList\n", i );
				return false;
			}
			ConvertShopRankInfoToCliRankInfo( rankInfoRst2.rankInfoArr[i], pShopMsg->RankList[MAX_CHARTPAGE_LEN+i]  );
		}

		NewSendPlayerPkg( GCRankInfoByPageRst, pNotifyPlayer, rankInfoRst2 );
	}
	else
	{
		GCRankInfoByPageRst rankInfoRst;
		StructMemSet( rankInfoRst ,0x0, sizeof(GCRankInfoByPageRst) );
		rankInfoRst.rankCount     =  pShopMsg->RanksLen;
		rankInfoRst.rankPageIndex = (char)( pShopMsg->PageIndex & 0x7F );//后面的7位表示页数
		rankInfoRst.pageEnd       = ((pShopMsg->PageIndex & 0x80 ) == 0 )?0x1:0x0;//是否是最后一页
		for ( int i = 0 ; i< rankInfoRst.rankCount ; i++ )
		{
			if ( ( i >= ARRAY_SIZE(rankInfoRst.rankInfoArr) )
				|| ( i >= ARRAY_SIZE(pShopMsg->RankList) )
				)
			{
				D_ERROR( "CDealShopSrvPkg::OnQueryShopRankRst, i(%d)越界3，rankInfoRst.rankInfoArr或pShopMsg->RankList\n", i );
				return false;
			}
			ConvertShopRankInfoToCliRankInfo( rankInfoRst.rankInfoArr[i], pShopMsg->RankList[i] );
		}

		NewSendPlayerPkg( GCRankInfoByPageRst, pNotifyPlayer, rankInfoRst );
	}

	return true;

	TRY_END;
	return false;
}

bool CDealShopSrvPkg::OnCheckRoleByName(  CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnCheckRoleByName,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGQryRoleInfo , pShopMsg, pPkg, wPkgLen );

	CDbSrv* pDbSrv = CManDbSrvs::GetValidDBSrv();
	if( NULL == pDbSrv )
	{
		D_ERROR("OnCheckRoleByName,pDbSrv空\n");
		return false;
	}

	GDQueryPlayerExist querySrv;
	StructMemSet(querySrv, 0x00, sizeof(querySrv));
	querySrv.queryType = E_QUERY_SHOP;
	SafeStrCpy( querySrv.recvPlayerName, pShopMsg->RoleName );
	//编译警告去除，strncpy( querySrv.recvPlayerName, pShopMsg->RoleName, sizeof(querySrv.recvPlayerName) );

	//MsgToPut* pqueryRole = CreateSrvPkg( GDQueryPlayerExist, pDbSrv, querySrv );
	//pDbSrv->SendPkgToSrv( pqueryRole );
	SendMsgToSrv( GDQueryPlayerExist, pDbSrv, querySrv );

	return true;

	TRY_END;
	return false;
}

bool CDealShopSrvPkg::ConvertShopRankInfoToCliRankInfo(RankChartItem& clirankinfo, const RankInfo& shoprankinfo )
{
	stringstream ss;
	ss<<shoprankinfo.ClassID;
	SafeStrCpy( clirankinfo.itemClass,ss.str().c_str() );//职业
	//ACE_OS::snprintf( clirankinfo.itemClass, sizeof(clirankinfo.itemClass), "%d", shoprankinfo.ClassID );//职业
	SafeStrCpy( clirankinfo.itemName, shoprankinfo.RoleName );//姓名
	clirankinfo.itemNameLen  = (char)shoprankinfo.RoleNameLen;//姓名的长度
	clirankinfo.itemClassLen = 1;//种族的长度
	clirankinfo.itemRace     = (char)shoprankinfo.RaceID;//种族
	clirankinfo.itemLevel    = (int)shoprankinfo.Level;//等级
	clirankinfo.rankInfo     = (int)shoprankinfo.Cost;//花费
	return true;
}

bool CDealShopSrvPkg::OnDiamondToPrestigeNotify( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnDiamondToPrestigeNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealShopPkgPre(SGGuildPrestigeAdd , pShopMsg, pPkg, wPkgLen );
	if( pShopMsg->UserId.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnDiamondToPrestigeNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShopMsg->UserId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDiamondToPrestigeNotify,找不到通知的玩家\n");
		return false;
	}
	
	GMDiamondToPrestigeNtf ntf;
	StructMemSet(ntf, 0x00, sizeof(ntf));
	ntf.notiPlayerID = pNotifyPlayer->GetPlayerID();
	ntf.prestige = pShopMsg->PrestigeAdd;
	SendMsgToMapSrvByPlayer(GMDiamondToPrestigeNtf, pNotifyPlayer, ntf);

	return true;

	TRY_END;
	return false;
}


LoginServiceNS::LoginServiceProtocol CDealLogSrvPkg::m_LogProtocol;

///初始化(注册各命令字对应的处理函数);
void CDealLogSrvPkg::Init()
{
}
