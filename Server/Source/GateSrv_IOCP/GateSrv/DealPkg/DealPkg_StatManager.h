﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once



class PkgStatManager
{
private:
	PkgStatManager();
	virtual ~PkgStatManager();
public:
	static void Init();	//时间初始化
	static void RecvFromCli( unsigned int pkgCmd, unsigned int pkgSize );
	static void RecvFromSrv( unsigned int pkgCmd, unsigned int pkgSize );
	static void SendToCli( unsigned int pkgCmd, unsigned int pkgSize );
	static void SendToSrv( unsigned int pkgCmd, unsigned int pkgSize );

	enum
	{
		RECV_FROM_CLI	= 1,
		RECV_FROM_SRV	= 2,
		SEND_TO_CLI		= 4,
		SEND_TO_SRV		= 8,
	};
	static void LogStatResult(unsigned int type = 0xffff);
private:
	struct PkgStatInfo
	{
		unsigned int pkgCmd;//包命令字
		unsigned int pkgSize;//包大小
		unsigned int pkgNum;//包数量
	};
	static void DealPkgStat( unsigned int pkgCmd, unsigned int pkgSize, HASH_MAP<unsigned int, PkgStatInfo> & pkgStat);
	static void LogStatResult( const HASH_MAP<unsigned int, PkgStatInfo> & pkgStat);
	static HASH_MAP<unsigned int, PkgStatInfo> m_recvFromCli;
	static HASH_MAP<unsigned int, PkgStatInfo> m_recvFromSrv;
	static HASH_MAP<unsigned int, PkgStatInfo> m_sendToCli;
	static HASH_MAP<unsigned int, PkgStatInfo> m_sendToSrv;
	static unsigned int		m_start;
};