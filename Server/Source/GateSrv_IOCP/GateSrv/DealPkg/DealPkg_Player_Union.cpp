﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"

bool CDealPlayerPkg::OnCreateUnion( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnCreateUnion,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGCreateUnionRequest, pReq, pPkg, wPkgLen );

	GMForwardUnionCmd msg;
	StructMemSet(msg, 0x00, sizeof(msg));

	msg.lNotiPlayerID = pOwner->GetPlayerID();
	msg.cmdID = GCUnionUISelectCmd::CREATE_UNION;
	if(pReq->nameLen < sizeof(msg.createUnion.name))
	{
		StructMemCpy(msg.createUnion.name, pReq->name, pReq->nameLen);
		msg.createUnion.nameLen = pReq->nameLen;
	} else {
		StructMemCpy(msg.createUnion.name, pReq->name, sizeof(msg.createUnion.name));
		msg.createUnion.nameLen = sizeof(msg.createUnion.name);
	}

	SendMsgToMapSrvByPlayer(GMForwardUnionCmd, pOwner, msg);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnDestroyUnion( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnDestroyUnion,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDestroyUnionRequest, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	DestroyUnionRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid= pOwner->GetSeledRole();

	SendMsgToRelationSrv(DestroyUnionRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryUnionBasicInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnQueryUnionBasicInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryUnionBasicRequest, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	QueryUnionBasicRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();

	SendMsgToRelationSrv(QueryUnionBasicRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryUnionMemberInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnQueryUnionMemberInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryUnionMembersRequest, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	QueryUnionMembersRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();

	SendMsgToRelationSrv(QueryUnionMembersRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnAddUnionMemberReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddUnionMemberReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddUnionMemberRequest, pReq, pPkg, wPkgLen );

	AddUnionMemberRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid  = pOwner->GetSeledRole();
	if(pReq->nameLen < sizeof(srvmsg.memberAddedName))
	{
		SafeStrCpy(srvmsg.memberAddedName, pReq->name);//, pReq->nameLen);
		srvmsg.memberAddedNameLen = pReq->nameLen;
	} else {
		SafeStrCpy(srvmsg.memberAddedName, pReq->name);//, sizeof(srvmsg.memberAddedName));
		srvmsg.memberAddedNameLen = sizeof(srvmsg.memberAddedName);	
	}

	SendMsgToRelationSrv(AddUnionMemberRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnConfirmUnionMemberAddReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnConfirmUnionMemberAddReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddUnionMemberConfirmed, pReq, pPkg, wPkgLen );

	AddUnionMemberConfirmed srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.flag = pReq->flag;
	srvmsg.sponsorLen = (pReq->sponsorLen < sizeof(srvmsg.sponsor)) ? pReq->sponsorLen : sizeof(srvmsg.sponsor);
	StructMemCpy(srvmsg.sponsor, pReq->sponsor, srvmsg.sponsorLen);
	srvmsg.unionNameLen = (pReq->unionNameLen < sizeof(srvmsg.unionName)) ? pReq->unionNameLen : sizeof(srvmsg.unionName);
	StructMemCpy(srvmsg.unionName,  pReq->unionName, srvmsg.unionNameLen);
	srvmsg.uiid = pOwner->GetSeledRole();

	SendMsgToRelationSrv(AddUnionMemberConfirmed, srvmsg);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnRemoveUnionMemberReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRemoveUnionMemberReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGRemoveUnionMemberRequest, pReq, pPkg, wPkgLen );

	RemoveUnionMemberRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.memberRemoved = pReq->memberRemoved;

	SendMsgToRelationSrv(RemoveUnionMemberRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 修改工会职位对应权限
bool CDealPlayerPkg::OnModifyUnionPosRightReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnModifyUnionPosRightReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGModifyUnionPosRighstReq, pReq, pPkg, wPkgLen );

	ModifyUnionPosRighstRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.rights = pReq->rights;
	if(pReq->posLen < sizeof(srvmsg.pos))
	{
		srvmsg.posLen = pReq->posLen;
		StructMemCpy(srvmsg.pos, pReq->pos, pReq->posLen);
	} else {
		srvmsg.posLen = sizeof(srvmsg.pos);
		StructMemCpy(srvmsg.pos, pReq->pos, sizeof(srvmsg.pos));
	}

	//SendMsgToRelationSrv(ModifyUnionPosRighstRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 增加工会职位
bool CDealPlayerPkg::OnAddUnionPosReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddUnionPosReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddUnionPosReq, pReq, pPkg, wPkgLen );

	AddUnionPosRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.rights = pReq->rights;
	if(pReq->posLen < sizeof(srvmsg.pos))
	{
		srvmsg.posLen = pReq->posLen;
		StructMemCpy(srvmsg.pos, pReq->pos, pReq->posLen);
	} else {
		srvmsg.posLen = sizeof(srvmsg.pos);
		StructMemCpy(srvmsg.pos, pReq->pos, sizeof(srvmsg.pos));
	}

	//SendMsgToRelationSrv(AddUnionPosRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 减少工会职位
bool CDealPlayerPkg::OnRemoveUnionPosReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRemoveUnionPosReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGRemoveUnionPosReq, pReq, pPkg, wPkgLen );

	RemoveUnionPosRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	if(pReq->posLen < sizeof(srvmsg.pos))
	{
		srvmsg.posLen = pReq->posLen;
		StructMemCpy(srvmsg.pos, pReq->pos, pReq->posLen);
	} else {
		srvmsg.posLen = sizeof(srvmsg.pos);
		StructMemCpy(srvmsg.pos, pReq->pos, sizeof(srvmsg.pos));
	}

	//SendMsgToRelationSrv(RemoveUnionPosRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 转移会长职位
bool CDealPlayerPkg::OnTransformUnionCaptionReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnTransformUnionCaptionReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGTransformUnionCaptionReq, pReq, pPkg, wPkgLen );

	TransformUnionCaptionRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.newCaptionUiid = pReq->newCaptionUiid;
	/*if(pReq->newCaptionNameLen < sizeof(srvmsg.newCaptionName))
	{
	srvmsg.newCaptionNameLen = pReq->newCaptionNameLen;
	StructMemCpy(srvmsg.newCaptionName, pReq->newCaptionName, pReq->newCaptionNameLen);
	}
	else
	{
	srvmsg.newCaptionNameLen = sizeof(srvmsg.newCaptionName);
	StructMemCpy(srvmsg.newCaptionName, pReq->newCaptionName, sizeof(srvmsg.newCaptionName));
	}*/

	SendMsgToRelationSrv(TransformUnionCaptionRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 修改工会成员称号
bool CDealPlayerPkg::OnModifyUninMemberTitleReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnModifyUninMemberTitleReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGModifyUnionMemberTitleReq, pReq, pPkg, wPkgLen );

	ModifyUnionMemberTitleRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.modifiedUiid = pReq->modifiedUiid;
	if(pReq->titleLen < sizeof(srvmsg.title))
	{
		srvmsg.titleLen = pReq->titleLen;
		StructMemCpy(srvmsg.title, pReq->title, pReq->titleLen);
	}
	else
	{
		srvmsg.titleLen = sizeof(srvmsg.title);
		StructMemCpy(srvmsg.title, pReq->title, sizeof(srvmsg.title));
	}

	SendMsgToRelationSrv(ModifyUnionMemberTitleRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 提升工会成员职位
bool CDealPlayerPkg::OnAdvanceUnionMemberPosReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAdvanceUnionMemberPosReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAdvanceUnionMemberPosReq, pReq, pPkg, wPkgLen );

	AdvanceUnionMemberPosRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.advancedUiid = pReq->advancedUiid;

	SendMsgToRelationSrv(AdvanceUnionMemberPosRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 降低工会成员职位
bool CDealPlayerPkg::OnReduceUninMemberposReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnReduceUninMemberposReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGReduceUnionMemberPosReq, pReq, pPkg, wPkgLen );

	ReduceUnionMemberPosRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.reducedUiid = pReq->reducedUiid;

	SendMsgToRelationSrv(ReduceUnionMemberPosRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 工会频道聊天
bool CDealPlayerPkg::OnUnionChannelSpeekReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnUnionChannelSpeekReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGUnionChannelSpeekReq, pReq, pPkg, wPkgLen );

	UnionChannelSpeekRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	if(pReq->contentLen < sizeof(srvmsg.content))
	{
		srvmsg.contentLen = pReq->contentLen;
		StructMemCpy(srvmsg.content, pReq->content, pReq->contentLen);
	}
	else
	{
		srvmsg.contentLen = sizeof(srvmsg.content);
		StructMemCpy(srvmsg.content, pReq->content, sizeof(srvmsg.content));
	}

	SendMsgToRelationSrv(UnionChannelSpeekRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 更新工会职位管理
bool CDealPlayerPkg::OnUpdateUnionPosCfgReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnUpdateUnionPosCfgReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGUpdateUnionPosCfgReq, pReq, pPkg, wPkgLen );

	UpdateUnionPosCfgRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	if(0 == pReq->posListLen)
	{
		srvmsg.posListLen = 0;
		SendMsgToRelationSrv(UpdateUnionPosCfgRequest, srvmsg);
	}
	else
	{
		int totalListSize = pReq->posListLen;
		int maxSegmentSize = ARRAY_SIZE(srvmsg.posList);//sizeof(srvmsg.posList)/sizeof(srvmsg.posList[0]);

		int count = 0;
		for(int i = 0; i < totalListSize; i++)
		{
			if ( ( count >= ARRAY_SIZE(srvmsg.posList) )
				|| ( i >= ARRAY_SIZE(pReq->posList) )
				)
			{
				D_ERROR( "OnUpdateUnionPosCfgReq, count(%d)或i(%d)越界\n", count, i );
				return false;
			}
			//数据拷贝
			srvmsg.posList[count].right = pReq->posList[i].right;
			srvmsg.posList[count].seq = pReq->posList[i].seq;
			if(pReq->posList[i].posLen < sizeof(srvmsg.posList[count].pos))
			{
				srvmsg.posList[count].posLen = pReq->posList[i].posLen;
				StructMemCpy(srvmsg.posList[count].pos, pReq->posList[i].pos, pReq->posList[i].posLen);
			} else {
				srvmsg.posList[count].posLen = sizeof(srvmsg.posList[count].pos);
				StructMemCpy(srvmsg.posList[count].pos, pReq->posList[i].pos, sizeof(srvmsg.posList[count].pos));
			}

			//计数
			count++;

			//count=maxsegmentsize或者i=totalListSize-1时发送
			if((count == maxSegmentSize) || (totalListSize -1 == i))
			{
				srvmsg.posListLen = count;
				SendMsgToRelationSrv(UpdateUnionPosCfgRequest, srvmsg);

				count = 0;
			}	
		}
	}

	return true;
	TRY_END;
	return false;
}

// 工会成员禁言
bool CDealPlayerPkg::OnForbidUnionSpeekReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnForbidUnionSpeekReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGForbidUnionSpeekReq, pReq, pPkg, wPkgLen );

	ForbidUnionSpeekRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.forbidUiid = pReq->forbidUiid;


	SendMsgToRelationSrv(ForbidUnionSpeekRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

// 发布工会任务
bool CDealPlayerPkg::OnIssueUnionTaskReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnIssueUnionTaskReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGIssueUnionTaskReq, pReq, pPkg, wPkgLen );

	/*PostUnionTasksListRequest climsg;
	StructMemSet(climsg, 0x00, sizeof(climsg));

	climsg.durationTime = pReq->taskValidInterval;
	climsg.uiid = pOwner->GetSeledRole();

	SendMsgToRelationSrv(PostUnionTasksListRequest, climsg);*/


	GMForwardUnionCmd msg;
	StructMemSet(msg, 0x00, sizeof(msg));

	msg.lNotiPlayerID = pOwner->GetPlayerID();
	msg.cmdID = GCUnionUISelectCmd::ISSUE_UNION_TASK;
	msg.issueTask.taskValidInterval = pReq->taskValidInterval;

	SendMsgToMapSrvByPlayer(GMForwardUnionCmd, pOwner, msg);

	return true;
	TRY_END;
	return false;
}

// 学习工会技能
bool CDealPlayerPkg::OnLearnUnionSkillReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnLearnUnionSkillReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGLearnUnionSkillReq, pReq, pPkg, wPkgLen );

	/*StudyUnionSkillRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.skillId = pReq->skillID;

	SendMsgToRelationSrv(StudyUnionSkillRequest, srvmsg);*/


	GMForwardUnionCmd msg;
	StructMemSet(msg, 0x00, sizeof(msg));

	msg.lNotiPlayerID = pOwner->GetPlayerID();
	msg.cmdID = GCUnionUISelectCmd::LEARN_UNION_SKILL;
	msg.learnUnionSkill.skillID = pReq->skillID;

	SendMsgToMapSrvByPlayer(GMForwardUnionCmd, pOwner, msg);

	return true;
	TRY_END;
	return false;
}

// 提升工会等级
bool CDealPlayerPkg::OnAdvanceUnionLevelReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAdvanceUnionLevelReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAdvanceUnionLevelReq, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	/*AdvanceUnionLevelRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	SendMsgToRelationSrv(AdvanceUnionLevelRequest, srvmsg);*/

	GMForwardUnionCmd msg;
	StructMemSet(msg, 0x00, sizeof(msg));

	msg.lNotiPlayerID = pOwner->GetPlayerID();
	msg.cmdID = GCUnionUISelectCmd::ADVANCE_UNION_LEVEL;

	SendMsgToMapSrvByPlayer(GMForwardUnionCmd, pOwner, msg);

	return true;
	TRY_END;
	return false;
}

// 发布工会公告
bool CDealPlayerPkg::OnPostUnionBulletinReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPostUnionBulletinReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPostUnionBulletinReq, pReq, pPkg, wPkgLen );

	PostUnionBulletinRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.seq = pReq->seq;
	srvmsg.end = pReq->end;
	if(pReq->contentLen < sizeof(srvmsg.content))
	{
		srvmsg.contentLen = pReq->contentLen;
		StructMemCpy(srvmsg.content, pReq->content, pReq->contentLen);
	}
	else
	{
		srvmsg.contentLen = sizeof(srvmsg.content);
		StructMemCpy(srvmsg.content, pReq->content, sizeof(srvmsg.content));
	}

	SendMsgToRelationSrv(PostUnionBulletinRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}bool CDealPlayerPkg::OnQueryUnionLogReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnQueryUnionLogReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryUnionLogReq, pReq, pPkg, wPkgLen );

	QueryUnionLogRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.bookmark = pReq->bookmark;

	SendMsgToRelationSrv(QueryUnionLogRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnModifyUnionPrestigeReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnModifyUnionPrestigeReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGModifyUnionPrestigeReq, pReq, pPkg, wPkgLen );

	ModifyUnionPrestigeRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	srvmsg.point = pReq->point;
	srvmsg.bAdd = pReq->bAdd;

	SendMsgToRelationSrv(ModifyUnionPrestigeRequest, srvmsg);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnModifyUnionBadgeReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnModifyUnionBadgeReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGModifyUnionBadgeReq, pReq, pPkg, wPkgLen );

	/*ModifyUnionBadgeRequest srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.uiid = pOwner->GetSeledRole();
	SafeStrCpy(srvmsg.badgeData, pReq->badgeData);
	srvmsg.dataLen = (UINT)strlen(srvmsg.badgeData);

	SendMsgToRelationSrv(ModifyUnionBadgeRequest, srvmsg);*/


	GMForwardUnionCmd msg;
	StructMemSet(msg, 0x00, sizeof(msg));

	msg.lNotiPlayerID = pOwner->GetPlayerID();
	msg.cmdID = GCUnionUISelectCmd::DESIGN_UNION_BADGE;
	SafeStrCpy(msg.modifyUnionBadge.badgeData, pReq->badgeData);
	msg.modifyUnionBadge.dataLen = (UINT)strlen(msg.modifyUnionBadge.badgeData);

	SendMsgToMapSrvByPlayer(GMForwardUnionCmd, pOwner, msg);

	return true;
	TRY_END;
	return false;
}

