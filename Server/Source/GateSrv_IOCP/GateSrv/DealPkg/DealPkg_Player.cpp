﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"


CliProtocol CDealPlayerPkg::m_cliprotocol;


///初始化(注册各命令字对应的处理函数);
void CDealPlayerPkg::Init()
{
	/************************************************************************/
	/*todo wcj 2011.01.04 进入场景前的所有消息处理*/
	/************************************************************************/ 
	//todo wcj 2011.01.04 似乎没用到，暂时删除
	//Register( C_G_LOGIN, &OnPlayerLogin );//玩家登录；
	Register( C_G_REQ_RDSTR, &OnPlayerReqRdStr );//玩家登录过程：请求随机字符串；
	Register( C_G_USER_MD5VALUE, &OnPlayerUserMd5Value );//玩家登录过程：发来md5校验值；

	Register( C_G_CREATE_ROLE, &OnPlayerCreateRole);//玩家创建角色
	Register( C_G_DELETE_ROLE, &OnPlayerDeleteRole);//玩家删除角色

	Register( C_G_SELECT_ROLE, &OnPlayerSelRole );//玩家选角色；


	Register( C_G_NET_DETECT, &OnNetDetect ); //返回对服务器网络延时检测包的响应

	Register( C_G_SWITCH_MAP_REQ, &OnSwitchMapReq );//地图切换
	Register( C_G_SWITCH_ENTER_NEWMAP, &OnPlayerSwitchEnterNewMap );//玩家跳地图进入新地图请求;

	Register( C_G_DEFAULT_REBIRTH, &OnDefaultRebirth );//玩家超时被动或主动选择默认复活点复活；
	Register( C_G_REBIRTH_READY, &OnRebirthReady );//玩家重生准备好,发此消息通知SRV，SRV收到此消息后，将玩家Appear消息广播给复活点周围玩家
	/************************************************************************/
	/*                                                                      */
	/************************************************************************/
	Register( C_G_USE_MONEY_TYPE, &OnUseMoneyType );//玩家切换所使用的货币
	Register( C_G_HONOR_MASK, &OnHonorMask );//玩家选择显示的称号

	Register( C_G_RUN, &OnPlayerRun );//玩家跑动；
	Register( C_G_PLAYER_BEYONDBLOCK, &OnPlayerBeyondBlock );//玩家跑动中跨块；

	Register( C_G_ENTER_WORLD, &OnPlayerEnterWorld );//玩家进入世界；
	Register( C_G_ATTACK_MONSTER, &OnPlayerAttackMonster );//玩家攻击怪物；
	Register( C_G_REQ_ROLEINFO, &OnPlayerReqRoleInfo );//玩家请求别个玩家的角色信息（用于界面显示）；
	Register( C_G_REQ_MONSTERINFO, &OnPlayerReqMonsterInfo );//玩家请求某怪物的相关信息（用于界面显示）；		
	Register( C_G_TIME_CHECK, &OnPlayerTimeCheck );//玩家发来的网络延时检测包；	
	//Register( C_G_PLAYER_COMBO, &OnPlayerCombo );//临时协议，玩家连击；

	Register( C_G_CHAT, &OnPlayerChat );//临时协议，玩家连击；
	Register( C_G_ATTACK_PLAYER, &OnAttackPlayer ); //玩家攻击玩家

	Register( C_G_SCRIPT_CHAT, &OnPlayerScriptChat );//玩家与NPC聊天;



	Register( C_G_INVITEMOUNT,&OnInvitePlayerToRideTeam );

	Register( C_G_AGREE_BEINVITE,&OnReturnInviteRideTeamResult);

	Register( C_G_LEAVE_MOUNTTEAM,&OnLeaveRideTeam); 

	Register( C_G_REQ_BUFFEND, &OnReqBuffEnd );			 //玩家主要要求结束辅助效果

	Register( C_G_USE_ASSISTSKILL, &UseAssistSkill );   //玩家使用辅助技能

	/************************************************************************/
	/*关于组队*/
	/************************************************************************/ 
	Register( C_G_INVITE_PLAYER_JOIN_TEAM, &OnInvitePlayerJoinTeam );	 //玩家发送加入队伍
	Register( C_G_RECV_INVITE_JOIN_TEAM_FEEDBACK, &OnInviteTeamFeedBack );	  //被邀请玩家的反馈
	Register( C_G_REQ_QUIT_TEAM, &OnReqQuitTeam );	  //组员退出
	Register( C_G_DESTORY_TEAM, &OnDestoryTeam );	  //队伍解散
	Register( C_G_TEAM_SWITCH, &OnTeamSwitch );		  //队伍开关
	Register( C_G_CHANGE_TEAM_LEADER, &OnChangeLeader );	 //更换队长 
	Register( C_G_BAN_TEAM_MEMBER,  &OnBanTeamMember );		 //移除组员
	Register( C_G_CHANGE_TEAM_ITEM_MODE, &OnChangeTeamItemMode );  //更换组队物品分配模式
	Register( C_G_CHANGE_TEAM_EXP_MODE,  &OnChangeTeamExpMode );	//更换组队经验分配模式
	Register( C_G_ROLL_ITEM_FEEDBACK,  &OnCGRollItemFeedBack );	//roll的回答
	Register( C_G_CHANGE_REQ_SWITCH,  &OnCGChangeReqSwitch );	//更改入队邀请开关
	Register( C_G_CHANGE_TEAM_ROLL_LEVEL,  &OnCGChangeTeamRollLevel );	//更改入队邀请开关
	Register( C_G_REQ_JOIN_TEAM,  &OnCGReqJoinTeam );	//申请入队
	Register( C_G_RECV_REQ_JOIN_TEAM_FEEDBACK,  &OnCGRecvReqJoinTeamFeedBack );	//申请入队
	/************************************************************************/
	/*关于好友*/
	/************************************************************************/  
	Register( C_G_ADDFRIEND_REQ, &OnAddFriendReq );//当接到加好友的请求时
	Register( C_G_DELFRIEND_REQ, &OnRecvDelFriendReq );//当接到删除好友的请求时
	Register( C_G_ADDFRIEND_RESPONSE ,&OnRecvAddFriendResponse );//
	Register( C_G_CREATE_FRIENDGROUP_REQ, &OnCreateFriendGroupReq );//当接到创建好友组的请求
	Register( C_G_DEL_FRIENDGROUP_REQ, &OnDelFriendGroupReq );//当接到删除好友组时
	Register( C_G_MOVE_FRIENDGROUP_REQ, &OnMoveFriendFromGroupToOther );//当移动一个组时
	Register( C_G_CHG_FRIENDGROUPNAME_REQ, &OnChangeFriendGroupName );//当接到改变好友组名称的消息
	Register( C_G_FRIEND_CHAT ,&OnChatBetweenFriend );//好友之间的聊天消息
	Register( C_G_ADD_BLACKLIST_REQ, &OnAddPlayerToBlackList );//当玩家加入黑名单时
	Register( C_G_DEL_BLACKLIST_REQ, &OnDelPlayerFromBlackList );
	Register( C_G_LOCKFOE_REQ,  &OnLockFoePlayer );
	Register( C_G_QUERY_FRIEND_TWEETS_REQ, &OnQueryFriendTweetReq);
	Register( C_G_QUERY_FRIEND_LIST_REQ, &OnQueryFriendListReq);
	/************************************************************************/
	/*物品交易*/
	/************************************************************************/   
	Register( C_G_SEND_TRADE_REQ, &OnSendTradeReq );//发送交易请求
	Register( C_G_SEND_TRADE_REQ_RESULT, &OnSendTradeReqResult );//请求交易的结果（ 同意或拒绝）
	Register( C_G_ADD_OR_DELETE_ITEM, &OnAddOrDeleteItem );//增加或删除一个物品到交易栏
	Register( C_G_SET_TRADE_MONEY, &OnSetTradeMoney);//设置交易金钱
	Register( C_G_CANCEL_TRADE, &OnCancelTrade );//取消交易
	Register( C_G_LOCK_TRADE, &OnLockTrade );//锁定交易
	Register( C_G_CONFIRM_TRADE, &OnConfirmTrade );//确认交易
	/************************************************************************/
	/*todo wcj 2011.01.04 工会相关*/
	/************************************************************************/  
	Register( C_G_CREATE_UNION_REQ , &OnCreateUnion );//创建工会
	Register( C_G_DESTROY_UNION_REQ, &OnDestroyUnion );//解散工会
	Register( C_G_QUERY_UNINO_BASE_REQ , &OnQueryUnionBasicInfo );//请求工会信息
	Register( C_G_QUERY_UNION_MEMBER_REQ , &OnQueryUnionMemberInfo );//请求工会成员信息
	Register( C_G_ADD_UNION_MEMBER_REQ , &OnAddUnionMemberReq );//加入工会请求
	Register( C_G_ADD_UNION_MEMBER_CONFIRMED , &OnConfirmUnionMemberAddReq );//收到工会加入邀请
	Register( C_G_REMOVE_UNION_MEMBER_REQ , &OnRemoveUnionMemberReq );//离开工会
	//Register( C_G_MOD_UNION_POS_RIG_REQ , &OnModifyUnionPosRightReq );//修改工会职位对应权限
	//Register( C_G_ADD_UNION_POS_REQ , &OnAddUnionPosReq );//增加工会职位
	//Register( C_G_ROM_UNION_POS_REQ , &OnRemoveUnionPosReq );//减少工会职位
	Register( C_G_TRA_UNION_CAP_REQ , &OnTransformUnionCaptionReq );//转移会长职位
	Register( C_G_MOD_UNION_MBR_TTL_REQ , &OnModifyUninMemberTitleReq );//修改工会成员称号
	Register( C_G_ADV_UNION_MBR_POS_REQ , &OnAdvanceUnionMemberPosReq );//提升工会成员职位
	Register( C_G_RED_UNION_MBR_POS_REQ , &OnReduceUninMemberposReq );//降低工会成员职位
	Register( C_G_SPE_UNION_CHA_REQ , &OnUnionChannelSpeekReq );//工会频道聊天
	Register( C_G_UPD_UNION_POS_CFG_REQ , &OnUpdateUnionPosCfgReq );//更新工会职位管理
	Register( C_G_FOR_UNION_SPEEK_REQ, &OnForbidUnionSpeekReq);//工会成员禁言
	Register( C_G_ISSUE_UNION_TASK_REQ, &OnIssueUnionTaskReq);//发布工会任务
	Register( C_G_LEARN_UNION_SKILL_REQ, &OnLearnUnionSkillReq);//学习工会技能
	Register( C_G_ADV_UNION_LEVEL_REQ, &OnAdvanceUnionLevelReq);//提升工会等级
	Register( C_G_POS_UNION_BULLETIN_REQ, &OnPostUnionBulletinReq);//发布工会公告
	Register( C_G_QUERY_UNION_LOG_REQ, OnQueryUnionLogReq);//获取工会日至
	Register( C_G_MOD_UNION_PRESTIGE_REQ, OnModifyUnionPrestigeReq);//修改工会威望
	Register( C_G_MOD_UNION_BADGE_REQ, OnModifyUnionBadgeReq);//修改工会徽章
	/************************************************************************/
	/*todo wcj 2011.01.04 道具相关*/
	/************************************************************************/ 
	Register( C_G_REPAIR_WEAR_BY_ITEM, &OnRepairWearByItem); // 修复耐久度
	Register( C_G_QUERY_ITEMPKG_PAGE ,&OnQueryItemPkgPage );//查询道具包对应页的道具信息
	Register( C_G_DROPITEM, &OnDropItem );//玩家丢弃道具时
	Register( C_G_SPLIT_ITEM,&OnSplitItem );//拆分道具
	Register( C_G_SWAPITEM, &OnSwapItem );//玩家交换道具
	Register( C_G_SWAP_EQUIPITEM , &OnSwapEquipItem );
	Register( C_G_QUERY_PKG, &OnQueryPkg );//玩家查询包裹消息；
	Register( C_G_QUERYITEMPACKAGE_INFO,&OnQueryItemPkgInfo );//查询道具包的信息
	Register( C_G_PICKITEM,&OnPickOneItem );//玩家拾取了一个道具
	Register( C_G_PICKALLITEM, &OnPickAllItem );//玩家拾取了一个道具包
	Register( C_G_USEITEM, &OnUseItem );//玩家使用道具
	Register( C_G_USE_DAMAGE_ITEM, &OnUseDamageItem );	//玩家使用伤害类道具
	Register( C_G_UNUSEITEM, &OnUnUseItem );//玩家不使用道具
	Register( C_G_EQUIPITEM, &OnEquipItem );//装备可装备道具
	Register( C_G_ACTIVEMOUNT,&OnActiveMountItem);
	Register( C_G_QUERY_REPAIRITEM_COST , &OnQueryRepairItemCost );

	/************************************************************************/
	/*todo wcj 2011.01.04 聊天相关*/
	/************************************************************************/ 
	Register( C_G_CREATE_CHAT_GROUP ,&OnCreateChatGroup );//创建群聊
	Register( C_G_DESTROY_CHAT_GROUP ,&OnDestroyChatGroup );//销毁群聊
	Register( C_G_ADD_CHAT_GROUP_MEMBER_BY_OWNER ,&OnAddChatGroupMemberByOwner );//加入群聊成员（群主）
	Register( C_G_ADD_CHAT_GROUP_MEMBER ,&OnAddChatGroupMember );//加入群聊成员
	Register( C_G_REMOVE_CHAT_GROUP_MEMBER ,&OnRemoveChatGroupMember );//群聊成员离开
	Register( C_G_CHAT_GROUP_SPEAK ,&OnChatGroupSpeak );//群聊成员聊天
	Register( C_G_REPAIR_WEAR_BY_NPC, &OnRepairWearByNpc);//通过NPC进行修理
	Register( C_G_IRC_ENTER_WORLD, &OnIrcEnterWorld );//Irc进入世界

	/************************************************************************/
	/*todo wcj 2011.01.04 宠物相关*/
	/************************************************************************/  
	Register( C_G_CHANGE_PET_NAME, &OnPlayerChangePetName );			//更换宠物姓名
	Register( C_G_SUMMON_PET, &OnPlayerSummonPet );		//召唤宠物
	Register( C_G_CLOSE_PET, &OnPlayerClosePet );		//唤回宠物
	Register( C_G_CHANGE_PET_IMAGE, &OnPlayerChangePetImage );	//更新宠物形象
	Register( C_G_CHANGE_PET_SKILL_STATE, &OnPlayerChangePetSkillState );//重新设置宠物使用的技能；

	/************************************************************************/
	/*todo wcj 2011.01.04 邮件相关*/
	/************************************************************************/  
	Register( C_G_SENDMAIL_TO_PLAYER, &OnSendMailToPlayer );//发送邮件
	Register( C_G_DELETE_MAIL, &OnDeleteMail );//删除邮件
	Register( C_G_QUERY_MAILLIST_BYPAGE, &OnQueryMailListByPageIndex );//请求邮件列表
	Register( C_G_QUERY_MAIL_DETIALINFO, &OnQueryMailDetialInfo );//邮件的详细信息
	Register( C_G_PICK_MAIL_ATTACHITEM, &OnPickMailAttachItem );//拾取邮件道具
	Register( C_G_PICK_MAIL_ATTACHMONEY, &OnPickMailAttachMoney );//拾取邮件金钱
	Register( C_G_DELETE_ALLMAIL , &OnDeleteAllMail );//删除所有邮件

	/************************************************************************/
	/*todo wcj 2011.01.04 保护托盘相关功能*/
	/************************************************************************/ 
	Register( C_G_PUTITEM_TO_PROTECTPLATE, &OnPutItemToProtectPlate );//将物品放入保护托盘
	Register( C_G_CONFIRM_PROTECTPLATE, &OnConfirmProtectPlate );//执行保护托盘程序
	Register( C_G_RELEASE_SPECIALPROTECT, &OnReleaseItemSpecialProtect );//取消道具的特殊保护
	Register( C_G_CANCEL_SPECIALPROTECT, &OnCancelItemUnSpecialProtectWaitTime );//取消道具的解除特殊保护的申请
	Register( C_G_TAKE_ITEM_FROM_PROTECTPLATE, &OnTakeItemFromProtectPlate );//从保护道具的托盘中取出道具
	/************************************************************************/
	/*todo wcj 2011.01.04 拍卖相关*/
	/************************************************************************/ 
	Register( C_G_QUERY_MY_BIDSTATE,&OnPlayerQueryMyBidState );
	Register( C_G_QUERY_MY_AUCTIONS,&OnPlayerQueryMyAuctions);
	Register( C_G_QUERY_AUCTIONS_BYCONDITION,&OnPlayerQueryAuctionByCondition);
	Register( C_G_BEGIN_NEW_AUCTION,&OnPlayerBeginNewAuction );
	Register( C_G_CANCEL_AUCTION,&OnPlayerCancelMyAuction );
	Register( C_G_BID_AUCTIONITEM,&OnPlayerBidAuctionItem );
	/************************************************************************/
	/*todo wcj 2011.01.04 查询或者修改自身或者其他玩家信息或者行为*/
	/************************************************************************/
	Register( C_G_MOUSETIP_REQ, &OnMouseTipReq );//鼠标停留信息查询(HP|MP)
	Register( C_G_QUERY_ACCOUNT_INFO, &OnAccountInfoReq);//获取帐户信息

	Register( C_G_CHANGE_BATTLEMODE, &OnChangeBattleMode );//改变战斗模式
	Register( C_G_ADD_APT_REQ, &OnAddAptReq );//玩家请求加活点；

	Register( C_G_IRC_QUERY_PLAYERINFO , &OnIrcQueryPlayerInfo );

	Register( C_G_QUERY_SUIT_INFO, &OnQuerySuitInfo );//查询套装信息
	Register( C_G_CLEAR_SUIT_INFO, &OnClearSuitInfo );//清空套装信息
	Register( C_G_SAVE_SUIT_INFO, &OnSaveSuitInfo );//保存套装信息
	Register( C_G_CHANAGE_SUIT, &OnChanageSuit );//一键换装

	Register( C_G_REQ_PLAYER_DETAIL_INFO, &OnReqPlayerDetailInfo );//右键查询玩家属性
	Register( C_G_QUERY_NICKUNION, &OnQueryNickUnion );//查询昵称对应玩家；
	Register( C_G_QUERY_OFF_LINE_PLAYER_INFO, &OnQueryOfflinePlayerInfo);

	/************************************************************************/
	/*todo wcj 2011.01.04 系统相关信息*/
	/************************************************************************/
	Register( C_G_QUERY_EVILTIME, &OnQueryEvilTime );//查询恶魔时间
	Register( C_G_QUERY_RANKINFO_BYPAGE ,&OnQueryRankInfoByPageIndex );//依据页数查询排行榜
	Register( C_G_QUERY_RANKINFO_BYNAME, &OnQueryRankInfoByPlayerName );//依据玩家姓名查询排行榜
	Register( C_G_QUERY_VIP_INFO, &OnVipInfoReq);//查询vip信息

	Register( C_G_QUERY_SHOP, &OnQueryShop );//查询商店信息；
	Register( C_G_SHOP_BUY_REQ, &OnShopBuyReq );//购买商店物品请求；
	Register( C_G_ITEM_SOLD_REQ, &OnItemSoldReq );//出售商品请求；
	Register( C_G_PURCHASE_ITEM, &OnPurchaseItemReq);//购买物品

	Register( C_G_USE_SKILL, &OnUseSkill );//使用技能 **新技能协议
	Register( C_G_USE_SKILL_2, &OnUseSkill2 );//使用技能 **新技能协议,04.29新方向，线性及其它群攻，由cli传来其预判，服务器验证；
	Register( C_G_START_CAST, &OnStartCast );
	Register( C_G_STOP_CAST, &OnStopCast );

	Register( C_G_CLIENT_SYNCHTIME ,&OnClientSynchTime );//客戶端同步時間

	/************************************************************************/
	/*todo wcj 2011.01.04 系统相关*/
	/************************************************************************/
	Register( C_G_PURCHASE_VIP, &OnPurchaseVipReq);//购买vip
	Register( C_G_FETCH_VIP_ITEM, &OnFetchVipItemReq);//领取vip物品
	Register( C_G_FETCH_NON_VIP_ITEM, &OnFetchNonVipItemReq);//领取非vip物品
	Register( C_G_PURCHASE_GIFT, &OnPurchaseGiftReq);//赠送

	Register( C_G_GLOBE_COPYINFO_QUERY, &OnPlayerGlobeCopyInfoQuery );//玩家查询全局副本信息；
	Register( C_G_TEAM_COPYINFO_QUERY, &OnPlayerTeamCopyInfoQuery );//玩家查询组队副本信息；


	Register( C_G_CLOSE_STORAGEPLATE, &OnCloseStoragePlate);
	Register( C_G_CANCEL_QUEUE, &OnCancelQueue );//玩家主动退出排队；

	Register( C_G_DISCARD_COLLECT_ITEM, &OnDiscardCollectItem );//放弃采集道具
	Register( C_G_PICK_COLLECT_ITEM, &OnPickCollectItem );//放弃采集道具
	Register( C_G_PICK_ALL_COLLECT_ITEM, &OnPickAllCollectItem );//放弃采集道具

	Register( C_G_EQUIP_FASHION_SWITCH, &OnEquipFashionSwitch);//装备态/时装态切换
	Register( C_G_END_ANAMORPHIC, &OnEndAnamorphic );//停止变形oragePlate );

	Register( C_G_UPGRADE_SKILL, &OnUpgradeSkillReq );  //升级技能

	Register( C_G_MANUAL_UPGRADE_REQ, &OnManualUpgradeReq);//手動升級

	Register( C_G_ALLOCATE_POINT_REQ, &OnAllocatePointReq);

	/************************************************************************/
	/*todo wcj 2011.01.04 个人商店或者摆摊相关*/
	/************************************************************************/
	Register( C_G_CHANGE_STROAGE_LOCKSTATE, &OnChangeStorageLockState );
	Register( C_G_TAKEITEM_FROM_STORAGE, &OnTakeItemFromStorage );
	Register( C_G_PUTITEM_TO_STORAGE, &OnPutItemToStorage );
	Register( C_G_TAKEITEM_FROM_STORAGEINDEX_TO_PKGINDEX, &OnTakeItemFromStorageIndexToPkgIndex );
	Register( C_G_PUTITEM_FROM_PKGINDEX_TO_STORAGEINDEX, &OnPutItemToStorageIndexFromPkgIndex );
	Register( C_G_SET_NEW_STORAGE_PASSWORD, &OnSetNewStoragePsd );
	Register( C_G_RESET_STORAGE_PASSWORD,&OnResetStoragePsd );
	Register( C_G_TAKEITEM_CHECK_PASSWORD, &OnTakeItemCheckPassWord );
	Register( C_G_CHANGE_LOCKSTATE_CHECK_PSD, &OnChangeStorageLockStateCheckPsd );
	Register( C_G_EXTEND_STORAGE, &OnExtendStorage );
	Register( C_G_SORT_STORAGE, &OnSortStorage );
	Register( C_G_SWAP_STORAGE_ITEM, &OnSwapStorageItem );
	Register( C_G_SORT_PKG_ITEM_BEGIN, &OnSortPkgItemBegin );
	Register( C_G_SORT_PKG_ITEM_END, &OnSortPkgItemEnd );
	Register( C_G_SORT_PKG_ITEM, &OnSortPkgItem );

	Register( CTCMD_QUERY_PLAYERID_REQ, &OnGMPlayerIDReq );
	Register( GTCMD_QUERY_REQ, &OnGMQueryReq );
	Register( C_G_BUILD_NPC_REQ, &OnItemNpcReq );
	Register( C_G_BATCH_SELL_ITEMS_REQ, &OnBatchSellItemsReq);
	Register( C_G_REPURCHASE_ITEM_REQ, &OnRepurchaseItemReq);
	Register( C_G_CLOSE_OLTEST_DLG, &OnCloseOLTestDlg );
	Register( C_G_ANSWER_OLTEST_QUESTION, &OnAnswerQuestion );
	Register( C_G_DROP_OLTEST, &OnPlayerDropOLTest );
	Register( C_G_SEND_SYSMAIL_TO_PLAYER, &OnPlayerSendSysMailToOther );

	Register( C_G_RESET_COPY, &OnPlayerResetCopy );//玩家请求重置副本；

	Register( C_G_TIME_LIMIT_RES, &OnPlayerTimeLimitRes );//玩家点击倒计时确认框；

	Register( C_G_SELECT_WAR_REBIRTH_OPTION,  &OnSelectWarRebirthOption);//选择攻城战复活点选项
	Register( C_G_REPURCHASE_ITEMS_LIST_REQ,  &OnRepurchaseItemsListReq);//获取回购列表

	Register( C_G_RACEMASTER_SELECT_OPTYPE ,&OnRaceMasterSelectOPType );//族长操作族长列表
	Register( C_G_ADD_NEWMSG_TO_RACEMASTER_NOTICES, &OnRaceMasterSetNewPublicNotice );//族长发布新的公告 
	Register( C_G_DECLARE_WAR_TO_OTHERRACE, &OnRaceMasterDeclareWarToOtherRace );//族长对其他种族宣战 

	Register( C_G_DELETE_PUBLICNOTICE, &OnGMRemovePublicNotice );//GM移除公告
	Register( C_G_LOAD_NEW_PUBLICNOTICE,&OnGMReloadPublicNotices );//GM重新加载公告
	Register( C_G_ADD_NEW_PUBLICNOTICE, &OnGMAddNewPublicNotice );//添加新的公告
	Register( C_G_MODIFY_PUBLICNOTICE, &OnGMModifyPublicNotice );//GM修改公告
	Register( C_G_ADD_NEW_MARQUEUE_NOTICE,&OnGMAddNewMarqueueNotice );//GM增加跑马灯
	Register( C_G_QUERY_ALL_PUBLICNOTICES, &OnGMQueryAllPublicNotices );//GM查询所有公告

	Register( C_G_RETURN_SEL_ROLE, &OnPlayerReturnSelRole );//返回角色选择


	//C_G_ITEM_UPGRADE_PLATE,弹物品升级托盘;
	Register( C_G_ITEM_UPGRADE_PLATE, &OnCGItemUpgradePlate );//弹物品升级托盘
	//C_G_ADDSTH_TO_PLATE                     	=	0x013c,	//CGAddSthToPlate
	Register( C_G_ADDSTH_TO_PLATE, &OnAddSthToPlate );//往托盘中放物品
	//C_G_TAKESTH_FROM_PLATE                  	=	0x013d,	//CGTakeSthFromPlate
	Register( C_G_TAKESTH_FROM_PLATE, &OnTakeSthFromPlate );//从托盘中取物品
	//C_G_PLATE_EXEC                          	=	0x013e,	//CGPlateExec
	Register( C_G_PLATE_EXEC, &OnPlateExec );//尝试托盘操作
	//C_G_CLOSE_PLATE                         	=	0x013f,	//CGClosePlate
	Register( C_G_CLOSE_PLATE, &OnClosePlate );//关托盘

	Register( C_G_TIMED_WEAR_IS_ZERO, &OnTimedWearIsZero);

	
	Register( C_G_DEL_FOE_REQ, &OnDeleteFoePlayer );//删除仇人

	Register( C_G_ADD_SPSKILL_REQ, &OnCGAddSpSkillReq );

	Register( C_G_DELETE_TASK, &OnDeleteTaskReq );//删除任务

	Register( C_G_GET_CLIENT_DATA, &OnGetClientData);//获取客户信息
	Register( C_G_SAVE_CLIENT_DATA, &OnSaveClientData); //保存客户信息


	Register( C_G_ENTER_PW_MAP,&OnEnterPWMap );
	Register( C_G_GET_SHIFT_PW_LIST,&OnShiftPWListReq );
	Register( C_G_SHIFT_PW,&OnShiftPW );
	Register( C_G_MODIFY_EFFECTIVE_SKILL_ID, &OnModEffectiveSkillID);


	Register( C_G_MODIFY_TWEET_RECVER_FLAG, &OnSetViewedConditionOfTweet);
	Register( C_G_REPLY_CALL_MEMBER, &OnReplyCallMember);



	return;
}



bool CDealPlayerPkg::OnPlayerAttackMonster( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerAttackMonster,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGAttackMonster, attackMonster, pPkg, wPkgLen );
	//if ( wPkgLen != sizeof(CGAttackMonster) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}
	//const CGAttackMonster* attackMonster = (const CGAttackMonster*) pPkg;

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该攻击怪物\n" );
		return false;
	}

	if ( attackMonster->attackerID != pOwner->GetPlayerID() )
	{
		D_WARNING( "收到客户端:%x攻击怪物%x消息，消息中的攻击者%x非其本人\n\n", ((unsigned long)(DWORD_PTR)pOwner), attackMonster->lMonsterID, attackMonster->attackerID );
		return false;
	}

	//D_WARNING( "收到客户端:%x攻击怪物%x消息\n\n", attackMonster->attackerID.dwPID, attackMonster->lMonsterID );

	GMAttackMonster srvmsg;
	srvmsg.sequenceID = attackMonster->sequenceID;
	srvmsg.attackerID = attackMonster->attackerID;
	srvmsg.lSkillType = attackMonster->lSkillType;
	srvmsg.lMonsterID = attackMonster->lMonsterID;
	srvmsg.nAttackPosX = attackMonster->nAttackPosX;
	srvmsg.nAttackPosY = attackMonster->nAttackPosY; 
	srvmsg.fAttackerPosX = attackMonster->nAttackerPosX;
	srvmsg.fAttackerPosY = attackMonster->nAttackerPosY;

	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		//MsgToPut* pNewMsg = CreateSrvPkg( GMAttackMonster, pMapsrv, gmAttack );
		//pMapsrv->SendPkgToSrv( pNewMsg );
		GateSrvSendMsgToMapSrv( GMAttackMonster );
		//D_WARNING( "向MAPSRV发送玩家攻击怪物消息(位置%d,%d，攻击者：%x,被攻击者:%x)\n\n"
		//	, gmAttack.nAttackPosX, gmAttack.nAttackPosY, gmAttack.attackerID.dwPID, gmAttack.lMonsterID );
	} else {
		D_WARNING( "收到玩家攻击怪物消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}
bool CDealPlayerPkg::OnPlayerScriptChat( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerScriptChat,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGScriptChat, pScriptChat, pPkg, wPkgLen );

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该与NPC聊天\n" );
		return false;
	}

	GMScriptChat srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();

	srvmsg.tgtType = pScriptChat->tgtType;
	srvmsg.tgtID = pScriptChat->tgtID;
	srvmsg.nOption = pScriptChat->nOption;

	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		//MsgToPut* pMsg = CreateSrvPkg( GMScriptChat, pMapsrv, scriptChat );
		//pMapsrv->SendPkgToSrv( pMsg );
		GateSrvSendMsgToMapSrv( GMScriptChat );
	} else {
		D_WARNING( "收到玩家脚本对话消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}

///向玩家返回跳地图出错消息；
void CDealPlayerPkg::SendSwitchMapFailed( CPlayer* pPlayer )
{
	TRY_BEGIN;

	if ( NULL == pPlayer )
	{
		D_ERROR( "SendSwitchMapFailed,pOwner空\n" );
		return;
	}
	//2.通知客户端目标地图信息；
	GCSwitchMap playerMsg;
	playerMsg.playerID = pPlayer->GetPlayerID();
	playerMsg.nErrCode = GCSwitchMap::OP_ERROR;
	playerMsg.usMapID = 0;
	playerMsg.iPosX = 0;
	playerMsg.iPosY = 0;
	NewSendPlayerPkg( GCSwitchMap, pPlayer, playerMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCSwitchMap, pPlayer, playerMsg );
	//pPlayer->SendPkgToPlayer( pNewMsg );//通知客户端

	return;
	TRY_END;
	return;
}

///玩家切换地图
bool CDealPlayerPkg::OnSwitchMapReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnSwitchMapReq,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGSwitchMap, pSwitchMap, pPkg, wPkgLen );

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家%s尚未进入战斗状态，不应该切换地图\n", pOwner->GetAccount() );
		SendSwitchMapFailed( pOwner );
		return false;
	}

	if ( pOwner->IsInSwitching() )
	{
		D_WARNING( "玩家%s在跳地图中，不再理会新的跳地图请求\n", pOwner->GetAccount() );
		return false;
	}

	CMapSrv* pOrgMapSrv = pOwner->GetMapSrv();
	if ( NULL == pOrgMapSrv )
	{
		D_WARNING( "玩家%s地图时，找不到其原来所在的mapsrv\n", pOwner->GetAccount() );
		return false;
	}

	/*
	1、检测位置，是否可以跳转，找到跳转点，
	如果当前已处于跳转状态，则不予理会，跳转状态也不理会其它消息，如果找不到跳转点，则通知客户端跳转失败；
	如果找不到目标地图，则通知客户端跳转地图失败;
	2、向原mapsrv发离开地图消息；
	3、向客户端发地图跳转消息；
	4、保存玩家的跳转状态，准备以后在收到mapsrv的玩家离开消息时，或者收到客户端的跳地图准备完成时，进行后一步处理(enteringworld)；
	*/

	//D_DEBUG( "收到玩家%s跳地图请求，该玩家说明的，其自身当前地图:%d,点(%d,%d)\n", pOwner->GetAccount(), pSwitchMap->usMapID, pSwitchMap->iPosX, pSwitchMap->iPosY );

	// 获取地图编号及位置
	int iPosX = 0 , iPosY = 0;
	unsigned short usMapID = 0;
	if ( !(pOwner->GetCurPos( iPosX, iPosY )) )
	{
		D_ERROR( "跳地图时，不能确定玩家%s当前所在点\n", pOwner->GetAccount() );
		SendSwitchMapFailed( pOwner );
		return false;
	}
	if ( !(pOwner->GetCurMapID( usMapID )) )
	{
		D_ERROR( "跳地图时，不能确定玩家%s当前所在地图\n", pOwner->GetAccount() );
		SendSwitchMapFailed( pOwner );
		return false;
	}

	bool bResult = false;

	int iTargetPosX = 0;
	int iTargetPosY = 0;
	if ( usMapID != pSwitchMap->usMapID )  
	{
		D_ERROR( "跳地图时，玩家%s声明的其当前地图号与gate当前保存的玩家所在地图号不一致\n", pOwner->GetAccount() );
		SendSwitchMapFailed( pOwner );
		return false;
	}

	// 可以跳转的条件(1、客户端传的mapid与Gate当前保存的玩家mapid一致；2、其所传位置对应的跳转点与服务器当前保存的玩家位置对应的跳转点一致)
	CMapTeleporter* pTargetTeleporter = NULL;
	CMapSrv* pTargetMapSrv = NULL;
	CMapTeleporter* pClientTeleporter = CMapTeleManagerSingle::instance()->GetMapTelePorter(pSwitchMap->usMapID, pSwitchMap->iPosX, pSwitchMap->iPosY);
	//改为新移动后，gate没有即时更新移动玩家的新位置信息，暂时较难校验玩家位置，这一块以后再加，目前估且相信客户端报来的位置信息,by dzj, 07.22; CMapTeleporter* pTeleporter = CMapTeleManagerSingle::instance()->GetMapTelePorter(usMapID, iPosX, iPosY);
	CMapTeleporter* pTeleporter = pClientTeleporter;//CMapTeleManagerSingle::instance()->GetMapTelePorter(usMapID, iPosX, iPosY);
	if((NULL != pClientTeleporter) && (NULL != pTeleporter) && (pClientTeleporter == pTeleporter))
	{
		pTargetTeleporter = CMapTeleManagerSingle::instance()->GetMapTelePorter(pTeleporter->LinkID());
		if ( NULL != pTargetTeleporter )
		{
			//// 获取目标地图及位置
			pTargetMapSrv = CDealMapSrvPkg::GetMapSrvByMapcode( pTargetTeleporter->MapID() );
			if ( NULL == pTargetMapSrv )
			{
				D_WARNING("跳地图时，找不到管理地图%l的mapsrv\n", pTargetTeleporter->MapID());
				SendSwitchMapFailed( pOwner );
				return false;
			} 
			pTargetTeleporter->GetValidPoint(iTargetPosX, iTargetPosY);
			bResult = true;
		} else {
			D_WARNING( "目标传送点空\n" );
			SendSwitchMapFailed( pOwner );
			return false;
		}
	} else {
		D_WARNING( "找不到合适的跳转点\n" );
		SendSwitchMapFailed( pOwner );
		return false;
	}

	if ( ! bResult )
	{
		D_WARNING( "玩家%s跳地图失败\n", pOwner->GetAccount() );
		SendSwitchMapFailed( pOwner );
		return false;
	}

	pOwner->OnMapOrPlayerSwitchReq( pTargetTeleporter->MapID(), iTargetPosX, iTargetPosY );

	return true;
	TRY_END
		return true;
}

///玩家超时被动或主动选择默认复活点复活；
bool CDealPlayerPkg::OnDefaultRebirth( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnDefaultRebirth,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGDefaultRebirth, pDefaultRebirth, pPkg, wPkgLen );
	//// 检查消息长度
	//if ( wPkgLen != sizeof(CGDefaultRebirth) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该复活\n" );
		return false;
	}


	GMDefaultRebirth srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.sequenceID = pDefaultRebirth->sequenceID;

	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		//MsgToPut* pMsg = CreateSrvPkg( GMDefaultRebirth, pMapsrv, tosend );
		//pMapsrv->SendPkgToSrv( pMsg );
		GateSrvSendMsgToMapSrv( GMDefaultRebirth );
	} else {
		D_WARNING( "CDealPlayerPkg::OnDefaultRebirth，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END
		return true;
}

///玩家重生准备好,发此消息通知SRV，SRV收到此消息后，将玩家Appear消息广播给复活点周围玩家
bool CDealPlayerPkg::OnRebirthReady( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnRebirthReady,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGRebirthReady, pRebirthReady, pPkg, wPkgLen );
	//// 检查消息长度
	//if ( wPkgLen != sizeof(CGRebirthReady) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该发重生准备好消息\n" );
		return false;
	}


	GMRebirthReady srvmsg;
	srvmsg.sequenceID = pRebirthReady->sequenceID;
	srvmsg.rebirthPlayerID = pOwner->GetPlayerID();

	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		//MsgToPut* pMsg = CreateSrvPkg( GMRebirthReady, pMapsrv, tosend );
		//pMapsrv->SendPkgToSrv( pMsg );
		GateSrvSendMsgToMapSrv( GMRebirthReady );
	} else {
		D_WARNING( "CDealPlayerPkg::OnRebirthReady，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END
		return true;
}

//玩家查询包裹消息；
bool CDealPlayerPkg::OnQueryPkg( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnQueryPkg,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGQueryPkg, pQueryPkg, pPkg, wPkgLen );
	//// 检查消息长度
	//if ( wPkgLen != sizeof(CGQueryPkg) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}
	// 转发
	//const CGQueryPkg* pQueryPkg = (const CGQueryPkg *)pPkg;

	if ( pQueryPkg->nGroupID <=0 )
	{
		D_WARNING( "CDealPlayerPkg::OnQueryPkg，客户端发来的包裹组别号无效\n" );
		return false;
	}

	if ( !(pOwner->IsInfoFull()) )
	{
		D_ERROR( "玩家请求包裹消息时，包裹信息不完整，需要客户端稍后再请求\n" );
		return false;
	}

	GCPlayerPkg playerPkg;
	playerPkg.playerID = pOwner->GetPlayerID();
	int stpos = (pQueryPkg->nGroupID-1) * PKG_GROUP_NUM;//传送起始道具位置；
	//int endpos = pQueryPkg->nGroupID * PKG_GROUP_NUM;//传送终止道具位置(不包含)；
	//ACE_UNUSED_ARG( endpos );
	if ( ( stpos < 0 ) //起始位置小于0;
		|| ( stpos >= ARRAY_SIZE(pOwner->GetFullPlayerInfo().pkgInfo.pkgs) ) //全包裹的道具数小于起始位置；
		)
	{
		D_WARNING( "CDealPlayerPkg::OnQueryPkg，客户端发来的包裹组别号无效2\n" );
		return false;
	}
	unsigned int tocpysize = (ARRAY_SIZE(pOwner->GetFullPlayerInfo().pkgInfo.pkgs)-stpos+1)*sizeof(pOwner->GetFullPlayerInfo().pkgInfo.pkgs[0]);
	if ( tocpysize >= sizeof(playerPkg.playerPkg) )
	{
		tocpysize = sizeof(playerPkg.playerPkg);
	}
	StructMemCpy( playerPkg.playerPkg
		, &(pOwner->GetFullPlayerInfo().pkgInfo.pkgs[stpos])
		, tocpysize
		);
	playerPkg.ucIndex = pQueryPkg->nGroupID;
	SAFE_ARRSIZE(playerPkg.playerPkg, playerPkg.pkgSize);

	NewSendPlayerPkg( GCPlayerPkg, pOwner, playerPkg );
	//MsgToPut* pMsg = CreatePlayerPkg( GCPlayerPkg, pOwner, playerPkg );
	//pOwner->SendPkgToPlayer( pMsg );

	return true;
	TRY_END;
	return false;
}


///玩家请求别个玩家的角色信息（用于界面显示）；
bool CDealPlayerPkg::OnPlayerReqRoleInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerReqRoleInfo,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGReqRoleInfo, reqRoleInfo, pPkg, wPkgLen );
	//if ( wPkgLen != sizeof(CGReqRoleInfo) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}
	//const CGReqRoleInfo* reqRoleInfo = (const CGReqRoleInfo*) pPkg;

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该请求其它玩家的消息16\n" );
		return false;
	}

	GMReqRoleInfo srvmsg;
	srvmsg.lSelfPlayerID = pOwner->GetPlayerID();
	srvmsg.lReqPlayerID = reqRoleInfo->lPlayerID;

	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		//MsgToPut* pNewMsg = CreateSrvPkg( GMReqRoleInfo, pMapsrv, gmReqRoleInfo );
		//pMapsrv->SendPkgToSrv( pNewMsg );
		GateSrvSendMsgToMapSrv( GMReqRoleInfo );
	} else {
		D_WARNING( "收到请求角色信息消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}

//玩家返回的对服务器网络延时检测包的响应；
bool CDealPlayerPkg::OnNetDetect( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnNetDetect,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGNetDetect, netDetect, pPkg, wPkgLen );
	//if ( wPkgLen != sizeof(CGNetDetect) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}
	//const CGNetDetect* netDetect = (const CGNetDetect*) pPkg;

	pOwner->OnNetDetectPkgRcved( netDetect->pkgID );

	return true;
	TRY_END;
	return false;
}

//玩家聊天；
bool CDealPlayerPkg::OnPlayerChat( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerChat,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGChat, chatInfo, pPkg, wPkgLen );

	//if ( true )
	//{
	//	//测试代码，向客户端返回一个聊天消息；
	//	//D_DEBUG( "测试，收到客户端聊天消息%s\n", chatInfo->strChat );
	//	static const char* sendmsg = "srv:hello, just for test!hello, just for test!hello, just for test!hello, just for test!";
	//	++(pOwner->m_lastsendid);
	//	//服务端，向客户端发送GCChat;
	//	GCChat testmsg;
	//	memset( &testmsg, 0, sizeof(testmsg) );
	//	testmsg.sourcePlayerID.dwPID = pOwner->m_lastsendid;//最后一次的发包序号；
	//	SafeStrCpy( testmsg.strChat, sendmsg );
	//	testmsg.chatSize = (unsigned int)(strlen(testmsg.strChat) + 1);
	//	NewSendPlayerPkg( GCChat, pOwner, testmsg );
	//	return true;
	//}

	if ( !pOwner->IsInFighting() && !pOwner->IsIrcPlayer() )
	{
		D_WARNING( "玩家尚未进入战斗状态 并且不是IRC玩家，不应该与其它玩家聊天\n" );
		return false;
	}

	///校验玩家可发的聊天类型，避免玩家尝试发出系统消息之类的信息
	/*
	CT_SCOPE_CHAT                        	=	0,           	//周边范围内群聊
	CT_PRIVATE_CHAT                      	=	1,           	//私聊
	//CT_MAP_BRO_CHAT                      	=	2,           	//地图内喊话，玩家没有地图内喊话，只有mapsrv可以进行此喊话；
	CT_WORLD_CHAT                        	=	8,           	//全世界广播
	CT_TEAM_CHAT                         	=	9,           	//组队广播
	CT_PRIVATE_IRC_CHAT                  	=	11,          	//IRC私聊
	CT_WORLD_IRC_CHAT                    	=	12,          	//IRC世界聊天
	CT_UNION_CHAT                        	=	13,          	//工会聊天
	CT_UNION_IRC_CHAT                    	=	14,          	//工会IRC聊天
	CT_RACE_BRO_CHAT                    	=	17,          	//种族喊话
	*/
	if ( ( chatInfo->nType != CT_SCOPE_CHAT ) 
		&& ( chatInfo->nType != CT_PRIVATE_CHAT ) 
		&& ( chatInfo->nType != CT_RACE_BRO_CHAT ) 
		&& ( chatInfo->nType != CT_WORLD_CHAT ) 
		&& ( chatInfo->nType != CT_TEAM_CHAT ) 
		&& ( chatInfo->nType != CT_PRIVATE_IRC_CHAT ) 
		&& ( chatInfo->nType != CT_WORLD_IRC_CHAT ) 
		&& ( chatInfo->nType != CT_UNION_CHAT ) 
		&& ( chatInfo->nType != CT_UNION_IRC_CHAT ) 
		)
	{
		D_WARNING( "玩家%s发来非法的聊天类型%d，踢其下线\n", pOwner->GetAccount(), chatInfo->nType );
		pOwner->ReqDestorySelf();
		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//处理GM恢复玩家数据指令;
	if ( '@' == chatInfo->strChat[0] )
	{
		const char* restoregmcmd = "@restoreplayer ";
		const size_t cmplen = strlen( restoregmcmd );
		if ( strlen( chatInfo->strChat ) > cmplen )
		{
			bool isrestorecmd = true;//是否为恢复GM指令；
			for ( size_t i=0; i<cmplen; ++i )
			{
				if ( chatInfo->strChat[i] != restoregmcmd[i] )
				{
					isrestorecmd = false;
					break;
				}
			}

			if ( isrestorecmd )
			{
				//确实为恢复gm指令，找出恢复文件名执行恢复操作；
				NoDBPlayerInfoRestore( (const char*) (&(chatInfo->strChat[cmplen])) );
				return true;
			}
		}
#ifdef PKG_STAT
		const char* logpkgstatcmd = "@logpkgstat";
		if ( strcmp(logpkgstatcmd,chatInfo->strChat) == 0 )
		{
			PkgStatManager::LogStatResult();
			return true;
		}
#endif
	}
	//处理GM恢复玩家数据指令;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	//聊天处理过程...
	//1.若为组队聊天，则转关系服务器并返回；
	//2.若为世界聊天，则转centersrv并返回；
	//3.若为密聊，则转centersrv并返回；
	//4.地图内聊天，转mapsrv并返回；
	//5.工会聊天，转关系服务器并返回
	//...聊天处理过程
	////////////////////////////////////////////////////////////////////////////	

	//1.若为组队聊天，则转关系服务器并返回；
	if ( CT_TEAM_CHAT == chatInfo->nType )
	{
		if( !pOwner->IsInTeam() )
		{
			//......
			return false;
		}
		PlayerID tmpID = pOwner->GetPlayerID();

		SafeStruct( TeamBroadcastRequest, teamchatReq );
		teamchatReq.fromGateId = tmpID.wGID;
		teamchatReq.fromPlayerId = tmpID.dwPID;
		SafeStrCpy( teamchatReq.fromMemberInd.memberInd, pOwner->GetRoleName() );
		teamchatReq.fromMemberInd.memberIndLen = (UINT)strlen(teamchatReq.fromMemberInd.memberInd);
		SafeStrCpy( teamchatReq.broadcastData.data, chatInfo->strChat );
		teamchatReq.broadcastData.dataLen = (UINT)strlen(teamchatReq.broadcastData.data) + 1;
		teamchatReq.teamInd = pOwner->GetTeamId();	
		teamchatReq.requestSeq = 0;
		teamchatReq.dataType = 0;	//暂时定聊天
		SendMsgToRelationSrv( TeamBroadcastRequest, teamchatReq );
		D_DEBUG("小队聊天发送\n");
		return true;
	}

	//2.若为IRC世界聊天，则验证权限，转centersrv并返回(IRC世界聊天暂时不扣物品)；
	if ( /*CT_WORLD_CHAT == chatInfo->nType || 普通世界聊天要转mapsrv扣物品*/( CT_WORLD_IRC_CHAT == chatInfo->nType && pOwner->IsIrcPlayer()) )
	{
		GESrvGroupChat srvGroupChat;
		srvGroupChat.chatType = chatInfo->nType;
		srvGroupChat.extInfo = 0;//无额外信息；
		SafeStrCpy( srvGroupChat.chatPlayerName, pOwner->GetRoleName() );
		SafeStrCpy( srvGroupChat.chatContent, chatInfo->strChat );

		//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		//MsgToPut* pBrocastMsg = CreateSrvPkg( GESrvGroupChat, pCenterSrv, srvGroupChat );
		//pCenterSrv->SendPkgToSrv( pBrocastMsg );//送centersrv转发；
		SendMsgToCenterSrv( GESrvGroupChat, srvGroupChat );

		return true;
	}

	//3.若为密聊（没有指定聊天对象的PlayerID），则验证权限，然后转centersrv并返回；
	if ( CT_PRIVATE_CHAT == chatInfo->nType || ( CT_PRIVATE_IRC_CHAT == chatInfo->nType && pOwner->IsIrcPlayer() ) )
	{
		GEPrivateChat privateChat;
		privateChat.chatType = chatInfo->nType;
		SafeStrCpy( privateChat.chatPlayerName, pOwner->GetRoleName() );//说话者昵称；
		SafeStrCpy( privateChat.tgtPlayerName, chatInfo->tgtName );//说话对象昵称；
		SafeStrCpy( privateChat.chatContent, chatInfo->strChat );//对话内容；

		//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		//MsgToPut* pPrivateMsg = CreateSrvPkg( GEPrivateChat, pCenterSrv, privateChat );
		//pCenterSrv->SendPkgToSrv( pPrivateMsg );//送centersrv转发；
		SendMsgToCenterSrv( GEPrivateChat, privateChat );

		return true;
	}

	//若为工会聊天，则转关系服务器并返回；
	if ( (CT_UNION_CHAT == chatInfo->nType) || (CT_UNION_IRC_CHAT ==chatInfo->nType && pOwner->IsIrcPlayer()) )
	{
		UnionChannelSpeekRequest req;
		StructMemSet(req, 0x00, sizeof(req));

		req.uiid = pOwner->GetSeledRole();
		if(chatInfo->chatSize < sizeof(req.content))
		{
			req.contentLen = chatInfo->chatSize;
			StructMemCpy(req.content, chatInfo->strChat, chatInfo->chatSize);
		}
		else
		{
			req.contentLen = sizeof(req.content);
			StructMemCpy(req.content, chatInfo->strChat, sizeof(req.content));
		}

		SendMsgToRelationSrv( UnionChannelSpeekRequest, req );
		return true;
	}

	//4.地图内聊天，转mapsrv并返回；
	if ( ( chatInfo->nType != CT_SCOPE_CHAT ) 
		&& ( chatInfo->nType != CT_RACE_BRO_CHAT ) 
		&& ( chatInfo->nType != CT_WORLD_CHAT ) 
		)
	{
		//需要再校验一次，发给mapsrv的只可能是这几种，错误情况例如：发来IRC聊天类型，但其当前并非IRC玩家等；
		D_WARNING( "玩家%s发来非法的聊天类型%d，可能是非IRC玩家发IRC聊天消息，踢其下线\n", pOwner->GetAccount(), chatInfo->nType );
		pOwner->ReqDestorySelf();
		return false;
	}

	GMChat srvmsg;
	srvmsg.nType = chatInfo->nType;
	srvmsg.usedItemID = chatInfo->usedItemID;
	srvmsg.sourcePlayerID = pOwner->GetPlayerID();
	srvmsg.targetPlayerID = chatInfo->targetPlayerID;
	SafeStrCpy( srvmsg.strChat, chatInfo->strChat );

	if ( (CT_WORLD_CHAT == srvmsg.nType) 
		|| (CT_RACE_BRO_CHAT == srvmsg.nType) 
		)
	{
		///世界聊天或地图内广播，必须要使用物品，否则不允许；
		if ( 0 == srvmsg.usedItemID )
		{
			D_WARNING( "%s世界或地图内喊话，没有使用道具，喊话失败\n", pOwner->GetAccount() );
			return false;
		}
	}

	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		//MsgToPut* pNewMsg = CreateSrvPkg( GMChat, pMapsrv, gmChat );
		//pMapsrv->SendPkgToSrv( pNewMsg );
		GateSrvSendMsgToMapSrv( GMChat );
	} else {
		D_WARNING( "收到玩家聊天消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}

///心跳检测包；
bool CDealPlayerPkg::OnPlayerTimeCheck( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerTimeCheck,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGTimeCheck, timeCheck, pPkg, wPkgLen );
	//if ( wPkgLen != sizeof(CGTimeCheck) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}
	//const CGTimeCheck* timeCheck = (const CGTimeCheck*) pPkg;

	//D_DEBUG( "收到客户端%s发来的心跳包%d\n", pOwner->GetAccount(), timeCheck->checkID  );

	GCTimeCheck gcMsg;
	gcMsg.checkID = timeCheck->checkID;
	gcMsg.sequenceID = timeCheck->sequenceID;

	NewSendPlayerPkg( GCTimeCheck, pOwner, gcMsg );
	//MsgToPut* returnMsg = CreatePlayerPkg( GCTimeCheck, pOwner, gcMsg );
	//if ( NULL != returnMsg )
	//{
	//	pOwner->SendPkgToPlayer( returnMsg );
	//}

	//if ( true )
	//{
	//	//打出收到的非机器人心跳消息，调试主动断开正常客户端问题, by dzj 09.01.20；
	//	const char* playerAccount = pOwner->GetAccount();
	//	if ( NULL != playerAccount )
	//	{			
	//		size_t namelen = strlen( playerAccount );
	//		if ( namelen < 5 )
	//		{
	//			D_DEBUG( "收到客户端%s发来心跳消息，编号:%d\n", playerAccount, gcMsg.checkID );
	//		}
	//	}
	//}

	pOwner->SetBeatRcved();

	return true;
	TRY_END;
	return false;
}

///玩家请求某怪物的相关信息（用于界面显示）；
bool CDealPlayerPkg::OnPlayerReqMonsterInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerReqMonsterInfo,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGReqMonsterInfo, reqRoleInfo, pPkg, wPkgLen );
	//if ( wPkgLen != sizeof(CGReqMonsterInfo) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}
	//const CGReqMonsterInfo* reqRoleInfo = (const CGReqMonsterInfo*) pPkg;

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该请求怪物信息\n" );
		return false;
	}

	GMReqMonsterInfo srvmsg;
	srvmsg.lSelfPlayerID = pOwner->GetPlayerID();
	srvmsg.lMonsterID = reqRoleInfo->lMonsterID;

	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		//MsgToPut* pNewMsg = CreateSrvPkg( GMReqMonsterInfo, pMapsrv, gmReqMonsterInfo );
		//pMapsrv->SendPkgToSrv( pNewMsg );
		GateSrvSendMsgToMapSrv( GMReqMonsterInfo );
	} else {
		D_WARNING( "收到请求怪物信息消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}

///玩家攻击玩家
bool CDealPlayerPkg::OnAttackPlayer( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnAttackPlayer,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGAttackPlayer, attackPlayer, pPkg, wPkgLen );
	//if ( wPkgLen != sizeof(CGAttackPlayer) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}
	//const CGAttackPlayer* attackPlayer = (const CGAttackPlayer*) pPkg;

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该攻击其它玩家\n" );
		return false;
	}

	if ( attackPlayer->attackerID != pOwner->GetPlayerID() )
	{
		D_WARNING( "收到客户端:%x攻击玩家消息，消息中的攻击者非其本人\n\n", ((unsigned long)(DWORD_PTR)pOwner) );
	}

	GMAttackPlayer srvmsg;
	srvmsg.attackerID = attackPlayer->attackerID;
	srvmsg.targetID = attackPlayer->targetID;
	srvmsg.lSkillType = attackPlayer->lSkillType;
	srvmsg.fAttackerPosX = attackPlayer->nAttackerPosX;
	srvmsg.fAttackerPosY = attackPlayer->nAttackerPosY;
	srvmsg.sequenceID = attackPlayer->sequenceID;

	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		//MsgToPut* pNewMsg = CreateSrvPkg( GMAttackPlayer, pMapsrv, gmAttack );
		//pMapsrv->SendPkgToSrv( pNewMsg );
		GateSrvSendMsgToMapSrv( GMAttackPlayer );
	} else {
		D_WARNING( "收到玩家攻击玩家消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}




///玩家跳地图进入新地图请求;
bool CDealPlayerPkg::OnPlayerSwitchEnterNewMap( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerSwitchEnterNewMap,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGSwitchEnterNewMap, pSwitchEnterNewMap, pPkg, wPkgLen );
	//if ( wPkgLen != sizeof(CGSwitchEnterNewMap) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}

	if ( !(pOwner->IsInSwitching()) )
	{
		D_ERROR( "收到玩家%s跳地图完成消息时，发现该玩家不处于地图跳转状态\n", pOwner->GetAccount() );
		return false;
	}

	//D_DEBUG( "收到玩家%s进入新地图请求\n", pOwner->GetAccount() );

	pOwner->SwitchPlayerEnterNewMap( pSwitchEnterNewMap->sequenceID );//跳地图过程中，玩家请求进入新地图;

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnPlayerEnterWorld( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerEnterWorld,pOwner空\n" );
		return false;
	}

	if ( pOwner->IsDbPreSend() )
	{
		D_ERROR( "玩家%s重复进世界!!\n", pOwner->GetAccount() );
		return false;
	}

	DealPlayerPkgPre( CGEnterWorld, pEnterWorld, pPkg, wPkgLen );

	D_DEBUG( "收到玩家%s进入世界请求\n", pOwner->GetAccount() );

	//向dbsrv请求玩家详细信息；
	if ( !pOwner->SetCurStat( PS_QUERY_DETAIL_INFO ) )
	{
		//阶段校验失败;
		D_WARNING( "玩家%s进入世界失败，断开连接\n", pOwner->GetAccount() );
		pOwner->ReqDestorySelf();//断开连接；
		return false;
	}

	//清空DGGet标记，确保读取新的角色信息正确
	pOwner->ResetDBGetFlag();//清存DB信息；
	pOwner->ClearNDSMRInfo();//清非存盘，但跨地图保存信息；

	//向DBSrv请求玩家角色等信息；
	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pOwner->GetAccount() );
	if ( NULL == pDbSrv )
	{
		GCSrvError srvError;
		srvError.errNO = 1001;
		NewSendPlayerPkg( GCSrvError, pOwner, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pOwner, srvError );
		//pOwner->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的mapsrv;
		D_WARNING( "玩家发进入世界请求时，找不到玩家所在的dbsrvd，取角色详细信息失败\n\n" );
		return false;
	}
	GDPlayerInfo playerInfo;
	playerInfo.sequenceID = pEnterWorld->sequenceID;  //网络序列ID
	playerInfo.playerID = pOwner->GetPlayerID();
	SafeStrCpy( playerInfo.szAccount, pOwner->GetAccount() );
	SafeStrCpy( playerInfo.strNickName, pOwner->GetRoleName() );
	playerInfo.uiRoleID = pOwner->GetSeledRole();
	//MsgToPut* pReqDetailInfo = CreateSrvPkg( GDPlayerInfo, pDbSrv, playerInfo );
	//pDbSrv->SendPkgToSrv( pReqDetailInfo );
	SendMsgToSrv( GDPlayerInfo, pDbSrv, playerInfo );
	//#endif //ifdef WAIT_QUEUE

	return true;
	TRY_END;
	return false;
}

///Register( C_G_HONOR_MASK, &OnHonorMask );//玩家选择显示的称号
bool CDealPlayerPkg::OnHonorMask( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealPlayerPkg::OnHonorMask,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGHonorMask, pHonorMask, pPkg, wPkgLen );

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该切换所使用的称号\n" );
		return false;
	}

	//消息转发mapsrv;
	GMHonorMask  srvmsg;
	srvmsg.honorMask = pHonorMask->honorMask;
	srvmsg.playerID = pOwner->GetPlayerID();//填入玩家在本GateSrv上的ID号；
	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		GateSrvSendMsgToMapSrv( GMHonorMask );
	} else {
		D_WARNING( "收到切换称号消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}


///玩家跑动消息处理;
bool CDealPlayerPkg::OnPlayerRun( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerRun,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGRun, pRun, pPkg, wPkgLen );


	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该进行跑动\n" );
		return false;
	}

	//D_INFO("收到CGRun消息,玩家(%f,%f) 目标点(%f,%f) 序列:%d \n",pRun->fOrgWorldPosX, pRun->fOrgWorldPosY, pRun->fNewWorldPosX, pRun->fNewWorldPosY, pRun->sequenceID );

	//消息转发mapsrv;
	GMRun  srvmsg;
	srvmsg.sequenceID = pRun->sequenceID;
	srvmsg.lPlayerID = pOwner->GetPlayerID();//填入玩家在本GateSrv上的ID号；
	srvmsg.fCurWorldPosX = pRun->fOrgWorldPosX;
	srvmsg.fCurWorldPosY = pRun->fOrgWorldPosY;
	srvmsg.fTargetWorldPosX = pRun->fNewWorldPosX;
	srvmsg.fTargetWorldPosY = pRun->fNewWorldPosY;
	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		GateSrvSendMsgToMapSrv( GMRun );
	} 
	else 
	{
		D_WARNING( "收到玩家跑动消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}
	return true;
	TRY_END;
	return false;
}

///玩家跑动中跨块；
bool CDealPlayerPkg::OnPlayerBeyondBlock( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerBeyondBlock,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGPlayerBeyondBlock, pBeyondBlock, pPkg, wPkgLen );

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该有跑动跨块\n" );
		return false;
	}

	//消息转发mapsrv;
	GMPlayerBeyondBlock  srvmsg;
	srvmsg.movePlayerID = pOwner->GetPlayerID();//填入玩家在本GateSrv上的ID号；
	srvmsg.fCurX = pBeyondBlock->fCurX;
	srvmsg.fCurY = pBeyondBlock->fCurY;
	srvmsg.fTargetX = pBeyondBlock->fTargetX;
	srvmsg.fTargetY = pBeyondBlock->fTargetY;
	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		GateSrvSendMsgToMapSrv( GMPlayerBeyondBlock );
	} 
	else 
	{
		D_WARNING( "收到玩家移动跨块消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;

}

bool CDealPlayerPkg::OnQueryItemPkgInfo( CPlayer* pOwner, const char* pPkg ,const unsigned short wPkgLen )
{

	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnQueryItemPkgInfo,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGQueryItemPackageInfo, queryInfo, pPkg, wPkgLen );

	//D_DEBUG("玩家查询%d 包裹道具信息\n",queryInfo->ItemPackageID );

	GMQueryItemPkg srvmsg;
	srvmsg.lplayerID = pOwner->GetPlayerID();
	srvmsg.pkgID = queryInfo->ItemPackageID;
	srvmsg.queryType = queryInfo->queryType;
	GateSrvSendMsgToMapSrv(GMQueryItemPkg);
	return true;

	return false;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnPickOneItem(CPlayer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;	

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPickOneItem,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGPickItem, pickmsg, pPkg, wPkgLen );

	GMPickItem srvmsg;
	srvmsg.ItemID =  pickmsg->ItemTypeID;
	srvmsg.pkgID  = pickmsg->ItemPackageID;
	srvmsg.playID =  pOwner->GetPlayerID();
	srvmsg.ItemPkgIndex = pickmsg->ItemPkgIndex;

	//D_DEBUG("拾取了道具包中的道具 ItemID %d pkgID:%d\n",srvmsg.ItemID, srvmsg.pkgID );

	GateSrvSendMsgToMapSrv( GMPickItem );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnPickAllItem(CPlayer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;	

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPickAllItem,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGPickAllItems, pickmsg, pPkg, wPkgLen );

	GMPickAllItem srvmsg;
	srvmsg.pkgID  = pickmsg->ItemPackageID;
	srvmsg.playID = pOwner->GetPlayerID();


	GateSrvSendMsgToMapSrv( GMPickAllItem );

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnUseItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnUseItem,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGUseItem, pickmsg, pPkg, wPkgLen );

	GMUseItem srvmsg;
	srvmsg.itemUID = pickmsg->itemUID;
	srvmsg.lPlayerID = pOwner->GetPlayerID();
	srvmsg.itemIndex = pickmsg->itemIndex;


	GateSrvSendMsgToMapSrv(GMUseItem);

	return true;

	TRY_END;

	return false;
}


bool CDealPlayerPkg::OnUnUseItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnUseItem,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGUnUseItem, pUnUse, pPkg, wPkgLen );

	GMUnUseItem srvmsg;
	srvmsg.from = pUnUse->From;
	srvmsg.to = pUnUse->To;
	srvmsg.unUsePlayer = pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv( GMUnUseItem );

	return true;
	TRY_END;
	return true;
}

bool CDealPlayerPkg::OnDropItem(CPlayer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnDropItem,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGDropItem, pDrop, pPkg, wPkgLen );

	GMPlayerDropItem srvmsg;
	srvmsg.dropPlayer = pOwner->GetPlayerID();
	srvmsg.itemUID = pDrop->itemUID;
	srvmsg.itemIndex = pDrop->itemIndex;

	GateSrvSendMsgToMapSrv( GMPlayerDropItem );

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnSwapItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnSwapItem,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGSWAPITEM, pSwap, pPkg, wPkgLen );

	GMSwapItem srvmsg;
	srvmsg.from = pSwap->pkgFrom;
	srvmsg.to   = pSwap->pkgTo;
	srvmsg.playerID =  pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv( GMSwapItem );

	return true;

	TRY_END;

	return false;
}

//上马操作
bool CDealPlayerPkg::OnActiveMountItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	//PKG_CONVERT(CGActiveMount);
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnActiveMountItem,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGActiveMount, pMsg, pPkg, wPkgLen );

	GMActiveMount srvmsg;
	srvmsg.itemUID = pMsg->itemUID;
	srvmsg.playerID = pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv( GMActiveMount );

	return true;
	TRY_END;
	return false;
}

//邀请别人上马
bool CDealPlayerPkg::OnInvitePlayerToRideTeam(CPlayer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	//PKG_CONVERT( CGInviteMount );
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnInvitePlayerToRideTeam,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGInviteMount, pMsg, pPkg, wPkgLen );

	GMInviteMount srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.targetID = pMsg->targetID;

	GateSrvSendMsgToMapSrv( GMInviteMount );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnReturnInviteRideTeamResult(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	//PKG_CONVERT( CGAgreeBeInvite);
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnReturnInviteRideTeamResult,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGAgreeBeInvite, pMsg, pPkg, wPkgLen );

	GMAgreeBeInvite srvmsg;
	srvmsg.isAgree =  pMsg->isAgree;
	srvmsg.launchID = pMsg->launchID;
	srvmsg.playerID = pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv(GMAgreeBeInvite);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnLeaveRideTeam(CPlayer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnLeaveRideTeam,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGLeaveMountTeam, pMsg, pPkg, wPkgLen ); ACE_UNUSED_ARG( pMsg );

	GMLeaveMountTeam srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();
	GateSrvSendMsgToMapSrv(GMLeaveMountTeam);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnSplitItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnSplitItem ,pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGSplitItem, pMsg, pPkg, wPkgLen );

	GMSplitItem srvmsg;
	srvmsg.splitPlayer = pOwner->GetPlayerID();
	srvmsg.itemUID = pMsg->ItemPkgIndex;
	srvmsg.splitNum = pMsg->SplitNum;

	GateSrvSendMsgToMapSrv(GMSplitItem);

	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnEquipItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnEquipItem ,pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGEquipItem, pMsg, pPkg, wPkgLen );

	GMEquipItem srvmsg;
	srvmsg.lPlayerID = pOwner->GetPlayerID();
	srvmsg.equipitemUID =  pMsg->equipItemUID;
	srvmsg.equipIndex = pMsg->equipIndex;
	srvmsg.equipItemIndex = pMsg->equipItemIndex;
	srvmsg.equipReturnIndex = pMsg->equipReturnIndex;

	GateSrvSendMsgToMapSrv(GMEquipItem);
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnReqBuffEnd( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnReqBuffEnd,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGReqBuffEnd, pMsg, pPkg, wPkgLen );

	GMReqBuffEnd ge;
	ge.buffIndex = pMsg->buffIndex;
	ge.changePlayer = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMReqBuffEnd, pOwner, ge );

	return true;
	TRY_END;
	return false;
};


bool CDealPlayerPkg::UseAssistSkill( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "UseAssistSkill,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGUseAssistSkill, pMsg, pPkg, wPkgLen );

	GMUseAssistSkill srvmsg;
	srvmsg.targetPlayer = pMsg->targetPlayer;
	srvmsg.skillID = pMsg->skillID;
	srvmsg.useSkillPlayer = pOwner->GetPlayerID();
	srvmsg.bPlayer = pMsg->bPlayer;
	srvmsg.isUnionSkill = pMsg->isUnionSkill;
	GateSrvSendMsgToMapSrv( GMUseAssistSkill );

	return true;
	TRY_END;
	return false;
}

//Register( C_G_ITEM_UPGRADE_PLATE, &OnCGItemUpgradePlate );//弹物品升级托盘
bool CDealPlayerPkg::OnCGItemUpgradePlate( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnCGItemUpgradePlate,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGItemUpgradePlate, pPlateReq, pPkg, wPkgLen );

	GMItemUpgradePlate srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	GateSrvSendMsgToMapSrv(GMItemUpgradePlate);

	return true;
	TRY_END;
	return false;
}

//往托盘中放物品, CGAddSthToPlate
bool CDealPlayerPkg::OnAddSthToPlate( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddSthToPlate,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddSthToPlate, pAddSthToPlate, pPkg, wPkgLen );

	GMAddSthToPlate srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	srvmsg.plateType = pAddSthToPlate->plateType;
	srvmsg.platePos  = pAddSthToPlate->platePos;
	srvmsg.sthID     = pAddSthToPlate->sthID;

	GateSrvSendMsgToMapSrv(GMAddSthToPlate);

	return true;
	TRY_END;
	return false;
}

//从托盘中取物品,CGTakeSthFromPlate
bool CDealPlayerPkg::OnTakeSthFromPlate( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnTakeSthFromPlate,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGTakeSthFromPlate, pTakeSthFromPlate, pPkg, wPkgLen );

	GMTakeSthFromPlate srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	srvmsg.platePos  = pTakeSthFromPlate->platePos;
	srvmsg.sthID     = pTakeSthFromPlate->sthID;

	GateSrvSendMsgToMapSrv(GMTakeSthFromPlate);

	return true;
	TRY_END;
	return false;
}

//尝试托盘操作,CGPlateExec
bool CDealPlayerPkg::OnPlateExec( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlateExec,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPlateExec, pPlateExec, pPkg, wPkgLen ); ACE_UNUSED_ARG( pPlateExec );

	GMPlateExec srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv(GMPlateExec);

	return true;
	TRY_END;
	return false;
}

//关托盘,CGClosePlate
bool CDealPlayerPkg::OnClosePlate( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnClosePlate,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGClosePlate, pClosePlate, pPkg, wPkgLen ); ACE_UNUSED_ARG( pClosePlate );

	GMClosePlate srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv(GMClosePlate);

	return true;
	TRY_END;
	return false;}

bool CDealPlayerPkg::OnRepairWearByItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRepairWearByItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGRepairWearByItem, pRepairWearByItem, pPkg, wPkgLen );

	GMRepairWearByItem srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	srvmsg.itemRepairman = pRepairWearByItem->itemRepairman;
	srvmsg.repairTarget = pRepairWearByItem->repairTarget;

	GateSrvSendMsgToMapSrv(GMRepairWearByItem);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnTimedWearIsZero( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnTimedWearIsZero,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGTimedWearIsZero, pTimedWearIsZero, pPkg, wPkgLen );

	GMTimedWearIsZero srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	srvmsg.itemID = pTimedWearIsZero->uiID;
	GateSrvSendMsgToMapSrv(GMTimedWearIsZero);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnCGAddSpSkillReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnCGAddSpSkillReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddSpSkillReq, pReq, pPkg, wPkgLen );

	GMAddSpSkillReq srvReq;
	srvReq.spSkillID = pReq->skillID;
	srvReq.reqPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMAddSpSkillReq, pOwner, srvReq );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnUpgradeSkillReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnUpgradeSkillReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGUpgradeSkill, pReq, pPkg, wPkgLen );

	GMUpgradeSkill reqMsg;
	reqMsg.reqPlayerID = pOwner->GetPlayerID();
	reqMsg.skillID = pReq->skillID;
	SendMsgToMapSrvByPlayer( GMUpgradeSkill, pOwner, reqMsg );


	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnDeleteTaskReq(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnDeleteTaskReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDeleteTask, pReq, pPkg, wPkgLen );

	GMDeleteTask reqMsg;
	reqMsg.playerID = pOwner->GetPlayerID();
	reqMsg.TaskID = pReq->taskID;
	reqMsg.taskType = pReq->taskType;
	SendMsgToMapSrvByPlayer( GMDeleteTask, pOwner, reqMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnGetClientData(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnGetClientData,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGGetClientData, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pOwner->GetAccount() );
	if ( NULL == pDbSrv )
	{
		GCSrvError srvError;
		srvError.errNO = 1001;

		NewSendPlayerPkg( GCSrvError, pOwner, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pOwner, srvError );
		//if ( NULL != pOwner )
		//{
		//	pOwner->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的dbsrv;
		//}
		D_WARNING( "获取玩家的客户信息时，找不到玩家所在的dbsrvd\n\n" );
		return false;
	}

	GDGetClientData getClientData;
	getClientData.playerID = pOwner->GetPlayerID();
	getClientData.uiRoleID = pOwner->GetSeledRole();

	//MsgToPut* pGetClientMsg = CreateSrvPkg( GDGetClientData, pDbSrv, getClientData );
	//pDbSrv->SendPkgToSrv( pGetClientMsg );
	SendMsgToSrv( GDGetClientData, pDbSrv, getClientData );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnSaveClientData(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnSaveClientData,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSaveClientData, pReq, pPkg, wPkgLen );

	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pOwner->GetAccount() );
	if ( NULL == pDbSrv )
	{
		GCSrvError srvError;
		srvError.errNO = 1001;

		NewSendPlayerPkg( GCSrvError, pOwner, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pOwner, srvError );
		//if ( NULL != pOwner )
		//{
		//	pOwner->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的dbsrv;
		//}
		D_WARNING( "保存玩家的客户信息时，找不到玩家所在的dbsrvd\n\n" );
		return false;
	}

	GDSaveClientData saveClientData;
	saveClientData.playerID = pOwner->GetPlayerID();
	saveClientData.uiRoleID = pOwner->GetSeledRole();

	StructMemCpy(saveClientData.clientData, pReq->clientData, sizeof(saveClientData.clientData));
	saveClientData.dataSize = pReq->dataSize;
	saveClientData.index = pReq->index;
	saveClientData.isOK = pReq->isOK;

	//MsgToPut* pSaveClientMsg = CreateSrvPkg( GDSaveClientData, pDbSrv, saveClientData );
	//pDbSrv->SendPkgToSrv( pSaveClientMsg );
	SendMsgToSrv( GDSaveClientData, pDbSrv, saveClientData );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnClientSynchTime(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnClientSynchTime,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGClinetSynchTime, pReq, pPkg, wPkgLen );
	pOwner->RecClientSynchTimeMsg( pReq->timeSynchID );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnCreateChatGroup(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnCreateChatGroup,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGCreateChatGroup, pReq, pPkg, wPkgLen );

	CreateChatGroupRequest req;
	StructMemSet(req, 0x00, sizeof(req));

	req.group.owner.gateId = pOwner->GetPlayerID().wGID;
	req.group.owner.playerId = pOwner->GetPlayerID().dwPID;
	const char * pNickname = pOwner->GetRoleName();
	size_t nicknameLen = strlen(pNickname);
	StructMemCpy(req.group.owner.memberId.memberInd, pNickname, sizeof(req.group.owner.memberId.memberInd));
	if (sizeof(req.group.owner.memberId.memberInd) > nicknameLen)
	{
		req.group.owner.memberId.memberIndLen  = (USHORT)nicknameLen;
	} else {
		req.group.owner.memberId.memberIndLen = sizeof(req.group.owner.memberId.memberInd);
	}

	if(pReq->chatGroupPasswdSize <= sizeof(req.group.password.password))
	{
		req.group.password.passwordLen = pReq->chatGroupPasswdSize ;
	} else {
		req.group.password.passwordLen = sizeof(req.group.password.password);
	}

	StructMemCpy(req.group.password.password, pReq->charGroupPasswd, req.group.password.passwordLen);

	SendMsgToRelationSrv(CreateChatGroupRequest, req);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnDestroyChatGroup(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnDestroyChatGroup,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDestroyChatGroup, pReq, pPkg, wPkgLen );

	DestroyChatGroupRequest req;
	StructMemSet(req, 0x00, sizeof(req));

	req.gateId = pOwner->GetPlayerID().wGID;
	req.playerId = pOwner->GetPlayerID().dwPID;
	req.groupId = pReq->chatGroupID;

	SendMsgToRelationSrv(DestroyChatGroupRequest,req);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnAddChatGroupMemberByOwner(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddChatGroupMemberByOwner,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddChatGroupMemberByOwner, pReq, pPkg, wPkgLen );

	// 要加入的玩家是否存在？
	GEAddChatGroupMemberByOwner req;
	StructMemSet(req, 0x00, sizeof(req));

	req.chatGroupID = pReq->chatGroupID;
	req.reqPlayerID = pOwner->GetPlayerID();
	SafeStrCpy(req.targetPlayerName, pReq->memberName);

	SendMsgToCenterSrv(GEAddChatGroupMemberByOwner, req);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnAddChatGroupMember(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddChatGroupMember,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddChatGroupMember, pReq, pPkg, wPkgLen );

	AddChatGroupMemberRequest req;
	StructMemSet(req, 0x00, sizeof(req));

	req.gateId = pOwner->GetPlayerID().wGID;
	req.groupId = pReq->chatGroupID;
	req.playerId = pOwner->GetPlayerID().dwPID ;
	req.memberAdded.gateId = pOwner->GetPlayerID().wGID;
	req.memberAdded.playerId = pOwner->GetPlayerID().dwPID ;

	const char * pNickname = pOwner->GetRoleName();
	size_t nicknameLen = strlen(pNickname);
	StructMemCpy(req.memberAdded.memberId.memberInd, pNickname, sizeof(req.memberAdded.memberId.memberInd));
	if(sizeof(req.memberAdded.memberId.memberInd) > nicknameLen)
	{
		req.memberAdded.memberId.memberIndLen = (USHORT)nicknameLen;
	} else {
		req.memberAdded.memberId.memberIndLen = sizeof(req.memberAdded.memberId.memberInd);
	}

	if(pReq->chatGroupPasswdSize <= sizeof(req.passwrod.password))
	{
		req.passwrod.passwordLen = pReq->chatGroupPasswdSize;
	} else {
		req.passwrod.passwordLen = sizeof(req.passwrod.password);
	}

	StructMemCpy(req.passwrod.password, pReq->charGroupPasswd, req.passwrod.passwordLen);

	SendMsgToRelationSrv(AddChatGroupMemberRequest, req);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnRemoveChatGroupMember(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRemoveChatGroupMember,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGRemoveChatGroupMember, pReq, pPkg, wPkgLen );

	RemoveChatGroupMemberRequest req;
	StructMemSet(req, 0x00, sizeof(req));

	req.groupId = pReq->chatGroupID;
	req.gateId = pOwner->GetPlayerID().wGID;
	req.groupId = pOwner->GetPlayerID().dwPID;
	req.memberRemoved.gateId  = pOwner->GetPlayerID().wGID;
	req.memberRemoved.playerId = pOwner->GetPlayerID().dwPID;
	StructMemCpy(req.memberRemoved.memberId.memberInd, pReq->memberName, sizeof(req.memberRemoved.memberId.memberInd));
	if(sizeof(req.memberRemoved.memberId.memberInd) > pReq->memberNameSize)
	{
		req.memberRemoved.memberId.memberIndLen = pReq->memberNameSize;
	} else {
		req.memberRemoved.memberId.memberIndLen = sizeof(req.memberRemoved.memberId.memberInd);
	}

	SendMsgToRelationSrv(RemoveChatGroupMemberRequest, req);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnChatGroupSpeak(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnChatGroupSpeak,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGChatGroupSpeak, pReq, pPkg, wPkgLen );

	ChatGroupSpeekRequest req;
	StructMemSet(req, 0x00, sizeof(req));

	req.groupId = pReq->chatGroupID;
	req.gateId = pOwner->GetPlayerID().wGID;
	req.playerId = pOwner->GetPlayerID().dwPID;
	SafeStrCpy(req.data.data, pReq->chatData);
	if(sizeof(req.data.data) > pReq->chatDataSize)
		req.data.dataLen = pReq->chatDataSize;
	else
		req.data.dataLen = sizeof(req.data.data);

	SendMsgToRelationSrv(ChatGroupSpeekRequest, req);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnRepairWearByNpc(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRepairWearByNpc,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGRepairWearByNPC, pReq, pPkg, wPkgLen );

	GMRepairWearByNPC srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.selfId = pOwner->GetPlayerID();
	srvmsg.repairAll = pReq->repairAll;
	srvmsg.itemID = pReq->repairTarget;

	GateSrvSendMsgToMapSrv(GMRepairWearByNPC);

	return true;

	TRY_END;

	return false;
}

//查询商店信息；
bool CDealPlayerPkg::OnQueryShop( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnQueryShop,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryShop, pReq, pPkg, wPkgLen );

	GMQueryShop srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.lQueryPlayerID = pOwner->GetPlayerID();
	srvmsg.shopID = pReq->shopID;
	srvmsg.shopPageID = pReq->shopPageID;

	GateSrvSendMsgToMapSrv(GMQueryShop);

	return true;
	TRY_END;
	return false;
}

//出售商品请求；
bool CDealPlayerPkg::OnItemSoldReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnItemSoldReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGItemSoldReq, pReq, pPkg, wPkgLen );

	GMItemSoldReq srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	/*
	unsigned long itemID;//物品类型ID号；
	unsigned long itemPos;//售出物品在包裹中的位置；
	unsigned long itemNum;//售出数量；
	unsigned long moneyNum;//本次售出希望获得的金钱数；
	*/
	srvmsg.sellerID = pOwner->GetPlayerID();
	srvmsg.itemID = pReq->itemID;
	srvmsg.itemPos = pReq->itemPos;
	srvmsg.itemNum = pReq->itemNum;
	srvmsg.silverMoneyNum = pReq->silverMoneyNum;

	GateSrvSendMsgToMapSrv(GMItemSoldReq);

	return true;
	TRY_END;
	return false;
}

//玩家请求加活点；
bool CDealPlayerPkg::OnAddAptReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddAptReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddActivePtReq, pReq, pPkg, wPkgLen );

	GMAddActivePtReq srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.reqPlayer = pOwner->GetPlayerID();
	srvmsg.reqID = pReq->reqID;
	srvmsg.nickNameSize = pReq->nickNameSize;
	SafeStrCpy( srvmsg.nickName, pReq->nickName );

	GateSrvSendMsgToMapSrv(GMAddActivePtReq);

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnUseDamageItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnUseDamageItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGUseDamageItem, pReq, pPkg, wPkgLen );

	GMUseDamageItem srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.attackPosX = pReq->attackPosX;
	srvmsg.attackPosY = pReq->attackPosY;
	srvmsg.itemUID = pReq->itemTypeID;
	srvmsg.targetID = pReq->targetID;
	srvmsg.attackPlayer = pOwner->GetPlayerID();


	GateSrvSendMsgToMapSrv(GMUseDamageItem);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryEvilTime(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner)
	{
		D_ERROR("OnQueryEvilTime,pOwner为空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryEvilTime, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	GMQueryEvilTime srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();
	GateSrvSendMsgToMapSrv( GMQueryEvilTime );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryRepairItemCost(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner)
	{
		D_ERROR("OnQueryRepairItemCost,pOwner为空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryRepairItemCost, pReq, pPkg, wPkgLen );
	ACE_UNUSED_ARG(pReq);

	GMQueryRepairItemCost srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	GateSrvSendMsgToMapSrv( GMQueryRepairItemCost );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnSwapEquipItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner)
	{
		D_ERROR("OnSwapEquipItem,pOwner为空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSwapEquipItem, pReq, pPkg, wPkgLen );

	GMSwapEquipItem srvmsg;
	srvmsg.swapPlayer = pOwner->GetPlayerID();
	srvmsg.from = pReq->equipFrom;
	srvmsg.to = pReq->equipTo;
	GateSrvSendMsgToMapSrv( GMSwapEquipItem );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryItemPkgPage(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("CDealPlayerPkg::OnQueryItemPkgPage pOwner为空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryItemPkgPage,  pReq, pPkg, wPkgLen );

	GMQueryItemPkgPage srvmsg;
	srvmsg.queryPlayer = pOwner->GetPlayerID();
	srvmsg.pageIndex = pReq->queryPage;
	GateSrvSendMsgToMapSrv( GMQueryItemPkgPage );
	return true;

	TRY_END;

	return false;

}

bool CDealPlayerPkg::OnIrcQueryPlayerInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner)
	{
		D_ERROR("OnQueryRepairItemCost,pOwner为空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGIrcQueryPlayerInfo, pReq, pPkg, wPkgLen );

	QueryPlayerInfoRequest reqmsg;
	SafeStrCpy( reqmsg.targetName, pReq->playerName );
	reqmsg.nameLen = (unsigned int)strlen( reqmsg.targetName );
	reqmsg.uiid = pOwner->GetRoleID();
	SendMsgToRelationSrv( QueryPlayerInfoRequest, reqmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnChangeBattleMode( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnChangeBattleMode,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGChangeBattleMode, pReq, pPkg, wPkgLen );

	GMChangeBattleMode srvmsg;
	srvmsg.mode = pReq->changeMode;
	srvmsg.reqID = pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv(GMChangeBattleMode);

	return true;
	TRY_END;
	return false;
}


//购买商店物品请求；
bool CDealPlayerPkg::OnShopBuyReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnShopBuyReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGShopBuyReq, pReq, pPkg, wPkgLen );

	GMShopBuyReq srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	srvmsg.lBuyerID = pOwner->GetPlayerID();
	srvmsg.shopID = pReq->shopID;
	srvmsg.shopPageID = pReq->shopPageID;
	srvmsg.shopPagePos = pReq->shopPagePos;
	srvmsg.itemID = pReq->itemID;
	srvmsg.itemNum = pReq->itemNum;
	srvmsg.moneyType = pReq->moneyType;
	srvmsg.moneyID = pReq->moneyID;
	srvmsg.moneyNum = pReq->moneyNum;

	GateSrvSendMsgToMapSrv(GMShopBuyReq);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnIrcEnterWorld( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnIrcEnterWorld,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGIrcEnterWorld, pEnterWorld, pPkg, wPkgLen ); ACE_UNUSED_ARG( pEnterWorld );

	pOwner->OnPlayerRoleOnLine( 0x02 );

	GCIrcEnterWorldRst climsg;
	climsg.bSuccess = true;

	pOwner->SetIrcFlag( true );

	NewSendPlayerPkg( GCIrcEnterWorldRst, pOwner, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg(  GCIrcEnterWorldRst, pOwner, climsg );
	//pOwner->SendPkgToPlayer( pNewMsg );
	D_DEBUG("IRC玩家%s 进入世界\n", pOwner->GetRoleName() );
	return true;

	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnPlayerChangePetName( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerChangePetName,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGChangePetName, pReq, pPkg, wPkgLen );
	GMChangePetName mapMsg;
	SafeStrCpy( mapMsg.petName, pReq->petName );
	mapMsg.ownerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMChangePetName, pOwner, mapMsg );
	D_INFO("宠物申请改名%s 长度:%d\n", pReq->petName, pReq->petNameLen );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnPlayerSummonPet( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerSummonPet,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSummonPet, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );
	GMSummonPet mapMsg;
	mapMsg.ownerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMSummonPet, pOwner, mapMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnPlayerClosePet( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerClosePet,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGClosePet, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );
	GMClosePet mapMsg;
	mapMsg.ownerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMClosePet, pOwner, mapMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnPlayerChangePetSkillState( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerChangePetSkillState,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGChangePetSkillState, pReq, pPkg, wPkgLen );
	GMChangePetSkillState mapMsg;
	mapMsg.petSkillState = pReq->skillState;
	mapMsg.ownerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMChangePetSkillState, pOwner, mapMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnPlayerChangePetImage( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerChangePetImage,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGChangePetImage, pReq, pPkg, wPkgLen );
	GMChangePetImage mapMsg;
	mapMsg.imageID = pReq->imageID;
	mapMsg.ownerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMChangePetImage, pOwner, mapMsg );

	return true;
	TRY_END;
	return false;
}

////判断是不是查询工会排行信息
//inline bool IsUnionRankChartID( char sign )
//{
//	return ( sign == '2' );
//}
//
////判断是否是商店排行
//inline bool IsShopRankChartID( char sign )
//{
//	return ( sign == '3' );
//}

bool CDealPlayerPkg::OnQueryRankInfoByPageIndex(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnQueryRankInfoByPageIndex,pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryRankInfoByPageIndex, pReq, pPkg, wPkgLen );

	if ( RANK_UNION == pReq->rankType )
	{
		QueryUnionRankRequest queryMsg;
		queryMsg.page = 0;
		queryMsg.race = 0;
		queryMsg.uiid = pOwner->GetSeledRole();
		SendMsgToRelationSrv( QueryUnionRankRequest, queryMsg );

		D_DEBUG("玩家 %s 查询工会排行榜的第%d信息 \n", pOwner->GetRoleName(), queryMsg.page );
	} else if( RANK_STRDIAM == pReq->rankType ) {//如果是星钻排行榜
		GSNewRankQuery newRankQuery;
		newRankQuery.UserId.wGID = pOwner->GetPlayerID().dwPID;
		newRankQuery.UserId.dwPID = pOwner->GetPlayerID().dwPID;
		SendMsgToShopSrv( GSNewRankQuery, newRankQuery );
		//GSShopRank querymsg;
		//querymsg.UserId.dwPID = pOwner->GetPlayerID().dwPID;
		//querymsg.UserId.wGID  = pOwner->GetPlayerID().wGID;
		//querymsg.RaceID   = pReq->rankType;
		//querymsg.RankType = pReq->rankType;//如果最后一位能被2整除，则是每周，否则为每月
		//querymsg.PageIndex= 0;//pReq->ChartPageIndex;
		//SendMsgToShopSrv( GSShopRank, querymsg );//发送给shopsrv

		//D_DEBUG("玩家 %s 查询星钻排行榜的第%d信息 排行类型:%d 排行种族:%d\n", pOwner->GetRoleName(), querymsg.PageIndex, querymsg.RankType,querymsg.RaceID );
	} else {
		GFQueryRankChartInfo queryMsg;
		queryMsg.queryPlayer = pOwner->GetPlayerID();
		queryMsg.rankType    = pReq->rankType;
		SendMsgToFunctionSrv( GFQueryRankChartInfo, queryMsg );//向functionsrv查询排行榜;
	}

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryRankInfoByPlayerName(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	D_ERROR( "CDealPlayerPkg::OnQueryRankInfoByPlayerName，客户端发来已废弃消息:根据玩家名字查询排行榜，忽略\n" );
	return true;

	if ( NULL == pOwner )
	{
		D_ERROR("OnQueryRankInfoByPlayerName,pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryRankInfoByPlayerName, pReq, pPkg, wPkgLen );

	unsigned int rankid = ACE_OS::atoi( pReq->rankChartID );
	//if ( !IsUnionRankChartID( pReq->rankChartID[2] ) )
	if ( RANK_UNION != rankid ) //OnQueryRankInfoByPageIndex中还有对星钻的判断，这里为什么没有？by dzj, 10.07.26;
	{
		GFQueryRankPlayerByName queryMsg;
		StructMemSet( queryMsg, 0x0, sizeof(queryMsg) );
		queryMsg.queryPlayerID = pOwner->GetPlayerID();
		queryMsg.rankChartID   = rankid;
		SafeStrCpy( queryMsg.queryName , pReq->playerName  );

		SendMsgToFunctionSrv( GFQueryRankPlayerByName, queryMsg );

		D_DEBUG("玩家%s搜索排行榜%d的角色%s\n", pOwner->GetRoleName(), queryMsg.rankChartID , queryMsg.queryName );	
	} else {
		SearchUnionRankRequest queryMsg;
		queryMsg.race = pOwner->GetRace();
		queryMsg.uiid = pOwner->GetSeledRole();
		SafeStrCpy( queryMsg.unionName , pReq->playerName );

		SendMsgToRelationSrv( SearchUnionRankRequest, queryMsg );

		D_DEBUG("玩家 %s 在工会排行榜搜索 %s 工会的信息\n", pOwner->GetRoleName(), queryMsg.unionName );
	}

	return true;
	TRY_END;
	return false;
}

////已废弃，原ID号改由MOUSE_TIP使用，bool CDealPlayerPkg::OnUsePartionSkill( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
//{
//	if ( NULL == pOwner )
//	{
//		D_ERROR("OnUsePartionSkill,pOwner空\n");
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	D_ERROR( "OnUsePartionSkill, 该协议已废弃，不应再收到\n" );
//
//	//DealPlayerPkgPre( CGUsePartionSkill, pReq, pPkg, wPkgLen );
//
//	//GMUsePartionSkill mapMsg;
//	//mapMsg.partionSkillId = pReq->partionSkillID;
//	//mapMsg.targetID = pReq->targetID;
//	//mapMsg.movePosX = pReq->targetPosX;
//	//mapMsg.movetPosY = pReq->targetPosY;
//	//mapMsg.attackPlayerID = pOwner->GetPlayerID();
//	//mapMsg.sequenceID = pReq->sequenceID;
//	//SendMsgToMapSrvByPlayer( GMUsePartionSkill, pOwner, mapMsg );
//
//	return true;
//	TRY_END;
//	return false;
//}
//Register( C_G_MOUSETIP_REQ, &OnMouseTipReq );//鼠标停留信息查询(HP|MP)
bool CDealPlayerPkg::OnMouseTipReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnUsePartionSkill,pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGMouseTipReq, pReq, pPkg, wPkgLen );

	GMMouseTipReq mouseTipReq;
	mouseTipReq.qryPlayerID = pOwner->GetPlayerID();
	mouseTipReq.tgtPlayerID = pReq->tgtPlayerID;

	SendMsgToMapSrvByPlayer( GMMouseTipReq, pOwner, mouseTipReq );

	return true;
	TRY_END;
	return false;
}

//请求帐户信息
bool CDealPlayerPkg::OnAccountInfoReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAccountInfoReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryAccountInfo, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	GSQueryAccountInfo srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	PlayerID playerid = pOwner->GetPlayerID();
	const char *pAccount = pOwner->GetAccount();
	size_t accountLen = strlen(pAccount);

	srvmsg.UserId.dwPID = playerid.dwPID;
	srvmsg.UserId.wGID = playerid.wGID;
	srvmsg.AccountLen = (BYTE)((accountLen <= sizeof(srvmsg.Account)) ? accountLen : sizeof(srvmsg.Account));
	StructMemCpy(srvmsg.Account, pAccount, srvmsg.AccountLen);

	SendMsgToShopSrv(GSQueryAccountInfo, srvmsg);

	return true;
	TRY_END;
	return false;
}

//购买物品
bool CDealPlayerPkg::OnPurchaseItemReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPurchaseItemReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPurchaseItem, pReq, pPkg, wPkgLen );

	//转发到mapserver判断是否包裹中足够空间
	GMCheckPkgFreeSpace checkPkgFreeSpace;
	StructMemSet(checkPkgFreeSpace, 0x00, sizeof(checkPkgFreeSpace));

	checkPkgFreeSpace.lNotiPlayerID = pOwner->GetPlayerID();
	checkPkgFreeSpace.ItemID = pReq->ItemID;
	checkPkgFreeSpace.ItemNum = pReq->ItemNum;
	checkPkgFreeSpace.CliCalc = pReq->CliCalc;
	checkPkgFreeSpace.PurchaseType = pReq->PurchaseType;

	SendMsgToMapSrvByPlayer(GMCheckPkgFreeSpace, pOwner, checkPkgFreeSpace);
	return true;
	TRY_END;
	return false;
}

//vip信息
bool CDealPlayerPkg::OnVipInfoReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnVipInfoReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryVipInfo, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	GSQueryVipInfo srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	PlayerID playerid = pOwner->GetPlayerID();
	const char *pAccount = pOwner->GetAccount();
	size_t accountLen = strlen(pAccount);

	srvmsg.UserId.dwPID = playerid.dwPID;
	srvmsg.UserId.wGID = playerid.wGID;
	srvmsg.AccountLen = (BYTE)((accountLen <= sizeof(srvmsg.Account)) ? accountLen : sizeof(srvmsg.Account));
	StructMemCpy(srvmsg.Account, pAccount, srvmsg.AccountLen);
	srvmsg.RoleID = pOwner->GetSeledRole();

	SendMsgToShopSrv(GSQueryVipInfo, srvmsg);

	return true;
	TRY_END;
	return false;
}

//购买vip
bool CDealPlayerPkg::OnPurchaseVipReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPurchaseVipReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPurchaseVip, pReq, pPkg, wPkgLen );

	GSPurchaseVip srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	PlayerID playerid = pOwner->GetPlayerID();
	const char *pAccount = pOwner->GetAccount();
	size_t accountLen = strlen(pAccount);

	srvmsg.UserId.dwPID = playerid.dwPID;
	srvmsg.UserId.wGID = playerid.wGID;
	srvmsg.AccountLen = (BYTE)((accountLen <= sizeof(srvmsg.Account)) ? accountLen : sizeof(srvmsg.Account));
	StructMemCpy(srvmsg.Account, pAccount, srvmsg.AccountLen);
	srvmsg.RoleID = pOwner->GetSeledRole();

	srvmsg.CliCalc = pReq->CliCalc;
	srvmsg.VipID = pReq->VipID;

	SendMsgToShopSrv(GSPurchaseVip, srvmsg);

	return true;
	TRY_END;
	return false;
}

//领取Vip物品
bool CDealPlayerPkg::OnFetchVipItemReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnFetchVipItemReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGFetchVipItem, pReq, pPkg, wPkgLen );

	GSFetchVipItem srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	PlayerID playerid = pOwner->GetPlayerID();
	const char *pAccount = pOwner->GetAccount();
	size_t accountLen = strlen(pAccount);

	srvmsg.UserId.dwPID = playerid.dwPID;
	srvmsg.UserId.wGID = playerid.wGID;
	srvmsg.AccountLen = (BYTE)((accountLen <= sizeof(srvmsg.Account)) ? accountLen : sizeof(srvmsg.Account));
	StructMemCpy(srvmsg.Account, pAccount, srvmsg.AccountLen);
	srvmsg.RoleID = pOwner->GetSeledRole();

	srvmsg.VipID = pReq->VipID;

	SendMsgToShopSrv(GSFetchVipItem, srvmsg);

	return true;
	TRY_END;
	return false;
}

//领取非Vip物品
bool CDealPlayerPkg::OnFetchNonVipItemReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnFetchNonVipItemReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGFetchNonVipItem, pReq, pPkg, wPkgLen );

	GSFetchCachedItem srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	PlayerID playerid = pOwner->GetPlayerID();
	const char *pAccount = pOwner->GetAccount();
	size_t accountLen = strlen(pAccount);

	srvmsg.UserId.dwPID = playerid.dwPID;
	srvmsg.UserId.wGID = playerid.wGID;
	srvmsg.AccountLen = (BYTE)((accountLen <= sizeof(srvmsg.Account)) ? accountLen : sizeof(srvmsg.Account));
	StructMemCpy(srvmsg.Account, pAccount, srvmsg.AccountLen);
	srvmsg.RoleID = pOwner->GetSeledRole();
	srvmsg.CachedItem.ItemID = pReq->itemId.ItemID;
	srvmsg.CachedItem.ItemNum = pReq->itemId.ItemNum;
	srvmsg.CachedItem.TransID = pReq->itemId.TransID;

	SendMsgToShopSrv(GSFetchCachedItem, srvmsg);

	return true;
	TRY_END;
	return false;
}

//赠送
bool CDealPlayerPkg::OnPurchaseGiftReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPurchaseGiftReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPurchaseGift, pReq, pPkg, wPkgLen );

	// 发送工会信息给shop，shop根据最新的unionid确定星钻消费是否累计
	GSPlayerGuildInfo unionInfo;
	StructMemSet(unionInfo, 0x00, sizeof(unionInfo));

	PlayerID playerid = pOwner->GetPlayerID();
	unionInfo.UserId.wGID = playerid.wGID;
	unionInfo.UserId.dwPID = playerid.dwPID;
	unionInfo.GuildNo = pOwner->GetUnionID();
	unionInfo.RoleID = pOwner->GetSeledRole();
	SendMsgToShopSrv(GSPlayerGuildInfo, unionInfo);

	//发送购买
	GSPurchaseGift srvmsg;
	StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

	//PlayerID playerid = pOwner->GetPlayerID();
	const char *pAccount = pOwner->GetAccount();
	size_t accountLen = strlen(pAccount);

	srvmsg.UserId.dwPID = playerid.dwPID;
	srvmsg.UserId.wGID = playerid.wGID;
	srvmsg.SrcAccountLen = (BYTE)((accountLen <= sizeof(srvmsg.SrcAccount)) ? accountLen : sizeof(srvmsg.SrcAccount));
	StructMemCpy(srvmsg.SrcAccount, pAccount, srvmsg.SrcAccountLen);
	srvmsg.SrcRoleID = pOwner->GetSeledRole();

	srvmsg.CliCalc = pReq->CliCalc;
	srvmsg.DestAccountLen = (BYTE)((pReq->DestAccountLen <= sizeof(srvmsg.DestAccount)) ? pReq->DestAccountLen : sizeof(srvmsg.DestAccount));
	StructMemCpy(srvmsg.DestAccount, pReq->DestAccount, srvmsg.DestAccountLen);
	srvmsg.DestRoleID = pReq->DestRoleID;
	srvmsg.ItemID = pReq->ItemID;
	srvmsg.ItemNum = pReq->ItemNum;
	srvmsg.PurchaseType = pReq->PurchaseType;

	srvmsg.NoteLen = (BYTE)(((pReq->NoteLen <= sizeof(srvmsg.Note)) ? pReq->NoteLen : sizeof(srvmsg.Note)));
	StructMemCpy(srvmsg.Note, pReq->Note, srvmsg.NoteLen);

	SendMsgToShopSrv(GSPurchaseGift, srvmsg);

	return true;
	TRY_END;
	return false;
}


//bool CDealPlayerPkg::OnPetMove( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
//{
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnPetMove,pOwner空\n" );
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	DealPlayerPkgPre( CGPetMove, pReq, pPkg, wPkgLen ); 	
//
//	return true;
//	TRY_END;
//	return false;
//}


bool CDealPlayerPkg::OnSendMailToPlayer(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnSendMailToPlayer, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSendMailToPlayer ,pReq, pPkg, wPkgLen );

	GFSendMailToPlayer srvmsg;
	StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
	srvmsg.sendPlayerID = pOwner->GetPlayerID();
	srvmsg.sendPlayerUID = pOwner->GetRoleID();

	SafeStrCpy( srvmsg.recvPlayerName , pReq->szRecvMailPlayerName );
	SafeStrCpy( srvmsg.sendMailContent, pReq->szMailContent );
	SafeStrCpy( srvmsg.sendMailTitle,   pReq->szMailTitle  );
	SafeStrCpy( srvmsg.sendPlayerName,  pOwner->GetRoleName() );

	SendMsgToFunctionSrv( GFSendMailToPlayer, srvmsg );

	return true;

	TRY_END;
	return false;
}

//玩家主动退出排队；
bool CDealPlayerPkg::OnCancelQueue( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnCancelQueue, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGCancelQueue ,pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );
	if ( PS_QUEUEING == pOwner->GetCurStat() )
	{
		//向mapsrv发退出世界消息，mapsrv收到后会在findplayer失败后去排队中删除玩家；
		GMPlayerLeave srvmsg;
		srvmsg.nSpecialFlag = GMPlayerLeave::QUIT_QUEUE;
		srvmsg.lMapCode = 0;//没有特别的地图号；
		srvmsg.lPlayerID = pOwner->GetPlayerID();
		CMapSrv* pMapSrv = pOwner->GetMapSrv();
		if ( NULL == pMapSrv )
		{
			D_WARNING( "OnCancelQueue，找不到玩家所在的mapsrv\n" );
			return false;
		}
		//MsgToPut* pNewMsg = CreateSrvPkg( GMPlayerLeave, pMapSrv, playerLeave );
		//pMapSrv->SendPkgToSrv( pNewMsg );
		GateSrvSendMsgToMapSrv( GMPlayerLeave );

		//将玩家状态重新置为选角色之前状态
		pOwner->ForceSetCurStat( PS_ROLESELED );//直接重置状态，绕过前置条件的检测，因为这里已确定当前为排队中状态，而从排队中状态转为角色已选状态，也只能在这里进行，而不能修改SetCurStat函数，导致玩家外挂发来特定消息时，影响服务器端此玩家状态与服务器逻辑。

	} else {
		//玩家不处于排队状态，可能是外挂之类发来的错误包，也可能玩家已经排到队首开始进世界了，忽略本包；
	}

	return true;
	TRY_END;
	return false;	
}


bool CDealPlayerPkg::OnReqPlayerDetailInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnReqPlayerDetailInfo, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGReqPlayerDetailInfo ,pReq, pPkg, wPkgLen );

	D_DEBUG( "OnReqPlayerDetailInfo, 玩家%s请求玩家(%d:%d)的角色详细信息\n", pOwner->GetAccount(), pReq->playerID.wGID, pReq->playerID.dwPID );

	GMReqPlayerDetailInfo reqMsg;
	reqMsg.reqPlayer = pOwner->GetPlayerID();
	reqMsg.reqTarget = pReq->playerID;
	SendMsgToMapSrvByPlayer( GMReqPlayerDetailInfo, pOwner, reqMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQuerySuitInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnQuerySuitInfo, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGQuerySuitInfo ,pReq, pPkg, wPkgLen );

	GMQuerySuitInfo reqMsg;
	StructMemSet(reqMsg, 0x00, sizeof(reqMsg));
	reqMsg.lNotiPlayerID = pOwner->GetPlayerID();
	reqMsg.index = pReq->index;

	SendMsgToMapSrvByPlayer( GMQuerySuitInfo, pOwner, reqMsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnClearSuitInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnClearSuitInfo, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGClearSuitInfo ,pReq, pPkg, wPkgLen );

	GMClearSuitInfo reqMsg;
	StructMemSet(reqMsg, 0x00, sizeof(reqMsg));
	reqMsg.lNotiPlayerID = pOwner->GetPlayerID();
	reqMsg.index = pReq->index;

	SendMsgToMapSrvByPlayer( GMClearSuitInfo, pOwner, reqMsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnSaveSuitInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnSaveSuitInfo, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGSaveSuitInfo ,pReq, pPkg, wPkgLen );

	GMSaveSuitInfo reqMsg;
	StructMemSet(reqMsg, 0x00, sizeof(reqMsg));
	reqMsg.lNotiPlayerID = pOwner->GetPlayerID();
	reqMsg.index = pReq->index;
	StructMemCpy(reqMsg.suit, &pReq->suit, sizeof(reqMsg.suit));

	SendMsgToMapSrvByPlayer( GMSaveSuitInfo, pOwner, reqMsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnChanageSuit( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnChanageSuit, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGChanageSuit ,pReq, pPkg, wPkgLen );

	GMChangeSuit reqMsg;
	StructMemSet(reqMsg, 0x00, sizeof(reqMsg));

	reqMsg.lNotiPlayerID = pOwner->GetPlayerID();
	reqMsg.index = pReq->index;
	reqMsg.repairConfirm = pReq->repairConfirm;

	SendMsgToMapSrvByPlayer( GMChangeSuit, pOwner, reqMsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnDiscardCollectItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnDiscardCollectItem, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGDiscardCollectItem ,pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );
	GMDiscardCollectItem mapMsg;
	mapMsg.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMDiscardCollectItem, pOwner, mapMsg );
	return true;

	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnPickCollectItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnPickAllCollectItem, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGPickCollectItem ,pReq, pPkg, wPkgLen );

	GMPickCollectItem mapMsg;
	mapMsg.index = pReq->index;
	mapMsg.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMPickCollectItem, pOwner, mapMsg );
	return true;

	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnPickAllCollectItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnDiscardCollectItem, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGPickAllCollectItem ,pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	GMPickAllCollectItem mapMsg;
	mapMsg.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMPickAllCollectItem, pOwner, mapMsg);
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnEquipFashionSwitch( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnEquipFashionSwitch, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGEquipFashionSwitch ,pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	GMEquipFashionSwitch mapMsg;
	mapMsg.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMEquipFashionSwitch, pOwner, mapMsg);
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnEndAnamorphic( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnEndAnamorphic, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGEndAnamorphic, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	GMEndAnamorphic reqMsg;
	StructMemSet(reqMsg, 0x00, sizeof(reqMsg));

	reqMsg.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMEndAnamorphic, pOwner, reqMsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnPutItemToProtectPlate(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnPutItemToProtectPlate, pOwner空\n");
		return false;
	}

	TRY_BEGIN;
	DealPlayerPkgPre( CGPutItemToProtectPlate ,pReq, pPkg, wPkgLen );

	GMPutItemToProtectItem putItemMsg;
	putItemMsg.itemUID =  pReq->protectItem.uiID;
	putItemMsg.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMPutItemToProtectItem, pOwner , putItemMsg );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnConfirmProtectPlate(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnConfirmProtectPlate, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGConfirmProtectPlate,pReq, pPkg, wPkgLen  );

	GMConfirmProtectPlate confirmPlateMsg;
	confirmPlateMsg.isConfirm = pReq->confirmPlate;
	confirmPlateMsg.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMConfirmProtectPlate, pOwner,confirmPlateMsg );

	return true;

	TRY_END;

	return false;
}


bool CDealPlayerPkg::OnReleaseItemSpecialProtect(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnReleaseItemSpecialProtect, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGReleaseSpecialProtect, pReq, pPkg, wPkgLen  );

	GMRleaseItemSpecialProp releaseProtect;
	releaseProtect.itemUID = pReq->protectItemUID;
	releaseProtect.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMRleaseItemSpecialProp, pOwner , releaseProtect );

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnCancelItemUnSpecialProtectWaitTime(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnCancelItemUnSpecialProtectWaitTime, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGCancelSpecialProtect,pReq, pPkg, wPkgLen   );

	GMCancelItemSpecialProp cancelProtect;
	cancelProtect.itemUID = pReq->protectItemUID;
	cancelProtect.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMCancelItemSpecialProp, pOwner, cancelProtect );

	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnTakeItemFromProtectPlate(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnTakeItemFromProtectPlate, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGTakeItemFromProtectPlate,pReq, pPkg, wPkgLen   );

	GMTakeItemFromProtectPlate takeItem;
	takeItem.itemUID = pReq->takeItemID;
	takeItem.lNotiPlayerID = pOwner->GetPlayerID();

	SendMsgToMapSrvByPlayer( GMTakeItemFromProtectPlate, pOwner, takeItem );
	return true;

	TRY_END;

	return false;
}


bool CDealPlayerPkg::OnUseSkill( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnUseSkill, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGUseSkill, pUseSkill, pPkg, wPkgLen   );

	GMUseSkill useSkill;
	useSkill.attackID = pOwner->GetPlayerID();
	useSkill.useSkill = *pUseSkill;
	SendMsgToMapSrvByPlayer( GMUseSkill, pOwner, useSkill );

	return true;
	TRY_END;
	return false;
}

//Register( C_G_USE_SKILL_2, &OnUseSkill2 );//使用技能 **新技能协议,04.29新方向，线性及其它群攻，由cli传来其预判，服务器验证；
bool CDealPlayerPkg::OnUseSkill2( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnUseSkill2, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGUseSkill2, pUseSkill, pPkg, wPkgLen   );

	GMUseSkill2 useSkill;
	useSkill.attackID = pOwner->GetPlayerID();
	useSkill.useSkill = *pUseSkill;
	SendMsgToMapSrvByPlayer( GMUseSkill2, pOwner, useSkill );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnCloseStoragePlate(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnCloseStoragePlate, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGCloseStoragePlate ,pCloseStorage, pPkg, wPkgLen );

	GMCloseStoragePlate closeStorage;
	closeStorage.playerID = pOwner->GetPlayerID();

	SendMsgToMapSrvByPlayer( GMCloseStoragePlate, pOwner,closeStorage );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnChangeStorageLockState( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnChangeStorageLockState, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGChangeStorageLockState ,pchangeLockState, pPkg, wPkgLen );

	GMChangeStorageLockState changeLockState;
	changeLockState.playerID = pOwner->GetPlayerID();

	SendMsgToMapSrvByPlayer( GMChangeStorageLockState, pOwner,changeLockState );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnTakeItemFromStorage(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnTakeItemFromStorage, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGTakeItemFromStorage ,pTakeItemStorage, pPkg, wPkgLen );

	GMTakeItemFromStorage takemsg;
	takemsg.playerID = pOwner->GetPlayerID();
	takemsg.storageIndex = pTakeItemStorage->storageIndex;

	SendMsgToMapSrvByPlayer( GMTakeItemFromStorage, pOwner,takemsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnPutItemToStorage(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnPutItemToStorage, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPutItemToStorage ,pPutItemStorage, pPkg, wPkgLen );

	GMPutItemToStorage putmsg;
	putmsg.playerID = pOwner->GetPlayerID();
	putmsg.pkgIndex = pPutItemStorage->pkgIndex;

	SendMsgToMapSrvByPlayer( GMPutItemToStorage, pOwner,putmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnTakeItemFromStorageIndexToPkgIndex( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnTakeItemFromStorageIndexToPkgIndex, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGTakeItemFromStorageIndexToPkgIndex ,pTakeItemIndex, pPkg, wPkgLen );

	GMTakeItemFromStorageIndexToPkgIndex srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.pkgIndex = pTakeItemIndex->pkgIndex;
	srvmsg.storageIndex = pTakeItemIndex->storageIndex;
	SendMsgToMapSrvByPlayer( GMTakeItemFromStorageIndexToPkgIndex, pOwner,srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnPutItemToStorageIndexFromPkgIndex(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnPutItemToStorageIndexFromPkgIndex, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPutItemFromPkgIndexToStorageIndex ,pPutItemIndex, pPkg, wPkgLen );

	GMPutItemFromPkgIndexToStorageIndex srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.pkgIndex = pPutItemIndex->pkgIndex;
	srvmsg.storageIndex = pPutItemIndex->storageIndex;
	SendMsgToMapSrvByPlayer( GMPutItemFromPkgIndexToStorageIndex, pOwner,srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnSetNewStoragePsd(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnSetNewStoragePsd, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSetNewStoragePassWord,pSetPsd, pPkg, wPkgLen );


	GMSetNewStoragePassWord srvmsg;
	StructMemSet( srvmsg, 0x0 ,sizeof(srvmsg) );
	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.securityMode  = pSetPsd->safeStrategy;
	SafeStrCpy( srvmsg.szStoragePassWord, pSetPsd->szPassWord );
	SendMsgToMapSrvByPlayer( GMSetNewStoragePassWord, pOwner,srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnResetStoragePsd( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnResetStoragePsd, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGResetStoragePassWord ,pReSetPsd, pPkg, wPkgLen );

	GMResetStoragePassWord srvmsg;
	StructMemSet( srvmsg, 0x0 ,sizeof(srvmsg) );
	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.securityMode  = pReSetPsd->safeStrategy;
	SafeStrCpy( srvmsg.szOldStoragePassWord, pReSetPsd->szOldPassWord );
	SafeStrCpy( srvmsg.szStoragePassWord,    pReSetPsd->szNewPassWord );
	SendMsgToMapSrvByPlayer( GMResetStoragePassWord, pOwner,srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnTakeItemCheckPassWord(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnTakeItemCheckPassWord, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGTakeItemCheckPassWord, pTakeCheckPsd,pPkg, wPkgLen );

	GMTakeItemCheckPassWord srvmsg;
	StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
	srvmsg.playerID = pOwner->GetPlayerID();
	SafeStrCpy( srvmsg.szStoragePassWord ,pTakeCheckPsd->szPassWord );
	SendMsgToMapSrvByPlayer( GMTakeItemCheckPassWord, pOwner, srvmsg );
	return true;

	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnChangeStorageLockStateCheckPsd(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnChangeStorageLockStateCheckPsd, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGChangeStorageStateCheckPassWord, pChangeLock,pPkg, wPkgLen  );

	GMChangeStorageStateCheckPassWord srvmsg;
	StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
	srvmsg.playerID = pOwner->GetPlayerID();
	SafeStrCpy( srvmsg.szStoragePassWord, pChangeLock->szPassWord  );
	SendMsgToMapSrvByPlayer( GMChangeStorageStateCheckPassWord, pOwner, srvmsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnExtendStorage(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnExtendStorage, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGExtendStorage, pExtendStorage, pPkg, wPkgLen );

	GMExtendStorage srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMExtendStorage , pOwner, srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnSortStorage(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnSortStorage, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSortStorage, pSortStorage ,  pPkg, wPkgLen );

	GMSortStorage srvmsg;
	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.storageLable = pSortStorage->storageLableIndex;
	SendMsgToMapSrvByPlayer( GMSortStorage, pOwner, srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnSwapStorageItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnSwapStorageItem, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSwapStorageItem , pSwapItem,pPkg,wPkgLen );

	GMSwapStorageItem srvmsg;
	srvmsg.playerID  = pOwner->GetPlayerID();
	srvmsg.fromIndex = pSwapItem->fromStorageIndex;
	srvmsg.toIndex   = pSwapItem->toStorageIndex;
	SendMsgToMapSrvByPlayer( GMSwapStorageItem, pOwner ,srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnSortPkgItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnSortPkgItem, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSortPkgItem,  pSortItem,pPkg,wPkgLen )

		GMSortPkgItem srvmsg;
	srvmsg.sortPlayerID = pOwner->GetPlayerID();
	StructMemCpy(srvmsg.sortPkgInfo,pSortItem,sizeof(CGSortPkgItem));
	SendMsgToMapSrvByPlayer( GMSortPkgItem, pOwner, srvmsg );
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnSortPkgItemBegin(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnSortPkgItemBegin, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSortPkgItemBegin,  pSortItem,pPkg,wPkgLen )

		GMSortPkgItemBegin srvmsg;
	srvmsg.sortPlayerID = pOwner->GetPlayerID();
	srvmsg.sortPage = pSortItem->sortPage;
	SendMsgToMapSrvByPlayer( GMSortPkgItemBegin, pOwner, srvmsg );
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnSortPkgItemEnd(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnSortPkgItemEnd, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSortPkgItemEnd,  pSortItem,pPkg,wPkgLen )

		GMSortPkgItemEnd srvmsg;
	srvmsg.sortPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMSortPkgItemEnd, pOwner, srvmsg );
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnDeleteAllMail(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnDeleteAllMail, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDeleteAllMail, pReq, pPkg, wPkgLen ); ACE_UNUSED_ARG( pReq );

	GFDeleteAllMail delAllMail;
	delAllMail.deletePlayerUID = pOwner->GetRoleID();

	SendMsgToFunctionSrv( GFDeleteAllMail, delAllMail );

	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryMailListByPageIndex(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnQueryMailListByPageIndex, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryMailListByPage, pReq, pPkg, wPkgLen );


	GFQueryMailListByPage srvmsg;
	srvmsg.mailType = pReq->mailType;
	srvmsg.pageIndex = pReq->page;
	srvmsg.queryPlayerUID = pOwner->GetRoleID();
	srvmsg.queryPlayerID  = pOwner->GetPlayerID();

	SendMsgToFunctionSrv( GFQueryMailListByPage, srvmsg );

	return true;

	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnQueryMailDetialInfo(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnQueryMailDetialInfo, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGQueryMailDetailInfo, pReq, pPkg, wPkgLen );


	GFQueryMailDetialInfo srvmsg;
	srvmsg.mailType = pReq->mailType;
	srvmsg.mailUID  = pReq->mailuid;
	srvmsg.queryPlayerID = pOwner->GetPlayerID();
	srvmsg.queryPlayerUID =pOwner->GetRoleID();

	SendMsgToFunctionSrv(GFQueryMailDetialInfo, srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnDeleteMail(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnDeleteMail, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDeleteMail, pReq, pPkg, wPkgLen  );

	GFDeleteMail srvmsg;
	srvmsg.deletePlayerID = pOwner->GetPlayerID();
	srvmsg.deleteplayeruid = pOwner->GetRoleID();
	srvmsg.mailType  = pReq->mailType;
	srvmsg.mailuid   = pReq->mailuid;

	SendMsgToFunctionSrv( GFDeleteMail , srvmsg );

	return true;
	TRY_END;

	return false;
}


bool CDealPlayerPkg::OnPickMailAttachItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnPickMailAttachItem, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPickMailAttachItem ,pReq, pPkg, wPkgLen   );

	GFPickItemFromMail srvmsg;
	srvmsg.mailUID   = pReq->mailuid;
	srvmsg.pickIndex = pReq->pickIndex;
	srvmsg.pickPlayerID = pOwner->GetPlayerID();
	srvmsg.pickPlayerUID = pOwner->GetRoleID();

	SendMsgToFunctionSrv( GFPickItemFromMail,srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnPickMailAttachMoney(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR("OnPickMailAttachMoney, pOwner空\n");
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGPickMailAttachMoney ,pReq, pPkg, wPkgLen   );

	GFPickMoneyFromMail srvmsg;
	srvmsg.mailuid = pReq->mailuid;
	srvmsg.pickPlayerID = pOwner->GetPlayerID();
	srvmsg.pickPlayerUID = pOwner->GetRoleID();

	SendMsgToFunctionSrv( GFPickMoneyFromMail, srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnGMPlayerIDReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "CDealPlayerPkg::OnGMPlayerIDReq,pOwner空\n" );
		return false;
	}

	if(!pOwner->IsGMAccount())
	{
		D_ERROR("CDealPlayerPkg::OnGMPlayerIDReq,玩家的账号%s不是gm账号\n", pOwner->GetAccount());
		return false;
	}

	DealPlayerPkgPre( GTCmd_Query_PlayerID_Req, pCliMsg, pPkg, wPkgLen );

	GEGMQueryTargetCheck srvmsg;
	srvmsg.gmPlayerId = pOwner->GetPlayerID();
	srvmsg.gmReq = *pCliMsg;

	//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
	//if ( NULL == pCenterSrv )
	//{
	//	D_WARNING( "CDealPlayerPkg::OnGMPlayerIDReq找不到CenterSrv\n" );
	//	return false;
	//}

	//MsgToPut* pNewMsg = CreateSrvPkg( GEGMQueryTargetCheck, pCenterSrv, srvmsg );
	//pCenterSrv->SendPkgToSrv( pNewMsg );
	SendMsgToCenterSrv( GEGMQueryTargetCheck, srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnGMQueryReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "CDealPlayerPkg::OnGMQueryReq,pOwner空\n" );
		return false;
	}

	if(!pOwner->IsGMAccount())
	{
		D_ERROR("CDealPlayerPkg::OnGMQueryReq,玩家的账号%s不是gm账号\n", pOwner->GetAccount());
		return false;
	}

	DealPlayerPkgPre( GTCmd_Query_Req, pCliMsg, pPkg, wPkgLen );

	switch(pCliMsg->uType)
	{
	case GTCT_ROLE_BASE://角色基本信息
	case GTCT_ROLE_SKILL://角色技能
	case GTCT_ROLE_BAG://角色背包
	case GTCT_ROLE_QUEST://角色任务
	case GTCT_ROLE_EQUIP://角色装备
	case GTCT_ROLE_PET://宠物信息
		{
			CMapSrv* pMapsrv = pOwner->GetMapSrv();
			if ( NULL != pMapsrv )
			{
				GMGMQueryReq srvmsg;
				srvmsg.gmPlayerId = pOwner->GetPlayerID();
				srvmsg.targetPlayerId  = pCliMsg->xKey.unKey.xRoleKey.uPlayerID;
				srvmsg.gmReq = *pCliMsg;

				//MsgToPut* pNewMsg = CreateSrvPkg( GMGMQueryReq, pMapsrv, req );
				//pMapsrv->SendPkgToSrv( pNewMsg );
				GateSrvSendMsgToMapSrv( GMGMQueryReq );
			} 
			else 
			{
				D_WARNING( "CDealPlayerPkg::OnGMQueryReq，找不到玩家所在的mapsrv:%d\n", pOwner->GetMapSrvID() );
			}
		}
		break;

	case GTCT_ROLE_RELATION://角色社交
		{
			QueryPlayerRelationRequest srvmsg;
			StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

			srvmsg.uiid = pCliMsg->xKey.unKey.xRoleKey.uRoleID;
			srvmsg.gmGateId = pOwner->GetPlayerID().wGID;
			srvmsg.gmPlayerId = pOwner->GetPlayerID().dwPID;
			SendMsgToRelationSrv(QueryPlayerRelationRequest, srvmsg);
		}
		break;
	case GTCT_ROLE_MAIL://角色邮件
		{
			GFQueryMailListByPage srvmsg;
			srvmsg.mailType = (MAIL_TYPE)pCliMsg->xKey.unKey.xRoleKey.uType;
			srvmsg.pageIndex = pCliMsg->xKey.unKey.xRoleKey.uPage;
			srvmsg.queryPlayerUID = pCliMsg->xKey.unKey.xRoleKey.uRoleID;
			srvmsg.queryPlayerID  = pOwner->GetPlayerID();

			SendMsgToFunctionSrv( GFQueryMailListByPage, srvmsg );
		}
		break;

	default:
		{
		}
	}

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnItemNpcReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "OnItemNpcReq,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGBuildNpcReq, pCliMsg, pPkg, wPkgLen );

	GMItemNpcReq mapReq;
	mapReq.req = (*pCliMsg);
	mapReq.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMItemNpcReq, pOwner, mapReq );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnBatchSellItemsReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "OnBatchSellItemsReq,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGBatchSellItemsReq, pCliMsg, pPkg, wPkgLen );

	GMBatchSellItemsReq mapReq;
	mapReq.req = (*pCliMsg);
	mapReq.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMBatchSellItemsReq, pOwner, mapReq );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnRepurchaseItemReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "OnRepurchaseItemReq,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGRepurchaseItemReq, pCliMsg, pPkg, wPkgLen );

	GMRepurchaseItemReq mapReq;
	mapReq.req = (*pCliMsg);
	mapReq.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMRepurchaseItemReq, pOwner, mapReq );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnCloseOLTestDlg(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnCloseOLTestDlg, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGCloseOLTestDlg, pCliMsg, pPkg, wPkgLen );

	GMCloseOLTestDlg closeOLTestDlg;
	closeOLTestDlg.closePlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMCloseOLTestDlg, pOwner, closeOLTestDlg );

	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnAnswerQuestion(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnAnswerQuestion, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGAnswerOLTestQuestion, pClimsg, pPkg, wPkgLen );

	GMAnswerOLTestQuestion answerQuestion;
	answerQuestion.answerPlayerID = pOwner->GetPlayerID();
	answerQuestion.selectID = pClimsg->selectID;
	SendMsgToMapSrvByPlayer( GMAnswerOLTestQuestion, pOwner, answerQuestion );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnPlayerDropOLTest(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerDropOLTest, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGDropOLTest,pClimsg, pPkg, wPkgLen );

	GMDropOLTest dropTest;
	dropTest.dropTestPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMDropOLTest, pOwner, dropTest );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnPlayerSendSysMailToOther(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerSendSysMailToOther, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGSendSysMailToPlayer,pClimsg, pPkg, wPkgLen );

	GFSendSysMailToPlayer sendSysMail;
	StructMemSet( sendSysMail, 0, sizeof(sendSysMail) );
	SafeStrCpy( sendSysMail.recvPlayerName , pClimsg->recvPlayerName );
	SafeStrCpy( sendSysMail.sendMailContent, pClimsg->mailContent );
	SafeStrCpy( sendSysMail.sendMailTitle, pClimsg->mailTitle );
	StructMemCpy( sendSysMail.attachItem, pClimsg->attachItemArr, sizeof(sendSysMail.attachItem) );
	sendSysMail.silverMoney = 0;//客户端不能附送银币
	sendSysMail.goldMoney = pClimsg->goldmoney;//双币种判断

	SendMsgToFunctionSrv( GFSendSysMailToPlayer, sendSysMail );

	return true;
	TRY_END;
	return false;
}

//Register( C_G_GLOBE_COPYINFO_QUERY, &OnPlayerGlobeCopyInfoQuery );//玩家查询全局副本信息；
bool CDealPlayerPkg::OnPlayerGlobeCopyInfoQuery( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerGlobeCopyInfoQuery, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGGlobeCopyInfoQuery, pClimsg, pPkg, wPkgLen );

	GEQueryGlobeCopyInfos globeCopyInfos;
	globeCopyInfos.notiPlayerID = pOwner->GetPlayerID();

	SendMsgToCenterSrv( GEQueryGlobeCopyInfos, globeCopyInfos );

	//向连接的每个mapsrv都发一个伪副本查询消息；
	GMQueryGlobeCopyInfos pcopyQuery;
	pcopyQuery.notiPlayerID = pOwner->GetPlayerID();
	CManMapSrvs::SendBroadcastMsg<GMQueryGlobeCopyInfos>( &pcopyQuery );

	return true;
	TRY_END;
	return false;
}

//Register( C_G_TEAM_COPYINFO_QUERY, &OnPlayerTeamCopyInfoQuery );//玩家查询组队副本信息；
bool CDealPlayerPkg::OnPlayerTeamCopyInfoQuery( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerTeamCopyInfoQuery, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGTeamCopyInfoQuery, pClimsg, pPkg, wPkgLen );

	GEQueryTeamCopyInfos teamCopyInfos;
	teamCopyInfos.notiPlayerID = pOwner->GetPlayerID();
	teamCopyInfos.teamID = pOwner->GetPlayerCopyFlag();

	SendMsgToCenterSrv( GEQueryTeamCopyInfos, teamCopyInfos );

	return true;
	TRY_END;
	return false;
}

//Register( C_G_RESET_COPY, &OnPlayerResetCopy );//玩家请求重置副本；
bool CDealPlayerPkg::OnPlayerResetCopy( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerResetCopy, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGResetCopy, pClimsg, pPkg, wPkgLen );

	if ( !(pOwner->IsTeamCopyInfoLeader()) )
	{
		//不是队长,不能重置副本;
		GCChat resp;
		StructMemSet(resp, 0x00, sizeof(resp));

		resp.chatType = CT_SYS_WARNING;
		resp.extInfo = 0;
		SafeStrCpy( resp.strChat, "您没有重置副本权限" );
		resp.chatSize = (unsigned int)strlen( resp.strChat ) + 1;

		NewSendPlayerPkg( GCChat, pOwner, resp );
		return true;
	}

	//是队长，则转往centersrv处理；
	GEResetCopy resetCopyCmd;
	resetCopyCmd.captainPlayerID = pOwner->GetPlayerID();
	resetCopyCmd.steamID = pOwner->GetPlayerCopyFlag();
	resetCopyCmd.copyMapID = pClimsg->copyMapID;
	SendMsgToCenterSrv( GEResetCopy, resetCopyCmd );

	return true;
	TRY_END;
	return false;
}

//Register( C_G_TIME_LIMIT_RES, &OnPlayerTimeLimitRes );//玩家点击倒计时确认框；
bool CDealPlayerPkg::OnPlayerTimeLimitRes( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerResetCopy, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGTimeLimitRes, pClimsg, pPkg, wPkgLen );

	GMTimeLimitRes mapsrvMsg;//消息转发mapsrv;
	mapsrvMsg.resPlayerID = pOwner->GetPlayerID();
	mapsrvMsg.limitReason = pClimsg->limitReason;

	SendMsgToMapSrvByPlayer( GMTimeLimitRes, pOwner, mapsrvMsg );

	return true;
	TRY_END;
	return false;

}

bool CDealPlayerPkg::OnSelectWarRebirthOption( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnSelectWarRebirthOption, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGSelectWarRebirthOption, pClimsg, pPkg, wPkgLen );

	GMSelectWarRebirthOption option;
	option.lNotiPlayerID = pOwner->GetPlayerID();
	option.req = *pClimsg;

	SendMsgToMapSrvByPlayer( GMSelectWarRebirthOption, pOwner, option );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnRepurchaseItemsListReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnRepurchaseItemsListReq, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGRepurchaseItemsListReq, pClimsg, pPkg, wPkgLen );

	GMRepurchaseItemsListReq listReq;
	listReq.lNotiPlayerID = pOwner->GetPlayerID();
	listReq.req = *pClimsg;

	SendMsgToMapSrvByPlayer( GMRepurchaseItemsListReq, pOwner, listReq );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnRaceMasterSelectOPType(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnRaceMasterSelectOPType, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGRaceMasterSelectOpType,  pClimsg, pPkg, wPkgLen );

	GMRaceMasterSelectOpType srvmsg;
	srvmsg.opType =  pClimsg->opType;
	srvmsg.selectPlayerID =  pOwner->GetPlayerID();

	SendMsgToMapSrvByPlayer( GMRaceMasterSelectOpType, pOwner, srvmsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnRaceMasterDeclareWarToOtherRace(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnRaceMasterDeclareWarToOtherRace, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGRaceMasterDeclareWarToOtherRace,  pClimsg, pPkg, wPkgLen );

	GMRaceMasterDeclareWarToOtherRace srvmsg;
	srvmsg.race = pOwner->GetRace();
	srvmsg.otherracetype = pClimsg->raceType;
	srvmsg.playerID = pOwner->GetPlayerID();

	SendMsgToMapSrvByPlayer( GMRaceMasterDeclareWarToOtherRace, pOwner, srvmsg );
	return true;

	TRY_END;
	return false;
}

#define DEAL_PLAYER_MSG_CHECK( MSG_TYPE )\
	if ( NULL == pOwner ){\
	D_ERROR( "%s,pOwner空\n",__FUNCTION__);\
	return false;}\
	DealPlayerPkgPre( MSG_TYPE, pClimsg, pPkg, wPkgLen );\


bool CDealPlayerPkg::OnGMRemovePublicNotice( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_PLAYER_MSG_CHECK(CGGMDeletePublicNotice);

	GFRemovePublicNotice srvmsg;
	srvmsg.msgid = pClimsg->msgid;
	SendMsgToFunctionSrv( GFRemovePublicNotice, srvmsg );
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnGMAddNewPublicNotice( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_PLAYER_MSG_CHECK(CGGMAddNewPublicNotice);

	GFGMAddNewPublicNotice srvmsg;
	StructMemCpy( srvmsg.srvmsg, &pClimsg->newnotice, sizeof(PublicNotice) );
	SendMsgToFunctionSrv( GFGMAddNewPublicNotice, srvmsg );
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnGMModifyPublicNotice( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_PLAYER_MSG_CHECK(CGGMModifyPublicNotice);

	GFModifyPublicNotice srvmsg;
	StructMemCpy( srvmsg.srvmsg, &pClimsg->modifynotice, sizeof(PublicNotice) );
	SendMsgToFunctionSrv( GFModifyPublicNotice, srvmsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnGMAddNewMarqueueNotice( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_PLAYER_MSG_CHECK(CGGMAddNewMarqueueNotice);

	GMAddMarqueueNotice srvmsg;
	StructMemCpy( srvmsg.marqueue, &pClimsg->newnotice, sizeof(MarqueueNotice) );
	CManMapSrvs::SendBroadcastMsg<GMAddMarqueueNotice>( &srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnGMQueryAllPublicNotices( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_PLAYER_MSG_CHECK(CGGMQueryAllPublicNotice);

	GFQueryAllPublicNotice srvmsg;
	srvmsg.queryPlayerID = pOwner->GetPlayerID();
	SendMsgToFunctionSrv( GFQueryAllPublicNotice, srvmsg );
	return true;
	TRY_END; 

	return false;
}

bool CDealPlayerPkg::OnGMReloadPublicNotices( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_PLAYER_MSG_CHECK(CGGMLoadNewPublicNotice);

	GFLoadNewPublicNotice srvmsg;
	SendMsgToFunctionSrv( GFLoadNewPublicNotice, srvmsg );
	return true;
	TRY_END;

	return false;
}


bool CDealPlayerPkg::OnRaceMasterSetNewPublicNotice(CPlayer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnRaceMasterSetNewPublicNotice, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGAddNewMsgToRaceMasterNotices,  pClimsg, pPkg, wPkgLen );


	CGAddNewMsgToRaceMasterNotices* pMsg = (CGAddNewMsgToRaceMasterNotices *)pPkg;

	GMRaceMasterSetNewPublicMsg srvmsg;
	StructMemSet( srvmsg,0x0, sizeof(srvmsg) );

	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.race  = pOwner->GetRace();
	int len = sizeof(srvmsg.msgArr) > pMsg->msgLen ? pMsg->msgLen:sizeof(srvmsg.msgArr);
	SafeStrCpy( srvmsg.msgArr, pMsg->msgArr );

	SendMsgToMapSrvByPlayer( GMRaceMasterSetNewPublicMsg, pOwner, srvmsg );
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnPlayerReturnSelRole( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerReturnSelRole, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGReturnSelRole,  pClimsg, pPkg, wPkgLen );

	pOwner->OnPlayerReturnSelRole();
	return true;

	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnPlayerQueryMyBidState(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerQueryMyBidState, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGQueryMyBidState, pClimsg, pPkg, wPkgLen );

	GFQueryMyBid srvmsg;
	srvmsg.pageIndex = pClimsg->pageIndex;
	srvmsg.playerUID = pOwner->GetSeledRole();
	srvmsg.queryPlayerID = pOwner->GetPlayerID();
	SendMsgToFunctionSrv(  GFQueryMyBid, srvmsg );
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnPlayerQueryMyAuctions(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerQueryMyAuctions, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGQueryMyAuctions, pClimsg, pPkg, wPkgLen );

	GFQueryMyAuction srvmsg;
	srvmsg.pageIndex = pClimsg->pageIndex;
	srvmsg.playerUID = pOwner->GetSeledRole();
	srvmsg.queryPlayerID = pOwner->GetPlayerID();
	SendMsgToFunctionSrv(  GFQueryMyAuction, srvmsg );
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnPlayerQueryAuctionByCondition(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerQueryAuctionByCondition, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGQueryAuctionByCondition,  pClimsg, pPkg, wPkgLen );

	GFQueryAuctionCondition srvmsg;
	StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );

	SafeStrCpy( srvmsg.itemname,pClimsg->itemName );
	srvmsg.itemclass = pClimsg->itemclass;
	srvmsg.itempos   = pClimsg->itempos;
	srvmsg.level1    = pClimsg->level1;
	srvmsg.level2    = pClimsg->level2;
	srvmsg.levelorder = pClimsg->levelorder;
	srvmsg.masslevel  = pClimsg->masslevel;
	srvmsg.massorder  = pClimsg->massorder;
	srvmsg.orderby    = pClimsg->orderby;
	srvmsg.pageindex  = pClimsg->pageIndex;
	srvmsg.selectmask = pClimsg->selectMask;
	srvmsg.queryPlayerID = pOwner->GetPlayerID();
	SendMsgToFunctionSrv( GFQueryAuctionCondition, srvmsg );
	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnPlayerBeginNewAuction(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerBeginNewAuction, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre(CGBeginNewAuction, pClimsg, pPkg, wPkgLen );

	GMPlayerStartNewAuction srvmsg;
	srvmsg.expiretype = pClimsg->expireType;
	srvmsg.fixprice = pClimsg->fixprice;
	srvmsg.origiprice = pClimsg->orgiprice;
	srvmsg.pkgIndex  = pClimsg->pkgIndex;
	srvmsg.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMPlayerStartNewAuction, pOwner, srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnPlayerCancelMyAuction(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerCancelMyAuction, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGCancelMyAuction,  pClimsg, pPkg, wPkgLen  );

	GFPlayerCancelAuction srvmsg;
	srvmsg.auctionUID = pClimsg->auctionUID;
	srvmsg.playerID = pOwner->GetPlayerID();
	srvmsg.playerUID = pOwner->GetSeledRole();
	SendMsgToFunctionSrv( GFPlayerCancelAuction, srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnPlayerBidAuctionItem(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnPlayerBidAuctionItem, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGBidAuctionItem,  pClimsg, pPkg, wPkgLen  );

	GFPlayerBidItemSection1 srvmsg;
	srvmsg.auctionUID = pClimsg->auctionUID;
	srvmsg.playerID   = pOwner->GetPlayerID();
	srvmsg.playerUID  = pOwner->GetSeledRole();
	srvmsg.newprice   = pClimsg->newprice;
	SendMsgToFunctionSrv( GFPlayerBidItemSection1, srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnEnterPWMap( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnEnterPWMap, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGEnterPWMap,  pClimsg, pPkg, wPkgLen  );

	GEEnterPWMapCheck srvmsg;
	srvmsg.pwid = pClimsg->pwid;
	srvmsg.pwMapid = pClimsg->mapid;
	srvmsg.lNoticePlayerID = pOwner->GetPlayerID();
	srvmsg.isVip = false;
	srvmsg.isShift = false;
	SendMsgToCenterSrv( GEEnterPWMapCheck, srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnShiftPWListReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnShiftPWListReq, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGGetShiftPWList,  pClimsg, pPkg, wPkgLen  );

	GCShiftPWList rst;
	StructMemSet(rst, 0x00, sizeof(rst));

	std::vector<unsigned short> pwVec;
	CPseudowireManagerSingle::instance()->GetShiftPWOption(0, pwVec);
	for(BYTE i = 0; i < pwVec.size(); i++)
	{
		rst.data[rst.count] = pwVec[i];
		rst.count++;

		if(rst.count == sizeof(rst.data)/sizeof(rst.data[0]))
		{
			NewSendPlayerPkg(GCShiftPWList, pOwner, rst);
			rst.count = 0;
		}
	}

	if(rst.count > 0)
		NewSendPlayerPkg(GCShiftPWList, pOwner, rst);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnShiftPW( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnShiftPW, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGShiftPW,  pClimsg, pPkg, wPkgLen  );

	//玩家是否在伪线地图中?

	//到centerserver检查是否可以进入新的伪线地图
	GEEnterPWMapCheck srvmsg;
	srvmsg.pwid = pClimsg->pwid;
	srvmsg.pwMapid = 0;
	srvmsg.lNoticePlayerID = pOwner->GetPlayerID();
	srvmsg.isVip = false;
	srvmsg.isShift = true;
	SendMsgToCenterSrv( GEEnterPWMapCheck, srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnManualUpgradeReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnManualUpgradeReq, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGManualUpgradeReq,  pClimsg, pPkg, wPkgLen  );

	GMManualUpgradeReq forward;
	forward.lNotiPlayerID = pOwner->GetPlayerID();
	forward.req = *pClimsg;
	SendMsgToMapSrvByPlayer(GMManualUpgradeReq, pOwner, forward);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnAllocatePointReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnAllocatePointReq, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGAllocatePointReq,  pClimsg, pPkg, wPkgLen  );

	GMAllocatePointReq forward;
	forward.lNotiPlayerID = pOwner->GetPlayerID();
	forward.req = *pClimsg;
	SendMsgToMapSrvByPlayer(GMAllocatePointReq, pOwner, forward);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnModEffectiveSkillID( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnModEffectiveSkillID, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGModifyEffectiveSkillID,  pClimsg, pPkg, wPkgLen  );

	GMModifyEffectSkillID forward;
	forward.lNotiPlayerID = pOwner->GetPlayerID();
	forward.req = *pClimsg;
	SendMsgToMapSrvByPlayer(GMModifyEffectSkillID, pOwner, forward);

	return true;

	TRY_END;

	return false;
}


bool CDealPlayerPkg::OnSetViewedConditionOfTweet( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "CDealPlayerPkg::OnSetViewedConditionOfTweet,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGModifyTweetReceiveFlag, pCliMsg, pPkg, wPkgLen );

	ModifyTweetReceiveFlag req;
	req.gateId = pOwner->GetPlayerID().wGID;
	req.playerId = pOwner->GetPlayerID().dwPID;
	req.flag = pCliMsg->flag;

	SendMsgToRelationSrv(ModifyTweetReceiveFlag, req);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryOfflinePlayerInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "CDealPlayerPkg::OnQueryOfflinePlayerInfo,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGQueryOffLinePlayerInfo, pCliMsg, pPkg, wPkgLen );

	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( NULL );
	if ( NULL == pDbSrv )
	{
		GCSrvError srvError;
		srvError.errNO = 1001;
		NewSendPlayerPkg( GCSrvError, pOwner, srvError );

		D_WARNING( "CDealPlayerPkg::OnQueryOfflinePlayerInfo，找不到玩家所在的dbsrvd\n\n" );
		return false;
	}

	GDQueryOffLinePlayerInfoReq farward;
	farward.lNotiPlayerID = pOwner->GetPlayerID();
	farward.req = *pCliMsg;
	SendMsgToSrv( GDQueryOffLinePlayerInfoReq, pDbSrv, farward );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnReplyCallMember(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnReplyCallMember, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGReplyCallMember,  pClimsg, pPkg, wPkgLen  );

	GMReplyCallMember forward;
	forward.replyPlayerID = pOwner->GetPlayerID();
	StructMemCpy(forward.replyCallMember,pClimsg,sizeof(forward.replyCallMember));
	SendMsgToMapSrvByPlayer(GMReplyCallMember, pOwner, forward);

	return true;

	TRY_END;

	return false;
}

//Register( C_G_QUERY_NICKUNION, &OnQueryNickUnion );//查询昵称对应玩家；
bool CDealPlayerPkg::OnQueryNickUnion( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("CDealPlayerPkg::OnQueryNickUnion, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGQueryNickUnion, pCliQry, pPkg, wPkgLen );

	if ( NULL == pCliQry )
	{
		D_ERROR("CDealPlayerPkg::OnQueryNickUnion, NULL == pCliQry\n");
		return false;
	}

	GRQueryNickUnion grQuery;
	StructMemSet( grQuery, 0, sizeof(grQuery) );
	grQuery.queryPlayerID.GateSrvID = pOwner->GetPlayerID().wGID;
	grQuery.queryPlayerID.PPlayer   = pOwner->GetPlayerID().dwPID;
	grQuery.nickArrLen = 0;

	if ( pCliQry->qryArrLen > ARRAY_SIZE(pCliQry->qryArr) ) //确保客户端传来的数组大小不越界
	{
		D_ERROR("CDealPlayerPkg::OnQueryNickUnion, 客户端消息中qryLen(%d)越界\n", pCliQry->qryArrLen);
		return false;
	}

	for ( BYTE i=0; i<pCliQry->qryArrLen; ++i )
	{
		if ( (i >= ARRAY_SIZE(grQuery.nickArr)) 
			|| (i >= ARRAY_SIZE(pCliQry->qryArr)) )
		{
			D_ERROR( "CDealPlayerPkg::OnQueryNickUnion, i(%d) >= ARRAY_SIZE(grQuery.nickArr)(%d)", i, ARRAY_SIZE(grQuery.nickArr) );
			break;
		}
		SafeStrCpy( grQuery.nickArr[i].Name, pCliQry->qryArr[i].qryName );
		grQuery.nickArr[i].NameLen = min( (unsigned int)strlen(grQuery.nickArr[i].Name), (unsigned int)ARRAY_SIZE(grQuery.nickArr[i].Name));//简单起见，直接设为char数组大小
		++ grQuery.nickArrLen;//查询元素个数++;
	}

	SendMsgToRelationSrv(GRQueryNickUnion, grQuery);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnStartCast(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnStartCast, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGStartCast,  pClimsg, pPkg, wPkgLen  );

	GMStartCast forward;
	forward.lNotiPlayerID = pOwner->GetPlayerID();
	StructMemCpy(forward.req,pClimsg,sizeof(forward.req));
	SendMsgToMapSrvByPlayer(GMStartCast, pOwner, forward);

	return true;

	TRY_END;

	return false;
}

bool CDealPlayerPkg::OnStopCast(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR("OnStartCast, pOwner空\n");
		return false;
	}

	DealPlayerPkgPre( CGStopCast,  pClimsg, pPkg, wPkgLen  );

	GMStopCast forward;
	forward.lNotiPlayerID = pOwner->GetPlayerID();
	SendMsgToMapSrvByPlayer(GMStopCast, pOwner, forward);

	return true;

	TRY_END;

	return false;
}
