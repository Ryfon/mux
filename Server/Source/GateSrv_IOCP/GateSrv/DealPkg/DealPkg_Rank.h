﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once


class CFunctionSrv;
//RankSrv消息处理类
class CDealRankSrvPkg:public IDealPkg<CFunctionSrv>
{
private:
	CDealRankSrvPkg(){};
	virtual ~CDealRankSrvPkg(){};

public:
	//初始化
	static void Init();

public:
	//Register( F_G_BIAS_INFO, &OnFGBiasInfo );//functionsrv发来排行榜数据缓存信息；
	static bool OnFGBiasInfo( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//Register( F_G_HONOR_PLAYER, &OnFGHonorPlayer );//functionsrv发来称号相关信息；
	static bool OnFGHonorPlayer( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//当接到排行榜信息时
	static bool OnRecvRankChartInfo( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	////当接到排行榜查询时 
	//static bool OnRecvQueryRankChartByPageRst(  CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen  );
	///functionsrv发来的排行榜查询结果；
	static bool OnNewRankInfo( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen  );	

	//当接到天谴的玩家时
	static bool OnRecvPunishmentPlayer( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen   );

	//当接到排行榜清空的消息
	static bool OnRecvClearRankChart( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//依据玩家的姓名查询排行榜的信息
	static bool OnRecvQueryRankInfoByPlayerName( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen  );

	//查询玩家是否在线
	static bool OnQueryRecvMailPlayerExist( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//从邮件中拾取道具时
	static bool OnPickItemFromMail( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//从邮件中拾取金钱
	static bool OnPickMoneyFromMail( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//通知玩家是否有未读邮件
	static bool OnNoticePlayerHaveUnReadMail( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//查询邮件列表时
	static bool OnQueryMailListByPageRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//查询邮件详细信息时
	static bool OnQueryMailDetialInfoRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//当返回邮件的附属信息的时候
	static bool OnQueryMailAttachInfoRst(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen  );

	//当发送邮件失败时
	static bool OnSendMailError( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen  );

	//当删除邮件时
	static bool OnDeleteMailRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//更新未读邮件的个数
	static bool OnUpdateUnReadMailCount( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//当接到删除排行榜玩家的消息时
	static bool OnRecvRemoveRankChartPlayer( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	//异常接受处理
	static bool OnExceptionNotify( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnStorageItemInfo( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnStorageSafeInfo( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnStorageForbiddenInfo( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen  );

	static bool OnNoticeFirstUseStorage( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnMapSrvLoadRaceMasterRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnForbiddenRaceMasterAuthority( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnSelectRaceMaster( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnSendMailNotify( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnRecvPublicNotices( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen  );//接受公告信息

	static bool OnModifyPublicNotice( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );//GM修改了公告内容后,通知全服务器

	static bool OnRemovePublicNotice( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );//GM删除了公告后,通知全服务器

	static bool OnAddPublicNotice( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );//GM增加了新的公告,通知全服务器

	static bool OnBidItemRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryMyBidRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryMyAuctionRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnBidItemSection1( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen  );

	static bool OnQueryAuctionConditionRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

	static bool OnCancelAuctionRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen );

public:
	static void ConvertRankPlayerToRankItem( RankChartItem& rankItem, const RankChartPlayer& rankPlayer );
};


