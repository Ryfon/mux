﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once
class CRelationSrv;
/// relationSrv的消息处理类
class CDealRelationSrvPkg : public IDealPkg<CRelationSrv>
{
private:
	static RelationServiceProtocol m_relationProtocol;
private:
	CDealRelationSrvPkg() {};
	virtual ~CDealRelationSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();
public:
	//todo wcj 未注册的消息
	static bool OnReportGateIdResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	

	/// 
	///static bool On*****( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	static bool OnCreateTeamResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	//创建组队组播
	static bool OnCreateTeamNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	//增加组员组播
	static bool OnAddTeamMemberNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	//增加组员的结果
	static bool OnAddTeamMemberResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//解除组队的组播
	static bool OnTeamDestroyedNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//删除组员的组播
	static bool OnRemoveTeamMemberNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//改变队长的组播
	static bool OnModifyTeamCaptainNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//小队聊天
	static bool OnTeamBroadcastNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//组队物品分配模式的更改
	static bool OnModifyExpSharedModeNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//组队经验分配模式的更改
	static bool OnModifyGoodSharedModeNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//收到更改ROLL等级的NOTIFY
	static bool OnModifyTeamRollItemLevelNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//组队信息的请求反馈
	static bool OnQueryTeamResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//接到RelationSrv传来的好友组信息
	static bool OnRecvFriendGroupInfo(  CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//接到RelationSrv传来的好友信息
	static bool OnRecvFriendInfo( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//接到RelationSrv传来的加好友的消息
	static bool OnRecvAddFriendResult(  CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//接到了RelationSrv传来的删除好友的消息
	static bool OnRecvDeleteFriendResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到RelationSrv发来的更新好友信息的消息时
	static bool OnRecvUpdateFriendInfo( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到RelationSrv返回的添加组的消息
	static bool OnRecvAddFriendGroup( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到了RelationSrv返回的改变好友组的消息
	static bool OnRecvMoveFrindToOtherGroup(  CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到 RelationSrv删除组成功的消息时
	static bool OnRecvDeleteFriendGroup( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到RelationSrv的聊天组消息转发时
	static bool OnRecvFriendGroupChat( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到RelationSrv的单人聊天消息转发时
	static bool OnRecvFriendSingleChat( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到RelationSrv的修改组名的返回时
	static bool OnRecvChangeFriendGroupName( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//当接到加黑名单的消息时
	static bool OnRecvAddPlayerToBlackList( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到删除黑名单的消息时
	static bool OnRecvDelPlayerFromBlackList( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//接到黑名单的列表时
	static bool OnRecvBlackList( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//接到更新好友组的聊天个数消息
	static bool OnRecvChangeFriendGroupMsgCount( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnRecvFoeList( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnRecvLockFoeResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRecvDelFoeResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRecvAddFoeResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRGUpdateEnemy( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//创建群聊结果
	static bool OnCreateChatGroupResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//销毁群聊结果
	static bool OnDestroyChatGroupResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//销毁群聊通知
	static bool OnDestrocyChatGroupNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//加入群聊成员结果
	static bool OnAddChatGroupMemberResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//加入群聊成员通知
	static bool OnAddChatGroupMemberNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//群聊成员离开结果
	static bool OnRemoveChatGroupMemberResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//群聊成员离开通知
	static bool OnRemoveChatGroupMemberNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//群聊聊天结果
	static bool OnChatGroupSpeakResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//群聊聊天通知
	static bool OnChatGroupSpeakNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//
	static bool OnTeamMemberCountRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 创建工会结果
	static bool OnCreateUnionRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 解散工会结果
	static bool OnDestroyUnionRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 解散工会通知
	static bool OnDestroyUnionNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 请求工会基本信息
	static bool OnQueryUnionBasicInfo( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 请求工会成员信息
	static bool OnQueryUnionMemberInfo( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员加入邀请
	static bool OnAddUnionMember( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员增加结果
	static bool OnAddUnionMemberRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员增加通知
	static bool OnAddUnionMemberNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员离开结果
	static bool OnRemoveUnionMemberRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员离开通知
	static bool OnRemoveUnionMemberNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//// 工会职位对应权限修改通知
	//static bool OnUninPosRightsNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//// 工会职位对应权限修改结果
	//static bool OnModifyUnionPosRightRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//// 工会职位增加结果
	//static bool OnAddUnionPosRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//// 工会职位减少结果
	//static bool OnRemoveUnionPosRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 转移公会会长职务结果
	static bool OnTransformUnionCaptionRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 转移公会会长职务通知
	static bool OnTransformUninCaptionNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 修改工会成员称号结果
	static bool OnModifyUnionMemberTitleRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 修改工会成员称号通知
	static bool OnUnionMemberTitileNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 提升工会成员职位结果
	static bool OnAdvanceUnionMemberPosRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 提升工会成员职位通知
	static bool OnAdvanceUnionMemerPosNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 降低工会成员职位结果
	static bool OnReduceUnionMemberPosRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 降低工会成员职位通知
	static bool OnReduceUnionMemberPosNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会频道聊天结果
	static bool OnUnionChannelSpeekRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会频道聊天通知
	static bool OnUnionChannelSpeekNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 更新工会职务管理结果
	static bool OnUpdateUnionPosCfgRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 更新工会职务管理通知
	static bool OnUpdateUnionPosCfgNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员禁言结果
	static bool OnForbidUnionSpeekRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员禁言通知
	static bool OnForbidUnionSpeekNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 通知工会技能
	static bool OnUnionSkills( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 发布任务结果
	static bool OnIssueUnionTaskRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 发布任务通知
	static bool OnIssueUnionTaskNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//// 显示工会任务
	//static bool OnDisplayUnionTasks( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 学习工会技能结果
	static bool OnLearnUnionSkillRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//// 学习工会技能通知
	//static bool OnLearnUnionSkillNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 修改工会活跃度通知
	static bool OnChanageUnionActiveNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 提升工会等级结果
	static bool OnAdvanceUnionLevelRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会等级通知
	static bool OnUnionLevelNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 发布工会公告结果
	static bool OnPostUnionBulletinRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会公告通知
	static bool OnUnionBulletinNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员上线通知
	static bool OnUnionMemberOnLineNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会成员下线通知
	static bool OnUnionMemberOffLineNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 获取工会日至结果
	static bool OnQueryUnionLogRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 工会邮件通知
	static bool OnUnionMailNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 玩家是否工会会长
	static bool OnUnionOwnerCheckRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 修改工会威望结果
	static bool OnModifyUnionPrestigeRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 修改工会威望通知
	static bool OnModifyUnionPrestigeNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 修改工会徽章结果
	static bool OnModifyUnionBadgeRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 修改工会徽章通知
	static bool OnModifyUnionBadgeNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//工会活跃度或威望n倍
	static bool OnUnionMultiNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//GM查询关系信息
	static bool OnGMQueryRelationRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryPlayerInfoResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//当接到玩家上线时的分配的组队ID号
	static bool OnRecvPlayerUpLineTeamIDNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到玩家查询的工会排行
	static bool OnRecvQueryUnionRstByPage( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到玩家依据工会名查询的工会排行信息时
	static bool OnRecvQueryUnionRstByUnionName( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当接到族长信息返回时
	static bool OnRecvQueryRaceMasterRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//组队日至
	static bool OnTeamLogNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnFriendTweetNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnQueryFriendTweetRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnQueryFriendListRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnFriendNonTeamGainNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnQueryExpOfLevelUpReq( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnFriendTeamGainNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnFriendNonTeamGainMailNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( R_G_QUERY_NICKUNION_RST, &OnQueryNickUnionRst );//查询昵称战盟结果
	static bool OnQueryNickUnionRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

public:
	static void ConvertRTeamInfoToCliTeamInfo( const Team* pSrvTeam,  GCTeamInfo* cliTeam );

	static void ConvertRTeamInfoToMapTeamInfo(const Team* pSrvTeam,  GMTeamDetialInfo* mapTeam);

	static TYPE_ID ConvertRSrvTeamIndToTeamID( const TeamInd& teamInd );

	static TeamInd ConvertTeamIDToRSrvTeamInd( const TYPE_ID teamID );

	static void ConvertRFriendInfoToCliFriendInfo( MUX_PROTO::FriendInfo& cliInfo, RelationServiceNS::FriendInfo& srvInfo );

	static void ConvertRFoeInfoToCliFoeInfo( MUX_PROTO::FoeInfo& foeInfo ,RelationServiceNS::EnemylistItem& enemyInfo );

	static void ConvertUnionRankToCliRankInfo( const RelationServiceNS::UnionRankInfo& unionRankInfo, RankChartItem& rankItem );


};
