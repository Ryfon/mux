﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"
 


//初始化(注册各命令字对应的处理函数)
void CDealRankSrvPkg::Init()
{
	Register( F_G_BIAS_INFO, &OnFGBiasInfo );//functionsrv发来排行榜数据缓存信息；
	Register( F_G_HONOR_PLAYER, &OnFGHonorPlayer );//functionsrv发来称号相关信息；

	Register( F_G_RANKCHART_INFO, &OnRecvRankChartInfo );//从排行榜服务器获取排行榜的信息
	//Register( F_G_RANKCHARTINFO_BYPAGE, &OnRecvQueryRankChartByPageRst );
	Register( F_G_NEWRANKINFO, &OnNewRankInfo );	
	Register( F_G_PUNISHMENT_PLAYER, &OnRecvPunishmentPlayer );
	Register( F_G_RANKCHART_REFRESH, &OnRecvClearRankChart );
	Register( F_G_RANKCHARTINFO_BYNAME, &OnRecvQueryRankInfoByPlayerName );
	Register( F_G_QUERYMAIL_RECVPLAYER_INFO ,&OnQueryRecvMailPlayerExist );
	Register( F_G_PICK_MAIL_ATTACH_MONEY , &OnPickMoneyFromMail );
	Register( F_G_PICK_MAIL_ATTACH_ITEM , &OnPickItemFromMail );
	Register( F_G_NOTICE_PLAYER_HAVE_UNREADMAIL, &OnNoticePlayerHaveUnReadMail );
	Register( F_G_QUERYMAILLIST_BYPAGE_RST, &OnQueryMailListByPageRst );
	Register( F_G_QUERY_MAIL_DETIALINFO_RST, &OnQueryMailDetialInfoRst );
	Register( F_G_QUERY_MAIL_ATTACHINFO_RST, &OnQueryMailAttachInfoRst );
	Register( F_G_SENDMIAL_FAIL, &OnSendMailError );
	Register( F_G_DELETEMAIL_RESULT, &OnDeleteMailRst ); 
	Register( F_G_PLAYER_UNREADMAIL_COUNT_UPDATE, &OnUpdateUnReadMailCount );
	Register( F_G_REMOVE_RANKPLAYER , &OnRecvRemoveRankChartPlayer );
	Register( F_G_EXCEPTION_NOTIFY , &OnExceptionNotify );
	Register( F_G_STORAGE_SAFE_INFO, &OnStorageSafeInfo );
	Register( F_G_STORAGE_ITEM_INFO, &OnStorageItemInfo );
	Register( F_G_STORAGE_FORBIDDEN_INFO, &OnStorageForbiddenInfo );
	Register( F_G_NEW_STROAGE_NOTIFY , &OnNoticeFirstUseStorage );
	Register( F_G_QUERY_RACEMASTER_INFO_RST ,&OnMapSrvLoadRaceMasterRst );
	Register( F_G_FORBIDDEN_RACEMASTER_AUTHORITY, &OnForbiddenRaceMasterAuthority );
	Register( F_G_START_SELECT_RACEMASTER,&OnSelectRaceMaster );
	Register( F_G_SENDMAIL_SUCCESS_NOTI_GM, &OnSendMailNotify );

	Register( F_G_RECV_PUBLICNOTICE, &OnRecvPublicNotices );
	Register( F_G_MODIFY_PUBLICNOTICE,&OnModifyPublicNotice );
	Register( F_G_REMOVE_PUBLICNOTICE,&OnRemovePublicNotice );
	Register( F_G_ADD_NEW_PUBLICNOTICE,&OnAddPublicNotice );

	Register( F_G_PLAYER_BIDITEM_RST,&OnBidItemRst );
	Register( F_G_QUERY_MYBID_RST, &OnQueryMyBidRst );
	Register( F_G_PLAYER_BIDITEM_SECTION1,&OnBidItemSection1 );
	Register( F_G_QUERY_AUCTION_BYCONDITION_RST, &OnQueryAuctionConditionRst );
	Register( F_G_CANCELAUCTION_RST, &OnCancelAuctionRst );
	Register( F_G_QUERY_MYAUCTION_RST, &OnQueryMyAuctionRst );
}



/////////////////////接受从RankSrv传来的排行榜信息////////////////////////////////////////////

//Register( F_G_HONOR_PLAYER, &OnFGHonorPlayer );//functionsrv发来称号相关信息；
bool CDealRankSrvPkg::OnFGHonorPlayer( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR( "OnFGHonorPlayer,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(FGHonorPlayer) )
	{
		D_ERROR( "OnFGHonorPlayer, wPkgLen != sizeof(FGHonorPlayer)\n" );
		return false;
	}

	const FGHonorPlayer*  pHonorInfo = (FGHonorPlayer *)pPkg;
	if ( NULL == pHonorInfo )
	{
		D_ERROR( "OnFGHonorPlayer, NULL == pHonorInfo\n" );
		return false;
	}

	//准备群发此HonorPlayer消息；
	pOwner->OnRcvHonorPlayer( pHonorInfo->honorPlayer );

	return true;
	TRY_END;
	return false;
}

//Register( F_G_BIAS_INFO, &OnFGBiasInfo );//functionsrv发来排行榜数据缓存信息；
bool CDealRankSrvPkg::OnFGBiasInfo( CFunctionSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR( "OnFGBiasInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(FGBiasInfo) )
	{
		D_ERROR( "OnFGBiasInfo, wPkgLen != sizeof(FGBiasInfo)\n" );
		return false;
	}

	const FGBiasInfo*  pBiasInfo = (FGBiasInfo *)pPkg;
	if ( NULL == pBiasInfo )
	{
		D_ERROR( "OnFGBiasInfo, NULL == pBiasInfo\n" );
		return false;
	}

	GMBiasInfo gmBiasInfo;
	StructMemCpy( gmBiasInfo.arrBias, pBiasInfo->arrBias, sizeof(gmBiasInfo.arrBias) );
	StructMemCpy( gmBiasInfo.arrRankSeq, pBiasInfo->arrRankSeq, sizeof(gmBiasInfo.arrRankSeq) );

	CMapSrv* pNotiMapSrv = CManMapSrvs::GetMapserver( pBiasInfo->notiMapSrvID );
	if ( NULL != pNotiMapSrv )
	{
		SendMsgToSrv( GMBiasInfo, pNotiMapSrv, gmBiasInfo );
	} else {
		//通知所有的mapsrv
		CManMapSrvs::SendBroadcastMsg<GMBiasInfo>( &gmBiasInfo );
	}

	return true;
	TRY_END;
	return false;
}

bool CDealRankSrvPkg::OnRecvRankChartInfo(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR( "OnRecvRankChartInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(FGRankChartInfo) )
	{
		return false;
	}

	FGRankChartInfo*  rankChartInfoPage = (FGRankChartInfo *)pPkg;

	GMRankPlayerInfoUpdate gmRankPlayerUpdate;
	StructMemSet( gmRankPlayerUpdate, 0x0, sizeof(GMRankPlayerInfoUpdate) );
	gmRankPlayerUpdate.rankChartPage = rankChartInfoPage->pageIndex;
	gmRankPlayerUpdate.rankCount     = rankChartInfoPage->Count;
	gmRankPlayerUpdate.rankID        = rankChartInfoPage->rankID;
	D_DEBUG("收到了排行榜%d的第%d页的信息，有%d元素\n",gmRankPlayerUpdate.rankID,gmRankPlayerUpdate.rankChartPage,gmRankPlayerUpdate.rankCount );
	D_DEBUG("-------------------------------------------------------------------------\n");
	for ( int i = 0; i< gmRankPlayerUpdate.rankCount ; i++ )
	{
		if ( ( i >= ARRAY_SIZE(gmRankPlayerUpdate.rankChartSegArr) )
			|| ( i >= ARRAY_SIZE(rankChartInfoPage->rankChartSegArr) )
			)
		{
			D_ERROR( "CDealRankSrvPkg::OnRecvRankChartInfo, i(%d), gmRankPlayerUpdate或rankChartInfoPage越界\n", i );
			return false;
		}
		RankChartPlayer& rankPlayer       = gmRankPlayerUpdate.rankChartSegArr[i];
		RankChartPlayer& updateRankPlayer = rankChartInfoPage->rankChartSegArr[i];
		StructMemCpy( rankPlayer, &updateRankPlayer, sizeof(RankChartPlayer) );
		D_DEBUG("%d. 姓名:%s 排行值:%d\n", (gmRankPlayerUpdate.rankChartPage-1)*10 + i,rankPlayer.playerName,rankPlayer.rankInfo );
	}

	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMRankPlayerInfoUpdate>( &gmRankPlayerUpdate );

	return true;
	TRY_END;
	return false;
}

bool CDealRankSrvPkg::OnRecvPunishmentPlayer( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen   )
{
	if ( pOwner == NULL )
	{
		D_ERROR( "OnRecvPunishmentPlayer,pOwner空\n" );
		return false;
	}

	if ( sizeof(FGPunishmentPlayer) != wPkgLen )
	{
		D_ERROR("OnRecvPunishmentPlayer,wPkgLen长度错误\n");
		return false;
	}

	TRY_BEGIN;

	FGPunishmentPlayer* punishmentPlayer =  (FGPunishmentPlayer *)pPkg;
	if( punishmentPlayer->punishCount[E_HUMAN_TYPE] == 0 && punishmentPlayer->punishCount[E_AKA_TYPE] == 0 
		&& punishmentPlayer->punishCount[E_ELF_TYPE] == 0 )
	{
		D_WARNING("CDealRankSrvPkg::OnRecvPunishmentPlayer 发来的天谴人员人数为0\n");
		return false;
	}

	GMPunishmentStart mapMsg;
	for( int i=0; i< E_RACE_MAX ; ++i )
	{
		if( i == E_HUMAN_TYPE )		//人类
		{
			for( int j=0; j< punishmentPlayer->punishCount[i]; j++ )
			{
				mapMsg.humanRace.player[j].mapID = 0;
				mapMsg.humanRace.player[j].punishTime = RandNumber( 1,59 );
				mapMsg.humanRace.player[j].bPunished = false;
				SafeStrCpy( mapMsg.humanRace.player[j].name, punishmentPlayer->playerName[E_HUMAN_TYPE*E_RACE_MAX+j] );
				mapMsg.humanRace.player[j].posX = 0;
				mapMsg.humanRace.player[j].posY = 0;
			}
			mapMsg.humanRace.playerNum = punishmentPlayer->punishCount[i];
		}

		if( i == E_AKA_TYPE){   //艾卡
			for( int j=0; j< punishmentPlayer->punishCount[i]; j++ )
			{
				mapMsg.ekkaRace.player[j].mapID = 0;
				mapMsg.ekkaRace.player[j].punishTime = RandNumber( 1,59 );
				mapMsg.ekkaRace.player[j].bPunished = false;
				SafeStrCpy( mapMsg.ekkaRace.player[j].name, punishmentPlayer->playerName[E_AKA_TYPE*E_RACE_MAX+j] );
				mapMsg.ekkaRace.player[j].posX = 0;
				mapMsg.ekkaRace.player[j].posY = 0;
			}
			mapMsg.ekkaRace.playerNum = punishmentPlayer->punishCount[i];
		}

		if( i == E_ELF_TYPE )
		{
			for( int j=0; j< punishmentPlayer->punishCount[i]; j++ )
			{
				mapMsg.elfRace.player[j].mapID = 0;
				mapMsg.elfRace.player[j].punishTime = RandNumber( 1,59 );
				mapMsg.elfRace.player[j].bPunished = false;
				SafeStrCpy( mapMsg.elfRace.player[j].name, punishmentPlayer->playerName[E_ELF_TYPE*E_RACE_MAX+j] );
				mapMsg.ekkaRace.player[j].posX = 0;
				mapMsg.ekkaRace.player[j].posY = 0;
			}
			mapMsg.elfRace.playerNum = punishmentPlayer->punishCount[i];
		}
	}
	CManMapSrvs::SendBroadcastMsg<GMPunishmentStart>( &mapMsg );	//向所有Srv通知天谴开始

	GESrvGroupChat raceMsg;
	raceMsg.chatType = CT_RACE_SYS_EVENT;

	string strName;
	string strChat ="作恶多端触犯天怒,他们将不再受到主神的庇护,伟大的主神将降罪于他.各位主神的勇士们抓住并且杀了他们,主神将永远庇护着你们!";
	if( mapMsg.humanRace.playerNum != 0 )
	{
		strName.clear();
		for( int i=0;i<mapMsg.humanRace.playerNum; i++ )
		{
			strName += mapMsg.humanRace.player[i].name;
			if( i != mapMsg.humanRace.playerNum-1 )
			{
				strName += ",";
			}
		}
		strName += strChat;
		SafeStrCpy( raceMsg.chatContent, strName.c_str() );
		raceMsg.extInfo = RACE_HUMAN;
		SendMsgToCenterSrv( GESrvGroupChat, raceMsg );
	}

	if( mapMsg.elfRace.playerNum != 0 )
	{
		strName.clear();
		for( int i=0;i<mapMsg.elfRace.playerNum; ++i)
		{
			strName += mapMsg.elfRace.player[i].name;
			if( i != mapMsg.elfRace.playerNum-1 )
			{
				strName += ",";
			}
		}
		strName += strChat;
		SafeStrCpy( raceMsg.chatContent, strName.c_str() );
		raceMsg.extInfo = RACE_ELF;
		SendMsgToCenterSrv( GESrvGroupChat, raceMsg );
	}

	if( mapMsg.ekkaRace.playerNum != 0 )
	{
		strName.clear();
		for( int i=0;i< mapMsg.ekkaRace.playerNum; ++i)
		{
			strName += mapMsg.ekkaRace.player[i].name;
			if( i != mapMsg.ekkaRace.playerNum-1 )
			{
				strName += ",";
			}
		}
		strName += strChat;
		SafeStrCpy( raceMsg.chatContent, strName.c_str() );
		raceMsg.extInfo = RACE_ARKA;
		SendMsgToCenterSrv( GESrvGroupChat, raceMsg );
	}

	//通知centersrv全服通知活动的状态
	GENotifyActivityState censrvMsg;
	censrvMsg.activityState = GCNotifyActivityState::ACTIVITY_START;
	censrvMsg.activityType = GCNotifyActivityState::PUNISHMENT_ACTIVITY;
	SendMsgToCenterSrv( GENotifyActivityState, censrvMsg );

	TRY_END;
	return true;
}

bool CDealRankSrvPkg::OnRecvClearRankChart(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR( "OnRecvClearRankChart,pOwner空\n" );
		return false;
	}

	if ( sizeof(FGRankChartRefresh) != wPkgLen )
	{
		D_ERROR("OnRecvClearRankChart,wPkgLen长度错误\n");
		return false;
	}

	FGRankChartRefresh* pRankRefresh = (FGRankChartRefresh* )pPkg;

	GMRankChartRefresh rankFreshMsg;
	rankFreshMsg.rankChartID = pRankRefresh->rankID;

	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMRankChartRefresh>( &rankFreshMsg );
	return true;
}

bool CDealRankSrvPkg::OnRecvQueryRankInfoByPlayerName(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnRecvQueryRankInfoByPlayerName: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGQueryRankInfoByNameRst)  != wPkgLen )
	{
		D_ERROR("OnRecvQueryRankInfoByPlayerName, wPkgLen 长度错误\n");
		return false;
	}

	FGQueryRankInfoByNameRst* pRankInfoRst = ( FGQueryRankInfoByNameRst *)pPkg;

	if ( SELF_SRV_ID != pRankInfoRst->queryPlayerID.wGID )
	{
		D_WARNING( "收到functionsrv发来非本GateSrv管理玩家的相关信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pRankInfoRst->queryPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	GCQueryRankPlayerByNameRst queryRst;
	StructMemSet( queryRst, 0x0, sizeof(GCQueryRankPlayerByNameRst) );
	if ( pRankInfoRst->isSuc )//如果搜索成功
	{
		queryRst.rankIndex = pRankInfoRst->rankIndex;
		queryRst.queryRst  = true;
		ConvertRankPlayerToRankItem( queryRst.rankPlayer, pRankInfoRst->rankPlayer );
	}
	else
	{
		queryRst.queryRst = false;
	}

	NewSendPlayerPkg( GCQueryRankPlayerByNameRst, pPlayer, queryRst );
	//MsgToPut* pClietMsg = CreatePlayerPkg( GCQueryRankPlayerByNameRst, pPlayer, queryRst );
	//pPlayer->SendPkgToPlayer( pClietMsg );

	return true;
}


bool CDealRankSrvPkg::OnQueryRecvMailPlayerExist(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnQueryRecvMailPlayerExist: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGQueryRecvMailPlayerInfo)  != wPkgLen )
	{
		D_ERROR("OnQueryRecvMailPlayerExist, wPkgLen 长度错误\n");
		return false;
	}

	FGQueryRecvMailPlayerInfo* pQueryInfo = ( FGQueryRecvMailPlayerInfo *)pPkg;

	//向DB查询玩家是否存在
	CDbSrv* pDbSrv = CManDbSrvs::GetValidDBSrv();
	if( pDbSrv )
	{
		GDQueryPlayerExist querySrv;
		querySrv.queryType = E_QUERY_MAIL;
		querySrv.queryUID  = pQueryInfo->mailUID;
		SafeStrCpy( querySrv.recvPlayerName, pQueryInfo->recvPlayerName );


		//MsgToPut* pqueryRole = CreateSrvPkg( GDQueryPlayerExist, pDbSrv, querySrv );
		//pDbSrv->SendPkgToSrv( pqueryRole );
		SendMsgToSrv( GDQueryPlayerExist, pDbSrv, querySrv );
	}

	return true;
}

bool CDealRankSrvPkg::OnPickMoneyFromMail(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnPickMoneyFromMail: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGPickMailAttachMoney)  != wPkgLen )
	{
		D_ERROR("OnPickMoneyFromMail, wPkgLen 长度错误\n");
		return false;
	}

	TRY_BEGIN;

	FGPickMailAttachMoney* pPickMoney = (FGPickMailAttachMoney *)pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pPickMoney->pickPlayerID.dwPID );
	if ( pPlayer )
	{
		CMapSrv* pMapSrv = pPlayer->GetMapSrv();
		if ( pMapSrv )
		{
			GMPickMailMoney pickMoneyMsg;
			pickMoneyMsg.mailuid = pPickMoney->mailuid;//属于哪个邮件的编号
			pickMoneyMsg.money   = pPickMoney->money;
			pickMoneyMsg.pickPlayerID = pPlayer->GetPlayerID();

			//MsgToPut* pMsg = CreateSrvPkg( GMPickMailMoney, pMapSrv, pickMoneyMsg );
			//pMapSrv->SendPkgToSrv( pMsg );
			SendMsgToSrv( GMPickMailMoney, pMapSrv, pickMoneyMsg );
		}
	}	

	TRY_END;

	return true;
}

bool CDealRankSrvPkg::OnQueryMailDetialInfoRst(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnQueryMailDetialInfoRst: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGQueryMailDetialInfoRst)  != wPkgLen )
	{
		D_ERROR("OnQueryMailDetialInfoRst, wPkgLen 长度错误\n");
		return false;
	}

	FGQueryMailDetialInfoRst* pMailDetial =( FGQueryMailDetialInfoRst *)pPkg;

	TRY_BEGIN;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMailDetial->queryPlayer.dwPID );
	if ( pPlayer )
	{
		GCQueryMailDetialInfoRst srvmsg;
		StructMemSet( srvmsg, 0x0 , sizeof(srvmsg) );
		srvmsg.mailuid = pMailDetial->mailuid;
		SafeStrCpy2( srvmsg.szSendPlayerName, pMailDetial->szSendPlayerName,srvmsg.nameLen );
		SafeStrCpy2( srvmsg.szMailTitle,      pMailDetial->szMailTitle, srvmsg.titleLen );
		SafeStrCpy2( srvmsg.szMailContent,    pMailDetial->szContent,   srvmsg.contentLen );
		/*srvmsg.nameLen = ACE_OS::snprintf( srvmsg.szSendPlayerName, sizeof(srvmsg.szSendPlayerName), "%s",pMailDetial->szSendPlayerName );
		srvmsg.titleLen = ACE_OS::snprintf( srvmsg.szMailTitle, sizeof(srvmsg.szMailTitle),"%s", pMailDetial->szMailTitle );
		srvmsg.contentLen = ACE_OS::snprintf( srvmsg.szMailContent, sizeof( srvmsg.szMailContent), "%s", pMailDetial->szContent );*/
		NewSendPlayerPkg( GCQueryMailDetialInfoRst, pPlayer, srvmsg );
	}

	TRY_END;

	return true;
}

bool CDealRankSrvPkg::OnQueryMailAttachInfoRst(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnQueryMailAttachInfoRst: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGQueryMailDetialAttachInfoRst)  != wPkgLen )
	{
		D_ERROR("OnQueryMailAttachInfoRst, wPkgLen 长度错误\n");
		return false;
	}

	FGQueryMailDetialAttachInfoRst* pMailAttach = ( FGQueryMailDetialAttachInfoRst* )pPkg;

	TRY_BEGIN;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMailAttach->queryPlayer.dwPID );
	if ( pPlayer )
	{
		GCQueryMailAttachInfoRst srvmsg;
		srvmsg.mailuid   = pMailAttach->mailuid;
		srvmsg.itemCount = MAX_MAIL_ATTACHITEM_COUNT;
		srvmsg.silverMoney     = pMailAttach->silverMoney;
		srvmsg.goldMoney     = pMailAttach->goldMoney;
		StructMemCpy( srvmsg.szItemInfo, pMailAttach->attachItem, sizeof(ItemInfo_i)*MAX_MAIL_ATTACHITEM_COUNT );

		NewSendPlayerPkg( GCQueryMailAttachInfoRst, pPlayer, srvmsg );
		//MsgToPut* pClietMsg = CreatePlayerPkg( GCQueryMailAttachInfoRst, pPlayer, srvmsg );
		//pPlayer->SendPkgToPlayer( pClietMsg );
	}

	TRY_END;

	return true;
}

bool CDealRankSrvPkg::OnSendMailError(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnSendMailError: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGSendMailFail)  != wPkgLen )
	{
		D_ERROR("OnSendMailError, wPkgLen 长度错误\n");
		return false;
	}

	FGSendMailFail* pSendMsg = ( FGSendMailFail *)pPkg;

	TRY_BEGIN;

	if ( SELF_SRV_ID != pSendMsg->sendPlayerID.wGID )
	{
		D_ERROR("OnSendMailError 发送的GateSrv不对\n");
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pSendMsg->sendPlayerID.dwPID );
	if ( pPlayer )
	{
		GCSendMailError srvmsg;
		srvmsg.errNo = pSendMsg->errNo;

		NewSendPlayerPkg( GCSendMailError, pPlayer, srvmsg );
		//MsgToPut* pMsgToPut = CreatePlayerPkg( GCSendMailError, pPlayer,srvmsg );
		//pPlayer->SendPkgToPlayer( pMsgToPut );
		return true;
	}
	else
	{
		D_ERROR("发送邮件失败，找不到通知的玩家\n");
		return false;
	}

	TRY_END;

	return true;
}

bool CDealRankSrvPkg::OnDeleteMailRst(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnDeleteMailRst: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGDeleteMailRst)  != wPkgLen )
	{
		D_ERROR("OnDeleteMailRst, wPkgLen 长度错误\n");
		return false;
	}

	FGDeleteMailRst* pDelMailRst = ( FGDeleteMailRst *)pPkg;

	TRY_BEGIN;

	if ( SELF_SRV_ID != pDelMailRst->deletePlayerID.wGID )
	{
		D_ERROR("OnDeleteMailRst 发送的GateSrv不对\n");
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pDelMailRst->deletePlayerID.dwPID );
	if ( pPlayer )
	{
		GCDeleteMailRst srvmsg;
		srvmsg.mailuid = pDelMailRst->mailuid;
		srvmsg.isSucc  = pDelMailRst->delmailRst;

		NewSendPlayerPkg( GCDeleteMailRst, pPlayer, srvmsg );
		//MsgToPut* pMsgToPut = CreatePlayerPkg( GCDeleteMailRst, pPlayer,srvmsg );
		//pPlayer->SendPkgToPlayer( pMsgToPut );
		return true;
	}
	else
	{
		D_ERROR("OnDeleteMailRst 删除邮件找不到玩家\n");
		return false;
	}

	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnQueryMailListByPageRst(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnQueryMailListByPageRst: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGQueryMailListInfoByPageRst)  != wPkgLen )
	{
		D_ERROR("OnQueryMailListByPageRst, wPkgLen 长度错误\n");
		return false;
	}

	FGQueryMailListInfoByPageRst* pMailPage = ( FGQueryMailListInfoByPageRst *)pPkg;

	TRY_BEGIN;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMailPage->queryPlayer.dwPID );
	if ( pPlayer )
	{
		if(pPlayer->GetRoleID() ==  pMailPage->mailOwnerUID)
		{
			GCQueryMailListByPageRst srvmsg;
			StructMemSet( srvmsg, 0x0 ,sizeof(GCQueryMailListByPageRst) );
			srvmsg.pageEnd   = pMailPage->pageEnd;
			srvmsg.pageIndex = pMailPage->pageIndex;
			srvmsg.arrCount =  pMailPage->mailArrCount;
			StructMemCpy( srvmsg.mailList, pMailPage->mailConciseInfo, MAX_MAIL_CONCISE_COUNT* sizeof(MailConciseInfo) );

			NewSendPlayerPkg( GCQueryMailListByPageRst, pPlayer, srvmsg );
			//MsgToPut* pClietMsg = CreatePlayerPkg( GCQueryMailListByPageRst, pPlayer, srvmsg );
			//pPlayer->SendPkgToPlayer( pClietMsg );
		}
		else
		{
			GTCmd_Query_Rsp resp;
			StructMemSet(resp, 0x00, sizeof(resp));

			resp.uType = GTCT_ROLE_MAIL;
			resp.xKey.unKey.xRoleKey.uRoleID = pMailPage->mailOwnerUID ;
			resp.xKey.unKey.xRoleKey.uType = pMailPage->mailType;
			resp.xKey.unKey.xRoleKey.uPage = pMailPage->pageIndex;
			resp.xInfo.none = GTCT_ROLE_MAIL;

			GTCmd_Info_Role_Mail &mailInfo =resp.xInfo.xInfo.xRoleMail;
			mailInfo.uCount =  pMailPage->mailArrCount;
			StructMemCpy( mailInfo.xMail, pMailPage->mailConciseInfo, MAX_MAIL_CONCISE_COUNT * sizeof(MailConciseInfo) );

			NewSendPlayerPkg( GTCmd_Query_Rsp, pPlayer, resp );
		}
	}
	return true;

	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnNoticePlayerHaveUnReadMail(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnNoticePlayerHaveUnReadMail: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGNoticePlayerHaveUnReadMail)  != wPkgLen )
	{
		D_ERROR("OnNoticePlayerHaveUnReadMail, wPkgLen 长度错误\n");
		return false;
	}

	TRY_BEGIN;

	FGNoticePlayerHaveUnReadMail* pNoticeMsg = ( FGNoticePlayerHaveUnReadMail* )pPkg;

	GENoticePlayerHaveUnReadMail noticemsg;
	SafeStrCpy( noticemsg.recvPlayerName, pNoticeMsg->recvPlayerName );
	noticemsg.unreadmailCount = pNoticeMsg->unreadCount;

	SendMsgToCenterSrv( GENoticePlayerHaveUnReadMail, noticemsg );
	return true;

	TRY_END;

	return false;
}


bool CDealRankSrvPkg::OnUpdateUnReadMailCount(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnUpdateUnReadMailCount: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGUpdatePlayerUnReadMailCount)  !=  wPkgLen )
	{
		D_ERROR("OnUpdateUnReadMailCount, wPkgLen 长度错误\n");
		return false;
	}

	FGUpdatePlayerUnReadMailCount* pUpdateMsg = ( FGUpdatePlayerUnReadMailCount *)pPkg;

	if ( SELF_SRV_ID != pUpdateMsg->playerID.wGID )
	{
		D_ERROR("接到了一个不属于该GateSrv的消息 %d  ErrNo:%d \n",SELF_SRV_ID , pUpdateMsg->playerID.wGID );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pUpdateMsg->playerID.dwPID );
	if ( pPlayer )
	{

		GCPlayerHaveUnReadMail srvmsg;
		srvmsg.unReadMailCount = pUpdateMsg->unreadCount;

		NewSendPlayerPkg( GCPlayerHaveUnReadMail, pPlayer, srvmsg );
		//MsgToPut* pMsg = CreatePlayerPkg( GCPlayerHaveUnReadMail, pPlayer, srvmsg );
		//pPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return false;
}

bool CDealRankSrvPkg::OnRecvRemoveRankChartPlayer(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnRecvRemoveRankChartPlayer: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGRemoveRankPlayerByUID)  !=  wPkgLen )
	{
		D_ERROR("OnRecvRemoveRankChartPlayer, wPkgLen 长度错误\n");
		return false;
	}

	FGRemoveRankPlayerByUID* pRemoveRankPlayer = (FGRemoveRankPlayerByUID *)pPkg;

	GMRemoveChartPlayer srvmsg;
	srvmsg.rankChartID = pRemoveRankPlayer->rankID;
	srvmsg.playerUID   = pRemoveRankPlayer->playerUID;

	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMRemoveChartPlayer>( &srvmsg );
	return true;
}


bool CDealRankSrvPkg::OnExceptionNotify( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnExceptionNotify: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGExceptionNotify)  !=  wPkgLen )
	{
		D_ERROR("OnExceptionNotify, wPkgLen 长度错误\n");
		return false;
	}

	FGExceptionNotify* pException = (FGExceptionNotify *)pPkg;
	ACE_UNUSED_ARG( pException );

	stringstream ss;
	ss <<"FunctionSrv发生异常"; 

	GESrvGroupChat allChat;
	SafeStrCpy( allChat.chatContent, ss.str().c_str() );
	allChat.chatType = CT_WORLD_CHAT;
	allChat.extInfo = CT_SYS_EVENT_NOTI;
	SendMsgToCenterSrv( GESrvGroupChat, allChat );
	return true;
}


bool CDealRankSrvPkg::OnNoticeFirstUseStorage( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnNoticeFirstUseStorage: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGNoticeFirstUseStorage)  !=  wPkgLen )
	{
		D_ERROR("OnNoticeFirstUseStorage, wPkgLen 长度错误\n");
		return false;
	}

	FGNoticeFirstUseStorage* pFirstUse = ( FGNoticeFirstUseStorage *)pPkg;

	if ( SELF_SRV_ID != pFirstUse->playerID.wGID )
	{
		D_ERROR("接到了一个不属于该GateSrv的消息 %d  ErrNo:%d \n",SELF_SRV_ID , pFirstUse->playerID.wGID );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pFirstUse->playerID.dwPID );
	if ( pPlayer )
	{
		NewSendPlayerPkg( GCShowStoragePasswordDlg, pPlayer, pFirstUse->showPsdDlg  );
	}

	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnMapSrvLoadRaceMasterRst(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnNoticeFirstUseStorage: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGQueryRaceMasterInfoRst)  !=  wPkgLen )
	{
		D_ERROR("OnMapSrvLoadRaceMasterRst, wPkgLen 长度错误\n");
		return false;
	}

	FGQueryRaceMasterInfoRst* pRaceMasterInfo = ( FGQueryRaceMasterInfoRst *)pPkg;
	CManMapSrvs::SendBroadcastMsg<GMMapSrvLoadRaceMasterRst>( &pRaceMasterInfo->queryRst );

	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnForbiddenRaceMasterAuthority(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnForbiddenRaceMasterAuthority: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGForbiddenRaceMasterAuthority)  !=  wPkgLen )
	{
		D_ERROR("OnForbiddenRaceMasterAuthority, wPkgLen 长度错误\n");
		return false;
	}

	GMForbiddenRaceMasterAuthority srvmsg;
	CManMapSrvs::SendBroadcastMsg<GMForbiddenRaceMasterAuthority>( &srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnSelectRaceMaster(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnSelectRaceMaster: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGStartSelectRaceMaster)  !=  wPkgLen )
	{
		D_ERROR("OnSelectRaceMaster, wPkgLen 长度错误\n");
		return false;
	}

	D_DEBUG("接到Functionsrv请求选择族长的消息:FGStartSelectRaceMaster\n");

	//向relationSrv请求族长信息
	//CRelationSrv* pRelationSrv = CManRelationSrvs::GetRelationserver();
	//if ( !pRelationSrv )
	//	return false;

	QueryRaceMasterRequest srvmsg;
	//MsgToPut* pNewMsg = CreateRelationPkg( QueryRaceMasterRequest, pRelationSrv, srvmsg );
	//pRelationSrv->SendPkgToSrv( pNewMsg );
	SendMsgToRelationSrv( QueryRaceMasterRequest, srvmsg );

	D_DEBUG("将QueryRaceMasterRequest转发给Relationsrv\n");

	return true;
	TRY_END;
	return false;
}

bool CDealRankSrvPkg::OnStorageItemInfo(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnStorageItemInfo: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGNoticeStorageItemInfo)  !=  wPkgLen )
	{
		D_ERROR("OnStorageItemInfo, wPkgLen 长度错误\n");
		return false;
	}

	FGNoticeStorageItemInfo* pStorageItemInfo = ( FGNoticeStorageItemInfo *)pPkg;

	if ( SELF_SRV_ID != pStorageItemInfo->playerID.wGID )
	{
		D_ERROR("接到了一个不属于该GateSrv的消息 %d  ErrNo:%d \n",SELF_SRV_ID , pStorageItemInfo->playerID.wGID );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pStorageItemInfo->playerID.dwPID );
	if ( pPlayer )
	{
		CMapSrv* pMapsrv = pPlayer->GetMapSrv();
		if ( pMapsrv )
		{
			//MsgToPut* pMsg = CreateSrvPkg( GMStorageItemInfo, pMapsrv, pStorageItemInfo->itemInfo );
			//pMapsrv->SendPkgToSrv( pMsg );
			SendMsgToSrv( GMStorageItemInfo, pMapsrv, pStorageItemInfo->itemInfo );
		}
	}
	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnStorageSafeInfo(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnStorageSafeInfo: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGNoticeStorageSafeInfo)  !=  wPkgLen )
	{
		D_ERROR("OnStorageSafeInfo, wPkgLen 长度错误\n");
		return false;
	}

	FGNoticeStorageSafeInfo* pStorageSafeInfo = ( FGNoticeStorageSafeInfo *)pPkg;

	if ( SELF_SRV_ID != pStorageSafeInfo->playerID.wGID )
	{
		D_ERROR("接到了一个不属于该GateSrv的消息 %d  ErrNo:%d \n",SELF_SRV_ID , pStorageSafeInfo->playerID.wGID );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pStorageSafeInfo->playerID.dwPID );
	if ( pPlayer )
	{
		CMapSrv* pMapsrv = pPlayer->GetMapSrv();
		if ( pMapsrv )
		{
			//MsgToPut* pMsg = CreateSrvPkg( GMStorageSafeInfo, pMapsrv, pStorageSafeInfo->safeInfo );
			//pMapsrv->SendPkgToSrv( pMsg );
			SendMsgToSrv( GMStorageSafeInfo, pMapsrv, pStorageSafeInfo->safeInfo );
		}
	}
	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnStorageForbiddenInfo(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnStorageForbiddenInfo: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGNoticeStorageForbiddenInfo)  !=  wPkgLen )
	{
		D_ERROR("OnStorageForbiddenInfo, wPkgLen 长度错误\n");
		return false;
	}

	FGNoticeStorageForbiddenInfo* pStorageForbiddenInfo = ( FGNoticeStorageForbiddenInfo *)pPkg;

	if ( SELF_SRV_ID != pStorageForbiddenInfo->playerID.wGID )
	{
		D_ERROR("接到了一个不属于该GateSrv的消息 %d  ErrNo:%d \n",SELF_SRV_ID , pStorageForbiddenInfo->playerID.wGID );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pStorageForbiddenInfo->playerID.dwPID );
	if ( pPlayer )
	{
		CMapSrv* pMapsrv = pPlayer->GetMapSrv();
		if ( pMapsrv )
		{
			//MsgToPut* pMsg = CreateSrvPkg( GMStorageForbiddenInfo, pMapsrv, pStorageForbiddenInfo->forbiddenInfo );
			//pMapsrv->SendPkgToSrv( pMsg );
			SendMsgToSrv( GMStorageForbiddenInfo, pMapsrv, pStorageForbiddenInfo->forbiddenInfo );
		}
	}
	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnPickItemFromMail(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnPickItemFromMail: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGPickMailAttachItem)  != wPkgLen )
	{
		D_ERROR("OnPickItemFromMail, wPkgLen 长度错误\n");
		return false;
	}

	FGPickMailAttachItem* pPickItem = ( FGPickMailAttachItem *)pPkg;

	if ( SELF_SRV_ID != pPickItem->pickPlayerID.wGID )
	{
		D_ERROR("接到了一个不属于该GateSrv的消息 %d  ErrNo:%d \n",SELF_SRV_ID , pPickItem->pickPlayerID.wGID );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pPickItem->pickPlayerID.dwPID );
	if ( pPlayer )
	{
		CMapSrv* pMapsrv = pPlayer->GetMapSrv();
		if ( pMapsrv )
		{
			GMPickMailItem srvmsg;
			srvmsg.itemIndex = pPickItem->pickIndex;
			srvmsg.itemInfo  = pPickItem->itemInfo;
			srvmsg.pickPlayerID = pPickItem->pickPlayerID;
			srvmsg.mailuid   = pPickItem->mailuid;//属于哪个邮件的编号


			//MsgToPut* pMsg = CreateSrvPkg( GMPickMailItem, pMapsrv, srvmsg );
			//pMapsrv->SendPkgToSrv( pMsg );
			SendMsgToSrv( GMPickMailItem, pMapsrv, srvmsg );
		}
	}	
	return true;
}

bool CDealRankSrvPkg::OnSendMailNotify( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealRankSrvPkg::OnSendMailNotify: pOwner为空\n");
		return false;
	}

	if ( sizeof(FGSendMailSuccNotifyGM)  != wPkgLen )
	{
		D_ERROR("OnSendMailNotify, wPkgLen 长度错误\n");
		return false;
	}

	FGSendMailSuccNotifyGM* pMailNotify = ( FGSendMailSuccNotifyGM *)pPkg;

	CLogManager::DoSendMail(NULL, pMailNotify->recvPlayerUID, pMailNotify->mailUID);//记日至

	return true;
}



bool CDealRankSrvPkg::OnRecvPublicNotices( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGRecvPublicNotice );

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMsg->queryPlayerID.dwPID );
	if ( pPlayer )
	{
		NewSendPlayerPkg( GCRecvPublicNotices, pPlayer, pMsg->srvmsg );
		return true;
	}
	else
	{
		D_ERROR("在GateSrv:%d上,找不到玩家%d\n", pMsg->queryPlayerID.wGID,pMsg->queryPlayerID.dwPID );
	}

	TRY_END;
	return true;
}

bool CDealRankSrvPkg::OnModifyPublicNotice( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGModifyPublicNotice );

	CManMapSrvs::SendBroadcastMsg<GMModifyPublicNotice>( &pMsg->srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnRemovePublicNotice( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGRemovePublicNotice );

	GMRemovePublicNotice srvmsg;
	srvmsg.msgid = pMsg->msgid;
	CManMapSrvs::SendBroadcastMsg<GMRemovePublicNotice>( &srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnAddPublicNotice( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGAddNewPublicNotice );

	CManMapSrvs::SendBroadcastMsg<GMAddNewPublicNotice>( &pMsg->srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnBidItemRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGBidItemRst );

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMsg->bidPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "OnBidItemRst 收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	if ( pMsg->bidItemRst.rstNo != 0 )//如果竞拍失败了，把钱退给玩家
	{
		GMSubPlayerMoney srvmsg;
		srvmsg.playerID = pPlayer->GetPlayerID();
		srvmsg.subMoney = pMsg->costprice;
		SendMsgToMapSrvByPlayer( GMSubPlayerMoney,pPlayer, srvmsg );
	}

	NewSendPlayerPkg( GCBidAuctionItemRst, pPlayer, pMsg->bidItemRst );
	return true;

	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnBidItemSection1( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGBidItemSection1 );

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "OnBidItemSection1 收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	GMPlayerBidItem srvmsg;
	srvmsg.lNotiPlayerID = pPlayer->GetPlayerID();
	srvmsg.auctionUID    = pMsg->auctionUID;
	srvmsg.lastprice     = pMsg->lastPrice;
	srvmsg.newprice      = pMsg->newprice;

	SendMsgToMapSrvByPlayer( GMPlayerBidItem, pPlayer, srvmsg );
	return true;

	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnQueryAuctionConditionRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGQueryAuctionConditionRst );

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMsg->queryPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "OnQueryAuctionConditionRst 收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	NewSendPlayerPkg( GCQueryAuctionConditionRst, pPlayer, pMsg->queryRst );
	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnCancelAuctionRst(  CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGCancelAuctionRst );

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMsg->cancePlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "OnCancelAuctionRst 收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	NewSendPlayerPkg( GCCancelMyAuctionRst, pPlayer, pMsg->cancelRst );
	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnQueryMyBidRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGQueryMyBidRst );

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMsg->queryPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "OnQueryMyBidRst 收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	NewSendPlayerPkg( GCQueryMyBidStateRst, pPlayer, pMsg->queryBidState );
	return true;
	TRY_END;

	return false;
}

bool CDealRankSrvPkg::OnQueryMyAuctionRst( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_FUNCTION_MSG_CHECK( FGQueryMyAuctionRst );

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMsg->queryPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "OnQueryMyAuctionRst 收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	NewSendPlayerPkg( GCQueryMyAuctionsRst, pPlayer, pMsg->auctionRst );
	return true;
	TRY_END;

	return false;
}

void CDealRankSrvPkg::ConvertRankPlayerToRankItem(RankChartItem& rankItem, const RankChartPlayer& rankPlayer )
{
	rankItem.itemUID = rankPlayer.playerUID;
	rankItem.rankInfo = rankPlayer.rankInfo;
	rankItem.itemLevel = rankPlayer.level;
	SafeStrCpy( rankItem.itemName, rankPlayer.playerName );
	rankItem.itemNameLen  = (char)ACE_OS::strlen( rankPlayer.playerName );
	ACE_OS::sprintf( rankItem.itemClass, "%d", rankPlayer.CLASS );
	rankItem.itemRace = (char)rankPlayer.race;
	rankItem.itemClassLen = 1;
}

///functionsrv发来的排行榜查询结果；
bool CDealRankSrvPkg::OnNewRankInfo( CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR( "OnNewRankInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen !=  sizeof(FGNewRankInfo) )
	{
		D_ERROR("OnNewRankInfo,wPkgLen大小不对\n");
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealRankSrvPkg::OnNewRankInfo, NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	FGNewRankInfo* pRankInfo = (FGNewRankInfo *)pPkg;

	if ( SELF_SRV_ID != pRankInfo->notifyPlayerID.wGID )
	{
		D_WARNING( "收到functionsrv发来非本GateSrv管理玩家的相关信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pRankInfo->notifyPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	pPlayer->FillSelfRankInfo( pRankInfo->rankInfo );

	NewSendPlayerPkg( GCNewRankInfo, pPlayer, pRankInfo->rankInfo );

	return true;
	TRY_END;
	return false;
} // bool CDealRankSrvPkg::OnNewRankInfo(  CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )

//bool CDealRankSrvPkg::OnRecvQueryRankChartByPageRst(CFunctionSrv* pOwner,const char* pPkg, const unsigned short wPkgLen )
//{
//	if ( pOwner == NULL )
//	{
//		D_ERROR( "OnRecvQueryRankChartByPageRst,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen !=  sizeof(FGRankChartInfoByPage) )
//	{
//		D_ERROR("OnRecvRankChartInfo,wPkgLen大小不对\n");
//		return false;
//	}
//
//	if ( NULL == pPkg )
//	{
//		D_ERROR( "CDealRankSrvPkg::OnRecvQueryRankChartByPageRst, NULL == pPkg\n" );
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	FGRankChartInfoByPage* pRankChartRst = (FGRankChartInfoByPage *)pPkg;
//
//	if ( SELF_SRV_ID != pRankChartRst->notifyPlayerID.wGID )
//	{
//		D_WARNING( "收到functionsrv发来非本GateSrv管理玩家的相关信息\n" );
//		return false;
//	}
//
//	CPlayer* pPlayer = CManPlayer::FindPlayer( pRankChartRst->notifyPlayerID.dwPID );
//	if ( NULL == pPlayer )
//	{
//		D_WARNING( "收到function发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
//		return false;
//	}
//
//	//如果消息有两页
//	if ( pRankChartRst->Count > MAX_CHARTPAGE_LEN )
//	{
//		//发送前半页
//		GCRankInfoByPageRst rankInfoRst1;
//		StructMemSet( rankInfoRst1 ,0x0, sizeof(GCRankInfoByPageRst) );
//		rankInfoRst1.rankCount     = MAX_CHARTPAGE_LEN;
//		rankInfoRst1.rankPageIndex = pRankChartRst->pageIndex;
//		for ( int i = 0 ; i< rankInfoRst1.rankCount ; i++ )
//		{
//			if ( ( i >= ARRAY_SIZE( rankInfoRst1.rankInfoArr ) )
//				|| ( i >= ARRAY_SIZE( pRankChartRst->rankChartSegArr ) )
//				)
//			{
//				D_ERROR( "CDealRankSrvPkg::OnRecvQueryRankChartByPageRst, i(%d), rankInfoRst1.rankInfoArr或pRankChartRst->rankChartSegArr越界\n", i );
//				return false;
//			}
//			ConvertRankPlayerToRankItem( rankInfoRst1.rankInfoArr[i], pRankChartRst->rankChartSegArr[i] );
//		}
//
//		NewSendPlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst1 );
//		//MsgToPut* pClietMsg1 = CreatePlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst1 );
//		//pPlayer->SendPkgToPlayer( pClietMsg1 );
//
//		//发送后半页
//		GCRankInfoByPageRst rankInfoRst2;
//		StructMemSet( rankInfoRst2 ,0x0, sizeof(GCRankInfoByPageRst) );
//		rankInfoRst2.rankCount     =  pRankChartRst->Count - MAX_CHARTPAGE_LEN ;
//		rankInfoRst2.rankPageIndex =  pRankChartRst->pageIndex;
//		rankInfoRst2.pageEnd       =  pRankChartRst->pageEnd;
//		for ( int i = 0 ; i< rankInfoRst2.rankCount ; i++ )
//		{
//			if ( ( i >= ARRAY_SIZE(rankInfoRst2.rankInfoArr) )
//				|| ( (MAX_CHARTPAGE_LEN+i) >= ARRAY_SIZE(pRankChartRst->rankChartSegArr) )
//				)
//			{
//				D_ERROR( "OnRecvQueryRankChartByPageRst, i(%d)或MAX_CHARTPAGE_LEN+i越界\n", i );
//				return false;
//			}
//			ConvertRankPlayerToRankItem( rankInfoRst2.rankInfoArr[i], pRankChartRst->rankChartSegArr[ MAX_CHARTPAGE_LEN + i ] );
//		}
//
//		NewSendPlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst2 );
//		//MsgToPut* pClietMsg2 = CreatePlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst2 );
//		//pPlayer->SendPkgToPlayer( pClietMsg2 );
//	}
//	else
//	{
//		GCRankInfoByPageRst rankInfoRst;
//		StructMemSet( rankInfoRst ,0x0, sizeof(GCRankInfoByPageRst) );
//		rankInfoRst.rankCount     = pRankChartRst->Count;
//		rankInfoRst.rankPageIndex = pRankChartRst->pageIndex;
//		rankInfoRst.pageEnd       = pRankChartRst->pageEnd;
//		for ( int i = 0 ; i< rankInfoRst.rankCount ; i++ )
//		{
//			if ( ( i >= ARRAY_SIZE( rankInfoRst.rankInfoArr ) )
//				|| ( i >= ARRAY_SIZE( pRankChartRst->rankChartSegArr ) )
//				)
//			{
//				D_ERROR( "OnRecvQueryRankChartByPageRst, i(%d)， rankInfoRst.rankInfoArr或pRankChartRst->rankChartSegArr越界\n", i );
//				return false;
//			}
//			ConvertRankPlayerToRankItem( rankInfoRst.rankInfoArr[i], pRankChartRst->rankChartSegArr[i] );
//		}
//
//		NewSendPlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst );
//		//MsgToPut* pClietMsg = CreatePlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst );
//		//pPlayer->SendPkgToPlayer( pClietMsg );
//	}
//
//	TRY_END;
//
//	return true;
//}