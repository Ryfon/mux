﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"


HASH_MAP<unsigned int, PkgStatManager::PkgStatInfo> PkgStatManager::m_recvFromCli;
HASH_MAP<unsigned int, PkgStatManager::PkgStatInfo> PkgStatManager::m_recvFromSrv;
HASH_MAP<unsigned int, PkgStatManager::PkgStatInfo> PkgStatManager::m_sendToCli;
HASH_MAP<unsigned int, PkgStatManager::PkgStatInfo> PkgStatManager::m_sendToSrv;
unsigned int PkgStatManager::m_start = 0;

void PkgStatManager::Init()
{
	m_start = GetTickCount();
}

void PkgStatManager::RecvFromCli(unsigned int pkgCmd, unsigned int pkgSize)
{
	DealPkgStat( pkgCmd, pkgSize, m_recvFromCli );
}

void PkgStatManager::RecvFromSrv(unsigned int pkgCmd, unsigned int pkgSize)
{
	DealPkgStat( pkgCmd, pkgSize, m_recvFromSrv );
}

void PkgStatManager::SendToCli(unsigned int pkgCmd, unsigned int pkgSize)
{
	DealPkgStat( pkgCmd, pkgSize, m_sendToCli );
}

void PkgStatManager::SendToSrv(unsigned int pkgCmd, unsigned int pkgSize)
{
	DealPkgStat( pkgCmd, pkgSize, m_sendToSrv );
}

void PkgStatManager::DealPkgStat( unsigned int pkgCmd, unsigned int pkgSize, HASH_MAP<unsigned int, PkgStatInfo> & pkgStat)
{
#ifdef PKG_STAT
	HASH_MAP<unsigned int, PkgStatInfo>::iterator it = pkgStat.find(pkgCmd);
	if ( it == pkgStat.end() )
	{
		PkgStatInfo info;
		info.pkgCmd = pkgCmd;
		info.pkgSize = pkgSize;
		info.pkgNum = 1;
		pkgStat.insert( std::make_pair(pkgCmd,info) );
	}
	else
	{
		++ (it->second.pkgNum);
	}
#endif
}

void PkgStatManager::LogStatResult(unsigned int type)
{
#ifdef PKG_STAT
	if (type | RECV_FROM_CLI)
	{
		D_DEBUG("收到客户端包统计\n");
		LogStatResult( m_recvFromCli );
	}
	if (type | RECV_FROM_SRV)
	{
		D_DEBUG("收到服务端包统计\n");
		LogStatResult( m_recvFromSrv );
	}
	if (type | SEND_TO_CLI )
	{
		D_DEBUG("发往客户端包统计\n");
		LogStatResult( m_sendToCli );
	}
	if (type | SEND_TO_SRV )
	{
		D_DEBUG("发往服务端包统计\n");
		LogStatResult( m_sendToSrv );
	}
#endif
}

void PkgStatManager::LogStatResult( const HASH_MAP<unsigned int, PkgStatInfo> & pkgStat)
{
	D_DEBUG( "//////////////////////////////////////////////////////////////" );
	unsigned int now = GetTickCount();
	unsigned int passedmsec = now - m_start;
	D_DEBUG( "包统计..., 区间起始时刻:%u(msec),当前时刻:%u(msec),区间用时:%u(msec)", m_start, now, passedmsec );
	D_DEBUG( "命令字\t\t|\t\t包大小\t\t|\t\t包数量\t\t|\t\t总大小\t\t|\t\tbits/秒\n");
	unsigned int totalbits = 0;
	for ( HASH_MAP<unsigned int, PkgStatInfo>::const_iterator iter=pkgStat.begin(); iter!=pkgStat.end(); ++iter )
	{
		const PkgStatInfo &tempPkgStat = iter->second;
		totalbits = tempPkgStat.pkgSize*tempPkgStat.pkgNum*8;
		D_DEBUG( "%x\t\t|\t\t%u\t\t|\t\t%u\t\t|\t\t%u\t\t|\t\t%f\n", tempPkgStat.pkgCmd, tempPkgStat.pkgSize, tempPkgStat.pkgNum, totalbits, (float)totalbits/(passedmsec/1000) );
	}
	D_DEBUG( "...包统计结束\n");
	D_DEBUG( "//////////////////////////////////////////////////////////////\n" );
}
