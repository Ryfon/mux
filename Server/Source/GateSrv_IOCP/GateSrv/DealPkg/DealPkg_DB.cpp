﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"

 

///初始化(注册各命令字对应的处理函数);
void CDealDbSrvPkg::Init()
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//#define D_G_DBGET_PRE                    0x71c5     //DB取信息开始;
	Register( D_G_DBGET_PRE, &OnDbGetPre );
	//#define D_G_DBGET_POST                   0x71c6     //DB取信息结束(与M_G_DBSAVE_POST一一对应);
	Register( D_G_DBGET_POST, &OnDbGetPost );
	//#define D_G_DBGET_INFO                   0x71c8     //DB保存信息;
	Register( D_G_DBGET_INFO, &OnDbGetInfo );
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Register( D_G_PLAYER_INFO_LOGIN, &OnPlayerInfoLogin );//登录后从DB取到的玩家信息；
	Register( D_G_NEW_PLAYR_INFO_RETURN, &OnPlayerCreateRole );//玩家创建角色
	Register( D_G_DELETE_PLAYER_INFO_RETURN, &OnPlayerDeleteRole);//玩家删除角色
	Register( D_G_PLAYER_INFO, &OnPlayerInfo );//玩家进入世界时，从DB取到的玩家角色详细信息；
	//Register( D_G_PLAYER_PKG, &OnPlayerPkg );//玩家进入世界时，从DB取到的玩家角色详细信息；
	Register( D_G_BATCH_ITEM_SEQUENCE, &OnGetBatchItemSeq );//获取批量物品流水号
	Register( D_G_UPDATE_ITEM_INFO, &OnUpdateItemInfo);// 更新道具信息
	Register( D_G_GET_CLIENT_DATA, &OnGetClientData);// 更新客户信息
	Register( D_G_QUERY_PLAYER_EXIST_RST, &OnQueryPlayerExistRst );

	Register( D_G_CITY_INFO_RESULT, &OnCityInfoResult );
	Register( D_G_HEARTBEAT_CHECK_REQ, &OnHeartbeatCheck);//心跳检查
	Register( D_G_EXCEPTION_NOTIFY, &OnExceptionNotify );//异常通知
	Register( D_G_QUREY_OFFLINE_PLAYER_INFO_RST, &OnQueryOffLinePlayerInfoRst);//非在线玩家的信息
}



/////dbsrv发来的玩家包裹信息
//bool CDealDbSrvPkg::OnPlayerPkg( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	ACE_UNUSED_ARG(pOwner);
//
//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(DGPlayerPkg) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	//结构校验通过
//	const DGPlayerPkg* playerInfoRst = (const DGPlayerPkg*)pPkg;
//
//	if ( SELF_SRV_ID != playerInfoRst->playerID.wGID )
//	{
//		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的包裹信息\n" );
//		return false;
//	}
//
//	CPlayer* pPlayer = CManPlayer::FindPlayer( playerInfoRst->playerID.dwPID );
//	if ( NULL == pPlayer )
//	{
//		D_WARNING( "收到dbsrv发来玩家包裹信息时，在本GateSrv中找不到对应玩家\n" );
//		return false;
//	}
//
//	//pPlayer->SetPlayerPkgInfo( playerInfoRst->playerPkg );//save the pkg info;
//
//	GCPlayerPkg gcPlayerPkg;//通知玩家自身包裹信息；
//	gcPlayerPkg.playerID = pPlayer->GetPlayerID();
//	//gcPlayerPkg.playerPkg = playerInfoRst->playerPkg;
//	MsgToPut* cliPlayerPkg = CreatePlayerPkg( GCPlayerPkg, pPlayer, gcPlayerPkg );
//	pPlayer->SendPkgToPlayer( cliPlayerPkg );
//
//	if ( PS_WORLDENTERING != pPlayer->GetCurStat() )
//	{
//		//如果还在等待IsTimeBiasAquired，则PKGINFO也等时间对准后与APPEAR消息一起发;
//		return true;
//	}
//
//	CMapSrv* pMapsrv = pPlayer->GetMapSrv();//找到mapserver，将相应包转发出去；
//	if ( NULL == pMapsrv )
//	{
//		//如果没有对应的mapsrv则找mapsrv0;
//		GCSrvError srvError;
//		srvError.errNO = 1001;
//		MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pPlayer, srvError );
//		pPlayer->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的mapsrv;
//		D_WARNING( "CDealDbSrvPkg::OnPlayerPkg A，当前没有管理玩家地图%d的mapsrv\n", pPlayer->GetMapID() );
//		return false;
//	}
//
//	GMPlayerPkg playerPkg;
//	playerPkg.playerID = pPlayer->GetPlayerID();
//	//playerPkg.playerPkg = playerInfoRst->playerPkg;
//	MsgToPut* pNewMsg = CreateSrvPkg( GMPlayerPkg, pMapsrv, playerPkg );
//	pMapsrv->SendPkgToSrv( pNewMsg );
//
//	return true;
//	TRY_END;
//	return false;
//}

////DB取信息开始;	Register( D_G_DBGET_PRE, &OnDbGetPre );
bool CDealDbSrvPkg::OnDbGetPre( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnDbGetPre,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGDbGetPre) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGDbGetPre* dbGetPre = (const DGDbGetPre*)pPkg;

	if ( SELF_SRV_ID != dbGetPre->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的角色存盘信息开始\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( dbGetPre->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家角色存盘信息开始时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	pPlayer->OnDbGetPre( dbGetPre );

	return true;
	TRY_END;
	return false;
}

////DB取信息结束(与M_G_DBSAVE_POST一一对应);	Register( D_G_DBGET_POST, &OnDbGetPost );
bool CDealDbSrvPkg::OnDbGetPost( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnDbGetPost,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGDbGetPost) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGDbGetPost* dbGetPost = (const DGDbGetPost*)pPkg;

	if ( SELF_SRV_ID != dbGetPost->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的角色存盘信息结束\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( dbGetPost->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家角色存盘信息结束时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	pPlayer->OnDbGetPost( dbGetPost );

	return true;
	TRY_END;
	return false;
}

////DB保存信息; 	Register( D_G_DBGET_INFO, &OnDbGetInfo );
bool CDealDbSrvPkg::OnDbGetInfo( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnDbGetInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGDbGetInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGDbGetInfo* dbGetInfo = (const DGDbGetInfo*)pPkg;

	if ( SELF_SRV_ID != dbGetInfo->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的角色存盘信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( dbGetInfo->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家角色存盘信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	pPlayer->OnDbGetInfo( dbGetInfo );

	return true;
	TRY_END;
	return false;
}

///dbsrv发来的玩家角色详细信息;
bool CDealDbSrvPkg::OnPlayerInfo( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{

	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGPlayerInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGPlayerInfo* playerInfoRst = (const DGPlayerInfo*)pPkg;
	unsigned int sequenceID = playerInfoRst->sequenceID;
	ACE_UNUSED_ARG( sequenceID );

	if ( SELF_SRV_ID != playerInfoRst->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的角色详细信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( playerInfoRst->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家角色详细信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	if ( playerInfoRst->byFlag == playerInfoRst->FLAG_INVALID )
	{
		//进入世界时取玩家数据失败;
		D_WARNING( "玩家%s进入世界时取玩家角色详细信息失败，断开连接\n", pPlayer->GetAccount() );
		pPlayer->ReqDestorySelf();//断开连接；
		return false;
	}

	FullPlayerInfo &fullPlayerInfo = pPlayer->GetFullPlayerInfo();
	if(!playerInfoRst->bEnd)
	{
		pPlayer->SetFullInfoFalse();
		ReceiveFullStr<DGPlayerInfo, FullPlayerInfo>(playerInfoRst, &fullPlayerInfo);
		return true;
	} else {		
		ReceiveFullStr<DGPlayerInfo, FullPlayerInfo>(playerInfoRst, &fullPlayerInfo);
		pPlayer->SetFullInfoTrue();
	}

	if ( pPlayer->IsInFighting() )
	{
		D_ERROR( "战斗状态下收到玩家DB信息\n" );
		return false;
	}

	//取玩家数据成功；
	if ( !pPlayer->SetCurStat( PS_DETAIL_INFO_GOT ) )
	{
		//登录失败;
		D_WARNING( "置玩家%s进入PS_DETAIL_INFO_GOT状态失败，断开连接\n", pPlayer->GetAccount() );
		pPlayer->ReqDestorySelf();//断开连接；
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	//..............................
	//顺序关系:mapid-->mapsrv-->mapsrvid
	//然后在每次玩家的appear消息中会重新调用SetPlayerInfo设置玩家所在的新地图；
	pPlayer->SetPlayerInfo();//内部会设置玩家所在的地图号;
	//pPlayer->SetMapSrv( CDealMapSrvPkg::GetMapSrvByMapcode( pPlayer->GetMapID() ) );//玩家进世界时设置其对应的mapsrv指针;
	CMapSrv* pPlayerMapSrv = CDealMapSrvPkg::GetMapSrvByMapcode(pPlayer->GetMapID());
	if ( NULL != pPlayerMapSrv )
	{
		D_DEBUG( "DB发来%s的PlayerInfo, MapSrvID:%d, player's MapID:%d\n", pPlayer->GetAccount(), pPlayerMapSrv->GetID(), pPlayer->GetMapID() );//副本调试日志
		pPlayer->SetMapSrvID( pPlayerMapSrv->GetID() );
	} 
	else 
	{
		GCSrvError srvError;
		srvError.errNO = 1001;

		NewSendPlayerPkg( GCSrvError, pPlayer, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pPlayer, srvError );
		//pPlayer->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的mapsrv;
		D_WARNING( "CDealDbSrvPkg::OnPlayerInfo A，当前没有管理玩家地图%d的mapsrv\n", pPlayer->GetMapID() );
		return false;
	}
	//..............................
	////////////////////////////////////////////////////////////////////////////////////////////////

	/*
	struct log_track_player_login
	{
	static const USHORT	wCmd = G_O_logtrack_player_login;

	UINT       	player_accont;                         	//玩家帐号
	UINT       	player_id;                             	//玩家id
	USHORT     	player_name_Len;                       	//玩家名字的大小
	CHAR       	player_name[MAX_RNAME_SIZE];             	//
	USHORT     	online_timelong;                       	//在线时间长度
	USHORT     	happen_time_Len;                       	//发生时间
	CHAR       	happen_time[MAX_RNAME_SIZE];             	//
	USHORT     	remark_Len;                            	//备注的大小
	CHAR       	remark[MAX_RMSSAGE_SIZE];                	//
	};
	*/
	//LogServiceNS::log_track_player_login logPlayerLogin;
	//SafeStrCpy( logPlayerLogin.player_name, pPlayer->GetRoleName() );
	//logPlayerLogin.player_name_Len = (USHORT)strlen( logPlayerLogin.player_name ) + 1;
	//logPlayerLogin.remark_Len = 0;
	//logPlayerLogin.happen_time_Len = 0;

	////SendMsgToLogSrv<LogServiceNS::log_track_player_login>( logPlayerLogin );//通知日志服务器有新玩家登录；	
	//NewSendMsgToLogSrv(LogServiceNS::log_track_player_login, logPlayerLogin);

	//if ( !pPlayer->IsTimeBiasAquired() )
	//{
	//	//返回，等待时间基准校对完毕；		
	//	return true;
	//}

	//时间基准校验已过，向mapsrv发玩家出现消息；
	pPlayer->EnterMapSrv( true );

	//玩家角色上线(选定)相关通知
	pPlayer->OnPlayerRoleOnLine( 0x01 );

	return true;
	TRY_END;
	return false;
}


///dbsrv发来的玩家角色相关信息;
bool CDealDbSrvPkg::OnPlayerInfoLogin( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerInfoLogin,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGPlayerInfoLogin) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGPlayerInfoLogin* playerInfoRst = (const DGPlayerInfoLogin*)pPkg;

	if ( SELF_SRV_ID != playerInfoRst->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的相关信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( playerInfoRst->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	} 

	if(playerInfoRst->byFlag == DGPlayerInfoLogin::FLAG_INVALID)//失败则直接返回
	{
		GCPlayerRole playerRole;
		playerRole.playerInfo.nameSize = 0;
		playerRole.playerInfo.equipInfo.equipSize = 0;
		playerRole.nErrCode = GCPlayerRole::ISERR_OK;
		//列表最后一项；
		playerRole.byFlag = GCPlayerRole::FLAG_END;

		D_WARNING( "OnPlayerInfoLogin，从DB取玩家%s数据失败\n", pPlayer->GetAccount() );

		NewSendPlayerPkg( GCPlayerRole, pPlayer, playerRole ); 
	}
	else if(playerInfoRst->byFlag == DGPlayerInfoLogin::FLAG_OK)//成功则加入
	{		
		if(playerInfoRst->playerInfo.uiID > 0)
		{
			GCPlayerRole roleInfo;
			roleInfo.playerInfo = playerInfoRst->playerInfo;
			roleInfo.fashion = playerInfoRst->fashion;
			roleInfo.tag = playerInfoRst->tag;

			pPlayer->AddPlayerInfoLogin( roleInfo );
		}
	}
	else if(playerInfoRst->byFlag == DGPlayerInfoLogin::FLAG_MSGEND)//角色列表完整
	{
		if ( ! (pPlayer->SetCurStat( PS_DB_LOGIN_QUERYED )) )
		{
			//取数据成功，但置状态失败;
			D_WARNING( "玩家%sDB取数据成功，但置状态失败\n", pPlayer->GetAccount() );
			pPlayer->ReqDestorySelf();//断开连接；
			return false;
		}

		D_DEBUG( "OnPlayerInfoLogin，从DB取玩家%s数据成功\n", pPlayer->GetAccount() );

		//将玩家角色信息列表通知客户端
		for ( int i=0; i<pPlayer->GetRoleNum(); ++i )
		{
			//通知客户端自身拥有的角色；
			GCPlayerRole roleInfo;
			bool isInfoOk = pPlayer->GetRoleInfoLogin( i, roleInfo );
			if ( !isInfoOk )
			{
				continue;
			}
			////取到了有效角色信息；
			GCPlayerRole playerRole;	
			playerRole.nErrCode = GCPlayerRole::ISERR_OK;
			playerRole.byFlag = GCPlayerRole::FLAG_ITEM;
			playerRole.playerInfo = roleInfo.playerInfo;
			playerRole.fashion = roleInfo.fashion;
			playerRole.tag = roleInfo.tag;
			SAFE_ARRSIZE(playerRole.playerInfo.szNickName, playerRole.playerInfo.nameSize);
			SAFE_ARRSIZE(playerRole.playerInfo.equipInfo.equips, playerRole.playerInfo.equipInfo.equipSize);
			SAFE_ARRSIZE(playerRole.fashion.fashionData, playerRole.fashion.fashionSize);

			NewSendPlayerPkg( GCPlayerRole, pPlayer, playerRole );
			//MsgToPut* pRoleMsg = CreatePlayerPkg( GCPlayerRole, pPlayer, playerRole );
			//pPlayer->SendPkgToPlayer( pRoleMsg );//通知玩家其自身的ＩＤ号；

		}

		GCPlayerRole playerRole;
		StructMemSet(playerRole, 0x00, sizeof(playerRole));
		playerRole.playerInfo.nameSize = 0;
		playerRole.playerInfo.equipInfo.equipSize = 0;
		playerRole.nErrCode = GCPlayerRole::ISERR_OK;
		//列表最后一项；
		playerRole.byFlag = GCPlayerRole::FLAG_END;

		NewSendPlayerPkg( GCPlayerRole, pPlayer, playerRole );
		//MsgToPut* pRoleMsg = CreatePlayerPkg( GCPlayerRole, pPlayer, playerRole );
		//pPlayer->SendPkgToPlayer( pRoleMsg );//通知玩家其自身的ＩＤ号；

	}

	return true;
	TRY_END;
	return false;
}

bool CDealDbSrvPkg::OnPlayerCreateRole( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerCreateRole,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGNewPlayerInfoReturn) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGNewPlayerInfoReturn* createRoleReturn = (const DGNewPlayerInfoReturn*)pPkg;
	if ( SELF_SRV_ID != createRoleReturn->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的相关信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( createRoleReturn->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	// 返回玩家创建结果
	GCCreateRole createRole;
	StructMemSet( createRole, 0x0,sizeof( GCCreateRole ) );
	createRole.sequenceID = createRoleReturn->sequenceID;
	createRole.byIsOK = createRoleReturn->iRet;
	StructMemCpy( createRole.equipInfo , &createRoleReturn->playerInfoLogin.equipInfo, sizeof(PlayerInfo_2_Equips) ); 
	if(createRole.byIsOK == GCCreateRole::ISSELECTOK_SUCCESS)
	{
		//将创建成功的角色加入玩家角色列表
		if( createRoleReturn->playerInfoLogin.uiID > 0 )
		{
			createRole.wRoleNo = createRoleReturn->playerInfoLogin.uiID;

			GCPlayerRole newRole;
			StructMemSet(newRole, 0x00, sizeof(newRole));
			newRole.playerInfo = createRoleReturn->playerInfoLogin;
			pPlayer->AddPlayerInfoLogin( newRole );
		}
	}

	NewSendPlayerPkg( GCCreateRole, pPlayer, createRole );
	//MsgToPut* pCreateRoleMsg = CreatePlayerPkg( GCCreateRole, pPlayer, createRole );
	//pPlayer->SendPkgToPlayer( pCreateRoleMsg );//通知玩家其自身的ＩＤ号；

	return true;
	TRY_END;
	return false;
}

bool CDealDbSrvPkg::OnPlayerDeleteRole( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerDeleteRole,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGDeletePlayerInfoReturn) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGDeletePlayerInfoReturn* deleteRoleReturn = (const DGDeletePlayerInfoReturn*)pPkg;
	if ( SELF_SRV_ID != deleteRoleReturn->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的相关信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( deleteRoleReturn->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	// 返回玩家删除结果
	GCDeleteRole deleteRole;	
	deleteRole.byIsOK = deleteRoleReturn->iRet;
	deleteRole.wRoleNo = deleteRoleReturn->uiRoleID;
	if(deleteRole.byIsOK == GCDeleteRole::OP_SUCCESS)
	{
		//将角色从玩家角色列表删除
		pPlayer->DeletePlayerInfoLogin(deleteRoleReturn->uiRoleID);
	}

	NewSendPlayerPkg( GCDeleteRole, pPlayer, deleteRole );
	//MsgToPut* pDeleteRoleMsg = CreatePlayerPkg( GCDeleteRole, pPlayer, deleteRole );
	//pPlayer->SendPkgToPlayer( pDeleteRoleMsg );//通知玩家其自身的ＩＤ号；

	return true;
	TRY_END;
	return false;
}

bool CDealDbSrvPkg::OnGetBatchItemSeq( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnGetBatchItemSeq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGBatchItemSequence) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	DGBatchItemSequence * pItemSeq = (DGBatchItemSequence *) pPkg;

	GMBatchItemSequence batchItemSeq;
	batchItemSeq.bSuccess = pItemSeq->bSuccess;
	batchItemSeq.uiStartSequence = pItemSeq->uiStartSequence;
	batchItemSeq.uiEndSequence = pItemSeq->uiEndSequence;

	// 获取指定的mapServer
	CMapSrv *pMapSrv = CManMapSrvs::GetMapserver(pItemSeq->uiServerID);
	if(NULL == pMapSrv)
	{
		D_DEBUG("没有找到指定的mapserver\n");
		return false;
	}

	//MsgToPut* pNewMsg = CreateSrvPkg( GMBatchItemSequence, pMapSrv, batchItemSeq );
	//pMapSrv->SendPkgToSrv( pNewMsg );//通知玩家其自身的ＩＤ号；
	SendMsgToSrv( GMBatchItemSequence, pMapSrv, batchItemSeq );

	return true;
	TRY_END;
	return false;
}

bool  CDealDbSrvPkg::OnUpdateItemInfo( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnUpdateItemInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGUpdateItemInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGUpdateItemInfo* pUpdateItemInfo = (const DGUpdateItemInfo*)pPkg;
	if ( SELF_SRV_ID != pUpdateItemInfo->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来的非本GateSrv管理玩家的道具更新结果信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pUpdateItemInfo->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家道具更新结果信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	CMapSrv* pMapsrv = pPlayer->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		GMUpdateItemInfo forwardItem;
		forwardItem.playerID = pUpdateItemInfo->playerID;
		forwardItem.bUpdateSuccess = pUpdateItemInfo->bUpdateSuccess;

		//MsgToPut* pNewMsg = CreateSrvPkg( GMUpdateItemInfo, pMapsrv, forwardItem );
		//pMapsrv->SendPkgToSrv( pNewMsg );
		SendMsgToSrv( GMUpdateItemInfo, pMapsrv, forwardItem );
	} 
	else 
	{
		GCSrvError srvError;
		srvError.errNO = 1001;

		NewSendPlayerPkg( GCSrvError, pPlayer, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pPlayer, srvError );
		//pPlayer->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的mapsrv;
		D_WARNING( "发送道具信息消息时，找不到玩家所在的mapsrv:%d\n\n", pPlayer->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}

bool  CDealDbSrvPkg::OnGetClientData( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnGetClientData,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(DGGetClientData) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const DGGetClientData* pDGGetClientData = (const DGGetClientData*)pPkg;
	if ( SELF_SRV_ID != pDGGetClientData->playerID.wGID )
	{
		D_WARNING( "收到dbsrv发来非本GateSrv管理玩家的相关信息\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pDGGetClientData->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到dbsrv发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	// 返回玩家创建结果
	GCGetClientData gcClientData;
	StructMemCpy( gcClientData.clientData, pDGGetClientData->clientData, sizeof(gcClientData.clientData) );
	gcClientData.dataSize = pDGGetClientData->dataSize;
	gcClientData.index = pDGGetClientData->index;
	gcClientData.isOK = pDGGetClientData->isOK;

	NewSendPlayerPkg( GCGetClientData, pPlayer, gcClientData );
	//MsgToPut* pGetClietMsg = CreatePlayerPkg( GCGetClientData, pPlayer, gcClientData );
	//pPlayer->SendPkgToPlayer( pGetClietMsg );//通知玩家其自身的ＩＤ号；

	return true;
	TRY_END;
	return false;
}


bool CDealDbSrvPkg::OnQueryPlayerExistRst(CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("OnQueryPlayerExistRst,pOwner为空\n");
		return false;
	}

	if ( wPkgLen != sizeof(DGQueryPlayerExistRst) )
	{
		return false;
	}

	TRY_BEGIN;

	const DGQueryPlayerExistRst* pQueryRst = ( DGQueryPlayerExistRst * )pPkg;

	if(E_QUERY_MAIL == pQueryRst->queryType)
	{
		GFQueryRecvMailPlayerInfo queryMailPlayerInfo;
		queryMailPlayerInfo.isExist = pQueryRst->playerExist;
		queryMailPlayerInfo.mailUID = pQueryRst->queryUID;
		queryMailPlayerInfo.recvPlayerUID = pQueryRst->playeruid;

		SendMsgToFunctionSrv( GFQueryRecvMailPlayerInfo,queryMailPlayerInfo );
	}
	else if(E_QUERY_SHOP == pQueryRst->queryType)
	{
		GSQryRoleInfo rst;
		StructMemSet(rst, 0x00, sizeof(rst));

		rst.RoleStatus = pQueryRst->playerExist ? 0 : 1;
		size_t recvPlayerNameLen = strlen(pQueryRst->recvPlayerName);
		rst.RoleNameLen = (BYTE) ( recvPlayerNameLen < sizeof(rst.RoleName) ? recvPlayerNameLen : sizeof(rst.RoleName) );
		SafeStrCpy( rst.RoleName, pQueryRst->recvPlayerName );

		SendMsgToShopSrv( GSQryRoleInfo, rst );
	}


	return true;
	TRY_END;
	return false;
}


bool CDealDbSrvPkg::OnCityInfoResult( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("OnReqCityInfoResult,pOwner为空\n");
		return false;
	}

	if ( wPkgLen != sizeof(DGCityInfoResult) )
	{
		return false;
	}

	TRY_BEGIN;

	const DGCityInfoResult* pQueryRst = ( DGCityInfoResult * )pPkg;

	GMCityInfo mapMsg;
	StructMemCpy( mapMsg.cityInfoArr, pQueryRst->cityArr, sizeof(pQueryRst->cityArr) );// * sizeof( pQueryRst->cityArr[0]) );
	mapMsg.cityNum = pQueryRst->cityNum;
	CMapSrv* pMapSrv = CManMapSrvs::GetMapserver( pQueryRst->reqMapsrvID );
	if( NULL == pMapSrv )
	{
		D_ERROR("找不到对应的Mapsrv\n");
		return false;
	}
	//MsgToPut* pNewMsg = CreateSrvPkg( GMCityInfo, pMapSrv, mapMsg );
	//pMapSrv->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GMCityInfo, pMapSrv, mapMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealDbSrvPkg::OnHeartbeatCheck( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("OnHeartbeatCheck,pOwner为空\n");
		return false;
	}

	if ( wPkgLen != sizeof(DGHeartbeatCheckReq) )
	{
		return false;
	}

	TRY_BEGIN;

	const DGHeartbeatCheckReq* pHeartbeatCheckReq = ( DGHeartbeatCheckReq * )pPkg;
	GDHeartbeatCheckRst rst;
	rst.sequenceID = pHeartbeatCheckReq->sequenceID;

	//MsgToPut* pNewMsg = CreateSrvPkg( GDHeartbeatCheckRst, pOwner, rst );
	//pOwner->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GDHeartbeatCheckRst, pOwner, rst );

	return true;
	TRY_END;
	return false;
}


bool CDealDbSrvPkg::OnExceptionNotify( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("DGExceptionNotify,pOwner为空\n");
		return false;
	}

	if ( wPkgLen != sizeof(DGExceptionNotify) )
	{
		return false;
	}

	TRY_BEGIN;

	const DGExceptionNotify* pException = ( DGExceptionNotify * )pPkg;

	stringstream ss;
	ss <<"DBsrv Id："  << pException->srvId  << "发生异常";

	GESrvGroupChat allChat;
	SafeStrCpy( allChat.chatContent, ss.str().c_str() );
	allChat.chatType = CT_WORLD_CHAT;
	allChat.extInfo = CT_SYS_EVENT_NOTI;
	SendMsgToCenterSrv( GESrvGroupChat, allChat );

	return true;
	TRY_END;
	return false;
}

bool CDealDbSrvPkg::OnQueryOffLinePlayerInfoRst( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealDbSrvPkg::OnQueryOffLinePlayerInfoRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(DGQueryOffLinePlayerInfoRst) )
	{
		D_ERROR( "CDealDbSrvPkg::OnQueryOffLinePlayerInfoRstw, PkgLen != sizeof(DGQueryOffLinePlayerInfoRst\n" );
		return false;
	}

	const DGQueryOffLinePlayerInfoRst* pRst = (const DGQueryOffLinePlayerInfoRst*)pPkg;
	if ( SELF_SRV_ID != pRst->lNotiPlayerID.wGID)
	{
		D_WARNING( "CDealDbSrvPkg::OnQueryOffLinePlayerInfoRst,收到dbsrv发来非本GateSrv管理玩家的相关信息\n" );
		return false;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if ( NULL == pNotiPlayer )
	{
		D_WARNING( "CDealDbSrvPkg::OnQueryOffLinePlayerInfoRst, 收到dbsrv发来玩家相关信息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	GCQueryOffLinePlayerInfoRst cliMsg = pRst->rst;
	NewSendPlayerPkg( GCQueryOffLinePlayerInfoRst, pNotiPlayer, cliMsg );

	return true;
	TRY_END;
	return false;
}
