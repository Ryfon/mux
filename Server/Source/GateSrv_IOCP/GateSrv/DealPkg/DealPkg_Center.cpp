﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"

 
///初始化(注册各命令字对应的处理函数);
void CDealCenterSrvPkg::Init()
{
	Register( E_G_PLAYER_ISEXIST, &OnPlayerIsExist );//centersrv发来的玩家是否在线查询结果；
	Register( E_G_INVITE_ERROR, &OnInviteError );
	Register( E_G_INVITE_SUCCESS, &OnInviteSuccess );
	Register( E_G_INVITE_PLAYER_JOIN_TEAM, &OnInvitePlayerJoinTeam );

	Register( E_G_SRVGROUP_CHAT, &OnSrvGroupChat );//centersrv发来的世界聊天消息EGSrvGroupChat；
	Register( E_G_PRIVATE_CHAT, &OnPrivateChat ); //center发往gate密聊聊天消息，gate收到后应转发给密聊对象玩家,StrEGPrivateChat

	Register( E_G_ADDFRIENDFAILED,&OnRecvAddFriendError );//增加好友出错
	Register( E_G_ADDFRIENDSUCCESS,&OnRecvAddFriendOk );//增加好友成功
	Register( E_G_REQADDFRIEND,&OnRecvAddFriendReq );
	Register( E_G_REQ_JOIN_TEAM, &OnEGReqJoinTeam );  //收到转发的申请组队消息
	Register( E_G_REQ_JOIN_ERROR, &OnEGReqJoinError );  //收到转发的申请组队错误消息
	Register( E_G_FEEDBACK_ERROR, &OnFeedbackError );   //收到反馈失败消息
	Register( E_G_CHAT_ERROR, &OnChatError );//聊天出错
	Register( E_G_ADD_CHAT_GROUP_MEMBER_BY_OWNER, &OnAddChatGroupMemberByOwner );
	Register( E_G_IRC_QUERY_PLAYERINFO, &OnIrcQueryPlayerInfo );
	Register( E_G_IRC_QUERY_PLAYERINFO_RST, &OnIrcQueryPlayerInfoRst );
	Register( E_G_CHECK_PUNISHMENT_RESULT, &OnCheckPunishmentResult );
	Register( E_G_GMCMD_RESULT, &OnGmCmdResult );
	Register( E_G_GMCMD_REQUEST, &OnGmCmdRequest );
	//Register( E_G_RACE_GROUP_NOTIFY, &OnRaceGroupNotify );
	Register( E_G_NOTICE_PLAYER_HAVE_UNREAD_MAIL, &OnRecvNoticePlayerHaveUnReadMail );
	Register( E_G_NOTIFY_ACTIVITY_STATE, &OnNotifyActivityState );
	Register( E_G_GMQUERY_TARGET_CHECK_RST, &OnGMQueryTargetCheckRst);

	Register( E_G_NEW_COPY_CMD,   &OnNewCopyCmd );//通知创建新副本；
	Register( E_G_DEL_COPY_CMD,   &OnDelCopyCmd );//通知删除旧副本；
	Register( E_G_DEL_COPY_QUEST, &OnDelCopyQuest );//centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
	Register( E_G_COPY_RES_RST,   &OnCopyResRst );//返回给gate的副本相关操作结果
	Register( E_G_ENTER_COPY_CMD, &OnEnterCopyCmd );//指令玩家进入副本；
	Register( E_G_LEAVE_COPY_CMD, &OnLeaveCopyCmd );//指令玩家离开副本
	Register( E_G_LEAVE_STEAM_NOTI, &OnLeaveSTeamNoti );//通知玩家离开副本组队(删去玩家在各副本中的第一次进入信息，并从当前副本中踢出去)

	Register( E_G_GLOBE_COPYINFOS, &OnGlobeCopyInfos );//收到center发来的全局副本信息查询结果；
	Register( E_G_TEAM_COPYINFOS, &OnTeamCopyInfos );//收到center发来的组队副本信息查询结果；

	Register( E_G_ENTER_PWMAP_CHECK_RST, &OnEnterPWMapCheckRst );//
	Register( E_G_MODIFY_PW_STATE_RST, &OnModifyPWStateRst );//
	Register( E_G_MODIFY_PW_STATE_NTF, &OnModifyPWstateNtf );//
	Register( E_G_MODIFY_PWMAP_STATE_RST, &OnModifyPWMapStateRst );//
	Register( E_G_MODIFY_PWMAP_STATE_NTF, &OnModifyPWMapStateNtf );//
	Register( E_G_PWMAP_PLAYER_NUM_NTF, &OnPWMapPlayerNumNtf );//

}	

///centersrv返回的玩家在线唯一性验证结果；
bool CDealCenterSrvPkg::OnPlayerIsExist( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerIsExist,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(EGPlayerIsExist) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const EGPlayerIsExist* playerIsExist = (const EGPlayerIsExist*)pPkg;

	//D_DEBUG( "收到玩家%s的唯一性验证结果\n", playerIsExist->szAccount );

	if ( SELF_SRV_ID != playerIsExist->playerID.wGID )
	{
		D_WARNING( "收到centersrv发来非本GateSrv管理玩家的在线唯一性验证结果\n" );
		return false;
	}

	bool ischeckok = ( EGPlayerIsExist::UNIQUE_CHECK_OK == playerIsExist->nErrCode );

	CPlayer* pPlayer = CManPlayer::FindPlayer( playerIsExist->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到centersrv验证结果时，找不到玩家，可能玩家在验证过程中已下线\n" );
		//可能情况是，玩家向centersrv发验证请求后未收到验证结果前就下线了，
		//如果centersrv的验证结果为成功，则此时玩家已被加入在线列表，因此应通知centersrv该玩家已下线并从在线列表中删去；
		//如果验证结果不成功，则centersrv并没有把该玩家加入在线列表，因此也就无需通知centersrv该玩家下线了；
		//同样的情形也发生在与mapsrv通信时(见收到mapsrv玩家appear消息时的处理）
		if ( ischeckok )
		{
			GEPlayerOffline playerOffline;
			playerOffline.playerID = playerIsExist->playerID;
			SafeStrCpy( playerOffline.szAccount, playerIsExist->szAccount );
			//MsgToPut* pNewMsg = CreateSrvPkg( GEPlayerOffline, pOwner, playerOffline );
			//pOwner->SendPkgToSrv( pNewMsg );
			SendMsgToSrv( GEPlayerOffline, pOwner, playerOffline );
			D_DEBUG( "4、通知centersrv,%s下线\n", playerOffline.szAccount );
		}		
		return false;
	}

	if ( ischeckok )
	{
		if ( ! (pPlayer->SetCurStat( PS_CENTER_CHECKED )) )
		{
			//登录成功，但置状态失败;
			D_WARNING( "玩家%sCenter验证成功后置状态失败\n", pPlayer->GetAccount() );
			pPlayer->ReqDestorySelf();//断开连接；
			return false;
		}

		D_DEBUG( "玩家%s CenterSrv验证成功\n", pPlayer->GetAccount() );
		CLogManager::DoAccountLogin(pPlayer, 0);//记录账号上线

		//通知loginserver(防沉迷计时开始)
		CNewLoginSrv* pNewLoginSrv = CManNewLoginSrv::GetNewLoginSrv();
		if ( NULL == pNewLoginSrv )
		{
			//找不到登录服务器；
			GCSrvError srvError;
			srvError.errNO = 1001;

			NewSendPlayerPkg( GCSrvError, pPlayer, srvError );
			//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pPlayer, srvError );
			//pPlayer->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的mapsrv;
			D_WARNING( "3收到玩家新登录消息时，找不到新登录服务器\n\n" );
			return false;
		}

		GL_ReportUserEnterGame enterGameNotify;
		enterGameNotify.gateId = pPlayer->GetPlayerID().wGID;
		enterGameNotify.playerId = pPlayer->GetPlayerID().dwPID;
		SafeStrCpy( enterGameNotify.userId.userId, pPlayer->GetAccount() );
		enterGameNotify.userId.userIdLen = (unsigned short) (strlen( enterGameNotify.userId.userId ) + 1);
		//MsgToPut* pNotifyMsg = CreateNewLoginSrvPkg( GL_ReportUserEnterGame, pNewLoginSrv, enterGameNotify );
		//pNewLoginSrv->SendPkgToSrv( pNotifyMsg );
		SendMsgToNewLoginSrv( GL_ReportUserEnterGame, enterGameNotify );

		//通知客户端登录成功；
		GCPlayerLogin playerLogin;
		playerLogin.byIsLoginOK = GCPlayerLogin::ISLOGINOK_SUCCESS;
		playerLogin.sequenceID = playerIsExist->sequenceID;

		NewSendPlayerPkg( GCPlayerLogin, pPlayer, playerLogin );
		//MsgToPut* pLoginSuc = CreatePlayerPkg( GCPlayerLogin, pPlayer, playerLogin );
		//pPlayer->SendPkgToPlayer( pLoginSuc );//通知玩家登录成功；

		//通知客户端其自身的ID号;
		GCPlayerSelfInfo playerSelfInfo;
		playerSelfInfo.lPlayerID = pPlayer->GetPlayerID();

		NewSendPlayerPkg( GCPlayerSelfInfo, pPlayer, playerSelfInfo );
		//MsgToPut* pPlayerSelfInfo = CreatePlayerPkg( GCPlayerSelfInfo, pPlayer, playerSelfInfo );
		//pPlayer->SendPkgToPlayer( pPlayerSelfInfo );//通知玩家其自身的ＩＤ号；

		//先清玩家之前保存的相关信息；
		pPlayer->ClearPlayerInfo();

		//向DBSrv请求玩家角色等信息；
		CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pPlayer->GetAccount() );
		if ( NULL == pDbSrv )
		{
			GCSrvError srvError;
			srvError.errNO = 1001;

			NewSendPlayerPkg( GCSrvError, pPlayer, srvError );
			D_WARNING( "玩家登录唯一性验证成功后，找不到玩家所在的dbsrvd，取角色信息失败\n\n" );
			return false;
		}
		if ( !pPlayer->SetCurStat( PS_DB_LOGIN_QUERYING ) )
		{
			D_ERROR( "置玩家进入dbloginquery状态失败\n" );
			return false;
		}
		GDPlayerInfoLogin reqPlayerInfo;
		reqPlayerInfo.playerID = pPlayer->GetPlayerID();
		SafeStrCpy( reqPlayerInfo.szAccount, pPlayer->GetAccount() );
		//MsgToPut* pReqPlayerInfoMsg = CreateSrvPkg( GDPlayerInfoLogin, pDbSrv, reqPlayerInfo );
		//pDbSrv->SendPkgToSrv( pReqPlayerInfoMsg );
		SendMsgToSrv( GDPlayerInfoLogin, pDbSrv, reqPlayerInfo );
	} else {
		//登录失败，通知玩家有另外的玩家在线；
		D_DEBUG( "玩家%s CenterSrv验证失败\n", pPlayer->GetAccount() );
		GCPlayerLogin playerLogin;
		playerLogin.byIsLoginOK = GCPlayerLogin::ISLOGINOK_FAILED_ONLINE;

		NewSendPlayerPkg( GCPlayerLogin, pPlayer, playerLogin );
		pPlayer->SetCurStat( PS_CONN );//重置玩家状态；
		pPlayer->ReqDestorySelf();//登录失败断开自身，防止恶意继续占用连接；
		return false;
	}

	return true;
	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnInvitePlayerJoinTeam( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnInvitePlayerJoinTeam,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof( EGInvitePlayerJoinTeam) )
	{
		D_ERROR("EGInvitePlayerJoinTeam 包长错误\n");
		return false;
	}

	const EGInvitePlayerJoinTeam* pSrvMsg = (EGInvitePlayerJoinTeam*)pPkg;
	CPlayer* pBeInvitedPlayer = CManPlayer::FindPlayer( pSrvMsg->beInvitedPlayerID.dwPID );

	GEInviteError errMsg;
	bool isError = false;
	if( NULL == pBeInvitedPlayer)
	{		
		isError = true;
		errMsg.nErrorCode = PLAYER_NOT_FIND;
	} else if ( pBeInvitedPlayer->IsInTeam() )
	{
		isError = true;
		errMsg.nErrorCode = PLAYER_HAS_TEAM;
	} else if ( !(pBeInvitedPlayer->GetTeamSwitch()) ) {
		isError = true;
		errMsg.nErrorCode = PLAYER_REFUSE;
	}else if( (pBeInvitedPlayer->GetCurStat() != PS_MAPFIGHTING))
	{
		isError = true;
		errMsg.nErrorCode = PLAYER_BUSY;
	}else if( pBeInvitedPlayer->GetRace() != pSrvMsg->inviterInfo.invitePlayerRace )
	{
		isError = true;
		errMsg.nErrorCode = PLAYER_NOT_SAME_RACE;
	}else if( pBeInvitedPlayer->IsEvil() != pSrvMsg->inviterInfo.isEvil )
	{
		isError = true;
		errMsg.nErrorCode = PLAYER_IS_EVIL;
	}else if( !pBeInvitedPlayer->AddTeamInvitePlayer( pSrvMsg->inviterInfo.invitePlayerID ) )
	{
		isError = true;
		errMsg.nErrorCode = PLAYER_BUSY;
	}

	if ( isError )
	{
		SafeStrCpy( errMsg.beInvitedPlayerName, pSrvMsg->beInvitedPlayerName );
		errMsg.beInvitedPlayerNameLen = (UINT)strlen( errMsg.beInvitedPlayerName ) + 1;
		errMsg.invitePlayerID = pSrvMsg->inviterInfo.invitePlayerID;
		//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		//if ( NULL == pCenterSrv )
		//{
		//	D_ERROR("找不到centresrv\n");
		//	return false;
		//}
		//MsgToPut* pNewMsg = CreateSrvPkg( GEInviteError, pCenterSrv, errMsg );
		//pCenterSrv->SendPkgToSrv( pNewMsg );
		SendMsgToCenterSrv( GEInviteError, errMsg );
		return false;
	}


	GCRecvInviteJoinTeam climsg;
	climsg.invitePlayerID = pSrvMsg->inviterInfo.invitePlayerID;  //发给的是被邀请的人，不要搞混了
	SafeStrCpy( climsg.invitePlayerName, pSrvMsg->inviterInfo.invitePlayerName );  //发起者的姓名
	SAFE_ARRSIZE( climsg.invitePlayerName, climsg.nameSize );
	climsg.extInfo = 0;//无额外信息；

	if ( pBeInvitedPlayer->IsCurInCopy() )
	{
		//若玩家当前在副本中，则提醒玩家有可能会消副本；
		climsg.extInfo = 1;//玩家当前在副本中；
	}

	NewSendPlayerPkg( GCRecvInviteJoinTeam, pBeInvitedPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRecvInviteJoinTeam, pBeInvitedPlayer, climsg);
	//pBeInvitedPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnInviteError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnInviteError,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(EGInviteError) )
	{
		return false;
	}

	EGInviteError* pSrvMsg = (EGInviteError*)pPkg;
	if( pSrvMsg->invitePlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("invite的GID与本GATESRV不符\n");
		return false;
	}

	CPlayer* pInvitePlayer = CManPlayer::FindPlayer( pSrvMsg->invitePlayerID.dwPID );
	if( NULL == pInvitePlayer )
	{
		D_ERROR("找不到邀请玩家\n");
		return false;
	}

	GCInvitePlayerJoinTeam errMsg;
	SafeStrCpy( errMsg.playerName, pSrvMsg->beInvitedPlayerName );
	errMsg.nErrorCode = pSrvMsg->nErrorCode;
	errMsg.nameSize = pSrvMsg->beInvitedPlayerNameLen;

	NewSendPlayerPkg( GCInvitePlayerJoinTeam, pInvitePlayer, errMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCInvitePlayerJoinTeam, pInvitePlayer , errMsg );
	//pInvitePlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnInviteSuccess( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnInviteSuccess,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(EGInviteSuccess) )
	{
		return false;
	}

	EGInviteSuccess* pSrvMsg = (EGInviteSuccess*)pPkg;

	if( pSrvMsg->invitePlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR( "CDealCenterSrvPkg::OnInviteSuccess 邀请玩家的GID不对\n" );
		return false;
	}

	CPlayer* pInvitePlayer = CManPlayer::FindPlayer( pSrvMsg->invitePlayerID.dwPID );
	if( NULL == pInvitePlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnInviteSuccess 邀请玩家找不到\n");
		GEFeedbackError errMsg;
		errMsg.nErrorCode = PLAYER_NOT_FIND;	//邀请的人不存在了
		errMsg.lNotiPlayerID.dwPID = pSrvMsg->beInvitedPlayer.playerId;
		errMsg.lNotiPlayerID.wGID = pSrvMsg->beInvitedPlayer.gateId;
		SendMsgToCenterSrv( GEFeedbackError, errMsg );
		return false;
	}

	if( pInvitePlayer->IsInTeam() && pInvitePlayer->IsTeamCaptain() )  //如果邀请的人有组,则向relationsrv发送Add消息，还必须验证是否是队长
	{
		AddTeamMemberRequest relationMsg;
		relationMsg.teamInd = pInvitePlayer->GetTeamId();
		relationMsg.requestSeq = 0;
		relationMsg.member = pSrvMsg->beInvitedPlayer;
		relationMsg.palyerId = pInvitePlayer->GetPlayerDwPID();
		relationMsg.gateId = pInvitePlayer->GetPlayerID().wGID;
		relationMsg.flag = 0;

		CRelationSrv* pRelationSrv = CManRelationSrvs::GetRelationserver();
		if( NULL == pRelationSrv )
		{
			D_ERROR("CDealCenterSrvPkg::OnInviteSuccess 找不到组队1\n");
			return false;
		}
		//MsgToPut* pNewMsg = CreateRelationPkg( AddTeamMemberRequest, pRelationSrv, relationMsg );
		//pRelationSrv->SendPkgToSrv( pNewMsg );
		SendMsgToRelationSrv( AddTeamMemberRequest, relationMsg );
		D_DEBUG("CDealCenterSrvPkg::OnInviteSuccess 发送增加组员请求\n");
	}else if( !pInvitePlayer->IsInTeam() )
	{						//没有组，则要发送创建组队的消息
		CreateTeamRequest createMsg;
		createMsg.team.teamMembers.membersSize = 2;	  //创建组的时候肯定是2个人
		createMsg.team.teamMembers.members[0] = pInvitePlayer->GetMemberRelationSrvInfo();
		createMsg.team.teamMembers.members[1] = pSrvMsg->beInvitedPlayer;
		createMsg.team.captainGateId = pInvitePlayer->GetPlayerID().wGID;
		createMsg.team.captainPlayerId = pInvitePlayer->GetPlayerID().dwPID;
		createMsg.team.expSharedMode  = E_SHARE_MODE;
		createMsg.team.goodSharedMode = E_FREE_MODE;
		createMsg.team.rollItemLevel  = E_ORANGE_LEVEL;
		StructMemSet( createMsg.team.teamInd,  0, sizeof( createMsg.team.teamInd ) );   //创建的时候这项不需要
		//发送
		CRelationSrv* pRelationSrv = CManRelationSrvs::GetRelationserver();
		if( NULL == pRelationSrv )
		{
			D_ERROR("CDealCenterSrvPkg::OnInviteSuccess 找不到组队2\n");
			return false;
		}
		//MsgToPut* pNewMsg = CreateRelationPkg( CreateTeamRequest, pRelationSrv, createMsg );
		//pRelationSrv->SendPkgToSrv( pNewMsg );
		SendMsgToRelationSrv( CreateTeamRequest, createMsg );
		D_DEBUG("CDealCenterSrvPkg::OnInviteSuccess 发送创建队伍请求\n");
	}else  //有组队但是不是队长，也需要反馈错误
	{
		D_ERROR("CDealCenterSrvPkg::OnInviteSuccess 玩家已经不是队长\n");
		GEFeedbackError errMsg;
		errMsg.nErrorCode = PLAYER_NOT_CAPTAIN;	//邀请的人不存在了
		errMsg.lNotiPlayerID.dwPID = pSrvMsg->beInvitedPlayer.playerId;
		errMsg.lNotiPlayerID.wGID = pSrvMsg->beInvitedPlayer.gateId;
		SendMsgToCenterSrv( GEFeedbackError, errMsg );
		return false;
	}

	return true;
	TRY_END;
	return false;
}

//center发往gate密聊聊天消息，gate收到后应转发给密聊对象玩家,StrEGPrivateChat
bool CDealCenterSrvPkg::OnPrivateChat( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnPrivateChat,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(EGPrivateChat) )
	{
		return false;
	}
	const EGPrivateChat* pPrivateChat = (const EGPrivateChat*) pPkg;

	//找到目标玩家，校验目标玩家的昵称；
	CPlayer* pPlayer = CManPlayer::FindPlayer( pPrivateChat->tgtPlayerID.dwPID );
	if ( NULL == pPlayer ) 
	{
		D_WARNING( "centersrv发来密聊消息，密聊对象:%s，已不存在\n", pPrivateChat->tgtPlayerName );
		return false;
	}

	//如果是发往客户端的系统消息，则直接发送
	if ( CT_SYS_WARNING == pPrivateChat->chatType )
	{
		GCChat chatInfo;
		StructMemSet( chatInfo, 0, sizeof(chatInfo) );
		chatInfo.chatType = pPrivateChat->chatType;
		chatInfo.extInfo = 0;//无额外信息；
		chatInfo.sourcePlayerID.wGID  = 0;
		chatInfo.sourcePlayerID.dwPID = 0;

		chatInfo.chatSize = (UINT)strlen( pPrivateChat->chatContent ) + 1;
		SAFE_ARRSIZE( chatInfo.strChat, chatInfo.chatSize );         //说话内容长度；
		SafeStrCpy( chatInfo.strChat, pPrivateChat->chatContent );   //说话内容

		NewSendPlayerPkg( GCChat, pPlayer, chatInfo );
		return true;
	}

	//以下校验昵称；
	bool isMatch = ( 0 == strcmp( pPrivateChat->tgtPlayerName, pPlayer->GetRoleName() ) );
	if ( !isMatch )
	{
		D_WARNING( "centersrv发来密聊消息，昵称校验失败:%s VS %s\n", pPrivateChat->tgtPlayerName, pPlayer->GetRoleName() );
		return false;
	}

	GCChat chatInfo;
	StructMemSet( chatInfo, 0, sizeof(chatInfo) );
	chatInfo.chatType = pPrivateChat->chatType;
	chatInfo.extInfo = 0;//无额外信息；
	chatInfo.sourcePlayerID.wGID  = 0;//密聊消息，不传递发消息者的playerID，因为客户端可能无法根据PlayerID找到说话者的信息（例如角色名）;
	chatInfo.sourcePlayerID.dwPID = 0;

	chatInfo.srcNameSize = (UINT)strlen( pPrivateChat->chatPlayerName ) + 1;
	SAFE_ARRSIZE( chatInfo.srcName, chatInfo.srcNameSize );      //说话者昵称长度；

	SafeStrCpy( chatInfo.srcName, pPrivateChat->chatPlayerName );//说话者昵称；

	chatInfo.chatSize = (UINT)strlen( pPrivateChat->chatContent ) + 1;
	SAFE_ARRSIZE( chatInfo.strChat, chatInfo.chatSize );         //说话内容长度；

	SafeStrCpy( chatInfo.strChat, pPrivateChat->chatContent );   //说话内容

	NewSendPlayerPkg( GCChat, pPlayer, chatInfo );
	//MsgToPut* pChatMsg = CreatePlayerPkg( GCChat, pPlayer, chatInfo );
	//pPlayer->SendPkgToPlayer( pChatMsg );//密聊信息通知说话对象;

	return true;
	TRY_END;
	return false;

}

///centersrv发来的世界聊天消息EGSrvGroupChat；
bool CDealCenterSrvPkg::OnSrvGroupChat( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnSrvGroupChat,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(EGSrvGroupChat) )
	{
		return false;
	}
	const EGSrvGroupChat* pSrvMsg = (const EGSrvGroupChat*)pPkg;

	GCChat chatInfo;
	chatInfo.chatType = pSrvMsg->chatType;
	chatInfo.extInfo = pSrvMsg->extInfo;
	chatInfo.sourcePlayerID.wGID  = 0;//世界广播消息，不传递发消息者的playerID，因为客户端可能无法根据PlayerID找到说话者的信息（例如角色名）;
	chatInfo.sourcePlayerID.dwPID = 0;
	chatInfo.srcNameSize = (UINT)strlen( pSrvMsg->chatPlayerName ) + 1;
	SAFE_ARRSIZE( chatInfo.srcName, chatInfo.srcNameSize );
	SafeStrCpy( chatInfo.srcName, pSrvMsg->chatPlayerName );
	chatInfo.chatSize = (UINT)strlen( pSrvMsg->chatContent ) + 1;
	SAFE_ARRSIZE( chatInfo.strChat, chatInfo.chatSize );
	SafeStrCpy( chatInfo.strChat, pSrvMsg->chatContent );

	//若为世界消息，则填入真正的消息类型；
	if ( CT_WORLD_CHAT == chatInfo.chatType )
	{
		//检测是否为世界聊天的其它种类消息
		if ( 0 != chatInfo.extInfo )
		{
			chatInfo.chatType = (CHAT_TYPE)chatInfo.extInfo;
		}
	}

#ifdef DS_EPOLL
	GatePlayerBrocast( GCChat, chatInfo );//全gate广播消息	
#else  //DS_EPOLL
	MsgToPut* pBrocastMsg = CreateGateBrocastPkg( GCChat, chatInfo );//全gate广播消息；
	if ( NULL != g_pClientSender )
	{
		if ( g_pClientSender->PushMsg( pBrocastMsg ) < 0 ) //直接压入client通信队列，因为到了底层会解析广播包发送给所有的连入连接，而不是特定人;
		{
			D_ERROR( "0926dbg, OnSrvGroupChat, PushMsg失败\n" );
		}
	}
#endif //DS_EPOLL

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnRecvAddFriendReq(CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvAddFriendReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGReqAgreeAddFriend) )
	{
		return false;
	}

	const EGReqAgreeAddFriend* pSrvMsg = ( const EGReqAgreeAddFriend *)pPkg;
	if( pSrvMsg->targetID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR( "邀请玩家的GID不对\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->targetID.dwPID );
	if( NULL == pPlayer )
	{
		GEAgreeAddFriendResult errMsg;
		errMsg.isAddReqer = false;
		errMsg.errorCode = GCAddFriendResponse::ADD_FRIEND_FAIL;
		SafeStrCpy( errMsg.launchPlayerName, pSrvMsg->luanchPlayerName );
		SafeStrCpy( errMsg.targetPlayerName, pSrvMsg->targetPlayerName );
		errMsg.reqPlayerID = pSrvMsg->luanchID;
		errMsg.responsePlayerID = pSrvMsg->targetID;
		SendMsgToCenterSrv( GEAgreeAddFriendResult, errMsg );
		return false;
	}

	//被邀请玩家在跳地图
	if ( pPlayer->IsInSwitching() )
	{
		GEAgreeAddFriendResult errMsg;
		errMsg.isAddReqer = false;
		errMsg.errorCode = GCAddFriendResponse::ADD_FRIEND_FAIL;
		SafeStrCpy( errMsg.launchPlayerName, pSrvMsg->luanchPlayerName );
		errMsg.reqPlayerID = pSrvMsg->luanchID;
		errMsg.responsePlayerID = pPlayer->GetPlayerID();
		SafeStrCpy( errMsg.targetPlayerName, pPlayer->GetRoleName() );
		SendMsgToCenterSrv( GEAgreeAddFriendResult, errMsg );
		D_DEBUG("玩家%s 在跳地图过程中,不接受其他加好友消息\n", pPlayer->GetRoleName() );
		return false;
	}

	if( !pPlayer->AddFriendInvitePlayer( pSrvMsg->luanchID ) )
	{
		GEAgreeAddFriendResult errMsg;
		errMsg.isAddReqer = false;
		errMsg.errorCode = GCAddFriendResponse::ADD_FRIEND_FAIL;
		SafeStrCpy( errMsg.launchPlayerName, pSrvMsg->luanchPlayerName );
		errMsg.reqPlayerID = pSrvMsg->luanchID;
		errMsg.responsePlayerID = pPlayer->GetPlayerID();
		SafeStrCpy( errMsg.targetPlayerName, pPlayer->GetRoleName() );
		SendMsgToCenterSrv( GEAgreeAddFriendResult, errMsg );
		return false;
	}

	GCAddFriendReq addfriendReq;
	StructMemSet( addfriendReq, 0x0, sizeof(addfriendReq) );
	addfriendReq.playerID = pSrvMsg->luanchID;
	addfriendReq.groupID  = pSrvMsg->groupID;
	SafeStrCpy2( addfriendReq.reqPlayerName, pSrvMsg->luanchPlayerName,addfriendReq.reqPlayerNameSize );
	/*addfriendReq.reqPlayerNameSize = ACE_OS::snprintf( addfriendReq.reqPlayerName,
	sizeof(addfriendReq.reqPlayerName),
	"%s",
	pSrvMsg->luanchPlayerName
	);
	addfriendReq.reqPlayerName[MAX_NAME_SIZE-1] = '\0';*/

	D_DEBUG("向客户端转发了加好友的请求,%s发出了加好友申请\n", addfriendReq.reqPlayerName );

	NewSendPlayerPkg( GCAddFriendReq, pPlayer, addfriendReq );

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnRecvAddFriendError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvAddFriendError,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGAddFriendFailed) )
	{
		return false;
	}

	const EGAddFriendFailed* pSrvMsg = ( const EGAddFriendFailed *)pPkg;

	if( pSrvMsg->launchPlayer.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR( "邀请玩家的GID不对\n" );
		return false;
	}
	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->launchPlayer.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("邀请玩家找不到\n");
		return false;
	}

	GCAddFriendResponse addFriendResp;
	StructMemSet(addFriendResp, 0x0, sizeof(GCAddFriendResponse));

	addFriendResp.nErrcode = pSrvMsg->errCode;
	addFriendResp.isAgree = false;
	SafeStrCpy2( addFriendResp.friendInfo.nickName, pSrvMsg->targetPlayerName, addFriendResp.friendInfo.nickNameSize );
	//addFriendResp.friendInfo.nickNameSize = ACE_OS::snprintf(addFriendResp.friendInfo.nickName,
	//	sizeof(addFriendResp.friendInfo.nickName),
	//	"%s",
	//	pSrvMsg->targetPlayerName 
	//	);

	NewSendPlayerPkg( GCAddFriendResponse, pPlayer, addFriendResp );


	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnRecvAddFriendOk(CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvAddFriendOk,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGAddFriendOK))
	{
		return false;
	}

	CRelationSrv* pRelationSrv = CManRelationSrvs::GetRelationserver();
	if( NULL == pRelationSrv )
	{
		D_ERROR("找不到RelationSrv服务器\n");
		return false;
	}

	const EGAddFriendOK* pSrvMsg = ( const EGAddFriendOK *)pPkg;
	if( pSrvMsg->lNoticePlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR( "邀请玩家的GID不对\n" );
		return false;
	}
	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->lNoticePlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("邀请玩家找不到\n");
		return false;
	}

	GRAddFriend addFriendMsg;
	addFriendMsg.groupId   = pSrvMsg->groupID;
	addFriendMsg.ByAdduiID = pSrvMsg->addFriendUID; 
	addFriendMsg.uiID = pPlayer->GetFullPlayerInfo().baseInfo.uiID;
	//MsgToPut* pNewMsg = CreateRelationPkg(GRAddFriend,pRelationSrv, addFriendMsg);
	//pRelationSrv->SendPkgToSrv( pNewMsg );
	SendMsgToRelationSrv( GRAddFriend, addFriendMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnEGReqJoinTeam( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnEGReqJoinTeam,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGReqJoinTeam))
	{
		return false;
	}

	EGReqJoinTeam* pSrvMsg = (EGReqJoinTeam*)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->captainPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		GEReqJoinError errmsg;
		SafeStrCpy( errmsg.captainPlayerName, pSrvMsg->reqPlayer.memberName.memberName );
		errmsg.captainPlayerNameLen = pSrvMsg->captainNameLen;
		errmsg.nErrorCode = PLAYER_NOT_FIND;
		errmsg.reqPlayerID.dwPID = pSrvMsg->reqPlayer.playerId;
		errmsg.reqPlayerID.wGID = pSrvMsg->reqPlayer.gateId;
		SendMsgToCenterSrv( GEReqJoinError, errmsg );
		return false;
	}


	if( (!pNotifyPlayer->IsInTeam()) || (!pNotifyPlayer->IsTeamCaptain()) || (!pNotifyPlayer->IsReqSwitch()) )
	{
		GEReqJoinError errmsg;
		SafeStrCpy( errmsg.captainPlayerName, pNotifyPlayer->GetRoleName() );
		errmsg.captainPlayerNameLen = (UINT)strlen(pNotifyPlayer->GetRoleName() ) +1;
		errmsg.nErrorCode = PLAYER_REFUSE; //???暂时定这个
		errmsg.reqPlayerID.dwPID = pSrvMsg->reqPlayer.playerId;
		errmsg.reqPlayerID.wGID = pSrvMsg->reqPlayer.gateId;
		SendMsgToCenterSrv( GEReqJoinError, errmsg );
		return false;
	}

	if( pNotifyPlayer->IsEvil() != pSrvMsg->isReqPlayerEvil )
	{
		GEReqJoinError errmsg;
		SafeStrCpy( errmsg.captainPlayerName, pNotifyPlayer->GetRoleName() );
		errmsg.captainPlayerNameLen = (UINT)strlen(pNotifyPlayer->GetRoleName() ) +1;
		errmsg.nErrorCode = PLAYER_IS_EVIL; //
		errmsg.reqPlayerID.dwPID = pSrvMsg->reqPlayer.playerId;
		errmsg.reqPlayerID.wGID = pSrvMsg->reqPlayer.gateId;
		SendMsgToCenterSrv( GEReqJoinError, errmsg );
		return false;
	}

	//如果队伍中已经拥有了７个人　则返回错误
	if( pNotifyPlayer->GetTeamNum() == MAX_TEAM_MEMBERS_COUNT )
	{
		GEReqJoinError errmsg;
		SafeStrCpy( errmsg.captainPlayerName, pNotifyPlayer->GetRoleName() );
		errmsg.captainPlayerNameLen = (int)strlen( pNotifyPlayer->GetRoleName() );
		errmsg.nErrorCode = PLAYER_MAX_ERROR;
		errmsg.reqPlayerID.dwPID = pSrvMsg->reqPlayer.playerId;
		errmsg.reqPlayerID.wGID = pSrvMsg->reqPlayer.gateId;
		SendMsgToCenterSrv( GEReqJoinError, errmsg );
		return false;
	}

	PlayerID reqPlayerID;
	reqPlayerID.dwPID = pSrvMsg->reqPlayer.playerId;
	reqPlayerID.wGID = pSrvMsg->reqPlayer.gateId;
	if( !pNotifyPlayer->AddTeamReqPlayer( reqPlayerID ) )
	{
		GEReqJoinError errmsg;
		SafeStrCpy( errmsg.captainPlayerName, pNotifyPlayer->GetRoleName() );
		errmsg.nErrorCode = PLAYER_BUSY;
		errmsg.reqPlayerID.dwPID = pSrvMsg->reqPlayer.playerId;
		errmsg.reqPlayerID.wGID = pSrvMsg->reqPlayer.gateId;
		SendMsgToCenterSrv( GEReqJoinError, errmsg );
		return false;
	}

	GCRecvReqJoinTeam climsg;
	SafeStrCpy( climsg.reqPlayerName, pSrvMsg->reqPlayer.memberName.memberName );
	climsg.nameSize = (UINT)strlen(climsg.reqPlayerName) + 1;
	climsg.reqPlayerID.dwPID = pSrvMsg->reqPlayer.playerId;
	climsg.reqPlayerID.wGID = pSrvMsg->reqPlayer.gateId;
	NewSendPlayerPkg( GCRecvReqJoinTeam, pNotifyPlayer, climsg );


	return true;
	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnEGReqJoinError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnEGReqJoinError,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGReqJoinError))
	{
		return false;
	}

	EGReqJoinError* pSrvMsg = (EGReqJoinError*)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->reqPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		return false;
	}

	GCReqJoinTeamError errmsg;
	errmsg.nErrorCode = pSrvMsg->nErrorCode;
	SafeStrCpy( errmsg.teamLeadName, pSrvMsg->captainPlayerName );
	errmsg.nameSize = (UINT)strlen(errmsg.teamLeadName) + 1;

	NewSendPlayerPkg( GCReqJoinTeamError, pNotifyPlayer, errmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCReqJoinTeamError, pNotifyPlayer, errmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnFeedbackError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnFeedbackError,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGFeedbackError))
	{
		return false;
	}

	EGFeedbackError* pSrvMsg = (EGFeedbackError*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealCenterSrvPkg::OnFeedbackError 找不到通知的玩家\n");
		return false;
	}

	GCRecvInviteJoinTeamFeedBackError errMsg;
	errMsg.nErrorCode = pSrvMsg->errorCode;

	NewSendPlayerPkg( GCRecvInviteJoinTeamFeedBackError, pNotifyPlayer, errMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRecvInviteJoinTeamFeedBackError, pNotifyPlayer, errMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnChatError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnChatError,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGChatError))
	{
		return false;
	}

	EGChatError* pSrvMsg = (EGChatError*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealCenterSrvPkg::OnChatError 找不到通知的玩家\n");
		return false;
	}

	GCChatError errMsg;

	errMsg.nType = pSrvMsg->nType;
	errMsg.nErrorType = pSrvMsg->errorCode;
	errMsg.tgtNameSize = sizeof(errMsg.tgtName); 
	SafeStrCpy( errMsg.tgtName, pSrvMsg->tgtPlayerName );//, sizeof(errMsg.tgtName));

	NewSendPlayerPkg( GCChatError, pNotifyPlayer, errMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChatError, pNotifyPlayer, errMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnAddChatGroupMemberByOwner( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddChatGroupMemberByOwner,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGAddChatGroupMemberByOwner))
	{
		return false;
	}

	EGAddChatGroupMemberByOwner* pSrvMsg = (EGAddChatGroupMemberByOwner*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealCenterSrvPkg::OnChatError 找不到通知的玩家\n");
		return false;
	}

	if(!pSrvMsg->exitstTargetName)
	{
		GCAddChatGroupMemberResult result;
		StructMemSet(result, 0x00, sizeof(result));

		result.ret = (UINT)NO_EXIST;
		result.chatGroupMemberCount = 1;
		SafeStrCpy(result.chatGroupMembers[0].memberName, pSrvMsg->tgtPlayerName);
		if(sizeof(result.chatGroupMembers[0].memberName) > strlen(pSrvMsg->tgtPlayerName))
			result.chatGroupMembers[0].memberNameSize = (USHORT)(strlen(pSrvMsg->tgtPlayerName));
		else
			result.chatGroupMembers[0].memberNameSize = sizeof(result.chatGroupMembers[0].memberName);

		NewSendPlayerPkg( GCAddChatGroupMemberResult, pNotifyPlayer, result );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddChatGroupMemberResult, pNotifyPlayer, result );
		//pNotifyPlayer->SendPkgToPlayer( pNewMsg );	
	}
	else
	{
		AddChatGroupMemberRequest req;
		StructMemSet(req, 0x00, sizeof(req));

		req.gateId = pNotifyPlayer->GetPlayerID().wGID;
		req.playerId = pNotifyPlayer->GetPlayerID().dwPID;
		req.groupId = pSrvMsg->chatGroupID;
		req.memberAdded.gateId = pSrvMsg->targetPlayerID.wGID;
		req.memberAdded.playerId = pSrvMsg->targetPlayerID.dwPID;
		StructMemCpy( req.memberAdded.memberId.memberInd, pSrvMsg->tgtPlayerName, sizeof(req.memberAdded.memberId.memberInd) );
		if ( sizeof(req.memberAdded.memberId.memberInd) > strlen(pSrvMsg->tgtPlayerName) )
		{
			req.memberAdded.memberId.memberIndLen = (USHORT)(strlen(pSrvMsg->tgtPlayerName));
		} else {
			req.memberAdded.memberId.memberIndLen = sizeof(req.memberAdded.memberId.memberInd);
		}

		SendMsgToRelationSrv(AddChatGroupMemberRequest, req);
	}

	return true;
	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnIrcQueryPlayerInfo( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnIrcQueryPlayerInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGIrcQueryPlayerInfo))
	{
		return false;
	}

	EGIrcQueryPlayerInfo* pSrvMsg = (EGIrcQueryPlayerInfo*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->queryID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealCenterSrvPkg::OnIrcQueryPlayerInfo 找不到通知的玩家\n");
		SafeStruct( GEIrcQueryPlayerInfoRst, errMsg);
		errMsg.errorCode = GCIrcQueryPlayerInfoRst::NOT_FOUND_QUERY_PLAYER;
		SendMsgToCenterSrv( GEIrcQueryPlayerInfoRst, errMsg );
		return false;
	}

	SafeStruct( GEIrcQueryPlayerInfoRst, rstmsg);
	rstmsg.errorCode = GCIrcQueryPlayerInfoRst::QUERY_SUCCESS;
	rstmsg.playerClass = pNotifyPlayer->GetClass();
	rstmsg.playerLevel = pNotifyPlayer->GetLevel();
	rstmsg.playerRace = pNotifyPlayer->GetRace();
	rstmsg.playerSex = pNotifyPlayer->GetSex();
	SafeStrCpy( rstmsg.playerName, pNotifyPlayer->GetRoleName() );
	rstmsg.reqID = pSrvMsg->reqID;
	rstmsg.mapID = pNotifyPlayer->GetMapID();
	SendMsgToCenterSrv( GEIrcQueryPlayerInfoRst, rstmsg );

	return true;
	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnIrcQueryPlayerInfoRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnIrcQueryPlayerInfoRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGIrcQueryPlayerInfoRst))
	{
		return false;
	}

	EGIrcQueryPlayerInfoRst* pSrvMsg = (EGIrcQueryPlayerInfoRst*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->reqID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealCenterSrvPkg::OnIrcQueryPlayerInfoRst 找不到通知的玩家\n");
		return false;
	}

	SafeStruct( GCIrcQueryPlayerInfoRst, climsg);
	climsg.errorCode = pSrvMsg->errorCode;
	climsg.playerClass = pSrvMsg->playerClass;
	climsg.playerLevel = pSrvMsg->playerLevel;
	climsg.playerRace = pSrvMsg->playerRace;
	climsg.playerSex = pSrvMsg->playerSex;
	SafeStrCpy( climsg.playerName, pSrvMsg->playerName );
	climsg.nameSize = (int)strlen( climsg.playerName ) + 1;
	climsg.mapID = pNotifyPlayer->GetMapID();

	NewSendPlayerPkg( GCIrcQueryPlayerInfoRst, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCIrcQueryPlayerInfoRst, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;	
}


bool CDealCenterSrvPkg::OnCheckPunishmentResult( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnCheckPunishmentResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGCheckPunishmentResult))
	{
		return false;
	}

	EGCheckPunishmentResult* pSrvMsg = (EGCheckPunishmentResult*)pPkg;
	if( pSrvMsg->humanRace.playerNum == 0 && pSrvMsg->elfRace.playerNum == 0 && pSrvMsg->ekkaRace.playerNum == 0 )
	{
		D_WARNING("本次天谴没有任何天谴玩家不触发\n");
		return false;
	}

	//通知mapsrv天谴开始
	GMPunishmentStart mapMsg;
	mapMsg.humanRace = pSrvMsg->humanRace;
	mapMsg.elfRace = pSrvMsg->elfRace;
	mapMsg.ekkaRace = pSrvMsg->ekkaRace;
	CManMapSrvs::SendBroadcastMsg<GMPunishmentStart>( &mapMsg ); 


	//告之玩家天谴活动开始
	GESrvGroupChat cenMsg;
	stringstream sysChat;
	for( int i=0;i<pSrvMsg->humanRace.playerNum; i++ )
	{
		sysChat << pSrvMsg->humanRace.player[i].name <<"、"; 
	}
	for( int i=0;i<pSrvMsg->elfRace.playerNum; ++i)
	{
		sysChat << pSrvMsg->elfRace.player[i].name <<"、";
	}
	for( int i=0;i<pSrvMsg->ekkaRace.playerNum; ++i)
	{
		sysChat << pSrvMsg->ekkaRace.player[i].name <<"、";
	}
	sysChat << "作恶多端触犯天怒，他们将不再受到主神的庇护，伟大的主神将降罪于他。各位主神的勇士们抓住并且杀了他们，主神将永远庇护着你们！";
	SafeStrCpy( cenMsg.chatContent, sysChat.str().c_str() );
	cenMsg.chatType = CT_WORLD_CHAT;
	cenMsg.extInfo = CT_SYS_EVENT_NOTI;//系统事件	
	SendMsgToCenterSrv( GESrvGroupChat, cenMsg );

	return true;
	TRY_END;
	return false;	
}


bool CDealCenterSrvPkg::OnGmCmdResult( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnGmCmdResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGGmCmdResult))
	{
		return false;
	}

	EGGmCmdResult* pSrvMsg = (EGGmCmdResult*)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->requestPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnGmCmdResult 找不到通知的Player\n");
		return false;
	}


	SafeStruct( GCChat, sysMsg );
	if( pSrvMsg->resultCode )
	{
		SafeStrCpy( sysMsg.strChat, "使用GM命令成功" );
	}else{
		SafeStrCpy( sysMsg.strChat, "使用GM命令失败" );
	}
	sysMsg.chatSize = (UINT)strlen( sysMsg.strChat ) +1;
	sysMsg.chatType = CT_SYS_EVENT_NOTI;
	sysMsg.extInfo = 0;//无额外信息；

	NewSendPlayerPkg( GCChat, pNotifyPlayer, sysMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChat, pNotifyPlayer, sysMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );


	return true;
	TRY_END;
	return false;	
}


bool CDealCenterSrvPkg::OnGmCmdRequest( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnGmCmdRequest,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(EGGmCmdRequest))
	{
		return false;
	}

	EGGmCmdRequest* pSrvMsg = (EGGmCmdRequest*)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->targetPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnGmCmdRequest 找不到通知的Player\n");
		return false;
	}

	GMGmCmdRequest mapMsg;
	mapMsg.requestPlayerID = pSrvMsg->requestPlayerID;
	mapMsg.targetPlayerID = pSrvMsg->targetPlayerID;
	SafeStrCpy( mapMsg.strChat, pSrvMsg->strChat );
	SendMsgToMapSrvByPlayer( GMGmCmdRequest, pNotifyPlayer, mapMsg );

	return true;
	TRY_END;
	return false;	
}

//bool CDealCenterSrvPkg::OnRaceGroupNotify( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if ( NULL == pOwner )
//	{
//		D_ERROR( "CDealCenterSrvPkg::OnRaceGroupNotify,pOwner空\n" );
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	if ( wPkgLen!= sizeof(EGRaceGroupNotify))
//	{
//		return false;
//	}
//
//	EGRaceGroupNotify* pSrvMsg = (EGRaceGroupNotify*)pPkg; 
//	
//	GCChat cliMsg;
//	StructMemSet( cliMsg, 0, sizeof( cliMsg ) );
//	SafeStrCpy( cliMsg.strChat, pSrvMsg->chatContent );
//	cliMsg.chatSize = (UINT)strlen( cliMsg.strChat ) + 1;
//	cliMsg.nType = pSrvMsg->chatType;
//#ifdef DS_EPOLL
//	GatePlayerBrocast( GCChat, cliMsg );//全gate广播消息	
//#else  //DS_EPOLL
//	MsgToPut* pBrocastMsg = CreateGateBrocastPkg( GCChat, cliMsg );//全gate广播消息；
//	if ( NULL != g_pClientSender )
//	{
//		if ( g_pClientSender->PushMsg( pBrocastMsg ) < 0 ) //直接压入client通信队列，因为到了底层会解析广播包发送给所有的连入连接，而不是特定人;
//		{
//			D_ERROR( "0926dbg, OnRaceGroupNotify, PushMsg失败\n" );
//		}
//	}
//#endif //DS_EPOLL
//	return true;
//	TRY_END;
//	return false;	
//}

bool CDealCenterSrvPkg::OnRecvNoticePlayerHaveUnReadMail(CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnRecvNoticePlayerHaveUnReadMail,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnRecvNoticePlayerHaveUnReadMail,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGNoticePlayerHaveUnReadMail) )
	{
		return false;
	}

	EGNoticePlayerHaveUnReadMail* pSrvMsg = ( EGNoticePlayerHaveUnReadMail *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->recvPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnRecvNoticePlayerHaveUnReadMail 找不到通知的Player\n");
		return false;
	}

	GCPlayerHaveUnReadMail climsg;
	climsg.unReadMailCount = pSrvMsg->unReadMailCount;

	NewSendPlayerPkg( GCPlayerHaveUnReadMail, pNotifyPlayer, climsg );
	//MsgToPut* pMsgToPut = CreatePlayerPkg( GCPlayerHaveUnReadMail,pNotifyPlayer,climsg  );
	//pNotifyPlayer->SendPkgToPlayer( pMsgToPut );

	return true;

	TRY_END;
	return false;
}


bool CDealCenterSrvPkg::OnNotifyActivityState( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnNotifyActivityState,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnNotifyActivityState,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGNotifyActivityState) )
	{
		return false;
	}

	EGNotifyActivityState* pSrvMsg = ( EGNotifyActivityState *)pPkg;

	GCNotifyActivityState cliMsg;
	cliMsg.activityType = (GCNotifyActivityState::ActivityType)pSrvMsg->activityType;
	cliMsg.activityState = (GCNotifyActivityState::ActivityState)pSrvMsg->activityState;

#ifdef DS_EPOLL
	GatePlayerBrocast( GCNotifyActivityState, cliMsg );//全gate广播消息	
#else  //DS_EPOLL
	MsgToPut* pBrocastMsg = CreateGateBrocastPkg( GCNotifyActivityState, cliMsg );//全gate广播消息；
	if ( NULL != g_pClientSender )
	{
		if ( g_pClientSender->PushMsg( pBrocastMsg ) < 0 ) //直接压入client通信队列，因为到了底层会解析广播包发送给所有的连入连接，而不是特定人;
		{
			D_ERROR( "0926dbg, OnSrvGroupChat, PushMsg失败\n" );
		}
	}
#endif //DS_EPOLL


	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnGMQueryTargetCheckRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnGMQueryTargetCheckRst,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnGMQueryTargetCheckRst,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGGMQueryTargetCheckRst) )
	{
		return false;
	}

	EGGMQueryTargetCheckRst* pSrvMsg = ( EGGMQueryTargetCheckRst *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->gmPlayerId.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnGMQueryTargetCheckRst 找不到通知的Player\n");
		return false;
	}

	NewSendPlayerPkg( GTCmd_Query_PlayerID_Rsp, pNotifyPlayer, pSrvMsg->resp );

	return true;

	TRY_END;
	return false;
}

///Register( E_G_NEW_COPY_CMD, &OnNewCopyCmd );//通知创建新副本；
bool CDealCenterSrvPkg::OnNewCopyCmd( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnNewCopyCmd,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnNewCopyCmd,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGNewCopyCmd) )
	{
		return false;
	}
	EGNewCopyCmd* pSrvMsg = (EGNewCopyCmd*) pPkg;

	GMNewCopyCmd gmMsg;
	gmMsg.copyInfo = pSrvMsg->copyInfo;

	CMapSrv* pMapSrv = CManMapSrvs::GetMapserver( gmMsg.copyInfo.mapsrvID );
	if ( NULL == pMapSrv )
	{
		D_WARNING( "OnNewCopyCmd，找不到指定的mapsrv%d\n", gmMsg.copyInfo.mapsrvID );
		return false;
	}
	//MsgToPut* pNewMsg = CreateSrvPkg( GMNewCopyCmd, pMapSrv, gmMsg );
	//pMapSrv->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GMNewCopyCmd, pMapSrv, gmMsg );

	return true;
	TRY_END;
	return false;
}

///Register( E_G_DEL_COPY_CMD, &OnDelCopyCmd );//通知删除旧副本；
bool CDealCenterSrvPkg::OnDelCopyCmd( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnDelCopyCmd,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnDelCopyCmd,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGDelCopyCmd) )
	{
		return false;
	}
	EGDelCopyCmd* pSrvMsg = (EGDelCopyCmd*) pPkg;

	GMDelCopyCmd gmMsg;
	gmMsg.copyInfo = pSrvMsg->copyInfo;

	CMapSrv* pMapSrv = CManMapSrvs::GetMapserver( gmMsg.copyInfo.mapsrvID );
	if ( NULL == pMapSrv )
	{
		D_WARNING( "OnDelCopyCmd，找不到指定的mapsrv%d\n", gmMsg.copyInfo.mapsrvID );
		return false;
	}
	//MsgToPut* pNewMsg = CreateSrvPkg( GMDelCopyCmd, pMapSrv, gmMsg );
	//pMapSrv->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GMDelCopyCmd, pMapSrv, gmMsg );

	return true;
	TRY_END;
	return false;
}

///Register( E_G_DEL_COPY_QUEST, &OnDelCopyQuest );//centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
bool CDealCenterSrvPkg::OnDelCopyQuest( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnDelCopyQuest,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnDelCopyQuest,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGDelCopyQuest) )
	{
		return false;
	}
	EGDelCopyQuest* pSrvMsg = (EGDelCopyQuest*) pPkg;

	GMDelCopyQuest gmMsg;
	gmMsg.copyInfo = pSrvMsg->copyInfo;

	CMapSrv* pMapSrv = CManMapSrvs::GetMapserver( gmMsg.copyInfo.mapsrvID );
	if ( NULL == pMapSrv )
	{
		D_WARNING( "OnDelCopyQuest，找不到指定的mapsrv%d\n", gmMsg.copyInfo.mapsrvID );
		return false;
	}
	//MsgToPut* pNewMsg = CreateSrvPkg( GMDelCopyQuest, pMapSrv, gmMsg );
	//pMapSrv->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GMDelCopyQuest, pMapSrv, gmMsg );

	return true;
	TRY_END;
	return false;
}

///Register( E_G_COPY_RES_RST, &OnCopyResRst );//返回给gate的副本相关操作结果
bool CDealCenterSrvPkg::OnCopyResRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	return true;
}

///Register( E_G_ENTER_COPY_CMD, &OnEnterCopyCmd );//指令玩家进入副本；
bool CDealCenterSrvPkg::OnEnterCopyCmd( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	/*
	//1、设置欲跳副本信息；
	//2、发跳地图指令；
	//中间任何一步失败，踢玩家，以保证centersrv的副本维护逻辑；
	*/

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnEnterCopyCmd,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnEnterCopyCmd,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGEnterCopyCmd) )
	{
		return false;
	}
	EGEnterCopyCmd* pSrvMsg = (EGEnterCopyCmd*) pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->enterPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnEnterCopyCmd 找不到对应的Player\n");
		return false;
	}

	pPlayer->OnPlayerEnterRealCopyCmd( pSrvMsg );

	return true;
	TRY_END;
	return false;
}

///Register( E_G_LEAVE_COPY_CMD, &OnLeaveCopyCmd );//指令玩家离开副本
bool CDealCenterSrvPkg::OnLeaveCopyCmd( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	/*
	//1、设置欲outmap信息(或清可能的欲跳副本信息)；
	//2、发跳地图指令；
	//中间任何一步失败，踢玩家，以保证centersrv的副本维护逻辑；
	*/

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnLeaveCopyCmd,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnLeaveCopyCmd,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGLeaveCopyCmd) )
	{
		return false;
	}
	EGLeaveCopyCmd* pSrvMsg = (EGLeaveCopyCmd*) pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->leavePlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnLeaveCopyCmd 找不到对应的Player\n");
		return false;
	}

	pPlayer->OnPlayerLeaveRealCopyCmd( pSrvMsg );

	return true;
	TRY_END;
	return false;
}

///通知玩家离开副本组队(删去玩家在各副本中的第一次进入信息，并从当前副本中踢出去)，会针对副本组队拥有的每个副本发送一次；
bool CDealCenterSrvPkg::OnLeaveSTeamNoti( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnLeaveSTeamNoti,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnLeaveSTeamNoti,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGLeaveSTeamNoti) )
	{
		return false;
	}
	EGLeaveSTeamNoti* pSrvMsg = (EGLeaveSTeamNoti*) pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->leavePlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnLeaveSTeamNoti 找不到对应的Player\n");
		return false;
	}

	pPlayer->OnPlayerLeaveSteam( pSrvMsg->copyID );

	return true;
	TRY_END;
	return false;
}

///Register( E_G_GLOBE_COPYINFOS, &OnGlobeCopyInfos );//收到center发来的全局副本信息查询结果；
bool CDealCenterSrvPkg::OnGlobeCopyInfos( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnGlobeCopyInfos,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnGlobeCopyInfos,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGGlobeCopyInfos) )
	{
		return false;
	}
	EGGlobeCopyInfos* pSrvMsg = (EGGlobeCopyInfos*) pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->notiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnGlobeCopyInfos 找不到对应的Player\n");
		return false;
	}

	//转发客户端
	NewSendPlayerPkg( GCGlobeCopyInfos, pPlayer, pSrvMsg->globeCopyInfos );

	return true;
	TRY_END;
	return false;
}

///Register( E_G_TEAM_COPYINFOS, &OnTeamCopyInfos );//收到center发来的组队副本信息查询结果；
bool CDealCenterSrvPkg::OnTeamCopyInfos( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnTeamCopyInfos,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnTeamCopyInfos,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGTeamCopyInfos) )
	{
		return false;
	}
	EGTeamCopyInfos* pSrvMsg = (EGTeamCopyInfos*) pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->notiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnTeamCopyInfos 找不到对应的Player\n");
		return false;
	}

	if ( pPlayer->GetPlayerCopyFlag() != pSrvMsg->steamID )
	{
		D_WARNING( "小概率事件，向玩家%s发送组队%d的副本信息时，发现该玩家已处于不同的组队%d，放弃发送\n"
			, pPlayer->GetAccount(), pSrvMsg->steamID, pPlayer->GetPlayerCopyFlag() );
		return false;
	}

	//转发客户端
	NewSendPlayerPkg( GCTeamCopyInfos, pPlayer, pSrvMsg->teamCopyInfos );

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnEnterPWMapCheckRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnEnterPWMapCheckRst,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnEnterPWMapCheckRst,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGEnterPWMapCheckRst) )
	{
		return false;
	}

	EGEnterPWMapCheckRst* pSrvMsg = (EGEnterPWMapCheckRst*) pPkg;
	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->lNoticePlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnEnterPWMapCheckRst 找不到对应的Player\n");
		return false;
	}

	if(pSrvMsg->ret != GCEnterPWErr::OK)
	{
		//通知客户端不能进入
		GCEnterPWErr enterErr;
		enterErr.retcode = pSrvMsg->ret;
		NewSendPlayerPkg(GCEnterPWErr, pPlayer, enterErr);
		return false;
	}

	//进入新的伪线地图(区分登陆和切线进入)

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnModifyPWStateRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnModifyPWStateRst,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnModifyPWStateRst,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGModifyPWStateRst) )
	{
		return false;
	}

	EGModifyPWStateRst* pSrvMsg = (EGModifyPWStateRst*) pPkg;
	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->lNoticePlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnModifyPWStateRst 找不到对应的Player\n");
		return false;
	}

	//发送给客户端

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnModifyPWstateNtf( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnModifyPWstateNtf,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnModifyPWstateNtf,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGModifyPWStateNtf) )
	{
		return false;
	}

	EGModifyPWStateNtf* pSrvMsg = (EGModifyPWStateNtf*) pPkg;

	//更新本地状态
	CPseudowireManagerSingle::instance()->OnPWStateChange(pSrvMsg->pwid, (PseudowireArg_i::ePWState)pSrvMsg->state);

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnModifyPWMapStateRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnModifyPWMapStateRst,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnModifyPWMapStateRst,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGModifyPWMapStateRst) )
	{
		return false;
	}

	EGModifyPWMapStateRst* pSrvMsg = (EGModifyPWMapStateRst*) pPkg;
	CPlayer* pPlayer = CManPlayer::FindPlayer( pSrvMsg->lNoticePlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnModifyPWMapStateRst 找不到对应的Player\n");
		return false;
	}

	//发送给客户端

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnModifyPWMapStateNtf( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnModifyPWMapStateNtf,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnModifyPWMapStateNtf,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGModifyPWMapStateNtf) )
	{
		return false;
	}

	EGModifyPWMapStateNtf* pSrvMsg = (EGModifyPWMapStateNtf*) pPkg;

	//更新本地状态
	CPseudowireManagerSingle::instance()->OnPWMapStateChange(pSrvMsg->pwid, pSrvMsg->pwMapid, (PseudowireMapArg_i::ePWMapState)pSrvMsg->state);

	return true;
	TRY_END;
	return false;
}

bool CDealCenterSrvPkg::OnPWMapPlayerNumNtf( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealCenterSrvPkg::OnPWMapPlayerNumNtf,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealCenterSrvPkg::OnPWMapPlayerNumNtf,NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(EGPWMapPlayerNumNtf) )
	{
		return false;
	}

	EGPWMapPlayerNumNtf* pSrvMsg = (EGPWMapPlayerNumNtf*) pPkg;

	//更新本地状态
	CPseudowireManagerSingle::instance()->OnPWMapPlayerNumChange(pSrvMsg->pwid, pSrvMsg->pwMapid, pSrvMsg->pwCurrentNum);

	return true;
	TRY_END;
	return false;
}
